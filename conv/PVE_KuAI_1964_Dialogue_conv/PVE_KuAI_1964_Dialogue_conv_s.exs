<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVE_KuAI_1964_Dialogue_conv-afterFW-fixed</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVE_KuAI_1964_Dialogue_conv.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">83</ud-information>
            <ud-information attribute-name="# HIAT:w">61</ud-information>
            <ud-information attribute-name="# e">61</ud-information>
            <ud-information attribute-name="# HIAT:u">19</ud-information>
            <ud-information attribute-name="# sc">16</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVE">
            <abbreviation>PVE</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="KuAI">
            <abbreviation>KuAI</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T45" />
         <tli id="T44" />
         <tli id="T43" />
         <tli id="T0" />
         <tli id="T49" />
         <tli id="T48" />
         <tli id="T47" />
         <tli id="T46" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T53" />
         <tli id="T52" />
         <tli id="T51" />
         <tli id="T50" />
         <tli id="T13" />
         <tli id="T54" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T58" />
         <tli id="T57" />
         <tli id="T56" />
         <tli id="T55" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T61" />
         <tli id="T60" />
         <tli id="T59" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-PVE"
                      id="tx-PVE"
                      speaker="PVE"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-PVE">
            <ts e="T1" id="Seg_0" n="sc" s="T45">
               <ts e="T49" id="Seg_2" n="HIAT:u" s="T45">
                  <ts e="T44" id="Seg_4" n="HIAT:w" s="T45">qwällo</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_7" n="HIAT:w" s="T44">qaran</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0" id="Seg_10" n="HIAT:w" s="T43">mandulʼe</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_13" n="HIAT:w" s="T0">tasse</ts>
                  <nts id="Seg_14" n="HIAT:ip">?</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_17" n="HIAT:u" s="T49">
                  <ts e="T48" id="Seg_19" n="HIAT:w" s="T49">qwänǯandə</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_22" n="HIAT:w" s="T48">assə</ts>
                  <nts id="Seg_23" n="HIAT:ip">?</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1" id="Seg_26" n="HIAT:u" s="T47">
                  <ts e="T46" id="Seg_28" n="HIAT:w" s="T47">meːŋa</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1" id="Seg_31" n="HIAT:w" s="T46">nʼekutse</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T13" id="Seg_34" n="sc" s="T53">
               <ts e="T13" id="Seg_36" n="HIAT:u" s="T53">
                  <ts e="T52" id="Seg_38" n="HIAT:w" s="T53">a</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_41" n="HIAT:w" s="T52">tä</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_44" n="HIAT:w" s="T51">kundoqɨnɨ</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T50">tʼümbalʼi</ts>
                  <nts id="Seg_48" n="HIAT:ip">?</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T58" id="Seg_50" n="sc" s="T54">
               <ts e="T58" id="Seg_52" n="HIAT:u" s="T54">
                  <ts e="T14" id="Seg_54" n="HIAT:w" s="T54">tan</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_57" n="HIAT:w" s="T14">nin</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_60" n="HIAT:w" s="T15">kundoqɨn</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_63" n="HIAT:w" s="T16">illandə</ts>
                  <nts id="Seg_64" n="HIAT:ip">?</nts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T61" id="Seg_66" n="sc" s="T17">
               <ts e="T61" id="Seg_68" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_70" n="HIAT:w" s="T17">kutʼän</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_73" n="HIAT:w" s="T18">tan</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_76" n="HIAT:w" s="T19">ilɨzandə</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_79" n="HIAT:w" s="T20">iːr</ts>
                  <nts id="Seg_80" n="HIAT:ip">?</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-PVE">
            <ts e="T1" id="Seg_82" n="sc" s="T45">
               <ts e="T44" id="Seg_84" n="e" s="T45">qwällo </ts>
               <ts e="T43" id="Seg_86" n="e" s="T44">qaran </ts>
               <ts e="T0" id="Seg_88" n="e" s="T43">mandulʼe </ts>
               <ts e="T49" id="Seg_90" n="e" s="T0">tasse? </ts>
               <ts e="T48" id="Seg_92" n="e" s="T49">qwänǯandə </ts>
               <ts e="T47" id="Seg_94" n="e" s="T48">assə? </ts>
               <ts e="T46" id="Seg_96" n="e" s="T47">meːŋa </ts>
               <ts e="T1" id="Seg_98" n="e" s="T46">nʼekutse. </ts>
            </ts>
            <ts e="T13" id="Seg_99" n="sc" s="T53">
               <ts e="T52" id="Seg_101" n="e" s="T53">a </ts>
               <ts e="T51" id="Seg_103" n="e" s="T52">tä </ts>
               <ts e="T50" id="Seg_105" n="e" s="T51">kundoqɨnɨ </ts>
               <ts e="T13" id="Seg_107" n="e" s="T50">tʼümbalʼi? </ts>
            </ts>
            <ts e="T58" id="Seg_108" n="sc" s="T54">
               <ts e="T14" id="Seg_110" n="e" s="T54">tan </ts>
               <ts e="T15" id="Seg_112" n="e" s="T14">nin </ts>
               <ts e="T16" id="Seg_114" n="e" s="T15">kundoqɨn </ts>
               <ts e="T58" id="Seg_116" n="e" s="T16">illandə? </ts>
            </ts>
            <ts e="T61" id="Seg_117" n="sc" s="T17">
               <ts e="T18" id="Seg_119" n="e" s="T17">kutʼän </ts>
               <ts e="T19" id="Seg_121" n="e" s="T18">tan </ts>
               <ts e="T20" id="Seg_123" n="e" s="T19">ilɨzandə </ts>
               <ts e="T61" id="Seg_125" n="e" s="T20">iːr? </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-PVE">
            <ta e="T49" id="Seg_126" s="T45">PVE_KuAI_1964_Dialogue_conv.PVE.001 (001.001)</ta>
            <ta e="T47" id="Seg_127" s="T49">PVE_KuAI_1964_Dialogue_conv.PVE.002 (001.002)</ta>
            <ta e="T1" id="Seg_128" s="T47">PVE_KuAI_1964_Dialogue_conv.PVE.003 (001.003)</ta>
            <ta e="T13" id="Seg_129" s="T53">PVE_KuAI_1964_Dialogue_conv.PVE.004 (001.008)</ta>
            <ta e="T58" id="Seg_130" s="T54">PVE_KuAI_1964_Dialogue_conv.PVE.005 (001.010)</ta>
            <ta e="T61" id="Seg_131" s="T17">PVE_KuAI_1964_Dialogue_conv.PVE.006 (001.012)</ta>
         </annotation>
         <annotation name="st" tierref="st-PVE">
            <ta e="T49" id="Seg_132" s="T45">kwӓ′llо ′kаран мандулʼе та′ссе?</ta>
            <ta e="T47" id="Seg_133" s="T49">kwӓнджандъ ассъ?</ta>
            <ta e="T1" id="Seg_134" s="T47">′ме̄ңа ′нʼекутсе.</ta>
            <ta e="T13" id="Seg_135" s="T53">а тӓ кун′доkыны ′тʼӱмбалʼи?</ta>
            <ta e="T58" id="Seg_136" s="T54">тан нин кун′доkын и′лландъ?</ta>
            <ta e="T61" id="Seg_137" s="T17">ку′тʼӓн тан илы′зандъ ӣр?</ta>
         </annotation>
         <annotation name="stl" tierref="stl-PVE">
            <ta e="T49" id="Seg_138" s="T45">qwällo qaran mandulʼe tasse?</ta>
            <ta e="T47" id="Seg_139" s="T49">qwänǯandə assə?</ta>
            <ta e="T1" id="Seg_140" s="T47">meːŋa nʼekutse.</ta>
            <ta e="T13" id="Seg_141" s="T53">a tä kundoqɨnɨ tʼümbalʼi?</ta>
            <ta e="T58" id="Seg_142" s="T54">tan nin kundoqɨn illandə?</ta>
            <ta e="T61" id="Seg_143" s="T17">kutʼän tan ilɨzandə iːr?</ta>
         </annotation>
         <annotation name="ts" tierref="ts-PVE">
            <ta e="T49" id="Seg_144" s="T45">qwällo qaran mandulʼe tasse? </ta>
            <ta e="T47" id="Seg_145" s="T49">qwänǯandə assə? </ta>
            <ta e="T1" id="Seg_146" s="T47">meːŋa nʼekutse. </ta>
            <ta e="T13" id="Seg_147" s="T53">a tä kundoqɨnɨ tʼümbalʼi? </ta>
            <ta e="T58" id="Seg_148" s="T54">tan nin kundoqɨn illandə? </ta>
            <ta e="T61" id="Seg_149" s="T17">kutʼän tan ilɨzandə iːr? </ta>
         </annotation>
         <annotation name="mb" tierref="mb-PVE">
            <ta e="T44" id="Seg_150" s="T45">qwäl-lo</ta>
            <ta e="T43" id="Seg_151" s="T44">qara-n</ta>
            <ta e="T0" id="Seg_152" s="T43">mandu-lʼe</ta>
            <ta e="T49" id="Seg_153" s="T0">tas-se</ta>
            <ta e="T48" id="Seg_154" s="T49">qwä-nǯa-ndə</ta>
            <ta e="T47" id="Seg_155" s="T48">assə</ta>
            <ta e="T46" id="Seg_156" s="T47">meːŋa</ta>
            <ta e="T1" id="Seg_157" s="T46">nʼe-kut-se</ta>
            <ta e="T52" id="Seg_158" s="T53">a</ta>
            <ta e="T51" id="Seg_159" s="T52">tä</ta>
            <ta e="T50" id="Seg_160" s="T51">kundo-qɨnɨ</ta>
            <ta e="T13" id="Seg_161" s="T50">tʼü-mba-lʼi</ta>
            <ta e="T14" id="Seg_162" s="T54">tat</ta>
            <ta e="T15" id="Seg_163" s="T14">nin</ta>
            <ta e="T16" id="Seg_164" s="T15">kundo-qən</ta>
            <ta e="T58" id="Seg_165" s="T16">illa-ndə</ta>
            <ta e="T18" id="Seg_166" s="T17">kutʼä-n</ta>
            <ta e="T19" id="Seg_167" s="T18">tat</ta>
            <ta e="T20" id="Seg_168" s="T19">illɨ-za-ndə</ta>
            <ta e="T61" id="Seg_169" s="T20">iːr</ta>
         </annotation>
         <annotation name="mp" tierref="mp-PVE">
            <ta e="T44" id="Seg_170" s="T45">qwən-laj</ta>
            <ta e="T43" id="Seg_171" s="T44">qarɨ-n</ta>
            <ta e="T0" id="Seg_172" s="T43">mandu-le</ta>
            <ta e="T49" id="Seg_173" s="T0">tan-se</ta>
            <ta e="T48" id="Seg_174" s="T49">qwən-nǯɨ-ndɨ</ta>
            <ta e="T47" id="Seg_175" s="T48">assɨ</ta>
            <ta e="T46" id="Seg_176" s="T47">mäkkä</ta>
            <ta e="T1" id="Seg_177" s="T46">nʼe-kutti-se</ta>
            <ta e="T52" id="Seg_178" s="T53">a</ta>
            <ta e="T51" id="Seg_179" s="T52">tɛː</ta>
            <ta e="T50" id="Seg_180" s="T51">kundɨ-qɨnnɨ</ta>
            <ta e="T13" id="Seg_181" s="T50">tüː-mbɨ-li</ta>
            <ta e="T14" id="Seg_182" s="T54">tan</ta>
            <ta e="T15" id="Seg_183" s="T14">niːn</ta>
            <ta e="T16" id="Seg_184" s="T15">kundɨ-qən</ta>
            <ta e="T58" id="Seg_185" s="T16">illɨ-ndɨ</ta>
            <ta e="T18" id="Seg_186" s="T17">kuːča-n</ta>
            <ta e="T19" id="Seg_187" s="T18">tan</ta>
            <ta e="T20" id="Seg_188" s="T19">illɨ-sɨ-ndɨ</ta>
            <ta e="T61" id="Seg_189" s="T20">irä</ta>
         </annotation>
         <annotation name="ge" tierref="ge-PVE">
            <ta e="T44" id="Seg_190" s="T45">go.away-OPT.1DU</ta>
            <ta e="T43" id="Seg_191" s="T44">morning-ADV.LOC</ta>
            <ta e="T0" id="Seg_192" s="T43">cowberry.[VBLZ]-CVB</ta>
            <ta e="T49" id="Seg_193" s="T0">you.SG.NOM-COM</ta>
            <ta e="T48" id="Seg_194" s="T49">go.away-FUT-2SG.S</ta>
            <ta e="T47" id="Seg_195" s="T48">NEG</ta>
            <ta e="T46" id="Seg_196" s="T47">I.ALL</ta>
            <ta e="T1" id="Seg_197" s="T46">NEG-who-COM</ta>
            <ta e="T52" id="Seg_198" s="T53">but</ta>
            <ta e="T51" id="Seg_199" s="T52">you.PL.NOM</ta>
            <ta e="T50" id="Seg_200" s="T51">long-EL</ta>
            <ta e="T13" id="Seg_201" s="T50">come-PST.NAR-2DU</ta>
            <ta e="T14" id="Seg_202" s="T54">you.SG.NOM</ta>
            <ta e="T15" id="Seg_203" s="T14">there</ta>
            <ta e="T16" id="Seg_204" s="T15">long-LOC</ta>
            <ta e="T58" id="Seg_205" s="T16">live-2SG.S</ta>
            <ta e="T18" id="Seg_206" s="T17">where-ADV.LOC</ta>
            <ta e="T19" id="Seg_207" s="T18">you.SG.NOM</ta>
            <ta e="T20" id="Seg_208" s="T19">live-PST-2SG.S</ta>
            <ta e="T61" id="Seg_209" s="T20">month.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr-PVE">
            <ta e="T44" id="Seg_210" s="T45">уйти-OPT.1DU</ta>
            <ta e="T43" id="Seg_211" s="T44">утро-ADV.LOC</ta>
            <ta e="T0" id="Seg_212" s="T43">брусника.[VBLZ]-CVB</ta>
            <ta e="T49" id="Seg_213" s="T0">ты.NOM-COM</ta>
            <ta e="T48" id="Seg_214" s="T49">уйти-FUT-2SG.S</ta>
            <ta e="T47" id="Seg_215" s="T48">NEG</ta>
            <ta e="T46" id="Seg_216" s="T47">я.ALL</ta>
            <ta e="T1" id="Seg_217" s="T46">NEG-кто-COM</ta>
            <ta e="T52" id="Seg_218" s="T53">а</ta>
            <ta e="T51" id="Seg_219" s="T52">вы.PL.NOM</ta>
            <ta e="T50" id="Seg_220" s="T51">долго-EL</ta>
            <ta e="T13" id="Seg_221" s="T50">прийти-PST.NAR-2DU</ta>
            <ta e="T14" id="Seg_222" s="T54">ты.NOM</ta>
            <ta e="T15" id="Seg_223" s="T14">там</ta>
            <ta e="T16" id="Seg_224" s="T15">долго-LOC</ta>
            <ta e="T58" id="Seg_225" s="T16">жить-2SG.S</ta>
            <ta e="T18" id="Seg_226" s="T17">куда-ADV.LOC</ta>
            <ta e="T19" id="Seg_227" s="T18">ты.NOM</ta>
            <ta e="T20" id="Seg_228" s="T19">жить-PST-2SG.S</ta>
            <ta e="T61" id="Seg_229" s="T20">месяц.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc-PVE">
            <ta e="T44" id="Seg_230" s="T45">v-v:mood.pn</ta>
            <ta e="T43" id="Seg_231" s="T44">n-adv:case</ta>
            <ta e="T0" id="Seg_232" s="T43">n.[n&gt;v]-v&gt;adv</ta>
            <ta e="T49" id="Seg_233" s="T0">pers-n:case</ta>
            <ta e="T48" id="Seg_234" s="T49">v-v:tense-v:pn</ta>
            <ta e="T47" id="Seg_235" s="T48">ptcl</ta>
            <ta e="T46" id="Seg_236" s="T47">pers</ta>
            <ta e="T1" id="Seg_237" s="T46">pro:neg-interrog-n:case</ta>
            <ta e="T52" id="Seg_238" s="T53">conj</ta>
            <ta e="T51" id="Seg_239" s="T52">pers</ta>
            <ta e="T50" id="Seg_240" s="T51">adv-n:case</ta>
            <ta e="T13" id="Seg_241" s="T50">v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_242" s="T54">pers</ta>
            <ta e="T15" id="Seg_243" s="T14">adv</ta>
            <ta e="T16" id="Seg_244" s="T15">adv-n:case</ta>
            <ta e="T58" id="Seg_245" s="T16">v-v:pn</ta>
            <ta e="T18" id="Seg_246" s="T17">interrog-adv:case</ta>
            <ta e="T19" id="Seg_247" s="T18">pers</ta>
            <ta e="T20" id="Seg_248" s="T19">v-v:tense-v:pn</ta>
            <ta e="T61" id="Seg_249" s="T20">n.[n:case]</ta>
         </annotation>
         <annotation name="ps" tierref="ps-PVE">
            <ta e="T44" id="Seg_250" s="T45">v</ta>
            <ta e="T43" id="Seg_251" s="T44">n</ta>
            <ta e="T0" id="Seg_252" s="T43">n</ta>
            <ta e="T49" id="Seg_253" s="T0">pers</ta>
            <ta e="T48" id="Seg_254" s="T49">v</ta>
            <ta e="T47" id="Seg_255" s="T48">ptcl</ta>
            <ta e="T46" id="Seg_256" s="T47">pers</ta>
            <ta e="T1" id="Seg_257" s="T46">pro</ta>
            <ta e="T52" id="Seg_258" s="T53">conj</ta>
            <ta e="T51" id="Seg_259" s="T52">pers</ta>
            <ta e="T50" id="Seg_260" s="T51">adv</ta>
            <ta e="T13" id="Seg_261" s="T50">v</ta>
            <ta e="T14" id="Seg_262" s="T54">pers</ta>
            <ta e="T15" id="Seg_263" s="T14">adv</ta>
            <ta e="T16" id="Seg_264" s="T15">adv</ta>
            <ta e="T58" id="Seg_265" s="T16">v</ta>
            <ta e="T18" id="Seg_266" s="T17">interrog</ta>
            <ta e="T19" id="Seg_267" s="T18">pers</ta>
            <ta e="T20" id="Seg_268" s="T19">v</ta>
            <ta e="T61" id="Seg_269" s="T20">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-PVE">
            <ta e="T44" id="Seg_270" s="T45">0.1.h:A</ta>
            <ta e="T43" id="Seg_271" s="T44">adv:Time</ta>
            <ta e="T49" id="Seg_272" s="T0">pro:Com</ta>
            <ta e="T48" id="Seg_273" s="T49">0.2.h:A</ta>
            <ta e="T1" id="Seg_274" s="T46">pro:Com</ta>
            <ta e="T51" id="Seg_275" s="T52">pro.h:A</ta>
            <ta e="T50" id="Seg_276" s="T51">adv:So</ta>
            <ta e="T14" id="Seg_277" s="T54">pro.h:Th</ta>
            <ta e="T15" id="Seg_278" s="T14">adv:L</ta>
            <ta e="T16" id="Seg_279" s="T15">adv:Time</ta>
            <ta e="T18" id="Seg_280" s="T17">pro:L</ta>
            <ta e="T19" id="Seg_281" s="T18">pro.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-PVE">
            <ta e="T44" id="Seg_282" s="T45">0.1.h:S v:pred</ta>
            <ta e="T0" id="Seg_283" s="T43">s:purp</ta>
            <ta e="T48" id="Seg_284" s="T49">0.2.h:S v:pred</ta>
            <ta e="T51" id="Seg_285" s="T52">pro.h:S</ta>
            <ta e="T13" id="Seg_286" s="T50">v:pred</ta>
            <ta e="T14" id="Seg_287" s="T54">pro.h:S</ta>
            <ta e="T58" id="Seg_288" s="T16">v:pred</ta>
            <ta e="T19" id="Seg_289" s="T18">pro.h:S</ta>
            <ta e="T20" id="Seg_290" s="T19">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST-PVE" />
         <annotation name="BOR" tierref="BOR-PVE">
            <ta e="T1" id="Seg_291" s="T46">RUS:gram</ta>
            <ta e="T52" id="Seg_292" s="T53">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-PVE" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-PVE" />
         <annotation name="CS" tierref="CS-PVE" />
         <annotation name="fe" tierref="fe-PVE">
            <ta e="T49" id="Seg_293" s="T45">Let's go get pick berries tomorrow. </ta>
            <ta e="T47" id="Seg_294" s="T49">Are you coming or not?</ta>
            <ta e="T1" id="Seg_295" s="T47">I have noone to come with me.</ta>
            <ta e="T13" id="Seg_296" s="T53">Have you come from far away?</ta>
            <ta e="T58" id="Seg_297" s="T54">Have you been living there for a long time?</ta>
            <ta e="T61" id="Seg_298" s="T17">Where did you live earlier?</ta>
         </annotation>
         <annotation name="fg" tierref="fg-PVE">
            <ta e="T49" id="Seg_299" s="T45">Lass uns morgen Beere sammeln.</ta>
            <ta e="T47" id="Seg_300" s="T49">Kommst du oder nicht?</ta>
            <ta e="T1" id="Seg_301" s="T47">Niemand kommt mit mir.</ta>
            <ta e="T13" id="Seg_302" s="T53">Bist du von weit her gekommen?</ta>
            <ta e="T58" id="Seg_303" s="T54">Lebst du hier schon lange?</ta>
            <ta e="T61" id="Seg_304" s="T17">Wo hast du vorher gelebt?</ta>
         </annotation>
         <annotation name="fr" tierref="fr-PVE">
            <ta e="T49" id="Seg_305" s="T45">Пойдем завтра за ягодой с тобой.</ta>
            <ta e="T47" id="Seg_306" s="T49">Пойдешь или нет?</ta>
            <ta e="T1" id="Seg_307" s="T47">Мне не с кем.</ta>
            <ta e="T13" id="Seg_308" s="T53">А вы издалека приехали?</ta>
            <ta e="T58" id="Seg_309" s="T54">Ты там давно живешь?</ta>
            <ta e="T61" id="Seg_310" s="T17">Где ты раньше жила?</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr-PVE">
            <ta e="T49" id="Seg_311" s="T45">пойдем завтра за ягодой с тобой</ta>
            <ta e="T47" id="Seg_312" s="T49">пойдешь или нет</ta>
            <ta e="T1" id="Seg_313" s="T47">мне не с кем</ta>
            <ta e="T13" id="Seg_314" s="T53">а вы издалека приехали</ta>
            <ta e="T58" id="Seg_315" s="T54">ты там давно живешь</ta>
            <ta e="T61" id="Seg_316" s="T17">где ты раньше жила</ta>
         </annotation>
         <annotation name="nt" tierref="nt-PVE" />
         <annotation name="nto" tierref="nto-PVE" />
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-KuAI"
                      id="tx-KuAI"
                      speaker="KuAI"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KuAI">
            <ts e="T53" id="Seg_317" n="sc" s="T1">
               <ts e="T2" id="Seg_319" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_321" n="HIAT:w" s="T1">kundoktɨ</ts>
                  <nts id="Seg_322" n="HIAT:ip">?</nts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T6" id="Seg_325" n="HIAT:u" s="T2">
                  <ts e="T3" id="Seg_327" n="HIAT:w" s="T2">tamdʼe</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_330" n="HIAT:w" s="T3">magazin</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_333" n="HIAT:w" s="T4">tämdas</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_336" n="HIAT:w" s="T5">assə</ts>
                  <nts id="Seg_337" n="HIAT:ip">?</nts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_340" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_342" n="HIAT:w" s="T6">počta</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_345" n="HIAT:w" s="T7">ass</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_348" n="HIAT:w" s="T8">tʼükomba</ts>
                  <nts id="Seg_349" n="HIAT:ip">?</nts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_352" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_354" n="HIAT:w" s="T9">mega</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_357" n="HIAT:w" s="T10">näkrɨm</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_360" n="HIAT:w" s="T11">nadə</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_363" n="HIAT:w" s="T12">üːtku</ts>
                  <nts id="Seg_364" n="HIAT:ip">.</nts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T54" id="Seg_366" n="sc" s="T13">
               <ts e="T54" id="Seg_368" n="HIAT:u" s="T13">
                  <ts e="T54" id="Seg_370" n="HIAT:w" s="T13">Novosibirskaɣannɨ</ts>
                  <nts id="Seg_371" n="HIAT:ip">.</nts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T17" id="Seg_373" n="sc" s="T58">
               <ts e="T17" id="Seg_375" n="HIAT:u" s="T58">
                  <ts e="T57" id="Seg_377" n="HIAT:w" s="T58">assə</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_380" n="HIAT:w" s="T57">kundokɨn</ts>
                  <nts id="Seg_381" n="HIAT:ip">,</nts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_384" n="HIAT:w" s="T56">muqtut</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_387" n="HIAT:w" s="T55">iːrret</ts>
                  <nts id="Seg_388" n="HIAT:ip">.</nts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T42" id="Seg_390" n="sc" s="T61">
               <ts e="T21" id="Seg_392" n="HIAT:u" s="T61">
                  <ts e="T60" id="Seg_394" n="HIAT:w" s="T61">man</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_397" n="HIAT:w" s="T60">Мoskvaɣɨn</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_400" n="HIAT:w" s="T59">tʼeːlambaŋ</ts>
                  <nts id="Seg_401" n="HIAT:ip">.</nts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_404" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_406" n="HIAT:w" s="T21">Мoskvaɣɨn</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_409" n="HIAT:w" s="T22">man</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_412" n="HIAT:w" s="T23">oːɣalǯikuzaŋ</ts>
                  <nts id="Seg_413" n="HIAT:ip">.</nts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_416" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_418" n="HIAT:w" s="T24">man</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_421" n="HIAT:w" s="T25">oːɣalǯikuzaŋ</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_424" n="HIAT:w" s="T26">kundɨ</ts>
                  <nts id="Seg_425" n="HIAT:ip">.</nts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_428" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_430" n="HIAT:w" s="T27">man</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_433" n="HIAT:w" s="T28">malmǯɨsam</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_436" n="HIAT:w" s="T29">šɨtt</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_439" n="HIAT:w" s="T30">instituːtam</ts>
                  <nts id="Seg_440" n="HIAT:ip">,</nts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_443" n="HIAT:w" s="T31">aspiranturam</ts>
                  <nts id="Seg_444" n="HIAT:ip">.</nts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_447" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_449" n="HIAT:w" s="T32">müːttäɣɨn</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_452" n="HIAT:w" s="T33">eppa</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_455" n="HIAT:w" s="T34">tättə</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_458" n="HIAT:w" s="T35">pottə</ts>
                  <nts id="Seg_459" n="HIAT:ip">.</nts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_462" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_464" n="HIAT:w" s="T36">man</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_467" n="HIAT:w" s="T37">eːkkuzaŋ</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_470" n="HIAT:w" s="T38">woennaja</ts>
                  <nts id="Seg_471" n="HIAT:ip">,</nts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_474" n="HIAT:w" s="T39">kapitan</ts>
                  <nts id="Seg_475" n="HIAT:ip">.</nts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_478" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_480" n="HIAT:w" s="T40">man</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_483" n="HIAT:w" s="T41">oːɣalʼǯikuzam</ts>
                  <nts id="Seg_484" n="HIAT:ip">.</nts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KuAI">
            <ts e="T53" id="Seg_486" n="sc" s="T1">
               <ts e="T2" id="Seg_488" n="e" s="T1">kundoktɨ? </ts>
               <ts e="T3" id="Seg_490" n="e" s="T2">tamdʼe </ts>
               <ts e="T4" id="Seg_492" n="e" s="T3">magazin </ts>
               <ts e="T5" id="Seg_494" n="e" s="T4">tämdas </ts>
               <ts e="T6" id="Seg_496" n="e" s="T5">assə? </ts>
               <ts e="T7" id="Seg_498" n="e" s="T6">počta </ts>
               <ts e="T8" id="Seg_500" n="e" s="T7">ass </ts>
               <ts e="T9" id="Seg_502" n="e" s="T8">tʼükomba? </ts>
               <ts e="T10" id="Seg_504" n="e" s="T9">mega </ts>
               <ts e="T11" id="Seg_506" n="e" s="T10">näkrɨm </ts>
               <ts e="T12" id="Seg_508" n="e" s="T11">nadə </ts>
               <ts e="T53" id="Seg_510" n="e" s="T12">üːtku. </ts>
            </ts>
            <ts e="T54" id="Seg_511" n="sc" s="T13">
               <ts e="T54" id="Seg_513" n="e" s="T13">Novosibirskaɣannɨ. </ts>
            </ts>
            <ts e="T17" id="Seg_514" n="sc" s="T58">
               <ts e="T57" id="Seg_516" n="e" s="T58">assə </ts>
               <ts e="T56" id="Seg_518" n="e" s="T57">kundokɨn, </ts>
               <ts e="T55" id="Seg_520" n="e" s="T56">muqtut </ts>
               <ts e="T17" id="Seg_522" n="e" s="T55">iːrret. </ts>
            </ts>
            <ts e="T42" id="Seg_523" n="sc" s="T61">
               <ts e="T60" id="Seg_525" n="e" s="T61">man </ts>
               <ts e="T59" id="Seg_527" n="e" s="T60">Мoskvaɣɨn </ts>
               <ts e="T21" id="Seg_529" n="e" s="T59">tʼeːlambaŋ. </ts>
               <ts e="T22" id="Seg_531" n="e" s="T21">Мoskvaɣɨn </ts>
               <ts e="T23" id="Seg_533" n="e" s="T22">man </ts>
               <ts e="T24" id="Seg_535" n="e" s="T23">oːɣalǯikuzaŋ. </ts>
               <ts e="T25" id="Seg_537" n="e" s="T24">man </ts>
               <ts e="T26" id="Seg_539" n="e" s="T25">oːɣalǯikuzaŋ </ts>
               <ts e="T27" id="Seg_541" n="e" s="T26">kundɨ. </ts>
               <ts e="T28" id="Seg_543" n="e" s="T27">man </ts>
               <ts e="T29" id="Seg_545" n="e" s="T28">malmǯɨsam </ts>
               <ts e="T30" id="Seg_547" n="e" s="T29">šɨtt </ts>
               <ts e="T31" id="Seg_549" n="e" s="T30">instituːtam, </ts>
               <ts e="T32" id="Seg_551" n="e" s="T31">aspiranturam. </ts>
               <ts e="T33" id="Seg_553" n="e" s="T32">müːttäɣɨn </ts>
               <ts e="T34" id="Seg_555" n="e" s="T33">eppa </ts>
               <ts e="T35" id="Seg_557" n="e" s="T34">tättə </ts>
               <ts e="T36" id="Seg_559" n="e" s="T35">pottə. </ts>
               <ts e="T37" id="Seg_561" n="e" s="T36">man </ts>
               <ts e="T38" id="Seg_563" n="e" s="T37">eːkkuzaŋ </ts>
               <ts e="T39" id="Seg_565" n="e" s="T38">woennaja, </ts>
               <ts e="T40" id="Seg_567" n="e" s="T39">kapitan. </ts>
               <ts e="T41" id="Seg_569" n="e" s="T40">man </ts>
               <ts e="T42" id="Seg_571" n="e" s="T41">oːɣalʼǯikuzam. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KuAI">
            <ta e="T2" id="Seg_572" s="T1">PVE_KuAI_1964_Dialogue_conv.KuAI.001 (001.004)</ta>
            <ta e="T6" id="Seg_573" s="T2">PVE_KuAI_1964_Dialogue_conv.KuAI.002 (001.005)</ta>
            <ta e="T9" id="Seg_574" s="T6">PVE_KuAI_1964_Dialogue_conv.KuAI.003 (001.006)</ta>
            <ta e="T53" id="Seg_575" s="T9">PVE_KuAI_1964_Dialogue_conv.KuAI.004 (001.007)</ta>
            <ta e="T54" id="Seg_576" s="T13">PVE_KuAI_1964_Dialogue_conv.KuAI.005 (001.009)</ta>
            <ta e="T17" id="Seg_577" s="T58">PVE_KuAI_1964_Dialogue_conv.KuAI.006 (001.011)</ta>
            <ta e="T21" id="Seg_578" s="T61">PVE_KuAI_1964_Dialogue_conv.KuAI.007 (001.013)</ta>
            <ta e="T24" id="Seg_579" s="T21">PVE_KuAI_1964_Dialogue_conv.KuAI.008 (001.014)</ta>
            <ta e="T27" id="Seg_580" s="T24">PVE_KuAI_1964_Dialogue_conv.KuAI.009 (001.015)</ta>
            <ta e="T32" id="Seg_581" s="T27">PVE_KuAI_1964_Dialogue_conv.KuAI.010 (001.016)</ta>
            <ta e="T36" id="Seg_582" s="T32">PVE_KuAI_1964_Dialogue_conv.KuAI.011 (001.017)</ta>
            <ta e="T40" id="Seg_583" s="T36">PVE_KuAI_1964_Dialogue_conv.KuAI.012 (001.018)</ta>
            <ta e="T42" id="Seg_584" s="T40">PVE_KuAI_1964_Dialogue_conv.KuAI.013 (001.019)</ta>
         </annotation>
         <annotation name="st" tierref="st-KuAI">
            <ta e="T2" id="Seg_585" s="T1">кундокты?</ta>
            <ta e="T6" id="Seg_586" s="T2">там′дʼе мага′зин ′тӓмдас ассъ?</ta>
            <ta e="T9" id="Seg_587" s="T6">почта асс ′тʼӱкомба?</ta>
            <ta e="T53" id="Seg_588" s="T9">мега ′не̨(ӓ)крым надъ ′ӱ̄тку.</ta>
            <ta e="T54" id="Seg_589" s="T13">Новосибирскаɣанны.</ta>
            <ta e="T17" id="Seg_590" s="T58">′ассъ кун′докын, ′муkтут ӣ′ррет.</ta>
            <ta e="T21" id="Seg_591" s="T61">ман Москваɣын ′тʼе̄ламбаң.</ta>
            <ta e="T24" id="Seg_592" s="T21">Москваɣын ман ′о̄ɣалджику′заң.</ta>
            <ta e="T27" id="Seg_593" s="T24">ман ′о̄ɣалджикузаң кунды.</ta>
            <ta e="T32" id="Seg_594" s="T27">ман ′малмджысам ′шытт инститӯтам, аспирантурам.</ta>
            <ta e="T36" id="Seg_595" s="T32">′мӱ̄ттӓɣын ′еппа ′тӓттъ поттъ.</ta>
            <ta e="T40" id="Seg_596" s="T36">ман ′е̄ккузаң военная, капитан.</ta>
            <ta e="T42" id="Seg_597" s="T40">ман ′о̄ɣалʼджикузам.</ta>
         </annotation>
         <annotation name="stl" tierref="stl-KuAI">
            <ta e="T2" id="Seg_598" s="T1">kundoktɨ?</ta>
            <ta e="T6" id="Seg_599" s="T2">tamdʼe magazin tämdas assə?</ta>
            <ta e="T9" id="Seg_600" s="T6">počta ass tʼükomba?</ta>
            <ta e="T53" id="Seg_601" s="T9">mega ne(ä)krɨm nadə üːtku.</ta>
            <ta e="T54" id="Seg_602" s="T13">Нovosibirskaɣannɨ.</ta>
            <ta e="T17" id="Seg_603" s="T58">assə kundokɨn, muqtut iːrret.</ta>
            <ta e="T21" id="Seg_604" s="T61">man Мoskvaɣɨn tʼeːlambaŋ.</ta>
            <ta e="T24" id="Seg_605" s="T21">Мoskvaɣɨn man oːɣalǯikuzaŋ.</ta>
            <ta e="T27" id="Seg_606" s="T24">man oːɣalǯikuzaŋ kundɨ.</ta>
            <ta e="T32" id="Seg_607" s="T27">man malmǯɨsam šɨtt instituːtam, aspiranturam.</ta>
            <ta e="T36" id="Seg_608" s="T32">müːttäɣɨn eppa tättə pottə.</ta>
            <ta e="T40" id="Seg_609" s="T36">man eːkkuzaŋ voennaja, kapitan.</ta>
            <ta e="T42" id="Seg_610" s="T40">man oːɣalʼǯikuzam.</ta>
         </annotation>
         <annotation name="ts" tierref="ts-KuAI">
            <ta e="T2" id="Seg_611" s="T1">kundoktɨ? </ta>
            <ta e="T6" id="Seg_612" s="T2">tamdʼe magazin tämdas assə? </ta>
            <ta e="T9" id="Seg_613" s="T6">počta ass tʼükomba? </ta>
            <ta e="T53" id="Seg_614" s="T9">mega näkrɨm nadə üːtku. </ta>
            <ta e="T54" id="Seg_615" s="T13">Novosibirskaɣannɨ. </ta>
            <ta e="T17" id="Seg_616" s="T58">assə kundokɨn, muqtut iːrret. </ta>
            <ta e="T21" id="Seg_617" s="T61">man Мoskvaɣɨn tʼeːlambaŋ. </ta>
            <ta e="T24" id="Seg_618" s="T21">Мoskvaɣɨn man oːɣalǯikuzaŋ. </ta>
            <ta e="T27" id="Seg_619" s="T24">man oːɣalǯikuzaŋ kundɨ. </ta>
            <ta e="T32" id="Seg_620" s="T27">man malmǯɨsam šɨtt instituːtam, aspiranturam. </ta>
            <ta e="T36" id="Seg_621" s="T32">müːttäɣɨn eppa tättə pottə. </ta>
            <ta e="T40" id="Seg_622" s="T36">man eːkkuzaŋ woennaja, kapitan. </ta>
            <ta e="T42" id="Seg_623" s="T40">man oːɣalʼǯikuzam. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-KuAI">
            <ta e="T2" id="Seg_624" s="T1">kundoktɨ</ta>
            <ta e="T3" id="Seg_625" s="T2">tam-dʼe</ta>
            <ta e="T4" id="Seg_626" s="T3">magazin</ta>
            <ta e="T5" id="Seg_627" s="T4">tämda-s</ta>
            <ta e="T6" id="Seg_628" s="T5">assə</ta>
            <ta e="T7" id="Seg_629" s="T6">počta</ta>
            <ta e="T8" id="Seg_630" s="T7">ass</ta>
            <ta e="T9" id="Seg_631" s="T8">tʼü-ko-mba</ta>
            <ta e="T10" id="Seg_632" s="T9">mega</ta>
            <ta e="T11" id="Seg_633" s="T10">näkr-ɨ-m</ta>
            <ta e="T12" id="Seg_634" s="T11">nadə</ta>
            <ta e="T53" id="Seg_635" s="T12">üːt-ku</ta>
            <ta e="T54" id="Seg_636" s="T13">Novosibirsk-a-ɣannɨ</ta>
            <ta e="T57" id="Seg_637" s="T58">assə</ta>
            <ta e="T56" id="Seg_638" s="T57">kundo-kɨn</ta>
            <ta e="T55" id="Seg_639" s="T56">muqtut</ta>
            <ta e="T17" id="Seg_640" s="T55">iːrre-t</ta>
            <ta e="T60" id="Seg_641" s="T61">man</ta>
            <ta e="T59" id="Seg_642" s="T60">Мoskva-ɣɨn</ta>
            <ta e="T21" id="Seg_643" s="T59">tʼeːla-mba-ŋ</ta>
            <ta e="T22" id="Seg_644" s="T21">Мoskva-ɣɨn</ta>
            <ta e="T23" id="Seg_645" s="T22">man</ta>
            <ta e="T24" id="Seg_646" s="T23">oːɣal-ǯi-ku-za-ŋ</ta>
            <ta e="T25" id="Seg_647" s="T24">man</ta>
            <ta e="T26" id="Seg_648" s="T25">oːɣal-ǯi-ku-za-ŋ</ta>
            <ta e="T27" id="Seg_649" s="T26">kundɨ</ta>
            <ta e="T28" id="Seg_650" s="T27">man</ta>
            <ta e="T29" id="Seg_651" s="T28">malm-ǯɨ-sa-m</ta>
            <ta e="T30" id="Seg_652" s="T29">šɨtt</ta>
            <ta e="T31" id="Seg_653" s="T30">instituːt-a-m</ta>
            <ta e="T32" id="Seg_654" s="T31">aspirantura-m</ta>
            <ta e="T33" id="Seg_655" s="T32">müːttä-ɣɨn</ta>
            <ta e="T34" id="Seg_656" s="T33">e-ppa</ta>
            <ta e="T35" id="Seg_657" s="T34">tättə</ta>
            <ta e="T36" id="Seg_658" s="T35">pot-tə</ta>
            <ta e="T37" id="Seg_659" s="T36">man</ta>
            <ta e="T38" id="Seg_660" s="T37">eː-ku-za-ŋ</ta>
            <ta e="T39" id="Seg_661" s="T38">woennaja</ta>
            <ta e="T40" id="Seg_662" s="T39">kapitan</ta>
            <ta e="T41" id="Seg_663" s="T40">man</ta>
            <ta e="T42" id="Seg_664" s="T41">oːɣalʼ-ǯi-ku-za-m</ta>
         </annotation>
         <annotation name="mp" tierref="mp-KuAI">
            <ta e="T2" id="Seg_665" s="T1">kundoktɨ</ta>
            <ta e="T3" id="Seg_666" s="T2">taw-tʼelɨ</ta>
            <ta e="T4" id="Seg_667" s="T3">magazin</ta>
            <ta e="T5" id="Seg_668" s="T4">tämtɨ-sɨ</ta>
            <ta e="T6" id="Seg_669" s="T5">assɨ</ta>
            <ta e="T7" id="Seg_670" s="T6">počtə</ta>
            <ta e="T8" id="Seg_671" s="T7">assɨ</ta>
            <ta e="T9" id="Seg_672" s="T8">tüː-ku-mbɨ</ta>
            <ta e="T10" id="Seg_673" s="T9">mäkkä</ta>
            <ta e="T11" id="Seg_674" s="T10">näkɨr-ɨ-m</ta>
            <ta e="T12" id="Seg_675" s="T11">nadə</ta>
            <ta e="T53" id="Seg_676" s="T12">üːtɨ-gu</ta>
            <ta e="T54" id="Seg_677" s="T13">Novosibirsk-ɨ-qɨnnɨ</ta>
            <ta e="T57" id="Seg_678" s="T58">assɨ</ta>
            <ta e="T56" id="Seg_679" s="T57">kundɨ-qən</ta>
            <ta e="T55" id="Seg_680" s="T56">muktut</ta>
            <ta e="T17" id="Seg_681" s="T55">irä-tɨ</ta>
            <ta e="T60" id="Seg_682" s="T61">man</ta>
            <ta e="T59" id="Seg_683" s="T60">Maskwa-qən</ta>
            <ta e="T21" id="Seg_684" s="T59">tʼeːlɨ-mbɨ-ŋ</ta>
            <ta e="T22" id="Seg_685" s="T21">Maskwa-qən</ta>
            <ta e="T23" id="Seg_686" s="T22">man</ta>
            <ta e="T24" id="Seg_687" s="T23">oːqəl-nče-ku-sɨ-ŋ</ta>
            <ta e="T25" id="Seg_688" s="T24">man</ta>
            <ta e="T26" id="Seg_689" s="T25">oːqəl-nče-ku-sɨ-ŋ</ta>
            <ta e="T27" id="Seg_690" s="T26">kundɨ</ta>
            <ta e="T28" id="Seg_691" s="T27">man</ta>
            <ta e="T29" id="Seg_692" s="T28">malmɨ-ču-sɨ-m</ta>
            <ta e="T30" id="Seg_693" s="T29">šittə</ta>
            <ta e="T31" id="Seg_694" s="T30">instituːt-ɨ-m</ta>
            <ta e="T32" id="Seg_695" s="T31">aspirantura-m</ta>
            <ta e="T33" id="Seg_696" s="T32">müːdu-qən</ta>
            <ta e="T34" id="Seg_697" s="T33">eː-mbɨ</ta>
            <ta e="T35" id="Seg_698" s="T34">tättɨ</ta>
            <ta e="T36" id="Seg_699" s="T35">po-tɨ</ta>
            <ta e="T37" id="Seg_700" s="T36">man</ta>
            <ta e="T38" id="Seg_701" s="T37">eː-ku-sɨ-n</ta>
            <ta e="T39" id="Seg_702" s="T38">woennaja</ta>
            <ta e="T40" id="Seg_703" s="T39">kapitan</ta>
            <ta e="T41" id="Seg_704" s="T40">man</ta>
            <ta e="T42" id="Seg_705" s="T41">oːqəl-tɨ-ku-sɨ-m</ta>
         </annotation>
         <annotation name="ge" tierref="ge-KuAI">
            <ta e="T2" id="Seg_706" s="T1">far</ta>
            <ta e="T3" id="Seg_707" s="T2">this-day.[NOM]</ta>
            <ta e="T4" id="Seg_708" s="T3">shop.[NOM]</ta>
            <ta e="T5" id="Seg_709" s="T4">trade-PST.[3SG.S]</ta>
            <ta e="T6" id="Seg_710" s="T5">NEG</ta>
            <ta e="T7" id="Seg_711" s="T6">post.[NOM]</ta>
            <ta e="T8" id="Seg_712" s="T7">NEG</ta>
            <ta e="T9" id="Seg_713" s="T8">come-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T10" id="Seg_714" s="T9">I.ALL</ta>
            <ta e="T11" id="Seg_715" s="T10">letter-EP-ACC</ta>
            <ta e="T12" id="Seg_716" s="T11">one.should</ta>
            <ta e="T53" id="Seg_717" s="T12">send-INF</ta>
            <ta e="T54" id="Seg_718" s="T13">Novosibirsk-EP-EL</ta>
            <ta e="T57" id="Seg_719" s="T58">NEG</ta>
            <ta e="T56" id="Seg_720" s="T57">long-LOC</ta>
            <ta e="T55" id="Seg_721" s="T56">six</ta>
            <ta e="T17" id="Seg_722" s="T55">month.[NOM]-3SG</ta>
            <ta e="T60" id="Seg_723" s="T61">I.NOM</ta>
            <ta e="T59" id="Seg_724" s="T60">Moscow-LOC</ta>
            <ta e="T21" id="Seg_725" s="T59">be.born-PST.NAR-1SG.S</ta>
            <ta e="T22" id="Seg_726" s="T21">Moscow-LOC</ta>
            <ta e="T23" id="Seg_727" s="T22">I.NOM</ta>
            <ta e="T24" id="Seg_728" s="T23">learn-IPFV3-HAB-PST-1SG.S</ta>
            <ta e="T25" id="Seg_729" s="T24">I.NOM</ta>
            <ta e="T26" id="Seg_730" s="T25">learn-IPFV3-HAB-PST-1SG.S</ta>
            <ta e="T27" id="Seg_731" s="T26">long</ta>
            <ta e="T28" id="Seg_732" s="T27">I.NOM</ta>
            <ta e="T29" id="Seg_733" s="T28">run.out-TR-PST-1SG.O</ta>
            <ta e="T30" id="Seg_734" s="T29">two</ta>
            <ta e="T31" id="Seg_735" s="T30">institute-EP-ACC</ta>
            <ta e="T32" id="Seg_736" s="T31">graduate.school-ACC</ta>
            <ta e="T33" id="Seg_737" s="T32">war-LOC</ta>
            <ta e="T34" id="Seg_738" s="T33">be-DUR.[3SG.S]</ta>
            <ta e="T35" id="Seg_739" s="T34">four</ta>
            <ta e="T36" id="Seg_740" s="T35">year.[NOM]-3SG</ta>
            <ta e="T37" id="Seg_741" s="T36">I.NOM</ta>
            <ta e="T38" id="Seg_742" s="T37">be-HAB-PST-3SG.S</ta>
            <ta e="T39" id="Seg_743" s="T38">military</ta>
            <ta e="T40" id="Seg_744" s="T39">captain.[NOM]</ta>
            <ta e="T41" id="Seg_745" s="T40">I.NOM</ta>
            <ta e="T42" id="Seg_746" s="T41">learn-TR-HAB-PST-1SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr-KuAI">
            <ta e="T2" id="Seg_747" s="T1">в.даль</ta>
            <ta e="T3" id="Seg_748" s="T2">этот-день.[NOM]</ta>
            <ta e="T4" id="Seg_749" s="T3">магазин.[NOM]</ta>
            <ta e="T5" id="Seg_750" s="T4">торговать-PST.[3SG.S]</ta>
            <ta e="T6" id="Seg_751" s="T5">NEG</ta>
            <ta e="T7" id="Seg_752" s="T6">почта.[NOM]</ta>
            <ta e="T8" id="Seg_753" s="T7">NEG</ta>
            <ta e="T9" id="Seg_754" s="T8">прийти-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T10" id="Seg_755" s="T9">я.ALL</ta>
            <ta e="T11" id="Seg_756" s="T10">письмо-EP-ACC</ta>
            <ta e="T12" id="Seg_757" s="T11">надо</ta>
            <ta e="T53" id="Seg_758" s="T12">посылать-INF</ta>
            <ta e="T54" id="Seg_759" s="T13">Новосибирск-EP-EL</ta>
            <ta e="T57" id="Seg_760" s="T58">NEG</ta>
            <ta e="T56" id="Seg_761" s="T57">долго-LOC</ta>
            <ta e="T55" id="Seg_762" s="T56">шесть</ta>
            <ta e="T17" id="Seg_763" s="T55">месяц.[NOM]-3SG</ta>
            <ta e="T60" id="Seg_764" s="T61">я.NOM</ta>
            <ta e="T59" id="Seg_765" s="T60">Москва-LOC</ta>
            <ta e="T21" id="Seg_766" s="T59">родиться-PST.NAR-1SG.S</ta>
            <ta e="T22" id="Seg_767" s="T21">Москва-LOC</ta>
            <ta e="T23" id="Seg_768" s="T22">я.NOM</ta>
            <ta e="T24" id="Seg_769" s="T23">учиться-IPFV3-HAB-PST-1SG.S</ta>
            <ta e="T25" id="Seg_770" s="T24">я.NOM</ta>
            <ta e="T26" id="Seg_771" s="T25">учиться-IPFV3-HAB-PST-1SG.S</ta>
            <ta e="T27" id="Seg_772" s="T26">долго</ta>
            <ta e="T28" id="Seg_773" s="T27">я.NOM</ta>
            <ta e="T29" id="Seg_774" s="T28">кончиться-TR-PST-1SG.O</ta>
            <ta e="T30" id="Seg_775" s="T29">два</ta>
            <ta e="T31" id="Seg_776" s="T30">институт-EP-ACC</ta>
            <ta e="T32" id="Seg_777" s="T31">аспирантура-ACC</ta>
            <ta e="T33" id="Seg_778" s="T32">война-LOC</ta>
            <ta e="T34" id="Seg_779" s="T33">быть-DUR.[3SG.S]</ta>
            <ta e="T35" id="Seg_780" s="T34">четыре</ta>
            <ta e="T36" id="Seg_781" s="T35">год.[NOM]-3SG</ta>
            <ta e="T37" id="Seg_782" s="T36">я.NOM</ta>
            <ta e="T38" id="Seg_783" s="T37">быть-HAB-PST-3SG.S</ta>
            <ta e="T39" id="Seg_784" s="T38">военная</ta>
            <ta e="T40" id="Seg_785" s="T39">капитан.[NOM]</ta>
            <ta e="T41" id="Seg_786" s="T40">я.NOM</ta>
            <ta e="T42" id="Seg_787" s="T41">учиться-TR-HAB-PST-1SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc-KuAI">
            <ta e="T2" id="Seg_788" s="T1">adv</ta>
            <ta e="T3" id="Seg_789" s="T2">dem-n.[n:case]</ta>
            <ta e="T4" id="Seg_790" s="T3">n.[n:case]</ta>
            <ta e="T5" id="Seg_791" s="T4">v-v:tense.[v:pn]</ta>
            <ta e="T6" id="Seg_792" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_793" s="T6">n.[n:case]</ta>
            <ta e="T8" id="Seg_794" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_795" s="T8">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T10" id="Seg_796" s="T9">pers</ta>
            <ta e="T11" id="Seg_797" s="T10">n-n:ins-n:case</ta>
            <ta e="T12" id="Seg_798" s="T11">ptcl</ta>
            <ta e="T53" id="Seg_799" s="T12">v-v:inf</ta>
            <ta e="T54" id="Seg_800" s="T13">nprop-n:ins-n:case</ta>
            <ta e="T57" id="Seg_801" s="T58">ptcl</ta>
            <ta e="T56" id="Seg_802" s="T57">adv-n:case</ta>
            <ta e="T55" id="Seg_803" s="T56">num</ta>
            <ta e="T17" id="Seg_804" s="T55">n.[n:case]-n:poss</ta>
            <ta e="T60" id="Seg_805" s="T61">pers</ta>
            <ta e="T59" id="Seg_806" s="T60">nprop-n:case</ta>
            <ta e="T21" id="Seg_807" s="T59">v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_808" s="T21">nprop-n:case</ta>
            <ta e="T23" id="Seg_809" s="T22">pers</ta>
            <ta e="T24" id="Seg_810" s="T23">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_811" s="T24">pers</ta>
            <ta e="T26" id="Seg_812" s="T25">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_813" s="T26">adv</ta>
            <ta e="T28" id="Seg_814" s="T27">pers</ta>
            <ta e="T29" id="Seg_815" s="T28">v-n&gt;v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_816" s="T29">num</ta>
            <ta e="T31" id="Seg_817" s="T30">n-n:ins-n:case</ta>
            <ta e="T32" id="Seg_818" s="T31">n-n:case</ta>
            <ta e="T33" id="Seg_819" s="T32">n-n:case</ta>
            <ta e="T34" id="Seg_820" s="T33">v-v&gt;v.[v:pn]</ta>
            <ta e="T35" id="Seg_821" s="T34">num</ta>
            <ta e="T36" id="Seg_822" s="T35">n.[n:case]-n:poss</ta>
            <ta e="T37" id="Seg_823" s="T36">pers</ta>
            <ta e="T38" id="Seg_824" s="T37">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T39" id="Seg_825" s="T38">adj</ta>
            <ta e="T40" id="Seg_826" s="T39">n.[n:case]</ta>
            <ta e="T41" id="Seg_827" s="T40">pers</ta>
            <ta e="T42" id="Seg_828" s="T41">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps-KuAI">
            <ta e="T2" id="Seg_829" s="T1">adv</ta>
            <ta e="T3" id="Seg_830" s="T2">n</ta>
            <ta e="T4" id="Seg_831" s="T3">n</ta>
            <ta e="T5" id="Seg_832" s="T4">v</ta>
            <ta e="T6" id="Seg_833" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_834" s="T6">n</ta>
            <ta e="T8" id="Seg_835" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_836" s="T8">v</ta>
            <ta e="T10" id="Seg_837" s="T9">pers</ta>
            <ta e="T11" id="Seg_838" s="T10">n</ta>
            <ta e="T12" id="Seg_839" s="T11">ptcl</ta>
            <ta e="T53" id="Seg_840" s="T12">v</ta>
            <ta e="T54" id="Seg_841" s="T13">nprop</ta>
            <ta e="T57" id="Seg_842" s="T58">ptcl</ta>
            <ta e="T56" id="Seg_843" s="T57">adv</ta>
            <ta e="T55" id="Seg_844" s="T56">num</ta>
            <ta e="T17" id="Seg_845" s="T55">n</ta>
            <ta e="T60" id="Seg_846" s="T61">pers</ta>
            <ta e="T59" id="Seg_847" s="T60">nprop</ta>
            <ta e="T21" id="Seg_848" s="T59">v</ta>
            <ta e="T22" id="Seg_849" s="T21">nprop</ta>
            <ta e="T23" id="Seg_850" s="T22">v</ta>
            <ta e="T24" id="Seg_851" s="T23">v</ta>
            <ta e="T25" id="Seg_852" s="T24">pers</ta>
            <ta e="T26" id="Seg_853" s="T25">v</ta>
            <ta e="T27" id="Seg_854" s="T26">adv</ta>
            <ta e="T28" id="Seg_855" s="T27">pers</ta>
            <ta e="T29" id="Seg_856" s="T28">v</ta>
            <ta e="T30" id="Seg_857" s="T29">num</ta>
            <ta e="T31" id="Seg_858" s="T30">n</ta>
            <ta e="T32" id="Seg_859" s="T31">n</ta>
            <ta e="T33" id="Seg_860" s="T32">n</ta>
            <ta e="T34" id="Seg_861" s="T33">n</ta>
            <ta e="T35" id="Seg_862" s="T34">num</ta>
            <ta e="T36" id="Seg_863" s="T35">n</ta>
            <ta e="T37" id="Seg_864" s="T36">pers</ta>
            <ta e="T38" id="Seg_865" s="T37">v</ta>
            <ta e="T39" id="Seg_866" s="T38">adj</ta>
            <ta e="T40" id="Seg_867" s="T39">n</ta>
            <ta e="T41" id="Seg_868" s="T40">pers</ta>
            <ta e="T42" id="Seg_869" s="T41">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-KuAI">
            <ta e="T3" id="Seg_870" s="T2">np:Time</ta>
            <ta e="T4" id="Seg_871" s="T3">np:A</ta>
            <ta e="T7" id="Seg_872" s="T6">np:A</ta>
            <ta e="T11" id="Seg_873" s="T10">np:Th</ta>
            <ta e="T53" id="Seg_874" s="T12">v:Th</ta>
            <ta e="T54" id="Seg_875" s="T13">np:So</ta>
            <ta e="T56" id="Seg_876" s="T57">adv:Time</ta>
            <ta e="T17" id="Seg_877" s="T55">np:Time</ta>
            <ta e="T60" id="Seg_878" s="T61">pro.h:P</ta>
            <ta e="T59" id="Seg_879" s="T60">np:L</ta>
            <ta e="T22" id="Seg_880" s="T21">np:L</ta>
            <ta e="T23" id="Seg_881" s="T22">pro.h:A</ta>
            <ta e="T25" id="Seg_882" s="T24">pro.h:A</ta>
            <ta e="T27" id="Seg_883" s="T26">adv:Time</ta>
            <ta e="T28" id="Seg_884" s="T27">pro.h:A</ta>
            <ta e="T33" id="Seg_885" s="T32">np:L</ta>
            <ta e="T34" id="Seg_886" s="T33">0.3.h:Th</ta>
            <ta e="T36" id="Seg_887" s="T35">np:Time</ta>
            <ta e="T37" id="Seg_888" s="T36">pro.h:Th</ta>
            <ta e="T41" id="Seg_889" s="T40">pro.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-KuAI">
            <ta e="T4" id="Seg_890" s="T3">np:S</ta>
            <ta e="T5" id="Seg_891" s="T4">v:pred</ta>
            <ta e="T7" id="Seg_892" s="T6">np:S</ta>
            <ta e="T9" id="Seg_893" s="T8">v:pred</ta>
            <ta e="T12" id="Seg_894" s="T11">ptcl:pred</ta>
            <ta e="T53" id="Seg_895" s="T12">v:O</ta>
            <ta e="T60" id="Seg_896" s="T61">pro.h:S</ta>
            <ta e="T21" id="Seg_897" s="T59">v:pred</ta>
            <ta e="T23" id="Seg_898" s="T22">pro.h:S</ta>
            <ta e="T24" id="Seg_899" s="T23">v:pred</ta>
            <ta e="T25" id="Seg_900" s="T24">pro.h:S</ta>
            <ta e="T26" id="Seg_901" s="T25">v:pred</ta>
            <ta e="T28" id="Seg_902" s="T27">pro.h:S</ta>
            <ta e="T29" id="Seg_903" s="T28">v:pred</ta>
            <ta e="T31" id="Seg_904" s="T30">np:O</ta>
            <ta e="T32" id="Seg_905" s="T31">np:O</ta>
            <ta e="T34" id="Seg_906" s="T33">0.3.h:S v:pred</ta>
            <ta e="T37" id="Seg_907" s="T36">pro.h:S</ta>
            <ta e="T38" id="Seg_908" s="T37">cop</ta>
            <ta e="T40" id="Seg_909" s="T39">n:pred</ta>
            <ta e="T41" id="Seg_910" s="T40">pro.h:S</ta>
            <ta e="T42" id="Seg_911" s="T41">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST-KuAI" />
         <annotation name="BOR" tierref="BOR-KuAI">
            <ta e="T4" id="Seg_912" s="T3">RUS:cult</ta>
            <ta e="T7" id="Seg_913" s="T6">RUS:cult</ta>
            <ta e="T12" id="Seg_914" s="T11">RUS:gram</ta>
            <ta e="T54" id="Seg_915" s="T13">RUS:cult</ta>
            <ta e="T59" id="Seg_916" s="T60">RUS:cult</ta>
            <ta e="T22" id="Seg_917" s="T21">RUS:cult</ta>
            <ta e="T31" id="Seg_918" s="T30">RUS:cult</ta>
            <ta e="T32" id="Seg_919" s="T31">RUS:cult</ta>
            <ta e="T39" id="Seg_920" s="T38">RUS:cult</ta>
            <ta e="T40" id="Seg_921" s="T39">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-KuAI" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-KuAI" />
         <annotation name="CS" tierref="CS-KuAI" />
         <annotation name="fe" tierref="fe-KuAI">
            <ta e="T2" id="Seg_922" s="T1">Is it far away?</ta>
            <ta e="T6" id="Seg_923" s="T2">Was the shop open today?</ta>
            <ta e="T9" id="Seg_924" s="T6">Did the post come?</ta>
            <ta e="T53" id="Seg_925" s="T9">I need to send a letter.</ta>
            <ta e="T54" id="Seg_926" s="T13">From Novosibirsk.</ta>
            <ta e="T17" id="Seg_927" s="T58">Not long, six months.</ta>
            <ta e="T21" id="Seg_928" s="T61">I was born in Moscow.</ta>
            <ta e="T24" id="Seg_929" s="T21">I studied in Moscow.</ta>
            <ta e="T27" id="Seg_930" s="T24">I studied for a long time.</ta>
            <ta e="T32" id="Seg_931" s="T27">I graduated from two institutes and from post-graduate education.</ta>
            <ta e="T36" id="Seg_932" s="T32">I was at war for four years.</ta>
            <ta e="T40" id="Seg_933" s="T36">I was a military woman, a Captain.</ta>
            <ta e="T42" id="Seg_934" s="T40">I taught.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-KuAI">
            <ta e="T2" id="Seg_935" s="T1">Ist es weit?</ta>
            <ta e="T6" id="Seg_936" s="T2">Hatte der Laden heute geöffnet?</ta>
            <ta e="T9" id="Seg_937" s="T6">Ist die Post gekommen?</ta>
            <ta e="T53" id="Seg_938" s="T9">Ich muss einen Brief verschicken.</ta>
            <ta e="T54" id="Seg_939" s="T13">Aus Novosibirsk.</ta>
            <ta e="T17" id="Seg_940" s="T58">Nicht lange, sechs Monate.</ta>
            <ta e="T21" id="Seg_941" s="T61">Ich wurde in Moskau geboren.</ta>
            <ta e="T24" id="Seg_942" s="T21">Ich habe in Moskau studiert.</ta>
            <ta e="T27" id="Seg_943" s="T24">Ich habe lange studiert.</ta>
            <ta e="T32" id="Seg_944" s="T27">Ich machte Abschlüsse an zwei Instituten und einen Doktor.</ta>
            <ta e="T36" id="Seg_945" s="T32">Ich war vier Jahre im Krieg.</ta>
            <ta e="T40" id="Seg_946" s="T36">Ich war Soldatin, ein Hauptmann.</ta>
            <ta e="T42" id="Seg_947" s="T40">Ich habe unterrichtet.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KuAI">
            <ta e="T2" id="Seg_948" s="T1">Далеко?</ta>
            <ta e="T6" id="Seg_949" s="T2">Сегодня магазин работал (торговал), нет?</ta>
            <ta e="T9" id="Seg_950" s="T6">Почта не приходила?</ta>
            <ta e="T53" id="Seg_951" s="T9">Мне надо письмо отправить.</ta>
            <ta e="T54" id="Seg_952" s="T13">Из Новосибирска.</ta>
            <ta e="T17" id="Seg_953" s="T58">Не долго, шесть месяцев.</ta>
            <ta e="T21" id="Seg_954" s="T61">Я родилась в Москве.</ta>
            <ta e="T24" id="Seg_955" s="T21">В Москве я училась.</ta>
            <ta e="T27" id="Seg_956" s="T24">Я училась долго.</ta>
            <ta e="T32" id="Seg_957" s="T27">Я окончила два института, аспирантуру.</ta>
            <ta e="T36" id="Seg_958" s="T32">Была на войне четыре года.</ta>
            <ta e="T40" id="Seg_959" s="T36">Я была военная, капитан.</ta>
            <ta e="T42" id="Seg_960" s="T40">Я учила.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr-KuAI">
            <ta e="T2" id="Seg_961" s="T1">далеко</ta>
            <ta e="T6" id="Seg_962" s="T2">сегодня магазин работал (торговал) нет</ta>
            <ta e="T9" id="Seg_963" s="T6">почта не приходила</ta>
            <ta e="T53" id="Seg_964" s="T9">мне надо письмо отправить</ta>
            <ta e="T54" id="Seg_965" s="T13">из Новосибирска</ta>
            <ta e="T17" id="Seg_966" s="T58">не долго шесть месяцев</ta>
            <ta e="T21" id="Seg_967" s="T61">я родилась в Москве</ta>
            <ta e="T24" id="Seg_968" s="T21">в Москве я училась</ta>
            <ta e="T27" id="Seg_969" s="T24">я училась долго</ta>
            <ta e="T32" id="Seg_970" s="T27">я окончила два института</ta>
            <ta e="T36" id="Seg_971" s="T32">была на войне четыре года</ta>
            <ta e="T40" id="Seg_972" s="T36">я была военная</ta>
            <ta e="T42" id="Seg_973" s="T40">я учу</ta>
         </annotation>
         <annotation name="nt" tierref="nt-KuAI" />
         <annotation name="nto" tierref="nto-KuAI" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T45" />
            <conversion-tli id="T44" />
            <conversion-tli id="T43" />
            <conversion-tli id="T0" />
            <conversion-tli id="T49" />
            <conversion-tli id="T48" />
            <conversion-tli id="T47" />
            <conversion-tli id="T46" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T53" />
            <conversion-tli id="T52" />
            <conversion-tli id="T51" />
            <conversion-tli id="T50" />
            <conversion-tli id="T13" />
            <conversion-tli id="T54" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T58" />
            <conversion-tli id="T57" />
            <conversion-tli id="T56" />
            <conversion-tli id="T55" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T61" />
            <conversion-tli id="T60" />
            <conversion-tli id="T59" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-PVE"
                          name="ref"
                          segmented-tier-id="tx-PVE"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-PVE"
                          name="st"
                          segmented-tier-id="tx-PVE"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl-PVE"
                          name="stl"
                          segmented-tier-id="tx-PVE"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-PVE"
                          name="ts"
                          segmented-tier-id="tx-PVE"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-PVE"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-PVE"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-PVE"
                          name="mb"
                          segmented-tier-id="tx-PVE"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-PVE"
                          name="mp"
                          segmented-tier-id="tx-PVE"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-PVE"
                          name="ge"
                          segmented-tier-id="tx-PVE"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-PVE"
                          name="gr"
                          segmented-tier-id="tx-PVE"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-PVE"
                          name="mc"
                          segmented-tier-id="tx-PVE"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-PVE"
                          name="ps"
                          segmented-tier-id="tx-PVE"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-PVE"
                          name="SeR"
                          segmented-tier-id="tx-PVE"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-PVE"
                          name="SyF"
                          segmented-tier-id="tx-PVE"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-PVE"
                          name="IST"
                          segmented-tier-id="tx-PVE"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-PVE"
                          name="BOR"
                          segmented-tier-id="tx-PVE"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-PVE"
                          name="BOR-Phon"
                          segmented-tier-id="tx-PVE"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-PVE"
                          name="BOR-Morph"
                          segmented-tier-id="tx-PVE"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-PVE"
                          name="CS"
                          segmented-tier-id="tx-PVE"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-PVE"
                          name="fe"
                          segmented-tier-id="tx-PVE"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-PVE"
                          name="fg"
                          segmented-tier-id="tx-PVE"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-PVE"
                          name="fr"
                          segmented-tier-id="tx-PVE"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-PVE"
                          name="ltr"
                          segmented-tier-id="tx-PVE"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-PVE"
                          name="nt"
                          segmented-tier-id="tx-PVE"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto-PVE"
                          name="nto"
                          segmented-tier-id="tx-PVE"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-KuAI"
                          name="ref"
                          segmented-tier-id="tx-KuAI"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-KuAI"
                          name="st"
                          segmented-tier-id="tx-KuAI"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl-KuAI"
                          name="stl"
                          segmented-tier-id="tx-KuAI"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KuAI"
                          name="ts"
                          segmented-tier-id="tx-KuAI"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KuAI"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KuAI"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-KuAI"
                          name="mb"
                          segmented-tier-id="tx-KuAI"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-KuAI"
                          name="mp"
                          segmented-tier-id="tx-KuAI"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-KuAI"
                          name="ge"
                          segmented-tier-id="tx-KuAI"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-KuAI"
                          name="gr"
                          segmented-tier-id="tx-KuAI"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-KuAI"
                          name="mc"
                          segmented-tier-id="tx-KuAI"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-KuAI"
                          name="ps"
                          segmented-tier-id="tx-KuAI"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-KuAI"
                          name="SeR"
                          segmented-tier-id="tx-KuAI"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-KuAI"
                          name="SyF"
                          segmented-tier-id="tx-KuAI"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-KuAI"
                          name="IST"
                          segmented-tier-id="tx-KuAI"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-KuAI"
                          name="BOR"
                          segmented-tier-id="tx-KuAI"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-KuAI"
                          name="BOR-Phon"
                          segmented-tier-id="tx-KuAI"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-KuAI"
                          name="BOR-Morph"
                          segmented-tier-id="tx-KuAI"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-KuAI"
                          name="CS"
                          segmented-tier-id="tx-KuAI"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KuAI"
                          name="fe"
                          segmented-tier-id="tx-KuAI"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KuAI"
                          name="fg"
                          segmented-tier-id="tx-KuAI"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KuAI"
                          name="fr"
                          segmented-tier-id="tx-KuAI"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-KuAI"
                          name="ltr"
                          segmented-tier-id="tx-KuAI"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KuAI"
                          name="nt"
                          segmented-tier-id="tx-KuAI"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto-KuAI"
                          name="nto"
                          segmented-tier-id="tx-KuAI"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
