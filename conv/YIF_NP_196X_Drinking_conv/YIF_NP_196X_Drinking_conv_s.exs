<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>YIF_NP_196X_Drinking_conv</transcription-name>
         <referenced-file url="YIF_NP_196X_Drinking_conv.wav" />
         <referenced-file url="YIF_NP_196X_Drinking_conv.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">YIF_NP_196X_Drinking_conv.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">87</ud-information>
            <ud-information attribute-name="# HIAT:w">50</ud-information>
            <ud-information attribute-name="# e">52</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">2</ud-information>
            <ud-information attribute-name="# HIAT:u">12</ud-information>
            <ud-information attribute-name="# sc">18</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="YIF">
            <abbreviation>YIF</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="NP">
            <abbreviation>NP</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.0" type="appl" />
         <tli id="T2" time="0.602" type="appl" />
         <tli id="T3" time="1.204" type="appl" />
         <tli id="T4" time="1.806" type="appl" />
         <tli id="T5" time="2.408" type="appl" />
         <tli id="T6" time="3.01" type="appl" />
         <tli id="T7" time="3.612" type="appl" />
         <tli id="T8" time="4.2139999999999995" type="appl" />
         <tli id="T9" time="4.816" type="appl" />
         <tli id="T10" time="5.418" type="appl" />
         <tli id="T11" time="6.02" type="appl" />
         <tli id="T12" time="6.685" type="appl" />
         <tli id="T13" time="7.35" type="appl" />
         <tli id="T14" time="8.125" type="appl" />
         <tli id="T15" time="8.58" type="appl" />
         <tli id="T16" time="9.035" type="appl" />
         <tli id="T17" time="9.49" type="appl" />
         <tli id="T18" time="9.945" type="appl" />
         <tli id="T19" time="10.4" type="appl" />
         <tli id="T20" time="10.855" type="appl" />
         <tli id="T21" time="11.31" type="appl" />
         <tli id="T22" time="11.7225" type="appl" />
         <tli id="T23" time="12.135" type="appl" />
         <tli id="T24" time="13.005" type="appl" />
         <tli id="T25" time="13.488333333333333" type="appl" />
         <tli id="T26" time="13.971666666666668" type="appl" />
         <tli id="T27" time="14.455" type="appl" />
         <tli id="T28" time="14.465" type="appl" />
         <tli id="T29" time="14.834999999999999" type="appl" />
         <tli id="T30" time="15.205" type="appl" />
         <tli id="T31" time="15.575" type="appl" />
         <tli id="T32" time="16.29" type="appl" />
         <tli id="T33" time="17.314999999999998" type="appl" />
         <tli id="T34" time="18.34" type="appl" />
         <tli id="T35" time="19.916666666666668" type="appl" />
         <tli id="T36" time="21.493333333333332" type="appl" />
         <tli id="T37" time="23.07" type="appl" />
         <tli id="T38" time="23.708" type="appl" />
         <tli id="T39" time="28.763" type="appl" />
         <tli id="T40" time="28.918" type="appl" />
         <tli id="T41" time="29.83442857142857" type="appl" />
         <tli id="T42" time="30.750857142857143" type="appl" />
         <tli id="T43" time="31.66728571428571" type="appl" />
         <tli id="T44" time="32.58371428571429" type="appl" />
         <tli id="T45" time="33.500142857142855" type="appl" />
         <tli id="T46" time="34.41657142857143" type="appl" />
         <tli id="T47" time="35.333" type="appl" />
         <tli id="T48" time="35.966" type="appl" />
         <tli id="T49" time="44.333" type="appl" />
         <tli id="T50" time="44.44" type="appl" />
         <tli id="T51" time="45.4" type="appl" />
         <tli id="T52" time="46.36" type="appl" />
         <tli id="T53" time="47.32" type="appl" />
         <tli id="T54" time="48.28" type="appl" />
         <tli id="T55" time="49.239999999999995" type="appl" />
         <tli id="T56" time="50.2" type="appl" />
         <tli id="T57" time="51.16" type="appl" />
         <tli id="T58" time="52.12" type="appl" />
         <tli id="T59" time="53.08" type="appl" />
         <tli id="T60" time="54.04" type="appl" />
         <tli id="T61" time="55.0" type="appl" />
         <tli id="T0" time="55.294" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-YIF"
                      id="tx-YIF"
                      speaker="YIF"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-YIF">
            <ts e="T13" id="Seg_0" n="sc" s="T1">
               <ts e="T11" id="Seg_2" n="HIAT:u" s="T1">
                  <nts id="Seg_3" n="HIAT:ip">(</nts>
                  <ts e="T2" id="Seg_5" n="HIAT:w" s="T1">Liza</ts>
                  <nts id="Seg_6" n="HIAT:ip">/</nts>
                  <nts id="Seg_7" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_9" n="HIAT:w" s="T2">Nadeʒda</ts>
                  <nts id="Seg_10" n="HIAT:ip">)</nts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">Petrovna</ts>
                  <nts id="Seg_14" n="HIAT:ip">,</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_17" n="HIAT:w" s="T4">mat</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">čʼek</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T6">kuralbak</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">kioskan</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_28" n="HIAT:ip">—</nts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">kioskam</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">elle</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">ketemba</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_41" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">Üdegu</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">axota</ts>
                  <nts id="Seg_47" n="HIAT:ip">!</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T27" id="Seg_49" n="sc" s="T24">
               <ts e="T27" id="Seg_51" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_53" n="HIAT:w" s="T24">Maɣazin</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_56" n="HIAT:w" s="T25">kuralgegu</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_59" n="HIAT:w" s="T26">nado</ts>
                  <nts id="Seg_60" n="HIAT:ip">.</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T37" id="Seg_62" n="sc" s="T32">
               <ts e="T34" id="Seg_64" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_66" n="HIAT:w" s="T32">Üdegu</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_69" n="HIAT:w" s="T33">oxota</ts>
                  <nts id="Seg_70" n="HIAT:ip">!</nts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_73" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_75" n="HIAT:w" s="T34">Üteǯekogu</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_78" n="HIAT:w" s="T35">potom</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_81" n="HIAT:w" s="T36">nado</ts>
                  <nts id="Seg_82" n="HIAT:ip">!</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T49" id="Seg_84" n="sc" s="T48">
               <ts e="T49" id="Seg_86" n="HIAT:u" s="T48">
                  <nts id="Seg_87" n="HIAT:ip">(</nts>
                  <nts id="Seg_88" n="HIAT:ip">(</nts>
                  <ats e="T49" id="Seg_89" n="HIAT:non-pho" s="T48">…</ats>
                  <nts id="Seg_90" n="HIAT:ip">)</nts>
                  <nts id="Seg_91" n="HIAT:ip">)</nts>
                  <nts id="Seg_92" n="HIAT:ip">.</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-YIF">
            <ts e="T13" id="Seg_94" n="sc" s="T1">
               <ts e="T2" id="Seg_96" n="e" s="T1">(Liza/ </ts>
               <ts e="T3" id="Seg_98" n="e" s="T2">Nadeʒda) </ts>
               <ts e="T4" id="Seg_100" n="e" s="T3">Petrovna, </ts>
               <ts e="T5" id="Seg_102" n="e" s="T4">mat </ts>
               <ts e="T6" id="Seg_104" n="e" s="T5">čʼek </ts>
               <ts e="T7" id="Seg_106" n="e" s="T6">kuralbak </ts>
               <ts e="T8" id="Seg_108" n="e" s="T7">kioskan — </ts>
               <ts e="T9" id="Seg_110" n="e" s="T8">kioskam </ts>
               <ts e="T10" id="Seg_112" n="e" s="T9">elle </ts>
               <ts e="T11" id="Seg_114" n="e" s="T10">ketemba. </ts>
               <ts e="T12" id="Seg_116" n="e" s="T11">Üdegu </ts>
               <ts e="T13" id="Seg_118" n="e" s="T12">axota! </ts>
            </ts>
            <ts e="T27" id="Seg_119" n="sc" s="T24">
               <ts e="T25" id="Seg_121" n="e" s="T24">Maɣazin </ts>
               <ts e="T26" id="Seg_123" n="e" s="T25">kuralgegu </ts>
               <ts e="T27" id="Seg_125" n="e" s="T26">nado. </ts>
            </ts>
            <ts e="T37" id="Seg_126" n="sc" s="T32">
               <ts e="T33" id="Seg_128" n="e" s="T32">Üdegu </ts>
               <ts e="T34" id="Seg_130" n="e" s="T33">oxota! </ts>
               <ts e="T35" id="Seg_132" n="e" s="T34">Üteǯekogu </ts>
               <ts e="T36" id="Seg_134" n="e" s="T35">potom </ts>
               <ts e="T37" id="Seg_136" n="e" s="T36">nado! </ts>
            </ts>
            <ts e="T49" id="Seg_137" n="sc" s="T48">
               <ts e="T49" id="Seg_139" n="e" s="T48">((…)). </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-YIF">
            <ta e="T11" id="Seg_140" s="T1">YIF_NP_196X_Drinking_conv.YIF.001 (001)</ta>
            <ta e="T13" id="Seg_141" s="T11">YIF_NP_196X_Drinking_conv.YIF.002 (002)</ta>
            <ta e="T27" id="Seg_142" s="T24">YIF_NP_196X_Drinking_conv.YIF.003 (005)</ta>
            <ta e="T34" id="Seg_143" s="T32">YIF_NP_196X_Drinking_conv.YIF.004 (007.001)</ta>
            <ta e="T37" id="Seg_144" s="T34">YIF_NP_196X_Drinking_conv.YIF.005 (007.002)</ta>
            <ta e="T49" id="Seg_145" s="T48">YIF_NP_196X_Drinking_conv.YIF.006 (010)</ta>
         </annotation>
         <annotation name="st" tierref="st-YIF">
            <ta e="T11" id="Seg_146" s="T1">Лиза Петровна, мат чек кура́лбак кио́скан — кио́скам элле́ кэ́тэмба.</ta>
            <ta e="T13" id="Seg_147" s="T11">Ӱдэгу́ охо́та!</ta>
            <ta e="T27" id="Seg_148" s="T24">В магази́н кура́лгэгу на́до.</ta>
            <ta e="T34" id="Seg_149" s="T32">Ӱдэгу́ охо́та! </ta>
            <ta e="T37" id="Seg_150" s="T34">Ӱтэ́джекогу пото́м на́до!</ta>
            <ta e="T49" id="Seg_151" s="T48">((…)).</ta>
         </annotation>
         <annotation name="stl" tierref="stl-YIF">
            <ta e="T11" id="Seg_152" s="T1">Liza Petrovna, mat čʼek kuralbak kioskan — kioskam elle ketemba.</ta>
            <ta e="T13" id="Seg_153" s="T11">Üdegu oxota!</ta>
            <ta e="T27" id="Seg_154" s="T24">V magazin kuralgegu nado.</ta>
            <ta e="T34" id="Seg_155" s="T32">Üdegu oxota! </ta>
            <ta e="T37" id="Seg_156" s="T34">Üteǯekogu potom nado!</ta>
            <ta e="T49" id="Seg_157" s="T48">((…)).</ta>
         </annotation>
         <annotation name="ts" tierref="ts-YIF">
            <ta e="T11" id="Seg_158" s="T1">(Liza/ Nadeʒda) Petrovna, mat čʼek kuralbak kioskan — kioskam elle ketemba. </ta>
            <ta e="T13" id="Seg_159" s="T11">Üdegu axota! </ta>
            <ta e="T27" id="Seg_160" s="T24">Maɣazin kuralgegu nado. </ta>
            <ta e="T34" id="Seg_161" s="T32">Üdegu oxota! </ta>
            <ta e="T37" id="Seg_162" s="T34">Üteǯekogu potom nado! </ta>
            <ta e="T49" id="Seg_163" s="T48">((…)). </ta>
         </annotation>
         <annotation name="mb" tierref="mb-YIF">
            <ta e="T2" id="Seg_164" s="T1">Liza</ta>
            <ta e="T3" id="Seg_165" s="T2">Nadeʒda</ta>
            <ta e="T4" id="Seg_166" s="T3">Petrovna</ta>
            <ta e="T5" id="Seg_167" s="T4">mat</ta>
            <ta e="T6" id="Seg_168" s="T5">čʼek</ta>
            <ta e="T7" id="Seg_169" s="T6">kur-a-l-ba-k</ta>
            <ta e="T8" id="Seg_170" s="T7">kiosk-a-n</ta>
            <ta e="T9" id="Seg_171" s="T8">kiosk-a-m</ta>
            <ta e="T10" id="Seg_172" s="T9">elle</ta>
            <ta e="T11" id="Seg_173" s="T10">kete-mba</ta>
            <ta e="T12" id="Seg_174" s="T11">üde-gu</ta>
            <ta e="T13" id="Seg_175" s="T12">axota</ta>
            <ta e="T25" id="Seg_176" s="T24">maɣazin</ta>
            <ta e="T26" id="Seg_177" s="T25">kur-a-l-ge-gu</ta>
            <ta e="T27" id="Seg_178" s="T26">nado</ta>
            <ta e="T33" id="Seg_179" s="T32">üde-gu</ta>
            <ta e="T34" id="Seg_180" s="T33">oxota</ta>
            <ta e="T35" id="Seg_181" s="T34">üte-ǯe-ko-gu</ta>
            <ta e="T36" id="Seg_182" s="T35">potom</ta>
            <ta e="T37" id="Seg_183" s="T36">nado</ta>
         </annotation>
         <annotation name="mp" tierref="mp-YIF">
            <ta e="T2" id="Seg_184" s="T1">Liza</ta>
            <ta e="T3" id="Seg_185" s="T2">Nadeʒda</ta>
            <ta e="T4" id="Seg_186" s="T3">Petrovna</ta>
            <ta e="T5" id="Seg_187" s="T4">man</ta>
            <ta e="T6" id="Seg_188" s="T5">ček</ta>
            <ta e="T7" id="Seg_189" s="T6">kur-ɨ-lɨ-mbɨ-k</ta>
            <ta e="T8" id="Seg_190" s="T7">kiosk-ɨ-n</ta>
            <ta e="T9" id="Seg_191" s="T8">kiosk-ɨ-m</ta>
            <ta e="T10" id="Seg_192" s="T9">illä</ta>
            <ta e="T11" id="Seg_193" s="T10">kɛttɛ-mbɨ</ta>
            <ta e="T12" id="Seg_194" s="T11">üdɨ-gu</ta>
            <ta e="T13" id="Seg_195" s="T12">axota</ta>
            <ta e="T25" id="Seg_196" s="T24">maɣazin</ta>
            <ta e="T26" id="Seg_197" s="T25">kur-ɨ-lɨ-ku-gu</ta>
            <ta e="T27" id="Seg_198" s="T26">naːda</ta>
            <ta e="T33" id="Seg_199" s="T32">üdɨ-gu</ta>
            <ta e="T34" id="Seg_200" s="T33">axota</ta>
            <ta e="T35" id="Seg_201" s="T34">üdɨ-nǯe-ku-gu</ta>
            <ta e="T36" id="Seg_202" s="T35">patom</ta>
            <ta e="T37" id="Seg_203" s="T36">naːda</ta>
         </annotation>
         <annotation name="ge" tierref="ge-YIF">
            <ta e="T2" id="Seg_204" s="T1">Liza.[NOM]</ta>
            <ta e="T3" id="Seg_205" s="T2">Nadezhda.[NOM]</ta>
            <ta e="T4" id="Seg_206" s="T3">Petrovna.[NOM]</ta>
            <ta e="T5" id="Seg_207" s="T4">I.NOM</ta>
            <ta e="T6" id="Seg_208" s="T5">fast</ta>
            <ta e="T7" id="Seg_209" s="T6">run-EP-RES-PST.NAR-1SG.S</ta>
            <ta e="T8" id="Seg_210" s="T7">kiosk-EP-ILL2</ta>
            <ta e="T9" id="Seg_211" s="T8">kiosk-EP-ACC</ta>
            <ta e="T10" id="Seg_212" s="T9">down</ta>
            <ta e="T11" id="Seg_213" s="T10">##push-PST.NAR.[3SG.S]</ta>
            <ta e="T12" id="Seg_214" s="T11">drink-INF</ta>
            <ta e="T13" id="Seg_215" s="T12">one.wants</ta>
            <ta e="T25" id="Seg_216" s="T24">shop</ta>
            <ta e="T26" id="Seg_217" s="T25">run-EP-RES-HAB-INF</ta>
            <ta e="T27" id="Seg_218" s="T26">one.should</ta>
            <ta e="T33" id="Seg_219" s="T32">drink-INF</ta>
            <ta e="T34" id="Seg_220" s="T33">one.wants</ta>
            <ta e="T35" id="Seg_221" s="T34">drink-IPFV3-HAB-INF</ta>
            <ta e="T36" id="Seg_222" s="T35">then</ta>
            <ta e="T37" id="Seg_223" s="T36">one.should</ta>
         </annotation>
         <annotation name="gr" tierref="gr-YIF">
            <ta e="T2" id="Seg_224" s="T1">Лиза.[NOM]</ta>
            <ta e="T3" id="Seg_225" s="T2">Надежда.[NOM]</ta>
            <ta e="T4" id="Seg_226" s="T3">Петровна.[NOM]</ta>
            <ta e="T5" id="Seg_227" s="T4">я.NOM</ta>
            <ta e="T6" id="Seg_228" s="T5">быстро</ta>
            <ta e="T7" id="Seg_229" s="T6">бегать-EP-RES-PST.NAR-1SG.S</ta>
            <ta e="T8" id="Seg_230" s="T7">киоск-EP-ILL2</ta>
            <ta e="T9" id="Seg_231" s="T8">киоск-EP-ACC</ta>
            <ta e="T10" id="Seg_232" s="T9">вниз</ta>
            <ta e="T11" id="Seg_233" s="T10">удариться-PST.NAR.[3SG.S]</ta>
            <ta e="T12" id="Seg_234" s="T11">пить-INF</ta>
            <ta e="T13" id="Seg_235" s="T12">хочется</ta>
            <ta e="T25" id="Seg_236" s="T24">магазин</ta>
            <ta e="T26" id="Seg_237" s="T25">бегать-EP-RES-HAB-INF</ta>
            <ta e="T27" id="Seg_238" s="T26">надо</ta>
            <ta e="T33" id="Seg_239" s="T32">пить-INF</ta>
            <ta e="T34" id="Seg_240" s="T33">хочется</ta>
            <ta e="T35" id="Seg_241" s="T34">пить-IPFV3-HAB-INF</ta>
            <ta e="T36" id="Seg_242" s="T35">потом</ta>
            <ta e="T37" id="Seg_243" s="T36">надо</ta>
         </annotation>
         <annotation name="mc" tierref="mc-YIF">
            <ta e="T2" id="Seg_244" s="T1">nprop.[n:case]</ta>
            <ta e="T3" id="Seg_245" s="T2">nprop.[n:case]</ta>
            <ta e="T4" id="Seg_246" s="T3">nprop.[n:case]</ta>
            <ta e="T5" id="Seg_247" s="T4">pers</ta>
            <ta e="T6" id="Seg_248" s="T5">adv</ta>
            <ta e="T7" id="Seg_249" s="T6">v-v:ins-v&gt;v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_250" s="T7">n-n:ins-n:case</ta>
            <ta e="T9" id="Seg_251" s="T8">n-n:ins-n:case</ta>
            <ta e="T10" id="Seg_252" s="T9">adv</ta>
            <ta e="T11" id="Seg_253" s="T10">v-v:tense.[v:pn]</ta>
            <ta e="T12" id="Seg_254" s="T11">v-v:inf</ta>
            <ta e="T13" id="Seg_255" s="T12">ptcl</ta>
            <ta e="T25" id="Seg_256" s="T24">n</ta>
            <ta e="T26" id="Seg_257" s="T25">v-n:ins-v&gt;v-v&gt;v-v:inf</ta>
            <ta e="T27" id="Seg_258" s="T26">ptcl</ta>
            <ta e="T33" id="Seg_259" s="T32">v-v:inf</ta>
            <ta e="T34" id="Seg_260" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_261" s="T34">v-v&gt;v-v&gt;v-v:inf</ta>
            <ta e="T36" id="Seg_262" s="T35">adv</ta>
            <ta e="T37" id="Seg_263" s="T36">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps-YIF">
            <ta e="T2" id="Seg_264" s="T1">nprop</ta>
            <ta e="T3" id="Seg_265" s="T2">nprop</ta>
            <ta e="T4" id="Seg_266" s="T3">nprop</ta>
            <ta e="T5" id="Seg_267" s="T4">pers</ta>
            <ta e="T6" id="Seg_268" s="T5">adv</ta>
            <ta e="T7" id="Seg_269" s="T6">v</ta>
            <ta e="T8" id="Seg_270" s="T7">n</ta>
            <ta e="T9" id="Seg_271" s="T8">n</ta>
            <ta e="T10" id="Seg_272" s="T9">adv</ta>
            <ta e="T11" id="Seg_273" s="T10">v</ta>
            <ta e="T12" id="Seg_274" s="T11">v</ta>
            <ta e="T13" id="Seg_275" s="T12">ptcl</ta>
            <ta e="T25" id="Seg_276" s="T24">n</ta>
            <ta e="T26" id="Seg_277" s="T25">v</ta>
            <ta e="T27" id="Seg_278" s="T26">ptcl</ta>
            <ta e="T33" id="Seg_279" s="T32">v</ta>
            <ta e="T34" id="Seg_280" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_281" s="T34">v</ta>
            <ta e="T36" id="Seg_282" s="T35">adv</ta>
            <ta e="T37" id="Seg_283" s="T36">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-YIF" />
         <annotation name="SyF" tierref="SyF-YIF" />
         <annotation name="IST" tierref="IST-YIF" />
         <annotation name="BOR" tierref="BOR-YIF">
            <ta e="T2" id="Seg_284" s="T1">RUS:cult</ta>
            <ta e="T3" id="Seg_285" s="T2">RUS:cult</ta>
            <ta e="T4" id="Seg_286" s="T3">RUS:cult</ta>
            <ta e="T8" id="Seg_287" s="T7">RUS:cult</ta>
            <ta e="T9" id="Seg_288" s="T8">RUS:cult</ta>
            <ta e="T13" id="Seg_289" s="T12">RUS:mod</ta>
            <ta e="T25" id="Seg_290" s="T24">RUS:cult</ta>
            <ta e="T27" id="Seg_291" s="T26">RUS:gram</ta>
            <ta e="T34" id="Seg_292" s="T33">RUS:mod</ta>
            <ta e="T36" id="Seg_293" s="T35">RUS:core</ta>
            <ta e="T37" id="Seg_294" s="T36">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-YIF" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-YIF" />
         <annotation name="CS" tierref="CS-YIF" />
         <annotation name="fe" tierref="fe-YIF">
            <ta e="T11" id="Seg_295" s="T1">(Liza/Nadezhda?) Petrovna, I went to the kiosk quickly – and the kiosk is closed already.</ta>
            <ta e="T13" id="Seg_296" s="T11">I want to have a drink!</ta>
            <ta e="T27" id="Seg_297" s="T24">We should go to the store.</ta>
            <ta e="T34" id="Seg_298" s="T32">I want a drink!</ta>
            <ta e="T37" id="Seg_299" s="T34">We need to have some drink later.</ta>
            <ta e="T49" id="Seg_300" s="T48">(…).</ta>
         </annotation>
         <annotation name="fg" tierref="fg-YIF">
            <ta e="T11" id="Seg_301" s="T1">(Liza/Nadezhda?) Petrovna, ich ging schnell zum Kiosk – und der Kiosk hat schon zu.</ta>
            <ta e="T13" id="Seg_302" s="T11">Ich will was trinken!</ta>
            <ta e="T27" id="Seg_303" s="T24">Wir sollten zum Laden gehen.</ta>
            <ta e="T34" id="Seg_304" s="T32">Ich will was trinken! </ta>
            <ta e="T37" id="Seg_305" s="T34">Wir müssen später etwas zu trinken haben.</ta>
            <ta e="T49" id="Seg_306" s="T48">(…).</ta>
         </annotation>
         <annotation name="fr" tierref="fr-YIF">
            <ta e="T11" id="Seg_307" s="T1">(Лиза/Надежда?) Петровна, я быстро сбегала в киоск — киоск уже закрыт.</ta>
            <ta e="T13" id="Seg_308" s="T11">Выпить охота!</ta>
            <ta e="T27" id="Seg_309" s="T24">В магазин сбегать надо.</ta>
            <ta e="T34" id="Seg_310" s="T32">Выпить охота! </ta>
            <ta e="T37" id="Seg_311" s="T34">Выпить немного потом надо!</ta>
            <ta e="T49" id="Seg_312" s="T48">(…).</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr-YIF">
            <ta e="T11" id="Seg_313" s="T1">Лиза Петровна, я быстро сбегала в киоск — киоск уже закрыт.</ta>
            <ta e="T13" id="Seg_314" s="T11">Выпить охота!</ta>
            <ta e="T27" id="Seg_315" s="T24">В магазин сбегать надо.</ta>
            <ta e="T34" id="Seg_316" s="T32">Выпить охота! </ta>
            <ta e="T37" id="Seg_317" s="T34">Выпить немного потом надо!</ta>
            <ta e="T49" id="Seg_318" s="T48">(…).</ta>
         </annotation>
         <annotation name="nt" tierref="nt-YIF">
            <ta e="T11" id="Seg_319" s="T1">[AAV:] ketembadəta?</ta>
            <ta e="T37" id="Seg_320" s="T34">[AAV:] Üteǯekogu &gt; No raboteǯekogu ?</ta>
         </annotation>
         <annotation name="nto" tierref="nto-YIF" />
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-NP"
                      id="tx-NP"
                      speaker="NP"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-NP">
            <ts e="T23" id="Seg_321" n="sc" s="T14">
               <ts e="T21" id="Seg_323" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_325" n="HIAT:w" s="T14">Meka</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_328" n="HIAT:w" s="T15">naj</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_331" n="HIAT:w" s="T16">oxota</ts>
                  <nts id="Seg_332" n="HIAT:ip">,</nts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_335" n="HIAT:w" s="T17">no</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_338" n="HIAT:w" s="T18">taw</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_340" n="HIAT:ip">(</nts>
                  <ts e="T20" id="Seg_342" n="HIAT:w" s="T19">kaj=</ts>
                  <nts id="Seg_343" n="HIAT:ip">)</nts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_346" n="HIAT:w" s="T20">katolenǯe</ts>
                  <nts id="Seg_347" n="HIAT:ip">?</nts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_350" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_352" n="HIAT:w" s="T21">Načit</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_355" n="HIAT:w" s="T22">kulaj</ts>
                  <nts id="Seg_356" n="HIAT:ip">.</nts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T31" id="Seg_358" n="sc" s="T28">
               <ts e="T31" id="Seg_360" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_362" n="HIAT:w" s="T28">A</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_365" n="HIAT:w" s="T29">wes</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_368" n="HIAT:w" s="T30">ketemba</ts>
                  <nts id="Seg_369" n="HIAT:ip">.</nts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T39" id="Seg_371" n="sc" s="T38">
               <ts e="T39" id="Seg_373" n="HIAT:u" s="T38">
                  <nts id="Seg_374" n="HIAT:ip">(</nts>
                  <nts id="Seg_375" n="HIAT:ip">(</nts>
                  <ats e="T39" id="Seg_376" n="HIAT:non-pho" s="T38">…</ats>
                  <nts id="Seg_377" n="HIAT:ip">)</nts>
                  <nts id="Seg_378" n="HIAT:ip">)</nts>
                  <nts id="Seg_379" n="HIAT:ip">.</nts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T47" id="Seg_381" n="sc" s="T40">
               <ts e="T47" id="Seg_383" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_385" n="HIAT:w" s="T40">Kuralgela</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_388" n="HIAT:w" s="T41">Tatjana</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_391" n="HIAT:w" s="T42">Semёnovnan</ts>
                  <nts id="Seg_392" n="HIAT:ip">,</nts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_395" n="HIAT:w" s="T43">tabnan</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_398" n="HIAT:w" s="T44">kajnaj</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_401" n="HIAT:w" s="T45">nʼetu</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_404" n="HIAT:w" s="T46">üteǯikogu</ts>
                  <nts id="Seg_405" n="HIAT:ip">.</nts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T61" id="Seg_407" n="sc" s="T50">
               <ts e="T61" id="Seg_409" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_411" n="HIAT:w" s="T50">A</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_414" n="HIAT:w" s="T51">tabnan</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_417" n="HIAT:w" s="T52">uʒe</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_420" n="HIAT:w" s="T53">davno</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_423" n="HIAT:w" s="T54">üteǯimbadet</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_425" n="HIAT:ip">(</nts>
                  <ts e="T56" id="Seg_427" n="HIAT:w" s="T55">i</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_430" n="HIAT:w" s="T56">wes</ts>
                  <nts id="Seg_431" n="HIAT:ip">)</nts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_434" n="HIAT:w" s="T57">pone</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_437" n="HIAT:w" s="T58">čačembadet</ts>
                  <nts id="Seg_438" n="HIAT:ip">,</nts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_440" n="HIAT:ip">(</nts>
                  <ts e="T60" id="Seg_442" n="HIAT:w" s="T59">kotte</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_445" n="HIAT:w" s="T60">wes</ts>
                  <nts id="Seg_446" n="HIAT:ip">)</nts>
                  <nts id="Seg_447" n="HIAT:ip">.</nts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-NP">
            <ts e="T23" id="Seg_449" n="sc" s="T14">
               <ts e="T15" id="Seg_451" n="e" s="T14">Meka </ts>
               <ts e="T16" id="Seg_453" n="e" s="T15">naj </ts>
               <ts e="T17" id="Seg_455" n="e" s="T16">oxota, </ts>
               <ts e="T18" id="Seg_457" n="e" s="T17">no </ts>
               <ts e="T19" id="Seg_459" n="e" s="T18">taw </ts>
               <ts e="T20" id="Seg_461" n="e" s="T19">(kaj=) </ts>
               <ts e="T21" id="Seg_463" n="e" s="T20">katolenǯe? </ts>
               <ts e="T22" id="Seg_465" n="e" s="T21">Načit </ts>
               <ts e="T23" id="Seg_467" n="e" s="T22">kulaj. </ts>
            </ts>
            <ts e="T31" id="Seg_468" n="sc" s="T28">
               <ts e="T29" id="Seg_470" n="e" s="T28">A </ts>
               <ts e="T30" id="Seg_472" n="e" s="T29">wes </ts>
               <ts e="T31" id="Seg_474" n="e" s="T30">ketemba. </ts>
            </ts>
            <ts e="T39" id="Seg_475" n="sc" s="T38">
               <ts e="T39" id="Seg_477" n="e" s="T38">((…)). </ts>
            </ts>
            <ts e="T47" id="Seg_478" n="sc" s="T40">
               <ts e="T41" id="Seg_480" n="e" s="T40">Kuralgela </ts>
               <ts e="T42" id="Seg_482" n="e" s="T41">Tatjana </ts>
               <ts e="T43" id="Seg_484" n="e" s="T42">Semёnovnan, </ts>
               <ts e="T44" id="Seg_486" n="e" s="T43">tabnan </ts>
               <ts e="T45" id="Seg_488" n="e" s="T44">kajnaj </ts>
               <ts e="T46" id="Seg_490" n="e" s="T45">nʼetu </ts>
               <ts e="T47" id="Seg_492" n="e" s="T46">üteǯikogu. </ts>
            </ts>
            <ts e="T61" id="Seg_493" n="sc" s="T50">
               <ts e="T51" id="Seg_495" n="e" s="T50">A </ts>
               <ts e="T52" id="Seg_497" n="e" s="T51">tabnan </ts>
               <ts e="T53" id="Seg_499" n="e" s="T52">uʒe </ts>
               <ts e="T54" id="Seg_501" n="e" s="T53">davno </ts>
               <ts e="T55" id="Seg_503" n="e" s="T54">üteǯimbadet </ts>
               <ts e="T56" id="Seg_505" n="e" s="T55">(i </ts>
               <ts e="T57" id="Seg_507" n="e" s="T56">wes) </ts>
               <ts e="T58" id="Seg_509" n="e" s="T57">pone </ts>
               <ts e="T59" id="Seg_511" n="e" s="T58">čačembadet, </ts>
               <ts e="T60" id="Seg_513" n="e" s="T59">(kotte </ts>
               <ts e="T61" id="Seg_515" n="e" s="T60">wes). </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-NP">
            <ta e="T21" id="Seg_516" s="T14">YIF_NP_196X_Drinking_conv.NP.001 (003)</ta>
            <ta e="T23" id="Seg_517" s="T21">YIF_NP_196X_Drinking_conv.NP.002 (004)</ta>
            <ta e="T31" id="Seg_518" s="T28">YIF_NP_196X_Drinking_conv.NP.003 (006)</ta>
            <ta e="T39" id="Seg_519" s="T38">YIF_NP_196X_Drinking_conv.NP.004 (008)</ta>
            <ta e="T47" id="Seg_520" s="T40">YIF_NP_196X_Drinking_conv.NP.005 (009)</ta>
            <ta e="T61" id="Seg_521" s="T50">YIF_NP_196X_Drinking_conv.NP.006 (011)</ta>
         </annotation>
         <annotation name="st" tierref="st-NP">
            <ta e="T21" id="Seg_522" s="T14">Ме́ка най охо́та, но тав кай… като́ленджэ?</ta>
            <ta e="T23" id="Seg_523" s="T21">Начи́т ку́лай.</ta>
            <ta e="T31" id="Seg_524" s="T28">А вес кэ́тэмба.</ta>
            <ta e="T39" id="Seg_525" s="T38">((…)).</ta>
            <ta e="T47" id="Seg_526" s="T40">Татья́нан Семёновнан кура́лгэгу, табна́н ка́йнай не́ту ӱтэ́джикогу.</ta>
            <ta e="T61" id="Seg_527" s="T50">Табна́н уже да́вно лагу́шкап ӱтэ́джимбадэт — по́нэ ҷаҷэмба́дэт.</ta>
         </annotation>
         <annotation name="stl" tierref="stl-NP">
            <ta e="T21" id="Seg_528" s="T14">Meka naj oxota, no tav kaj… katolenǯe?</ta>
            <ta e="T23" id="Seg_529" s="T21">Načʼit kulaj.</ta>
            <ta e="T31" id="Seg_530" s="T28">A ves ketemba.</ta>
            <ta e="T39" id="Seg_531" s="T38">((…)).</ta>
            <ta e="T47" id="Seg_532" s="T40">Tatjanan Semёnovnan kuralgegu, tabnan kajnaj netu üteǯikogu.</ta>
            <ta e="T61" id="Seg_533" s="T50">Tabnan uʒe davno laguškap üteǯimbadet — pone čačembadet.</ta>
         </annotation>
         <annotation name="ts" tierref="ts-NP">
            <ta e="T21" id="Seg_534" s="T14">Meka naj oxota, no taw kaj… katolenǯe? </ta>
            <ta e="T23" id="Seg_535" s="T21">Načit kulaj. </ta>
            <ta e="T31" id="Seg_536" s="T28">A wes ketemba. </ta>
            <ta e="T39" id="Seg_537" s="T38">((…)). </ta>
            <ta e="T47" id="Seg_538" s="T40">Kuralgela Tatjana Semёnovnan, tabnan kajnaj nʼetu üteǯikogu. </ta>
            <ta e="T61" id="Seg_539" s="T50">A tabnan uʒe davno üteǯimbadet (i wes) pone čačembadet, (kotte wes). </ta>
         </annotation>
         <annotation name="mb" tierref="mb-NP">
            <ta e="T15" id="Seg_540" s="T14">meka</ta>
            <ta e="T16" id="Seg_541" s="T15">naj</ta>
            <ta e="T17" id="Seg_542" s="T16">oxota</ta>
            <ta e="T18" id="Seg_543" s="T17">no</ta>
            <ta e="T19" id="Seg_544" s="T18">taw</ta>
            <ta e="T20" id="Seg_545" s="T19">kaj</ta>
            <ta e="T21" id="Seg_546" s="T20">katol-e-nǯe</ta>
            <ta e="T22" id="Seg_547" s="T21">nači-t</ta>
            <ta e="T23" id="Seg_548" s="T22">ku-la-j</ta>
            <ta e="T29" id="Seg_549" s="T28">a</ta>
            <ta e="T30" id="Seg_550" s="T29">wes</ta>
            <ta e="T31" id="Seg_551" s="T30">kete-mba</ta>
            <ta e="T41" id="Seg_552" s="T40">kur-a-l-ge-la</ta>
            <ta e="T42" id="Seg_553" s="T41">Tatjana</ta>
            <ta e="T43" id="Seg_554" s="T42">Semёnovna-n</ta>
            <ta e="T44" id="Seg_555" s="T43">tab-nan</ta>
            <ta e="T45" id="Seg_556" s="T44">kaj-naj</ta>
            <ta e="T46" id="Seg_557" s="T45">nʼetu</ta>
            <ta e="T47" id="Seg_558" s="T46">üte-ǯi-ko-gu</ta>
            <ta e="T51" id="Seg_559" s="T50">a</ta>
            <ta e="T52" id="Seg_560" s="T51">tab-nan</ta>
            <ta e="T53" id="Seg_561" s="T52">uʒe</ta>
            <ta e="T54" id="Seg_562" s="T53">davno</ta>
            <ta e="T55" id="Seg_563" s="T54">üte-ǯi-mba-det</ta>
            <ta e="T56" id="Seg_564" s="T55">i</ta>
            <ta e="T57" id="Seg_565" s="T56">wes</ta>
            <ta e="T58" id="Seg_566" s="T57">pone</ta>
            <ta e="T59" id="Seg_567" s="T58">čače-mba-det</ta>
            <ta e="T60" id="Seg_568" s="T59">kotte</ta>
            <ta e="T61" id="Seg_569" s="T60">wes</ta>
         </annotation>
         <annotation name="mp" tierref="mp-NP">
            <ta e="T15" id="Seg_570" s="T14">mäkkä</ta>
            <ta e="T16" id="Seg_571" s="T15">naj</ta>
            <ta e="T17" id="Seg_572" s="T16">axota</ta>
            <ta e="T18" id="Seg_573" s="T17">nu</ta>
            <ta e="T19" id="Seg_574" s="T18">taw</ta>
            <ta e="T20" id="Seg_575" s="T19">qaj</ta>
            <ta e="T21" id="Seg_576" s="T20">qadol-ɨ-nd</ta>
            <ta e="T22" id="Seg_577" s="T21">nača-n</ta>
            <ta e="T23" id="Seg_578" s="T22">qu-la-j</ta>
            <ta e="T29" id="Seg_579" s="T28">a</ta>
            <ta e="T30" id="Seg_580" s="T29">wesʼ</ta>
            <ta e="T31" id="Seg_581" s="T30">kɛttɛ-mbɨ</ta>
            <ta e="T41" id="Seg_582" s="T40">kur-ɨ-lɨ-ku-le</ta>
            <ta e="T42" id="Seg_583" s="T41">Tatjana</ta>
            <ta e="T43" id="Seg_584" s="T42">Semёnovna-n</ta>
            <ta e="T44" id="Seg_585" s="T43">tab-nan</ta>
            <ta e="T45" id="Seg_586" s="T44">qaj-naj</ta>
            <ta e="T46" id="Seg_587" s="T45">nʼetu</ta>
            <ta e="T47" id="Seg_588" s="T46">üdɨ-nǯe-ku-gu</ta>
            <ta e="T51" id="Seg_589" s="T50">a</ta>
            <ta e="T52" id="Seg_590" s="T51">tab-nan</ta>
            <ta e="T53" id="Seg_591" s="T52">uʒe</ta>
            <ta e="T54" id="Seg_592" s="T53">davno</ta>
            <ta e="T55" id="Seg_593" s="T54">üdɨ-nǯe-mbɨ-dət</ta>
            <ta e="T56" id="Seg_594" s="T55">i</ta>
            <ta e="T57" id="Seg_595" s="T56">wesʼ</ta>
            <ta e="T58" id="Seg_596" s="T57">poːne</ta>
            <ta e="T59" id="Seg_597" s="T58">čanǯe-mbɨ-dət</ta>
            <ta e="T60" id="Seg_598" s="T59">kotte</ta>
            <ta e="T61" id="Seg_599" s="T60">wesʼ</ta>
         </annotation>
         <annotation name="ge" tierref="ge-NP">
            <ta e="T15" id="Seg_600" s="T14">I.ALL</ta>
            <ta e="T16" id="Seg_601" s="T15">also</ta>
            <ta e="T17" id="Seg_602" s="T16">one.wants</ta>
            <ta e="T18" id="Seg_603" s="T17">well</ta>
            <ta e="T19" id="Seg_604" s="T18">this.[NOM]</ta>
            <ta e="T20" id="Seg_605" s="T19">what.[NOM]</ta>
            <ta e="T21" id="Seg_606" s="T20">scratch-EP-2SG.S</ta>
            <ta e="T22" id="Seg_607" s="T21">there-ADV.LOC</ta>
            <ta e="T23" id="Seg_608" s="T22">die-FUT-1DU</ta>
            <ta e="T29" id="Seg_609" s="T28">but</ta>
            <ta e="T30" id="Seg_610" s="T29">all</ta>
            <ta e="T31" id="Seg_611" s="T30">##push-PST.NAR.[3SG.S]</ta>
            <ta e="T41" id="Seg_612" s="T40">run-EP-RES-HAB-CVB</ta>
            <ta e="T42" id="Seg_613" s="T41">Tatyana.[NOM]</ta>
            <ta e="T43" id="Seg_614" s="T42">Semyonovna-ILL2</ta>
            <ta e="T44" id="Seg_615" s="T43">(s)he-ADES</ta>
            <ta e="T45" id="Seg_616" s="T44">what-EMPH</ta>
            <ta e="T46" id="Seg_617" s="T45">NEG.EX</ta>
            <ta e="T47" id="Seg_618" s="T46">drink-IPFV3-HAB-INF</ta>
            <ta e="T51" id="Seg_619" s="T50">but</ta>
            <ta e="T52" id="Seg_620" s="T51">(s)he-ADES</ta>
            <ta e="T53" id="Seg_621" s="T52">already</ta>
            <ta e="T54" id="Seg_622" s="T53">long.ago</ta>
            <ta e="T55" id="Seg_623" s="T54">drink-IPFV3-PST.NAR-3PL</ta>
            <ta e="T56" id="Seg_624" s="T55">and</ta>
            <ta e="T57" id="Seg_625" s="T56">all</ta>
            <ta e="T58" id="Seg_626" s="T57">outward(s)</ta>
            <ta e="T59" id="Seg_627" s="T58">go.out-PST.NAR-3PL</ta>
            <ta e="T60" id="Seg_628" s="T59">%%</ta>
            <ta e="T61" id="Seg_629" s="T60">all</ta>
         </annotation>
         <annotation name="gr" tierref="gr-NP">
            <ta e="T15" id="Seg_630" s="T14">я.ALL</ta>
            <ta e="T16" id="Seg_631" s="T15">тоже</ta>
            <ta e="T17" id="Seg_632" s="T16">хочется</ta>
            <ta e="T18" id="Seg_633" s="T17">ну</ta>
            <ta e="T19" id="Seg_634" s="T18">этот.[NOM]</ta>
            <ta e="T20" id="Seg_635" s="T19">что.[NOM]</ta>
            <ta e="T21" id="Seg_636" s="T20">царапать-EP-2SG.S</ta>
            <ta e="T22" id="Seg_637" s="T21">туда-ADV.LOC</ta>
            <ta e="T23" id="Seg_638" s="T22">умирать-FUT-1DU</ta>
            <ta e="T29" id="Seg_639" s="T28">а</ta>
            <ta e="T30" id="Seg_640" s="T29">весь</ta>
            <ta e="T31" id="Seg_641" s="T30">удариться-PST.NAR.[3SG.S]</ta>
            <ta e="T41" id="Seg_642" s="T40">бегать-EP-RES-HAB-CVB</ta>
            <ta e="T42" id="Seg_643" s="T41">Татьяна.[NOM]</ta>
            <ta e="T43" id="Seg_644" s="T42">Семёновна-ILL2</ta>
            <ta e="T44" id="Seg_645" s="T43">он(а)-ADES</ta>
            <ta e="T45" id="Seg_646" s="T44">что-EMPH</ta>
            <ta e="T46" id="Seg_647" s="T45">NEG.EX</ta>
            <ta e="T47" id="Seg_648" s="T46">пить-IPFV3-HAB-INF</ta>
            <ta e="T51" id="Seg_649" s="T50">а</ta>
            <ta e="T52" id="Seg_650" s="T51">он(а)-ADES</ta>
            <ta e="T53" id="Seg_651" s="T52">уже</ta>
            <ta e="T54" id="Seg_652" s="T53">давно</ta>
            <ta e="T55" id="Seg_653" s="T54">пить-IPFV3-PST.NAR-3PL</ta>
            <ta e="T56" id="Seg_654" s="T55">и</ta>
            <ta e="T57" id="Seg_655" s="T56">весь</ta>
            <ta e="T58" id="Seg_656" s="T57">наружу</ta>
            <ta e="T59" id="Seg_657" s="T58">выходить-PST.NAR-3PL</ta>
            <ta e="T60" id="Seg_658" s="T59">%%</ta>
            <ta e="T61" id="Seg_659" s="T60">весь</ta>
         </annotation>
         <annotation name="mc" tierref="mc-NP">
            <ta e="T15" id="Seg_660" s="T14">pers</ta>
            <ta e="T16" id="Seg_661" s="T15">ptcl</ta>
            <ta e="T17" id="Seg_662" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_663" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_664" s="T18">dem.[n:case]</ta>
            <ta e="T20" id="Seg_665" s="T19">interrog.[n:case]</ta>
            <ta e="T21" id="Seg_666" s="T20">v-n:ins-v:pn</ta>
            <ta e="T22" id="Seg_667" s="T21">adv-adv:case</ta>
            <ta e="T23" id="Seg_668" s="T22">v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_669" s="T28">conj</ta>
            <ta e="T30" id="Seg_670" s="T29">quant</ta>
            <ta e="T31" id="Seg_671" s="T30">v-v:tense.[v:pn]</ta>
            <ta e="T41" id="Seg_672" s="T40">v-n:ins-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T42" id="Seg_673" s="T41">nprop.[n:case]</ta>
            <ta e="T43" id="Seg_674" s="T42">nprop-n:case</ta>
            <ta e="T44" id="Seg_675" s="T43">pers-n:case</ta>
            <ta e="T45" id="Seg_676" s="T44">interrog-clit</ta>
            <ta e="T46" id="Seg_677" s="T45">ptcl</ta>
            <ta e="T47" id="Seg_678" s="T46">v-v&gt;v-v&gt;v-v:inf</ta>
            <ta e="T51" id="Seg_679" s="T50">conj</ta>
            <ta e="T52" id="Seg_680" s="T51">pers-n:case</ta>
            <ta e="T53" id="Seg_681" s="T52">adv</ta>
            <ta e="T54" id="Seg_682" s="T53">adv</ta>
            <ta e="T55" id="Seg_683" s="T54">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T56" id="Seg_684" s="T55">conj</ta>
            <ta e="T57" id="Seg_685" s="T56">quant</ta>
            <ta e="T58" id="Seg_686" s="T57">adv</ta>
            <ta e="T59" id="Seg_687" s="T58">v-v:tense-v:pn</ta>
            <ta e="T60" id="Seg_688" s="T59">%%</ta>
            <ta e="T61" id="Seg_689" s="T60">quant</ta>
         </annotation>
         <annotation name="ps" tierref="ps-NP">
            <ta e="T15" id="Seg_690" s="T14">pers</ta>
            <ta e="T16" id="Seg_691" s="T15">ptcl</ta>
            <ta e="T17" id="Seg_692" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_693" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_694" s="T18">dem</ta>
            <ta e="T20" id="Seg_695" s="T19">interrog</ta>
            <ta e="T21" id="Seg_696" s="T20">v</ta>
            <ta e="T22" id="Seg_697" s="T21">adv</ta>
            <ta e="T23" id="Seg_698" s="T22">v</ta>
            <ta e="T29" id="Seg_699" s="T28">conj</ta>
            <ta e="T30" id="Seg_700" s="T29">quant</ta>
            <ta e="T31" id="Seg_701" s="T30">v</ta>
            <ta e="T41" id="Seg_702" s="T40">v</ta>
            <ta e="T42" id="Seg_703" s="T41">nprop</ta>
            <ta e="T43" id="Seg_704" s="T42">nprop</ta>
            <ta e="T44" id="Seg_705" s="T43">pers</ta>
            <ta e="T45" id="Seg_706" s="T44">interrog</ta>
            <ta e="T46" id="Seg_707" s="T45">ptcl</ta>
            <ta e="T47" id="Seg_708" s="T46">v</ta>
            <ta e="T51" id="Seg_709" s="T50">conj</ta>
            <ta e="T52" id="Seg_710" s="T51">pers</ta>
            <ta e="T53" id="Seg_711" s="T52">v</ta>
            <ta e="T54" id="Seg_712" s="T53">adv</ta>
            <ta e="T55" id="Seg_713" s="T54">v</ta>
            <ta e="T56" id="Seg_714" s="T55">conj</ta>
            <ta e="T57" id="Seg_715" s="T56">quant</ta>
            <ta e="T58" id="Seg_716" s="T57">adv</ta>
            <ta e="T59" id="Seg_717" s="T58">v</ta>
            <ta e="T60" id="Seg_718" s="T59">%%</ta>
            <ta e="T61" id="Seg_719" s="T60">quant</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-NP" />
         <annotation name="SyF" tierref="SyF-NP" />
         <annotation name="IST" tierref="IST-NP" />
         <annotation name="BOR" tierref="BOR-NP">
            <ta e="T17" id="Seg_720" s="T16">RUS:mod</ta>
            <ta e="T18" id="Seg_721" s="T17">RUS:disc</ta>
            <ta e="T29" id="Seg_722" s="T28">RUS:gram</ta>
            <ta e="T30" id="Seg_723" s="T29">RUS:core</ta>
            <ta e="T42" id="Seg_724" s="T41">RUS:cult</ta>
            <ta e="T43" id="Seg_725" s="T42">RUS:cult</ta>
            <ta e="T45" id="Seg_726" s="T44">-</ta>
            <ta e="T51" id="Seg_727" s="T50">RUS:gram</ta>
            <ta e="T53" id="Seg_728" s="T52">RUS:gram</ta>
            <ta e="T54" id="Seg_729" s="T53">RUS:core</ta>
            <ta e="T56" id="Seg_730" s="T55">RUS:gram</ta>
            <ta e="T57" id="Seg_731" s="T56">RUS:core</ta>
            <ta e="T61" id="Seg_732" s="T60">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-NP" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-NP" />
         <annotation name="CS" tierref="CS-NP" />
         <annotation name="fe" tierref="fe-NP">
            <ta e="T21" id="Seg_733" s="T14">I want to have one, too, but what… to do?</ta>
            <ta e="T23" id="Seg_734" s="T21">We’ll die.</ta>
            <ta e="T31" id="Seg_735" s="T28">Everything is closed.</ta>
            <ta e="T39" id="Seg_736" s="T38">(…).</ta>
            <ta e="T47" id="Seg_737" s="T40">We need to go to Tatiana Semenova, she doesn’t have anything to drink.</ta>
            <ta e="T61" id="Seg_738" s="T50">Her barrel was emptied a long time ago – it’s been taken out to the street.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-NP">
            <ta e="T21" id="Seg_739" s="T14">Ich will auch einen trinken, aber was… tun?</ta>
            <ta e="T23" id="Seg_740" s="T21">Wir werden eingehen.</ta>
            <ta e="T31" id="Seg_741" s="T28">Es hat alles zu.</ta>
            <ta e="T39" id="Seg_742" s="T38">(…).</ta>
            <ta e="T47" id="Seg_743" s="T40">Wir müssen zu Tatiana Semenova gehen, sie hat nichts zu trinken.</ta>
            <ta e="T61" id="Seg_744" s="T50">Ihr Fass wurde vor langer Zeit geleert – es ist hinaus zur Straße gebracht worden.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-NP">
            <ta e="T21" id="Seg_745" s="T14">Мне тоже охота, но это что… поделаешь?</ta>
            <ta e="T23" id="Seg_746" s="T21">Там помрём.</ta>
            <ta e="T31" id="Seg_747" s="T28">А всё закрыто.</ta>
            <ta e="T39" id="Seg_748" s="T38">(…).</ta>
            <ta e="T47" id="Seg_749" s="T40">К Татьяне Семёновне сбегать, у неё ничего нет выпить.</ta>
            <ta e="T61" id="Seg_750" s="T50">У неё уже давно лагушок выпили — на улицу вынесли.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr-NP">
            <ta e="T21" id="Seg_751" s="T14">Мне тоже охота, но это что… поделаешь?</ta>
            <ta e="T23" id="Seg_752" s="T21">Там помрём.</ta>
            <ta e="T31" id="Seg_753" s="T28">А всё закрыто.</ta>
            <ta e="T39" id="Seg_754" s="T38">(…).</ta>
            <ta e="T47" id="Seg_755" s="T40">К Татьяне Семёновне сбегать, у неё ничего нет выпить.</ta>
            <ta e="T61" id="Seg_756" s="T50">У неё уже давно лагушок выпили — на улицу вынесли.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-NP">
            <ta e="T21" id="Seg_757" s="T14">[AAV:] no tav kaj &gt; taba xajs… ? katolenǯe &gt; katbolenǯe teper ?</ta>
            <ta e="T23" id="Seg_758" s="T21">[AAV:] Načʼit &gt; Kučʼit ?</ta>
            <ta e="T47" id="Seg_759" s="T40">[AAV:] tabnan &gt; pej tabnan? üteǯikogu &gt; üteǯikutə ?</ta>
         </annotation>
         <annotation name="nto" tierref="nto-NP" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-YIF"
                          name="ref"
                          segmented-tier-id="tx-YIF"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-YIF"
                          name="st"
                          segmented-tier-id="tx-YIF"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl-YIF"
                          name="stl"
                          segmented-tier-id="tx-YIF"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-YIF"
                          name="ts"
                          segmented-tier-id="tx-YIF"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-YIF"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-YIF"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-YIF"
                          name="mb"
                          segmented-tier-id="tx-YIF"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-YIF"
                          name="mp"
                          segmented-tier-id="tx-YIF"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-YIF"
                          name="ge"
                          segmented-tier-id="tx-YIF"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-YIF"
                          name="gr"
                          segmented-tier-id="tx-YIF"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-YIF"
                          name="mc"
                          segmented-tier-id="tx-YIF"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-YIF"
                          name="ps"
                          segmented-tier-id="tx-YIF"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-YIF"
                          name="SeR"
                          segmented-tier-id="tx-YIF"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-YIF"
                          name="SyF"
                          segmented-tier-id="tx-YIF"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-YIF"
                          name="IST"
                          segmented-tier-id="tx-YIF"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-YIF"
                          name="BOR"
                          segmented-tier-id="tx-YIF"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-YIF"
                          name="BOR-Phon"
                          segmented-tier-id="tx-YIF"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-YIF"
                          name="BOR-Morph"
                          segmented-tier-id="tx-YIF"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-YIF"
                          name="CS"
                          segmented-tier-id="tx-YIF"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-YIF"
                          name="fe"
                          segmented-tier-id="tx-YIF"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-YIF"
                          name="fg"
                          segmented-tier-id="tx-YIF"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-YIF"
                          name="fr"
                          segmented-tier-id="tx-YIF"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-YIF"
                          name="ltr"
                          segmented-tier-id="tx-YIF"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-YIF"
                          name="nt"
                          segmented-tier-id="tx-YIF"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto-YIF"
                          name="nto"
                          segmented-tier-id="tx-YIF"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-NP"
                          name="ref"
                          segmented-tier-id="tx-NP"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-NP"
                          name="st"
                          segmented-tier-id="tx-NP"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl-NP"
                          name="stl"
                          segmented-tier-id="tx-NP"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-NP"
                          name="ts"
                          segmented-tier-id="tx-NP"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-NP"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-NP"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-NP"
                          name="mb"
                          segmented-tier-id="tx-NP"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-NP"
                          name="mp"
                          segmented-tier-id="tx-NP"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-NP"
                          name="ge"
                          segmented-tier-id="tx-NP"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-NP"
                          name="gr"
                          segmented-tier-id="tx-NP"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-NP"
                          name="mc"
                          segmented-tier-id="tx-NP"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-NP"
                          name="ps"
                          segmented-tier-id="tx-NP"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-NP"
                          name="SeR"
                          segmented-tier-id="tx-NP"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-NP"
                          name="SyF"
                          segmented-tier-id="tx-NP"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-NP"
                          name="IST"
                          segmented-tier-id="tx-NP"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-NP"
                          name="BOR"
                          segmented-tier-id="tx-NP"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-NP"
                          name="BOR-Phon"
                          segmented-tier-id="tx-NP"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-NP"
                          name="BOR-Morph"
                          segmented-tier-id="tx-NP"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-NP"
                          name="CS"
                          segmented-tier-id="tx-NP"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-NP"
                          name="fe"
                          segmented-tier-id="tx-NP"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-NP"
                          name="fg"
                          segmented-tier-id="tx-NP"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-NP"
                          name="fr"
                          segmented-tier-id="tx-NP"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-NP"
                          name="ltr"
                          segmented-tier-id="tx-NP"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-NP"
                          name="nt"
                          segmented-tier-id="tx-NP"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto-NP"
                          name="nto"
                          segmented-tier-id="tx-NP"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
