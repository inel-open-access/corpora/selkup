<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>AGS_1964_SnakeInMouth_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">AGS_1964_SnakeInMouth_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">126</ud-information>
            <ud-information attribute-name="# HIAT:w">100</ud-information>
            <ud-information attribute-name="# e">100</ud-information>
            <ud-information attribute-name="# HIAT:u">18</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="AGS">
            <abbreviation>AGS</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="AGS"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T101" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">oqqɨrɨŋ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">matʼtʼöɣɨn</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">qum</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">qonʼdinba</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">oqqɨrɨŋ</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">ündadit</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_24" n="HIAT:ip">(</nts>
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">qonǯurnam</ts>
                  <nts id="Seg_27" n="HIAT:ip">)</nts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_30" n="HIAT:w" s="T8">küdärʼem</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_33" n="HIAT:w" s="T9">qajda</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_36" n="HIAT:w" s="T10">lʼenim</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_39" n="HIAT:w" s="T11">amlʼe</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_42" n="HIAT:w" s="T12">taːdəram</ts>
                  <nts id="Seg_43" n="HIAT:ip">.</nts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_46" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_48" n="HIAT:w" s="T13">na</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_51" n="HIAT:w" s="T14">resitizaŋ</ts>
                  <nts id="Seg_52" n="HIAT:ip">.</nts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_55" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_57" n="HIAT:w" s="T15">oqqɨrɨŋ</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_60" n="HIAT:w" s="T16">ündɨdʼäm</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_63" n="HIAT:w" s="T17">paja</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_66" n="HIAT:w" s="T18">siːtälʼe</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_69" n="HIAT:w" s="T19">taːdärɨŋ</ts>
                  <nts id="Seg_70" n="HIAT:ip">.</nts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_73" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_75" n="HIAT:w" s="T20">tʼärɨŋ</ts>
                  <nts id="Seg_76" n="HIAT:ip">:</nts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">qajda</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_82" n="HIAT:w" s="T22">tan</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_85" n="HIAT:w" s="T23">aŋɨnända</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_88" n="HIAT:w" s="T24">qwässa</ts>
                  <nts id="Seg_89" n="HIAT:ip">,</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_92" n="HIAT:w" s="T25">i</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_95" n="HIAT:w" s="T26">täbanni</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_98" n="HIAT:w" s="T27">tʼärɨŋ</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_101" n="HIAT:w" s="T28">na</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_104" n="HIAT:w" s="T29">qunni</ts>
                  <nts id="Seg_105" n="HIAT:ip">.</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_108" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_110" n="HIAT:w" s="T30">na</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_113" n="HIAT:w" s="T31">qum</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_116" n="HIAT:w" s="T32">tüwuŋ</ts>
                  <nts id="Seg_117" n="HIAT:ip">.</nts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_120" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_122" n="HIAT:w" s="T33">oqqɨrɨŋ</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_125" n="HIAT:w" s="T34">qwanba</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_128" n="HIAT:w" s="T35">balʼnʼicaːndə</ts>
                  <nts id="Seg_129" n="HIAT:ip">.</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_132" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_134" n="HIAT:w" s="T36">tʼärɨmba</ts>
                  <nts id="Seg_135" n="HIAT:ip">:</nts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_138" n="HIAT:w" s="T37">qajda</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_141" n="HIAT:w" s="T38">sündem</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_144" n="HIAT:w" s="T39">porɨmba</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_147" n="HIAT:w" s="T40">tüːntare</ts>
                  <nts id="Seg_148" n="HIAT:ip">.</nts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_151" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_153" n="HIAT:w" s="T41">i</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_156" n="HIAT:w" s="T42">tʼärɨmba</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_159" n="HIAT:w" s="T43">qajda</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_162" n="HIAT:w" s="T44">profesorane</ts>
                  <nts id="Seg_163" n="HIAT:ip">:</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_166" n="HIAT:w" s="T45">man</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_169" n="HIAT:w" s="T46">pajam</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_172" n="HIAT:w" s="T47">qonǯɨrba</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_175" n="HIAT:w" s="T48">qajda</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_178" n="HIAT:w" s="T49">aŋanɨ</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_181" n="HIAT:w" s="T50">qwässa</ts>
                  <nts id="Seg_182" n="HIAT:ip">.</nts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_185" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_187" n="HIAT:w" s="T51">a</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_190" n="HIAT:w" s="T52">na</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_193" n="HIAT:w" s="T53">prafesar</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_196" n="HIAT:w" s="T54">wesʼ</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_199" n="HIAT:w" s="T55">pin</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_202" n="HIAT:w" s="T56">aːmdimba</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_205" n="HIAT:w" s="T57">täbɨnan</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_208" n="HIAT:w" s="T58">qäɣɨn</ts>
                  <nts id="Seg_209" n="HIAT:ip">.</nts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_212" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_214" n="HIAT:w" s="T59">qarʼiːmɨn</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_217" n="HIAT:w" s="T60">tättɨmdälǯɨ</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_220" n="HIAT:w" s="T61">časkɨn</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_223" n="HIAT:w" s="T62">čaːnǯɨŋ</ts>
                  <nts id="Seg_224" n="HIAT:ip">.</nts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_227" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_229" n="HIAT:w" s="T63">ina</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_232" n="HIAT:w" s="T64">quwam</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_235" n="HIAT:w" s="T65">siːdɨt</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_238" n="HIAT:w" s="T66">täp</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_241" n="HIAT:w" s="T67">pitä</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_244" n="HIAT:w" s="T68">qaːɣɨn</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_247" n="HIAT:w" s="T69">süː</ts>
                  <nts id="Seg_248" n="HIAT:ip">.</nts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_251" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_253" n="HIAT:w" s="T70">na</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_256" n="HIAT:w" s="T71">süː</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_259" n="HIAT:w" s="T72">ilakumba</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_262" n="HIAT:w" s="T73">na</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_265" n="HIAT:w" s="T74">quːnan</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_268" n="HIAT:w" s="T75">sündeɣɨn</ts>
                  <nts id="Seg_269" n="HIAT:ip">.</nts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_272" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_274" n="HIAT:w" s="T76">patpikumba</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_277" n="HIAT:w" s="T77">aŋɨɣɨn</ts>
                  <nts id="Seg_278" n="HIAT:ip">.</nts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_281" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_283" n="HIAT:w" s="T78">nännɨ</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_286" n="HIAT:w" s="T79">na</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_289" n="HIAT:w" s="T80">qum</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_292" n="HIAT:w" s="T81">soːj</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_295" n="HIAT:w" s="T82">äːsuzan</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_297" n="HIAT:ip">(</nts>
                  <ts e="T84" id="Seg_299" n="HIAT:w" s="T83">äːsukuzan</ts>
                  <nts id="Seg_300" n="HIAT:ip">)</nts>
                  <nts id="Seg_301" n="HIAT:ip">.</nts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_304" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_306" n="HIAT:w" s="T84">na</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_309" n="HIAT:w" s="T85">prafesɨr</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_312" n="HIAT:w" s="T86">assɨ</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_315" n="HIAT:w" s="T87">mimbat</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_318" n="HIAT:w" s="T88">na</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_321" n="HIAT:w" s="T89">qunni</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_324" n="HIAT:w" s="T90">qugu</ts>
                  <nts id="Seg_325" n="HIAT:ip">.</nts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_328" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_330" n="HIAT:w" s="T91">na</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_333" n="HIAT:w" s="T92">qum</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_336" n="HIAT:w" s="T93">tidam</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_339" n="HIAT:w" s="T94">na</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_342" n="HIAT:w" s="T95">ilaqquŋ</ts>
                  <nts id="Seg_343" n="HIAT:ip">.</nts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_346" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_348" n="HIAT:w" s="T96">na</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_351" n="HIAT:w" s="T97">äːssan</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_354" n="HIAT:w" s="T98">qundoqɨn</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_357" n="HIAT:w" s="T99">i</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_360" n="HIAT:w" s="T100">sitʼeptəmbiqumbattə</ts>
                  <nts id="Seg_361" n="HIAT:ip">.</nts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T101" id="Seg_363" n="sc" s="T1">
               <ts e="T2" id="Seg_365" n="e" s="T1">oqqɨrɨŋ </ts>
               <ts e="T3" id="Seg_367" n="e" s="T2">matʼtʼöɣɨn </ts>
               <ts e="T4" id="Seg_369" n="e" s="T3">qum </ts>
               <ts e="T5" id="Seg_371" n="e" s="T4">qonʼdinba. </ts>
               <ts e="T6" id="Seg_373" n="e" s="T5">oqqɨrɨŋ </ts>
               <ts e="T7" id="Seg_375" n="e" s="T6">ündadit </ts>
               <ts e="T8" id="Seg_377" n="e" s="T7">(qonǯurnam) </ts>
               <ts e="T9" id="Seg_379" n="e" s="T8">küdärʼem </ts>
               <ts e="T10" id="Seg_381" n="e" s="T9">qajda </ts>
               <ts e="T11" id="Seg_383" n="e" s="T10">lʼenim </ts>
               <ts e="T12" id="Seg_385" n="e" s="T11">amlʼe </ts>
               <ts e="T13" id="Seg_387" n="e" s="T12">taːdəram. </ts>
               <ts e="T14" id="Seg_389" n="e" s="T13">na </ts>
               <ts e="T15" id="Seg_391" n="e" s="T14">resitizaŋ. </ts>
               <ts e="T16" id="Seg_393" n="e" s="T15">oqqɨrɨŋ </ts>
               <ts e="T17" id="Seg_395" n="e" s="T16">ündɨdʼäm </ts>
               <ts e="T18" id="Seg_397" n="e" s="T17">paja </ts>
               <ts e="T19" id="Seg_399" n="e" s="T18">siːtälʼe </ts>
               <ts e="T20" id="Seg_401" n="e" s="T19">taːdärɨŋ. </ts>
               <ts e="T21" id="Seg_403" n="e" s="T20">tʼärɨŋ: </ts>
               <ts e="T22" id="Seg_405" n="e" s="T21">qajda </ts>
               <ts e="T23" id="Seg_407" n="e" s="T22">tan </ts>
               <ts e="T24" id="Seg_409" n="e" s="T23">aŋɨnända </ts>
               <ts e="T25" id="Seg_411" n="e" s="T24">qwässa, </ts>
               <ts e="T26" id="Seg_413" n="e" s="T25">i </ts>
               <ts e="T27" id="Seg_415" n="e" s="T26">täbanni </ts>
               <ts e="T28" id="Seg_417" n="e" s="T27">tʼärɨŋ </ts>
               <ts e="T29" id="Seg_419" n="e" s="T28">na </ts>
               <ts e="T30" id="Seg_421" n="e" s="T29">qunni. </ts>
               <ts e="T31" id="Seg_423" n="e" s="T30">na </ts>
               <ts e="T32" id="Seg_425" n="e" s="T31">qum </ts>
               <ts e="T33" id="Seg_427" n="e" s="T32">tüwuŋ. </ts>
               <ts e="T34" id="Seg_429" n="e" s="T33">oqqɨrɨŋ </ts>
               <ts e="T35" id="Seg_431" n="e" s="T34">qwanba </ts>
               <ts e="T36" id="Seg_433" n="e" s="T35">balʼnʼicaːndə. </ts>
               <ts e="T37" id="Seg_435" n="e" s="T36">tʼärɨmba: </ts>
               <ts e="T38" id="Seg_437" n="e" s="T37">qajda </ts>
               <ts e="T39" id="Seg_439" n="e" s="T38">sündem </ts>
               <ts e="T40" id="Seg_441" n="e" s="T39">porɨmba </ts>
               <ts e="T41" id="Seg_443" n="e" s="T40">tüːntare. </ts>
               <ts e="T42" id="Seg_445" n="e" s="T41">i </ts>
               <ts e="T43" id="Seg_447" n="e" s="T42">tʼärɨmba </ts>
               <ts e="T44" id="Seg_449" n="e" s="T43">qajda </ts>
               <ts e="T45" id="Seg_451" n="e" s="T44">profesorane: </ts>
               <ts e="T46" id="Seg_453" n="e" s="T45">man </ts>
               <ts e="T47" id="Seg_455" n="e" s="T46">pajam </ts>
               <ts e="T48" id="Seg_457" n="e" s="T47">qonǯɨrba </ts>
               <ts e="T49" id="Seg_459" n="e" s="T48">qajda </ts>
               <ts e="T50" id="Seg_461" n="e" s="T49">aŋanɨ </ts>
               <ts e="T51" id="Seg_463" n="e" s="T50">qwässa. </ts>
               <ts e="T52" id="Seg_465" n="e" s="T51">a </ts>
               <ts e="T53" id="Seg_467" n="e" s="T52">na </ts>
               <ts e="T54" id="Seg_469" n="e" s="T53">prafesar </ts>
               <ts e="T55" id="Seg_471" n="e" s="T54">wesʼ </ts>
               <ts e="T56" id="Seg_473" n="e" s="T55">pin </ts>
               <ts e="T57" id="Seg_475" n="e" s="T56">aːmdimba </ts>
               <ts e="T58" id="Seg_477" n="e" s="T57">täbɨnan </ts>
               <ts e="T59" id="Seg_479" n="e" s="T58">qäɣɨn. </ts>
               <ts e="T60" id="Seg_481" n="e" s="T59">qarʼiːmɨn </ts>
               <ts e="T61" id="Seg_483" n="e" s="T60">tättɨmdälǯɨ </ts>
               <ts e="T62" id="Seg_485" n="e" s="T61">časkɨn </ts>
               <ts e="T63" id="Seg_487" n="e" s="T62">čaːnǯɨŋ. </ts>
               <ts e="T64" id="Seg_489" n="e" s="T63">ina </ts>
               <ts e="T65" id="Seg_491" n="e" s="T64">quwam </ts>
               <ts e="T66" id="Seg_493" n="e" s="T65">siːdɨt </ts>
               <ts e="T67" id="Seg_495" n="e" s="T66">täp </ts>
               <ts e="T68" id="Seg_497" n="e" s="T67">pitä </ts>
               <ts e="T69" id="Seg_499" n="e" s="T68">qaːɣɨn </ts>
               <ts e="T70" id="Seg_501" n="e" s="T69">süː. </ts>
               <ts e="T71" id="Seg_503" n="e" s="T70">na </ts>
               <ts e="T72" id="Seg_505" n="e" s="T71">süː </ts>
               <ts e="T73" id="Seg_507" n="e" s="T72">ilakumba </ts>
               <ts e="T74" id="Seg_509" n="e" s="T73">na </ts>
               <ts e="T75" id="Seg_511" n="e" s="T74">quːnan </ts>
               <ts e="T76" id="Seg_513" n="e" s="T75">sündeɣɨn. </ts>
               <ts e="T77" id="Seg_515" n="e" s="T76">patpikumba </ts>
               <ts e="T78" id="Seg_517" n="e" s="T77">aŋɨɣɨn. </ts>
               <ts e="T79" id="Seg_519" n="e" s="T78">nännɨ </ts>
               <ts e="T80" id="Seg_521" n="e" s="T79">na </ts>
               <ts e="T81" id="Seg_523" n="e" s="T80">qum </ts>
               <ts e="T82" id="Seg_525" n="e" s="T81">soːj </ts>
               <ts e="T83" id="Seg_527" n="e" s="T82">äːsuzan </ts>
               <ts e="T84" id="Seg_529" n="e" s="T83">(äːsukuzan). </ts>
               <ts e="T85" id="Seg_531" n="e" s="T84">na </ts>
               <ts e="T86" id="Seg_533" n="e" s="T85">prafesɨr </ts>
               <ts e="T87" id="Seg_535" n="e" s="T86">assɨ </ts>
               <ts e="T88" id="Seg_537" n="e" s="T87">mimbat </ts>
               <ts e="T89" id="Seg_539" n="e" s="T88">na </ts>
               <ts e="T90" id="Seg_541" n="e" s="T89">qunni </ts>
               <ts e="T91" id="Seg_543" n="e" s="T90">qugu. </ts>
               <ts e="T92" id="Seg_545" n="e" s="T91">na </ts>
               <ts e="T93" id="Seg_547" n="e" s="T92">qum </ts>
               <ts e="T94" id="Seg_549" n="e" s="T93">tidam </ts>
               <ts e="T95" id="Seg_551" n="e" s="T94">na </ts>
               <ts e="T96" id="Seg_553" n="e" s="T95">ilaqquŋ. </ts>
               <ts e="T97" id="Seg_555" n="e" s="T96">na </ts>
               <ts e="T98" id="Seg_557" n="e" s="T97">äːssan </ts>
               <ts e="T99" id="Seg_559" n="e" s="T98">qundoqɨn </ts>
               <ts e="T100" id="Seg_561" n="e" s="T99">i </ts>
               <ts e="T101" id="Seg_563" n="e" s="T100">sitʼeptəmbiqumbattə. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_564" s="T1">AGS_1964_SnakeInMouth_flk.001 (001.001)</ta>
            <ta e="T13" id="Seg_565" s="T5">AGS_1964_SnakeInMouth_flk.002 (001.002)</ta>
            <ta e="T15" id="Seg_566" s="T13">AGS_1964_SnakeInMouth_flk.003 (001.003)</ta>
            <ta e="T20" id="Seg_567" s="T15">AGS_1964_SnakeInMouth_flk.004 (001.004)</ta>
            <ta e="T30" id="Seg_568" s="T20">AGS_1964_SnakeInMouth_flk.005 (001.005)</ta>
            <ta e="T33" id="Seg_569" s="T30">AGS_1964_SnakeInMouth_flk.006 (001.006)</ta>
            <ta e="T36" id="Seg_570" s="T33">AGS_1964_SnakeInMouth_flk.007 (001.007)</ta>
            <ta e="T41" id="Seg_571" s="T36">AGS_1964_SnakeInMouth_flk.008 (001.008)</ta>
            <ta e="T51" id="Seg_572" s="T41">AGS_1964_SnakeInMouth_flk.009 (001.009)</ta>
            <ta e="T59" id="Seg_573" s="T51">AGS_1964_SnakeInMouth_flk.010 (001.010)</ta>
            <ta e="T63" id="Seg_574" s="T59">AGS_1964_SnakeInMouth_flk.011 (001.011)</ta>
            <ta e="T70" id="Seg_575" s="T63">AGS_1964_SnakeInMouth_flk.012 (001.012)</ta>
            <ta e="T76" id="Seg_576" s="T70">AGS_1964_SnakeInMouth_flk.013 (001.013)</ta>
            <ta e="T78" id="Seg_577" s="T76">AGS_1964_SnakeInMouth_flk.014 (001.014)</ta>
            <ta e="T84" id="Seg_578" s="T78">AGS_1964_SnakeInMouth_flk.015 (001.015)</ta>
            <ta e="T91" id="Seg_579" s="T84">AGS_1964_SnakeInMouth_flk.016 (001.016)</ta>
            <ta e="T96" id="Seg_580" s="T91">AGS_1964_SnakeInMouth_flk.017 (001.017)</ta>
            <ta e="T101" id="Seg_581" s="T96">AGS_1964_SnakeInMouth_flk.018 (001.018)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_582" s="T1">′оkkырың ма′тʼтʼӧɣын kум ′kонʼдинба.</ta>
            <ta e="T13" id="Seg_583" s="T5">′оkkырың ‵ӱнда′дит (kонджур′нам) ′кӱдӓрʼем kай′да ′лʼеним ам′лʼе ′та̄дърам.</ta>
            <ta e="T15" id="Seg_584" s="T13">на ре′ситизаң.</ta>
            <ta e="T20" id="Seg_585" s="T15">′оkkырың ӱнды′дʼӓм па′jа сӣт(т)ӓ′лʼе ′та̄дӓрың.</ta>
            <ta e="T30" id="Seg_586" s="T20">тʼӓ′рың: kай′да тан аңы′нӓнда(ы) ′kwӓсса, и тӓ′банни ′тʼӓрың на ′kунни.</ta>
            <ta e="T33" id="Seg_587" s="T30">на ′kум ′тӱвуң.</ta>
            <ta e="T36" id="Seg_588" s="T33">оkkы′рың kwан′ба балʼ′нʼица ′андъ.</ta>
            <ta e="T41" id="Seg_589" s="T36">тʼӓрым′ба: kай′да сӱн′дем ′порымба ′тӱ̄нта′ре.</ta>
            <ta e="T51" id="Seg_590" s="T41">и тʼӓрымба kай′да про′фесоране: ман па′jам kонджыр′ба kай′да ′аңана(ы)н ′kwӓсса.</ta>
            <ta e="T59" id="Seg_591" s="T51">а на пра′фесар весʼ ′пин ′а̄мдим‵ба тӓбы′нан ′kӓɣын.</ta>
            <ta e="T63" id="Seg_592" s="T59">kа′рʼӣмын ′тӓттым‵дӓлджы ′часкын ′тша̄нджың.</ta>
            <ta e="T70" id="Seg_593" s="T63">′ина ′kувам ′сӣды(а)т тӓп пи′тӓ ′kа̄ɣын сӱ̄.</ta>
            <ta e="T76" id="Seg_594" s="T70">на сӱ̄ ′илакумба на ′kӯнан сӱн′де(ӓ)ɣын.</ta>
            <ta e="T78" id="Seg_595" s="T76">′патпикумба ′аңа(ы)ɣын.</ta>
            <ta e="T84" id="Seg_596" s="T78">′нӓнны на kум ′со̄й ӓ̄су′зан (′ӓ̄сукунз̂ан).</ta>
            <ta e="T91" id="Seg_597" s="T84">на пра′фесыр ′ассы мим′бат на ′kунни ′kугу.</ta>
            <ta e="T96" id="Seg_598" s="T91">на kум ти′дам на и′ла′kkуң.</ta>
            <ta e="T101" id="Seg_599" s="T96">на ′ӓ̄ссан kун′доkы(а)н и си′тʼептъмби kум′баттъ.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_600" s="T1">oqqɨrɨŋ matʼtʼöɣɨn qum qonʼdinba.</ta>
            <ta e="T13" id="Seg_601" s="T5">oqqɨrɨŋ ündadit (qonǯurnam) küdärʼem qajda lʼenim amlʼe taːdəram.</ta>
            <ta e="T15" id="Seg_602" s="T13">na resitizaŋ.</ta>
            <ta e="T20" id="Seg_603" s="T15">oqqɨrɨŋ ündɨdʼäm paja siːt(t)älʼe taːdärɨŋ.</ta>
            <ta e="T30" id="Seg_604" s="T20">tʼärɨŋ: qajda tan aŋɨnända(ɨ) qwässa, i täbanni tʼärɨŋ na qunni.</ta>
            <ta e="T33" id="Seg_605" s="T30">na qum tüvuŋ.</ta>
            <ta e="T36" id="Seg_606" s="T33">oqqɨrɨŋ qwanba balʼnʼica andə.</ta>
            <ta e="T41" id="Seg_607" s="T36">tʼärɨmba: qajda sündem porɨmba tüːntare.</ta>
            <ta e="T51" id="Seg_608" s="T41">i tʼärɨmba qajda profesorane: man pajam qonǯɨrba qajda aŋana(ɨ)n qwässa.</ta>
            <ta e="T59" id="Seg_609" s="T51">a na prafesar vesʼ pin aːmdimba täbɨnan qäɣɨn.</ta>
            <ta e="T63" id="Seg_610" s="T59">qarʼiːmɨn tättɨmdälǯɨ časkɨn tšaːnǯɨŋ.</ta>
            <ta e="T70" id="Seg_611" s="T63">ina quvam siːdɨ(a)t täp pitä qaːɣɨn süː.</ta>
            <ta e="T76" id="Seg_612" s="T70">na süː ilakumba na quːnan sünde(ä)ɣɨn.</ta>
            <ta e="T78" id="Seg_613" s="T76">patpikumba aŋa(ɨ)ɣɨn.</ta>
            <ta e="T84" id="Seg_614" s="T78">nännɨ na qum soːj äːsuzan (äːsukunẑan).</ta>
            <ta e="T91" id="Seg_615" s="T84">na prafesɨr assɨ mimbat na qunni qugu.</ta>
            <ta e="T96" id="Seg_616" s="T91">na qum tidam na ilaqquŋ.</ta>
            <ta e="T101" id="Seg_617" s="T96">na äːssan qundoqɨ(a)n i sitʼeptəmbi qumbattə.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_618" s="T1">oqqɨrɨŋ matʼtʼöɣɨn qum qonʼdinba. </ta>
            <ta e="T13" id="Seg_619" s="T5">oqqɨrɨŋ ündadit (qonǯurnam) küdärʼem qajda lʼenim amlʼe taːdəram. </ta>
            <ta e="T15" id="Seg_620" s="T13">na resitizaŋ. </ta>
            <ta e="T20" id="Seg_621" s="T15">oqqɨrɨŋ ündɨdʼäm paja siːtälʼe taːdärɨŋ. </ta>
            <ta e="T30" id="Seg_622" s="T20">tʼärɨŋ: qajda tan aŋɨnända qwässa, i täbanni tʼärɨŋ na qunni. </ta>
            <ta e="T33" id="Seg_623" s="T30">na qum tüwuŋ. </ta>
            <ta e="T36" id="Seg_624" s="T33">oqqɨrɨŋ qwanba balʼnʼicaːndə. </ta>
            <ta e="T41" id="Seg_625" s="T36">tʼärɨmba: qajda sündem porɨmba tüːntare. </ta>
            <ta e="T51" id="Seg_626" s="T41">i tʼärɨmba qajda profesorane: man pajam qonǯɨrba qajda aŋanɨ qwässa. </ta>
            <ta e="T59" id="Seg_627" s="T51">a na prafesar wesʼ pin aːmdimba täbɨnan qäɣɨn. </ta>
            <ta e="T63" id="Seg_628" s="T59">qarʼiːmɨn tättɨmdälǯɨ časkɨn čaːnǯɨŋ. </ta>
            <ta e="T70" id="Seg_629" s="T63">ina quwam siːdɨt täp pitä qaːɣɨn süː. </ta>
            <ta e="T76" id="Seg_630" s="T70">na süː ilakumba na quːnan sündeɣɨn. </ta>
            <ta e="T78" id="Seg_631" s="T76">patpikumba aŋɨɣɨn. </ta>
            <ta e="T84" id="Seg_632" s="T78">nännɨ na qum soːj äːsuzan (äːsukuzan). </ta>
            <ta e="T91" id="Seg_633" s="T84">na prafesɨr assɨ mimbat na qunni qugu. </ta>
            <ta e="T96" id="Seg_634" s="T91">na qum tidam na ilaqquŋ. </ta>
            <ta e="T101" id="Seg_635" s="T96">na äːssan qundoqɨn i sitʼeptəmbiqumbattə. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_636" s="T1">oqqɨr-ɨ-ŋ</ta>
            <ta e="T3" id="Seg_637" s="T2">matʼtʼö-ɣɨn</ta>
            <ta e="T4" id="Seg_638" s="T3">qum</ta>
            <ta e="T5" id="Seg_639" s="T4">qonʼdi-nba</ta>
            <ta e="T6" id="Seg_640" s="T5">oqqɨr-ɨ-ŋ</ta>
            <ta e="T7" id="Seg_641" s="T6">ünda-di-t</ta>
            <ta e="T8" id="Seg_642" s="T7">qo-nǯu-r-na-m</ta>
            <ta e="T9" id="Seg_643" s="T8">küdärʼe-m</ta>
            <ta e="T10" id="Seg_644" s="T9">qaj-ta</ta>
            <ta e="T11" id="Seg_645" s="T10">lʼen-i-m</ta>
            <ta e="T12" id="Seg_646" s="T11">am-lʼe</ta>
            <ta e="T13" id="Seg_647" s="T12">taːdə-r-a-m</ta>
            <ta e="T14" id="Seg_648" s="T13">na</ta>
            <ta e="T15" id="Seg_649" s="T14">resiti-za-ŋ</ta>
            <ta e="T16" id="Seg_650" s="T15">oqqɨr-ɨ-ŋ</ta>
            <ta e="T17" id="Seg_651" s="T16">ündɨ-dʼä-m</ta>
            <ta e="T18" id="Seg_652" s="T17">paja</ta>
            <ta e="T19" id="Seg_653" s="T18">siːtä-lʼe</ta>
            <ta e="T20" id="Seg_654" s="T19">taːdä-r-ɨ-ŋ</ta>
            <ta e="T21" id="Seg_655" s="T20">tʼärɨ-n</ta>
            <ta e="T22" id="Seg_656" s="T21">qaj-ta</ta>
            <ta e="T23" id="Seg_657" s="T22">tat</ta>
            <ta e="T24" id="Seg_658" s="T23">aŋ-ɨ-nända</ta>
            <ta e="T25" id="Seg_659" s="T24">qwäs-sa</ta>
            <ta e="T26" id="Seg_660" s="T25">i</ta>
            <ta e="T27" id="Seg_661" s="T26">täb-a-nni</ta>
            <ta e="T28" id="Seg_662" s="T27">tʼärɨ-n</ta>
            <ta e="T29" id="Seg_663" s="T28">na</ta>
            <ta e="T30" id="Seg_664" s="T29">qun-ni</ta>
            <ta e="T31" id="Seg_665" s="T30">na</ta>
            <ta e="T32" id="Seg_666" s="T31">qum</ta>
            <ta e="T33" id="Seg_667" s="T32">tüwu-ŋ</ta>
            <ta e="T34" id="Seg_668" s="T33">oqqɨr-ɨ-ŋ</ta>
            <ta e="T35" id="Seg_669" s="T34">qwan-ba</ta>
            <ta e="T36" id="Seg_670" s="T35">balʼnʼicaː-ndə</ta>
            <ta e="T37" id="Seg_671" s="T36">tʼärɨ-mba</ta>
            <ta e="T38" id="Seg_672" s="T37">qaj-ta</ta>
            <ta e="T39" id="Seg_673" s="T38">sünde-m</ta>
            <ta e="T40" id="Seg_674" s="T39">por-ɨ-mba</ta>
            <ta e="T41" id="Seg_675" s="T40">tüː-n-tare</ta>
            <ta e="T42" id="Seg_676" s="T41">i</ta>
            <ta e="T43" id="Seg_677" s="T42">tʼärɨ-mba</ta>
            <ta e="T44" id="Seg_678" s="T43">qaj-ta</ta>
            <ta e="T45" id="Seg_679" s="T44">profesor-a-ne</ta>
            <ta e="T46" id="Seg_680" s="T45">man</ta>
            <ta e="T47" id="Seg_681" s="T46">paja-m</ta>
            <ta e="T48" id="Seg_682" s="T47">qo-nǯɨ-r-ba</ta>
            <ta e="T49" id="Seg_683" s="T48">qaj-ta</ta>
            <ta e="T50" id="Seg_684" s="T49">aŋ-a-nɨ</ta>
            <ta e="T51" id="Seg_685" s="T50">qwäs-sa</ta>
            <ta e="T52" id="Seg_686" s="T51">a</ta>
            <ta e="T53" id="Seg_687" s="T52">na</ta>
            <ta e="T54" id="Seg_688" s="T53">prafesar</ta>
            <ta e="T55" id="Seg_689" s="T54">wesʼ</ta>
            <ta e="T56" id="Seg_690" s="T55">pi-n</ta>
            <ta e="T57" id="Seg_691" s="T56">aːmdi-mba</ta>
            <ta e="T58" id="Seg_692" s="T57">täb-ɨ-nan</ta>
            <ta e="T59" id="Seg_693" s="T58">qä-ɣɨn</ta>
            <ta e="T60" id="Seg_694" s="T59">qarʼiː-mɨn</ta>
            <ta e="T61" id="Seg_695" s="T60">tättɨ-mdälǯɨ</ta>
            <ta e="T62" id="Seg_696" s="T61">čas-kɨn</ta>
            <ta e="T63" id="Seg_697" s="T62">čaːnǯɨ-n</ta>
            <ta e="T64" id="Seg_698" s="T63">ina</ta>
            <ta e="T65" id="Seg_699" s="T64">quw-a-m</ta>
            <ta e="T66" id="Seg_700" s="T65">siːdɨ-t</ta>
            <ta e="T67" id="Seg_701" s="T66">täp</ta>
            <ta e="T68" id="Seg_702" s="T67">pitä</ta>
            <ta e="T69" id="Seg_703" s="T68">qaː-ɣɨn</ta>
            <ta e="T70" id="Seg_704" s="T69">süː</ta>
            <ta e="T71" id="Seg_705" s="T70">na</ta>
            <ta e="T72" id="Seg_706" s="T71">süː</ta>
            <ta e="T73" id="Seg_707" s="T72">ila-ku-mba</ta>
            <ta e="T74" id="Seg_708" s="T73">na</ta>
            <ta e="T75" id="Seg_709" s="T74">quː-nan</ta>
            <ta e="T76" id="Seg_710" s="T75">sünde-ɣɨn</ta>
            <ta e="T77" id="Seg_711" s="T76">patpi-ku-mba</ta>
            <ta e="T78" id="Seg_712" s="T77">aŋ-ɨ-ɣɨn</ta>
            <ta e="T79" id="Seg_713" s="T78">nännɨ</ta>
            <ta e="T80" id="Seg_714" s="T79">na</ta>
            <ta e="T81" id="Seg_715" s="T80">qum</ta>
            <ta e="T82" id="Seg_716" s="T81">soːj</ta>
            <ta e="T83" id="Seg_717" s="T82">äːsu-za-n</ta>
            <ta e="T84" id="Seg_718" s="T83">äːsu-ku-za-n</ta>
            <ta e="T85" id="Seg_719" s="T84">na</ta>
            <ta e="T86" id="Seg_720" s="T85">prafesɨr</ta>
            <ta e="T87" id="Seg_721" s="T86">assɨ</ta>
            <ta e="T88" id="Seg_722" s="T87">mi-mba-t</ta>
            <ta e="T89" id="Seg_723" s="T88">na</ta>
            <ta e="T90" id="Seg_724" s="T89">qun-ni</ta>
            <ta e="T91" id="Seg_725" s="T90">qu-gu</ta>
            <ta e="T92" id="Seg_726" s="T91">na</ta>
            <ta e="T93" id="Seg_727" s="T92">qum</ta>
            <ta e="T94" id="Seg_728" s="T93">tidam</ta>
            <ta e="T95" id="Seg_729" s="T94">na</ta>
            <ta e="T96" id="Seg_730" s="T95">ila-qqu-ŋ</ta>
            <ta e="T97" id="Seg_731" s="T96">na</ta>
            <ta e="T98" id="Seg_732" s="T97">äː-ssa-ŋ</ta>
            <ta e="T99" id="Seg_733" s="T98">qundo-qɨn</ta>
            <ta e="T100" id="Seg_734" s="T99">i</ta>
            <ta e="T101" id="Seg_735" s="T100">sitʼeptə-mbi-qu-mba-ttə</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_736" s="T1">okkɨr-ɨ-k</ta>
            <ta e="T3" id="Seg_737" s="T2">matʼtʼi-qən</ta>
            <ta e="T4" id="Seg_738" s="T3">qum</ta>
            <ta e="T5" id="Seg_739" s="T4">qontə-mbɨ</ta>
            <ta e="T6" id="Seg_740" s="T5">okkɨr-ɨ-k</ta>
            <ta e="T7" id="Seg_741" s="T6">ündɨ-ntɨ-tɨ</ta>
            <ta e="T8" id="Seg_742" s="T7">qo-nče-r-ŋɨ-m</ta>
            <ta e="T9" id="Seg_743" s="T8">qüːdere-m</ta>
            <ta e="T10" id="Seg_744" s="T9">qaj-ta</ta>
            <ta e="T11" id="Seg_745" s="T10">lʼen-ɨ-m</ta>
            <ta e="T12" id="Seg_746" s="T11">am-le</ta>
            <ta e="T13" id="Seg_747" s="T12">tadɨ-r-ɨ-m</ta>
            <ta e="T14" id="Seg_748" s="T13">na</ta>
            <ta e="T15" id="Seg_749" s="T14">resiti-sɨ-n</ta>
            <ta e="T16" id="Seg_750" s="T15">okkɨr-ɨ-k</ta>
            <ta e="T17" id="Seg_751" s="T16">ündɨ-ntɨ-m</ta>
            <ta e="T18" id="Seg_752" s="T17">paja</ta>
            <ta e="T19" id="Seg_753" s="T18">söde-le</ta>
            <ta e="T20" id="Seg_754" s="T19">tadɨ-r-ɨ-n</ta>
            <ta e="T21" id="Seg_755" s="T20">tʼarɨ-n</ta>
            <ta e="T22" id="Seg_756" s="T21">qaj-ta</ta>
            <ta e="T23" id="Seg_757" s="T22">tan</ta>
            <ta e="T24" id="Seg_758" s="T23">aːŋ-ɨ-qəntɨ</ta>
            <ta e="T25" id="Seg_759" s="T24">qwən-sɨ</ta>
            <ta e="T26" id="Seg_760" s="T25">i</ta>
            <ta e="T27" id="Seg_761" s="T26">tap-ɨ-nɨ</ta>
            <ta e="T28" id="Seg_762" s="T27">tʼarɨ-n</ta>
            <ta e="T29" id="Seg_763" s="T28">na</ta>
            <ta e="T30" id="Seg_764" s="T29">qum-nɨ</ta>
            <ta e="T31" id="Seg_765" s="T30">na</ta>
            <ta e="T32" id="Seg_766" s="T31">qum</ta>
            <ta e="T33" id="Seg_767" s="T32">töu-n</ta>
            <ta e="T34" id="Seg_768" s="T33">okkɨr-ɨ-k</ta>
            <ta e="T35" id="Seg_769" s="T34">qwən-mbɨ</ta>
            <ta e="T36" id="Seg_770" s="T35">balʼnʼica-ndɨ</ta>
            <ta e="T37" id="Seg_771" s="T36">tʼarɨ-mbɨ</ta>
            <ta e="T38" id="Seg_772" s="T37">qaj-ta</ta>
            <ta e="T39" id="Seg_773" s="T38">sündʼi-mɨ</ta>
            <ta e="T40" id="Seg_774" s="T39">por-ɨ-mbɨ</ta>
            <ta e="T41" id="Seg_775" s="T40">tüː-n-tare</ta>
            <ta e="T42" id="Seg_776" s="T41">i</ta>
            <ta e="T43" id="Seg_777" s="T42">tʼarɨ-mbɨ</ta>
            <ta e="T44" id="Seg_778" s="T43">qaj-ta</ta>
            <ta e="T45" id="Seg_779" s="T44">profesor-ɨ-nɨ</ta>
            <ta e="T46" id="Seg_780" s="T45">man</ta>
            <ta e="T47" id="Seg_781" s="T46">paja-mɨ</ta>
            <ta e="T48" id="Seg_782" s="T47">qo-nče-r-mbɨ</ta>
            <ta e="T49" id="Seg_783" s="T48">qaj-ta</ta>
            <ta e="T50" id="Seg_784" s="T49">aːŋ-ɨ-nɨ</ta>
            <ta e="T51" id="Seg_785" s="T50">qwən-sɨ</ta>
            <ta e="T52" id="Seg_786" s="T51">a</ta>
            <ta e="T53" id="Seg_787" s="T52">na</ta>
            <ta e="T54" id="Seg_788" s="T53">profesor</ta>
            <ta e="T55" id="Seg_789" s="T54">wesʼ</ta>
            <ta e="T56" id="Seg_790" s="T55">pi-n</ta>
            <ta e="T57" id="Seg_791" s="T56">aːmdi-mbɨ</ta>
            <ta e="T58" id="Seg_792" s="T57">tap-ɨ-nan</ta>
            <ta e="T59" id="Seg_793" s="T58">qəː-qən</ta>
            <ta e="T60" id="Seg_794" s="T59">qarɨ-mɨn</ta>
            <ta e="T61" id="Seg_795" s="T60">tättɨ-mǯel</ta>
            <ta e="T62" id="Seg_796" s="T61">čas-qən</ta>
            <ta e="T63" id="Seg_797" s="T62">čanǯɨ-n</ta>
            <ta e="T64" id="Seg_798" s="T63">ina</ta>
            <ta e="T65" id="Seg_799" s="T64">qum-ɨ-m</ta>
            <ta e="T66" id="Seg_800" s="T65">sittə-tɨ</ta>
            <ta e="T67" id="Seg_801" s="T66">tap</ta>
            <ta e="T68" id="Seg_802" s="T67">pitä</ta>
            <ta e="T69" id="Seg_803" s="T68">qaj-qən</ta>
            <ta e="T70" id="Seg_804" s="T69">sü</ta>
            <ta e="T71" id="Seg_805" s="T70">na</ta>
            <ta e="T72" id="Seg_806" s="T71">sü</ta>
            <ta e="T73" id="Seg_807" s="T72">illɨ-ku-mbɨ</ta>
            <ta e="T74" id="Seg_808" s="T73">na</ta>
            <ta e="T75" id="Seg_809" s="T74">qum-nan</ta>
            <ta e="T76" id="Seg_810" s="T75">sündʼi-qən</ta>
            <ta e="T77" id="Seg_811" s="T76">patpɨ-ku-mbɨ</ta>
            <ta e="T78" id="Seg_812" s="T77">aːŋ-ɨ-qən</ta>
            <ta e="T79" id="Seg_813" s="T78">nɨːnɨ</ta>
            <ta e="T80" id="Seg_814" s="T79">na</ta>
            <ta e="T81" id="Seg_815" s="T80">qum</ta>
            <ta e="T82" id="Seg_816" s="T81">sоj</ta>
            <ta e="T83" id="Seg_817" s="T82">äsɨ-sɨ-n</ta>
            <ta e="T84" id="Seg_818" s="T83">äsɨ-ku-sɨ-n</ta>
            <ta e="T85" id="Seg_819" s="T84">na</ta>
            <ta e="T86" id="Seg_820" s="T85">profesor</ta>
            <ta e="T87" id="Seg_821" s="T86">assɨ</ta>
            <ta e="T88" id="Seg_822" s="T87">mi-mbɨ-tɨ</ta>
            <ta e="T89" id="Seg_823" s="T88">na</ta>
            <ta e="T90" id="Seg_824" s="T89">qum-nɨ</ta>
            <ta e="T91" id="Seg_825" s="T90">quː-gu</ta>
            <ta e="T92" id="Seg_826" s="T91">na</ta>
            <ta e="T93" id="Seg_827" s="T92">qum</ta>
            <ta e="T94" id="Seg_828" s="T93">tiːtam</ta>
            <ta e="T95" id="Seg_829" s="T94">na</ta>
            <ta e="T96" id="Seg_830" s="T95">illɨ-ku-n</ta>
            <ta e="T97" id="Seg_831" s="T96">na</ta>
            <ta e="T98" id="Seg_832" s="T97">eː-sɨ-n</ta>
            <ta e="T99" id="Seg_833" s="T98">kundɨ-qɨn</ta>
            <ta e="T100" id="Seg_834" s="T99">i</ta>
            <ta e="T101" id="Seg_835" s="T100">sitteptɨ-mbɨ-ku-mbɨ-tɨt</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_836" s="T1">one-EP-ADVZ</ta>
            <ta e="T3" id="Seg_837" s="T2">taiga-LOC</ta>
            <ta e="T4" id="Seg_838" s="T3">human.being.[NOM]</ta>
            <ta e="T5" id="Seg_839" s="T4">sleep-PST.NAR.[3SG.S]</ta>
            <ta e="T6" id="Seg_840" s="T5">one-EP-ADVZ</ta>
            <ta e="T7" id="Seg_841" s="T6">hear-IPFV-3SG.O</ta>
            <ta e="T8" id="Seg_842" s="T7">see-IPFV3-FRQ-CO-1SG.O</ta>
            <ta e="T9" id="Seg_843" s="T8">dream-ACC</ta>
            <ta e="T10" id="Seg_844" s="T9">what-INDEF</ta>
            <ta e="T11" id="Seg_845" s="T10">idleness-EP-ACC</ta>
            <ta e="T12" id="Seg_846" s="T11">eat-CVB</ta>
            <ta e="T13" id="Seg_847" s="T12">bring-FRQ-EP-1SG.O</ta>
            <ta e="T14" id="Seg_848" s="T13">this</ta>
            <ta e="T15" id="Seg_849" s="T14">%%-PST-3SG.S</ta>
            <ta e="T16" id="Seg_850" s="T15">one-EP-ADVZ</ta>
            <ta e="T17" id="Seg_851" s="T16">hear-IPFV-1SG.O</ta>
            <ta e="T18" id="Seg_852" s="T17">old.woman.[NOM]</ta>
            <ta e="T19" id="Seg_853" s="T18">wake.up-CVB</ta>
            <ta e="T20" id="Seg_854" s="T19">bring-FRQ-EP-3SG.S</ta>
            <ta e="T21" id="Seg_855" s="T20">say-3SG.S</ta>
            <ta e="T22" id="Seg_856" s="T21">what-INDEF</ta>
            <ta e="T23" id="Seg_857" s="T22">you.SG.GEN</ta>
            <ta e="T24" id="Seg_858" s="T23">mouth-EP-ILL.3SG</ta>
            <ta e="T25" id="Seg_859" s="T24">go.away-PST.[3SG.S]</ta>
            <ta e="T26" id="Seg_860" s="T25">and</ta>
            <ta e="T27" id="Seg_861" s="T26">(s)he-EP-ALL</ta>
            <ta e="T28" id="Seg_862" s="T27">say-3SG.S</ta>
            <ta e="T29" id="Seg_863" s="T28">this</ta>
            <ta e="T30" id="Seg_864" s="T29">human.being-ALL</ta>
            <ta e="T31" id="Seg_865" s="T30">this</ta>
            <ta e="T32" id="Seg_866" s="T31">human.being.[NOM]</ta>
            <ta e="T33" id="Seg_867" s="T32">get.angry-3SG.S</ta>
            <ta e="T34" id="Seg_868" s="T33">one-EP-ADVZ</ta>
            <ta e="T35" id="Seg_869" s="T34">go.away-PST.NAR.[3SG.S]</ta>
            <ta e="T36" id="Seg_870" s="T35">hospital-ILL</ta>
            <ta e="T37" id="Seg_871" s="T36">say-PST.NAR.[3SG.S]</ta>
            <ta e="T38" id="Seg_872" s="T37">what-INDEF</ta>
            <ta e="T39" id="Seg_873" s="T38">inside-1SG</ta>
            <ta e="T40" id="Seg_874" s="T39">burn.down-EP-PST.NAR.[3SG.S]</ta>
            <ta e="T41" id="Seg_875" s="T40">fire-GEN-like</ta>
            <ta e="T42" id="Seg_876" s="T41">and</ta>
            <ta e="T43" id="Seg_877" s="T42">say-PST.NAR.[3SG.S]</ta>
            <ta e="T44" id="Seg_878" s="T43">what-INDEF</ta>
            <ta e="T45" id="Seg_879" s="T44">professor-EP-ALL</ta>
            <ta e="T46" id="Seg_880" s="T45">I.NOM</ta>
            <ta e="T47" id="Seg_881" s="T46">old.woman-1SG</ta>
            <ta e="T48" id="Seg_882" s="T47">see-IPFV3-FRQ-PST.NAR.[3SG.S]</ta>
            <ta e="T49" id="Seg_883" s="T48">what-INDEF</ta>
            <ta e="T50" id="Seg_884" s="T49">mouth-EP-ALL</ta>
            <ta e="T51" id="Seg_885" s="T50">go.away-PST.[3SG.S]</ta>
            <ta e="T52" id="Seg_886" s="T51">but</ta>
            <ta e="T53" id="Seg_887" s="T52">this</ta>
            <ta e="T54" id="Seg_888" s="T53">professor.[NOM]</ta>
            <ta e="T55" id="Seg_889" s="T54">all</ta>
            <ta e="T56" id="Seg_890" s="T55">night-ADV.LOC</ta>
            <ta e="T57" id="Seg_891" s="T56">sit-PST.NAR.[3SG.S]</ta>
            <ta e="T58" id="Seg_892" s="T57">(s)he-EP-ADES</ta>
            <ta e="T59" id="Seg_893" s="T58">side-LOC</ta>
            <ta e="T60" id="Seg_894" s="T59">morning-PROL</ta>
            <ta e="T61" id="Seg_895" s="T60">four-ORD</ta>
            <ta e="T62" id="Seg_896" s="T61">hour-LOC</ta>
            <ta e="T63" id="Seg_897" s="T62">go.out-3SG.S</ta>
            <ta e="T64" id="Seg_898" s="T63">this.[NOM]</ta>
            <ta e="T65" id="Seg_899" s="T64">human.being-EP-ACC</ta>
            <ta e="T66" id="Seg_900" s="T65">wake.up-3SG.O</ta>
            <ta e="T67" id="Seg_901" s="T66">(s)he.[NOM]</ta>
            <ta e="T68" id="Seg_902" s="T67">night.[NOM]</ta>
            <ta e="T69" id="Seg_903" s="T68">what-LOC</ta>
            <ta e="T70" id="Seg_904" s="T69">snake.[NOM]</ta>
            <ta e="T71" id="Seg_905" s="T70">this</ta>
            <ta e="T72" id="Seg_906" s="T71">snake.[NOM]</ta>
            <ta e="T73" id="Seg_907" s="T72">live-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T74" id="Seg_908" s="T73">this</ta>
            <ta e="T75" id="Seg_909" s="T74">human.being-ADES</ta>
            <ta e="T76" id="Seg_910" s="T75">inside-LOC</ta>
            <ta e="T77" id="Seg_911" s="T76">be-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T78" id="Seg_912" s="T77">mouth-EP-LOC</ta>
            <ta e="T79" id="Seg_913" s="T78">then</ta>
            <ta e="T80" id="Seg_914" s="T79">this</ta>
            <ta e="T81" id="Seg_915" s="T80">human.being.[NOM]</ta>
            <ta e="T82" id="Seg_916" s="T81">good</ta>
            <ta e="T83" id="Seg_917" s="T82">become-PST-3SG.S</ta>
            <ta e="T84" id="Seg_918" s="T83">become-HAB-PST-3SG.S</ta>
            <ta e="T85" id="Seg_919" s="T84">this</ta>
            <ta e="T86" id="Seg_920" s="T85">professor.[NOM]</ta>
            <ta e="T87" id="Seg_921" s="T86">NEG</ta>
            <ta e="T88" id="Seg_922" s="T87">give-PST.NAR-3SG.O</ta>
            <ta e="T89" id="Seg_923" s="T88">this</ta>
            <ta e="T90" id="Seg_924" s="T89">human.being-ALL</ta>
            <ta e="T91" id="Seg_925" s="T90">die-INF</ta>
            <ta e="T92" id="Seg_926" s="T91">this</ta>
            <ta e="T93" id="Seg_927" s="T92">human.being.[NOM]</ta>
            <ta e="T94" id="Seg_928" s="T93">now</ta>
            <ta e="T95" id="Seg_929" s="T94">this</ta>
            <ta e="T96" id="Seg_930" s="T95">live-HAB-3SG.S</ta>
            <ta e="T97" id="Seg_931" s="T96">this</ta>
            <ta e="T98" id="Seg_932" s="T97">be-PST-3SG.S</ta>
            <ta e="T99" id="Seg_933" s="T98">long-ADV.LOC</ta>
            <ta e="T100" id="Seg_934" s="T99">and</ta>
            <ta e="T101" id="Seg_935" s="T100">cheat-DUR-HAB-PST.NAR-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_936" s="T1">один-EP-ADVZ</ta>
            <ta e="T3" id="Seg_937" s="T2">тайга-LOC</ta>
            <ta e="T4" id="Seg_938" s="T3">человек.[NOM]</ta>
            <ta e="T5" id="Seg_939" s="T4">спать-PST.NAR.[3SG.S]</ta>
            <ta e="T6" id="Seg_940" s="T5">один-EP-ADVZ</ta>
            <ta e="T7" id="Seg_941" s="T6">слышать-IPFV-3SG.O</ta>
            <ta e="T8" id="Seg_942" s="T7">увидеть-IPFV3-FRQ-CO-1SG.O</ta>
            <ta e="T9" id="Seg_943" s="T8">сон-ACC</ta>
            <ta e="T10" id="Seg_944" s="T9">что-INDEF</ta>
            <ta e="T11" id="Seg_945" s="T10">лень-EP-ACC</ta>
            <ta e="T12" id="Seg_946" s="T11">съесть-CVB</ta>
            <ta e="T13" id="Seg_947" s="T12">принести-FRQ-EP-1SG.O</ta>
            <ta e="T14" id="Seg_948" s="T13">этот</ta>
            <ta e="T15" id="Seg_949" s="T14">%%-PST-3SG.S</ta>
            <ta e="T16" id="Seg_950" s="T15">один-EP-ADVZ</ta>
            <ta e="T17" id="Seg_951" s="T16">слышать-IPFV-1SG.O</ta>
            <ta e="T18" id="Seg_952" s="T17">старуха.[NOM]</ta>
            <ta e="T19" id="Seg_953" s="T18">проснуться-CVB</ta>
            <ta e="T20" id="Seg_954" s="T19">принести-FRQ-EP-3SG.S</ta>
            <ta e="T21" id="Seg_955" s="T20">сказать-3SG.S</ta>
            <ta e="T22" id="Seg_956" s="T21">что-INDEF</ta>
            <ta e="T23" id="Seg_957" s="T22">ты.GEN</ta>
            <ta e="T24" id="Seg_958" s="T23">рот-EP-ILL.3SG</ta>
            <ta e="T25" id="Seg_959" s="T24">уйти-PST.[3SG.S]</ta>
            <ta e="T26" id="Seg_960" s="T25">и</ta>
            <ta e="T27" id="Seg_961" s="T26">он(а)-EP-ALL</ta>
            <ta e="T28" id="Seg_962" s="T27">сказать-3SG.S</ta>
            <ta e="T29" id="Seg_963" s="T28">этот</ta>
            <ta e="T30" id="Seg_964" s="T29">человек-ALL</ta>
            <ta e="T31" id="Seg_965" s="T30">этот</ta>
            <ta e="T32" id="Seg_966" s="T31">человек.[NOM]</ta>
            <ta e="T33" id="Seg_967" s="T32">рассердиться-3SG.S</ta>
            <ta e="T34" id="Seg_968" s="T33">один-EP-ADVZ</ta>
            <ta e="T35" id="Seg_969" s="T34">уйти-PST.NAR.[3SG.S]</ta>
            <ta e="T36" id="Seg_970" s="T35">больница-ILL</ta>
            <ta e="T37" id="Seg_971" s="T36">сказать-PST.NAR.[3SG.S]</ta>
            <ta e="T38" id="Seg_972" s="T37">что-INDEF</ta>
            <ta e="T39" id="Seg_973" s="T38">нутро-1SG</ta>
            <ta e="T40" id="Seg_974" s="T39">сгореть-EP-PST.NAR.[3SG.S]</ta>
            <ta e="T41" id="Seg_975" s="T40">огонь-GEN-как</ta>
            <ta e="T42" id="Seg_976" s="T41">и</ta>
            <ta e="T43" id="Seg_977" s="T42">сказать-PST.NAR.[3SG.S]</ta>
            <ta e="T44" id="Seg_978" s="T43">что-INDEF</ta>
            <ta e="T45" id="Seg_979" s="T44">профессор-EP-ALL</ta>
            <ta e="T46" id="Seg_980" s="T45">я.NOM</ta>
            <ta e="T47" id="Seg_981" s="T46">старуха-1SG</ta>
            <ta e="T48" id="Seg_982" s="T47">увидеть-IPFV3-FRQ-PST.NAR.[3SG.S]</ta>
            <ta e="T49" id="Seg_983" s="T48">что-INDEF</ta>
            <ta e="T50" id="Seg_984" s="T49">рот-EP-ALL</ta>
            <ta e="T51" id="Seg_985" s="T50">уйти-PST.[3SG.S]</ta>
            <ta e="T52" id="Seg_986" s="T51">а</ta>
            <ta e="T53" id="Seg_987" s="T52">этот</ta>
            <ta e="T54" id="Seg_988" s="T53">профессор.[NOM]</ta>
            <ta e="T55" id="Seg_989" s="T54">весь</ta>
            <ta e="T56" id="Seg_990" s="T55">ночь-ADV.LOC</ta>
            <ta e="T57" id="Seg_991" s="T56">сидеть-PST.NAR.[3SG.S]</ta>
            <ta e="T58" id="Seg_992" s="T57">он(а)-EP-ADES</ta>
            <ta e="T59" id="Seg_993" s="T58">бок-LOC</ta>
            <ta e="T60" id="Seg_994" s="T59">утро-PROL</ta>
            <ta e="T61" id="Seg_995" s="T60">четыре-ORD</ta>
            <ta e="T62" id="Seg_996" s="T61">час-LOC</ta>
            <ta e="T63" id="Seg_997" s="T62">выйти-3SG.S</ta>
            <ta e="T64" id="Seg_998" s="T63">этот.[NOM]</ta>
            <ta e="T65" id="Seg_999" s="T64">человек-EP-ACC</ta>
            <ta e="T66" id="Seg_1000" s="T65">разбудить-3SG.O</ta>
            <ta e="T67" id="Seg_1001" s="T66">он(а).[NOM]</ta>
            <ta e="T68" id="Seg_1002" s="T67">ночь.[NOM]</ta>
            <ta e="T69" id="Seg_1003" s="T68">что-LOC</ta>
            <ta e="T70" id="Seg_1004" s="T69">змея.[NOM]</ta>
            <ta e="T71" id="Seg_1005" s="T70">этот</ta>
            <ta e="T72" id="Seg_1006" s="T71">змея.[NOM]</ta>
            <ta e="T73" id="Seg_1007" s="T72">жить-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T74" id="Seg_1008" s="T73">этот</ta>
            <ta e="T75" id="Seg_1009" s="T74">человек-ADES</ta>
            <ta e="T76" id="Seg_1010" s="T75">нутро-LOC</ta>
            <ta e="T77" id="Seg_1011" s="T76">находиться-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T78" id="Seg_1012" s="T77">рот-EP-LOC</ta>
            <ta e="T79" id="Seg_1013" s="T78">потом</ta>
            <ta e="T80" id="Seg_1014" s="T79">этот</ta>
            <ta e="T81" id="Seg_1015" s="T80">человек.[NOM]</ta>
            <ta e="T82" id="Seg_1016" s="T81">хороший</ta>
            <ta e="T83" id="Seg_1017" s="T82">стать-PST-3SG.S</ta>
            <ta e="T84" id="Seg_1018" s="T83">стать-HAB-PST-3SG.S</ta>
            <ta e="T85" id="Seg_1019" s="T84">этот</ta>
            <ta e="T86" id="Seg_1020" s="T85">профессор.[NOM]</ta>
            <ta e="T87" id="Seg_1021" s="T86">NEG</ta>
            <ta e="T88" id="Seg_1022" s="T87">дать-PST.NAR-3SG.O</ta>
            <ta e="T89" id="Seg_1023" s="T88">этот</ta>
            <ta e="T90" id="Seg_1024" s="T89">человек-ALL</ta>
            <ta e="T91" id="Seg_1025" s="T90">умереть-INF</ta>
            <ta e="T92" id="Seg_1026" s="T91">этот</ta>
            <ta e="T93" id="Seg_1027" s="T92">человек.[NOM]</ta>
            <ta e="T94" id="Seg_1028" s="T93">сейчас</ta>
            <ta e="T95" id="Seg_1029" s="T94">этот</ta>
            <ta e="T96" id="Seg_1030" s="T95">жить-HAB-3SG.S</ta>
            <ta e="T97" id="Seg_1031" s="T96">этот</ta>
            <ta e="T98" id="Seg_1032" s="T97">быть-PST-3SG.S</ta>
            <ta e="T99" id="Seg_1033" s="T98">долго-ADV.LOC</ta>
            <ta e="T100" id="Seg_1034" s="T99">и</ta>
            <ta e="T101" id="Seg_1035" s="T100">обмануть-DUR-HAB-PST.NAR-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1036" s="T1">num-n:ins-adj&gt;adv</ta>
            <ta e="T3" id="Seg_1037" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_1038" s="T3">n.[n:case]</ta>
            <ta e="T5" id="Seg_1039" s="T4">v-v:tense.[v:pn]</ta>
            <ta e="T6" id="Seg_1040" s="T5">num-n:ins-adj&gt;adv</ta>
            <ta e="T7" id="Seg_1041" s="T6">v-v&gt;v-v:pn</ta>
            <ta e="T8" id="Seg_1042" s="T7">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T9" id="Seg_1043" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_1044" s="T9">interrog-clit</ta>
            <ta e="T11" id="Seg_1045" s="T10">n-n:ins-n:case</ta>
            <ta e="T12" id="Seg_1046" s="T11">v-v&gt;adv</ta>
            <ta e="T13" id="Seg_1047" s="T12">v-v&gt;v-n:ins-v:pn</ta>
            <ta e="T14" id="Seg_1048" s="T13">dem</ta>
            <ta e="T15" id="Seg_1049" s="T14">v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_1050" s="T15">num-n:ins-adj&gt;adv</ta>
            <ta e="T17" id="Seg_1051" s="T16">v-v&gt;v-v:pn</ta>
            <ta e="T18" id="Seg_1052" s="T17">n.[n:case]</ta>
            <ta e="T19" id="Seg_1053" s="T18">v-v&gt;adv</ta>
            <ta e="T20" id="Seg_1054" s="T19">v-v&gt;v-n:ins-v:pn</ta>
            <ta e="T21" id="Seg_1055" s="T20">v-v:pn</ta>
            <ta e="T22" id="Seg_1056" s="T21">interrog-clit</ta>
            <ta e="T23" id="Seg_1057" s="T22">pers</ta>
            <ta e="T24" id="Seg_1058" s="T23">n-n:ins-n:case.poss</ta>
            <ta e="T25" id="Seg_1059" s="T24">v-v:tense.[v:pn]</ta>
            <ta e="T26" id="Seg_1060" s="T25">conj</ta>
            <ta e="T27" id="Seg_1061" s="T26">pers-n:ins-n:case</ta>
            <ta e="T28" id="Seg_1062" s="T27">v-v:pn</ta>
            <ta e="T29" id="Seg_1063" s="T28">dem</ta>
            <ta e="T30" id="Seg_1064" s="T29">n-n:case</ta>
            <ta e="T31" id="Seg_1065" s="T30">dem</ta>
            <ta e="T32" id="Seg_1066" s="T31">n.[n:case]</ta>
            <ta e="T33" id="Seg_1067" s="T32">v-v:pn</ta>
            <ta e="T34" id="Seg_1068" s="T33">num-n:ins-adj&gt;adv</ta>
            <ta e="T35" id="Seg_1069" s="T34">v-v:tense.[v:pn]</ta>
            <ta e="T36" id="Seg_1070" s="T35">n-n:case</ta>
            <ta e="T37" id="Seg_1071" s="T36">v-v:tense.[v:pn]</ta>
            <ta e="T38" id="Seg_1072" s="T37">interrog-clit</ta>
            <ta e="T39" id="Seg_1073" s="T38">n-n:poss</ta>
            <ta e="T40" id="Seg_1074" s="T39">v-n:ins-v:tense.[v:pn]</ta>
            <ta e="T41" id="Seg_1075" s="T40">n-n:case-pp</ta>
            <ta e="T42" id="Seg_1076" s="T41">conj</ta>
            <ta e="T43" id="Seg_1077" s="T42">v-v:tense.[v:pn]</ta>
            <ta e="T44" id="Seg_1078" s="T43">interrog-clit</ta>
            <ta e="T45" id="Seg_1079" s="T44">n-n:ins-n:case</ta>
            <ta e="T46" id="Seg_1080" s="T45">pers</ta>
            <ta e="T47" id="Seg_1081" s="T46">n-n:poss</ta>
            <ta e="T48" id="Seg_1082" s="T47">v-v&gt;v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T49" id="Seg_1083" s="T48">interrog-clit</ta>
            <ta e="T50" id="Seg_1084" s="T49">n-n:ins-n:case</ta>
            <ta e="T51" id="Seg_1085" s="T50">v-v:tense.[v:pn]</ta>
            <ta e="T52" id="Seg_1086" s="T51">conj</ta>
            <ta e="T53" id="Seg_1087" s="T52">dem</ta>
            <ta e="T54" id="Seg_1088" s="T53">n.[n:case]</ta>
            <ta e="T55" id="Seg_1089" s="T54">quant</ta>
            <ta e="T56" id="Seg_1090" s="T55">n-adv:case</ta>
            <ta e="T57" id="Seg_1091" s="T56">v-v:tense.[v:pn]</ta>
            <ta e="T58" id="Seg_1092" s="T57">pers-n:ins-n:case</ta>
            <ta e="T59" id="Seg_1093" s="T58">n-n:case</ta>
            <ta e="T60" id="Seg_1094" s="T59">n-n:case</ta>
            <ta e="T61" id="Seg_1095" s="T60">num-num&gt;adj</ta>
            <ta e="T62" id="Seg_1096" s="T61">n-n:case</ta>
            <ta e="T63" id="Seg_1097" s="T62">v-v:pn</ta>
            <ta e="T64" id="Seg_1098" s="T63">dem.[n:case]</ta>
            <ta e="T65" id="Seg_1099" s="T64">n-n:ins-n:case</ta>
            <ta e="T66" id="Seg_1100" s="T65">v-v:pn</ta>
            <ta e="T67" id="Seg_1101" s="T66">pers.[n:case]</ta>
            <ta e="T68" id="Seg_1102" s="T67">n.[n:case]</ta>
            <ta e="T69" id="Seg_1103" s="T68">interrog-n:case</ta>
            <ta e="T70" id="Seg_1104" s="T69">n.[n:case]</ta>
            <ta e="T71" id="Seg_1105" s="T70">dem</ta>
            <ta e="T72" id="Seg_1106" s="T71">n.[n:case]</ta>
            <ta e="T73" id="Seg_1107" s="T72">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T74" id="Seg_1108" s="T73">dem</ta>
            <ta e="T75" id="Seg_1109" s="T74">n-n:case</ta>
            <ta e="T76" id="Seg_1110" s="T75">n-n:case</ta>
            <ta e="T77" id="Seg_1111" s="T76">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T78" id="Seg_1112" s="T77">n-n:ins-n:case</ta>
            <ta e="T79" id="Seg_1113" s="T78">adv</ta>
            <ta e="T80" id="Seg_1114" s="T79">dem</ta>
            <ta e="T81" id="Seg_1115" s="T80">n.[n:case]</ta>
            <ta e="T82" id="Seg_1116" s="T81">adj</ta>
            <ta e="T83" id="Seg_1117" s="T82">v-v:tense-v:pn</ta>
            <ta e="T84" id="Seg_1118" s="T83">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T85" id="Seg_1119" s="T84">dem</ta>
            <ta e="T86" id="Seg_1120" s="T85">n.[n:case]</ta>
            <ta e="T87" id="Seg_1121" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_1122" s="T87">v-v:tense-v:pn</ta>
            <ta e="T89" id="Seg_1123" s="T88">dem</ta>
            <ta e="T90" id="Seg_1124" s="T89">n-n:case</ta>
            <ta e="T91" id="Seg_1125" s="T90">v-v:inf</ta>
            <ta e="T92" id="Seg_1126" s="T91">dem</ta>
            <ta e="T93" id="Seg_1127" s="T92">n.[n:case]</ta>
            <ta e="T94" id="Seg_1128" s="T93">adv</ta>
            <ta e="T95" id="Seg_1129" s="T94">dem</ta>
            <ta e="T96" id="Seg_1130" s="T95">v-v&gt;v-v:pn</ta>
            <ta e="T97" id="Seg_1131" s="T96">dem</ta>
            <ta e="T98" id="Seg_1132" s="T97">v-v:tense-v:pn</ta>
            <ta e="T99" id="Seg_1133" s="T98">adv-adv:case</ta>
            <ta e="T100" id="Seg_1134" s="T99">conj</ta>
            <ta e="T101" id="Seg_1135" s="T100">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1136" s="T1">num</ta>
            <ta e="T3" id="Seg_1137" s="T2">n</ta>
            <ta e="T4" id="Seg_1138" s="T3">n</ta>
            <ta e="T5" id="Seg_1139" s="T4">v</ta>
            <ta e="T6" id="Seg_1140" s="T5">num</ta>
            <ta e="T7" id="Seg_1141" s="T6">v</ta>
            <ta e="T8" id="Seg_1142" s="T7">v</ta>
            <ta e="T9" id="Seg_1143" s="T8">n</ta>
            <ta e="T10" id="Seg_1144" s="T9">pro</ta>
            <ta e="T11" id="Seg_1145" s="T10">n</ta>
            <ta e="T12" id="Seg_1146" s="T11">adv</ta>
            <ta e="T13" id="Seg_1147" s="T12">v</ta>
            <ta e="T14" id="Seg_1148" s="T13">dem</ta>
            <ta e="T15" id="Seg_1149" s="T14">v</ta>
            <ta e="T16" id="Seg_1150" s="T15">num</ta>
            <ta e="T17" id="Seg_1151" s="T16">v</ta>
            <ta e="T18" id="Seg_1152" s="T17">n</ta>
            <ta e="T19" id="Seg_1153" s="T18">adv</ta>
            <ta e="T20" id="Seg_1154" s="T19">v</ta>
            <ta e="T21" id="Seg_1155" s="T20">v</ta>
            <ta e="T22" id="Seg_1156" s="T21">pro</ta>
            <ta e="T23" id="Seg_1157" s="T22">pers</ta>
            <ta e="T24" id="Seg_1158" s="T23">n</ta>
            <ta e="T25" id="Seg_1159" s="T24">v</ta>
            <ta e="T26" id="Seg_1160" s="T25">conj</ta>
            <ta e="T27" id="Seg_1161" s="T26">pers</ta>
            <ta e="T28" id="Seg_1162" s="T27">v</ta>
            <ta e="T29" id="Seg_1163" s="T28">dem</ta>
            <ta e="T30" id="Seg_1164" s="T29">n</ta>
            <ta e="T31" id="Seg_1165" s="T30">dem</ta>
            <ta e="T32" id="Seg_1166" s="T31">n</ta>
            <ta e="T33" id="Seg_1167" s="T32">v</ta>
            <ta e="T34" id="Seg_1168" s="T33">num</ta>
            <ta e="T35" id="Seg_1169" s="T34">v</ta>
            <ta e="T36" id="Seg_1170" s="T35">n</ta>
            <ta e="T37" id="Seg_1171" s="T36">v</ta>
            <ta e="T38" id="Seg_1172" s="T37">pro</ta>
            <ta e="T39" id="Seg_1173" s="T38">n</ta>
            <ta e="T40" id="Seg_1174" s="T39">v</ta>
            <ta e="T41" id="Seg_1175" s="T40">pp</ta>
            <ta e="T42" id="Seg_1176" s="T41">conj</ta>
            <ta e="T43" id="Seg_1177" s="T42">v</ta>
            <ta e="T44" id="Seg_1178" s="T43">pro</ta>
            <ta e="T45" id="Seg_1179" s="T44">n</ta>
            <ta e="T46" id="Seg_1180" s="T45">pers</ta>
            <ta e="T47" id="Seg_1181" s="T46">n</ta>
            <ta e="T48" id="Seg_1182" s="T47">v</ta>
            <ta e="T49" id="Seg_1183" s="T48">pro</ta>
            <ta e="T50" id="Seg_1184" s="T49">n</ta>
            <ta e="T51" id="Seg_1185" s="T50">v</ta>
            <ta e="T52" id="Seg_1186" s="T51">conj</ta>
            <ta e="T53" id="Seg_1187" s="T52">dem</ta>
            <ta e="T54" id="Seg_1188" s="T53">n</ta>
            <ta e="T55" id="Seg_1189" s="T54">quant</ta>
            <ta e="T56" id="Seg_1190" s="T55">n</ta>
            <ta e="T57" id="Seg_1191" s="T56">v</ta>
            <ta e="T58" id="Seg_1192" s="T57">pers</ta>
            <ta e="T59" id="Seg_1193" s="T58">n</ta>
            <ta e="T60" id="Seg_1194" s="T59">n</ta>
            <ta e="T61" id="Seg_1195" s="T60">v</ta>
            <ta e="T62" id="Seg_1196" s="T61">n</ta>
            <ta e="T63" id="Seg_1197" s="T62">v</ta>
            <ta e="T64" id="Seg_1198" s="T63">dem</ta>
            <ta e="T65" id="Seg_1199" s="T64">n</ta>
            <ta e="T66" id="Seg_1200" s="T65">v</ta>
            <ta e="T67" id="Seg_1201" s="T66">pers</ta>
            <ta e="T68" id="Seg_1202" s="T67">n</ta>
            <ta e="T69" id="Seg_1203" s="T68">interrog</ta>
            <ta e="T70" id="Seg_1204" s="T69">n</ta>
            <ta e="T71" id="Seg_1205" s="T70">dem</ta>
            <ta e="T72" id="Seg_1206" s="T71">n</ta>
            <ta e="T73" id="Seg_1207" s="T72">v</ta>
            <ta e="T74" id="Seg_1208" s="T73">dem</ta>
            <ta e="T75" id="Seg_1209" s="T74">n</ta>
            <ta e="T76" id="Seg_1210" s="T75">n</ta>
            <ta e="T77" id="Seg_1211" s="T76">v</ta>
            <ta e="T78" id="Seg_1212" s="T77">n</ta>
            <ta e="T79" id="Seg_1213" s="T78">adv</ta>
            <ta e="T80" id="Seg_1214" s="T79">dem</ta>
            <ta e="T81" id="Seg_1215" s="T80">n</ta>
            <ta e="T82" id="Seg_1216" s="T81">adj</ta>
            <ta e="T83" id="Seg_1217" s="T82">v</ta>
            <ta e="T84" id="Seg_1218" s="T83">v</ta>
            <ta e="T85" id="Seg_1219" s="T84">dem</ta>
            <ta e="T86" id="Seg_1220" s="T85">n</ta>
            <ta e="T87" id="Seg_1221" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_1222" s="T87">v</ta>
            <ta e="T89" id="Seg_1223" s="T88">dem</ta>
            <ta e="T90" id="Seg_1224" s="T89">n</ta>
            <ta e="T91" id="Seg_1225" s="T90">v</ta>
            <ta e="T92" id="Seg_1226" s="T91">dem</ta>
            <ta e="T93" id="Seg_1227" s="T92">n</ta>
            <ta e="T94" id="Seg_1228" s="T93">adv</ta>
            <ta e="T95" id="Seg_1229" s="T94">dem</ta>
            <ta e="T96" id="Seg_1230" s="T95">v</ta>
            <ta e="T97" id="Seg_1231" s="T96">dem</ta>
            <ta e="T98" id="Seg_1232" s="T97">v</ta>
            <ta e="T99" id="Seg_1233" s="T98">adv</ta>
            <ta e="T100" id="Seg_1234" s="T99">conj</ta>
            <ta e="T101" id="Seg_1235" s="T100">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_1236" s="T1">adv:Time</ta>
            <ta e="T3" id="Seg_1237" s="T2">np:L</ta>
            <ta e="T4" id="Seg_1238" s="T3">np.h:Th</ta>
            <ta e="T7" id="Seg_1239" s="T6">0.3.h:E</ta>
            <ta e="T9" id="Seg_1240" s="T8">np:Th</ta>
            <ta e="T15" id="Seg_1241" s="T14">0.3.h:P</ta>
            <ta e="T17" id="Seg_1242" s="T16">0.1.h:E</ta>
            <ta e="T18" id="Seg_1243" s="T17">np.h:A</ta>
            <ta e="T21" id="Seg_1244" s="T20">0.3.h:A</ta>
            <ta e="T22" id="Seg_1245" s="T21">pro:A</ta>
            <ta e="T23" id="Seg_1246" s="T22">pro.h:Poss</ta>
            <ta e="T24" id="Seg_1247" s="T23">np:G</ta>
            <ta e="T27" id="Seg_1248" s="T26">pro.h:R</ta>
            <ta e="T28" id="Seg_1249" s="T27">0.3.h:A</ta>
            <ta e="T30" id="Seg_1250" s="T29">np.h:R</ta>
            <ta e="T32" id="Seg_1251" s="T31">np.h:E</ta>
            <ta e="T34" id="Seg_1252" s="T33">adv:Time</ta>
            <ta e="T35" id="Seg_1253" s="T34">0.3.h:A</ta>
            <ta e="T36" id="Seg_1254" s="T35">np:G</ta>
            <ta e="T37" id="Seg_1255" s="T36">0.3.h:A</ta>
            <ta e="T38" id="Seg_1256" s="T37">pro:P</ta>
            <ta e="T39" id="Seg_1257" s="T38">np:L 0.1.h:Poss</ta>
            <ta e="T43" id="Seg_1258" s="T42">0.3.h:A</ta>
            <ta e="T45" id="Seg_1259" s="T44">np.h:R</ta>
            <ta e="T46" id="Seg_1260" s="T45">pro.h:Poss</ta>
            <ta e="T47" id="Seg_1261" s="T46">np.h:E </ta>
            <ta e="T49" id="Seg_1262" s="T48">pro:A</ta>
            <ta e="T50" id="Seg_1263" s="T49">np:G</ta>
            <ta e="T54" id="Seg_1264" s="T53">np.h:Th</ta>
            <ta e="T56" id="Seg_1265" s="T55">np:Time</ta>
            <ta e="T58" id="Seg_1266" s="T57">pro.h:Poss</ta>
            <ta e="T59" id="Seg_1267" s="T58">np:L</ta>
            <ta e="T60" id="Seg_1268" s="T59">np:Time</ta>
            <ta e="T62" id="Seg_1269" s="T61">np:Time</ta>
            <ta e="T63" id="Seg_1270" s="T62">0.3:A</ta>
            <ta e="T65" id="Seg_1271" s="T64">np.h:P</ta>
            <ta e="T66" id="Seg_1272" s="T65">0.3.h:A</ta>
            <ta e="T67" id="Seg_1273" s="T66">pro:A</ta>
            <ta e="T68" id="Seg_1274" s="T67">np:Time</ta>
            <ta e="T72" id="Seg_1275" s="T71">np:Th</ta>
            <ta e="T75" id="Seg_1276" s="T74">np.h:Poss</ta>
            <ta e="T76" id="Seg_1277" s="T75">np:L</ta>
            <ta e="T77" id="Seg_1278" s="T76">0.3:Th</ta>
            <ta e="T78" id="Seg_1279" s="T77">np:L</ta>
            <ta e="T79" id="Seg_1280" s="T78">adv:Time</ta>
            <ta e="T81" id="Seg_1281" s="T80">np.h:Th</ta>
            <ta e="T86" id="Seg_1282" s="T85">np.h:A</ta>
            <ta e="T90" id="Seg_1283" s="T89">np.h:B</ta>
            <ta e="T91" id="Seg_1284" s="T90">v:Th</ta>
            <ta e="T93" id="Seg_1285" s="T92">np.h:Th</ta>
            <ta e="T94" id="Seg_1286" s="T93">adv:Time</ta>
            <ta e="T98" id="Seg_1287" s="T97">0.3:Th</ta>
            <ta e="T99" id="Seg_1288" s="T98">adv:Time</ta>
            <ta e="T101" id="Seg_1289" s="T100">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_1290" s="T3">np.h:S</ta>
            <ta e="T5" id="Seg_1291" s="T4">v:pred</ta>
            <ta e="T7" id="Seg_1292" s="T6">0.3.h:S v:pred</ta>
            <ta e="T9" id="Seg_1293" s="T8">np:O</ta>
            <ta e="T11" id="Seg_1294" s="T10">np:O</ta>
            <ta e="T13" id="Seg_1295" s="T12">0.1.h:S v:pred</ta>
            <ta e="T15" id="Seg_1296" s="T14">0.3.h:S v:pred</ta>
            <ta e="T17" id="Seg_1297" s="T16">0.1.h:S v:pred</ta>
            <ta e="T18" id="Seg_1298" s="T17">np.h:S</ta>
            <ta e="T20" id="Seg_1299" s="T19">v:pred</ta>
            <ta e="T21" id="Seg_1300" s="T20">0.3.h:S v:pred</ta>
            <ta e="T22" id="Seg_1301" s="T21">pro:S</ta>
            <ta e="T25" id="Seg_1302" s="T24">v:pred</ta>
            <ta e="T28" id="Seg_1303" s="T27">0.3.h:S v:pred</ta>
            <ta e="T32" id="Seg_1304" s="T31">np.h:S</ta>
            <ta e="T33" id="Seg_1305" s="T32">v:pred</ta>
            <ta e="T35" id="Seg_1306" s="T34">0.3.h:S v:pred</ta>
            <ta e="T37" id="Seg_1307" s="T36">0.3.h:S v:pred</ta>
            <ta e="T38" id="Seg_1308" s="T37">pro:S</ta>
            <ta e="T40" id="Seg_1309" s="T39">v:pred</ta>
            <ta e="T43" id="Seg_1310" s="T42">0.3.h:S v:pred</ta>
            <ta e="T47" id="Seg_1311" s="T46">np.h:S</ta>
            <ta e="T48" id="Seg_1312" s="T47">v:pred</ta>
            <ta e="T49" id="Seg_1313" s="T48">pro:S</ta>
            <ta e="T51" id="Seg_1314" s="T50">v:pred</ta>
            <ta e="T54" id="Seg_1315" s="T53">np.h:S</ta>
            <ta e="T57" id="Seg_1316" s="T56">v:pred</ta>
            <ta e="T63" id="Seg_1317" s="T62">0.3:S v:pred</ta>
            <ta e="T65" id="Seg_1318" s="T64">np.h:O</ta>
            <ta e="T66" id="Seg_1319" s="T65">0.3.h:S v:pred</ta>
            <ta e="T67" id="Seg_1320" s="T66">pro:S</ta>
            <ta e="T72" id="Seg_1321" s="T71">np:S</ta>
            <ta e="T73" id="Seg_1322" s="T72">v:pred</ta>
            <ta e="T77" id="Seg_1323" s="T76">0.3:S v:pred</ta>
            <ta e="T81" id="Seg_1324" s="T80">np.h:S</ta>
            <ta e="T82" id="Seg_1325" s="T81">adj:pred</ta>
            <ta e="T83" id="Seg_1326" s="T82">cop</ta>
            <ta e="T86" id="Seg_1327" s="T85">np.h:S</ta>
            <ta e="T88" id="Seg_1328" s="T87">v:pred</ta>
            <ta e="T91" id="Seg_1329" s="T90">v:O</ta>
            <ta e="T93" id="Seg_1330" s="T92">np.h:S</ta>
            <ta e="T96" id="Seg_1331" s="T95">v:pred</ta>
            <ta e="T98" id="Seg_1332" s="T97">0.3:S v:pred</ta>
            <ta e="T101" id="Seg_1333" s="T100">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T26" id="Seg_1334" s="T25">RUS:gram</ta>
            <ta e="T36" id="Seg_1335" s="T35">RUS:cult</ta>
            <ta e="T42" id="Seg_1336" s="T41">RUS:gram</ta>
            <ta e="T45" id="Seg_1337" s="T44">RUS:cult</ta>
            <ta e="T52" id="Seg_1338" s="T51">RUS:gram</ta>
            <ta e="T54" id="Seg_1339" s="T53">RUS:cult</ta>
            <ta e="T55" id="Seg_1340" s="T54">RUS:core</ta>
            <ta e="T62" id="Seg_1341" s="T61">RUS:cult</ta>
            <ta e="T86" id="Seg_1342" s="T85">RUS:cult</ta>
            <ta e="T100" id="Seg_1343" s="T99">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T86" id="Seg_1344" s="T85">Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T36" id="Seg_1345" s="T35">dir:infl</ta>
            <ta e="T45" id="Seg_1346" s="T44">dir:infl</ta>
            <ta e="T54" id="Seg_1347" s="T53">dir:bare</ta>
            <ta e="T62" id="Seg_1348" s="T61">dir:infl</ta>
            <ta e="T86" id="Seg_1349" s="T85">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_1350" s="T1">Once a man was sleeping in the taiga.</ta>
            <ta e="T13" id="Seg_1351" s="T5">Once he saw a dream: I was eating idleness(?).</ta>
            <ta e="T15" id="Seg_1352" s="T13">He woke up.</ta>
            <ta e="T20" id="Seg_1353" s="T15">Once I hear: an old woman is waking (me?).</ta>
            <ta e="T30" id="Seg_1354" s="T20">She says: “Something got into your mouth,” – she says to him, to that man.</ta>
            <ta e="T33" id="Seg_1355" s="T30">This man got angry.</ta>
            <ta e="T36" id="Seg_1356" s="T33">Once he went to a hospital.</ta>
            <ta e="T41" id="Seg_1357" s="T36">He said: “Something is burning in my stomach like a fire.”</ta>
            <ta e="T51" id="Seg_1358" s="T41">And he said to some professor: “My old woman saw, that something had got into my mouth.”</ta>
            <ta e="T59" id="Seg_1359" s="T51">This professor sat the whole night with him.</ta>
            <ta e="T63" id="Seg_1360" s="T59">In the morning at four o'clock it went out.</ta>
            <ta e="T70" id="Seg_1361" s="T63">He woke that man: it (had crawled out) at night, a snake.</ta>
            <ta e="T76" id="Seg_1362" s="T70">This snake had been living inside him.</ta>
            <ta e="T78" id="Seg_1363" s="T76">It was in his mouth.</ta>
            <ta e="T84" id="Seg_1364" s="T78">Then that man got better.</ta>
            <ta e="T91" id="Seg_1365" s="T84">This professor didn't let that man die.</ta>
            <ta e="T96" id="Seg_1366" s="T91">This man is still alive.</ta>
            <ta e="T101" id="Seg_1367" s="T96">It happened long ago and it's not true.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_1368" s="T1">Einst schlief ein Mann in der Taiga.</ta>
            <ta e="T13" id="Seg_1369" s="T5">Einmal sah er im Traum: Ich aß Untätigkeit(?).</ta>
            <ta e="T15" id="Seg_1370" s="T13">Er wachte auf.</ta>
            <ta e="T20" id="Seg_1371" s="T15">Einmal höre ich: eine alte Frau weckt (mich?).</ta>
            <ta e="T30" id="Seg_1372" s="T20">Sie sagt: "Etwas kam in deinen Mund", sagte sie zu ihm, zu diesem Mann.</ta>
            <ta e="T33" id="Seg_1373" s="T30">Dieser Mann wurde wütend.</ta>
            <ta e="T36" id="Seg_1374" s="T33">Einmal ging er ins Krankenhaus.</ta>
            <ta e="T41" id="Seg_1375" s="T36">Er sagte: "Etwas brennt in meinem Bauch wie Feuer."</ta>
            <ta e="T51" id="Seg_1376" s="T41">Und er sagte zu irgendeinem Professor: "Meine alte Frau sah, dass etwas in meinen Mund gekommen ist."</ta>
            <ta e="T59" id="Seg_1377" s="T51">Dieser Professor saß die ganze Nacht an seiner Seite.</ta>
            <ta e="T63" id="Seg_1378" s="T59">Am Morgen um vier Uhr kam es heraus.</ta>
            <ta e="T70" id="Seg_1379" s="T63">Er weckte diesen Mann: es (kroch) bei Nacht (heraus), eine Schlange.</ta>
            <ta e="T76" id="Seg_1380" s="T70">Diese Schlange hatte im Inneren des Mannes gelebt.</ta>
            <ta e="T78" id="Seg_1381" s="T76">Sie war in seinem Mund.</ta>
            <ta e="T84" id="Seg_1382" s="T78">Dann ging es diesem Mann besser.</ta>
            <ta e="T91" id="Seg_1383" s="T84">Dieser Professor ließ diesen Mann nicht sterben.</ta>
            <ta e="T96" id="Seg_1384" s="T91">Dieser Mann lebt jetzt noch.</ta>
            <ta e="T101" id="Seg_1385" s="T96">Das passierte vor langer Zeit und ist nicht wahr.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_1386" s="T1">Однажды в тайге человек спал.</ta>
            <ta e="T13" id="Seg_1387" s="T5">Однажды слышит (видит) сон: будто леня(?) я ем.</ta>
            <ta e="T15" id="Seg_1388" s="T13">Он проснулся.</ta>
            <ta e="T20" id="Seg_1389" s="T15">Однажды слышу, что женщина будит (меня?).</ta>
            <ta e="T30" id="Seg_1390" s="T20">Она говорит: “Кто-то в твой рот залез”, – ему говорит, этому человеку.</ta>
            <ta e="T33" id="Seg_1391" s="T30">Этот человек рассердился.</ta>
            <ta e="T36" id="Seg_1392" s="T33">Однажды пошел в больницу.</ta>
            <ta e="T41" id="Seg_1393" s="T36">Говорит: “Что-то в моем животе горит огнем”.</ta>
            <ta e="T51" id="Seg_1394" s="T41">И сказал какому-то профессору: “Моя старуха видела, как в мой рот кто-то залез”.</ta>
            <ta e="T59" id="Seg_1395" s="T51">Этот профессор всю ночь просидел около него.</ta>
            <ta e="T63" id="Seg_1396" s="T59">Утром в четыре часа вышел.</ta>
            <ta e="T70" id="Seg_1397" s="T63">Этого человека разбудил: ночью (выползла) змея.</ta>
            <ta e="T76" id="Seg_1398" s="T70">Эта змея жила внутри него.</ta>
            <ta e="T78" id="Seg_1399" s="T76">Жила во рту.</ta>
            <ta e="T84" id="Seg_1400" s="T78">Потом этот человек поправился.</ta>
            <ta e="T91" id="Seg_1401" s="T84">Этот профессор не дал этому человеку умереть.</ta>
            <ta e="T96" id="Seg_1402" s="T91">Этот человек сейчас живет.</ta>
            <ta e="T101" id="Seg_1403" s="T96">Это было давно и неправда.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_1404" s="T1">однажды в тайге человек спал</ta>
            <ta e="T13" id="Seg_1405" s="T5">однажды слышит (видит) сон что леня я ем</ta>
            <ta e="T15" id="Seg_1406" s="T13">разбудился</ta>
            <ta e="T20" id="Seg_1407" s="T15">однажды слышу женщина будит меня</ta>
            <ta e="T30" id="Seg_1408" s="T20">говорит кто-то в твой рот залез ему говорит этому человеку</ta>
            <ta e="T33" id="Seg_1409" s="T30">этот человек осердился</ta>
            <ta e="T36" id="Seg_1410" s="T33">однажды пошел в больницу</ta>
            <ta e="T41" id="Seg_1411" s="T36">говорит что-то в животе (нутро) горит огнем</ta>
            <ta e="T51" id="Seg_1412" s="T41">и сказал какому-то профессору моя баба видела как в мой рот кто-то залез</ta>
            <ta e="T59" id="Seg_1413" s="T51">этот профессор всю ночь просидел у него около</ta>
            <ta e="T63" id="Seg_1414" s="T59">утром в четыре часа вышел</ta>
            <ta e="T70" id="Seg_1415" s="T63">этого человека разбудил выползла змея</ta>
            <ta e="T76" id="Seg_1416" s="T70">эта змея жила у него в животе (внутри)</ta>
            <ta e="T78" id="Seg_1417" s="T76">жила во рту</ta>
            <ta e="T84" id="Seg_1418" s="T78">потом этот человек поправился (вылечился)</ta>
            <ta e="T91" id="Seg_1419" s="T84">этот профессор не дал ему умереть</ta>
            <ta e="T96" id="Seg_1420" s="T91">этот человек сейчас живет</ta>
            <ta e="T101" id="Seg_1421" s="T96">это было давно и не правда</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
