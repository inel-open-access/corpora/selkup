<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>BAG_1964_ItjaMousetrapped_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">BAG_1964_ItjaMousetrapped_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">41</ud-information>
            <ud-information attribute-name="# HIAT:w">31</ud-information>
            <ud-information attribute-name="# e">31</ud-information>
            <ud-information attribute-name="# HIAT:u">7</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="BAG">
            <abbreviation>BAG</abbreviation>
            <sex value="f" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="BAG"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T31" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Itʼe</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">imnʼassɨkkej</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">iːlаkustə</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Itʼe</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">palʼdʼukus</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">qwelalʼlʼе</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">i</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">suːrulʼlʼe</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">paldʼukus</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_35" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">Okkəmɨqаn</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">paldʼülʼewlə</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_42" n="HIAT:ip">(</nts>
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">nʼanna</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">čaːčakus</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">nild</ts>
                  <nts id="Seg_51" n="HIAT:ip">)</nts>
                  <nts id="Seg_52" n="HIAT:ip">.</nts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_55" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_57" n="HIAT:w" s="T14">tʼаŋоndə</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_60" n="HIAT:w" s="T15">miːtaŋ</ts>
                  <nts id="Seg_61" n="HIAT:ip">.</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_64" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_66" n="HIAT:w" s="T16">а</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_69" n="HIAT:w" s="T17">nʼanno</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_72" n="HIAT:w" s="T18">na</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_75" n="HIAT:w" s="T19">tʼaraŋ</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_78" n="HIAT:w" s="T20">qal</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_81" n="HIAT:w" s="T21">šɨndə</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_84" n="HIAT:w" s="T22">neldʼölčeːnǯaŋ</ts>
                  <nts id="Seg_85" n="HIAT:ip">.</nts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_88" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_90" n="HIAT:w" s="T23">I</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_93" n="HIAT:w" s="T24">naːtnо</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_96" n="HIAT:w" s="T25">tʼäraŋ</ts>
                  <nts id="Seg_97" n="HIAT:ip">:</nts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_100" n="HIAT:w" s="T26">neldʼölčеnǯaŋ</ts>
                  <nts id="Seg_101" n="HIAT:ip">.</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_104" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_106" n="HIAT:w" s="T27">Aj</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_109" n="HIAT:w" s="T28">pilʼеj</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_112" n="HIAT:w" s="T29">tоpan</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_115" n="HIAT:w" s="T30">neːldʼölǯеnǯaŋ</ts>
                  <nts id="Seg_116" n="HIAT:ip">.</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T31" id="Seg_118" n="sc" s="T0">
               <ts e="T1" id="Seg_120" n="e" s="T0">Itʼe </ts>
               <ts e="T2" id="Seg_122" n="e" s="T1">imnʼassɨkkej </ts>
               <ts e="T3" id="Seg_124" n="e" s="T2">iːlаkustə. </ts>
               <ts e="T4" id="Seg_126" n="e" s="T3">Itʼe </ts>
               <ts e="T5" id="Seg_128" n="e" s="T4">palʼdʼukus </ts>
               <ts e="T6" id="Seg_130" n="e" s="T5">qwelalʼlʼе </ts>
               <ts e="T7" id="Seg_132" n="e" s="T6">i </ts>
               <ts e="T8" id="Seg_134" n="e" s="T7">suːrulʼlʼe </ts>
               <ts e="T9" id="Seg_136" n="e" s="T8">paldʼukus. </ts>
               <ts e="T10" id="Seg_138" n="e" s="T9">Okkəmɨqаn </ts>
               <ts e="T11" id="Seg_140" n="e" s="T10">paldʼülʼewlə </ts>
               <ts e="T12" id="Seg_142" n="e" s="T11">(nʼanna </ts>
               <ts e="T13" id="Seg_144" n="e" s="T12">čaːčakus </ts>
               <ts e="T14" id="Seg_146" n="e" s="T13">nild). </ts>
               <ts e="T15" id="Seg_148" n="e" s="T14">tʼаŋоndə </ts>
               <ts e="T16" id="Seg_150" n="e" s="T15">miːtaŋ. </ts>
               <ts e="T17" id="Seg_152" n="e" s="T16">а </ts>
               <ts e="T18" id="Seg_154" n="e" s="T17">nʼanno </ts>
               <ts e="T19" id="Seg_156" n="e" s="T18">na </ts>
               <ts e="T20" id="Seg_158" n="e" s="T19">tʼaraŋ </ts>
               <ts e="T21" id="Seg_160" n="e" s="T20">qal </ts>
               <ts e="T22" id="Seg_162" n="e" s="T21">šɨndə </ts>
               <ts e="T23" id="Seg_164" n="e" s="T22">neldʼölčeːnǯaŋ. </ts>
               <ts e="T24" id="Seg_166" n="e" s="T23">I </ts>
               <ts e="T25" id="Seg_168" n="e" s="T24">naːtnо </ts>
               <ts e="T26" id="Seg_170" n="e" s="T25">tʼäraŋ: </ts>
               <ts e="T27" id="Seg_172" n="e" s="T26">neldʼölčеnǯaŋ. </ts>
               <ts e="T28" id="Seg_174" n="e" s="T27">Aj </ts>
               <ts e="T29" id="Seg_176" n="e" s="T28">pilʼеj </ts>
               <ts e="T30" id="Seg_178" n="e" s="T29">tоpan </ts>
               <ts e="T31" id="Seg_180" n="e" s="T30">neːldʼölǯеnǯaŋ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_181" s="T0">BAG_1964_ItjaMousetrapped_flk.001 (001.001)</ta>
            <ta e="T9" id="Seg_182" s="T3">BAG_1964_ItjaMousetrapped_flk.002 (001.002)</ta>
            <ta e="T14" id="Seg_183" s="T9">BAG_1964_ItjaMousetrapped_flk.003 (001.003)</ta>
            <ta e="T16" id="Seg_184" s="T14">BAG_1964_ItjaMousetrapped_flk.004 (001.004)</ta>
            <ta e="T23" id="Seg_185" s="T16">BAG_1964_ItjaMousetrapped_flk.005 (001.005)</ta>
            <ta e="T27" id="Seg_186" s="T23">BAG_1964_ItjaMousetrapped_flk.006 (001.006)</ta>
            <ta e="T31" id="Seg_187" s="T27">BAG_1964_ItjaMousetrapped_flk.007 (001.007)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_188" s="T0">Итʼе им′нʼäссыккей ′ӣlакусть</ta>
            <ta e="T9" id="Seg_189" s="T3">Итʼе ′палʼдʼукус kвälалʼлʼе и ′сӯрулʼлʼе паlдʼукус.</ta>
            <ta e="T14" id="Seg_190" s="T9">оккьрми′kан ′па̄лдʼӱ′лʼевлʼе ′нäнна ′тша̄тшакус ниlд</ta>
            <ta e="T16" id="Seg_191" s="T14">тʼаңондъ ′мӣтаң</ta>
            <ta e="T23" id="Seg_192" s="T16">а ′нäнно на тʼäр′аң kал ′шындъ ′ни(е)lдʼöl′тше̄нжаң</ta>
            <ta e="T27" id="Seg_193" s="T23">и ′на̄тно тä′раң: ′неlдʼöl′тшенжаң</ta>
            <ta e="T31" id="Seg_194" s="T27">ай пилʼей то′пан ′не̄lдʼöl′тшенжаң</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T3" id="Seg_195" s="T0">Itʼe imnʼässɨkkej iːlаkustə</ta>
            <ta e="T9" id="Seg_196" s="T3">Itʼe palʼdʼukus qwälalʼlʼе i suːrulʼlʼe paldʼukus</ta>
            <ta e="T14" id="Seg_197" s="T9">okkəmɨqаn paldʼülʼewlə (nʼänna tšaːtšakus nild)</ta>
            <ta e="T16" id="Seg_198" s="T14">tʼаŋоndə miːtaŋ</ta>
            <ta e="T23" id="Seg_199" s="T16">а nʼänno na tʼäraŋ qal šɨndə ni(e)ldʼöltšeːnǯaŋ</ta>
            <ta e="T27" id="Seg_200" s="T23">i naːtnо tʼäraŋ: nеldʼöltšеnǯaŋ</ta>
            <ta e="T31" id="Seg_201" s="T27">aj pilʼеj tоpan neːldʼölǯеnǯaŋ</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_202" s="T0">Itʼe imnʼassɨkkej iːlаkustə. </ta>
            <ta e="T9" id="Seg_203" s="T3">Itʼe palʼdʼukus qwelalʼlʼе i suːrulʼlʼe paldʼukus. </ta>
            <ta e="T14" id="Seg_204" s="T9">Okkəmɨqаn paldʼülʼewlə (nʼanna čaːčakus nild). </ta>
            <ta e="T16" id="Seg_205" s="T14">tʼаŋоndə miːtaŋ. </ta>
            <ta e="T23" id="Seg_206" s="T16">а nʼanno na tʼaraŋ qal šɨndə neldʼölčeːnǯaŋ. </ta>
            <ta e="T27" id="Seg_207" s="T23">I naːtnо tʼäraŋ: neldʼölčеnǯaŋ. </ta>
            <ta e="T31" id="Seg_208" s="T27">Aj pilʼеj tоpan neːldʼölǯеnǯaŋ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_209" s="T0">Itʼe</ta>
            <ta e="T2" id="Seg_210" s="T1">imnʼa-ssɨ-kkej</ta>
            <ta e="T3" id="Seg_211" s="T2">iːlа-ku-s-tə</ta>
            <ta e="T4" id="Seg_212" s="T3">Itʼe</ta>
            <ta e="T5" id="Seg_213" s="T4">palʼdʼu-ku-s</ta>
            <ta e="T6" id="Seg_214" s="T5">qwela-lʼ-lʼе</ta>
            <ta e="T7" id="Seg_215" s="T6">i</ta>
            <ta e="T8" id="Seg_216" s="T7">suːru-lʼ-lʼe</ta>
            <ta e="T9" id="Seg_217" s="T8">paldʼu-ku-s</ta>
            <ta e="T10" id="Seg_218" s="T9">okkə-mɨ-qаn</ta>
            <ta e="T11" id="Seg_219" s="T10">paldʼü-lʼewlə</ta>
            <ta e="T12" id="Seg_220" s="T11">nʼanna</ta>
            <ta e="T13" id="Seg_221" s="T12">čaːča-ku-s</ta>
            <ta e="T14" id="Seg_222" s="T13">nild</ta>
            <ta e="T15" id="Seg_223" s="T14">tʼаŋо-ndə</ta>
            <ta e="T16" id="Seg_224" s="T15">miːta-ŋ</ta>
            <ta e="T17" id="Seg_225" s="T16">а</ta>
            <ta e="T18" id="Seg_226" s="T17">nʼanno</ta>
            <ta e="T19" id="Seg_227" s="T18">na</ta>
            <ta e="T20" id="Seg_228" s="T19">tʼara-ŋ</ta>
            <ta e="T21" id="Seg_229" s="T20">qal</ta>
            <ta e="T22" id="Seg_230" s="T21">šɨndə</ta>
            <ta e="T23" id="Seg_231" s="T22">neldʼö-lčeː-nǯa-ŋ</ta>
            <ta e="T24" id="Seg_232" s="T23">i</ta>
            <ta e="T25" id="Seg_233" s="T24">naːtnо</ta>
            <ta e="T26" id="Seg_234" s="T25">tʼära-ŋ</ta>
            <ta e="T27" id="Seg_235" s="T26">neldʼö-lčе-nǯa-ŋ</ta>
            <ta e="T28" id="Seg_236" s="T27">aj</ta>
            <ta e="T29" id="Seg_237" s="T28">pilʼе-j</ta>
            <ta e="T30" id="Seg_238" s="T29">tоpa-n</ta>
            <ta e="T31" id="Seg_239" s="T30">neːldʼö-lǯе-nǯa-ŋ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_240" s="T0">Itʼe</ta>
            <ta e="T2" id="Seg_241" s="T1">imnʼa-sɨ-qi</ta>
            <ta e="T3" id="Seg_242" s="T2">ilɨ-kku-sɨ-tɨt</ta>
            <ta e="T4" id="Seg_243" s="T3">Itʼe</ta>
            <ta e="T5" id="Seg_244" s="T4">paldʼu-kku-sɨ</ta>
            <ta e="T6" id="Seg_245" s="T5">qwǝlɨ-j-le</ta>
            <ta e="T7" id="Seg_246" s="T6">i</ta>
            <ta e="T8" id="Seg_247" s="T7">suːrǝm-j-le</ta>
            <ta e="T9" id="Seg_248" s="T8">paldʼu-kku-sɨ</ta>
            <ta e="T10" id="Seg_249" s="T9">okkɨr-mɨ-qən</ta>
            <ta e="T11" id="Seg_250" s="T10">paldʼu-lewle</ta>
            <ta e="T12" id="Seg_251" s="T11">nʼannä</ta>
            <ta e="T13" id="Seg_252" s="T12">čaːǯɨ-kku-sɨ</ta>
            <ta e="T14" id="Seg_253" s="T13">nik</ta>
            <ta e="T15" id="Seg_254" s="T14">tаŋо-ndɨ</ta>
            <ta e="T16" id="Seg_255" s="T15">medɨ-ŋ</ta>
            <ta e="T17" id="Seg_256" s="T16">a</ta>
            <ta e="T18" id="Seg_257" s="T17">nʼannä</ta>
            <ta e="T19" id="Seg_258" s="T18">na</ta>
            <ta e="T20" id="Seg_259" s="T19">tʼarɨ-ŋ</ta>
            <ta e="T21" id="Seg_260" s="T20">qal</ta>
            <ta e="T22" id="Seg_261" s="T21">tašintɨ</ta>
            <ta e="T23" id="Seg_262" s="T22">nʼalʼdö-lʼčǝ-nǯɨ-ŋ</ta>
            <ta e="T24" id="Seg_263" s="T23">i</ta>
            <ta e="T25" id="Seg_264" s="T24">načʼa</ta>
            <ta e="T26" id="Seg_265" s="T25">tʼarɨ-ŋ</ta>
            <ta e="T27" id="Seg_266" s="T26">nʼalʼdö-lʼčǝ-nǯɨ-ŋ</ta>
            <ta e="T28" id="Seg_267" s="T27">aj</ta>
            <ta e="T29" id="Seg_268" s="T28">pɛläk-lʼ</ta>
            <ta e="T30" id="Seg_269" s="T29">topǝ-n</ta>
            <ta e="T31" id="Seg_270" s="T30">nʼalʼdö-lʼčǝ-nǯɨ-ŋ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_271" s="T0">Itja.[NOM]</ta>
            <ta e="T2" id="Seg_272" s="T1">brother-DYA-DU</ta>
            <ta e="T3" id="Seg_273" s="T2">live-HAB-PST-3PL</ta>
            <ta e="T4" id="Seg_274" s="T3">Itja.[NOM]</ta>
            <ta e="T5" id="Seg_275" s="T4">go-HAB-PST.[3SG.S]</ta>
            <ta e="T6" id="Seg_276" s="T5">fish-CAP-CVB</ta>
            <ta e="T7" id="Seg_277" s="T6">and</ta>
            <ta e="T8" id="Seg_278" s="T7">wild.animal-CAP-CVB</ta>
            <ta e="T9" id="Seg_279" s="T8">go-HAB-PST.[3SG.S]</ta>
            <ta e="T10" id="Seg_280" s="T9">one-something-LOC</ta>
            <ta e="T11" id="Seg_281" s="T10">go-CVB</ta>
            <ta e="T12" id="Seg_282" s="T11">forward</ta>
            <ta e="T13" id="Seg_283" s="T12">run-HAB-PST.[3SG.S]</ta>
            <ta e="T14" id="Seg_284" s="T13">so</ta>
            <ta e="T15" id="Seg_285" s="T14">trap-ILL</ta>
            <ta e="T16" id="Seg_286" s="T15">get.into-3SG.S</ta>
            <ta e="T17" id="Seg_287" s="T16">but</ta>
            <ta e="T18" id="Seg_288" s="T17">forward</ta>
            <ta e="T19" id="Seg_289" s="T18">this</ta>
            <ta e="T20" id="Seg_290" s="T19">say-3SG.S</ta>
            <ta e="T21" id="Seg_291" s="T20">it.is.said</ta>
            <ta e="T22" id="Seg_292" s="T21">you.SG.ACC</ta>
            <ta e="T23" id="Seg_293" s="T22">kick-PFV-FUT-1SG.S</ta>
            <ta e="T24" id="Seg_294" s="T23">and</ta>
            <ta e="T25" id="Seg_295" s="T24">there</ta>
            <ta e="T26" id="Seg_296" s="T25">say-3SG.S</ta>
            <ta e="T27" id="Seg_297" s="T26">kick-PFV-FUT-1SG.S</ta>
            <ta e="T28" id="Seg_298" s="T27">again</ta>
            <ta e="T29" id="Seg_299" s="T28">side-ADJZ</ta>
            <ta e="T30" id="Seg_300" s="T29">leg-GEN</ta>
            <ta e="T31" id="Seg_301" s="T30">kick-PFV-FUT-3SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_302" s="T0">Итя.[NOM]</ta>
            <ta e="T2" id="Seg_303" s="T1">брат-DYA-DU</ta>
            <ta e="T3" id="Seg_304" s="T2">жить-HAB-PST-3PL</ta>
            <ta e="T4" id="Seg_305" s="T3">Итя.[NOM]</ta>
            <ta e="T5" id="Seg_306" s="T4">ходить-HAB-PST.[3SG.S]</ta>
            <ta e="T6" id="Seg_307" s="T5">рыба-CAP-CVB</ta>
            <ta e="T7" id="Seg_308" s="T6">и</ta>
            <ta e="T8" id="Seg_309" s="T7">зверь-CAP-CVB</ta>
            <ta e="T9" id="Seg_310" s="T8">ходить-HAB-PST.[3SG.S]</ta>
            <ta e="T10" id="Seg_311" s="T9">один-нечто-LOC</ta>
            <ta e="T11" id="Seg_312" s="T10">ходить-CVB</ta>
            <ta e="T12" id="Seg_313" s="T11">вперёд</ta>
            <ta e="T13" id="Seg_314" s="T12">бегать-HAB-PST.[3SG.S]</ta>
            <ta e="T14" id="Seg_315" s="T13">так</ta>
            <ta e="T15" id="Seg_316" s="T14">ловушка-ILL</ta>
            <ta e="T16" id="Seg_317" s="T15">попасть-3SG.S</ta>
            <ta e="T17" id="Seg_318" s="T16">а</ta>
            <ta e="T18" id="Seg_319" s="T17">вперёд</ta>
            <ta e="T19" id="Seg_320" s="T18">этот</ta>
            <ta e="T20" id="Seg_321" s="T19">сказать-3SG.S</ta>
            <ta e="T21" id="Seg_322" s="T20">мол</ta>
            <ta e="T22" id="Seg_323" s="T21">ты.ACC</ta>
            <ta e="T23" id="Seg_324" s="T22">стоптать-PFV-FUT-1SG.S</ta>
            <ta e="T24" id="Seg_325" s="T23">и</ta>
            <ta e="T25" id="Seg_326" s="T24">там</ta>
            <ta e="T26" id="Seg_327" s="T25">сказать-3SG.S</ta>
            <ta e="T27" id="Seg_328" s="T26">стоптать-PFV-FUT-1SG.S</ta>
            <ta e="T28" id="Seg_329" s="T27">опять</ta>
            <ta e="T29" id="Seg_330" s="T28">сторона-ADJZ</ta>
            <ta e="T30" id="Seg_331" s="T29">нога-GEN</ta>
            <ta e="T31" id="Seg_332" s="T30">стоптать-PFV-FUT-3SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_333" s="T0">nprop-n:case</ta>
            <ta e="T2" id="Seg_334" s="T1">n-n&gt;n-n:num</ta>
            <ta e="T3" id="Seg_335" s="T2">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_336" s="T3">nprop-n:case</ta>
            <ta e="T5" id="Seg_337" s="T4">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_338" s="T5">n-n&gt;v-v&gt;adv</ta>
            <ta e="T7" id="Seg_339" s="T6">conj</ta>
            <ta e="T8" id="Seg_340" s="T7">n-n&gt;v-v&gt;adv</ta>
            <ta e="T9" id="Seg_341" s="T8">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_342" s="T9">num-n-n:case</ta>
            <ta e="T11" id="Seg_343" s="T10">v-v&gt;adv</ta>
            <ta e="T12" id="Seg_344" s="T11">adv</ta>
            <ta e="T13" id="Seg_345" s="T12">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_346" s="T13">adv</ta>
            <ta e="T15" id="Seg_347" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_348" s="T15">v-v:pn</ta>
            <ta e="T17" id="Seg_349" s="T16">conj</ta>
            <ta e="T18" id="Seg_350" s="T17">adv</ta>
            <ta e="T19" id="Seg_351" s="T18">dem</ta>
            <ta e="T20" id="Seg_352" s="T19">v-v:pn</ta>
            <ta e="T21" id="Seg_353" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_354" s="T21">pers</ta>
            <ta e="T23" id="Seg_355" s="T22">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_356" s="T23">conj</ta>
            <ta e="T25" id="Seg_357" s="T24">adv</ta>
            <ta e="T26" id="Seg_358" s="T25">v-v:pn</ta>
            <ta e="T27" id="Seg_359" s="T26">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_360" s="T27">adv</ta>
            <ta e="T29" id="Seg_361" s="T28">n-n&gt;adj</ta>
            <ta e="T30" id="Seg_362" s="T29">n-n:case</ta>
            <ta e="T31" id="Seg_363" s="T30">v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_364" s="T0">nprop</ta>
            <ta e="T2" id="Seg_365" s="T1">n</ta>
            <ta e="T3" id="Seg_366" s="T2">v</ta>
            <ta e="T4" id="Seg_367" s="T3">nprop</ta>
            <ta e="T5" id="Seg_368" s="T4">v</ta>
            <ta e="T6" id="Seg_369" s="T5">adv</ta>
            <ta e="T7" id="Seg_370" s="T6">conj</ta>
            <ta e="T8" id="Seg_371" s="T7">adv</ta>
            <ta e="T9" id="Seg_372" s="T8">v</ta>
            <ta e="T10" id="Seg_373" s="T9">adv</ta>
            <ta e="T11" id="Seg_374" s="T10">adv</ta>
            <ta e="T12" id="Seg_375" s="T11">adv</ta>
            <ta e="T13" id="Seg_376" s="T12">v</ta>
            <ta e="T14" id="Seg_377" s="T13">adv</ta>
            <ta e="T15" id="Seg_378" s="T14">n</ta>
            <ta e="T16" id="Seg_379" s="T15">v</ta>
            <ta e="T17" id="Seg_380" s="T16">conj</ta>
            <ta e="T18" id="Seg_381" s="T17">adv</ta>
            <ta e="T19" id="Seg_382" s="T18">dem</ta>
            <ta e="T20" id="Seg_383" s="T19">v</ta>
            <ta e="T21" id="Seg_384" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_385" s="T21">pers</ta>
            <ta e="T23" id="Seg_386" s="T22">v</ta>
            <ta e="T24" id="Seg_387" s="T23">conj</ta>
            <ta e="T25" id="Seg_388" s="T24">adv</ta>
            <ta e="T26" id="Seg_389" s="T25">v</ta>
            <ta e="T27" id="Seg_390" s="T26">v</ta>
            <ta e="T28" id="Seg_391" s="T27">conj</ta>
            <ta e="T29" id="Seg_392" s="T28">adj</ta>
            <ta e="T30" id="Seg_393" s="T29">n</ta>
            <ta e="T31" id="Seg_394" s="T30">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_395" s="T0">np.h:S</ta>
            <ta e="T3" id="Seg_396" s="T2">v:pred</ta>
            <ta e="T4" id="Seg_397" s="T3">np.h:S</ta>
            <ta e="T5" id="Seg_398" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_399" s="T5">s:purp</ta>
            <ta e="T8" id="Seg_400" s="T7">s:purp</ta>
            <ta e="T9" id="Seg_401" s="T8">0.3.h:S v:pred</ta>
            <ta e="T11" id="Seg_402" s="T10">s:temp</ta>
            <ta e="T13" id="Seg_403" s="T12">0.3.h:S v:pred</ta>
            <ta e="T16" id="Seg_404" s="T15">0.3.h:S v:pred</ta>
            <ta e="T20" id="Seg_405" s="T19">0.3.h:S v:pred</ta>
            <ta e="T22" id="Seg_406" s="T21">pro.h:O</ta>
            <ta e="T23" id="Seg_407" s="T22">0.1.h:S v:pred</ta>
            <ta e="T26" id="Seg_408" s="T25">0.3.h:S v:pred</ta>
            <ta e="T27" id="Seg_409" s="T26">0.1.h:S v:pred</ta>
            <ta e="T31" id="Seg_410" s="T30">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_411" s="T0">np.h:Th</ta>
            <ta e="T2" id="Seg_412" s="T1">np:Com</ta>
            <ta e="T4" id="Seg_413" s="T3">np.h:A</ta>
            <ta e="T9" id="Seg_414" s="T8">0.3.h:A</ta>
            <ta e="T13" id="Seg_415" s="T12">0.3.h:A</ta>
            <ta e="T15" id="Seg_416" s="T14">np:G</ta>
            <ta e="T16" id="Seg_417" s="T15">0.3.h:P</ta>
            <ta e="T18" id="Seg_418" s="T17">adv:G</ta>
            <ta e="T20" id="Seg_419" s="T19">0.3.h:A</ta>
            <ta e="T22" id="Seg_420" s="T21">pro.h:P</ta>
            <ta e="T23" id="Seg_421" s="T22">0.1.h:A</ta>
            <ta e="T25" id="Seg_422" s="T24">adv:L</ta>
            <ta e="T26" id="Seg_423" s="T25">0.3.h:A</ta>
            <ta e="T27" id="Seg_424" s="T26">0.1.h:A</ta>
            <ta e="T30" id="Seg_425" s="T29">np:Ins</ta>
            <ta e="T31" id="Seg_426" s="T30">0.3.h:A</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T7" id="Seg_427" s="T6">RUS:gram</ta>
            <ta e="T17" id="Seg_428" s="T16">RUS:gram</ta>
            <ta e="T24" id="Seg_429" s="T23">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_430" s="T0">Жил Итя с братом (/с братьями).</ta>
            <ta e="T9" id="Seg_431" s="T3">Итя ходил рыбачить и охотиться ходил.</ta>
            <ta e="T14" id="Seg_432" s="T9">Однажды, походив, туда пришел так.</ta>
            <ta e="T16" id="Seg_433" s="T14">…В ловушку попал.</ta>
            <ta e="T23" id="Seg_434" s="T16">А потом говорит: мол, я тебя пну.</ta>
            <ta e="T27" id="Seg_435" s="T23">И еще говорит: пну.</ta>
            <ta e="T31" id="Seg_436" s="T27">И одной ногой пнул.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_437" s="T0">Itja lived with his brother(s).</ta>
            <ta e="T9" id="Seg_438" s="T3">Itja went fishing and hunting.</ta>
            <ta e="T14" id="Seg_439" s="T9">Once after a hunt he went there.</ta>
            <ta e="T16" id="Seg_440" s="T14">…He falls into a trap.</ta>
            <ta e="T23" id="Seg_441" s="T16">And he says: I will kick you.</ta>
            <ta e="T27" id="Seg_442" s="T23">And again he says: I will kick (you).</ta>
            <ta e="T31" id="Seg_443" s="T27">Again he kicks with one foot. </ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_444" s="T0">Itja lebte mit seinem Bruder (/mit seinen Brüdern).</ta>
            <ta e="T9" id="Seg_445" s="T3">Itja ging fischen und jagen. </ta>
            <ta e="T14" id="Seg_446" s="T9">Einmal nach der Jagd kam er dorthin. </ta>
            <ta e="T16" id="Seg_447" s="T14">…Er ist in eine Falle geraten.</ta>
            <ta e="T23" id="Seg_448" s="T16">Und er sagt: "Ich trete dich."</ta>
            <ta e="T27" id="Seg_449" s="T23">Und wieder sagt er: "Ich stoße dich."</ta>
            <ta e="T31" id="Seg_450" s="T27">Und hat mit dem Fuß getreten.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_451" s="T0">Итя с братьями жил.</ta>
            <ta e="T9" id="Seg_452" s="T3">Итя ходил рыбачить</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T14" id="Seg_453" s="T9">There is no Russian translation from here.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
