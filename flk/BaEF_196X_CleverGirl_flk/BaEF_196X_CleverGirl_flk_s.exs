<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>BaEF_196X_CleverGirl_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">BaEF_196X_CleverGirl_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">151</ud-information>
            <ud-information attribute-name="# HIAT:w">114</ud-information>
            <ud-information attribute-name="# e">114</ud-information>
            <ud-information attribute-name="# HIAT:u">18</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="BaEF">
            <abbreviation>BaEF</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="BaEF"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T115" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Täjze</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6" n="HIAT:ip">(</nts>
                  <ts e="T3" id="Seg_8" n="HIAT:w" s="T2">swa</ts>
                  <nts id="Seg_9" n="HIAT:ip">)</nts>
                  <nts id="Seg_10" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_12" n="HIAT:w" s="T3">nädeŋ</ts>
                  <nts id="Seg_13" n="HIAT:ip">.</nts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_16" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_18" n="HIAT:w" s="T4">Soː</ts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_21" n="HIAT:w" s="T5">ɣunnan</ts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_24" n="HIAT:w" s="T6">it</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_27" n="HIAT:w" s="T7">epant</ts>
                  <nts id="Seg_28" n="HIAT:ip">.</nts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_31" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_33" n="HIAT:w" s="T8">Iːt</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_36" n="HIAT:w" s="T9">swaː</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_39" n="HIAT:w" s="T10">eːppandan</ts>
                  <nts id="Seg_40" n="HIAT:ip">.</nts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_43" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_45" n="HIAT:w" s="T11">Wes</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_48" n="HIAT:w" s="T12">meːspɨmbat</ts>
                  <nts id="Seg_49" n="HIAT:ip">.</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_52" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_54" n="HIAT:w" s="T13">Poːɣɨndo</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_57" n="HIAT:w" s="T14">meːspɨqumbat</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_60" n="HIAT:w" s="T15">säqij</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_62" n="HIAT:ip">(</nts>
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">wɨːresqalam</ts>
                  <nts id="Seg_65" n="HIAT:ip">)</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_68" n="HIAT:w" s="T17">mɨlam</ts>
                  <nts id="Seg_69" n="HIAT:ip">:</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_72" n="HIAT:w" s="T18">qoptɨla</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_75" n="HIAT:w" s="T19">meːqomɨndat</ts>
                  <nts id="Seg_76" n="HIAT:ip">,</nts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_79" n="HIAT:w" s="T20">qoptɨn</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_82" n="HIAT:w" s="T21">tobɨlam</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_85" n="HIAT:w" s="T22">tačʼinqumbat</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">i</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">qoptɨn</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_94" n="HIAT:w" s="T25">ollam</ts>
                  <nts id="Seg_95" n="HIAT:ip">.</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_98" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_100" n="HIAT:w" s="T26">Sʼäqij</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_103" n="HIAT:w" s="T27">matelʼe</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_106" n="HIAT:w" s="T28">meːqumbat</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_109" n="HIAT:w" s="T29">i</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_112" n="HIAT:w" s="T30">poːrlam</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_115" n="HIAT:w" s="T31">näj</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_118" n="HIAT:w" s="T32">wsʼaqij</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_121" n="HIAT:w" s="T33">maʒelgumbat</ts>
                  <nts id="Seg_122" n="HIAT:ip">,</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_125" n="HIAT:w" s="T34">meːqumbat</ts>
                  <nts id="Seg_126" n="HIAT:ip">.</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_129" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_131" n="HIAT:w" s="T35">Arɨn</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_134" n="HIAT:w" s="T36">qaːdarɨqɨndo</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_137" n="HIAT:w" s="T37">tüːqumbattə</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_140" n="HIAT:w" s="T38">tau</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_143" n="HIAT:w" s="T39">qugu</ts>
                  <nts id="Seg_144" n="HIAT:ip">.</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_147" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_149" n="HIAT:w" s="T40">Iːɣendə</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_152" n="HIAT:w" s="T41">medɨmbaq</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_155" n="HIAT:w" s="T42">para</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_158" n="HIAT:w" s="T43">nädɨgu</ts>
                  <nts id="Seg_159" n="HIAT:ip">.</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_162" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_164" n="HIAT:w" s="T44">Iːt</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_167" n="HIAT:w" s="T45">qɨgelɨmba</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_170" n="HIAT:w" s="T46">nädɨgu</ts>
                  <nts id="Seg_171" n="HIAT:ip">.</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_174" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_176" n="HIAT:w" s="T47">Tawarišqantne</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_179" n="HIAT:w" s="T48">äʒolgumba</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_182" n="HIAT:w" s="T49">nädem</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_185" n="HIAT:w" s="T50">peːɣalgu</ts>
                  <nts id="Seg_186" n="HIAT:ip">,</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_189" n="HIAT:w" s="T51">swaːlaɣi</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_192" n="HIAT:w" s="T52">nädäm</ts>
                  <nts id="Seg_193" n="HIAT:ip">.</nts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_196" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_198" n="HIAT:w" s="T53">Na</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_201" n="HIAT:w" s="T54">tawariš</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_204" n="HIAT:w" s="T55">qwanba</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_207" n="HIAT:w" s="T56">nädem</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_210" n="HIAT:w" s="T57">peːɣalgu</ts>
                  <nts id="Seg_211" n="HIAT:ip">.</nts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_214" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_216" n="HIAT:w" s="T58">Sət</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_219" n="HIAT:w" s="T59">tʼet</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_222" n="HIAT:w" s="T60">qraɣɨn</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_225" n="HIAT:w" s="T61">eppa</ts>
                  <nts id="Seg_226" n="HIAT:ip">,</nts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_229" n="HIAT:w" s="T62">swaː</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_232" n="HIAT:w" s="T63">nädäm</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_235" n="HIAT:w" s="T64">as</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_238" n="HIAT:w" s="T65">qombat</ts>
                  <nts id="Seg_239" n="HIAT:ip">.</nts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_242" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_244" n="HIAT:w" s="T66">Qʼödəmǯelʼi</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_247" n="HIAT:w" s="T67">jedəɣɨn</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_250" n="HIAT:w" s="T68">qombat</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_253" n="HIAT:w" s="T69">swaː</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_256" n="HIAT:w" s="T70">nädäm</ts>
                  <nts id="Seg_257" n="HIAT:ip">.</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_260" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_262" n="HIAT:w" s="T71">Swa</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_265" n="HIAT:w" s="T72">depqa</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_268" n="HIAT:w" s="T73">qomandɨt</ts>
                  <nts id="Seg_269" n="HIAT:ip">,</nts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_272" n="HIAT:w" s="T74">täjze</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_275" n="HIAT:w" s="T75">nädäm</ts>
                  <nts id="Seg_276" n="HIAT:ip">.</nts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_279" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_281" n="HIAT:w" s="T76">Qwessʼe</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_284" n="HIAT:w" s="T77">tümba</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_287" n="HIAT:w" s="T78">jeːdent</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_290" n="HIAT:w" s="T79">i</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_293" n="HIAT:w" s="T80">čenčaŋ</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_296" n="HIAT:w" s="T81">što</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_299" n="HIAT:w" s="T82">qoow</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_302" n="HIAT:w" s="T83">nädem</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_305" n="HIAT:w" s="T84">swaː</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_308" n="HIAT:w" s="T85">i</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_311" n="HIAT:w" s="T86">täjze</ts>
                  <nts id="Seg_312" n="HIAT:ip">.</nts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_315" n="HIAT:u" s="T87">
                  <nts id="Seg_316" n="HIAT:ip">“</nts>
                  <ts e="T88" id="Seg_318" n="HIAT:w" s="T87">Tan</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_321" n="HIAT:w" s="T88">iːndane</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_324" n="HIAT:w" s="T89">swaː</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_327" n="HIAT:w" s="T90">paja</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_330" n="HIAT:w" s="T91">jenɨnǯan</ts>
                  <nts id="Seg_331" n="HIAT:ip">.</nts>
                  <nts id="Seg_332" n="HIAT:ip">”</nts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T104" id="Seg_335" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_337" n="HIAT:w" s="T92">Ezew</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_340" n="HIAT:w" s="T93">täːrbɨlʼe</ts>
                  <nts id="Seg_341" n="HIAT:ip">,</nts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_344" n="HIAT:w" s="T94">tʼärbɨlʼe</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_347" n="HIAT:w" s="T95">i</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_350" n="HIAT:w" s="T96">tʼärɨŋ</ts>
                  <nts id="Seg_351" n="HIAT:ip">:</nts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_353" n="HIAT:ip">“</nts>
                  <ts e="T98" id="Seg_355" n="HIAT:w" s="T97">Qostɨlaj</ts>
                  <nts id="Seg_356" n="HIAT:ip">,</nts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_359" n="HIAT:w" s="T98">täjze</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_362" n="HIAT:w" s="T99">jeŋ</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_365" n="HIAT:w" s="T100">na</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_368" n="HIAT:w" s="T101">nädäŋ</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_371" n="HIAT:w" s="T102">alʼi</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_374" n="HIAT:w" s="T103">ass</ts>
                  <nts id="Seg_375" n="HIAT:ip">.</nts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_378" n="HIAT:u" s="T104">
                  <ts e="T105" id="Seg_380" n="HIAT:w" s="T104">Üːdəlaj</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_383" n="HIAT:w" s="T105">näjaqum</ts>
                  <nts id="Seg_384" n="HIAT:ip">,</nts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_387" n="HIAT:w" s="T106">štob</ts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_390" n="HIAT:w" s="T107">qöːsqügu</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_393" n="HIAT:w" s="T108">täbɨnä</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_396" n="HIAT:w" s="T109">na</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_399" n="HIAT:w" s="T110">nädäne</ts>
                  <nts id="Seg_400" n="HIAT:ip">,</nts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_403" n="HIAT:w" s="T111">čenčugu</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_406" n="HIAT:w" s="T112">täbɨnenǯiŋ</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_409" n="HIAT:w" s="T113">alʼi</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_412" n="HIAT:w" s="T114">ass</ts>
                  <nts id="Seg_413" n="HIAT:ip">.</nts>
                  <nts id="Seg_414" n="HIAT:ip">”</nts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T115" id="Seg_416" n="sc" s="T1">
               <ts e="T2" id="Seg_418" n="e" s="T1">Täjze </ts>
               <ts e="T3" id="Seg_420" n="e" s="T2">(swa) </ts>
               <ts e="T4" id="Seg_422" n="e" s="T3">nädeŋ. </ts>
               <ts e="T5" id="Seg_424" n="e" s="T4">Soː </ts>
               <ts e="T6" id="Seg_426" n="e" s="T5">ɣunnan </ts>
               <ts e="T7" id="Seg_428" n="e" s="T6">it </ts>
               <ts e="T8" id="Seg_430" n="e" s="T7">epant. </ts>
               <ts e="T9" id="Seg_432" n="e" s="T8">Iːt </ts>
               <ts e="T10" id="Seg_434" n="e" s="T9">swaː </ts>
               <ts e="T11" id="Seg_436" n="e" s="T10">eːppandan. </ts>
               <ts e="T12" id="Seg_438" n="e" s="T11">Wes </ts>
               <ts e="T13" id="Seg_440" n="e" s="T12">meːspɨmbat. </ts>
               <ts e="T14" id="Seg_442" n="e" s="T13">Poːɣɨndo </ts>
               <ts e="T15" id="Seg_444" n="e" s="T14">meːspɨqumbat </ts>
               <ts e="T16" id="Seg_446" n="e" s="T15">säqij </ts>
               <ts e="T17" id="Seg_448" n="e" s="T16">(wɨːresqalam) </ts>
               <ts e="T18" id="Seg_450" n="e" s="T17">mɨlam: </ts>
               <ts e="T19" id="Seg_452" n="e" s="T18">qoptɨla </ts>
               <ts e="T20" id="Seg_454" n="e" s="T19">meːqomɨndat, </ts>
               <ts e="T21" id="Seg_456" n="e" s="T20">qoptɨn </ts>
               <ts e="T22" id="Seg_458" n="e" s="T21">tobɨlam </ts>
               <ts e="T23" id="Seg_460" n="e" s="T22">tačʼinqumbat </ts>
               <ts e="T24" id="Seg_462" n="e" s="T23">i </ts>
               <ts e="T25" id="Seg_464" n="e" s="T24">qoptɨn </ts>
               <ts e="T26" id="Seg_466" n="e" s="T25">ollam. </ts>
               <ts e="T27" id="Seg_468" n="e" s="T26">Sʼäqij </ts>
               <ts e="T28" id="Seg_470" n="e" s="T27">matelʼe </ts>
               <ts e="T29" id="Seg_472" n="e" s="T28">meːqumbat </ts>
               <ts e="T30" id="Seg_474" n="e" s="T29">i </ts>
               <ts e="T31" id="Seg_476" n="e" s="T30">poːrlam </ts>
               <ts e="T32" id="Seg_478" n="e" s="T31">näj </ts>
               <ts e="T33" id="Seg_480" n="e" s="T32">wsʼaqij </ts>
               <ts e="T34" id="Seg_482" n="e" s="T33">maʒelgumbat, </ts>
               <ts e="T35" id="Seg_484" n="e" s="T34">meːqumbat. </ts>
               <ts e="T36" id="Seg_486" n="e" s="T35">Arɨn </ts>
               <ts e="T37" id="Seg_488" n="e" s="T36">qaːdarɨqɨndo </ts>
               <ts e="T38" id="Seg_490" n="e" s="T37">tüːqumbattə </ts>
               <ts e="T39" id="Seg_492" n="e" s="T38">tau </ts>
               <ts e="T40" id="Seg_494" n="e" s="T39">qugu. </ts>
               <ts e="T41" id="Seg_496" n="e" s="T40">Iːɣendə </ts>
               <ts e="T42" id="Seg_498" n="e" s="T41">medɨmbaq </ts>
               <ts e="T43" id="Seg_500" n="e" s="T42">para </ts>
               <ts e="T44" id="Seg_502" n="e" s="T43">nädɨgu. </ts>
               <ts e="T45" id="Seg_504" n="e" s="T44">Iːt </ts>
               <ts e="T46" id="Seg_506" n="e" s="T45">qɨgelɨmba </ts>
               <ts e="T47" id="Seg_508" n="e" s="T46">nädɨgu. </ts>
               <ts e="T48" id="Seg_510" n="e" s="T47">Tawarišqantne </ts>
               <ts e="T49" id="Seg_512" n="e" s="T48">äʒolgumba </ts>
               <ts e="T50" id="Seg_514" n="e" s="T49">nädem </ts>
               <ts e="T51" id="Seg_516" n="e" s="T50">peːɣalgu, </ts>
               <ts e="T52" id="Seg_518" n="e" s="T51">swaːlaɣi </ts>
               <ts e="T53" id="Seg_520" n="e" s="T52">nädäm. </ts>
               <ts e="T54" id="Seg_522" n="e" s="T53">Na </ts>
               <ts e="T55" id="Seg_524" n="e" s="T54">tawariš </ts>
               <ts e="T56" id="Seg_526" n="e" s="T55">qwanba </ts>
               <ts e="T57" id="Seg_528" n="e" s="T56">nädem </ts>
               <ts e="T58" id="Seg_530" n="e" s="T57">peːɣalgu. </ts>
               <ts e="T59" id="Seg_532" n="e" s="T58">Sət </ts>
               <ts e="T60" id="Seg_534" n="e" s="T59">tʼet </ts>
               <ts e="T61" id="Seg_536" n="e" s="T60">qraɣɨn </ts>
               <ts e="T62" id="Seg_538" n="e" s="T61">eppa, </ts>
               <ts e="T63" id="Seg_540" n="e" s="T62">swaː </ts>
               <ts e="T64" id="Seg_542" n="e" s="T63">nädäm </ts>
               <ts e="T65" id="Seg_544" n="e" s="T64">as </ts>
               <ts e="T66" id="Seg_546" n="e" s="T65">qombat. </ts>
               <ts e="T67" id="Seg_548" n="e" s="T66">Qʼödəmǯelʼi </ts>
               <ts e="T68" id="Seg_550" n="e" s="T67">jedəɣɨn </ts>
               <ts e="T69" id="Seg_552" n="e" s="T68">qombat </ts>
               <ts e="T70" id="Seg_554" n="e" s="T69">swaː </ts>
               <ts e="T71" id="Seg_556" n="e" s="T70">nädäm. </ts>
               <ts e="T72" id="Seg_558" n="e" s="T71">Swa </ts>
               <ts e="T73" id="Seg_560" n="e" s="T72">depqa </ts>
               <ts e="T74" id="Seg_562" n="e" s="T73">qomandɨt, </ts>
               <ts e="T75" id="Seg_564" n="e" s="T74">täjze </ts>
               <ts e="T76" id="Seg_566" n="e" s="T75">nädäm. </ts>
               <ts e="T77" id="Seg_568" n="e" s="T76">Qwessʼe </ts>
               <ts e="T78" id="Seg_570" n="e" s="T77">tümba </ts>
               <ts e="T79" id="Seg_572" n="e" s="T78">jeːdent </ts>
               <ts e="T80" id="Seg_574" n="e" s="T79">i </ts>
               <ts e="T81" id="Seg_576" n="e" s="T80">čenčaŋ </ts>
               <ts e="T82" id="Seg_578" n="e" s="T81">što </ts>
               <ts e="T83" id="Seg_580" n="e" s="T82">qoow </ts>
               <ts e="T84" id="Seg_582" n="e" s="T83">nädem </ts>
               <ts e="T85" id="Seg_584" n="e" s="T84">swaː </ts>
               <ts e="T86" id="Seg_586" n="e" s="T85">i </ts>
               <ts e="T87" id="Seg_588" n="e" s="T86">täjze. </ts>
               <ts e="T88" id="Seg_590" n="e" s="T87">“Tan </ts>
               <ts e="T89" id="Seg_592" n="e" s="T88">iːndane </ts>
               <ts e="T90" id="Seg_594" n="e" s="T89">swaː </ts>
               <ts e="T91" id="Seg_596" n="e" s="T90">paja </ts>
               <ts e="T92" id="Seg_598" n="e" s="T91">jenɨnǯan.” </ts>
               <ts e="T93" id="Seg_600" n="e" s="T92">Ezew </ts>
               <ts e="T94" id="Seg_602" n="e" s="T93">täːrbɨlʼe, </ts>
               <ts e="T95" id="Seg_604" n="e" s="T94">tʼärbɨlʼe </ts>
               <ts e="T96" id="Seg_606" n="e" s="T95">i </ts>
               <ts e="T97" id="Seg_608" n="e" s="T96">tʼärɨŋ: </ts>
               <ts e="T98" id="Seg_610" n="e" s="T97">“Qostɨlaj, </ts>
               <ts e="T99" id="Seg_612" n="e" s="T98">täjze </ts>
               <ts e="T100" id="Seg_614" n="e" s="T99">jeŋ </ts>
               <ts e="T101" id="Seg_616" n="e" s="T100">na </ts>
               <ts e="T102" id="Seg_618" n="e" s="T101">nädäŋ </ts>
               <ts e="T103" id="Seg_620" n="e" s="T102">alʼi </ts>
               <ts e="T104" id="Seg_622" n="e" s="T103">ass. </ts>
               <ts e="T105" id="Seg_624" n="e" s="T104">Üːdəlaj </ts>
               <ts e="T106" id="Seg_626" n="e" s="T105">näjaqum, </ts>
               <ts e="T107" id="Seg_628" n="e" s="T106">štob </ts>
               <ts e="T108" id="Seg_630" n="e" s="T107">qöːsqügu </ts>
               <ts e="T109" id="Seg_632" n="e" s="T108">täbɨnä </ts>
               <ts e="T110" id="Seg_634" n="e" s="T109">na </ts>
               <ts e="T111" id="Seg_636" n="e" s="T110">nädäne, </ts>
               <ts e="T112" id="Seg_638" n="e" s="T111">čenčugu </ts>
               <ts e="T113" id="Seg_640" n="e" s="T112">täbɨnenǯiŋ </ts>
               <ts e="T114" id="Seg_642" n="e" s="T113">alʼi </ts>
               <ts e="T115" id="Seg_644" n="e" s="T114">ass.” </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_645" s="T1">BaEF_196X_CleverGirl_flk.001 (001.001)</ta>
            <ta e="T8" id="Seg_646" s="T4">BaEF_196X_CleverGirl_flk.002 (001.002)</ta>
            <ta e="T11" id="Seg_647" s="T8">BaEF_196X_CleverGirl_flk.003 (001.003)</ta>
            <ta e="T13" id="Seg_648" s="T11">BaEF_196X_CleverGirl_flk.004 (001.004)</ta>
            <ta e="T26" id="Seg_649" s="T13">BaEF_196X_CleverGirl_flk.005 (001.005)</ta>
            <ta e="T35" id="Seg_650" s="T26">BaEF_196X_CleverGirl_flk.006 (001.006)</ta>
            <ta e="T40" id="Seg_651" s="T35">BaEF_196X_CleverGirl_flk.007 (001.007)</ta>
            <ta e="T44" id="Seg_652" s="T40">BaEF_196X_CleverGirl_flk.008 (001.008)</ta>
            <ta e="T47" id="Seg_653" s="T44">BaEF_196X_CleverGirl_flk.009 (001.009)</ta>
            <ta e="T53" id="Seg_654" s="T47">BaEF_196X_CleverGirl_flk.010 (001.010)</ta>
            <ta e="T58" id="Seg_655" s="T53">BaEF_196X_CleverGirl_flk.011 (001.011)</ta>
            <ta e="T66" id="Seg_656" s="T58">BaEF_196X_CleverGirl_flk.012 (001.012)</ta>
            <ta e="T71" id="Seg_657" s="T66">BaEF_196X_CleverGirl_flk.013 (001.013)</ta>
            <ta e="T76" id="Seg_658" s="T71">BaEF_196X_CleverGirl_flk.014 (001.014)</ta>
            <ta e="T87" id="Seg_659" s="T76">BaEF_196X_CleverGirl_flk.015 (001.015)</ta>
            <ta e="T92" id="Seg_660" s="T87">BaEF_196X_CleverGirl_flk.016 (001.016)</ta>
            <ta e="T104" id="Seg_661" s="T92">BaEF_196X_CleverGirl_flk.017 (001.017)</ta>
            <ta e="T115" id="Seg_662" s="T104">BaEF_196X_CleverGirl_flk.018 (001.018)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_663" s="T1">тӓй′зе (сва) нӓ′де̨ң.</ta>
            <ta e="T8" id="Seg_664" s="T4">′со̄ɣуннан ит ′еп(п)ант.</ta>
            <ta e="T11" id="Seg_665" s="T8">ӣт сва̄ ′е̄ппан′дан.</ta>
            <ta e="T13" id="Seg_666" s="T11">вес ме̄спымбат.</ta>
            <ta e="T26" id="Seg_667" s="T13">′по̄ɣындо ′ме̄спыкум‵бат ′сӓкий (′вы̄рескалам) ′мылам: kопты′ла ′ме̄комындат, kоп′тын тобы′лам та‵чинкум′бат и kоп′тын о′ллам.</ta>
            <ta e="T35" id="Seg_668" s="T26">сʼӓкий ма′телʼе ′ме̄кумбат и ′по̄рлам нӓй всякий ма′желгумбат, ′ме̄кумбат.</ta>
            <ta e="T40" id="Seg_669" s="T35">′арын ′kа̄дарыkындо ′тӱ̄кумбаттъ та′у ку′гу.</ta>
            <ta e="T44" id="Seg_670" s="T40">′ӣɣендъ медым′бак па′ра нӓды′гу.</ta>
            <ta e="T47" id="Seg_671" s="T44">ӣт кы′гелымба нӓды′гу.</ta>
            <ta e="T53" id="Seg_672" s="T47">та′варищ‵kантне ′ӓжолгумба нӓ′де̨м ′пе̄ɣалгу, ′сва̄лаɣ(k)и нӓ′дӓм.</ta>
            <ta e="T58" id="Seg_673" s="T53">на таварищ kван′ба нӓ′дем ′пе̄ɣалгу.</ta>
            <ta e="T66" id="Seg_674" s="T58">сът ′тʼет ′kраɣын ′еппа, сва̄ нӓ′дӓм ас kом′бат.</ta>
            <ta e="T71" id="Seg_675" s="T66">кʼӧдъмджелʼи ′jедъɣын kом′бат сва̄ нӓ′дӓм.</ta>
            <ta e="T76" id="Seg_676" s="T71">сва ′депка ′kомандыт, тӓй′зе нӓ′дӓм.</ta>
            <ta e="T87" id="Seg_677" s="T76">′квессʼе ′тӱмба ′jе̄дент и ′тшентшаң што kо′оw нӓ′дем сва̄ и тӓйзе.</ta>
            <ta e="T92" id="Seg_678" s="T87">тан ӣндане сва̄ паjа ′jенынджан.</ta>
            <ta e="T104" id="Seg_679" s="T92">е′зеw тӓ̄рбылʼе, тʼӓрбылʼе и тʼӓрың: kосты′лай, тӓй′зе jең на нӓ′дӓң алʼи асс.</ta>
            <ta e="T115" id="Seg_680" s="T104">′ӱ̄дъ′лай нӓjаkум, штоб ′кӧ̄скӱгу тӓбы′нӓ на нӓ′дӓне, тшентшугу тӓбы′ненджиң ′алʼи асс.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_681" s="T1">täjze (swa) nädeŋ.</ta>
            <ta e="T8" id="Seg_682" s="T4">soːɣunnan it ep(p)ant.</ta>
            <ta e="T11" id="Seg_683" s="T8">iːt swaː eːppandan.</ta>
            <ta e="T13" id="Seg_684" s="T11">wes meːspɨmbat.</ta>
            <ta e="T26" id="Seg_685" s="T13">poːɣɨndo meːspɨqumbat säqij (wɨːresqalam) mɨlam: qoptɨla meːqomɨndat, qoptɨn tobɨlam tačʼinqumbat i qoptɨn ollam.</ta>
            <ta e="T35" id="Seg_686" s="T26">sʼäqij matelʼe meːqumbat i poːrlam näj wsʼaqij maʒelgumbat, meːqumbat.</ta>
            <ta e="T40" id="Seg_687" s="T35">arɨn qaːdarɨqɨndo tüːqumbattə tau qugu.</ta>
            <ta e="T44" id="Seg_688" s="T40">iːɣendə medɨmbaq para nädɨgu.</ta>
            <ta e="T47" id="Seg_689" s="T44">iːt qɨgelɨmba nädɨgu.</ta>
            <ta e="T53" id="Seg_690" s="T47">tawarišqantne äʒolgumba nädem peːɣalgu, swaːlaɣ(q)i nädäm.</ta>
            <ta e="T58" id="Seg_691" s="T53">na tawariš qwanba nädem peːɣalgu.</ta>
            <ta e="T66" id="Seg_692" s="T58">sət tʼet qraɣɨn eppa, swaː nädäm as qombat.</ta>
            <ta e="T71" id="Seg_693" s="T66">qʼödəmǯelʼi jedəɣɨn qombat swaː nädäm.</ta>
            <ta e="T76" id="Seg_694" s="T71">swa depqa qomandɨt, täjze nädäm.</ta>
            <ta e="T87" id="Seg_695" s="T76">qwessʼe tümba jeːdent i čenčaŋ što qoow nädem swaː i täjze.</ta>
            <ta e="T92" id="Seg_696" s="T87">tan iːndane swaː paja jenɨnǯan.</ta>
            <ta e="T104" id="Seg_697" s="T92">ezew täːrbɨlʼe, tʼärbɨlʼe i tʼärɨŋ: qostɨlaj, täjze jeŋ na nädäŋ alʼi ass.</ta>
            <ta e="T115" id="Seg_698" s="T104">üːdəlaj näjaqum, štob qöːsqügu täbɨnä na nädäne, čenčugu täbɨnenǯiŋ alʼi ass.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_699" s="T1">Täjze (swa) nädeŋ. </ta>
            <ta e="T8" id="Seg_700" s="T4">Soː ɣunnan it epant. </ta>
            <ta e="T11" id="Seg_701" s="T8">Iːt swaː eːppandan. </ta>
            <ta e="T13" id="Seg_702" s="T11">Wes meːspɨmbat. </ta>
            <ta e="T26" id="Seg_703" s="T13">Poːɣɨndo meːspɨqumbat säqij (wɨːresqalam) mɨlam: qoptɨla meːqomɨndat, qoptɨn tobɨlam tačʼinqumbat i qoptɨn ollam. </ta>
            <ta e="T35" id="Seg_704" s="T26">Sʼäqij matelʼe meːqumbat i poːrlam näj wsʼaqij maʒelgumbat, meːqumbat. </ta>
            <ta e="T40" id="Seg_705" s="T35">Arɨn qaːdarɨqɨndo tüːqumbattə tau qugu. </ta>
            <ta e="T44" id="Seg_706" s="T40">Iːɣendə medɨmbaq para nädɨgu. </ta>
            <ta e="T47" id="Seg_707" s="T44">Iːt qɨgelɨmba nädɨgu. </ta>
            <ta e="T53" id="Seg_708" s="T47">Tawarišqantne äʒolgumba nädem peːɣalgu, swaːlaɣi nädäm. </ta>
            <ta e="T58" id="Seg_709" s="T53">Na tawariš qwanba nädem peːɣalgu. </ta>
            <ta e="T66" id="Seg_710" s="T58">Sət tʼet qraɣɨn eppa, swaː nädäm as qombat. </ta>
            <ta e="T71" id="Seg_711" s="T66">Qʼödəmǯelʼi jedəɣɨn qombat swaː nädäm. </ta>
            <ta e="T76" id="Seg_712" s="T71">Swa depqa qomandɨt, täjze nädäm. </ta>
            <ta e="T87" id="Seg_713" s="T76">Qwessʼe tümba jeːdent i čenčaŋ što qoow nädem swaː i täjze. </ta>
            <ta e="T92" id="Seg_714" s="T87">“Tan iːndane swaː paja jenɨnǯan.” </ta>
            <ta e="T104" id="Seg_715" s="T92">Ezew täːrbɨlʼe, tʼärbɨlʼe i tʼärɨŋ: “Qostɨlaj, täjze jeŋ na nädäŋ alʼi ass. </ta>
            <ta e="T115" id="Seg_716" s="T104">Üːdəlaj näjaqum, štob qöːsqügu täbɨnä na nädäne, čenčugu täbɨnenǯiŋ alʼi ass.” </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_717" s="T1">täj-ze</ta>
            <ta e="T3" id="Seg_718" s="T2">swa</ta>
            <ta e="T4" id="Seg_719" s="T3">nädeŋ</ta>
            <ta e="T5" id="Seg_720" s="T4">soː</ta>
            <ta e="T6" id="Seg_721" s="T5">ɣun-nan</ta>
            <ta e="T7" id="Seg_722" s="T6">i-t</ta>
            <ta e="T8" id="Seg_723" s="T7">e-pa-nt</ta>
            <ta e="T9" id="Seg_724" s="T8">iː-t</ta>
            <ta e="T10" id="Seg_725" s="T9">swaː</ta>
            <ta e="T11" id="Seg_726" s="T10">eː-ppa-nda-n</ta>
            <ta e="T12" id="Seg_727" s="T11">wes</ta>
            <ta e="T13" id="Seg_728" s="T12">meː-spɨ-mba-t</ta>
            <ta e="T14" id="Seg_729" s="T13">poː-ɣɨndo</ta>
            <ta e="T15" id="Seg_730" s="T14">meː-spɨ-qu-mba-t</ta>
            <ta e="T16" id="Seg_731" s="T15">säqij</ta>
            <ta e="T17" id="Seg_732" s="T16">wɨːresqa-la-m</ta>
            <ta e="T18" id="Seg_733" s="T17">mɨ-la-m</ta>
            <ta e="T19" id="Seg_734" s="T18">qoptɨ-la</ta>
            <ta e="T20" id="Seg_735" s="T19">meː-qo-mɨ-nda-t</ta>
            <ta e="T21" id="Seg_736" s="T20">qoptɨ-n</ta>
            <ta e="T22" id="Seg_737" s="T21">tob-ɨ-la-m</ta>
            <ta e="T23" id="Seg_738" s="T22">tačʼin-qu-mba-t</ta>
            <ta e="T24" id="Seg_739" s="T23">i</ta>
            <ta e="T25" id="Seg_740" s="T24">qoptɨ-n</ta>
            <ta e="T26" id="Seg_741" s="T25">ol-la-m</ta>
            <ta e="T27" id="Seg_742" s="T26">sʼäqij</ta>
            <ta e="T28" id="Seg_743" s="T27">matelʼe</ta>
            <ta e="T29" id="Seg_744" s="T28">meː-qu-mba-t</ta>
            <ta e="T30" id="Seg_745" s="T29">i</ta>
            <ta e="T31" id="Seg_746" s="T30">poːr-la-m</ta>
            <ta e="T32" id="Seg_747" s="T31">näj</ta>
            <ta e="T33" id="Seg_748" s="T32">wsʼaqij</ta>
            <ta e="T34" id="Seg_749" s="T33">maʒe-l-gu-mba-t</ta>
            <ta e="T35" id="Seg_750" s="T34">meː-qu-mba-t</ta>
            <ta e="T36" id="Seg_751" s="T35">ar-ɨ-n</ta>
            <ta e="T37" id="Seg_752" s="T36">qaːdar-ɨ-qɨndo</ta>
            <ta e="T38" id="Seg_753" s="T37">tüː-qu-mba-ttə</ta>
            <ta e="T39" id="Seg_754" s="T38">tau</ta>
            <ta e="T40" id="Seg_755" s="T39">qu-gu</ta>
            <ta e="T41" id="Seg_756" s="T40">iː-ɣendə</ta>
            <ta e="T42" id="Seg_757" s="T41">medɨ-mba-q</ta>
            <ta e="T43" id="Seg_758" s="T42">para</ta>
            <ta e="T44" id="Seg_759" s="T43">nädɨ-gu</ta>
            <ta e="T45" id="Seg_760" s="T44">iː-t</ta>
            <ta e="T46" id="Seg_761" s="T45">qɨge-lɨ-mba</ta>
            <ta e="T47" id="Seg_762" s="T46">nädɨ-gu</ta>
            <ta e="T48" id="Seg_763" s="T47">tawariš-qa-nt-ne</ta>
            <ta e="T49" id="Seg_764" s="T48">äʒ-ol-gu-mba</ta>
            <ta e="T50" id="Seg_765" s="T49">näde-m</ta>
            <ta e="T51" id="Seg_766" s="T50">peː-ɣal-gu</ta>
            <ta e="T52" id="Seg_767" s="T51">swaː-laɣi</ta>
            <ta e="T53" id="Seg_768" s="T52">nädä-m</ta>
            <ta e="T54" id="Seg_769" s="T53">na</ta>
            <ta e="T55" id="Seg_770" s="T54">tawariš</ta>
            <ta e="T56" id="Seg_771" s="T55">qwan-ba</ta>
            <ta e="T57" id="Seg_772" s="T56">näde-m</ta>
            <ta e="T58" id="Seg_773" s="T57">peː-ɣal-gu</ta>
            <ta e="T59" id="Seg_774" s="T58">sət</ta>
            <ta e="T60" id="Seg_775" s="T59">tʼet</ta>
            <ta e="T61" id="Seg_776" s="T60">qra-ɣɨn</ta>
            <ta e="T62" id="Seg_777" s="T61">e-ppa</ta>
            <ta e="T63" id="Seg_778" s="T62">swaː</ta>
            <ta e="T64" id="Seg_779" s="T63">nädä-m</ta>
            <ta e="T65" id="Seg_780" s="T64">as</ta>
            <ta e="T66" id="Seg_781" s="T65">qo-mba-t</ta>
            <ta e="T67" id="Seg_782" s="T66">qʼöd-ə-mǯelʼi</ta>
            <ta e="T68" id="Seg_783" s="T67">jedə-ɣɨn</ta>
            <ta e="T69" id="Seg_784" s="T68">qo-mba-t</ta>
            <ta e="T70" id="Seg_785" s="T69">swaː</ta>
            <ta e="T71" id="Seg_786" s="T70">nädä-m</ta>
            <ta e="T72" id="Seg_787" s="T71">swa</ta>
            <ta e="T73" id="Seg_788" s="T72">depqa</ta>
            <ta e="T74" id="Seg_789" s="T73">qo-ma-ndɨ-t</ta>
            <ta e="T75" id="Seg_790" s="T74">täj-ze</ta>
            <ta e="T76" id="Seg_791" s="T75">nädä-m</ta>
            <ta e="T77" id="Seg_792" s="T76">qwessʼe</ta>
            <ta e="T78" id="Seg_793" s="T77">tü-mba</ta>
            <ta e="T79" id="Seg_794" s="T78">jeːde-nt</ta>
            <ta e="T80" id="Seg_795" s="T79">i</ta>
            <ta e="T81" id="Seg_796" s="T80">čenča-ŋ</ta>
            <ta e="T82" id="Seg_797" s="T81">što</ta>
            <ta e="T83" id="Seg_798" s="T82">qo-o-w</ta>
            <ta e="T84" id="Seg_799" s="T83">näde-m</ta>
            <ta e="T85" id="Seg_800" s="T84">swaː</ta>
            <ta e="T86" id="Seg_801" s="T85">i</ta>
            <ta e="T87" id="Seg_802" s="T86">täj-ze</ta>
            <ta e="T88" id="Seg_803" s="T87">Tan</ta>
            <ta e="T89" id="Seg_804" s="T88">iː-nda-ne</ta>
            <ta e="T90" id="Seg_805" s="T89">swaː</ta>
            <ta e="T91" id="Seg_806" s="T90">paja</ta>
            <ta e="T92" id="Seg_807" s="T91">je-nɨ-nǯa-n</ta>
            <ta e="T93" id="Seg_808" s="T92">eze-w</ta>
            <ta e="T94" id="Seg_809" s="T93">täːrbɨ-lʼe</ta>
            <ta e="T95" id="Seg_810" s="T94">tʼärbɨ-lʼe</ta>
            <ta e="T96" id="Seg_811" s="T95">i</ta>
            <ta e="T97" id="Seg_812" s="T96">tʼärɨ-ŋ</ta>
            <ta e="T98" id="Seg_813" s="T97">qostɨ-la-j</ta>
            <ta e="T99" id="Seg_814" s="T98">täj-ze</ta>
            <ta e="T100" id="Seg_815" s="T99">je-ŋ</ta>
            <ta e="T101" id="Seg_816" s="T100">na</ta>
            <ta e="T102" id="Seg_817" s="T101">nädäŋ</ta>
            <ta e="T103" id="Seg_818" s="T102">alʼi</ta>
            <ta e="T104" id="Seg_819" s="T103">ass</ta>
            <ta e="T105" id="Seg_820" s="T104">üːdə-la-j</ta>
            <ta e="T106" id="Seg_821" s="T105">nä-j-a-qum</ta>
            <ta e="T107" id="Seg_822" s="T106">štob</ta>
            <ta e="T108" id="Seg_823" s="T107">qöːs-qü-gu</ta>
            <ta e="T109" id="Seg_824" s="T108">täb-ɨ-nä</ta>
            <ta e="T110" id="Seg_825" s="T109">na</ta>
            <ta e="T111" id="Seg_826" s="T110">nädä-ne</ta>
            <ta e="T112" id="Seg_827" s="T111">čenču-gu</ta>
            <ta e="T113" id="Seg_828" s="T112">täbɨn-enǯi-ŋ</ta>
            <ta e="T114" id="Seg_829" s="T113">alʼi</ta>
            <ta e="T115" id="Seg_830" s="T114">ass</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_831" s="T1">täj-se</ta>
            <ta e="T3" id="Seg_832" s="T2">sawa</ta>
            <ta e="T4" id="Seg_833" s="T3">nädek</ta>
            <ta e="T5" id="Seg_834" s="T4">soː</ta>
            <ta e="T6" id="Seg_835" s="T5">qum-nan</ta>
            <ta e="T7" id="Seg_836" s="T6">iː-tə</ta>
            <ta e="T8" id="Seg_837" s="T7">eː-mbɨ-ntɨ</ta>
            <ta e="T9" id="Seg_838" s="T8">iː-tə</ta>
            <ta e="T10" id="Seg_839" s="T9">sawa</ta>
            <ta e="T11" id="Seg_840" s="T10">eː-mbɨ-ntɨ-n</ta>
            <ta e="T12" id="Seg_841" s="T11">wesʼ</ta>
            <ta e="T13" id="Seg_842" s="T12">meː-špə-mbɨ-t</ta>
            <ta e="T14" id="Seg_843" s="T13">po-qɨntɨ</ta>
            <ta e="T15" id="Seg_844" s="T14">meː-špə-ku-mbɨ-t</ta>
            <ta e="T16" id="Seg_845" s="T15">wsʼäkij</ta>
            <ta e="T17" id="Seg_846" s="T16">wɨːresqa-la-m</ta>
            <ta e="T18" id="Seg_847" s="T17">mɨ-la-m</ta>
            <ta e="T19" id="Seg_848" s="T18">qoptə-la</ta>
            <ta e="T20" id="Seg_849" s="T19">meː-ku-mbɨ-ntɨ-t</ta>
            <ta e="T21" id="Seg_850" s="T20">qoptə-n</ta>
            <ta e="T22" id="Seg_851" s="T21">tob-ɨ-la-m</ta>
            <ta e="T23" id="Seg_852" s="T22">tačʼin-ku-mbɨ-t</ta>
            <ta e="T24" id="Seg_853" s="T23">i</ta>
            <ta e="T25" id="Seg_854" s="T24">qoptə-n</ta>
            <ta e="T26" id="Seg_855" s="T25">olə-la-m</ta>
            <ta e="T27" id="Seg_856" s="T26">wsʼäkij</ta>
            <ta e="T28" id="Seg_857" s="T27">matelʼe</ta>
            <ta e="T29" id="Seg_858" s="T28">meː-ku-mbɨ-t</ta>
            <ta e="T30" id="Seg_859" s="T29">i</ta>
            <ta e="T31" id="Seg_860" s="T30">por-la-m</ta>
            <ta e="T32" id="Seg_861" s="T31">naj</ta>
            <ta e="T33" id="Seg_862" s="T32">wsʼäkij</ta>
            <ta e="T34" id="Seg_863" s="T33">maǯə-lɨ-ku-mbɨ-t</ta>
            <ta e="T35" id="Seg_864" s="T34">meː-ku-mbɨ-t</ta>
            <ta e="T36" id="Seg_865" s="T35">aːr-ɨ-n</ta>
            <ta e="T37" id="Seg_866" s="T36">kadar-ɨ-qɨntɨ</ta>
            <ta e="T38" id="Seg_867" s="T37">tüː-ku-mbɨ-tɨn</ta>
            <ta e="T39" id="Seg_868" s="T38">taw</ta>
            <ta e="T40" id="Seg_869" s="T39">qo-gu</ta>
            <ta e="T41" id="Seg_870" s="T40">iː-qɨntɨ</ta>
            <ta e="T42" id="Seg_871" s="T41">medə-mbɨ-qij</ta>
            <ta e="T43" id="Seg_872" s="T42">para</ta>
            <ta e="T44" id="Seg_873" s="T43">nedɨ-gu</ta>
            <ta e="T45" id="Seg_874" s="T44">iː-tə</ta>
            <ta e="T46" id="Seg_875" s="T45">kɨgɨ-lɨ-mbɨ</ta>
            <ta e="T47" id="Seg_876" s="T46">nedɨ-gu</ta>
            <ta e="T48" id="Seg_877" s="T47">tawariš-ka-ntɨ-nä</ta>
            <ta e="T49" id="Seg_878" s="T48">əǯu-ol-ku-mbɨ</ta>
            <ta e="T50" id="Seg_879" s="T49">nädek-m</ta>
            <ta e="T51" id="Seg_880" s="T50">peː-qɨl-gu</ta>
            <ta e="T52" id="Seg_881" s="T51">sawa-laq</ta>
            <ta e="T53" id="Seg_882" s="T52">nädek-m</ta>
            <ta e="T54" id="Seg_883" s="T53">na</ta>
            <ta e="T55" id="Seg_884" s="T54">tawariš</ta>
            <ta e="T56" id="Seg_885" s="T55">qwan-mbɨ</ta>
            <ta e="T57" id="Seg_886" s="T56">nädek-m</ta>
            <ta e="T58" id="Seg_887" s="T57">peː-qɨl-gu</ta>
            <ta e="T59" id="Seg_888" s="T58">sədə</ta>
            <ta e="T60" id="Seg_889" s="T59">tettɨ</ta>
            <ta e="T61" id="Seg_890" s="T60">kraj-qɨn</ta>
            <ta e="T62" id="Seg_891" s="T61">eː-mbɨ</ta>
            <ta e="T63" id="Seg_892" s="T62">sawa</ta>
            <ta e="T64" id="Seg_893" s="T63">nädek-m</ta>
            <ta e="T65" id="Seg_894" s="T64">asa</ta>
            <ta e="T66" id="Seg_895" s="T65">qo-mbɨ-t</ta>
            <ta e="T67" id="Seg_896" s="T66">köt-ɨ-mǯʼeːli</ta>
            <ta e="T68" id="Seg_897" s="T67">eːde-qɨn</ta>
            <ta e="T69" id="Seg_898" s="T68">qo-mbɨ-t</ta>
            <ta e="T70" id="Seg_899" s="T69">sawa</ta>
            <ta e="T71" id="Seg_900" s="T70">nädek-m</ta>
            <ta e="T72" id="Seg_901" s="T71">sawa</ta>
            <ta e="T73" id="Seg_902" s="T72">dʼepka</ta>
            <ta e="T74" id="Seg_903" s="T73">qo-mbɨ-ntɨ-t</ta>
            <ta e="T75" id="Seg_904" s="T74">täj-se</ta>
            <ta e="T76" id="Seg_905" s="T75">nädek-m</ta>
            <ta e="T77" id="Seg_906" s="T76">kössə</ta>
            <ta e="T78" id="Seg_907" s="T77">tüː-mbɨ</ta>
            <ta e="T79" id="Seg_908" s="T78">eːde-ntə</ta>
            <ta e="T80" id="Seg_909" s="T79">i</ta>
            <ta e="T81" id="Seg_910" s="T80">čenču-n</ta>
            <ta e="T82" id="Seg_911" s="T81">što</ta>
            <ta e="T83" id="Seg_912" s="T82">qo-nɨ-w</ta>
            <ta e="T84" id="Seg_913" s="T83">nädek-m</ta>
            <ta e="T85" id="Seg_914" s="T84">sawa</ta>
            <ta e="T86" id="Seg_915" s="T85">i</ta>
            <ta e="T87" id="Seg_916" s="T86">täj-se</ta>
            <ta e="T88" id="Seg_917" s="T87">tan</ta>
            <ta e="T89" id="Seg_918" s="T88">iː-ntɨ-nä</ta>
            <ta e="T90" id="Seg_919" s="T89">sawa</ta>
            <ta e="T91" id="Seg_920" s="T90">paja</ta>
            <ta e="T92" id="Seg_921" s="T91">eː-ntɨ-enǯɨ-n</ta>
            <ta e="T93" id="Seg_922" s="T92">aze-w</ta>
            <ta e="T94" id="Seg_923" s="T93">tärba-le</ta>
            <ta e="T95" id="Seg_924" s="T94">tärba-le</ta>
            <ta e="T96" id="Seg_925" s="T95">i</ta>
            <ta e="T97" id="Seg_926" s="T96">tʼärɨ-n</ta>
            <ta e="T98" id="Seg_927" s="T97">kostɨ-lä-j</ta>
            <ta e="T99" id="Seg_928" s="T98">täj-se</ta>
            <ta e="T100" id="Seg_929" s="T99">eː-n</ta>
            <ta e="T101" id="Seg_930" s="T100">na</ta>
            <ta e="T102" id="Seg_931" s="T101">nädek</ta>
            <ta e="T103" id="Seg_932" s="T102">alʼi</ta>
            <ta e="T104" id="Seg_933" s="T103">asa</ta>
            <ta e="T105" id="Seg_934" s="T104">üdə-lä-j</ta>
            <ta e="T106" id="Seg_935" s="T105">ne-lʼ-ɨ-qum</ta>
            <ta e="T107" id="Seg_936" s="T106">štobɨ</ta>
            <ta e="T108" id="Seg_937" s="T107">*qöːš-ku-gu</ta>
            <ta e="T109" id="Seg_938" s="T108">täp-ɨ-nä</ta>
            <ta e="T110" id="Seg_939" s="T109">na</ta>
            <ta e="T111" id="Seg_940" s="T110">nädek-nä</ta>
            <ta e="T112" id="Seg_941" s="T111">čenču-gu</ta>
            <ta e="T113" id="Seg_942" s="T112">tɨbəndu-enǯɨ-n</ta>
            <ta e="T114" id="Seg_943" s="T113">alʼi</ta>
            <ta e="T115" id="Seg_944" s="T114">asa</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_945" s="T1">mind-ADJZ</ta>
            <ta e="T3" id="Seg_946" s="T2">good</ta>
            <ta e="T4" id="Seg_947" s="T3">girl.[NOM]</ta>
            <ta e="T5" id="Seg_948" s="T4">good</ta>
            <ta e="T6" id="Seg_949" s="T5">human.being-ADES</ta>
            <ta e="T7" id="Seg_950" s="T6">son.[NOM]-3SG</ta>
            <ta e="T8" id="Seg_951" s="T7">be-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T9" id="Seg_952" s="T8">son.[NOM]-3SG</ta>
            <ta e="T10" id="Seg_953" s="T9">good</ta>
            <ta e="T11" id="Seg_954" s="T10">be-PST.NAR-INFER-3SG.S</ta>
            <ta e="T12" id="Seg_955" s="T11">all</ta>
            <ta e="T13" id="Seg_956" s="T12">do-IPFV2-PST.NAR-3SG.O</ta>
            <ta e="T14" id="Seg_957" s="T13">tree-EL.3SG</ta>
            <ta e="T15" id="Seg_958" s="T14">do-IPFV2-HAB-PST.NAR-3SG.O</ta>
            <ta e="T16" id="Seg_959" s="T15">any</ta>
            <ta e="T17" id="Seg_960" s="T16">carving-PL-ACC</ta>
            <ta e="T18" id="Seg_961" s="T17">something-PL-ACC</ta>
            <ta e="T19" id="Seg_962" s="T18">bed-PL.[NOM]</ta>
            <ta e="T20" id="Seg_963" s="T19">do-HAB-PST.NAR-INFER-3SG.O</ta>
            <ta e="T21" id="Seg_964" s="T20">bed-GEN</ta>
            <ta e="T22" id="Seg_965" s="T21">leg-EP-PL-ACC</ta>
            <ta e="T23" id="Seg_966" s="T22">turn-HAB-PST.NAR-3SG.O</ta>
            <ta e="T24" id="Seg_967" s="T23">and</ta>
            <ta e="T25" id="Seg_968" s="T24">bed-GEN</ta>
            <ta e="T26" id="Seg_969" s="T25">head-PL-ACC</ta>
            <ta e="T27" id="Seg_970" s="T26">any</ta>
            <ta e="T28" id="Seg_971" s="T27">%%</ta>
            <ta e="T29" id="Seg_972" s="T28">do-HAB-PST.NAR-3SG.O</ta>
            <ta e="T30" id="Seg_973" s="T29">and</ta>
            <ta e="T31" id="Seg_974" s="T30">shelf-PL-ACC</ta>
            <ta e="T32" id="Seg_975" s="T31">also</ta>
            <ta e="T33" id="Seg_976" s="T32">any</ta>
            <ta e="T34" id="Seg_977" s="T33">cut-RES-HAB-PST.NAR-3SG.O</ta>
            <ta e="T35" id="Seg_978" s="T34">do-HAB-PST.NAR-3SG.O</ta>
            <ta e="T36" id="Seg_979" s="T35">other-EP-GEN</ta>
            <ta e="T37" id="Seg_980" s="T36">land-EP-ILL.3SG</ta>
            <ta e="T38" id="Seg_981" s="T37">come-HAB-PST.NAR-3PL</ta>
            <ta e="T39" id="Seg_982" s="T38">this</ta>
            <ta e="T40" id="Seg_983" s="T39">find-INF</ta>
            <ta e="T41" id="Seg_984" s="T40">son-ILL.3SG</ta>
            <ta e="T42" id="Seg_985" s="T41">achieve-PST.NAR-3DU.S</ta>
            <ta e="T43" id="Seg_986" s="T42">time.[NOM]</ta>
            <ta e="T44" id="Seg_987" s="T43">get.married-INF</ta>
            <ta e="T45" id="Seg_988" s="T44">son.[NOM]-3SG</ta>
            <ta e="T46" id="Seg_989" s="T45">want-RES-PST.NAR.[3SG.S]</ta>
            <ta e="T47" id="Seg_990" s="T46">get.married-INF</ta>
            <ta e="T48" id="Seg_991" s="T47">friend-DIM-OBL.3SG-ALL</ta>
            <ta e="T49" id="Seg_992" s="T48">say-MOM-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T50" id="Seg_993" s="T49">girl-ACC</ta>
            <ta e="T51" id="Seg_994" s="T50">look.for-DRV-INF</ta>
            <ta e="T52" id="Seg_995" s="T51">good-ATTEN</ta>
            <ta e="T53" id="Seg_996" s="T52">girl-ACC</ta>
            <ta e="T54" id="Seg_997" s="T53">this</ta>
            <ta e="T55" id="Seg_998" s="T54">friend.[NOM]</ta>
            <ta e="T56" id="Seg_999" s="T55">leave-PST.NAR.[3SG.S]</ta>
            <ta e="T57" id="Seg_1000" s="T56">girl-ACC</ta>
            <ta e="T58" id="Seg_1001" s="T57">look.for-DRV-INF</ta>
            <ta e="T59" id="Seg_1002" s="T58">two</ta>
            <ta e="T60" id="Seg_1003" s="T59">four</ta>
            <ta e="T61" id="Seg_1004" s="T60">land-LOC</ta>
            <ta e="T62" id="Seg_1005" s="T61">be-PST.NAR.[3SG.S]</ta>
            <ta e="T63" id="Seg_1006" s="T62">good</ta>
            <ta e="T64" id="Seg_1007" s="T63">girl-ACC</ta>
            <ta e="T65" id="Seg_1008" s="T64">NEG</ta>
            <ta e="T66" id="Seg_1009" s="T65">find-PST.NAR-3SG.O</ta>
            <ta e="T67" id="Seg_1010" s="T66">ten-EP-ORD</ta>
            <ta e="T68" id="Seg_1011" s="T67">village-LOC</ta>
            <ta e="T69" id="Seg_1012" s="T68">find-PST.NAR-3SG.O</ta>
            <ta e="T70" id="Seg_1013" s="T69">good</ta>
            <ta e="T71" id="Seg_1014" s="T70">girl-ACC</ta>
            <ta e="T72" id="Seg_1015" s="T71">good</ta>
            <ta e="T73" id="Seg_1016" s="T72">girl.[NOM]</ta>
            <ta e="T74" id="Seg_1017" s="T73">see-PST.NAR-INFER-3SG.O</ta>
            <ta e="T75" id="Seg_1018" s="T74">mind-ADJZ</ta>
            <ta e="T76" id="Seg_1019" s="T75">girl-ACC</ta>
            <ta e="T77" id="Seg_1020" s="T76">backward</ta>
            <ta e="T78" id="Seg_1021" s="T77">come-PST.NAR.[3SG.S]</ta>
            <ta e="T79" id="Seg_1022" s="T78">village-ILL</ta>
            <ta e="T80" id="Seg_1023" s="T79">and</ta>
            <ta e="T81" id="Seg_1024" s="T80">say-3SG.S</ta>
            <ta e="T82" id="Seg_1025" s="T81">that</ta>
            <ta e="T83" id="Seg_1026" s="T82">find-CO-1SG.O</ta>
            <ta e="T84" id="Seg_1027" s="T83">girl-ACC</ta>
            <ta e="T85" id="Seg_1028" s="T84">good</ta>
            <ta e="T86" id="Seg_1029" s="T85">and</ta>
            <ta e="T87" id="Seg_1030" s="T86">mind-ADJZ</ta>
            <ta e="T88" id="Seg_1031" s="T87">you.SG.NOM</ta>
            <ta e="T89" id="Seg_1032" s="T88">son-OBL.2SG-ALL</ta>
            <ta e="T90" id="Seg_1033" s="T89">good</ta>
            <ta e="T91" id="Seg_1034" s="T90">wife.[NOM]</ta>
            <ta e="T92" id="Seg_1035" s="T91">be-INFER-FUT-3SG.S</ta>
            <ta e="T93" id="Seg_1036" s="T92">father.[NOM]-1SG</ta>
            <ta e="T94" id="Seg_1037" s="T93">think-CVB</ta>
            <ta e="T95" id="Seg_1038" s="T94">think-CVB</ta>
            <ta e="T96" id="Seg_1039" s="T95">and</ta>
            <ta e="T97" id="Seg_1040" s="T96">say-3SG.S</ta>
            <ta e="T98" id="Seg_1041" s="T97">get.to.know-OPT-1DU</ta>
            <ta e="T99" id="Seg_1042" s="T98">mind-ADJZ</ta>
            <ta e="T100" id="Seg_1043" s="T99">be-3SG.S</ta>
            <ta e="T101" id="Seg_1044" s="T100">this</ta>
            <ta e="T102" id="Seg_1045" s="T101">girl.[NOM]</ta>
            <ta e="T103" id="Seg_1046" s="T102">or</ta>
            <ta e="T104" id="Seg_1047" s="T103">NEG</ta>
            <ta e="T105" id="Seg_1048" s="T104">send-OPT-1DU</ta>
            <ta e="T106" id="Seg_1049" s="T105">woman-ADJZ-EP-human.being.[NOM]</ta>
            <ta e="T107" id="Seg_1050" s="T106">so.that</ta>
            <ta e="T108" id="Seg_1051" s="T107">go-HAB-INF</ta>
            <ta e="T109" id="Seg_1052" s="T108">(s)he-EP-ALL</ta>
            <ta e="T110" id="Seg_1053" s="T109">this</ta>
            <ta e="T111" id="Seg_1054" s="T110">girl-ALL</ta>
            <ta e="T112" id="Seg_1055" s="T111">speak-INF</ta>
            <ta e="T113" id="Seg_1056" s="T112">marry-FUT-3SG.S</ta>
            <ta e="T114" id="Seg_1057" s="T113">or</ta>
            <ta e="T115" id="Seg_1058" s="T114">NEG</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_1059" s="T1">ум-ADJZ</ta>
            <ta e="T3" id="Seg_1060" s="T2">хороший</ta>
            <ta e="T4" id="Seg_1061" s="T3">девушка.[NOM]</ta>
            <ta e="T5" id="Seg_1062" s="T4">хороший</ta>
            <ta e="T6" id="Seg_1063" s="T5">человек-ADES</ta>
            <ta e="T7" id="Seg_1064" s="T6">сын.[NOM]-3SG</ta>
            <ta e="T8" id="Seg_1065" s="T7">быть-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T9" id="Seg_1066" s="T8">сын.[NOM]-3SG</ta>
            <ta e="T10" id="Seg_1067" s="T9">хороший</ta>
            <ta e="T11" id="Seg_1068" s="T10">быть-PST.NAR-INFER-3SG.S</ta>
            <ta e="T12" id="Seg_1069" s="T11">все</ta>
            <ta e="T13" id="Seg_1070" s="T12">сделать-IPFV2-PST.NAR-3SG.O</ta>
            <ta e="T14" id="Seg_1071" s="T13">дерево-EL.3SG</ta>
            <ta e="T15" id="Seg_1072" s="T14">сделать-IPFV2-HAB-PST.NAR-3SG.O</ta>
            <ta e="T16" id="Seg_1073" s="T15">всякий</ta>
            <ta e="T17" id="Seg_1074" s="T16">вырезка-PL-ACC</ta>
            <ta e="T18" id="Seg_1075" s="T17">нечто-PL-ACC</ta>
            <ta e="T19" id="Seg_1076" s="T18">кровать-PL.[NOM]</ta>
            <ta e="T20" id="Seg_1077" s="T19">сделать-HAB-PST.NAR-INFER-3SG.O</ta>
            <ta e="T21" id="Seg_1078" s="T20">кровать-GEN</ta>
            <ta e="T22" id="Seg_1079" s="T21">нога-EP-PL-ACC</ta>
            <ta e="T23" id="Seg_1080" s="T22">выточить-HAB-PST.NAR-3SG.O</ta>
            <ta e="T24" id="Seg_1081" s="T23">и</ta>
            <ta e="T25" id="Seg_1082" s="T24">кровать-GEN</ta>
            <ta e="T26" id="Seg_1083" s="T25">голова-PL-ACC</ta>
            <ta e="T27" id="Seg_1084" s="T26">всякий</ta>
            <ta e="T28" id="Seg_1085" s="T27">%%</ta>
            <ta e="T29" id="Seg_1086" s="T28">сделать-HAB-PST.NAR-3SG.O</ta>
            <ta e="T30" id="Seg_1087" s="T29">и</ta>
            <ta e="T31" id="Seg_1088" s="T30">полка-PL-ACC</ta>
            <ta e="T32" id="Seg_1089" s="T31">тоже</ta>
            <ta e="T33" id="Seg_1090" s="T32">всякий</ta>
            <ta e="T34" id="Seg_1091" s="T33">отрезать-RES-HAB-PST.NAR-3SG.O</ta>
            <ta e="T35" id="Seg_1092" s="T34">сделать-HAB-PST.NAR-3SG.O</ta>
            <ta e="T36" id="Seg_1093" s="T35">другой-EP-GEN</ta>
            <ta e="T37" id="Seg_1094" s="T36">край-EP-ILL.3SG</ta>
            <ta e="T38" id="Seg_1095" s="T37">прийти-HAB-PST.NAR-3PL</ta>
            <ta e="T39" id="Seg_1096" s="T38">этот</ta>
            <ta e="T40" id="Seg_1097" s="T39">найти-INF</ta>
            <ta e="T41" id="Seg_1098" s="T40">сын-ILL.3SG</ta>
            <ta e="T42" id="Seg_1099" s="T41">достичь-PST.NAR-3DU.S</ta>
            <ta e="T43" id="Seg_1100" s="T42">пора.[NOM]</ta>
            <ta e="T44" id="Seg_1101" s="T43">жениться-INF</ta>
            <ta e="T45" id="Seg_1102" s="T44">сын.[NOM]-3SG</ta>
            <ta e="T46" id="Seg_1103" s="T45">хотеть-RES-PST.NAR.[3SG.S]</ta>
            <ta e="T47" id="Seg_1104" s="T46">жениться-INF</ta>
            <ta e="T48" id="Seg_1105" s="T47">товарищ-DIM-OBL.3SG-ALL</ta>
            <ta e="T49" id="Seg_1106" s="T48">сказать-MOM-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T50" id="Seg_1107" s="T49">девушка-ACC</ta>
            <ta e="T51" id="Seg_1108" s="T50">искать-DRV-INF</ta>
            <ta e="T52" id="Seg_1109" s="T51">хороший-ATTEN</ta>
            <ta e="T53" id="Seg_1110" s="T52">девушка-ACC</ta>
            <ta e="T54" id="Seg_1111" s="T53">этот</ta>
            <ta e="T55" id="Seg_1112" s="T54">товарищ.[NOM]</ta>
            <ta e="T56" id="Seg_1113" s="T55">отправиться-PST.NAR.[3SG.S]</ta>
            <ta e="T57" id="Seg_1114" s="T56">девушка-ACC</ta>
            <ta e="T58" id="Seg_1115" s="T57">искать-DRV-INF</ta>
            <ta e="T59" id="Seg_1116" s="T58">два</ta>
            <ta e="T60" id="Seg_1117" s="T59">четыре</ta>
            <ta e="T61" id="Seg_1118" s="T60">край-LOC</ta>
            <ta e="T62" id="Seg_1119" s="T61">быть-PST.NAR.[3SG.S]</ta>
            <ta e="T63" id="Seg_1120" s="T62">хороший</ta>
            <ta e="T64" id="Seg_1121" s="T63">девушка-ACC</ta>
            <ta e="T65" id="Seg_1122" s="T64">NEG</ta>
            <ta e="T66" id="Seg_1123" s="T65">найти-PST.NAR-3SG.O</ta>
            <ta e="T67" id="Seg_1124" s="T66">десять-EP-ORD</ta>
            <ta e="T68" id="Seg_1125" s="T67">деревня-LOC</ta>
            <ta e="T69" id="Seg_1126" s="T68">найти-PST.NAR-3SG.O</ta>
            <ta e="T70" id="Seg_1127" s="T69">хороший</ta>
            <ta e="T71" id="Seg_1128" s="T70">девушка-ACC</ta>
            <ta e="T72" id="Seg_1129" s="T71">хороший</ta>
            <ta e="T73" id="Seg_1130" s="T72">девка.[NOM]</ta>
            <ta e="T74" id="Seg_1131" s="T73">увидеть-PST.NAR-INFER-3SG.O</ta>
            <ta e="T75" id="Seg_1132" s="T74">ум-ADJZ</ta>
            <ta e="T76" id="Seg_1133" s="T75">девушка-ACC</ta>
            <ta e="T77" id="Seg_1134" s="T76">назад</ta>
            <ta e="T78" id="Seg_1135" s="T77">прийти-PST.NAR.[3SG.S]</ta>
            <ta e="T79" id="Seg_1136" s="T78">деревня-ILL</ta>
            <ta e="T80" id="Seg_1137" s="T79">и</ta>
            <ta e="T81" id="Seg_1138" s="T80">сказать-3SG.S</ta>
            <ta e="T82" id="Seg_1139" s="T81">что</ta>
            <ta e="T83" id="Seg_1140" s="T82">найти-CO-1SG.O</ta>
            <ta e="T84" id="Seg_1141" s="T83">девушка-ACC</ta>
            <ta e="T85" id="Seg_1142" s="T84">хороший</ta>
            <ta e="T86" id="Seg_1143" s="T85">и</ta>
            <ta e="T87" id="Seg_1144" s="T86">ум-ADJZ</ta>
            <ta e="T88" id="Seg_1145" s="T87">ты.NOM</ta>
            <ta e="T89" id="Seg_1146" s="T88">сын-OBL.2SG-ALL</ta>
            <ta e="T90" id="Seg_1147" s="T89">хороший</ta>
            <ta e="T91" id="Seg_1148" s="T90">жена.[NOM]</ta>
            <ta e="T92" id="Seg_1149" s="T91">быть-INFER-FUT-3SG.S</ta>
            <ta e="T93" id="Seg_1150" s="T92">отец.[NOM]-1SG</ta>
            <ta e="T94" id="Seg_1151" s="T93">думать-CVB</ta>
            <ta e="T95" id="Seg_1152" s="T94">думать-CVB</ta>
            <ta e="T96" id="Seg_1153" s="T95">и</ta>
            <ta e="T97" id="Seg_1154" s="T96">сказать-3SG.S</ta>
            <ta e="T98" id="Seg_1155" s="T97">узнать-OPT-1DU</ta>
            <ta e="T99" id="Seg_1156" s="T98">ум-ADJZ</ta>
            <ta e="T100" id="Seg_1157" s="T99">быть-3SG.S</ta>
            <ta e="T101" id="Seg_1158" s="T100">этот</ta>
            <ta e="T102" id="Seg_1159" s="T101">девушка.[NOM]</ta>
            <ta e="T103" id="Seg_1160" s="T102">али</ta>
            <ta e="T104" id="Seg_1161" s="T103">NEG</ta>
            <ta e="T105" id="Seg_1162" s="T104">посылать-OPT-1DU</ta>
            <ta e="T106" id="Seg_1163" s="T105">женщина-ADJZ-EP-человек.[NOM]</ta>
            <ta e="T107" id="Seg_1164" s="T106">чтобы</ta>
            <ta e="T108" id="Seg_1165" s="T107">сходить-HAB-INF</ta>
            <ta e="T109" id="Seg_1166" s="T108">он(а)-EP-ALL</ta>
            <ta e="T110" id="Seg_1167" s="T109">этот</ta>
            <ta e="T111" id="Seg_1168" s="T110">девушка-ALL</ta>
            <ta e="T112" id="Seg_1169" s="T111">говорить-INF</ta>
            <ta e="T113" id="Seg_1170" s="T112">выйти.замуж-FUT-3SG.S</ta>
            <ta e="T114" id="Seg_1171" s="T113">али</ta>
            <ta e="T115" id="Seg_1172" s="T114">NEG</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1173" s="T1">n-n&gt;adj</ta>
            <ta e="T3" id="Seg_1174" s="T2">adj</ta>
            <ta e="T4" id="Seg_1175" s="T3">n.[n:case]</ta>
            <ta e="T5" id="Seg_1176" s="T4">adj</ta>
            <ta e="T6" id="Seg_1177" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_1178" s="T6">n.[n:case]-n:poss</ta>
            <ta e="T8" id="Seg_1179" s="T7">v-v:tense-v:mood.[v:pn]</ta>
            <ta e="T9" id="Seg_1180" s="T8">n.[n:case]-n:poss</ta>
            <ta e="T10" id="Seg_1181" s="T9">adj</ta>
            <ta e="T11" id="Seg_1182" s="T10">v-v:tense-v:mood-v:pn</ta>
            <ta e="T12" id="Seg_1183" s="T11">quant</ta>
            <ta e="T13" id="Seg_1184" s="T12">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_1185" s="T13">n-n:case.poss</ta>
            <ta e="T15" id="Seg_1186" s="T14">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_1187" s="T15">adj</ta>
            <ta e="T17" id="Seg_1188" s="T16">n-n:num-n:case</ta>
            <ta e="T18" id="Seg_1189" s="T17">n-n:num-n:case</ta>
            <ta e="T19" id="Seg_1190" s="T18">n-n:num.[n:case]</ta>
            <ta e="T20" id="Seg_1191" s="T19">v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T21" id="Seg_1192" s="T20">n-n:case</ta>
            <ta e="T22" id="Seg_1193" s="T21">n-n:ins-n:num-n:case</ta>
            <ta e="T23" id="Seg_1194" s="T22">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_1195" s="T23">conj</ta>
            <ta e="T25" id="Seg_1196" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_1197" s="T25">n-n:num-n:case</ta>
            <ta e="T27" id="Seg_1198" s="T26">adj</ta>
            <ta e="T28" id="Seg_1199" s="T27">%%</ta>
            <ta e="T29" id="Seg_1200" s="T28">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_1201" s="T29">conj</ta>
            <ta e="T31" id="Seg_1202" s="T30">n-n:num-n:case</ta>
            <ta e="T32" id="Seg_1203" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_1204" s="T32">adj</ta>
            <ta e="T34" id="Seg_1205" s="T33">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T35" id="Seg_1206" s="T34">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T36" id="Seg_1207" s="T35">adj-n:ins-n:case</ta>
            <ta e="T37" id="Seg_1208" s="T36">n-n:ins-n:case.poss</ta>
            <ta e="T38" id="Seg_1209" s="T37">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T39" id="Seg_1210" s="T38">dem</ta>
            <ta e="T40" id="Seg_1211" s="T39">v-v:inf</ta>
            <ta e="T41" id="Seg_1212" s="T40">n-n:case.poss</ta>
            <ta e="T42" id="Seg_1213" s="T41">v-v:tense-v:pn</ta>
            <ta e="T43" id="Seg_1214" s="T42">n.[n:case]</ta>
            <ta e="T44" id="Seg_1215" s="T43">v-v:inf</ta>
            <ta e="T45" id="Seg_1216" s="T44">n.[n:case]-n:poss</ta>
            <ta e="T46" id="Seg_1217" s="T45">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T47" id="Seg_1218" s="T46">v-v:inf</ta>
            <ta e="T48" id="Seg_1219" s="T47">n-n&gt;n-n:obl.poss-n:case</ta>
            <ta e="T49" id="Seg_1220" s="T48">v-v&gt;v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T50" id="Seg_1221" s="T49">n-n:case</ta>
            <ta e="T51" id="Seg_1222" s="T50">v-v&gt;v-v:inf</ta>
            <ta e="T52" id="Seg_1223" s="T51">adj-adj&gt;adj</ta>
            <ta e="T53" id="Seg_1224" s="T52">n-n:case</ta>
            <ta e="T54" id="Seg_1225" s="T53">dem</ta>
            <ta e="T55" id="Seg_1226" s="T54">n.[n:case]</ta>
            <ta e="T56" id="Seg_1227" s="T55">v-v:tense.[v:pn]</ta>
            <ta e="T57" id="Seg_1228" s="T56">n-n:case</ta>
            <ta e="T58" id="Seg_1229" s="T57">v-v&gt;v-v:inf</ta>
            <ta e="T59" id="Seg_1230" s="T58">num</ta>
            <ta e="T60" id="Seg_1231" s="T59">num</ta>
            <ta e="T61" id="Seg_1232" s="T60">n-n:case</ta>
            <ta e="T62" id="Seg_1233" s="T61">v-v:tense.[v:pn]</ta>
            <ta e="T63" id="Seg_1234" s="T62">adj</ta>
            <ta e="T64" id="Seg_1235" s="T63">n-n:case</ta>
            <ta e="T65" id="Seg_1236" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_1237" s="T65">v-v:tense-v:pn</ta>
            <ta e="T67" id="Seg_1238" s="T66">num-n:ins-num&gt;adj</ta>
            <ta e="T68" id="Seg_1239" s="T67">n-n:case</ta>
            <ta e="T69" id="Seg_1240" s="T68">v-v:tense-v:pn</ta>
            <ta e="T70" id="Seg_1241" s="T69">adj</ta>
            <ta e="T71" id="Seg_1242" s="T70">n-n:case</ta>
            <ta e="T72" id="Seg_1243" s="T71">adj</ta>
            <ta e="T73" id="Seg_1244" s="T72">n.[n:case]</ta>
            <ta e="T74" id="Seg_1245" s="T73">v-v:tense-v:mood-v:pn</ta>
            <ta e="T75" id="Seg_1246" s="T74">n-n&gt;adj</ta>
            <ta e="T76" id="Seg_1247" s="T75">n-n:case</ta>
            <ta e="T77" id="Seg_1248" s="T76">adv</ta>
            <ta e="T78" id="Seg_1249" s="T77">v-v:tense.[v:pn]</ta>
            <ta e="T79" id="Seg_1250" s="T78">n-n:case</ta>
            <ta e="T80" id="Seg_1251" s="T79">conj</ta>
            <ta e="T81" id="Seg_1252" s="T80">v-v:pn</ta>
            <ta e="T82" id="Seg_1253" s="T81">conj</ta>
            <ta e="T83" id="Seg_1254" s="T82">v-v:ins-v:pn</ta>
            <ta e="T84" id="Seg_1255" s="T83">n-n:case</ta>
            <ta e="T85" id="Seg_1256" s="T84">adj</ta>
            <ta e="T86" id="Seg_1257" s="T85">conj</ta>
            <ta e="T87" id="Seg_1258" s="T86">n-n&gt;adj</ta>
            <ta e="T88" id="Seg_1259" s="T87">pers</ta>
            <ta e="T89" id="Seg_1260" s="T88">n-n:obl.poss-n:case</ta>
            <ta e="T90" id="Seg_1261" s="T89">adj</ta>
            <ta e="T91" id="Seg_1262" s="T90">n.[n:case]</ta>
            <ta e="T92" id="Seg_1263" s="T91">v-v:mood-v:tense-v:pn</ta>
            <ta e="T93" id="Seg_1264" s="T92">n.[n:case]-n:poss</ta>
            <ta e="T94" id="Seg_1265" s="T93">v-v&gt;adv</ta>
            <ta e="T95" id="Seg_1266" s="T94">v-v&gt;adv</ta>
            <ta e="T96" id="Seg_1267" s="T95">conj</ta>
            <ta e="T97" id="Seg_1268" s="T96">v-v:pn</ta>
            <ta e="T98" id="Seg_1269" s="T97">v-v:mood-v:pn</ta>
            <ta e="T99" id="Seg_1270" s="T98">n-n&gt;adj</ta>
            <ta e="T100" id="Seg_1271" s="T99">v-v:pn</ta>
            <ta e="T101" id="Seg_1272" s="T100">dem</ta>
            <ta e="T102" id="Seg_1273" s="T101">n.[n:case]</ta>
            <ta e="T103" id="Seg_1274" s="T102">conj</ta>
            <ta e="T104" id="Seg_1275" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_1276" s="T104">v-v:mood-v:pn</ta>
            <ta e="T106" id="Seg_1277" s="T105">n-n&gt;adj-n:ins-n.[n:case]</ta>
            <ta e="T107" id="Seg_1278" s="T106">conj</ta>
            <ta e="T108" id="Seg_1279" s="T107">v-v&gt;v-v:inf</ta>
            <ta e="T109" id="Seg_1280" s="T108">pers-n:ins-n:case</ta>
            <ta e="T110" id="Seg_1281" s="T109">dem</ta>
            <ta e="T111" id="Seg_1282" s="T110">n-n:case</ta>
            <ta e="T112" id="Seg_1283" s="T111">v-v:inf</ta>
            <ta e="T113" id="Seg_1284" s="T112">v-v:tense-v:pn</ta>
            <ta e="T114" id="Seg_1285" s="T113">conj</ta>
            <ta e="T115" id="Seg_1286" s="T114">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1287" s="T1">adj</ta>
            <ta e="T3" id="Seg_1288" s="T2">adj</ta>
            <ta e="T4" id="Seg_1289" s="T3">n</ta>
            <ta e="T5" id="Seg_1290" s="T4">adj</ta>
            <ta e="T6" id="Seg_1291" s="T5">n</ta>
            <ta e="T7" id="Seg_1292" s="T6">n</ta>
            <ta e="T8" id="Seg_1293" s="T7">v</ta>
            <ta e="T9" id="Seg_1294" s="T8">n</ta>
            <ta e="T10" id="Seg_1295" s="T9">adj</ta>
            <ta e="T11" id="Seg_1296" s="T10">v</ta>
            <ta e="T12" id="Seg_1297" s="T11">quant</ta>
            <ta e="T13" id="Seg_1298" s="T12">v</ta>
            <ta e="T14" id="Seg_1299" s="T13">n</ta>
            <ta e="T15" id="Seg_1300" s="T14">v</ta>
            <ta e="T16" id="Seg_1301" s="T15">adj</ta>
            <ta e="T17" id="Seg_1302" s="T16">n</ta>
            <ta e="T18" id="Seg_1303" s="T17">n</ta>
            <ta e="T19" id="Seg_1304" s="T18">n</ta>
            <ta e="T20" id="Seg_1305" s="T19">v</ta>
            <ta e="T21" id="Seg_1306" s="T20">n</ta>
            <ta e="T22" id="Seg_1307" s="T21">n</ta>
            <ta e="T23" id="Seg_1308" s="T22">v</ta>
            <ta e="T24" id="Seg_1309" s="T23">conj</ta>
            <ta e="T25" id="Seg_1310" s="T24">n</ta>
            <ta e="T26" id="Seg_1311" s="T25">n</ta>
            <ta e="T27" id="Seg_1312" s="T26">adj</ta>
            <ta e="T29" id="Seg_1313" s="T28">v</ta>
            <ta e="T30" id="Seg_1314" s="T29">conj</ta>
            <ta e="T31" id="Seg_1315" s="T30">n</ta>
            <ta e="T32" id="Seg_1316" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_1317" s="T32">adj</ta>
            <ta e="T34" id="Seg_1318" s="T33">v</ta>
            <ta e="T35" id="Seg_1319" s="T34">v</ta>
            <ta e="T36" id="Seg_1320" s="T35">adj</ta>
            <ta e="T37" id="Seg_1321" s="T36">n</ta>
            <ta e="T38" id="Seg_1322" s="T37">v</ta>
            <ta e="T39" id="Seg_1323" s="T38">dem</ta>
            <ta e="T40" id="Seg_1324" s="T39">v</ta>
            <ta e="T41" id="Seg_1325" s="T40">n</ta>
            <ta e="T42" id="Seg_1326" s="T41">v</ta>
            <ta e="T43" id="Seg_1327" s="T42">v</ta>
            <ta e="T44" id="Seg_1328" s="T43">v</ta>
            <ta e="T45" id="Seg_1329" s="T44">n</ta>
            <ta e="T46" id="Seg_1330" s="T45">v</ta>
            <ta e="T47" id="Seg_1331" s="T46">v</ta>
            <ta e="T48" id="Seg_1332" s="T47">n</ta>
            <ta e="T49" id="Seg_1333" s="T48">v</ta>
            <ta e="T50" id="Seg_1334" s="T49">n</ta>
            <ta e="T51" id="Seg_1335" s="T50">v</ta>
            <ta e="T52" id="Seg_1336" s="T51">adj</ta>
            <ta e="T53" id="Seg_1337" s="T52">n</ta>
            <ta e="T54" id="Seg_1338" s="T53">dem</ta>
            <ta e="T55" id="Seg_1339" s="T54">n</ta>
            <ta e="T56" id="Seg_1340" s="T55">v</ta>
            <ta e="T57" id="Seg_1341" s="T56">n</ta>
            <ta e="T58" id="Seg_1342" s="T57">v</ta>
            <ta e="T59" id="Seg_1343" s="T58">num</ta>
            <ta e="T60" id="Seg_1344" s="T59">num</ta>
            <ta e="T61" id="Seg_1345" s="T60">n</ta>
            <ta e="T62" id="Seg_1346" s="T61">v</ta>
            <ta e="T63" id="Seg_1347" s="T62">adj</ta>
            <ta e="T64" id="Seg_1348" s="T63">n</ta>
            <ta e="T65" id="Seg_1349" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_1350" s="T65">v</ta>
            <ta e="T67" id="Seg_1351" s="T66">adj</ta>
            <ta e="T68" id="Seg_1352" s="T67">n</ta>
            <ta e="T69" id="Seg_1353" s="T68">v</ta>
            <ta e="T70" id="Seg_1354" s="T69">adj</ta>
            <ta e="T71" id="Seg_1355" s="T70">n</ta>
            <ta e="T72" id="Seg_1356" s="T71">adj</ta>
            <ta e="T73" id="Seg_1357" s="T72">n</ta>
            <ta e="T74" id="Seg_1358" s="T73">v</ta>
            <ta e="T75" id="Seg_1359" s="T74">adj</ta>
            <ta e="T76" id="Seg_1360" s="T75">n</ta>
            <ta e="T77" id="Seg_1361" s="T76">adv</ta>
            <ta e="T78" id="Seg_1362" s="T77">v</ta>
            <ta e="T79" id="Seg_1363" s="T78">n</ta>
            <ta e="T80" id="Seg_1364" s="T79">conj</ta>
            <ta e="T81" id="Seg_1365" s="T80">v</ta>
            <ta e="T82" id="Seg_1366" s="T81">conj</ta>
            <ta e="T83" id="Seg_1367" s="T82">v</ta>
            <ta e="T84" id="Seg_1368" s="T83">n</ta>
            <ta e="T85" id="Seg_1369" s="T84">adj</ta>
            <ta e="T86" id="Seg_1370" s="T85">conj</ta>
            <ta e="T87" id="Seg_1371" s="T86">adj</ta>
            <ta e="T88" id="Seg_1372" s="T87">pers</ta>
            <ta e="T89" id="Seg_1373" s="T88">n</ta>
            <ta e="T90" id="Seg_1374" s="T89">adj</ta>
            <ta e="T91" id="Seg_1375" s="T90">n</ta>
            <ta e="T92" id="Seg_1376" s="T91">v</ta>
            <ta e="T93" id="Seg_1377" s="T92">n</ta>
            <ta e="T94" id="Seg_1378" s="T93">adv</ta>
            <ta e="T95" id="Seg_1379" s="T94">adv</ta>
            <ta e="T96" id="Seg_1380" s="T95">conj</ta>
            <ta e="T97" id="Seg_1381" s="T96">v</ta>
            <ta e="T98" id="Seg_1382" s="T97">v</ta>
            <ta e="T99" id="Seg_1383" s="T98">adj</ta>
            <ta e="T100" id="Seg_1384" s="T99">v</ta>
            <ta e="T101" id="Seg_1385" s="T100">dem</ta>
            <ta e="T102" id="Seg_1386" s="T101">n</ta>
            <ta e="T103" id="Seg_1387" s="T102">conj</ta>
            <ta e="T104" id="Seg_1388" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_1389" s="T104">v</ta>
            <ta e="T106" id="Seg_1390" s="T105">n</ta>
            <ta e="T107" id="Seg_1391" s="T106">conj</ta>
            <ta e="T108" id="Seg_1392" s="T107">v</ta>
            <ta e="T109" id="Seg_1393" s="T108">pers</ta>
            <ta e="T110" id="Seg_1394" s="T109">dem</ta>
            <ta e="T111" id="Seg_1395" s="T110">n</ta>
            <ta e="T112" id="Seg_1396" s="T111">v</ta>
            <ta e="T113" id="Seg_1397" s="T112">v</ta>
            <ta e="T114" id="Seg_1398" s="T113">conj</ta>
            <ta e="T115" id="Seg_1399" s="T114">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T12" id="Seg_1400" s="T11">RUS:core</ta>
            <ta e="T16" id="Seg_1401" s="T15">RUS:core</ta>
            <ta e="T17" id="Seg_1402" s="T16">RUS:cult</ta>
            <ta e="T24" id="Seg_1403" s="T23">RUS:gram</ta>
            <ta e="T27" id="Seg_1404" s="T26">RUS:core</ta>
            <ta e="T30" id="Seg_1405" s="T29">RUS:gram</ta>
            <ta e="T33" id="Seg_1406" s="T32">RUS:core</ta>
            <ta e="T43" id="Seg_1407" s="T42">RUS:cult</ta>
            <ta e="T48" id="Seg_1408" s="T47">RUS:cult</ta>
            <ta e="T55" id="Seg_1409" s="T54">RUS:cult</ta>
            <ta e="T61" id="Seg_1410" s="T60">RUS:cult</ta>
            <ta e="T73" id="Seg_1411" s="T72">RUS:core</ta>
            <ta e="T80" id="Seg_1412" s="T79">RUS:gram</ta>
            <ta e="T82" id="Seg_1413" s="T81">RUS:gram</ta>
            <ta e="T86" id="Seg_1414" s="T85">RUS:gram</ta>
            <ta e="T96" id="Seg_1415" s="T95">RUS:gram</ta>
            <ta e="T103" id="Seg_1416" s="T102">RUS:gram</ta>
            <ta e="T107" id="Seg_1417" s="T106">RUS:gram</ta>
            <ta e="T114" id="Seg_1418" s="T113">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_1419" s="T1">A clever girl.</ta>
            <ta e="T8" id="Seg_1420" s="T4">One good man had a son.</ta>
            <ta e="T11" id="Seg_1421" s="T8">His son was a good man.</ta>
            <ta e="T13" id="Seg_1422" s="T11">He was making everything.</ta>
            <ta e="T26" id="Seg_1423" s="T13">He made various things from wood: he made beds, he carved bed legs and heads.</ta>
            <ta e="T35" id="Seg_1424" s="T26">He made various (carvings?), he also made, he carved shelves.</ta>
            <ta e="T40" id="Seg_1425" s="T35">People came from other lands to buy these things.</ta>
            <ta e="T44" id="Seg_1426" s="T40">It was time for him to get married.</ta>
            <ta e="T47" id="Seg_1427" s="T44">He decided to get married.</ta>
            <ta e="T53" id="Seg_1428" s="T47">He said to his friend to go looking for a girl, a good girl.</ta>
            <ta e="T58" id="Seg_1429" s="T53">This friend went to look for a girl.</ta>
            <ta e="T66" id="Seg_1430" s="T58">He visited eight places, but he didn't find a good girl.</ta>
            <ta e="T71" id="Seg_1431" s="T66">In the tenth village he found a good girl.</ta>
            <ta e="T76" id="Seg_1432" s="T71">He found a good girl, a clever girl.</ta>
            <ta e="T87" id="Seg_1433" s="T76">He came back to the village and claimed to have found a good girl, a good and a clever one.</ta>
            <ta e="T92" id="Seg_1434" s="T87">“She will be a good wife for your son.”</ta>
            <ta e="T104" id="Seg_1435" s="T92">The father was thinking hard and then he said: “Let's find out whether this girl is clever or not.</ta>
            <ta e="T115" id="Seg_1436" s="T104">Let's send a woman to her, to this girl, to talk to her, whether she is going to marry or not.”</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_1437" s="T1">Ein schlaues Mädchen</ta>
            <ta e="T8" id="Seg_1438" s="T4">Ein guter Mann hatte einen Sohn.</ta>
            <ta e="T11" id="Seg_1439" s="T8">Sein Sohn war ein guter Mann.</ta>
            <ta e="T13" id="Seg_1440" s="T11">Er machte alles.</ta>
            <ta e="T26" id="Seg_1441" s="T13">Er machte verschiedene Dinge aus Holz: er baute Betten, er schnitzte Beine und Kopfteile von Betten.</ta>
            <ta e="T35" id="Seg_1442" s="T26">Er machte unterschiedliche (Schnitzereien?), er machte auch, er schnitzte Regale.</ta>
            <ta e="T40" id="Seg_1443" s="T35">Menschen kamen aus anderen Ländern, um diese Dinge zu kaufen.</ta>
            <ta e="T44" id="Seg_1444" s="T40">Es wurde Zeit für ihn zu heiraten.</ta>
            <ta e="T47" id="Seg_1445" s="T44">Er entschied sich zu heiraten.</ta>
            <ta e="T53" id="Seg_1446" s="T47">Er bat seinen Freund, nach einem Mädchen zu suchen, einem guten Mädchen.</ta>
            <ta e="T58" id="Seg_1447" s="T53">Dieser Freund ging nach einem Mädchen suchen.</ta>
            <ta e="T66" id="Seg_1448" s="T58">Er besuchte acht Orte, aber er fand kein gutes Mädchen.</ta>
            <ta e="T71" id="Seg_1449" s="T66">Im zehnten Dorf fand er ein gutes Mädchen.</ta>
            <ta e="T76" id="Seg_1450" s="T71">Er fand ein gutes Mädchen, ein schlaues Mädchen.</ta>
            <ta e="T87" id="Seg_1451" s="T76">Er kam ins Dorf zurück und behauptete, ein gutes Mädchen gefunden zu haben, ein gutes und ein schlaues.</ta>
            <ta e="T92" id="Seg_1452" s="T87">"Sie wird deinem Sohn eine gute Ehefrau sein."</ta>
            <ta e="T104" id="Seg_1453" s="T92">Der Vater dachte scharf nach und dann sagte er: "Lass uns herausfinden, ob dieses Mädchen schlau ist oder nicht.</ta>
            <ta e="T115" id="Seg_1454" s="T104">Lass uns eine Frau zu ihr schicken, um mit ihr zu besprechen, ob sie heiraten wird oder nicht."</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_1455" s="T1">Умная (хорошая) девушка.</ta>
            <ta e="T8" id="Seg_1456" s="T4">У хорошего человека сын был.</ta>
            <ta e="T11" id="Seg_1457" s="T8">Сын хороший был.</ta>
            <ta e="T13" id="Seg_1458" s="T11">Все делал.</ta>
            <ta e="T26" id="Seg_1459" s="T13">Из дерева делал всякие (вырезки) вещи: кровати делал, ножки кровати точил и изголовья у кровати.</ta>
            <ta e="T35" id="Seg_1460" s="T26">Всякие вырезки(?) делал, и полки тоже вырезал, делал.</ta>
            <ta e="T40" id="Seg_1461" s="T35">Из других стран приезжали это покупать.</ta>
            <ta e="T44" id="Seg_1462" s="T40">Сыну подошло время жениться.</ta>
            <ta e="T47" id="Seg_1463" s="T44">Сын вздумал жениться.</ta>
            <ta e="T53" id="Seg_1464" s="T47">Товарищу сказал невесту искать, хорошенькую девчонку.</ta>
            <ta e="T58" id="Seg_1465" s="T53">Этот товарищ пошел девушку искать.</ta>
            <ta e="T66" id="Seg_1466" s="T58">В восьми местах был, хорошую девушку не нашел.</ta>
            <ta e="T71" id="Seg_1467" s="T66">В десятой деревне нашел хорошую девушку.</ta>
            <ta e="T76" id="Seg_1468" s="T71">Хорошую девушку нашел, умную девушку.</ta>
            <ta e="T87" id="Seg_1469" s="T76">Обратно пришел в деревню и сказал, что, мол, я нашел девушку, хорошую и умную.</ta>
            <ta e="T92" id="Seg_1470" s="T87">“Твоему сыну будет хорошая жена”.</ta>
            <ta e="T104" id="Seg_1471" s="T92">Отец думал, думал и сказал: “Узнаем, умная эта девушка или нет.</ta>
            <ta e="T115" id="Seg_1472" s="T104">Отправим женщину, чтоб сходить к ней, к этой девушке, поговорить, замуж пойдет или нет.”</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_1473" s="T1">умная (хорошая) девушка</ta>
            <ta e="T8" id="Seg_1474" s="T4">у хорошего человека сын был</ta>
            <ta e="T11" id="Seg_1475" s="T8">сын хороший был</ta>
            <ta e="T13" id="Seg_1476" s="T11">все делал</ta>
            <ta e="T26" id="Seg_1477" s="T13">из дерева делал всякие (вырезки) вещи кровати делал (точеные) ножки кровати точил и головки у кровати</ta>
            <ta e="T35" id="Seg_1478" s="T26">всякие вырезки делал и полки тоже вырезал (со всякими вырезками) делал</ta>
            <ta e="T40" id="Seg_1479" s="T35">и(з) других стран приезжали это покупать</ta>
            <ta e="T44" id="Seg_1480" s="T40">сыну подошло время жениться</ta>
            <ta e="T47" id="Seg_1481" s="T44">сын вздумал жениться</ta>
            <ta e="T53" id="Seg_1482" s="T47">товарищу сказал невесту искать хорошенькую девчонку</ta>
            <ta e="T58" id="Seg_1483" s="T53">этот товарищ пошел невесту искать</ta>
            <ta e="T66" id="Seg_1484" s="T58">в восьми деревнях был хорошую девушку не нашел</ta>
            <ta e="T71" id="Seg_1485" s="T66">в десятой деревне нашел хорошую девку</ta>
            <ta e="T76" id="Seg_1486" s="T71">хорошую девку нашел умную</ta>
            <ta e="T87" id="Seg_1487" s="T76">обратно пришел в деревню и сказал что нашел девку хорошую и умную</ta>
            <ta e="T92" id="Seg_1488" s="T87">твоему сыну будет хорошая жена</ta>
            <ta e="T104" id="Seg_1489" s="T92">отец думал думал и сказал узнаем умная эта девка или нет</ta>
            <ta e="T115" id="Seg_1490" s="T104">отп(р)авим женщину чтоб сходить к ней к этой девушке поговорить замуж пойдет или нет</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T4" id="Seg_1491" s="T1">[BrM:] The text seems to be just the begining of a fairy tale.</ta>
            <ta e="T35" id="Seg_1492" s="T26">[BrM:] Tentative analysis of 'maʒelgumbat'.</ta>
            <ta e="T40" id="Seg_1493" s="T35">[BrM:] Tentative analysis of 'arɨn'. [BrM:] More probable analysis: 'tauqugu' – 'buy-HAB-INF'</ta>
            <ta e="T53" id="Seg_1494" s="T47">[KuAI:] Variant: 'swaːlaqi'.</ta>
            <ta e="T104" id="Seg_1495" s="T92">[BrM:] 'ezew': why 1SG?</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
