<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>ChAE_196X_HungryDaughterInLaw_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">ChAE_196X_HungryDaughterInLaw_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">191</ud-information>
            <ud-information attribute-name="# HIAT:w">136</ud-information>
            <ud-information attribute-name="# e">136</ud-information>
            <ud-information attribute-name="# HIAT:u">27</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="ChAE">
            <abbreviation>ChAE</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="ChAE"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T137" id="Seg_0" n="sc" s="T1">
               <ts e="T12" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Ilʼdʼät</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">passat</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_9" n="HIAT:ip">(</nts>
                  <ts e="T4" id="Seg_11" n="HIAT:w" s="T3">era</ts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_14" n="HIAT:w" s="T4">pajasaɣə</ts>
                  <nts id="Seg_15" n="HIAT:ip">)</nts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_18" n="HIAT:w" s="T5">nädɨgu</ts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_21" n="HIAT:w" s="T6">kɨgelɨmbaɣi</ts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_24" n="HIAT:w" s="T7">iːndi</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_26" n="HIAT:ip">(</nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">nädɨgu</ts>
                  <nts id="Seg_29" n="HIAT:ip">)</nts>
                  <nts id="Seg_30" n="HIAT:ip">,</nts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_33" n="HIAT:w" s="T9">aːrɨŋ</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_36" n="HIAT:w" s="T10">jedʼeɣɨndo</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_39" n="HIAT:w" s="T11">iːmbadi</ts>
                  <nts id="Seg_40" n="HIAT:ip">.</nts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_43" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_45" n="HIAT:w" s="T12">Iːmbat</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_48" n="HIAT:w" s="T13">näːdäm</ts>
                  <nts id="Seg_49" n="HIAT:ip">.</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_52" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_54" n="HIAT:w" s="T14">Iːmdə</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_57" n="HIAT:w" s="T15">nädämbadi</ts>
                  <nts id="Seg_58" n="HIAT:ip">.</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_61" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_63" n="HIAT:w" s="T16">Iːt</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_66" n="HIAT:w" s="T17">netlʼeːpe</ts>
                  <nts id="Seg_67" n="HIAT:ip">,</nts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">qwanba</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">kundaktɨ</ts>
                  <nts id="Seg_74" n="HIAT:ip">.</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_77" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_79" n="HIAT:w" s="T20">Ilʼdäntse</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_82" n="HIAT:w" s="T21">passandɨze</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_85" n="HIAT:w" s="T22">pajamda</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">qwädimbat</ts>
                  <nts id="Seg_89" n="HIAT:ip">.</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_92" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_94" n="HIAT:w" s="T24">Ilʼdʼät</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_97" n="HIAT:w" s="T25">pize</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_100" n="HIAT:w" s="T26">delǯe</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_103" n="HIAT:w" s="T27">uːdimba</ts>
                  <nts id="Seg_104" n="HIAT:ip">.</nts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_107" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_109" n="HIAT:w" s="T28">I</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_112" n="HIAT:w" s="T29">passat</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_115" n="HIAT:w" s="T30">näj</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_118" n="HIAT:w" s="T31">udində</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_121" n="HIAT:w" s="T32">näjɣum</ts>
                  <nts id="Seg_122" n="HIAT:ip">.</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_125" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_127" n="HIAT:w" s="T33">Silʼna</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_130" n="HIAT:w" s="T34">näɣalbɨdi</ts>
                  <nts id="Seg_131" n="HIAT:ip">.</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_134" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_136" n="HIAT:w" s="T35">I</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_139" n="HIAT:w" s="T36">snaxamtə</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_142" n="HIAT:w" s="T37">uːdigu</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_145" n="HIAT:w" s="T38">kuralʼǯekumbadi</ts>
                  <nts id="Seg_146" n="HIAT:ip">.</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_149" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_151" n="HIAT:w" s="T39">A</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_154" n="HIAT:w" s="T40">opsodimmɨm</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_157" n="HIAT:w" s="T41">swaŋ</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_160" n="HIAT:w" s="T42">as</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_163" n="HIAT:w" s="T43">mekumbadi</ts>
                  <nts id="Seg_164" n="HIAT:ip">.</nts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_167" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_169" n="HIAT:w" s="T44">Swa</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_172" n="HIAT:w" s="T45">apsodam</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_175" n="HIAT:w" s="T46">täp</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_178" n="HIAT:w" s="T47">az</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_181" n="HIAT:w" s="T48">awkumbat</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_184" n="HIAT:w" s="T49">täbɨstaɣɨze</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_187" n="HIAT:w" s="T50">warkɨlʼe</ts>
                  <nts id="Seg_188" n="HIAT:ip">.</nts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_191" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_193" n="HIAT:w" s="T51">Ilʼdät</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_196" n="HIAT:w" s="T52">äːʒulgumban</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_199" n="HIAT:w" s="T53">aurgu</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_202" n="HIAT:w" s="T54">nadə</ts>
                  <nts id="Seg_203" n="HIAT:ip">.</nts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_206" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_208" n="HIAT:w" s="T55">Ustolʼǯit</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_211" n="HIAT:w" s="T56">taqqɨlgumbat</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_213" n="HIAT:ip">(</nts>
                  <ts e="T58" id="Seg_215" n="HIAT:w" s="T57">taqqɨlgu</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_218" n="HIAT:w" s="T58">nadə</ts>
                  <nts id="Seg_219" n="HIAT:ip">)</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_222" n="HIAT:w" s="T59">aurgu</ts>
                  <nts id="Seg_223" n="HIAT:ip">,</nts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_226" n="HIAT:w" s="T60">nʼäjam</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_229" n="HIAT:w" s="T61">maʒelgumbatt</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_231" n="HIAT:ip">(</nts>
                  <ts e="T63" id="Seg_233" n="HIAT:w" s="T62">nʼäjam</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_236" n="HIAT:w" s="T63">maʒelgu</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_239" n="HIAT:w" s="T64">nado</ts>
                  <nts id="Seg_240" n="HIAT:ip">)</nts>
                  <nts id="Seg_241" n="HIAT:ip">.</nts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_244" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_246" n="HIAT:w" s="T65">Nagur</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_249" n="HIAT:w" s="T66">lamotinʼ</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_252" n="HIAT:w" s="T67">dʼär</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_255" n="HIAT:w" s="T68">aukumbattə</ts>
                  <nts id="Seg_256" n="HIAT:ip">.</nts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_259" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_261" n="HIAT:w" s="T69">Ilʼdʼät</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_264" n="HIAT:w" s="T70">äːǯougumba</ts>
                  <nts id="Seg_265" n="HIAT:ip">:</nts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_267" n="HIAT:ip">“</nts>
                  <ts e="T72" id="Seg_269" n="HIAT:w" s="T71">Qabɨǯaj</ts>
                  <nts id="Seg_270" n="HIAT:ip">.</nts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_273" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_275" n="HIAT:w" s="T72">To</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_278" n="HIAT:w" s="T73">qwandelgu</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_281" n="HIAT:w" s="T74">nʼäim</ts>
                  <nts id="Seg_282" n="HIAT:ip">,</nts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_285" n="HIAT:w" s="T75">wes</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_288" n="HIAT:w" s="T76">apsodʼimɨm</ts>
                  <nts id="Seg_289" n="HIAT:ip">.</nts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_292" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_294" n="HIAT:w" s="T77">Me</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_297" n="HIAT:w" s="T78">teper</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_300" n="HIAT:w" s="T79">kabəǯimbaj</ts>
                  <nts id="Seg_301" n="HIAT:ip">.</nts>
                  <nts id="Seg_302" n="HIAT:ip">”</nts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_305" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_307" n="HIAT:w" s="T80">Snoxat</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_310" n="HIAT:w" s="T81">ubiraimkumbat</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_313" n="HIAT:w" s="T82">wes</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_316" n="HIAT:w" s="T83">apsodimɨm</ts>
                  <nts id="Seg_317" n="HIAT:ip">.</nts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_320" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_322" n="HIAT:w" s="T84">Ilʼdʼät</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_325" n="HIAT:w" s="T85">i</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_328" n="HIAT:w" s="T86">passat</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_331" n="HIAT:w" s="T87">auwkumbadi</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_334" n="HIAT:w" s="T88">twelaŋ</ts>
                  <nts id="Seg_335" n="HIAT:ip">,</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_338" n="HIAT:w" s="T89">aurgumbaɣə</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_341" n="HIAT:w" s="T90">twelaŋ</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_344" n="HIAT:w" s="T91">ondi</ts>
                  <nts id="Seg_345" n="HIAT:ip">.</nts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_348" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_350" n="HIAT:w" s="T92">A</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_353" n="HIAT:w" s="T93">täp</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_356" n="HIAT:w" s="T94">amnaŋ</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_359" n="HIAT:w" s="T95">eːkumba</ts>
                  <nts id="Seg_360" n="HIAT:ip">.</nts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_363" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_365" n="HIAT:w" s="T96">Uːdikumba</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_368" n="HIAT:w" s="T97">qwezɨlʼe</ts>
                  <nts id="Seg_369" n="HIAT:ip">,</nts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_372" n="HIAT:w" s="T98">aurgu</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_375" n="HIAT:w" s="T99">kɨgɨkumba</ts>
                  <nts id="Seg_376" n="HIAT:ip">.</nts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_379" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_381" n="HIAT:w" s="T100">Snoxat</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_384" n="HIAT:w" s="T101">amnaŋ</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_387" n="HIAT:w" s="T102">ekumbaŋ</ts>
                  <nts id="Seg_388" n="HIAT:ip">,</nts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_391" n="HIAT:w" s="T103">ekumbaŋ</ts>
                  <nts id="Seg_392" n="HIAT:ip">,</nts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_395" n="HIAT:w" s="T104">ondə</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_398" n="HIAT:w" s="T105">ewɨntnä</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_401" n="HIAT:w" s="T106">qwanba</ts>
                  <nts id="Seg_402" n="HIAT:ip">.</nts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T114" id="Seg_405" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_407" n="HIAT:w" s="T107">Ewɨntnä</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_410" n="HIAT:w" s="T108">äːǯolban</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_413" n="HIAT:w" s="T109">što</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_415" n="HIAT:ip">“</nts>
                  <ts e="T111" id="Seg_417" n="HIAT:w" s="T110">Amnaŋ</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_420" n="HIAT:w" s="T111">qwajawan</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_422" n="HIAT:ip">(</nts>
                  <ts e="T113" id="Seg_424" n="HIAT:w" s="T112">amnaŋ</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_427" n="HIAT:w" s="T113">palʼdʼäŋ</ts>
                  <nts id="Seg_428" n="HIAT:ip">)</nts>
                  <nts id="Seg_429" n="HIAT:ip">.</nts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_432" n="HIAT:u" s="T114">
                  <ts e="T115" id="Seg_434" n="HIAT:w" s="T114">Aurgu</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_437" n="HIAT:w" s="T115">ass</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_440" n="HIAT:w" s="T116">mekudi</ts>
                  <nts id="Seg_441" n="HIAT:ip">,</nts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_444" n="HIAT:w" s="T117">man</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_447" n="HIAT:w" s="T118">ass</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_450" n="HIAT:w" s="T119">kabɨǯiqwaŋ</ts>
                  <nts id="Seg_451" n="HIAT:ip">.</nts>
                  <nts id="Seg_452" n="HIAT:ip">”</nts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_455" n="HIAT:u" s="T120">
                  <ts e="T121" id="Seg_457" n="HIAT:w" s="T120">Snoxadnänan</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_460" n="HIAT:w" s="T121">nagur</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_463" n="HIAT:w" s="T122">agat</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_466" n="HIAT:w" s="T123">epant</ts>
                  <nts id="Seg_467" n="HIAT:ip">.</nts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T132" id="Seg_470" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_472" n="HIAT:w" s="T124">Timnʼät</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_475" n="HIAT:w" s="T125">äːǯukumban</ts>
                  <nts id="Seg_476" n="HIAT:ip">:</nts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_478" n="HIAT:ip">“</nts>
                  <ts e="T127" id="Seg_480" n="HIAT:w" s="T126">Qwannɨk</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_483" n="HIAT:w" s="T127">maːt</ts>
                  <nts id="Seg_484" n="HIAT:ip">,</nts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_487" n="HIAT:w" s="T128">ettarɨk</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_490" n="HIAT:w" s="T129">swaj</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_493" n="HIAT:w" s="T130">apsot</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_496" n="HIAT:w" s="T131">potk</ts>
                  <nts id="Seg_497" n="HIAT:ip">.</nts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T137" id="Seg_500" n="HIAT:u" s="T132">
                  <ts e="T133" id="Seg_502" n="HIAT:w" s="T132">A</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_505" n="HIAT:w" s="T133">man</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_508" n="HIAT:w" s="T134">tekga</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_511" n="HIAT:w" s="T135">tʼülaks</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_514" n="HIAT:w" s="T136">aːmdedʼiku</ts>
                  <nts id="Seg_515" n="HIAT:ip">.</nts>
                  <nts id="Seg_516" n="HIAT:ip">”</nts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T137" id="Seg_518" n="sc" s="T1">
               <ts e="T2" id="Seg_520" n="e" s="T1">Ilʼdʼät </ts>
               <ts e="T3" id="Seg_522" n="e" s="T2">passat </ts>
               <ts e="T4" id="Seg_524" n="e" s="T3">(era </ts>
               <ts e="T5" id="Seg_526" n="e" s="T4">pajasaɣə) </ts>
               <ts e="T6" id="Seg_528" n="e" s="T5">nädɨgu </ts>
               <ts e="T7" id="Seg_530" n="e" s="T6">kɨgelɨmbaɣi </ts>
               <ts e="T8" id="Seg_532" n="e" s="T7">iːndi </ts>
               <ts e="T9" id="Seg_534" n="e" s="T8">(nädɨgu), </ts>
               <ts e="T10" id="Seg_536" n="e" s="T9">aːrɨŋ </ts>
               <ts e="T11" id="Seg_538" n="e" s="T10">jedʼeɣɨndo </ts>
               <ts e="T12" id="Seg_540" n="e" s="T11">iːmbadi. </ts>
               <ts e="T13" id="Seg_542" n="e" s="T12">Iːmbat </ts>
               <ts e="T14" id="Seg_544" n="e" s="T13">näːdäm. </ts>
               <ts e="T15" id="Seg_546" n="e" s="T14">Iːmdə </ts>
               <ts e="T16" id="Seg_548" n="e" s="T15">nädämbadi. </ts>
               <ts e="T17" id="Seg_550" n="e" s="T16">Iːt </ts>
               <ts e="T18" id="Seg_552" n="e" s="T17">netlʼeːpe, </ts>
               <ts e="T19" id="Seg_554" n="e" s="T18">qwanba </ts>
               <ts e="T20" id="Seg_556" n="e" s="T19">kundaktɨ. </ts>
               <ts e="T21" id="Seg_558" n="e" s="T20">Ilʼdäntse </ts>
               <ts e="T22" id="Seg_560" n="e" s="T21">passandɨze </ts>
               <ts e="T23" id="Seg_562" n="e" s="T22">pajamda </ts>
               <ts e="T24" id="Seg_564" n="e" s="T23">qwädimbat. </ts>
               <ts e="T25" id="Seg_566" n="e" s="T24">Ilʼdʼät </ts>
               <ts e="T26" id="Seg_568" n="e" s="T25">pize </ts>
               <ts e="T27" id="Seg_570" n="e" s="T26">delǯe </ts>
               <ts e="T28" id="Seg_572" n="e" s="T27">uːdimba. </ts>
               <ts e="T29" id="Seg_574" n="e" s="T28">I </ts>
               <ts e="T30" id="Seg_576" n="e" s="T29">passat </ts>
               <ts e="T31" id="Seg_578" n="e" s="T30">näj </ts>
               <ts e="T32" id="Seg_580" n="e" s="T31">udində </ts>
               <ts e="T33" id="Seg_582" n="e" s="T32">näjɣum. </ts>
               <ts e="T34" id="Seg_584" n="e" s="T33">Silʼna </ts>
               <ts e="T35" id="Seg_586" n="e" s="T34">näɣalbɨdi. </ts>
               <ts e="T36" id="Seg_588" n="e" s="T35">I </ts>
               <ts e="T37" id="Seg_590" n="e" s="T36">snaxamtə </ts>
               <ts e="T38" id="Seg_592" n="e" s="T37">uːdigu </ts>
               <ts e="T39" id="Seg_594" n="e" s="T38">kuralʼǯekumbadi. </ts>
               <ts e="T40" id="Seg_596" n="e" s="T39">A </ts>
               <ts e="T41" id="Seg_598" n="e" s="T40">opsodimmɨm </ts>
               <ts e="T42" id="Seg_600" n="e" s="T41">swaŋ </ts>
               <ts e="T43" id="Seg_602" n="e" s="T42">as </ts>
               <ts e="T44" id="Seg_604" n="e" s="T43">mekumbadi. </ts>
               <ts e="T45" id="Seg_606" n="e" s="T44">Swa </ts>
               <ts e="T46" id="Seg_608" n="e" s="T45">apsodam </ts>
               <ts e="T47" id="Seg_610" n="e" s="T46">täp </ts>
               <ts e="T48" id="Seg_612" n="e" s="T47">az </ts>
               <ts e="T49" id="Seg_614" n="e" s="T48">awkumbat </ts>
               <ts e="T50" id="Seg_616" n="e" s="T49">täbɨstaɣɨze </ts>
               <ts e="T51" id="Seg_618" n="e" s="T50">warkɨlʼe. </ts>
               <ts e="T52" id="Seg_620" n="e" s="T51">Ilʼdät </ts>
               <ts e="T53" id="Seg_622" n="e" s="T52">äːʒulgumban </ts>
               <ts e="T54" id="Seg_624" n="e" s="T53">aurgu </ts>
               <ts e="T55" id="Seg_626" n="e" s="T54">nadə. </ts>
               <ts e="T56" id="Seg_628" n="e" s="T55">Ustolʼǯit </ts>
               <ts e="T57" id="Seg_630" n="e" s="T56">taqqɨlgumbat </ts>
               <ts e="T58" id="Seg_632" n="e" s="T57">(taqqɨlgu </ts>
               <ts e="T59" id="Seg_634" n="e" s="T58">nadə) </ts>
               <ts e="T60" id="Seg_636" n="e" s="T59">aurgu, </ts>
               <ts e="T61" id="Seg_638" n="e" s="T60">nʼäjam </ts>
               <ts e="T62" id="Seg_640" n="e" s="T61">maʒelgumbatt </ts>
               <ts e="T63" id="Seg_642" n="e" s="T62">(nʼäjam </ts>
               <ts e="T64" id="Seg_644" n="e" s="T63">maʒelgu </ts>
               <ts e="T65" id="Seg_646" n="e" s="T64">nado). </ts>
               <ts e="T66" id="Seg_648" n="e" s="T65">Nagur </ts>
               <ts e="T67" id="Seg_650" n="e" s="T66">lamotinʼ </ts>
               <ts e="T68" id="Seg_652" n="e" s="T67">dʼär </ts>
               <ts e="T69" id="Seg_654" n="e" s="T68">aukumbattə. </ts>
               <ts e="T70" id="Seg_656" n="e" s="T69">Ilʼdʼät </ts>
               <ts e="T71" id="Seg_658" n="e" s="T70">äːǯougumba: </ts>
               <ts e="T72" id="Seg_660" n="e" s="T71">“Qabɨǯaj. </ts>
               <ts e="T73" id="Seg_662" n="e" s="T72">To </ts>
               <ts e="T74" id="Seg_664" n="e" s="T73">qwandelgu </ts>
               <ts e="T75" id="Seg_666" n="e" s="T74">nʼäim, </ts>
               <ts e="T76" id="Seg_668" n="e" s="T75">wes </ts>
               <ts e="T77" id="Seg_670" n="e" s="T76">apsodʼimɨm. </ts>
               <ts e="T78" id="Seg_672" n="e" s="T77">Me </ts>
               <ts e="T79" id="Seg_674" n="e" s="T78">teper </ts>
               <ts e="T80" id="Seg_676" n="e" s="T79">kabəǯimbaj.” </ts>
               <ts e="T81" id="Seg_678" n="e" s="T80">Snoxat </ts>
               <ts e="T82" id="Seg_680" n="e" s="T81">ubiraimkumbat </ts>
               <ts e="T83" id="Seg_682" n="e" s="T82">wes </ts>
               <ts e="T84" id="Seg_684" n="e" s="T83">apsodimɨm. </ts>
               <ts e="T85" id="Seg_686" n="e" s="T84">Ilʼdʼät </ts>
               <ts e="T86" id="Seg_688" n="e" s="T85">i </ts>
               <ts e="T87" id="Seg_690" n="e" s="T86">passat </ts>
               <ts e="T88" id="Seg_692" n="e" s="T87">auwkumbadi </ts>
               <ts e="T89" id="Seg_694" n="e" s="T88">twelaŋ, </ts>
               <ts e="T90" id="Seg_696" n="e" s="T89">aurgumbaɣə </ts>
               <ts e="T91" id="Seg_698" n="e" s="T90">twelaŋ </ts>
               <ts e="T92" id="Seg_700" n="e" s="T91">ondi. </ts>
               <ts e="T93" id="Seg_702" n="e" s="T92">A </ts>
               <ts e="T94" id="Seg_704" n="e" s="T93">täp </ts>
               <ts e="T95" id="Seg_706" n="e" s="T94">amnaŋ </ts>
               <ts e="T96" id="Seg_708" n="e" s="T95">eːkumba. </ts>
               <ts e="T97" id="Seg_710" n="e" s="T96">Uːdikumba </ts>
               <ts e="T98" id="Seg_712" n="e" s="T97">qwezɨlʼe, </ts>
               <ts e="T99" id="Seg_714" n="e" s="T98">aurgu </ts>
               <ts e="T100" id="Seg_716" n="e" s="T99">kɨgɨkumba. </ts>
               <ts e="T101" id="Seg_718" n="e" s="T100">Snoxat </ts>
               <ts e="T102" id="Seg_720" n="e" s="T101">amnaŋ </ts>
               <ts e="T103" id="Seg_722" n="e" s="T102">ekumbaŋ, </ts>
               <ts e="T104" id="Seg_724" n="e" s="T103">ekumbaŋ, </ts>
               <ts e="T105" id="Seg_726" n="e" s="T104">ondə </ts>
               <ts e="T106" id="Seg_728" n="e" s="T105">ewɨntnä </ts>
               <ts e="T107" id="Seg_730" n="e" s="T106">qwanba. </ts>
               <ts e="T108" id="Seg_732" n="e" s="T107">Ewɨntnä </ts>
               <ts e="T109" id="Seg_734" n="e" s="T108">äːǯolban </ts>
               <ts e="T110" id="Seg_736" n="e" s="T109">što </ts>
               <ts e="T111" id="Seg_738" n="e" s="T110">“Amnaŋ </ts>
               <ts e="T112" id="Seg_740" n="e" s="T111">qwajawan </ts>
               <ts e="T113" id="Seg_742" n="e" s="T112">(amnaŋ </ts>
               <ts e="T114" id="Seg_744" n="e" s="T113">palʼdʼäŋ). </ts>
               <ts e="T115" id="Seg_746" n="e" s="T114">Aurgu </ts>
               <ts e="T116" id="Seg_748" n="e" s="T115">ass </ts>
               <ts e="T117" id="Seg_750" n="e" s="T116">mekudi, </ts>
               <ts e="T118" id="Seg_752" n="e" s="T117">man </ts>
               <ts e="T119" id="Seg_754" n="e" s="T118">ass </ts>
               <ts e="T120" id="Seg_756" n="e" s="T119">kabɨǯiqwaŋ.” </ts>
               <ts e="T121" id="Seg_758" n="e" s="T120">Snoxadnänan </ts>
               <ts e="T122" id="Seg_760" n="e" s="T121">nagur </ts>
               <ts e="T123" id="Seg_762" n="e" s="T122">agat </ts>
               <ts e="T124" id="Seg_764" n="e" s="T123">epant. </ts>
               <ts e="T125" id="Seg_766" n="e" s="T124">Timnʼät </ts>
               <ts e="T126" id="Seg_768" n="e" s="T125">äːǯukumban: </ts>
               <ts e="T127" id="Seg_770" n="e" s="T126">“Qwannɨk </ts>
               <ts e="T128" id="Seg_772" n="e" s="T127">maːt, </ts>
               <ts e="T129" id="Seg_774" n="e" s="T128">ettarɨk </ts>
               <ts e="T130" id="Seg_776" n="e" s="T129">swaj </ts>
               <ts e="T131" id="Seg_778" n="e" s="T130">apsot </ts>
               <ts e="T132" id="Seg_780" n="e" s="T131">potk. </ts>
               <ts e="T133" id="Seg_782" n="e" s="T132">A </ts>
               <ts e="T134" id="Seg_784" n="e" s="T133">man </ts>
               <ts e="T135" id="Seg_786" n="e" s="T134">tekga </ts>
               <ts e="T136" id="Seg_788" n="e" s="T135">tʼülaks </ts>
               <ts e="T137" id="Seg_790" n="e" s="T136">aːmdedʼiku.” </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T12" id="Seg_791" s="T1">ChAE_196X_HungryDaughterInLaw_flk.001 (001.001)</ta>
            <ta e="T14" id="Seg_792" s="T12">ChAE_196X_HungryDaughterInLaw_flk.002 (001.002)</ta>
            <ta e="T16" id="Seg_793" s="T14">ChAE_196X_HungryDaughterInLaw_flk.003 (001.003)</ta>
            <ta e="T20" id="Seg_794" s="T16">ChAE_196X_HungryDaughterInLaw_flk.004 (001.004)</ta>
            <ta e="T24" id="Seg_795" s="T20">ChAE_196X_HungryDaughterInLaw_flk.005 (001.005)</ta>
            <ta e="T28" id="Seg_796" s="T24">ChAE_196X_HungryDaughterInLaw_flk.006 (001.006)</ta>
            <ta e="T33" id="Seg_797" s="T28">ChAE_196X_HungryDaughterInLaw_flk.007 (001.007)</ta>
            <ta e="T35" id="Seg_798" s="T33">ChAE_196X_HungryDaughterInLaw_flk.008 (001.008)</ta>
            <ta e="T39" id="Seg_799" s="T35">ChAE_196X_HungryDaughterInLaw_flk.009 (001.009)</ta>
            <ta e="T44" id="Seg_800" s="T39">ChAE_196X_HungryDaughterInLaw_flk.010 (001.010)</ta>
            <ta e="T51" id="Seg_801" s="T44">ChAE_196X_HungryDaughterInLaw_flk.011 (001.011)</ta>
            <ta e="T55" id="Seg_802" s="T51">ChAE_196X_HungryDaughterInLaw_flk.012 (001.012)</ta>
            <ta e="T65" id="Seg_803" s="T55">ChAE_196X_HungryDaughterInLaw_flk.013 (001.013)</ta>
            <ta e="T69" id="Seg_804" s="T65">ChAE_196X_HungryDaughterInLaw_flk.014 (001.014)</ta>
            <ta e="T72" id="Seg_805" s="T69">ChAE_196X_HungryDaughterInLaw_flk.015 (001.015)</ta>
            <ta e="T77" id="Seg_806" s="T72">ChAE_196X_HungryDaughterInLaw_flk.016 (001.016)</ta>
            <ta e="T80" id="Seg_807" s="T77">ChAE_196X_HungryDaughterInLaw_flk.017 (001.017)</ta>
            <ta e="T84" id="Seg_808" s="T80">ChAE_196X_HungryDaughterInLaw_flk.018 (001.018)</ta>
            <ta e="T92" id="Seg_809" s="T84">ChAE_196X_HungryDaughterInLaw_flk.019 (001.019)</ta>
            <ta e="T96" id="Seg_810" s="T92">ChAE_196X_HungryDaughterInLaw_flk.020 (001.020)</ta>
            <ta e="T100" id="Seg_811" s="T96">ChAE_196X_HungryDaughterInLaw_flk.021 (001.021)</ta>
            <ta e="T107" id="Seg_812" s="T100">ChAE_196X_HungryDaughterInLaw_flk.022 (001.022)</ta>
            <ta e="T114" id="Seg_813" s="T107">ChAE_196X_HungryDaughterInLaw_flk.023 (001.023)</ta>
            <ta e="T120" id="Seg_814" s="T114">ChAE_196X_HungryDaughterInLaw_flk.024 (001.024)</ta>
            <ta e="T124" id="Seg_815" s="T120">ChAE_196X_HungryDaughterInLaw_flk.025 (001.025)</ta>
            <ta e="T132" id="Seg_816" s="T124">ChAE_196X_HungryDaughterInLaw_flk.026 (001.026)</ta>
            <ta e="T137" id="Seg_817" s="T132">ChAE_196X_HungryDaughterInLaw_flk.027 (001.027)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T12" id="Seg_818" s="T1">илʼ′дʼӓт па′ссат (е′ра па′jасаɣъ) нӓды′гу кы′гелым′баɣи ′ӣнди (нӓдыгу), ′а̄рың ′jедʼеɣындо ′ӣмбади.</ta>
            <ta e="T14" id="Seg_819" s="T12">′ӣмбат нӓ̄′дӓм.</ta>
            <ta e="T16" id="Seg_820" s="T14">′ӣмдъ нӓдӓм′бади.</ta>
            <ta e="T20" id="Seg_821" s="T16">ӣт нет ′лʼе̄пе, kwан′ба кун′дакты.</ta>
            <ta e="T24" id="Seg_822" s="T20">илʼ′дӓнтсе па′ссандызе па′jамда ′kwӓдимбат.</ta>
            <ta e="T28" id="Seg_823" s="T24">илʼ′дʼӓт пизе′делдже ′ӯдимба.</ta>
            <ta e="T33" id="Seg_824" s="T28">и па′ссат ′нӓй ′удиндъ нӓй′ɣум.</ta>
            <ta e="T35" id="Seg_825" s="T33">′силʼна ′нӓɣалбыди.</ta>
            <ta e="T39" id="Seg_826" s="T35">и сна′хамтъ ′ӯдигу куралʼджекум′бади.</ta>
            <ta e="T44" id="Seg_827" s="T39">а оп′содиммым ′сваң ас мекум′бади.</ta>
            <ta e="T51" id="Seg_828" s="T44">′сва ап′содам тӓп аз авкум′бат тӓбыс′таɣызе варкы′лʼе.</ta>
            <ta e="T55" id="Seg_829" s="T51">илʼ′дӓт ′ӓ̄жулгумбан аур′гу надъ.</ta>
            <ta e="T65" id="Seg_830" s="T55">у′столʼджит ‵таkkылгум′бат (таkkыл′гу надъ) аур′гу, ′нʼӓjам мажелгум′батт (нʼӓjам ма′желгу надо).</ta>
            <ta e="T69" id="Seg_831" s="T65">′нагур ла′мотинʼдʼӓр ‵аукум′баттъ.</ta>
            <ta e="T72" id="Seg_832" s="T69">илʼдʼӓт ӓ̄′джоугумба kабы′(д)жай.</ta>
            <ta e="T77" id="Seg_833" s="T72">′то kwан′делгу ′нʼӓим, вес ап′содʼимым.</ta>
            <ta e="T80" id="Seg_834" s="T77">ме те′пер ‵кабъджим′бай.</ta>
            <ta e="T84" id="Seg_835" s="T80">сно′хат уби′раимкумбат вес ап′содимым.</ta>
            <ta e="T92" id="Seg_836" s="T84">илʼ′дʼӓт и па′ссат аувкум′бади ′тве̨лаң, аургум′баɣъ ′тве̨лаң он′ди.</ta>
            <ta e="T96" id="Seg_837" s="T92">а тӓп ам′наң ′е̨̄кумба.</ta>
            <ta e="T100" id="Seg_838" s="T96">′ӯдикум′ба ′kwезылʼе, аургу ′кыгыкум′ба.</ta>
            <ta e="T107" id="Seg_839" s="T100">сно′хат ам′наң ′е̨кумбаң, ′е̨кумбаң, ондъ ′е̨вынтнӓ kwан′ба.</ta>
            <ta e="T114" id="Seg_840" s="T107">‵е̨′вынт′нӓ ӓ̄джолб̂ан што ам′наң kва′jаван (ам′наң палʼ′дʼӓң).</ta>
            <ta e="T120" id="Seg_841" s="T114">аур′гу асс ме′куди, ман асс кабыджи′kwаң.</ta>
            <ta e="T124" id="Seg_842" s="T120">сно′хаднӓ′нан нагур а′гат ′е̨пант.</ta>
            <ta e="T132" id="Seg_843" s="T124">тим′нʼӓт ӓ̄джукумбан: kwа′ннык ′ма̄т, е̨тта′рык свай ап′сот потк.</ta>
            <ta e="T137" id="Seg_844" s="T132">а ман ′текга ′тʼӱлакс а̄мдед̂ʼику.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T12" id="Seg_845" s="T1">ilʼdʼät passat (era pajasaɣə) nädɨgu kɨgelɨmbaɣi iːndi (nädɨgu), aːrɨŋ jedʼeɣɨndo iːmbadi.</ta>
            <ta e="T14" id="Seg_846" s="T12">iːmbat näːdäm.</ta>
            <ta e="T16" id="Seg_847" s="T14">iːmdə nädämbadi.</ta>
            <ta e="T20" id="Seg_848" s="T16">iːt net lʼeːpe, qwanba kundaktɨ.</ta>
            <ta e="T24" id="Seg_849" s="T20">ilʼdäntse passandɨze pajamda qwädimbat.</ta>
            <ta e="T28" id="Seg_850" s="T24">ilʼdʼät pizedelǯe uːdimba.</ta>
            <ta e="T33" id="Seg_851" s="T28">i passat näj udində näjɣum.</ta>
            <ta e="T35" id="Seg_852" s="T33">silʼna näɣalbɨdi.</ta>
            <ta e="T39" id="Seg_853" s="T35">i snaxamtə uːdigu kuralʼǯekumbadi.</ta>
            <ta e="T44" id="Seg_854" s="T39">a opsodimmɨm swaŋ as mekumbadi.</ta>
            <ta e="T51" id="Seg_855" s="T44">swa apsodam täp az awkumbat täbɨstaɣɨze warkɨlʼe.</ta>
            <ta e="T55" id="Seg_856" s="T51">ilʼdät äːʒulgumban aurgu nadə.</ta>
            <ta e="T65" id="Seg_857" s="T55">ustolʼǯit taqqɨlgumbat (taqqɨlgu nadə) aurgu, nʼäjam maʒelgumbatt (nʼäjam maʒelgu nado).</ta>
            <ta e="T69" id="Seg_858" s="T65">nagur lamotinʼdʼär aukumbattə.</ta>
            <ta e="T72" id="Seg_859" s="T69">ilʼdʼät äːǯougumba qabɨ(d)ʒaj.</ta>
            <ta e="T77" id="Seg_860" s="T72">to qwandelgu nʼäim, wes apsodʼimɨm.</ta>
            <ta e="T80" id="Seg_861" s="T77">me teper kabəǯimbaj.</ta>
            <ta e="T84" id="Seg_862" s="T80">snoxat ubiraimkumbat wes apsodimɨm.</ta>
            <ta e="T92" id="Seg_863" s="T84">ilʼdʼät i passat auwkumbadi twelaŋ, aurgumbaɣə twelaŋ ondi.</ta>
            <ta e="T96" id="Seg_864" s="T92">a täp amnaŋ eːkumba.</ta>
            <ta e="T100" id="Seg_865" s="T96">uːdikumba qwezɨlʼe, aurgu kɨgɨkumba.</ta>
            <ta e="T107" id="Seg_866" s="T100">snoxat amnaŋ ekumbaŋ, ekumbaŋ, ondə ewɨntnä qwanba.</ta>
            <ta e="T114" id="Seg_867" s="T107">ewɨntnä äːǯolb̂an što amnaŋ qwajawan (amnaŋ palʼdʼäŋ).</ta>
            <ta e="T120" id="Seg_868" s="T114">aurgu ass mekudi, man ass kabɨǯiqwaŋ.</ta>
            <ta e="T124" id="Seg_869" s="T120">snoxadnänan nagur agat epant.</ta>
            <ta e="T132" id="Seg_870" s="T124">timnʼät äːǯukumban: qwannɨk maːt, ettarɨk swaj apsot potk.</ta>
            <ta e="T137" id="Seg_871" s="T132">a man tekga tʼülaks aːmded̂ʼiku.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T12" id="Seg_872" s="T1">Ilʼdʼät passat (era pajasaɣə) nädɨgu kɨgelɨmbaɣi iːndi (nädɨgu), aːrɨŋ jedʼeɣɨndo iːmbadi. </ta>
            <ta e="T14" id="Seg_873" s="T12">Iːmbat näːdäm. </ta>
            <ta e="T16" id="Seg_874" s="T14">Iːmdə nädämbadi. </ta>
            <ta e="T20" id="Seg_875" s="T16">Iːt netlʼeːpe, qwanba kundaktɨ. </ta>
            <ta e="T24" id="Seg_876" s="T20">Ilʼdäntse passandɨze pajamda qwädimbat. </ta>
            <ta e="T28" id="Seg_877" s="T24">Ilʼdʼät pize delǯe uːdimba. </ta>
            <ta e="T33" id="Seg_878" s="T28">I passat näj udində näjɣum. </ta>
            <ta e="T35" id="Seg_879" s="T33">Silʼna näɣalbɨdi. </ta>
            <ta e="T39" id="Seg_880" s="T35">I snaxamtə uːdigu kuralʼǯekumbadi. </ta>
            <ta e="T44" id="Seg_881" s="T39">A opsodimmɨm swaŋ as mekumbadi. </ta>
            <ta e="T51" id="Seg_882" s="T44">Swa apsodam täp az awkumbat täbɨstaɣɨze warkɨlʼe. </ta>
            <ta e="T55" id="Seg_883" s="T51">Ilʼdät äːʒulgumban aurgu nadə. </ta>
            <ta e="T65" id="Seg_884" s="T55">Ustolʼǯit taqqɨlgumbat (taqqɨlgu nadə) aurgu, nʼäjam maʒelgumbatt (nʼäjam maʒelgu nado). </ta>
            <ta e="T69" id="Seg_885" s="T65">Nagur lamotinʼ dʼär aukumbattə. </ta>
            <ta e="T72" id="Seg_886" s="T69">Ilʼdʼät äːǯougumba: “Qabɨǯaj. </ta>
            <ta e="T77" id="Seg_887" s="T72">To qwandelgu nʼäim, wes apsodʼimɨm. </ta>
            <ta e="T80" id="Seg_888" s="T77">Me teper kabəǯimbaj.” </ta>
            <ta e="T84" id="Seg_889" s="T80">Snoxat ubiraimkumbat wes apsodimɨm. </ta>
            <ta e="T92" id="Seg_890" s="T84">Ilʼdʼät i passat auwkumbadi twelaŋ, aurgumbaɣə twelaŋ ondi. </ta>
            <ta e="T96" id="Seg_891" s="T92">A täp amnaŋ eːkumba. </ta>
            <ta e="T100" id="Seg_892" s="T96">Uːdikumba qwezɨlʼe, aurgu kɨgɨkumba. </ta>
            <ta e="T107" id="Seg_893" s="T100">Snoxat amnaŋ ekumbaŋ, ekumbaŋ, ondə ewɨntnä qwanba. </ta>
            <ta e="T114" id="Seg_894" s="T107">Ewɨntnä äːǯolban što “Amnaŋ qwajawan (amnaŋ palʼdʼäŋ). </ta>
            <ta e="T120" id="Seg_895" s="T114">Aurgu ass mekudi, man ass kabɨǯiqwaŋ.” </ta>
            <ta e="T124" id="Seg_896" s="T120">Snoxadnänan nagur agat epant. </ta>
            <ta e="T132" id="Seg_897" s="T124">Timnʼät äːǯukumban: “Qwannɨk maːt, ettarɨk swaj apsot potk. </ta>
            <ta e="T137" id="Seg_898" s="T132">A man tekga tʼülaks aːmdedʼiku.” </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_899" s="T1">ilʼdʼä-t</ta>
            <ta e="T3" id="Seg_900" s="T2">passa-t</ta>
            <ta e="T4" id="Seg_901" s="T3">era</ta>
            <ta e="T5" id="Seg_902" s="T4">paja-sa-ɣə</ta>
            <ta e="T6" id="Seg_903" s="T5">nädɨ-gu</ta>
            <ta e="T7" id="Seg_904" s="T6">kɨge-l-ɨ-mba-ɣi</ta>
            <ta e="T8" id="Seg_905" s="T7">iː-n-di</ta>
            <ta e="T9" id="Seg_906" s="T8">nädɨ-gu</ta>
            <ta e="T10" id="Seg_907" s="T9">aːrɨŋ</ta>
            <ta e="T11" id="Seg_908" s="T10">jedʼe-ɣɨndo</ta>
            <ta e="T12" id="Seg_909" s="T11">iː-mba-di</ta>
            <ta e="T13" id="Seg_910" s="T12">iː-mba-t</ta>
            <ta e="T14" id="Seg_911" s="T13">näːdä-m</ta>
            <ta e="T15" id="Seg_912" s="T14">iː-m-də</ta>
            <ta e="T16" id="Seg_913" s="T15">nädä-mba-di</ta>
            <ta e="T17" id="Seg_914" s="T16">iː-t</ta>
            <ta e="T18" id="Seg_915" s="T17">net-lʼeːpe</ta>
            <ta e="T19" id="Seg_916" s="T18">qwan-ba</ta>
            <ta e="T20" id="Seg_917" s="T19">kundak-tɨ</ta>
            <ta e="T21" id="Seg_918" s="T20">ilʼdä-nt-se</ta>
            <ta e="T22" id="Seg_919" s="T21">passa-ndɨ-ze</ta>
            <ta e="T23" id="Seg_920" s="T22">paja-m-da</ta>
            <ta e="T24" id="Seg_921" s="T23">qwädi-mba-t</ta>
            <ta e="T25" id="Seg_922" s="T24">ilʼdʼä-t</ta>
            <ta e="T26" id="Seg_923" s="T25">pi-ze</ta>
            <ta e="T27" id="Seg_924" s="T26">del-ǯe</ta>
            <ta e="T28" id="Seg_925" s="T27">uːdi-mba</ta>
            <ta e="T29" id="Seg_926" s="T28">i</ta>
            <ta e="T30" id="Seg_927" s="T29">passa-t</ta>
            <ta e="T31" id="Seg_928" s="T30">näj</ta>
            <ta e="T32" id="Seg_929" s="T31">udi-ndə</ta>
            <ta e="T33" id="Seg_930" s="T32">nä-j-ɣum</ta>
            <ta e="T34" id="Seg_931" s="T33">silʼna</ta>
            <ta e="T35" id="Seg_932" s="T34">näɣal-bɨ-di</ta>
            <ta e="T36" id="Seg_933" s="T35">i</ta>
            <ta e="T37" id="Seg_934" s="T36">snaxa-m-tə</ta>
            <ta e="T38" id="Seg_935" s="T37">uːdi-gu</ta>
            <ta e="T39" id="Seg_936" s="T38">kuralʼǯe-ku-mba-di</ta>
            <ta e="T40" id="Seg_937" s="T39">a</ta>
            <ta e="T41" id="Seg_938" s="T40">op-sodi-mmɨ-m</ta>
            <ta e="T42" id="Seg_939" s="T41">swa-ŋ</ta>
            <ta e="T43" id="Seg_940" s="T42">as</ta>
            <ta e="T44" id="Seg_941" s="T43">me-ku-mba-di</ta>
            <ta e="T45" id="Seg_942" s="T44">swa</ta>
            <ta e="T46" id="Seg_943" s="T45">apsod-a-m</ta>
            <ta e="T47" id="Seg_944" s="T46">täp</ta>
            <ta e="T48" id="Seg_945" s="T47">az</ta>
            <ta e="T49" id="Seg_946" s="T48">aw-ku-mba-t</ta>
            <ta e="T50" id="Seg_947" s="T49">täb-ɨ-staɣɨ-ze</ta>
            <ta e="T51" id="Seg_948" s="T50">warkɨ-lʼe</ta>
            <ta e="T52" id="Seg_949" s="T51">ilʼdä-t</ta>
            <ta e="T53" id="Seg_950" s="T52">äːʒu-l-gu-mba-n</ta>
            <ta e="T54" id="Seg_951" s="T53">au-r-gu</ta>
            <ta e="T55" id="Seg_952" s="T54">nadə</ta>
            <ta e="T56" id="Seg_953" s="T55">ustolʼ-ǯit</ta>
            <ta e="T57" id="Seg_954" s="T56">taqqɨl-gu-mba-t</ta>
            <ta e="T58" id="Seg_955" s="T57">taqqɨl-gu</ta>
            <ta e="T59" id="Seg_956" s="T58">nadə</ta>
            <ta e="T60" id="Seg_957" s="T59">au-r-gu</ta>
            <ta e="T61" id="Seg_958" s="T60">nʼäj-a-m</ta>
            <ta e="T62" id="Seg_959" s="T61">maʒe-l-gu-mba-tt</ta>
            <ta e="T63" id="Seg_960" s="T62">nʼäj-a-m</ta>
            <ta e="T64" id="Seg_961" s="T63">maʒe-l-gu</ta>
            <ta e="T65" id="Seg_962" s="T64">nado</ta>
            <ta e="T66" id="Seg_963" s="T65">nagur</ta>
            <ta e="T67" id="Seg_964" s="T66">lamot-i-nʼ</ta>
            <ta e="T68" id="Seg_965" s="T67">dʼär</ta>
            <ta e="T69" id="Seg_966" s="T68">au-ku-mba-ttə</ta>
            <ta e="T70" id="Seg_967" s="T69">ilʼdʼä-t</ta>
            <ta e="T71" id="Seg_968" s="T70">äːǯo-u-gu-mba</ta>
            <ta e="T72" id="Seg_969" s="T71">qabɨǯa-j</ta>
            <ta e="T73" id="Seg_970" s="T72">to</ta>
            <ta e="T74" id="Seg_971" s="T73">qwan-de-l-gu</ta>
            <ta e="T75" id="Seg_972" s="T74">nʼäi-m</ta>
            <ta e="T76" id="Seg_973" s="T75">wes</ta>
            <ta e="T77" id="Seg_974" s="T76">ap-sodʼi-mɨ-m</ta>
            <ta e="T78" id="Seg_975" s="T77">me</ta>
            <ta e="T79" id="Seg_976" s="T78">teper</ta>
            <ta e="T80" id="Seg_977" s="T79">kabəǯi-mba-j</ta>
            <ta e="T81" id="Seg_978" s="T80">snoxa-t</ta>
            <ta e="T82" id="Seg_979" s="T81">ubiraim-ku-mba-t</ta>
            <ta e="T83" id="Seg_980" s="T82">wes</ta>
            <ta e="T84" id="Seg_981" s="T83">ap-sodi-mɨ-m</ta>
            <ta e="T85" id="Seg_982" s="T84">ilʼdʼä-t</ta>
            <ta e="T86" id="Seg_983" s="T85">i</ta>
            <ta e="T87" id="Seg_984" s="T86">passa-t</ta>
            <ta e="T88" id="Seg_985" s="T87">au-w-ku-mba-di</ta>
            <ta e="T89" id="Seg_986" s="T88">twelaŋ</ta>
            <ta e="T90" id="Seg_987" s="T89">au-r-gu-mba-ɣə</ta>
            <ta e="T91" id="Seg_988" s="T90">twelaŋ</ta>
            <ta e="T92" id="Seg_989" s="T91">ondi</ta>
            <ta e="T93" id="Seg_990" s="T92">a</ta>
            <ta e="T94" id="Seg_991" s="T93">täp</ta>
            <ta e="T95" id="Seg_992" s="T94">amna-ŋ</ta>
            <ta e="T96" id="Seg_993" s="T95">eː-ku-mba</ta>
            <ta e="T97" id="Seg_994" s="T96">uːdi-ku-mba</ta>
            <ta e="T98" id="Seg_995" s="T97">qwezɨ-lʼe</ta>
            <ta e="T99" id="Seg_996" s="T98">au-r-gu</ta>
            <ta e="T100" id="Seg_997" s="T99">kɨgɨ-ku-mba</ta>
            <ta e="T101" id="Seg_998" s="T100">snoxa-t</ta>
            <ta e="T102" id="Seg_999" s="T101">amna-ŋ</ta>
            <ta e="T103" id="Seg_1000" s="T102">e-ku-mba-ŋ</ta>
            <ta e="T104" id="Seg_1001" s="T103">e-ku-mba-ŋ</ta>
            <ta e="T105" id="Seg_1002" s="T104">ondə</ta>
            <ta e="T106" id="Seg_1003" s="T105">ewɨ-nt-nä</ta>
            <ta e="T107" id="Seg_1004" s="T106">qwan-ba</ta>
            <ta e="T108" id="Seg_1005" s="T107">ewɨ-nt-nä</ta>
            <ta e="T109" id="Seg_1006" s="T108">äːǯo-l-ba-n</ta>
            <ta e="T110" id="Seg_1007" s="T109">što</ta>
            <ta e="T111" id="Seg_1008" s="T110">amna-ŋ</ta>
            <ta e="T112" id="Seg_1009" s="T111">qwaja-wa-n</ta>
            <ta e="T113" id="Seg_1010" s="T112">amna-ŋ</ta>
            <ta e="T114" id="Seg_1011" s="T113">palʼdʼä-ŋ</ta>
            <ta e="T115" id="Seg_1012" s="T114">au-r-gu</ta>
            <ta e="T116" id="Seg_1013" s="T115">ass</ta>
            <ta e="T117" id="Seg_1014" s="T116">me-ku-di</ta>
            <ta e="T118" id="Seg_1015" s="T117">man</ta>
            <ta e="T119" id="Seg_1016" s="T118">ass</ta>
            <ta e="T120" id="Seg_1017" s="T119">kabɨǯi-q-wa-ŋ</ta>
            <ta e="T121" id="Seg_1018" s="T120">snoxa-dnä-nan</ta>
            <ta e="T122" id="Seg_1019" s="T121">nagur</ta>
            <ta e="T123" id="Seg_1020" s="T122">aga-t</ta>
            <ta e="T124" id="Seg_1021" s="T123">e-pa-nt</ta>
            <ta e="T125" id="Seg_1022" s="T124">timnʼä-t</ta>
            <ta e="T126" id="Seg_1023" s="T125">äːǯu-ku-mba-n</ta>
            <ta e="T127" id="Seg_1024" s="T126">qwann-ɨ-k</ta>
            <ta e="T128" id="Seg_1025" s="T127">maːt</ta>
            <ta e="T129" id="Seg_1026" s="T128">ettar-ɨ-k</ta>
            <ta e="T130" id="Seg_1027" s="T129">swa-j</ta>
            <ta e="T131" id="Seg_1028" s="T130">apsot</ta>
            <ta e="T132" id="Seg_1029" s="T131">pot-k</ta>
            <ta e="T133" id="Seg_1030" s="T132">a</ta>
            <ta e="T134" id="Seg_1031" s="T133">man</ta>
            <ta e="T135" id="Seg_1032" s="T134">tekga</ta>
            <ta e="T136" id="Seg_1033" s="T135">tʼü-la-k-s</ta>
            <ta e="T137" id="Seg_1034" s="T136">aːmde-dʼi-ku</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_1035" s="T1">ilʼdʼa-tə</ta>
            <ta e="T3" id="Seg_1036" s="T2">passa-tə</ta>
            <ta e="T4" id="Seg_1037" s="T3">era</ta>
            <ta e="T5" id="Seg_1038" s="T4">paja-sɨ-qi</ta>
            <ta e="T6" id="Seg_1039" s="T5">nedɨ-gu</ta>
            <ta e="T7" id="Seg_1040" s="T6">kɨgɨ-l-ɨ-mbɨ-qij</ta>
            <ta e="T8" id="Seg_1041" s="T7">iː-m-di</ta>
            <ta e="T9" id="Seg_1042" s="T8">nedɨ-gu</ta>
            <ta e="T10" id="Seg_1043" s="T9">arɨk</ta>
            <ta e="T11" id="Seg_1044" s="T10">eːde-qɨntɨ</ta>
            <ta e="T12" id="Seg_1045" s="T11">iː-mbɨ-di</ta>
            <ta e="T13" id="Seg_1046" s="T12">iː-mbɨ-t</ta>
            <ta e="T14" id="Seg_1047" s="T13">nädek-m</ta>
            <ta e="T15" id="Seg_1048" s="T14">iː-m-tə</ta>
            <ta e="T16" id="Seg_1049" s="T15">nedɨ-mbɨ-di</ta>
            <ta e="T17" id="Seg_1050" s="T16">iː-tə</ta>
            <ta e="T18" id="Seg_1051" s="T17">nedɨ-lʼewlʼe</ta>
            <ta e="T19" id="Seg_1052" s="T18">qwan-mbɨ</ta>
            <ta e="T20" id="Seg_1053" s="T19">kundak-ntə</ta>
            <ta e="T21" id="Seg_1054" s="T20">ilʼdʼa-ntɨ-se</ta>
            <ta e="T22" id="Seg_1055" s="T21">passa-ntɨ-se</ta>
            <ta e="T23" id="Seg_1056" s="T22">paja-m-tə</ta>
            <ta e="T24" id="Seg_1057" s="T23">qwɛdʼi-mbɨ-t</ta>
            <ta e="T25" id="Seg_1058" s="T24">ilʼdʼa-tə</ta>
            <ta e="T26" id="Seg_1059" s="T25">pi-se</ta>
            <ta e="T27" id="Seg_1060" s="T26">dʼel-ǯe</ta>
            <ta e="T28" id="Seg_1061" s="T27">uːdʼi-mbɨ</ta>
            <ta e="T29" id="Seg_1062" s="T28">i</ta>
            <ta e="T30" id="Seg_1063" s="T29">passa-tə</ta>
            <ta e="T31" id="Seg_1064" s="T30">naj</ta>
            <ta e="T32" id="Seg_1065" s="T31">uːdʼi-ntɨ</ta>
            <ta e="T33" id="Seg_1066" s="T32">ne-lʼ-qum</ta>
            <ta e="T34" id="Seg_1067" s="T33">silʼna</ta>
            <ta e="T35" id="Seg_1068" s="T34">nʼäqɨl-mbɨ-di</ta>
            <ta e="T36" id="Seg_1069" s="T35">i</ta>
            <ta e="T37" id="Seg_1070" s="T36">snaxa-m-tə</ta>
            <ta e="T38" id="Seg_1071" s="T37">uːdʼi-gu</ta>
            <ta e="T39" id="Seg_1072" s="T38">quralǯə-ku-mbɨ-di</ta>
            <ta e="T40" id="Seg_1073" s="T39">a</ta>
            <ta e="T41" id="Seg_1074" s="T40">am-sodə-mɨ-m</ta>
            <ta e="T42" id="Seg_1075" s="T41">sawa-ŋ</ta>
            <ta e="T43" id="Seg_1076" s="T42">asa</ta>
            <ta e="T44" id="Seg_1077" s="T43">me-ku-mbɨ-di</ta>
            <ta e="T45" id="Seg_1078" s="T44">sawa</ta>
            <ta e="T46" id="Seg_1079" s="T45">apsod-ɨ-m</ta>
            <ta e="T47" id="Seg_1080" s="T46">täp</ta>
            <ta e="T48" id="Seg_1081" s="T47">asa</ta>
            <ta e="T49" id="Seg_1082" s="T48">am-ku-mbɨ-t</ta>
            <ta e="T50" id="Seg_1083" s="T49">täp-ɨ-staɣɨ-se</ta>
            <ta e="T51" id="Seg_1084" s="T50">warkɨ-le</ta>
            <ta e="T52" id="Seg_1085" s="T51">ilʼdʼa-tə</ta>
            <ta e="T53" id="Seg_1086" s="T52">əǯu-l-ku-mbɨ-n</ta>
            <ta e="T54" id="Seg_1087" s="T53">am-r-gu</ta>
            <ta e="T55" id="Seg_1088" s="T54">nadə</ta>
            <ta e="T56" id="Seg_1089" s="T55">istol-ǯit</ta>
            <ta e="T57" id="Seg_1090" s="T56">taqqɨl-ku-mbɨ-tɨn</ta>
            <ta e="T58" id="Seg_1091" s="T57">taqqɨl-gu</ta>
            <ta e="T59" id="Seg_1092" s="T58">nadə</ta>
            <ta e="T60" id="Seg_1093" s="T59">am-r-gu</ta>
            <ta e="T61" id="Seg_1094" s="T60">nʼäj-ɨ-m</ta>
            <ta e="T62" id="Seg_1095" s="T61">maǯə-l-ku-mbɨ-tɨn</ta>
            <ta e="T63" id="Seg_1096" s="T62">nʼäj-ɨ-m</ta>
            <ta e="T64" id="Seg_1097" s="T63">maǯə-l-gu</ta>
            <ta e="T65" id="Seg_1098" s="T64">nadə</ta>
            <ta e="T66" id="Seg_1099" s="T65">nagur</ta>
            <ta e="T67" id="Seg_1100" s="T66">lamot-ɨ-n</ta>
            <ta e="T68" id="Seg_1101" s="T67">dʼar</ta>
            <ta e="T69" id="Seg_1102" s="T68">am-ku-mbɨ-tɨn</ta>
            <ta e="T70" id="Seg_1103" s="T69">ilʼdʼa-tə</ta>
            <ta e="T71" id="Seg_1104" s="T70">əǯu-u-ku-mbɨ</ta>
            <ta e="T72" id="Seg_1105" s="T71">qabuʒu-j</ta>
            <ta e="T73" id="Seg_1106" s="T72">to</ta>
            <ta e="T74" id="Seg_1107" s="T73">qwan-tɨ-l-gu</ta>
            <ta e="T75" id="Seg_1108" s="T74">nʼäj-m</ta>
            <ta e="T76" id="Seg_1109" s="T75">wesʼ</ta>
            <ta e="T77" id="Seg_1110" s="T76">am-sodə-mɨ-m</ta>
            <ta e="T78" id="Seg_1111" s="T77">me</ta>
            <ta e="T79" id="Seg_1112" s="T78">teper</ta>
            <ta e="T80" id="Seg_1113" s="T79">qabuʒu-mbɨ-j</ta>
            <ta e="T81" id="Seg_1114" s="T80">snaxa-tə</ta>
            <ta e="T82" id="Seg_1115" s="T81">ubiraj-ku-mbɨ-t</ta>
            <ta e="T83" id="Seg_1116" s="T82">wesʼ</ta>
            <ta e="T84" id="Seg_1117" s="T83">am-sodə-mɨ-m</ta>
            <ta e="T85" id="Seg_1118" s="T84">ilʼdʼa-tə</ta>
            <ta e="T86" id="Seg_1119" s="T85">i</ta>
            <ta e="T87" id="Seg_1120" s="T86">passa-tə</ta>
            <ta e="T88" id="Seg_1121" s="T87">am-u-ku-mbɨ-di</ta>
            <ta e="T89" id="Seg_1122" s="T88">twelak</ta>
            <ta e="T90" id="Seg_1123" s="T89">am-r-gu-mbɨ-qij</ta>
            <ta e="T91" id="Seg_1124" s="T90">twelak</ta>
            <ta e="T92" id="Seg_1125" s="T91">ondi</ta>
            <ta e="T93" id="Seg_1126" s="T92">a</ta>
            <ta e="T94" id="Seg_1127" s="T93">täp</ta>
            <ta e="T95" id="Seg_1128" s="T94">amna-ŋ</ta>
            <ta e="T96" id="Seg_1129" s="T95">eː-ku-mbɨ</ta>
            <ta e="T97" id="Seg_1130" s="T96">uːdʼi-ku-mbɨ</ta>
            <ta e="T98" id="Seg_1131" s="T97">qwəzɨ-le</ta>
            <ta e="T99" id="Seg_1132" s="T98">am-r-gu</ta>
            <ta e="T100" id="Seg_1133" s="T99">kɨgɨ-ku-mbɨ</ta>
            <ta e="T101" id="Seg_1134" s="T100">snaxa-tə</ta>
            <ta e="T102" id="Seg_1135" s="T101">amna-ŋ</ta>
            <ta e="T103" id="Seg_1136" s="T102">eː-ku-mbɨ-n</ta>
            <ta e="T104" id="Seg_1137" s="T103">eː-ku-mbɨ-n</ta>
            <ta e="T105" id="Seg_1138" s="T104">ondə</ta>
            <ta e="T106" id="Seg_1139" s="T105">awa-ntɨ-nä</ta>
            <ta e="T107" id="Seg_1140" s="T106">qwan-mbɨ</ta>
            <ta e="T108" id="Seg_1141" s="T107">awa-ntɨ-nä</ta>
            <ta e="T109" id="Seg_1142" s="T108">əǯu-l-mbɨ-n</ta>
            <ta e="T110" id="Seg_1143" s="T109">što</ta>
            <ta e="T111" id="Seg_1144" s="T110">amna-ŋ</ta>
            <ta e="T112" id="Seg_1145" s="T111">qwaja-nɨ-ŋ</ta>
            <ta e="T113" id="Seg_1146" s="T112">amna-ŋ</ta>
            <ta e="T114" id="Seg_1147" s="T113">palʼdʼi-ŋ</ta>
            <ta e="T115" id="Seg_1148" s="T114">am-r-gu</ta>
            <ta e="T116" id="Seg_1149" s="T115">asa</ta>
            <ta e="T117" id="Seg_1150" s="T116">me-ku-di</ta>
            <ta e="T118" id="Seg_1151" s="T117">man</ta>
            <ta e="T119" id="Seg_1152" s="T118">asa</ta>
            <ta e="T120" id="Seg_1153" s="T119">qabuʒu-ku-nɨ-ŋ</ta>
            <ta e="T121" id="Seg_1154" s="T120">snaxa-ntɨ-nan</ta>
            <ta e="T122" id="Seg_1155" s="T121">nagur</ta>
            <ta e="T123" id="Seg_1156" s="T122">agaː-tə</ta>
            <ta e="T124" id="Seg_1157" s="T123">eː-mbɨ-ntɨ</ta>
            <ta e="T125" id="Seg_1158" s="T124">tɨmnʼa-tə</ta>
            <ta e="T126" id="Seg_1159" s="T125">əǯu-ku-mbɨ-n</ta>
            <ta e="T127" id="Seg_1160" s="T126">qwan-ɨ-kɨ</ta>
            <ta e="T128" id="Seg_1161" s="T127">maːt</ta>
            <ta e="T129" id="Seg_1162" s="T128">ɛttɨr-ɨ-kɨ</ta>
            <ta e="T130" id="Seg_1163" s="T129">sawa-lʼ</ta>
            <ta e="T131" id="Seg_1164" s="T130">apsod</ta>
            <ta e="T132" id="Seg_1165" s="T131">poːt-kɨ</ta>
            <ta e="T133" id="Seg_1166" s="T132">a</ta>
            <ta e="T134" id="Seg_1167" s="T133">man</ta>
            <ta e="T135" id="Seg_1168" s="T134">tekka</ta>
            <ta e="T136" id="Seg_1169" s="T135">tüː-lä-ŋ-s</ta>
            <ta e="T137" id="Seg_1170" s="T136">amdɨ-dʼi-gu</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_1171" s="T1">father_in_law.[NOM]-3SG</ta>
            <ta e="T3" id="Seg_1172" s="T2">mother_in_law.[NOM]-3SG</ta>
            <ta e="T4" id="Seg_1173" s="T3">husband.[NOM]</ta>
            <ta e="T5" id="Seg_1174" s="T4">old.woman-DYA-DU.[NOM]</ta>
            <ta e="T6" id="Seg_1175" s="T5">marry.off-INF</ta>
            <ta e="T7" id="Seg_1176" s="T6">want-INCH-EP-PST.NAR-3DU.S</ta>
            <ta e="T8" id="Seg_1177" s="T7">son-ACC-3DU</ta>
            <ta e="T9" id="Seg_1178" s="T8">marry.off-INF</ta>
            <ta e="T10" id="Seg_1179" s="T9">other</ta>
            <ta e="T11" id="Seg_1180" s="T10">village-EL.3SG</ta>
            <ta e="T12" id="Seg_1181" s="T11">take-PST.NAR-3DU.O</ta>
            <ta e="T13" id="Seg_1182" s="T12">take-PST.NAR-3SG.O</ta>
            <ta e="T14" id="Seg_1183" s="T13">girl-ACC</ta>
            <ta e="T15" id="Seg_1184" s="T14">son-ACC-3SG</ta>
            <ta e="T16" id="Seg_1185" s="T15">marry.off-PST.NAR-3DU.O</ta>
            <ta e="T17" id="Seg_1186" s="T16">son.[NOM]-3SG</ta>
            <ta e="T18" id="Seg_1187" s="T17">get.married-CVB2</ta>
            <ta e="T19" id="Seg_1188" s="T18">leave-PST.NAR.[3SG.S]</ta>
            <ta e="T20" id="Seg_1189" s="T19">far-ILL</ta>
            <ta e="T21" id="Seg_1190" s="T20">father_in_law-OBL.3SG-COM</ta>
            <ta e="T22" id="Seg_1191" s="T21">mother_in_law-OBL.3SG-COM</ta>
            <ta e="T23" id="Seg_1192" s="T22">wife-ACC-3SG</ta>
            <ta e="T24" id="Seg_1193" s="T23">leave-PST.NAR-3SG.O</ta>
            <ta e="T25" id="Seg_1194" s="T24">father_in_law.[NOM]-3SG</ta>
            <ta e="T26" id="Seg_1195" s="T25">night-COM</ta>
            <ta e="T27" id="Seg_1196" s="T26">sun-%%</ta>
            <ta e="T28" id="Seg_1197" s="T27">work-PST.NAR.[3SG.S]</ta>
            <ta e="T29" id="Seg_1198" s="T28">and</ta>
            <ta e="T30" id="Seg_1199" s="T29">mother_in_law.[NOM]-3SG</ta>
            <ta e="T31" id="Seg_1200" s="T30">also</ta>
            <ta e="T32" id="Seg_1201" s="T31">work-PTCP.PRS</ta>
            <ta e="T33" id="Seg_1202" s="T32">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T34" id="Seg_1203" s="T33">very</ta>
            <ta e="T35" id="Seg_1204" s="T34">stretch-PST.NAR-3DU.O</ta>
            <ta e="T36" id="Seg_1205" s="T35">and</ta>
            <ta e="T37" id="Seg_1206" s="T36">daughter_in_law-ACC-3SG</ta>
            <ta e="T38" id="Seg_1207" s="T37">work-INF</ta>
            <ta e="T39" id="Seg_1208" s="T38">force-HAB-PST.NAR-3DU.O</ta>
            <ta e="T40" id="Seg_1209" s="T39">and</ta>
            <ta e="T41" id="Seg_1210" s="T40">eat-PTCP.NEC-something-ACC</ta>
            <ta e="T42" id="Seg_1211" s="T41">good-ADVZ</ta>
            <ta e="T43" id="Seg_1212" s="T42">NEG</ta>
            <ta e="T44" id="Seg_1213" s="T43">give-HAB-PST.NAR-3DU.O</ta>
            <ta e="T45" id="Seg_1214" s="T44">good</ta>
            <ta e="T46" id="Seg_1215" s="T45">food-EP-ACC</ta>
            <ta e="T47" id="Seg_1216" s="T46">(s)he.[NOM]</ta>
            <ta e="T48" id="Seg_1217" s="T47">NEG</ta>
            <ta e="T49" id="Seg_1218" s="T48">eat-HAB-PST.NAR-3SG.O</ta>
            <ta e="T50" id="Seg_1219" s="T49">(s)he-EP-DU-COM</ta>
            <ta e="T51" id="Seg_1220" s="T50">live-CVB</ta>
            <ta e="T52" id="Seg_1221" s="T51">father_in_law.[NOM]-3SG</ta>
            <ta e="T53" id="Seg_1222" s="T52">say-DRV-HAB-PST.NAR-3SG.S</ta>
            <ta e="T54" id="Seg_1223" s="T53">eat-FRQ-INF</ta>
            <ta e="T55" id="Seg_1224" s="T54">one.should</ta>
            <ta e="T56" id="Seg_1225" s="T55">table-%%</ta>
            <ta e="T57" id="Seg_1226" s="T56">gather-HAB-PST.NAR-3PL</ta>
            <ta e="T58" id="Seg_1227" s="T57">gather-INF</ta>
            <ta e="T59" id="Seg_1228" s="T58">one.should</ta>
            <ta e="T60" id="Seg_1229" s="T59">eat-FRQ-INF</ta>
            <ta e="T61" id="Seg_1230" s="T60">bread-EP-ACC</ta>
            <ta e="T62" id="Seg_1231" s="T61">cut-DRV-HAB-PST.NAR-3PL</ta>
            <ta e="T63" id="Seg_1232" s="T62">bread-EP-ACC</ta>
            <ta e="T64" id="Seg_1233" s="T63">cut-DRV-INF</ta>
            <ta e="T65" id="Seg_1234" s="T64">one.should</ta>
            <ta e="T66" id="Seg_1235" s="T65">three</ta>
            <ta e="T67" id="Seg_1236" s="T66">chunk-EP-GEN</ta>
            <ta e="T68" id="Seg_1237" s="T67">(each)</ta>
            <ta e="T69" id="Seg_1238" s="T68">eat-HAB-PST.NAR-3PL</ta>
            <ta e="T70" id="Seg_1239" s="T69">father_in_law.[NOM]-3SG</ta>
            <ta e="T71" id="Seg_1240" s="T70">say-%%-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T72" id="Seg_1241" s="T71">eat.one's.fill-1DU</ta>
            <ta e="T73" id="Seg_1242" s="T72">away</ta>
            <ta e="T74" id="Seg_1243" s="T73">leave-TR-DRV-INF</ta>
            <ta e="T75" id="Seg_1244" s="T74">bread-ACC</ta>
            <ta e="T76" id="Seg_1245" s="T75">all</ta>
            <ta e="T77" id="Seg_1246" s="T76">eat-PTCP.NEC-something-ACC</ta>
            <ta e="T78" id="Seg_1247" s="T77">we.[NOM]</ta>
            <ta e="T79" id="Seg_1248" s="T78">now</ta>
            <ta e="T80" id="Seg_1249" s="T79">eat.one's.fill-PST.NAR-1DU</ta>
            <ta e="T81" id="Seg_1250" s="T80">daughter_in_law.[NOM]-3SG</ta>
            <ta e="T82" id="Seg_1251" s="T81">clean-HAB-PST.NAR-3SG.O</ta>
            <ta e="T83" id="Seg_1252" s="T82">all</ta>
            <ta e="T84" id="Seg_1253" s="T83">eat-PTCP.NEC-something-ACC</ta>
            <ta e="T85" id="Seg_1254" s="T84">father_in_law.[NOM]-3SG</ta>
            <ta e="T86" id="Seg_1255" s="T85">and</ta>
            <ta e="T87" id="Seg_1256" s="T86">mother_in_law.[NOM]-3SG</ta>
            <ta e="T88" id="Seg_1257" s="T87">eat-%%-HAB-PST.NAR-3DU.O</ta>
            <ta e="T89" id="Seg_1258" s="T88">secretly</ta>
            <ta e="T90" id="Seg_1259" s="T89">eat-FRQ-INF-PST.NAR-3DU.S</ta>
            <ta e="T91" id="Seg_1260" s="T90">secretly</ta>
            <ta e="T92" id="Seg_1261" s="T91">oneself.3DU</ta>
            <ta e="T93" id="Seg_1262" s="T92">and</ta>
            <ta e="T94" id="Seg_1263" s="T93">(s)he.[NOM]</ta>
            <ta e="T95" id="Seg_1264" s="T94">hungry-ADVZ</ta>
            <ta e="T96" id="Seg_1265" s="T95">be-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T97" id="Seg_1266" s="T96">work-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T98" id="Seg_1267" s="T97">get.hungry-CVB</ta>
            <ta e="T99" id="Seg_1268" s="T98">eat-FRQ-INF</ta>
            <ta e="T100" id="Seg_1269" s="T99">want-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T101" id="Seg_1270" s="T100">daughter_in_law.[NOM]-3SG</ta>
            <ta e="T102" id="Seg_1271" s="T101">hungry-ADVZ</ta>
            <ta e="T103" id="Seg_1272" s="T102">be-HAB-PST.NAR-3SG.S</ta>
            <ta e="T104" id="Seg_1273" s="T103">be-HAB-PST.NAR-3SG.S</ta>
            <ta e="T105" id="Seg_1274" s="T104">own.3SG</ta>
            <ta e="T106" id="Seg_1275" s="T105">mother-OBL.3SG-ALL</ta>
            <ta e="T107" id="Seg_1276" s="T106">leave-PST.NAR.[3SG.S]</ta>
            <ta e="T108" id="Seg_1277" s="T107">mother-OBL.3SG-ALL</ta>
            <ta e="T109" id="Seg_1278" s="T108">say-DRV-PST.NAR-3SG.S</ta>
            <ta e="T110" id="Seg_1279" s="T109">that</ta>
            <ta e="T111" id="Seg_1280" s="T110">hungry-ADVZ</ta>
            <ta e="T112" id="Seg_1281" s="T111">%go-CO-1SG.S</ta>
            <ta e="T113" id="Seg_1282" s="T112">hungry-ADVZ</ta>
            <ta e="T114" id="Seg_1283" s="T113">walk-1SG.S</ta>
            <ta e="T115" id="Seg_1284" s="T114">eat-FRQ-INF</ta>
            <ta e="T116" id="Seg_1285" s="T115">NEG</ta>
            <ta e="T117" id="Seg_1286" s="T116">give-HAB-3DU.O</ta>
            <ta e="T118" id="Seg_1287" s="T117">I.NOM</ta>
            <ta e="T119" id="Seg_1288" s="T118">NEG</ta>
            <ta e="T120" id="Seg_1289" s="T119">eat.one's.fill-HAB-CO-1SG.S</ta>
            <ta e="T121" id="Seg_1290" s="T120">daughter_in_law-OBL.3SG-ADES</ta>
            <ta e="T122" id="Seg_1291" s="T121">three</ta>
            <ta e="T123" id="Seg_1292" s="T122">brother.[NOM]-3SG</ta>
            <ta e="T124" id="Seg_1293" s="T123">be-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T125" id="Seg_1294" s="T124">brother.[NOM]-3SG</ta>
            <ta e="T126" id="Seg_1295" s="T125">say-HAB-PST.NAR-3SG.S</ta>
            <ta e="T127" id="Seg_1296" s="T126">leave-EP-IMP.2SG.S</ta>
            <ta e="T128" id="Seg_1297" s="T127">house.[NOM]</ta>
            <ta e="T129" id="Seg_1298" s="T128">cook-EP-IMP.2SG.S</ta>
            <ta e="T130" id="Seg_1299" s="T129">good-DRV</ta>
            <ta e="T131" id="Seg_1300" s="T130">food.[NOM]</ta>
            <ta e="T132" id="Seg_1301" s="T131">cook-IMP.2SG.S</ta>
            <ta e="T133" id="Seg_1302" s="T132">and</ta>
            <ta e="T134" id="Seg_1303" s="T133">I.NOM</ta>
            <ta e="T135" id="Seg_1304" s="T134">you.ALL</ta>
            <ta e="T136" id="Seg_1305" s="T135">come-FUT-1SG.S-FUT</ta>
            <ta e="T137" id="Seg_1306" s="T136">sit-DRV-INF</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_1307" s="T1">свёкор.[NOM]-3SG</ta>
            <ta e="T3" id="Seg_1308" s="T2">свекровь.[NOM]-3SG</ta>
            <ta e="T4" id="Seg_1309" s="T3">муж.[NOM]</ta>
            <ta e="T5" id="Seg_1310" s="T4">старуха-DYA-DU.[NOM]</ta>
            <ta e="T6" id="Seg_1311" s="T5">женить-INF</ta>
            <ta e="T7" id="Seg_1312" s="T6">хотеть-INCH-EP-PST.NAR-3DU.S</ta>
            <ta e="T8" id="Seg_1313" s="T7">сын-ACC-3DU</ta>
            <ta e="T9" id="Seg_1314" s="T8">женить-INF</ta>
            <ta e="T10" id="Seg_1315" s="T9">другой</ta>
            <ta e="T11" id="Seg_1316" s="T10">деревня-EL.3SG</ta>
            <ta e="T12" id="Seg_1317" s="T11">взять-PST.NAR-3DU.O</ta>
            <ta e="T13" id="Seg_1318" s="T12">взять-PST.NAR-3SG.O</ta>
            <ta e="T14" id="Seg_1319" s="T13">девушка-ACC</ta>
            <ta e="T15" id="Seg_1320" s="T14">сын-ACC-3SG</ta>
            <ta e="T16" id="Seg_1321" s="T15">женить-PST.NAR-3DU.O</ta>
            <ta e="T17" id="Seg_1322" s="T16">сын.[NOM]-3SG</ta>
            <ta e="T18" id="Seg_1323" s="T17">жениться-CVB2</ta>
            <ta e="T19" id="Seg_1324" s="T18">отправиться-PST.NAR.[3SG.S]</ta>
            <ta e="T20" id="Seg_1325" s="T19">вдаль-ILL</ta>
            <ta e="T21" id="Seg_1326" s="T20">свёкор-OBL.3SG-COM</ta>
            <ta e="T22" id="Seg_1327" s="T21">свекровь-OBL.3SG-COM</ta>
            <ta e="T23" id="Seg_1328" s="T22">жена-ACC-3SG</ta>
            <ta e="T24" id="Seg_1329" s="T23">оставить-PST.NAR-3SG.O</ta>
            <ta e="T25" id="Seg_1330" s="T24">свёкор.[NOM]-3SG</ta>
            <ta e="T26" id="Seg_1331" s="T25">ночь-COM</ta>
            <ta e="T27" id="Seg_1332" s="T26">солнце-%%</ta>
            <ta e="T28" id="Seg_1333" s="T27">работать-PST.NAR.[3SG.S]</ta>
            <ta e="T29" id="Seg_1334" s="T28">и</ta>
            <ta e="T30" id="Seg_1335" s="T29">свекровь.[NOM]-3SG</ta>
            <ta e="T31" id="Seg_1336" s="T30">тоже</ta>
            <ta e="T32" id="Seg_1337" s="T31">работать-PTCP.PRS</ta>
            <ta e="T33" id="Seg_1338" s="T32">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T34" id="Seg_1339" s="T33">сильно</ta>
            <ta e="T35" id="Seg_1340" s="T34">натянуть-PST.NAR-3DU.O</ta>
            <ta e="T36" id="Seg_1341" s="T35">и</ta>
            <ta e="T37" id="Seg_1342" s="T36">сноха-ACC-3SG</ta>
            <ta e="T38" id="Seg_1343" s="T37">работать-INF</ta>
            <ta e="T39" id="Seg_1344" s="T38">заставить-HAB-PST.NAR-3DU.O</ta>
            <ta e="T40" id="Seg_1345" s="T39">а</ta>
            <ta e="T41" id="Seg_1346" s="T40">съесть-PTCP.NEC-нечто-ACC</ta>
            <ta e="T42" id="Seg_1347" s="T41">хороший-ADVZ</ta>
            <ta e="T43" id="Seg_1348" s="T42">NEG</ta>
            <ta e="T44" id="Seg_1349" s="T43">дать-HAB-PST.NAR-3DU.O</ta>
            <ta e="T45" id="Seg_1350" s="T44">хороший</ta>
            <ta e="T46" id="Seg_1351" s="T45">еда-EP-ACC</ta>
            <ta e="T47" id="Seg_1352" s="T46">он(а).[NOM]</ta>
            <ta e="T48" id="Seg_1353" s="T47">NEG</ta>
            <ta e="T49" id="Seg_1354" s="T48">съесть-HAB-PST.NAR-3SG.O</ta>
            <ta e="T50" id="Seg_1355" s="T49">он(а)-EP-DU-COM</ta>
            <ta e="T51" id="Seg_1356" s="T50">жить-CVB</ta>
            <ta e="T52" id="Seg_1357" s="T51">свёкор.[NOM]-3SG</ta>
            <ta e="T53" id="Seg_1358" s="T52">сказать-DRV-HAB-PST.NAR-3SG.S</ta>
            <ta e="T54" id="Seg_1359" s="T53">съесть-FRQ-INF</ta>
            <ta e="T55" id="Seg_1360" s="T54">надо</ta>
            <ta e="T56" id="Seg_1361" s="T55">стол-%%</ta>
            <ta e="T57" id="Seg_1362" s="T56">собрать-HAB-PST.NAR-3PL</ta>
            <ta e="T58" id="Seg_1363" s="T57">собрать-INF</ta>
            <ta e="T59" id="Seg_1364" s="T58">надо</ta>
            <ta e="T60" id="Seg_1365" s="T59">съесть-FRQ-INF</ta>
            <ta e="T61" id="Seg_1366" s="T60">хлеб-EP-ACC</ta>
            <ta e="T62" id="Seg_1367" s="T61">отрезать-DRV-HAB-PST.NAR-3PL</ta>
            <ta e="T63" id="Seg_1368" s="T62">хлеб-EP-ACC</ta>
            <ta e="T64" id="Seg_1369" s="T63">отрезать-DRV-INF</ta>
            <ta e="T65" id="Seg_1370" s="T64">надо</ta>
            <ta e="T66" id="Seg_1371" s="T65">три</ta>
            <ta e="T67" id="Seg_1372" s="T66">ломоть-EP-GEN</ta>
            <ta e="T68" id="Seg_1373" s="T67">по</ta>
            <ta e="T69" id="Seg_1374" s="T68">съесть-HAB-PST.NAR-3PL</ta>
            <ta e="T70" id="Seg_1375" s="T69">свёкор.[NOM]-3SG</ta>
            <ta e="T71" id="Seg_1376" s="T70">сказать-%%-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T72" id="Seg_1377" s="T71">насытиться-1DU</ta>
            <ta e="T73" id="Seg_1378" s="T72">прочь</ta>
            <ta e="T74" id="Seg_1379" s="T73">отправиться-TR-DRV-INF</ta>
            <ta e="T75" id="Seg_1380" s="T74">хлеб-ACC</ta>
            <ta e="T76" id="Seg_1381" s="T75">весь</ta>
            <ta e="T77" id="Seg_1382" s="T76">съесть-PTCP.NEC-нечто-ACC</ta>
            <ta e="T78" id="Seg_1383" s="T77">мы.[NOM]</ta>
            <ta e="T79" id="Seg_1384" s="T78">теперь</ta>
            <ta e="T80" id="Seg_1385" s="T79">насытиться-PST.NAR-1DU</ta>
            <ta e="T81" id="Seg_1386" s="T80">сноха.[NOM]-3SG</ta>
            <ta e="T82" id="Seg_1387" s="T81">убрать-HAB-PST.NAR-3SG.O</ta>
            <ta e="T83" id="Seg_1388" s="T82">весь</ta>
            <ta e="T84" id="Seg_1389" s="T83">съесть-PTCP.NEC-нечто-ACC</ta>
            <ta e="T85" id="Seg_1390" s="T84">свёкор.[NOM]-3SG</ta>
            <ta e="T86" id="Seg_1391" s="T85">и</ta>
            <ta e="T87" id="Seg_1392" s="T86">свекровь.[NOM]-3SG</ta>
            <ta e="T88" id="Seg_1393" s="T87">съесть-%%-HAB-PST.NAR-3DU.O</ta>
            <ta e="T89" id="Seg_1394" s="T88">тайком</ta>
            <ta e="T90" id="Seg_1395" s="T89">съесть-FRQ-INF-PST.NAR-3DU.S</ta>
            <ta e="T91" id="Seg_1396" s="T90">тайком</ta>
            <ta e="T92" id="Seg_1397" s="T91">сам.3DU</ta>
            <ta e="T93" id="Seg_1398" s="T92">а</ta>
            <ta e="T94" id="Seg_1399" s="T93">он(а).[NOM]</ta>
            <ta e="T95" id="Seg_1400" s="T94">голодный-ADVZ</ta>
            <ta e="T96" id="Seg_1401" s="T95">быть-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T97" id="Seg_1402" s="T96">работать-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T98" id="Seg_1403" s="T97">проголодаться-CVB</ta>
            <ta e="T99" id="Seg_1404" s="T98">съесть-FRQ-INF</ta>
            <ta e="T100" id="Seg_1405" s="T99">хотеть-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T101" id="Seg_1406" s="T100">сноха.[NOM]-3SG</ta>
            <ta e="T102" id="Seg_1407" s="T101">голодный-ADVZ</ta>
            <ta e="T103" id="Seg_1408" s="T102">быть-HAB-PST.NAR-3SG.S</ta>
            <ta e="T104" id="Seg_1409" s="T103">быть-HAB-PST.NAR-3SG.S</ta>
            <ta e="T105" id="Seg_1410" s="T104">свой.3SG</ta>
            <ta e="T106" id="Seg_1411" s="T105">мать-OBL.3SG-ALL</ta>
            <ta e="T107" id="Seg_1412" s="T106">отправиться-PST.NAR.[3SG.S]</ta>
            <ta e="T108" id="Seg_1413" s="T107">мать-OBL.3SG-ALL</ta>
            <ta e="T109" id="Seg_1414" s="T108">сказать-DRV-PST.NAR-3SG.S</ta>
            <ta e="T110" id="Seg_1415" s="T109">что</ta>
            <ta e="T111" id="Seg_1416" s="T110">голодный-ADVZ</ta>
            <ta e="T112" id="Seg_1417" s="T111">%идти-CO-1SG.S</ta>
            <ta e="T113" id="Seg_1418" s="T112">голодный-ADVZ</ta>
            <ta e="T114" id="Seg_1419" s="T113">ходить-1SG.S</ta>
            <ta e="T115" id="Seg_1420" s="T114">съесть-FRQ-INF</ta>
            <ta e="T116" id="Seg_1421" s="T115">NEG</ta>
            <ta e="T117" id="Seg_1422" s="T116">дать-HAB-3DU.O</ta>
            <ta e="T118" id="Seg_1423" s="T117">я.NOM</ta>
            <ta e="T119" id="Seg_1424" s="T118">NEG</ta>
            <ta e="T120" id="Seg_1425" s="T119">насытиться-HAB-CO-1SG.S</ta>
            <ta e="T121" id="Seg_1426" s="T120">сноха-OBL.3SG-ADES</ta>
            <ta e="T122" id="Seg_1427" s="T121">три</ta>
            <ta e="T123" id="Seg_1428" s="T122">брат.[NOM]-3SG</ta>
            <ta e="T124" id="Seg_1429" s="T123">быть-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T125" id="Seg_1430" s="T124">брат.[NOM]-3SG</ta>
            <ta e="T126" id="Seg_1431" s="T125">сказать-HAB-PST.NAR-3SG.S</ta>
            <ta e="T127" id="Seg_1432" s="T126">отправиться-EP-IMP.2SG.S</ta>
            <ta e="T128" id="Seg_1433" s="T127">дом.[NOM]</ta>
            <ta e="T129" id="Seg_1434" s="T128">сварить-EP-IMP.2SG.S</ta>
            <ta e="T130" id="Seg_1435" s="T129">хороший-DRV</ta>
            <ta e="T131" id="Seg_1436" s="T130">еда.[NOM]</ta>
            <ta e="T132" id="Seg_1437" s="T131">сварить-IMP.2SG.S</ta>
            <ta e="T133" id="Seg_1438" s="T132">а</ta>
            <ta e="T134" id="Seg_1439" s="T133">я.NOM</ta>
            <ta e="T135" id="Seg_1440" s="T134">ты.ALL</ta>
            <ta e="T136" id="Seg_1441" s="T135">прийти-FUT-1SG.S-FUT</ta>
            <ta e="T137" id="Seg_1442" s="T136">сидеть-DRV-INF</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1443" s="T1">n.[n:case]-n:poss</ta>
            <ta e="T3" id="Seg_1444" s="T2">n.[n:case]-n:poss</ta>
            <ta e="T4" id="Seg_1445" s="T3">n.[n:case]</ta>
            <ta e="T5" id="Seg_1446" s="T4">n-n&gt;n-n:num.[n:case]</ta>
            <ta e="T6" id="Seg_1447" s="T5">v-v:inf</ta>
            <ta e="T7" id="Seg_1448" s="T6">v-v&gt;v-v:ins-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_1449" s="T7">n-n:case-n:poss</ta>
            <ta e="T9" id="Seg_1450" s="T8">v-v:inf</ta>
            <ta e="T10" id="Seg_1451" s="T9">adj</ta>
            <ta e="T11" id="Seg_1452" s="T10">n-n:case.poss</ta>
            <ta e="T12" id="Seg_1453" s="T11">v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_1454" s="T12">v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_1455" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_1456" s="T14">n-n:case-n:poss</ta>
            <ta e="T16" id="Seg_1457" s="T15">v-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_1458" s="T16">n.[n:case]-n:poss</ta>
            <ta e="T18" id="Seg_1459" s="T17">v-v&gt;adv</ta>
            <ta e="T19" id="Seg_1460" s="T18">v-v:tense.[v:pn]</ta>
            <ta e="T20" id="Seg_1461" s="T19">adv-n:case</ta>
            <ta e="T21" id="Seg_1462" s="T20">n-n:obl.poss-n:case</ta>
            <ta e="T22" id="Seg_1463" s="T21">n-n:obl.poss-n:case</ta>
            <ta e="T23" id="Seg_1464" s="T22">n-n:case-n:poss</ta>
            <ta e="T24" id="Seg_1465" s="T23">v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_1466" s="T24">n.[n:case]-n:poss</ta>
            <ta e="T26" id="Seg_1467" s="T25">n-n:case</ta>
            <ta e="T27" id="Seg_1468" s="T26">n-%%</ta>
            <ta e="T28" id="Seg_1469" s="T27">v-v:tense.[v:pn]</ta>
            <ta e="T29" id="Seg_1470" s="T28">conj</ta>
            <ta e="T30" id="Seg_1471" s="T29">n.[n:case]-n:poss</ta>
            <ta e="T31" id="Seg_1472" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_1473" s="T31">v-v&gt;ptcp</ta>
            <ta e="T33" id="Seg_1474" s="T32">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T34" id="Seg_1475" s="T33">adv</ta>
            <ta e="T35" id="Seg_1476" s="T34">v-v:tense-v:pn</ta>
            <ta e="T36" id="Seg_1477" s="T35">conj</ta>
            <ta e="T37" id="Seg_1478" s="T36">n-n:case-n:poss</ta>
            <ta e="T38" id="Seg_1479" s="T37">v-v:inf</ta>
            <ta e="T39" id="Seg_1480" s="T38">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T40" id="Seg_1481" s="T39">conj</ta>
            <ta e="T41" id="Seg_1482" s="T40">v-v&gt;ptcp-n-n:case</ta>
            <ta e="T42" id="Seg_1483" s="T41">adj-adj&gt;adv</ta>
            <ta e="T43" id="Seg_1484" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_1485" s="T43">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T45" id="Seg_1486" s="T44">adj</ta>
            <ta e="T46" id="Seg_1487" s="T45">n-n:ins-n:case</ta>
            <ta e="T47" id="Seg_1488" s="T46">pers.[n:case]</ta>
            <ta e="T48" id="Seg_1489" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_1490" s="T48">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T50" id="Seg_1491" s="T49">pers-n:ins-n:num-n:case</ta>
            <ta e="T51" id="Seg_1492" s="T50">v-v&gt;adv</ta>
            <ta e="T52" id="Seg_1493" s="T51">n.[n:case]-n:poss</ta>
            <ta e="T53" id="Seg_1494" s="T52">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T54" id="Seg_1495" s="T53">v-v&gt;v-v:inf</ta>
            <ta e="T55" id="Seg_1496" s="T54">ptcl</ta>
            <ta e="T56" id="Seg_1497" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_1498" s="T56">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T58" id="Seg_1499" s="T57">v-v:inf</ta>
            <ta e="T59" id="Seg_1500" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_1501" s="T59">v-v&gt;v-v:inf</ta>
            <ta e="T61" id="Seg_1502" s="T60">n-n:ins-n:case</ta>
            <ta e="T62" id="Seg_1503" s="T61">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T63" id="Seg_1504" s="T62">n-n:ins-n:case</ta>
            <ta e="T64" id="Seg_1505" s="T63">v-v&gt;v-v:inf</ta>
            <ta e="T65" id="Seg_1506" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_1507" s="T65">num</ta>
            <ta e="T67" id="Seg_1508" s="T66">n-n:ins-n:case</ta>
            <ta e="T68" id="Seg_1509" s="T67">pp</ta>
            <ta e="T69" id="Seg_1510" s="T68">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T70" id="Seg_1511" s="T69">n.[n:case]-n:poss</ta>
            <ta e="T71" id="Seg_1512" s="T70">v-v&gt;v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T72" id="Seg_1513" s="T71">v-v:pn</ta>
            <ta e="T73" id="Seg_1514" s="T72">preverb</ta>
            <ta e="T74" id="Seg_1515" s="T73">v-v&gt;v-v&gt;v-v:inf</ta>
            <ta e="T75" id="Seg_1516" s="T74">n-n:case</ta>
            <ta e="T76" id="Seg_1517" s="T75">quant</ta>
            <ta e="T77" id="Seg_1518" s="T76">v-v&gt;ptcp-n-n:case</ta>
            <ta e="T78" id="Seg_1519" s="T77">pers.[n:case]</ta>
            <ta e="T79" id="Seg_1520" s="T78">adv</ta>
            <ta e="T80" id="Seg_1521" s="T79">v-v:tense-v:pn</ta>
            <ta e="T81" id="Seg_1522" s="T80">n.[n:case]-n:poss</ta>
            <ta e="T82" id="Seg_1523" s="T81">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T83" id="Seg_1524" s="T82">quant</ta>
            <ta e="T84" id="Seg_1525" s="T83">v-v&gt;ptcp-n-n:case</ta>
            <ta e="T85" id="Seg_1526" s="T84">n.[n:case]-n:poss</ta>
            <ta e="T86" id="Seg_1527" s="T85">conj</ta>
            <ta e="T87" id="Seg_1528" s="T86">n.[n:case]-n:poss</ta>
            <ta e="T88" id="Seg_1529" s="T87">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T89" id="Seg_1530" s="T88">adv</ta>
            <ta e="T90" id="Seg_1531" s="T89">v-v&gt;v-v:inf-v:tense-v:pn</ta>
            <ta e="T91" id="Seg_1532" s="T90">adv</ta>
            <ta e="T92" id="Seg_1533" s="T91">emphpro</ta>
            <ta e="T93" id="Seg_1534" s="T92">conj</ta>
            <ta e="T94" id="Seg_1535" s="T93">pers.[n:case]</ta>
            <ta e="T95" id="Seg_1536" s="T94">adj-adj&gt;adv</ta>
            <ta e="T96" id="Seg_1537" s="T95">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T97" id="Seg_1538" s="T96">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T98" id="Seg_1539" s="T97">v-v&gt;adv</ta>
            <ta e="T99" id="Seg_1540" s="T98">v-v&gt;v-v:inf</ta>
            <ta e="T100" id="Seg_1541" s="T99">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T101" id="Seg_1542" s="T100">n.[n:case]-n:poss</ta>
            <ta e="T102" id="Seg_1543" s="T101">adj-adj&gt;adv</ta>
            <ta e="T103" id="Seg_1544" s="T102">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T104" id="Seg_1545" s="T103">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T105" id="Seg_1546" s="T104">emphpro</ta>
            <ta e="T106" id="Seg_1547" s="T105">n-n:obl.poss-n:case</ta>
            <ta e="T107" id="Seg_1548" s="T106">v-v:tense.[v:pn]</ta>
            <ta e="T108" id="Seg_1549" s="T107">n-n:obl.poss-n:case</ta>
            <ta e="T109" id="Seg_1550" s="T108">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T110" id="Seg_1551" s="T109">conj</ta>
            <ta e="T111" id="Seg_1552" s="T110">adj-adj&gt;adv</ta>
            <ta e="T112" id="Seg_1553" s="T111">v-v:ins-v:pn</ta>
            <ta e="T113" id="Seg_1554" s="T112">adj-adj&gt;adv</ta>
            <ta e="T114" id="Seg_1555" s="T113">v-v:pn</ta>
            <ta e="T115" id="Seg_1556" s="T114">v-v&gt;v-v:inf</ta>
            <ta e="T116" id="Seg_1557" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_1558" s="T116">v-v&gt;v-v:pn</ta>
            <ta e="T118" id="Seg_1559" s="T117">pers</ta>
            <ta e="T119" id="Seg_1560" s="T118">ptcl</ta>
            <ta e="T120" id="Seg_1561" s="T119">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T121" id="Seg_1562" s="T120">n-n:obl.poss-n:case</ta>
            <ta e="T122" id="Seg_1563" s="T121">num</ta>
            <ta e="T123" id="Seg_1564" s="T122">n.[n:case]-n:poss</ta>
            <ta e="T124" id="Seg_1565" s="T123">v-v:tense-v:mood.[v:pn]</ta>
            <ta e="T125" id="Seg_1566" s="T124">n.[n:case]-n:poss</ta>
            <ta e="T126" id="Seg_1567" s="T125">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T127" id="Seg_1568" s="T126">v-v:ins-v:mood.pn</ta>
            <ta e="T128" id="Seg_1569" s="T127">n.[n:case]</ta>
            <ta e="T129" id="Seg_1570" s="T128">v-n:ins-v:mood.pn</ta>
            <ta e="T130" id="Seg_1571" s="T129">adj-adj&gt;adj</ta>
            <ta e="T131" id="Seg_1572" s="T130">n.[n:case]</ta>
            <ta e="T132" id="Seg_1573" s="T131">v-v:mood.pn</ta>
            <ta e="T133" id="Seg_1574" s="T132">conj</ta>
            <ta e="T134" id="Seg_1575" s="T133">pers</ta>
            <ta e="T135" id="Seg_1576" s="T134">pers</ta>
            <ta e="T136" id="Seg_1577" s="T135">v-v:tense-v:pn-v:tense</ta>
            <ta e="T137" id="Seg_1578" s="T136">v-v&gt;v-v:inf</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1579" s="T1">n</ta>
            <ta e="T3" id="Seg_1580" s="T2">n</ta>
            <ta e="T4" id="Seg_1581" s="T3">n</ta>
            <ta e="T5" id="Seg_1582" s="T4">n</ta>
            <ta e="T6" id="Seg_1583" s="T5">v</ta>
            <ta e="T7" id="Seg_1584" s="T6">v</ta>
            <ta e="T8" id="Seg_1585" s="T7">n</ta>
            <ta e="T9" id="Seg_1586" s="T8">v</ta>
            <ta e="T10" id="Seg_1587" s="T9">adj</ta>
            <ta e="T11" id="Seg_1588" s="T10">n</ta>
            <ta e="T12" id="Seg_1589" s="T11">v</ta>
            <ta e="T13" id="Seg_1590" s="T12">v</ta>
            <ta e="T14" id="Seg_1591" s="T13">n</ta>
            <ta e="T15" id="Seg_1592" s="T14">n</ta>
            <ta e="T16" id="Seg_1593" s="T15">v</ta>
            <ta e="T17" id="Seg_1594" s="T16">n</ta>
            <ta e="T18" id="Seg_1595" s="T17">adv</ta>
            <ta e="T19" id="Seg_1596" s="T18">v</ta>
            <ta e="T20" id="Seg_1597" s="T19">adv</ta>
            <ta e="T21" id="Seg_1598" s="T20">n</ta>
            <ta e="T22" id="Seg_1599" s="T21">n</ta>
            <ta e="T23" id="Seg_1600" s="T22">n</ta>
            <ta e="T24" id="Seg_1601" s="T23">v</ta>
            <ta e="T25" id="Seg_1602" s="T24">n</ta>
            <ta e="T26" id="Seg_1603" s="T25">n</ta>
            <ta e="T27" id="Seg_1604" s="T26">n</ta>
            <ta e="T28" id="Seg_1605" s="T27">v</ta>
            <ta e="T29" id="Seg_1606" s="T28">conj</ta>
            <ta e="T30" id="Seg_1607" s="T29">n</ta>
            <ta e="T31" id="Seg_1608" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_1609" s="T31">v</ta>
            <ta e="T33" id="Seg_1610" s="T32">n</ta>
            <ta e="T34" id="Seg_1611" s="T33">adv</ta>
            <ta e="T35" id="Seg_1612" s="T34">v</ta>
            <ta e="T36" id="Seg_1613" s="T35">conj</ta>
            <ta e="T37" id="Seg_1614" s="T36">n</ta>
            <ta e="T38" id="Seg_1615" s="T37">v</ta>
            <ta e="T39" id="Seg_1616" s="T38">v</ta>
            <ta e="T40" id="Seg_1617" s="T39">conj</ta>
            <ta e="T41" id="Seg_1618" s="T40">n</ta>
            <ta e="T42" id="Seg_1619" s="T41">adv</ta>
            <ta e="T43" id="Seg_1620" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_1621" s="T43">v</ta>
            <ta e="T45" id="Seg_1622" s="T44">adj</ta>
            <ta e="T46" id="Seg_1623" s="T45">n</ta>
            <ta e="T47" id="Seg_1624" s="T46">pers</ta>
            <ta e="T48" id="Seg_1625" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_1626" s="T48">v</ta>
            <ta e="T50" id="Seg_1627" s="T49">pers</ta>
            <ta e="T51" id="Seg_1628" s="T50">adv</ta>
            <ta e="T52" id="Seg_1629" s="T51">n</ta>
            <ta e="T53" id="Seg_1630" s="T52">v</ta>
            <ta e="T54" id="Seg_1631" s="T53">v</ta>
            <ta e="T55" id="Seg_1632" s="T54">ptcl</ta>
            <ta e="T56" id="Seg_1633" s="T55">n</ta>
            <ta e="T57" id="Seg_1634" s="T56">v</ta>
            <ta e="T58" id="Seg_1635" s="T57">v</ta>
            <ta e="T59" id="Seg_1636" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_1637" s="T59">v</ta>
            <ta e="T61" id="Seg_1638" s="T60">n</ta>
            <ta e="T62" id="Seg_1639" s="T61">v</ta>
            <ta e="T63" id="Seg_1640" s="T62">n</ta>
            <ta e="T64" id="Seg_1641" s="T63">v</ta>
            <ta e="T65" id="Seg_1642" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_1643" s="T65">num</ta>
            <ta e="T67" id="Seg_1644" s="T66">n</ta>
            <ta e="T68" id="Seg_1645" s="T67">pp</ta>
            <ta e="T69" id="Seg_1646" s="T68">v</ta>
            <ta e="T70" id="Seg_1647" s="T69">n</ta>
            <ta e="T71" id="Seg_1648" s="T70">v</ta>
            <ta e="T72" id="Seg_1649" s="T71">v</ta>
            <ta e="T73" id="Seg_1650" s="T72">preverb</ta>
            <ta e="T74" id="Seg_1651" s="T73">v</ta>
            <ta e="T75" id="Seg_1652" s="T74">n</ta>
            <ta e="T76" id="Seg_1653" s="T75">quant</ta>
            <ta e="T77" id="Seg_1654" s="T76">n</ta>
            <ta e="T78" id="Seg_1655" s="T77">pers</ta>
            <ta e="T79" id="Seg_1656" s="T78">adv</ta>
            <ta e="T80" id="Seg_1657" s="T79">v</ta>
            <ta e="T81" id="Seg_1658" s="T80">n</ta>
            <ta e="T82" id="Seg_1659" s="T81">v</ta>
            <ta e="T83" id="Seg_1660" s="T82">quant</ta>
            <ta e="T84" id="Seg_1661" s="T83">n</ta>
            <ta e="T85" id="Seg_1662" s="T84">n</ta>
            <ta e="T86" id="Seg_1663" s="T85">conj</ta>
            <ta e="T87" id="Seg_1664" s="T86">n</ta>
            <ta e="T88" id="Seg_1665" s="T87">v</ta>
            <ta e="T89" id="Seg_1666" s="T88">adv</ta>
            <ta e="T90" id="Seg_1667" s="T89">v</ta>
            <ta e="T91" id="Seg_1668" s="T90">adv</ta>
            <ta e="T92" id="Seg_1669" s="T91">emphpro</ta>
            <ta e="T93" id="Seg_1670" s="T92">conj</ta>
            <ta e="T94" id="Seg_1671" s="T93">pers</ta>
            <ta e="T95" id="Seg_1672" s="T94">adv</ta>
            <ta e="T96" id="Seg_1673" s="T95">v</ta>
            <ta e="T97" id="Seg_1674" s="T96">v</ta>
            <ta e="T98" id="Seg_1675" s="T97">adv</ta>
            <ta e="T99" id="Seg_1676" s="T98">v</ta>
            <ta e="T100" id="Seg_1677" s="T99">v</ta>
            <ta e="T101" id="Seg_1678" s="T100">n</ta>
            <ta e="T102" id="Seg_1679" s="T101">adv</ta>
            <ta e="T103" id="Seg_1680" s="T102">v</ta>
            <ta e="T104" id="Seg_1681" s="T103">v</ta>
            <ta e="T105" id="Seg_1682" s="T104">emphpro</ta>
            <ta e="T106" id="Seg_1683" s="T105">n</ta>
            <ta e="T107" id="Seg_1684" s="T106">v</ta>
            <ta e="T108" id="Seg_1685" s="T107">n</ta>
            <ta e="T109" id="Seg_1686" s="T108">v</ta>
            <ta e="T110" id="Seg_1687" s="T109">conj</ta>
            <ta e="T111" id="Seg_1688" s="T110">adv</ta>
            <ta e="T112" id="Seg_1689" s="T111">v</ta>
            <ta e="T113" id="Seg_1690" s="T112">adv</ta>
            <ta e="T114" id="Seg_1691" s="T113">v</ta>
            <ta e="T115" id="Seg_1692" s="T114">v</ta>
            <ta e="T116" id="Seg_1693" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_1694" s="T116">v</ta>
            <ta e="T118" id="Seg_1695" s="T117">pers</ta>
            <ta e="T119" id="Seg_1696" s="T118">ptcl</ta>
            <ta e="T120" id="Seg_1697" s="T119">v</ta>
            <ta e="T121" id="Seg_1698" s="T120">n</ta>
            <ta e="T122" id="Seg_1699" s="T121">num</ta>
            <ta e="T123" id="Seg_1700" s="T122">n</ta>
            <ta e="T124" id="Seg_1701" s="T123">v</ta>
            <ta e="T125" id="Seg_1702" s="T124">n</ta>
            <ta e="T126" id="Seg_1703" s="T125">v</ta>
            <ta e="T127" id="Seg_1704" s="T126">v</ta>
            <ta e="T128" id="Seg_1705" s="T127">n</ta>
            <ta e="T129" id="Seg_1706" s="T128">v</ta>
            <ta e="T130" id="Seg_1707" s="T129">adj</ta>
            <ta e="T131" id="Seg_1708" s="T130">n</ta>
            <ta e="T132" id="Seg_1709" s="T131">v</ta>
            <ta e="T133" id="Seg_1710" s="T132">conj</ta>
            <ta e="T134" id="Seg_1711" s="T133">pers</ta>
            <ta e="T135" id="Seg_1712" s="T134">pers</ta>
            <ta e="T136" id="Seg_1713" s="T135">v</ta>
            <ta e="T137" id="Seg_1714" s="T136">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_1715" s="T1">np.h:E</ta>
            <ta e="T3" id="Seg_1716" s="T2">np.h:E</ta>
            <ta e="T6" id="Seg_1717" s="T5">v:Th</ta>
            <ta e="T8" id="Seg_1718" s="T7">np.h:Th 0.3.h:Poss</ta>
            <ta e="T11" id="Seg_1719" s="T10">np:So</ta>
            <ta e="T12" id="Seg_1720" s="T11">0.3.h:A 0.3.h:Th</ta>
            <ta e="T13" id="Seg_1721" s="T12">0.3.h:A</ta>
            <ta e="T14" id="Seg_1722" s="T13">np.h:Th</ta>
            <ta e="T15" id="Seg_1723" s="T14">np.h:Th 0.3.h:Poss</ta>
            <ta e="T16" id="Seg_1724" s="T15">0.3.h:A</ta>
            <ta e="T17" id="Seg_1725" s="T16">np.h:A 0.3.h:Poss</ta>
            <ta e="T20" id="Seg_1726" s="T19">adv:G</ta>
            <ta e="T21" id="Seg_1727" s="T20">np:Com 0.3.h:Poss</ta>
            <ta e="T22" id="Seg_1728" s="T21">np:Com 0.3.h:Poss</ta>
            <ta e="T23" id="Seg_1729" s="T22">np.h:Th 0.3.h:Poss</ta>
            <ta e="T24" id="Seg_1730" s="T23">0.3.h:A</ta>
            <ta e="T25" id="Seg_1731" s="T24">np.h:A 0.3.h:Poss</ta>
            <ta e="T26" id="Seg_1732" s="T25">np:Time</ta>
            <ta e="T27" id="Seg_1733" s="T26">np:Time</ta>
            <ta e="T30" id="Seg_1734" s="T29">np.h:Th 0.3.h:Poss</ta>
            <ta e="T35" id="Seg_1735" s="T34">0.3.h:A</ta>
            <ta e="T37" id="Seg_1736" s="T36">np.h:Th 0.3.h:Poss</ta>
            <ta e="T39" id="Seg_1737" s="T38">0.3.h:A</ta>
            <ta e="T41" id="Seg_1738" s="T40">np:Th</ta>
            <ta e="T44" id="Seg_1739" s="T43">0.3.h:A</ta>
            <ta e="T46" id="Seg_1740" s="T45">np:P</ta>
            <ta e="T47" id="Seg_1741" s="T46">pro.h:A</ta>
            <ta e="T50" id="Seg_1742" s="T49">pro:Com</ta>
            <ta e="T52" id="Seg_1743" s="T51">np.h:A 0.3.h:Poss</ta>
            <ta e="T54" id="Seg_1744" s="T53">v:Th</ta>
            <ta e="T56" id="Seg_1745" s="T55">np:G</ta>
            <ta e="T57" id="Seg_1746" s="T56">0.3.h:A</ta>
            <ta e="T58" id="Seg_1747" s="T57">v:Th</ta>
            <ta e="T61" id="Seg_1748" s="T60">np:P</ta>
            <ta e="T62" id="Seg_1749" s="T61">0.3.h:A</ta>
            <ta e="T63" id="Seg_1750" s="T62">np:P</ta>
            <ta e="T64" id="Seg_1751" s="T63">v:Th</ta>
            <ta e="T67" id="Seg_1752" s="T66">np:P</ta>
            <ta e="T69" id="Seg_1753" s="T68">0.3.h:A</ta>
            <ta e="T70" id="Seg_1754" s="T69">np.h:A</ta>
            <ta e="T72" id="Seg_1755" s="T71">0.1.h:A</ta>
            <ta e="T78" id="Seg_1756" s="T77">pro.h:A</ta>
            <ta e="T79" id="Seg_1757" s="T78">adv:Time</ta>
            <ta e="T81" id="Seg_1758" s="T80">np.h:A 0.3.h:Poss</ta>
            <ta e="T84" id="Seg_1759" s="T83">np:Th</ta>
            <ta e="T85" id="Seg_1760" s="T84">np.h:A 0.3.h:Poss</ta>
            <ta e="T87" id="Seg_1761" s="T86">np.h:A 0.3.h:Poss</ta>
            <ta e="T90" id="Seg_1762" s="T89">0.3.h:A</ta>
            <ta e="T94" id="Seg_1763" s="T93">pro.h:Th</ta>
            <ta e="T97" id="Seg_1764" s="T96">0.3.h:A</ta>
            <ta e="T99" id="Seg_1765" s="T98">v:Th</ta>
            <ta e="T100" id="Seg_1766" s="T99">0.3.h:E</ta>
            <ta e="T101" id="Seg_1767" s="T100">np.h:Th</ta>
            <ta e="T106" id="Seg_1768" s="T105">np.h:G 0.3.h:Poss</ta>
            <ta e="T107" id="Seg_1769" s="T106">0.3.h:A</ta>
            <ta e="T108" id="Seg_1770" s="T107">np.h:R 0.3.h:Poss</ta>
            <ta e="T109" id="Seg_1771" s="T108">0.3.h:A</ta>
            <ta e="T112" id="Seg_1772" s="T111">0.1.h:A</ta>
            <ta e="T114" id="Seg_1773" s="T113">0.1.h:A</ta>
            <ta e="T117" id="Seg_1774" s="T116">0.3.h:A</ta>
            <ta e="T118" id="Seg_1775" s="T117">pro.h:A</ta>
            <ta e="T121" id="Seg_1776" s="T120">np.h:Poss</ta>
            <ta e="T123" id="Seg_1777" s="T122">np.h:Th 0.3.h:Poss</ta>
            <ta e="T125" id="Seg_1778" s="T124">np.h:A 0.3.h:Poss</ta>
            <ta e="T127" id="Seg_1779" s="T126">0.2.h:A</ta>
            <ta e="T129" id="Seg_1780" s="T128">0.2.h:A</ta>
            <ta e="T131" id="Seg_1781" s="T130">np:P</ta>
            <ta e="T132" id="Seg_1782" s="T131">0.2.h:A</ta>
            <ta e="T134" id="Seg_1783" s="T133">pro.h:A</ta>
            <ta e="T135" id="Seg_1784" s="T134">pro.h:G</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_1785" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_1786" s="T2">np.h:S</ta>
            <ta e="T6" id="Seg_1787" s="T5">v:O</ta>
            <ta e="T7" id="Seg_1788" s="T6">v:pred</ta>
            <ta e="T12" id="Seg_1789" s="T11">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T13" id="Seg_1790" s="T12">0.3.h:S v:pred</ta>
            <ta e="T14" id="Seg_1791" s="T13">np.h:O</ta>
            <ta e="T15" id="Seg_1792" s="T14">np.h:O</ta>
            <ta e="T16" id="Seg_1793" s="T15">0.3.h:S v:pred</ta>
            <ta e="T17" id="Seg_1794" s="T16">np.h:S</ta>
            <ta e="T18" id="Seg_1795" s="T17">s:temp</ta>
            <ta e="T19" id="Seg_1796" s="T18">v:pred</ta>
            <ta e="T23" id="Seg_1797" s="T22">np.h:O</ta>
            <ta e="T24" id="Seg_1798" s="T23">0.3.h:S v:pred</ta>
            <ta e="T25" id="Seg_1799" s="T24">np.h:S</ta>
            <ta e="T28" id="Seg_1800" s="T27">v:pred</ta>
            <ta e="T30" id="Seg_1801" s="T29">np.h:S</ta>
            <ta e="T33" id="Seg_1802" s="T32">n:pred</ta>
            <ta e="T35" id="Seg_1803" s="T34">0.3.h:S v:pred</ta>
            <ta e="T37" id="Seg_1804" s="T36">np.h:O</ta>
            <ta e="T39" id="Seg_1805" s="T38">0.3.h:S v:pred</ta>
            <ta e="T41" id="Seg_1806" s="T40">np:O</ta>
            <ta e="T44" id="Seg_1807" s="T43">0.3.h:S v:pred</ta>
            <ta e="T46" id="Seg_1808" s="T45">np:O</ta>
            <ta e="T47" id="Seg_1809" s="T46">pro.h:S</ta>
            <ta e="T49" id="Seg_1810" s="T48">v:pred</ta>
            <ta e="T51" id="Seg_1811" s="T49">s:temp</ta>
            <ta e="T52" id="Seg_1812" s="T51">np.h:S</ta>
            <ta e="T53" id="Seg_1813" s="T52">v:pred</ta>
            <ta e="T54" id="Seg_1814" s="T53">v:O</ta>
            <ta e="T55" id="Seg_1815" s="T54">ptcl:pred</ta>
            <ta e="T57" id="Seg_1816" s="T56">0.3.h:S v:pred</ta>
            <ta e="T58" id="Seg_1817" s="T57">v:O</ta>
            <ta e="T59" id="Seg_1818" s="T58">ptcl:pred</ta>
            <ta e="T60" id="Seg_1819" s="T59">s:purp</ta>
            <ta e="T61" id="Seg_1820" s="T60">np:O</ta>
            <ta e="T62" id="Seg_1821" s="T61">0.3.h:S v:pred</ta>
            <ta e="T64" id="Seg_1822" s="T63">v:O</ta>
            <ta e="T65" id="Seg_1823" s="T64">ptcl:pred</ta>
            <ta e="T67" id="Seg_1824" s="T66">np:O</ta>
            <ta e="T69" id="Seg_1825" s="T68">0.3.h:S v:pred</ta>
            <ta e="T70" id="Seg_1826" s="T69">np.h:S</ta>
            <ta e="T71" id="Seg_1827" s="T70">v:pred</ta>
            <ta e="T72" id="Seg_1828" s="T71">0.1.h:S v:pred</ta>
            <ta e="T78" id="Seg_1829" s="T77">pro.h:S</ta>
            <ta e="T80" id="Seg_1830" s="T79">v:pred</ta>
            <ta e="T81" id="Seg_1831" s="T80">np.h:S</ta>
            <ta e="T82" id="Seg_1832" s="T81">v:pred</ta>
            <ta e="T84" id="Seg_1833" s="T83">np:O</ta>
            <ta e="T85" id="Seg_1834" s="T84">np.h:S</ta>
            <ta e="T87" id="Seg_1835" s="T86">np.h:S</ta>
            <ta e="T88" id="Seg_1836" s="T87">v:pred</ta>
            <ta e="T90" id="Seg_1837" s="T89">0.3.h:S v:pred</ta>
            <ta e="T94" id="Seg_1838" s="T93">pro.h:S</ta>
            <ta e="T95" id="Seg_1839" s="T94">adj:pred</ta>
            <ta e="T96" id="Seg_1840" s="T95">cop</ta>
            <ta e="T97" id="Seg_1841" s="T96">0.3.h:S v:pred</ta>
            <ta e="T98" id="Seg_1842" s="T97">s:temp</ta>
            <ta e="T99" id="Seg_1843" s="T98">v:O</ta>
            <ta e="T100" id="Seg_1844" s="T99">0.3.h:S v:pred</ta>
            <ta e="T101" id="Seg_1845" s="T100">np.h:S</ta>
            <ta e="T102" id="Seg_1846" s="T101">adj:pred</ta>
            <ta e="T103" id="Seg_1847" s="T102">cop</ta>
            <ta e="T107" id="Seg_1848" s="T106">0.3.h:S v:pred</ta>
            <ta e="T109" id="Seg_1849" s="T108">0.3.h:S v:pred</ta>
            <ta e="T112" id="Seg_1850" s="T111">0.1.h:S v:pred</ta>
            <ta e="T114" id="Seg_1851" s="T113">0.1.h:S v:pred</ta>
            <ta e="T117" id="Seg_1852" s="T116">0.3.h:S v:pred</ta>
            <ta e="T118" id="Seg_1853" s="T117">pro.h:S</ta>
            <ta e="T120" id="Seg_1854" s="T119">v:pred</ta>
            <ta e="T123" id="Seg_1855" s="T122">np.h:S</ta>
            <ta e="T124" id="Seg_1856" s="T123">v:pred</ta>
            <ta e="T125" id="Seg_1857" s="T124">np.h:S</ta>
            <ta e="T126" id="Seg_1858" s="T125">v:pred</ta>
            <ta e="T127" id="Seg_1859" s="T126">0.2.h:S v:pred</ta>
            <ta e="T129" id="Seg_1860" s="T128">0.2.h:S v:pred</ta>
            <ta e="T131" id="Seg_1861" s="T130">np:O</ta>
            <ta e="T132" id="Seg_1862" s="T131">0.2.h:S v:pred</ta>
            <ta e="T134" id="Seg_1863" s="T133">pro.h:S</ta>
            <ta e="T136" id="Seg_1864" s="T135">v:pred</ta>
            <ta e="T137" id="Seg_1865" s="T136">s:purp</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T29" id="Seg_1866" s="T28">RUS:gram</ta>
            <ta e="T34" id="Seg_1867" s="T33">RUS:core</ta>
            <ta e="T36" id="Seg_1868" s="T35">RUS:gram</ta>
            <ta e="T37" id="Seg_1869" s="T36">RUS:core</ta>
            <ta e="T40" id="Seg_1870" s="T39">RUS:gram</ta>
            <ta e="T55" id="Seg_1871" s="T54">RUS:mod</ta>
            <ta e="T56" id="Seg_1872" s="T55">RUS:cult</ta>
            <ta e="T59" id="Seg_1873" s="T58">RUS:mod</ta>
            <ta e="T65" id="Seg_1874" s="T64">RUS:mod</ta>
            <ta e="T67" id="Seg_1875" s="T66">RUS:cult</ta>
            <ta e="T76" id="Seg_1876" s="T75">RUS:core</ta>
            <ta e="T79" id="Seg_1877" s="T78">RUS:core</ta>
            <ta e="T81" id="Seg_1878" s="T80">RUS:core</ta>
            <ta e="T82" id="Seg_1879" s="T81">RUS:cult</ta>
            <ta e="T83" id="Seg_1880" s="T82">RUS:core</ta>
            <ta e="T86" id="Seg_1881" s="T85">RUS:gram</ta>
            <ta e="T93" id="Seg_1882" s="T92">RUS:gram</ta>
            <ta e="T101" id="Seg_1883" s="T100">RUS:core</ta>
            <ta e="T110" id="Seg_1884" s="T109">RUS:gram</ta>
            <ta e="T121" id="Seg_1885" s="T120">RUS:core</ta>
            <ta e="T133" id="Seg_1886" s="T132">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T12" id="Seg_1887" s="T1">A father-in-law and a mother-in-law (a husband and a wife) decided to marry their son, they took a girl from an other village.</ta>
            <ta e="T14" id="Seg_1888" s="T12">They took a girl.</ta>
            <ta e="T16" id="Seg_1889" s="T14">They made their son marry.</ta>
            <ta e="T20" id="Seg_1890" s="T16">Having married, their son went far away.</ta>
            <ta e="T24" id="Seg_1891" s="T20">He left his wife with her father-in-law and her mother-in-law</ta>
            <ta e="T28" id="Seg_1892" s="T24">The father-in-law was working day and night.</ta>
            <ta e="T33" id="Seg_1893" s="T28">The mother-in-law was a hard-working woman as well.</ta>
            <ta e="T35" id="Seg_1894" s="T33">They were eager to work.</ta>
            <ta e="T39" id="Seg_1895" s="T35">And they made their daughter-in-law work.</ta>
            <ta e="T44" id="Seg_1896" s="T39">But they didn't give her enough to eat.</ta>
            <ta e="T51" id="Seg_1897" s="T44">While living with them, she didn't eat good food.</ta>
            <ta e="T55" id="Seg_1898" s="T51">The father-in-law said they should eat.</ta>
            <ta e="T65" id="Seg_1899" s="T55">They put food onto the table (one should bring food), they cut bread (one should cut bread).</ta>
            <ta e="T69" id="Seg_1900" s="T65">They ate three pieces each.</ta>
            <ta e="T72" id="Seg_1901" s="T69">The father-in-law said: “We filled up.</ta>
            <ta e="T77" id="Seg_1902" s="T72">We should take the bread and the rest of the food from the table.</ta>
            <ta e="T80" id="Seg_1903" s="T77">Now we filled up.”</ta>
            <ta e="T84" id="Seg_1904" s="T80">The daughter-in-law put away all the food.</ta>
            <ta e="T92" id="Seg_1905" s="T84">The father-in-law and the mother-in-law used to eat secretely, they ate secretely.</ta>
            <ta e="T96" id="Seg_1906" s="T92">And she used to be hungry.</ta>
            <ta e="T100" id="Seg_1907" s="T96">She worked being hungry, she wanted to eat.</ta>
            <ta e="T107" id="Seg_1908" s="T100">The daughter-in-law was hungry, she came to her mother.</ta>
            <ta e="T114" id="Seg_1909" s="T107">She said to her mother: “I am hungry.</ta>
            <ta e="T120" id="Seg_1910" s="T114">They don't give me [enough] to eat, I don't eat enough.</ta>
            <ta e="T124" id="Seg_1911" s="T120">The daughter-in-law had three brothers,</ta>
            <ta e="T132" id="Seg_1912" s="T124">One brother said: “Go home and cook some good food, cook it.</ta>
            <ta e="T137" id="Seg_1913" s="T132">And I'll come to visit you.”</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T12" id="Seg_1914" s="T1">Ein Schwiegervater und eine Schwiegermutter (Mann und Frau) entschieden, ihren Sohn zu verheiraten, sie nahmen ein Mädchen aus einem anderen Dorf.</ta>
            <ta e="T14" id="Seg_1915" s="T12">Sie nahmen ein Mädchen.</ta>
            <ta e="T16" id="Seg_1916" s="T14">Sie verheirateten ihren Sohn.</ta>
            <ta e="T20" id="Seg_1917" s="T16">Verheiratet ging der Sohn weit weg.</ta>
            <ta e="T24" id="Seg_1918" s="T20">Er ließ seine Frau bei ihrem Schwiegervater und ihrer Schwiegermutter zurück.</ta>
            <ta e="T28" id="Seg_1919" s="T24">Der Schwiegervater arbeitete Tag und Nacht.</ta>
            <ta e="T33" id="Seg_1920" s="T28">Die Schwiegermutter war ebenfalls eine hart arbeitende Frau.</ta>
            <ta e="T35" id="Seg_1921" s="T33">Sie waren sehr arbeitsfreudig.</ta>
            <ta e="T39" id="Seg_1922" s="T35">Und sie ließen ihre Schwiegertochter arbeiten.</ta>
            <ta e="T44" id="Seg_1923" s="T39">Aber sie gaben ihr nicht genug zu essen.</ta>
            <ta e="T51" id="Seg_1924" s="T44">Während sie bei ihnen lebte, aß sie kein gutes Essen.</ta>
            <ta e="T55" id="Seg_1925" s="T51">Der Schwiegervater sagte, sie sollten essen.</ta>
            <ta e="T65" id="Seg_1926" s="T55">Sie taten Essen auf den Tisch (man sollte Essen bringen), sie schnitten Brot (man sollte Brot schneiden).</ta>
            <ta e="T69" id="Seg_1927" s="T65">Jeder aß drei Scheiben.</ta>
            <ta e="T72" id="Seg_1928" s="T69">Der Schwiegervater sagte: "Wir haben genug gegessen.</ta>
            <ta e="T77" id="Seg_1929" s="T72">Wir sollten das Brot und das restliche Essen vom Tisch nehmen.</ta>
            <ta e="T80" id="Seg_1930" s="T77">Jetzt haben wir genug gegessen."</ta>
            <ta e="T84" id="Seg_1931" s="T80">Die Schwiegertochter räumte das ganze Essen weg.</ta>
            <ta e="T92" id="Seg_1932" s="T84">Der Schwiegervater und die Schwiegermutter aßen immer heimlich, sie aßen heimlich.</ta>
            <ta e="T96" id="Seg_1933" s="T92">Und sie war immer hungrig.</ta>
            <ta e="T100" id="Seg_1934" s="T96">Sie arbeitete hungrig, sie wollte essen.</ta>
            <ta e="T107" id="Seg_1935" s="T100">Die Schwiegertochter war hungrig, sie ging zu ihrer Mutter.</ta>
            <ta e="T114" id="Seg_1936" s="T107">Sie sagte ihrer Mutter: "Ich habe Hunger.</ta>
            <ta e="T120" id="Seg_1937" s="T114">Sie geben mir nicht [genug] zu essen, ich esse nicht genug.</ta>
            <ta e="T124" id="Seg_1938" s="T120">Die Schwiegertochter hatte drei Brüder.</ta>
            <ta e="T132" id="Seg_1939" s="T124">Ein Bruder sagte: "Geh nach Hause und koche dir gutes Essen, koche es.</ta>
            <ta e="T137" id="Seg_1940" s="T132">Und ich komme dich besuchen."</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T12" id="Seg_1941" s="T1">Свёкор и свекровь (муж с женой) вздумали сына женить, из другой деревни взяли [невесту].</ta>
            <ta e="T14" id="Seg_1942" s="T12">Взяли девушку.</ta>
            <ta e="T16" id="Seg_1943" s="T14">Сына женили.</ta>
            <ta e="T20" id="Seg_1944" s="T16">Сын, женившись, уехал далеко.</ta>
            <ta e="T24" id="Seg_1945" s="T20">Со свекром, со свекровью жену оставил.</ta>
            <ta e="T28" id="Seg_1946" s="T24">Свекор день и ночь работал.</ta>
            <ta e="T33" id="Seg_1947" s="T28">И свекровь тоже работящая женщина.</ta>
            <ta e="T35" id="Seg_1948" s="T33">Они сильно тянулись к работе (?).</ta>
            <ta e="T39" id="Seg_1949" s="T35">И сноху работать заставляли.</ta>
            <ta e="T44" id="Seg_1950" s="T39">А есть хорошо не давали.</ta>
            <ta e="T51" id="Seg_1951" s="T44">Хорошую пищу она не ела, с ними живя.</ta>
            <ta e="T55" id="Seg_1952" s="T51">Свекор говорит, что есть надо.</ta>
            <ta e="T65" id="Seg_1953" s="T55">На стол надо собрали поесть (собрать надо); хлеб нарезали (нарезать надо).</ta>
            <ta e="T69" id="Seg_1954" s="T65">По три куска съели.</ta>
            <ta e="T72" id="Seg_1955" s="T69">Свекор сказал: “Мы наелись.</ta>
            <ta e="T77" id="Seg_1956" s="T72">Надо унести [со стола] хлеб и остальную пищу.</ta>
            <ta e="T80" id="Seg_1957" s="T77">Мы теперь наелись”.</ta>
            <ta e="T84" id="Seg_1958" s="T80">Сноха убирала всю пищу.</ta>
            <ta e="T92" id="Seg_1959" s="T84">Свекор и свекровь ели тайком, ели сами тайком.</ta>
            <ta e="T96" id="Seg_1960" s="T92">А она голодная бывала.</ta>
            <ta e="T100" id="Seg_1961" s="T96">Работала проголадавшись, есть хотела.</ta>
            <ta e="T107" id="Seg_1962" s="T100">Сноха голодная была, к своей матери пошла.</ta>
            <ta e="T114" id="Seg_1963" s="T107">Матери говорит: “Я голодная хожу.</ta>
            <ta e="T120" id="Seg_1964" s="T114">Они есть не дают, я не наедаюсь”.</ta>
            <ta e="T124" id="Seg_1965" s="T120">У снохи было три брата.</ta>
            <ta e="T132" id="Seg_1966" s="T124">Брат говорил: “Иди домой, вари хорошую пищу, вари [повкуснее].</ta>
            <ta e="T137" id="Seg_1967" s="T132">А я к тебе приду посидеть”.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T12" id="Seg_1968" s="T1">муж с женой вздумали сына женить с другой деревни взять</ta>
            <ta e="T14" id="Seg_1969" s="T12">взяли девку</ta>
            <ta e="T16" id="Seg_1970" s="T14">сына женили</ta>
            <ta e="T20" id="Seg_1971" s="T16">сын женился уехал далеко</ta>
            <ta e="T24" id="Seg_1972" s="T20">со свекром со свекровью жену оставил</ta>
            <ta e="T28" id="Seg_1973" s="T24">свекор день и ночь работал</ta>
            <ta e="T33" id="Seg_1974" s="T28">свекровь тоже работящая была</ta>
            <ta e="T35" id="Seg_1975" s="T33">шибко жадные были на работу</ta>
            <ta e="T39" id="Seg_1976" s="T35">и сноху работать заставляли</ta>
            <ta e="T44" id="Seg_1977" s="T39">а есть хорошо не давали</ta>
            <ta e="T51" id="Seg_1978" s="T44">хорошую пищу она не поела с ними живя</ta>
            <ta e="T55" id="Seg_1979" s="T51">свекор говорит что есть надо</ta>
            <ta e="T65" id="Seg_1980" s="T55">на стол надо собрать есть хлеб нарезали</ta>
            <ta e="T69" id="Seg_1981" s="T65">по три куска съели</ta>
            <ta e="T72" id="Seg_1982" s="T69">свекор сказал что наелись</ta>
            <ta e="T77" id="Seg_1983" s="T72">прочь убрать (со стола) хлеб надо и остальную пищу</ta>
            <ta e="T80" id="Seg_1984" s="T77">мы теперь наелись</ta>
            <ta e="T84" id="Seg_1985" s="T80">сноха убирала всю пищу</ta>
            <ta e="T92" id="Seg_1986" s="T84">свекор и свекровь ели крадучи(сь) (тайком) ели сами крадучи(сь)</ta>
            <ta e="T96" id="Seg_1987" s="T92">а она голодная бывала</ta>
            <ta e="T100" id="Seg_1988" s="T96">работала и есть охота было есть хотела</ta>
            <ta e="T107" id="Seg_1989" s="T100">сноха голодная оставалась к своей матери пошла</ta>
            <ta e="T114" id="Seg_1990" s="T107">матери говорит что голодная хожу</ta>
            <ta e="T120" id="Seg_1991" s="T114">есть не дают я не наедаюсь</ta>
            <ta e="T124" id="Seg_1992" s="T120">у снохи было три брата</ta>
            <ta e="T132" id="Seg_1993" s="T124">иди домой вари хорошую пищу вари (повкуснее)</ta>
            <ta e="T137" id="Seg_1994" s="T132">я к тебе приду посидеть</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T12" id="Seg_1995" s="T1">[BrM:] This text is just the beginning of the fairy tale.</ta>
            <ta e="T20" id="Seg_1996" s="T16">[BrM:] 'net lʼeːpe' changed to 'netlʼeːpe'.</ta>
            <ta e="T28" id="Seg_1997" s="T24">[BrM:] 'pizedelǯe' changed to 'pize delǯe'.</ta>
            <ta e="T35" id="Seg_1998" s="T33">[BrM:] Tentative analysis of 'näɣalbɨdi'.</ta>
            <ta e="T69" id="Seg_1999" s="T65">[BrM:] 'lamotinʼdʼär' changed to 'lamotinʼ dʼär'.</ta>
            <ta e="T72" id="Seg_2000" s="T69">[KuAI:] Variant: 'qabɨʒaj'.</ta>
            <ta e="T124" id="Seg_2001" s="T120">[BrM:] '-dnä' as OBL.3SG?</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
