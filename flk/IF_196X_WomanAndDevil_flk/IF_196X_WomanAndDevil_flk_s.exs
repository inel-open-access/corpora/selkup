<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>IF_196X_WomanAndDevil_flk</transcription-name>
         <referenced-file url="IF_196X_WomanAndDevil_flk.wav" />
         <referenced-file url="IF_196X_WomanAndDevil_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">IF_196X_WomanAndDevil_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">259</ud-information>
            <ud-information attribute-name="# HIAT:w">150</ud-information>
            <ud-information attribute-name="# e">157</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">10</ud-information>
            <ud-information attribute-name="# HIAT:u">31</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="IF">
            <abbreviation>IF</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="1.65" type="appl" />
         <tli id="T55" time="3.575" type="intp" />
         <tli id="T1" time="5.5" type="appl" />
         <tli id="T2" time="8.345" type="appl" />
         <tli id="T3" time="9.23" type="appl" />
         <tli id="T4" time="10.115" type="appl" />
         <tli id="T5" time="11.42613403496877" />
         <tli id="T6" time="12.632" type="appl" />
         <tli id="T7" time="13.606" type="appl" />
         <tli id="T8" time="14.579" type="appl" />
         <tli id="T9" time="15.552" type="appl" />
         <tli id="T10" time="16.365" type="appl" />
         <tli id="T11" time="17.07" type="appl" />
         <tli id="T12" time="18.512470370541582" />
         <tli id="T13" time="21.252342650805392" />
         <tli id="T14" time="22.34" type="appl" />
         <tli id="T15" time="23.265" type="appl" />
         <tli id="T16" time="24.191" type="appl" />
         <tli id="T17" time="25.116" type="appl" />
         <tli id="T18" time="26.042" type="appl" />
         <tli id="T19" time="26.967" type="appl" />
         <tli id="T20" time="27.893" type="appl" />
         <tli id="T21" time="28.515" type="appl" />
         <tli id="T22" time="29.031" type="appl" />
         <tli id="T23" time="29.547" type="appl" />
         <tli id="T24" time="30.063" type="appl" />
         <tli id="T25" time="32.382" type="appl" />
         <tli id="T26" time="33.365" type="appl" />
         <tli id="T27" time="33.954" type="appl" />
         <tli id="T28" time="34.544" type="appl" />
         <tli id="T29" time="35.134" type="appl" />
         <tli id="T30" time="35.724" type="appl" />
         <tli id="T31" time="36.313" type="appl" />
         <tli id="T32" time="37.58491463777942" />
         <tli id="T33" time="38.1" type="appl" />
         <tli id="T34" time="38.68" type="appl" />
         <tli id="T35" time="39.259" type="appl" />
         <tli id="T36" time="39.839" type="appl" />
         <tli id="T37" time="40.792" type="appl" />
         <tli id="T38" time="42.83133674135026" />
         <tli id="T39" time="43.729" type="appl" />
         <tli id="T40" time="44.226" type="appl" />
         <tli id="T41" time="44.722" type="appl" />
         <tli id="T42" time="47.417789609529095" />
         <tli id="T43" time="47.964" type="appl" />
         <tli id="T44" time="48.568" type="appl" />
         <tli id="T45" time="49.171" type="appl" />
         <tli id="T46" time="49.775" type="appl" />
         <tli id="T47" time="50.378" type="appl" />
         <tli id="T48" time="50.982" type="appl" />
         <tli id="T49" time="52.53088459483893" />
         <tli id="T50" time="53.089" type="appl" />
         <tli id="T51" time="53.664" type="appl" />
         <tli id="T52" time="54.238" type="appl" />
         <tli id="T53" time="54.813" type="appl" />
         <tli id="T54" time="55.72406907719017" />
         <tli id="T56" time="56.471" type="appl" />
         <tli id="T57" time="56.846" type="appl" />
         <tli id="T58" time="57.22" type="appl" />
         <tli id="T59" time="57.595" type="appl" />
         <tli id="T60" time="58.308" type="appl" />
         <tli id="T61" time="58.703" type="appl" />
         <tli id="T62" time="59.098" type="appl" />
         <tli id="T63" time="59.494" type="appl" />
         <tli id="T64" time="59.889" type="appl" />
         <tli id="T65" time="60.284" type="appl" />
         <tli id="T66" time="62.9970633783284" />
         <tli id="T67" time="63.364" type="appl" />
         <tli id="T68" time="63.772" type="appl" />
         <tli id="T69" time="64.18" type="appl" />
         <tli id="T70" time="64.587" type="appl" />
         <tli id="T71" time="64.995" type="appl" />
         <tli id="T72" time="65.90359455641848" />
         <tli id="T73" time="66.478" type="appl" />
         <tli id="T74" time="66.979" type="appl" />
         <tli id="T75" time="67.48" type="appl" />
         <tli id="T76" time="67.981" type="appl" />
         <tli id="T77" time="68.482" type="appl" />
         <tli id="T78" time="68.984" type="appl" />
         <tli id="T79" time="69.485" type="appl" />
         <tli id="T80" time="69.986" type="appl" />
         <tli id="T81" time="70.487" type="appl" />
         <tli id="T82" time="71.91664758999015" />
         <tli id="T83" time="72.346" type="appl" />
         <tli id="T84" time="72.81" type="appl" />
         <tli id="T85" time="73.273" type="appl" />
         <tli id="T86" time="73.737" type="appl" />
         <tli id="T87" time="74.201" type="appl" />
         <tli id="T88" time="74.665" type="appl" />
         <tli id="T89" time="75.129" type="appl" />
         <tli id="T90" time="75.592" type="appl" />
         <tli id="T91" time="76.056" type="appl" />
         <tli id="T92" time="77.26973138816979" />
         <tli id="T93" time="78.344" type="appl" />
         <tli id="T94" time="79.401" type="appl" />
         <tli id="T95" time="80.459" type="appl" />
         <tli id="T96" time="81.516" type="appl" />
         <tli id="T97" time="83.45610967548076" />
         <tli id="T98" time="84.08" type="appl" />
         <tli id="T99" time="84.576" type="appl" />
         <tli id="T100" time="85.071" type="appl" />
         <tli id="T101" time="85.567" type="appl" />
         <tli id="T102" time="86.063" type="appl" />
         <tli id="T103" time="86.691" type="appl" />
         <tli id="T104" time="87.276" type="appl" />
         <tli id="T105" time="87.861" type="appl" />
         <tli id="T106" time="88.70919813496467" />
         <tli id="T107" time="89.018" type="appl" />
         <tli id="T108" time="89.283" type="appl" />
         <tli id="T109" time="89.547" type="appl" />
         <tli id="T110" time="89.811" type="appl" />
         <tli id="T111" time="90.076" type="appl" />
         <tli id="T112" time="90.96242643357576" />
         <tli id="T113" time="91.376" type="appl" />
         <tli id="T114" time="91.869" type="appl" />
         <tli id="T115" time="92.363" type="appl" />
         <tli id="T116" time="92.856" type="appl" />
         <tli id="T117" time="93.35" type="appl" />
         <tli id="T118" time="94.276" type="appl" />
         <tli id="T119" time="94.797" type="appl" />
         <tli id="T120" time="96.51" type="appl" />
         <tli id="T121" time="97.691" type="appl" />
         <tli id="T122" time="99.597" type="appl" />
         <tli id="T123" time="100.024" type="appl" />
         <tli id="T124" time="100.452" type="appl" />
         <tli id="T125" time="100.879" type="appl" />
         <tli id="T126" time="101.306" type="appl" />
         <tli id="T127" time="101.733" type="appl" />
         <tli id="T128" time="102.37" type="appl" />
         <tli id="T129" time="102.859" type="appl" />
         <tli id="T130" time="103.347" type="appl" />
         <tli id="T131" time="103.835" type="appl" />
         <tli id="T132" time="104.324" type="appl" />
         <tli id="T133" time="104.812" type="appl" />
         <tli id="T134" time="105.3" type="appl" />
         <tli id="T135" time="105.788" type="appl" />
         <tli id="T136" time="106.277" type="appl" />
         <tli id="T137" time="106.765" type="appl" />
         <tli id="T138" time="108.773" type="appl" />
         <tli id="T139" time="109.398" type="appl" />
         <tli id="T140" time="110.023" type="appl" />
         <tli id="T141" time="110.648" type="appl" />
         <tli id="T142" time="111.327" type="appl" />
         <tli id="T143" time="111.847" type="appl" />
         <tli id="T144" time="112.367" type="appl" />
         <tli id="T145" time="112.887" type="appl" />
         <tli id="T146" time="113.408" type="appl" />
         <tli id="T147" time="113.928" type="appl" />
         <tli id="T148" time="114.448" type="appl" />
         <tli id="T149" time="114.968" type="appl" />
         <tli id="T150" time="115.488" type="appl" />
         <tli id="T151" time="116.184" type="appl" />
         <tli id="T152" time="116.646" type="appl" />
         <tli id="T153" time="117.108" type="appl" />
         <tli id="T154" time="117.571" type="appl" />
         <tli id="T155" time="118.033" type="appl" />
         <tli id="T156" time="118.495" type="appl" />
         <tli id="T157" time="118.957" type="appl" />
         <tli id="T158" time="121.674" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="IF"
                      type="t">
         <timeline-fork end="T55" start="T0">
            <tli id="T0.tx.1" />
            <tli id="T0.tx.2" />
            <tli id="T0.tx.3" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T157" id="Seg_0" n="sc" s="T0">
               <ts e="T55" id="Seg_2" n="HIAT:u" s="T0">
                  <nts id="Seg_3" n="HIAT:ip">(</nts>
                  <nts id="Seg_4" n="HIAT:ip">(</nts>
                  <ats e="T0.tx.1" id="Seg_5" n="HIAT:non-pho" s="T0">KuAI:</ats>
                  <nts id="Seg_6" n="HIAT:ip">)</nts>
                  <nts id="Seg_7" n="HIAT:ip">)</nts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx.2" id="Seg_10" n="HIAT:w" s="T0.tx.1">Фрося</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx.3" id="Seg_13" n="HIAT:w" s="T0.tx.2">Ирикова</ts>
                  <nts id="Seg_14" n="HIAT:ip">,</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_17" n="HIAT:w" s="T0.tx.3">сказка</ts>
                  <nts id="Seg_18" n="HIAT:ip">.</nts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T5" id="Seg_21" n="HIAT:u" s="T55">
                  <nts id="Seg_22" n="HIAT:ip">(</nts>
                  <nts id="Seg_23" n="HIAT:ip">(</nts>
                  <ats e="T1" id="Seg_24" n="HIAT:non-pho" s="T55">IF:</ats>
                  <nts id="Seg_25" n="HIAT:ip">)</nts>
                  <nts id="Seg_26" n="HIAT:ip">)</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_29" n="HIAT:w" s="T1">Šittɨ</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_32" n="HIAT:w" s="T2">irra</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_35" n="HIAT:w" s="T3">imasɨqəj</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_38" n="HIAT:w" s="T4">ilɨmɨntɔːqəj</ts>
                  <nts id="Seg_39" n="HIAT:ip">.</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_42" n="HIAT:u" s="T5">
                  <nts id="Seg_43" n="HIAT:ip">(</nts>
                  <ts e="T6" id="Seg_45" n="HIAT:w" s="T5">Irraːtɨ=</ts>
                  <nts id="Seg_46" n="HIAT:ip">)</nts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_49" n="HIAT:w" s="T6">Irraiːtɨ</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_52" n="HIAT:w" s="T7">šöttɨ</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_55" n="HIAT:w" s="T8">qənpɔːqəj</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_59" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_61" n="HIAT:w" s="T9">Imaqəj</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_64" n="HIAT:w" s="T10">moqɨnɨ</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_67" n="HIAT:w" s="T11">qalɨmpɔːqəj</ts>
                  <nts id="Seg_68" n="HIAT:ip">.</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_71" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_73" n="HIAT:w" s="T12">Lɨpqɨmɔːtajan</ts>
                  <nts id="Seg_74" n="HIAT:ip">.</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_77" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_79" n="HIAT:w" s="T13">Ukkɨr</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_82" n="HIAT:w" s="T14">ima</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_85" n="HIAT:w" s="T15">ijajantɨsa</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_88" n="HIAT:w" s="T16">namɨššak</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_91" n="HIAT:w" s="T17">sɔːntɨrna</ts>
                  <nts id="Seg_92" n="HIAT:ip">,</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_95" n="HIAT:w" s="T18">namɨššak</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_98" n="HIAT:w" s="T19">sɔːntɨrna</ts>
                  <nts id="Seg_99" n="HIAT:ip">.</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_102" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_104" n="HIAT:w" s="T20">Toːna</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_107" n="HIAT:w" s="T21">ima</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_110" n="HIAT:w" s="T22">nık</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_113" n="HIAT:w" s="T23">kətɨŋɨtɨ</ts>
                  <nts id="Seg_114" n="HIAT:ip">.</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_117" n="HIAT:u" s="T24">
                  <nts id="Seg_118" n="HIAT:ip">(</nts>
                  <nts id="Seg_119" n="HIAT:ip">(</nts>
                  <ats e="T25" id="Seg_120" n="HIAT:non-pho" s="T24">…</ats>
                  <nts id="Seg_121" n="HIAT:ip">)</nts>
                  <nts id="Seg_122" n="HIAT:ip">)</nts>
                  <nts id="Seg_123" n="HIAT:ip">.</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_126" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_128" n="HIAT:w" s="T25">Uže</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_131" n="HIAT:w" s="T26">lɨpqɨmɔːtpa</ts>
                  <nts id="Seg_132" n="HIAT:ip">,</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_135" n="HIAT:w" s="T27">me</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_138" n="HIAT:w" s="T28">oš</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_141" n="HIAT:w" s="T29">onɨt</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_143" n="HIAT:ip">(</nts>
                  <ts e="T31" id="Seg_145" n="HIAT:w" s="T30">ontɨn</ts>
                  <nts id="Seg_146" n="HIAT:ip">)</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_149" n="HIAT:w" s="T31">pɛlɨkɔːlɛːŋɔːmɨn</ts>
                  <nts id="Seg_150" n="HIAT:ip">.</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_153" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_155" n="HIAT:w" s="T32">Təm</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_158" n="HIAT:w" s="T33">ima</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_161" n="HIAT:w" s="T34">ašša</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_164" n="HIAT:w" s="T35">üŋkɨltɨmpat</ts>
                  <nts id="Seg_165" n="HIAT:ip">.</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_168" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_170" n="HIAT:w" s="T36">Melʼtɨ</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_173" n="HIAT:w" s="T37">üčʼalɨmpa</ts>
                  <nts id="Seg_174" n="HIAT:ip">.</nts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_177" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_179" n="HIAT:w" s="T38">Ima</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_182" n="HIAT:w" s="T39">aj</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_185" n="HIAT:w" s="T40">kətɨɣɨtɨ</ts>
                  <nts id="Seg_186" n="HIAT:ip">.</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_189" n="HIAT:u" s="T41">
                  <nts id="Seg_190" n="HIAT:ip">“</nts>
                  <nts id="Seg_191" n="HIAT:ip">(</nts>
                  <ts e="T42" id="Seg_193" n="HIAT:w" s="T41">Čʼarɨqalɔːqɨ</ts>
                  <nts id="Seg_194" n="HIAT:ip">)</nts>
                  <nts id="Seg_195" n="HIAT:ip">”</nts>
                  <nts id="Seg_196" n="HIAT:ip">.</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_199" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_201" n="HIAT:w" s="T42">Mɨtta</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_203" n="HIAT:ip">(</nts>
                  <ts e="T44" id="Seg_205" n="HIAT:w" s="T43">ontɨ=</ts>
                  <nts id="Seg_206" n="HIAT:ip">)</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_208" n="HIAT:ip">(</nts>
                  <nts id="Seg_209" n="HIAT:ip">(</nts>
                  <ats e="T45" id="Seg_210" n="HIAT:non-pho" s="T44">…</ats>
                  <nts id="Seg_211" n="HIAT:ip">)</nts>
                  <nts id="Seg_212" n="HIAT:ip">)</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_215" n="HIAT:w" s="T45">ontɨ</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_218" n="HIAT:w" s="T46">ijamtɨ</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_221" n="HIAT:w" s="T47">kurraintɨ</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_224" n="HIAT:w" s="T48">orɨčʼčʼɨtɨ</ts>
                  <nts id="Seg_225" n="HIAT:ip">.</nts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_228" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_230" n="HIAT:w" s="T49">Ukkɨr</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_233" n="HIAT:w" s="T50">čʼontoːqɨn</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_236" n="HIAT:w" s="T51">mɔːta</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_239" n="HIAT:w" s="T52">qotta</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_242" n="HIAT:w" s="T53">alʼčʼɨlä</ts>
                  <nts id="Seg_243" n="HIAT:ip">,</nts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_246" n="HIAT:w" s="T54">loːsɨ</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_249" n="HIAT:w" s="T56">irra</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_252" n="HIAT:w" s="T57">šerra</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_255" n="HIAT:w" s="T58">ılla</ts>
                  <nts id="Seg_256" n="HIAT:ip">.</nts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_259" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_261" n="HIAT:w" s="T59">Təttantɨ</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_264" n="HIAT:w" s="T60">illa</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_267" n="HIAT:w" s="T61">omtɨ</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_270" n="HIAT:w" s="T62">illa</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_273" n="HIAT:w" s="T63">topɨti</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_276" n="HIAT:w" s="T64">šittɨ</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_279" n="HIAT:w" s="T65">čʼattɨmpɨla</ts>
                  <nts id="Seg_280" n="HIAT:ip">.</nts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_283" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_285" n="HIAT:w" s="T66">Ukkɨr</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_288" n="HIAT:w" s="T67">ima</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_291" n="HIAT:w" s="T68">toːna</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_294" n="HIAT:w" s="T69">ima</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_297" n="HIAT:w" s="T70">nık</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_300" n="HIAT:w" s="T71">kətɨŋɨtɨ</ts>
                  <nts id="Seg_301" n="HIAT:ip">.</nts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_304" n="HIAT:u" s="T72">
                  <nts id="Seg_305" n="HIAT:ip">“</nts>
                  <ts e="T73" id="Seg_307" n="HIAT:w" s="T72">Qəə</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_310" n="HIAT:w" s="T73">ilʼčʼa</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_313" n="HIAT:w" s="T74">topal</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_316" n="HIAT:w" s="T75">toː</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_319" n="HIAT:w" s="T76">loːqɨ</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_322" n="HIAT:w" s="T77">mat</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_325" n="HIAT:w" s="T78">kətsan</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_328" n="HIAT:w" s="T79">tukɨrɨm</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_331" n="HIAT:w" s="T80">pona</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_334" n="HIAT:w" s="T81">tattɨllam</ts>
                  <nts id="Seg_335" n="HIAT:ip">”</nts>
                  <nts id="Seg_336" n="HIAT:ip">.</nts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_339" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_341" n="HIAT:w" s="T82">Na</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_344" n="HIAT:w" s="T83">ima</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_346" n="HIAT:ip">(</nts>
                  <nts id="Seg_347" n="HIAT:ip">(</nts>
                  <ats e="T85" id="Seg_348" n="HIAT:non-pho" s="T84">…</ats>
                  <nts id="Seg_349" n="HIAT:ip">)</nts>
                  <nts id="Seg_350" n="HIAT:ip">)</nts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_353" n="HIAT:w" s="T85">ija</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_356" n="HIAT:w" s="T86">neːjamtɨ</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_359" n="HIAT:w" s="T87">keːraqɨntɨ</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_361" n="HIAT:ip">(</nts>
                  <nts id="Seg_362" n="HIAT:ip">(</nts>
                  <ats e="T89" id="Seg_363" n="HIAT:non-pho" s="T88">…</ats>
                  <nts id="Seg_364" n="HIAT:ip">)</nts>
                  <nts id="Seg_365" n="HIAT:ip">)</nts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_368" n="HIAT:w" s="T89">pona</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_371" n="HIAT:w" s="T90">toː</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_374" n="HIAT:w" s="T91">sačʼčʼɨmɔːtpa</ts>
                  <nts id="Seg_375" n="HIAT:ip">.</nts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_378" n="HIAT:u" s="T92">
                  <nts id="Seg_379" n="HIAT:ip">(</nts>
                  <nts id="Seg_380" n="HIAT:ip">(</nts>
                  <ats e="T93" id="Seg_381" n="HIAT:non-pho" s="T92">…</ats>
                  <nts id="Seg_382" n="HIAT:ip">)</nts>
                  <nts id="Seg_383" n="HIAT:ip">)</nts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_386" n="HIAT:w" s="T93">Sačʼčʼɨmɔːlla</ts>
                  <nts id="Seg_387" n="HIAT:ip">,</nts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_390" n="HIAT:w" s="T94">qälɨporɨ</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_393" n="HIAT:w" s="T95">qənpa</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_396" n="HIAT:w" s="T96">ɨraiːqɨntɨ</ts>
                  <nts id="Seg_397" n="HIAT:ip">.</nts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_400" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_402" n="HIAT:w" s="T97">İraiːqɨntɨ</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_405" n="HIAT:w" s="T98">tülä</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_407" n="HIAT:ip">(</nts>
                  <ts e="T100" id="Seg_409" n="HIAT:w" s="T99">ılla</ts>
                  <nts id="Seg_410" n="HIAT:ip">)</nts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_413" n="HIAT:w" s="T100">nık</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_416" n="HIAT:w" s="T101">kətɨŋɨtɨ</ts>
                  <nts id="Seg_417" n="HIAT:ip">.</nts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_420" n="HIAT:u" s="T102">
                  <nts id="Seg_421" n="HIAT:ip">“</nts>
                  <nts id="Seg_422" n="HIAT:ip">(</nts>
                  <nts id="Seg_423" n="HIAT:ip">(</nts>
                  <ats e="T103" id="Seg_424" n="HIAT:non-pho" s="T102">…</ats>
                  <nts id="Seg_425" n="HIAT:ip">)</nts>
                  <nts id="Seg_426" n="HIAT:ip">)</nts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_429" n="HIAT:w" s="T103">Loːsɨ</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_432" n="HIAT:w" s="T104">ira</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_435" n="HIAT:w" s="T105">tüːsa</ts>
                  <nts id="Seg_436" n="HIAT:ip">.</nts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_439" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_441" n="HIAT:w" s="T106">Qaj</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_444" n="HIAT:w" s="T107">imal</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_447" n="HIAT:w" s="T108">tɛp</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_450" n="HIAT:w" s="T109">qaj</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_453" n="HIAT:w" s="T110">ınna</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_456" n="HIAT:w" s="T111">amnɨt</ts>
                  <nts id="Seg_457" n="HIAT:ip">”</nts>
                  <nts id="Seg_458" n="HIAT:ip">.</nts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T117" id="Seg_461" n="HIAT:u" s="T112">
                  <ts e="T113" id="Seg_463" n="HIAT:w" s="T112">Toːna</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_466" n="HIAT:w" s="T113">iman</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_469" n="HIAT:w" s="T114">ira</ts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_472" n="HIAT:w" s="T115">ašša</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_475" n="HIAT:w" s="T116">əːtɨmpat</ts>
                  <nts id="Seg_476" n="HIAT:ip">.</nts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_479" n="HIAT:u" s="T117">
                  <nts id="Seg_480" n="HIAT:ip">“</nts>
                  <ts e="T118" id="Seg_482" n="HIAT:w" s="T117">Qajqo</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_484" n="HIAT:ip">(</nts>
                  <nts id="Seg_485" n="HIAT:ip">(</nts>
                  <ats e="T119" id="Seg_486" n="HIAT:non-pho" s="T118">…</ats>
                  <nts id="Seg_487" n="HIAT:ip">)</nts>
                  <nts id="Seg_488" n="HIAT:ip">)</nts>
                  <nts id="Seg_489" n="HIAT:ip">?</nts>
                  <nts id="Seg_490" n="HIAT:ip">”</nts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T121" id="Seg_493" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_495" n="HIAT:w" s="T119">Sukulʼteːla</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_498" n="HIAT:w" s="T120">tüːqolamnɔːtɨn</ts>
                  <nts id="Seg_499" n="HIAT:ip">.</nts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_502" n="HIAT:u" s="T121">
                  <ts e="T122" id="Seg_504" n="HIAT:w" s="T121">Pɨːpa</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_507" n="HIAT:w" s="T122">ɨra</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_510" n="HIAT:w" s="T123">na</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_513" n="HIAT:w" s="T124">inna</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_516" n="HIAT:w" s="T125">qaj</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_519" n="HIAT:w" s="T126">amnɨt</ts>
                  <nts id="Seg_520" n="HIAT:ip">.</nts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T137" id="Seg_523" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_525" n="HIAT:w" s="T127">Ijaiːntɨ</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_528" n="HIAT:w" s="T128">aj</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_530" n="HIAT:ip">(</nts>
                  <ts e="T130" id="Seg_532" n="HIAT:w" s="T129">ima</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_535" n="HIAT:w" s="T130">ima=</ts>
                  <nts id="Seg_536" n="HIAT:ip">)</nts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_539" n="HIAT:w" s="T131">imantɨ</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_542" n="HIAT:w" s="T132">olɨ</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_544" n="HIAT:ip">(</nts>
                  <nts id="Seg_545" n="HIAT:ip">(</nts>
                  <ats e="T134" id="Seg_546" n="HIAT:non-pho" s="T133">…</ats>
                  <nts id="Seg_547" n="HIAT:ip">)</nts>
                  <nts id="Seg_548" n="HIAT:ip">)</nts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_551" n="HIAT:w" s="T134">pontɨ</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_554" n="HIAT:w" s="T135">ınna</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_557" n="HIAT:w" s="T136">tokkaltɨmpat</ts>
                  <nts id="Seg_558" n="HIAT:ip">.</nts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T141" id="Seg_561" n="HIAT:u" s="T137">
                  <ts e="T138" id="Seg_563" n="HIAT:w" s="T137">Tüntɨrorɨnʼnʼɔːtɨn</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_566" n="HIAT:w" s="T138">ira</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_569" n="HIAT:w" s="T139">nık</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_572" n="HIAT:w" s="T140">čʼıːŋɨta</ts>
                  <nts id="Seg_573" n="HIAT:ip">.</nts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T150" id="Seg_576" n="HIAT:u" s="T141">
                  <nts id="Seg_577" n="HIAT:ip">“</nts>
                  <ts e="T142" id="Seg_579" n="HIAT:w" s="T141">Ta</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_582" n="HIAT:w" s="T142">tan</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_585" n="HIAT:w" s="T143">moːltɨsantɨ</ts>
                  <nts id="Seg_586" n="HIAT:ip">,</nts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_589" n="HIAT:w" s="T144">imam</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_592" n="HIAT:w" s="T145">aj</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_595" n="HIAT:w" s="T146">ijaiːm</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_598" n="HIAT:w" s="T147">mašım</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_601" n="HIAT:w" s="T148">tap</ts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_604" n="HIAT:w" s="T149">čʼəːtantɨrorɨnʼnʼɔːtɨn</ts>
                  <nts id="Seg_605" n="HIAT:ip">”</nts>
                  <nts id="Seg_606" n="HIAT:ip">.</nts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_609" n="HIAT:u" s="T150">
                  <ts e="T151" id="Seg_611" n="HIAT:w" s="T150">Tap</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_614" n="HIAT:w" s="T151">kəštɨ</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_617" n="HIAT:w" s="T152">čʼap</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_620" n="HIAT:w" s="T153">tüːŋɔːtɨn</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_623" n="HIAT:w" s="T154">naš</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_626" n="HIAT:w" s="T155">kural</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_629" n="HIAT:w" s="T156">iːtɨ</ts>
                  <nts id="Seg_630" n="HIAT:ip">.</nts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T157" id="Seg_632" n="sc" s="T0">
               <ts e="T55" id="Seg_634" n="e" s="T0">((KuAI:)) Фрося Ирикова, сказка. </ts>
               <ts e="T1" id="Seg_636" n="e" s="T55">((IF:)) </ts>
               <ts e="T2" id="Seg_638" n="e" s="T1">Šittɨ </ts>
               <ts e="T3" id="Seg_640" n="e" s="T2">irra </ts>
               <ts e="T4" id="Seg_642" n="e" s="T3">imasɨqəj </ts>
               <ts e="T5" id="Seg_644" n="e" s="T4">ilɨmɨntɔːqəj. </ts>
               <ts e="T6" id="Seg_646" n="e" s="T5">(Irraːtɨ=) </ts>
               <ts e="T7" id="Seg_648" n="e" s="T6">Irraiːtɨ </ts>
               <ts e="T8" id="Seg_650" n="e" s="T7">šöttɨ </ts>
               <ts e="T9" id="Seg_652" n="e" s="T8">qənpɔːqəj. </ts>
               <ts e="T10" id="Seg_654" n="e" s="T9">Imaqəj </ts>
               <ts e="T11" id="Seg_656" n="e" s="T10">moqɨnɨ </ts>
               <ts e="T12" id="Seg_658" n="e" s="T11">qalɨmpɔːqəj. </ts>
               <ts e="T13" id="Seg_660" n="e" s="T12">Lɨpqɨmɔːtajan. </ts>
               <ts e="T14" id="Seg_662" n="e" s="T13">Ukkɨr </ts>
               <ts e="T15" id="Seg_664" n="e" s="T14">ima </ts>
               <ts e="T16" id="Seg_666" n="e" s="T15">ijajantɨsa </ts>
               <ts e="T17" id="Seg_668" n="e" s="T16">namɨššak </ts>
               <ts e="T18" id="Seg_670" n="e" s="T17">sɔːntɨrna, </ts>
               <ts e="T19" id="Seg_672" n="e" s="T18">namɨššak </ts>
               <ts e="T20" id="Seg_674" n="e" s="T19">sɔːntɨrna. </ts>
               <ts e="T21" id="Seg_676" n="e" s="T20">Toːna </ts>
               <ts e="T22" id="Seg_678" n="e" s="T21">ima </ts>
               <ts e="T23" id="Seg_680" n="e" s="T22">nık </ts>
               <ts e="T24" id="Seg_682" n="e" s="T23">kətɨŋɨtɨ. </ts>
               <ts e="T25" id="Seg_684" n="e" s="T24">((…)). </ts>
               <ts e="T26" id="Seg_686" n="e" s="T25">Uže </ts>
               <ts e="T27" id="Seg_688" n="e" s="T26">lɨpqɨmɔːtpa, </ts>
               <ts e="T28" id="Seg_690" n="e" s="T27">me </ts>
               <ts e="T29" id="Seg_692" n="e" s="T28">oš </ts>
               <ts e="T30" id="Seg_694" n="e" s="T29">onɨt </ts>
               <ts e="T31" id="Seg_696" n="e" s="T30">(ontɨn) </ts>
               <ts e="T32" id="Seg_698" n="e" s="T31">pɛlɨkɔːlɛːŋɔːmɨn. </ts>
               <ts e="T33" id="Seg_700" n="e" s="T32">Təm </ts>
               <ts e="T34" id="Seg_702" n="e" s="T33">ima </ts>
               <ts e="T35" id="Seg_704" n="e" s="T34">ašša </ts>
               <ts e="T36" id="Seg_706" n="e" s="T35">üŋkɨltɨmpat. </ts>
               <ts e="T37" id="Seg_708" n="e" s="T36">Melʼtɨ </ts>
               <ts e="T38" id="Seg_710" n="e" s="T37">üčʼalɨmpa. </ts>
               <ts e="T39" id="Seg_712" n="e" s="T38">Ima </ts>
               <ts e="T40" id="Seg_714" n="e" s="T39">aj </ts>
               <ts e="T41" id="Seg_716" n="e" s="T40">kətɨɣɨtɨ. </ts>
               <ts e="T42" id="Seg_718" n="e" s="T41">“(Čʼarɨqalɔːqɨ)”. </ts>
               <ts e="T43" id="Seg_720" n="e" s="T42">Mɨtta </ts>
               <ts e="T44" id="Seg_722" n="e" s="T43">(ontɨ=) </ts>
               <ts e="T45" id="Seg_724" n="e" s="T44">((…)) </ts>
               <ts e="T46" id="Seg_726" n="e" s="T45">ontɨ </ts>
               <ts e="T47" id="Seg_728" n="e" s="T46">ijamtɨ </ts>
               <ts e="T48" id="Seg_730" n="e" s="T47">kurraintɨ </ts>
               <ts e="T49" id="Seg_732" n="e" s="T48">orɨčʼčʼɨtɨ. </ts>
               <ts e="T50" id="Seg_734" n="e" s="T49">Ukkɨr </ts>
               <ts e="T51" id="Seg_736" n="e" s="T50">čʼontoːqɨn </ts>
               <ts e="T52" id="Seg_738" n="e" s="T51">mɔːta </ts>
               <ts e="T53" id="Seg_740" n="e" s="T52">qotta </ts>
               <ts e="T54" id="Seg_742" n="e" s="T53">alʼčʼɨlä, </ts>
               <ts e="T56" id="Seg_744" n="e" s="T54">loːsɨ </ts>
               <ts e="T57" id="Seg_746" n="e" s="T56">irra </ts>
               <ts e="T58" id="Seg_748" n="e" s="T57">šerra </ts>
               <ts e="T59" id="Seg_750" n="e" s="T58">ılla. </ts>
               <ts e="T60" id="Seg_752" n="e" s="T59">Təttantɨ </ts>
               <ts e="T61" id="Seg_754" n="e" s="T60">illa </ts>
               <ts e="T62" id="Seg_756" n="e" s="T61">omtɨ </ts>
               <ts e="T63" id="Seg_758" n="e" s="T62">illa </ts>
               <ts e="T64" id="Seg_760" n="e" s="T63">topɨti </ts>
               <ts e="T65" id="Seg_762" n="e" s="T64">šittɨ </ts>
               <ts e="T66" id="Seg_764" n="e" s="T65">čʼattɨmpɨla. </ts>
               <ts e="T67" id="Seg_766" n="e" s="T66">Ukkɨr </ts>
               <ts e="T68" id="Seg_768" n="e" s="T67">ima </ts>
               <ts e="T69" id="Seg_770" n="e" s="T68">toːna </ts>
               <ts e="T70" id="Seg_772" n="e" s="T69">ima </ts>
               <ts e="T71" id="Seg_774" n="e" s="T70">nık </ts>
               <ts e="T72" id="Seg_776" n="e" s="T71">kətɨŋɨtɨ. </ts>
               <ts e="T73" id="Seg_778" n="e" s="T72">“Qəə </ts>
               <ts e="T74" id="Seg_780" n="e" s="T73">ilʼčʼa </ts>
               <ts e="T75" id="Seg_782" n="e" s="T74">topal </ts>
               <ts e="T76" id="Seg_784" n="e" s="T75">toː </ts>
               <ts e="T77" id="Seg_786" n="e" s="T76">loːqɨ </ts>
               <ts e="T78" id="Seg_788" n="e" s="T77">mat </ts>
               <ts e="T79" id="Seg_790" n="e" s="T78">kətsan </ts>
               <ts e="T80" id="Seg_792" n="e" s="T79">tukɨrɨm </ts>
               <ts e="T81" id="Seg_794" n="e" s="T80">pona </ts>
               <ts e="T82" id="Seg_796" n="e" s="T81">tattɨllam”. </ts>
               <ts e="T83" id="Seg_798" n="e" s="T82">Na </ts>
               <ts e="T84" id="Seg_800" n="e" s="T83">ima </ts>
               <ts e="T85" id="Seg_802" n="e" s="T84">((…)) </ts>
               <ts e="T86" id="Seg_804" n="e" s="T85">ija </ts>
               <ts e="T87" id="Seg_806" n="e" s="T86">neːjamtɨ </ts>
               <ts e="T88" id="Seg_808" n="e" s="T87">keːraqɨntɨ </ts>
               <ts e="T89" id="Seg_810" n="e" s="T88">((…)) </ts>
               <ts e="T90" id="Seg_812" n="e" s="T89">pona </ts>
               <ts e="T91" id="Seg_814" n="e" s="T90">toː </ts>
               <ts e="T92" id="Seg_816" n="e" s="T91">sačʼčʼɨmɔːtpa. </ts>
               <ts e="T93" id="Seg_818" n="e" s="T92">((…)) </ts>
               <ts e="T94" id="Seg_820" n="e" s="T93">Sačʼčʼɨmɔːlla, </ts>
               <ts e="T95" id="Seg_822" n="e" s="T94">qälɨporɨ </ts>
               <ts e="T96" id="Seg_824" n="e" s="T95">qənpa </ts>
               <ts e="T97" id="Seg_826" n="e" s="T96">ɨraiːqɨntɨ. </ts>
               <ts e="T98" id="Seg_828" n="e" s="T97">İraiːqɨntɨ </ts>
               <ts e="T99" id="Seg_830" n="e" s="T98">tülä </ts>
               <ts e="T100" id="Seg_832" n="e" s="T99">(ılla) </ts>
               <ts e="T101" id="Seg_834" n="e" s="T100">nık </ts>
               <ts e="T102" id="Seg_836" n="e" s="T101">kətɨŋɨtɨ. </ts>
               <ts e="T103" id="Seg_838" n="e" s="T102">“((…)) </ts>
               <ts e="T104" id="Seg_840" n="e" s="T103">Loːsɨ </ts>
               <ts e="T105" id="Seg_842" n="e" s="T104">ira </ts>
               <ts e="T106" id="Seg_844" n="e" s="T105">tüːsa. </ts>
               <ts e="T107" id="Seg_846" n="e" s="T106">Qaj </ts>
               <ts e="T108" id="Seg_848" n="e" s="T107">imal </ts>
               <ts e="T109" id="Seg_850" n="e" s="T108">tɛp </ts>
               <ts e="T110" id="Seg_852" n="e" s="T109">qaj </ts>
               <ts e="T111" id="Seg_854" n="e" s="T110">ınna </ts>
               <ts e="T112" id="Seg_856" n="e" s="T111">amnɨt”. </ts>
               <ts e="T113" id="Seg_858" n="e" s="T112">Toːna </ts>
               <ts e="T114" id="Seg_860" n="e" s="T113">iman </ts>
               <ts e="T115" id="Seg_862" n="e" s="T114">ira </ts>
               <ts e="T116" id="Seg_864" n="e" s="T115">ašša </ts>
               <ts e="T117" id="Seg_866" n="e" s="T116">əːtɨmpat. </ts>
               <ts e="T118" id="Seg_868" n="e" s="T117">“Qajqo </ts>
               <ts e="T119" id="Seg_870" n="e" s="T118">((…))?” </ts>
               <ts e="T120" id="Seg_872" n="e" s="T119">Sukulʼteːla </ts>
               <ts e="T121" id="Seg_874" n="e" s="T120">tüːqolamnɔːtɨn. </ts>
               <ts e="T122" id="Seg_876" n="e" s="T121">Pɨːpa </ts>
               <ts e="T123" id="Seg_878" n="e" s="T122">ɨra </ts>
               <ts e="T124" id="Seg_880" n="e" s="T123">na </ts>
               <ts e="T125" id="Seg_882" n="e" s="T124">inna </ts>
               <ts e="T126" id="Seg_884" n="e" s="T125">qaj </ts>
               <ts e="T127" id="Seg_886" n="e" s="T126">amnɨt. </ts>
               <ts e="T128" id="Seg_888" n="e" s="T127">Ijaiːntɨ </ts>
               <ts e="T129" id="Seg_890" n="e" s="T128">aj </ts>
               <ts e="T130" id="Seg_892" n="e" s="T129">(ima </ts>
               <ts e="T131" id="Seg_894" n="e" s="T130">ima=) </ts>
               <ts e="T132" id="Seg_896" n="e" s="T131">imantɨ </ts>
               <ts e="T133" id="Seg_898" n="e" s="T132">olɨ </ts>
               <ts e="T134" id="Seg_900" n="e" s="T133">((…)) </ts>
               <ts e="T135" id="Seg_902" n="e" s="T134">pontɨ </ts>
               <ts e="T136" id="Seg_904" n="e" s="T135">ınna </ts>
               <ts e="T137" id="Seg_906" n="e" s="T136">tokkaltɨmpat. </ts>
               <ts e="T138" id="Seg_908" n="e" s="T137">Tüntɨrorɨnʼnʼɔːtɨn </ts>
               <ts e="T139" id="Seg_910" n="e" s="T138">ira </ts>
               <ts e="T140" id="Seg_912" n="e" s="T139">nık </ts>
               <ts e="T141" id="Seg_914" n="e" s="T140">čʼıːŋɨta. </ts>
               <ts e="T142" id="Seg_916" n="e" s="T141">“Ta </ts>
               <ts e="T143" id="Seg_918" n="e" s="T142">tan </ts>
               <ts e="T144" id="Seg_920" n="e" s="T143">moːltɨsantɨ, </ts>
               <ts e="T145" id="Seg_922" n="e" s="T144">imam </ts>
               <ts e="T146" id="Seg_924" n="e" s="T145">aj </ts>
               <ts e="T147" id="Seg_926" n="e" s="T146">ijaiːm </ts>
               <ts e="T148" id="Seg_928" n="e" s="T147">mašım </ts>
               <ts e="T149" id="Seg_930" n="e" s="T148">tap </ts>
               <ts e="T150" id="Seg_932" n="e" s="T149">čʼəːtantɨrorɨnʼnʼɔːtɨn”. </ts>
               <ts e="T151" id="Seg_934" n="e" s="T150">Tap </ts>
               <ts e="T152" id="Seg_936" n="e" s="T151">kəštɨ </ts>
               <ts e="T153" id="Seg_938" n="e" s="T152">čʼap </ts>
               <ts e="T154" id="Seg_940" n="e" s="T153">tüːŋɔːtɨn </ts>
               <ts e="T155" id="Seg_942" n="e" s="T154">naš </ts>
               <ts e="T156" id="Seg_944" n="e" s="T155">kural </ts>
               <ts e="T157" id="Seg_946" n="e" s="T156">iːtɨ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T55" id="Seg_947" s="T0">IF_196X_WomanAndDevil_flk.001 (001)</ta>
            <ta e="T5" id="Seg_948" s="T55">IF_196X_WomanAndDevil_flk.002 (002)</ta>
            <ta e="T9" id="Seg_949" s="T5">IF_196X_WomanAndDevil_flk.003 (003)</ta>
            <ta e="T12" id="Seg_950" s="T9">IF_196X_WomanAndDevil_flk.004 (004)</ta>
            <ta e="T13" id="Seg_951" s="T12">IF_196X_WomanAndDevil_flk.005 (005)</ta>
            <ta e="T20" id="Seg_952" s="T13">IF_196X_WomanAndDevil_flk.006 (006)</ta>
            <ta e="T24" id="Seg_953" s="T20">IF_196X_WomanAndDevil_flk.007 (007)</ta>
            <ta e="T25" id="Seg_954" s="T24">IF_196X_WomanAndDevil_flk.008 (008)</ta>
            <ta e="T32" id="Seg_955" s="T25">IF_196X_WomanAndDevil_flk.009 (009)</ta>
            <ta e="T36" id="Seg_956" s="T32">IF_196X_WomanAndDevil_flk.010 (010)</ta>
            <ta e="T38" id="Seg_957" s="T36">IF_196X_WomanAndDevil_flk.011 (011)</ta>
            <ta e="T41" id="Seg_958" s="T38">IF_196X_WomanAndDevil_flk.012 (012)</ta>
            <ta e="T42" id="Seg_959" s="T41">IF_196X_WomanAndDevil_flk.013 (013)</ta>
            <ta e="T49" id="Seg_960" s="T42">IF_196X_WomanAndDevil_flk.014 (014)</ta>
            <ta e="T59" id="Seg_961" s="T49">IF_196X_WomanAndDevil_flk.015 (015)</ta>
            <ta e="T66" id="Seg_962" s="T59">IF_196X_WomanAndDevil_flk.016 (017)</ta>
            <ta e="T72" id="Seg_963" s="T66">IF_196X_WomanAndDevil_flk.017 (018)</ta>
            <ta e="T82" id="Seg_964" s="T72">IF_196X_WomanAndDevil_flk.018 (019)</ta>
            <ta e="T92" id="Seg_965" s="T82">IF_196X_WomanAndDevil_flk.019 (020)</ta>
            <ta e="T97" id="Seg_966" s="T92">IF_196X_WomanAndDevil_flk.020 (021)</ta>
            <ta e="T102" id="Seg_967" s="T97">IF_196X_WomanAndDevil_flk.021 (022)</ta>
            <ta e="T106" id="Seg_968" s="T102">IF_196X_WomanAndDevil_flk.022 (023)</ta>
            <ta e="T112" id="Seg_969" s="T106">IF_196X_WomanAndDevil_flk.023 (024)</ta>
            <ta e="T117" id="Seg_970" s="T112">IF_196X_WomanAndDevil_flk.024 (025)</ta>
            <ta e="T119" id="Seg_971" s="T117">IF_196X_WomanAndDevil_flk.025 (026)</ta>
            <ta e="T121" id="Seg_972" s="T119">IF_196X_WomanAndDevil_flk.026 (027)</ta>
            <ta e="T127" id="Seg_973" s="T121">IF_196X_WomanAndDevil_flk.027 (028)</ta>
            <ta e="T137" id="Seg_974" s="T127">IF_196X_WomanAndDevil_flk.028 (029)</ta>
            <ta e="T141" id="Seg_975" s="T137">IF_196X_WomanAndDevil_flk.029 (030)</ta>
            <ta e="T150" id="Seg_976" s="T141">IF_196X_WomanAndDevil_flk.030 (031)</ta>
            <ta e="T157" id="Seg_977" s="T150">IF_196X_WomanAndDevil_flk.031 (032)</ta>
         </annotation>
         <annotation name="st" tierref="st" />
         <annotation name="stl" tierref="stl">
            <ta e="T55" id="Seg_978" s="T0">((KuAI:)) Фрося Ирикова, сказка.</ta>
            <ta e="T5" id="Seg_979" s="T55">šittɨ ira imasɨqoːqəj ilɨmɨntoːqəj</ta>
            <ta e="T9" id="Seg_980" s="T5">iraitɨ šɔːtɨ qənpoːqəj</ta>
            <ta e="T12" id="Seg_981" s="T9">imaqəj moqɨnɨ kalɨmpoqəj</ta>
            <ta e="T13" id="Seg_982" s="T12">lɨpqɨ moːtajan</ta>
            <ta e="T20" id="Seg_983" s="T13">ukkɨr ima ijajantɨsa namɨssah soːntɨrna, namɨssah soːntɨrna </ta>
            <ta e="T24" id="Seg_984" s="T20">toːna ima ni kəːttıŋɨtɨ</ta>
            <ta e="T25" id="Seg_985" s="T24">((…)) </ta>
            <ta e="T32" id="Seg_986" s="T25">уже lɨpqɨmɔːtpa, me oš ontɨ ontɨn pelʼikoleːŋɔmɨn</ta>
            <ta e="T36" id="Seg_987" s="T32">təm ima ašša uŋkɨltɨmpat</ta>
            <ta e="T38" id="Seg_988" s="T36">melʼtɨ üčʼaleːmpa</ta>
            <ta e="T41" id="Seg_989" s="T38">ima aj kətəŋɨt</ta>
            <ta e="T42" id="Seg_990" s="T41">čʼarɨqalɔːqɨ</ta>
            <ta e="T49" id="Seg_991" s="T42">mɨtta ontɨ ((…)) ontɨ ijantɨ kurantɨ oːrɨčʼitɨ </ta>
            <ta e="T59" id="Seg_992" s="T49">ukkɨr čʼontaːqɨn naːtɨ qotta alʼčʼɨla, noːt ɨrra šerra illa </ta>
            <ta e="T66" id="Seg_993" s="T59">teːtantɨ illa omtɨ illa toːpɨti šittɨ čʼattɨmpɨla</ta>
            <ta e="T72" id="Seg_994" s="T66">ukkɨr ima toː ima ni kətɨŋɨtɨ</ta>
            <ta e="T82" id="Seg_995" s="T72">qəə ilʼčʼa topal toː loːqɨ man kɨntsam tukkɨrɨ po pona tatɨllam</ta>
            <ta e="T92" id="Seg_996" s="T82">naš ima ((…)) ijamtɨ keːraqɨntɨ ((…)) toː saːtčʼemotpa</ta>
            <ta e="T97" id="Seg_997" s="T92">((…)) saːtčʼemolla, hälɨporɨ hɨnːpa ɨraiqɨntɨ</ta>
            <ta e="T102" id="Seg_998" s="T97"> ɨraiqɨntɨ tülä (illa)? ni ketɨŋɨtɨ</ta>
            <ta e="T106" id="Seg_999" s="T102">((XXX)) loːsɨ ira tüːsa</ta>
            <ta e="T112" id="Seg_1000" s="T106">qaj imal təpqaj ɨnna amnɨt</ta>
            <ta e="T117" id="Seg_1001" s="T112">tona iman ɨra ašša ɛːtɨmpat</ta>
            <ta e="T119" id="Seg_1002" s="T117">qajka ((…))</ta>
            <ta e="T121" id="Seg_1003" s="T119">sukkulʼ teːla tüholamnɔːtɨn</ta>
            <ta e="T127" id="Seg_1004" s="T121">pɨpa ɨra na inna qaj amnɨt</ta>
            <ta e="T137" id="Seg_1005" s="T127">ijagitɨ aj imantɨ olɨ ((…)) pont inna tokkaltɨmpat</ta>
            <ta e="T141" id="Seg_1006" s="T137">tüntɨrorɨnötɨn ɨra ni čʼiita</ta>
            <ta e="T150" id="Seg_1007" s="T141">tatan ɨːltɨsantɨ, imam aj ijaim mašim tap čʼetantɨrorɨn´ötɨn</ta>
            <ta e="T157" id="Seg_1008" s="T150">tap kəːštɨ čʼap tüːɔːtɨn naš kural iːtɨ</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T55" id="Seg_1009" s="T0">((KuAI:)) Фрося Ирикова, сказка. </ta>
            <ta e="T5" id="Seg_1010" s="T55">((IF:)) Šittɨ irra imasɨqəj ilɨmɨntɔːqəj. </ta>
            <ta e="T9" id="Seg_1011" s="T5">(Irratɨ=) Irraiːtɨ šöttɨ qənpɔːqəj. </ta>
            <ta e="T12" id="Seg_1012" s="T9">Imaqəj moqɨnɨ qalɨmpɔːqəj. </ta>
            <ta e="T13" id="Seg_1013" s="T12">Lɨpqɨmɔːtajan. </ta>
            <ta e="T20" id="Seg_1014" s="T13">Ukkɨr ima ijajantɨsa namɨššak sɔːntɨrna, namɨššak sɔːntɨrna. </ta>
            <ta e="T24" id="Seg_1015" s="T20">Toːna ima nık kətɨŋɨtɨ. </ta>
            <ta e="T25" id="Seg_1016" s="T24">((…)). </ta>
            <ta e="T32" id="Seg_1017" s="T25">Uže lɨpqɨmɔːtpa, me oš onɨt (ontɨn) pɛlɨkɔːlɛːŋɔːmɨn. </ta>
            <ta e="T36" id="Seg_1018" s="T32">Təm ima ašša üŋkɨltɨmpat. </ta>
            <ta e="T38" id="Seg_1019" s="T36">Melʼtɨ üčʼalɨmpa. </ta>
            <ta e="T41" id="Seg_1020" s="T38">Ima aj kətɨɣɨtɨ. </ta>
            <ta e="T42" id="Seg_1021" s="T41">“(Čʼarɨqalɔːqɨ)”. </ta>
            <ta e="T49" id="Seg_1022" s="T42">Mɨtta (ontɨ=) ((…)) ontɨ ijamtɨ kurraintɨ orɨčʼčʼɨtɨ. </ta>
            <ta e="T59" id="Seg_1023" s="T49">Ukkɨr čʼontoːqɨn mɔːta qotta alʼčʼɨlä, loːsɨ irra šerra ılla. </ta>
            <ta e="T66" id="Seg_1024" s="T59">Təttantɨ illa omtɨ illa topɨti šittɨ čʼattɨmpɨla. </ta>
            <ta e="T72" id="Seg_1025" s="T66">Ukkɨr ima toːna ima nık kətɨŋɨtɨ. </ta>
            <ta e="T82" id="Seg_1026" s="T72">“Qəə ilʼčʼa topal toː loːqɨ mat kətsan tukɨrɨm pona tattɨllam”. </ta>
            <ta e="T92" id="Seg_1027" s="T82">Na ima ((…)) ija neːjamtɨ keːraqɨntɨ ((…)) pona toː sačʼčʼɨmɔːtpa. </ta>
            <ta e="T97" id="Seg_1028" s="T92">((…)) Sačʼčʼɨmɔːlla, qälɨporɨ qənpa ɨraiːqɨntɨ. </ta>
            <ta e="T102" id="Seg_1029" s="T97">İraiːqɨntɨ tülä (ılla) nık kətɨŋɨtɨ. </ta>
            <ta e="T106" id="Seg_1030" s="T102">“((…)) Loːsɨ ira tüːsa. </ta>
            <ta e="T112" id="Seg_1031" s="T106">Qaj imal tɛp qaj ınna amnɨt”. </ta>
            <ta e="T117" id="Seg_1032" s="T112">Toːna iman ira ašša əːtɨmpat. </ta>
            <ta e="T119" id="Seg_1033" s="T117">“Qajqo ((…))?” </ta>
            <ta e="T121" id="Seg_1034" s="T119">Sukulʼteːla tüːqolamnɔːtɨn. </ta>
            <ta e="T127" id="Seg_1035" s="T121">Pɨːpa ɨra na inna qaj amnɨt. </ta>
            <ta e="T137" id="Seg_1036" s="T127">Ijaiːntɨ aj (ima ima=) imantɨ olɨ ((…)) pontɨ ınna tokkaltɨmpat. </ta>
            <ta e="T141" id="Seg_1037" s="T137">Tüntɨrorɨnʼnʼɔːtɨn ira nık čʼıːŋɨta. </ta>
            <ta e="T150" id="Seg_1038" s="T141">“Ta tan moːltɨsantɨ, imam aj ijaiːm mašım tap čʼəːtantɨrorɨnʼnʼɔːtɨn”. </ta>
            <ta e="T157" id="Seg_1039" s="T150">Tap kəštɨ čʼap tüːŋɔːtɨn naš kural iːtɨ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_1040" s="T1">šittɨ</ta>
            <ta e="T3" id="Seg_1041" s="T2">irra</ta>
            <ta e="T4" id="Seg_1042" s="T3">ima-sɨ-qəj</ta>
            <ta e="T5" id="Seg_1043" s="T4">ilɨ-mɨ-ntɔː-qəj</ta>
            <ta e="T6" id="Seg_1044" s="T5">irra-tɨ</ta>
            <ta e="T7" id="Seg_1045" s="T6">irra-iː-tɨ</ta>
            <ta e="T8" id="Seg_1046" s="T7">šöt-tɨ</ta>
            <ta e="T9" id="Seg_1047" s="T8">qən-pɔː-qəj</ta>
            <ta e="T10" id="Seg_1048" s="T9">ima-qəj</ta>
            <ta e="T11" id="Seg_1049" s="T10">moqɨnɨ</ta>
            <ta e="T12" id="Seg_1050" s="T11">qalɨ-mpɔː-qəj</ta>
            <ta e="T13" id="Seg_1051" s="T12">lɨpqɨ-mɔːt-a-ja-n</ta>
            <ta e="T14" id="Seg_1052" s="T13">ukkɨr</ta>
            <ta e="T15" id="Seg_1053" s="T14">ima</ta>
            <ta e="T16" id="Seg_1054" s="T15">ija-ja-ntɨ-sa</ta>
            <ta e="T17" id="Seg_1055" s="T16">namɨ-ššak</ta>
            <ta e="T18" id="Seg_1056" s="T17">sɔːntɨr-na</ta>
            <ta e="T19" id="Seg_1057" s="T18">namɨ-ššak</ta>
            <ta e="T20" id="Seg_1058" s="T19">sɔːntɨr-na</ta>
            <ta e="T21" id="Seg_1059" s="T20">toːna</ta>
            <ta e="T22" id="Seg_1060" s="T21">ima</ta>
            <ta e="T23" id="Seg_1061" s="T22">nık</ta>
            <ta e="T24" id="Seg_1062" s="T23">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T26" id="Seg_1063" s="T25">uže</ta>
            <ta e="T27" id="Seg_1064" s="T26">lɨpqɨ-mɔːt-pa</ta>
            <ta e="T28" id="Seg_1065" s="T27">me</ta>
            <ta e="T29" id="Seg_1066" s="T28">oš</ta>
            <ta e="T30" id="Seg_1067" s="T29">onɨt</ta>
            <ta e="T31" id="Seg_1068" s="T30">ontɨn</ta>
            <ta e="T32" id="Seg_1069" s="T31">pɛlɨ-kɔːl-ɛː-ŋɔː-mɨn</ta>
            <ta e="T33" id="Seg_1070" s="T32">təm</ta>
            <ta e="T34" id="Seg_1071" s="T33">ima</ta>
            <ta e="T35" id="Seg_1072" s="T34">ašša</ta>
            <ta e="T36" id="Seg_1073" s="T35">üŋkɨl-tɨ-mpa-t</ta>
            <ta e="T37" id="Seg_1074" s="T36">melʼtɨ</ta>
            <ta e="T38" id="Seg_1075" s="T37">üčʼalɨ-mpa</ta>
            <ta e="T39" id="Seg_1076" s="T38">ima</ta>
            <ta e="T40" id="Seg_1077" s="T39">aj</ta>
            <ta e="T41" id="Seg_1078" s="T40">kətɨ-ɣɨ-tɨ</ta>
            <ta e="T42" id="Seg_1079" s="T41">čʼarɨ-qa-lɔːqɨ</ta>
            <ta e="T43" id="Seg_1080" s="T42">mɨtta</ta>
            <ta e="T44" id="Seg_1081" s="T43">ontɨ</ta>
            <ta e="T46" id="Seg_1082" s="T45">ontɨ</ta>
            <ta e="T47" id="Seg_1083" s="T46">ija-m-tɨ</ta>
            <ta e="T48" id="Seg_1084" s="T47">kurr-ai-ntɨ</ta>
            <ta e="T49" id="Seg_1085" s="T48">orɨ-čʼ-čʼɨ-tɨ</ta>
            <ta e="T50" id="Seg_1086" s="T49">ukkɨr</ta>
            <ta e="T51" id="Seg_1087" s="T50">čʼontoː-qɨn</ta>
            <ta e="T52" id="Seg_1088" s="T51">mɔːta</ta>
            <ta e="T53" id="Seg_1089" s="T52">qotta</ta>
            <ta e="T54" id="Seg_1090" s="T53">alʼčʼɨ-lä</ta>
            <ta e="T56" id="Seg_1091" s="T54">loːsɨ</ta>
            <ta e="T57" id="Seg_1092" s="T56">irra</ta>
            <ta e="T58" id="Seg_1093" s="T57">šer-a</ta>
            <ta e="T59" id="Seg_1094" s="T58">ılla</ta>
            <ta e="T60" id="Seg_1095" s="T59">tətta-ntɨ</ta>
            <ta e="T61" id="Seg_1096" s="T60">ılla</ta>
            <ta e="T62" id="Seg_1097" s="T61">omtɨ</ta>
            <ta e="T63" id="Seg_1098" s="T62">ılla</ta>
            <ta e="T64" id="Seg_1099" s="T63">topɨ-ti</ta>
            <ta e="T65" id="Seg_1100" s="T64">šittɨ</ta>
            <ta e="T66" id="Seg_1101" s="T65">čʼattɨ-mpɨ-la</ta>
            <ta e="T67" id="Seg_1102" s="T66">ukkɨr</ta>
            <ta e="T68" id="Seg_1103" s="T67">ima</ta>
            <ta e="T69" id="Seg_1104" s="T68">toːna</ta>
            <ta e="T70" id="Seg_1105" s="T69">ima</ta>
            <ta e="T71" id="Seg_1106" s="T70">nık</ta>
            <ta e="T72" id="Seg_1107" s="T71">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T73" id="Seg_1108" s="T72">qəə</ta>
            <ta e="T74" id="Seg_1109" s="T73">ilʼčʼa</ta>
            <ta e="T75" id="Seg_1110" s="T74">topa-l</ta>
            <ta e="T76" id="Seg_1111" s="T75">toː</ta>
            <ta e="T77" id="Seg_1112" s="T76">loːq-ɨ</ta>
            <ta e="T78" id="Seg_1113" s="T77">mat</ta>
            <ta e="T79" id="Seg_1114" s="T78">kətsa-n</ta>
            <ta e="T80" id="Seg_1115" s="T79">tukɨr-ɨ-m</ta>
            <ta e="T81" id="Seg_1116" s="T80">pona</ta>
            <ta e="T82" id="Seg_1117" s="T81">tat-tɨ-la-m</ta>
            <ta e="T83" id="Seg_1118" s="T82">na</ta>
            <ta e="T84" id="Seg_1119" s="T83">ima</ta>
            <ta e="T86" id="Seg_1120" s="T85">ija</ta>
            <ta e="T87" id="Seg_1121" s="T86">neː-ja-m-tɨ</ta>
            <ta e="T88" id="Seg_1122" s="T87">keːra-qɨn-tɨ</ta>
            <ta e="T90" id="Seg_1123" s="T89">pona</ta>
            <ta e="T91" id="Seg_1124" s="T90">toː</ta>
            <ta e="T92" id="Seg_1125" s="T91">sačʼčʼɨmɔːt-pa</ta>
            <ta e="T94" id="Seg_1126" s="T93">sačʼčʼɨmɔːl-la</ta>
            <ta e="T95" id="Seg_1127" s="T94">qälɨ-porɨ</ta>
            <ta e="T96" id="Seg_1128" s="T95">qən-pa</ta>
            <ta e="T97" id="Seg_1129" s="T96">ɨra-iː-qɨn-tɨ</ta>
            <ta e="T98" id="Seg_1130" s="T97">ira-iː-qɨn-tɨ</ta>
            <ta e="T99" id="Seg_1131" s="T98">tü-lä</ta>
            <ta e="T101" id="Seg_1132" s="T100">nık</ta>
            <ta e="T102" id="Seg_1133" s="T101">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T104" id="Seg_1134" s="T103">Loːsɨ</ta>
            <ta e="T105" id="Seg_1135" s="T104">ira</ta>
            <ta e="T106" id="Seg_1136" s="T105">tüː-sa</ta>
            <ta e="T107" id="Seg_1137" s="T106">qaj</ta>
            <ta e="T108" id="Seg_1138" s="T107">ima-l</ta>
            <ta e="T109" id="Seg_1139" s="T108">tɛp</ta>
            <ta e="T110" id="Seg_1140" s="T109">qaj</ta>
            <ta e="T111" id="Seg_1141" s="T110">ınna</ta>
            <ta e="T112" id="Seg_1142" s="T111">am-nɨ-t</ta>
            <ta e="T113" id="Seg_1143" s="T112">toːna</ta>
            <ta e="T114" id="Seg_1144" s="T113">ima-n</ta>
            <ta e="T115" id="Seg_1145" s="T114">ira</ta>
            <ta e="T116" id="Seg_1146" s="T115">ašša</ta>
            <ta e="T117" id="Seg_1147" s="T116">əːtɨ-mpa-t</ta>
            <ta e="T118" id="Seg_1148" s="T117">qaj-qo</ta>
            <ta e="T120" id="Seg_1149" s="T119">sukulʼteː-la</ta>
            <ta e="T121" id="Seg_1150" s="T120">tüː-q-olam-nɔː-tɨn</ta>
            <ta e="T122" id="Seg_1151" s="T121">pɨːpa</ta>
            <ta e="T123" id="Seg_1152" s="T122">ɨra</ta>
            <ta e="T124" id="Seg_1153" s="T123">na</ta>
            <ta e="T125" id="Seg_1154" s="T124">ınna</ta>
            <ta e="T126" id="Seg_1155" s="T125">qaj</ta>
            <ta e="T127" id="Seg_1156" s="T126">am-nɨ-t</ta>
            <ta e="T128" id="Seg_1157" s="T127">ija-iː-n-tɨ</ta>
            <ta e="T129" id="Seg_1158" s="T128">aj</ta>
            <ta e="T132" id="Seg_1159" s="T131">ima-n-tɨ</ta>
            <ta e="T133" id="Seg_1160" s="T132">olɨ</ta>
            <ta e="T135" id="Seg_1161" s="T134">po-ntɨ</ta>
            <ta e="T136" id="Seg_1162" s="T135">ınna</ta>
            <ta e="T137" id="Seg_1163" s="T136">tokk-altɨ-mpa-t</ta>
            <ta e="T138" id="Seg_1164" s="T137">tü-ntɨr-orɨ-nʼ-nʼɔː-tɨn</ta>
            <ta e="T139" id="Seg_1165" s="T138">ira</ta>
            <ta e="T140" id="Seg_1166" s="T139">nık</ta>
            <ta e="T141" id="Seg_1167" s="T140">čʼıː-ŋɨ-ta</ta>
            <ta e="T142" id="Seg_1168" s="T141">ta</ta>
            <ta e="T143" id="Seg_1169" s="T142">tat</ta>
            <ta e="T144" id="Seg_1170" s="T143">moːltɨ-sa-ntɨ</ta>
            <ta e="T145" id="Seg_1171" s="T144">ima-m</ta>
            <ta e="T146" id="Seg_1172" s="T145">aj</ta>
            <ta e="T147" id="Seg_1173" s="T146">ija-iː-m</ta>
            <ta e="T148" id="Seg_1174" s="T147">mašım</ta>
            <ta e="T149" id="Seg_1175" s="T148">tap</ta>
            <ta e="T150" id="Seg_1176" s="T149">čʼəːta-ntɨr-orɨ-nʼ-nʼɔː-tɨn</ta>
            <ta e="T151" id="Seg_1177" s="T150">tap</ta>
            <ta e="T152" id="Seg_1178" s="T151">kəš-tɨ</ta>
            <ta e="T153" id="Seg_1179" s="T152">čʼap</ta>
            <ta e="T154" id="Seg_1180" s="T153">tüː-ŋɔː-tɨn</ta>
            <ta e="T155" id="Seg_1181" s="T154">naš</ta>
            <ta e="T156" id="Seg_1182" s="T155">kural</ta>
            <ta e="T157" id="Seg_1183" s="T156">iːtɨ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_1184" s="T1">šittɨ</ta>
            <ta e="T3" id="Seg_1185" s="T2">ira</ta>
            <ta e="T4" id="Seg_1186" s="T3">ima-sɨ-qı</ta>
            <ta e="T5" id="Seg_1187" s="T4">ilɨ-mpɨ-ntɨ-qı</ta>
            <ta e="T6" id="Seg_1188" s="T5">ira-tɨ</ta>
            <ta e="T7" id="Seg_1189" s="T6">ira-iː-tɨ</ta>
            <ta e="T8" id="Seg_1190" s="T7">šöt-ntɨ</ta>
            <ta e="T9" id="Seg_1191" s="T8">qən-mpɨ-qı</ta>
            <ta e="T10" id="Seg_1192" s="T9">ima-qı</ta>
            <ta e="T11" id="Seg_1193" s="T10">moqɨnä</ta>
            <ta e="T12" id="Seg_1194" s="T11">qalɨ-mpɨ-qı</ta>
            <ta e="T13" id="Seg_1195" s="T12">*lɨpkɨ-mɔːt-ɨ-ŋɨ-n</ta>
            <ta e="T14" id="Seg_1196" s="T13">ukkɨr</ta>
            <ta e="T15" id="Seg_1197" s="T14">ima</ta>
            <ta e="T16" id="Seg_1198" s="T15">iːja-lʼa-ntɨ-sä</ta>
            <ta e="T17" id="Seg_1199" s="T16">na-ššak</ta>
            <ta e="T18" id="Seg_1200" s="T17">sɔːntɨr-ŋɨ</ta>
            <ta e="T19" id="Seg_1201" s="T18">na-ššak</ta>
            <ta e="T20" id="Seg_1202" s="T19">sɔːntɨr-ŋɨ</ta>
            <ta e="T21" id="Seg_1203" s="T20">toːnna</ta>
            <ta e="T22" id="Seg_1204" s="T21">ima</ta>
            <ta e="T23" id="Seg_1205" s="T22">nık</ta>
            <ta e="T24" id="Seg_1206" s="T23">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T26" id="Seg_1207" s="T25">uže</ta>
            <ta e="T27" id="Seg_1208" s="T26">*lɨpkɨ-mɔːt-mpɨ</ta>
            <ta e="T28" id="Seg_1209" s="T27">meː</ta>
            <ta e="T29" id="Seg_1210" s="T28">oš</ta>
            <ta e="T30" id="Seg_1211" s="T29">onɨt</ta>
            <ta e="T31" id="Seg_1212" s="T30">ontɨt</ta>
            <ta e="T32" id="Seg_1213" s="T31">pɛlɨ-kɔːlɨ-ɛː-ŋɨ-mɨt</ta>
            <ta e="T33" id="Seg_1214" s="T32">təp</ta>
            <ta e="T34" id="Seg_1215" s="T33">ima</ta>
            <ta e="T35" id="Seg_1216" s="T34">ašša</ta>
            <ta e="T36" id="Seg_1217" s="T35">üŋkɨl-tɨ-mpɨ-tɨ</ta>
            <ta e="T37" id="Seg_1218" s="T36">meːltɨ</ta>
            <ta e="T38" id="Seg_1219" s="T37">üčʼalɨ-mpɨ</ta>
            <ta e="T39" id="Seg_1220" s="T38">ima</ta>
            <ta e="T40" id="Seg_1221" s="T39">aj</ta>
            <ta e="T41" id="Seg_1222" s="T40">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T42" id="Seg_1223" s="T41">čʼarɨ-qa-lɔːqɨ</ta>
            <ta e="T43" id="Seg_1224" s="T42">mɨta</ta>
            <ta e="T44" id="Seg_1225" s="T43">ontɨ</ta>
            <ta e="T46" id="Seg_1226" s="T45">ontɨ</ta>
            <ta e="T47" id="Seg_1227" s="T46">iːja-m-tɨ</ta>
            <ta e="T48" id="Seg_1228" s="T47">kurɨ-ai-ntɨ</ta>
            <ta e="T49" id="Seg_1229" s="T48">orɨ-š-ntɨ-tɨ</ta>
            <ta e="T50" id="Seg_1230" s="T49">ukkɨr</ta>
            <ta e="T51" id="Seg_1231" s="T50">čʼontɨ-qɨn</ta>
            <ta e="T52" id="Seg_1232" s="T51">mɔːta</ta>
            <ta e="T53" id="Seg_1233" s="T52">qottä</ta>
            <ta e="T54" id="Seg_1234" s="T53">alʼčʼɨ-lä</ta>
            <ta e="T56" id="Seg_1235" s="T54">loːsɨ</ta>
            <ta e="T57" id="Seg_1236" s="T56">ira</ta>
            <ta e="T58" id="Seg_1237" s="T57">šeːr-ŋɨ</ta>
            <ta e="T59" id="Seg_1238" s="T58">ıllä</ta>
            <ta e="T60" id="Seg_1239" s="T59">təttɨ-ntɨ</ta>
            <ta e="T61" id="Seg_1240" s="T60">ıllä</ta>
            <ta e="T62" id="Seg_1241" s="T61">omtɨ</ta>
            <ta e="T63" id="Seg_1242" s="T62">ıllä</ta>
            <ta e="T64" id="Seg_1243" s="T63">topɨ-tɨ</ta>
            <ta e="T65" id="Seg_1244" s="T64">šittɨ</ta>
            <ta e="T66" id="Seg_1245" s="T65">čʼattɨ-mpɨ-lä</ta>
            <ta e="T67" id="Seg_1246" s="T66">ukkɨr</ta>
            <ta e="T68" id="Seg_1247" s="T67">ima</ta>
            <ta e="T69" id="Seg_1248" s="T68">toːnna</ta>
            <ta e="T70" id="Seg_1249" s="T69">ima</ta>
            <ta e="T71" id="Seg_1250" s="T70">nık</ta>
            <ta e="T72" id="Seg_1251" s="T71">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T73" id="Seg_1252" s="T72">qəəəə</ta>
            <ta e="T74" id="Seg_1253" s="T73">ilʼčʼa</ta>
            <ta e="T75" id="Seg_1254" s="T74">topɨ-lɨ</ta>
            <ta e="T76" id="Seg_1255" s="T75">toː</ta>
            <ta e="T77" id="Seg_1256" s="T76">loːq-ätɨ</ta>
            <ta e="T78" id="Seg_1257" s="T77">man</ta>
            <ta e="T79" id="Seg_1258" s="T78">kətsan-n</ta>
            <ta e="T80" id="Seg_1259" s="T79">tukɨr-ɨ-m</ta>
            <ta e="T81" id="Seg_1260" s="T80">ponä</ta>
            <ta e="T82" id="Seg_1261" s="T81">taːtɨ-tɨ-lä-m</ta>
            <ta e="T83" id="Seg_1262" s="T82">na</ta>
            <ta e="T84" id="Seg_1263" s="T83">ima</ta>
            <ta e="T86" id="Seg_1264" s="T85">iːja</ta>
            <ta e="T87" id="Seg_1265" s="T86">neː-ja-m-tɨ</ta>
            <ta e="T88" id="Seg_1266" s="T87">keːra-qɨn-ntɨ</ta>
            <ta e="T90" id="Seg_1267" s="T89">ponä</ta>
            <ta e="T91" id="Seg_1268" s="T90">toː</ta>
            <ta e="T92" id="Seg_1269" s="T91">sačʼčʼɨmɔːt-mpɨ</ta>
            <ta e="T94" id="Seg_1270" s="T93">sačʼčʼɨmɔːt-lä</ta>
            <ta e="T95" id="Seg_1271" s="T94">qälɨŋ-porɨ</ta>
            <ta e="T96" id="Seg_1272" s="T95">qən-mpɨ</ta>
            <ta e="T97" id="Seg_1273" s="T96">ira-iː-qɨn-ntɨ</ta>
            <ta e="T98" id="Seg_1274" s="T97">ira-iː-qɨn-ntɨ</ta>
            <ta e="T99" id="Seg_1275" s="T98">tü-lä</ta>
            <ta e="T101" id="Seg_1276" s="T100">nık</ta>
            <ta e="T102" id="Seg_1277" s="T101">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T104" id="Seg_1278" s="T103">loːsɨ</ta>
            <ta e="T105" id="Seg_1279" s="T104">ira</ta>
            <ta e="T106" id="Seg_1280" s="T105">tü-sɨ</ta>
            <ta e="T107" id="Seg_1281" s="T106">qaj</ta>
            <ta e="T108" id="Seg_1282" s="T107">ima-lɨ</ta>
            <ta e="T109" id="Seg_1283" s="T108">tɛp</ta>
            <ta e="T110" id="Seg_1284" s="T109">qaj</ta>
            <ta e="T111" id="Seg_1285" s="T110">ınnä</ta>
            <ta e="T112" id="Seg_1286" s="T111">am-ntɨ-tɨ</ta>
            <ta e="T113" id="Seg_1287" s="T112">toːnna</ta>
            <ta e="T114" id="Seg_1288" s="T113">ima-n</ta>
            <ta e="T115" id="Seg_1289" s="T114">ira</ta>
            <ta e="T116" id="Seg_1290" s="T115">ašša</ta>
            <ta e="T117" id="Seg_1291" s="T116">əːtɨ-mpɨ-tɨ</ta>
            <ta e="T118" id="Seg_1292" s="T117">qaj-tqo</ta>
            <ta e="T120" id="Seg_1293" s="T119">sukɨltä-la</ta>
            <ta e="T121" id="Seg_1294" s="T120">tü-qo-olam-ŋɨ-tɨt</ta>
            <ta e="T122" id="Seg_1295" s="T121">pɨːpa</ta>
            <ta e="T123" id="Seg_1296" s="T122">ira</ta>
            <ta e="T124" id="Seg_1297" s="T123">na</ta>
            <ta e="T125" id="Seg_1298" s="T124">ınnä</ta>
            <ta e="T126" id="Seg_1299" s="T125">qaj</ta>
            <ta e="T127" id="Seg_1300" s="T126">am-ntɨ-tɨ</ta>
            <ta e="T128" id="Seg_1301" s="T127">iːja-iː-n-tɨ</ta>
            <ta e="T129" id="Seg_1302" s="T128">aj</ta>
            <ta e="T132" id="Seg_1303" s="T131">ima-n-tɨ</ta>
            <ta e="T133" id="Seg_1304" s="T132">olɨ</ta>
            <ta e="T135" id="Seg_1305" s="T134">poː-ntɨ</ta>
            <ta e="T136" id="Seg_1306" s="T135">ınnä</ta>
            <ta e="T137" id="Seg_1307" s="T136">*tokk-altɨ-mpɨ-tɨ</ta>
            <ta e="T138" id="Seg_1308" s="T137">tü-ntɨr-orɨ-š-ŋɨ-tɨt</ta>
            <ta e="T139" id="Seg_1309" s="T138">ira</ta>
            <ta e="T140" id="Seg_1310" s="T139">nık</ta>
            <ta e="T141" id="Seg_1311" s="T140">čʼıːtɨ-ŋɨ-tɨ</ta>
            <ta e="T142" id="Seg_1312" s="T141">ta</ta>
            <ta e="T143" id="Seg_1313" s="T142">tan</ta>
            <ta e="T144" id="Seg_1314" s="T143">moːltɨ-sɨ-ntɨ</ta>
            <ta e="T145" id="Seg_1315" s="T144">ima-mɨ</ta>
            <ta e="T146" id="Seg_1316" s="T145">aj</ta>
            <ta e="T147" id="Seg_1317" s="T146">iːja-iː-mɨ</ta>
            <ta e="T148" id="Seg_1318" s="T147">mašım</ta>
            <ta e="T149" id="Seg_1319" s="T148">tam</ta>
            <ta e="T150" id="Seg_1320" s="T149">čʼəːtɨ-ntɨr-orɨ-š-ŋɨ-tɨt</ta>
            <ta e="T151" id="Seg_1321" s="T150">tam</ta>
            <ta e="T152" id="Seg_1322" s="T151">kəš-ntɨ</ta>
            <ta e="T153" id="Seg_1323" s="T152">čʼam</ta>
            <ta e="T154" id="Seg_1324" s="T153">tü-ŋɨ-tɨt</ta>
            <ta e="T155" id="Seg_1325" s="T154">našša</ta>
            <ta e="T156" id="Seg_1326" s="T155">kural</ta>
            <ta e="T157" id="Seg_1327" s="T156">iːtɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_1328" s="T1">two</ta>
            <ta e="T3" id="Seg_1329" s="T2">old.man.[NOM]</ta>
            <ta e="T4" id="Seg_1330" s="T3">woman-DYA-DU.[NOM]</ta>
            <ta e="T5" id="Seg_1331" s="T4">live-PST.NAR-INFER-3DU.S</ta>
            <ta e="T6" id="Seg_1332" s="T5">husband.[NOM]-3SG</ta>
            <ta e="T7" id="Seg_1333" s="T6">husband-PL.[NOM]-3SG</ta>
            <ta e="T8" id="Seg_1334" s="T7">forest-ILL</ta>
            <ta e="T9" id="Seg_1335" s="T8">leave-PST.NAR-3DU.S</ta>
            <ta e="T10" id="Seg_1336" s="T9">woman-DU.[NOM]</ta>
            <ta e="T11" id="Seg_1337" s="T10">home</ta>
            <ta e="T12" id="Seg_1338" s="T11">stay-PST.NAR-3DU.S</ta>
            <ta e="T13" id="Seg_1339" s="T12">get.dark-DRV-EP-CO-3SG.S</ta>
            <ta e="T14" id="Seg_1340" s="T13">one</ta>
            <ta e="T15" id="Seg_1341" s="T14">woman.[NOM]</ta>
            <ta e="T16" id="Seg_1342" s="T15">child-DIM-OBL.3SG-COM</ta>
            <ta e="T17" id="Seg_1343" s="T16">this-COR</ta>
            <ta e="T18" id="Seg_1344" s="T17">play-CO.[3SG.S]</ta>
            <ta e="T19" id="Seg_1345" s="T18">this-COR</ta>
            <ta e="T20" id="Seg_1346" s="T19">play-CO.[3SG.S]</ta>
            <ta e="T21" id="Seg_1347" s="T20">that</ta>
            <ta e="T22" id="Seg_1348" s="T21">woman.[NOM]</ta>
            <ta e="T23" id="Seg_1349" s="T22">so</ta>
            <ta e="T24" id="Seg_1350" s="T23">say-CO-3SG.O</ta>
            <ta e="T26" id="Seg_1351" s="T25">already</ta>
            <ta e="T27" id="Seg_1352" s="T26">get.dark-DRV-PST.NAR.[3SG.S]</ta>
            <ta e="T28" id="Seg_1353" s="T27">we.PL.NOM</ta>
            <ta e="T29" id="Seg_1354" s="T28">%%</ta>
            <ta e="T30" id="Seg_1355" s="T29">oneself.1PL.[NOM]</ta>
            <ta e="T31" id="Seg_1356" s="T30">oneself.3PL.[NOM]</ta>
            <ta e="T32" id="Seg_1357" s="T31">friend-CAR-PFV-CO-1PL</ta>
            <ta e="T33" id="Seg_1358" s="T32">(s)he</ta>
            <ta e="T34" id="Seg_1359" s="T33">woman.[NOM]</ta>
            <ta e="T35" id="Seg_1360" s="T34">NEG</ta>
            <ta e="T36" id="Seg_1361" s="T35">hear-TR-DUR-3SG.O</ta>
            <ta e="T37" id="Seg_1362" s="T36">all.the.time</ta>
            <ta e="T38" id="Seg_1363" s="T37">play.about-DUR.[3SG.S]</ta>
            <ta e="T39" id="Seg_1364" s="T38">woman.[NOM]</ta>
            <ta e="T40" id="Seg_1365" s="T39">again</ta>
            <ta e="T41" id="Seg_1366" s="T40">say-CO-3SG.O</ta>
            <ta e="T42" id="Seg_1367" s="T41">voice-%%-in.some.degree</ta>
            <ta e="T43" id="Seg_1368" s="T42">as.if</ta>
            <ta e="T44" id="Seg_1369" s="T43">own.3SG</ta>
            <ta e="T46" id="Seg_1370" s="T45">own.3SG</ta>
            <ta e="T47" id="Seg_1371" s="T46">child-ACC-3SG</ta>
            <ta e="T48" id="Seg_1372" s="T47">swaddle-%%-INFER.[3SG.S]</ta>
            <ta e="T49" id="Seg_1373" s="T48">force-VBLZ-INFER-3SG.O</ta>
            <ta e="T50" id="Seg_1374" s="T49">one</ta>
            <ta e="T51" id="Seg_1375" s="T50">middle-LOC</ta>
            <ta e="T52" id="Seg_1376" s="T51">tent.door.[NOM]</ta>
            <ta e="T53" id="Seg_1377" s="T52">on.one_s.back</ta>
            <ta e="T54" id="Seg_1378" s="T53">fall-CVB</ta>
            <ta e="T56" id="Seg_1379" s="T54">bear.[NOM]</ta>
            <ta e="T57" id="Seg_1380" s="T56">old.man.[NOM]</ta>
            <ta e="T58" id="Seg_1381" s="T57">come.in-CO.[3SG.S]</ta>
            <ta e="T59" id="Seg_1382" s="T58">down</ta>
            <ta e="T60" id="Seg_1383" s="T59">earth-ILL</ta>
            <ta e="T61" id="Seg_1384" s="T60">down</ta>
            <ta e="T62" id="Seg_1385" s="T61">sit.down.[3SG.S]</ta>
            <ta e="T63" id="Seg_1386" s="T62">down</ta>
            <ta e="T64" id="Seg_1387" s="T63">leg.[NOM]-3SG</ta>
            <ta e="T65" id="Seg_1388" s="T64">every.which.way</ta>
            <ta e="T66" id="Seg_1389" s="T65">throw-DUR-CVB</ta>
            <ta e="T67" id="Seg_1390" s="T66">one</ta>
            <ta e="T68" id="Seg_1391" s="T67">woman.[NOM]</ta>
            <ta e="T69" id="Seg_1392" s="T68">that</ta>
            <ta e="T70" id="Seg_1393" s="T69">woman.[NOM]</ta>
            <ta e="T71" id="Seg_1394" s="T70">so</ta>
            <ta e="T72" id="Seg_1395" s="T71">say-CO-3SG.O</ta>
            <ta e="T73" id="Seg_1396" s="T72">INTERJ</ta>
            <ta e="T74" id="Seg_1397" s="T73">grandfather.[NOM]</ta>
            <ta e="T75" id="Seg_1398" s="T74">leg.[NOM]-2SG</ta>
            <ta e="T76" id="Seg_1399" s="T75">away</ta>
            <ta e="T77" id="Seg_1400" s="T76">stick.up-IMP.2SG.O</ta>
            <ta e="T78" id="Seg_1401" s="T77">I.NOM</ta>
            <ta e="T79" id="Seg_1402" s="T78">grandson-GEN</ta>
            <ta e="T80" id="Seg_1403" s="T79">hay.dust-EP-ACC</ta>
            <ta e="T81" id="Seg_1404" s="T80">outwards</ta>
            <ta e="T82" id="Seg_1405" s="T81">bring-TR-OPT-1SG.O</ta>
            <ta e="T83" id="Seg_1406" s="T82">this</ta>
            <ta e="T84" id="Seg_1407" s="T83">woman.[NOM]</ta>
            <ta e="T86" id="Seg_1408" s="T85">son.[NOM]</ta>
            <ta e="T87" id="Seg_1409" s="T86">living.being-DIM-ACC-3SG</ta>
            <ta e="T88" id="Seg_1410" s="T87">%%-ILL-OBL.3SG</ta>
            <ta e="T90" id="Seg_1411" s="T89">outwards</ta>
            <ta e="T91" id="Seg_1412" s="T90">away</ta>
            <ta e="T92" id="Seg_1413" s="T91">jump.out-PST.NAR.[3SG.S]</ta>
            <ta e="T94" id="Seg_1414" s="T93">jump.out-CVB</ta>
            <ta e="T95" id="Seg_1415" s="T94">at.a.run-%%</ta>
            <ta e="T96" id="Seg_1416" s="T95">go.away-PST.NAR.[3SG.S]</ta>
            <ta e="T97" id="Seg_1417" s="T96">husband-PL-ILL-OBL.3SG</ta>
            <ta e="T98" id="Seg_1418" s="T97">husband-PL-ILL-OBL.3SG</ta>
            <ta e="T99" id="Seg_1419" s="T98">come-CVB</ta>
            <ta e="T101" id="Seg_1420" s="T100">so</ta>
            <ta e="T102" id="Seg_1421" s="T101">say-CO-3SG.O</ta>
            <ta e="T104" id="Seg_1422" s="T103">devil.[NOM]</ta>
            <ta e="T105" id="Seg_1423" s="T104">old.man.[NOM]</ta>
            <ta e="T106" id="Seg_1424" s="T105">come-PST</ta>
            <ta e="T107" id="Seg_1425" s="T106">whether</ta>
            <ta e="T108" id="Seg_1426" s="T107">wife.[NOM]-2SG</ta>
            <ta e="T109" id="Seg_1427" s="T108">maybe</ta>
            <ta e="T110" id="Seg_1428" s="T109">whether</ta>
            <ta e="T111" id="Seg_1429" s="T110">up</ta>
            <ta e="T112" id="Seg_1430" s="T111">eat-INFER-3SG.O</ta>
            <ta e="T113" id="Seg_1431" s="T112">that</ta>
            <ta e="T114" id="Seg_1432" s="T113">woman-GEN</ta>
            <ta e="T115" id="Seg_1433" s="T114">old.man.[NOM]</ta>
            <ta e="T116" id="Seg_1434" s="T115">NEG</ta>
            <ta e="T117" id="Seg_1435" s="T116">%%-PST.NAR-3SG.O</ta>
            <ta e="T118" id="Seg_1436" s="T117">what-TRL</ta>
            <ta e="T120" id="Seg_1437" s="T119">back-%%</ta>
            <ta e="T121" id="Seg_1438" s="T120">come-INF-be.going.to-CO-3PL</ta>
            <ta e="T122" id="Seg_1439" s="T121">bear.[NOM]</ta>
            <ta e="T123" id="Seg_1440" s="T122">old.man.[NOM]</ta>
            <ta e="T124" id="Seg_1441" s="T123">INFER</ta>
            <ta e="T125" id="Seg_1442" s="T124">up</ta>
            <ta e="T126" id="Seg_1443" s="T125">whether</ta>
            <ta e="T127" id="Seg_1444" s="T126">eat-INFER-3SG.O</ta>
            <ta e="T128" id="Seg_1445" s="T127">child-PL-GEN-3SG</ta>
            <ta e="T129" id="Seg_1446" s="T128">and</ta>
            <ta e="T132" id="Seg_1447" s="T131">wife-GEN-3SG</ta>
            <ta e="T133" id="Seg_1448" s="T132">head.[NOM]</ta>
            <ta e="T135" id="Seg_1449" s="T134">tree-ILL</ta>
            <ta e="T136" id="Seg_1450" s="T135">up</ta>
            <ta e="T137" id="Seg_1451" s="T136">put.on-TR-PST.NAR-3SG.O</ta>
            <ta e="T138" id="Seg_1452" s="T137">come-DRV-force-VBLZ-CO-3PL</ta>
            <ta e="T139" id="Seg_1453" s="T138">old.man.[NOM]</ta>
            <ta e="T140" id="Seg_1454" s="T139">so</ta>
            <ta e="T141" id="Seg_1455" s="T140">tell-CO-3SG.O</ta>
            <ta e="T142" id="Seg_1456" s="T141">%%</ta>
            <ta e="T143" id="Seg_1457" s="T142">you.SG.NOM</ta>
            <ta e="T144" id="Seg_1458" s="T143">lie-PST-2SG.S</ta>
            <ta e="T145" id="Seg_1459" s="T144">wife.[NOM]-1SG</ta>
            <ta e="T146" id="Seg_1460" s="T145">and</ta>
            <ta e="T147" id="Seg_1461" s="T146">child-PL.[NOM]-1SG</ta>
            <ta e="T148" id="Seg_1462" s="T147">I.ACC</ta>
            <ta e="T149" id="Seg_1463" s="T148">this</ta>
            <ta e="T150" id="Seg_1464" s="T149">meet-DRV-force-VBLZ-CO-3PL</ta>
            <ta e="T151" id="Seg_1465" s="T150">this</ta>
            <ta e="T152" id="Seg_1466" s="T151">half-ILL</ta>
            <ta e="T153" id="Seg_1467" s="T152">hardly</ta>
            <ta e="T154" id="Seg_1468" s="T153">come-CO-3PL</ta>
            <ta e="T155" id="Seg_1469" s="T154">numerous</ta>
            <ta e="T156" id="Seg_1470" s="T155">%%</ta>
            <ta e="T157" id="Seg_1471" s="T156">%%</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_1472" s="T1">два</ta>
            <ta e="T3" id="Seg_1473" s="T2">старик.[NOM]</ta>
            <ta e="T4" id="Seg_1474" s="T3">женщина-DYA-DU.[NOM]</ta>
            <ta e="T5" id="Seg_1475" s="T4">жить-PST.NAR-INFER-3DU.S</ta>
            <ta e="T6" id="Seg_1476" s="T5">муж.[NOM]-3SG</ta>
            <ta e="T7" id="Seg_1477" s="T6">муж-PL.[NOM]-3SG</ta>
            <ta e="T8" id="Seg_1478" s="T7">лес-ILL</ta>
            <ta e="T9" id="Seg_1479" s="T8">отправиться-PST.NAR-3DU.S</ta>
            <ta e="T10" id="Seg_1480" s="T9">женщина-DU.[NOM]</ta>
            <ta e="T11" id="Seg_1481" s="T10">домой</ta>
            <ta e="T12" id="Seg_1482" s="T11">остаться-PST.NAR-3DU.S</ta>
            <ta e="T13" id="Seg_1483" s="T12">стемнеть-DRV-EP-CO-3SG.S</ta>
            <ta e="T14" id="Seg_1484" s="T13">один</ta>
            <ta e="T15" id="Seg_1485" s="T14">женщина.[NOM]</ta>
            <ta e="T16" id="Seg_1486" s="T15">ребенок-DIM-OBL.3SG-COM</ta>
            <ta e="T17" id="Seg_1487" s="T16">этот-COR</ta>
            <ta e="T18" id="Seg_1488" s="T17">играть-CO.[3SG.S]</ta>
            <ta e="T19" id="Seg_1489" s="T18">этот-COR</ta>
            <ta e="T20" id="Seg_1490" s="T19">играть-CO.[3SG.S]</ta>
            <ta e="T21" id="Seg_1491" s="T20">тот</ta>
            <ta e="T22" id="Seg_1492" s="T21">женщина.[NOM]</ta>
            <ta e="T23" id="Seg_1493" s="T22">так</ta>
            <ta e="T24" id="Seg_1494" s="T23">сказать-CO-3SG.O</ta>
            <ta e="T26" id="Seg_1495" s="T25">уже</ta>
            <ta e="T27" id="Seg_1496" s="T26">стемнеть-DRV-PST.NAR.[3SG.S]</ta>
            <ta e="T28" id="Seg_1497" s="T27">мы.PL.NOM</ta>
            <ta e="T29" id="Seg_1498" s="T28">%%</ta>
            <ta e="T30" id="Seg_1499" s="T29">сам.1PL.[NOM]</ta>
            <ta e="T31" id="Seg_1500" s="T30">сам.3PL.[NOM]</ta>
            <ta e="T32" id="Seg_1501" s="T31">друг-CAR-PFV-CO-1PL</ta>
            <ta e="T33" id="Seg_1502" s="T32">он(а)</ta>
            <ta e="T34" id="Seg_1503" s="T33">женщина.[NOM]</ta>
            <ta e="T35" id="Seg_1504" s="T34">NEG</ta>
            <ta e="T36" id="Seg_1505" s="T35">слушать-TR-DUR-3SG.O</ta>
            <ta e="T37" id="Seg_1506" s="T36">всё.время</ta>
            <ta e="T38" id="Seg_1507" s="T37">баловаться-DUR.[3SG.S]</ta>
            <ta e="T39" id="Seg_1508" s="T38">женщина.[NOM]</ta>
            <ta e="T40" id="Seg_1509" s="T39">опять</ta>
            <ta e="T41" id="Seg_1510" s="T40">сказать-CO-3SG.O</ta>
            <ta e="T42" id="Seg_1511" s="T41">голос-%%-в.некоторой.степени</ta>
            <ta e="T43" id="Seg_1512" s="T42">будто</ta>
            <ta e="T44" id="Seg_1513" s="T43">свой.3SG</ta>
            <ta e="T46" id="Seg_1514" s="T45">свой.3SG</ta>
            <ta e="T47" id="Seg_1515" s="T46">ребенок-ACC-3SG</ta>
            <ta e="T48" id="Seg_1516" s="T47">запеленать-%%-INFER.[3SG.S]</ta>
            <ta e="T49" id="Seg_1517" s="T48">сила-VBLZ-INFER-3SG.O</ta>
            <ta e="T50" id="Seg_1518" s="T49">один</ta>
            <ta e="T51" id="Seg_1519" s="T50">середина-LOC</ta>
            <ta e="T52" id="Seg_1520" s="T51">дверь.чума.[NOM]</ta>
            <ta e="T53" id="Seg_1521" s="T52">на.спину</ta>
            <ta e="T54" id="Seg_1522" s="T53">упасть-CVB</ta>
            <ta e="T56" id="Seg_1523" s="T54">медведь.[NOM]</ta>
            <ta e="T57" id="Seg_1524" s="T56">старик.[NOM]</ta>
            <ta e="T58" id="Seg_1525" s="T57">войти-CO.[3SG.S]</ta>
            <ta e="T59" id="Seg_1526" s="T58">вниз</ta>
            <ta e="T60" id="Seg_1527" s="T59">земля-ILL</ta>
            <ta e="T61" id="Seg_1528" s="T60">вниз</ta>
            <ta e="T62" id="Seg_1529" s="T61">сесть.[3SG.S]</ta>
            <ta e="T63" id="Seg_1530" s="T62">вниз</ta>
            <ta e="T64" id="Seg_1531" s="T63">нога.[NOM]-3SG</ta>
            <ta e="T65" id="Seg_1532" s="T64">в.разные.стороны</ta>
            <ta e="T66" id="Seg_1533" s="T65">бросить-DUR-CVB</ta>
            <ta e="T67" id="Seg_1534" s="T66">один</ta>
            <ta e="T68" id="Seg_1535" s="T67">женщина.[NOM]</ta>
            <ta e="T69" id="Seg_1536" s="T68">тот</ta>
            <ta e="T70" id="Seg_1537" s="T69">женщина.[NOM]</ta>
            <ta e="T71" id="Seg_1538" s="T70">так</ta>
            <ta e="T72" id="Seg_1539" s="T71">сказать-CO-3SG.O</ta>
            <ta e="T73" id="Seg_1540" s="T72">INTERJ</ta>
            <ta e="T74" id="Seg_1541" s="T73">дедушка.[NOM]</ta>
            <ta e="T75" id="Seg_1542" s="T74">нога.[NOM]-2SG</ta>
            <ta e="T76" id="Seg_1543" s="T75">прочь</ta>
            <ta e="T77" id="Seg_1544" s="T76">стоять.торчком-IMP.2SG.O</ta>
            <ta e="T78" id="Seg_1545" s="T77">я.NOM</ta>
            <ta e="T79" id="Seg_1546" s="T78">внук-GEN</ta>
            <ta e="T80" id="Seg_1547" s="T79">труха-EP-ACC</ta>
            <ta e="T81" id="Seg_1548" s="T80">наружу</ta>
            <ta e="T82" id="Seg_1549" s="T81">принести-TR-OPT-1SG.O</ta>
            <ta e="T83" id="Seg_1550" s="T82">этот</ta>
            <ta e="T84" id="Seg_1551" s="T83">женщина.[NOM]</ta>
            <ta e="T86" id="Seg_1552" s="T85">сын.[NOM]</ta>
            <ta e="T87" id="Seg_1553" s="T86">живое.существо-DIM-ACC-3SG</ta>
            <ta e="T88" id="Seg_1554" s="T87">%%-ILL-OBL.3SG</ta>
            <ta e="T90" id="Seg_1555" s="T89">наружу</ta>
            <ta e="T91" id="Seg_1556" s="T90">прочь</ta>
            <ta e="T92" id="Seg_1557" s="T91">выскочить-PST.NAR.[3SG.S]</ta>
            <ta e="T94" id="Seg_1558" s="T93">выскочить-CVB</ta>
            <ta e="T95" id="Seg_1559" s="T94">бегом-%%</ta>
            <ta e="T96" id="Seg_1560" s="T95">уйти-PST.NAR.[3SG.S]</ta>
            <ta e="T97" id="Seg_1561" s="T96">муж-PL-ILL-OBL.3SG</ta>
            <ta e="T98" id="Seg_1562" s="T97">муж-PL-ILL-OBL.3SG</ta>
            <ta e="T99" id="Seg_1563" s="T98">прийти-CVB</ta>
            <ta e="T101" id="Seg_1564" s="T100">так</ta>
            <ta e="T102" id="Seg_1565" s="T101">сказать-CO-3SG.O</ta>
            <ta e="T104" id="Seg_1566" s="T103">чёрт.[NOM]</ta>
            <ta e="T105" id="Seg_1567" s="T104">старик.[NOM]</ta>
            <ta e="T106" id="Seg_1568" s="T105">прийти-PST</ta>
            <ta e="T107" id="Seg_1569" s="T106">что.ли</ta>
            <ta e="T108" id="Seg_1570" s="T107">жена.[NOM]-2SG</ta>
            <ta e="T109" id="Seg_1571" s="T108">может.быть</ta>
            <ta e="T110" id="Seg_1572" s="T109">что.ли</ta>
            <ta e="T111" id="Seg_1573" s="T110">вверх</ta>
            <ta e="T112" id="Seg_1574" s="T111">съесть-INFER-3SG.O</ta>
            <ta e="T113" id="Seg_1575" s="T112">тот</ta>
            <ta e="T114" id="Seg_1576" s="T113">женщина-GEN</ta>
            <ta e="T115" id="Seg_1577" s="T114">старик.[NOM]</ta>
            <ta e="T116" id="Seg_1578" s="T115">NEG</ta>
            <ta e="T117" id="Seg_1579" s="T116">%%-PST.NAR-3SG.O</ta>
            <ta e="T118" id="Seg_1580" s="T117">что-TRL</ta>
            <ta e="T120" id="Seg_1581" s="T119">назад-%%</ta>
            <ta e="T121" id="Seg_1582" s="T120">прийти-INF-собраться-CO-3PL</ta>
            <ta e="T122" id="Seg_1583" s="T121">медведь.[NOM]</ta>
            <ta e="T123" id="Seg_1584" s="T122">старик.[NOM]</ta>
            <ta e="T124" id="Seg_1585" s="T123">INFER</ta>
            <ta e="T125" id="Seg_1586" s="T124">вверх</ta>
            <ta e="T126" id="Seg_1587" s="T125">что.ли</ta>
            <ta e="T127" id="Seg_1588" s="T126">съесть-INFER-3SG.O</ta>
            <ta e="T128" id="Seg_1589" s="T127">ребенок-PL-GEN-3SG</ta>
            <ta e="T129" id="Seg_1590" s="T128">и</ta>
            <ta e="T132" id="Seg_1591" s="T131">жена-GEN-3SG</ta>
            <ta e="T133" id="Seg_1592" s="T132">голова.[NOM]</ta>
            <ta e="T135" id="Seg_1593" s="T134">дерево-ILL</ta>
            <ta e="T136" id="Seg_1594" s="T135">вверх</ta>
            <ta e="T137" id="Seg_1595" s="T136">надеть-TR-PST.NAR-3SG.O</ta>
            <ta e="T138" id="Seg_1596" s="T137">прийти-DRV-сила-VBLZ-CO-3PL</ta>
            <ta e="T139" id="Seg_1597" s="T138">старик.[NOM]</ta>
            <ta e="T140" id="Seg_1598" s="T139">так</ta>
            <ta e="T141" id="Seg_1599" s="T140">сказать-CO-3SG.O</ta>
            <ta e="T142" id="Seg_1600" s="T141">%%</ta>
            <ta e="T143" id="Seg_1601" s="T142">ты.NOM</ta>
            <ta e="T144" id="Seg_1602" s="T143">врать-PST-2SG.S</ta>
            <ta e="T145" id="Seg_1603" s="T144">жена.[NOM]-1SG</ta>
            <ta e="T146" id="Seg_1604" s="T145">и</ta>
            <ta e="T147" id="Seg_1605" s="T146">ребенок-PL.[NOM]-1SG</ta>
            <ta e="T148" id="Seg_1606" s="T147">я.ACC</ta>
            <ta e="T149" id="Seg_1607" s="T148">этот</ta>
            <ta e="T150" id="Seg_1608" s="T149">встретить-DRV-сила-VBLZ-CO-3PL</ta>
            <ta e="T151" id="Seg_1609" s="T150">этот</ta>
            <ta e="T152" id="Seg_1610" s="T151">половина-ILL</ta>
            <ta e="T153" id="Seg_1611" s="T152">едва</ta>
            <ta e="T154" id="Seg_1612" s="T153">прийти-CO-3PL</ta>
            <ta e="T155" id="Seg_1613" s="T154">многочисленный</ta>
            <ta e="T156" id="Seg_1614" s="T155">%%</ta>
            <ta e="T157" id="Seg_1615" s="T156">%%</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1616" s="T1">num</ta>
            <ta e="T3" id="Seg_1617" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_1618" s="T3">n-n&gt;n-n:num-n:case</ta>
            <ta e="T5" id="Seg_1619" s="T4">v-v:tense-v:mood-v:pn</ta>
            <ta e="T6" id="Seg_1620" s="T5">n-n:case-n:poss</ta>
            <ta e="T7" id="Seg_1621" s="T6">n-n:num-n:case-n:poss</ta>
            <ta e="T8" id="Seg_1622" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_1623" s="T8">v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_1624" s="T9">n-n:num-n:case</ta>
            <ta e="T11" id="Seg_1625" s="T10">adv</ta>
            <ta e="T12" id="Seg_1626" s="T11">v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_1627" s="T12">v-v&gt;v-n:ins-v:ins-v:pn</ta>
            <ta e="T14" id="Seg_1628" s="T13">num</ta>
            <ta e="T15" id="Seg_1629" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_1630" s="T15">n-n&gt;n-n:obl.poss-n:case</ta>
            <ta e="T17" id="Seg_1631" s="T16">dem-n:case</ta>
            <ta e="T18" id="Seg_1632" s="T17">v-v:ins-v:pn</ta>
            <ta e="T19" id="Seg_1633" s="T18">dem-n:case</ta>
            <ta e="T20" id="Seg_1634" s="T19">v-v:ins-v:pn</ta>
            <ta e="T21" id="Seg_1635" s="T20">dem</ta>
            <ta e="T22" id="Seg_1636" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_1637" s="T22">adv</ta>
            <ta e="T24" id="Seg_1638" s="T23">v-v:ins-v:pn</ta>
            <ta e="T26" id="Seg_1639" s="T25">adv</ta>
            <ta e="T27" id="Seg_1640" s="T26">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_1641" s="T27">pers</ta>
            <ta e="T29" id="Seg_1642" s="T28">%%</ta>
            <ta e="T30" id="Seg_1643" s="T29">emphpro-n:case</ta>
            <ta e="T31" id="Seg_1644" s="T30">emphpro-n:case</ta>
            <ta e="T32" id="Seg_1645" s="T31">n-n&gt;adj-v&gt;v-v:ins-n:poss</ta>
            <ta e="T33" id="Seg_1646" s="T32">pers</ta>
            <ta e="T34" id="Seg_1647" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_1648" s="T34">ptcl</ta>
            <ta e="T36" id="Seg_1649" s="T35">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T37" id="Seg_1650" s="T36">adv</ta>
            <ta e="T38" id="Seg_1651" s="T37">v-v&gt;v-v:pn</ta>
            <ta e="T39" id="Seg_1652" s="T38">n-n:case</ta>
            <ta e="T40" id="Seg_1653" s="T39">adv</ta>
            <ta e="T41" id="Seg_1654" s="T40">v-v:ins-v:pn</ta>
            <ta e="T42" id="Seg_1655" s="T41">n-%%-adj&gt;adj</ta>
            <ta e="T43" id="Seg_1656" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_1657" s="T43">emphpro</ta>
            <ta e="T46" id="Seg_1658" s="T45">emphpro</ta>
            <ta e="T47" id="Seg_1659" s="T46">n-n:case-n:poss</ta>
            <ta e="T48" id="Seg_1660" s="T47">v-%%-v:tense.mood-v:pn</ta>
            <ta e="T49" id="Seg_1661" s="T48">n-n&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T50" id="Seg_1662" s="T49">num</ta>
            <ta e="T51" id="Seg_1663" s="T50">n-n:case</ta>
            <ta e="T52" id="Seg_1664" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_1665" s="T52">adv</ta>
            <ta e="T54" id="Seg_1666" s="T53">v-v&gt;adv</ta>
            <ta e="T56" id="Seg_1667" s="T54">n-n:case</ta>
            <ta e="T57" id="Seg_1668" s="T56">n-n:case</ta>
            <ta e="T58" id="Seg_1669" s="T57">v-v:ins-v:pn</ta>
            <ta e="T59" id="Seg_1670" s="T58">adv</ta>
            <ta e="T60" id="Seg_1671" s="T59">n-n:case</ta>
            <ta e="T61" id="Seg_1672" s="T60">preverb</ta>
            <ta e="T62" id="Seg_1673" s="T61">v-v:pn</ta>
            <ta e="T63" id="Seg_1674" s="T62">adv</ta>
            <ta e="T64" id="Seg_1675" s="T63">n-n:case-n:poss</ta>
            <ta e="T65" id="Seg_1676" s="T64">preverb</ta>
            <ta e="T66" id="Seg_1677" s="T65">v-v&gt;v-v&gt;adv</ta>
            <ta e="T67" id="Seg_1678" s="T66">num</ta>
            <ta e="T68" id="Seg_1679" s="T67">n-n:case</ta>
            <ta e="T69" id="Seg_1680" s="T68">dem</ta>
            <ta e="T70" id="Seg_1681" s="T69">n-n:case</ta>
            <ta e="T71" id="Seg_1682" s="T70">adv</ta>
            <ta e="T72" id="Seg_1683" s="T71">v-v:ins-v:pn</ta>
            <ta e="T73" id="Seg_1684" s="T72">interj</ta>
            <ta e="T74" id="Seg_1685" s="T73">n-n:case</ta>
            <ta e="T75" id="Seg_1686" s="T74">n-n:case-n:poss</ta>
            <ta e="T76" id="Seg_1687" s="T75">preverb</ta>
            <ta e="T77" id="Seg_1688" s="T76">v-v:mood.pn</ta>
            <ta e="T78" id="Seg_1689" s="T77">pers</ta>
            <ta e="T79" id="Seg_1690" s="T78">n-n:case</ta>
            <ta e="T80" id="Seg_1691" s="T79">n-n:ins-n:case</ta>
            <ta e="T81" id="Seg_1692" s="T80">adv</ta>
            <ta e="T82" id="Seg_1693" s="T81">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T83" id="Seg_1694" s="T82">dem</ta>
            <ta e="T84" id="Seg_1695" s="T83">n-n:case</ta>
            <ta e="T86" id="Seg_1696" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_1697" s="T86">n-n&gt;n-n:case-n:poss</ta>
            <ta e="T88" id="Seg_1698" s="T87">%%-n:case-n:obl.poss</ta>
            <ta e="T90" id="Seg_1699" s="T89">adv</ta>
            <ta e="T91" id="Seg_1700" s="T90">preverb</ta>
            <ta e="T92" id="Seg_1701" s="T91">v-v:tense-v:pn</ta>
            <ta e="T94" id="Seg_1702" s="T93">v-v&gt;adv</ta>
            <ta e="T95" id="Seg_1703" s="T94">adv-%%</ta>
            <ta e="T96" id="Seg_1704" s="T95">v-v:tense-v:pn</ta>
            <ta e="T97" id="Seg_1705" s="T96">n-n:num-n:case-n:obl.poss</ta>
            <ta e="T98" id="Seg_1706" s="T97">n-n:num-n:case-n:obl.poss</ta>
            <ta e="T99" id="Seg_1707" s="T98">v-v&gt;adv</ta>
            <ta e="T101" id="Seg_1708" s="T100">adv</ta>
            <ta e="T102" id="Seg_1709" s="T101">v-v:ins-v:pn</ta>
            <ta e="T104" id="Seg_1710" s="T103">n-n:case</ta>
            <ta e="T105" id="Seg_1711" s="T104">n-n:case</ta>
            <ta e="T106" id="Seg_1712" s="T105">v-v:tense</ta>
            <ta e="T107" id="Seg_1713" s="T106">ptcl</ta>
            <ta e="T108" id="Seg_1714" s="T107">n-n:case-n:poss</ta>
            <ta e="T109" id="Seg_1715" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_1716" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_1717" s="T110">preverb</ta>
            <ta e="T112" id="Seg_1718" s="T111">v-v:tense.mood-v:pn</ta>
            <ta e="T113" id="Seg_1719" s="T112">dem</ta>
            <ta e="T114" id="Seg_1720" s="T113">n-n:case</ta>
            <ta e="T115" id="Seg_1721" s="T114">n-n:case</ta>
            <ta e="T116" id="Seg_1722" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_1723" s="T116">v-v:tense-v:pn</ta>
            <ta e="T118" id="Seg_1724" s="T117">interrog-n:case</ta>
            <ta e="T120" id="Seg_1725" s="T119">adv-%%</ta>
            <ta e="T121" id="Seg_1726" s="T120">v-v:inf-v-v:ins-v:pn</ta>
            <ta e="T122" id="Seg_1727" s="T121">n-n:case</ta>
            <ta e="T123" id="Seg_1728" s="T122">n-n:case</ta>
            <ta e="T124" id="Seg_1729" s="T123">ptcl</ta>
            <ta e="T125" id="Seg_1730" s="T124">preverb</ta>
            <ta e="T126" id="Seg_1731" s="T125">ptcl</ta>
            <ta e="T127" id="Seg_1732" s="T126">v-v:tense.mood-v:pn</ta>
            <ta e="T128" id="Seg_1733" s="T127">n-n:num-n:case-n:poss</ta>
            <ta e="T129" id="Seg_1734" s="T128">conj</ta>
            <ta e="T132" id="Seg_1735" s="T131">n-n:case-n:poss</ta>
            <ta e="T133" id="Seg_1736" s="T132">n-n:case</ta>
            <ta e="T135" id="Seg_1737" s="T134">n-n:case</ta>
            <ta e="T136" id="Seg_1738" s="T135">preverb</ta>
            <ta e="T137" id="Seg_1739" s="T136">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T138" id="Seg_1740" s="T137">v-v&gt;v-n-n&gt;v-v:ins-v:pn</ta>
            <ta e="T139" id="Seg_1741" s="T138">n-n:case</ta>
            <ta e="T140" id="Seg_1742" s="T139">adv</ta>
            <ta e="T141" id="Seg_1743" s="T140">v-v:ins-v:pn</ta>
            <ta e="T142" id="Seg_1744" s="T141">%%</ta>
            <ta e="T143" id="Seg_1745" s="T142">pers</ta>
            <ta e="T144" id="Seg_1746" s="T143">v-v:tense-v:pn</ta>
            <ta e="T145" id="Seg_1747" s="T144">n-n:case-n:poss</ta>
            <ta e="T146" id="Seg_1748" s="T145">conj</ta>
            <ta e="T147" id="Seg_1749" s="T146">n-n:num-n:case-n:poss</ta>
            <ta e="T148" id="Seg_1750" s="T147">pers</ta>
            <ta e="T149" id="Seg_1751" s="T148">dem</ta>
            <ta e="T150" id="Seg_1752" s="T149">v-v&gt;v-n-n&gt;v-v:ins-v:pn</ta>
            <ta e="T151" id="Seg_1753" s="T150">dem</ta>
            <ta e="T152" id="Seg_1754" s="T151">n-n:case</ta>
            <ta e="T153" id="Seg_1755" s="T152">ptcl</ta>
            <ta e="T154" id="Seg_1756" s="T153">v-v:ins-v:pn</ta>
            <ta e="T155" id="Seg_1757" s="T154">adj</ta>
            <ta e="T156" id="Seg_1758" s="T155">%%</ta>
            <ta e="T157" id="Seg_1759" s="T156">%%</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1760" s="T1">num</ta>
            <ta e="T3" id="Seg_1761" s="T2">n</ta>
            <ta e="T4" id="Seg_1762" s="T3">n</ta>
            <ta e="T5" id="Seg_1763" s="T4">v</ta>
            <ta e="T6" id="Seg_1764" s="T5">n</ta>
            <ta e="T7" id="Seg_1765" s="T6">n</ta>
            <ta e="T8" id="Seg_1766" s="T7">n</ta>
            <ta e="T9" id="Seg_1767" s="T8">v</ta>
            <ta e="T10" id="Seg_1768" s="T9">n</ta>
            <ta e="T11" id="Seg_1769" s="T10">adv</ta>
            <ta e="T12" id="Seg_1770" s="T11">v</ta>
            <ta e="T13" id="Seg_1771" s="T12">v</ta>
            <ta e="T14" id="Seg_1772" s="T13">num</ta>
            <ta e="T15" id="Seg_1773" s="T14">n</ta>
            <ta e="T16" id="Seg_1774" s="T15">n</ta>
            <ta e="T17" id="Seg_1775" s="T16">adv</ta>
            <ta e="T18" id="Seg_1776" s="T17">v</ta>
            <ta e="T19" id="Seg_1777" s="T18">adv</ta>
            <ta e="T20" id="Seg_1778" s="T19">v</ta>
            <ta e="T21" id="Seg_1779" s="T20">dem</ta>
            <ta e="T22" id="Seg_1780" s="T21">n</ta>
            <ta e="T23" id="Seg_1781" s="T22">adv</ta>
            <ta e="T24" id="Seg_1782" s="T23">v</ta>
            <ta e="T26" id="Seg_1783" s="T25">adv</ta>
            <ta e="T27" id="Seg_1784" s="T26">v</ta>
            <ta e="T28" id="Seg_1785" s="T27">pers</ta>
            <ta e="T30" id="Seg_1786" s="T29">emphpro</ta>
            <ta e="T31" id="Seg_1787" s="T30">emphpro</ta>
            <ta e="T32" id="Seg_1788" s="T31">v</ta>
            <ta e="T33" id="Seg_1789" s="T32">pers</ta>
            <ta e="T34" id="Seg_1790" s="T33">n</ta>
            <ta e="T35" id="Seg_1791" s="T34">ptcl</ta>
            <ta e="T36" id="Seg_1792" s="T35">v</ta>
            <ta e="T37" id="Seg_1793" s="T36">adv</ta>
            <ta e="T38" id="Seg_1794" s="T37">v</ta>
            <ta e="T39" id="Seg_1795" s="T38">n</ta>
            <ta e="T40" id="Seg_1796" s="T39">adv</ta>
            <ta e="T41" id="Seg_1797" s="T40">v</ta>
            <ta e="T42" id="Seg_1798" s="T41">adj</ta>
            <ta e="T43" id="Seg_1799" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_1800" s="T43">emphpro</ta>
            <ta e="T46" id="Seg_1801" s="T45">emphpro</ta>
            <ta e="T47" id="Seg_1802" s="T46">n</ta>
            <ta e="T48" id="Seg_1803" s="T47">v</ta>
            <ta e="T49" id="Seg_1804" s="T48">v</ta>
            <ta e="T50" id="Seg_1805" s="T49">num</ta>
            <ta e="T51" id="Seg_1806" s="T50">n</ta>
            <ta e="T52" id="Seg_1807" s="T51">n</ta>
            <ta e="T53" id="Seg_1808" s="T52">adv</ta>
            <ta e="T54" id="Seg_1809" s="T53">v</ta>
            <ta e="T56" id="Seg_1810" s="T54">n</ta>
            <ta e="T57" id="Seg_1811" s="T56">n</ta>
            <ta e="T58" id="Seg_1812" s="T57">v</ta>
            <ta e="T59" id="Seg_1813" s="T58">adv</ta>
            <ta e="T60" id="Seg_1814" s="T59">n</ta>
            <ta e="T61" id="Seg_1815" s="T60">preverb</ta>
            <ta e="T62" id="Seg_1816" s="T61">v</ta>
            <ta e="T63" id="Seg_1817" s="T62">adv</ta>
            <ta e="T64" id="Seg_1818" s="T63">v</ta>
            <ta e="T65" id="Seg_1819" s="T64">preverb</ta>
            <ta e="T66" id="Seg_1820" s="T65">v</ta>
            <ta e="T67" id="Seg_1821" s="T66">num</ta>
            <ta e="T68" id="Seg_1822" s="T67">n</ta>
            <ta e="T69" id="Seg_1823" s="T68">dem</ta>
            <ta e="T70" id="Seg_1824" s="T69">n</ta>
            <ta e="T71" id="Seg_1825" s="T70">adv</ta>
            <ta e="T72" id="Seg_1826" s="T71">v</ta>
            <ta e="T73" id="Seg_1827" s="T72">interj</ta>
            <ta e="T74" id="Seg_1828" s="T73">n</ta>
            <ta e="T75" id="Seg_1829" s="T74">n</ta>
            <ta e="T76" id="Seg_1830" s="T75">preverb</ta>
            <ta e="T77" id="Seg_1831" s="T76">v</ta>
            <ta e="T78" id="Seg_1832" s="T77">pers</ta>
            <ta e="T79" id="Seg_1833" s="T78">n</ta>
            <ta e="T80" id="Seg_1834" s="T79">n</ta>
            <ta e="T81" id="Seg_1835" s="T80">adv</ta>
            <ta e="T82" id="Seg_1836" s="T81">v</ta>
            <ta e="T83" id="Seg_1837" s="T82">dem</ta>
            <ta e="T84" id="Seg_1838" s="T83">n</ta>
            <ta e="T86" id="Seg_1839" s="T85">n</ta>
            <ta e="T87" id="Seg_1840" s="T86">n</ta>
            <ta e="T88" id="Seg_1841" s="T87">n</ta>
            <ta e="T90" id="Seg_1842" s="T89">adv</ta>
            <ta e="T91" id="Seg_1843" s="T90">preverb</ta>
            <ta e="T92" id="Seg_1844" s="T91">v</ta>
            <ta e="T94" id="Seg_1845" s="T93">v</ta>
            <ta e="T95" id="Seg_1846" s="T94">n</ta>
            <ta e="T96" id="Seg_1847" s="T95">v</ta>
            <ta e="T97" id="Seg_1848" s="T96">n</ta>
            <ta e="T98" id="Seg_1849" s="T97">n</ta>
            <ta e="T99" id="Seg_1850" s="T98">adv</ta>
            <ta e="T101" id="Seg_1851" s="T100">adv</ta>
            <ta e="T102" id="Seg_1852" s="T101">v</ta>
            <ta e="T104" id="Seg_1853" s="T103">n</ta>
            <ta e="T105" id="Seg_1854" s="T104">n</ta>
            <ta e="T106" id="Seg_1855" s="T105">v</ta>
            <ta e="T107" id="Seg_1856" s="T106">ptcl</ta>
            <ta e="T108" id="Seg_1857" s="T107">n</ta>
            <ta e="T109" id="Seg_1858" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_1859" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_1860" s="T110">preverb</ta>
            <ta e="T112" id="Seg_1861" s="T111">v</ta>
            <ta e="T113" id="Seg_1862" s="T112">dem</ta>
            <ta e="T114" id="Seg_1863" s="T113">n</ta>
            <ta e="T115" id="Seg_1864" s="T114">n</ta>
            <ta e="T116" id="Seg_1865" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_1866" s="T116">v</ta>
            <ta e="T118" id="Seg_1867" s="T117">interrog</ta>
            <ta e="T120" id="Seg_1868" s="T119">adv</ta>
            <ta e="T121" id="Seg_1869" s="T120">v</ta>
            <ta e="T122" id="Seg_1870" s="T121">n</ta>
            <ta e="T123" id="Seg_1871" s="T122">n</ta>
            <ta e="T124" id="Seg_1872" s="T123">ptcl</ta>
            <ta e="T125" id="Seg_1873" s="T124">preverb</ta>
            <ta e="T126" id="Seg_1874" s="T125">ptcl</ta>
            <ta e="T127" id="Seg_1875" s="T126">v</ta>
            <ta e="T128" id="Seg_1876" s="T127">n</ta>
            <ta e="T129" id="Seg_1877" s="T128">conj</ta>
            <ta e="T132" id="Seg_1878" s="T131">n</ta>
            <ta e="T133" id="Seg_1879" s="T132">n</ta>
            <ta e="T135" id="Seg_1880" s="T134">n</ta>
            <ta e="T136" id="Seg_1881" s="T135">preverb</ta>
            <ta e="T137" id="Seg_1882" s="T136">v</ta>
            <ta e="T138" id="Seg_1883" s="T137">v</ta>
            <ta e="T139" id="Seg_1884" s="T138">n</ta>
            <ta e="T140" id="Seg_1885" s="T139">adv</ta>
            <ta e="T141" id="Seg_1886" s="T140">v</ta>
            <ta e="T143" id="Seg_1887" s="T142">pers</ta>
            <ta e="T144" id="Seg_1888" s="T143">v</ta>
            <ta e="T145" id="Seg_1889" s="T144">n</ta>
            <ta e="T146" id="Seg_1890" s="T145">conj</ta>
            <ta e="T147" id="Seg_1891" s="T146">n</ta>
            <ta e="T148" id="Seg_1892" s="T147">pers</ta>
            <ta e="T149" id="Seg_1893" s="T148">dem</ta>
            <ta e="T150" id="Seg_1894" s="T149">v</ta>
            <ta e="T151" id="Seg_1895" s="T150">dem</ta>
            <ta e="T152" id="Seg_1896" s="T151">n</ta>
            <ta e="T153" id="Seg_1897" s="T152">conj</ta>
            <ta e="T154" id="Seg_1898" s="T153">v</ta>
            <ta e="T155" id="Seg_1899" s="T154">adj</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_1900" s="T2">np.h:Th</ta>
            <ta e="T4" id="Seg_1901" s="T3">np:Com</ta>
            <ta e="T7" id="Seg_1902" s="T6">np.h:A</ta>
            <ta e="T8" id="Seg_1903" s="T7">np:G</ta>
            <ta e="T10" id="Seg_1904" s="T9">np.h:Th</ta>
            <ta e="T11" id="Seg_1905" s="T10">adv:L</ta>
            <ta e="T13" id="Seg_1906" s="T12">0.3:Th</ta>
            <ta e="T15" id="Seg_1907" s="T14">np.h:A</ta>
            <ta e="T16" id="Seg_1908" s="T15">np:Com 0.3.h:Poss</ta>
            <ta e="T20" id="Seg_1909" s="T19">0.3.h:A</ta>
            <ta e="T22" id="Seg_1910" s="T21">np.h:A</ta>
            <ta e="T27" id="Seg_1911" s="T26">0.3:Th</ta>
            <ta e="T28" id="Seg_1912" s="T27">pro.h:Th</ta>
            <ta e="T33" id="Seg_1913" s="T32">pro.h:A</ta>
            <ta e="T37" id="Seg_1914" s="T36">adv:Time</ta>
            <ta e="T38" id="Seg_1915" s="T37">0.3.h:A</ta>
            <ta e="T39" id="Seg_1916" s="T38">np.h:A</ta>
            <ta e="T46" id="Seg_1917" s="T45">pro.h:Poss</ta>
            <ta e="T47" id="Seg_1918" s="T46">np.h:Th</ta>
            <ta e="T48" id="Seg_1919" s="T47">0.3.h:A</ta>
            <ta e="T49" id="Seg_1920" s="T48">0.3.h:A</ta>
            <ta e="T51" id="Seg_1921" s="T50">np:Time</ta>
            <ta e="T52" id="Seg_1922" s="T51">np:Th</ta>
            <ta e="T53" id="Seg_1923" s="T52">adv:G</ta>
            <ta e="T57" id="Seg_1924" s="T56">np.h:A</ta>
            <ta e="T59" id="Seg_1925" s="T58">adv:G</ta>
            <ta e="T60" id="Seg_1926" s="T59">np:G</ta>
            <ta e="T62" id="Seg_1927" s="T61">0.3.h:A</ta>
            <ta e="T64" id="Seg_1928" s="T63">np:Th 0.3.h:Poss</ta>
            <ta e="T66" id="Seg_1929" s="T65">0.3.h:A</ta>
            <ta e="T70" id="Seg_1930" s="T69">np.h:A</ta>
            <ta e="T75" id="Seg_1931" s="T74">np:Th 0.2.h:Poss</ta>
            <ta e="T77" id="Seg_1932" s="T76">0.2.h:A</ta>
            <ta e="T78" id="Seg_1933" s="T77">pro.h:A</ta>
            <ta e="T79" id="Seg_1934" s="T78">np.h:Poss</ta>
            <ta e="T80" id="Seg_1935" s="T79">np:Th</ta>
            <ta e="T81" id="Seg_1936" s="T80">adv:G</ta>
            <ta e="T84" id="Seg_1937" s="T83">np.h:A</ta>
            <ta e="T87" id="Seg_1938" s="T86">np.h:Th 0.3.h:Poss</ta>
            <ta e="T88" id="Seg_1939" s="T87">np:G 0.3.h:Poss</ta>
            <ta e="T90" id="Seg_1940" s="T89">adv:G</ta>
            <ta e="T92" id="Seg_1941" s="T91">0.3.h:A</ta>
            <ta e="T94" id="Seg_1942" s="T93">0.3.h:A</ta>
            <ta e="T96" id="Seg_1943" s="T95">0.3.h:A</ta>
            <ta e="T97" id="Seg_1944" s="T96">np:G 0.3.h:Poss</ta>
            <ta e="T98" id="Seg_1945" s="T97">np:G 0.3.h:Poss</ta>
            <ta e="T99" id="Seg_1946" s="T98">0.3.h:A</ta>
            <ta e="T102" id="Seg_1947" s="T101">0.3.h:A</ta>
            <ta e="T105" id="Seg_1948" s="T104">np.h:A</ta>
            <ta e="T108" id="Seg_1949" s="T107">np.h:P 0.2.h:Poss</ta>
            <ta e="T112" id="Seg_1950" s="T111">0.3.h:A</ta>
            <ta e="T114" id="Seg_1951" s="T113">np.h:Poss</ta>
            <ta e="T115" id="Seg_1952" s="T114">np.h:E</ta>
            <ta e="T120" id="Seg_1953" s="T119">adv:G</ta>
            <ta e="T121" id="Seg_1954" s="T120">0.3.h:A</ta>
            <ta e="T123" id="Seg_1955" s="T122">np.h:A</ta>
            <ta e="T127" id="Seg_1956" s="T126">0.3.h:P</ta>
            <ta e="T128" id="Seg_1957" s="T127">np.h:Poss</ta>
            <ta e="T132" id="Seg_1958" s="T131">np.h:Poss</ta>
            <ta e="T133" id="Seg_1959" s="T132">np:Th</ta>
            <ta e="T135" id="Seg_1960" s="T134">np:G</ta>
            <ta e="T137" id="Seg_1961" s="T136">0.3.h:A</ta>
            <ta e="T138" id="Seg_1962" s="T137">0.3.h:A</ta>
            <ta e="T139" id="Seg_1963" s="T138">np.h:A</ta>
            <ta e="T143" id="Seg_1964" s="T142">pro.h:A</ta>
            <ta e="T145" id="Seg_1965" s="T144">np.h:A 0.1.h:Poss</ta>
            <ta e="T147" id="Seg_1966" s="T146">np.h:A 0.1.h:Poss</ta>
            <ta e="T148" id="Seg_1967" s="T147">pro.h:Th</ta>
            <ta e="T152" id="Seg_1968" s="T151">np:G</ta>
            <ta e="T154" id="Seg_1969" s="T153">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_1970" s="T2">np.h:S</ta>
            <ta e="T5" id="Seg_1971" s="T4">v:pred</ta>
            <ta e="T7" id="Seg_1972" s="T6">np.h:S</ta>
            <ta e="T9" id="Seg_1973" s="T8">v:pred</ta>
            <ta e="T10" id="Seg_1974" s="T9">np.h:S</ta>
            <ta e="T12" id="Seg_1975" s="T11">v:pred</ta>
            <ta e="T13" id="Seg_1976" s="T12">0.3:S v:pred</ta>
            <ta e="T15" id="Seg_1977" s="T14">np.h:S</ta>
            <ta e="T18" id="Seg_1978" s="T17">v:pred</ta>
            <ta e="T20" id="Seg_1979" s="T19">0.3.h:S v:pred</ta>
            <ta e="T22" id="Seg_1980" s="T21">np.h:S</ta>
            <ta e="T24" id="Seg_1981" s="T23">v:pred</ta>
            <ta e="T27" id="Seg_1982" s="T26">0.3:S v:pred</ta>
            <ta e="T28" id="Seg_1983" s="T27">pro.h:S</ta>
            <ta e="T32" id="Seg_1984" s="T31">v:pred</ta>
            <ta e="T33" id="Seg_1985" s="T32">pro.h:S</ta>
            <ta e="T36" id="Seg_1986" s="T35">v:pred</ta>
            <ta e="T38" id="Seg_1987" s="T37">0.3.h:S v:pred</ta>
            <ta e="T39" id="Seg_1988" s="T38">np.h:S</ta>
            <ta e="T41" id="Seg_1989" s="T40">v:pred</ta>
            <ta e="T47" id="Seg_1990" s="T46">np.h:O</ta>
            <ta e="T48" id="Seg_1991" s="T47">0.3.h:S v:pred</ta>
            <ta e="T49" id="Seg_1992" s="T48">0.3.h:S v:pred</ta>
            <ta e="T54" id="Seg_1993" s="T51">s:temp</ta>
            <ta e="T57" id="Seg_1994" s="T56">np.h:S</ta>
            <ta e="T58" id="Seg_1995" s="T57">v:pred</ta>
            <ta e="T62" id="Seg_1996" s="T61">0.3.h:S v:pred</ta>
            <ta e="T66" id="Seg_1997" s="T63">s:temp</ta>
            <ta e="T70" id="Seg_1998" s="T69">np.h:S</ta>
            <ta e="T72" id="Seg_1999" s="T71">v:pred</ta>
            <ta e="T75" id="Seg_2000" s="T74">np:O</ta>
            <ta e="T77" id="Seg_2001" s="T76">0.2.h:S v:pred</ta>
            <ta e="T78" id="Seg_2002" s="T77">pro.h:S</ta>
            <ta e="T80" id="Seg_2003" s="T79">np:O</ta>
            <ta e="T82" id="Seg_2004" s="T81">v:pred</ta>
            <ta e="T84" id="Seg_2005" s="T83">np.h:S</ta>
            <ta e="T87" id="Seg_2006" s="T86">np.h:O</ta>
            <ta e="T92" id="Seg_2007" s="T91">0.3.h:S v:pred</ta>
            <ta e="T94" id="Seg_2008" s="T93">s:temp</ta>
            <ta e="T96" id="Seg_2009" s="T95">0.3.h:S v:pred</ta>
            <ta e="T99" id="Seg_2010" s="T97">s:temp</ta>
            <ta e="T102" id="Seg_2011" s="T101">0.3.h:S v:pred</ta>
            <ta e="T105" id="Seg_2012" s="T104">np.h:S</ta>
            <ta e="T106" id="Seg_2013" s="T105">v:pred</ta>
            <ta e="T108" id="Seg_2014" s="T107">np.h:O</ta>
            <ta e="T112" id="Seg_2015" s="T111">0.3.h:S v:pred</ta>
            <ta e="T115" id="Seg_2016" s="T114">np.h:S</ta>
            <ta e="T117" id="Seg_2017" s="T116">v:pred</ta>
            <ta e="T121" id="Seg_2018" s="T120">0.3.h:S v:pred</ta>
            <ta e="T123" id="Seg_2019" s="T122">np.h:S</ta>
            <ta e="T127" id="Seg_2020" s="T126">v:pred 0.3.h:O</ta>
            <ta e="T133" id="Seg_2021" s="T132">np:O</ta>
            <ta e="T137" id="Seg_2022" s="T136">0.3.h:S v:pred</ta>
            <ta e="T138" id="Seg_2023" s="T137">0.3.h:S v:pred</ta>
            <ta e="T139" id="Seg_2024" s="T138">np.h:S</ta>
            <ta e="T141" id="Seg_2025" s="T140">v:pred</ta>
            <ta e="T143" id="Seg_2026" s="T142">pro.h:S</ta>
            <ta e="T144" id="Seg_2027" s="T143">v:pred</ta>
            <ta e="T145" id="Seg_2028" s="T144">np.h:S</ta>
            <ta e="T147" id="Seg_2029" s="T146">np.h:S</ta>
            <ta e="T148" id="Seg_2030" s="T147">pro.h:O</ta>
            <ta e="T150" id="Seg_2031" s="T149">v:pred</ta>
            <ta e="T154" id="Seg_2032" s="T153">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T26" id="Seg_2033" s="T25">RUS:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T55" id="Seg_2034" s="T0">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T55" id="Seg_2035" s="T0">[KuAI:] Фрося Ирикова, сказка.</ta>
            <ta e="T5" id="Seg_2036" s="T55">[IF:] Жили двое мужчин с жёнами.</ta>
            <ta e="T9" id="Seg_2037" s="T5">Мужья в лес вдвоем ушли.</ta>
            <ta e="T12" id="Seg_2038" s="T9">Две женщины дома остались.</ta>
            <ta e="T13" id="Seg_2039" s="T12">Стемнело.</ta>
            <ta e="T20" id="Seg_2040" s="T13">Одна женщина так [сильно] заигралась со своим малышом, так сильно заигралась.</ta>
            <ta e="T24" id="Seg_2041" s="T20">Та женщина так говорит.</ta>
            <ta e="T25" id="Seg_2042" s="T24">(Успокойтесь, тихо себя ведите ?).</ta>
            <ta e="T32" id="Seg_2043" s="T25">Уже стемнело, ведь мы одни остались.</ta>
            <ta e="T36" id="Seg_2044" s="T32">Она, женщина, не слушается.</ta>
            <ta e="T38" id="Seg_2045" s="T36">Всё продолжает баловаться.</ta>
            <ta e="T41" id="Seg_2046" s="T38">Женщина снова говорит.</ta>
            <ta e="T42" id="Seg_2047" s="T41">“Потише”. [?]</ta>
            <ta e="T49" id="Seg_2048" s="T42">Как бы своего ребенка пеленает, старается.</ta>
            <ta e="T59" id="Seg_2049" s="T49">Однажды дверь чума упала и чёрт зашел.</ta>
            <ta e="T66" id="Seg_2050" s="T59">На землю/пол(?) сел, ноги в стороны раскинув.</ta>
            <ta e="T72" id="Seg_2051" s="T66">Одна женщина, та женщина так говорит.</ta>
            <ta e="T82" id="Seg_2052" s="T72">“Ой, дедушка, подвинься [=ноги убери], я у внука пойду вынесу труху”.</ta>
            <ta e="T92" id="Seg_2053" s="T82">Эта женщина своего сына-ребеночка (на подол?) (положила?) и на улицу выскочила.</ta>
            <ta e="T97" id="Seg_2054" s="T92">Выскочив, бегом побежала к своим мужикам.</ta>
            <ta e="T102" id="Seg_2055" s="T97">Придя к мужикам, так говорит.</ta>
            <ta e="T106" id="Seg_2056" s="T102">“Чёрт-старик пришел.</ta>
            <ta e="T112" id="Seg_2057" s="T106">Наверное, он жену твою что ли съел”.</ta>
            <ta e="T117" id="Seg_2058" s="T112">Тот женщины муж не (поверил?).</ta>
            <ta e="T119" id="Seg_2059" s="T117">“Зачем (врёшь?)?”</ta>
            <ta e="T121" id="Seg_2060" s="T119">Они обратно стали возвращаться.</ta>
            <ta e="T127" id="Seg_2061" s="T121">Старик-медведь, наверное, их съел.</ta>
            <ta e="T137" id="Seg_2062" s="T127">Детей и жены головы на деревья повесил.</ta>
            <ta e="T141" id="Seg_2063" s="T137">Едут они, мужик так говорит.</ta>
            <ta e="T150" id="Seg_2064" s="T141">“Что ты врала, мои жена и дети меня этот(?) встречают”.</ta>
            <ta e="T157" id="Seg_2065" s="T150">Только до половины доехали: (одни черепки)?.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T55" id="Seg_2066" s="T0">[KuAI:] Frosya Irikova, a fairy tale.</ta>
            <ta e="T5" id="Seg_2067" s="T55">[IF:] There lived two men with their wives.</ta>
            <ta e="T9" id="Seg_2068" s="T5">The two husbands left to the forest.</ta>
            <ta e="T12" id="Seg_2069" s="T9">The two women stayed home.</ta>
            <ta e="T13" id="Seg_2070" s="T12">It got dark.</ta>
            <ta e="T20" id="Seg_2071" s="T13">One woman got so absorbed in playing with her baby, she got so absorbed in playing.</ta>
            <ta e="T24" id="Seg_2072" s="T20">That woman said so.</ta>
            <ta e="T25" id="Seg_2073" s="T24">(Calm down, stay calm ?).</ta>
            <ta e="T32" id="Seg_2074" s="T25">It has already got dark, and we stayed alone.</ta>
            <ta e="T36" id="Seg_2075" s="T32">She doesn't listen, the [other] woman.</ta>
            <ta e="T38" id="Seg_2076" s="T36">She is playing about all the time.</ta>
            <ta e="T41" id="Seg_2077" s="T38">The woman says again.</ta>
            <ta e="T42" id="Seg_2078" s="T41">"[Be] more quite". [?]</ta>
            <ta e="T49" id="Seg_2079" s="T42">As if she is swaddling her child, she is making an effort.</ta>
            <ta e="T59" id="Seg_2080" s="T49">Once the door of the tent fell down and a devil came in.</ta>
            <ta e="T66" id="Seg_2081" s="T59">He sat down to the floor/to the ground(?), having spread his legs in different directions.</ta>
            <ta e="T72" id="Seg_2082" s="T66">One woman, that woman says like this.</ta>
            <ta e="T82" id="Seg_2083" s="T72">"Oh, grandfather, move your legs away, I'll carry outside the dust of my grandson."</ta>
            <ta e="T92" id="Seg_2084" s="T82">This woman (put?) her son, her child (into her skirt?) and ran outside.</ta>
            <ta e="T97" id="Seg_2085" s="T92">Having run out, she ran to her men.</ta>
            <ta e="T102" id="Seg_2086" s="T97">Having come to her men, she said like this.</ta>
            <ta e="T106" id="Seg_2087" s="T102">"A devil old man has come.</ta>
            <ta e="T112" id="Seg_2088" s="T106">Maybe he had eaten your wife."</ta>
            <ta e="T117" id="Seg_2089" s="T112">That woman's husband didn't (believe?) [her].</ta>
            <ta e="T119" id="Seg_2090" s="T117">"Why (are you lying?)?"</ta>
            <ta e="T121" id="Seg_2091" s="T119">They started to return.</ta>
            <ta e="T127" id="Seg_2092" s="T121">An old bear-man had probably eaten them.</ta>
            <ta e="T137" id="Seg_2093" s="T127">He hung on the tree the heads of the wife and of the children.</ta>
            <ta e="T141" id="Seg_2094" s="T137">They are coming, [that] man says like this. </ta>
            <ta e="T150" id="Seg_2095" s="T141">"Why were you lying to me, my wife and my children came out ot meet me."</ta>
            <ta e="T157" id="Seg_2096" s="T150">They made half of the road [and then they saw]: (only skulls)?.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T55" id="Seg_2097" s="T0">[KuAI:] Frosja Irikova, ein Märchen.</ta>
            <ta e="T5" id="Seg_2098" s="T55">[IF:] Es lebten zwei Männer mit ihren Frauen.</ta>
            <ta e="T9" id="Seg_2099" s="T5">Die zwei Männer gingen in den Wald.</ta>
            <ta e="T12" id="Seg_2100" s="T9">Die beiden Frauen blieben zuhause.</ta>
            <ta e="T13" id="Seg_2101" s="T12">Es wurde dunkel.</ta>
            <ta e="T20" id="Seg_2102" s="T13">Eine Frau war so vertieft darin, mit ihrem Baby zu spielen, sie war so vertieft darin zu spielen.</ta>
            <ta e="T24" id="Seg_2103" s="T20">Jene Frau sagte so:</ta>
            <ta e="T25" id="Seg_2104" s="T24">"(Komm runter, bleib ruhig ?)".</ta>
            <ta e="T32" id="Seg_2105" s="T25">Es ist schon dunkel geworden und wir sind alleine geblieben.</ta>
            <ta e="T36" id="Seg_2106" s="T32">Sie hört nicht zu, die Frau.</ta>
            <ta e="T38" id="Seg_2107" s="T36">Sie spielt die ganze Zeit herum.</ta>
            <ta e="T41" id="Seg_2108" s="T38">Die Frau sagt wieder:</ta>
            <ta e="T42" id="Seg_2109" s="T41">"Leiser." [?]</ta>
            <ta e="T49" id="Seg_2110" s="T42">Sie bemüht sich, als ob sie ihr Kind wickelt.</ta>
            <ta e="T59" id="Seg_2111" s="T49">Einmal fiel die Tür des Zeltes herunter und ein Teufel kam herein.</ta>
            <ta e="T66" id="Seg_2112" s="T59">Er setzte sich auf den Boden/auf die Erde(?) und streckte seine Beine in unterschiedliche Richtungen.</ta>
            <ta e="T72" id="Seg_2113" s="T66">Eine Frau, diese Frau sagt so:</ta>
            <ta e="T82" id="Seg_2114" s="T72">"Ach, Großvater, nimm deine Beine weg, ich trage den Mist meines Enkels hinaus."</ta>
            <ta e="T92" id="Seg_2115" s="T82">Diese Frau (legte?) ihren Sohn, ihr Kind (in ihren Rock?) und rannte nach draußen.</ta>
            <ta e="T97" id="Seg_2116" s="T92">Sie sprang nach draußen und rannte zu ihren Männern.</ta>
            <ta e="T102" id="Seg_2117" s="T97">Sie kommt zu ihren Männern und sagt so:</ta>
            <ta e="T106" id="Seg_2118" s="T102">"Ein alter Teufel ist gekommen.</ta>
            <ta e="T112" id="Seg_2119" s="T106">Vielleicht hat er deine Frau gegessen."</ta>
            <ta e="T117" id="Seg_2120" s="T112">Der Mann dieser Frau (glaubte?) [ihr] nicht.</ta>
            <ta e="T119" id="Seg_2121" s="T117">"Warum (lügst du?)?"</ta>
            <ta e="T121" id="Seg_2122" s="T119">Sie machten sich auf den Weg zurück.</ta>
            <ta e="T127" id="Seg_2123" s="T121">Der alte Bär-Mann, hat sie wahrscheinlich gefressen.</ta>
            <ta e="T137" id="Seg_2124" s="T127">Er hängte die Köpfe der Frau und der Kinder an einen Baum.</ta>
            <ta e="T141" id="Seg_2125" s="T137">Sie kommen, der Mann sagt so:</ta>
            <ta e="T150" id="Seg_2126" s="T141">"Warum hast du mich angelogen, meine Frau und meine Kinder kamen hinaus, um mich zu begrüßen."</ta>
            <ta e="T157" id="Seg_2127" s="T150">Sie gingen halb hin [und sahen]: (nur Schädel)?.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_2128" s="T55">Жили две замужние женщины</ta>
            <ta e="T9" id="Seg_2129" s="T5">Двое мужей в лес ушли.</ta>
            <ta e="T12" id="Seg_2130" s="T9">Женщины дома остались</ta>
            <ta e="T13" id="Seg_2131" s="T12">затемнело</ta>
            <ta e="T20" id="Seg_2132" s="T13">одна женщина так(сильно) заигралась со своим малышом</ta>
            <ta e="T24" id="Seg_2133" s="T20">та женщина так говорит </ta>
            <ta e="T25" id="Seg_2134" s="T24">успокойтесь? тихо себя ведите</ta>
            <ta e="T32" id="Seg_2135" s="T25">уже стемнело,ведь мы одни остались</ta>
            <ta e="T36" id="Seg_2136" s="T32">та женщина не слушается, не обращает внимания</ta>
            <ta e="T38" id="Seg_2137" s="T36">все продолжает играть</ta>
            <ta e="T41" id="Seg_2138" s="T38">женщина опять повторяет</ta>
            <ta e="T42" id="Seg_2139" s="T41">потише (обращается к той,которая с ребенком)</ta>
            <ta e="T49" id="Seg_2140" s="T42">как бы своего ребенка пеленает</ta>
            <ta e="T59" id="Seg_2141" s="T49">однажды на спину откинувшись сел. вот мужик - медведь зашел</ta>
            <ta e="T66" id="Seg_2142" s="T59">на пол сел,раскинув ноги</ta>
            <ta e="T72" id="Seg_2143" s="T66">та женщина говорит</ta>
            <ta e="T82" id="Seg_2144" s="T72">ой дедушка подвинься (ноги убери), у внука пойду на улицу вынесу труху (а сама ребенка положила)</ta>
            <ta e="T92" id="Seg_2145" s="T82">эта женщина на подол положила ребенка и вышла </ta>
            <ta e="T97" id="Seg_2146" s="T92">вышла, бегом побежала к своим мужикам</ta>
            <ta e="T102" id="Seg_2147" s="T97">придя к мужикам так говорит</ta>
            <ta e="T106" id="Seg_2148" s="T102">черт пришел</ta>
            <ta e="T112" id="Seg_2149" s="T106">наверное он жену твою съел</ta>
            <ta e="T117" id="Seg_2150" s="T112">тот женщины муж не поверил</ta>
            <ta e="T119" id="Seg_2151" s="T117">зачем (врешь?) </ta>
            <ta e="T121" id="Seg_2152" s="T119">обратно возвращается</ta>
            <ta e="T127" id="Seg_2153" s="T121">медведь, наверное,их съел</ta>
            <ta e="T137" id="Seg_2154" s="T127"> детей и материны головы на деревья повесил</ta>
            <ta e="T141" id="Seg_2155" s="T137">едут они,мужик говорит</ta>
            <ta e="T150" id="Seg_2156" s="T141">что врешь?, меня жена и дети встречают</ta>
            <ta e="T157" id="Seg_2157" s="T150">только подъехали (до половины) и видят: одни черепки</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T32" id="Seg_2158" s="T25">[BrM:] Maybe: ošontɨ - 'very'. Tentative analysis of pɛlɨkɔːleːŋɔmɨn, it could be two words or something derived from the stem qalɨ- 'to stay'.</ta>
            <ta e="T42" id="Seg_2159" s="T41">[BrM:] DIM? [AAV:] Speaking to the woman with the child.</ta>
            <ta e="T49" id="Seg_2160" s="T42">[AAV:] kurrɛntɨt orɨčʼčʼɨt ?</ta>
            <ta e="T59" id="Seg_2161" s="T49">[AAV:] mɔːta ‘дверь, представляющая собой полу чума, которая откидывается на нюк’</ta>
            <ta e="T82" id="Seg_2162" s="T72">[KSN:] But she put her child instead. [BrM:] Unclear form 'toː loqɨ'.</ta>
            <ta e="T92" id="Seg_2163" s="T82">[BrM:] Unknown stem 'keːra', cf. 'kernʼa' - 'scrap'.</ta>
            <ta e="T97" id="Seg_2164" s="T92">[AAV:] ponɨt sačʼčʼɨmɔːlla ?</ta>
            <ta e="T106" id="Seg_2165" s="T102">[AAV:] (?) mɔqɨti ? tüːsɨn ?</ta>
            <ta e="T112" id="Seg_2166" s="T106">[AAV:] tam imantə qəp?</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T55" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
