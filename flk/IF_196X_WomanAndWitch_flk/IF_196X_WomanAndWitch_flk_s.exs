<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>IF_196X_WomanAndWitch_flk</transcription-name>
         <referenced-file url="IF_196X_WomanAndWitch_flk.wav" />
         <referenced-file url="IF_196X_WomanAndWitch_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">IF_196X_WomanAndWitch_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">232</ud-information>
            <ud-information attribute-name="# HIAT:w">153</ud-information>
            <ud-information attribute-name="# e">152</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">4</ud-information>
            <ud-information attribute-name="# HIAT:u">27</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="IF">
            <abbreviation>IF</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="1.7" type="appl" />
         <tli id="T152" time="3.145" type="intp" />
         <tli id="T1" time="4.59" type="appl" />
         <tli id="T2" time="7.373" type="appl" />
         <tli id="T3" time="8.287" type="appl" />
         <tli id="T4" time="9.2" type="appl" />
         <tli id="T5" time="10.113" type="appl" />
         <tli id="T6" time="11.027" type="appl" />
         <tli id="T7" time="11.94" type="appl" />
         <tli id="T8" time="14.143" type="appl" />
         <tli id="T9" time="14.847" type="appl" />
         <tli id="T10" time="15.55" type="appl" />
         <tli id="T11" time="16.253" type="appl" />
         <tli id="T12" time="16.957" type="appl" />
         <tli id="T13" time="17.59965499450611" />
         <tli id="T14" time="18.972" type="appl" />
         <tli id="T15" time="19.67" type="appl" />
         <tli id="T16" time="20.369" type="appl" />
         <tli id="T17" time="21.067" type="appl" />
         <tli id="T18" time="21.765" type="appl" />
         <tli id="T19" time="22.943" type="appl" />
         <tli id="T20" time="23.62" type="appl" />
         <tli id="T21" time="24.298" type="appl" />
         <tli id="T22" time="24.975" type="appl" />
         <tli id="T23" time="25.653" type="appl" />
         <tli id="T24" time="26.33" type="appl" />
         <tli id="T25" time="27.008" type="appl" />
         <tli id="T26" time="27.967" type="appl" />
         <tli id="T27" time="28.884" type="appl" />
         <tli id="T28" time="29.802" type="appl" />
         <tli id="T29" time="30.72" type="appl" />
         <tli id="T30" time="31.638" type="appl" />
         <tli id="T31" time="32.555" type="appl" />
         <tli id="T32" time="33.473" type="appl" />
         <tli id="T33" time="34.391" type="appl" />
         <tli id="T34" time="35.704" type="appl" />
         <tli id="T35" time="36.841" type="appl" />
         <tli id="T36" time="38.482" type="appl" />
         <tli id="T37" time="40.26" type="appl" />
         <tli id="T38" time="41.02" type="appl" />
         <tli id="T39" time="41.781" type="appl" />
         <tli id="T40" time="42.542" type="appl" />
         <tli id="T41" time="43.303" type="appl" />
         <tli id="T42" time="44.063" type="appl" />
         <tli id="T43" time="44.824" type="appl" />
         <tli id="T44" time="46.83" type="appl" />
         <tli id="T45" time="48.253" type="appl" />
         <tli id="T46" time="49.676" type="appl" />
         <tli id="T47" time="51.099" type="appl" />
         <tli id="T48" time="52.574" type="appl" />
         <tli id="T49" time="53.233" type="appl" />
         <tli id="T50" time="53.891" type="appl" />
         <tli id="T51" time="55.576" type="appl" />
         <tli id="T52" time="56.02" type="appl" />
         <tli id="T53" time="56.464" type="appl" />
         <tli id="T54" time="56.908" type="appl" />
         <tli id="T55" time="57.352" type="appl" />
         <tli id="T56" time="57.797" type="appl" />
         <tli id="T57" time="58.241" type="appl" />
         <tli id="T58" time="58.685" type="appl" />
         <tli id="T59" time="59.129" type="appl" />
         <tli id="T60" time="59.573" type="appl" />
         <tli id="T61" time="61.649" type="appl" />
         <tli id="T62" time="62.591" type="appl" />
         <tli id="T63" time="63.029" type="appl" />
         <tli id="T64" time="63.468" type="appl" />
         <tli id="T65" time="63.906" type="appl" />
         <tli id="T66" time="64.344" type="appl" />
         <tli id="T67" time="64.783" type="appl" />
         <tli id="T68" time="65.221" type="appl" />
         <tli id="T69" time="65.66" type="appl" />
         <tli id="T70" time="66.098" type="appl" />
         <tli id="T71" time="67.254" type="appl" />
         <tli id="T72" time="67.943" type="appl" />
         <tli id="T73" time="68.632" type="appl" />
         <tli id="T74" time="69.473" type="appl" />
         <tli id="T75" time="70.123" type="appl" />
         <tli id="T76" time="70.773" type="appl" />
         <tli id="T77" time="71.423" type="appl" />
         <tli id="T78" time="72.074" type="appl" />
         <tli id="T79" time="72.724" type="appl" />
         <tli id="T80" time="73.374" type="appl" />
         <tli id="T81" time="74.024" type="appl" />
         <tli id="T82" time="75.104" type="appl" />
         <tli id="T83" time="75.643" type="appl" />
         <tli id="T84" time="76.183" type="appl" />
         <tli id="T85" time="76.723" type="appl" />
         <tli id="T86" time="77.263" type="appl" />
         <tli id="T87" time="77.803" type="appl" />
         <tli id="T88" time="78.342" type="appl" />
         <tli id="T89" time="78.882" type="appl" />
         <tli id="T153" time="79.5985" type="intp" />
         <tli id="T90" time="80.315" type="appl" />
         <tli id="T91" time="81.176" type="appl" />
         <tli id="T92" time="81.913" type="appl" />
         <tli id="T93" time="82.649" type="appl" />
         <tli id="T94" time="83.385" type="appl" />
         <tli id="T95" time="84.122" type="appl" />
         <tli id="T96" time="84.858" type="appl" />
         <tli id="T97" time="85.659" type="appl" />
         <tli id="T98" time="86.303" type="appl" />
         <tli id="T99" time="86.947" type="appl" />
         <tli id="T100" time="87.592" type="appl" />
         <tli id="T101" time="88.236" type="appl" />
         <tli id="T102" time="88.88" type="appl" />
         <tli id="T103" time="89.524" type="appl" />
         <tli id="T104" time="91.187" type="appl" />
         <tli id="T105" time="92.042" type="appl" />
         <tli id="T106" time="92.897" type="appl" />
         <tli id="T107" time="93.752" type="appl" />
         <tli id="T108" time="94.607" type="appl" />
         <tli id="T109" time="95.474" type="appl" />
         <tli id="T110" time="95.934" type="appl" />
         <tli id="T111" time="96.393" type="appl" />
         <tli id="T112" time="96.852" type="appl" />
         <tli id="T113" time="97.312" type="appl" />
         <tli id="T114" time="97.771" type="appl" />
         <tli id="T115" time="98.231" type="appl" />
         <tli id="T116" time="98.69" type="appl" />
         <tli id="T117" time="99.731" type="appl" />
         <tli id="T118" time="100.24" type="appl" />
         <tli id="T119" time="100.748" type="appl" />
         <tli id="T120" time="101.257" type="appl" />
         <tli id="T121" time="101.765" type="appl" />
         <tli id="T122" time="102.274" type="appl" />
         <tli id="T123" time="102.782" type="appl" />
         <tli id="T124" time="103.2" />
         <tli id="T125" time="103.8" />
         <tli id="T126" time="104.1" />
         <tli id="T127" time="104.8" />
         <tli id="T128" time="105.3" />
         <tli id="T129" time="105.8" />
         <tli id="T130" time="106.3" />
         <tli id="T131" time="106.8" />
         <tli id="T132" time="107.3" />
         <tli id="T133" time="107.8" />
         <tli id="T134" time="108.3" />
         <tli id="T135" time="110.232" />
         <tli id="T136" time="110.682" type="appl" />
         <tli id="T137" time="111.132" type="appl" />
         <tli id="T138" time="111.582" type="appl" />
         <tli id="T139" time="112.032" type="appl" />
         <tli id="T140" time="112.482" type="appl" />
         <tli id="T141" time="112.932" type="appl" />
         <tli id="T142" time="113.382" type="appl" />
         <tli id="T143" time="113.832" type="appl" />
         <tli id="T144" time="114.299" type="appl" />
         <tli id="T145" time="114.633" type="appl" />
         <tli id="T146" time="114.968" type="appl" />
         <tli id="T147" time="115.302" type="appl" />
         <tli id="T148" time="115.637" type="appl" />
         <tli id="T149" time="115.971" type="appl" />
         <tli id="T150" time="116.306" type="appl" />
         <tli id="T151" time="118.31" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="IF"
                      type="t">
         <timeline-fork end="T152" start="T0">
            <tli id="T0.tx.1" />
            <tli id="T0.tx.2" />
            <tli id="T0.tx.3" />
         </timeline-fork>
         <timeline-fork end="T153" start="T89">
            <tli id="T89.tx.1" />
            <tli id="T89.tx.2" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T150" id="Seg_0" n="sc" s="T0">
               <ts e="T0.tx.3" id="Seg_2" n="HIAT:u" s="T0">
                  <nts id="Seg_3" n="HIAT:ip">(</nts>
                  <nts id="Seg_4" n="HIAT:ip">(</nts>
                  <ats e="T0.tx.1" id="Seg_5" n="HIAT:non-pho" s="T0">KuAI:</ats>
                  <nts id="Seg_6" n="HIAT:ip">)</nts>
                  <nts id="Seg_7" n="HIAT:ip">)</nts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx.2" id="Seg_10" n="HIAT:w" s="T0.tx.1">Фрося</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx.3" id="Seg_13" n="HIAT:w" s="T0.tx.2">Ирикова</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_17" n="HIAT:u" s="T0.tx.3">
                  <ts e="T152" id="Seg_19" n="HIAT:w" s="T0.tx.3">Сказка</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_23" n="HIAT:u" s="T152">
                  <nts id="Seg_24" n="HIAT:ip">(</nts>
                  <nts id="Seg_25" n="HIAT:ip">(</nts>
                  <ats e="T1" id="Seg_26" n="HIAT:non-pho" s="T152">IF:</ats>
                  <nts id="Seg_27" n="HIAT:ip">)</nts>
                  <nts id="Seg_28" n="HIAT:ip">)</nts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_31" n="HIAT:w" s="T1">Ukkɨr</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_34" n="HIAT:w" s="T2">mɔːtqɨn</ts>
                  <nts id="Seg_35" n="HIAT:ip">,</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_38" n="HIAT:w" s="T3">ukkɨr</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_41" n="HIAT:w" s="T4">mɔːtqɨt</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_44" n="HIAT:w" s="T5">qumɨn</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_47" n="HIAT:w" s="T6">ilɨmmɨntɔːtɨn</ts>
                  <nts id="Seg_48" n="HIAT:ip">.</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_51" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_53" n="HIAT:w" s="T7">Nɨmtɨ</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_56" n="HIAT:w" s="T8">na</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_59" n="HIAT:w" s="T9">mɔːtqɨntɨ</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_62" n="HIAT:w" s="T10">nılʼčʼi</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_65" n="HIAT:w" s="T11">ima</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_68" n="HIAT:w" s="T12">ɛːppɨntɨ</ts>
                  <nts id="Seg_69" n="HIAT:ip">.</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_72" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_74" n="HIAT:w" s="T13">Qumɨn</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_77" n="HIAT:w" s="T14">lɨpkɨmɔːlla</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_80" n="HIAT:w" s="T15">muntɨ</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_83" n="HIAT:w" s="T16">ılla</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_86" n="HIAT:w" s="T17">qontɔːtɨn</ts>
                  <nts id="Seg_87" n="HIAT:ip">.</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_90" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_92" n="HIAT:w" s="T18">Na</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_95" n="HIAT:w" s="T19">ima</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_98" n="HIAT:w" s="T20">čʼeːlʼin</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_101" n="HIAT:w" s="T21">ašša</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_103" n="HIAT:ip">(</nts>
                  <ts e="T23" id="Seg_105" n="HIAT:w" s="T22">qo=</ts>
                  <nts id="Seg_106" n="HIAT:ip">)</nts>
                  <nts id="Seg_107" n="HIAT:ip">,</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_110" n="HIAT:w" s="T23">čʼeːlʼin</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_113" n="HIAT:w" s="T24">qontɨqɨjojimpa</ts>
                  <nts id="Seg_114" n="HIAT:ip">.</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_117" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_119" n="HIAT:w" s="T25">A</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_122" n="HIAT:w" s="T26">lipkɨqɨn</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_125" n="HIAT:w" s="T27">qumɨt</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_128" n="HIAT:w" s="T28">qontallɛːptäqɨn</ts>
                  <nts id="Seg_129" n="HIAT:ip">,</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_132" n="HIAT:w" s="T29">qumɨt</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_135" n="HIAT:w" s="T30">qontallɛːptäqɨntɨ</ts>
                  <nts id="Seg_136" n="HIAT:ip">,</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_139" n="HIAT:w" s="T31">nɔːt</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_142" n="HIAT:w" s="T32">čʼittɨmpɨqolamqɨjojimpa</ts>
                  <nts id="Seg_143" n="HIAT:ip">.</nts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_146" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_148" n="HIAT:w" s="T33">Amɨrqɨjojimpɨqo</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_151" n="HIAT:w" s="T34">olampa</ts>
                  <nts id="Seg_152" n="HIAT:ip">.</nts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_155" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_157" n="HIAT:w" s="T35">Šütɨran</ts>
                  <nts id="Seg_158" n="HIAT:ip">.</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_161" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_163" n="HIAT:w" s="T36">Meːltɨqam</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_166" n="HIAT:w" s="T37">nıŋ</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_169" n="HIAT:w" s="T38">orɨnʼnʼa</ts>
                  <nts id="Seg_170" n="HIAT:ip">,</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_173" n="HIAT:w" s="T39">čʼeːlɨt</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_176" n="HIAT:w" s="T40">qonta</ts>
                  <nts id="Seg_177" n="HIAT:ip">,</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_180" n="HIAT:w" s="T41">lipkɨqɨn</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_183" n="HIAT:w" s="T42">uːčʼa</ts>
                  <nts id="Seg_184" n="HIAT:ip">.</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_187" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_189" n="HIAT:w" s="T43">Ukkɨr</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_192" n="HIAT:w" s="T44">čʼontoːqɨn</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_195" n="HIAT:w" s="T45">omtane</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_198" n="HIAT:w" s="T46">šütɨra</ts>
                  <nts id="Seg_199" n="HIAT:ip">.</nts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_202" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_204" n="HIAT:w" s="T47">Pɨrnʼi</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_207" n="HIAT:w" s="T48">mɔːt</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_210" n="HIAT:w" s="T49">šeːrɨ</ts>
                  <nts id="Seg_211" n="HIAT:ip">.</nts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_214" n="HIAT:u" s="T50">
                  <nts id="Seg_215" n="HIAT:ip">(</nts>
                  <ts e="T51" id="Seg_217" n="HIAT:w" s="T50">Pɨrnʼi=</ts>
                  <nts id="Seg_218" n="HIAT:ip">)</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_221" n="HIAT:w" s="T51">pɨrnʼi</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_224" n="HIAT:w" s="T52">mɔːt</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_227" n="HIAT:w" s="T53">šeːrla</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_230" n="HIAT:w" s="T54">nıŋ</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_233" n="HIAT:w" s="T55">orɨnʼnʼa</ts>
                  <nts id="Seg_234" n="HIAT:ip">:</nts>
                  <nts id="Seg_235" n="HIAT:ip">“</nts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_238" n="HIAT:w" s="T56">Tat</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_241" n="HIAT:w" s="T57">qaqa</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_244" n="HIAT:w" s="T58">ašša</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_247" n="HIAT:w" s="T59">qontantɨ</ts>
                  <nts id="Seg_248" n="HIAT:ip">?</nts>
                  <nts id="Seg_249" n="HIAT:ip">”</nts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_252" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_254" n="HIAT:w" s="T60">Ima</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_257" n="HIAT:w" s="T61">qɨčʼčʼɨmɔːtpa</ts>
                  <nts id="Seg_258" n="HIAT:ip">.</nts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_261" n="HIAT:u" s="T62">
                  <nts id="Seg_262" n="HIAT:ip">“</nts>
                  <ts e="T63" id="Seg_264" n="HIAT:w" s="T62">Mat</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_267" n="HIAT:w" s="T63">taštɨ</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_270" n="HIAT:w" s="T64">tıː</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_273" n="HIAT:w" s="T65">inna</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_276" n="HIAT:w" s="T66">amɛntaɣ</ts>
                  <nts id="Seg_277" n="HIAT:ip">,</nts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_280" n="HIAT:w" s="T67">tolʼkə</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_283" n="HIAT:w" s="T68">moqanä</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_286" n="HIAT:w" s="T69">qoːranɛntak</ts>
                  <nts id="Seg_287" n="HIAT:ip">”</nts>
                  <nts id="Seg_288" n="HIAT:ip">.</nts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_291" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_293" n="HIAT:w" s="T70">Pɨrnʼi</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_296" n="HIAT:w" s="T71">moqanä</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_299" n="HIAT:w" s="T72">kurralna</ts>
                  <nts id="Seg_300" n="HIAT:ip">.</nts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_303" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_305" n="HIAT:w" s="T73">Ima</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_308" n="HIAT:w" s="T74">qɨčʼčʼɨmɔːlla</ts>
                  <nts id="Seg_309" n="HIAT:ip">,</nts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_312" n="HIAT:w" s="T75">mɨttɨ</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_315" n="HIAT:w" s="T76">torqɨ</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_318" n="HIAT:w" s="T77">orqɨlla</ts>
                  <nts id="Seg_319" n="HIAT:ip">,</nts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_322" n="HIAT:w" s="T78">qɔːlitɨ</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_325" n="HIAT:w" s="T79">mɨttɨsa</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_328" n="HIAT:w" s="T80">natqɨlɛlʼčʼɨn</ts>
                  <nts id="Seg_329" n="HIAT:ip">.</nts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_332" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_334" n="HIAT:w" s="T81">Na</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_337" n="HIAT:w" s="T82">nık</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_340" n="HIAT:w" s="T83">čʼəkäptɨrla</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_343" n="HIAT:w" s="T84">qumɨtɨt</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_346" n="HIAT:w" s="T85">puːtantɨ</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_349" n="HIAT:w" s="T86">patqɨlla</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_352" n="HIAT:w" s="T87">ılla</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_355" n="HIAT:w" s="T88">qontaja</ts>
                  <nts id="Seg_356" n="HIAT:ip">.</nts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_359" n="HIAT:u" s="T89">
                  <nts id="Seg_360" n="HIAT:ip">(</nts>
                  <nts id="Seg_361" n="HIAT:ip">(</nts>
                  <ats e="T89.tx.1" id="Seg_362" n="HIAT:non-pho" s="T89">KuAI:</ats>
                  <nts id="Seg_363" n="HIAT:ip">)</nts>
                  <nts id="Seg_364" n="HIAT:ip">)</nts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89.tx.2" id="Seg_367" n="HIAT:w" s="T89.tx.1">Ещё</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_370" n="HIAT:w" s="T89.tx.2">медленнее</ts>
                  <nts id="Seg_371" n="HIAT:ip">.</nts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_374" n="HIAT:u" s="T153">
                  <nts id="Seg_375" n="HIAT:ip">(</nts>
                  <nts id="Seg_376" n="HIAT:ip">(</nts>
                  <ats e="T90" id="Seg_377" n="HIAT:non-pho" s="T153">IF:</ats>
                  <nts id="Seg_378" n="HIAT:ip">)</nts>
                  <nts id="Seg_379" n="HIAT:ip">)</nts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_382" n="HIAT:w" s="T90">Pɨrnʼi</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_385" n="HIAT:w" s="T91">čʼap</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_388" n="HIAT:w" s="T92">tüjanɨ</ts>
                  <nts id="Seg_389" n="HIAT:ip">,</nts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_392" n="HIAT:w" s="T93">ima</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_395" n="HIAT:w" s="T94">meːlt</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_398" n="HIAT:w" s="T95">čʼäŋkan</ts>
                  <nts id="Seg_399" n="HIAT:ip">.</nts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_402" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_404" n="HIAT:w" s="T96">Tɨčʼčʼam</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_407" n="HIAT:w" s="T97">mantɛːja</ts>
                  <nts id="Seg_408" n="HIAT:ip">,</nts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_411" n="HIAT:w" s="T98">točʼčʼam</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_414" n="HIAT:w" s="T99">mantɛːja</ts>
                  <nts id="Seg_415" n="HIAT:ip">,</nts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_418" n="HIAT:w" s="T100">ima</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_421" n="HIAT:w" s="T101">meːlt</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_424" n="HIAT:w" s="T102">čʼä</ts>
                  <nts id="Seg_425" n="HIAT:ip">.</nts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_428" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_430" n="HIAT:w" s="T103">Pɨrnʼi</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_433" n="HIAT:w" s="T104">peːqolamnɨt</ts>
                  <nts id="Seg_434" n="HIAT:ip">,</nts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_437" n="HIAT:w" s="T105">qumɨtɨt</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_440" n="HIAT:w" s="T106">qɔːli</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_443" n="HIAT:w" s="T107">paqqɨrɨt</ts>
                  <nts id="Seg_444" n="HIAT:ip">.</nts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_447" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_449" n="HIAT:w" s="T108">Kutɨt</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_452" n="HIAT:w" s="T109">qɔːli</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_455" n="HIAT:w" s="T110">qap</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_458" n="HIAT:w" s="T111">qannɨmmɨntɨ</ts>
                  <nts id="Seg_459" n="HIAT:ip">,</nts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_462" n="HIAT:w" s="T112">značʼit</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_465" n="HIAT:w" s="T113">na</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_468" n="HIAT:w" s="T114">ima</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_471" n="HIAT:w" s="T115">najɛntɨ</ts>
                  <nts id="Seg_472" n="HIAT:ip">.</nts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_475" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_477" n="HIAT:w" s="T116">Kojɨmmɨntɨt</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_480" n="HIAT:w" s="T117">na</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_483" n="HIAT:w" s="T118">paqqɨrɨntɨt</ts>
                  <nts id="Seg_484" n="HIAT:ip">,</nts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_487" n="HIAT:w" s="T119">na</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_490" n="HIAT:w" s="T120">kojaltɨntɨt</ts>
                  <nts id="Seg_491" n="HIAT:ip">,</nts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_494" n="HIAT:w" s="T121">na</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_497" n="HIAT:w" s="T122">kojaltɨntɨt</ts>
                  <nts id="Seg_498" n="HIAT:ip">.</nts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T135" id="Seg_501" n="HIAT:u" s="T123">
                  <nts id="Seg_502" n="HIAT:ip">“</nts>
                  <ts e="T124" id="Seg_504" n="HIAT:w" s="T123">Qumɨn</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_507" n="HIAT:w" s="T124">mun</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_510" n="HIAT:w" s="T125">qɔːlɨjitɨ</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_513" n="HIAT:w" s="T126">pötpa</ts>
                  <nts id="Seg_514" n="HIAT:ip">,</nts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_517" n="HIAT:w" s="T127">naš</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_520" n="HIAT:w" s="T128">čʼaptät</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_523" n="HIAT:w" s="T129">qontɨtɨ</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_525" n="HIAT:ip">(</nts>
                  <ts e="T131" id="Seg_527" n="HIAT:w" s="T130">im-</ts>
                  <nts id="Seg_528" n="HIAT:ip">)</nts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_531" n="HIAT:w" s="T131">qumɨn</ts>
                  <nts id="Seg_532" n="HIAT:ip">”</nts>
                  <nts id="Seg_533" n="HIAT:ip">,</nts>
                  <nts id="Seg_534" n="HIAT:ip">–</nts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_537" n="HIAT:w" s="T132">pɨrnʼi</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_540" n="HIAT:w" s="T133">nık</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_543" n="HIAT:w" s="T134">kətɨŋɨt</ts>
                  <nts id="Seg_544" n="HIAT:ip">.</nts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_547" n="HIAT:u" s="T135">
                  <ts e="T136" id="Seg_549" n="HIAT:w" s="T135">A</ts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_552" n="HIAT:w" s="T136">to</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_555" n="HIAT:w" s="T137">na</ts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_558" n="HIAT:w" s="T138">ima</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_561" n="HIAT:w" s="T139">ippɨla</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_564" n="HIAT:w" s="T140">əːtɨkɔːl</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_567" n="HIAT:w" s="T141">na</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_570" n="HIAT:w" s="T142">taŋaltɨmpa</ts>
                  <nts id="Seg_571" n="HIAT:ip">.</nts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T150" id="Seg_574" n="HIAT:u" s="T143">
                  <ts e="T144" id="Seg_576" n="HIAT:w" s="T143">Pɨrnʼi</ts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_579" n="HIAT:w" s="T144">pona</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_582" n="HIAT:w" s="T145">tarlä</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_585" n="HIAT:w" s="T146">toː</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_588" n="HIAT:w" s="T147">tiː</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_591" n="HIAT:w" s="T148">toː</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_594" n="HIAT:w" s="T149">qənmɨntɨ</ts>
                  <nts id="Seg_595" n="HIAT:ip">.</nts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T150" id="Seg_597" n="sc" s="T0">
               <ts e="T152" id="Seg_599" n="e" s="T0">((KuAI:)) Фрося Ирикова. Сказка. </ts>
               <ts e="T1" id="Seg_601" n="e" s="T152">((IF:)) </ts>
               <ts e="T2" id="Seg_603" n="e" s="T1">Ukkɨr </ts>
               <ts e="T3" id="Seg_605" n="e" s="T2">mɔːtqɨn, </ts>
               <ts e="T4" id="Seg_607" n="e" s="T3">ukkɨr </ts>
               <ts e="T5" id="Seg_609" n="e" s="T4">mɔːtqɨt </ts>
               <ts e="T6" id="Seg_611" n="e" s="T5">qumɨn </ts>
               <ts e="T7" id="Seg_613" n="e" s="T6">ilɨmmɨntɔːtɨn. </ts>
               <ts e="T8" id="Seg_615" n="e" s="T7">Nɨmtɨ </ts>
               <ts e="T9" id="Seg_617" n="e" s="T8">na </ts>
               <ts e="T10" id="Seg_619" n="e" s="T9">mɔːtqɨntɨ </ts>
               <ts e="T11" id="Seg_621" n="e" s="T10">nılʼčʼi </ts>
               <ts e="T12" id="Seg_623" n="e" s="T11">ima </ts>
               <ts e="T13" id="Seg_625" n="e" s="T12">ɛːppɨntɨ. </ts>
               <ts e="T14" id="Seg_627" n="e" s="T13">Qumɨn </ts>
               <ts e="T15" id="Seg_629" n="e" s="T14">lɨpkɨmɔːlla </ts>
               <ts e="T16" id="Seg_631" n="e" s="T15">muntɨ </ts>
               <ts e="T17" id="Seg_633" n="e" s="T16">ılla </ts>
               <ts e="T18" id="Seg_635" n="e" s="T17">qontɔːtɨn. </ts>
               <ts e="T19" id="Seg_637" n="e" s="T18">Na </ts>
               <ts e="T20" id="Seg_639" n="e" s="T19">ima </ts>
               <ts e="T21" id="Seg_641" n="e" s="T20">čʼeːlʼin </ts>
               <ts e="T22" id="Seg_643" n="e" s="T21">ašša </ts>
               <ts e="T23" id="Seg_645" n="e" s="T22">(qo=), </ts>
               <ts e="T24" id="Seg_647" n="e" s="T23">čʼeːlʼin </ts>
               <ts e="T25" id="Seg_649" n="e" s="T24">qontɨqɨjojimpa. </ts>
               <ts e="T26" id="Seg_651" n="e" s="T25">A </ts>
               <ts e="T27" id="Seg_653" n="e" s="T26">lipkɨqɨn </ts>
               <ts e="T28" id="Seg_655" n="e" s="T27">qumɨt </ts>
               <ts e="T29" id="Seg_657" n="e" s="T28">qontallɛːptäqɨn, </ts>
               <ts e="T30" id="Seg_659" n="e" s="T29">qumɨt </ts>
               <ts e="T31" id="Seg_661" n="e" s="T30">qontallɛːptäqɨntɨ, </ts>
               <ts e="T32" id="Seg_663" n="e" s="T31">nɔːt </ts>
               <ts e="T33" id="Seg_665" n="e" s="T32">čʼittɨmpɨqolamqɨjojimpa. </ts>
               <ts e="T34" id="Seg_667" n="e" s="T33">Amɨrqɨjojimpɨqo </ts>
               <ts e="T35" id="Seg_669" n="e" s="T34">olampa. </ts>
               <ts e="T36" id="Seg_671" n="e" s="T35">Šütɨran. </ts>
               <ts e="T37" id="Seg_673" n="e" s="T36">Meːltɨqam </ts>
               <ts e="T38" id="Seg_675" n="e" s="T37">nıŋ </ts>
               <ts e="T39" id="Seg_677" n="e" s="T38">orɨnʼnʼa, </ts>
               <ts e="T40" id="Seg_679" n="e" s="T39">čʼeːlɨt </ts>
               <ts e="T41" id="Seg_681" n="e" s="T40">qonta, </ts>
               <ts e="T42" id="Seg_683" n="e" s="T41">lipkɨqɨn </ts>
               <ts e="T43" id="Seg_685" n="e" s="T42">uːčʼa. </ts>
               <ts e="T44" id="Seg_687" n="e" s="T43">Ukkɨr </ts>
               <ts e="T45" id="Seg_689" n="e" s="T44">čʼontoːqɨn </ts>
               <ts e="T46" id="Seg_691" n="e" s="T45">omtane </ts>
               <ts e="T47" id="Seg_693" n="e" s="T46">šütɨra. </ts>
               <ts e="T48" id="Seg_695" n="e" s="T47">Pɨrnʼi </ts>
               <ts e="T49" id="Seg_697" n="e" s="T48">mɔːt </ts>
               <ts e="T50" id="Seg_699" n="e" s="T49">šeːrɨ. </ts>
               <ts e="T51" id="Seg_701" n="e" s="T50">(Pɨrnʼi=) </ts>
               <ts e="T52" id="Seg_703" n="e" s="T51">pɨrnʼi </ts>
               <ts e="T53" id="Seg_705" n="e" s="T52">mɔːt </ts>
               <ts e="T54" id="Seg_707" n="e" s="T53">šeːrla </ts>
               <ts e="T55" id="Seg_709" n="e" s="T54">nıŋ </ts>
               <ts e="T56" id="Seg_711" n="e" s="T55">orɨnʼnʼa:“ </ts>
               <ts e="T57" id="Seg_713" n="e" s="T56">Tat </ts>
               <ts e="T58" id="Seg_715" n="e" s="T57">qaqa </ts>
               <ts e="T59" id="Seg_717" n="e" s="T58">ašša </ts>
               <ts e="T60" id="Seg_719" n="e" s="T59">qontantɨ?” </ts>
               <ts e="T61" id="Seg_721" n="e" s="T60">Ima </ts>
               <ts e="T62" id="Seg_723" n="e" s="T61">qɨčʼčʼɨmɔːtpa. </ts>
               <ts e="T63" id="Seg_725" n="e" s="T62">“Mat </ts>
               <ts e="T64" id="Seg_727" n="e" s="T63">taštɨ </ts>
               <ts e="T65" id="Seg_729" n="e" s="T64">tıː </ts>
               <ts e="T66" id="Seg_731" n="e" s="T65">inna </ts>
               <ts e="T67" id="Seg_733" n="e" s="T66">amɛntaɣ, </ts>
               <ts e="T68" id="Seg_735" n="e" s="T67">tolʼkə </ts>
               <ts e="T69" id="Seg_737" n="e" s="T68">moqanä </ts>
               <ts e="T70" id="Seg_739" n="e" s="T69">qoːranɛntak”. </ts>
               <ts e="T71" id="Seg_741" n="e" s="T70">Pɨrnʼi </ts>
               <ts e="T72" id="Seg_743" n="e" s="T71">moqanä </ts>
               <ts e="T73" id="Seg_745" n="e" s="T72">kurralna. </ts>
               <ts e="T74" id="Seg_747" n="e" s="T73">Ima </ts>
               <ts e="T75" id="Seg_749" n="e" s="T74">qɨčʼčʼɨmɔːlla, </ts>
               <ts e="T76" id="Seg_751" n="e" s="T75">mɨttɨ </ts>
               <ts e="T77" id="Seg_753" n="e" s="T76">torqɨ </ts>
               <ts e="T78" id="Seg_755" n="e" s="T77">orqɨlla, </ts>
               <ts e="T79" id="Seg_757" n="e" s="T78">qɔːlitɨ </ts>
               <ts e="T80" id="Seg_759" n="e" s="T79">mɨttɨsa </ts>
               <ts e="T81" id="Seg_761" n="e" s="T80">natqɨlɛlʼčʼɨn. </ts>
               <ts e="T82" id="Seg_763" n="e" s="T81">Na </ts>
               <ts e="T83" id="Seg_765" n="e" s="T82">nık </ts>
               <ts e="T84" id="Seg_767" n="e" s="T83">čʼəkäptɨrla </ts>
               <ts e="T85" id="Seg_769" n="e" s="T84">qumɨtɨt </ts>
               <ts e="T86" id="Seg_771" n="e" s="T85">puːtantɨ </ts>
               <ts e="T87" id="Seg_773" n="e" s="T86">patqɨlla </ts>
               <ts e="T88" id="Seg_775" n="e" s="T87">ılla </ts>
               <ts e="T89" id="Seg_777" n="e" s="T88">qontaja. </ts>
               <ts e="T153" id="Seg_779" n="e" s="T89">((KuAI:)) Ещё медленнее. </ts>
               <ts e="T90" id="Seg_781" n="e" s="T153">((IF:)) </ts>
               <ts e="T91" id="Seg_783" n="e" s="T90">Pɨrnʼi </ts>
               <ts e="T92" id="Seg_785" n="e" s="T91">čʼap </ts>
               <ts e="T93" id="Seg_787" n="e" s="T92">tüjanɨ, </ts>
               <ts e="T94" id="Seg_789" n="e" s="T93">ima </ts>
               <ts e="T95" id="Seg_791" n="e" s="T94">meːlt </ts>
               <ts e="T96" id="Seg_793" n="e" s="T95">čʼäŋkan. </ts>
               <ts e="T97" id="Seg_795" n="e" s="T96">Tɨčʼčʼam </ts>
               <ts e="T98" id="Seg_797" n="e" s="T97">mantɛːja, </ts>
               <ts e="T99" id="Seg_799" n="e" s="T98">točʼčʼam </ts>
               <ts e="T100" id="Seg_801" n="e" s="T99">mantɛːja, </ts>
               <ts e="T101" id="Seg_803" n="e" s="T100">ima </ts>
               <ts e="T102" id="Seg_805" n="e" s="T101">meːlt </ts>
               <ts e="T103" id="Seg_807" n="e" s="T102">čʼä. </ts>
               <ts e="T104" id="Seg_809" n="e" s="T103">Pɨrnʼi </ts>
               <ts e="T105" id="Seg_811" n="e" s="T104">peːqolamnɨt, </ts>
               <ts e="T106" id="Seg_813" n="e" s="T105">qumɨtɨt </ts>
               <ts e="T107" id="Seg_815" n="e" s="T106">qɔːli </ts>
               <ts e="T108" id="Seg_817" n="e" s="T107">paqqɨrɨt. </ts>
               <ts e="T109" id="Seg_819" n="e" s="T108">Kutɨt </ts>
               <ts e="T110" id="Seg_821" n="e" s="T109">qɔːli </ts>
               <ts e="T111" id="Seg_823" n="e" s="T110">qap </ts>
               <ts e="T112" id="Seg_825" n="e" s="T111">qannɨmmɨntɨ, </ts>
               <ts e="T113" id="Seg_827" n="e" s="T112">značʼit </ts>
               <ts e="T114" id="Seg_829" n="e" s="T113">na </ts>
               <ts e="T115" id="Seg_831" n="e" s="T114">ima </ts>
               <ts e="T116" id="Seg_833" n="e" s="T115">najɛntɨ. </ts>
               <ts e="T117" id="Seg_835" n="e" s="T116">Kojɨmmɨntɨt </ts>
               <ts e="T118" id="Seg_837" n="e" s="T117">na </ts>
               <ts e="T119" id="Seg_839" n="e" s="T118">paqqɨrɨntɨt, </ts>
               <ts e="T120" id="Seg_841" n="e" s="T119">na </ts>
               <ts e="T121" id="Seg_843" n="e" s="T120">kojaltɨntɨt, </ts>
               <ts e="T122" id="Seg_845" n="e" s="T121">na </ts>
               <ts e="T123" id="Seg_847" n="e" s="T122">kojaltɨntɨt. </ts>
               <ts e="T124" id="Seg_849" n="e" s="T123">“Qumɨn </ts>
               <ts e="T125" id="Seg_851" n="e" s="T124">mun </ts>
               <ts e="T126" id="Seg_853" n="e" s="T125">qɔːlɨjitɨ </ts>
               <ts e="T127" id="Seg_855" n="e" s="T126">pötpa, </ts>
               <ts e="T128" id="Seg_857" n="e" s="T127">naš </ts>
               <ts e="T129" id="Seg_859" n="e" s="T128">čʼaptät </ts>
               <ts e="T130" id="Seg_861" n="e" s="T129">qontɨtɨ </ts>
               <ts e="T131" id="Seg_863" n="e" s="T130">(im-) </ts>
               <ts e="T132" id="Seg_865" n="e" s="T131">qumɨn”,– </ts>
               <ts e="T133" id="Seg_867" n="e" s="T132">pɨrnʼi </ts>
               <ts e="T134" id="Seg_869" n="e" s="T133">nık </ts>
               <ts e="T135" id="Seg_871" n="e" s="T134">kətɨŋɨt. </ts>
               <ts e="T136" id="Seg_873" n="e" s="T135">A </ts>
               <ts e="T137" id="Seg_875" n="e" s="T136">to </ts>
               <ts e="T138" id="Seg_877" n="e" s="T137">na </ts>
               <ts e="T139" id="Seg_879" n="e" s="T138">ima </ts>
               <ts e="T140" id="Seg_881" n="e" s="T139">ippɨla </ts>
               <ts e="T141" id="Seg_883" n="e" s="T140">əːtɨkɔːl </ts>
               <ts e="T142" id="Seg_885" n="e" s="T141">na </ts>
               <ts e="T143" id="Seg_887" n="e" s="T142">taŋaltɨmpa. </ts>
               <ts e="T144" id="Seg_889" n="e" s="T143">Pɨrnʼi </ts>
               <ts e="T145" id="Seg_891" n="e" s="T144">pona </ts>
               <ts e="T146" id="Seg_893" n="e" s="T145">tarlä </ts>
               <ts e="T147" id="Seg_895" n="e" s="T146">toː </ts>
               <ts e="T148" id="Seg_897" n="e" s="T147">tiː </ts>
               <ts e="T149" id="Seg_899" n="e" s="T148">toː </ts>
               <ts e="T150" id="Seg_901" n="e" s="T149">qənmɨntɨ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T152" id="Seg_902" s="T0">IF_196X_WomanAndWitch_flk.001 (001)</ta>
            <ta e="T7" id="Seg_903" s="T152">IF_196X_WomanAndWitch_flk.002 (002)</ta>
            <ta e="T13" id="Seg_904" s="T7">IF_196X_WomanAndWitch_flk.003 (003)</ta>
            <ta e="T18" id="Seg_905" s="T13">IF_196X_WomanAndWitch_flk.004 (004)</ta>
            <ta e="T25" id="Seg_906" s="T18">IF_196X_WomanAndWitch_flk.005 (005)</ta>
            <ta e="T33" id="Seg_907" s="T25">IF_196X_WomanAndWitch_flk.006 (006)</ta>
            <ta e="T35" id="Seg_908" s="T33">IF_196X_WomanAndWitch_flk.007 (007)</ta>
            <ta e="T36" id="Seg_909" s="T35">IF_196X_WomanAndWitch_flk.008 (008)</ta>
            <ta e="T43" id="Seg_910" s="T36">IF_196X_WomanAndWitch_flk.009 (009)</ta>
            <ta e="T47" id="Seg_911" s="T43">IF_196X_WomanAndWitch_flk.010 (010)</ta>
            <ta e="T50" id="Seg_912" s="T47">IF_196X_WomanAndWitch_flk.011 (011)</ta>
            <ta e="T60" id="Seg_913" s="T50">IF_196X_WomanAndWitch_flk.012 (012)</ta>
            <ta e="T62" id="Seg_914" s="T60">IF_196X_WomanAndWitch_flk.013 (013)</ta>
            <ta e="T70" id="Seg_915" s="T62">IF_196X_WomanAndWitch_flk.014 (014)</ta>
            <ta e="T73" id="Seg_916" s="T70">IF_196X_WomanAndWitch_flk.015 (015)</ta>
            <ta e="T81" id="Seg_917" s="T73">IF_196X_WomanAndWitch_flk.016 (016)</ta>
            <ta e="T89" id="Seg_918" s="T81">IF_196X_WomanAndWitch_flk.017 (017)</ta>
            <ta e="T153" id="Seg_919" s="T89">IF_196X_WomanAndWitch_flk.018 (018)</ta>
            <ta e="T96" id="Seg_920" s="T153">IF_196X_WomanAndWitch_flk.019 (019)</ta>
            <ta e="T103" id="Seg_921" s="T96">IF_196X_WomanAndWitch_flk.020 (020)</ta>
            <ta e="T108" id="Seg_922" s="T103">IF_196X_WomanAndWitch_flk.021 (021)</ta>
            <ta e="T116" id="Seg_923" s="T108">IF_196X_WomanAndWitch_flk.022 (022)</ta>
            <ta e="T123" id="Seg_924" s="T116">IF_196X_WomanAndWitch_flk.023 (023)</ta>
            <ta e="T135" id="Seg_925" s="T123">IF_196X_WomanAndWitch_flk.024 (024)</ta>
            <ta e="T143" id="Seg_926" s="T135">IF_196X_WomanAndWitch_flk.025 (025) </ta>
            <ta e="T150" id="Seg_927" s="T143">IF_196X_WomanAndWitch_flk.026 (026)</ta>
         </annotation>
         <annotation name="st" tierref="st" />
         <annotation name="stl" tierref="stl">
            <ta e="T152" id="Seg_928" s="T0">((KuAI:)) Фрося Ирикова. Сказка</ta>
            <ta e="T7" id="Seg_929" s="T152">((IF:)) ukkɨr motqən… ukkɨr motqən qummɨn ilɨmɨnto:tɨn</ta>
            <ta e="T13" id="Seg_930" s="T7">nɨmtɨ na motqəntɨ nilʼčʼi imma e:pɨntɨ</ta>
            <ta e="T18" id="Seg_931" s="T13">qumɨn lɨpkɨmola muntɨ ılla qonto:tɨn</ta>
            <ta e="T25" id="Seg_932" s="T18">na imma čʼelʼin aša… qo… čʼelʼin qontɨqɨjojimpa </ta>
            <ta e="T33" id="Seg_933" s="T25">a lippɨqɨn qumɨt qontalepteɣən qumɨt qontalepteɣəntɨ nat čʼittɨmpɨqollamqɨjojimpa</ta>
            <ta e="T35" id="Seg_934" s="T33">amɨrqɨjojimpɨqo olampa</ta>
            <ta e="T36" id="Seg_935" s="T35">šütɨran</ta>
            <ta e="T43" id="Seg_936" s="T36">meltɨqam ni orɨnʼa, čʼelɨt qonta, lippɨqɨn u:čʼa</ta>
            <ta e="T47" id="Seg_937" s="T43">ukkɨr čʼontaqən omtane šütɨra</ta>
            <ta e="T50" id="Seg_938" s="T47">pərnʼi mot šʼerɨ</ta>
            <ta e="T60" id="Seg_939" s="T50">pərnʼi… pərnʼi mot še:rla ni orɨnʼa: "ta qaqa aša qonnantɨ?"</ta>
            <ta e="T62" id="Seg_940" s="T60">imma qəčʼčʼimotpa</ta>
            <ta e="T70" id="Seg_941" s="T62">man taštɨ ti inna aməntaɣɣɨ, tolʼkə moqana qoranentaq</ta>
            <ta e="T73" id="Seg_942" s="T70">pərnʼi moqana quralna</ta>
            <ta e="T81" id="Seg_943" s="T73">imma qəčʼčʼimola, mətti torqɨ orqəla, qolitɨ məttɨjisa natqɨlelčʼi(qɨttɨ)</ta>
            <ta e="T89" id="Seg_944" s="T81">na ni čʼekaptɨrla qumətɨt putantɨ patqəla ılla qontaja</ta>
            <ta e="T153" id="Seg_945" s="T89">((KuAI:)) Ещё медленнее.</ta>
            <ta e="T96" id="Seg_946" s="T153">((IF:)) pɨrnʼi čʼaptülanɨ, imma melʼt čʼeŋka</ta>
            <ta e="T103" id="Seg_947" s="T96">tičʼam mantaja, točʼam mantaja, imma melʼt čʼe(ŋk)</ta>
            <ta e="T108" id="Seg_948" s="T103">pərnʼi pe:qolampɨt, qumɨtɨt qoli paqarɨt</ta>
            <ta e="T116" id="Seg_949" s="T108">qutɨt qoliqap qannəmɨntɨt, značʼit na imma näjent(e)</ta>
            <ta e="T123" id="Seg_950" s="T116">qojimɨntɨt, na paqɨrɨntɨt, na kojaltɨntɨt, na kojaltɨntɨt</ta>
            <ta e="T135" id="Seg_951" s="T123">qumɨn mun qolijitɨ pötpa, naš čʼaptät qontɨti imm… qumɨn pɨrnʼi ni kətiɣit</ta>
            <ta e="T143" id="Seg_952" s="T135">a to na imma ippɨla etikol na taɣaltɨmpa</ta>
            <ta e="T150" id="Seg_953" s="T143">pɨrnʼi pona tarla to ti to qənmən(tɨ).</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T152" id="Seg_954" s="T0">((KuAI:)) Фрося Ирикова. Сказка. </ta>
            <ta e="T7" id="Seg_955" s="T152">((IF:)) Ukkɨr mɔːtqɨn, ukkɨr mɔːtqɨt qumɨn ilɨmmɨntɔːtɨn. </ta>
            <ta e="T13" id="Seg_956" s="T7">Nɨmtɨ na mɔːtqɨntɨ nılʼčʼi ima ɛːppɨntɨ. </ta>
            <ta e="T18" id="Seg_957" s="T13">Qumɨn lɨpkɨmɔːlla muntɨ ılla qontɔːtɨn. </ta>
            <ta e="T25" id="Seg_958" s="T18">Na ima čʼeːlʼin ašša (qo=), čʼeːlʼin qontɨqɨjojimpa. </ta>
            <ta e="T33" id="Seg_959" s="T25">A lipkɨqɨn qumɨt qontallɛːptäqɨn, qumɨt qontallɛːptäqɨntɨ, nɔːt čʼittɨmpɨqolamqɨjojimpa. </ta>
            <ta e="T35" id="Seg_960" s="T33">Amɨrqɨjojimpɨqo olampa. </ta>
            <ta e="T36" id="Seg_961" s="T35">Šütɨran. </ta>
            <ta e="T43" id="Seg_962" s="T36">Meːltɨqam nıŋ orɨnʼnʼa, čʼeːlɨt qonta, lipkɨqɨn uːčʼa. </ta>
            <ta e="T47" id="Seg_963" s="T43">Ukkɨr čʼontoːqɨn omtane šütɨra. </ta>
            <ta e="T50" id="Seg_964" s="T47">Pɨrnʼi mɔːt šeːrɨ. </ta>
            <ta e="T60" id="Seg_965" s="T50">(Pɨrnʼi=) pɨrnʼi mɔːt šeːrla nıŋ orɨnʼnʼa:“ Tat qaqa ašša qontantɨ?” </ta>
            <ta e="T62" id="Seg_966" s="T60">Ima qɨčʼčʼɨmɔːtpa. </ta>
            <ta e="T70" id="Seg_967" s="T62">“Mat taštɨ tıː inna amɛntaɣ, tolʼkə moqanä qoːranɛntak”. </ta>
            <ta e="T73" id="Seg_968" s="T70">Pɨrnʼi moqanä kurralna. </ta>
            <ta e="T81" id="Seg_969" s="T73">Ima qɨčʼčʼɨmɔːlla, mɨttɨ torqɨ orqɨlla, qɔːlitɨ mɨttɨsa natqɨlɛlʼčʼɨn. </ta>
            <ta e="T89" id="Seg_970" s="T81">Na nık čʼəkäptɨrla qumɨtɨt puːtantɨ patqɨlla ılla qontaja. </ta>
            <ta e="T153" id="Seg_971" s="T89">((KuAI:)) Ещё медленнее. </ta>
            <ta e="T96" id="Seg_972" s="T153">((IF:)) Pɨrnʼi čʼap tüjanɨ, ima meːlt čʼäŋkan. </ta>
            <ta e="T103" id="Seg_973" s="T96">Tɨčʼčʼam mantɛːja, točʼčʼam mantɛːja, ima meːlt čʼä. </ta>
            <ta e="T108" id="Seg_974" s="T103">Pɨrnʼi peːqolamnɨt, qumɨtɨt qɔːli paqqɨrɨt. </ta>
            <ta e="T116" id="Seg_975" s="T108">Kutɨt qɔːli qap qannɨmmɨntɨ, značʼit na ima najɛntɨ. </ta>
            <ta e="T123" id="Seg_976" s="T116">Kojɨmmɨntɨt na paqqɨrɨntɨt, na kojaltɨntɨt, na kojaltɨntɨt. </ta>
            <ta e="T135" id="Seg_977" s="T123">“Qumɨn mun qɔːlɨjitɨ pötpa, naš čʼaptät qontɨtɨ (im-) qumɨn”,– pɨrnʼi nık kətɨŋɨt. </ta>
            <ta e="T143" id="Seg_978" s="T135">A to na ima ippɨla əːtɨkɔːl na taŋaltɨmpa. </ta>
            <ta e="T150" id="Seg_979" s="T143">Pɨrnʼi pona tarlä toː tiː toː qənmɨntɨ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_980" s="T1">ukkɨr</ta>
            <ta e="T3" id="Seg_981" s="T2">mɔːt-qɨn</ta>
            <ta e="T4" id="Seg_982" s="T3">ukkɨr</ta>
            <ta e="T5" id="Seg_983" s="T4">mɔːt-qɨt</ta>
            <ta e="T6" id="Seg_984" s="T5">qum-ɨ-n</ta>
            <ta e="T7" id="Seg_985" s="T6">ilɨ-mmɨ-ntɔː-tɨn</ta>
            <ta e="T8" id="Seg_986" s="T7">nɨmtɨ</ta>
            <ta e="T9" id="Seg_987" s="T8">na</ta>
            <ta e="T10" id="Seg_988" s="T9">mɔːt-qɨn-tɨ</ta>
            <ta e="T11" id="Seg_989" s="T10">nılʼčʼi</ta>
            <ta e="T12" id="Seg_990" s="T11">ima</ta>
            <ta e="T13" id="Seg_991" s="T12">ɛː-ppɨ-ntɨ</ta>
            <ta e="T14" id="Seg_992" s="T13">qum-ɨ-n</ta>
            <ta e="T15" id="Seg_993" s="T14">lɨpkɨ-mɔːl-la</ta>
            <ta e="T16" id="Seg_994" s="T15">muntɨ</ta>
            <ta e="T17" id="Seg_995" s="T16">ılla</ta>
            <ta e="T18" id="Seg_996" s="T17">qontɔː-tɨn</ta>
            <ta e="T19" id="Seg_997" s="T18">na</ta>
            <ta e="T20" id="Seg_998" s="T19">ima</ta>
            <ta e="T21" id="Seg_999" s="T20">čʼeːlʼi-n</ta>
            <ta e="T22" id="Seg_1000" s="T21">ašša</ta>
            <ta e="T24" id="Seg_1001" s="T23">čʼeːlʼi-n</ta>
            <ta e="T25" id="Seg_1002" s="T24">qontɨ-qɨ-joji-mpa</ta>
            <ta e="T26" id="Seg_1003" s="T25">a</ta>
            <ta e="T27" id="Seg_1004" s="T26">*lipkɨ-qɨn</ta>
            <ta e="T28" id="Seg_1005" s="T27">qum-ɨ-t</ta>
            <ta e="T29" id="Seg_1006" s="T28">qont-all-ɛː-ptä-qɨn</ta>
            <ta e="T30" id="Seg_1007" s="T29">qum-ɨ-t</ta>
            <ta e="T31" id="Seg_1008" s="T30">qont-all-ɛː-ptä-qɨn-tɨ</ta>
            <ta e="T32" id="Seg_1009" s="T31">nɔːt</ta>
            <ta e="T33" id="Seg_1010" s="T32">čʼi-ttɨ-mpɨ-q-olam-qɨ-joji-mpa</ta>
            <ta e="T34" id="Seg_1011" s="T33">am-ɨ-r-qɨ-joji-mpɨ-qo</ta>
            <ta e="T35" id="Seg_1012" s="T34">olam-pa</ta>
            <ta e="T36" id="Seg_1013" s="T35">šüt-ɨ-r-a-n</ta>
            <ta e="T37" id="Seg_1014" s="T36">meːltɨ-qam</ta>
            <ta e="T38" id="Seg_1015" s="T37">nı-ŋ</ta>
            <ta e="T39" id="Seg_1016" s="T38">orɨ-nʼ-nʼa</ta>
            <ta e="T40" id="Seg_1017" s="T39">čʼeːlɨ-t</ta>
            <ta e="T41" id="Seg_1018" s="T40">qonta</ta>
            <ta e="T42" id="Seg_1019" s="T41">*lipkɨ-qɨn</ta>
            <ta e="T43" id="Seg_1020" s="T42">uːčʼa</ta>
            <ta e="T44" id="Seg_1021" s="T43">ukkɨr</ta>
            <ta e="T45" id="Seg_1022" s="T44">čʼontoː-qɨn</ta>
            <ta e="T46" id="Seg_1023" s="T45">omta-ne</ta>
            <ta e="T47" id="Seg_1024" s="T46">šüt-ɨ-r-a</ta>
            <ta e="T48" id="Seg_1025" s="T47">pɨrnʼi</ta>
            <ta e="T49" id="Seg_1026" s="T48">mɔːt</ta>
            <ta e="T50" id="Seg_1027" s="T49">šeːr-ɨ</ta>
            <ta e="T51" id="Seg_1028" s="T50">pɨrnʼi</ta>
            <ta e="T52" id="Seg_1029" s="T51">pɨrnʼi</ta>
            <ta e="T53" id="Seg_1030" s="T52">mɔːt</ta>
            <ta e="T54" id="Seg_1031" s="T53">šeːr-la</ta>
            <ta e="T55" id="Seg_1032" s="T54">nı-ŋ</ta>
            <ta e="T56" id="Seg_1033" s="T55">orɨ-nʼ-nʼa</ta>
            <ta e="T57" id="Seg_1034" s="T56">tat</ta>
            <ta e="T58" id="Seg_1035" s="T57">qaqa</ta>
            <ta e="T59" id="Seg_1036" s="T58">ašša</ta>
            <ta e="T60" id="Seg_1037" s="T59">qonta-ntɨ</ta>
            <ta e="T61" id="Seg_1038" s="T60">ima</ta>
            <ta e="T62" id="Seg_1039" s="T61">qɨčʼčʼɨ-mɔːt-pa</ta>
            <ta e="T63" id="Seg_1040" s="T62">Mat</ta>
            <ta e="T64" id="Seg_1041" s="T63">taštɨ</ta>
            <ta e="T65" id="Seg_1042" s="T64">tıː</ta>
            <ta e="T66" id="Seg_1043" s="T65">ınna</ta>
            <ta e="T67" id="Seg_1044" s="T66">am-ɛnta-ɣ</ta>
            <ta e="T68" id="Seg_1045" s="T67">tolʼkə</ta>
            <ta e="T69" id="Seg_1046" s="T68">moqanä</ta>
            <ta e="T70" id="Seg_1047" s="T69">qoːran-ɛnta-k</ta>
            <ta e="T71" id="Seg_1048" s="T70">pɨrnʼi</ta>
            <ta e="T72" id="Seg_1049" s="T71">moqanä</ta>
            <ta e="T73" id="Seg_1050" s="T72">kurr-al-na</ta>
            <ta e="T74" id="Seg_1051" s="T73">ima</ta>
            <ta e="T75" id="Seg_1052" s="T74">qɨčʼčʼɨ-mɔːl-la</ta>
            <ta e="T76" id="Seg_1053" s="T75">mɨttɨ</ta>
            <ta e="T77" id="Seg_1054" s="T76">torqɨ</ta>
            <ta e="T78" id="Seg_1055" s="T77">orqɨl-la</ta>
            <ta e="T79" id="Seg_1056" s="T78">qɔːli-tɨ</ta>
            <ta e="T80" id="Seg_1057" s="T79">mɨttɨ-sa</ta>
            <ta e="T81" id="Seg_1058" s="T80">nat-qɨl-ɛlʼčʼɨ-n</ta>
            <ta e="T82" id="Seg_1059" s="T81">na</ta>
            <ta e="T83" id="Seg_1060" s="T82">nık</ta>
            <ta e="T84" id="Seg_1061" s="T83">čʼək-äptɨ-r-la</ta>
            <ta e="T85" id="Seg_1062" s="T84">qum-ɨ-t-ɨ-t</ta>
            <ta e="T86" id="Seg_1063" s="T85">puːta-ntɨ</ta>
            <ta e="T87" id="Seg_1064" s="T86">pat-qɨl-la</ta>
            <ta e="T88" id="Seg_1065" s="T87">ılla</ta>
            <ta e="T89" id="Seg_1066" s="T88">qonta-ja</ta>
            <ta e="T91" id="Seg_1067" s="T90">pɨrnʼi</ta>
            <ta e="T92" id="Seg_1068" s="T91">čʼap</ta>
            <ta e="T93" id="Seg_1069" s="T92">tü-ja-nɨ</ta>
            <ta e="T94" id="Seg_1070" s="T93">ima</ta>
            <ta e="T95" id="Seg_1071" s="T94">meːlt</ta>
            <ta e="T96" id="Seg_1072" s="T95">čʼäŋka-n</ta>
            <ta e="T97" id="Seg_1073" s="T96">tɨčʼčʼa-m</ta>
            <ta e="T98" id="Seg_1074" s="T97">mant-ɛː-ja</ta>
            <ta e="T99" id="Seg_1075" s="T98">točʼčʼa-m</ta>
            <ta e="T100" id="Seg_1076" s="T99">mant-ɛː-ja</ta>
            <ta e="T101" id="Seg_1077" s="T100">ima</ta>
            <ta e="T102" id="Seg_1078" s="T101">meːlt</ta>
            <ta e="T103" id="Seg_1079" s="T102">čʼä</ta>
            <ta e="T104" id="Seg_1080" s="T103">pɨrnʼi</ta>
            <ta e="T105" id="Seg_1081" s="T104">peː-q-olam-nɨ-t</ta>
            <ta e="T106" id="Seg_1082" s="T105">qum-ɨ-t-ɨ-t</ta>
            <ta e="T107" id="Seg_1083" s="T106">qɔːli</ta>
            <ta e="T108" id="Seg_1084" s="T107">paqqɨ-rɨ-t</ta>
            <ta e="T109" id="Seg_1085" s="T108">kutɨ-t</ta>
            <ta e="T110" id="Seg_1086" s="T109">qɔːli</ta>
            <ta e="T111" id="Seg_1087" s="T110">qap</ta>
            <ta e="T112" id="Seg_1088" s="T111">qannɨ-mmɨ-ntɨ</ta>
            <ta e="T113" id="Seg_1089" s="T112">značʼit</ta>
            <ta e="T114" id="Seg_1090" s="T113">na</ta>
            <ta e="T115" id="Seg_1091" s="T114">ima</ta>
            <ta e="T116" id="Seg_1092" s="T115">najɛntɨ</ta>
            <ta e="T117" id="Seg_1093" s="T116">*kojɨ-mmɨ-ntɨ-t</ta>
            <ta e="T118" id="Seg_1094" s="T117">na</ta>
            <ta e="T119" id="Seg_1095" s="T118">paqqɨ-rɨ-ntɨ-t</ta>
            <ta e="T120" id="Seg_1096" s="T119">na</ta>
            <ta e="T121" id="Seg_1097" s="T120">*koj-altɨ-ntɨ-t</ta>
            <ta e="T122" id="Seg_1098" s="T121">na</ta>
            <ta e="T123" id="Seg_1099" s="T122">*koj-altɨ-ntɨ-t</ta>
            <ta e="T124" id="Seg_1100" s="T123">qum-ɨ-n</ta>
            <ta e="T125" id="Seg_1101" s="T124">mun</ta>
            <ta e="T126" id="Seg_1102" s="T125">qɔːlɨj-i-tɨ</ta>
            <ta e="T127" id="Seg_1103" s="T126">pöt-pa</ta>
            <ta e="T128" id="Seg_1104" s="T127">naš</ta>
            <ta e="T129" id="Seg_1105" s="T128">čʼaptät</ta>
            <ta e="T130" id="Seg_1106" s="T129">qontɨ-tɨ</ta>
            <ta e="T132" id="Seg_1107" s="T131">qum-ɨ-n</ta>
            <ta e="T133" id="Seg_1108" s="T132">pɨrnʼi</ta>
            <ta e="T134" id="Seg_1109" s="T133">nık</ta>
            <ta e="T135" id="Seg_1110" s="T134">kətɨ-ŋɨ-t</ta>
            <ta e="T136" id="Seg_1111" s="T135">a</ta>
            <ta e="T137" id="Seg_1112" s="T136">to</ta>
            <ta e="T138" id="Seg_1113" s="T137">na</ta>
            <ta e="T139" id="Seg_1114" s="T138">ima</ta>
            <ta e="T140" id="Seg_1115" s="T139">ippɨ-la</ta>
            <ta e="T141" id="Seg_1116" s="T140">əːtɨ-kɔːl</ta>
            <ta e="T142" id="Seg_1117" s="T141">na</ta>
            <ta e="T143" id="Seg_1118" s="T142">taŋ-altɨ-mpa</ta>
            <ta e="T144" id="Seg_1119" s="T143">pɨrnʼi</ta>
            <ta e="T145" id="Seg_1120" s="T144">pona</ta>
            <ta e="T146" id="Seg_1121" s="T145">tar-lä</ta>
            <ta e="T147" id="Seg_1122" s="T146">toː</ta>
            <ta e="T148" id="Seg_1123" s="T147">tiː</ta>
            <ta e="T149" id="Seg_1124" s="T148">toː</ta>
            <ta e="T150" id="Seg_1125" s="T149">qən-mɨ-ntɨ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_1126" s="T1">ukkɨr</ta>
            <ta e="T3" id="Seg_1127" s="T2">mɔːt-qɨn</ta>
            <ta e="T4" id="Seg_1128" s="T3">ukkɨr</ta>
            <ta e="T5" id="Seg_1129" s="T4">mɔːt-qɨn</ta>
            <ta e="T6" id="Seg_1130" s="T5">qum-ɨ-t</ta>
            <ta e="T7" id="Seg_1131" s="T6">ilɨ-mpɨ-ntɨ-tɨt</ta>
            <ta e="T8" id="Seg_1132" s="T7">nɨmtɨ</ta>
            <ta e="T9" id="Seg_1133" s="T8">na</ta>
            <ta e="T10" id="Seg_1134" s="T9">mɔːt-qɨn-ntɨ</ta>
            <ta e="T11" id="Seg_1135" s="T10">nılʼčʼɨ</ta>
            <ta e="T12" id="Seg_1136" s="T11">ima</ta>
            <ta e="T13" id="Seg_1137" s="T12">ɛː-mpɨ-ntɨ</ta>
            <ta e="T14" id="Seg_1138" s="T13">qum-ɨ-t</ta>
            <ta e="T15" id="Seg_1139" s="T14">*lɨpkɨ-mɔːt-lä</ta>
            <ta e="T16" id="Seg_1140" s="T15">muntɨk</ta>
            <ta e="T17" id="Seg_1141" s="T16">ıllä</ta>
            <ta e="T18" id="Seg_1142" s="T17">qontɨ-tɨt</ta>
            <ta e="T19" id="Seg_1143" s="T18">na</ta>
            <ta e="T20" id="Seg_1144" s="T19">ima</ta>
            <ta e="T21" id="Seg_1145" s="T20">čʼeːlɨ-n</ta>
            <ta e="T22" id="Seg_1146" s="T21">ašša</ta>
            <ta e="T24" id="Seg_1147" s="T23">čʼeːlɨ-n</ta>
            <ta e="T25" id="Seg_1148" s="T24">qontɨ-qo-joji-mpɨ</ta>
            <ta e="T26" id="Seg_1149" s="T25">a</ta>
            <ta e="T27" id="Seg_1150" s="T26">*lɨpɨ-qɨn</ta>
            <ta e="T28" id="Seg_1151" s="T27">qum-ɨ-t</ta>
            <ta e="T29" id="Seg_1152" s="T28">qontɨ-ɔːl-ɛː-ptäː-qɨn</ta>
            <ta e="T30" id="Seg_1153" s="T29">qum-ɨ-t</ta>
            <ta e="T31" id="Seg_1154" s="T30">qontɨ-ɔːl-ɛː-ptäː-qɨn-ntɨ</ta>
            <ta e="T32" id="Seg_1155" s="T31">nɔːtɨ</ta>
            <ta e="T33" id="Seg_1156" s="T32">čʼi-ttɨ-mpɨ-qo-olam-qo-joji-mpɨ</ta>
            <ta e="T34" id="Seg_1157" s="T33">am-ɨ-r-qo-joji-mpɨ-qo</ta>
            <ta e="T35" id="Seg_1158" s="T34">olam-mpɨ</ta>
            <ta e="T36" id="Seg_1159" s="T35">šüt-ɨ-r-ŋɨ-n</ta>
            <ta e="T37" id="Seg_1160" s="T36">meːltɨ-qam</ta>
            <ta e="T38" id="Seg_1161" s="T37">nılʼčʼɨ-k</ta>
            <ta e="T39" id="Seg_1162" s="T38">orɨ-š-ŋɨ</ta>
            <ta e="T40" id="Seg_1163" s="T39">čʼeːlɨ-k</ta>
            <ta e="T41" id="Seg_1164" s="T40">qontɨ</ta>
            <ta e="T42" id="Seg_1165" s="T41">*lɨpɨ-qɨn</ta>
            <ta e="T43" id="Seg_1166" s="T42">uːčʼɨ</ta>
            <ta e="T44" id="Seg_1167" s="T43">ukkɨr</ta>
            <ta e="T45" id="Seg_1168" s="T44">čʼontɨ-qɨn</ta>
            <ta e="T46" id="Seg_1169" s="T45">omtɨ-ŋɨ</ta>
            <ta e="T47" id="Seg_1170" s="T46">šüt-ɨ-r-ŋɨ</ta>
            <ta e="T48" id="Seg_1171" s="T47">pɨrni</ta>
            <ta e="T49" id="Seg_1172" s="T48">mɔːt</ta>
            <ta e="T50" id="Seg_1173" s="T49">šeːr-čʼɨ</ta>
            <ta e="T51" id="Seg_1174" s="T50">pɨrni</ta>
            <ta e="T52" id="Seg_1175" s="T51">pɨrni</ta>
            <ta e="T53" id="Seg_1176" s="T52">mɔːt</ta>
            <ta e="T54" id="Seg_1177" s="T53">šeːr-lä</ta>
            <ta e="T55" id="Seg_1178" s="T54">nılʼčʼɨ-k</ta>
            <ta e="T56" id="Seg_1179" s="T55">orɨ-š-ŋɨ</ta>
            <ta e="T57" id="Seg_1180" s="T56">tan</ta>
            <ta e="T58" id="Seg_1181" s="T57">qajqo</ta>
            <ta e="T59" id="Seg_1182" s="T58">ašša</ta>
            <ta e="T60" id="Seg_1183" s="T59">qontɨ-ntɨ</ta>
            <ta e="T61" id="Seg_1184" s="T60">ima</ta>
            <ta e="T62" id="Seg_1185" s="T61">*qɨčʼčʼɨ-mɔːt-mpɨ</ta>
            <ta e="T63" id="Seg_1186" s="T62">man</ta>
            <ta e="T64" id="Seg_1187" s="T63">tašıntɨ</ta>
            <ta e="T65" id="Seg_1188" s="T64">tıː</ta>
            <ta e="T66" id="Seg_1189" s="T65">ınnä</ta>
            <ta e="T67" id="Seg_1190" s="T66">am-ɛntɨ-k</ta>
            <ta e="T68" id="Seg_1191" s="T67">tolʼkə</ta>
            <ta e="T69" id="Seg_1192" s="T68">moqɨnä</ta>
            <ta e="T70" id="Seg_1193" s="T69">qoːran-ɛntɨ-k</ta>
            <ta e="T71" id="Seg_1194" s="T70">pɨrni</ta>
            <ta e="T72" id="Seg_1195" s="T71">moqɨnä</ta>
            <ta e="T73" id="Seg_1196" s="T72">*kurɨ-ätɔːl-ŋɨ</ta>
            <ta e="T74" id="Seg_1197" s="T73">ima</ta>
            <ta e="T75" id="Seg_1198" s="T74">*qɨčʼčʼɨ-mɔːt-lä</ta>
            <ta e="T76" id="Seg_1199" s="T75">mɨtɨn</ta>
            <ta e="T77" id="Seg_1200" s="T76">torqɨ</ta>
            <ta e="T78" id="Seg_1201" s="T77">orqɨl-lä</ta>
            <ta e="T79" id="Seg_1202" s="T78">qɔːlɨnʼ-tɨ</ta>
            <ta e="T80" id="Seg_1203" s="T79">mɨtɨn-sä</ta>
            <ta e="T81" id="Seg_1204" s="T80">nat-qɨl-ɛː-n</ta>
            <ta e="T82" id="Seg_1205" s="T81">na</ta>
            <ta e="T83" id="Seg_1206" s="T82">nık</ta>
            <ta e="T84" id="Seg_1207" s="T83">*čʼək-äptɨ-r-lä</ta>
            <ta e="T85" id="Seg_1208" s="T84">qum-ɨ-t-ɨ-n</ta>
            <ta e="T86" id="Seg_1209" s="T85">puːtɨ-ntɨ</ta>
            <ta e="T87" id="Seg_1210" s="T86">pat-qɨl-lä</ta>
            <ta e="T88" id="Seg_1211" s="T87">ıllä</ta>
            <ta e="T89" id="Seg_1212" s="T88">qontɨ-ŋɨ</ta>
            <ta e="T91" id="Seg_1213" s="T90">pɨrni</ta>
            <ta e="T92" id="Seg_1214" s="T91">čʼam</ta>
            <ta e="T93" id="Seg_1215" s="T92">tü-ja-ŋɨ</ta>
            <ta e="T94" id="Seg_1216" s="T93">ima</ta>
            <ta e="T95" id="Seg_1217" s="T94">meːltɨ</ta>
            <ta e="T96" id="Seg_1218" s="T95">čʼäːŋkɨ-n</ta>
            <ta e="T97" id="Seg_1219" s="T96">tɨčʼčʼä-m</ta>
            <ta e="T98" id="Seg_1220" s="T97">mantɨ-ɛː-ŋɨ</ta>
            <ta e="T99" id="Seg_1221" s="T98">točʼčʼä-m</ta>
            <ta e="T100" id="Seg_1222" s="T99">mantɨ-ɛː-ŋɨ</ta>
            <ta e="T101" id="Seg_1223" s="T100">ima</ta>
            <ta e="T102" id="Seg_1224" s="T101">meːltɨ</ta>
            <ta e="T103" id="Seg_1225" s="T102">čʼäːŋkɨ</ta>
            <ta e="T104" id="Seg_1226" s="T103">pɨrni</ta>
            <ta e="T105" id="Seg_1227" s="T104">peː-qo-olam-ŋɨ-tɨ</ta>
            <ta e="T106" id="Seg_1228" s="T105">qum-ɨ-t-ɨ-n</ta>
            <ta e="T107" id="Seg_1229" s="T106">qɔːlɨnʼ</ta>
            <ta e="T108" id="Seg_1230" s="T107">*paqqɨ-rɨ-tɨ</ta>
            <ta e="T109" id="Seg_1231" s="T108">kutɨ-n</ta>
            <ta e="T110" id="Seg_1232" s="T109">qɔːlɨnʼ</ta>
            <ta e="T111" id="Seg_1233" s="T110">qapı</ta>
            <ta e="T112" id="Seg_1234" s="T111">qantɨ-mpɨ-ntɨ</ta>
            <ta e="T113" id="Seg_1235" s="T112">značʼit</ta>
            <ta e="T114" id="Seg_1236" s="T113">na</ta>
            <ta e="T115" id="Seg_1237" s="T114">ima</ta>
            <ta e="T116" id="Seg_1238" s="T115">najntɨ</ta>
            <ta e="T117" id="Seg_1239" s="T116">*kolʼlʼɨ-mpɨ-ntɨ-tɨ</ta>
            <ta e="T118" id="Seg_1240" s="T117">na</ta>
            <ta e="T119" id="Seg_1241" s="T118">*paqqɨ-rɨ-ntɨ-tɨ</ta>
            <ta e="T120" id="Seg_1242" s="T119">na</ta>
            <ta e="T121" id="Seg_1243" s="T120">*kolʼlʼɨ-altɨ-ntɨ-tɨ</ta>
            <ta e="T122" id="Seg_1244" s="T121">na</ta>
            <ta e="T123" id="Seg_1245" s="T122">*kolʼlʼɨ-altɨ-ntɨ-tɨ</ta>
            <ta e="T124" id="Seg_1246" s="T123">qum-ɨ-t</ta>
            <ta e="T125" id="Seg_1247" s="T124">muntɨk</ta>
            <ta e="T126" id="Seg_1248" s="T125">qɔːlɨnʼ-ɨ-tɨ</ta>
            <ta e="T127" id="Seg_1249" s="T126">pöt-mpɨ</ta>
            <ta e="T128" id="Seg_1250" s="T127">našša</ta>
            <ta e="T129" id="Seg_1251" s="T128">čʼaptät</ta>
            <ta e="T130" id="Seg_1252" s="T129">qontɨ-ntɨ</ta>
            <ta e="T132" id="Seg_1253" s="T131">qum-ɨ-t</ta>
            <ta e="T133" id="Seg_1254" s="T132">pɨrni</ta>
            <ta e="T134" id="Seg_1255" s="T133">nık</ta>
            <ta e="T135" id="Seg_1256" s="T134">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T136" id="Seg_1257" s="T135">a</ta>
            <ta e="T137" id="Seg_1258" s="T136">to</ta>
            <ta e="T138" id="Seg_1259" s="T137">na</ta>
            <ta e="T139" id="Seg_1260" s="T138">ima</ta>
            <ta e="T140" id="Seg_1261" s="T139">ippɨ-lä</ta>
            <ta e="T141" id="Seg_1262" s="T140">əːtɨ-kɔːlɨ</ta>
            <ta e="T142" id="Seg_1263" s="T141">na</ta>
            <ta e="T143" id="Seg_1264" s="T142">*taŋɨ-altɨ-mpɨ</ta>
            <ta e="T144" id="Seg_1265" s="T143">pɨrni</ta>
            <ta e="T145" id="Seg_1266" s="T144">ponä</ta>
            <ta e="T146" id="Seg_1267" s="T145">tarɨ-lä</ta>
            <ta e="T147" id="Seg_1268" s="T146">toː</ta>
            <ta e="T148" id="Seg_1269" s="T147">tɨː</ta>
            <ta e="T149" id="Seg_1270" s="T148">toː</ta>
            <ta e="T150" id="Seg_1271" s="T149">qən-mpɨ-ntɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_1272" s="T1">one</ta>
            <ta e="T3" id="Seg_1273" s="T2">house-LOC</ta>
            <ta e="T4" id="Seg_1274" s="T3">one</ta>
            <ta e="T5" id="Seg_1275" s="T4">house-LOC</ta>
            <ta e="T6" id="Seg_1276" s="T5">human.being-EP-PL.[NOM]</ta>
            <ta e="T7" id="Seg_1277" s="T6">live-PST.NAR-INFER-3PL</ta>
            <ta e="T8" id="Seg_1278" s="T7">there</ta>
            <ta e="T9" id="Seg_1279" s="T8">this</ta>
            <ta e="T10" id="Seg_1280" s="T9">house-LOC-OBL.3SG</ta>
            <ta e="T11" id="Seg_1281" s="T10">such</ta>
            <ta e="T12" id="Seg_1282" s="T11">woman.[NOM]</ta>
            <ta e="T13" id="Seg_1283" s="T12">be-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T14" id="Seg_1284" s="T13">human.being-EP-PL.[NOM]</ta>
            <ta e="T15" id="Seg_1285" s="T14">get.dark-DRV-CVB</ta>
            <ta e="T16" id="Seg_1286" s="T15">all</ta>
            <ta e="T17" id="Seg_1287" s="T16">down</ta>
            <ta e="T18" id="Seg_1288" s="T17">sleep-3PL</ta>
            <ta e="T19" id="Seg_1289" s="T18">this</ta>
            <ta e="T20" id="Seg_1290" s="T19">woman.[NOM]</ta>
            <ta e="T21" id="Seg_1291" s="T20">day-ADV.LOC</ta>
            <ta e="T22" id="Seg_1292" s="T21">NEG</ta>
            <ta e="T24" id="Seg_1293" s="T23">day-ADV.LOC</ta>
            <ta e="T25" id="Seg_1294" s="T24">sleep-INF-%%-DUR.[3SG.S]</ta>
            <ta e="T26" id="Seg_1295" s="T25">but</ta>
            <ta e="T27" id="Seg_1296" s="T26">darkness-LOC</ta>
            <ta e="T28" id="Seg_1297" s="T27">human.being-EP-PL.[NOM]</ta>
            <ta e="T29" id="Seg_1298" s="T28">sleep-MOM-PFV-ACTN-LOC</ta>
            <ta e="T30" id="Seg_1299" s="T29">human.being-EP-PL.[NOM]</ta>
            <ta e="T31" id="Seg_1300" s="T30">sleep-MOM-PFV-ACTN-LOC-OBL.3SG</ta>
            <ta e="T32" id="Seg_1301" s="T31">then</ta>
            <ta e="T33" id="Seg_1302" s="T32">cauldron-VBLZ-DUR-INF-be.going.to-INF-%%-DUR.[3SG.S]</ta>
            <ta e="T34" id="Seg_1303" s="T33">eat-EP-FRQ-INF-%%-DUR-INF</ta>
            <ta e="T35" id="Seg_1304" s="T34">be.going.to-DUR.[3SG.S]</ta>
            <ta e="T36" id="Seg_1305" s="T35">sew-EP-FRQ-CO-3SG.S</ta>
            <ta e="T37" id="Seg_1306" s="T36">always-%%</ta>
            <ta e="T38" id="Seg_1307" s="T37">such-ADVZ</ta>
            <ta e="T39" id="Seg_1308" s="T38">force-VBLZ-CO.[3SG.S]</ta>
            <ta e="T40" id="Seg_1309" s="T39">day-ADVZ</ta>
            <ta e="T41" id="Seg_1310" s="T40">sleep.[3SG.S]</ta>
            <ta e="T42" id="Seg_1311" s="T41">darkness-LOC</ta>
            <ta e="T43" id="Seg_1312" s="T42">work.[3SG.S]</ta>
            <ta e="T44" id="Seg_1313" s="T43">one</ta>
            <ta e="T45" id="Seg_1314" s="T44">middle-LOC</ta>
            <ta e="T46" id="Seg_1315" s="T45">sit.down-CO.[3SG.S]</ta>
            <ta e="T47" id="Seg_1316" s="T46">sew-EP-FRQ-CO.[3SG.S]</ta>
            <ta e="T48" id="Seg_1317" s="T47">witch.[NOM]</ta>
            <ta e="T49" id="Seg_1318" s="T48">house.[NOM]</ta>
            <ta e="T50" id="Seg_1319" s="T49">come.in-RFL.[3SG.S]</ta>
            <ta e="T51" id="Seg_1320" s="T50">witch.[NOM]</ta>
            <ta e="T52" id="Seg_1321" s="T51">witch.[NOM]</ta>
            <ta e="T53" id="Seg_1322" s="T52">house.[NOM]</ta>
            <ta e="T54" id="Seg_1323" s="T53">come.in-CVB</ta>
            <ta e="T55" id="Seg_1324" s="T54">such-ADVZ</ta>
            <ta e="T56" id="Seg_1325" s="T55">force-VBLZ-CO.[3SG.S]</ta>
            <ta e="T57" id="Seg_1326" s="T56">you.SG.NOM</ta>
            <ta e="T58" id="Seg_1327" s="T57">why</ta>
            <ta e="T59" id="Seg_1328" s="T58">NEG</ta>
            <ta e="T60" id="Seg_1329" s="T59">sleep-2SG.S</ta>
            <ta e="T61" id="Seg_1330" s="T60">woman.[NOM]</ta>
            <ta e="T62" id="Seg_1331" s="T61">get.scared-DRV-PST.NAR.[3SG.S]</ta>
            <ta e="T63" id="Seg_1332" s="T62">I.NOM</ta>
            <ta e="T64" id="Seg_1333" s="T63">you.SG.ACC</ta>
            <ta e="T65" id="Seg_1334" s="T64">now</ta>
            <ta e="T66" id="Seg_1335" s="T65">up</ta>
            <ta e="T67" id="Seg_1336" s="T66">eat-FUT-1SG.S</ta>
            <ta e="T68" id="Seg_1337" s="T67">only</ta>
            <ta e="T69" id="Seg_1338" s="T68">home</ta>
            <ta e="T70" id="Seg_1339" s="T69">%%-FUT-1SG.S</ta>
            <ta e="T71" id="Seg_1340" s="T70">witch.[NOM]</ta>
            <ta e="T72" id="Seg_1341" s="T71">home</ta>
            <ta e="T73" id="Seg_1342" s="T72">go-MOM-CO.[3SG.S]</ta>
            <ta e="T74" id="Seg_1343" s="T73">woman.[NOM]</ta>
            <ta e="T75" id="Seg_1344" s="T74">get.scared-DRV-CVB</ta>
            <ta e="T76" id="Seg_1345" s="T75">ointment.from.fish.entrails.[NOM]</ta>
            <ta e="T77" id="Seg_1346" s="T76">container.[NOM]</ta>
            <ta e="T78" id="Seg_1347" s="T77">catch-CVB</ta>
            <ta e="T79" id="Seg_1348" s="T78">armpit.[NOM]-3SG</ta>
            <ta e="T80" id="Seg_1349" s="T79">ointment.from.fish.entrails-INSTR</ta>
            <ta e="T81" id="Seg_1350" s="T80">anoint-MULO-PFV-3SG.S</ta>
            <ta e="T82" id="Seg_1351" s="T81">here</ta>
            <ta e="T83" id="Seg_1352" s="T82">so</ta>
            <ta e="T84" id="Seg_1353" s="T83">hurry-ATTEN-DRV-CVB</ta>
            <ta e="T85" id="Seg_1354" s="T84">human.being-EP-PL-EP-GEN</ta>
            <ta e="T86" id="Seg_1355" s="T85">inside-ADV.ILL</ta>
            <ta e="T87" id="Seg_1356" s="T86">go.down-DRV-CVB</ta>
            <ta e="T88" id="Seg_1357" s="T87">down</ta>
            <ta e="T89" id="Seg_1358" s="T88">sleep-CO.[3SG.S]</ta>
            <ta e="T91" id="Seg_1359" s="T90">witch.[NOM]</ta>
            <ta e="T92" id="Seg_1360" s="T91">hardly</ta>
            <ta e="T93" id="Seg_1361" s="T92">come-%%-CO.[3SG.S]</ta>
            <ta e="T94" id="Seg_1362" s="T93">woman.[NOM]</ta>
            <ta e="T95" id="Seg_1363" s="T94">all.the.time</ta>
            <ta e="T96" id="Seg_1364" s="T95">NEG.EX-3SG.S</ta>
            <ta e="T97" id="Seg_1365" s="T96">here-%%</ta>
            <ta e="T98" id="Seg_1366" s="T97">give.a.look-PFV-CO.[3SG.S]</ta>
            <ta e="T99" id="Seg_1367" s="T98">there-%%</ta>
            <ta e="T100" id="Seg_1368" s="T99">give.a.look-PFV-CO.[3SG.S]</ta>
            <ta e="T101" id="Seg_1369" s="T100">woman.[NOM]</ta>
            <ta e="T102" id="Seg_1370" s="T101">all.the.time</ta>
            <ta e="T103" id="Seg_1371" s="T102">NEG.EX.[3SG.S]</ta>
            <ta e="T104" id="Seg_1372" s="T103">witch.[NOM]</ta>
            <ta e="T105" id="Seg_1373" s="T104">look.for-INF-be.going.to-CO-3SG.O</ta>
            <ta e="T106" id="Seg_1374" s="T105">human.being-EP-PL-EP-GEN</ta>
            <ta e="T107" id="Seg_1375" s="T106">armpit.[NOM]</ta>
            <ta e="T108" id="Seg_1376" s="T107">touch-TR-3SG.O</ta>
            <ta e="T109" id="Seg_1377" s="T108">who-GEN</ta>
            <ta e="T110" id="Seg_1378" s="T109">armpit.[NOM]</ta>
            <ta e="T111" id="Seg_1379" s="T110">supposedly</ta>
            <ta e="T112" id="Seg_1380" s="T111">freeze-RES-INFER.[3SG.S]</ta>
            <ta e="T113" id="Seg_1381" s="T112">then</ta>
            <ta e="T114" id="Seg_1382" s="T113">this</ta>
            <ta e="T115" id="Seg_1383" s="T114">woman.[NOM]</ta>
            <ta e="T116" id="Seg_1384" s="T115">here.it.is.[3SG.S]</ta>
            <ta e="T117" id="Seg_1385" s="T116">go.round-DUR-INFER-3SG.O</ta>
            <ta e="T118" id="Seg_1386" s="T117">INFER</ta>
            <ta e="T119" id="Seg_1387" s="T118">touch-TR-INFER-3SG.O</ta>
            <ta e="T120" id="Seg_1388" s="T119">INFER</ta>
            <ta e="T121" id="Seg_1389" s="T120">turn-TR-INFER-3SG.O</ta>
            <ta e="T122" id="Seg_1390" s="T121">INFER</ta>
            <ta e="T123" id="Seg_1391" s="T122">turn-TR-INFER-3SG.O</ta>
            <ta e="T124" id="Seg_1392" s="T123">human.being-EP-PL.[NOM]</ta>
            <ta e="T125" id="Seg_1393" s="T124">all</ta>
            <ta e="T126" id="Seg_1394" s="T125">armpit-EP.[NOM]-3SG</ta>
            <ta e="T127" id="Seg_1395" s="T126">warm-RES.[3SG.S]</ta>
            <ta e="T128" id="Seg_1396" s="T127">numerous</ta>
            <ta e="T129" id="Seg_1397" s="T128">%%</ta>
            <ta e="T130" id="Seg_1398" s="T129">sleep-INFER.[3SG.S]</ta>
            <ta e="T132" id="Seg_1399" s="T131">human.being-EP-PL.[NOM]</ta>
            <ta e="T133" id="Seg_1400" s="T132">witch.[NOM]</ta>
            <ta e="T134" id="Seg_1401" s="T133">so</ta>
            <ta e="T135" id="Seg_1402" s="T134">say-CO-3SG.O</ta>
            <ta e="T136" id="Seg_1403" s="T135">and</ta>
            <ta e="T137" id="Seg_1404" s="T136">that</ta>
            <ta e="T138" id="Seg_1405" s="T137">this</ta>
            <ta e="T139" id="Seg_1406" s="T138">woman.[NOM]</ta>
            <ta e="T140" id="Seg_1407" s="T139">lie-CVB</ta>
            <ta e="T141" id="Seg_1408" s="T140">word-CAR</ta>
            <ta e="T142" id="Seg_1409" s="T141">here</ta>
            <ta e="T143" id="Seg_1410" s="T142">go.quiet-TR-DUR.[3SG.S]</ta>
            <ta e="T144" id="Seg_1411" s="T143">witch.[NOM]</ta>
            <ta e="T145" id="Seg_1412" s="T144">outwards</ta>
            <ta e="T146" id="Seg_1413" s="T145">go.out-CVB</ta>
            <ta e="T147" id="Seg_1414" s="T146">away</ta>
            <ta e="T148" id="Seg_1415" s="T147">here</ta>
            <ta e="T149" id="Seg_1416" s="T148">away</ta>
            <ta e="T150" id="Seg_1417" s="T149">go.away-PST.NAR-INFER.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_1418" s="T1">один</ta>
            <ta e="T3" id="Seg_1419" s="T2">дом-LOC</ta>
            <ta e="T4" id="Seg_1420" s="T3">один</ta>
            <ta e="T5" id="Seg_1421" s="T4">дом-LOC</ta>
            <ta e="T6" id="Seg_1422" s="T5">человек-EP-PL.[NOM]</ta>
            <ta e="T7" id="Seg_1423" s="T6">жить-PST.NAR-INFER-3PL</ta>
            <ta e="T8" id="Seg_1424" s="T7">там</ta>
            <ta e="T9" id="Seg_1425" s="T8">этот</ta>
            <ta e="T10" id="Seg_1426" s="T9">дом-LOC-OBL.3SG</ta>
            <ta e="T11" id="Seg_1427" s="T10">такой</ta>
            <ta e="T12" id="Seg_1428" s="T11">женщина.[NOM]</ta>
            <ta e="T13" id="Seg_1429" s="T12">быть-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T14" id="Seg_1430" s="T13">человек-EP-PL.[NOM]</ta>
            <ta e="T15" id="Seg_1431" s="T14">стемнеть-DRV-CVB</ta>
            <ta e="T16" id="Seg_1432" s="T15">всё</ta>
            <ta e="T17" id="Seg_1433" s="T16">вниз</ta>
            <ta e="T18" id="Seg_1434" s="T17">спать-3PL</ta>
            <ta e="T19" id="Seg_1435" s="T18">этот</ta>
            <ta e="T20" id="Seg_1436" s="T19">женщина.[NOM]</ta>
            <ta e="T21" id="Seg_1437" s="T20">день-ADV.LOC</ta>
            <ta e="T22" id="Seg_1438" s="T21">NEG</ta>
            <ta e="T24" id="Seg_1439" s="T23">день-ADV.LOC</ta>
            <ta e="T25" id="Seg_1440" s="T24">спать-INF-%%-DUR.[3SG.S]</ta>
            <ta e="T26" id="Seg_1441" s="T25">а</ta>
            <ta e="T27" id="Seg_1442" s="T26">темнота-LOC</ta>
            <ta e="T28" id="Seg_1443" s="T27">человек-EP-PL.[NOM]</ta>
            <ta e="T29" id="Seg_1444" s="T28">спать-MOM-PFV-ACTN-LOC</ta>
            <ta e="T30" id="Seg_1445" s="T29">человек-EP-PL.[NOM]</ta>
            <ta e="T31" id="Seg_1446" s="T30">спать-MOM-PFV-ACTN-LOC-OBL.3SG</ta>
            <ta e="T32" id="Seg_1447" s="T31">затем</ta>
            <ta e="T33" id="Seg_1448" s="T32">котёл-VBLZ-DUR-INF-собраться-INF-%%-DUR.[3SG.S]</ta>
            <ta e="T34" id="Seg_1449" s="T33">съесть-EP-FRQ-INF-%%-DUR-INF</ta>
            <ta e="T35" id="Seg_1450" s="T34">собраться-DUR.[3SG.S]</ta>
            <ta e="T36" id="Seg_1451" s="T35">сшить-EP-FRQ-CO-3SG.S</ta>
            <ta e="T37" id="Seg_1452" s="T36">всегда-%%</ta>
            <ta e="T38" id="Seg_1453" s="T37">такой-ADVZ</ta>
            <ta e="T39" id="Seg_1454" s="T38">сила-VBLZ-CO.[3SG.S]</ta>
            <ta e="T40" id="Seg_1455" s="T39">день-ADVZ</ta>
            <ta e="T41" id="Seg_1456" s="T40">спать.[3SG.S]</ta>
            <ta e="T42" id="Seg_1457" s="T41">темнота-LOC</ta>
            <ta e="T43" id="Seg_1458" s="T42">работать.[3SG.S]</ta>
            <ta e="T44" id="Seg_1459" s="T43">один</ta>
            <ta e="T45" id="Seg_1460" s="T44">середина-LOC</ta>
            <ta e="T46" id="Seg_1461" s="T45">сесть-CO.[3SG.S]</ta>
            <ta e="T47" id="Seg_1462" s="T46">сшить-EP-FRQ-CO.[3SG.S]</ta>
            <ta e="T48" id="Seg_1463" s="T47">ведьма.[NOM]</ta>
            <ta e="T49" id="Seg_1464" s="T48">дом.[NOM]</ta>
            <ta e="T50" id="Seg_1465" s="T49">войти-RFL.[3SG.S]</ta>
            <ta e="T51" id="Seg_1466" s="T50">ведьма.[NOM]</ta>
            <ta e="T52" id="Seg_1467" s="T51">ведьма.[NOM]</ta>
            <ta e="T53" id="Seg_1468" s="T52">дом.[NOM]</ta>
            <ta e="T54" id="Seg_1469" s="T53">войти-CVB</ta>
            <ta e="T55" id="Seg_1470" s="T54">такой-ADVZ</ta>
            <ta e="T56" id="Seg_1471" s="T55">сила-VBLZ-CO.[3SG.S]</ta>
            <ta e="T57" id="Seg_1472" s="T56">ты.NOM</ta>
            <ta e="T58" id="Seg_1473" s="T57">почему</ta>
            <ta e="T59" id="Seg_1474" s="T58">NEG</ta>
            <ta e="T60" id="Seg_1475" s="T59">спать-2SG.S</ta>
            <ta e="T61" id="Seg_1476" s="T60">женщина.[NOM]</ta>
            <ta e="T62" id="Seg_1477" s="T61">испугаться-DRV-PST.NAR.[3SG.S]</ta>
            <ta e="T63" id="Seg_1478" s="T62">я.NOM</ta>
            <ta e="T64" id="Seg_1479" s="T63">ты.ACC</ta>
            <ta e="T65" id="Seg_1480" s="T64">сейчас</ta>
            <ta e="T66" id="Seg_1481" s="T65">вверх</ta>
            <ta e="T67" id="Seg_1482" s="T66">съесть-FUT-1SG.S</ta>
            <ta e="T68" id="Seg_1483" s="T67">только</ta>
            <ta e="T69" id="Seg_1484" s="T68">домой</ta>
            <ta e="T70" id="Seg_1485" s="T69">%%-FUT-1SG.S</ta>
            <ta e="T71" id="Seg_1486" s="T70">ведьма.[NOM]</ta>
            <ta e="T72" id="Seg_1487" s="T71">домой</ta>
            <ta e="T73" id="Seg_1488" s="T72">идти-MOM-CO.[3SG.S]</ta>
            <ta e="T74" id="Seg_1489" s="T73">женщина.[NOM]</ta>
            <ta e="T75" id="Seg_1490" s="T74">испугаться-DRV-CVB</ta>
            <ta e="T76" id="Seg_1491" s="T75">мазь.из.рыбьих.внутренностей.[NOM]</ta>
            <ta e="T77" id="Seg_1492" s="T76">ёмкость.[NOM]</ta>
            <ta e="T78" id="Seg_1493" s="T77">схватить-CVB</ta>
            <ta e="T79" id="Seg_1494" s="T78">подмышка.[NOM]-3SG</ta>
            <ta e="T80" id="Seg_1495" s="T79">мазь.из.рыбьих.внутренностей-INSTR</ta>
            <ta e="T81" id="Seg_1496" s="T80">мазать-MULO-PFV-3SG.S</ta>
            <ta e="T82" id="Seg_1497" s="T81">вот</ta>
            <ta e="T83" id="Seg_1498" s="T82">так</ta>
            <ta e="T84" id="Seg_1499" s="T83">торопиться-ATTEN-DRV-CVB</ta>
            <ta e="T85" id="Seg_1500" s="T84">человек-EP-PL-EP-GEN</ta>
            <ta e="T86" id="Seg_1501" s="T85">внутри-ADV.ILL</ta>
            <ta e="T87" id="Seg_1502" s="T86">залезть-DRV-CVB</ta>
            <ta e="T88" id="Seg_1503" s="T87">вниз</ta>
            <ta e="T89" id="Seg_1504" s="T88">спать-CO.[3SG.S]</ta>
            <ta e="T91" id="Seg_1505" s="T90">ведьма.[NOM]</ta>
            <ta e="T92" id="Seg_1506" s="T91">едва</ta>
            <ta e="T93" id="Seg_1507" s="T92">прийти-%%-CO.[3SG.S]</ta>
            <ta e="T94" id="Seg_1508" s="T93">женщина.[NOM]</ta>
            <ta e="T95" id="Seg_1509" s="T94">всё.время</ta>
            <ta e="T96" id="Seg_1510" s="T95">NEG.EX-3SG.S</ta>
            <ta e="T97" id="Seg_1511" s="T96">сюда-%%</ta>
            <ta e="T98" id="Seg_1512" s="T97">взглянуть-PFV-CO.[3SG.S]</ta>
            <ta e="T99" id="Seg_1513" s="T98">туда-%%</ta>
            <ta e="T100" id="Seg_1514" s="T99">взглянуть-PFV-CO.[3SG.S]</ta>
            <ta e="T101" id="Seg_1515" s="T100">женщина.[NOM]</ta>
            <ta e="T102" id="Seg_1516" s="T101">всё.время</ta>
            <ta e="T103" id="Seg_1517" s="T102">NEG.EX.[3SG.S]</ta>
            <ta e="T104" id="Seg_1518" s="T103">ведьма.[NOM]</ta>
            <ta e="T105" id="Seg_1519" s="T104">искать-INF-собраться-CO-3SG.O</ta>
            <ta e="T106" id="Seg_1520" s="T105">человек-EP-PL-EP-GEN</ta>
            <ta e="T107" id="Seg_1521" s="T106">подмышка.[NOM]</ta>
            <ta e="T108" id="Seg_1522" s="T107">щупать-TR-3SG.O</ta>
            <ta e="T109" id="Seg_1523" s="T108">кто-GEN</ta>
            <ta e="T110" id="Seg_1524" s="T109">подмышка.[NOM]</ta>
            <ta e="T111" id="Seg_1525" s="T110">вроде</ta>
            <ta e="T112" id="Seg_1526" s="T111">замерзнуть-RES-INFER.[3SG.S]</ta>
            <ta e="T113" id="Seg_1527" s="T112">значит</ta>
            <ta e="T114" id="Seg_1528" s="T113">этот</ta>
            <ta e="T115" id="Seg_1529" s="T114">женщина.[NOM]</ta>
            <ta e="T116" id="Seg_1530" s="T115">вот.он.есть.[3SG.S]</ta>
            <ta e="T117" id="Seg_1531" s="T116">кружиться-DUR-INFER-3SG.O</ta>
            <ta e="T118" id="Seg_1532" s="T117">INFER</ta>
            <ta e="T119" id="Seg_1533" s="T118">щупать-TR-INFER-3SG.O</ta>
            <ta e="T120" id="Seg_1534" s="T119">INFER</ta>
            <ta e="T121" id="Seg_1535" s="T120">повернуться-TR-INFER-3SG.O</ta>
            <ta e="T122" id="Seg_1536" s="T121">INFER</ta>
            <ta e="T123" id="Seg_1537" s="T122">повернуться-TR-INFER-3SG.O</ta>
            <ta e="T124" id="Seg_1538" s="T123">человек-EP-PL.[NOM]</ta>
            <ta e="T125" id="Seg_1539" s="T124">всё</ta>
            <ta e="T126" id="Seg_1540" s="T125">подмышка-EP.[NOM]-3SG</ta>
            <ta e="T127" id="Seg_1541" s="T126">нагреться-RES.[3SG.S]</ta>
            <ta e="T128" id="Seg_1542" s="T127">многочисленный</ta>
            <ta e="T129" id="Seg_1543" s="T128">%%</ta>
            <ta e="T130" id="Seg_1544" s="T129">спать-INFER.[3SG.S]</ta>
            <ta e="T132" id="Seg_1545" s="T131">человек-EP-PL.[NOM]</ta>
            <ta e="T133" id="Seg_1546" s="T132">ведьма.[NOM]</ta>
            <ta e="T134" id="Seg_1547" s="T133">так</ta>
            <ta e="T135" id="Seg_1548" s="T134">сказать-CO-3SG.O</ta>
            <ta e="T136" id="Seg_1549" s="T135">а</ta>
            <ta e="T137" id="Seg_1550" s="T136">тот</ta>
            <ta e="T138" id="Seg_1551" s="T137">этот</ta>
            <ta e="T139" id="Seg_1552" s="T138">женщина.[NOM]</ta>
            <ta e="T140" id="Seg_1553" s="T139">лежать-CVB</ta>
            <ta e="T141" id="Seg_1554" s="T140">слово-CAR</ta>
            <ta e="T142" id="Seg_1555" s="T141">вот</ta>
            <ta e="T143" id="Seg_1556" s="T142">притихнуть-TR-DUR.[3SG.S]</ta>
            <ta e="T144" id="Seg_1557" s="T143">ведьма.[NOM]</ta>
            <ta e="T145" id="Seg_1558" s="T144">наружу</ta>
            <ta e="T146" id="Seg_1559" s="T145">выйти-CVB</ta>
            <ta e="T147" id="Seg_1560" s="T146">прочь</ta>
            <ta e="T148" id="Seg_1561" s="T147">сюда</ta>
            <ta e="T149" id="Seg_1562" s="T148">прочь</ta>
            <ta e="T150" id="Seg_1563" s="T149">уйти-PST.NAR-INFER.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1564" s="T1">num</ta>
            <ta e="T3" id="Seg_1565" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_1566" s="T3">num</ta>
            <ta e="T5" id="Seg_1567" s="T4">n-n:case</ta>
            <ta e="T6" id="Seg_1568" s="T5">n-n:ins-n:num-n:case</ta>
            <ta e="T7" id="Seg_1569" s="T6">v-v:tense-v:mood-v:pn</ta>
            <ta e="T8" id="Seg_1570" s="T7">adv</ta>
            <ta e="T9" id="Seg_1571" s="T8">dem</ta>
            <ta e="T10" id="Seg_1572" s="T9">n-n:case-n:obl.poss</ta>
            <ta e="T11" id="Seg_1573" s="T10">dem</ta>
            <ta e="T12" id="Seg_1574" s="T11">n-n:case</ta>
            <ta e="T13" id="Seg_1575" s="T12">v-v:tense-v:mood-v:pn</ta>
            <ta e="T14" id="Seg_1576" s="T13">n-n:ins-n:num-n:case</ta>
            <ta e="T15" id="Seg_1577" s="T14">v-v&gt;v-v&gt;adv</ta>
            <ta e="T16" id="Seg_1578" s="T15">quant</ta>
            <ta e="T17" id="Seg_1579" s="T16">preverb</ta>
            <ta e="T18" id="Seg_1580" s="T17">v-v:pn</ta>
            <ta e="T19" id="Seg_1581" s="T18">dem</ta>
            <ta e="T20" id="Seg_1582" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_1583" s="T20">n-n&gt;adv</ta>
            <ta e="T22" id="Seg_1584" s="T21">ptcl</ta>
            <ta e="T24" id="Seg_1585" s="T23">n-n&gt;adv</ta>
            <ta e="T25" id="Seg_1586" s="T24">v-v:inf-%%-v&gt;v-v:pn</ta>
            <ta e="T26" id="Seg_1587" s="T25">conj</ta>
            <ta e="T27" id="Seg_1588" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_1589" s="T27">n-n:ins-n:num-n:case</ta>
            <ta e="T29" id="Seg_1590" s="T28">v-v&gt;v-v&gt;v-v&gt;n-n:case</ta>
            <ta e="T30" id="Seg_1591" s="T29">n-n:ins-n:num-n:case</ta>
            <ta e="T31" id="Seg_1592" s="T30">v-v&gt;v-v&gt;v-v&gt;n-n:case-n:obl.poss</ta>
            <ta e="T32" id="Seg_1593" s="T31">adv</ta>
            <ta e="T33" id="Seg_1594" s="T32">n-n&gt;v-v&gt;v-v:inf-v-v:inf-%%-v&gt;v-v:pn</ta>
            <ta e="T34" id="Seg_1595" s="T33">v-v:ins-v&gt;v-v:inf-%%-v&gt;v-v:inf</ta>
            <ta e="T35" id="Seg_1596" s="T34">v-v&gt;v-v:pn</ta>
            <ta e="T36" id="Seg_1597" s="T35">v-v:ins-v&gt;v-v:ins-v:pn</ta>
            <ta e="T37" id="Seg_1598" s="T36">adv-%%</ta>
            <ta e="T38" id="Seg_1599" s="T37">dem-adj&gt;adv</ta>
            <ta e="T39" id="Seg_1600" s="T38">n-n&gt;v-v:ins-v:pn</ta>
            <ta e="T40" id="Seg_1601" s="T39">n-n&gt;adv</ta>
            <ta e="T41" id="Seg_1602" s="T40">v-v:pn</ta>
            <ta e="T42" id="Seg_1603" s="T41">n-n:case</ta>
            <ta e="T43" id="Seg_1604" s="T42">v-v:pn</ta>
            <ta e="T44" id="Seg_1605" s="T43">num</ta>
            <ta e="T45" id="Seg_1606" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_1607" s="T45">v-v:ins-v:pn</ta>
            <ta e="T47" id="Seg_1608" s="T46">v-v:ins-v&gt;v-v:ins-v:pn</ta>
            <ta e="T48" id="Seg_1609" s="T47">n-n:case</ta>
            <ta e="T49" id="Seg_1610" s="T48">n-n:case</ta>
            <ta e="T50" id="Seg_1611" s="T49">v-v&gt;v-v:pn</ta>
            <ta e="T51" id="Seg_1612" s="T50">n-n:case</ta>
            <ta e="T52" id="Seg_1613" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_1614" s="T52">n-n:case</ta>
            <ta e="T54" id="Seg_1615" s="T53">v-v&gt;adv</ta>
            <ta e="T55" id="Seg_1616" s="T54">dem-adj&gt;adv</ta>
            <ta e="T56" id="Seg_1617" s="T55">n-n&gt;v-v:ins-v:pn</ta>
            <ta e="T57" id="Seg_1618" s="T56">pers</ta>
            <ta e="T58" id="Seg_1619" s="T57">interrog</ta>
            <ta e="T59" id="Seg_1620" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_1621" s="T59">v-v:pn</ta>
            <ta e="T61" id="Seg_1622" s="T60">n-n:case</ta>
            <ta e="T62" id="Seg_1623" s="T61">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T63" id="Seg_1624" s="T62">pers</ta>
            <ta e="T64" id="Seg_1625" s="T63">pers</ta>
            <ta e="T65" id="Seg_1626" s="T64">adv</ta>
            <ta e="T66" id="Seg_1627" s="T65">preverb</ta>
            <ta e="T67" id="Seg_1628" s="T66">v-v:tense-v:pn</ta>
            <ta e="T68" id="Seg_1629" s="T67">adv</ta>
            <ta e="T69" id="Seg_1630" s="T68">adv</ta>
            <ta e="T70" id="Seg_1631" s="T69">v-v:tense-v:pn</ta>
            <ta e="T71" id="Seg_1632" s="T70">n-n:case</ta>
            <ta e="T72" id="Seg_1633" s="T71">adv</ta>
            <ta e="T73" id="Seg_1634" s="T72">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T74" id="Seg_1635" s="T73">n-n:case</ta>
            <ta e="T75" id="Seg_1636" s="T74">v-v&gt;v-v&gt;adv</ta>
            <ta e="T76" id="Seg_1637" s="T75">n-n:case</ta>
            <ta e="T77" id="Seg_1638" s="T76">n-n:case</ta>
            <ta e="T78" id="Seg_1639" s="T77">v-v&gt;adv</ta>
            <ta e="T79" id="Seg_1640" s="T78">n-n:case-n:poss</ta>
            <ta e="T80" id="Seg_1641" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_1642" s="T80">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T82" id="Seg_1643" s="T81">ptcl</ta>
            <ta e="T83" id="Seg_1644" s="T82">adv</ta>
            <ta e="T84" id="Seg_1645" s="T83">v-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T85" id="Seg_1646" s="T84">n-n:ins-n:num-n:ins-n:case</ta>
            <ta e="T86" id="Seg_1647" s="T85">pp-adv:case</ta>
            <ta e="T87" id="Seg_1648" s="T86">v-v&gt;v-v&gt;adv</ta>
            <ta e="T88" id="Seg_1649" s="T87">preverb</ta>
            <ta e="T89" id="Seg_1650" s="T88">v-v:ins-v:pn</ta>
            <ta e="T91" id="Seg_1651" s="T90">n-n:case</ta>
            <ta e="T92" id="Seg_1652" s="T91">ptcl</ta>
            <ta e="T93" id="Seg_1653" s="T92">v-%%-v:ins-v:pn</ta>
            <ta e="T94" id="Seg_1654" s="T93">n-n:case</ta>
            <ta e="T95" id="Seg_1655" s="T94">adv</ta>
            <ta e="T96" id="Seg_1656" s="T95">v-v:pn</ta>
            <ta e="T97" id="Seg_1657" s="T96">adv-%%</ta>
            <ta e="T98" id="Seg_1658" s="T97">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T99" id="Seg_1659" s="T98">adv-%%</ta>
            <ta e="T100" id="Seg_1660" s="T99">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T101" id="Seg_1661" s="T100">n-n:case</ta>
            <ta e="T102" id="Seg_1662" s="T101">adv</ta>
            <ta e="T103" id="Seg_1663" s="T102">v-v:pn</ta>
            <ta e="T104" id="Seg_1664" s="T103">n-n:case</ta>
            <ta e="T105" id="Seg_1665" s="T104">v-v:inf-v-v:ins-v:pn</ta>
            <ta e="T106" id="Seg_1666" s="T105">n-n:ins-n:num-n:ins-n:case</ta>
            <ta e="T107" id="Seg_1667" s="T106">n-n:case</ta>
            <ta e="T108" id="Seg_1668" s="T107">v-v&gt;v-v:pn</ta>
            <ta e="T109" id="Seg_1669" s="T108">interrog-n:case</ta>
            <ta e="T110" id="Seg_1670" s="T109">n-n:case</ta>
            <ta e="T111" id="Seg_1671" s="T110">ptcl</ta>
            <ta e="T112" id="Seg_1672" s="T111">v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T113" id="Seg_1673" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_1674" s="T113">dem</ta>
            <ta e="T115" id="Seg_1675" s="T114">n-n:case</ta>
            <ta e="T116" id="Seg_1676" s="T115">v-v:pn</ta>
            <ta e="T117" id="Seg_1677" s="T116">v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T118" id="Seg_1678" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_1679" s="T118">v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T120" id="Seg_1680" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_1681" s="T120">v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T122" id="Seg_1682" s="T121">ptcl</ta>
            <ta e="T123" id="Seg_1683" s="T122">v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T124" id="Seg_1684" s="T123">n-n:ins-n:num-n:case</ta>
            <ta e="T125" id="Seg_1685" s="T124">quant</ta>
            <ta e="T126" id="Seg_1686" s="T125">n-n:ins-n:case-n:poss</ta>
            <ta e="T127" id="Seg_1687" s="T126">v-v&gt;v-v:pn</ta>
            <ta e="T128" id="Seg_1688" s="T127">adj</ta>
            <ta e="T129" id="Seg_1689" s="T128">%%</ta>
            <ta e="T130" id="Seg_1690" s="T129">v-v:tense.mood-v:pn</ta>
            <ta e="T132" id="Seg_1691" s="T131">n-n:ins-n:num-n:case</ta>
            <ta e="T133" id="Seg_1692" s="T132">n-n:case</ta>
            <ta e="T134" id="Seg_1693" s="T133">adv</ta>
            <ta e="T135" id="Seg_1694" s="T134">v-v:ins-v:pn</ta>
            <ta e="T136" id="Seg_1695" s="T135">conj</ta>
            <ta e="T137" id="Seg_1696" s="T136">dem</ta>
            <ta e="T138" id="Seg_1697" s="T137">dem</ta>
            <ta e="T139" id="Seg_1698" s="T138">n-n:case</ta>
            <ta e="T140" id="Seg_1699" s="T139">v-v&gt;adv</ta>
            <ta e="T141" id="Seg_1700" s="T140">n-n&gt;adj</ta>
            <ta e="T142" id="Seg_1701" s="T141">ptcl</ta>
            <ta e="T143" id="Seg_1702" s="T142">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T144" id="Seg_1703" s="T143">n-n:case</ta>
            <ta e="T145" id="Seg_1704" s="T144">adv</ta>
            <ta e="T146" id="Seg_1705" s="T145">v-v&gt;adv</ta>
            <ta e="T147" id="Seg_1706" s="T146">adv</ta>
            <ta e="T148" id="Seg_1707" s="T147">adv</ta>
            <ta e="T149" id="Seg_1708" s="T148">preverb</ta>
            <ta e="T150" id="Seg_1709" s="T149">v-v:tense-v:mood-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1710" s="T1">num</ta>
            <ta e="T3" id="Seg_1711" s="T2">n</ta>
            <ta e="T4" id="Seg_1712" s="T3">num</ta>
            <ta e="T5" id="Seg_1713" s="T4">n</ta>
            <ta e="T6" id="Seg_1714" s="T5">n</ta>
            <ta e="T7" id="Seg_1715" s="T6">v</ta>
            <ta e="T8" id="Seg_1716" s="T7">adv</ta>
            <ta e="T9" id="Seg_1717" s="T8">dem</ta>
            <ta e="T10" id="Seg_1718" s="T9">n</ta>
            <ta e="T11" id="Seg_1719" s="T10">dem</ta>
            <ta e="T12" id="Seg_1720" s="T11">n</ta>
            <ta e="T13" id="Seg_1721" s="T12">v</ta>
            <ta e="T14" id="Seg_1722" s="T13">n</ta>
            <ta e="T15" id="Seg_1723" s="T14">adv</ta>
            <ta e="T16" id="Seg_1724" s="T15">quant</ta>
            <ta e="T17" id="Seg_1725" s="T16">preverb</ta>
            <ta e="T18" id="Seg_1726" s="T17">v</ta>
            <ta e="T19" id="Seg_1727" s="T18">dem</ta>
            <ta e="T20" id="Seg_1728" s="T19">n</ta>
            <ta e="T21" id="Seg_1729" s="T20">adv</ta>
            <ta e="T22" id="Seg_1730" s="T21">ptcl</ta>
            <ta e="T24" id="Seg_1731" s="T23">adv</ta>
            <ta e="T25" id="Seg_1732" s="T24">v</ta>
            <ta e="T26" id="Seg_1733" s="T25">conj</ta>
            <ta e="T27" id="Seg_1734" s="T26">n</ta>
            <ta e="T28" id="Seg_1735" s="T27">n</ta>
            <ta e="T29" id="Seg_1736" s="T28">n</ta>
            <ta e="T30" id="Seg_1737" s="T29">n</ta>
            <ta e="T31" id="Seg_1738" s="T30">n</ta>
            <ta e="T32" id="Seg_1739" s="T31">adv</ta>
            <ta e="T33" id="Seg_1740" s="T32">v</ta>
            <ta e="T34" id="Seg_1741" s="T33">v</ta>
            <ta e="T35" id="Seg_1742" s="T34">v</ta>
            <ta e="T36" id="Seg_1743" s="T35">v</ta>
            <ta e="T37" id="Seg_1744" s="T36">adv</ta>
            <ta e="T38" id="Seg_1745" s="T37">adv</ta>
            <ta e="T39" id="Seg_1746" s="T38">v</ta>
            <ta e="T40" id="Seg_1747" s="T39">adv</ta>
            <ta e="T41" id="Seg_1748" s="T40">v</ta>
            <ta e="T42" id="Seg_1749" s="T41">n</ta>
            <ta e="T43" id="Seg_1750" s="T42">v</ta>
            <ta e="T44" id="Seg_1751" s="T43">num</ta>
            <ta e="T45" id="Seg_1752" s="T44">n</ta>
            <ta e="T46" id="Seg_1753" s="T45">v</ta>
            <ta e="T47" id="Seg_1754" s="T46">v</ta>
            <ta e="T48" id="Seg_1755" s="T47">n</ta>
            <ta e="T49" id="Seg_1756" s="T48">n</ta>
            <ta e="T50" id="Seg_1757" s="T49">v</ta>
            <ta e="T51" id="Seg_1758" s="T50">n</ta>
            <ta e="T52" id="Seg_1759" s="T51">n</ta>
            <ta e="T53" id="Seg_1760" s="T52">n</ta>
            <ta e="T54" id="Seg_1761" s="T53">adv</ta>
            <ta e="T55" id="Seg_1762" s="T54">adv</ta>
            <ta e="T56" id="Seg_1763" s="T55">v</ta>
            <ta e="T57" id="Seg_1764" s="T56">pers</ta>
            <ta e="T58" id="Seg_1765" s="T57">interrog</ta>
            <ta e="T59" id="Seg_1766" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_1767" s="T59">v</ta>
            <ta e="T61" id="Seg_1768" s="T60">n</ta>
            <ta e="T62" id="Seg_1769" s="T61">v</ta>
            <ta e="T63" id="Seg_1770" s="T62">pers</ta>
            <ta e="T64" id="Seg_1771" s="T63">pers</ta>
            <ta e="T65" id="Seg_1772" s="T64">adv</ta>
            <ta e="T66" id="Seg_1773" s="T65">preverb</ta>
            <ta e="T67" id="Seg_1774" s="T66">v</ta>
            <ta e="T68" id="Seg_1775" s="T67">adv</ta>
            <ta e="T69" id="Seg_1776" s="T68">adv</ta>
            <ta e="T70" id="Seg_1777" s="T69">v</ta>
            <ta e="T71" id="Seg_1778" s="T70">n</ta>
            <ta e="T72" id="Seg_1779" s="T71">adv</ta>
            <ta e="T73" id="Seg_1780" s="T72">v</ta>
            <ta e="T74" id="Seg_1781" s="T73">n</ta>
            <ta e="T75" id="Seg_1782" s="T74">v</ta>
            <ta e="T76" id="Seg_1783" s="T75">n</ta>
            <ta e="T77" id="Seg_1784" s="T76">n</ta>
            <ta e="T78" id="Seg_1785" s="T77">adv</ta>
            <ta e="T79" id="Seg_1786" s="T78">n</ta>
            <ta e="T80" id="Seg_1787" s="T79">n</ta>
            <ta e="T81" id="Seg_1788" s="T80">v</ta>
            <ta e="T82" id="Seg_1789" s="T81">pro</ta>
            <ta e="T83" id="Seg_1790" s="T82">adv</ta>
            <ta e="T84" id="Seg_1791" s="T83">adv</ta>
            <ta e="T85" id="Seg_1792" s="T84">n</ta>
            <ta e="T86" id="Seg_1793" s="T85">pp</ta>
            <ta e="T87" id="Seg_1794" s="T86">v</ta>
            <ta e="T88" id="Seg_1795" s="T87">preverb</ta>
            <ta e="T89" id="Seg_1796" s="T88">v</ta>
            <ta e="T91" id="Seg_1797" s="T90">n</ta>
            <ta e="T92" id="Seg_1798" s="T91">conj</ta>
            <ta e="T93" id="Seg_1799" s="T92">v</ta>
            <ta e="T94" id="Seg_1800" s="T93">n</ta>
            <ta e="T95" id="Seg_1801" s="T94">adv</ta>
            <ta e="T96" id="Seg_1802" s="T95">v</ta>
            <ta e="T97" id="Seg_1803" s="T96">adv</ta>
            <ta e="T98" id="Seg_1804" s="T97">v</ta>
            <ta e="T99" id="Seg_1805" s="T98">adv</ta>
            <ta e="T100" id="Seg_1806" s="T99">v</ta>
            <ta e="T101" id="Seg_1807" s="T100">n</ta>
            <ta e="T102" id="Seg_1808" s="T101">adv</ta>
            <ta e="T103" id="Seg_1809" s="T102">v</ta>
            <ta e="T104" id="Seg_1810" s="T103">n</ta>
            <ta e="T105" id="Seg_1811" s="T104">v</ta>
            <ta e="T106" id="Seg_1812" s="T105">n</ta>
            <ta e="T107" id="Seg_1813" s="T106">n</ta>
            <ta e="T108" id="Seg_1814" s="T107">v</ta>
            <ta e="T109" id="Seg_1815" s="T108">interrog</ta>
            <ta e="T110" id="Seg_1816" s="T109">n</ta>
            <ta e="T111" id="Seg_1817" s="T110">ptcl</ta>
            <ta e="T112" id="Seg_1818" s="T111">v</ta>
            <ta e="T113" id="Seg_1819" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_1820" s="T113">dem</ta>
            <ta e="T115" id="Seg_1821" s="T114">n</ta>
            <ta e="T116" id="Seg_1822" s="T115">v</ta>
            <ta e="T117" id="Seg_1823" s="T116">v</ta>
            <ta e="T118" id="Seg_1824" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_1825" s="T118">v</ta>
            <ta e="T120" id="Seg_1826" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_1827" s="T120">v</ta>
            <ta e="T122" id="Seg_1828" s="T121">ptcl</ta>
            <ta e="T123" id="Seg_1829" s="T122">v</ta>
            <ta e="T124" id="Seg_1830" s="T123">n</ta>
            <ta e="T125" id="Seg_1831" s="T124">quant</ta>
            <ta e="T126" id="Seg_1832" s="T125">n</ta>
            <ta e="T127" id="Seg_1833" s="T126">v</ta>
            <ta e="T128" id="Seg_1834" s="T127">adj</ta>
            <ta e="T130" id="Seg_1835" s="T129">v</ta>
            <ta e="T132" id="Seg_1836" s="T131">n</ta>
            <ta e="T133" id="Seg_1837" s="T132">n</ta>
            <ta e="T134" id="Seg_1838" s="T133">adv</ta>
            <ta e="T135" id="Seg_1839" s="T134">v</ta>
            <ta e="T136" id="Seg_1840" s="T135">conj</ta>
            <ta e="T137" id="Seg_1841" s="T136">dem</ta>
            <ta e="T138" id="Seg_1842" s="T137">dem</ta>
            <ta e="T139" id="Seg_1843" s="T138">n</ta>
            <ta e="T140" id="Seg_1844" s="T139">adv</ta>
            <ta e="T141" id="Seg_1845" s="T140">adj</ta>
            <ta e="T142" id="Seg_1846" s="T141">ptcl</ta>
            <ta e="T143" id="Seg_1847" s="T142">v</ta>
            <ta e="T144" id="Seg_1848" s="T143">n</ta>
            <ta e="T145" id="Seg_1849" s="T144">adv</ta>
            <ta e="T146" id="Seg_1850" s="T145">adv</ta>
            <ta e="T147" id="Seg_1851" s="T146">adv</ta>
            <ta e="T148" id="Seg_1852" s="T147">adv</ta>
            <ta e="T149" id="Seg_1853" s="T148">preverb</ta>
            <ta e="T150" id="Seg_1854" s="T149">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_1855" s="T2">np:L</ta>
            <ta e="T5" id="Seg_1856" s="T4">np:L</ta>
            <ta e="T6" id="Seg_1857" s="T5">np.h:Th</ta>
            <ta e="T8" id="Seg_1858" s="T7">adv:L</ta>
            <ta e="T10" id="Seg_1859" s="T9">np:L</ta>
            <ta e="T12" id="Seg_1860" s="T11">np.h:Th</ta>
            <ta e="T14" id="Seg_1861" s="T13">np.h:P</ta>
            <ta e="T15" id="Seg_1862" s="T14">0.3:Th</ta>
            <ta e="T20" id="Seg_1863" s="T19">np.h:Th</ta>
            <ta e="T21" id="Seg_1864" s="T20">adv:Time</ta>
            <ta e="T24" id="Seg_1865" s="T23">adv:Time</ta>
            <ta e="T27" id="Seg_1866" s="T26">np:Time</ta>
            <ta e="T28" id="Seg_1867" s="T27">np.h:Th</ta>
            <ta e="T30" id="Seg_1868" s="T29">np.h:Th</ta>
            <ta e="T32" id="Seg_1869" s="T31">adv:Time</ta>
            <ta e="T33" id="Seg_1870" s="T32">0.3.h:A</ta>
            <ta e="T34" id="Seg_1871" s="T33">v:Th</ta>
            <ta e="T35" id="Seg_1872" s="T34">0.3.h:A</ta>
            <ta e="T36" id="Seg_1873" s="T35">0.3.h:A</ta>
            <ta e="T39" id="Seg_1874" s="T38">0.3.h:A</ta>
            <ta e="T40" id="Seg_1875" s="T39">adv:Time</ta>
            <ta e="T41" id="Seg_1876" s="T40">0.3.h:Th</ta>
            <ta e="T42" id="Seg_1877" s="T41">np:Time</ta>
            <ta e="T43" id="Seg_1878" s="T42">0.3.h:A</ta>
            <ta e="T45" id="Seg_1879" s="T44">np:Time</ta>
            <ta e="T46" id="Seg_1880" s="T45">0.3.h:A</ta>
            <ta e="T47" id="Seg_1881" s="T46">0.3.h:A</ta>
            <ta e="T48" id="Seg_1882" s="T47">np.h:A</ta>
            <ta e="T49" id="Seg_1883" s="T48">np:G</ta>
            <ta e="T52" id="Seg_1884" s="T51">np.h:A</ta>
            <ta e="T53" id="Seg_1885" s="T52">np:G</ta>
            <ta e="T54" id="Seg_1886" s="T53">0.3.h:A</ta>
            <ta e="T57" id="Seg_1887" s="T56">pro.h:Th</ta>
            <ta e="T61" id="Seg_1888" s="T60">np.h:E</ta>
            <ta e="T63" id="Seg_1889" s="T62">pro.h:A</ta>
            <ta e="T64" id="Seg_1890" s="T63">pro.h:P</ta>
            <ta e="T65" id="Seg_1891" s="T64">adv:Time</ta>
            <ta e="T69" id="Seg_1892" s="T68">adv:G</ta>
            <ta e="T70" id="Seg_1893" s="T69">0.1.h:A</ta>
            <ta e="T71" id="Seg_1894" s="T70">np.h:A</ta>
            <ta e="T72" id="Seg_1895" s="T71">adv:G</ta>
            <ta e="T74" id="Seg_1896" s="T73">np.h:A</ta>
            <ta e="T75" id="Seg_1897" s="T74">0.3.h:E</ta>
            <ta e="T77" id="Seg_1898" s="T76">np:Th</ta>
            <ta e="T78" id="Seg_1899" s="T77">0.3.h:A</ta>
            <ta e="T79" id="Seg_1900" s="T78">np:P 0.3.h:Poss</ta>
            <ta e="T80" id="Seg_1901" s="T79">np:Ins</ta>
            <ta e="T84" id="Seg_1902" s="T83">0.3.h:A</ta>
            <ta e="T86" id="Seg_1903" s="T85">pp:G</ta>
            <ta e="T87" id="Seg_1904" s="T86">0.3.h:A</ta>
            <ta e="T89" id="Seg_1905" s="T88">0.3.h:P</ta>
            <ta e="T91" id="Seg_1906" s="T90">np.h:A</ta>
            <ta e="T94" id="Seg_1907" s="T93">np.h:Th</ta>
            <ta e="T97" id="Seg_1908" s="T96">adv:L</ta>
            <ta e="T98" id="Seg_1909" s="T97">0.3.h:A</ta>
            <ta e="T99" id="Seg_1910" s="T98">adv:L</ta>
            <ta e="T100" id="Seg_1911" s="T99">0.3.h:A</ta>
            <ta e="T101" id="Seg_1912" s="T100">np.h:Th</ta>
            <ta e="T104" id="Seg_1913" s="T103">np.h:A</ta>
            <ta e="T106" id="Seg_1914" s="T105">np.h:Poss</ta>
            <ta e="T107" id="Seg_1915" s="T106">np:Th</ta>
            <ta e="T108" id="Seg_1916" s="T107">0.3.h:A</ta>
            <ta e="T109" id="Seg_1917" s="T108">pro.h:Poss</ta>
            <ta e="T110" id="Seg_1918" s="T109">np:Th</ta>
            <ta e="T117" id="Seg_1919" s="T116">0.3.h:A</ta>
            <ta e="T119" id="Seg_1920" s="T118">0.3.h:A 0.3:Th</ta>
            <ta e="T121" id="Seg_1921" s="T120">0.3.h:A</ta>
            <ta e="T123" id="Seg_1922" s="T122">0.3.h:A</ta>
            <ta e="T126" id="Seg_1923" s="T125">np:Th 0.3.h:Poss</ta>
            <ta e="T132" id="Seg_1924" s="T131">np.h:Th</ta>
            <ta e="T133" id="Seg_1925" s="T132">np.h:A</ta>
            <ta e="T139" id="Seg_1926" s="T138">np.h:A</ta>
            <ta e="T140" id="Seg_1927" s="T139">0.3.h:Th</ta>
            <ta e="T144" id="Seg_1928" s="T143">np.h:A</ta>
            <ta e="T145" id="Seg_1929" s="T144">adv:G</ta>
            <ta e="T146" id="Seg_1930" s="T145">0.3.h:A</ta>
            <ta e="T147" id="Seg_1931" s="T146">adv:G</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T6" id="Seg_1932" s="T5">np.h:S</ta>
            <ta e="T7" id="Seg_1933" s="T6">v:pred</ta>
            <ta e="T12" id="Seg_1934" s="T11">np.h:S</ta>
            <ta e="T13" id="Seg_1935" s="T12">v:pred</ta>
            <ta e="T14" id="Seg_1936" s="T13">np.h:S</ta>
            <ta e="T15" id="Seg_1937" s="T14">s:temp</ta>
            <ta e="T18" id="Seg_1938" s="T17">v:pred</ta>
            <ta e="T20" id="Seg_1939" s="T19">np.h:S</ta>
            <ta e="T25" id="Seg_1940" s="T24">v:pred</ta>
            <ta e="T29" id="Seg_1941" s="T27">s:temp</ta>
            <ta e="T31" id="Seg_1942" s="T29">s:temp</ta>
            <ta e="T33" id="Seg_1943" s="T32">0.3.h:S v:pred</ta>
            <ta e="T34" id="Seg_1944" s="T33">v:O</ta>
            <ta e="T35" id="Seg_1945" s="T34">0.3.h:S v:pred</ta>
            <ta e="T36" id="Seg_1946" s="T35">0.3.h:S v:pred</ta>
            <ta e="T39" id="Seg_1947" s="T38">0.3.h:S v:pred</ta>
            <ta e="T41" id="Seg_1948" s="T40">0.3.h:S v:pred</ta>
            <ta e="T43" id="Seg_1949" s="T42">0.3.h:S v:pred</ta>
            <ta e="T46" id="Seg_1950" s="T45">0.3.h:S v:pred</ta>
            <ta e="T47" id="Seg_1951" s="T46">0.3.h:S v:pred</ta>
            <ta e="T48" id="Seg_1952" s="T47">np.h:S</ta>
            <ta e="T50" id="Seg_1953" s="T49">v:pred</ta>
            <ta e="T52" id="Seg_1954" s="T51">np.h:S</ta>
            <ta e="T54" id="Seg_1955" s="T52">s:temp</ta>
            <ta e="T56" id="Seg_1956" s="T55">v:pred</ta>
            <ta e="T57" id="Seg_1957" s="T56">pro.h:S</ta>
            <ta e="T60" id="Seg_1958" s="T59">v:pred</ta>
            <ta e="T61" id="Seg_1959" s="T60">np.h:S</ta>
            <ta e="T62" id="Seg_1960" s="T61">v:pred</ta>
            <ta e="T63" id="Seg_1961" s="T62">pro.h:S</ta>
            <ta e="T64" id="Seg_1962" s="T63">pro.h:O</ta>
            <ta e="T67" id="Seg_1963" s="T66">v:pred</ta>
            <ta e="T70" id="Seg_1964" s="T69">0.1.h:S v:pred</ta>
            <ta e="T71" id="Seg_1965" s="T70">np.h:S</ta>
            <ta e="T73" id="Seg_1966" s="T72">v:pred</ta>
            <ta e="T74" id="Seg_1967" s="T73">np.h:S</ta>
            <ta e="T75" id="Seg_1968" s="T74">s:adv</ta>
            <ta e="T78" id="Seg_1969" s="T75">s:temp</ta>
            <ta e="T79" id="Seg_1970" s="T78">np:O</ta>
            <ta e="T81" id="Seg_1971" s="T80">v:pred</ta>
            <ta e="T87" id="Seg_1972" s="T83">s:temp</ta>
            <ta e="T89" id="Seg_1973" s="T88">0.3.h:S v:pred</ta>
            <ta e="T91" id="Seg_1974" s="T90">np.h:S</ta>
            <ta e="T93" id="Seg_1975" s="T92">v:pred</ta>
            <ta e="T94" id="Seg_1976" s="T93">np.h:S</ta>
            <ta e="T96" id="Seg_1977" s="T95">v:pred</ta>
            <ta e="T98" id="Seg_1978" s="T97">0.3.h:S v:pred</ta>
            <ta e="T100" id="Seg_1979" s="T99">0.3.h:S v:pred</ta>
            <ta e="T101" id="Seg_1980" s="T100">np.h:S</ta>
            <ta e="T103" id="Seg_1981" s="T102">v:pred</ta>
            <ta e="T104" id="Seg_1982" s="T103">np.h:S</ta>
            <ta e="T105" id="Seg_1983" s="T104">v:pred</ta>
            <ta e="T107" id="Seg_1984" s="T106">np:O</ta>
            <ta e="T108" id="Seg_1985" s="T107">0.3.h:S v:pred</ta>
            <ta e="T112" id="Seg_1986" s="T108">s:compl</ta>
            <ta e="T115" id="Seg_1987" s="T114">n:pred</ta>
            <ta e="T116" id="Seg_1988" s="T115">cop</ta>
            <ta e="T117" id="Seg_1989" s="T116">0.3.h:S v:pred</ta>
            <ta e="T119" id="Seg_1990" s="T118">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T121" id="Seg_1991" s="T120">0.3.h:S v:pred</ta>
            <ta e="T123" id="Seg_1992" s="T122">0.3.h:S v:pred</ta>
            <ta e="T126" id="Seg_1993" s="T125">np:S</ta>
            <ta e="T127" id="Seg_1994" s="T126">v:pred</ta>
            <ta e="T130" id="Seg_1995" s="T129">v:pred</ta>
            <ta e="T132" id="Seg_1996" s="T131">np.h:S</ta>
            <ta e="T133" id="Seg_1997" s="T132">np.h:S</ta>
            <ta e="T135" id="Seg_1998" s="T134">v:pred</ta>
            <ta e="T139" id="Seg_1999" s="T138">np.h:S</ta>
            <ta e="T141" id="Seg_2000" s="T139">s:adv</ta>
            <ta e="T143" id="Seg_2001" s="T142">v:pred</ta>
            <ta e="T144" id="Seg_2002" s="T143">np.h:S</ta>
            <ta e="T146" id="Seg_2003" s="T144">s:temp</ta>
            <ta e="T150" id="Seg_2004" s="T149">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T26" id="Seg_2005" s="T25">RUS:gram</ta>
            <ta e="T68" id="Seg_2006" s="T67">RUS:cult</ta>
            <ta e="T113" id="Seg_2007" s="T112">RUS:cult</ta>
            <ta e="T136" id="Seg_2008" s="T135">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T68" id="Seg_2009" s="T67">Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T152" id="Seg_2010" s="T0">RUS:ext</ta>
            <ta e="T68" id="Seg_2011" s="T67">RUS:int.ins</ta>
            <ta e="T153" id="Seg_2012" s="T89">RUS:ext</ta>
            <ta e="T113" id="Seg_2013" s="T112">RUS:int.ins</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T152" id="Seg_2014" s="T0">[KuAI:] Фрося Ирикова, cказка.</ta>
            <ta e="T7" id="Seg_2015" s="T152">[IF:] В одном доме, в одном доме жили люди.</ta>
            <ta e="T13" id="Seg_2016" s="T7">Там в этом доме такая женщина была.</ta>
            <ta e="T18" id="Seg_2017" s="T13">Люди, когда стемнеет, все засыпают.</ta>
            <ta e="T25" id="Seg_2018" s="T18">Эта женщина днём не… днём спит.</ta>
            <ta e="T33" id="Seg_2019" s="T25">А в темноте, когда люди спят, когда люди спят, потом готовить собирается начинать.</ta>
            <ta e="T35" id="Seg_2020" s="T33">Есть начинать собирается.</ta>
            <ta e="T36" id="Seg_2021" s="T35">Шьёт.</ta>
            <ta e="T43" id="Seg_2022" s="T36">Всегда так делает, днём спит, ночью работает.</ta>
            <ta e="T47" id="Seg_2023" s="T43">Однажды села, шьёт.</ta>
            <ta e="T50" id="Seg_2024" s="T47">Ведьма в дом зашла.</ta>
            <ta e="T60" id="Seg_2025" s="T50">Ведьма, ведьма в дом войдя, так спрашивает [=делает]: “Ты почему не спишь?”</ta>
            <ta e="T62" id="Seg_2026" s="T60">Женщина испугалась.</ta>
            <ta e="T70" id="Seg_2027" s="T62">“Я тебя сейчас съем, только домой схожу”.</ta>
            <ta e="T73" id="Seg_2028" s="T70">Ведьма домой ушла.</ta>
            <ta e="T81" id="Seg_2029" s="T73">Женщина испугавшись, короб с мазью схватив, подмышками мазью смазала.</ta>
            <ta e="T89" id="Seg_2030" s="T81">И так быстро между людей забравшись, спать легла.</ta>
            <ta e="T153" id="Seg_2031" s="T89">[KuAI:] Ещё медленнее.</ta>
            <ta e="T96" id="Seg_2032" s="T153">[IF:] Ведьма только вернулась, женщины всё [нигде] нет.</ta>
            <ta e="T103" id="Seg_2033" s="T96">Сюда посмотрела, туда посмотрела, женщины всё нет.</ta>
            <ta e="T108" id="Seg_2034" s="T103">Ведьма начинает искать, подмышками у людей щупает.</ta>
            <ta e="T116" id="Seg_2035" s="T108">У кого подмышка вроде холодная, значит та женщина и есть.</ta>
            <ta e="T123" id="Seg_2036" s="T116">Всех обходит, трогает, обходит, обходит.</ta>
            <ta e="T135" id="Seg_2037" s="T123">"У людей у всех подмышкой тепло, (значит?), спят люди", – сказала ведьма.</ta>
            <ta e="T143" id="Seg_2038" s="T135">А та эта женщина лежа молча помалкивает.</ta>
            <ta e="T150" id="Seg_2039" s="T143">Ведьма на улицу выйдя, прочь туда далеко ушла.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T152" id="Seg_2040" s="T0">[KuAI:] Frosya Irikova, a fairy tale.</ta>
            <ta e="T7" id="Seg_2041" s="T152">[IF:] There lived people in one house, in one house.</ta>
            <ta e="T13" id="Seg_2042" s="T7">There in this house there was such a woman.</ta>
            <ta e="T18" id="Seg_2043" s="T13">When it's getting dark, all the people fall asleep.</ta>
            <ta e="T25" id="Seg_2044" s="T18">This woman is not… at daytime, she sleeps at daytime.</ta>
            <ta e="T33" id="Seg_2045" s="T25">And in the darkness, when all the people are sleeping, then she starts cooking.</ta>
            <ta e="T35" id="Seg_2046" s="T33">She is going to eat.</ta>
            <ta e="T36" id="Seg_2047" s="T35">She sews.</ta>
            <ta e="T43" id="Seg_2048" s="T36">She always makes like this, she sleeps at daytime, she works at nighttime.</ta>
            <ta e="T47" id="Seg_2049" s="T43">Once she sat down and started sewing.</ta>
            <ta e="T50" id="Seg_2050" s="T47">A witch entered the house.</ta>
            <ta e="T60" id="Seg_2051" s="T50">The witch, the witch, having come in, asks [=makes] like this: "Why aren't you sleeping?"</ta>
            <ta e="T62" id="Seg_2052" s="T60">The woman got scared.</ta>
            <ta e="T70" id="Seg_2053" s="T62">"I'll eat you now, I'll just go home and bring something."</ta>
            <ta e="T73" id="Seg_2054" s="T70">The witch went home.</ta>
            <ta e="T81" id="Seg_2055" s="T73">The woman, having got scared, having taken a box with some fish ointment, anointed her armpits with this ointment.</ta>
            <ta e="T89" id="Seg_2056" s="T81">And having quickly squeezed between the [other] people, she got asleep.</ta>
            <ta e="T153" id="Seg_2057" s="T89">[KuAI:] More slowly.</ta>
            <ta e="T96" id="Seg_2058" s="T153">[IF:] The witch has just come back, there is no woman anywhere.</ta>
            <ta e="T103" id="Seg_2059" s="T96">She looked here, she looked there, still no woman.</ta>
            <ta e="T108" id="Seg_2060" s="T103">The witch started to search, she examines the people's armpits.</ta>
            <ta e="T116" id="Seg_2061" s="T108">Whose armpit is cold, this [person] is that woman.</ta>
            <ta e="T123" id="Seg_2062" s="T116">She goes around, she examines, she goes around, she goes around.</ta>
            <ta e="T135" id="Seg_2063" s="T123">"All the people's armpits are warm, (which means?) they are sleeping," – the witch said.</ta>
            <ta e="T143" id="Seg_2064" s="T135">And that woman is silently lying, she got quiet.</ta>
            <ta e="T150" id="Seg_2065" s="T143">Having gone outside, the witch left.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T152" id="Seg_2066" s="T0">[KuAI:] Frosja Irikova, ein Märchen.</ta>
            <ta e="T7" id="Seg_2067" s="T152">[IF:] Es lebten Leute in einem Haus, in einem Haus.</ta>
            <ta e="T13" id="Seg_2068" s="T7">Dort in diesem Haus war so eine Frau.</ta>
            <ta e="T18" id="Seg_2069" s="T13">Wenn es dunkel wird, schlafen alle Leute ein.</ta>
            <ta e="T25" id="Seg_2070" s="T18">Diese Frau ist nicht… am Tag, sie schläft am Tag.</ta>
            <ta e="T33" id="Seg_2071" s="T25">Und in der Dunkelheit, wenn alle Leute schlafen, dann fängt sie an zu kochen.</ta>
            <ta e="T35" id="Seg_2072" s="T33">Sie fängt an zu essen.</ta>
            <ta e="T36" id="Seg_2073" s="T35">Sie näht.</ta>
            <ta e="T43" id="Seg_2074" s="T36">Sie macht es immer so, sie schläft am Tag, sie arbeitet in der Nacht.</ta>
            <ta e="T47" id="Seg_2075" s="T43">Einmal setzte sie sich und fing an zu nähen.</ta>
            <ta e="T50" id="Seg_2076" s="T47">Eine Hexe kam ins Haus.</ta>
            <ta e="T60" id="Seg_2077" s="T50">Als die Hexe, die Hexe hineinkam, fragt [=macht] sie so: "Warum schläft du nicht?"</ta>
            <ta e="T62" id="Seg_2078" s="T60">Die Frau erschrak.</ta>
            <ta e="T70" id="Seg_2079" s="T62">"Ich werde dich jetzt essen, ich gehe nur gerade nach Hause."</ta>
            <ta e="T73" id="Seg_2080" s="T70">Die Hexe ging nach Hause.</ta>
            <ta e="T81" id="Seg_2081" s="T73">Die Frau, verängstigt, nahm eine Kiste mit Fischtran und beschmierte ihre Achselhöhlen mit Fischtran.</ta>
            <ta e="T89" id="Seg_2082" s="T81">Und sie quetschte sich schnell zwischen die [anderen] Leute, sie schlief ein.</ta>
            <ta e="T153" id="Seg_2083" s="T89">[KuAI:] Noch langsamer.</ta>
            <ta e="T96" id="Seg_2084" s="T153">[IF:] Die Hexe kam gerade zurück, dort ist keine Frau.</ta>
            <ta e="T103" id="Seg_2085" s="T96">Sie guckte hier, sie guckte dort, immer noch keine Frau.</ta>
            <ta e="T108" id="Seg_2086" s="T103">Die Hexe fing an zu suchen, sie untersucht die Achselhöhlen der Leute.</ta>
            <ta e="T116" id="Seg_2087" s="T108">Wessen Achselhöhle kalt ist, das ist jene Frau.</ta>
            <ta e="T123" id="Seg_2088" s="T116">Sie geht herum, sie untersucht, sie geht herum, sie geht herum.</ta>
            <ta e="T135" id="Seg_2089" s="T123">"Die Achselhöhlen aller Leute sind warm, (das heißt?), die schlafen", sagte die Hexe.</ta>
            <ta e="T143" id="Seg_2090" s="T135">Und jene Frau liegt still da, sie ist ruhig.</ta>
            <ta e="T150" id="Seg_2091" s="T143">Die Hexe ging nach draußen und ging fort.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T7" id="Seg_2092" s="T152">В одном доме… в одном доме жили люди.</ta>
            <ta e="T13" id="Seg_2093" s="T7">В этом доме такая женщина жила.</ta>
            <ta e="T18" id="Seg_2094" s="T13">Когда стемнеет, все спать ложатся.</ta>
            <ta e="T25" id="Seg_2095" s="T18">Эта женщина днём не… сп… днём спит.</ta>
            <ta e="T33" id="Seg_2096" s="T25">а когда стемнеет, когда люди спят когда люди спят вот готовить собирается начинать</ta>
            <ta e="T35" id="Seg_2097" s="T33">есть начинать собирается</ta>
            <ta e="T36" id="Seg_2098" s="T35">шьёт</ta>
            <ta e="T43" id="Seg_2099" s="T36">всегда так делает, днём спит, ночью работает</ta>
            <ta e="T47" id="Seg_2100" s="T43">однажды сидит, шьёт</ta>
            <ta e="T50" id="Seg_2101" s="T47">ведьма в дом зашла</ta>
            <ta e="T60" id="Seg_2102" s="T50">ведьма… ведьма в дом войдя, так спрашивает (=делает): "Ты почему не спишь?"</ta>
            <ta e="T62" id="Seg_2103" s="T60">Женщина испугалась.</ta>
            <ta e="T70" id="Seg_2104" s="T62">"Я тебя сейчас съем, только домой схожу".</ta>
            <ta e="T73" id="Seg_2105" s="T70">Ведьма домой ушла.</ta>
            <ta e="T81" id="Seg_2106" s="T73">Женщина испугалась, короб с мазью взяла, под мышками мазью намазала</ta>
            <ta e="T89" id="Seg_2107" s="T81">и так быстро между людьми легла с ними спать</ta>
            <ta e="T153" id="Seg_2108" s="T89">Ещё медленнее.</ta>
            <ta e="T96" id="Seg_2109" s="T153">Ведьма вернулась, женщины нигде нет.</ta>
            <ta e="T103" id="Seg_2110" s="T96">Сюда посмотрела, туда посмотрела, женщины нигде нет.</ta>
            <ta e="T108" id="Seg_2111" s="T103">Ведьма начинает искать, под мышками у людей щупает.</ta>
            <ta e="T116" id="Seg_2112" s="T108">У кого подмышка холодная, значит тот женщина и есть.</ta>
            <ta e="T123" id="Seg_2113" s="T116">Всех обходит, трогает, вокруг ходит, ходит.</ta>
            <ta e="T135" id="Seg_2114" s="T123">"У людей у всех под мышкой тепло, значит эти жен… люди давно уже спят", сказала ведьма.</ta>
            <ta e="T143" id="Seg_2115" s="T135">А та женщина лежит молча и помалкивает.</ta>
            <ta e="T150" id="Seg_2116" s="T143">Ведьма на улицу вышла и далеко ушла.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T36" id="Seg_2117" s="T35">[BrM:] Tentative analysis of 'šütiran'.</ta>
            <ta e="T47" id="Seg_2118" s="T43">[BrM:] Tentative analysis of 'šütira'.</ta>
            <ta e="T108" id="Seg_2119" s="T103">[BrM:] Tentative analysis of 'paqqɨrɨt'.</ta>
            <ta e="T123" id="Seg_2120" s="T116">[BrM:] Objective conjugation with intransitive 'kojɨ-' - 'go.round'?</ta>
            <ta e="T135" id="Seg_2121" s="T123">[BrM:] Unclear agreement.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T152" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T153" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
