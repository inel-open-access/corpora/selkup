<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KAI_1965_OldWitch_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KAI_1965_OldWitch_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">27</ud-information>
            <ud-information attribute-name="# HIAT:w">20</ud-information>
            <ud-information attribute-name="# e">20</ud-information>
            <ud-information attribute-name="# HIAT:u">3</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KAI">
            <abbreviation>KAI</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T275" />
         <tli id="T276" />
         <tli id="T277" />
         <tli id="T278" />
         <tli id="T279" />
         <tli id="T280" />
         <tli id="T281" />
         <tli id="T282" />
         <tli id="T283" />
         <tli id="T284" />
         <tli id="T285" />
         <tli id="T286" />
         <tli id="T287" />
         <tli id="T288" />
         <tli id="T289" />
         <tli id="T290" />
         <tli id="T291" />
         <tli id="T292" />
         <tli id="T293" />
         <tli id="T294" />
         <tli id="T295" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KAI"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T295" id="Seg_0" n="sc" s="T275">
               <ts e="T282" id="Seg_2" n="HIAT:u" s="T275">
                  <ts e="T276" id="Seg_4" n="HIAT:w" s="T275">Ira</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_7" n="HIAT:w" s="T276">ilɨmpa</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_10" n="HIAT:w" s="T277">najtɨ</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_13" n="HIAT:w" s="T278">mɨ</ts>
                  <nts id="Seg_14" n="HIAT:ip">,</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_17" n="HIAT:w" s="T279">a</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_20" n="HIAT:w" s="T280">nat</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_23" n="HIAT:w" s="T281">ira</ts>
                  <nts id="Seg_24" n="HIAT:ip">.</nts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T289" id="Seg_27" n="HIAT:u" s="T282">
                  <ts e="T283" id="Seg_29" n="HIAT:w" s="T282">Ila</ts>
                  <nts id="Seg_30" n="HIAT:ip">,</nts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_33" n="HIAT:w" s="T283">ila</ts>
                  <nts id="Seg_34" n="HIAT:ip">,</nts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_37" n="HIAT:w" s="T284">šittɨ</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_40" n="HIAT:w" s="T285">ijatɨ</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_43" n="HIAT:w" s="T286">ɛːppa</ts>
                  <nts id="Seg_44" n="HIAT:ip">,</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_47" n="HIAT:w" s="T287">iːjalʼ</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_50" n="HIAT:w" s="T288">mɔːtɨka</ts>
                  <nts id="Seg_51" n="HIAT:ip">.</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T295" id="Seg_54" n="HIAT:u" s="T289">
                  <ts e="T290" id="Seg_56" n="HIAT:w" s="T289">Ukkɨr</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_59" n="HIAT:w" s="T290">tet</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_62" n="HIAT:w" s="T291">čʼontoːqɨn</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_65" n="HIAT:w" s="T292">loːsɨlʼ</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_68" n="HIAT:w" s="T293">imaqota</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_71" n="HIAT:w" s="T294">tümmɨnta</ts>
                  <nts id="Seg_72" n="HIAT:ip">.</nts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T295" id="Seg_74" n="sc" s="T275">
               <ts e="T276" id="Seg_76" n="e" s="T275">Ira </ts>
               <ts e="T277" id="Seg_78" n="e" s="T276">ilɨmpa </ts>
               <ts e="T278" id="Seg_80" n="e" s="T277">najtɨ </ts>
               <ts e="T279" id="Seg_82" n="e" s="T278">mɨ, </ts>
               <ts e="T280" id="Seg_84" n="e" s="T279">a </ts>
               <ts e="T281" id="Seg_86" n="e" s="T280">nat </ts>
               <ts e="T282" id="Seg_88" n="e" s="T281">ira. </ts>
               <ts e="T283" id="Seg_90" n="e" s="T282">Ila, </ts>
               <ts e="T284" id="Seg_92" n="e" s="T283">ila, </ts>
               <ts e="T285" id="Seg_94" n="e" s="T284">šittɨ </ts>
               <ts e="T286" id="Seg_96" n="e" s="T285">ijatɨ </ts>
               <ts e="T287" id="Seg_98" n="e" s="T286">ɛːppa, </ts>
               <ts e="T288" id="Seg_100" n="e" s="T287">iːjalʼ </ts>
               <ts e="T289" id="Seg_102" n="e" s="T288">mɔːtɨka. </ts>
               <ts e="T290" id="Seg_104" n="e" s="T289">Ukkɨr </ts>
               <ts e="T291" id="Seg_106" n="e" s="T290">tet </ts>
               <ts e="T292" id="Seg_108" n="e" s="T291">čʼontoːqɨn </ts>
               <ts e="T293" id="Seg_110" n="e" s="T292">loːsɨlʼ </ts>
               <ts e="T294" id="Seg_112" n="e" s="T293">imaqota </ts>
               <ts e="T295" id="Seg_114" n="e" s="T294">tümmɨnta. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T282" id="Seg_115" s="T275">KAI_1965_OldWitch_flk.001 (001.001)</ta>
            <ta e="T289" id="Seg_116" s="T282">KAI_1965_OldWitch_flk.002 (001.002)</ta>
            <ta e="T295" id="Seg_117" s="T289">KAI_1965_OldWitch_flk.003 (001.003)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T282" id="Seg_118" s="T275">′ира ′иlымба найды мы ′аңат[д̂]ира.</ta>
            <ta e="T289" id="Seg_119" s="T282">иllа, ′иllа ′шʼитты ′иjаты ′еппа, ′ӣjалʼ мо̄ттыkа.</ta>
            <ta e="T295" id="Seg_120" s="T289">о[у]ккыр тет чон′доkын ′лозылʼ им[м]аkота ′тӱмынта.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T282" id="Seg_121" s="T275">ira ilʼɨmpa najdɨ mɨ aŋat[d̂]ira.</ta>
            <ta e="T289" id="Seg_122" s="T282">ilʼlʼa, ilʼlʼa šittɨ ijatɨ eppa, iːjalʼ moːttɨqa.</ta>
            <ta e="T295" id="Seg_123" s="T289">o[u]kkɨr tet čondoqɨn lozɨlʼ im[m]aqota tümɨnta.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T282" id="Seg_124" s="T275">Ira ilɨmpa najtɨ mɨ, a nat ira. </ta>
            <ta e="T289" id="Seg_125" s="T282">Ila, ila, šittɨ ijatɨ ɛːppa, iːjalʼ mɔːtɨka. </ta>
            <ta e="T295" id="Seg_126" s="T289">Ukkɨr tet čʼontoːqɨn loːsɨlʼ imaqota tümmɨnta. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T276" id="Seg_127" s="T275">ira</ta>
            <ta e="T277" id="Seg_128" s="T276">ilɨ-mpa</ta>
            <ta e="T278" id="Seg_129" s="T277">na-j-tɨ</ta>
            <ta e="T279" id="Seg_130" s="T278">mɨ</ta>
            <ta e="T280" id="Seg_131" s="T279">a</ta>
            <ta e="T281" id="Seg_132" s="T280">na-t</ta>
            <ta e="T282" id="Seg_133" s="T281">ira</ta>
            <ta e="T283" id="Seg_134" s="T282">ila</ta>
            <ta e="T284" id="Seg_135" s="T283">ila</ta>
            <ta e="T285" id="Seg_136" s="T284">šittɨ</ta>
            <ta e="T286" id="Seg_137" s="T285">ija-tɨ</ta>
            <ta e="T287" id="Seg_138" s="T286">ɛː-ppa</ta>
            <ta e="T288" id="Seg_139" s="T287">iːja-lʼ</ta>
            <ta e="T289" id="Seg_140" s="T288">mɔːt-ɨ-ka</ta>
            <ta e="T290" id="Seg_141" s="T289">ukkɨr</ta>
            <ta e="T291" id="Seg_142" s="T290">tet</ta>
            <ta e="T292" id="Seg_143" s="T291">čʼontoː-qɨn</ta>
            <ta e="T293" id="Seg_144" s="T292">loːsɨ-lʼ</ta>
            <ta e="T294" id="Seg_145" s="T293">imaqota</ta>
            <ta e="T295" id="Seg_146" s="T294">tü-mmɨ-nta</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T276" id="Seg_147" s="T275">ira</ta>
            <ta e="T277" id="Seg_148" s="T276">ilɨ-mpɨ</ta>
            <ta e="T278" id="Seg_149" s="T277">na-ɨ-tɨ</ta>
            <ta e="T279" id="Seg_150" s="T278">mɨ</ta>
            <ta e="T280" id="Seg_151" s="T279">a</ta>
            <ta e="T281" id="Seg_152" s="T280">na-n</ta>
            <ta e="T282" id="Seg_153" s="T281">ira</ta>
            <ta e="T283" id="Seg_154" s="T282">ilɨ</ta>
            <ta e="T284" id="Seg_155" s="T283">ilɨ</ta>
            <ta e="T285" id="Seg_156" s="T284">šittɨ</ta>
            <ta e="T286" id="Seg_157" s="T285">iːja-tɨ</ta>
            <ta e="T287" id="Seg_158" s="T286">ɛː-mpɨ</ta>
            <ta e="T288" id="Seg_159" s="T287">iːja-lʼ</ta>
            <ta e="T289" id="Seg_160" s="T288">mɔːt-ɨ-ka</ta>
            <ta e="T290" id="Seg_161" s="T289">ukkɨr</ta>
            <ta e="T291" id="Seg_162" s="T290">tɔːt</ta>
            <ta e="T292" id="Seg_163" s="T291">čʼontɨ-qɨn</ta>
            <ta e="T293" id="Seg_164" s="T292">lоːsɨ-lʼ</ta>
            <ta e="T294" id="Seg_165" s="T293">imaqota</ta>
            <ta e="T295" id="Seg_166" s="T294">tü-mpɨ-ntɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T276" id="Seg_167" s="T275">old.man.[NOM]</ta>
            <ta e="T277" id="Seg_168" s="T276">live-PST.NAR.[3SG.S]</ta>
            <ta e="T278" id="Seg_169" s="T277">this-EP.[NOM]-3SG</ta>
            <ta e="T279" id="Seg_170" s="T278">something.[NOM]</ta>
            <ta e="T280" id="Seg_171" s="T279">but</ta>
            <ta e="T281" id="Seg_172" s="T280">this-GEN</ta>
            <ta e="T282" id="Seg_173" s="T281">old.man.[NOM]</ta>
            <ta e="T283" id="Seg_174" s="T282">live.[3SG.S]</ta>
            <ta e="T284" id="Seg_175" s="T283">live.[3SG.S]</ta>
            <ta e="T285" id="Seg_176" s="T284">two</ta>
            <ta e="T286" id="Seg_177" s="T285">son.[NOM]-3SG</ta>
            <ta e="T287" id="Seg_178" s="T286">be-DUR.[3SG.S]</ta>
            <ta e="T288" id="Seg_179" s="T287">son-ADJZ</ta>
            <ta e="T289" id="Seg_180" s="T288">house-EP-DIM.[NOM]</ta>
            <ta e="T290" id="Seg_181" s="T289">one</ta>
            <ta e="T291" id="Seg_182" s="T290">whole</ta>
            <ta e="T292" id="Seg_183" s="T291">middle-LOC</ta>
            <ta e="T293" id="Seg_184" s="T292">devil-ADJZ</ta>
            <ta e="T294" id="Seg_185" s="T293">old.woman.[NOM]</ta>
            <ta e="T295" id="Seg_186" s="T294">come-PST.NAR-INFER.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T276" id="Seg_187" s="T275">старик.[NOM]</ta>
            <ta e="T277" id="Seg_188" s="T276">жить-PST.NAR.[3SG.S]</ta>
            <ta e="T278" id="Seg_189" s="T277">это-EP.[NOM]-3SG</ta>
            <ta e="T279" id="Seg_190" s="T278">нечто.[NOM]</ta>
            <ta e="T280" id="Seg_191" s="T279">а</ta>
            <ta e="T281" id="Seg_192" s="T280">это-GEN</ta>
            <ta e="T282" id="Seg_193" s="T281">старик.[NOM]</ta>
            <ta e="T283" id="Seg_194" s="T282">жить.[3SG.S]</ta>
            <ta e="T284" id="Seg_195" s="T283">жить.[3SG.S]</ta>
            <ta e="T285" id="Seg_196" s="T284">два</ta>
            <ta e="T286" id="Seg_197" s="T285">сын.[NOM]-3SG</ta>
            <ta e="T287" id="Seg_198" s="T286">быть-DUR.[3SG.S]</ta>
            <ta e="T288" id="Seg_199" s="T287">сын-ADJZ</ta>
            <ta e="T289" id="Seg_200" s="T288">дом-EP-DIM.[NOM]</ta>
            <ta e="T290" id="Seg_201" s="T289">один</ta>
            <ta e="T291" id="Seg_202" s="T290">целый</ta>
            <ta e="T292" id="Seg_203" s="T291">середина-LOC</ta>
            <ta e="T293" id="Seg_204" s="T292">черт-ADJZ</ta>
            <ta e="T294" id="Seg_205" s="T293">старуха.[NOM]</ta>
            <ta e="T295" id="Seg_206" s="T294">прийти-PST.NAR-INFER.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T276" id="Seg_207" s="T275">n-n:case</ta>
            <ta e="T277" id="Seg_208" s="T276">v-v:tense-v:pn</ta>
            <ta e="T278" id="Seg_209" s="T277">pro-n:ins-n:case-n:poss</ta>
            <ta e="T279" id="Seg_210" s="T278">n-n:case</ta>
            <ta e="T280" id="Seg_211" s="T279">conj</ta>
            <ta e="T281" id="Seg_212" s="T280">pro-n:case</ta>
            <ta e="T282" id="Seg_213" s="T281">n-n:case</ta>
            <ta e="T283" id="Seg_214" s="T282">v-v:pn</ta>
            <ta e="T284" id="Seg_215" s="T283">v-v:pn</ta>
            <ta e="T285" id="Seg_216" s="T284">num</ta>
            <ta e="T286" id="Seg_217" s="T285">n-n:case-n:poss</ta>
            <ta e="T287" id="Seg_218" s="T286">v-v&gt;v-v:pn</ta>
            <ta e="T288" id="Seg_219" s="T287">n-n&gt;adj</ta>
            <ta e="T289" id="Seg_220" s="T288">n-n:ins-n&gt;n-n:case</ta>
            <ta e="T290" id="Seg_221" s="T289">num</ta>
            <ta e="T291" id="Seg_222" s="T290">adj</ta>
            <ta e="T292" id="Seg_223" s="T291">n-n:case</ta>
            <ta e="T293" id="Seg_224" s="T292">n-n&gt;adj</ta>
            <ta e="T294" id="Seg_225" s="T293">n-n:case</ta>
            <ta e="T295" id="Seg_226" s="T294">v-v:tense-v:mood-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T276" id="Seg_227" s="T275">n</ta>
            <ta e="T277" id="Seg_228" s="T276">v</ta>
            <ta e="T278" id="Seg_229" s="T277">pro</ta>
            <ta e="T279" id="Seg_230" s="T278">n</ta>
            <ta e="T280" id="Seg_231" s="T279">conj</ta>
            <ta e="T281" id="Seg_232" s="T280">pro</ta>
            <ta e="T282" id="Seg_233" s="T281">n</ta>
            <ta e="T283" id="Seg_234" s="T282">v</ta>
            <ta e="T284" id="Seg_235" s="T283">v</ta>
            <ta e="T285" id="Seg_236" s="T284">num</ta>
            <ta e="T286" id="Seg_237" s="T285">n</ta>
            <ta e="T287" id="Seg_238" s="T286">v</ta>
            <ta e="T288" id="Seg_239" s="T287">adj</ta>
            <ta e="T289" id="Seg_240" s="T288">n</ta>
            <ta e="T290" id="Seg_241" s="T289">num</ta>
            <ta e="T291" id="Seg_242" s="T290">adj</ta>
            <ta e="T292" id="Seg_243" s="T291">n</ta>
            <ta e="T293" id="Seg_244" s="T292">adj</ta>
            <ta e="T294" id="Seg_245" s="T293">n</ta>
            <ta e="T295" id="Seg_246" s="T294">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T276" id="Seg_247" s="T275">np.h:Th</ta>
            <ta e="T283" id="Seg_248" s="T282">0.3.h:Th</ta>
            <ta e="T284" id="Seg_249" s="T283">0.3.h:Th</ta>
            <ta e="T286" id="Seg_250" s="T285">np.h:Th 0.3.h:Poss</ta>
            <ta e="T288" id="Seg_251" s="T287">np.h:Poss</ta>
            <ta e="T292" id="Seg_252" s="T291">np:Time</ta>
            <ta e="T294" id="Seg_253" s="T293">np.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T276" id="Seg_254" s="T275">np.h:S</ta>
            <ta e="T277" id="Seg_255" s="T276">v:pred</ta>
            <ta e="T283" id="Seg_256" s="T282">0.3.h:S v:pred</ta>
            <ta e="T284" id="Seg_257" s="T283">0.3.h:S v:pred</ta>
            <ta e="T286" id="Seg_258" s="T285">np.h:S</ta>
            <ta e="T287" id="Seg_259" s="T286">v:pred</ta>
            <ta e="T294" id="Seg_260" s="T293">np.h:S</ta>
            <ta e="T295" id="Seg_261" s="T294">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T276" id="Seg_262" s="T275">new</ta>
            <ta e="T282" id="Seg_263" s="T281">giv-active</ta>
            <ta e="T283" id="Seg_264" s="T282">0.giv-active</ta>
            <ta e="T284" id="Seg_265" s="T283">0.giv-active</ta>
            <ta e="T286" id="Seg_266" s="T285">new</ta>
            <ta e="T289" id="Seg_267" s="T288">new</ta>
            <ta e="T294" id="Seg_268" s="T293">new</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T282" id="Seg_269" s="T275">Старик жил (…).</ta>
            <ta e="T289" id="Seg_270" s="T282">Жил, жил, двое сыновей было, сыновий дом.</ta>
            <ta e="T295" id="Seg_271" s="T289">Однажды чертовка старуха пришла.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T282" id="Seg_272" s="T275">An old man lived (…).</ta>
            <ta e="T289" id="Seg_273" s="T282">He lived, lived, he had two sons, his son's house.</ta>
            <ta e="T295" id="Seg_274" s="T289">Once there came an old witch.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T282" id="Seg_275" s="T275">Ein alter Mann lebte (…).</ta>
            <ta e="T289" id="Seg_276" s="T282">Er lebte, er lebte, er hatte zwei Söhne, das Haus des Sohnes.</ta>
            <ta e="T295" id="Seg_277" s="T289">Einmal kam eine alte Hexe.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T282" id="Seg_278" s="T275">старик жил</ta>
            <ta e="T289" id="Seg_279" s="T282">жил, жил, двое сыновей были в семейном доме</ta>
            <ta e="T295" id="Seg_280" s="T289">однажды чертовка старуха пришла</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T295" id="Seg_281" s="T289">[OSV:] the form "tümɨnta" has been edited into "tümmɨnta".</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
