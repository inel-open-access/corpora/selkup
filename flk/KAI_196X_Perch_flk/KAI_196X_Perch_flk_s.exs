<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KAI_196X_Perch_flk</transcription-name>
         <referenced-file url="KAI_196X_Perch_flk.wav" />
         <referenced-file url="KAI_196X_Perch_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KAI_196X_Perch_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">331</ud-information>
            <ud-information attribute-name="# HIAT:w">195</ud-information>
            <ud-information attribute-name="# e">202</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">8</ud-information>
            <ud-information attribute-name="# HIAT:u">31</ud-information>
            <ud-information attribute-name="# sc">60</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KAI">
            <abbreviation>KAI</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.689" type="appl" />
         <tli id="T2" time="1.2358571428571428" type="appl" />
         <tli id="T3" time="1.782714285714286" type="appl" />
         <tli id="T4" time="2.3295714285714286" type="appl" />
         <tli id="T5" time="2.876428571428572" type="appl" />
         <tli id="T6" time="3.423285714285715" type="appl" />
         <tli id="T7" time="3.9701428571428576" type="appl" />
         <tli id="T8" time="4.517" type="appl" />
         <tli id="T9" time="4.999666666666667" type="appl" />
         <tli id="T10" time="5.482333333333333" type="appl" />
         <tli id="T11" time="5.965" type="appl" />
         <tli id="T12" time="6.447666666666667" type="appl" />
         <tli id="T13" time="6.9303333333333335" type="appl" />
         <tli id="T14" time="7.413" type="appl" />
         <tli id="T15" time="7.895666666666666" type="appl" />
         <tli id="T16" time="8.378333333333334" type="appl" />
         <tli id="T17" time="8.861" type="appl" />
         <tli id="T18" time="9.343666666666667" type="appl" />
         <tli id="T19" time="9.826333333333332" type="appl" />
         <tli id="T20" time="10.309" type="appl" />
         <tli id="T21" time="10.348" type="appl" />
         <tli id="T22" time="10.988666666666667" type="appl" />
         <tli id="T23" time="11.629333333333333" type="appl" />
         <tli id="T24" time="12.27" type="appl" />
         <tli id="T25" time="12.543" type="appl" />
         <tli id="T26" time="12.991" type="appl" />
         <tli id="T27" time="13.439" type="appl" />
         <tli id="T28" time="14.0" type="appl" />
         <tli id="T29" time="14.526315789473685" type="appl" />
         <tli id="T30" time="15.052631578947368" type="appl" />
         <tli id="T31" time="15.578947368421053" type="appl" />
         <tli id="T32" time="16.105263157894736" type="appl" />
         <tli id="T33" time="16.63157894736842" type="appl" />
         <tli id="T34" time="17.157894736842106" type="appl" />
         <tli id="T35" time="17.684210526315788" type="appl" />
         <tli id="T36" time="18.210526315789473" type="appl" />
         <tli id="T37" time="18.736842105263158" type="appl" />
         <tli id="T38" time="19.263157894736842" type="appl" />
         <tli id="T39" time="19.789473684210527" type="appl" />
         <tli id="T40" time="20.315789473684212" type="appl" />
         <tli id="T41" time="20.842105263157894" type="appl" />
         <tli id="T42" time="21.36842105263158" type="appl" />
         <tli id="T43" time="21.894736842105264" type="appl" />
         <tli id="T44" time="22.421052631578945" type="appl" />
         <tli id="T45" time="22.94736842105263" type="appl" />
         <tli id="T46" time="23.473684210526315" type="appl" />
         <tli id="T47" time="24.0" type="appl" />
         <tli id="T48" time="24.361" type="appl" />
         <tli id="T49" time="24.997333333333334" type="appl" />
         <tli id="T50" time="25.633666666666667" type="appl" />
         <tli id="T51" time="26.27" type="appl" />
         <tli id="T52" time="26.686" type="appl" />
         <tli id="T53" time="27.2522" type="appl" />
         <tli id="T54" time="27.8184" type="appl" />
         <tli id="T55" time="28.3846" type="appl" />
         <tli id="T56" time="28.9508" type="appl" />
         <tli id="T57" time="29.517" type="appl" />
         <tli id="T58" time="30.049" type="appl" />
         <tli id="T59" time="30.471249999999998" type="appl" />
         <tli id="T60" time="30.8935" type="appl" />
         <tli id="T61" time="31.31575" type="appl" />
         <tli id="T62" time="31.738" type="appl" />
         <tli id="T63" time="32.413" type="appl" />
         <tli id="T64" time="32.8285" type="appl" />
         <tli id="T65" time="33.244" type="appl" />
         <tli id="T66" time="33.6595" type="appl" />
         <tli id="T67" time="34.075" type="appl" />
         <tli id="T68" time="34.179" type="appl" />
         <tli id="T69" time="34.4875" type="appl" />
         <tli id="T70" time="34.796" type="appl" />
         <tli id="T71" time="35.1045" type="appl" />
         <tli id="T72" time="35.413" type="appl" />
         <tli id="T73" time="35.725" type="appl" />
         <tli id="T74" time="36.556" type="appl" />
         <tli id="T75" time="39.049" type="appl" />
         <tli id="T76" time="39.57066666666667" type="appl" />
         <tli id="T77" time="40.092333333333336" type="appl" />
         <tli id="T78" time="40.614000000000004" type="appl" />
         <tli id="T79" time="41.135666666666665" type="appl" />
         <tli id="T80" time="41.657333333333334" type="appl" />
         <tli id="T81" time="42.179" type="appl" />
         <tli id="T82" time="42.478" type="appl" />
         <tli id="T83" time="42.915333333333336" type="appl" />
         <tli id="T84" time="43.352666666666664" type="appl" />
         <tli id="T85" time="43.79" type="appl" />
         <tli id="T86" time="43.985" type="appl" />
         <tli id="T87" time="44.81275" type="appl" />
         <tli id="T88" time="45.6405" type="appl" />
         <tli id="T89" time="46.46825" type="appl" />
         <tli id="T90" time="47.296" type="appl" />
         <tli id="T91" time="48.283" type="appl" />
         <tli id="T92" time="49.036500000000004" type="appl" />
         <tli id="T93" time="49.79" type="appl" />
         <tli id="T94" time="51.4" type="appl" />
         <tli id="T95" time="51.826499999999996" type="appl" />
         <tli id="T96" time="52.253" type="appl" />
         <tli id="T97" time="52.679500000000004" type="appl" />
         <tli id="T98" time="53.106" type="appl" />
         <tli id="T99" time="53.5325" type="appl" />
         <tli id="T100" time="53.959" type="appl" />
         <tli id="T101" time="55.127" type="appl" />
         <tli id="T102" time="55.703375" type="appl" />
         <tli id="T103" time="56.27975" type="appl" />
         <tli id="T104" time="56.856125" type="appl" />
         <tli id="T105" time="57.432500000000005" type="appl" />
         <tli id="T106" time="58.008875" type="appl" />
         <tli id="T107" time="58.58525" type="appl" />
         <tli id="T108" time="59.161625" type="appl" />
         <tli id="T109" time="59.738" type="appl" />
         <tli id="T110" time="60.01" type="appl" />
         <tli id="T111" time="60.714749999999995" type="appl" />
         <tli id="T112" time="61.4195" type="appl" />
         <tli id="T113" time="62.12425" type="appl" />
         <tli id="T114" time="62.829" type="appl" />
         <tli id="T115" time="62.933" type="appl" />
         <tli id="T116" time="63.478375" type="appl" />
         <tli id="T117" time="64.02375" type="appl" />
         <tli id="T118" time="64.569125" type="appl" />
         <tli id="T119" time="65.1145" type="appl" />
         <tli id="T120" time="65.659875" type="appl" />
         <tli id="T121" time="66.20525" type="appl" />
         <tli id="T122" time="66.750625" type="appl" />
         <tli id="T123" time="67.296" type="appl" />
         <tli id="T124" time="68.114" type="appl" />
         <tli id="T125" time="69.452" type="appl" />
         <tli id="T126" time="70.79" type="appl" />
         <tli id="T127" time="71.0" type="appl" />
         <tli id="T128" time="71.35714285714286" type="appl" />
         <tli id="T129" time="71.71428571428571" type="appl" />
         <tli id="T130" time="72.07142857142857" type="appl" />
         <tli id="T131" time="72.42857142857143" type="appl" />
         <tli id="T132" time="72.78571428571429" type="appl" />
         <tli id="T133" time="73.14285714285714" type="appl" />
         <tli id="T134" time="73.5" type="appl" />
         <tli id="T135" time="73.85714285714286" type="appl" />
         <tli id="T136" time="74.21428571428571" type="appl" />
         <tli id="T137" time="74.57142857142857" type="appl" />
         <tli id="T138" time="74.92857142857143" type="appl" />
         <tli id="T139" time="75.28571428571429" type="appl" />
         <tli id="T140" time="75.64285714285714" type="appl" />
         <tli id="T141" time="76.0" type="appl" />
         <tli id="T142" time="76.855" type="appl" />
         <tli id="T143" time="77.387" type="appl" />
         <tli id="T144" time="77.478" type="appl" />
         <tli id="T145" time="77.92399999999999" type="appl" />
         <tli id="T146" time="78.37" type="appl" />
         <tli id="T147" time="78.816" type="appl" />
         <tli id="T148" time="78.881" type="appl" />
         <tli id="T149" time="79.26128571428572" type="appl" />
         <tli id="T150" time="79.64157142857142" type="appl" />
         <tli id="T151" time="80.02185714285714" type="appl" />
         <tli id="T152" time="80.40214285714286" type="appl" />
         <tli id="T153" time="80.78242857142858" type="appl" />
         <tli id="T154" time="81.16271428571429" type="appl" />
         <tli id="T155" time="81.543" type="appl" />
         <tli id="T156" time="82.0" type="appl" />
         <tli id="T157" time="82.33333333333333" type="appl" />
         <tli id="T158" time="82.66666666666667" type="appl" />
         <tli id="T159" time="83.0" type="appl" />
         <tli id="T160" time="83.33333333333333" type="appl" />
         <tli id="T161" time="83.66666666666667" type="appl" />
         <tli id="T162" time="84.0" type="appl" />
         <tli id="T163" time="84.33333333333333" type="appl" />
         <tli id="T164" time="84.66666666666667" type="appl" />
         <tli id="T165" time="85.0" type="appl" />
         <tli id="T166" time="85.699" type="appl" />
         <tli id="T167" time="86.036" type="appl" />
         <tli id="T168" time="86.153" type="appl" />
         <tli id="T169" time="86.7115" type="appl" />
         <tli id="T170" time="87.27" type="appl" />
         <tli id="T171" time="87.907" type="appl" />
         <tli id="T172" time="88.16288235294117" type="appl" />
         <tli id="T173" time="88.41876470588235" type="appl" />
         <tli id="T174" time="88.67464705882352" type="appl" />
         <tli id="T175" time="88.93052941176471" type="appl" />
         <tli id="T176" time="89.18641176470588" type="appl" />
         <tli id="T177" time="89.44229411764705" type="appl" />
         <tli id="T178" time="89.69817647058824" type="appl" />
         <tli id="T179" time="89.95405882352941" type="appl" />
         <tli id="T180" time="90.2099411764706" type="appl" />
         <tli id="T181" time="90.46582352941176" type="appl" />
         <tli id="T182" time="90.72170588235295" type="appl" />
         <tli id="T183" time="90.97758823529412" type="appl" />
         <tli id="T184" time="91.23347058823529" type="appl" />
         <tli id="T185" time="91.48935294117648" type="appl" />
         <tli id="T186" time="91.74523529411765" type="appl" />
         <tli id="T187" time="92.00111764705883" type="appl" />
         <tli id="T188" time="92.257" type="appl" />
         <tli id="T189" time="94.829" type="appl" />
         <tli id="T190" time="95.31762499999999" type="appl" />
         <tli id="T191" time="95.80624999999999" type="appl" />
         <tli id="T192" time="96.29487499999999" type="appl" />
         <tli id="T193" time="96.7835" type="appl" />
         <tli id="T194" time="97.272125" type="appl" />
         <tli id="T195" time="97.76075" type="appl" />
         <tli id="T196" time="98.249375" type="appl" />
         <tli id="T197" time="98.738" type="appl" />
         <tli id="T198" time="99.226625" type="appl" />
         <tli id="T199" time="99.71525" type="appl" />
         <tli id="T200" time="100.203875" type="appl" />
         <tli id="T201" time="100.6925" type="appl" />
         <tli id="T202" time="101.18112500000001" type="appl" />
         <tli id="T203" time="101.66975000000001" type="appl" />
         <tli id="T204" time="102.158375" type="appl" />
         <tli id="T205" time="102.647" type="appl" />
         <tli id="T206" time="104.894" type="appl" />
         <tli id="T207" time="105.2427" type="appl" />
         <tli id="T208" time="105.59140000000001" type="appl" />
         <tli id="T209" time="105.9401" type="appl" />
         <tli id="T210" time="106.28880000000001" type="appl" />
         <tli id="T211" time="106.6375" type="appl" />
         <tli id="T212" time="106.9862" type="appl" />
         <tli id="T213" time="107.3349" type="appl" />
         <tli id="T214" time="107.6836" type="appl" />
         <tli id="T215" time="108.0323" type="appl" />
         <tli id="T216" time="108.381" type="appl" />
         <tli id="T217" time="108.7297" type="appl" />
         <tli id="T218" time="109.0784" type="appl" />
         <tli id="T219" time="109.4271" type="appl" />
         <tli id="T220" time="109.7758" type="appl" />
         <tli id="T221" time="110.1245" type="appl" />
         <tli id="T222" time="110.47319999999999" type="appl" />
         <tli id="T223" time="110.8219" type="appl" />
         <tli id="T224" time="111.1706" type="appl" />
         <tli id="T225" time="111.5193" type="appl" />
         <tli id="T226" time="111.868" type="appl" />
         <tli id="T227" time="112.101" type="appl" />
         <tli id="T228" time="112.321" type="appl" />
         <tli id="T229" time="112.541" type="appl" />
         <tli id="T230" time="112.761" type="appl" />
         <tli id="T231" time="112.981" type="appl" />
         <tli id="T232" time="113.201" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KAI"
                      type="t">
         <timeline-fork end="T126" start="T125">
            <tli id="T125.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T20" id="Seg_0" n="sc" s="T1">
               <ts e="T8" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Seːpɨlʼak</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">qoralʼa</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">qənta</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">šöttɨ</ts>
                  <nts id="Seg_14" n="HIAT:ip">,</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_17" n="HIAT:w" s="T5">qoraŋa</ts>
                  <nts id="Seg_18" n="HIAT:ip">,</nts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_21" n="HIAT:w" s="T6">qoraŋa</ts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_24" n="HIAT:w" s="T7">nəəj</ts>
                  <nts id="Seg_25" n="HIAT:ip">.</nts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_28" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_30" n="HIAT:w" s="T8">Ukkɨr</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_33" n="HIAT:w" s="T9">čʼontot</ts>
                  <nts id="Seg_34" n="HIAT:ip">,</nts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">qorala</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">poqqɨta</ts>
                  <nts id="Seg_41" n="HIAT:ip">,</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_44" n="HIAT:w" s="T12">qaj</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_47" n="HIAT:w" s="T13">poqqɨtɨlʼa</ts>
                  <nts id="Seg_48" n="HIAT:ip">,</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_50" n="HIAT:ip">(</nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">qaj-</ts>
                  <nts id="Seg_53" n="HIAT:ip">)</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_56" n="HIAT:w" s="T15">qaj</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_59" n="HIAT:w" s="T16">utšelʼas</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_62" n="HIAT:w" s="T17">qənna</ts>
                  <nts id="Seg_63" n="HIAT:ip">,</nts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_66" n="HIAT:w" s="T18">qɔːsa</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_69" n="HIAT:w" s="T19">qənnɨnt</ts>
                  <nts id="Seg_70" n="HIAT:ip">.</nts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T24" id="Seg_72" n="sc" s="T21">
               <ts e="T24" id="Seg_74" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_76" n="HIAT:w" s="T21">Mərqɨ</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_79" n="HIAT:w" s="T22">qɔːsa</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_82" n="HIAT:w" s="T23">qətɨnt</ts>
                  <nts id="Seg_83" n="HIAT:ip">.</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T27" id="Seg_85" n="sc" s="T25">
               <ts e="T27" id="Seg_87" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_89" n="HIAT:w" s="T25">Konna</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_92" n="HIAT:w" s="T26">tanta</ts>
                  <nts id="Seg_93" n="HIAT:ip">.</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T47" id="Seg_95" n="sc" s="T28">
               <ts e="T47" id="Seg_97" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_99" n="HIAT:w" s="T28">Konna</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_102" n="HIAT:w" s="T29">tanta</ts>
                  <nts id="Seg_103" n="HIAT:ip">,</nts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_106" n="HIAT:w" s="T30">qɔːt</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_109" n="HIAT:w" s="T31">mɨta</ts>
                  <nts id="Seg_110" n="HIAT:ip">,</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_113" n="HIAT:w" s="T32">qɔːtɨ</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_115" n="HIAT:ip">(</nts>
                  <nts id="Seg_116" n="HIAT:ip">(</nts>
                  <ats e="T34" id="Seg_117" n="HIAT:non-pho" s="T33">…</ats>
                  <nts id="Seg_118" n="HIAT:ip">)</nts>
                  <nts id="Seg_119" n="HIAT:ip">)</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_122" n="HIAT:w" s="T34">tɛnɨmɨt</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_125" n="HIAT:w" s="T35">qɔːt</ts>
                  <nts id="Seg_126" n="HIAT:ip">,</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_129" n="HIAT:w" s="T36">qɔːt</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_132" n="HIAT:w" s="T37">qɔːsam</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_135" n="HIAT:w" s="T38">ken</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_138" n="HIAT:w" s="T39">mɨnt</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_141" n="HIAT:w" s="T40">čʼɔːwsɨn</ts>
                  <nts id="Seg_142" n="HIAT:ip">,</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_145" n="HIAT:w" s="T41">i</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_148" n="HIAT:w" s="T42">štop</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_151" n="HIAT:w" s="T43">tɛnɨmɨqt</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_153" n="HIAT:ip">(</nts>
                  <nts id="Seg_154" n="HIAT:ip">(</nts>
                  <ats e="T45" id="Seg_155" n="HIAT:non-pho" s="T44">…</ats>
                  <nts id="Seg_156" n="HIAT:ip">)</nts>
                  <nts id="Seg_157" n="HIAT:ip">)</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_160" n="HIAT:w" s="T45">loːsɨ</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_163" n="HIAT:w" s="T46">tɛnɨmɨqt</ts>
                  <nts id="Seg_164" n="HIAT:ip">.</nts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T51" id="Seg_166" n="sc" s="T48">
               <ts e="T51" id="Seg_168" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_170" n="HIAT:w" s="T48">Qɔːsat</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_173" n="HIAT:w" s="T49">ılla</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_176" n="HIAT:w" s="T50">qəčʼot</ts>
                  <nts id="Seg_177" n="HIAT:ip">.</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T57" id="Seg_179" n="sc" s="T52">
               <ts e="T57" id="Seg_181" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_183" n="HIAT:w" s="T52">Qɔːsat</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_186" n="HIAT:w" s="T53">ılla</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_189" n="HIAT:w" s="T54">pirɨmpɨlʼa</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_192" n="HIAT:w" s="T55">ɔːmta</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_195" n="HIAT:w" s="T56">İčʼaičʼɨka</ts>
                  <nts id="Seg_196" n="HIAT:ip">.</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T62" id="Seg_198" n="sc" s="T58">
               <ts e="T62" id="Seg_200" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_202" n="HIAT:w" s="T58">Qɔːt</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_205" n="HIAT:w" s="T59">ɔːmtɨla</ts>
                  <nts id="Seg_206" n="HIAT:ip">,</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_209" n="HIAT:w" s="T60">loːsɨ</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_212" n="HIAT:w" s="T61">qɔːntšɛːnt</ts>
                  <nts id="Seg_213" n="HIAT:ip">.</nts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T67" id="Seg_215" n="sc" s="T63">
               <ts e="T67" id="Seg_217" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_219" n="HIAT:w" s="T63">Loːsɨ</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_222" n="HIAT:w" s="T64">qɔːntšɛːnt</ts>
                  <nts id="Seg_223" n="HIAT:ip">,</nts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_226" n="HIAT:w" s="T65">loːsɨ</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_229" n="HIAT:w" s="T66">tüntɨ</ts>
                  <nts id="Seg_230" n="HIAT:ip">.</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T72" id="Seg_232" n="sc" s="T68">
               <ts e="T72" id="Seg_234" n="HIAT:u" s="T68">
                  <nts id="Seg_235" n="HIAT:ip">“</nts>
                  <ts e="T69" id="Seg_237" n="HIAT:w" s="T68">Tam</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_240" n="HIAT:w" s="T69">ijaja</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_243" n="HIAT:w" s="T70">qaj</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_246" n="HIAT:w" s="T71">meːtɨntɨ</ts>
                  <nts id="Seg_247" n="HIAT:ip">?</nts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T74" id="Seg_249" n="sc" s="T73">
               <ts e="T74" id="Seg_251" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_253" n="HIAT:w" s="T73">Qaj</ts>
                  <nts id="Seg_254" n="HIAT:ip">?</nts>
                  <nts id="Seg_255" n="HIAT:ip">”</nts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T81" id="Seg_257" n="sc" s="T75">
               <ts e="T81" id="Seg_259" n="HIAT:u" s="T75">
                  <nts id="Seg_260" n="HIAT:ip">“</nts>
                  <ts e="T76" id="Seg_262" n="HIAT:w" s="T75">Qaj</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_265" n="HIAT:w" s="T76">meːtɨntak</ts>
                  <nts id="Seg_266" n="HIAT:ip">,</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_269" n="HIAT:w" s="T77">mɨta</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_272" n="HIAT:w" s="T78">apsam</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_275" n="HIAT:w" s="T79">tap</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_278" n="HIAT:w" s="T80">pirɨmɨntak</ts>
                  <nts id="Seg_279" n="HIAT:ip">.</nts>
                  <nts id="Seg_280" n="HIAT:ip">”</nts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T85" id="Seg_282" n="sc" s="T82">
               <ts e="T85" id="Seg_284" n="HIAT:u" s="T82">
                  <nts id="Seg_285" n="HIAT:ip">“</nts>
                  <ts e="T83" id="Seg_287" n="HIAT:w" s="T82">Na</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_290" n="HIAT:w" s="T83">qajɨlʼ</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_293" n="HIAT:w" s="T84">apsal</ts>
                  <nts id="Seg_294" n="HIAT:ip">?</nts>
                  <nts id="Seg_295" n="HIAT:ip">”</nts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T90" id="Seg_297" n="sc" s="T86">
               <ts e="T90" id="Seg_299" n="HIAT:u" s="T86">
                  <nts id="Seg_300" n="HIAT:ip">“</nts>
                  <ts e="T87" id="Seg_302" n="HIAT:w" s="T86">Ašʼ</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_305" n="HIAT:w" s="T87">man</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_308" n="HIAT:w" s="T88">na</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_311" n="HIAT:w" s="T89">mɨtɨlʼam</ts>
                  <nts id="Seg_312" n="HIAT:ip">.</nts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T93" id="Seg_314" n="sc" s="T91">
               <ts e="T93" id="Seg_316" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_318" n="HIAT:w" s="T91">Namalʼ</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_321" n="HIAT:w" s="T92">nʼüːnʼčʼa</ts>
                  <nts id="Seg_322" n="HIAT:ip">.</nts>
                  <nts id="Seg_323" n="HIAT:ip">”</nts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T100" id="Seg_325" n="sc" s="T94">
               <ts e="T100" id="Seg_327" n="HIAT:u" s="T94">
                  <nts id="Seg_328" n="HIAT:ip">“</nts>
                  <ts e="T95" id="Seg_330" n="HIAT:w" s="T94">Tan</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_333" n="HIAT:w" s="T95">mompa</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_336" n="HIAT:w" s="T96">qaj</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_339" n="HIAT:w" s="T97">nilʼčʼi</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_342" n="HIAT:w" s="T98">apsal</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_344" n="HIAT:ip">(</nts>
                  <ts e="T100" id="Seg_346" n="HIAT:w" s="T99">pirɨntal</ts>
                  <nts id="Seg_347" n="HIAT:ip">)</nts>
                  <nts id="Seg_348" n="HIAT:ip">?</nts>
                  <nts id="Seg_349" n="HIAT:ip">”</nts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T109" id="Seg_351" n="sc" s="T101">
               <ts e="T109" id="Seg_353" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_355" n="HIAT:w" s="T101">Nɨnɨ</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_358" n="HIAT:w" s="T102">namantɨ</ts>
                  <nts id="Seg_359" n="HIAT:ip">,</nts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_362" n="HIAT:w" s="T103">это</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_365" n="HIAT:w" s="T104">интересно</ts>
                  <nts id="Seg_366" n="HIAT:ip">,</nts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_369" n="HIAT:w" s="T105">kətɨlʼap</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_372" n="HIAT:w" s="T106">tap</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_375" n="HIAT:w" s="T107">što</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_378" n="HIAT:w" s="T108">lʼi</ts>
                  <nts id="Seg_379" n="HIAT:ip">.</nts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T114" id="Seg_381" n="sc" s="T110">
               <ts e="T114" id="Seg_383" n="HIAT:u" s="T110">
                  <nts id="Seg_384" n="HIAT:ip">“</nts>
                  <ts e="T111" id="Seg_386" n="HIAT:w" s="T110">Na</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_389" n="HIAT:w" s="T111">man</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_392" n="HIAT:w" s="T112">aššʼa</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_395" n="HIAT:w" s="T113">qoptɨksaŋ</ts>
                  <nts id="Seg_396" n="HIAT:ip">.</nts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T123" id="Seg_398" n="sc" s="T115">
               <ts e="T123" id="Seg_400" n="HIAT:u" s="T115">
                  <ts e="T116" id="Seg_402" n="HIAT:w" s="T115">Onak</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_405" n="HIAT:w" s="T116">šaŋ</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_408" n="HIAT:w" s="T117">qoptɨksaŋ</ts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_411" n="HIAT:w" s="T118">mompa</ts>
                  <nts id="Seg_412" n="HIAT:ip">,</nts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_414" n="HIAT:ip">(</nts>
                  <ts e="T120" id="Seg_416" n="HIAT:w" s="T119">nam-</ts>
                  <nts id="Seg_417" n="HIAT:ip">)</nts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_420" n="HIAT:w" s="T120">namap</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_422" n="HIAT:ip">(</nts>
                  <ts e="T122" id="Seg_424" n="HIAT:w" s="T121">sam</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_427" n="HIAT:w" s="T122">pirɨmpam</ts>
                  <nts id="Seg_428" n="HIAT:ip">)</nts>
                  <nts id="Seg_429" n="HIAT:ip">.</nts>
                  <nts id="Seg_430" n="HIAT:ip">”</nts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T126" id="Seg_432" n="sc" s="T124">
               <ts e="T126" id="Seg_434" n="HIAT:u" s="T124">
                  <nts id="Seg_435" n="HIAT:ip">“</nts>
                  <ts e="T125" id="Seg_437" n="HIAT:w" s="T124">Qaj</ts>
                  <nts id="Seg_438" n="HIAT:ip">,</nts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125.tx.1" id="Seg_441" n="HIAT:w" s="T125">kutar</ts>
                  <nts id="Seg_442" n="HIAT:ip">(</nts>
                  <nts id="Seg_443" n="HIAT:ip">(</nts>
                  <ats e="T126" id="Seg_444" n="HIAT:non-pho" s="T125.tx.1">…</ats>
                  <nts id="Seg_445" n="HIAT:ip">)</nts>
                  <nts id="Seg_446" n="HIAT:ip">)</nts>
                  <nts id="Seg_447" n="HIAT:ip">?</nts>
                  <nts id="Seg_448" n="HIAT:ip">”</nts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T141" id="Seg_450" n="sc" s="T127">
               <ts e="T141" id="Seg_452" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_454" n="HIAT:w" s="T127">Təm</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_457" n="HIAT:w" s="T128">na</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_459" n="HIAT:ip">(</nts>
                  <ts e="T130" id="Seg_461" n="HIAT:w" s="T129">kolʼamtɨ</ts>
                  <nts id="Seg_462" n="HIAT:ip">)</nts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_465" n="HIAT:w" s="T130">ašša</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_468" n="HIAT:w" s="T131">qɔːptɨ</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_471" n="HIAT:w" s="T132">ɛːsɨ</ts>
                  <nts id="Seg_472" n="HIAT:ip">,</nts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_474" n="HIAT:ip">(</nts>
                  <nts id="Seg_475" n="HIAT:ip">(</nts>
                  <ats e="T134" id="Seg_476" n="HIAT:non-pho" s="T133">…</ats>
                  <nts id="Seg_477" n="HIAT:ip">)</nts>
                  <nts id="Seg_478" n="HIAT:ip">)</nts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_481" n="HIAT:w" s="T134">namalʼ</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_484" n="HIAT:w" s="T135">pirät</ts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_486" n="HIAT:ip">(</nts>
                  <nts id="Seg_487" n="HIAT:ip">(</nts>
                  <ats e="T137" id="Seg_488" n="HIAT:non-pho" s="T136">…</ats>
                  <nts id="Seg_489" n="HIAT:ip">)</nts>
                  <nts id="Seg_490" n="HIAT:ip">)</nts>
                  <nts id="Seg_491" n="HIAT:ip">,</nts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_494" n="HIAT:w" s="T137">namantɨ</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_497" n="HIAT:w" s="T138">nʼüːnʼɨk</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_499" n="HIAT:ip">(</nts>
                  <ts e="T140" id="Seg_501" n="HIAT:w" s="T139">ɛŋa</ts>
                  <nts id="Seg_502" n="HIAT:ip">)</nts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_504" n="HIAT:ip">(</nts>
                  <nts id="Seg_505" n="HIAT:ip">(</nts>
                  <ats e="T141" id="Seg_506" n="HIAT:non-pho" s="T140">…</ats>
                  <nts id="Seg_507" n="HIAT:ip">)</nts>
                  <nts id="Seg_508" n="HIAT:ip">)</nts>
                  <nts id="Seg_509" n="HIAT:ip">.</nts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T143" id="Seg_511" n="sc" s="T142">
               <ts e="T143" id="Seg_513" n="HIAT:u" s="T142">
                  <ts e="T143" id="Seg_515" n="HIAT:w" s="T142">Tal</ts>
                  <nts id="Seg_516" n="HIAT:ip">.</nts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T147" id="Seg_518" n="sc" s="T144">
               <ts e="T147" id="Seg_520" n="HIAT:u" s="T144">
                  <ts e="T145" id="Seg_522" n="HIAT:w" s="T144">Man</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_525" n="HIAT:w" s="T145">aššʼ</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_528" n="HIAT:w" s="T146">mintak</ts>
                  <nts id="Seg_529" n="HIAT:ip">.</nts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T155" id="Seg_531" n="sc" s="T148">
               <ts e="T155" id="Seg_533" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_535" n="HIAT:w" s="T148">Ilʼčʼa</ts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_538" n="HIAT:w" s="T149">nılʼ</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_541" n="HIAT:w" s="T150">orɨnʼnʼa</ts>
                  <nts id="Seg_542" n="HIAT:ip">:</nts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_544" n="HIAT:ip">“</nts>
                  <ts e="T152" id="Seg_546" n="HIAT:w" s="T151">Man</ts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_549" n="HIAT:w" s="T152">aššʼ</ts>
                  <nts id="Seg_550" n="HIAT:ip">,</nts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_553" n="HIAT:w" s="T153">aššʼ</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_556" n="HIAT:w" s="T154">mintak</ts>
                  <nts id="Seg_557" n="HIAT:ip">.</nts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T165" id="Seg_559" n="sc" s="T156">
               <ts e="T165" id="Seg_561" n="HIAT:u" s="T156">
                  <ts e="T157" id="Seg_563" n="HIAT:w" s="T156">Tan</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_566" n="HIAT:w" s="T157">äj</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_569" n="HIAT:w" s="T158">mɨ</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_571" n="HIAT:ip">(</nts>
                  <nts id="Seg_572" n="HIAT:ip">(</nts>
                  <ats e="T160" id="Seg_573" n="HIAT:non-pho" s="T159">…</ats>
                  <nts id="Seg_574" n="HIAT:ip">)</nts>
                  <nts id="Seg_575" n="HIAT:ip">)</nts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_578" n="HIAT:w" s="T160">man</ts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_581" n="HIAT:w" s="T161">mompa</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_583" n="HIAT:ip">(</nts>
                  <ts e="T163" id="Seg_585" n="HIAT:w" s="T162">loːsɨ</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_588" n="HIAT:w" s="T163">iralʼ</ts>
                  <nts id="Seg_589" n="HIAT:ip">)</nts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_592" n="HIAT:w" s="T164">qup</ts>
                  <nts id="Seg_593" n="HIAT:ip">.</nts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T167" id="Seg_595" n="sc" s="T166">
               <ts e="T167" id="Seg_597" n="HIAT:u" s="T166">
                  <ts e="T167" id="Seg_599" n="HIAT:w" s="T166">Tal</ts>
                  <nts id="Seg_600" n="HIAT:ip">.</nts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T170" id="Seg_602" n="sc" s="T168">
               <ts e="T170" id="Seg_604" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_606" n="HIAT:w" s="T168">Loːsɨ</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_609" n="HIAT:w" s="T169">iralʼ</ts>
                  <nts id="Seg_610" n="HIAT:ip">.</nts>
                  <nts id="Seg_611" n="HIAT:ip">”</nts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T188" id="Seg_613" n="sc" s="T171">
               <ts e="T188" id="Seg_615" n="HIAT:u" s="T171">
                  <nts id="Seg_616" n="HIAT:ip">“</nts>
                  <ts e="T172" id="Seg_618" n="HIAT:w" s="T171">Nu</ts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_621" n="HIAT:w" s="T172">latno</ts>
                  <nts id="Seg_622" n="HIAT:ip">,</nts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_625" n="HIAT:w" s="T173">tan</ts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_628" n="HIAT:w" s="T174">mompa</ts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_631" n="HIAT:w" s="T175">ašša</ts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_634" n="HIAT:w" s="T176">qup</ts>
                  <nts id="Seg_635" n="HIAT:ip">,</nts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_638" n="HIAT:w" s="T177">tan</ts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_641" n="HIAT:w" s="T178">mompa</ts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_644" n="HIAT:w" s="T179">mat</ts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_647" n="HIAT:w" s="T180">šintɨ</ts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_650" n="HIAT:w" s="T181">qurɨlak</ts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_653" n="HIAT:w" s="T182">mompa</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_656" n="HIAT:w" s="T183">tap</ts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_659" n="HIAT:w" s="T184">mükälʼ</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_661" n="HIAT:ip">(</nts>
                  <nts id="Seg_662" n="HIAT:ip">(</nts>
                  <ats e="T186" id="Seg_663" n="HIAT:non-pho" s="T185">…</ats>
                  <nts id="Seg_664" n="HIAT:ip">)</nts>
                  <nts id="Seg_665" n="HIAT:ip">)</nts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_668" n="HIAT:w" s="T186">moː</ts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_670" n="HIAT:ip">(</nts>
                  <ts e="T188" id="Seg_672" n="HIAT:w" s="T187">to</ts>
                  <nts id="Seg_673" n="HIAT:ip">)</nts>
                  <nts id="Seg_674" n="HIAT:ip">.</nts>
                  <nts id="Seg_675" n="HIAT:ip">”</nts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T205" id="Seg_677" n="sc" s="T189">
               <ts e="T205" id="Seg_679" n="HIAT:u" s="T189">
                  <ts e="T190" id="Seg_681" n="HIAT:w" s="T189">Nɨnɨ</ts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_684" n="HIAT:w" s="T190">tına</ts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_687" n="HIAT:w" s="T191">mükat</ts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_690" n="HIAT:w" s="T192">näkälä</ts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_693" n="HIAT:w" s="T193">mükat</ts>
                  <nts id="Seg_694" n="HIAT:ip">,</nts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_697" n="HIAT:w" s="T194">mükat</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_699" n="HIAT:ip">(</nts>
                  <ts e="T196" id="Seg_701" n="HIAT:w" s="T195">moːmtɨ</ts>
                  <nts id="Seg_702" n="HIAT:ip">)</nts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_705" n="HIAT:w" s="T196">meːqɨlnɨt</ts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_708" n="HIAT:w" s="T197">meːqɨlnɨt</ts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_711" n="HIAT:w" s="T198">tına</ts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_714" n="HIAT:w" s="T199">loːsɨ</ts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_717" n="HIAT:w" s="T200">iramtɨ</ts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_720" n="HIAT:w" s="T201">näіntɨ</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_722" n="HIAT:ip">(</nts>
                  <ts e="T203" id="Seg_724" n="HIAT:w" s="T202">m-</ts>
                  <nts id="Seg_725" n="HIAT:ip">)</nts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_728" n="HIAT:w" s="T203">pontɨ</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_731" n="HIAT:w" s="T204">kuraiŋɨtɨ</ts>
                  <nts id="Seg_732" n="HIAT:ip">.</nts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T226" id="Seg_734" n="sc" s="T206">
               <ts e="T226" id="Seg_736" n="HIAT:u" s="T206">
                  <ts e="T207" id="Seg_738" n="HIAT:w" s="T206">Loːsɨ</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_741" n="HIAT:w" s="T207">iratɨ</ts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_744" n="HIAT:w" s="T208">pontɨ</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_747" n="HIAT:w" s="T209">kuraіŋɨtɨ</ts>
                  <nts id="Seg_748" n="HIAT:ip">,</nts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_751" n="HIAT:w" s="T210">tına</ts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_754" n="HIAT:w" s="T211">loːsɨ</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_757" n="HIAT:w" s="T212">iratɨ</ts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_760" n="HIAT:w" s="T213">näіntɨ</ts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_763" n="HIAT:w" s="T214">pontɨ</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_765" n="HIAT:ip">(</nts>
                  <ts e="T216" id="Seg_767" n="HIAT:w" s="T215">kur-</ts>
                  <nts id="Seg_768" n="HIAT:ip">)</nts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_771" n="HIAT:w" s="T216">kuraіla</ts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_774" n="HIAT:w" s="T217">loːsɨ</ts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_777" n="HIAT:w" s="T218">ira</ts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_780" n="HIAT:w" s="T219">qotta</ts>
                  <nts id="Seg_781" n="HIAT:ip">,</nts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_784" n="HIAT:w" s="T220">loːsɨ</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_787" n="HIAT:w" s="T221">irap</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_790" n="HIAT:w" s="T222">pärqɨllä</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_793" n="HIAT:w" s="T223">mɨlä</ts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_796" n="HIAT:w" s="T224">ılla</ts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_799" n="HIAT:w" s="T225">qənnɨt</ts>
                  <nts id="Seg_800" n="HIAT:ip">.</nts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T232" id="Seg_802" n="sc" s="T227">
               <ts e="T232" id="Seg_804" n="HIAT:u" s="T227">
                  <ts e="T228" id="Seg_806" n="HIAT:w" s="T227">Loːsɨ</ts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_809" n="HIAT:w" s="T228">ira</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_812" n="HIAT:w" s="T229">pärqɨllä</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_815" n="HIAT:w" s="T230">ılla</ts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_818" n="HIAT:w" s="T231">qətaja</ts>
                  <nts id="Seg_819" n="HIAT:ip">.</nts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T20" id="Seg_821" n="sc" s="T1">
               <ts e="T2" id="Seg_823" n="e" s="T1">Seːpɨlʼak </ts>
               <ts e="T3" id="Seg_825" n="e" s="T2">qoralʼa </ts>
               <ts e="T4" id="Seg_827" n="e" s="T3">qənta </ts>
               <ts e="T5" id="Seg_829" n="e" s="T4">šöttɨ, </ts>
               <ts e="T6" id="Seg_831" n="e" s="T5">qoraŋa, </ts>
               <ts e="T7" id="Seg_833" n="e" s="T6">qoraŋa </ts>
               <ts e="T8" id="Seg_835" n="e" s="T7">nəəj. </ts>
               <ts e="T9" id="Seg_837" n="e" s="T8">Ukkɨr </ts>
               <ts e="T10" id="Seg_839" n="e" s="T9">čʼontot, </ts>
               <ts e="T11" id="Seg_841" n="e" s="T10">qorala </ts>
               <ts e="T12" id="Seg_843" n="e" s="T11">poqqɨta, </ts>
               <ts e="T13" id="Seg_845" n="e" s="T12">qaj </ts>
               <ts e="T14" id="Seg_847" n="e" s="T13">poqqɨtɨlʼa, </ts>
               <ts e="T15" id="Seg_849" n="e" s="T14">(qaj-) </ts>
               <ts e="T16" id="Seg_851" n="e" s="T15">qaj </ts>
               <ts e="T17" id="Seg_853" n="e" s="T16">utšelʼas </ts>
               <ts e="T18" id="Seg_855" n="e" s="T17">qənna, </ts>
               <ts e="T19" id="Seg_857" n="e" s="T18">qɔːsa </ts>
               <ts e="T20" id="Seg_859" n="e" s="T19">qənnɨnt. </ts>
            </ts>
            <ts e="T24" id="Seg_860" n="sc" s="T21">
               <ts e="T22" id="Seg_862" n="e" s="T21">Mərqɨ </ts>
               <ts e="T23" id="Seg_864" n="e" s="T22">qɔːsa </ts>
               <ts e="T24" id="Seg_866" n="e" s="T23">qətɨnt. </ts>
            </ts>
            <ts e="T27" id="Seg_867" n="sc" s="T25">
               <ts e="T26" id="Seg_869" n="e" s="T25">Konna </ts>
               <ts e="T27" id="Seg_871" n="e" s="T26">tanta. </ts>
            </ts>
            <ts e="T47" id="Seg_872" n="sc" s="T28">
               <ts e="T29" id="Seg_874" n="e" s="T28">Konna </ts>
               <ts e="T30" id="Seg_876" n="e" s="T29">tanta, </ts>
               <ts e="T31" id="Seg_878" n="e" s="T30">qɔːt </ts>
               <ts e="T32" id="Seg_880" n="e" s="T31">mɨta, </ts>
               <ts e="T33" id="Seg_882" n="e" s="T32">qɔːtɨ </ts>
               <ts e="T34" id="Seg_884" n="e" s="T33">((…)) </ts>
               <ts e="T35" id="Seg_886" n="e" s="T34">tɛnɨmɨt </ts>
               <ts e="T36" id="Seg_888" n="e" s="T35">qɔːt, </ts>
               <ts e="T37" id="Seg_890" n="e" s="T36">qɔːt </ts>
               <ts e="T38" id="Seg_892" n="e" s="T37">qɔːsam </ts>
               <ts e="T39" id="Seg_894" n="e" s="T38">ken </ts>
               <ts e="T40" id="Seg_896" n="e" s="T39">mɨnt </ts>
               <ts e="T41" id="Seg_898" n="e" s="T40">čʼɔːwsɨn, </ts>
               <ts e="T42" id="Seg_900" n="e" s="T41">i </ts>
               <ts e="T43" id="Seg_902" n="e" s="T42">štop </ts>
               <ts e="T44" id="Seg_904" n="e" s="T43">tɛnɨmɨqt </ts>
               <ts e="T45" id="Seg_906" n="e" s="T44">((…)) </ts>
               <ts e="T46" id="Seg_908" n="e" s="T45">loːsɨ </ts>
               <ts e="T47" id="Seg_910" n="e" s="T46">tɛnɨmɨqt. </ts>
            </ts>
            <ts e="T51" id="Seg_911" n="sc" s="T48">
               <ts e="T49" id="Seg_913" n="e" s="T48">Qɔːsat </ts>
               <ts e="T50" id="Seg_915" n="e" s="T49">ılla </ts>
               <ts e="T51" id="Seg_917" n="e" s="T50">qəčʼot. </ts>
            </ts>
            <ts e="T57" id="Seg_918" n="sc" s="T52">
               <ts e="T53" id="Seg_920" n="e" s="T52">Qɔːsat </ts>
               <ts e="T54" id="Seg_922" n="e" s="T53">ılla </ts>
               <ts e="T55" id="Seg_924" n="e" s="T54">pirɨmpɨlʼa </ts>
               <ts e="T56" id="Seg_926" n="e" s="T55">ɔːmta </ts>
               <ts e="T57" id="Seg_928" n="e" s="T56">İčʼaičʼɨka. </ts>
            </ts>
            <ts e="T62" id="Seg_929" n="sc" s="T58">
               <ts e="T59" id="Seg_931" n="e" s="T58">Qɔːt </ts>
               <ts e="T60" id="Seg_933" n="e" s="T59">ɔːmtɨla, </ts>
               <ts e="T61" id="Seg_935" n="e" s="T60">loːsɨ </ts>
               <ts e="T62" id="Seg_937" n="e" s="T61">qɔːntšɛːnt. </ts>
            </ts>
            <ts e="T67" id="Seg_938" n="sc" s="T63">
               <ts e="T64" id="Seg_940" n="e" s="T63">Loːsɨ </ts>
               <ts e="T65" id="Seg_942" n="e" s="T64">qɔːntšɛːnt, </ts>
               <ts e="T66" id="Seg_944" n="e" s="T65">loːsɨ </ts>
               <ts e="T67" id="Seg_946" n="e" s="T66">tüntɨ. </ts>
            </ts>
            <ts e="T72" id="Seg_947" n="sc" s="T68">
               <ts e="T69" id="Seg_949" n="e" s="T68">“Tam </ts>
               <ts e="T70" id="Seg_951" n="e" s="T69">ijaja </ts>
               <ts e="T71" id="Seg_953" n="e" s="T70">qaj </ts>
               <ts e="T72" id="Seg_955" n="e" s="T71">meːtɨntɨ? </ts>
            </ts>
            <ts e="T74" id="Seg_956" n="sc" s="T73">
               <ts e="T74" id="Seg_958" n="e" s="T73">Qaj?” </ts>
            </ts>
            <ts e="T81" id="Seg_959" n="sc" s="T75">
               <ts e="T76" id="Seg_961" n="e" s="T75">“Qaj </ts>
               <ts e="T77" id="Seg_963" n="e" s="T76">meːtɨntak, </ts>
               <ts e="T78" id="Seg_965" n="e" s="T77">mɨta </ts>
               <ts e="T79" id="Seg_967" n="e" s="T78">apsam </ts>
               <ts e="T80" id="Seg_969" n="e" s="T79">tap </ts>
               <ts e="T81" id="Seg_971" n="e" s="T80">pirɨmɨntak.” </ts>
            </ts>
            <ts e="T85" id="Seg_972" n="sc" s="T82">
               <ts e="T83" id="Seg_974" n="e" s="T82">“Na </ts>
               <ts e="T84" id="Seg_976" n="e" s="T83">qajɨlʼ </ts>
               <ts e="T85" id="Seg_978" n="e" s="T84">apsal?” </ts>
            </ts>
            <ts e="T90" id="Seg_979" n="sc" s="T86">
               <ts e="T87" id="Seg_981" n="e" s="T86">“Ašʼ </ts>
               <ts e="T88" id="Seg_983" n="e" s="T87">man </ts>
               <ts e="T89" id="Seg_985" n="e" s="T88">na </ts>
               <ts e="T90" id="Seg_987" n="e" s="T89">mɨtɨlʼam. </ts>
            </ts>
            <ts e="T93" id="Seg_988" n="sc" s="T91">
               <ts e="T92" id="Seg_990" n="e" s="T91">Namalʼ </ts>
               <ts e="T93" id="Seg_992" n="e" s="T92">nʼüːnʼčʼa.” </ts>
            </ts>
            <ts e="T100" id="Seg_993" n="sc" s="T94">
               <ts e="T95" id="Seg_995" n="e" s="T94">“Tan </ts>
               <ts e="T96" id="Seg_997" n="e" s="T95">mompa </ts>
               <ts e="T97" id="Seg_999" n="e" s="T96">qaj </ts>
               <ts e="T98" id="Seg_1001" n="e" s="T97">nilʼčʼi </ts>
               <ts e="T99" id="Seg_1003" n="e" s="T98">apsal </ts>
               <ts e="T100" id="Seg_1005" n="e" s="T99">(pirɨntal)?” </ts>
            </ts>
            <ts e="T109" id="Seg_1006" n="sc" s="T101">
               <ts e="T102" id="Seg_1008" n="e" s="T101">Nɨnɨ </ts>
               <ts e="T103" id="Seg_1010" n="e" s="T102">namantɨ, </ts>
               <ts e="T104" id="Seg_1012" n="e" s="T103">это </ts>
               <ts e="T105" id="Seg_1014" n="e" s="T104">интересно, </ts>
               <ts e="T106" id="Seg_1016" n="e" s="T105">kətɨlʼap </ts>
               <ts e="T107" id="Seg_1018" n="e" s="T106">tap </ts>
               <ts e="T108" id="Seg_1020" n="e" s="T107">što </ts>
               <ts e="T109" id="Seg_1022" n="e" s="T108">lʼi. </ts>
            </ts>
            <ts e="T114" id="Seg_1023" n="sc" s="T110">
               <ts e="T111" id="Seg_1025" n="e" s="T110">“Na </ts>
               <ts e="T112" id="Seg_1027" n="e" s="T111">man </ts>
               <ts e="T113" id="Seg_1029" n="e" s="T112">aššʼa </ts>
               <ts e="T114" id="Seg_1031" n="e" s="T113">qoptɨksaŋ. </ts>
            </ts>
            <ts e="T123" id="Seg_1032" n="sc" s="T115">
               <ts e="T116" id="Seg_1034" n="e" s="T115">Onak </ts>
               <ts e="T117" id="Seg_1036" n="e" s="T116">šaŋ </ts>
               <ts e="T118" id="Seg_1038" n="e" s="T117">qoptɨksaŋ </ts>
               <ts e="T119" id="Seg_1040" n="e" s="T118">mompa, </ts>
               <ts e="T120" id="Seg_1042" n="e" s="T119">(nam-) </ts>
               <ts e="T121" id="Seg_1044" n="e" s="T120">namap </ts>
               <ts e="T122" id="Seg_1046" n="e" s="T121">(sam </ts>
               <ts e="T123" id="Seg_1048" n="e" s="T122">pirɨmpam).” </ts>
            </ts>
            <ts e="T126" id="Seg_1049" n="sc" s="T124">
               <ts e="T125" id="Seg_1051" n="e" s="T124">“Qaj, </ts>
               <ts e="T126" id="Seg_1053" n="e" s="T125">kutar((…))?” </ts>
            </ts>
            <ts e="T141" id="Seg_1054" n="sc" s="T127">
               <ts e="T128" id="Seg_1056" n="e" s="T127">Təm </ts>
               <ts e="T129" id="Seg_1058" n="e" s="T128">na </ts>
               <ts e="T130" id="Seg_1060" n="e" s="T129">(kolʼamtɨ) </ts>
               <ts e="T131" id="Seg_1062" n="e" s="T130">ašša </ts>
               <ts e="T132" id="Seg_1064" n="e" s="T131">qɔːptɨ </ts>
               <ts e="T133" id="Seg_1066" n="e" s="T132">ɛːsɨ, </ts>
               <ts e="T134" id="Seg_1068" n="e" s="T133">((…)) </ts>
               <ts e="T135" id="Seg_1070" n="e" s="T134">namalʼ </ts>
               <ts e="T136" id="Seg_1072" n="e" s="T135">pirät </ts>
               <ts e="T137" id="Seg_1074" n="e" s="T136">((…)), </ts>
               <ts e="T138" id="Seg_1076" n="e" s="T137">namantɨ </ts>
               <ts e="T139" id="Seg_1078" n="e" s="T138">nʼüːnʼɨk </ts>
               <ts e="T140" id="Seg_1080" n="e" s="T139">(ɛŋa) </ts>
               <ts e="T141" id="Seg_1082" n="e" s="T140">((…)). </ts>
            </ts>
            <ts e="T143" id="Seg_1083" n="sc" s="T142">
               <ts e="T143" id="Seg_1085" n="e" s="T142">Tal. </ts>
            </ts>
            <ts e="T147" id="Seg_1086" n="sc" s="T144">
               <ts e="T145" id="Seg_1088" n="e" s="T144">Man </ts>
               <ts e="T146" id="Seg_1090" n="e" s="T145">aššʼ </ts>
               <ts e="T147" id="Seg_1092" n="e" s="T146">mintak. </ts>
            </ts>
            <ts e="T155" id="Seg_1093" n="sc" s="T148">
               <ts e="T149" id="Seg_1095" n="e" s="T148">Ilʼčʼa </ts>
               <ts e="T150" id="Seg_1097" n="e" s="T149">nılʼ </ts>
               <ts e="T151" id="Seg_1099" n="e" s="T150">orɨnʼnʼa: </ts>
               <ts e="T152" id="Seg_1101" n="e" s="T151">“Man </ts>
               <ts e="T153" id="Seg_1103" n="e" s="T152">aššʼ, </ts>
               <ts e="T154" id="Seg_1105" n="e" s="T153">aššʼ </ts>
               <ts e="T155" id="Seg_1107" n="e" s="T154">mintak. </ts>
            </ts>
            <ts e="T165" id="Seg_1108" n="sc" s="T156">
               <ts e="T157" id="Seg_1110" n="e" s="T156">Tan </ts>
               <ts e="T158" id="Seg_1112" n="e" s="T157">äj </ts>
               <ts e="T159" id="Seg_1114" n="e" s="T158">mɨ </ts>
               <ts e="T160" id="Seg_1116" n="e" s="T159">((…)) </ts>
               <ts e="T161" id="Seg_1118" n="e" s="T160">man </ts>
               <ts e="T162" id="Seg_1120" n="e" s="T161">mompa </ts>
               <ts e="T163" id="Seg_1122" n="e" s="T162">(loːsɨ </ts>
               <ts e="T164" id="Seg_1124" n="e" s="T163">iralʼ) </ts>
               <ts e="T165" id="Seg_1126" n="e" s="T164">qup. </ts>
            </ts>
            <ts e="T167" id="Seg_1127" n="sc" s="T166">
               <ts e="T167" id="Seg_1129" n="e" s="T166">Tal. </ts>
            </ts>
            <ts e="T170" id="Seg_1130" n="sc" s="T168">
               <ts e="T169" id="Seg_1132" n="e" s="T168">Loːsɨ </ts>
               <ts e="T170" id="Seg_1134" n="e" s="T169">iralʼ.” </ts>
            </ts>
            <ts e="T188" id="Seg_1135" n="sc" s="T171">
               <ts e="T172" id="Seg_1137" n="e" s="T171">“Nu </ts>
               <ts e="T173" id="Seg_1139" n="e" s="T172">latno, </ts>
               <ts e="T174" id="Seg_1141" n="e" s="T173">tan </ts>
               <ts e="T175" id="Seg_1143" n="e" s="T174">mompa </ts>
               <ts e="T176" id="Seg_1145" n="e" s="T175">ašša </ts>
               <ts e="T177" id="Seg_1147" n="e" s="T176">qup, </ts>
               <ts e="T178" id="Seg_1149" n="e" s="T177">tan </ts>
               <ts e="T179" id="Seg_1151" n="e" s="T178">mompa </ts>
               <ts e="T180" id="Seg_1153" n="e" s="T179">mat </ts>
               <ts e="T181" id="Seg_1155" n="e" s="T180">šintɨ </ts>
               <ts e="T182" id="Seg_1157" n="e" s="T181">qurɨlak </ts>
               <ts e="T183" id="Seg_1159" n="e" s="T182">mompa </ts>
               <ts e="T184" id="Seg_1161" n="e" s="T183">tap </ts>
               <ts e="T185" id="Seg_1163" n="e" s="T184">mükälʼ </ts>
               <ts e="T186" id="Seg_1165" n="e" s="T185">((…)) </ts>
               <ts e="T187" id="Seg_1167" n="e" s="T186">moː </ts>
               <ts e="T188" id="Seg_1169" n="e" s="T187">(to).” </ts>
            </ts>
            <ts e="T205" id="Seg_1170" n="sc" s="T189">
               <ts e="T190" id="Seg_1172" n="e" s="T189">Nɨnɨ </ts>
               <ts e="T191" id="Seg_1174" n="e" s="T190">tına </ts>
               <ts e="T192" id="Seg_1176" n="e" s="T191">mükat </ts>
               <ts e="T193" id="Seg_1178" n="e" s="T192">näkälä </ts>
               <ts e="T194" id="Seg_1180" n="e" s="T193">mükat, </ts>
               <ts e="T195" id="Seg_1182" n="e" s="T194">mükat </ts>
               <ts e="T196" id="Seg_1184" n="e" s="T195">(moːmtɨ) </ts>
               <ts e="T197" id="Seg_1186" n="e" s="T196">meːqɨlnɨt </ts>
               <ts e="T198" id="Seg_1188" n="e" s="T197">meːqɨlnɨt </ts>
               <ts e="T199" id="Seg_1190" n="e" s="T198">tına </ts>
               <ts e="T200" id="Seg_1192" n="e" s="T199">loːsɨ </ts>
               <ts e="T201" id="Seg_1194" n="e" s="T200">iramtɨ </ts>
               <ts e="T202" id="Seg_1196" n="e" s="T201">näіntɨ </ts>
               <ts e="T203" id="Seg_1198" n="e" s="T202">(m-) </ts>
               <ts e="T204" id="Seg_1200" n="e" s="T203">pontɨ </ts>
               <ts e="T205" id="Seg_1202" n="e" s="T204">kuraiŋɨtɨ. </ts>
            </ts>
            <ts e="T226" id="Seg_1203" n="sc" s="T206">
               <ts e="T207" id="Seg_1205" n="e" s="T206">Loːsɨ </ts>
               <ts e="T208" id="Seg_1207" n="e" s="T207">iratɨ </ts>
               <ts e="T209" id="Seg_1209" n="e" s="T208">pontɨ </ts>
               <ts e="T210" id="Seg_1211" n="e" s="T209">kuraіŋɨtɨ, </ts>
               <ts e="T211" id="Seg_1213" n="e" s="T210">tına </ts>
               <ts e="T212" id="Seg_1215" n="e" s="T211">loːsɨ </ts>
               <ts e="T213" id="Seg_1217" n="e" s="T212">iratɨ </ts>
               <ts e="T214" id="Seg_1219" n="e" s="T213">näіntɨ </ts>
               <ts e="T215" id="Seg_1221" n="e" s="T214">pontɨ </ts>
               <ts e="T216" id="Seg_1223" n="e" s="T215">(kur-) </ts>
               <ts e="T217" id="Seg_1225" n="e" s="T216">kuraіla </ts>
               <ts e="T218" id="Seg_1227" n="e" s="T217">loːsɨ </ts>
               <ts e="T219" id="Seg_1229" n="e" s="T218">ira </ts>
               <ts e="T220" id="Seg_1231" n="e" s="T219">qotta, </ts>
               <ts e="T221" id="Seg_1233" n="e" s="T220">loːsɨ </ts>
               <ts e="T222" id="Seg_1235" n="e" s="T221">irap </ts>
               <ts e="T223" id="Seg_1237" n="e" s="T222">pärqɨllä </ts>
               <ts e="T224" id="Seg_1239" n="e" s="T223">mɨlä </ts>
               <ts e="T225" id="Seg_1241" n="e" s="T224">ılla </ts>
               <ts e="T226" id="Seg_1243" n="e" s="T225">qənnɨt. </ts>
            </ts>
            <ts e="T232" id="Seg_1244" n="sc" s="T227">
               <ts e="T228" id="Seg_1246" n="e" s="T227">Loːsɨ </ts>
               <ts e="T229" id="Seg_1248" n="e" s="T228">ira </ts>
               <ts e="T230" id="Seg_1250" n="e" s="T229">pärqɨllä </ts>
               <ts e="T231" id="Seg_1252" n="e" s="T230">ılla </ts>
               <ts e="T232" id="Seg_1254" n="e" s="T231">qətaja. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T8" id="Seg_1255" s="T1">KAI_196X_Perch_flk.001 (001)</ta>
            <ta e="T20" id="Seg_1256" s="T8">KAI_196X_Perch_flk.002 (002)</ta>
            <ta e="T24" id="Seg_1257" s="T21">KAI_196X_Perch_flk.003 (003)</ta>
            <ta e="T27" id="Seg_1258" s="T25">KAI_196X_Perch_flk.004 (004)</ta>
            <ta e="T47" id="Seg_1259" s="T28">KAI_196X_Perch_flk.005 (005)</ta>
            <ta e="T51" id="Seg_1260" s="T48">KAI_196X_Perch_flk.006 (006)</ta>
            <ta e="T57" id="Seg_1261" s="T52">KAI_196X_Perch_flk.007 (007)</ta>
            <ta e="T62" id="Seg_1262" s="T58">KAI_196X_Perch_flk.008 (008)</ta>
            <ta e="T67" id="Seg_1263" s="T63">KAI_196X_Perch_flk.009 (009)</ta>
            <ta e="T72" id="Seg_1264" s="T68">KAI_196X_Perch_flk.010 (010)</ta>
            <ta e="T74" id="Seg_1265" s="T73">KAI_196X_Perch_flk.011 (011)</ta>
            <ta e="T81" id="Seg_1266" s="T75">KAI_196X_Perch_flk.012 (012)</ta>
            <ta e="T85" id="Seg_1267" s="T82">KAI_196X_Perch_flk.013 (013)</ta>
            <ta e="T90" id="Seg_1268" s="T86">KAI_196X_Perch_flk.014 (014)</ta>
            <ta e="T93" id="Seg_1269" s="T91">KAI_196X_Perch_flk.015 (015)</ta>
            <ta e="T100" id="Seg_1270" s="T94">KAI_196X_Perch_flk.016 (016)</ta>
            <ta e="T109" id="Seg_1271" s="T101">KAI_196X_Perch_flk.017 (017)</ta>
            <ta e="T114" id="Seg_1272" s="T110">KAI_196X_Perch_flk.018 (018)</ta>
            <ta e="T123" id="Seg_1273" s="T115">KAI_196X_Perch_flk.019 (019)</ta>
            <ta e="T126" id="Seg_1274" s="T124">KAI_196X_Perch_flk.020 (020)</ta>
            <ta e="T141" id="Seg_1275" s="T127">KAI_196X_Perch_flk.021 (021)</ta>
            <ta e="T143" id="Seg_1276" s="T142">KAI_196X_Perch_flk.022 (022)</ta>
            <ta e="T147" id="Seg_1277" s="T144">KAI_196X_Perch_flk.023 (023)</ta>
            <ta e="T155" id="Seg_1278" s="T148">KAI_196X_Perch_flk.024 (024)</ta>
            <ta e="T165" id="Seg_1279" s="T156">KAI_196X_Perch_flk.025 (025)</ta>
            <ta e="T167" id="Seg_1280" s="T166">KAI_196X_Perch_flk.026 (026)</ta>
            <ta e="T170" id="Seg_1281" s="T168">KAI_196X_Perch_flk.027 (027)</ta>
            <ta e="T188" id="Seg_1282" s="T171">KAI_196X_Perch_flk.028 (028)</ta>
            <ta e="T205" id="Seg_1283" s="T189">KAI_196X_Perch_flk.029 (029)</ta>
            <ta e="T226" id="Seg_1284" s="T206">KAI_196X_Perch_flk.030 (030)</ta>
            <ta e="T232" id="Seg_1285" s="T227">KAI_196X_Perch_flk.031 (031)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T8" id="Seg_1286" s="T1">Сеплак ӄорала ӄэнта щӧты, ӄораӈа , ӄораӈа э-эй.</ta>
            <ta e="T20" id="Seg_1287" s="T8">Укырчонтот, ӄорала поӄыта, ӄай поӄытыла, ӄай ут щеляс ӄэна, ӄөса ӄэнынт.</ta>
            <ta e="T24" id="Seg_1288" s="T21">Мээрӄы ӄоса ӄэтынт.</ta>
            <ta e="T27" id="Seg_1289" s="T25">Ӄөна танта, ӄөна танта.</ta>
            <ta e="T47" id="Seg_1290" s="T28">Ӄөна танта, ӄөт мыта ӄөты (…08:48?) тенымыт ӄөт ӄөсам кен мынт човсын и чтоп тенымыкт Лозы тенымыкт.</ta>
            <ta e="T51" id="Seg_1291" s="T48">Ӄөсат ыла ӄэчот.</ta>
            <ta e="T57" id="Seg_1292" s="T52">Ӄөсат ыла пирымпыла омта Ича-ичыка.</ta>
            <ta e="T62" id="Seg_1293" s="T58">Ӄот омтыла, Лозы ӄөнщент.</ta>
            <ta e="T67" id="Seg_1294" s="T63">Лозы ӄөнтщент, Лозы тӱнты.</ta>
            <ta e="T72" id="Seg_1295" s="T68">Там ияя ӄай метынты?</ta>
            <ta e="T74" id="Seg_1296" s="T73">Ӄай?</ta>
            <ta e="T81" id="Seg_1297" s="T75">Ӄай метынтак, мыта апсам тап пирымынтак.</ta>
            <ta e="T85" id="Seg_1298" s="T82">На ӄаіль апсал?</ta>
            <ta e="T90" id="Seg_1299" s="T86">Ашщ ман на мытылям.</ta>
            <ta e="T93" id="Seg_1300" s="T91">Намал нюнча.</ta>
            <ta e="T100" id="Seg_1301" s="T94">Тан момпа кай нильчи апсал пирынтал.</ta>
            <ta e="T109" id="Seg_1302" s="T101">Ныны наманты, это инчиресно, кэтылап тап что ли.</ta>
            <ta e="T114" id="Seg_1303" s="T110">На ман ашща ӄоптысаӈ.</ta>
            <ta e="T123" id="Seg_1304" s="T115">Онак щаӈ ӄоптысаӈ момпа, намам-сам пирыӈпам.</ta>
            <ta e="T126" id="Seg_1305" s="T124">Ӄай, кутар (…09:40-09:42?)</ta>
            <ta e="T141" id="Seg_1306" s="T127">тэм на (колямты? 09:43) ашща ӄопты есы, намал пирят, наманта нюньік еӈа.</ta>
            <ta e="T143" id="Seg_1307" s="T142">Тал.</ta>
            <ta e="T147" id="Seg_1308" s="T144">Ман ашща минтак.</ta>
            <ta e="T155" id="Seg_1309" s="T148">Ильча ниль орыня: ман ашщ, ашщ минтак.</ta>
            <ta e="T165" id="Seg_1310" s="T156">Тан ей мы(…09:53?) ман момпа Лоизыираль ӄуп.</ta>
            <ta e="T170" id="Seg_1311" s="T168"> Лозыираль куп.</ta>
            <ta e="T188" id="Seg_1312" s="T171">Ну латно, тан ашща куп, тан момпа мат щинты ӄурылак момпа тап мӱӄаль мо то.</ta>
            <ta e="T205" id="Seg_1313" s="T189">Ныны тина мӱкат нӓӄала мӱка, мӱкат момты мәӄылныт, мәӄылныт тина Лозы ирамты нӓінты понты ӄураиӈыты.</ta>
            <ta e="T226" id="Seg_1314" s="T206">Лозы ираты понты ӄураіӈыты, тина Лозы ираты нӓінты понты ӄураіла Лозы ира ӄотта, Лозы ирап пярӄыла мыла Лозы ирап ыла ӄэныт.</ta>
            <ta e="T232" id="Seg_1315" s="T227">Лозы Ира пярӄала ыла ӄэтая.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T8" id="Seg_1316" s="T1">Seplʼak qoralʼa qənta šötɨ, qoraŋa , qoraŋa ə-əj.</ta>
            <ta e="T20" id="Seg_1317" s="T8">Ukɨrčʼontot, qoralʼa poqɨta, qaj poqɨtɨlʼa, qaj ut šelʼas qəna, qɔːsa qənɨnt.</ta>
            <ta e="T24" id="Seg_1318" s="T21">Məərqɨ qosa qətɨnt.</ta>
            <ta e="T27" id="Seg_1319" s="T25">Qɔːna tanta, qɔːna tanta.</ta>
            <ta e="T47" id="Seg_1320" s="T28">Qɔːna tanta, qɔːt mɨta qɔːtɨ (…08:48?) tenɨmɨt qɔːt qɔːsam ken mɨnt čʼowsɨn i čʼtop tenɨmɨkt Lʼozɨ tenɨmɨkt.</ta>
            <ta e="T51" id="Seg_1321" s="T48">Qɔːsat ɨlʼa qəčʼot.</ta>
            <ta e="T57" id="Seg_1322" s="T52">Qɔːsat ɨlʼa pirɨmpɨlʼa omta Ičʼa-ičʼɨka.</ta>
            <ta e="T62" id="Seg_1323" s="T58">Qot omtɨlʼa, Lʼozɨ qɔːnšent.</ta>
            <ta e="T67" id="Seg_1324" s="T63">Lʼozɨ qɔːntšent, Lʼozɨ tüntɨ.</ta>
            <ta e="T72" id="Seg_1325" s="T68">Tam ijaʼa qaj metɨntɨ?</ta>
            <ta e="T74" id="Seg_1326" s="T73">Qaj?</ta>
            <ta e="T81" id="Seg_1327" s="T75">Qaj metɨntak, mɨta apsam tap pirɨmɨntak.</ta>
            <ta e="T85" id="Seg_1328" s="T82">Na qaіlʼ apsal?</ta>
            <ta e="T90" id="Seg_1329" s="T86">Ašš man na mɨtɨlʼam.</ta>
            <ta e="T93" id="Seg_1330" s="T91">Namalʼ nюnčʼa.</ta>
            <ta e="T100" id="Seg_1331" s="T94">Tan mompa kaj nilʼčʼi apsal pirɨntal.</ta>
            <ta e="T109" id="Seg_1332" s="T101">Nɨnɨ namantɨ, əto inčʼiresno, kətɨlʼap tap čʼto lʼi.</ta>
            <ta e="T114" id="Seg_1333" s="T110">Na man ašša qoptɨsaŋ.</ta>
            <ta e="T123" id="Seg_1334" s="T115">Onak šaŋ qoptɨsaŋ mompa, namam-sam pirɨŋpam.</ta>
            <ta e="T126" id="Seg_1335" s="T124">Qaj, kutar (…09:40-09:42?)</ta>
            <ta e="T141" id="Seg_1336" s="T127">təm na (kolʼamtɨ? 09:43) ašša qoptɨ esɨ, namal pirʼat, namanta nʼünʼіk eŋa.</ta>
            <ta e="T143" id="Seg_1337" s="T142">Tal.</ta>
            <ta e="T147" id="Seg_1338" s="T144">Man ašša mintak.</ta>
            <ta e="T155" id="Seg_1339" s="T148">Ilʼčʼa nilʼ orɨnʼa: man ašš, ašš mintak.</ta>
            <ta e="T165" id="Seg_1340" s="T156">Tan ej mɨ(…09:53?) man mompa Lʼoizɨiralʼ qup.</ta>
            <ta e="T170" id="Seg_1341" s="T168">Lʼozɨiralʼ qup.</ta>
            <ta e="T188" id="Seg_1342" s="T171">Nu latno, tan ašša qup, tan mompa mat šintɨ qurɨlak mompa tap müqalʼ mo to.</ta>
            <ta e="T205" id="Seg_1343" s="T189">Nɨnɨ tina mükat näqala müka, mükat momtɨ mәqɨlnɨt, mәqɨlnɨt tina Lozɨ iramtɨ näіntɨ pontɨ quraiŋɨtɨ.</ta>
            <ta e="T226" id="Seg_1344" s="T206">Lozɨ iratɨ pontɨ quraіŋɨtɨ, tina Lozɨ iratɨ näіntɨ pontɨ quraіla Lʼozɨ ira qotta, Lozɨ irap pʼarqɨla mɨla Lozɨ irap ɨla qənɨt.</ta>
            <ta e="T232" id="Seg_1345" s="T227">Lozɨ Ira pʼarqala ɨla qətaja.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T8" id="Seg_1346" s="T1">Seːpɨlʼak qoralʼa qənta šöttɨ, qoraŋa, qoraŋa nəəj. </ta>
            <ta e="T20" id="Seg_1347" s="T8">Ukkɨr čʼontot, qorala poqqɨta, qaj poqqɨtɨlʼa, (qaj-) qaj utšelʼas qənna, qɔːsa qənnɨnt. </ta>
            <ta e="T24" id="Seg_1348" s="T21">Mərqɨ qɔːsa qətɨnt. </ta>
            <ta e="T27" id="Seg_1349" s="T25">Konna tanta. </ta>
            <ta e="T47" id="Seg_1350" s="T28">Konna tanta, qɔːt mɨta, qɔːtɨ ((…)) tɛnɨmɨt qɔːt, qɔːt qɔːsam ken mɨnt čʼɔːwsɨn, i štop tɛnɨmɨqt ((…)) loːsɨ tɛnɨmɨqt. </ta>
            <ta e="T51" id="Seg_1351" s="T48">Qɔːsat ılla qəčʼot. </ta>
            <ta e="T57" id="Seg_1352" s="T52">Qɔːsat ılla pirɨmpɨlʼa ɔːmta İčʼaičʼɨka. </ta>
            <ta e="T62" id="Seg_1353" s="T58">Qɔːt ɔːmtɨla, loːsɨ qɔːntšɛːnt. </ta>
            <ta e="T67" id="Seg_1354" s="T63">Loːsɨ qɔːntšɛːnt, loːsɨ tüntɨ. </ta>
            <ta e="T72" id="Seg_1355" s="T68">“Tam ijaja qaj meːtɨntɨ? </ta>
            <ta e="T74" id="Seg_1356" s="T73">Qaj?” </ta>
            <ta e="T81" id="Seg_1357" s="T75">“Qaj meːtɨntak, mɨta apsam tap pirɨmɨntak.” </ta>
            <ta e="T85" id="Seg_1358" s="T82">“Na qajɨlʼ apsal?” </ta>
            <ta e="T90" id="Seg_1359" s="T86">“Ašʼ man na mɨtɨlʼam. </ta>
            <ta e="T93" id="Seg_1360" s="T91">Namalʼ nʼüːnʼčʼa.” </ta>
            <ta e="T100" id="Seg_1361" s="T94">“Tan mompa qaj nilʼčʼi apsal (pirɨntal)?” </ta>
            <ta e="T109" id="Seg_1362" s="T101">Nɨnɨ namantɨ, это интересно, kətɨlʼap tap što lʼi. </ta>
            <ta e="T114" id="Seg_1363" s="T110">“Na man aššʼa qoptɨksaŋ. </ta>
            <ta e="T123" id="Seg_1364" s="T115">Onak šaŋ qoptɨksaŋ mompa, (nam-) namap (sam pirɨmpam).” </ta>
            <ta e="T126" id="Seg_1365" s="T124">“Qaj, kutar((…))?” </ta>
            <ta e="T141" id="Seg_1366" s="T127">Təm na (kolʼamtɨ) ašša qɔːptɨ ɛːsɨ, ((…)) namalʼ pirät ((…)), ((…)), namantɨ nʼüːnʼɨk (ɛŋa?) ((…)). </ta>
            <ta e="T143" id="Seg_1367" s="T142">Tal. </ta>
            <ta e="T147" id="Seg_1368" s="T144">Man aššʼ mintak. </ta>
            <ta e="T155" id="Seg_1369" s="T148">Ilʼčʼa nılʼ orɨnʼnʼa: “Man aššʼ, aššʼ mintak. </ta>
            <ta e="T165" id="Seg_1370" s="T156">Tan äj mɨ ((…)) man mompa (loːsɨ iralʼ) qup. </ta>
            <ta e="T167" id="Seg_1371" s="T166">Tal. </ta>
            <ta e="T170" id="Seg_1372" s="T168">Loːsɨ iralʼ.” </ta>
            <ta e="T188" id="Seg_1373" s="T171">“Nu latno, tan mompa ašša qup, tan mompa mat šintɨ qurɨlak mompa tap mükälʼ ((…)) moː (to).” </ta>
            <ta e="T205" id="Seg_1374" s="T189">Nɨnɨ tına mükat näkälä mükat, mükat (moːmtɨ) meːqɨlnɨt meːqɨlnɨt tına loːsɨ iramtɨ näіntɨ (m-) pontɨ kuraiŋɨtɨ. </ta>
            <ta e="T226" id="Seg_1375" s="T206">Loːsɨ iratɨ pontɨ kuraіŋɨtɨ, tına loːsɨ iratɨ näіntɨ pontɨ (kur-) kuraіla loːsɨ ira qotta, loːsɨ irap pärqɨllä mɨlä ılla qənnɨt. </ta>
            <ta e="T232" id="Seg_1376" s="T227">Loːsɨ ira pärqɨllä ılla qətaja. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_1377" s="T1">seːpɨlʼak</ta>
            <ta e="T3" id="Seg_1378" s="T2">qora-lʼa</ta>
            <ta e="T4" id="Seg_1379" s="T3">qən-ta</ta>
            <ta e="T5" id="Seg_1380" s="T4">šöt-tɨ</ta>
            <ta e="T6" id="Seg_1381" s="T5">qora-ŋa</ta>
            <ta e="T7" id="Seg_1382" s="T6">qora-ŋa</ta>
            <ta e="T8" id="Seg_1383" s="T7">nəəj</ta>
            <ta e="T9" id="Seg_1384" s="T8">ukkɨr</ta>
            <ta e="T10" id="Seg_1385" s="T9">čʼonto-t</ta>
            <ta e="T11" id="Seg_1386" s="T10">qora-la</ta>
            <ta e="T12" id="Seg_1387" s="T11">poqqɨ-ta</ta>
            <ta e="T13" id="Seg_1388" s="T12">qaj</ta>
            <ta e="T14" id="Seg_1389" s="T13">poqqɨ-tɨ-lʼa</ta>
            <ta e="T16" id="Seg_1390" s="T15">qaj</ta>
            <ta e="T17" id="Seg_1391" s="T16">utše-lʼa-s</ta>
            <ta e="T18" id="Seg_1392" s="T17">qən-na</ta>
            <ta e="T19" id="Seg_1393" s="T18">qɔːsa</ta>
            <ta e="T20" id="Seg_1394" s="T19">qən-nɨ-nt</ta>
            <ta e="T22" id="Seg_1395" s="T21">mərqɨ</ta>
            <ta e="T23" id="Seg_1396" s="T22">qɔːsa</ta>
            <ta e="T24" id="Seg_1397" s="T23">qət-ɨ-nt</ta>
            <ta e="T26" id="Seg_1398" s="T25">konna</ta>
            <ta e="T27" id="Seg_1399" s="T26">tanta</ta>
            <ta e="T29" id="Seg_1400" s="T28">konna</ta>
            <ta e="T30" id="Seg_1401" s="T29">tanta</ta>
            <ta e="T31" id="Seg_1402" s="T30">qɔːt</ta>
            <ta e="T32" id="Seg_1403" s="T31">mɨta</ta>
            <ta e="T33" id="Seg_1404" s="T32">qɔːtɨ</ta>
            <ta e="T35" id="Seg_1405" s="T34">tɛnɨmɨ-t</ta>
            <ta e="T36" id="Seg_1406" s="T35">qɔːt</ta>
            <ta e="T37" id="Seg_1407" s="T36">qɔːt</ta>
            <ta e="T38" id="Seg_1408" s="T37">qɔːsa-m</ta>
            <ta e="T39" id="Seg_1409" s="T38">ken</ta>
            <ta e="T40" id="Seg_1410" s="T39">mɨnt</ta>
            <ta e="T41" id="Seg_1411" s="T40">čʼɔːwsɨ-n</ta>
            <ta e="T42" id="Seg_1412" s="T41">i</ta>
            <ta e="T43" id="Seg_1413" s="T42">štop</ta>
            <ta e="T44" id="Seg_1414" s="T43">tɛnɨmɨ-qt</ta>
            <ta e="T46" id="Seg_1415" s="T45">loːsɨ</ta>
            <ta e="T47" id="Seg_1416" s="T46">tɛnɨmɨ-qt</ta>
            <ta e="T49" id="Seg_1417" s="T48">qɔːsa-t</ta>
            <ta e="T50" id="Seg_1418" s="T49">ılla</ta>
            <ta e="T51" id="Seg_1419" s="T50">qəčʼo-t</ta>
            <ta e="T53" id="Seg_1420" s="T52">qɔːsa-t</ta>
            <ta e="T54" id="Seg_1421" s="T53">ılla</ta>
            <ta e="T55" id="Seg_1422" s="T54">pi-rɨ-mpɨ-lʼa</ta>
            <ta e="T56" id="Seg_1423" s="T55">ɔːmta</ta>
            <ta e="T57" id="Seg_1424" s="T56">İčʼaičʼɨka</ta>
            <ta e="T59" id="Seg_1425" s="T58">qɔːt</ta>
            <ta e="T60" id="Seg_1426" s="T59">ɔːmtɨ-la</ta>
            <ta e="T61" id="Seg_1427" s="T60">loːsɨ</ta>
            <ta e="T62" id="Seg_1428" s="T61">qɔːnt-š-ɛː-nt</ta>
            <ta e="T64" id="Seg_1429" s="T63">loːsɨ</ta>
            <ta e="T65" id="Seg_1430" s="T64">qɔːnt-š-ɛː-nt</ta>
            <ta e="T66" id="Seg_1431" s="T65">loːsɨ</ta>
            <ta e="T67" id="Seg_1432" s="T66">tü-ntɨ</ta>
            <ta e="T69" id="Seg_1433" s="T68">tam</ta>
            <ta e="T70" id="Seg_1434" s="T69">ija-ja</ta>
            <ta e="T71" id="Seg_1435" s="T70">qaj</ta>
            <ta e="T72" id="Seg_1436" s="T71">meː-tɨ-ntɨ</ta>
            <ta e="T74" id="Seg_1437" s="T73">qaj</ta>
            <ta e="T76" id="Seg_1438" s="T75">qaj</ta>
            <ta e="T77" id="Seg_1439" s="T76">meː-tɨ-nta-k</ta>
            <ta e="T78" id="Seg_1440" s="T77">mɨta</ta>
            <ta e="T79" id="Seg_1441" s="T78">apsa-m</ta>
            <ta e="T80" id="Seg_1442" s="T79">tap</ta>
            <ta e="T81" id="Seg_1443" s="T80">pi-rɨ-mɨ-nta-k</ta>
            <ta e="T83" id="Seg_1444" s="T82">Na</ta>
            <ta e="T84" id="Seg_1445" s="T83">qaj-ɨ-lʼ</ta>
            <ta e="T85" id="Seg_1446" s="T84">apsa-l</ta>
            <ta e="T87" id="Seg_1447" s="T86">ašʼ</ta>
            <ta e="T88" id="Seg_1448" s="T87">man</ta>
            <ta e="T89" id="Seg_1449" s="T88">na</ta>
            <ta e="T90" id="Seg_1450" s="T89">mɨtɨ-lʼa-m</ta>
            <ta e="T92" id="Seg_1451" s="T91">nam-a-lʼ</ta>
            <ta e="T93" id="Seg_1452" s="T92">nʼüːnʼ-čʼa</ta>
            <ta e="T95" id="Seg_1453" s="T94">Tan</ta>
            <ta e="T96" id="Seg_1454" s="T95">mompa</ta>
            <ta e="T97" id="Seg_1455" s="T96">qaj</ta>
            <ta e="T98" id="Seg_1456" s="T97">nilʼčʼi</ta>
            <ta e="T99" id="Seg_1457" s="T98">apsa-l</ta>
            <ta e="T100" id="Seg_1458" s="T99">pi-rɨ-nta-l</ta>
            <ta e="T102" id="Seg_1459" s="T101">nɨnɨ</ta>
            <ta e="T103" id="Seg_1460" s="T102">namantɨ</ta>
            <ta e="T106" id="Seg_1461" s="T105">kətɨ-lʼa-p</ta>
            <ta e="T107" id="Seg_1462" s="T106">tap</ta>
            <ta e="T108" id="Seg_1463" s="T107">što</ta>
            <ta e="T109" id="Seg_1464" s="T108">lʼi</ta>
            <ta e="T111" id="Seg_1465" s="T110">Na</ta>
            <ta e="T112" id="Seg_1466" s="T111">man</ta>
            <ta e="T113" id="Seg_1467" s="T112">aššʼa</ta>
            <ta e="T114" id="Seg_1468" s="T113">qoptɨk-sa-ŋ</ta>
            <ta e="T116" id="Seg_1469" s="T115">onak</ta>
            <ta e="T117" id="Seg_1470" s="T116">šaŋ</ta>
            <ta e="T118" id="Seg_1471" s="T117">qoptɨk-sa-ŋ</ta>
            <ta e="T119" id="Seg_1472" s="T118">mompa</ta>
            <ta e="T121" id="Seg_1473" s="T120">nam-a-p</ta>
            <ta e="T123" id="Seg_1474" s="T122">pi-rɨ-mpa-m</ta>
            <ta e="T125" id="Seg_1475" s="T124">qaj</ta>
            <ta e="T126" id="Seg_1476" s="T125">kutar</ta>
            <ta e="T128" id="Seg_1477" s="T127">təm</ta>
            <ta e="T129" id="Seg_1478" s="T128">na</ta>
            <ta e="T130" id="Seg_1479" s="T129">kolʼamtɨ</ta>
            <ta e="T131" id="Seg_1480" s="T130">ašša</ta>
            <ta e="T132" id="Seg_1481" s="T131">qɔːptɨ</ta>
            <ta e="T133" id="Seg_1482" s="T132">ɛː-sɨ</ta>
            <ta e="T135" id="Seg_1483" s="T134">nam-a-lʼ</ta>
            <ta e="T136" id="Seg_1484" s="T135">pi-r-ät</ta>
            <ta e="T138" id="Seg_1485" s="T137">namantɨ</ta>
            <ta e="T139" id="Seg_1486" s="T138">nʼüːnʼɨ-k</ta>
            <ta e="T140" id="Seg_1487" s="T139">ɛ-ŋa</ta>
            <ta e="T143" id="Seg_1488" s="T142">ta-l</ta>
            <ta e="T145" id="Seg_1489" s="T144">Man</ta>
            <ta e="T146" id="Seg_1490" s="T145">aššʼ</ta>
            <ta e="T147" id="Seg_1491" s="T146">mi-nta-k</ta>
            <ta e="T149" id="Seg_1492" s="T148">ilʼčʼa</ta>
            <ta e="T150" id="Seg_1493" s="T149">nı-lʼ</ta>
            <ta e="T151" id="Seg_1494" s="T150">orɨ-nʼ-nʼa</ta>
            <ta e="T152" id="Seg_1495" s="T151">Man</ta>
            <ta e="T153" id="Seg_1496" s="T152">aššʼ</ta>
            <ta e="T154" id="Seg_1497" s="T153">aššʼ</ta>
            <ta e="T155" id="Seg_1498" s="T154">mi-nta-k</ta>
            <ta e="T157" id="Seg_1499" s="T156">tat</ta>
            <ta e="T158" id="Seg_1500" s="T157">aj</ta>
            <ta e="T159" id="Seg_1501" s="T158">mɨ</ta>
            <ta e="T161" id="Seg_1502" s="T160">man</ta>
            <ta e="T162" id="Seg_1503" s="T161">mompa</ta>
            <ta e="T163" id="Seg_1504" s="T162">loːsɨ</ta>
            <ta e="T164" id="Seg_1505" s="T163">ira-lʼ</ta>
            <ta e="T165" id="Seg_1506" s="T164">qup</ta>
            <ta e="T167" id="Seg_1507" s="T166">ta-l</ta>
            <ta e="T169" id="Seg_1508" s="T168">loːsɨ</ta>
            <ta e="T170" id="Seg_1509" s="T169">ira-lʼ</ta>
            <ta e="T174" id="Seg_1510" s="T173">tan</ta>
            <ta e="T175" id="Seg_1511" s="T174">mompa</ta>
            <ta e="T176" id="Seg_1512" s="T175">ašša</ta>
            <ta e="T177" id="Seg_1513" s="T176">qup</ta>
            <ta e="T178" id="Seg_1514" s="T177">tan</ta>
            <ta e="T179" id="Seg_1515" s="T178">mompa</ta>
            <ta e="T180" id="Seg_1516" s="T179">mat</ta>
            <ta e="T181" id="Seg_1517" s="T180">šintɨ</ta>
            <ta e="T182" id="Seg_1518" s="T181">qurɨ-la-k</ta>
            <ta e="T183" id="Seg_1519" s="T182">mompa</ta>
            <ta e="T184" id="Seg_1520" s="T183">tap</ta>
            <ta e="T185" id="Seg_1521" s="T184">mükä-lʼ</ta>
            <ta e="T187" id="Seg_1522" s="T186">moː</ta>
            <ta e="T190" id="Seg_1523" s="T189">nɨnɨ</ta>
            <ta e="T191" id="Seg_1524" s="T190">tına</ta>
            <ta e="T192" id="Seg_1525" s="T191">müka-t</ta>
            <ta e="T193" id="Seg_1526" s="T192">näkä-lä</ta>
            <ta e="T194" id="Seg_1527" s="T193">müka-t</ta>
            <ta e="T195" id="Seg_1528" s="T194">müka-t</ta>
            <ta e="T196" id="Seg_1529" s="T195">moː-m-tɨ</ta>
            <ta e="T197" id="Seg_1530" s="T196">meː-qɨl-nɨ-t</ta>
            <ta e="T198" id="Seg_1531" s="T197">meː-qɨl-nɨ-t</ta>
            <ta e="T199" id="Seg_1532" s="T198">tına</ta>
            <ta e="T200" id="Seg_1533" s="T199">loːsɨ</ta>
            <ta e="T201" id="Seg_1534" s="T200">ira-m-tɨ</ta>
            <ta e="T202" id="Seg_1535" s="T201">näіntɨ</ta>
            <ta e="T204" id="Seg_1536" s="T203">po-ntɨ</ta>
            <ta e="T205" id="Seg_1537" s="T204">kur-ai-ŋɨ-tɨ</ta>
            <ta e="T207" id="Seg_1538" s="T206">loːsɨ</ta>
            <ta e="T208" id="Seg_1539" s="T207">ira-tɨ</ta>
            <ta e="T209" id="Seg_1540" s="T208">po-ntɨ</ta>
            <ta e="T210" id="Seg_1541" s="T209">kur-aі-ŋɨ-tɨ</ta>
            <ta e="T211" id="Seg_1542" s="T210">tına</ta>
            <ta e="T212" id="Seg_1543" s="T211">loːsɨ</ta>
            <ta e="T213" id="Seg_1544" s="T212">ira-tɨ</ta>
            <ta e="T214" id="Seg_1545" s="T213">näіntɨ</ta>
            <ta e="T215" id="Seg_1546" s="T214">po-ntɨ</ta>
            <ta e="T217" id="Seg_1547" s="T216">kur-aі-la</ta>
            <ta e="T218" id="Seg_1548" s="T217">loːsɨ</ta>
            <ta e="T219" id="Seg_1549" s="T218">ira</ta>
            <ta e="T220" id="Seg_1550" s="T219">qotta</ta>
            <ta e="T221" id="Seg_1551" s="T220">loːsɨ</ta>
            <ta e="T222" id="Seg_1552" s="T221">ira-p</ta>
            <ta e="T223" id="Seg_1553" s="T222">pärqɨl-lä</ta>
            <ta e="T224" id="Seg_1554" s="T223">mɨ-lä</ta>
            <ta e="T225" id="Seg_1555" s="T224">ılla</ta>
            <ta e="T226" id="Seg_1556" s="T225">qən-nɨ-t</ta>
            <ta e="T228" id="Seg_1557" s="T227">loːsɨ</ta>
            <ta e="T229" id="Seg_1558" s="T228">ira</ta>
            <ta e="T230" id="Seg_1559" s="T229">pärqɨl-lä</ta>
            <ta e="T231" id="Seg_1560" s="T230">ılla</ta>
            <ta e="T232" id="Seg_1561" s="T231">qət-a-ja</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_1562" s="T1">seːpɨlaŋ</ta>
            <ta e="T3" id="Seg_1563" s="T2">kora-lä</ta>
            <ta e="T4" id="Seg_1564" s="T3">qən-ntɨ</ta>
            <ta e="T5" id="Seg_1565" s="T4">šöt-ntɨ</ta>
            <ta e="T6" id="Seg_1566" s="T5">kora-ŋɨ</ta>
            <ta e="T7" id="Seg_1567" s="T6">kora-ŋɨ</ta>
            <ta e="T8" id="Seg_1568" s="T7">nəəj</ta>
            <ta e="T9" id="Seg_1569" s="T8">ukkɨr</ta>
            <ta e="T10" id="Seg_1570" s="T9">čʼontɨ-n</ta>
            <ta e="T11" id="Seg_1571" s="T10">kora-lä</ta>
            <ta e="T12" id="Seg_1572" s="T11">poqqɨ-tɨ</ta>
            <ta e="T13" id="Seg_1573" s="T12">qaj</ta>
            <ta e="T14" id="Seg_1574" s="T13">poqqɨ-tɨ-lä</ta>
            <ta e="T16" id="Seg_1575" s="T15">qaj</ta>
            <ta e="T17" id="Seg_1576" s="T16">utɨšeː-lʼa-sä</ta>
            <ta e="T18" id="Seg_1577" s="T17">qət-ŋɨ</ta>
            <ta e="T19" id="Seg_1578" s="T18">qɔːsa</ta>
            <ta e="T20" id="Seg_1579" s="T19">qət-ŋɨ-ntɨ</ta>
            <ta e="T22" id="Seg_1580" s="T21">wərqɨ</ta>
            <ta e="T23" id="Seg_1581" s="T22">qɔːsa</ta>
            <ta e="T24" id="Seg_1582" s="T23">qət-ɨ-ntɨ</ta>
            <ta e="T26" id="Seg_1583" s="T25">konnä</ta>
            <ta e="T27" id="Seg_1584" s="T26">tantɨ</ta>
            <ta e="T29" id="Seg_1585" s="T28">konnä</ta>
            <ta e="T30" id="Seg_1586" s="T29">tantɨ</ta>
            <ta e="T31" id="Seg_1587" s="T30">qɔːtɨ</ta>
            <ta e="T32" id="Seg_1588" s="T31">mɨta</ta>
            <ta e="T33" id="Seg_1589" s="T32">qɔːtɨ</ta>
            <ta e="T35" id="Seg_1590" s="T34">tɛnɨmɨ-tɨ</ta>
            <ta e="T36" id="Seg_1591" s="T35">qɔːtɨ</ta>
            <ta e="T37" id="Seg_1592" s="T36">qɔːtɨ</ta>
            <ta e="T38" id="Seg_1593" s="T37">qɔːsa-m</ta>
            <ta e="T39" id="Seg_1594" s="T38">ket</ta>
            <ta e="T40" id="Seg_1595" s="T39">mɨntɨ</ta>
            <ta e="T41" id="Seg_1596" s="T40">čʼɔːpsɨ-n</ta>
            <ta e="T42" id="Seg_1597" s="T41">i</ta>
            <ta e="T43" id="Seg_1598" s="T42">štobɨ</ta>
            <ta e="T44" id="Seg_1599" s="T43">tɛnɨmɨ-qɨntoːqo</ta>
            <ta e="T46" id="Seg_1600" s="T45">loːsɨ</ta>
            <ta e="T47" id="Seg_1601" s="T46">tɛnɨmɨ-qɨntoːqo</ta>
            <ta e="T49" id="Seg_1602" s="T48">qɔːsa-tɨ</ta>
            <ta e="T50" id="Seg_1603" s="T49">ıllä</ta>
            <ta e="T51" id="Seg_1604" s="T50">qəčʼo-tɨ</ta>
            <ta e="T53" id="Seg_1605" s="T52">qɔːsa-tɨ</ta>
            <ta e="T54" id="Seg_1606" s="T53">ıllä</ta>
            <ta e="T55" id="Seg_1607" s="T54">pi-rɨ-mpɨ-lä</ta>
            <ta e="T56" id="Seg_1608" s="T55">ɔːmtɨ</ta>
            <ta e="T57" id="Seg_1609" s="T56">İčʼakɨčʼɨka</ta>
            <ta e="T59" id="Seg_1610" s="T58">qɔːtɨ</ta>
            <ta e="T60" id="Seg_1611" s="T59">ɔːmtɨ-lä</ta>
            <ta e="T61" id="Seg_1612" s="T60">loːsɨ</ta>
            <ta e="T62" id="Seg_1613" s="T61">qontɨ-š-ɛː-ntɨ</ta>
            <ta e="T64" id="Seg_1614" s="T63">loːsɨ</ta>
            <ta e="T65" id="Seg_1615" s="T64">qontɨ-š-ɛː-ntɨ</ta>
            <ta e="T66" id="Seg_1616" s="T65">loːsɨ</ta>
            <ta e="T67" id="Seg_1617" s="T66">tü-ntɨ</ta>
            <ta e="T69" id="Seg_1618" s="T68">tam</ta>
            <ta e="T70" id="Seg_1619" s="T69">iːja-ja</ta>
            <ta e="T71" id="Seg_1620" s="T70">qaj</ta>
            <ta e="T72" id="Seg_1621" s="T71">meː-ntɨ-ntɨ</ta>
            <ta e="T74" id="Seg_1622" s="T73">qaj</ta>
            <ta e="T76" id="Seg_1623" s="T75">qaj</ta>
            <ta e="T77" id="Seg_1624" s="T76">meː-ntɨ-ntɨ-k</ta>
            <ta e="T78" id="Seg_1625" s="T77">mɨta</ta>
            <ta e="T79" id="Seg_1626" s="T78">apsɨ-m</ta>
            <ta e="T80" id="Seg_1627" s="T79">tam</ta>
            <ta e="T81" id="Seg_1628" s="T80">pi-rɨ-mpɨ-ntɨ-k</ta>
            <ta e="T83" id="Seg_1629" s="T82">na</ta>
            <ta e="T84" id="Seg_1630" s="T83">qaj-ɨ-lʼ</ta>
            <ta e="T85" id="Seg_1631" s="T84">apsɨ-lɨ</ta>
            <ta e="T87" id="Seg_1632" s="T86">ašša</ta>
            <ta e="T88" id="Seg_1633" s="T87">man</ta>
            <ta e="T89" id="Seg_1634" s="T88">na</ta>
            <ta e="T90" id="Seg_1635" s="T89">mɨtɨ-lʼa-m</ta>
            <ta e="T92" id="Seg_1636" s="T91">na-ɨ-lʼ</ta>
            <ta e="T93" id="Seg_1637" s="T92">nʼüːnʼɨ-čʼɨ</ta>
            <ta e="T95" id="Seg_1638" s="T94">tan</ta>
            <ta e="T96" id="Seg_1639" s="T95">mompa</ta>
            <ta e="T97" id="Seg_1640" s="T96">qaj</ta>
            <ta e="T98" id="Seg_1641" s="T97">nılʼčʼɨ</ta>
            <ta e="T99" id="Seg_1642" s="T98">apsɨ-lɨ</ta>
            <ta e="T100" id="Seg_1643" s="T99">pi-rɨ-ntɨ-l</ta>
            <ta e="T102" id="Seg_1644" s="T101">nɨːnɨ</ta>
            <ta e="T103" id="Seg_1645" s="T102">namantɨ</ta>
            <ta e="T106" id="Seg_1646" s="T105">kətɨ-lä-m</ta>
            <ta e="T107" id="Seg_1647" s="T106">tam</ta>
            <ta e="T108" id="Seg_1648" s="T107">što</ta>
            <ta e="T109" id="Seg_1649" s="T108">lʼi</ta>
            <ta e="T111" id="Seg_1650" s="T110">na</ta>
            <ta e="T112" id="Seg_1651" s="T111">man</ta>
            <ta e="T113" id="Seg_1652" s="T112">ašša</ta>
            <ta e="T114" id="Seg_1653" s="T113">qoptɨŋ-sɨ-k</ta>
            <ta e="T116" id="Seg_1654" s="T115">onäk</ta>
            <ta e="T117" id="Seg_1655" s="T116">šaŋ</ta>
            <ta e="T118" id="Seg_1656" s="T117">qoptɨŋ-sɨ-k</ta>
            <ta e="T119" id="Seg_1657" s="T118">mompa</ta>
            <ta e="T121" id="Seg_1658" s="T120">na-ɨ-m</ta>
            <ta e="T123" id="Seg_1659" s="T122">pi-rɨ-mpɨ-m</ta>
            <ta e="T125" id="Seg_1660" s="T124">qaj</ta>
            <ta e="T126" id="Seg_1661" s="T125">kuttar</ta>
            <ta e="T128" id="Seg_1662" s="T127">təp</ta>
            <ta e="T129" id="Seg_1663" s="T128">na</ta>
            <ta e="T130" id="Seg_1664" s="T129">kolʼamtɨ</ta>
            <ta e="T131" id="Seg_1665" s="T130">ašša</ta>
            <ta e="T132" id="Seg_1666" s="T131">qɔːptɨ</ta>
            <ta e="T133" id="Seg_1667" s="T132">ɛː-sɨ</ta>
            <ta e="T135" id="Seg_1668" s="T134">na-ɨ-lʼ</ta>
            <ta e="T136" id="Seg_1669" s="T135">pi-rɨ-ätɨ</ta>
            <ta e="T138" id="Seg_1670" s="T137">namantɨ</ta>
            <ta e="T139" id="Seg_1671" s="T138">nʼüːnʼɨ-k</ta>
            <ta e="T140" id="Seg_1672" s="T139">ɛː-ŋɨ</ta>
            <ta e="T143" id="Seg_1673" s="T142">ta-l</ta>
            <ta e="T145" id="Seg_1674" s="T144">man</ta>
            <ta e="T146" id="Seg_1675" s="T145">ašša</ta>
            <ta e="T147" id="Seg_1676" s="T146">mi-ɛntɨ-k</ta>
            <ta e="T149" id="Seg_1677" s="T148">ilʼčʼa</ta>
            <ta e="T150" id="Seg_1678" s="T149">nılʼčʼɨ-lʼ</ta>
            <ta e="T151" id="Seg_1679" s="T150">orɨ-š-ŋɨ</ta>
            <ta e="T152" id="Seg_1680" s="T151">man</ta>
            <ta e="T153" id="Seg_1681" s="T152">ašša</ta>
            <ta e="T154" id="Seg_1682" s="T153">ašša</ta>
            <ta e="T155" id="Seg_1683" s="T154">mi-ɛntɨ-k</ta>
            <ta e="T157" id="Seg_1684" s="T156">tan</ta>
            <ta e="T158" id="Seg_1685" s="T157">aj</ta>
            <ta e="T159" id="Seg_1686" s="T158">mɨ</ta>
            <ta e="T161" id="Seg_1687" s="T160">man</ta>
            <ta e="T162" id="Seg_1688" s="T161">mompa</ta>
            <ta e="T163" id="Seg_1689" s="T162">loːsɨ</ta>
            <ta e="T164" id="Seg_1690" s="T163">ira-lʼ</ta>
            <ta e="T165" id="Seg_1691" s="T164">qum</ta>
            <ta e="T167" id="Seg_1692" s="T166">ta-l</ta>
            <ta e="T169" id="Seg_1693" s="T168">loːsɨ</ta>
            <ta e="T170" id="Seg_1694" s="T169">ira-lʼ</ta>
            <ta e="T174" id="Seg_1695" s="T173">tan</ta>
            <ta e="T175" id="Seg_1696" s="T174">mompa</ta>
            <ta e="T176" id="Seg_1697" s="T175">ašša</ta>
            <ta e="T177" id="Seg_1698" s="T176">qum</ta>
            <ta e="T178" id="Seg_1699" s="T177">tan</ta>
            <ta e="T179" id="Seg_1700" s="T178">mompa</ta>
            <ta e="T180" id="Seg_1701" s="T179">man</ta>
            <ta e="T181" id="Seg_1702" s="T180">tašıntɨ</ta>
            <ta e="T182" id="Seg_1703" s="T181">kurɨ-lä-k</ta>
            <ta e="T183" id="Seg_1704" s="T182">mompa</ta>
            <ta e="T184" id="Seg_1705" s="T183">tam</ta>
            <ta e="T185" id="Seg_1706" s="T184">mükä-lʼ</ta>
            <ta e="T187" id="Seg_1707" s="T186">moː</ta>
            <ta e="T190" id="Seg_1708" s="T189">nɨːnɨ</ta>
            <ta e="T191" id="Seg_1709" s="T190">tına</ta>
            <ta e="T192" id="Seg_1710" s="T191">mükä-t</ta>
            <ta e="T193" id="Seg_1711" s="T192">näkä-lä</ta>
            <ta e="T194" id="Seg_1712" s="T193">mükä-t</ta>
            <ta e="T195" id="Seg_1713" s="T194">mükä-n</ta>
            <ta e="T196" id="Seg_1714" s="T195">moː-m-tɨ</ta>
            <ta e="T197" id="Seg_1715" s="T196">meː-qɨl-ŋɨ-tɨ</ta>
            <ta e="T198" id="Seg_1716" s="T197">meː-qɨl-ŋɨ-tɨ</ta>
            <ta e="T199" id="Seg_1717" s="T198">tına</ta>
            <ta e="T200" id="Seg_1718" s="T199">loːsɨ</ta>
            <ta e="T201" id="Seg_1719" s="T200">ira-m-tɨ</ta>
            <ta e="T202" id="Seg_1720" s="T201">najntɨ</ta>
            <ta e="T204" id="Seg_1721" s="T203">poː-ntɨ</ta>
            <ta e="T205" id="Seg_1722" s="T204">kurɨ-ɛː-ŋɨ-tɨ</ta>
            <ta e="T207" id="Seg_1723" s="T206">loːsɨ</ta>
            <ta e="T208" id="Seg_1724" s="T207">ira-tɨ</ta>
            <ta e="T209" id="Seg_1725" s="T208">poː-ntɨ</ta>
            <ta e="T210" id="Seg_1726" s="T209">kurɨ-ɛː-ŋɨ-tɨ</ta>
            <ta e="T211" id="Seg_1727" s="T210">tına</ta>
            <ta e="T212" id="Seg_1728" s="T211">loːsɨ</ta>
            <ta e="T213" id="Seg_1729" s="T212">ira-tɨ</ta>
            <ta e="T214" id="Seg_1730" s="T213">najntɨ</ta>
            <ta e="T215" id="Seg_1731" s="T214">poː-ntɨ</ta>
            <ta e="T217" id="Seg_1732" s="T216">kurɨ-ɛː-lä</ta>
            <ta e="T218" id="Seg_1733" s="T217">loːsɨ</ta>
            <ta e="T219" id="Seg_1734" s="T218">ira</ta>
            <ta e="T220" id="Seg_1735" s="T219">qottä</ta>
            <ta e="T221" id="Seg_1736" s="T220">loːsɨ</ta>
            <ta e="T222" id="Seg_1737" s="T221">ira-m</ta>
            <ta e="T223" id="Seg_1738" s="T222">pärqɨl-lä</ta>
            <ta e="T224" id="Seg_1739" s="T223">mɨ-lä</ta>
            <ta e="T225" id="Seg_1740" s="T224">ıllä</ta>
            <ta e="T226" id="Seg_1741" s="T225">qət-ŋɨ-tɨ</ta>
            <ta e="T228" id="Seg_1742" s="T227">loːsɨ</ta>
            <ta e="T229" id="Seg_1743" s="T228">ira</ta>
            <ta e="T230" id="Seg_1744" s="T229">pärqɨl-lä</ta>
            <ta e="T231" id="Seg_1745" s="T230">ıllä</ta>
            <ta e="T232" id="Seg_1746" s="T231">qət-ɨ-ŋɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_1747" s="T1">enough</ta>
            <ta e="T3" id="Seg_1748" s="T2">go.hunting-CVB</ta>
            <ta e="T4" id="Seg_1749" s="T3">go.away-INFER.[3SG.S]</ta>
            <ta e="T5" id="Seg_1750" s="T4">forest-ILL</ta>
            <ta e="T6" id="Seg_1751" s="T5">go.hunting-CO.[3SG.S]</ta>
            <ta e="T7" id="Seg_1752" s="T6">go.hunting-CO.[3SG.S]</ta>
            <ta e="T8" id="Seg_1753" s="T7">%%</ta>
            <ta e="T9" id="Seg_1754" s="T8">one</ta>
            <ta e="T10" id="Seg_1755" s="T9">middle-ADV.LOC</ta>
            <ta e="T11" id="Seg_1756" s="T10">go.hunting-CVB</ta>
            <ta e="T12" id="Seg_1757" s="T11">seine-TR.[3SG.S]</ta>
            <ta e="T13" id="Seg_1758" s="T12">whether</ta>
            <ta e="T14" id="Seg_1759" s="T13">seine-TR-CVB</ta>
            <ta e="T16" id="Seg_1760" s="T15">whether</ta>
            <ta e="T17" id="Seg_1761" s="T16">palm-DIM-INSTR</ta>
            <ta e="T18" id="Seg_1762" s="T17">kill-CO.[3SG.S]</ta>
            <ta e="T19" id="Seg_1763" s="T18">perch.[NOM]</ta>
            <ta e="T20" id="Seg_1764" s="T19">kill-CO-INFER.[3SG.S]</ta>
            <ta e="T22" id="Seg_1765" s="T21">big</ta>
            <ta e="T23" id="Seg_1766" s="T22">perch.[NOM]</ta>
            <ta e="T24" id="Seg_1767" s="T23">kill-EP-INFER.[3SG.S]</ta>
            <ta e="T26" id="Seg_1768" s="T25">upwards</ta>
            <ta e="T27" id="Seg_1769" s="T26">go.out.[3SG.S]</ta>
            <ta e="T29" id="Seg_1770" s="T28">upwards</ta>
            <ta e="T30" id="Seg_1771" s="T29">go.out.[3SG.S]</ta>
            <ta e="T31" id="Seg_1772" s="T30">probably</ta>
            <ta e="T32" id="Seg_1773" s="T31">as.if</ta>
            <ta e="T33" id="Seg_1774" s="T32">probably</ta>
            <ta e="T35" id="Seg_1775" s="T34">know-3SG.O</ta>
            <ta e="T36" id="Seg_1776" s="T35">probably</ta>
            <ta e="T37" id="Seg_1777" s="T36">probably</ta>
            <ta e="T38" id="Seg_1778" s="T37">perch-ACC</ta>
            <ta e="T39" id="Seg_1779" s="T38">after.all</ta>
            <ta e="T40" id="Seg_1780" s="T39">%%</ta>
            <ta e="T41" id="Seg_1781" s="T40">spit-ADV.LOC</ta>
            <ta e="T42" id="Seg_1782" s="T41">and</ta>
            <ta e="T43" id="Seg_1783" s="T42">štobɨ</ta>
            <ta e="T44" id="Seg_1784" s="T43">know-SUP.2/3SG</ta>
            <ta e="T46" id="Seg_1785" s="T45">devil.[NOM]</ta>
            <ta e="T47" id="Seg_1786" s="T46">know-SUP.2/3SG</ta>
            <ta e="T49" id="Seg_1787" s="T48">perch.[NOM]-3SG</ta>
            <ta e="T50" id="Seg_1788" s="T49">down</ta>
            <ta e="T51" id="Seg_1789" s="T50">%%-3SG.O</ta>
            <ta e="T53" id="Seg_1790" s="T52">perch.[NOM]-3SG</ta>
            <ta e="T54" id="Seg_1791" s="T53">down</ta>
            <ta e="T55" id="Seg_1792" s="T54">become.ready-CAUS-DUR-CVB</ta>
            <ta e="T56" id="Seg_1793" s="T55">sit.[3SG.S]</ta>
            <ta e="T57" id="Seg_1794" s="T56">Ichakichika.[NOM]</ta>
            <ta e="T59" id="Seg_1795" s="T58">probably</ta>
            <ta e="T60" id="Seg_1796" s="T59">sit-CVB</ta>
            <ta e="T61" id="Seg_1797" s="T60">devil.[NOM]</ta>
            <ta e="T62" id="Seg_1798" s="T61">appear-DRV-PFV-INFER.[3SG.S]</ta>
            <ta e="T64" id="Seg_1799" s="T63">devil.[NOM]</ta>
            <ta e="T65" id="Seg_1800" s="T64">appear-DRV-PFV-INFER.[3SG.S]</ta>
            <ta e="T66" id="Seg_1801" s="T65">devil.[NOM]</ta>
            <ta e="T67" id="Seg_1802" s="T66">come-INFER.[3SG.S]</ta>
            <ta e="T69" id="Seg_1803" s="T68">this</ta>
            <ta e="T70" id="Seg_1804" s="T69">guy-DIM.[NOM]</ta>
            <ta e="T71" id="Seg_1805" s="T70">what.[NOM]</ta>
            <ta e="T72" id="Seg_1806" s="T71">make-IPFV-INFER.[3SG.S]</ta>
            <ta e="T74" id="Seg_1807" s="T73">what.[NOM]</ta>
            <ta e="T76" id="Seg_1808" s="T75">what.[NOM]</ta>
            <ta e="T77" id="Seg_1809" s="T76">make-IPFV-INFER-1SG.S</ta>
            <ta e="T78" id="Seg_1810" s="T77">he.says</ta>
            <ta e="T79" id="Seg_1811" s="T78">food-ACC</ta>
            <ta e="T80" id="Seg_1812" s="T79">this</ta>
            <ta e="T81" id="Seg_1813" s="T80">become.ready-CAUS-DUR-INFER-1SG.S</ta>
            <ta e="T83" id="Seg_1814" s="T82">this</ta>
            <ta e="T84" id="Seg_1815" s="T83">what-EP-ADJZ</ta>
            <ta e="T85" id="Seg_1816" s="T84">food.[NOM]-2SG</ta>
            <ta e="T87" id="Seg_1817" s="T86">NEG</ta>
            <ta e="T88" id="Seg_1818" s="T87">I.NOM</ta>
            <ta e="T89" id="Seg_1819" s="T88">here</ta>
            <ta e="T90" id="Seg_1820" s="T89">liver-DIM-ACC</ta>
            <ta e="T92" id="Seg_1821" s="T91">this-EP-ADJZ</ta>
            <ta e="T93" id="Seg_1822" s="T92">be.tasty-DRV.[3SG.S]</ta>
            <ta e="T95" id="Seg_1823" s="T94">you.SG.NOM</ta>
            <ta e="T96" id="Seg_1824" s="T95">it.is.said</ta>
            <ta e="T97" id="Seg_1825" s="T96">what.[NOM]</ta>
            <ta e="T98" id="Seg_1826" s="T97">such</ta>
            <ta e="T99" id="Seg_1827" s="T98">food.[NOM]-2SG</ta>
            <ta e="T100" id="Seg_1828" s="T99">become.ready-CAUS-INFER-2SG.O</ta>
            <ta e="T102" id="Seg_1829" s="T101">then</ta>
            <ta e="T103" id="Seg_1830" s="T102">so</ta>
            <ta e="T106" id="Seg_1831" s="T105">say-OPT-1SG.O</ta>
            <ta e="T107" id="Seg_1832" s="T106">this</ta>
            <ta e="T108" id="Seg_1833" s="T107">that</ta>
            <ta e="T109" id="Seg_1834" s="T108">whether</ta>
            <ta e="T111" id="Seg_1835" s="T110">this</ta>
            <ta e="T112" id="Seg_1836" s="T111">I.NOM</ta>
            <ta e="T113" id="Seg_1837" s="T112">NEG</ta>
            <ta e="T114" id="Seg_1838" s="T113">castrate-PST-1SG.S</ta>
            <ta e="T116" id="Seg_1839" s="T115">oneself.1SG</ta>
            <ta e="T117" id="Seg_1840" s="T116">%%</ta>
            <ta e="T118" id="Seg_1841" s="T117">castrate-PST-1SG.S</ta>
            <ta e="T119" id="Seg_1842" s="T118">it.is.said</ta>
            <ta e="T121" id="Seg_1843" s="T120">this-EP-ACC</ta>
            <ta e="T123" id="Seg_1844" s="T122">become.ready-CAUS-DUR-1SG.O</ta>
            <ta e="T125" id="Seg_1845" s="T124">what.[NOM]</ta>
            <ta e="T126" id="Seg_1846" s="T125">how</ta>
            <ta e="T128" id="Seg_1847" s="T127">(s)he.[NOM]</ta>
            <ta e="T129" id="Seg_1848" s="T128">this</ta>
            <ta e="T130" id="Seg_1849" s="T129">%%</ta>
            <ta e="T131" id="Seg_1850" s="T130">NEG</ta>
            <ta e="T132" id="Seg_1851" s="T131">short</ta>
            <ta e="T133" id="Seg_1852" s="T132">be-PST.[3SG.S]</ta>
            <ta e="T135" id="Seg_1853" s="T134">this-EP-ADJZ</ta>
            <ta e="T136" id="Seg_1854" s="T135">become.ready-CAUS-IMP.2SG.O</ta>
            <ta e="T138" id="Seg_1855" s="T137">so</ta>
            <ta e="T139" id="Seg_1856" s="T138">be.tasty-ADVZ</ta>
            <ta e="T140" id="Seg_1857" s="T139">be-CO.[3SG.S]</ta>
            <ta e="T143" id="Seg_1858" s="T142">%%-2SG.O</ta>
            <ta e="T145" id="Seg_1859" s="T144">I.NOM</ta>
            <ta e="T146" id="Seg_1860" s="T145">NEG</ta>
            <ta e="T147" id="Seg_1861" s="T146">give-FUT-1SG.S</ta>
            <ta e="T149" id="Seg_1862" s="T148">grandfather.[NOM]</ta>
            <ta e="T150" id="Seg_1863" s="T149">such-ADJZ</ta>
            <ta e="T151" id="Seg_1864" s="T150">force-VBLZ-CO.[3SG.S]</ta>
            <ta e="T152" id="Seg_1865" s="T151">I.NOM</ta>
            <ta e="T153" id="Seg_1866" s="T152">NEG</ta>
            <ta e="T154" id="Seg_1867" s="T153">NEG</ta>
            <ta e="T155" id="Seg_1868" s="T154">give-FUT-1SG.S</ta>
            <ta e="T157" id="Seg_1869" s="T156">you.SG.NOM</ta>
            <ta e="T158" id="Seg_1870" s="T157">again</ta>
            <ta e="T159" id="Seg_1871" s="T158">something.[NOM]</ta>
            <ta e="T161" id="Seg_1872" s="T160">I.NOM</ta>
            <ta e="T162" id="Seg_1873" s="T161">it.is.said</ta>
            <ta e="T163" id="Seg_1874" s="T162">devil.[NOM]</ta>
            <ta e="T164" id="Seg_1875" s="T163">old.man-ADJZ</ta>
            <ta e="T165" id="Seg_1876" s="T164">human.being.[NOM]</ta>
            <ta e="T167" id="Seg_1877" s="T166">%%-2SG.O</ta>
            <ta e="T169" id="Seg_1878" s="T168">devil.[NOM]</ta>
            <ta e="T170" id="Seg_1879" s="T169">old.man-ADJZ</ta>
            <ta e="T174" id="Seg_1880" s="T173">you.SG.NOM</ta>
            <ta e="T175" id="Seg_1881" s="T174">it.is.said</ta>
            <ta e="T176" id="Seg_1882" s="T175">NEG</ta>
            <ta e="T177" id="Seg_1883" s="T176">human.being.[NOM]</ta>
            <ta e="T178" id="Seg_1884" s="T177">you.SG.NOM</ta>
            <ta e="T179" id="Seg_1885" s="T178">it.is.said</ta>
            <ta e="T180" id="Seg_1886" s="T179">I.NOM</ta>
            <ta e="T181" id="Seg_1887" s="T180">you.SG.ACC</ta>
            <ta e="T182" id="Seg_1888" s="T181">wind.round-OPT-1SG.S</ta>
            <ta e="T183" id="Seg_1889" s="T182">it.is.said</ta>
            <ta e="T184" id="Seg_1890" s="T183">this</ta>
            <ta e="T185" id="Seg_1891" s="T184">bird.cherry.tree-ADJZ</ta>
            <ta e="T187" id="Seg_1892" s="T186">branch.[NOM]</ta>
            <ta e="T190" id="Seg_1893" s="T189">then</ta>
            <ta e="T191" id="Seg_1894" s="T190">that</ta>
            <ta e="T192" id="Seg_1895" s="T191">bird.cherry.tree-PL.[NOM]</ta>
            <ta e="T193" id="Seg_1896" s="T192">pull-CVB</ta>
            <ta e="T194" id="Seg_1897" s="T193">bird.cherry.tree-PL.[NOM]</ta>
            <ta e="T195" id="Seg_1898" s="T194">bird.cherry.tree-GEN</ta>
            <ta e="T196" id="Seg_1899" s="T195">branch-ACC-3SG</ta>
            <ta e="T197" id="Seg_1900" s="T196">make-MULO-CO-3SG.O</ta>
            <ta e="T198" id="Seg_1901" s="T197">make-MULO-CO-3SG.O</ta>
            <ta e="T199" id="Seg_1902" s="T198">that</ta>
            <ta e="T200" id="Seg_1903" s="T199">devil.[NOM]</ta>
            <ta e="T201" id="Seg_1904" s="T200">old.man-ACC-3SG</ta>
            <ta e="T202" id="Seg_1905" s="T201">here.it.is.[3SG.S]</ta>
            <ta e="T204" id="Seg_1906" s="T203">tree-ILL</ta>
            <ta e="T205" id="Seg_1907" s="T204">wind.round-PFV-CO-3SG.O</ta>
            <ta e="T207" id="Seg_1908" s="T206">devil.[NOM]</ta>
            <ta e="T208" id="Seg_1909" s="T207">old.man.[NOM]-3SG</ta>
            <ta e="T209" id="Seg_1910" s="T208">tree-ILL</ta>
            <ta e="T210" id="Seg_1911" s="T209">wind.round-PFV-CO-3SG.O</ta>
            <ta e="T211" id="Seg_1912" s="T210">that</ta>
            <ta e="T212" id="Seg_1913" s="T211">devil.[NOM]</ta>
            <ta e="T213" id="Seg_1914" s="T212">old.man.[NOM]-3SG</ta>
            <ta e="T214" id="Seg_1915" s="T213">here.it.is.[3SG.S]</ta>
            <ta e="T215" id="Seg_1916" s="T214">tree-ILL</ta>
            <ta e="T217" id="Seg_1917" s="T216">wind.round-PFV-CVB</ta>
            <ta e="T218" id="Seg_1918" s="T217">devil.[NOM]</ta>
            <ta e="T219" id="Seg_1919" s="T218">old.man.[NOM]</ta>
            <ta e="T220" id="Seg_1920" s="T219">on.one_s.back</ta>
            <ta e="T221" id="Seg_1921" s="T220">devil.[NOM]</ta>
            <ta e="T222" id="Seg_1922" s="T221">old.man-ACC</ta>
            <ta e="T223" id="Seg_1923" s="T222">stab-CVB</ta>
            <ta e="T224" id="Seg_1924" s="T223">make.something-CVB</ta>
            <ta e="T225" id="Seg_1925" s="T224">down</ta>
            <ta e="T226" id="Seg_1926" s="T225">kill-CO-3SG.O</ta>
            <ta e="T228" id="Seg_1927" s="T227">devil.[NOM]</ta>
            <ta e="T229" id="Seg_1928" s="T228">old.man.[NOM]</ta>
            <ta e="T230" id="Seg_1929" s="T229">stab-CVB</ta>
            <ta e="T231" id="Seg_1930" s="T230">down</ta>
            <ta e="T232" id="Seg_1931" s="T231">kill-EP-CO.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_1932" s="T1">достаточно</ta>
            <ta e="T3" id="Seg_1933" s="T2">отправиться.на.охоту-CVB</ta>
            <ta e="T4" id="Seg_1934" s="T3">уйти-INFER.[3SG.S]</ta>
            <ta e="T5" id="Seg_1935" s="T4">лес-ILL</ta>
            <ta e="T6" id="Seg_1936" s="T5">отправиться.на.охоту-CO.[3SG.S]</ta>
            <ta e="T7" id="Seg_1937" s="T6">отправиться.на.охоту-CO.[3SG.S]</ta>
            <ta e="T8" id="Seg_1938" s="T7">%%</ta>
            <ta e="T9" id="Seg_1939" s="T8">один</ta>
            <ta e="T10" id="Seg_1940" s="T9">середина-ADV.LOC</ta>
            <ta e="T11" id="Seg_1941" s="T10">отправиться.на.охоту-CVB</ta>
            <ta e="T12" id="Seg_1942" s="T11">невод-TR.[3SG.S]</ta>
            <ta e="T13" id="Seg_1943" s="T12">что.ли</ta>
            <ta e="T14" id="Seg_1944" s="T13">невод-TR-CVB</ta>
            <ta e="T16" id="Seg_1945" s="T15">что.ли</ta>
            <ta e="T17" id="Seg_1946" s="T16">ладонь-DIM-INSTR</ta>
            <ta e="T18" id="Seg_1947" s="T17">убить-CO.[3SG.S]</ta>
            <ta e="T19" id="Seg_1948" s="T18">окунь.[NOM]</ta>
            <ta e="T20" id="Seg_1949" s="T19">убить-CO-INFER.[3SG.S]</ta>
            <ta e="T22" id="Seg_1950" s="T21">большой</ta>
            <ta e="T23" id="Seg_1951" s="T22">окунь.[NOM]</ta>
            <ta e="T24" id="Seg_1952" s="T23">убить-EP-INFER.[3SG.S]</ta>
            <ta e="T26" id="Seg_1953" s="T25">вверх</ta>
            <ta e="T27" id="Seg_1954" s="T26">выйти.[3SG.S]</ta>
            <ta e="T29" id="Seg_1955" s="T28">вверх</ta>
            <ta e="T30" id="Seg_1956" s="T29">выйти.[3SG.S]</ta>
            <ta e="T31" id="Seg_1957" s="T30">наверное</ta>
            <ta e="T32" id="Seg_1958" s="T31">будто</ta>
            <ta e="T33" id="Seg_1959" s="T32">наверное</ta>
            <ta e="T35" id="Seg_1960" s="T34">знать-3SG.O</ta>
            <ta e="T36" id="Seg_1961" s="T35">наверное</ta>
            <ta e="T37" id="Seg_1962" s="T36">наверное</ta>
            <ta e="T38" id="Seg_1963" s="T37">окунь-ACC</ta>
            <ta e="T39" id="Seg_1964" s="T38">ведь</ta>
            <ta e="T40" id="Seg_1965" s="T39">%%</ta>
            <ta e="T41" id="Seg_1966" s="T40">вертел-ADV.LOC</ta>
            <ta e="T42" id="Seg_1967" s="T41">и</ta>
            <ta e="T43" id="Seg_1968" s="T42">чтобы</ta>
            <ta e="T44" id="Seg_1969" s="T43">знать-SUP.2/3SG</ta>
            <ta e="T46" id="Seg_1970" s="T45">чёрт.[NOM]</ta>
            <ta e="T47" id="Seg_1971" s="T46">знать-SUP.2/3SG</ta>
            <ta e="T49" id="Seg_1972" s="T48">окунь.[NOM]-3SG</ta>
            <ta e="T50" id="Seg_1973" s="T49">вниз</ta>
            <ta e="T51" id="Seg_1974" s="T50">%%-3SG.O</ta>
            <ta e="T53" id="Seg_1975" s="T52">окунь.[NOM]-3SG</ta>
            <ta e="T54" id="Seg_1976" s="T53">вниз</ta>
            <ta e="T55" id="Seg_1977" s="T54">стать.готовым-CAUS-DUR-CVB</ta>
            <ta e="T56" id="Seg_1978" s="T55">сидеть.[3SG.S]</ta>
            <ta e="T57" id="Seg_1979" s="T56">Ичакичика.[NOM]</ta>
            <ta e="T59" id="Seg_1980" s="T58">наверное</ta>
            <ta e="T60" id="Seg_1981" s="T59">сидеть-CVB</ta>
            <ta e="T61" id="Seg_1982" s="T60">чёрт.[NOM]</ta>
            <ta e="T62" id="Seg_1983" s="T61">показаться-DRV-PFV-INFER.[3SG.S]</ta>
            <ta e="T64" id="Seg_1984" s="T63">чёрт.[NOM]</ta>
            <ta e="T65" id="Seg_1985" s="T64">показаться-DRV-PFV-INFER.[3SG.S]</ta>
            <ta e="T66" id="Seg_1986" s="T65">чёрт.[NOM]</ta>
            <ta e="T67" id="Seg_1987" s="T66">прийти-INFER.[3SG.S]</ta>
            <ta e="T69" id="Seg_1988" s="T68">этот</ta>
            <ta e="T70" id="Seg_1989" s="T69">парень-DIM.[NOM]</ta>
            <ta e="T71" id="Seg_1990" s="T70">что.[NOM]</ta>
            <ta e="T72" id="Seg_1991" s="T71">сделать-IPFV-INFER.[3SG.S]</ta>
            <ta e="T74" id="Seg_1992" s="T73">что.[NOM]</ta>
            <ta e="T76" id="Seg_1993" s="T75">что.[NOM]</ta>
            <ta e="T77" id="Seg_1994" s="T76">сделать-IPFV-INFER-1SG.S</ta>
            <ta e="T78" id="Seg_1995" s="T77">мол</ta>
            <ta e="T79" id="Seg_1996" s="T78">еда-ACC</ta>
            <ta e="T80" id="Seg_1997" s="T79">этот</ta>
            <ta e="T81" id="Seg_1998" s="T80">стать.готовым-CAUS-DUR-INFER-1SG.S</ta>
            <ta e="T83" id="Seg_1999" s="T82">этот</ta>
            <ta e="T84" id="Seg_2000" s="T83">что-EP-ADJZ</ta>
            <ta e="T85" id="Seg_2001" s="T84">еда.[NOM]-2SG</ta>
            <ta e="T87" id="Seg_2002" s="T86">NEG</ta>
            <ta e="T88" id="Seg_2003" s="T87">я.NOM</ta>
            <ta e="T89" id="Seg_2004" s="T88">вот</ta>
            <ta e="T90" id="Seg_2005" s="T89">печень-DIM-ACC</ta>
            <ta e="T92" id="Seg_2006" s="T91">этот-EP-ADJZ</ta>
            <ta e="T93" id="Seg_2007" s="T92">быть.вкусным-DRV.[3SG.S]</ta>
            <ta e="T95" id="Seg_2008" s="T94">ты.NOM</ta>
            <ta e="T96" id="Seg_2009" s="T95">мол</ta>
            <ta e="T97" id="Seg_2010" s="T96">что.[NOM]</ta>
            <ta e="T98" id="Seg_2011" s="T97">такой</ta>
            <ta e="T99" id="Seg_2012" s="T98">еда.[NOM]-2SG</ta>
            <ta e="T100" id="Seg_2013" s="T99">стать.готовым-CAUS-INFER-2SG.O</ta>
            <ta e="T102" id="Seg_2014" s="T101">потом</ta>
            <ta e="T103" id="Seg_2015" s="T102">так</ta>
            <ta e="T106" id="Seg_2016" s="T105">сказать-OPT-1SG.O</ta>
            <ta e="T107" id="Seg_2017" s="T106">этот</ta>
            <ta e="T108" id="Seg_2018" s="T107">что</ta>
            <ta e="T109" id="Seg_2019" s="T108">что.ли</ta>
            <ta e="T111" id="Seg_2020" s="T110">этот</ta>
            <ta e="T112" id="Seg_2021" s="T111">я.NOM</ta>
            <ta e="T113" id="Seg_2022" s="T112">NEG</ta>
            <ta e="T114" id="Seg_2023" s="T113">кастрировать-PST-1SG.S</ta>
            <ta e="T116" id="Seg_2024" s="T115">сам.1SG</ta>
            <ta e="T117" id="Seg_2025" s="T116">%%</ta>
            <ta e="T118" id="Seg_2026" s="T117">кастрировать-PST-1SG.S</ta>
            <ta e="T119" id="Seg_2027" s="T118">мол</ta>
            <ta e="T121" id="Seg_2028" s="T120">этот-EP-ACC</ta>
            <ta e="T123" id="Seg_2029" s="T122">стать.готовым-CAUS-DUR-1SG.O</ta>
            <ta e="T125" id="Seg_2030" s="T124">что.[NOM]</ta>
            <ta e="T126" id="Seg_2031" s="T125">как</ta>
            <ta e="T128" id="Seg_2032" s="T127">он(а).[NOM]</ta>
            <ta e="T129" id="Seg_2033" s="T128">этот</ta>
            <ta e="T130" id="Seg_2034" s="T129">%%</ta>
            <ta e="T131" id="Seg_2035" s="T130">NEG</ta>
            <ta e="T132" id="Seg_2036" s="T131">короткий</ta>
            <ta e="T133" id="Seg_2037" s="T132">быть-PST.[3SG.S]</ta>
            <ta e="T135" id="Seg_2038" s="T134">этот-EP-ADJZ</ta>
            <ta e="T136" id="Seg_2039" s="T135">стать.готовым-CAUS-IMP.2SG.O</ta>
            <ta e="T138" id="Seg_2040" s="T137">так</ta>
            <ta e="T139" id="Seg_2041" s="T138">быть.вкусным-ADVZ</ta>
            <ta e="T140" id="Seg_2042" s="T139">быть-CO.[3SG.S]</ta>
            <ta e="T143" id="Seg_2043" s="T142">%%-2SG.O</ta>
            <ta e="T145" id="Seg_2044" s="T144">я.NOM</ta>
            <ta e="T146" id="Seg_2045" s="T145">NEG</ta>
            <ta e="T147" id="Seg_2046" s="T146">дать-FUT-1SG.S</ta>
            <ta e="T149" id="Seg_2047" s="T148">дедушка.[NOM]</ta>
            <ta e="T150" id="Seg_2048" s="T149">такой-ADJZ</ta>
            <ta e="T151" id="Seg_2049" s="T150">сила-VBLZ-CO.[3SG.S]</ta>
            <ta e="T152" id="Seg_2050" s="T151">я.NOM</ta>
            <ta e="T153" id="Seg_2051" s="T152">NEG</ta>
            <ta e="T154" id="Seg_2052" s="T153">NEG</ta>
            <ta e="T155" id="Seg_2053" s="T154">дать-FUT-1SG.S</ta>
            <ta e="T157" id="Seg_2054" s="T156">ты.NOM</ta>
            <ta e="T158" id="Seg_2055" s="T157">опять</ta>
            <ta e="T159" id="Seg_2056" s="T158">нечто.[NOM]</ta>
            <ta e="T161" id="Seg_2057" s="T160">я.NOM</ta>
            <ta e="T162" id="Seg_2058" s="T161">мол</ta>
            <ta e="T163" id="Seg_2059" s="T162">чёрт.[NOM]</ta>
            <ta e="T164" id="Seg_2060" s="T163">старик-ADJZ</ta>
            <ta e="T165" id="Seg_2061" s="T164">человек.[NOM]</ta>
            <ta e="T167" id="Seg_2062" s="T166">%%-2SG.O</ta>
            <ta e="T169" id="Seg_2063" s="T168">чёрт.[NOM]</ta>
            <ta e="T170" id="Seg_2064" s="T169">старик-ADJZ</ta>
            <ta e="T174" id="Seg_2065" s="T173">ты.NOM</ta>
            <ta e="T175" id="Seg_2066" s="T174">мол</ta>
            <ta e="T176" id="Seg_2067" s="T175">NEG</ta>
            <ta e="T177" id="Seg_2068" s="T176">человек.[NOM]</ta>
            <ta e="T178" id="Seg_2069" s="T177">ты.NOM</ta>
            <ta e="T179" id="Seg_2070" s="T178">мол</ta>
            <ta e="T180" id="Seg_2071" s="T179">я.NOM</ta>
            <ta e="T181" id="Seg_2072" s="T180">ты.ACC</ta>
            <ta e="T182" id="Seg_2073" s="T181">замотать-OPT-1SG.S</ta>
            <ta e="T183" id="Seg_2074" s="T182">мол</ta>
            <ta e="T184" id="Seg_2075" s="T183">этот</ta>
            <ta e="T185" id="Seg_2076" s="T184">черемуха-ADJZ</ta>
            <ta e="T187" id="Seg_2077" s="T186">ветка.[NOM]</ta>
            <ta e="T190" id="Seg_2078" s="T189">потом</ta>
            <ta e="T191" id="Seg_2079" s="T190">тот</ta>
            <ta e="T192" id="Seg_2080" s="T191">черемуха-PL.[NOM]</ta>
            <ta e="T193" id="Seg_2081" s="T192">тянуть-CVB</ta>
            <ta e="T194" id="Seg_2082" s="T193">черемуха-PL.[NOM]</ta>
            <ta e="T195" id="Seg_2083" s="T194">черемуха-GEN</ta>
            <ta e="T196" id="Seg_2084" s="T195">ветка-ACC-3SG</ta>
            <ta e="T197" id="Seg_2085" s="T196">сделать-MULO-CO-3SG.O</ta>
            <ta e="T198" id="Seg_2086" s="T197">сделать-MULO-CO-3SG.O</ta>
            <ta e="T199" id="Seg_2087" s="T198">тот</ta>
            <ta e="T200" id="Seg_2088" s="T199">чёрт.[NOM]</ta>
            <ta e="T201" id="Seg_2089" s="T200">старик-ACC-3SG</ta>
            <ta e="T202" id="Seg_2090" s="T201">вот.он.есть.[3SG.S]</ta>
            <ta e="T204" id="Seg_2091" s="T203">дерево-ILL</ta>
            <ta e="T205" id="Seg_2092" s="T204">замотать-PFV-CO-3SG.O</ta>
            <ta e="T207" id="Seg_2093" s="T206">чёрт.[NOM]</ta>
            <ta e="T208" id="Seg_2094" s="T207">старик.[NOM]-3SG</ta>
            <ta e="T209" id="Seg_2095" s="T208">дерево-ILL</ta>
            <ta e="T210" id="Seg_2096" s="T209">замотать-PFV-CO-3SG.O</ta>
            <ta e="T211" id="Seg_2097" s="T210">тот</ta>
            <ta e="T212" id="Seg_2098" s="T211">чёрт.[NOM]</ta>
            <ta e="T213" id="Seg_2099" s="T212">старик.[NOM]-3SG</ta>
            <ta e="T214" id="Seg_2100" s="T213">вот.он.есть.[3SG.S]</ta>
            <ta e="T215" id="Seg_2101" s="T214">дерево-ILL</ta>
            <ta e="T217" id="Seg_2102" s="T216">замотать-PFV-CVB</ta>
            <ta e="T218" id="Seg_2103" s="T217">чёрт.[NOM]</ta>
            <ta e="T219" id="Seg_2104" s="T218">старик.[NOM]</ta>
            <ta e="T220" id="Seg_2105" s="T219">на.спину</ta>
            <ta e="T221" id="Seg_2106" s="T220">чёрт.[NOM]</ta>
            <ta e="T222" id="Seg_2107" s="T221">старик-ACC</ta>
            <ta e="T223" id="Seg_2108" s="T222">заколоть-CVB</ta>
            <ta e="T224" id="Seg_2109" s="T223">сделать.нечто-CVB</ta>
            <ta e="T225" id="Seg_2110" s="T224">вниз</ta>
            <ta e="T226" id="Seg_2111" s="T225">убить-CO-3SG.O</ta>
            <ta e="T228" id="Seg_2112" s="T227">чёрт.[NOM]</ta>
            <ta e="T229" id="Seg_2113" s="T228">старик.[NOM]</ta>
            <ta e="T230" id="Seg_2114" s="T229">заколоть-CVB</ta>
            <ta e="T231" id="Seg_2115" s="T230">вниз</ta>
            <ta e="T232" id="Seg_2116" s="T231">убить-EP-CO.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_2117" s="T1">adv</ta>
            <ta e="T3" id="Seg_2118" s="T2">v-v&gt;adv</ta>
            <ta e="T4" id="Seg_2119" s="T3">v-v:tense.mood.[v:pn]</ta>
            <ta e="T5" id="Seg_2120" s="T4">n-n:case</ta>
            <ta e="T6" id="Seg_2121" s="T5">v-v:ins.[v:pn]</ta>
            <ta e="T7" id="Seg_2122" s="T6">v-v:ins.[v:pn]</ta>
            <ta e="T8" id="Seg_2123" s="T7">%%</ta>
            <ta e="T9" id="Seg_2124" s="T8">num</ta>
            <ta e="T10" id="Seg_2125" s="T9">n-n&gt;adv</ta>
            <ta e="T11" id="Seg_2126" s="T10">v-v&gt;adv</ta>
            <ta e="T12" id="Seg_2127" s="T11">n-n&gt;v.[v:pn]</ta>
            <ta e="T13" id="Seg_2128" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_2129" s="T13">n-n&gt;v-v&gt;adv</ta>
            <ta e="T16" id="Seg_2130" s="T15">ptcl</ta>
            <ta e="T17" id="Seg_2131" s="T16">n-n&gt;n-n:case</ta>
            <ta e="T18" id="Seg_2132" s="T17">v-v:ins.[v:pn]</ta>
            <ta e="T19" id="Seg_2133" s="T18">n.[n:case]</ta>
            <ta e="T20" id="Seg_2134" s="T19">v-v:ins-v:tense.mood.[v:pn]</ta>
            <ta e="T22" id="Seg_2135" s="T21">adj</ta>
            <ta e="T23" id="Seg_2136" s="T22">n.[n:case]</ta>
            <ta e="T24" id="Seg_2137" s="T23">v-v:ins-v:tense.mood.[v:pn]</ta>
            <ta e="T26" id="Seg_2138" s="T25">adv</ta>
            <ta e="T27" id="Seg_2139" s="T26">v.[v:pn]</ta>
            <ta e="T29" id="Seg_2140" s="T28">adv</ta>
            <ta e="T30" id="Seg_2141" s="T29">v.[v:pn]</ta>
            <ta e="T31" id="Seg_2142" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_2143" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_2144" s="T32">ptcl</ta>
            <ta e="T35" id="Seg_2145" s="T34">v-v:pn</ta>
            <ta e="T36" id="Seg_2146" s="T35">ptcl</ta>
            <ta e="T37" id="Seg_2147" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_2148" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_2149" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_2150" s="T39">%%</ta>
            <ta e="T41" id="Seg_2151" s="T40">n-n&gt;adv</ta>
            <ta e="T42" id="Seg_2152" s="T41">conj</ta>
            <ta e="T43" id="Seg_2153" s="T42">conj</ta>
            <ta e="T44" id="Seg_2154" s="T43">v-v:inf.poss</ta>
            <ta e="T46" id="Seg_2155" s="T45">n.[n:case]</ta>
            <ta e="T47" id="Seg_2156" s="T46">v-v:inf.poss</ta>
            <ta e="T49" id="Seg_2157" s="T48">n.[n:case]-n:poss</ta>
            <ta e="T50" id="Seg_2158" s="T49">preverb</ta>
            <ta e="T51" id="Seg_2159" s="T50">v-v:pn</ta>
            <ta e="T53" id="Seg_2160" s="T52">n.[n:case]-n:poss</ta>
            <ta e="T54" id="Seg_2161" s="T53">preverb</ta>
            <ta e="T55" id="Seg_2162" s="T54">v-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T56" id="Seg_2163" s="T55">v.[v:pn]</ta>
            <ta e="T57" id="Seg_2164" s="T56">nprop.[n:case]</ta>
            <ta e="T59" id="Seg_2165" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_2166" s="T59">v-v&gt;adv</ta>
            <ta e="T61" id="Seg_2167" s="T60">n.[n:case]</ta>
            <ta e="T62" id="Seg_2168" s="T61">v-v&gt;v-v&gt;v-v:tense.mood.[v:pn]</ta>
            <ta e="T64" id="Seg_2169" s="T63">n.[n:case]</ta>
            <ta e="T65" id="Seg_2170" s="T64">v-v&gt;v-v&gt;v-v:tense.mood.[v:pn]</ta>
            <ta e="T66" id="Seg_2171" s="T65">n.[n:case]</ta>
            <ta e="T67" id="Seg_2172" s="T66">v-v:tense.mood.[v:pn]</ta>
            <ta e="T69" id="Seg_2173" s="T68">dem</ta>
            <ta e="T70" id="Seg_2174" s="T69">n-n&gt;n.[n:case]</ta>
            <ta e="T71" id="Seg_2175" s="T70">interrog.[n:case]</ta>
            <ta e="T72" id="Seg_2176" s="T71">v-v&gt;v-v:tense.mood.[v:pn]</ta>
            <ta e="T74" id="Seg_2177" s="T73">interrog.[n:case]</ta>
            <ta e="T76" id="Seg_2178" s="T75">interrog.[n:case]</ta>
            <ta e="T77" id="Seg_2179" s="T76">v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T78" id="Seg_2180" s="T77">ptcl</ta>
            <ta e="T79" id="Seg_2181" s="T78">n-n:case</ta>
            <ta e="T80" id="Seg_2182" s="T79">dem</ta>
            <ta e="T81" id="Seg_2183" s="T80">v-v&gt;v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T83" id="Seg_2184" s="T82">dem</ta>
            <ta e="T84" id="Seg_2185" s="T83">interrog-n:ins-n&gt;adj</ta>
            <ta e="T85" id="Seg_2186" s="T84">n.[n:case]-n:poss</ta>
            <ta e="T87" id="Seg_2187" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_2188" s="T87">pers</ta>
            <ta e="T89" id="Seg_2189" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_2190" s="T89">n-n&gt;n-n:case</ta>
            <ta e="T92" id="Seg_2191" s="T91">dem-n:ins-n&gt;adj</ta>
            <ta e="T93" id="Seg_2192" s="T92">v-v&gt;v.[v:pn]</ta>
            <ta e="T95" id="Seg_2193" s="T94">pers</ta>
            <ta e="T96" id="Seg_2194" s="T95">ptcl</ta>
            <ta e="T97" id="Seg_2195" s="T96">interrog.[n:case]</ta>
            <ta e="T98" id="Seg_2196" s="T97">dem</ta>
            <ta e="T99" id="Seg_2197" s="T98">n.[n:case]-n:poss</ta>
            <ta e="T100" id="Seg_2198" s="T99">v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T102" id="Seg_2199" s="T101">adv</ta>
            <ta e="T103" id="Seg_2200" s="T102">adv</ta>
            <ta e="T106" id="Seg_2201" s="T105">v-v:mood-v:pn</ta>
            <ta e="T107" id="Seg_2202" s="T106">dem</ta>
            <ta e="T108" id="Seg_2203" s="T107">conj</ta>
            <ta e="T109" id="Seg_2204" s="T108">ptcl</ta>
            <ta e="T111" id="Seg_2205" s="T110">dem</ta>
            <ta e="T112" id="Seg_2206" s="T111">pers</ta>
            <ta e="T113" id="Seg_2207" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_2208" s="T113">v-v:tense-v:pn</ta>
            <ta e="T116" id="Seg_2209" s="T115">emphpro</ta>
            <ta e="T117" id="Seg_2210" s="T116">%%</ta>
            <ta e="T118" id="Seg_2211" s="T117">v-v:tense-v:pn</ta>
            <ta e="T119" id="Seg_2212" s="T118">ptcl</ta>
            <ta e="T121" id="Seg_2213" s="T120">dem-n:ins-n:case</ta>
            <ta e="T123" id="Seg_2214" s="T122">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T125" id="Seg_2215" s="T124">interrog.[n:case]</ta>
            <ta e="T126" id="Seg_2216" s="T125">interrog</ta>
            <ta e="T128" id="Seg_2217" s="T127">pers.[n:case]</ta>
            <ta e="T129" id="Seg_2218" s="T128">dem</ta>
            <ta e="T130" id="Seg_2219" s="T129">%%</ta>
            <ta e="T131" id="Seg_2220" s="T130">ptcl</ta>
            <ta e="T132" id="Seg_2221" s="T131">adj</ta>
            <ta e="T133" id="Seg_2222" s="T132">v-v:tense.[v:pn]</ta>
            <ta e="T135" id="Seg_2223" s="T134">dem-n:ins-n&gt;adj</ta>
            <ta e="T136" id="Seg_2224" s="T135">v-v&gt;v-v:mood.pn</ta>
            <ta e="T138" id="Seg_2225" s="T137">adv</ta>
            <ta e="T139" id="Seg_2226" s="T138">v-v&gt;adv</ta>
            <ta e="T140" id="Seg_2227" s="T139">v-v:ins.[v:pn]</ta>
            <ta e="T143" id="Seg_2228" s="T142">%%-v:pn</ta>
            <ta e="T145" id="Seg_2229" s="T144">pers</ta>
            <ta e="T146" id="Seg_2230" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_2231" s="T146">v-v:tense-v:pn</ta>
            <ta e="T149" id="Seg_2232" s="T148">n.[n:case]</ta>
            <ta e="T150" id="Seg_2233" s="T149">dem-n&gt;adj</ta>
            <ta e="T151" id="Seg_2234" s="T150">n-n&gt;v-v:ins.[v:pn]</ta>
            <ta e="T152" id="Seg_2235" s="T151">pers</ta>
            <ta e="T153" id="Seg_2236" s="T152">ptcl</ta>
            <ta e="T154" id="Seg_2237" s="T153">ptcl</ta>
            <ta e="T155" id="Seg_2238" s="T154">v-v:tense-v:pn</ta>
            <ta e="T157" id="Seg_2239" s="T156">pers</ta>
            <ta e="T158" id="Seg_2240" s="T157">adv</ta>
            <ta e="T159" id="Seg_2241" s="T158">n.[n:case]</ta>
            <ta e="T161" id="Seg_2242" s="T160">pers</ta>
            <ta e="T162" id="Seg_2243" s="T161">ptcl</ta>
            <ta e="T163" id="Seg_2244" s="T162">n.[n:case]</ta>
            <ta e="T164" id="Seg_2245" s="T163">n-n&gt;adj</ta>
            <ta e="T165" id="Seg_2246" s="T164">n.[n:case]</ta>
            <ta e="T167" id="Seg_2247" s="T166">%%-v:pn</ta>
            <ta e="T169" id="Seg_2248" s="T168">n.[n:case]</ta>
            <ta e="T170" id="Seg_2249" s="T169">n-n&gt;adj</ta>
            <ta e="T174" id="Seg_2250" s="T173">pers</ta>
            <ta e="T175" id="Seg_2251" s="T174">ptcl</ta>
            <ta e="T176" id="Seg_2252" s="T175">ptcl</ta>
            <ta e="T177" id="Seg_2253" s="T176">n.[n:case]</ta>
            <ta e="T178" id="Seg_2254" s="T177">pers</ta>
            <ta e="T179" id="Seg_2255" s="T178">ptcl</ta>
            <ta e="T180" id="Seg_2256" s="T179">pers</ta>
            <ta e="T181" id="Seg_2257" s="T180">pers</ta>
            <ta e="T182" id="Seg_2258" s="T181">v-v:mood-v:pn</ta>
            <ta e="T183" id="Seg_2259" s="T182">ptcl</ta>
            <ta e="T184" id="Seg_2260" s="T183">dem</ta>
            <ta e="T185" id="Seg_2261" s="T184">n-n&gt;adj</ta>
            <ta e="T187" id="Seg_2262" s="T186">n.[n:case]</ta>
            <ta e="T190" id="Seg_2263" s="T189">adv</ta>
            <ta e="T191" id="Seg_2264" s="T190">dem</ta>
            <ta e="T192" id="Seg_2265" s="T191">n-n:num.[n:case]</ta>
            <ta e="T193" id="Seg_2266" s="T192">v-v&gt;adv</ta>
            <ta e="T194" id="Seg_2267" s="T193">n-n:num.[n:case]</ta>
            <ta e="T195" id="Seg_2268" s="T194">n-n:case</ta>
            <ta e="T196" id="Seg_2269" s="T195">n-n:case-n:poss</ta>
            <ta e="T197" id="Seg_2270" s="T196">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T198" id="Seg_2271" s="T197">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T199" id="Seg_2272" s="T198">dem</ta>
            <ta e="T200" id="Seg_2273" s="T199">n.[n:case]</ta>
            <ta e="T201" id="Seg_2274" s="T200">n-n:case-n:poss</ta>
            <ta e="T202" id="Seg_2275" s="T201">v.[v:pn]</ta>
            <ta e="T204" id="Seg_2276" s="T203">n-n:case</ta>
            <ta e="T205" id="Seg_2277" s="T204">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T207" id="Seg_2278" s="T206">n.[n:case]</ta>
            <ta e="T208" id="Seg_2279" s="T207">n.[n:case]-n:poss</ta>
            <ta e="T209" id="Seg_2280" s="T208">n-n:case</ta>
            <ta e="T210" id="Seg_2281" s="T209">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T211" id="Seg_2282" s="T210">dem</ta>
            <ta e="T212" id="Seg_2283" s="T211">n.[n:case]</ta>
            <ta e="T213" id="Seg_2284" s="T212">n.[n:case]-n:poss</ta>
            <ta e="T214" id="Seg_2285" s="T213">v.[v:pn]</ta>
            <ta e="T215" id="Seg_2286" s="T214">n-n:case</ta>
            <ta e="T217" id="Seg_2287" s="T216">v-v&gt;v-v&gt;adv</ta>
            <ta e="T218" id="Seg_2288" s="T217">n.[n:case]</ta>
            <ta e="T219" id="Seg_2289" s="T218">n.[n:case]</ta>
            <ta e="T220" id="Seg_2290" s="T219">adv</ta>
            <ta e="T221" id="Seg_2291" s="T220">n.[n:case]</ta>
            <ta e="T222" id="Seg_2292" s="T221">n-n:case</ta>
            <ta e="T223" id="Seg_2293" s="T222">v-v&gt;adv</ta>
            <ta e="T224" id="Seg_2294" s="T223">v-v&gt;adv</ta>
            <ta e="T225" id="Seg_2295" s="T224">preverb</ta>
            <ta e="T226" id="Seg_2296" s="T225">v-v:ins-v:pn</ta>
            <ta e="T228" id="Seg_2297" s="T227">n.[n:case]</ta>
            <ta e="T229" id="Seg_2298" s="T228">n.[n:case]</ta>
            <ta e="T230" id="Seg_2299" s="T229">v-v&gt;adv</ta>
            <ta e="T231" id="Seg_2300" s="T230">preverb</ta>
            <ta e="T232" id="Seg_2301" s="T231">v-n:ins-v:ins.[v:pn]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_2302" s="T1">adv</ta>
            <ta e="T3" id="Seg_2303" s="T2">adv</ta>
            <ta e="T4" id="Seg_2304" s="T3">v</ta>
            <ta e="T5" id="Seg_2305" s="T4">n</ta>
            <ta e="T6" id="Seg_2306" s="T5">v</ta>
            <ta e="T7" id="Seg_2307" s="T6">v</ta>
            <ta e="T9" id="Seg_2308" s="T8">num</ta>
            <ta e="T10" id="Seg_2309" s="T9">adv</ta>
            <ta e="T11" id="Seg_2310" s="T10">adv</ta>
            <ta e="T12" id="Seg_2311" s="T11">v</ta>
            <ta e="T13" id="Seg_2312" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_2313" s="T13">adv</ta>
            <ta e="T16" id="Seg_2314" s="T15">ptcl</ta>
            <ta e="T17" id="Seg_2315" s="T16">n</ta>
            <ta e="T18" id="Seg_2316" s="T17">v</ta>
            <ta e="T19" id="Seg_2317" s="T18">n</ta>
            <ta e="T20" id="Seg_2318" s="T19">v</ta>
            <ta e="T22" id="Seg_2319" s="T21">adj</ta>
            <ta e="T23" id="Seg_2320" s="T22">n</ta>
            <ta e="T24" id="Seg_2321" s="T23">v</ta>
            <ta e="T26" id="Seg_2322" s="T25">adv</ta>
            <ta e="T27" id="Seg_2323" s="T26">v</ta>
            <ta e="T29" id="Seg_2324" s="T28">adv</ta>
            <ta e="T30" id="Seg_2325" s="T29">v</ta>
            <ta e="T31" id="Seg_2326" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_2327" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_2328" s="T32">ptcl</ta>
            <ta e="T35" id="Seg_2329" s="T34">v</ta>
            <ta e="T36" id="Seg_2330" s="T35">ptcl</ta>
            <ta e="T37" id="Seg_2331" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_2332" s="T37">n</ta>
            <ta e="T39" id="Seg_2333" s="T38">ptcl</ta>
            <ta e="T41" id="Seg_2334" s="T40">adv</ta>
            <ta e="T42" id="Seg_2335" s="T41">conj</ta>
            <ta e="T43" id="Seg_2336" s="T42">conj</ta>
            <ta e="T44" id="Seg_2337" s="T43">v</ta>
            <ta e="T46" id="Seg_2338" s="T45">n</ta>
            <ta e="T47" id="Seg_2339" s="T46">v</ta>
            <ta e="T49" id="Seg_2340" s="T48">n</ta>
            <ta e="T50" id="Seg_2341" s="T49">preverb</ta>
            <ta e="T51" id="Seg_2342" s="T50">v</ta>
            <ta e="T53" id="Seg_2343" s="T52">n</ta>
            <ta e="T54" id="Seg_2344" s="T53">preverb</ta>
            <ta e="T55" id="Seg_2345" s="T54">adv</ta>
            <ta e="T56" id="Seg_2346" s="T55">v</ta>
            <ta e="T57" id="Seg_2347" s="T56">nprop</ta>
            <ta e="T59" id="Seg_2348" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_2349" s="T59">adv</ta>
            <ta e="T61" id="Seg_2350" s="T60">n</ta>
            <ta e="T62" id="Seg_2351" s="T61">v</ta>
            <ta e="T64" id="Seg_2352" s="T63">n</ta>
            <ta e="T65" id="Seg_2353" s="T64">v</ta>
            <ta e="T66" id="Seg_2354" s="T65">n</ta>
            <ta e="T67" id="Seg_2355" s="T66">v</ta>
            <ta e="T69" id="Seg_2356" s="T68">dem</ta>
            <ta e="T70" id="Seg_2357" s="T69">n</ta>
            <ta e="T71" id="Seg_2358" s="T70">interrog</ta>
            <ta e="T72" id="Seg_2359" s="T71">v</ta>
            <ta e="T74" id="Seg_2360" s="T73">interrog</ta>
            <ta e="T76" id="Seg_2361" s="T75">interrog</ta>
            <ta e="T77" id="Seg_2362" s="T76">v</ta>
            <ta e="T78" id="Seg_2363" s="T77">ptcl</ta>
            <ta e="T79" id="Seg_2364" s="T78">n</ta>
            <ta e="T80" id="Seg_2365" s="T79">dem</ta>
            <ta e="T81" id="Seg_2366" s="T80">v</ta>
            <ta e="T83" id="Seg_2367" s="T82">dem</ta>
            <ta e="T84" id="Seg_2368" s="T83">adj</ta>
            <ta e="T85" id="Seg_2369" s="T84">n</ta>
            <ta e="T87" id="Seg_2370" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_2371" s="T87">pers</ta>
            <ta e="T89" id="Seg_2372" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_2373" s="T89">n</ta>
            <ta e="T92" id="Seg_2374" s="T91">adj</ta>
            <ta e="T93" id="Seg_2375" s="T92">v</ta>
            <ta e="T95" id="Seg_2376" s="T94">pers</ta>
            <ta e="T96" id="Seg_2377" s="T95">ptcl</ta>
            <ta e="T97" id="Seg_2378" s="T96">interrog</ta>
            <ta e="T98" id="Seg_2379" s="T97">dem</ta>
            <ta e="T99" id="Seg_2380" s="T98">n</ta>
            <ta e="T100" id="Seg_2381" s="T99">v</ta>
            <ta e="T102" id="Seg_2382" s="T101">adv</ta>
            <ta e="T103" id="Seg_2383" s="T102">adv</ta>
            <ta e="T106" id="Seg_2384" s="T105">v</ta>
            <ta e="T107" id="Seg_2385" s="T106">dem</ta>
            <ta e="T108" id="Seg_2386" s="T107">conj</ta>
            <ta e="T109" id="Seg_2387" s="T108">ptcl</ta>
            <ta e="T111" id="Seg_2388" s="T110">dem</ta>
            <ta e="T112" id="Seg_2389" s="T111">pers</ta>
            <ta e="T113" id="Seg_2390" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_2391" s="T113">v</ta>
            <ta e="T116" id="Seg_2392" s="T115">emphpro</ta>
            <ta e="T118" id="Seg_2393" s="T117">v</ta>
            <ta e="T119" id="Seg_2394" s="T118">ptcl</ta>
            <ta e="T121" id="Seg_2395" s="T120">dem</ta>
            <ta e="T123" id="Seg_2396" s="T122">v</ta>
            <ta e="T125" id="Seg_2397" s="T124">interrog</ta>
            <ta e="T126" id="Seg_2398" s="T125">interrog</ta>
            <ta e="T128" id="Seg_2399" s="T127">pers</ta>
            <ta e="T129" id="Seg_2400" s="T128">dem</ta>
            <ta e="T131" id="Seg_2401" s="T130">ptcl</ta>
            <ta e="T132" id="Seg_2402" s="T131">adj</ta>
            <ta e="T133" id="Seg_2403" s="T132">v</ta>
            <ta e="T135" id="Seg_2404" s="T134">dem</ta>
            <ta e="T136" id="Seg_2405" s="T135">v</ta>
            <ta e="T138" id="Seg_2406" s="T137">adv</ta>
            <ta e="T139" id="Seg_2407" s="T138">adv</ta>
            <ta e="T140" id="Seg_2408" s="T139">v</ta>
            <ta e="T143" id="Seg_2409" s="T142">v</ta>
            <ta e="T145" id="Seg_2410" s="T144">pers</ta>
            <ta e="T146" id="Seg_2411" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_2412" s="T146">v</ta>
            <ta e="T149" id="Seg_2413" s="T148">n</ta>
            <ta e="T150" id="Seg_2414" s="T149">adj</ta>
            <ta e="T151" id="Seg_2415" s="T150">v</ta>
            <ta e="T152" id="Seg_2416" s="T151">pers</ta>
            <ta e="T153" id="Seg_2417" s="T152">ptcl</ta>
            <ta e="T154" id="Seg_2418" s="T153">ptcl</ta>
            <ta e="T155" id="Seg_2419" s="T154">v</ta>
            <ta e="T157" id="Seg_2420" s="T156">pers</ta>
            <ta e="T158" id="Seg_2421" s="T157">adv</ta>
            <ta e="T159" id="Seg_2422" s="T158">n</ta>
            <ta e="T161" id="Seg_2423" s="T160">pers</ta>
            <ta e="T162" id="Seg_2424" s="T161">ptcl</ta>
            <ta e="T163" id="Seg_2425" s="T162">n</ta>
            <ta e="T164" id="Seg_2426" s="T163">adj</ta>
            <ta e="T165" id="Seg_2427" s="T164">n</ta>
            <ta e="T167" id="Seg_2428" s="T166">v</ta>
            <ta e="T169" id="Seg_2429" s="T168">n</ta>
            <ta e="T170" id="Seg_2430" s="T169">adj</ta>
            <ta e="T174" id="Seg_2431" s="T173">pers</ta>
            <ta e="T175" id="Seg_2432" s="T174">ptcl</ta>
            <ta e="T176" id="Seg_2433" s="T175">ptcl</ta>
            <ta e="T177" id="Seg_2434" s="T176">n</ta>
            <ta e="T178" id="Seg_2435" s="T177">pers</ta>
            <ta e="T179" id="Seg_2436" s="T178">ptcl</ta>
            <ta e="T180" id="Seg_2437" s="T179">pers</ta>
            <ta e="T181" id="Seg_2438" s="T180">pers</ta>
            <ta e="T182" id="Seg_2439" s="T181">v</ta>
            <ta e="T183" id="Seg_2440" s="T182">ptcl</ta>
            <ta e="T184" id="Seg_2441" s="T183">dem</ta>
            <ta e="T185" id="Seg_2442" s="T184">adj</ta>
            <ta e="T187" id="Seg_2443" s="T186">n</ta>
            <ta e="T190" id="Seg_2444" s="T189">adv</ta>
            <ta e="T191" id="Seg_2445" s="T190">dem</ta>
            <ta e="T192" id="Seg_2446" s="T191">n</ta>
            <ta e="T193" id="Seg_2447" s="T192">adv</ta>
            <ta e="T194" id="Seg_2448" s="T193">n</ta>
            <ta e="T195" id="Seg_2449" s="T194">n</ta>
            <ta e="T196" id="Seg_2450" s="T195">n</ta>
            <ta e="T197" id="Seg_2451" s="T196">v</ta>
            <ta e="T198" id="Seg_2452" s="T197">v</ta>
            <ta e="T199" id="Seg_2453" s="T198">dem</ta>
            <ta e="T200" id="Seg_2454" s="T199">n</ta>
            <ta e="T201" id="Seg_2455" s="T200">n</ta>
            <ta e="T202" id="Seg_2456" s="T201">v</ta>
            <ta e="T204" id="Seg_2457" s="T203">n</ta>
            <ta e="T205" id="Seg_2458" s="T204">v</ta>
            <ta e="T207" id="Seg_2459" s="T206">n</ta>
            <ta e="T208" id="Seg_2460" s="T207">n</ta>
            <ta e="T209" id="Seg_2461" s="T208">n</ta>
            <ta e="T210" id="Seg_2462" s="T209">v</ta>
            <ta e="T211" id="Seg_2463" s="T210">dem</ta>
            <ta e="T212" id="Seg_2464" s="T211">n</ta>
            <ta e="T213" id="Seg_2465" s="T212">n</ta>
            <ta e="T214" id="Seg_2466" s="T213">v</ta>
            <ta e="T215" id="Seg_2467" s="T214">n</ta>
            <ta e="T217" id="Seg_2468" s="T216">adv</ta>
            <ta e="T218" id="Seg_2469" s="T217">n</ta>
            <ta e="T219" id="Seg_2470" s="T218">n</ta>
            <ta e="T220" id="Seg_2471" s="T219">adv</ta>
            <ta e="T221" id="Seg_2472" s="T220">n</ta>
            <ta e="T222" id="Seg_2473" s="T221">n</ta>
            <ta e="T223" id="Seg_2474" s="T222">adv</ta>
            <ta e="T224" id="Seg_2475" s="T223">adv</ta>
            <ta e="T225" id="Seg_2476" s="T224">preverb</ta>
            <ta e="T226" id="Seg_2477" s="T225">v</ta>
            <ta e="T228" id="Seg_2478" s="T227">n</ta>
            <ta e="T229" id="Seg_2479" s="T228">n</ta>
            <ta e="T230" id="Seg_2480" s="T229">adv</ta>
            <ta e="T231" id="Seg_2481" s="T230">preverb</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_2482" s="T2">0.3.h:A</ta>
            <ta e="T4" id="Seg_2483" s="T3">0.3.h:A</ta>
            <ta e="T5" id="Seg_2484" s="T4">np:G</ta>
            <ta e="T6" id="Seg_2485" s="T5">0.3.h:A</ta>
            <ta e="T7" id="Seg_2486" s="T6">0.3.h:A</ta>
            <ta e="T10" id="Seg_2487" s="T9">adv:Time</ta>
            <ta e="T11" id="Seg_2488" s="T10">0.3.h:A</ta>
            <ta e="T12" id="Seg_2489" s="T11">0.3.h:A</ta>
            <ta e="T14" id="Seg_2490" s="T13">0.3.h:A</ta>
            <ta e="T17" id="Seg_2491" s="T16">np:Ins</ta>
            <ta e="T18" id="Seg_2492" s="T17">0.3.h:A</ta>
            <ta e="T19" id="Seg_2493" s="T18">np:P</ta>
            <ta e="T20" id="Seg_2494" s="T19">0.3.h:A</ta>
            <ta e="T23" id="Seg_2495" s="T22">np:P</ta>
            <ta e="T24" id="Seg_2496" s="T23">0.3.h:A</ta>
            <ta e="T26" id="Seg_2497" s="T25">adv:G</ta>
            <ta e="T27" id="Seg_2498" s="T26">0.3.h:A</ta>
            <ta e="T29" id="Seg_2499" s="T28">adv:G</ta>
            <ta e="T30" id="Seg_2500" s="T29">0.3.h:A</ta>
            <ta e="T35" id="Seg_2501" s="T34">0.3.h:E</ta>
            <ta e="T38" id="Seg_2502" s="T37">np:P</ta>
            <ta e="T41" id="Seg_2503" s="T40">adv:L</ta>
            <ta e="T46" id="Seg_2504" s="T45">np.h:E</ta>
            <ta e="T49" id="Seg_2505" s="T48">np:P</ta>
            <ta e="T51" id="Seg_2506" s="T50">0.3.h:A</ta>
            <ta e="T53" id="Seg_2507" s="T52">np:P</ta>
            <ta e="T55" id="Seg_2508" s="T54">0.3.h:A</ta>
            <ta e="T57" id="Seg_2509" s="T56">np.h:Th</ta>
            <ta e="T60" id="Seg_2510" s="T59">0.3.h:Th</ta>
            <ta e="T61" id="Seg_2511" s="T60">np.h:Th</ta>
            <ta e="T64" id="Seg_2512" s="T63">np.h:Th</ta>
            <ta e="T66" id="Seg_2513" s="T65">np.h:A</ta>
            <ta e="T70" id="Seg_2514" s="T69">np.h:A</ta>
            <ta e="T71" id="Seg_2515" s="T70">pro:P</ta>
            <ta e="T76" id="Seg_2516" s="T75">pro:P</ta>
            <ta e="T77" id="Seg_2517" s="T76">0.1.h:A</ta>
            <ta e="T79" id="Seg_2518" s="T78">np:P</ta>
            <ta e="T81" id="Seg_2519" s="T80">0.1.h:A</ta>
            <ta e="T85" id="Seg_2520" s="T84">np:Th 0.2.h:Poss</ta>
            <ta e="T88" id="Seg_2521" s="T87">pro.h:A</ta>
            <ta e="T90" id="Seg_2522" s="T89">np:P</ta>
            <ta e="T92" id="Seg_2523" s="T91">pro:Th</ta>
            <ta e="T95" id="Seg_2524" s="T94">pro.h:A</ta>
            <ta e="T99" id="Seg_2525" s="T98">np:P 0.2.h:Poss</ta>
            <ta e="T102" id="Seg_2526" s="T101">adv:Time</ta>
            <ta e="T106" id="Seg_2527" s="T105">0.1.h:A</ta>
            <ta e="T112" id="Seg_2528" s="T111">pro.h:A</ta>
            <ta e="T116" id="Seg_2529" s="T115">pro.h:P</ta>
            <ta e="T118" id="Seg_2530" s="T117">0.1.h:A</ta>
            <ta e="T121" id="Seg_2531" s="T120">pro:P</ta>
            <ta e="T123" id="Seg_2532" s="T122">0.1.h:A</ta>
            <ta e="T128" id="Seg_2533" s="T127">pro:Th</ta>
            <ta e="T136" id="Seg_2534" s="T135">0.2.h:A</ta>
            <ta e="T140" id="Seg_2535" s="T139">0.3:Th</ta>
            <ta e="T143" id="Seg_2536" s="T142">0.2.h:A</ta>
            <ta e="T145" id="Seg_2537" s="T144">pro.h:A</ta>
            <ta e="T149" id="Seg_2538" s="T148">np.h:A</ta>
            <ta e="T152" id="Seg_2539" s="T151">pro.h:A</ta>
            <ta e="T161" id="Seg_2540" s="T160">pro.h:Th</ta>
            <ta e="T167" id="Seg_2541" s="T166">0.2.h:A</ta>
            <ta e="T174" id="Seg_2542" s="T173">pro.h:Th</ta>
            <ta e="T180" id="Seg_2543" s="T179">pro.h:A</ta>
            <ta e="T181" id="Seg_2544" s="T180">pro.h:Th</ta>
            <ta e="T187" id="Seg_2545" s="T186">np:Ins</ta>
            <ta e="T192" id="Seg_2546" s="T191">np:Th</ta>
            <ta e="T193" id="Seg_2547" s="T192">0.3.h:A</ta>
            <ta e="T195" id="Seg_2548" s="T194">np:Poss</ta>
            <ta e="T196" id="Seg_2549" s="T195">np:P</ta>
            <ta e="T197" id="Seg_2550" s="T196">0.3.h:A</ta>
            <ta e="T201" id="Seg_2551" s="T200">np.h:Th</ta>
            <ta e="T204" id="Seg_2552" s="T203">np:G</ta>
            <ta e="T205" id="Seg_2553" s="T204">0.3.h:A</ta>
            <ta e="T208" id="Seg_2554" s="T207">np.h:Th</ta>
            <ta e="T209" id="Seg_2555" s="T208">np:G</ta>
            <ta e="T210" id="Seg_2556" s="T209">0.3.h:A</ta>
            <ta e="T213" id="Seg_2557" s="T212">np.h:Th</ta>
            <ta e="T215" id="Seg_2558" s="T214">np:G</ta>
            <ta e="T217" id="Seg_2559" s="T216">0.3.h:A</ta>
            <ta e="T219" id="Seg_2560" s="T218">np.h:Th</ta>
            <ta e="T220" id="Seg_2561" s="T219">adv:G</ta>
            <ta e="T222" id="Seg_2562" s="T221">np.h:P</ta>
            <ta e="T223" id="Seg_2563" s="T222">0.3.h:A</ta>
            <ta e="T226" id="Seg_2564" s="T225">0.3.h:A 0.3.h:P</ta>
            <ta e="T229" id="Seg_2565" s="T228">np.h:P</ta>
            <ta e="T230" id="Seg_2566" s="T229">0.3.h:A</ta>
            <ta e="T232" id="Seg_2567" s="T231">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_2568" s="T2">s:temp</ta>
            <ta e="T4" id="Seg_2569" s="T3">0.3.h:S v:pred</ta>
            <ta e="T6" id="Seg_2570" s="T5">0.3.h:S v:pred</ta>
            <ta e="T7" id="Seg_2571" s="T6">0.3.h:S v:pred</ta>
            <ta e="T11" id="Seg_2572" s="T10">s:temp</ta>
            <ta e="T12" id="Seg_2573" s="T11">0.3.h:S v:pred</ta>
            <ta e="T14" id="Seg_2574" s="T13">s:temp</ta>
            <ta e="T18" id="Seg_2575" s="T17">0.3.h:S v:pred</ta>
            <ta e="T19" id="Seg_2576" s="T18">np:O</ta>
            <ta e="T20" id="Seg_2577" s="T19">0.3.h:S v:pred</ta>
            <ta e="T23" id="Seg_2578" s="T22">np:O</ta>
            <ta e="T24" id="Seg_2579" s="T23">0.3.h:S v:pred</ta>
            <ta e="T27" id="Seg_2580" s="T26">0.3.h:S v:pred</ta>
            <ta e="T30" id="Seg_2581" s="T29">0.3.h:S v:pred</ta>
            <ta e="T35" id="Seg_2582" s="T34">0.3.h:S v:pred</ta>
            <ta e="T38" id="Seg_2583" s="T37">np:O</ta>
            <ta e="T47" id="Seg_2584" s="T41">s:purp</ta>
            <ta e="T49" id="Seg_2585" s="T48">np:O</ta>
            <ta e="T51" id="Seg_2586" s="T50">0.3.h:S v:pred</ta>
            <ta e="T55" id="Seg_2587" s="T52">s:temp</ta>
            <ta e="T56" id="Seg_2588" s="T55">v:pred</ta>
            <ta e="T57" id="Seg_2589" s="T56">np.h:S</ta>
            <ta e="T60" id="Seg_2590" s="T59">s:temp</ta>
            <ta e="T61" id="Seg_2591" s="T60">np.h:S</ta>
            <ta e="T62" id="Seg_2592" s="T61">v:pred</ta>
            <ta e="T64" id="Seg_2593" s="T63">np.h:S</ta>
            <ta e="T65" id="Seg_2594" s="T64">v:pred</ta>
            <ta e="T66" id="Seg_2595" s="T65">np.h:S</ta>
            <ta e="T67" id="Seg_2596" s="T66">v:pred</ta>
            <ta e="T70" id="Seg_2597" s="T69">np.h:S</ta>
            <ta e="T71" id="Seg_2598" s="T70">pro:O</ta>
            <ta e="T72" id="Seg_2599" s="T71">v:pred</ta>
            <ta e="T76" id="Seg_2600" s="T75">pro:O</ta>
            <ta e="T77" id="Seg_2601" s="T76">0.1.h:S v:pred</ta>
            <ta e="T79" id="Seg_2602" s="T78">np:O</ta>
            <ta e="T81" id="Seg_2603" s="T80">0.1.h:S v:pred</ta>
            <ta e="T84" id="Seg_2604" s="T83">adj:pred</ta>
            <ta e="T85" id="Seg_2605" s="T84">np:S</ta>
            <ta e="T88" id="Seg_2606" s="T87">pro.h:S</ta>
            <ta e="T90" id="Seg_2607" s="T89">np:O</ta>
            <ta e="T92" id="Seg_2608" s="T91">pro:S</ta>
            <ta e="T93" id="Seg_2609" s="T92">v:pred</ta>
            <ta e="T95" id="Seg_2610" s="T94">pro.h:S</ta>
            <ta e="T99" id="Seg_2611" s="T98">np:O</ta>
            <ta e="T100" id="Seg_2612" s="T99">v:pred</ta>
            <ta e="T106" id="Seg_2613" s="T105">0.1.h:S v:pred</ta>
            <ta e="T112" id="Seg_2614" s="T111">pro.h:S</ta>
            <ta e="T114" id="Seg_2615" s="T113">v:pred</ta>
            <ta e="T116" id="Seg_2616" s="T115">pro.h:O</ta>
            <ta e="T118" id="Seg_2617" s="T117">0.1.h:S v:pred</ta>
            <ta e="T121" id="Seg_2618" s="T120">pro:O</ta>
            <ta e="T123" id="Seg_2619" s="T122">0.1.h:S v:pred</ta>
            <ta e="T128" id="Seg_2620" s="T127">pro:S</ta>
            <ta e="T132" id="Seg_2621" s="T131">adj:pred</ta>
            <ta e="T133" id="Seg_2622" s="T132">cop</ta>
            <ta e="T136" id="Seg_2623" s="T135">0.2.h:S v:pred</ta>
            <ta e="T139" id="Seg_2624" s="T138">adj:pred</ta>
            <ta e="T140" id="Seg_2625" s="T139">0.3:S cop</ta>
            <ta e="T143" id="Seg_2626" s="T142">0.2.h:S v:pred</ta>
            <ta e="T145" id="Seg_2627" s="T144">pro.h:S</ta>
            <ta e="T147" id="Seg_2628" s="T146">v:pred</ta>
            <ta e="T149" id="Seg_2629" s="T148">np.h:S</ta>
            <ta e="T151" id="Seg_2630" s="T150">v:pred</ta>
            <ta e="T152" id="Seg_2631" s="T151">pro.h:S</ta>
            <ta e="T155" id="Seg_2632" s="T154">v:pred</ta>
            <ta e="T161" id="Seg_2633" s="T160">pro.h:S</ta>
            <ta e="T165" id="Seg_2634" s="T164">n:pred</ta>
            <ta e="T167" id="Seg_2635" s="T166">0.2.h:S v:pred</ta>
            <ta e="T174" id="Seg_2636" s="T173">pro.h:S</ta>
            <ta e="T177" id="Seg_2637" s="T176">n:pred</ta>
            <ta e="T180" id="Seg_2638" s="T179">pro.h:S</ta>
            <ta e="T181" id="Seg_2639" s="T180">pro.h:O</ta>
            <ta e="T182" id="Seg_2640" s="T181">v:pred</ta>
            <ta e="T193" id="Seg_2641" s="T190">s:temp</ta>
            <ta e="T196" id="Seg_2642" s="T195">np:O</ta>
            <ta e="T197" id="Seg_2643" s="T196">0.3.h:S v:pred</ta>
            <ta e="T201" id="Seg_2644" s="T200">np.h:O</ta>
            <ta e="T205" id="Seg_2645" s="T204">0.3.h:S v:pred</ta>
            <ta e="T208" id="Seg_2646" s="T207">np.h:O</ta>
            <ta e="T210" id="Seg_2647" s="T209">0.3.h:S v:pred</ta>
            <ta e="T217" id="Seg_2648" s="T210">s:temp</ta>
            <ta e="T219" id="Seg_2649" s="T218">np.h:O</ta>
            <ta e="T223" id="Seg_2650" s="T220">s:temp</ta>
            <ta e="T226" id="Seg_2651" s="T225">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T230" id="Seg_2652" s="T227">s:temp</ta>
            <ta e="T232" id="Seg_2653" s="T231">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T42" id="Seg_2654" s="T41">RUS:gram</ta>
            <ta e="T43" id="Seg_2655" s="T42">RUS:gram</ta>
            <ta e="T108" id="Seg_2656" s="T107">RUS:gram</ta>
            <ta e="T109" id="Seg_2657" s="T108">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T8" id="Seg_2658" s="T1">He went far enough into the forest, he went hunting.</ta>
            <ta e="T20" id="Seg_2659" s="T8">Once while hunting, while fishing with a seine, he killed a perch with his palm.</ta>
            <ta e="T24" id="Seg_2660" s="T21">He killed a big perch.</ta>
            <ta e="T27" id="Seg_2661" s="T25">He came out upwards (?).</ta>
            <ta e="T47" id="Seg_2662" s="T28">Having come out upwards (?), probably he knows (?), probably he [cooked] the perch on a spit, for the devil to know it.</ta>
            <ta e="T51" id="Seg_2663" s="T48">He spitted(?) the perch.</ta>
            <ta e="T57" id="Seg_2664" s="T52">Ichakichika sits cooking the perch.</ta>
            <ta e="T62" id="Seg_2665" s="T58">And while he was sitting, the devil appeared.</ta>
            <ta e="T67" id="Seg_2666" s="T63">The devil appeared, the devil came.</ta>
            <ta e="T72" id="Seg_2667" s="T68">“What is the guy doing?</ta>
            <ta e="T74" id="Seg_2668" s="T73">What?”</ta>
            <ta e="T81" id="Seg_2669" s="T75">“What I am doing, I am cooking food.”</ta>
            <ta e="T85" id="Seg_2670" s="T82">“What's your food?”</ta>
            <ta e="T90" id="Seg_2671" s="T86">“No, I am [cooking] the liver.</ta>
            <ta e="T93" id="Seg_2672" s="T91">It' so tasty.”</ta>
            <ta e="T100" id="Seg_2673" s="T94">“What kind of food are you cooking?”</ta>
            <ta e="T109" id="Seg_2674" s="T101">Then, it's interesting, but should I tell it?</ta>
            <ta e="T114" id="Seg_2675" s="T110">“I have castrated.</ta>
            <ta e="T123" id="Seg_2676" s="T115">I've castrated myself and now I'm cooking it.”</ta>
            <ta e="T126" id="Seg_2677" s="T124">“What, how(?)?”</ta>
            <ta e="T141" id="Seg_2678" s="T127">“It wasn't short, cook it, it's so tasty.”</ta>
            <ta e="T143" id="Seg_2679" s="T142">Wait.</ta>
            <ta e="T147" id="Seg_2680" s="T144">“I won't give it.”</ta>
            <ta e="T155" id="Seg_2681" s="T148">The old man says so : “No; I won't give it.</ta>
            <ta e="T165" id="Seg_2682" s="T156">You (?), I am a devil old man.</ta>
            <ta e="T167" id="Seg_2683" s="T166">Wait.</ta>
            <ta e="T170" id="Seg_2684" s="T168">A devil old man.”</ta>
            <ta e="T188" id="Seg_2685" s="T171">“OK, you are not a man, you, I'll wrap these branches of a bird cherry tree (so?).”</ta>
            <ta e="T205" id="Seg_2686" s="T189">Then, having pulled out the bird cherry tree, he made the branches of the bird cherry tree, and he wraped this devil old man into these branches.</ta>
            <ta e="T226" id="Seg_2687" s="T206">He wraped the devil old man into those branches, having wraped him, he put the devil old man onto the back and having stabbed the devil old man he killed him.</ta>
            <ta e="T232" id="Seg_2688" s="T227">Having stabbed the devil old man, he killed him.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T8" id="Seg_2689" s="T1">Er ging weit genug in den Wald, er ging jagen.</ta>
            <ta e="T20" id="Seg_2690" s="T8">Einmal während der Jagd, als er mit einem Zugnetz fischte, tötete er einen Barsch mit der Hand.</ta>
            <ta e="T24" id="Seg_2691" s="T21">Er tötete einen großen Barsch.</ta>
            <ta e="T27" id="Seg_2692" s="T25">Er kam heraus, aufwärts.</ta>
            <ta e="T47" id="Seg_2693" s="T28">Hinaufgekommen weiß er vielleicht, vielleicht [briet] er den Barsch am Spieß, um den Teufel wissen zu lassen.</ta>
            <ta e="T51" id="Seg_2694" s="T48">Er spießte (?) den Barsch auf.</ta>
            <ta e="T57" id="Seg_2695" s="T52">Ichakichika sitzt, während der Barsch brät.</ta>
            <ta e="T62" id="Seg_2696" s="T58">Und während er saß, erschien der Teufel.</ta>
            <ta e="T67" id="Seg_2697" s="T63">Der Teufel erschien, der Teufel kam.</ta>
            <ta e="T72" id="Seg_2698" s="T68">"Was macht der Kerl?</ta>
            <ta e="T74" id="Seg_2699" s="T73">Was?"</ta>
            <ta e="T81" id="Seg_2700" s="T75">"Was ich mache, ich mache Essen."</ta>
            <ta e="T85" id="Seg_2701" s="T82">"Was ist dein Essen?"</ta>
            <ta e="T90" id="Seg_2702" s="T86">"Nein, ich [koche] hier die Leber.</ta>
            <ta e="T93" id="Seg_2703" s="T91">Sie ist so lecker."</ta>
            <ta e="T100" id="Seg_2704" s="T94">"Was für Essen machst du da?"</ta>
            <ta e="T109" id="Seg_2705" s="T101">Dann, es ist interessant, aber soll ich es erzählen?</ta>
            <ta e="T114" id="Seg_2706" s="T110">"Ich habe kastriert.</ta>
            <ta e="T123" id="Seg_2707" s="T115">Ich habe mich selbst kastriert und jetzt koche ich es."</ta>
            <ta e="T126" id="Seg_2708" s="T124">"Was, wie?"</ta>
            <ta e="T141" id="Seg_2709" s="T127">"Es war nicht kurz, koch es, es ist so lecker."</ta>
            <ta e="T143" id="Seg_2710" s="T142">Warte.</ta>
            <ta e="T147" id="Seg_2711" s="T144">"Ich gebe es nicht her."</ta>
            <ta e="T155" id="Seg_2712" s="T148">Der alte Mann sagt so: "Nein, ich gebe es nicht her.</ta>
            <ta e="T165" id="Seg_2713" s="T156">Du, ich bin ein alter Teufel.</ta>
            <ta e="T167" id="Seg_2714" s="T166">Warte.</ta>
            <ta e="T170" id="Seg_2715" s="T168">Ein alter Teufel."</ta>
            <ta e="T188" id="Seg_2716" s="T171">Gut, du bist kein Mann, du, ich umwickel dich mit diesen Ästen der Wildkirsche."</ta>
            <ta e="T205" id="Seg_2717" s="T189">Dann, nachdem er den Wildkirschenbaum hervorgeholt hatte, machte er Äste des Wildkirschenbaums, er wickelte diesen alten Teufel in diese Äste.</ta>
            <ta e="T226" id="Seg_2718" s="T206">Er wickelte den alten Teufel in die Äste, als er ihn umwickelt hatte, legte er den alten Teufel auf den Rücken und, indem er ihn erstach, tötete er den alten Teufel.</ta>
            <ta e="T232" id="Seg_2719" s="T227">Als er den alten Teufel erstach, tötete er ihn.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T8" id="Seg_2720" s="T1">На достаточное расстояние ушела на охоту в лес, охотится, охотится, (?).</ta>
            <ta e="T20" id="Seg_2721" s="T8">Однажды, охотясь, рыбачил, рыбача ли ладошкой убил, окуня убил.</ta>
            <ta e="T24" id="Seg_2722" s="T21">Большого окуня убил.</ta>
            <ta e="T27" id="Seg_2723" s="T25">Наверх вышел.</ta>
            <ta e="T47" id="Seg_2724" s="T28">Наверх вышел, наверное, это, наверное, (?) знает, наверное, наверное, окуня ведь (?) на вертеле, и чтобы знал, черт чтобы знал.</ta>
            <ta e="T51" id="Seg_2725" s="T48">Окуня вниз нанизал(?).</ta>
            <ta e="T57" id="Seg_2726" s="T52">Окуня жаря, сидит Ичаичыка.</ta>
            <ta e="T62" id="Seg_2727" s="T58">Вроде, пока он сидел, черт появился.</ta>
            <ta e="T67" id="Seg_2728" s="T63">Черт появился, черт пришел.</ta>
            <ta e="T72" id="Seg_2729" s="T68">“Этот мальчик, что делает?</ta>
            <ta e="T74" id="Seg_2730" s="T73">Что?”</ta>
            <ta e="T81" id="Seg_2731" s="T75">“Что я делаю, мол, еду вот готовлю”.</ta>
            <ta e="T85" id="Seg_2732" s="T82">“Это какая еда у тебя?”</ta>
            <ta e="T90" id="Seg_2733" s="T86">“Нет, я вот печень.</ta>
            <ta e="T93" id="Seg_2734" s="T91">Так вкусно”.</ta>
            <ta e="T100" id="Seg_2735" s="T94">“Ты, мол, что за такую еду (готовишь?)?”</ta>
            <ta e="T109" id="Seg_2736" s="T101">Потом так, это интересно, рассказать мне это что ли?</ta>
            <ta e="T114" id="Seg_2737" s="T110">“Это я ведь кастрировал.</ta>
            <ta e="T123" id="Seg_2738" s="T115">Сам себя кастрировал, мол, это сам пеку”.</ta>
            <ta e="T126" id="Seg_2739" s="T124">“Что, как (?)?”</ta>
            <ta e="T141" id="Seg_2740" s="T127">“Он этот (?) не короткий был, это испеки, так вкусно”.</ta>
            <ta e="T143" id="Seg_2741" s="T142">Подожди.</ta>
            <ta e="T147" id="Seg_2742" s="T144">“Я не дам”.</ta>
            <ta e="T155" id="Seg_2743" s="T148">Старик так говорит (=делается): “Я нет, не дам.</ta>
            <ta e="T165" id="Seg_2744" s="T156">Ты это (?) я, мол, (черт-старик?) человек.</ta>
            <ta e="T167" id="Seg_2745" s="T166">Подожди.</ta>
            <ta e="T170" id="Seg_2746" s="T168">Черт-старик (человек)”.</ta>
            <ta e="T188" id="Seg_2747" s="T171">“Ну ладно, ты, мол, не человек, ты, мол, я тебя заверну, мол, эти черёмуховые ветки (так?)”.</ta>
            <ta e="T205" id="Seg_2748" s="T189">Потом вот черёмыху натаскав, черёмуху, черёмуховые ветви наделал, наделал, этого черта-старика вот в дерево завернул.</ta>
            <ta e="T226" id="Seg_2749" s="T206">Черта-старика в дерево завернул, этот черт-старик есть, (его) в дерево завернув, черта-старика на спину (положив), черта-старика, заколов, что сделав, убил.</ta>
            <ta e="T232" id="Seg_2750" s="T227">Черта-старика, заколов, убил.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T8" id="Seg_2751" s="T1">(Достаточно) На достаточное расстояние разъезжать пойдёт в лес, ездит, ездит эээ.</ta>
            <ta e="T20" id="Seg_2752" s="T8">В одном промежутке, рыбачит, или руки, ладошкой убил, окунь убил.</ta>
            <ta e="T24" id="Seg_2753" s="T21">Большой окунь убил.</ta>
            <ta e="T27" id="Seg_2754" s="T25">Верх вышел, вверх вышел.</ta>
            <ta e="T47" id="Seg_2755" s="T28">Вверх поднялся, наверно это, наверно (…?) знает, наверно окунь на это вертел и чтоб знал Лозы, знал.</ta>
            <ta e="T51" id="Seg_2756" s="T48">Окуня вниз нанизал.</ta>
            <ta e="T57" id="Seg_2757" s="T52">Окуня вниз печёт сидит Ича-ичыка.</ta>
            <ta e="T62" id="Seg_2758" s="T58">Вроде бы сидя, Лозы появился.</ta>
            <ta e="T67" id="Seg_2759" s="T63">Лозы появился, Лозы прищёл.</ta>
            <ta e="T72" id="Seg_2760" s="T68">Этот мальчик, что делает?</ta>
            <ta e="T74" id="Seg_2761" s="T73">Что?</ta>
            <ta e="T81" id="Seg_2762" s="T75">Что делаю, вот еду вот пеку.</ta>
            <ta e="T85" id="Seg_2763" s="T82">Это какую еду?</ta>
            <ta e="T90" id="Seg_2764" s="T86">Нет я вот печень.</ta>
            <ta e="T93" id="Seg_2765" s="T91">Это вкусно.</ta>
            <ta e="T100" id="Seg_2766" s="T94">Ты мол что за такую еду печёшь?</ta>
            <ta e="T109" id="Seg_2767" s="T101">Потом так, это интересно, сказать это что ли? (кому-то)</ta>
            <ta e="T114" id="Seg_2768" s="T110">Это я ведь кастрировал.</ta>
            <ta e="T123" id="Seg_2769" s="T115">Сам себя кастрировал это пеку.</ta>
            <ta e="T126" id="Seg_2770" s="T124">Что, как (…..?)</ta>
            <ta e="T141" id="Seg_2771" s="T127">он это (…?) не короткий был, это испеки, так вкусно)</ta>
            <ta e="T143" id="Seg_2772" s="T142">Подожди.</ta>
            <ta e="T147" id="Seg_2773" s="T144">Я не дам.</ta>
            <ta e="T155" id="Seg_2774" s="T148">Старик так делается: я не, не дам.</ta>
            <ta e="T165" id="Seg_2775" s="T156">Ты это (….?) я лишь Лозы ира человек.</ta>
            <ta e="T170" id="Seg_2776" s="T168">Лозы ира человек.</ta>
            <ta e="T188" id="Seg_2777" s="T171">Ну ладно, ты не человек, ты это я тебя заверну лишь этими черёмуховыми ветками так.</ta>
            <ta e="T205" id="Seg_2778" s="T189">Потом вот черёмуху натаскал черёмуху, черёмуховые ветви наделал, наделал этого Лозы ира в дерево завернул (закрутил)</ta>
            <ta e="T226" id="Seg_2779" s="T206">Лозы ира в дерево завернул, этого Лозы ира этого в дерево завернул (закрутил) Лозы ира назад, Лозы ира заколов Лозы ира вниз убил.</ta>
            <ta e="T232" id="Seg_2780" s="T227">Лозы ира заколов вниз убил.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T8" id="Seg_2781" s="T1">[BrM:] Probably it is not the very beginning of the story.</ta>
            <ta e="T20" id="Seg_2782" s="T8">[BrM:] Tentative analysis of 'qənna', 'qənnɨnt' (unprobable CO before INFER). Cf. 'qətɨnt' in sent. 3.</ta>
            <ta e="T24" id="Seg_2783" s="T21">[BrM:] Tentative analysis of 'qətɨnt', see sent. 2.</ta>
            <ta e="T47" id="Seg_2784" s="T28">[BrM:] Tentative analysis of '-qt' in 'tɛnɨmɨqt' as SUP.3SG. [AAV:] čʼɔːwsɨn &gt; čʼɔːktɨn ? [AAV:] i štəp (štip) tɛnɨmɨt ?</ta>
            <ta e="T67" id="Seg_2785" s="T63">[BrM:] Tentative transcription of 'tüntɨ'.</ta>
            <ta e="T109" id="Seg_2786" s="T101">[IES:] The storyteller is asking someone else.</ta>
            <ta e="T232" id="Seg_2787" s="T227">[BrM:] Uncertain glossing of 'qətaja'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
