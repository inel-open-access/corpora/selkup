<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KFN_1967_HumanSizedPike_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KFN_1965_HumanSizedPike_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">111</ud-information>
            <ud-information attribute-name="# HIAT:w">88</ud-information>
            <ud-information attribute-name="# e">88</ud-information>
            <ud-information attribute-name="# HIAT:u">13</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KFN">
            <abbreviation>KFN</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KFN"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T88" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Qunando</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">warkə</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">pič</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Polta</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">tamɨt</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">tu</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">eːnda</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_29" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">Qandeqɨt</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">olgom</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">morelʼišpad</ts>
                  <nts id="Seg_38" n="HIAT:ip">,</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_41" n="HIAT:w" s="T10">ünda</ts>
                  <nts id="Seg_42" n="HIAT:ip">,</nts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_45" n="HIAT:w" s="T11">kutark</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_48" n="HIAT:w" s="T12">tüldʼe</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_51" n="HIAT:w" s="T13">tare</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_54" n="HIAT:w" s="T14">čenda</ts>
                  <nts id="Seg_55" n="HIAT:ip">.</nts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_58" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_60" n="HIAT:w" s="T15">Mi</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_63" n="HIAT:w" s="T16">šedəqut</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_66" n="HIAT:w" s="T17">töwaj</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_69" n="HIAT:w" s="T18">na</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_72" n="HIAT:w" s="T19">tond</ts>
                  <nts id="Seg_73" n="HIAT:ip">.</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_76" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_78" n="HIAT:w" s="T20">Olɣo</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_81" n="HIAT:w" s="T21">wes</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_84" n="HIAT:w" s="T22">morelʼba</ts>
                  <nts id="Seg_85" n="HIAT:ip">,</nts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">nɨndo</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">miː</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_94" n="HIAT:w" s="T25">wargaj</ts>
                  <nts id="Seg_95" n="HIAT:ip">,</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">kaʒnɨj</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">čeːl</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">ünda</ts>
                  <nts id="Seg_105" n="HIAT:ip">,</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_108" n="HIAT:w" s="T29">qajda</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_111" n="HIAT:w" s="T30">načat</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_114" n="HIAT:w" s="T31">kajak</ts>
                  <nts id="Seg_115" n="HIAT:ip">.</nts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_118" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_120" n="HIAT:w" s="T32">Ulɣo</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_123" n="HIAT:w" s="T33">wes</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_126" n="HIAT:w" s="T34">taqpigelǯʼembak</ts>
                  <nts id="Seg_127" n="HIAT:ip">.</nts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_130" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_132" n="HIAT:w" s="T35">Mat</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_135" n="HIAT:w" s="T36">pajam</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_138" n="HIAT:w" s="T37">uruk</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_141" n="HIAT:w" s="T38">larɨmbɨ</ts>
                  <nts id="Seg_142" n="HIAT:ip">.</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_145" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_147" n="HIAT:w" s="T39">Tabə</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_150" n="HIAT:w" s="T40">tond</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_153" n="HIAT:w" s="T41">ütetko</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_156" n="HIAT:w" s="T42">naj</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_159" n="HIAT:w" s="T43">aː</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_162" n="HIAT:w" s="T44">qojah</ts>
                  <nts id="Seg_163" n="HIAT:ip">.</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_166" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_168" n="HIAT:w" s="T45">Na</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_171" n="HIAT:w" s="T46">tu</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_174" n="HIAT:w" s="T47">nʼärt</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_177" n="HIAT:w" s="T48">kojoɣot</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_180" n="HIAT:w" s="T49">eːk</ts>
                  <nts id="Seg_181" n="HIAT:ip">.</nts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_184" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_186" n="HIAT:w" s="T50">Nʼäroɣɨn</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_189" n="HIAT:w" s="T51">eja</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_192" n="HIAT:w" s="T52">na</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_195" n="HIAT:w" s="T53">tu</ts>
                  <nts id="Seg_196" n="HIAT:ip">,</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_199" n="HIAT:w" s="T54">okɨr</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_202" n="HIAT:w" s="T55">kaːdarɣɨnt</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_205" n="HIAT:w" s="T56">tɨtɨl</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_208" n="HIAT:w" s="T57">olkoː</ts>
                  <nts id="Seg_209" n="HIAT:ip">.</nts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_212" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_214" n="HIAT:w" s="T58">To</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_217" n="HIAT:w" s="T59">haːlgə</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_220" n="HIAT:w" s="T60">uruk</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_223" n="HIAT:w" s="T61">korreːja</ts>
                  <nts id="Seg_224" n="HIAT:ip">.</nts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_227" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_229" n="HIAT:w" s="T62">Na</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_232" n="HIAT:w" s="T63">tokot</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_235" n="HIAT:w" s="T64">warga</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_238" n="HIAT:w" s="T65">warɣə</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_241" n="HIAT:w" s="T66">pitča</ts>
                  <nts id="Seg_242" n="HIAT:ip">,</nts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_245" n="HIAT:w" s="T67">qunando</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_248" n="HIAT:w" s="T68">warkə</ts>
                  <nts id="Seg_249" n="HIAT:ip">,</nts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_252" n="HIAT:w" s="T69">piča</ts>
                  <nts id="Seg_253" n="HIAT:ip">.</nts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_256" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_258" n="HIAT:w" s="T70">Warwarwa</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_261" n="HIAT:w" s="T71">paja</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_264" n="HIAT:w" s="T72">čeːnča</ts>
                  <nts id="Seg_265" n="HIAT:ip">,</nts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_268" n="HIAT:w" s="T73">šandə</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_271" n="HIAT:w" s="T74">kɨba</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_274" n="HIAT:w" s="T75">andee</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_277" n="HIAT:w" s="T76">kojamban</ts>
                  <nts id="Seg_278" n="HIAT:ip">,</nts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_281" n="HIAT:w" s="T77">piča</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_284" n="HIAT:w" s="T78">na</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_287" n="HIAT:w" s="T79">kup</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_290" n="HIAT:w" s="T80">oralbat</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_293" n="HIAT:w" s="T81">i</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_296" n="HIAT:w" s="T82">qup</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_299" n="HIAT:w" s="T83">kalʼembat</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_302" n="HIAT:w" s="T84">piča</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_305" n="HIAT:w" s="T85">oralbat</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_308" n="HIAT:w" s="T86">polgu</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_311" n="HIAT:w" s="T87">kɨga</ts>
                  <nts id="Seg_312" n="HIAT:ip">.</nts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T88" id="Seg_314" n="sc" s="T0">
               <ts e="T1" id="Seg_316" n="e" s="T0">Qunando </ts>
               <ts e="T2" id="Seg_318" n="e" s="T1">warkə </ts>
               <ts e="T3" id="Seg_320" n="e" s="T2">pič. </ts>
               <ts e="T4" id="Seg_322" n="e" s="T3">Polta </ts>
               <ts e="T5" id="Seg_324" n="e" s="T4">tamɨt </ts>
               <ts e="T6" id="Seg_326" n="e" s="T5">tu </ts>
               <ts e="T7" id="Seg_328" n="e" s="T6">eːnda. </ts>
               <ts e="T8" id="Seg_330" n="e" s="T7">Qandeqɨt </ts>
               <ts e="T9" id="Seg_332" n="e" s="T8">olgom </ts>
               <ts e="T10" id="Seg_334" n="e" s="T9">morelʼišpad, </ts>
               <ts e="T11" id="Seg_336" n="e" s="T10">ünda, </ts>
               <ts e="T12" id="Seg_338" n="e" s="T11">kutark </ts>
               <ts e="T13" id="Seg_340" n="e" s="T12">tüldʼe </ts>
               <ts e="T14" id="Seg_342" n="e" s="T13">tare </ts>
               <ts e="T15" id="Seg_344" n="e" s="T14">čenda. </ts>
               <ts e="T16" id="Seg_346" n="e" s="T15">Mi </ts>
               <ts e="T17" id="Seg_348" n="e" s="T16">šedəqut </ts>
               <ts e="T18" id="Seg_350" n="e" s="T17">töwaj </ts>
               <ts e="T19" id="Seg_352" n="e" s="T18">na </ts>
               <ts e="T20" id="Seg_354" n="e" s="T19">tond. </ts>
               <ts e="T21" id="Seg_356" n="e" s="T20">Olɣo </ts>
               <ts e="T22" id="Seg_358" n="e" s="T21">wes </ts>
               <ts e="T23" id="Seg_360" n="e" s="T22">morelʼba, </ts>
               <ts e="T24" id="Seg_362" n="e" s="T23">nɨndo </ts>
               <ts e="T25" id="Seg_364" n="e" s="T24">miː </ts>
               <ts e="T26" id="Seg_366" n="e" s="T25">wargaj, </ts>
               <ts e="T27" id="Seg_368" n="e" s="T26">kaʒnɨj </ts>
               <ts e="T28" id="Seg_370" n="e" s="T27">čeːl </ts>
               <ts e="T29" id="Seg_372" n="e" s="T28">ünda, </ts>
               <ts e="T30" id="Seg_374" n="e" s="T29">qajda </ts>
               <ts e="T31" id="Seg_376" n="e" s="T30">načat </ts>
               <ts e="T32" id="Seg_378" n="e" s="T31">kajak. </ts>
               <ts e="T33" id="Seg_380" n="e" s="T32">Ulɣo </ts>
               <ts e="T34" id="Seg_382" n="e" s="T33">wes </ts>
               <ts e="T35" id="Seg_384" n="e" s="T34">taqpigelǯʼembak. </ts>
               <ts e="T36" id="Seg_386" n="e" s="T35">Mat </ts>
               <ts e="T37" id="Seg_388" n="e" s="T36">pajam </ts>
               <ts e="T38" id="Seg_390" n="e" s="T37">uruk </ts>
               <ts e="T39" id="Seg_392" n="e" s="T38">larɨmbɨ. </ts>
               <ts e="T40" id="Seg_394" n="e" s="T39">Tabə </ts>
               <ts e="T41" id="Seg_396" n="e" s="T40">tond </ts>
               <ts e="T42" id="Seg_398" n="e" s="T41">ütetko </ts>
               <ts e="T43" id="Seg_400" n="e" s="T42">naj </ts>
               <ts e="T44" id="Seg_402" n="e" s="T43">aː </ts>
               <ts e="T45" id="Seg_404" n="e" s="T44">qojah. </ts>
               <ts e="T46" id="Seg_406" n="e" s="T45">Na </ts>
               <ts e="T47" id="Seg_408" n="e" s="T46">tu </ts>
               <ts e="T48" id="Seg_410" n="e" s="T47">nʼärt </ts>
               <ts e="T49" id="Seg_412" n="e" s="T48">kojoɣot </ts>
               <ts e="T50" id="Seg_414" n="e" s="T49">eːk. </ts>
               <ts e="T51" id="Seg_416" n="e" s="T50">Nʼäroɣɨn </ts>
               <ts e="T52" id="Seg_418" n="e" s="T51">eja </ts>
               <ts e="T53" id="Seg_420" n="e" s="T52">na </ts>
               <ts e="T54" id="Seg_422" n="e" s="T53">tu, </ts>
               <ts e="T55" id="Seg_424" n="e" s="T54">okɨr </ts>
               <ts e="T56" id="Seg_426" n="e" s="T55">kaːdarɣɨnt </ts>
               <ts e="T57" id="Seg_428" n="e" s="T56">tɨtɨl </ts>
               <ts e="T58" id="Seg_430" n="e" s="T57">olkoː. </ts>
               <ts e="T59" id="Seg_432" n="e" s="T58">To </ts>
               <ts e="T60" id="Seg_434" n="e" s="T59">haːlgə </ts>
               <ts e="T61" id="Seg_436" n="e" s="T60">uruk </ts>
               <ts e="T62" id="Seg_438" n="e" s="T61">korreːja. </ts>
               <ts e="T63" id="Seg_440" n="e" s="T62">Na </ts>
               <ts e="T64" id="Seg_442" n="e" s="T63">tokot </ts>
               <ts e="T65" id="Seg_444" n="e" s="T64">warga </ts>
               <ts e="T66" id="Seg_446" n="e" s="T65">warɣə </ts>
               <ts e="T67" id="Seg_448" n="e" s="T66">pitča, </ts>
               <ts e="T68" id="Seg_450" n="e" s="T67">qunando </ts>
               <ts e="T69" id="Seg_452" n="e" s="T68">warkə, </ts>
               <ts e="T70" id="Seg_454" n="e" s="T69">piča. </ts>
               <ts e="T71" id="Seg_456" n="e" s="T70">Warwarwa </ts>
               <ts e="T72" id="Seg_458" n="e" s="T71">paja </ts>
               <ts e="T73" id="Seg_460" n="e" s="T72">čeːnča, </ts>
               <ts e="T74" id="Seg_462" n="e" s="T73">šandə </ts>
               <ts e="T75" id="Seg_464" n="e" s="T74">kɨba </ts>
               <ts e="T76" id="Seg_466" n="e" s="T75">andee </ts>
               <ts e="T77" id="Seg_468" n="e" s="T76">kojamban, </ts>
               <ts e="T78" id="Seg_470" n="e" s="T77">piča </ts>
               <ts e="T79" id="Seg_472" n="e" s="T78">na </ts>
               <ts e="T80" id="Seg_474" n="e" s="T79">kup </ts>
               <ts e="T81" id="Seg_476" n="e" s="T80">oralbat </ts>
               <ts e="T82" id="Seg_478" n="e" s="T81">i </ts>
               <ts e="T83" id="Seg_480" n="e" s="T82">qup </ts>
               <ts e="T84" id="Seg_482" n="e" s="T83">kalʼembat </ts>
               <ts e="T85" id="Seg_484" n="e" s="T84">piča </ts>
               <ts e="T86" id="Seg_486" n="e" s="T85">oralbat </ts>
               <ts e="T87" id="Seg_488" n="e" s="T86">polgu </ts>
               <ts e="T88" id="Seg_490" n="e" s="T87">kɨga. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_491" s="T0">KFN_1965_HumanSizedPike_flk.001 (001.001)</ta>
            <ta e="T7" id="Seg_492" s="T3">KFN_1965_HumanSizedPike_flk.002 (001.002)</ta>
            <ta e="T15" id="Seg_493" s="T7">KFN_1965_HumanSizedPike_flk.003 (001.003)</ta>
            <ta e="T20" id="Seg_494" s="T15">KFN_1965_HumanSizedPike_flk.004 (001.004)</ta>
            <ta e="T32" id="Seg_495" s="T20">KFN_1965_HumanSizedPike_flk.005 (001.005)</ta>
            <ta e="T35" id="Seg_496" s="T32">KFN_1965_HumanSizedPike_flk.006 (001.006)</ta>
            <ta e="T39" id="Seg_497" s="T35">KFN_1965_HumanSizedPike_flk.007 (001.007)</ta>
            <ta e="T45" id="Seg_498" s="T39">KFN_1965_HumanSizedPike_flk.008 (001.008)</ta>
            <ta e="T50" id="Seg_499" s="T45">KFN_1965_HumanSizedPike_flk.009 (001.009)</ta>
            <ta e="T58" id="Seg_500" s="T50">KFN_1965_HumanSizedPike_flk.010 (001.010)</ta>
            <ta e="T62" id="Seg_501" s="T58">KFN_1965_HumanSizedPike_flk.011 (001.011)</ta>
            <ta e="T70" id="Seg_502" s="T62">KFN_1965_HumanSizedPike_flk.012 (001.012)</ta>
            <ta e="T88" id="Seg_503" s="T70">KFN_1965_HumanSizedPike_flk.013 (001.013)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_504" s="T0">kунандо варкъ пич</ta>
            <ta e="T7" id="Seg_505" s="T3">поlта ′тамыт ту ēнда.</ta>
            <ta e="T15" id="Seg_506" s="T7">′kандеɣыт ′оlгом мо′релʼишпад ӱнда кутарк ′тӱlдʼе таре ченда.</ta>
            <ta e="T20" id="Seg_507" s="T15">ми шедъkут на тонд.</ta>
            <ta e="T32" id="Seg_508" s="T20">оl′ɣо вес мо′релʼба, нын′до мӣ варгай, кажный чеl ӱн′да kaйда на′чат ка′jак</ta>
            <ta e="T35" id="Seg_509" s="T32">у′lɣо вес таkпигелдʼжембак</ta>
            <ta e="T39" id="Seg_510" s="T35">мат па′jам у′рук ′lарымбы</ta>
            <ta e="T45" id="Seg_511" s="T39">табъ тонд ′ӱтетко най ā kоjаk.</ta>
            <ta e="T50" id="Seg_512" s="T45">на ту нʼäрт ко′jоɣот ēк.</ta>
            <ta e="T58" id="Seg_513" s="T50">нʼäроɣын еjа на ту, ′окыр ′kāдарынт тытыл оl′кō.</ta>
            <ta e="T62" id="Seg_514" s="T58">то ′hālгъ у′рук корр′ējа</ta>
            <ta e="T70" id="Seg_515" s="T62">на ′токот вар′га варɣъ пит′ча, kунандо варкъ, пи′ча</ta>
            <ta e="T88" id="Seg_516" s="T70">Варварва па′jа ′чēнча, шандъ к′ыба анд′ē kо′jамбан, пи′ча на kуп о′ралбат и kуп ′олʼембат, пи′ча о′ралбат ′полгу кыга</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T3" id="Seg_517" s="T0">qunando warkə pič</ta>
            <ta e="T7" id="Seg_518" s="T3">Polta tamɨt tu eːnda.</ta>
            <ta e="T15" id="Seg_519" s="T7">qandeɣɨt olgom morelʼišpad, ünda, kutark tüldʼe tare čenda.</ta>
            <ta e="T20" id="Seg_520" s="T15">mi šedəqut töwaj na tond.</ta>
            <ta e="T32" id="Seg_521" s="T20">olɣo wes morelʼba, nɨndo miː wargaj, kaʒnɨj čeːl ünda, qajda načat kajak</ta>
            <ta e="T35" id="Seg_522" s="T32">ulɣo wes taqpigelǯʼembak.</ta>
            <ta e="T39" id="Seg_523" s="T35">mat pajam uruk larɨmbɨ.</ta>
            <ta e="T45" id="Seg_524" s="T39">tabə tond ütetko naj aː ko′jak.</ta>
            <ta e="T50" id="Seg_525" s="T45">na tu nʼärt kojoɣot eːk.</ta>
            <ta e="T58" id="Seg_526" s="T50">nʼäroɣɨn eja na tu, okɨr qaːdarɣɨnt tɨtɨl olkoː.</ta>
            <ta e="T62" id="Seg_527" s="T58">to haːlgə uruk korreːja.</ta>
            <ta e="T70" id="Seg_528" s="T62">na tokot warga warɣə pitča, qunando warkə, piča.</ta>
            <ta e="T88" id="Seg_529" s="T70">Warwarwa paja čeːnča, šandə kɨba andeː qojamban, piča na qup oralbat i qup qolʼembat, piča oralbat polgu kɨga.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_530" s="T0">Qunando warkə pič. </ta>
            <ta e="T7" id="Seg_531" s="T3">Polta tamɨt tu eːnda. </ta>
            <ta e="T15" id="Seg_532" s="T7">Qandeqɨt olgom morelʼišpad, ünda, kutark tüldʼe tare čenda. </ta>
            <ta e="T20" id="Seg_533" s="T15">Mi šedəqut töwaj na tond. </ta>
            <ta e="T32" id="Seg_534" s="T20">Olɣo wes morelʼba, nɨndo miː wargaj, kaʒnɨj čeːl ünda, qajda načat kajak. </ta>
            <ta e="T35" id="Seg_535" s="T32">Ulɣo wes taqpigelǯʼembak. </ta>
            <ta e="T39" id="Seg_536" s="T35">Mat pajam uruk larɨmbɨ. </ta>
            <ta e="T45" id="Seg_537" s="T39">Tabə tond ütetko naj aː qojah. </ta>
            <ta e="T50" id="Seg_538" s="T45">Na tu nʼärt kojoɣot eːk. </ta>
            <ta e="T58" id="Seg_539" s="T50">Nʼäroɣɨn eja na tu, okɨr kaːdarɣɨnt tɨtɨl olkoː. </ta>
            <ta e="T62" id="Seg_540" s="T58">To haːlgə uruk korreːja. </ta>
            <ta e="T70" id="Seg_541" s="T62">Na tokot warga warɣə pitča, qunando warkə, piča. </ta>
            <ta e="T88" id="Seg_542" s="T70">Warwarwa paja čeːnča, šandə kɨba andee kojamban, piča na kup oralbat i qup kalʼembat piča oralbat polgu kɨga. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_543" s="T0">qu-nando</ta>
            <ta e="T2" id="Seg_544" s="T1">warkə</ta>
            <ta e="T3" id="Seg_545" s="T2">pič</ta>
            <ta e="T4" id="Seg_546" s="T3">Polta</ta>
            <ta e="T5" id="Seg_547" s="T4">ta-mɨt</ta>
            <ta e="T6" id="Seg_548" s="T5">tu</ta>
            <ta e="T7" id="Seg_549" s="T6">eː-ndɨ</ta>
            <ta e="T8" id="Seg_550" s="T7">qande-qɨt</ta>
            <ta e="T9" id="Seg_551" s="T8">olgo-m</ta>
            <ta e="T10" id="Seg_552" s="T9">more-lʼi-špa-d</ta>
            <ta e="T11" id="Seg_553" s="T10">ünda</ta>
            <ta e="T12" id="Seg_554" s="T11">kutark</ta>
            <ta e="T13" id="Seg_555" s="T12">tüldʼe</ta>
            <ta e="T14" id="Seg_556" s="T13">tare</ta>
            <ta e="T15" id="Seg_557" s="T14">čenda</ta>
            <ta e="T16" id="Seg_558" s="T15">mi</ta>
            <ta e="T17" id="Seg_559" s="T16">šedə-qut</ta>
            <ta e="T18" id="Seg_560" s="T17">tö-wa-j</ta>
            <ta e="T19" id="Seg_561" s="T18">na</ta>
            <ta e="T20" id="Seg_562" s="T19">to-nd</ta>
            <ta e="T21" id="Seg_563" s="T20">olɣo</ta>
            <ta e="T22" id="Seg_564" s="T21">wes</ta>
            <ta e="T23" id="Seg_565" s="T22">more-lʼ-ba</ta>
            <ta e="T24" id="Seg_566" s="T23">nɨndo</ta>
            <ta e="T25" id="Seg_567" s="T24">miː</ta>
            <ta e="T26" id="Seg_568" s="T25">warga-j</ta>
            <ta e="T27" id="Seg_569" s="T26">kaʒnɨj</ta>
            <ta e="T28" id="Seg_570" s="T27">čeːl</ta>
            <ta e="T29" id="Seg_571" s="T28">ünda</ta>
            <ta e="T30" id="Seg_572" s="T29">qaj-da</ta>
            <ta e="T31" id="Seg_573" s="T30">nača-t</ta>
            <ta e="T32" id="Seg_574" s="T31">kaja-k</ta>
            <ta e="T33" id="Seg_575" s="T32">ulɣo</ta>
            <ta e="T34" id="Seg_576" s="T33">wes</ta>
            <ta e="T35" id="Seg_577" s="T34">taqpige-lǯʼe-mba-k</ta>
            <ta e="T36" id="Seg_578" s="T35">mat</ta>
            <ta e="T37" id="Seg_579" s="T36">paja-m</ta>
            <ta e="T38" id="Seg_580" s="T37">ur-u-k</ta>
            <ta e="T39" id="Seg_581" s="T38">larɨ-mbɨ</ta>
            <ta e="T40" id="Seg_582" s="T39">tabə</ta>
            <ta e="T41" id="Seg_583" s="T40">to-nd</ta>
            <ta e="T42" id="Seg_584" s="T41">üt-e-tqo</ta>
            <ta e="T43" id="Seg_585" s="T42">naj</ta>
            <ta e="T44" id="Seg_586" s="T43">aː</ta>
            <ta e="T45" id="Seg_587" s="T44">qoja-h</ta>
            <ta e="T46" id="Seg_588" s="T45">na</ta>
            <ta e="T47" id="Seg_589" s="T46">tu</ta>
            <ta e="T48" id="Seg_590" s="T47">nʼär-t</ta>
            <ta e="T49" id="Seg_591" s="T48">kojo-ɣot</ta>
            <ta e="T50" id="Seg_592" s="T49">eː-k</ta>
            <ta e="T51" id="Seg_593" s="T50">nʼär-o-ɣɨn</ta>
            <ta e="T52" id="Seg_594" s="T51">e-ja</ta>
            <ta e="T53" id="Seg_595" s="T52">na</ta>
            <ta e="T54" id="Seg_596" s="T53">tu</ta>
            <ta e="T55" id="Seg_597" s="T54">okɨr</ta>
            <ta e="T56" id="Seg_598" s="T55">kaːdar-ɣɨnt</ta>
            <ta e="T57" id="Seg_599" s="T56">tɨtɨ-lʼ</ta>
            <ta e="T58" id="Seg_600" s="T57">olko</ta>
            <ta e="T59" id="Seg_601" s="T58">to</ta>
            <ta e="T60" id="Seg_602" s="T59">haːlgə</ta>
            <ta e="T61" id="Seg_603" s="T60">ur-u-k</ta>
            <ta e="T62" id="Seg_604" s="T61">korreː-ja</ta>
            <ta e="T63" id="Seg_605" s="T62">na</ta>
            <ta e="T64" id="Seg_606" s="T63">to-kot</ta>
            <ta e="T65" id="Seg_607" s="T64">warga</ta>
            <ta e="T66" id="Seg_608" s="T65">warɣə</ta>
            <ta e="T67" id="Seg_609" s="T66">pitča</ta>
            <ta e="T68" id="Seg_610" s="T67">qu-nando</ta>
            <ta e="T69" id="Seg_611" s="T68">warkə</ta>
            <ta e="T70" id="Seg_612" s="T69">piča</ta>
            <ta e="T71" id="Seg_613" s="T70">Warwarwa</ta>
            <ta e="T72" id="Seg_614" s="T71">paja</ta>
            <ta e="T73" id="Seg_615" s="T72">čeːnča</ta>
            <ta e="T74" id="Seg_616" s="T73">šandə</ta>
            <ta e="T75" id="Seg_617" s="T74">kɨba</ta>
            <ta e="T76" id="Seg_618" s="T75">ande-e</ta>
            <ta e="T77" id="Seg_619" s="T76">koja-mba-n</ta>
            <ta e="T78" id="Seg_620" s="T77">piča</ta>
            <ta e="T79" id="Seg_621" s="T78">na</ta>
            <ta e="T80" id="Seg_622" s="T79">kup</ta>
            <ta e="T81" id="Seg_623" s="T80">ora-lə-ba-t</ta>
            <ta e="T82" id="Seg_624" s="T81">i</ta>
            <ta e="T83" id="Seg_625" s="T82">qup</ta>
            <ta e="T84" id="Seg_626" s="T83">kalʼe-mba-t</ta>
            <ta e="T85" id="Seg_627" s="T84">piča</ta>
            <ta e="T86" id="Seg_628" s="T85">ora-lə-ba-t</ta>
            <ta e="T87" id="Seg_629" s="T86">pol-gu</ta>
            <ta e="T88" id="Seg_630" s="T87">kɨga</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_631" s="T0">qum-nando</ta>
            <ta e="T2" id="Seg_632" s="T1">wargɨ</ta>
            <ta e="T3" id="Seg_633" s="T2">piča</ta>
            <ta e="T4" id="Seg_634" s="T3">Polta</ta>
            <ta e="T5" id="Seg_635" s="T4">ta-mun</ta>
            <ta e="T6" id="Seg_636" s="T5">to</ta>
            <ta e="T7" id="Seg_637" s="T6">e-ndɨ</ta>
            <ta e="T8" id="Seg_638" s="T7">qande-qɨn</ta>
            <ta e="T9" id="Seg_639" s="T8">olgo-m</ta>
            <ta e="T10" id="Seg_640" s="T9">more-lə-špɨ-tɨ</ta>
            <ta e="T11" id="Seg_641" s="T10">undɛ</ta>
            <ta e="T12" id="Seg_642" s="T11">kutark</ta>
            <ta e="T13" id="Seg_643" s="T12">tüːlʼde</ta>
            <ta e="T14" id="Seg_644" s="T13">tare</ta>
            <ta e="T15" id="Seg_645" s="T14">čendɨ</ta>
            <ta e="T16" id="Seg_646" s="T15">mi</ta>
            <ta e="T17" id="Seg_647" s="T16">šitə-qum</ta>
            <ta e="T18" id="Seg_648" s="T17">töː-wa-j</ta>
            <ta e="T19" id="Seg_649" s="T18">na</ta>
            <ta e="T20" id="Seg_650" s="T19">to-nde</ta>
            <ta e="T21" id="Seg_651" s="T20">olgo</ta>
            <ta e="T22" id="Seg_652" s="T21">wesʼ</ta>
            <ta e="T23" id="Seg_653" s="T22">more-lə-mbɨ</ta>
            <ta e="T24" id="Seg_654" s="T23">nɨndɨ</ta>
            <ta e="T25" id="Seg_655" s="T24">mi</ta>
            <ta e="T26" id="Seg_656" s="T25">wargɨ-j</ta>
            <ta e="T27" id="Seg_657" s="T26">kaʒnɨj</ta>
            <ta e="T28" id="Seg_658" s="T27">čeːl</ta>
            <ta e="T29" id="Seg_659" s="T28">undɛ</ta>
            <ta e="T30" id="Seg_660" s="T29">qaj-da</ta>
            <ta e="T31" id="Seg_661" s="T30">nača-tɨ</ta>
            <ta e="T32" id="Seg_662" s="T31">koja-k</ta>
            <ta e="T33" id="Seg_663" s="T32">olgo</ta>
            <ta e="T34" id="Seg_664" s="T33">wesʼ</ta>
            <ta e="T35" id="Seg_665" s="T34">takpige-lʼčǝ-mbɨ-k</ta>
            <ta e="T36" id="Seg_666" s="T35">man</ta>
            <ta e="T37" id="Seg_667" s="T36">paja-mɨ</ta>
            <ta e="T38" id="Seg_668" s="T37">or-ɨ-k</ta>
            <ta e="T39" id="Seg_669" s="T38">larɨ-mbɨ</ta>
            <ta e="T40" id="Seg_670" s="T39">tab</ta>
            <ta e="T41" id="Seg_671" s="T40">to-nde</ta>
            <ta e="T42" id="Seg_672" s="T41">üt-ɨ-tqo</ta>
            <ta e="T43" id="Seg_673" s="T42">naj</ta>
            <ta e="T44" id="Seg_674" s="T43">aː</ta>
            <ta e="T45" id="Seg_675" s="T44">koja-k</ta>
            <ta e="T46" id="Seg_676" s="T45">na</ta>
            <ta e="T47" id="Seg_677" s="T46">to</ta>
            <ta e="T48" id="Seg_678" s="T47">nʼar-n</ta>
            <ta e="T49" id="Seg_679" s="T48">kojo-qɨn</ta>
            <ta e="T50" id="Seg_680" s="T49">e-k</ta>
            <ta e="T51" id="Seg_681" s="T50">nʼar-ɨ-qɨn</ta>
            <ta e="T52" id="Seg_682" s="T51">e-ŋɨ</ta>
            <ta e="T53" id="Seg_683" s="T52">na</ta>
            <ta e="T54" id="Seg_684" s="T53">to</ta>
            <ta e="T55" id="Seg_685" s="T54">okkər</ta>
            <ta e="T56" id="Seg_686" s="T55">kaːdar-qɨnt</ta>
            <ta e="T57" id="Seg_687" s="T56">tɨtɨ-lʼ</ta>
            <ta e="T58" id="Seg_688" s="T57">olko</ta>
            <ta e="T59" id="Seg_689" s="T58">to</ta>
            <ta e="T60" id="Seg_690" s="T59">halge</ta>
            <ta e="T61" id="Seg_691" s="T60">or-ɨ-k</ta>
            <ta e="T62" id="Seg_692" s="T61">korreː-ŋɨ</ta>
            <ta e="T63" id="Seg_693" s="T62">na</ta>
            <ta e="T64" id="Seg_694" s="T63">to-qɨn</ta>
            <ta e="T65" id="Seg_695" s="T64">wargɨ</ta>
            <ta e="T66" id="Seg_696" s="T65">wargɨ</ta>
            <ta e="T67" id="Seg_697" s="T66">piča</ta>
            <ta e="T68" id="Seg_698" s="T67">qum-nando</ta>
            <ta e="T69" id="Seg_699" s="T68">wargɨ</ta>
            <ta e="T70" id="Seg_700" s="T69">piča</ta>
            <ta e="T71" id="Seg_701" s="T70">Warwara</ta>
            <ta e="T72" id="Seg_702" s="T71">paja</ta>
            <ta e="T73" id="Seg_703" s="T72">čenčɨ</ta>
            <ta e="T74" id="Seg_704" s="T73">šandɛ</ta>
            <ta e="T75" id="Seg_705" s="T74">kɨba</ta>
            <ta e="T76" id="Seg_706" s="T75">ande-se</ta>
            <ta e="T77" id="Seg_707" s="T76">koja-mbɨ-n</ta>
            <ta e="T78" id="Seg_708" s="T77">piča</ta>
            <ta e="T79" id="Seg_709" s="T78">na</ta>
            <ta e="T80" id="Seg_710" s="T79">kum</ta>
            <ta e="T81" id="Seg_711" s="T80">ora-lə-mbɨ-tɨ</ta>
            <ta e="T82" id="Seg_712" s="T81">i</ta>
            <ta e="T83" id="Seg_713" s="T82">qum</ta>
            <ta e="T84" id="Seg_714" s="T83">qalɨ-mbɨ-tɨ</ta>
            <ta e="T85" id="Seg_715" s="T84">piča</ta>
            <ta e="T86" id="Seg_716" s="T85">ora-lə-mbɨ-tɨ</ta>
            <ta e="T87" id="Seg_717" s="T86">pol-gu</ta>
            <ta e="T88" id="Seg_718" s="T87">kɨge</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_719" s="T0">human.being-ABL</ta>
            <ta e="T2" id="Seg_720" s="T1">big</ta>
            <ta e="T3" id="Seg_721" s="T2">pike.[NOM]</ta>
            <ta e="T4" id="Seg_722" s="T3">Polta</ta>
            <ta e="T5" id="Seg_723" s="T4">on.other.side-PROL</ta>
            <ta e="T6" id="Seg_724" s="T5">lake.[NOM]</ta>
            <ta e="T7" id="Seg_725" s="T6">be-INFER.[3SG.S]</ta>
            <ta e="T8" id="Seg_726" s="T7">autumn-LOC</ta>
            <ta e="T9" id="Seg_727" s="T8">ice-ACC</ta>
            <ta e="T10" id="Seg_728" s="T9">break-INCH-IPFV2-3SG.O</ta>
            <ta e="T11" id="Seg_729" s="T10">hear.[3SG.S]</ta>
            <ta e="T12" id="Seg_730" s="T11">how</ta>
            <ta e="T13" id="Seg_731" s="T12">rifle.[NOM]</ta>
            <ta e="T14" id="Seg_732" s="T13">like</ta>
            <ta e="T15" id="Seg_733" s="T14">shoot.[3SG.S]</ta>
            <ta e="T16" id="Seg_734" s="T15">we.DU.NOM</ta>
            <ta e="T17" id="Seg_735" s="T16">two-human.being.[NOM]</ta>
            <ta e="T18" id="Seg_736" s="T17">come-CO-1DU</ta>
            <ta e="T19" id="Seg_737" s="T18">this</ta>
            <ta e="T20" id="Seg_738" s="T19">lake-ILL</ta>
            <ta e="T21" id="Seg_739" s="T20">ice.[NOM]</ta>
            <ta e="T22" id="Seg_740" s="T21">all</ta>
            <ta e="T23" id="Seg_741" s="T22">break-INCH-PST.NAR.[3SG.S]</ta>
            <ta e="T24" id="Seg_742" s="T23">here</ta>
            <ta e="T25" id="Seg_743" s="T24">we.DU.NOM</ta>
            <ta e="T26" id="Seg_744" s="T25">live-1DU</ta>
            <ta e="T27" id="Seg_745" s="T26">every</ta>
            <ta e="T28" id="Seg_746" s="T27">day.[NOM]</ta>
            <ta e="T29" id="Seg_747" s="T28">hear.[3SG.S]</ta>
            <ta e="T30" id="Seg_748" s="T29">what-INDEF.[NOM]</ta>
            <ta e="T31" id="Seg_749" s="T30">there-ADV.LOC</ta>
            <ta e="T32" id="Seg_750" s="T31">go-3SG.S</ta>
            <ta e="T33" id="Seg_751" s="T32">ice.[NOM]</ta>
            <ta e="T34" id="Seg_752" s="T33">all</ta>
            <ta e="T35" id="Seg_753" s="T34">%%-PFV-PST.NAR-3SG.S</ta>
            <ta e="T36" id="Seg_754" s="T35">I.GEN</ta>
            <ta e="T37" id="Seg_755" s="T36">wife-1SG</ta>
            <ta e="T38" id="Seg_756" s="T37">force-EP-ADVZ</ta>
            <ta e="T39" id="Seg_757" s="T38">be.afraid-PST.NAR.[3SG.S]</ta>
            <ta e="T40" id="Seg_758" s="T39">(s)he</ta>
            <ta e="T41" id="Seg_759" s="T40">lake-ILL</ta>
            <ta e="T42" id="Seg_760" s="T41">water-EP-TRL</ta>
            <ta e="T43" id="Seg_761" s="T42">also</ta>
            <ta e="T44" id="Seg_762" s="T43">NEG</ta>
            <ta e="T45" id="Seg_763" s="T44">go-3SG.S</ta>
            <ta e="T46" id="Seg_764" s="T45">this</ta>
            <ta e="T47" id="Seg_765" s="T46">lake.[NOM]</ta>
            <ta e="T48" id="Seg_766" s="T47">swamp-GEN</ta>
            <ta e="T49" id="Seg_767" s="T48">near-LOC</ta>
            <ta e="T50" id="Seg_768" s="T49">be-3SG.S</ta>
            <ta e="T51" id="Seg_769" s="T50">swamp-EP-LOC</ta>
            <ta e="T52" id="Seg_770" s="T51">be-CO.[3SG.S]</ta>
            <ta e="T53" id="Seg_771" s="T52">this</ta>
            <ta e="T54" id="Seg_772" s="T53">lake.[NOM]</ta>
            <ta e="T55" id="Seg_773" s="T54">one</ta>
            <ta e="T56" id="Seg_774" s="T55">side-LOC.3SG</ta>
            <ta e="T57" id="Seg_775" s="T56">cedar-ADJZ</ta>
            <ta e="T58" id="Seg_776" s="T57">island.[NOM]</ta>
            <ta e="T59" id="Seg_777" s="T58">lake.[NOM]</ta>
            <ta e="T60" id="Seg_778" s="T59">middle.[NOM]</ta>
            <ta e="T61" id="Seg_779" s="T60">force-EP-ADVZ</ta>
            <ta e="T62" id="Seg_780" s="T61">deep-CO.[3SG.S]</ta>
            <ta e="T63" id="Seg_781" s="T62">this</ta>
            <ta e="T64" id="Seg_782" s="T63">lake-LOC</ta>
            <ta e="T65" id="Seg_783" s="T64">live.[3SG.S]</ta>
            <ta e="T66" id="Seg_784" s="T65">big.[NOM]</ta>
            <ta e="T67" id="Seg_785" s="T66">pike.[NOM]</ta>
            <ta e="T68" id="Seg_786" s="T67">human.being-ABL</ta>
            <ta e="T69" id="Seg_787" s="T68">big</ta>
            <ta e="T70" id="Seg_788" s="T69">pike.[NOM]</ta>
            <ta e="T71" id="Seg_789" s="T70">Warwara.[NOM]</ta>
            <ta e="T72" id="Seg_790" s="T71">old.woman.[NOM]</ta>
            <ta e="T73" id="Seg_791" s="T72">say.[3SG.S]</ta>
            <ta e="T74" id="Seg_792" s="T73">young</ta>
            <ta e="T75" id="Seg_793" s="T74">small</ta>
            <ta e="T76" id="Seg_794" s="T75">boat-INSTR</ta>
            <ta e="T77" id="Seg_795" s="T76">go-PST.NAR-3SG.S</ta>
            <ta e="T78" id="Seg_796" s="T77">pike.[NOM]</ta>
            <ta e="T79" id="Seg_797" s="T78">this</ta>
            <ta e="T80" id="Seg_798" s="T79">human.being.[NOM]</ta>
            <ta e="T81" id="Seg_799" s="T80">hold-INCH-PST.NAR-3SG.O</ta>
            <ta e="T82" id="Seg_800" s="T81">and</ta>
            <ta e="T83" id="Seg_801" s="T82">human.being.[NOM]</ta>
            <ta e="T84" id="Seg_802" s="T83">stay-PST.NAR-3SG.O</ta>
            <ta e="T85" id="Seg_803" s="T84">pike.[NOM]</ta>
            <ta e="T86" id="Seg_804" s="T85">hold-INCH-PST.NAR-3SG.O</ta>
            <ta e="T87" id="Seg_805" s="T86">swallow-INF</ta>
            <ta e="T88" id="Seg_806" s="T87">want.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_807" s="T0">человек-ABL</ta>
            <ta e="T2" id="Seg_808" s="T1">большой</ta>
            <ta e="T3" id="Seg_809" s="T2">щука.[NOM]</ta>
            <ta e="T4" id="Seg_810" s="T3">Польто</ta>
            <ta e="T5" id="Seg_811" s="T4">на.другую.сторону-PROL</ta>
            <ta e="T6" id="Seg_812" s="T5">озеро.[NOM]</ta>
            <ta e="T7" id="Seg_813" s="T6">быть-INFER.[3SG.S]</ta>
            <ta e="T8" id="Seg_814" s="T7">осень-LOC</ta>
            <ta e="T9" id="Seg_815" s="T8">лёд-ACC</ta>
            <ta e="T10" id="Seg_816" s="T9">сломать-INCH-IPFV2-3SG.O</ta>
            <ta e="T11" id="Seg_817" s="T10">слышать.[3SG.S]</ta>
            <ta e="T12" id="Seg_818" s="T11">как</ta>
            <ta e="T13" id="Seg_819" s="T12">ружье.[NOM]</ta>
            <ta e="T14" id="Seg_820" s="T13">как</ta>
            <ta e="T15" id="Seg_821" s="T14">стрелять.[3SG.S]</ta>
            <ta e="T16" id="Seg_822" s="T15">мы.DU.NOM</ta>
            <ta e="T17" id="Seg_823" s="T16">два-человек.[NOM]</ta>
            <ta e="T18" id="Seg_824" s="T17">прийти-CO-1DU</ta>
            <ta e="T19" id="Seg_825" s="T18">этот</ta>
            <ta e="T20" id="Seg_826" s="T19">озеро-ILL</ta>
            <ta e="T21" id="Seg_827" s="T20">лёд.[NOM]</ta>
            <ta e="T22" id="Seg_828" s="T21">весь</ta>
            <ta e="T23" id="Seg_829" s="T22">сломать-INCH-PST.NAR.[3SG.S]</ta>
            <ta e="T24" id="Seg_830" s="T23">здесь</ta>
            <ta e="T25" id="Seg_831" s="T24">мы.DU.NOM</ta>
            <ta e="T26" id="Seg_832" s="T25">жить-1DU</ta>
            <ta e="T27" id="Seg_833" s="T26">каждый</ta>
            <ta e="T28" id="Seg_834" s="T27">день.[NOM]</ta>
            <ta e="T29" id="Seg_835" s="T28">слышать.[3SG.S]</ta>
            <ta e="T30" id="Seg_836" s="T29">что-INDEF.[NOM]</ta>
            <ta e="T31" id="Seg_837" s="T30">туда-ADV.LOC</ta>
            <ta e="T32" id="Seg_838" s="T31">ходить-3SG.S</ta>
            <ta e="T33" id="Seg_839" s="T32">лёд.[NOM]</ta>
            <ta e="T34" id="Seg_840" s="T33">весь</ta>
            <ta e="T35" id="Seg_841" s="T34">%%-PFV-PST.NAR-3SG.S</ta>
            <ta e="T36" id="Seg_842" s="T35">я.GEN</ta>
            <ta e="T37" id="Seg_843" s="T36">жена-1SG</ta>
            <ta e="T38" id="Seg_844" s="T37">сила-EP-ADVZ</ta>
            <ta e="T39" id="Seg_845" s="T38">бояться-PST.NAR.[3SG.S]</ta>
            <ta e="T40" id="Seg_846" s="T39">он(а)</ta>
            <ta e="T41" id="Seg_847" s="T40">озеро-ILL</ta>
            <ta e="T42" id="Seg_848" s="T41">вода-EP-TRL</ta>
            <ta e="T43" id="Seg_849" s="T42">тоже</ta>
            <ta e="T44" id="Seg_850" s="T43">NEG</ta>
            <ta e="T45" id="Seg_851" s="T44">ходить-3SG.S</ta>
            <ta e="T46" id="Seg_852" s="T45">этот</ta>
            <ta e="T47" id="Seg_853" s="T46">озеро.[NOM]</ta>
            <ta e="T48" id="Seg_854" s="T47">болото-GEN</ta>
            <ta e="T49" id="Seg_855" s="T48">рядом-LOC</ta>
            <ta e="T50" id="Seg_856" s="T49">быть-3SG.S</ta>
            <ta e="T51" id="Seg_857" s="T50">болото-EP-LOC</ta>
            <ta e="T52" id="Seg_858" s="T51">быть-CO.[3SG.S]</ta>
            <ta e="T53" id="Seg_859" s="T52">этот</ta>
            <ta e="T54" id="Seg_860" s="T53">озеро.[NOM]</ta>
            <ta e="T55" id="Seg_861" s="T54">один</ta>
            <ta e="T56" id="Seg_862" s="T55">сторона-LOC.3SG</ta>
            <ta e="T57" id="Seg_863" s="T56">кедр-ADJZ</ta>
            <ta e="T58" id="Seg_864" s="T57">остров.[NOM]</ta>
            <ta e="T59" id="Seg_865" s="T58">озеро.[NOM]</ta>
            <ta e="T60" id="Seg_866" s="T59">середина.[NOM]</ta>
            <ta e="T61" id="Seg_867" s="T60">сила-EP-ADVZ</ta>
            <ta e="T62" id="Seg_868" s="T61">глубокий-CO.[3SG.S]</ta>
            <ta e="T63" id="Seg_869" s="T62">этот</ta>
            <ta e="T64" id="Seg_870" s="T63">озеро-LOC</ta>
            <ta e="T65" id="Seg_871" s="T64">жить.[3SG.S]</ta>
            <ta e="T66" id="Seg_872" s="T65">большой.[NOM]</ta>
            <ta e="T67" id="Seg_873" s="T66">щука.[NOM]</ta>
            <ta e="T68" id="Seg_874" s="T67">человек-ABL</ta>
            <ta e="T69" id="Seg_875" s="T68">большой</ta>
            <ta e="T70" id="Seg_876" s="T69">щука.[NOM]</ta>
            <ta e="T71" id="Seg_877" s="T70">Варвара.[NOM]</ta>
            <ta e="T72" id="Seg_878" s="T71">старуха.[NOM]</ta>
            <ta e="T73" id="Seg_879" s="T72">сказать.[3SG.S]</ta>
            <ta e="T74" id="Seg_880" s="T73">молодой</ta>
            <ta e="T75" id="Seg_881" s="T74">маленький</ta>
            <ta e="T76" id="Seg_882" s="T75">обласок-INSTR</ta>
            <ta e="T77" id="Seg_883" s="T76">ходить-PST.NAR-3SG.S</ta>
            <ta e="T78" id="Seg_884" s="T77">щука.[NOM]</ta>
            <ta e="T79" id="Seg_885" s="T78">этот</ta>
            <ta e="T80" id="Seg_886" s="T79">человек.[NOM]</ta>
            <ta e="T81" id="Seg_887" s="T80">держать-INCH-PST.NAR-3SG.O</ta>
            <ta e="T82" id="Seg_888" s="T81">и</ta>
            <ta e="T83" id="Seg_889" s="T82">человек.[NOM]</ta>
            <ta e="T84" id="Seg_890" s="T83">остаться-PST.NAR-3SG.O</ta>
            <ta e="T85" id="Seg_891" s="T84">щука.[NOM]</ta>
            <ta e="T86" id="Seg_892" s="T85">держать-INCH-PST.NAR-3SG.O</ta>
            <ta e="T87" id="Seg_893" s="T86">проглотить-INF</ta>
            <ta e="T88" id="Seg_894" s="T87">хотеть.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_895" s="T0">n-n:case</ta>
            <ta e="T2" id="Seg_896" s="T1">adj</ta>
            <ta e="T3" id="Seg_897" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_898" s="T3">nprop</ta>
            <ta e="T5" id="Seg_899" s="T4">adv-n:case</ta>
            <ta e="T6" id="Seg_900" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_901" s="T6">v-v:mood-v:pn</ta>
            <ta e="T8" id="Seg_902" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_903" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_904" s="T9">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T11" id="Seg_905" s="T10">v-v:pn</ta>
            <ta e="T12" id="Seg_906" s="T11">conj</ta>
            <ta e="T13" id="Seg_907" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_908" s="T13">pp</ta>
            <ta e="T15" id="Seg_909" s="T14">v-v:pn</ta>
            <ta e="T16" id="Seg_910" s="T15">pers</ta>
            <ta e="T17" id="Seg_911" s="T16">num-n-n:case</ta>
            <ta e="T18" id="Seg_912" s="T17">v-v:ins-v:pn</ta>
            <ta e="T19" id="Seg_913" s="T18">dem</ta>
            <ta e="T20" id="Seg_914" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_915" s="T20">n-n:case</ta>
            <ta e="T22" id="Seg_916" s="T21">quant</ta>
            <ta e="T23" id="Seg_917" s="T22">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_918" s="T23">adv</ta>
            <ta e="T25" id="Seg_919" s="T24">pers</ta>
            <ta e="T26" id="Seg_920" s="T25">v-v:pn</ta>
            <ta e="T27" id="Seg_921" s="T26">adj</ta>
            <ta e="T28" id="Seg_922" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_923" s="T28">v-v:pn</ta>
            <ta e="T30" id="Seg_924" s="T29">interrog-clit-n:case</ta>
            <ta e="T31" id="Seg_925" s="T30">adv-adv:case</ta>
            <ta e="T32" id="Seg_926" s="T31">v-v:pn</ta>
            <ta e="T33" id="Seg_927" s="T32">n-n:case</ta>
            <ta e="T34" id="Seg_928" s="T33">quant</ta>
            <ta e="T35" id="Seg_929" s="T34">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T36" id="Seg_930" s="T35">pers</ta>
            <ta e="T37" id="Seg_931" s="T36">n-n:poss</ta>
            <ta e="T38" id="Seg_932" s="T37">n-n:ins-adj&gt;adv</ta>
            <ta e="T39" id="Seg_933" s="T38">v-v:tense-v:pn</ta>
            <ta e="T40" id="Seg_934" s="T39">pers</ta>
            <ta e="T41" id="Seg_935" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_936" s="T41">n-n:ins-n:case</ta>
            <ta e="T43" id="Seg_937" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_938" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_939" s="T44">v-v:pn</ta>
            <ta e="T46" id="Seg_940" s="T45">dem</ta>
            <ta e="T47" id="Seg_941" s="T46">n-n:case</ta>
            <ta e="T48" id="Seg_942" s="T47">n-n:case</ta>
            <ta e="T49" id="Seg_943" s="T48">adv-n:case</ta>
            <ta e="T50" id="Seg_944" s="T49">v-v:pn</ta>
            <ta e="T51" id="Seg_945" s="T50">n-n:ins-n:case</ta>
            <ta e="T52" id="Seg_946" s="T51">v-v:ins-v:pn</ta>
            <ta e="T53" id="Seg_947" s="T52">dem</ta>
            <ta e="T54" id="Seg_948" s="T53">n-n:case</ta>
            <ta e="T55" id="Seg_949" s="T54">num</ta>
            <ta e="T56" id="Seg_950" s="T55">n-n:case.poss</ta>
            <ta e="T57" id="Seg_951" s="T56">n-n&gt;adj</ta>
            <ta e="T58" id="Seg_952" s="T57">n-n:case</ta>
            <ta e="T59" id="Seg_953" s="T58">n-n:case</ta>
            <ta e="T60" id="Seg_954" s="T59">n-n:case</ta>
            <ta e="T61" id="Seg_955" s="T60">n-n:ins-adj&gt;adv</ta>
            <ta e="T62" id="Seg_956" s="T61">adj-v:ins-v:pn</ta>
            <ta e="T63" id="Seg_957" s="T62">dem</ta>
            <ta e="T64" id="Seg_958" s="T63">n-n:case</ta>
            <ta e="T65" id="Seg_959" s="T64">v-v:pn</ta>
            <ta e="T66" id="Seg_960" s="T65">adj-n:case</ta>
            <ta e="T67" id="Seg_961" s="T66">n-n:case</ta>
            <ta e="T68" id="Seg_962" s="T67">n-n:case</ta>
            <ta e="T69" id="Seg_963" s="T68">adj</ta>
            <ta e="T70" id="Seg_964" s="T69">n-n:case</ta>
            <ta e="T71" id="Seg_965" s="T70">nprop-n:case</ta>
            <ta e="T72" id="Seg_966" s="T71">n-n:case</ta>
            <ta e="T73" id="Seg_967" s="T72">v-v:pn</ta>
            <ta e="T74" id="Seg_968" s="T73">adj</ta>
            <ta e="T75" id="Seg_969" s="T74">adj</ta>
            <ta e="T76" id="Seg_970" s="T75">n-n:case</ta>
            <ta e="T77" id="Seg_971" s="T76">v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_972" s="T77">n-n:case</ta>
            <ta e="T79" id="Seg_973" s="T78">dem</ta>
            <ta e="T80" id="Seg_974" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_975" s="T80">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T82" id="Seg_976" s="T81">conj</ta>
            <ta e="T83" id="Seg_977" s="T82">n-n:case</ta>
            <ta e="T84" id="Seg_978" s="T83">v-v:tense-v:pn</ta>
            <ta e="T85" id="Seg_979" s="T84">n-n:case</ta>
            <ta e="T86" id="Seg_980" s="T85">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T87" id="Seg_981" s="T86">v-v:inf</ta>
            <ta e="T88" id="Seg_982" s="T87">v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_983" s="T0">n</ta>
            <ta e="T2" id="Seg_984" s="T1">adj</ta>
            <ta e="T3" id="Seg_985" s="T2">n</ta>
            <ta e="T4" id="Seg_986" s="T3">nprop</ta>
            <ta e="T5" id="Seg_987" s="T4">dem</ta>
            <ta e="T6" id="Seg_988" s="T5">dem</ta>
            <ta e="T7" id="Seg_989" s="T6">v</ta>
            <ta e="T8" id="Seg_990" s="T7">n</ta>
            <ta e="T9" id="Seg_991" s="T8">n</ta>
            <ta e="T10" id="Seg_992" s="T9">v</ta>
            <ta e="T11" id="Seg_993" s="T10">v</ta>
            <ta e="T12" id="Seg_994" s="T11">conj</ta>
            <ta e="T13" id="Seg_995" s="T12">n</ta>
            <ta e="T14" id="Seg_996" s="T13">pp</ta>
            <ta e="T15" id="Seg_997" s="T14">v</ta>
            <ta e="T16" id="Seg_998" s="T15">pers</ta>
            <ta e="T17" id="Seg_999" s="T16">n</ta>
            <ta e="T18" id="Seg_1000" s="T17">v</ta>
            <ta e="T19" id="Seg_1001" s="T18">pro</ta>
            <ta e="T20" id="Seg_1002" s="T19">n</ta>
            <ta e="T21" id="Seg_1003" s="T20">n</ta>
            <ta e="T22" id="Seg_1004" s="T21">quant</ta>
            <ta e="T23" id="Seg_1005" s="T22">v</ta>
            <ta e="T24" id="Seg_1006" s="T23">adv</ta>
            <ta e="T25" id="Seg_1007" s="T24">pers</ta>
            <ta e="T26" id="Seg_1008" s="T25">v</ta>
            <ta e="T27" id="Seg_1009" s="T26">adj</ta>
            <ta e="T28" id="Seg_1010" s="T27">n</ta>
            <ta e="T29" id="Seg_1011" s="T28">v</ta>
            <ta e="T30" id="Seg_1012" s="T29">pro</ta>
            <ta e="T31" id="Seg_1013" s="T30">adv</ta>
            <ta e="T32" id="Seg_1014" s="T31">v</ta>
            <ta e="T33" id="Seg_1015" s="T32">n</ta>
            <ta e="T34" id="Seg_1016" s="T33">quant</ta>
            <ta e="T35" id="Seg_1017" s="T34">v</ta>
            <ta e="T36" id="Seg_1018" s="T35">pers</ta>
            <ta e="T37" id="Seg_1019" s="T36">n</ta>
            <ta e="T38" id="Seg_1020" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_1021" s="T38">v</ta>
            <ta e="T40" id="Seg_1022" s="T39">pers</ta>
            <ta e="T41" id="Seg_1023" s="T40">n</ta>
            <ta e="T42" id="Seg_1024" s="T41">n</ta>
            <ta e="T43" id="Seg_1025" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_1026" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_1027" s="T44">v</ta>
            <ta e="T46" id="Seg_1028" s="T45">pro</ta>
            <ta e="T47" id="Seg_1029" s="T46">dem</ta>
            <ta e="T48" id="Seg_1030" s="T47">n</ta>
            <ta e="T49" id="Seg_1031" s="T48">pp</ta>
            <ta e="T50" id="Seg_1032" s="T49">v</ta>
            <ta e="T51" id="Seg_1033" s="T50">n</ta>
            <ta e="T52" id="Seg_1034" s="T51">v</ta>
            <ta e="T53" id="Seg_1035" s="T52">pro</ta>
            <ta e="T54" id="Seg_1036" s="T53">dem</ta>
            <ta e="T55" id="Seg_1037" s="T54">num</ta>
            <ta e="T56" id="Seg_1038" s="T55">n</ta>
            <ta e="T57" id="Seg_1039" s="T56">adj</ta>
            <ta e="T58" id="Seg_1040" s="T57">n</ta>
            <ta e="T59" id="Seg_1041" s="T58">n</ta>
            <ta e="T60" id="Seg_1042" s="T59">n</ta>
            <ta e="T61" id="Seg_1043" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_1044" s="T61">adj</ta>
            <ta e="T63" id="Seg_1045" s="T62">pro</ta>
            <ta e="T64" id="Seg_1046" s="T63">n</ta>
            <ta e="T65" id="Seg_1047" s="T64">v</ta>
            <ta e="T66" id="Seg_1048" s="T65">adj</ta>
            <ta e="T67" id="Seg_1049" s="T66">n</ta>
            <ta e="T68" id="Seg_1050" s="T67">n</ta>
            <ta e="T69" id="Seg_1051" s="T68">adj</ta>
            <ta e="T70" id="Seg_1052" s="T69">n</ta>
            <ta e="T71" id="Seg_1053" s="T70">nprop</ta>
            <ta e="T72" id="Seg_1054" s="T71">n</ta>
            <ta e="T73" id="Seg_1055" s="T72">v</ta>
            <ta e="T74" id="Seg_1056" s="T73">adj</ta>
            <ta e="T75" id="Seg_1057" s="T74">n</ta>
            <ta e="T76" id="Seg_1058" s="T75">n</ta>
            <ta e="T77" id="Seg_1059" s="T76">v</ta>
            <ta e="T78" id="Seg_1060" s="T77">n</ta>
            <ta e="T79" id="Seg_1061" s="T78">pro</ta>
            <ta e="T80" id="Seg_1062" s="T79">n</ta>
            <ta e="T81" id="Seg_1063" s="T80">v</ta>
            <ta e="T82" id="Seg_1064" s="T81">conj</ta>
            <ta e="T83" id="Seg_1065" s="T82">n</ta>
            <ta e="T84" id="Seg_1066" s="T83">v</ta>
            <ta e="T85" id="Seg_1067" s="T84">n</ta>
            <ta e="T86" id="Seg_1068" s="T85">v</ta>
            <ta e="T87" id="Seg_1069" s="T86">v</ta>
            <ta e="T88" id="Seg_1070" s="T87">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_1071" s="T3">np:S</ta>
            <ta e="T7" id="Seg_1072" s="T6">v:pred</ta>
            <ta e="T9" id="Seg_1073" s="T8">np:O</ta>
            <ta e="T10" id="Seg_1074" s="T9">0.3:S v:pred</ta>
            <ta e="T11" id="Seg_1075" s="T10">0.3:S v:pred</ta>
            <ta e="T15" id="Seg_1076" s="T11">s:compl</ta>
            <ta e="T16" id="Seg_1077" s="T15">pro.h:S</ta>
            <ta e="T18" id="Seg_1078" s="T17">v:pred</ta>
            <ta e="T21" id="Seg_1079" s="T20">np:S</ta>
            <ta e="T23" id="Seg_1080" s="T22">v:pred</ta>
            <ta e="T25" id="Seg_1081" s="T24">pro.h:S</ta>
            <ta e="T26" id="Seg_1082" s="T25">v:pred</ta>
            <ta e="T29" id="Seg_1083" s="T28">0.3:S v:pred</ta>
            <ta e="T30" id="Seg_1084" s="T29">pro.h:S</ta>
            <ta e="T32" id="Seg_1085" s="T31">v:pred</ta>
            <ta e="T33" id="Seg_1086" s="T32">np:S</ta>
            <ta e="T35" id="Seg_1087" s="T34">v:pred</ta>
            <ta e="T37" id="Seg_1088" s="T36">np.h:S</ta>
            <ta e="T39" id="Seg_1089" s="T38">v:pred</ta>
            <ta e="T40" id="Seg_1090" s="T39">pro.h:S</ta>
            <ta e="T45" id="Seg_1091" s="T44">v:pred</ta>
            <ta e="T47" id="Seg_1092" s="T46">np:S</ta>
            <ta e="T50" id="Seg_1093" s="T49">v:pred</ta>
            <ta e="T52" id="Seg_1094" s="T51">v:pred</ta>
            <ta e="T54" id="Seg_1095" s="T53">np:S</ta>
            <ta e="T59" id="Seg_1096" s="T58">np:S</ta>
            <ta e="T62" id="Seg_1097" s="T61">adj:pred</ta>
            <ta e="T65" id="Seg_1098" s="T64">v:pred</ta>
            <ta e="T67" id="Seg_1099" s="T66">np:S</ta>
            <ta e="T71" id="Seg_1100" s="T70">np.h:S</ta>
            <ta e="T73" id="Seg_1101" s="T72">v:pred</ta>
            <ta e="T77" id="Seg_1102" s="T76">0.3.h:S v:pred</ta>
            <ta e="T78" id="Seg_1103" s="T77">np:S</ta>
            <ta e="T80" id="Seg_1104" s="T79">np.h:O</ta>
            <ta e="T81" id="Seg_1105" s="T80">v:pred</ta>
            <ta e="T83" id="Seg_1106" s="T82">np.h:O</ta>
            <ta e="T84" id="Seg_1107" s="T83">0.3:S v:pred</ta>
            <ta e="T85" id="Seg_1108" s="T84">np:S</ta>
            <ta e="T86" id="Seg_1109" s="T85">0.3.h:O v:pred</ta>
            <ta e="T87" id="Seg_1110" s="T86">v:O</ta>
            <ta e="T88" id="Seg_1111" s="T87">0.3:S v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T4" id="Seg_1112" s="T3">np:Th</ta>
            <ta e="T5" id="Seg_1113" s="T4">adv:L</ta>
            <ta e="T8" id="Seg_1114" s="T7">np:Time</ta>
            <ta e="T9" id="Seg_1115" s="T8">np:P</ta>
            <ta e="T10" id="Seg_1116" s="T9">0.3:A</ta>
            <ta e="T11" id="Seg_1117" s="T10">0.3.h:E</ta>
            <ta e="T16" id="Seg_1118" s="T15">pro.h:A</ta>
            <ta e="T20" id="Seg_1119" s="T19">np:G</ta>
            <ta e="T21" id="Seg_1120" s="T20">np:P</ta>
            <ta e="T24" id="Seg_1121" s="T23">adv:L</ta>
            <ta e="T25" id="Seg_1122" s="T24">pro.h:Th</ta>
            <ta e="T28" id="Seg_1123" s="T27">np:Time</ta>
            <ta e="T30" id="Seg_1124" s="T29">pro.h:A</ta>
            <ta e="T31" id="Seg_1125" s="T30">adv:L</ta>
            <ta e="T33" id="Seg_1126" s="T32">np:P</ta>
            <ta e="T36" id="Seg_1127" s="T35">pro.h:Poss</ta>
            <ta e="T37" id="Seg_1128" s="T36">np.h:E</ta>
            <ta e="T40" id="Seg_1129" s="T39">pro.h:A</ta>
            <ta e="T41" id="Seg_1130" s="T40">np:G</ta>
            <ta e="T47" id="Seg_1131" s="T46">np:Th</ta>
            <ta e="T49" id="Seg_1132" s="T48">pp:L</ta>
            <ta e="T51" id="Seg_1133" s="T50">np:L</ta>
            <ta e="T54" id="Seg_1134" s="T53">np:Th</ta>
            <ta e="T56" id="Seg_1135" s="T55">np:L</ta>
            <ta e="T58" id="Seg_1136" s="T57">np:Th</ta>
            <ta e="T59" id="Seg_1137" s="T58">np:Th</ta>
            <ta e="T64" id="Seg_1138" s="T63">np:L</ta>
            <ta e="T67" id="Seg_1139" s="T66">np:Th</ta>
            <ta e="T71" id="Seg_1140" s="T70">np.h:A</ta>
            <ta e="T76" id="Seg_1141" s="T75">np:Ins</ta>
            <ta e="T77" id="Seg_1142" s="T76">0.3.h:A</ta>
            <ta e="T78" id="Seg_1143" s="T77">np:A</ta>
            <ta e="T80" id="Seg_1144" s="T79">np.h:P</ta>
            <ta e="T83" id="Seg_1145" s="T82">np.h:P</ta>
            <ta e="T84" id="Seg_1146" s="T83">0.3:A</ta>
            <ta e="T85" id="Seg_1147" s="T84">np:A</ta>
            <ta e="T86" id="Seg_1148" s="T85">0.3.h:P</ta>
            <ta e="T87" id="Seg_1149" s="T86">v:Th</ta>
            <ta e="T88" id="Seg_1150" s="T87">0.3:A</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T22" id="Seg_1151" s="T21">RUS:core</ta>
            <ta e="T27" id="Seg_1152" s="T26">RUS:core</ta>
            <ta e="T34" id="Seg_1153" s="T33">RUS:core</ta>
            <ta e="T82" id="Seg_1154" s="T81">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_1155" s="T0">Щука величиной с человека. </ta>
            <ta e="T7" id="Seg_1156" s="T3">Вверх по Польте есть озеро.</ta>
            <ta e="T15" id="Seg_1157" s="T7">Осенью лёд ломается, слышно, как будто из ружья стреляют. </ta>
            <ta e="T20" id="Seg_1158" s="T15">Мы вдвоём пришли на это озеро. </ta>
            <ta e="T32" id="Seg_1159" s="T20">Лёд был весь сломан, здесь мы живем, каждый день слышно, будто кто-то там ходит. </ta>
            <ta e="T35" id="Seg_1160" s="T32">Лёд был весь выворочен. </ta>
            <ta e="T39" id="Seg_1161" s="T35">Моя жена очень боялась.</ta>
            <ta e="T45" id="Seg_1162" s="T39">Она на озеро за водой даже не ходила. </ta>
            <ta e="T50" id="Seg_1163" s="T45">Это озеро находится на болоте.</ta>
            <ta e="T58" id="Seg_1164" s="T50"> ‎‎Это озеро находится на болоте, с одной стороны — кедровый остров.</ta>
            <ta e="T62" id="Seg_1165" s="T58">Озеро в середине очень глубокое. </ta>
            <ta e="T70" id="Seg_1166" s="T62">В этом озере живут большие щуки, величиной с человека.</ta>
            <ta e="T88" id="Seg_1167" s="T70">Варвара говорит: "На новом обласке здесь ездил человек, щука этого человека схватила и проглотила."</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_1168" s="T0">A pike the size of a man.</ta>
            <ta e="T7" id="Seg_1169" s="T3">There is a lake near Polta.</ta>
            <ta e="T15" id="Seg_1170" s="T7">In fall the ice breaks, it sounds like gun shots.</ta>
            <ta e="T20" id="Seg_1171" s="T15">We go to the lake.</ta>
            <ta e="T32" id="Seg_1172" s="T20">All the ice started to break. We live here. Everyday it sounds like as if something walks around there.</ta>
            <ta e="T35" id="Seg_1173" s="T32">The ice was all twisted.</ta>
            <ta e="T39" id="Seg_1174" s="T35">My wife was very anxious.</ta>
            <ta e="T45" id="Seg_1175" s="T39">She even does not go to the lake to get water.</ta>
            <ta e="T50" id="Seg_1176" s="T45">This lake is at a swamp.</ta>
            <ta e="T58" id="Seg_1177" s="T50">This lake is at the swamp, on one side there is a cedar island.</ta>
            <ta e="T62" id="Seg_1178" s="T58">The middle of the lake is very deep.</ta>
            <ta e="T70" id="Seg_1179" s="T62">In the lake there lives a big pike, a human-sized pike.</ta>
            <ta e="T88" id="Seg_1180" s="T70">The old Warwara said: "A human was going by a new, small boat, the pike grabbed him and devoured him."</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_1181" s="T0">Ein Hecht so groß wie ein Mensch.</ta>
            <ta e="T7" id="Seg_1182" s="T3">Dort gibt es den See Polta.</ta>
            <ta e="T15" id="Seg_1183" s="T7">Im Herbst bricht das Eis, wie Gewehrschüsse hört sich das an.</ta>
            <ta e="T20" id="Seg_1184" s="T15">Wir zwei gingen zu diesem See.</ta>
            <ta e="T32" id="Seg_1185" s="T20">Das ganze Eis begann zu brechen. Hier leben wir. Jeden Tag hört es sich so an als ob etwas dort langgeht.</ta>
            <ta e="T35" id="Seg_1186" s="T32">Das Eis war ganz verdreht.</ta>
            <ta e="T39" id="Seg_1187" s="T35">Meine Frau war sehr ängstlich.</ta>
            <ta e="T45" id="Seg_1188" s="T39">Sie ging nicht einmal zum See um Wasser zu holen.</ta>
            <ta e="T50" id="Seg_1189" s="T45">Dieser See ist am Sumpf gelegen.</ta>
            <ta e="T58" id="Seg_1190" s="T50">Dieser See ist am Sumpf gelegen, auf einer Seite ist eine Zedarinsel.</ta>
            <ta e="T62" id="Seg_1191" s="T58">In der Mitte ist der See sehr tief.</ta>
            <ta e="T70" id="Seg_1192" s="T62">In diesem See lebte ein großer Hecht, ein menschengroßer Hecht.</ta>
            <ta e="T88" id="Seg_1193" s="T70">Warwara sagt: "In einem neuen Boot fuhr ein Mensch, der Hecht packte diesen Mensch und verschluckte ihn."</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T7" id="Seg_1194" s="T3">вверх по польте озеро, там есть.</ta>
            <ta e="T15" id="Seg_1195" s="T7">Осенью лед ломает(ся) слыхать как ружьем стреляет.</ta>
            <ta e="T20" id="Seg_1196" s="T15">мы двое пришли на это озеро.</ta>
            <ta e="T32" id="Seg_1197" s="T20">лёд вес сломан, там мы живи, каждый день слыхать кто-то там ходит </ta>
            <ta e="T35" id="Seg_1198" s="T32">лед вес выворочен.</ta>
            <ta e="T39" id="Seg_1199" s="T35">моя жена очень боялась.</ta>
            <ta e="T45" id="Seg_1200" s="T39">она на озеро за водой даже не ходила</ta>
            <ta e="T50" id="Seg_1201" s="T45">это озеро на болоте находится.</ta>
            <ta e="T58" id="Seg_1202" s="T50">на болоте это озеро, с одной стороны есть кедровый остров </ta>
            <ta e="T62" id="Seg_1203" s="T58">озеро в средине очень глубокое</ta>
            <ta e="T70" id="Seg_1204" s="T62">на эром острове живут большие щуки, с человека велочиной щуки.</ta>
            <ta e="T88" id="Seg_1205" s="T70">варвара говорит новым обласком ездил (человеку щука это человека сватила проглотила щука сватила проглотить хочет </ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T7" id="Seg_1206" s="T3">[WNB:] lake Polta: 59.741747, 81.482303</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
