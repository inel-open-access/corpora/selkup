<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDEFCD497F-00F3-F7DD-B0FE-A1364CAB2158">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">flk\KFN_1967_BigBear1_flk\KFN_1967_BigBear1_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">79</ud-information>
            <ud-information attribute-name="# HIAT:w">58</ud-information>
            <ud-information attribute-name="# e">58</ud-information>
            <ud-information attribute-name="# HIAT:u">12</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KFN">
            <abbreviation>KFN</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KFN"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T58" id="Seg_0" n="sc" s="T0">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Iːďe</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">poqurešpa</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">Qaːnaɣe</ts>
                  <nts id="Seg_11" n="HIAT:ip">,</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T3">šitə</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_17" n="HIAT:w" s="T4">qut</ts>
                  <nts id="Seg_18" n="HIAT:ip">.</nts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_21" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_23" n="HIAT:w" s="T5">Pönəge</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">qwäl</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">twerna</ts>
                  <nts id="Seg_30" n="HIAT:ip">.</nts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_33" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_35" n="HIAT:w" s="T8">Iːďe</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_38" n="HIAT:w" s="T9">sap</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_41" n="HIAT:w" s="T10">pakɨlʼešpat</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_43" n="HIAT:ip">(</nts>
                  <ts e="T12" id="Seg_45" n="HIAT:w" s="T11">pakɨlbat</ts>
                  <nts id="Seg_46" n="HIAT:ip">)</nts>
                  <nts id="Seg_47" n="HIAT:ip">,</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_50" n="HIAT:w" s="T12">besetkap</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_53" n="HIAT:w" s="T13">saːɣe</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">qamǯimbat</ts>
                  <nts id="Seg_57" n="HIAT:ip">.</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_60" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_62" n="HIAT:w" s="T15">Pönäge</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_65" n="HIAT:w" s="T16">besetkat</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_68" n="HIAT:w" s="T17">omnɨmba</ts>
                  <nts id="Seg_69" n="HIAT:ip">.</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_72" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_74" n="HIAT:w" s="T18">Taːt</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_77" n="HIAT:w" s="T19">rokuatpa</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_80" n="HIAT:w" s="T20">sant</ts>
                  <nts id="Seg_81" n="HIAT:ip">.</nts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_84" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_86" n="HIAT:w" s="T21">Pöneɣe</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_89" n="HIAT:w" s="T22">nejwatpa</ts>
                  <nts id="Seg_90" n="HIAT:ip">.</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_93" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_95" n="HIAT:w" s="T23">Tabi</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_98" n="HIAT:w" s="T24">amgu</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_101" n="HIAT:w" s="T25">kɨgelɨmba</ts>
                  <nts id="Seg_102" n="HIAT:ip">.</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_105" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_107" n="HIAT:w" s="T26">Tabi</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_110" n="HIAT:w" s="T27">aː</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_113" n="HIAT:w" s="T28">čeːnča</ts>
                  <nts id="Seg_114" n="HIAT:ip">.</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_117" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_119" n="HIAT:w" s="T29">Miʒe</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_122" n="HIAT:w" s="T30">iga</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_125" n="HIAT:w" s="T31">awešk</ts>
                  <nts id="Seg_126" n="HIAT:ip">,</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_129" n="HIAT:w" s="T32">mi</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_132" n="HIAT:w" s="T33">teka</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_135" n="HIAT:w" s="T34">peqap</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_138" n="HIAT:w" s="T35">čaǯelaje</ts>
                  <nts id="Seg_139" n="HIAT:ip">,</nts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_142" n="HIAT:w" s="T36">peqan</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_145" n="HIAT:w" s="T37">waǯʼ</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_148" n="HIAT:w" s="T38">lučše</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_151" n="HIAT:w" s="T39">mi</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_154" n="HIAT:w" s="T40">waǯʼinan</ts>
                  <nts id="Seg_155" n="HIAT:ip">.</nts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_158" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_160" n="HIAT:w" s="T41">Peqam</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_163" n="HIAT:w" s="T42">nʼöːdat</ts>
                  <nts id="Seg_164" n="HIAT:ip">,</nts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_167" n="HIAT:w" s="T43">Pönaki</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_170" n="HIAT:w" s="T44">mogoɣənt</ts>
                  <nts id="Seg_171" n="HIAT:ip">.</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_174" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_176" n="HIAT:w" s="T45">Peq</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_179" n="HIAT:w" s="T46">patom</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_182" n="HIAT:w" s="T47">innä</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_185" n="HIAT:w" s="T48">wašeǯəmba</ts>
                  <nts id="Seg_186" n="HIAT:ip">,</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_189" n="HIAT:w" s="T49">nuːšunend</ts>
                  <nts id="Seg_190" n="HIAT:ip">.</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_193" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_195" n="HIAT:w" s="T50">Nu</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_198" n="HIAT:w" s="T51">šunǯeɣɨt</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_201" n="HIAT:w" s="T52">nop</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_204" n="HIAT:w" s="T53">qoštɨt</ts>
                  <nts id="Seg_205" n="HIAT:ip">,</nts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_208" n="HIAT:w" s="T54">wes</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_211" n="HIAT:w" s="T55">qɨška</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_214" n="HIAT:w" s="T56">haitqo</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_217" n="HIAT:w" s="T57">meːɣɨt</ts>
                  <nts id="Seg_218" n="HIAT:ip">.</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T58" id="Seg_220" n="sc" s="T0">
               <ts e="T1" id="Seg_222" n="e" s="T0">Iːďe </ts>
               <ts e="T2" id="Seg_224" n="e" s="T1">poqurešpa </ts>
               <ts e="T3" id="Seg_226" n="e" s="T2">Qaːnaɣe, </ts>
               <ts e="T4" id="Seg_228" n="e" s="T3">šitə </ts>
               <ts e="T5" id="Seg_230" n="e" s="T4">qut. </ts>
               <ts e="T6" id="Seg_232" n="e" s="T5">Pönəge </ts>
               <ts e="T7" id="Seg_234" n="e" s="T6">qwäl </ts>
               <ts e="T8" id="Seg_236" n="e" s="T7">twerna. </ts>
               <ts e="T9" id="Seg_238" n="e" s="T8">Iːďe </ts>
               <ts e="T10" id="Seg_240" n="e" s="T9">sap </ts>
               <ts e="T11" id="Seg_242" n="e" s="T10">pakɨlʼešpat </ts>
               <ts e="T12" id="Seg_244" n="e" s="T11">(pakɨlbat), </ts>
               <ts e="T13" id="Seg_246" n="e" s="T12">besetkap </ts>
               <ts e="T14" id="Seg_248" n="e" s="T13">saːɣe </ts>
               <ts e="T15" id="Seg_250" n="e" s="T14">qamǯimbat. </ts>
               <ts e="T16" id="Seg_252" n="e" s="T15">Pönäge </ts>
               <ts e="T17" id="Seg_254" n="e" s="T16">besetkat </ts>
               <ts e="T18" id="Seg_256" n="e" s="T17">omnɨmba. </ts>
               <ts e="T19" id="Seg_258" n="e" s="T18">Taːt </ts>
               <ts e="T20" id="Seg_260" n="e" s="T19">rokuatpa </ts>
               <ts e="T21" id="Seg_262" n="e" s="T20">sant. </ts>
               <ts e="T22" id="Seg_264" n="e" s="T21">Pöneɣe </ts>
               <ts e="T23" id="Seg_266" n="e" s="T22">nejwatpa. </ts>
               <ts e="T24" id="Seg_268" n="e" s="T23">Tabi </ts>
               <ts e="T25" id="Seg_270" n="e" s="T24">amgu </ts>
               <ts e="T26" id="Seg_272" n="e" s="T25">kɨgelɨmba. </ts>
               <ts e="T27" id="Seg_274" n="e" s="T26">Tabi </ts>
               <ts e="T28" id="Seg_276" n="e" s="T27">aː </ts>
               <ts e="T29" id="Seg_278" n="e" s="T28">čeːnča. </ts>
               <ts e="T30" id="Seg_280" n="e" s="T29">Miʒe </ts>
               <ts e="T31" id="Seg_282" n="e" s="T30">iga </ts>
               <ts e="T32" id="Seg_284" n="e" s="T31">awešk, </ts>
               <ts e="T33" id="Seg_286" n="e" s="T32">mi </ts>
               <ts e="T34" id="Seg_288" n="e" s="T33">teka </ts>
               <ts e="T35" id="Seg_290" n="e" s="T34">peqap </ts>
               <ts e="T36" id="Seg_292" n="e" s="T35">čaǯelaje, </ts>
               <ts e="T37" id="Seg_294" n="e" s="T36">peqan </ts>
               <ts e="T38" id="Seg_296" n="e" s="T37">waǯʼ </ts>
               <ts e="T39" id="Seg_298" n="e" s="T38">lučše </ts>
               <ts e="T40" id="Seg_300" n="e" s="T39">mi </ts>
               <ts e="T41" id="Seg_302" n="e" s="T40">waǯʼinan. </ts>
               <ts e="T42" id="Seg_304" n="e" s="T41">Peqam </ts>
               <ts e="T43" id="Seg_306" n="e" s="T42">nʼöːdat, </ts>
               <ts e="T44" id="Seg_308" n="e" s="T43">Pönaki </ts>
               <ts e="T45" id="Seg_310" n="e" s="T44">mogoɣənt. </ts>
               <ts e="T46" id="Seg_312" n="e" s="T45">Peq </ts>
               <ts e="T47" id="Seg_314" n="e" s="T46">patom </ts>
               <ts e="T48" id="Seg_316" n="e" s="T47">innä </ts>
               <ts e="T49" id="Seg_318" n="e" s="T48">wašeǯəmba, </ts>
               <ts e="T50" id="Seg_320" n="e" s="T49">nuːšunend. </ts>
               <ts e="T51" id="Seg_322" n="e" s="T50">Nu </ts>
               <ts e="T52" id="Seg_324" n="e" s="T51">šunǯeɣɨt </ts>
               <ts e="T53" id="Seg_326" n="e" s="T52">nop </ts>
               <ts e="T54" id="Seg_328" n="e" s="T53">qoštɨt, </ts>
               <ts e="T55" id="Seg_330" n="e" s="T54">wes </ts>
               <ts e="T56" id="Seg_332" n="e" s="T55">qɨška </ts>
               <ts e="T57" id="Seg_334" n="e" s="T56">haitqo </ts>
               <ts e="T58" id="Seg_336" n="e" s="T57">meːɣɨt. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_337" s="T0">KFN_1967_BigBear1_flk.001 (001.001)</ta>
            <ta e="T8" id="Seg_338" s="T5">KFN_1967_BigBear1_flk.002 (001.002)</ta>
            <ta e="T15" id="Seg_339" s="T8">KFN_1967_BigBear1_flk.003 (001.003)</ta>
            <ta e="T18" id="Seg_340" s="T15">KFN_1967_BigBear1_flk.004 (001.004)</ta>
            <ta e="T21" id="Seg_341" s="T18">KFN_1967_BigBear1_flk.005 (001.005)</ta>
            <ta e="T23" id="Seg_342" s="T21">KFN_1967_BigBear1_flk.006 (001.006)</ta>
            <ta e="T26" id="Seg_343" s="T23">KFN_1967_BigBear1_flk.007 (001.007)</ta>
            <ta e="T29" id="Seg_344" s="T26">KFN_1967_BigBear1_flk.008 (001.008)</ta>
            <ta e="T41" id="Seg_345" s="T29">KFN_1967_BigBear1_flk.009 (001.009)</ta>
            <ta e="T45" id="Seg_346" s="T41">KFN_1967_BigBear1_flk.010 (001.010)</ta>
            <ta e="T50" id="Seg_347" s="T45">KFN_1967_BigBear1_flk.011 (001.011)</ta>
            <ta e="T58" id="Seg_348" s="T50">KFN_1967_BigBear1_flk.012 (001.012)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_349" s="T0">ӣдʼe поkурешпа kа̄наɣе шитъ kут</ta>
            <ta e="T8" id="Seg_350" s="T5">пöнъɣе kwäl ′тверна</ta>
            <ta e="T15" id="Seg_351" s="T8">ӣдʼе сап пакыле′шпап (пакылбат) бе′сеткап ′са̄ɣе kамджимбат.</ta>
            <ta e="T18" id="Seg_352" s="T15">пöнäге бесеткат ′омнымба </ta>
            <ta e="T21" id="Seg_353" s="T18">та̄т року′атпа сант</ta>
            <ta e="T23" id="Seg_354" s="T21">пöнäɣе ней′ватпа</ta>
            <ta e="T26" id="Seg_355" s="T23">та′би амгу кы′гелымбы</ta>
            <ta e="T29" id="Seg_356" s="T26">та′би а̄ ′че̄нча </ta>
            <ta e="T41" id="Seg_357" s="T29">миже и′га вешк ми ′тека ′паkап чажелаj(й)е, ′пеkан wадʼзʼ лучше ми wадʼзʼи′нан</ta>
            <ta e="T45" id="Seg_358" s="T41">′пеkап(м) нʼöдат. пöнаки мо′гоɣънт</ta>
            <ta e="T50" id="Seg_359" s="T45">пеk патом ин′нä ва′шежемба нӯшʼи′ненд</ta>
            <ta e="T58" id="Seg_360" s="T50">ну шʼунджеɣыт ноп kош′тыт, вес kы′шка hаитkо ме̄ɣыт</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_361" s="T0">iːďe poqurešpa qaːnaɣe, šitə qut</ta>
            <ta e="T8" id="Seg_362" s="T5">pönəge qwäl tverna</ta>
            <ta e="T15" id="Seg_363" s="T8">iːďe sap pakɨlʼešpat (pakɨlbat), besetkap saːɣe qamǯimbat.</ta>
            <ta e="T18" id="Seg_364" s="T15">pönäge besetkat omnɨmba</ta>
            <ta e="T21" id="Seg_365" s="T18">taːt rokuatpa sant</ta>
            <ta e="T23" id="Seg_366" s="T21">pöneɣe nejvatpa</ta>
            <ta e="T26" id="Seg_367" s="T23">abi amgu kɨgelɨmba</ta>
            <ta e="T29" id="Seg_368" s="T26">tabi aː čeːnča</ta>
            <ta e="T41" id="Seg_369" s="T29">miʒe iga wešk, mi teka peqap čaǯelaje, peqan waǯʼ lučše mi waǯʼinan</ta>
            <ta e="T45" id="Seg_370" s="T41">peqam(p) nʼöːdat, Pönaki mogoɣənt</ta>
            <ta e="T50" id="Seg_371" s="T45">peq patom innä wašeǯəmba, nuːšunend</ta>
            <ta e="T58" id="Seg_372" s="T50">nu šunǯeɣɨt nop qoštɨt, ves qɨška haitqo meːɣɨt</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_373" s="T0">Iːďe poqurešpa Qaːnaɣe, šitə qut. </ta>
            <ta e="T8" id="Seg_374" s="T5">Pönəge qwäl twerna. </ta>
            <ta e="T15" id="Seg_375" s="T8">Iːďe sap pakɨlʼešpat (pakɨlbat), besetkap saːɣe qamǯimbat. </ta>
            <ta e="T18" id="Seg_376" s="T15">Pönäge besetkat omnɨmba. </ta>
            <ta e="T21" id="Seg_377" s="T18">Taːt rokuatpa sant. </ta>
            <ta e="T23" id="Seg_378" s="T21">Pöneɣe nejwatpa. </ta>
            <ta e="T26" id="Seg_379" s="T23">Tabi amgu kɨgelɨmba. </ta>
            <ta e="T29" id="Seg_380" s="T26">Tabi aː čeːnča. </ta>
            <ta e="T41" id="Seg_381" s="T29">Miʒe iga awešk, mi teka peqap čaǯelaje, peqan waǯʼ lučše mi waǯʼinan. </ta>
            <ta e="T45" id="Seg_382" s="T41">Peqam nʼöːdat, Pönaki mogoɣənt. </ta>
            <ta e="T50" id="Seg_383" s="T45">Peq patom innä wašeǯəmba, nuːšunend. </ta>
            <ta e="T58" id="Seg_384" s="T50">Nu šunǯeɣɨt nop qoštɨt, wes qɨška haitqo meːɣɨt. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_385" s="T0">Iːďe</ta>
            <ta e="T2" id="Seg_386" s="T1">poqu-r-e-špa</ta>
            <ta e="T3" id="Seg_387" s="T2">Qaːna-ɣe</ta>
            <ta e="T4" id="Seg_388" s="T3">šitə</ta>
            <ta e="T5" id="Seg_389" s="T4">qu-t</ta>
            <ta e="T6" id="Seg_390" s="T5">pönəge</ta>
            <ta e="T7" id="Seg_391" s="T6">qwäl</ta>
            <ta e="T8" id="Seg_392" s="T7">twe-r-na</ta>
            <ta e="T9" id="Seg_393" s="T8">Iːďe</ta>
            <ta e="T10" id="Seg_394" s="T9">sa-p</ta>
            <ta e="T11" id="Seg_395" s="T10">pakɨl-e-špa-t</ta>
            <ta e="T12" id="Seg_396" s="T11">pakɨl-ba-t</ta>
            <ta e="T13" id="Seg_397" s="T12">besetka-p</ta>
            <ta e="T14" id="Seg_398" s="T13">saː-ɣe</ta>
            <ta e="T15" id="Seg_399" s="T14">qam-ǯi-mba-t</ta>
            <ta e="T16" id="Seg_400" s="T15">Pönäge</ta>
            <ta e="T17" id="Seg_401" s="T16">besetka-t</ta>
            <ta e="T18" id="Seg_402" s="T17">omnɨ-mba</ta>
            <ta e="T19" id="Seg_403" s="T18">taːt</ta>
            <ta e="T20" id="Seg_404" s="T19">rokuat-pa</ta>
            <ta e="T21" id="Seg_405" s="T20">sa-nt</ta>
            <ta e="T22" id="Seg_406" s="T21">Pöneɣe</ta>
            <ta e="T23" id="Seg_407" s="T22">nejwat-pa</ta>
            <ta e="T24" id="Seg_408" s="T23">tabi</ta>
            <ta e="T25" id="Seg_409" s="T24">am-gu</ta>
            <ta e="T26" id="Seg_410" s="T25">kɨge-lɨ-mba</ta>
            <ta e="T27" id="Seg_411" s="T26">tabi</ta>
            <ta e="T28" id="Seg_412" s="T27">aː</ta>
            <ta e="T29" id="Seg_413" s="T28">čeːnča</ta>
            <ta e="T30" id="Seg_414" s="T29">miʒe</ta>
            <ta e="T31" id="Seg_415" s="T30">iga</ta>
            <ta e="T32" id="Seg_416" s="T31">aw-e-šk</ta>
            <ta e="T33" id="Seg_417" s="T32">mi</ta>
            <ta e="T34" id="Seg_418" s="T33">teka</ta>
            <ta e="T35" id="Seg_419" s="T34">peqa-p</ta>
            <ta e="T36" id="Seg_420" s="T35">čaǯe-la-je</ta>
            <ta e="T37" id="Seg_421" s="T36">peqa-n</ta>
            <ta e="T38" id="Seg_422" s="T37">waǯʼ</ta>
            <ta e="T39" id="Seg_423" s="T38">lučše</ta>
            <ta e="T40" id="Seg_424" s="T39">mi</ta>
            <ta e="T41" id="Seg_425" s="T40">waǯʼi-nan</ta>
            <ta e="T42" id="Seg_426" s="T41">peqa-m</ta>
            <ta e="T43" id="Seg_427" s="T42">nʼöːda-t</ta>
            <ta e="T44" id="Seg_428" s="T43">Pönaki</ta>
            <ta e="T45" id="Seg_429" s="T44">mogo-ɣən-t</ta>
            <ta e="T46" id="Seg_430" s="T45">peq</ta>
            <ta e="T47" id="Seg_431" s="T46">patom</ta>
            <ta e="T48" id="Seg_432" s="T47">innä</ta>
            <ta e="T49" id="Seg_433" s="T48">waše-ǯə-mba</ta>
            <ta e="T50" id="Seg_434" s="T49">nuː-šune-nd</ta>
            <ta e="T51" id="Seg_435" s="T50">nu</ta>
            <ta e="T52" id="Seg_436" s="T51">šunǯe-ɣɨt</ta>
            <ta e="T53" id="Seg_437" s="T52">nop</ta>
            <ta e="T54" id="Seg_438" s="T53">qoš-tɨ-t</ta>
            <ta e="T55" id="Seg_439" s="T54">wes</ta>
            <ta e="T56" id="Seg_440" s="T55">qɨška</ta>
            <ta e="T57" id="Seg_441" s="T56">hai-tqo</ta>
            <ta e="T58" id="Seg_442" s="T57">meː-ɣɨ-t</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_443" s="T0">Itʼe</ta>
            <ta e="T2" id="Seg_444" s="T1">poq-r-ɨ-špɨ</ta>
            <ta e="T3" id="Seg_445" s="T2">Kaːna-se</ta>
            <ta e="T4" id="Seg_446" s="T3">šitə</ta>
            <ta e="T5" id="Seg_447" s="T4">qum-t</ta>
            <ta e="T6" id="Seg_448" s="T5">Pöːnege</ta>
            <ta e="T7" id="Seg_449" s="T6">kwel</ta>
            <ta e="T8" id="Seg_450" s="T7">twe-r-ŋɨ</ta>
            <ta e="T9" id="Seg_451" s="T8">Itʼe</ta>
            <ta e="T10" id="Seg_452" s="T9">sa-m</ta>
            <ta e="T11" id="Seg_453" s="T10">pakɨl-ɨ-špɨ-t</ta>
            <ta e="T12" id="Seg_454" s="T11">pakɨl-mbɨ-t</ta>
            <ta e="T13" id="Seg_455" s="T12">besetka-p</ta>
            <ta e="T14" id="Seg_456" s="T13">sa-se</ta>
            <ta e="T15" id="Seg_457" s="T14">qam-nǯe-mbɨ-t</ta>
            <ta e="T16" id="Seg_458" s="T15">Pöːnege</ta>
            <ta e="T17" id="Seg_459" s="T16">besetka-nde</ta>
            <ta e="T18" id="Seg_460" s="T17">omde-mbɨ</ta>
            <ta e="T19" id="Seg_461" s="T18">tat</ta>
            <ta e="T20" id="Seg_462" s="T19">rokuat-mbɨ</ta>
            <ta e="T21" id="Seg_463" s="T20">sa-nde</ta>
            <ta e="T22" id="Seg_464" s="T21">Pöːnege</ta>
            <ta e="T23" id="Seg_465" s="T22">nʼajwat-mbɨ</ta>
            <ta e="T24" id="Seg_466" s="T23">tab</ta>
            <ta e="T25" id="Seg_467" s="T24">am-gu</ta>
            <ta e="T26" id="Seg_468" s="T25">kɨge-lɨ-mbɨ</ta>
            <ta e="T27" id="Seg_469" s="T26">tab</ta>
            <ta e="T28" id="Seg_470" s="T27">aː</ta>
            <ta e="T29" id="Seg_471" s="T28">čenčɨ</ta>
            <ta e="T30" id="Seg_472" s="T29">miǯnɨt</ta>
            <ta e="T31" id="Seg_473" s="T30">ɨgɨ</ta>
            <ta e="T32" id="Seg_474" s="T31">am-ɨ-äšɨk</ta>
            <ta e="T33" id="Seg_475" s="T32">mi</ta>
            <ta e="T34" id="Seg_476" s="T33">teːka</ta>
            <ta e="T35" id="Seg_477" s="T34">peqqa-m</ta>
            <ta e="T36" id="Seg_478" s="T35">čačɨ-la-j</ta>
            <ta e="T37" id="Seg_479" s="T36">peqqa-n</ta>
            <ta e="T38" id="Seg_480" s="T37">waǯʼe</ta>
            <ta e="T39" id="Seg_481" s="T38">lučše</ta>
            <ta e="T40" id="Seg_482" s="T39">mi</ta>
            <ta e="T41" id="Seg_483" s="T40">waǯʼe-nando</ta>
            <ta e="T42" id="Seg_484" s="T41">peqqa-m</ta>
            <ta e="T43" id="Seg_485" s="T42">nʼöːda-di</ta>
            <ta e="T44" id="Seg_486" s="T43">Pöːnege</ta>
            <ta e="T45" id="Seg_487" s="T44">moqo-qɨn-t</ta>
            <ta e="T46" id="Seg_488" s="T45">peqqa</ta>
            <ta e="T47" id="Seg_489" s="T46">patom</ta>
            <ta e="T48" id="Seg_490" s="T47">inne</ta>
            <ta e="T49" id="Seg_491" s="T48">waše-lʼčǝ-mbɨ</ta>
            <ta e="T50" id="Seg_492" s="T49">nop-šünde-nde</ta>
            <ta e="T51" id="Seg_493" s="T50">nom</ta>
            <ta e="T52" id="Seg_494" s="T51">šünde-qɨn</ta>
            <ta e="T53" id="Seg_495" s="T52">nom</ta>
            <ta e="T54" id="Seg_496" s="T53">qoš-ntɨ-t</ta>
            <ta e="T55" id="Seg_497" s="T54">wesʼ</ta>
            <ta e="T56" id="Seg_498" s="T55">qɨška</ta>
            <ta e="T57" id="Seg_499" s="T56">haj-tqo</ta>
            <ta e="T58" id="Seg_500" s="T57">me-ŋɨ-t</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_501" s="T0">Itja.[NOM]</ta>
            <ta e="T2" id="Seg_502" s="T1">net-VBLZ-EP-IPFV2.[3SG.S]</ta>
            <ta e="T3" id="Seg_503" s="T2">Kana-COM</ta>
            <ta e="T4" id="Seg_504" s="T3">two</ta>
            <ta e="T5" id="Seg_505" s="T4">human.being.[NOM]-3SG</ta>
            <ta e="T6" id="Seg_506" s="T5">Pönege.[NOM]</ta>
            <ta e="T7" id="Seg_507" s="T6">fish.[NOM]</ta>
            <ta e="T8" id="Seg_508" s="T7">steal-FRQ-CO.[3SG.S]</ta>
            <ta e="T9" id="Seg_509" s="T8">Itja.[NOM]</ta>
            <ta e="T10" id="Seg_510" s="T9">tar-ACC</ta>
            <ta e="T11" id="Seg_511" s="T10">excavate-EP-IPFV2-3SG.O</ta>
            <ta e="T12" id="Seg_512" s="T11">excavate-PST.NAR-3SG.O</ta>
            <ta e="T13" id="Seg_513" s="T12">bench-ACC</ta>
            <ta e="T14" id="Seg_514" s="T13">tar-INSTR</ta>
            <ta e="T15" id="Seg_515" s="T14">pour-IPFV3-PST.NAR-3SG.O</ta>
            <ta e="T16" id="Seg_516" s="T15">Pönege.[NOM]</ta>
            <ta e="T17" id="Seg_517" s="T16">bench-ILL</ta>
            <ta e="T18" id="Seg_518" s="T17">sit.down-PST.NAR.[3SG.S]</ta>
            <ta e="T19" id="Seg_519" s="T18">ass.[NOM]</ta>
            <ta e="T20" id="Seg_520" s="T19">glue-PST.NAR.[3SG.S]</ta>
            <ta e="T21" id="Seg_521" s="T20">tar-ILL</ta>
            <ta e="T22" id="Seg_522" s="T21">Pönege.[NOM]</ta>
            <ta e="T23" id="Seg_523" s="T22">get.angry-PST.NAR.[3SG.S]</ta>
            <ta e="T24" id="Seg_524" s="T23">(s)he</ta>
            <ta e="T25" id="Seg_525" s="T24">eat-INF</ta>
            <ta e="T26" id="Seg_526" s="T25">want-RES-PST.NAR.[3SG.S]</ta>
            <ta e="T27" id="Seg_527" s="T26">(s)he</ta>
            <ta e="T28" id="Seg_528" s="T27">NEG</ta>
            <ta e="T29" id="Seg_529" s="T28">say.[3SG.S]</ta>
            <ta e="T30" id="Seg_530" s="T29">we.DU.ACC</ta>
            <ta e="T31" id="Seg_531" s="T30">NEG.IMP</ta>
            <ta e="T32" id="Seg_532" s="T31">eat-EP-IMP.2SG.S</ta>
            <ta e="T33" id="Seg_533" s="T32">we.DU.NOM</ta>
            <ta e="T34" id="Seg_534" s="T33">you.SG.DAT</ta>
            <ta e="T35" id="Seg_535" s="T34">elk-ACC</ta>
            <ta e="T36" id="Seg_536" s="T35">shoot-FUT-1DU</ta>
            <ta e="T37" id="Seg_537" s="T36">elk-GEN</ta>
            <ta e="T38" id="Seg_538" s="T37">meat.[NOM]</ta>
            <ta e="T39" id="Seg_539" s="T38">better</ta>
            <ta e="T40" id="Seg_540" s="T39">we.PL.GEN</ta>
            <ta e="T41" id="Seg_541" s="T40">meat-ABL</ta>
            <ta e="T42" id="Seg_542" s="T41">elk-ACC</ta>
            <ta e="T43" id="Seg_543" s="T42">hunt-3DU.O</ta>
            <ta e="T44" id="Seg_544" s="T43">Pönege.[NOM]</ta>
            <ta e="T45" id="Seg_545" s="T44">back-LOC-3SG</ta>
            <ta e="T46" id="Seg_546" s="T45">elk.[NOM]</ta>
            <ta e="T47" id="Seg_547" s="T46">then</ta>
            <ta e="T48" id="Seg_548" s="T47">up</ta>
            <ta e="T49" id="Seg_549" s="T48">fly-PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T50" id="Seg_550" s="T49">god-inside-ILL</ta>
            <ta e="T51" id="Seg_551" s="T50">god.[NOM]</ta>
            <ta e="T52" id="Seg_552" s="T51">inside-LOC</ta>
            <ta e="T53" id="Seg_553" s="T52">god.[NOM]</ta>
            <ta e="T54" id="Seg_554" s="T53">discover-IPFV-3SG.O</ta>
            <ta e="T55" id="Seg_555" s="T54">all</ta>
            <ta e="T56" id="Seg_556" s="T55">star.[NOM]</ta>
            <ta e="T57" id="Seg_557" s="T56">eye-TRL</ta>
            <ta e="T58" id="Seg_558" s="T57">do-CO-3SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_559" s="T0">Итя.[NOM]</ta>
            <ta e="T2" id="Seg_560" s="T1">сеть-VBLZ-EP-IPFV2.[3SG.S]</ta>
            <ta e="T3" id="Seg_561" s="T2">Кана-COM</ta>
            <ta e="T4" id="Seg_562" s="T3">два</ta>
            <ta e="T5" id="Seg_563" s="T4">человек.[NOM]-3SG</ta>
            <ta e="T6" id="Seg_564" s="T5">Пёнеге.[NOM]</ta>
            <ta e="T7" id="Seg_565" s="T6">рыба.[NOM]</ta>
            <ta e="T8" id="Seg_566" s="T7">украсть-FRQ-CO.[3SG.S]</ta>
            <ta e="T9" id="Seg_567" s="T8">Итя.[NOM]</ta>
            <ta e="T10" id="Seg_568" s="T9">смола-ACC</ta>
            <ta e="T11" id="Seg_569" s="T10">откопать-EP-IPFV2-3SG.O</ta>
            <ta e="T12" id="Seg_570" s="T11">откопать-PST.NAR-3SG.O</ta>
            <ta e="T13" id="Seg_571" s="T12">скамейка-ACC</ta>
            <ta e="T14" id="Seg_572" s="T13">смола-INSTR</ta>
            <ta e="T15" id="Seg_573" s="T14">лить-IPFV3-PST.NAR-3SG.O</ta>
            <ta e="T16" id="Seg_574" s="T15">Пёнеге.[NOM]</ta>
            <ta e="T17" id="Seg_575" s="T16">скамейка-ILL</ta>
            <ta e="T18" id="Seg_576" s="T17">сесть-PST.NAR.[3SG.S]</ta>
            <ta e="T19" id="Seg_577" s="T18">жопа.[NOM]</ta>
            <ta e="T20" id="Seg_578" s="T19">клеить-PST.NAR.[3SG.S]</ta>
            <ta e="T21" id="Seg_579" s="T20">смола-ILL</ta>
            <ta e="T22" id="Seg_580" s="T21">Пёнеге.[NOM]</ta>
            <ta e="T23" id="Seg_581" s="T22">рассердиться-PST.NAR.[3SG.S]</ta>
            <ta e="T24" id="Seg_582" s="T23">он(а)</ta>
            <ta e="T25" id="Seg_583" s="T24">есть-INF</ta>
            <ta e="T26" id="Seg_584" s="T25">хотеть-RES-PST.NAR.[3SG.S]</ta>
            <ta e="T27" id="Seg_585" s="T26">он(а)</ta>
            <ta e="T28" id="Seg_586" s="T27">NEG</ta>
            <ta e="T29" id="Seg_587" s="T28">сказать.[3SG.S]</ta>
            <ta e="T30" id="Seg_588" s="T29">мы.DU.ACC</ta>
            <ta e="T31" id="Seg_589" s="T30">NEG.IMP</ta>
            <ta e="T32" id="Seg_590" s="T31">есть-EP-IMP.2SG.S</ta>
            <ta e="T33" id="Seg_591" s="T32">мы.DU.NOM</ta>
            <ta e="T34" id="Seg_592" s="T33">ты.DAT</ta>
            <ta e="T35" id="Seg_593" s="T34">лось-ACC</ta>
            <ta e="T36" id="Seg_594" s="T35">стрелять-FUT-1DU</ta>
            <ta e="T37" id="Seg_595" s="T36">лось-GEN</ta>
            <ta e="T38" id="Seg_596" s="T37">мясо.[NOM]</ta>
            <ta e="T39" id="Seg_597" s="T38">лучше</ta>
            <ta e="T40" id="Seg_598" s="T39">мы.PL.GEN</ta>
            <ta e="T41" id="Seg_599" s="T40">мясо-ABL</ta>
            <ta e="T42" id="Seg_600" s="T41">лось-ACC</ta>
            <ta e="T43" id="Seg_601" s="T42">погнать-3DU.O</ta>
            <ta e="T44" id="Seg_602" s="T43">Пёнеге.[NOM]</ta>
            <ta e="T45" id="Seg_603" s="T44">спина-LOC-3SG</ta>
            <ta e="T46" id="Seg_604" s="T45">лось.[NOM]</ta>
            <ta e="T47" id="Seg_605" s="T46">потом</ta>
            <ta e="T48" id="Seg_606" s="T47">вверх</ta>
            <ta e="T49" id="Seg_607" s="T48">лететь-PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T50" id="Seg_608" s="T49">бог-нутро-ILL</ta>
            <ta e="T51" id="Seg_609" s="T50">бог.[NOM]</ta>
            <ta e="T52" id="Seg_610" s="T51">нутро-LOC</ta>
            <ta e="T53" id="Seg_611" s="T52">бог.[NOM]</ta>
            <ta e="T54" id="Seg_612" s="T53">узнать-IPFV-3SG.O</ta>
            <ta e="T55" id="Seg_613" s="T54">весь</ta>
            <ta e="T56" id="Seg_614" s="T55">звезда.[NOM]</ta>
            <ta e="T57" id="Seg_615" s="T56">глаз-TRL</ta>
            <ta e="T58" id="Seg_616" s="T57">делать-CO-3SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_617" s="T0">nprop-n:case1</ta>
            <ta e="T2" id="Seg_618" s="T1">n-n&gt;v-infl:ins-v&gt;v-v:pn</ta>
            <ta e="T3" id="Seg_619" s="T2">nprop-n:case2</ta>
            <ta e="T4" id="Seg_620" s="T3">num</ta>
            <ta e="T5" id="Seg_621" s="T4">n-n:case1-n:poss</ta>
            <ta e="T6" id="Seg_622" s="T5">nprop-n:case1</ta>
            <ta e="T7" id="Seg_623" s="T6">n-n:case1</ta>
            <ta e="T8" id="Seg_624" s="T7">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T9" id="Seg_625" s="T8">nprop-n:case1</ta>
            <ta e="T10" id="Seg_626" s="T9">n-n:case1</ta>
            <ta e="T11" id="Seg_627" s="T10">v-infl:ins-v&gt;v-v:pn</ta>
            <ta e="T12" id="Seg_628" s="T11">v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_629" s="T12">n-n:case1</ta>
            <ta e="T14" id="Seg_630" s="T13">n-n:case2</ta>
            <ta e="T15" id="Seg_631" s="T14">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_632" s="T15">nprop-n:case1</ta>
            <ta e="T17" id="Seg_633" s="T16">n-n:case2</ta>
            <ta e="T18" id="Seg_634" s="T17">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_635" s="T18">n-n:case1</ta>
            <ta e="T20" id="Seg_636" s="T19">v-v:tense-v:pn</ta>
            <ta e="T21" id="Seg_637" s="T20">n-n:case2</ta>
            <ta e="T22" id="Seg_638" s="T21">nprop-n:case1</ta>
            <ta e="T23" id="Seg_639" s="T22">v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_640" s="T23">pers</ta>
            <ta e="T25" id="Seg_641" s="T24">v-v:ninf</ta>
            <ta e="T26" id="Seg_642" s="T25">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_643" s="T26">pers</ta>
            <ta e="T28" id="Seg_644" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_645" s="T28">v-v:pn</ta>
            <ta e="T30" id="Seg_646" s="T29">pers</ta>
            <ta e="T31" id="Seg_647" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_648" s="T31">v-infl:ins-v:mood-pn</ta>
            <ta e="T33" id="Seg_649" s="T32">pers</ta>
            <ta e="T34" id="Seg_650" s="T33">pers</ta>
            <ta e="T35" id="Seg_651" s="T34">n-n:case1</ta>
            <ta e="T36" id="Seg_652" s="T35">v-v:tense-v:pn</ta>
            <ta e="T37" id="Seg_653" s="T36">n-n:case2</ta>
            <ta e="T38" id="Seg_654" s="T37">n-n:case1</ta>
            <ta e="T39" id="Seg_655" s="T38">adj</ta>
            <ta e="T40" id="Seg_656" s="T39">pers</ta>
            <ta e="T41" id="Seg_657" s="T40">n-n:case1</ta>
            <ta e="T42" id="Seg_658" s="T41">n-n:case1</ta>
            <ta e="T43" id="Seg_659" s="T42">v-v:pn</ta>
            <ta e="T44" id="Seg_660" s="T43">nprop-n:case1</ta>
            <ta e="T45" id="Seg_661" s="T44">n-n:case1-n:poss</ta>
            <ta e="T46" id="Seg_662" s="T45">n-n:case1</ta>
            <ta e="T47" id="Seg_663" s="T46">adv</ta>
            <ta e="T48" id="Seg_664" s="T47">preverb</ta>
            <ta e="T49" id="Seg_665" s="T48">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T50" id="Seg_666" s="T49">n-n-n:case2</ta>
            <ta e="T51" id="Seg_667" s="T50">n-n:case1</ta>
            <ta e="T52" id="Seg_668" s="T51">n-n:case1</ta>
            <ta e="T53" id="Seg_669" s="T52">n-n:case1</ta>
            <ta e="T54" id="Seg_670" s="T53">v-v&gt;v-v:pn</ta>
            <ta e="T55" id="Seg_671" s="T54">quant</ta>
            <ta e="T56" id="Seg_672" s="T55">n-n:case1</ta>
            <ta e="T57" id="Seg_673" s="T56">n-n:case1</ta>
            <ta e="T58" id="Seg_674" s="T57">v-v:ins-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_675" s="T0">nprop</ta>
            <ta e="T2" id="Seg_676" s="T1">v</ta>
            <ta e="T3" id="Seg_677" s="T2">nprop</ta>
            <ta e="T4" id="Seg_678" s="T3">num</ta>
            <ta e="T5" id="Seg_679" s="T4">n</ta>
            <ta e="T6" id="Seg_680" s="T5">nprop</ta>
            <ta e="T7" id="Seg_681" s="T6">n</ta>
            <ta e="T8" id="Seg_682" s="T7">v</ta>
            <ta e="T9" id="Seg_683" s="T8">nprop</ta>
            <ta e="T10" id="Seg_684" s="T9">n</ta>
            <ta e="T11" id="Seg_685" s="T10">v</ta>
            <ta e="T12" id="Seg_686" s="T11">v</ta>
            <ta e="T13" id="Seg_687" s="T12">n</ta>
            <ta e="T14" id="Seg_688" s="T13">n</ta>
            <ta e="T15" id="Seg_689" s="T14">v</ta>
            <ta e="T16" id="Seg_690" s="T15">nprop</ta>
            <ta e="T17" id="Seg_691" s="T16">n</ta>
            <ta e="T18" id="Seg_692" s="T17">v</ta>
            <ta e="T19" id="Seg_693" s="T18">n</ta>
            <ta e="T20" id="Seg_694" s="T19">v</ta>
            <ta e="T21" id="Seg_695" s="T20">n</ta>
            <ta e="T22" id="Seg_696" s="T21">nprop</ta>
            <ta e="T23" id="Seg_697" s="T22">v</ta>
            <ta e="T24" id="Seg_698" s="T23">pers</ta>
            <ta e="T25" id="Seg_699" s="T24">v</ta>
            <ta e="T26" id="Seg_700" s="T25">v</ta>
            <ta e="T27" id="Seg_701" s="T26">pers</ta>
            <ta e="T28" id="Seg_702" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_703" s="T28">v</ta>
            <ta e="T30" id="Seg_704" s="T29">pers</ta>
            <ta e="T31" id="Seg_705" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_706" s="T31">v</ta>
            <ta e="T33" id="Seg_707" s="T32">pers</ta>
            <ta e="T34" id="Seg_708" s="T33">pers</ta>
            <ta e="T35" id="Seg_709" s="T34">n</ta>
            <ta e="T36" id="Seg_710" s="T35">v</ta>
            <ta e="T37" id="Seg_711" s="T36">n</ta>
            <ta e="T38" id="Seg_712" s="T37">n</ta>
            <ta e="T39" id="Seg_713" s="T38">adj</ta>
            <ta e="T40" id="Seg_714" s="T39">pers</ta>
            <ta e="T41" id="Seg_715" s="T40">n</ta>
            <ta e="T42" id="Seg_716" s="T41">n</ta>
            <ta e="T43" id="Seg_717" s="T42">v</ta>
            <ta e="T44" id="Seg_718" s="T43">nprop</ta>
            <ta e="T45" id="Seg_719" s="T44">n</ta>
            <ta e="T46" id="Seg_720" s="T45">n</ta>
            <ta e="T47" id="Seg_721" s="T46">adv</ta>
            <ta e="T48" id="Seg_722" s="T47">preverb</ta>
            <ta e="T49" id="Seg_723" s="T48">v</ta>
            <ta e="T50" id="Seg_724" s="T49">n</ta>
            <ta e="T51" id="Seg_725" s="T50">n</ta>
            <ta e="T52" id="Seg_726" s="T51">n</ta>
            <ta e="T53" id="Seg_727" s="T52">n</ta>
            <ta e="T54" id="Seg_728" s="T53">v</ta>
            <ta e="T55" id="Seg_729" s="T54">quant</ta>
            <ta e="T56" id="Seg_730" s="T55">n</ta>
            <ta e="T57" id="Seg_731" s="T56">n</ta>
            <ta e="T58" id="Seg_732" s="T57">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_733" s="T0">np.h:S</ta>
            <ta e="T2" id="Seg_734" s="T1">v:pred</ta>
            <ta e="T6" id="Seg_735" s="T5">np.h:S</ta>
            <ta e="T7" id="Seg_736" s="T6">np:O</ta>
            <ta e="T8" id="Seg_737" s="T7">v:pred</ta>
            <ta e="T9" id="Seg_738" s="T8">np.h:S</ta>
            <ta e="T10" id="Seg_739" s="T9">np:O</ta>
            <ta e="T11" id="Seg_740" s="T10">v:pred</ta>
            <ta e="T13" id="Seg_741" s="T12">np:O</ta>
            <ta e="T15" id="Seg_742" s="T14">0.3.h:S v:pred</ta>
            <ta e="T16" id="Seg_743" s="T15">np.h:S</ta>
            <ta e="T18" id="Seg_744" s="T17">v:pred</ta>
            <ta e="T19" id="Seg_745" s="T18">np:S</ta>
            <ta e="T20" id="Seg_746" s="T19">v:pred</ta>
            <ta e="T22" id="Seg_747" s="T21">np.h:S</ta>
            <ta e="T23" id="Seg_748" s="T22">v:pred</ta>
            <ta e="T24" id="Seg_749" s="T23">pro.h:S</ta>
            <ta e="T25" id="Seg_750" s="T24">s:compl</ta>
            <ta e="T26" id="Seg_751" s="T25">v:pred</ta>
            <ta e="T27" id="Seg_752" s="T26">pro.h:S</ta>
            <ta e="T29" id="Seg_753" s="T28">v:pred</ta>
            <ta e="T30" id="Seg_754" s="T29">pro.h:O</ta>
            <ta e="T32" id="Seg_755" s="T31">0.2.h:S v:pred</ta>
            <ta e="T33" id="Seg_756" s="T32">pro.h:S</ta>
            <ta e="T35" id="Seg_757" s="T34">np:O</ta>
            <ta e="T36" id="Seg_758" s="T35">v:pred</ta>
            <ta e="T38" id="Seg_759" s="T37">np:S</ta>
            <ta e="T42" id="Seg_760" s="T41">np:O</ta>
            <ta e="T43" id="Seg_761" s="T42">0.3.h:S v:pred</ta>
            <ta e="T44" id="Seg_762" s="T43">np.h:S</ta>
            <ta e="T46" id="Seg_763" s="T45">np:S</ta>
            <ta e="T49" id="Seg_764" s="T48">v:pred</ta>
            <ta e="T53" id="Seg_765" s="T52">np.h:S</ta>
            <ta e="T54" id="Seg_766" s="T53">v:pred</ta>
            <ta e="T56" id="Seg_767" s="T55">np:O</ta>
            <ta e="T58" id="Seg_768" s="T57">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_769" s="T0">np.h:A</ta>
            <ta e="T3" id="Seg_770" s="T2">np.h:Com</ta>
            <ta e="T6" id="Seg_771" s="T5">np.h:A</ta>
            <ta e="T7" id="Seg_772" s="T6">np:Th</ta>
            <ta e="T9" id="Seg_773" s="T8">np.h:A</ta>
            <ta e="T10" id="Seg_774" s="T9">np:P</ta>
            <ta e="T13" id="Seg_775" s="T12">np:P</ta>
            <ta e="T15" id="Seg_776" s="T14">0.3.h:A</ta>
            <ta e="T16" id="Seg_777" s="T15">np.h:A</ta>
            <ta e="T17" id="Seg_778" s="T16">np:G</ta>
            <ta e="T19" id="Seg_779" s="T18">np:P</ta>
            <ta e="T21" id="Seg_780" s="T20">np:G</ta>
            <ta e="T22" id="Seg_781" s="T21">np.h:E</ta>
            <ta e="T24" id="Seg_782" s="T23">pro.h:E</ta>
            <ta e="T27" id="Seg_783" s="T26">pro.h:A</ta>
            <ta e="T30" id="Seg_784" s="T29">pro.h:P</ta>
            <ta e="T32" id="Seg_785" s="T31">0.2.h:A</ta>
            <ta e="T33" id="Seg_786" s="T32">pro.h:A</ta>
            <ta e="T34" id="Seg_787" s="T33">pro.h:R</ta>
            <ta e="T35" id="Seg_788" s="T34">np:Th</ta>
            <ta e="T37" id="Seg_789" s="T36">np:Poss</ta>
            <ta e="T38" id="Seg_790" s="T37">np:Th</ta>
            <ta e="T40" id="Seg_791" s="T39">pro.h:Poss</ta>
            <ta e="T42" id="Seg_792" s="T41">np:Th</ta>
            <ta e="T43" id="Seg_793" s="T42">0.3.h:A</ta>
            <ta e="T44" id="Seg_794" s="T43">np.h:Th</ta>
            <ta e="T46" id="Seg_795" s="T45">np:A</ta>
            <ta e="T50" id="Seg_796" s="T49">np:G</ta>
            <ta e="T51" id="Seg_797" s="T50">np.h:Poss</ta>
            <ta e="T52" id="Seg_798" s="T51">np:L</ta>
            <ta e="T53" id="Seg_799" s="T52">np.h:E</ta>
            <ta e="T56" id="Seg_800" s="T55">np:P</ta>
            <ta e="T58" id="Seg_801" s="T57">0.3.h:A</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T13" id="Seg_802" s="T12">RUS:cult </ta>
            <ta e="T17" id="Seg_803" s="T16">RUS:cult </ta>
            <ta e="T39" id="Seg_804" s="T38">RUS:core</ta>
            <ta e="T47" id="Seg_805" s="T46">RUS:core</ta>
            <ta e="T55" id="Seg_806" s="T54">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_807" s="T0">Идя вдоем с Каном сети ставили, два человека. </ta>
            <ta e="T8" id="Seg_808" s="T5">Пёнеге рыбы воровал. </ta>
            <ta e="T15" id="Seg_809" s="T8">Идя смолы накопал, скамейку в лодке смолой намазал.</ta>
            <ta e="T18" id="Seg_810" s="T15">Пёнеге на скамейку сел.</ta>
            <ta e="T21" id="Seg_811" s="T18"> Жопа прилипла к смоле. </ta>
            <ta e="T23" id="Seg_812" s="T21">Пёнеге рассердился. </ta>
            <ta e="T26" id="Seg_813" s="T23">Он [их обоих] съесть захотел.</ta>
            <ta e="T29" id="Seg_814" s="T26">Он [им] ничего не говорит. </ta>
            <ta e="T41" id="Seg_815" s="T29">«Не ешь нас, мы тебе лося добудем, мясо лося лучше, чем наше мясо».</ta>
            <ta e="T45" id="Seg_816" s="T41">Лося погнали, Пёнеге сзади. </ta>
            <ta e="T50" id="Seg_817" s="T45">Лось потом вверх полетел на небо. </ta>
            <ta e="T58" id="Seg_818" s="T50">На небе Бог узнал, всех звездами сделал. </ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_819" s="T0">Itja and Kana caught fish with a net.</ta>
            <ta e="T8" id="Seg_820" s="T5">Pönege stole their fish.</ta>
            <ta e="T15" id="Seg_821" s="T8">Itja picked up tar, smeared the bench (in the boat) with tar.</ta>
            <ta e="T18" id="Seg_822" s="T15">Pönege sat down on the bench.</ta>
            <ta e="T21" id="Seg_823" s="T18">His ass stuck to the tar.</ta>
            <ta e="T23" id="Seg_824" s="T21">Pönege got angry.</ta>
            <ta e="T26" id="Seg_825" s="T23">He wanted to eat [them both].</ta>
            <ta e="T29" id="Seg_826" s="T26">He tells [them] nothing.</ta>
            <ta e="T41" id="Seg_827" s="T29">"Do not eat us, we will shoot an elk for you, elk meat is better than our meat."</ta>
            <ta e="T45" id="Seg_828" s="T41">They hunted an elk, Pönege was in the back.</ta>
            <ta e="T50" id="Seg_829" s="T45">Then the elk flew high in the sky.</ta>
            <ta e="T58" id="Seg_830" s="T50">In heaven god got to know everything and turned them into stars.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_831" s="T0">Idja stellte mit Kana Netze, ‎‎zwei Leute.</ta>
            <ta e="T8" id="Seg_832" s="T5">Pönege stahl die Fische. </ta>
            <ta e="T15" id="Seg_833" s="T8">Idja grub Teer aus, beschmierte die Bank (im Boot) mit Teer. </ta>
            <ta e="T18" id="Seg_834" s="T15">Pönege setzte sich auf die Bank.</ta>
            <ta e="T21" id="Seg_835" s="T18">Sein Arsch blieb am Teer kleben.</ta>
            <ta e="T23" id="Seg_836" s="T21">Pönege ärgerte sich. </ta>
            <ta e="T26" id="Seg_837" s="T23">Er wollte [sie beide] fressen. </ta>
            <ta e="T29" id="Seg_838" s="T26">Er sagt [ihnen] nichts. </ta>
            <ta e="T41" id="Seg_839" s="T29">“Friss uns nicht, wir erlegen für dich einen Elch, Elchfleisch ist besser als unser Fleisch.” </ta>
            <ta e="T45" id="Seg_840" s="T41">Sie jagten zu dritt einen Elch, Pönege [lief] hinten.</ta>
            <ta e="T50" id="Seg_841" s="T45">Der Elch flog dann zum Himmel hinauf. </ta>
            <ta e="T58" id="Seg_842" s="T50">Im Himmel erfuhr Gott alles und verwandelte sie alle in Sterne. </ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_843" s="T0">Идя сеть смотрели с Каном двое</ta>
            <ta e="T8" id="Seg_844" s="T5">черт рыбы воровал</ta>
            <ta e="T15" id="Seg_845" s="T8">Идя накопал смолы бесетку смолой намазал</ta>
            <ta e="T18" id="Seg_846" s="T15">черт на бесетку сел</ta>
            <ta e="T21" id="Seg_847" s="T18">жопа прилипла к смоле</ta>
            <ta e="T23" id="Seg_848" s="T21">черт рассердился</ta>
            <ta e="T26" id="Seg_849" s="T23">их двоих съесть захотел</ta>
            <ta e="T29" id="Seg_850" s="T26">им (их) не говорит</ta>
            <ta e="T41" id="Seg_851" s="T29">нас не съешь мы тебе лося добудем, лося мясо лучше чем наше мясо.</ta>
            <ta e="T45" id="Seg_852" s="T41">лося погнали (их трое было) черт сзади.</ta>
            <ta e="T50" id="Seg_853" s="T45">лось потом вверх полетел на небо</ta>
            <ta e="T58" id="Seg_854" s="T50">на небе бог узнал всех сделал на звездами </ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
