<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KFN_1967_BigBear2_flk</transcription-name>
         <referenced-file url="KFN_1967_BigBear2_flk.wav" />
         <referenced-file url="KFN_1967_BigBear2_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KFN_1967_BigBear2_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">144</ud-information>
            <ud-information attribute-name="# HIAT:w">98</ud-information>
            <ud-information attribute-name="# e">98</ud-information>
            <ud-information attribute-name="# HIAT:u">19</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KFN">
            <abbreviation>KFN</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.622" type="appl" />
         <tli id="T1" time="6.008" type="appl" />
         <tli id="T94" time="6.7005" type="intp" />
         <tli id="T2" time="7.393" type="appl" />
         <tli id="T3" time="8.779" type="appl" />
         <tli id="T4" time="10.164" type="appl" />
         <tli id="T5" time="11.55" type="appl" />
         <tli id="T6" time="13.476" type="appl" />
         <tli id="T7" time="14.415" type="appl" />
         <tli id="T8" time="15.354" type="appl" />
         <tli id="T9" time="16.292" type="appl" />
         <tli id="T10" time="17.231" type="appl" />
         <tli id="T11" time="18.17" type="appl" />
         <tli id="T12" time="20.07301950439073" />
         <tli id="T13" time="20.791" type="appl" />
         <tli id="T14" time="21.554" type="appl" />
         <tli id="T15" time="22.316" type="appl" />
         <tli id="T16" time="23.079" type="appl" />
         <tli id="T17" time="23.841" type="appl" />
         <tli id="T18" time="24.604" type="appl" />
         <tli id="T19" time="25.366" type="appl" />
         <tli id="T20" time="27.036" type="appl" />
         <tli id="T21" time="27.63" type="appl" />
         <tli id="T22" time="28.224" type="appl" />
         <tli id="T23" time="28.817" type="appl" />
         <tli id="T24" time="29.411" type="appl" />
         <tli id="T25" time="30.004" type="appl" />
         <tli id="T26" time="31.246" type="appl" />
         <tli id="T27" time="32.096" type="appl" />
         <tli id="T28" time="32.946" type="appl" />
         <tli id="T29" time="34.90612093158083" />
         <tli id="T30" time="35.947" type="appl" />
         <tli id="T31" time="37.06" type="appl" />
         <tli id="T32" time="38.172" type="appl" />
         <tli id="T33" time="39.284" type="appl" />
         <tli id="T34" time="40.397" type="appl" />
         <tli id="T35" time="41.509" type="appl" />
         <tli id="T36" time="42.621" type="appl" />
         <tli id="T37" time="43.734" type="appl" />
         <tli id="T38" time="44.846" type="appl" />
         <tli id="T39" time="47.417" type="appl" />
         <tli id="T40" time="49.033" type="appl" />
         <tli id="T41" time="50.65" type="appl" />
         <tli id="T95" time="51.38454545454545" type="intp" />
         <tli id="T42" time="52.266" type="appl" />
         <tli id="T43" time="53.882" type="appl" />
         <tli id="T44" time="55.401" type="appl" />
         <tli id="T45" time="56.171" type="appl" />
         <tli id="T46" time="56.94" type="appl" />
         <tli id="T96" time="57.29" type="intp" />
         <tli id="T47" time="57.71" type="appl" />
         <tli id="T48" time="58.479" type="appl" />
         <tli id="T97" time="59.458" type="intp" />
         <tli id="T49" time="60.437" type="appl" />
         <tli id="T98" time="60.931444444444445" type="intp" />
         <tli id="T50" time="61.327" type="appl" />
         <tli id="T51" time="62.218" type="appl" />
         <tli id="T52" time="63.108" type="appl" />
         <tli id="T53" time="63.999" type="appl" />
         <tli id="T54" time="64.889" type="appl" />
         <tli id="T55" time="66.58562564259535" />
         <tli id="T56" time="67.0589515757776" />
         <tli id="T57" time="67.51227782164229" />
         <tli id="T58" time="69.30558311778347" />
         <tli id="T59" time="69.9789059241413" />
         <tli id="T60" time="70.39889935781007" />
         <tli id="T61" time="70.57889654366811" />
         <tli id="T62" time="71.128" type="appl" />
         <tli id="T63" time="73.53218370422775" />
         <tli id="T64" time="74.118" type="appl" />
         <tli id="T65" time="74.725" type="appl" />
         <tli id="T66" time="75.375" type="appl" />
         <tli id="T67" time="76.024" type="appl" />
         <tli id="T68" time="76.674" type="appl" />
         <tli id="T69" time="77.324" type="appl" />
         <tli id="T70" time="77.973" type="appl" />
         <tli id="T71" time="78.623" type="appl" />
         <tli id="T72" time="80.496" type="appl" />
         <tli id="T73" time="81.098" type="appl" />
         <tli id="T74" time="81.7" type="appl" />
         <tli id="T75" time="83.92535454691959" />
         <tli id="T76" time="84.532" type="appl" />
         <tli id="T77" time="85.246" type="appl" />
         <tli id="T78" time="85.96" type="appl" />
         <tli id="T79" time="86.675" type="appl" />
         <tli id="T80" time="87.389" type="appl" />
         <tli id="T81" time="89.58526605778898" />
         <tli id="T99" time="90.38161724370636" type="intp" />
         <tli id="T82" time="91.04524323197084" />
         <tli id="T83" time="92.63855165493642" />
         <tli id="T84" time="93.13854383787542" />
         <tli id="T85" time="93.904" type="appl" />
         <tli id="T86" time="95.413" type="appl" />
         <tli id="T87" time="96.294" type="appl" />
         <tli id="T88" time="97.175" type="appl" />
         <tli id="T89" time="98.668" type="appl" />
         <tli id="T90" time="99.532" type="appl" />
         <tli id="T91" time="100.397" type="appl" />
         <tli id="T92" time="101.262" type="appl" />
         <tli id="T93" time="102.771" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KFN"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T92" id="Seg_0" n="sc" s="T0">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Ugot</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_7" n="HIAT:w" s="T1">Iǯʼe</ts>
                  <nts id="Seg_8" n="HIAT:ip">,</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_11" n="HIAT:w" s="T94">Kana</ts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_14" n="HIAT:w" s="T2">poqrešpukumbaɣ</ts>
                  <nts id="Seg_15" n="HIAT:ip">,</nts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_18" n="HIAT:w" s="T3">qwelp</ts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_21" n="HIAT:w" s="T4">qwadešpukaɣ</ts>
                  <nts id="Seg_22" n="HIAT:ip">.</nts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_25" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_27" n="HIAT:w" s="T5">Pönege</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_30" n="HIAT:w" s="T6">tabjan</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_33" n="HIAT:w" s="T7">annond</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_36" n="HIAT:w" s="T8">omdle</ts>
                  <nts id="Seg_37" n="HIAT:ip">,</nts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_40" n="HIAT:w" s="T9">twerkut</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">qwelp</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">naʒʼ</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_50" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_52" n="HIAT:w" s="T12">Iʒʼe</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_55" n="HIAT:w" s="T13">patom</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_58" n="HIAT:w" s="T14">qoštembat</ts>
                  <nts id="Seg_59" n="HIAT:ip">:</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_62" n="HIAT:w" s="T15">tab</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_65" n="HIAT:w" s="T16">mi</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_68" n="HIAT:w" s="T17">qweleut</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_71" n="HIAT:w" s="T18">twelešpat</ts>
                  <nts id="Seg_72" n="HIAT:ip">.</nts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_75" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_77" n="HIAT:w" s="T19">Iʒʼe</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_80" n="HIAT:w" s="T20">maʒʼönd</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_83" n="HIAT:w" s="T21">qwenna</ts>
                  <nts id="Seg_84" n="HIAT:ip">,</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_87" n="HIAT:w" s="T22">sap</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_90" n="HIAT:w" s="T23">kočʼek</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_92" n="HIAT:ip">(</nts>
                  <ts e="T25" id="Seg_94" n="HIAT:w" s="T24">paqqamüt</ts>
                  <nts id="Seg_95" n="HIAT:ip">)</nts>
                  <nts id="Seg_96" n="HIAT:ip">.</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_99" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_101" n="HIAT:w" s="T25">Besedkamdə</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_104" n="HIAT:w" s="T26">sahe</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_107" n="HIAT:w" s="T27">čʼoutə</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_110" n="HIAT:w" s="T28">naʒʼ</ts>
                  <nts id="Seg_111" n="HIAT:ip">.</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_114" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_116" n="HIAT:w" s="T29">No</ts>
                  <nts id="Seg_117" n="HIAT:ip">,</nts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_120" n="HIAT:w" s="T30">qwenna</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_123" n="HIAT:w" s="T31">Pönege</ts>
                  <nts id="Seg_124" n="HIAT:ip">,</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_127" n="HIAT:w" s="T32">omda</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_130" n="HIAT:w" s="T33">annondə</ts>
                  <nts id="Seg_131" n="HIAT:ip">,</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_134" n="HIAT:w" s="T34">tadə</ts>
                  <nts id="Seg_135" n="HIAT:ip">,</nts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_138" n="HIAT:w" s="T35">sandə</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_140" n="HIAT:ip">—</nts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_143" n="HIAT:w" s="T36">tat</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_146" n="HIAT:w" s="T37">rokwanna</ts>
                  <nts id="Seg_147" n="HIAT:ip">.</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_150" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_152" n="HIAT:w" s="T38">Potom</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_155" n="HIAT:w" s="T39">nʼajwanna</ts>
                  <nts id="Seg_156" n="HIAT:ip">:</nts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_159" n="HIAT:w" s="T40">«Qwelage</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_162" n="HIAT:w" s="T41">Iǯep</ts>
                  <nts id="Seg_163" n="HIAT:ip">,</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_166" n="HIAT:w" s="T95">Kanap</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_169" n="HIAT:w" s="T42">amgu»</ts>
                  <nts id="Seg_170" n="HIAT:ip">.</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_173" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_175" n="HIAT:w" s="T43">Tab</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_178" n="HIAT:w" s="T44">töa</ts>
                  <nts id="Seg_179" n="HIAT:ip">,</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_182" n="HIAT:w" s="T45">Pönege</ts>
                  <nts id="Seg_183" n="HIAT:ip">,</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_186" n="HIAT:w" s="T46">Iʒʼep</ts>
                  <nts id="Seg_187" n="HIAT:ip">,</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_190" n="HIAT:w" s="T96">Kanam</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_193" n="HIAT:w" s="T47">amgu</ts>
                  <nts id="Seg_194" n="HIAT:ip">.</nts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_197" n="HIAT:u" s="T48">
                  <nts id="Seg_198" n="HIAT:ip">(</nts>
                  <ts e="T97" id="Seg_200" n="HIAT:w" s="T48">Kana</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_203" n="HIAT:w" s="T97">Pö-</ts>
                  <nts id="Seg_204" n="HIAT:ip">)</nts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_207" n="HIAT:w" s="T49">Kana</ts>
                  <nts id="Seg_208" n="HIAT:ip">,</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_211" n="HIAT:w" s="T98">Iǯʼe</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_214" n="HIAT:w" s="T50">čenča</ts>
                  <nts id="Seg_215" n="HIAT:ip">:</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_218" n="HIAT:w" s="T51">«Tat</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_221" n="HIAT:w" s="T52">miǯe</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_224" n="HIAT:w" s="T53">ɨg</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_227" n="HIAT:w" s="T54">awešk</ts>
                  <nts id="Seg_228" n="HIAT:ip">.</nts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_231" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_233" n="HIAT:w" s="T55">Mi</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_236" n="HIAT:w" s="T56">peqqam</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_239" n="HIAT:w" s="T57">nʼodɨlajhe</ts>
                  <nts id="Seg_240" n="HIAT:ip">.</nts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_243" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_245" n="HIAT:w" s="T58">Peqqan</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_248" n="HIAT:w" s="T59">waʒʼe</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_251" n="HIAT:w" s="T60">mi</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_254" n="HIAT:w" s="T61">waʒʼenan</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_257" n="HIAT:w" s="T62">nʼüwella»</ts>
                  <nts id="Seg_258" n="HIAT:ip">.</nts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_261" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_263" n="HIAT:w" s="T63">Pöqqam</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_266" n="HIAT:w" s="T64">nʼodat</ts>
                  <nts id="Seg_267" n="HIAT:ip">.</nts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_270" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_272" n="HIAT:w" s="T65">Pönege</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_275" n="HIAT:w" s="T66">sadʼi</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_278" n="HIAT:w" s="T67">nʼodembat</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_281" n="HIAT:w" s="T68">peqqən</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_284" n="HIAT:w" s="T69">waʒʼem</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_287" n="HIAT:w" s="T70">amgu</ts>
                  <nts id="Seg_288" n="HIAT:ip">.</nts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_291" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_293" n="HIAT:w" s="T71">Peqqa</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_296" n="HIAT:w" s="T72">Nomn</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_299" n="HIAT:w" s="T73">enne</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_302" n="HIAT:w" s="T74">wašeʒʼänə</ts>
                  <nts id="Seg_303" n="HIAT:ip">.</nts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_306" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_308" n="HIAT:w" s="T75">A</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_311" n="HIAT:w" s="T76">Nop</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_314" n="HIAT:w" s="T77">koštɨd</ts>
                  <nts id="Seg_315" n="HIAT:ip">:</nts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_318" n="HIAT:w" s="T78">«Qaj</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_321" n="HIAT:w" s="T79">nɨlʼǯʼi</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_324" n="HIAT:w" s="T80">töndat</ts>
                  <nts id="Seg_325" n="HIAT:ip">?</nts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_328" n="HIAT:u" s="T81">
                  <ts e="T99" id="Seg_330" n="HIAT:w" s="T81">Kana</ts>
                  <nts id="Seg_331" n="HIAT:ip">,</nts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_334" n="HIAT:w" s="T99">Iʒʼe</ts>
                  <nts id="Seg_335" n="HIAT:ip">,</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_338" n="HIAT:w" s="T82">Pönege</ts>
                  <nts id="Seg_339" n="HIAT:ip">.</nts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_342" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_344" n="HIAT:w" s="T83">Qaj</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_347" n="HIAT:w" s="T84">megu</ts>
                  <nts id="Seg_348" n="HIAT:ip">?</nts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_351" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_353" n="HIAT:w" s="T85">Mat</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_356" n="HIAT:w" s="T86">qɨškaxajtko</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_359" n="HIAT:w" s="T87">mejap</ts>
                  <nts id="Seg_360" n="HIAT:ip">.</nts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_363" n="HIAT:u" s="T88">
                  <nts id="Seg_364" n="HIAT:ip">(</nts>
                  <ts e="T89" id="Seg_366" n="HIAT:w" s="T88">Wes</ts>
                  <nts id="Seg_367" n="HIAT:ip">)</nts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_370" n="HIAT:w" s="T89">qɨškaxajoɣɨt</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_373" n="HIAT:w" s="T90">ti</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_376" n="HIAT:w" s="T91">ejält»</ts>
                  <nts id="Seg_377" n="HIAT:ip">.</nts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T92" id="Seg_379" n="sc" s="T0">
               <ts e="T1" id="Seg_381" n="e" s="T0">Ugot </ts>
               <ts e="T94" id="Seg_383" n="e" s="T1">Iǯʼe, </ts>
               <ts e="T2" id="Seg_385" n="e" s="T94">Kana </ts>
               <ts e="T3" id="Seg_387" n="e" s="T2">poqrešpukumbaɣ, </ts>
               <ts e="T4" id="Seg_389" n="e" s="T3">qwelp </ts>
               <ts e="T5" id="Seg_391" n="e" s="T4">qwadešpukaɣ. </ts>
               <ts e="T6" id="Seg_393" n="e" s="T5">Pönege </ts>
               <ts e="T7" id="Seg_395" n="e" s="T6">tabjan </ts>
               <ts e="T8" id="Seg_397" n="e" s="T7">annond </ts>
               <ts e="T9" id="Seg_399" n="e" s="T8">omdle, </ts>
               <ts e="T10" id="Seg_401" n="e" s="T9">twerkut </ts>
               <ts e="T11" id="Seg_403" n="e" s="T10">qwelp </ts>
               <ts e="T12" id="Seg_405" n="e" s="T11">naʒʼ. </ts>
               <ts e="T13" id="Seg_407" n="e" s="T12">Iʒʼe </ts>
               <ts e="T14" id="Seg_409" n="e" s="T13">patom </ts>
               <ts e="T15" id="Seg_411" n="e" s="T14">qoštembat: </ts>
               <ts e="T16" id="Seg_413" n="e" s="T15">tab </ts>
               <ts e="T17" id="Seg_415" n="e" s="T16">mi </ts>
               <ts e="T18" id="Seg_417" n="e" s="T17">qweleut </ts>
               <ts e="T19" id="Seg_419" n="e" s="T18">twelešpat. </ts>
               <ts e="T20" id="Seg_421" n="e" s="T19">Iʒʼe </ts>
               <ts e="T21" id="Seg_423" n="e" s="T20">maʒʼönd </ts>
               <ts e="T22" id="Seg_425" n="e" s="T21">qwenna, </ts>
               <ts e="T23" id="Seg_427" n="e" s="T22">sap </ts>
               <ts e="T24" id="Seg_429" n="e" s="T23">kočʼek </ts>
               <ts e="T25" id="Seg_431" n="e" s="T24">(paqqamüt). </ts>
               <ts e="T26" id="Seg_433" n="e" s="T25">Besedkamdə </ts>
               <ts e="T27" id="Seg_435" n="e" s="T26">sahe </ts>
               <ts e="T28" id="Seg_437" n="e" s="T27">čʼoutə </ts>
               <ts e="T29" id="Seg_439" n="e" s="T28">naʒʼ. </ts>
               <ts e="T30" id="Seg_441" n="e" s="T29">No, </ts>
               <ts e="T31" id="Seg_443" n="e" s="T30">qwenna </ts>
               <ts e="T32" id="Seg_445" n="e" s="T31">Pönege, </ts>
               <ts e="T33" id="Seg_447" n="e" s="T32">omda </ts>
               <ts e="T34" id="Seg_449" n="e" s="T33">annondə, </ts>
               <ts e="T35" id="Seg_451" n="e" s="T34">tadə, </ts>
               <ts e="T36" id="Seg_453" n="e" s="T35">sandə — </ts>
               <ts e="T37" id="Seg_455" n="e" s="T36">tat </ts>
               <ts e="T38" id="Seg_457" n="e" s="T37">rokwanna. </ts>
               <ts e="T39" id="Seg_459" n="e" s="T38">Potom </ts>
               <ts e="T40" id="Seg_461" n="e" s="T39">nʼajwanna: </ts>
               <ts e="T41" id="Seg_463" n="e" s="T40">«Qwelage </ts>
               <ts e="T95" id="Seg_465" n="e" s="T41">Iǯep, </ts>
               <ts e="T42" id="Seg_467" n="e" s="T95">Kanap </ts>
               <ts e="T43" id="Seg_469" n="e" s="T42">amgu». </ts>
               <ts e="T44" id="Seg_471" n="e" s="T43">Tab </ts>
               <ts e="T45" id="Seg_473" n="e" s="T44">töa, </ts>
               <ts e="T46" id="Seg_475" n="e" s="T45">Pönege, </ts>
               <ts e="T96" id="Seg_477" n="e" s="T46">Iʒʼep, </ts>
               <ts e="T47" id="Seg_479" n="e" s="T96">Kanam </ts>
               <ts e="T48" id="Seg_481" n="e" s="T47">amgu. </ts>
               <ts e="T97" id="Seg_483" n="e" s="T48">(Kana </ts>
               <ts e="T49" id="Seg_485" n="e" s="T97">Pö-) </ts>
               <ts e="T98" id="Seg_487" n="e" s="T49">Kana, </ts>
               <ts e="T50" id="Seg_489" n="e" s="T98">Iǯʼe </ts>
               <ts e="T51" id="Seg_491" n="e" s="T50">čenča: </ts>
               <ts e="T52" id="Seg_493" n="e" s="T51">«Tat </ts>
               <ts e="T53" id="Seg_495" n="e" s="T52">miǯe </ts>
               <ts e="T54" id="Seg_497" n="e" s="T53">ɨg </ts>
               <ts e="T55" id="Seg_499" n="e" s="T54">awešk. </ts>
               <ts e="T56" id="Seg_501" n="e" s="T55">Mi </ts>
               <ts e="T57" id="Seg_503" n="e" s="T56">peqqam </ts>
               <ts e="T58" id="Seg_505" n="e" s="T57">nʼodɨlajhe. </ts>
               <ts e="T59" id="Seg_507" n="e" s="T58">Peqqan </ts>
               <ts e="T60" id="Seg_509" n="e" s="T59">waʒʼe </ts>
               <ts e="T61" id="Seg_511" n="e" s="T60">mi </ts>
               <ts e="T62" id="Seg_513" n="e" s="T61">waʒʼenan </ts>
               <ts e="T63" id="Seg_515" n="e" s="T62">nʼüwella». </ts>
               <ts e="T64" id="Seg_517" n="e" s="T63">Pöqqam </ts>
               <ts e="T65" id="Seg_519" n="e" s="T64">nʼodat. </ts>
               <ts e="T66" id="Seg_521" n="e" s="T65">Pönege </ts>
               <ts e="T67" id="Seg_523" n="e" s="T66">sadʼi </ts>
               <ts e="T68" id="Seg_525" n="e" s="T67">nʼodembat </ts>
               <ts e="T69" id="Seg_527" n="e" s="T68">peqqən </ts>
               <ts e="T70" id="Seg_529" n="e" s="T69">waʒʼem </ts>
               <ts e="T71" id="Seg_531" n="e" s="T70">amgu. </ts>
               <ts e="T72" id="Seg_533" n="e" s="T71">Peqqa </ts>
               <ts e="T73" id="Seg_535" n="e" s="T72">Nomn </ts>
               <ts e="T74" id="Seg_537" n="e" s="T73">enne </ts>
               <ts e="T75" id="Seg_539" n="e" s="T74">wašeʒʼänə. </ts>
               <ts e="T76" id="Seg_541" n="e" s="T75">A </ts>
               <ts e="T77" id="Seg_543" n="e" s="T76">Nop </ts>
               <ts e="T78" id="Seg_545" n="e" s="T77">koštɨd: </ts>
               <ts e="T79" id="Seg_547" n="e" s="T78">«Qaj </ts>
               <ts e="T80" id="Seg_549" n="e" s="T79">nɨlʼǯʼi </ts>
               <ts e="T81" id="Seg_551" n="e" s="T80">töndat? </ts>
               <ts e="T99" id="Seg_553" n="e" s="T81">Kana, </ts>
               <ts e="T82" id="Seg_555" n="e" s="T99">Iʒʼe, </ts>
               <ts e="T83" id="Seg_557" n="e" s="T82">Pönege. </ts>
               <ts e="T84" id="Seg_559" n="e" s="T83">Qaj </ts>
               <ts e="T85" id="Seg_561" n="e" s="T84">megu? </ts>
               <ts e="T86" id="Seg_563" n="e" s="T85">Mat </ts>
               <ts e="T87" id="Seg_565" n="e" s="T86">qɨškaxajtko </ts>
               <ts e="T88" id="Seg_567" n="e" s="T87">mejap. </ts>
               <ts e="T89" id="Seg_569" n="e" s="T88">(Wes) </ts>
               <ts e="T90" id="Seg_571" n="e" s="T89">qɨškaxajoɣɨt </ts>
               <ts e="T91" id="Seg_573" n="e" s="T90">ti </ts>
               <ts e="T92" id="Seg_575" n="e" s="T91">ejält». </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_576" s="T0">KFN_1967_BigBear2_flk.001 (001)</ta>
            <ta e="T12" id="Seg_577" s="T5">KFN_1967_BigBear2_flk.002 (002)</ta>
            <ta e="T19" id="Seg_578" s="T12">KFN_1967_BigBear2_flk.003 (003)</ta>
            <ta e="T25" id="Seg_579" s="T19">KFN_1967_BigBear2_flk.004 (004)</ta>
            <ta e="T29" id="Seg_580" s="T25">KFN_1967_BigBear2_flk.005 (005)</ta>
            <ta e="T38" id="Seg_581" s="T29">KFN_1967_BigBear2_flk.006 (006)</ta>
            <ta e="T43" id="Seg_582" s="T38">KFN_1967_BigBear2_flk.007 (007)</ta>
            <ta e="T48" id="Seg_583" s="T43">KFN_1967_BigBear2_flk.008 (008)</ta>
            <ta e="T55" id="Seg_584" s="T48">KFN_1967_BigBear2_flk.009 (009)</ta>
            <ta e="T58" id="Seg_585" s="T55">KFN_1967_BigBear2_flk.010 (010.001)</ta>
            <ta e="T63" id="Seg_586" s="T58">KFN_1967_BigBear2_flk.011 (010.002)</ta>
            <ta e="T65" id="Seg_587" s="T63">KFN_1967_BigBear2_flk.012 (011)</ta>
            <ta e="T71" id="Seg_588" s="T65">KFN_1967_BigBear2_flk.013 (012)</ta>
            <ta e="T75" id="Seg_589" s="T71">KFN_1967_BigBear2_flk.014 (013)</ta>
            <ta e="T81" id="Seg_590" s="T75">KFN_1967_BigBear2_flk.015 (014)</ta>
            <ta e="T83" id="Seg_591" s="T81">KFN_1967_BigBear2_flk.016 (015.001)</ta>
            <ta e="T85" id="Seg_592" s="T83">KFN_1967_BigBear2_flk.017 (015.002)</ta>
            <ta e="T88" id="Seg_593" s="T85">KFN_1967_BigBear2_flk.018 (016)</ta>
            <ta e="T92" id="Seg_594" s="T88">KFN_1967_BigBear2_flk.019 (017)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_595" s="T0">Уго́т Идже-ка́на поӄре́шпукумбаӷ, квэлп квадэ́шпукаӷ.</ta>
            <ta e="T12" id="Seg_596" s="T5">Пӧнэге табья́н анно́н омдле́, вэ́ркут квэлп най.</ta>
            <ta e="T19" id="Seg_597" s="T12">И́дже потом коштэмба́т: таб мi квэ́лэут твэ́лешпат.</ta>
            <ta e="T25" id="Seg_598" s="T19">И́дже маджӧнд квэ́нна, сап ко́чек пакэлба́т.</ta>
            <ta e="T29" id="Seg_599" s="T25">Бесе́дкамд са́ҳе чӧут най.</ta>
            <ta e="T38" id="Seg_600" s="T29">Но, квэ́нна Пӧнэге, омда́ анно́нд, тад, шанд — тат роква́нна.</ta>
            <ta e="T43" id="Seg_601" s="T38">Потом няйва́нна: «Квэ́лаге И́джеп-ка́нап амгу́».</ta>
            <ta e="T48" id="Seg_602" s="T43">Таб тӧа, Пӧнэге, Идже-ка́нам амгу́.</ta>
            <ta e="T55" id="Seg_603" s="T48">Ка́на, ка́на-И́дже ҷэ́нҷа: «Тат И́джеп ыг аве́шпык.</ta>
            <ta e="T58" id="Seg_604" s="T55">Мi пе́ӄӄэп нӧдлайҳе.</ta>
            <ta e="T63" id="Seg_605" s="T58">Пе́ӄӄэн ва́джем мi ва́дженан нӱелла».</ta>
            <ta e="T65" id="Seg_606" s="T63">Пе́ӄӄэм нӧдат.</ta>
            <ta e="T71" id="Seg_607" s="T65">Пӧнэге сзади нӧдэмбат — пе́ӄӄэн ва́джем амгу́.</ta>
            <ta e="T75" id="Seg_608" s="T71">Пеӄӄ Номн эннэ́ вашэ́джӓ.</ta>
            <ta e="T81" id="Seg_609" s="T75">А Ноп кошты́д: «Кай ныльджи́ тӧнда?</ta>
            <ta e="T83" id="Seg_610" s="T81">Ка́на-И́дже, Пӧнэге. </ta>
            <ta e="T85" id="Seg_611" s="T83">Кай ме́гу?</ta>
            <ta e="T88" id="Seg_612" s="T85">Мат кышкаха́йтко ме́яп.</ta>
            <ta e="T92" id="Seg_613" s="T88">Вес кышкахайӧгыт тi эйӓлт».</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_614" s="T0">Ugot Iǯe-kana poqrešpukumbaɣ, kvelp kvadešpukaɣ.</ta>
            <ta e="T12" id="Seg_615" s="T5">Pönege tabjan annon omdle, verkut kvelp naj.</ta>
            <ta e="T19" id="Seg_616" s="T12">Iǯe potom koštembat: tab mi kveleut tvelešpat.</ta>
            <ta e="T25" id="Seg_617" s="T19">Iǯe maǯönd kvenna, sap koček pakelbat.</ta>
            <ta e="T29" id="Seg_618" s="T25">Besedkamd saxe čöut naj.</ta>
            <ta e="T38" id="Seg_619" s="T29">No, kvenna Pönege, omda annond, tad, šand — tat rokvanna.</ta>
            <ta e="T43" id="Seg_620" s="T38">Potom nʼajvanna: «Kvelage Iǯep-kanap amgu».</ta>
            <ta e="T48" id="Seg_621" s="T43">Tab töa, Pönege, Iǯe-kanam amgu.</ta>
            <ta e="T55" id="Seg_622" s="T48">Kana, kana-Iǯe čenča: «Tat Iǯep ɨg avešpɨk.</ta>
            <ta e="T58" id="Seg_623" s="T55">Mi peqqep nödlajxe.</ta>
            <ta e="T63" id="Seg_624" s="T58">Peqqen vaǯem mi vaǯenan nüella».</ta>
            <ta e="T65" id="Seg_625" s="T63">Peqqem nödat.</ta>
            <ta e="T71" id="Seg_626" s="T65">Pönege szadi nödembat — peqqen vaǯem amgu.</ta>
            <ta e="T75" id="Seg_627" s="T71">Peqq Nomn enne vašeǯä.</ta>
            <ta e="T81" id="Seg_628" s="T75">A Nop koštɨd: «Kaj nɨlʼǯi tönda?</ta>
            <ta e="T83" id="Seg_629" s="T81">Kana-Iǯe, Pönege.</ta>
            <ta e="T85" id="Seg_630" s="T83">Kaj megu?</ta>
            <ta e="T88" id="Seg_631" s="T85">Mat kɨškaxajtko mejap.</ta>
            <ta e="T92" id="Seg_632" s="T88">Ves kɨškaxajögɨt ti ejält».</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_633" s="T0">Ugot Iǯʼe, Kana poqrešpukumbaɣ, qwelp qwadešpukaɣ. </ta>
            <ta e="T12" id="Seg_634" s="T5">Pönege tabjan annond omdle, twerkut qwelp naʒʼ. </ta>
            <ta e="T19" id="Seg_635" s="T12">Iʒʼe patom qoštembat: tab mi qweleut twelešpat. </ta>
            <ta e="T25" id="Seg_636" s="T19">Iʒʼe maʒʼönd qwenna, sap kočʼek (paqqamüt). </ta>
            <ta e="T29" id="Seg_637" s="T25">Besedkamdə sahe čʼoutə naʒʼ. </ta>
            <ta e="T38" id="Seg_638" s="T29">No, qwenna Pönege, omda annondə, tadə, sandə — tat rokwanna. </ta>
            <ta e="T43" id="Seg_639" s="T38">Potom nʼajwanna: «Qwelage Iǯep, Kanap amgu». </ta>
            <ta e="T48" id="Seg_640" s="T43">Tab töa, Pönege, Iʒʼep, Kanam amgu. </ta>
            <ta e="T55" id="Seg_641" s="T48">(Kana Pö-) Kana, Iǯʼe čenča: «Tat miǯe ɨg awešk. </ta>
            <ta e="T58" id="Seg_642" s="T55">Mi peqqam nʼodɨlajhe. </ta>
            <ta e="T63" id="Seg_643" s="T58">Peqqan waʒʼe mi waʒʼenan nʼüwella». </ta>
            <ta e="T65" id="Seg_644" s="T63">Pöqqam nʼodat. </ta>
            <ta e="T71" id="Seg_645" s="T65">Pönege sadʼi nʼodembat — peqqən waʒʼem amgu. </ta>
            <ta e="T75" id="Seg_646" s="T71">Peqqa Nomn enne wašeʒʼänə. </ta>
            <ta e="T81" id="Seg_647" s="T75">A Nop koštɨd: «Qaj nɨlʼǯʼi töndat? </ta>
            <ta e="T83" id="Seg_648" s="T81">Kana, Iʒʼe, Pönege. </ta>
            <ta e="T85" id="Seg_649" s="T83">Qaj megu? </ta>
            <ta e="T88" id="Seg_650" s="T85">Mat qɨškaxajtko mejap. </ta>
            <ta e="T92" id="Seg_651" s="T88">(Wes) qɨškaxajoɣɨt ti ejält». </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_652" s="T0">ugot</ta>
            <ta e="T94" id="Seg_653" s="T1">Iǯʼe</ta>
            <ta e="T2" id="Seg_654" s="T94">Kana</ta>
            <ta e="T3" id="Seg_655" s="T2">poq-r-e-špu-ku-mba-ɣ</ta>
            <ta e="T4" id="Seg_656" s="T3">qwel-p</ta>
            <ta e="T5" id="Seg_657" s="T4">qwad-e-špu-ka-ɣ</ta>
            <ta e="T6" id="Seg_658" s="T5">pönege</ta>
            <ta e="T7" id="Seg_659" s="T6">tab-ja-n</ta>
            <ta e="T8" id="Seg_660" s="T7">anno-nd</ta>
            <ta e="T9" id="Seg_661" s="T8">omd-le</ta>
            <ta e="T10" id="Seg_662" s="T9">twe-r-ku-t</ta>
            <ta e="T11" id="Seg_663" s="T10">qwel-p</ta>
            <ta e="T12" id="Seg_664" s="T11">naʒʼ</ta>
            <ta e="T13" id="Seg_665" s="T12">Iʒʼe</ta>
            <ta e="T14" id="Seg_666" s="T13">patom</ta>
            <ta e="T15" id="Seg_667" s="T14">qošte-mba-t</ta>
            <ta e="T16" id="Seg_668" s="T15">tab</ta>
            <ta e="T17" id="Seg_669" s="T16">mi</ta>
            <ta e="T18" id="Seg_670" s="T17">qwel-e-ut</ta>
            <ta e="T19" id="Seg_671" s="T18">twe-le-špa-t</ta>
            <ta e="T20" id="Seg_672" s="T19">Iʒʼe</ta>
            <ta e="T21" id="Seg_673" s="T20">maʒʼö-nd</ta>
            <ta e="T22" id="Seg_674" s="T21">qwen-na</ta>
            <ta e="T23" id="Seg_675" s="T22">sa-p</ta>
            <ta e="T24" id="Seg_676" s="T23">kočʼek-k</ta>
            <ta e="T25" id="Seg_677" s="T24">paqqa-mü-t</ta>
            <ta e="T26" id="Seg_678" s="T25">besedka-m-də</ta>
            <ta e="T27" id="Seg_679" s="T26">sa-he</ta>
            <ta e="T28" id="Seg_680" s="T27">čʼo-u-tə</ta>
            <ta e="T29" id="Seg_681" s="T28">naʒʼ</ta>
            <ta e="T30" id="Seg_682" s="T29">no</ta>
            <ta e="T31" id="Seg_683" s="T30">qwen-na</ta>
            <ta e="T32" id="Seg_684" s="T31">Pönege</ta>
            <ta e="T33" id="Seg_685" s="T32">omda</ta>
            <ta e="T34" id="Seg_686" s="T33">anno-ndə</ta>
            <ta e="T35" id="Seg_687" s="T34">tadə</ta>
            <ta e="T36" id="Seg_688" s="T35">sandə</ta>
            <ta e="T37" id="Seg_689" s="T36">tat</ta>
            <ta e="T38" id="Seg_690" s="T37">rokwan-na</ta>
            <ta e="T39" id="Seg_691" s="T38">potom</ta>
            <ta e="T40" id="Seg_692" s="T39">nʼajwan-na</ta>
            <ta e="T41" id="Seg_693" s="T40">qwe-la-ge</ta>
            <ta e="T95" id="Seg_694" s="T41">Iǯe-p</ta>
            <ta e="T42" id="Seg_695" s="T95">Kana-p</ta>
            <ta e="T43" id="Seg_696" s="T42">am-gu</ta>
            <ta e="T44" id="Seg_697" s="T43">tab</ta>
            <ta e="T45" id="Seg_698" s="T44">tö-a</ta>
            <ta e="T46" id="Seg_699" s="T45">Pönege</ta>
            <ta e="T96" id="Seg_700" s="T46">Iʒʼe-p</ta>
            <ta e="T47" id="Seg_701" s="T96">Kana-m</ta>
            <ta e="T48" id="Seg_702" s="T47">am-gu</ta>
            <ta e="T97" id="Seg_703" s="T48">Kana</ta>
            <ta e="T98" id="Seg_704" s="T49">Kana</ta>
            <ta e="T50" id="Seg_705" s="T98">Iǯʼe</ta>
            <ta e="T51" id="Seg_706" s="T50">čenča</ta>
            <ta e="T52" id="Seg_707" s="T51">tat</ta>
            <ta e="T53" id="Seg_708" s="T52">miǯe</ta>
            <ta e="T54" id="Seg_709" s="T53">ɨg</ta>
            <ta e="T55" id="Seg_710" s="T54">aw-ešk</ta>
            <ta e="T56" id="Seg_711" s="T55">mi</ta>
            <ta e="T57" id="Seg_712" s="T56">peqqa-m</ta>
            <ta e="T58" id="Seg_713" s="T57">nʼo-dɨ-la-j-he</ta>
            <ta e="T59" id="Seg_714" s="T58">peqqa-n</ta>
            <ta e="T60" id="Seg_715" s="T59">waʒʼe</ta>
            <ta e="T61" id="Seg_716" s="T60">mi</ta>
            <ta e="T62" id="Seg_717" s="T61">waʒʼe-nan</ta>
            <ta e="T63" id="Seg_718" s="T62">nʼüwe-lla</ta>
            <ta e="T64" id="Seg_719" s="T63">pöqqa-m</ta>
            <ta e="T65" id="Seg_720" s="T64">nʼo-da-t</ta>
            <ta e="T66" id="Seg_721" s="T65">pönege</ta>
            <ta e="T67" id="Seg_722" s="T66">sadʼi</ta>
            <ta e="T68" id="Seg_723" s="T67">nʼo-de-mba-t</ta>
            <ta e="T69" id="Seg_724" s="T68">peqqə-n</ta>
            <ta e="T70" id="Seg_725" s="T69">waʒʼe-m</ta>
            <ta e="T71" id="Seg_726" s="T70">am-gu</ta>
            <ta e="T72" id="Seg_727" s="T71">peqqa</ta>
            <ta e="T73" id="Seg_728" s="T72">nom-n</ta>
            <ta e="T74" id="Seg_729" s="T73">inne</ta>
            <ta e="T75" id="Seg_730" s="T74">waše-ʒʼä-nə</ta>
            <ta e="T76" id="Seg_731" s="T75">a</ta>
            <ta e="T77" id="Seg_732" s="T76">Nop</ta>
            <ta e="T78" id="Seg_733" s="T77">koštɨ-d</ta>
            <ta e="T79" id="Seg_734" s="T78">qaj</ta>
            <ta e="T80" id="Seg_735" s="T79">nɨlʼǯʼi</ta>
            <ta e="T81" id="Seg_736" s="T80">tö-nda-t</ta>
            <ta e="T99" id="Seg_737" s="T81">Kana</ta>
            <ta e="T82" id="Seg_738" s="T99">Iʒʼe</ta>
            <ta e="T83" id="Seg_739" s="T82">Pönege</ta>
            <ta e="T84" id="Seg_740" s="T83">qaj</ta>
            <ta e="T85" id="Seg_741" s="T84">me-gu</ta>
            <ta e="T86" id="Seg_742" s="T85">mat</ta>
            <ta e="T87" id="Seg_743" s="T86">qɨška-xaj-tko</ta>
            <ta e="T88" id="Seg_744" s="T87">me-ja-p</ta>
            <ta e="T89" id="Seg_745" s="T88">wes</ta>
            <ta e="T90" id="Seg_746" s="T89">qɨška-xajo-ɣɨt</ta>
            <ta e="T91" id="Seg_747" s="T90">ti</ta>
            <ta e="T92" id="Seg_748" s="T91">e-jä-lt</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_749" s="T0">ugon</ta>
            <ta e="T94" id="Seg_750" s="T1">Iʒe</ta>
            <ta e="T2" id="Seg_751" s="T94">Kana</ta>
            <ta e="T3" id="Seg_752" s="T2">poq-r-ɨ-špɨ-ku-mbɨ-q</ta>
            <ta e="T4" id="Seg_753" s="T3">qwel-p</ta>
            <ta e="T5" id="Seg_754" s="T4">qwat-ɨ-špɨ-ku-q</ta>
            <ta e="T6" id="Seg_755" s="T5">Pöːnege</ta>
            <ta e="T7" id="Seg_756" s="T6">tab-štja-ni</ta>
            <ta e="T8" id="Seg_757" s="T7">ande-nde</ta>
            <ta e="T9" id="Seg_758" s="T8">omde-le</ta>
            <ta e="T10" id="Seg_759" s="T9">twe-r-ku-tɨ</ta>
            <ta e="T11" id="Seg_760" s="T10">qwel-p</ta>
            <ta e="T12" id="Seg_761" s="T11">naj</ta>
            <ta e="T13" id="Seg_762" s="T12">Iǯe</ta>
            <ta e="T14" id="Seg_763" s="T13">patom</ta>
            <ta e="T15" id="Seg_764" s="T14">koštɨ-mbɨ-tɨ</ta>
            <ta e="T16" id="Seg_765" s="T15">tab</ta>
            <ta e="T17" id="Seg_766" s="T16">mi</ta>
            <ta e="T18" id="Seg_767" s="T17">qwel-ɨ-ut</ta>
            <ta e="T19" id="Seg_768" s="T18">twe-lɨ-špɨ-tɨ</ta>
            <ta e="T20" id="Seg_769" s="T19">Iǯe</ta>
            <ta e="T21" id="Seg_770" s="T20">maǯʼo-nde</ta>
            <ta e="T22" id="Seg_771" s="T21">qwän-ŋɨ</ta>
            <ta e="T23" id="Seg_772" s="T22">sa-m</ta>
            <ta e="T24" id="Seg_773" s="T23">koček-k</ta>
            <ta e="T25" id="Seg_774" s="T24">paqə-%%-t</ta>
            <ta e="T26" id="Seg_775" s="T25">besetka-m-tɨ</ta>
            <ta e="T27" id="Seg_776" s="T26">sa-se</ta>
            <ta e="T28" id="Seg_777" s="T27">čö-wa-tɨ</ta>
            <ta e="T29" id="Seg_778" s="T28">naj</ta>
            <ta e="T30" id="Seg_779" s="T29">nu</ta>
            <ta e="T31" id="Seg_780" s="T30">qwän-ŋɨ</ta>
            <ta e="T32" id="Seg_781" s="T31">Pöːnege</ta>
            <ta e="T33" id="Seg_782" s="T32">omde</ta>
            <ta e="T34" id="Seg_783" s="T33">ande-nde</ta>
            <ta e="T35" id="Seg_784" s="T34">taːd</ta>
            <ta e="T36" id="Seg_785" s="T35">šandɛ</ta>
            <ta e="T37" id="Seg_786" s="T36">tat</ta>
            <ta e="T38" id="Seg_787" s="T37">rokuat-ŋɨ</ta>
            <ta e="T39" id="Seg_788" s="T38">patom</ta>
            <ta e="T40" id="Seg_789" s="T39">nʼajwat-ŋɨ</ta>
            <ta e="T41" id="Seg_790" s="T40">qwän-le-k</ta>
            <ta e="T95" id="Seg_791" s="T41">Iǯe-p</ta>
            <ta e="T42" id="Seg_792" s="T95">Kana-p</ta>
            <ta e="T43" id="Seg_793" s="T42">am-gu</ta>
            <ta e="T44" id="Seg_794" s="T43">tab</ta>
            <ta e="T45" id="Seg_795" s="T44">töː-ɨ</ta>
            <ta e="T46" id="Seg_796" s="T45">Pöːnege</ta>
            <ta e="T96" id="Seg_797" s="T46">Iǯe-p</ta>
            <ta e="T47" id="Seg_798" s="T96">Kana-m</ta>
            <ta e="T48" id="Seg_799" s="T47">am-gu</ta>
            <ta e="T97" id="Seg_800" s="T48">Kana</ta>
            <ta e="T98" id="Seg_801" s="T49">Kana</ta>
            <ta e="T50" id="Seg_802" s="T98">Iǯe</ta>
            <ta e="T51" id="Seg_803" s="T50">čenčɨ</ta>
            <ta e="T52" id="Seg_804" s="T51">tan</ta>
            <ta e="T53" id="Seg_805" s="T52">miǯnɨt</ta>
            <ta e="T54" id="Seg_806" s="T53">ɨgɨ</ta>
            <ta e="T55" id="Seg_807" s="T54">am-äšɨk</ta>
            <ta e="T56" id="Seg_808" s="T55">mi</ta>
            <ta e="T57" id="Seg_809" s="T56">peqqa-m</ta>
            <ta e="T58" id="Seg_810" s="T57">nö-dɨ-le-j-se</ta>
            <ta e="T59" id="Seg_811" s="T58">peqqa-n</ta>
            <ta e="T60" id="Seg_812" s="T59">waǯʼe</ta>
            <ta e="T61" id="Seg_813" s="T60">mi</ta>
            <ta e="T62" id="Seg_814" s="T61">waǯʼe-nan</ta>
            <ta e="T63" id="Seg_815" s="T62">nüe-la</ta>
            <ta e="T64" id="Seg_816" s="T63">peqqa-m</ta>
            <ta e="T65" id="Seg_817" s="T64">nö-ntɨ-dət</ta>
            <ta e="T66" id="Seg_818" s="T65">Pöːnege</ta>
            <ta e="T67" id="Seg_819" s="T66">sadʼi</ta>
            <ta e="T68" id="Seg_820" s="T67">nö-ntɨ-mbɨ-tɨ</ta>
            <ta e="T69" id="Seg_821" s="T68">peqqa-n</ta>
            <ta e="T70" id="Seg_822" s="T69">waǯʼe-m</ta>
            <ta e="T71" id="Seg_823" s="T70">am-gu</ta>
            <ta e="T72" id="Seg_824" s="T71">peqqa</ta>
            <ta e="T73" id="Seg_825" s="T72">nom-ni</ta>
            <ta e="T74" id="Seg_826" s="T73">inne</ta>
            <ta e="T75" id="Seg_827" s="T74">waše-nǯe-n</ta>
            <ta e="T76" id="Seg_828" s="T75">a</ta>
            <ta e="T77" id="Seg_829" s="T76">nop</ta>
            <ta e="T78" id="Seg_830" s="T77">koštɨ-tɨ</ta>
            <ta e="T79" id="Seg_831" s="T78">qaj</ta>
            <ta e="T80" id="Seg_832" s="T79">nɨlʼǯi</ta>
            <ta e="T81" id="Seg_833" s="T80">töː-ndɨ-dət</ta>
            <ta e="T99" id="Seg_834" s="T81">Kana</ta>
            <ta e="T82" id="Seg_835" s="T99">Iǯe</ta>
            <ta e="T83" id="Seg_836" s="T82">Pöːnege</ta>
            <ta e="T84" id="Seg_837" s="T83">qaj</ta>
            <ta e="T85" id="Seg_838" s="T84">me-gu</ta>
            <ta e="T86" id="Seg_839" s="T85">man</ta>
            <ta e="T87" id="Seg_840" s="T86">qɨška-saj-tqo</ta>
            <ta e="T88" id="Seg_841" s="T87">me-ja-p</ta>
            <ta e="T89" id="Seg_842" s="T88">wesʼ</ta>
            <ta e="T90" id="Seg_843" s="T89">qɨška-sajɨ-qɨt</ta>
            <ta e="T91" id="Seg_844" s="T90">tiː</ta>
            <ta e="T92" id="Seg_845" s="T91">e-ja-lt</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_846" s="T0">earlier</ta>
            <ta e="T94" id="Seg_847" s="T1">Itja</ta>
            <ta e="T2" id="Seg_848" s="T94">Kana</ta>
            <ta e="T3" id="Seg_849" s="T2">net-VBLZ-EP-IPFV2-HAB-PST.NAR-3DU.S</ta>
            <ta e="T4" id="Seg_850" s="T3">fish-ACC</ta>
            <ta e="T5" id="Seg_851" s="T4">catch-EP-IPFV2-HAB-3DU.S</ta>
            <ta e="T6" id="Seg_852" s="T5">devil.[NOM]</ta>
            <ta e="T7" id="Seg_853" s="T6">(s)he-DU-ALL</ta>
            <ta e="T8" id="Seg_854" s="T7">boat-ILL</ta>
            <ta e="T9" id="Seg_855" s="T8">sit.down-CVB</ta>
            <ta e="T10" id="Seg_856" s="T9">steal-FRQ-HAB-3SG.O</ta>
            <ta e="T11" id="Seg_857" s="T10">fish-ACC</ta>
            <ta e="T12" id="Seg_858" s="T11">also</ta>
            <ta e="T13" id="Seg_859" s="T12">Itja.[NOM]</ta>
            <ta e="T14" id="Seg_860" s="T13">then</ta>
            <ta e="T15" id="Seg_861" s="T14">find.out-PST.NAR-3SG.O</ta>
            <ta e="T16" id="Seg_862" s="T15">(s)he.[NOM]</ta>
            <ta e="T17" id="Seg_863" s="T16">we.PL.GEN</ta>
            <ta e="T18" id="Seg_864" s="T17">fish-EP-1PL</ta>
            <ta e="T19" id="Seg_865" s="T18">steal-RES-IPFV2-3SG.O</ta>
            <ta e="T20" id="Seg_866" s="T19">Itja.[NOM]</ta>
            <ta e="T21" id="Seg_867" s="T20">taiga-ILL</ta>
            <ta e="T22" id="Seg_868" s="T21">go.away-CO.[3SG.S]</ta>
            <ta e="T23" id="Seg_869" s="T22">tar-ACC</ta>
            <ta e="T24" id="Seg_870" s="T23">much-ADVZ</ta>
            <ta e="T25" id="Seg_871" s="T24">dig.out-%%-3SG.O</ta>
            <ta e="T26" id="Seg_872" s="T25">bench-ACC-3SG</ta>
            <ta e="T27" id="Seg_873" s="T26">tar-INSTR</ta>
            <ta e="T28" id="Seg_874" s="T27">smear-CO-3SG.O</ta>
            <ta e="T29" id="Seg_875" s="T28">also</ta>
            <ta e="T30" id="Seg_876" s="T29">well</ta>
            <ta e="T31" id="Seg_877" s="T30">go.away-CO.[3SG.S]</ta>
            <ta e="T32" id="Seg_878" s="T31">Pönege.[NOM]</ta>
            <ta e="T33" id="Seg_879" s="T32">sit.down.[3SG.S]</ta>
            <ta e="T34" id="Seg_880" s="T33">boat-ILL</ta>
            <ta e="T35" id="Seg_881" s="T34">straight</ta>
            <ta e="T36" id="Seg_882" s="T35">new</ta>
            <ta e="T37" id="Seg_883" s="T36">ass.[NOM]</ta>
            <ta e="T38" id="Seg_884" s="T37">glue-CO.[3SG.S]</ta>
            <ta e="T39" id="Seg_885" s="T38">then</ta>
            <ta e="T40" id="Seg_886" s="T39">get.angry-CO.[3SG.S]</ta>
            <ta e="T41" id="Seg_887" s="T40">go.away-OPT-1SG.S</ta>
            <ta e="T95" id="Seg_888" s="T41">Itja-ACC</ta>
            <ta e="T42" id="Seg_889" s="T95">Kana-ACC</ta>
            <ta e="T43" id="Seg_890" s="T42">eat-INF</ta>
            <ta e="T44" id="Seg_891" s="T43">(s)he.[NOM]</ta>
            <ta e="T45" id="Seg_892" s="T44">come-EP.[3SG.S]</ta>
            <ta e="T46" id="Seg_893" s="T45">Pönege.[NOM]</ta>
            <ta e="T96" id="Seg_894" s="T46">Itja-ACC</ta>
            <ta e="T47" id="Seg_895" s="T96">Kana-ACC</ta>
            <ta e="T48" id="Seg_896" s="T47">eat-INF</ta>
            <ta e="T97" id="Seg_897" s="T48">Kana.[NOM]</ta>
            <ta e="T98" id="Seg_898" s="T49">Kana.[NOM]</ta>
            <ta e="T50" id="Seg_899" s="T98">Itja.[NOM]</ta>
            <ta e="T51" id="Seg_900" s="T50">say.[3SG.S]</ta>
            <ta e="T52" id="Seg_901" s="T51">you.SG.[NOM]</ta>
            <ta e="T53" id="Seg_902" s="T52">we.DU.ACC</ta>
            <ta e="T54" id="Seg_903" s="T53">NEG.IMP</ta>
            <ta e="T55" id="Seg_904" s="T54">eat-IMP.2SG.S</ta>
            <ta e="T56" id="Seg_905" s="T55">we.DU.NOM</ta>
            <ta e="T57" id="Seg_906" s="T56">elk-ACC</ta>
            <ta e="T58" id="Seg_907" s="T57">drive-TR-OPT-1DU-OPT</ta>
            <ta e="T59" id="Seg_908" s="T58">elk-GEN</ta>
            <ta e="T60" id="Seg_909" s="T59">meat.[NOM]</ta>
            <ta e="T61" id="Seg_910" s="T60">we.DU.GEN</ta>
            <ta e="T62" id="Seg_911" s="T61">meat-ADES</ta>
            <ta e="T63" id="Seg_912" s="T62">be.sweet-FUT.[3SG.S]</ta>
            <ta e="T64" id="Seg_913" s="T63">elk-ACC</ta>
            <ta e="T65" id="Seg_914" s="T64">drive-IPFV-3PL</ta>
            <ta e="T66" id="Seg_915" s="T65">devil.[NOM]</ta>
            <ta e="T67" id="Seg_916" s="T66">behind</ta>
            <ta e="T68" id="Seg_917" s="T67">drive-IPFV-PST.NAR-3SG.O</ta>
            <ta e="T69" id="Seg_918" s="T68">elk-GEN</ta>
            <ta e="T70" id="Seg_919" s="T69">meat-ACC</ta>
            <ta e="T71" id="Seg_920" s="T70">eat-INF</ta>
            <ta e="T72" id="Seg_921" s="T71">elk.[NOM]</ta>
            <ta e="T73" id="Seg_922" s="T72">god-ALL</ta>
            <ta e="T74" id="Seg_923" s="T73">up</ta>
            <ta e="T75" id="Seg_924" s="T74">fly-IPFV3-3SG.S</ta>
            <ta e="T76" id="Seg_925" s="T75">but</ta>
            <ta e="T77" id="Seg_926" s="T76">god.[NOM]</ta>
            <ta e="T78" id="Seg_927" s="T77">find.out-3SG.O</ta>
            <ta e="T79" id="Seg_928" s="T78">what</ta>
            <ta e="T80" id="Seg_929" s="T79">such</ta>
            <ta e="T81" id="Seg_930" s="T80">come-INFER-3PL</ta>
            <ta e="T99" id="Seg_931" s="T81">Kana.[NOM]</ta>
            <ta e="T82" id="Seg_932" s="T99">Itja.[NOM]</ta>
            <ta e="T83" id="Seg_933" s="T82">Pönege.[NOM]</ta>
            <ta e="T84" id="Seg_934" s="T83">what.[NOM]</ta>
            <ta e="T85" id="Seg_935" s="T84">do-INF</ta>
            <ta e="T86" id="Seg_936" s="T85">I.NOM</ta>
            <ta e="T87" id="Seg_937" s="T86">star-SNGL-TRL</ta>
            <ta e="T88" id="Seg_938" s="T87">do-CO-1SG.O</ta>
            <ta e="T89" id="Seg_939" s="T88">all</ta>
            <ta e="T90" id="Seg_940" s="T89">star-SNGL-LOC</ta>
            <ta e="T91" id="Seg_941" s="T90">you.PL.NOM</ta>
            <ta e="T92" id="Seg_942" s="T91">be-CO-2PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_943" s="T0">раньше</ta>
            <ta e="T94" id="Seg_944" s="T1">Идже</ta>
            <ta e="T2" id="Seg_945" s="T94">Кана</ta>
            <ta e="T3" id="Seg_946" s="T2">сеть-VBLZ-EP-IPFV2-HAB-PST.NAR-3DU.S</ta>
            <ta e="T4" id="Seg_947" s="T3">рыба-ACC</ta>
            <ta e="T5" id="Seg_948" s="T4">поймать-EP-IPFV2-HAB-3DU.S</ta>
            <ta e="T6" id="Seg_949" s="T5">чёрт.[NOM]</ta>
            <ta e="T7" id="Seg_950" s="T6">он(а)-DU-ALL</ta>
            <ta e="T8" id="Seg_951" s="T7">обласок-ILL</ta>
            <ta e="T9" id="Seg_952" s="T8">сесть-CVB</ta>
            <ta e="T10" id="Seg_953" s="T9">украсть-FRQ-HAB-3SG.O</ta>
            <ta e="T11" id="Seg_954" s="T10">рыба-ACC</ta>
            <ta e="T12" id="Seg_955" s="T11">тоже</ta>
            <ta e="T13" id="Seg_956" s="T12">Итя.[NOM]</ta>
            <ta e="T14" id="Seg_957" s="T13">потом</ta>
            <ta e="T15" id="Seg_958" s="T14">узнать-PST.NAR-3SG.O</ta>
            <ta e="T16" id="Seg_959" s="T15">он(а).[NOM]</ta>
            <ta e="T17" id="Seg_960" s="T16">мы.PL.GEN</ta>
            <ta e="T18" id="Seg_961" s="T17">рыба-EP-1PL</ta>
            <ta e="T19" id="Seg_962" s="T18">украсть-RES-IPFV2-3SG.O</ta>
            <ta e="T20" id="Seg_963" s="T19">Итя.[NOM]</ta>
            <ta e="T21" id="Seg_964" s="T20">тайга-ILL</ta>
            <ta e="T22" id="Seg_965" s="T21">пойти-CO.[3SG.S]</ta>
            <ta e="T23" id="Seg_966" s="T22">смола-ACC</ta>
            <ta e="T24" id="Seg_967" s="T23">много-ADVZ</ta>
            <ta e="T25" id="Seg_968" s="T24">выкопать-%%-3SG.O</ta>
            <ta e="T26" id="Seg_969" s="T25">скамейка-ACC-3SG</ta>
            <ta e="T27" id="Seg_970" s="T26">смола-INSTR</ta>
            <ta e="T28" id="Seg_971" s="T27">обмазать-CO-3SG.O</ta>
            <ta e="T29" id="Seg_972" s="T28">тоже</ta>
            <ta e="T30" id="Seg_973" s="T29">ну</ta>
            <ta e="T31" id="Seg_974" s="T30">пойти-CO.[3SG.S]</ta>
            <ta e="T32" id="Seg_975" s="T31">Пёнеге.[NOM]</ta>
            <ta e="T33" id="Seg_976" s="T32">сесть.[3SG.S]</ta>
            <ta e="T34" id="Seg_977" s="T33">обласок-ILL</ta>
            <ta e="T35" id="Seg_978" s="T34">прямой</ta>
            <ta e="T36" id="Seg_979" s="T35">новый</ta>
            <ta e="T37" id="Seg_980" s="T36">задница.[NOM]</ta>
            <ta e="T38" id="Seg_981" s="T37">клеить-CO.[3SG.S]</ta>
            <ta e="T39" id="Seg_982" s="T38">потом</ta>
            <ta e="T40" id="Seg_983" s="T39">рассердиться-CO.[3SG.S]</ta>
            <ta e="T41" id="Seg_984" s="T40">пойти-OPT-1SG.S</ta>
            <ta e="T95" id="Seg_985" s="T41">Итя-ACC</ta>
            <ta e="T42" id="Seg_986" s="T95">Кана-ACC</ta>
            <ta e="T43" id="Seg_987" s="T42">есть-INF</ta>
            <ta e="T44" id="Seg_988" s="T43">он(а).[NOM]</ta>
            <ta e="T45" id="Seg_989" s="T44">прийти-EP.[3SG.S]</ta>
            <ta e="T46" id="Seg_990" s="T45">Пёнеге.[NOM]</ta>
            <ta e="T96" id="Seg_991" s="T46">Итя-ACC</ta>
            <ta e="T47" id="Seg_992" s="T96">Кана-ACC</ta>
            <ta e="T48" id="Seg_993" s="T47">есть-INF</ta>
            <ta e="T97" id="Seg_994" s="T48">Кана.[NOM]</ta>
            <ta e="T98" id="Seg_995" s="T49">Кана.[NOM]</ta>
            <ta e="T50" id="Seg_996" s="T98">Итя.[NOM]</ta>
            <ta e="T51" id="Seg_997" s="T50">сказать.[3SG.S]</ta>
            <ta e="T52" id="Seg_998" s="T51">ты.[NOM]</ta>
            <ta e="T53" id="Seg_999" s="T52">мы.DU.ACC</ta>
            <ta e="T54" id="Seg_1000" s="T53">NEG.IMP</ta>
            <ta e="T55" id="Seg_1001" s="T54">есть-IMP.2SG.S</ta>
            <ta e="T56" id="Seg_1002" s="T55">мы.DU.NOM</ta>
            <ta e="T57" id="Seg_1003" s="T56">лось-ACC</ta>
            <ta e="T58" id="Seg_1004" s="T57">гнать-TR-OPT-1DU-OPT</ta>
            <ta e="T59" id="Seg_1005" s="T58">лось-GEN</ta>
            <ta e="T60" id="Seg_1006" s="T59">мясо.[NOM]</ta>
            <ta e="T61" id="Seg_1007" s="T60">мы.DU.GEN</ta>
            <ta e="T62" id="Seg_1008" s="T61">мясо-ADES</ta>
            <ta e="T63" id="Seg_1009" s="T62">быть.сладким-FUT.[3SG.S]</ta>
            <ta e="T64" id="Seg_1010" s="T63">лось-ACC</ta>
            <ta e="T65" id="Seg_1011" s="T64">гнать-IPFV-3PL</ta>
            <ta e="T66" id="Seg_1012" s="T65">чёрт.[NOM]</ta>
            <ta e="T67" id="Seg_1013" s="T66">сзади</ta>
            <ta e="T68" id="Seg_1014" s="T67">гнать-IPFV-PST.NAR-3SG.O</ta>
            <ta e="T69" id="Seg_1015" s="T68">лось-GEN</ta>
            <ta e="T70" id="Seg_1016" s="T69">мясо-ACC</ta>
            <ta e="T71" id="Seg_1017" s="T70">есть-INF</ta>
            <ta e="T72" id="Seg_1018" s="T71">лось.[NOM]</ta>
            <ta e="T73" id="Seg_1019" s="T72">бог-ALL</ta>
            <ta e="T74" id="Seg_1020" s="T73">наверх</ta>
            <ta e="T75" id="Seg_1021" s="T74">лететь-IPFV3-3SG.S</ta>
            <ta e="T76" id="Seg_1022" s="T75">а</ta>
            <ta e="T77" id="Seg_1023" s="T76">бог.[NOM]</ta>
            <ta e="T78" id="Seg_1024" s="T77">узнать-3SG.O</ta>
            <ta e="T79" id="Seg_1025" s="T78">что</ta>
            <ta e="T80" id="Seg_1026" s="T79">такой</ta>
            <ta e="T81" id="Seg_1027" s="T80">прийти-INFER-3PL</ta>
            <ta e="T99" id="Seg_1028" s="T81">Кана.[NOM]</ta>
            <ta e="T82" id="Seg_1029" s="T99">Итя.[NOM]</ta>
            <ta e="T83" id="Seg_1030" s="T82">Пёнеге.[NOM]</ta>
            <ta e="T84" id="Seg_1031" s="T83">что.[NOM]</ta>
            <ta e="T85" id="Seg_1032" s="T84">делать-INF</ta>
            <ta e="T86" id="Seg_1033" s="T85">я.NOM</ta>
            <ta e="T87" id="Seg_1034" s="T86">звезда-SNGL-TRL</ta>
            <ta e="T88" id="Seg_1035" s="T87">делать-CO-1SG.O</ta>
            <ta e="T89" id="Seg_1036" s="T88">весь</ta>
            <ta e="T90" id="Seg_1037" s="T89">звезда-SNGL-LOC</ta>
            <ta e="T91" id="Seg_1038" s="T90">вы.PL.NOM</ta>
            <ta e="T92" id="Seg_1039" s="T91">быть-CO-2PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1040" s="T0">adv</ta>
            <ta e="T94" id="Seg_1041" s="T1">nprop</ta>
            <ta e="T2" id="Seg_1042" s="T94">nprop</ta>
            <ta e="T3" id="Seg_1043" s="T2">n-n&gt;v-n:ins-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_1044" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_1045" s="T4">v-n:ins-v&gt;v-n&gt;n-v:pn</ta>
            <ta e="T6" id="Seg_1046" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_1047" s="T6">pers-n:num-n:case</ta>
            <ta e="T8" id="Seg_1048" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_1049" s="T8">v-v&gt;adv</ta>
            <ta e="T10" id="Seg_1050" s="T9">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T11" id="Seg_1051" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_1052" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_1053" s="T12">nprop-n:case</ta>
            <ta e="T14" id="Seg_1054" s="T13">adv</ta>
            <ta e="T15" id="Seg_1055" s="T14">v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_1056" s="T15">pers-n:case</ta>
            <ta e="T17" id="Seg_1057" s="T16">pers</ta>
            <ta e="T18" id="Seg_1058" s="T17">n-n:ins-n:poss</ta>
            <ta e="T19" id="Seg_1059" s="T18">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T20" id="Seg_1060" s="T19">nprop-n:case</ta>
            <ta e="T21" id="Seg_1061" s="T20">n-n:case</ta>
            <ta e="T22" id="Seg_1062" s="T21">v-v:ins-v:pn</ta>
            <ta e="T23" id="Seg_1063" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_1064" s="T23">quant-adj&gt;adv</ta>
            <ta e="T25" id="Seg_1065" s="T24">v-v&gt;v-v:pn</ta>
            <ta e="T26" id="Seg_1066" s="T25">n-n:case-n:poss</ta>
            <ta e="T27" id="Seg_1067" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_1068" s="T27">v-n:ins-v:pn</ta>
            <ta e="T29" id="Seg_1069" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_1070" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_1071" s="T30">v-v:ins-v:pn</ta>
            <ta e="T32" id="Seg_1072" s="T31">nprop-n:case</ta>
            <ta e="T33" id="Seg_1073" s="T32">v-v:pn</ta>
            <ta e="T34" id="Seg_1074" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_1075" s="T34">adj</ta>
            <ta e="T36" id="Seg_1076" s="T35">adj</ta>
            <ta e="T37" id="Seg_1077" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_1078" s="T37">v-v:ins-v:pn</ta>
            <ta e="T39" id="Seg_1079" s="T38">adv</ta>
            <ta e="T40" id="Seg_1080" s="T39">v-v:ins-v:pn</ta>
            <ta e="T41" id="Seg_1081" s="T40">v-v:mood-v:pn</ta>
            <ta e="T95" id="Seg_1082" s="T41">nprop-n:case</ta>
            <ta e="T42" id="Seg_1083" s="T95">nprop-n:case</ta>
            <ta e="T43" id="Seg_1084" s="T42">v-v:inf</ta>
            <ta e="T44" id="Seg_1085" s="T43">pers-n:case</ta>
            <ta e="T45" id="Seg_1086" s="T44">v-n:ins-v:pn</ta>
            <ta e="T46" id="Seg_1087" s="T45">nprop-n:case</ta>
            <ta e="T96" id="Seg_1088" s="T46">nprop-n:case</ta>
            <ta e="T47" id="Seg_1089" s="T96">nprop-n:case</ta>
            <ta e="T48" id="Seg_1090" s="T47">v-v:inf</ta>
            <ta e="T97" id="Seg_1091" s="T48">nprop-n:case</ta>
            <ta e="T98" id="Seg_1092" s="T49">nprop-n:case</ta>
            <ta e="T50" id="Seg_1093" s="T98">nprop-n:case</ta>
            <ta e="T51" id="Seg_1094" s="T50">v-v:pn</ta>
            <ta e="T52" id="Seg_1095" s="T51">pers-n:case</ta>
            <ta e="T53" id="Seg_1096" s="T52">pers</ta>
            <ta e="T54" id="Seg_1097" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_1098" s="T54">v-n:ins-v&gt;v-v:mood.pn</ta>
            <ta e="T56" id="Seg_1099" s="T55">pers</ta>
            <ta e="T57" id="Seg_1100" s="T56">n-n:case</ta>
            <ta e="T58" id="Seg_1101" s="T57">v-v&gt;v-v:mood-v:pn-v:mood</ta>
            <ta e="T59" id="Seg_1102" s="T58">n-n:case</ta>
            <ta e="T60" id="Seg_1103" s="T59">n-n:case</ta>
            <ta e="T61" id="Seg_1104" s="T60">pers</ta>
            <ta e="T62" id="Seg_1105" s="T61">n-n:case</ta>
            <ta e="T63" id="Seg_1106" s="T62">v-v:tense-v:pn</ta>
            <ta e="T64" id="Seg_1107" s="T63">n-n:case</ta>
            <ta e="T65" id="Seg_1108" s="T64">v-v&gt;v-v:pn</ta>
            <ta e="T66" id="Seg_1109" s="T65">n-n:case</ta>
            <ta e="T67" id="Seg_1110" s="T66">adv</ta>
            <ta e="T68" id="Seg_1111" s="T67">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T69" id="Seg_1112" s="T68">n-n:case</ta>
            <ta e="T70" id="Seg_1113" s="T69">n-n:case</ta>
            <ta e="T71" id="Seg_1114" s="T70">v-v:inf</ta>
            <ta e="T72" id="Seg_1115" s="T71">n-n:case</ta>
            <ta e="T73" id="Seg_1116" s="T72">n-n:case</ta>
            <ta e="T74" id="Seg_1117" s="T73">preverb</ta>
            <ta e="T75" id="Seg_1118" s="T74">v-v&gt;v-v:pn</ta>
            <ta e="T76" id="Seg_1119" s="T75">conj</ta>
            <ta e="T77" id="Seg_1120" s="T76">n-n:case</ta>
            <ta e="T78" id="Seg_1121" s="T77">v-v:pn</ta>
            <ta e="T79" id="Seg_1122" s="T78">interrog</ta>
            <ta e="T80" id="Seg_1123" s="T79">dem</ta>
            <ta e="T81" id="Seg_1124" s="T80">v-v:mood-v:pn</ta>
            <ta e="T99" id="Seg_1125" s="T81">nprop-n:case</ta>
            <ta e="T82" id="Seg_1126" s="T99">nprop-n:case</ta>
            <ta e="T83" id="Seg_1127" s="T82">nprop-n:case</ta>
            <ta e="T84" id="Seg_1128" s="T83">interrog-n:case</ta>
            <ta e="T85" id="Seg_1129" s="T84">v-v:inf</ta>
            <ta e="T86" id="Seg_1130" s="T85">pers</ta>
            <ta e="T87" id="Seg_1131" s="T86">n-n&gt;n-n:case</ta>
            <ta e="T88" id="Seg_1132" s="T87">v-v:ins-v:pn</ta>
            <ta e="T89" id="Seg_1133" s="T88">quant</ta>
            <ta e="T90" id="Seg_1134" s="T89">n-n&gt;n-n:case</ta>
            <ta e="T91" id="Seg_1135" s="T90">pers</ta>
            <ta e="T92" id="Seg_1136" s="T91">v-v:ins-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1137" s="T0">adv</ta>
            <ta e="T94" id="Seg_1138" s="T1">nprop</ta>
            <ta e="T2" id="Seg_1139" s="T94">nprop</ta>
            <ta e="T3" id="Seg_1140" s="T2">v</ta>
            <ta e="T4" id="Seg_1141" s="T3">n</ta>
            <ta e="T5" id="Seg_1142" s="T4">v</ta>
            <ta e="T6" id="Seg_1143" s="T5">n</ta>
            <ta e="T7" id="Seg_1144" s="T6">pers</ta>
            <ta e="T8" id="Seg_1145" s="T7">n</ta>
            <ta e="T9" id="Seg_1146" s="T8">adv</ta>
            <ta e="T10" id="Seg_1147" s="T9">v</ta>
            <ta e="T11" id="Seg_1148" s="T10">n</ta>
            <ta e="T12" id="Seg_1149" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_1150" s="T12">nprop</ta>
            <ta e="T14" id="Seg_1151" s="T13">adv</ta>
            <ta e="T15" id="Seg_1152" s="T14">v</ta>
            <ta e="T16" id="Seg_1153" s="T15">pers</ta>
            <ta e="T17" id="Seg_1154" s="T16">pers</ta>
            <ta e="T18" id="Seg_1155" s="T17">n</ta>
            <ta e="T19" id="Seg_1156" s="T18">v</ta>
            <ta e="T20" id="Seg_1157" s="T19">nprop</ta>
            <ta e="T21" id="Seg_1158" s="T20">n</ta>
            <ta e="T22" id="Seg_1159" s="T21">v</ta>
            <ta e="T23" id="Seg_1160" s="T22">n</ta>
            <ta e="T24" id="Seg_1161" s="T23">adv</ta>
            <ta e="T25" id="Seg_1162" s="T24">v</ta>
            <ta e="T26" id="Seg_1163" s="T25">n</ta>
            <ta e="T27" id="Seg_1164" s="T26">n</ta>
            <ta e="T28" id="Seg_1165" s="T27">v</ta>
            <ta e="T29" id="Seg_1166" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_1167" s="T29">v</ta>
            <ta e="T31" id="Seg_1168" s="T30">v</ta>
            <ta e="T32" id="Seg_1169" s="T31">nprop</ta>
            <ta e="T33" id="Seg_1170" s="T32">v</ta>
            <ta e="T34" id="Seg_1171" s="T33">n</ta>
            <ta e="T35" id="Seg_1172" s="T34">adj</ta>
            <ta e="T36" id="Seg_1173" s="T35">adj</ta>
            <ta e="T37" id="Seg_1174" s="T36">n</ta>
            <ta e="T38" id="Seg_1175" s="T37">v</ta>
            <ta e="T39" id="Seg_1176" s="T38">adv</ta>
            <ta e="T40" id="Seg_1177" s="T39">v</ta>
            <ta e="T41" id="Seg_1178" s="T40">v</ta>
            <ta e="T95" id="Seg_1179" s="T41">nprop</ta>
            <ta e="T42" id="Seg_1180" s="T95">nprop</ta>
            <ta e="T43" id="Seg_1181" s="T42">v</ta>
            <ta e="T44" id="Seg_1182" s="T43">pers</ta>
            <ta e="T45" id="Seg_1183" s="T44">v</ta>
            <ta e="T46" id="Seg_1184" s="T45">nprop</ta>
            <ta e="T96" id="Seg_1185" s="T46">nprop</ta>
            <ta e="T47" id="Seg_1186" s="T96">nprop</ta>
            <ta e="T48" id="Seg_1187" s="T47">v</ta>
            <ta e="T97" id="Seg_1188" s="T48">nprop</ta>
            <ta e="T98" id="Seg_1189" s="T49">nprop</ta>
            <ta e="T50" id="Seg_1190" s="T98">nprop</ta>
            <ta e="T51" id="Seg_1191" s="T50">v</ta>
            <ta e="T52" id="Seg_1192" s="T51">pers</ta>
            <ta e="T53" id="Seg_1193" s="T52">pers</ta>
            <ta e="T54" id="Seg_1194" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_1195" s="T54">v</ta>
            <ta e="T56" id="Seg_1196" s="T55">pers</ta>
            <ta e="T57" id="Seg_1197" s="T56">n</ta>
            <ta e="T58" id="Seg_1198" s="T57">v</ta>
            <ta e="T59" id="Seg_1199" s="T58">n</ta>
            <ta e="T60" id="Seg_1200" s="T59">n</ta>
            <ta e="T61" id="Seg_1201" s="T60">pers</ta>
            <ta e="T62" id="Seg_1202" s="T61">n</ta>
            <ta e="T63" id="Seg_1203" s="T62">v</ta>
            <ta e="T64" id="Seg_1204" s="T63">n</ta>
            <ta e="T65" id="Seg_1205" s="T64">v</ta>
            <ta e="T66" id="Seg_1206" s="T65">n</ta>
            <ta e="T67" id="Seg_1207" s="T66">adv</ta>
            <ta e="T68" id="Seg_1208" s="T67">v</ta>
            <ta e="T69" id="Seg_1209" s="T68">n</ta>
            <ta e="T70" id="Seg_1210" s="T69">n</ta>
            <ta e="T71" id="Seg_1211" s="T70">v</ta>
            <ta e="T72" id="Seg_1212" s="T71">n</ta>
            <ta e="T73" id="Seg_1213" s="T72">n</ta>
            <ta e="T74" id="Seg_1214" s="T73">preverb</ta>
            <ta e="T75" id="Seg_1215" s="T74">v</ta>
            <ta e="T76" id="Seg_1216" s="T75">conj</ta>
            <ta e="T77" id="Seg_1217" s="T76">n</ta>
            <ta e="T78" id="Seg_1218" s="T77">v</ta>
            <ta e="T79" id="Seg_1219" s="T78">interrog</ta>
            <ta e="T80" id="Seg_1220" s="T79">dem</ta>
            <ta e="T81" id="Seg_1221" s="T80">v</ta>
            <ta e="T99" id="Seg_1222" s="T81">nprop</ta>
            <ta e="T82" id="Seg_1223" s="T99">nprop</ta>
            <ta e="T83" id="Seg_1224" s="T82">nprop</ta>
            <ta e="T84" id="Seg_1225" s="T83">interrog</ta>
            <ta e="T85" id="Seg_1226" s="T84">v</ta>
            <ta e="T86" id="Seg_1227" s="T85">pers</ta>
            <ta e="T87" id="Seg_1228" s="T86">n</ta>
            <ta e="T88" id="Seg_1229" s="T87">v</ta>
            <ta e="T89" id="Seg_1230" s="T88">quant</ta>
            <ta e="T90" id="Seg_1231" s="T89">n</ta>
            <ta e="T91" id="Seg_1232" s="T90">pers</ta>
            <ta e="T92" id="Seg_1233" s="T91">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_1234" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_1235" s="T2">v:pred</ta>
            <ta e="T4" id="Seg_1236" s="T3">np:O</ta>
            <ta e="T5" id="Seg_1237" s="T4">0.3.h:S v:pred</ta>
            <ta e="T6" id="Seg_1238" s="T5">np.h:S</ta>
            <ta e="T9" id="Seg_1239" s="T6">s:temp</ta>
            <ta e="T10" id="Seg_1240" s="T9">v:pred</ta>
            <ta e="T11" id="Seg_1241" s="T10">np:O</ta>
            <ta e="T13" id="Seg_1242" s="T12">np.h:S</ta>
            <ta e="T15" id="Seg_1243" s="T14">v:pred</ta>
            <ta e="T16" id="Seg_1244" s="T15">pro.h:S</ta>
            <ta e="T18" id="Seg_1245" s="T17">np:O</ta>
            <ta e="T19" id="Seg_1246" s="T18">v:pred</ta>
            <ta e="T20" id="Seg_1247" s="T19">np.h:S</ta>
            <ta e="T22" id="Seg_1248" s="T21">v:pred</ta>
            <ta e="T23" id="Seg_1249" s="T22">np:O</ta>
            <ta e="T25" id="Seg_1250" s="T24">0.3.h:S v:pred</ta>
            <ta e="T26" id="Seg_1251" s="T25">np:O</ta>
            <ta e="T28" id="Seg_1252" s="T27">0.3.h:S v:pred</ta>
            <ta e="T31" id="Seg_1253" s="T30">v:pred</ta>
            <ta e="T32" id="Seg_1254" s="T31">np.h:S</ta>
            <ta e="T33" id="Seg_1255" s="T32">0.3.h:S v:pred</ta>
            <ta e="T37" id="Seg_1256" s="T36">np:S</ta>
            <ta e="T38" id="Seg_1257" s="T37">v:pred</ta>
            <ta e="T40" id="Seg_1258" s="T39">0.3.h:S v:pred</ta>
            <ta e="T41" id="Seg_1259" s="T40">0.1.h:S v:pred</ta>
            <ta e="T43" id="Seg_1260" s="T41">s:purp</ta>
            <ta e="T44" id="Seg_1261" s="T43">pro.h:S</ta>
            <ta e="T45" id="Seg_1262" s="T44">v:pred</ta>
            <ta e="T48" id="Seg_1263" s="T46">s:purp</ta>
            <ta e="T97" id="Seg_1264" s="T48">np.h:S</ta>
            <ta e="T50" id="Seg_1265" s="T49">np.h:S</ta>
            <ta e="T51" id="Seg_1266" s="T50">v:pred</ta>
            <ta e="T52" id="Seg_1267" s="T51">pro.h:S</ta>
            <ta e="T53" id="Seg_1268" s="T52">pro.h:O</ta>
            <ta e="T55" id="Seg_1269" s="T54">v:pred</ta>
            <ta e="T56" id="Seg_1270" s="T55">pro.h:S</ta>
            <ta e="T57" id="Seg_1271" s="T56">np:O</ta>
            <ta e="T58" id="Seg_1272" s="T57">v:pred</ta>
            <ta e="T60" id="Seg_1273" s="T59">np:S</ta>
            <ta e="T63" id="Seg_1274" s="T62">v:pred</ta>
            <ta e="T64" id="Seg_1275" s="T63">np:O</ta>
            <ta e="T65" id="Seg_1276" s="T64">0.3.h:S v:pred</ta>
            <ta e="T66" id="Seg_1277" s="T65">np.h:S</ta>
            <ta e="T68" id="Seg_1278" s="T67">v:pred</ta>
            <ta e="T71" id="Seg_1279" s="T68">s:purp</ta>
            <ta e="T72" id="Seg_1280" s="T71">np:S</ta>
            <ta e="T75" id="Seg_1281" s="T74">v:pred</ta>
            <ta e="T77" id="Seg_1282" s="T76">np:S</ta>
            <ta e="T78" id="Seg_1283" s="T77">v:pred</ta>
            <ta e="T79" id="Seg_1284" s="T78">pro.h:S</ta>
            <ta e="T81" id="Seg_1285" s="T80">v:pred</ta>
            <ta e="T86" id="Seg_1286" s="T85">pro.h:S</ta>
            <ta e="T88" id="Seg_1287" s="T87">v:pred 0.3.h:O</ta>
            <ta e="T91" id="Seg_1288" s="T90">pro.h:S</ta>
            <ta e="T92" id="Seg_1289" s="T91">v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_1290" s="T0">adv:Time</ta>
            <ta e="T2" id="Seg_1291" s="T1">np.h:A</ta>
            <ta e="T4" id="Seg_1292" s="T3">np:P</ta>
            <ta e="T5" id="Seg_1293" s="T4">0.3.h:A</ta>
            <ta e="T6" id="Seg_1294" s="T5">np.h:A</ta>
            <ta e="T7" id="Seg_1295" s="T6">pro:G</ta>
            <ta e="T8" id="Seg_1296" s="T7">np:G</ta>
            <ta e="T11" id="Seg_1297" s="T10">np:Th</ta>
            <ta e="T13" id="Seg_1298" s="T12">np.h:E</ta>
            <ta e="T14" id="Seg_1299" s="T13">adv:Time</ta>
            <ta e="T16" id="Seg_1300" s="T15">pro.h:A</ta>
            <ta e="T17" id="Seg_1301" s="T16">pro.h:Poss</ta>
            <ta e="T18" id="Seg_1302" s="T17">np:Th</ta>
            <ta e="T20" id="Seg_1303" s="T19">np.h:A</ta>
            <ta e="T21" id="Seg_1304" s="T20">np:G</ta>
            <ta e="T23" id="Seg_1305" s="T22">np:Th</ta>
            <ta e="T25" id="Seg_1306" s="T24">0.3.h:A</ta>
            <ta e="T26" id="Seg_1307" s="T25">np:P</ta>
            <ta e="T27" id="Seg_1308" s="T26">np:Ins</ta>
            <ta e="T28" id="Seg_1309" s="T27">0.3.h:A</ta>
            <ta e="T32" id="Seg_1310" s="T31">np.h:A</ta>
            <ta e="T33" id="Seg_1311" s="T32">0.3.h:A</ta>
            <ta e="T34" id="Seg_1312" s="T33">np:G</ta>
            <ta e="T37" id="Seg_1313" s="T36">np:P</ta>
            <ta e="T39" id="Seg_1314" s="T38">adv:Time</ta>
            <ta e="T40" id="Seg_1315" s="T39">0.3.h:E</ta>
            <ta e="T41" id="Seg_1316" s="T40">0.1.h:A</ta>
            <ta e="T42" id="Seg_1317" s="T41">np.h:P</ta>
            <ta e="T44" id="Seg_1318" s="T43">pro.h:A</ta>
            <ta e="T47" id="Seg_1319" s="T46">np.h:P</ta>
            <ta e="T97" id="Seg_1320" s="T48">np.h:A</ta>
            <ta e="T50" id="Seg_1321" s="T49">np.h:A</ta>
            <ta e="T52" id="Seg_1322" s="T51">pro.h:A</ta>
            <ta e="T53" id="Seg_1323" s="T52">pro.h:P</ta>
            <ta e="T56" id="Seg_1324" s="T55">pro.h:A</ta>
            <ta e="T57" id="Seg_1325" s="T56">np:Th</ta>
            <ta e="T59" id="Seg_1326" s="T58">np:Poss</ta>
            <ta e="T60" id="Seg_1327" s="T59">np:Th</ta>
            <ta e="T61" id="Seg_1328" s="T60">pro.h:Poss</ta>
            <ta e="T64" id="Seg_1329" s="T63">np:Th</ta>
            <ta e="T65" id="Seg_1330" s="T64">0.3.h:A</ta>
            <ta e="T66" id="Seg_1331" s="T65">np.h:A</ta>
            <ta e="T67" id="Seg_1332" s="T66">adv:L</ta>
            <ta e="T69" id="Seg_1333" s="T68">np:Poss</ta>
            <ta e="T70" id="Seg_1334" s="T69">np:P</ta>
            <ta e="T72" id="Seg_1335" s="T71">np:A</ta>
            <ta e="T73" id="Seg_1336" s="T72">np:G</ta>
            <ta e="T77" id="Seg_1337" s="T76">np:E</ta>
            <ta e="T79" id="Seg_1338" s="T78">pro.h:A</ta>
            <ta e="T86" id="Seg_1339" s="T85">pro.h:A</ta>
            <ta e="T88" id="Seg_1340" s="T87">0.3.h:P</ta>
            <ta e="T91" id="Seg_1341" s="T90">pro.h:Th</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T14" id="Seg_1342" s="T13">RUS:core</ta>
            <ta e="T26" id="Seg_1343" s="T25">RUS:cult </ta>
            <ta e="T30" id="Seg_1344" s="T29">RUS:disc</ta>
            <ta e="T39" id="Seg_1345" s="T38">RUS:core</ta>
            <ta e="T67" id="Seg_1346" s="T66">RUS:core</ta>
            <ta e="T76" id="Seg_1347" s="T75">RUS:gram</ta>
            <ta e="T89" id="Seg_1348" s="T88">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_1349" s="T0">Раньше Идже и Кана рыбачили, рыбу добывали.</ta>
            <ta e="T12" id="Seg_1350" s="T5">Пёнэге к ним в лодку сев, тоже воровал рыбу.</ta>
            <ta e="T19" id="Seg_1351" s="T12">Идже потом догадался: он нашу рыбу ворует.</ta>
            <ta e="T25" id="Seg_1352" s="T19">Идже в тайгу пошёл, смолы много (накопал?).</ta>
            <ta e="T29" id="Seg_1353" s="T25">Скамейку смолой намазал.</ta>
            <ta e="T38" id="Seg_1354" s="T29">Но, пошёл Пёнэге, сел в лодку, прямую, новую — зад приклеился.</ta>
            <ta e="T43" id="Seg_1355" s="T38">Потом рассердился: «Пойду Иджю и Кану съем».</ta>
            <ta e="T48" id="Seg_1356" s="T43">Он пришёл, Пёнэге, Иджю и Кану съесть.</ta>
            <ta e="T55" id="Seg_1357" s="T48">Кана, Идже говорит: «Ты нас не ешь.</ta>
            <ta e="T58" id="Seg_1358" s="T55">Мы лося загоним.</ta>
            <ta e="T63" id="Seg_1359" s="T58">Лосиное мясо вкуснее нашего мяса будет».</ta>
            <ta e="T65" id="Seg_1360" s="T63">Лося гонят.</ta>
            <ta e="T71" id="Seg_1361" s="T65">Пёнэге сзади гонит — лосиное мясо съесть.</ta>
            <ta e="T75" id="Seg_1362" s="T71">Лось к Богу вверх поднялся (взлетел).</ta>
            <ta e="T81" id="Seg_1363" s="T75">А Бог узнал: «Кто такие пришли?</ta>
            <ta e="T83" id="Seg_1364" s="T81">Кана, Идже, Пёнэге. </ta>
            <ta e="T85" id="Seg_1365" s="T83">Что делать?</ta>
            <ta e="T88" id="Seg_1366" s="T85">Я звёздами сделал.</ta>
            <ta e="T92" id="Seg_1367" s="T88">Все звёздами вы станьте».</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_1368" s="T0">Earlier Idje and Kana fished with nets, they were catching fish.</ta>
            <ta e="T12" id="Seg_1369" s="T5">Pönege got into their boat and stole fish as well.</ta>
            <ta e="T19" id="Seg_1370" s="T12">Then Idje figured it out: he steals our fish.</ta>
            <ta e="T25" id="Seg_1371" s="T19">Idje went to the taiga and (dug out?) a lot of tar.</ta>
            <ta e="T29" id="Seg_1372" s="T25">He covered the bank with tar.</ta>
            <ta e="T38" id="Seg_1373" s="T29">Well, Pönege went and got into the boat, a straight one and a new one — his butt got stuck to it.</ta>
            <ta e="T43" id="Seg_1374" s="T38">Then he got angry: “I’ll go and eat Idje and Kana.”</ta>
            <ta e="T48" id="Seg_1375" s="T43">He came, Pönege, to eat Idje and Kana.</ta>
            <ta e="T55" id="Seg_1376" s="T48">Kana, Idje said: “Don’t eat us.</ta>
            <ta e="T58" id="Seg_1377" s="T55">We’ll catch an elk. ‎‎ </ta>
            <ta e="T63" id="Seg_1378" s="T58">Elk meat is better than our flesh.”</ta>
            <ta e="T65" id="Seg_1379" s="T63">They chase an elk.</ta>
            <ta e="T71" id="Seg_1380" s="T65">Pönege is at the back of the chasers to eat elk meat.</ta>
            <ta e="T75" id="Seg_1381" s="T71">The elk went up high, to God (it flew up).</ta>
            <ta e="T81" id="Seg_1382" s="T75">And God found that out: “Who came?</ta>
            <ta e="T83" id="Seg_1383" s="T81">Kana, Idje, Pönege.</ta>
            <ta e="T85" id="Seg_1384" s="T83">What to do?</ta>
            <ta e="T88" id="Seg_1385" s="T85">I made them to stars.</ta>
            <ta e="T92" id="Seg_1386" s="T88">All of you will become stars.”</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_1387" s="T0">Früher gingen Itja und Kana fischen, sie fingen Fisch.</ta>
            <ta e="T12" id="Seg_1388" s="T5">Pönege stieg zu ihnen ins Boot und stahl den Fisch.</ta>
            <ta e="T19" id="Seg_1389" s="T12">Itja fand raus: Er stielt unseren Fisch. </ta>
            <ta e="T25" id="Seg_1390" s="T19">Idje ging in die Taiga und (grub?) viel Teer aus.</ta>
            <ta e="T29" id="Seg_1391" s="T25">Er bedeckte die Bank mit Teer. </ta>
            <ta e="T38" id="Seg_1392" s="T29">Aber Pönege stieg ins Boot, gerade, neu — sein Hintern klebte daran fest.</ta>
            <ta e="T43" id="Seg_1393" s="T38">Dann wurde er wütend: "Ich werde gehen und Itja und Kana essen." </ta>
            <ta e="T48" id="Seg_1394" s="T43">Er, Pönege, kam um Itja und Kana zu essen.</ta>
            <ta e="T55" id="Seg_1395" s="T48">Kana, Itja sagte: "Iss uns nicht." </ta>
            <ta e="T58" id="Seg_1396" s="T55">Wir fangen einen Elch.</ta>
            <ta e="T63" id="Seg_1397" s="T58">Elchfleisch ist besser als unser Fleisch. </ta>
            <ta e="T65" id="Seg_1398" s="T63">Sie jagten einen Elch. </ta>
            <ta e="T71" id="Seg_1399" s="T65">Pönege ist hinter den Jägern, um Elch zu essen. </ta>
            <ta e="T75" id="Seg_1400" s="T71">Der Elch stieg hoch, zu Gott (er flog hoch). </ta>
            <ta e="T81" id="Seg_1401" s="T75">Und Gott erkannte: "Wer kommt dort? </ta>
            <ta e="T83" id="Seg_1402" s="T81">Kana, Itja, Pönege. </ta>
            <ta e="T85" id="Seg_1403" s="T83">Was tun? </ta>
            <ta e="T88" id="Seg_1404" s="T85">Ich habe sie zu Sternen gemacht. </ta>
            <ta e="T92" id="Seg_1405" s="T88">Ihr werdet alle Sterne werden." </ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_1406" s="T0">Раньше Идже-кана рыбачили, рыбу добывали.</ta>
            <ta e="T12" id="Seg_1407" s="T5">Пёнэге к ним в лодку сев, воровал рыбу тоже.</ta>
            <ta e="T19" id="Seg_1408" s="T12">Идже потом догадался: он нашу рыбу ворует.</ta>
            <ta e="T25" id="Seg_1409" s="T19">Идже в тайгу пошёл, смолы много накопал.</ta>
            <ta e="T29" id="Seg_1410" s="T25">Беседку смолой намазал.</ta>
            <ta e="T38" id="Seg_1411" s="T29">Но, пошёл Пёнэге, сел в лодку, прямую, новую — зад приклеился.</ta>
            <ta e="T43" id="Seg_1412" s="T38">Потом рассердился: «Пойду Иджю съем».</ta>
            <ta e="T48" id="Seg_1413" s="T43">Он пришёл, Пёнэге, Иджю съесть.</ta>
            <ta e="T55" id="Seg_1414" s="T48">Кана, кана-Идже говорит: «Ты Иджю не ешь.</ta>
            <ta e="T58" id="Seg_1415" s="T55">Мы лося загоним. </ta>
            <ta e="T63" id="Seg_1416" s="T58">Лосиное мясо вкуснее нашего мяса будет».</ta>
            <ta e="T65" id="Seg_1417" s="T63">Лося гонят.</ta>
            <ta e="T71" id="Seg_1418" s="T65">Пёнэге сзади гонит — лосиное мясо съесть.</ta>
            <ta e="T75" id="Seg_1419" s="T71">Лось к Богу вверх поднялся (взлетел).</ta>
            <ta e="T81" id="Seg_1420" s="T75">А Бог узнал: «Кто такой пришёл?</ta>
            <ta e="T83" id="Seg_1421" s="T81">Кана-Идже, Пёнэге.</ta>
            <ta e="T85" id="Seg_1422" s="T83">Что делать?</ta>
            <ta e="T88" id="Seg_1423" s="T85">Я звёздами сделал.</ta>
            <ta e="T92" id="Seg_1424" s="T88">Все звёздами вы станьте».</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T12" id="Seg_1425" s="T5">[AAV:] twelkut ?</ta>
            <ta e="T25" id="Seg_1426" s="T19">[AAV:] pakelbat &gt; paqam( )üt ?</ta>
            <ta e="T29" id="Seg_1427" s="T25">[AAV:] Here "беседка" is a seat in the boat.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T94" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T95" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T96" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T97" />
            <conversion-tli id="T49" />
            <conversion-tli id="T98" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T99" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
