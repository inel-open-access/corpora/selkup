<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KFN_1967_Language_flk</transcription-name>
         <referenced-file url="KFN_1967_Language_flk.wav" />
         <referenced-file url="KFN_1967_Language_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KFN_1967_Language_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">100</ud-information>
            <ud-information attribute-name="# HIAT:w">63</ud-information>
            <ud-information attribute-name="# e">65</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">2</ud-information>
            <ud-information attribute-name="# HIAT:u">14</ud-information>
            <ud-information attribute-name="# sc">4</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KFN">
            <abbreviation>KFN</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="1.667" type="appl" />
         <tli id="T2" time="2.5333267218510516" />
         <tli id="T65" time="3.073325312561407" />
         <tli id="T3" time="6.766649007049519" />
         <tli id="T4" time="7.66" type="appl" />
         <tli id="T5" time="8.76" type="appl" />
         <tli id="T6" time="9.86" type="appl" />
         <tli id="T7" time="10.96" type="appl" />
         <tli id="T8" time="13.539964663367066" />
         <tli id="T9" time="14.07" type="appl" />
         <tli id="T10" time="14.99" type="appl" />
         <tli id="T11" time="15.91" type="appl" />
         <tli id="T12" time="16.83" type="appl" />
         <tli id="T13" time="17.75" type="appl" />
         <tli id="T14" time="18.67" type="appl" />
         <tli id="T15" time="19.59" type="appl" />
         <tli id="T67" time="20.05" type="intp" />
         <tli id="T16" time="20.51" type="appl" />
         <tli id="T17" time="21.63" type="appl" />
         <tli id="T18" time="22.55" type="appl" />
         <tli id="T19" time="23.47" type="appl" />
         <tli id="T66" time="23.93" type="intp" />
         <tli id="T20" time="24.39" type="appl" />
         <tli id="T21" time="25.31" type="appl" />
         <tli id="T22" time="26.23" type="appl" />
         <tli id="T23" time="27.15" type="appl" />
         <tli id="T24" time="29.25" type="appl" />
         <tli id="T25" time="30.71" type="appl" />
         <tli id="T26" time="31.77" type="appl" />
         <tli id="T27" time="32.31" type="appl" />
         <tli id="T28" time="32.85" type="appl" />
         <tli id="T29" time="33.39" type="appl" />
         <tli id="T30" time="33.93" type="appl" />
         <tli id="T31" time="34.47" type="appl" />
         <tli id="T32" time="36.166572279057775" />
         <tli id="T33" time="36.63" type="appl" />
         <tli id="T34" time="37.53" type="appl" />
         <tli id="T35" time="38.45" type="appl" />
         <tli id="T36" time="39.27" type="appl" />
         <tli id="T37" time="40.09" type="appl" />
         <tli id="T38" time="40.91" type="appl" />
         <tli id="T39" time="42.143" type="appl" />
         <tli id="T40" time="42.957" type="appl" />
         <tli id="T41" time="44.69321669286697" />
         <tli id="T42" time="45.245" type="appl" />
         <tli id="T43" time="45.82654706843191" />
         <tli id="T44" time="46.24" type="appl" />
         <tli id="T45" time="46.66" type="appl" />
         <tli id="T46" time="47.08" type="appl" />
         <tli id="T47" time="47.5" type="appl" />
         <tli id="T48" time="47.92" type="appl" />
         <tli id="T49" time="49.64653709901258" />
         <tli id="T50" time="50.361" type="appl" />
         <tli id="T51" time="50.993" type="appl" />
         <tli id="T52" time="51.626" type="appl" />
         <tli id="T53" time="52.258" type="appl" />
         <tli id="T54" time="53.266527651552366" />
         <tli id="T68" time="53.56551975110883" type="intp" />
         <tli id="T55" time="54.313" type="appl" />
         <tli id="T56" time="55.016" type="appl" />
         <tli id="T57" time="55.719" type="appl" />
         <tli id="T58" time="56.422" type="appl" />
         <tli id="T59" time="58.233181356234034" />
         <tli id="T60" time="59.108" type="appl" />
         <tli id="T61" time="60.434" type="appl" />
         <tli id="T62" time="61.76" type="appl" />
         <tli id="T63" time="63.086" type="appl" />
         <tli id="T64" time="67.853" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KFN"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T65" id="Seg_0" n="sc" s="T0">
               <ts e="T65" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Čapte</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">eǯ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_10" n="HIAT:w" s="T2">čat</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T63" id="Seg_13" n="sc" s="T3">
               <ts e="T8" id="Seg_15" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_17" n="HIAT:w" s="T3">Ugot</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_20" n="HIAT:w" s="T4">qudnan</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_23" n="HIAT:w" s="T5">oqqur</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">öǯ</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">eːkumba</ts>
                  <nts id="Seg_30" n="HIAT:ip">.</nts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_33" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_35" n="HIAT:w" s="T8">Patom</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_38" n="HIAT:w" s="T9">tabɨt</ts>
                  <nts id="Seg_39" n="HIAT:ip">,</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_42" n="HIAT:w" s="T10">koček</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_45" n="HIAT:w" s="T11">qut</ts>
                  <nts id="Seg_46" n="HIAT:ip">,</nts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">taqqɨlɨmbat</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_51" n="HIAT:ip">—</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_54" n="HIAT:w" s="T13">Nomn</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_57" n="HIAT:w" s="T14">enne</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_60" n="HIAT:w" s="T15">medegu</ts>
                  <nts id="Seg_61" n="HIAT:ip">.</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_64" n="HIAT:u" s="T67">
                  <nts id="Seg_65" n="HIAT:ip">(</nts>
                  <nts id="Seg_66" n="HIAT:ip">(</nts>
                  <ats e="T16" id="Seg_67" n="HIAT:non-pho" s="T67">…</ats>
                  <nts id="Seg_68" n="HIAT:ip">)</nts>
                  <nts id="Seg_69" n="HIAT:ip">)</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_72" n="HIAT:w" s="T16">Pop</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_75" n="HIAT:w" s="T17">to</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_78" n="HIAT:w" s="T18">ille</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_81" n="HIAT:w" s="T19">qwetembat</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_83" n="HIAT:ip">(</nts>
                  <nts id="Seg_84" n="HIAT:ip">(</nts>
                  <ats e="T20" id="Seg_85" n="HIAT:non-pho" s="T66">…</ats>
                  <nts id="Seg_86" n="HIAT:ip">)</nts>
                  <nts id="Seg_87" n="HIAT:ip">)</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_89" n="HIAT:ip">—</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_92" n="HIAT:w" s="T20">Nomn</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_95" n="HIAT:w" s="T21">enne</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_98" n="HIAT:w" s="T22">medegu</ts>
                  <nts id="Seg_99" n="HIAT:ip">.</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_102" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_104" n="HIAT:w" s="T23">Mešpɨmbat</ts>
                  <nts id="Seg_105" n="HIAT:ip">,</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_108" n="HIAT:w" s="T24">mešpɨmbat</ts>
                  <nts id="Seg_109" n="HIAT:ip">.</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_112" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_114" n="HIAT:w" s="T25">Nop</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_117" n="HIAT:w" s="T26">patom</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_120" n="HIAT:w" s="T27">koštembat</ts>
                  <nts id="Seg_121" n="HIAT:ip">,</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_124" n="HIAT:w" s="T28">što</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_127" n="HIAT:w" s="T29">tabet</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_130" n="HIAT:w" s="T30">enne</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_133" n="HIAT:w" s="T31">medlade</ts>
                  <nts id="Seg_134" n="HIAT:ip">.</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_137" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_139" n="HIAT:w" s="T32">Qaj</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_142" n="HIAT:w" s="T33">megu</ts>
                  <nts id="Seg_143" n="HIAT:ip">?</nts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_146" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_148" n="HIAT:w" s="T34">Wes</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_151" n="HIAT:w" s="T35">eǯemdɨt</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_154" n="HIAT:w" s="T36">targu</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_157" n="HIAT:w" s="T37">nado</ts>
                  <nts id="Seg_158" n="HIAT:ip">.</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_161" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_163" n="HIAT:w" s="T38">Wes</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_166" n="HIAT:w" s="T39">eǯemdɨt</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_169" n="HIAT:w" s="T40">tarnat</ts>
                  <nts id="Seg_170" n="HIAT:ip">.</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_173" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_175" n="HIAT:w" s="T41">Wes</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_178" n="HIAT:w" s="T42">püːʒat</ts>
                  <nts id="Seg_179" n="HIAT:ip">.</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_182" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_184" n="HIAT:w" s="T43">Kutɨnaj</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_187" n="HIAT:w" s="T44">kutarnaj</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_190" n="HIAT:w" s="T45">čenčela</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_193" n="HIAT:w" s="T47">ark</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_196" n="HIAT:w" s="T48">čare</ts>
                  <nts id="Seg_197" n="HIAT:ip">.</nts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_200" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_202" n="HIAT:w" s="T49">Našaqqet</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_205" n="HIAT:w" s="T50">qajmnʼa</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_208" n="HIAT:w" s="T51">qun</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_211" n="HIAT:w" s="T52">aː</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_214" n="HIAT:w" s="T53">tänulelat</ts>
                  <nts id="Seg_215" n="HIAT:ip">.</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_218" n="HIAT:u" s="T54">
                  <ts e="T68" id="Seg_220" n="HIAT:w" s="T54">Na</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_222" n="HIAT:ip">(</nts>
                  <ts e="T55" id="Seg_224" n="HIAT:w" s="T68">ušitet</ts>
                  <nts id="Seg_225" n="HIAT:ip">)</nts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_228" n="HIAT:w" s="T55">tak</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_231" n="HIAT:w" s="T56">čʼaǯat</ts>
                  <nts id="Seg_232" n="HIAT:ip">,</nts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_235" n="HIAT:w" s="T57">olga</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_238" n="HIAT:w" s="T58">wargelat</ts>
                  <nts id="Seg_239" n="HIAT:ip">.</nts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_242" n="HIAT:u" s="T59">
                  <nts id="Seg_243" n="HIAT:ip">(</nts>
                  <ts e="T60" id="Seg_245" n="HIAT:w" s="T59">Elʼǯik</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_248" n="HIAT:w" s="T60">qwelʼgit</ts>
                  <nts id="Seg_249" n="HIAT:ip">)</nts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_252" n="HIAT:w" s="T61">še</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_255" n="HIAT:w" s="T62">memɨndɨt</ts>
                  <nts id="Seg_256" n="HIAT:ip">.</nts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T65" id="Seg_258" n="sc" s="T0">
               <ts e="T1" id="Seg_260" n="e" s="T0">Čapte </ts>
               <ts e="T2" id="Seg_262" n="e" s="T1">eǯ </ts>
               <ts e="T65" id="Seg_264" n="e" s="T2">čat. </ts>
            </ts>
            <ts e="T63" id="Seg_265" n="sc" s="T3">
               <ts e="T4" id="Seg_267" n="e" s="T3">Ugot </ts>
               <ts e="T5" id="Seg_269" n="e" s="T4">qudnan </ts>
               <ts e="T6" id="Seg_271" n="e" s="T5">oqqur </ts>
               <ts e="T7" id="Seg_273" n="e" s="T6">öǯ </ts>
               <ts e="T8" id="Seg_275" n="e" s="T7">eːkumba. </ts>
               <ts e="T9" id="Seg_277" n="e" s="T8">Patom </ts>
               <ts e="T10" id="Seg_279" n="e" s="T9">tabɨt, </ts>
               <ts e="T11" id="Seg_281" n="e" s="T10">koček </ts>
               <ts e="T12" id="Seg_283" n="e" s="T11">qut, </ts>
               <ts e="T13" id="Seg_285" n="e" s="T12">taqqɨlɨmbat — </ts>
               <ts e="T14" id="Seg_287" n="e" s="T13">Nomn </ts>
               <ts e="T15" id="Seg_289" n="e" s="T14">enne </ts>
               <ts e="T67" id="Seg_291" n="e" s="T15">medegu. </ts>
               <ts e="T16" id="Seg_293" n="e" s="T67">((…)) </ts>
               <ts e="T17" id="Seg_295" n="e" s="T16">Pop </ts>
               <ts e="T18" id="Seg_297" n="e" s="T17">to </ts>
               <ts e="T19" id="Seg_299" n="e" s="T18">ille </ts>
               <ts e="T66" id="Seg_301" n="e" s="T19">qwetembat </ts>
               <ts e="T20" id="Seg_303" n="e" s="T66">((…)) — </ts>
               <ts e="T21" id="Seg_305" n="e" s="T20">Nomn </ts>
               <ts e="T22" id="Seg_307" n="e" s="T21">enne </ts>
               <ts e="T23" id="Seg_309" n="e" s="T22">medegu. </ts>
               <ts e="T24" id="Seg_311" n="e" s="T23">Mešpɨmbat, </ts>
               <ts e="T25" id="Seg_313" n="e" s="T24">mešpɨmbat. </ts>
               <ts e="T26" id="Seg_315" n="e" s="T25">Nop </ts>
               <ts e="T27" id="Seg_317" n="e" s="T26">patom </ts>
               <ts e="T28" id="Seg_319" n="e" s="T27">koštembat, </ts>
               <ts e="T29" id="Seg_321" n="e" s="T28">što </ts>
               <ts e="T30" id="Seg_323" n="e" s="T29">tabet </ts>
               <ts e="T31" id="Seg_325" n="e" s="T30">enne </ts>
               <ts e="T32" id="Seg_327" n="e" s="T31">medlade. </ts>
               <ts e="T33" id="Seg_329" n="e" s="T32">Qaj </ts>
               <ts e="T34" id="Seg_331" n="e" s="T33">megu? </ts>
               <ts e="T35" id="Seg_333" n="e" s="T34">Wes </ts>
               <ts e="T36" id="Seg_335" n="e" s="T35">eǯemdɨt </ts>
               <ts e="T37" id="Seg_337" n="e" s="T36">targu </ts>
               <ts e="T38" id="Seg_339" n="e" s="T37">nado. </ts>
               <ts e="T39" id="Seg_341" n="e" s="T38">Wes </ts>
               <ts e="T40" id="Seg_343" n="e" s="T39">eǯemdɨt </ts>
               <ts e="T41" id="Seg_345" n="e" s="T40">tarnat. </ts>
               <ts e="T42" id="Seg_347" n="e" s="T41">Wes </ts>
               <ts e="T43" id="Seg_349" n="e" s="T42">püːʒat. </ts>
               <ts e="T44" id="Seg_351" n="e" s="T43">Kutɨnaj </ts>
               <ts e="T45" id="Seg_353" n="e" s="T44">kutarnaj </ts>
               <ts e="T47" id="Seg_355" n="e" s="T45">čenčela </ts>
               <ts e="T48" id="Seg_357" n="e" s="T47">ark </ts>
               <ts e="T49" id="Seg_359" n="e" s="T48">čare. </ts>
               <ts e="T50" id="Seg_361" n="e" s="T49">Našaqqet </ts>
               <ts e="T51" id="Seg_363" n="e" s="T50">qajmnʼa </ts>
               <ts e="T52" id="Seg_365" n="e" s="T51">qun </ts>
               <ts e="T53" id="Seg_367" n="e" s="T52">aː </ts>
               <ts e="T54" id="Seg_369" n="e" s="T53">tänulelat. </ts>
               <ts e="T68" id="Seg_371" n="e" s="T54">Na </ts>
               <ts e="T55" id="Seg_373" n="e" s="T68">(ušitet) </ts>
               <ts e="T56" id="Seg_375" n="e" s="T55">tak </ts>
               <ts e="T57" id="Seg_377" n="e" s="T56">čʼaǯat, </ts>
               <ts e="T58" id="Seg_379" n="e" s="T57">olga </ts>
               <ts e="T59" id="Seg_381" n="e" s="T58">wargelat. </ts>
               <ts e="T60" id="Seg_383" n="e" s="T59">(Elʼǯik </ts>
               <ts e="T61" id="Seg_385" n="e" s="T60">qwelʼgit) </ts>
               <ts e="T62" id="Seg_387" n="e" s="T61">še </ts>
               <ts e="T63" id="Seg_389" n="e" s="T62">memɨndɨt. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T65" id="Seg_390" s="T0">KFN_1967_Language_flk.001 (001)</ta>
            <ta e="T8" id="Seg_391" s="T3">KFN_1967_Language_flk.002 (002)</ta>
            <ta e="T67" id="Seg_392" s="T8">KFN_1967_Language_flk.003 (003)</ta>
            <ta e="T23" id="Seg_393" s="T67">KFN_1967_Language_flk.004 (004)</ta>
            <ta e="T25" id="Seg_394" s="T23">KFN_1967_Language_flk.005 (005)</ta>
            <ta e="T32" id="Seg_395" s="T25">KFN_1967_Language_flk.006 (006)</ta>
            <ta e="T34" id="Seg_396" s="T32">KFN_1967_Language_flk.007 (007)</ta>
            <ta e="T38" id="Seg_397" s="T34">KFN_1967_Language_flk.008 (008)</ta>
            <ta e="T41" id="Seg_398" s="T38">KFN_1967_Language_flk.009 (009)</ta>
            <ta e="T43" id="Seg_399" s="T41">KFN_1967_Language_flk.010 (010)</ta>
            <ta e="T49" id="Seg_400" s="T43">KFN_1967_Language_flk.011 (011)</ta>
            <ta e="T54" id="Seg_401" s="T49">KFN_1967_Language_flk.012 (012)</ta>
            <ta e="T59" id="Seg_402" s="T54">KFN_1967_Language_flk.013 (013)</ta>
            <ta e="T63" id="Seg_403" s="T59">KFN_1967_Language_flk.014 (014)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T65" id="Seg_404" s="T0">Чаптэ эдж чат.</ta>
            <ta e="T8" id="Seg_405" s="T3">Угот ӄуднан оӄӄур эдж э̄кумба.</ta>
            <ta e="T67" id="Seg_406" s="T8">Потом табыт, кочек ӄут, таӄӄылымбат — Номн эннэ медэгу.</ta>
            <ta e="T23" id="Seg_407" s="T67">Поп то илле ӄвэтэмбат — Номн эннэ медэгу.</ta>
            <ta e="T25" id="Seg_408" s="T23">Мешпымбат, мешпымбат.</ta>
            <ta e="T32" id="Seg_409" s="T25">Ноп потом коштэмбат, что табэт эннэ медладэ.</ta>
            <ta e="T34" id="Seg_410" s="T32">Кай мегу?</ta>
            <ta e="T38" id="Seg_411" s="T34">Вес эджэмд таргу надо.</ta>
            <ta e="T41" id="Seg_412" s="T38">Вес э̄джэмд тарнат.</ta>
            <ta e="T43" id="Seg_413" s="T41">Вес ӱльджат.</ta>
            <ta e="T49" id="Seg_414" s="T43">Коднай кутарнай а̄ ҷэнҷэла арк чаре.</ta>
            <ta e="T54" id="Seg_415" s="T49">Нашаӄӄэт ӄаймная ӄун а̄ тӓнуледат.</ta>
            <ta e="T59" id="Seg_416" s="T54">Науштэ так ҷаджат, олга варгат.</ta>
            <ta e="T63" id="Seg_417" s="T59">Ыльджек ӄведи шэ мемындыт.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T65" id="Seg_418" s="T0">Čʼapte eǯ čat.</ta>
            <ta e="T8" id="Seg_419" s="T3">Ugot qudnan oqqur eǯ eːkumba.</ta>
            <ta e="T67" id="Seg_420" s="T8">Potom tabɨt, koček qut, taqqɨlɨmbat — Nomn enne medegu.</ta>
            <ta e="T23" id="Seg_421" s="T67">Pop to ille qvetembat — Nomn enne medegu.</ta>
            <ta e="T25" id="Seg_422" s="T23">Mešpɨmbat, mešpɨmbat.</ta>
            <ta e="T32" id="Seg_423" s="T25">Nop potom koštembat, što tabet enne medlade.</ta>
            <ta e="T34" id="Seg_424" s="T32">Kaj megu?</ta>
            <ta e="T38" id="Seg_425" s="T34">Ves eǯemd targu nado.</ta>
            <ta e="T41" id="Seg_426" s="T38">Ves eːǯemd tarnat.</ta>
            <ta e="T43" id="Seg_427" s="T41">Ves ülʼǯat.</ta>
            <ta e="T49" id="Seg_428" s="T43">Kodnaj kutarnaj aː čenčela ark čare.</ta>
            <ta e="T54" id="Seg_429" s="T49">Našaqqet qajmnaja qun aː tänuledat.</ta>
            <ta e="T59" id="Seg_430" s="T54">Naušte tak čaǯat, olga vargat.</ta>
            <ta e="T63" id="Seg_431" s="T59">Ɨlʼǯek qvedi še memɨndɨt.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T65" id="Seg_432" s="T0">Čapte eǯ čat. </ta>
            <ta e="T8" id="Seg_433" s="T3">Ugot qudnan oqqur öǯ eːkumba. </ta>
            <ta e="T67" id="Seg_434" s="T8">Patom tabɨt, koček qut, taqqɨlɨmbat — Nomn enne medegu. </ta>
            <ta e="T23" id="Seg_435" s="T67">((…)) Pop to ille qwetembat ((…)) — Nomn enne medegu. </ta>
            <ta e="T25" id="Seg_436" s="T23">Mešpɨmbat, mešpɨmbat. </ta>
            <ta e="T32" id="Seg_437" s="T25">Nop patom koštembat, što tabet enne medlade. </ta>
            <ta e="T34" id="Seg_438" s="T32">Qaj megu? </ta>
            <ta e="T38" id="Seg_439" s="T34">Wes eǯemdɨt targu nado. </ta>
            <ta e="T41" id="Seg_440" s="T38">Wes eǯemdɨt tarnat. </ta>
            <ta e="T43" id="Seg_441" s="T41">Wes püːʒat. </ta>
            <ta e="T49" id="Seg_442" s="T43">Kutɨnaj kutarnaj čenčela ark čare. </ta>
            <ta e="T54" id="Seg_443" s="T49">Našaqqet qajmnʼa qun aː tänulelat. </ta>
            <ta e="T59" id="Seg_444" s="T54">Na (ušitet) tak čʼaǯat, olga wargelat. </ta>
            <ta e="T63" id="Seg_445" s="T59">(Elʼǯik qwelʼgit) še memɨndɨt. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_446" s="T0">čapte</ta>
            <ta e="T2" id="Seg_447" s="T1">eǯ</ta>
            <ta e="T65" id="Seg_448" s="T2">čat</ta>
            <ta e="T4" id="Seg_449" s="T3">ugot</ta>
            <ta e="T5" id="Seg_450" s="T4">qu-d-nan</ta>
            <ta e="T6" id="Seg_451" s="T5">oqqur</ta>
            <ta e="T7" id="Seg_452" s="T6">öǯ</ta>
            <ta e="T8" id="Seg_453" s="T7">eː-ku-mba</ta>
            <ta e="T9" id="Seg_454" s="T8">patom</ta>
            <ta e="T10" id="Seg_455" s="T9">tab-ǝ-t</ta>
            <ta e="T11" id="Seg_456" s="T10">koček</ta>
            <ta e="T12" id="Seg_457" s="T11">qu-t</ta>
            <ta e="T13" id="Seg_458" s="T12">taqqɨ-lɨ-mba-t</ta>
            <ta e="T14" id="Seg_459" s="T13">Nom-n</ta>
            <ta e="T15" id="Seg_460" s="T14">inne</ta>
            <ta e="T67" id="Seg_461" s="T15">mede-gu</ta>
            <ta e="T17" id="Seg_462" s="T16">po-p</ta>
            <ta e="T18" id="Seg_463" s="T17">to</ta>
            <ta e="T19" id="Seg_464" s="T18">ille</ta>
            <ta e="T66" id="Seg_465" s="T19">qwe-te-mba-t</ta>
            <ta e="T21" id="Seg_466" s="T20">Nom-n</ta>
            <ta e="T22" id="Seg_467" s="T21">inne</ta>
            <ta e="T23" id="Seg_468" s="T22">mede-gu</ta>
            <ta e="T24" id="Seg_469" s="T23">me-špɨ-mba-t</ta>
            <ta e="T25" id="Seg_470" s="T24">me-špɨ-mba-t</ta>
            <ta e="T26" id="Seg_471" s="T25">nop</ta>
            <ta e="T27" id="Seg_472" s="T26">patom</ta>
            <ta e="T28" id="Seg_473" s="T27">košte-mba-t</ta>
            <ta e="T29" id="Seg_474" s="T28">što</ta>
            <ta e="T30" id="Seg_475" s="T29">tab-e-t</ta>
            <ta e="T31" id="Seg_476" s="T30">inne</ta>
            <ta e="T32" id="Seg_477" s="T31">med-la-de</ta>
            <ta e="T33" id="Seg_478" s="T32">qaj</ta>
            <ta e="T34" id="Seg_479" s="T33">me-gu</ta>
            <ta e="T35" id="Seg_480" s="T34">wes</ta>
            <ta e="T36" id="Seg_481" s="T35">eǯe-m-dɨt</ta>
            <ta e="T37" id="Seg_482" s="T36">tar-gu</ta>
            <ta e="T38" id="Seg_483" s="T37">nado</ta>
            <ta e="T39" id="Seg_484" s="T38">wes</ta>
            <ta e="T40" id="Seg_485" s="T39">eǯe-m-dɨt</ta>
            <ta e="T41" id="Seg_486" s="T40">tar-na-t</ta>
            <ta e="T42" id="Seg_487" s="T41">wes</ta>
            <ta e="T43" id="Seg_488" s="T42">püːʒa-t</ta>
            <ta e="T44" id="Seg_489" s="T43">kutɨ-naj</ta>
            <ta e="T45" id="Seg_490" s="T44">kutar-naj</ta>
            <ta e="T47" id="Seg_491" s="T45">čenče-la</ta>
            <ta e="T48" id="Seg_492" s="T47">ark</ta>
            <ta e="T49" id="Seg_493" s="T48">čare</ta>
            <ta e="T50" id="Seg_494" s="T49">našaq-qet</ta>
            <ta e="T51" id="Seg_495" s="T50">qaj-m-nʼa</ta>
            <ta e="T52" id="Seg_496" s="T51">qu-n</ta>
            <ta e="T53" id="Seg_497" s="T52">aː</ta>
            <ta e="T54" id="Seg_498" s="T53">tänu-le-lat</ta>
            <ta e="T68" id="Seg_499" s="T54">na</ta>
            <ta e="T55" id="Seg_500" s="T68">ušitet</ta>
            <ta e="T56" id="Seg_501" s="T55">tak</ta>
            <ta e="T57" id="Seg_502" s="T56">čʼaǯa-t</ta>
            <ta e="T58" id="Seg_503" s="T57">olga</ta>
            <ta e="T59" id="Seg_504" s="T58">warge-lat</ta>
            <ta e="T60" id="Seg_505" s="T59">elʼǯi-k</ta>
            <ta e="T61" id="Seg_506" s="T60">qwelʼgit</ta>
            <ta e="T62" id="Seg_507" s="T61">še</ta>
            <ta e="T63" id="Seg_508" s="T62">me-mɨ-ndɨ-t</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_509" s="T0">čapte</ta>
            <ta e="T2" id="Seg_510" s="T1">ɛʒɛ</ta>
            <ta e="T65" id="Seg_511" s="T2">čad</ta>
            <ta e="T4" id="Seg_512" s="T3">ugon</ta>
            <ta e="T5" id="Seg_513" s="T4">qum-t-nan</ta>
            <ta e="T6" id="Seg_514" s="T5">okkər</ta>
            <ta e="T7" id="Seg_515" s="T6">ɛʒɛ</ta>
            <ta e="T8" id="Seg_516" s="T7">e-ku-mbɨ</ta>
            <ta e="T9" id="Seg_517" s="T8">patom</ta>
            <ta e="T10" id="Seg_518" s="T9">tab-ɨ-t</ta>
            <ta e="T11" id="Seg_519" s="T10">koček</ta>
            <ta e="T12" id="Seg_520" s="T11">qum-t</ta>
            <ta e="T13" id="Seg_521" s="T12">takkɨ-lɨ-mbɨ-dət</ta>
            <ta e="T14" id="Seg_522" s="T13">nom-ni</ta>
            <ta e="T15" id="Seg_523" s="T14">inne</ta>
            <ta e="T67" id="Seg_524" s="T15">medɨ-gu</ta>
            <ta e="T17" id="Seg_525" s="T16">po-m</ta>
            <ta e="T18" id="Seg_526" s="T17">to</ta>
            <ta e="T19" id="Seg_527" s="T18">illä</ta>
            <ta e="T66" id="Seg_528" s="T19">qwän-ntɨ-mbɨ-dət</ta>
            <ta e="T21" id="Seg_529" s="T20">nom-ni</ta>
            <ta e="T22" id="Seg_530" s="T21">inne</ta>
            <ta e="T23" id="Seg_531" s="T22">medɨ-gu</ta>
            <ta e="T24" id="Seg_532" s="T23">me-špɨ-mbɨ-dət</ta>
            <ta e="T25" id="Seg_533" s="T24">me-špɨ-mbɨ-dət</ta>
            <ta e="T26" id="Seg_534" s="T25">nom</ta>
            <ta e="T27" id="Seg_535" s="T26">patom</ta>
            <ta e="T28" id="Seg_536" s="T27">koštɨ-mbɨ-tɨ</ta>
            <ta e="T29" id="Seg_537" s="T28">što</ta>
            <ta e="T30" id="Seg_538" s="T29">tab-ɨ-t</ta>
            <ta e="T31" id="Seg_539" s="T30">inne</ta>
            <ta e="T32" id="Seg_540" s="T31">medɨ-la-dət</ta>
            <ta e="T33" id="Seg_541" s="T32">qaj</ta>
            <ta e="T34" id="Seg_542" s="T33">me-gu</ta>
            <ta e="T35" id="Seg_543" s="T34">wesʼ</ta>
            <ta e="T36" id="Seg_544" s="T35">ɛʒɛ-m-dɨt</ta>
            <ta e="T37" id="Seg_545" s="T36">tar-gu</ta>
            <ta e="T38" id="Seg_546" s="T37">naːda</ta>
            <ta e="T39" id="Seg_547" s="T38">wesʼ</ta>
            <ta e="T40" id="Seg_548" s="T39">ɛʒɛ-m-dɨt</ta>
            <ta e="T41" id="Seg_549" s="T40">tar-ŋɨ-dət</ta>
            <ta e="T42" id="Seg_550" s="T41">wesʼ</ta>
            <ta e="T43" id="Seg_551" s="T42">püːʒu-dət</ta>
            <ta e="T44" id="Seg_552" s="T43">kutɨ-naj</ta>
            <ta e="T45" id="Seg_553" s="T44">kutar-naj</ta>
            <ta e="T47" id="Seg_554" s="T45">čenčɨ-lɨ</ta>
            <ta e="T48" id="Seg_555" s="T47">aːrq</ta>
            <ta e="T49" id="Seg_556" s="T48">čaːr</ta>
            <ta e="T50" id="Seg_557" s="T49">naššak-qɨn</ta>
            <ta e="T51" id="Seg_558" s="T50">qaj-m-naj</ta>
            <ta e="T52" id="Seg_559" s="T51">qum-t</ta>
            <ta e="T53" id="Seg_560" s="T52">aː</ta>
            <ta e="T54" id="Seg_561" s="T53">tanu-la-lt</ta>
            <ta e="T68" id="Seg_562" s="T54">na</ta>
            <ta e="T55" id="Seg_563" s="T68">%%</ta>
            <ta e="T56" id="Seg_564" s="T55">tak</ta>
            <ta e="T57" id="Seg_565" s="T56">čačɨ-dət</ta>
            <ta e="T58" id="Seg_566" s="T57">olga</ta>
            <ta e="T59" id="Seg_567" s="T58">wargɨ-lt</ta>
            <ta e="T60" id="Seg_568" s="T59">%%-k</ta>
            <ta e="T61" id="Seg_569" s="T60">%%</ta>
            <ta e="T62" id="Seg_570" s="T61">šeː</ta>
            <ta e="T63" id="Seg_571" s="T62">me-mbɨ-ndɨ-tɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_572" s="T0">tale.[NOM]</ta>
            <ta e="T2" id="Seg_573" s="T1">word.[NOM]</ta>
            <ta e="T65" id="Seg_574" s="T2">about</ta>
            <ta e="T4" id="Seg_575" s="T3">earlier</ta>
            <ta e="T5" id="Seg_576" s="T4">human.being-PL-ADES</ta>
            <ta e="T6" id="Seg_577" s="T5">one</ta>
            <ta e="T7" id="Seg_578" s="T6">word.[NOM]</ta>
            <ta e="T8" id="Seg_579" s="T7">be-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T9" id="Seg_580" s="T8">then</ta>
            <ta e="T10" id="Seg_581" s="T9">(s)he-EP-PL</ta>
            <ta e="T11" id="Seg_582" s="T10">much</ta>
            <ta e="T12" id="Seg_583" s="T11">human.being.[NOM]-PL</ta>
            <ta e="T13" id="Seg_584" s="T12">collect-RES-PST.NAR-3PL</ta>
            <ta e="T14" id="Seg_585" s="T13">god-ALL</ta>
            <ta e="T15" id="Seg_586" s="T14">up</ta>
            <ta e="T67" id="Seg_587" s="T15">get.to-INF</ta>
            <ta e="T17" id="Seg_588" s="T16">tree-ACC</ta>
            <ta e="T18" id="Seg_589" s="T17">that</ta>
            <ta e="T19" id="Seg_590" s="T18">down</ta>
            <ta e="T66" id="Seg_591" s="T19">go.away-IPFV-PST.NAR-3PL</ta>
            <ta e="T21" id="Seg_592" s="T20">god-ALL</ta>
            <ta e="T22" id="Seg_593" s="T21">up</ta>
            <ta e="T23" id="Seg_594" s="T22">get.to-INF</ta>
            <ta e="T24" id="Seg_595" s="T23">do-IPFV2-PST.NAR-3PL</ta>
            <ta e="T25" id="Seg_596" s="T24">do-IPFV2-PST.NAR-3PL</ta>
            <ta e="T26" id="Seg_597" s="T25">god.[NOM]</ta>
            <ta e="T27" id="Seg_598" s="T26">then</ta>
            <ta e="T28" id="Seg_599" s="T27">understand-PST.NAR-3SG.O</ta>
            <ta e="T29" id="Seg_600" s="T28">what</ta>
            <ta e="T30" id="Seg_601" s="T29">(s)he-EP.[NOM]-PL</ta>
            <ta e="T31" id="Seg_602" s="T30">up</ta>
            <ta e="T32" id="Seg_603" s="T31">get.to-FUT-3PL</ta>
            <ta e="T33" id="Seg_604" s="T32">what.[NOM]</ta>
            <ta e="T34" id="Seg_605" s="T33">do-INF</ta>
            <ta e="T35" id="Seg_606" s="T34">all</ta>
            <ta e="T36" id="Seg_607" s="T35">word-ACC-3PL</ta>
            <ta e="T37" id="Seg_608" s="T36">change-INF</ta>
            <ta e="T38" id="Seg_609" s="T37">one.should</ta>
            <ta e="T39" id="Seg_610" s="T38">all</ta>
            <ta e="T40" id="Seg_611" s="T39">word-ACC-3PL</ta>
            <ta e="T41" id="Seg_612" s="T40">change-CO-3PL</ta>
            <ta e="T42" id="Seg_613" s="T41">all</ta>
            <ta e="T43" id="Seg_614" s="T42">confuse-3PL</ta>
            <ta e="T44" id="Seg_615" s="T43">who.[NOM]-EMPH</ta>
            <ta e="T45" id="Seg_616" s="T44">how-EMPH</ta>
            <ta e="T47" id="Seg_617" s="T45">say-RES.[3SG.S]</ta>
            <ta e="T48" id="Seg_618" s="T47">other</ta>
            <ta e="T49" id="Seg_619" s="T48">in</ta>
            <ta e="T50" id="Seg_620" s="T49">so-LOC</ta>
            <ta e="T51" id="Seg_621" s="T50">what-ACC-EMPH</ta>
            <ta e="T52" id="Seg_622" s="T51">human.being.[NOM]-PL</ta>
            <ta e="T53" id="Seg_623" s="T52">NEG</ta>
            <ta e="T54" id="Seg_624" s="T53">know-FUT-2PL</ta>
            <ta e="T68" id="Seg_625" s="T54">this</ta>
            <ta e="T55" id="Seg_626" s="T68">%%</ta>
            <ta e="T56" id="Seg_627" s="T55">away</ta>
            <ta e="T57" id="Seg_628" s="T56">go-3PL</ta>
            <ta e="T58" id="Seg_629" s="T57">simply</ta>
            <ta e="T59" id="Seg_630" s="T58">live-2PL</ta>
            <ta e="T60" id="Seg_631" s="T59">%%-ADVZ</ta>
            <ta e="T61" id="Seg_632" s="T60">%%</ta>
            <ta e="T62" id="Seg_633" s="T61">language.[NOM]</ta>
            <ta e="T63" id="Seg_634" s="T62">do-PST.NAR-INFER-3SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_635" s="T0">сказка.[NOM]</ta>
            <ta e="T2" id="Seg_636" s="T1">слово.[NOM]</ta>
            <ta e="T65" id="Seg_637" s="T2">про</ta>
            <ta e="T4" id="Seg_638" s="T3">раньше</ta>
            <ta e="T5" id="Seg_639" s="T4">человек-PL-ADES</ta>
            <ta e="T6" id="Seg_640" s="T5">один</ta>
            <ta e="T7" id="Seg_641" s="T6">слово.[NOM]</ta>
            <ta e="T8" id="Seg_642" s="T7">быть-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T9" id="Seg_643" s="T8">потом</ta>
            <ta e="T10" id="Seg_644" s="T9">он(а)-EP-PL</ta>
            <ta e="T11" id="Seg_645" s="T10">много</ta>
            <ta e="T12" id="Seg_646" s="T11">человек.[NOM]-PL</ta>
            <ta e="T13" id="Seg_647" s="T12">собрать-RES-PST.NAR-3PL</ta>
            <ta e="T14" id="Seg_648" s="T13">бог-ALL</ta>
            <ta e="T15" id="Seg_649" s="T14">вверх</ta>
            <ta e="T67" id="Seg_650" s="T15">добраться-INF</ta>
            <ta e="T17" id="Seg_651" s="T16">дерево-ACC</ta>
            <ta e="T18" id="Seg_652" s="T17">тот</ta>
            <ta e="T19" id="Seg_653" s="T18">вниз</ta>
            <ta e="T66" id="Seg_654" s="T19">пойти-IPFV-PST.NAR-3PL</ta>
            <ta e="T21" id="Seg_655" s="T20">бог-ALL</ta>
            <ta e="T22" id="Seg_656" s="T21">вверх</ta>
            <ta e="T23" id="Seg_657" s="T22">добраться-INF</ta>
            <ta e="T24" id="Seg_658" s="T23">делать-IPFV2-PST.NAR-3PL</ta>
            <ta e="T25" id="Seg_659" s="T24">делать-IPFV2-PST.NAR-3PL</ta>
            <ta e="T26" id="Seg_660" s="T25">бог.[NOM]</ta>
            <ta e="T27" id="Seg_661" s="T26">потом</ta>
            <ta e="T28" id="Seg_662" s="T27">понять-PST.NAR-3SG.O</ta>
            <ta e="T29" id="Seg_663" s="T28">что</ta>
            <ta e="T30" id="Seg_664" s="T29">он(а)-EP.[NOM]-PL</ta>
            <ta e="T31" id="Seg_665" s="T30">вверх</ta>
            <ta e="T32" id="Seg_666" s="T31">добраться-FUT-3PL</ta>
            <ta e="T33" id="Seg_667" s="T32">что.[NOM]</ta>
            <ta e="T34" id="Seg_668" s="T33">делать-INF</ta>
            <ta e="T35" id="Seg_669" s="T34">весь</ta>
            <ta e="T36" id="Seg_670" s="T35">слово-ACC-3PL</ta>
            <ta e="T37" id="Seg_671" s="T36">поменять-INF</ta>
            <ta e="T38" id="Seg_672" s="T37">надо</ta>
            <ta e="T39" id="Seg_673" s="T38">весь</ta>
            <ta e="T40" id="Seg_674" s="T39">слово-ACC-3PL</ta>
            <ta e="T41" id="Seg_675" s="T40">поменять-CO-3PL</ta>
            <ta e="T42" id="Seg_676" s="T41">весь</ta>
            <ta e="T43" id="Seg_677" s="T42">запутать-3PL</ta>
            <ta e="T44" id="Seg_678" s="T43">кто.[NOM]-EMPH</ta>
            <ta e="T45" id="Seg_679" s="T44">как-EMPH</ta>
            <ta e="T47" id="Seg_680" s="T45">сказать-RES.[3SG.S]</ta>
            <ta e="T48" id="Seg_681" s="T47">другой</ta>
            <ta e="T49" id="Seg_682" s="T48">по</ta>
            <ta e="T50" id="Seg_683" s="T49">так-LOC</ta>
            <ta e="T51" id="Seg_684" s="T50">что-ACC-EMPH</ta>
            <ta e="T52" id="Seg_685" s="T51">человек.[NOM]-PL</ta>
            <ta e="T53" id="Seg_686" s="T52">NEG</ta>
            <ta e="T54" id="Seg_687" s="T53">знать-FUT-2PL</ta>
            <ta e="T68" id="Seg_688" s="T54">этот</ta>
            <ta e="T55" id="Seg_689" s="T68">%%</ta>
            <ta e="T56" id="Seg_690" s="T55">прочь</ta>
            <ta e="T57" id="Seg_691" s="T56">бросать-3PL</ta>
            <ta e="T58" id="Seg_692" s="T57">просто</ta>
            <ta e="T59" id="Seg_693" s="T58">жить-2PL</ta>
            <ta e="T60" id="Seg_694" s="T59">%%-ADVZ</ta>
            <ta e="T61" id="Seg_695" s="T60">%%</ta>
            <ta e="T62" id="Seg_696" s="T61">язык.[NOM]</ta>
            <ta e="T63" id="Seg_697" s="T62">делать-PST.NAR-INFER-3SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_698" s="T0">n-n:case</ta>
            <ta e="T2" id="Seg_699" s="T1">n-n:case</ta>
            <ta e="T65" id="Seg_700" s="T2">pp</ta>
            <ta e="T4" id="Seg_701" s="T3">adv</ta>
            <ta e="T5" id="Seg_702" s="T4">n-n:num-n:case</ta>
            <ta e="T6" id="Seg_703" s="T5">num</ta>
            <ta e="T7" id="Seg_704" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_705" s="T7">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_706" s="T8">adv</ta>
            <ta e="T10" id="Seg_707" s="T9">pers-n:ins-n:num</ta>
            <ta e="T11" id="Seg_708" s="T10">quant</ta>
            <ta e="T12" id="Seg_709" s="T11">n-n:case-n:num</ta>
            <ta e="T13" id="Seg_710" s="T12">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_711" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_712" s="T14">preverb</ta>
            <ta e="T67" id="Seg_713" s="T15">v-v:inf</ta>
            <ta e="T17" id="Seg_714" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_715" s="T17">dem</ta>
            <ta e="T19" id="Seg_716" s="T18">preverb</ta>
            <ta e="T66" id="Seg_717" s="T19">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T21" id="Seg_718" s="T20">n-n:case</ta>
            <ta e="T22" id="Seg_719" s="T21">preverb</ta>
            <ta e="T23" id="Seg_720" s="T22">v-v:inf</ta>
            <ta e="T24" id="Seg_721" s="T23">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_722" s="T24">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T26" id="Seg_723" s="T25">n-n:case</ta>
            <ta e="T27" id="Seg_724" s="T26">adv</ta>
            <ta e="T28" id="Seg_725" s="T27">v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_726" s="T28">conj</ta>
            <ta e="T30" id="Seg_727" s="T29">pers-n:ins-n:case-n:num</ta>
            <ta e="T31" id="Seg_728" s="T30">preverb</ta>
            <ta e="T32" id="Seg_729" s="T31">v-v:tense-v:pn</ta>
            <ta e="T33" id="Seg_730" s="T32">interrog-n:case</ta>
            <ta e="T34" id="Seg_731" s="T33">v-v:inf</ta>
            <ta e="T35" id="Seg_732" s="T34">quant</ta>
            <ta e="T36" id="Seg_733" s="T35">n-n:case-n:poss</ta>
            <ta e="T37" id="Seg_734" s="T36">v-v:inf</ta>
            <ta e="T38" id="Seg_735" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_736" s="T38">quant</ta>
            <ta e="T40" id="Seg_737" s="T39">n-n:case-n:poss</ta>
            <ta e="T41" id="Seg_738" s="T40">v-v:ins-v:pn</ta>
            <ta e="T42" id="Seg_739" s="T41">quant</ta>
            <ta e="T43" id="Seg_740" s="T42">v-v:pn</ta>
            <ta e="T44" id="Seg_741" s="T43">interrog-n:case-clit</ta>
            <ta e="T45" id="Seg_742" s="T44">interrog-clit</ta>
            <ta e="T47" id="Seg_743" s="T45">v-v&gt;v-v:pn</ta>
            <ta e="T48" id="Seg_744" s="T47">adj</ta>
            <ta e="T49" id="Seg_745" s="T48">pp</ta>
            <ta e="T50" id="Seg_746" s="T49">adv-n:case</ta>
            <ta e="T51" id="Seg_747" s="T50">interrog-n:case-clit</ta>
            <ta e="T52" id="Seg_748" s="T51">n-n:case-n:num</ta>
            <ta e="T53" id="Seg_749" s="T52">ptcl</ta>
            <ta e="T54" id="Seg_750" s="T53">v-v:tense-v:pn</ta>
            <ta e="T68" id="Seg_751" s="T54">dem</ta>
            <ta e="T55" id="Seg_752" s="T68">%%</ta>
            <ta e="T56" id="Seg_753" s="T55">preverb</ta>
            <ta e="T57" id="Seg_754" s="T56">v-v:pn</ta>
            <ta e="T58" id="Seg_755" s="T57">adv</ta>
            <ta e="T59" id="Seg_756" s="T58">v-v:pn</ta>
            <ta e="T60" id="Seg_757" s="T59">%%-%%&gt;adv</ta>
            <ta e="T61" id="Seg_758" s="T60">%%</ta>
            <ta e="T62" id="Seg_759" s="T61">n-n:case</ta>
            <ta e="T63" id="Seg_760" s="T62">v-v:tense-v:mood-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_761" s="T0">n</ta>
            <ta e="T2" id="Seg_762" s="T1">n</ta>
            <ta e="T65" id="Seg_763" s="T2">pp</ta>
            <ta e="T4" id="Seg_764" s="T3">adv</ta>
            <ta e="T5" id="Seg_765" s="T4">n</ta>
            <ta e="T6" id="Seg_766" s="T5">num</ta>
            <ta e="T7" id="Seg_767" s="T6">n</ta>
            <ta e="T8" id="Seg_768" s="T7">v</ta>
            <ta e="T9" id="Seg_769" s="T8">adv</ta>
            <ta e="T10" id="Seg_770" s="T9">pers</ta>
            <ta e="T11" id="Seg_771" s="T10">quant</ta>
            <ta e="T12" id="Seg_772" s="T11">n</ta>
            <ta e="T13" id="Seg_773" s="T12">v</ta>
            <ta e="T14" id="Seg_774" s="T13">n</ta>
            <ta e="T15" id="Seg_775" s="T14">preverb</ta>
            <ta e="T67" id="Seg_776" s="T15">v</ta>
            <ta e="T17" id="Seg_777" s="T16">n</ta>
            <ta e="T18" id="Seg_778" s="T17">dem</ta>
            <ta e="T19" id="Seg_779" s="T18">preverb</ta>
            <ta e="T66" id="Seg_780" s="T19">v</ta>
            <ta e="T21" id="Seg_781" s="T20">n</ta>
            <ta e="T22" id="Seg_782" s="T21">preverb</ta>
            <ta e="T23" id="Seg_783" s="T22">v</ta>
            <ta e="T24" id="Seg_784" s="T23">v</ta>
            <ta e="T25" id="Seg_785" s="T24">v</ta>
            <ta e="T26" id="Seg_786" s="T25">n</ta>
            <ta e="T27" id="Seg_787" s="T26">adv</ta>
            <ta e="T28" id="Seg_788" s="T27">v</ta>
            <ta e="T29" id="Seg_789" s="T28">conj</ta>
            <ta e="T30" id="Seg_790" s="T29">n</ta>
            <ta e="T31" id="Seg_791" s="T30">preverb</ta>
            <ta e="T32" id="Seg_792" s="T31">v</ta>
            <ta e="T33" id="Seg_793" s="T32">interrog</ta>
            <ta e="T34" id="Seg_794" s="T33">v</ta>
            <ta e="T35" id="Seg_795" s="T34">quant</ta>
            <ta e="T36" id="Seg_796" s="T35">n</ta>
            <ta e="T37" id="Seg_797" s="T36">v</ta>
            <ta e="T38" id="Seg_798" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_799" s="T38">quant</ta>
            <ta e="T40" id="Seg_800" s="T39">n</ta>
            <ta e="T41" id="Seg_801" s="T40">v</ta>
            <ta e="T42" id="Seg_802" s="T41">quant</ta>
            <ta e="T43" id="Seg_803" s="T42">v</ta>
            <ta e="T44" id="Seg_804" s="T43">pro</ta>
            <ta e="T45" id="Seg_805" s="T44">adv</ta>
            <ta e="T47" id="Seg_806" s="T45">v</ta>
            <ta e="T48" id="Seg_807" s="T47">adj</ta>
            <ta e="T49" id="Seg_808" s="T48">pp</ta>
            <ta e="T50" id="Seg_809" s="T49">adv</ta>
            <ta e="T51" id="Seg_810" s="T50">pro</ta>
            <ta e="T52" id="Seg_811" s="T51">n</ta>
            <ta e="T53" id="Seg_812" s="T52">ptcl</ta>
            <ta e="T54" id="Seg_813" s="T53">v</ta>
            <ta e="T68" id="Seg_814" s="T54">pro</ta>
            <ta e="T55" id="Seg_815" s="T68">%%</ta>
            <ta e="T56" id="Seg_816" s="T55">preverb</ta>
            <ta e="T57" id="Seg_817" s="T56">v</ta>
            <ta e="T58" id="Seg_818" s="T57">adv</ta>
            <ta e="T59" id="Seg_819" s="T58">v</ta>
            <ta e="T60" id="Seg_820" s="T59">adv</ta>
            <ta e="T61" id="Seg_821" s="T60">%%</ta>
            <ta e="T62" id="Seg_822" s="T61">n</ta>
            <ta e="T63" id="Seg_823" s="T62">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T7" id="Seg_824" s="T6">np:S</ta>
            <ta e="T8" id="Seg_825" s="T7">v:pred</ta>
            <ta e="T10" id="Seg_826" s="T9">pro.h:S</ta>
            <ta e="T12" id="Seg_827" s="T11">np.h:S</ta>
            <ta e="T13" id="Seg_828" s="T12">v:pred</ta>
            <ta e="T67" id="Seg_829" s="T13">s:purp</ta>
            <ta e="T17" id="Seg_830" s="T16">np:O</ta>
            <ta e="T66" id="Seg_831" s="T19">0.3.h:S v:pred</ta>
            <ta e="T23" id="Seg_832" s="T20">s:purp</ta>
            <ta e="T24" id="Seg_833" s="T23">0.3.h:S v:pred</ta>
            <ta e="T25" id="Seg_834" s="T24">0.3.h:S v:pred</ta>
            <ta e="T26" id="Seg_835" s="T25">np:S</ta>
            <ta e="T28" id="Seg_836" s="T27">v:pred</ta>
            <ta e="T32" id="Seg_837" s="T28">s:compl</ta>
            <ta e="T37" id="Seg_838" s="T36">v:O</ta>
            <ta e="T38" id="Seg_839" s="T37">ptcl:pred</ta>
            <ta e="T40" id="Seg_840" s="T39">np:O</ta>
            <ta e="T41" id="Seg_841" s="T40">0.3.h:S v:pred</ta>
            <ta e="T43" id="Seg_842" s="T42">0.3.h:S v:pred</ta>
            <ta e="T44" id="Seg_843" s="T43">pro.h:S</ta>
            <ta e="T47" id="Seg_844" s="T45">v:pred</ta>
            <ta e="T51" id="Seg_845" s="T50">pro:O</ta>
            <ta e="T52" id="Seg_846" s="T51">np.h:S</ta>
            <ta e="T54" id="Seg_847" s="T53">v:pred</ta>
            <ta e="T57" id="Seg_848" s="T56">0.3.h:S v:pred</ta>
            <ta e="T59" id="Seg_849" s="T58">0.3.h:S v:pred</ta>
            <ta e="T62" id="Seg_850" s="T61">np:O</ta>
            <ta e="T63" id="Seg_851" s="T62">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T4" id="Seg_852" s="T3">adv:Time</ta>
            <ta e="T5" id="Seg_853" s="T4">np.h:Poss</ta>
            <ta e="T7" id="Seg_854" s="T6">np:Th</ta>
            <ta e="T9" id="Seg_855" s="T8">adv:Time</ta>
            <ta e="T10" id="Seg_856" s="T9">pro.h:A</ta>
            <ta e="T12" id="Seg_857" s="T11">np.h:A</ta>
            <ta e="T14" id="Seg_858" s="T13">np:G</ta>
            <ta e="T17" id="Seg_859" s="T16">np:P</ta>
            <ta e="T66" id="Seg_860" s="T19">0.3.h:A</ta>
            <ta e="T21" id="Seg_861" s="T20">np:G</ta>
            <ta e="T24" id="Seg_862" s="T23">0.3.h:A</ta>
            <ta e="T25" id="Seg_863" s="T24">0.3.h:A</ta>
            <ta e="T26" id="Seg_864" s="T25">np:E</ta>
            <ta e="T27" id="Seg_865" s="T26">adv:Time</ta>
            <ta e="T30" id="Seg_866" s="T29">pro.h:A</ta>
            <ta e="T37" id="Seg_867" s="T36">v:Th</ta>
            <ta e="T40" id="Seg_868" s="T39">np:Th</ta>
            <ta e="T41" id="Seg_869" s="T40">0.3.h:A</ta>
            <ta e="T43" id="Seg_870" s="T42">0.3.h:E</ta>
            <ta e="T44" id="Seg_871" s="T43">pro.h:A</ta>
            <ta e="T50" id="Seg_872" s="T49">adv:Time</ta>
            <ta e="T51" id="Seg_873" s="T50">pro:Th</ta>
            <ta e="T52" id="Seg_874" s="T51">np.h:E</ta>
            <ta e="T57" id="Seg_875" s="T56">0.3.h:A</ta>
            <ta e="T59" id="Seg_876" s="T58">0.3.h:Th</ta>
            <ta e="T62" id="Seg_877" s="T61">np:Th</ta>
            <ta e="T63" id="Seg_878" s="T62">0.3.h:A</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T9" id="Seg_879" s="T8">RUS:core</ta>
            <ta e="T27" id="Seg_880" s="T26">RUS:core</ta>
            <ta e="T29" id="Seg_881" s="T28">RUS:gram</ta>
            <ta e="T35" id="Seg_882" s="T34">RUS:core</ta>
            <ta e="T38" id="Seg_883" s="T37">RUS:mod</ta>
            <ta e="T39" id="Seg_884" s="T38">RUS:core</ta>
            <ta e="T42" id="Seg_885" s="T41">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T65" id="Seg_886" s="T0">Сказка про слово.</ta>
            <ta e="T8" id="Seg_887" s="T3">Раньше у людей одно слово было.</ta>
            <ta e="T67" id="Seg_888" s="T8">Потом они, много людей, собрались — до Бога наверх добраться.</ta>
            <ta e="T23" id="Seg_889" s="T67">(?) Дерево (вниз?) надставили (?) — до Бога вверх добраться.</ta>
            <ta e="T25" id="Seg_890" s="T23">Делают, делают.</ta>
            <ta e="T32" id="Seg_891" s="T25">Бог потом понял, что они наверх доберутся.</ta>
            <ta e="T34" id="Seg_892" s="T32">Что делать?</ta>
            <ta e="T38" id="Seg_893" s="T34">Все слова поменять надо.</ta>
            <ta e="T41" id="Seg_894" s="T38">Все слова поменяли.</ta>
            <ta e="T43" id="Seg_895" s="T41">Всё перепутали.</ta>
            <ta e="T49" id="Seg_896" s="T43">Кто как стали разговаривать, на другом языке.</ta>
            <ta e="T54" id="Seg_897" s="T49">Тогда ничего вы, люди, не узнаете.</ta>
            <ta e="T59" id="Seg_898" s="T54">С того места на север идут, просто живут.</ta>
            <ta e="T63" id="Seg_899" s="T59">(В конце концов?) язык сделал [бог].</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T65" id="Seg_900" s="T0">A story about a word.</ta>
            <ta e="T8" id="Seg_901" s="T3">In earlier times people had only one word.</ta>
            <ta e="T67" id="Seg_902" s="T8">Then they, a lot of people, got together – they wanted to go up and reach God.</ta>
            <ta e="T23" id="Seg_903" s="T67">(?) They set up a tree (?) – to go up and reach God.</ta>
            <ta e="T25" id="Seg_904" s="T23">They worked and worked.</ta>
            <ta e="T32" id="Seg_905" s="T25">God then understood that they would reach Him up high.</ta>
            <ta e="T34" id="Seg_906" s="T32">What to do?</ta>
            <ta e="T38" id="Seg_907" s="T34">All the words need to be changed.</ta>
            <ta e="T41" id="Seg_908" s="T38">They changed all the words.</ta>
            <ta e="T43" id="Seg_909" s="T41">They mixed up everything.</ta>
            <ta e="T49" id="Seg_910" s="T43">Nobody ever spoke another language in any way. [?]</ta>
            <ta e="T54" id="Seg_911" s="T49">Then you, people, will not know anything.</ta>
            <ta e="T59" id="Seg_912" s="T54">From that place they went north, just living.</ta>
            <ta e="T63" id="Seg_913" s="T59">(Finally?) [the God] made the language.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T65" id="Seg_914" s="T0">Eine Geschichte über ein Wort.</ta>
            <ta e="T8" id="Seg_915" s="T3">In früheren Zeiten hatten Menschen nur ein Wort.</ta>
            <ta e="T67" id="Seg_916" s="T8">Dann kamen sie, eine Menge Menschen, zusammen – sie wollten hinauf gehen und zu Gott gelangen.</ta>
            <ta e="T23" id="Seg_917" s="T67">(?) Sie richteten einen Baum auf (?) – um hinauf zu gehen und zu Gott zu gelangen.</ta>
            <ta e="T25" id="Seg_918" s="T23">Sie arbeiteten und arbeiteten.</ta>
            <ta e="T32" id="Seg_919" s="T25">Gott verstand dann, dass sie zu ihm nach oben gelangen würden.</ta>
            <ta e="T34" id="Seg_920" s="T32">Was machen?</ta>
            <ta e="T38" id="Seg_921" s="T34">Alle Wörter müssen geändert werden.</ta>
            <ta e="T41" id="Seg_922" s="T38">Sie änderten alle Wörter.</ta>
            <ta e="T43" id="Seg_923" s="T41">Sie vermischten alles.</ta>
            <ta e="T49" id="Seg_924" s="T43">Keiner sprach mehr ein andere Sprache auf keiner Weise. [?]</ta>
            <ta e="T54" id="Seg_925" s="T49">Dann werdet ihr, Leute, nichts mehr wissen.</ta>
            <ta e="T59" id="Seg_926" s="T54">Von dem Ort gingen sie nach Norden, lebten nur.</ta>
            <ta e="T63" id="Seg_927" s="T59">(Letztendlich?) hat [der Gott] die Sprache gemacht.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T65" id="Seg_928" s="T0">Сказка про слово.</ta>
            <ta e="T8" id="Seg_929" s="T3">Раньше у людей одно слово было.</ta>
            <ta e="T67" id="Seg_930" s="T8">Потом они, много людей, собрались — до Бога наверх добраться.</ta>
            <ta e="T23" id="Seg_931" s="T67">Дерево вниз надставили — до Бога вверх добраться.</ta>
            <ta e="T25" id="Seg_932" s="T23">Делают, делают.</ta>
            <ta e="T32" id="Seg_933" s="T25">Бог потом понял, что они наверх доберутся.</ta>
            <ta e="T34" id="Seg_934" s="T32">Что делать?</ta>
            <ta e="T38" id="Seg_935" s="T34">Все слова поменять надо.</ta>
            <ta e="T41" id="Seg_936" s="T38">Все слова поменяли.</ta>
            <ta e="T43" id="Seg_937" s="T41">Всё забыли.</ta>
            <ta e="T49" id="Seg_938" s="T43">Никто никак не разговаривал на другом языке.</ta>
            <ta e="T54" id="Seg_939" s="T49">Тогда ничего люди не узнают.</ta>
            <ta e="T59" id="Seg_940" s="T54">С того места на север идут, просто живут.</ta>
            <ta e="T63" id="Seg_941" s="T59">Так красивым язык сделал.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T65" id="Seg_942" s="T0">[WNB:] Word means here 'language'.</ta>
            <ta e="T23" id="Seg_943" s="T67">[AAV:] Pop stroi-le [build-CVB] ?</ta>
            <ta e="T49" id="Seg_944" s="T43">[AAV:] negative aː absent?</ta>
            <ta e="T63" id="Seg_945" s="T59">[AAV:] Elʼǯik qwelʼǯʼət ?</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T65" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T67" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T66" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T68" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
