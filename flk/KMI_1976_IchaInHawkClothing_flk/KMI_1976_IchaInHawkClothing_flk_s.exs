<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KMI_1976_IchaInHawkClothing_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KMI_1976_IchaInHawkClothing_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">483</ud-information>
            <ud-information attribute-name="# HIAT:w">351</ud-information>
            <ud-information attribute-name="# e">351</ud-information>
            <ud-information attribute-name="# HIAT:u">63</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KMI">
            <abbreviation>KMI</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
         <tli id="T204" />
         <tli id="T205" />
         <tli id="T206" />
         <tli id="T207" />
         <tli id="T350" />
         <tli id="T208" />
         <tli id="T209" />
         <tli id="T210" />
         <tli id="T211" />
         <tli id="T212" />
         <tli id="T351" />
         <tli id="T213" />
         <tli id="T214" />
         <tli id="T215" />
         <tli id="T216" />
         <tli id="T217" />
         <tli id="T218" />
         <tli id="T219" />
         <tli id="T220" />
         <tli id="T221" />
         <tli id="T222" />
         <tli id="T223" />
         <tli id="T224" />
         <tli id="T225" />
         <tli id="T226" />
         <tli id="T227" />
         <tli id="T228" />
         <tli id="T229" />
         <tli id="T230" />
         <tli id="T231" />
         <tli id="T232" />
         <tli id="T233" />
         <tli id="T234" />
         <tli id="T235" />
         <tli id="T236" />
         <tli id="T237" />
         <tli id="T238" />
         <tli id="T239" />
         <tli id="T240" />
         <tli id="T241" />
         <tli id="T242" />
         <tli id="T243" />
         <tli id="T244" />
         <tli id="T245" />
         <tli id="T246" />
         <tli id="T247" />
         <tli id="T248" />
         <tli id="T249" />
         <tli id="T250" />
         <tli id="T251" />
         <tli id="T252" />
         <tli id="T253" />
         <tli id="T254" />
         <tli id="T255" />
         <tli id="T256" />
         <tli id="T257" />
         <tli id="T258" />
         <tli id="T259" />
         <tli id="T260" />
         <tli id="T261" />
         <tli id="T262" />
         <tli id="T263" />
         <tli id="T264" />
         <tli id="T265" />
         <tli id="T266" />
         <tli id="T267" />
         <tli id="T268" />
         <tli id="T269" />
         <tli id="T270" />
         <tli id="T271" />
         <tli id="T272" />
         <tli id="T273" />
         <tli id="T274" />
         <tli id="T275" />
         <tli id="T276" />
         <tli id="T277" />
         <tli id="T278" />
         <tli id="T279" />
         <tli id="T280" />
         <tli id="T281" />
         <tli id="T282" />
         <tli id="T283" />
         <tli id="T284" />
         <tli id="T285" />
         <tli id="T286" />
         <tli id="T287" />
         <tli id="T288" />
         <tli id="T289" />
         <tli id="T290" />
         <tli id="T291" />
         <tli id="T292" />
         <tli id="T293" />
         <tli id="T294" />
         <tli id="T295" />
         <tli id="T296" />
         <tli id="T297" />
         <tli id="T298" />
         <tli id="T299" />
         <tli id="T300" />
         <tli id="T301" />
         <tli id="T302" />
         <tli id="T303" />
         <tli id="T304" />
         <tli id="T305" />
         <tli id="T306" />
         <tli id="T307" />
         <tli id="T308" />
         <tli id="T309" />
         <tli id="T310" />
         <tli id="T311" />
         <tli id="T312" />
         <tli id="T313" />
         <tli id="T314" />
         <tli id="T315" />
         <tli id="T316" />
         <tli id="T317" />
         <tli id="T318" />
         <tli id="T319" />
         <tli id="T320" />
         <tli id="T321" />
         <tli id="T322" />
         <tli id="T323" />
         <tli id="T324" />
         <tli id="T325" />
         <tli id="T326" />
         <tli id="T327" />
         <tli id="T328" />
         <tli id="T329" />
         <tli id="T330" />
         <tli id="T331" />
         <tli id="T332" />
         <tli id="T333" />
         <tli id="T334" />
         <tli id="T335" />
         <tli id="T336" />
         <tli id="T337" />
         <tli id="T338" />
         <tli id="T339" />
         <tli id="T340" />
         <tli id="T341" />
         <tli id="T342" />
         <tli id="T343" />
         <tli id="T344" />
         <tli id="T345" />
         <tli id="T346" />
         <tli id="T347" />
         <tli id="T348" />
         <tli id="T349" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KMI"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T349" id="Seg_0" n="sc" s="T0">
               <ts e="T1" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Ičʼakiːčʼika</ts>
                  <nts id="Seg_5" n="HIAT:ip">.</nts>
                  <nts id="Seg_6" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T4" id="Seg_8" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_10" n="HIAT:w" s="T1">Ilɨmpa</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_13" n="HIAT:w" s="T2">Ičʼakʼiːčʼika</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">imlʼantɨsa</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_20" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_22" n="HIAT:w" s="T4">Ičʼakiːčʼik</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_25" n="HIAT:w" s="T5">qänna</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">mačʼontɨ</ts>
                  <nts id="Seg_29" n="HIAT:ip">,</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_32" n="HIAT:w" s="T7">qoŋɨtɨ</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_35" n="HIAT:w" s="T8">qup</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_38" n="HIAT:w" s="T9">pačʼetta</ts>
                  <nts id="Seg_39" n="HIAT:ip">.</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_42" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_44" n="HIAT:w" s="T10">Tüŋa</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_47" n="HIAT:w" s="T11">tättɨčʼaktɨ</ts>
                  <nts id="Seg_48" n="HIAT:ip">,</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_51" n="HIAT:w" s="T12">qoŋɨtɨ</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_54" n="HIAT:w" s="T13">märɨqa</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_57" n="HIAT:w" s="T14">loːs</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_59" n="HIAT:ip">(</nts>
                  <nts id="Seg_60" n="HIAT:ip">/</nts>
                  <ts e="T16" id="Seg_62" n="HIAT:w" s="T15">loːsɨlʼ</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_65" n="HIAT:w" s="T16">qup</ts>
                  <nts id="Seg_66" n="HIAT:ip">)</nts>
                  <nts id="Seg_67" n="HIAT:ip">.</nts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_70" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_72" n="HIAT:w" s="T17">Ilʼčʼa</ts>
                  <nts id="Seg_73" n="HIAT:ip">,</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_76" n="HIAT:w" s="T18">qoj</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_79" n="HIAT:w" s="T19">meːttal</ts>
                  <nts id="Seg_80" n="HIAT:ip">?</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_83" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_85" n="HIAT:w" s="T20">Ma</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_88" n="HIAT:w" s="T21">poːp</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_91" n="HIAT:w" s="T22">pačʼertap</ts>
                  <nts id="Seg_92" n="HIAT:ip">–</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_95" n="HIAT:w" s="T23">loːsɨ</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_98" n="HIAT:w" s="T24">kättɨtɨ</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_100" n="HIAT:ip">(</nts>
                  <nts id="Seg_101" n="HIAT:ip">/</nts>
                  <ts e="T26" id="Seg_103" n="HIAT:w" s="T25">kättɨsɨtɨ</ts>
                  <nts id="Seg_104" n="HIAT:ip">)</nts>
                  <nts id="Seg_105" n="HIAT:ip">.</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_108" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_110" n="HIAT:w" s="T26">Ma</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_113" n="HIAT:w" s="T27">okoːt</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_116" n="HIAT:w" s="T28">tap</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_119" n="HIAT:w" s="T29">poːp</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_122" n="HIAT:w" s="T30">utsä</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_125" n="HIAT:w" s="T31">šitɨ</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_128" n="HIAT:w" s="T32">nɨtqolʼlʼɛːkkak</ts>
                  <nts id="Seg_129" n="HIAT:ip">.</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_132" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_134" n="HIAT:w" s="T33">Kusa</ts>
                  <nts id="Seg_135" n="HIAT:ip">,</nts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_138" n="HIAT:w" s="T34">kätsan</ts>
                  <nts id="Seg_139" n="HIAT:ip">,</nts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_142" n="HIAT:w" s="T35">šitɨ</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_145" n="HIAT:w" s="T36">nɨtqalʼät</ts>
                  <nts id="Seg_146" n="HIAT:ip">,</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_149" n="HIAT:w" s="T37">kusa</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_152" n="HIAT:w" s="T38">saŋatɨ</ts>
                  <nts id="Seg_153" n="HIAT:ip">!</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_156" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_158" n="HIAT:w" s="T39">Loːsɨlʼ</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_161" n="HIAT:w" s="T40">ilʼčʼat</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_164" n="HIAT:w" s="T41">poːp</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_167" n="HIAT:w" s="T42">šitɨ</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_170" n="HIAT:w" s="T43">nɨtqɨlnɨtɨ</ts>
                  <nts id="Seg_171" n="HIAT:ip">.</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_174" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_176" n="HIAT:w" s="T44">Kusa</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_179" n="HIAT:w" s="T45">utol</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_182" n="HIAT:w" s="T46">saqaltɨ</ts>
                  <nts id="Seg_183" n="HIAT:ip">,</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_186" n="HIAT:w" s="T47">šitɨ</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_189" n="HIAT:w" s="T48">utol</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_192" n="HIAT:w" s="T49">tokalʼtätɨ</ts>
                  <nts id="Seg_193" n="HIAT:ip">,</nts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_196" n="HIAT:w" s="T50">naqqəltɨ</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_199" n="HIAT:w" s="T51">kɨsa</ts>
                  <nts id="Seg_200" n="HIAT:ip">!</nts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_203" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_205" n="HIAT:w" s="T52">Poː</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_208" n="HIAT:w" s="T53">orqɨlsɨt</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_211" n="HIAT:w" s="T54">utemtɨ</ts>
                  <nts id="Seg_212" n="HIAT:ip">,</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_215" n="HIAT:w" s="T55">šitɨ</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_218" n="HIAT:w" s="T56">utətɨ</ts>
                  <nts id="Seg_219" n="HIAT:ip">.</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_222" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_224" n="HIAT:w" s="T57">Täm</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_227" n="HIAT:w" s="T58">ɔːmta</ts>
                  <nts id="Seg_228" n="HIAT:ip">,</nts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_231" n="HIAT:w" s="T59">ortɨ</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_234" n="HIAT:w" s="T60">čʼäːŋka</ts>
                  <nts id="Seg_235" n="HIAT:ip">.</nts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_238" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_240" n="HIAT:w" s="T61">Loːsɨ</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_243" n="HIAT:w" s="T62">kätäsɨtɨ</ts>
                  <nts id="Seg_244" n="HIAT:ip">:</nts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_247" n="HIAT:w" s="T63">Ɔːmtasik</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_250" n="HIAT:w" s="T64">i</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_253" n="HIAT:w" s="T65">teːkäsik</ts>
                  <nts id="Seg_254" n="HIAT:ip">.</nts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_257" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_259" n="HIAT:w" s="T66">Loːsɨ</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_262" n="HIAT:w" s="T67">qəssɨ</ts>
                  <nts id="Seg_263" n="HIAT:ip">.</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_266" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_268" n="HIAT:w" s="T68">Ičʼakɛːčʼika</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_271" n="HIAT:w" s="T69">ɔːmtɨsa</ts>
                  <nts id="Seg_272" n="HIAT:ip">,</nts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_275" n="HIAT:w" s="T70">ɔːmtɨsa</ts>
                  <nts id="Seg_276" n="HIAT:ip">,</nts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_279" n="HIAT:w" s="T71">kuntɨ</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_282" n="HIAT:w" s="T72">čʼap</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_285" n="HIAT:w" s="T73">kuntɨ</ts>
                  <nts id="Seg_286" n="HIAT:ip">,</nts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_289" n="HIAT:w" s="T74">qäntɨk</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_292" n="HIAT:w" s="T75">čʼap</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_295" n="HIAT:w" s="T76">qäntɨk</ts>
                  <nts id="Seg_296" n="HIAT:ip">.</nts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_299" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_301" n="HIAT:w" s="T77">Tɛnɨrpɨsa</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_304" n="HIAT:w" s="T78">qäntɨk</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_307" n="HIAT:w" s="T79">tanteqo</ts>
                  <nts id="Seg_308" n="HIAT:ip">.</nts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_311" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_313" n="HIAT:w" s="T80">Qältɨrsa</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_316" n="HIAT:w" s="T81">i</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_319" n="HIAT:w" s="T82">kutarsa</ts>
                  <nts id="Seg_320" n="HIAT:ip">,</nts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_323" n="HIAT:w" s="T83">ɛːppa</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_326" n="HIAT:w" s="T84">qätɨtɨ</ts>
                  <nts id="Seg_327" n="HIAT:ip">.</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_330" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_332" n="HIAT:w" s="T85">Täpɨni</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_335" n="HIAT:w" s="T86">tüŋa</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_338" n="HIAT:w" s="T87">qorqə</ts>
                  <nts id="Seg_339" n="HIAT:ip">.</nts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_342" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_344" n="HIAT:w" s="T88">Ilʼčʼa</ts>
                  <nts id="Seg_345" n="HIAT:ip">,</nts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_348" n="HIAT:w" s="T89">poːp</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_351" n="HIAT:w" s="T90">sitɨ</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_354" n="HIAT:w" s="T91">nɨtqɨltɨ</ts>
                  <nts id="Seg_355" n="HIAT:ip">.</nts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_358" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_360" n="HIAT:w" s="T92">Qorqə</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_363" n="HIAT:w" s="T93">sitɨ</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_366" n="HIAT:w" s="T94">nɨtqɨlʼsitɨ</ts>
                  <nts id="Seg_367" n="HIAT:ip">,</nts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_370" n="HIAT:w" s="T95">utɨtɨ</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_373" n="HIAT:w" s="T96">tıːtesitɨ</ts>
                  <nts id="Seg_374" n="HIAT:ip">.</nts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_377" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_379" n="HIAT:w" s="T97">I</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_382" n="HIAT:w" s="T98">utətə</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_385" n="HIAT:w" s="T99">uːpɨl</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_388" n="HIAT:w" s="T100">alʼčʼasa</ts>
                  <nts id="Seg_389" n="HIAT:ip">.</nts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T104" id="Seg_392" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_394" n="HIAT:w" s="T101">Tanta</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_397" n="HIAT:w" s="T102">poː</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_400" n="HIAT:w" s="T103">nɔːnɨ</ts>
                  <nts id="Seg_401" n="HIAT:ip">.</nts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_404" n="HIAT:u" s="T104">
                  <ts e="T105" id="Seg_406" n="HIAT:w" s="T104">Kuttarna</ts>
                  <nts id="Seg_407" n="HIAT:ip">,</nts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_410" n="HIAT:w" s="T105">qälterna</ts>
                  <nts id="Seg_411" n="HIAT:ip">,</nts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_414" n="HIAT:w" s="T106">tɛnɨrpa</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_417" n="HIAT:w" s="T107">qäntɨk</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_420" n="HIAT:w" s="T108">mitɨqo</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_423" n="HIAT:w" s="T109">i</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_426" n="HIAT:w" s="T110">qonterqo</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_429" n="HIAT:w" s="T111">loːsɨp</ts>
                  <nts id="Seg_430" n="HIAT:ip">.</nts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T121" id="Seg_433" n="HIAT:u" s="T112">
                  <ts e="T113" id="Seg_435" n="HIAT:w" s="T112">Kuttarna</ts>
                  <nts id="Seg_436" n="HIAT:ip">,</nts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_439" n="HIAT:w" s="T113">qälimpa</ts>
                  <nts id="Seg_440" n="HIAT:ip">,</nts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_443" n="HIAT:w" s="T114">tɛnɨrpɨt</ts>
                  <nts id="Seg_444" n="HIAT:ip">,</nts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_447" n="HIAT:w" s="T115">kuttar</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_450" n="HIAT:w" s="T116">loːsɨp</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_453" n="HIAT:w" s="T117">mitäqo</ts>
                  <nts id="Seg_454" n="HIAT:ip">,</nts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_457" n="HIAT:w" s="T118">orqɨlqo</ts>
                  <nts id="Seg_458" n="HIAT:ip">,</nts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_461" n="HIAT:w" s="T119">orqɨllä</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_464" n="HIAT:w" s="T120">iːlela</ts>
                  <nts id="Seg_465" n="HIAT:ip">.</nts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_468" n="HIAT:u" s="T121">
                  <ts e="T122" id="Seg_470" n="HIAT:w" s="T121">Tɛnɨrpa</ts>
                  <nts id="Seg_471" n="HIAT:ip">,</nts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_474" n="HIAT:w" s="T122">kučʼä</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_477" n="HIAT:w" s="T123">qateqo</ts>
                  <nts id="Seg_478" n="HIAT:ip">.</nts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_481" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_483" n="HIAT:w" s="T124">Oloːqɨntɨ</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_486" n="HIAT:w" s="T125">alʼčʼa</ts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_489" n="HIAT:w" s="T126">tɛnɨtɨ</ts>
                  <nts id="Seg_490" n="HIAT:ip">:</nts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_493" n="HIAT:w" s="T127">Nɔːtna</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_496" n="HIAT:w" s="T128">karmanʼmɨ</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_499" n="HIAT:w" s="T129">mennɨmpɨqo</ts>
                  <nts id="Seg_500" n="HIAT:ip">,</nts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_503" n="HIAT:w" s="T130">korpɨtqo</ts>
                  <nts id="Seg_504" n="HIAT:ip">!</nts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T135" id="Seg_507" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_509" n="HIAT:w" s="T131">Karmanʼqɨntɨ</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_512" n="HIAT:w" s="T132">qosɨtɨ</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_515" n="HIAT:w" s="T133">mɨšʼaltukat</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_518" n="HIAT:w" s="T134">qopɨ</ts>
                  <nts id="Seg_519" n="HIAT:ip">.</nts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_522" n="HIAT:u" s="T135">
                  <ts e="T136" id="Seg_524" n="HIAT:w" s="T135">Nɨŋɨlʼä</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_527" n="HIAT:w" s="T136">panɨtqolapsɨt</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_530" n="HIAT:w" s="T137">mɨšʼaltukat</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_533" n="HIAT:w" s="T138">qopɨ</ts>
                  <nts id="Seg_534" n="HIAT:ip">.</nts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T142" id="Seg_537" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_539" n="HIAT:w" s="T139">Panɨsɨtɨ</ts>
                  <nts id="Seg_540" n="HIAT:ip">,</nts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_543" n="HIAT:w" s="T140">märqɨlɔː</ts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_546" n="HIAT:w" s="T141">ɛsɨsa</ts>
                  <nts id="Seg_547" n="HIAT:ip">.</nts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_550" n="HIAT:u" s="T142">
                  <ts e="T143" id="Seg_552" n="HIAT:w" s="T142">Tokaltistɨ</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_555" n="HIAT:w" s="T143">oloːqɨntɨ</ts>
                  <nts id="Seg_556" n="HIAT:ip">.</nts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_559" n="HIAT:u" s="T144">
                  <ts e="T145" id="Seg_561" n="HIAT:w" s="T144">Nɨːnɨ</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_564" n="HIAT:w" s="T145">qotä</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_567" n="HIAT:w" s="T146">toː</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_570" n="HIAT:w" s="T147">tıːtesɨtɨ</ts>
                  <nts id="Seg_571" n="HIAT:ip">.</nts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T150" id="Seg_574" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_576" n="HIAT:w" s="T148">Očʼik</ts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_579" n="HIAT:w" s="T149">panalsɨtɨ</ts>
                  <nts id="Seg_580" n="HIAT:ip">.</nts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T155" id="Seg_583" n="HIAT:u" s="T150">
                  <ts e="T151" id="Seg_585" n="HIAT:w" s="T150">Aj</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_588" n="HIAT:w" s="T151">očʼik</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_591" n="HIAT:w" s="T152">tokaltistɨ</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_594" n="HIAT:w" s="T153">oloːqɨntɨ</ts>
                  <nts id="Seg_595" n="HIAT:ip">,</nts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_598" n="HIAT:w" s="T154">kəpoːqɨntɨ</ts>
                  <nts id="Seg_599" n="HIAT:ip">.</nts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T164" id="Seg_602" n="HIAT:u" s="T155">
                  <ts e="T156" id="Seg_604" n="HIAT:w" s="T155">Nɨːn</ts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_607" n="HIAT:w" s="T156">aj</ts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_610" n="HIAT:w" s="T157">toː</ts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_613" n="HIAT:w" s="T158">tıːsätɨ</ts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_616" n="HIAT:w" s="T159">nʼuːqontɨ</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_619" n="HIAT:w" s="T160">nɔːnɨ</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_622" n="HIAT:w" s="T161">i</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_625" n="HIAT:w" s="T162">olontɨ</ts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_628" n="HIAT:w" s="T163">nɔːnɨ</ts>
                  <nts id="Seg_629" n="HIAT:ip">.</nts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_632" n="HIAT:u" s="T164">
                  <ts e="T165" id="Seg_634" n="HIAT:w" s="T164">Aj</ts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_637" n="HIAT:w" s="T165">panɨsɨtɨ</ts>
                  <nts id="Seg_638" n="HIAT:ip">,</nts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_641" n="HIAT:w" s="T166">panɨsɨtɨ</ts>
                  <nts id="Seg_642" n="HIAT:ip">,</nts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_645" n="HIAT:w" s="T167">panɨsɨtɨ</ts>
                  <nts id="Seg_646" n="HIAT:ip">.</nts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T173" id="Seg_649" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_651" n="HIAT:w" s="T168">Aj</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_654" n="HIAT:w" s="T169">tɛnɨrsɨ</ts>
                  <nts id="Seg_655" n="HIAT:ip">:</nts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_658" n="HIAT:w" s="T170">aj</ts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_661" n="HIAT:w" s="T171">panɨtqo</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_664" n="HIAT:w" s="T172">nɔːtno</ts>
                  <nts id="Seg_665" n="HIAT:ip">.</nts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T178" id="Seg_668" n="HIAT:u" s="T173">
                  <ts e="T174" id="Seg_670" n="HIAT:w" s="T173">Panɨsɨtɨ</ts>
                  <nts id="Seg_671" n="HIAT:ip">,</nts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_674" n="HIAT:w" s="T174">panɨsɨtɨ</ts>
                  <nts id="Seg_675" n="HIAT:ip">,</nts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_678" n="HIAT:w" s="T175">panɨsɨtɨ</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_681" n="HIAT:w" s="T176">mɨšʼaltukat</ts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_684" n="HIAT:w" s="T177">qopɨp</ts>
                  <nts id="Seg_685" n="HIAT:ip">.</nts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T180" id="Seg_688" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_690" n="HIAT:w" s="T178">Märq</ts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_693" n="HIAT:w" s="T179">ɛsa</ts>
                  <nts id="Seg_694" n="HIAT:ip">.</nts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T187" id="Seg_697" n="HIAT:u" s="T180">
                  <ts e="T181" id="Seg_699" n="HIAT:w" s="T180">Tiː</ts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_702" n="HIAT:w" s="T181">tokaltisɨtɨ</ts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_705" n="HIAT:w" s="T182">mɨšʼaltukat</ts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_708" n="HIAT:w" s="T183">qopɨp</ts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_711" n="HIAT:w" s="T184">käpoːqɨntɨ</ts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_714" n="HIAT:w" s="T185">i</ts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_717" n="HIAT:w" s="T186">nʼuːqoːqontɨ</ts>
                  <nts id="Seg_718" n="HIAT:ip">.</nts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T191" id="Seg_721" n="HIAT:u" s="T187">
                  <ts e="T188" id="Seg_723" n="HIAT:w" s="T187">Mɨšʼaltukat</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_726" n="HIAT:w" s="T188">qopɨ</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_729" n="HIAT:w" s="T189">tuːresitɨ</ts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_732" n="HIAT:w" s="T190">topoːqontɨ</ts>
                  <nts id="Seg_733" n="HIAT:ip">.</nts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T195" id="Seg_736" n="HIAT:u" s="T191">
                  <ts e="T192" id="Seg_738" n="HIAT:w" s="T191">Nɨːnɨ</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_741" n="HIAT:w" s="T192">täp</ts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_744" n="HIAT:w" s="T193">ɛsa</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_747" n="HIAT:w" s="T194">mɨšʼaltukatqo</ts>
                  <nts id="Seg_748" n="HIAT:ip">.</nts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T200" id="Seg_751" n="HIAT:u" s="T195">
                  <ts e="T196" id="Seg_753" n="HIAT:w" s="T195">Nɨːnɨ</ts>
                  <nts id="Seg_754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_756" n="HIAT:w" s="T196">mɨssɛːŋa</ts>
                  <nts id="Seg_757" n="HIAT:ip">,</nts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_760" n="HIAT:w" s="T197">omti</ts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_763" n="HIAT:w" s="T198">toːlɔː</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_766" n="HIAT:w" s="T199">toːnɨk</ts>
                  <nts id="Seg_767" n="HIAT:ip">.</nts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T203" id="Seg_770" n="HIAT:u" s="T200">
                  <ts e="T201" id="Seg_772" n="HIAT:w" s="T200">Ɔːmtɨlʼa</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_775" n="HIAT:w" s="T201">siqlaltɨmpa</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_778" n="HIAT:w" s="T202">tuːmtɨ</ts>
                  <nts id="Seg_779" n="HIAT:ip">.</nts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T208" id="Seg_782" n="HIAT:u" s="T203">
                  <ts e="T204" id="Seg_784" n="HIAT:w" s="T203">Siqlaltɨmpɨsɨtɨ</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_787" n="HIAT:w" s="T204">tuːmtɨ</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_790" n="HIAT:w" s="T205">i</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_793" n="HIAT:w" s="T206">mɨssɛːsa</ts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_796" n="HIAT:w" s="T207">num</ts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_799" n="HIAT:w" s="T350">ınʼnʼaːqɨt</ts>
                  <nts id="Seg_800" n="HIAT:ip">.</nts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T213" id="Seg_803" n="HIAT:u" s="T208">
                  <ts e="T209" id="Seg_805" n="HIAT:w" s="T208">I</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_808" n="HIAT:w" s="T209">nat</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_811" n="HIAT:w" s="T210">piːntersa</ts>
                  <nts id="Seg_812" n="HIAT:ip">,</nts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_815" n="HIAT:w" s="T211">kolʼempɨsa</ts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_818" n="HIAT:w" s="T212">num</ts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_821" n="HIAT:w" s="T351">ınʼnʼaːqɨt</ts>
                  <nts id="Seg_822" n="HIAT:ip">.</nts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T223" id="Seg_825" n="HIAT:u" s="T213">
                  <ts e="T214" id="Seg_827" n="HIAT:w" s="T213">Nɨːnɨ</ts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_830" n="HIAT:w" s="T214">mɨsʼsʼɛːsʼa</ts>
                  <nts id="Seg_831" n="HIAT:ip">,</nts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_834" n="HIAT:w" s="T215">tıːmpelä</ts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_837" n="HIAT:w" s="T216">kɨp</ts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_840" n="HIAT:w" s="T217">nʼoːtɨqolapsɨtɨ</ts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_843" n="HIAT:w" s="T218">takkɨlʼɔː</ts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_846" n="HIAT:w" s="T219">ilʼi</ts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_849" n="HIAT:w" s="T220">toː</ts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_852" n="HIAT:w" s="T221">tılʼtʼara</ts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_855" n="HIAT:w" s="T222">tıːmpelʼa</ts>
                  <nts id="Seg_856" n="HIAT:ip">.</nts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T231" id="Seg_859" n="HIAT:u" s="T223">
                  <ts e="T224" id="Seg_861" n="HIAT:w" s="T223">Okkɨr</ts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_864" n="HIAT:w" s="T224">čʼontoːqɨt</ts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_867" n="HIAT:w" s="T225">qoŋɨtɨ</ts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_870" n="HIAT:w" s="T226">kɨt</ts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_873" n="HIAT:w" s="T227">šünčʼeːqɨt</ts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_876" n="HIAT:w" s="T228">kɨːqɨt</ts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_879" n="HIAT:w" s="T229">antɨ</ts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_882" n="HIAT:w" s="T230">qänta</ts>
                  <nts id="Seg_883" n="HIAT:ip">.</nts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T233" id="Seg_886" n="HIAT:u" s="T231">
                  <ts e="T232" id="Seg_888" n="HIAT:w" s="T231">İllalɔː</ts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_891" n="HIAT:w" s="T232">üːtesa</ts>
                  <nts id="Seg_892" n="HIAT:ip">.</nts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T238" id="Seg_895" n="HIAT:u" s="T233">
                  <ts e="T234" id="Seg_897" n="HIAT:w" s="T233">Qoŋɨtɨ</ts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_900" n="HIAT:w" s="T234">loːsɨ</ts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_903" n="HIAT:w" s="T235">sitɨ</ts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_906" n="HIAT:w" s="T236">nälʼaiːntɨsa</ts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_909" n="HIAT:w" s="T237">qäntɔːtɨt</ts>
                  <nts id="Seg_910" n="HIAT:ip">.</nts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T243" id="Seg_913" n="HIAT:u" s="T238">
                  <ts e="T239" id="Seg_915" n="HIAT:w" s="T238">Loːsɨ</ts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_918" n="HIAT:w" s="T239">qoŋɨtɨ</ts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_921" n="HIAT:w" s="T240">mɨsalʼtuka</ts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_924" n="HIAT:w" s="T241">kolʼempa</ts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_927" n="HIAT:w" s="T242">oloːqɨnt</ts>
                  <nts id="Seg_928" n="HIAT:ip">.</nts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T245" id="Seg_931" n="HIAT:u" s="T243">
                  <ts e="T244" id="Seg_933" n="HIAT:w" s="T243">Tılʼtɨrlʼa</ts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_936" n="HIAT:w" s="T244">kolʼempa</ts>
                  <nts id="Seg_937" n="HIAT:ip">.</nts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T256" id="Seg_940" n="HIAT:u" s="T245">
                  <ts e="T246" id="Seg_942" n="HIAT:w" s="T245">Oj</ts>
                  <nts id="Seg_943" n="HIAT:ip">,</nts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_946" n="HIAT:w" s="T246">nälʼa</ts>
                  <nts id="Seg_947" n="HIAT:ip">,</nts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_950" n="HIAT:w" s="T247">mannɨmpät</ts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_953" n="HIAT:w" s="T248">kuttar</ts>
                  <nts id="Seg_954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_956" n="HIAT:w" s="T249">näkɨrɨlʼ</ts>
                  <nts id="Seg_957" n="HIAT:ip">,</nts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_960" n="HIAT:w" s="T250">suːrut</ts>
                  <nts id="Seg_961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_963" n="HIAT:w" s="T251">näka</ts>
                  <nts id="Seg_964" n="HIAT:ip">,</nts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_967" n="HIAT:w" s="T252">suːrup</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_970" n="HIAT:w" s="T253">tılʼterna</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_973" n="HIAT:w" s="T254">meː</ts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_976" n="HIAT:w" s="T255">pɔːrɨqɨnɨt</ts>
                  <nts id="Seg_977" n="HIAT:ip">!</nts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T269" id="Seg_980" n="HIAT:u" s="T256">
                  <ts e="T257" id="Seg_982" n="HIAT:w" s="T256">Loːsɨ</ts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_985" n="HIAT:w" s="T257">suːmpa</ts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_988" n="HIAT:w" s="T258">nalʼäiːqɨntɨ</ts>
                  <nts id="Seg_989" n="HIAT:ip">:</nts>
                  <nts id="Seg_990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_992" n="HIAT:w" s="T259">Nälʼa</ts>
                  <nts id="Seg_993" n="HIAT:ip">,</nts>
                  <nts id="Seg_994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_996" n="HIAT:w" s="T260">nʼalʼa</ts>
                  <nts id="Seg_997" n="HIAT:ip">,</nts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1000" n="HIAT:w" s="T261">mennɨmpatɨ</ts>
                  <nts id="Seg_1001" n="HIAT:ip">,</nts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_1004" n="HIAT:w" s="T262">qej</ts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_1007" n="HIAT:w" s="T263">meː</ts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_1010" n="HIAT:w" s="T264">pɔːroːqɨnɨt</ts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1013" n="HIAT:w" s="T265">suːrut</ts>
                  <nts id="Seg_1014" n="HIAT:ip">,</nts>
                  <nts id="Seg_1015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1017" n="HIAT:w" s="T266">kuttar</ts>
                  <nts id="Seg_1018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1020" n="HIAT:w" s="T267">suːrup</ts>
                  <nts id="Seg_1021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1023" n="HIAT:w" s="T268">tıːntɨ</ts>
                  <nts id="Seg_1024" n="HIAT:ip">.</nts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T280" id="Seg_1027" n="HIAT:u" s="T269">
                  <ts e="T270" id="Seg_1029" n="HIAT:w" s="T269">Mɨsʼsʼaltuka</ts>
                  <nts id="Seg_1030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1032" n="HIAT:w" s="T270">mɨssɛːlä</ts>
                  <nts id="Seg_1033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1035" n="HIAT:w" s="T271">qəssa</ts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1038" n="HIAT:w" s="T272">täpɨn</ts>
                  <nts id="Seg_1039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1041" n="HIAT:w" s="T273">ukoːntɨ</ts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1044" n="HIAT:w" s="T274">kɨntɨ</ts>
                  <nts id="Seg_1045" n="HIAT:ip">,</nts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1048" n="HIAT:w" s="T275">marqɨ</ts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1051" n="HIAT:w" s="T276">tümontɨ</ts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1054" n="HIAT:w" s="T277">kɨt</ts>
                  <nts id="Seg_1055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1057" n="HIAT:w" s="T278">qanoːqɨt</ts>
                  <nts id="Seg_1058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1060" n="HIAT:w" s="T279">omta</ts>
                  <nts id="Seg_1061" n="HIAT:ip">.</nts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T292" id="Seg_1064" n="HIAT:u" s="T280">
                  <ts e="T281" id="Seg_1066" n="HIAT:w" s="T280">Nɨːnɨ</ts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1069" n="HIAT:w" s="T281">šitɨ</ts>
                  <nts id="Seg_1070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1072" n="HIAT:w" s="T282">tümɨt</ts>
                  <nts id="Seg_1073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1075" n="HIAT:w" s="T283">pɔːrɨntɨ</ts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1078" n="HIAT:w" s="T284">nʼantɨ</ts>
                  <nts id="Seg_1079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1081" n="HIAT:w" s="T285">nʼaqqɨlʼlʼä</ts>
                  <nts id="Seg_1082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1083" n="HIAT:ip">(</nts>
                  <nts id="Seg_1084" n="HIAT:ip">/</nts>
                  <ts e="T287" id="Seg_1086" n="HIAT:w" s="T286">nʼakkɨsɨtɨ</ts>
                  <nts id="Seg_1087" n="HIAT:ip">)</nts>
                  <nts id="Seg_1088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1090" n="HIAT:w" s="T287">ɨːtɨsɨtɨ</ts>
                  <nts id="Seg_1091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1093" n="HIAT:w" s="T288">čʼesɨn</ts>
                  <nts id="Seg_1094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1096" n="HIAT:w" s="T289">kɨt</ts>
                  <nts id="Seg_1097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1099" n="HIAT:w" s="T290">sünnʼentɨ</ts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1101" n="HIAT:ip">(</nts>
                  <nts id="Seg_1102" n="HIAT:ip">/</nts>
                  <ts e="T292" id="Seg_1104" n="HIAT:w" s="T291">sünčʼöntɨ</ts>
                  <nts id="Seg_1105" n="HIAT:ip">)</nts>
                  <nts id="Seg_1106" n="HIAT:ip">.</nts>
                  <nts id="Seg_1107" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T297" id="Seg_1109" n="HIAT:u" s="T292">
                  <ts e="T293" id="Seg_1111" n="HIAT:w" s="T292">Loːsɨ</ts>
                  <nts id="Seg_1112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1114" n="HIAT:w" s="T293">tünta</ts>
                  <nts id="Seg_1115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1117" n="HIAT:w" s="T294">suːmpɨla</ts>
                  <nts id="Seg_1118" n="HIAT:ip">,</nts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1121" n="HIAT:w" s="T295">šitɨ</ts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1124" n="HIAT:w" s="T296">nalʼat</ts>
                  <nts id="Seg_1125" n="HIAT:ip">.</nts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T306" id="Seg_1128" n="HIAT:u" s="T297">
                  <ts e="T298" id="Seg_1130" n="HIAT:w" s="T297">Tüsa</ts>
                  <nts id="Seg_1131" n="HIAT:ip">,</nts>
                  <nts id="Seg_1132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1134" n="HIAT:w" s="T298">čʼesɨn</ts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1137" n="HIAT:w" s="T299">qänta</ts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1140" n="HIAT:w" s="T300">antɨsa</ts>
                  <nts id="Seg_1141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1143" n="HIAT:w" s="T301">tüsa</ts>
                  <nts id="Seg_1144" n="HIAT:ip">,</nts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1147" n="HIAT:w" s="T302">tettɨčʼaktɨ</ts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1150" n="HIAT:w" s="T303">čʼesɨntɨ</ts>
                  <nts id="Seg_1151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1153" n="HIAT:w" s="T304">tüsa</ts>
                  <nts id="Seg_1154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1156" n="HIAT:w" s="T305">antɨsa</ts>
                  <nts id="Seg_1157" n="HIAT:ip">.</nts>
                  <nts id="Seg_1158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T310" id="Seg_1160" n="HIAT:u" s="T306">
                  <ts e="T307" id="Seg_1162" n="HIAT:w" s="T306">Loːsɨ</ts>
                  <nts id="Seg_1163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1165" n="HIAT:w" s="T307">qontɨrtättɨ</ts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1168" n="HIAT:w" s="T308">čʼäːŋka</ts>
                  <nts id="Seg_1169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1171" n="HIAT:w" s="T309">čʼesɨnap</ts>
                  <nts id="Seg_1172" n="HIAT:ip">.</nts>
                  <nts id="Seg_1173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T315" id="Seg_1175" n="HIAT:u" s="T310">
                  <ts e="T311" id="Seg_1177" n="HIAT:w" s="T310">Čʼesɨn</ts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1180" n="HIAT:w" s="T311">soːlʼmɨntɨ</ts>
                  <nts id="Seg_1181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1183" n="HIAT:w" s="T312">mišalsitɨ</ts>
                  <nts id="Seg_1184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1186" n="HIAT:w" s="T313">šitɨ</ts>
                  <nts id="Seg_1187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1189" n="HIAT:w" s="T314">tüjmɨ</ts>
                  <nts id="Seg_1190" n="HIAT:ip">.</nts>
                  <nts id="Seg_1191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T320" id="Seg_1193" n="HIAT:u" s="T315">
                  <ts e="T316" id="Seg_1195" n="HIAT:w" s="T315">Loːsɨ</ts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1198" n="HIAT:w" s="T316">quptäːtɨ</ts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1201" n="HIAT:w" s="T317">asa</ts>
                  <nts id="Seg_1202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1204" n="HIAT:w" s="T318">tɛnɨmɨtɨ</ts>
                  <nts id="Seg_1205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1206" n="HIAT:ip">(</nts>
                  <nts id="Seg_1207" n="HIAT:ip">/</nts>
                  <ts e="T320" id="Seg_1209" n="HIAT:w" s="T319">qumpa</ts>
                  <nts id="Seg_1210" n="HIAT:ip">)</nts>
                  <nts id="Seg_1211" n="HIAT:ip">.</nts>
                  <nts id="Seg_1212" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T336" id="Seg_1214" n="HIAT:u" s="T320">
                  <ts e="T321" id="Seg_1216" n="HIAT:w" s="T320">Täp</ts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1219" n="HIAT:w" s="T321">antoːqɨt</ts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1222" n="HIAT:w" s="T322">orqɨlsɨtɨ</ts>
                  <nts id="Seg_1223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1225" n="HIAT:w" s="T323">loːsɨn</ts>
                  <nts id="Seg_1226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1228" n="HIAT:w" s="T324">nalʼaiːmtɨ</ts>
                  <nts id="Seg_1229" n="HIAT:ip">,</nts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1232" n="HIAT:w" s="T325">kartɨstɨ</ts>
                  <nts id="Seg_1233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1235" n="HIAT:w" s="T326">kɨt</ts>
                  <nts id="Seg_1236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1238" n="HIAT:w" s="T327">qanɨktɨ</ts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1241" n="HIAT:w" s="T328">i</ts>
                  <nts id="Seg_1242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1244" n="HIAT:w" s="T329">loːsɨn</ts>
                  <nts id="Seg_1245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1247" n="HIAT:w" s="T330">nalʼap</ts>
                  <nts id="Seg_1248" n="HIAT:ip">,</nts>
                  <nts id="Seg_1249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1251" n="HIAT:w" s="T331">okkɨr</ts>
                  <nts id="Seg_1252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1254" n="HIAT:w" s="T332">nʼeːmtɨ</ts>
                  <nts id="Seg_1255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1257" n="HIAT:w" s="T333">čʼelʼčʼɔːllä</ts>
                  <nts id="Seg_1258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1260" n="HIAT:w" s="T334">pissätɨ</ts>
                  <nts id="Seg_1261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1263" n="HIAT:w" s="T335">sʼöntɨ</ts>
                  <nts id="Seg_1264" n="HIAT:ip">.</nts>
                  <nts id="Seg_1265" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T343" id="Seg_1267" n="HIAT:u" s="T336">
                  <ts e="T337" id="Seg_1269" n="HIAT:w" s="T336">Šintälelʼ</ts>
                  <nts id="Seg_1270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1272" n="HIAT:w" s="T337">nälʼamtɨ</ts>
                  <nts id="Seg_1273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1275" n="HIAT:w" s="T338">aj</ts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1278" n="HIAT:w" s="T339">nılʼčʼik</ts>
                  <nts id="Seg_1279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1281" n="HIAT:w" s="T340">čʼattɨsɨtɨ</ts>
                  <nts id="Seg_1282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1284" n="HIAT:w" s="T341">i</ts>
                  <nts id="Seg_1285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1287" n="HIAT:w" s="T342">selʼtisɨtɨ</ts>
                  <nts id="Seg_1288" n="HIAT:ip">.</nts>
                  <nts id="Seg_1289" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T349" id="Seg_1291" n="HIAT:u" s="T343">
                  <ts e="T344" id="Seg_1293" n="HIAT:w" s="T343">Mat</ts>
                  <nts id="Seg_1294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1296" n="HIAT:w" s="T344">čʼaptämɨ</ts>
                  <nts id="Seg_1297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1299" n="HIAT:w" s="T345">kutɨka</ts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1302" n="HIAT:w" s="T346">üŋkɨltempɨsɨtɨ</ts>
                  <nts id="Seg_1303" n="HIAT:ip">,</nts>
                  <nts id="Seg_1304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1306" n="HIAT:w" s="T347">meːltetqo</ts>
                  <nts id="Seg_1307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1309" n="HIAT:w" s="T348">apsɨtɨ</ts>
                  <nts id="Seg_1310" n="HIAT:ip">.</nts>
                  <nts id="Seg_1311" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T349" id="Seg_1312" n="sc" s="T0">
               <ts e="T1" id="Seg_1314" n="e" s="T0">Ičʼakiːčʼika. </ts>
               <ts e="T2" id="Seg_1316" n="e" s="T1">Ilɨmpa </ts>
               <ts e="T3" id="Seg_1318" n="e" s="T2">Ičʼakʼiːčʼika </ts>
               <ts e="T4" id="Seg_1320" n="e" s="T3">imlʼantɨsa. </ts>
               <ts e="T5" id="Seg_1322" n="e" s="T4">Ičʼakiːčʼik </ts>
               <ts e="T6" id="Seg_1324" n="e" s="T5">qänna </ts>
               <ts e="T7" id="Seg_1326" n="e" s="T6">mačʼontɨ, </ts>
               <ts e="T8" id="Seg_1328" n="e" s="T7">qoŋɨtɨ </ts>
               <ts e="T9" id="Seg_1330" n="e" s="T8">qup </ts>
               <ts e="T10" id="Seg_1332" n="e" s="T9">pačʼetta. </ts>
               <ts e="T11" id="Seg_1334" n="e" s="T10">Tüŋa </ts>
               <ts e="T12" id="Seg_1336" n="e" s="T11">tättɨčʼaktɨ, </ts>
               <ts e="T13" id="Seg_1338" n="e" s="T12">qoŋɨtɨ </ts>
               <ts e="T14" id="Seg_1340" n="e" s="T13">märɨqa </ts>
               <ts e="T15" id="Seg_1342" n="e" s="T14">loːs </ts>
               <ts e="T16" id="Seg_1344" n="e" s="T15">(/loːsɨlʼ </ts>
               <ts e="T17" id="Seg_1346" n="e" s="T16">qup). </ts>
               <ts e="T18" id="Seg_1348" n="e" s="T17">Ilʼčʼa, </ts>
               <ts e="T19" id="Seg_1350" n="e" s="T18">qoj </ts>
               <ts e="T20" id="Seg_1352" n="e" s="T19">meːttal? </ts>
               <ts e="T21" id="Seg_1354" n="e" s="T20">Ma </ts>
               <ts e="T22" id="Seg_1356" n="e" s="T21">poːp </ts>
               <ts e="T23" id="Seg_1358" n="e" s="T22">pačʼertap– </ts>
               <ts e="T24" id="Seg_1360" n="e" s="T23">loːsɨ </ts>
               <ts e="T25" id="Seg_1362" n="e" s="T24">kättɨtɨ </ts>
               <ts e="T26" id="Seg_1364" n="e" s="T25">(/kättɨsɨtɨ). </ts>
               <ts e="T27" id="Seg_1366" n="e" s="T26">Ma </ts>
               <ts e="T28" id="Seg_1368" n="e" s="T27">okoːt </ts>
               <ts e="T29" id="Seg_1370" n="e" s="T28">tap </ts>
               <ts e="T30" id="Seg_1372" n="e" s="T29">poːp </ts>
               <ts e="T31" id="Seg_1374" n="e" s="T30">utsä </ts>
               <ts e="T32" id="Seg_1376" n="e" s="T31">šitɨ </ts>
               <ts e="T33" id="Seg_1378" n="e" s="T32">nɨtqolʼlʼɛːkkak. </ts>
               <ts e="T34" id="Seg_1380" n="e" s="T33">Kusa, </ts>
               <ts e="T35" id="Seg_1382" n="e" s="T34">kätsan, </ts>
               <ts e="T36" id="Seg_1384" n="e" s="T35">šitɨ </ts>
               <ts e="T37" id="Seg_1386" n="e" s="T36">nɨtqalʼät, </ts>
               <ts e="T38" id="Seg_1388" n="e" s="T37">kusa </ts>
               <ts e="T39" id="Seg_1390" n="e" s="T38">saŋatɨ! </ts>
               <ts e="T40" id="Seg_1392" n="e" s="T39">Loːsɨlʼ </ts>
               <ts e="T41" id="Seg_1394" n="e" s="T40">ilʼčʼat </ts>
               <ts e="T42" id="Seg_1396" n="e" s="T41">poːp </ts>
               <ts e="T43" id="Seg_1398" n="e" s="T42">šitɨ </ts>
               <ts e="T44" id="Seg_1400" n="e" s="T43">nɨtqɨlnɨtɨ. </ts>
               <ts e="T45" id="Seg_1402" n="e" s="T44">Kusa </ts>
               <ts e="T46" id="Seg_1404" n="e" s="T45">utol </ts>
               <ts e="T47" id="Seg_1406" n="e" s="T46">saqaltɨ, </ts>
               <ts e="T48" id="Seg_1408" n="e" s="T47">šitɨ </ts>
               <ts e="T49" id="Seg_1410" n="e" s="T48">utol </ts>
               <ts e="T50" id="Seg_1412" n="e" s="T49">tokalʼtätɨ, </ts>
               <ts e="T51" id="Seg_1414" n="e" s="T50">naqqəltɨ </ts>
               <ts e="T52" id="Seg_1416" n="e" s="T51">kɨsa! </ts>
               <ts e="T53" id="Seg_1418" n="e" s="T52">Poː </ts>
               <ts e="T54" id="Seg_1420" n="e" s="T53">orqɨlsɨt </ts>
               <ts e="T55" id="Seg_1422" n="e" s="T54">utemtɨ, </ts>
               <ts e="T56" id="Seg_1424" n="e" s="T55">šitɨ </ts>
               <ts e="T57" id="Seg_1426" n="e" s="T56">utətɨ. </ts>
               <ts e="T58" id="Seg_1428" n="e" s="T57">Täm </ts>
               <ts e="T59" id="Seg_1430" n="e" s="T58">ɔːmta, </ts>
               <ts e="T60" id="Seg_1432" n="e" s="T59">ortɨ </ts>
               <ts e="T61" id="Seg_1434" n="e" s="T60">čʼäːŋka. </ts>
               <ts e="T62" id="Seg_1436" n="e" s="T61">Loːsɨ </ts>
               <ts e="T63" id="Seg_1438" n="e" s="T62">kätäsɨtɨ: </ts>
               <ts e="T64" id="Seg_1440" n="e" s="T63">Ɔːmtasik </ts>
               <ts e="T65" id="Seg_1442" n="e" s="T64">i </ts>
               <ts e="T66" id="Seg_1444" n="e" s="T65">teːkäsik. </ts>
               <ts e="T67" id="Seg_1446" n="e" s="T66">Loːsɨ </ts>
               <ts e="T68" id="Seg_1448" n="e" s="T67">qəssɨ. </ts>
               <ts e="T69" id="Seg_1450" n="e" s="T68">Ičʼakɛːčʼika </ts>
               <ts e="T70" id="Seg_1452" n="e" s="T69">ɔːmtɨsa, </ts>
               <ts e="T71" id="Seg_1454" n="e" s="T70">ɔːmtɨsa, </ts>
               <ts e="T72" id="Seg_1456" n="e" s="T71">kuntɨ </ts>
               <ts e="T73" id="Seg_1458" n="e" s="T72">čʼap </ts>
               <ts e="T74" id="Seg_1460" n="e" s="T73">kuntɨ, </ts>
               <ts e="T75" id="Seg_1462" n="e" s="T74">qäntɨk </ts>
               <ts e="T76" id="Seg_1464" n="e" s="T75">čʼap </ts>
               <ts e="T77" id="Seg_1466" n="e" s="T76">qäntɨk. </ts>
               <ts e="T78" id="Seg_1468" n="e" s="T77">Tɛnɨrpɨsa </ts>
               <ts e="T79" id="Seg_1470" n="e" s="T78">qäntɨk </ts>
               <ts e="T80" id="Seg_1472" n="e" s="T79">tanteqo. </ts>
               <ts e="T81" id="Seg_1474" n="e" s="T80">Qältɨrsa </ts>
               <ts e="T82" id="Seg_1476" n="e" s="T81">i </ts>
               <ts e="T83" id="Seg_1478" n="e" s="T82">kutarsa, </ts>
               <ts e="T84" id="Seg_1480" n="e" s="T83">ɛːppa </ts>
               <ts e="T85" id="Seg_1482" n="e" s="T84">qätɨtɨ. </ts>
               <ts e="T86" id="Seg_1484" n="e" s="T85">Täpɨni </ts>
               <ts e="T87" id="Seg_1486" n="e" s="T86">tüŋa </ts>
               <ts e="T88" id="Seg_1488" n="e" s="T87">qorqə. </ts>
               <ts e="T89" id="Seg_1490" n="e" s="T88">Ilʼčʼa, </ts>
               <ts e="T90" id="Seg_1492" n="e" s="T89">poːp </ts>
               <ts e="T91" id="Seg_1494" n="e" s="T90">sitɨ </ts>
               <ts e="T92" id="Seg_1496" n="e" s="T91">nɨtqɨltɨ. </ts>
               <ts e="T93" id="Seg_1498" n="e" s="T92">Qorqə </ts>
               <ts e="T94" id="Seg_1500" n="e" s="T93">sitɨ </ts>
               <ts e="T95" id="Seg_1502" n="e" s="T94">nɨtqɨlʼsitɨ, </ts>
               <ts e="T96" id="Seg_1504" n="e" s="T95">utɨtɨ </ts>
               <ts e="T97" id="Seg_1506" n="e" s="T96">tıːtesitɨ. </ts>
               <ts e="T98" id="Seg_1508" n="e" s="T97">I </ts>
               <ts e="T99" id="Seg_1510" n="e" s="T98">utətə </ts>
               <ts e="T100" id="Seg_1512" n="e" s="T99">uːpɨl </ts>
               <ts e="T101" id="Seg_1514" n="e" s="T100">alʼčʼasa. </ts>
               <ts e="T102" id="Seg_1516" n="e" s="T101">Tanta </ts>
               <ts e="T103" id="Seg_1518" n="e" s="T102">poː </ts>
               <ts e="T104" id="Seg_1520" n="e" s="T103">nɔːnɨ. </ts>
               <ts e="T105" id="Seg_1522" n="e" s="T104">Kuttarna, </ts>
               <ts e="T106" id="Seg_1524" n="e" s="T105">qälterna, </ts>
               <ts e="T107" id="Seg_1526" n="e" s="T106">tɛnɨrpa </ts>
               <ts e="T108" id="Seg_1528" n="e" s="T107">qäntɨk </ts>
               <ts e="T109" id="Seg_1530" n="e" s="T108">mitɨqo </ts>
               <ts e="T110" id="Seg_1532" n="e" s="T109">i </ts>
               <ts e="T111" id="Seg_1534" n="e" s="T110">qonterqo </ts>
               <ts e="T112" id="Seg_1536" n="e" s="T111">loːsɨp. </ts>
               <ts e="T113" id="Seg_1538" n="e" s="T112">Kuttarna, </ts>
               <ts e="T114" id="Seg_1540" n="e" s="T113">qälimpa, </ts>
               <ts e="T115" id="Seg_1542" n="e" s="T114">tɛnɨrpɨt, </ts>
               <ts e="T116" id="Seg_1544" n="e" s="T115">kuttar </ts>
               <ts e="T117" id="Seg_1546" n="e" s="T116">loːsɨp </ts>
               <ts e="T118" id="Seg_1548" n="e" s="T117">mitäqo, </ts>
               <ts e="T119" id="Seg_1550" n="e" s="T118">orqɨlqo, </ts>
               <ts e="T120" id="Seg_1552" n="e" s="T119">orqɨllä </ts>
               <ts e="T121" id="Seg_1554" n="e" s="T120">iːlela. </ts>
               <ts e="T122" id="Seg_1556" n="e" s="T121">Tɛnɨrpa, </ts>
               <ts e="T123" id="Seg_1558" n="e" s="T122">kučʼä </ts>
               <ts e="T124" id="Seg_1560" n="e" s="T123">qateqo. </ts>
               <ts e="T125" id="Seg_1562" n="e" s="T124">Oloːqɨntɨ </ts>
               <ts e="T126" id="Seg_1564" n="e" s="T125">alʼčʼa </ts>
               <ts e="T127" id="Seg_1566" n="e" s="T126">tɛnɨtɨ: </ts>
               <ts e="T128" id="Seg_1568" n="e" s="T127">Nɔːtna </ts>
               <ts e="T129" id="Seg_1570" n="e" s="T128">karmanʼmɨ </ts>
               <ts e="T130" id="Seg_1572" n="e" s="T129">mennɨmpɨqo, </ts>
               <ts e="T131" id="Seg_1574" n="e" s="T130">korpɨtqo! </ts>
               <ts e="T132" id="Seg_1576" n="e" s="T131">Karmanʼqɨntɨ </ts>
               <ts e="T133" id="Seg_1578" n="e" s="T132">qosɨtɨ </ts>
               <ts e="T134" id="Seg_1580" n="e" s="T133">mɨšʼaltukat </ts>
               <ts e="T135" id="Seg_1582" n="e" s="T134">qopɨ. </ts>
               <ts e="T136" id="Seg_1584" n="e" s="T135">Nɨŋɨlʼä </ts>
               <ts e="T137" id="Seg_1586" n="e" s="T136">panɨtqolapsɨt </ts>
               <ts e="T138" id="Seg_1588" n="e" s="T137">mɨšʼaltukat </ts>
               <ts e="T139" id="Seg_1590" n="e" s="T138">qopɨ. </ts>
               <ts e="T140" id="Seg_1592" n="e" s="T139">Panɨsɨtɨ, </ts>
               <ts e="T141" id="Seg_1594" n="e" s="T140">märqɨlɔː </ts>
               <ts e="T142" id="Seg_1596" n="e" s="T141">ɛsɨsa. </ts>
               <ts e="T143" id="Seg_1598" n="e" s="T142">Tokaltistɨ </ts>
               <ts e="T144" id="Seg_1600" n="e" s="T143">oloːqɨntɨ. </ts>
               <ts e="T145" id="Seg_1602" n="e" s="T144">Nɨːnɨ </ts>
               <ts e="T146" id="Seg_1604" n="e" s="T145">qotä </ts>
               <ts e="T147" id="Seg_1606" n="e" s="T146">toː </ts>
               <ts e="T148" id="Seg_1608" n="e" s="T147">tıːtesɨtɨ. </ts>
               <ts e="T149" id="Seg_1610" n="e" s="T148">Očʼik </ts>
               <ts e="T150" id="Seg_1612" n="e" s="T149">panalsɨtɨ. </ts>
               <ts e="T151" id="Seg_1614" n="e" s="T150">Aj </ts>
               <ts e="T152" id="Seg_1616" n="e" s="T151">očʼik </ts>
               <ts e="T153" id="Seg_1618" n="e" s="T152">tokaltistɨ </ts>
               <ts e="T154" id="Seg_1620" n="e" s="T153">oloːqɨntɨ, </ts>
               <ts e="T155" id="Seg_1622" n="e" s="T154">kəpoːqɨntɨ. </ts>
               <ts e="T156" id="Seg_1624" n="e" s="T155">Nɨːn </ts>
               <ts e="T157" id="Seg_1626" n="e" s="T156">aj </ts>
               <ts e="T158" id="Seg_1628" n="e" s="T157">toː </ts>
               <ts e="T159" id="Seg_1630" n="e" s="T158">tıːsätɨ </ts>
               <ts e="T160" id="Seg_1632" n="e" s="T159">nʼuːqontɨ </ts>
               <ts e="T161" id="Seg_1634" n="e" s="T160">nɔːnɨ </ts>
               <ts e="T162" id="Seg_1636" n="e" s="T161">i </ts>
               <ts e="T163" id="Seg_1638" n="e" s="T162">olontɨ </ts>
               <ts e="T164" id="Seg_1640" n="e" s="T163">nɔːnɨ. </ts>
               <ts e="T165" id="Seg_1642" n="e" s="T164">Aj </ts>
               <ts e="T166" id="Seg_1644" n="e" s="T165">panɨsɨtɨ, </ts>
               <ts e="T167" id="Seg_1646" n="e" s="T166">panɨsɨtɨ, </ts>
               <ts e="T168" id="Seg_1648" n="e" s="T167">panɨsɨtɨ. </ts>
               <ts e="T169" id="Seg_1650" n="e" s="T168">Aj </ts>
               <ts e="T170" id="Seg_1652" n="e" s="T169">tɛnɨrsɨ: </ts>
               <ts e="T171" id="Seg_1654" n="e" s="T170">aj </ts>
               <ts e="T172" id="Seg_1656" n="e" s="T171">panɨtqo </ts>
               <ts e="T173" id="Seg_1658" n="e" s="T172">nɔːtno. </ts>
               <ts e="T174" id="Seg_1660" n="e" s="T173">Panɨsɨtɨ, </ts>
               <ts e="T175" id="Seg_1662" n="e" s="T174">panɨsɨtɨ, </ts>
               <ts e="T176" id="Seg_1664" n="e" s="T175">panɨsɨtɨ </ts>
               <ts e="T177" id="Seg_1666" n="e" s="T176">mɨšʼaltukat </ts>
               <ts e="T178" id="Seg_1668" n="e" s="T177">qopɨp. </ts>
               <ts e="T179" id="Seg_1670" n="e" s="T178">Märq </ts>
               <ts e="T180" id="Seg_1672" n="e" s="T179">ɛsa. </ts>
               <ts e="T181" id="Seg_1674" n="e" s="T180">Tiː </ts>
               <ts e="T182" id="Seg_1676" n="e" s="T181">tokaltisɨtɨ </ts>
               <ts e="T183" id="Seg_1678" n="e" s="T182">mɨšʼaltukat </ts>
               <ts e="T184" id="Seg_1680" n="e" s="T183">qopɨp </ts>
               <ts e="T185" id="Seg_1682" n="e" s="T184">käpoːqɨntɨ </ts>
               <ts e="T186" id="Seg_1684" n="e" s="T185">i </ts>
               <ts e="T187" id="Seg_1686" n="e" s="T186">nʼuːqoːqontɨ. </ts>
               <ts e="T188" id="Seg_1688" n="e" s="T187">Mɨšʼaltukat </ts>
               <ts e="T189" id="Seg_1690" n="e" s="T188">qopɨ </ts>
               <ts e="T190" id="Seg_1692" n="e" s="T189">tuːresitɨ </ts>
               <ts e="T191" id="Seg_1694" n="e" s="T190">topoːqontɨ. </ts>
               <ts e="T192" id="Seg_1696" n="e" s="T191">Nɨːnɨ </ts>
               <ts e="T193" id="Seg_1698" n="e" s="T192">täp </ts>
               <ts e="T194" id="Seg_1700" n="e" s="T193">ɛsa </ts>
               <ts e="T195" id="Seg_1702" n="e" s="T194">mɨšʼaltukatqo. </ts>
               <ts e="T196" id="Seg_1704" n="e" s="T195">Nɨːnɨ </ts>
               <ts e="T197" id="Seg_1706" n="e" s="T196">mɨssɛːŋa, </ts>
               <ts e="T198" id="Seg_1708" n="e" s="T197">omti </ts>
               <ts e="T199" id="Seg_1710" n="e" s="T198">toːlɔː </ts>
               <ts e="T200" id="Seg_1712" n="e" s="T199">toːnɨk. </ts>
               <ts e="T201" id="Seg_1714" n="e" s="T200">Ɔːmtɨlʼa </ts>
               <ts e="T202" id="Seg_1716" n="e" s="T201">siqlaltɨmpa </ts>
               <ts e="T203" id="Seg_1718" n="e" s="T202">tuːmtɨ. </ts>
               <ts e="T204" id="Seg_1720" n="e" s="T203">Siqlaltɨmpɨsɨtɨ </ts>
               <ts e="T205" id="Seg_1722" n="e" s="T204">tuːmtɨ </ts>
               <ts e="T206" id="Seg_1724" n="e" s="T205">i </ts>
               <ts e="T207" id="Seg_1726" n="e" s="T206">mɨssɛːsa </ts>
               <ts e="T350" id="Seg_1728" n="e" s="T207">num </ts>
               <ts e="T208" id="Seg_1730" n="e" s="T350">ınʼnʼaːqɨt. </ts>
               <ts e="T209" id="Seg_1732" n="e" s="T208">I </ts>
               <ts e="T210" id="Seg_1734" n="e" s="T209">nat </ts>
               <ts e="T211" id="Seg_1736" n="e" s="T210">piːntersa, </ts>
               <ts e="T212" id="Seg_1738" n="e" s="T211">kolʼempɨsa </ts>
               <ts e="T351" id="Seg_1740" n="e" s="T212">num </ts>
               <ts e="T213" id="Seg_1742" n="e" s="T351">ınʼnʼaːqɨt. </ts>
               <ts e="T214" id="Seg_1744" n="e" s="T213">Nɨːnɨ </ts>
               <ts e="T215" id="Seg_1746" n="e" s="T214">mɨsʼsʼɛːsʼa, </ts>
               <ts e="T216" id="Seg_1748" n="e" s="T215">tıːmpelä </ts>
               <ts e="T217" id="Seg_1750" n="e" s="T216">kɨp </ts>
               <ts e="T218" id="Seg_1752" n="e" s="T217">nʼoːtɨqolapsɨtɨ </ts>
               <ts e="T219" id="Seg_1754" n="e" s="T218">takkɨlʼɔː </ts>
               <ts e="T220" id="Seg_1756" n="e" s="T219">ilʼi </ts>
               <ts e="T221" id="Seg_1758" n="e" s="T220">toː </ts>
               <ts e="T222" id="Seg_1760" n="e" s="T221">tılʼtʼara </ts>
               <ts e="T223" id="Seg_1762" n="e" s="T222">tıːmpelʼa. </ts>
               <ts e="T224" id="Seg_1764" n="e" s="T223">Okkɨr </ts>
               <ts e="T225" id="Seg_1766" n="e" s="T224">čʼontoːqɨt </ts>
               <ts e="T226" id="Seg_1768" n="e" s="T225">qoŋɨtɨ </ts>
               <ts e="T227" id="Seg_1770" n="e" s="T226">kɨt </ts>
               <ts e="T228" id="Seg_1772" n="e" s="T227">šünčʼeːqɨt </ts>
               <ts e="T229" id="Seg_1774" n="e" s="T228">kɨːqɨt </ts>
               <ts e="T230" id="Seg_1776" n="e" s="T229">antɨ </ts>
               <ts e="T231" id="Seg_1778" n="e" s="T230">qänta. </ts>
               <ts e="T232" id="Seg_1780" n="e" s="T231">İllalɔː </ts>
               <ts e="T233" id="Seg_1782" n="e" s="T232">üːtesa. </ts>
               <ts e="T234" id="Seg_1784" n="e" s="T233">Qoŋɨtɨ </ts>
               <ts e="T235" id="Seg_1786" n="e" s="T234">loːsɨ </ts>
               <ts e="T236" id="Seg_1788" n="e" s="T235">sitɨ </ts>
               <ts e="T237" id="Seg_1790" n="e" s="T236">nälʼaiːntɨsa </ts>
               <ts e="T238" id="Seg_1792" n="e" s="T237">qäntɔːtɨt. </ts>
               <ts e="T239" id="Seg_1794" n="e" s="T238">Loːsɨ </ts>
               <ts e="T240" id="Seg_1796" n="e" s="T239">qoŋɨtɨ </ts>
               <ts e="T241" id="Seg_1798" n="e" s="T240">mɨsalʼtuka </ts>
               <ts e="T242" id="Seg_1800" n="e" s="T241">kolʼempa </ts>
               <ts e="T243" id="Seg_1802" n="e" s="T242">oloːqɨnt. </ts>
               <ts e="T244" id="Seg_1804" n="e" s="T243">Tılʼtɨrlʼa </ts>
               <ts e="T245" id="Seg_1806" n="e" s="T244">kolʼempa. </ts>
               <ts e="T246" id="Seg_1808" n="e" s="T245">Oj, </ts>
               <ts e="T247" id="Seg_1810" n="e" s="T246">nälʼa, </ts>
               <ts e="T248" id="Seg_1812" n="e" s="T247">mannɨmpät </ts>
               <ts e="T249" id="Seg_1814" n="e" s="T248">kuttar </ts>
               <ts e="T250" id="Seg_1816" n="e" s="T249">näkɨrɨlʼ, </ts>
               <ts e="T251" id="Seg_1818" n="e" s="T250">suːrut </ts>
               <ts e="T252" id="Seg_1820" n="e" s="T251">näka, </ts>
               <ts e="T253" id="Seg_1822" n="e" s="T252">suːrup </ts>
               <ts e="T254" id="Seg_1824" n="e" s="T253">tılʼterna </ts>
               <ts e="T255" id="Seg_1826" n="e" s="T254">meː </ts>
               <ts e="T256" id="Seg_1828" n="e" s="T255">pɔːrɨqɨnɨt! </ts>
               <ts e="T257" id="Seg_1830" n="e" s="T256">Loːsɨ </ts>
               <ts e="T258" id="Seg_1832" n="e" s="T257">suːmpa </ts>
               <ts e="T259" id="Seg_1834" n="e" s="T258">nalʼäiːqɨntɨ: </ts>
               <ts e="T260" id="Seg_1836" n="e" s="T259">Nälʼa, </ts>
               <ts e="T261" id="Seg_1838" n="e" s="T260">nʼalʼa, </ts>
               <ts e="T262" id="Seg_1840" n="e" s="T261">mennɨmpatɨ, </ts>
               <ts e="T263" id="Seg_1842" n="e" s="T262">qej </ts>
               <ts e="T264" id="Seg_1844" n="e" s="T263">meː </ts>
               <ts e="T265" id="Seg_1846" n="e" s="T264">pɔːroːqɨnɨt </ts>
               <ts e="T266" id="Seg_1848" n="e" s="T265">suːrut, </ts>
               <ts e="T267" id="Seg_1850" n="e" s="T266">kuttar </ts>
               <ts e="T268" id="Seg_1852" n="e" s="T267">suːrup </ts>
               <ts e="T269" id="Seg_1854" n="e" s="T268">tıːntɨ. </ts>
               <ts e="T270" id="Seg_1856" n="e" s="T269">Mɨsʼsʼaltuka </ts>
               <ts e="T271" id="Seg_1858" n="e" s="T270">mɨssɛːlä </ts>
               <ts e="T272" id="Seg_1860" n="e" s="T271">qəssa </ts>
               <ts e="T273" id="Seg_1862" n="e" s="T272">täpɨn </ts>
               <ts e="T274" id="Seg_1864" n="e" s="T273">ukoːntɨ </ts>
               <ts e="T275" id="Seg_1866" n="e" s="T274">kɨntɨ, </ts>
               <ts e="T276" id="Seg_1868" n="e" s="T275">marqɨ </ts>
               <ts e="T277" id="Seg_1870" n="e" s="T276">tümontɨ </ts>
               <ts e="T278" id="Seg_1872" n="e" s="T277">kɨt </ts>
               <ts e="T279" id="Seg_1874" n="e" s="T278">qanoːqɨt </ts>
               <ts e="T280" id="Seg_1876" n="e" s="T279">omta. </ts>
               <ts e="T281" id="Seg_1878" n="e" s="T280">Nɨːnɨ </ts>
               <ts e="T282" id="Seg_1880" n="e" s="T281">šitɨ </ts>
               <ts e="T283" id="Seg_1882" n="e" s="T282">tümɨt </ts>
               <ts e="T284" id="Seg_1884" n="e" s="T283">pɔːrɨntɨ </ts>
               <ts e="T285" id="Seg_1886" n="e" s="T284">nʼantɨ </ts>
               <ts e="T286" id="Seg_1888" n="e" s="T285">nʼaqqɨlʼlʼä </ts>
               <ts e="T287" id="Seg_1890" n="e" s="T286">(/nʼakkɨsɨtɨ) </ts>
               <ts e="T288" id="Seg_1892" n="e" s="T287">ɨːtɨsɨtɨ </ts>
               <ts e="T289" id="Seg_1894" n="e" s="T288">čʼesɨn </ts>
               <ts e="T290" id="Seg_1896" n="e" s="T289">kɨt </ts>
               <ts e="T291" id="Seg_1898" n="e" s="T290">sünnʼentɨ </ts>
               <ts e="T292" id="Seg_1900" n="e" s="T291">(/sünčʼöntɨ). </ts>
               <ts e="T293" id="Seg_1902" n="e" s="T292">Loːsɨ </ts>
               <ts e="T294" id="Seg_1904" n="e" s="T293">tünta </ts>
               <ts e="T295" id="Seg_1906" n="e" s="T294">suːmpɨla, </ts>
               <ts e="T296" id="Seg_1908" n="e" s="T295">šitɨ </ts>
               <ts e="T297" id="Seg_1910" n="e" s="T296">nalʼat. </ts>
               <ts e="T298" id="Seg_1912" n="e" s="T297">Tüsa, </ts>
               <ts e="T299" id="Seg_1914" n="e" s="T298">čʼesɨn </ts>
               <ts e="T300" id="Seg_1916" n="e" s="T299">qänta </ts>
               <ts e="T301" id="Seg_1918" n="e" s="T300">antɨsa </ts>
               <ts e="T302" id="Seg_1920" n="e" s="T301">tüsa, </ts>
               <ts e="T303" id="Seg_1922" n="e" s="T302">tettɨčʼaktɨ </ts>
               <ts e="T304" id="Seg_1924" n="e" s="T303">čʼesɨntɨ </ts>
               <ts e="T305" id="Seg_1926" n="e" s="T304">tüsa </ts>
               <ts e="T306" id="Seg_1928" n="e" s="T305">antɨsa. </ts>
               <ts e="T307" id="Seg_1930" n="e" s="T306">Loːsɨ </ts>
               <ts e="T308" id="Seg_1932" n="e" s="T307">qontɨrtättɨ </ts>
               <ts e="T309" id="Seg_1934" n="e" s="T308">čʼäːŋka </ts>
               <ts e="T310" id="Seg_1936" n="e" s="T309">čʼesɨnap. </ts>
               <ts e="T311" id="Seg_1938" n="e" s="T310">Čʼesɨn </ts>
               <ts e="T312" id="Seg_1940" n="e" s="T311">soːlʼmɨntɨ </ts>
               <ts e="T313" id="Seg_1942" n="e" s="T312">mišalsitɨ </ts>
               <ts e="T314" id="Seg_1944" n="e" s="T313">šitɨ </ts>
               <ts e="T315" id="Seg_1946" n="e" s="T314">tüjmɨ. </ts>
               <ts e="T316" id="Seg_1948" n="e" s="T315">Loːsɨ </ts>
               <ts e="T317" id="Seg_1950" n="e" s="T316">quptäːtɨ </ts>
               <ts e="T318" id="Seg_1952" n="e" s="T317">asa </ts>
               <ts e="T319" id="Seg_1954" n="e" s="T318">tɛnɨmɨtɨ </ts>
               <ts e="T320" id="Seg_1956" n="e" s="T319">(/qumpa). </ts>
               <ts e="T321" id="Seg_1958" n="e" s="T320">Täp </ts>
               <ts e="T322" id="Seg_1960" n="e" s="T321">antoːqɨt </ts>
               <ts e="T323" id="Seg_1962" n="e" s="T322">orqɨlsɨtɨ </ts>
               <ts e="T324" id="Seg_1964" n="e" s="T323">loːsɨn </ts>
               <ts e="T325" id="Seg_1966" n="e" s="T324">nalʼaiːmtɨ, </ts>
               <ts e="T326" id="Seg_1968" n="e" s="T325">kartɨstɨ </ts>
               <ts e="T327" id="Seg_1970" n="e" s="T326">kɨt </ts>
               <ts e="T328" id="Seg_1972" n="e" s="T327">qanɨktɨ </ts>
               <ts e="T329" id="Seg_1974" n="e" s="T328">i </ts>
               <ts e="T330" id="Seg_1976" n="e" s="T329">loːsɨn </ts>
               <ts e="T331" id="Seg_1978" n="e" s="T330">nalʼap, </ts>
               <ts e="T332" id="Seg_1980" n="e" s="T331">okkɨr </ts>
               <ts e="T333" id="Seg_1982" n="e" s="T332">nʼeːmtɨ </ts>
               <ts e="T334" id="Seg_1984" n="e" s="T333">čʼelʼčʼɔːllä </ts>
               <ts e="T335" id="Seg_1986" n="e" s="T334">pissätɨ </ts>
               <ts e="T336" id="Seg_1988" n="e" s="T335">sʼöntɨ. </ts>
               <ts e="T337" id="Seg_1990" n="e" s="T336">Šintälelʼ </ts>
               <ts e="T338" id="Seg_1992" n="e" s="T337">nälʼamtɨ </ts>
               <ts e="T339" id="Seg_1994" n="e" s="T338">aj </ts>
               <ts e="T340" id="Seg_1996" n="e" s="T339">nılʼčʼik </ts>
               <ts e="T341" id="Seg_1998" n="e" s="T340">čʼattɨsɨtɨ </ts>
               <ts e="T342" id="Seg_2000" n="e" s="T341">i </ts>
               <ts e="T343" id="Seg_2002" n="e" s="T342">selʼtisɨtɨ. </ts>
               <ts e="T344" id="Seg_2004" n="e" s="T343">Mat </ts>
               <ts e="T345" id="Seg_2006" n="e" s="T344">čʼaptämɨ </ts>
               <ts e="T346" id="Seg_2008" n="e" s="T345">kutɨka </ts>
               <ts e="T347" id="Seg_2010" n="e" s="T346">üŋkɨltempɨsɨtɨ, </ts>
               <ts e="T348" id="Seg_2012" n="e" s="T347">meːltetqo </ts>
               <ts e="T349" id="Seg_2014" n="e" s="T348">apsɨtɨ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T1" id="Seg_2015" s="T0">KMI_1976_IchaInHawkClothing_flk.001 (001)</ta>
            <ta e="T4" id="Seg_2016" s="T1">KMI_1976_IchaInHawkClothing_flk.002 (002.001)</ta>
            <ta e="T10" id="Seg_2017" s="T4">KMI_1976_IchaInHawkClothing_flk.003 (002.002)</ta>
            <ta e="T17" id="Seg_2018" s="T10">KMI_1976_IchaInHawkClothing_flk.004 (002.003)</ta>
            <ta e="T20" id="Seg_2019" s="T17">KMI_1976_IchaInHawkClothing_flk.005 (002.004)</ta>
            <ta e="T26" id="Seg_2020" s="T20">KMI_1976_IchaInHawkClothing_flk.006 (002.005)</ta>
            <ta e="T33" id="Seg_2021" s="T26">KMI_1976_IchaInHawkClothing_flk.007 (002.006)</ta>
            <ta e="T39" id="Seg_2022" s="T33">KMI_1976_IchaInHawkClothing_flk.008 (002.007)</ta>
            <ta e="T44" id="Seg_2023" s="T39">KMI_1976_IchaInHawkClothing_flk.009 (002.008)</ta>
            <ta e="T52" id="Seg_2024" s="T44">KMI_1976_IchaInHawkClothing_flk.010 (002.009)</ta>
            <ta e="T57" id="Seg_2025" s="T52">KMI_1976_IchaInHawkClothing_flk.011 (002.010)</ta>
            <ta e="T61" id="Seg_2026" s="T57">KMI_1976_IchaInHawkClothing_flk.012 (002.011)</ta>
            <ta e="T66" id="Seg_2027" s="T61">KMI_1976_IchaInHawkClothing_flk.013 (002.012)</ta>
            <ta e="T68" id="Seg_2028" s="T66">KMI_1976_IchaInHawkClothing_flk.014 (002.013)</ta>
            <ta e="T77" id="Seg_2029" s="T68">KMI_1976_IchaInHawkClothing_flk.015 (002.014)</ta>
            <ta e="T80" id="Seg_2030" s="T77">KMI_1976_IchaInHawkClothing_flk.016 (002.015)</ta>
            <ta e="T85" id="Seg_2031" s="T80">KMI_1976_IchaInHawkClothing_flk.017 (002.016)</ta>
            <ta e="T88" id="Seg_2032" s="T85">KMI_1976_IchaInHawkClothing_flk.018 (002.017)</ta>
            <ta e="T92" id="Seg_2033" s="T88">KMI_1976_IchaInHawkClothing_flk.019 (002.018)</ta>
            <ta e="T97" id="Seg_2034" s="T92">KMI_1976_IchaInHawkClothing_flk.020 (002.019)</ta>
            <ta e="T101" id="Seg_2035" s="T97">KMI_1976_IchaInHawkClothing_flk.021 (002.020)</ta>
            <ta e="T104" id="Seg_2036" s="T101">KMI_1976_IchaInHawkClothing_flk.022 (002.021)</ta>
            <ta e="T112" id="Seg_2037" s="T104">KMI_1976_IchaInHawkClothing_flk.023 (003.001)</ta>
            <ta e="T121" id="Seg_2038" s="T112">KMI_1976_IchaInHawkClothing_flk.024 (003.002)</ta>
            <ta e="T124" id="Seg_2039" s="T121">KMI_1976_IchaInHawkClothing_flk.025 (003.003)</ta>
            <ta e="T131" id="Seg_2040" s="T124">KMI_1976_IchaInHawkClothing_flk.026 (003.004)</ta>
            <ta e="T135" id="Seg_2041" s="T131">KMI_1976_IchaInHawkClothing_flk.027 (003.005)</ta>
            <ta e="T139" id="Seg_2042" s="T135">KMI_1976_IchaInHawkClothing_flk.028 (003.006)</ta>
            <ta e="T142" id="Seg_2043" s="T139">KMI_1976_IchaInHawkClothing_flk.029 (003.007)</ta>
            <ta e="T144" id="Seg_2044" s="T142">KMI_1976_IchaInHawkClothing_flk.030 (003.008)</ta>
            <ta e="T148" id="Seg_2045" s="T144">KMI_1976_IchaInHawkClothing_flk.031 (003.009)</ta>
            <ta e="T150" id="Seg_2046" s="T148">KMI_1976_IchaInHawkClothing_flk.032 (003.010)</ta>
            <ta e="T155" id="Seg_2047" s="T150">KMI_1976_IchaInHawkClothing_flk.033 (003.011)</ta>
            <ta e="T164" id="Seg_2048" s="T155">KMI_1976_IchaInHawkClothing_flk.034 (003.012)</ta>
            <ta e="T168" id="Seg_2049" s="T164">KMI_1976_IchaInHawkClothing_flk.035 (003.013)</ta>
            <ta e="T173" id="Seg_2050" s="T168">KMI_1976_IchaInHawkClothing_flk.036 (003.014)</ta>
            <ta e="T178" id="Seg_2051" s="T173">KMI_1976_IchaInHawkClothing_flk.037 (003.015)</ta>
            <ta e="T180" id="Seg_2052" s="T178">KMI_1976_IchaInHawkClothing_flk.038 (003.016)</ta>
            <ta e="T187" id="Seg_2053" s="T180">KMI_1976_IchaInHawkClothing_flk.039 (003.017)</ta>
            <ta e="T191" id="Seg_2054" s="T187">KMI_1976_IchaInHawkClothing_flk.040 (003.018)</ta>
            <ta e="T195" id="Seg_2055" s="T191">KMI_1976_IchaInHawkClothing_flk.041 (003.019)</ta>
            <ta e="T200" id="Seg_2056" s="T195">KMI_1976_IchaInHawkClothing_flk.042 (003.020)</ta>
            <ta e="T203" id="Seg_2057" s="T200">KMI_1976_IchaInHawkClothing_flk.043 (003.021)</ta>
            <ta e="T208" id="Seg_2058" s="T203">KMI_1976_IchaInHawkClothing_flk.044 (003.022)</ta>
            <ta e="T213" id="Seg_2059" s="T208">KMI_1976_IchaInHawkClothing_flk.045 (003.023)</ta>
            <ta e="T223" id="Seg_2060" s="T213">KMI_1976_IchaInHawkClothing_flk.046 (003.024)</ta>
            <ta e="T231" id="Seg_2061" s="T223">KMI_1976_IchaInHawkClothing_flk.047 (004.001)</ta>
            <ta e="T233" id="Seg_2062" s="T231">KMI_1976_IchaInHawkClothing_flk.048 (004.002)</ta>
            <ta e="T238" id="Seg_2063" s="T233">KMI_1976_IchaInHawkClothing_flk.049 (004.003)</ta>
            <ta e="T243" id="Seg_2064" s="T238">KMI_1976_IchaInHawkClothing_flk.050 (004.004)</ta>
            <ta e="T245" id="Seg_2065" s="T243">KMI_1976_IchaInHawkClothing_flk.051 (004.005)</ta>
            <ta e="T256" id="Seg_2066" s="T245">KMI_1976_IchaInHawkClothing_flk.052 (004.006)</ta>
            <ta e="T269" id="Seg_2067" s="T256">KMI_1976_IchaInHawkClothing_flk.053 (004.007)</ta>
            <ta e="T280" id="Seg_2068" s="T269">KMI_1976_IchaInHawkClothing_flk.054 (004.008)</ta>
            <ta e="T292" id="Seg_2069" s="T280">KMI_1976_IchaInHawkClothing_flk.055 (004.009)</ta>
            <ta e="T297" id="Seg_2070" s="T292">KMI_1976_IchaInHawkClothing_flk.056 (004.010)</ta>
            <ta e="T306" id="Seg_2071" s="T297">KMI_1976_IchaInHawkClothing_flk.057 (004.011)</ta>
            <ta e="T310" id="Seg_2072" s="T306">KMI_1976_IchaInHawkClothing_flk.058 (004.012)</ta>
            <ta e="T315" id="Seg_2073" s="T310">KMI_1976_IchaInHawkClothing_flk.059 (004.013)</ta>
            <ta e="T320" id="Seg_2074" s="T315">KMI_1976_IchaInHawkClothing_flk.060 (004.014)</ta>
            <ta e="T336" id="Seg_2075" s="T320">KMI_1976_IchaInHawkClothing_flk.061 (004.015)</ta>
            <ta e="T343" id="Seg_2076" s="T336">KMI_1976_IchaInHawkClothing_flk.062 (004.016)</ta>
            <ta e="T349" id="Seg_2077" s="T343">KMI_1976_IchaInHawkClothing_flk.063 (004.017)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T1" id="Seg_2078" s="T0">и′че[а]к′ӣчика.</ta>
            <ta e="T4" id="Seg_2079" s="T1">′ӣlымба и′чакʼӣчиkа им′lандыса.</ta>
            <ta e="T10" id="Seg_2080" s="T4">и′чакӣчик ′kӓнна ма̄′чонты, kоңыты kуп ′пачетта.</ta>
            <ta e="T17" id="Seg_2081" s="T10">тӱңа тӓрычакты kоңыты ′мӓрыка lо̄с (lо̄зыl kуп).</ta>
            <ta e="T20" id="Seg_2082" s="T17">и′lча kой ′меттаl?</ta>
            <ta e="T26" id="Seg_2083" s="T20">ма поп ′пачертап lо̄зы kӓттыты [kӓттысыты].</ta>
            <ta e="T33" id="Seg_2084" s="T26">ма о′кот тап по̄п ′утсӓ ′шʼиты ‵нытколʼе̄как.</ta>
            <ta e="T39" id="Seg_2085" s="T33">куса кӓтсан шʼиты ныткалʼе̄т ку′са са′ңаты. </ta>
            <ta e="T44" id="Seg_2086" s="T39">lозыl иlʼ′чат по̄п шʼиты ′наkыlныты.</ta>
            <ta e="T52" id="Seg_2087" s="T44">ку[ъ]′са у′тоl са̄′каlд̂ы шʼиты ӯ′тоl то′kаlʼдеты ′наkkъlты кы′са.</ta>
            <ta e="T57" id="Seg_2088" s="T52">по орkылсыт ӯтемты шʼиты ′утъты.</ta>
            <ta e="T61" id="Seg_2089" s="T57">тӓм ′омта ′орты че̄нка.</ta>
            <ta e="T66" id="Seg_2090" s="T61">lо̄зы ′kӓтӓсыты ′омдазик и те̄′кʼесик.</ta>
            <ta e="T68" id="Seg_2091" s="T66">lо̄зы kə̄ссы.</ta>
            <ta e="T77" id="Seg_2092" s="T68">и′ча[е]к′е̨̄чика ′омдыса ′омдыса кунды чап кунды, kӓндык чап kӓндык.</ta>
            <ta e="T80" id="Seg_2093" s="T77">′тӓ̄нырпыса ′kӓндык ′тандеkо.</ta>
            <ta e="T85" id="Seg_2094" s="T80">kӓlтырса и kутарса [′эппа kӓтыты].</ta>
            <ta e="T88" id="Seg_2095" s="T85">′тӓбыни тӱ̄ңа kорkъ.</ta>
            <ta e="T92" id="Seg_2096" s="T88">′иlча по̄п ′сӣты нытkа[ы]lты.</ta>
            <ta e="T97" id="Seg_2097" s="T92">kорГъ сӣты ныт′kы[ə]lʼситы утыты ′тӣтеситы.</ta>
            <ta e="T101" id="Seg_2098" s="T97">и утътъ ′ӯбыl а̄lʼча[е]са.</ta>
            <ta e="T104" id="Seg_2099" s="T101">′танта ′по̄но̄ны.</ta>
            <ta e="T112" id="Seg_2100" s="T104">ку′ттарна, ′kӓlтерна, ′тӓ̄нырпа ′kӓ̄н[т]дык ′мӣта[ы,ъ]kо и ′kондерkо ′lо̄сып.</ta>
            <ta e="T121" id="Seg_2101" s="T112">kу′ттарна, kӓlимпа, тӓ̄нырпыт, ′куттар ′lосып мӣтӓkо, орГыlkо, ′орГыlӓ ′ӣlеlа.</ta>
            <ta e="T124" id="Seg_2102" s="T121">′тӓ̄нырпа куче ′kа̄теkо.</ta>
            <ta e="T131" id="Seg_2103" s="T124">о′lоɣынты ′аlʼча тӓ̄ныты нотна кар′манʼмы ′менымпыkо kорбытkо.</ta>
            <ta e="T135" id="Seg_2104" s="T131">кар′манʼkынты ′kо̄сыты ′мысʼ[щʼ]аlтукат kопы.</ta>
            <ta e="T139" id="Seg_2105" s="T135">′ныңыlʼӓ ′панытkолапсыт мы′сʼаlтукат kопы.</ta>
            <ta e="T142" id="Seg_2106" s="T139">панысыты ′мӓрkыlо̄ ӓсыса.</ta>
            <ta e="T144" id="Seg_2107" s="T142">то′kаl′дисты о′lокынты.</ta>
            <ta e="T148" id="Seg_2108" s="T144">ныны kо′тӓ то титесыты.</ta>
            <ta e="T150" id="Seg_2109" s="T148">′очик па̄′налсыты.</ta>
            <ta e="T155" id="Seg_2110" s="T150">ай ′очик тоkаlдисты. оlоɣынды къбоɣынды.</ta>
            <ta e="T164" id="Seg_2111" s="T155">нын ай то тисӓты нʼуɣонды ноны и оlонды но̄ны.</ta>
            <ta e="T168" id="Seg_2112" s="T164">ай ′па̄нысыты, ′па̄нысыты, ′па̄нысыты.</ta>
            <ta e="T173" id="Seg_2113" s="T168">ай ′тӓнырсы ай панытkо но̄тно.</ta>
            <ta e="T178" id="Seg_2114" s="T173">′па̄нысыты, ′па̄нысыты, ‎‎′па̄нысыты мы̄сʼаlтукат kопып.</ta>
            <ta e="T180" id="Seg_2115" s="T178">мӓрk ӓ̄са.</ta>
            <ta e="T187" id="Seg_2116" s="T180">тӣ то′kаlдис[ш]ыты мы̄сʼаlʼтукат kобып кӓ′боɣынды и ′нʼӯɣо ɣонды.</ta>
            <ta e="T191" id="Seg_2117" s="T187">мысʼаlтукат kобы ту′реситы тобоɣонды.</ta>
            <ta e="T195" id="Seg_2118" s="T191">ныны тӓп а̄са мысʼаlтукатkо.</ta>
            <ta e="T200" id="Seg_2119" s="T195">ныны мы′сʼсʼӓңа, ′омти ′то̄lо тонык.</ta>
            <ta e="T203" id="Seg_2120" s="T200">омд̂ыlʼа сиг′lаlдымба ′тумты.</ta>
            <ta e="T208" id="Seg_2121" s="T203">сиг′lадымбысыты тумты и мы′ссʼӓ̄са ′нумынʼа̄kты.</ta>
            <ta e="T213" id="Seg_2122" s="T208"> и нат ′пиндерса, ′коlембыса нумынʼа̄kыт.</ta>
            <ta e="T223" id="Seg_2123" s="T213">ныны ′мы′сʼсʼӓсʼа тимбеlӓ кып нʼодыkоlапсыты таkkылʼo или то ′тиlдʼара тимбеlʼа.</ta>
            <ta e="T231" id="Seg_2124" s="T223">оккыр чон′доkыт kоңыты кыт щʼундʼеkыт кыkыт анды kӓ̄нта.</ta>
            <ta e="T233" id="Seg_2125" s="T231">и′llаlо̄ ′ӱ̄теса.</ta>
            <ta e="T238" id="Seg_2126" s="T233">kоңыты lо̄сы ′ситы нӓlʼаиндыса kӓнтотыт.</ta>
            <ta e="T243" id="Seg_2127" s="T238">lосы kоңыты ′мысалʼтука ′коlʼемба о′lоɣынт.</ta>
            <ta e="T245" id="Seg_2128" s="T243">′ты[и]lдырlʼа ′коlʼемпа.</ta>
            <ta e="T256" id="Seg_2129" s="T245">ой, нӓlʼа ′маннымбет ку′ттар ′нӓkырыl сурут нӓ′ка суруруп тиlдерна ме ′по̄рыɣыныт.</ta>
            <ta e="T269" id="Seg_2130" s="T256">lосы ′сумпа на′lʼӓиɣынты нӓ′lа̄ нʼа′lа ′менымбаты ке̄й ме пороkыны сурут ку′ттар сӯруп тинты.</ta>
            <ta e="T280" id="Seg_2131" s="T269">мы′сʼсʼаlтука мы′ссӓlӓ kъ̊̄сса тӓбын у′конты кынты марГы тӱмонты кыт kа̄ноkыт ′омта.</ta>
            <ta e="T292" id="Seg_2132" s="T280">ныны шʼиты тӱ̄мыт по̄рынты нʼанды нʼаkkылʼӓ [нʼаkkысыты] ′ытысыты ′чесын кыт сӱ′ннʼенты [cӱнтʼӧнты].</ta>
            <ta e="T297" id="Seg_2133" s="T292">lо̄сы тӱнта ′сумбыlа шʼиты на′лʼäт.</ta>
            <ta e="T306" id="Seg_2134" s="T297">тӱ̄са, чесын kӓнта андыса тӱ̄са, терычаkты че′сынты тӱ̄са андыса.</ta>
            <ta e="T310" id="Seg_2135" s="T306">lо̄зы kо̄нды̄ртӓтты че̄нка ′чесынап.</ta>
            <ta e="T315" id="Seg_2136" s="T310">чесын ′соlʼмынты мищʼалʼш[c]иты шʼиты тӱ̄ймы.</ta>
            <ta e="T320" id="Seg_2137" s="T315">lо̄зы kуптӓ̄ты ′аза тӓ̄нымыты [kумпа].</ta>
            <ta e="T336" id="Seg_2138" s="T320">тӓп андоɣыт орГыlсыты lо̄зын на′lаимты ′kартысты кытkаныкты и lозын на′lʼап оккырнʼемты чеl′чоlӓ ′пӣсӓты сʼӧ̄нты.</ta>
            <ta e="T343" id="Seg_2139" s="T336">шʼиндеlе нӓ′лʼамды ай ниlчик ′чаттысыты и ′селʼдисыты.</ta>
            <ta e="T349" id="Seg_2140" s="T343">мат чап′тӓмы ′кутыка ′ӱңгыlдембы сыты, меlдетkо апсыты.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T1" id="Seg_2141" s="T0">iče[a]kiːčika.</ta>
            <ta e="T4" id="Seg_2142" s="T1">iːlʼɨmpa ičakʼiːčiqa imlʼandɨsa.</ta>
            <ta e="T10" id="Seg_2143" s="T4">ičakiːčik qänna maːčontɨ, qoŋɨtɨ qup pačetta.</ta>
            <ta e="T17" id="Seg_2144" s="T10">tüŋa tärɨčaktɨ qoŋɨtɨ märɨka lʼoːs (lʼoːzɨlʼ qup).</ta>
            <ta e="T20" id="Seg_2145" s="T17">ilʼča qoj mettalʼ?</ta>
            <ta e="T26" id="Seg_2146" s="T20">ma pop pačertap ‎‎lʼoːzɨ qättɨtɨ [qättɨsɨtɨ].</ta>
            <ta e="T33" id="Seg_2147" s="T26">ma okot tap poːp utsä šitɨ nɨtkolʼeːkak.</ta>
            <ta e="T39" id="Seg_2148" s="T33">kusa kätsan šitɨ nɨtkalʼeːt kusa saŋatɨ. </ta>
            <ta e="T44" id="Seg_2149" s="T39">lʼozɨlʼ ilʼʼčat poːp šitɨ naqɨlʼnɨtɨ.</ta>
            <ta e="T52" id="Seg_2150" s="T44">ku[ə]sa utolʼ saːkalʼd̂ɨ šitɨ uːtolʼ toqalʼʼdetɨ naqqəlʼtɨ kɨsa.</ta>
            <ta e="T57" id="Seg_2151" s="T52">po orqɨlsɨt uːtemtɨ šitɨ utətɨ.</ta>
            <ta e="T61" id="Seg_2152" s="T57">täm omta ortɨ čeːnka.</ta>
            <ta e="T66" id="Seg_2153" s="T61">lʼoːzɨ qätäsɨtɨ omdazik i teːkʼesik.</ta>
            <ta e="T68" id="Seg_2154" s="T66">lʼoːzɨ qəːssɨ.</ta>
            <ta e="T77" id="Seg_2155" s="T68">iča[e]keːčika omdɨsa omdɨsa kundɨ čap kundɨ, qändɨk čap qändɨk.</ta>
            <ta e="T80" id="Seg_2156" s="T77">täːnɨrpɨsa qändɨk tandeqo.</ta>
            <ta e="T85" id="Seg_2157" s="T80">qälʼtɨrsa i qutarsa [ɛppa qätɨtɨ].</ta>
            <ta e="T88" id="Seg_2158" s="T85">täpɨni tüːŋa qorqə.</ta>
            <ta e="T92" id="Seg_2159" s="T88">ilʼča poːp siːtɨ nɨtqa[ɨ]lʼtɨ.</ta>
            <ta e="T97" id="Seg_2160" s="T92">qorГə siːtɨ nɨtqɨ[ə]lʼʼsitɨ utɨtɨ tiːtesitɨ.</ta>
            <ta e="T101" id="Seg_2161" s="T97">i utətə uːpɨlʼ aːlʼʼča[e]sa.</ta>
            <ta e="T104" id="Seg_2162" s="T101">tanta poːnoːnɨ.</ta>
            <ta e="T112" id="Seg_2163" s="T104">kuttarna, qälʼterna, täːnɨrpa qäːn[t]dɨk miːta[ɨ,ə]qo i qonderqo lʼoːsɨp.</ta>
            <ta e="T121" id="Seg_2164" s="T112">quttarna, qälʼimpa, täːnɨrpɨt, kuttar lʼosɨp miːtäqo, orГɨlʼqo, orГɨlʼä iːlʼelʼa.</ta>
            <ta e="T124" id="Seg_2165" s="T121">täːnɨrpa kuče qaːteqo.</ta>
            <ta e="T131" id="Seg_2166" s="T124">olʼoqɨntɨ alʼʼča täːnɨtɨ notna karmanʼmɨ menɨmpɨqo qorpɨtqo.</ta>
            <ta e="T135" id="Seg_2167" s="T131">karmanʼqɨntɨ qoːsɨtɨ mɨsʼ[šʼ]alʼtukat qopɨ.</ta>
            <ta e="T139" id="Seg_2168" s="T135">nɨŋɨlʼʼä panɨtqolapsɨt mɨsʼalʼtukat qopɨ.</ta>
            <ta e="T142" id="Seg_2169" s="T139">panɨsɨtɨ märqɨlʼoː äsɨsa.</ta>
            <ta e="T144" id="Seg_2170" s="T142">toqalʼdistɨ olʼokɨntɨ.</ta>
            <ta e="T148" id="Seg_2171" s="T144">nɨnɨ qotä to titesɨtɨ.</ta>
            <ta e="T150" id="Seg_2172" s="T148">očik paːnalsɨtɨ.</ta>
            <ta e="T155" id="Seg_2173" s="T150">aj očik toqalʼdistɨ. olʼoqɨndɨ kəpoqɨndɨ.</ta>
            <ta e="T164" id="Seg_2174" s="T155">nɨn aj to tisätɨ nʼuqondɨ nonɨ i olʼondɨ noːnɨ.</ta>
            <ta e="T168" id="Seg_2175" s="T164">aj paːnɨsɨtɨ, paːnɨsɨtɨ, paːnɨsɨtɨ.</ta>
            <ta e="T173" id="Seg_2176" s="T168">aj tänɨrsɨ aj panɨtqo noːtno.</ta>
            <ta e="T178" id="Seg_2177" s="T173">paːnɨsɨtɨ, paːnɨsɨtɨ, paːnɨsɨtɨ mɨːsʼalʼtukat qopɨp.</ta>
            <ta e="T180" id="Seg_2178" s="T178">märq äːsa.</ta>
            <ta e="T187" id="Seg_2179" s="T180">tiː toqalʼdis[š]ɨtɨ mɨːsʼalʼʼtukat qopɨp käpoqɨndɨ i nʼuːqo qondɨ.</ta>
            <ta e="T191" id="Seg_2180" s="T187">mɨsʼalʼtukat qopɨ turesitɨ topoqondɨ.</ta>
            <ta e="T195" id="Seg_2181" s="T191">nɨnɨ täp aːsa mɨsʼalʼtukatqo.</ta>
            <ta e="T200" id="Seg_2182" s="T195">nɨnɨ mɨsʼsʼäŋa, omti toːlʼo tonɨk.</ta>
            <ta e="T203" id="Seg_2183" s="T200">omd̂ɨlʼʼa siglʼalʼdɨmpa tumtɨ.</ta>
            <ta e="T208" id="Seg_2184" s="T203">siglʼadɨmpɨsɨtɨ tumtɨ i mɨssʼäːsa numɨnʼaːqtɨ.</ta>
            <ta e="T213" id="Seg_2185" s="T208"> i nat pindersa, kolʼempɨsa numɨnʼaːqɨt.</ta>
            <ta e="T223" id="Seg_2186" s="T213">nɨnɨ mɨsʼsʼäsʼa timpelʼä kɨp nʼodɨqolʼapsɨtɨ taqqɨlʼo ili to tilʼdʼara timpelʼʼa.</ta>
            <ta e="T231" id="Seg_2187" s="T223">okkɨr čondoqɨt qoŋɨtɨ kɨt šʼundʼeqɨt kɨqɨt andɨ qäːnta.</ta>
            <ta e="T233" id="Seg_2188" s="T231">ilʼlʼalʼoː üːtesa.</ta>
            <ta e="T238" id="Seg_2189" s="T233">qoŋɨtɨ lʼoːsɨ sitɨ nälʼʼaindɨsa qäntotɨt.</ta>
            <ta e="T243" id="Seg_2190" s="T238">lʼosɨ qoŋɨtɨ mɨsalʼtuka kolʼʼempa olʼoqɨnt.</ta>
            <ta e="T245" id="Seg_2191" s="T243">tɨ[i]lʼdɨrlʼʼa kolʼʼempa.</ta>
            <ta e="T256" id="Seg_2192" s="T245">oj, nälʼʼa mannɨmpet kuttar näqɨrɨlʼ surut näka sururup tilʼderna me poːrɨqɨnɨt.</ta>
            <ta e="T269" id="Seg_2193" s="T256">lʼosɨ sumpa nalʼʼäiqɨntɨ nälʼaː nʼalʼa menɨmpatɨ keːj me poroqɨnɨ surut kuttar suːrup tintɨ.</ta>
            <ta e="T280" id="Seg_2194" s="T269">mɨsʼsʼalʼtuka mɨssälʼä qəːssa täpɨn ukontɨ kɨntɨ marГɨ tümontɨ kɨt qaːnoqɨt omta.</ta>
            <ta e="T292" id="Seg_2195" s="T280">nɨnɨ šitɨ tüːmɨt poːrɨntɨ nʼandɨ nʼaqqɨlʼä [nʼaqqɨsɨtɨ] ɨtɨsɨtɨ česɨn kɨt sünnʼentɨ [sünčöntɨ].</ta>
            <ta e="T297" id="Seg_2196" s="T292">lʼoːsɨ tünta sumpɨlʼa šitɨ nalʼät.</ta>
            <ta e="T306" id="Seg_2197" s="T297">tüːsa, česɨn qänta andɨsa tüːsa, terɨčaqtɨ česɨntɨ tüːsa andɨsa.</ta>
            <ta e="T310" id="Seg_2198" s="T306">lʼoːzɨ qoːndɨːrtättɨ čeːnka česɨnap.</ta>
            <ta e="T315" id="Seg_2199" s="T310">česɨn solʼmɨntɨ mišʼalʼš[s]itɨ šitɨ tüːjmɨ.</ta>
            <ta e="T320" id="Seg_2200" s="T315">lʼoːzɨ quptäːtɨ aza täːnɨmɨtɨ [qumpa].</ta>
            <ta e="T336" id="Seg_2201" s="T320">täp andoqɨt orГɨlʼsɨtɨ lʼoːzɨn nalʼaimtɨ qartɨstɨ kɨtqanɨktɨ i lʼozɨn nalʼʼap okkɨrnʼemtɨ čelʼčolʼä piːsätɨ sʼöːntɨ.</ta>
            <ta e="T343" id="Seg_2202" s="T336">šindelʼe nälʼamdɨ aj nilʼčik čattɨsɨtɨ i selʼdisɨtɨ.</ta>
            <ta e="T349" id="Seg_2203" s="T343">mat čaptämɨ kutɨka üŋgɨlʼdempɨ sɨtɨ, melʼdetqo apsɨtɨ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T1" id="Seg_2204" s="T0">Ičʼakiːčʼika. </ta>
            <ta e="T4" id="Seg_2205" s="T1">Ilɨmpa Ičʼakʼiːčʼika imlʼantɨsa. </ta>
            <ta e="T10" id="Seg_2206" s="T4">Ičʼakiːčʼik qänna mačʼontɨ, qoŋɨtɨ qup pačʼetta. </ta>
            <ta e="T17" id="Seg_2207" s="T10">Tüŋa tättɨčʼaktɨ, qoŋɨtɨ märɨqa loːs (/loːsɨlʼ qup). </ta>
            <ta e="T20" id="Seg_2208" s="T17">Ilʼčʼa, qoj meːttal? </ta>
            <ta e="T26" id="Seg_2209" s="T20">Ma poːp pačʼertap– loːsɨ kättɨtɨ (/kättɨsɨtɨ). </ta>
            <ta e="T33" id="Seg_2210" s="T26">Ma okoːt tap poːp utsä šitɨ nɨtqolʼlʼɛːkkak. </ta>
            <ta e="T39" id="Seg_2211" s="T33">Kusa, kätsan, šitɨ nɨtqalʼät, kusa saŋatɨ! </ta>
            <ta e="T44" id="Seg_2212" s="T39">Loːsɨlʼ ilʼčʼat poːp šitɨ nɨtqɨlnɨtɨ. </ta>
            <ta e="T52" id="Seg_2213" s="T44">Kusa utol saqaltɨ, šitɨ utol tokalʼtätɨ, naqqəltɨ kɨsa! </ta>
            <ta e="T57" id="Seg_2214" s="T52">Poː orqɨlsɨt utemtɨ, šitɨ utətɨ. </ta>
            <ta e="T61" id="Seg_2215" s="T57">Täm ɔːmta, ortɨ čʼäːŋka. </ta>
            <ta e="T66" id="Seg_2216" s="T61">Loːsɨ kätäsɨtɨ: Ɔːmtasik i teːkäsik. </ta>
            <ta e="T68" id="Seg_2217" s="T66">Loːsɨ qəssɨ. </ta>
            <ta e="T77" id="Seg_2218" s="T68">Ičʼakɛːčʼika ɔːmtɨsa, ɔːmtɨsa, kuntɨ čʼap kuntɨ, qäntɨk čʼap qäntɨk. </ta>
            <ta e="T80" id="Seg_2219" s="T77">Tɛnɨrpɨsa qäntɨk tanteqo. </ta>
            <ta e="T85" id="Seg_2220" s="T80">Qältɨrsa i kutarsa, ɛːppa qätɨtɨ. </ta>
            <ta e="T88" id="Seg_2221" s="T85">Täpɨni tüŋa qorqə. </ta>
            <ta e="T92" id="Seg_2222" s="T88">Ilʼčʼa, poːp sitɨ nɨtqɨltɨ. </ta>
            <ta e="T97" id="Seg_2223" s="T92">Qorqə sitɨ nɨtqɨlʼsitɨ, utɨtɨ tıːtesitɨ. </ta>
            <ta e="T101" id="Seg_2224" s="T97">I utətə uːpɨl alʼčʼasa. </ta>
            <ta e="T104" id="Seg_2225" s="T101">Tanta poː nɔːnɨ. </ta>
            <ta e="T112" id="Seg_2226" s="T104">Kuttarna, qälterna, tɛnɨrpa qäntɨk mitɨqo i qonterqo loːsɨp. </ta>
            <ta e="T121" id="Seg_2227" s="T112">Kuttarna, qälimpa, tɛnɨrpɨt, kuttar loːsɨp mitäqo, orqɨlqo, orqɨllä iːlela. </ta>
            <ta e="T124" id="Seg_2228" s="T121">Tɛnɨrpa, kučʼä qateqo. </ta>
            <ta e="T131" id="Seg_2229" s="T124">Oloːqɨntɨ alʼčʼa tɛnɨtɨ: Nɔːtna karmanʼmɨ mennɨmpɨqo, korpɨtqo! </ta>
            <ta e="T135" id="Seg_2230" s="T131">Karmanʼqɨntɨ qosɨtɨ mɨšʼaltukat qopɨ. </ta>
            <ta e="T139" id="Seg_2231" s="T135">Nɨŋɨlʼä panɨtqolapsɨt mɨšʼaltukat qopɨ. </ta>
            <ta e="T142" id="Seg_2232" s="T139">Panɨsɨtɨ, märqɨlɔː ɛsɨsa. </ta>
            <ta e="T144" id="Seg_2233" s="T142">Tokaltistɨ oloːqɨntɨ. </ta>
            <ta e="T148" id="Seg_2234" s="T144">Nɨːnɨ qotä toː tıːtesɨtɨ. </ta>
            <ta e="T150" id="Seg_2235" s="T148">Očʼik panalsɨtɨ. </ta>
            <ta e="T155" id="Seg_2236" s="T150">Aj očʼik tokaltistɨ oloːqɨntɨ, kəpoːqɨntɨ. </ta>
            <ta e="T164" id="Seg_2237" s="T155">Nɨːn aj toː tıːsätɨ nʼuːqontɨ nɔːnɨ i olontɨ nɔːnɨ. </ta>
            <ta e="T168" id="Seg_2238" s="T164">Aj panɨsɨtɨ, panɨsɨtɨ, panɨsɨtɨ. </ta>
            <ta e="T173" id="Seg_2239" s="T168">Aj tɛnɨrsɨ: aj panɨtqo nɔːtno. </ta>
            <ta e="T178" id="Seg_2240" s="T173">Panɨsɨtɨ, panɨsɨtɨ, panɨsɨtɨ mɨšʼaltukat qopɨp. </ta>
            <ta e="T180" id="Seg_2241" s="T178">Märq ɛsa. </ta>
            <ta e="T187" id="Seg_2242" s="T180">Tiː tokaltisɨtɨ mɨšʼaltukat qopɨp käpoːqɨntɨ i nʼuːqoːqontɨ. </ta>
            <ta e="T191" id="Seg_2243" s="T187">Mɨšʼaltukat qopɨ tuːresitɨ topoːqontɨ. </ta>
            <ta e="T195" id="Seg_2244" s="T191">Nɨːnɨ täp ɛsa mɨšʼaltukatqo. </ta>
            <ta e="T200" id="Seg_2245" s="T195">Nɨːnɨ mɨssɛːŋa, omti toːlɔː toːnɨk. </ta>
            <ta e="T203" id="Seg_2246" s="T200">Ɔːmtɨlʼa siqlaltɨmpa tuːmtɨ. </ta>
            <ta e="T208" id="Seg_2247" s="T203">Siqlaltɨmpɨsɨtɨ tuːmtɨ i mɨssɛːsa num ınʼnʼaːqɨt. </ta>
            <ta e="T213" id="Seg_2248" s="T208">I nat piːntersa, kolʼempɨsa num ınʼnʼaːqɨt. </ta>
            <ta e="T223" id="Seg_2249" s="T213">Nɨːnɨ mɨsʼsʼɛːsʼa, tıːmpelä kɨp nʼoːtɨqolapsɨtɨ takkɨlʼɔː ilʼi toː tılʼtʼara tıːmpelʼa. </ta>
            <ta e="T231" id="Seg_2250" s="T223">Okkɨr čʼontoːqɨt qoŋɨtɨ kɨt šünčʼeːqɨt kɨːqɨt antɨ qänta. </ta>
            <ta e="T233" id="Seg_2251" s="T231">İllalɔː üːtesa. </ta>
            <ta e="T238" id="Seg_2252" s="T233">Qoŋɨtɨ loːsɨ sitɨ nälʼaiːntɨsa qäntɔːtɨt. </ta>
            <ta e="T243" id="Seg_2253" s="T238">Loːsɨ qoŋɨtɨ mɨsalʼtuka kolʼempa oloːqɨnt. </ta>
            <ta e="T245" id="Seg_2254" s="T243">Tılʼtɨrlʼa kolʼempa. </ta>
            <ta e="T256" id="Seg_2255" s="T245">Oj, nälʼa, mannɨmpät kuttar näkɨrɨlʼ, suːrut näka, suːrup tılʼterna meː pɔːrɨqɨnɨt! </ta>
            <ta e="T269" id="Seg_2256" s="T256">Loːsɨ suːmpa nalʼäiːqɨntɨ: Nälʼa, nʼalʼa, mennɨmpatɨ, qej meː pɔːroːqɨnɨt suːrut, kuttar suːrup tıːntɨ. </ta>
            <ta e="T280" id="Seg_2257" s="T269">Mɨsʼsʼaltuka mɨssɛːlä qəssa täpɨn ukoːntɨ kɨntɨ, marqɨ tümontɨ kɨt qanoːqɨt omta. </ta>
            <ta e="T292" id="Seg_2258" s="T280">Nɨːnɨ šitɨ tümɨt pɔːrɨntɨ nʼantɨ nʼaqqɨlʼlʼä (/nʼakkɨsɨtɨ) ɨːtɨsɨtɨ čʼesɨn kɨt sünnʼentɨ (/sünčʼöntɨ). </ta>
            <ta e="T297" id="Seg_2259" s="T292">Loːsɨ tünta suːmpɨla, šitɨ nalʼat. </ta>
            <ta e="T306" id="Seg_2260" s="T297">Tüsa, čʼesɨn qänta antɨsa tüsa, tettɨčʼaktɨ čʼesɨntɨ tüsa antɨsa. </ta>
            <ta e="T310" id="Seg_2261" s="T306">Loːsɨ qontɨrtättɨ čʼäːŋka čʼesɨnap. </ta>
            <ta e="T315" id="Seg_2262" s="T310">Čʼesɨn soːlʼmɨntɨ mišalsitɨ šitɨ tüjmɨ. </ta>
            <ta e="T320" id="Seg_2263" s="T315">Loːsɨ quptäːtɨ asa tɛnɨmɨtɨ (/qumpa). </ta>
            <ta e="T336" id="Seg_2264" s="T320">Täp antoːqɨt orqɨlsɨtɨ loːsɨn nalʼaiːmtɨ, kartɨstɨ kɨt qanɨktɨ i loːsɨn nalʼap, okkɨr nʼeːmtɨ čʼelʼčʼɔːllä pissätɨ sʼöntɨ. </ta>
            <ta e="T343" id="Seg_2265" s="T336">Šintälelʼ nälʼamtɨ aj nılʼčʼik čʼattɨsɨtɨ i selʼtisɨtɨ. </ta>
            <ta e="T349" id="Seg_2266" s="T343">Mat čʼaptämɨ kutɨka üŋkɨltempɨsɨtɨ, meːltetqo apsɨtɨ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_2267" s="T0">Ičʼakiːčʼika</ta>
            <ta e="T2" id="Seg_2268" s="T1">ilɨ-mpa</ta>
            <ta e="T3" id="Seg_2269" s="T2">Ičʼakʼiːčʼika</ta>
            <ta e="T4" id="Seg_2270" s="T3">imlʼa-ntɨ-sa</ta>
            <ta e="T5" id="Seg_2271" s="T4">Ičʼakiːčʼik</ta>
            <ta e="T6" id="Seg_2272" s="T5">qän-na</ta>
            <ta e="T7" id="Seg_2273" s="T6">mačʼo-ntɨ</ta>
            <ta e="T8" id="Seg_2274" s="T7">qo-ŋɨ-tɨ</ta>
            <ta e="T9" id="Seg_2275" s="T8">qup</ta>
            <ta e="T10" id="Seg_2276" s="T9">pačʼe-tta</ta>
            <ta e="T11" id="Seg_2277" s="T10">tü-ŋa</ta>
            <ta e="T12" id="Seg_2278" s="T11">tättɨčʼa-k-tɨ</ta>
            <ta e="T13" id="Seg_2279" s="T12">qo-ŋɨ-tɨ</ta>
            <ta e="T14" id="Seg_2280" s="T13">märɨqa</ta>
            <ta e="T15" id="Seg_2281" s="T14">loːs</ta>
            <ta e="T16" id="Seg_2282" s="T15">loːsɨ-lʼ</ta>
            <ta e="T17" id="Seg_2283" s="T16">qup</ta>
            <ta e="T18" id="Seg_2284" s="T17">ilʼčʼa</ta>
            <ta e="T19" id="Seg_2285" s="T18">qoj</ta>
            <ta e="T20" id="Seg_2286" s="T19">meː-tta-l</ta>
            <ta e="T21" id="Seg_2287" s="T20">ma</ta>
            <ta e="T22" id="Seg_2288" s="T21">poː-p</ta>
            <ta e="T23" id="Seg_2289" s="T22">pačʼe-r-ta-p</ta>
            <ta e="T24" id="Seg_2290" s="T23">loːsɨ</ta>
            <ta e="T25" id="Seg_2291" s="T24">kättɨ-tɨ</ta>
            <ta e="T26" id="Seg_2292" s="T25">kättɨ-sɨ-tɨ</ta>
            <ta e="T27" id="Seg_2293" s="T26">ma</ta>
            <ta e="T28" id="Seg_2294" s="T27">okoːt</ta>
            <ta e="T29" id="Seg_2295" s="T28">tap</ta>
            <ta e="T30" id="Seg_2296" s="T29">poː-p</ta>
            <ta e="T31" id="Seg_2297" s="T30">ut-sä</ta>
            <ta e="T32" id="Seg_2298" s="T31">šitɨ</ta>
            <ta e="T33" id="Seg_2299" s="T32">nɨt-qolʼlʼ-ɛː-kka-k</ta>
            <ta e="T34" id="Seg_2300" s="T33">kusa</ta>
            <ta e="T35" id="Seg_2301" s="T34">kätsan</ta>
            <ta e="T36" id="Seg_2302" s="T35">šitɨ</ta>
            <ta e="T37" id="Seg_2303" s="T36">nɨt-qalʼ-ät</ta>
            <ta e="T38" id="Seg_2304" s="T37">kusa</ta>
            <ta e="T39" id="Seg_2305" s="T38">saŋ-atɨ</ta>
            <ta e="T40" id="Seg_2306" s="T39">loːsɨ-lʼ</ta>
            <ta e="T41" id="Seg_2307" s="T40">ilʼčʼa-t</ta>
            <ta e="T42" id="Seg_2308" s="T41">poː-p</ta>
            <ta e="T43" id="Seg_2309" s="T42">šitɨ</ta>
            <ta e="T44" id="Seg_2310" s="T43">nɨt-qɨl-nɨ-tɨ</ta>
            <ta e="T45" id="Seg_2311" s="T44">kusa</ta>
            <ta e="T46" id="Seg_2312" s="T45">uto-l</ta>
            <ta e="T47" id="Seg_2313" s="T46">saqal-tɨ</ta>
            <ta e="T48" id="Seg_2314" s="T47">šitɨ</ta>
            <ta e="T49" id="Seg_2315" s="T48">uto-l</ta>
            <ta e="T50" id="Seg_2316" s="T49">tok-alʼt-ätɨ</ta>
            <ta e="T51" id="Seg_2317" s="T50">naqqə-l-tɨ</ta>
            <ta e="T52" id="Seg_2318" s="T51">kɨsa</ta>
            <ta e="T53" id="Seg_2319" s="T52">poː</ta>
            <ta e="T54" id="Seg_2320" s="T53">orqɨl-sɨ-t</ta>
            <ta e="T55" id="Seg_2321" s="T54">ute-m-tɨ</ta>
            <ta e="T56" id="Seg_2322" s="T55">šitɨ</ta>
            <ta e="T57" id="Seg_2323" s="T56">utə-tɨ</ta>
            <ta e="T58" id="Seg_2324" s="T57">täm</ta>
            <ta e="T59" id="Seg_2325" s="T58">ɔːmta</ta>
            <ta e="T60" id="Seg_2326" s="T59">or-tɨ</ta>
            <ta e="T61" id="Seg_2327" s="T60">čʼäːŋka</ta>
            <ta e="T62" id="Seg_2328" s="T61">loːsɨ</ta>
            <ta e="T63" id="Seg_2329" s="T62">kätä-sɨ-tɨ</ta>
            <ta e="T64" id="Seg_2330" s="T63">ɔːmt-asik</ta>
            <ta e="T65" id="Seg_2331" s="T64">i</ta>
            <ta e="T66" id="Seg_2332" s="T65">teːk-äsik</ta>
            <ta e="T67" id="Seg_2333" s="T66">loːsɨ</ta>
            <ta e="T68" id="Seg_2334" s="T67">qəs-sɨ</ta>
            <ta e="T69" id="Seg_2335" s="T68">Ičʼakɛːčʼika</ta>
            <ta e="T70" id="Seg_2336" s="T69">ɔːmtɨ-sa</ta>
            <ta e="T71" id="Seg_2337" s="T70">ɔːmtɨ-sa</ta>
            <ta e="T72" id="Seg_2338" s="T71">kuntɨ</ta>
            <ta e="T73" id="Seg_2339" s="T72">čʼap</ta>
            <ta e="T74" id="Seg_2340" s="T73">kuntɨ</ta>
            <ta e="T75" id="Seg_2341" s="T74">qäntɨk</ta>
            <ta e="T76" id="Seg_2342" s="T75">čʼap</ta>
            <ta e="T77" id="Seg_2343" s="T76">qäntɨk</ta>
            <ta e="T78" id="Seg_2344" s="T77">tɛnɨ-r-pɨ-sa</ta>
            <ta e="T79" id="Seg_2345" s="T78">qäntɨk</ta>
            <ta e="T80" id="Seg_2346" s="T79">tante-qo</ta>
            <ta e="T81" id="Seg_2347" s="T80">qäl-tɨr-sa</ta>
            <ta e="T82" id="Seg_2348" s="T81">i</ta>
            <ta e="T83" id="Seg_2349" s="T82">kuta-r-sa</ta>
            <ta e="T84" id="Seg_2350" s="T83">ɛː-ppa</ta>
            <ta e="T85" id="Seg_2351" s="T84">qätɨ-tɨ</ta>
            <ta e="T86" id="Seg_2352" s="T85">täp-ɨ-ni</ta>
            <ta e="T87" id="Seg_2353" s="T86">tü-ŋa</ta>
            <ta e="T88" id="Seg_2354" s="T87">qorqə</ta>
            <ta e="T89" id="Seg_2355" s="T88">ilʼčʼa</ta>
            <ta e="T90" id="Seg_2356" s="T89">poː-p</ta>
            <ta e="T91" id="Seg_2357" s="T90">sitɨ</ta>
            <ta e="T92" id="Seg_2358" s="T91">nɨt-qɨl-tɨ</ta>
            <ta e="T93" id="Seg_2359" s="T92">qorqə</ta>
            <ta e="T94" id="Seg_2360" s="T93">sitɨ</ta>
            <ta e="T95" id="Seg_2361" s="T94">nɨt-qɨlʼ-si-tɨ</ta>
            <ta e="T96" id="Seg_2362" s="T95">utɨ-tɨ</ta>
            <ta e="T97" id="Seg_2363" s="T96">tıː-te-si-tɨ</ta>
            <ta e="T98" id="Seg_2364" s="T97">i</ta>
            <ta e="T99" id="Seg_2365" s="T98">utə-tə</ta>
            <ta e="T100" id="Seg_2366" s="T99">uːpɨl</ta>
            <ta e="T101" id="Seg_2367" s="T100">alʼčʼa-sa</ta>
            <ta e="T102" id="Seg_2368" s="T101">tanta</ta>
            <ta e="T103" id="Seg_2369" s="T102">poː</ta>
            <ta e="T104" id="Seg_2370" s="T103">nɔː-nɨ</ta>
            <ta e="T105" id="Seg_2371" s="T104">kutta-r-na</ta>
            <ta e="T106" id="Seg_2372" s="T105">qäl-ter-na</ta>
            <ta e="T107" id="Seg_2373" s="T106">tɛnɨ-r-pa</ta>
            <ta e="T108" id="Seg_2374" s="T107">qäntɨk</ta>
            <ta e="T109" id="Seg_2375" s="T108">mitɨ-qo</ta>
            <ta e="T110" id="Seg_2376" s="T109">i</ta>
            <ta e="T111" id="Seg_2377" s="T110">qo-nter-qo</ta>
            <ta e="T112" id="Seg_2378" s="T111">loːsɨ-p</ta>
            <ta e="T113" id="Seg_2379" s="T112">kutta-r-na</ta>
            <ta e="T114" id="Seg_2380" s="T113">qäl-i-mpa</ta>
            <ta e="T115" id="Seg_2381" s="T114">tɛnɨ-r-pɨ-t</ta>
            <ta e="T116" id="Seg_2382" s="T115">kuttar</ta>
            <ta e="T117" id="Seg_2383" s="T116">loːsɨ-p</ta>
            <ta e="T118" id="Seg_2384" s="T117">mitä-qo</ta>
            <ta e="T119" id="Seg_2385" s="T118">orqɨl-qo</ta>
            <ta e="T120" id="Seg_2386" s="T119">orqɨl-lä</ta>
            <ta e="T121" id="Seg_2387" s="T120">iː-le-la</ta>
            <ta e="T122" id="Seg_2388" s="T121">tɛnɨ-r-pa</ta>
            <ta e="T123" id="Seg_2389" s="T122">kutʼä</ta>
            <ta e="T124" id="Seg_2390" s="T123">qate-qo</ta>
            <ta e="T125" id="Seg_2391" s="T124">oloː-qɨn-tɨ</ta>
            <ta e="T126" id="Seg_2392" s="T125">alʼčʼa</ta>
            <ta e="T127" id="Seg_2393" s="T126">tɛnɨ-tɨ</ta>
            <ta e="T128" id="Seg_2394" s="T127">nɔːtna</ta>
            <ta e="T129" id="Seg_2395" s="T128">karmanʼ-mɨ</ta>
            <ta e="T130" id="Seg_2396" s="T129">mennɨ-mpɨ-qo</ta>
            <ta e="T131" id="Seg_2397" s="T130">korpɨt-qo</ta>
            <ta e="T132" id="Seg_2398" s="T131">karmanʼ-qɨn-tɨ</ta>
            <ta e="T133" id="Seg_2399" s="T132">qo-sɨ-tɨ</ta>
            <ta e="T134" id="Seg_2400" s="T133">mɨšʼaltuka-t</ta>
            <ta e="T135" id="Seg_2401" s="T134">qopɨ</ta>
            <ta e="T136" id="Seg_2402" s="T135">nɨŋ-ɨ-lʼä</ta>
            <ta e="T137" id="Seg_2403" s="T136">panɨ-t-q-olap-sɨ-t</ta>
            <ta e="T138" id="Seg_2404" s="T137">mɨšʼaltuka-t</ta>
            <ta e="T139" id="Seg_2405" s="T138">qopɨ</ta>
            <ta e="T140" id="Seg_2406" s="T139">panɨ-sɨ-tɨ</ta>
            <ta e="T141" id="Seg_2407" s="T140">märqɨ-lɔː</ta>
            <ta e="T142" id="Seg_2408" s="T141">ɛsɨ-sa</ta>
            <ta e="T143" id="Seg_2409" s="T142">tok-alti-s-tɨ</ta>
            <ta e="T144" id="Seg_2410" s="T143">oloː-qɨn-tɨ</ta>
            <ta e="T145" id="Seg_2411" s="T144">nɨːnɨ</ta>
            <ta e="T146" id="Seg_2412" s="T145">qotä</ta>
            <ta e="T147" id="Seg_2413" s="T146">toː</ta>
            <ta e="T148" id="Seg_2414" s="T147">tıː-te-sɨ-tɨ</ta>
            <ta e="T149" id="Seg_2415" s="T148">očʼik</ta>
            <ta e="T150" id="Seg_2416" s="T149">pan-al-sɨ-tɨ</ta>
            <ta e="T151" id="Seg_2417" s="T150">aj</ta>
            <ta e="T152" id="Seg_2418" s="T151">očʼik</ta>
            <ta e="T153" id="Seg_2419" s="T152">tok-alti-s-tɨ</ta>
            <ta e="T154" id="Seg_2420" s="T153">oloː-qɨn-tɨ</ta>
            <ta e="T155" id="Seg_2421" s="T154">kəpoː-qɨn-tɨ</ta>
            <ta e="T156" id="Seg_2422" s="T155">nɨːn</ta>
            <ta e="T157" id="Seg_2423" s="T156">aj</ta>
            <ta e="T158" id="Seg_2424" s="T157">toː</ta>
            <ta e="T159" id="Seg_2425" s="T158">tıː-sä-tɨ</ta>
            <ta e="T160" id="Seg_2426" s="T159">nʼuːqo-n-tɨ</ta>
            <ta e="T161" id="Seg_2427" s="T160">nɔː-nɨ</ta>
            <ta e="T162" id="Seg_2428" s="T161">i</ta>
            <ta e="T163" id="Seg_2429" s="T162">olo-n-tɨ</ta>
            <ta e="T164" id="Seg_2430" s="T163">nɔː-nɨ</ta>
            <ta e="T165" id="Seg_2431" s="T164">aj</ta>
            <ta e="T166" id="Seg_2432" s="T165">panɨ-sɨ-tɨ</ta>
            <ta e="T167" id="Seg_2433" s="T166">panɨ-sɨ-tɨ</ta>
            <ta e="T168" id="Seg_2434" s="T167">panɨ-sɨ-tɨ</ta>
            <ta e="T169" id="Seg_2435" s="T168">aj</ta>
            <ta e="T170" id="Seg_2436" s="T169">tɛnɨ-r-sɨ</ta>
            <ta e="T171" id="Seg_2437" s="T170">aj</ta>
            <ta e="T172" id="Seg_2438" s="T171">panɨ-t-qo</ta>
            <ta e="T173" id="Seg_2439" s="T172">nɔːtno</ta>
            <ta e="T174" id="Seg_2440" s="T173">panɨ-sɨ-tɨ</ta>
            <ta e="T175" id="Seg_2441" s="T174">panɨ-sɨ-tɨ</ta>
            <ta e="T176" id="Seg_2442" s="T175">panɨ-sɨ-tɨ</ta>
            <ta e="T177" id="Seg_2443" s="T176">mɨšʼaltuka-t</ta>
            <ta e="T178" id="Seg_2444" s="T177">qopɨ-p</ta>
            <ta e="T179" id="Seg_2445" s="T178">märq</ta>
            <ta e="T180" id="Seg_2446" s="T179">ɛsa</ta>
            <ta e="T181" id="Seg_2447" s="T180">tiː</ta>
            <ta e="T182" id="Seg_2448" s="T181">tok-alti-sɨ-tɨ</ta>
            <ta e="T183" id="Seg_2449" s="T182">mɨšʼaltuka-t</ta>
            <ta e="T184" id="Seg_2450" s="T183">qopɨ-p</ta>
            <ta e="T185" id="Seg_2451" s="T184">käpoː-qɨn-tɨ</ta>
            <ta e="T186" id="Seg_2452" s="T185">i</ta>
            <ta e="T187" id="Seg_2453" s="T186">nʼuːqoː-qon-tɨ</ta>
            <ta e="T188" id="Seg_2454" s="T187">mɨšʼaltuka-t</ta>
            <ta e="T189" id="Seg_2455" s="T188">qopɨ</ta>
            <ta e="T190" id="Seg_2456" s="T189">tuːre-si-tɨ</ta>
            <ta e="T191" id="Seg_2457" s="T190">topoː-qon-tɨ</ta>
            <ta e="T192" id="Seg_2458" s="T191">nɨːnɨ</ta>
            <ta e="T193" id="Seg_2459" s="T192">täp</ta>
            <ta e="T194" id="Seg_2460" s="T193">ɛsa</ta>
            <ta e="T195" id="Seg_2461" s="T194">mɨšʼaltuka-tqo</ta>
            <ta e="T196" id="Seg_2462" s="T195">nɨːnɨ</ta>
            <ta e="T197" id="Seg_2463" s="T196">mɨss-ɛː-ŋa</ta>
            <ta e="T198" id="Seg_2464" s="T197">omti</ta>
            <ta e="T199" id="Seg_2465" s="T198">toː-lɔː</ta>
            <ta e="T200" id="Seg_2466" s="T199">toːn-ɨ-k</ta>
            <ta e="T201" id="Seg_2467" s="T200">ɔːmtɨ-lʼa</ta>
            <ta e="T202" id="Seg_2468" s="T201">siq-laltɨ-mpa</ta>
            <ta e="T203" id="Seg_2469" s="T202">tuː-m-tɨ</ta>
            <ta e="T204" id="Seg_2470" s="T203">siq-laltɨ-mpɨ-sɨ-tɨ</ta>
            <ta e="T205" id="Seg_2471" s="T204">tuː-m-tɨ</ta>
            <ta e="T206" id="Seg_2472" s="T205">i</ta>
            <ta e="T207" id="Seg_2473" s="T206">mɨss-ɛː-sa</ta>
            <ta e="T350" id="Seg_2474" s="T207">num</ta>
            <ta e="T208" id="Seg_2475" s="T350">ınʼnʼaː-qɨt</ta>
            <ta e="T209" id="Seg_2476" s="T208">i</ta>
            <ta e="T210" id="Seg_2477" s="T209">na-t</ta>
            <ta e="T211" id="Seg_2478" s="T210">piːnte-r-sa</ta>
            <ta e="T212" id="Seg_2479" s="T211">kolʼe-mpɨ-sa</ta>
            <ta e="T351" id="Seg_2480" s="T212">num</ta>
            <ta e="T213" id="Seg_2481" s="T351">ınʼnʼaː-qɨt</ta>
            <ta e="T214" id="Seg_2482" s="T213">nɨːnɨ</ta>
            <ta e="T215" id="Seg_2483" s="T214">mɨsʼsʼ-ɛː-sʼa</ta>
            <ta e="T216" id="Seg_2484" s="T215">tıː-mpe-lä</ta>
            <ta e="T217" id="Seg_2485" s="T216">kɨ-p</ta>
            <ta e="T218" id="Seg_2486" s="T217">nʼoː-tɨ-q-olap-sɨ-tɨ</ta>
            <ta e="T219" id="Seg_2487" s="T218">takkɨ-lʼɔː</ta>
            <ta e="T220" id="Seg_2488" s="T219">ilʼi</ta>
            <ta e="T221" id="Seg_2489" s="T220">toː</ta>
            <ta e="T222" id="Seg_2490" s="T221">tılʼ-tʼara</ta>
            <ta e="T223" id="Seg_2491" s="T222">tıː-mpe-lʼa</ta>
            <ta e="T224" id="Seg_2492" s="T223">okkɨr</ta>
            <ta e="T225" id="Seg_2493" s="T224">čʼontoː-qɨt</ta>
            <ta e="T226" id="Seg_2494" s="T225">qo-ŋɨ-tɨ</ta>
            <ta e="T227" id="Seg_2495" s="T226">kɨ-t</ta>
            <ta e="T228" id="Seg_2496" s="T227">šünčʼeː-qɨt</ta>
            <ta e="T229" id="Seg_2497" s="T228">kɨː-qɨt</ta>
            <ta e="T230" id="Seg_2498" s="T229">antɨ</ta>
            <ta e="T231" id="Seg_2499" s="T230">qän-ta</ta>
            <ta e="T232" id="Seg_2500" s="T231">i̇lla-lɔː</ta>
            <ta e="T233" id="Seg_2501" s="T232">üːte-sa</ta>
            <ta e="T234" id="Seg_2502" s="T233">qo-ŋɨ-tɨ</ta>
            <ta e="T235" id="Seg_2503" s="T234">loːsɨ</ta>
            <ta e="T236" id="Seg_2504" s="T235">sitɨ</ta>
            <ta e="T237" id="Seg_2505" s="T236">nälʼa-iː-ntɨ-sa</ta>
            <ta e="T238" id="Seg_2506" s="T237">qän-tɔː-tɨt</ta>
            <ta e="T239" id="Seg_2507" s="T238">loːsɨ</ta>
            <ta e="T240" id="Seg_2508" s="T239">qo-ŋɨ-tɨ</ta>
            <ta e="T241" id="Seg_2509" s="T240">mɨsalʼtuka</ta>
            <ta e="T242" id="Seg_2510" s="T241">kolʼe-mpa</ta>
            <ta e="T243" id="Seg_2511" s="T242">oloː-qɨn-t</ta>
            <ta e="T244" id="Seg_2512" s="T243">tılʼ-tɨr-lʼa</ta>
            <ta e="T245" id="Seg_2513" s="T244">kolʼe-mpa</ta>
            <ta e="T246" id="Seg_2514" s="T245">oj</ta>
            <ta e="T247" id="Seg_2515" s="T246">nälʼa</ta>
            <ta e="T248" id="Seg_2516" s="T247">mannɨ-mp-ät</ta>
            <ta e="T249" id="Seg_2517" s="T248">kuttar</ta>
            <ta e="T250" id="Seg_2518" s="T249">näkɨr-ɨ-lʼ</ta>
            <ta e="T251" id="Seg_2519" s="T250">suːru-t</ta>
            <ta e="T252" id="Seg_2520" s="T251">näka</ta>
            <ta e="T253" id="Seg_2521" s="T252">suːrup</ta>
            <ta e="T254" id="Seg_2522" s="T253">tılʼ-ter-na</ta>
            <ta e="T255" id="Seg_2523" s="T254">meː</ta>
            <ta e="T256" id="Seg_2524" s="T255">pɔːrɨ-qɨ-nɨt</ta>
            <ta e="T257" id="Seg_2525" s="T256">loːsɨ</ta>
            <ta e="T258" id="Seg_2526" s="T257">suːmpa</ta>
            <ta e="T259" id="Seg_2527" s="T258">nalʼä-iː-qɨn-tɨ</ta>
            <ta e="T260" id="Seg_2528" s="T259">nälʼa</ta>
            <ta e="T261" id="Seg_2529" s="T260">nʼalʼa</ta>
            <ta e="T262" id="Seg_2530" s="T261">mennɨ-mpa-tɨ</ta>
            <ta e="T263" id="Seg_2531" s="T262">qej</ta>
            <ta e="T264" id="Seg_2532" s="T263">meː</ta>
            <ta e="T265" id="Seg_2533" s="T264">pɔːroː-qɨ-nɨt</ta>
            <ta e="T266" id="Seg_2534" s="T265">suːru-t</ta>
            <ta e="T267" id="Seg_2535" s="T266">kuttar</ta>
            <ta e="T268" id="Seg_2536" s="T267">suːrup</ta>
            <ta e="T269" id="Seg_2537" s="T268">tıː-ntɨ</ta>
            <ta e="T270" id="Seg_2538" s="T269">mɨsʼsʼaltuka</ta>
            <ta e="T271" id="Seg_2539" s="T270">mɨss-ɛː-lä</ta>
            <ta e="T272" id="Seg_2540" s="T271">qəs-sa</ta>
            <ta e="T273" id="Seg_2541" s="T272">täp-ɨ-n</ta>
            <ta e="T274" id="Seg_2542" s="T273">ukoːn-tɨ</ta>
            <ta e="T275" id="Seg_2543" s="T274">kɨ-ntɨ</ta>
            <ta e="T276" id="Seg_2544" s="T275">marqɨ</ta>
            <ta e="T277" id="Seg_2545" s="T276">tümo-ntɨ</ta>
            <ta e="T278" id="Seg_2546" s="T277">kɨ-t</ta>
            <ta e="T279" id="Seg_2547" s="T278">qanoː-qɨt</ta>
            <ta e="T280" id="Seg_2548" s="T279">omta</ta>
            <ta e="T281" id="Seg_2549" s="T280">nɨːnɨ</ta>
            <ta e="T282" id="Seg_2550" s="T281">šitɨ</ta>
            <ta e="T283" id="Seg_2551" s="T282">tümɨ-t</ta>
            <ta e="T284" id="Seg_2552" s="T283">pɔːrɨ-n-tɨ</ta>
            <ta e="T285" id="Seg_2553" s="T284">nʼantɨ</ta>
            <ta e="T286" id="Seg_2554" s="T285">nʼaq-qɨlʼ-lʼä</ta>
            <ta e="T287" id="Seg_2555" s="T286">nʼak-kɨ-sɨ-tɨ</ta>
            <ta e="T288" id="Seg_2556" s="T287">ɨːtɨ-sɨ-tɨ</ta>
            <ta e="T289" id="Seg_2557" s="T288">čʼesɨn</ta>
            <ta e="T290" id="Seg_2558" s="T289">kɨ-t</ta>
            <ta e="T291" id="Seg_2559" s="T290">sünnʼe-ntɨ</ta>
            <ta e="T292" id="Seg_2560" s="T291">sünčʼö-ntɨ</ta>
            <ta e="T293" id="Seg_2561" s="T292">loːsɨ</ta>
            <ta e="T294" id="Seg_2562" s="T293">tü-nta</ta>
            <ta e="T295" id="Seg_2563" s="T294">suːmpɨ-la</ta>
            <ta e="T296" id="Seg_2564" s="T295">šitɨ</ta>
            <ta e="T297" id="Seg_2565" s="T296">nalʼa-t</ta>
            <ta e="T298" id="Seg_2566" s="T297">tü-sa</ta>
            <ta e="T299" id="Seg_2567" s="T298">čʼesɨn</ta>
            <ta e="T300" id="Seg_2568" s="T299">qän-ta</ta>
            <ta e="T301" id="Seg_2569" s="T300">antɨ-sa</ta>
            <ta e="T302" id="Seg_2570" s="T301">tü-sa</ta>
            <ta e="T303" id="Seg_2571" s="T302">tettɨčʼa-k-tɨ</ta>
            <ta e="T304" id="Seg_2572" s="T303">čʼesɨn-tɨ</ta>
            <ta e="T305" id="Seg_2573" s="T304">tü-sa</ta>
            <ta e="T306" id="Seg_2574" s="T305">antɨ-sa</ta>
            <ta e="T307" id="Seg_2575" s="T306">loːsɨ</ta>
            <ta e="T308" id="Seg_2576" s="T307">qo-ntɨr-tä-ttɨ</ta>
            <ta e="T309" id="Seg_2577" s="T308">čʼäːŋka</ta>
            <ta e="T310" id="Seg_2578" s="T309">čʼesɨn-a-p</ta>
            <ta e="T311" id="Seg_2579" s="T310">čʼesɨn</ta>
            <ta e="T312" id="Seg_2580" s="T311">soːlʼ-mɨn-tɨ</ta>
            <ta e="T313" id="Seg_2581" s="T312">mišal-si-tɨ</ta>
            <ta e="T314" id="Seg_2582" s="T313">šitɨ</ta>
            <ta e="T315" id="Seg_2583" s="T314">tüjmɨ</ta>
            <ta e="T316" id="Seg_2584" s="T315">loːsɨ</ta>
            <ta e="T317" id="Seg_2585" s="T316">qu-ptäː-tɨ</ta>
            <ta e="T318" id="Seg_2586" s="T317">asa</ta>
            <ta e="T319" id="Seg_2587" s="T318">tɛnɨmɨ-tɨ</ta>
            <ta e="T320" id="Seg_2588" s="T319">qu-mpa</ta>
            <ta e="T321" id="Seg_2589" s="T320">täp</ta>
            <ta e="T322" id="Seg_2590" s="T321">antoː-qɨt</ta>
            <ta e="T323" id="Seg_2591" s="T322">orqɨl-sɨ-tɨ</ta>
            <ta e="T324" id="Seg_2592" s="T323">loːsɨ-n</ta>
            <ta e="T325" id="Seg_2593" s="T324">nalʼa-iː-m-tɨ</ta>
            <ta e="T326" id="Seg_2594" s="T325">kar-tɨ-s-tɨ</ta>
            <ta e="T327" id="Seg_2595" s="T326">kɨ-t</ta>
            <ta e="T328" id="Seg_2596" s="T327">qanɨk-tɨ</ta>
            <ta e="T329" id="Seg_2597" s="T328">i</ta>
            <ta e="T330" id="Seg_2598" s="T329">loːsɨ-n</ta>
            <ta e="T331" id="Seg_2599" s="T330">nalʼa-p</ta>
            <ta e="T332" id="Seg_2600" s="T331">okkɨr</ta>
            <ta e="T333" id="Seg_2601" s="T332">nʼeː-m-tɨ</ta>
            <ta e="T334" id="Seg_2602" s="T333">čʼelʼčʼ-ɔːl-lä</ta>
            <ta e="T335" id="Seg_2603" s="T334">pis-sä-tɨ</ta>
            <ta e="T336" id="Seg_2604" s="T335">sʼö-ntɨ</ta>
            <ta e="T337" id="Seg_2605" s="T336">šin-täl-e-lʼ</ta>
            <ta e="T338" id="Seg_2606" s="T337">nälʼa-m-tɨ</ta>
            <ta e="T339" id="Seg_2607" s="T338">aj</ta>
            <ta e="T340" id="Seg_2608" s="T339">nılʼčʼi-k</ta>
            <ta e="T341" id="Seg_2609" s="T340">čʼattɨ-sɨ-tɨ</ta>
            <ta e="T342" id="Seg_2610" s="T341">i</ta>
            <ta e="T343" id="Seg_2611" s="T342">selʼti-sɨ-tɨ</ta>
            <ta e="T344" id="Seg_2612" s="T343">mat</ta>
            <ta e="T345" id="Seg_2613" s="T344">čʼaptä-mɨ</ta>
            <ta e="T346" id="Seg_2614" s="T345">kutɨka</ta>
            <ta e="T347" id="Seg_2615" s="T346">üŋkɨl-te-mpɨ-sɨ-tɨ</ta>
            <ta e="T348" id="Seg_2616" s="T347">meːlte-tqo</ta>
            <ta e="T349" id="Seg_2617" s="T348">ap-sɨ-tɨ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_2618" s="T0">Ičʼakɨčʼɨka</ta>
            <ta e="T2" id="Seg_2619" s="T1">ilɨ-mpɨ</ta>
            <ta e="T3" id="Seg_2620" s="T2">Ičʼakɨčʼɨka</ta>
            <ta e="T4" id="Seg_2621" s="T3">imɨlʼa-ntɨ-sä</ta>
            <ta e="T5" id="Seg_2622" s="T4">Ičʼakɨčʼɨka</ta>
            <ta e="T6" id="Seg_2623" s="T5">qən-ŋɨ</ta>
            <ta e="T7" id="Seg_2624" s="T6">mačʼɨ-ntɨ</ta>
            <ta e="T8" id="Seg_2625" s="T7">qo-ŋɨ-tɨ</ta>
            <ta e="T9" id="Seg_2626" s="T8">qum</ta>
            <ta e="T10" id="Seg_2627" s="T9">pačʼčʼɨ-ttɨ</ta>
            <ta e="T11" id="Seg_2628" s="T10">tü-ŋɨ</ta>
            <ta e="T12" id="Seg_2629" s="T11">təttɨčʼa-k-ntɨ</ta>
            <ta e="T13" id="Seg_2630" s="T12">qo-ŋɨ-tɨ</ta>
            <ta e="T14" id="Seg_2631" s="T13">wərqɨ</ta>
            <ta e="T15" id="Seg_2632" s="T14">lоːsɨ</ta>
            <ta e="T16" id="Seg_2633" s="T15">lоːsɨ-lʼ</ta>
            <ta e="T17" id="Seg_2634" s="T16">qum</ta>
            <ta e="T18" id="Seg_2635" s="T17">ilʼčʼa</ta>
            <ta e="T19" id="Seg_2636" s="T18">qaj</ta>
            <ta e="T20" id="Seg_2637" s="T19">meː-tɨ-l</ta>
            <ta e="T21" id="Seg_2638" s="T20">man</ta>
            <ta e="T22" id="Seg_2639" s="T21">poː-m</ta>
            <ta e="T23" id="Seg_2640" s="T22">pačʼčʼɨ-r-tɨ-m</ta>
            <ta e="T24" id="Seg_2641" s="T23">lоːsɨ</ta>
            <ta e="T25" id="Seg_2642" s="T24">kətɨ-tɨ</ta>
            <ta e="T26" id="Seg_2643" s="T25">kətɨ-sɨ-tɨ</ta>
            <ta e="T27" id="Seg_2644" s="T26">man</ta>
            <ta e="T28" id="Seg_2645" s="T27">ukoːn</ta>
            <ta e="T29" id="Seg_2646" s="T28">tam</ta>
            <ta e="T30" id="Seg_2647" s="T29">poː-m</ta>
            <ta e="T31" id="Seg_2648" s="T30">utɨ-sä</ta>
            <ta e="T32" id="Seg_2649" s="T31">šittɨ</ta>
            <ta e="T33" id="Seg_2650" s="T32">nɨta-qɨl-ɛː-kkɨ-k</ta>
            <ta e="T34" id="Seg_2651" s="T33">kɨssa</ta>
            <ta e="T35" id="Seg_2652" s="T34">kətsan</ta>
            <ta e="T36" id="Seg_2653" s="T35">šittɨ</ta>
            <ta e="T37" id="Seg_2654" s="T36">nɨta-qɨl-ätɨ</ta>
            <ta e="T38" id="Seg_2655" s="T37">kɨssa</ta>
            <ta e="T39" id="Seg_2656" s="T38">saŋa-ätɨ</ta>
            <ta e="T40" id="Seg_2657" s="T39">lоːsɨ-lʼ</ta>
            <ta e="T41" id="Seg_2658" s="T40">ilʼčʼa-tɨ</ta>
            <ta e="T42" id="Seg_2659" s="T41">poː-m</ta>
            <ta e="T43" id="Seg_2660" s="T42">šittɨ</ta>
            <ta e="T44" id="Seg_2661" s="T43">nɨta-qɨl-ŋɨ-tɨ</ta>
            <ta e="T45" id="Seg_2662" s="T44">kɨssa</ta>
            <ta e="T46" id="Seg_2663" s="T45">utɨ-lɨ</ta>
            <ta e="T47" id="Seg_2664" s="T46">säqäl-ätɨ</ta>
            <ta e="T48" id="Seg_2665" s="T47">šittɨ</ta>
            <ta e="T49" id="Seg_2666" s="T48">utɨ-lɨ</ta>
            <ta e="T50" id="Seg_2667" s="T49">tokk-altɨ-ätɨ</ta>
            <ta e="T51" id="Seg_2668" s="T50">noqqo-ɔːl-ätɨ</ta>
            <ta e="T52" id="Seg_2669" s="T51">kɨssa</ta>
            <ta e="T53" id="Seg_2670" s="T52">poː</ta>
            <ta e="T54" id="Seg_2671" s="T53">orqɨl-sɨ-tɨ</ta>
            <ta e="T55" id="Seg_2672" s="T54">utɨ-m-tɨ</ta>
            <ta e="T56" id="Seg_2673" s="T55">šittɨ</ta>
            <ta e="T57" id="Seg_2674" s="T56">utɨ-tɨ</ta>
            <ta e="T58" id="Seg_2675" s="T57">təp</ta>
            <ta e="T59" id="Seg_2676" s="T58">ɔːmtɨ</ta>
            <ta e="T60" id="Seg_2677" s="T59">orɨ-tɨ</ta>
            <ta e="T61" id="Seg_2678" s="T60">čʼäːŋkɨ</ta>
            <ta e="T62" id="Seg_2679" s="T61">lоːsɨ</ta>
            <ta e="T63" id="Seg_2680" s="T62">kətɨ-sɨ-tɨ</ta>
            <ta e="T64" id="Seg_2681" s="T63">ɔːmtɨ-äšɨk</ta>
            <ta e="T65" id="Seg_2682" s="T64">i</ta>
            <ta e="T66" id="Seg_2683" s="T65">təːkɨ-äšɨk</ta>
            <ta e="T67" id="Seg_2684" s="T66">lоːsɨ</ta>
            <ta e="T68" id="Seg_2685" s="T67">qən-sɨ</ta>
            <ta e="T69" id="Seg_2686" s="T68">Ičʼakɨčʼɨka</ta>
            <ta e="T70" id="Seg_2687" s="T69">ɔːmtɨ-sɨ</ta>
            <ta e="T71" id="Seg_2688" s="T70">ɔːmtɨ-sɨ</ta>
            <ta e="T72" id="Seg_2689" s="T71">kuntɨ</ta>
            <ta e="T73" id="Seg_2690" s="T72">čʼam</ta>
            <ta e="T74" id="Seg_2691" s="T73">kuntɨ</ta>
            <ta e="T75" id="Seg_2692" s="T74">qäntɨk</ta>
            <ta e="T76" id="Seg_2693" s="T75">čʼam</ta>
            <ta e="T77" id="Seg_2694" s="T76">qäntɨk</ta>
            <ta e="T78" id="Seg_2695" s="T77">tɛnɨ-r-mpɨ-sɨ</ta>
            <ta e="T79" id="Seg_2696" s="T78">qäntɨk</ta>
            <ta e="T80" id="Seg_2697" s="T79">tantɨ-qo</ta>
            <ta e="T81" id="Seg_2698" s="T80">qäl-ntɨr-sɨ</ta>
            <ta e="T82" id="Seg_2699" s="T81">i</ta>
            <ta e="T83" id="Seg_2700" s="T82">kutta-r-sɨ</ta>
            <ta e="T84" id="Seg_2701" s="T83">ɛː-mpɨ</ta>
            <ta e="T85" id="Seg_2702" s="T84">qättɨ-ntɨlʼ</ta>
            <ta e="T86" id="Seg_2703" s="T85">təp-ɨ-nɨŋ</ta>
            <ta e="T87" id="Seg_2704" s="T86">tü-ŋɨ</ta>
            <ta e="T88" id="Seg_2705" s="T87">qorqɨ</ta>
            <ta e="T89" id="Seg_2706" s="T88">ilʼčʼa</ta>
            <ta e="T90" id="Seg_2707" s="T89">poː-m</ta>
            <ta e="T91" id="Seg_2708" s="T90">šittɨ</ta>
            <ta e="T92" id="Seg_2709" s="T91">nɨta-qɨl-ätɨ</ta>
            <ta e="T93" id="Seg_2710" s="T92">qorqɨ</ta>
            <ta e="T94" id="Seg_2711" s="T93">šittɨ</ta>
            <ta e="T95" id="Seg_2712" s="T94">nɨta-qɨl-sɨ-tɨ</ta>
            <ta e="T96" id="Seg_2713" s="T95">utɨ-tɨ</ta>
            <ta e="T97" id="Seg_2714" s="T96">tıː-tɨ-sɨ-tɨ</ta>
            <ta e="T98" id="Seg_2715" s="T97">i</ta>
            <ta e="T99" id="Seg_2716" s="T98">utɨ-tɨ</ta>
            <ta e="T100" id="Seg_2717" s="T99">uːpɨl</ta>
            <ta e="T101" id="Seg_2718" s="T100">alʼčʼɨ-sɨ</ta>
            <ta e="T102" id="Seg_2719" s="T101">tantɨ</ta>
            <ta e="T103" id="Seg_2720" s="T102">poː</ta>
            <ta e="T104" id="Seg_2721" s="T103">*nɔː-nɨ</ta>
            <ta e="T105" id="Seg_2722" s="T104">kutta-r-ŋɨ</ta>
            <ta e="T106" id="Seg_2723" s="T105">qäl-ntɨr-ŋɨ</ta>
            <ta e="T107" id="Seg_2724" s="T106">tɛnɨ-r-mpɨ</ta>
            <ta e="T108" id="Seg_2725" s="T107">qäntɨk</ta>
            <ta e="T109" id="Seg_2726" s="T108">mitɨ-qo</ta>
            <ta e="T110" id="Seg_2727" s="T109">i</ta>
            <ta e="T111" id="Seg_2728" s="T110">qo-ntɨr-qo</ta>
            <ta e="T112" id="Seg_2729" s="T111">lоːsɨ-m</ta>
            <ta e="T113" id="Seg_2730" s="T112">kutta-r-ŋɨ</ta>
            <ta e="T114" id="Seg_2731" s="T113">qäl-ɨ-mpɨ</ta>
            <ta e="T115" id="Seg_2732" s="T114">tɛnɨ-r-mpɨ-tɨ</ta>
            <ta e="T116" id="Seg_2733" s="T115">kuttar</ta>
            <ta e="T117" id="Seg_2734" s="T116">lоːsɨ-m</ta>
            <ta e="T118" id="Seg_2735" s="T117">mitɨ-qo</ta>
            <ta e="T119" id="Seg_2736" s="T118">orqɨl-qo</ta>
            <ta e="T120" id="Seg_2737" s="T119">orqɨl-lä</ta>
            <ta e="T121" id="Seg_2738" s="T120">iː-lɨ-lä</ta>
            <ta e="T122" id="Seg_2739" s="T121">tɛnɨ-r-mpɨ</ta>
            <ta e="T123" id="Seg_2740" s="T122">kučʼčʼä</ta>
            <ta e="T124" id="Seg_2741" s="T123">qattɨ-qo</ta>
            <ta e="T125" id="Seg_2742" s="T124">olɨ-qɨn-ntɨ</ta>
            <ta e="T126" id="Seg_2743" s="T125">alʼčʼɨ</ta>
            <ta e="T127" id="Seg_2744" s="T126">tɛnɨ-tɨ</ta>
            <ta e="T128" id="Seg_2745" s="T127">nɔːtna</ta>
            <ta e="T129" id="Seg_2746" s="T128">karman-mɨ</ta>
            <ta e="T130" id="Seg_2747" s="T129">mantɨ-mpɨ-qo</ta>
            <ta e="T131" id="Seg_2748" s="T130">korpɨt-qo</ta>
            <ta e="T132" id="Seg_2749" s="T131">karman-qɨn-ntɨ</ta>
            <ta e="T133" id="Seg_2750" s="T132">qo-sɨ-tɨ</ta>
            <ta e="T134" id="Seg_2751" s="T133">mɨšaltuka-n</ta>
            <ta e="T135" id="Seg_2752" s="T134">qopɨ</ta>
            <ta e="T136" id="Seg_2753" s="T135">nɨŋ-ɨ-lä</ta>
            <ta e="T137" id="Seg_2754" s="T136">panɨ-tɨ-qo-olam-sɨ-tɨ</ta>
            <ta e="T138" id="Seg_2755" s="T137">mɨšaltuka-n</ta>
            <ta e="T139" id="Seg_2756" s="T138">qopɨ</ta>
            <ta e="T140" id="Seg_2757" s="T139">panɨ-sɨ-tɨ</ta>
            <ta e="T141" id="Seg_2758" s="T140">wərqɨ-lɔːqɨ</ta>
            <ta e="T142" id="Seg_2759" s="T141">ɛsɨ-sɨ</ta>
            <ta e="T143" id="Seg_2760" s="T142">tokk-altɨ-sɨ-tɨ</ta>
            <ta e="T144" id="Seg_2761" s="T143">olɨ-qɨn-ntɨ</ta>
            <ta e="T145" id="Seg_2762" s="T144">nɨːnɨ</ta>
            <ta e="T146" id="Seg_2763" s="T145">qottä</ta>
            <ta e="T147" id="Seg_2764" s="T146">toː</ta>
            <ta e="T148" id="Seg_2765" s="T147">tıː-tɨ-sɨ-tɨ</ta>
            <ta e="T149" id="Seg_2766" s="T148">očʼɨŋ</ta>
            <ta e="T150" id="Seg_2767" s="T149">panɨ-alʼ-sɨ-tɨ</ta>
            <ta e="T151" id="Seg_2768" s="T150">aj</ta>
            <ta e="T152" id="Seg_2769" s="T151">očʼɨŋ</ta>
            <ta e="T153" id="Seg_2770" s="T152">tokk-altɨ-sɨ-tɨ</ta>
            <ta e="T154" id="Seg_2771" s="T153">olɨ-qɨn-ntɨ</ta>
            <ta e="T155" id="Seg_2772" s="T154">kəpɨ-qɨn-ntɨ</ta>
            <ta e="T156" id="Seg_2773" s="T155">nɨːnɨ</ta>
            <ta e="T157" id="Seg_2774" s="T156">aj</ta>
            <ta e="T158" id="Seg_2775" s="T157">toː</ta>
            <ta e="T159" id="Seg_2776" s="T158">tıː-sɨ-tɨ</ta>
            <ta e="T160" id="Seg_2777" s="T159">nʼuːqɨ-n-tɨ</ta>
            <ta e="T161" id="Seg_2778" s="T160">*nɔː-nɨ</ta>
            <ta e="T162" id="Seg_2779" s="T161">i</ta>
            <ta e="T163" id="Seg_2780" s="T162">olɨ-n-tɨ</ta>
            <ta e="T164" id="Seg_2781" s="T163">*nɔː-nɨ</ta>
            <ta e="T165" id="Seg_2782" s="T164">aj</ta>
            <ta e="T166" id="Seg_2783" s="T165">panɨ-sɨ-tɨ</ta>
            <ta e="T167" id="Seg_2784" s="T166">panɨ-sɨ-tɨ</ta>
            <ta e="T168" id="Seg_2785" s="T167">panɨ-sɨ-tɨ</ta>
            <ta e="T169" id="Seg_2786" s="T168">aj</ta>
            <ta e="T170" id="Seg_2787" s="T169">tɛnɨ-r-sɨ</ta>
            <ta e="T171" id="Seg_2788" s="T170">aj</ta>
            <ta e="T172" id="Seg_2789" s="T171">panɨ-tɨ-qo</ta>
            <ta e="T173" id="Seg_2790" s="T172">nɔːtna</ta>
            <ta e="T174" id="Seg_2791" s="T173">panɨ-sɨ-tɨ</ta>
            <ta e="T175" id="Seg_2792" s="T174">panɨ-sɨ-tɨ</ta>
            <ta e="T176" id="Seg_2793" s="T175">panɨ-sɨ-tɨ</ta>
            <ta e="T177" id="Seg_2794" s="T176">mɨšaltuka-n</ta>
            <ta e="T178" id="Seg_2795" s="T177">qopɨ-m</ta>
            <ta e="T179" id="Seg_2796" s="T178">wərqɨ</ta>
            <ta e="T180" id="Seg_2797" s="T179">ɛsɨ</ta>
            <ta e="T181" id="Seg_2798" s="T180">tiː</ta>
            <ta e="T182" id="Seg_2799" s="T181">tokk-altɨ-sɨ-tɨ</ta>
            <ta e="T183" id="Seg_2800" s="T182">mɨšaltuka-n</ta>
            <ta e="T184" id="Seg_2801" s="T183">qopɨ-m</ta>
            <ta e="T185" id="Seg_2802" s="T184">kəpɨ-qɨn-ntɨ</ta>
            <ta e="T186" id="Seg_2803" s="T185">i</ta>
            <ta e="T187" id="Seg_2804" s="T186">nʼuːqɨ-qɨn-ntɨ</ta>
            <ta e="T188" id="Seg_2805" s="T187">mɨšaltuka-n</ta>
            <ta e="T189" id="Seg_2806" s="T188">qopɨ</ta>
            <ta e="T190" id="Seg_2807" s="T189">tuːrɨ-sɨ-tɨ</ta>
            <ta e="T191" id="Seg_2808" s="T190">topɨ-qɨn-ntɨ</ta>
            <ta e="T192" id="Seg_2809" s="T191">nɨːnɨ</ta>
            <ta e="T193" id="Seg_2810" s="T192">təp</ta>
            <ta e="T194" id="Seg_2811" s="T193">ɛsɨ</ta>
            <ta e="T195" id="Seg_2812" s="T194">mɨšaltuka-tqo</ta>
            <ta e="T196" id="Seg_2813" s="T195">nɨːnɨ</ta>
            <ta e="T197" id="Seg_2814" s="T196">wəšɨ-ɛː-ŋɨ</ta>
            <ta e="T198" id="Seg_2815" s="T197">omtɨ</ta>
            <ta e="T199" id="Seg_2816" s="T198">toː-lɔːqɨ</ta>
            <ta e="T200" id="Seg_2817" s="T199">toːn-ɨ-k</ta>
            <ta e="T201" id="Seg_2818" s="T200">ɔːmtɨ-lä</ta>
            <ta e="T202" id="Seg_2819" s="T201">sɨq-altɨ-mpɨ</ta>
            <ta e="T203" id="Seg_2820" s="T202">tuː-m-tɨ</ta>
            <ta e="T204" id="Seg_2821" s="T203">sɨq-altɨ-mpɨ-sɨ-tɨ</ta>
            <ta e="T205" id="Seg_2822" s="T204">tuː-m-tɨ</ta>
            <ta e="T206" id="Seg_2823" s="T205">i</ta>
            <ta e="T207" id="Seg_2824" s="T206">wəšɨ-ɛː-sɨ</ta>
            <ta e="T350" id="Seg_2825" s="T207">nom</ta>
            <ta e="T208" id="Seg_2826" s="T350">ınnä-qɨn</ta>
            <ta e="T209" id="Seg_2827" s="T208">i</ta>
            <ta e="T210" id="Seg_2828" s="T209">na-tɨ</ta>
            <ta e="T211" id="Seg_2829" s="T210">piːntɨ-r-sɨ</ta>
            <ta e="T212" id="Seg_2830" s="T211">kolʼɨ-mpɨ-sɨ</ta>
            <ta e="T351" id="Seg_2831" s="T212">nom</ta>
            <ta e="T213" id="Seg_2832" s="T351">ınnä-qɨn</ta>
            <ta e="T214" id="Seg_2833" s="T213">nɨːnɨ</ta>
            <ta e="T215" id="Seg_2834" s="T214">wəšɨ-ɛː-sɨ</ta>
            <ta e="T216" id="Seg_2835" s="T215">tıː-mpɨ-lä</ta>
            <ta e="T217" id="Seg_2836" s="T216">kɨ-m</ta>
            <ta e="T218" id="Seg_2837" s="T217">nʼoː-tɨ-qo-olam-sɨ-tɨ</ta>
            <ta e="T219" id="Seg_2838" s="T218">takkɨ-lɔːqɨ</ta>
            <ta e="T220" id="Seg_2839" s="T219">ilʼi</ta>
            <ta e="T221" id="Seg_2840" s="T220">toː</ta>
            <ta e="T222" id="Seg_2841" s="T221">tılɨ-ntɨr</ta>
            <ta e="T223" id="Seg_2842" s="T222">tıː-mpɨ-lä</ta>
            <ta e="T224" id="Seg_2843" s="T223">ukkɨr</ta>
            <ta e="T225" id="Seg_2844" s="T224">čʼontɨ-qɨn</ta>
            <ta e="T226" id="Seg_2845" s="T225">qo-ŋɨ-tɨ</ta>
            <ta e="T227" id="Seg_2846" s="T226">kɨ-n</ta>
            <ta e="T228" id="Seg_2847" s="T227">šünʼčʼɨ-qɨn</ta>
            <ta e="T229" id="Seg_2848" s="T228">kɨ-qɨn</ta>
            <ta e="T230" id="Seg_2849" s="T229">antɨ</ta>
            <ta e="T231" id="Seg_2850" s="T230">qən-ntɨ</ta>
            <ta e="T232" id="Seg_2851" s="T231">ıllä-lɔːqɨ</ta>
            <ta e="T233" id="Seg_2852" s="T232">üːtɨ-sɨ</ta>
            <ta e="T234" id="Seg_2853" s="T233">qo-ŋɨ-tɨ</ta>
            <ta e="T235" id="Seg_2854" s="T234">lоːsɨ</ta>
            <ta e="T236" id="Seg_2855" s="T235">šittɨ</ta>
            <ta e="T237" id="Seg_2856" s="T236">nälʼa-iː-ntɨ-sä</ta>
            <ta e="T238" id="Seg_2857" s="T237">qən-ntɨ-tɨt</ta>
            <ta e="T239" id="Seg_2858" s="T238">lоːsɨ</ta>
            <ta e="T240" id="Seg_2859" s="T239">qo-ŋɨ-tɨ</ta>
            <ta e="T241" id="Seg_2860" s="T240">mɨšaltuka</ta>
            <ta e="T242" id="Seg_2861" s="T241">kolʼɨ-mpɨ</ta>
            <ta e="T243" id="Seg_2862" s="T242">olɨ-qɨn-ntɨ</ta>
            <ta e="T244" id="Seg_2863" s="T243">tıː-ntɨr-lä</ta>
            <ta e="T245" id="Seg_2864" s="T244">kolʼɨ-mpɨ</ta>
            <ta e="T246" id="Seg_2865" s="T245">oj</ta>
            <ta e="T247" id="Seg_2866" s="T246">nälʼa</ta>
            <ta e="T248" id="Seg_2867" s="T247">mantɨ-mpɨ-ätɨ</ta>
            <ta e="T249" id="Seg_2868" s="T248">kuttar</ta>
            <ta e="T250" id="Seg_2869" s="T249">nəkɨr-ɨ-lʼ</ta>
            <ta e="T251" id="Seg_2870" s="T250">suːrɨm-n</ta>
            <ta e="T252" id="Seg_2871" s="T251">nəkɨr</ta>
            <ta e="T253" id="Seg_2872" s="T252">suːrɨm</ta>
            <ta e="T254" id="Seg_2873" s="T253">tıː-ntɨr-ŋɨ</ta>
            <ta e="T255" id="Seg_2874" s="T254">meː</ta>
            <ta e="T256" id="Seg_2875" s="T255">pɔːrɨ-qɨn-nɨt</ta>
            <ta e="T257" id="Seg_2876" s="T256">lоːsɨ</ta>
            <ta e="T258" id="Seg_2877" s="T257">suːmpɨ</ta>
            <ta e="T259" id="Seg_2878" s="T258">nälʼa-iː-qɨn-ntɨ</ta>
            <ta e="T260" id="Seg_2879" s="T259">nälʼa</ta>
            <ta e="T261" id="Seg_2880" s="T260">nälʼa</ta>
            <ta e="T262" id="Seg_2881" s="T261">mantɨ-mpɨ-ätɨ</ta>
            <ta e="T263" id="Seg_2882" s="T262">qaj</ta>
            <ta e="T264" id="Seg_2883" s="T263">meː</ta>
            <ta e="T265" id="Seg_2884" s="T264">pɔːrɨ-qɨn-nɨt</ta>
            <ta e="T266" id="Seg_2885" s="T265">suːrɨm-tɨ</ta>
            <ta e="T267" id="Seg_2886" s="T266">kuttar</ta>
            <ta e="T268" id="Seg_2887" s="T267">suːrɨm</ta>
            <ta e="T269" id="Seg_2888" s="T268">tıː-ntɨ</ta>
            <ta e="T270" id="Seg_2889" s="T269">mɨšaltuka</ta>
            <ta e="T271" id="Seg_2890" s="T270">wəšɨ-ɛː-lä</ta>
            <ta e="T272" id="Seg_2891" s="T271">qən-sɨ</ta>
            <ta e="T273" id="Seg_2892" s="T272">təp-ɨ-n</ta>
            <ta e="T274" id="Seg_2893" s="T273">ukoːn-ntɨ</ta>
            <ta e="T275" id="Seg_2894" s="T274">kɨ-ntɨ</ta>
            <ta e="T276" id="Seg_2895" s="T275">wərqɨ</ta>
            <ta e="T277" id="Seg_2896" s="T276">tümɨ-ntɨ</ta>
            <ta e="T278" id="Seg_2897" s="T277">kɨ-n</ta>
            <ta e="T279" id="Seg_2898" s="T278">qanɨŋ-qɨn</ta>
            <ta e="T280" id="Seg_2899" s="T279">omtɨ</ta>
            <ta e="T281" id="Seg_2900" s="T280">nɨːnɨ</ta>
            <ta e="T282" id="Seg_2901" s="T281">šittɨ</ta>
            <ta e="T283" id="Seg_2902" s="T282">tümɨ-n</ta>
            <ta e="T284" id="Seg_2903" s="T283">pɔːrɨ-n-tɨ</ta>
            <ta e="T285" id="Seg_2904" s="T284">nʼentɨ</ta>
            <ta e="T286" id="Seg_2905" s="T285">näkä-qɨl-lä</ta>
            <ta e="T287" id="Seg_2906" s="T286">näkä-kkɨ-sɨ-tɨ</ta>
            <ta e="T288" id="Seg_2907" s="T287">ɨːtɨ-sɨ-tɨ</ta>
            <ta e="T289" id="Seg_2908" s="T288">čʼəsɨn</ta>
            <ta e="T290" id="Seg_2909" s="T289">kɨ-n</ta>
            <ta e="T291" id="Seg_2910" s="T290">šünʼčʼɨ-ntɨ</ta>
            <ta e="T292" id="Seg_2911" s="T291">šünʼčʼɨ-ntɨ</ta>
            <ta e="T293" id="Seg_2912" s="T292">lоːsɨ</ta>
            <ta e="T294" id="Seg_2913" s="T293">tü-ntɨ</ta>
            <ta e="T295" id="Seg_2914" s="T294">suːmpɨ-lä</ta>
            <ta e="T296" id="Seg_2915" s="T295">šittɨ</ta>
            <ta e="T297" id="Seg_2916" s="T296">nälʼa-t</ta>
            <ta e="T298" id="Seg_2917" s="T297">tü-sɨ</ta>
            <ta e="T299" id="Seg_2918" s="T298">čʼəsɨn</ta>
            <ta e="T300" id="Seg_2919" s="T299">qan-ntɨ</ta>
            <ta e="T301" id="Seg_2920" s="T300">antɨ-sä</ta>
            <ta e="T302" id="Seg_2921" s="T301">tü-sɨ</ta>
            <ta e="T303" id="Seg_2922" s="T302">təttɨčʼa-k-ntɨ</ta>
            <ta e="T304" id="Seg_2923" s="T303">čʼəsɨn-ntɨ</ta>
            <ta e="T305" id="Seg_2924" s="T304">tü-sɨ</ta>
            <ta e="T306" id="Seg_2925" s="T305">antɨ-sä</ta>
            <ta e="T307" id="Seg_2926" s="T306">lоːsɨ</ta>
            <ta e="T308" id="Seg_2927" s="T307">qo-ntɨr-tɨ-tɨ</ta>
            <ta e="T309" id="Seg_2928" s="T308">čʼäːŋkɨ</ta>
            <ta e="T310" id="Seg_2929" s="T309">čʼəsɨn-ɨ-m</ta>
            <ta e="T311" id="Seg_2930" s="T310">čʼəsɨn</ta>
            <ta e="T312" id="Seg_2931" s="T311">soːj-mɨn-ntɨ</ta>
            <ta e="T313" id="Seg_2932" s="T312">mišal-sɨ-tɨ</ta>
            <ta e="T314" id="Seg_2933" s="T313">šittɨ</ta>
            <ta e="T315" id="Seg_2934" s="T314">tümɨ</ta>
            <ta e="T316" id="Seg_2935" s="T315">lоːsɨ</ta>
            <ta e="T317" id="Seg_2936" s="T316">qu-ptäː-tɨ</ta>
            <ta e="T318" id="Seg_2937" s="T317">ašša</ta>
            <ta e="T319" id="Seg_2938" s="T318">tɛnɨmɨ-tɨ</ta>
            <ta e="T320" id="Seg_2939" s="T319">qu-mpɨ</ta>
            <ta e="T321" id="Seg_2940" s="T320">təp</ta>
            <ta e="T322" id="Seg_2941" s="T321">antɨ-qɨn</ta>
            <ta e="T323" id="Seg_2942" s="T322">orqɨl-sɨ-tɨ</ta>
            <ta e="T324" id="Seg_2943" s="T323">lоːsɨ-n</ta>
            <ta e="T325" id="Seg_2944" s="T324">nälʼa-iː-m-tɨ</ta>
            <ta e="T326" id="Seg_2945" s="T325">kəːrɨ-tɨ-sɨ-tɨ</ta>
            <ta e="T327" id="Seg_2946" s="T326">kɨ-n</ta>
            <ta e="T328" id="Seg_2947" s="T327">qanɨŋ-ntɨ</ta>
            <ta e="T329" id="Seg_2948" s="T328">i</ta>
            <ta e="T330" id="Seg_2949" s="T329">lоːsɨ-n</ta>
            <ta e="T331" id="Seg_2950" s="T330">nälʼa-m</ta>
            <ta e="T332" id="Seg_2951" s="T331">ukkɨr</ta>
            <ta e="T333" id="Seg_2952" s="T332">neː-m-tɨ</ta>
            <ta e="T334" id="Seg_2953" s="T333">čʼelʼčʼɨ-ɔːl-lä</ta>
            <ta e="T335" id="Seg_2954" s="T334">pin-sɨ-tɨ</ta>
            <ta e="T336" id="Seg_2955" s="T335">sö-ntɨ</ta>
            <ta e="T337" id="Seg_2956" s="T336">šittɨ-täl-ɨ-lʼ</ta>
            <ta e="T338" id="Seg_2957" s="T337">nälʼa-m-tɨ</ta>
            <ta e="T339" id="Seg_2958" s="T338">aj</ta>
            <ta e="T340" id="Seg_2959" s="T339">nılʼčʼɨ-k</ta>
            <ta e="T341" id="Seg_2960" s="T340">čʼattɨ-sɨ-tɨ</ta>
            <ta e="T342" id="Seg_2961" s="T341">i</ta>
            <ta e="T343" id="Seg_2962" s="T342">čʼelʼčʼɨ-sɨ-tɨ</ta>
            <ta e="T344" id="Seg_2963" s="T343">man</ta>
            <ta e="T345" id="Seg_2964" s="T344">čʼaptä-mɨ</ta>
            <ta e="T346" id="Seg_2965" s="T345">kutɨ</ta>
            <ta e="T347" id="Seg_2966" s="T346">üŋkɨl-tɨ-mpɨ-sɨ-tɨ</ta>
            <ta e="T348" id="Seg_2967" s="T347">meːltɨ-tqo</ta>
            <ta e="T349" id="Seg_2968" s="T348">am-sɨ-tɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_2969" s="T0">Ichakichika.[NOM]</ta>
            <ta e="T2" id="Seg_2970" s="T1">live-PST.NAR.[3SG.S]</ta>
            <ta e="T3" id="Seg_2971" s="T2">Ichakichika.[NOM]</ta>
            <ta e="T4" id="Seg_2972" s="T3">grandmother-OBL.3SG-COM</ta>
            <ta e="T5" id="Seg_2973" s="T4">Ichakichika</ta>
            <ta e="T6" id="Seg_2974" s="T5">leave-CO.[3SG.S]</ta>
            <ta e="T7" id="Seg_2975" s="T6">forest-ILL</ta>
            <ta e="T8" id="Seg_2976" s="T7">see-CO-3SG.O</ta>
            <ta e="T9" id="Seg_2977" s="T8">human.being.[NOM]</ta>
            <ta e="T10" id="Seg_2978" s="T9">chop-DETR.[3SG.S]</ta>
            <ta e="T11" id="Seg_2979" s="T10">come-CO.[3SG.S]</ta>
            <ta e="T12" id="Seg_2980" s="T11">the.close.one-ADVZ-ADV.ILL</ta>
            <ta e="T13" id="Seg_2981" s="T12">see-CO-3SG.O</ta>
            <ta e="T14" id="Seg_2982" s="T13">big</ta>
            <ta e="T15" id="Seg_2983" s="T14">devil.[NOM]</ta>
            <ta e="T16" id="Seg_2984" s="T15">devil-ADJZ</ta>
            <ta e="T17" id="Seg_2985" s="T16">human.being.[NOM]</ta>
            <ta e="T18" id="Seg_2986" s="T17">grandfather.[NOM]</ta>
            <ta e="T19" id="Seg_2987" s="T18">what.[NOM]</ta>
            <ta e="T20" id="Seg_2988" s="T19">make-TR-2SG.O</ta>
            <ta e="T21" id="Seg_2989" s="T20">I.NOM</ta>
            <ta e="T22" id="Seg_2990" s="T21">tree-ACC</ta>
            <ta e="T23" id="Seg_2991" s="T22">chop-FRQ-TR-1SG.O</ta>
            <ta e="T24" id="Seg_2992" s="T23">devil.[NOM]</ta>
            <ta e="T25" id="Seg_2993" s="T24">say-3SG.O</ta>
            <ta e="T26" id="Seg_2994" s="T25">say-PST-3SG.O</ta>
            <ta e="T27" id="Seg_2995" s="T26">I.NOM</ta>
            <ta e="T28" id="Seg_2996" s="T27">earlier</ta>
            <ta e="T29" id="Seg_2997" s="T28">this</ta>
            <ta e="T30" id="Seg_2998" s="T29">tree-ACC</ta>
            <ta e="T31" id="Seg_2999" s="T30">hand-INSTR</ta>
            <ta e="T32" id="Seg_3000" s="T31">into.parts</ta>
            <ta e="T33" id="Seg_3001" s="T32">tear-MULO-PFV-HAB-1SG.S</ta>
            <ta e="T34" id="Seg_3002" s="T33">come.on</ta>
            <ta e="T35" id="Seg_3003" s="T34">grandson.[NOM]</ta>
            <ta e="T36" id="Seg_3004" s="T35">into.parts</ta>
            <ta e="T37" id="Seg_3005" s="T36">tear-MULO-IMP.2SG.O</ta>
            <ta e="T38" id="Seg_3006" s="T37">come.on</ta>
            <ta e="T39" id="Seg_3007" s="T38">try-IMP.2SG.O</ta>
            <ta e="T40" id="Seg_3008" s="T39">devil-ADJZ</ta>
            <ta e="T41" id="Seg_3009" s="T40">grandfather.[NOM]-3SG</ta>
            <ta e="T42" id="Seg_3010" s="T41">tree-ACC</ta>
            <ta e="T43" id="Seg_3011" s="T42">into.parts</ta>
            <ta e="T44" id="Seg_3012" s="T43">tear-MULO-CO-3SG.O</ta>
            <ta e="T45" id="Seg_3013" s="T44">come.on</ta>
            <ta e="T46" id="Seg_3014" s="T45">hand.[NOM]-2SG</ta>
            <ta e="T47" id="Seg_3015" s="T46">push.in-IMP.2SG.O</ta>
            <ta e="T48" id="Seg_3016" s="T47">two</ta>
            <ta e="T49" id="Seg_3017" s="T48">hand.[NOM]-2SG</ta>
            <ta e="T50" id="Seg_3018" s="T49">put.on-TR-IMP.2SG.O</ta>
            <ta e="T51" id="Seg_3019" s="T50">push-MOM-IMP.2SG.O</ta>
            <ta e="T52" id="Seg_3020" s="T51">come.on</ta>
            <ta e="T53" id="Seg_3021" s="T52">tree.[NOM]</ta>
            <ta e="T54" id="Seg_3022" s="T53">catch-PST-3SG.O</ta>
            <ta e="T55" id="Seg_3023" s="T54">hand-ACC-3SG</ta>
            <ta e="T56" id="Seg_3024" s="T55">two</ta>
            <ta e="T57" id="Seg_3025" s="T56">hand.[NOM]-3SG</ta>
            <ta e="T58" id="Seg_3026" s="T57">(s)he.[NOM]</ta>
            <ta e="T59" id="Seg_3027" s="T58">sit.[3SG.S]</ta>
            <ta e="T60" id="Seg_3028" s="T59">force.[NOM]-3SG</ta>
            <ta e="T61" id="Seg_3029" s="T60">NEG.EX.[3SG.S]</ta>
            <ta e="T62" id="Seg_3030" s="T61">devil.[NOM]</ta>
            <ta e="T63" id="Seg_3031" s="T62">say-PST-3SG.O</ta>
            <ta e="T64" id="Seg_3032" s="T63">sit-IMP.2SG.S</ta>
            <ta e="T65" id="Seg_3033" s="T64">and</ta>
            <ta e="T66" id="Seg_3034" s="T65">dry-IMP.2SG.S</ta>
            <ta e="T67" id="Seg_3035" s="T66">devil.[NOM]</ta>
            <ta e="T68" id="Seg_3036" s="T67">go.away-PST.[3SG.S]</ta>
            <ta e="T69" id="Seg_3037" s="T68">Ichakichika.[NOM]</ta>
            <ta e="T70" id="Seg_3038" s="T69">sit-PST.[3SG.S]</ta>
            <ta e="T71" id="Seg_3039" s="T70">sit-PST.[3SG.S]</ta>
            <ta e="T72" id="Seg_3040" s="T71">long</ta>
            <ta e="T73" id="Seg_3041" s="T72">hardly</ta>
            <ta e="T74" id="Seg_3042" s="T73">long</ta>
            <ta e="T75" id="Seg_3043" s="T74">how</ta>
            <ta e="T76" id="Seg_3044" s="T75">hardly</ta>
            <ta e="T77" id="Seg_3045" s="T76">how</ta>
            <ta e="T78" id="Seg_3046" s="T77">think-FRQ-DUR-PST.[3SG.S]</ta>
            <ta e="T79" id="Seg_3047" s="T78">how</ta>
            <ta e="T80" id="Seg_3048" s="T79">go.out-INF</ta>
            <ta e="T81" id="Seg_3049" s="T80">go-DRV-PST.[3SG.S]</ta>
            <ta e="T82" id="Seg_3050" s="T81">and</ta>
            <ta e="T83" id="Seg_3051" s="T82">go-FRQ-PST.[3SG.S]</ta>
            <ta e="T84" id="Seg_3052" s="T83">be-PST.NAR.[3SG.S]</ta>
            <ta e="T85" id="Seg_3053" s="T84">hit-PTCP.PRS</ta>
            <ta e="T86" id="Seg_3054" s="T85">(s)he-EP-ALL</ta>
            <ta e="T87" id="Seg_3055" s="T86">come-CO.[3SG.S]</ta>
            <ta e="T88" id="Seg_3056" s="T87">bear.[NOM]</ta>
            <ta e="T89" id="Seg_3057" s="T88">grandfather.[NOM]</ta>
            <ta e="T90" id="Seg_3058" s="T89">tree-ACC</ta>
            <ta e="T91" id="Seg_3059" s="T90">every.which.way</ta>
            <ta e="T92" id="Seg_3060" s="T91">tear-MULO-IMP.2SG.O</ta>
            <ta e="T93" id="Seg_3061" s="T92">bear.[NOM]</ta>
            <ta e="T94" id="Seg_3062" s="T93">into.parts</ta>
            <ta e="T95" id="Seg_3063" s="T94">tear-MULO-PST-3SG.O</ta>
            <ta e="T96" id="Seg_3064" s="T95">hand.[NOM]-3SG</ta>
            <ta e="T97" id="Seg_3065" s="T96">take.out-TR-PST-3SG.O</ta>
            <ta e="T98" id="Seg_3066" s="T97">and</ta>
            <ta e="T99" id="Seg_3067" s="T98">hand.[NOM]-3SG</ta>
            <ta e="T100" id="Seg_3068" s="T99">free</ta>
            <ta e="T101" id="Seg_3069" s="T100">fall-PST.[3SG.S]</ta>
            <ta e="T102" id="Seg_3070" s="T101">go.out.[3SG.S]</ta>
            <ta e="T103" id="Seg_3071" s="T102">tree.[NOM]</ta>
            <ta e="T104" id="Seg_3072" s="T103">out-ADV.EL</ta>
            <ta e="T105" id="Seg_3073" s="T104">go-FRQ-CO.[3SG.S]</ta>
            <ta e="T106" id="Seg_3074" s="T105">go-DRV-CO.[3SG.S]</ta>
            <ta e="T107" id="Seg_3075" s="T106">think-FRQ-DUR.[3SG.S]</ta>
            <ta e="T108" id="Seg_3076" s="T107">how</ta>
            <ta e="T109" id="Seg_3077" s="T108">catch.up-INF</ta>
            <ta e="T110" id="Seg_3078" s="T109">and</ta>
            <ta e="T111" id="Seg_3079" s="T110">see-DRV-INF</ta>
            <ta e="T112" id="Seg_3080" s="T111">devil-ACC</ta>
            <ta e="T113" id="Seg_3081" s="T112">go-FRQ-CO.[3SG.S]</ta>
            <ta e="T114" id="Seg_3082" s="T113">go-EP-DUR.[3SG.S]</ta>
            <ta e="T115" id="Seg_3083" s="T114">think-FRQ-DUR-3SG.O</ta>
            <ta e="T116" id="Seg_3084" s="T115">how</ta>
            <ta e="T117" id="Seg_3085" s="T116">devil-ACC</ta>
            <ta e="T118" id="Seg_3086" s="T117">catch.up-INF</ta>
            <ta e="T119" id="Seg_3087" s="T118">catch-INF</ta>
            <ta e="T120" id="Seg_3088" s="T119">catch-CVB</ta>
            <ta e="T121" id="Seg_3089" s="T120">take-RES-OPT.[3SG.S]</ta>
            <ta e="T122" id="Seg_3090" s="T121">think-FRQ-DUR.[3SG.S]</ta>
            <ta e="T123" id="Seg_3091" s="T122">where</ta>
            <ta e="T124" id="Seg_3092" s="T123">where.get.to-INF</ta>
            <ta e="T125" id="Seg_3093" s="T124">head-ILL-OBL.3SG</ta>
            <ta e="T126" id="Seg_3094" s="T125">fall.[3SG.S]</ta>
            <ta e="T127" id="Seg_3095" s="T126">mind.[NOM]-3SG</ta>
            <ta e="T128" id="Seg_3096" s="T127">one.needs</ta>
            <ta e="T129" id="Seg_3097" s="T128">pocket.[NOM]-1SG</ta>
            <ta e="T130" id="Seg_3098" s="T129">give.a.look-DUR-INF</ta>
            <ta e="T131" id="Seg_3099" s="T130">mix-INF</ta>
            <ta e="T132" id="Seg_3100" s="T131">pocket-LOC-OBL.3SG</ta>
            <ta e="T133" id="Seg_3101" s="T132">find-PST-3SG.O</ta>
            <ta e="T134" id="Seg_3102" s="T133">hawk-GEN</ta>
            <ta e="T135" id="Seg_3103" s="T134">skin.[NOM]</ta>
            <ta e="T136" id="Seg_3104" s="T135">stand-EP-CVB</ta>
            <ta e="T137" id="Seg_3105" s="T136">knead.the.skin-TR-INF-begin-PST-3SG.O</ta>
            <ta e="T138" id="Seg_3106" s="T137">hawk-GEN</ta>
            <ta e="T139" id="Seg_3107" s="T138">skin.[NOM]</ta>
            <ta e="T140" id="Seg_3108" s="T139">knead.the.skin-PST-3SG.O</ta>
            <ta e="T141" id="Seg_3109" s="T140">big-in.some.degree</ta>
            <ta e="T142" id="Seg_3110" s="T141">become-PST.[3SG.S]</ta>
            <ta e="T143" id="Seg_3111" s="T142">put.on-TR-PST-3SG.O</ta>
            <ta e="T144" id="Seg_3112" s="T143">head-ILL-OBL.3SG</ta>
            <ta e="T145" id="Seg_3113" s="T144">then</ta>
            <ta e="T146" id="Seg_3114" s="T145">on.one_s.back</ta>
            <ta e="T147" id="Seg_3115" s="T146">away</ta>
            <ta e="T148" id="Seg_3116" s="T147">take.out-TR-PST-3SG.O</ta>
            <ta e="T149" id="Seg_3117" s="T148">again</ta>
            <ta e="T150" id="Seg_3118" s="T149">knead.the.skin-INCH-PST-3SG.O</ta>
            <ta e="T151" id="Seg_3119" s="T150">and</ta>
            <ta e="T152" id="Seg_3120" s="T151">again</ta>
            <ta e="T153" id="Seg_3121" s="T152">put.on-TR-PST-3SG.O</ta>
            <ta e="T154" id="Seg_3122" s="T153">head-ILL-OBL.3SG</ta>
            <ta e="T155" id="Seg_3123" s="T154">body-ILL-OBL.3SG</ta>
            <ta e="T156" id="Seg_3124" s="T155">then</ta>
            <ta e="T157" id="Seg_3125" s="T156">again</ta>
            <ta e="T158" id="Seg_3126" s="T157">away</ta>
            <ta e="T159" id="Seg_3127" s="T158">take.out-PST-3SG.O</ta>
            <ta e="T160" id="Seg_3128" s="T159">skin-GEN-3SG</ta>
            <ta e="T161" id="Seg_3129" s="T160">out-ADV.EL</ta>
            <ta e="T162" id="Seg_3130" s="T161">and</ta>
            <ta e="T163" id="Seg_3131" s="T162">head-GEN-3SG</ta>
            <ta e="T164" id="Seg_3132" s="T163">out-ADV.EL</ta>
            <ta e="T165" id="Seg_3133" s="T164">again</ta>
            <ta e="T166" id="Seg_3134" s="T165">knead.the.skin-PST-3SG.O</ta>
            <ta e="T167" id="Seg_3135" s="T166">knead.the.skin-PST-3SG.O</ta>
            <ta e="T168" id="Seg_3136" s="T167">knead.the.skin-PST-3SG.O</ta>
            <ta e="T169" id="Seg_3137" s="T168">again</ta>
            <ta e="T170" id="Seg_3138" s="T169">think-FRQ-PST.[3SG.S]</ta>
            <ta e="T171" id="Seg_3139" s="T170">again</ta>
            <ta e="T172" id="Seg_3140" s="T171">knead.the.skin-TR-INF</ta>
            <ta e="T173" id="Seg_3141" s="T172">one.needs</ta>
            <ta e="T174" id="Seg_3142" s="T173">knead.the.skin-PST-3SG.O</ta>
            <ta e="T175" id="Seg_3143" s="T174">knead.the.skin-PST-3SG.O</ta>
            <ta e="T176" id="Seg_3144" s="T175">knead.the.skin-PST-3SG.O</ta>
            <ta e="T177" id="Seg_3145" s="T176">hawk-GEN</ta>
            <ta e="T178" id="Seg_3146" s="T177">skin-ACC</ta>
            <ta e="T179" id="Seg_3147" s="T178">big</ta>
            <ta e="T180" id="Seg_3148" s="T179">become.[3SG.S]</ta>
            <ta e="T181" id="Seg_3149" s="T180">now</ta>
            <ta e="T182" id="Seg_3150" s="T181">put.on-TR-PST-3SG.O</ta>
            <ta e="T183" id="Seg_3151" s="T182">hawk-GEN</ta>
            <ta e="T184" id="Seg_3152" s="T183">skin-ACC</ta>
            <ta e="T185" id="Seg_3153" s="T184">body-ILL-OBL.3SG</ta>
            <ta e="T186" id="Seg_3154" s="T185">and</ta>
            <ta e="T187" id="Seg_3155" s="T186">skin-ILL-OBL.3SG</ta>
            <ta e="T188" id="Seg_3156" s="T187">hawk-GEN</ta>
            <ta e="T189" id="Seg_3157" s="T188">skin.[NOM]</ta>
            <ta e="T190" id="Seg_3158" s="T189">suffice-PST-3SG.O</ta>
            <ta e="T191" id="Seg_3159" s="T190">leg-ILL-OBL.3SG</ta>
            <ta e="T192" id="Seg_3160" s="T191">from.here</ta>
            <ta e="T193" id="Seg_3161" s="T192">(s)he.[NOM]</ta>
            <ta e="T194" id="Seg_3162" s="T193">become.[3SG.S]</ta>
            <ta e="T195" id="Seg_3163" s="T194">hawk-TRL</ta>
            <ta e="T196" id="Seg_3164" s="T195">then</ta>
            <ta e="T197" id="Seg_3165" s="T196">fly.up-PFV-CO.[3SG.S]</ta>
            <ta e="T198" id="Seg_3166" s="T197">sit.down.[3SG.S]</ta>
            <ta e="T199" id="Seg_3167" s="T198">away-in.some.degree</ta>
            <ta e="T200" id="Seg_3168" s="T199">at.some.distance-EP-ADVZ</ta>
            <ta e="T201" id="Seg_3169" s="T200">sit-CVB</ta>
            <ta e="T202" id="Seg_3170" s="T201">tremble-CAUS-DUR.[3SG.S]</ta>
            <ta e="T203" id="Seg_3171" s="T202">feather-ACC-3SG</ta>
            <ta e="T204" id="Seg_3172" s="T203">tremble-CAUS-DUR-PST-3SG.O</ta>
            <ta e="T205" id="Seg_3173" s="T204">feather-ACC-3SG</ta>
            <ta e="T206" id="Seg_3174" s="T205">and</ta>
            <ta e="T207" id="Seg_3175" s="T206">fly.up-PFV-PST.[3SG.S]</ta>
            <ta e="T350" id="Seg_3176" s="T207">sky.[NOM]</ta>
            <ta e="T208" id="Seg_3177" s="T350">up-ADV.LOC</ta>
            <ta e="T209" id="Seg_3178" s="T208">and</ta>
            <ta e="T210" id="Seg_3179" s="T209">this.[NOM]-3SG</ta>
            <ta e="T211" id="Seg_3180" s="T210">fly.down-FRQ-PST.[3SG.S]</ta>
            <ta e="T212" id="Seg_3181" s="T211">turn-DUR-PST.[3SG.S]</ta>
            <ta e="T351" id="Seg_3182" s="T212">sky.[NOM]</ta>
            <ta e="T213" id="Seg_3183" s="T351">up-ADV.LOC</ta>
            <ta e="T214" id="Seg_3184" s="T213">then</ta>
            <ta e="T215" id="Seg_3185" s="T214">fly.up-PFV-PST.[3SG.S]</ta>
            <ta e="T216" id="Seg_3186" s="T215">fly-DUR-CVB</ta>
            <ta e="T217" id="Seg_3187" s="T216">river-ACC</ta>
            <ta e="T218" id="Seg_3188" s="T217">catch.up-TR-INF-begin-PST-3SG.O</ta>
            <ta e="T219" id="Seg_3189" s="T218">down.the.river-in.some.degree</ta>
            <ta e="T220" id="Seg_3190" s="T219">or</ta>
            <ta e="T221" id="Seg_3191" s="T220">away</ta>
            <ta e="T222" id="Seg_3192" s="T221">rise-DRV.[3SG.S]</ta>
            <ta e="T223" id="Seg_3193" s="T222">fly-DUR-CVB</ta>
            <ta e="T224" id="Seg_3194" s="T223">one</ta>
            <ta e="T225" id="Seg_3195" s="T224">middle-LOC</ta>
            <ta e="T226" id="Seg_3196" s="T225">see-CO-3SG.O</ta>
            <ta e="T227" id="Seg_3197" s="T226">river-GEN</ta>
            <ta e="T228" id="Seg_3198" s="T227">inside-LOC</ta>
            <ta e="T229" id="Seg_3199" s="T228">river-LOC</ta>
            <ta e="T230" id="Seg_3200" s="T229">boat.[NOM]</ta>
            <ta e="T231" id="Seg_3201" s="T230">go.away-INFER.[3SG.S]</ta>
            <ta e="T232" id="Seg_3202" s="T231">down-in.some.degree</ta>
            <ta e="T233" id="Seg_3203" s="T232">let.go-PST.[3SG.S]</ta>
            <ta e="T234" id="Seg_3204" s="T233">see-CO-3SG.O</ta>
            <ta e="T235" id="Seg_3205" s="T234">devil.[NOM]</ta>
            <ta e="T236" id="Seg_3206" s="T235">two</ta>
            <ta e="T237" id="Seg_3207" s="T236">daughter-PL-OBL.3SG-COM</ta>
            <ta e="T238" id="Seg_3208" s="T237">go.away-INFER-3PL</ta>
            <ta e="T239" id="Seg_3209" s="T238">devil.[NOM]</ta>
            <ta e="T240" id="Seg_3210" s="T239">see-CO-3SG.O</ta>
            <ta e="T241" id="Seg_3211" s="T240">hawk.[NOM]</ta>
            <ta e="T242" id="Seg_3212" s="T241">turn-DUR.[3SG.S]</ta>
            <ta e="T243" id="Seg_3213" s="T242">head-LOC-OBL.3SG</ta>
            <ta e="T244" id="Seg_3214" s="T243">fly-DRV-CVB</ta>
            <ta e="T245" id="Seg_3215" s="T244">turn-DUR.[3SG.S]</ta>
            <ta e="T246" id="Seg_3216" s="T245">oh</ta>
            <ta e="T247" id="Seg_3217" s="T246">daughter.[NOM]</ta>
            <ta e="T248" id="Seg_3218" s="T247">give.a.look-DUR-IMP.2SG.O</ta>
            <ta e="T249" id="Seg_3219" s="T248">how</ta>
            <ta e="T250" id="Seg_3220" s="T249">ornament-EP-ADJZ</ta>
            <ta e="T251" id="Seg_3221" s="T250">wild.animal-GEN</ta>
            <ta e="T252" id="Seg_3222" s="T251">ornament.[NOM]</ta>
            <ta e="T253" id="Seg_3223" s="T252">wild.animal.[NOM]</ta>
            <ta e="T254" id="Seg_3224" s="T253">fly-DRV-CO.[3SG.S]</ta>
            <ta e="T255" id="Seg_3225" s="T254">we.PL.GEN</ta>
            <ta e="T256" id="Seg_3226" s="T255">top-LOC-OBL.1PL</ta>
            <ta e="T257" id="Seg_3227" s="T256">devil.[NOM]</ta>
            <ta e="T258" id="Seg_3228" s="T257">sing.[3SG.S]</ta>
            <ta e="T259" id="Seg_3229" s="T258">daughter-PL-ILL-OBL.3SG</ta>
            <ta e="T260" id="Seg_3230" s="T259">daughter.[NOM]</ta>
            <ta e="T261" id="Seg_3231" s="T260">daughter.[NOM]</ta>
            <ta e="T262" id="Seg_3232" s="T261">give.a.look-DUR-IMP.2SG.O</ta>
            <ta e="T263" id="Seg_3233" s="T262">what.[NOM]</ta>
            <ta e="T264" id="Seg_3234" s="T263">we.PL.GEN</ta>
            <ta e="T265" id="Seg_3235" s="T264">top-LOC-OBL.1PL</ta>
            <ta e="T266" id="Seg_3236" s="T265">wild.animal.[NOM]-3SG</ta>
            <ta e="T267" id="Seg_3237" s="T266">how</ta>
            <ta e="T268" id="Seg_3238" s="T267">wild.animal.[NOM]</ta>
            <ta e="T269" id="Seg_3239" s="T268">fly-INFER.[3SG.S]</ta>
            <ta e="T270" id="Seg_3240" s="T269">hawk.[NOM]</ta>
            <ta e="T271" id="Seg_3241" s="T270">fly.up-PFV-CVB</ta>
            <ta e="T272" id="Seg_3242" s="T271">leave-PST.[3SG.S]</ta>
            <ta e="T273" id="Seg_3243" s="T272">(s)he-EP-GEN</ta>
            <ta e="T274" id="Seg_3244" s="T273">earlier-ADV.ILL</ta>
            <ta e="T275" id="Seg_3245" s="T274">river-ILL</ta>
            <ta e="T276" id="Seg_3246" s="T275">big</ta>
            <ta e="T277" id="Seg_3247" s="T276">larch-ILL</ta>
            <ta e="T278" id="Seg_3248" s="T277">river-GEN</ta>
            <ta e="T279" id="Seg_3249" s="T278">bank-LOC</ta>
            <ta e="T280" id="Seg_3250" s="T279">sit.down.[3SG.S]</ta>
            <ta e="T281" id="Seg_3251" s="T280">then</ta>
            <ta e="T282" id="Seg_3252" s="T281">two</ta>
            <ta e="T283" id="Seg_3253" s="T282">larch-GEN</ta>
            <ta e="T284" id="Seg_3254" s="T283">top-GEN-3SG</ta>
            <ta e="T285" id="Seg_3255" s="T284">together</ta>
            <ta e="T286" id="Seg_3256" s="T285">pull-MULO-CVB</ta>
            <ta e="T287" id="Seg_3257" s="T286">pull-HAB-PST-3SG.O</ta>
            <ta e="T288" id="Seg_3258" s="T287">hang-PST-3SG.O</ta>
            <ta e="T289" id="Seg_3259" s="T288">snare.[NOM]</ta>
            <ta e="T290" id="Seg_3260" s="T289">river-GEN</ta>
            <ta e="T291" id="Seg_3261" s="T290">inside-ILL</ta>
            <ta e="T292" id="Seg_3262" s="T291">inside-ILL</ta>
            <ta e="T293" id="Seg_3263" s="T292">devil.[NOM]</ta>
            <ta e="T294" id="Seg_3264" s="T293">come-IPFV.[3SG.S]</ta>
            <ta e="T295" id="Seg_3265" s="T294">sing-CVB</ta>
            <ta e="T296" id="Seg_3266" s="T295">two</ta>
            <ta e="T297" id="Seg_3267" s="T296">daughter-PL.[NOM]</ta>
            <ta e="T298" id="Seg_3268" s="T297">come-PST.[3SG.S]</ta>
            <ta e="T299" id="Seg_3269" s="T298">snare.[NOM]</ta>
            <ta e="T300" id="Seg_3270" s="T299">near-ILL</ta>
            <ta e="T301" id="Seg_3271" s="T300">boat-INSTR</ta>
            <ta e="T302" id="Seg_3272" s="T301">come-PST.[3SG.S]</ta>
            <ta e="T303" id="Seg_3273" s="T302">the.close.one-ADVZ-ADV.ILL</ta>
            <ta e="T304" id="Seg_3274" s="T303">snare-ILL</ta>
            <ta e="T305" id="Seg_3275" s="T304">come-PST.[3SG.S]</ta>
            <ta e="T306" id="Seg_3276" s="T305">boat-INSTR</ta>
            <ta e="T307" id="Seg_3277" s="T306">devil.[NOM]</ta>
            <ta e="T308" id="Seg_3278" s="T307">see-DRV-TR-3SG.O</ta>
            <ta e="T309" id="Seg_3279" s="T308">NEG</ta>
            <ta e="T310" id="Seg_3280" s="T309">snare-EP-ACC</ta>
            <ta e="T311" id="Seg_3281" s="T310">snare.[NOM]</ta>
            <ta e="T312" id="Seg_3282" s="T311">neck-PROL-OBL.3SG</ta>
            <ta e="T313" id="Seg_3283" s="T312">pull-PST-3SG.O</ta>
            <ta e="T314" id="Seg_3284" s="T313">two</ta>
            <ta e="T315" id="Seg_3285" s="T314">larch.[NOM]</ta>
            <ta e="T316" id="Seg_3286" s="T315">devil.[NOM]</ta>
            <ta e="T317" id="Seg_3287" s="T316">die-ACTN.[NOM]-3SG</ta>
            <ta e="T318" id="Seg_3288" s="T317">NEG</ta>
            <ta e="T319" id="Seg_3289" s="T318">know-3SG.O</ta>
            <ta e="T320" id="Seg_3290" s="T319">die-PST.NAR.[3SG.S]</ta>
            <ta e="T321" id="Seg_3291" s="T320">(s)he.[NOM]</ta>
            <ta e="T322" id="Seg_3292" s="T321">boat-LOC</ta>
            <ta e="T323" id="Seg_3293" s="T322">catch-PST-3SG.O</ta>
            <ta e="T324" id="Seg_3294" s="T323">devil-GEN</ta>
            <ta e="T325" id="Seg_3295" s="T324">daughter-PL-ACC-3SG</ta>
            <ta e="T326" id="Seg_3296" s="T325">urge-TR-PST-3SG.O</ta>
            <ta e="T327" id="Seg_3297" s="T326">river-GEN</ta>
            <ta e="T328" id="Seg_3298" s="T327">bank-ILL</ta>
            <ta e="T329" id="Seg_3299" s="T328">and</ta>
            <ta e="T330" id="Seg_3300" s="T329">devil-GEN</ta>
            <ta e="T331" id="Seg_3301" s="T330">daughter-ACC</ta>
            <ta e="T332" id="Seg_3302" s="T331">one</ta>
            <ta e="T333" id="Seg_3303" s="T332">living.being-ACC-3SG</ta>
            <ta e="T334" id="Seg_3304" s="T333">stamp-MOM-CVB</ta>
            <ta e="T335" id="Seg_3305" s="T334">put-PST-3SG.O</ta>
            <ta e="T336" id="Seg_3306" s="T335">clay-ILL</ta>
            <ta e="T337" id="Seg_3307" s="T336">two-ITER.NUM-EP-ADJZ</ta>
            <ta e="T338" id="Seg_3308" s="T337">daughter-ACC-3SG</ta>
            <ta e="T339" id="Seg_3309" s="T338">also</ta>
            <ta e="T340" id="Seg_3310" s="T339">such-ADVZ</ta>
            <ta e="T341" id="Seg_3311" s="T340">throw-PST-3SG.O</ta>
            <ta e="T342" id="Seg_3312" s="T341">and</ta>
            <ta e="T343" id="Seg_3313" s="T342">stamp-PST-3SG.O</ta>
            <ta e="T344" id="Seg_3314" s="T343">I.GEN</ta>
            <ta e="T345" id="Seg_3315" s="T344">tale.[NOM]-1SG</ta>
            <ta e="T346" id="Seg_3316" s="T345">who.[NOM]</ta>
            <ta e="T347" id="Seg_3317" s="T346">hear-TR-DUR-PST-3SG.O</ta>
            <ta e="T348" id="Seg_3318" s="T347">always-TRL</ta>
            <ta e="T349" id="Seg_3319" s="T348">eat-PST-3SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_3320" s="T0">Ичакичика.[NOM]</ta>
            <ta e="T2" id="Seg_3321" s="T1">жить-PST.NAR.[3SG.S]</ta>
            <ta e="T3" id="Seg_3322" s="T2">Ичакичика.[NOM]</ta>
            <ta e="T4" id="Seg_3323" s="T3">бабушка-OBL.3SG-COM</ta>
            <ta e="T5" id="Seg_3324" s="T4">Ичакичика</ta>
            <ta e="T6" id="Seg_3325" s="T5">отправиться-CO.[3SG.S]</ta>
            <ta e="T7" id="Seg_3326" s="T6">лес-ILL</ta>
            <ta e="T8" id="Seg_3327" s="T7">увидеть-CO-3SG.O</ta>
            <ta e="T9" id="Seg_3328" s="T8">человек.[NOM]</ta>
            <ta e="T10" id="Seg_3329" s="T9">рубить-DETR.[3SG.S]</ta>
            <ta e="T11" id="Seg_3330" s="T10">прийти-CO.[3SG.S]</ta>
            <ta e="T12" id="Seg_3331" s="T11">близкий-ADVZ-ADV.ILL</ta>
            <ta e="T13" id="Seg_3332" s="T12">увидеть-CO-3SG.O</ta>
            <ta e="T14" id="Seg_3333" s="T13">большой</ta>
            <ta e="T15" id="Seg_3334" s="T14">чёрт.[NOM]</ta>
            <ta e="T16" id="Seg_3335" s="T15">чёрт-ADJZ</ta>
            <ta e="T17" id="Seg_3336" s="T16">человек.[NOM]</ta>
            <ta e="T18" id="Seg_3337" s="T17">дедушка.[NOM]</ta>
            <ta e="T19" id="Seg_3338" s="T18">что.[NOM]</ta>
            <ta e="T20" id="Seg_3339" s="T19">сделать-TR-2SG.O</ta>
            <ta e="T21" id="Seg_3340" s="T20">я.NOM</ta>
            <ta e="T22" id="Seg_3341" s="T21">дерево-ACC</ta>
            <ta e="T23" id="Seg_3342" s="T22">рубить-FRQ-TR-1SG.O</ta>
            <ta e="T24" id="Seg_3343" s="T23">чёрт.[NOM]</ta>
            <ta e="T25" id="Seg_3344" s="T24">сказать-3SG.O</ta>
            <ta e="T26" id="Seg_3345" s="T25">сказать-PST-3SG.O</ta>
            <ta e="T27" id="Seg_3346" s="T26">я.NOM</ta>
            <ta e="T28" id="Seg_3347" s="T27">раньше</ta>
            <ta e="T29" id="Seg_3348" s="T28">этот</ta>
            <ta e="T30" id="Seg_3349" s="T29">дерево-ACC</ta>
            <ta e="T31" id="Seg_3350" s="T30">рука-INSTR</ta>
            <ta e="T32" id="Seg_3351" s="T31">на.части</ta>
            <ta e="T33" id="Seg_3352" s="T32">рвать-MULO-PFV-HAB-1SG.S</ta>
            <ta e="T34" id="Seg_3353" s="T33">ну.ка</ta>
            <ta e="T35" id="Seg_3354" s="T34">внук.[NOM]</ta>
            <ta e="T36" id="Seg_3355" s="T35">на.части</ta>
            <ta e="T37" id="Seg_3356" s="T36">рвать-MULO-IMP.2SG.O</ta>
            <ta e="T38" id="Seg_3357" s="T37">ну.ка</ta>
            <ta e="T39" id="Seg_3358" s="T38">пробовать-IMP.2SG.O</ta>
            <ta e="T40" id="Seg_3359" s="T39">чёрт-ADJZ</ta>
            <ta e="T41" id="Seg_3360" s="T40">дедушка.[NOM]-3SG</ta>
            <ta e="T42" id="Seg_3361" s="T41">дерево-ACC</ta>
            <ta e="T43" id="Seg_3362" s="T42">на.части</ta>
            <ta e="T44" id="Seg_3363" s="T43">рвать-MULO-CO-3SG.O</ta>
            <ta e="T45" id="Seg_3364" s="T44">ну.ка</ta>
            <ta e="T46" id="Seg_3365" s="T45">рука.[NOM]-2SG</ta>
            <ta e="T47" id="Seg_3366" s="T46">засунуть-IMP.2SG.O</ta>
            <ta e="T48" id="Seg_3367" s="T47">два</ta>
            <ta e="T49" id="Seg_3368" s="T48">рука.[NOM]-2SG</ta>
            <ta e="T50" id="Seg_3369" s="T49">надеть-TR-IMP.2SG.O</ta>
            <ta e="T51" id="Seg_3370" s="T50">толкать-MOM-IMP.2SG.O</ta>
            <ta e="T52" id="Seg_3371" s="T51">ну.ка</ta>
            <ta e="T53" id="Seg_3372" s="T52">дерево.[NOM]</ta>
            <ta e="T54" id="Seg_3373" s="T53">схватить-PST-3SG.O</ta>
            <ta e="T55" id="Seg_3374" s="T54">рука-ACC-3SG</ta>
            <ta e="T56" id="Seg_3375" s="T55">два</ta>
            <ta e="T57" id="Seg_3376" s="T56">рука.[NOM]-3SG</ta>
            <ta e="T58" id="Seg_3377" s="T57">он(а).[NOM]</ta>
            <ta e="T59" id="Seg_3378" s="T58">сидеть.[3SG.S]</ta>
            <ta e="T60" id="Seg_3379" s="T59">сила.[NOM]-3SG</ta>
            <ta e="T61" id="Seg_3380" s="T60">NEG.EX.[3SG.S]</ta>
            <ta e="T62" id="Seg_3381" s="T61">чёрт.[NOM]</ta>
            <ta e="T63" id="Seg_3382" s="T62">сказать-PST-3SG.O</ta>
            <ta e="T64" id="Seg_3383" s="T63">сидеть-IMP.2SG.S</ta>
            <ta e="T65" id="Seg_3384" s="T64">и</ta>
            <ta e="T66" id="Seg_3385" s="T65">сохнуть-IMP.2SG.S</ta>
            <ta e="T67" id="Seg_3386" s="T66">чёрт.[NOM]</ta>
            <ta e="T68" id="Seg_3387" s="T67">уйти-PST.[3SG.S]</ta>
            <ta e="T69" id="Seg_3388" s="T68">Ичакичика.[NOM]</ta>
            <ta e="T70" id="Seg_3389" s="T69">сидеть-PST.[3SG.S]</ta>
            <ta e="T71" id="Seg_3390" s="T70">сидеть-PST.[3SG.S]</ta>
            <ta e="T72" id="Seg_3391" s="T71">долго</ta>
            <ta e="T73" id="Seg_3392" s="T72">едва</ta>
            <ta e="T74" id="Seg_3393" s="T73">долго</ta>
            <ta e="T75" id="Seg_3394" s="T74">как</ta>
            <ta e="T76" id="Seg_3395" s="T75">едва</ta>
            <ta e="T77" id="Seg_3396" s="T76">как</ta>
            <ta e="T78" id="Seg_3397" s="T77">думать-FRQ-DUR-PST.[3SG.S]</ta>
            <ta e="T79" id="Seg_3398" s="T78">как</ta>
            <ta e="T80" id="Seg_3399" s="T79">выйти-INF</ta>
            <ta e="T81" id="Seg_3400" s="T80">ходить-DRV-PST.[3SG.S]</ta>
            <ta e="T82" id="Seg_3401" s="T81">и</ta>
            <ta e="T83" id="Seg_3402" s="T82">ходить-FRQ-PST.[3SG.S]</ta>
            <ta e="T84" id="Seg_3403" s="T83">быть-PST.NAR.[3SG.S]</ta>
            <ta e="T85" id="Seg_3404" s="T84">ударить-PTCP.PRS</ta>
            <ta e="T86" id="Seg_3405" s="T85">он(а)-EP-ALL</ta>
            <ta e="T87" id="Seg_3406" s="T86">прийти-CO.[3SG.S]</ta>
            <ta e="T88" id="Seg_3407" s="T87">медведь.[NOM]</ta>
            <ta e="T89" id="Seg_3408" s="T88">дедушка.[NOM]</ta>
            <ta e="T90" id="Seg_3409" s="T89">дерево-ACC</ta>
            <ta e="T91" id="Seg_3410" s="T90">в.разные.стороны</ta>
            <ta e="T92" id="Seg_3411" s="T91">рвать-MULO-IMP.2SG.O</ta>
            <ta e="T93" id="Seg_3412" s="T92">медведь.[NOM]</ta>
            <ta e="T94" id="Seg_3413" s="T93">на.части</ta>
            <ta e="T95" id="Seg_3414" s="T94">рвать-MULO-PST-3SG.O</ta>
            <ta e="T96" id="Seg_3415" s="T95">рука.[NOM]-3SG</ta>
            <ta e="T97" id="Seg_3416" s="T96">вытащить-TR-PST-3SG.O</ta>
            <ta e="T98" id="Seg_3417" s="T97">и</ta>
            <ta e="T99" id="Seg_3418" s="T98">рука.[NOM]-3SG</ta>
            <ta e="T100" id="Seg_3419" s="T99">свободный</ta>
            <ta e="T101" id="Seg_3420" s="T100">упасть-PST.[3SG.S]</ta>
            <ta e="T102" id="Seg_3421" s="T101">выйти.[3SG.S]</ta>
            <ta e="T103" id="Seg_3422" s="T102">дерево.[NOM]</ta>
            <ta e="T104" id="Seg_3423" s="T103">из-ADV.EL</ta>
            <ta e="T105" id="Seg_3424" s="T104">ходить-FRQ-CO.[3SG.S]</ta>
            <ta e="T106" id="Seg_3425" s="T105">ходить-DRV-CO.[3SG.S]</ta>
            <ta e="T107" id="Seg_3426" s="T106">думать-FRQ-DUR.[3SG.S]</ta>
            <ta e="T108" id="Seg_3427" s="T107">как</ta>
            <ta e="T109" id="Seg_3428" s="T108">догнать-INF</ta>
            <ta e="T110" id="Seg_3429" s="T109">и</ta>
            <ta e="T111" id="Seg_3430" s="T110">увидеть-DRV-INF</ta>
            <ta e="T112" id="Seg_3431" s="T111">чёрт-ACC</ta>
            <ta e="T113" id="Seg_3432" s="T112">ходить-FRQ-CO.[3SG.S]</ta>
            <ta e="T114" id="Seg_3433" s="T113">ходить-EP-DUR.[3SG.S]</ta>
            <ta e="T115" id="Seg_3434" s="T114">думать-FRQ-DUR-3SG.O</ta>
            <ta e="T116" id="Seg_3435" s="T115">как</ta>
            <ta e="T117" id="Seg_3436" s="T116">чёрт-ACC</ta>
            <ta e="T118" id="Seg_3437" s="T117">догнать-INF</ta>
            <ta e="T119" id="Seg_3438" s="T118">схватить-INF</ta>
            <ta e="T120" id="Seg_3439" s="T119">схватить-CVB</ta>
            <ta e="T121" id="Seg_3440" s="T120">взять-RES-OPT.[3SG.S]</ta>
            <ta e="T122" id="Seg_3441" s="T121">думать-FRQ-DUR.[3SG.S]</ta>
            <ta e="T123" id="Seg_3442" s="T122">куда</ta>
            <ta e="T124" id="Seg_3443" s="T123">куда.деваться-INF</ta>
            <ta e="T125" id="Seg_3444" s="T124">голова-ILL-OBL.3SG</ta>
            <ta e="T126" id="Seg_3445" s="T125">упасть.[3SG.S]</ta>
            <ta e="T127" id="Seg_3446" s="T126">ум.[NOM]-3SG</ta>
            <ta e="T128" id="Seg_3447" s="T127">нужно</ta>
            <ta e="T129" id="Seg_3448" s="T128">карман.[NOM]-1SG</ta>
            <ta e="T130" id="Seg_3449" s="T129">взглянуть-DUR-INF</ta>
            <ta e="T131" id="Seg_3450" s="T130">смешать-INF</ta>
            <ta e="T132" id="Seg_3451" s="T131">карман-LOC-OBL.3SG</ta>
            <ta e="T133" id="Seg_3452" s="T132">найти-PST-3SG.O</ta>
            <ta e="T134" id="Seg_3453" s="T133">ястреб-GEN</ta>
            <ta e="T135" id="Seg_3454" s="T134">шкура.[NOM]</ta>
            <ta e="T136" id="Seg_3455" s="T135">стоять-EP-CVB</ta>
            <ta e="T137" id="Seg_3456" s="T136">мять.шкуру-TR-INF-начать-PST-3SG.O</ta>
            <ta e="T138" id="Seg_3457" s="T137">ястреб-GEN</ta>
            <ta e="T139" id="Seg_3458" s="T138">шкура.[NOM]</ta>
            <ta e="T140" id="Seg_3459" s="T139">мять.шкуру-PST-3SG.O</ta>
            <ta e="T141" id="Seg_3460" s="T140">большой-в.некоторой.степени</ta>
            <ta e="T142" id="Seg_3461" s="T141">стать-PST.[3SG.S]</ta>
            <ta e="T143" id="Seg_3462" s="T142">надеть-TR-PST-3SG.O</ta>
            <ta e="T144" id="Seg_3463" s="T143">голова-ILL-OBL.3SG</ta>
            <ta e="T145" id="Seg_3464" s="T144">потом</ta>
            <ta e="T146" id="Seg_3465" s="T145">на.спину</ta>
            <ta e="T147" id="Seg_3466" s="T146">прочь</ta>
            <ta e="T148" id="Seg_3467" s="T147">вытащить-TR-PST-3SG.O</ta>
            <ta e="T149" id="Seg_3468" s="T148">снова</ta>
            <ta e="T150" id="Seg_3469" s="T149">мять.шкуру-INCH-PST-3SG.O</ta>
            <ta e="T151" id="Seg_3470" s="T150">и</ta>
            <ta e="T152" id="Seg_3471" s="T151">снова</ta>
            <ta e="T153" id="Seg_3472" s="T152">надеть-TR-PST-3SG.O</ta>
            <ta e="T154" id="Seg_3473" s="T153">голова-ILL-OBL.3SG</ta>
            <ta e="T155" id="Seg_3474" s="T154">тело-ILL-OBL.3SG</ta>
            <ta e="T156" id="Seg_3475" s="T155">потом</ta>
            <ta e="T157" id="Seg_3476" s="T156">опять</ta>
            <ta e="T158" id="Seg_3477" s="T157">прочь</ta>
            <ta e="T159" id="Seg_3478" s="T158">вытащить-PST-3SG.O</ta>
            <ta e="T160" id="Seg_3479" s="T159">кожа-GEN-3SG</ta>
            <ta e="T161" id="Seg_3480" s="T160">из-ADV.EL</ta>
            <ta e="T162" id="Seg_3481" s="T161">и</ta>
            <ta e="T163" id="Seg_3482" s="T162">голова-GEN-3SG</ta>
            <ta e="T164" id="Seg_3483" s="T163">из-ADV.EL</ta>
            <ta e="T165" id="Seg_3484" s="T164">опять</ta>
            <ta e="T166" id="Seg_3485" s="T165">мять.шкуру-PST-3SG.O</ta>
            <ta e="T167" id="Seg_3486" s="T166">мять.шкуру-PST-3SG.O</ta>
            <ta e="T168" id="Seg_3487" s="T167">мять.шкуру-PST-3SG.O</ta>
            <ta e="T169" id="Seg_3488" s="T168">опять</ta>
            <ta e="T170" id="Seg_3489" s="T169">думать-FRQ-PST.[3SG.S]</ta>
            <ta e="T171" id="Seg_3490" s="T170">опять</ta>
            <ta e="T172" id="Seg_3491" s="T171">мять.шкуру-TR-INF</ta>
            <ta e="T173" id="Seg_3492" s="T172">нужно</ta>
            <ta e="T174" id="Seg_3493" s="T173">мять.шкуру-PST-3SG.O</ta>
            <ta e="T175" id="Seg_3494" s="T174">мять.шкуру-PST-3SG.O</ta>
            <ta e="T176" id="Seg_3495" s="T175">мять.шкуру-PST-3SG.O</ta>
            <ta e="T177" id="Seg_3496" s="T176">ястреб-GEN</ta>
            <ta e="T178" id="Seg_3497" s="T177">шкура-ACC</ta>
            <ta e="T179" id="Seg_3498" s="T178">большой</ta>
            <ta e="T180" id="Seg_3499" s="T179">стать.[3SG.S]</ta>
            <ta e="T181" id="Seg_3500" s="T180">теперь</ta>
            <ta e="T182" id="Seg_3501" s="T181">надеть-TR-PST-3SG.O</ta>
            <ta e="T183" id="Seg_3502" s="T182">ястреб-GEN</ta>
            <ta e="T184" id="Seg_3503" s="T183">шкура-ACC</ta>
            <ta e="T185" id="Seg_3504" s="T184">тело-ILL-OBL.3SG</ta>
            <ta e="T186" id="Seg_3505" s="T185">и</ta>
            <ta e="T187" id="Seg_3506" s="T186">кожа-ILL-OBL.3SG</ta>
            <ta e="T188" id="Seg_3507" s="T187">ястреб-GEN</ta>
            <ta e="T189" id="Seg_3508" s="T188">шкура.[NOM]</ta>
            <ta e="T190" id="Seg_3509" s="T189">хватить-PST-3SG.O</ta>
            <ta e="T191" id="Seg_3510" s="T190">нога-ILL-OBL.3SG</ta>
            <ta e="T192" id="Seg_3511" s="T191">отсюда</ta>
            <ta e="T193" id="Seg_3512" s="T192">он(а).[NOM]</ta>
            <ta e="T194" id="Seg_3513" s="T193">стать.[3SG.S]</ta>
            <ta e="T195" id="Seg_3514" s="T194">ястреб-TRL</ta>
            <ta e="T196" id="Seg_3515" s="T195">потом</ta>
            <ta e="T197" id="Seg_3516" s="T196">взлететь-PFV-CO.[3SG.S]</ta>
            <ta e="T198" id="Seg_3517" s="T197">сесть.[3SG.S]</ta>
            <ta e="T199" id="Seg_3518" s="T198">прочь-в.некоторой.степени</ta>
            <ta e="T200" id="Seg_3519" s="T199">поодаль-EP-ADVZ</ta>
            <ta e="T201" id="Seg_3520" s="T200">сидеть-CVB</ta>
            <ta e="T202" id="Seg_3521" s="T201">трястись-CAUS-DUR.[3SG.S]</ta>
            <ta e="T203" id="Seg_3522" s="T202">перо-ACC-3SG</ta>
            <ta e="T204" id="Seg_3523" s="T203">трястись-CAUS-DUR-PST-3SG.O</ta>
            <ta e="T205" id="Seg_3524" s="T204">перо-ACC-3SG</ta>
            <ta e="T206" id="Seg_3525" s="T205">и</ta>
            <ta e="T207" id="Seg_3526" s="T206">взлететь-PFV-PST.[3SG.S]</ta>
            <ta e="T350" id="Seg_3527" s="T207">небо.[NOM]</ta>
            <ta e="T208" id="Seg_3528" s="T350">вверх-ADV.LOC</ta>
            <ta e="T209" id="Seg_3529" s="T208">и</ta>
            <ta e="T210" id="Seg_3530" s="T209">это.[NOM]-3SG</ta>
            <ta e="T211" id="Seg_3531" s="T210">планировать-FRQ-PST.[3SG.S]</ta>
            <ta e="T212" id="Seg_3532" s="T211">повернуться-DUR-PST.[3SG.S]</ta>
            <ta e="T351" id="Seg_3533" s="T212">небо.[NOM]</ta>
            <ta e="T213" id="Seg_3534" s="T351">вверх-ADV.LOC</ta>
            <ta e="T214" id="Seg_3535" s="T213">потом</ta>
            <ta e="T215" id="Seg_3536" s="T214">взлететь-PFV-PST.[3SG.S]</ta>
            <ta e="T216" id="Seg_3537" s="T215">летать-DUR-CVB</ta>
            <ta e="T217" id="Seg_3538" s="T216">река-ACC</ta>
            <ta e="T218" id="Seg_3539" s="T217">догонять-TR-INF-начать-PST-3SG.O</ta>
            <ta e="T219" id="Seg_3540" s="T218">вниз.по.течению.реки-в.некоторой.степени</ta>
            <ta e="T220" id="Seg_3541" s="T219">или</ta>
            <ta e="T221" id="Seg_3542" s="T220">прочь</ta>
            <ta e="T222" id="Seg_3543" s="T221">поднять-DRV.[3SG.S]</ta>
            <ta e="T223" id="Seg_3544" s="T222">летать-DUR-CVB</ta>
            <ta e="T224" id="Seg_3545" s="T223">один</ta>
            <ta e="T225" id="Seg_3546" s="T224">середина-LOC</ta>
            <ta e="T226" id="Seg_3547" s="T225">увидеть-CO-3SG.O</ta>
            <ta e="T227" id="Seg_3548" s="T226">река-GEN</ta>
            <ta e="T228" id="Seg_3549" s="T227">внутри-LOC</ta>
            <ta e="T229" id="Seg_3550" s="T228">река-LOC</ta>
            <ta e="T230" id="Seg_3551" s="T229">ветка.[NOM]</ta>
            <ta e="T231" id="Seg_3552" s="T230">уйти-INFER.[3SG.S]</ta>
            <ta e="T232" id="Seg_3553" s="T231">вниз-в.некоторой.степени</ta>
            <ta e="T233" id="Seg_3554" s="T232">пустить-PST.[3SG.S]</ta>
            <ta e="T234" id="Seg_3555" s="T233">увидеть-CO-3SG.O</ta>
            <ta e="T235" id="Seg_3556" s="T234">чёрт.[NOM]</ta>
            <ta e="T236" id="Seg_3557" s="T235">два</ta>
            <ta e="T237" id="Seg_3558" s="T236">дочь-PL-OBL.3SG-COM</ta>
            <ta e="T238" id="Seg_3559" s="T237">уйти-INFER-3PL</ta>
            <ta e="T239" id="Seg_3560" s="T238">чёрт.[NOM]</ta>
            <ta e="T240" id="Seg_3561" s="T239">увидеть-CO-3SG.O</ta>
            <ta e="T241" id="Seg_3562" s="T240">ястреб.[NOM]</ta>
            <ta e="T242" id="Seg_3563" s="T241">повернуться-DUR.[3SG.S]</ta>
            <ta e="T243" id="Seg_3564" s="T242">голова-LOC-OBL.3SG</ta>
            <ta e="T244" id="Seg_3565" s="T243">летать-DRV-CVB</ta>
            <ta e="T245" id="Seg_3566" s="T244">повернуться-DUR.[3SG.S]</ta>
            <ta e="T246" id="Seg_3567" s="T245">ой</ta>
            <ta e="T247" id="Seg_3568" s="T246">дочь.[NOM]</ta>
            <ta e="T248" id="Seg_3569" s="T247">взглянуть-DUR-IMP.2SG.O</ta>
            <ta e="T249" id="Seg_3570" s="T248">как</ta>
            <ta e="T250" id="Seg_3571" s="T249">узор-EP-ADJZ</ta>
            <ta e="T251" id="Seg_3572" s="T250">зверь-GEN</ta>
            <ta e="T252" id="Seg_3573" s="T251">узор.[NOM]</ta>
            <ta e="T253" id="Seg_3574" s="T252">зверь.[NOM]</ta>
            <ta e="T254" id="Seg_3575" s="T253">летать-DRV-CO.[3SG.S]</ta>
            <ta e="T255" id="Seg_3576" s="T254">мы.PL.GEN</ta>
            <ta e="T256" id="Seg_3577" s="T255">верхняя.часть-LOC-OBL.1PL</ta>
            <ta e="T257" id="Seg_3578" s="T256">чёрт.[NOM]</ta>
            <ta e="T258" id="Seg_3579" s="T257">петь.[3SG.S]</ta>
            <ta e="T259" id="Seg_3580" s="T258">дочь-PL-ILL-OBL.3SG</ta>
            <ta e="T260" id="Seg_3581" s="T259">дочь.[NOM]</ta>
            <ta e="T261" id="Seg_3582" s="T260">дочь.[NOM]</ta>
            <ta e="T262" id="Seg_3583" s="T261">взглянуть-DUR-IMP.2SG.O</ta>
            <ta e="T263" id="Seg_3584" s="T262">что.[NOM]</ta>
            <ta e="T264" id="Seg_3585" s="T263">мы.PL.GEN</ta>
            <ta e="T265" id="Seg_3586" s="T264">верх-LOC-OBL.1PL</ta>
            <ta e="T266" id="Seg_3587" s="T265">зверь.[NOM]-3SG</ta>
            <ta e="T267" id="Seg_3588" s="T266">как</ta>
            <ta e="T268" id="Seg_3589" s="T267">зверь.[NOM]</ta>
            <ta e="T269" id="Seg_3590" s="T268">летать-INFER.[3SG.S]</ta>
            <ta e="T270" id="Seg_3591" s="T269">ястреб.[NOM]</ta>
            <ta e="T271" id="Seg_3592" s="T270">взлететь-PFV-CVB</ta>
            <ta e="T272" id="Seg_3593" s="T271">отправиться-PST.[3SG.S]</ta>
            <ta e="T273" id="Seg_3594" s="T272">он(а)-EP-GEN</ta>
            <ta e="T274" id="Seg_3595" s="T273">раньше-ADV.ILL</ta>
            <ta e="T275" id="Seg_3596" s="T274">река-ILL</ta>
            <ta e="T276" id="Seg_3597" s="T275">большой</ta>
            <ta e="T277" id="Seg_3598" s="T276">лиственница-ILL</ta>
            <ta e="T278" id="Seg_3599" s="T277">река-GEN</ta>
            <ta e="T279" id="Seg_3600" s="T278">берег-LOC</ta>
            <ta e="T280" id="Seg_3601" s="T279">сесть.[3SG.S]</ta>
            <ta e="T281" id="Seg_3602" s="T280">потом</ta>
            <ta e="T282" id="Seg_3603" s="T281">два</ta>
            <ta e="T283" id="Seg_3604" s="T282">лиственница-GEN</ta>
            <ta e="T284" id="Seg_3605" s="T283">верхняя.часть-GEN-3SG</ta>
            <ta e="T285" id="Seg_3606" s="T284">вместе</ta>
            <ta e="T286" id="Seg_3607" s="T285">тянуть-MULO-CVB</ta>
            <ta e="T287" id="Seg_3608" s="T286">тянуть-HAB-PST-3SG.O</ta>
            <ta e="T288" id="Seg_3609" s="T287">повесить-PST-3SG.O</ta>
            <ta e="T289" id="Seg_3610" s="T288">силок.[NOM]</ta>
            <ta e="T290" id="Seg_3611" s="T289">река-GEN</ta>
            <ta e="T291" id="Seg_3612" s="T290">внутри-ILL</ta>
            <ta e="T292" id="Seg_3613" s="T291">нутро-ILL</ta>
            <ta e="T293" id="Seg_3614" s="T292">чёрт.[NOM]</ta>
            <ta e="T294" id="Seg_3615" s="T293">прийти-IPFV.[3SG.S]</ta>
            <ta e="T295" id="Seg_3616" s="T294">петь-CVB</ta>
            <ta e="T296" id="Seg_3617" s="T295">два</ta>
            <ta e="T297" id="Seg_3618" s="T296">дочь-PL.[NOM]</ta>
            <ta e="T298" id="Seg_3619" s="T297">прийти-PST.[3SG.S]</ta>
            <ta e="T299" id="Seg_3620" s="T298">силок.[NOM]</ta>
            <ta e="T300" id="Seg_3621" s="T299">рядом-ILL</ta>
            <ta e="T301" id="Seg_3622" s="T300">ветка-INSTR</ta>
            <ta e="T302" id="Seg_3623" s="T301">прийти-PST.[3SG.S]</ta>
            <ta e="T303" id="Seg_3624" s="T302">близкий-ADVZ-ADV.ILL</ta>
            <ta e="T304" id="Seg_3625" s="T303">силок-ILL</ta>
            <ta e="T305" id="Seg_3626" s="T304">прийти-PST.[3SG.S]</ta>
            <ta e="T306" id="Seg_3627" s="T305">ветка-INSTR</ta>
            <ta e="T307" id="Seg_3628" s="T306">чёрт.[NOM]</ta>
            <ta e="T308" id="Seg_3629" s="T307">увидеть-DRV-TR-3SG.O</ta>
            <ta e="T309" id="Seg_3630" s="T308">NEG</ta>
            <ta e="T310" id="Seg_3631" s="T309">силок-EP-ACC</ta>
            <ta e="T311" id="Seg_3632" s="T310">силок.[NOM]</ta>
            <ta e="T312" id="Seg_3633" s="T311">шея-PROL-OBL.3SG</ta>
            <ta e="T313" id="Seg_3634" s="T312">дернуть-PST-3SG.O</ta>
            <ta e="T314" id="Seg_3635" s="T313">два</ta>
            <ta e="T315" id="Seg_3636" s="T314">лиственница.[NOM]</ta>
            <ta e="T316" id="Seg_3637" s="T315">чёрт.[NOM]</ta>
            <ta e="T317" id="Seg_3638" s="T316">умереть-ACTN.[NOM]-3SG</ta>
            <ta e="T318" id="Seg_3639" s="T317">NEG</ta>
            <ta e="T319" id="Seg_3640" s="T318">знать-3SG.O</ta>
            <ta e="T320" id="Seg_3641" s="T319">умереть-PST.NAR.[3SG.S]</ta>
            <ta e="T321" id="Seg_3642" s="T320">он(а).[NOM]</ta>
            <ta e="T322" id="Seg_3643" s="T321">ветка-LOC</ta>
            <ta e="T323" id="Seg_3644" s="T322">схватить-PST-3SG.O</ta>
            <ta e="T324" id="Seg_3645" s="T323">чёрт-GEN</ta>
            <ta e="T325" id="Seg_3646" s="T324">дочь-PL-ACC-3SG</ta>
            <ta e="T326" id="Seg_3647" s="T325">погнать-TR-PST-3SG.O</ta>
            <ta e="T327" id="Seg_3648" s="T326">река-GEN</ta>
            <ta e="T328" id="Seg_3649" s="T327">берег-ILL</ta>
            <ta e="T329" id="Seg_3650" s="T328">и</ta>
            <ta e="T330" id="Seg_3651" s="T329">чёрт-GEN</ta>
            <ta e="T331" id="Seg_3652" s="T330">дочь-ACC</ta>
            <ta e="T332" id="Seg_3653" s="T331">один</ta>
            <ta e="T333" id="Seg_3654" s="T332">живое.существо-ACC-3SG</ta>
            <ta e="T334" id="Seg_3655" s="T333">топтать-MOM-CVB</ta>
            <ta e="T335" id="Seg_3656" s="T334">положить-PST-3SG.O</ta>
            <ta e="T336" id="Seg_3657" s="T335">глина-ILL</ta>
            <ta e="T337" id="Seg_3658" s="T336">два-ITER.NUM-EP-ADJZ</ta>
            <ta e="T338" id="Seg_3659" s="T337">дочь-ACC-3SG</ta>
            <ta e="T339" id="Seg_3660" s="T338">тоже</ta>
            <ta e="T340" id="Seg_3661" s="T339">такой-ADVZ</ta>
            <ta e="T341" id="Seg_3662" s="T340">бросать-PST-3SG.O</ta>
            <ta e="T342" id="Seg_3663" s="T341">и</ta>
            <ta e="T343" id="Seg_3664" s="T342">топтать-PST-3SG.O</ta>
            <ta e="T344" id="Seg_3665" s="T343">я.GEN</ta>
            <ta e="T345" id="Seg_3666" s="T344">сказка.[NOM]-1SG</ta>
            <ta e="T346" id="Seg_3667" s="T345">кто.[NOM]</ta>
            <ta e="T347" id="Seg_3668" s="T346">слушать-TR-DUR-PST-3SG.O</ta>
            <ta e="T348" id="Seg_3669" s="T347">всегда-TRL</ta>
            <ta e="T349" id="Seg_3670" s="T348">съесть-PST-3SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_3671" s="T0">nprop-n:case</ta>
            <ta e="T2" id="Seg_3672" s="T1">v-v:tense-v:pn</ta>
            <ta e="T3" id="Seg_3673" s="T2">nprop-n:case</ta>
            <ta e="T4" id="Seg_3674" s="T3">n-n:obl.poss-n:case</ta>
            <ta e="T5" id="Seg_3675" s="T4">nprop</ta>
            <ta e="T6" id="Seg_3676" s="T5">v-v:ins-v:pn</ta>
            <ta e="T7" id="Seg_3677" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_3678" s="T7">v-v:ins-v:pn</ta>
            <ta e="T9" id="Seg_3679" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_3680" s="T9">v-v&gt;v-v:pn</ta>
            <ta e="T11" id="Seg_3681" s="T10">v-v:ins-v:pn</ta>
            <ta e="T12" id="Seg_3682" s="T11">n-n&gt;adv-adv:case</ta>
            <ta e="T13" id="Seg_3683" s="T12">v-v:ins-v:pn</ta>
            <ta e="T14" id="Seg_3684" s="T13">adj</ta>
            <ta e="T15" id="Seg_3685" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_3686" s="T15">n-n&gt;adj</ta>
            <ta e="T17" id="Seg_3687" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_3688" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_3689" s="T18">interrog-n:case</ta>
            <ta e="T20" id="Seg_3690" s="T19">v-v&gt;v-v:pn</ta>
            <ta e="T21" id="Seg_3691" s="T20">pers</ta>
            <ta e="T22" id="Seg_3692" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_3693" s="T22">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T24" id="Seg_3694" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_3695" s="T24">v-v:pn</ta>
            <ta e="T26" id="Seg_3696" s="T25">v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_3697" s="T26">pers</ta>
            <ta e="T28" id="Seg_3698" s="T27">adv</ta>
            <ta e="T29" id="Seg_3699" s="T28">dem</ta>
            <ta e="T30" id="Seg_3700" s="T29">n-n:case</ta>
            <ta e="T31" id="Seg_3701" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_3702" s="T31">preverb</ta>
            <ta e="T33" id="Seg_3703" s="T32">v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T34" id="Seg_3704" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_3705" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_3706" s="T35">preverb</ta>
            <ta e="T37" id="Seg_3707" s="T36">v-v&gt;v-v:mood.pn</ta>
            <ta e="T38" id="Seg_3708" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_3709" s="T38">v-v:mood.pn</ta>
            <ta e="T40" id="Seg_3710" s="T39">n-n&gt;adj</ta>
            <ta e="T41" id="Seg_3711" s="T40">n-n:case-n:poss</ta>
            <ta e="T42" id="Seg_3712" s="T41">n-n:case</ta>
            <ta e="T43" id="Seg_3713" s="T42">preverb</ta>
            <ta e="T44" id="Seg_3714" s="T43">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T45" id="Seg_3715" s="T44">ptcl</ta>
            <ta e="T46" id="Seg_3716" s="T45">n-n:case-n:poss</ta>
            <ta e="T47" id="Seg_3717" s="T46">v-v:mood.pn</ta>
            <ta e="T48" id="Seg_3718" s="T47">num</ta>
            <ta e="T49" id="Seg_3719" s="T48">n-n:case-n:poss</ta>
            <ta e="T50" id="Seg_3720" s="T49">v-v&gt;v-v:mood.pn</ta>
            <ta e="T51" id="Seg_3721" s="T50">v-v&gt;v-v:mood.pn</ta>
            <ta e="T52" id="Seg_3722" s="T51">ptcl</ta>
            <ta e="T53" id="Seg_3723" s="T52">n-n:case</ta>
            <ta e="T54" id="Seg_3724" s="T53">v-v:tense-v:pn</ta>
            <ta e="T55" id="Seg_3725" s="T54">n-n:case-n:poss</ta>
            <ta e="T56" id="Seg_3726" s="T55">num</ta>
            <ta e="T57" id="Seg_3727" s="T56">n-n:case-n:poss</ta>
            <ta e="T58" id="Seg_3728" s="T57">pers-n:case</ta>
            <ta e="T59" id="Seg_3729" s="T58">v-v:pn</ta>
            <ta e="T60" id="Seg_3730" s="T59">n-n:case-n:poss</ta>
            <ta e="T61" id="Seg_3731" s="T60">v-v:pn</ta>
            <ta e="T62" id="Seg_3732" s="T61">n-n:case</ta>
            <ta e="T63" id="Seg_3733" s="T62">v-v:tense-v:pn</ta>
            <ta e="T64" id="Seg_3734" s="T63">v-v:mood.pn</ta>
            <ta e="T65" id="Seg_3735" s="T64">conj</ta>
            <ta e="T66" id="Seg_3736" s="T65">v-v:mood.pn</ta>
            <ta e="T67" id="Seg_3737" s="T66">n-n:case</ta>
            <ta e="T68" id="Seg_3738" s="T67">v-v:tense-v:pn</ta>
            <ta e="T69" id="Seg_3739" s="T68">nprop-n:case</ta>
            <ta e="T70" id="Seg_3740" s="T69">v-v:tense-v:pn</ta>
            <ta e="T71" id="Seg_3741" s="T70">v-v:tense-v:pn</ta>
            <ta e="T72" id="Seg_3742" s="T71">adv</ta>
            <ta e="T73" id="Seg_3743" s="T72">ptcl</ta>
            <ta e="T74" id="Seg_3744" s="T73">adv</ta>
            <ta e="T75" id="Seg_3745" s="T74">interrog</ta>
            <ta e="T76" id="Seg_3746" s="T75">ptcl</ta>
            <ta e="T77" id="Seg_3747" s="T76">interrog</ta>
            <ta e="T78" id="Seg_3748" s="T77">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T79" id="Seg_3749" s="T78">interrog</ta>
            <ta e="T80" id="Seg_3750" s="T79">v-v:inf</ta>
            <ta e="T81" id="Seg_3751" s="T80">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T82" id="Seg_3752" s="T81">conj</ta>
            <ta e="T83" id="Seg_3753" s="T82">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T84" id="Seg_3754" s="T83">v-v:tense-v:pn</ta>
            <ta e="T85" id="Seg_3755" s="T84">v-v&gt;ptcp</ta>
            <ta e="T86" id="Seg_3756" s="T85">pers-n:ins-n:case</ta>
            <ta e="T87" id="Seg_3757" s="T86">v-v:ins-v:pn</ta>
            <ta e="T88" id="Seg_3758" s="T87">n-n:case</ta>
            <ta e="T89" id="Seg_3759" s="T88">n-n:case</ta>
            <ta e="T90" id="Seg_3760" s="T89">n-n:case</ta>
            <ta e="T91" id="Seg_3761" s="T90">preverb</ta>
            <ta e="T92" id="Seg_3762" s="T91">v-v&gt;v-v:mood.pn</ta>
            <ta e="T93" id="Seg_3763" s="T92">n-n:case</ta>
            <ta e="T94" id="Seg_3764" s="T93">preverb</ta>
            <ta e="T95" id="Seg_3765" s="T94">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T96" id="Seg_3766" s="T95">n-n:case-n:poss</ta>
            <ta e="T97" id="Seg_3767" s="T96">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T98" id="Seg_3768" s="T97">conj</ta>
            <ta e="T99" id="Seg_3769" s="T98">n-n:case-n:poss</ta>
            <ta e="T100" id="Seg_3770" s="T99">adj</ta>
            <ta e="T101" id="Seg_3771" s="T100">v-v:tense-v:pn</ta>
            <ta e="T102" id="Seg_3772" s="T101">v-v:pn</ta>
            <ta e="T103" id="Seg_3773" s="T102">n-n:case</ta>
            <ta e="T104" id="Seg_3774" s="T103">pp-adv:case</ta>
            <ta e="T105" id="Seg_3775" s="T104">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T106" id="Seg_3776" s="T105">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T107" id="Seg_3777" s="T106">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T108" id="Seg_3778" s="T107">interrog</ta>
            <ta e="T109" id="Seg_3779" s="T108">v-v:inf</ta>
            <ta e="T110" id="Seg_3780" s="T109">conj</ta>
            <ta e="T111" id="Seg_3781" s="T110">v-v&gt;v-v:inf</ta>
            <ta e="T112" id="Seg_3782" s="T111">n-n:case</ta>
            <ta e="T113" id="Seg_3783" s="T112">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T114" id="Seg_3784" s="T113">v-v:ins-v&gt;v-v:pn</ta>
            <ta e="T115" id="Seg_3785" s="T114">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T116" id="Seg_3786" s="T115">conj</ta>
            <ta e="T117" id="Seg_3787" s="T116">n-n:case</ta>
            <ta e="T118" id="Seg_3788" s="T117">v-v:inf</ta>
            <ta e="T119" id="Seg_3789" s="T118">v-v:inf</ta>
            <ta e="T120" id="Seg_3790" s="T119">v-v&gt;adv</ta>
            <ta e="T121" id="Seg_3791" s="T120">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T122" id="Seg_3792" s="T121">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T123" id="Seg_3793" s="T122">interrog</ta>
            <ta e="T124" id="Seg_3794" s="T123">qv-v:inf</ta>
            <ta e="T125" id="Seg_3795" s="T124">n-n:case-n:obl.poss</ta>
            <ta e="T126" id="Seg_3796" s="T125">v-v:pn</ta>
            <ta e="T127" id="Seg_3797" s="T126">n-n:case-n:poss</ta>
            <ta e="T128" id="Seg_3798" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_3799" s="T128">n-n:case-n:poss</ta>
            <ta e="T130" id="Seg_3800" s="T129">v-v&gt;v-v:inf</ta>
            <ta e="T131" id="Seg_3801" s="T130">v-v:inf</ta>
            <ta e="T132" id="Seg_3802" s="T131">n-n:case-n:obl.poss</ta>
            <ta e="T133" id="Seg_3803" s="T132">v-v:tense-v:pn</ta>
            <ta e="T134" id="Seg_3804" s="T133">n-n:case</ta>
            <ta e="T135" id="Seg_3805" s="T134">n-n:case</ta>
            <ta e="T136" id="Seg_3806" s="T135">v-v:ins-v&gt;adv</ta>
            <ta e="T137" id="Seg_3807" s="T136">v-v&gt;v-v:inf-v-v:tense-v:pn</ta>
            <ta e="T138" id="Seg_3808" s="T137">n-n:case</ta>
            <ta e="T139" id="Seg_3809" s="T138">n-n:case</ta>
            <ta e="T140" id="Seg_3810" s="T139">v-v:tense-v:pn</ta>
            <ta e="T141" id="Seg_3811" s="T140">adj-adj&gt;adj</ta>
            <ta e="T142" id="Seg_3812" s="T141">v-v:tense-v:pn</ta>
            <ta e="T143" id="Seg_3813" s="T142">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T144" id="Seg_3814" s="T143">n-n:case-n:obl.poss</ta>
            <ta e="T145" id="Seg_3815" s="T144">adv</ta>
            <ta e="T146" id="Seg_3816" s="T145">adv</ta>
            <ta e="T147" id="Seg_3817" s="T146">preverb</ta>
            <ta e="T148" id="Seg_3818" s="T147">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T149" id="Seg_3819" s="T148">adv</ta>
            <ta e="T150" id="Seg_3820" s="T149">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T151" id="Seg_3821" s="T150">conj</ta>
            <ta e="T152" id="Seg_3822" s="T151">adv</ta>
            <ta e="T153" id="Seg_3823" s="T152">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T154" id="Seg_3824" s="T153">n-n:case-n:obl.poss</ta>
            <ta e="T155" id="Seg_3825" s="T154">n-n:case-n:obl.poss</ta>
            <ta e="T156" id="Seg_3826" s="T155">adv</ta>
            <ta e="T157" id="Seg_3827" s="T156">adv</ta>
            <ta e="T158" id="Seg_3828" s="T157">preverb</ta>
            <ta e="T159" id="Seg_3829" s="T158">v-v:tense-v:pn</ta>
            <ta e="T160" id="Seg_3830" s="T159">n-n:case-n:poss</ta>
            <ta e="T161" id="Seg_3831" s="T160">pp-adv:case</ta>
            <ta e="T162" id="Seg_3832" s="T161">conj</ta>
            <ta e="T163" id="Seg_3833" s="T162">n-n:case-n:poss</ta>
            <ta e="T164" id="Seg_3834" s="T163">pp-adv:case</ta>
            <ta e="T165" id="Seg_3835" s="T164">adv</ta>
            <ta e="T166" id="Seg_3836" s="T165">v-v:tense-v:pn</ta>
            <ta e="T167" id="Seg_3837" s="T166">v-v:tense-v:pn</ta>
            <ta e="T168" id="Seg_3838" s="T167">v-v:tense-v:pn</ta>
            <ta e="T169" id="Seg_3839" s="T168">adv</ta>
            <ta e="T170" id="Seg_3840" s="T169">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T171" id="Seg_3841" s="T170">adv</ta>
            <ta e="T172" id="Seg_3842" s="T171">v-v&gt;v-v:inf</ta>
            <ta e="T173" id="Seg_3843" s="T172">ptcl</ta>
            <ta e="T174" id="Seg_3844" s="T173">v-v:tense-v:pn</ta>
            <ta e="T175" id="Seg_3845" s="T174">v-v:tense-v:pn</ta>
            <ta e="T176" id="Seg_3846" s="T175">v-v:tense-v:pn</ta>
            <ta e="T177" id="Seg_3847" s="T176">n-n:case</ta>
            <ta e="T178" id="Seg_3848" s="T177">n-n:case</ta>
            <ta e="T179" id="Seg_3849" s="T178">adj</ta>
            <ta e="T180" id="Seg_3850" s="T179">v-v:pn</ta>
            <ta e="T181" id="Seg_3851" s="T180">adv</ta>
            <ta e="T182" id="Seg_3852" s="T181">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T183" id="Seg_3853" s="T182">n-n:case</ta>
            <ta e="T184" id="Seg_3854" s="T183">n-n:case</ta>
            <ta e="T185" id="Seg_3855" s="T184">n-n:case-n:obl.poss</ta>
            <ta e="T186" id="Seg_3856" s="T185">conj</ta>
            <ta e="T187" id="Seg_3857" s="T186">n-n:case-n:obl.poss</ta>
            <ta e="T188" id="Seg_3858" s="T187">n-n:case</ta>
            <ta e="T189" id="Seg_3859" s="T188">n-n:case</ta>
            <ta e="T190" id="Seg_3860" s="T189">v-v:tense-v:pn</ta>
            <ta e="T191" id="Seg_3861" s="T190">n-n:case-n:obl.poss</ta>
            <ta e="T192" id="Seg_3862" s="T191">adv</ta>
            <ta e="T193" id="Seg_3863" s="T192">pers-n:case</ta>
            <ta e="T194" id="Seg_3864" s="T193">v-v:pn</ta>
            <ta e="T195" id="Seg_3865" s="T194">n-n:case</ta>
            <ta e="T196" id="Seg_3866" s="T195">adv</ta>
            <ta e="T197" id="Seg_3867" s="T196">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T198" id="Seg_3868" s="T197">v-v:pn</ta>
            <ta e="T199" id="Seg_3869" s="T198">adv-adv&gt;adv</ta>
            <ta e="T200" id="Seg_3870" s="T199">adv-n:ins-n&gt;adv</ta>
            <ta e="T201" id="Seg_3871" s="T200">v-v&gt;adv</ta>
            <ta e="T202" id="Seg_3872" s="T201">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T203" id="Seg_3873" s="T202">n-n:case-n:poss</ta>
            <ta e="T204" id="Seg_3874" s="T203">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T205" id="Seg_3875" s="T204">n-n:case-n:poss</ta>
            <ta e="T206" id="Seg_3876" s="T205">conj</ta>
            <ta e="T207" id="Seg_3877" s="T206">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T350" id="Seg_3878" s="T207">n-n:case</ta>
            <ta e="T208" id="Seg_3879" s="T350">adv&gt;adv</ta>
            <ta e="T209" id="Seg_3880" s="T208">conj</ta>
            <ta e="T210" id="Seg_3881" s="T209">pro-n:case-n:poss</ta>
            <ta e="T211" id="Seg_3882" s="T210">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T212" id="Seg_3883" s="T211">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T351" id="Seg_3884" s="T212">n-n:case</ta>
            <ta e="T213" id="Seg_3885" s="T351">adv&gt;adv</ta>
            <ta e="T214" id="Seg_3886" s="T213">adv</ta>
            <ta e="T215" id="Seg_3887" s="T214">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T216" id="Seg_3888" s="T215">v-v&gt;v-v&gt;adv</ta>
            <ta e="T217" id="Seg_3889" s="T216">n-n:case</ta>
            <ta e="T218" id="Seg_3890" s="T217">v-v&gt;v-v:inf-v-v:tense-v:pn</ta>
            <ta e="T219" id="Seg_3891" s="T218">adv-adv&gt;adv</ta>
            <ta e="T220" id="Seg_3892" s="T219">conj</ta>
            <ta e="T221" id="Seg_3893" s="T220">preverb</ta>
            <ta e="T222" id="Seg_3894" s="T221">v-v&gt;v-v:pn</ta>
            <ta e="T223" id="Seg_3895" s="T222">v-v&gt;v-v&gt;adv</ta>
            <ta e="T224" id="Seg_3896" s="T223">num</ta>
            <ta e="T225" id="Seg_3897" s="T224">n-n:case</ta>
            <ta e="T226" id="Seg_3898" s="T225">v-v:ins-v:pn</ta>
            <ta e="T227" id="Seg_3899" s="T226">n-n:case</ta>
            <ta e="T228" id="Seg_3900" s="T227">pp-n:case</ta>
            <ta e="T229" id="Seg_3901" s="T228">n-n:case</ta>
            <ta e="T230" id="Seg_3902" s="T229">n-n:case</ta>
            <ta e="T231" id="Seg_3903" s="T230">v-v:tense.mood-v:pn</ta>
            <ta e="T232" id="Seg_3904" s="T231">adv-adv&gt;adv</ta>
            <ta e="T233" id="Seg_3905" s="T232">v-v:tense-v:pn</ta>
            <ta e="T234" id="Seg_3906" s="T233">v-v:ins-v:pn</ta>
            <ta e="T235" id="Seg_3907" s="T234">n-n:case</ta>
            <ta e="T236" id="Seg_3908" s="T235">num</ta>
            <ta e="T237" id="Seg_3909" s="T236">n-n:num-n:obl.poss-n:case</ta>
            <ta e="T238" id="Seg_3910" s="T237">v-v:tense.mood-v:pn</ta>
            <ta e="T239" id="Seg_3911" s="T238">n-n:case</ta>
            <ta e="T240" id="Seg_3912" s="T239">v-v:ins-v:pn</ta>
            <ta e="T241" id="Seg_3913" s="T240">n-n:case</ta>
            <ta e="T242" id="Seg_3914" s="T241">v-v&gt;v-v:pn</ta>
            <ta e="T243" id="Seg_3915" s="T242">n-n:case-n:obl.poss</ta>
            <ta e="T244" id="Seg_3916" s="T243">v-v&gt;v-v&gt;adv</ta>
            <ta e="T245" id="Seg_3917" s="T244">v-v&gt;v-v:pn</ta>
            <ta e="T246" id="Seg_3918" s="T245">interj</ta>
            <ta e="T247" id="Seg_3919" s="T246">n-n:case</ta>
            <ta e="T248" id="Seg_3920" s="T247">v-v&gt;v-v:mood.pn</ta>
            <ta e="T249" id="Seg_3921" s="T248">conj</ta>
            <ta e="T250" id="Seg_3922" s="T249">n-n:ins-n&gt;adj</ta>
            <ta e="T251" id="Seg_3923" s="T250">n-n:case</ta>
            <ta e="T252" id="Seg_3924" s="T251">n-n:case</ta>
            <ta e="T253" id="Seg_3925" s="T252">n-n:case</ta>
            <ta e="T254" id="Seg_3926" s="T253">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T255" id="Seg_3927" s="T254">pers</ta>
            <ta e="T256" id="Seg_3928" s="T255">n-n:case-n:obl.poss</ta>
            <ta e="T257" id="Seg_3929" s="T256">n-n:case</ta>
            <ta e="T258" id="Seg_3930" s="T257">v-v:pn</ta>
            <ta e="T259" id="Seg_3931" s="T258">n-n:num-n:case-n:obl.poss</ta>
            <ta e="T260" id="Seg_3932" s="T259">n-n:case</ta>
            <ta e="T261" id="Seg_3933" s="T260">n-n:case</ta>
            <ta e="T262" id="Seg_3934" s="T261">v-v&gt;v-v:mood.pn</ta>
            <ta e="T263" id="Seg_3935" s="T262">interrog-n:case</ta>
            <ta e="T264" id="Seg_3936" s="T263">pers</ta>
            <ta e="T265" id="Seg_3937" s="T264">n-n:case-n:obl.poss</ta>
            <ta e="T266" id="Seg_3938" s="T265">n-n:case-n:poss</ta>
            <ta e="T267" id="Seg_3939" s="T266">conj</ta>
            <ta e="T268" id="Seg_3940" s="T267">n-n:case</ta>
            <ta e="T269" id="Seg_3941" s="T268">v-v:tense.mood-v:pn</ta>
            <ta e="T270" id="Seg_3942" s="T269">n-n:case</ta>
            <ta e="T271" id="Seg_3943" s="T270">v-v&gt;v-v&gt;adv</ta>
            <ta e="T272" id="Seg_3944" s="T271">v-v:tense-v:pn</ta>
            <ta e="T273" id="Seg_3945" s="T272">pers-n:ins-n:case</ta>
            <ta e="T274" id="Seg_3946" s="T273">adv-adv:case</ta>
            <ta e="T275" id="Seg_3947" s="T274">n-n:case</ta>
            <ta e="T276" id="Seg_3948" s="T275">adj</ta>
            <ta e="T277" id="Seg_3949" s="T276">n-n:case</ta>
            <ta e="T278" id="Seg_3950" s="T277">n-n:case</ta>
            <ta e="T279" id="Seg_3951" s="T278">n-n:case</ta>
            <ta e="T280" id="Seg_3952" s="T279">v-v:pn</ta>
            <ta e="T281" id="Seg_3953" s="T280">adv</ta>
            <ta e="T282" id="Seg_3954" s="T281">num</ta>
            <ta e="T283" id="Seg_3955" s="T282">n-n:case</ta>
            <ta e="T284" id="Seg_3956" s="T283">n-n:case-n:poss</ta>
            <ta e="T285" id="Seg_3957" s="T284">adv</ta>
            <ta e="T286" id="Seg_3958" s="T285">v-v&gt;v-v&gt;adv</ta>
            <ta e="T287" id="Seg_3959" s="T286">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T288" id="Seg_3960" s="T287">v-v:tense-v:pn</ta>
            <ta e="T289" id="Seg_3961" s="T288">n-n:case</ta>
            <ta e="T290" id="Seg_3962" s="T289">n-n:case</ta>
            <ta e="T291" id="Seg_3963" s="T290">pp-n:case</ta>
            <ta e="T292" id="Seg_3964" s="T291">n-n:case</ta>
            <ta e="T293" id="Seg_3965" s="T292">n-n:case</ta>
            <ta e="T294" id="Seg_3966" s="T293">v-v&gt;v-v:pn</ta>
            <ta e="T295" id="Seg_3967" s="T294">v-v&gt;adv</ta>
            <ta e="T296" id="Seg_3968" s="T295">num</ta>
            <ta e="T297" id="Seg_3969" s="T296">n-n:num-n:case</ta>
            <ta e="T298" id="Seg_3970" s="T297">v-v:tense-v:pn</ta>
            <ta e="T299" id="Seg_3971" s="T298">n-n:case</ta>
            <ta e="T300" id="Seg_3972" s="T299">pp-n:case</ta>
            <ta e="T301" id="Seg_3973" s="T300">n-n:case</ta>
            <ta e="T302" id="Seg_3974" s="T301">v-v:tense-v:pn</ta>
            <ta e="T303" id="Seg_3975" s="T302">n-n&gt;adv-adv:case</ta>
            <ta e="T304" id="Seg_3976" s="T303">n-n:case</ta>
            <ta e="T305" id="Seg_3977" s="T304">v-v:tense-v:pn</ta>
            <ta e="T306" id="Seg_3978" s="T305">n-n:case</ta>
            <ta e="T307" id="Seg_3979" s="T306">n-n:case</ta>
            <ta e="T308" id="Seg_3980" s="T307">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T309" id="Seg_3981" s="T308">ptcl</ta>
            <ta e="T310" id="Seg_3982" s="T309">n-n:ins-n:case</ta>
            <ta e="T311" id="Seg_3983" s="T310">n-n:case</ta>
            <ta e="T312" id="Seg_3984" s="T311">n-n:case-n:obl.poss</ta>
            <ta e="T313" id="Seg_3985" s="T312">v-v:tense-v:pn</ta>
            <ta e="T314" id="Seg_3986" s="T313">num</ta>
            <ta e="T315" id="Seg_3987" s="T314">n-n:case</ta>
            <ta e="T316" id="Seg_3988" s="T315">n-n:case</ta>
            <ta e="T317" id="Seg_3989" s="T316">v-v&gt;n-n:case-n:poss</ta>
            <ta e="T318" id="Seg_3990" s="T317">ptcl</ta>
            <ta e="T319" id="Seg_3991" s="T318">v-v:pn</ta>
            <ta e="T320" id="Seg_3992" s="T319">v-v:tense-v:pn</ta>
            <ta e="T321" id="Seg_3993" s="T320">pers-n:case</ta>
            <ta e="T322" id="Seg_3994" s="T321">n-n:case</ta>
            <ta e="T323" id="Seg_3995" s="T322">v-v:tense-v:pn</ta>
            <ta e="T324" id="Seg_3996" s="T323">n-n:case</ta>
            <ta e="T325" id="Seg_3997" s="T324">n-n:num-n:case-n:poss</ta>
            <ta e="T326" id="Seg_3998" s="T325">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T327" id="Seg_3999" s="T326">n-n:case</ta>
            <ta e="T328" id="Seg_4000" s="T327">n-n:case</ta>
            <ta e="T329" id="Seg_4001" s="T328">conj</ta>
            <ta e="T330" id="Seg_4002" s="T329">n-n:case</ta>
            <ta e="T331" id="Seg_4003" s="T330">n-n:case</ta>
            <ta e="T332" id="Seg_4004" s="T331">num</ta>
            <ta e="T333" id="Seg_4005" s="T332">n-n:case-n:poss</ta>
            <ta e="T334" id="Seg_4006" s="T333">v-v&gt;v-v&gt;adv</ta>
            <ta e="T335" id="Seg_4007" s="T334">v-v:tense-v:pn</ta>
            <ta e="T336" id="Seg_4008" s="T335">n-n:case</ta>
            <ta e="T337" id="Seg_4009" s="T336">num-num&gt;adv-n:ins-adv&gt;adj</ta>
            <ta e="T338" id="Seg_4010" s="T337">n-n:case-n:poss</ta>
            <ta e="T339" id="Seg_4011" s="T338">ptcl</ta>
            <ta e="T340" id="Seg_4012" s="T339">dem-adj&gt;adv</ta>
            <ta e="T341" id="Seg_4013" s="T340">v-v:tense-v:pn</ta>
            <ta e="T342" id="Seg_4014" s="T341">conj</ta>
            <ta e="T343" id="Seg_4015" s="T342">v-v:tense-v:pn</ta>
            <ta e="T344" id="Seg_4016" s="T343">pers</ta>
            <ta e="T345" id="Seg_4017" s="T344">n-n:case-n:poss</ta>
            <ta e="T346" id="Seg_4018" s="T345">interrog-n:case</ta>
            <ta e="T347" id="Seg_4019" s="T346">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T348" id="Seg_4020" s="T347">adv-n:case</ta>
            <ta e="T349" id="Seg_4021" s="T348">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_4022" s="T0">nprop</ta>
            <ta e="T2" id="Seg_4023" s="T1">v</ta>
            <ta e="T3" id="Seg_4024" s="T2">nprop</ta>
            <ta e="T4" id="Seg_4025" s="T3">n</ta>
            <ta e="T5" id="Seg_4026" s="T4">nprop</ta>
            <ta e="T6" id="Seg_4027" s="T5">v</ta>
            <ta e="T7" id="Seg_4028" s="T6">n</ta>
            <ta e="T8" id="Seg_4029" s="T7">v</ta>
            <ta e="T9" id="Seg_4030" s="T8">n</ta>
            <ta e="T10" id="Seg_4031" s="T9">v</ta>
            <ta e="T11" id="Seg_4032" s="T10">v</ta>
            <ta e="T12" id="Seg_4033" s="T11">adv</ta>
            <ta e="T13" id="Seg_4034" s="T12">v</ta>
            <ta e="T14" id="Seg_4035" s="T13">adj</ta>
            <ta e="T15" id="Seg_4036" s="T14">n</ta>
            <ta e="T16" id="Seg_4037" s="T15">adj</ta>
            <ta e="T17" id="Seg_4038" s="T16">n</ta>
            <ta e="T18" id="Seg_4039" s="T17">n</ta>
            <ta e="T19" id="Seg_4040" s="T18">interrog</ta>
            <ta e="T20" id="Seg_4041" s="T19">v</ta>
            <ta e="T21" id="Seg_4042" s="T20">pers</ta>
            <ta e="T22" id="Seg_4043" s="T21">n</ta>
            <ta e="T23" id="Seg_4044" s="T22">v</ta>
            <ta e="T24" id="Seg_4045" s="T23">n</ta>
            <ta e="T25" id="Seg_4046" s="T24">v</ta>
            <ta e="T26" id="Seg_4047" s="T25">v</ta>
            <ta e="T27" id="Seg_4048" s="T26">pers</ta>
            <ta e="T28" id="Seg_4049" s="T27">adv</ta>
            <ta e="T29" id="Seg_4050" s="T28">dem</ta>
            <ta e="T30" id="Seg_4051" s="T29">n</ta>
            <ta e="T31" id="Seg_4052" s="T30">n</ta>
            <ta e="T32" id="Seg_4053" s="T31">preverb</ta>
            <ta e="T33" id="Seg_4054" s="T32">v</ta>
            <ta e="T34" id="Seg_4055" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_4056" s="T34">n</ta>
            <ta e="T36" id="Seg_4057" s="T35">preverb</ta>
            <ta e="T37" id="Seg_4058" s="T36">v</ta>
            <ta e="T38" id="Seg_4059" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_4060" s="T38">v</ta>
            <ta e="T40" id="Seg_4061" s="T39">adj</ta>
            <ta e="T41" id="Seg_4062" s="T40">n</ta>
            <ta e="T42" id="Seg_4063" s="T41">n</ta>
            <ta e="T43" id="Seg_4064" s="T42">preverb</ta>
            <ta e="T44" id="Seg_4065" s="T43">v</ta>
            <ta e="T45" id="Seg_4066" s="T44">ptcl</ta>
            <ta e="T46" id="Seg_4067" s="T45">v</ta>
            <ta e="T47" id="Seg_4068" s="T46">v</ta>
            <ta e="T48" id="Seg_4069" s="T47">num</ta>
            <ta e="T49" id="Seg_4070" s="T48">v</ta>
            <ta e="T50" id="Seg_4071" s="T49">v</ta>
            <ta e="T51" id="Seg_4072" s="T50">v</ta>
            <ta e="T52" id="Seg_4073" s="T51">ptcl</ta>
            <ta e="T53" id="Seg_4074" s="T52">n</ta>
            <ta e="T54" id="Seg_4075" s="T53">v</ta>
            <ta e="T55" id="Seg_4076" s="T54">n</ta>
            <ta e="T56" id="Seg_4077" s="T55">num</ta>
            <ta e="T57" id="Seg_4078" s="T56">n</ta>
            <ta e="T58" id="Seg_4079" s="T57">pers</ta>
            <ta e="T59" id="Seg_4080" s="T58">v</ta>
            <ta e="T60" id="Seg_4081" s="T59">n</ta>
            <ta e="T61" id="Seg_4082" s="T60">v</ta>
            <ta e="T62" id="Seg_4083" s="T61">n</ta>
            <ta e="T63" id="Seg_4084" s="T62">v</ta>
            <ta e="T64" id="Seg_4085" s="T63">v</ta>
            <ta e="T65" id="Seg_4086" s="T64">conj</ta>
            <ta e="T66" id="Seg_4087" s="T65">v</ta>
            <ta e="T67" id="Seg_4088" s="T66">n</ta>
            <ta e="T68" id="Seg_4089" s="T67">v</ta>
            <ta e="T69" id="Seg_4090" s="T68">nprop</ta>
            <ta e="T70" id="Seg_4091" s="T69">v</ta>
            <ta e="T71" id="Seg_4092" s="T70">v</ta>
            <ta e="T72" id="Seg_4093" s="T71">adv</ta>
            <ta e="T73" id="Seg_4094" s="T72">ptcl</ta>
            <ta e="T74" id="Seg_4095" s="T73">adv</ta>
            <ta e="T75" id="Seg_4096" s="T74">interrog</ta>
            <ta e="T76" id="Seg_4097" s="T75">ptcl</ta>
            <ta e="T77" id="Seg_4098" s="T76">interrog</ta>
            <ta e="T78" id="Seg_4099" s="T77">v</ta>
            <ta e="T79" id="Seg_4100" s="T78">interrog</ta>
            <ta e="T80" id="Seg_4101" s="T79">v</ta>
            <ta e="T81" id="Seg_4102" s="T80">v</ta>
            <ta e="T82" id="Seg_4103" s="T81">conj</ta>
            <ta e="T83" id="Seg_4104" s="T82">v</ta>
            <ta e="T84" id="Seg_4105" s="T83">v</ta>
            <ta e="T85" id="Seg_4106" s="T84">v</ta>
            <ta e="T86" id="Seg_4107" s="T85">pers</ta>
            <ta e="T87" id="Seg_4108" s="T86">v</ta>
            <ta e="T88" id="Seg_4109" s="T87">n</ta>
            <ta e="T89" id="Seg_4110" s="T88">n</ta>
            <ta e="T90" id="Seg_4111" s="T89">n</ta>
            <ta e="T91" id="Seg_4112" s="T90">preverb</ta>
            <ta e="T92" id="Seg_4113" s="T91">v</ta>
            <ta e="T93" id="Seg_4114" s="T92">n</ta>
            <ta e="T94" id="Seg_4115" s="T93">preverb</ta>
            <ta e="T95" id="Seg_4116" s="T94">v</ta>
            <ta e="T96" id="Seg_4117" s="T95">n</ta>
            <ta e="T97" id="Seg_4118" s="T96">v</ta>
            <ta e="T98" id="Seg_4119" s="T97">conj</ta>
            <ta e="T99" id="Seg_4120" s="T98">n</ta>
            <ta e="T100" id="Seg_4121" s="T99">adj</ta>
            <ta e="T101" id="Seg_4122" s="T100">v</ta>
            <ta e="T102" id="Seg_4123" s="T101">v</ta>
            <ta e="T103" id="Seg_4124" s="T102">n</ta>
            <ta e="T104" id="Seg_4125" s="T103">pp</ta>
            <ta e="T105" id="Seg_4126" s="T104">v</ta>
            <ta e="T106" id="Seg_4127" s="T105">v</ta>
            <ta e="T107" id="Seg_4128" s="T106">v</ta>
            <ta e="T108" id="Seg_4129" s="T107">interrog</ta>
            <ta e="T109" id="Seg_4130" s="T108">v</ta>
            <ta e="T110" id="Seg_4131" s="T109">conj</ta>
            <ta e="T111" id="Seg_4132" s="T110">v</ta>
            <ta e="T112" id="Seg_4133" s="T111">n</ta>
            <ta e="T113" id="Seg_4134" s="T112">v</ta>
            <ta e="T114" id="Seg_4135" s="T113">v</ta>
            <ta e="T115" id="Seg_4136" s="T114">v</ta>
            <ta e="T116" id="Seg_4137" s="T115">conj</ta>
            <ta e="T117" id="Seg_4138" s="T116">n</ta>
            <ta e="T118" id="Seg_4139" s="T117">v</ta>
            <ta e="T119" id="Seg_4140" s="T118">v</ta>
            <ta e="T120" id="Seg_4141" s="T119">adv</ta>
            <ta e="T121" id="Seg_4142" s="T120">v</ta>
            <ta e="T122" id="Seg_4143" s="T121">v</ta>
            <ta e="T123" id="Seg_4144" s="T122">interrog</ta>
            <ta e="T124" id="Seg_4145" s="T123">qv</ta>
            <ta e="T125" id="Seg_4146" s="T124">n</ta>
            <ta e="T126" id="Seg_4147" s="T125">v</ta>
            <ta e="T127" id="Seg_4148" s="T126">n</ta>
            <ta e="T128" id="Seg_4149" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_4150" s="T128">n</ta>
            <ta e="T130" id="Seg_4151" s="T129">v</ta>
            <ta e="T131" id="Seg_4152" s="T130">v</ta>
            <ta e="T132" id="Seg_4153" s="T131">n</ta>
            <ta e="T133" id="Seg_4154" s="T132">v</ta>
            <ta e="T134" id="Seg_4155" s="T133">n</ta>
            <ta e="T135" id="Seg_4156" s="T134">n</ta>
            <ta e="T136" id="Seg_4157" s="T135">adv</ta>
            <ta e="T137" id="Seg_4158" s="T136">v</ta>
            <ta e="T138" id="Seg_4159" s="T137">n</ta>
            <ta e="T139" id="Seg_4160" s="T138">n</ta>
            <ta e="T140" id="Seg_4161" s="T139">v</ta>
            <ta e="T141" id="Seg_4162" s="T140">adj</ta>
            <ta e="T142" id="Seg_4163" s="T141">v</ta>
            <ta e="T143" id="Seg_4164" s="T142">v</ta>
            <ta e="T144" id="Seg_4165" s="T143">n</ta>
            <ta e="T145" id="Seg_4166" s="T144">adv</ta>
            <ta e="T146" id="Seg_4167" s="T145">adv</ta>
            <ta e="T147" id="Seg_4168" s="T146">preverb</ta>
            <ta e="T148" id="Seg_4169" s="T147">v</ta>
            <ta e="T149" id="Seg_4170" s="T148">adv</ta>
            <ta e="T150" id="Seg_4171" s="T149">v</ta>
            <ta e="T151" id="Seg_4172" s="T150">conj</ta>
            <ta e="T152" id="Seg_4173" s="T151">adv</ta>
            <ta e="T153" id="Seg_4174" s="T152">v</ta>
            <ta e="T154" id="Seg_4175" s="T153">n</ta>
            <ta e="T155" id="Seg_4176" s="T154">n</ta>
            <ta e="T156" id="Seg_4177" s="T155">adv</ta>
            <ta e="T157" id="Seg_4178" s="T156">adv</ta>
            <ta e="T158" id="Seg_4179" s="T157">preverb</ta>
            <ta e="T159" id="Seg_4180" s="T158">v</ta>
            <ta e="T160" id="Seg_4181" s="T159">n</ta>
            <ta e="T161" id="Seg_4182" s="T160">pp</ta>
            <ta e="T162" id="Seg_4183" s="T161">conj</ta>
            <ta e="T163" id="Seg_4184" s="T162">n</ta>
            <ta e="T164" id="Seg_4185" s="T163">pp</ta>
            <ta e="T165" id="Seg_4186" s="T164">adv</ta>
            <ta e="T166" id="Seg_4187" s="T165">v</ta>
            <ta e="T167" id="Seg_4188" s="T166">v</ta>
            <ta e="T168" id="Seg_4189" s="T167">v</ta>
            <ta e="T169" id="Seg_4190" s="T168">adv</ta>
            <ta e="T170" id="Seg_4191" s="T169">v</ta>
            <ta e="T171" id="Seg_4192" s="T170">adv</ta>
            <ta e="T172" id="Seg_4193" s="T171">v</ta>
            <ta e="T173" id="Seg_4194" s="T172">ptcl</ta>
            <ta e="T174" id="Seg_4195" s="T173">v</ta>
            <ta e="T175" id="Seg_4196" s="T174">v</ta>
            <ta e="T176" id="Seg_4197" s="T175">v</ta>
            <ta e="T177" id="Seg_4198" s="T176">n</ta>
            <ta e="T178" id="Seg_4199" s="T177">n</ta>
            <ta e="T179" id="Seg_4200" s="T178">adj</ta>
            <ta e="T180" id="Seg_4201" s="T179">v</ta>
            <ta e="T181" id="Seg_4202" s="T180">adv</ta>
            <ta e="T182" id="Seg_4203" s="T181">v</ta>
            <ta e="T183" id="Seg_4204" s="T182">n</ta>
            <ta e="T184" id="Seg_4205" s="T183">n</ta>
            <ta e="T185" id="Seg_4206" s="T184">n</ta>
            <ta e="T186" id="Seg_4207" s="T185">conj</ta>
            <ta e="T187" id="Seg_4208" s="T186">n</ta>
            <ta e="T188" id="Seg_4209" s="T187">n</ta>
            <ta e="T189" id="Seg_4210" s="T188">n</ta>
            <ta e="T190" id="Seg_4211" s="T189">v</ta>
            <ta e="T191" id="Seg_4212" s="T190">n</ta>
            <ta e="T192" id="Seg_4213" s="T191">adv</ta>
            <ta e="T193" id="Seg_4214" s="T192">pers</ta>
            <ta e="T194" id="Seg_4215" s="T193">v</ta>
            <ta e="T195" id="Seg_4216" s="T194">n</ta>
            <ta e="T196" id="Seg_4217" s="T195">adv</ta>
            <ta e="T197" id="Seg_4218" s="T196">v</ta>
            <ta e="T198" id="Seg_4219" s="T197">v</ta>
            <ta e="T199" id="Seg_4220" s="T198">adv</ta>
            <ta e="T200" id="Seg_4221" s="T199">adv</ta>
            <ta e="T201" id="Seg_4222" s="T200">adv</ta>
            <ta e="T202" id="Seg_4223" s="T201">v</ta>
            <ta e="T203" id="Seg_4224" s="T202">n</ta>
            <ta e="T204" id="Seg_4225" s="T203">v</ta>
            <ta e="T205" id="Seg_4226" s="T204">n</ta>
            <ta e="T206" id="Seg_4227" s="T205">conj</ta>
            <ta e="T207" id="Seg_4228" s="T206">v</ta>
            <ta e="T350" id="Seg_4229" s="T207">n</ta>
            <ta e="T208" id="Seg_4230" s="T350">adv</ta>
            <ta e="T209" id="Seg_4231" s="T208">conj</ta>
            <ta e="T210" id="Seg_4232" s="T209">pro</ta>
            <ta e="T211" id="Seg_4233" s="T210">v</ta>
            <ta e="T212" id="Seg_4234" s="T211">v</ta>
            <ta e="T351" id="Seg_4235" s="T212">n</ta>
            <ta e="T213" id="Seg_4236" s="T351">adv</ta>
            <ta e="T214" id="Seg_4237" s="T213">adv</ta>
            <ta e="T215" id="Seg_4238" s="T214">v</ta>
            <ta e="T216" id="Seg_4239" s="T215">adv</ta>
            <ta e="T217" id="Seg_4240" s="T216">n</ta>
            <ta e="T218" id="Seg_4241" s="T217">v</ta>
            <ta e="T219" id="Seg_4242" s="T218">adv</ta>
            <ta e="T220" id="Seg_4243" s="T219">conj</ta>
            <ta e="T221" id="Seg_4244" s="T220">preverb</ta>
            <ta e="T222" id="Seg_4245" s="T221">v</ta>
            <ta e="T223" id="Seg_4246" s="T222">adv</ta>
            <ta e="T224" id="Seg_4247" s="T223">num</ta>
            <ta e="T225" id="Seg_4248" s="T224">n</ta>
            <ta e="T226" id="Seg_4249" s="T225">v</ta>
            <ta e="T227" id="Seg_4250" s="T226">n</ta>
            <ta e="T228" id="Seg_4251" s="T227">n</ta>
            <ta e="T229" id="Seg_4252" s="T228">n</ta>
            <ta e="T230" id="Seg_4253" s="T229">n</ta>
            <ta e="T231" id="Seg_4254" s="T230">v</ta>
            <ta e="T232" id="Seg_4255" s="T231">adv</ta>
            <ta e="T233" id="Seg_4256" s="T232">v</ta>
            <ta e="T234" id="Seg_4257" s="T233">v</ta>
            <ta e="T235" id="Seg_4258" s="T234">n</ta>
            <ta e="T236" id="Seg_4259" s="T235">num</ta>
            <ta e="T237" id="Seg_4260" s="T236">n</ta>
            <ta e="T238" id="Seg_4261" s="T237">v</ta>
            <ta e="T239" id="Seg_4262" s="T238">n</ta>
            <ta e="T240" id="Seg_4263" s="T239">v</ta>
            <ta e="T241" id="Seg_4264" s="T240">n</ta>
            <ta e="T242" id="Seg_4265" s="T241">v</ta>
            <ta e="T243" id="Seg_4266" s="T242">n</ta>
            <ta e="T244" id="Seg_4267" s="T243">adv</ta>
            <ta e="T245" id="Seg_4268" s="T244">v</ta>
            <ta e="T246" id="Seg_4269" s="T245">interj</ta>
            <ta e="T247" id="Seg_4270" s="T246">n</ta>
            <ta e="T248" id="Seg_4271" s="T247">v</ta>
            <ta e="T249" id="Seg_4272" s="T248">conj</ta>
            <ta e="T250" id="Seg_4273" s="T249">adj</ta>
            <ta e="T251" id="Seg_4274" s="T250">n</ta>
            <ta e="T252" id="Seg_4275" s="T251">v</ta>
            <ta e="T253" id="Seg_4276" s="T252">n</ta>
            <ta e="T254" id="Seg_4277" s="T253">v</ta>
            <ta e="T255" id="Seg_4278" s="T254">pers</ta>
            <ta e="T256" id="Seg_4279" s="T255">n</ta>
            <ta e="T257" id="Seg_4280" s="T256">n</ta>
            <ta e="T258" id="Seg_4281" s="T257">v</ta>
            <ta e="T259" id="Seg_4282" s="T258">n</ta>
            <ta e="T260" id="Seg_4283" s="T259">n</ta>
            <ta e="T261" id="Seg_4284" s="T260">n</ta>
            <ta e="T262" id="Seg_4285" s="T261">v</ta>
            <ta e="T263" id="Seg_4286" s="T262">interrog</ta>
            <ta e="T264" id="Seg_4287" s="T263">pers</ta>
            <ta e="T265" id="Seg_4288" s="T264">n</ta>
            <ta e="T266" id="Seg_4289" s="T265">n</ta>
            <ta e="T267" id="Seg_4290" s="T266">conj</ta>
            <ta e="T268" id="Seg_4291" s="T267">n</ta>
            <ta e="T269" id="Seg_4292" s="T268">v</ta>
            <ta e="T270" id="Seg_4293" s="T269">n</ta>
            <ta e="T271" id="Seg_4294" s="T270">adv</ta>
            <ta e="T272" id="Seg_4295" s="T271">v</ta>
            <ta e="T273" id="Seg_4296" s="T272">pers</ta>
            <ta e="T274" id="Seg_4297" s="T273">adv</ta>
            <ta e="T275" id="Seg_4298" s="T274">n</ta>
            <ta e="T276" id="Seg_4299" s="T275">adj</ta>
            <ta e="T277" id="Seg_4300" s="T276">n</ta>
            <ta e="T278" id="Seg_4301" s="T277">n</ta>
            <ta e="T279" id="Seg_4302" s="T278">n</ta>
            <ta e="T280" id="Seg_4303" s="T279">v</ta>
            <ta e="T281" id="Seg_4304" s="T280">adv</ta>
            <ta e="T282" id="Seg_4305" s="T281">num</ta>
            <ta e="T283" id="Seg_4306" s="T282">n</ta>
            <ta e="T284" id="Seg_4307" s="T283">n</ta>
            <ta e="T285" id="Seg_4308" s="T284">adv</ta>
            <ta e="T286" id="Seg_4309" s="T285">adv</ta>
            <ta e="T287" id="Seg_4310" s="T286">v</ta>
            <ta e="T288" id="Seg_4311" s="T287">v</ta>
            <ta e="T289" id="Seg_4312" s="T288">n</ta>
            <ta e="T290" id="Seg_4313" s="T289">n</ta>
            <ta e="T291" id="Seg_4314" s="T290">pp</ta>
            <ta e="T292" id="Seg_4315" s="T291">n</ta>
            <ta e="T293" id="Seg_4316" s="T292">n</ta>
            <ta e="T294" id="Seg_4317" s="T293">v</ta>
            <ta e="T295" id="Seg_4318" s="T294">adv</ta>
            <ta e="T296" id="Seg_4319" s="T295">num</ta>
            <ta e="T297" id="Seg_4320" s="T296">n</ta>
            <ta e="T298" id="Seg_4321" s="T297">v</ta>
            <ta e="T299" id="Seg_4322" s="T298">n</ta>
            <ta e="T300" id="Seg_4323" s="T299">pp</ta>
            <ta e="T301" id="Seg_4324" s="T300">n</ta>
            <ta e="T302" id="Seg_4325" s="T301">v</ta>
            <ta e="T303" id="Seg_4326" s="T302">adv</ta>
            <ta e="T304" id="Seg_4327" s="T303">n</ta>
            <ta e="T305" id="Seg_4328" s="T304">v</ta>
            <ta e="T306" id="Seg_4329" s="T305">n</ta>
            <ta e="T307" id="Seg_4330" s="T306">n</ta>
            <ta e="T308" id="Seg_4331" s="T307">v</ta>
            <ta e="T309" id="Seg_4332" s="T308">ptcl</ta>
            <ta e="T310" id="Seg_4333" s="T309">n</ta>
            <ta e="T311" id="Seg_4334" s="T310">n</ta>
            <ta e="T312" id="Seg_4335" s="T311">n</ta>
            <ta e="T313" id="Seg_4336" s="T312">v</ta>
            <ta e="T314" id="Seg_4337" s="T313">num</ta>
            <ta e="T315" id="Seg_4338" s="T314">n</ta>
            <ta e="T316" id="Seg_4339" s="T315">n</ta>
            <ta e="T317" id="Seg_4340" s="T316">n</ta>
            <ta e="T318" id="Seg_4341" s="T317">ptcl</ta>
            <ta e="T319" id="Seg_4342" s="T318">v</ta>
            <ta e="T320" id="Seg_4343" s="T319">v</ta>
            <ta e="T321" id="Seg_4344" s="T320">pers</ta>
            <ta e="T322" id="Seg_4345" s="T321">n</ta>
            <ta e="T323" id="Seg_4346" s="T322">v</ta>
            <ta e="T324" id="Seg_4347" s="T323">n</ta>
            <ta e="T325" id="Seg_4348" s="T324">n</ta>
            <ta e="T326" id="Seg_4349" s="T325">v</ta>
            <ta e="T327" id="Seg_4350" s="T326">n</ta>
            <ta e="T328" id="Seg_4351" s="T327">n</ta>
            <ta e="T329" id="Seg_4352" s="T328">conj</ta>
            <ta e="T330" id="Seg_4353" s="T329">n</ta>
            <ta e="T331" id="Seg_4354" s="T330">n</ta>
            <ta e="T332" id="Seg_4355" s="T331">num</ta>
            <ta e="T333" id="Seg_4356" s="T332">n</ta>
            <ta e="T334" id="Seg_4357" s="T333">adv</ta>
            <ta e="T335" id="Seg_4358" s="T334">v</ta>
            <ta e="T336" id="Seg_4359" s="T335">n</ta>
            <ta e="T337" id="Seg_4360" s="T336">adj</ta>
            <ta e="T338" id="Seg_4361" s="T337">n</ta>
            <ta e="T339" id="Seg_4362" s="T338">ptcl</ta>
            <ta e="T340" id="Seg_4363" s="T339">adv</ta>
            <ta e="T341" id="Seg_4364" s="T340">v</ta>
            <ta e="T342" id="Seg_4365" s="T341">conj</ta>
            <ta e="T343" id="Seg_4366" s="T342">v</ta>
            <ta e="T344" id="Seg_4367" s="T343">pers</ta>
            <ta e="T345" id="Seg_4368" s="T344">n</ta>
            <ta e="T346" id="Seg_4369" s="T345">interrog</ta>
            <ta e="T347" id="Seg_4370" s="T346">v</ta>
            <ta e="T348" id="Seg_4371" s="T347">adv</ta>
            <ta e="T349" id="Seg_4372" s="T348">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_4373" s="T2">np.h:Th</ta>
            <ta e="T4" id="Seg_4374" s="T3">np:Com 0.3.h:Poss</ta>
            <ta e="T5" id="Seg_4375" s="T4">np.h:A</ta>
            <ta e="T7" id="Seg_4376" s="T6">np:G</ta>
            <ta e="T8" id="Seg_4377" s="T7">0.3.h:E</ta>
            <ta e="T9" id="Seg_4378" s="T8">np.h:A</ta>
            <ta e="T11" id="Seg_4379" s="T10">0.3.h:A</ta>
            <ta e="T12" id="Seg_4380" s="T11">adv:G</ta>
            <ta e="T13" id="Seg_4381" s="T12">0.3.h:E</ta>
            <ta e="T15" id="Seg_4382" s="T14">np.h:Th</ta>
            <ta e="T19" id="Seg_4383" s="T18">pro:P</ta>
            <ta e="T20" id="Seg_4384" s="T19">0.2.h:A</ta>
            <ta e="T21" id="Seg_4385" s="T20">pro.h:A</ta>
            <ta e="T22" id="Seg_4386" s="T21">np:P</ta>
            <ta e="T24" id="Seg_4387" s="T23">np.h:A</ta>
            <ta e="T27" id="Seg_4388" s="T26">pro.h:A</ta>
            <ta e="T28" id="Seg_4389" s="T27">adv:Time</ta>
            <ta e="T30" id="Seg_4390" s="T29">np:P</ta>
            <ta e="T31" id="Seg_4391" s="T30">np:Ins</ta>
            <ta e="T37" id="Seg_4392" s="T36">0.2.h:A 0.3:P</ta>
            <ta e="T39" id="Seg_4393" s="T38">0.2.h:A</ta>
            <ta e="T41" id="Seg_4394" s="T40">np.h:A</ta>
            <ta e="T42" id="Seg_4395" s="T41">np:P</ta>
            <ta e="T46" id="Seg_4396" s="T45">np:Th 0.2.h:Poss</ta>
            <ta e="T47" id="Seg_4397" s="T46">0.2.h:A</ta>
            <ta e="T49" id="Seg_4398" s="T48">np:Th 0.2.h:Poss</ta>
            <ta e="T50" id="Seg_4399" s="T49">0.2.h:A</ta>
            <ta e="T51" id="Seg_4400" s="T50">0.2.h:A 0.3:Th</ta>
            <ta e="T53" id="Seg_4401" s="T52">np:A</ta>
            <ta e="T55" id="Seg_4402" s="T54">np:P 0.3.h:Poss</ta>
            <ta e="T57" id="Seg_4403" s="T56">np:P 0.3.h:Poss</ta>
            <ta e="T58" id="Seg_4404" s="T57">pro.h:Th</ta>
            <ta e="T60" id="Seg_4405" s="T59">np:Th 0.3.h:Poss</ta>
            <ta e="T62" id="Seg_4406" s="T61">np.h:A</ta>
            <ta e="T64" id="Seg_4407" s="T63">0.2.h:Th</ta>
            <ta e="T66" id="Seg_4408" s="T65">0.2.h:P</ta>
            <ta e="T67" id="Seg_4409" s="T66">np.h:A</ta>
            <ta e="T69" id="Seg_4410" s="T68">np.h:Th</ta>
            <ta e="T71" id="Seg_4411" s="T70">0.3.h:Th</ta>
            <ta e="T78" id="Seg_4412" s="T77">0.3.h:E</ta>
            <ta e="T80" id="Seg_4413" s="T79">0.3.h:A</ta>
            <ta e="T81" id="Seg_4414" s="T80">0.3.h:A</ta>
            <ta e="T83" id="Seg_4415" s="T82">0.3.h:A</ta>
            <ta e="T84" id="Seg_4416" s="T83">0.3.h:Th</ta>
            <ta e="T86" id="Seg_4417" s="T85">pro.h:G</ta>
            <ta e="T88" id="Seg_4418" s="T87">np:A</ta>
            <ta e="T90" id="Seg_4419" s="T89">np:P</ta>
            <ta e="T92" id="Seg_4420" s="T91">0.2.h:A</ta>
            <ta e="T93" id="Seg_4421" s="T92">np:A</ta>
            <ta e="T95" id="Seg_4422" s="T94">0.3:P</ta>
            <ta e="T96" id="Seg_4423" s="T95">np:Th 0.3.h:Poss</ta>
            <ta e="T97" id="Seg_4424" s="T96">0.3:A</ta>
            <ta e="T99" id="Seg_4425" s="T98">np:Th 0.3.h:Poss</ta>
            <ta e="T102" id="Seg_4426" s="T101">0.3.h:A</ta>
            <ta e="T103" id="Seg_4427" s="T102">pp:So</ta>
            <ta e="T105" id="Seg_4428" s="T104">0.3.h:A</ta>
            <ta e="T106" id="Seg_4429" s="T105">0.3.h:A</ta>
            <ta e="T107" id="Seg_4430" s="T106">0.3.h:E</ta>
            <ta e="T109" id="Seg_4431" s="T108">0.3.h:A</ta>
            <ta e="T111" id="Seg_4432" s="T110">0.3.h:E</ta>
            <ta e="T112" id="Seg_4433" s="T111">np.h:Th</ta>
            <ta e="T113" id="Seg_4434" s="T112">0.3.h:A</ta>
            <ta e="T114" id="Seg_4435" s="T113">0.3.h:A</ta>
            <ta e="T115" id="Seg_4436" s="T114">0.3.h:E</ta>
            <ta e="T117" id="Seg_4437" s="T116">np.h:P</ta>
            <ta e="T118" id="Seg_4438" s="T117">0.3.h:A</ta>
            <ta e="T119" id="Seg_4439" s="T118">0.3.h:A</ta>
            <ta e="T120" id="Seg_4440" s="T119">0.3.h:A</ta>
            <ta e="T121" id="Seg_4441" s="T120">0.3.h:A</ta>
            <ta e="T122" id="Seg_4442" s="T121">0.3.h:E</ta>
            <ta e="T123" id="Seg_4443" s="T122">pro:G</ta>
            <ta e="T124" id="Seg_4444" s="T123">0.3.h:A</ta>
            <ta e="T125" id="Seg_4445" s="T124">np:G 0.3.h:Poss</ta>
            <ta e="T127" id="Seg_4446" s="T126">np:Th</ta>
            <ta e="T129" id="Seg_4447" s="T128">np:Th 0.1.h:Poss</ta>
            <ta e="T130" id="Seg_4448" s="T129">v:Th</ta>
            <ta e="T131" id="Seg_4449" s="T130">v:Th</ta>
            <ta e="T132" id="Seg_4450" s="T131">np:L 0.3.h:Poss</ta>
            <ta e="T133" id="Seg_4451" s="T132">0.3.h:B</ta>
            <ta e="T134" id="Seg_4452" s="T133">np:Poss</ta>
            <ta e="T135" id="Seg_4453" s="T134">np:Th</ta>
            <ta e="T137" id="Seg_4454" s="T136">0.3.h:A</ta>
            <ta e="T138" id="Seg_4455" s="T137">np:Poss</ta>
            <ta e="T139" id="Seg_4456" s="T138">np:P</ta>
            <ta e="T140" id="Seg_4457" s="T139">0.3.h:A 0.3:P</ta>
            <ta e="T142" id="Seg_4458" s="T141">0.3:Th</ta>
            <ta e="T143" id="Seg_4459" s="T142">0.3.h:A 0.3:Th</ta>
            <ta e="T144" id="Seg_4460" s="T143">np:G 0.3.h:Poss</ta>
            <ta e="T145" id="Seg_4461" s="T144">adv:Time</ta>
            <ta e="T146" id="Seg_4462" s="T145">adv:G</ta>
            <ta e="T148" id="Seg_4463" s="T147">0.3.h:A 0.3:Th</ta>
            <ta e="T150" id="Seg_4464" s="T149">0.3.h:A 0.3:P</ta>
            <ta e="T153" id="Seg_4465" s="T152">0.3.h:A 0.3:Th</ta>
            <ta e="T154" id="Seg_4466" s="T153">np:G 0.3.h:Poss</ta>
            <ta e="T155" id="Seg_4467" s="T154">np:G 0.3.h:Poss</ta>
            <ta e="T156" id="Seg_4468" s="T155">adv:Time</ta>
            <ta e="T159" id="Seg_4469" s="T158">0.3.h:A 0.3:Th</ta>
            <ta e="T160" id="Seg_4470" s="T159">pp:So 0.3.h:Poss</ta>
            <ta e="T163" id="Seg_4471" s="T162">pp:So 0.3.h:Poss</ta>
            <ta e="T166" id="Seg_4472" s="T165">0.3.h:A 0.3:P</ta>
            <ta e="T167" id="Seg_4473" s="T166">0.3.h:A 0.3:P</ta>
            <ta e="T168" id="Seg_4474" s="T167">0.3.h:A 0.3:P</ta>
            <ta e="T170" id="Seg_4475" s="T169">0.3.h:E</ta>
            <ta e="T172" id="Seg_4476" s="T171">v:Th</ta>
            <ta e="T174" id="Seg_4477" s="T173">0.3.h:A 0.3:P</ta>
            <ta e="T175" id="Seg_4478" s="T174">0.3.h:A 0.3:P</ta>
            <ta e="T176" id="Seg_4479" s="T175">0.3.h:A</ta>
            <ta e="T177" id="Seg_4480" s="T176">np:Poss</ta>
            <ta e="T178" id="Seg_4481" s="T177">np:P</ta>
            <ta e="T180" id="Seg_4482" s="T179">0.3:Th</ta>
            <ta e="T181" id="Seg_4483" s="T180">adv:Time</ta>
            <ta e="T182" id="Seg_4484" s="T181">0.3.h:A</ta>
            <ta e="T183" id="Seg_4485" s="T182">np:Poss</ta>
            <ta e="T184" id="Seg_4486" s="T183">np:Th</ta>
            <ta e="T185" id="Seg_4487" s="T184">np:G 0.3.h:Poss</ta>
            <ta e="T187" id="Seg_4488" s="T186">np:G 0.3.h:Poss</ta>
            <ta e="T188" id="Seg_4489" s="T187">np:Poss</ta>
            <ta e="T189" id="Seg_4490" s="T188">np:Th</ta>
            <ta e="T191" id="Seg_4491" s="T190">np:G 0.3.h:Poss</ta>
            <ta e="T193" id="Seg_4492" s="T192">pro.h:Th</ta>
            <ta e="T196" id="Seg_4493" s="T195">adv:Time</ta>
            <ta e="T197" id="Seg_4494" s="T196">0.3.h:A</ta>
            <ta e="T198" id="Seg_4495" s="T197">0.3.h:A</ta>
            <ta e="T200" id="Seg_4496" s="T199">adv:L</ta>
            <ta e="T202" id="Seg_4497" s="T201">0.3.h:A</ta>
            <ta e="T203" id="Seg_4498" s="T202">np:Th 0.3.h:Poss</ta>
            <ta e="T204" id="Seg_4499" s="T203">0.3.h:A</ta>
            <ta e="T205" id="Seg_4500" s="T204">np:Th 0.3.h:Poss</ta>
            <ta e="T207" id="Seg_4501" s="T206">0.3.h:A</ta>
            <ta e="T208" id="Seg_4502" s="T350">adv:G</ta>
            <ta e="T211" id="Seg_4503" s="T210">0.3.h:A</ta>
            <ta e="T212" id="Seg_4504" s="T211">0.3.h:A</ta>
            <ta e="T213" id="Seg_4505" s="T351">adv:L</ta>
            <ta e="T214" id="Seg_4506" s="T213">adv:Time</ta>
            <ta e="T215" id="Seg_4507" s="T214">0.3.h:A</ta>
            <ta e="T217" id="Seg_4508" s="T216">np:Th</ta>
            <ta e="T218" id="Seg_4509" s="T217">0.3.h:A</ta>
            <ta e="T219" id="Seg_4510" s="T218">adv:Path</ta>
            <ta e="T222" id="Seg_4511" s="T221">0.3.h:A</ta>
            <ta e="T225" id="Seg_4512" s="T224">np:Time</ta>
            <ta e="T226" id="Seg_4513" s="T225">0.3.h:E</ta>
            <ta e="T227" id="Seg_4514" s="T226">np:Poss</ta>
            <ta e="T228" id="Seg_4515" s="T227">np:L</ta>
            <ta e="T229" id="Seg_4516" s="T228">np:L</ta>
            <ta e="T230" id="Seg_4517" s="T229">np:Th</ta>
            <ta e="T232" id="Seg_4518" s="T231">adv:G</ta>
            <ta e="T233" id="Seg_4519" s="T232">0.3.h:A</ta>
            <ta e="T234" id="Seg_4520" s="T233">0.3.h:E</ta>
            <ta e="T235" id="Seg_4521" s="T234">np.h:A</ta>
            <ta e="T237" id="Seg_4522" s="T236">np:Com 0.3.h:Poss</ta>
            <ta e="T239" id="Seg_4523" s="T238">np.h:E</ta>
            <ta e="T241" id="Seg_4524" s="T240">np.h:A</ta>
            <ta e="T243" id="Seg_4525" s="T242">np:L 0.3.h:Poss</ta>
            <ta e="T245" id="Seg_4526" s="T244">0.3.h:A</ta>
            <ta e="T248" id="Seg_4527" s="T247">0.2.h:A</ta>
            <ta e="T253" id="Seg_4528" s="T252">np:A</ta>
            <ta e="T255" id="Seg_4529" s="T254">pro.h:Poss</ta>
            <ta e="T256" id="Seg_4530" s="T255">np:L</ta>
            <ta e="T257" id="Seg_4531" s="T256">np.h:A</ta>
            <ta e="T259" id="Seg_4532" s="T258">np.h:R 0.3.h:Poss</ta>
            <ta e="T262" id="Seg_4533" s="T261">0.2.h:A</ta>
            <ta e="T264" id="Seg_4534" s="T263">pro.h:Poss</ta>
            <ta e="T265" id="Seg_4535" s="T264">np:L</ta>
            <ta e="T266" id="Seg_4536" s="T265">np:Th</ta>
            <ta e="T268" id="Seg_4537" s="T267">np:A</ta>
            <ta e="T270" id="Seg_4538" s="T269">np.h:A</ta>
            <ta e="T274" id="Seg_4539" s="T273">adv:Time</ta>
            <ta e="T275" id="Seg_4540" s="T274">np:G</ta>
            <ta e="T277" id="Seg_4541" s="T276">np:G</ta>
            <ta e="T278" id="Seg_4542" s="T277">np:Poss</ta>
            <ta e="T279" id="Seg_4543" s="T278">np:L</ta>
            <ta e="T280" id="Seg_4544" s="T279">0.3.h:A</ta>
            <ta e="T281" id="Seg_4545" s="T280">adv:Time</ta>
            <ta e="T283" id="Seg_4546" s="T282">np:Poss</ta>
            <ta e="T284" id="Seg_4547" s="T283">np:Th</ta>
            <ta e="T287" id="Seg_4548" s="T286">0.3.h:A 0.3:Th</ta>
            <ta e="T288" id="Seg_4549" s="T287">0.3.h:A</ta>
            <ta e="T289" id="Seg_4550" s="T288">np:Th</ta>
            <ta e="T290" id="Seg_4551" s="T289">pp:G</ta>
            <ta e="T293" id="Seg_4552" s="T292">np.h:A</ta>
            <ta e="T297" id="Seg_4553" s="T296">np:Com</ta>
            <ta e="T298" id="Seg_4554" s="T297">0.3.h:A</ta>
            <ta e="T299" id="Seg_4555" s="T298">pp:G</ta>
            <ta e="T301" id="Seg_4556" s="T300">np:Ins</ta>
            <ta e="T302" id="Seg_4557" s="T301">0.3.h:A</ta>
            <ta e="T304" id="Seg_4558" s="T303">np:G</ta>
            <ta e="T305" id="Seg_4559" s="T304">0.3.h:A</ta>
            <ta e="T306" id="Seg_4560" s="T305">np:Ins</ta>
            <ta e="T307" id="Seg_4561" s="T306">np.h:E</ta>
            <ta e="T310" id="Seg_4562" s="T309">np:Th</ta>
            <ta e="T311" id="Seg_4563" s="T310">np:A</ta>
            <ta e="T312" id="Seg_4564" s="T311">np:Path 0.3.h:Poss</ta>
            <ta e="T315" id="Seg_4565" s="T314">np:P</ta>
            <ta e="T316" id="Seg_4566" s="T315">np.h:E</ta>
            <ta e="T317" id="Seg_4567" s="T316">0.3.h:P</ta>
            <ta e="T321" id="Seg_4568" s="T320">pro.h:A</ta>
            <ta e="T322" id="Seg_4569" s="T321">np:L</ta>
            <ta e="T324" id="Seg_4570" s="T323">np.h:Poss</ta>
            <ta e="T325" id="Seg_4571" s="T324">np.h:P</ta>
            <ta e="T326" id="Seg_4572" s="T325">0.3.h:A 0.3:Th</ta>
            <ta e="T327" id="Seg_4573" s="T326">np:Poss</ta>
            <ta e="T328" id="Seg_4574" s="T327">np:G</ta>
            <ta e="T330" id="Seg_4575" s="T329">np.h:Poss</ta>
            <ta e="T331" id="Seg_4576" s="T330">np.h:Th</ta>
            <ta e="T333" id="Seg_4577" s="T332">np.h:P</ta>
            <ta e="T334" id="Seg_4578" s="T333">0.3.h:A</ta>
            <ta e="T335" id="Seg_4579" s="T334">0.3.h:A</ta>
            <ta e="T336" id="Seg_4580" s="T335">np:G</ta>
            <ta e="T338" id="Seg_4581" s="T337">np.h:Th</ta>
            <ta e="T341" id="Seg_4582" s="T340">0.3.h:A</ta>
            <ta e="T343" id="Seg_4583" s="T342">0.3.h:A 0.3.h:P</ta>
            <ta e="T344" id="Seg_4584" s="T343">pro.h:Poss</ta>
            <ta e="T345" id="Seg_4585" s="T344">np:Th 0.1.h:Poss</ta>
            <ta e="T346" id="Seg_4586" s="T345">pro.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_4587" s="T1">v:pred</ta>
            <ta e="T3" id="Seg_4588" s="T2">np.h:S</ta>
            <ta e="T5" id="Seg_4589" s="T4">np.h:S</ta>
            <ta e="T6" id="Seg_4590" s="T5">v:pred</ta>
            <ta e="T8" id="Seg_4591" s="T7">0.3.h:S v:pred</ta>
            <ta e="T9" id="Seg_4592" s="T8">np.h:S</ta>
            <ta e="T10" id="Seg_4593" s="T9">v:pred</ta>
            <ta e="T11" id="Seg_4594" s="T10">0.3.h:S v:pred</ta>
            <ta e="T13" id="Seg_4595" s="T12">0.3.h:S v:pred</ta>
            <ta e="T15" id="Seg_4596" s="T14">np.h:O</ta>
            <ta e="T19" id="Seg_4597" s="T18">pro:O</ta>
            <ta e="T20" id="Seg_4598" s="T19">0.2.h:S v:pred</ta>
            <ta e="T21" id="Seg_4599" s="T20">pro.h:S</ta>
            <ta e="T22" id="Seg_4600" s="T21">np:O</ta>
            <ta e="T23" id="Seg_4601" s="T22">v:pred</ta>
            <ta e="T24" id="Seg_4602" s="T23">np.h:S</ta>
            <ta e="T25" id="Seg_4603" s="T24">v:pred</ta>
            <ta e="T27" id="Seg_4604" s="T26">pro.h:S</ta>
            <ta e="T30" id="Seg_4605" s="T29">np:O</ta>
            <ta e="T33" id="Seg_4606" s="T32">v:pred</ta>
            <ta e="T37" id="Seg_4607" s="T36">0.2.h:S v:pred 0.3:O</ta>
            <ta e="T39" id="Seg_4608" s="T38">0.2.h:S v:pred</ta>
            <ta e="T41" id="Seg_4609" s="T40">np.h:S</ta>
            <ta e="T42" id="Seg_4610" s="T41">np:O</ta>
            <ta e="T44" id="Seg_4611" s="T43">v:pred</ta>
            <ta e="T46" id="Seg_4612" s="T45">np:O</ta>
            <ta e="T47" id="Seg_4613" s="T46">0.2.h:S v:pred</ta>
            <ta e="T49" id="Seg_4614" s="T48">np:O</ta>
            <ta e="T50" id="Seg_4615" s="T49">0.2.h:S v:pred</ta>
            <ta e="T51" id="Seg_4616" s="T50">0.2.h:S v:pred 0.3:O</ta>
            <ta e="T53" id="Seg_4617" s="T52">np:S</ta>
            <ta e="T54" id="Seg_4618" s="T53">v:pred</ta>
            <ta e="T55" id="Seg_4619" s="T54">np:O</ta>
            <ta e="T57" id="Seg_4620" s="T56">np:O</ta>
            <ta e="T58" id="Seg_4621" s="T57">pro.h:S</ta>
            <ta e="T59" id="Seg_4622" s="T58">v:pred</ta>
            <ta e="T60" id="Seg_4623" s="T59">np:S</ta>
            <ta e="T61" id="Seg_4624" s="T60">v:pred</ta>
            <ta e="T62" id="Seg_4625" s="T61">np.h:S</ta>
            <ta e="T63" id="Seg_4626" s="T62">v:pred</ta>
            <ta e="T64" id="Seg_4627" s="T63">0.2.h:S v:pred</ta>
            <ta e="T66" id="Seg_4628" s="T65">0.2.h:S v:pred</ta>
            <ta e="T67" id="Seg_4629" s="T66">np.h:S</ta>
            <ta e="T68" id="Seg_4630" s="T67">v:pred</ta>
            <ta e="T69" id="Seg_4631" s="T68">np.h:S</ta>
            <ta e="T70" id="Seg_4632" s="T69">v:pred</ta>
            <ta e="T71" id="Seg_4633" s="T70">0.3.h:S v:pred</ta>
            <ta e="T78" id="Seg_4634" s="T77">0.3.h:S v:pred</ta>
            <ta e="T80" id="Seg_4635" s="T78">s:compl</ta>
            <ta e="T81" id="Seg_4636" s="T80">0.3.h:S v:pred</ta>
            <ta e="T83" id="Seg_4637" s="T82">0.3.h:S v:pred</ta>
            <ta e="T84" id="Seg_4638" s="T83">0.3.h:S cop</ta>
            <ta e="T85" id="Seg_4639" s="T84">adj:pred</ta>
            <ta e="T87" id="Seg_4640" s="T86">v:pred</ta>
            <ta e="T88" id="Seg_4641" s="T87">np:S</ta>
            <ta e="T90" id="Seg_4642" s="T89">np:O</ta>
            <ta e="T92" id="Seg_4643" s="T91">0.2.h:S v:pred</ta>
            <ta e="T93" id="Seg_4644" s="T92">np:S</ta>
            <ta e="T95" id="Seg_4645" s="T94">v:pred 0.3:O</ta>
            <ta e="T96" id="Seg_4646" s="T95">np:O</ta>
            <ta e="T97" id="Seg_4647" s="T96">0.3:S v:pred</ta>
            <ta e="T99" id="Seg_4648" s="T98">np:S</ta>
            <ta e="T101" id="Seg_4649" s="T100">v:pred</ta>
            <ta e="T102" id="Seg_4650" s="T101">0.3.h:S v:pred</ta>
            <ta e="T105" id="Seg_4651" s="T104">0.3.h:S v:pred</ta>
            <ta e="T106" id="Seg_4652" s="T105">0.3.h:S v:pred</ta>
            <ta e="T107" id="Seg_4653" s="T106">0.3.h:S v:pred</ta>
            <ta e="T112" id="Seg_4654" s="T107">s:compl</ta>
            <ta e="T113" id="Seg_4655" s="T112">0.3.h:S v:pred</ta>
            <ta e="T114" id="Seg_4656" s="T113">0.3.h:S v:pred</ta>
            <ta e="T115" id="Seg_4657" s="T114">0.3.h:S v:pred</ta>
            <ta e="T121" id="Seg_4658" s="T115">s:compl</ta>
            <ta e="T122" id="Seg_4659" s="T121">0.3.h:S v:pred</ta>
            <ta e="T124" id="Seg_4660" s="T122">s:compl</ta>
            <ta e="T126" id="Seg_4661" s="T125">v:pred</ta>
            <ta e="T127" id="Seg_4662" s="T126">np:S</ta>
            <ta e="T128" id="Seg_4663" s="T127">ptcl:pred</ta>
            <ta e="T130" id="Seg_4664" s="T129">v:S</ta>
            <ta e="T131" id="Seg_4665" s="T130">v:S</ta>
            <ta e="T133" id="Seg_4666" s="T132">0.3.h:S v:pred</ta>
            <ta e="T135" id="Seg_4667" s="T134">np:O</ta>
            <ta e="T136" id="Seg_4668" s="T135">s:adv</ta>
            <ta e="T137" id="Seg_4669" s="T136">0.3.h:S v:pred</ta>
            <ta e="T139" id="Seg_4670" s="T138">np:O</ta>
            <ta e="T140" id="Seg_4671" s="T139">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T141" id="Seg_4672" s="T140">adj:pred</ta>
            <ta e="T142" id="Seg_4673" s="T141">0.3:S cop</ta>
            <ta e="T143" id="Seg_4674" s="T142">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T148" id="Seg_4675" s="T147">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T150" id="Seg_4676" s="T149">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T153" id="Seg_4677" s="T152">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T159" id="Seg_4678" s="T158">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T166" id="Seg_4679" s="T165">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T167" id="Seg_4680" s="T166">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T168" id="Seg_4681" s="T167">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T170" id="Seg_4682" s="T169">0.3.h:S v:pred</ta>
            <ta e="T172" id="Seg_4683" s="T171">v:S</ta>
            <ta e="T173" id="Seg_4684" s="T172">ptcl:pred</ta>
            <ta e="T174" id="Seg_4685" s="T173">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T175" id="Seg_4686" s="T174">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T176" id="Seg_4687" s="T175">0.3.h:S v:pred</ta>
            <ta e="T178" id="Seg_4688" s="T177">np:O</ta>
            <ta e="T179" id="Seg_4689" s="T178">adj:pred</ta>
            <ta e="T180" id="Seg_4690" s="T179">0.3:S cop</ta>
            <ta e="T182" id="Seg_4691" s="T181">0.3.h:S v:pred</ta>
            <ta e="T184" id="Seg_4692" s="T183">np:O</ta>
            <ta e="T189" id="Seg_4693" s="T188">np:S</ta>
            <ta e="T190" id="Seg_4694" s="T189">v:pred</ta>
            <ta e="T193" id="Seg_4695" s="T192">pro.h:S</ta>
            <ta e="T194" id="Seg_4696" s="T193">cop</ta>
            <ta e="T195" id="Seg_4697" s="T194">n:pred</ta>
            <ta e="T197" id="Seg_4698" s="T196">0.3.h:S v:pred</ta>
            <ta e="T198" id="Seg_4699" s="T197">0.3.h:S v:pred</ta>
            <ta e="T201" id="Seg_4700" s="T200">s:adv</ta>
            <ta e="T202" id="Seg_4701" s="T201">0.3.h:S v:pred</ta>
            <ta e="T203" id="Seg_4702" s="T202">np:O</ta>
            <ta e="T204" id="Seg_4703" s="T203">0.3.h:S v:pred</ta>
            <ta e="T205" id="Seg_4704" s="T204">np:O</ta>
            <ta e="T207" id="Seg_4705" s="T206">0.3.h:S v:pred</ta>
            <ta e="T211" id="Seg_4706" s="T210">0.3.h:S v:pred</ta>
            <ta e="T212" id="Seg_4707" s="T211">0.3.h:S v:pred</ta>
            <ta e="T215" id="Seg_4708" s="T214">0.3.h:S v:pred</ta>
            <ta e="T216" id="Seg_4709" s="T215">s:adv</ta>
            <ta e="T217" id="Seg_4710" s="T216">np:O</ta>
            <ta e="T218" id="Seg_4711" s="T217">0.3.h:S v:pred</ta>
            <ta e="T222" id="Seg_4712" s="T221">0.3.h:S v:pred</ta>
            <ta e="T223" id="Seg_4713" s="T222">s:adv</ta>
            <ta e="T226" id="Seg_4714" s="T225">0.3.h:S v:pred</ta>
            <ta e="T230" id="Seg_4715" s="T229">np:S</ta>
            <ta e="T231" id="Seg_4716" s="T230">v:pred</ta>
            <ta e="T233" id="Seg_4717" s="T232">0.3.h:S v:pred</ta>
            <ta e="T234" id="Seg_4718" s="T233">0.3.h:S v:pred</ta>
            <ta e="T235" id="Seg_4719" s="T234">np.h:S</ta>
            <ta e="T238" id="Seg_4720" s="T237">v:pred</ta>
            <ta e="T239" id="Seg_4721" s="T238">np.h:S</ta>
            <ta e="T240" id="Seg_4722" s="T239">v:pred</ta>
            <ta e="T241" id="Seg_4723" s="T240">np.h:S</ta>
            <ta e="T242" id="Seg_4724" s="T241">v:pred</ta>
            <ta e="T244" id="Seg_4725" s="T243">s:adv</ta>
            <ta e="T245" id="Seg_4726" s="T244">0.3.h:S v:pred</ta>
            <ta e="T248" id="Seg_4727" s="T247">0.2.h:S v:pred</ta>
            <ta e="T253" id="Seg_4728" s="T252">np:S</ta>
            <ta e="T254" id="Seg_4729" s="T253">v:pred</ta>
            <ta e="T257" id="Seg_4730" s="T256">np.h:S</ta>
            <ta e="T258" id="Seg_4731" s="T257">v:pred</ta>
            <ta e="T262" id="Seg_4732" s="T261">0.2.h:S v:pred</ta>
            <ta e="T266" id="Seg_4733" s="T262">s:compl</ta>
            <ta e="T269" id="Seg_4734" s="T266">s:compl</ta>
            <ta e="T270" id="Seg_4735" s="T269">np.h:S</ta>
            <ta e="T271" id="Seg_4736" s="T270">s:adv</ta>
            <ta e="T272" id="Seg_4737" s="T271">v:pred</ta>
            <ta e="T280" id="Seg_4738" s="T279">0.3.h:S v:pred</ta>
            <ta e="T286" id="Seg_4739" s="T281">s:temp</ta>
            <ta e="T287" id="Seg_4740" s="T286">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T288" id="Seg_4741" s="T287">0.3.h:S v:pred</ta>
            <ta e="T289" id="Seg_4742" s="T288">np:O</ta>
            <ta e="T293" id="Seg_4743" s="T292">np.h:S</ta>
            <ta e="T294" id="Seg_4744" s="T293">v:pred</ta>
            <ta e="T295" id="Seg_4745" s="T294">s:adv</ta>
            <ta e="T298" id="Seg_4746" s="T297">0.3.h:S v:pred</ta>
            <ta e="T302" id="Seg_4747" s="T301">0.3.h:S v:pred</ta>
            <ta e="T305" id="Seg_4748" s="T304">0.3.h:S v:pred</ta>
            <ta e="T307" id="Seg_4749" s="T306">np.h:S</ta>
            <ta e="T308" id="Seg_4750" s="T307">v:pred</ta>
            <ta e="T310" id="Seg_4751" s="T309">np:O</ta>
            <ta e="T311" id="Seg_4752" s="T310">np:S</ta>
            <ta e="T313" id="Seg_4753" s="T312">v:pred</ta>
            <ta e="T315" id="Seg_4754" s="T314">np:O</ta>
            <ta e="T316" id="Seg_4755" s="T315">np.h:S</ta>
            <ta e="T317" id="Seg_4756" s="T316">s:compl</ta>
            <ta e="T319" id="Seg_4757" s="T318">v:pred</ta>
            <ta e="T321" id="Seg_4758" s="T320">pro.h:S</ta>
            <ta e="T323" id="Seg_4759" s="T322">v:pred</ta>
            <ta e="T325" id="Seg_4760" s="T324">np.h:O</ta>
            <ta e="T326" id="Seg_4761" s="T325">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T331" id="Seg_4762" s="T330">np.h:O</ta>
            <ta e="T334" id="Seg_4763" s="T332">s:temp</ta>
            <ta e="T335" id="Seg_4764" s="T334">0.3.h:S v:pred</ta>
            <ta e="T338" id="Seg_4765" s="T337">np.h:O</ta>
            <ta e="T341" id="Seg_4766" s="T340">0.3.h:S v:pred</ta>
            <ta e="T343" id="Seg_4767" s="T342">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T347" id="Seg_4768" s="T343">s:compl</ta>
            <ta e="T349" id="Seg_4769" s="T348">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T65" id="Seg_4770" s="T64">RUS:gram</ta>
            <ta e="T82" id="Seg_4771" s="T81">RUS:gram</ta>
            <ta e="T98" id="Seg_4772" s="T97">RUS:gram</ta>
            <ta e="T110" id="Seg_4773" s="T109">RUS:gram</ta>
            <ta e="T128" id="Seg_4774" s="T127">RUS:mod</ta>
            <ta e="T129" id="Seg_4775" s="T128">RUS:cult</ta>
            <ta e="T132" id="Seg_4776" s="T131">RUS:cult</ta>
            <ta e="T162" id="Seg_4777" s="T161">RUS:cult</ta>
            <ta e="T173" id="Seg_4778" s="T172">RUS:mod</ta>
            <ta e="T186" id="Seg_4779" s="T185">RUS:gram</ta>
            <ta e="T206" id="Seg_4780" s="T205">RUS:gram</ta>
            <ta e="T209" id="Seg_4781" s="T208">RUS:gram</ta>
            <ta e="T329" id="Seg_4782" s="T328">RUS:gram</ta>
            <ta e="T342" id="Seg_4783" s="T341">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T1" id="Seg_4784" s="T0">Ичакичика.</ta>
            <ta e="T4" id="Seg_4785" s="T1">Жил Ичакичика с бабушкой.</ta>
            <ta e="T10" id="Seg_4786" s="T4">Ичакичика идёт в лес, видит, человек [деревья] рубит.</ta>
            <ta e="T17" id="Seg_4787" s="T10">Подходит ближе, видит, большой чёрт.</ta>
            <ta e="T20" id="Seg_4788" s="T17">"Дедушка, что ты делаешь?"</ta>
            <ta e="T26" id="Seg_4789" s="T20">"Я деревья рублю," – говорит (/сказал) чёрт.</ta>
            <ta e="T33" id="Seg_4790" s="T26">"Я раньше это дерево руками расщеплял.</ta>
            <ta e="T39" id="Seg_4791" s="T33">Ну-ка, внук, расщепи, ну-ка, попробуй!" </ta>
            <ta e="T44" id="Seg_4792" s="T39">Старик-чёрт дерево расщепляет.</ta>
            <ta e="T52" id="Seg_4793" s="T44">"Ну-ка руку засунь, две руки впихни, ну-ка толкни!"</ta>
            <ta e="T57" id="Seg_4794" s="T52">Дерево защемило руку, обе руки.</ta>
            <ta e="T61" id="Seg_4795" s="T57">Он [Ичакычика] сидит, сил нет.</ta>
            <ta e="T66" id="Seg_4796" s="T61">Чёрт сказал: "Сиди и сохни."</ta>
            <ta e="T68" id="Seg_4797" s="T66">Чёрт ушел.</ta>
            <ta e="T77" id="Seg_4798" s="T68">Ичакичика сидел, сидел, долго ли, [коротко ли?].</ta>
            <ta e="T80" id="Seg_4799" s="T77">Думал, как выбраться.</ta>
            <ta e="T85" id="Seg_4800" s="T80">Ходил, ходил, бился.</ta>
            <ta e="T88" id="Seg_4801" s="T85">К нему приходит медведь.</ta>
            <ta e="T92" id="Seg_4802" s="T88">"Дедушка, расщепи дерево."</ta>
            <ta e="T97" id="Seg_4803" s="T92">Медведь [дерево] расщепил, руку вытащил.</ta>
            <ta e="T101" id="Seg_4804" s="T97">И руки [Ичакычики], свободные, упали.</ta>
            <ta e="T104" id="Seg_4805" s="T101">Он выбирается из [щели] дерева.</ta>
            <ta e="T112" id="Seg_4806" s="T104">Ходит, ходит, думает, как догнать и увидеть чёрта.</ta>
            <ta e="T121" id="Seg_4807" s="T112">Ходит, ходит, думает, как чёрта догнать, схватить, и как он, [его] схватив, возьмёт.</ta>
            <ta e="T124" id="Seg_4808" s="T121">Думает, куда податься.</ta>
            <ta e="T131" id="Seg_4809" s="T124">В голову ему приходит мысль: "Нужно в моём кармане посмотреть, [пощупать?]!"</ta>
            <ta e="T135" id="Seg_4810" s="T131">В кармане он нашёл шкуру ястреба.</ta>
            <ta e="T139" id="Seg_4811" s="T135">Он стал стоя мять шкуру ястреба-мышелова.</ta>
            <ta e="T142" id="Seg_4812" s="T139">Мял-мял, она побольше стала.</ta>
            <ta e="T144" id="Seg_4813" s="T142">Надел её на голову.</ta>
            <ta e="T148" id="Seg_4814" s="T144">Потом на спину стянул.</ta>
            <ta e="T150" id="Seg_4815" s="T148">Снова начал мять.</ta>
            <ta e="T155" id="Seg_4816" s="T150">И снова надел на голову, на тело.</ta>
            <ta e="T164" id="Seg_4817" s="T155">Потом опять стянул с кожи [тела] и с головы.</ta>
            <ta e="T168" id="Seg_4818" s="T164">Снова мял, мял, мял.</ta>
            <ta e="T173" id="Seg_4819" s="T168">Снова подумал: "Опять мять нужно".</ta>
            <ta e="T178" id="Seg_4820" s="T173">Мял, мял, мял шкуру ястреба.</ta>
            <ta e="T180" id="Seg_4821" s="T178">Она большая становится. </ta>
            <ta e="T187" id="Seg_4822" s="T180">Теперь он надел шкуру ястреба на тело, на кожу.</ta>
            <ta e="T191" id="Seg_4823" s="T187">Шкуры ястреба хватило ему до ног.</ta>
            <ta e="T195" id="Seg_4824" s="T191">После этого он уже ястребом становится.</ta>
            <ta e="T200" id="Seg_4825" s="T195">Потом взлетает, садится поодаль подальше.</ta>
            <ta e="T203" id="Seg_4826" s="T200">Сидя, потряхивает перьями.</ta>
            <ta e="T208" id="Seg_4827" s="T203">Потряс перьями и взлетел в небо. </ta>
            <ta e="T213" id="Seg_4828" s="T208">И спланировал, развернулся в небе.</ta>
            <ta e="T223" id="Seg_4829" s="T213">Потом взлетел, полетел ниже вдоль реки или поднимался взлетая.</ta>
            <ta e="T231" id="Seg_4830" s="T223">Однажды видит, по реке ветка плывёт.</ta>
            <ta e="T233" id="Seg_4831" s="T231">Немного ниже спустился.</ta>
            <ta e="T238" id="Seg_4832" s="T233">Видит, чёрт с двумя дочерьми едет.</ta>
            <ta e="T243" id="Seg_4833" s="T238">Чёрт видит, ястреб кружит над головой.</ta>
            <ta e="T245" id="Seg_4834" s="T243">Летая, кружится.</ta>
            <ta e="T256" id="Seg_4835" s="T245">"Ой, дочка, смотри, какой красивый зверь летает над нашими головами!"</ta>
            <ta e="T269" id="Seg_4836" s="T256">Чёрт поёт дочерям: "Дочка, дочка, посмотри, какой над нами зверь, [посмотри], как зверь летит!"</ta>
            <ta e="T280" id="Seg_4837" s="T269">Ястреб, взлетев, отправился прежде них на реку, сел на большую лиственницу на берегу реки. </ta>
            <ta e="T292" id="Seg_4838" s="T280">Затем, стянув верхушки двух лиственниц, повесил силок в реке.</ta>
            <ta e="T297" id="Seg_4839" s="T292">Чёрт едет, напевая, [и с ним] две дочки.</ta>
            <ta e="T306" id="Seg_4840" s="T297">Приехал, к силку на ветке подъехал, близко к силку приехал на ветке.</ta>
            <ta e="T310" id="Seg_4841" s="T306">Чёрт не видит силок.</ta>
            <ta e="T315" id="Seg_4842" s="T310">Силок через его шею стянул две лиственницы.</ta>
            <ta e="T320" id="Seg_4843" s="T315">Чёрт смерти не знает (/умер).</ta>
            <ta e="T336" id="Seg_4844" s="T320">Он [Ичакычика] поймал в ветке дочерей чёрта, пристал к берегу и дочь чёрта, одну дочь, растоптав, бросил в глину.</ta>
            <ta e="T343" id="Seg_4845" s="T336">Вторую дочь он тоже бросил и растоптал.</ta>
            <ta e="T349" id="Seg_4846" s="T343">Кто мою сказку слушал, [тот] навсегда скушал.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T1" id="Seg_4847" s="T0">Ichakichika.</ta>
            <ta e="T4" id="Seg_4848" s="T1">Ichakichika lived with his grandmother.</ta>
            <ta e="T10" id="Seg_4849" s="T4">Ichakichika goes to the forest and sees that one human is chopping [trees].</ta>
            <ta e="T17" id="Seg_4850" s="T10">He comes closer and sees that it is a big devil.</ta>
            <ta e="T20" id="Seg_4851" s="T17">"Grandfather, what are you doing?"</ta>
            <ta e="T26" id="Seg_4852" s="T20">"I'm chopping trees," – the devil says (/said).</ta>
            <ta e="T33" id="Seg_4853" s="T26">"Earlier I used to split this tree with my bare hands.</ta>
            <ta e="T39" id="Seg_4854" s="T33">Come on, grandson, split it, try it!"</ta>
            <ta e="T44" id="Seg_4855" s="T39">The old devil splits the tree.</ta>
            <ta e="T52" id="Seg_4856" s="T44">"Come on, stick your hand in, stick both hands in, come on, now push!"</ta>
            <ta e="T57" id="Seg_4857" s="T52">The tree squeezed his hand, both hands.</ta>
            <ta e="T61" id="Seg_4858" s="T57">He [Ichakichika] is sitting there without force.</ta>
            <ta e="T66" id="Seg_4859" s="T61">The devil said: "Sit and dry out."</ta>
            <ta e="T68" id="Seg_4860" s="T66">The devil went away.</ta>
            <ta e="T77" id="Seg_4861" s="T68">Ichakichika was sitting there [sooner or later?].</ta>
            <ta e="T80" id="Seg_4862" s="T77">He considered how he could get out.</ta>
            <ta e="T85" id="Seg_4863" s="T80">He walked and walked, struggled [with the tree].</ta>
            <ta e="T88" id="Seg_4864" s="T85">A bear came to him.</ta>
            <ta e="T92" id="Seg_4865" s="T88">"Grandfather, split the tree."</ta>
            <ta e="T97" id="Seg_4866" s="T92">The bear split [the tree] and pulled his hand out.</ta>
            <ta e="T101" id="Seg_4867" s="T97">And Ichakichika's hands came unstuck.</ta>
            <ta e="T104" id="Seg_4868" s="T101">He pulls them out of the tree.</ta>
            <ta e="T112" id="Seg_4869" s="T104">He walks and walks, thinking how he could chase and meet the devil.</ta>
            <ta e="T121" id="Seg_4870" s="T112">He walks and walks, thinking how he could chase, catch and take the devil.</ta>
            <ta e="T124" id="Seg_4871" s="T121">He thinks where to go.</ta>
            <ta e="T131" id="Seg_4872" s="T124">An idea came to him: "I have to look into my pocket, [feel it?]!"</ta>
            <ta e="T135" id="Seg_4873" s="T131">In the pocket he found the skin of a hawk.</ta>
            <ta e="T139" id="Seg_4874" s="T135">He began to knead the skin of the hawk.</ta>
            <ta e="T142" id="Seg_4875" s="T139">He kneaded and kneaded, it got bigger.</ta>
            <ta e="T144" id="Seg_4876" s="T142">He put it on his head.</ta>
            <ta e="T148" id="Seg_4877" s="T144">Then he pulled it down onto his back.</ta>
            <ta e="T150" id="Seg_4878" s="T148">He began to knead the skin again.</ta>
            <ta e="T155" id="Seg_4879" s="T150">And again he put it on his head, on his body.</ta>
            <ta e="T164" id="Seg_4880" s="T155">Then he took it off from his skin [his body] and from his head.</ta>
            <ta e="T168" id="Seg_4881" s="T164">He kneaded, kneaded and kneaded the skin again.</ta>
            <ta e="T173" id="Seg_4882" s="T168">He thought again: "I have to knead it again".</ta>
            <ta e="T178" id="Seg_4883" s="T173">He kneaded, kneaded and kneaded the skin of the hawk.</ta>
            <ta e="T180" id="Seg_4884" s="T178">It got bigger.</ta>
            <ta e="T187" id="Seg_4885" s="T180">Now he put the skin of the hawk on his body, on his skin.</ta>
            <ta e="T191" id="Seg_4886" s="T187">The skin of the hawk reached him to the legs.</ta>
            <ta e="T195" id="Seg_4887" s="T191">After this he already turned into a hawk.</ta>
            <ta e="T200" id="Seg_4888" s="T195">Then he flies up, sits down a little further on.</ta>
            <ta e="T203" id="Seg_4889" s="T200">Sitting he shakes his feathers.</ta>
            <ta e="T208" id="Seg_4890" s="T203">He shook his feathers and flew up to the sky.</ta>
            <ta e="T213" id="Seg_4891" s="T208">And he soared there, circled in the sky.</ta>
            <ta e="T223" id="Seg_4892" s="T213">Then he flew up, flew down along the river or flew up again.</ta>
            <ta e="T231" id="Seg_4893" s="T223">Once he sees a boat swimming on the river.</ta>
            <ta e="T233" id="Seg_4894" s="T231">He flew a little bit down.</ta>
            <ta e="T238" id="Seg_4895" s="T233">He sees the devil driving with his dwo daughters.</ta>
            <ta e="T243" id="Seg_4896" s="T238">The devil sees the hawk circling over his head.</ta>
            <ta e="T245" id="Seg_4897" s="T243">Flying it circles.</ta>
            <ta e="T256" id="Seg_4898" s="T245">"Oh, daughters, look, what a beautiful wild animal is flying over our heads!"</ta>
            <ta e="T269" id="Seg_4899" s="T256">The devil sings to his daughters: "Daughters, daughters, look, what kind of animal is over us, [look] how the animal is flying!"</ta>
            <ta e="T280" id="Seg_4900" s="T269">The hawk flew up, came earlier to the river than they, it sat down on a big larch on the riverbank.</ta>
            <ta e="T292" id="Seg_4901" s="T280">Then, having pulled the tops of two larches together, it hung up a snare in the middle of the river.</ta>
            <ta e="T297" id="Seg_4902" s="T292">The devil comes singing, [with him] are his two daughters.</ta>
            <ta e="T306" id="Seg_4903" s="T297">He came, approached the snare on his boat.</ta>
            <ta e="T310" id="Seg_4904" s="T306">The devil doesn't see the snare.</ta>
            <ta e="T315" id="Seg_4905" s="T310">The snare pulled the two larches together through his neck.</ta>
            <ta e="T320" id="Seg_4906" s="T315">The devil didn't notice that he died.</ta>
            <ta e="T336" id="Seg_4907" s="T320">He [Ichakichika] caught the devil's daughters in the boat, pulled it to the shore, and threw at first one of the devil's daughters into the clay and trampled her.</ta>
            <ta e="T343" id="Seg_4908" s="T336">He threw also the second daughter there and trampled her.</ta>
            <ta e="T349" id="Seg_4909" s="T343">The one, who listened to my fairytale, has eaten forever.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T1" id="Seg_4910" s="T0">Itschakitschika.</ta>
            <ta e="T4" id="Seg_4911" s="T1">Es lebte Itschakitschika mit seiner Großmutter.</ta>
            <ta e="T10" id="Seg_4912" s="T4">Itschakitschika geht in den Wald und sieht, dass ein Mensch dort [Bäume] fällt.</ta>
            <ta e="T17" id="Seg_4913" s="T10">Er kommt näher heran, sieht, ein großer Teufel.</ta>
            <ta e="T20" id="Seg_4914" s="T17">"Großvater, was machst du?"</ta>
            <ta e="T26" id="Seg_4915" s="T20">"Ich fälle Bäume", – sagt (/sagte) der Teufel.</ta>
            <ta e="T33" id="Seg_4916" s="T26">"Früher habe ich diesen Baum mit den Händen gespalten.</ta>
            <ta e="T39" id="Seg_4917" s="T33">Spalte ihn mal, Enkelchen, versuch es!"</ta>
            <ta e="T44" id="Seg_4918" s="T39">Der alte Teufel spaltet den Baum.</ta>
            <ta e="T52" id="Seg_4919" s="T44">"Jetzt steck mal deine Hand hinein, steck beide Hände hinein, nun schieb!"</ta>
            <ta e="T57" id="Seg_4920" s="T52">Der Baum klemmte seine Hand ein, beide Hände.</ta>
            <ta e="T61" id="Seg_4921" s="T57">Er [Itschakitschika] sitzt dort, ohne Kraft.</ta>
            <ta e="T66" id="Seg_4922" s="T61">Der Teufel sagte: "Sitz da und vertrockne."</ta>
            <ta e="T68" id="Seg_4923" s="T66">Der Teufel ging weg.</ta>
            <ta e="T77" id="Seg_4924" s="T68">Itschakitschika saß und saß, [kurz oder lang?].</ta>
            <ta e="T80" id="Seg_4925" s="T77">Er überlegte, wie er seine Hände herausziehen könnte.</ta>
            <ta e="T85" id="Seg_4926" s="T80">Er ging und ging, rang [mit dem Baum].</ta>
            <ta e="T88" id="Seg_4927" s="T85">Ein Bär kommt zu ihm.</ta>
            <ta e="T92" id="Seg_4928" s="T88">"Großvater, spalte den Baum."</ta>
            <ta e="T97" id="Seg_4929" s="T92">Der Bär spaltete [den Baum] und zog die Hand heraus.</ta>
            <ta e="T101" id="Seg_4930" s="T97">Itschakitschikas Hände kamen frei.</ta>
            <ta e="T104" id="Seg_4931" s="T101">Er zieht sie aus dem Baum heraus.</ta>
            <ta e="T112" id="Seg_4932" s="T104">Er geht und geht und denkt nach, wie er den Teufel jagen und treffen könnte.</ta>
            <ta e="T121" id="Seg_4933" s="T112">Er geht und geht, er denkt nach, wie er den Teufel fangen, greifen und nehmen könnte.</ta>
            <ta e="T124" id="Seg_4934" s="T121">Er denkt nach, wohin er gehen sollte.</ta>
            <ta e="T131" id="Seg_4935" s="T124">Es kommt ihm der Gedanke: "Ich müsste in meine Tasche schauen, [befühlen?]!"</ta>
            <ta e="T135" id="Seg_4936" s="T131">In der Tasche fand er die Haut eines Habichts.</ta>
            <ta e="T139" id="Seg_4937" s="T135">Stehend fing er an, die Haut des Habichtes weich zu machen.</ta>
            <ta e="T142" id="Seg_4938" s="T139">Er machte sie weich, sie wurde etwas größer.</ta>
            <ta e="T144" id="Seg_4939" s="T142">Er zog sie über den Kopf.</ta>
            <ta e="T148" id="Seg_4940" s="T144">Dann zog er sie auf den Rücken hinunter.</ta>
            <ta e="T150" id="Seg_4941" s="T148">Er machte die Haut wieder weich.</ta>
            <ta e="T155" id="Seg_4942" s="T150">Er zog sie noch mal über den Kopf, über den Körper.</ta>
            <ta e="T164" id="Seg_4943" s="T155">Dann nahm er sie wieder vom Körper und vom Kopf.</ta>
            <ta e="T168" id="Seg_4944" s="T164">Er machte die Haut wieder weich, machte sie weich und machte sie weich.</ta>
            <ta e="T173" id="Seg_4945" s="T168">Er dachte wieder: "Ich muss sie wieder weich machen."</ta>
            <ta e="T178" id="Seg_4946" s="T173">Er machte die Haut des Habichts wieder weich, machte sie weich und machte sie weich.</ta>
            <ta e="T180" id="Seg_4947" s="T178">Sie wird größer. </ta>
            <ta e="T187" id="Seg_4948" s="T180">Nun zog er die Haut des Habichts auf seinen Körper, auf seine Haut.</ta>
            <ta e="T191" id="Seg_4949" s="T187">Die Haut des Habichts reichte ihm bis zu den Beinen.</ta>
            <ta e="T195" id="Seg_4950" s="T191">Da wurde er schon ein Habicht.</ta>
            <ta e="T200" id="Seg_4951" s="T195">Dann fliegt er los, landet etwas weiter weg.</ta>
            <ta e="T203" id="Seg_4952" s="T200">Sitzend schüttelt er seine Federn.</ta>
            <ta e="T208" id="Seg_4953" s="T203">Er schüttelte sie und flog hinauf zum Himmel.</ta>
            <ta e="T213" id="Seg_4954" s="T208">Und segelte dort, kreiste am Himmel.</ta>
            <ta e="T223" id="Seg_4955" s="T213">Dann flog er hinauf, er flog den Fluss entlang oder flog hinauf.</ta>
            <ta e="T231" id="Seg_4956" s="T223">Einmal sieht er, dass auf dem Fluss ein Boot schwimmt.</ta>
            <ta e="T233" id="Seg_4957" s="T231">Er flog etwas niedriger.</ta>
            <ta e="T238" id="Seg_4958" s="T233">Er sah den Teufel mit seinen zwei Töchtern fahren.</ta>
            <ta e="T243" id="Seg_4959" s="T238">Der Teufel sieht, dass der Habicht über seinem Kopf kreist.</ta>
            <ta e="T245" id="Seg_4960" s="T243">Er flog und kreiste.</ta>
            <ta e="T256" id="Seg_4961" s="T245">"Hej, Töchter, schaut, was für ein schönes gemustertes Wildtier über unseren Köpfen schwebt!"</ta>
            <ta e="T269" id="Seg_4962" s="T256">Der Teufel singt zu den Töchtern: "Töchter, Töchter, schaut, was für ein Tier über uns ist, [seht], wie das Tier fliegt!"</ta>
            <ta e="T280" id="Seg_4963" s="T269">Der Habicht flog los, kam vor ihnen am Fluss an und setzte sich auf eine große Lärche am Ufer des Flusses.</ta>
            <ta e="T292" id="Seg_4964" s="T280">Dann zog er die Wipfel zweier Lärchen zusammen und hängte eine Schlinge in die Mitte des Flusses.</ta>
            <ta e="T297" id="Seg_4965" s="T292">Der Teufel fährt singend, [mit den] beiden Töchtern.</ta>
            <ta e="T306" id="Seg_4966" s="T297">Er kam [dorthin], fuhr mit dem Boot näher an die Falle heran.</ta>
            <ta e="T310" id="Seg_4967" s="T306">Der Teufel sieht die Schlinge nicht.</ta>
            <ta e="T315" id="Seg_4968" s="T310">Die Schlinge zog die beiden Lärchen durch seinen Hals zusammen.</ta>
            <ta e="T320" id="Seg_4969" s="T315">Der Teufel merkte nicht, dass er starb.</ta>
            <ta e="T336" id="Seg_4970" s="T320">Er [Itschakitschika] nahm die Töchter des Teufels aus dem Boot, zog es ans Ufer und warf zunächst eine der Töchter des Teufels in den Schlamm.</ta>
            <ta e="T343" id="Seg_4971" s="T336">Die zweite Tochter warf er auch in den Schlamm und zertrat sie.</ta>
            <ta e="T349" id="Seg_4972" s="T343">Wer mein Märchen gehört hat, hat für immer gegessen.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_4973" s="T1">жил с бабушкой</ta>
            <ta e="T10" id="Seg_4974" s="T4">пошел в лес видит, человек рубит</ta>
            <ta e="T17" id="Seg_4975" s="T10">подошел ближе ходит черт</ta>
            <ta e="T20" id="Seg_4976" s="T17">дедушка, что ты делаешь?</ta>
            <ta e="T33" id="Seg_4977" s="T26">я раньше это дерево руками рвал [распорол, разрывал, расщеплял]</ta>
            <ta e="T39" id="Seg_4978" s="T33">ну-ка внучка расщепи ну-ка попробуй </ta>
            <ta e="T44" id="Seg_4979" s="T39">старик черт дерево расщепил</ta>
            <ta e="T52" id="Seg_4980" s="T44">ну-ка рукой толкай обе руки твои толкай [впихивай] подтяни [оттяни, тяни]</ta>
            <ta e="T57" id="Seg_4981" s="T52">дерево поймало руки обе руки</ta>
            <ta e="T61" id="Seg_4982" s="T57">он сидит силы нет</ta>
            <ta e="T66" id="Seg_4983" s="T61">черт сиди и сушись</ta>
            <ta e="T68" id="Seg_4984" s="T66">черт ушел</ta>
            <ta e="T77" id="Seg_4985" s="T68">сидел сидел долго ли</ta>
            <ta e="T80" id="Seg_4986" s="T77">думает как вырвать [выручить]</ta>
            <ta e="T85" id="Seg_4987" s="T80">ходит и ходит</ta>
            <ta e="T88" id="Seg_4988" s="T85">к нему пришел медведь</ta>
            <ta e="T92" id="Seg_4989" s="T88">медведь дерево порви</ta>
            <ta e="T97" id="Seg_4990" s="T92">медведь рванул пополам распорол</ta>
            <ta e="T101" id="Seg_4991" s="T97">руки свободны стали</ta>
            <ta e="T104" id="Seg_4992" s="T101">вышел из дерева (из щели)</ta>
            <ta e="T112" id="Seg_4993" s="T104">ходит ходит и думает как догнать и увидеть черта [лешего]</ta>
            <ta e="T121" id="Seg_4994" s="T112">ходил ходил думает как лешего поймать схватить схватив взять</ta>
            <ta e="T124" id="Seg_4995" s="T121">думал что делать куда деться</ta>
            <ta e="T131" id="Seg_4996" s="T124">в голову упал ум нужно в кармане смотреть щупать</ta>
            <ta e="T135" id="Seg_4997" s="T131">в кармане нашел коршун [ястреб-стервятник] шкура</ta>
            <ta e="T139" id="Seg_4998" s="T135">мять [мягчить] стал мышелова шкура</ta>
            <ta e="T142" id="Seg_4999" s="T139">мял побольше [не такой большой, средний] стала</ta>
            <ta e="T144" id="Seg_5000" s="T142">на голову одел</ta>
            <ta e="T148" id="Seg_5001" s="T144">потом снял с головы</ta>
            <ta e="T150" id="Seg_5002" s="T148">опять мял [начинал мять]</ta>
            <ta e="T155" id="Seg_5003" s="T150">опять одел до туловища [на голову и на туловище] одел</ta>
            <ta e="T164" id="Seg_5004" s="T155">снял с туловища и с головы</ta>
            <ta e="T173" id="Seg_5005" s="T168">а думает опять мять надо</ta>
            <ta e="T180" id="Seg_5006" s="T178">большая стала </ta>
            <ta e="T187" id="Seg_5007" s="T180">до половины тела полностью все тело </ta>
            <ta e="T191" id="Seg_5008" s="T187">мышелова шкура хватило ему до ног</ta>
            <ta e="T195" id="Seg_5009" s="T191">он уже соколом стал</ta>
            <ta e="T200" id="Seg_5010" s="T195">потом полетел сел там [подальше] там [поближе]</ta>
            <ta e="T203" id="Seg_5011" s="T200">сидя потрясает [отряхивает] свои перышки</ta>
            <ta e="T208" id="Seg_5012" s="T203">и улетел на небо </ta>
            <ta e="T213" id="Seg_5013" s="T208">планирует поворот давал на небе</ta>
            <ta e="T223" id="Seg_5014" s="T213">потом улетел по речке догонять проверить вдоль</ta>
            <ta e="T231" id="Seg_5015" s="T223">увидел вдоль реки по реке ветка плывет</ta>
            <ta e="T233" id="Seg_5016" s="T231">ниже немного спустился</ta>
            <ta e="T238" id="Seg_5017" s="T233">видит с двумя девушками</ta>
            <ta e="T243" id="Seg_5018" s="T238">черт увидел сокол над головой</ta>
            <ta e="T245" id="Seg_5019" s="T243">летел кругами [кружится]</ta>
            <ta e="T256" id="Seg_5020" s="T245">дочки смотрите красивенький летает над нашими головами [верхом]</ta>
            <ta e="T269" id="Seg_5021" s="T256">черт поет девочкам какой зверь летит</ta>
            <ta e="T280" id="Seg_5022" s="T269">коршун улетел на большую лиственницу около берега речки сел</ta>
            <ta e="T292" id="Seg_5023" s="T280">верхушка стянул повесил силок [петлю] посреди реки</ta>
            <ta e="T297" id="Seg_5024" s="T292">черт едет напевая с двумя девочками</ta>
            <ta e="T306" id="Seg_5025" s="T297">поближе к силку подошел</ta>
            <ta e="T310" id="Seg_5026" s="T306">черт не видел силок</ta>
            <ta e="T315" id="Seg_5027" s="T310">петля [силок] за шею стянул две листвен.</ta>
            <ta e="T320" id="Seg_5028" s="T315">черти смерти [умер]</ta>
            <ta e="T336" id="Seg_5029" s="T320">он поймал в ветке дочерей черта пристал к берегу и черта одну дочку бросил в глину [ил]</ta>
            <ta e="T343" id="Seg_5030" s="T336">вторую дочь тоже [также] бросил и топтал [растоптал]</ta>
            <ta e="T349" id="Seg_5031" s="T343">кто мою сказку слушал тот навсегда скушал</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T17" id="Seg_5032" s="T10">[OSV:] The word "tärɨčʼaktɨ" has been edited into "tättɨčʼaktɨ".</ta>
            <ta e="T131" id="Seg_5033" s="T124">[OSV:] unclear meaning of the verb "korpɨtqo".</ta>
            <ta e="T208" id="Seg_5034" s="T203">‎‎[OSV:] 1) The verbal form has been edited into "siqlaltɨmpɨsɨtɨ"; 2) the word "num-ɨ-nʼaːq-tɨ" has been edited into "num ınʼnʼaːqɨt".</ta>
            <ta e="T233" id="Seg_5035" s="T231">[OSV:] "ıllä üːtɨqo" - "let go down".</ta>
            <ta e="T269" id="Seg_5036" s="T256"> [OSV:] The postpositional form has been edited into "poːroːqɨnɨt".</ta>
            <ta e="T306" id="Seg_5037" s="T297">[OSV:] The word "tärɨčʼaktɨ" has been edited into "təttɨčʼaktɨ".</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T350" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T351" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
