<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KNI_196X_NutcrackerAndBear_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KNI_1967_NutcrackerAndBear_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">71</ud-information>
            <ud-information attribute-name="# HIAT:w">49</ud-information>
            <ud-information attribute-name="# e">49</ud-information>
            <ud-information attribute-name="# HIAT:u">10</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KNI">
            <abbreviation>KNI</abbreviation>
            <sex value="m" />
            <languages-used>
               <language lang="sel" />
            </languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KNI"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T50" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">qaːzar</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">qaːrɨnʼnʼe</ts>
                  <nts id="Seg_8" n="HIAT:ip">:</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_11" n="HIAT:w" s="T3">nildʼiŋ</ts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_14" n="HIAT:w" s="T4">kɨkkaŋ</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_17" n="HIAT:w" s="T5">swäŋgə</ts>
                  <nts id="Seg_18" n="HIAT:ip">.</nts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_21" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T6">qwärgə</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">qwännä</ts>
                  <nts id="Seg_27" n="HIAT:ip">,</nts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_30" n="HIAT:w" s="T8">nɨ</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_33" n="HIAT:w" s="T9">kɨndə</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_36" n="HIAT:w" s="T10">miːtaŋ</ts>
                  <nts id="Seg_37" n="HIAT:ip">.</nts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_40" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_42" n="HIAT:w" s="T11">qai</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_45" n="HIAT:w" s="T12">swäŋgəna</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_48" n="HIAT:w" s="T13">tʼäŋu</ts>
                  <nts id="Seg_49" n="HIAT:ip">.</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_52" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_54" n="HIAT:w" s="T14">qwärgə</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_57" n="HIAT:w" s="T15">aqol</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_60" n="HIAT:w" s="T16">qwännä</ts>
                  <nts id="Seg_61" n="HIAT:ip">.</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_64" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_66" n="HIAT:w" s="T17">tɨːsʼkam</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_69" n="HIAT:w" s="T18">qoːɣət</ts>
                  <nts id="Seg_70" n="HIAT:ip">.</nts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_73" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_75" n="HIAT:w" s="T19">tɨːsʼka</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_78" n="HIAT:w" s="T20">tʼäraŋ</ts>
                  <nts id="Seg_79" n="HIAT:ip">:</nts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_82" n="HIAT:w" s="T21">nildʼi</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_85" n="HIAT:w" s="T22">čwäčon</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">tʼüːwwə</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_90" n="HIAT:ip">(</nts>
                  <ts e="T25" id="Seg_92" n="HIAT:w" s="T24">tʼüːwla</ts>
                  <nts id="Seg_93" n="HIAT:ip">)</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_96" n="HIAT:w" s="T25">qoːnaŋ</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_99" n="HIAT:w" s="T26">eŋ</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_101" n="HIAT:ip">(</nts>
                  <ts e="T28" id="Seg_103" n="HIAT:w" s="T27">eːɣan</ts>
                  <nts id="Seg_104" n="HIAT:ip">)</nts>
                  <nts id="Seg_105" n="HIAT:ip">.</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_108" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_110" n="HIAT:w" s="T28">qwärgə</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_113" n="HIAT:w" s="T29">qwannä</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_116" n="HIAT:w" s="T30">nitʼä</ts>
                  <nts id="Seg_117" n="HIAT:ip">.</nts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_120" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_122" n="HIAT:w" s="T31">qwärgə</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_125" n="HIAT:w" s="T32">miːtaŋ</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_128" n="HIAT:w" s="T33">i</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_131" n="HIAT:w" s="T34">tʼüːwam</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_134" n="HIAT:w" s="T35">qoɣɨt</ts>
                  <nts id="Seg_135" n="HIAT:ip">.</nts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_138" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_140" n="HIAT:w" s="T36">tɨːsʼkanɨ</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_143" n="HIAT:w" s="T37">tʼäraŋ</ts>
                  <nts id="Seg_144" n="HIAT:ip">:</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_147" n="HIAT:w" s="T38">a</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_150" n="HIAT:w" s="T39">što</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_153" n="HIAT:w" s="T40">wätʼöl</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_156" n="HIAT:w" s="T41">äːqwi</ts>
                  <nts id="Seg_157" n="HIAT:ip">.</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_160" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_162" n="HIAT:w" s="T42">a</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_165" n="HIAT:w" s="T43">qaːzarɨnnan</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_168" n="HIAT:w" s="T44">okkɨrɨglʼe</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_171" n="HIAT:w" s="T45">säːqə</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_174" n="HIAT:w" s="T46">watʼö</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_176" n="HIAT:ip">(</nts>
                  <ts e="T48" id="Seg_178" n="HIAT:w" s="T47">wädʼät</ts>
                  <nts id="Seg_179" n="HIAT:ip">)</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_182" n="HIAT:w" s="T48">äːqwi</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_184" n="HIAT:ip">(</nts>
                  <ts e="T50" id="Seg_186" n="HIAT:w" s="T49">äːquwi</ts>
                  <nts id="Seg_187" n="HIAT:ip">)</nts>
                  <nts id="Seg_188" n="HIAT:ip">.</nts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T50" id="Seg_190" n="sc" s="T1">
               <ts e="T2" id="Seg_192" n="e" s="T1">qaːzar </ts>
               <ts e="T3" id="Seg_194" n="e" s="T2">qaːrɨnʼnʼe: </ts>
               <ts e="T4" id="Seg_196" n="e" s="T3">nildʼiŋ </ts>
               <ts e="T5" id="Seg_198" n="e" s="T4">kɨkkaŋ </ts>
               <ts e="T6" id="Seg_200" n="e" s="T5">swäŋgə. </ts>
               <ts e="T7" id="Seg_202" n="e" s="T6">qwärgə </ts>
               <ts e="T8" id="Seg_204" n="e" s="T7">qwännä, </ts>
               <ts e="T9" id="Seg_206" n="e" s="T8">nɨ </ts>
               <ts e="T10" id="Seg_208" n="e" s="T9">kɨndə </ts>
               <ts e="T11" id="Seg_210" n="e" s="T10">miːtaŋ. </ts>
               <ts e="T12" id="Seg_212" n="e" s="T11">qai </ts>
               <ts e="T13" id="Seg_214" n="e" s="T12">swäŋgəna </ts>
               <ts e="T14" id="Seg_216" n="e" s="T13">tʼäŋu. </ts>
               <ts e="T15" id="Seg_218" n="e" s="T14">qwärgə </ts>
               <ts e="T16" id="Seg_220" n="e" s="T15">aqol </ts>
               <ts e="T17" id="Seg_222" n="e" s="T16">qwännä. </ts>
               <ts e="T18" id="Seg_224" n="e" s="T17">tɨːsʼkam </ts>
               <ts e="T19" id="Seg_226" n="e" s="T18">qoːɣət. </ts>
               <ts e="T20" id="Seg_228" n="e" s="T19">tɨːsʼka </ts>
               <ts e="T21" id="Seg_230" n="e" s="T20">tʼäraŋ: </ts>
               <ts e="T22" id="Seg_232" n="e" s="T21">nildʼi </ts>
               <ts e="T23" id="Seg_234" n="e" s="T22">čwäčon </ts>
               <ts e="T24" id="Seg_236" n="e" s="T23">tʼüːwwə </ts>
               <ts e="T25" id="Seg_238" n="e" s="T24">(tʼüːwla) </ts>
               <ts e="T26" id="Seg_240" n="e" s="T25">qoːnaŋ </ts>
               <ts e="T27" id="Seg_242" n="e" s="T26">eŋ </ts>
               <ts e="T28" id="Seg_244" n="e" s="T27">(eːɣan). </ts>
               <ts e="T29" id="Seg_246" n="e" s="T28">qwärgə </ts>
               <ts e="T30" id="Seg_248" n="e" s="T29">qwannä </ts>
               <ts e="T31" id="Seg_250" n="e" s="T30">nitʼä. </ts>
               <ts e="T32" id="Seg_252" n="e" s="T31">qwärgə </ts>
               <ts e="T33" id="Seg_254" n="e" s="T32">miːtaŋ </ts>
               <ts e="T34" id="Seg_256" n="e" s="T33">i </ts>
               <ts e="T35" id="Seg_258" n="e" s="T34">tʼüːwam </ts>
               <ts e="T36" id="Seg_260" n="e" s="T35">qoɣɨt. </ts>
               <ts e="T37" id="Seg_262" n="e" s="T36">tɨːsʼkanɨ </ts>
               <ts e="T38" id="Seg_264" n="e" s="T37">tʼäraŋ: </ts>
               <ts e="T39" id="Seg_266" n="e" s="T38">a </ts>
               <ts e="T40" id="Seg_268" n="e" s="T39">što </ts>
               <ts e="T41" id="Seg_270" n="e" s="T40">wätʼöl </ts>
               <ts e="T42" id="Seg_272" n="e" s="T41">äːqwi. </ts>
               <ts e="T43" id="Seg_274" n="e" s="T42">a </ts>
               <ts e="T44" id="Seg_276" n="e" s="T43">qaːzarɨnnan </ts>
               <ts e="T45" id="Seg_278" n="e" s="T44">okkɨrɨglʼe </ts>
               <ts e="T46" id="Seg_280" n="e" s="T45">säːqə </ts>
               <ts e="T47" id="Seg_282" n="e" s="T46">watʼö </ts>
               <ts e="T48" id="Seg_284" n="e" s="T47">(wädʼät) </ts>
               <ts e="T49" id="Seg_286" n="e" s="T48">äːqwi </ts>
               <ts e="T50" id="Seg_288" n="e" s="T49">(äːquwi). </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_289" s="T1">KNI_1967_NutcrackerAndBear_flk.001 (001.001)</ta>
            <ta e="T11" id="Seg_290" s="T6">KNI_1967_NutcrackerAndBear_flk.002 (001.002)</ta>
            <ta e="T14" id="Seg_291" s="T11">KNI_1967_NutcrackerAndBear_flk.003 (001.003)</ta>
            <ta e="T17" id="Seg_292" s="T14">KNI_1967_NutcrackerAndBear_flk.004 (001.004)</ta>
            <ta e="T19" id="Seg_293" s="T17">KNI_1967_NutcrackerAndBear_flk.005 (001.005)</ta>
            <ta e="T28" id="Seg_294" s="T19">KNI_1967_NutcrackerAndBear_flk.006 (001.006)</ta>
            <ta e="T31" id="Seg_295" s="T28">KNI_1967_NutcrackerAndBear_flk.007 (001.007)</ta>
            <ta e="T36" id="Seg_296" s="T31">KNI_1967_NutcrackerAndBear_flk.008 (001.008)</ta>
            <ta e="T42" id="Seg_297" s="T36">KNI_1967_NutcrackerAndBear_flk.009 (001.009)</ta>
            <ta e="T50" id="Seg_298" s="T42">KNI_1967_NutcrackerAndBear_flk.010 (001.010)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_299" s="T1">′kа̄зар ′kа̄рынʼнʼе: ниl′дʼиң ′кыккаң ′свӓңг̂ъ.</ta>
            <ta e="T11" id="Seg_300" s="T6">′kwӓргъ kwӓннӓ, ны кындъ ′мӣтаң.</ta>
            <ta e="T14" id="Seg_301" s="T11">′kаи ′свӓңгъна ′тʼӓңу.</ta>
            <ta e="T17" id="Seg_302" s="T14">′kwӓргъ а′k(к)ол(l) kwӓ′ннӓ.</ta>
            <ta e="T19" id="Seg_303" s="T17">′ты̄сʼкам ′kо̄ɣъ(ы)т.</ta>
            <ta e="T28" id="Seg_304" s="T19">′ты̄сʼка ′тʼӓраң: ниl′дʼи ′тшвӓтшон ′тʼӱ̄ввъ (тʼӱ̄вла) ′kо̄наң ең (′е̄ɣан).</ta>
            <ta e="T31" id="Seg_305" s="T28">′kwӓргъ kwа′ннӓ ни′тʼӓ.</ta>
            <ta e="T36" id="Seg_306" s="T31">kwӓргъ мӣ′таң и тʼӱ̄вам ′kоɣыт.</ta>
            <ta e="T42" id="Seg_307" s="T36">′ты̄сʼканы тʼӓ′раң: а што вӓ′тʼӧл ′ӓ̄kви.</ta>
            <ta e="T50" id="Seg_308" s="T42">а ′kа̄зарыннан ‵оккыры′глʼе ′сӓ̄kъ wа′тʼӧ (вӓдʼӓт) ′ӓ̄kwи (′ӓ̄kуви).</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_309" s="T1">qaːzar qaːrɨnʼnʼe: nildʼiŋ kɨkkaŋ sväŋĝə.</ta>
            <ta e="T11" id="Seg_310" s="T6">qwärgə qwännä, nɨ kɨndə miːtaŋ.</ta>
            <ta e="T14" id="Seg_311" s="T11">qai sväŋgəna tʼäŋu.</ta>
            <ta e="T17" id="Seg_312" s="T14">qwärgə aq(k)ol(l) qwännä.</ta>
            <ta e="T19" id="Seg_313" s="T17">tɨːsʼkam qoːɣə(ɨ)t.</ta>
            <ta e="T28" id="Seg_314" s="T19">tɨːsʼka tʼäraŋ: nildʼi tšvätšon tʼüːvvə (tʼüːvla) qoːnaŋ eŋ (eːɣan).</ta>
            <ta e="T31" id="Seg_315" s="T28">qwärgə qwannä nitʼä.</ta>
            <ta e="T36" id="Seg_316" s="T31">qwärgə miːtaŋ i tʼüːvam qoɣɨt.</ta>
            <ta e="T42" id="Seg_317" s="T36">tɨːsʼkanɨ tʼäraŋ: a što vätʼöl äːqvi.</ta>
            <ta e="T50" id="Seg_318" s="T42">a qaːzarɨnnan okkɨrɨglʼe säːqə watʼö (vädʼät) äːqwi (äːquvi).</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_319" s="T1">qaːzar qaːrɨnʼnʼe: nildʼiŋ kɨkkaŋ swäŋgə. </ta>
            <ta e="T11" id="Seg_320" s="T6">qwärgə qwännä, nɨ kɨndə miːtaŋ. </ta>
            <ta e="T14" id="Seg_321" s="T11">qai swäŋgəna tʼäŋu. </ta>
            <ta e="T17" id="Seg_322" s="T14">qwärgə aqol qwännä. </ta>
            <ta e="T19" id="Seg_323" s="T17">tɨːsʼkam qoːɣət. </ta>
            <ta e="T28" id="Seg_324" s="T19">tɨːsʼka tʼäraŋ: nildʼi čwäčon tʼüːwwə (tʼüːwla) qoːnaŋ eŋ (eːɣan). </ta>
            <ta e="T31" id="Seg_325" s="T28">qwärgə qwannä nitʼä. </ta>
            <ta e="T36" id="Seg_326" s="T31">qwärgə miːtaŋ i tʼüːwam qoɣɨt. </ta>
            <ta e="T42" id="Seg_327" s="T36">tɨːsʼkanɨ tʼäraŋ: a što wätʼöl äːqwi. </ta>
            <ta e="T50" id="Seg_328" s="T42">a qaːzarɨnnan okkɨrɨglʼe säːqə watʼö (wädʼät) äːqwi (äːquwi). </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_329" s="T1">qaːzar</ta>
            <ta e="T3" id="Seg_330" s="T2">qaːrɨ-nʼ-nʼe</ta>
            <ta e="T4" id="Seg_331" s="T3">nildʼi-ŋ</ta>
            <ta e="T5" id="Seg_332" s="T4">kɨkka-n</ta>
            <ta e="T6" id="Seg_333" s="T5">swäŋgə</ta>
            <ta e="T7" id="Seg_334" s="T6">qwärgə</ta>
            <ta e="T8" id="Seg_335" s="T7">qwän-nä</ta>
            <ta e="T9" id="Seg_336" s="T8">nɨ</ta>
            <ta e="T10" id="Seg_337" s="T9">kɨ-ndə</ta>
            <ta e="T11" id="Seg_338" s="T10">miːta-n</ta>
            <ta e="T12" id="Seg_339" s="T11">qai</ta>
            <ta e="T13" id="Seg_340" s="T12">swäŋgə-na</ta>
            <ta e="T14" id="Seg_341" s="T13">tʼäŋu</ta>
            <ta e="T15" id="Seg_342" s="T14">qwärgə</ta>
            <ta e="T16" id="Seg_343" s="T15">aqol</ta>
            <ta e="T17" id="Seg_344" s="T16">qwän-nä</ta>
            <ta e="T18" id="Seg_345" s="T17">tɨːsʼka-m</ta>
            <ta e="T19" id="Seg_346" s="T18">qoː-ɣə-t</ta>
            <ta e="T20" id="Seg_347" s="T19">tɨːsʼka</ta>
            <ta e="T21" id="Seg_348" s="T20">tʼära-n</ta>
            <ta e="T22" id="Seg_349" s="T21">nildʼi</ta>
            <ta e="T23" id="Seg_350" s="T22">čwäčo-n</ta>
            <ta e="T24" id="Seg_351" s="T23">tʼüːwwə</ta>
            <ta e="T25" id="Seg_352" s="T24">tʼüːw-la</ta>
            <ta e="T26" id="Seg_353" s="T25">qoːnɨŋ</ta>
            <ta e="T27" id="Seg_354" s="T26">e-n</ta>
            <ta e="T28" id="Seg_355" s="T27">eː-ɣa-ŋ</ta>
            <ta e="T29" id="Seg_356" s="T28">qwärgə</ta>
            <ta e="T30" id="Seg_357" s="T29">qwan-nä</ta>
            <ta e="T31" id="Seg_358" s="T30">nitʼä</ta>
            <ta e="T32" id="Seg_359" s="T31">qwärgə</ta>
            <ta e="T33" id="Seg_360" s="T32">miːta-n</ta>
            <ta e="T34" id="Seg_361" s="T33">i</ta>
            <ta e="T35" id="Seg_362" s="T34">tʼüːwa-m</ta>
            <ta e="T36" id="Seg_363" s="T35">qo-ɣɨ-t</ta>
            <ta e="T37" id="Seg_364" s="T36">tɨːsʼka-nɨ</ta>
            <ta e="T38" id="Seg_365" s="T37">tʼära-n</ta>
            <ta e="T39" id="Seg_366" s="T38">a</ta>
            <ta e="T40" id="Seg_367" s="T39">što</ta>
            <ta e="T41" id="Seg_368" s="T40">wätʼö-l</ta>
            <ta e="T42" id="Seg_369" s="T41">äː-q-wi</ta>
            <ta e="T43" id="Seg_370" s="T42">a</ta>
            <ta e="T44" id="Seg_371" s="T43">qaːzar-ɨ-n-nan</ta>
            <ta e="T45" id="Seg_372" s="T44">okkɨr-ɨ-g-lʼe</ta>
            <ta e="T46" id="Seg_373" s="T45">säːqə</ta>
            <ta e="T47" id="Seg_374" s="T46">watʼö</ta>
            <ta e="T48" id="Seg_375" s="T47">wädʼä-t</ta>
            <ta e="T49" id="Seg_376" s="T48">äː-q-wi</ta>
            <ta e="T50" id="Seg_377" s="T49">äː-qu-wi</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_378" s="T1">qaːzar</ta>
            <ta e="T3" id="Seg_379" s="T2">qarrə-š-nɨ</ta>
            <ta e="T4" id="Seg_380" s="T3">nɨlʼǯi-n</ta>
            <ta e="T5" id="Seg_381" s="T4">kɨkkɨ-n</ta>
            <ta e="T6" id="Seg_382" s="T5">swäŋɨ</ta>
            <ta e="T7" id="Seg_383" s="T6">qwärqa</ta>
            <ta e="T8" id="Seg_384" s="T7">qwən-ŋɨ</ta>
            <ta e="T9" id="Seg_385" s="T8">na</ta>
            <ta e="T10" id="Seg_386" s="T9">kɨ-ndɨ</ta>
            <ta e="T11" id="Seg_387" s="T10">miːta-n</ta>
            <ta e="T12" id="Seg_388" s="T11">qaj</ta>
            <ta e="T13" id="Seg_389" s="T12">swäŋɨ-naj</ta>
            <ta e="T14" id="Seg_390" s="T13">tʼäŋu</ta>
            <ta e="T15" id="Seg_391" s="T14">qwärqa</ta>
            <ta e="T16" id="Seg_392" s="T15">aqqol</ta>
            <ta e="T17" id="Seg_393" s="T16">qwən-ŋɨ</ta>
            <ta e="T18" id="Seg_394" s="T17">tɨːsʼka-m</ta>
            <ta e="T19" id="Seg_395" s="T18">qo-ŋɨ-tɨ</ta>
            <ta e="T20" id="Seg_396" s="T19">tɨːsʼka</ta>
            <ta e="T21" id="Seg_397" s="T20">tʼarɨ-n</ta>
            <ta e="T22" id="Seg_398" s="T21">nɨlʼǯi</ta>
            <ta e="T23" id="Seg_399" s="T22">čwäče-n</ta>
            <ta e="T24" id="Seg_400" s="T23">tʼüːwə</ta>
            <ta e="T25" id="Seg_401" s="T24">tʼüːwə-la</ta>
            <ta e="T26" id="Seg_402" s="T25">qoːnɨŋ</ta>
            <ta e="T27" id="Seg_403" s="T26">eː-n</ta>
            <ta e="T28" id="Seg_404" s="T27">eː-ŋɨ-n</ta>
            <ta e="T29" id="Seg_405" s="T28">qwärqa</ta>
            <ta e="T30" id="Seg_406" s="T29">qwən-ŋɨ</ta>
            <ta e="T31" id="Seg_407" s="T30">natʼtʼa</ta>
            <ta e="T32" id="Seg_408" s="T31">qwärqa</ta>
            <ta e="T33" id="Seg_409" s="T32">miːta-n</ta>
            <ta e="T34" id="Seg_410" s="T33">i</ta>
            <ta e="T35" id="Seg_411" s="T34">tʼüːwə-m</ta>
            <ta e="T36" id="Seg_412" s="T35">qo-ŋɨ-tɨ</ta>
            <ta e="T37" id="Seg_413" s="T36">tɨːsʼka-naj</ta>
            <ta e="T38" id="Seg_414" s="T37">tʼarɨ-n</ta>
            <ta e="T39" id="Seg_415" s="T38">a</ta>
            <ta e="T40" id="Seg_416" s="T39">što</ta>
            <ta e="T41" id="Seg_417" s="T40">wätʼtʼi-lʼ</ta>
            <ta e="T42" id="Seg_418" s="T41">eː-qi-wi</ta>
            <ta e="T43" id="Seg_419" s="T42">a</ta>
            <ta e="T44" id="Seg_420" s="T43">qaːzar-ɨ-n-nan</ta>
            <ta e="T45" id="Seg_421" s="T44">okkɨr-ɨ-ku-le</ta>
            <ta e="T46" id="Seg_422" s="T45">säɣə</ta>
            <ta e="T47" id="Seg_423" s="T46">wätʼtʼi</ta>
            <ta e="T48" id="Seg_424" s="T47">wätʼtʼi-tɨ</ta>
            <ta e="T49" id="Seg_425" s="T48">eː-qi-ŋɨ</ta>
            <ta e="T50" id="Seg_426" s="T49">eː-ku-ŋɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_427" s="T1">nutcracker.[NOM]</ta>
            <ta e="T3" id="Seg_428" s="T2">cry-US-ALL.[3SG.S]</ta>
            <ta e="T4" id="Seg_429" s="T3">such-ADV.LOC</ta>
            <ta e="T5" id="Seg_430" s="T4">want-1SG.S</ta>
            <ta e="T6" id="Seg_431" s="T5">cone.[NOM]</ta>
            <ta e="T7" id="Seg_432" s="T6">bear.[NOM]</ta>
            <ta e="T8" id="Seg_433" s="T7">go.away-CO.[3SG.S]</ta>
            <ta e="T9" id="Seg_434" s="T8">this</ta>
            <ta e="T10" id="Seg_435" s="T9">river-ILL</ta>
            <ta e="T11" id="Seg_436" s="T10">achieve-3SG.S</ta>
            <ta e="T12" id="Seg_437" s="T11">what</ta>
            <ta e="T13" id="Seg_438" s="T12">cone-EMPH</ta>
            <ta e="T14" id="Seg_439" s="T13">NEG.EX.[3SG.S]</ta>
            <ta e="T15" id="Seg_440" s="T14">bear.[NOM]</ta>
            <ta e="T16" id="Seg_441" s="T15">again</ta>
            <ta e="T17" id="Seg_442" s="T16">go.away-CO.[3SG.S]</ta>
            <ta e="T18" id="Seg_443" s="T17">sparrow-ACC</ta>
            <ta e="T19" id="Seg_444" s="T18">find-CO-3SG.O</ta>
            <ta e="T20" id="Seg_445" s="T19">sparrow.[NOM]</ta>
            <ta e="T21" id="Seg_446" s="T20">say-3SG.S</ta>
            <ta e="T22" id="Seg_447" s="T21">such</ta>
            <ta e="T23" id="Seg_448" s="T22">place-ADV.LOC</ta>
            <ta e="T24" id="Seg_449" s="T23">larch.[NOM]</ta>
            <ta e="T25" id="Seg_450" s="T24">larch-PL.[NOM]</ta>
            <ta e="T26" id="Seg_451" s="T25">many</ta>
            <ta e="T27" id="Seg_452" s="T26">be-3SG.S</ta>
            <ta e="T28" id="Seg_453" s="T27">be-CO-3SG.S</ta>
            <ta e="T29" id="Seg_454" s="T28">bear.[NOM]</ta>
            <ta e="T30" id="Seg_455" s="T29">go.away-CO.[3SG.S]</ta>
            <ta e="T31" id="Seg_456" s="T30">there</ta>
            <ta e="T32" id="Seg_457" s="T31">bear.[NOM]</ta>
            <ta e="T33" id="Seg_458" s="T32">achieve-3SG.S</ta>
            <ta e="T34" id="Seg_459" s="T33">and</ta>
            <ta e="T35" id="Seg_460" s="T34">larch-ACC</ta>
            <ta e="T36" id="Seg_461" s="T35">find-CO-3SG.O</ta>
            <ta e="T37" id="Seg_462" s="T36">sparrow-EMPH</ta>
            <ta e="T38" id="Seg_463" s="T37">say-3SG.S</ta>
            <ta e="T39" id="Seg_464" s="T38">but</ta>
            <ta e="T40" id="Seg_465" s="T39">what</ta>
            <ta e="T41" id="Seg_466" s="T40">meat-ADJZ</ta>
            <ta e="T42" id="Seg_467" s="T41">be-HAB-CO.[3SG.S]</ta>
            <ta e="T43" id="Seg_468" s="T42">but</ta>
            <ta e="T44" id="Seg_469" s="T43">nutcracker-EP-GEN-ADES</ta>
            <ta e="T45" id="Seg_470" s="T44">one-EP-HAB-CVB</ta>
            <ta e="T46" id="Seg_471" s="T45">black</ta>
            <ta e="T47" id="Seg_472" s="T46">meat.[NOM]</ta>
            <ta e="T48" id="Seg_473" s="T47">meat.[NOM]-3SG</ta>
            <ta e="T49" id="Seg_474" s="T48">be-3DU.S-CO.[3SG.S]</ta>
            <ta e="T50" id="Seg_475" s="T49">be-HAB-CO.[3SG.S]]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_476" s="T1">кедровка.[NOM]</ta>
            <ta e="T3" id="Seg_477" s="T2">кричать-US-ALL.[3SG.S]</ta>
            <ta e="T4" id="Seg_478" s="T3">такой-ADV.LOC</ta>
            <ta e="T5" id="Seg_479" s="T4">хотеть-1SG.S</ta>
            <ta e="T6" id="Seg_480" s="T5">шишка.[NOM]</ta>
            <ta e="T7" id="Seg_481" s="T6">медведь.[NOM]</ta>
            <ta e="T8" id="Seg_482" s="T7">уйти-CO.[3SG.S]</ta>
            <ta e="T9" id="Seg_483" s="T8">этот</ta>
            <ta e="T10" id="Seg_484" s="T9">река-ILL</ta>
            <ta e="T11" id="Seg_485" s="T10">достичь-3SG.S</ta>
            <ta e="T12" id="Seg_486" s="T11">что</ta>
            <ta e="T13" id="Seg_487" s="T12">шишка-EMPH</ta>
            <ta e="T14" id="Seg_488" s="T13">NEG.EX.[3SG.S]</ta>
            <ta e="T15" id="Seg_489" s="T14">медведь.[NOM]</ta>
            <ta e="T16" id="Seg_490" s="T15">опять</ta>
            <ta e="T17" id="Seg_491" s="T16">уйти-CO.[3SG.S]</ta>
            <ta e="T18" id="Seg_492" s="T17">воробей-ACC</ta>
            <ta e="T19" id="Seg_493" s="T18">найти-CO-3SG.O</ta>
            <ta e="T20" id="Seg_494" s="T19">воробей.[NOM]</ta>
            <ta e="T21" id="Seg_495" s="T20">сказать-3SG.S</ta>
            <ta e="T22" id="Seg_496" s="T21">такой</ta>
            <ta e="T23" id="Seg_497" s="T22">место-ADV.LOC</ta>
            <ta e="T24" id="Seg_498" s="T23">лиственница.[NOM]</ta>
            <ta e="T25" id="Seg_499" s="T24">лиственница-PL.[NOM]</ta>
            <ta e="T26" id="Seg_500" s="T25">много</ta>
            <ta e="T27" id="Seg_501" s="T26">быть-3SG.S</ta>
            <ta e="T28" id="Seg_502" s="T27">быть-CO-3SG.S</ta>
            <ta e="T29" id="Seg_503" s="T28">медведь.[NOM]</ta>
            <ta e="T30" id="Seg_504" s="T29">уйти-CO.[3SG.S]</ta>
            <ta e="T31" id="Seg_505" s="T30">туда</ta>
            <ta e="T32" id="Seg_506" s="T31">медведь.[NOM]</ta>
            <ta e="T33" id="Seg_507" s="T32">достичь-3SG.S</ta>
            <ta e="T34" id="Seg_508" s="T33">и</ta>
            <ta e="T35" id="Seg_509" s="T34">лиственница-ACC</ta>
            <ta e="T36" id="Seg_510" s="T35">найти-CO-3SG.O</ta>
            <ta e="T37" id="Seg_511" s="T36">воробей-EMPH</ta>
            <ta e="T38" id="Seg_512" s="T37">сказать-3SG.S</ta>
            <ta e="T39" id="Seg_513" s="T38">а</ta>
            <ta e="T40" id="Seg_514" s="T39">что</ta>
            <ta e="T41" id="Seg_515" s="T40">мясо-ADJZ</ta>
            <ta e="T42" id="Seg_516" s="T41">быть-HAB-CO.[3SG.S]</ta>
            <ta e="T43" id="Seg_517" s="T42">а</ta>
            <ta e="T44" id="Seg_518" s="T43">кедровка-EP-GEN-ADES</ta>
            <ta e="T45" id="Seg_519" s="T44">один-EP-HAB-CVB</ta>
            <ta e="T46" id="Seg_520" s="T45">чёрный</ta>
            <ta e="T47" id="Seg_521" s="T46">мясо.[NOM]</ta>
            <ta e="T48" id="Seg_522" s="T47">мясо.[NOM]-3SG</ta>
            <ta e="T49" id="Seg_523" s="T48">быть-3DU.S-CO.[3SG.S]</ta>
            <ta e="T50" id="Seg_524" s="T49">быть-HAB-CO.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_525" s="T1">n.[n:case]</ta>
            <ta e="T3" id="Seg_526" s="T2">v-v&gt;v-n:case.[v:pn]</ta>
            <ta e="T4" id="Seg_527" s="T3">dem-adv:case</ta>
            <ta e="T5" id="Seg_528" s="T4">v-v:pn</ta>
            <ta e="T6" id="Seg_529" s="T5">n.[n:case]</ta>
            <ta e="T7" id="Seg_530" s="T6">n.[n:case]</ta>
            <ta e="T8" id="Seg_531" s="T7">v-v:ins.[v:pn]</ta>
            <ta e="T9" id="Seg_532" s="T8">dem</ta>
            <ta e="T10" id="Seg_533" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_534" s="T10">v-v:pn</ta>
            <ta e="T12" id="Seg_535" s="T11">interrog</ta>
            <ta e="T13" id="Seg_536" s="T12">n-clit</ta>
            <ta e="T14" id="Seg_537" s="T13">v.[v:pn]</ta>
            <ta e="T15" id="Seg_538" s="T14">n.[n:case]</ta>
            <ta e="T16" id="Seg_539" s="T15">adv</ta>
            <ta e="T17" id="Seg_540" s="T16">v-v:ins.[v:pn]</ta>
            <ta e="T18" id="Seg_541" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_542" s="T18">v-v:ins-v:pn</ta>
            <ta e="T20" id="Seg_543" s="T19">n.[n:case]</ta>
            <ta e="T21" id="Seg_544" s="T20">v-v:pn</ta>
            <ta e="T22" id="Seg_545" s="T21">dem</ta>
            <ta e="T23" id="Seg_546" s="T22">n-adv:case</ta>
            <ta e="T24" id="Seg_547" s="T23">n.[n:case]</ta>
            <ta e="T25" id="Seg_548" s="T24">n-n:num.[n:case]</ta>
            <ta e="T26" id="Seg_549" s="T25">quant</ta>
            <ta e="T27" id="Seg_550" s="T26">v-v:pn</ta>
            <ta e="T28" id="Seg_551" s="T27">v-v:ins-v:pn</ta>
            <ta e="T29" id="Seg_552" s="T28">n.[n:case]</ta>
            <ta e="T30" id="Seg_553" s="T29">v-v:ins.[v:pn]</ta>
            <ta e="T31" id="Seg_554" s="T30">adv</ta>
            <ta e="T32" id="Seg_555" s="T31">n.[n:case]</ta>
            <ta e="T33" id="Seg_556" s="T32">v-v:pn</ta>
            <ta e="T34" id="Seg_557" s="T33">conj</ta>
            <ta e="T35" id="Seg_558" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_559" s="T35">v-v:ins-v:pn</ta>
            <ta e="T37" id="Seg_560" s="T36">n-clit</ta>
            <ta e="T38" id="Seg_561" s="T37">v-v:pn</ta>
            <ta e="T39" id="Seg_562" s="T38">conj</ta>
            <ta e="T40" id="Seg_563" s="T39">conj</ta>
            <ta e="T41" id="Seg_564" s="T40">n-n&gt;adj</ta>
            <ta e="T42" id="Seg_565" s="T41">v-v&gt;v-v:ins.[v:pn]</ta>
            <ta e="T43" id="Seg_566" s="T42">conj</ta>
            <ta e="T44" id="Seg_567" s="T43">n-n:ins-n:case-n:case</ta>
            <ta e="T45" id="Seg_568" s="T44">num-n:ins-v&gt;v-v&gt;adv</ta>
            <ta e="T46" id="Seg_569" s="T45">adj</ta>
            <ta e="T47" id="Seg_570" s="T46">n.[n:case]</ta>
            <ta e="T48" id="Seg_571" s="T47">n.[n:case]-n:poss</ta>
            <ta e="T49" id="Seg_572" s="T48">v-v:pn-v:ins.[v:pn]</ta>
            <ta e="T50" id="Seg_573" s="T49">v-v&gt;v-v:ins.[v:pn]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_574" s="T1">n</ta>
            <ta e="T3" id="Seg_575" s="T2">v</ta>
            <ta e="T4" id="Seg_576" s="T3">dem</ta>
            <ta e="T5" id="Seg_577" s="T4">v</ta>
            <ta e="T6" id="Seg_578" s="T5">n</ta>
            <ta e="T7" id="Seg_579" s="T6">n</ta>
            <ta e="T8" id="Seg_580" s="T7">v</ta>
            <ta e="T9" id="Seg_581" s="T8">dem</ta>
            <ta e="T10" id="Seg_582" s="T9">n</ta>
            <ta e="T11" id="Seg_583" s="T10">v</ta>
            <ta e="T12" id="Seg_584" s="T11">interrog</ta>
            <ta e="T13" id="Seg_585" s="T12">n</ta>
            <ta e="T14" id="Seg_586" s="T13">v</ta>
            <ta e="T15" id="Seg_587" s="T14">n</ta>
            <ta e="T16" id="Seg_588" s="T15">adv</ta>
            <ta e="T17" id="Seg_589" s="T16">v</ta>
            <ta e="T18" id="Seg_590" s="T17">n</ta>
            <ta e="T19" id="Seg_591" s="T18">v</ta>
            <ta e="T20" id="Seg_592" s="T19">n</ta>
            <ta e="T21" id="Seg_593" s="T20">v</ta>
            <ta e="T22" id="Seg_594" s="T21">dem</ta>
            <ta e="T23" id="Seg_595" s="T22">n</ta>
            <ta e="T24" id="Seg_596" s="T23">n</ta>
            <ta e="T25" id="Seg_597" s="T24">n</ta>
            <ta e="T26" id="Seg_598" s="T25">quant</ta>
            <ta e="T27" id="Seg_599" s="T26">v</ta>
            <ta e="T28" id="Seg_600" s="T27">v</ta>
            <ta e="T29" id="Seg_601" s="T28">n</ta>
            <ta e="T30" id="Seg_602" s="T29">v</ta>
            <ta e="T31" id="Seg_603" s="T30">adv</ta>
            <ta e="T32" id="Seg_604" s="T31">n</ta>
            <ta e="T33" id="Seg_605" s="T32">v</ta>
            <ta e="T34" id="Seg_606" s="T33">conj</ta>
            <ta e="T35" id="Seg_607" s="T34">n</ta>
            <ta e="T36" id="Seg_608" s="T35">v</ta>
            <ta e="T37" id="Seg_609" s="T36">n</ta>
            <ta e="T38" id="Seg_610" s="T37">v</ta>
            <ta e="T39" id="Seg_611" s="T38">conj</ta>
            <ta e="T40" id="Seg_612" s="T39">conj</ta>
            <ta e="T41" id="Seg_613" s="T40">n</ta>
            <ta e="T42" id="Seg_614" s="T41">v</ta>
            <ta e="T43" id="Seg_615" s="T42">conj</ta>
            <ta e="T44" id="Seg_616" s="T43">n</ta>
            <ta e="T45" id="Seg_617" s="T44">adv</ta>
            <ta e="T46" id="Seg_618" s="T45">adj</ta>
            <ta e="T47" id="Seg_619" s="T46">n</ta>
            <ta e="T48" id="Seg_620" s="T47">n</ta>
            <ta e="T49" id="Seg_621" s="T48">v</ta>
            <ta e="T50" id="Seg_622" s="T49">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_623" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_624" s="T2">v:pred</ta>
            <ta e="T5" id="Seg_625" s="T4">0.1.h:S v:pred</ta>
            <ta e="T6" id="Seg_626" s="T5">np:O</ta>
            <ta e="T7" id="Seg_627" s="T6">np.h:S</ta>
            <ta e="T8" id="Seg_628" s="T7">v:pred</ta>
            <ta e="T11" id="Seg_629" s="T10">0.3.h:S v:pred</ta>
            <ta e="T13" id="Seg_630" s="T12">np:S</ta>
            <ta e="T14" id="Seg_631" s="T13">v:pred</ta>
            <ta e="T15" id="Seg_632" s="T14">np.h:S</ta>
            <ta e="T17" id="Seg_633" s="T16">v:pred</ta>
            <ta e="T18" id="Seg_634" s="T17">np.h:O</ta>
            <ta e="T19" id="Seg_635" s="T18">0.3.h:S v:pred</ta>
            <ta e="T20" id="Seg_636" s="T19">np.h:S</ta>
            <ta e="T21" id="Seg_637" s="T20">v:pred</ta>
            <ta e="T24" id="Seg_638" s="T23">np:S</ta>
            <ta e="T27" id="Seg_639" s="T26">v:pred</ta>
            <ta e="T29" id="Seg_640" s="T28">np.h:S</ta>
            <ta e="T30" id="Seg_641" s="T29">v:pred</ta>
            <ta e="T32" id="Seg_642" s="T31">np.h:S</ta>
            <ta e="T33" id="Seg_643" s="T32">v:pred</ta>
            <ta e="T35" id="Seg_644" s="T34">np:O</ta>
            <ta e="T36" id="Seg_645" s="T35">0.3.h:S v:pred</ta>
            <ta e="T37" id="Seg_646" s="T36">np.h:S</ta>
            <ta e="T38" id="Seg_647" s="T37">v:pred</ta>
            <ta e="T41" id="Seg_648" s="T40">np:S</ta>
            <ta e="T42" id="Seg_649" s="T41">v:pred</ta>
            <ta e="T47" id="Seg_650" s="T46">np:S</ta>
            <ta e="T49" id="Seg_651" s="T48">v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_652" s="T1">np.h:A</ta>
            <ta e="T5" id="Seg_653" s="T4">0.1.h:E</ta>
            <ta e="T6" id="Seg_654" s="T5">np:Th</ta>
            <ta e="T7" id="Seg_655" s="T6">np.h:A</ta>
            <ta e="T10" id="Seg_656" s="T9">np:G</ta>
            <ta e="T11" id="Seg_657" s="T10">0.3.h:A</ta>
            <ta e="T13" id="Seg_658" s="T12">np:Th</ta>
            <ta e="T15" id="Seg_659" s="T14">np.h:A</ta>
            <ta e="T18" id="Seg_660" s="T17">np.h:Th</ta>
            <ta e="T19" id="Seg_661" s="T18">0.3.h:B</ta>
            <ta e="T20" id="Seg_662" s="T19">np.h:A</ta>
            <ta e="T23" id="Seg_663" s="T22">np:L</ta>
            <ta e="T24" id="Seg_664" s="T23">np:Th</ta>
            <ta e="T29" id="Seg_665" s="T28">np.h:A</ta>
            <ta e="T31" id="Seg_666" s="T30">adv:G</ta>
            <ta e="T32" id="Seg_667" s="T31">np.h:A</ta>
            <ta e="T35" id="Seg_668" s="T34">np:Th</ta>
            <ta e="T36" id="Seg_669" s="T35">0.3.h:B</ta>
            <ta e="T37" id="Seg_670" s="T36">np.h:A</ta>
            <ta e="T41" id="Seg_671" s="T40">np:Th</ta>
            <ta e="T44" id="Seg_672" s="T43">np.h:Poss</ta>
            <ta e="T47" id="Seg_673" s="T46">np:Th</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T34" id="Seg_674" s="T33">RUS:gram</ta>
            <ta e="T39" id="Seg_675" s="T38">RUS:gram</ta>
            <ta e="T40" id="Seg_676" s="T39">RUS:gram</ta>
            <ta e="T43" id="Seg_677" s="T42">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_678" s="T1">A nutcracker cries: I want nuts so much.</ta>
            <ta e="T11" id="Seg_679" s="T6">A bear went and reached this river.</ta>
            <ta e="T14" id="Seg_680" s="T11">There are no cones.</ta>
            <ta e="T17" id="Seg_681" s="T14">The bear went again.</ta>
            <ta e="T19" id="Seg_682" s="T17">He found a sparrow.</ta>
            <ta e="T28" id="Seg_683" s="T19">The sparrow said: In such a place there are many larches.</ta>
            <ta e="T31" id="Seg_684" s="T28">The bear went there.</ta>
            <ta e="T36" id="Seg_685" s="T31">The bear came and found a larch.</ta>
            <ta e="T42" id="Seg_686" s="T36">The sparrow said: And the meat (???).</ta>
            <ta e="T50" id="Seg_687" s="T42">And the nutcracker had the black meat all the time.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_688" s="T1">Ein Nussknacker ruft: Ich will so gern Nüsse.</ta>
            <ta e="T11" id="Seg_689" s="T6">Ein Bär ging und erreichte diesen Fluss.</ta>
            <ta e="T14" id="Seg_690" s="T11">Es gibt keine Nüsse.</ta>
            <ta e="T17" id="Seg_691" s="T14">Der Bär ging wieder.</ta>
            <ta e="T19" id="Seg_692" s="T17">Er fand einen Spatz.</ta>
            <ta e="T28" id="Seg_693" s="T19">Der Spatz sagt: An diesem Ort gibt es viele Lärchen.</ta>
            <ta e="T31" id="Seg_694" s="T28">Der Bär ging dorthin.</ta>
            <ta e="T36" id="Seg_695" s="T31">Der Bär kam an und fand eine Lärche.</ta>
            <ta e="T42" id="Seg_696" s="T36">Der Spatz sagt: Und das Fleisch (?).</ta>
            <ta e="T50" id="Seg_697" s="T42">Und der Nussknacker hatte schwarzes Fleisch.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_698" s="T1">Кедровка кричит: Так хочу орехов.</ta>
            <ta e="T11" id="Seg_699" s="T6">Медведь ушел, к этой речке пришел.</ta>
            <ta e="T14" id="Seg_700" s="T11">Никаких шишек нет.</ta>
            <ta e="T17" id="Seg_701" s="T14">Медведь опять пошел.</ta>
            <ta e="T19" id="Seg_702" s="T17">Воробья нашел.</ta>
            <ta e="T28" id="Seg_703" s="T19">Воробей сказал: В такой-то местности лиственниц много есть.</ta>
            <ta e="T31" id="Seg_704" s="T28">Медведь пошел туда.</ta>
            <ta e="T36" id="Seg_705" s="T31">Медведь пришел и лиственницу нашел.</ta>
            <ta e="T42" id="Seg_706" s="T36">Воробей сказал: А что, мясо было.</ta>
            <ta e="T50" id="Seg_707" s="T42">А у кедровки все время черное мясо было. </ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_708" s="T1">кедровка кричит так хочу орехи</ta>
            <ta e="T11" id="Seg_709" s="T6">медведь ушел к этой речке пришел</ta>
            <ta e="T14" id="Seg_710" s="T11">никаких шишек нет</ta>
            <ta e="T17" id="Seg_711" s="T14">медведь опять пошел</ta>
            <ta e="T19" id="Seg_712" s="T17">воробья нашел</ta>
            <ta e="T28" id="Seg_713" s="T19">воробей сказал в такой местности лиственниц много есть</ta>
            <ta e="T31" id="Seg_714" s="T28">медведь пошел сюда</ta>
            <ta e="T36" id="Seg_715" s="T31">медведь пришел лиственницу нашел</ta>
            <ta e="T42" id="Seg_716" s="T36">воробей сказал а что мясо было</ta>
            <ta e="T50" id="Seg_717" s="T42">кедровка все время черное мясо было</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
