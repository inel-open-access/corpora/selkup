<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KNK_1965_BearAndHare_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KNK_1965_BearAndHare_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">142</ud-information>
            <ud-information attribute-name="# HIAT:w">102</ud-information>
            <ud-information attribute-name="# e">102</ud-information>
            <ud-information attribute-name="# HIAT:u">19</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KNK">
            <abbreviation>KNK</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KNK"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T145" id="Seg_0" n="sc" s="T43">
               <ts e="T45" id="Seg_2" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_4" n="HIAT:w" s="T43">Čʼaptä</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_7" n="HIAT:w" s="T44">1</ts>
                  <nts id="Seg_8" n="HIAT:ip">.</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_11" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_13" n="HIAT:w" s="T45">Qorqɨ</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_16" n="HIAT:w" s="T46">aj</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_19" n="HIAT:w" s="T47">nʼoma</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_23" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_25" n="HIAT:w" s="T48">Qorqɨ</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_28" n="HIAT:w" s="T49">nʼomam</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_31" n="HIAT:w" s="T50">qoŋɨtɨ</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_35" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_37" n="HIAT:w" s="T51">Mat</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_40" n="HIAT:w" s="T52">tašıntɨ</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_43" n="HIAT:w" s="T53">amtap</ts>
                  <nts id="Seg_44" n="HIAT:ip">!</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_47" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_49" n="HIAT:w" s="T54">Nʼoma</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_52" n="HIAT:w" s="T55">kətɨsɨtɨ</ts>
                  <nts id="Seg_53" n="HIAT:ip">:</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_56" n="HIAT:w" s="T56">Mat</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_59" n="HIAT:w" s="T57">tan</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_62" n="HIAT:w" s="T58">nɔːnə</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_65" n="HIAT:w" s="T59">ors</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_68" n="HIAT:w" s="T60">ɛːŋak</ts>
                  <nts id="Seg_69" n="HIAT:ip">!</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_72" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_74" n="HIAT:w" s="T61">Кutar</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_77" n="HIAT:w" s="T62">mašıp</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_80" n="HIAT:w" s="T63">amtal</ts>
                  <nts id="Seg_81" n="HIAT:ip">?</nts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_84" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_86" n="HIAT:w" s="T64">Qorqɨ</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_89" n="HIAT:w" s="T65">aqlɨmɔːnna</ts>
                  <nts id="Seg_90" n="HIAT:ip">,</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_93" n="HIAT:w" s="T66">kɨtɨŋɨt</ts>
                  <nts id="Seg_94" n="HIAT:ip">:</nts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_97" n="HIAT:w" s="T67">Tat</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_99" n="HIAT:ip">(</nts>
                  <ts e="T69" id="Seg_101" n="HIAT:w" s="T68">aša</ts>
                  <nts id="Seg_102" n="HIAT:ip">)</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_105" n="HIAT:w" s="T69">kɨpa</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_108" n="HIAT:w" s="T70">ɛːŋantɨ</ts>
                  <nts id="Seg_109" n="HIAT:ip">!</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_112" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_114" n="HIAT:w" s="T71">Mannɨpɨlɨmɨt</ts>
                  <nts id="Seg_115" n="HIAT:ip">!</nts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_118" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_120" n="HIAT:w" s="T72">Kisa</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_123" n="HIAT:w" s="T73">qɨllɔːmıː</ts>
                  <nts id="Seg_124" n="HIAT:ip">!</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_127" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_129" n="HIAT:w" s="T74">Kutɨn</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_132" n="HIAT:w" s="T75">nɔːne</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_135" n="HIAT:w" s="T76">suːrɨkijat</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_138" n="HIAT:w" s="T77">nɨrkɨmɔːttɔːtɨt</ts>
                  <nts id="Seg_139" n="HIAT:ip">,</nts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_142" n="HIAT:w" s="T78">namɨ</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_145" n="HIAT:w" s="T79">ors</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_148" n="HIAT:w" s="T80">ɛːŋa</ts>
                  <nts id="Seg_149" n="HIAT:ip">.</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_152" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_154" n="HIAT:w" s="T81">Təpɨt</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_157" n="HIAT:w" s="T82">wottɨ</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_159" n="HIAT:ip">(</nts>
                  <ts e="T84" id="Seg_161" n="HIAT:w" s="T83">məttɨ</ts>
                  <nts id="Seg_162" n="HIAT:ip">)</nts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_165" n="HIAT:w" s="T84">šüː</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_168" n="HIAT:w" s="T85">qənnɔːtɨt</ts>
                  <nts id="Seg_169" n="HIAT:ip">.</nts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_172" n="HIAT:u" s="T86">
                  <ts e="T87" id="Seg_174" n="HIAT:w" s="T86">Nʼoma</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_177" n="HIAT:w" s="T87">uːkɨt</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_180" n="HIAT:w" s="T88">qälɨmpa</ts>
                  <nts id="Seg_181" n="HIAT:ip">,</nts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_184" n="HIAT:w" s="T89">suːrɨkijat</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_187" n="HIAT:w" s="T90">nɨrkɨmɔːllä</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_190" n="HIAT:w" s="T91">mačʼonte</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_193" n="HIAT:w" s="T92">kurəlnɔːtɨt</ts>
                  <nts id="Seg_194" n="HIAT:ip">.</nts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T99" id="Seg_197" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_199" n="HIAT:w" s="T93">Qorqɨ</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_202" n="HIAT:w" s="T94">wərqɨ</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_205" n="HIAT:w" s="T95">eŋa</ts>
                  <nts id="Seg_206" n="HIAT:ip">,</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_209" n="HIAT:w" s="T96">təp</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_212" n="HIAT:w" s="T97">kuntaktɨ</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_215" n="HIAT:w" s="T98">ata</ts>
                  <nts id="Seg_216" n="HIAT:ip">.</nts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_219" n="HIAT:u" s="T99">
                  <ts e="T100" id="Seg_221" n="HIAT:w" s="T99">Nʼoma</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_223" n="HIAT:ip">(</nts>
                  <ts e="T101" id="Seg_225" n="HIAT:w" s="T100">koŋɨmpa</ts>
                  <nts id="Seg_226" n="HIAT:ip">/</nts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_229" n="HIAT:w" s="T101">kətɨsɨt</ts>
                  <nts id="Seg_230" n="HIAT:ip">)</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_233" n="HIAT:w" s="T102">mulɨmpa</ts>
                  <nts id="Seg_234" n="HIAT:ip">:</nts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_237" n="HIAT:w" s="T103">Aga</ts>
                  <nts id="Seg_238" n="HIAT:ip">,</nts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_241" n="HIAT:w" s="T104">männɨmpät</ts>
                  <nts id="Seg_242" n="HIAT:ip">,</nts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_245" n="HIAT:w" s="T105">mat</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_248" n="HIAT:w" s="T106">uːkɨt</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_251" n="HIAT:w" s="T107">qəlɨmpak</ts>
                  <nts id="Seg_252" n="HIAT:ip">,</nts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_255" n="HIAT:w" s="T108">man</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_258" n="HIAT:w" s="T109">nɔːne</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_261" n="HIAT:w" s="T110">čʼosä</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_264" n="HIAT:w" s="T111">tuntɨtɨlʼ</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_267" n="HIAT:w" s="T112">muntɨk</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_270" n="HIAT:w" s="T113">suːrɨp</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_273" n="HIAT:w" s="T114">nɨrkɨmɔːnnɔːtɨt</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_276" n="HIAT:w" s="T115">aj</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_279" n="HIAT:w" s="T116">mačʼonte</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_282" n="HIAT:w" s="T117">kurəlnɔːtət</ts>
                  <nts id="Seg_283" n="HIAT:ip">.</nts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_286" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_288" n="HIAT:w" s="T118">Qorqɨ</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_291" n="HIAT:w" s="T119">aqlɨmɔːnnä</ts>
                  <nts id="Seg_292" n="HIAT:ip">,</nts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_295" n="HIAT:w" s="T120">kətɨŋɨt</ts>
                  <nts id="Seg_296" n="HIAT:ip">:</nts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_299" n="HIAT:w" s="T121">onɨlʼ</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_302" n="HIAT:w" s="T122">kətsat</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_305" n="HIAT:w" s="T123">tan</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_308" n="HIAT:w" s="T124">nɔːn</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_311" n="HIAT:w" s="T125">muntɨk</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_314" n="HIAT:w" s="T126">ɛnnɔːtɨt</ts>
                  <nts id="Seg_315" n="HIAT:ip">.</nts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_318" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_320" n="HIAT:w" s="T127">Mat</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_323" n="HIAT:w" s="T128">onäk</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_326" n="HIAT:w" s="T129">qontɨrsap</ts>
                  <nts id="Seg_327" n="HIAT:ip">,</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_330" n="HIAT:w" s="T130">tat</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_333" n="HIAT:w" s="T131">uːkɨt</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_336" n="HIAT:w" s="T132">qəlɨmpal</ts>
                  <nts id="Seg_337" n="HIAT:ip">.</nts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T137" id="Seg_340" n="HIAT:u" s="T133">
                  <ts e="T134" id="Seg_342" n="HIAT:w" s="T133">Tat</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_345" n="HIAT:w" s="T134">onɨlʼ</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_348" n="HIAT:w" s="T135">ors</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_351" n="HIAT:w" s="T136">ɛːŋantɨ</ts>
                  <nts id="Seg_352" n="HIAT:ip">.</nts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T142" id="Seg_355" n="HIAT:u" s="T137">
                  <ts e="T138" id="Seg_357" n="HIAT:w" s="T137">Qorqɨ</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_360" n="HIAT:w" s="T138">nɨrkɨmɔːnna</ts>
                  <nts id="Seg_361" n="HIAT:ip">,</nts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_364" n="HIAT:w" s="T139">čʼäkäptɨlä</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_367" n="HIAT:w" s="T140">mačʼontɨ</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_370" n="HIAT:w" s="T141">pakta</ts>
                  <nts id="Seg_371" n="HIAT:ip">.</nts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T145" id="Seg_374" n="HIAT:u" s="T142">
                  <ts e="T143" id="Seg_376" n="HIAT:w" s="T142">Nʼomaja</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_379" n="HIAT:w" s="T143">orsä</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_382" n="HIAT:w" s="T144">pisɨnʼnʼa</ts>
                  <nts id="Seg_383" n="HIAT:ip">.</nts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T145" id="Seg_385" n="sc" s="T43">
               <ts e="T44" id="Seg_387" n="e" s="T43">Čʼaptä </ts>
               <ts e="T45" id="Seg_389" n="e" s="T44">1. </ts>
               <ts e="T46" id="Seg_391" n="e" s="T45">Qorqɨ </ts>
               <ts e="T47" id="Seg_393" n="e" s="T46">aj </ts>
               <ts e="T48" id="Seg_395" n="e" s="T47">nʼoma. </ts>
               <ts e="T49" id="Seg_397" n="e" s="T48">Qorqɨ </ts>
               <ts e="T50" id="Seg_399" n="e" s="T49">nʼomam </ts>
               <ts e="T51" id="Seg_401" n="e" s="T50">qoŋɨtɨ. </ts>
               <ts e="T52" id="Seg_403" n="e" s="T51">Mat </ts>
               <ts e="T53" id="Seg_405" n="e" s="T52">tašıntɨ </ts>
               <ts e="T54" id="Seg_407" n="e" s="T53">amtap! </ts>
               <ts e="T55" id="Seg_409" n="e" s="T54">Nʼoma </ts>
               <ts e="T56" id="Seg_411" n="e" s="T55">kətɨsɨtɨ: </ts>
               <ts e="T57" id="Seg_413" n="e" s="T56">Mat </ts>
               <ts e="T58" id="Seg_415" n="e" s="T57">tan </ts>
               <ts e="T59" id="Seg_417" n="e" s="T58">nɔːnə </ts>
               <ts e="T60" id="Seg_419" n="e" s="T59">ors </ts>
               <ts e="T61" id="Seg_421" n="e" s="T60">ɛːŋak! </ts>
               <ts e="T62" id="Seg_423" n="e" s="T61">Кutar </ts>
               <ts e="T63" id="Seg_425" n="e" s="T62">mašıp </ts>
               <ts e="T64" id="Seg_427" n="e" s="T63">amtal? </ts>
               <ts e="T65" id="Seg_429" n="e" s="T64">Qorqɨ </ts>
               <ts e="T66" id="Seg_431" n="e" s="T65">aqlɨmɔːnna, </ts>
               <ts e="T67" id="Seg_433" n="e" s="T66">kɨtɨŋɨt: </ts>
               <ts e="T68" id="Seg_435" n="e" s="T67">Tat </ts>
               <ts e="T69" id="Seg_437" n="e" s="T68">(aša) </ts>
               <ts e="T70" id="Seg_439" n="e" s="T69">kɨpa </ts>
               <ts e="T71" id="Seg_441" n="e" s="T70">ɛːŋantɨ! </ts>
               <ts e="T72" id="Seg_443" n="e" s="T71">Mannɨpɨlɨmɨt! </ts>
               <ts e="T73" id="Seg_445" n="e" s="T72">Kisa </ts>
               <ts e="T74" id="Seg_447" n="e" s="T73">qɨllɔːmıː! </ts>
               <ts e="T75" id="Seg_449" n="e" s="T74">Kutɨn </ts>
               <ts e="T76" id="Seg_451" n="e" s="T75">nɔːne </ts>
               <ts e="T77" id="Seg_453" n="e" s="T76">suːrɨkijat </ts>
               <ts e="T78" id="Seg_455" n="e" s="T77">nɨrkɨmɔːttɔːtɨt, </ts>
               <ts e="T79" id="Seg_457" n="e" s="T78">namɨ </ts>
               <ts e="T80" id="Seg_459" n="e" s="T79">ors </ts>
               <ts e="T81" id="Seg_461" n="e" s="T80">ɛːŋa. </ts>
               <ts e="T82" id="Seg_463" n="e" s="T81">Təpɨt </ts>
               <ts e="T83" id="Seg_465" n="e" s="T82">wottɨ </ts>
               <ts e="T84" id="Seg_467" n="e" s="T83">(məttɨ) </ts>
               <ts e="T85" id="Seg_469" n="e" s="T84">šüː </ts>
               <ts e="T86" id="Seg_471" n="e" s="T85">qənnɔːtɨt. </ts>
               <ts e="T87" id="Seg_473" n="e" s="T86">Nʼoma </ts>
               <ts e="T88" id="Seg_475" n="e" s="T87">uːkɨt </ts>
               <ts e="T89" id="Seg_477" n="e" s="T88">qälɨmpa, </ts>
               <ts e="T90" id="Seg_479" n="e" s="T89">suːrɨkijat </ts>
               <ts e="T91" id="Seg_481" n="e" s="T90">nɨrkɨmɔːllä </ts>
               <ts e="T92" id="Seg_483" n="e" s="T91">mačʼonte </ts>
               <ts e="T93" id="Seg_485" n="e" s="T92">kurəlnɔːtɨt. </ts>
               <ts e="T94" id="Seg_487" n="e" s="T93">Qorqɨ </ts>
               <ts e="T95" id="Seg_489" n="e" s="T94">wərqɨ </ts>
               <ts e="T96" id="Seg_491" n="e" s="T95">eŋa, </ts>
               <ts e="T97" id="Seg_493" n="e" s="T96">təp </ts>
               <ts e="T98" id="Seg_495" n="e" s="T97">kuntaktɨ </ts>
               <ts e="T99" id="Seg_497" n="e" s="T98">ata. </ts>
               <ts e="T100" id="Seg_499" n="e" s="T99">Nʼoma </ts>
               <ts e="T101" id="Seg_501" n="e" s="T100">(koŋɨmpa/ </ts>
               <ts e="T102" id="Seg_503" n="e" s="T101">kətɨsɨt) </ts>
               <ts e="T103" id="Seg_505" n="e" s="T102">mulɨmpa: </ts>
               <ts e="T104" id="Seg_507" n="e" s="T103">Aga, </ts>
               <ts e="T105" id="Seg_509" n="e" s="T104">männɨmpät, </ts>
               <ts e="T106" id="Seg_511" n="e" s="T105">mat </ts>
               <ts e="T107" id="Seg_513" n="e" s="T106">uːkɨt </ts>
               <ts e="T108" id="Seg_515" n="e" s="T107">qəlɨmpak, </ts>
               <ts e="T109" id="Seg_517" n="e" s="T108">man </ts>
               <ts e="T110" id="Seg_519" n="e" s="T109">nɔːne </ts>
               <ts e="T111" id="Seg_521" n="e" s="T110">čʼosä </ts>
               <ts e="T112" id="Seg_523" n="e" s="T111">tuntɨtɨlʼ </ts>
               <ts e="T113" id="Seg_525" n="e" s="T112">muntɨk </ts>
               <ts e="T114" id="Seg_527" n="e" s="T113">suːrɨp </ts>
               <ts e="T115" id="Seg_529" n="e" s="T114">nɨrkɨmɔːnnɔːtɨt </ts>
               <ts e="T116" id="Seg_531" n="e" s="T115">aj </ts>
               <ts e="T117" id="Seg_533" n="e" s="T116">mačʼonte </ts>
               <ts e="T118" id="Seg_535" n="e" s="T117">kurəlnɔːtət. </ts>
               <ts e="T119" id="Seg_537" n="e" s="T118">Qorqɨ </ts>
               <ts e="T120" id="Seg_539" n="e" s="T119">aqlɨmɔːnnä, </ts>
               <ts e="T121" id="Seg_541" n="e" s="T120">kətɨŋɨt: </ts>
               <ts e="T122" id="Seg_543" n="e" s="T121">onɨlʼ </ts>
               <ts e="T123" id="Seg_545" n="e" s="T122">kətsat </ts>
               <ts e="T124" id="Seg_547" n="e" s="T123">tan </ts>
               <ts e="T125" id="Seg_549" n="e" s="T124">nɔːn </ts>
               <ts e="T126" id="Seg_551" n="e" s="T125">muntɨk </ts>
               <ts e="T127" id="Seg_553" n="e" s="T126">ɛnnɔːtɨt. </ts>
               <ts e="T128" id="Seg_555" n="e" s="T127">Mat </ts>
               <ts e="T129" id="Seg_557" n="e" s="T128">onäk </ts>
               <ts e="T130" id="Seg_559" n="e" s="T129">qontɨrsap, </ts>
               <ts e="T131" id="Seg_561" n="e" s="T130">tat </ts>
               <ts e="T132" id="Seg_563" n="e" s="T131">uːkɨt </ts>
               <ts e="T133" id="Seg_565" n="e" s="T132">qəlɨmpal. </ts>
               <ts e="T134" id="Seg_567" n="e" s="T133">Tat </ts>
               <ts e="T135" id="Seg_569" n="e" s="T134">onɨlʼ </ts>
               <ts e="T136" id="Seg_571" n="e" s="T135">ors </ts>
               <ts e="T137" id="Seg_573" n="e" s="T136">ɛːŋantɨ. </ts>
               <ts e="T138" id="Seg_575" n="e" s="T137">Qorqɨ </ts>
               <ts e="T139" id="Seg_577" n="e" s="T138">nɨrkɨmɔːnna, </ts>
               <ts e="T140" id="Seg_579" n="e" s="T139">čʼäkäptɨlä </ts>
               <ts e="T141" id="Seg_581" n="e" s="T140">mačʼontɨ </ts>
               <ts e="T142" id="Seg_583" n="e" s="T141">pakta. </ts>
               <ts e="T143" id="Seg_585" n="e" s="T142">Nʼomaja </ts>
               <ts e="T144" id="Seg_587" n="e" s="T143">orsä </ts>
               <ts e="T145" id="Seg_589" n="e" s="T144">pisɨnʼnʼa. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T45" id="Seg_590" s="T43">KNK_1965_BearAndHare_flk.001 (001.001)</ta>
            <ta e="T48" id="Seg_591" s="T45">KNK_1965_BearAndHare_flk.002 (001.002)</ta>
            <ta e="T51" id="Seg_592" s="T48">KNK_1965_BearAndHare_flk.003 (001.003)</ta>
            <ta e="T54" id="Seg_593" s="T51">KNK_1965_BearAndHare_flk.004 (001.004)</ta>
            <ta e="T61" id="Seg_594" s="T54">KNK_1965_BearAndHare_flk.005 (001.005)</ta>
            <ta e="T64" id="Seg_595" s="T61">KNK_1965_BearAndHare_flk.006 (001.006)</ta>
            <ta e="T71" id="Seg_596" s="T64">KNK_1965_BearAndHare_flk.007 (001.007)</ta>
            <ta e="T72" id="Seg_597" s="T71">KNK_1965_BearAndHare_flk.008 (001.008)</ta>
            <ta e="T74" id="Seg_598" s="T72">KNK_1965_BearAndHare_flk.009 (001.009)</ta>
            <ta e="T81" id="Seg_599" s="T74">KNK_1965_BearAndHare_flk.010 (001.010)</ta>
            <ta e="T86" id="Seg_600" s="T81">KNK_1965_BearAndHare_flk.011 (001.011)</ta>
            <ta e="T93" id="Seg_601" s="T86">KNK_1965_BearAndHare_flk.012 (001.012)</ta>
            <ta e="T99" id="Seg_602" s="T93">KNK_1965_BearAndHare_flk.013 (001.013)</ta>
            <ta e="T118" id="Seg_603" s="T99">KNK_1965_BearAndHare_flk.014 (001.014)</ta>
            <ta e="T127" id="Seg_604" s="T118">KNK_1965_BearAndHare_flk.015 (001.015)</ta>
            <ta e="T133" id="Seg_605" s="T127">KNK_1965_BearAndHare_flk.016 (001.016)</ta>
            <ta e="T137" id="Seg_606" s="T133">KNK_1965_BearAndHare_flk.017 (001.017)</ta>
            <ta e="T142" id="Seg_607" s="T137">KNK_1965_BearAndHare_flk.018 (001.018)</ta>
            <ta e="T145" id="Seg_608" s="T142">KNK_1965_BearAndHare_flk.019 (001.019)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T45" id="Seg_609" s="T43">чаптӓ № 1.</ta>
            <ta e="T48" id="Seg_610" s="T45">′kорkы ай ′нӧма.</ta>
            <ta e="T51" id="Seg_611" s="T48">′kорk[ɣ]ы ′нӧмам kоң′ыты.</ta>
            <ta e="T54" id="Seg_612" s="T51">Мат ′ташинты ′амтап!</ta>
            <ta e="T61" id="Seg_613" s="T54">нӧма kəтысыты: мат таннонə ор′сӓңак!</ta>
            <ta e="T64" id="Seg_614" s="T61">Кутар машип амтал?</ta>
            <ta e="T71" id="Seg_615" s="T64">′kорkы аглымонна, kытыңыт: тат[н] (аша) kыпа еңанты!</ta>
            <ta e="T72" id="Seg_616" s="T71">′манны‵пылымыт!</ta>
            <ta e="T74" id="Seg_617" s="T72">Киса kылʼло̄ми!</ta>
            <ta e="T81" id="Seg_618" s="T74">′кутын‵но̄не ′сурыкиjат ′нырkымот′тотыт наммы орсеңа.</ta>
            <ta e="T86" id="Seg_619" s="T81">тӧпыт в[w]отты (мəтты) шʼӱ kəн′ноты[ə]т.</ta>
            <ta e="T93" id="Seg_620" s="T86">нӧма ′ӯкыт ′kӓ̄лымпа ′сӯрыки‵jат ′нырkы‵молле ′ма̄чонте ку‵ррəлно̄ты[ə]т.</ta>
            <ta e="T99" id="Seg_621" s="T93">′kорkы вəрɣы еңа тӧп ′кунта̄кты ′а̄та.</ta>
            <ta e="T118" id="Seg_622" s="T99">нӧма (kоңымпа) (кə̄тысыт:) мулымпа ага, ′мӓннымпет мат ӯkыт ′kə̄лымпак манно̄не чо̄сӓ тунтытылʼ ′мунтык ′сӯрып ′нырkымонн‵отыт ай мачонте kурəлно̄тəт.</ta>
            <ta e="T127" id="Seg_623" s="T118">kорkы аглымоннӓ, kə̊тыңыт: онылʼ ′kӧтсат таннон мунтык ӓннотыт.</ta>
            <ta e="T133" id="Seg_624" s="T127">мат онӓк kондырсап тат ӯkыт kəлымпал.</ta>
            <ta e="T137" id="Seg_625" s="T133">тат о̄нылʼ орсӓңанты.</ta>
            <ta e="T142" id="Seg_626" s="T137">′kорkы ′нырkы[и]монна чӓkӓптыллʼе мачонты пакта.</ta>
            <ta e="T145" id="Seg_627" s="T142">нӧмаjа орсӓ писыннʼа.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T45" id="Seg_628" s="T43">čʼaptä № 1.</ta>
            <ta e="T48" id="Seg_629" s="T45">qorqɨ aj nöma.</ta>
            <ta e="T51" id="Seg_630" s="T48">qorq[q]ɨ nömam qoŋɨtɨ.</ta>
            <ta e="T54" id="Seg_631" s="T51">Мat tašintɨ amtap!</ta>
            <ta e="T61" id="Seg_632" s="T54">nöma qətɨsɨtɨ: mat tannonə orsäŋak!</ta>
            <ta e="T64" id="Seg_633" s="T61">Kutar mašip amtal?</ta>
            <ta e="T71" id="Seg_634" s="T64">qorqɨ aglɨmonna, qɨtɨŋɨt: tat[n] (aša) qɨpa eŋantɨ!</ta>
            <ta e="T72" id="Seg_635" s="T71">mannɨpɨlɨmɨt!</ta>
            <ta e="T74" id="Seg_636" s="T72">Кisa qɨlʼloːmi!</ta>
            <ta e="T81" id="Seg_637" s="T74">kutɨnnoːne surɨkijat nɨrqɨmottotɨt nammɨ orseŋa.</ta>
            <ta e="T86" id="Seg_638" s="T81">töpɨt v[w]ottɨ (məttɨ) šʼü qənnotɨ[ə]t.</ta>
            <ta e="T93" id="Seg_639" s="T86">nöma uːkɨt qäːlɨmpa suːrɨkijat nɨrqɨmolle maːčʼonte kurrəlnoːtɨ[ə]t.</ta>
            <ta e="T99" id="Seg_640" s="T93">qorqɨ vərqɨ eŋa töp kuntaːktɨ aːta.</ta>
            <ta e="T118" id="Seg_641" s="T99">nöma (qoŋɨmpa) (kəːtɨsɨt:) mulɨmpa aga, männɨmpet mat uːqɨt qəːlɨmpak mannoːne čʼoːsä tuntɨtɨlʼ muntɨk suːrɨp nɨrqɨmonnotɨt aj mačʼonte qurəlnoːtət.</ta>
            <ta e="T127" id="Seg_642" s="T118">qorqɨ aglɨmonnä, qətɨŋɨt: onɨlʼ qötsat tannon muntɨk ännotɨt.</ta>
            <ta e="T133" id="Seg_643" s="T127">mat onäk qondɨrsap tat uːqɨt qəlɨmpal.</ta>
            <ta e="T137" id="Seg_644" s="T133">tat oːnɨlʼ orsäŋantɨ.</ta>
            <ta e="T142" id="Seg_645" s="T137">qorqɨ nɨrqɨ[i]monna čʼäqäptɨllʼe mačʼontɨ pakta.</ta>
            <ta e="T145" id="Seg_646" s="T142">nömaja orsä pisɨnnʼa.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T45" id="Seg_647" s="T43">Čʼaptä №1. </ta>
            <ta e="T48" id="Seg_648" s="T45">Qorqɨ aj nʼoma. </ta>
            <ta e="T51" id="Seg_649" s="T48">Qorqɨ nʼomam qoŋɨtɨ. </ta>
            <ta e="T54" id="Seg_650" s="T51">Mat tašıntɨ amtap! </ta>
            <ta e="T61" id="Seg_651" s="T54">Nʼoma kətɨsɨtɨ: Mat tan nɔːnə ors ɛːŋak! </ta>
            <ta e="T64" id="Seg_652" s="T61">Кutar mašıp amtal? </ta>
            <ta e="T71" id="Seg_653" s="T64">Qorqɨ aqlɨmɔːnna, kɨtɨŋɨt: Tat (aša) kɨpa ɛːŋantɨ! </ta>
            <ta e="T72" id="Seg_654" s="T71">Mannɨpɨlɨmɨt! </ta>
            <ta e="T74" id="Seg_655" s="T72">Kisa qɨllɔːmıː! </ta>
            <ta e="T81" id="Seg_656" s="T74">Kutɨn nɔːne suːrɨkijat nɨrkɨmɔːttɔːtɨt, namɨ ors ɛːŋa. </ta>
            <ta e="T86" id="Seg_657" s="T81">Təpɨt wottɨ (məttɨ) šüː qənnɔːtɨt. </ta>
            <ta e="T93" id="Seg_658" s="T86">Nʼoma uːkɨt qälɨmpa, suːrɨkijat nɨrkɨmɔːllä mačʼonte kurəlnɔːtɨt. </ta>
            <ta e="T99" id="Seg_659" s="T93">Qorqɨ wərqɨ eŋa, təp kuntaktɨ ata. </ta>
            <ta e="T118" id="Seg_660" s="T99">Nʼoma (koŋɨmpa/ kətɨsɨt) mulɨmpa: Aga, männɨmpät, mat uːkɨt qəlɨmpak, man nɔːne čʼosä tuntɨtɨlʼ muntɨk suːrɨp nɨrkɨmɔːnnɔːtɨt aj mačʼonte kurəlnɔːtət. </ta>
            <ta e="T127" id="Seg_661" s="T118">Qorqɨ aqlɨmɔːnnä, kətɨŋɨt: onɨlʼ kətsat tan nɔːn muntɨk ɛnnɔːtɨt. </ta>
            <ta e="T133" id="Seg_662" s="T127">Mat onäk qontɨrsap, tat uːkɨt qəlɨmpal. </ta>
            <ta e="T137" id="Seg_663" s="T133">Tat onɨlʼ ors ɛːŋantɨ. </ta>
            <ta e="T142" id="Seg_664" s="T137">Qorqɨ nɨrkɨmɔːnna, čʼäkäptɨlä mačʼontɨ pakta. </ta>
            <ta e="T145" id="Seg_665" s="T142">Nʼomaja orsä pisɨnʼnʼa. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T44" id="Seg_666" s="T43">čʼaptä</ta>
            <ta e="T46" id="Seg_667" s="T45">qorqɨ</ta>
            <ta e="T47" id="Seg_668" s="T46">aj</ta>
            <ta e="T48" id="Seg_669" s="T47">nʼoma</ta>
            <ta e="T49" id="Seg_670" s="T48">qorqɨ</ta>
            <ta e="T50" id="Seg_671" s="T49">nʼoma-m</ta>
            <ta e="T51" id="Seg_672" s="T50">qo-ŋɨ-tɨ</ta>
            <ta e="T52" id="Seg_673" s="T51">mat</ta>
            <ta e="T53" id="Seg_674" s="T52">tašıntɨ</ta>
            <ta e="T54" id="Seg_675" s="T53">am-ta-p</ta>
            <ta e="T55" id="Seg_676" s="T54">nʼoma</ta>
            <ta e="T56" id="Seg_677" s="T55">kətɨ-sɨ-tɨ</ta>
            <ta e="T57" id="Seg_678" s="T56">Mat</ta>
            <ta e="T58" id="Seg_679" s="T57">tat</ta>
            <ta e="T59" id="Seg_680" s="T58">nɔː-nə</ta>
            <ta e="T60" id="Seg_681" s="T59">or-s</ta>
            <ta e="T61" id="Seg_682" s="T60">ɛː-ŋa-k</ta>
            <ta e="T62" id="Seg_683" s="T61">kutar</ta>
            <ta e="T63" id="Seg_684" s="T62">mašıp</ta>
            <ta e="T64" id="Seg_685" s="T63">am-ta-l</ta>
            <ta e="T65" id="Seg_686" s="T64">qorqɨ</ta>
            <ta e="T66" id="Seg_687" s="T65">aqlɨ-mɔːn-na</ta>
            <ta e="T67" id="Seg_688" s="T66">kɨtɨ-ŋɨ-t</ta>
            <ta e="T68" id="Seg_689" s="T67">tat</ta>
            <ta e="T69" id="Seg_690" s="T68">aša</ta>
            <ta e="T70" id="Seg_691" s="T69">kɨpa</ta>
            <ta e="T71" id="Seg_692" s="T70">ɛː-ŋa-ntɨ</ta>
            <ta e="T72" id="Seg_693" s="T71">mannɨ-pɨ-lɨ-mɨt</ta>
            <ta e="T73" id="Seg_694" s="T72">kisa</ta>
            <ta e="T74" id="Seg_695" s="T73">qɨl-lɔː-mıː</ta>
            <ta e="T75" id="Seg_696" s="T74">kutɨ-n</ta>
            <ta e="T76" id="Seg_697" s="T75">nɔː-ne</ta>
            <ta e="T77" id="Seg_698" s="T76">suːrɨ-k-ija-t</ta>
            <ta e="T78" id="Seg_699" s="T77">nɨrkɨ-mɔːt-tɔː-tɨt</ta>
            <ta e="T79" id="Seg_700" s="T78">namɨ</ta>
            <ta e="T80" id="Seg_701" s="T79">or-s</ta>
            <ta e="T81" id="Seg_702" s="T80">ɛː-ŋa</ta>
            <ta e="T82" id="Seg_703" s="T81">təp-ɨ-t</ta>
            <ta e="T83" id="Seg_704" s="T82">wottɨ</ta>
            <ta e="T84" id="Seg_705" s="T83">məttɨ</ta>
            <ta e="T85" id="Seg_706" s="T84">šüː</ta>
            <ta e="T86" id="Seg_707" s="T85">qən-nɔː-tɨt</ta>
            <ta e="T87" id="Seg_708" s="T86">nʼoma</ta>
            <ta e="T88" id="Seg_709" s="T87">uːkɨ-t</ta>
            <ta e="T89" id="Seg_710" s="T88">qälɨ-mpa</ta>
            <ta e="T90" id="Seg_711" s="T89">suːrɨ-k-ija-t</ta>
            <ta e="T91" id="Seg_712" s="T90">nɨrkɨ-mɔːl-lä</ta>
            <ta e="T92" id="Seg_713" s="T91">mačʼo-nte</ta>
            <ta e="T93" id="Seg_714" s="T92">kur-əl-nɔː-tɨt</ta>
            <ta e="T94" id="Seg_715" s="T93">qorqɨ</ta>
            <ta e="T95" id="Seg_716" s="T94">wərqɨ</ta>
            <ta e="T96" id="Seg_717" s="T95">e-ŋa</ta>
            <ta e="T97" id="Seg_718" s="T96">təp</ta>
            <ta e="T98" id="Seg_719" s="T97">kuntak-tɨ</ta>
            <ta e="T99" id="Seg_720" s="T98">ata</ta>
            <ta e="T100" id="Seg_721" s="T99">nʼoma</ta>
            <ta e="T101" id="Seg_722" s="T100">koŋɨ-mpa</ta>
            <ta e="T102" id="Seg_723" s="T101">kətɨ-sɨ-t</ta>
            <ta e="T103" id="Seg_724" s="T102">mulɨ-mpa</ta>
            <ta e="T104" id="Seg_725" s="T103">aga</ta>
            <ta e="T105" id="Seg_726" s="T104">männɨ-mp-ät</ta>
            <ta e="T106" id="Seg_727" s="T105">mat</ta>
            <ta e="T107" id="Seg_728" s="T106">uːkɨ-t</ta>
            <ta e="T108" id="Seg_729" s="T107">qəlɨ-mpa-k</ta>
            <ta e="T109" id="Seg_730" s="T108">man</ta>
            <ta e="T110" id="Seg_731" s="T109">nɔː-ne</ta>
            <ta e="T111" id="Seg_732" s="T110">čʼosä</ta>
            <ta e="T112" id="Seg_733" s="T111">tu-ntɨ-tɨlʼ</ta>
            <ta e="T113" id="Seg_734" s="T112">muntɨk</ta>
            <ta e="T114" id="Seg_735" s="T113">suːrɨp</ta>
            <ta e="T115" id="Seg_736" s="T114">nɨrkɨ-mɔːn-nɔː-tɨt</ta>
            <ta e="T116" id="Seg_737" s="T115">aj</ta>
            <ta e="T117" id="Seg_738" s="T116">mačʼo-nte</ta>
            <ta e="T118" id="Seg_739" s="T117">kur-əl-nɔː-tət</ta>
            <ta e="T119" id="Seg_740" s="T118">qorqɨ</ta>
            <ta e="T120" id="Seg_741" s="T119">aqlɨ-mɔːn-nä</ta>
            <ta e="T121" id="Seg_742" s="T120">kətɨ-ŋɨ-t</ta>
            <ta e="T122" id="Seg_743" s="T121">on-ɨ-lʼ</ta>
            <ta e="T123" id="Seg_744" s="T122">kətsat</ta>
            <ta e="T124" id="Seg_745" s="T123">tat</ta>
            <ta e="T125" id="Seg_746" s="T124">nɔː-n</ta>
            <ta e="T126" id="Seg_747" s="T125">muntɨk</ta>
            <ta e="T127" id="Seg_748" s="T126">ɛn-nɔː-tɨt</ta>
            <ta e="T128" id="Seg_749" s="T127">mat</ta>
            <ta e="T129" id="Seg_750" s="T128">onäk</ta>
            <ta e="T130" id="Seg_751" s="T129">qo-ntɨ-r-sa-p</ta>
            <ta e="T131" id="Seg_752" s="T130">tan</ta>
            <ta e="T132" id="Seg_753" s="T131">uːkɨ-t</ta>
            <ta e="T133" id="Seg_754" s="T132">qəlɨ-mpa-l</ta>
            <ta e="T134" id="Seg_755" s="T133">tan</ta>
            <ta e="T135" id="Seg_756" s="T134">on-ɨ-lʼ</ta>
            <ta e="T136" id="Seg_757" s="T135">or-s</ta>
            <ta e="T137" id="Seg_758" s="T136">ɛː-ŋa-ntɨ</ta>
            <ta e="T138" id="Seg_759" s="T137">qorqɨ</ta>
            <ta e="T139" id="Seg_760" s="T138">nɨrkɨ-mɔːn-na</ta>
            <ta e="T140" id="Seg_761" s="T139">*čʼäk-äptɨ-lä</ta>
            <ta e="T141" id="Seg_762" s="T140">mačʼo-ntɨ</ta>
            <ta e="T142" id="Seg_763" s="T141">pakta</ta>
            <ta e="T143" id="Seg_764" s="T142">nʼoma-ja</ta>
            <ta e="T144" id="Seg_765" s="T143">or-sä</ta>
            <ta e="T145" id="Seg_766" s="T144">pisɨ-nʼ-nʼa</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T44" id="Seg_767" s="T43">čʼaptä</ta>
            <ta e="T46" id="Seg_768" s="T45">qorqɨ</ta>
            <ta e="T47" id="Seg_769" s="T46">aj</ta>
            <ta e="T48" id="Seg_770" s="T47">nʼoma</ta>
            <ta e="T49" id="Seg_771" s="T48">qorqɨ</ta>
            <ta e="T50" id="Seg_772" s="T49">nʼoma-m</ta>
            <ta e="T51" id="Seg_773" s="T50">qo-ŋɨ-tɨ</ta>
            <ta e="T52" id="Seg_774" s="T51">man</ta>
            <ta e="T53" id="Seg_775" s="T52">tašıntɨ</ta>
            <ta e="T54" id="Seg_776" s="T53">am-ɛntɨ-m</ta>
            <ta e="T55" id="Seg_777" s="T54">nʼoma</ta>
            <ta e="T56" id="Seg_778" s="T55">kətɨ-sɨ-tɨ</ta>
            <ta e="T57" id="Seg_779" s="T56">man</ta>
            <ta e="T58" id="Seg_780" s="T57">tan</ta>
            <ta e="T59" id="Seg_781" s="T58">*nɔː-nɨ</ta>
            <ta e="T60" id="Seg_782" s="T59">orɨ-sä</ta>
            <ta e="T61" id="Seg_783" s="T60">ɛː-ŋɨ-k</ta>
            <ta e="T62" id="Seg_784" s="T61">kuttar</ta>
            <ta e="T63" id="Seg_785" s="T62">mašım</ta>
            <ta e="T64" id="Seg_786" s="T63">am-ɛntɨ-l</ta>
            <ta e="T65" id="Seg_787" s="T64">qorqɨ</ta>
            <ta e="T66" id="Seg_788" s="T65">*aqlɨ-mɔːt-ŋɨ</ta>
            <ta e="T67" id="Seg_789" s="T66">kətɨ-ŋɨ-t</ta>
            <ta e="T68" id="Seg_790" s="T67">tan</ta>
            <ta e="T69" id="Seg_791" s="T68">aš</ta>
            <ta e="T70" id="Seg_792" s="T69">kɨpa</ta>
            <ta e="T71" id="Seg_793" s="T70">ɛː-ŋɨ-ntɨ</ta>
            <ta e="T72" id="Seg_794" s="T71">mantɨ-mpɨ-lä-mɨt</ta>
            <ta e="T73" id="Seg_795" s="T72">kɨssa</ta>
            <ta e="T74" id="Seg_796" s="T73">qən-lä-mıː</ta>
            <ta e="T75" id="Seg_797" s="T74">kutɨ-n</ta>
            <ta e="T76" id="Seg_798" s="T75">*nɔː-nɨ</ta>
            <ta e="T77" id="Seg_799" s="T76">suːrɨm-ka-iːja-t</ta>
            <ta e="T78" id="Seg_800" s="T77">nɨrkɨ-mɔːt-ɛntɨ-tɨt</ta>
            <ta e="T79" id="Seg_801" s="T78">na</ta>
            <ta e="T80" id="Seg_802" s="T79">orɨ-sä</ta>
            <ta e="T81" id="Seg_803" s="T80">ɛː-ŋɨ</ta>
            <ta e="T82" id="Seg_804" s="T81">təp-ɨ-t</ta>
            <ta e="T83" id="Seg_805" s="T82">wəttɨ</ta>
            <ta e="T84" id="Seg_806" s="T83">wəttɨ</ta>
            <ta e="T85" id="Seg_807" s="T84">*šüː</ta>
            <ta e="T86" id="Seg_808" s="T85">qən-ŋɨ-tɨt</ta>
            <ta e="T87" id="Seg_809" s="T86">nʼoma</ta>
            <ta e="T88" id="Seg_810" s="T87">uːkɨ-k</ta>
            <ta e="T89" id="Seg_811" s="T88">qälɨ-mpɨ</ta>
            <ta e="T90" id="Seg_812" s="T89">suːrɨm-ka-iːja-t</ta>
            <ta e="T91" id="Seg_813" s="T90">nɨrkɨ-mɔːt-lä</ta>
            <ta e="T92" id="Seg_814" s="T91">mačʼɨ-ntɨ</ta>
            <ta e="T93" id="Seg_815" s="T92">kur-ɔːl-ŋɨ-tɨt</ta>
            <ta e="T94" id="Seg_816" s="T93">qorqɨ</ta>
            <ta e="T95" id="Seg_817" s="T94">wərqɨ</ta>
            <ta e="T96" id="Seg_818" s="T95">ɛː-ŋɨ</ta>
            <ta e="T97" id="Seg_819" s="T96">təp</ta>
            <ta e="T98" id="Seg_820" s="T97">kuntak-ntɨ</ta>
            <ta e="T99" id="Seg_821" s="T98">atɨ</ta>
            <ta e="T100" id="Seg_822" s="T99">nʼoma</ta>
            <ta e="T101" id="Seg_823" s="T100">*koŋɨ-mpɨ</ta>
            <ta e="T102" id="Seg_824" s="T101">kətɨ-sɨ-tɨ</ta>
            <ta e="T103" id="Seg_825" s="T102">mulɨ-mpɨ</ta>
            <ta e="T104" id="Seg_826" s="T103">aga</ta>
            <ta e="T105" id="Seg_827" s="T104">mantɨ-mpɨ-ätɨ</ta>
            <ta e="T106" id="Seg_828" s="T105">man</ta>
            <ta e="T107" id="Seg_829" s="T106">uːkɨ-k</ta>
            <ta e="T108" id="Seg_830" s="T107">qälɨ-mpɨ-k</ta>
            <ta e="T109" id="Seg_831" s="T108">man</ta>
            <ta e="T110" id="Seg_832" s="T109">*nɔː-nɨ</ta>
            <ta e="T111" id="Seg_833" s="T110">čʼəssä</ta>
            <ta e="T112" id="Seg_834" s="T111">tü-ntɨ-ntɨlʼ</ta>
            <ta e="T113" id="Seg_835" s="T112">muntɨk</ta>
            <ta e="T114" id="Seg_836" s="T113">suːrɨm</ta>
            <ta e="T115" id="Seg_837" s="T114">nɨrkɨ-mɔːt-ŋɨ-tɨt</ta>
            <ta e="T116" id="Seg_838" s="T115">aj</ta>
            <ta e="T117" id="Seg_839" s="T116">mačʼɨ-ntɨ</ta>
            <ta e="T118" id="Seg_840" s="T117">kur-ɔːl-ŋɨ-tɨt</ta>
            <ta e="T119" id="Seg_841" s="T118">qorqɨ</ta>
            <ta e="T120" id="Seg_842" s="T119">*aqlɨ-mɔːt-n</ta>
            <ta e="T121" id="Seg_843" s="T120">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T122" id="Seg_844" s="T121">ontɨ-ɨ-lʼ</ta>
            <ta e="T123" id="Seg_845" s="T122">kətsan</ta>
            <ta e="T124" id="Seg_846" s="T123">tan</ta>
            <ta e="T125" id="Seg_847" s="T124">*nɔː-nɨ</ta>
            <ta e="T126" id="Seg_848" s="T125">muntɨk</ta>
            <ta e="T127" id="Seg_849" s="T126">ɛnɨ-ŋɨ-tɨt</ta>
            <ta e="T128" id="Seg_850" s="T127">man</ta>
            <ta e="T129" id="Seg_851" s="T128">onäk</ta>
            <ta e="T130" id="Seg_852" s="T129">qo-ntɨ-r-sɨ-m</ta>
            <ta e="T131" id="Seg_853" s="T130">tan</ta>
            <ta e="T132" id="Seg_854" s="T131">uːkɨ-k</ta>
            <ta e="T133" id="Seg_855" s="T132">qälɨ-mpɨ-l</ta>
            <ta e="T134" id="Seg_856" s="T133">tan</ta>
            <ta e="T135" id="Seg_857" s="T134">onäntɨ-ɨ-lʼ</ta>
            <ta e="T136" id="Seg_858" s="T135">orɨ-sä</ta>
            <ta e="T137" id="Seg_859" s="T136">ɛː-ŋɨ-ntɨ</ta>
            <ta e="T138" id="Seg_860" s="T137">qorqɨ</ta>
            <ta e="T139" id="Seg_861" s="T138">nɨrkɨ-mɔːt-ŋɨ</ta>
            <ta e="T140" id="Seg_862" s="T139">*čʼək-äptɨ-lä</ta>
            <ta e="T141" id="Seg_863" s="T140">mačʼɨ-ntɨ</ta>
            <ta e="T142" id="Seg_864" s="T141">paktɨ</ta>
            <ta e="T143" id="Seg_865" s="T142">nʼoma-ja</ta>
            <ta e="T144" id="Seg_866" s="T143">orɨ-sä</ta>
            <ta e="T145" id="Seg_867" s="T144">pisɨ-š-ŋɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T44" id="Seg_868" s="T43">tale.[NOM]</ta>
            <ta e="T46" id="Seg_869" s="T45">bear.[NOM]</ta>
            <ta e="T47" id="Seg_870" s="T46">and</ta>
            <ta e="T48" id="Seg_871" s="T47">hare.[NOM]</ta>
            <ta e="T49" id="Seg_872" s="T48">bear.[NOM]</ta>
            <ta e="T50" id="Seg_873" s="T49">hare-ACC</ta>
            <ta e="T51" id="Seg_874" s="T50">see-CO-3SG.O</ta>
            <ta e="T52" id="Seg_875" s="T51">I.NOM</ta>
            <ta e="T53" id="Seg_876" s="T52">you.SG.ACC</ta>
            <ta e="T54" id="Seg_877" s="T53">eat-FUT-1SG.O</ta>
            <ta e="T55" id="Seg_878" s="T54">hare.[NOM]</ta>
            <ta e="T56" id="Seg_879" s="T55">say-PST-3SG.O</ta>
            <ta e="T57" id="Seg_880" s="T56">I.NOM</ta>
            <ta e="T58" id="Seg_881" s="T57">you.SG.GEN</ta>
            <ta e="T59" id="Seg_882" s="T58">as.compared.to-ADV.EL</ta>
            <ta e="T60" id="Seg_883" s="T59">force-INSTR</ta>
            <ta e="T61" id="Seg_884" s="T60">be-CO-1SG.S</ta>
            <ta e="T62" id="Seg_885" s="T61">how</ta>
            <ta e="T63" id="Seg_886" s="T62">I.ACC</ta>
            <ta e="T64" id="Seg_887" s="T63">eat-FUT-2SG.O</ta>
            <ta e="T65" id="Seg_888" s="T64">bear.[NOM]</ta>
            <ta e="T66" id="Seg_889" s="T65">surprise-DRV-CO.[3SG.S]</ta>
            <ta e="T67" id="Seg_890" s="T66">say-CO-3SG</ta>
            <ta e="T68" id="Seg_891" s="T67">you.SG.NOM</ta>
            <ta e="T70" id="Seg_892" s="T69">small</ta>
            <ta e="T71" id="Seg_893" s="T70">be-CO-2SG.S</ta>
            <ta e="T72" id="Seg_894" s="T71">give.a.look-DUR-OPT-1PL</ta>
            <ta e="T73" id="Seg_895" s="T72">come.on</ta>
            <ta e="T74" id="Seg_896" s="T73">go.away-OPT-1DU</ta>
            <ta e="T75" id="Seg_897" s="T74">who-GEN</ta>
            <ta e="T76" id="Seg_898" s="T75">out-ADV.EL</ta>
            <ta e="T77" id="Seg_899" s="T76">wild.animal-DIM-child-PL.[NOM]</ta>
            <ta e="T78" id="Seg_900" s="T77">be.frightened-DRV-FUT-3PL</ta>
            <ta e="T79" id="Seg_901" s="T78">this</ta>
            <ta e="T80" id="Seg_902" s="T79">force-INSTR</ta>
            <ta e="T81" id="Seg_903" s="T80">be-CO.[3SG.S]</ta>
            <ta e="T82" id="Seg_904" s="T81">(s)he-EP-PL</ta>
            <ta e="T83" id="Seg_905" s="T82">road.[NOM]</ta>
            <ta e="T84" id="Seg_906" s="T83">road.[NOM]</ta>
            <ta e="T85" id="Seg_907" s="T84">through</ta>
            <ta e="T86" id="Seg_908" s="T85">go.away-CO-3PL</ta>
            <ta e="T87" id="Seg_909" s="T86">hare.[NOM]</ta>
            <ta e="T88" id="Seg_910" s="T87">front.part-ADVZ</ta>
            <ta e="T89" id="Seg_911" s="T88">run-PST.NAR.[3SG.S]</ta>
            <ta e="T90" id="Seg_912" s="T89">wild.animal-DIM-child-PL.[NOM]</ta>
            <ta e="T91" id="Seg_913" s="T90">be.frightened-DRV-CVB</ta>
            <ta e="T92" id="Seg_914" s="T91">forest-ILL</ta>
            <ta e="T93" id="Seg_915" s="T92">run-MOM-CO-3PL</ta>
            <ta e="T94" id="Seg_916" s="T93">bear.[NOM]</ta>
            <ta e="T95" id="Seg_917" s="T94">big</ta>
            <ta e="T96" id="Seg_918" s="T95">be-CO.[3SG.S]</ta>
            <ta e="T97" id="Seg_919" s="T96">(s)he.[NOM]</ta>
            <ta e="T98" id="Seg_920" s="T97">far-ADV.ILL</ta>
            <ta e="T99" id="Seg_921" s="T98">be.visible.[3SG.S]</ta>
            <ta e="T100" id="Seg_922" s="T99">hare.[NOM]</ta>
            <ta e="T101" id="Seg_923" s="T100">say-PST.NAR.[3SG.S]</ta>
            <ta e="T102" id="Seg_924" s="T101">say-PST-3SG.O</ta>
            <ta e="T103" id="Seg_925" s="T102">tell-PST.NAR.[3SG.S]</ta>
            <ta e="T104" id="Seg_926" s="T103">aha</ta>
            <ta e="T105" id="Seg_927" s="T104">give.a.look-DUR-IMP.2SG.O</ta>
            <ta e="T106" id="Seg_928" s="T105">I.NOM</ta>
            <ta e="T107" id="Seg_929" s="T106">front.part-ADVZ</ta>
            <ta e="T108" id="Seg_930" s="T107">run-PST.NAR-1SG.S</ta>
            <ta e="T109" id="Seg_931" s="T108">I.GEN</ta>
            <ta e="T110" id="Seg_932" s="T109">out-ADV.EL</ta>
            <ta e="T111" id="Seg_933" s="T110">towards</ta>
            <ta e="T112" id="Seg_934" s="T111">come-IPFV-PTCP.PRS</ta>
            <ta e="T113" id="Seg_935" s="T112">all</ta>
            <ta e="T114" id="Seg_936" s="T113">wild.animal.[NOM]</ta>
            <ta e="T115" id="Seg_937" s="T114">be.frightened-DRV-CO-3PL</ta>
            <ta e="T116" id="Seg_938" s="T115">and</ta>
            <ta e="T117" id="Seg_939" s="T116">forest-ILL</ta>
            <ta e="T118" id="Seg_940" s="T117">run-MOM-CO-3PL</ta>
            <ta e="T119" id="Seg_941" s="T118">bear.[NOM]</ta>
            <ta e="T120" id="Seg_942" s="T119">surprise-DRV-CO.[3SG.S]</ta>
            <ta e="T121" id="Seg_943" s="T120">say-CO-3SG.O</ta>
            <ta e="T122" id="Seg_944" s="T121">oneself.3SG-EP-ADJZ</ta>
            <ta e="T123" id="Seg_945" s="T122">grandson.[NOM]</ta>
            <ta e="T124" id="Seg_946" s="T123">you.SG.GEN</ta>
            <ta e="T125" id="Seg_947" s="T124">out-ADV.EL</ta>
            <ta e="T126" id="Seg_948" s="T125">all</ta>
            <ta e="T127" id="Seg_949" s="T126">be.afraid-CO-3PL</ta>
            <ta e="T128" id="Seg_950" s="T127">I.NOM</ta>
            <ta e="T129" id="Seg_951" s="T128">oneself.1SG</ta>
            <ta e="T130" id="Seg_952" s="T129">see-IPFV-FRQ-PST-1SG.O</ta>
            <ta e="T131" id="Seg_953" s="T130">you.SG.NOM</ta>
            <ta e="T132" id="Seg_954" s="T131">front.part-ADVZ</ta>
            <ta e="T133" id="Seg_955" s="T132">go-PST.NAR-2SG.O</ta>
            <ta e="T134" id="Seg_956" s="T133">you.SG.NOM</ta>
            <ta e="T135" id="Seg_957" s="T134">oneself.2SG-EP-ADJZ</ta>
            <ta e="T136" id="Seg_958" s="T135">force-INSTR</ta>
            <ta e="T137" id="Seg_959" s="T136">be-CO-2SG.S</ta>
            <ta e="T138" id="Seg_960" s="T137">bear.[NOM]</ta>
            <ta e="T139" id="Seg_961" s="T138">be.frightened-DRV-CO.[3SG.S]</ta>
            <ta e="T140" id="Seg_962" s="T139">hurry-ATTEN-CVB</ta>
            <ta e="T141" id="Seg_963" s="T140">forest-ILL</ta>
            <ta e="T142" id="Seg_964" s="T141">run.[3SG.S]</ta>
            <ta e="T143" id="Seg_965" s="T142">hare-DIM.[NOM]</ta>
            <ta e="T144" id="Seg_966" s="T143">force-INSTR</ta>
            <ta e="T145" id="Seg_967" s="T144">laugh-US-CO.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T44" id="Seg_968" s="T43">сказка.[NOM]</ta>
            <ta e="T46" id="Seg_969" s="T45">медведь.[NOM]</ta>
            <ta e="T47" id="Seg_970" s="T46">и</ta>
            <ta e="T48" id="Seg_971" s="T47">заяц.[NOM]</ta>
            <ta e="T49" id="Seg_972" s="T48">медведь.[NOM]</ta>
            <ta e="T50" id="Seg_973" s="T49">заяц-ACC</ta>
            <ta e="T51" id="Seg_974" s="T50">видеть-CO-3SG.O</ta>
            <ta e="T52" id="Seg_975" s="T51">я.NOM</ta>
            <ta e="T53" id="Seg_976" s="T52">ты.ACC</ta>
            <ta e="T54" id="Seg_977" s="T53">съесть-FUT-1SG.O</ta>
            <ta e="T55" id="Seg_978" s="T54">заяц.[NOM]</ta>
            <ta e="T56" id="Seg_979" s="T55">сказать-PST-3SG.O</ta>
            <ta e="T57" id="Seg_980" s="T56">я.NOM</ta>
            <ta e="T58" id="Seg_981" s="T57">ты.GEN</ta>
            <ta e="T59" id="Seg_982" s="T58">по.сравнению.с-ADV.EL</ta>
            <ta e="T60" id="Seg_983" s="T59">сила-INSTR</ta>
            <ta e="T61" id="Seg_984" s="T60">быть-CO-1SG.S</ta>
            <ta e="T62" id="Seg_985" s="T61">как</ta>
            <ta e="T63" id="Seg_986" s="T62">я.ACC</ta>
            <ta e="T64" id="Seg_987" s="T63">съесть-FUT-2SG.O</ta>
            <ta e="T65" id="Seg_988" s="T64">медведь.[NOM]</ta>
            <ta e="T66" id="Seg_989" s="T65">удивить-DRV-CO.[3SG.S]</ta>
            <ta e="T67" id="Seg_990" s="T66">сказать-CO-3SG</ta>
            <ta e="T68" id="Seg_991" s="T67">ты.NOM</ta>
            <ta e="T69" id="Seg_992" s="T68">же</ta>
            <ta e="T70" id="Seg_993" s="T69">маленький</ta>
            <ta e="T71" id="Seg_994" s="T70">быть-CO-2SG.S</ta>
            <ta e="T72" id="Seg_995" s="T71">взглянуть-DUR-OPT-1PL</ta>
            <ta e="T73" id="Seg_996" s="T72">ну_ка</ta>
            <ta e="T74" id="Seg_997" s="T73">уйти-OPT-1DU</ta>
            <ta e="T75" id="Seg_998" s="T74">кто-GEN</ta>
            <ta e="T76" id="Seg_999" s="T75">из-ADV.EL</ta>
            <ta e="T77" id="Seg_1000" s="T76">зверь-DIM-ребенок-PL.[NOM]</ta>
            <ta e="T78" id="Seg_1001" s="T77">испугаться-DRV-FUT-3PL</ta>
            <ta e="T79" id="Seg_1002" s="T78">этот</ta>
            <ta e="T80" id="Seg_1003" s="T79">сила-INSTR</ta>
            <ta e="T81" id="Seg_1004" s="T80">быть-CO.[3SG.S]</ta>
            <ta e="T82" id="Seg_1005" s="T81">он(а)-EP-PL</ta>
            <ta e="T83" id="Seg_1006" s="T82">дорога.[NOM]</ta>
            <ta e="T84" id="Seg_1007" s="T83">дорога.[NOM]</ta>
            <ta e="T85" id="Seg_1008" s="T84">сквозь</ta>
            <ta e="T86" id="Seg_1009" s="T85">уйти-CO-3PL</ta>
            <ta e="T87" id="Seg_1010" s="T86">заяц.[NOM]</ta>
            <ta e="T88" id="Seg_1011" s="T87">перед-ADVZ</ta>
            <ta e="T89" id="Seg_1012" s="T88">бегать-PST.NAR.[3SG.S]</ta>
            <ta e="T90" id="Seg_1013" s="T89">зверь-DIM-ребенок-PL.[NOM]</ta>
            <ta e="T91" id="Seg_1014" s="T90">испугаться-DRV-CVB</ta>
            <ta e="T92" id="Seg_1015" s="T91">лес-ILL</ta>
            <ta e="T93" id="Seg_1016" s="T92">бегать-MOM-CO-3PL</ta>
            <ta e="T94" id="Seg_1017" s="T93">медведь.[NOM]</ta>
            <ta e="T95" id="Seg_1018" s="T94">большой</ta>
            <ta e="T96" id="Seg_1019" s="T95">быть-CO.[3SG.S]</ta>
            <ta e="T97" id="Seg_1020" s="T96">он(а).[NOM]</ta>
            <ta e="T98" id="Seg_1021" s="T97">далеко-ADV.ILL</ta>
            <ta e="T99" id="Seg_1022" s="T98">виднеться.[3SG.S]</ta>
            <ta e="T100" id="Seg_1023" s="T99">заяц.[NOM]</ta>
            <ta e="T101" id="Seg_1024" s="T100">сказать-PST.NAR.[3SG.S]</ta>
            <ta e="T102" id="Seg_1025" s="T101">сказать-PST-3SG.O</ta>
            <ta e="T103" id="Seg_1026" s="T102">сказать-PST.NAR.[3SG.S]</ta>
            <ta e="T104" id="Seg_1027" s="T103">ага</ta>
            <ta e="T105" id="Seg_1028" s="T104">взглянуть-DUR-IMP.2SG.O</ta>
            <ta e="T106" id="Seg_1029" s="T105">я.NOM</ta>
            <ta e="T107" id="Seg_1030" s="T106">перед-ADVZ</ta>
            <ta e="T108" id="Seg_1031" s="T107">бегать-PST.NAR-1SG.S</ta>
            <ta e="T109" id="Seg_1032" s="T108">я.GEN</ta>
            <ta e="T110" id="Seg_1033" s="T109">из-ADV.EL</ta>
            <ta e="T111" id="Seg_1034" s="T110">навстречу</ta>
            <ta e="T112" id="Seg_1035" s="T111">прийти-IPFV-PTCP.PRS</ta>
            <ta e="T113" id="Seg_1036" s="T112">всё</ta>
            <ta e="T114" id="Seg_1037" s="T113">зверь.[NOM]</ta>
            <ta e="T115" id="Seg_1038" s="T114">испугаться-DRV-CO-3PL</ta>
            <ta e="T116" id="Seg_1039" s="T115">и</ta>
            <ta e="T117" id="Seg_1040" s="T116">лес-ILL</ta>
            <ta e="T118" id="Seg_1041" s="T117">бегать-MOM-CO-3PL</ta>
            <ta e="T119" id="Seg_1042" s="T118">медведь.[NOM]</ta>
            <ta e="T120" id="Seg_1043" s="T119">удивить-DRV-CO.[3SG.S]</ta>
            <ta e="T121" id="Seg_1044" s="T120">сказать-CO-3SG.O</ta>
            <ta e="T122" id="Seg_1045" s="T121">сам.3SG-EP-ADJZ</ta>
            <ta e="T123" id="Seg_1046" s="T122">внук.[NOM]</ta>
            <ta e="T124" id="Seg_1047" s="T123">ты.GEN</ta>
            <ta e="T125" id="Seg_1048" s="T124">из-ADV.EL</ta>
            <ta e="T126" id="Seg_1049" s="T125">всё</ta>
            <ta e="T127" id="Seg_1050" s="T126">бояться-CO-3PL</ta>
            <ta e="T128" id="Seg_1051" s="T127">я.NOM</ta>
            <ta e="T129" id="Seg_1052" s="T128">сам.1SG</ta>
            <ta e="T130" id="Seg_1053" s="T129">видеть-IPFV-FRQ-PST-1SG.O</ta>
            <ta e="T131" id="Seg_1054" s="T130">ты.NOM</ta>
            <ta e="T132" id="Seg_1055" s="T131">перед-ADVZ</ta>
            <ta e="T133" id="Seg_1056" s="T132">ходить-PST.NAR-2SG.O</ta>
            <ta e="T134" id="Seg_1057" s="T133">ты.NOM</ta>
            <ta e="T135" id="Seg_1058" s="T134">сам.2SG-EP-ADJZ</ta>
            <ta e="T136" id="Seg_1059" s="T135">сила-INSTR</ta>
            <ta e="T137" id="Seg_1060" s="T136">быть-CO-2SG.S</ta>
            <ta e="T138" id="Seg_1061" s="T137">медведь.[NOM]</ta>
            <ta e="T139" id="Seg_1062" s="T138">испугаться-DRV-CO.[3SG.S]</ta>
            <ta e="T140" id="Seg_1063" s="T139">торопиться-ATTEN-CVB</ta>
            <ta e="T141" id="Seg_1064" s="T140">лес-ILL</ta>
            <ta e="T142" id="Seg_1065" s="T141">побежать.[3SG.S]</ta>
            <ta e="T143" id="Seg_1066" s="T142">заяц-DIM.[NOM]</ta>
            <ta e="T144" id="Seg_1067" s="T143">сила-INSTR</ta>
            <ta e="T145" id="Seg_1068" s="T144">смеяться-US-CO.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T44" id="Seg_1069" s="T43">n-n:case</ta>
            <ta e="T46" id="Seg_1070" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_1071" s="T46">conj</ta>
            <ta e="T48" id="Seg_1072" s="T47">n-n:case</ta>
            <ta e="T49" id="Seg_1073" s="T48">n-n:case</ta>
            <ta e="T50" id="Seg_1074" s="T49">n-n:case</ta>
            <ta e="T51" id="Seg_1075" s="T50">v-v:tense-v:pn</ta>
            <ta e="T52" id="Seg_1076" s="T51">pers</ta>
            <ta e="T53" id="Seg_1077" s="T52">pers</ta>
            <ta e="T54" id="Seg_1078" s="T53">v-v:tense-v:pn</ta>
            <ta e="T55" id="Seg_1079" s="T54">n-n:case</ta>
            <ta e="T56" id="Seg_1080" s="T55">v-v:tense-v:pn</ta>
            <ta e="T57" id="Seg_1081" s="T56">pers</ta>
            <ta e="T58" id="Seg_1082" s="T57">pers</ta>
            <ta e="T59" id="Seg_1083" s="T58">pp-adv:case</ta>
            <ta e="T60" id="Seg_1084" s="T59">n-n:case</ta>
            <ta e="T61" id="Seg_1085" s="T60">v-v:tense-v:pn</ta>
            <ta e="T62" id="Seg_1086" s="T61">interrog</ta>
            <ta e="T63" id="Seg_1087" s="T62">pers</ta>
            <ta e="T64" id="Seg_1088" s="T63">v-v:tense-v:pn</ta>
            <ta e="T65" id="Seg_1089" s="T64">n-n:case</ta>
            <ta e="T66" id="Seg_1090" s="T65">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T67" id="Seg_1091" s="T66">v-v:tense-v:pn</ta>
            <ta e="T68" id="Seg_1092" s="T67">pers</ta>
            <ta e="T69" id="Seg_1093" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_1094" s="T69">adj</ta>
            <ta e="T71" id="Seg_1095" s="T70">v-v:tense-v:pn</ta>
            <ta e="T72" id="Seg_1096" s="T71">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T73" id="Seg_1097" s="T72">ptcl</ta>
            <ta e="T74" id="Seg_1098" s="T73">v-v:mood-v:pn</ta>
            <ta e="T75" id="Seg_1099" s="T74">interrog-n:case</ta>
            <ta e="T76" id="Seg_1100" s="T75">pp-adv:case</ta>
            <ta e="T77" id="Seg_1101" s="T76">n-n&gt;n-n-n:num-n:case</ta>
            <ta e="T78" id="Seg_1102" s="T77">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T79" id="Seg_1103" s="T78">dem</ta>
            <ta e="T80" id="Seg_1104" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_1105" s="T80">v-v:tense-v:pn</ta>
            <ta e="T82" id="Seg_1106" s="T81">pers-n:ins-n:num</ta>
            <ta e="T83" id="Seg_1107" s="T82">n-n:case</ta>
            <ta e="T84" id="Seg_1108" s="T83">n-n:case</ta>
            <ta e="T85" id="Seg_1109" s="T84">pp</ta>
            <ta e="T86" id="Seg_1110" s="T85">v-v:tense-v:pn</ta>
            <ta e="T87" id="Seg_1111" s="T86">n-n:case</ta>
            <ta e="T88" id="Seg_1112" s="T87">n-n&gt;adv</ta>
            <ta e="T89" id="Seg_1113" s="T88">v-v:tense-v:pn</ta>
            <ta e="T90" id="Seg_1114" s="T89">n-n&gt;n-n-n:num-n:case</ta>
            <ta e="T91" id="Seg_1115" s="T90">v-v&gt;v-v&gt;adv</ta>
            <ta e="T92" id="Seg_1116" s="T91">n-n:case</ta>
            <ta e="T93" id="Seg_1117" s="T92">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T94" id="Seg_1118" s="T93">n-n:case</ta>
            <ta e="T95" id="Seg_1119" s="T94">adj</ta>
            <ta e="T96" id="Seg_1120" s="T95">v-v:tense-v:pn</ta>
            <ta e="T97" id="Seg_1121" s="T96">pers-n:case</ta>
            <ta e="T98" id="Seg_1122" s="T97">adv-adv:case</ta>
            <ta e="T99" id="Seg_1123" s="T98">v-v:pn</ta>
            <ta e="T100" id="Seg_1124" s="T99">n-n:case</ta>
            <ta e="T101" id="Seg_1125" s="T100">v-v:tense-v:pn</ta>
            <ta e="T102" id="Seg_1126" s="T101">v-v:tense-v:pn</ta>
            <ta e="T103" id="Seg_1127" s="T102">v-v:tense-v:pn</ta>
            <ta e="T104" id="Seg_1128" s="T103">interj</ta>
            <ta e="T105" id="Seg_1129" s="T104">v-v&gt;v-v:mood.pn</ta>
            <ta e="T106" id="Seg_1130" s="T105">pers</ta>
            <ta e="T107" id="Seg_1131" s="T106">n-n&gt;adv</ta>
            <ta e="T108" id="Seg_1132" s="T107">v-v:tense-v:pn</ta>
            <ta e="T109" id="Seg_1133" s="T108">pers</ta>
            <ta e="T110" id="Seg_1134" s="T109">pp-adv:case</ta>
            <ta e="T111" id="Seg_1135" s="T110">adv</ta>
            <ta e="T112" id="Seg_1136" s="T111">v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T113" id="Seg_1137" s="T112">quant</ta>
            <ta e="T114" id="Seg_1138" s="T113">n-n:case</ta>
            <ta e="T115" id="Seg_1139" s="T114">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T116" id="Seg_1140" s="T115">conj</ta>
            <ta e="T117" id="Seg_1141" s="T116">n-n:case</ta>
            <ta e="T118" id="Seg_1142" s="T117">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T119" id="Seg_1143" s="T118">n-n:case</ta>
            <ta e="T120" id="Seg_1144" s="T119">v-v&gt;v-v:pn-v:tense-v:pn</ta>
            <ta e="T121" id="Seg_1145" s="T120">v-v:tense-v:pn</ta>
            <ta e="T122" id="Seg_1146" s="T121">emphpro-n:ins-n&gt;adj</ta>
            <ta e="T123" id="Seg_1147" s="T122">n-n:case</ta>
            <ta e="T124" id="Seg_1148" s="T123">pers</ta>
            <ta e="T125" id="Seg_1149" s="T124">pp-n&gt;adv</ta>
            <ta e="T126" id="Seg_1150" s="T125">quant</ta>
            <ta e="T127" id="Seg_1151" s="T126">v-v:tense-v:pn</ta>
            <ta e="T128" id="Seg_1152" s="T127">pers</ta>
            <ta e="T129" id="Seg_1153" s="T128">emphpro</ta>
            <ta e="T130" id="Seg_1154" s="T129">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T131" id="Seg_1155" s="T130">pers</ta>
            <ta e="T132" id="Seg_1156" s="T131">n-n&gt;adv</ta>
            <ta e="T133" id="Seg_1157" s="T132">v-v:tense-v:pn</ta>
            <ta e="T134" id="Seg_1158" s="T133">pers</ta>
            <ta e="T135" id="Seg_1159" s="T134">emphpro-n:ins-n&gt;adj</ta>
            <ta e="T136" id="Seg_1160" s="T135">n-n:case</ta>
            <ta e="T137" id="Seg_1161" s="T136">v-v:tense-v:pn</ta>
            <ta e="T138" id="Seg_1162" s="T137">n-n:case</ta>
            <ta e="T139" id="Seg_1163" s="T138">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T140" id="Seg_1164" s="T139">v-v&gt;v-v&gt;adv</ta>
            <ta e="T141" id="Seg_1165" s="T140">n-n:case</ta>
            <ta e="T142" id="Seg_1166" s="T141">v-v:pn</ta>
            <ta e="T143" id="Seg_1167" s="T142">n-n&gt;n-n:case</ta>
            <ta e="T144" id="Seg_1168" s="T143">n-n:case</ta>
            <ta e="T145" id="Seg_1169" s="T144">v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T44" id="Seg_1170" s="T43">n</ta>
            <ta e="T46" id="Seg_1171" s="T45">n</ta>
            <ta e="T47" id="Seg_1172" s="T46">conj</ta>
            <ta e="T48" id="Seg_1173" s="T47">n</ta>
            <ta e="T49" id="Seg_1174" s="T48">n</ta>
            <ta e="T50" id="Seg_1175" s="T49">n</ta>
            <ta e="T51" id="Seg_1176" s="T50">v</ta>
            <ta e="T52" id="Seg_1177" s="T51">pers</ta>
            <ta e="T53" id="Seg_1178" s="T52">pers</ta>
            <ta e="T54" id="Seg_1179" s="T53">v</ta>
            <ta e="T55" id="Seg_1180" s="T54">n</ta>
            <ta e="T56" id="Seg_1181" s="T55">v</ta>
            <ta e="T57" id="Seg_1182" s="T56">pers</ta>
            <ta e="T58" id="Seg_1183" s="T57">pers</ta>
            <ta e="T59" id="Seg_1184" s="T58">pp</ta>
            <ta e="T60" id="Seg_1185" s="T59">n</ta>
            <ta e="T61" id="Seg_1186" s="T60">v</ta>
            <ta e="T62" id="Seg_1187" s="T61">interrog</ta>
            <ta e="T63" id="Seg_1188" s="T62">pers</ta>
            <ta e="T64" id="Seg_1189" s="T63">v</ta>
            <ta e="T65" id="Seg_1190" s="T64">n</ta>
            <ta e="T66" id="Seg_1191" s="T65">v</ta>
            <ta e="T67" id="Seg_1192" s="T66">v</ta>
            <ta e="T68" id="Seg_1193" s="T67">pers</ta>
            <ta e="T69" id="Seg_1194" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_1195" s="T69">adj</ta>
            <ta e="T71" id="Seg_1196" s="T70">v</ta>
            <ta e="T72" id="Seg_1197" s="T71">v</ta>
            <ta e="T73" id="Seg_1198" s="T72">ptcl</ta>
            <ta e="T74" id="Seg_1199" s="T73">v</ta>
            <ta e="T75" id="Seg_1200" s="T74">interrog</ta>
            <ta e="T76" id="Seg_1201" s="T75">pp</ta>
            <ta e="T77" id="Seg_1202" s="T76">n</ta>
            <ta e="T78" id="Seg_1203" s="T77">v</ta>
            <ta e="T79" id="Seg_1204" s="T78">dem</ta>
            <ta e="T80" id="Seg_1205" s="T79">n</ta>
            <ta e="T81" id="Seg_1206" s="T80">v</ta>
            <ta e="T82" id="Seg_1207" s="T81">pers</ta>
            <ta e="T83" id="Seg_1208" s="T82">n</ta>
            <ta e="T84" id="Seg_1209" s="T83">n</ta>
            <ta e="T85" id="Seg_1210" s="T84">pp</ta>
            <ta e="T86" id="Seg_1211" s="T85">v</ta>
            <ta e="T87" id="Seg_1212" s="T86">n</ta>
            <ta e="T88" id="Seg_1213" s="T87">adv</ta>
            <ta e="T89" id="Seg_1214" s="T88">v</ta>
            <ta e="T90" id="Seg_1215" s="T89">n</ta>
            <ta e="T91" id="Seg_1216" s="T90">adv</ta>
            <ta e="T92" id="Seg_1217" s="T91">n</ta>
            <ta e="T93" id="Seg_1218" s="T92">v</ta>
            <ta e="T94" id="Seg_1219" s="T93">n</ta>
            <ta e="T95" id="Seg_1220" s="T94">adj</ta>
            <ta e="T96" id="Seg_1221" s="T95">v</ta>
            <ta e="T97" id="Seg_1222" s="T96">pers</ta>
            <ta e="T98" id="Seg_1223" s="T97">adv</ta>
            <ta e="T99" id="Seg_1224" s="T98">v</ta>
            <ta e="T100" id="Seg_1225" s="T99">n</ta>
            <ta e="T101" id="Seg_1226" s="T100">v</ta>
            <ta e="T102" id="Seg_1227" s="T101">v</ta>
            <ta e="T103" id="Seg_1228" s="T102">v</ta>
            <ta e="T104" id="Seg_1229" s="T103">interj</ta>
            <ta e="T105" id="Seg_1230" s="T104">v</ta>
            <ta e="T106" id="Seg_1231" s="T105">pers</ta>
            <ta e="T107" id="Seg_1232" s="T106">adv</ta>
            <ta e="T108" id="Seg_1233" s="T107">v</ta>
            <ta e="T109" id="Seg_1234" s="T108">pers</ta>
            <ta e="T110" id="Seg_1235" s="T109">pp</ta>
            <ta e="T111" id="Seg_1236" s="T110">adv</ta>
            <ta e="T112" id="Seg_1237" s="T111">ptcp</ta>
            <ta e="T113" id="Seg_1238" s="T112">quant</ta>
            <ta e="T114" id="Seg_1239" s="T113">n</ta>
            <ta e="T115" id="Seg_1240" s="T114">v</ta>
            <ta e="T116" id="Seg_1241" s="T115">conj</ta>
            <ta e="T117" id="Seg_1242" s="T116">n</ta>
            <ta e="T118" id="Seg_1243" s="T117">v</ta>
            <ta e="T119" id="Seg_1244" s="T118">n</ta>
            <ta e="T120" id="Seg_1245" s="T119">v</ta>
            <ta e="T121" id="Seg_1246" s="T120">v</ta>
            <ta e="T122" id="Seg_1247" s="T121">emphpro</ta>
            <ta e="T123" id="Seg_1248" s="T122">n</ta>
            <ta e="T124" id="Seg_1249" s="T123">pers</ta>
            <ta e="T125" id="Seg_1250" s="T124">adv</ta>
            <ta e="T126" id="Seg_1251" s="T125">quant</ta>
            <ta e="T127" id="Seg_1252" s="T126">v</ta>
            <ta e="T128" id="Seg_1253" s="T127">pers</ta>
            <ta e="T129" id="Seg_1254" s="T128">emphpro</ta>
            <ta e="T130" id="Seg_1255" s="T129">v</ta>
            <ta e="T131" id="Seg_1256" s="T130">pers</ta>
            <ta e="T132" id="Seg_1257" s="T131">adv</ta>
            <ta e="T133" id="Seg_1258" s="T132">v</ta>
            <ta e="T134" id="Seg_1259" s="T133">pers</ta>
            <ta e="T135" id="Seg_1260" s="T134">emphpro</ta>
            <ta e="T136" id="Seg_1261" s="T135">n</ta>
            <ta e="T137" id="Seg_1262" s="T136">v</ta>
            <ta e="T138" id="Seg_1263" s="T137">n</ta>
            <ta e="T139" id="Seg_1264" s="T138">v</ta>
            <ta e="T140" id="Seg_1265" s="T139">adv</ta>
            <ta e="T141" id="Seg_1266" s="T140">n</ta>
            <ta e="T142" id="Seg_1267" s="T141">v</ta>
            <ta e="T143" id="Seg_1268" s="T142">n</ta>
            <ta e="T144" id="Seg_1269" s="T143">n</ta>
            <ta e="T145" id="Seg_1270" s="T144">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T49" id="Seg_1271" s="T48">np.h:E</ta>
            <ta e="T50" id="Seg_1272" s="T49">np.h:Th</ta>
            <ta e="T52" id="Seg_1273" s="T51">pro.h:A</ta>
            <ta e="T53" id="Seg_1274" s="T52">pro.h:P</ta>
            <ta e="T55" id="Seg_1275" s="T54">np.h:A</ta>
            <ta e="T57" id="Seg_1276" s="T56">pro.h:Th</ta>
            <ta e="T60" id="Seg_1277" s="T59">np:Ins</ta>
            <ta e="T63" id="Seg_1278" s="T62">pro.h:P</ta>
            <ta e="T64" id="Seg_1279" s="T63">0.2.h:A</ta>
            <ta e="T65" id="Seg_1280" s="T64">np.h:E</ta>
            <ta e="T67" id="Seg_1281" s="T66">0.3.h:A</ta>
            <ta e="T68" id="Seg_1282" s="T67">pro.h:Th</ta>
            <ta e="T72" id="Seg_1283" s="T71">0.1.h:A</ta>
            <ta e="T74" id="Seg_1284" s="T73">0.1.h:A</ta>
            <ta e="T77" id="Seg_1285" s="T76">np:E</ta>
            <ta e="T79" id="Seg_1286" s="T78">pro.h:Th</ta>
            <ta e="T80" id="Seg_1287" s="T79">np:Ins</ta>
            <ta e="T82" id="Seg_1288" s="T81">pro.h:A</ta>
            <ta e="T84" id="Seg_1289" s="T83">pp:Path</ta>
            <ta e="T87" id="Seg_1290" s="T86">np.h:A</ta>
            <ta e="T88" id="Seg_1291" s="T87">adv:L</ta>
            <ta e="T90" id="Seg_1292" s="T89">np:A</ta>
            <ta e="T92" id="Seg_1293" s="T91">np:G</ta>
            <ta e="T94" id="Seg_1294" s="T93">np.h:Th</ta>
            <ta e="T97" id="Seg_1295" s="T96">pro.h:Th</ta>
            <ta e="T98" id="Seg_1296" s="T97">adv:L</ta>
            <ta e="T100" id="Seg_1297" s="T99">np.h:A</ta>
            <ta e="T105" id="Seg_1298" s="T104">0.2.h:A</ta>
            <ta e="T106" id="Seg_1299" s="T105">pro.h:A</ta>
            <ta e="T107" id="Seg_1300" s="T106">adv:L</ta>
            <ta e="T111" id="Seg_1301" s="T110">adv:Path</ta>
            <ta e="T114" id="Seg_1302" s="T113">np:E</ta>
            <ta e="T117" id="Seg_1303" s="T116">np:G</ta>
            <ta e="T118" id="Seg_1304" s="T117">0.3:A</ta>
            <ta e="T119" id="Seg_1305" s="T118">np.h:E</ta>
            <ta e="T121" id="Seg_1306" s="T120">0.3.h:A</ta>
            <ta e="T123" id="Seg_1307" s="T122">np:E</ta>
            <ta e="T126" id="Seg_1308" s="T125">pro:E</ta>
            <ta e="T128" id="Seg_1309" s="T127">pro.h:E</ta>
            <ta e="T131" id="Seg_1310" s="T130">pro.h:A</ta>
            <ta e="T132" id="Seg_1311" s="T131">adv:L</ta>
            <ta e="T134" id="Seg_1312" s="T133">pro.h:Th</ta>
            <ta e="T136" id="Seg_1313" s="T135">np:Ins</ta>
            <ta e="T138" id="Seg_1314" s="T137">np.h:E</ta>
            <ta e="T141" id="Seg_1315" s="T140">np:G</ta>
            <ta e="T142" id="Seg_1316" s="T141">0.3.h:A</ta>
            <ta e="T143" id="Seg_1317" s="T142">np.h:A</ta>
            <ta e="T144" id="Seg_1318" s="T143">np:Ins</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T49" id="Seg_1319" s="T48">np.h:S</ta>
            <ta e="T50" id="Seg_1320" s="T49">np.h:O</ta>
            <ta e="T51" id="Seg_1321" s="T50">v:pred</ta>
            <ta e="T52" id="Seg_1322" s="T51">pro.h:S</ta>
            <ta e="T53" id="Seg_1323" s="T52">pro.h:O</ta>
            <ta e="T54" id="Seg_1324" s="T53">v:pred</ta>
            <ta e="T55" id="Seg_1325" s="T54">np.h:S</ta>
            <ta e="T56" id="Seg_1326" s="T55">v:pred</ta>
            <ta e="T57" id="Seg_1327" s="T56">pro.h:S</ta>
            <ta e="T60" id="Seg_1328" s="T59">n:pred</ta>
            <ta e="T61" id="Seg_1329" s="T60">cop</ta>
            <ta e="T63" id="Seg_1330" s="T62">pro.h:O</ta>
            <ta e="T64" id="Seg_1331" s="T63">0.2.h:S v:pred</ta>
            <ta e="T65" id="Seg_1332" s="T64">np.h:S</ta>
            <ta e="T66" id="Seg_1333" s="T65">v:pred</ta>
            <ta e="T67" id="Seg_1334" s="T66">0.3.h:S v:pred</ta>
            <ta e="T68" id="Seg_1335" s="T67">pro.h:S</ta>
            <ta e="T70" id="Seg_1336" s="T69">adj:pred</ta>
            <ta e="T71" id="Seg_1337" s="T70">cop</ta>
            <ta e="T72" id="Seg_1338" s="T71">0.1.h:S v:pred</ta>
            <ta e="T74" id="Seg_1339" s="T73">0.1.h:S v:pred</ta>
            <ta e="T78" id="Seg_1340" s="T74">s:rel</ta>
            <ta e="T79" id="Seg_1341" s="T78">pro.h:S</ta>
            <ta e="T80" id="Seg_1342" s="T79">n:pred</ta>
            <ta e="T81" id="Seg_1343" s="T80">cop</ta>
            <ta e="T82" id="Seg_1344" s="T81">pro.h:S</ta>
            <ta e="T86" id="Seg_1345" s="T85">v:pred</ta>
            <ta e="T87" id="Seg_1346" s="T86">np.h:S</ta>
            <ta e="T89" id="Seg_1347" s="T88">v:pred</ta>
            <ta e="T90" id="Seg_1348" s="T89">np:S</ta>
            <ta e="T91" id="Seg_1349" s="T90">s:adv</ta>
            <ta e="T93" id="Seg_1350" s="T92">v:pred</ta>
            <ta e="T94" id="Seg_1351" s="T93">np.h:S</ta>
            <ta e="T95" id="Seg_1352" s="T94">adj:pred</ta>
            <ta e="T96" id="Seg_1353" s="T95">cop</ta>
            <ta e="T97" id="Seg_1354" s="T96">pro.h:S</ta>
            <ta e="T99" id="Seg_1355" s="T98">v:pred</ta>
            <ta e="T100" id="Seg_1356" s="T99">np.h:S</ta>
            <ta e="T101" id="Seg_1357" s="T100">v:pred</ta>
            <ta e="T102" id="Seg_1358" s="T101">v:pred</ta>
            <ta e="T103" id="Seg_1359" s="T102">v:pred</ta>
            <ta e="T105" id="Seg_1360" s="T104">0.2.h:S v:pred</ta>
            <ta e="T106" id="Seg_1361" s="T105">pro.h:S</ta>
            <ta e="T108" id="Seg_1362" s="T107">v:pred</ta>
            <ta e="T114" id="Seg_1363" s="T113">np:S</ta>
            <ta e="T115" id="Seg_1364" s="T114">v:pred</ta>
            <ta e="T118" id="Seg_1365" s="T117">0.3:S v:pred</ta>
            <ta e="T119" id="Seg_1366" s="T118">np.h:S</ta>
            <ta e="T120" id="Seg_1367" s="T119">v:pred</ta>
            <ta e="T121" id="Seg_1368" s="T120">0.3.h:S v:pred</ta>
            <ta e="T123" id="Seg_1369" s="T122">np:S</ta>
            <ta e="T126" id="Seg_1370" s="T125">pro:S</ta>
            <ta e="T127" id="Seg_1371" s="T126">v:pred</ta>
            <ta e="T128" id="Seg_1372" s="T127">pro.h:S</ta>
            <ta e="T130" id="Seg_1373" s="T129">v:pred</ta>
            <ta e="T131" id="Seg_1374" s="T130">pro.h:S</ta>
            <ta e="T133" id="Seg_1375" s="T132">v:pred</ta>
            <ta e="T134" id="Seg_1376" s="T133">pro.h:S</ta>
            <ta e="T136" id="Seg_1377" s="T135">n:pred</ta>
            <ta e="T137" id="Seg_1378" s="T136">cop</ta>
            <ta e="T138" id="Seg_1379" s="T137">np.h:S</ta>
            <ta e="T139" id="Seg_1380" s="T138">v:pred</ta>
            <ta e="T140" id="Seg_1381" s="T139">s:adv</ta>
            <ta e="T142" id="Seg_1382" s="T141">0.3.h:S v:pred</ta>
            <ta e="T143" id="Seg_1383" s="T142">np.h:S</ta>
            <ta e="T145" id="Seg_1384" s="T144">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T49" id="Seg_1385" s="T48">new</ta>
            <ta e="T50" id="Seg_1386" s="T49">new</ta>
            <ta e="T52" id="Seg_1387" s="T51">giv-active-Q</ta>
            <ta e="T53" id="Seg_1388" s="T52">giv-active-Q</ta>
            <ta e="T55" id="Seg_1389" s="T54">giv-active</ta>
            <ta e="T56" id="Seg_1390" s="T55">quot-sp</ta>
            <ta e="T57" id="Seg_1391" s="T56">giv-active-Q</ta>
            <ta e="T58" id="Seg_1392" s="T57">giv-active-Q</ta>
            <ta e="T63" id="Seg_1393" s="T62">giv-active-Q</ta>
            <ta e="T64" id="Seg_1394" s="T63">0.giv-active-Q</ta>
            <ta e="T65" id="Seg_1395" s="T64">giv-active</ta>
            <ta e="T67" id="Seg_1396" s="T66">quot-sp</ta>
            <ta e="T68" id="Seg_1397" s="T67">giv-active-Q</ta>
            <ta e="T72" id="Seg_1398" s="T71">giv-active-Q</ta>
            <ta e="T74" id="Seg_1399" s="T73">0.giv-active-Q</ta>
            <ta e="T77" id="Seg_1400" s="T76">accs-gen-Q</ta>
            <ta e="T82" id="Seg_1401" s="T81">giv-inactive</ta>
            <ta e="T83" id="Seg_1402" s="T82">accs-gen</ta>
            <ta e="T84" id="Seg_1403" s="T83">accs-gen</ta>
            <ta e="T87" id="Seg_1404" s="T86">giv-active</ta>
            <ta e="T90" id="Seg_1405" s="T89">giv-inactive</ta>
            <ta e="T92" id="Seg_1406" s="T91">accs-gen</ta>
            <ta e="T94" id="Seg_1407" s="T93">giv-inactive</ta>
            <ta e="T97" id="Seg_1408" s="T96">giv-active</ta>
            <ta e="T100" id="Seg_1409" s="T99">giv-inactive</ta>
            <ta e="T101" id="Seg_1410" s="T100">quot-sp</ta>
            <ta e="T102" id="Seg_1411" s="T101">quot-sp</ta>
            <ta e="T103" id="Seg_1412" s="T102">quot-sp</ta>
            <ta e="T105" id="Seg_1413" s="T104">0.giv-active-Q</ta>
            <ta e="T106" id="Seg_1414" s="T105">giv-active-Q</ta>
            <ta e="T109" id="Seg_1415" s="T108">giv-active-Q</ta>
            <ta e="T114" id="Seg_1416" s="T113">giv-inactive-Q</ta>
            <ta e="T117" id="Seg_1417" s="T116">giv-inactive-Q</ta>
            <ta e="T119" id="Seg_1418" s="T118">giv-active</ta>
            <ta e="T121" id="Seg_1419" s="T120">quot-sp</ta>
            <ta e="T123" id="Seg_1420" s="T122">new-Q</ta>
            <ta e="T124" id="Seg_1421" s="T123">giv-active-Q</ta>
            <ta e="T128" id="Seg_1422" s="T127">giv-active-Q</ta>
            <ta e="T131" id="Seg_1423" s="T130">giv-active-Q</ta>
            <ta e="T134" id="Seg_1424" s="T133">giv-active-Q</ta>
            <ta e="T138" id="Seg_1425" s="T137">giv-active</ta>
            <ta e="T141" id="Seg_1426" s="T140">giv-inactive</ta>
            <ta e="T143" id="Seg_1427" s="T142">giv-inactive</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T45" id="Seg_1428" s="T43">Сказка № 1.</ta>
            <ta e="T48" id="Seg_1429" s="T45">Медведь и заяц.</ta>
            <ta e="T51" id="Seg_1430" s="T48">Медведь увидел зайца.</ta>
            <ta e="T54" id="Seg_1431" s="T51">"Я тебя съем".</ta>
            <ta e="T61" id="Seg_1432" s="T54">Заяц сказал: "Я тебя сильнее.</ta>
            <ta e="T64" id="Seg_1433" s="T61">Как [ты] меня съешь?"</ta>
            <ta e="T71" id="Seg_1434" s="T64">Медведь удивился: "Ты же маленький!"</ta>
            <ta e="T72" id="Seg_1435" s="T71">"Посмотрим!</ta>
            <ta e="T74" id="Seg_1436" s="T72">Ну-ка пошли!</ta>
            <ta e="T81" id="Seg_1437" s="T74">Кого испугаются детёныши зверей, тот сильнее. </ta>
            <ta e="T86" id="Seg_1438" s="T81">Они идут по дороге.</ta>
            <ta e="T93" id="Seg_1439" s="T86">Заяц вперед побежал, детёныши зверей, испугавшись, в лес убежали.</ta>
            <ta e="T99" id="Seg_1440" s="T93">Медведь большой, его далеко видно.</ta>
            <ta e="T118" id="Seg_1441" s="T99">Заяц сказал: "Ага, посмотри, я впереди бежал, меня все идущие навстречу звери пугаются и в лес убегают".</ta>
            <ta e="T127" id="Seg_1442" s="T118">Медведь удивился: [Даже сам] внук, все тебя боятся.</ta>
            <ta e="T133" id="Seg_1443" s="T127">Я сам видел, ты впереди шёл.</ta>
            <ta e="T137" id="Seg_1444" s="T133">Ты сильный".</ta>
            <ta e="T142" id="Seg_1445" s="T137">Медведь испугался, скорее в лес убежал.</ta>
            <ta e="T145" id="Seg_1446" s="T142">Заяц сильно смеялся.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T45" id="Seg_1447" s="T43">Tale no.1.</ta>
            <ta e="T48" id="Seg_1448" s="T45">A bear and a hare. </ta>
            <ta e="T51" id="Seg_1449" s="T48">A bear saw a hare. </ta>
            <ta e="T54" id="Seg_1450" s="T51">"I will eat you". </ta>
            <ta e="T61" id="Seg_1451" s="T54">The hare said: "I am stronger than you. </ta>
            <ta e="T64" id="Seg_1452" s="T61">How can you eat me?" </ta>
            <ta e="T71" id="Seg_1453" s="T64">The bear got surprised: "But you are small!" </ta>
            <ta e="T72" id="Seg_1454" s="T71">"We'll see! </ta>
            <ta e="T74" id="Seg_1455" s="T72">Come on, let's go! </ta>
            <ta e="T81" id="Seg_1456" s="T74">The one by whom the baby wild animals will get frightened, is the strongest one. </ta>
            <ta e="T86" id="Seg_1457" s="T81">They walk along the road. </ta>
            <ta e="T93" id="Seg_1458" s="T86">The hare ran in front, the baby animals got frightened and ran away into the forest. </ta>
            <ta e="T99" id="Seg_1459" s="T93">The bear is big, he is visible from far away. </ta>
            <ta e="T118" id="Seg_1460" s="T99">The hare said: "Aha, look, I was in front, all the wild animals got frightened by me and ran away into the forest". </ta>
            <ta e="T127" id="Seg_1461" s="T118">The bear was surprised and said: Even the grandson himself, everybody is afraid of you. </ta>
            <ta e="T133" id="Seg_1462" s="T127">I saw it myself, you were walking in front. </ta>
            <ta e="T137" id="Seg_1463" s="T133">You are strong".</ta>
            <ta e="T142" id="Seg_1464" s="T137">The bear got scared and ran away into the forest quickly. </ta>
            <ta e="T145" id="Seg_1465" s="T142">The hare laughed a lot.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T45" id="Seg_1466" s="T43">Geschichte Nr. 1.</ta>
            <ta e="T48" id="Seg_1467" s="T45">Der Bär und der Hase.</ta>
            <ta e="T51" id="Seg_1468" s="T48">Der Bär sah den Hasen.</ta>
            <ta e="T54" id="Seg_1469" s="T51">"Ich werde dich fressen".</ta>
            <ta e="T61" id="Seg_1470" s="T54">Der Hase sagte: "Ich bin stärker als du. </ta>
            <ta e="T64" id="Seg_1471" s="T61">Wie kannst du mich fressen?"</ta>
            <ta e="T71" id="Seg_1472" s="T64">Der Bär wunderte sich: "Aber du bist klein!"</ta>
            <ta e="T72" id="Seg_1473" s="T71">"Wir werden es sehen!</ta>
            <ta e="T74" id="Seg_1474" s="T72">Komm, gehen wir!</ta>
            <ta e="T81" id="Seg_1475" s="T74">Derjenige, vor dem sich die Baby-Wildtiere erschrecken werden, ist der stärkste.</ta>
            <ta e="T86" id="Seg_1476" s="T81">Sie gingen den Weg entlang.</ta>
            <ta e="T93" id="Seg_1477" s="T86">Der Hase lief vorne, die Baby-Wildtiere erschruken und liefen in den Wald.</ta>
            <ta e="T99" id="Seg_1478" s="T93">Der Bär ist groß, er wird von weitem gesehen.</ta>
            <ta e="T118" id="Seg_1479" s="T99">Der Hase sagte: "Aha, sieh mal, Ich lief vorne, alle Wildtiere erschraken sich vor mir und liefen in den Wald.</ta>
            <ta e="T127" id="Seg_1480" s="T118">Der Bär wunderte sich und sagte: Sogar der Enkel selbst, alle haben Angst vor dir.</ta>
            <ta e="T133" id="Seg_1481" s="T127">Ich sah es selbst, du liefst vorne.</ta>
            <ta e="T137" id="Seg_1482" s="T133">Du bist stark".</ta>
            <ta e="T142" id="Seg_1483" s="T137">Der Bär bekam Angst, lief schnell weg in den Wald.</ta>
            <ta e="T145" id="Seg_1484" s="T142">Der Hase lachte sehr.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T51" id="Seg_1485" s="T48">увидел</ta>
            <ta e="T54" id="Seg_1486" s="T51">тебя съем</ta>
            <ta e="T61" id="Seg_1487" s="T54">сказал тебя сильнее</ta>
            <ta e="T64" id="Seg_1488" s="T61">меня съешь</ta>
            <ta e="T71" id="Seg_1489" s="T64">удивился</ta>
            <ta e="T72" id="Seg_1490" s="T71">посмотрим (кто сильнее)</ta>
            <ta e="T81" id="Seg_1491" s="T74">от кого испугаются тот</ta>
            <ta e="T86" id="Seg_1492" s="T81">за ним вслед</ta>
            <ta e="T93" id="Seg_1493" s="T86">вперед идет испугавшиеся в лес убежали</ta>
            <ta e="T127" id="Seg_1494" s="T118">о тебе все боятся</ta>
            <ta e="T133" id="Seg_1495" s="T127">сам видел идешь</ta>
            <ta e="T142" id="Seg_1496" s="T137">скорее</ta>
            <ta e="T145" id="Seg_1497" s="T142">смеется</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T86" id="Seg_1498" s="T81">[OSv:] "wəttɨ(t) šüː" - "along the road".</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T51" id="Seg_1499" s="T48">kə̄ныты – убил</ta>
            <ta e="T61" id="Seg_1500" s="T54">kəтыңыт – говорит</ta>
            <ta e="T145" id="Seg_1501" s="T142">′писыссӓ – смеялся; мат писишkа кыкак – я люблю смеяться. рушʼират – русские; кəлымпотын – идут; пос орсымылʼ эңак – самый сильный; мена чо̄сӓ kӓлымпа – мне навстречу идет</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
