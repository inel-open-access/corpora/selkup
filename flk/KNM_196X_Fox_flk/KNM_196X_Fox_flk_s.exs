<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KNM_196X_Fox_flk</transcription-name>
         <referenced-file url="KNM_196X_Fox_flk.wav" />
         <referenced-file url="KNM_196X_Fox_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KNM_196X_Fox_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">168</ud-information>
            <ud-information attribute-name="# HIAT:w">121</ud-information>
            <ud-information attribute-name="# e">118</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">2</ud-information>
            <ud-information attribute-name="# HIAT:u">29</ud-information>
            <ud-information attribute-name="# sc">58</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KNM">
            <abbreviation>KNM</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.013" type="appl" />
         <tli id="T2" time="0.8996" type="appl" />
         <tli id="T3" time="1.7861999999999998" type="appl" />
         <tli id="T4" time="2.6727999999999996" type="appl" />
         <tli id="T5" time="3.5593999999999997" type="appl" />
         <tli id="T6" time="4.446" type="appl" />
         <tli id="T14" time="7.6259999999999994" type="intp" />
         <tli id="T7" time="10.806" type="appl" />
         <tli id="T8" time="11.712499999999999" type="appl" />
         <tli id="T9" time="12.619" type="appl" />
         <tli id="T10" time="13.07933242452805" />
         <tli id="T11" time="13.863666666666667" type="appl" />
         <tli id="T12" time="14.661333333333333" type="appl" />
         <tli id="T13" time="15.472332201033144" />
         <tli id="T15" time="16.54" type="appl" />
         <tli id="T16" time="17.42" type="appl" />
         <tli id="T17" time="17.84" type="appl" />
         <tli id="T18" time="18.4986" type="appl" />
         <tli id="T19" time="19.1572" type="appl" />
         <tli id="T20" time="19.8158" type="appl" />
         <tli id="T21" time="20.4744" type="appl" />
         <tli id="T22" time="21.133" type="appl" />
         <tli id="T23" time="21.926" type="appl" />
         <tli id="T24" time="22.96483333333333" type="appl" />
         <tli id="T25" time="24.003666666666664" type="appl" />
         <tli id="T26" time="25.042499999999997" type="appl" />
         <tli id="T27" time="26.081333333333333" type="appl" />
         <tli id="T28" time="27.120166666666666" type="appl" />
         <tli id="T29" time="28.159" type="appl" />
         <tli id="T30" time="28.513" type="appl" />
         <tli id="T31" time="28.959600000000002" type="appl" />
         <tli id="T32" time="29.406200000000002" type="appl" />
         <tli id="T33" time="29.8528" type="appl" />
         <tli id="T34" time="30.2994" type="appl" />
         <tli id="T35" time="30.746" type="appl" />
         <tli id="T36" time="31.013" type="appl" />
         <tli id="T37" time="31.962" type="appl" />
         <tli id="T38" time="32.911" type="appl" />
         <tli id="T39" time="33.86" type="appl" />
         <tli id="T40" time="34.186" type="appl" />
         <tli id="T41" time="35.098" type="appl" />
         <tli id="T42" time="36.01" type="appl" />
         <tli id="T43" time="36.922000000000004" type="appl" />
         <tli id="T44" time="37.834" type="appl" />
         <tli id="T45" time="38.746" type="appl" />
         <tli id="T46" time="39.526" type="appl" />
         <tli id="T47" time="40.118" type="appl" />
         <tli id="T48" time="40.71" type="appl" />
         <tli id="T49" time="41.302" type="appl" />
         <tli id="T50" time="41.894" type="appl" />
         <tli id="T51" time="42.486" type="appl" />
         <tli id="T52" time="43.253" type="appl" />
         <tli id="T53" time="44.197" type="appl" />
         <tli id="T54" time="45.141" type="appl" />
         <tli id="T55" time="46.085" type="appl" />
         <tli id="T56" time="47.028999999999996" type="appl" />
         <tli id="T57" time="47.973" type="appl" />
         <tli id="T58" time="48.406" type="appl" />
         <tli id="T59" time="49.273714285714284" type="appl" />
         <tli id="T60" time="50.14142857142857" type="appl" />
         <tli id="T61" time="51.009142857142855" type="appl" />
         <tli id="T62" time="51.87685714285714" type="appl" />
         <tli id="T63" time="52.744571428571426" type="appl" />
         <tli id="T64" time="53.61228571428571" type="appl" />
         <tli id="T65" time="54.48" type="appl" />
         <tli id="T66" time="56.12" type="appl" />
         <tli id="T67" time="56.85333333333333" type="appl" />
         <tli id="T68" time="57.586666666666666" type="appl" />
         <tli id="T69" time="58.32" type="appl" />
         <tli id="T70" time="59.053333333333335" type="appl" />
         <tli id="T71" time="59.78666666666667" type="appl" />
         <tli id="T72" time="60.52" type="appl" />
         <tli id="T73" time="61.466" type="appl" />
         <tli id="T74" time="63.086" type="appl" />
         <tli id="T75" time="63.24" type="appl" />
         <tli id="T76" time="64.09333333333333" type="appl" />
         <tli id="T77" time="64.94666666666666" type="appl" />
         <tli id="T78" time="65.8" type="appl" />
         <tli id="T79" time="66.419" type="appl" />
         <tli id="T80" time="67.323" type="appl" />
         <tli id="T81" time="68.22699999999999" type="appl" />
         <tli id="T82" time="69.131" type="appl" />
         <tli id="T83" time="70.035" type="appl" />
         <tli id="T84" time="70.939" type="appl" />
         <tli id="T85" time="72.146" type="appl" />
         <tli id="T86" time="73.06866666666667" type="appl" />
         <tli id="T87" time="73.99133333333333" type="appl" />
         <tli id="T88" time="74.914" type="appl" />
         <tli id="T89" time="75.83666666666667" type="appl" />
         <tli id="T90" time="76.75933333333333" type="appl" />
         <tli id="T91" time="77.682" type="appl" />
         <tli id="T92" time="79.132" type="appl" />
         <tli id="T93" time="80.06200000000001" type="appl" />
         <tli id="T94" time="80.992" type="appl" />
         <tli id="T95" time="81.146" type="appl" />
         <tli id="T96" time="81.896" type="appl" />
         <tli id="T97" time="82.646" type="appl" />
         <tli id="T98" time="83.126" type="appl" />
         <tli id="T99" time="84.393" type="appl" />
         <tli id="T100" time="85.219" type="appl" />
         <tli id="T101" time="85.759" type="appl" />
         <tli id="T102" time="86.29899999999999" type="appl" />
         <tli id="T103" time="86.839" type="appl" />
         <tli id="T104" time="87.873" type="appl" />
         <tli id="T105" time="88.57633333333334" type="appl" />
         <tli id="T106" time="89.27966666666667" type="appl" />
         <tli id="T107" time="89.983" type="appl" />
         <tli id="T108" time="90.68633333333334" type="appl" />
         <tli id="T109" time="91.38966666666667" type="appl" />
         <tli id="T110" time="92.093" type="appl" />
         <tli id="T111" time="94.619" type="appl" />
         <tli id="T112" time="95.2825" type="appl" />
         <tli id="T113" time="95.946" type="appl" />
         <tli id="T114" time="96.6095" type="appl" />
         <tli id="T115" time="97.273" type="appl" />
         <tli id="T116" time="97.36" type="appl" />
         <tli id="T117" time="97.98166666666667" type="appl" />
         <tli id="T118" time="98.60333333333332" type="appl" />
         <tli id="T119" time="99.225" type="appl" />
         <tli id="T120" time="99.87" type="appl" />
         <tli id="T121" time="100.84" type="appl" />
         <tli id="T122" time="101.81" type="appl" />
         <tli id="T123" time="102.78" type="appl" />
         <tli id="T124" time="103.75" type="appl" />
         <tli id="T125" time="103.88" type="appl" />
         <tli id="T126" time="104.53666666666666" type="appl" />
         <tli id="T127" time="105.19333333333333" type="appl" />
         <tli id="T128" time="105.85" type="appl" />
         <tli id="T129" time="106.83" type="appl" />
         <tli id="T130" time="107.512" type="appl" />
         <tli id="T131" time="108.194" type="appl" />
         <tli id="T132" time="108.87599999999999" type="appl" />
         <tli id="T133" time="109.55799999999999" type="appl" />
         <tli id="T134" time="110.24" type="appl" />
         <tli id="T135" time="111.345" type="appl" />
         <tli id="T136" time="111.9575" type="appl" />
         <tli id="T137" time="112.57" type="appl" />
         <tli id="T138" time="113.1825" type="appl" />
         <tli id="T139" time="113.795" type="appl" />
         <tli id="T140" time="114.4075" type="appl" />
         <tli id="T141" time="115.02" type="appl" />
         <tli id="T142" time="115.295" type="appl" />
         <tli id="T143" time="115.86" type="appl" />
         <tli id="T144" time="116.425" type="appl" />
         <tli id="T145" time="116.99" type="appl" />
         <tli id="T146" time="117.37" type="appl" />
         <tli id="T147" time="118.089" type="appl" />
         <tli id="T148" time="118.808" type="appl" />
         <tli id="T149" time="119.527" type="appl" />
         <tli id="T150" time="120.24600000000001" type="appl" />
         <tli id="T151" time="120.965" type="appl" />
         <tli id="T0" time="122.287" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KNM"
                      type="t">
         <timeline-fork end="T6" start="T1">
            <tli id="T1.tx.1" />
            <tli id="T1.tx.2" />
            <tli id="T1.tx.3" />
            <tli id="T1.tx.4" />
            <tli id="T1.tx.5" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T6" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <nts id="Seg_3" n="HIAT:ip">(</nts>
                  <nts id="Seg_4" n="HIAT:ip">(</nts>
                  <ats e="T1.tx.1" id="Seg_5" n="HIAT:non-pho" s="T1">KuAI:</ats>
                  <nts id="Seg_6" n="HIAT:ip">)</nts>
                  <nts id="Seg_7" n="HIAT:ip">)</nts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1.tx.2" id="Seg_10" n="HIAT:w" s="T1.tx.1">Кунин</ts>
                  <nts id="Seg_11" n="HIAT:ip">,</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1.tx.3" id="Seg_14" n="HIAT:w" s="T1.tx.2">председатель</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1.tx.4" id="Seg_17" n="HIAT:w" s="T1.tx.3">сельсовета</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1.tx.5" id="Seg_20" n="HIAT:w" s="T1.tx.4">села</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_23" n="HIAT:w" s="T1.tx.5">Толька</ts>
                  <nts id="Seg_24" n="HIAT:ip">.</nts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T9" id="Seg_26" n="sc" s="T14">
               <ts e="T9" id="Seg_28" n="HIAT:u" s="T14">
                  <nts id="Seg_29" n="HIAT:ip">(</nts>
                  <nts id="Seg_30" n="HIAT:ip">(</nts>
                  <ats e="T7" id="Seg_31" n="HIAT:non-pho" s="T14">KNM:</ats>
                  <nts id="Seg_32" n="HIAT:ip">)</nts>
                  <nts id="Seg_33" n="HIAT:ip">)</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_36" n="HIAT:w" s="T7">Ɨralʼ</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_39" n="HIAT:w" s="T8">ämasɨqäqɨn</ts>
                  <nts id="Seg_40" n="HIAT:ip">.</nts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T16" id="Seg_42" n="sc" s="T10">
               <ts e="T16" id="Seg_44" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_46" n="HIAT:w" s="T10">Ukoːn</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_49" n="HIAT:w" s="T11">ilimpɔːqı</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_52" n="HIAT:w" s="T12">šittɨ</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T13">ɨralʼ</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">ämasɨqäqɨn</ts>
                  <nts id="Seg_59" n="HIAT:ip">.</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T22" id="Seg_61" n="sc" s="T17">
               <ts e="T22" id="Seg_63" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_65" n="HIAT:w" s="T17">Ɨrra</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_68" n="HIAT:w" s="T18">kət</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_71" n="HIAT:w" s="T19">qəːlɨmɨnʼa</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_74" n="HIAT:w" s="T20">šottɨ</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_77" n="HIAT:w" s="T21">qənta</ts>
                  <nts id="Seg_78" n="HIAT:ip">.</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T29" id="Seg_80" n="sc" s="T23">
               <ts e="T29" id="Seg_82" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_84" n="HIAT:w" s="T23">Moqonälʼa</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_87" n="HIAT:w" s="T24">tüptäqantɨ</ts>
                  <nts id="Seg_88" n="HIAT:ip">,</nts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_91" n="HIAT:w" s="T25">məttaqɨn</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_94" n="HIAT:w" s="T26">ukkɨr</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_97" n="HIAT:w" s="T27">loqa</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_100" n="HIAT:w" s="T28">qommɨntɨtɨ</ts>
                  <nts id="Seg_101" n="HIAT:ip">.</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T35" id="Seg_103" n="sc" s="T30">
               <ts e="T35" id="Seg_105" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_107" n="HIAT:w" s="T30">Mɨtɨ</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_110" n="HIAT:w" s="T31">tüŋa</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_113" n="HIAT:w" s="T32">üntɨ</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_115" n="HIAT:ip">(</nts>
                  <ts e="T34" id="Seg_117" n="HIAT:w" s="T33">okkɨr</ts>
                  <nts id="Seg_118" n="HIAT:ip">)</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_121" n="HIAT:w" s="T34">loqa</ts>
                  <nts id="Seg_122" n="HIAT:ip">.</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T39" id="Seg_124" n="sc" s="T36">
               <ts e="T39" id="Seg_126" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_128" n="HIAT:w" s="T36">Qaqlaqɨntɨ</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_131" n="HIAT:w" s="T37">ɨnna</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_134" n="HIAT:w" s="T38">telʼtɨmpatɨ</ts>
                  <nts id="Seg_135" n="HIAT:ip">.</nts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T45" id="Seg_137" n="sc" s="T40">
               <ts e="T45" id="Seg_139" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_141" n="HIAT:w" s="T40">Moqonä</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_144" n="HIAT:w" s="T41">tülʼa</ts>
                  <nts id="Seg_145" n="HIAT:ip">,</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_148" n="HIAT:w" s="T42">imaqotaqɨntɨ</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_151" n="HIAT:w" s="T43">nı</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_154" n="HIAT:w" s="T44">kətɨŋɨtɨ</ts>
                  <nts id="Seg_155" n="HIAT:ip">.</nts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T51" id="Seg_157" n="sc" s="T46">
               <ts e="T51" id="Seg_159" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_161" n="HIAT:w" s="T46">Man</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_164" n="HIAT:w" s="T47">tat</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_167" n="HIAT:w" s="T48">čʼɔːtɨ</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_170" n="HIAT:w" s="T49">telʼtɨrma</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_173" n="HIAT:w" s="T50">tattɨntam</ts>
                  <nts id="Seg_174" n="HIAT:ip">.</nts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T57" id="Seg_176" n="sc" s="T52">
               <ts e="T57" id="Seg_178" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_180" n="HIAT:w" s="T52">Imaqota</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_183" n="HIAT:w" s="T53">ima</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_186" n="HIAT:w" s="T54">orqɨlla</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_189" n="HIAT:w" s="T55">čʼam</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_192" n="HIAT:w" s="T56">näkɨlʼŋitɨ</ts>
                  <nts id="Seg_193" n="HIAT:ip">.</nts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T65" id="Seg_195" n="sc" s="T58">
               <ts e="T65" id="Seg_197" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_199" n="HIAT:w" s="T58">Loqa</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_202" n="HIAT:w" s="T59">ilʼamɛːla</ts>
                  <nts id="Seg_203" n="HIAT:ip">,</nts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_206" n="HIAT:w" s="T60">təpɨt</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_209" n="HIAT:w" s="T61">kunkɨlʼtɨ</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_212" n="HIAT:w" s="T62">par</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_215" n="HIAT:w" s="T63">ınna</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_218" n="HIAT:w" s="T64">omtɨja</ts>
                  <nts id="Seg_219" n="HIAT:ip">.</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T72" id="Seg_221" n="sc" s="T66">
               <ts e="T72" id="Seg_223" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_225" n="HIAT:w" s="T66">Ɨrra</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_228" n="HIAT:w" s="T67">toː</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_231" n="HIAT:w" s="T68">milʼa</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_234" n="HIAT:w" s="T69">čʼap</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_237" n="HIAT:w" s="T70">loqap</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_240" n="HIAT:w" s="T71">qättɨŋɨtɨ</ts>
                  <nts id="Seg_241" n="HIAT:ip">.</nts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T74" id="Seg_243" n="sc" s="T73">
               <ts e="T74" id="Seg_245" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_247" n="HIAT:w" s="T73">Čʼarampɨt</ts>
                  <nts id="Seg_248" n="HIAT:ip">.</nts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T78" id="Seg_250" n="sc" s="T75">
               <ts e="T78" id="Seg_252" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_254" n="HIAT:w" s="T75">Imaqotat</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_257" n="HIAT:w" s="T76">kunkɨl</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_260" n="HIAT:w" s="T77">qättɨmpɨtɨ</ts>
                  <nts id="Seg_261" n="HIAT:ip">.</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T84" id="Seg_263" n="sc" s="T79">
               <ts e="T84" id="Seg_265" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_267" n="HIAT:w" s="T79">Imaqota</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_270" n="HIAT:w" s="T80">üːtɨptäqan</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_273" n="HIAT:w" s="T81">loqa</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_276" n="HIAT:w" s="T82">pona</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_279" n="HIAT:w" s="T83">pakta</ts>
                  <nts id="Seg_280" n="HIAT:ip">.</nts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T91" id="Seg_282" n="sc" s="T85">
               <ts e="T91" id="Seg_284" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_286" n="HIAT:w" s="T85">Ɨrra</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_289" n="HIAT:w" s="T86">nʼomalʼ</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_292" n="HIAT:w" s="T87">porqɨmtɨ</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_295" n="HIAT:w" s="T88">toqqaltɨla</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_298" n="HIAT:w" s="T89">loqam</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_301" n="HIAT:w" s="T90">noːtɨŋɨtɨ</ts>
                  <nts id="Seg_302" n="HIAT:ip">.</nts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T94" id="Seg_304" n="sc" s="T92">
               <ts e="T94" id="Seg_306" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_308" n="HIAT:w" s="T92">Nan</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_311" n="HIAT:w" s="T93">noːlɨtɨ</ts>
                  <nts id="Seg_312" n="HIAT:ip">.</nts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T97" id="Seg_314" n="sc" s="T95">
               <ts e="T97" id="Seg_316" n="HIAT:u" s="T95">
                  <ts e="T96" id="Seg_318" n="HIAT:w" s="T95">Kuntɨk</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_321" n="HIAT:w" s="T96">nʼoːlɨtɨ</ts>
                  <nts id="Seg_322" n="HIAT:ip">.</nts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T99" id="Seg_324" n="sc" s="T98">
               <ts e="T99" id="Seg_326" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_328" n="HIAT:w" s="T98">Nʼoːlɨtɨ</ts>
                  <nts id="Seg_329" n="HIAT:ip">.</nts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T103" id="Seg_331" n="sc" s="T100">
               <ts e="T103" id="Seg_333" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_335" n="HIAT:w" s="T100">Pöllʼa</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_338" n="HIAT:w" s="T101">sak</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_341" n="HIAT:w" s="T102">quŋa</ts>
                  <nts id="Seg_342" n="HIAT:ip">.</nts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T110" id="Seg_344" n="sc" s="T104">
               <ts e="T110" id="Seg_346" n="HIAT:u" s="T104">
                  <ts e="T105" id="Seg_348" n="HIAT:w" s="T104">Ukkɨr</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_351" n="HIAT:w" s="T105">čʼonta</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_354" n="HIAT:w" s="T106">porqɨmtɨ</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_357" n="HIAT:w" s="T107">toː</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_360" n="HIAT:w" s="T108">mačʼa</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_363" n="HIAT:w" s="T109">čʼattɨŋɨtɨ</ts>
                  <nts id="Seg_364" n="HIAT:ip">.</nts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T115" id="Seg_366" n="sc" s="T111">
               <ts e="T115" id="Seg_368" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_370" n="HIAT:w" s="T111">Qalʼ</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_373" n="HIAT:w" s="T112">porq</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_375" n="HIAT:ip">(</nts>
                  <ts e="T114" id="Seg_377" n="HIAT:w" s="T113">tarɨmatɨ</ts>
                  <nts id="Seg_378" n="HIAT:ip">)</nts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_381" n="HIAT:w" s="T114">nʼoːtɨmpatɨ</ts>
                  <nts id="Seg_382" n="HIAT:ip">.</nts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T119" id="Seg_384" n="sc" s="T116">
               <ts e="T119" id="Seg_386" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_388" n="HIAT:w" s="T116">Aj</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_391" n="HIAT:w" s="T117">kuntɨk</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_394" n="HIAT:w" s="T118">nʼoːmpatɨ</ts>
                  <nts id="Seg_395" n="HIAT:ip">.</nts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T124" id="Seg_397" n="sc" s="T120">
               <ts e="T124" id="Seg_399" n="HIAT:u" s="T120">
                  <ts e="T121" id="Seg_401" n="HIAT:w" s="T120">Ukkɨr</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_404" n="HIAT:w" s="T121">čʼontaqɨn</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_407" n="HIAT:w" s="T122">təpɨ</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_410" n="HIAT:w" s="T123">qalaıːmpatɨ</ts>
                  <nts id="Seg_411" n="HIAT:ip">.</nts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T128" id="Seg_413" n="sc" s="T125">
               <ts e="T128" id="Seg_415" n="HIAT:u" s="T125">
                  <ts e="T126" id="Seg_417" n="HIAT:w" s="T125">Namɨšak</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_420" n="HIAT:w" s="T126">čʼasɨk</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_423" n="HIAT:w" s="T127">ɛsɨmpa</ts>
                  <nts id="Seg_424" n="HIAT:ip">.</nts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T134" id="Seg_426" n="sc" s="T129">
               <ts e="T134" id="Seg_428" n="HIAT:u" s="T129">
                  <ts e="T130" id="Seg_430" n="HIAT:w" s="T129">Na</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_433" n="HIAT:w" s="T130">pötpɨlʼ</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_436" n="HIAT:w" s="T131">ɨrram</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_439" n="HIAT:w" s="T132">ılla</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_442" n="HIAT:w" s="T133">qantaıːmpatɨ</ts>
                  <nts id="Seg_443" n="HIAT:ip">.</nts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T141" id="Seg_445" n="sc" s="T135">
               <ts e="T141" id="Seg_447" n="HIAT:u" s="T135">
                  <ts e="T136" id="Seg_449" n="HIAT:w" s="T135">Loqa</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_452" n="HIAT:w" s="T136">puːn</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_455" n="HIAT:w" s="T137">šittälʼa</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_458" n="HIAT:w" s="T138">sukkulʼta</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_461" n="HIAT:w" s="T139">čʼap</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_464" n="HIAT:w" s="T140">tümpa</ts>
                  <nts id="Seg_465" n="HIAT:ip">.</nts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T145" id="Seg_467" n="sc" s="T142">
               <ts e="T145" id="Seg_469" n="HIAT:u" s="T142">
                  <ts e="T143" id="Seg_471" n="HIAT:w" s="T142">Ɨrra</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_474" n="HIAT:w" s="T143">ılla</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_477" n="HIAT:w" s="T144">qumpa</ts>
                  <nts id="Seg_478" n="HIAT:ip">.</nts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T151" id="Seg_480" n="sc" s="T146">
               <ts e="T151" id="Seg_482" n="HIAT:u" s="T146">
                  <ts e="T147" id="Seg_484" n="HIAT:w" s="T146">Loqa</ts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_487" n="HIAT:w" s="T147">šittälʼa</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_490" n="HIAT:w" s="T148">na</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_493" n="HIAT:w" s="T149">apsɨt</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_496" n="HIAT:w" s="T150">ammɨntɨtɨ</ts>
                  <nts id="Seg_497" n="HIAT:ip">.</nts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T6" id="Seg_499" n="sc" s="T1">
               <ts e="T6" id="Seg_501" n="e" s="T1">((KuAI:)) Кунин, председатель сельсовета села Толька. </ts>
            </ts>
            <ts e="T9" id="Seg_502" n="sc" s="T14">
               <ts e="T7" id="Seg_504" n="e" s="T14">((KNM:)) </ts>
               <ts e="T8" id="Seg_506" n="e" s="T7">Ɨralʼ </ts>
               <ts e="T9" id="Seg_508" n="e" s="T8">ämasɨqäqɨn. </ts>
            </ts>
            <ts e="T16" id="Seg_509" n="sc" s="T10">
               <ts e="T11" id="Seg_511" n="e" s="T10">Ukoːn </ts>
               <ts e="T12" id="Seg_513" n="e" s="T11">ilimpɔːqı </ts>
               <ts e="T13" id="Seg_515" n="e" s="T12">šittɨ </ts>
               <ts e="T15" id="Seg_517" n="e" s="T13">ɨralʼ </ts>
               <ts e="T16" id="Seg_519" n="e" s="T15">ämasɨqäqɨn. </ts>
            </ts>
            <ts e="T22" id="Seg_520" n="sc" s="T17">
               <ts e="T18" id="Seg_522" n="e" s="T17">Ɨrra </ts>
               <ts e="T19" id="Seg_524" n="e" s="T18">kət </ts>
               <ts e="T20" id="Seg_526" n="e" s="T19">qəːlɨmɨnʼa </ts>
               <ts e="T21" id="Seg_528" n="e" s="T20">šottɨ </ts>
               <ts e="T22" id="Seg_530" n="e" s="T21">qənta. </ts>
            </ts>
            <ts e="T29" id="Seg_531" n="sc" s="T23">
               <ts e="T24" id="Seg_533" n="e" s="T23">Moqonälʼa </ts>
               <ts e="T25" id="Seg_535" n="e" s="T24">tüptäqantɨ, </ts>
               <ts e="T26" id="Seg_537" n="e" s="T25">məttaqɨn </ts>
               <ts e="T27" id="Seg_539" n="e" s="T26">ukkɨr </ts>
               <ts e="T28" id="Seg_541" n="e" s="T27">loqa </ts>
               <ts e="T29" id="Seg_543" n="e" s="T28">qommɨntɨtɨ. </ts>
            </ts>
            <ts e="T35" id="Seg_544" n="sc" s="T30">
               <ts e="T31" id="Seg_546" n="e" s="T30">Mɨtɨ </ts>
               <ts e="T32" id="Seg_548" n="e" s="T31">tüŋa </ts>
               <ts e="T33" id="Seg_550" n="e" s="T32">üntɨ </ts>
               <ts e="T34" id="Seg_552" n="e" s="T33">(okkɨr) </ts>
               <ts e="T35" id="Seg_554" n="e" s="T34">loqa. </ts>
            </ts>
            <ts e="T39" id="Seg_555" n="sc" s="T36">
               <ts e="T37" id="Seg_557" n="e" s="T36">Qaqlaqɨntɨ </ts>
               <ts e="T38" id="Seg_559" n="e" s="T37">ɨnna </ts>
               <ts e="T39" id="Seg_561" n="e" s="T38">telʼtɨmpatɨ. </ts>
            </ts>
            <ts e="T45" id="Seg_562" n="sc" s="T40">
               <ts e="T41" id="Seg_564" n="e" s="T40">Moqonä </ts>
               <ts e="T42" id="Seg_566" n="e" s="T41">tülʼa, </ts>
               <ts e="T43" id="Seg_568" n="e" s="T42">imaqotaqɨntɨ </ts>
               <ts e="T44" id="Seg_570" n="e" s="T43">nı </ts>
               <ts e="T45" id="Seg_572" n="e" s="T44">kətɨŋɨtɨ. </ts>
            </ts>
            <ts e="T51" id="Seg_573" n="sc" s="T46">
               <ts e="T47" id="Seg_575" n="e" s="T46">Man </ts>
               <ts e="T48" id="Seg_577" n="e" s="T47">tat </ts>
               <ts e="T49" id="Seg_579" n="e" s="T48">čʼɔːtɨ </ts>
               <ts e="T50" id="Seg_581" n="e" s="T49">telʼtɨrma </ts>
               <ts e="T51" id="Seg_583" n="e" s="T50">tattɨntam. </ts>
            </ts>
            <ts e="T57" id="Seg_584" n="sc" s="T52">
               <ts e="T53" id="Seg_586" n="e" s="T52">Imaqota </ts>
               <ts e="T54" id="Seg_588" n="e" s="T53">ima </ts>
               <ts e="T55" id="Seg_590" n="e" s="T54">orqɨlla </ts>
               <ts e="T56" id="Seg_592" n="e" s="T55">čʼam </ts>
               <ts e="T57" id="Seg_594" n="e" s="T56">näkɨlʼŋitɨ. </ts>
            </ts>
            <ts e="T65" id="Seg_595" n="sc" s="T58">
               <ts e="T59" id="Seg_597" n="e" s="T58">Loqa </ts>
               <ts e="T60" id="Seg_599" n="e" s="T59">ilʼamɛːla, </ts>
               <ts e="T61" id="Seg_601" n="e" s="T60">təpɨt </ts>
               <ts e="T62" id="Seg_603" n="e" s="T61">kunkɨlʼtɨ </ts>
               <ts e="T63" id="Seg_605" n="e" s="T62">par </ts>
               <ts e="T64" id="Seg_607" n="e" s="T63">ınna </ts>
               <ts e="T65" id="Seg_609" n="e" s="T64">omtɨja. </ts>
            </ts>
            <ts e="T72" id="Seg_610" n="sc" s="T66">
               <ts e="T67" id="Seg_612" n="e" s="T66">Ɨrra </ts>
               <ts e="T68" id="Seg_614" n="e" s="T67">toː </ts>
               <ts e="T69" id="Seg_616" n="e" s="T68">milʼa </ts>
               <ts e="T70" id="Seg_618" n="e" s="T69">čʼap </ts>
               <ts e="T71" id="Seg_620" n="e" s="T70">loqap </ts>
               <ts e="T72" id="Seg_622" n="e" s="T71">qättɨŋɨtɨ. </ts>
            </ts>
            <ts e="T74" id="Seg_623" n="sc" s="T73">
               <ts e="T74" id="Seg_625" n="e" s="T73">Čʼarampɨt. </ts>
            </ts>
            <ts e="T78" id="Seg_626" n="sc" s="T75">
               <ts e="T76" id="Seg_628" n="e" s="T75">Imaqotat </ts>
               <ts e="T77" id="Seg_630" n="e" s="T76">kunkɨl </ts>
               <ts e="T78" id="Seg_632" n="e" s="T77">qättɨmpɨtɨ. </ts>
            </ts>
            <ts e="T84" id="Seg_633" n="sc" s="T79">
               <ts e="T80" id="Seg_635" n="e" s="T79">Imaqota </ts>
               <ts e="T81" id="Seg_637" n="e" s="T80">üːtɨptäqan </ts>
               <ts e="T82" id="Seg_639" n="e" s="T81">loqa </ts>
               <ts e="T83" id="Seg_641" n="e" s="T82">pona </ts>
               <ts e="T84" id="Seg_643" n="e" s="T83">pakta. </ts>
            </ts>
            <ts e="T91" id="Seg_644" n="sc" s="T85">
               <ts e="T86" id="Seg_646" n="e" s="T85">Ɨrra </ts>
               <ts e="T87" id="Seg_648" n="e" s="T86">nʼomalʼ </ts>
               <ts e="T88" id="Seg_650" n="e" s="T87">porqɨmtɨ </ts>
               <ts e="T89" id="Seg_652" n="e" s="T88">toqqaltɨla </ts>
               <ts e="T90" id="Seg_654" n="e" s="T89">loqam </ts>
               <ts e="T91" id="Seg_656" n="e" s="T90">noːtɨŋɨtɨ. </ts>
            </ts>
            <ts e="T94" id="Seg_657" n="sc" s="T92">
               <ts e="T93" id="Seg_659" n="e" s="T92">Nan </ts>
               <ts e="T94" id="Seg_661" n="e" s="T93">noːlɨtɨ. </ts>
            </ts>
            <ts e="T97" id="Seg_662" n="sc" s="T95">
               <ts e="T96" id="Seg_664" n="e" s="T95">Kuntɨk </ts>
               <ts e="T97" id="Seg_666" n="e" s="T96">nʼoːlɨtɨ. </ts>
            </ts>
            <ts e="T99" id="Seg_667" n="sc" s="T98">
               <ts e="T99" id="Seg_669" n="e" s="T98">Nʼoːlɨtɨ. </ts>
            </ts>
            <ts e="T103" id="Seg_670" n="sc" s="T100">
               <ts e="T101" id="Seg_672" n="e" s="T100">Pöllʼa </ts>
               <ts e="T102" id="Seg_674" n="e" s="T101">sak </ts>
               <ts e="T103" id="Seg_676" n="e" s="T102">quŋa. </ts>
            </ts>
            <ts e="T110" id="Seg_677" n="sc" s="T104">
               <ts e="T105" id="Seg_679" n="e" s="T104">Ukkɨr </ts>
               <ts e="T106" id="Seg_681" n="e" s="T105">čʼonta </ts>
               <ts e="T107" id="Seg_683" n="e" s="T106">porqɨmtɨ </ts>
               <ts e="T108" id="Seg_685" n="e" s="T107">toː </ts>
               <ts e="T109" id="Seg_687" n="e" s="T108">mačʼa </ts>
               <ts e="T110" id="Seg_689" n="e" s="T109">čʼattɨŋɨtɨ. </ts>
            </ts>
            <ts e="T115" id="Seg_690" n="sc" s="T111">
               <ts e="T112" id="Seg_692" n="e" s="T111">Qalʼ </ts>
               <ts e="T113" id="Seg_694" n="e" s="T112">porq </ts>
               <ts e="T114" id="Seg_696" n="e" s="T113">(tarɨmatɨ) </ts>
               <ts e="T115" id="Seg_698" n="e" s="T114">nʼoːtɨmpatɨ. </ts>
            </ts>
            <ts e="T119" id="Seg_699" n="sc" s="T116">
               <ts e="T117" id="Seg_701" n="e" s="T116">Aj </ts>
               <ts e="T118" id="Seg_703" n="e" s="T117">kuntɨk </ts>
               <ts e="T119" id="Seg_705" n="e" s="T118">nʼoːmpatɨ. </ts>
            </ts>
            <ts e="T124" id="Seg_706" n="sc" s="T120">
               <ts e="T121" id="Seg_708" n="e" s="T120">Ukkɨr </ts>
               <ts e="T122" id="Seg_710" n="e" s="T121">čʼontaqɨn </ts>
               <ts e="T123" id="Seg_712" n="e" s="T122">təpɨ </ts>
               <ts e="T124" id="Seg_714" n="e" s="T123">qalaıːmpatɨ. </ts>
            </ts>
            <ts e="T128" id="Seg_715" n="sc" s="T125">
               <ts e="T126" id="Seg_717" n="e" s="T125">Namɨšak </ts>
               <ts e="T127" id="Seg_719" n="e" s="T126">čʼasɨk </ts>
               <ts e="T128" id="Seg_721" n="e" s="T127">ɛsɨmpa. </ts>
            </ts>
            <ts e="T134" id="Seg_722" n="sc" s="T129">
               <ts e="T130" id="Seg_724" n="e" s="T129">Na </ts>
               <ts e="T131" id="Seg_726" n="e" s="T130">pötpɨlʼ </ts>
               <ts e="T132" id="Seg_728" n="e" s="T131">ɨrram </ts>
               <ts e="T133" id="Seg_730" n="e" s="T132">ılla </ts>
               <ts e="T134" id="Seg_732" n="e" s="T133">qantaıːmpatɨ. </ts>
            </ts>
            <ts e="T141" id="Seg_733" n="sc" s="T135">
               <ts e="T136" id="Seg_735" n="e" s="T135">Loqa </ts>
               <ts e="T137" id="Seg_737" n="e" s="T136">puːn </ts>
               <ts e="T138" id="Seg_739" n="e" s="T137">šittälʼa </ts>
               <ts e="T139" id="Seg_741" n="e" s="T138">sukkulʼta </ts>
               <ts e="T140" id="Seg_743" n="e" s="T139">čʼap </ts>
               <ts e="T141" id="Seg_745" n="e" s="T140">tümpa. </ts>
            </ts>
            <ts e="T145" id="Seg_746" n="sc" s="T142">
               <ts e="T143" id="Seg_748" n="e" s="T142">Ɨrra </ts>
               <ts e="T144" id="Seg_750" n="e" s="T143">ılla </ts>
               <ts e="T145" id="Seg_752" n="e" s="T144">qumpa. </ts>
            </ts>
            <ts e="T151" id="Seg_753" n="sc" s="T146">
               <ts e="T147" id="Seg_755" n="e" s="T146">Loqa </ts>
               <ts e="T148" id="Seg_757" n="e" s="T147">šittälʼa </ts>
               <ts e="T149" id="Seg_759" n="e" s="T148">na </ts>
               <ts e="T150" id="Seg_761" n="e" s="T149">apsɨt </ts>
               <ts e="T151" id="Seg_763" n="e" s="T150">ammɨntɨtɨ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_764" s="T1">KNM_196X_Fox_flk.001 (001)</ta>
            <ta e="T9" id="Seg_765" s="T14">KNM_196X_Fox_flk.002 (002)</ta>
            <ta e="T16" id="Seg_766" s="T10">KNM_196X_Fox_flk.003 (003)</ta>
            <ta e="T22" id="Seg_767" s="T17">KNM_196X_Fox_flk.004 (005)</ta>
            <ta e="T29" id="Seg_768" s="T23">KNM_196X_Fox_flk.005 (006)</ta>
            <ta e="T35" id="Seg_769" s="T30">KNM_196X_Fox_flk.006 (007)</ta>
            <ta e="T39" id="Seg_770" s="T36">KNM_196X_Fox_flk.007 (008)</ta>
            <ta e="T45" id="Seg_771" s="T40">KNM_196X_Fox_flk.008 (009)</ta>
            <ta e="T51" id="Seg_772" s="T46">KNM_196X_Fox_flk.009 (010)</ta>
            <ta e="T57" id="Seg_773" s="T52">KNM_196X_Fox_flk.010 (011)</ta>
            <ta e="T65" id="Seg_774" s="T58">KNM_196X_Fox_flk.011 (012)</ta>
            <ta e="T72" id="Seg_775" s="T66">KNM_196X_Fox_flk.012 (013)</ta>
            <ta e="T74" id="Seg_776" s="T73">KNM_196X_Fox_flk.013 (014)</ta>
            <ta e="T78" id="Seg_777" s="T75">KNM_196X_Fox_flk.014 (015)</ta>
            <ta e="T84" id="Seg_778" s="T79">KNM_196X_Fox_flk.015 (016)</ta>
            <ta e="T91" id="Seg_779" s="T85">KNM_196X_Fox_flk.016 (017)</ta>
            <ta e="T94" id="Seg_780" s="T92">KNM_196X_Fox_flk.017 (018)</ta>
            <ta e="T97" id="Seg_781" s="T95">KNM_196X_Fox_flk.018 (019)</ta>
            <ta e="T99" id="Seg_782" s="T98">KNM_196X_Fox_flk.019 (020)</ta>
            <ta e="T103" id="Seg_783" s="T100">KNM_196X_Fox_flk.020 (021)</ta>
            <ta e="T110" id="Seg_784" s="T104">KNM_196X_Fox_flk.021 (022)</ta>
            <ta e="T115" id="Seg_785" s="T111">KNM_196X_Fox_flk.022 (023)</ta>
            <ta e="T119" id="Seg_786" s="T116">KNM_196X_Fox_flk.023 (024)</ta>
            <ta e="T124" id="Seg_787" s="T120">KNM_196X_Fox_flk.024 (025)</ta>
            <ta e="T128" id="Seg_788" s="T125">KNM_196X_Fox_flk.025 (026)</ta>
            <ta e="T134" id="Seg_789" s="T129">KNM_196X_Fox_flk.026 (027)</ta>
            <ta e="T141" id="Seg_790" s="T135">KNM_196X_Fox_flk.027 (028)</ta>
            <ta e="T145" id="Seg_791" s="T142">KNM_196X_Fox_flk.028 (029)</ta>
            <ta e="T151" id="Seg_792" s="T146">KNM_196X_Fox_flk.029 (030)</ta>
         </annotation>
         <annotation name="st" tierref="st" />
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_793" s="T1">Кунин, председатель сельсовета села Толька.</ta>
            <ta e="T9" id="Seg_794" s="T14">ɨrra əmʼasɨkaqɨ</ta>
            <ta e="T16" id="Seg_795" s="T10">ukon ilʼimpɔqı šʼətqɨn ɨrra ämʼasɨqəqı</ta>
            <ta e="T22" id="Seg_796" s="T17">ɨrra kən qɨlɨma šʼəttɨ qənta</ta>
            <ta e="T29" id="Seg_797" s="T23">moqonälʼa tüptäqantɨ wəttaqɨn ukkɨr loqa qonmmɨntɨtɨ</ta>
            <ta e="T35" id="Seg_798" s="T30">mɨtɨ tümpa üntɨ (okkɨr) loqa</ta>
            <ta e="T39" id="Seg_799" s="T36">qaqlaːqɨn(tɨ) ɨnna telʼtɨmpatɨ </ta>
            <ta e="T45" id="Seg_800" s="T40">moqonä tülʼa imaqotaqɨntɨ nɨ kətɨqɨtɨ</ta>
            <ta e="T51" id="Seg_801" s="T46">man tat čʼottɨ telʼtɨrma tattɨntam</ta>
            <ta e="T57" id="Seg_802" s="T52">imaqota ima ərqɨla čʼam näqɨlʼŋitɨ</ta>
            <ta e="T65" id="Seg_803" s="T58">loqa ilʼameːlʼa təpɨ kunkɨltɨ tar ınna ɔːmtɨja</ta>
            <ta e="T72" id="Seg_804" s="T66">ɨrra to milʼa čʼap loqap qättɨqɨti</ta>
            <ta e="T74" id="Seg_805" s="T73">čʼaqaltet</ta>
            <ta e="T78" id="Seg_806" s="T75">imaqotat kunkɨl qätɨmpɨtɨ</ta>
            <ta e="T84" id="Seg_807" s="T79">imaqota ütɨptäqan loqa ponna pakta</ta>
            <ta e="T91" id="Seg_808" s="T85">ɨrra nʼomaj porqɨntɨ toqqaltɨla loqam nʼottɨqɨtɨ </ta>
            <ta e="T94" id="Seg_809" s="T92">nan nʼolɨtɨ </ta>
            <ta e="T97" id="Seg_810" s="T95">kuntɨk nʼolɨtɨ</ta>
            <ta e="T99" id="Seg_811" s="T98">nʼolɨtɨ</ta>
            <ta e="T103" id="Seg_812" s="T100">pöllʼa sʼak kuqe</ta>
            <ta e="T110" id="Seg_813" s="T104">ukkɨr čʼonta porqɨntɨ to mačʼa čʼattɨqɨtɨ</ta>
            <ta e="T115" id="Seg_814" s="T111">qalʼ porq tarɨ matɨ nʼotɨmpatɨ</ta>
            <ta e="T119" id="Seg_815" s="T116">aj kuntɨk nʼompatɨ</ta>
            <ta e="T124" id="Seg_816" s="T120">ukkɨr čʼontaqɨn təpɨ qaraıːmpatɨ</ta>
            <ta e="T128" id="Seg_817" s="T125">namɨšak čʼasiqa ɛːsɨmpa</ta>
            <ta e="T134" id="Seg_818" s="T129">na pötpɨ ɨrram ɨlla qantaıːmpatɨ</ta>
            <ta e="T141" id="Seg_819" s="T135">loqa pun šʼitɛlʼa sukkulʼ(t)a čʼap tümpa</ta>
            <ta e="T145" id="Seg_820" s="T142">ɨrra ılla qumpa</ta>
            <ta e="T151" id="Seg_821" s="T146">loqa šʼittɛlʼa na apsɨt ammɨntɨtɨ</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_822" s="T1">((KuAI:)) Кунин, председатель сельсовета села Толька. </ta>
            <ta e="T9" id="Seg_823" s="T14">((KNM:)) Ɨralʼ ämasɨqäqɨn. </ta>
            <ta e="T16" id="Seg_824" s="T10">Ukoːn ilimpɔːqı šittɨ ɨralʼ ämasɨqäqɨn. </ta>
            <ta e="T22" id="Seg_825" s="T17">Ɨrra kət qəːlɨmɨnʼa šottɨ qənta. </ta>
            <ta e="T29" id="Seg_826" s="T23">Moqonälʼa tüptäqantɨ, məttaqɨn ukkɨr loqa qommɨntɨtɨ. </ta>
            <ta e="T35" id="Seg_827" s="T30">Mɨtɨ tüŋa üntɨ (okkɨr) loqa. </ta>
            <ta e="T39" id="Seg_828" s="T36">Qaqlaqɨntɨ ɨnna telʼtɨmpatɨ. </ta>
            <ta e="T45" id="Seg_829" s="T40">Moqonä tülʼa, imaqotaqɨntɨ nı kətɨŋɨtɨ. </ta>
            <ta e="T51" id="Seg_830" s="T46">Man tat čʼɔːtɨ telʼtɨrma tattɨntam. </ta>
            <ta e="T57" id="Seg_831" s="T52">Imaqota ima orqɨlla čʼam näkɨlʼŋitɨ. </ta>
            <ta e="T65" id="Seg_832" s="T58">Loqa ilʼamɛːla, təpɨt kunkɨlʼtɨ par ınna omtɨja. </ta>
            <ta e="T72" id="Seg_833" s="T66">Ɨrra toː milʼa čʼap loqap qättɨŋɨtɨ. </ta>
            <ta e="T74" id="Seg_834" s="T73">Čʼarampɨt. </ta>
            <ta e="T78" id="Seg_835" s="T75">Imaqotat kunkɨl qättɨmpɨtɨ. </ta>
            <ta e="T84" id="Seg_836" s="T79">Imaqota üːtɨptäqan loqa pona pakta. </ta>
            <ta e="T91" id="Seg_837" s="T85">Ɨrra nʼomalʼ porqɨmtɨ toqqaltɨla loqam noːtɨŋɨtɨ. </ta>
            <ta e="T94" id="Seg_838" s="T92">Nan noːlɨtɨ. </ta>
            <ta e="T97" id="Seg_839" s="T95">Kuntɨk nʼoːlɨtɨ. </ta>
            <ta e="T99" id="Seg_840" s="T98">Nʼoːlɨtɨ. </ta>
            <ta e="T103" id="Seg_841" s="T100">Pöllʼa sak quŋa. </ta>
            <ta e="T110" id="Seg_842" s="T104">Ukkɨr čʼonta porqɨmtɨ toː mačʼa čʼattɨŋɨtɨ. </ta>
            <ta e="T115" id="Seg_843" s="T111">Qalʼ porq (tarɨmatɨ) nʼoːtɨmpatɨ. </ta>
            <ta e="T119" id="Seg_844" s="T116">Aj kuntɨk nʼoːmpatɨ. </ta>
            <ta e="T124" id="Seg_845" s="T120">Ukkɨr čʼontaqɨn təpɨ qalaıːmpatɨ. </ta>
            <ta e="T128" id="Seg_846" s="T125">Namɨšak čʼasɨk ɛsɨmpa. </ta>
            <ta e="T134" id="Seg_847" s="T129">Na pötpɨlʼ ɨrram ılla qantaıːmpatɨ. </ta>
            <ta e="T141" id="Seg_848" s="T135">Loqa puːn šittälʼa sukkulʼta čʼap tümpa. </ta>
            <ta e="T145" id="Seg_849" s="T142">Ɨrra ılla qumpa. </ta>
            <ta e="T151" id="Seg_850" s="T146">Loqa šittälʼa na apsɨt ammɨntɨtɨ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T8" id="Seg_851" s="T7">ɨra-lʼ</ta>
            <ta e="T9" id="Seg_852" s="T8">äma-sɨ-qäqɨ-n</ta>
            <ta e="T11" id="Seg_853" s="T10">ukoːn</ta>
            <ta e="T12" id="Seg_854" s="T11">ili-mpɔː-qı</ta>
            <ta e="T13" id="Seg_855" s="T12">šittɨ</ta>
            <ta e="T15" id="Seg_856" s="T13">ɨra-lʼ</ta>
            <ta e="T16" id="Seg_857" s="T15">äma-sɨ-qäqɨ-n</ta>
            <ta e="T18" id="Seg_858" s="T17">ɨrra</ta>
            <ta e="T19" id="Seg_859" s="T18">kə-t</ta>
            <ta e="T20" id="Seg_860" s="T19">qəːlɨ-mɨnʼa</ta>
            <ta e="T21" id="Seg_861" s="T20">šot-tɨ</ta>
            <ta e="T22" id="Seg_862" s="T21">qən-ta</ta>
            <ta e="T24" id="Seg_863" s="T23">moqonä-lʼa</ta>
            <ta e="T25" id="Seg_864" s="T24">tü-ptä-qan-tɨ</ta>
            <ta e="T26" id="Seg_865" s="T25">mətta-qɨn</ta>
            <ta e="T27" id="Seg_866" s="T26">ukkɨr</ta>
            <ta e="T28" id="Seg_867" s="T27">loqa</ta>
            <ta e="T29" id="Seg_868" s="T28">qo-mmɨ-ntɨ-tɨ</ta>
            <ta e="T31" id="Seg_869" s="T30">mɨtɨ</ta>
            <ta e="T32" id="Seg_870" s="T31">tü-ŋa</ta>
            <ta e="T33" id="Seg_871" s="T32">üntɨ</ta>
            <ta e="T34" id="Seg_872" s="T33">okkɨr</ta>
            <ta e="T35" id="Seg_873" s="T34">loqa</ta>
            <ta e="T37" id="Seg_874" s="T36">qaqla-qɨn-tɨ</ta>
            <ta e="T38" id="Seg_875" s="T37">ɨnna</ta>
            <ta e="T39" id="Seg_876" s="T38">telʼtɨ-mpa-tɨ</ta>
            <ta e="T41" id="Seg_877" s="T40">moqonä</ta>
            <ta e="T42" id="Seg_878" s="T41">tü-lʼa</ta>
            <ta e="T43" id="Seg_879" s="T42">imaqota-qɨn-tɨ</ta>
            <ta e="T44" id="Seg_880" s="T43">nı</ta>
            <ta e="T45" id="Seg_881" s="T44">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T47" id="Seg_882" s="T46">man</ta>
            <ta e="T48" id="Seg_883" s="T47">tan</ta>
            <ta e="T49" id="Seg_884" s="T48">čʼɔːtɨ</ta>
            <ta e="T50" id="Seg_885" s="T49">telʼtɨrma</ta>
            <ta e="T51" id="Seg_886" s="T50">tattɨ-nta-m</ta>
            <ta e="T53" id="Seg_887" s="T52">imaqota</ta>
            <ta e="T54" id="Seg_888" s="T53">ima</ta>
            <ta e="T55" id="Seg_889" s="T54">orqɨl-la</ta>
            <ta e="T56" id="Seg_890" s="T55">čʼam</ta>
            <ta e="T57" id="Seg_891" s="T56">näkɨ-lʼ-ŋi-tɨ</ta>
            <ta e="T59" id="Seg_892" s="T58">loqa</ta>
            <ta e="T60" id="Seg_893" s="T59">ilʼa-m-ɛː-la</ta>
            <ta e="T61" id="Seg_894" s="T60">təp-ɨ-t</ta>
            <ta e="T62" id="Seg_895" s="T61">kunkɨlʼ-tɨ</ta>
            <ta e="T63" id="Seg_896" s="T62">par</ta>
            <ta e="T64" id="Seg_897" s="T63">ınna</ta>
            <ta e="T65" id="Seg_898" s="T64">omtɨ-ja</ta>
            <ta e="T67" id="Seg_899" s="T66">ɨrra</ta>
            <ta e="T68" id="Seg_900" s="T67">toː</ta>
            <ta e="T69" id="Seg_901" s="T68">mi-lʼa</ta>
            <ta e="T70" id="Seg_902" s="T69">čʼap</ta>
            <ta e="T71" id="Seg_903" s="T70">loqa-p</ta>
            <ta e="T72" id="Seg_904" s="T71">qättɨ-ŋɨ-tɨ</ta>
            <ta e="T74" id="Seg_905" s="T73">čʼar-a-mpɨ-t</ta>
            <ta e="T76" id="Seg_906" s="T75">imaqota-t</ta>
            <ta e="T77" id="Seg_907" s="T76">kunkɨl</ta>
            <ta e="T78" id="Seg_908" s="T77">qättɨ-mpɨ-tɨ</ta>
            <ta e="T80" id="Seg_909" s="T79">imaqota</ta>
            <ta e="T81" id="Seg_910" s="T80">üːtɨ-ptä-qan</ta>
            <ta e="T82" id="Seg_911" s="T81">loqa</ta>
            <ta e="T83" id="Seg_912" s="T82">pona</ta>
            <ta e="T84" id="Seg_913" s="T83">pakta</ta>
            <ta e="T86" id="Seg_914" s="T85">ɨrra</ta>
            <ta e="T87" id="Seg_915" s="T86">nʼoma-lʼ</ta>
            <ta e="T88" id="Seg_916" s="T87">porqɨ-m-tɨ</ta>
            <ta e="T89" id="Seg_917" s="T88">toqq-altɨ-la</ta>
            <ta e="T90" id="Seg_918" s="T89">loqa-m</ta>
            <ta e="T91" id="Seg_919" s="T90">noː-tɨ-ŋɨ-tɨ</ta>
            <ta e="T93" id="Seg_920" s="T92">nan</ta>
            <ta e="T94" id="Seg_921" s="T93">noː-lɨ-tɨ</ta>
            <ta e="T96" id="Seg_922" s="T95">kuntɨ-k</ta>
            <ta e="T97" id="Seg_923" s="T96">nʼoː-lɨ-tɨ</ta>
            <ta e="T99" id="Seg_924" s="T98">nʼoː-lɨ-tɨ</ta>
            <ta e="T101" id="Seg_925" s="T100">pöl-lʼa</ta>
            <ta e="T102" id="Seg_926" s="T101">sak</ta>
            <ta e="T103" id="Seg_927" s="T102">qu-ŋa</ta>
            <ta e="T105" id="Seg_928" s="T104">ukkɨr</ta>
            <ta e="T106" id="Seg_929" s="T105">čʼonta</ta>
            <ta e="T107" id="Seg_930" s="T106">porqɨ-m-tɨ</ta>
            <ta e="T108" id="Seg_931" s="T107">toː</ta>
            <ta e="T109" id="Seg_932" s="T108">mačʼa</ta>
            <ta e="T110" id="Seg_933" s="T109">čʼattɨ-ŋɨ-tɨ</ta>
            <ta e="T112" id="Seg_934" s="T111">qa-lʼ</ta>
            <ta e="T113" id="Seg_935" s="T112">porq</ta>
            <ta e="T114" id="Seg_936" s="T113">tarɨ-matɨ</ta>
            <ta e="T115" id="Seg_937" s="T114">nʼoː-tɨ-mpa-tɨ</ta>
            <ta e="T117" id="Seg_938" s="T116">aj</ta>
            <ta e="T118" id="Seg_939" s="T117">kuntɨ-k</ta>
            <ta e="T119" id="Seg_940" s="T118">nʼoː-mpa-tɨ</ta>
            <ta e="T121" id="Seg_941" s="T120">ukkɨr</ta>
            <ta e="T122" id="Seg_942" s="T121">čʼonta-qɨn</ta>
            <ta e="T123" id="Seg_943" s="T122">təpɨ</ta>
            <ta e="T124" id="Seg_944" s="T123">qala-ıː-mpa-tɨ</ta>
            <ta e="T126" id="Seg_945" s="T125">namɨšak</ta>
            <ta e="T127" id="Seg_946" s="T126">čʼasɨ-k</ta>
            <ta e="T128" id="Seg_947" s="T127">ɛsɨ-mpa</ta>
            <ta e="T130" id="Seg_948" s="T129">na</ta>
            <ta e="T131" id="Seg_949" s="T130">pöt-pɨlʼ</ta>
            <ta e="T132" id="Seg_950" s="T131">ɨrra-m</ta>
            <ta e="T133" id="Seg_951" s="T132">ılla</ta>
            <ta e="T134" id="Seg_952" s="T133">qanta-ıː-mpa-tɨ</ta>
            <ta e="T136" id="Seg_953" s="T135">loqa</ta>
            <ta e="T137" id="Seg_954" s="T136">puːn</ta>
            <ta e="T138" id="Seg_955" s="T137">šittälʼa</ta>
            <ta e="T139" id="Seg_956" s="T138">sukkulʼta</ta>
            <ta e="T140" id="Seg_957" s="T139">čʼap</ta>
            <ta e="T141" id="Seg_958" s="T140">tü-mpa</ta>
            <ta e="T143" id="Seg_959" s="T142">ɨrra</ta>
            <ta e="T144" id="Seg_960" s="T143">ılla</ta>
            <ta e="T145" id="Seg_961" s="T144">qu-mpa</ta>
            <ta e="T147" id="Seg_962" s="T146">loqa</ta>
            <ta e="T148" id="Seg_963" s="T147">šittälʼa</ta>
            <ta e="T149" id="Seg_964" s="T148">na</ta>
            <ta e="T150" id="Seg_965" s="T149">apsɨ-t</ta>
            <ta e="T151" id="Seg_966" s="T150">am-mɨ-ntɨ-tɨ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T8" id="Seg_967" s="T7">ira-lʼ</ta>
            <ta e="T9" id="Seg_968" s="T8">ama-sɨ-qı-t</ta>
            <ta e="T11" id="Seg_969" s="T10">ukoːn</ta>
            <ta e="T12" id="Seg_970" s="T11">ilɨ-mpɨ-qı</ta>
            <ta e="T13" id="Seg_971" s="T12">šittɨ</ta>
            <ta e="T15" id="Seg_972" s="T13">ira-lʼ</ta>
            <ta e="T16" id="Seg_973" s="T15">ama-sɨ-qı-n</ta>
            <ta e="T18" id="Seg_974" s="T17">ira</ta>
            <ta e="T19" id="Seg_975" s="T18">kə-n</ta>
            <ta e="T20" id="Seg_976" s="T19">qəːlɨ-mɨnʼa</ta>
            <ta e="T21" id="Seg_977" s="T20">šöt-ntɨ</ta>
            <ta e="T22" id="Seg_978" s="T21">qən-ntɨ</ta>
            <ta e="T24" id="Seg_979" s="T23">moqɨnä-lʼa</ta>
            <ta e="T25" id="Seg_980" s="T24">tü-ptäː-qɨn-ntɨ</ta>
            <ta e="T26" id="Seg_981" s="T25">wəttɨ-qɨn</ta>
            <ta e="T27" id="Seg_982" s="T26">ukkɨr</ta>
            <ta e="T28" id="Seg_983" s="T27">loqa</ta>
            <ta e="T29" id="Seg_984" s="T28">qo-mpɨ-ntɨ-tɨ</ta>
            <ta e="T31" id="Seg_985" s="T30">mɨta</ta>
            <ta e="T32" id="Seg_986" s="T31">tü-ŋɨ</ta>
            <ta e="T33" id="Seg_987" s="T32">üntɨ</ta>
            <ta e="T34" id="Seg_988" s="T33">ukkɨr</ta>
            <ta e="T35" id="Seg_989" s="T34">loqa</ta>
            <ta e="T37" id="Seg_990" s="T36">qaqlɨ-qɨn-ntɨ</ta>
            <ta e="T38" id="Seg_991" s="T37">ınnä</ta>
            <ta e="T39" id="Seg_992" s="T38">tɛltɨ-mpɨ-tɨ</ta>
            <ta e="T41" id="Seg_993" s="T40">moqɨnä</ta>
            <ta e="T42" id="Seg_994" s="T41">tü-lä</ta>
            <ta e="T43" id="Seg_995" s="T42">imaqota-qɨn-ntɨ</ta>
            <ta e="T44" id="Seg_996" s="T43">nık</ta>
            <ta e="T45" id="Seg_997" s="T44">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T47" id="Seg_998" s="T46">man</ta>
            <ta e="T48" id="Seg_999" s="T47">tan</ta>
            <ta e="T49" id="Seg_1000" s="T48">čʼɔːtɨ</ta>
            <ta e="T50" id="Seg_1001" s="T49">telʼtɨrma</ta>
            <ta e="T51" id="Seg_1002" s="T50">taːtɨ-ntɨ-m</ta>
            <ta e="T53" id="Seg_1003" s="T52">imaqota</ta>
            <ta e="T54" id="Seg_1004" s="T53">ima</ta>
            <ta e="T55" id="Seg_1005" s="T54">orqɨl-lä</ta>
            <ta e="T56" id="Seg_1006" s="T55">čʼam</ta>
            <ta e="T57" id="Seg_1007" s="T56">näkä-lɨ-ŋɨ-tɨ</ta>
            <ta e="T59" id="Seg_1008" s="T58">loqa</ta>
            <ta e="T60" id="Seg_1009" s="T59">ilɨ-m-ɛː-lä</ta>
            <ta e="T61" id="Seg_1010" s="T60">təp-ɨ-n</ta>
            <ta e="T62" id="Seg_1011" s="T61">kuŋkol-tɨ</ta>
            <ta e="T63" id="Seg_1012" s="T62">pɔːrɨ</ta>
            <ta e="T64" id="Seg_1013" s="T63">ınnä</ta>
            <ta e="T65" id="Seg_1014" s="T64">omtɨ-ŋɨ</ta>
            <ta e="T67" id="Seg_1015" s="T66">ira</ta>
            <ta e="T68" id="Seg_1016" s="T67">toː</ta>
            <ta e="T69" id="Seg_1017" s="T68">mi-lä</ta>
            <ta e="T70" id="Seg_1018" s="T69">čʼam</ta>
            <ta e="T71" id="Seg_1019" s="T70">loqa-m</ta>
            <ta e="T72" id="Seg_1020" s="T71">qättɨ-ŋɨ-tɨ</ta>
            <ta e="T74" id="Seg_1021" s="T73">čʼar-ɨ-mpɨ-tɨ</ta>
            <ta e="T76" id="Seg_1022" s="T75">imaqota-n</ta>
            <ta e="T77" id="Seg_1023" s="T76">kuŋkol</ta>
            <ta e="T78" id="Seg_1024" s="T77">qättɨ-mpɨ-tɨ</ta>
            <ta e="T80" id="Seg_1025" s="T79">imaqota</ta>
            <ta e="T81" id="Seg_1026" s="T80">üːtɨ-ptäː-qɨn</ta>
            <ta e="T82" id="Seg_1027" s="T81">loqa</ta>
            <ta e="T83" id="Seg_1028" s="T82">ponä</ta>
            <ta e="T84" id="Seg_1029" s="T83">paktɨ</ta>
            <ta e="T86" id="Seg_1030" s="T85">ira</ta>
            <ta e="T87" id="Seg_1031" s="T86">nʼoma-lʼ</ta>
            <ta e="T88" id="Seg_1032" s="T87">porqɨ-m-tɨ</ta>
            <ta e="T89" id="Seg_1033" s="T88">*tokk-altɨ-lä</ta>
            <ta e="T90" id="Seg_1034" s="T89">loqa-m</ta>
            <ta e="T91" id="Seg_1035" s="T90">nʼoː-ntɨ-ŋɨ-tɨ</ta>
            <ta e="T93" id="Seg_1036" s="T92">nanɨ</ta>
            <ta e="T94" id="Seg_1037" s="T93">nʼoː-lɨ-tɨ</ta>
            <ta e="T96" id="Seg_1038" s="T95">kuntɨ-k</ta>
            <ta e="T97" id="Seg_1039" s="T96">nʼoː-lɨ-tɨ</ta>
            <ta e="T99" id="Seg_1040" s="T98">nʼoː-lɨ-tɨ</ta>
            <ta e="T101" id="Seg_1041" s="T100">pöt-lä</ta>
            <ta e="T102" id="Seg_1042" s="T101">sʼak</ta>
            <ta e="T103" id="Seg_1043" s="T102">qu-ŋɨ</ta>
            <ta e="T105" id="Seg_1044" s="T104">ukkɨr</ta>
            <ta e="T106" id="Seg_1045" s="T105">čʼontɨ</ta>
            <ta e="T107" id="Seg_1046" s="T106">porqɨ-m-tɨ</ta>
            <ta e="T108" id="Seg_1047" s="T107">toː</ta>
            <ta e="T109" id="Seg_1048" s="T108">mačʼä</ta>
            <ta e="T110" id="Seg_1049" s="T109">čʼattɨ-ŋɨ-tɨ</ta>
            <ta e="T112" id="Seg_1050" s="T111">qaj-lʼ</ta>
            <ta e="T113" id="Seg_1051" s="T112">porqɨ</ta>
            <ta e="T114" id="Seg_1052" s="T113">*tarɨ-matɨ</ta>
            <ta e="T115" id="Seg_1053" s="T114">nʼoː-ntɨ-mpɨ-tɨ</ta>
            <ta e="T117" id="Seg_1054" s="T116">aj</ta>
            <ta e="T118" id="Seg_1055" s="T117">kuntɨ-k</ta>
            <ta e="T119" id="Seg_1056" s="T118">nʼoː-mpɨ-tɨ</ta>
            <ta e="T121" id="Seg_1057" s="T120">ukkɨr</ta>
            <ta e="T122" id="Seg_1058" s="T121">čʼontɨ-qɨn</ta>
            <ta e="T123" id="Seg_1059" s="T122">təp</ta>
            <ta e="T124" id="Seg_1060" s="T123">qalɨ-ıː-mpɨ-ntɨ</ta>
            <ta e="T126" id="Seg_1061" s="T125">namɨššak</ta>
            <ta e="T127" id="Seg_1062" s="T126">*čʼasɨ-k</ta>
            <ta e="T128" id="Seg_1063" s="T127">ɛsɨ-mpɨ</ta>
            <ta e="T130" id="Seg_1064" s="T129">na</ta>
            <ta e="T131" id="Seg_1065" s="T130">pöt-mpɨlʼ</ta>
            <ta e="T132" id="Seg_1066" s="T131">ira-mɨ</ta>
            <ta e="T133" id="Seg_1067" s="T132">ıllä</ta>
            <ta e="T134" id="Seg_1068" s="T133">qantɨ-ıː-mpɨ-ntɨ</ta>
            <ta e="T136" id="Seg_1069" s="T135">loqa</ta>
            <ta e="T137" id="Seg_1070" s="T136">puːn</ta>
            <ta e="T138" id="Seg_1071" s="T137">šittälʼ</ta>
            <ta e="T139" id="Seg_1072" s="T138">sukɨltä</ta>
            <ta e="T140" id="Seg_1073" s="T139">čʼam</ta>
            <ta e="T141" id="Seg_1074" s="T140">tü-mpɨ</ta>
            <ta e="T143" id="Seg_1075" s="T142">ira</ta>
            <ta e="T144" id="Seg_1076" s="T143">ıllä</ta>
            <ta e="T145" id="Seg_1077" s="T144">qu-mpɨ</ta>
            <ta e="T147" id="Seg_1078" s="T146">loqa</ta>
            <ta e="T148" id="Seg_1079" s="T147">šittälʼ</ta>
            <ta e="T149" id="Seg_1080" s="T148">na</ta>
            <ta e="T150" id="Seg_1081" s="T149">apsɨ-tɨ</ta>
            <ta e="T151" id="Seg_1082" s="T150">am-mpɨ-ntɨ-tɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T8" id="Seg_1083" s="T7">old.man-ADJZ</ta>
            <ta e="T9" id="Seg_1084" s="T8">mother-DYA-DU-%%</ta>
            <ta e="T11" id="Seg_1085" s="T10">earlier</ta>
            <ta e="T12" id="Seg_1086" s="T11">live-PST.NAR-3DU.S</ta>
            <ta e="T13" id="Seg_1087" s="T12">two</ta>
            <ta e="T15" id="Seg_1088" s="T13">old.man-ADJZ</ta>
            <ta e="T16" id="Seg_1089" s="T15">mother-DYA-DU-%%</ta>
            <ta e="T18" id="Seg_1090" s="T17">old.man.[NOM]</ta>
            <ta e="T19" id="Seg_1091" s="T18">winter-ADV.LOC</ta>
            <ta e="T20" id="Seg_1092" s="T19">fish-%%</ta>
            <ta e="T21" id="Seg_1093" s="T20">forest-ILL</ta>
            <ta e="T22" id="Seg_1094" s="T21">go.away-INFER.[3SG.S]</ta>
            <ta e="T24" id="Seg_1095" s="T23">home-%%</ta>
            <ta e="T25" id="Seg_1096" s="T24">come-ACTN-LOC-OBL.3SG</ta>
            <ta e="T26" id="Seg_1097" s="T25">road-LOC</ta>
            <ta e="T27" id="Seg_1098" s="T26">one</ta>
            <ta e="T28" id="Seg_1099" s="T27">fox.[NOM]</ta>
            <ta e="T29" id="Seg_1100" s="T28">find-PST.NAR-INFER-3SG.O</ta>
            <ta e="T31" id="Seg_1101" s="T30">as.if</ta>
            <ta e="T32" id="Seg_1102" s="T31">come-CO.[3SG.S]</ta>
            <ta e="T33" id="Seg_1103" s="T32">be.heard.[3SG.S]</ta>
            <ta e="T34" id="Seg_1104" s="T33">one</ta>
            <ta e="T35" id="Seg_1105" s="T34">fox.[NOM]</ta>
            <ta e="T37" id="Seg_1106" s="T36">sledge-ILL-OBL.3SG</ta>
            <ta e="T38" id="Seg_1107" s="T37">up</ta>
            <ta e="T39" id="Seg_1108" s="T38">load-PST.NAR-3SG.O</ta>
            <ta e="T41" id="Seg_1109" s="T40">home</ta>
            <ta e="T42" id="Seg_1110" s="T41">come-CVB</ta>
            <ta e="T43" id="Seg_1111" s="T42">old.woman-ILL-OBL.3SG</ta>
            <ta e="T44" id="Seg_1112" s="T43">so</ta>
            <ta e="T45" id="Seg_1113" s="T44">say-CO-3SG.O</ta>
            <ta e="T47" id="Seg_1114" s="T46">I.NOM</ta>
            <ta e="T48" id="Seg_1115" s="T47">you.SG.GEN</ta>
            <ta e="T49" id="Seg_1116" s="T48">for</ta>
            <ta e="T50" id="Seg_1117" s="T49">%%</ta>
            <ta e="T51" id="Seg_1118" s="T50">bring-INFER-1SG.O</ta>
            <ta e="T53" id="Seg_1119" s="T52">old.woman.[NOM]</ta>
            <ta e="T54" id="Seg_1120" s="T53">wife.[NOM]</ta>
            <ta e="T55" id="Seg_1121" s="T54">catch-CVB</ta>
            <ta e="T56" id="Seg_1122" s="T55">hardly</ta>
            <ta e="T57" id="Seg_1123" s="T56">pull-INCH-CO-3SG.O</ta>
            <ta e="T59" id="Seg_1124" s="T58">fox.[NOM]</ta>
            <ta e="T60" id="Seg_1125" s="T59">life-TRL-PFV-CVB</ta>
            <ta e="T61" id="Seg_1126" s="T60">(s)he-EP-GEN</ta>
            <ta e="T62" id="Seg_1127" s="T61">knee.[NOM]-3SG</ta>
            <ta e="T63" id="Seg_1128" s="T62">above</ta>
            <ta e="T64" id="Seg_1129" s="T63">up</ta>
            <ta e="T65" id="Seg_1130" s="T64">sit.down-CO.[3SG.S]</ta>
            <ta e="T67" id="Seg_1131" s="T66">old.man.[NOM]</ta>
            <ta e="T68" id="Seg_1132" s="T67">away</ta>
            <ta e="T69" id="Seg_1133" s="T68">give-CVB</ta>
            <ta e="T70" id="Seg_1134" s="T69">hardly</ta>
            <ta e="T71" id="Seg_1135" s="T70">fox-ACC</ta>
            <ta e="T72" id="Seg_1136" s="T71">hit-CO-3SG.O</ta>
            <ta e="T74" id="Seg_1137" s="T73">hit-EP-PST.NAR-3SG.O</ta>
            <ta e="T76" id="Seg_1138" s="T75">old.woman-GEN</ta>
            <ta e="T77" id="Seg_1139" s="T76">knee.[NOM]</ta>
            <ta e="T78" id="Seg_1140" s="T77">hit-PST.NAR-3SG.O</ta>
            <ta e="T80" id="Seg_1141" s="T79">old.woman.[NOM]</ta>
            <ta e="T81" id="Seg_1142" s="T80">let.go-ACTN-LOC</ta>
            <ta e="T82" id="Seg_1143" s="T81">fox.[NOM]</ta>
            <ta e="T83" id="Seg_1144" s="T82">outwards</ta>
            <ta e="T84" id="Seg_1145" s="T83">run.[3SG.S]</ta>
            <ta e="T86" id="Seg_1146" s="T85">old.man.[NOM]</ta>
            <ta e="T87" id="Seg_1147" s="T86">hare-ADJZ</ta>
            <ta e="T88" id="Seg_1148" s="T87">clothing-ACC-3SG</ta>
            <ta e="T89" id="Seg_1149" s="T88">put.on-TR-CVB</ta>
            <ta e="T90" id="Seg_1150" s="T89">fox-ACC</ta>
            <ta e="T91" id="Seg_1151" s="T90">catch.up-IPFV-CO-3SG.O</ta>
            <ta e="T93" id="Seg_1152" s="T92">sometimes</ta>
            <ta e="T94" id="Seg_1153" s="T93">catch.up-INCH-3SG.O</ta>
            <ta e="T96" id="Seg_1154" s="T95">long-ADVZ</ta>
            <ta e="T97" id="Seg_1155" s="T96">catch.up-INCH-3SG.O</ta>
            <ta e="T99" id="Seg_1156" s="T98">catch.up-INCH-3SG.O</ta>
            <ta e="T101" id="Seg_1157" s="T100">warm-CVB</ta>
            <ta e="T102" id="Seg_1158" s="T101">%%</ta>
            <ta e="T103" id="Seg_1159" s="T102">die-CO.[3SG.S]</ta>
            <ta e="T105" id="Seg_1160" s="T104">one</ta>
            <ta e="T106" id="Seg_1161" s="T105">middle.[NOM]</ta>
            <ta e="T107" id="Seg_1162" s="T106">clothing-ACC-3SG</ta>
            <ta e="T108" id="Seg_1163" s="T107">away</ta>
            <ta e="T109" id="Seg_1164" s="T108">away</ta>
            <ta e="T110" id="Seg_1165" s="T109">throw-CO-3SG.O</ta>
            <ta e="T112" id="Seg_1166" s="T111">what-ADJZ</ta>
            <ta e="T113" id="Seg_1167" s="T112">clothing.[NOM]</ta>
            <ta e="T114" id="Seg_1168" s="T113">tremble-%%</ta>
            <ta e="T115" id="Seg_1169" s="T114">catch.up-IPFV-PST.NAR-3SG.O</ta>
            <ta e="T117" id="Seg_1170" s="T116">again</ta>
            <ta e="T118" id="Seg_1171" s="T117">long-ADVZ</ta>
            <ta e="T119" id="Seg_1172" s="T118">catch.up-PST.NAR-3SG.O</ta>
            <ta e="T121" id="Seg_1173" s="T120">one</ta>
            <ta e="T122" id="Seg_1174" s="T121">middle-LOC</ta>
            <ta e="T123" id="Seg_1175" s="T122">(s)he.[NOM]</ta>
            <ta e="T124" id="Seg_1176" s="T123">fall.behind-RFL.PFV-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T126" id="Seg_1177" s="T125">thus.much</ta>
            <ta e="T127" id="Seg_1178" s="T126">cold-ADVZ</ta>
            <ta e="T128" id="Seg_1179" s="T127">become-PST.NAR.[3SG.S]</ta>
            <ta e="T130" id="Seg_1180" s="T129">this</ta>
            <ta e="T131" id="Seg_1181" s="T130">warm-PTCP.PST</ta>
            <ta e="T132" id="Seg_1182" s="T131">old.man.[NOM]-1SG</ta>
            <ta e="T133" id="Seg_1183" s="T132">down</ta>
            <ta e="T134" id="Seg_1184" s="T133">freeze-RFL.PFV-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T136" id="Seg_1185" s="T135">fox.[NOM]</ta>
            <ta e="T137" id="Seg_1186" s="T136">afterwards</ta>
            <ta e="T138" id="Seg_1187" s="T137">then</ta>
            <ta e="T139" id="Seg_1188" s="T138">back</ta>
            <ta e="T140" id="Seg_1189" s="T139">hardly</ta>
            <ta e="T141" id="Seg_1190" s="T140">come-PST.NAR.[3SG.S]</ta>
            <ta e="T143" id="Seg_1191" s="T142">old.man.[NOM]</ta>
            <ta e="T144" id="Seg_1192" s="T143">down</ta>
            <ta e="T145" id="Seg_1193" s="T144">die-PST.NAR.[3SG.S]</ta>
            <ta e="T147" id="Seg_1194" s="T146">fox.[NOM]</ta>
            <ta e="T148" id="Seg_1195" s="T147">then</ta>
            <ta e="T149" id="Seg_1196" s="T148">this</ta>
            <ta e="T150" id="Seg_1197" s="T149">food.[NOM]-3SG</ta>
            <ta e="T151" id="Seg_1198" s="T150">eat-PST.NAR-INFER-3SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T8" id="Seg_1199" s="T7">старик-ADJZ</ta>
            <ta e="T9" id="Seg_1200" s="T8">мать-DYA-DU-%%</ta>
            <ta e="T11" id="Seg_1201" s="T10">раньше</ta>
            <ta e="T12" id="Seg_1202" s="T11">жить-PST.NAR-3DU.S</ta>
            <ta e="T13" id="Seg_1203" s="T12">два</ta>
            <ta e="T15" id="Seg_1204" s="T13">старик-ADJZ</ta>
            <ta e="T16" id="Seg_1205" s="T15">мать-DYA-DU-%%</ta>
            <ta e="T18" id="Seg_1206" s="T17">старик.[NOM]</ta>
            <ta e="T19" id="Seg_1207" s="T18">зима-ADV.LOC</ta>
            <ta e="T20" id="Seg_1208" s="T19">рыба-%%</ta>
            <ta e="T21" id="Seg_1209" s="T20">лес-ILL</ta>
            <ta e="T22" id="Seg_1210" s="T21">уйти-INFER.[3SG.S]</ta>
            <ta e="T24" id="Seg_1211" s="T23">домой-%%</ta>
            <ta e="T25" id="Seg_1212" s="T24">прийти-ACTN-LOC-OBL.3SG</ta>
            <ta e="T26" id="Seg_1213" s="T25">дорога-LOC</ta>
            <ta e="T27" id="Seg_1214" s="T26">один</ta>
            <ta e="T28" id="Seg_1215" s="T27">лисица.[NOM]</ta>
            <ta e="T29" id="Seg_1216" s="T28">найти-PST.NAR-INFER-3SG.O</ta>
            <ta e="T31" id="Seg_1217" s="T30">будто</ta>
            <ta e="T32" id="Seg_1218" s="T31">прийти-CO.[3SG.S]</ta>
            <ta e="T33" id="Seg_1219" s="T32">слышаться.[3SG.S]</ta>
            <ta e="T34" id="Seg_1220" s="T33">один</ta>
            <ta e="T35" id="Seg_1221" s="T34">лисица.[NOM]</ta>
            <ta e="T37" id="Seg_1222" s="T36">нарты-ILL-OBL.3SG</ta>
            <ta e="T38" id="Seg_1223" s="T37">вверх</ta>
            <ta e="T39" id="Seg_1224" s="T38">погрузить-PST.NAR-3SG.O</ta>
            <ta e="T41" id="Seg_1225" s="T40">домой</ta>
            <ta e="T42" id="Seg_1226" s="T41">прийти-CVB</ta>
            <ta e="T43" id="Seg_1227" s="T42">старуха-ILL-OBL.3SG</ta>
            <ta e="T44" id="Seg_1228" s="T43">так</ta>
            <ta e="T45" id="Seg_1229" s="T44">сказать-CO-3SG.O</ta>
            <ta e="T47" id="Seg_1230" s="T46">я.NOM</ta>
            <ta e="T48" id="Seg_1231" s="T47">ты.GEN</ta>
            <ta e="T49" id="Seg_1232" s="T48">для</ta>
            <ta e="T50" id="Seg_1233" s="T49">%%</ta>
            <ta e="T51" id="Seg_1234" s="T50">принести-INFER-1SG.O</ta>
            <ta e="T53" id="Seg_1235" s="T52">старуха.[NOM]</ta>
            <ta e="T54" id="Seg_1236" s="T53">жена.[NOM]</ta>
            <ta e="T55" id="Seg_1237" s="T54">схватить-CVB</ta>
            <ta e="T56" id="Seg_1238" s="T55">едва</ta>
            <ta e="T57" id="Seg_1239" s="T56">тянуть-INCH-CO-3SG.O</ta>
            <ta e="T59" id="Seg_1240" s="T58">лисица.[NOM]</ta>
            <ta e="T60" id="Seg_1241" s="T59">жизнь-TRL-PFV-CVB</ta>
            <ta e="T61" id="Seg_1242" s="T60">он(а)-EP-GEN</ta>
            <ta e="T62" id="Seg_1243" s="T61">колено.[NOM]-3SG</ta>
            <ta e="T63" id="Seg_1244" s="T62">над</ta>
            <ta e="T64" id="Seg_1245" s="T63">вверх</ta>
            <ta e="T65" id="Seg_1246" s="T64">сесть-CO.[3SG.S]</ta>
            <ta e="T67" id="Seg_1247" s="T66">старик.[NOM]</ta>
            <ta e="T68" id="Seg_1248" s="T67">прочь</ta>
            <ta e="T69" id="Seg_1249" s="T68">дать-CVB</ta>
            <ta e="T70" id="Seg_1250" s="T69">едва</ta>
            <ta e="T71" id="Seg_1251" s="T70">лисица-ACC</ta>
            <ta e="T72" id="Seg_1252" s="T71">ударить-CO-3SG.O</ta>
            <ta e="T74" id="Seg_1253" s="T73">попасть-EP-PST.NAR-3SG.O</ta>
            <ta e="T76" id="Seg_1254" s="T75">старуха-GEN</ta>
            <ta e="T77" id="Seg_1255" s="T76">колено.[NOM]</ta>
            <ta e="T78" id="Seg_1256" s="T77">ударить-PST.NAR-3SG.O</ta>
            <ta e="T80" id="Seg_1257" s="T79">старуха.[NOM]</ta>
            <ta e="T81" id="Seg_1258" s="T80">пустить-ACTN-LOC</ta>
            <ta e="T82" id="Seg_1259" s="T81">лисица.[NOM]</ta>
            <ta e="T83" id="Seg_1260" s="T82">наружу</ta>
            <ta e="T84" id="Seg_1261" s="T83">побежать.[3SG.S]</ta>
            <ta e="T86" id="Seg_1262" s="T85">старик.[NOM]</ta>
            <ta e="T87" id="Seg_1263" s="T86">заяц-ADJZ</ta>
            <ta e="T88" id="Seg_1264" s="T87">одежда-ACC-3SG</ta>
            <ta e="T89" id="Seg_1265" s="T88">надеть-TR-CVB</ta>
            <ta e="T90" id="Seg_1266" s="T89">лисица-ACC</ta>
            <ta e="T91" id="Seg_1267" s="T90">догонять-IPFV-CO-3SG.O</ta>
            <ta e="T93" id="Seg_1268" s="T92">некоторое.время</ta>
            <ta e="T94" id="Seg_1269" s="T93">догонять-INCH-3SG.O</ta>
            <ta e="T96" id="Seg_1270" s="T95">долго-ADVZ</ta>
            <ta e="T97" id="Seg_1271" s="T96">догонять-INCH-3SG.O</ta>
            <ta e="T99" id="Seg_1272" s="T98">догонять-INCH-3SG.O</ta>
            <ta e="T101" id="Seg_1273" s="T100">нагреться-CVB</ta>
            <ta e="T102" id="Seg_1274" s="T101">%%</ta>
            <ta e="T103" id="Seg_1275" s="T102">умереть-CO.[3SG.S]</ta>
            <ta e="T105" id="Seg_1276" s="T104">один</ta>
            <ta e="T106" id="Seg_1277" s="T105">середина.[NOM]</ta>
            <ta e="T107" id="Seg_1278" s="T106">одежда-ACC-3SG</ta>
            <ta e="T108" id="Seg_1279" s="T107">прочь</ta>
            <ta e="T109" id="Seg_1280" s="T108">прочь</ta>
            <ta e="T110" id="Seg_1281" s="T109">бросить-CO-3SG.O</ta>
            <ta e="T112" id="Seg_1282" s="T111">что-ADJZ</ta>
            <ta e="T113" id="Seg_1283" s="T112">одежда.[NOM]</ta>
            <ta e="T114" id="Seg_1284" s="T113">задрожать-%%</ta>
            <ta e="T115" id="Seg_1285" s="T114">догонять-IPFV-PST.NAR-3SG.O</ta>
            <ta e="T117" id="Seg_1286" s="T116">опять</ta>
            <ta e="T118" id="Seg_1287" s="T117">долго-ADVZ</ta>
            <ta e="T119" id="Seg_1288" s="T118">догонять-PST.NAR-3SG.O</ta>
            <ta e="T121" id="Seg_1289" s="T120">один</ta>
            <ta e="T122" id="Seg_1290" s="T121">середина-LOC</ta>
            <ta e="T123" id="Seg_1291" s="T122">он(а).[NOM]</ta>
            <ta e="T124" id="Seg_1292" s="T123">отстать-RFL.PFV-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T126" id="Seg_1293" s="T125">настолько</ta>
            <ta e="T127" id="Seg_1294" s="T126">холодный-ADVZ</ta>
            <ta e="T128" id="Seg_1295" s="T127">стать-PST.NAR.[3SG.S]</ta>
            <ta e="T130" id="Seg_1296" s="T129">этот</ta>
            <ta e="T131" id="Seg_1297" s="T130">нагреться-PTCP.PST</ta>
            <ta e="T132" id="Seg_1298" s="T131">старик.[NOM]-1SG</ta>
            <ta e="T133" id="Seg_1299" s="T132">вниз</ta>
            <ta e="T134" id="Seg_1300" s="T133">замерзнуть-RFL.PFV-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T136" id="Seg_1301" s="T135">лисица.[NOM]</ta>
            <ta e="T137" id="Seg_1302" s="T136">потом</ta>
            <ta e="T138" id="Seg_1303" s="T137">потом</ta>
            <ta e="T139" id="Seg_1304" s="T138">назад</ta>
            <ta e="T140" id="Seg_1305" s="T139">едва</ta>
            <ta e="T141" id="Seg_1306" s="T140">прийти-PST.NAR.[3SG.S]</ta>
            <ta e="T143" id="Seg_1307" s="T142">старик.[NOM]</ta>
            <ta e="T144" id="Seg_1308" s="T143">вниз</ta>
            <ta e="T145" id="Seg_1309" s="T144">умереть-PST.NAR.[3SG.S]</ta>
            <ta e="T147" id="Seg_1310" s="T146">лисица.[NOM]</ta>
            <ta e="T148" id="Seg_1311" s="T147">потом</ta>
            <ta e="T149" id="Seg_1312" s="T148">этот</ta>
            <ta e="T150" id="Seg_1313" s="T149">еда.[NOM]-3SG</ta>
            <ta e="T151" id="Seg_1314" s="T150">съесть-PST.NAR-INFER-3SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T8" id="Seg_1315" s="T7">n-n&gt;adj</ta>
            <ta e="T9" id="Seg_1316" s="T8">n-n&gt;n-n:num-%%</ta>
            <ta e="T11" id="Seg_1317" s="T10">adv</ta>
            <ta e="T12" id="Seg_1318" s="T11">v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_1319" s="T12">num</ta>
            <ta e="T15" id="Seg_1320" s="T13">n-n&gt;adj</ta>
            <ta e="T16" id="Seg_1321" s="T15">n-n&gt;n-n:num-%%</ta>
            <ta e="T18" id="Seg_1322" s="T17">n.[n:case]</ta>
            <ta e="T19" id="Seg_1323" s="T18">n-n&gt;adv</ta>
            <ta e="T20" id="Seg_1324" s="T19">n-%%</ta>
            <ta e="T21" id="Seg_1325" s="T20">n-n:case</ta>
            <ta e="T22" id="Seg_1326" s="T21">v-v:tense.mood.[v:pn]</ta>
            <ta e="T24" id="Seg_1327" s="T23">adv-%%</ta>
            <ta e="T25" id="Seg_1328" s="T24">v-v&gt;n-n:case-n:obl.poss</ta>
            <ta e="T26" id="Seg_1329" s="T25">n-n:case</ta>
            <ta e="T27" id="Seg_1330" s="T26">num</ta>
            <ta e="T28" id="Seg_1331" s="T27">n.[n:case]</ta>
            <ta e="T29" id="Seg_1332" s="T28">v-v:tense-v:mood-v:pn</ta>
            <ta e="T31" id="Seg_1333" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_1334" s="T31">v-v:ins.[v:pn]</ta>
            <ta e="T33" id="Seg_1335" s="T32">v.[v:pn]</ta>
            <ta e="T34" id="Seg_1336" s="T33">num</ta>
            <ta e="T35" id="Seg_1337" s="T34">n.[n:case]</ta>
            <ta e="T37" id="Seg_1338" s="T36">n-n:case-n:obl.poss</ta>
            <ta e="T38" id="Seg_1339" s="T37">preverb</ta>
            <ta e="T39" id="Seg_1340" s="T38">v-v:tense-v:pn</ta>
            <ta e="T41" id="Seg_1341" s="T40">adv</ta>
            <ta e="T42" id="Seg_1342" s="T41">v-v&gt;adv</ta>
            <ta e="T43" id="Seg_1343" s="T42">n-n:case-n:obl.poss</ta>
            <ta e="T44" id="Seg_1344" s="T43">adv</ta>
            <ta e="T45" id="Seg_1345" s="T44">v-v:ins-v:pn</ta>
            <ta e="T47" id="Seg_1346" s="T46">pers</ta>
            <ta e="T48" id="Seg_1347" s="T47">pers</ta>
            <ta e="T49" id="Seg_1348" s="T48">pp</ta>
            <ta e="T50" id="Seg_1349" s="T49">%%</ta>
            <ta e="T51" id="Seg_1350" s="T50">v-v:tense.mood-v:pn</ta>
            <ta e="T53" id="Seg_1351" s="T52">n.[n:case]</ta>
            <ta e="T54" id="Seg_1352" s="T53">n.[n:case]</ta>
            <ta e="T55" id="Seg_1353" s="T54">v-v&gt;adv</ta>
            <ta e="T56" id="Seg_1354" s="T55">ptcl</ta>
            <ta e="T57" id="Seg_1355" s="T56">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T59" id="Seg_1356" s="T58">n.[n:case]</ta>
            <ta e="T60" id="Seg_1357" s="T59">n-n&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T61" id="Seg_1358" s="T60">pers-n:ins-n:case</ta>
            <ta e="T62" id="Seg_1359" s="T61">n.[n:case]-n:poss</ta>
            <ta e="T63" id="Seg_1360" s="T62">pp</ta>
            <ta e="T64" id="Seg_1361" s="T63">preverb</ta>
            <ta e="T65" id="Seg_1362" s="T64">v-v:ins.[v:pn]</ta>
            <ta e="T67" id="Seg_1363" s="T66">n.[n:case]</ta>
            <ta e="T68" id="Seg_1364" s="T67">preverb</ta>
            <ta e="T69" id="Seg_1365" s="T68">v-v&gt;adv</ta>
            <ta e="T70" id="Seg_1366" s="T69">ptcl</ta>
            <ta e="T71" id="Seg_1367" s="T70">n-n:case</ta>
            <ta e="T72" id="Seg_1368" s="T71">v-v:ins-v:pn</ta>
            <ta e="T74" id="Seg_1369" s="T73">v-v:ins-v:tense-v:pn</ta>
            <ta e="T76" id="Seg_1370" s="T75">n-n:case</ta>
            <ta e="T77" id="Seg_1371" s="T76">n.[n:case]</ta>
            <ta e="T78" id="Seg_1372" s="T77">v-v:tense-v:pn</ta>
            <ta e="T80" id="Seg_1373" s="T79">n.[n:case]</ta>
            <ta e="T81" id="Seg_1374" s="T80">v-v&gt;n-n:case</ta>
            <ta e="T82" id="Seg_1375" s="T81">n.[n:case]</ta>
            <ta e="T83" id="Seg_1376" s="T82">adv</ta>
            <ta e="T84" id="Seg_1377" s="T83">v.[v:pn]</ta>
            <ta e="T86" id="Seg_1378" s="T85">n.[n:case]</ta>
            <ta e="T87" id="Seg_1379" s="T86">n-n&gt;adj</ta>
            <ta e="T88" id="Seg_1380" s="T87">n-n:case-n:poss</ta>
            <ta e="T89" id="Seg_1381" s="T88">v-v&gt;v-v&gt;adv</ta>
            <ta e="T90" id="Seg_1382" s="T89">n-n:case</ta>
            <ta e="T91" id="Seg_1383" s="T90">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T93" id="Seg_1384" s="T92">adv</ta>
            <ta e="T94" id="Seg_1385" s="T93">v-v&gt;v-v:pn</ta>
            <ta e="T96" id="Seg_1386" s="T95">adv-adj&gt;adv</ta>
            <ta e="T97" id="Seg_1387" s="T96">v-v&gt;v-v:pn</ta>
            <ta e="T99" id="Seg_1388" s="T98">v-v&gt;v-v:pn</ta>
            <ta e="T101" id="Seg_1389" s="T100">v-v&gt;adv</ta>
            <ta e="T102" id="Seg_1390" s="T101">%%</ta>
            <ta e="T103" id="Seg_1391" s="T102">v-v:ins.[v:pn]</ta>
            <ta e="T105" id="Seg_1392" s="T104">num</ta>
            <ta e="T106" id="Seg_1393" s="T105">n.[n:case]</ta>
            <ta e="T107" id="Seg_1394" s="T106">n-n:case-n:poss</ta>
            <ta e="T108" id="Seg_1395" s="T107">adv</ta>
            <ta e="T109" id="Seg_1396" s="T108">preverb</ta>
            <ta e="T110" id="Seg_1397" s="T109">v-v:ins-v:pn</ta>
            <ta e="T112" id="Seg_1398" s="T111">interrog-n&gt;adj</ta>
            <ta e="T113" id="Seg_1399" s="T112">n.[n:case]</ta>
            <ta e="T114" id="Seg_1400" s="T113">v-%%</ta>
            <ta e="T115" id="Seg_1401" s="T114">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T117" id="Seg_1402" s="T116">adv</ta>
            <ta e="T118" id="Seg_1403" s="T117">adv-adj&gt;adv</ta>
            <ta e="T119" id="Seg_1404" s="T118">v-v:tense-v:pn</ta>
            <ta e="T121" id="Seg_1405" s="T120">num</ta>
            <ta e="T122" id="Seg_1406" s="T121">n-n:case</ta>
            <ta e="T123" id="Seg_1407" s="T122">pers.[n:case]</ta>
            <ta e="T124" id="Seg_1408" s="T123">v-v&gt;v-v:tense-v:mood.[v:pn]</ta>
            <ta e="T126" id="Seg_1409" s="T125">adv</ta>
            <ta e="T127" id="Seg_1410" s="T126">adj-adj&gt;adv</ta>
            <ta e="T128" id="Seg_1411" s="T127">v-v:tense.[v:pn]</ta>
            <ta e="T130" id="Seg_1412" s="T129">dem</ta>
            <ta e="T131" id="Seg_1413" s="T130">v-v&gt;ptcp</ta>
            <ta e="T132" id="Seg_1414" s="T131">n.[n:case]-n:poss</ta>
            <ta e="T133" id="Seg_1415" s="T132">preverb</ta>
            <ta e="T134" id="Seg_1416" s="T133">v-v&gt;v-v:tense-v:mood.[v:pn]</ta>
            <ta e="T136" id="Seg_1417" s="T135">n.[n:case]</ta>
            <ta e="T137" id="Seg_1418" s="T136">adv</ta>
            <ta e="T138" id="Seg_1419" s="T137">adv</ta>
            <ta e="T139" id="Seg_1420" s="T138">adv</ta>
            <ta e="T140" id="Seg_1421" s="T139">ptcl</ta>
            <ta e="T141" id="Seg_1422" s="T140">v-v:tense.[v:pn]</ta>
            <ta e="T143" id="Seg_1423" s="T142">n.[n:case]</ta>
            <ta e="T144" id="Seg_1424" s="T143">preverb</ta>
            <ta e="T145" id="Seg_1425" s="T144">v-v:tense.[v:pn]</ta>
            <ta e="T147" id="Seg_1426" s="T146">n.[n:case]</ta>
            <ta e="T148" id="Seg_1427" s="T147">adv</ta>
            <ta e="T149" id="Seg_1428" s="T148">dem</ta>
            <ta e="T150" id="Seg_1429" s="T149">n.[n:case]-n:poss</ta>
            <ta e="T151" id="Seg_1430" s="T150">v-v:tense-v:mood-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T8" id="Seg_1431" s="T7">adj</ta>
            <ta e="T9" id="Seg_1432" s="T8">n</ta>
            <ta e="T11" id="Seg_1433" s="T10">adv</ta>
            <ta e="T12" id="Seg_1434" s="T11">v</ta>
            <ta e="T13" id="Seg_1435" s="T12">num</ta>
            <ta e="T15" id="Seg_1436" s="T13">adj</ta>
            <ta e="T16" id="Seg_1437" s="T15">n</ta>
            <ta e="T18" id="Seg_1438" s="T17">n</ta>
            <ta e="T19" id="Seg_1439" s="T18">adv</ta>
            <ta e="T20" id="Seg_1440" s="T19">v</ta>
            <ta e="T21" id="Seg_1441" s="T20">n</ta>
            <ta e="T22" id="Seg_1442" s="T21">v</ta>
            <ta e="T24" id="Seg_1443" s="T23">adv</ta>
            <ta e="T25" id="Seg_1444" s="T24">n</ta>
            <ta e="T26" id="Seg_1445" s="T25">n</ta>
            <ta e="T27" id="Seg_1446" s="T26">num</ta>
            <ta e="T28" id="Seg_1447" s="T27">n</ta>
            <ta e="T29" id="Seg_1448" s="T28">v</ta>
            <ta e="T31" id="Seg_1449" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_1450" s="T31">v</ta>
            <ta e="T33" id="Seg_1451" s="T32">v</ta>
            <ta e="T34" id="Seg_1452" s="T33">num</ta>
            <ta e="T35" id="Seg_1453" s="T34">n</ta>
            <ta e="T37" id="Seg_1454" s="T36">n</ta>
            <ta e="T38" id="Seg_1455" s="T37">preverb</ta>
            <ta e="T39" id="Seg_1456" s="T38">v</ta>
            <ta e="T41" id="Seg_1457" s="T40">adv</ta>
            <ta e="T42" id="Seg_1458" s="T41">adv</ta>
            <ta e="T43" id="Seg_1459" s="T42">n</ta>
            <ta e="T44" id="Seg_1460" s="T43">adv</ta>
            <ta e="T45" id="Seg_1461" s="T44">v</ta>
            <ta e="T47" id="Seg_1462" s="T46">pers</ta>
            <ta e="T48" id="Seg_1463" s="T47">pers</ta>
            <ta e="T49" id="Seg_1464" s="T48">pp</ta>
            <ta e="T51" id="Seg_1465" s="T50">v</ta>
            <ta e="T53" id="Seg_1466" s="T52">n</ta>
            <ta e="T54" id="Seg_1467" s="T53">n</ta>
            <ta e="T55" id="Seg_1468" s="T54">adv</ta>
            <ta e="T56" id="Seg_1469" s="T55">adv</ta>
            <ta e="T57" id="Seg_1470" s="T56">v</ta>
            <ta e="T59" id="Seg_1471" s="T58">n</ta>
            <ta e="T60" id="Seg_1472" s="T59">adv</ta>
            <ta e="T61" id="Seg_1473" s="T60">pers</ta>
            <ta e="T62" id="Seg_1474" s="T61">n</ta>
            <ta e="T63" id="Seg_1475" s="T62">pp</ta>
            <ta e="T64" id="Seg_1476" s="T63">preverb</ta>
            <ta e="T65" id="Seg_1477" s="T64">v</ta>
            <ta e="T67" id="Seg_1478" s="T66">n</ta>
            <ta e="T68" id="Seg_1479" s="T67">preverb</ta>
            <ta e="T69" id="Seg_1480" s="T68">adv</ta>
            <ta e="T70" id="Seg_1481" s="T69">conj</ta>
            <ta e="T71" id="Seg_1482" s="T70">n</ta>
            <ta e="T72" id="Seg_1483" s="T71">v</ta>
            <ta e="T74" id="Seg_1484" s="T73">v</ta>
            <ta e="T76" id="Seg_1485" s="T75">n</ta>
            <ta e="T77" id="Seg_1486" s="T76">n</ta>
            <ta e="T78" id="Seg_1487" s="T77">v</ta>
            <ta e="T80" id="Seg_1488" s="T79">n</ta>
            <ta e="T81" id="Seg_1489" s="T80">n</ta>
            <ta e="T82" id="Seg_1490" s="T81">n</ta>
            <ta e="T83" id="Seg_1491" s="T82">adv</ta>
            <ta e="T84" id="Seg_1492" s="T83">v</ta>
            <ta e="T86" id="Seg_1493" s="T85">n</ta>
            <ta e="T87" id="Seg_1494" s="T86">adj</ta>
            <ta e="T88" id="Seg_1495" s="T87">n</ta>
            <ta e="T89" id="Seg_1496" s="T88">adv</ta>
            <ta e="T90" id="Seg_1497" s="T89">n</ta>
            <ta e="T91" id="Seg_1498" s="T90">v</ta>
            <ta e="T93" id="Seg_1499" s="T92">adv</ta>
            <ta e="T94" id="Seg_1500" s="T93">v</ta>
            <ta e="T96" id="Seg_1501" s="T95">adv</ta>
            <ta e="T97" id="Seg_1502" s="T96">v</ta>
            <ta e="T99" id="Seg_1503" s="T98">v</ta>
            <ta e="T101" id="Seg_1504" s="T100">adv</ta>
            <ta e="T103" id="Seg_1505" s="T102">v</ta>
            <ta e="T105" id="Seg_1506" s="T104">num</ta>
            <ta e="T106" id="Seg_1507" s="T105">n</ta>
            <ta e="T107" id="Seg_1508" s="T106">n</ta>
            <ta e="T108" id="Seg_1509" s="T107">adv</ta>
            <ta e="T109" id="Seg_1510" s="T108">preverb</ta>
            <ta e="T110" id="Seg_1511" s="T109">v</ta>
            <ta e="T112" id="Seg_1512" s="T111">adj</ta>
            <ta e="T113" id="Seg_1513" s="T112">n</ta>
            <ta e="T114" id="Seg_1514" s="T113">v</ta>
            <ta e="T115" id="Seg_1515" s="T114">v</ta>
            <ta e="T117" id="Seg_1516" s="T116">adv</ta>
            <ta e="T118" id="Seg_1517" s="T117">adv</ta>
            <ta e="T119" id="Seg_1518" s="T118">v</ta>
            <ta e="T121" id="Seg_1519" s="T120">num</ta>
            <ta e="T122" id="Seg_1520" s="T121">n</ta>
            <ta e="T123" id="Seg_1521" s="T122">pers</ta>
            <ta e="T124" id="Seg_1522" s="T123">v</ta>
            <ta e="T126" id="Seg_1523" s="T125">adv</ta>
            <ta e="T127" id="Seg_1524" s="T126">adv</ta>
            <ta e="T128" id="Seg_1525" s="T127">v</ta>
            <ta e="T130" id="Seg_1526" s="T129">dem</ta>
            <ta e="T131" id="Seg_1527" s="T130">ptcp</ta>
            <ta e="T132" id="Seg_1528" s="T131">n</ta>
            <ta e="T133" id="Seg_1529" s="T132">preverb</ta>
            <ta e="T134" id="Seg_1530" s="T133">v</ta>
            <ta e="T136" id="Seg_1531" s="T135">n</ta>
            <ta e="T137" id="Seg_1532" s="T136">adv</ta>
            <ta e="T138" id="Seg_1533" s="T137">adv</ta>
            <ta e="T139" id="Seg_1534" s="T138">adv</ta>
            <ta e="T140" id="Seg_1535" s="T139">conj</ta>
            <ta e="T141" id="Seg_1536" s="T140">v</ta>
            <ta e="T143" id="Seg_1537" s="T142">n</ta>
            <ta e="T144" id="Seg_1538" s="T143">preverb</ta>
            <ta e="T145" id="Seg_1539" s="T144">v</ta>
            <ta e="T147" id="Seg_1540" s="T146">n</ta>
            <ta e="T148" id="Seg_1541" s="T147">adv</ta>
            <ta e="T149" id="Seg_1542" s="T148">dem</ta>
            <ta e="T150" id="Seg_1543" s="T149">n</ta>
            <ta e="T151" id="Seg_1544" s="T150">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T11" id="Seg_1545" s="T10">adv:Time</ta>
            <ta e="T16" id="Seg_1546" s="T15">np.h:Th</ta>
            <ta e="T18" id="Seg_1547" s="T17">np.h:A</ta>
            <ta e="T19" id="Seg_1548" s="T18">adv:Time</ta>
            <ta e="T21" id="Seg_1549" s="T20">np:G</ta>
            <ta e="T24" id="Seg_1550" s="T23">adv:G</ta>
            <ta e="T25" id="Seg_1551" s="T24">0.3.h:A</ta>
            <ta e="T26" id="Seg_1552" s="T25">np:L</ta>
            <ta e="T28" id="Seg_1553" s="T27">np:Th</ta>
            <ta e="T29" id="Seg_1554" s="T28">0.3.h:B</ta>
            <ta e="T35" id="Seg_1555" s="T34">np:A</ta>
            <ta e="T37" id="Seg_1556" s="T36">np:G 0.3.h:Poss</ta>
            <ta e="T39" id="Seg_1557" s="T38">0.3.h:A 0.3:Th</ta>
            <ta e="T41" id="Seg_1558" s="T40">adv:G</ta>
            <ta e="T42" id="Seg_1559" s="T41">0.3.h:A</ta>
            <ta e="T43" id="Seg_1560" s="T42">np.h:R 0.3.h:Poss</ta>
            <ta e="T45" id="Seg_1561" s="T44">0.3.h:A</ta>
            <ta e="T47" id="Seg_1562" s="T46">pro.h:A</ta>
            <ta e="T48" id="Seg_1563" s="T47">pro.h:R</ta>
            <ta e="T51" id="Seg_1564" s="T50">0.3:Th</ta>
            <ta e="T53" id="Seg_1565" s="T52">np.h:A</ta>
            <ta e="T55" id="Seg_1566" s="T54">0.3.h:A</ta>
            <ta e="T57" id="Seg_1567" s="T56">0.3:Th</ta>
            <ta e="T59" id="Seg_1568" s="T58">np:A</ta>
            <ta e="T60" id="Seg_1569" s="T59">0.3:P</ta>
            <ta e="T61" id="Seg_1570" s="T60">pro.h:Poss</ta>
            <ta e="T62" id="Seg_1571" s="T61">pp:G</ta>
            <ta e="T67" id="Seg_1572" s="T66">np.h:A</ta>
            <ta e="T69" id="Seg_1573" s="T68">0.3.h:A</ta>
            <ta e="T71" id="Seg_1574" s="T70">np:P</ta>
            <ta e="T74" id="Seg_1575" s="T73">0.3.h:A 0.3:P</ta>
            <ta e="T76" id="Seg_1576" s="T75">np.h:Poss</ta>
            <ta e="T77" id="Seg_1577" s="T76">np:P</ta>
            <ta e="T78" id="Seg_1578" s="T77">0.3.h:A</ta>
            <ta e="T80" id="Seg_1579" s="T79">np.h:A</ta>
            <ta e="T82" id="Seg_1580" s="T81">np:A</ta>
            <ta e="T83" id="Seg_1581" s="T82">adv:G</ta>
            <ta e="T86" id="Seg_1582" s="T85">np.h:A</ta>
            <ta e="T88" id="Seg_1583" s="T87">np:Th 0.3.h:Poss</ta>
            <ta e="T89" id="Seg_1584" s="T88">0.3.h:A</ta>
            <ta e="T90" id="Seg_1585" s="T89">np:Th</ta>
            <ta e="T94" id="Seg_1586" s="T93">0.3.h:A 0.3:Th</ta>
            <ta e="T96" id="Seg_1587" s="T95">adv:Time</ta>
            <ta e="T97" id="Seg_1588" s="T96">0.3.h:A 0.3:Th</ta>
            <ta e="T99" id="Seg_1589" s="T98">0.3.h:A 0.3:Th</ta>
            <ta e="T103" id="Seg_1590" s="T102">0.3.h:P</ta>
            <ta e="T106" id="Seg_1591" s="T105">np:Time</ta>
            <ta e="T107" id="Seg_1592" s="T106">np:Th 0.3.h:Poss</ta>
            <ta e="T110" id="Seg_1593" s="T109">0.3.h:A</ta>
            <ta e="T115" id="Seg_1594" s="T114">0.3.h:A 0.3:Th</ta>
            <ta e="T118" id="Seg_1595" s="T117">adv:Time</ta>
            <ta e="T119" id="Seg_1596" s="T118">0.3.h:A 0.3:Th</ta>
            <ta e="T122" id="Seg_1597" s="T121">np:Time</ta>
            <ta e="T123" id="Seg_1598" s="T122">pro.h:Th</ta>
            <ta e="T128" id="Seg_1599" s="T127">0.3.h:E</ta>
            <ta e="T132" id="Seg_1600" s="T131">np.h:E</ta>
            <ta e="T136" id="Seg_1601" s="T135">np:A</ta>
            <ta e="T138" id="Seg_1602" s="T137">adv:Time</ta>
            <ta e="T139" id="Seg_1603" s="T138">adv:G</ta>
            <ta e="T143" id="Seg_1604" s="T142">np.h:P</ta>
            <ta e="T147" id="Seg_1605" s="T146">np:A</ta>
            <ta e="T148" id="Seg_1606" s="T147">adv:Time</ta>
            <ta e="T150" id="Seg_1607" s="T149">np:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T12" id="Seg_1608" s="T11">0.3.h:S v:pred</ta>
            <ta e="T18" id="Seg_1609" s="T17">np.h:S</ta>
            <ta e="T22" id="Seg_1610" s="T21">v:pred</ta>
            <ta e="T25" id="Seg_1611" s="T23">s:temp</ta>
            <ta e="T28" id="Seg_1612" s="T27">np:O</ta>
            <ta e="T29" id="Seg_1613" s="T28">0.3.h:S v:pred</ta>
            <ta e="T32" id="Seg_1614" s="T31">v:pred</ta>
            <ta e="T35" id="Seg_1615" s="T34">np:S</ta>
            <ta e="T39" id="Seg_1616" s="T38">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T42" id="Seg_1617" s="T40">s:temp</ta>
            <ta e="T45" id="Seg_1618" s="T44">0.3.h:S v:pred</ta>
            <ta e="T47" id="Seg_1619" s="T46">pro.h:S</ta>
            <ta e="T51" id="Seg_1620" s="T50">v:pred 0.3:O</ta>
            <ta e="T53" id="Seg_1621" s="T52">np.h:S</ta>
            <ta e="T55" id="Seg_1622" s="T54">s:temp</ta>
            <ta e="T57" id="Seg_1623" s="T56">v:pred 0.3:O</ta>
            <ta e="T59" id="Seg_1624" s="T58">np:S</ta>
            <ta e="T60" id="Seg_1625" s="T59">s:temp</ta>
            <ta e="T65" id="Seg_1626" s="T64">v:pred</ta>
            <ta e="T67" id="Seg_1627" s="T66">np.h:S</ta>
            <ta e="T69" id="Seg_1628" s="T67">s:temp</ta>
            <ta e="T71" id="Seg_1629" s="T70">np:O</ta>
            <ta e="T72" id="Seg_1630" s="T71">v:pred</ta>
            <ta e="T74" id="Seg_1631" s="T73">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T77" id="Seg_1632" s="T76">np:O</ta>
            <ta e="T78" id="Seg_1633" s="T77">0.3.h:S v:pred</ta>
            <ta e="T81" id="Seg_1634" s="T79">s:temp</ta>
            <ta e="T82" id="Seg_1635" s="T81">np:S</ta>
            <ta e="T84" id="Seg_1636" s="T83">v:pred</ta>
            <ta e="T86" id="Seg_1637" s="T85">np.h:S</ta>
            <ta e="T89" id="Seg_1638" s="T86">s:temp</ta>
            <ta e="T90" id="Seg_1639" s="T89">np:O</ta>
            <ta e="T91" id="Seg_1640" s="T90">v:pred</ta>
            <ta e="T94" id="Seg_1641" s="T93">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T97" id="Seg_1642" s="T96">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T99" id="Seg_1643" s="T98">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T101" id="Seg_1644" s="T100">s:adv</ta>
            <ta e="T103" id="Seg_1645" s="T102">0.3.h:S v:pred</ta>
            <ta e="T107" id="Seg_1646" s="T106">np:O</ta>
            <ta e="T110" id="Seg_1647" s="T109">0.3.h:S v:pred</ta>
            <ta e="T115" id="Seg_1648" s="T114">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T119" id="Seg_1649" s="T118">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T123" id="Seg_1650" s="T122">pro.h:S</ta>
            <ta e="T124" id="Seg_1651" s="T123">v:pred</ta>
            <ta e="T128" id="Seg_1652" s="T127">0.3.h:S v:pred</ta>
            <ta e="T132" id="Seg_1653" s="T131">np.h:S</ta>
            <ta e="T134" id="Seg_1654" s="T133">v:pred</ta>
            <ta e="T136" id="Seg_1655" s="T135">np:S</ta>
            <ta e="T141" id="Seg_1656" s="T140">v:pred</ta>
            <ta e="T143" id="Seg_1657" s="T142">np.h:S</ta>
            <ta e="T145" id="Seg_1658" s="T144">v:pred</ta>
            <ta e="T147" id="Seg_1659" s="T146">np:S</ta>
            <ta e="T150" id="Seg_1660" s="T149">np:O</ta>
            <ta e="T151" id="Seg_1661" s="T150">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T6" id="Seg_1662" s="T1">RUS:ext</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_1663" s="T1">[KuAI:] Kunin, the chairman of the village council.</ta>
            <ta e="T9" id="Seg_1664" s="T14">[KNM:] An old man with his wife.</ta>
            <ta e="T16" id="Seg_1665" s="T10">Long ago there lived in a forest an old man with his wife.</ta>
            <ta e="T22" id="Seg_1666" s="T17">The old man went to fish into the forest in winter.</ta>
            <ta e="T29" id="Seg_1667" s="T23">As he was heading home, he found a fox on the road.</ta>
            <ta e="T35" id="Seg_1668" s="T30">Probably a fox had come.</ta>
            <ta e="T39" id="Seg_1669" s="T36">He put it to his sledge.</ta>
            <ta e="T45" id="Seg_1670" s="T40">As he came home, he told his wife.</ta>
            <ta e="T51" id="Seg_1671" s="T46">I brought it (?) to you.</ta>
            <ta e="T57" id="Seg_1672" s="T52">The old woman, the wife took it and started to pull it.</ta>
            <ta e="T65" id="Seg_1673" s="T58">The fox became alive and sat down onto her knees.</ta>
            <ta e="T72" id="Seg_1674" s="T66">The old man (gave it away?) wanted to hit the fox.</ta>
            <ta e="T74" id="Seg_1675" s="T73">He hit it.</ta>
            <ta e="T78" id="Seg_1676" s="T75">He hit the old woman's knee.</ta>
            <ta e="T84" id="Seg_1677" s="T79">When the old woman let the fox go, it ran outside.</ta>
            <ta e="T91" id="Seg_1678" s="T85">The old man went to catch the fox, having put on his hare coat.</ta>
            <ta e="T94" id="Seg_1679" s="T92">So he pursued it.</ta>
            <ta e="T97" id="Seg_1680" s="T95">He pursued it for a long time.</ta>
            <ta e="T99" id="Seg_1681" s="T98">He pursued it.</ta>
            <ta e="T103" id="Seg_1682" s="T100">He sweated up to death.</ta>
            <ta e="T110" id="Seg_1683" s="T104">Once he threw away his coat.</ta>
            <ta e="T115" id="Seg_1684" s="T111">In what kind of clothes he was pursuing it trembling(?).</ta>
            <ta e="T119" id="Seg_1685" s="T116">He pursued it again for a long time.</ta>
            <ta e="T124" id="Seg_1686" s="T120">At some point he fell behind.</ta>
            <ta e="T128" id="Seg_1687" s="T125">He became very cold.</ta>
            <ta e="T134" id="Seg_1688" s="T129">This old man, who had sweated, froze.</ta>
            <ta e="T141" id="Seg_1689" s="T135">The fox then came back.</ta>
            <ta e="T145" id="Seg_1690" s="T142">The old man died.</ta>
            <ta e="T151" id="Seg_1691" s="T146">The fox then ate this food (this old man).</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_1692" s="T1">[KuAI:] Kunin, der Vorsitzende des Dorfrates.</ta>
            <ta e="T9" id="Seg_1693" s="T14">[KNM:] Ein alter Mann mit seiner Frau.</ta>
            <ta e="T16" id="Seg_1694" s="T10">Vor langer Zeit lebten in einem Wald ein alter Mann mit seiner Frau.</ta>
            <ta e="T22" id="Seg_1695" s="T17">Der alte Mann ging im Winter zum Fischen in den Wald.</ta>
            <ta e="T29" id="Seg_1696" s="T23">Als er nach Hause lief, fand er auf der Straße einen Fuchs.</ta>
            <ta e="T35" id="Seg_1697" s="T30">Vielleicht war ein Fuchs gekommen.</ta>
            <ta e="T39" id="Seg_1698" s="T36">Er lud ihn auf seinen Schlitten.</ta>
            <ta e="T45" id="Seg_1699" s="T40">Als er nach Hause kam, sagte er so zu seiner Frau.</ta>
            <ta e="T51" id="Seg_1700" s="T46">Ich brachte ihn dir.</ta>
            <ta e="T57" id="Seg_1701" s="T52">Die alte Frau, die Ehefrau, nahm ihn und begann an ihm zu ziehen.</ta>
            <ta e="T65" id="Seg_1702" s="T58">Der Fuchs wurde lebendig und setzte sich auf ihre Knie.</ta>
            <ta e="T72" id="Seg_1703" s="T66">Der alte Mann gab ihn weg, er wollte den Fuchs schlagen.</ta>
            <ta e="T74" id="Seg_1704" s="T73">Er schlug ihn.</ta>
            <ta e="T78" id="Seg_1705" s="T75">Er schlug das Knie der alten Frau.</ta>
            <ta e="T84" id="Seg_1706" s="T79">Als die alte Frau den Fuchs losließ, rannte er nach draußen.</ta>
            <ta e="T91" id="Seg_1707" s="T85">Nachdem er seinen Hasenmantel angezogen hatte, verfolgte der alte Mann den Fuchs.</ta>
            <ta e="T94" id="Seg_1708" s="T92">So verfolgte er ihn.</ta>
            <ta e="T97" id="Seg_1709" s="T95">Er verfolgte ihn lange.</ta>
            <ta e="T99" id="Seg_1710" s="T98">Er verfolgte ihn.</ta>
            <ta e="T103" id="Seg_1711" s="T100">Er schwitzte sich zu Tode.</ta>
            <ta e="T110" id="Seg_1712" s="T104">Einmal warf er seinen Mantel weg.</ta>
            <ta e="T115" id="Seg_1713" s="T111">In was für Kleidung verfolgte er ihn zitternd.</ta>
            <ta e="T119" id="Seg_1714" s="T116">Er verfolgte ihn wieder für lange Zeit.</ta>
            <ta e="T124" id="Seg_1715" s="T120">Auf einmal fiel er zurück.</ta>
            <ta e="T128" id="Seg_1716" s="T125">Ihm wurde sehr kalt.</ta>
            <ta e="T134" id="Seg_1717" s="T129">Dieser alte verschwitzte Mann fror.</ta>
            <ta e="T141" id="Seg_1718" s="T135">Der Fuchs kam dann zurück.</ta>
            <ta e="T145" id="Seg_1719" s="T142">Der alte Mann starb.</ta>
            <ta e="T151" id="Seg_1720" s="T146">Der Fuchs aß dann dieses Essen (den alten Mann).</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_1721" s="T1">[KuAI:] Кунин, председатель сельсовета села Толька.</ta>
            <ta e="T9" id="Seg_1722" s="T14">[KNM:] Дед с бабкой.</ta>
            <ta e="T16" id="Seg_1723" s="T10">Давно жили в лесу старик со старухой.</ta>
            <ta e="T22" id="Seg_1724" s="T17">старик зимой пошёл рыбачить в лес.</ta>
            <ta e="T29" id="Seg_1725" s="T23">Когда домой возвращался, на дороге одну лису нашел.</ta>
            <ta e="T35" id="Seg_1726" s="T30">Видно пришла, слышно, (одна) лиса.</ta>
            <ta e="T39" id="Seg_1727" s="T36">На нарты свои загрузил.</ta>
            <ta e="T45" id="Seg_1728" s="T40">Домой придя, старухе своей так сказал.</ta>
            <ta e="T51" id="Seg_1729" s="T46">Я тебе (?) принес.</ta>
            <ta e="T57" id="Seg_1730" s="T52">Старуха, жена, взяв, только потянула.</ta>
            <ta e="T65" id="Seg_1731" s="T58">Лиса ожила, у неё на коленях лиса села.</ta>
            <ta e="T72" id="Seg_1732" s="T66">Старик её давая, хотел стукнуть лису.</ta>
            <ta e="T74" id="Seg_1733" s="T73">Попал…</ta>
            <ta e="T78" id="Seg_1734" s="T75">Старухино колено стукнул.</ta>
            <ta e="T84" id="Seg_1735" s="T79">Когда старуха [ее] упустила, лиса на улицу выбежала.</ta>
            <ta e="T91" id="Seg_1736" s="T85">Старик, надев заячью шубу, за лисой побежал.</ta>
            <ta e="T94" id="Seg_1737" s="T92">Вот догонял.</ta>
            <ta e="T97" id="Seg_1738" s="T95">Долго догонял.</ta>
            <ta e="T99" id="Seg_1739" s="T98">Догонял.</ta>
            <ta e="T103" id="Seg_1740" s="T100">Вспотел до смерти.</ta>
            <ta e="T110" id="Seg_1741" s="T104">Один раз шубу вон выкинул.</ta>
            <ta e="T115" id="Seg_1742" s="T111">В одной рубашке (замёрз?) догонял.</ta>
            <ta e="T119" id="Seg_1743" s="T116">Опять долго догонял.</ta>
            <ta e="T124" id="Seg_1744" s="T120">Однажды он отстал.</ta>
            <ta e="T128" id="Seg_1745" s="T125">Сильно холодно стало.</ta>
            <ta e="T134" id="Seg_1746" s="T129">Тёплый старик замёрз.</ta>
            <ta e="T141" id="Seg_1747" s="T135">Лиса потом обратно пришла.</ta>
            <ta e="T145" id="Seg_1748" s="T142">Дед умер.</ta>
            <ta e="T151" id="Seg_1749" s="T146">Лиса потом эту еду [старика] стала съедать.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_1750" s="T1">Кунин, председатель сельсовета села Толька.</ta>
            <ta e="T9" id="Seg_1751" s="T14">дед с бабкой</ta>
            <ta e="T16" id="Seg_1752" s="T10">давно жили в лесу дед с бабкой</ta>
            <ta e="T22" id="Seg_1753" s="T17">дед зимой пошёл рыбачить в лес</ta>
            <ta e="T29" id="Seg_1754" s="T23">домой возвращался на дороге одну лису нашел</ta>
            <ta e="T35" id="Seg_1755" s="T30">видно пришла слышно лиса</ta>
            <ta e="T39" id="Seg_1756" s="T36">на нарты загрузил</ta>
            <ta e="T45" id="Seg_1757" s="T40">домой придя старухе так сказал</ta>
            <ta e="T51" id="Seg_1758" s="T46">я тебе ? принес</ta>
            <ta e="T57" id="Seg_1759" s="T52">старуха жена взяв только потянула</ta>
            <ta e="T65" id="Seg_1760" s="T58">лиса ожила у неё на коленях лиса села</ta>
            <ta e="T72" id="Seg_1761" s="T66">старик её давая, хотел стукнуть лису</ta>
            <ta e="T74" id="Seg_1762" s="T73">мимо</ta>
            <ta e="T78" id="Seg_1763" s="T75">бабушкино колено стукнул</ta>
            <ta e="T84" id="Seg_1764" s="T79">старуха упустила лиса на улицу выбежала</ta>
            <ta e="T91" id="Seg_1765" s="T85">старик надев заячью шубу за лесой побежал</ta>
            <ta e="T94" id="Seg_1766" s="T92">вот догонял</ta>
            <ta e="T97" id="Seg_1767" s="T95">долго догонял</ta>
            <ta e="T99" id="Seg_1768" s="T98">догонял</ta>
            <ta e="T103" id="Seg_1769" s="T100">вспотел до смерти</ta>
            <ta e="T110" id="Seg_1770" s="T104">один раз шубку вон выкинул</ta>
            <ta e="T115" id="Seg_1771" s="T111">в одной рубашке замёрз? догонял</ta>
            <ta e="T119" id="Seg_1772" s="T116">ой долго догонял</ta>
            <ta e="T124" id="Seg_1773" s="T120">однажды он отстал</ta>
            <ta e="T128" id="Seg_1774" s="T125">сильно холодно стало</ta>
            <ta e="T134" id="Seg_1775" s="T129">тёплый старик замёрз</ta>
            <ta e="T141" id="Seg_1776" s="T135">лиса потом (во второй раз) вернулась</ta>
            <ta e="T145" id="Seg_1777" s="T142">дед умер</ta>
            <ta e="T151" id="Seg_1778" s="T146">лиса стал съедать этого старика</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T9" id="Seg_1779" s="T14">[AAV:] lit. "with his mother" (?)</ta>
            <ta e="T39" id="Seg_1780" s="T36">[AAV:] tantɨmpatɨ ?</ta>
            <ta e="T65" id="Seg_1781" s="T58">[BrM:] Tentative transcription and analysis of 'par'. [AAV:] ilʼamɨla ? omtɨlʼa ?</ta>
            <ta e="T74" id="Seg_1782" s="T73">[BrM:] The original translation was 'he missed'. [AAV:] čaɣampɨt ?</ta>
            <ta e="T84" id="Seg_1783" s="T79">[AAV:] Imaqotan ?</ta>
            <ta e="T103" id="Seg_1784" s="T100">[BrM:] sʼak is probably Coordinative case, but it is not clear, how it is compounded with the previous stem.</ta>
            <ta e="T124" id="Seg_1785" s="T120">[BrM:] On the record one hears 'qaraıːmpatɨ', it was changed to 'qalaıːmpatɨ' by KSN. Tentative analysis of 'qalaıːmpatɨ', morphonologically 'qalaıːmmɨntɨ' would be expected.</ta>
            <ta e="T134" id="Seg_1786" s="T129">[BrM:] Tentative analysis of 'qantaıːmpatɨ', morphonologically 'qantaıːmmɨntɨ' would be expected.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T14" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
