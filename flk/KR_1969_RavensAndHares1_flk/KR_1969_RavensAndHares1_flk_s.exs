<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KR_1969_RavensAndHares_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KR_1969_RavensAndHares1_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">179</ud-information>
            <ud-information attribute-name="# HIAT:w">129</ud-information>
            <ud-information attribute-name="# e">129</ud-information>
            <ud-information attribute-name="# HIAT:u">37</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KR">
            <abbreviation>KR</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T90" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KR"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T133" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Külʼan</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">aj</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">nʼoman</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Ilimpɔːtɨn</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">külʼaj</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">nʼomaj</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">əmäsin</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_29" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">Ukkor</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">čʼontaqɨn</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">nʼoma</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">qənpa</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">šöttɨ</ts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_47" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">Štalʼ</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">qompatɨ</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">šötqɨn</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">čʼošij</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">qaqɨlʼe</ts>
                  <nts id="Seg_62" n="HIAT:ip">.</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_65" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17">Qaqɨlʼen</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">qanəqqɨn</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_72" n="HIAT:ip">(</nts>
                  <nts id="Seg_73" n="HIAT:ip">/</nts>
                  <ts e="T20" id="Seg_75" n="HIAT:w" s="T19">qanqɨn</ts>
                  <nts id="Seg_76" n="HIAT:ip">)</nts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_79" n="HIAT:w" s="T20">ɨralʼaka</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_82" n="HIAT:w" s="T21">nɨŋɨntɨ</ts>
                  <nts id="Seg_83" n="HIAT:ip">.</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_86" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_88" n="HIAT:w" s="T22">Nʼoma</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_91" n="HIAT:w" s="T23">nı</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_94" n="HIAT:w" s="T24">kətɨmpatɨ</ts>
                  <nts id="Seg_95" n="HIAT:ip">:</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_98" n="HIAT:w" s="T25">Nannar</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T26">pusqatɔːlaš</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">tılla</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_107" n="HIAT:w" s="T29">aša</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_110" n="HIAT:w" s="T30">atta</ts>
                  <nts id="Seg_111" n="HIAT:ip">.</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_114" n="HIAT:u" s="T31">
                  <ts e="T33" id="Seg_116" n="HIAT:w" s="T31">Pusqatɔːlpa</ts>
                  <nts id="Seg_117" n="HIAT:ip">.</nts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_120" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_122" n="HIAT:w" s="T33">Nʼoma</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_125" n="HIAT:w" s="T34">toː</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_128" n="HIAT:w" s="T35">qälla</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_131" n="HIAT:w" s="T36">čʼošilaka</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_134" n="HIAT:w" s="T37">orqɨlmmɨntɨtɨ</ts>
                  <nts id="Seg_135" n="HIAT:ip">.</nts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_138" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_140" n="HIAT:w" s="T38">Šitälʼ</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_143" n="HIAT:w" s="T39">moqonä</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_146" n="HIAT:w" s="T40">qənpa</ts>
                  <nts id="Seg_147" n="HIAT:ip">.</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_150" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_152" n="HIAT:w" s="T41">Iːjalʼaiːtən</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_155" n="HIAT:w" s="T42">ɔːntalpɔːtɨn</ts>
                  <nts id="Seg_156" n="HIAT:ip">.</nts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_159" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_161" n="HIAT:w" s="T43">Amɨrqolampɔːtɨn</ts>
                  <nts id="Seg_162" n="HIAT:ip">.</nts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_165" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_167" n="HIAT:w" s="T44">Štälʼ</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_170" n="HIAT:w" s="T45">külʼan</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_173" n="HIAT:w" s="T46">iːjaja</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_176" n="HIAT:w" s="T47">tümmɨntɨ</ts>
                  <nts id="Seg_177" n="HIAT:ip">.</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_180" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_182" n="HIAT:w" s="T48">Moqɨna</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_185" n="HIAT:w" s="T49">paktɨmpa</ts>
                  <nts id="Seg_186" n="HIAT:ip">.</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_189" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_191" n="HIAT:w" s="T50">Əmɨtɨ</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_194" n="HIAT:w" s="T51">tümpa</ts>
                  <nts id="Seg_195" n="HIAT:ip">.</nts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_198" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_200" n="HIAT:w" s="T52">Tɛː</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_203" n="HIAT:w" s="T53">kuːnɨ</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_206" n="HIAT:w" s="T54">čʼošim</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_209" n="HIAT:w" s="T55">qosɔːlɨn</ts>
                  <nts id="Seg_210" n="HIAT:ip">.</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_213" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_215" n="HIAT:w" s="T56">Man</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_218" n="HIAT:w" s="T57">ira</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_221" n="HIAT:w" s="T58">nɔːnɨ</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_224" n="HIAT:w" s="T59">mišalsam</ts>
                  <nts id="Seg_225" n="HIAT:ip">.</nts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_228" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_230" n="HIAT:w" s="T60">Täːlɨ</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_233" n="HIAT:w" s="T61">ukkuršak</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_236" n="HIAT:w" s="T62">qəllıː</ts>
                  <nts id="Seg_237" n="HIAT:ip">.</nts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_240" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_242" n="HIAT:w" s="T63">Qarɨn</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_245" n="HIAT:w" s="T64">qənpɔːqı</ts>
                  <nts id="Seg_246" n="HIAT:ip">.</nts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_249" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_251" n="HIAT:w" s="T65">Ɨralaka</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_254" n="HIAT:w" s="T66">čʼošij</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_257" n="HIAT:w" s="T67">qaqəle</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_260" n="HIAT:w" s="T68">üːqɨlmmɨntɨtɨ</ts>
                  <nts id="Seg_261" n="HIAT:ip">.</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_264" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_266" n="HIAT:w" s="T69">Külʼa</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_269" n="HIAT:w" s="T70">paktɨla</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_271" n="HIAT:ip">(</nts>
                  <nts id="Seg_272" n="HIAT:ip">/</nts>
                  <ts e="T72" id="Seg_274" n="HIAT:w" s="T71">waššɛıːla</ts>
                  <nts id="Seg_275" n="HIAT:ip">)</nts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_278" n="HIAT:w" s="T72">qaqlin</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_281" n="HIAT:w" s="T73">iːntɨ</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_284" n="HIAT:w" s="T74">omtijimpa</ts>
                  <nts id="Seg_285" n="HIAT:ip">.</nts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_288" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_290" n="HIAT:w" s="T75">Nı</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_293" n="HIAT:w" s="T76">laŋkɔːjpa</ts>
                  <nts id="Seg_294" n="HIAT:ip">:</nts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_297" n="HIAT:w" s="T77">küːk</ts>
                  <nts id="Seg_298" n="HIAT:ip">!</nts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_301" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_303" n="HIAT:w" s="T78">Irra</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_306" n="HIAT:w" s="T79">sukulta</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_309" n="HIAT:w" s="T80">piːqɨlʼlʼa</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_312" n="HIAT:w" s="T81">külʼam</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_315" n="HIAT:w" s="T82">qättɨmpatɨ</ts>
                  <nts id="Seg_316" n="HIAT:ip">.</nts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_319" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_321" n="HIAT:w" s="T83">Külʼa</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_324" n="HIAT:w" s="T84">qulʼčʼimpa</ts>
                  <nts id="Seg_325" n="HIAT:ip">.</nts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_328" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_330" n="HIAT:w" s="T85">Nʼoma</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_333" n="HIAT:w" s="T86">nı</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_336" n="HIAT:w" s="T87">kətɨmpatɨ</ts>
                  <nts id="Seg_337" n="HIAT:ip">:</nts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_340" n="HIAT:w" s="T88">Pusqatɔːlaš</ts>
                  <nts id="Seg_341" n="HIAT:ip">.</nts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_344" n="HIAT:u" s="T90">
                  <ts e="T92" id="Seg_346" n="HIAT:w" s="T90">Pusqatɔːlpa</ts>
                  <nts id="Seg_347" n="HIAT:ip">.</nts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_350" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_352" n="HIAT:w" s="T92">Nʼoma</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_355" n="HIAT:w" s="T93">paktɨlʼa</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_358" n="HIAT:w" s="T94">čʼošilaka</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_361" n="HIAT:w" s="T95">mišalmmɨntɨtɨ</ts>
                  <nts id="Seg_362" n="HIAT:ip">.</nts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T99" id="Seg_365" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_367" n="HIAT:w" s="T96">Šitalʼ</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_370" n="HIAT:w" s="T97">moqɨna</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_373" n="HIAT:w" s="T98">qənpa</ts>
                  <nts id="Seg_374" n="HIAT:ip">.</nts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_377" n="HIAT:u" s="T99">
                  <ts e="T100" id="Seg_379" n="HIAT:w" s="T99">Tümpa</ts>
                  <nts id="Seg_380" n="HIAT:ip">.</nts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_383" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_385" n="HIAT:w" s="T100">Külʼan</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_388" n="HIAT:w" s="T101">iːjajä</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_391" n="HIAT:w" s="T102">soqončʼɔːtɨn</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_393" n="HIAT:ip">(</nts>
                  <nts id="Seg_394" n="HIAT:ip">/</nts>
                  <ts e="T104" id="Seg_396" n="HIAT:w" s="T103">soqončʼintɨ</ts>
                  <nts id="Seg_397" n="HIAT:ip">)</nts>
                  <nts id="Seg_398" n="HIAT:ip">:</nts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_401" n="HIAT:w" s="T104">Amä</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_404" n="HIAT:w" s="T105">kun</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_407" n="HIAT:w" s="T106">ɛːŋa</ts>
                  <nts id="Seg_408" n="HIAT:ip">?</nts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_411" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_413" n="HIAT:w" s="T107">Amal</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_416" n="HIAT:w" s="T108">čʼošilaka</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_419" n="HIAT:w" s="T109">tačʼalmmɨntɨtɨ</ts>
                  <nts id="Seg_420" n="HIAT:ip">.</nts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_423" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_425" n="HIAT:w" s="T110">Ontɨn</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_428" n="HIAT:w" s="T111">amɨrqolampɔːtɨn</ts>
                  <nts id="Seg_429" n="HIAT:ip">.</nts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_432" n="HIAT:u" s="T112">
                  <ts e="T113" id="Seg_434" n="HIAT:w" s="T112">Külʼan</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_437" n="HIAT:w" s="T113">iːjaja</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_440" n="HIAT:w" s="T114">aj</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_443" n="HIAT:w" s="T115">tüŋa</ts>
                  <nts id="Seg_444" n="HIAT:ip">.</nts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_447" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_449" n="HIAT:w" s="T116">Ama</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_452" n="HIAT:w" s="T117">ɨːrɨk</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_455" n="HIAT:w" s="T118">čʼäːŋka</ts>
                  <nts id="Seg_456" n="HIAT:ip">.</nts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_459" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_461" n="HIAT:w" s="T119">Amal</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_464" n="HIAT:w" s="T120">iralakalʼa</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_467" n="HIAT:w" s="T121">qätɨsɨtɨ</ts>
                  <nts id="Seg_468" n="HIAT:ip">.</nts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_471" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_473" n="HIAT:w" s="T122">Moqɨnä</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_476" n="HIAT:w" s="T123">qəlʼlʼa</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_479" n="HIAT:w" s="T124">kətɨmpatɨ</ts>
                  <nts id="Seg_480" n="HIAT:ip">.</nts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T128" id="Seg_483" n="HIAT:u" s="T125">
                  <ts e="T126" id="Seg_485" n="HIAT:w" s="T125">Šitalʼ</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_488" n="HIAT:w" s="T126">muntɨk</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_491" n="HIAT:w" s="T127">čʼuːrɨqolompɔːtɨn</ts>
                  <nts id="Seg_492" n="HIAT:ip">.</nts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_495" n="HIAT:u" s="T128">
                  <ts e="T129" id="Seg_497" n="HIAT:w" s="T128">Turpan</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_500" n="HIAT:w" s="T129">mü</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_503" n="HIAT:w" s="T130">šüːwɨn</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_506" n="HIAT:w" s="T131">ılʼlʼa</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_509" n="HIAT:w" s="T132">sačʼčʼimɔːttɨn</ts>
                  <nts id="Seg_510" n="HIAT:ip">.</nts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T133" id="Seg_512" n="sc" s="T0">
               <ts e="T1" id="Seg_514" n="e" s="T0">Külʼan </ts>
               <ts e="T2" id="Seg_516" n="e" s="T1">aj </ts>
               <ts e="T3" id="Seg_518" n="e" s="T2">nʼoman. </ts>
               <ts e="T4" id="Seg_520" n="e" s="T3">Ilimpɔːtɨn </ts>
               <ts e="T5" id="Seg_522" n="e" s="T4">külʼaj </ts>
               <ts e="T6" id="Seg_524" n="e" s="T5">nʼomaj </ts>
               <ts e="T7" id="Seg_526" n="e" s="T6">əmäsin. </ts>
               <ts e="T8" id="Seg_528" n="e" s="T7">Ukkor </ts>
               <ts e="T9" id="Seg_530" n="e" s="T8">čʼontaqɨn </ts>
               <ts e="T10" id="Seg_532" n="e" s="T9">nʼoma </ts>
               <ts e="T11" id="Seg_534" n="e" s="T10">qənpa </ts>
               <ts e="T12" id="Seg_536" n="e" s="T11">šöttɨ. </ts>
               <ts e="T13" id="Seg_538" n="e" s="T12">Štalʼ </ts>
               <ts e="T14" id="Seg_540" n="e" s="T13">qompatɨ </ts>
               <ts e="T15" id="Seg_542" n="e" s="T14">šötqɨn </ts>
               <ts e="T16" id="Seg_544" n="e" s="T15">čʼošij </ts>
               <ts e="T17" id="Seg_546" n="e" s="T16">qaqɨlʼe. </ts>
               <ts e="T18" id="Seg_548" n="e" s="T17">Qaqɨlʼen </ts>
               <ts e="T19" id="Seg_550" n="e" s="T18">qanəqqɨn </ts>
               <ts e="T20" id="Seg_552" n="e" s="T19">(/qanqɨn) </ts>
               <ts e="T21" id="Seg_554" n="e" s="T20">ɨralʼaka </ts>
               <ts e="T22" id="Seg_556" n="e" s="T21">nɨŋɨntɨ. </ts>
               <ts e="T23" id="Seg_558" n="e" s="T22">Nʼoma </ts>
               <ts e="T24" id="Seg_560" n="e" s="T23">nı </ts>
               <ts e="T25" id="Seg_562" n="e" s="T24">kətɨmpatɨ: </ts>
               <ts e="T26" id="Seg_564" n="e" s="T25">Nannar </ts>
               <ts e="T28" id="Seg_566" n="e" s="T26">pusqatɔːlaš </ts>
               <ts e="T29" id="Seg_568" n="e" s="T28">tılla </ts>
               <ts e="T30" id="Seg_570" n="e" s="T29">aša </ts>
               <ts e="T31" id="Seg_572" n="e" s="T30">atta. </ts>
               <ts e="T33" id="Seg_574" n="e" s="T31">Pusqatɔːlpa. </ts>
               <ts e="T34" id="Seg_576" n="e" s="T33">Nʼoma </ts>
               <ts e="T35" id="Seg_578" n="e" s="T34">toː </ts>
               <ts e="T36" id="Seg_580" n="e" s="T35">qälla </ts>
               <ts e="T37" id="Seg_582" n="e" s="T36">čʼošilaka </ts>
               <ts e="T38" id="Seg_584" n="e" s="T37">orqɨlmmɨntɨtɨ. </ts>
               <ts e="T39" id="Seg_586" n="e" s="T38">Šitälʼ </ts>
               <ts e="T40" id="Seg_588" n="e" s="T39">moqonä </ts>
               <ts e="T41" id="Seg_590" n="e" s="T40">qənpa. </ts>
               <ts e="T42" id="Seg_592" n="e" s="T41">Iːjalʼaiːtən </ts>
               <ts e="T43" id="Seg_594" n="e" s="T42">ɔːntalpɔːtɨn. </ts>
               <ts e="T44" id="Seg_596" n="e" s="T43">Amɨrqolampɔːtɨn. </ts>
               <ts e="T45" id="Seg_598" n="e" s="T44">Štälʼ </ts>
               <ts e="T46" id="Seg_600" n="e" s="T45">külʼan </ts>
               <ts e="T47" id="Seg_602" n="e" s="T46">iːjaja </ts>
               <ts e="T48" id="Seg_604" n="e" s="T47">tümmɨntɨ. </ts>
               <ts e="T49" id="Seg_606" n="e" s="T48">Moqɨna </ts>
               <ts e="T50" id="Seg_608" n="e" s="T49">paktɨmpa. </ts>
               <ts e="T51" id="Seg_610" n="e" s="T50">Əmɨtɨ </ts>
               <ts e="T52" id="Seg_612" n="e" s="T51">tümpa. </ts>
               <ts e="T53" id="Seg_614" n="e" s="T52">Tɛː </ts>
               <ts e="T54" id="Seg_616" n="e" s="T53">kuːnɨ </ts>
               <ts e="T55" id="Seg_618" n="e" s="T54">čʼošim </ts>
               <ts e="T56" id="Seg_620" n="e" s="T55">qosɔːlɨn. </ts>
               <ts e="T57" id="Seg_622" n="e" s="T56">Man </ts>
               <ts e="T58" id="Seg_624" n="e" s="T57">ira </ts>
               <ts e="T59" id="Seg_626" n="e" s="T58">nɔːnɨ </ts>
               <ts e="T60" id="Seg_628" n="e" s="T59">mišalsam. </ts>
               <ts e="T61" id="Seg_630" n="e" s="T60">Täːlɨ </ts>
               <ts e="T62" id="Seg_632" n="e" s="T61">ukkuršak </ts>
               <ts e="T63" id="Seg_634" n="e" s="T62">qəllıː. </ts>
               <ts e="T64" id="Seg_636" n="e" s="T63">Qarɨn </ts>
               <ts e="T65" id="Seg_638" n="e" s="T64">qənpɔːqı. </ts>
               <ts e="T66" id="Seg_640" n="e" s="T65">Ɨralaka </ts>
               <ts e="T67" id="Seg_642" n="e" s="T66">čʼošij </ts>
               <ts e="T68" id="Seg_644" n="e" s="T67">qaqəle </ts>
               <ts e="T69" id="Seg_646" n="e" s="T68">üːqɨlmmɨntɨtɨ. </ts>
               <ts e="T70" id="Seg_648" n="e" s="T69">Külʼa </ts>
               <ts e="T71" id="Seg_650" n="e" s="T70">paktɨla </ts>
               <ts e="T72" id="Seg_652" n="e" s="T71">(/waššɛıːla) </ts>
               <ts e="T73" id="Seg_654" n="e" s="T72">qaqlin </ts>
               <ts e="T74" id="Seg_656" n="e" s="T73">iːntɨ </ts>
               <ts e="T75" id="Seg_658" n="e" s="T74">omtijimpa. </ts>
               <ts e="T76" id="Seg_660" n="e" s="T75">Nı </ts>
               <ts e="T77" id="Seg_662" n="e" s="T76">laŋkɔːjpa: </ts>
               <ts e="T78" id="Seg_664" n="e" s="T77">küːk! </ts>
               <ts e="T79" id="Seg_666" n="e" s="T78">Irra </ts>
               <ts e="T80" id="Seg_668" n="e" s="T79">sukulta </ts>
               <ts e="T81" id="Seg_670" n="e" s="T80">piːqɨlʼlʼa </ts>
               <ts e="T82" id="Seg_672" n="e" s="T81">külʼam </ts>
               <ts e="T83" id="Seg_674" n="e" s="T82">qättɨmpatɨ. </ts>
               <ts e="T84" id="Seg_676" n="e" s="T83">Külʼa </ts>
               <ts e="T85" id="Seg_678" n="e" s="T84">qulʼčʼimpa. </ts>
               <ts e="T86" id="Seg_680" n="e" s="T85">Nʼoma </ts>
               <ts e="T87" id="Seg_682" n="e" s="T86">nı </ts>
               <ts e="T88" id="Seg_684" n="e" s="T87">kətɨmpatɨ: </ts>
               <ts e="T90" id="Seg_686" n="e" s="T88">Pusqatɔːlaš. </ts>
               <ts e="T92" id="Seg_688" n="e" s="T90">Pusqatɔːlpa. </ts>
               <ts e="T93" id="Seg_690" n="e" s="T92">Nʼoma </ts>
               <ts e="T94" id="Seg_692" n="e" s="T93">paktɨlʼa </ts>
               <ts e="T95" id="Seg_694" n="e" s="T94">čʼošilaka </ts>
               <ts e="T96" id="Seg_696" n="e" s="T95">mišalmmɨntɨtɨ. </ts>
               <ts e="T97" id="Seg_698" n="e" s="T96">Šitalʼ </ts>
               <ts e="T98" id="Seg_700" n="e" s="T97">moqɨna </ts>
               <ts e="T99" id="Seg_702" n="e" s="T98">qənpa. </ts>
               <ts e="T100" id="Seg_704" n="e" s="T99">Tümpa. </ts>
               <ts e="T101" id="Seg_706" n="e" s="T100">Külʼan </ts>
               <ts e="T102" id="Seg_708" n="e" s="T101">iːjajä </ts>
               <ts e="T103" id="Seg_710" n="e" s="T102">soqončʼɔːtɨn </ts>
               <ts e="T104" id="Seg_712" n="e" s="T103">(/soqončʼintɨ): </ts>
               <ts e="T105" id="Seg_714" n="e" s="T104">Amä </ts>
               <ts e="T106" id="Seg_716" n="e" s="T105">kun </ts>
               <ts e="T107" id="Seg_718" n="e" s="T106">ɛːŋa? </ts>
               <ts e="T108" id="Seg_720" n="e" s="T107">Amal </ts>
               <ts e="T109" id="Seg_722" n="e" s="T108">čʼošilaka </ts>
               <ts e="T110" id="Seg_724" n="e" s="T109">tačʼalmmɨntɨtɨ. </ts>
               <ts e="T111" id="Seg_726" n="e" s="T110">Ontɨn </ts>
               <ts e="T112" id="Seg_728" n="e" s="T111">amɨrqolampɔːtɨn. </ts>
               <ts e="T113" id="Seg_730" n="e" s="T112">Külʼan </ts>
               <ts e="T114" id="Seg_732" n="e" s="T113">iːjaja </ts>
               <ts e="T115" id="Seg_734" n="e" s="T114">aj </ts>
               <ts e="T116" id="Seg_736" n="e" s="T115">tüŋa. </ts>
               <ts e="T117" id="Seg_738" n="e" s="T116">Ama </ts>
               <ts e="T118" id="Seg_740" n="e" s="T117">ɨːrɨk </ts>
               <ts e="T119" id="Seg_742" n="e" s="T118">čʼäːŋka. </ts>
               <ts e="T120" id="Seg_744" n="e" s="T119">Amal </ts>
               <ts e="T121" id="Seg_746" n="e" s="T120">iralakalʼa </ts>
               <ts e="T122" id="Seg_748" n="e" s="T121">qätɨsɨtɨ. </ts>
               <ts e="T123" id="Seg_750" n="e" s="T122">Moqɨnä </ts>
               <ts e="T124" id="Seg_752" n="e" s="T123">qəlʼlʼa </ts>
               <ts e="T125" id="Seg_754" n="e" s="T124">kətɨmpatɨ. </ts>
               <ts e="T126" id="Seg_756" n="e" s="T125">Šitalʼ </ts>
               <ts e="T127" id="Seg_758" n="e" s="T126">muntɨk </ts>
               <ts e="T128" id="Seg_760" n="e" s="T127">čʼuːrɨqolompɔːtɨn. </ts>
               <ts e="T129" id="Seg_762" n="e" s="T128">Turpan </ts>
               <ts e="T130" id="Seg_764" n="e" s="T129">mü </ts>
               <ts e="T131" id="Seg_766" n="e" s="T130">šüːwɨn </ts>
               <ts e="T132" id="Seg_768" n="e" s="T131">ılʼlʼa </ts>
               <ts e="T133" id="Seg_770" n="e" s="T132">sačʼčʼimɔːttɨn. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_771" s="T0">KR_1969_RavensAndHares1_flk.001 (001)</ta>
            <ta e="T7" id="Seg_772" s="T3">KR_1969_RavensAndHares1_flk.002 (002.001)</ta>
            <ta e="T12" id="Seg_773" s="T7">KR_1969_RavensAndHares1_flk.003 (002.002)</ta>
            <ta e="T17" id="Seg_774" s="T12">KR_1969_RavensAndHares1_flk.004 (002.003)</ta>
            <ta e="T22" id="Seg_775" s="T17">KR_1969_RavensAndHares1_flk.005 (002.004)</ta>
            <ta e="T31" id="Seg_776" s="T22">KR_1969_RavensAndHares1_flk.006 (002.005)</ta>
            <ta e="T33" id="Seg_777" s="T31">KR_1969_RavensAndHares1_flk.007 (002.006)</ta>
            <ta e="T38" id="Seg_778" s="T33">KR_1969_RavensAndHares1_flk.008 (002.007)</ta>
            <ta e="T41" id="Seg_779" s="T38">KR_1969_RavensAndHares1_flk.009 (002.008)</ta>
            <ta e="T43" id="Seg_780" s="T41">KR_1969_RavensAndHares1_flk.010 (002.009)</ta>
            <ta e="T44" id="Seg_781" s="T43">KR_1969_RavensAndHares1_flk.011 (002.010)</ta>
            <ta e="T48" id="Seg_782" s="T44">KR_1969_RavensAndHares1_flk.012 (002.011)</ta>
            <ta e="T50" id="Seg_783" s="T48">KR_1969_RavensAndHares1_flk.013 (002.012)</ta>
            <ta e="T52" id="Seg_784" s="T50">KR_1969_RavensAndHares1_flk.014 (002.013)</ta>
            <ta e="T56" id="Seg_785" s="T52">KR_1969_RavensAndHares1_flk.015 (002.014)</ta>
            <ta e="T60" id="Seg_786" s="T56">KR_1969_RavensAndHares1_flk.016 (002.015)</ta>
            <ta e="T63" id="Seg_787" s="T60">KR_1969_RavensAndHares1_flk.017 (002.016)</ta>
            <ta e="T65" id="Seg_788" s="T63">KR_1969_RavensAndHares1_flk.018 (002.017)</ta>
            <ta e="T69" id="Seg_789" s="T65">KR_1969_RavensAndHares1_flk.019 (002.018)</ta>
            <ta e="T75" id="Seg_790" s="T69">KR_1969_RavensAndHares1_flk.020 (002.019)</ta>
            <ta e="T78" id="Seg_791" s="T75">KR_1969_RavensAndHares1_flk.021 (002.020)</ta>
            <ta e="T83" id="Seg_792" s="T78">KR_1969_RavensAndHares1_flk.022 (002.021)</ta>
            <ta e="T85" id="Seg_793" s="T83">KR_1969_RavensAndHares1_flk.023 (002.022)</ta>
            <ta e="T90" id="Seg_794" s="T85">KR_1969_RavensAndHares1_flk.024 (002.023)</ta>
            <ta e="T92" id="Seg_795" s="T90">KR_1969_RavensAndHares1_flk.025 (002.024)</ta>
            <ta e="T96" id="Seg_796" s="T92">KR_1969_RavensAndHares1_flk.026 (002.025)</ta>
            <ta e="T99" id="Seg_797" s="T96">KR_1969_RavensAndHares1_flk.027 (002.026)</ta>
            <ta e="T100" id="Seg_798" s="T99">KR_1969_RavensAndHares1_flk.028 (002.027)</ta>
            <ta e="T107" id="Seg_799" s="T100">KR_1969_RavensAndHares1_flk.029 (002.028)</ta>
            <ta e="T110" id="Seg_800" s="T107">KR_1969_RavensAndHares1_flk.030 (002.029)</ta>
            <ta e="T112" id="Seg_801" s="T110">KR_1969_RavensAndHares1_flk.031 (002.030)</ta>
            <ta e="T116" id="Seg_802" s="T112">KR_1969_RavensAndHares1_flk.032 (002.031)</ta>
            <ta e="T119" id="Seg_803" s="T116">KR_1969_RavensAndHares1_flk.033 (002.032)</ta>
            <ta e="T122" id="Seg_804" s="T119">KR_1969_RavensAndHares1_flk.034 (002.033)</ta>
            <ta e="T125" id="Seg_805" s="T122">KR_1969_RavensAndHares1_flk.035 (002.034)</ta>
            <ta e="T128" id="Seg_806" s="T125">KR_1969_RavensAndHares1_flk.036 (002.035)</ta>
            <ta e="T133" id="Seg_807" s="T128">KR_1969_RavensAndHares1_flk.037 (002.036)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_808" s="T0">кӱ̄llан ай ′нʼӧ̄ман.</ta>
            <ta e="T7" id="Seg_809" s="T3">‵иlим′по̄тын ′кӱ̄llай ′нʼӧ̄май ′əммӓсин.</ta>
            <ta e="T12" id="Seg_810" s="T7">′укко[ы]р чон′таkын ′нʼӧ̄мма k[х̥]ъ̊нпа′шʼӧ̄тты.</ta>
            <ta e="T17" id="Seg_811" s="T12">′ш[с]талʼ ′kомпаты шʼӧ̄тkын ′чо̄ший ′kа̄ɣылʼе.</ta>
            <ta e="T22" id="Seg_812" s="T17">′kа̄ɣылʼен (к[х]анk[х̥]ын) ′х̥анъх̥ын ′ыралʼака ′ныңынты.</ta>
            <ta e="T31" id="Seg_813" s="T22">′нʼӧ̄мма ни ′kъ̊̄тымпаты: ′наннар ′пусkа‵тоlашʼ ′ты̄llа ашʼа атта.</ta>
            <ta e="T33" id="Seg_814" s="T31">′пусkа′тоlпа.</ta>
            <ta e="T38" id="Seg_815" s="T33">′нʼӧ̄мма ′то̄kӓ̊llа чо̄шиlака ′оркыл[l]мынтыты.</ta>
            <ta e="T41" id="Seg_816" s="T38">′шʼителʼ ′моkонӓ ′къ̊̄нпа.</ta>
            <ta e="T43" id="Seg_817" s="T41">′ӣjалʼа ′ӣтън ′а̊̄нталʼ′потын.</ta>
            <ta e="T44" id="Seg_818" s="T43">‵аммырkоlам′по̄тын.</ta>
            <ta e="T48" id="Seg_819" s="T44">штɛлʼ кӱ̄llан ′ӣjаj[лʼ]а ′тӱ̄ммынты.</ta>
            <ta e="T50" id="Seg_820" s="T48">′моkына ′паkтымпа.</ta>
            <ta e="T52" id="Seg_821" s="T50">′ə̄мыты тӱ̄мпа.</ta>
            <ta e="T56" id="Seg_822" s="T52">те̄ ̊куны ′чошим ко̄с′соlи[ы]н.</ta>
            <ta e="T60" id="Seg_823" s="T56">ман ӣраноны ми′щʼалʼсам.</ta>
            <ta e="T63" id="Seg_824" s="T60">′тɛ̄lы ′укку[ы]р шʼах k[х̥]ъ̊llи.</ta>
            <ta e="T65" id="Seg_825" s="T63">′k[х̥]арын kъ̊нпо̄kи.</ta>
            <ta e="T69" id="Seg_826" s="T65">′ыраlака ′чоший ′kаɣъlе ӱ̄ку[ы]lмынтыты.</ta>
            <ta e="T75" id="Seg_827" s="T69">′кӱ̄llа ′паkтыlа (‵ва̊′шʼшʼеиlа) ′kаɣlининты ом′тиjимпа.</ta>
            <ta e="T78" id="Seg_828" s="T75">нӣ ′lаңкойпа: кӱ̄к!</ta>
            <ta e="T83" id="Seg_829" s="T78">′ирра ′сукуlта ′пӣкыlʼа ′кӱ̄lʼlʼам ′kӓттымбаты.</ta>
            <ta e="T85" id="Seg_830" s="T83">′кӱ̄lʼlʼа ′куlчимпа.</ta>
            <ta e="T90" id="Seg_831" s="T85">′нʼӧмма ни ′kъ̊тымпаты ‵пусkа′тоlашʼ.</ta>
            <ta e="T92" id="Seg_832" s="T90">‵пусkа′тоlпа.</ta>
            <ta e="T96" id="Seg_833" s="T92">нʼӧмма ′паkтылʼа ′чошʼиlака ми′щʼалʼмынтыты.</ta>
            <ta e="T99" id="Seg_834" s="T96">шʼиталʼ ′моkына ′kъ̊̄нпа.</ta>
            <ta e="T100" id="Seg_835" s="T99">тӱ̄мпа.</ta>
            <ta e="T107" id="Seg_836" s="T100">′кӱlʼlʼан ′ӣjаjӓ (′со̄kончинты) со̄kончо̄тын ′аммɛ кун е̄ɣа?</ta>
            <ta e="T110" id="Seg_837" s="T107">′аммаl ′чошʼиlака ′тачалʼмынты‵ты.</ta>
            <ta e="T112" id="Seg_838" s="T110">′онтын ′амырkоlам‵по̄тын.</ta>
            <ta e="T116" id="Seg_839" s="T112">′кӱlʼlʼан ‵ӣjаjа ай ′тӱ̄ɣа.</ta>
            <ta e="T119" id="Seg_840" s="T116">′амма ′ырых чӓ̄ңка.</ta>
            <ta e="T122" id="Seg_841" s="T119">′а̄маl ′ираlака lаɣ[k]ӓтысыты.</ta>
            <ta e="T125" id="Seg_842" s="T122">моkынӓ ′k[х̥]ъ̊lʼlʼа ‵kъ̊тым′паты.</ta>
            <ta e="T128" id="Seg_843" s="T125">′шʼитаlʼ ′мунтых ‵чурыk[х̥]оlом′потын.</ta>
            <ta e="T133" id="Seg_844" s="T128">′турпан мӱшʼӱв[w]ын ′иlʼlʼа ′сатчи‵мо̄тын.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T3" id="Seg_845" s="T0">küːlʼlʼan aj nʼöːman.</ta>
            <ta e="T7" id="Seg_846" s="T3">ilʼimpoːtɨn küːlʼlʼaj nʼöːmaj əmmäsin.</ta>
            <ta e="T12" id="Seg_847" s="T7">ukko[ɨ]r čʼontaqɨn nʼöːmma q[h̥]ənpašöːttɨ.</ta>
            <ta e="T17" id="Seg_848" s="T12">š[s]talʼ qompatɨ šöːtqɨn čʼoːšij qaːqɨlʼe.</ta>
            <ta e="T22" id="Seg_849" s="T17">qaːqɨlʼen (k[h]anq[h̥]ɨn) h̥anəh̥ɨn ɨralʼaka nɨŋɨntɨ.</ta>
            <ta e="T31" id="Seg_850" s="T22">nʼöːmma ni qəːtɨmpatɨ: nannar pusqatolʼaš tɨːlʼlʼa aša atta.</ta>
            <ta e="T33" id="Seg_851" s="T31">pusqatolʼpa.</ta>
            <ta e="T38" id="Seg_852" s="T33">nʼöːmma toːqälʼlʼa čʼoːšilʼaka orkɨl[lʼ]mɨntɨtɨ.</ta>
            <ta e="T41" id="Seg_853" s="T38">šʼitelʼ moqonä kəːnpa.</ta>
            <ta e="T43" id="Seg_854" s="T41">iːjalʼa iːtən aːntalʼpotɨn.</ta>
            <ta e="T44" id="Seg_855" s="T43">ammɨrqolʼampoːtɨn.</ta>
            <ta e="T48" id="Seg_856" s="T44">štɛlʼ küːlʼlʼan iːjaj[lʼ]a tüːmmɨntɨ.</ta>
            <ta e="T50" id="Seg_857" s="T48">moqɨna paqtɨmpa.</ta>
            <ta e="T52" id="Seg_858" s="T50">əːmɨtɨ tüːmpa.</ta>
            <ta e="T56" id="Seg_859" s="T52">teː kunɨ čʼošim koːssolʼi[ɨ]n.</ta>
            <ta e="T60" id="Seg_860" s="T56">man iːranonɨ miщʼalʼsam.</ta>
            <ta e="T63" id="Seg_861" s="T60">tɛːlʼɨ ukku[ɨ]r šah q[h̥]əlʼlʼi.</ta>
            <ta e="T65" id="Seg_862" s="T63">q[h̥]arɨn qənpoːqi.</ta>
            <ta e="T69" id="Seg_863" s="T65">ɨralʼaka čʼošij qaqəlʼe üːku[ɨ]lʼmɨntɨtɨ.</ta>
            <ta e="T75" id="Seg_864" s="T69">küːlʼlʼa paqtɨlʼa (vašʼšʼeilʼa) qaqlʼinintɨ omtijimpa.</ta>
            <ta e="T78" id="Seg_865" s="T75">niː lʼaŋkojpa: küːk!</ta>
            <ta e="T83" id="Seg_866" s="T78">irra sukulʼta piːkɨlʼʼa küːlʼʼlʼʼam qättɨmpatɨ.</ta>
            <ta e="T85" id="Seg_867" s="T83">küːlʼʼlʼʼa kulʼčʼimpa.</ta>
            <ta e="T90" id="Seg_868" s="T85">nʼömma ni qətɨmpatɨ pusqatolʼaš.</ta>
            <ta e="T92" id="Seg_869" s="T90">pusqatolʼpa.</ta>
            <ta e="T96" id="Seg_870" s="T92">nʼömma paqtɨlʼa čʼošʼilʼaka miщʼalʼmɨntɨtɨ.</ta>
            <ta e="T99" id="Seg_871" s="T96">šʼitalʼ moqɨna qəːnpa.</ta>
            <ta e="T100" id="Seg_872" s="T99">tüːmpa.</ta>
            <ta e="T107" id="Seg_873" s="T100">külʼʼlʼʼan iːjajä (soːqončʼintɨ) soːqončʼoːtɨn ammɛ kun eːqa?</ta>
            <ta e="T110" id="Seg_874" s="T107">ammalʼ čʼošʼilʼaka tačʼalʼmɨntɨtɨ.</ta>
            <ta e="T112" id="Seg_875" s="T110">ontɨn amɨrqolʼampoːtɨn.</ta>
            <ta e="T116" id="Seg_876" s="T112">külʼʼlʼʼan iːjaja aj tüːqa.</ta>
            <ta e="T119" id="Seg_877" s="T116">amma ɨrɨh čʼäːŋka.</ta>
            <ta e="T122" id="Seg_878" s="T119">aːmalʼ iralʼaka lʼaq[q]ätɨsɨtɨ.</ta>
            <ta e="T125" id="Seg_879" s="T122">moqɨnä q[h̥]əlʼlʼa qətɨmpatɨ.</ta>
            <ta e="T128" id="Seg_880" s="T125">šʼitalʼ muntɨh čʼurɨq[h̥]olʼompotɨn.</ta>
            <ta e="T133" id="Seg_881" s="T128">turpan müšʼüv[w]ɨn ilʼlʼa satčʼimoːtɨn.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_882" s="T0">Külʼan aj nʼoman. </ta>
            <ta e="T7" id="Seg_883" s="T3">Ilimpɔːtɨn külʼaj nʼomaj əmäsin. </ta>
            <ta e="T12" id="Seg_884" s="T7">Ukkor čʼontaqɨn nʼoma qənpa šöttɨ. </ta>
            <ta e="T17" id="Seg_885" s="T12">Štalʼ qompatɨ šötqɨn čʼošij qaqɨlʼe. </ta>
            <ta e="T22" id="Seg_886" s="T17">Qaqɨlʼen qanəqqɨn (/qanqɨn) ɨralʼaka nɨŋɨntɨ. </ta>
            <ta e="T31" id="Seg_887" s="T22">Nʼoma nı kətɨmpatɨ: Nannar pusqatɔːlaš, tılla aša atta. </ta>
            <ta e="T33" id="Seg_888" s="T31">Pusqatɔːlpa. </ta>
            <ta e="T38" id="Seg_889" s="T33">Nʼoma toː qälla čʼošilaka orqɨlmmɨntɨtɨ. </ta>
            <ta e="T41" id="Seg_890" s="T38">Šitälʼ moqonä qənpa. </ta>
            <ta e="T43" id="Seg_891" s="T41">Iːjalʼaiːtən ɔːntalpɔːtɨn. </ta>
            <ta e="T44" id="Seg_892" s="T43">Amɨrqolampɔːtɨn. </ta>
            <ta e="T48" id="Seg_893" s="T44">Štälʼ külʼan iːjaja tümmɨntɨ. </ta>
            <ta e="T50" id="Seg_894" s="T48">Moqɨna paktɨmpa. </ta>
            <ta e="T52" id="Seg_895" s="T50">Əmɨtɨ tümpa. </ta>
            <ta e="T56" id="Seg_896" s="T52">Tɛː kuːnɨ čʼošim qosɔːlɨn. </ta>
            <ta e="T60" id="Seg_897" s="T56">Man ira nɔːnɨ mišalsam. </ta>
            <ta e="T63" id="Seg_898" s="T60">Täːlɨ ukkuršak qəllıː. </ta>
            <ta e="T65" id="Seg_899" s="T63">Qarɨn qənpɔːqı. </ta>
            <ta e="T69" id="Seg_900" s="T65">Ɨralaka čʼošij qaqəle üːqɨlmmɨntɨtɨ. </ta>
            <ta e="T75" id="Seg_901" s="T69">Külʼa paktɨla (/waššɛıːla) qaqlin iːntɨ omtijimpa. </ta>
            <ta e="T78" id="Seg_902" s="T75">Nı laŋkɔːjpa: küːk! </ta>
            <ta e="T83" id="Seg_903" s="T78">Irra sukulta piːqɨlʼlʼa külʼam qättɨmpatɨ. </ta>
            <ta e="T85" id="Seg_904" s="T83">Külʼa qulʼčʼimpa. </ta>
            <ta e="T90" id="Seg_905" s="T85">Nʼoma nı kətɨmpatɨ: Pusqatɔːlaš. </ta>
            <ta e="T92" id="Seg_906" s="T90">Pusqatɔːlpa. </ta>
            <ta e="T96" id="Seg_907" s="T92">Nʼoma paktɨlʼa čʼošilaka mišalmmɨntɨtɨ. </ta>
            <ta e="T99" id="Seg_908" s="T96">Šitalʼ moqɨna qənpa. </ta>
            <ta e="T100" id="Seg_909" s="T99">Tümpa. </ta>
            <ta e="T107" id="Seg_910" s="T100">Külʼan iːjajä soqončʼɔːtɨn (/soqončʼintɨ): Amä kun ɛːŋa? </ta>
            <ta e="T110" id="Seg_911" s="T107">Amal čʼošilaka tačʼalmmɨntɨtɨ. </ta>
            <ta e="T112" id="Seg_912" s="T110">Ontɨn amɨrqolampɔːtɨn. </ta>
            <ta e="T116" id="Seg_913" s="T112">Külʼan iːjaja aj tüŋa. </ta>
            <ta e="T119" id="Seg_914" s="T116">Ama ɨːrɨk čʼäːŋka. </ta>
            <ta e="T122" id="Seg_915" s="T119">Amal iralakalʼa qätɨsɨtɨ. </ta>
            <ta e="T125" id="Seg_916" s="T122">Moqɨnä qəlʼlʼa kətɨmpatɨ. </ta>
            <ta e="T128" id="Seg_917" s="T125">Šitalʼ muntɨk čʼuːrɨqolompɔːtɨn. </ta>
            <ta e="T133" id="Seg_918" s="T128">Turpan mü šüːwɨn ılʼlʼa sačʼčʼimɔːttɨn. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_919" s="T0">külʼa-n</ta>
            <ta e="T2" id="Seg_920" s="T1">aj</ta>
            <ta e="T3" id="Seg_921" s="T2">nʼoma-n</ta>
            <ta e="T4" id="Seg_922" s="T3">ili-mpɔː-tɨn</ta>
            <ta e="T5" id="Seg_923" s="T4">külʼa-j</ta>
            <ta e="T6" id="Seg_924" s="T5">nʼoma-j</ta>
            <ta e="T7" id="Seg_925" s="T6">əmä-si-n</ta>
            <ta e="T8" id="Seg_926" s="T7">ukkor</ta>
            <ta e="T9" id="Seg_927" s="T8">čʼonta-qɨn</ta>
            <ta e="T10" id="Seg_928" s="T9">nʼoma</ta>
            <ta e="T11" id="Seg_929" s="T10">qən-pa</ta>
            <ta e="T12" id="Seg_930" s="T11">šöt-tɨ</ta>
            <ta e="T13" id="Seg_931" s="T12">štalʼ</ta>
            <ta e="T14" id="Seg_932" s="T13">qo-mpa-tɨ</ta>
            <ta e="T15" id="Seg_933" s="T14">šöt-qɨn</ta>
            <ta e="T16" id="Seg_934" s="T15">čʼoš-i-j</ta>
            <ta e="T17" id="Seg_935" s="T16">qaqɨlʼe</ta>
            <ta e="T18" id="Seg_936" s="T17">qaqɨlʼe-n</ta>
            <ta e="T19" id="Seg_937" s="T18">qanəq-qɨn</ta>
            <ta e="T20" id="Seg_938" s="T19">qan-qɨn</ta>
            <ta e="T21" id="Seg_939" s="T20">ɨra-lʼaka</ta>
            <ta e="T22" id="Seg_940" s="T21">nɨŋ-ɨ-ntɨ</ta>
            <ta e="T23" id="Seg_941" s="T22">nʼoma</ta>
            <ta e="T24" id="Seg_942" s="T23">nı</ta>
            <ta e="T25" id="Seg_943" s="T24">kətɨ-mpa-tɨ</ta>
            <ta e="T26" id="Seg_944" s="T25">nannar</ta>
            <ta e="T28" id="Seg_945" s="T26">pusqa-t-ɔːl-aš</ta>
            <ta e="T29" id="Seg_946" s="T28">tıl-la</ta>
            <ta e="T30" id="Seg_947" s="T29">aša</ta>
            <ta e="T31" id="Seg_948" s="T30">atta</ta>
            <ta e="T33" id="Seg_949" s="T31">pusqa-t-ɔːl-pa</ta>
            <ta e="T34" id="Seg_950" s="T33">nʼoma</ta>
            <ta e="T35" id="Seg_951" s="T34">toː</ta>
            <ta e="T36" id="Seg_952" s="T35">qäl-la</ta>
            <ta e="T37" id="Seg_953" s="T36">čʼoš-i-laka</ta>
            <ta e="T38" id="Seg_954" s="T37">orqɨl-mmɨ-ntɨ-tɨ</ta>
            <ta e="T39" id="Seg_955" s="T38">šitälʼ</ta>
            <ta e="T40" id="Seg_956" s="T39">moqonä</ta>
            <ta e="T41" id="Seg_957" s="T40">qən-pa</ta>
            <ta e="T42" id="Seg_958" s="T41">iːja-lʼa-iː-tə-n</ta>
            <ta e="T43" id="Seg_959" s="T42">ɔːnt-al-pɔː-tɨn</ta>
            <ta e="T44" id="Seg_960" s="T43">am-ɨ-r-q-olam-pɔː-tɨn</ta>
            <ta e="T45" id="Seg_961" s="T44">štälʼ</ta>
            <ta e="T46" id="Seg_962" s="T45">külʼa-n</ta>
            <ta e="T47" id="Seg_963" s="T46">iːja-ja</ta>
            <ta e="T48" id="Seg_964" s="T47">tü-mmɨ-ntɨ</ta>
            <ta e="T49" id="Seg_965" s="T48">moqɨna</ta>
            <ta e="T50" id="Seg_966" s="T49">paktɨ-mpa</ta>
            <ta e="T51" id="Seg_967" s="T50">əmɨ-tɨ</ta>
            <ta e="T52" id="Seg_968" s="T51">tü-mpa</ta>
            <ta e="T53" id="Seg_969" s="T52">tɛː</ta>
            <ta e="T54" id="Seg_970" s="T53">kunɨ</ta>
            <ta e="T55" id="Seg_971" s="T54">čʼoš-i-m</ta>
            <ta e="T56" id="Seg_972" s="T55">qo-sɔː-lɨn</ta>
            <ta e="T57" id="Seg_973" s="T56">man</ta>
            <ta e="T58" id="Seg_974" s="T57">ira</ta>
            <ta e="T59" id="Seg_975" s="T58">nɔː-nɨ</ta>
            <ta e="T60" id="Seg_976" s="T59">mišal-sa-m</ta>
            <ta e="T61" id="Seg_977" s="T60">täːlɨ</ta>
            <ta e="T62" id="Seg_978" s="T61">ukkur-šak</ta>
            <ta e="T63" id="Seg_979" s="T62">qəl-l-ıː</ta>
            <ta e="T64" id="Seg_980" s="T63">qarɨ-n</ta>
            <ta e="T65" id="Seg_981" s="T64">qən-pɔː-qı</ta>
            <ta e="T66" id="Seg_982" s="T65">ɨra-laka</ta>
            <ta e="T67" id="Seg_983" s="T66">čʼoš-i-j</ta>
            <ta e="T68" id="Seg_984" s="T67">qaqəle</ta>
            <ta e="T69" id="Seg_985" s="T68">üː-qɨl-mmɨ-ntɨ-tɨ</ta>
            <ta e="T70" id="Seg_986" s="T69">külʼa</ta>
            <ta e="T71" id="Seg_987" s="T70">paktɨ-la</ta>
            <ta e="T72" id="Seg_988" s="T71">wašš-ɛıː-la</ta>
            <ta e="T73" id="Seg_989" s="T72">qaqli-n</ta>
            <ta e="T74" id="Seg_990" s="T73">iː-ntɨ</ta>
            <ta e="T75" id="Seg_991" s="T74">omti-ji-mpa</ta>
            <ta e="T76" id="Seg_992" s="T75">nı</ta>
            <ta e="T77" id="Seg_993" s="T76">laŋk-ɔːj-pa</ta>
            <ta e="T78" id="Seg_994" s="T77">küːk</ta>
            <ta e="T79" id="Seg_995" s="T78">irra</ta>
            <ta e="T80" id="Seg_996" s="T79">sukulta</ta>
            <ta e="T81" id="Seg_997" s="T80">piːqɨlʼ-lʼa</ta>
            <ta e="T82" id="Seg_998" s="T81">külʼa-m</ta>
            <ta e="T83" id="Seg_999" s="T82">qättɨ-mpa-tɨ</ta>
            <ta e="T84" id="Seg_1000" s="T83">külʼa</ta>
            <ta e="T85" id="Seg_1001" s="T84">qu-lʼčʼi-mpa</ta>
            <ta e="T86" id="Seg_1002" s="T85">nʼoma</ta>
            <ta e="T87" id="Seg_1003" s="T86">nı</ta>
            <ta e="T88" id="Seg_1004" s="T87">kətɨ-mpa-tɨ</ta>
            <ta e="T90" id="Seg_1005" s="T88">pusqa-t-ɔːl-aš</ta>
            <ta e="T92" id="Seg_1006" s="T90">pusqa-t-ɔːl-pa</ta>
            <ta e="T93" id="Seg_1007" s="T92">nʼoma</ta>
            <ta e="T94" id="Seg_1008" s="T93">paktɨ-lʼa</ta>
            <ta e="T95" id="Seg_1009" s="T94">čʼoš-i-laka</ta>
            <ta e="T96" id="Seg_1010" s="T95">mišal-mmɨ-ntɨ-tɨ</ta>
            <ta e="T97" id="Seg_1011" s="T96">šitalʼ</ta>
            <ta e="T98" id="Seg_1012" s="T97">moqɨna</ta>
            <ta e="T99" id="Seg_1013" s="T98">qən-pa</ta>
            <ta e="T100" id="Seg_1014" s="T99">tü-mpa</ta>
            <ta e="T101" id="Seg_1015" s="T100">külʼa-n</ta>
            <ta e="T102" id="Seg_1016" s="T101">iːja-jä</ta>
            <ta e="T103" id="Seg_1017" s="T102">soqo-nčʼɔː-tɨn</ta>
            <ta e="T104" id="Seg_1018" s="T103">soqo-nčʼi-ntɨ</ta>
            <ta e="T105" id="Seg_1019" s="T104">amä</ta>
            <ta e="T106" id="Seg_1020" s="T105">kun</ta>
            <ta e="T107" id="Seg_1021" s="T106">ɛː-ŋa</ta>
            <ta e="T108" id="Seg_1022" s="T107">ama-l</ta>
            <ta e="T109" id="Seg_1023" s="T108">čʼoš-i-laka</ta>
            <ta e="T110" id="Seg_1024" s="T109">tačʼal-mmɨ-ntɨ-tɨ</ta>
            <ta e="T111" id="Seg_1025" s="T110">ontɨn</ta>
            <ta e="T112" id="Seg_1026" s="T111">am-ɨ-r-q-olam-pɔː-tɨn</ta>
            <ta e="T113" id="Seg_1027" s="T112">külʼa-n</ta>
            <ta e="T114" id="Seg_1028" s="T113">iːja-ja</ta>
            <ta e="T115" id="Seg_1029" s="T114">aj</ta>
            <ta e="T116" id="Seg_1030" s="T115">tü-ŋa</ta>
            <ta e="T117" id="Seg_1031" s="T116">ama</ta>
            <ta e="T118" id="Seg_1032" s="T117">ɨːrɨŋ</ta>
            <ta e="T119" id="Seg_1033" s="T118">čʼäːŋka</ta>
            <ta e="T120" id="Seg_1034" s="T119">ama-l</ta>
            <ta e="T121" id="Seg_1035" s="T120">ira-laka-lʼa</ta>
            <ta e="T122" id="Seg_1036" s="T121">qätɨ-sɨ-tɨ</ta>
            <ta e="T123" id="Seg_1037" s="T122">moqɨnä</ta>
            <ta e="T124" id="Seg_1038" s="T123">qəlʼ-lʼa</ta>
            <ta e="T125" id="Seg_1039" s="T124">kətɨ-mpa-tɨ</ta>
            <ta e="T126" id="Seg_1040" s="T125">šitalʼ</ta>
            <ta e="T127" id="Seg_1041" s="T126">muntɨk</ta>
            <ta e="T128" id="Seg_1042" s="T127">čʼuːrɨ-q-olom-pɔː-tɨn</ta>
            <ta e="T129" id="Seg_1043" s="T128">turpa-n</ta>
            <ta e="T130" id="Seg_1044" s="T129">mü</ta>
            <ta e="T131" id="Seg_1045" s="T130">šüː-wɨn</ta>
            <ta e="T132" id="Seg_1046" s="T131">ılʼlʼa</ta>
            <ta e="T133" id="Seg_1047" s="T132">sačʼčʼi-mɔːt-tɨn</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1048" s="T0">külä-t</ta>
            <ta e="T2" id="Seg_1049" s="T1">aj</ta>
            <ta e="T3" id="Seg_1050" s="T2">nʼoma-t</ta>
            <ta e="T4" id="Seg_1051" s="T3">ilɨ-mpɨ-tɨt</ta>
            <ta e="T5" id="Seg_1052" s="T4">külä-lʼ</ta>
            <ta e="T6" id="Seg_1053" s="T5">nʼoma-lʼ</ta>
            <ta e="T7" id="Seg_1054" s="T6">ama-sɨ-t</ta>
            <ta e="T8" id="Seg_1055" s="T7">ukkɨr</ta>
            <ta e="T9" id="Seg_1056" s="T8">čʼontɨ-qɨn</ta>
            <ta e="T10" id="Seg_1057" s="T9">nʼoma</ta>
            <ta e="T11" id="Seg_1058" s="T10">qən-mpɨ</ta>
            <ta e="T12" id="Seg_1059" s="T11">šöt-ntɨ</ta>
            <ta e="T13" id="Seg_1060" s="T12">šittälʼ</ta>
            <ta e="T14" id="Seg_1061" s="T13">qo-mpɨ-tɨ</ta>
            <ta e="T15" id="Seg_1062" s="T14">šöt-qɨn</ta>
            <ta e="T16" id="Seg_1063" s="T15">čʼoš-ɨ-lʼ</ta>
            <ta e="T17" id="Seg_1064" s="T16">qaqlɨ</ta>
            <ta e="T18" id="Seg_1065" s="T17">qaqlɨ-n</ta>
            <ta e="T19" id="Seg_1066" s="T18">qan-qɨn</ta>
            <ta e="T20" id="Seg_1067" s="T19">qan-qɨn</ta>
            <ta e="T21" id="Seg_1068" s="T20">ira-laka</ta>
            <ta e="T22" id="Seg_1069" s="T21">nɨŋ-ɨ-ntɨ</ta>
            <ta e="T23" id="Seg_1070" s="T22">nʼoma</ta>
            <ta e="T24" id="Seg_1071" s="T23">nık</ta>
            <ta e="T25" id="Seg_1072" s="T24">kətɨ-mpɨ-tɨ</ta>
            <ta e="T26" id="Seg_1073" s="T25">nannɛr</ta>
            <ta e="T28" id="Seg_1074" s="T26">pusqa-tɨ-ɔːl-äšɨk</ta>
            <ta e="T29" id="Seg_1075" s="T28">tılɨ-lä</ta>
            <ta e="T30" id="Seg_1076" s="T29">ašša</ta>
            <ta e="T31" id="Seg_1077" s="T30">atɨ</ta>
            <ta e="T33" id="Seg_1078" s="T31">pusqa-tɨ-ɔːl-mpɨ</ta>
            <ta e="T34" id="Seg_1079" s="T33">nʼoma</ta>
            <ta e="T35" id="Seg_1080" s="T34">toː</ta>
            <ta e="T36" id="Seg_1081" s="T35">qäl-lä</ta>
            <ta e="T37" id="Seg_1082" s="T36">čʼoš-ɨ-laka</ta>
            <ta e="T38" id="Seg_1083" s="T37">orqɨl-mpɨ-ntɨ-tɨ</ta>
            <ta e="T39" id="Seg_1084" s="T38">šittälʼ</ta>
            <ta e="T40" id="Seg_1085" s="T39">moqɨnä</ta>
            <ta e="T41" id="Seg_1086" s="T40">qən-mpɨ</ta>
            <ta e="T42" id="Seg_1087" s="T41">iːja-lʼa-iː-tɨ-naj</ta>
            <ta e="T43" id="Seg_1088" s="T42">ɔːntɨ-al-mpɨ-tɨt</ta>
            <ta e="T44" id="Seg_1089" s="T43">am-ɨ-r-qo-olam-mpɨ-tɨt</ta>
            <ta e="T45" id="Seg_1090" s="T44">šittälʼ</ta>
            <ta e="T46" id="Seg_1091" s="T45">külä-n</ta>
            <ta e="T47" id="Seg_1092" s="T46">iːja-ja</ta>
            <ta e="T48" id="Seg_1093" s="T47">tü-mpɨ-ntɨ</ta>
            <ta e="T49" id="Seg_1094" s="T48">moqɨnä</ta>
            <ta e="T50" id="Seg_1095" s="T49">paktɨ-mpɨ</ta>
            <ta e="T51" id="Seg_1096" s="T50">ama-tɨ</ta>
            <ta e="T52" id="Seg_1097" s="T51">tü-mpɨ</ta>
            <ta e="T53" id="Seg_1098" s="T52">tɛː</ta>
            <ta e="T54" id="Seg_1099" s="T53">kunɨ</ta>
            <ta e="T55" id="Seg_1100" s="T54">čʼoš-ɨ-m</ta>
            <ta e="T56" id="Seg_1101" s="T55">qo-sɨ-lɨt</ta>
            <ta e="T57" id="Seg_1102" s="T56">man</ta>
            <ta e="T58" id="Seg_1103" s="T57">ira</ta>
            <ta e="T59" id="Seg_1104" s="T58">*nɔː-nɨ</ta>
            <ta e="T60" id="Seg_1105" s="T59">mišal-sɨ-m</ta>
            <ta e="T61" id="Seg_1106" s="T60">täːlɨ</ta>
            <ta e="T62" id="Seg_1107" s="T61">ukkɨr-ššak</ta>
            <ta e="T63" id="Seg_1108" s="T62">qən-lä-mıː</ta>
            <ta e="T64" id="Seg_1109" s="T63">qarɨ-n</ta>
            <ta e="T65" id="Seg_1110" s="T64">qən-mpɨ-qı</ta>
            <ta e="T66" id="Seg_1111" s="T65">ira-laka</ta>
            <ta e="T67" id="Seg_1112" s="T66">čʼoš-ɨ-lʼ</ta>
            <ta e="T68" id="Seg_1113" s="T67">qaqlɨ</ta>
            <ta e="T69" id="Seg_1114" s="T68">üː-qɨl-mpɨ-ntɨ-tɨ</ta>
            <ta e="T70" id="Seg_1115" s="T69">külä</ta>
            <ta e="T71" id="Seg_1116" s="T70">paktɨ-lä</ta>
            <ta e="T72" id="Seg_1117" s="T71">wəšɨ-ɛː-lä</ta>
            <ta e="T73" id="Seg_1118" s="T72">qaqlɨ-n</ta>
            <ta e="T74" id="Seg_1119" s="T73">*iː-ntɨ</ta>
            <ta e="T75" id="Seg_1120" s="T74">omtɨ-lɨ-mpɨ</ta>
            <ta e="T76" id="Seg_1121" s="T75">nık</ta>
            <ta e="T77" id="Seg_1122" s="T76">laŋkɨ-ɔːl-mpɨ</ta>
            <ta e="T78" id="Seg_1123" s="T77">küːk</ta>
            <ta e="T79" id="Seg_1124" s="T78">ira</ta>
            <ta e="T80" id="Seg_1125" s="T79">sukɨltä</ta>
            <ta e="T81" id="Seg_1126" s="T80">piːqɨl-lä</ta>
            <ta e="T82" id="Seg_1127" s="T81">külä-m</ta>
            <ta e="T83" id="Seg_1128" s="T82">qättɨ-mpɨ-tɨ</ta>
            <ta e="T84" id="Seg_1129" s="T83">külä</ta>
            <ta e="T85" id="Seg_1130" s="T84">qu-lʼčʼɨ-mpɨ</ta>
            <ta e="T86" id="Seg_1131" s="T85">nʼoma</ta>
            <ta e="T87" id="Seg_1132" s="T86">nık</ta>
            <ta e="T88" id="Seg_1133" s="T87">kətɨ-mpɨ-tɨ</ta>
            <ta e="T90" id="Seg_1134" s="T88">pusqa-tɨ-ɔːl-äšɨk</ta>
            <ta e="T92" id="Seg_1135" s="T90">pusqa-tɨ-ɔːl-mpɨ</ta>
            <ta e="T93" id="Seg_1136" s="T92">nʼoma</ta>
            <ta e="T94" id="Seg_1137" s="T93">paktɨ-lä</ta>
            <ta e="T95" id="Seg_1138" s="T94">čʼoš-ɨ-laka</ta>
            <ta e="T96" id="Seg_1139" s="T95">mišal-mpɨ-ntɨ-tɨ</ta>
            <ta e="T97" id="Seg_1140" s="T96">šittälʼ</ta>
            <ta e="T98" id="Seg_1141" s="T97">moqɨnä</ta>
            <ta e="T99" id="Seg_1142" s="T98">qən-mpɨ</ta>
            <ta e="T100" id="Seg_1143" s="T99">tü-mpɨ</ta>
            <ta e="T101" id="Seg_1144" s="T100">külä-n</ta>
            <ta e="T102" id="Seg_1145" s="T101">iːja-ja</ta>
            <ta e="T103" id="Seg_1146" s="T102">soqɨš-ntɨ-tɨt</ta>
            <ta e="T104" id="Seg_1147" s="T103">soqɨš-ntɨ-ntɨ</ta>
            <ta e="T105" id="Seg_1148" s="T104">ama</ta>
            <ta e="T106" id="Seg_1149" s="T105">kun</ta>
            <ta e="T107" id="Seg_1150" s="T106">ɛː-ŋɨ</ta>
            <ta e="T108" id="Seg_1151" s="T107">ama-lɨ</ta>
            <ta e="T109" id="Seg_1152" s="T108">čʼoš-ɨ-laka</ta>
            <ta e="T110" id="Seg_1153" s="T109">tačʼal-mpɨ-ntɨ-tɨ</ta>
            <ta e="T111" id="Seg_1154" s="T110">ontɨt</ta>
            <ta e="T112" id="Seg_1155" s="T111">am-ɨ-r-qo-olam-mpɨ-tɨt</ta>
            <ta e="T113" id="Seg_1156" s="T112">külä-n</ta>
            <ta e="T114" id="Seg_1157" s="T113">iːja-ja</ta>
            <ta e="T115" id="Seg_1158" s="T114">aj</ta>
            <ta e="T116" id="Seg_1159" s="T115">tü-ŋɨ</ta>
            <ta e="T117" id="Seg_1160" s="T116">ama</ta>
            <ta e="T118" id="Seg_1161" s="T117">ɨːrɨŋ</ta>
            <ta e="T119" id="Seg_1162" s="T118">čʼäːŋkɨ</ta>
            <ta e="T120" id="Seg_1163" s="T119">ama-lɨ</ta>
            <ta e="T121" id="Seg_1164" s="T120">ira-laka-lʼa</ta>
            <ta e="T122" id="Seg_1165" s="T121">qättɨ-sɨ-tɨ</ta>
            <ta e="T123" id="Seg_1166" s="T122">moqɨnä</ta>
            <ta e="T124" id="Seg_1167" s="T123">qən-lä</ta>
            <ta e="T125" id="Seg_1168" s="T124">kətɨ-mpɨ-tɨ</ta>
            <ta e="T126" id="Seg_1169" s="T125">šittälʼ</ta>
            <ta e="T127" id="Seg_1170" s="T126">muntɨk</ta>
            <ta e="T128" id="Seg_1171" s="T127">čʼuːrɨ-qo-olam-mpɨ-tɨt</ta>
            <ta e="T129" id="Seg_1172" s="T128">turpa-n</ta>
            <ta e="T130" id="Seg_1173" s="T129">mü</ta>
            <ta e="T131" id="Seg_1174" s="T130">*šüː-mɨn</ta>
            <ta e="T132" id="Seg_1175" s="T131">ıllä</ta>
            <ta e="T133" id="Seg_1176" s="T132">sačʼčʼɨ-mɔːt-tɨt</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1177" s="T0">raven-PL.[NOM]</ta>
            <ta e="T2" id="Seg_1178" s="T1">and</ta>
            <ta e="T3" id="Seg_1179" s="T2">hare-PL.[NOM]</ta>
            <ta e="T4" id="Seg_1180" s="T3">live-PST.NAR-3PL</ta>
            <ta e="T5" id="Seg_1181" s="T4">raven-ADJZ</ta>
            <ta e="T6" id="Seg_1182" s="T5">hare-ADJZ</ta>
            <ta e="T7" id="Seg_1183" s="T6">mother-DYA-PL.[NOM]</ta>
            <ta e="T8" id="Seg_1184" s="T7">one</ta>
            <ta e="T9" id="Seg_1185" s="T8">middle-LOC</ta>
            <ta e="T10" id="Seg_1186" s="T9">hare.[NOM]</ta>
            <ta e="T11" id="Seg_1187" s="T10">leave-PST.NAR.[3SG.S]</ta>
            <ta e="T12" id="Seg_1188" s="T11">forest-ILL</ta>
            <ta e="T13" id="Seg_1189" s="T12">then</ta>
            <ta e="T14" id="Seg_1190" s="T13">see-PST.NAR-3SG.O</ta>
            <ta e="T15" id="Seg_1191" s="T14">forest-LOC</ta>
            <ta e="T16" id="Seg_1192" s="T15">fat-EP-ADJZ</ta>
            <ta e="T17" id="Seg_1193" s="T16">sledge.[NOM]</ta>
            <ta e="T18" id="Seg_1194" s="T17">sledge-GEN</ta>
            <ta e="T19" id="Seg_1195" s="T18">near-LOC</ta>
            <ta e="T20" id="Seg_1196" s="T19">near-LOC</ta>
            <ta e="T21" id="Seg_1197" s="T20">old.man-SNGL.[NOM]</ta>
            <ta e="T22" id="Seg_1198" s="T21">stand-EP-IPFV.[3SG.S]</ta>
            <ta e="T23" id="Seg_1199" s="T22">hare.[NOM]</ta>
            <ta e="T24" id="Seg_1200" s="T23">so</ta>
            <ta e="T25" id="Seg_1201" s="T24">say-PST.NAR-3SG.O</ta>
            <ta e="T26" id="Seg_1202" s="T25">so.much</ta>
            <ta e="T28" id="Seg_1203" s="T26">storm-TR-MOM-IMP.2SG.S</ta>
            <ta e="T29" id="Seg_1204" s="T28">rise-CVB</ta>
            <ta e="T30" id="Seg_1205" s="T29">NEG</ta>
            <ta e="T31" id="Seg_1206" s="T30">be.visible.[3SG.S]</ta>
            <ta e="T33" id="Seg_1207" s="T31">storm-TR-MOM-PST.NAR.[3SG.S]</ta>
            <ta e="T34" id="Seg_1208" s="T33">hare.[NOM]</ta>
            <ta e="T35" id="Seg_1209" s="T34">there</ta>
            <ta e="T36" id="Seg_1210" s="T35">go-CVB</ta>
            <ta e="T37" id="Seg_1211" s="T36">fat-EP-SNGL</ta>
            <ta e="T38" id="Seg_1212" s="T37">catch-PST.NAR-INFER-3SG.O</ta>
            <ta e="T39" id="Seg_1213" s="T38">then</ta>
            <ta e="T40" id="Seg_1214" s="T39">home</ta>
            <ta e="T41" id="Seg_1215" s="T40">leave-PST.NAR.[3SG.S]</ta>
            <ta e="T42" id="Seg_1216" s="T41">child-DIM-PL.[NOM]-3SG-EMPH</ta>
            <ta e="T43" id="Seg_1217" s="T42">happiness-TR-PST.NAR-3PL</ta>
            <ta e="T44" id="Seg_1218" s="T43">eat-EP-FRQ-INF-begin-PST.NAR-3PL</ta>
            <ta e="T45" id="Seg_1219" s="T44">then</ta>
            <ta e="T46" id="Seg_1220" s="T45">raven-GEN</ta>
            <ta e="T47" id="Seg_1221" s="T46">child-DIM.[NOM]</ta>
            <ta e="T48" id="Seg_1222" s="T47">come-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T49" id="Seg_1223" s="T48">home</ta>
            <ta e="T50" id="Seg_1224" s="T49">run-PST.NAR.[3SG.S]</ta>
            <ta e="T51" id="Seg_1225" s="T50">mother.[NOM]-3SG</ta>
            <ta e="T52" id="Seg_1226" s="T51">come-PST.NAR.[3SG.S]</ta>
            <ta e="T53" id="Seg_1227" s="T52">you.PL.NOM</ta>
            <ta e="T54" id="Seg_1228" s="T53">where.from</ta>
            <ta e="T55" id="Seg_1229" s="T54">fat-EP-ACC</ta>
            <ta e="T56" id="Seg_1230" s="T55">find-PST-2PL</ta>
            <ta e="T57" id="Seg_1231" s="T56">I.NOM</ta>
            <ta e="T58" id="Seg_1232" s="T57">old.man.[NOM]</ta>
            <ta e="T59" id="Seg_1233" s="T58">out-ADV.EL</ta>
            <ta e="T60" id="Seg_1234" s="T59">pick-PST-1SG.O</ta>
            <ta e="T61" id="Seg_1235" s="T60">tomorrow</ta>
            <ta e="T62" id="Seg_1236" s="T61">one-COR</ta>
            <ta e="T63" id="Seg_1237" s="T62">go.away-OPT-1DU</ta>
            <ta e="T64" id="Seg_1238" s="T63">morning-ADV.LOC</ta>
            <ta e="T65" id="Seg_1239" s="T64">go.away-PST.NAR-3DU.S</ta>
            <ta e="T66" id="Seg_1240" s="T65">old.man-SNGL.[NOM]</ta>
            <ta e="T67" id="Seg_1241" s="T66">fat-EP-ADJZ</ta>
            <ta e="T68" id="Seg_1242" s="T67">sledge.[NOM]</ta>
            <ta e="T69" id="Seg_1243" s="T68">pull-MULO-PST.NAR-INFER-3SG.O</ta>
            <ta e="T70" id="Seg_1244" s="T69">raven.[NOM]</ta>
            <ta e="T71" id="Seg_1245" s="T70">run-CVB</ta>
            <ta e="T72" id="Seg_1246" s="T71">get.up-PFV-CVB</ta>
            <ta e="T73" id="Seg_1247" s="T72">sledge-GEN</ta>
            <ta e="T74" id="Seg_1248" s="T73">on-ILL</ta>
            <ta e="T75" id="Seg_1249" s="T74">sit.down-RES-PST.NAR.[3SG.S]</ta>
            <ta e="T76" id="Seg_1250" s="T75">so</ta>
            <ta e="T77" id="Seg_1251" s="T76">cry-MOM-PST.NAR.[3SG.S]</ta>
            <ta e="T78" id="Seg_1252" s="T77">kraa</ta>
            <ta e="T79" id="Seg_1253" s="T78">old.man.[NOM]</ta>
            <ta e="T80" id="Seg_1254" s="T79">back</ta>
            <ta e="T81" id="Seg_1255" s="T80">turn-CVB</ta>
            <ta e="T82" id="Seg_1256" s="T81">raven-ACC</ta>
            <ta e="T83" id="Seg_1257" s="T82">hit-PST.NAR-3SG.O</ta>
            <ta e="T84" id="Seg_1258" s="T83">raven.[NOM]</ta>
            <ta e="T85" id="Seg_1259" s="T84">die-PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T86" id="Seg_1260" s="T85">hare.[NOM]</ta>
            <ta e="T87" id="Seg_1261" s="T86">so</ta>
            <ta e="T88" id="Seg_1262" s="T87">say-PST.NAR-3SG.O</ta>
            <ta e="T90" id="Seg_1263" s="T88">storm-TR-MOM-IMP.2SG.S</ta>
            <ta e="T92" id="Seg_1264" s="T90">storm-TR-MOM-PST.NAR.[3SG.S]</ta>
            <ta e="T93" id="Seg_1265" s="T92">hare.[NOM]</ta>
            <ta e="T94" id="Seg_1266" s="T93">run-CVB</ta>
            <ta e="T95" id="Seg_1267" s="T94">fat-EP-SNGL.[NOM]</ta>
            <ta e="T96" id="Seg_1268" s="T95">pick-PST.NAR-INFER-3SG.O</ta>
            <ta e="T97" id="Seg_1269" s="T96">then</ta>
            <ta e="T98" id="Seg_1270" s="T97">home</ta>
            <ta e="T99" id="Seg_1271" s="T98">leave-PST.NAR.[3SG.S]</ta>
            <ta e="T100" id="Seg_1272" s="T99">come-PST.NAR.[3SG.S]</ta>
            <ta e="T101" id="Seg_1273" s="T100">raven-GEN</ta>
            <ta e="T102" id="Seg_1274" s="T101">child-DIM.[NOM]</ta>
            <ta e="T103" id="Seg_1275" s="T102">ask-IPFV-3PL</ta>
            <ta e="T104" id="Seg_1276" s="T103">ask-IPFV-INFER.[3SG.S]</ta>
            <ta e="T105" id="Seg_1277" s="T104">mother.[NOM]</ta>
            <ta e="T106" id="Seg_1278" s="T105">where</ta>
            <ta e="T107" id="Seg_1279" s="T106">be-CO.[3SG.S]</ta>
            <ta e="T108" id="Seg_1280" s="T107">mother.[NOM]-2SG</ta>
            <ta e="T109" id="Seg_1281" s="T108">fat-EP-SNGL.[NOM]</ta>
            <ta e="T110" id="Seg_1282" s="T109">not.can-PST.NAR-INFER-3SG.O</ta>
            <ta e="T111" id="Seg_1283" s="T110">oneself.3PL.[NOM]</ta>
            <ta e="T112" id="Seg_1284" s="T111">eat-EP-FRQ-INF-begin-PST.NAR-3PL</ta>
            <ta e="T113" id="Seg_1285" s="T112">raven-GEN</ta>
            <ta e="T114" id="Seg_1286" s="T113">child-DIM.[NOM]</ta>
            <ta e="T115" id="Seg_1287" s="T114">again</ta>
            <ta e="T116" id="Seg_1288" s="T115">come-CO.[3SG.S]</ta>
            <ta e="T117" id="Seg_1289" s="T116">mother.[NOM]</ta>
            <ta e="T118" id="Seg_1290" s="T117">still</ta>
            <ta e="T119" id="Seg_1291" s="T118">NEG.EX.[3SG.S]</ta>
            <ta e="T120" id="Seg_1292" s="T119">mother.[NOM]-2SG</ta>
            <ta e="T121" id="Seg_1293" s="T120">old.man-SNGL-DIM.[NOM]</ta>
            <ta e="T122" id="Seg_1294" s="T121">hit-PST-3SG.O</ta>
            <ta e="T123" id="Seg_1295" s="T122">home</ta>
            <ta e="T124" id="Seg_1296" s="T123">leave-CVB</ta>
            <ta e="T125" id="Seg_1297" s="T124">say-PST.NAR-3SG.O</ta>
            <ta e="T126" id="Seg_1298" s="T125">then</ta>
            <ta e="T127" id="Seg_1299" s="T126">all</ta>
            <ta e="T128" id="Seg_1300" s="T127">cry-INF-begin-PST.NAR-3PL</ta>
            <ta e="T129" id="Seg_1301" s="T128">chimney-GEN</ta>
            <ta e="T130" id="Seg_1302" s="T129">hole.[NOM]</ta>
            <ta e="T131" id="Seg_1303" s="T130">through-PROL</ta>
            <ta e="T132" id="Seg_1304" s="T131">down</ta>
            <ta e="T133" id="Seg_1305" s="T132">take.out-DRV-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1306" s="T0">ворон-PL.[NOM]</ta>
            <ta e="T2" id="Seg_1307" s="T1">и</ta>
            <ta e="T3" id="Seg_1308" s="T2">заяц-PL.[NOM]</ta>
            <ta e="T4" id="Seg_1309" s="T3">жить-PST.NAR-3PL</ta>
            <ta e="T5" id="Seg_1310" s="T4">ворон-ADJZ</ta>
            <ta e="T6" id="Seg_1311" s="T5">заяц-ADJZ</ta>
            <ta e="T7" id="Seg_1312" s="T6">мать-DYA-PL.[NOM]</ta>
            <ta e="T8" id="Seg_1313" s="T7">один</ta>
            <ta e="T9" id="Seg_1314" s="T8">середина-LOC</ta>
            <ta e="T10" id="Seg_1315" s="T9">заяц.[NOM]</ta>
            <ta e="T11" id="Seg_1316" s="T10">отправиться-PST.NAR.[3SG.S]</ta>
            <ta e="T12" id="Seg_1317" s="T11">лес-ILL</ta>
            <ta e="T13" id="Seg_1318" s="T12">потом</ta>
            <ta e="T14" id="Seg_1319" s="T13">видеть-PST.NAR-3SG.O</ta>
            <ta e="T15" id="Seg_1320" s="T14">лес-LOC</ta>
            <ta e="T16" id="Seg_1321" s="T15">сало-EP-ADJZ</ta>
            <ta e="T17" id="Seg_1322" s="T16">нарты.[NOM]</ta>
            <ta e="T18" id="Seg_1323" s="T17">нарты-GEN</ta>
            <ta e="T19" id="Seg_1324" s="T18">рядом-LOC</ta>
            <ta e="T20" id="Seg_1325" s="T19">рядом-LOC</ta>
            <ta e="T21" id="Seg_1326" s="T20">старик-SNGL.[NOM]</ta>
            <ta e="T22" id="Seg_1327" s="T21">стоять-EP-IPFV.[3SG.S]</ta>
            <ta e="T23" id="Seg_1328" s="T22">заяц.[NOM]</ta>
            <ta e="T24" id="Seg_1329" s="T23">так</ta>
            <ta e="T25" id="Seg_1330" s="T24">сказать-PST.NAR-3SG.O</ta>
            <ta e="T26" id="Seg_1331" s="T25">настолько</ta>
            <ta e="T28" id="Seg_1332" s="T26">буря-TR-MOM-IMP.2SG.S</ta>
            <ta e="T29" id="Seg_1333" s="T28">поднять-CVB</ta>
            <ta e="T30" id="Seg_1334" s="T29">NEG</ta>
            <ta e="T31" id="Seg_1335" s="T30">виднеться.[3SG.S]</ta>
            <ta e="T33" id="Seg_1336" s="T31">буря-TR-MOM-PST.NAR.[3SG.S]</ta>
            <ta e="T34" id="Seg_1337" s="T33">заяц.[NOM]</ta>
            <ta e="T35" id="Seg_1338" s="T34">туда</ta>
            <ta e="T36" id="Seg_1339" s="T35">ходить-CVB</ta>
            <ta e="T37" id="Seg_1340" s="T36">сало-EP-SNGL</ta>
            <ta e="T38" id="Seg_1341" s="T37">схватить-PST.NAR-INFER-3SG.O</ta>
            <ta e="T39" id="Seg_1342" s="T38">потом</ta>
            <ta e="T40" id="Seg_1343" s="T39">домой</ta>
            <ta e="T41" id="Seg_1344" s="T40">отправиться-PST.NAR.[3SG.S]</ta>
            <ta e="T42" id="Seg_1345" s="T41">ребенок-DIM-PL.[NOM]-3SG-EMPH</ta>
            <ta e="T43" id="Seg_1346" s="T42">радость-TR-PST.NAR-3PL</ta>
            <ta e="T44" id="Seg_1347" s="T43">съесть-EP-FRQ-INF-начать-PST.NAR-3PL</ta>
            <ta e="T45" id="Seg_1348" s="T44">потом</ta>
            <ta e="T46" id="Seg_1349" s="T45">ворон-GEN</ta>
            <ta e="T47" id="Seg_1350" s="T46">ребенок-DIM.[NOM]</ta>
            <ta e="T48" id="Seg_1351" s="T47">прийти-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T49" id="Seg_1352" s="T48">домой</ta>
            <ta e="T50" id="Seg_1353" s="T49">побежать-PST.NAR.[3SG.S]</ta>
            <ta e="T51" id="Seg_1354" s="T50">мать.[NOM]-3SG</ta>
            <ta e="T52" id="Seg_1355" s="T51">прийти-PST.NAR.[3SG.S]</ta>
            <ta e="T53" id="Seg_1356" s="T52">вы.PL.NOM</ta>
            <ta e="T54" id="Seg_1357" s="T53">откуда</ta>
            <ta e="T55" id="Seg_1358" s="T54">сало-EP-ACC</ta>
            <ta e="T56" id="Seg_1359" s="T55">находить-PST-2PL</ta>
            <ta e="T57" id="Seg_1360" s="T56">я.NOM</ta>
            <ta e="T58" id="Seg_1361" s="T57">старик.[NOM]</ta>
            <ta e="T59" id="Seg_1362" s="T58">из-ADV.EL</ta>
            <ta e="T60" id="Seg_1363" s="T59">забрать-PST-1SG.O</ta>
            <ta e="T61" id="Seg_1364" s="T60">завтра</ta>
            <ta e="T62" id="Seg_1365" s="T61">один-COR</ta>
            <ta e="T63" id="Seg_1366" s="T62">уйти-OPT-1DU</ta>
            <ta e="T64" id="Seg_1367" s="T63">утро-ADV.LOC</ta>
            <ta e="T65" id="Seg_1368" s="T64">уйти-PST.NAR-3DU.S</ta>
            <ta e="T66" id="Seg_1369" s="T65">старик-SNGL.[NOM]</ta>
            <ta e="T67" id="Seg_1370" s="T66">сало-EP-ADJZ</ta>
            <ta e="T68" id="Seg_1371" s="T67">нарты.[NOM]</ta>
            <ta e="T69" id="Seg_1372" s="T68">тащить-MULO-PST.NAR-INFER-3SG.O</ta>
            <ta e="T70" id="Seg_1373" s="T69">ворон.[NOM]</ta>
            <ta e="T71" id="Seg_1374" s="T70">побежать-CVB</ta>
            <ta e="T72" id="Seg_1375" s="T71">встать-PFV-CVB</ta>
            <ta e="T73" id="Seg_1376" s="T72">нарты-GEN</ta>
            <ta e="T74" id="Seg_1377" s="T73">на-ILL</ta>
            <ta e="T75" id="Seg_1378" s="T74">сесть-RES-PST.NAR.[3SG.S]</ta>
            <ta e="T76" id="Seg_1379" s="T75">так</ta>
            <ta e="T77" id="Seg_1380" s="T76">кричать-MOM-PST.NAR.[3SG.S]</ta>
            <ta e="T78" id="Seg_1381" s="T77">кр_кр</ta>
            <ta e="T79" id="Seg_1382" s="T78">старик.[NOM]</ta>
            <ta e="T80" id="Seg_1383" s="T79">назад</ta>
            <ta e="T81" id="Seg_1384" s="T80">повернуться-CVB</ta>
            <ta e="T82" id="Seg_1385" s="T81">ворон-ACC</ta>
            <ta e="T83" id="Seg_1386" s="T82">ударить-PST.NAR-3SG.O</ta>
            <ta e="T84" id="Seg_1387" s="T83">ворон.[NOM]</ta>
            <ta e="T85" id="Seg_1388" s="T84">умереть-PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T86" id="Seg_1389" s="T85">заяц.[NOM]</ta>
            <ta e="T87" id="Seg_1390" s="T86">так</ta>
            <ta e="T88" id="Seg_1391" s="T87">сказать-PST.NAR-3SG.O</ta>
            <ta e="T90" id="Seg_1392" s="T88">буря-TR-MOM-IMP.2SG.S</ta>
            <ta e="T92" id="Seg_1393" s="T90">буря-TR-MOM-PST.NAR.[3SG.S]</ta>
            <ta e="T93" id="Seg_1394" s="T92">заяц.[NOM]</ta>
            <ta e="T94" id="Seg_1395" s="T93">побежать-CVB</ta>
            <ta e="T95" id="Seg_1396" s="T94">сало-EP-SNGL.[NOM]</ta>
            <ta e="T96" id="Seg_1397" s="T95">забрать-PST.NAR-INFER-3SG.O</ta>
            <ta e="T97" id="Seg_1398" s="T96">потом</ta>
            <ta e="T98" id="Seg_1399" s="T97">домой</ta>
            <ta e="T99" id="Seg_1400" s="T98">отправиться-PST.NAR.[3SG.S]</ta>
            <ta e="T100" id="Seg_1401" s="T99">прийти-PST.NAR.[3SG.S]</ta>
            <ta e="T101" id="Seg_1402" s="T100">ворон-GEN</ta>
            <ta e="T102" id="Seg_1403" s="T101">ребенок-DIM.[NOM]</ta>
            <ta e="T103" id="Seg_1404" s="T102">спрашивать-IPFV-3PL</ta>
            <ta e="T104" id="Seg_1405" s="T103">спрашивать-IPFV-INFER.[3SG.S]</ta>
            <ta e="T105" id="Seg_1406" s="T104">мать.[NOM]</ta>
            <ta e="T106" id="Seg_1407" s="T105">где</ta>
            <ta e="T107" id="Seg_1408" s="T106">быть-CO.[3SG.S]</ta>
            <ta e="T108" id="Seg_1409" s="T107">мать.[NOM]-2SG</ta>
            <ta e="T109" id="Seg_1410" s="T108">сало-EP-SNGL.[NOM]</ta>
            <ta e="T110" id="Seg_1411" s="T109">не.мочь-PST.NAR-INFER-3SG.O</ta>
            <ta e="T111" id="Seg_1412" s="T110">сам.3PL.[NOM]</ta>
            <ta e="T112" id="Seg_1413" s="T111">съесть-EP-FRQ-INF-начать-PST.NAR-3PL</ta>
            <ta e="T113" id="Seg_1414" s="T112">ворон-GEN</ta>
            <ta e="T114" id="Seg_1415" s="T113">ребенок-DIM.[NOM]</ta>
            <ta e="T115" id="Seg_1416" s="T114">опять</ta>
            <ta e="T116" id="Seg_1417" s="T115">прийти-CO.[3SG.S]</ta>
            <ta e="T117" id="Seg_1418" s="T116">мать.[NOM]</ta>
            <ta e="T118" id="Seg_1419" s="T117">всё.ёще</ta>
            <ta e="T119" id="Seg_1420" s="T118">NEG.EX.[3SG.S]</ta>
            <ta e="T120" id="Seg_1421" s="T119">мать.[NOM]-2SG</ta>
            <ta e="T121" id="Seg_1422" s="T120">старик-SNGL-DIM.[NOM]</ta>
            <ta e="T122" id="Seg_1423" s="T121">ударить-PST-3SG.O</ta>
            <ta e="T123" id="Seg_1424" s="T122">домой</ta>
            <ta e="T124" id="Seg_1425" s="T123">отправиться-CVB</ta>
            <ta e="T125" id="Seg_1426" s="T124">сказать-PST.NAR-3SG.O</ta>
            <ta e="T126" id="Seg_1427" s="T125">потом</ta>
            <ta e="T127" id="Seg_1428" s="T126">всё</ta>
            <ta e="T128" id="Seg_1429" s="T127">плакать-INF-начать-PST.NAR-3PL</ta>
            <ta e="T129" id="Seg_1430" s="T128">труба-GEN</ta>
            <ta e="T130" id="Seg_1431" s="T129">дыра.[NOM]</ta>
            <ta e="T131" id="Seg_1432" s="T130">сквозь-PROL</ta>
            <ta e="T132" id="Seg_1433" s="T131">вниз</ta>
            <ta e="T133" id="Seg_1434" s="T132">вытащить-DRV-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1435" s="T0">n-n:num-n:case</ta>
            <ta e="T2" id="Seg_1436" s="T1">conj</ta>
            <ta e="T3" id="Seg_1437" s="T2">n-n:num-n:case</ta>
            <ta e="T4" id="Seg_1438" s="T3">v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_1439" s="T4">n-n&gt;adj</ta>
            <ta e="T6" id="Seg_1440" s="T5">n-n&gt;adj</ta>
            <ta e="T7" id="Seg_1441" s="T6">n-n&gt;n-n:num-n:case</ta>
            <ta e="T8" id="Seg_1442" s="T7">num</ta>
            <ta e="T9" id="Seg_1443" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_1444" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_1445" s="T10">v-v:tense-v:pn</ta>
            <ta e="T12" id="Seg_1446" s="T11">n-n:case</ta>
            <ta e="T13" id="Seg_1447" s="T12">adv</ta>
            <ta e="T14" id="Seg_1448" s="T13">v-v:tense-v:pn</ta>
            <ta e="T15" id="Seg_1449" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_1450" s="T15">n-n:ins-n&gt;adj</ta>
            <ta e="T17" id="Seg_1451" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_1452" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_1453" s="T18">pp-n:case</ta>
            <ta e="T20" id="Seg_1454" s="T19">pp-n:case</ta>
            <ta e="T21" id="Seg_1455" s="T20">n-n&gt;n-n:case</ta>
            <ta e="T22" id="Seg_1456" s="T21">v-v:ins-v&gt;v-v:pn</ta>
            <ta e="T23" id="Seg_1457" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_1458" s="T23">adv</ta>
            <ta e="T25" id="Seg_1459" s="T24">v-v:tense-v:pn</ta>
            <ta e="T26" id="Seg_1460" s="T25">adv</ta>
            <ta e="T28" id="Seg_1461" s="T26">n-n&gt;v-v&gt;v-v:mood.pn</ta>
            <ta e="T29" id="Seg_1462" s="T28">v-v&gt;adv</ta>
            <ta e="T30" id="Seg_1463" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_1464" s="T30">v-v:pn</ta>
            <ta e="T33" id="Seg_1465" s="T31">n-n&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T34" id="Seg_1466" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_1467" s="T34">adv</ta>
            <ta e="T36" id="Seg_1468" s="T35">v-v&gt;adv</ta>
            <ta e="T37" id="Seg_1469" s="T36">n-n:ins-n&gt;n</ta>
            <ta e="T38" id="Seg_1470" s="T37">v-v:tense-v:mood-v:pn</ta>
            <ta e="T39" id="Seg_1471" s="T38">adv</ta>
            <ta e="T40" id="Seg_1472" s="T39">adv</ta>
            <ta e="T41" id="Seg_1473" s="T40">v-v:tense-v:pn</ta>
            <ta e="T42" id="Seg_1474" s="T41">n-n&gt;n-n:num-n:case-n:poss-clit</ta>
            <ta e="T43" id="Seg_1475" s="T42">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T44" id="Seg_1476" s="T43">v-v:ins-v&gt;v-v:inf-v-v:tense-v:pn</ta>
            <ta e="T45" id="Seg_1477" s="T44">adv</ta>
            <ta e="T46" id="Seg_1478" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_1479" s="T46">n-n&gt;n-n:case</ta>
            <ta e="T48" id="Seg_1480" s="T47">v-v:tense-v:mood-v:pn</ta>
            <ta e="T49" id="Seg_1481" s="T48">adv</ta>
            <ta e="T50" id="Seg_1482" s="T49">v-v:tense-v:pn</ta>
            <ta e="T51" id="Seg_1483" s="T50">n-n:case-n:poss</ta>
            <ta e="T52" id="Seg_1484" s="T51">v-v:tense-v:pn</ta>
            <ta e="T53" id="Seg_1485" s="T52">pers</ta>
            <ta e="T54" id="Seg_1486" s="T53">interrog</ta>
            <ta e="T55" id="Seg_1487" s="T54">n-n:ins-n:case</ta>
            <ta e="T56" id="Seg_1488" s="T55">v-v:tense-v:pn</ta>
            <ta e="T57" id="Seg_1489" s="T56">pers</ta>
            <ta e="T58" id="Seg_1490" s="T57">n-n:case</ta>
            <ta e="T59" id="Seg_1491" s="T58">pp-adv:case</ta>
            <ta e="T60" id="Seg_1492" s="T59">v-v:tense-v:pn</ta>
            <ta e="T61" id="Seg_1493" s="T60">adv</ta>
            <ta e="T62" id="Seg_1494" s="T61">num-n:case</ta>
            <ta e="T63" id="Seg_1495" s="T62">v-v:mood-v:pn</ta>
            <ta e="T64" id="Seg_1496" s="T63">n-n&gt;adv</ta>
            <ta e="T65" id="Seg_1497" s="T64">v-v:tense-v:pn</ta>
            <ta e="T66" id="Seg_1498" s="T65">n-n&gt;n-n:case</ta>
            <ta e="T67" id="Seg_1499" s="T66">n-n:ins-n&gt;adj</ta>
            <ta e="T68" id="Seg_1500" s="T67">n-n:case</ta>
            <ta e="T69" id="Seg_1501" s="T68">v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T70" id="Seg_1502" s="T69">n-n:case</ta>
            <ta e="T71" id="Seg_1503" s="T70">v-v&gt;adv</ta>
            <ta e="T72" id="Seg_1504" s="T71">v-v&gt;v-v&gt;adv</ta>
            <ta e="T73" id="Seg_1505" s="T72">n-n:case</ta>
            <ta e="T74" id="Seg_1506" s="T73">pp-n:case</ta>
            <ta e="T75" id="Seg_1507" s="T74">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T76" id="Seg_1508" s="T75">adv</ta>
            <ta e="T77" id="Seg_1509" s="T76">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_1510" s="T77">interj</ta>
            <ta e="T79" id="Seg_1511" s="T78">n-n:case</ta>
            <ta e="T80" id="Seg_1512" s="T79">adv</ta>
            <ta e="T81" id="Seg_1513" s="T80">v-v&gt;adv</ta>
            <ta e="T82" id="Seg_1514" s="T81">n-n:case</ta>
            <ta e="T83" id="Seg_1515" s="T82">v-v:tense-v:pn</ta>
            <ta e="T84" id="Seg_1516" s="T83">n-n:case</ta>
            <ta e="T85" id="Seg_1517" s="T84">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T86" id="Seg_1518" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_1519" s="T86">adv</ta>
            <ta e="T88" id="Seg_1520" s="T87">v-v:tense-v:pn</ta>
            <ta e="T90" id="Seg_1521" s="T88">n-n&gt;v-v&gt;v-v:mood.pn</ta>
            <ta e="T92" id="Seg_1522" s="T90">n-n&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T93" id="Seg_1523" s="T92">n-n:case</ta>
            <ta e="T94" id="Seg_1524" s="T93">v-v&gt;adv</ta>
            <ta e="T95" id="Seg_1525" s="T94">n-n:ins-n&gt;n-n:case</ta>
            <ta e="T96" id="Seg_1526" s="T95">v-v:tense-v:mood-v:pn</ta>
            <ta e="T97" id="Seg_1527" s="T96">adv</ta>
            <ta e="T98" id="Seg_1528" s="T97">adv</ta>
            <ta e="T99" id="Seg_1529" s="T98">v-v:tense-v:pn</ta>
            <ta e="T100" id="Seg_1530" s="T99">v-v:tense-v:pn</ta>
            <ta e="T101" id="Seg_1531" s="T100">n-n:case</ta>
            <ta e="T102" id="Seg_1532" s="T101">n-n&gt;n-n:case</ta>
            <ta e="T103" id="Seg_1533" s="T102">v-v&gt;v-v:pn</ta>
            <ta e="T104" id="Seg_1534" s="T103">v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T105" id="Seg_1535" s="T104">n-n:case</ta>
            <ta e="T106" id="Seg_1536" s="T105">interrog</ta>
            <ta e="T107" id="Seg_1537" s="T106">v-v:ins-v:pn</ta>
            <ta e="T108" id="Seg_1538" s="T107">n-n:case-n:poss</ta>
            <ta e="T109" id="Seg_1539" s="T108">n-n:ins-n&gt;n-n:case</ta>
            <ta e="T110" id="Seg_1540" s="T109">v-v:tense-v:mood-v:pn</ta>
            <ta e="T111" id="Seg_1541" s="T110">emphpro-n:case</ta>
            <ta e="T112" id="Seg_1542" s="T111">v-v:ins-v&gt;v-v:inf-v-v:tense-v:pn</ta>
            <ta e="T113" id="Seg_1543" s="T112">n-n:case</ta>
            <ta e="T114" id="Seg_1544" s="T113">n-n&gt;n-n:case</ta>
            <ta e="T115" id="Seg_1545" s="T114">adv</ta>
            <ta e="T116" id="Seg_1546" s="T115">v-v:ins-v:pn</ta>
            <ta e="T117" id="Seg_1547" s="T116">n-n:case</ta>
            <ta e="T118" id="Seg_1548" s="T117">adv</ta>
            <ta e="T119" id="Seg_1549" s="T118">v-v:pn</ta>
            <ta e="T120" id="Seg_1550" s="T119">n-n:case-n:poss</ta>
            <ta e="T121" id="Seg_1551" s="T120">n-n&gt;n-n&gt;n-n:case</ta>
            <ta e="T122" id="Seg_1552" s="T121">v-v:tense-v:pn</ta>
            <ta e="T123" id="Seg_1553" s="T122">adv</ta>
            <ta e="T124" id="Seg_1554" s="T123">v-v&gt;adv</ta>
            <ta e="T125" id="Seg_1555" s="T124">v-v:tense-v:pn</ta>
            <ta e="T126" id="Seg_1556" s="T125">adv</ta>
            <ta e="T127" id="Seg_1557" s="T126">quant</ta>
            <ta e="T128" id="Seg_1558" s="T127">v-v:inf-v-v:tense-v:pn</ta>
            <ta e="T129" id="Seg_1559" s="T128">n-n:case</ta>
            <ta e="T130" id="Seg_1560" s="T129">n-n:case</ta>
            <ta e="T131" id="Seg_1561" s="T130">pp-n:case</ta>
            <ta e="T132" id="Seg_1562" s="T131">preverb</ta>
            <ta e="T133" id="Seg_1563" s="T132">v-v&gt;v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1564" s="T0">n</ta>
            <ta e="T2" id="Seg_1565" s="T1">conj</ta>
            <ta e="T3" id="Seg_1566" s="T2">n</ta>
            <ta e="T4" id="Seg_1567" s="T3">v</ta>
            <ta e="T5" id="Seg_1568" s="T4">n</ta>
            <ta e="T6" id="Seg_1569" s="T5">n</ta>
            <ta e="T7" id="Seg_1570" s="T6">n</ta>
            <ta e="T8" id="Seg_1571" s="T7">num</ta>
            <ta e="T9" id="Seg_1572" s="T8">n</ta>
            <ta e="T10" id="Seg_1573" s="T9">n</ta>
            <ta e="T11" id="Seg_1574" s="T10">v</ta>
            <ta e="T12" id="Seg_1575" s="T11">n</ta>
            <ta e="T13" id="Seg_1576" s="T12">adv</ta>
            <ta e="T14" id="Seg_1577" s="T13">v</ta>
            <ta e="T15" id="Seg_1578" s="T14">n</ta>
            <ta e="T16" id="Seg_1579" s="T15">adj</ta>
            <ta e="T17" id="Seg_1580" s="T16">n</ta>
            <ta e="T18" id="Seg_1581" s="T17">n</ta>
            <ta e="T19" id="Seg_1582" s="T18">pp</ta>
            <ta e="T20" id="Seg_1583" s="T19">pp</ta>
            <ta e="T21" id="Seg_1584" s="T20">n</ta>
            <ta e="T22" id="Seg_1585" s="T21">v</ta>
            <ta e="T23" id="Seg_1586" s="T22">n</ta>
            <ta e="T24" id="Seg_1587" s="T23">adv</ta>
            <ta e="T25" id="Seg_1588" s="T24">v</ta>
            <ta e="T26" id="Seg_1589" s="T25">adv</ta>
            <ta e="T28" id="Seg_1590" s="T26">v</ta>
            <ta e="T29" id="Seg_1591" s="T28">v</ta>
            <ta e="T30" id="Seg_1592" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_1593" s="T30">v</ta>
            <ta e="T33" id="Seg_1594" s="T31">v</ta>
            <ta e="T34" id="Seg_1595" s="T33">n</ta>
            <ta e="T35" id="Seg_1596" s="T34">adv</ta>
            <ta e="T36" id="Seg_1597" s="T35">v</ta>
            <ta e="T37" id="Seg_1598" s="T36">n</ta>
            <ta e="T38" id="Seg_1599" s="T37">v</ta>
            <ta e="T39" id="Seg_1600" s="T38">adv</ta>
            <ta e="T40" id="Seg_1601" s="T39">adv</ta>
            <ta e="T41" id="Seg_1602" s="T40">v</ta>
            <ta e="T42" id="Seg_1603" s="T41">n</ta>
            <ta e="T43" id="Seg_1604" s="T42">v</ta>
            <ta e="T44" id="Seg_1605" s="T43">v</ta>
            <ta e="T45" id="Seg_1606" s="T44">adv</ta>
            <ta e="T46" id="Seg_1607" s="T45">n</ta>
            <ta e="T47" id="Seg_1608" s="T46">n</ta>
            <ta e="T48" id="Seg_1609" s="T47">v</ta>
            <ta e="T49" id="Seg_1610" s="T48">adv</ta>
            <ta e="T50" id="Seg_1611" s="T49">v</ta>
            <ta e="T51" id="Seg_1612" s="T50">n</ta>
            <ta e="T52" id="Seg_1613" s="T51">v</ta>
            <ta e="T53" id="Seg_1614" s="T52">pers</ta>
            <ta e="T54" id="Seg_1615" s="T53">interrog</ta>
            <ta e="T55" id="Seg_1616" s="T54">n</ta>
            <ta e="T56" id="Seg_1617" s="T55">v</ta>
            <ta e="T57" id="Seg_1618" s="T56">pers</ta>
            <ta e="T58" id="Seg_1619" s="T57">n</ta>
            <ta e="T59" id="Seg_1620" s="T58">pp</ta>
            <ta e="T60" id="Seg_1621" s="T59">v</ta>
            <ta e="T61" id="Seg_1622" s="T60">adv</ta>
            <ta e="T62" id="Seg_1623" s="T61">adv</ta>
            <ta e="T63" id="Seg_1624" s="T62">v</ta>
            <ta e="T64" id="Seg_1625" s="T63">adv</ta>
            <ta e="T65" id="Seg_1626" s="T64">v</ta>
            <ta e="T66" id="Seg_1627" s="T65">n</ta>
            <ta e="T67" id="Seg_1628" s="T66">adj</ta>
            <ta e="T68" id="Seg_1629" s="T67">n</ta>
            <ta e="T69" id="Seg_1630" s="T68">v</ta>
            <ta e="T70" id="Seg_1631" s="T69">n</ta>
            <ta e="T71" id="Seg_1632" s="T70">adv</ta>
            <ta e="T72" id="Seg_1633" s="T71">adv</ta>
            <ta e="T73" id="Seg_1634" s="T72">n</ta>
            <ta e="T74" id="Seg_1635" s="T73">pp</ta>
            <ta e="T75" id="Seg_1636" s="T74">v</ta>
            <ta e="T76" id="Seg_1637" s="T75">adv</ta>
            <ta e="T77" id="Seg_1638" s="T76">v</ta>
            <ta e="T78" id="Seg_1639" s="T77">interj</ta>
            <ta e="T79" id="Seg_1640" s="T78">n</ta>
            <ta e="T80" id="Seg_1641" s="T79">adv</ta>
            <ta e="T81" id="Seg_1642" s="T80">adv</ta>
            <ta e="T82" id="Seg_1643" s="T81">n</ta>
            <ta e="T83" id="Seg_1644" s="T82">v</ta>
            <ta e="T84" id="Seg_1645" s="T83">n</ta>
            <ta e="T85" id="Seg_1646" s="T84">v</ta>
            <ta e="T86" id="Seg_1647" s="T85">n</ta>
            <ta e="T87" id="Seg_1648" s="T86">adv</ta>
            <ta e="T88" id="Seg_1649" s="T87">v</ta>
            <ta e="T90" id="Seg_1650" s="T88">v</ta>
            <ta e="T92" id="Seg_1651" s="T90">v</ta>
            <ta e="T93" id="Seg_1652" s="T92">n</ta>
            <ta e="T94" id="Seg_1653" s="T93">adv</ta>
            <ta e="T95" id="Seg_1654" s="T94">n</ta>
            <ta e="T96" id="Seg_1655" s="T95">v</ta>
            <ta e="T97" id="Seg_1656" s="T96">adv</ta>
            <ta e="T98" id="Seg_1657" s="T97">adv</ta>
            <ta e="T99" id="Seg_1658" s="T98">v</ta>
            <ta e="T100" id="Seg_1659" s="T99">v</ta>
            <ta e="T101" id="Seg_1660" s="T100">n</ta>
            <ta e="T102" id="Seg_1661" s="T101">n</ta>
            <ta e="T103" id="Seg_1662" s="T102">v</ta>
            <ta e="T104" id="Seg_1663" s="T103">v</ta>
            <ta e="T105" id="Seg_1664" s="T104">n</ta>
            <ta e="T106" id="Seg_1665" s="T105">interrog</ta>
            <ta e="T107" id="Seg_1666" s="T106">n</ta>
            <ta e="T108" id="Seg_1667" s="T107">n</ta>
            <ta e="T109" id="Seg_1668" s="T108">n</ta>
            <ta e="T110" id="Seg_1669" s="T109">v</ta>
            <ta e="T111" id="Seg_1670" s="T110">emphpro</ta>
            <ta e="T112" id="Seg_1671" s="T111">v</ta>
            <ta e="T113" id="Seg_1672" s="T112">n</ta>
            <ta e="T114" id="Seg_1673" s="T113">n</ta>
            <ta e="T115" id="Seg_1674" s="T114">adv</ta>
            <ta e="T116" id="Seg_1675" s="T115">v</ta>
            <ta e="T117" id="Seg_1676" s="T116">n</ta>
            <ta e="T118" id="Seg_1677" s="T117">adv</ta>
            <ta e="T119" id="Seg_1678" s="T118">v</ta>
            <ta e="T120" id="Seg_1679" s="T119">n</ta>
            <ta e="T121" id="Seg_1680" s="T120">n</ta>
            <ta e="T122" id="Seg_1681" s="T121">v</ta>
            <ta e="T123" id="Seg_1682" s="T122">adv</ta>
            <ta e="T124" id="Seg_1683" s="T123">adv</ta>
            <ta e="T125" id="Seg_1684" s="T124">v</ta>
            <ta e="T126" id="Seg_1685" s="T125">adv</ta>
            <ta e="T127" id="Seg_1686" s="T126">quant</ta>
            <ta e="T128" id="Seg_1687" s="T127">v</ta>
            <ta e="T129" id="Seg_1688" s="T128">n</ta>
            <ta e="T130" id="Seg_1689" s="T129">n</ta>
            <ta e="T131" id="Seg_1690" s="T130">pp</ta>
            <ta e="T132" id="Seg_1691" s="T131">preverb</ta>
            <ta e="T133" id="Seg_1692" s="T132">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T7" id="Seg_1693" s="T6">np.h:Th</ta>
            <ta e="T10" id="Seg_1694" s="T9">np.h:A</ta>
            <ta e="T12" id="Seg_1695" s="T11">np:G</ta>
            <ta e="T14" id="Seg_1696" s="T13">0.3.h:E</ta>
            <ta e="T15" id="Seg_1697" s="T14">np:L</ta>
            <ta e="T17" id="Seg_1698" s="T16">np:Th</ta>
            <ta e="T18" id="Seg_1699" s="T17">pp:L</ta>
            <ta e="T20" id="Seg_1700" s="T19">pp:L</ta>
            <ta e="T21" id="Seg_1701" s="T20">np.h:Th</ta>
            <ta e="T23" id="Seg_1702" s="T22">np.h:A</ta>
            <ta e="T28" id="Seg_1703" s="T26">0.2:A</ta>
            <ta e="T31" id="Seg_1704" s="T30">0.3:Th</ta>
            <ta e="T33" id="Seg_1705" s="T31">0.3:A</ta>
            <ta e="T34" id="Seg_1706" s="T33">np.h:A</ta>
            <ta e="T37" id="Seg_1707" s="T36">np:Th</ta>
            <ta e="T39" id="Seg_1708" s="T38">adv:Time</ta>
            <ta e="T40" id="Seg_1709" s="T39">adv:G</ta>
            <ta e="T41" id="Seg_1710" s="T40">0.3.h:A</ta>
            <ta e="T42" id="Seg_1711" s="T41">np.h:E 0.3.h:Poss</ta>
            <ta e="T44" id="Seg_1712" s="T43">0.3.h:A</ta>
            <ta e="T45" id="Seg_1713" s="T44">adv:Time</ta>
            <ta e="T46" id="Seg_1714" s="T45">np:Poss</ta>
            <ta e="T47" id="Seg_1715" s="T46">np.h:A</ta>
            <ta e="T49" id="Seg_1716" s="T48">adv:G</ta>
            <ta e="T50" id="Seg_1717" s="T49">0.3.h:A</ta>
            <ta e="T51" id="Seg_1718" s="T50">np.h:A 0.3.h:Poss</ta>
            <ta e="T53" id="Seg_1719" s="T52">pro.h:A</ta>
            <ta e="T54" id="Seg_1720" s="T53">pro:L</ta>
            <ta e="T55" id="Seg_1721" s="T54">np:Th</ta>
            <ta e="T57" id="Seg_1722" s="T56">pro.h:A</ta>
            <ta e="T58" id="Seg_1723" s="T57">pp:So</ta>
            <ta e="T60" id="Seg_1724" s="T59">0.3:Th</ta>
            <ta e="T61" id="Seg_1725" s="T60">adv:Time</ta>
            <ta e="T63" id="Seg_1726" s="T62">0.1.h:A</ta>
            <ta e="T64" id="Seg_1727" s="T63">adv:Time</ta>
            <ta e="T65" id="Seg_1728" s="T64">0.3.h:A</ta>
            <ta e="T66" id="Seg_1729" s="T65">np.h:A</ta>
            <ta e="T68" id="Seg_1730" s="T67">np:Th</ta>
            <ta e="T70" id="Seg_1731" s="T69">np.h:A</ta>
            <ta e="T73" id="Seg_1732" s="T72">pp:L</ta>
            <ta e="T77" id="Seg_1733" s="T76">0.3.h:A</ta>
            <ta e="T79" id="Seg_1734" s="T78">np.h:A</ta>
            <ta e="T82" id="Seg_1735" s="T81">np:P</ta>
            <ta e="T84" id="Seg_1736" s="T83">np.h:P</ta>
            <ta e="T86" id="Seg_1737" s="T85">np.h:A</ta>
            <ta e="T90" id="Seg_1738" s="T88">0.2:A</ta>
            <ta e="T92" id="Seg_1739" s="T90">0.3:A</ta>
            <ta e="T93" id="Seg_1740" s="T92">np.h:A</ta>
            <ta e="T95" id="Seg_1741" s="T94">np:Th</ta>
            <ta e="T97" id="Seg_1742" s="T96">adv:Time</ta>
            <ta e="T98" id="Seg_1743" s="T97">adv:G</ta>
            <ta e="T99" id="Seg_1744" s="T98">0.3.h:A</ta>
            <ta e="T100" id="Seg_1745" s="T99">0.3.h:A</ta>
            <ta e="T101" id="Seg_1746" s="T100">np.h:Poss</ta>
            <ta e="T102" id="Seg_1747" s="T101">np.h:A</ta>
            <ta e="T105" id="Seg_1748" s="T104">np.h:Th</ta>
            <ta e="T108" id="Seg_1749" s="T107">np.h:E 0.2.h:Poss</ta>
            <ta e="T109" id="Seg_1750" s="T108">np:Th</ta>
            <ta e="T111" id="Seg_1751" s="T110">pro.h:A</ta>
            <ta e="T113" id="Seg_1752" s="T112">np.h:Poss</ta>
            <ta e="T114" id="Seg_1753" s="T113">np.h:A</ta>
            <ta e="T117" id="Seg_1754" s="T116">np.h:Th</ta>
            <ta e="T120" id="Seg_1755" s="T119">np.h:P 0.2.h:Poss</ta>
            <ta e="T121" id="Seg_1756" s="T120">np.h:A</ta>
            <ta e="T123" id="Seg_1757" s="T122">adv:G</ta>
            <ta e="T125" id="Seg_1758" s="T124">0.3.h:A</ta>
            <ta e="T126" id="Seg_1759" s="T125">adv:Time</ta>
            <ta e="T128" id="Seg_1760" s="T127">0.3.h:A</ta>
            <ta e="T129" id="Seg_1761" s="T128">np:Poss</ta>
            <ta e="T130" id="Seg_1762" s="T129">pp:Path</ta>
            <ta e="T133" id="Seg_1763" s="T132">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_1764" s="T3">v:pred</ta>
            <ta e="T7" id="Seg_1765" s="T6">np.h:S</ta>
            <ta e="T10" id="Seg_1766" s="T9">np.h:S</ta>
            <ta e="T11" id="Seg_1767" s="T10">v:pred</ta>
            <ta e="T14" id="Seg_1768" s="T13">0.3.h:S v:pred</ta>
            <ta e="T17" id="Seg_1769" s="T16">np:O</ta>
            <ta e="T21" id="Seg_1770" s="T20">np.h:S</ta>
            <ta e="T22" id="Seg_1771" s="T21">v:pred</ta>
            <ta e="T23" id="Seg_1772" s="T22">np.h:S</ta>
            <ta e="T25" id="Seg_1773" s="T24">v:pred</ta>
            <ta e="T28" id="Seg_1774" s="T26">0.2:S v:pred</ta>
            <ta e="T29" id="Seg_1775" s="T28">s:adv</ta>
            <ta e="T31" id="Seg_1776" s="T30">0.3:S v:pred</ta>
            <ta e="T33" id="Seg_1777" s="T31">0.3:S v:pred</ta>
            <ta e="T34" id="Seg_1778" s="T33">np.h:S</ta>
            <ta e="T36" id="Seg_1779" s="T34">s:adv</ta>
            <ta e="T37" id="Seg_1780" s="T36">np:O</ta>
            <ta e="T38" id="Seg_1781" s="T37">v:pred</ta>
            <ta e="T41" id="Seg_1782" s="T40">0.3.h:S v:pred</ta>
            <ta e="T42" id="Seg_1783" s="T41">np.h:S</ta>
            <ta e="T43" id="Seg_1784" s="T42">v:pred</ta>
            <ta e="T44" id="Seg_1785" s="T43">0.3.h:S v:pred</ta>
            <ta e="T47" id="Seg_1786" s="T46">np.h:S</ta>
            <ta e="T48" id="Seg_1787" s="T47">v:pred</ta>
            <ta e="T50" id="Seg_1788" s="T49">0.3.h:S v:pred</ta>
            <ta e="T51" id="Seg_1789" s="T50">np.h:S</ta>
            <ta e="T52" id="Seg_1790" s="T51">v:pred</ta>
            <ta e="T53" id="Seg_1791" s="T52">pro.h:S</ta>
            <ta e="T55" id="Seg_1792" s="T54">np:O</ta>
            <ta e="T56" id="Seg_1793" s="T55">v:pred</ta>
            <ta e="T57" id="Seg_1794" s="T56">pro.h:S</ta>
            <ta e="T60" id="Seg_1795" s="T59">0.3:O v:pred</ta>
            <ta e="T63" id="Seg_1796" s="T62">0.1.h:S v:pred</ta>
            <ta e="T65" id="Seg_1797" s="T64">0.3.h:S v:pred</ta>
            <ta e="T66" id="Seg_1798" s="T65">np.h:S</ta>
            <ta e="T68" id="Seg_1799" s="T67">np:O</ta>
            <ta e="T69" id="Seg_1800" s="T68">v:pred</ta>
            <ta e="T70" id="Seg_1801" s="T69">np.h:S</ta>
            <ta e="T71" id="Seg_1802" s="T70">s:adv</ta>
            <ta e="T72" id="Seg_1803" s="T71">s:adv</ta>
            <ta e="T75" id="Seg_1804" s="T74">v:pred</ta>
            <ta e="T77" id="Seg_1805" s="T76">0.3.h:S v:pred</ta>
            <ta e="T79" id="Seg_1806" s="T78">np.h:S</ta>
            <ta e="T81" id="Seg_1807" s="T79">s:adv</ta>
            <ta e="T82" id="Seg_1808" s="T81">np:O</ta>
            <ta e="T83" id="Seg_1809" s="T82">v:pred</ta>
            <ta e="T84" id="Seg_1810" s="T83">np.h:S</ta>
            <ta e="T85" id="Seg_1811" s="T84">v:pred</ta>
            <ta e="T86" id="Seg_1812" s="T85">np.h:S</ta>
            <ta e="T88" id="Seg_1813" s="T87">v:pred</ta>
            <ta e="T90" id="Seg_1814" s="T88">0.2:S v:pred</ta>
            <ta e="T92" id="Seg_1815" s="T90">0.3:S v:pred</ta>
            <ta e="T93" id="Seg_1816" s="T92">np.h:S</ta>
            <ta e="T94" id="Seg_1817" s="T93">s:adv</ta>
            <ta e="T95" id="Seg_1818" s="T94">np:O</ta>
            <ta e="T96" id="Seg_1819" s="T95">v:pred</ta>
            <ta e="T99" id="Seg_1820" s="T98">0.3.h:S v:pred</ta>
            <ta e="T100" id="Seg_1821" s="T99">0.3.h:S v:pred</ta>
            <ta e="T102" id="Seg_1822" s="T101">np.h:S</ta>
            <ta e="T103" id="Seg_1823" s="T102">v:pred</ta>
            <ta e="T104" id="Seg_1824" s="T103">v:pred</ta>
            <ta e="T105" id="Seg_1825" s="T104">np.h:S</ta>
            <ta e="T106" id="Seg_1826" s="T105">pro:pred</ta>
            <ta e="T107" id="Seg_1827" s="T106">cop</ta>
            <ta e="T108" id="Seg_1828" s="T107">np.h:S</ta>
            <ta e="T109" id="Seg_1829" s="T108">np:O</ta>
            <ta e="T110" id="Seg_1830" s="T109">v:pred</ta>
            <ta e="T111" id="Seg_1831" s="T110">pro.h:S</ta>
            <ta e="T112" id="Seg_1832" s="T111">v:pred</ta>
            <ta e="T114" id="Seg_1833" s="T113">np.h:S</ta>
            <ta e="T116" id="Seg_1834" s="T115">v:pred</ta>
            <ta e="T117" id="Seg_1835" s="T116">np.h:S</ta>
            <ta e="T119" id="Seg_1836" s="T118">v:pred</ta>
            <ta e="T120" id="Seg_1837" s="T119">np.h:O</ta>
            <ta e="T121" id="Seg_1838" s="T120">np.h:S</ta>
            <ta e="T122" id="Seg_1839" s="T121">v:pred</ta>
            <ta e="T124" id="Seg_1840" s="T122">s:adv</ta>
            <ta e="T125" id="Seg_1841" s="T124">0.3.h:S v:pred</ta>
            <ta e="T128" id="Seg_1842" s="T127">0.3.h:S v:pred</ta>
            <ta e="T133" id="Seg_1843" s="T132">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T7" id="Seg_1844" s="T6">new</ta>
            <ta e="T10" id="Seg_1845" s="T9">accs-inf</ta>
            <ta e="T14" id="Seg_1846" s="T13">0.giv-active</ta>
            <ta e="T15" id="Seg_1847" s="T14">accs-gen</ta>
            <ta e="T17" id="Seg_1848" s="T16">new</ta>
            <ta e="T19" id="Seg_1849" s="T18">accs-sit</ta>
            <ta e="T20" id="Seg_1850" s="T19">accs-sit</ta>
            <ta e="T21" id="Seg_1851" s="T20">new</ta>
            <ta e="T23" id="Seg_1852" s="T22">giv-inactive</ta>
            <ta e="T25" id="Seg_1853" s="T24">quot-sp</ta>
            <ta e="T34" id="Seg_1854" s="T33">giv-inactive</ta>
            <ta e="T37" id="Seg_1855" s="T36">giv-inactive</ta>
            <ta e="T40" id="Seg_1856" s="T39">accs-sit</ta>
            <ta e="T41" id="Seg_1857" s="T40">0.giv-active</ta>
            <ta e="T42" id="Seg_1858" s="T41">accs-inf</ta>
            <ta e="T44" id="Seg_1859" s="T43">0.giv-active</ta>
            <ta e="T47" id="Seg_1860" s="T46">accs-inf</ta>
            <ta e="T49" id="Seg_1861" s="T48">accs-sit</ta>
            <ta e="T50" id="Seg_1862" s="T49">0.giv-active</ta>
            <ta e="T51" id="Seg_1863" s="T50">giv-inactive</ta>
            <ta e="T53" id="Seg_1864" s="T52">giv-inactive-Q</ta>
            <ta e="T55" id="Seg_1865" s="T54">giv-inactive-Q</ta>
            <ta e="T57" id="Seg_1866" s="T56">giv-active-Q</ta>
            <ta e="T58" id="Seg_1867" s="T57">giv-active-Q</ta>
            <ta e="T61" id="Seg_1868" s="T60">accs-sit-Q</ta>
            <ta e="T63" id="Seg_1869" s="T62">0.accs-aggr-Q</ta>
            <ta e="T64" id="Seg_1870" s="T63">accs-sit</ta>
            <ta e="T65" id="Seg_1871" s="T64">0.giv-active</ta>
            <ta e="T66" id="Seg_1872" s="T65">giv-inactive</ta>
            <ta e="T68" id="Seg_1873" s="T67">giv-inactive</ta>
            <ta e="T70" id="Seg_1874" s="T69">giv-inactive</ta>
            <ta e="T74" id="Seg_1875" s="T73">accs-sit</ta>
            <ta e="T77" id="Seg_1876" s="T76">0.giv-active quot-sp</ta>
            <ta e="T79" id="Seg_1877" s="T78">giv-inactive</ta>
            <ta e="T82" id="Seg_1878" s="T81">giv-active</ta>
            <ta e="T84" id="Seg_1879" s="T83">giv-active</ta>
            <ta e="T86" id="Seg_1880" s="T85">giv-inactive</ta>
            <ta e="T88" id="Seg_1881" s="T87">quot-sp</ta>
            <ta e="T93" id="Seg_1882" s="T92">giv-inactive</ta>
            <ta e="T95" id="Seg_1883" s="T94">giv-inactive</ta>
            <ta e="T98" id="Seg_1884" s="T97">accs-sit</ta>
            <ta e="T99" id="Seg_1885" s="T98">0.giv-active</ta>
            <ta e="T100" id="Seg_1886" s="T99">0.giv-active</ta>
            <ta e="T102" id="Seg_1887" s="T101">giv-inactive</ta>
            <ta e="T103" id="Seg_1888" s="T102">quot-sp</ta>
            <ta e="T104" id="Seg_1889" s="T103">quot-sp</ta>
            <ta e="T105" id="Seg_1890" s="T104">giv-inactive-Q</ta>
            <ta e="T108" id="Seg_1891" s="T107">giv-inactive-Q</ta>
            <ta e="T109" id="Seg_1892" s="T108">giv-inactive-Q</ta>
            <ta e="T111" id="Seg_1893" s="T110">giv-inactive</ta>
            <ta e="T114" id="Seg_1894" s="T113">giv-inactive</ta>
            <ta e="T117" id="Seg_1895" s="T116">giv-inactive-Q</ta>
            <ta e="T120" id="Seg_1896" s="T119">giv-active-Q</ta>
            <ta e="T121" id="Seg_1897" s="T120">giv-inactive-Q</ta>
            <ta e="T125" id="Seg_1898" s="T124">0.giv-inactive quot-sp</ta>
            <ta e="T128" id="Seg_1899" s="T127">accs-aggr</ta>
            <ta e="T130" id="Seg_1900" s="T129">new</ta>
            <ta e="T133" id="Seg_1901" s="T132">0.giv-active</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_1902" s="T0">Вороны и зайцы.</ta>
            <ta e="T7" id="Seg_1903" s="T3">Жили ворона [с воронятами] и зайчиха [с зайчатами].</ta>
            <ta e="T12" id="Seg_1904" s="T7">Однажды зайчиха отправилась в лес.</ta>
            <ta e="T17" id="Seg_1905" s="T12">И увидела в лесу нарту с салом.</ta>
            <ta e="T22" id="Seg_1906" s="T17">Рядом с нартами старик стоит.</ta>
            <ta e="T31" id="Seg_1907" s="T22">Зайчиха сказала: "Метель поднимись, подняв [снег], [так, чтобы ничего] не было видно".</ta>
            <ta e="T33" id="Seg_1908" s="T31">Метель поднялась.</ta>
            <ta e="T38" id="Seg_1909" s="T33">Зайчиха пошла [к нартам] и схватила кусок сала.</ta>
            <ta e="T41" id="Seg_1910" s="T38">Потом домой отправилась.</ta>
            <ta e="T43" id="Seg_1911" s="T41">Детки обрадовались.</ta>
            <ta e="T44" id="Seg_1912" s="T43">Стали есть.</ta>
            <ta e="T48" id="Seg_1913" s="T44">Потом воронёнок пришел.</ta>
            <ta e="T50" id="Seg_1914" s="T48">Домой побежал.</ta>
            <ta e="T52" id="Seg_1915" s="T50">Его мать пришла.</ta>
            <ta e="T56" id="Seg_1916" s="T52">"Вы откуда сало взяли?"</ta>
            <ta e="T60" id="Seg_1917" s="T56">"Я у старика утащила".</ta>
            <ta e="T63" id="Seg_1918" s="T60">"Завтра вместе пойдем".</ta>
            <ta e="T65" id="Seg_1919" s="T63">Утром пошли [вдвоём].</ta>
            <ta e="T69" id="Seg_1920" s="T65">Старик вёз нарту с салом.</ta>
            <ta e="T75" id="Seg_1921" s="T69">Ворониха, подбежав (подлетев), села на нарты.</ta>
            <ta e="T78" id="Seg_1922" s="T75">Крикнула: "Кр-кр!"</ta>
            <ta e="T83" id="Seg_1923" s="T78">Старик обернулся и ударил ворониху.</ta>
            <ta e="T85" id="Seg_1924" s="T83">Ворониха умерла.</ta>
            <ta e="T90" id="Seg_1925" s="T85">Зайчиха сказала: "Метель, поднимись".</ta>
            <ta e="T92" id="Seg_1926" s="T90">Метель поднялась.</ta>
            <ta e="T96" id="Seg_1927" s="T92">Зайчиха, подбежав, забрала кусок сала.</ta>
            <ta e="T99" id="Seg_1928" s="T96">Потом домой пошла.</ta>
            <ta e="T100" id="Seg_1929" s="T99">Пришла.</ta>
            <ta e="T107" id="Seg_1930" s="T100">Воронёнок [с другими воронятами] спрашивают: "Где мама?"</ta>
            <ta e="T110" id="Seg_1931" s="T107">"Твоя мама кусок сала не может [донести]".</ta>
            <ta e="T112" id="Seg_1932" s="T110">Сами стали есть.</ta>
            <ta e="T116" id="Seg_1933" s="T112">Вороненок опять пришел.</ta>
            <ta e="T119" id="Seg_1934" s="T116">"Мамы еще нет".</ta>
            <ta e="T122" id="Seg_1935" s="T119">"Твою маму старичок убил".</ta>
            <ta e="T125" id="Seg_1936" s="T122">Он домой пошел и рассказал про это.</ta>
            <ta e="T128" id="Seg_1937" s="T125">И все заплакали.</ta>
            <ta e="T133" id="Seg_1938" s="T128">В трубу выскочили. [?]</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_1939" s="T0">Ravens and hares.</ta>
            <ta e="T7" id="Seg_1940" s="T3">There lived a raven hen and a doe hare with their children.</ta>
            <ta e="T12" id="Seg_1941" s="T7">Once the hare left for the forest.</ta>
            <ta e="T17" id="Seg_1942" s="T12">In the forest she saw a sledge with fat in it.</ta>
            <ta e="T22" id="Seg_1943" s="T17">An old man was standing near the sledge.</ta>
            <ta e="T31" id="Seg_1944" s="T22">The doe hare said so: "Snowstorm, come, raise the snow, so that nothing can be seen".</ta>
            <ta e="T33" id="Seg_1945" s="T31">The snowstorm came.</ta>
            <ta e="T38" id="Seg_1946" s="T33">The hare went there [to the sledge] and grabbed the fat.</ta>
            <ta e="T41" id="Seg_1947" s="T38">Then she went back home.</ta>
            <ta e="T43" id="Seg_1948" s="T41">Her children were happy.</ta>
            <ta e="T44" id="Seg_1949" s="T43">They began to eat it.</ta>
            <ta e="T48" id="Seg_1950" s="T44">Then a raven fledgeling came along.</ta>
            <ta e="T50" id="Seg_1951" s="T48">He ran back home.</ta>
            <ta e="T52" id="Seg_1952" s="T50">His mother came.</ta>
            <ta e="T56" id="Seg_1953" s="T52">"Where did you find the fat?"</ta>
            <ta e="T60" id="Seg_1954" s="T56">"I stole it from the old man".</ta>
            <ta e="T63" id="Seg_1955" s="T60">"Tomorrow we'll go there together".</ta>
            <ta e="T65" id="Seg_1956" s="T63">In the morning they both went off.</ta>
            <ta e="T69" id="Seg_1957" s="T65">The old man was driving the sledge with fat in it.</ta>
            <ta e="T75" id="Seg_1958" s="T69">The raven flew up to the sledge and sat on it.</ta>
            <ta e="T78" id="Seg_1959" s="T75">She cried: "Kraa-kraa!"</ta>
            <ta e="T83" id="Seg_1960" s="T78">The old man turned back and hit the raven.</ta>
            <ta e="T85" id="Seg_1961" s="T83">The raven died.</ta>
            <ta e="T90" id="Seg_1962" s="T85">The hare said: "Snowstorm, come".</ta>
            <ta e="T92" id="Seg_1963" s="T90">The snowstorm came.</ta>
            <ta e="T96" id="Seg_1964" s="T92">The hare came and grabbed a piece of fat.</ta>
            <ta e="T99" id="Seg_1965" s="T96">Then she went home.</ta>
            <ta e="T100" id="Seg_1966" s="T99">She came.</ta>
            <ta e="T107" id="Seg_1967" s="T100">[That] raven fledgeling [and others] ask: "Where is mum?"</ta>
            <ta e="T110" id="Seg_1968" s="T107">"Your mother cannot bring the fat".</ta>
            <ta e="T112" id="Seg_1969" s="T110">They themselves started to eat.</ta>
            <ta e="T116" id="Seg_1970" s="T112">The raven fledgeling came again.</ta>
            <ta e="T119" id="Seg_1971" s="T116">"Mum is still not there".</ta>
            <ta e="T122" id="Seg_1972" s="T119">"The old man killed your mother".</ta>
            <ta e="T125" id="Seg_1973" s="T122">He went home and told about that.</ta>
            <ta e="T128" id="Seg_1974" s="T125">Then they all began to cry.</ta>
            <ta e="T133" id="Seg_1975" s="T128">They popped out through the chimney hole. [?]</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_1976" s="T0">Raben und Hasen.</ta>
            <ta e="T7" id="Seg_1977" s="T3">Es lebten eine Rabenmutter mit Rabenjungen und eine Häsin mit Häschen.</ta>
            <ta e="T12" id="Seg_1978" s="T7">Einmal ging die Häsin in den Wald.</ta>
            <ta e="T17" id="Seg_1979" s="T12">Im Wald sah sie einen Schlitten mit Fett darin.</ta>
            <ta e="T22" id="Seg_1980" s="T17">Neben dem Schlitten stand ein alter Mann.</ta>
            <ta e="T31" id="Seg_1981" s="T22">Die Häsin sagte: "Schneesturm, komm, wirbel den Schnee so auf, dass nichts zu sehen ist."</ta>
            <ta e="T33" id="Seg_1982" s="T31">Der Schneesturm kam auf.</ta>
            <ta e="T38" id="Seg_1983" s="T33">Die Häsin ging dorthin und schnappte sich das Fett.</ta>
            <ta e="T41" id="Seg_1984" s="T38">Dann ging sie nach Hause.</ta>
            <ta e="T43" id="Seg_1985" s="T41">Ihre Häschen freuten sich.</ta>
            <ta e="T44" id="Seg_1986" s="T43">Sie fingen an es zu essen.</ta>
            <ta e="T48" id="Seg_1987" s="T44">Dann kam ein Rabenjunge vorbei.</ta>
            <ta e="T50" id="Seg_1988" s="T48">Es lief zurück nach Hause.</ta>
            <ta e="T52" id="Seg_1989" s="T50">Seine Mutter kam.</ta>
            <ta e="T56" id="Seg_1990" s="T52">"Wo habt ihr das Fett gefunden?"</ta>
            <ta e="T60" id="Seg_1991" s="T56">"Ich habe es beim Alten gestohlen."</ta>
            <ta e="T63" id="Seg_1992" s="T60">"Morgen gehen wir zusammen".</ta>
            <ta e="T65" id="Seg_1993" s="T63">Am nächsten Morgen gingen sie los.</ta>
            <ta e="T69" id="Seg_1994" s="T65">Der Alte fuhr auf dem Schlitten mit Fett darin.</ta>
            <ta e="T75" id="Seg_1995" s="T69">Die Rabenmutter flog hoch und setzte sich auf den Schlitten.</ta>
            <ta e="T78" id="Seg_1996" s="T75">Sie schrie: "Kraa-Kraa!"</ta>
            <ta e="T83" id="Seg_1997" s="T78">Der Alte drehte sich um und schlug die Rabenmutter.</ta>
            <ta e="T85" id="Seg_1998" s="T83">Die Rabenmutter starb.</ta>
            <ta e="T90" id="Seg_1999" s="T85">Die Häsin sagte: "Schneesturm, komm."</ta>
            <ta e="T92" id="Seg_2000" s="T90">Der Schneesturm kam auf.</ta>
            <ta e="T96" id="Seg_2001" s="T92">Die Häsin kam angelaufen und schnappte sich das Fett.</ta>
            <ta e="T99" id="Seg_2002" s="T96">Danach ging sie heim.</ta>
            <ta e="T100" id="Seg_2003" s="T99">Sie kam an.</ta>
            <ta e="T107" id="Seg_2004" s="T100">Das Rabenjunge und die anderen Rabenjungen fragten: "Wo ist Mama?"</ta>
            <ta e="T110" id="Seg_2005" s="T107">"Deine Mutter kann das Fett nicht bringen."</ta>
            <ta e="T112" id="Seg_2006" s="T110">Sie selbst fingen an zu essen.</ta>
            <ta e="T116" id="Seg_2007" s="T112">Das Rabenjunge kam wieder.</ta>
            <ta e="T119" id="Seg_2008" s="T116">"Mama ist immer noch nicht da."</ta>
            <ta e="T122" id="Seg_2009" s="T119">"Der Alte hat deine Mutter getötet."</ta>
            <ta e="T125" id="Seg_2010" s="T122">Es ging nach Hause und erzählte es.</ta>
            <ta e="T128" id="Seg_2011" s="T125">Dann haben sie alle angefangen zu weinen.</ta>
            <ta e="T133" id="Seg_2012" s="T128">Sie sprangen durch das Rauchloch hinaus. [?]</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_2013" s="T0">вороны и зайцы.</ta>
            <ta e="T7" id="Seg_2014" s="T3">жили вороны с воронятами зайцы с зайчатами (вороны и зайцы) (семейка, семья).</ta>
            <ta e="T12" id="Seg_2015" s="T7">однажды зайчиха пошла в лес.</ta>
            <ta e="T17" id="Seg_2016" s="T12">(значит) и нашла в лесу с салом нарту.</ta>
            <ta e="T22" id="Seg_2017" s="T17">нарты около старик стоит.</ta>
            <ta e="T31" id="Seg_2018" s="T22">зайчиха так сказала пусть поднимется метель (снего большой, буран) через несколько шагов = близко не видно [дажне не видно]</ta>
            <ta e="T33" id="Seg_2019" s="T31">метель поднялась.</ta>
            <ta e="T38" id="Seg_2020" s="T33">зайчиха пошла кусок сала схватила.</ta>
            <ta e="T41" id="Seg_2021" s="T38">и (значит) домой пошла.</ta>
            <ta e="T43" id="Seg_2022" s="T41">детишки обрадовались.</ta>
            <ta e="T44" id="Seg_2023" s="T43">сели есть.</ta>
            <ta e="T48" id="Seg_2024" s="T44">и вороненок пришел.</ta>
            <ta e="T50" id="Seg_2025" s="T48">домой побежал.</ta>
            <ta e="T52" id="Seg_2026" s="T50">мать пришла.</ta>
            <ta e="T56" id="Seg_2027" s="T52">Вы откуда сало взяли</ta>
            <ta e="T60" id="Seg_2028" s="T56">я у старика утащила.</ta>
            <ta e="T63" id="Seg_2029" s="T60">завтра вместе пойдем.</ta>
            <ta e="T65" id="Seg_2030" s="T63">утром пошли вдвоем.</ta>
            <ta e="T69" id="Seg_2031" s="T65">старик с салом нарту вез.</ta>
            <ta e="T75" id="Seg_2032" s="T69">ворона подлетела на нарту села</ta>
            <ta e="T78" id="Seg_2033" s="T75">так крикнула: кр-кр!</ta>
            <ta e="T83" id="Seg_2034" s="T78">старик обернулся ворону ударил.</ta>
            <ta e="T85" id="Seg_2035" s="T83">ворона умерла.</ta>
            <ta e="T90" id="Seg_2036" s="T85">зайчиха так сказала: пусть поднимется метель.</ta>
            <ta e="T92" id="Seg_2037" s="T90">поднялась метель.</ta>
            <ta e="T96" id="Seg_2038" s="T92">зайчиха подбежав кусок сала схватила.</ta>
            <ta e="T99" id="Seg_2039" s="T96">и домой пошла.</ta>
            <ta e="T100" id="Seg_2040" s="T99">пришла.</ta>
            <ta e="T107" id="Seg_2041" s="T100">вороненок спрашивает мама где?</ta>
            <ta e="T110" id="Seg_2042" s="T107">мать (твоя) кусок сала не может принести.</ta>
            <ta e="T112" id="Seg_2043" s="T110">сами сели есть.</ta>
            <ta e="T116" id="Seg_2044" s="T112">вороненок опять [и] пришел.</ta>
            <ta e="T119" id="Seg_2045" s="T116">мамы еще нет.</ta>
            <ta e="T122" id="Seg_2046" s="T119">мать твою старичок убил.</ta>
            <ta e="T125" id="Seg_2047" s="T122">домой пошел и сказал.</ta>
            <ta e="T128" id="Seg_2048" s="T125">и все заплакали.</ta>
            <ta e="T133" id="Seg_2049" s="T128">в трубу упали. [?]</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T7" id="Seg_2050" s="T3">[AAV:] lit. "raven's and hare's families (mothers with their children)"</ta>
            <ta e="T12" id="Seg_2051" s="T7">[SOV:] "ukkɨr (tot) čʼontoːqɨn" - "once upon a time".</ta>
            <ta e="T22" id="Seg_2052" s="T17">[OSV:] The form "qanəqɨn" has been edited into "qanəqqɨn".</ta>
            <ta e="T133" id="Seg_2053" s="T128">[AAV:] unclear sentence</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T90" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
