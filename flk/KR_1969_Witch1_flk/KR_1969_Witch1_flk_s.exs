<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KR_1969_Witch_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KR_1969_Witch1_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">173</ud-information>
            <ud-information attribute-name="# HIAT:w">122</ud-information>
            <ud-information attribute-name="# e">122</ud-information>
            <ud-information attribute-name="# HIAT:u">35</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KR">
            <abbreviation>KR</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T571" />
         <tli id="T572" />
         <tli id="T573" />
         <tli id="T574" />
         <tli id="T575" />
         <tli id="T576" />
         <tli id="T577" />
         <tli id="T578" />
         <tli id="T579" />
         <tli id="T580" />
         <tli id="T581" />
         <tli id="T582" />
         <tli id="T583" />
         <tli id="T584" />
         <tli id="T585" />
         <tli id="T586" />
         <tli id="T587" />
         <tli id="T588" />
         <tli id="T589" />
         <tli id="T590" />
         <tli id="T591" />
         <tli id="T592" />
         <tli id="T593" />
         <tli id="T594" />
         <tli id="T595" />
         <tli id="T596" />
         <tli id="T597" />
         <tli id="T598" />
         <tli id="T599" />
         <tli id="T600" />
         <tli id="T601" />
         <tli id="T602" />
         <tli id="T603" />
         <tli id="T604" />
         <tli id="T605" />
         <tli id="T606" />
         <tli id="T607" />
         <tli id="T608" />
         <tli id="T609" />
         <tli id="T610" />
         <tli id="T611" />
         <tli id="T612" />
         <tli id="T613" />
         <tli id="T614" />
         <tli id="T615" />
         <tli id="T616" />
         <tli id="T617" />
         <tli id="T618" />
         <tli id="T619" />
         <tli id="T620" />
         <tli id="T621" />
         <tli id="T622" />
         <tli id="T623" />
         <tli id="T624" />
         <tli id="T625" />
         <tli id="T626" />
         <tli id="T627" />
         <tli id="T628" />
         <tli id="T629" />
         <tli id="T630" />
         <tli id="T631" />
         <tli id="T632" />
         <tli id="T633" />
         <tli id="T634" />
         <tli id="T635" />
         <tli id="T636" />
         <tli id="T637" />
         <tli id="T638" />
         <tli id="T639" />
         <tli id="T640" />
         <tli id="T641" />
         <tli id="T642" />
         <tli id="T643" />
         <tli id="T644" />
         <tli id="T645" />
         <tli id="T646" />
         <tli id="T647" />
         <tli id="T648" />
         <tli id="T649" />
         <tli id="T650" />
         <tli id="T651" />
         <tli id="T652" />
         <tli id="T653" />
         <tli id="T654" />
         <tli id="T655" />
         <tli id="T656" />
         <tli id="T657" />
         <tli id="T658" />
         <tli id="T659" />
         <tli id="T660" />
         <tli id="T661" />
         <tli id="T662" />
         <tli id="T663" />
         <tli id="T664" />
         <tli id="T665" />
         <tli id="T666" />
         <tli id="T667" />
         <tli id="T668" />
         <tli id="T669" />
         <tli id="T670" />
         <tli id="T671" />
         <tli id="T672" />
         <tli id="T673" />
         <tli id="T674" />
         <tli id="T675" />
         <tli id="T676" />
         <tli id="T677" />
         <tli id="T678" />
         <tli id="T679" />
         <tli id="T680" />
         <tli id="T681" />
         <tli id="T682" />
         <tli id="T683" />
         <tli id="T684" />
         <tli id="T685" />
         <tli id="T686" />
         <tli id="T687" />
         <tli id="T688" />
         <tli id="T689" />
         <tli id="T690" />
         <tli id="T691" />
         <tli id="T692" />
         <tli id="T693" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KR"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T693" id="Seg_0" n="sc" s="T571">
               <ts e="T572" id="Seg_2" n="HIAT:u" s="T571">
                  <ts e="T572" id="Seg_4" n="HIAT:w" s="T571">Pɨrniː</ts>
                  <nts id="Seg_5" n="HIAT:ip">.</nts>
                  <nts id="Seg_6" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T575" id="Seg_8" n="HIAT:u" s="T572">
                  <ts e="T573" id="Seg_10" n="HIAT:w" s="T572">Ilimpɔːqı</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_13" n="HIAT:w" s="T573">ɨralʼ</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_16" n="HIAT:w" s="T574">mɨqaːqi</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T578" id="Seg_20" n="HIAT:u" s="T575">
                  <ts e="T576" id="Seg_22" n="HIAT:w" s="T575">Iːjati</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_25" n="HIAT:w" s="T576">ɛːppɨmmɨntɨ</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_28" n="HIAT:w" s="T577">imantɨsa</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T584" id="Seg_32" n="HIAT:u" s="T578">
                  <ts e="T579" id="Seg_34" n="HIAT:w" s="T578">Ijatɨ</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_37" n="HIAT:w" s="T579">pona</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_40" n="HIAT:w" s="T580">tarɨlʼa</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_43" n="HIAT:w" s="T581">üːnɨqıtɨ</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_46" n="HIAT:w" s="T582">suːtɨwsä</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_49" n="HIAT:w" s="T583">natqɨlqolammɨntɨtɨ</ts>
                  <nts id="Seg_50" n="HIAT:ip">.</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T591" id="Seg_53" n="HIAT:u" s="T584">
                  <ts e="T585" id="Seg_55" n="HIAT:w" s="T584">Ɨrra</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_58" n="HIAT:w" s="T585">nıː</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_61" n="HIAT:w" s="T586">kəttɨkɨt</ts>
                  <nts id="Seg_62" n="HIAT:ip">:</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_65" n="HIAT:w" s="T587">pɨrni</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_68" n="HIAT:w" s="T588">täntɨ</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_71" n="HIAT:w" s="T589">na</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_74" n="HIAT:w" s="T590">tüːnnɨntɨ</ts>
                  <nts id="Seg_75" n="HIAT:ip">.</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T594" id="Seg_78" n="HIAT:u" s="T591">
                  <ts e="T592" id="Seg_80" n="HIAT:w" s="T591">Ijatɨ</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_83" n="HIAT:w" s="T592">jen</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_86" n="HIAT:w" s="T593">ɛsɨmpa</ts>
                  <nts id="Seg_87" n="HIAT:ip">.</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T596" id="Seg_90" n="HIAT:u" s="T594">
                  <ts e="T595" id="Seg_92" n="HIAT:w" s="T594">Meːl</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_95" n="HIAT:w" s="T595">nantɨnɨtɨ</ts>
                  <nts id="Seg_96" n="HIAT:ip">.</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T600" id="Seg_99" n="HIAT:u" s="T596">
                  <ts e="T597" id="Seg_101" n="HIAT:w" s="T596">Ɔːtaqıtɨ</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_104" n="HIAT:w" s="T597">orqɨlʼpat</ts>
                  <nts id="Seg_105" n="HIAT:ip">,</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_108" n="HIAT:w" s="T598">qaqlɨ</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_111" n="HIAT:w" s="T599">saralpintɨtɨ</ts>
                  <nts id="Seg_112" n="HIAT:ip">.</nts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T601" id="Seg_115" n="HIAT:u" s="T600">
                  <ts e="T601" id="Seg_117" n="HIAT:w" s="T600">Qənpa</ts>
                  <nts id="Seg_118" n="HIAT:ip">.</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T602" id="Seg_121" n="HIAT:u" s="T601">
                  <ts e="T602" id="Seg_123" n="HIAT:w" s="T601">Laqolʼtɨmpa</ts>
                  <nts id="Seg_124" n="HIAT:ip">.</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T604" id="Seg_127" n="HIAT:u" s="T602">
                  <ts e="T603" id="Seg_129" n="HIAT:w" s="T602">Pɨrni</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_132" n="HIAT:w" s="T603">pɔːktɨmmäntɨ</ts>
                  <nts id="Seg_133" n="HIAT:ip">.</nts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T608" id="Seg_136" n="HIAT:u" s="T604">
                  <ts e="T605" id="Seg_138" n="HIAT:w" s="T604">Qumɨnɨk</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_141" n="HIAT:w" s="T605">tüːlʼa</ts>
                  <nts id="Seg_142" n="HIAT:ip">,</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_145" n="HIAT:w" s="T606">wənnɨmtɨ</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_148" n="HIAT:w" s="T607">qatɨtqolampatɨ</ts>
                  <nts id="Seg_149" n="HIAT:ip">.</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T613" id="Seg_152" n="HIAT:u" s="T608">
                  <ts e="T609" id="Seg_154" n="HIAT:w" s="T608">Ontɨ</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_157" n="HIAT:w" s="T609">nıː</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_160" n="HIAT:w" s="T610">čʼiːta</ts>
                  <nts id="Seg_161" n="HIAT:ip">:</nts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_164" n="HIAT:w" s="T611">ɨrramɨ</ts>
                  <nts id="Seg_165" n="HIAT:ip">,</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_168" n="HIAT:w" s="T612">ɨrramɨ</ts>
                  <nts id="Seg_169" n="HIAT:ip">!</nts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T616" id="Seg_172" n="HIAT:u" s="T613">
                  <ts e="T614" id="Seg_174" n="HIAT:w" s="T613">Qumɨn</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_177" n="HIAT:w" s="T614">qaqlɨntɨ</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_180" n="HIAT:w" s="T615">tilčʼimpa</ts>
                  <nts id="Seg_181" n="HIAT:ip">.</nts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T618" id="Seg_184" n="HIAT:u" s="T616">
                  <ts e="T617" id="Seg_186" n="HIAT:w" s="T616">Moqɨnä</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_189" n="HIAT:w" s="T617">tüːmpoːqı</ts>
                  <nts id="Seg_190" n="HIAT:ip">.</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T620" id="Seg_193" n="HIAT:u" s="T618">
                  <ts e="T619" id="Seg_195" n="HIAT:w" s="T618">Mɔːttɨ</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_198" n="HIAT:w" s="T619">šeːrraıːmpa</ts>
                  <nts id="Seg_199" n="HIAT:ip">.</nts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T626" id="Seg_202" n="HIAT:u" s="T620">
                  <ts e="T621" id="Seg_204" n="HIAT:w" s="T620">Ɨrran</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_207" n="HIAT:w" s="T621">wəntɨ</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_210" n="HIAT:w" s="T622">nıː</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_213" n="HIAT:w" s="T623">qattɨnɨtɨ</ts>
                  <nts id="Seg_214" n="HIAT:ip">:</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_217" n="HIAT:w" s="T624">ilʼčʼamɨ</ts>
                  <nts id="Seg_218" n="HIAT:ip">,</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_221" n="HIAT:w" s="T625">ilʼčʼamɨ</ts>
                  <nts id="Seg_222" n="HIAT:ip">!</nts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T631" id="Seg_225" n="HIAT:u" s="T626">
                  <ts e="T627" id="Seg_227" n="HIAT:w" s="T626">Imaqotan</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_230" n="HIAT:w" s="T627">wəntɨ</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_233" n="HIAT:w" s="T628">qattɨnɨtɨ</ts>
                  <nts id="Seg_234" n="HIAT:ip">:</nts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_237" n="HIAT:w" s="T629">imɨlʼamɨ</ts>
                  <nts id="Seg_238" n="HIAT:ip">,</nts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_241" n="HIAT:w" s="T630">imɨlʼamɨ</ts>
                  <nts id="Seg_242" n="HIAT:ip">!</nts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T635" id="Seg_245" n="HIAT:u" s="T631">
                  <ts e="T632" id="Seg_247" n="HIAT:w" s="T631">Qumɨt</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_250" n="HIAT:w" s="T632">pɨrnisa</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_253" n="HIAT:w" s="T633">nannɨr</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_256" n="HIAT:w" s="T634">munʼilʼmotpɔːtɨn</ts>
                  <nts id="Seg_257" n="HIAT:ip">.</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T641" id="Seg_260" n="HIAT:u" s="T635">
                  <ts e="T636" id="Seg_262" n="HIAT:w" s="T635">Takkarma</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_265" n="HIAT:w" s="T636">meːtɨntɔːtɨn</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_268" n="HIAT:w" s="T637">kuttär</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_271" n="HIAT:w" s="T638">pɨrniw</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_274" n="HIAT:w" s="T639">ılla</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_277" n="HIAT:w" s="T640">qättɔːwɨn</ts>
                  <nts id="Seg_278" n="HIAT:ip">.</nts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T645" id="Seg_281" n="HIAT:u" s="T641">
                  <ts e="T642" id="Seg_283" n="HIAT:w" s="T641">Nıː</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_286" n="HIAT:w" s="T642">pinpɔːtɨn</ts>
                  <nts id="Seg_287" n="HIAT:ip">:</nts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_290" n="HIAT:w" s="T643">tüːntɨ</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_293" n="HIAT:w" s="T644">pärqɨltɛntɔːtɨn</ts>
                  <nts id="Seg_294" n="HIAT:ip">.</nts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T651" id="Seg_297" n="HIAT:u" s="T645">
                  <ts e="T646" id="Seg_299" n="HIAT:w" s="T645">Imaqota</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_302" n="HIAT:w" s="T646">wərɨqa</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_305" n="HIAT:w" s="T647">mɨtɨnɨlʼ</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_308" n="HIAT:w" s="T648">čʼi</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_311" n="HIAT:w" s="T649">karra</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_314" n="HIAT:w" s="T650">ɨːtɨmmɨntɨtɨ</ts>
                  <nts id="Seg_315" n="HIAT:ip">.</nts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T652" id="Seg_318" n="HIAT:u" s="T651">
                  <ts e="T652" id="Seg_320" n="HIAT:w" s="T651">Mušurɨqolampatɨ</ts>
                  <nts id="Seg_321" n="HIAT:ip">.</nts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T655" id="Seg_324" n="HIAT:u" s="T652">
                  <ts e="T653" id="Seg_326" n="HIAT:w" s="T652">Onʼa</ts>
                  <nts id="Seg_327" n="HIAT:ip">,</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_330" n="HIAT:w" s="T653">čʼi</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_333" n="HIAT:w" s="T654">korpɨtɨ</ts>
                  <nts id="Seg_334" n="HIAT:ip">!</nts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T659" id="Seg_337" n="HIAT:u" s="T655">
                  <ts e="T656" id="Seg_339" n="HIAT:w" s="T655">Toːnna</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_342" n="HIAT:w" s="T656">ima</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_345" n="HIAT:w" s="T657">meːlte</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_348" n="HIAT:w" s="T658">ɔːmta</ts>
                  <nts id="Seg_349" n="HIAT:ip">.</nts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T662" id="Seg_352" n="HIAT:u" s="T659">
                  <ts e="T660" id="Seg_354" n="HIAT:w" s="T659">Pɨrniː</ts>
                  <nts id="Seg_355" n="HIAT:ip">,</nts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_358" n="HIAT:w" s="T660">orɨm</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_361" n="HIAT:w" s="T661">ɛːŋantɨ</ts>
                  <nts id="Seg_362" n="HIAT:ip">.</nts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T664" id="Seg_365" n="HIAT:u" s="T662">
                  <ts e="T663" id="Seg_367" n="HIAT:w" s="T662">Čʼi</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_370" n="HIAT:w" s="T663">korpɨtɨ</ts>
                  <nts id="Seg_371" n="HIAT:ip">.</nts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T666" id="Seg_374" n="HIAT:u" s="T664">
                  <ts e="T665" id="Seg_376" n="HIAT:w" s="T664">Pɨrniː</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_379" n="HIAT:w" s="T665">paktɨmpa</ts>
                  <nts id="Seg_380" n="HIAT:ip">.</nts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T672" id="Seg_383" n="HIAT:u" s="T666">
                  <ts e="T667" id="Seg_385" n="HIAT:w" s="T666">Čʼiw</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_388" n="HIAT:w" s="T667">čʼaw</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_391" n="HIAT:w" s="T668">korpɨtqantantɨmpatɨ</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_393" n="HIAT:ip">(</nts>
                  <ts e="T670" id="Seg_395" n="HIAT:w" s="T669">korpɨtqolampatɨ</ts>
                  <nts id="Seg_396" n="HIAT:ip">)</nts>
                  <nts id="Seg_397" n="HIAT:ip">,</nts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_400" n="HIAT:w" s="T670">kuntok</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_403" n="HIAT:w" s="T671">ɛːŋa</ts>
                  <nts id="Seg_404" n="HIAT:ip">.</nts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T676" id="Seg_407" n="HIAT:u" s="T672">
                  <ts e="T673" id="Seg_409" n="HIAT:w" s="T672">Tüː</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_412" n="HIAT:w" s="T673">tä</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_415" n="HIAT:w" s="T674">šım</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_418" n="HIAT:w" s="T675">amtɨ</ts>
                  <nts id="Seg_419" n="HIAT:ip">.</nts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T679" id="Seg_422" n="HIAT:u" s="T676">
                  <ts e="T677" id="Seg_424" n="HIAT:w" s="T676">Ɨrra</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_427" n="HIAT:w" s="T677">nıː</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_430" n="HIAT:w" s="T678">kəttakɨtɨ</ts>
                  <nts id="Seg_431" n="HIAT:ip">.</nts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T680" id="Seg_434" n="HIAT:u" s="T679">
                  <ts e="T680" id="Seg_436" n="HIAT:w" s="T679">Qoralʼok</ts>
                  <nts id="Seg_437" n="HIAT:ip">.</nts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T686" id="Seg_440" n="HIAT:u" s="T680">
                  <ts e="T681" id="Seg_442" n="HIAT:w" s="T680">Pɨrniː</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_445" n="HIAT:w" s="T681">karra</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_448" n="HIAT:w" s="T682">čʼam</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_451" n="HIAT:w" s="T683">čʼıqɨlʼtempa</ts>
                  <nts id="Seg_452" n="HIAT:ip">,</nts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_455" n="HIAT:w" s="T684">tüːntɨ</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_458" n="HIAT:w" s="T685">alʼčʼimpa</ts>
                  <nts id="Seg_459" n="HIAT:ip">.</nts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T689" id="Seg_462" n="HIAT:u" s="T686">
                  <ts e="T687" id="Seg_464" n="HIAT:w" s="T686">Alʼčʼɨwtäːqɨntɨ</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_467" n="HIAT:w" s="T687">nıː</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_470" n="HIAT:w" s="T688">kɛtɨmpatɨ</ts>
                  <nts id="Seg_471" n="HIAT:ip">.</nts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T693" id="Seg_474" n="HIAT:u" s="T689">
                  <ts e="T690" id="Seg_476" n="HIAT:w" s="T689">Tɨːnɨ</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_479" n="HIAT:w" s="T690">jenna</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_482" n="HIAT:w" s="T691">qumɨn</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_485" n="HIAT:w" s="T692">quqɨlɨqilɔːtɨn</ts>
                  <nts id="Seg_486" n="HIAT:ip">.</nts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T693" id="Seg_488" n="sc" s="T571">
               <ts e="T572" id="Seg_490" n="e" s="T571">Pɨrniː. </ts>
               <ts e="T573" id="Seg_492" n="e" s="T572">Ilimpɔːqı </ts>
               <ts e="T574" id="Seg_494" n="e" s="T573">ɨralʼ </ts>
               <ts e="T575" id="Seg_496" n="e" s="T574">mɨqaːqi. </ts>
               <ts e="T576" id="Seg_498" n="e" s="T575">Iːjati </ts>
               <ts e="T577" id="Seg_500" n="e" s="T576">ɛːppɨmmɨntɨ </ts>
               <ts e="T578" id="Seg_502" n="e" s="T577">imantɨsa. </ts>
               <ts e="T579" id="Seg_504" n="e" s="T578">Ijatɨ </ts>
               <ts e="T580" id="Seg_506" n="e" s="T579">pona </ts>
               <ts e="T581" id="Seg_508" n="e" s="T580">tarɨlʼa </ts>
               <ts e="T582" id="Seg_510" n="e" s="T581">üːnɨqıtɨ </ts>
               <ts e="T583" id="Seg_512" n="e" s="T582">suːtɨwsä </ts>
               <ts e="T584" id="Seg_514" n="e" s="T583">natqɨlqolammɨntɨtɨ. </ts>
               <ts e="T585" id="Seg_516" n="e" s="T584">Ɨrra </ts>
               <ts e="T586" id="Seg_518" n="e" s="T585">nıː </ts>
               <ts e="T587" id="Seg_520" n="e" s="T586">kəttɨkɨt: </ts>
               <ts e="T588" id="Seg_522" n="e" s="T587">pɨrni </ts>
               <ts e="T589" id="Seg_524" n="e" s="T588">täntɨ </ts>
               <ts e="T590" id="Seg_526" n="e" s="T589">na </ts>
               <ts e="T591" id="Seg_528" n="e" s="T590">tüːnnɨntɨ. </ts>
               <ts e="T592" id="Seg_530" n="e" s="T591">Ijatɨ </ts>
               <ts e="T593" id="Seg_532" n="e" s="T592">jen </ts>
               <ts e="T594" id="Seg_534" n="e" s="T593">ɛsɨmpa. </ts>
               <ts e="T595" id="Seg_536" n="e" s="T594">Meːl </ts>
               <ts e="T596" id="Seg_538" n="e" s="T595">nantɨnɨtɨ. </ts>
               <ts e="T597" id="Seg_540" n="e" s="T596">Ɔːtaqıtɨ </ts>
               <ts e="T598" id="Seg_542" n="e" s="T597">orqɨlʼpat, </ts>
               <ts e="T599" id="Seg_544" n="e" s="T598">qaqlɨ </ts>
               <ts e="T600" id="Seg_546" n="e" s="T599">saralpintɨtɨ. </ts>
               <ts e="T601" id="Seg_548" n="e" s="T600">Qənpa. </ts>
               <ts e="T602" id="Seg_550" n="e" s="T601">Laqolʼtɨmpa. </ts>
               <ts e="T603" id="Seg_552" n="e" s="T602">Pɨrni </ts>
               <ts e="T604" id="Seg_554" n="e" s="T603">pɔːktɨmmäntɨ. </ts>
               <ts e="T605" id="Seg_556" n="e" s="T604">Qumɨnɨk </ts>
               <ts e="T606" id="Seg_558" n="e" s="T605">tüːlʼa, </ts>
               <ts e="T607" id="Seg_560" n="e" s="T606">wənnɨmtɨ </ts>
               <ts e="T608" id="Seg_562" n="e" s="T607">qatɨtqolampatɨ. </ts>
               <ts e="T609" id="Seg_564" n="e" s="T608">Ontɨ </ts>
               <ts e="T610" id="Seg_566" n="e" s="T609">nıː </ts>
               <ts e="T611" id="Seg_568" n="e" s="T610">čʼiːta: </ts>
               <ts e="T612" id="Seg_570" n="e" s="T611">ɨrramɨ, </ts>
               <ts e="T613" id="Seg_572" n="e" s="T612">ɨrramɨ! </ts>
               <ts e="T614" id="Seg_574" n="e" s="T613">Qumɨn </ts>
               <ts e="T615" id="Seg_576" n="e" s="T614">qaqlɨntɨ </ts>
               <ts e="T616" id="Seg_578" n="e" s="T615">tilčʼimpa. </ts>
               <ts e="T617" id="Seg_580" n="e" s="T616">Moqɨnä </ts>
               <ts e="T618" id="Seg_582" n="e" s="T617">tüːmpoːqı. </ts>
               <ts e="T619" id="Seg_584" n="e" s="T618">Mɔːttɨ </ts>
               <ts e="T620" id="Seg_586" n="e" s="T619">šeːrraıːmpa. </ts>
               <ts e="T621" id="Seg_588" n="e" s="T620">Ɨrran </ts>
               <ts e="T622" id="Seg_590" n="e" s="T621">wəntɨ </ts>
               <ts e="T623" id="Seg_592" n="e" s="T622">nıː </ts>
               <ts e="T624" id="Seg_594" n="e" s="T623">qattɨnɨtɨ: </ts>
               <ts e="T625" id="Seg_596" n="e" s="T624">ilʼčʼamɨ, </ts>
               <ts e="T626" id="Seg_598" n="e" s="T625">ilʼčʼamɨ! </ts>
               <ts e="T627" id="Seg_600" n="e" s="T626">Imaqotan </ts>
               <ts e="T628" id="Seg_602" n="e" s="T627">wəntɨ </ts>
               <ts e="T629" id="Seg_604" n="e" s="T628">qattɨnɨtɨ: </ts>
               <ts e="T630" id="Seg_606" n="e" s="T629">imɨlʼamɨ, </ts>
               <ts e="T631" id="Seg_608" n="e" s="T630">imɨlʼamɨ! </ts>
               <ts e="T632" id="Seg_610" n="e" s="T631">Qumɨt </ts>
               <ts e="T633" id="Seg_612" n="e" s="T632">pɨrnisa </ts>
               <ts e="T634" id="Seg_614" n="e" s="T633">nannɨr </ts>
               <ts e="T635" id="Seg_616" n="e" s="T634">munʼilʼmotpɔːtɨn. </ts>
               <ts e="T636" id="Seg_618" n="e" s="T635">Takkarma </ts>
               <ts e="T637" id="Seg_620" n="e" s="T636">meːtɨntɔːtɨn </ts>
               <ts e="T638" id="Seg_622" n="e" s="T637">kuttär </ts>
               <ts e="T639" id="Seg_624" n="e" s="T638">pɨrniw </ts>
               <ts e="T640" id="Seg_626" n="e" s="T639">ılla </ts>
               <ts e="T641" id="Seg_628" n="e" s="T640">qättɔːwɨn. </ts>
               <ts e="T642" id="Seg_630" n="e" s="T641">Nıː </ts>
               <ts e="T643" id="Seg_632" n="e" s="T642">pinpɔːtɨn: </ts>
               <ts e="T644" id="Seg_634" n="e" s="T643">tüːntɨ </ts>
               <ts e="T645" id="Seg_636" n="e" s="T644">pärqɨltɛntɔːtɨn. </ts>
               <ts e="T646" id="Seg_638" n="e" s="T645">Imaqota </ts>
               <ts e="T647" id="Seg_640" n="e" s="T646">wərɨqa </ts>
               <ts e="T648" id="Seg_642" n="e" s="T647">mɨtɨnɨlʼ </ts>
               <ts e="T649" id="Seg_644" n="e" s="T648">čʼi </ts>
               <ts e="T650" id="Seg_646" n="e" s="T649">karra </ts>
               <ts e="T651" id="Seg_648" n="e" s="T650">ɨːtɨmmɨntɨtɨ. </ts>
               <ts e="T652" id="Seg_650" n="e" s="T651">Mušurɨqolampatɨ. </ts>
               <ts e="T653" id="Seg_652" n="e" s="T652">Onʼa, </ts>
               <ts e="T654" id="Seg_654" n="e" s="T653">čʼi </ts>
               <ts e="T655" id="Seg_656" n="e" s="T654">korpɨtɨ! </ts>
               <ts e="T656" id="Seg_658" n="e" s="T655">Toːnna </ts>
               <ts e="T657" id="Seg_660" n="e" s="T656">ima </ts>
               <ts e="T658" id="Seg_662" n="e" s="T657">meːlte </ts>
               <ts e="T659" id="Seg_664" n="e" s="T658">ɔːmta. </ts>
               <ts e="T660" id="Seg_666" n="e" s="T659">Pɨrniː, </ts>
               <ts e="T661" id="Seg_668" n="e" s="T660">orɨm </ts>
               <ts e="T662" id="Seg_670" n="e" s="T661">ɛːŋantɨ. </ts>
               <ts e="T663" id="Seg_672" n="e" s="T662">Čʼi </ts>
               <ts e="T664" id="Seg_674" n="e" s="T663">korpɨtɨ. </ts>
               <ts e="T665" id="Seg_676" n="e" s="T664">Pɨrniː </ts>
               <ts e="T666" id="Seg_678" n="e" s="T665">paktɨmpa. </ts>
               <ts e="T667" id="Seg_680" n="e" s="T666">Čʼiw </ts>
               <ts e="T668" id="Seg_682" n="e" s="T667">čʼaw </ts>
               <ts e="T669" id="Seg_684" n="e" s="T668">korpɨtqantantɨmpatɨ </ts>
               <ts e="T670" id="Seg_686" n="e" s="T669">(korpɨtqolampatɨ), </ts>
               <ts e="T671" id="Seg_688" n="e" s="T670">kuntok </ts>
               <ts e="T672" id="Seg_690" n="e" s="T671">ɛːŋa. </ts>
               <ts e="T673" id="Seg_692" n="e" s="T672">Tüː </ts>
               <ts e="T674" id="Seg_694" n="e" s="T673">tä </ts>
               <ts e="T675" id="Seg_696" n="e" s="T674">šım </ts>
               <ts e="T676" id="Seg_698" n="e" s="T675">amtɨ. </ts>
               <ts e="T677" id="Seg_700" n="e" s="T676">Ɨrra </ts>
               <ts e="T678" id="Seg_702" n="e" s="T677">nıː </ts>
               <ts e="T679" id="Seg_704" n="e" s="T678">kəttakɨtɨ. </ts>
               <ts e="T680" id="Seg_706" n="e" s="T679">Qoralʼok. </ts>
               <ts e="T681" id="Seg_708" n="e" s="T680">Pɨrniː </ts>
               <ts e="T682" id="Seg_710" n="e" s="T681">karra </ts>
               <ts e="T683" id="Seg_712" n="e" s="T682">čʼam </ts>
               <ts e="T684" id="Seg_714" n="e" s="T683">čʼıqɨlʼtempa, </ts>
               <ts e="T685" id="Seg_716" n="e" s="T684">tüːntɨ </ts>
               <ts e="T686" id="Seg_718" n="e" s="T685">alʼčʼimpa. </ts>
               <ts e="T687" id="Seg_720" n="e" s="T686">Alʼčʼɨwtäːqɨntɨ </ts>
               <ts e="T688" id="Seg_722" n="e" s="T687">nıː </ts>
               <ts e="T689" id="Seg_724" n="e" s="T688">kɛtɨmpatɨ. </ts>
               <ts e="T690" id="Seg_726" n="e" s="T689">Tɨːnɨ </ts>
               <ts e="T691" id="Seg_728" n="e" s="T690">jenna </ts>
               <ts e="T692" id="Seg_730" n="e" s="T691">qumɨn </ts>
               <ts e="T693" id="Seg_732" n="e" s="T692">quqɨlɨqilɔːtɨn. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T572" id="Seg_733" s="T571">KR_1969_Witch1_flk.001 (001)</ta>
            <ta e="T575" id="Seg_734" s="T572">KR_1969_Witch1_flk.002 (002.001)</ta>
            <ta e="T578" id="Seg_735" s="T575">KR_1969_Witch1_flk.003 (002.002)</ta>
            <ta e="T584" id="Seg_736" s="T578">KR_1969_Witch1_flk.004 (002.003)</ta>
            <ta e="T591" id="Seg_737" s="T584">KR_1969_Witch1_flk.005 (002.004)</ta>
            <ta e="T594" id="Seg_738" s="T591">KR_1969_Witch1_flk.006 (002.005)</ta>
            <ta e="T596" id="Seg_739" s="T594">KR_1969_Witch1_flk.007 (002.006)</ta>
            <ta e="T600" id="Seg_740" s="T596">KR_1969_Witch1_flk.008 (002.007)</ta>
            <ta e="T601" id="Seg_741" s="T600">KR_1969_Witch1_flk.009 (002.008)</ta>
            <ta e="T602" id="Seg_742" s="T601">KR_1969_Witch1_flk.010 (002.009)</ta>
            <ta e="T604" id="Seg_743" s="T602">KR_1969_Witch1_flk.011 (002.010)</ta>
            <ta e="T608" id="Seg_744" s="T604">KR_1969_Witch1_flk.012 (002.011)</ta>
            <ta e="T613" id="Seg_745" s="T608">KR_1969_Witch1_flk.013 (002.012)</ta>
            <ta e="T616" id="Seg_746" s="T613">KR_1969_Witch1_flk.014 (002.013)</ta>
            <ta e="T618" id="Seg_747" s="T616">KR_1969_Witch1_flk.015 (002.014)</ta>
            <ta e="T620" id="Seg_748" s="T618">KR_1969_Witch1_flk.016 (002.015)</ta>
            <ta e="T626" id="Seg_749" s="T620">KR_1969_Witch1_flk.017 (002.016)</ta>
            <ta e="T631" id="Seg_750" s="T626">KR_1969_Witch1_flk.018 (002.017)</ta>
            <ta e="T635" id="Seg_751" s="T631">KR_1969_Witch1_flk.019 (003.001)</ta>
            <ta e="T641" id="Seg_752" s="T635">KR_1969_Witch1_flk.020 (003.002)</ta>
            <ta e="T645" id="Seg_753" s="T641">KR_1969_Witch1_flk.021 (003.003)</ta>
            <ta e="T651" id="Seg_754" s="T645">KR_1969_Witch1_flk.022 (003.004)</ta>
            <ta e="T652" id="Seg_755" s="T651">KR_1969_Witch1_flk.023 (003.005)</ta>
            <ta e="T655" id="Seg_756" s="T652">KR_1969_Witch1_flk.024 (003.006)</ta>
            <ta e="T659" id="Seg_757" s="T655">KR_1969_Witch1_flk.025 (003.007)</ta>
            <ta e="T662" id="Seg_758" s="T659">KR_1969_Witch1_flk.026 (003.008)</ta>
            <ta e="T664" id="Seg_759" s="T662">KR_1969_Witch1_flk.027 (003.009)</ta>
            <ta e="T666" id="Seg_760" s="T664">KR_1969_Witch1_flk.028 (003.010)</ta>
            <ta e="T672" id="Seg_761" s="T666">KR_1969_Witch1_flk.029 (003.011)</ta>
            <ta e="T676" id="Seg_762" s="T672">KR_1969_Witch1_flk.030 (003.012)</ta>
            <ta e="T679" id="Seg_763" s="T676">KR_1969_Witch1_flk.031 (003.013)</ta>
            <ta e="T680" id="Seg_764" s="T679">KR_1969_Witch1_flk.032 (003.014)</ta>
            <ta e="T686" id="Seg_765" s="T680">KR_1969_Witch1_flk.033 (003.015)</ta>
            <ta e="T689" id="Seg_766" s="T686">KR_1969_Witch1_flk.034 (003.016)</ta>
            <ta e="T693" id="Seg_767" s="T689">KR_1969_Witch1_flk.035 (003.017)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T572" id="Seg_768" s="T571">пыр′нӣ.</ta>
            <ta e="T575" id="Seg_769" s="T572">′иlимпоkи ыралмы′kа̄kи.</ta>
            <ta e="T578" id="Seg_770" s="T575">′ӣjати[й] ′е̨̄ппымынты и′мантыса.</ta>
            <ta e="T584" id="Seg_771" s="T578">′иjаты ′по̄на ′та̄рыlʼа ′ӱ̄ны‵ɣиты ′сӯтыwсӓ ′натkыlколамынтыты.</ta>
            <ta e="T591" id="Seg_772" s="T584">ыр[р]аннӣ kъ̊ттыɣыт, ′пырни ′тӓнтына ′тӱ̄ннынты.</ta>
            <ta e="T594" id="Seg_773" s="T591">иjаты jе′нӓ̄сымпа.</ta>
            <ta e="T596" id="Seg_774" s="T594">меl ′нантыныты.</ta>
            <ta e="T600" id="Seg_775" s="T596">′а̊̄та[ɣ]ӣты ′орkылʼпат, ′kа̄ɣlы са′раlпинтыты.</ta>
            <ta e="T601" id="Seg_776" s="T600">k[х̥]ъ̊̄нпа.</ta>
            <ta e="T602" id="Seg_777" s="T601">lа̄′kоlʼд̂ымпа.</ta>
            <ta e="T604" id="Seg_778" s="T602">′пырни ′па̊kтымӓнты.</ta>
            <ta e="T608" id="Seg_779" s="T604">′kумыных ′тӱ̄lʼа, ′wəннымты k[х̥]атыт коlам′паты.</ta>
            <ta e="T613" id="Seg_780" s="T608">онты нӣ чӣта ы′ррамы, ы̄′ррамы!</ta>
            <ta e="T616" id="Seg_781" s="T613">kумын[т] ′kаɣlанты ′тиlчимпа.</ta>
            <ta e="T618" id="Seg_782" s="T616">моkынӓ ‵тӱ̄мпо̄kи.</ta>
            <ta e="T620" id="Seg_783" s="T618">ма̊̄тты шʼе′рраӣмпа.</ta>
            <ta e="T626" id="Seg_784" s="T620">′ырран ′wəнты нӣ ′k[х̥]а̄ттыныты: ӣlʼчамы, ӣlʼчамы!</ta>
            <ta e="T631" id="Seg_785" s="T626">‵има′kотан ′wəнты k[х̥]а̄ттыныты ′ӣмы‵lʼамы, ′ӣмы‵lʼамы. </ta>
            <ta e="T635" id="Seg_786" s="T631">′kӯмыт[н] ′пырниса ′нанныр ′мунʼилʼмотпотын.</ta>
            <ta e="T641" id="Seg_787" s="T635">′таkkарма ′ме̄тынто̄тын ′куттӓр пырниф ′иlʼlа х[ɣ]ӓ[ъ̊]′тто̄wын </ta>
            <ta e="T645" id="Seg_788" s="T641">нӣ пин′по̄тын ′тӱ̄нд̂ы ‵перkыlтен′тотын.</ta>
            <ta e="T651" id="Seg_789" s="T645">‵ӣма′k[х̥]ота wӓ[ə]рыkа ′мытыныl[лʼ] чи k[х̥]а′рра ′ытымынтыты.</ta>
            <ta e="T652" id="Seg_790" s="T651">′мушʼурыко̄лампаты.</ta>
            <ta e="T655" id="Seg_791" s="T652">о′нʼа! чи kор[ɛ]пыты. </ta>
            <ta e="T659" id="Seg_792" s="T655">то̄на ′имма меlте ′о̄мта.</ta>
            <ta e="T662" id="Seg_793" s="T659">пыр′нӣ, орыме̄ɣанты.</ta>
            <ta e="T664" id="Seg_794" s="T662">чи ′kорпыты.</ta>
            <ta e="T666" id="Seg_795" s="T664">′пырнӣ ′паkтымпа.</ta>
            <ta e="T672" id="Seg_796" s="T666">чиф чаф ‵kорбытхан′тантымпаты [kорпынkоlампаты] кун′ток е̄ɣа.</ta>
            <ta e="T676" id="Seg_797" s="T672">тӱ̄ ′тӓ щʼим ′амты.</ta>
            <ta e="T679" id="Seg_798" s="T676">ырра нӣ kъ̊ттаɣыты.</ta>
            <ta e="T680" id="Seg_799" s="T679">kо′раlʼок[х].</ta>
            <ta e="T686" id="Seg_800" s="T680">′пырнӣ kарра чам чы̄хыlтемпа тӱ̄нты ′аlчимпа.</ta>
            <ta e="T689" id="Seg_801" s="T686">аlчифтӓɣынты нӣ к[k]э[ӓ]тымпаты.</ta>
            <ta e="T693" id="Seg_802" s="T689">′тыны ′jенна ′k[х̥]умын ‵k[х̥]ӯkыlыɣи′lӧ̄тын.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T572" id="Seg_803" s="T571">pɨrniː.</ta>
            <ta e="T575" id="Seg_804" s="T572">ilʼimpoqi ɨralmɨqaːqi.</ta>
            <ta e="T578" id="Seg_805" s="T575">iːjati[j] eːppɨmɨntɨ imantɨsa.</ta>
            <ta e="T584" id="Seg_806" s="T578">ijatɨ poːna taːrɨlʼʼa üːnɨqitɨ suːtɨwsä natqɨlʼkolamɨntɨtɨ.</ta>
            <ta e="T591" id="Seg_807" s="T584">ɨr[r]anniː qəttɨqɨt, pɨrni täntɨna tüːnnɨntɨ.</ta>
            <ta e="T594" id="Seg_808" s="T591">ijatɨ jenäːsɨmpa.</ta>
            <ta e="T596" id="Seg_809" s="T594">melʼ nantɨnɨtɨ.</ta>
            <ta e="T600" id="Seg_810" s="T596">aːta[q]iːtɨ orqɨlʼpat, qaːqlʼɨ saralʼpintɨtɨ.</ta>
            <ta e="T601" id="Seg_811" s="T600">q[q]əːnpa.</ta>
            <ta e="T602" id="Seg_812" s="T601">lʼaːqolʼd̂ɨmpa.</ta>
            <ta e="T604" id="Seg_813" s="T602">pɨrni paqtɨmäntɨ.</ta>
            <ta e="T608" id="Seg_814" s="T604">qumɨnɨh tüːlʼa, wənnɨmtɨ q[q]atɨt kolʼampatɨ.</ta>
            <ta e="T613" id="Seg_815" s="T608">ontɨ niː čiːta ɨrramɨ, ɨːrramɨ!</ta>
            <ta e="T616" id="Seg_816" s="T613">qumɨn[t] qaqlʼantɨ tilʼčimpa.</ta>
            <ta e="T618" id="Seg_817" s="T616">moqɨnä tüːmpoːqi.</ta>
            <ta e="T620" id="Seg_818" s="T618">maːttɨ šerraiːmpa.</ta>
            <ta e="T626" id="Seg_819" s="T620">ɨrran wəntɨ niː q[q]aːttɨnɨtɨ: iːlʼčamɨ, iːlʼčamɨ!</ta>
            <ta e="T631" id="Seg_820" s="T626">imaqotan wəntɨ q[q]aːttɨnɨtɨ iːmɨlʼamɨ, iːmɨlʼamɨ. </ta>
            <ta e="T635" id="Seg_821" s="T631">quːmɨt[n] pɨrnisa nannɨr munʼilʼmotpotɨn.</ta>
            <ta e="T641" id="Seg_822" s="T635">taqqarma meːtɨntoːtɨn kuttär pɨrnif ilʼlʼa h[q]ä[ə]ttoːwɨn </ta>
            <ta e="T645" id="Seg_823" s="T641">niː pinpoːtɨn tüːnd̂ɨ perqɨlʼtentotɨn.</ta>
            <ta e="T651" id="Seg_824" s="T645">iːmaq[q]ota wä[ə]rɨqa mɨtɨnɨlʼ[lʼ] či q[q]arra ɨtɨmɨntɨtɨ.</ta>
            <ta e="T652" id="Seg_825" s="T651">mušurɨkoːlampatɨ.</ta>
            <ta e="T655" id="Seg_826" s="T652">onʼa! či qor[ɛ]pɨtɨ. </ta>
            <ta e="T659" id="Seg_827" s="T655">toːna imma melʼte oːmta.</ta>
            <ta e="T662" id="Seg_828" s="T659">pɨrniː, orɨmeːqantɨ.</ta>
            <ta e="T664" id="Seg_829" s="T662">či qorpɨtɨ.</ta>
            <ta e="T666" id="Seg_830" s="T664">pɨrniː paqtɨmpa.</ta>
            <ta e="T672" id="Seg_831" s="T666">čif čaf qorpɨthantantɨmpatɨ [qorpɨnqolʼampatɨ] kuntok eːqa.</ta>
            <ta e="T676" id="Seg_832" s="T672">tüː tä šʼim amtɨ.</ta>
            <ta e="T679" id="Seg_833" s="T676">ɨrra niː qəttaqɨtɨ.</ta>
            <ta e="T680" id="Seg_834" s="T679">qoralʼʼok[h].</ta>
            <ta e="T686" id="Seg_835" s="T680">pɨrniː qarra čam čɨːhɨlʼtempa tüːntɨ alʼčimpa.</ta>
            <ta e="T689" id="Seg_836" s="T686">alʼčiftäqɨntɨ niː k[q]ɛ[ä]tɨmpatɨ.</ta>
            <ta e="T693" id="Seg_837" s="T689">tɨnɨ jenna q[q]umɨn q[q]uːqɨlʼɨqilʼöːtɨn.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T572" id="Seg_838" s="T571">Pɨrniː. </ta>
            <ta e="T575" id="Seg_839" s="T572">Ilimpɔːqı ɨralʼ mɨqaːqi. </ta>
            <ta e="T578" id="Seg_840" s="T575">Iːjati ɛːppɨmmɨntɨ imantɨsa. </ta>
            <ta e="T584" id="Seg_841" s="T578">Ijatɨ pona tarɨlʼa üːnɨqıtɨ suːtɨwsä natqɨlqolammɨntɨtɨ. </ta>
            <ta e="T591" id="Seg_842" s="T584">Ɨrra nıː kəttɨkɨt: pɨrni täntɨ na tüːnnɨntɨ. </ta>
            <ta e="T594" id="Seg_843" s="T591">Ijatɨ jen ɛsɨmpa. </ta>
            <ta e="T596" id="Seg_844" s="T594">Meːl nantɨnɨtɨ. </ta>
            <ta e="T600" id="Seg_845" s="T596">Ɔːtaqıtɨ orqɨlʼpat, qaqlɨ saralpintɨtɨ. </ta>
            <ta e="T601" id="Seg_846" s="T600">Qənpa. </ta>
            <ta e="T602" id="Seg_847" s="T601">Laqolʼtɨmpa. </ta>
            <ta e="T604" id="Seg_848" s="T602">Pɨrni pɔːktɨmmäntɨ. </ta>
            <ta e="T608" id="Seg_849" s="T604">Qumɨnɨk tüːlʼa, wənnɨmtɨ qatɨtqolampatɨ. </ta>
            <ta e="T613" id="Seg_850" s="T608">Ontɨ nıː čʼiːta: ɨrramɨ, ɨrramɨ! </ta>
            <ta e="T616" id="Seg_851" s="T613">Qumɨn qaqlɨntɨ tilčʼimpa. </ta>
            <ta e="T618" id="Seg_852" s="T616">Moqɨnä tüːmpoːqı. </ta>
            <ta e="T620" id="Seg_853" s="T618">Mɔːttɨ šeːrraıːmpa. </ta>
            <ta e="T626" id="Seg_854" s="T620">Ɨrran wəntɨ nıː qattɨnɨtɨ: ilʼčʼamɨ, ilʼčʼamɨ! </ta>
            <ta e="T631" id="Seg_855" s="T626">Imaqotan wəntɨ qattɨnɨtɨ: imɨlʼamɨ, imɨlʼamɨ! </ta>
            <ta e="T635" id="Seg_856" s="T631">Qumɨt pɨrnisa nannɨr munʼilʼmotpɔːtɨn. </ta>
            <ta e="T641" id="Seg_857" s="T635">Takkarma meːtɨntɔːtɨn kuttär pɨrniw ılla qättɔːwɨn. </ta>
            <ta e="T645" id="Seg_858" s="T641">Nıː pinpɔːtɨn: tüːntɨ pärqɨltɛntɔːtɨn. </ta>
            <ta e="T651" id="Seg_859" s="T645">Imaqota wərɨqa mɨtɨnɨlʼ čʼi karra ɨːtɨmmɨntɨtɨ. </ta>
            <ta e="T652" id="Seg_860" s="T651">Mušurɨqolampatɨ. </ta>
            <ta e="T655" id="Seg_861" s="T652">Onʼa, čʼi korpɨtɨ! </ta>
            <ta e="T659" id="Seg_862" s="T655">Toːnna ima meːlte ɔːmta. </ta>
            <ta e="T662" id="Seg_863" s="T659">Pɨrniː, orɨm ɛːŋantɨ. </ta>
            <ta e="T664" id="Seg_864" s="T662">Čʼi korpɨtɨ. </ta>
            <ta e="T666" id="Seg_865" s="T664">Pɨrniː paktɨmpa. </ta>
            <ta e="T672" id="Seg_866" s="T666">Čʼiw čʼaw korpɨtqantantɨmpatɨ (korpɨtqolampatɨ), kuntok ɛːŋa. </ta>
            <ta e="T676" id="Seg_867" s="T672">Tüː tä šım amtɨ. </ta>
            <ta e="T679" id="Seg_868" s="T676">Ɨrra nıː kəttakɨtɨ. </ta>
            <ta e="T680" id="Seg_869" s="T679">Qoralʼok. </ta>
            <ta e="T686" id="Seg_870" s="T680">Pɨrniː karra čʼam čʼıqɨlʼtempa, tüːntɨ alʼčʼimpa. </ta>
            <ta e="T689" id="Seg_871" s="T686">Alʼčʼɨwtäːqɨntɨ nıː kɛtɨmpatɨ. </ta>
            <ta e="T693" id="Seg_872" s="T689">Tɨːnɨ jenna qumɨn quqɨlɨqilɔːtɨn. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T572" id="Seg_873" s="T571">pɨrniː</ta>
            <ta e="T573" id="Seg_874" s="T572">ili-mpɔː-qı</ta>
            <ta e="T574" id="Seg_875" s="T573">ɨra-lʼ</ta>
            <ta e="T575" id="Seg_876" s="T574">mɨ-qaːqi</ta>
            <ta e="T576" id="Seg_877" s="T575">iːja-ti</ta>
            <ta e="T577" id="Seg_878" s="T576">ɛː-ppɨ-mmɨ-ntɨ</ta>
            <ta e="T578" id="Seg_879" s="T577">ima-ntɨ-sa</ta>
            <ta e="T579" id="Seg_880" s="T578">ija-tɨ</ta>
            <ta e="T580" id="Seg_881" s="T579">pona</ta>
            <ta e="T581" id="Seg_882" s="T580">tarɨ-lʼa</ta>
            <ta e="T582" id="Seg_883" s="T581">üːnɨ-qı-tɨ</ta>
            <ta e="T583" id="Seg_884" s="T582">suːtɨw-sä</ta>
            <ta e="T584" id="Seg_885" s="T583">nat-qɨl-q-ola-mmɨ-ntɨ-tɨ</ta>
            <ta e="T585" id="Seg_886" s="T584">ɨrra</ta>
            <ta e="T586" id="Seg_887" s="T585">nıː</ta>
            <ta e="T587" id="Seg_888" s="T586">kəttɨ-kɨ-t</ta>
            <ta e="T588" id="Seg_889" s="T587">pɨrni</ta>
            <ta e="T589" id="Seg_890" s="T588">täntɨ</ta>
            <ta e="T590" id="Seg_891" s="T589">na</ta>
            <ta e="T591" id="Seg_892" s="T590">tüː-nnɨ-ntɨ</ta>
            <ta e="T592" id="Seg_893" s="T591">ija-tɨ</ta>
            <ta e="T593" id="Seg_894" s="T592">jen</ta>
            <ta e="T594" id="Seg_895" s="T593">ɛsɨ-mpa</ta>
            <ta e="T595" id="Seg_896" s="T594">meːl</ta>
            <ta e="T596" id="Seg_897" s="T595">nan-tɨ-nɨ-tɨ</ta>
            <ta e="T597" id="Seg_898" s="T596">ɔːta-qı-tɨ</ta>
            <ta e="T598" id="Seg_899" s="T597">orqɨlʼ-pa-t</ta>
            <ta e="T599" id="Seg_900" s="T598">qaqlɨ</ta>
            <ta e="T600" id="Seg_901" s="T599">sar-al-pi-ntɨ-tɨ</ta>
            <ta e="T601" id="Seg_902" s="T600">qən-pa</ta>
            <ta e="T602" id="Seg_903" s="T601">laqolʼtɨ-mpa</ta>
            <ta e="T603" id="Seg_904" s="T602">pɨrni</ta>
            <ta e="T604" id="Seg_905" s="T603">pɔːktɨ-mmä-ntɨ</ta>
            <ta e="T605" id="Seg_906" s="T604">qum-ɨ-nɨk</ta>
            <ta e="T606" id="Seg_907" s="T605">tüː-lʼa</ta>
            <ta e="T607" id="Seg_908" s="T606">wənnɨ-m-tɨ</ta>
            <ta e="T608" id="Seg_909" s="T607">qatɨ-t-q-olam-pa-tɨ</ta>
            <ta e="T609" id="Seg_910" s="T608">ontɨ</ta>
            <ta e="T610" id="Seg_911" s="T609">nıː</ta>
            <ta e="T611" id="Seg_912" s="T610">čʼiːta</ta>
            <ta e="T612" id="Seg_913" s="T611">ɨrra-mɨ</ta>
            <ta e="T613" id="Seg_914" s="T612">ɨrra-mɨ</ta>
            <ta e="T614" id="Seg_915" s="T613">qum-ɨ-n</ta>
            <ta e="T615" id="Seg_916" s="T614">qaqlɨ-ntɨ</ta>
            <ta e="T616" id="Seg_917" s="T615">tilčʼi-mpa</ta>
            <ta e="T617" id="Seg_918" s="T616">moqɨnä</ta>
            <ta e="T618" id="Seg_919" s="T617">tüː-mpoː-qı</ta>
            <ta e="T619" id="Seg_920" s="T618">mɔːt-tɨ</ta>
            <ta e="T620" id="Seg_921" s="T619">šeːrr-aıː-mpa</ta>
            <ta e="T621" id="Seg_922" s="T620">ɨrra-n</ta>
            <ta e="T622" id="Seg_923" s="T621">wəntɨ</ta>
            <ta e="T623" id="Seg_924" s="T622">nıː</ta>
            <ta e="T624" id="Seg_925" s="T623">qat-tɨ-nɨ-tɨ</ta>
            <ta e="T625" id="Seg_926" s="T624">ilʼčʼa-mɨ</ta>
            <ta e="T626" id="Seg_927" s="T625">ilʼčʼa-mɨ</ta>
            <ta e="T627" id="Seg_928" s="T626">imaqota-n</ta>
            <ta e="T628" id="Seg_929" s="T627">wəntɨ</ta>
            <ta e="T629" id="Seg_930" s="T628">qat-tɨ-nɨ-tɨ</ta>
            <ta e="T630" id="Seg_931" s="T629">imɨlʼa-mɨ</ta>
            <ta e="T631" id="Seg_932" s="T630">imɨlʼa-mɨ</ta>
            <ta e="T632" id="Seg_933" s="T631">qum-ɨ-t</ta>
            <ta e="T633" id="Seg_934" s="T632">pɨrni-sa</ta>
            <ta e="T634" id="Seg_935" s="T633">nannɨr</ta>
            <ta e="T635" id="Seg_936" s="T634">munʼilʼmot-pɔː-tɨn</ta>
            <ta e="T636" id="Seg_937" s="T635">takkarma</ta>
            <ta e="T637" id="Seg_938" s="T636">meː-tɨ-ntɔː-tɨn</ta>
            <ta e="T638" id="Seg_939" s="T637">kuttär</ta>
            <ta e="T639" id="Seg_940" s="T638">pɨrni-w</ta>
            <ta e="T640" id="Seg_941" s="T639">ılla</ta>
            <ta e="T641" id="Seg_942" s="T640">qättɔː-wɨn</ta>
            <ta e="T642" id="Seg_943" s="T641">nıː</ta>
            <ta e="T643" id="Seg_944" s="T642">pin-pɔː-tɨn</ta>
            <ta e="T644" id="Seg_945" s="T643">tüː-ntɨ</ta>
            <ta e="T645" id="Seg_946" s="T644">pärqɨl-tɛntɔː-tɨn</ta>
            <ta e="T646" id="Seg_947" s="T645">imaqota</ta>
            <ta e="T647" id="Seg_948" s="T646">wərɨqa</ta>
            <ta e="T648" id="Seg_949" s="T647">mɨtɨ-n-ɨ-lʼ</ta>
            <ta e="T649" id="Seg_950" s="T648">čʼi</ta>
            <ta e="T650" id="Seg_951" s="T649">karra</ta>
            <ta e="T651" id="Seg_952" s="T650">ɨːtɨ-mmɨ-ntɨ-tɨ</ta>
            <ta e="T652" id="Seg_953" s="T651">mušu-rɨ-q-olam-pa-tɨ</ta>
            <ta e="T653" id="Seg_954" s="T652">onʼa</ta>
            <ta e="T654" id="Seg_955" s="T653">čʼi</ta>
            <ta e="T655" id="Seg_956" s="T654">korpɨt-ɨ</ta>
            <ta e="T656" id="Seg_957" s="T655">toːnna</ta>
            <ta e="T657" id="Seg_958" s="T656">ima</ta>
            <ta e="T658" id="Seg_959" s="T657">meːlte</ta>
            <ta e="T659" id="Seg_960" s="T658">ɔːmta</ta>
            <ta e="T660" id="Seg_961" s="T659">pɨrniː</ta>
            <ta e="T661" id="Seg_962" s="T660">orɨ-m</ta>
            <ta e="T662" id="Seg_963" s="T661">ɛː-ŋa-ntɨ</ta>
            <ta e="T663" id="Seg_964" s="T662">čʼi</ta>
            <ta e="T664" id="Seg_965" s="T663">korpɨt-ɨ</ta>
            <ta e="T665" id="Seg_966" s="T664">pɨrniː</ta>
            <ta e="T666" id="Seg_967" s="T665">paktɨ-mpa</ta>
            <ta e="T667" id="Seg_968" s="T666">čʼi-w</ta>
            <ta e="T668" id="Seg_969" s="T667">čʼaw</ta>
            <ta e="T669" id="Seg_970" s="T668">korpɨt-q-antantɨ-mpa-tɨ</ta>
            <ta e="T670" id="Seg_971" s="T669">korpɨt-q-ola-mpa-tɨ</ta>
            <ta e="T671" id="Seg_972" s="T670">kuntok</ta>
            <ta e="T672" id="Seg_973" s="T671">ɛː-ŋa</ta>
            <ta e="T673" id="Seg_974" s="T672">tü</ta>
            <ta e="T674" id="Seg_975" s="T673">tä</ta>
            <ta e="T675" id="Seg_976" s="T674">šım</ta>
            <ta e="T676" id="Seg_977" s="T675">am-tɨ</ta>
            <ta e="T677" id="Seg_978" s="T676">ɨrra</ta>
            <ta e="T678" id="Seg_979" s="T677">nıː</ta>
            <ta e="T679" id="Seg_980" s="T678">kətta-kɨ-tɨ</ta>
            <ta e="T680" id="Seg_981" s="T679">qoralʼok</ta>
            <ta e="T681" id="Seg_982" s="T680">pɨrniː</ta>
            <ta e="T682" id="Seg_983" s="T681">karra</ta>
            <ta e="T683" id="Seg_984" s="T682">čʼam</ta>
            <ta e="T684" id="Seg_985" s="T683">čʼıqɨlʼ-te-mpa</ta>
            <ta e="T685" id="Seg_986" s="T684">tüː-ntɨ</ta>
            <ta e="T686" id="Seg_987" s="T685">alʼčʼi-mpa</ta>
            <ta e="T687" id="Seg_988" s="T686">alʼčʼɨ-wtäː-qɨn-tɨ</ta>
            <ta e="T688" id="Seg_989" s="T687">nıː</ta>
            <ta e="T689" id="Seg_990" s="T688">kɛtɨ-mpa-tɨ</ta>
            <ta e="T690" id="Seg_991" s="T689">tɨːnɨ</ta>
            <ta e="T691" id="Seg_992" s="T690">jenna</ta>
            <ta e="T692" id="Seg_993" s="T691">qum-ɨ-n</ta>
            <ta e="T693" id="Seg_994" s="T692">qu-q-ɨlɨ-qilɔː-tɨn</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T572" id="Seg_995" s="T571">pɨrni</ta>
            <ta e="T573" id="Seg_996" s="T572">ilɨ-mpɨ-qı</ta>
            <ta e="T574" id="Seg_997" s="T573">ira-lʼ</ta>
            <ta e="T575" id="Seg_998" s="T574">mɨ-qı</ta>
            <ta e="T576" id="Seg_999" s="T575">iːja-tɨ</ta>
            <ta e="T577" id="Seg_1000" s="T576">ɛː-mpɨ-mpɨ-ntɨ</ta>
            <ta e="T578" id="Seg_1001" s="T577">ima-ntɨ-sä</ta>
            <ta e="T579" id="Seg_1002" s="T578">iːja-tɨ</ta>
            <ta e="T580" id="Seg_1003" s="T579">ponä</ta>
            <ta e="T581" id="Seg_1004" s="T580">tarɨ-lä</ta>
            <ta e="T582" id="Seg_1005" s="T581">üːnɨ-qı-tɨ</ta>
            <ta e="T583" id="Seg_1006" s="T582">suːtɨw-sä</ta>
            <ta e="T584" id="Seg_1007" s="T583">nat-qɨl-qo-olam-mpɨ-ntɨ-tɨ</ta>
            <ta e="T585" id="Seg_1008" s="T584">ira</ta>
            <ta e="T586" id="Seg_1009" s="T585">nık</ta>
            <ta e="T587" id="Seg_1010" s="T586">kətɨ-kkɨ-tɨ</ta>
            <ta e="T588" id="Seg_1011" s="T587">pɨrni</ta>
            <ta e="T589" id="Seg_1012" s="T588">täntɨ</ta>
            <ta e="T590" id="Seg_1013" s="T589">na</ta>
            <ta e="T591" id="Seg_1014" s="T590">tü-ɛntɨ-ntɨ</ta>
            <ta e="T592" id="Seg_1015" s="T591">iːja-tɨ</ta>
            <ta e="T593" id="Seg_1016" s="T592">nʼennä</ta>
            <ta e="T594" id="Seg_1017" s="T593">ɛsɨ-mpɨ</ta>
            <ta e="T595" id="Seg_1018" s="T594">meːl</ta>
            <ta e="T596" id="Seg_1019" s="T595">nat-ntɨ-ŋɨ-tɨ</ta>
            <ta e="T597" id="Seg_1020" s="T596">ɔːtä-qı-tɨ</ta>
            <ta e="T598" id="Seg_1021" s="T597">orqɨl-mpɨ-tɨ</ta>
            <ta e="T599" id="Seg_1022" s="T598">qaqlɨ</ta>
            <ta e="T600" id="Seg_1023" s="T599">*sar-ätɔːl-mpɨ-ntɨ-tɨ</ta>
            <ta e="T601" id="Seg_1024" s="T600">qən-mpɨ</ta>
            <ta e="T602" id="Seg_1025" s="T601">laqaltɨ-mpɨ</ta>
            <ta e="T603" id="Seg_1026" s="T602">pɨrni</ta>
            <ta e="T604" id="Seg_1027" s="T603">paktɨ-mpɨ-ntɨ</ta>
            <ta e="T605" id="Seg_1028" s="T604">qum-ɨ-nɨŋ</ta>
            <ta e="T606" id="Seg_1029" s="T605">tü-lä</ta>
            <ta e="T607" id="Seg_1030" s="T606">wəntɨ-m-tɨ</ta>
            <ta e="T608" id="Seg_1031" s="T607">qatɨ-tɨ-qo-olam-mpɨ-tɨ</ta>
            <ta e="T609" id="Seg_1032" s="T608">ontɨ</ta>
            <ta e="T610" id="Seg_1033" s="T609">nık</ta>
            <ta e="T611" id="Seg_1034" s="T610">čʼıːtɨ</ta>
            <ta e="T612" id="Seg_1035" s="T611">ira-mɨ</ta>
            <ta e="T613" id="Seg_1036" s="T612">ira-mɨ</ta>
            <ta e="T614" id="Seg_1037" s="T613">qum-ɨ-n</ta>
            <ta e="T615" id="Seg_1038" s="T614">qaqlɨ-ntɨ</ta>
            <ta e="T616" id="Seg_1039" s="T615">tilʼčʼi-mpɨ</ta>
            <ta e="T617" id="Seg_1040" s="T616">moqɨnä</ta>
            <ta e="T618" id="Seg_1041" s="T617">tü-mpɨ-qı</ta>
            <ta e="T619" id="Seg_1042" s="T618">mɔːt-ntɨ</ta>
            <ta e="T620" id="Seg_1043" s="T619">šeːr-ɛː-mpɨ</ta>
            <ta e="T621" id="Seg_1044" s="T620">ira-n</ta>
            <ta e="T622" id="Seg_1045" s="T621">wəntɨ</ta>
            <ta e="T623" id="Seg_1046" s="T622">nık</ta>
            <ta e="T624" id="Seg_1047" s="T623">qatɨ-tɨ-ŋɨ-tɨ</ta>
            <ta e="T625" id="Seg_1048" s="T624">ilʼčʼa-mɨ</ta>
            <ta e="T626" id="Seg_1049" s="T625">ilʼčʼa-mɨ</ta>
            <ta e="T627" id="Seg_1050" s="T626">imaqota-n</ta>
            <ta e="T628" id="Seg_1051" s="T627">wəntɨ</ta>
            <ta e="T629" id="Seg_1052" s="T628">qatɨ-tɨ-ŋɨ-tɨ</ta>
            <ta e="T630" id="Seg_1053" s="T629">imɨlʼa-mɨ</ta>
            <ta e="T631" id="Seg_1054" s="T630">imɨlʼa-mɨ</ta>
            <ta e="T632" id="Seg_1055" s="T631">qum-ɨ-t</ta>
            <ta e="T633" id="Seg_1056" s="T632">pɨrni-sä</ta>
            <ta e="T634" id="Seg_1057" s="T633">nannɛr</ta>
            <ta e="T635" id="Seg_1058" s="T634">munʼɨlmot-mpɨ-tɨt</ta>
            <ta e="T636" id="Seg_1059" s="T635">takkɨrmo</ta>
            <ta e="T637" id="Seg_1060" s="T636">meː-tɨ-ntɨ-tɨt</ta>
            <ta e="T638" id="Seg_1061" s="T637">kuttar</ta>
            <ta e="T639" id="Seg_1062" s="T638">pɨrni-m</ta>
            <ta e="T640" id="Seg_1063" s="T639">ıllä</ta>
            <ta e="T641" id="Seg_1064" s="T640">qättɨ-tɨt</ta>
            <ta e="T642" id="Seg_1065" s="T641">nık</ta>
            <ta e="T643" id="Seg_1066" s="T642">pin-mpɨ-tɨt</ta>
            <ta e="T644" id="Seg_1067" s="T643">tü-ntɨ</ta>
            <ta e="T645" id="Seg_1068" s="T644">pärqɨl-ɛntɨ-tɨt</ta>
            <ta e="T646" id="Seg_1069" s="T645">imaqota</ta>
            <ta e="T647" id="Seg_1070" s="T646">wərqɨ</ta>
            <ta e="T648" id="Seg_1071" s="T647">mɨtɨ-t-ɨ-lʼ</ta>
            <ta e="T649" id="Seg_1072" s="T648">čʼi</ta>
            <ta e="T650" id="Seg_1073" s="T649">karrä</ta>
            <ta e="T651" id="Seg_1074" s="T650">ɨːtɨ-mpɨ-ntɨ-tɨ</ta>
            <ta e="T652" id="Seg_1075" s="T651">mušɨ-rɨ-qo-olam-mpɨ-tɨ</ta>
            <ta e="T653" id="Seg_1076" s="T652">onʼa</ta>
            <ta e="T654" id="Seg_1077" s="T653">čʼi</ta>
            <ta e="T655" id="Seg_1078" s="T654">korpɨt-ätɨ</ta>
            <ta e="T656" id="Seg_1079" s="T655">toːnna</ta>
            <ta e="T657" id="Seg_1080" s="T656">ima</ta>
            <ta e="T658" id="Seg_1081" s="T657">meːltɨ</ta>
            <ta e="T659" id="Seg_1082" s="T658">ɔːmtɨ</ta>
            <ta e="T660" id="Seg_1083" s="T659">pɨrni</ta>
            <ta e="T661" id="Seg_1084" s="T660">orɨm-mɨ</ta>
            <ta e="T662" id="Seg_1085" s="T661">ɛː-ŋɨ-ntɨ</ta>
            <ta e="T663" id="Seg_1086" s="T662">čʼi</ta>
            <ta e="T664" id="Seg_1087" s="T663">korpɨt-ätɨ</ta>
            <ta e="T665" id="Seg_1088" s="T664">pɨrni</ta>
            <ta e="T666" id="Seg_1089" s="T665">paktɨ-mpɨ</ta>
            <ta e="T667" id="Seg_1090" s="T666">čʼi-m</ta>
            <ta e="T668" id="Seg_1091" s="T667">čʼam</ta>
            <ta e="T669" id="Seg_1092" s="T668">korpɨt-qo-antaltɨ-mpɨ-tɨ</ta>
            <ta e="T670" id="Seg_1093" s="T669">korpɨt-qo-olam-mpɨ-tɨ</ta>
            <ta e="T671" id="Seg_1094" s="T670">kuntak</ta>
            <ta e="T672" id="Seg_1095" s="T671">ɛː-ŋɨ</ta>
            <ta e="T673" id="Seg_1096" s="T672">tü</ta>
            <ta e="T674" id="Seg_1097" s="T673">to</ta>
            <ta e="T675" id="Seg_1098" s="T674">mašım</ta>
            <ta e="T676" id="Seg_1099" s="T675">am-ɛntɨ</ta>
            <ta e="T677" id="Seg_1100" s="T676">ira</ta>
            <ta e="T678" id="Seg_1101" s="T677">nık</ta>
            <ta e="T679" id="Seg_1102" s="T678">kətɨ-kkɨ-tɨ</ta>
            <ta e="T681" id="Seg_1103" s="T680">pɨrni</ta>
            <ta e="T682" id="Seg_1104" s="T681">karrä</ta>
            <ta e="T683" id="Seg_1105" s="T682">čʼam</ta>
            <ta e="T684" id="Seg_1106" s="T683">čʼıqɨl-tɨ-mpɨ</ta>
            <ta e="T685" id="Seg_1107" s="T684">tü-ntɨ</ta>
            <ta e="T686" id="Seg_1108" s="T685">alʼčʼɨ-mpɨ</ta>
            <ta e="T687" id="Seg_1109" s="T686">alʼčʼɨ-ptäː-qɨn-ntɨ</ta>
            <ta e="T688" id="Seg_1110" s="T687">nık</ta>
            <ta e="T689" id="Seg_1111" s="T688">kətɨ-mpɨ-tɨ</ta>
            <ta e="T690" id="Seg_1112" s="T689">tɨːnɨ</ta>
            <ta e="T691" id="Seg_1113" s="T690">nʼennä</ta>
            <ta e="T692" id="Seg_1114" s="T691">qum-ɨ-t</ta>
            <ta e="T693" id="Seg_1115" s="T692">qu-qo-olam-qɨlɨ-tɨt</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T572" id="Seg_1116" s="T571">witch.[NOM]</ta>
            <ta e="T573" id="Seg_1117" s="T572">live-PST.NAR-3DU.S</ta>
            <ta e="T574" id="Seg_1118" s="T573">old.man-ADJZ</ta>
            <ta e="T575" id="Seg_1119" s="T574">something-DU</ta>
            <ta e="T576" id="Seg_1120" s="T575">child.[NOM]-3SG</ta>
            <ta e="T577" id="Seg_1121" s="T576">be-DUR-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T578" id="Seg_1122" s="T577">wife-OBL.3SG-COM</ta>
            <ta e="T579" id="Seg_1123" s="T578">child.[NOM]-3SG</ta>
            <ta e="T580" id="Seg_1124" s="T579">outwards</ta>
            <ta e="T581" id="Seg_1125" s="T580">go.out-CVB</ta>
            <ta e="T582" id="Seg_1126" s="T581">leash-DU.[NOM]-3SG</ta>
            <ta e="T583" id="Seg_1127" s="T582">%%-INSTR</ta>
            <ta e="T584" id="Seg_1128" s="T583">anoint-MULO-INF-begin-PST.NAR-INFER-3SG.O</ta>
            <ta e="T585" id="Seg_1129" s="T584">husband.[NOM]</ta>
            <ta e="T586" id="Seg_1130" s="T585">so</ta>
            <ta e="T587" id="Seg_1131" s="T586">say-HAB-3SG.O</ta>
            <ta e="T588" id="Seg_1132" s="T587">witch.[NOM]</ta>
            <ta e="T589" id="Seg_1133" s="T588">you.SG.ALL</ta>
            <ta e="T590" id="Seg_1134" s="T589">INFER</ta>
            <ta e="T591" id="Seg_1135" s="T590">come-FUT-INFER.[3SG.S]</ta>
            <ta e="T592" id="Seg_1136" s="T591">child.[NOM]-3SG</ta>
            <ta e="T593" id="Seg_1137" s="T592">forward</ta>
            <ta e="T594" id="Seg_1138" s="T593">become-PST.NAR.[3SG.S]</ta>
            <ta e="T595" id="Seg_1139" s="T594">always</ta>
            <ta e="T596" id="Seg_1140" s="T595">anoint-IPFV-CO-3SG.O</ta>
            <ta e="T597" id="Seg_1141" s="T596">reindeer-DU.[NOM]-3SG</ta>
            <ta e="T598" id="Seg_1142" s="T597">catch-PST.NAR-3SG.O</ta>
            <ta e="T599" id="Seg_1143" s="T598">sledge.[NOM]</ta>
            <ta e="T600" id="Seg_1144" s="T599">harness-MOM-PST.NAR-INFER-3SG.O</ta>
            <ta e="T601" id="Seg_1145" s="T600">leave-PST.NAR.[3SG.S]</ta>
            <ta e="T602" id="Seg_1146" s="T601">start.for-PST.NAR.[3SG.S]</ta>
            <ta e="T603" id="Seg_1147" s="T602">witch.[NOM]</ta>
            <ta e="T604" id="Seg_1148" s="T603">run-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T605" id="Seg_1149" s="T604">human.being-EP-ALL</ta>
            <ta e="T606" id="Seg_1150" s="T605">come-CVB</ta>
            <ta e="T607" id="Seg_1151" s="T606">face-ACC-3SG</ta>
            <ta e="T608" id="Seg_1152" s="T607">claw-TR-INF-begin-PST.NAR-3SG.O</ta>
            <ta e="T609" id="Seg_1153" s="T608">oneself.3SG.[NOM]</ta>
            <ta e="T610" id="Seg_1154" s="T609">so</ta>
            <ta e="T611" id="Seg_1155" s="T610">tell.[3SG.S]</ta>
            <ta e="T612" id="Seg_1156" s="T611">husband.[NOM]-1SG</ta>
            <ta e="T613" id="Seg_1157" s="T612">husband.[NOM]-1SG</ta>
            <ta e="T614" id="Seg_1158" s="T613">human.being-EP-GEN</ta>
            <ta e="T615" id="Seg_1159" s="T614">sledge-ILL</ta>
            <ta e="T616" id="Seg_1160" s="T615">sit.down-PST.NAR.[3SG.S]</ta>
            <ta e="T617" id="Seg_1161" s="T616">home</ta>
            <ta e="T618" id="Seg_1162" s="T617">come-PST.NAR-3DU.S</ta>
            <ta e="T619" id="Seg_1163" s="T618">tent-ILL</ta>
            <ta e="T620" id="Seg_1164" s="T619">come.in-PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T621" id="Seg_1165" s="T620">husband-GEN</ta>
            <ta e="T622" id="Seg_1166" s="T621">face.[NOM]</ta>
            <ta e="T623" id="Seg_1167" s="T622">so</ta>
            <ta e="T624" id="Seg_1168" s="T623">claw-TR-CO-3SG.O</ta>
            <ta e="T625" id="Seg_1169" s="T624">grandfather.[NOM]-1SG</ta>
            <ta e="T626" id="Seg_1170" s="T625">grandfather.[NOM]-1SG</ta>
            <ta e="T627" id="Seg_1171" s="T626">old.woman-GEN</ta>
            <ta e="T628" id="Seg_1172" s="T627">face.[NOM]</ta>
            <ta e="T629" id="Seg_1173" s="T628">claw-TR-CO-3SG.O</ta>
            <ta e="T630" id="Seg_1174" s="T629">grandmother.[NOM]-1SG</ta>
            <ta e="T631" id="Seg_1175" s="T630">grandmother.[NOM]-1SG</ta>
            <ta e="T632" id="Seg_1176" s="T631">human.being-EP-PL.[NOM]</ta>
            <ta e="T633" id="Seg_1177" s="T632">witch-INSTR</ta>
            <ta e="T634" id="Seg_1178" s="T633">so.much</ta>
            <ta e="T635" id="Seg_1179" s="T634">be.bored-PST.NAR-3PL</ta>
            <ta e="T636" id="Seg_1180" s="T635">place.for.meeting.[NOM]</ta>
            <ta e="T637" id="Seg_1181" s="T636">make-TR-IPFV-3PL</ta>
            <ta e="T638" id="Seg_1182" s="T637">how</ta>
            <ta e="T639" id="Seg_1183" s="T638">witch-ACC</ta>
            <ta e="T640" id="Seg_1184" s="T639">down</ta>
            <ta e="T641" id="Seg_1185" s="T640">hit-3PL</ta>
            <ta e="T642" id="Seg_1186" s="T641">so</ta>
            <ta e="T643" id="Seg_1187" s="T642">put-PST.NAR-3PL</ta>
            <ta e="T644" id="Seg_1188" s="T643">fire-ILL</ta>
            <ta e="T645" id="Seg_1189" s="T644">push-FUT-3PL</ta>
            <ta e="T646" id="Seg_1190" s="T645">old.woman.[NOM]</ta>
            <ta e="T647" id="Seg_1191" s="T646">big</ta>
            <ta e="T648" id="Seg_1192" s="T647">liver-PL-EP-ADJZ</ta>
            <ta e="T649" id="Seg_1193" s="T648">cauldron.[NOM]</ta>
            <ta e="T650" id="Seg_1194" s="T649">onto.the.fire</ta>
            <ta e="T651" id="Seg_1195" s="T650">hang-PST.NAR-INFER-3SG.O</ta>
            <ta e="T652" id="Seg_1196" s="T651">be.cooking-CAUS-INF-begin-PST.NAR-3SG.O</ta>
            <ta e="T653" id="Seg_1197" s="T652">daughter_in_law.[NOM]</ta>
            <ta e="T654" id="Seg_1198" s="T653">cauldron.[NOM]</ta>
            <ta e="T655" id="Seg_1199" s="T654">mix-IMP.2SG.O</ta>
            <ta e="T656" id="Seg_1200" s="T655">that</ta>
            <ta e="T657" id="Seg_1201" s="T656">woman.[NOM]</ta>
            <ta e="T658" id="Seg_1202" s="T657">all.the.time</ta>
            <ta e="T659" id="Seg_1203" s="T658">sit.[3SG.S]</ta>
            <ta e="T660" id="Seg_1204" s="T659">witch.[NOM]</ta>
            <ta e="T661" id="Seg_1205" s="T660">force.[NOM]-1SG</ta>
            <ta e="T662" id="Seg_1206" s="T661">be-CO-2SG.S</ta>
            <ta e="T663" id="Seg_1207" s="T662">cauldron.[NOM]</ta>
            <ta e="T664" id="Seg_1208" s="T663">mix-IMP.2SG.O</ta>
            <ta e="T665" id="Seg_1209" s="T664">witch.[NOM]</ta>
            <ta e="T666" id="Seg_1210" s="T665">run-PST.NAR.[3SG.S]</ta>
            <ta e="T667" id="Seg_1211" s="T666">cauldron-ACC</ta>
            <ta e="T668" id="Seg_1212" s="T667">hardly</ta>
            <ta e="T669" id="Seg_1213" s="T668">mix-INF-try-PST.NAR-3SG.O</ta>
            <ta e="T670" id="Seg_1214" s="T669">mix-INF-be.going.to-PST.NAR-3SG.O</ta>
            <ta e="T671" id="Seg_1215" s="T670">far</ta>
            <ta e="T672" id="Seg_1216" s="T671">be-CO.[3SG.S]</ta>
            <ta e="T673" id="Seg_1217" s="T672">fire.[NOM]</ta>
            <ta e="T674" id="Seg_1218" s="T673">that</ta>
            <ta e="T675" id="Seg_1219" s="T674">I.ACC</ta>
            <ta e="T676" id="Seg_1220" s="T675">eat-FUT.[3SG.S]</ta>
            <ta e="T677" id="Seg_1221" s="T676">husband.[NOM]</ta>
            <ta e="T678" id="Seg_1222" s="T677">so</ta>
            <ta e="T679" id="Seg_1223" s="T678">say-HAB-3SG.O</ta>
            <ta e="T681" id="Seg_1224" s="T680">witch.[NOM]</ta>
            <ta e="T682" id="Seg_1225" s="T681">to.the.fireplace</ta>
            <ta e="T683" id="Seg_1226" s="T682">hardly</ta>
            <ta e="T684" id="Seg_1227" s="T683">step-TR-PST.NAR.[3SG.S]</ta>
            <ta e="T685" id="Seg_1228" s="T684">fire-ILL</ta>
            <ta e="T686" id="Seg_1229" s="T685">fall-PST.NAR.[3SG.S]</ta>
            <ta e="T687" id="Seg_1230" s="T686">fall-ACTN-LOC-OBL.3SG</ta>
            <ta e="T688" id="Seg_1231" s="T687">so</ta>
            <ta e="T689" id="Seg_1232" s="T688">say-PST.NAR-3SG.O</ta>
            <ta e="T690" id="Seg_1233" s="T689">from.here</ta>
            <ta e="T691" id="Seg_1234" s="T690">forward</ta>
            <ta e="T692" id="Seg_1235" s="T691">human.being-EP-PL.[NOM]</ta>
            <ta e="T693" id="Seg_1236" s="T692">die-INF-begin-MULS-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T572" id="Seg_1237" s="T571">ведьма.[NOM]</ta>
            <ta e="T573" id="Seg_1238" s="T572">жить-PST.NAR-3DU.S</ta>
            <ta e="T574" id="Seg_1239" s="T573">старик-ADJZ</ta>
            <ta e="T575" id="Seg_1240" s="T574">нечто-DU</ta>
            <ta e="T576" id="Seg_1241" s="T575">ребенок.[NOM]-3SG</ta>
            <ta e="T577" id="Seg_1242" s="T576">быть-DUR-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T578" id="Seg_1243" s="T577">жена-OBL.3SG-COM</ta>
            <ta e="T579" id="Seg_1244" s="T578">ребенок.[NOM]-3SG</ta>
            <ta e="T580" id="Seg_1245" s="T579">наружу</ta>
            <ta e="T581" id="Seg_1246" s="T580">выйти-CVB</ta>
            <ta e="T582" id="Seg_1247" s="T581">поводок.в.оленьей.упряжи-DU.[NOM]-3SG</ta>
            <ta e="T583" id="Seg_1248" s="T582">%%-INSTR</ta>
            <ta e="T584" id="Seg_1249" s="T583">мазать-MULO-INF-начать-PST.NAR-INFER-3SG.O</ta>
            <ta e="T585" id="Seg_1250" s="T584">муж.[NOM]</ta>
            <ta e="T586" id="Seg_1251" s="T585">так</ta>
            <ta e="T587" id="Seg_1252" s="T586">сказать-HAB-3SG.O</ta>
            <ta e="T588" id="Seg_1253" s="T587">ведьма.[NOM]</ta>
            <ta e="T589" id="Seg_1254" s="T588">ты.ALL</ta>
            <ta e="T590" id="Seg_1255" s="T589">INFER</ta>
            <ta e="T591" id="Seg_1256" s="T590">прийти-FUT-INFER.[3SG.S]</ta>
            <ta e="T592" id="Seg_1257" s="T591">ребенок.[NOM]-3SG</ta>
            <ta e="T593" id="Seg_1258" s="T592">вперёд</ta>
            <ta e="T594" id="Seg_1259" s="T593">стать-PST.NAR.[3SG.S]</ta>
            <ta e="T595" id="Seg_1260" s="T594">всегда</ta>
            <ta e="T596" id="Seg_1261" s="T595">мазать-IPFV-CO-3SG.O</ta>
            <ta e="T597" id="Seg_1262" s="T596">олень-DU.[NOM]-3SG</ta>
            <ta e="T598" id="Seg_1263" s="T597">схватить-PST.NAR-3SG.O</ta>
            <ta e="T599" id="Seg_1264" s="T598">нарта.[NOM]</ta>
            <ta e="T600" id="Seg_1265" s="T599">запрячь-MOM-PST.NAR-INFER-3SG.O</ta>
            <ta e="T601" id="Seg_1266" s="T600">отправиться-PST.NAR.[3SG.S]</ta>
            <ta e="T602" id="Seg_1267" s="T601">тронуться-PST.NAR.[3SG.S]</ta>
            <ta e="T603" id="Seg_1268" s="T602">ведьма.[NOM]</ta>
            <ta e="T604" id="Seg_1269" s="T603">побежать-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T605" id="Seg_1270" s="T604">человек-EP-ALL</ta>
            <ta e="T606" id="Seg_1271" s="T605">прийти-CVB</ta>
            <ta e="T607" id="Seg_1272" s="T606">лицо-ACC-3SG</ta>
            <ta e="T608" id="Seg_1273" s="T607">коготь-TR-INF-начать-PST.NAR-3SG.O</ta>
            <ta e="T609" id="Seg_1274" s="T608">сам.3SG.[NOM]</ta>
            <ta e="T610" id="Seg_1275" s="T609">так</ta>
            <ta e="T611" id="Seg_1276" s="T610">сказать.[3SG.S]</ta>
            <ta e="T612" id="Seg_1277" s="T611">муж.[NOM]-1SG</ta>
            <ta e="T613" id="Seg_1278" s="T612">муж.[NOM]-1SG</ta>
            <ta e="T614" id="Seg_1279" s="T613">человек-EP-GEN</ta>
            <ta e="T615" id="Seg_1280" s="T614">нарта-ILL</ta>
            <ta e="T616" id="Seg_1281" s="T615">сесть.в.транспорт-PST.NAR.[3SG.S]</ta>
            <ta e="T617" id="Seg_1282" s="T616">домой</ta>
            <ta e="T618" id="Seg_1283" s="T617">прийти-PST.NAR-3DU.S</ta>
            <ta e="T619" id="Seg_1284" s="T618">чум-ILL</ta>
            <ta e="T620" id="Seg_1285" s="T619">войти-PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T621" id="Seg_1286" s="T620">муж-GEN</ta>
            <ta e="T622" id="Seg_1287" s="T621">лицо.[NOM]</ta>
            <ta e="T623" id="Seg_1288" s="T622">так</ta>
            <ta e="T624" id="Seg_1289" s="T623">коготь-TR-CO-3SG.O</ta>
            <ta e="T625" id="Seg_1290" s="T624">дедушка.[NOM]-1SG</ta>
            <ta e="T626" id="Seg_1291" s="T625">дедушка.[NOM]-1SG</ta>
            <ta e="T627" id="Seg_1292" s="T626">старуха-GEN</ta>
            <ta e="T628" id="Seg_1293" s="T627">лицо.[NOM]</ta>
            <ta e="T629" id="Seg_1294" s="T628">коготь-TR-CO-3SG.O</ta>
            <ta e="T630" id="Seg_1295" s="T629">бабушка.[NOM]-1SG</ta>
            <ta e="T631" id="Seg_1296" s="T630">бабушка.[NOM]-1SG</ta>
            <ta e="T632" id="Seg_1297" s="T631">человек-EP-PL.[NOM]</ta>
            <ta e="T633" id="Seg_1298" s="T632">ведьма-INSTR</ta>
            <ta e="T634" id="Seg_1299" s="T633">настолько</ta>
            <ta e="T635" id="Seg_1300" s="T634">надоесть-PST.NAR-3PL</ta>
            <ta e="T636" id="Seg_1301" s="T635">место.для.собрания.[NOM]</ta>
            <ta e="T637" id="Seg_1302" s="T636">сделать-TR-IPFV-3PL</ta>
            <ta e="T638" id="Seg_1303" s="T637">как</ta>
            <ta e="T639" id="Seg_1304" s="T638">ведьма-ACC</ta>
            <ta e="T640" id="Seg_1305" s="T639">вниз</ta>
            <ta e="T641" id="Seg_1306" s="T640">ударить-3PL</ta>
            <ta e="T642" id="Seg_1307" s="T641">так</ta>
            <ta e="T643" id="Seg_1308" s="T642">положить-PST.NAR-3PL</ta>
            <ta e="T644" id="Seg_1309" s="T643">огонь-ILL</ta>
            <ta e="T645" id="Seg_1310" s="T644">толкнуть-FUT-3PL</ta>
            <ta e="T646" id="Seg_1311" s="T645">старуха.[NOM]</ta>
            <ta e="T647" id="Seg_1312" s="T646">большой</ta>
            <ta e="T648" id="Seg_1313" s="T647">печень-PL-EP-ADJZ</ta>
            <ta e="T649" id="Seg_1314" s="T648">котёл.[NOM]</ta>
            <ta e="T650" id="Seg_1315" s="T649">на.огонь</ta>
            <ta e="T651" id="Seg_1316" s="T650">повесить-PST.NAR-INFER-3SG.O</ta>
            <ta e="T652" id="Seg_1317" s="T651">свариться-CAUS-INF-начать-PST.NAR-3SG.O</ta>
            <ta e="T653" id="Seg_1318" s="T652">сноха.[NOM]</ta>
            <ta e="T654" id="Seg_1319" s="T653">котёл.[NOM]</ta>
            <ta e="T655" id="Seg_1320" s="T654">смешать-IMP.2SG.O</ta>
            <ta e="T656" id="Seg_1321" s="T655">тот</ta>
            <ta e="T657" id="Seg_1322" s="T656">женщина.[NOM]</ta>
            <ta e="T658" id="Seg_1323" s="T657">все.время</ta>
            <ta e="T659" id="Seg_1324" s="T658">сидеть.[3SG.S]</ta>
            <ta e="T660" id="Seg_1325" s="T659">ведьма.[NOM]</ta>
            <ta e="T661" id="Seg_1326" s="T660">сила.[NOM]-1SG</ta>
            <ta e="T662" id="Seg_1327" s="T661">быть-CO-2SG.S</ta>
            <ta e="T663" id="Seg_1328" s="T662">котёл.[NOM]</ta>
            <ta e="T664" id="Seg_1329" s="T663">смешать-IMP.2SG.O</ta>
            <ta e="T665" id="Seg_1330" s="T664">ведьма.[NOM]</ta>
            <ta e="T666" id="Seg_1331" s="T665">побежать-PST.NAR.[3SG.S]</ta>
            <ta e="T667" id="Seg_1332" s="T666">котёл-ACC</ta>
            <ta e="T668" id="Seg_1333" s="T667">едва</ta>
            <ta e="T669" id="Seg_1334" s="T668">смешать-INF-попытаться-PST.NAR-3SG.O</ta>
            <ta e="T670" id="Seg_1335" s="T669">смешать-INF-собраться-PST.NAR-3SG.O</ta>
            <ta e="T671" id="Seg_1336" s="T670">далеко</ta>
            <ta e="T672" id="Seg_1337" s="T671">быть-CO.[3SG.S]</ta>
            <ta e="T673" id="Seg_1338" s="T672">огонь.[NOM]</ta>
            <ta e="T674" id="Seg_1339" s="T673">тот</ta>
            <ta e="T675" id="Seg_1340" s="T674">я.ACC</ta>
            <ta e="T676" id="Seg_1341" s="T675">съесть-FUT.[3SG.S]</ta>
            <ta e="T677" id="Seg_1342" s="T676">муж.[NOM]</ta>
            <ta e="T678" id="Seg_1343" s="T677">так</ta>
            <ta e="T679" id="Seg_1344" s="T678">сказать-HAB-3SG.O</ta>
            <ta e="T681" id="Seg_1345" s="T680">ведьма.[NOM]</ta>
            <ta e="T682" id="Seg_1346" s="T681">к.очагу</ta>
            <ta e="T683" id="Seg_1347" s="T682">едва</ta>
            <ta e="T684" id="Seg_1348" s="T683">наступить-TR-PST.NAR.[3SG.S]</ta>
            <ta e="T685" id="Seg_1349" s="T684">огонь-ILL</ta>
            <ta e="T686" id="Seg_1350" s="T685">упасть-PST.NAR.[3SG.S]</ta>
            <ta e="T687" id="Seg_1351" s="T686">упасть-ACTN-LOC-OBL.3SG</ta>
            <ta e="T688" id="Seg_1352" s="T687">так</ta>
            <ta e="T689" id="Seg_1353" s="T688">сказать-PST.NAR-3SG.O</ta>
            <ta e="T690" id="Seg_1354" s="T689">отсюда</ta>
            <ta e="T691" id="Seg_1355" s="T690">вперёд</ta>
            <ta e="T692" id="Seg_1356" s="T691">человек-EP-PL.[NOM]</ta>
            <ta e="T693" id="Seg_1357" s="T692">умереть-INF-начать-MULS-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T572" id="Seg_1358" s="T571">n-n:case</ta>
            <ta e="T573" id="Seg_1359" s="T572">v-v:tense-v:pn</ta>
            <ta e="T574" id="Seg_1360" s="T573">n-n&gt;adj</ta>
            <ta e="T575" id="Seg_1361" s="T574">n-n:num</ta>
            <ta e="T576" id="Seg_1362" s="T575">n-n:case-n:poss</ta>
            <ta e="T577" id="Seg_1363" s="T576">v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T578" id="Seg_1364" s="T577">n-n:obl.poss-n:case</ta>
            <ta e="T579" id="Seg_1365" s="T578">n-n:case-n:poss</ta>
            <ta e="T580" id="Seg_1366" s="T579">adv</ta>
            <ta e="T581" id="Seg_1367" s="T580">v-v&gt;adv</ta>
            <ta e="T582" id="Seg_1368" s="T581">n-n:num-n:case-n:poss</ta>
            <ta e="T583" id="Seg_1369" s="T582">n-n:case</ta>
            <ta e="T584" id="Seg_1370" s="T583">v-v&gt;v-v:inf-v-v:tense-v:mood-v:pn</ta>
            <ta e="T585" id="Seg_1371" s="T584">n-n:case</ta>
            <ta e="T586" id="Seg_1372" s="T585">adv</ta>
            <ta e="T587" id="Seg_1373" s="T586">v-v&gt;v-v:pn</ta>
            <ta e="T588" id="Seg_1374" s="T587">n-n:case</ta>
            <ta e="T589" id="Seg_1375" s="T588">pers</ta>
            <ta e="T590" id="Seg_1376" s="T589">ptcl</ta>
            <ta e="T591" id="Seg_1377" s="T590">v-v:tense-v:mood-v:pn</ta>
            <ta e="T592" id="Seg_1378" s="T591">n-n:case-n:poss</ta>
            <ta e="T593" id="Seg_1379" s="T592">adv</ta>
            <ta e="T594" id="Seg_1380" s="T593">v-v:tense-v:pn</ta>
            <ta e="T595" id="Seg_1381" s="T594">adv</ta>
            <ta e="T596" id="Seg_1382" s="T595">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T597" id="Seg_1383" s="T596">n-n:num-n:case-n:poss</ta>
            <ta e="T598" id="Seg_1384" s="T597">v-v:tense-v:pn</ta>
            <ta e="T599" id="Seg_1385" s="T598">n-n:case</ta>
            <ta e="T600" id="Seg_1386" s="T599">v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T601" id="Seg_1387" s="T600">v-v:tense-v:pn</ta>
            <ta e="T602" id="Seg_1388" s="T601">v-v:tense-v:pn</ta>
            <ta e="T603" id="Seg_1389" s="T602">n-n:case</ta>
            <ta e="T604" id="Seg_1390" s="T603">v-v:tense-v:mood-v:pn</ta>
            <ta e="T605" id="Seg_1391" s="T604">n-n:ins-n:case</ta>
            <ta e="T606" id="Seg_1392" s="T605">v-v&gt;adv</ta>
            <ta e="T607" id="Seg_1393" s="T606">n-n:case-n:poss</ta>
            <ta e="T608" id="Seg_1394" s="T607">n-n&gt;v-v:inf-v-v:tense-v:pn</ta>
            <ta e="T609" id="Seg_1395" s="T608">emphpro-n:case</ta>
            <ta e="T610" id="Seg_1396" s="T609">adv</ta>
            <ta e="T611" id="Seg_1397" s="T610">v-v:pn</ta>
            <ta e="T612" id="Seg_1398" s="T611">n-n:case-n:poss</ta>
            <ta e="T613" id="Seg_1399" s="T612">n-n:case-n:poss</ta>
            <ta e="T614" id="Seg_1400" s="T613">n-n:ins-n:case</ta>
            <ta e="T615" id="Seg_1401" s="T614">n-n:case</ta>
            <ta e="T616" id="Seg_1402" s="T615">v-v:tense-v:pn</ta>
            <ta e="T617" id="Seg_1403" s="T616">adv</ta>
            <ta e="T618" id="Seg_1404" s="T617">v-v:tense-v:pn</ta>
            <ta e="T619" id="Seg_1405" s="T618">n-n:case</ta>
            <ta e="T620" id="Seg_1406" s="T619">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T621" id="Seg_1407" s="T620">n-n:case</ta>
            <ta e="T622" id="Seg_1408" s="T621">n-n:case</ta>
            <ta e="T623" id="Seg_1409" s="T622">adv</ta>
            <ta e="T624" id="Seg_1410" s="T623">n-n&gt;v-v:ins-v:pn</ta>
            <ta e="T625" id="Seg_1411" s="T624">n-n:case-n:poss</ta>
            <ta e="T626" id="Seg_1412" s="T625">n-n:case-n:poss</ta>
            <ta e="T627" id="Seg_1413" s="T626">n-n:case</ta>
            <ta e="T628" id="Seg_1414" s="T627">n-n:case</ta>
            <ta e="T629" id="Seg_1415" s="T628">n-n&gt;v-v:ins-v:pn</ta>
            <ta e="T630" id="Seg_1416" s="T629">n-n:case-n:poss</ta>
            <ta e="T631" id="Seg_1417" s="T630">n-n:case-n:poss</ta>
            <ta e="T632" id="Seg_1418" s="T631">n-n:ins-n:num-n:case</ta>
            <ta e="T633" id="Seg_1419" s="T632">n-n:case</ta>
            <ta e="T634" id="Seg_1420" s="T633">adv</ta>
            <ta e="T635" id="Seg_1421" s="T634">v-v:tense-v:pn</ta>
            <ta e="T636" id="Seg_1422" s="T635">n-n:case</ta>
            <ta e="T637" id="Seg_1423" s="T636">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T638" id="Seg_1424" s="T637">interrog</ta>
            <ta e="T639" id="Seg_1425" s="T638">n-n:case</ta>
            <ta e="T640" id="Seg_1426" s="T639">adv</ta>
            <ta e="T641" id="Seg_1427" s="T640">v-v:pn</ta>
            <ta e="T642" id="Seg_1428" s="T641">adv</ta>
            <ta e="T643" id="Seg_1429" s="T642">v-v:tense-v:pn</ta>
            <ta e="T644" id="Seg_1430" s="T643">n-n:case</ta>
            <ta e="T645" id="Seg_1431" s="T644">v-v:tense-v:pn</ta>
            <ta e="T646" id="Seg_1432" s="T645">n-n:case</ta>
            <ta e="T647" id="Seg_1433" s="T646">adj</ta>
            <ta e="T648" id="Seg_1434" s="T647">n-n:num-n:ins-n&gt;adj</ta>
            <ta e="T649" id="Seg_1435" s="T648">n-n:case</ta>
            <ta e="T650" id="Seg_1436" s="T649">adv</ta>
            <ta e="T651" id="Seg_1437" s="T650">v-v:tense-v:mood-v:pn</ta>
            <ta e="T652" id="Seg_1438" s="T651">v-v&gt;v-v:inf-v-v:tense-v:pn</ta>
            <ta e="T653" id="Seg_1439" s="T652">n-n:case</ta>
            <ta e="T654" id="Seg_1440" s="T653">n-n:case</ta>
            <ta e="T655" id="Seg_1441" s="T654">v-v:mood.pn</ta>
            <ta e="T656" id="Seg_1442" s="T655">dem</ta>
            <ta e="T657" id="Seg_1443" s="T656">n-n:case</ta>
            <ta e="T658" id="Seg_1444" s="T657">adv</ta>
            <ta e="T659" id="Seg_1445" s="T658">v-v:pn</ta>
            <ta e="T660" id="Seg_1446" s="T659">n-n:case</ta>
            <ta e="T661" id="Seg_1447" s="T660">n-n:case-n:poss</ta>
            <ta e="T662" id="Seg_1448" s="T661">v-v:ins-v:pn</ta>
            <ta e="T663" id="Seg_1449" s="T662">n-n:case</ta>
            <ta e="T664" id="Seg_1450" s="T663">v-v:mood.pn</ta>
            <ta e="T665" id="Seg_1451" s="T664">n-n:case</ta>
            <ta e="T666" id="Seg_1452" s="T665">v-v:tense-v:pn</ta>
            <ta e="T667" id="Seg_1453" s="T666">n-n:case</ta>
            <ta e="T668" id="Seg_1454" s="T667">conj</ta>
            <ta e="T669" id="Seg_1455" s="T668">v-v:inf-v-v:tense-v:pn</ta>
            <ta e="T670" id="Seg_1456" s="T669">v-v:inf-v-v:tense-v:pn</ta>
            <ta e="T671" id="Seg_1457" s="T670">adv</ta>
            <ta e="T672" id="Seg_1458" s="T671">v-v:ins-v:pn</ta>
            <ta e="T673" id="Seg_1459" s="T672">n-n:case</ta>
            <ta e="T674" id="Seg_1460" s="T673">dem</ta>
            <ta e="T675" id="Seg_1461" s="T674">pers</ta>
            <ta e="T676" id="Seg_1462" s="T675">v-v:tense-v:pn</ta>
            <ta e="T677" id="Seg_1463" s="T676">n-n:case</ta>
            <ta e="T678" id="Seg_1464" s="T677">adv</ta>
            <ta e="T679" id="Seg_1465" s="T678">v-v&gt;v-v:pn</ta>
            <ta e="T681" id="Seg_1466" s="T680">n-n:case</ta>
            <ta e="T682" id="Seg_1467" s="T681">adv</ta>
            <ta e="T683" id="Seg_1468" s="T682">conj</ta>
            <ta e="T684" id="Seg_1469" s="T683">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T685" id="Seg_1470" s="T684">n-n:case</ta>
            <ta e="T686" id="Seg_1471" s="T685">v-v:tense-v:pn</ta>
            <ta e="T687" id="Seg_1472" s="T686">v-v&gt;n-n:case-n:obl.poss</ta>
            <ta e="T688" id="Seg_1473" s="T687">adv</ta>
            <ta e="T689" id="Seg_1474" s="T688">v-v:tense-v:pn</ta>
            <ta e="T690" id="Seg_1475" s="T689">adv</ta>
            <ta e="T691" id="Seg_1476" s="T690">adv</ta>
            <ta e="T692" id="Seg_1477" s="T691">n-n:ins-n:num-n:case</ta>
            <ta e="T693" id="Seg_1478" s="T692">v-v:inf-v-v&gt;v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T572" id="Seg_1479" s="T571">n</ta>
            <ta e="T573" id="Seg_1480" s="T572">v</ta>
            <ta e="T574" id="Seg_1481" s="T573">adj</ta>
            <ta e="T575" id="Seg_1482" s="T574">n</ta>
            <ta e="T576" id="Seg_1483" s="T575">v</ta>
            <ta e="T577" id="Seg_1484" s="T576">v</ta>
            <ta e="T578" id="Seg_1485" s="T577">n</ta>
            <ta e="T579" id="Seg_1486" s="T578">n</ta>
            <ta e="T580" id="Seg_1487" s="T579">adv</ta>
            <ta e="T581" id="Seg_1488" s="T580">adv</ta>
            <ta e="T582" id="Seg_1489" s="T581">n</ta>
            <ta e="T583" id="Seg_1490" s="T582">n</ta>
            <ta e="T584" id="Seg_1491" s="T583">v</ta>
            <ta e="T585" id="Seg_1492" s="T584">n</ta>
            <ta e="T586" id="Seg_1493" s="T585">adv</ta>
            <ta e="T587" id="Seg_1494" s="T586">v</ta>
            <ta e="T588" id="Seg_1495" s="T587">n</ta>
            <ta e="T589" id="Seg_1496" s="T588">pers</ta>
            <ta e="T590" id="Seg_1497" s="T589">ptcl</ta>
            <ta e="T591" id="Seg_1498" s="T590">v</ta>
            <ta e="T592" id="Seg_1499" s="T591">n</ta>
            <ta e="T593" id="Seg_1500" s="T592">adv</ta>
            <ta e="T594" id="Seg_1501" s="T593">v</ta>
            <ta e="T595" id="Seg_1502" s="T594">adv</ta>
            <ta e="T596" id="Seg_1503" s="T595">dem</ta>
            <ta e="T597" id="Seg_1504" s="T596">n</ta>
            <ta e="T598" id="Seg_1505" s="T597">v</ta>
            <ta e="T599" id="Seg_1506" s="T598">n</ta>
            <ta e="T600" id="Seg_1507" s="T599">v</ta>
            <ta e="T601" id="Seg_1508" s="T600">v</ta>
            <ta e="T602" id="Seg_1509" s="T601">v</ta>
            <ta e="T603" id="Seg_1510" s="T602">n</ta>
            <ta e="T604" id="Seg_1511" s="T603">v</ta>
            <ta e="T605" id="Seg_1512" s="T604">n</ta>
            <ta e="T606" id="Seg_1513" s="T605">adv</ta>
            <ta e="T607" id="Seg_1514" s="T606">n</ta>
            <ta e="T608" id="Seg_1515" s="T607">v</ta>
            <ta e="T609" id="Seg_1516" s="T608">emphpro</ta>
            <ta e="T610" id="Seg_1517" s="T609">adv</ta>
            <ta e="T611" id="Seg_1518" s="T610">v</ta>
            <ta e="T612" id="Seg_1519" s="T611">n</ta>
            <ta e="T613" id="Seg_1520" s="T612">n</ta>
            <ta e="T614" id="Seg_1521" s="T613">n</ta>
            <ta e="T615" id="Seg_1522" s="T614">n</ta>
            <ta e="T616" id="Seg_1523" s="T615">v</ta>
            <ta e="T617" id="Seg_1524" s="T616">adv</ta>
            <ta e="T618" id="Seg_1525" s="T617">v</ta>
            <ta e="T619" id="Seg_1526" s="T618">n</ta>
            <ta e="T620" id="Seg_1527" s="T619">v</ta>
            <ta e="T621" id="Seg_1528" s="T620">n</ta>
            <ta e="T622" id="Seg_1529" s="T621">n</ta>
            <ta e="T623" id="Seg_1530" s="T622">adv</ta>
            <ta e="T624" id="Seg_1531" s="T623">n</ta>
            <ta e="T625" id="Seg_1532" s="T624">n</ta>
            <ta e="T626" id="Seg_1533" s="T625">n</ta>
            <ta e="T627" id="Seg_1534" s="T626">n</ta>
            <ta e="T628" id="Seg_1535" s="T627">n</ta>
            <ta e="T629" id="Seg_1536" s="T628">n</ta>
            <ta e="T630" id="Seg_1537" s="T629">n</ta>
            <ta e="T631" id="Seg_1538" s="T630">n</ta>
            <ta e="T632" id="Seg_1539" s="T631">n</ta>
            <ta e="T633" id="Seg_1540" s="T632">n</ta>
            <ta e="T634" id="Seg_1541" s="T633">adv</ta>
            <ta e="T635" id="Seg_1542" s="T634">v</ta>
            <ta e="T636" id="Seg_1543" s="T635">n</ta>
            <ta e="T637" id="Seg_1544" s="T636">v</ta>
            <ta e="T638" id="Seg_1545" s="T637">interrog</ta>
            <ta e="T639" id="Seg_1546" s="T638">n</ta>
            <ta e="T640" id="Seg_1547" s="T639">adv</ta>
            <ta e="T641" id="Seg_1548" s="T640">v</ta>
            <ta e="T642" id="Seg_1549" s="T641">adv</ta>
            <ta e="T643" id="Seg_1550" s="T642">v</ta>
            <ta e="T644" id="Seg_1551" s="T643">n</ta>
            <ta e="T645" id="Seg_1552" s="T644">v</ta>
            <ta e="T646" id="Seg_1553" s="T645">n</ta>
            <ta e="T647" id="Seg_1554" s="T646">adj</ta>
            <ta e="T648" id="Seg_1555" s="T647">n</ta>
            <ta e="T649" id="Seg_1556" s="T648">n</ta>
            <ta e="T650" id="Seg_1557" s="T649">adv</ta>
            <ta e="T651" id="Seg_1558" s="T650">v</ta>
            <ta e="T652" id="Seg_1559" s="T651">v</ta>
            <ta e="T653" id="Seg_1560" s="T652">n</ta>
            <ta e="T654" id="Seg_1561" s="T653">n</ta>
            <ta e="T655" id="Seg_1562" s="T654">v</ta>
            <ta e="T656" id="Seg_1563" s="T655">dem</ta>
            <ta e="T657" id="Seg_1564" s="T656">n</ta>
            <ta e="T658" id="Seg_1565" s="T657">adv</ta>
            <ta e="T659" id="Seg_1566" s="T658">v</ta>
            <ta e="T660" id="Seg_1567" s="T659">n</ta>
            <ta e="T661" id="Seg_1568" s="T660">n</ta>
            <ta e="T662" id="Seg_1569" s="T661">v</ta>
            <ta e="T663" id="Seg_1570" s="T662">n</ta>
            <ta e="T664" id="Seg_1571" s="T663">v</ta>
            <ta e="T665" id="Seg_1572" s="T664">n</ta>
            <ta e="T666" id="Seg_1573" s="T665">v</ta>
            <ta e="T667" id="Seg_1574" s="T666">n</ta>
            <ta e="T668" id="Seg_1575" s="T667">conj</ta>
            <ta e="T669" id="Seg_1576" s="T668">v</ta>
            <ta e="T670" id="Seg_1577" s="T669">v</ta>
            <ta e="T671" id="Seg_1578" s="T670">adv</ta>
            <ta e="T672" id="Seg_1579" s="T671">v</ta>
            <ta e="T673" id="Seg_1580" s="T672">n</ta>
            <ta e="T674" id="Seg_1581" s="T673">dem</ta>
            <ta e="T675" id="Seg_1582" s="T674">pers</ta>
            <ta e="T676" id="Seg_1583" s="T675">v</ta>
            <ta e="T677" id="Seg_1584" s="T676">n</ta>
            <ta e="T678" id="Seg_1585" s="T677">adv</ta>
            <ta e="T679" id="Seg_1586" s="T678">v</ta>
            <ta e="T681" id="Seg_1587" s="T680">n</ta>
            <ta e="T682" id="Seg_1588" s="T681">adv</ta>
            <ta e="T683" id="Seg_1589" s="T682">adv</ta>
            <ta e="T684" id="Seg_1590" s="T683">v</ta>
            <ta e="T685" id="Seg_1591" s="T684">n</ta>
            <ta e="T686" id="Seg_1592" s="T685">v</ta>
            <ta e="T687" id="Seg_1593" s="T686">n</ta>
            <ta e="T688" id="Seg_1594" s="T687">adv</ta>
            <ta e="T689" id="Seg_1595" s="T688">v</ta>
            <ta e="T690" id="Seg_1596" s="T689">adv</ta>
            <ta e="T691" id="Seg_1597" s="T690">adv</ta>
            <ta e="T692" id="Seg_1598" s="T691">n</ta>
            <ta e="T693" id="Seg_1599" s="T692">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T575" id="Seg_1600" s="T574">np.h:Th</ta>
            <ta e="T576" id="Seg_1601" s="T575">np.h:Th 0.3.h:Poss</ta>
            <ta e="T578" id="Seg_1602" s="T577">np:Com</ta>
            <ta e="T579" id="Seg_1603" s="T578">np.h:A</ta>
            <ta e="T580" id="Seg_1604" s="T579">adv:G</ta>
            <ta e="T582" id="Seg_1605" s="T581">np:P</ta>
            <ta e="T583" id="Seg_1606" s="T582">np:Ins</ta>
            <ta e="T585" id="Seg_1607" s="T584">np.h:A</ta>
            <ta e="T588" id="Seg_1608" s="T587">np.h:A</ta>
            <ta e="T589" id="Seg_1609" s="T588">pro:G</ta>
            <ta e="T592" id="Seg_1610" s="T591">np.h:A</ta>
            <ta e="T595" id="Seg_1611" s="T594">adv:Time</ta>
            <ta e="T596" id="Seg_1612" s="T595">0.3.h:A</ta>
            <ta e="T597" id="Seg_1613" s="T596">np:Th</ta>
            <ta e="T598" id="Seg_1614" s="T597">0.3.h:A</ta>
            <ta e="T599" id="Seg_1615" s="T598">np:Th</ta>
            <ta e="T600" id="Seg_1616" s="T599">0.3.h:A</ta>
            <ta e="T601" id="Seg_1617" s="T600">0.3.h:A</ta>
            <ta e="T602" id="Seg_1618" s="T601">0.3.h:A</ta>
            <ta e="T603" id="Seg_1619" s="T602">np.h:A</ta>
            <ta e="T605" id="Seg_1620" s="T604">np.h:G</ta>
            <ta e="T607" id="Seg_1621" s="T606">np:P 0.3.h:Poss</ta>
            <ta e="T608" id="Seg_1622" s="T607">0.3.h:A</ta>
            <ta e="T609" id="Seg_1623" s="T608">pro.h:A</ta>
            <ta e="T612" id="Seg_1624" s="T611">0.1.h:Poss</ta>
            <ta e="T613" id="Seg_1625" s="T612">0.1.h:Poss</ta>
            <ta e="T614" id="Seg_1626" s="T613">np.h:Poss</ta>
            <ta e="T615" id="Seg_1627" s="T614">np:G</ta>
            <ta e="T616" id="Seg_1628" s="T615">0.3.h:A</ta>
            <ta e="T617" id="Seg_1629" s="T616">adv:L</ta>
            <ta e="T618" id="Seg_1630" s="T617">0.3.h:A</ta>
            <ta e="T619" id="Seg_1631" s="T618">np:G</ta>
            <ta e="T620" id="Seg_1632" s="T619">0.3.h:A</ta>
            <ta e="T621" id="Seg_1633" s="T620">np:Poss</ta>
            <ta e="T622" id="Seg_1634" s="T621">np:P</ta>
            <ta e="T624" id="Seg_1635" s="T623">0.3.h:A</ta>
            <ta e="T627" id="Seg_1636" s="T626">np:Poss</ta>
            <ta e="T628" id="Seg_1637" s="T627">np:P</ta>
            <ta e="T629" id="Seg_1638" s="T628">0.3.h:A</ta>
            <ta e="T632" id="Seg_1639" s="T631">np.h:E</ta>
            <ta e="T633" id="Seg_1640" s="T632">np:Com</ta>
            <ta e="T636" id="Seg_1641" s="T635">np:Th</ta>
            <ta e="T637" id="Seg_1642" s="T636">0.3.h:A</ta>
            <ta e="T639" id="Seg_1643" s="T638">np.h:P</ta>
            <ta e="T641" id="Seg_1644" s="T640">0.3.h:A</ta>
            <ta e="T643" id="Seg_1645" s="T642">0.3.h:A</ta>
            <ta e="T644" id="Seg_1646" s="T643">np:G</ta>
            <ta e="T645" id="Seg_1647" s="T644">0.3.h:A 0.3.h:P</ta>
            <ta e="T646" id="Seg_1648" s="T645">np.h:A</ta>
            <ta e="T649" id="Seg_1649" s="T648">np:Th</ta>
            <ta e="T650" id="Seg_1650" s="T649">adv:L</ta>
            <ta e="T652" id="Seg_1651" s="T651">0.3.h:A</ta>
            <ta e="T654" id="Seg_1652" s="T653">np:Th</ta>
            <ta e="T655" id="Seg_1653" s="T654">0.2.h:A</ta>
            <ta e="T657" id="Seg_1654" s="T656">np.h:A</ta>
            <ta e="T658" id="Seg_1655" s="T657">adv:Time</ta>
            <ta e="T662" id="Seg_1656" s="T661">0.2.h:Th</ta>
            <ta e="T663" id="Seg_1657" s="T662">np:Th</ta>
            <ta e="T664" id="Seg_1658" s="T663">0.2.h:A</ta>
            <ta e="T665" id="Seg_1659" s="T664">np.h:A</ta>
            <ta e="T667" id="Seg_1660" s="T666">np:Th</ta>
            <ta e="T669" id="Seg_1661" s="T668">0.3.h:A</ta>
            <ta e="T670" id="Seg_1662" s="T669">0.3.h:A</ta>
            <ta e="T672" id="Seg_1663" s="T671">0.3:Th</ta>
            <ta e="T673" id="Seg_1664" s="T672">np:Cau</ta>
            <ta e="T675" id="Seg_1665" s="T674">pro.h:P</ta>
            <ta e="T677" id="Seg_1666" s="T676">np.h:A</ta>
            <ta e="T681" id="Seg_1667" s="T680">np.h:A</ta>
            <ta e="T682" id="Seg_1668" s="T681">adv:G</ta>
            <ta e="T685" id="Seg_1669" s="T684">np:G</ta>
            <ta e="T686" id="Seg_1670" s="T685">0.3.h:P</ta>
            <ta e="T687" id="Seg_1671" s="T686">0.3.h:P</ta>
            <ta e="T689" id="Seg_1672" s="T688">0.3.h:A</ta>
            <ta e="T692" id="Seg_1673" s="T691">np.h:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T573" id="Seg_1674" s="T572">v:pred</ta>
            <ta e="T575" id="Seg_1675" s="T574">np.h:S</ta>
            <ta e="T576" id="Seg_1676" s="T575">np.h:S</ta>
            <ta e="T577" id="Seg_1677" s="T576">v:pred</ta>
            <ta e="T579" id="Seg_1678" s="T578">np.h:S</ta>
            <ta e="T581" id="Seg_1679" s="T579">s:adv</ta>
            <ta e="T582" id="Seg_1680" s="T581">np:O</ta>
            <ta e="T584" id="Seg_1681" s="T583">v:pred</ta>
            <ta e="T585" id="Seg_1682" s="T584">np.h:S</ta>
            <ta e="T587" id="Seg_1683" s="T586">v:pred</ta>
            <ta e="T588" id="Seg_1684" s="T587">np.h:S</ta>
            <ta e="T591" id="Seg_1685" s="T590">v:pred</ta>
            <ta e="T592" id="Seg_1686" s="T591">np.h:S</ta>
            <ta e="T594" id="Seg_1687" s="T593">v:pred</ta>
            <ta e="T596" id="Seg_1688" s="T595">0.3.h:S v:pred</ta>
            <ta e="T597" id="Seg_1689" s="T596">np:O</ta>
            <ta e="T598" id="Seg_1690" s="T597">0.3.h:S v:pred</ta>
            <ta e="T599" id="Seg_1691" s="T598">np:O</ta>
            <ta e="T600" id="Seg_1692" s="T599">0.3.h:S v:pred</ta>
            <ta e="T601" id="Seg_1693" s="T600">0.3.h:S v:pred</ta>
            <ta e="T602" id="Seg_1694" s="T601">0.3.h:S v:pred</ta>
            <ta e="T603" id="Seg_1695" s="T602">np.h:S</ta>
            <ta e="T604" id="Seg_1696" s="T603">v:pred</ta>
            <ta e="T606" id="Seg_1697" s="T604">s:adv</ta>
            <ta e="T607" id="Seg_1698" s="T606">np:O</ta>
            <ta e="T608" id="Seg_1699" s="T607">0.3.h:S v:pred</ta>
            <ta e="T609" id="Seg_1700" s="T608">pro.h:S</ta>
            <ta e="T611" id="Seg_1701" s="T610">v:pred</ta>
            <ta e="T616" id="Seg_1702" s="T615">0.3.h:S v:pred</ta>
            <ta e="T618" id="Seg_1703" s="T617">0.3.h:S v:pred</ta>
            <ta e="T620" id="Seg_1704" s="T619">0.3.h:S v:pred</ta>
            <ta e="T622" id="Seg_1705" s="T621">np:O</ta>
            <ta e="T624" id="Seg_1706" s="T623">0.3.h:S v:pred</ta>
            <ta e="T628" id="Seg_1707" s="T627">np:O</ta>
            <ta e="T629" id="Seg_1708" s="T628">0.3.h:S v:pred</ta>
            <ta e="T632" id="Seg_1709" s="T631">np.h:S</ta>
            <ta e="T635" id="Seg_1710" s="T634">v:pred</ta>
            <ta e="T636" id="Seg_1711" s="T635">np:O</ta>
            <ta e="T637" id="Seg_1712" s="T636">0.3.h:S v:pred</ta>
            <ta e="T641" id="Seg_1713" s="T637">s:compl</ta>
            <ta e="T643" id="Seg_1714" s="T642">0.3.h:S v:pred</ta>
            <ta e="T645" id="Seg_1715" s="T644">0.3.h:S 0.3.h:O v:pred</ta>
            <ta e="T646" id="Seg_1716" s="T645">np.h:S</ta>
            <ta e="T649" id="Seg_1717" s="T648">np:O</ta>
            <ta e="T651" id="Seg_1718" s="T650">v:pred</ta>
            <ta e="T652" id="Seg_1719" s="T651">0.3.h:S v:pred</ta>
            <ta e="T654" id="Seg_1720" s="T653">np:O</ta>
            <ta e="T655" id="Seg_1721" s="T654">0.2.h:S v:pred</ta>
            <ta e="T657" id="Seg_1722" s="T656">np.h:S</ta>
            <ta e="T659" id="Seg_1723" s="T658">v:pred</ta>
            <ta e="T661" id="Seg_1724" s="T660">n:pred</ta>
            <ta e="T662" id="Seg_1725" s="T661">0.2.h:S cop</ta>
            <ta e="T663" id="Seg_1726" s="T662">np:O</ta>
            <ta e="T664" id="Seg_1727" s="T663">0.2.h:S v:pred</ta>
            <ta e="T665" id="Seg_1728" s="T664">np.h:S</ta>
            <ta e="T666" id="Seg_1729" s="T665">v:pred</ta>
            <ta e="T667" id="Seg_1730" s="T666">np:O</ta>
            <ta e="T669" id="Seg_1731" s="T668">0.3.h:S v:pred</ta>
            <ta e="T670" id="Seg_1732" s="T669">0.3.h:S v:pred</ta>
            <ta e="T672" id="Seg_1733" s="T671">0.3:S v:pred</ta>
            <ta e="T673" id="Seg_1734" s="T672">np:S</ta>
            <ta e="T675" id="Seg_1735" s="T674">pro.h:O</ta>
            <ta e="T676" id="Seg_1736" s="T675">v:pred</ta>
            <ta e="T677" id="Seg_1737" s="T676">np.h:S</ta>
            <ta e="T679" id="Seg_1738" s="T678">v:pred</ta>
            <ta e="T681" id="Seg_1739" s="T680">np.h:S</ta>
            <ta e="T684" id="Seg_1740" s="T683">v:pred</ta>
            <ta e="T686" id="Seg_1741" s="T685">0.3.h:S v:pred</ta>
            <ta e="T687" id="Seg_1742" s="T686">s:temp</ta>
            <ta e="T689" id="Seg_1743" s="T688">0.3.h:S v:pred</ta>
            <ta e="T692" id="Seg_1744" s="T691">np.h:S</ta>
            <ta e="T693" id="Seg_1745" s="T692">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T575" id="Seg_1746" s="T574">new</ta>
            <ta e="T576" id="Seg_1747" s="T575">new</ta>
            <ta e="T578" id="Seg_1748" s="T577">accs-inf</ta>
            <ta e="T579" id="Seg_1749" s="T578">giv-active</ta>
            <ta e="T582" id="Seg_1750" s="T581">new</ta>
            <ta e="T585" id="Seg_1751" s="T584">giv-inactive</ta>
            <ta e="T587" id="Seg_1752" s="T586">quot-sp</ta>
            <ta e="T588" id="Seg_1753" s="T587">new-Q</ta>
            <ta e="T589" id="Seg_1754" s="T588">giv-inactive-Q</ta>
            <ta e="T592" id="Seg_1755" s="T591">giv-active</ta>
            <ta e="T596" id="Seg_1756" s="T595">0.giv-active</ta>
            <ta e="T597" id="Seg_1757" s="T596">accs-gen</ta>
            <ta e="T599" id="Seg_1758" s="T598">accs-sit</ta>
            <ta e="T601" id="Seg_1759" s="T600">0.giv-active</ta>
            <ta e="T602" id="Seg_1760" s="T601">0.giv-active</ta>
            <ta e="T603" id="Seg_1761" s="T602">giv-inactive</ta>
            <ta e="T605" id="Seg_1762" s="T604">giv-inactive</ta>
            <ta e="T607" id="Seg_1763" s="T606">accs-inf</ta>
            <ta e="T608" id="Seg_1764" s="T607">0.giv-active</ta>
            <ta e="T609" id="Seg_1765" s="T608">giv-inactive</ta>
            <ta e="T612" id="Seg_1766" s="T611">giv-active-Q</ta>
            <ta e="T613" id="Seg_1767" s="T612">giv-active-Q</ta>
            <ta e="T615" id="Seg_1768" s="T614">accs-gen</ta>
            <ta e="T616" id="Seg_1769" s="T615">0.giv-active</ta>
            <ta e="T618" id="Seg_1770" s="T617">0.giv-active</ta>
            <ta e="T619" id="Seg_1771" s="T618">accs-gen</ta>
            <ta e="T620" id="Seg_1772" s="T619">0.giv-active</ta>
            <ta e="T621" id="Seg_1773" s="T620">giv-inactive</ta>
            <ta e="T622" id="Seg_1774" s="T621">accs-gen</ta>
            <ta e="T624" id="Seg_1775" s="T623">0.giv-active</ta>
            <ta e="T627" id="Seg_1776" s="T626">giv-inactive</ta>
            <ta e="T628" id="Seg_1777" s="T627">accs-gen</ta>
            <ta e="T629" id="Seg_1778" s="T628">0.giv-active</ta>
            <ta e="T632" id="Seg_1779" s="T631">new</ta>
            <ta e="T633" id="Seg_1780" s="T632">giv-active</ta>
            <ta e="T636" id="Seg_1781" s="T635">new</ta>
            <ta e="T637" id="Seg_1782" s="T636">0.giv-active</ta>
            <ta e="T639" id="Seg_1783" s="T638">giv-active</ta>
            <ta e="T643" id="Seg_1784" s="T642">0.giv-active</ta>
            <ta e="T644" id="Seg_1785" s="T643">accs-gen</ta>
            <ta e="T646" id="Seg_1786" s="T645">giv-inactive</ta>
            <ta e="T649" id="Seg_1787" s="T648">new</ta>
            <ta e="T652" id="Seg_1788" s="T651">0.giv-active</ta>
            <ta e="T653" id="Seg_1789" s="T652">giv-inactive-Q</ta>
            <ta e="T654" id="Seg_1790" s="T653">giv-inactive-Q</ta>
            <ta e="T657" id="Seg_1791" s="T656">giv-active</ta>
            <ta e="T660" id="Seg_1792" s="T659">giv-inactive-Q</ta>
            <ta e="T663" id="Seg_1793" s="T662">giv-inactive-Q</ta>
            <ta e="T664" id="Seg_1794" s="T663">0.giv-active-Q</ta>
            <ta e="T665" id="Seg_1795" s="T664">giv-active</ta>
            <ta e="T667" id="Seg_1796" s="T666">giv-inactive</ta>
            <ta e="T669" id="Seg_1797" s="T668">0.giv-active</ta>
            <ta e="T672" id="Seg_1798" s="T671">0.giv-active</ta>
            <ta e="T673" id="Seg_1799" s="T672">giv-active-Q</ta>
            <ta e="T675" id="Seg_1800" s="T674">giv-active-Q</ta>
            <ta e="T677" id="Seg_1801" s="T676">giv-inactive</ta>
            <ta e="T679" id="Seg_1802" s="T678">quot-sp</ta>
            <ta e="T681" id="Seg_1803" s="T680">giv-inactive</ta>
            <ta e="T685" id="Seg_1804" s="T684">giv-inactive</ta>
            <ta e="T686" id="Seg_1805" s="T685">giv-active</ta>
            <ta e="T689" id="Seg_1806" s="T688">quot-sp/0.giv-active</ta>
            <ta e="T692" id="Seg_1807" s="T691">accs-gen</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T572" id="Seg_1808" s="T571">Ведьма.</ta>
            <ta e="T575" id="Seg_1809" s="T572">Жили старик со старухой.</ta>
            <ta e="T578" id="Seg_1810" s="T575">Сын был [у него] с женой.</ta>
            <ta e="T584" id="Seg_1811" s="T578">Сын, выйдя на улицу, стал красить оленью упряжку.</ta>
            <ta e="T591" id="Seg_1812" s="T584">Старик так сказал: "Ведьма к тебе придет."</ta>
            <ta e="T594" id="Seg_1813" s="T591">Сын не обратил внимания.</ta>
            <ta e="T596" id="Seg_1814" s="T594">Дальше мазал [краской].</ta>
            <ta e="T600" id="Seg_1815" s="T596">Оленей поймал, нарты запряг.</ta>
            <ta e="T601" id="Seg_1816" s="T600">Поехал.</ta>
            <ta e="T602" id="Seg_1817" s="T601">Ехал [на нартах].</ta>
            <ta e="T604" id="Seg_1818" s="T602">Ведьма бежала.</ta>
            <ta e="T608" id="Seg_1819" s="T604">К человеку подойдя, стала царапать его лицо.</ta>
            <ta e="T613" id="Seg_1820" s="T608">Сама так говорит: "Муж мой, муж мой!"</ta>
            <ta e="T616" id="Seg_1821" s="T613"> Села на нарты человека.</ta>
            <ta e="T618" id="Seg_1822" s="T616">Домой приехали.</ta>
            <ta e="T620" id="Seg_1823" s="T618">В дом [она] зашла.</ta>
            <ta e="T626" id="Seg_1824" s="T620">Старику лицо так царапает: "Дедушка, дедушка!"</ta>
            <ta e="T631" id="Seg_1825" s="T626">Старухе лицо царапает: "Бабушка, бабушка!" </ta>
            <ta e="T635" id="Seg_1826" s="T631">Людям ведьма так сильно надоела.</ta>
            <ta e="T641" id="Seg_1827" s="T635">Собрание сделали, [чтобы решить] как ведьму убить. </ta>
            <ta e="T645" id="Seg_1828" s="T641">Так решили, [что] в костёр [её] толкнут.</ta>
            <ta e="T651" id="Seg_1829" s="T645">Старуха повесила на огонь большой котёл с рыбьими кишками. </ta>
            <ta e="T652" id="Seg_1830" s="T651">Варить стала.</ta>
            <ta e="T655" id="Seg_1831" s="T652">"Сноха, котёл помешай!" </ta>
            <ta e="T659" id="Seg_1832" s="T655">Та женщина всё сидит.</ta>
            <ta e="T662" id="Seg_1833" s="T659">"Ведьма, ты послушная (?).</ta>
            <ta e="T664" id="Seg_1834" s="T662">Котёл помешай."</ta>
            <ta e="T666" id="Seg_1835" s="T664">Ведьма побежала.</ta>
            <ta e="T672" id="Seg_1836" s="T666">Она едва попыталась помешать котел, [но] он был далеко.</ta>
            <ta e="T676" id="Seg_1837" s="T672">"Огонь меня съест."</ta>
            <ta e="T679" id="Seg_1838" s="T676">Старик так говорит.</ta>
            <ta e="T680" id="Seg_1839" s="T679">"Поближе [подойди] (?)."</ta>
            <ta e="T686" id="Seg_1840" s="T680">Ведьма едва подошла к очагу, [как] в огонь упала.</ta>
            <ta e="T689" id="Seg_1841" s="T686">Когда падала, так сказала.</ta>
            <ta e="T693" id="Seg_1842" s="T689">"После меня люди будут умирать".</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T572" id="Seg_1843" s="T571">A witch.</ta>
            <ta e="T575" id="Seg_1844" s="T572">There lived an old man and his wife.</ta>
            <ta e="T578" id="Seg_1845" s="T575">He and his wife had a son.</ta>
            <ta e="T584" id="Seg_1846" s="T578">The son went outdoors and began to paint his reindeer sledge. </ta>
            <ta e="T591" id="Seg_1847" s="T584">The old man said so: “The witch will come to you”. </ta>
            <ta e="T594" id="Seg_1848" s="T591">The son did not pay attention. </ta>
            <ta e="T596" id="Seg_1849" s="T594">He kept painting. </ta>
            <ta e="T600" id="Seg_1850" s="T596">He caught the reindeer, harnessed them to the sledge.</ta>
            <ta e="T601" id="Seg_1851" s="T600">He left.</ta>
            <ta e="T602" id="Seg_1852" s="T601">He was going on the sledge. </ta>
            <ta e="T604" id="Seg_1853" s="T602">The witch was running. </ta>
            <ta e="T608" id="Seg_1854" s="T604">She came up to the man and started to scratch his face. </ta>
            <ta e="T613" id="Seg_1855" s="T608">And she says so: “My husband, my husband!” </ta>
            <ta e="T616" id="Seg_1856" s="T613">She climbed onto the man’s sledge. </ta>
            <ta e="T618" id="Seg_1857" s="T616">They came home. </ta>
            <ta e="T620" id="Seg_1858" s="T618">She came into the tent.</ta>
            <ta e="T626" id="Seg_1859" s="T620">She is scratching the old man's face: “Grandfather, grandfather!”</ta>
            <ta e="T631" id="Seg_1860" s="T626">She is scratching the old woman's face: “Grandmother, grandmother!”</ta>
            <ta e="T635" id="Seg_1861" s="T631">The people were very much annoyed with the witch. </ta>
            <ta e="T641" id="Seg_1862" s="T635">They gathered to discuss how to kill the witch. </ta>
            <ta e="T645" id="Seg_1863" s="T641">So they decided to push her into the fire. </ta>
            <ta e="T651" id="Seg_1864" s="T645">The old woman hung a big cauldron with fish guts on the fire. </ta>
            <ta e="T652" id="Seg_1865" s="T651">She started to cook. </ta>
            <ta e="T655" id="Seg_1866" s="T652">“Good daughter, stir the cauldron!”</ta>
            <ta e="T659" id="Seg_1867" s="T655">The woman keeps sitting.</ta>
            <ta e="T662" id="Seg_1868" s="T659">“Witch, you are obedient.</ta>
            <ta e="T664" id="Seg_1869" s="T662">Stir in the cauldron.”</ta>
            <ta e="T666" id="Seg_1870" s="T664">The witch ran.</ta>
            <ta e="T672" id="Seg_1871" s="T666">She tried to hardly stir the cauldron, but it was too far. </ta>
            <ta e="T676" id="Seg_1872" s="T672">“The fire will eat me." </ta>
            <ta e="T679" id="Seg_1873" s="T676">The old man says so. </ta>
            <ta e="T680" id="Seg_1874" s="T679">“Come up closer."</ta>
            <ta e="T686" id="Seg_1875" s="T680">No sooner the witch came up to the fireplace as she fell into the fire. </ta>
            <ta e="T689" id="Seg_1876" s="T686">While falling she said so.</ta>
            <ta e="T693" id="Seg_1877" s="T689">“After me people will die.” </ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T572" id="Seg_1878" s="T571">Hexe.</ta>
            <ta e="T575" id="Seg_1879" s="T572">Es lebten ein alter Mann und seine Frau.</ta>
            <ta e="T578" id="Seg_1880" s="T575">Mit seiner Frau hatte er einen Sohn.</ta>
            <ta e="T584" id="Seg_1881" s="T578">Der Sohn ging nach draußen und fing an seinen Rentierschlitten zu bemalen.</ta>
            <ta e="T591" id="Seg_1882" s="T584">Der alte Mann sagte also: "Die Hexe wird zu dir kommen.</ta>
            <ta e="T594" id="Seg_1883" s="T591">Der Sohn schenkte dem keine Beachtung.</ta>
            <ta e="T596" id="Seg_1884" s="T594">Er malte weiter.</ta>
            <ta e="T600" id="Seg_1885" s="T596">Er fing die Rentiere ein, spannte sie vor den Schlitten.</ta>
            <ta e="T601" id="Seg_1886" s="T600">Er fuhr davon.</ta>
            <ta e="T602" id="Seg_1887" s="T601">Er fuhr auf dem Schlitten.</ta>
            <ta e="T604" id="Seg_1888" s="T602">Die Hexe rannte.</ta>
            <ta e="T608" id="Seg_1889" s="T604">Sie kam auf den Mann zu und begann sein Gesicht zu zerkratzen.</ta>
            <ta e="T613" id="Seg_1890" s="T608">Und also sagt sie: "Mein Ehemann, mein Ehemann!"</ta>
            <ta e="T616" id="Seg_1891" s="T613">Sie besteigt den Schlitten des Mannes.</ta>
            <ta e="T618" id="Seg_1892" s="T616">Sie kamen nach Hause.</ta>
            <ta e="T620" id="Seg_1893" s="T618">Sie kam in das Zelt.</ta>
            <ta e="T626" id="Seg_1894" s="T620">Sie zerkratzt das Gesicht des alten Mannes: "Großvater, Großvater!"</ta>
            <ta e="T631" id="Seg_1895" s="T626">Sie zerkratzt das Gesicht der alten Frau: "Großmutter! Großmutter!"</ta>
            <ta e="T635" id="Seg_1896" s="T631">Die Leute waren sehr verärgert über die Hexe.</ta>
            <ta e="T641" id="Seg_1897" s="T635">Sie versammelten sich, um zu beraten, wie die Hexe zu töten sei.</ta>
            <ta e="T645" id="Seg_1898" s="T641">So beschlossen sie, sie ins Feuer zu zerren.</ta>
            <ta e="T651" id="Seg_1899" s="T645">Die alte Frau hängte einen großen Kessel mit Fischinnereien über das Feuer.</ta>
            <ta e="T652" id="Seg_1900" s="T651">Sie fing an zu kochen.</ta>
            <ta e="T655" id="Seg_1901" s="T652">"Liebe Tochter, rühr den Kessel um!"</ta>
            <ta e="T659" id="Seg_1902" s="T655">Die Frau bleibt sitzen.</ta>
            <ta e="T662" id="Seg_1903" s="T659">"Hexe, du bist gehorsam.</ta>
            <ta e="T664" id="Seg_1904" s="T662">Rühr den Kessel um."</ta>
            <ta e="T666" id="Seg_1905" s="T664">Die Hexe rannte.</ta>
            <ta e="T672" id="Seg_1906" s="T666">Sie versuchte den Kessel umzurühren, aber er war zu weit weg.</ta>
            <ta e="T676" id="Seg_1907" s="T672">"Das Feuer wird mich fressen."</ta>
            <ta e="T679" id="Seg_1908" s="T676">Der alte Mann sagte also.</ta>
            <ta e="T680" id="Seg_1909" s="T679">"Komm näher ran."</ta>
            <ta e="T686" id="Seg_1910" s="T680">Kaum näherte sich die Hexe der Feuerstelle, als sie in das Feuer hinein fiel.</ta>
            <ta e="T689" id="Seg_1911" s="T686">Während sie fiel, sagte sie.</ta>
            <ta e="T693" id="Seg_1912" s="T689">"Nach mir werden Menschen sterben".</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T572" id="Seg_1913" s="T571">ведьма</ta>
            <ta e="T575" id="Seg_1914" s="T572">жили старик со старухой</ta>
            <ta e="T578" id="Seg_1915" s="T575">сын был (у них) с женой.</ta>
            <ta e="T584" id="Seg_1916" s="T578">сын на улицу выйдя оленью упряжку (сбрую) краской мазать (красить) стал.</ta>
            <ta e="T591" id="Seg_1917" s="T584">старик так сказал: ведьма к тебе придет</ta>
            <ta e="T594" id="Seg_1918" s="T591">сын не обращает внимания</ta>
            <ta e="T596" id="Seg_1919" s="T594">всё мажет (краской)</ta>
            <ta e="T600" id="Seg_1920" s="T596">оленей поймал нарты запряг.</ta>
            <ta e="T601" id="Seg_1921" s="T600">поехал</ta>
            <ta e="T602" id="Seg_1922" s="T601">едет (на нартах)</ta>
            <ta e="T604" id="Seg_1923" s="T602">ведьма бежит</ta>
            <ta e="T608" id="Seg_1924" s="T604">к человеку подойдя лицо (его) царапать стала</ta>
            <ta e="T613" id="Seg_1925" s="T608">сама так говорит муж мой, муж мой!</ta>
            <ta e="T616" id="Seg_1926" s="T613">человека на нарты села</ta>
            <ta e="T618" id="Seg_1927" s="T616">домой приехали</ta>
            <ta e="T620" id="Seg_1928" s="T618">в дом зашла (ведьма)</ta>
            <ta e="T626" id="Seg_1929" s="T620">старику лицо так царапает дедушка дедушка!</ta>
            <ta e="T631" id="Seg_1930" s="T626">старухино лицо царапает бабушка (моя) бабушка (моя) </ta>
            <ta e="T635" id="Seg_1931" s="T631">людям ведьма до того надоела</ta>
            <ta e="T641" id="Seg_1932" s="T635">собрание сделали (собрали) как ведьму убить </ta>
            <ta e="T645" id="Seg_1933" s="T641">так решили в костёр толкнуть (её)</ta>
            <ta e="T651" id="Seg_1934" s="T645">старуха большой с кишками рыбьими котёл на костёр повесила</ta>
            <ta e="T652" id="Seg_1935" s="T651">варить стала</ta>
            <ta e="T655" id="Seg_1936" s="T652">сноха котёл помешай </ta>
            <ta e="T659" id="Seg_1937" s="T655">та женщина всё сидит</ta>
            <ta e="T662" id="Seg_1938" s="T659">ведьма, ты послушная [исполнительная] есть</ta>
            <ta e="T664" id="Seg_1939" s="T662">котёл помешай</ta>
            <ta e="T666" id="Seg_1940" s="T664">ведьма побежала</ta>
            <ta e="T672" id="Seg_1941" s="T666">котёл но хотела помешать далеко (он) был</ta>
            <ta e="T676" id="Seg_1942" s="T672">огонь меня съест</ta>
            <ta e="T679" id="Seg_1943" s="T676">старик так сказал</ta>
            <ta e="T680" id="Seg_1944" s="T679">поближе подойди</ta>
            <ta e="T686" id="Seg_1945" s="T680">ведьма поближе наступила [хотела наступить] в костёр упала</ta>
            <ta e="T689" id="Seg_1946" s="T686">когда падала так сказала</ta>
            <ta e="T693" id="Seg_1947" s="T689">после меня (в будущем) люди пусть умирают</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T575" id="Seg_1948" s="T572">[OSV:] "iralʼ mɨqäːqi" - "spouses".</ta>
            <ta e="T594" id="Seg_1949" s="T591">[OSV:] "nʼennä ɛsɨqo" - "to stop doing something". </ta>
            <ta e="T641" id="Seg_1950" s="T635">[OSV:] "ıllä qättɨqo" - "to kill".</ta>
            <ta e="T645" id="Seg_1951" s="T641">[OSV:] "pinqo" - "to put", here it has the meaning "to decide".</ta>
            <ta e="T651" id="Seg_1952" s="T645">[OSV:] In the adjective "mɨ̄tɨnɨlʼ" "-n" can be interpreted as the Plural marker but for adjectives ending with "-nɨlʼ" a further research is needed.</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T572" id="Seg_1953" s="T571">Сказка, кот. диктовала Ритта</ta>
            <ta e="T680" id="Seg_1954" s="T679">(2р)</ta>
            <ta e="T693" id="Seg_1955" s="T689">1) ′по̄kӓк сӓ̄[ə]ра, ман jе′нӓ̄сымпах - на улице дождь, я не обращаю внимания; 2) жанна ′чӯра, ме jе′нӓ̄сымпо̄wын - жанна плачет, мы не обращаем внимания; 3) ехать (на чем-нибудь, на нартах) - ′lа̄kаlдʼеkа ′чоңна [ехать надо]; 4) поедем - lа̄kаl′д̂ɛlи.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
