<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KR_196X_RavensAndHares2_flk_afterFW</transcription-name>
         <referenced-file url="KR_196X_RavensAndHares2_flk.wav" />
         <referenced-file url="KR_196X_RavensAndHares2_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KR_196X_RavensAndHares2_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">361</ud-information>
            <ud-information attribute-name="# HIAT:w">235</ud-information>
            <ud-information attribute-name="# e">235</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">5</ud-information>
            <ud-information attribute-name="# HIAT:u">41</ud-information>
            <ud-information attribute-name="# sc">80</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KR">
            <abbreviation>KR</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.92" type="appl" />
         <tli id="T2" time="2.1153333333333335" type="appl" />
         <tli id="T3" time="3.3106666666666666" type="appl" />
         <tli id="T4" time="5.593249876151203" />
         <tli id="T277" time="5.826579727957273" />
         <tli id="T5" time="6.046576445374425" />
         <tli id="T6" time="6.467166666666667" type="appl" />
         <tli id="T7" time="7.2283333333333335" type="appl" />
         <tli id="T8" time="7.9895" type="appl" />
         <tli id="T9" time="8.750666666666667" type="appl" />
         <tli id="T10" time="9.511833333333334" type="appl" />
         <tli id="T11" time="10.273" type="appl" />
         <tli id="T12" time="10.379" type="appl" />
         <tli id="T13" time="11.192444444444444" type="appl" />
         <tli id="T14" time="12.005888888888888" type="appl" />
         <tli id="T15" time="12.819333333333333" type="appl" />
         <tli id="T16" time="13.632777777777777" type="appl" />
         <tli id="T17" time="14.446222222222222" type="appl" />
         <tli id="T18" time="15.259666666666666" type="appl" />
         <tli id="T19" time="16.07311111111111" type="appl" />
         <tli id="T20" time="16.886555555555553" type="appl" />
         <tli id="T21" time="17.7" type="appl" />
         <tli id="T22" time="17.88" type="appl" />
         <tli id="T23" time="18.326666666666664" type="appl" />
         <tli id="T24" time="18.773333333333333" type="appl" />
         <tli id="T25" time="19.22" type="appl" />
         <tli id="T26" time="19.666666666666664" type="appl" />
         <tli id="T27" time="20.113333333333333" type="appl" />
         <tli id="T28" time="20.56" type="appl" />
         <tli id="T29" time="21.146" type="appl" />
         <tli id="T30" time="21.856" type="appl" />
         <tli id="T31" time="22.566" type="appl" />
         <tli id="T32" time="23.276" type="appl" />
         <tli id="T33" time="23.986" type="appl" />
         <tli id="T34" time="24.695999999999998" type="appl" />
         <tli id="T35" time="25.406" type="appl" />
         <tli id="T36" time="25.879" type="appl" />
         <tli id="T37" time="26.457375000000003" type="appl" />
         <tli id="T38" time="27.03575" type="appl" />
         <tli id="T39" time="27.614125" type="appl" />
         <tli id="T40" time="28.192500000000003" type="appl" />
         <tli id="T41" time="28.770875" type="appl" />
         <tli id="T42" time="29.34925" type="appl" />
         <tli id="T43" time="29.927625" type="appl" />
         <tli id="T44" time="30.506" type="appl" />
         <tli id="T45" time="30.88" type="appl" />
         <tli id="T46" time="31.46075" type="appl" />
         <tli id="T47" time="32.0415" type="appl" />
         <tli id="T48" time="32.62225" type="appl" />
         <tli id="T49" time="33.203" type="appl" />
         <tli id="T50" time="33.783750000000005" type="appl" />
         <tli id="T51" time="34.3645" type="appl" />
         <tli id="T52" time="34.94525" type="appl" />
         <tli id="T53" time="35.526" type="appl" />
         <tli id="T54" time="35.586" type="appl" />
         <tli id="T55" time="35.98383333333333" type="appl" />
         <tli id="T56" time="36.38166666666667" type="appl" />
         <tli id="T57" time="36.7795" type="appl" />
         <tli id="T58" time="37.17733333333333" type="appl" />
         <tli id="T59" time="37.57516666666667" type="appl" />
         <tli id="T60" time="37.973" type="appl" />
         <tli id="T61" time="38.179" type="appl" />
         <tli id="T62" time="39.1545" type="appl" />
         <tli id="T63" time="40.13" type="appl" />
         <tli id="T64" time="41.1055" type="appl" />
         <tli id="T65" time="42.080999999999996" type="appl" />
         <tli id="T66" time="43.0565" type="appl" />
         <tli id="T67" time="44.032" type="appl" />
         <tli id="T68" time="47.006" type="appl" />
         <tli id="T69" time="47.91842857142857" type="appl" />
         <tli id="T70" time="48.83085714285714" type="appl" />
         <tli id="T71" time="49.74328571428571" type="appl" />
         <tli id="T72" time="50.65571428571429" type="appl" />
         <tli id="T73" time="51.56814285714286" type="appl" />
         <tli id="T74" time="52.48057142857143" type="appl" />
         <tli id="T75" time="53.393" type="appl" />
         <tli id="T76" time="53.586" type="appl" />
         <tli id="T77" time="54.1806" type="appl" />
         <tli id="T78" time="54.7752" type="appl" />
         <tli id="T79" time="55.3698" type="appl" />
         <tli id="T80" time="55.9644" type="appl" />
         <tli id="T81" time="56.559" type="appl" />
         <tli id="T82" time="56.633" type="appl" />
         <tli id="T83" time="57.3556" type="appl" />
         <tli id="T84" time="58.0782" type="appl" />
         <tli id="T85" time="58.8008" type="appl" />
         <tli id="T86" time="59.5234" type="appl" />
         <tli id="T87" time="60.246" type="appl" />
         <tli id="T88" time="61.293" type="appl" />
         <tli id="T89" time="62.43533333333333" type="appl" />
         <tli id="T90" time="63.577666666666666" type="appl" />
         <tli id="T91" time="64.72" type="appl" />
         <tli id="T92" time="65.406" type="appl" />
         <tli id="T93" time="66.0094" type="appl" />
         <tli id="T94" time="66.61280000000001" type="appl" />
         <tli id="T95" time="67.2162" type="appl" />
         <tli id="T96" time="67.81960000000001" type="appl" />
         <tli id="T97" time="68.423" type="appl" />
         <tli id="T98" time="69.0264" type="appl" />
         <tli id="T99" time="69.6298" type="appl" />
         <tli id="T100" time="70.2332" type="appl" />
         <tli id="T101" time="70.8366" type="appl" />
         <tli id="T102" time="71.44" type="appl" />
         <tli id="T103" time="72.126" type="appl" />
         <tli id="T104" time="72.83933333333334" type="appl" />
         <tli id="T105" time="73.55266666666667" type="appl" />
         <tli id="T106" time="74.266" type="appl" />
         <tli id="T107" time="74.97933333333334" type="appl" />
         <tli id="T108" time="75.69266666666667" type="appl" />
         <tli id="T109" time="76.406" type="appl" />
         <tli id="T110" time="77.11933333333334" type="appl" />
         <tli id="T111" time="77.83266666666667" type="appl" />
         <tli id="T112" time="78.546" type="appl" />
         <tli id="T113" time="79.453" type="appl" />
         <tli id="T114" time="80.0184" type="appl" />
         <tli id="T115" time="80.5838" type="appl" />
         <tli id="T116" time="81.14920000000001" type="appl" />
         <tli id="T117" time="81.7146" type="appl" />
         <tli id="T118" time="82.28" type="appl" />
         <tli id="T119" time="82.339" type="appl" />
         <tli id="T120" time="83.29714285714286" type="appl" />
         <tli id="T121" time="84.25528571428572" type="appl" />
         <tli id="T122" time="85.21342857142858" type="appl" />
         <tli id="T123" time="86.17157142857143" type="appl" />
         <tli id="T124" time="87.12971428571429" type="appl" />
         <tli id="T125" time="88.08785714285715" type="appl" />
         <tli id="T126" time="89.046" type="appl" />
         <tli id="T127" time="90.013" type="appl" />
         <tli id="T128" time="90.42375000000001" type="appl" />
         <tli id="T129" time="90.8345" type="appl" />
         <tli id="T130" time="91.24525" type="appl" />
         <tli id="T131" time="91.656" type="appl" />
         <tli id="T132" time="92.06675000000001" type="appl" />
         <tli id="T133" time="92.4775" type="appl" />
         <tli id="T134" time="92.88825" type="appl" />
         <tli id="T135" time="93.299" type="appl" />
         <tli id="T136" time="94.379" type="appl" />
         <tli id="T137" time="95.0684" type="appl" />
         <tli id="T138" time="95.7578" type="appl" />
         <tli id="T139" time="96.4472" type="appl" />
         <tli id="T140" time="97.1366" type="appl" />
         <tli id="T141" time="97.826" type="appl" />
         <tli id="T142" time="98.106" type="appl" />
         <tli id="T143" time="98.78999999999999" type="appl" />
         <tli id="T144" time="99.47399999999999" type="appl" />
         <tli id="T145" time="100.158" type="appl" />
         <tli id="T146" time="100.842" type="appl" />
         <tli id="T147" time="101.526" type="appl" />
         <tli id="T148" time="103.619" type="appl" />
         <tli id="T149" time="103.99328571428572" type="appl" />
         <tli id="T150" time="104.36757142857142" type="appl" />
         <tli id="T151" time="104.74185714285714" type="appl" />
         <tli id="T152" time="105.11614285714286" type="appl" />
         <tli id="T153" time="105.49042857142858" type="appl" />
         <tli id="T154" time="105.86471428571429" type="appl" />
         <tli id="T155" time="106.239" type="appl" />
         <tli id="T156" time="106.353" type="appl" />
         <tli id="T157" time="106.75975" type="appl" />
         <tli id="T158" time="107.1665" type="appl" />
         <tli id="T159" time="107.57325" type="appl" />
         <tli id="T160" time="107.98" type="appl" />
         <tli id="T161" time="108.066" type="appl" />
         <tli id="T162" time="108.486" type="appl" />
         <tli id="T163" time="108.90599999999999" type="appl" />
         <tli id="T164" time="109.326" type="appl" />
         <tli id="T165" time="109.346" type="appl" />
         <tli id="T166" time="109.846" type="appl" />
         <tli id="T167" time="110.346" type="appl" />
         <tli id="T168" time="110.846" type="appl" />
         <tli id="T169" time="111.346" type="appl" />
         <tli id="T170" time="111.846" type="appl" />
         <tli id="T171" time="111.94" type="appl" />
         <tli id="T172" time="112.63" type="appl" />
         <tli id="T173" time="113.32" type="appl" />
         <tli id="T174" time="114.01" type="appl" />
         <tli id="T175" time="114.7" type="appl" />
         <tli id="T176" time="115.193" type="appl" />
         <tli id="T177" time="115.89533333333333" type="appl" />
         <tli id="T178" time="116.59766666666667" type="appl" />
         <tli id="T179" time="117.3" type="appl" />
         <tli id="T180" time="117.446" type="appl" />
         <tli id="T181" time="117.93171428571428" type="appl" />
         <tli id="T182" time="118.41742857142857" type="appl" />
         <tli id="T183" time="118.90314285714285" type="appl" />
         <tli id="T184" time="119.38885714285715" type="appl" />
         <tli id="T185" time="119.87457142857143" type="appl" />
         <tli id="T186" time="120.36028571428572" type="appl" />
         <tli id="T187" time="120.846" type="appl" />
         <tli id="T188" time="121.026" type="appl" />
         <tli id="T189" time="121.506" type="appl" />
         <tli id="T190" time="121.986" type="appl" />
         <tli id="T191" time="122.466" type="appl" />
         <tli id="T192" time="122.946" type="appl" />
         <tli id="T193" time="123.426" type="appl" />
         <tli id="T194" time="123.726" type="appl" />
         <tli id="T195" time="124.55742857142857" type="appl" />
         <tli id="T196" time="125.38885714285713" type="appl" />
         <tli id="T197" time="126.22028571428571" type="appl" />
         <tli id="T198" time="127.05171428571428" type="appl" />
         <tli id="T199" time="127.88314285714286" type="appl" />
         <tli id="T200" time="128.71457142857142" type="appl" />
         <tli id="T201" time="129.546" type="appl" />
         <tli id="T202" time="130.086" type="appl" />
         <tli id="T203" time="130.441875" type="appl" />
         <tli id="T204" time="130.79775" type="appl" />
         <tli id="T205" time="131.153625" type="appl" />
         <tli id="T206" time="131.5095" type="appl" />
         <tli id="T207" time="131.865375" type="appl" />
         <tli id="T208" time="132.22125" type="appl" />
         <tli id="T209" time="132.577125" type="appl" />
         <tli id="T210" time="132.933" type="appl" />
         <tli id="T211" time="135.746" type="appl" />
         <tli id="T212" time="136.54266666666666" type="appl" />
         <tli id="T213" time="137.33933333333334" type="appl" />
         <tli id="T214" time="138.13600000000002" type="appl" />
         <tli id="T215" time="138.93266666666668" type="appl" />
         <tli id="T216" time="139.72933333333333" type="appl" />
         <tli id="T217" time="140.526" type="appl" />
         <tli id="T218" time="140.953" type="appl" />
         <tli id="T219" time="141.5702" type="appl" />
         <tli id="T220" time="142.1874" type="appl" />
         <tli id="T221" time="142.8046" type="appl" />
         <tli id="T222" time="143.4218" type="appl" />
         <tli id="T223" time="144.039" type="appl" />
         <tli id="T224" time="144.206" type="appl" />
         <tli id="T225" time="145.2225" type="appl" />
         <tli id="T226" time="146.239" type="appl" />
         <tli id="T227" time="146.5" type="appl" />
         <tli id="T228" time="146.91875" type="appl" />
         <tli id="T229" time="147.3375" type="appl" />
         <tli id="T230" time="147.75625" type="appl" />
         <tli id="T231" time="148.175" type="appl" />
         <tli id="T232" time="148.59375" type="appl" />
         <tli id="T233" time="149.0125" type="appl" />
         <tli id="T234" time="149.43125" type="appl" />
         <tli id="T235" time="149.85" type="appl" />
         <tli id="T236" time="150.66333333333333" type="appl" />
         <tli id="T237" time="151.47666666666666" type="appl" />
         <tli id="T238" time="152.29" type="appl" />
         <tli id="T239" time="152.42" type="appl" />
         <tli id="T240" time="153.0722857142857" type="appl" />
         <tli id="T241" time="153.7245714285714" type="appl" />
         <tli id="T242" time="154.37685714285712" type="appl" />
         <tli id="T243" time="155.02914285714286" type="appl" />
         <tli id="T244" time="155.68142857142857" type="appl" />
         <tli id="T245" time="156.33371428571428" type="appl" />
         <tli id="T246" time="156.986" type="appl" />
         <tli id="T247" time="157.199" type="appl" />
         <tli id="T248" time="157.725375" type="appl" />
         <tli id="T249" time="158.25175000000002" type="appl" />
         <tli id="T250" time="158.77812500000002" type="appl" />
         <tli id="T251" time="159.30450000000002" type="appl" />
         <tli id="T252" time="159.830875" type="appl" />
         <tli id="T253" time="160.35725" type="appl" />
         <tli id="T254" time="160.883625" type="appl" />
         <tli id="T255" time="161.41" type="appl" />
         <tli id="T256" time="161.666" type="appl" />
         <tli id="T257" time="162.36666666666667" type="appl" />
         <tli id="T258" time="163.06733333333332" type="appl" />
         <tli id="T259" time="163.768" type="appl" />
         <tli id="T260" time="164.46866666666668" type="appl" />
         <tli id="T261" time="165.16933333333333" type="appl" />
         <tli id="T262" time="165.87" type="appl" />
         <tli id="T263" time="165.92" type="appl" />
         <tli id="T264" time="166.50285714285712" type="appl" />
         <tli id="T265" time="167.0857142857143" type="appl" />
         <tli id="T266" time="167.66857142857143" type="appl" />
         <tli id="T267" time="168.25142857142856" type="appl" />
         <tli id="T268" time="168.8342857142857" type="appl" />
         <tli id="T269" time="169.41714285714286" type="appl" />
         <tli id="T270" time="170.0" type="appl" />
         <tli id="T271" time="171.506" type="appl" />
         <tli id="T272" time="172.47266666666667" type="appl" />
         <tli id="T273" time="173.43933333333334" type="appl" />
         <tli id="T274" time="174.406" type="appl" />
         <tli id="T275" time="175.84" type="appl" />
         <tli id="T276" time="176.493" type="appl" />
         <tli id="T0" time="176.624" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KR"
                      type="t">
         <timeline-fork end="T4" start="T1">
            <tli id="T1.tx.1" />
            <tli id="T1.tx.2" />
            <tli id="T1.tx.3" />
            <tli id="T1.tx.4" />
         </timeline-fork>
         <timeline-fork end="T73" start="T72">
            <tli id="T72.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T4" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <nts id="Seg_3" n="HIAT:ip">(</nts>
                  <nts id="Seg_4" n="HIAT:ip">(</nts>
                  <ats e="T1.tx.1" id="Seg_5" n="HIAT:non-pho" s="T1">KuAI:</ats>
                  <nts id="Seg_6" n="HIAT:ip">)</nts>
                  <nts id="Seg_7" n="HIAT:ip">)</nts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1.tx.2" id="Seg_10" n="HIAT:w" s="T1.tx.1">Рита</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1.tx.3" id="Seg_13" n="HIAT:w" s="T1.tx.2">Кунина</ts>
                  <nts id="Seg_14" n="HIAT:ip">,</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1.tx.4" id="Seg_17" n="HIAT:w" s="T1.tx.3">cказка</ts>
                  <nts id="Seg_18" n="HIAT:ip">;</nts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_21" n="HIAT:w" s="T1.tx.4">говори</ts>
                  <nts id="Seg_22" n="HIAT:ip">.</nts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T11" id="Seg_24" n="sc" s="T277">
               <ts e="T11" id="Seg_26" n="HIAT:u" s="T277">
                  <nts id="Seg_27" n="HIAT:ip">(</nts>
                  <nts id="Seg_28" n="HIAT:ip">(</nts>
                  <ats e="T5" id="Seg_29" n="HIAT:non-pho" s="T277">KR:</ats>
                  <nts id="Seg_30" n="HIAT:ip">)</nts>
                  <nts id="Seg_31" n="HIAT:ip">)</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_34" n="HIAT:w" s="T5">Nʼoma</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_37" n="HIAT:w" s="T6">i</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_40" n="HIAT:w" s="T7">kulä</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_43" n="HIAT:w" s="T8">ämɨsɨqäqı</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_46" n="HIAT:w" s="T9">ilʼɨmpɔːqı</ts>
                  <nts id="Seg_47" n="HIAT:ip">,</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_50" n="HIAT:w" s="T10">ilʼɨmpɔːqı</ts>
                  <nts id="Seg_51" n="HIAT:ip">.</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T21" id="Seg_53" n="sc" s="T12">
               <ts e="T21" id="Seg_55" n="HIAT:u" s="T12">
                  <nts id="Seg_56" n="HIAT:ip">(</nts>
                  <ts e="T13" id="Seg_58" n="HIAT:w" s="T12">Nʼoma=</ts>
                  <nts id="Seg_59" n="HIAT:ip">)</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_62" n="HIAT:w" s="T13">nʼoma</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_64" n="HIAT:ip">(</nts>
                  <ts e="T15" id="Seg_66" n="HIAT:w" s="T14">šottɨ=</ts>
                  <nts id="Seg_67" n="HIAT:ip">)</nts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_70" n="HIAT:w" s="T15">šottɨ</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_73" n="HIAT:w" s="T16">qəlla</ts>
                  <nts id="Seg_74" n="HIAT:ip">,</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_77" n="HIAT:w" s="T17">šottɨ</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_80" n="HIAT:w" s="T18">qənpa</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_83" n="HIAT:w" s="T19">i</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_86" n="HIAT:w" s="T20">šittalä</ts>
                  <nts id="Seg_87" n="HIAT:ip">.</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T28" id="Seg_89" n="sc" s="T22">
               <ts e="T28" id="Seg_91" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_93" n="HIAT:w" s="T22">Ukkɨr</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_96" n="HIAT:w" s="T23">iralaka</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_98" n="HIAT:ip">(</nts>
                  <nts id="Seg_99" n="HIAT:ip">(</nts>
                  <ats e="T25" id="Seg_100" n="HIAT:non-pho" s="T24">…</ats>
                  <nts id="Seg_101" n="HIAT:ip">)</nts>
                  <nts id="Seg_102" n="HIAT:ip">)</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_105" n="HIAT:w" s="T25">ürɨ</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_108" n="HIAT:w" s="T26">qaqlɨ</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_110" n="HIAT:ip">(</nts>
                  <ts e="T28" id="Seg_112" n="HIAT:w" s="T27">üqɨlmmɨntɨtɨ</ts>
                  <nts id="Seg_113" n="HIAT:ip">)</nts>
                  <nts id="Seg_114" n="HIAT:ip">.</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T35" id="Seg_116" n="sc" s="T29">
               <ts e="T35" id="Seg_118" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_120" n="HIAT:w" s="T29">Šittalʼ</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_123" n="HIAT:w" s="T30">qəlla</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_126" n="HIAT:w" s="T31">čʼošɨlaka</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_129" n="HIAT:w" s="T32">iːmmɨntɨtɨ</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_132" n="HIAT:w" s="T33">moqonä</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_135" n="HIAT:w" s="T34">tümpa</ts>
                  <nts id="Seg_136" n="HIAT:ip">.</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T44" id="Seg_138" n="sc" s="T36">
               <ts e="T44" id="Seg_140" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_142" n="HIAT:w" s="T36">Nʼoma</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_145" n="HIAT:w" s="T37">ijaiːtɨ</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_148" n="HIAT:w" s="T38">tamɨ</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_151" n="HIAT:w" s="T39">nɔːt</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_154" n="HIAT:w" s="T40">puːn</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_157" n="HIAT:w" s="T41">kula</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_160" n="HIAT:w" s="T42">ijaiːtɨ</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_163" n="HIAT:w" s="T43">tünnɨnt</ts>
                  <nts id="Seg_164" n="HIAT:ip">.</nts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T53" id="Seg_166" n="sc" s="T45">
               <ts e="T53" id="Seg_168" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_170" n="HIAT:w" s="T45">Šittalʼ</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_172" n="HIAT:ip">(</nts>
                  <ts e="T47" id="Seg_174" n="HIAT:w" s="T46">nʼoma=</ts>
                  <nts id="Seg_175" n="HIAT:ip">)</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_178" n="HIAT:w" s="T47">nʼoman</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_181" n="HIAT:w" s="T48">ijaja</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_184" n="HIAT:w" s="T49">i</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_187" n="HIAT:w" s="T50">kulan</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_190" n="HIAT:w" s="T51">ijaja</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_193" n="HIAT:w" s="T52">pijatuksɨt</ts>
                  <nts id="Seg_194" n="HIAT:ip">.</nts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T60" id="Seg_196" n="sc" s="T54">
               <ts e="T60" id="Seg_198" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_200" n="HIAT:w" s="T54">Kulan</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_203" n="HIAT:w" s="T55">ijaja</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_206" n="HIAT:w" s="T56">moqonä</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_209" n="HIAT:w" s="T57">qəlla</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_212" n="HIAT:w" s="T58">nık</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_215" n="HIAT:w" s="T59">kətɨmpat</ts>
                  <nts id="Seg_216" n="HIAT:ip">.</nts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T67" id="Seg_218" n="sc" s="T61">
               <ts e="T67" id="Seg_220" n="HIAT:u" s="T61">
                  <nts id="Seg_221" n="HIAT:ip">“</nts>
                  <nts id="Seg_222" n="HIAT:ip">(</nts>
                  <ts e="T62" id="Seg_224" n="HIAT:w" s="T61">Tın=</ts>
                  <nts id="Seg_225" n="HIAT:ip">)</nts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_228" n="HIAT:w" s="T62">nʼoma</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_231" n="HIAT:w" s="T63">tɨqa</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_234" n="HIAT:w" s="T64">tɨna</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_237" n="HIAT:w" s="T65">ürlaka</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_240" n="HIAT:w" s="T66">taːtsɨt</ts>
                  <nts id="Seg_241" n="HIAT:ip">.</nts>
                  <nts id="Seg_242" n="HIAT:ip">”</nts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T75" id="Seg_244" n="sc" s="T68">
               <ts e="T75" id="Seg_246" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_248" n="HIAT:w" s="T68">Nʼomaja</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_251" n="HIAT:w" s="T69">i</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_254" n="HIAT:w" s="T70">kulaja</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_257" n="HIAT:w" s="T71">ämɨsɨqäqı</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_259" n="HIAT:ip">(</nts>
                  <ts e="T72.tx.1" id="Seg_261" n="HIAT:w" s="T72">ila</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_264" n="HIAT:w" s="T72.tx.1">-</ts>
                  <nts id="Seg_265" n="HIAT:ip">)</nts>
                  <nts id="Seg_266" n="HIAT:ip">,</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_269" n="HIAT:w" s="T73">ilʼɨmpɔːqı</ts>
                  <nts id="Seg_270" n="HIAT:ip">,</nts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_273" n="HIAT:w" s="T74">ilʼɨmpɔːqı</ts>
                  <nts id="Seg_274" n="HIAT:ip">.</nts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T81" id="Seg_276" n="sc" s="T76">
               <ts e="T81" id="Seg_278" n="HIAT:u" s="T76">
                  <nts id="Seg_279" n="HIAT:ip">(</nts>
                  <ts e="T77" id="Seg_281" n="HIAT:w" s="T76">Nʼoma=</ts>
                  <nts id="Seg_282" n="HIAT:ip">)</nts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_285" n="HIAT:w" s="T77">nʼoma</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_288" n="HIAT:w" s="T78">tɨna</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_291" n="HIAT:w" s="T79">šottɨ</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_294" n="HIAT:w" s="T80">qənpa</ts>
                  <nts id="Seg_295" n="HIAT:ip">.</nts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T87" id="Seg_297" n="sc" s="T82">
               <ts e="T87" id="Seg_299" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_301" n="HIAT:w" s="T82">Ukkɨr</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_304" n="HIAT:w" s="T83">iralaka</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_307" n="HIAT:w" s="T84">čʼošɨ</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_310" n="HIAT:w" s="T85">qaɣlɨ</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_313" n="HIAT:w" s="T86">üqɨlmmɨntɨt</ts>
                  <nts id="Seg_314" n="HIAT:ip">.</nts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T91" id="Seg_316" n="sc" s="T88">
               <ts e="T91" id="Seg_318" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_320" n="HIAT:w" s="T88">Nʼoma</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_323" n="HIAT:w" s="T89">ämɨsɨqäqı</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_326" n="HIAT:w" s="T90">ilʼimpɔːqı</ts>
                  <nts id="Seg_327" n="HIAT:ip">.</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T102" id="Seg_329" n="sc" s="T92">
               <ts e="T102" id="Seg_331" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_333" n="HIAT:w" s="T92">Ukkɨr</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_335" n="HIAT:ip">(</nts>
                  <ts e="T94" id="Seg_337" n="HIAT:w" s="T93">iralaka=</ts>
                  <nts id="Seg_338" n="HIAT:ip">)</nts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_341" n="HIAT:w" s="T94">qənpanɨ</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_344" n="HIAT:w" s="T95">nʼoma</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_347" n="HIAT:w" s="T96">šottɨ</ts>
                  <nts id="Seg_348" n="HIAT:ip">;</nts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_351" n="HIAT:w" s="T97">ukkɨr</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_354" n="HIAT:w" s="T98">iralaka</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_357" n="HIAT:w" s="T99">čʼošɨ</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_360" n="HIAT:w" s="T100">qaɣlɨ</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_363" n="HIAT:w" s="T101">üqɨlmmɨntɨt</ts>
                  <nts id="Seg_364" n="HIAT:ip">.</nts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T112" id="Seg_366" n="sc" s="T103">
               <ts e="T112" id="Seg_368" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_370" n="HIAT:w" s="T103">Nʼoma</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_373" n="HIAT:w" s="T104">qənpa</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_376" n="HIAT:w" s="T105">näčʼčʼa</ts>
                  <nts id="Seg_377" n="HIAT:ip">,</nts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_380" n="HIAT:w" s="T106">šittalä</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_383" n="HIAT:w" s="T107">čʼošɨlaka</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_386" n="HIAT:w" s="T108">orqɨlʼmmɨntɨt</ts>
                  <nts id="Seg_387" n="HIAT:ip">,</nts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_390" n="HIAT:w" s="T109">šittälʼ</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_393" n="HIAT:w" s="T110">moqonä</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_396" n="HIAT:w" s="T111">qənpa</ts>
                  <nts id="Seg_397" n="HIAT:ip">.</nts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T118" id="Seg_399" n="sc" s="T113">
               <ts e="T118" id="Seg_401" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_403" n="HIAT:w" s="T113">Šittal</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_406" n="HIAT:w" s="T114">moqonä</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_409" n="HIAT:w" s="T115">qəlla</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_412" n="HIAT:w" s="T116">ijaiːtɨ</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_415" n="HIAT:w" s="T117">awstɨmpatɨ</ts>
                  <nts id="Seg_416" n="HIAT:ip">.</nts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T126" id="Seg_418" n="sc" s="T119">
               <ts e="T126" id="Seg_420" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_422" n="HIAT:w" s="T119">Ijaiːtɨ</ts>
                  <nts id="Seg_423" n="HIAT:ip">,</nts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_426" n="HIAT:w" s="T120">ukkɨr</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_429" n="HIAT:w" s="T121">ijaja</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_432" n="HIAT:w" s="T122">nʼoman</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_435" n="HIAT:w" s="T123">qulan</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_438" n="HIAT:w" s="T124">ijaja</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_441" n="HIAT:w" s="T125">pijatukkɨɣit</ts>
                  <nts id="Seg_442" n="HIAT:ip">.</nts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T135" id="Seg_444" n="sc" s="T127">
               <ts e="T135" id="Seg_446" n="HIAT:u" s="T127">
                  <nts id="Seg_447" n="HIAT:ip">(</nts>
                  <ts e="T128" id="Seg_449" n="HIAT:w" s="T127">Qula=</ts>
                  <nts id="Seg_450" n="HIAT:ip">)</nts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_453" n="HIAT:w" s="T128">qulan</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_456" n="HIAT:w" s="T129">ijaja</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_459" n="HIAT:w" s="T130">moqona</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_462" n="HIAT:w" s="T131">qəlla</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_465" n="HIAT:w" s="T132">ämɨtɨ</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_468" n="HIAT:w" s="T133">nık</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_471" n="HIAT:w" s="T134">kəttɨmpatɨt</ts>
                  <nts id="Seg_472" n="HIAT:ip">.</nts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T141" id="Seg_474" n="sc" s="T136">
               <ts e="T141" id="Seg_476" n="HIAT:u" s="T136">
                  <nts id="Seg_477" n="HIAT:ip">(</nts>
                  <ts e="T137" id="Seg_479" n="HIAT:w" s="T136">Nʼoma=</ts>
                  <nts id="Seg_480" n="HIAT:ip">)</nts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_483" n="HIAT:w" s="T137">nʼoma</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_486" n="HIAT:w" s="T138">ijaiːtɨ</ts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_489" n="HIAT:w" s="T139">ürɨlakam</ts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_492" n="HIAT:w" s="T140">orqɨlʼpat</ts>
                  <nts id="Seg_493" n="HIAT:ip">.</nts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T147" id="Seg_495" n="sc" s="T142">
               <ts e="T147" id="Seg_497" n="HIAT:u" s="T142">
                  <ts e="T143" id="Seg_499" n="HIAT:w" s="T142">Šittälʼ</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_502" n="HIAT:w" s="T143">qula</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_505" n="HIAT:w" s="T144">toː</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_508" n="HIAT:w" s="T145">mɔːt</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_511" n="HIAT:w" s="T146">qənpa</ts>
                  <nts id="Seg_512" n="HIAT:ip">.</nts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T155" id="Seg_514" n="sc" s="T148">
               <ts e="T155" id="Seg_516" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_518" n="HIAT:w" s="T148">Qula</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_521" n="HIAT:w" s="T149">toː</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_524" n="HIAT:w" s="T150">mɔːt</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_527" n="HIAT:w" s="T151">qənpa</ts>
                  <nts id="Seg_528" n="HIAT:ip">,</nts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_531" n="HIAT:w" s="T152">šittälʼa</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_534" n="HIAT:w" s="T153">nık</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_537" n="HIAT:w" s="T154">kəttɨmpat</ts>
                  <nts id="Seg_538" n="HIAT:ip">.</nts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T160" id="Seg_540" n="sc" s="T156">
               <ts e="T160" id="Seg_542" n="HIAT:u" s="T156">
                  <nts id="Seg_543" n="HIAT:ip">“</nts>
                  <ts e="T157" id="Seg_545" n="HIAT:w" s="T156">Tat</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_548" n="HIAT:w" s="T157">kunɨn</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_551" n="HIAT:w" s="T158">čʼošɨlaka</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_554" n="HIAT:w" s="T159">qosal</ts>
                  <nts id="Seg_555" n="HIAT:ip">?</nts>
                  <nts id="Seg_556" n="HIAT:ip">”</nts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T164" id="Seg_558" n="sc" s="T161">
               <ts e="T164" id="Seg_560" n="HIAT:u" s="T161">
                  <ts e="T162" id="Seg_562" n="HIAT:w" s="T161">Nʼoma</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_565" n="HIAT:w" s="T162">nık</ts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_568" n="HIAT:w" s="T163">kəttɨɣit</ts>
                  <nts id="Seg_569" n="HIAT:ip">.</nts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T170" id="Seg_571" n="sc" s="T165">
               <ts e="T170" id="Seg_573" n="HIAT:u" s="T165">
                  <nts id="Seg_574" n="HIAT:ip">“</nts>
                  <ts e="T166" id="Seg_576" n="HIAT:w" s="T165">Mat</ts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_579" n="HIAT:w" s="T166">šotqɨntɨ</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_582" n="HIAT:w" s="T167">ɨralaka</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_585" n="HIAT:w" s="T168">qontɨrsam</ts>
                  <nts id="Seg_586" n="HIAT:ip">,</nts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_589" n="HIAT:w" s="T169">šittälʼ</ts>
                  <nts id="Seg_590" n="HIAT:ip">.</nts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T175" id="Seg_592" n="sc" s="T171">
               <ts e="T175" id="Seg_594" n="HIAT:u" s="T171">
                  <nts id="Seg_595" n="HIAT:ip">(</nts>
                  <ts e="T172" id="Seg_597" n="HIAT:w" s="T171">Ü-</ts>
                  <nts id="Seg_598" n="HIAT:ip">)</nts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_601" n="HIAT:w" s="T172">ürɨ</ts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_604" n="HIAT:w" s="T173">qaɣl</ts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_607" n="HIAT:w" s="T174">üqɨlmmɨntɨt</ts>
                  <nts id="Seg_608" n="HIAT:ip">.</nts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T179" id="Seg_610" n="sc" s="T176">
               <ts e="T179" id="Seg_612" n="HIAT:u" s="T176">
                  <ts e="T177" id="Seg_614" n="HIAT:w" s="T176">Šittalʼä</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_617" n="HIAT:w" s="T177">šottɨ</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_620" n="HIAT:w" s="T178">qənpa</ts>
                  <nts id="Seg_621" n="HIAT:ip">.</nts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T187" id="Seg_623" n="sc" s="T180">
               <ts e="T187" id="Seg_625" n="HIAT:u" s="T180">
                  <ts e="T181" id="Seg_627" n="HIAT:w" s="T180">Nʼoma</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_630" n="HIAT:w" s="T181">qɔːtɨ</ts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_633" n="HIAT:w" s="T182">pɔːr</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_636" n="HIAT:w" s="T183">sɨrsa</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_639" n="HIAT:w" s="T184">natqɨlpat</ts>
                  <nts id="Seg_640" n="HIAT:ip">,</nts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_643" n="HIAT:w" s="T185">nık</ts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_646" n="HIAT:w" s="T186">kəttɨɣit</ts>
                  <nts id="Seg_647" n="HIAT:ip">.</nts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T193" id="Seg_649" n="sc" s="T188">
               <ts e="T193" id="Seg_651" n="HIAT:u" s="T188">
                  <nts id="Seg_652" n="HIAT:ip">“</nts>
                  <ts e="T189" id="Seg_654" n="HIAT:w" s="T188">Nannɛr</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_657" n="HIAT:w" s="T189">pušqatɔːlla</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_660" n="HIAT:w" s="T190">štɨl</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_663" n="HIAT:w" s="T191">aša</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_666" n="HIAT:w" s="T192">atɨ</ts>
                  <nts id="Seg_667" n="HIAT:ip">.</nts>
                  <nts id="Seg_668" n="HIAT:ip">”</nts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T201" id="Seg_670" n="sc" s="T194">
               <ts e="T201" id="Seg_672" n="HIAT:u" s="T194">
                  <ts e="T195" id="Seg_674" n="HIAT:w" s="T194">Qula</ts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_677" n="HIAT:w" s="T195">qaɣlɨn</ts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_680" n="HIAT:w" s="T196">omtɨıːla</ts>
                  <nts id="Seg_681" n="HIAT:ip">,</nts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_684" n="HIAT:w" s="T197">qüq</ts>
                  <nts id="Seg_685" n="HIAT:ip">,</nts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_688" n="HIAT:w" s="T198">qajlak</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_691" n="HIAT:w" s="T199">awsɨlaka</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_694" n="HIAT:w" s="T200">orqɨlʼpat</ts>
                  <nts id="Seg_695" n="HIAT:ip">.</nts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T210" id="Seg_697" n="sc" s="T202">
               <ts e="T210" id="Seg_699" n="HIAT:u" s="T202">
                  <ts e="T203" id="Seg_701" n="HIAT:w" s="T202">Šittalä</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_704" n="HIAT:w" s="T203">iralaka</ts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_707" n="HIAT:w" s="T204">sukulta</ts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_710" n="HIAT:w" s="T205">piːqɨlla</ts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_713" n="HIAT:w" s="T206">pürüsa</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_716" n="HIAT:w" s="T207">qättɨmpatɨ</ts>
                  <nts id="Seg_717" n="HIAT:ip">,</nts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_720" n="HIAT:w" s="T208">ılla</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_723" n="HIAT:w" s="T209">qulʼčʼimpa</ts>
                  <nts id="Seg_724" n="HIAT:ip">.</nts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T217" id="Seg_726" n="sc" s="T211">
               <ts e="T217" id="Seg_728" n="HIAT:u" s="T211">
                  <nts id="Seg_729" n="HIAT:ip">(</nts>
                  <ts e="T212" id="Seg_731" n="HIAT:w" s="T211">Tä-</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_734" n="HIAT:w" s="T212">im-</ts>
                  <nts id="Seg_735" n="HIAT:ip">)</nts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_738" n="HIAT:w" s="T213">nʼoma</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_741" n="HIAT:w" s="T214">tɨna</ts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_744" n="HIAT:w" s="T215">qənpanə</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_747" n="HIAT:w" s="T216">näčʼčʼa</ts>
                  <nts id="Seg_748" n="HIAT:ip">.</nts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T223" id="Seg_750" n="sc" s="T218">
               <ts e="T223" id="Seg_752" n="HIAT:u" s="T218">
                  <ts e="T219" id="Seg_754" n="HIAT:w" s="T218">Čʼošɨlak</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_757" n="HIAT:w" s="T219">orqɨlmmɨntɨt</ts>
                  <nts id="Seg_758" n="HIAT:ip">,</nts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_761" n="HIAT:w" s="T220">šittälʼ</ts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_764" n="HIAT:w" s="T221">moqonä</ts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_767" n="HIAT:w" s="T222">pakta</ts>
                  <nts id="Seg_768" n="HIAT:ip">.</nts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T226" id="Seg_770" n="sc" s="T224">
               <ts e="T226" id="Seg_772" n="HIAT:u" s="T224">
                  <ts e="T225" id="Seg_774" n="HIAT:w" s="T224">Ijajaiːmtɨ</ts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_777" n="HIAT:w" s="T225">awstɨmpatɨ</ts>
                  <nts id="Seg_778" n="HIAT:ip">.</nts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T238" id="Seg_780" n="sc" s="T227">
               <ts e="T235" id="Seg_782" n="HIAT:u" s="T227">
                  <nts id="Seg_783" n="HIAT:ip">(</nts>
                  <ts e="T228" id="Seg_785" n="HIAT:w" s="T227">Qula=</ts>
                  <nts id="Seg_786" n="HIAT:ip">)</nts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_789" n="HIAT:w" s="T228">qulan</ts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_792" n="HIAT:w" s="T229">ijaja</ts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_795" n="HIAT:w" s="T230">nık</ts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_798" n="HIAT:w" s="T231">kəttɨɣɨt</ts>
                  <nts id="Seg_799" n="HIAT:ip">:</nts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_801" n="HIAT:ip">“</nts>
                  <ts e="T233" id="Seg_803" n="HIAT:w" s="T232">Amä</ts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_806" n="HIAT:w" s="T233">qaj</ts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_809" n="HIAT:w" s="T234">meːtɨt</ts>
                  <nts id="Seg_810" n="HIAT:ip">?</nts>
                  <nts id="Seg_811" n="HIAT:ip">”</nts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T238" id="Seg_814" n="HIAT:u" s="T235">
                  <nts id="Seg_815" n="HIAT:ip">“</nts>
                  <ts e="T236" id="Seg_817" n="HIAT:w" s="T235">Amalɨ</ts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_820" n="HIAT:w" s="T236">čʼošɨlaka</ts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_823" n="HIAT:w" s="T237">tačʼalmmɨntɨt</ts>
                  <nts id="Seg_824" n="HIAT:ip">.</nts>
                  <nts id="Seg_825" n="HIAT:ip">”</nts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T246" id="Seg_827" n="sc" s="T239">
               <ts e="T246" id="Seg_829" n="HIAT:u" s="T239">
                  <ts e="T240" id="Seg_831" n="HIAT:w" s="T239">Šittalʼ</ts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_834" n="HIAT:w" s="T240">nʼoma</ts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_837" n="HIAT:w" s="T241">šittalä</ts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_839" n="HIAT:ip">(</nts>
                  <nts id="Seg_840" n="HIAT:ip">(</nts>
                  <ats e="T243" id="Seg_841" n="HIAT:non-pho" s="T242">…</ats>
                  <nts id="Seg_842" n="HIAT:ip">)</nts>
                  <nts id="Seg_843" n="HIAT:ip">)</nts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_846" n="HIAT:w" s="T243">moqonä</ts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_849" n="HIAT:w" s="T244">qəlla</ts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_852" n="HIAT:w" s="T245">ɔːntalpa</ts>
                  <nts id="Seg_853" n="HIAT:ip">.</nts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T255" id="Seg_855" n="sc" s="T247">
               <ts e="T255" id="Seg_857" n="HIAT:u" s="T247">
                  <ts e="T248" id="Seg_859" n="HIAT:w" s="T247">Nʼoman</ts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_862" n="HIAT:w" s="T248">amɨrnɔːtɨn</ts>
                  <nts id="Seg_863" n="HIAT:ip">,</nts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_866" n="HIAT:w" s="T249">na</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_869" n="HIAT:w" s="T250">puːnɨ</ts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_872" n="HIAT:w" s="T251">nık</ts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_875" n="HIAT:w" s="T252">kəttɨɣɨt</ts>
                  <nts id="Seg_876" n="HIAT:ip">:</nts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_878" n="HIAT:ip">“</nts>
                  <ts e="T254" id="Seg_880" n="HIAT:w" s="T253">Amal</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_883" n="HIAT:w" s="T254">qattɨsa</ts>
                  <nts id="Seg_884" n="HIAT:ip">?</nts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T262" id="Seg_886" n="sc" s="T256">
               <ts e="T262" id="Seg_888" n="HIAT:u" s="T256">
                  <ts e="T257" id="Seg_890" n="HIAT:w" s="T256">Amal</ts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_893" n="HIAT:w" s="T257">qattɨsat</ts>
                  <nts id="Seg_894" n="HIAT:ip">,</nts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_897" n="HIAT:w" s="T258">amantɨtɨ</ts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_900" n="HIAT:w" s="T259">iralaka</ts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_902" n="HIAT:ip">(</nts>
                  <nts id="Seg_903" n="HIAT:ip">(</nts>
                  <ats e="T261" id="Seg_904" n="HIAT:non-pho" s="T260">…</ats>
                  <nts id="Seg_905" n="HIAT:ip">)</nts>
                  <nts id="Seg_906" n="HIAT:ip">)</nts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_909" n="HIAT:w" s="T261">qättɨsɨtɨ</ts>
                  <nts id="Seg_910" n="HIAT:ip">.</nts>
                  <nts id="Seg_911" n="HIAT:ip">”</nts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T270" id="Seg_913" n="sc" s="T263">
               <ts e="T270" id="Seg_915" n="HIAT:u" s="T263">
                  <ts e="T264" id="Seg_917" n="HIAT:w" s="T263">Muntɨk</ts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_920" n="HIAT:w" s="T264">čʼuːrɨla</ts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_923" n="HIAT:w" s="T265">kɨpa</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_926" n="HIAT:w" s="T266">mü</ts>
                  <nts id="Seg_927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_929" n="HIAT:w" s="T267">šiːmɨn</ts>
                  <nts id="Seg_930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_932" n="HIAT:w" s="T268">ılla</ts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_935" n="HIAT:w" s="T269">paktɨsɔːtɨn</ts>
                  <nts id="Seg_936" n="HIAT:ip">.</nts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T274" id="Seg_938" n="sc" s="T271">
               <ts e="T274" id="Seg_940" n="HIAT:u" s="T271">
                  <ts e="T272" id="Seg_942" n="HIAT:w" s="T271">Nʼomaj</ts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_945" n="HIAT:w" s="T272">amʼasɨnɨ</ts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_948" n="HIAT:w" s="T273">amɨrnɔːtɨn</ts>
                  <nts id="Seg_949" n="HIAT:ip">.</nts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T276" id="Seg_951" n="sc" s="T275">
               <ts e="T276" id="Seg_953" n="HIAT:u" s="T275">
                  <ts e="T276" id="Seg_955" n="HIAT:w" s="T275">Всё</ts>
                  <nts id="Seg_956" n="HIAT:ip">.</nts>
                  <nts id="Seg_957" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T4" id="Seg_958" n="sc" s="T1">
               <ts e="T4" id="Seg_960" n="e" s="T1">((KuAI:)) Рита Кунина, cказка; говори. </ts>
            </ts>
            <ts e="T11" id="Seg_961" n="sc" s="T277">
               <ts e="T5" id="Seg_963" n="e" s="T277">((KR:)) </ts>
               <ts e="T6" id="Seg_965" n="e" s="T5">Nʼoma </ts>
               <ts e="T7" id="Seg_967" n="e" s="T6">i </ts>
               <ts e="T8" id="Seg_969" n="e" s="T7">kulä </ts>
               <ts e="T9" id="Seg_971" n="e" s="T8">ämɨsɨqäqı </ts>
               <ts e="T10" id="Seg_973" n="e" s="T9">ilʼɨmpɔːqı, </ts>
               <ts e="T11" id="Seg_975" n="e" s="T10">ilʼɨmpɔːqı. </ts>
            </ts>
            <ts e="T21" id="Seg_976" n="sc" s="T12">
               <ts e="T13" id="Seg_978" n="e" s="T12">(Nʼoma=) </ts>
               <ts e="T14" id="Seg_980" n="e" s="T13">nʼoma </ts>
               <ts e="T15" id="Seg_982" n="e" s="T14">(šottɨ=) </ts>
               <ts e="T16" id="Seg_984" n="e" s="T15">šottɨ </ts>
               <ts e="T17" id="Seg_986" n="e" s="T16">qəlla, </ts>
               <ts e="T18" id="Seg_988" n="e" s="T17">šottɨ </ts>
               <ts e="T19" id="Seg_990" n="e" s="T18">qənpa </ts>
               <ts e="T20" id="Seg_992" n="e" s="T19">i </ts>
               <ts e="T21" id="Seg_994" n="e" s="T20">šittalä. </ts>
            </ts>
            <ts e="T28" id="Seg_995" n="sc" s="T22">
               <ts e="T23" id="Seg_997" n="e" s="T22">Ukkɨr </ts>
               <ts e="T24" id="Seg_999" n="e" s="T23">iralaka </ts>
               <ts e="T25" id="Seg_1001" n="e" s="T24">((…)) </ts>
               <ts e="T26" id="Seg_1003" n="e" s="T25">ürɨ </ts>
               <ts e="T27" id="Seg_1005" n="e" s="T26">qaqlɨ </ts>
               <ts e="T28" id="Seg_1007" n="e" s="T27">(üqɨlmmɨntɨtɨ). </ts>
            </ts>
            <ts e="T35" id="Seg_1008" n="sc" s="T29">
               <ts e="T30" id="Seg_1010" n="e" s="T29">Šittalʼ </ts>
               <ts e="T31" id="Seg_1012" n="e" s="T30">qəlla </ts>
               <ts e="T32" id="Seg_1014" n="e" s="T31">čʼošɨlaka </ts>
               <ts e="T33" id="Seg_1016" n="e" s="T32">iːmmɨntɨtɨ </ts>
               <ts e="T34" id="Seg_1018" n="e" s="T33">moqonä </ts>
               <ts e="T35" id="Seg_1020" n="e" s="T34">tümpa. </ts>
            </ts>
            <ts e="T44" id="Seg_1021" n="sc" s="T36">
               <ts e="T37" id="Seg_1023" n="e" s="T36">Nʼoma </ts>
               <ts e="T38" id="Seg_1025" n="e" s="T37">ijaiːtɨ </ts>
               <ts e="T39" id="Seg_1027" n="e" s="T38">tamɨ </ts>
               <ts e="T40" id="Seg_1029" n="e" s="T39">nɔːt </ts>
               <ts e="T41" id="Seg_1031" n="e" s="T40">puːn </ts>
               <ts e="T42" id="Seg_1033" n="e" s="T41">kula </ts>
               <ts e="T43" id="Seg_1035" n="e" s="T42">ijaiːtɨ </ts>
               <ts e="T44" id="Seg_1037" n="e" s="T43">tünnɨnt. </ts>
            </ts>
            <ts e="T53" id="Seg_1038" n="sc" s="T45">
               <ts e="T46" id="Seg_1040" n="e" s="T45">Šittalʼ </ts>
               <ts e="T47" id="Seg_1042" n="e" s="T46">(nʼoma=) </ts>
               <ts e="T48" id="Seg_1044" n="e" s="T47">nʼoman </ts>
               <ts e="T49" id="Seg_1046" n="e" s="T48">ijaja </ts>
               <ts e="T50" id="Seg_1048" n="e" s="T49">i </ts>
               <ts e="T51" id="Seg_1050" n="e" s="T50">kulan </ts>
               <ts e="T52" id="Seg_1052" n="e" s="T51">ijaja </ts>
               <ts e="T53" id="Seg_1054" n="e" s="T52">pijatuksɨt. </ts>
            </ts>
            <ts e="T60" id="Seg_1055" n="sc" s="T54">
               <ts e="T55" id="Seg_1057" n="e" s="T54">Kulan </ts>
               <ts e="T56" id="Seg_1059" n="e" s="T55">ijaja </ts>
               <ts e="T57" id="Seg_1061" n="e" s="T56">moqonä </ts>
               <ts e="T58" id="Seg_1063" n="e" s="T57">qəlla </ts>
               <ts e="T59" id="Seg_1065" n="e" s="T58">nık </ts>
               <ts e="T60" id="Seg_1067" n="e" s="T59">kətɨmpat. </ts>
            </ts>
            <ts e="T67" id="Seg_1068" n="sc" s="T61">
               <ts e="T62" id="Seg_1070" n="e" s="T61">“(Tın=) </ts>
               <ts e="T63" id="Seg_1072" n="e" s="T62">nʼoma </ts>
               <ts e="T64" id="Seg_1074" n="e" s="T63">tɨqa </ts>
               <ts e="T65" id="Seg_1076" n="e" s="T64">tɨna </ts>
               <ts e="T66" id="Seg_1078" n="e" s="T65">ürlaka </ts>
               <ts e="T67" id="Seg_1080" n="e" s="T66">taːtsɨt.” </ts>
            </ts>
            <ts e="T75" id="Seg_1081" n="sc" s="T68">
               <ts e="T69" id="Seg_1083" n="e" s="T68">Nʼomaja </ts>
               <ts e="T70" id="Seg_1085" n="e" s="T69">i </ts>
               <ts e="T71" id="Seg_1087" n="e" s="T70">kulaja </ts>
               <ts e="T72" id="Seg_1089" n="e" s="T71">ämɨsɨqäqı </ts>
               <ts e="T73" id="Seg_1091" n="e" s="T72">(ila -), </ts>
               <ts e="T74" id="Seg_1093" n="e" s="T73">ilʼɨmpɔːqı, </ts>
               <ts e="T75" id="Seg_1095" n="e" s="T74">ilʼɨmpɔːqı. </ts>
            </ts>
            <ts e="T81" id="Seg_1096" n="sc" s="T76">
               <ts e="T77" id="Seg_1098" n="e" s="T76">(Nʼoma=) </ts>
               <ts e="T78" id="Seg_1100" n="e" s="T77">nʼoma </ts>
               <ts e="T79" id="Seg_1102" n="e" s="T78">tɨna </ts>
               <ts e="T80" id="Seg_1104" n="e" s="T79">šottɨ </ts>
               <ts e="T81" id="Seg_1106" n="e" s="T80">qənpa. </ts>
            </ts>
            <ts e="T87" id="Seg_1107" n="sc" s="T82">
               <ts e="T83" id="Seg_1109" n="e" s="T82">Ukkɨr </ts>
               <ts e="T84" id="Seg_1111" n="e" s="T83">iralaka </ts>
               <ts e="T85" id="Seg_1113" n="e" s="T84">čʼošɨ </ts>
               <ts e="T86" id="Seg_1115" n="e" s="T85">qaɣlɨ </ts>
               <ts e="T87" id="Seg_1117" n="e" s="T86">üqɨlmmɨntɨt. </ts>
            </ts>
            <ts e="T91" id="Seg_1118" n="sc" s="T88">
               <ts e="T89" id="Seg_1120" n="e" s="T88">Nʼoma </ts>
               <ts e="T90" id="Seg_1122" n="e" s="T89">ämɨsɨqäqı </ts>
               <ts e="T91" id="Seg_1124" n="e" s="T90">ilʼimpɔːqı. </ts>
            </ts>
            <ts e="T102" id="Seg_1125" n="sc" s="T92">
               <ts e="T93" id="Seg_1127" n="e" s="T92">Ukkɨr </ts>
               <ts e="T94" id="Seg_1129" n="e" s="T93">(iralaka=) </ts>
               <ts e="T95" id="Seg_1131" n="e" s="T94">qənpanɨ </ts>
               <ts e="T96" id="Seg_1133" n="e" s="T95">nʼoma </ts>
               <ts e="T97" id="Seg_1135" n="e" s="T96">šottɨ; </ts>
               <ts e="T98" id="Seg_1137" n="e" s="T97">ukkɨr </ts>
               <ts e="T99" id="Seg_1139" n="e" s="T98">iralaka </ts>
               <ts e="T100" id="Seg_1141" n="e" s="T99">čʼošɨ </ts>
               <ts e="T101" id="Seg_1143" n="e" s="T100">qaɣlɨ </ts>
               <ts e="T102" id="Seg_1145" n="e" s="T101">üqɨlmmɨntɨt. </ts>
            </ts>
            <ts e="T112" id="Seg_1146" n="sc" s="T103">
               <ts e="T104" id="Seg_1148" n="e" s="T103">Nʼoma </ts>
               <ts e="T105" id="Seg_1150" n="e" s="T104">qənpa </ts>
               <ts e="T106" id="Seg_1152" n="e" s="T105">näčʼčʼa, </ts>
               <ts e="T107" id="Seg_1154" n="e" s="T106">šittalä </ts>
               <ts e="T108" id="Seg_1156" n="e" s="T107">čʼošɨlaka </ts>
               <ts e="T109" id="Seg_1158" n="e" s="T108">orqɨlʼmmɨntɨt, </ts>
               <ts e="T110" id="Seg_1160" n="e" s="T109">šittälʼ </ts>
               <ts e="T111" id="Seg_1162" n="e" s="T110">moqonä </ts>
               <ts e="T112" id="Seg_1164" n="e" s="T111">qənpa. </ts>
            </ts>
            <ts e="T118" id="Seg_1165" n="sc" s="T113">
               <ts e="T114" id="Seg_1167" n="e" s="T113">Šittal </ts>
               <ts e="T115" id="Seg_1169" n="e" s="T114">moqonä </ts>
               <ts e="T116" id="Seg_1171" n="e" s="T115">qəlla </ts>
               <ts e="T117" id="Seg_1173" n="e" s="T116">ijaiːtɨ </ts>
               <ts e="T118" id="Seg_1175" n="e" s="T117">awstɨmpatɨ. </ts>
            </ts>
            <ts e="T126" id="Seg_1176" n="sc" s="T119">
               <ts e="T120" id="Seg_1178" n="e" s="T119">Ijaiːtɨ, </ts>
               <ts e="T121" id="Seg_1180" n="e" s="T120">ukkɨr </ts>
               <ts e="T122" id="Seg_1182" n="e" s="T121">ijaja </ts>
               <ts e="T123" id="Seg_1184" n="e" s="T122">nʼoman </ts>
               <ts e="T124" id="Seg_1186" n="e" s="T123">qulan </ts>
               <ts e="T125" id="Seg_1188" n="e" s="T124">ijaja </ts>
               <ts e="T126" id="Seg_1190" n="e" s="T125">pijatukkɨɣit. </ts>
            </ts>
            <ts e="T135" id="Seg_1191" n="sc" s="T127">
               <ts e="T128" id="Seg_1193" n="e" s="T127">(Qula=) </ts>
               <ts e="T129" id="Seg_1195" n="e" s="T128">qulan </ts>
               <ts e="T130" id="Seg_1197" n="e" s="T129">ijaja </ts>
               <ts e="T131" id="Seg_1199" n="e" s="T130">moqona </ts>
               <ts e="T132" id="Seg_1201" n="e" s="T131">qəlla </ts>
               <ts e="T133" id="Seg_1203" n="e" s="T132">ämɨtɨ </ts>
               <ts e="T134" id="Seg_1205" n="e" s="T133">nık </ts>
               <ts e="T135" id="Seg_1207" n="e" s="T134">kəttɨmpatɨt. </ts>
            </ts>
            <ts e="T141" id="Seg_1208" n="sc" s="T136">
               <ts e="T137" id="Seg_1210" n="e" s="T136">(Nʼoma=) </ts>
               <ts e="T138" id="Seg_1212" n="e" s="T137">nʼoma </ts>
               <ts e="T139" id="Seg_1214" n="e" s="T138">ijaiːtɨ </ts>
               <ts e="T140" id="Seg_1216" n="e" s="T139">ürɨlakam </ts>
               <ts e="T141" id="Seg_1218" n="e" s="T140">orqɨlʼpat. </ts>
            </ts>
            <ts e="T147" id="Seg_1219" n="sc" s="T142">
               <ts e="T143" id="Seg_1221" n="e" s="T142">Šittälʼ </ts>
               <ts e="T144" id="Seg_1223" n="e" s="T143">qula </ts>
               <ts e="T145" id="Seg_1225" n="e" s="T144">toː </ts>
               <ts e="T146" id="Seg_1227" n="e" s="T145">mɔːt </ts>
               <ts e="T147" id="Seg_1229" n="e" s="T146">qənpa. </ts>
            </ts>
            <ts e="T155" id="Seg_1230" n="sc" s="T148">
               <ts e="T149" id="Seg_1232" n="e" s="T148">Qula </ts>
               <ts e="T150" id="Seg_1234" n="e" s="T149">toː </ts>
               <ts e="T151" id="Seg_1236" n="e" s="T150">mɔːt </ts>
               <ts e="T152" id="Seg_1238" n="e" s="T151">qənpa, </ts>
               <ts e="T153" id="Seg_1240" n="e" s="T152">šittälʼa </ts>
               <ts e="T154" id="Seg_1242" n="e" s="T153">nık </ts>
               <ts e="T155" id="Seg_1244" n="e" s="T154">kəttɨmpat. </ts>
            </ts>
            <ts e="T160" id="Seg_1245" n="sc" s="T156">
               <ts e="T157" id="Seg_1247" n="e" s="T156">“Tat </ts>
               <ts e="T158" id="Seg_1249" n="e" s="T157">kunɨn </ts>
               <ts e="T159" id="Seg_1251" n="e" s="T158">čʼošɨlaka </ts>
               <ts e="T160" id="Seg_1253" n="e" s="T159">qosal?” </ts>
            </ts>
            <ts e="T164" id="Seg_1254" n="sc" s="T161">
               <ts e="T162" id="Seg_1256" n="e" s="T161">Nʼoma </ts>
               <ts e="T163" id="Seg_1258" n="e" s="T162">nık </ts>
               <ts e="T164" id="Seg_1260" n="e" s="T163">kəttɨɣit. </ts>
            </ts>
            <ts e="T170" id="Seg_1261" n="sc" s="T165">
               <ts e="T166" id="Seg_1263" n="e" s="T165">“Mat </ts>
               <ts e="T167" id="Seg_1265" n="e" s="T166">šotqɨntɨ </ts>
               <ts e="T168" id="Seg_1267" n="e" s="T167">ɨralaka </ts>
               <ts e="T169" id="Seg_1269" n="e" s="T168">qontɨrsam, </ts>
               <ts e="T170" id="Seg_1271" n="e" s="T169">šittälʼ. </ts>
            </ts>
            <ts e="T175" id="Seg_1272" n="sc" s="T171">
               <ts e="T172" id="Seg_1274" n="e" s="T171">(Ü-) </ts>
               <ts e="T173" id="Seg_1276" n="e" s="T172">ürɨ </ts>
               <ts e="T174" id="Seg_1278" n="e" s="T173">qaɣl </ts>
               <ts e="T175" id="Seg_1280" n="e" s="T174">üqɨlmmɨntɨt. </ts>
            </ts>
            <ts e="T179" id="Seg_1281" n="sc" s="T176">
               <ts e="T177" id="Seg_1283" n="e" s="T176">Šittalʼä </ts>
               <ts e="T178" id="Seg_1285" n="e" s="T177">šottɨ </ts>
               <ts e="T179" id="Seg_1287" n="e" s="T178">qənpa. </ts>
            </ts>
            <ts e="T187" id="Seg_1288" n="sc" s="T180">
               <ts e="T181" id="Seg_1290" n="e" s="T180">Nʼoma </ts>
               <ts e="T182" id="Seg_1292" n="e" s="T181">qɔːtɨ </ts>
               <ts e="T183" id="Seg_1294" n="e" s="T182">pɔːr </ts>
               <ts e="T184" id="Seg_1296" n="e" s="T183">sɨrsa </ts>
               <ts e="T185" id="Seg_1298" n="e" s="T184">natqɨlpat, </ts>
               <ts e="T186" id="Seg_1300" n="e" s="T185">nık </ts>
               <ts e="T187" id="Seg_1302" n="e" s="T186">kəttɨɣit. </ts>
            </ts>
            <ts e="T193" id="Seg_1303" n="sc" s="T188">
               <ts e="T189" id="Seg_1305" n="e" s="T188">“Nannɛr </ts>
               <ts e="T190" id="Seg_1307" n="e" s="T189">pušqatɔːlla </ts>
               <ts e="T191" id="Seg_1309" n="e" s="T190">štɨl </ts>
               <ts e="T192" id="Seg_1311" n="e" s="T191">aša </ts>
               <ts e="T193" id="Seg_1313" n="e" s="T192">atɨ.” </ts>
            </ts>
            <ts e="T201" id="Seg_1314" n="sc" s="T194">
               <ts e="T195" id="Seg_1316" n="e" s="T194">Qula </ts>
               <ts e="T196" id="Seg_1318" n="e" s="T195">qaɣlɨn </ts>
               <ts e="T197" id="Seg_1320" n="e" s="T196">omtɨıːla, </ts>
               <ts e="T198" id="Seg_1322" n="e" s="T197">qüq, </ts>
               <ts e="T199" id="Seg_1324" n="e" s="T198">qajlak </ts>
               <ts e="T200" id="Seg_1326" n="e" s="T199">awsɨlaka </ts>
               <ts e="T201" id="Seg_1328" n="e" s="T200">orqɨlʼpat. </ts>
            </ts>
            <ts e="T210" id="Seg_1329" n="sc" s="T202">
               <ts e="T203" id="Seg_1331" n="e" s="T202">Šittalä </ts>
               <ts e="T204" id="Seg_1333" n="e" s="T203">iralaka </ts>
               <ts e="T205" id="Seg_1335" n="e" s="T204">sukulta </ts>
               <ts e="T206" id="Seg_1337" n="e" s="T205">piːqɨlla </ts>
               <ts e="T207" id="Seg_1339" n="e" s="T206">pürüsa </ts>
               <ts e="T208" id="Seg_1341" n="e" s="T207">qättɨmpatɨ, </ts>
               <ts e="T209" id="Seg_1343" n="e" s="T208">ılla </ts>
               <ts e="T210" id="Seg_1345" n="e" s="T209">qulʼčʼimpa. </ts>
            </ts>
            <ts e="T217" id="Seg_1346" n="sc" s="T211">
               <ts e="T212" id="Seg_1348" n="e" s="T211">(Tä- </ts>
               <ts e="T213" id="Seg_1350" n="e" s="T212">im-) </ts>
               <ts e="T214" id="Seg_1352" n="e" s="T213">nʼoma </ts>
               <ts e="T215" id="Seg_1354" n="e" s="T214">tɨna </ts>
               <ts e="T216" id="Seg_1356" n="e" s="T215">qənpanə </ts>
               <ts e="T217" id="Seg_1358" n="e" s="T216">näčʼčʼa. </ts>
            </ts>
            <ts e="T223" id="Seg_1359" n="sc" s="T218">
               <ts e="T219" id="Seg_1361" n="e" s="T218">Čʼošɨlak </ts>
               <ts e="T220" id="Seg_1363" n="e" s="T219">orqɨlmmɨntɨt, </ts>
               <ts e="T221" id="Seg_1365" n="e" s="T220">šittälʼ </ts>
               <ts e="T222" id="Seg_1367" n="e" s="T221">moqonä </ts>
               <ts e="T223" id="Seg_1369" n="e" s="T222">pakta. </ts>
            </ts>
            <ts e="T226" id="Seg_1370" n="sc" s="T224">
               <ts e="T225" id="Seg_1372" n="e" s="T224">Ijajaiːmtɨ </ts>
               <ts e="T226" id="Seg_1374" n="e" s="T225">awstɨmpatɨ. </ts>
            </ts>
            <ts e="T238" id="Seg_1375" n="sc" s="T227">
               <ts e="T228" id="Seg_1377" n="e" s="T227">(Qula=) </ts>
               <ts e="T229" id="Seg_1379" n="e" s="T228">qulan </ts>
               <ts e="T230" id="Seg_1381" n="e" s="T229">ijaja </ts>
               <ts e="T231" id="Seg_1383" n="e" s="T230">nık </ts>
               <ts e="T232" id="Seg_1385" n="e" s="T231">kəttɨɣɨt: </ts>
               <ts e="T233" id="Seg_1387" n="e" s="T232">“Amä </ts>
               <ts e="T234" id="Seg_1389" n="e" s="T233">qaj </ts>
               <ts e="T235" id="Seg_1391" n="e" s="T234">meːtɨt?” </ts>
               <ts e="T236" id="Seg_1393" n="e" s="T235">“Amalɨ </ts>
               <ts e="T237" id="Seg_1395" n="e" s="T236">čʼošɨlaka </ts>
               <ts e="T238" id="Seg_1397" n="e" s="T237">tačʼalmmɨntɨt.” </ts>
            </ts>
            <ts e="T246" id="Seg_1398" n="sc" s="T239">
               <ts e="T240" id="Seg_1400" n="e" s="T239">Šittalʼ </ts>
               <ts e="T241" id="Seg_1402" n="e" s="T240">nʼoma </ts>
               <ts e="T242" id="Seg_1404" n="e" s="T241">šittalä </ts>
               <ts e="T243" id="Seg_1406" n="e" s="T242">((…)) </ts>
               <ts e="T244" id="Seg_1408" n="e" s="T243">moqonä </ts>
               <ts e="T245" id="Seg_1410" n="e" s="T244">qəlla </ts>
               <ts e="T246" id="Seg_1412" n="e" s="T245">ɔːntalpa. </ts>
            </ts>
            <ts e="T255" id="Seg_1413" n="sc" s="T247">
               <ts e="T248" id="Seg_1415" n="e" s="T247">Nʼoman </ts>
               <ts e="T249" id="Seg_1417" n="e" s="T248">amɨrnɔːtɨn, </ts>
               <ts e="T250" id="Seg_1419" n="e" s="T249">na </ts>
               <ts e="T251" id="Seg_1421" n="e" s="T250">puːnɨ </ts>
               <ts e="T252" id="Seg_1423" n="e" s="T251">nık </ts>
               <ts e="T253" id="Seg_1425" n="e" s="T252">kəttɨɣɨt: </ts>
               <ts e="T254" id="Seg_1427" n="e" s="T253">“Amal </ts>
               <ts e="T255" id="Seg_1429" n="e" s="T254">qattɨsa? </ts>
            </ts>
            <ts e="T262" id="Seg_1430" n="sc" s="T256">
               <ts e="T257" id="Seg_1432" n="e" s="T256">Amal </ts>
               <ts e="T258" id="Seg_1434" n="e" s="T257">qattɨsat, </ts>
               <ts e="T259" id="Seg_1436" n="e" s="T258">amantɨtɨ </ts>
               <ts e="T260" id="Seg_1438" n="e" s="T259">iralaka </ts>
               <ts e="T261" id="Seg_1440" n="e" s="T260">((…)) </ts>
               <ts e="T262" id="Seg_1442" n="e" s="T261">qättɨsɨtɨ.” </ts>
            </ts>
            <ts e="T270" id="Seg_1443" n="sc" s="T263">
               <ts e="T264" id="Seg_1445" n="e" s="T263">Muntɨk </ts>
               <ts e="T265" id="Seg_1447" n="e" s="T264">čʼuːrɨla </ts>
               <ts e="T266" id="Seg_1449" n="e" s="T265">kɨpa </ts>
               <ts e="T267" id="Seg_1451" n="e" s="T266">mü </ts>
               <ts e="T268" id="Seg_1453" n="e" s="T267">šiːmɨn </ts>
               <ts e="T269" id="Seg_1455" n="e" s="T268">ılla </ts>
               <ts e="T270" id="Seg_1457" n="e" s="T269">paktɨsɔːtɨn. </ts>
            </ts>
            <ts e="T274" id="Seg_1458" n="sc" s="T271">
               <ts e="T272" id="Seg_1460" n="e" s="T271">Nʼomaj </ts>
               <ts e="T273" id="Seg_1462" n="e" s="T272">amʼasɨnɨ </ts>
               <ts e="T274" id="Seg_1464" n="e" s="T273">amɨrnɔːtɨn. </ts>
            </ts>
            <ts e="T276" id="Seg_1465" n="sc" s="T275">
               <ts e="T276" id="Seg_1467" n="e" s="T275">Всё. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_1468" s="T1">KR_196X_RavensAndHares2_flk.001 (001)</ta>
            <ta e="T11" id="Seg_1469" s="T277">KR_196X_RavensAndHares2_flk.002 (002)</ta>
            <ta e="T21" id="Seg_1470" s="T12">KR_196X_RavensAndHares2_flk.003 (003)</ta>
            <ta e="T28" id="Seg_1471" s="T22">KR_196X_RavensAndHares2_flk.004 (004)</ta>
            <ta e="T35" id="Seg_1472" s="T29">KR_196X_RavensAndHares2_flk.005 (005)</ta>
            <ta e="T44" id="Seg_1473" s="T36">KR_196X_RavensAndHares2_flk.006 (006)</ta>
            <ta e="T53" id="Seg_1474" s="T45">KR_196X_RavensAndHares2_flk.007 (007)</ta>
            <ta e="T60" id="Seg_1475" s="T54">KR_196X_RavensAndHares2_flk.008 (008)</ta>
            <ta e="T67" id="Seg_1476" s="T61">KR_196X_RavensAndHares2_flk.009 (009)</ta>
            <ta e="T75" id="Seg_1477" s="T68">KR_196X_RavensAndHares2_flk.010 (010)</ta>
            <ta e="T81" id="Seg_1478" s="T76">KR_196X_RavensAndHares2_flk.011 (011)</ta>
            <ta e="T87" id="Seg_1479" s="T82">KR_196X_RavensAndHares2_flk.012 (012)</ta>
            <ta e="T91" id="Seg_1480" s="T88">KR_196X_RavensAndHares2_flk.013 (013)</ta>
            <ta e="T102" id="Seg_1481" s="T92">KR_196X_RavensAndHares2_flk.014 (014)</ta>
            <ta e="T112" id="Seg_1482" s="T103">KR_196X_RavensAndHares2_flk.015 (015)</ta>
            <ta e="T118" id="Seg_1483" s="T113">KR_196X_RavensAndHares2_flk.016 (016)</ta>
            <ta e="T126" id="Seg_1484" s="T119">KR_196X_RavensAndHares2_flk.017 (017)</ta>
            <ta e="T135" id="Seg_1485" s="T127">KR_196X_RavensAndHares2_flk.018 (018)</ta>
            <ta e="T141" id="Seg_1486" s="T136">KR_196X_RavensAndHares2_flk.019 (019)</ta>
            <ta e="T147" id="Seg_1487" s="T142">KR_196X_RavensAndHares2_flk.020 (020)</ta>
            <ta e="T155" id="Seg_1488" s="T148">KR_196X_RavensAndHares2_flk.021 (021)</ta>
            <ta e="T160" id="Seg_1489" s="T156">KR_196X_RavensAndHares2_flk.022 (022)</ta>
            <ta e="T164" id="Seg_1490" s="T161">KR_196X_RavensAndHares2_flk.023 (023)</ta>
            <ta e="T170" id="Seg_1491" s="T165">KR_196X_RavensAndHares2_flk.024 (024)</ta>
            <ta e="T175" id="Seg_1492" s="T171">KR_196X_RavensAndHares2_flk.025 (025)</ta>
            <ta e="T179" id="Seg_1493" s="T176">KR_196X_RavensAndHares2_flk.026 (026)</ta>
            <ta e="T187" id="Seg_1494" s="T180">KR_196X_RavensAndHares2_flk.027 (027)</ta>
            <ta e="T193" id="Seg_1495" s="T188">KR_196X_RavensAndHares2_flk.028 (028)</ta>
            <ta e="T201" id="Seg_1496" s="T194">KR_196X_RavensAndHares2_flk.029 (029)</ta>
            <ta e="T210" id="Seg_1497" s="T202">KR_196X_RavensAndHares2_flk.030 (030)</ta>
            <ta e="T217" id="Seg_1498" s="T211">KR_196X_RavensAndHares2_flk.031 (031)</ta>
            <ta e="T223" id="Seg_1499" s="T218">KR_196X_RavensAndHares2_flk.032 (032)</ta>
            <ta e="T226" id="Seg_1500" s="T224">KR_196X_RavensAndHares2_flk.033 (033)</ta>
            <ta e="T235" id="Seg_1501" s="T227">KR_196X_RavensAndHares2_flk.034 (034)</ta>
            <ta e="T238" id="Seg_1502" s="T235">KR_196X_RavensAndHares2_flk.035 (035)</ta>
            <ta e="T246" id="Seg_1503" s="T239">KR_196X_RavensAndHares2_flk.036 (036)</ta>
            <ta e="T255" id="Seg_1504" s="T247">KR_196X_RavensAndHares2_flk.037 (037)</ta>
            <ta e="T262" id="Seg_1505" s="T256">KR_196X_RavensAndHares2_flk.038 (038)</ta>
            <ta e="T270" id="Seg_1506" s="T263">KR_196X_RavensAndHares2_flk.039 (039)</ta>
            <ta e="T274" id="Seg_1507" s="T271">KR_196X_RavensAndHares2_flk.040 (040)</ta>
            <ta e="T276" id="Seg_1508" s="T275">KR_196X_RavensAndHares2_flk.041 (041)</ta>
         </annotation>
         <annotation name="st" tierref="st" />
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_1509" s="T1">Рита Кунина, cказка; говори.</ta>
            <ta e="T11" id="Seg_1510" s="T277">nʼoma i qulʼa ämɨsaqaqı ilʼimpɔːqı, ilʼimpɔːqı.</ta>
            <ta e="T21" id="Seg_1511" s="T12">nʼoma… nʼoma šʼottɨ qəla, šʼottɨ qənpa i šʼittalä.</ta>
            <ta e="T28" id="Seg_1512" s="T22">ukkɨr ira laka ürɨ qaqlɨ ükɨlmmɨntɨt(ɨ).</ta>
            <ta e="T35" id="Seg_1513" s="T29">šʼittalʼ qəla čʼošɨ laka iːmmɨntɨtɨ moqonä tümpa.</ta>
            <ta e="T44" id="Seg_1514" s="T36">nʼoma ijaıːtɨ tamɨ nɔːt pun qula ijaıːtɨ tünnɨnt.</ta>
            <ta e="T53" id="Seg_1515" s="T45">šʼittalʼ nʼoma … nʼoman ijaja i qulan ijaja pijatupsɨt.</ta>
            <ta e="T60" id="Seg_1516" s="T54">qulan ijaja moqonä qəla nı kətɨmpat</ta>
            <ta e="T67" id="Seg_1517" s="T61">tɨn… nʼoma tɨqa tɨna ür laka taːtsɨt.</ta>
            <ta e="T75" id="Seg_1518" s="T68">nʼomaja i qulaja ämɨsaqaqı ila, ilʼimpɔːqı, ilʼimpɔːqı.</ta>
            <ta e="T81" id="Seg_1519" s="T76">nʼoma tɨna šʼottɨ qənpa.</ta>
            <ta e="T87" id="Seg_1520" s="T82">ukkɨr ija laka čʼoši qaɣlɨ ükkɨlmmɨntɨt.</ta>
            <ta e="T91" id="Seg_1521" s="T88">nʼoma ämɨsaqaqı ilʼimpɔːqı.</ta>
            <ta e="T102" id="Seg_1522" s="T92">ukkɨr ira laka… qənpanɨ nʼoma šʼəttɨ; ukkɨr ira laka čʼoši qaɣlɨ ükkɨlmmɨntɨt.</ta>
            <ta e="T112" id="Seg_1523" s="T103">nʼoma qənpa nɛčʼa, šʼittalä čʼoši laka orqilʼmmɨntɨt, šʼittalʼ moqonä qənpa.</ta>
            <ta e="T118" id="Seg_1524" s="T113">šʼittal moqonä qəla ijaıːtɨ awstɨmpa(tɨ).</ta>
            <ta e="T126" id="Seg_1525" s="T119">ijaitɨ, ukkɨr ijaja nʼoman qulan ijaja pijatukkɨɣit.</ta>
            <ta e="T135" id="Seg_1526" s="T127">qula… qulan ijaja moqona qəla ämɨtɨ nı kəttɨmpat(ɨt).</ta>
            <ta e="T141" id="Seg_1527" s="T136">n'oma… nʼoma ijaıːtɨ üri lakam orqəlʼ(pat).</ta>
            <ta e="T147" id="Seg_1528" s="T142">šʼittalʼ qulʼa to mɔːt qənpa.</ta>
            <ta e="T155" id="Seg_1529" s="T148">qulʼa to mɔːt qənpa, šʼittalʼä nı kəttɨmpat. </ta>
            <ta e="T160" id="Seg_1530" s="T156">tat kunɨn čʼoši laka qossal?</ta>
            <ta e="T164" id="Seg_1531" s="T161">nʼoma nı kəttiɣit.</ta>
            <ta e="T170" id="Seg_1532" s="T165">mat šʼotqɨntɨ ɨra laka qontɨrsam, šʼittalʼ.</ta>
            <ta e="T175" id="Seg_1533" s="T171">ü… ürɨ qaɣl ükkilmmɨ(ntɨt).</ta>
            <ta e="T179" id="Seg_1534" s="T176">šʼittalʼä šʼottɨ qənpa.</ta>
            <ta e="T187" id="Seg_1535" s="T180">nʼoma qotɨ por sɨrsa natqɨlʼpat, nı kəttɨɣit. </ta>
            <ta e="T193" id="Seg_1536" s="T188">nannɛr pušqattɔːla tɨl aša attɨ.</ta>
            <ta e="T201" id="Seg_1537" s="T194">qulʼa qaɣlɨn omtɨıːla qüq qaj lak awsɨ laka orqɨlʼpat.</ta>
            <ta e="T210" id="Seg_1538" s="T202">šʼittalä ira laka sukulʼta piqɨla. pürüsa qättɨmpatɨ, ılla qulʼčʼimpa.</ta>
            <ta e="T217" id="Seg_1539" s="T211">tä… im… nʼoma tɨna qənpanə nečʼa.</ta>
            <ta e="T223" id="Seg_1540" s="T218">čʼošɨ lak orqɨlmmɨntɨt, šʼittalʼ moqonä pakta.</ta>
            <ta e="T226" id="Seg_1541" s="T224">ijajaımːtɨ awstɨmpat(ɨ).</ta>
            <ta e="T235" id="Seg_1542" s="T227">qula… qulan ijaja nı kəttɨɣɨt: amä qaj meːtɨt? amalä čʼoši laka tačʼalmmɨntɨt.</ta>
            <ta e="T238" id="Seg_1543" s="T235">qula… qulan ijaja nı kəttɨɣɨt: amä qaj meːtɨt? amalä čʼoši laka tačʼalmmɨntɨt.</ta>
            <ta e="T246" id="Seg_1544" s="T239">šʼittalʼ nʼoma šʼittalä moqonä qəla ontalpa. </ta>
            <ta e="T255" id="Seg_1545" s="T247">nʼoman amɨrnɔːtɨn, na punɨ nı kəttɨɣɨt: amal qattɨ(je)?</ta>
            <ta e="T262" id="Seg_1546" s="T256">amal qattɨsat amantɨtɨ ira laka qät(tɨsɨtɨ).</ta>
            <ta e="T270" id="Seg_1547" s="T263">muntɨk čʼurɨla kɨpa müšʼiː(mɨn) ılla paktɨsɔːtɨn.</ta>
            <ta e="T274" id="Seg_1548" s="T271">nʼomaj amʼasɨnɨ amɨrnɔːtɨn.</ta>
            <ta e="T276" id="Seg_1549" s="T275">Всё.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_1550" s="T1">((KuAI:)) Рита Кунина, cказка; говори.</ta>
            <ta e="T11" id="Seg_1551" s="T277">((KR:)) Nʼoma i kulä ämɨsɨqäqı ilʼɨmpɔːqı, ilʼɨmpɔːqı. </ta>
            <ta e="T21" id="Seg_1552" s="T12">(Nʼoma=) nʼoma (šottɨ=) šottɨ qəlla, šottɨ qənpa i šittalä. </ta>
            <ta e="T28" id="Seg_1553" s="T22">Ukkɨr iralaka ((…)) ürɨ qaqlɨ (üqɨlmmɨntɨtɨ). </ta>
            <ta e="T35" id="Seg_1554" s="T29">Šittalʼ qəlla čʼošɨlaka iːmmɨntɨtɨ moqonä tümpa. </ta>
            <ta e="T44" id="Seg_1555" s="T36">Nʼoma ijaiːtɨ tamɨ nɔːt puːn kula ijaiːtɨ tünnɨnt. </ta>
            <ta e="T53" id="Seg_1556" s="T45">Šittalʼ nʼoma… nʼoman ijaja i kulan ijaja pijatuksɨt. </ta>
            <ta e="T60" id="Seg_1557" s="T54">Kulan ijaja moqonä qəlla nık kətɨmpat. </ta>
            <ta e="T67" id="Seg_1558" s="T61">“Tın… nʼoma tɨqa tɨna ürlaka taːtsɨt.” </ta>
            <ta e="T75" id="Seg_1559" s="T68">Nʼomaja i kulaja ämɨsɨqäqı (ila -), ilʼɨmpɔːqı, ilʼɨmpɔːqı. </ta>
            <ta e="T81" id="Seg_1560" s="T76">(Nʼoma=) nʼoma tɨna šottɨ qənpa. </ta>
            <ta e="T87" id="Seg_1561" s="T82">Ukkɨr iralaka čʼošɨ qaɣlɨ üqɨlmmɨntɨt. </ta>
            <ta e="T91" id="Seg_1562" s="T88">Nʼoma ämɨsɨqäqı ilʼimpɔːqı. </ta>
            <ta e="T102" id="Seg_1563" s="T92">Ukkɨr iralaka… qənpanɨ nʼoma šottɨ; ukkɨr iralaka čʼošɨ qaɣlɨ üqɨlmmɨntɨt. </ta>
            <ta e="T112" id="Seg_1564" s="T103">Nʼoma qənpa näčʼčʼa, šittalä čʼošɨlaka orqɨlʼmmɨntɨt, šittälʼ moqonä qənpa. </ta>
            <ta e="T118" id="Seg_1565" s="T113">Šittal moqonä qəlla ijaiːtɨ awstɨmpatɨ. </ta>
            <ta e="T126" id="Seg_1566" s="T119">Ijaiːtɨ, ukkɨr ijaja nʼoman qulan ijaja pijatukkɨɣit. </ta>
            <ta e="T135" id="Seg_1567" s="T127">Qula… qulan ijaja moqona qəlla ämɨtɨ nık kəttɨmpatɨt. </ta>
            <ta e="T141" id="Seg_1568" s="T136">Nʼoma… nʼoma ijaiːtɨ ürɨlakam orqɨlʼpat. </ta>
            <ta e="T147" id="Seg_1569" s="T142">Šittälʼ qula toː mɔːt qənpa. </ta>
            <ta e="T155" id="Seg_1570" s="T148">Qula toː mɔːt qənpa, šittälʼa nık kəttɨmpat. </ta>
            <ta e="T160" id="Seg_1571" s="T156">“Tat kunɨn čʼošɨlaka qosal?” </ta>
            <ta e="T164" id="Seg_1572" s="T161">Nʼoma nık kəttɨɣit. </ta>
            <ta e="T170" id="Seg_1573" s="T165">“Mat šotqɨntɨ ɨralaka qontɨrsam, šittälʼ. </ta>
            <ta e="T175" id="Seg_1574" s="T171">(Ü-) ürɨ qaɣl üqɨlmmɨntɨt. </ta>
            <ta e="T179" id="Seg_1575" s="T176">Šittalʼä šottɨ qənpa. </ta>
            <ta e="T187" id="Seg_1576" s="T180">Nʼoma qɔːtɨ pɔːr sɨrsa natqɨlpat, nık kəttɨɣit. </ta>
            <ta e="T193" id="Seg_1577" s="T188">“Nannɛr pušqatɔːlla štɨl aša atɨ.” </ta>
            <ta e="T201" id="Seg_1578" s="T194">Qula qaɣlɨn omtɨıːla, qüq, qajlak awsɨlaka orqɨlʼpat. </ta>
            <ta e="T210" id="Seg_1579" s="T202">Šittalä iralaka sukulta piːqɨlla pürüsa qättɨmpatɨ, ılla qulʼčʼimpa. </ta>
            <ta e="T217" id="Seg_1580" s="T211">(Tä- im-) nʼoma tɨna qənpanə näčʼčʼa. </ta>
            <ta e="T223" id="Seg_1581" s="T218">Čʼošɨlak orqɨlmmɨntɨt, šittälʼ moqonä pakta. </ta>
            <ta e="T226" id="Seg_1582" s="T224">Ijajaiːmtɨ awstɨmpatɨ. </ta>
            <ta e="T235" id="Seg_1583" s="T227">(Qula=) qulan ijaja nık kəttɨɣɨt: “Amä qaj meːtɨt?” </ta>
            <ta e="T238" id="Seg_1584" s="T235">“Amalɨ čʼošɨlaka tačʼalmmɨntɨt.” </ta>
            <ta e="T246" id="Seg_1585" s="T239">Šittalʼ nʼoma šittalä ((…)) moqonä qəlla ɔːntalpa. </ta>
            <ta e="T255" id="Seg_1586" s="T247">Nʼoman amɨrnɔːtɨn, na puːnɨ nık kəttɨɣɨt: “Amal qattɨsa? </ta>
            <ta e="T262" id="Seg_1587" s="T256">Amal qattɨsat, amantɨtɨ iralaka ((…)) qättɨsɨtɨ.” </ta>
            <ta e="T270" id="Seg_1588" s="T263">Muntɨk čʼuːrɨla kɨpa mü šiːmɨn ılla paktɨsɔːtɨn. </ta>
            <ta e="T274" id="Seg_1589" s="T271">Nʼomaj amʼasɨnɨ amɨrnɔːtɨn. </ta>
            <ta e="T276" id="Seg_1590" s="T275">Всё. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T6" id="Seg_1591" s="T5">nʼoma</ta>
            <ta e="T7" id="Seg_1592" s="T6">i</ta>
            <ta e="T8" id="Seg_1593" s="T7">kulä</ta>
            <ta e="T9" id="Seg_1594" s="T8">ämɨ-sɨ-qäqı</ta>
            <ta e="T10" id="Seg_1595" s="T9">ilʼɨ-mpɔː-qı</ta>
            <ta e="T11" id="Seg_1596" s="T10">ilʼɨ-mpɔː-qı</ta>
            <ta e="T13" id="Seg_1597" s="T12">nʼoma</ta>
            <ta e="T14" id="Seg_1598" s="T13">nʼoma</ta>
            <ta e="T15" id="Seg_1599" s="T14">šot-tɨ</ta>
            <ta e="T16" id="Seg_1600" s="T15">šot-tɨ</ta>
            <ta e="T17" id="Seg_1601" s="T16">qəl-la</ta>
            <ta e="T18" id="Seg_1602" s="T17">šot-tɨ</ta>
            <ta e="T19" id="Seg_1603" s="T18">qən-pa</ta>
            <ta e="T20" id="Seg_1604" s="T19">i</ta>
            <ta e="T21" id="Seg_1605" s="T20">šittalä</ta>
            <ta e="T23" id="Seg_1606" s="T22">ukkɨr</ta>
            <ta e="T24" id="Seg_1607" s="T23">ira-laka</ta>
            <ta e="T26" id="Seg_1608" s="T25">ürɨ</ta>
            <ta e="T27" id="Seg_1609" s="T26">qaqlɨ</ta>
            <ta e="T28" id="Seg_1610" s="T27">üqɨl-mmɨ-ntɨ-tɨ</ta>
            <ta e="T30" id="Seg_1611" s="T29">šittalʼ</ta>
            <ta e="T31" id="Seg_1612" s="T30">qəl-la</ta>
            <ta e="T32" id="Seg_1613" s="T31">čʼoš-ɨ-laka</ta>
            <ta e="T33" id="Seg_1614" s="T32">iː-mmɨ-ntɨ-tɨ</ta>
            <ta e="T34" id="Seg_1615" s="T33">moqonä</ta>
            <ta e="T35" id="Seg_1616" s="T34">tü-mpa</ta>
            <ta e="T37" id="Seg_1617" s="T36">nʼoma</ta>
            <ta e="T38" id="Seg_1618" s="T37">ija-iː-tɨ</ta>
            <ta e="T39" id="Seg_1619" s="T38">tamɨ</ta>
            <ta e="T40" id="Seg_1620" s="T39">nɔːt</ta>
            <ta e="T41" id="Seg_1621" s="T40">puːn</ta>
            <ta e="T42" id="Seg_1622" s="T41">kula</ta>
            <ta e="T43" id="Seg_1623" s="T42">ija-iː-tɨ</ta>
            <ta e="T44" id="Seg_1624" s="T43">tü-nnɨnt</ta>
            <ta e="T46" id="Seg_1625" s="T45">šittalʼ</ta>
            <ta e="T47" id="Seg_1626" s="T46">nʼoma</ta>
            <ta e="T48" id="Seg_1627" s="T47">nʼoma-n</ta>
            <ta e="T49" id="Seg_1628" s="T48">ija-ja</ta>
            <ta e="T50" id="Seg_1629" s="T49">i</ta>
            <ta e="T51" id="Seg_1630" s="T50">kula-n</ta>
            <ta e="T52" id="Seg_1631" s="T51">ija-ja</ta>
            <ta e="T53" id="Seg_1632" s="T52">pija-tu-k-sɨ-t</ta>
            <ta e="T55" id="Seg_1633" s="T54">kula-n</ta>
            <ta e="T56" id="Seg_1634" s="T55">ija-ja</ta>
            <ta e="T57" id="Seg_1635" s="T56">moqonä</ta>
            <ta e="T58" id="Seg_1636" s="T57">qəl-la</ta>
            <ta e="T59" id="Seg_1637" s="T58">nık</ta>
            <ta e="T60" id="Seg_1638" s="T59">kətɨ-mpa-t</ta>
            <ta e="T62" id="Seg_1639" s="T61">tın</ta>
            <ta e="T63" id="Seg_1640" s="T62">nʼoma</ta>
            <ta e="T64" id="Seg_1641" s="T63">tɨqa</ta>
            <ta e="T65" id="Seg_1642" s="T64">tɨna</ta>
            <ta e="T66" id="Seg_1643" s="T65">ür-laka</ta>
            <ta e="T67" id="Seg_1644" s="T66">taːt-sɨ-t</ta>
            <ta e="T69" id="Seg_1645" s="T68">nʼoma-ja</ta>
            <ta e="T70" id="Seg_1646" s="T69">i</ta>
            <ta e="T71" id="Seg_1647" s="T70">kula-ja</ta>
            <ta e="T72" id="Seg_1648" s="T71">ämɨ-sɨ-qäqı</ta>
            <ta e="T73" id="Seg_1649" s="T72">ila</ta>
            <ta e="T74" id="Seg_1650" s="T73">ilʼɨ-mpɔː-qı</ta>
            <ta e="T75" id="Seg_1651" s="T74">ilʼɨ-mpɔː-qı</ta>
            <ta e="T77" id="Seg_1652" s="T76">nʼoma</ta>
            <ta e="T78" id="Seg_1653" s="T77">nʼoma</ta>
            <ta e="T79" id="Seg_1654" s="T78">tɨna</ta>
            <ta e="T80" id="Seg_1655" s="T79">šot-tɨ</ta>
            <ta e="T81" id="Seg_1656" s="T80">qən-pa</ta>
            <ta e="T83" id="Seg_1657" s="T82">ukkɨr</ta>
            <ta e="T84" id="Seg_1658" s="T83">ira-laka</ta>
            <ta e="T85" id="Seg_1659" s="T84">čʼošɨ</ta>
            <ta e="T86" id="Seg_1660" s="T85">qaɣlɨ</ta>
            <ta e="T87" id="Seg_1661" s="T86">üqɨl-mmɨ-ntɨ-t</ta>
            <ta e="T89" id="Seg_1662" s="T88">nʼoma</ta>
            <ta e="T90" id="Seg_1663" s="T89">ämɨ-sɨ-qäqı</ta>
            <ta e="T91" id="Seg_1664" s="T90">ilʼi-mpɔː-qı</ta>
            <ta e="T93" id="Seg_1665" s="T92">ukkɨr</ta>
            <ta e="T94" id="Seg_1666" s="T93">ira-laka</ta>
            <ta e="T95" id="Seg_1667" s="T94">qən-pa-nɨ</ta>
            <ta e="T96" id="Seg_1668" s="T95">nʼoma</ta>
            <ta e="T97" id="Seg_1669" s="T96">šot-tɨ</ta>
            <ta e="T98" id="Seg_1670" s="T97">ukkɨr</ta>
            <ta e="T99" id="Seg_1671" s="T98">ira-laka</ta>
            <ta e="T100" id="Seg_1672" s="T99">čʼošɨ</ta>
            <ta e="T101" id="Seg_1673" s="T100">qaɣlɨ</ta>
            <ta e="T102" id="Seg_1674" s="T101">üqɨl-mmɨ-ntɨ-t</ta>
            <ta e="T104" id="Seg_1675" s="T103">nʼoma</ta>
            <ta e="T105" id="Seg_1676" s="T104">qən-pa</ta>
            <ta e="T106" id="Seg_1677" s="T105">näčʼčʼa</ta>
            <ta e="T107" id="Seg_1678" s="T106">šittalä</ta>
            <ta e="T108" id="Seg_1679" s="T107">čʼoš-ɨ-laka</ta>
            <ta e="T109" id="Seg_1680" s="T108">orqɨlʼ-mmɨ-ntɨ-t</ta>
            <ta e="T110" id="Seg_1681" s="T109">šit-tälʼ</ta>
            <ta e="T111" id="Seg_1682" s="T110">moqonä</ta>
            <ta e="T112" id="Seg_1683" s="T111">qən-pa</ta>
            <ta e="T114" id="Seg_1684" s="T113">šittal</ta>
            <ta e="T115" id="Seg_1685" s="T114">moqonä</ta>
            <ta e="T116" id="Seg_1686" s="T115">qəl-la</ta>
            <ta e="T117" id="Seg_1687" s="T116">ija-iː-tɨ</ta>
            <ta e="T118" id="Seg_1688" s="T117">aws-tɨ-mpa-tɨ</ta>
            <ta e="T120" id="Seg_1689" s="T119">ija-iː-tɨ</ta>
            <ta e="T121" id="Seg_1690" s="T120">ukkɨr</ta>
            <ta e="T122" id="Seg_1691" s="T121">ija-ja</ta>
            <ta e="T123" id="Seg_1692" s="T122">nʼoma-n</ta>
            <ta e="T124" id="Seg_1693" s="T123">qula-n</ta>
            <ta e="T125" id="Seg_1694" s="T124">ija-ja</ta>
            <ta e="T126" id="Seg_1695" s="T125">pija-tu-kkɨ-ɣi-t</ta>
            <ta e="T128" id="Seg_1696" s="T127">qula</ta>
            <ta e="T129" id="Seg_1697" s="T128">qula-n</ta>
            <ta e="T130" id="Seg_1698" s="T129">ija-ja</ta>
            <ta e="T131" id="Seg_1699" s="T130">moqona</ta>
            <ta e="T132" id="Seg_1700" s="T131">qəl-la</ta>
            <ta e="T133" id="Seg_1701" s="T132">ämɨ-tɨ</ta>
            <ta e="T134" id="Seg_1702" s="T133">nık</ta>
            <ta e="T135" id="Seg_1703" s="T134">kəttɨ-mpa-tɨt</ta>
            <ta e="T137" id="Seg_1704" s="T136">nʼoma</ta>
            <ta e="T138" id="Seg_1705" s="T137">nʼoma</ta>
            <ta e="T139" id="Seg_1706" s="T138">ija-iː-tɨ</ta>
            <ta e="T140" id="Seg_1707" s="T139">ür-ɨ-laka-m</ta>
            <ta e="T141" id="Seg_1708" s="T140">orqɨlʼ-pa-t</ta>
            <ta e="T143" id="Seg_1709" s="T142">šittälʼ</ta>
            <ta e="T144" id="Seg_1710" s="T143">qula</ta>
            <ta e="T145" id="Seg_1711" s="T144">toː</ta>
            <ta e="T146" id="Seg_1712" s="T145">mɔːt</ta>
            <ta e="T147" id="Seg_1713" s="T146">qən-pa</ta>
            <ta e="T149" id="Seg_1714" s="T148">qula</ta>
            <ta e="T150" id="Seg_1715" s="T149">toː</ta>
            <ta e="T151" id="Seg_1716" s="T150">mɔːt</ta>
            <ta e="T152" id="Seg_1717" s="T151">qən-pa</ta>
            <ta e="T153" id="Seg_1718" s="T152">šittälʼa</ta>
            <ta e="T154" id="Seg_1719" s="T153">nık</ta>
            <ta e="T155" id="Seg_1720" s="T154">kəttɨ-mpa-t</ta>
            <ta e="T157" id="Seg_1721" s="T156">tat</ta>
            <ta e="T158" id="Seg_1722" s="T157">kun-ɨ-n</ta>
            <ta e="T159" id="Seg_1723" s="T158">čʼoš-ɨ-laka</ta>
            <ta e="T160" id="Seg_1724" s="T159">qo-sa-l</ta>
            <ta e="T162" id="Seg_1725" s="T161">nʼoma</ta>
            <ta e="T163" id="Seg_1726" s="T162">nık</ta>
            <ta e="T164" id="Seg_1727" s="T163">kəttɨ-ɣi-t</ta>
            <ta e="T166" id="Seg_1728" s="T165">Mat</ta>
            <ta e="T167" id="Seg_1729" s="T166">šot-qɨn-tɨ</ta>
            <ta e="T168" id="Seg_1730" s="T167">ɨra-laka</ta>
            <ta e="T169" id="Seg_1731" s="T168">qo-ntɨr-sa-m</ta>
            <ta e="T170" id="Seg_1732" s="T169">šittälʼ</ta>
            <ta e="T173" id="Seg_1733" s="T172">ürɨ</ta>
            <ta e="T174" id="Seg_1734" s="T173">qaɣl</ta>
            <ta e="T175" id="Seg_1735" s="T174">üqɨl-mmɨ-ntɨ-t</ta>
            <ta e="T177" id="Seg_1736" s="T176">šittalʼä</ta>
            <ta e="T178" id="Seg_1737" s="T177">šot-tɨ</ta>
            <ta e="T179" id="Seg_1738" s="T178">qən-pa</ta>
            <ta e="T181" id="Seg_1739" s="T180">nʼoma</ta>
            <ta e="T182" id="Seg_1740" s="T181">qɔːt-ɨ</ta>
            <ta e="T183" id="Seg_1741" s="T182">pɔːr</ta>
            <ta e="T184" id="Seg_1742" s="T183">sɨr-sa</ta>
            <ta e="T185" id="Seg_1743" s="T184">nat-qɨl-pa-t</ta>
            <ta e="T186" id="Seg_1744" s="T185">nık</ta>
            <ta e="T187" id="Seg_1745" s="T186">kəttɨ-ɣi-t</ta>
            <ta e="T189" id="Seg_1746" s="T188">nannɛr</ta>
            <ta e="T190" id="Seg_1747" s="T189">pušqa-t-ɔːl-la</ta>
            <ta e="T191" id="Seg_1748" s="T190">štɨl</ta>
            <ta e="T192" id="Seg_1749" s="T191">aša</ta>
            <ta e="T193" id="Seg_1750" s="T192">atɨ</ta>
            <ta e="T195" id="Seg_1751" s="T194">qula</ta>
            <ta e="T196" id="Seg_1752" s="T195">qaɣlɨ-n</ta>
            <ta e="T197" id="Seg_1753" s="T196">omtɨ-ıː-la</ta>
            <ta e="T198" id="Seg_1754" s="T197">qüq</ta>
            <ta e="T199" id="Seg_1755" s="T198">qaj-lak</ta>
            <ta e="T200" id="Seg_1756" s="T199">awsɨ-laka</ta>
            <ta e="T201" id="Seg_1757" s="T200">orqɨlʼ-pa-t</ta>
            <ta e="T203" id="Seg_1758" s="T202">šittalä</ta>
            <ta e="T204" id="Seg_1759" s="T203">ira-laka</ta>
            <ta e="T205" id="Seg_1760" s="T204">sukulta</ta>
            <ta e="T206" id="Seg_1761" s="T205">piːqɨl-la</ta>
            <ta e="T207" id="Seg_1762" s="T206">pürü-sa</ta>
            <ta e="T208" id="Seg_1763" s="T207">qättɨ-mpa-tɨ</ta>
            <ta e="T209" id="Seg_1764" s="T208">ılla</ta>
            <ta e="T210" id="Seg_1765" s="T209">qu-lʼčʼi-mpa</ta>
            <ta e="T214" id="Seg_1766" s="T213">nʼoma</ta>
            <ta e="T215" id="Seg_1767" s="T214">tɨna</ta>
            <ta e="T216" id="Seg_1768" s="T215">qən-pa-nə</ta>
            <ta e="T217" id="Seg_1769" s="T216">näčʼčʼa</ta>
            <ta e="T219" id="Seg_1770" s="T218">čʼošɨ-lak</ta>
            <ta e="T220" id="Seg_1771" s="T219">orqɨl-mmɨ-ntɨ-t</ta>
            <ta e="T221" id="Seg_1772" s="T220">šittälʼ</ta>
            <ta e="T222" id="Seg_1773" s="T221">moqonä</ta>
            <ta e="T223" id="Seg_1774" s="T222">pakta</ta>
            <ta e="T225" id="Seg_1775" s="T224">ija-ja-iː-m-tɨ</ta>
            <ta e="T226" id="Seg_1776" s="T225">aws-tɨ-mpa-tɨ</ta>
            <ta e="T228" id="Seg_1777" s="T227">qula</ta>
            <ta e="T229" id="Seg_1778" s="T228">qula-n</ta>
            <ta e="T230" id="Seg_1779" s="T229">ija-ja</ta>
            <ta e="T231" id="Seg_1780" s="T230">nık</ta>
            <ta e="T232" id="Seg_1781" s="T231">kəttɨ-ɣɨ-t</ta>
            <ta e="T233" id="Seg_1782" s="T232">amä</ta>
            <ta e="T234" id="Seg_1783" s="T233">qaj</ta>
            <ta e="T235" id="Seg_1784" s="T234">meː-tɨ-t</ta>
            <ta e="T236" id="Seg_1785" s="T235">ama-lɨ</ta>
            <ta e="T237" id="Seg_1786" s="T236">čʼoš-ɨ-laka</ta>
            <ta e="T238" id="Seg_1787" s="T237">tačʼal-mmɨ-ntɨ-t</ta>
            <ta e="T240" id="Seg_1788" s="T239">šittalʼ</ta>
            <ta e="T241" id="Seg_1789" s="T240">nʼoma</ta>
            <ta e="T242" id="Seg_1790" s="T241">šittalä</ta>
            <ta e="T244" id="Seg_1791" s="T243">moqonä</ta>
            <ta e="T245" id="Seg_1792" s="T244">qəl-la</ta>
            <ta e="T246" id="Seg_1793" s="T245">ɔːnt-al-pa</ta>
            <ta e="T248" id="Seg_1794" s="T247">nʼoma-n</ta>
            <ta e="T249" id="Seg_1795" s="T248">am-ɨ-r-nɔː-tɨn</ta>
            <ta e="T250" id="Seg_1796" s="T249">na</ta>
            <ta e="T251" id="Seg_1797" s="T250">puːnɨ</ta>
            <ta e="T252" id="Seg_1798" s="T251">nık</ta>
            <ta e="T253" id="Seg_1799" s="T252">kəttɨ-ɣɨ-t</ta>
            <ta e="T254" id="Seg_1800" s="T253">ama-l</ta>
            <ta e="T255" id="Seg_1801" s="T254">qattɨ-sa</ta>
            <ta e="T257" id="Seg_1802" s="T256">ama-l</ta>
            <ta e="T258" id="Seg_1803" s="T257">qattɨ-sa-t</ta>
            <ta e="T259" id="Seg_1804" s="T258">ama-ntɨ-tɨ</ta>
            <ta e="T260" id="Seg_1805" s="T259">ira-laka</ta>
            <ta e="T262" id="Seg_1806" s="T261">qättɨ-sɨ-tɨ</ta>
            <ta e="T264" id="Seg_1807" s="T263">muntɨk</ta>
            <ta e="T265" id="Seg_1808" s="T264">čʼuːrɨ-la</ta>
            <ta e="T266" id="Seg_1809" s="T265">kɨpa</ta>
            <ta e="T267" id="Seg_1810" s="T266">mü</ta>
            <ta e="T268" id="Seg_1811" s="T267">šiː-mɨn</ta>
            <ta e="T269" id="Seg_1812" s="T268">ılla</ta>
            <ta e="T270" id="Seg_1813" s="T269">paktɨ-sɔː-tɨn</ta>
            <ta e="T272" id="Seg_1814" s="T271">nʼoma-j</ta>
            <ta e="T273" id="Seg_1815" s="T272">amʼa-sɨ-nɨ</ta>
            <ta e="T274" id="Seg_1816" s="T273">am-ɨ-r-nɔː-tɨn</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T6" id="Seg_1817" s="T5">nʼoma</ta>
            <ta e="T7" id="Seg_1818" s="T6">i</ta>
            <ta e="T8" id="Seg_1819" s="T7">külä</ta>
            <ta e="T9" id="Seg_1820" s="T8">ama-sɨ-qı</ta>
            <ta e="T10" id="Seg_1821" s="T9">ilɨ-mpɨ-qı</ta>
            <ta e="T11" id="Seg_1822" s="T10">ilɨ-mpɨ-qı</ta>
            <ta e="T13" id="Seg_1823" s="T12">nʼoma</ta>
            <ta e="T14" id="Seg_1824" s="T13">nʼoma</ta>
            <ta e="T15" id="Seg_1825" s="T14">šöt-ntɨ</ta>
            <ta e="T16" id="Seg_1826" s="T15">šöt-ntɨ</ta>
            <ta e="T17" id="Seg_1827" s="T16">qən-lä</ta>
            <ta e="T18" id="Seg_1828" s="T17">šöt-ntɨ</ta>
            <ta e="T19" id="Seg_1829" s="T18">qən-mpɨ</ta>
            <ta e="T20" id="Seg_1830" s="T19">i</ta>
            <ta e="T21" id="Seg_1831" s="T20">šittälʼ</ta>
            <ta e="T23" id="Seg_1832" s="T22">ukkɨr</ta>
            <ta e="T24" id="Seg_1833" s="T23">ira-laka</ta>
            <ta e="T26" id="Seg_1834" s="T25">ür</ta>
            <ta e="T27" id="Seg_1835" s="T26">qaqlɨ</ta>
            <ta e="T28" id="Seg_1836" s="T27">üqɨl-mpɨ-ntɨ-tɨ</ta>
            <ta e="T30" id="Seg_1837" s="T29">šittälʼ</ta>
            <ta e="T31" id="Seg_1838" s="T30">qäl-lä</ta>
            <ta e="T32" id="Seg_1839" s="T31">čʼoš-ɨ-laka</ta>
            <ta e="T33" id="Seg_1840" s="T32">iː-mpɨ-ntɨ-tɨ</ta>
            <ta e="T34" id="Seg_1841" s="T33">moqɨnä</ta>
            <ta e="T35" id="Seg_1842" s="T34">tü-mpɨ</ta>
            <ta e="T37" id="Seg_1843" s="T36">nʼoma</ta>
            <ta e="T38" id="Seg_1844" s="T37">iːja-iː-tɨ</ta>
            <ta e="T39" id="Seg_1845" s="T38">tammɨ</ta>
            <ta e="T40" id="Seg_1846" s="T39">nɔːtɨ</ta>
            <ta e="T41" id="Seg_1847" s="T40">puːn</ta>
            <ta e="T42" id="Seg_1848" s="T41">külä</ta>
            <ta e="T43" id="Seg_1849" s="T42">iːja-iː-tɨ</ta>
            <ta e="T44" id="Seg_1850" s="T43">tü-nnɨnt</ta>
            <ta e="T46" id="Seg_1851" s="T45">šittälʼ</ta>
            <ta e="T47" id="Seg_1852" s="T46">nʼoma</ta>
            <ta e="T48" id="Seg_1853" s="T47">nʼoma-n</ta>
            <ta e="T49" id="Seg_1854" s="T48">iːja-ja</ta>
            <ta e="T50" id="Seg_1855" s="T49">i</ta>
            <ta e="T51" id="Seg_1856" s="T50">külä-n</ta>
            <ta e="T52" id="Seg_1857" s="T51">iːja-ja</ta>
            <ta e="T53" id="Seg_1858" s="T52">pija-tɨ-kkɨ-sɨ-tɨt</ta>
            <ta e="T55" id="Seg_1859" s="T54">külä-n</ta>
            <ta e="T56" id="Seg_1860" s="T55">iːja-ja</ta>
            <ta e="T57" id="Seg_1861" s="T56">moqɨnä</ta>
            <ta e="T58" id="Seg_1862" s="T57">qən-lä</ta>
            <ta e="T59" id="Seg_1863" s="T58">nık</ta>
            <ta e="T60" id="Seg_1864" s="T59">kətɨ-mpɨ-tɨ</ta>
            <ta e="T62" id="Seg_1865" s="T61">tına</ta>
            <ta e="T63" id="Seg_1866" s="T62">nʼoma</ta>
            <ta e="T64" id="Seg_1867" s="T63">tɨqa</ta>
            <ta e="T65" id="Seg_1868" s="T64">tına</ta>
            <ta e="T66" id="Seg_1869" s="T65">ür-laka</ta>
            <ta e="T67" id="Seg_1870" s="T66">taːtɨ-sɨ-tɨ</ta>
            <ta e="T69" id="Seg_1871" s="T68">nʼoma-ja</ta>
            <ta e="T70" id="Seg_1872" s="T69">i</ta>
            <ta e="T71" id="Seg_1873" s="T70">külä-ja</ta>
            <ta e="T72" id="Seg_1874" s="T71">ama-sɨ-qı</ta>
            <ta e="T73" id="Seg_1875" s="T72">ilɨ</ta>
            <ta e="T74" id="Seg_1876" s="T73">ilɨ-mpɨ-qı</ta>
            <ta e="T75" id="Seg_1877" s="T74">ilɨ-mpɨ-qı</ta>
            <ta e="T77" id="Seg_1878" s="T76">nʼoma</ta>
            <ta e="T78" id="Seg_1879" s="T77">nʼoma</ta>
            <ta e="T79" id="Seg_1880" s="T78">tına</ta>
            <ta e="T80" id="Seg_1881" s="T79">šöt-ntɨ</ta>
            <ta e="T81" id="Seg_1882" s="T80">qən-mpɨ</ta>
            <ta e="T83" id="Seg_1883" s="T82">ukkɨr</ta>
            <ta e="T84" id="Seg_1884" s="T83">ira-laka</ta>
            <ta e="T85" id="Seg_1885" s="T84">čʼoš</ta>
            <ta e="T86" id="Seg_1886" s="T85">qaqlɨ</ta>
            <ta e="T87" id="Seg_1887" s="T86">üqɨl-mpɨ-ntɨ-tɨ</ta>
            <ta e="T89" id="Seg_1888" s="T88">nʼoma</ta>
            <ta e="T90" id="Seg_1889" s="T89">ama-sɨ-qı</ta>
            <ta e="T91" id="Seg_1890" s="T90">ilɨ-mpɨ-qı</ta>
            <ta e="T93" id="Seg_1891" s="T92">ukkɨr</ta>
            <ta e="T94" id="Seg_1892" s="T93">ira-laka</ta>
            <ta e="T95" id="Seg_1893" s="T94">qən-mpɨ-ŋɨ</ta>
            <ta e="T96" id="Seg_1894" s="T95">nʼoma</ta>
            <ta e="T97" id="Seg_1895" s="T96">šöt-ntɨ</ta>
            <ta e="T98" id="Seg_1896" s="T97">ukkɨr</ta>
            <ta e="T99" id="Seg_1897" s="T98">ira-laka</ta>
            <ta e="T100" id="Seg_1898" s="T99">čʼoš</ta>
            <ta e="T101" id="Seg_1899" s="T100">qaqlɨ</ta>
            <ta e="T102" id="Seg_1900" s="T101">üqɨl-mpɨ-ntɨ-tɨ</ta>
            <ta e="T104" id="Seg_1901" s="T103">nʼoma</ta>
            <ta e="T105" id="Seg_1902" s="T104">qən-mpɨ</ta>
            <ta e="T106" id="Seg_1903" s="T105">näčʼčʼä</ta>
            <ta e="T107" id="Seg_1904" s="T106">šittälʼ</ta>
            <ta e="T108" id="Seg_1905" s="T107">čʼoš-ɨ-laka</ta>
            <ta e="T109" id="Seg_1906" s="T108">orqɨl-mpɨ-ntɨ-tɨ</ta>
            <ta e="T110" id="Seg_1907" s="T109">šittɨ-mtäl</ta>
            <ta e="T111" id="Seg_1908" s="T110">moqɨnä</ta>
            <ta e="T112" id="Seg_1909" s="T111">qən-mpɨ</ta>
            <ta e="T114" id="Seg_1910" s="T113">šittälʼ</ta>
            <ta e="T115" id="Seg_1911" s="T114">moqɨnä</ta>
            <ta e="T116" id="Seg_1912" s="T115">qən-lä</ta>
            <ta e="T117" id="Seg_1913" s="T116">iːja-iː-tɨ</ta>
            <ta e="T118" id="Seg_1914" s="T117">apsɨ-tɨ-mpɨ-tɨ</ta>
            <ta e="T120" id="Seg_1915" s="T119">iːja-iː-tɨ</ta>
            <ta e="T121" id="Seg_1916" s="T120">ukkɨr</ta>
            <ta e="T122" id="Seg_1917" s="T121">iːja-ja</ta>
            <ta e="T123" id="Seg_1918" s="T122">nʼoma-n</ta>
            <ta e="T124" id="Seg_1919" s="T123">külä-n</ta>
            <ta e="T125" id="Seg_1920" s="T124">iːja-ja</ta>
            <ta e="T126" id="Seg_1921" s="T125">pija-tɨ-kkɨ-ŋɨ-tɨ</ta>
            <ta e="T128" id="Seg_1922" s="T127">külä</ta>
            <ta e="T129" id="Seg_1923" s="T128">külä-n</ta>
            <ta e="T130" id="Seg_1924" s="T129">iːja-ja</ta>
            <ta e="T131" id="Seg_1925" s="T130">moqɨnä</ta>
            <ta e="T132" id="Seg_1926" s="T131">qən-lä</ta>
            <ta e="T133" id="Seg_1927" s="T132">ama-ntɨ</ta>
            <ta e="T134" id="Seg_1928" s="T133">nık</ta>
            <ta e="T135" id="Seg_1929" s="T134">kətɨ-mpɨ-tɨt</ta>
            <ta e="T137" id="Seg_1930" s="T136">nʼoma</ta>
            <ta e="T138" id="Seg_1931" s="T137">nʼoma</ta>
            <ta e="T139" id="Seg_1932" s="T138">iːja-iː-tɨ</ta>
            <ta e="T140" id="Seg_1933" s="T139">ür-ɨ-laka-m</ta>
            <ta e="T141" id="Seg_1934" s="T140">orqɨl-mpɨ-tɨ</ta>
            <ta e="T143" id="Seg_1935" s="T142">šittälʼ</ta>
            <ta e="T144" id="Seg_1936" s="T143">külä</ta>
            <ta e="T145" id="Seg_1937" s="T144">toː</ta>
            <ta e="T146" id="Seg_1938" s="T145">mɔːt</ta>
            <ta e="T147" id="Seg_1939" s="T146">qən-mpɨ</ta>
            <ta e="T149" id="Seg_1940" s="T148">külä</ta>
            <ta e="T150" id="Seg_1941" s="T149">toː</ta>
            <ta e="T151" id="Seg_1942" s="T150">mɔːt</ta>
            <ta e="T152" id="Seg_1943" s="T151">qən-mpɨ</ta>
            <ta e="T153" id="Seg_1944" s="T152">šittälʼ</ta>
            <ta e="T154" id="Seg_1945" s="T153">nık</ta>
            <ta e="T155" id="Seg_1946" s="T154">kətɨ-mpɨ-tɨ</ta>
            <ta e="T157" id="Seg_1947" s="T156">tan</ta>
            <ta e="T158" id="Seg_1948" s="T157">kun-ɨ-n</ta>
            <ta e="T159" id="Seg_1949" s="T158">čʼoš-ɨ-laka</ta>
            <ta e="T160" id="Seg_1950" s="T159">qo-sɨ-l</ta>
            <ta e="T162" id="Seg_1951" s="T161">nʼoma</ta>
            <ta e="T163" id="Seg_1952" s="T162">nık</ta>
            <ta e="T164" id="Seg_1953" s="T163">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T166" id="Seg_1954" s="T165">man</ta>
            <ta e="T167" id="Seg_1955" s="T166">šöt-qɨn-ntɨ</ta>
            <ta e="T168" id="Seg_1956" s="T167">ira-laka</ta>
            <ta e="T169" id="Seg_1957" s="T168">qo-ntɨr-sɨ-m</ta>
            <ta e="T170" id="Seg_1958" s="T169">šittälʼ</ta>
            <ta e="T173" id="Seg_1959" s="T172">ür</ta>
            <ta e="T174" id="Seg_1960" s="T173">qaqlɨ</ta>
            <ta e="T175" id="Seg_1961" s="T174">üqɨl-mpɨ-ntɨ-tɨ</ta>
            <ta e="T177" id="Seg_1962" s="T176">šittälʼ</ta>
            <ta e="T178" id="Seg_1963" s="T177">šöt-ntɨ</ta>
            <ta e="T179" id="Seg_1964" s="T178">qən-mpɨ</ta>
            <ta e="T181" id="Seg_1965" s="T180">nʼoma</ta>
            <ta e="T182" id="Seg_1966" s="T181">qɔːt-ɨ</ta>
            <ta e="T183" id="Seg_1967" s="T182">pɔːrɨ</ta>
            <ta e="T184" id="Seg_1968" s="T183">sɨrɨ-sä</ta>
            <ta e="T185" id="Seg_1969" s="T184">nat-qɨl-mpɨ-tɨ</ta>
            <ta e="T186" id="Seg_1970" s="T185">nık</ta>
            <ta e="T187" id="Seg_1971" s="T186">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T189" id="Seg_1972" s="T188">nannɛr</ta>
            <ta e="T190" id="Seg_1973" s="T189">pusqa-tɨ-ɔːl-lä</ta>
            <ta e="T191" id="Seg_1974" s="T190">šittälʼ</ta>
            <ta e="T192" id="Seg_1975" s="T191">ašša</ta>
            <ta e="T193" id="Seg_1976" s="T192">atɨ</ta>
            <ta e="T195" id="Seg_1977" s="T194">külä</ta>
            <ta e="T196" id="Seg_1978" s="T195">qaqlɨ-n</ta>
            <ta e="T197" id="Seg_1979" s="T196">omtɨ-ıː-lä</ta>
            <ta e="T198" id="Seg_1980" s="T197">küːk</ta>
            <ta e="T199" id="Seg_1981" s="T198">qaj-laka</ta>
            <ta e="T200" id="Seg_1982" s="T199">apsɨ-laka</ta>
            <ta e="T201" id="Seg_1983" s="T200">orqɨl-mpɨ-tɨ</ta>
            <ta e="T203" id="Seg_1984" s="T202">šittälʼ</ta>
            <ta e="T204" id="Seg_1985" s="T203">ira-laka</ta>
            <ta e="T205" id="Seg_1986" s="T204">sukɨltä</ta>
            <ta e="T206" id="Seg_1987" s="T205">piːqɨl-lä</ta>
            <ta e="T207" id="Seg_1988" s="T206">pürü-sä</ta>
            <ta e="T208" id="Seg_1989" s="T207">qättɨ-mpɨ-tɨ</ta>
            <ta e="T209" id="Seg_1990" s="T208">ıllä</ta>
            <ta e="T210" id="Seg_1991" s="T209">qu-lʼčʼɨ-mpɨ</ta>
            <ta e="T214" id="Seg_1992" s="T213">nʼoma</ta>
            <ta e="T215" id="Seg_1993" s="T214">tına</ta>
            <ta e="T216" id="Seg_1994" s="T215">qən-mpɨ-n</ta>
            <ta e="T217" id="Seg_1995" s="T216">näčʼčʼä</ta>
            <ta e="T219" id="Seg_1996" s="T218">čʼoš-laka</ta>
            <ta e="T220" id="Seg_1997" s="T219">orqɨl-mpɨ-ntɨ-tɨ</ta>
            <ta e="T221" id="Seg_1998" s="T220">šittälʼ</ta>
            <ta e="T222" id="Seg_1999" s="T221">moqɨnä</ta>
            <ta e="T223" id="Seg_2000" s="T222">paktɨ</ta>
            <ta e="T225" id="Seg_2001" s="T224">iːja-ja-iː-m-tɨ</ta>
            <ta e="T226" id="Seg_2002" s="T225">apsɨ-tɨ-mpɨ-tɨ</ta>
            <ta e="T228" id="Seg_2003" s="T227">külä</ta>
            <ta e="T229" id="Seg_2004" s="T228">külä-n</ta>
            <ta e="T230" id="Seg_2005" s="T229">iːja-ja</ta>
            <ta e="T231" id="Seg_2006" s="T230">nık</ta>
            <ta e="T232" id="Seg_2007" s="T231">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T233" id="Seg_2008" s="T232">ama</ta>
            <ta e="T234" id="Seg_2009" s="T233">qaj</ta>
            <ta e="T235" id="Seg_2010" s="T234">meː-ntɨ-tɨ</ta>
            <ta e="T236" id="Seg_2011" s="T235">ama-lɨ</ta>
            <ta e="T237" id="Seg_2012" s="T236">čʼoš-ɨ-laka</ta>
            <ta e="T238" id="Seg_2013" s="T237">tačʼal-mpɨ-ntɨ-tɨ</ta>
            <ta e="T240" id="Seg_2014" s="T239">šittälʼ</ta>
            <ta e="T241" id="Seg_2015" s="T240">nʼoma</ta>
            <ta e="T242" id="Seg_2016" s="T241">šittälʼ</ta>
            <ta e="T244" id="Seg_2017" s="T243">moqɨnä</ta>
            <ta e="T245" id="Seg_2018" s="T244">qən-lä</ta>
            <ta e="T246" id="Seg_2019" s="T245">ɔːntɨ-ätɔːl-mpɨ</ta>
            <ta e="T248" id="Seg_2020" s="T247">nʼoma-n</ta>
            <ta e="T249" id="Seg_2021" s="T248">am-ɨ-r-ŋɨ-tɨt</ta>
            <ta e="T250" id="Seg_2022" s="T249">na</ta>
            <ta e="T251" id="Seg_2023" s="T250">puːn</ta>
            <ta e="T252" id="Seg_2024" s="T251">nık</ta>
            <ta e="T253" id="Seg_2025" s="T252">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T254" id="Seg_2026" s="T253">ama-lɨ</ta>
            <ta e="T255" id="Seg_2027" s="T254">qattɨ-sɨ</ta>
            <ta e="T257" id="Seg_2028" s="T256">ama-lɨ</ta>
            <ta e="T258" id="Seg_2029" s="T257">qattɨ-sɨ-tɨ</ta>
            <ta e="T259" id="Seg_2030" s="T258">ama-ntɨ-ntɨ</ta>
            <ta e="T260" id="Seg_2031" s="T259">ira-laka</ta>
            <ta e="T262" id="Seg_2032" s="T261">qättɨ-sɨ-tɨ</ta>
            <ta e="T264" id="Seg_2033" s="T263">muntɨk</ta>
            <ta e="T265" id="Seg_2034" s="T264">čʼuːrɨ-lä</ta>
            <ta e="T266" id="Seg_2035" s="T265">kɨpa</ta>
            <ta e="T267" id="Seg_2036" s="T266">mü</ta>
            <ta e="T268" id="Seg_2037" s="T267">šüː-mɨn</ta>
            <ta e="T269" id="Seg_2038" s="T268">ıllä</ta>
            <ta e="T270" id="Seg_2039" s="T269">paktɨ-sɨ-tɨt</ta>
            <ta e="T272" id="Seg_2040" s="T271">nʼoma-lʼ</ta>
            <ta e="T273" id="Seg_2041" s="T272">ama-sɨ-nɨ</ta>
            <ta e="T274" id="Seg_2042" s="T273">am-ɨ-r-ŋɨ-tɨt</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T6" id="Seg_2043" s="T5">hare.[NOM]</ta>
            <ta e="T7" id="Seg_2044" s="T6">and</ta>
            <ta e="T8" id="Seg_2045" s="T7">raven.[NOM]</ta>
            <ta e="T9" id="Seg_2046" s="T8">mother-DYA-DU.[NOM]</ta>
            <ta e="T10" id="Seg_2047" s="T9">live-PST.NAR-3DU.S</ta>
            <ta e="T11" id="Seg_2048" s="T10">live-PST.NAR-3DU.S</ta>
            <ta e="T13" id="Seg_2049" s="T12">hare.[NOM]</ta>
            <ta e="T14" id="Seg_2050" s="T13">hare.[NOM]</ta>
            <ta e="T15" id="Seg_2051" s="T14">forest-ILL</ta>
            <ta e="T16" id="Seg_2052" s="T15">forest-ILL</ta>
            <ta e="T17" id="Seg_2053" s="T16">go.away-CVB</ta>
            <ta e="T18" id="Seg_2054" s="T17">forest-ILL</ta>
            <ta e="T19" id="Seg_2055" s="T18">go.away-PST.NAR.[3SG.S]</ta>
            <ta e="T20" id="Seg_2056" s="T19">and</ta>
            <ta e="T21" id="Seg_2057" s="T20">then</ta>
            <ta e="T23" id="Seg_2058" s="T22">one</ta>
            <ta e="T24" id="Seg_2059" s="T23">old.man-SNGL.[NOM]</ta>
            <ta e="T26" id="Seg_2060" s="T25">fat.[NOM]</ta>
            <ta e="T27" id="Seg_2061" s="T26">sledge.[NOM]</ta>
            <ta e="T28" id="Seg_2062" s="T27">drag-PST.NAR-INFER-3SG.O</ta>
            <ta e="T30" id="Seg_2063" s="T29">then</ta>
            <ta e="T31" id="Seg_2064" s="T30">go-CVB</ta>
            <ta e="T32" id="Seg_2065" s="T31">fat-EP-SNGL.[NOM]</ta>
            <ta e="T33" id="Seg_2066" s="T32">take-PST.NAR-INFER-3SG.O</ta>
            <ta e="T34" id="Seg_2067" s="T33">home</ta>
            <ta e="T35" id="Seg_2068" s="T34">come-PST.NAR.[3SG.S]</ta>
            <ta e="T37" id="Seg_2069" s="T36">hare.[NOM]</ta>
            <ta e="T38" id="Seg_2070" s="T37">child-PL.[NOM]-3SG</ta>
            <ta e="T39" id="Seg_2071" s="T38">this</ta>
            <ta e="T40" id="Seg_2072" s="T39">then</ta>
            <ta e="T41" id="Seg_2073" s="T40">afterwards</ta>
            <ta e="T42" id="Seg_2074" s="T41">raven.[NOM]</ta>
            <ta e="T43" id="Seg_2075" s="T42">child-PL.[NOM]-3SG</ta>
            <ta e="T44" id="Seg_2076" s="T43">come-%%</ta>
            <ta e="T46" id="Seg_2077" s="T45">then</ta>
            <ta e="T47" id="Seg_2078" s="T46">hare.[NOM]</ta>
            <ta e="T48" id="Seg_2079" s="T47">hare-GEN</ta>
            <ta e="T49" id="Seg_2080" s="T48">child-DIM.[NOM]</ta>
            <ta e="T50" id="Seg_2081" s="T49">and</ta>
            <ta e="T51" id="Seg_2082" s="T50">raven-GEN</ta>
            <ta e="T52" id="Seg_2083" s="T51">child-DIM.[NOM]</ta>
            <ta e="T53" id="Seg_2084" s="T52">%%-TR-HAB-PST-3PL</ta>
            <ta e="T55" id="Seg_2085" s="T54">raven-GEN</ta>
            <ta e="T56" id="Seg_2086" s="T55">child-DIM.[NOM]</ta>
            <ta e="T57" id="Seg_2087" s="T56">home</ta>
            <ta e="T58" id="Seg_2088" s="T57">leave-CVB</ta>
            <ta e="T59" id="Seg_2089" s="T58">so</ta>
            <ta e="T60" id="Seg_2090" s="T59">say-DUR-3SG.O</ta>
            <ta e="T62" id="Seg_2091" s="T61">that</ta>
            <ta e="T63" id="Seg_2092" s="T62">hare.[NOM]</ta>
            <ta e="T64" id="Seg_2093" s="T63">%%</ta>
            <ta e="T65" id="Seg_2094" s="T64">that</ta>
            <ta e="T66" id="Seg_2095" s="T65">fat-SNGL.[NOM]</ta>
            <ta e="T67" id="Seg_2096" s="T66">bring-PST-3SG.O</ta>
            <ta e="T69" id="Seg_2097" s="T68">hare-DIM.[NOM]</ta>
            <ta e="T70" id="Seg_2098" s="T69">and</ta>
            <ta e="T71" id="Seg_2099" s="T70">raven-DIM.[NOM]</ta>
            <ta e="T72" id="Seg_2100" s="T71">mother-DYA-DU.[NOM]</ta>
            <ta e="T73" id="Seg_2101" s="T72">live.[3SG.S]</ta>
            <ta e="T74" id="Seg_2102" s="T73">live-PST.NAR-3DU.S</ta>
            <ta e="T75" id="Seg_2103" s="T74">live-PST.NAR-3DU.S</ta>
            <ta e="T77" id="Seg_2104" s="T76">hare.[NOM]</ta>
            <ta e="T78" id="Seg_2105" s="T77">hare.[NOM]</ta>
            <ta e="T79" id="Seg_2106" s="T78">that</ta>
            <ta e="T80" id="Seg_2107" s="T79">forest-ILL</ta>
            <ta e="T81" id="Seg_2108" s="T80">go.away-PST.NAR.[3SG.S]</ta>
            <ta e="T83" id="Seg_2109" s="T82">one</ta>
            <ta e="T84" id="Seg_2110" s="T83">old.man-SNGL.[NOM]</ta>
            <ta e="T85" id="Seg_2111" s="T84">fat.[NOM]</ta>
            <ta e="T86" id="Seg_2112" s="T85">sledge.[NOM]</ta>
            <ta e="T87" id="Seg_2113" s="T86">drag-PST.NAR-INFER-3SG.O</ta>
            <ta e="T89" id="Seg_2114" s="T88">hare.[NOM]</ta>
            <ta e="T90" id="Seg_2115" s="T89">mother-DYA-DU.[NOM]</ta>
            <ta e="T91" id="Seg_2116" s="T90">live-PST.NAR-3DU.S</ta>
            <ta e="T93" id="Seg_2117" s="T92">one</ta>
            <ta e="T94" id="Seg_2118" s="T93">old.man-SNGL.[NOM]</ta>
            <ta e="T95" id="Seg_2119" s="T94">go.away-DUR-CO.[3SG.S]</ta>
            <ta e="T96" id="Seg_2120" s="T95">hare.[NOM]</ta>
            <ta e="T97" id="Seg_2121" s="T96">forest-ILL</ta>
            <ta e="T98" id="Seg_2122" s="T97">one</ta>
            <ta e="T99" id="Seg_2123" s="T98">old.man-SNGL.[NOM]</ta>
            <ta e="T100" id="Seg_2124" s="T99">fat.[NOM]</ta>
            <ta e="T101" id="Seg_2125" s="T100">sledge.[NOM]</ta>
            <ta e="T102" id="Seg_2126" s="T101">drag-PST.NAR-INFER-3SG.O</ta>
            <ta e="T104" id="Seg_2127" s="T103">hare.[NOM]</ta>
            <ta e="T105" id="Seg_2128" s="T104">go.away-PST.NAR.[3SG.S]</ta>
            <ta e="T106" id="Seg_2129" s="T105">there</ta>
            <ta e="T107" id="Seg_2130" s="T106">then</ta>
            <ta e="T108" id="Seg_2131" s="T107">fat-EP-SNGL.[NOM]</ta>
            <ta e="T109" id="Seg_2132" s="T108">catch-PST.NAR-INFER-3SG.O</ta>
            <ta e="T110" id="Seg_2133" s="T109">two-ITER.NUM</ta>
            <ta e="T111" id="Seg_2134" s="T110">home</ta>
            <ta e="T112" id="Seg_2135" s="T111">go.away-PST.NAR.[3SG.S]</ta>
            <ta e="T114" id="Seg_2136" s="T113">then</ta>
            <ta e="T115" id="Seg_2137" s="T114">home</ta>
            <ta e="T116" id="Seg_2138" s="T115">go.away-CVB</ta>
            <ta e="T117" id="Seg_2139" s="T116">son-PL.[NOM]-3SG</ta>
            <ta e="T118" id="Seg_2140" s="T117">food-TR-PST.NAR-3SG.O</ta>
            <ta e="T120" id="Seg_2141" s="T119">son-PL.[NOM]-3SG</ta>
            <ta e="T121" id="Seg_2142" s="T120">one</ta>
            <ta e="T122" id="Seg_2143" s="T121">child-DIM.[NOM]</ta>
            <ta e="T123" id="Seg_2144" s="T122">hare-GEN</ta>
            <ta e="T124" id="Seg_2145" s="T123">raven-GEN</ta>
            <ta e="T125" id="Seg_2146" s="T124">child-DIM.[NOM]</ta>
            <ta e="T126" id="Seg_2147" s="T125">%%-TR-HAB-CO-3SG.O</ta>
            <ta e="T128" id="Seg_2148" s="T127">raven.[NOM]</ta>
            <ta e="T129" id="Seg_2149" s="T128">raven-GEN</ta>
            <ta e="T130" id="Seg_2150" s="T129">child-DIM.[NOM]</ta>
            <ta e="T131" id="Seg_2151" s="T130">home</ta>
            <ta e="T132" id="Seg_2152" s="T131">go.away-CVB</ta>
            <ta e="T133" id="Seg_2153" s="T132">mother-ILL</ta>
            <ta e="T134" id="Seg_2154" s="T133">so</ta>
            <ta e="T135" id="Seg_2155" s="T134">say-PST.NAR-3PL</ta>
            <ta e="T137" id="Seg_2156" s="T136">hare.[NOM]</ta>
            <ta e="T138" id="Seg_2157" s="T137">hare.[NOM]</ta>
            <ta e="T139" id="Seg_2158" s="T138">son-PL.[NOM]-3SG</ta>
            <ta e="T140" id="Seg_2159" s="T139">fat-EP-SNGL-ACC</ta>
            <ta e="T141" id="Seg_2160" s="T140">catch-PST.NAR-3SG.O</ta>
            <ta e="T143" id="Seg_2161" s="T142">then</ta>
            <ta e="T144" id="Seg_2162" s="T143">raven.[NOM]</ta>
            <ta e="T145" id="Seg_2163" s="T144">away</ta>
            <ta e="T146" id="Seg_2164" s="T145">house.[NOM]</ta>
            <ta e="T147" id="Seg_2165" s="T146">go.away-PST.NAR.[3SG.S]</ta>
            <ta e="T149" id="Seg_2166" s="T148">raven.[NOM]</ta>
            <ta e="T150" id="Seg_2167" s="T149">away</ta>
            <ta e="T151" id="Seg_2168" s="T150">house.[NOM]</ta>
            <ta e="T152" id="Seg_2169" s="T151">go.away-PST.NAR.[3SG.S]</ta>
            <ta e="T153" id="Seg_2170" s="T152">then</ta>
            <ta e="T154" id="Seg_2171" s="T153">so</ta>
            <ta e="T155" id="Seg_2172" s="T154">say-PST.NAR-3SG.O</ta>
            <ta e="T157" id="Seg_2173" s="T156">you.SG.NOM</ta>
            <ta e="T158" id="Seg_2174" s="T157">where-EP-ADV.LOC</ta>
            <ta e="T159" id="Seg_2175" s="T158">fat-EP-SNGL.[NOM]</ta>
            <ta e="T160" id="Seg_2176" s="T159">find-PST-2SG.O</ta>
            <ta e="T162" id="Seg_2177" s="T161">hare.[NOM]</ta>
            <ta e="T163" id="Seg_2178" s="T162">so</ta>
            <ta e="T164" id="Seg_2179" s="T163">say-CO-3SG.O</ta>
            <ta e="T166" id="Seg_2180" s="T165">I.NOM</ta>
            <ta e="T167" id="Seg_2181" s="T166">forest-LOC-OBL.3SG</ta>
            <ta e="T168" id="Seg_2182" s="T167">old.man-SNGL.[NOM]</ta>
            <ta e="T169" id="Seg_2183" s="T168">see-DRV-PST-1SG.O</ta>
            <ta e="T170" id="Seg_2184" s="T169">then</ta>
            <ta e="T173" id="Seg_2185" s="T172">fat.[NOM]</ta>
            <ta e="T174" id="Seg_2186" s="T173">sledge.[NOM]</ta>
            <ta e="T175" id="Seg_2187" s="T174">drag-PST.NAR-INFER-3SG.O</ta>
            <ta e="T177" id="Seg_2188" s="T176">then</ta>
            <ta e="T178" id="Seg_2189" s="T177">forest-ILL</ta>
            <ta e="T179" id="Seg_2190" s="T178">go.away-PST.NAR.[3SG.S]</ta>
            <ta e="T181" id="Seg_2191" s="T180">hare.[NOM]</ta>
            <ta e="T182" id="Seg_2192" s="T181">forehead-EP.[NOM]</ta>
            <ta e="T183" id="Seg_2193" s="T182">top.[NOM]</ta>
            <ta e="T184" id="Seg_2194" s="T183">snow-INSTR</ta>
            <ta e="T185" id="Seg_2195" s="T184">anoint-MULO-PST.NAR-3SG.O</ta>
            <ta e="T186" id="Seg_2196" s="T185">so</ta>
            <ta e="T187" id="Seg_2197" s="T186">say-CO-3SG.O</ta>
            <ta e="T189" id="Seg_2198" s="T188">so.much</ta>
            <ta e="T190" id="Seg_2199" s="T189">storm-TR-MOM-CVB</ta>
            <ta e="T191" id="Seg_2200" s="T190">then</ta>
            <ta e="T192" id="Seg_2201" s="T191">NEG</ta>
            <ta e="T193" id="Seg_2202" s="T192">be.visible.[3SG.S]</ta>
            <ta e="T195" id="Seg_2203" s="T194">raven.[NOM]</ta>
            <ta e="T196" id="Seg_2204" s="T195">sledge-ADV.LOC</ta>
            <ta e="T197" id="Seg_2205" s="T196">sit.down-RFL.PFV-CVB</ta>
            <ta e="T198" id="Seg_2206" s="T197">kraa</ta>
            <ta e="T199" id="Seg_2207" s="T198">what-SNGL.[NOM]</ta>
            <ta e="T200" id="Seg_2208" s="T199">food-SNGL.[NOM]</ta>
            <ta e="T201" id="Seg_2209" s="T200">catch-PST.NAR-3SG.O</ta>
            <ta e="T203" id="Seg_2210" s="T202">then</ta>
            <ta e="T204" id="Seg_2211" s="T203">old.man-SNGL.[NOM]</ta>
            <ta e="T205" id="Seg_2212" s="T204">back</ta>
            <ta e="T206" id="Seg_2213" s="T205">turn-CVB</ta>
            <ta e="T207" id="Seg_2214" s="T206">%%-INSTR</ta>
            <ta e="T208" id="Seg_2215" s="T207">hit-PST.NAR-3SG.O</ta>
            <ta e="T209" id="Seg_2216" s="T208">down</ta>
            <ta e="T210" id="Seg_2217" s="T209">die-PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T214" id="Seg_2218" s="T213">hare.[NOM]</ta>
            <ta e="T215" id="Seg_2219" s="T214">that</ta>
            <ta e="T216" id="Seg_2220" s="T215">leave-PST.NAR-3SG.S</ta>
            <ta e="T217" id="Seg_2221" s="T216">there</ta>
            <ta e="T219" id="Seg_2222" s="T218">fat-SNGL.[NOM]</ta>
            <ta e="T220" id="Seg_2223" s="T219">catch-PST.NAR-INFER-3SG.O</ta>
            <ta e="T221" id="Seg_2224" s="T220">then</ta>
            <ta e="T222" id="Seg_2225" s="T221">home</ta>
            <ta e="T223" id="Seg_2226" s="T222">run.[3SG.S]</ta>
            <ta e="T225" id="Seg_2227" s="T224">child-DIM-PL-ACC-3SG</ta>
            <ta e="T226" id="Seg_2228" s="T225">food-TR-PST.NAR-3SG.O</ta>
            <ta e="T228" id="Seg_2229" s="T227">raven.[NOM]</ta>
            <ta e="T229" id="Seg_2230" s="T228">raven-GEN</ta>
            <ta e="T230" id="Seg_2231" s="T229">child-DIM.[NOM]</ta>
            <ta e="T231" id="Seg_2232" s="T230">so</ta>
            <ta e="T232" id="Seg_2233" s="T231">say-CO-3SG.O</ta>
            <ta e="T233" id="Seg_2234" s="T232">mother.[NOM]</ta>
            <ta e="T234" id="Seg_2235" s="T233">what.[NOM]</ta>
            <ta e="T235" id="Seg_2236" s="T234">make-IPFV-3SG.O</ta>
            <ta e="T236" id="Seg_2237" s="T235">mother.[NOM]-2SG</ta>
            <ta e="T237" id="Seg_2238" s="T236">fat-EP-SNGL.[NOM]</ta>
            <ta e="T238" id="Seg_2239" s="T237">not.can-DUR-INFER-3SG.O</ta>
            <ta e="T240" id="Seg_2240" s="T239">then</ta>
            <ta e="T241" id="Seg_2241" s="T240">hare.[NOM]</ta>
            <ta e="T242" id="Seg_2242" s="T241">then</ta>
            <ta e="T244" id="Seg_2243" s="T243">home</ta>
            <ta e="T245" id="Seg_2244" s="T244">go.away-CVB</ta>
            <ta e="T246" id="Seg_2245" s="T245">be.happy-MOM-PST.NAR.[3SG.S]</ta>
            <ta e="T248" id="Seg_2246" s="T247">hare-GEN</ta>
            <ta e="T249" id="Seg_2247" s="T248">eat-EP-FRQ-CO-3PL</ta>
            <ta e="T250" id="Seg_2248" s="T249">this</ta>
            <ta e="T251" id="Seg_2249" s="T250">afterwards</ta>
            <ta e="T252" id="Seg_2250" s="T251">so</ta>
            <ta e="T253" id="Seg_2251" s="T252">say-CO-3SG.O</ta>
            <ta e="T254" id="Seg_2252" s="T253">mother.[NOM]-2SG</ta>
            <ta e="T255" id="Seg_2253" s="T254">where.to.go-PST.[3SG.S]</ta>
            <ta e="T257" id="Seg_2254" s="T256">mother.[NOM]-2SG</ta>
            <ta e="T258" id="Seg_2255" s="T257">where.to.go-PST-3SG.O</ta>
            <ta e="T259" id="Seg_2256" s="T258">mother-ILL-OBL.2SG</ta>
            <ta e="T260" id="Seg_2257" s="T259">old.man-SNGL.[NOM]</ta>
            <ta e="T262" id="Seg_2258" s="T261">hit-PST-3SG.O</ta>
            <ta e="T264" id="Seg_2259" s="T263">all</ta>
            <ta e="T265" id="Seg_2260" s="T264">cry-CVB</ta>
            <ta e="T266" id="Seg_2261" s="T265">small</ta>
            <ta e="T267" id="Seg_2262" s="T266">hole.[NOM]</ta>
            <ta e="T268" id="Seg_2263" s="T267">into-PROL</ta>
            <ta e="T269" id="Seg_2264" s="T268">down</ta>
            <ta e="T270" id="Seg_2265" s="T269">run-PST-3PL</ta>
            <ta e="T272" id="Seg_2266" s="T271">hare-ADJZ</ta>
            <ta e="T273" id="Seg_2267" s="T272">mother-DYA-%%</ta>
            <ta e="T274" id="Seg_2268" s="T273">eat-EP-FRQ-CO-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T6" id="Seg_2269" s="T5">заяц.[NOM]</ta>
            <ta e="T7" id="Seg_2270" s="T6">и</ta>
            <ta e="T8" id="Seg_2271" s="T7">ворон.[NOM]</ta>
            <ta e="T9" id="Seg_2272" s="T8">мать-DYA-DU.[NOM]</ta>
            <ta e="T10" id="Seg_2273" s="T9">жить-PST.NAR-3DU.S</ta>
            <ta e="T11" id="Seg_2274" s="T10">жить-PST.NAR-3DU.S</ta>
            <ta e="T13" id="Seg_2275" s="T12">заяц.[NOM]</ta>
            <ta e="T14" id="Seg_2276" s="T13">заяц.[NOM]</ta>
            <ta e="T15" id="Seg_2277" s="T14">лес-ILL</ta>
            <ta e="T16" id="Seg_2278" s="T15">лес-ILL</ta>
            <ta e="T17" id="Seg_2279" s="T16">уйти-CVB</ta>
            <ta e="T18" id="Seg_2280" s="T17">лес-ILL</ta>
            <ta e="T19" id="Seg_2281" s="T18">уйти-PST.NAR.[3SG.S]</ta>
            <ta e="T20" id="Seg_2282" s="T19">и</ta>
            <ta e="T21" id="Seg_2283" s="T20">потом</ta>
            <ta e="T23" id="Seg_2284" s="T22">один</ta>
            <ta e="T24" id="Seg_2285" s="T23">старик-SNGL.[NOM]</ta>
            <ta e="T26" id="Seg_2286" s="T25">жир.[NOM]</ta>
            <ta e="T27" id="Seg_2287" s="T26">нарты.[NOM]</ta>
            <ta e="T28" id="Seg_2288" s="T27">потащить-PST.NAR-INFER-3SG.O</ta>
            <ta e="T30" id="Seg_2289" s="T29">потом</ta>
            <ta e="T31" id="Seg_2290" s="T30">ходить-CVB</ta>
            <ta e="T32" id="Seg_2291" s="T31">сало-EP-SNGL.[NOM]</ta>
            <ta e="T33" id="Seg_2292" s="T32">взять-PST.NAR-INFER-3SG.O</ta>
            <ta e="T34" id="Seg_2293" s="T33">домой</ta>
            <ta e="T35" id="Seg_2294" s="T34">прийти-PST.NAR.[3SG.S]</ta>
            <ta e="T37" id="Seg_2295" s="T36">заяц.[NOM]</ta>
            <ta e="T38" id="Seg_2296" s="T37">ребенок-PL.[NOM]-3SG</ta>
            <ta e="T39" id="Seg_2297" s="T38">это</ta>
            <ta e="T40" id="Seg_2298" s="T39">затем</ta>
            <ta e="T41" id="Seg_2299" s="T40">потом</ta>
            <ta e="T42" id="Seg_2300" s="T41">ворон.[NOM]</ta>
            <ta e="T43" id="Seg_2301" s="T42">ребенок-PL.[NOM]-3SG</ta>
            <ta e="T44" id="Seg_2302" s="T43">прийти-%%</ta>
            <ta e="T46" id="Seg_2303" s="T45">потом</ta>
            <ta e="T47" id="Seg_2304" s="T46">заяц.[NOM]</ta>
            <ta e="T48" id="Seg_2305" s="T47">заяц-GEN</ta>
            <ta e="T49" id="Seg_2306" s="T48">ребенок-DIM.[NOM]</ta>
            <ta e="T50" id="Seg_2307" s="T49">и</ta>
            <ta e="T51" id="Seg_2308" s="T50">ворон-GEN</ta>
            <ta e="T52" id="Seg_2309" s="T51">ребенок-DIM.[NOM]</ta>
            <ta e="T53" id="Seg_2310" s="T52">%%-TR-HAB-PST-3PL</ta>
            <ta e="T55" id="Seg_2311" s="T54">ворон-GEN</ta>
            <ta e="T56" id="Seg_2312" s="T55">ребенок-DIM.[NOM]</ta>
            <ta e="T57" id="Seg_2313" s="T56">домой</ta>
            <ta e="T58" id="Seg_2314" s="T57">отправиться-CVB</ta>
            <ta e="T59" id="Seg_2315" s="T58">так</ta>
            <ta e="T60" id="Seg_2316" s="T59">сказать-DUR-3SG.O</ta>
            <ta e="T62" id="Seg_2317" s="T61">тот</ta>
            <ta e="T63" id="Seg_2318" s="T62">заяц.[NOM]</ta>
            <ta e="T64" id="Seg_2319" s="T63">%%</ta>
            <ta e="T65" id="Seg_2320" s="T64">тот</ta>
            <ta e="T66" id="Seg_2321" s="T65">жир-SNGL.[NOM]</ta>
            <ta e="T67" id="Seg_2322" s="T66">принести-PST-3SG.O</ta>
            <ta e="T69" id="Seg_2323" s="T68">заяц-DIM.[NOM]</ta>
            <ta e="T70" id="Seg_2324" s="T69">и</ta>
            <ta e="T71" id="Seg_2325" s="T70">ворон-DIM.[NOM]</ta>
            <ta e="T72" id="Seg_2326" s="T71">мать-DYA-DU.[NOM]</ta>
            <ta e="T73" id="Seg_2327" s="T72">жить.[3SG.S]</ta>
            <ta e="T74" id="Seg_2328" s="T73">жить-PST.NAR-3DU.S</ta>
            <ta e="T75" id="Seg_2329" s="T74">жить-PST.NAR-3DU.S</ta>
            <ta e="T77" id="Seg_2330" s="T76">заяц.[NOM]</ta>
            <ta e="T78" id="Seg_2331" s="T77">заяц.[NOM]</ta>
            <ta e="T79" id="Seg_2332" s="T78">тот</ta>
            <ta e="T80" id="Seg_2333" s="T79">лес-ILL</ta>
            <ta e="T81" id="Seg_2334" s="T80">уйти-PST.NAR.[3SG.S]</ta>
            <ta e="T83" id="Seg_2335" s="T82">один</ta>
            <ta e="T84" id="Seg_2336" s="T83">старик-SNGL.[NOM]</ta>
            <ta e="T85" id="Seg_2337" s="T84">сало.[NOM]</ta>
            <ta e="T86" id="Seg_2338" s="T85">нарты.[NOM]</ta>
            <ta e="T87" id="Seg_2339" s="T86">потащить-PST.NAR-INFER-3SG.O</ta>
            <ta e="T89" id="Seg_2340" s="T88">заяц.[NOM]</ta>
            <ta e="T90" id="Seg_2341" s="T89">мать-DYA-DU.[NOM]</ta>
            <ta e="T91" id="Seg_2342" s="T90">жить-PST.NAR-3DU.S</ta>
            <ta e="T93" id="Seg_2343" s="T92">один</ta>
            <ta e="T94" id="Seg_2344" s="T93">старик-SNGL.[NOM]</ta>
            <ta e="T95" id="Seg_2345" s="T94">уйти-DUR-CO.[3SG.S]</ta>
            <ta e="T96" id="Seg_2346" s="T95">заяц.[NOM]</ta>
            <ta e="T97" id="Seg_2347" s="T96">лес-ILL</ta>
            <ta e="T98" id="Seg_2348" s="T97">один</ta>
            <ta e="T99" id="Seg_2349" s="T98">старик-SNGL.[NOM]</ta>
            <ta e="T100" id="Seg_2350" s="T99">сало.[NOM]</ta>
            <ta e="T101" id="Seg_2351" s="T100">нарты.[NOM]</ta>
            <ta e="T102" id="Seg_2352" s="T101">потащить-PST.NAR-INFER-3SG.O</ta>
            <ta e="T104" id="Seg_2353" s="T103">заяц.[NOM]</ta>
            <ta e="T105" id="Seg_2354" s="T104">уйти-PST.NAR.[3SG.S]</ta>
            <ta e="T106" id="Seg_2355" s="T105">туда</ta>
            <ta e="T107" id="Seg_2356" s="T106">потом</ta>
            <ta e="T108" id="Seg_2357" s="T107">сало-EP-SNGL.[NOM]</ta>
            <ta e="T109" id="Seg_2358" s="T108">схватить-PST.NAR-INFER-3SG.O</ta>
            <ta e="T110" id="Seg_2359" s="T109">два-ITER.NUM</ta>
            <ta e="T111" id="Seg_2360" s="T110">домой</ta>
            <ta e="T112" id="Seg_2361" s="T111">уйти-PST.NAR.[3SG.S]</ta>
            <ta e="T114" id="Seg_2362" s="T113">потом</ta>
            <ta e="T115" id="Seg_2363" s="T114">домой</ta>
            <ta e="T116" id="Seg_2364" s="T115">уйти-CVB</ta>
            <ta e="T117" id="Seg_2365" s="T116">сын-PL.[NOM]-3SG</ta>
            <ta e="T118" id="Seg_2366" s="T117">еда-TR-PST.NAR-3SG.O</ta>
            <ta e="T120" id="Seg_2367" s="T119">сын-PL.[NOM]-3SG</ta>
            <ta e="T121" id="Seg_2368" s="T120">один</ta>
            <ta e="T122" id="Seg_2369" s="T121">ребенок-DIM.[NOM]</ta>
            <ta e="T123" id="Seg_2370" s="T122">заяц-GEN</ta>
            <ta e="T124" id="Seg_2371" s="T123">ворон-GEN</ta>
            <ta e="T125" id="Seg_2372" s="T124">ребенок-DIM.[NOM]</ta>
            <ta e="T126" id="Seg_2373" s="T125">%%-TR-HAB-CO-3SG.O</ta>
            <ta e="T128" id="Seg_2374" s="T127">ворон.[NOM]</ta>
            <ta e="T129" id="Seg_2375" s="T128">ворон-GEN</ta>
            <ta e="T130" id="Seg_2376" s="T129">ребенок-DIM.[NOM]</ta>
            <ta e="T131" id="Seg_2377" s="T130">домой</ta>
            <ta e="T132" id="Seg_2378" s="T131">уйти-CVB</ta>
            <ta e="T133" id="Seg_2379" s="T132">мать-ILL</ta>
            <ta e="T134" id="Seg_2380" s="T133">так</ta>
            <ta e="T135" id="Seg_2381" s="T134">сказать-PST.NAR-3PL</ta>
            <ta e="T137" id="Seg_2382" s="T136">заяц.[NOM]</ta>
            <ta e="T138" id="Seg_2383" s="T137">заяц.[NOM]</ta>
            <ta e="T139" id="Seg_2384" s="T138">сын-PL.[NOM]-3SG</ta>
            <ta e="T140" id="Seg_2385" s="T139">жир-EP-SNGL-ACC</ta>
            <ta e="T141" id="Seg_2386" s="T140">схватить-PST.NAR-3SG.O</ta>
            <ta e="T143" id="Seg_2387" s="T142">потом</ta>
            <ta e="T144" id="Seg_2388" s="T143">ворон.[NOM]</ta>
            <ta e="T145" id="Seg_2389" s="T144">прочь</ta>
            <ta e="T146" id="Seg_2390" s="T145">дом.[NOM]</ta>
            <ta e="T147" id="Seg_2391" s="T146">уйти-PST.NAR.[3SG.S]</ta>
            <ta e="T149" id="Seg_2392" s="T148">ворон.[NOM]</ta>
            <ta e="T150" id="Seg_2393" s="T149">прочь</ta>
            <ta e="T151" id="Seg_2394" s="T150">дом.[NOM]</ta>
            <ta e="T152" id="Seg_2395" s="T151">уйти-PST.NAR.[3SG.S]</ta>
            <ta e="T153" id="Seg_2396" s="T152">потом</ta>
            <ta e="T154" id="Seg_2397" s="T153">так</ta>
            <ta e="T155" id="Seg_2398" s="T154">сказать-PST.NAR-3SG.O</ta>
            <ta e="T157" id="Seg_2399" s="T156">ты.NOM</ta>
            <ta e="T158" id="Seg_2400" s="T157">где-EP-ADV.LOC</ta>
            <ta e="T159" id="Seg_2401" s="T158">сало-EP-SNGL.[NOM]</ta>
            <ta e="T160" id="Seg_2402" s="T159">найти-PST-2SG.O</ta>
            <ta e="T162" id="Seg_2403" s="T161">заяц.[NOM]</ta>
            <ta e="T163" id="Seg_2404" s="T162">так</ta>
            <ta e="T164" id="Seg_2405" s="T163">сказать-CO-3SG.O</ta>
            <ta e="T166" id="Seg_2406" s="T165">я.NOM</ta>
            <ta e="T167" id="Seg_2407" s="T166">лес-LOC-OBL.3SG</ta>
            <ta e="T168" id="Seg_2408" s="T167">старик-SNGL.[NOM]</ta>
            <ta e="T169" id="Seg_2409" s="T168">увидеть-DRV-PST-1SG.O</ta>
            <ta e="T170" id="Seg_2410" s="T169">потом</ta>
            <ta e="T173" id="Seg_2411" s="T172">жир.[NOM]</ta>
            <ta e="T174" id="Seg_2412" s="T173">нарты.[NOM]</ta>
            <ta e="T175" id="Seg_2413" s="T174">потащить-PST.NAR-INFER-3SG.O</ta>
            <ta e="T177" id="Seg_2414" s="T176">потом</ta>
            <ta e="T178" id="Seg_2415" s="T177">лес-ILL</ta>
            <ta e="T179" id="Seg_2416" s="T178">уйти-PST.NAR.[3SG.S]</ta>
            <ta e="T181" id="Seg_2417" s="T180">заяц.[NOM]</ta>
            <ta e="T182" id="Seg_2418" s="T181">лоб-EP.[NOM]</ta>
            <ta e="T183" id="Seg_2419" s="T182">верх.[NOM]</ta>
            <ta e="T184" id="Seg_2420" s="T183">снег-INSTR</ta>
            <ta e="T185" id="Seg_2421" s="T184">мазать-MULO-PST.NAR-3SG.O</ta>
            <ta e="T186" id="Seg_2422" s="T185">так</ta>
            <ta e="T187" id="Seg_2423" s="T186">сказать-CO-3SG.O</ta>
            <ta e="T189" id="Seg_2424" s="T188">настолько</ta>
            <ta e="T190" id="Seg_2425" s="T189">буря-TR-MOM-CVB</ta>
            <ta e="T191" id="Seg_2426" s="T190">потом</ta>
            <ta e="T192" id="Seg_2427" s="T191">NEG</ta>
            <ta e="T193" id="Seg_2428" s="T192">виднеться.[3SG.S]</ta>
            <ta e="T195" id="Seg_2429" s="T194">ворон.[NOM]</ta>
            <ta e="T196" id="Seg_2430" s="T195">нарты-ADV.LOC</ta>
            <ta e="T197" id="Seg_2431" s="T196">сесть-RFL.PFV-CVB</ta>
            <ta e="T198" id="Seg_2432" s="T197">кр_кр</ta>
            <ta e="T199" id="Seg_2433" s="T198">что-SNGL.[NOM]</ta>
            <ta e="T200" id="Seg_2434" s="T199">еда-SNGL.[NOM]</ta>
            <ta e="T201" id="Seg_2435" s="T200">схватить-PST.NAR-3SG.O</ta>
            <ta e="T203" id="Seg_2436" s="T202">потом</ta>
            <ta e="T204" id="Seg_2437" s="T203">старик-SNGL.[NOM]</ta>
            <ta e="T205" id="Seg_2438" s="T204">назад</ta>
            <ta e="T206" id="Seg_2439" s="T205">повернуться-CVB</ta>
            <ta e="T207" id="Seg_2440" s="T206">%%-INSTR</ta>
            <ta e="T208" id="Seg_2441" s="T207">ударить-PST.NAR-3SG.O</ta>
            <ta e="T209" id="Seg_2442" s="T208">вниз</ta>
            <ta e="T210" id="Seg_2443" s="T209">умереть-PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T214" id="Seg_2444" s="T213">заяц.[NOM]</ta>
            <ta e="T215" id="Seg_2445" s="T214">тот</ta>
            <ta e="T216" id="Seg_2446" s="T215">отправиться-PST.NAR-3SG.S</ta>
            <ta e="T217" id="Seg_2447" s="T216">туда</ta>
            <ta e="T219" id="Seg_2448" s="T218">сало-SNGL.[NOM]</ta>
            <ta e="T220" id="Seg_2449" s="T219">схватить-PST.NAR-INFER-3SG.O</ta>
            <ta e="T221" id="Seg_2450" s="T220">потом</ta>
            <ta e="T222" id="Seg_2451" s="T221">домой</ta>
            <ta e="T223" id="Seg_2452" s="T222">побежать.[3SG.S]</ta>
            <ta e="T225" id="Seg_2453" s="T224">ребенок-DIM-PL-ACC-3SG</ta>
            <ta e="T226" id="Seg_2454" s="T225">еда-TR-PST.NAR-3SG.O</ta>
            <ta e="T228" id="Seg_2455" s="T227">ворон.[NOM]</ta>
            <ta e="T229" id="Seg_2456" s="T228">ворон-GEN</ta>
            <ta e="T230" id="Seg_2457" s="T229">ребенок-DIM.[NOM]</ta>
            <ta e="T231" id="Seg_2458" s="T230">так</ta>
            <ta e="T232" id="Seg_2459" s="T231">сказать-CO-3SG.O</ta>
            <ta e="T233" id="Seg_2460" s="T232">мать.[NOM]</ta>
            <ta e="T234" id="Seg_2461" s="T233">что.[NOM]</ta>
            <ta e="T235" id="Seg_2462" s="T234">сделать-IPFV-3SG.O</ta>
            <ta e="T236" id="Seg_2463" s="T235">мать.[NOM]-2SG</ta>
            <ta e="T237" id="Seg_2464" s="T236">сало-EP-SNGL.[NOM]</ta>
            <ta e="T238" id="Seg_2465" s="T237">не.мочь-DUR-INFER-3SG.O</ta>
            <ta e="T240" id="Seg_2466" s="T239">потом</ta>
            <ta e="T241" id="Seg_2467" s="T240">заяц.[NOM]</ta>
            <ta e="T242" id="Seg_2468" s="T241">потом</ta>
            <ta e="T244" id="Seg_2469" s="T243">домой</ta>
            <ta e="T245" id="Seg_2470" s="T244">уйти-CVB</ta>
            <ta e="T246" id="Seg_2471" s="T245">радоваться-MOM-PST.NAR.[3SG.S]</ta>
            <ta e="T248" id="Seg_2472" s="T247">заяц-GEN</ta>
            <ta e="T249" id="Seg_2473" s="T248">съесть-EP-FRQ-CO-3PL</ta>
            <ta e="T250" id="Seg_2474" s="T249">этот</ta>
            <ta e="T251" id="Seg_2475" s="T250">потом</ta>
            <ta e="T252" id="Seg_2476" s="T251">так</ta>
            <ta e="T253" id="Seg_2477" s="T252">сказать-CO-3SG.O</ta>
            <ta e="T254" id="Seg_2478" s="T253">мать.[NOM]-2SG</ta>
            <ta e="T255" id="Seg_2479" s="T254">куда.деваться-PST.[3SG.S]</ta>
            <ta e="T257" id="Seg_2480" s="T256">мать.[NOM]-2SG</ta>
            <ta e="T258" id="Seg_2481" s="T257">куда.деваться-PST-3SG.O</ta>
            <ta e="T259" id="Seg_2482" s="T258">мать-ILL-OBL.2SG</ta>
            <ta e="T260" id="Seg_2483" s="T259">старик-SNGL.[NOM]</ta>
            <ta e="T262" id="Seg_2484" s="T261">ударить-PST-3SG.O</ta>
            <ta e="T264" id="Seg_2485" s="T263">всё</ta>
            <ta e="T265" id="Seg_2486" s="T264">плакать-CVB</ta>
            <ta e="T266" id="Seg_2487" s="T265">маленький</ta>
            <ta e="T267" id="Seg_2488" s="T266">дыра.[NOM]</ta>
            <ta e="T268" id="Seg_2489" s="T267">в-PROL</ta>
            <ta e="T269" id="Seg_2490" s="T268">вниз</ta>
            <ta e="T270" id="Seg_2491" s="T269">побежать-PST-3PL</ta>
            <ta e="T272" id="Seg_2492" s="T271">заяц-ADJZ</ta>
            <ta e="T273" id="Seg_2493" s="T272">мать-DYA-%%</ta>
            <ta e="T274" id="Seg_2494" s="T273">съесть-EP-FRQ-CO-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T6" id="Seg_2495" s="T5">n.[n:case]</ta>
            <ta e="T7" id="Seg_2496" s="T6">conj</ta>
            <ta e="T8" id="Seg_2497" s="T7">n.[n:case]</ta>
            <ta e="T9" id="Seg_2498" s="T8">n-n&gt;n-n:num.[n:case]</ta>
            <ta e="T10" id="Seg_2499" s="T9">v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_2500" s="T10">v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_2501" s="T12">n.[n:case]</ta>
            <ta e="T14" id="Seg_2502" s="T13">n.[n:case]</ta>
            <ta e="T15" id="Seg_2503" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_2504" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_2505" s="T16">v-v&gt;adv</ta>
            <ta e="T18" id="Seg_2506" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_2507" s="T18">v-v:tense.[v:pn]</ta>
            <ta e="T20" id="Seg_2508" s="T19">conj</ta>
            <ta e="T21" id="Seg_2509" s="T20">adv</ta>
            <ta e="T23" id="Seg_2510" s="T22">num</ta>
            <ta e="T24" id="Seg_2511" s="T23">n-n&gt;n.[n:case]</ta>
            <ta e="T26" id="Seg_2512" s="T25">n.[n:case]</ta>
            <ta e="T27" id="Seg_2513" s="T26">n.[n:case]</ta>
            <ta e="T28" id="Seg_2514" s="T27">v-v:tense-v:mood-v:pn</ta>
            <ta e="T30" id="Seg_2515" s="T29">adv</ta>
            <ta e="T31" id="Seg_2516" s="T30">v-v&gt;adv</ta>
            <ta e="T32" id="Seg_2517" s="T31">n-n:ins-n&gt;n.[n:case]</ta>
            <ta e="T33" id="Seg_2518" s="T32">v-v:tense-v:mood-v:pn</ta>
            <ta e="T34" id="Seg_2519" s="T33">adv</ta>
            <ta e="T35" id="Seg_2520" s="T34">v-v:tense.[v:pn]</ta>
            <ta e="T37" id="Seg_2521" s="T36">n.[n:case]</ta>
            <ta e="T38" id="Seg_2522" s="T37">n-n:num.[n:case]-n:poss</ta>
            <ta e="T39" id="Seg_2523" s="T38">dem</ta>
            <ta e="T40" id="Seg_2524" s="T39">adv</ta>
            <ta e="T41" id="Seg_2525" s="T40">adv</ta>
            <ta e="T42" id="Seg_2526" s="T41">n.[n:case]</ta>
            <ta e="T43" id="Seg_2527" s="T42">n-n:num.[n:case]-n:poss</ta>
            <ta e="T44" id="Seg_2528" s="T43">v-%%</ta>
            <ta e="T46" id="Seg_2529" s="T45">adv</ta>
            <ta e="T47" id="Seg_2530" s="T46">n.[n:case]</ta>
            <ta e="T48" id="Seg_2531" s="T47">n-n:case</ta>
            <ta e="T49" id="Seg_2532" s="T48">n-n&gt;n.[n:case]</ta>
            <ta e="T50" id="Seg_2533" s="T49">conj</ta>
            <ta e="T51" id="Seg_2534" s="T50">n-n:case</ta>
            <ta e="T52" id="Seg_2535" s="T51">n-n&gt;n.[n:case]</ta>
            <ta e="T53" id="Seg_2536" s="T52">ptcl-n&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T55" id="Seg_2537" s="T54">n-n:case</ta>
            <ta e="T56" id="Seg_2538" s="T55">n-n&gt;n.[n:case]</ta>
            <ta e="T57" id="Seg_2539" s="T56">adv</ta>
            <ta e="T58" id="Seg_2540" s="T57">v-v&gt;adv</ta>
            <ta e="T59" id="Seg_2541" s="T58">adv</ta>
            <ta e="T60" id="Seg_2542" s="T59">v-v&gt;v-v:pn</ta>
            <ta e="T62" id="Seg_2543" s="T61">dem</ta>
            <ta e="T63" id="Seg_2544" s="T62">n.[n:case]</ta>
            <ta e="T64" id="Seg_2545" s="T63">%%</ta>
            <ta e="T65" id="Seg_2546" s="T64">dem</ta>
            <ta e="T66" id="Seg_2547" s="T65">n-n&gt;n.[n:case]</ta>
            <ta e="T67" id="Seg_2548" s="T66">v-v:tense-v:pn</ta>
            <ta e="T69" id="Seg_2549" s="T68">n-n&gt;n.[n:case]</ta>
            <ta e="T70" id="Seg_2550" s="T69">conj</ta>
            <ta e="T71" id="Seg_2551" s="T70">n-n&gt;n.[n:case]</ta>
            <ta e="T72" id="Seg_2552" s="T71">n-n&gt;n-n:num.[n:case]</ta>
            <ta e="T73" id="Seg_2553" s="T72">v.[v:pn]</ta>
            <ta e="T74" id="Seg_2554" s="T73">v-v:tense-v:pn</ta>
            <ta e="T75" id="Seg_2555" s="T74">v-v:tense-v:pn</ta>
            <ta e="T77" id="Seg_2556" s="T76">n.[n:case]</ta>
            <ta e="T78" id="Seg_2557" s="T77">n.[n:case]</ta>
            <ta e="T79" id="Seg_2558" s="T78">dem</ta>
            <ta e="T80" id="Seg_2559" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_2560" s="T80">v-v:tense.[v:pn]</ta>
            <ta e="T83" id="Seg_2561" s="T82">num</ta>
            <ta e="T84" id="Seg_2562" s="T83">n-n&gt;n.[n:case]</ta>
            <ta e="T85" id="Seg_2563" s="T84">n.[n:case]</ta>
            <ta e="T86" id="Seg_2564" s="T85">n.[n:case]</ta>
            <ta e="T87" id="Seg_2565" s="T86">v-v:tense-v:mood-v:pn</ta>
            <ta e="T89" id="Seg_2566" s="T88">n.[n:case]</ta>
            <ta e="T90" id="Seg_2567" s="T89">n-n&gt;n-n:num.[n:case]</ta>
            <ta e="T91" id="Seg_2568" s="T90">v-v:tense-v:pn</ta>
            <ta e="T93" id="Seg_2569" s="T92">num</ta>
            <ta e="T94" id="Seg_2570" s="T93">n-n&gt;n.[n:case]</ta>
            <ta e="T95" id="Seg_2571" s="T94">v-v&gt;v-v:ins.[v:pn]</ta>
            <ta e="T96" id="Seg_2572" s="T95">n.[n:case]</ta>
            <ta e="T97" id="Seg_2573" s="T96">n-n:case</ta>
            <ta e="T98" id="Seg_2574" s="T97">num</ta>
            <ta e="T99" id="Seg_2575" s="T98">n-n&gt;n.[n:case]</ta>
            <ta e="T100" id="Seg_2576" s="T99">n.[n:case]</ta>
            <ta e="T101" id="Seg_2577" s="T100">n.[n:case]</ta>
            <ta e="T102" id="Seg_2578" s="T101">v-v:tense-v:mood-v:pn</ta>
            <ta e="T104" id="Seg_2579" s="T103">n.[n:case]</ta>
            <ta e="T105" id="Seg_2580" s="T104">v-v:tense.[v:pn]</ta>
            <ta e="T106" id="Seg_2581" s="T105">adv</ta>
            <ta e="T107" id="Seg_2582" s="T106">adv</ta>
            <ta e="T108" id="Seg_2583" s="T107">n-n:ins-n&gt;n.[n:case]</ta>
            <ta e="T109" id="Seg_2584" s="T108">v-v:tense-v:mood-v:pn</ta>
            <ta e="T110" id="Seg_2585" s="T109">num-num&gt;adv</ta>
            <ta e="T111" id="Seg_2586" s="T110">adv</ta>
            <ta e="T112" id="Seg_2587" s="T111">v-v:tense.[v:pn]</ta>
            <ta e="T114" id="Seg_2588" s="T113">adv</ta>
            <ta e="T115" id="Seg_2589" s="T114">adv</ta>
            <ta e="T116" id="Seg_2590" s="T115">v-v&gt;adv</ta>
            <ta e="T117" id="Seg_2591" s="T116">n-n:num.[n:case]-n:poss</ta>
            <ta e="T118" id="Seg_2592" s="T117">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T120" id="Seg_2593" s="T119">n-n:num.[n:case]-n:poss</ta>
            <ta e="T121" id="Seg_2594" s="T120">num</ta>
            <ta e="T122" id="Seg_2595" s="T121">n-n&gt;n.[n:case]</ta>
            <ta e="T123" id="Seg_2596" s="T122">n-n:case</ta>
            <ta e="T124" id="Seg_2597" s="T123">n-n:case</ta>
            <ta e="T125" id="Seg_2598" s="T124">n-n&gt;n.[n:case]</ta>
            <ta e="T126" id="Seg_2599" s="T125">ptcl-n&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T128" id="Seg_2600" s="T127">n.[n:case]</ta>
            <ta e="T129" id="Seg_2601" s="T128">n-n:case</ta>
            <ta e="T130" id="Seg_2602" s="T129">n-n&gt;n.[n:case]</ta>
            <ta e="T131" id="Seg_2603" s="T130">adv</ta>
            <ta e="T132" id="Seg_2604" s="T131">v-v&gt;adv</ta>
            <ta e="T133" id="Seg_2605" s="T132">n-n:case</ta>
            <ta e="T134" id="Seg_2606" s="T133">adv</ta>
            <ta e="T135" id="Seg_2607" s="T134">v-v:tense-v:pn</ta>
            <ta e="T137" id="Seg_2608" s="T136">n.[n:case]</ta>
            <ta e="T138" id="Seg_2609" s="T137">n.[n:case]</ta>
            <ta e="T139" id="Seg_2610" s="T138">n-n:num.[n:case]-n:poss</ta>
            <ta e="T140" id="Seg_2611" s="T139">n-n:ins-n&gt;n-n:case</ta>
            <ta e="T141" id="Seg_2612" s="T140">v-v:tense-v:pn</ta>
            <ta e="T143" id="Seg_2613" s="T142">adv</ta>
            <ta e="T144" id="Seg_2614" s="T143">n.[n:case]</ta>
            <ta e="T145" id="Seg_2615" s="T144">preverb</ta>
            <ta e="T146" id="Seg_2616" s="T145">n.[n:case]</ta>
            <ta e="T147" id="Seg_2617" s="T146">v-v:tense.[v:pn]</ta>
            <ta e="T149" id="Seg_2618" s="T148">n.[n:case]</ta>
            <ta e="T150" id="Seg_2619" s="T149">preverb</ta>
            <ta e="T151" id="Seg_2620" s="T150">n.[n:case]</ta>
            <ta e="T152" id="Seg_2621" s="T151">v-v:tense.[v:pn]</ta>
            <ta e="T153" id="Seg_2622" s="T152">adv</ta>
            <ta e="T154" id="Seg_2623" s="T153">adv</ta>
            <ta e="T155" id="Seg_2624" s="T154">v-v:tense-v:pn</ta>
            <ta e="T157" id="Seg_2625" s="T156">pers</ta>
            <ta e="T158" id="Seg_2626" s="T157">interrog-n:ins-adv:case</ta>
            <ta e="T159" id="Seg_2627" s="T158">n-n:ins-n&gt;n.[n:case]</ta>
            <ta e="T160" id="Seg_2628" s="T159">v-v:tense-v:pn</ta>
            <ta e="T162" id="Seg_2629" s="T161">n.[n:case]</ta>
            <ta e="T163" id="Seg_2630" s="T162">adv</ta>
            <ta e="T164" id="Seg_2631" s="T163">v-v:ins-v:pn</ta>
            <ta e="T166" id="Seg_2632" s="T165">pers</ta>
            <ta e="T167" id="Seg_2633" s="T166">n-n:case-n:obl.poss</ta>
            <ta e="T168" id="Seg_2634" s="T167">n-n&gt;n.[n:case]</ta>
            <ta e="T169" id="Seg_2635" s="T168">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T170" id="Seg_2636" s="T169">adv</ta>
            <ta e="T173" id="Seg_2637" s="T172">n.[n:case]</ta>
            <ta e="T174" id="Seg_2638" s="T173">n.[n:case]</ta>
            <ta e="T175" id="Seg_2639" s="T174">v-v:tense-v:mood-v:pn</ta>
            <ta e="T177" id="Seg_2640" s="T176">adv</ta>
            <ta e="T178" id="Seg_2641" s="T177">n-n:case</ta>
            <ta e="T179" id="Seg_2642" s="T178">v-v:tense.[v:pn]</ta>
            <ta e="T181" id="Seg_2643" s="T180">n.[n:case]</ta>
            <ta e="T182" id="Seg_2644" s="T181">n-n:ins.[n:case]</ta>
            <ta e="T183" id="Seg_2645" s="T182">n.[n:case]</ta>
            <ta e="T184" id="Seg_2646" s="T183">n-n:case</ta>
            <ta e="T185" id="Seg_2647" s="T184">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T186" id="Seg_2648" s="T185">adv</ta>
            <ta e="T187" id="Seg_2649" s="T186">v-v:ins-v:pn</ta>
            <ta e="T189" id="Seg_2650" s="T188">adv</ta>
            <ta e="T190" id="Seg_2651" s="T189">n-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T191" id="Seg_2652" s="T190">adv</ta>
            <ta e="T192" id="Seg_2653" s="T191">ptcl</ta>
            <ta e="T193" id="Seg_2654" s="T192">v.[v:pn]</ta>
            <ta e="T195" id="Seg_2655" s="T194">n.[n:case]</ta>
            <ta e="T196" id="Seg_2656" s="T195">n-n&gt;adv</ta>
            <ta e="T197" id="Seg_2657" s="T196">v-v&gt;v-v&gt;adv</ta>
            <ta e="T198" id="Seg_2658" s="T197">interj</ta>
            <ta e="T199" id="Seg_2659" s="T198">interrog-n&gt;n.[n:case]</ta>
            <ta e="T200" id="Seg_2660" s="T199">n-n&gt;n.[n:case]</ta>
            <ta e="T201" id="Seg_2661" s="T200">v-v:tense-v:pn</ta>
            <ta e="T203" id="Seg_2662" s="T202">adv</ta>
            <ta e="T204" id="Seg_2663" s="T203">n-n&gt;n.[n:case]</ta>
            <ta e="T205" id="Seg_2664" s="T204">adv</ta>
            <ta e="T206" id="Seg_2665" s="T205">v-v&gt;adv</ta>
            <ta e="T207" id="Seg_2666" s="T206">n-n:case</ta>
            <ta e="T208" id="Seg_2667" s="T207">v-v:tense-v:pn</ta>
            <ta e="T209" id="Seg_2668" s="T208">preverb</ta>
            <ta e="T210" id="Seg_2669" s="T209">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T214" id="Seg_2670" s="T213">n.[n:case]</ta>
            <ta e="T215" id="Seg_2671" s="T214">dem</ta>
            <ta e="T216" id="Seg_2672" s="T215">v-v:tense-v:pn</ta>
            <ta e="T217" id="Seg_2673" s="T216">adv</ta>
            <ta e="T219" id="Seg_2674" s="T218">n-n&gt;n.[n:case]</ta>
            <ta e="T220" id="Seg_2675" s="T219">v-v:tense-v:mood-v:pn</ta>
            <ta e="T221" id="Seg_2676" s="T220">adv</ta>
            <ta e="T222" id="Seg_2677" s="T221">adv</ta>
            <ta e="T223" id="Seg_2678" s="T222">v.[v:pn]</ta>
            <ta e="T225" id="Seg_2679" s="T224">n-n&gt;n-n:num-n:case-n:poss</ta>
            <ta e="T226" id="Seg_2680" s="T225">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T228" id="Seg_2681" s="T227">n.[n:case]</ta>
            <ta e="T229" id="Seg_2682" s="T228">n-n:case</ta>
            <ta e="T230" id="Seg_2683" s="T229">n-n&gt;n.[n:case]</ta>
            <ta e="T231" id="Seg_2684" s="T230">adv</ta>
            <ta e="T232" id="Seg_2685" s="T231">v-v:ins-v:pn</ta>
            <ta e="T233" id="Seg_2686" s="T232">n.[n:case]</ta>
            <ta e="T234" id="Seg_2687" s="T233">interrog.[n:case]</ta>
            <ta e="T235" id="Seg_2688" s="T234">v-v&gt;v-v:pn</ta>
            <ta e="T236" id="Seg_2689" s="T235">n.[n:case]-n:poss</ta>
            <ta e="T237" id="Seg_2690" s="T236">n-n:ins-n&gt;n.[n:case]</ta>
            <ta e="T238" id="Seg_2691" s="T237">v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T240" id="Seg_2692" s="T239">adv</ta>
            <ta e="T241" id="Seg_2693" s="T240">n.[n:case]</ta>
            <ta e="T242" id="Seg_2694" s="T241">adv</ta>
            <ta e="T244" id="Seg_2695" s="T243">adv</ta>
            <ta e="T245" id="Seg_2696" s="T244">v-v&gt;adv</ta>
            <ta e="T246" id="Seg_2697" s="T245">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T248" id="Seg_2698" s="T247">n-n:case</ta>
            <ta e="T249" id="Seg_2699" s="T248">v-v:ins-v&gt;v-v:ins-v:pn</ta>
            <ta e="T250" id="Seg_2700" s="T249">dem</ta>
            <ta e="T251" id="Seg_2701" s="T250">adv</ta>
            <ta e="T252" id="Seg_2702" s="T251">adv</ta>
            <ta e="T253" id="Seg_2703" s="T252">v-v:ins-v:pn</ta>
            <ta e="T254" id="Seg_2704" s="T253">n.[n:case]-n:poss</ta>
            <ta e="T255" id="Seg_2705" s="T254">qv-v:tense.[v:pn]</ta>
            <ta e="T257" id="Seg_2706" s="T256">n.[n:case]-n:poss</ta>
            <ta e="T258" id="Seg_2707" s="T257">qv-v:tense-v:pn</ta>
            <ta e="T259" id="Seg_2708" s="T258">n-n:case-n:obl.poss</ta>
            <ta e="T260" id="Seg_2709" s="T259">n-n&gt;n.[n:case]</ta>
            <ta e="T262" id="Seg_2710" s="T261">v-v:tense-v:pn</ta>
            <ta e="T264" id="Seg_2711" s="T263">quant</ta>
            <ta e="T265" id="Seg_2712" s="T264">v-v&gt;adv</ta>
            <ta e="T266" id="Seg_2713" s="T265">adj</ta>
            <ta e="T267" id="Seg_2714" s="T266">n.[n:case]</ta>
            <ta e="T268" id="Seg_2715" s="T267">pp-n:case</ta>
            <ta e="T269" id="Seg_2716" s="T268">preverb</ta>
            <ta e="T270" id="Seg_2717" s="T269">v-v:tense-v:pn</ta>
            <ta e="T272" id="Seg_2718" s="T271">n-n&gt;adj</ta>
            <ta e="T273" id="Seg_2719" s="T272">n-n&gt;n-n:case</ta>
            <ta e="T274" id="Seg_2720" s="T273">v-v:ins-v&gt;v-v:ins-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T6" id="Seg_2721" s="T5">n</ta>
            <ta e="T7" id="Seg_2722" s="T6">conj</ta>
            <ta e="T8" id="Seg_2723" s="T7">n</ta>
            <ta e="T9" id="Seg_2724" s="T8">n</ta>
            <ta e="T10" id="Seg_2725" s="T9">v</ta>
            <ta e="T11" id="Seg_2726" s="T10">v</ta>
            <ta e="T13" id="Seg_2727" s="T12">n</ta>
            <ta e="T14" id="Seg_2728" s="T13">n</ta>
            <ta e="T15" id="Seg_2729" s="T14">n</ta>
            <ta e="T16" id="Seg_2730" s="T15">n</ta>
            <ta e="T17" id="Seg_2731" s="T16">adv</ta>
            <ta e="T18" id="Seg_2732" s="T17">n</ta>
            <ta e="T19" id="Seg_2733" s="T18">v</ta>
            <ta e="T20" id="Seg_2734" s="T19">conj</ta>
            <ta e="T21" id="Seg_2735" s="T20">adv</ta>
            <ta e="T23" id="Seg_2736" s="T22">num</ta>
            <ta e="T24" id="Seg_2737" s="T23">n</ta>
            <ta e="T26" id="Seg_2738" s="T25">n</ta>
            <ta e="T27" id="Seg_2739" s="T26">n</ta>
            <ta e="T28" id="Seg_2740" s="T27">v</ta>
            <ta e="T30" id="Seg_2741" s="T29">adv</ta>
            <ta e="T31" id="Seg_2742" s="T30">adv</ta>
            <ta e="T32" id="Seg_2743" s="T31">n</ta>
            <ta e="T33" id="Seg_2744" s="T32">v</ta>
            <ta e="T34" id="Seg_2745" s="T33">adv</ta>
            <ta e="T35" id="Seg_2746" s="T34">v</ta>
            <ta e="T37" id="Seg_2747" s="T36">n</ta>
            <ta e="T38" id="Seg_2748" s="T37">n</ta>
            <ta e="T39" id="Seg_2749" s="T38">dem</ta>
            <ta e="T40" id="Seg_2750" s="T39">adv</ta>
            <ta e="T41" id="Seg_2751" s="T40">adv</ta>
            <ta e="T42" id="Seg_2752" s="T41">n</ta>
            <ta e="T43" id="Seg_2753" s="T42">n</ta>
            <ta e="T44" id="Seg_2754" s="T43">v</ta>
            <ta e="T46" id="Seg_2755" s="T45">adv</ta>
            <ta e="T47" id="Seg_2756" s="T46">n</ta>
            <ta e="T48" id="Seg_2757" s="T47">n</ta>
            <ta e="T49" id="Seg_2758" s="T48">n</ta>
            <ta e="T50" id="Seg_2759" s="T49">conj</ta>
            <ta e="T51" id="Seg_2760" s="T50">n</ta>
            <ta e="T52" id="Seg_2761" s="T51">n</ta>
            <ta e="T55" id="Seg_2762" s="T54">n</ta>
            <ta e="T56" id="Seg_2763" s="T55">n</ta>
            <ta e="T57" id="Seg_2764" s="T56">adv</ta>
            <ta e="T58" id="Seg_2765" s="T57">adv</ta>
            <ta e="T59" id="Seg_2766" s="T58">adv</ta>
            <ta e="T60" id="Seg_2767" s="T59">v</ta>
            <ta e="T62" id="Seg_2768" s="T61">dem</ta>
            <ta e="T63" id="Seg_2769" s="T62">n</ta>
            <ta e="T65" id="Seg_2770" s="T64">dem</ta>
            <ta e="T66" id="Seg_2771" s="T65">n</ta>
            <ta e="T67" id="Seg_2772" s="T66">v</ta>
            <ta e="T69" id="Seg_2773" s="T68">n</ta>
            <ta e="T70" id="Seg_2774" s="T69">conj</ta>
            <ta e="T71" id="Seg_2775" s="T70">n</ta>
            <ta e="T72" id="Seg_2776" s="T71">n</ta>
            <ta e="T73" id="Seg_2777" s="T72">v</ta>
            <ta e="T74" id="Seg_2778" s="T73">v</ta>
            <ta e="T75" id="Seg_2779" s="T74">v</ta>
            <ta e="T77" id="Seg_2780" s="T76">n</ta>
            <ta e="T78" id="Seg_2781" s="T77">n</ta>
            <ta e="T79" id="Seg_2782" s="T78">dem</ta>
            <ta e="T80" id="Seg_2783" s="T79">n</ta>
            <ta e="T81" id="Seg_2784" s="T80">v</ta>
            <ta e="T83" id="Seg_2785" s="T82">num</ta>
            <ta e="T84" id="Seg_2786" s="T83">n</ta>
            <ta e="T85" id="Seg_2787" s="T84">n</ta>
            <ta e="T86" id="Seg_2788" s="T85">n</ta>
            <ta e="T87" id="Seg_2789" s="T86">v</ta>
            <ta e="T89" id="Seg_2790" s="T88">n</ta>
            <ta e="T90" id="Seg_2791" s="T89">n</ta>
            <ta e="T91" id="Seg_2792" s="T90">v</ta>
            <ta e="T93" id="Seg_2793" s="T92">num</ta>
            <ta e="T94" id="Seg_2794" s="T93">n</ta>
            <ta e="T95" id="Seg_2795" s="T94">v</ta>
            <ta e="T96" id="Seg_2796" s="T95">n</ta>
            <ta e="T97" id="Seg_2797" s="T96">n</ta>
            <ta e="T98" id="Seg_2798" s="T97">num</ta>
            <ta e="T99" id="Seg_2799" s="T98">n</ta>
            <ta e="T100" id="Seg_2800" s="T99">n</ta>
            <ta e="T101" id="Seg_2801" s="T100">n</ta>
            <ta e="T102" id="Seg_2802" s="T101">v</ta>
            <ta e="T104" id="Seg_2803" s="T103">n</ta>
            <ta e="T105" id="Seg_2804" s="T104">v</ta>
            <ta e="T106" id="Seg_2805" s="T105">adv</ta>
            <ta e="T107" id="Seg_2806" s="T106">adv</ta>
            <ta e="T108" id="Seg_2807" s="T107">n</ta>
            <ta e="T109" id="Seg_2808" s="T108">v</ta>
            <ta e="T110" id="Seg_2809" s="T109">adv</ta>
            <ta e="T111" id="Seg_2810" s="T110">adv</ta>
            <ta e="T112" id="Seg_2811" s="T111">v</ta>
            <ta e="T114" id="Seg_2812" s="T113">adv</ta>
            <ta e="T115" id="Seg_2813" s="T114">adv</ta>
            <ta e="T116" id="Seg_2814" s="T115">adv</ta>
            <ta e="T117" id="Seg_2815" s="T116">n</ta>
            <ta e="T118" id="Seg_2816" s="T117">v</ta>
            <ta e="T120" id="Seg_2817" s="T119">n</ta>
            <ta e="T121" id="Seg_2818" s="T120">num</ta>
            <ta e="T122" id="Seg_2819" s="T121">n</ta>
            <ta e="T123" id="Seg_2820" s="T122">n</ta>
            <ta e="T124" id="Seg_2821" s="T123">n</ta>
            <ta e="T125" id="Seg_2822" s="T124">n</ta>
            <ta e="T128" id="Seg_2823" s="T127">n</ta>
            <ta e="T129" id="Seg_2824" s="T128">n</ta>
            <ta e="T130" id="Seg_2825" s="T129">n</ta>
            <ta e="T131" id="Seg_2826" s="T130">adv</ta>
            <ta e="T132" id="Seg_2827" s="T131">adv</ta>
            <ta e="T133" id="Seg_2828" s="T132">n</ta>
            <ta e="T134" id="Seg_2829" s="T133">adv</ta>
            <ta e="T135" id="Seg_2830" s="T134">v</ta>
            <ta e="T137" id="Seg_2831" s="T136">n</ta>
            <ta e="T138" id="Seg_2832" s="T137">n</ta>
            <ta e="T139" id="Seg_2833" s="T138">n</ta>
            <ta e="T140" id="Seg_2834" s="T139">n</ta>
            <ta e="T141" id="Seg_2835" s="T140">v</ta>
            <ta e="T143" id="Seg_2836" s="T142">adv</ta>
            <ta e="T144" id="Seg_2837" s="T143">n</ta>
            <ta e="T145" id="Seg_2838" s="T144">preverb</ta>
            <ta e="T146" id="Seg_2839" s="T145">n</ta>
            <ta e="T147" id="Seg_2840" s="T146">v</ta>
            <ta e="T149" id="Seg_2841" s="T148">n</ta>
            <ta e="T150" id="Seg_2842" s="T149">preverb</ta>
            <ta e="T151" id="Seg_2843" s="T150">n</ta>
            <ta e="T152" id="Seg_2844" s="T151">v</ta>
            <ta e="T153" id="Seg_2845" s="T152">adv</ta>
            <ta e="T154" id="Seg_2846" s="T153">adv</ta>
            <ta e="T155" id="Seg_2847" s="T154">v</ta>
            <ta e="T157" id="Seg_2848" s="T156">pers</ta>
            <ta e="T158" id="Seg_2849" s="T157">interrog</ta>
            <ta e="T159" id="Seg_2850" s="T158">n</ta>
            <ta e="T160" id="Seg_2851" s="T159">v</ta>
            <ta e="T162" id="Seg_2852" s="T161">n</ta>
            <ta e="T163" id="Seg_2853" s="T162">adv</ta>
            <ta e="T164" id="Seg_2854" s="T163">v</ta>
            <ta e="T166" id="Seg_2855" s="T165">pers</ta>
            <ta e="T167" id="Seg_2856" s="T166">n</ta>
            <ta e="T168" id="Seg_2857" s="T167">n</ta>
            <ta e="T169" id="Seg_2858" s="T168">v</ta>
            <ta e="T170" id="Seg_2859" s="T169">adv</ta>
            <ta e="T173" id="Seg_2860" s="T172">n</ta>
            <ta e="T174" id="Seg_2861" s="T173">n</ta>
            <ta e="T175" id="Seg_2862" s="T174">v</ta>
            <ta e="T177" id="Seg_2863" s="T176">adv</ta>
            <ta e="T178" id="Seg_2864" s="T177">n</ta>
            <ta e="T179" id="Seg_2865" s="T178">v</ta>
            <ta e="T181" id="Seg_2866" s="T180">n</ta>
            <ta e="T182" id="Seg_2867" s="T181">n</ta>
            <ta e="T183" id="Seg_2868" s="T182">n</ta>
            <ta e="T184" id="Seg_2869" s="T183">n</ta>
            <ta e="T185" id="Seg_2870" s="T184">v</ta>
            <ta e="T186" id="Seg_2871" s="T185">adv</ta>
            <ta e="T187" id="Seg_2872" s="T186">v</ta>
            <ta e="T189" id="Seg_2873" s="T188">adv</ta>
            <ta e="T190" id="Seg_2874" s="T189">adv</ta>
            <ta e="T191" id="Seg_2875" s="T190">adv</ta>
            <ta e="T192" id="Seg_2876" s="T191">ptcl</ta>
            <ta e="T193" id="Seg_2877" s="T192">v</ta>
            <ta e="T195" id="Seg_2878" s="T194">n</ta>
            <ta e="T196" id="Seg_2879" s="T195">adv</ta>
            <ta e="T197" id="Seg_2880" s="T196">adv</ta>
            <ta e="T198" id="Seg_2881" s="T197">interj</ta>
            <ta e="T199" id="Seg_2882" s="T198">n</ta>
            <ta e="T200" id="Seg_2883" s="T199">n</ta>
            <ta e="T201" id="Seg_2884" s="T200">v</ta>
            <ta e="T203" id="Seg_2885" s="T202">adv</ta>
            <ta e="T204" id="Seg_2886" s="T203">n</ta>
            <ta e="T205" id="Seg_2887" s="T204">adv</ta>
            <ta e="T206" id="Seg_2888" s="T205">adv</ta>
            <ta e="T207" id="Seg_2889" s="T206">n</ta>
            <ta e="T208" id="Seg_2890" s="T207">v</ta>
            <ta e="T209" id="Seg_2891" s="T208">preverb</ta>
            <ta e="T210" id="Seg_2892" s="T209">v</ta>
            <ta e="T214" id="Seg_2893" s="T213">n</ta>
            <ta e="T215" id="Seg_2894" s="T214">dem</ta>
            <ta e="T216" id="Seg_2895" s="T215">v</ta>
            <ta e="T217" id="Seg_2896" s="T216">adv</ta>
            <ta e="T219" id="Seg_2897" s="T218">n</ta>
            <ta e="T220" id="Seg_2898" s="T219">v</ta>
            <ta e="T221" id="Seg_2899" s="T220">adv</ta>
            <ta e="T222" id="Seg_2900" s="T221">adv</ta>
            <ta e="T223" id="Seg_2901" s="T222">v</ta>
            <ta e="T225" id="Seg_2902" s="T224">n</ta>
            <ta e="T226" id="Seg_2903" s="T225">v</ta>
            <ta e="T228" id="Seg_2904" s="T227">n</ta>
            <ta e="T229" id="Seg_2905" s="T228">n</ta>
            <ta e="T230" id="Seg_2906" s="T229">n</ta>
            <ta e="T231" id="Seg_2907" s="T230">adv</ta>
            <ta e="T232" id="Seg_2908" s="T231">v</ta>
            <ta e="T233" id="Seg_2909" s="T232">n</ta>
            <ta e="T234" id="Seg_2910" s="T233">interrog</ta>
            <ta e="T235" id="Seg_2911" s="T234">v</ta>
            <ta e="T236" id="Seg_2912" s="T235">v</ta>
            <ta e="T237" id="Seg_2913" s="T236">n</ta>
            <ta e="T238" id="Seg_2914" s="T237">v</ta>
            <ta e="T240" id="Seg_2915" s="T239">adv</ta>
            <ta e="T241" id="Seg_2916" s="T240">n</ta>
            <ta e="T242" id="Seg_2917" s="T241">adv</ta>
            <ta e="T244" id="Seg_2918" s="T243">adv</ta>
            <ta e="T245" id="Seg_2919" s="T244">adv</ta>
            <ta e="T246" id="Seg_2920" s="T245">v</ta>
            <ta e="T248" id="Seg_2921" s="T247">n</ta>
            <ta e="T249" id="Seg_2922" s="T248">v</ta>
            <ta e="T250" id="Seg_2923" s="T249">dem</ta>
            <ta e="T251" id="Seg_2924" s="T250">adv</ta>
            <ta e="T252" id="Seg_2925" s="T251">adv</ta>
            <ta e="T253" id="Seg_2926" s="T252">v</ta>
            <ta e="T254" id="Seg_2927" s="T253">n</ta>
            <ta e="T255" id="Seg_2928" s="T254">qv</ta>
            <ta e="T257" id="Seg_2929" s="T256">n</ta>
            <ta e="T258" id="Seg_2930" s="T257">qv</ta>
            <ta e="T259" id="Seg_2931" s="T258">n</ta>
            <ta e="T260" id="Seg_2932" s="T259">n</ta>
            <ta e="T262" id="Seg_2933" s="T261">v</ta>
            <ta e="T264" id="Seg_2934" s="T263">quant</ta>
            <ta e="T265" id="Seg_2935" s="T264">adv</ta>
            <ta e="T266" id="Seg_2936" s="T265">adj</ta>
            <ta e="T267" id="Seg_2937" s="T266">n</ta>
            <ta e="T268" id="Seg_2938" s="T267">pp</ta>
            <ta e="T269" id="Seg_2939" s="T268">preverb</ta>
            <ta e="T270" id="Seg_2940" s="T269">v</ta>
            <ta e="T272" id="Seg_2941" s="T271">n</ta>
            <ta e="T273" id="Seg_2942" s="T272">n</ta>
            <ta e="T274" id="Seg_2943" s="T273">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T6" id="Seg_2944" s="T5">np.h:Th</ta>
            <ta e="T8" id="Seg_2945" s="T7">np.h:Th</ta>
            <ta e="T9" id="Seg_2946" s="T8">np:Com</ta>
            <ta e="T11" id="Seg_2947" s="T10">0.3.h:Th</ta>
            <ta e="T14" id="Seg_2948" s="T13">np.h:A</ta>
            <ta e="T16" id="Seg_2949" s="T15">np:G</ta>
            <ta e="T18" id="Seg_2950" s="T17">np:G</ta>
            <ta e="T21" id="Seg_2951" s="T20">adv:Time</ta>
            <ta e="T24" id="Seg_2952" s="T23">np.h:A</ta>
            <ta e="T27" id="Seg_2953" s="T26">np:Th</ta>
            <ta e="T30" id="Seg_2954" s="T29">adv:Time</ta>
            <ta e="T32" id="Seg_2955" s="T31">np:Th</ta>
            <ta e="T33" id="Seg_2956" s="T32">0.3.h:A</ta>
            <ta e="T34" id="Seg_2957" s="T33">adv:G</ta>
            <ta e="T35" id="Seg_2958" s="T34">0.3.h:A</ta>
            <ta e="T37" id="Seg_2959" s="T36">np.h:Poss</ta>
            <ta e="T40" id="Seg_2960" s="T39">adv:Time</ta>
            <ta e="T42" id="Seg_2961" s="T41">np.h:Poss</ta>
            <ta e="T43" id="Seg_2962" s="T42">np.h:A</ta>
            <ta e="T46" id="Seg_2963" s="T45">adv:Time</ta>
            <ta e="T48" id="Seg_2964" s="T47">np.h:Poss</ta>
            <ta e="T49" id="Seg_2965" s="T48">np.h:A</ta>
            <ta e="T51" id="Seg_2966" s="T50">np.h:Poss</ta>
            <ta e="T52" id="Seg_2967" s="T51">np.h:A</ta>
            <ta e="T55" id="Seg_2968" s="T54">np.h:Poss</ta>
            <ta e="T56" id="Seg_2969" s="T55">np.h:A</ta>
            <ta e="T57" id="Seg_2970" s="T56">adv:G</ta>
            <ta e="T63" id="Seg_2971" s="T62">np.h:A</ta>
            <ta e="T66" id="Seg_2972" s="T65">np:Th</ta>
            <ta e="T69" id="Seg_2973" s="T68">np.h:Th</ta>
            <ta e="T71" id="Seg_2974" s="T70">np.h:Th</ta>
            <ta e="T72" id="Seg_2975" s="T71">np:Com</ta>
            <ta e="T75" id="Seg_2976" s="T74">0.3.h:Th</ta>
            <ta e="T78" id="Seg_2977" s="T77">np.h:A</ta>
            <ta e="T80" id="Seg_2978" s="T79">np:G</ta>
            <ta e="T84" id="Seg_2979" s="T83">np.h:A</ta>
            <ta e="T86" id="Seg_2980" s="T85">np:Th</ta>
            <ta e="T89" id="Seg_2981" s="T88">np.h:Th</ta>
            <ta e="T90" id="Seg_2982" s="T89">np:Com</ta>
            <ta e="T96" id="Seg_2983" s="T95">np.h:A</ta>
            <ta e="T97" id="Seg_2984" s="T96">np:G</ta>
            <ta e="T99" id="Seg_2985" s="T98">np.h:A</ta>
            <ta e="T101" id="Seg_2986" s="T100">np:Th</ta>
            <ta e="T104" id="Seg_2987" s="T103">np.h:A</ta>
            <ta e="T106" id="Seg_2988" s="T105">adv:G</ta>
            <ta e="T107" id="Seg_2989" s="T106">adv:Time</ta>
            <ta e="T108" id="Seg_2990" s="T107">np:Th</ta>
            <ta e="T109" id="Seg_2991" s="T108">0.3.h:A</ta>
            <ta e="T111" id="Seg_2992" s="T110">adv:G</ta>
            <ta e="T112" id="Seg_2993" s="T111">0.3.h:A</ta>
            <ta e="T114" id="Seg_2994" s="T113">adv:Time</ta>
            <ta e="T115" id="Seg_2995" s="T114">adv:G</ta>
            <ta e="T117" id="Seg_2996" s="T116">np.h:B 0.3.h:Poss</ta>
            <ta e="T118" id="Seg_2997" s="T117">0.3.h:A</ta>
            <ta e="T122" id="Seg_2998" s="T121">np.h:A</ta>
            <ta e="T123" id="Seg_2999" s="T122">np.h:Poss</ta>
            <ta e="T124" id="Seg_3000" s="T123">np.h:Poss</ta>
            <ta e="T125" id="Seg_3001" s="T124">np.h:Th</ta>
            <ta e="T129" id="Seg_3002" s="T128">np.h:Poss</ta>
            <ta e="T130" id="Seg_3003" s="T129">np.h:A</ta>
            <ta e="T131" id="Seg_3004" s="T130">adv:G</ta>
            <ta e="T133" id="Seg_3005" s="T132">np.h:R</ta>
            <ta e="T138" id="Seg_3006" s="T137">np.h:Poss</ta>
            <ta e="T139" id="Seg_3007" s="T138">np.h:A</ta>
            <ta e="T140" id="Seg_3008" s="T139">np:Th</ta>
            <ta e="T143" id="Seg_3009" s="T142">adv:Time</ta>
            <ta e="T144" id="Seg_3010" s="T143">np.h:A</ta>
            <ta e="T146" id="Seg_3011" s="T145">np:G</ta>
            <ta e="T149" id="Seg_3012" s="T148">np.h:A</ta>
            <ta e="T151" id="Seg_3013" s="T150">np:G</ta>
            <ta e="T153" id="Seg_3014" s="T152">adv:Time</ta>
            <ta e="T155" id="Seg_3015" s="T154">0.3.h:A</ta>
            <ta e="T157" id="Seg_3016" s="T156">pro.h:B</ta>
            <ta e="T158" id="Seg_3017" s="T157">pro:L</ta>
            <ta e="T159" id="Seg_3018" s="T158">np:Th</ta>
            <ta e="T162" id="Seg_3019" s="T161">np.h:A</ta>
            <ta e="T166" id="Seg_3020" s="T165">pro.h:E</ta>
            <ta e="T167" id="Seg_3021" s="T166">np:L</ta>
            <ta e="T168" id="Seg_3022" s="T167">np.h:Th</ta>
            <ta e="T170" id="Seg_3023" s="T169">adv:Time</ta>
            <ta e="T174" id="Seg_3024" s="T173">np:Th</ta>
            <ta e="T175" id="Seg_3025" s="T174">0.3.h:A</ta>
            <ta e="T177" id="Seg_3026" s="T176">adv:Time</ta>
            <ta e="T178" id="Seg_3027" s="T177">np:G</ta>
            <ta e="T179" id="Seg_3028" s="T178">0.3.h:A</ta>
            <ta e="T181" id="Seg_3029" s="T180">np.h:A</ta>
            <ta e="T183" id="Seg_3030" s="T182">np:Th</ta>
            <ta e="T184" id="Seg_3031" s="T183">np:Ins</ta>
            <ta e="T187" id="Seg_3032" s="T186">0.3.h:A</ta>
            <ta e="T191" id="Seg_3033" s="T190">adv:Time</ta>
            <ta e="T193" id="Seg_3034" s="T192">0.3:Th</ta>
            <ta e="T195" id="Seg_3035" s="T194">np.h:A</ta>
            <ta e="T196" id="Seg_3036" s="T195">np:L</ta>
            <ta e="T199" id="Seg_3037" s="T198">pro:Th</ta>
            <ta e="T200" id="Seg_3038" s="T199">np:Th</ta>
            <ta e="T203" id="Seg_3039" s="T202">adv:Time</ta>
            <ta e="T204" id="Seg_3040" s="T203">np.h:A</ta>
            <ta e="T205" id="Seg_3041" s="T204">adv:G</ta>
            <ta e="T207" id="Seg_3042" s="T206">np:Ins</ta>
            <ta e="T208" id="Seg_3043" s="T207">0.3:P</ta>
            <ta e="T210" id="Seg_3044" s="T209">0.3.h:P</ta>
            <ta e="T214" id="Seg_3045" s="T213">np.h:A</ta>
            <ta e="T217" id="Seg_3046" s="T216">adv:G</ta>
            <ta e="T219" id="Seg_3047" s="T218">np:Th</ta>
            <ta e="T220" id="Seg_3048" s="T219">0.3.h:A</ta>
            <ta e="T221" id="Seg_3049" s="T220">adv:Time</ta>
            <ta e="T222" id="Seg_3050" s="T221">adv:G</ta>
            <ta e="T223" id="Seg_3051" s="T222">0.3.h:A</ta>
            <ta e="T225" id="Seg_3052" s="T224">np.h:B 0.3.h:Poss</ta>
            <ta e="T226" id="Seg_3053" s="T225">0.3.h:A</ta>
            <ta e="T229" id="Seg_3054" s="T228">np.h:Poss</ta>
            <ta e="T230" id="Seg_3055" s="T229">np.h:A</ta>
            <ta e="T233" id="Seg_3056" s="T232">np.h:A</ta>
            <ta e="T234" id="Seg_3057" s="T233">pro:P</ta>
            <ta e="T236" id="Seg_3058" s="T235">np.h:A 0.2.h:Poss</ta>
            <ta e="T237" id="Seg_3059" s="T236">np:Th</ta>
            <ta e="T240" id="Seg_3060" s="T239">adv:Time</ta>
            <ta e="T241" id="Seg_3061" s="T240">np.h:E</ta>
            <ta e="T244" id="Seg_3062" s="T243">adv:G</ta>
            <ta e="T248" id="Seg_3063" s="T247">np.h:Poss</ta>
            <ta e="T249" id="Seg_3064" s="T248">0.3.h:A</ta>
            <ta e="T251" id="Seg_3065" s="T250">adv:Time</ta>
            <ta e="T253" id="Seg_3066" s="T252">0.3.h:A</ta>
            <ta e="T254" id="Seg_3067" s="T253">np.h:A 0.2.h:Poss</ta>
            <ta e="T257" id="Seg_3068" s="T256">np.h:A 0.2.h:Poss</ta>
            <ta e="T259" id="Seg_3069" s="T258">np.h:P 0.2.h:Poss</ta>
            <ta e="T260" id="Seg_3070" s="T259">np.h:A</ta>
            <ta e="T264" id="Seg_3071" s="T263">pro.h:A</ta>
            <ta e="T267" id="Seg_3072" s="T266">pp:G</ta>
            <ta e="T272" id="Seg_3073" s="T271">np.h:A</ta>
            <ta e="T273" id="Seg_3074" s="T272">np:Com</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T6" id="Seg_3075" s="T5">np.h:S</ta>
            <ta e="T8" id="Seg_3076" s="T7">np.h:S</ta>
            <ta e="T10" id="Seg_3077" s="T9">v:pred</ta>
            <ta e="T11" id="Seg_3078" s="T10">0.3.h:S v:pred</ta>
            <ta e="T14" id="Seg_3079" s="T13">np.h:S</ta>
            <ta e="T17" id="Seg_3080" s="T15">s:temp</ta>
            <ta e="T19" id="Seg_3081" s="T18">v:pred</ta>
            <ta e="T24" id="Seg_3082" s="T23">np.h:S</ta>
            <ta e="T27" id="Seg_3083" s="T26">np:O</ta>
            <ta e="T28" id="Seg_3084" s="T27">v:pred</ta>
            <ta e="T31" id="Seg_3085" s="T30">s:temp</ta>
            <ta e="T32" id="Seg_3086" s="T31">np:O</ta>
            <ta e="T33" id="Seg_3087" s="T32">0.3.h:S v:pred</ta>
            <ta e="T35" id="Seg_3088" s="T34">0.3.h:S v:pred</ta>
            <ta e="T43" id="Seg_3089" s="T42">np.h:S</ta>
            <ta e="T44" id="Seg_3090" s="T43">v:pred</ta>
            <ta e="T49" id="Seg_3091" s="T48">np.h:S</ta>
            <ta e="T52" id="Seg_3092" s="T51">np.h:S</ta>
            <ta e="T53" id="Seg_3093" s="T52">v:pred</ta>
            <ta e="T56" id="Seg_3094" s="T55">np.h:S</ta>
            <ta e="T58" id="Seg_3095" s="T56">s:temp</ta>
            <ta e="T60" id="Seg_3096" s="T59">v:pred</ta>
            <ta e="T63" id="Seg_3097" s="T62">np.h:S</ta>
            <ta e="T66" id="Seg_3098" s="T65">np:O</ta>
            <ta e="T67" id="Seg_3099" s="T66">v:pred</ta>
            <ta e="T69" id="Seg_3100" s="T68">np.h:S</ta>
            <ta e="T71" id="Seg_3101" s="T70">np.h:S</ta>
            <ta e="T74" id="Seg_3102" s="T73">v:pred</ta>
            <ta e="T75" id="Seg_3103" s="T74">0.3.h:S v:pred</ta>
            <ta e="T78" id="Seg_3104" s="T77">np.h:S</ta>
            <ta e="T81" id="Seg_3105" s="T80">v:pred</ta>
            <ta e="T84" id="Seg_3106" s="T83">np.h:S</ta>
            <ta e="T86" id="Seg_3107" s="T85">np:O</ta>
            <ta e="T87" id="Seg_3108" s="T86">v:pred</ta>
            <ta e="T89" id="Seg_3109" s="T88">np.h:S</ta>
            <ta e="T91" id="Seg_3110" s="T90">v:pred</ta>
            <ta e="T95" id="Seg_3111" s="T94">v:pred</ta>
            <ta e="T96" id="Seg_3112" s="T95">np.h:S</ta>
            <ta e="T99" id="Seg_3113" s="T98">np.h:S</ta>
            <ta e="T101" id="Seg_3114" s="T100">np:O</ta>
            <ta e="T102" id="Seg_3115" s="T101">v:pred</ta>
            <ta e="T104" id="Seg_3116" s="T103">np.h:S</ta>
            <ta e="T105" id="Seg_3117" s="T104">v:pred</ta>
            <ta e="T108" id="Seg_3118" s="T107">np:O</ta>
            <ta e="T109" id="Seg_3119" s="T108">0.3.h:S v:pred</ta>
            <ta e="T112" id="Seg_3120" s="T111">0.3.h:S v:pred</ta>
            <ta e="T116" id="Seg_3121" s="T114">s:temp</ta>
            <ta e="T117" id="Seg_3122" s="T116">np.h:O</ta>
            <ta e="T118" id="Seg_3123" s="T117">0.3.h:S v:pred</ta>
            <ta e="T122" id="Seg_3124" s="T121">np.h:S</ta>
            <ta e="T125" id="Seg_3125" s="T124">np.h:O</ta>
            <ta e="T126" id="Seg_3126" s="T125">v:pred</ta>
            <ta e="T130" id="Seg_3127" s="T129">np.h:S</ta>
            <ta e="T132" id="Seg_3128" s="T130">s:temp</ta>
            <ta e="T135" id="Seg_3129" s="T134">v:pred</ta>
            <ta e="T139" id="Seg_3130" s="T138">np.h:S</ta>
            <ta e="T140" id="Seg_3131" s="T139">np:O</ta>
            <ta e="T141" id="Seg_3132" s="T140">v:pred</ta>
            <ta e="T144" id="Seg_3133" s="T143">np.h:S</ta>
            <ta e="T147" id="Seg_3134" s="T146">v:pred</ta>
            <ta e="T149" id="Seg_3135" s="T148">np.h:S</ta>
            <ta e="T152" id="Seg_3136" s="T151">v:pred</ta>
            <ta e="T155" id="Seg_3137" s="T154">0.3.h:S v:pred</ta>
            <ta e="T157" id="Seg_3138" s="T156">pro.h:S</ta>
            <ta e="T159" id="Seg_3139" s="T158">np:O</ta>
            <ta e="T160" id="Seg_3140" s="T159">v:pred</ta>
            <ta e="T162" id="Seg_3141" s="T161">np.h:S</ta>
            <ta e="T164" id="Seg_3142" s="T163">v:pred</ta>
            <ta e="T166" id="Seg_3143" s="T165">pro.h:S</ta>
            <ta e="T168" id="Seg_3144" s="T167">np.h:O</ta>
            <ta e="T169" id="Seg_3145" s="T168">v:pred</ta>
            <ta e="T174" id="Seg_3146" s="T173">np:O</ta>
            <ta e="T175" id="Seg_3147" s="T174">0.3.h:S v:pred</ta>
            <ta e="T179" id="Seg_3148" s="T178">0.3.h:S v:pred</ta>
            <ta e="T181" id="Seg_3149" s="T180">np.h:S</ta>
            <ta e="T183" id="Seg_3150" s="T182">np:O</ta>
            <ta e="T185" id="Seg_3151" s="T184">v:pred</ta>
            <ta e="T187" id="Seg_3152" s="T186">0.3.h:S v:pred</ta>
            <ta e="T190" id="Seg_3153" s="T188">s:temp</ta>
            <ta e="T193" id="Seg_3154" s="T192">0.3:S v:pred</ta>
            <ta e="T195" id="Seg_3155" s="T194">np.h:S</ta>
            <ta e="T197" id="Seg_3156" s="T195">s:temp</ta>
            <ta e="T199" id="Seg_3157" s="T198">pro:O</ta>
            <ta e="T200" id="Seg_3158" s="T199">np:O</ta>
            <ta e="T201" id="Seg_3159" s="T200">v:pred</ta>
            <ta e="T204" id="Seg_3160" s="T203">np.h:S</ta>
            <ta e="T206" id="Seg_3161" s="T204">s:temp</ta>
            <ta e="T208" id="Seg_3162" s="T207">v:pred 0.3:O</ta>
            <ta e="T210" id="Seg_3163" s="T209">0.3.h:S v:pred</ta>
            <ta e="T214" id="Seg_3164" s="T213">np.h:S</ta>
            <ta e="T216" id="Seg_3165" s="T215">v:pred</ta>
            <ta e="T219" id="Seg_3166" s="T218">np:O</ta>
            <ta e="T220" id="Seg_3167" s="T219">0.3.h:S v:pred</ta>
            <ta e="T223" id="Seg_3168" s="T222">0.3.h:S v:pred</ta>
            <ta e="T225" id="Seg_3169" s="T224">np.h:O</ta>
            <ta e="T226" id="Seg_3170" s="T225">0.3.h:S v:pred</ta>
            <ta e="T230" id="Seg_3171" s="T229">np.h:S</ta>
            <ta e="T232" id="Seg_3172" s="T231">v:pred</ta>
            <ta e="T233" id="Seg_3173" s="T232">np.h:S</ta>
            <ta e="T234" id="Seg_3174" s="T233">pro:O</ta>
            <ta e="T235" id="Seg_3175" s="T234">v:pred</ta>
            <ta e="T236" id="Seg_3176" s="T235">np.h:S</ta>
            <ta e="T237" id="Seg_3177" s="T236">np:O</ta>
            <ta e="T238" id="Seg_3178" s="T237">v:pred</ta>
            <ta e="T241" id="Seg_3179" s="T240">np.h:S</ta>
            <ta e="T245" id="Seg_3180" s="T243">s:temp</ta>
            <ta e="T246" id="Seg_3181" s="T245">v:pred</ta>
            <ta e="T249" id="Seg_3182" s="T248">0.3.h:S v:pred</ta>
            <ta e="T253" id="Seg_3183" s="T252">0.3.h:S v:pred</ta>
            <ta e="T254" id="Seg_3184" s="T253">np.h:S</ta>
            <ta e="T255" id="Seg_3185" s="T254">v:pred</ta>
            <ta e="T257" id="Seg_3186" s="T256">np.h:S</ta>
            <ta e="T258" id="Seg_3187" s="T257">v:pred</ta>
            <ta e="T260" id="Seg_3188" s="T259">np.h:S</ta>
            <ta e="T262" id="Seg_3189" s="T261">v:pred</ta>
            <ta e="T264" id="Seg_3190" s="T263">pro.h:S</ta>
            <ta e="T265" id="Seg_3191" s="T264">s:adv</ta>
            <ta e="T270" id="Seg_3192" s="T269">v:pred</ta>
            <ta e="T272" id="Seg_3193" s="T271">np.h:S</ta>
            <ta e="T274" id="Seg_3194" s="T273">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T7" id="Seg_3195" s="T6">RUS:gram</ta>
            <ta e="T20" id="Seg_3196" s="T19">RUS:gram</ta>
            <ta e="T50" id="Seg_3197" s="T49">RUS:gram</ta>
            <ta e="T75" id="Seg_3198" s="T68">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_3199" s="T1">[KuAI:] Rita Kunina, a fairy tale; speak.</ta>
            <ta e="T11" id="Seg_3200" s="T277">[KR:] A hare and a raven lived with their parents, they lived.</ta>
            <ta e="T21" id="Seg_3201" s="T12">The hare, having gone to the forest, he went to the forest and then…</ta>
            <ta e="T28" id="Seg_3202" s="T22">One old man was driving a sledge with fat.</ta>
            <ta e="T35" id="Seg_3203" s="T29">Then, having gone, he took a piece of fat and went home.</ta>
            <ta e="T44" id="Seg_3204" s="T36">The hare's children, then the raven's children came.</ta>
            <ta e="T53" id="Seg_3205" s="T45">Then the hare… the hare nestling and the raven nestling were mocking each other.</ta>
            <ta e="T60" id="Seg_3206" s="T54">The raven nestling having gone home said like this.</ta>
            <ta e="T67" id="Seg_3207" s="T61">“The hare had brought a piece of fat.”</ta>
            <ta e="T75" id="Seg_3208" s="T68">Little raven and little hare lived with their parents, they lived.</ta>
            <ta e="T81" id="Seg_3209" s="T76">The hare went to the forest.</ta>
            <ta e="T87" id="Seg_3210" s="T82">An old man was driving a sledge with fat.</ta>
            <ta e="T91" id="Seg_3211" s="T88">The hare lived with their parents.</ta>
            <ta e="T102" id="Seg_3212" s="T92">One old man… the hare went to the forest, one old man was driving a sledge with fat.</ta>
            <ta e="T112" id="Seg_3213" s="T103">The hare went there, then he grasped a piece of fat, then he went home.</ta>
            <ta e="T118" id="Seg_3214" s="T113">Then he went home and fed the children.</ta>
            <ta e="T126" id="Seg_3215" s="T119">The children, the little hare mocks the little raven.</ta>
            <ta e="T135" id="Seg_3216" s="T127">The raven… the little raven having come home, they told to the mother.</ta>
            <ta e="T141" id="Seg_3217" s="T136">“The hare… the little hare… took a piece of fat.”</ta>
            <ta e="T147" id="Seg_3218" s="T142">Then the raven went to the house.</ta>
            <ta e="T155" id="Seg_3219" s="T148">The raven went away to the house, then she said.</ta>
            <ta e="T160" id="Seg_3220" s="T156">“Where did you find that piece of fat?”</ta>
            <ta e="T164" id="Seg_3221" s="T161">The hare said so.</ta>
            <ta e="T170" id="Seg_3222" s="T165">“I saw an old man in the forest.</ta>
            <ta e="T175" id="Seg_3223" s="T171">He was driving a sledge with fat.”</ta>
            <ta e="T179" id="Seg_3224" s="T176">Then he went to the forest.</ta>
            <ta e="T187" id="Seg_3225" s="T180">The hare had annointed the top of his head with snow, so he said.</ta>
            <ta e="T193" id="Seg_3226" s="T188">“there was such a snowstorm, nothing could be seen.”</ta>
            <ta e="T201" id="Seg_3227" s="T194">The raven sat down onto the sledge, kraa, she grasped a piece of something, a piece of food.</ta>
            <ta e="T210" id="Seg_3228" s="T202">Then the old man turned back, hit [the raven] with a stick, she died.</ta>
            <ta e="T217" id="Seg_3229" s="T211">The hare went there.</ta>
            <ta e="T223" id="Seg_3230" s="T218">He grasped a piece of fat and ran home.</ta>
            <ta e="T226" id="Seg_3231" s="T224">He fed the children.</ta>
            <ta e="T235" id="Seg_3232" s="T227">The little raven said so: “What is the mother doing?”</ta>
            <ta e="T238" id="Seg_3233" s="T235">“Your mother cannot [bring] the piece of fat.”</ta>
            <ta e="T246" id="Seg_3234" s="T239">Then the hare went home happy.</ta>
            <ta e="T255" id="Seg_3235" s="T247">The hare's [children] are eating, then the hare says so: “Where has your mother gone?</ta>
            <ta e="T262" id="Seg_3236" s="T256">Where has your mother gone, the old man hit your mother.”</ta>
            <ta e="T270" id="Seg_3237" s="T263">All the little ran into the small holes crying.</ta>
            <ta e="T274" id="Seg_3238" s="T271">The hare and his parents were eating.</ta>
            <ta e="T276" id="Seg_3239" s="T275">That's all.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_3240" s="T1">[KuAI:] Rita Kunina, ein Märchen; sprich.</ta>
            <ta e="T11" id="Seg_3241" s="T277">[KR:] Ein Hase und ein Rabe lebten mit ihren Eltern, sie lebten.</ta>
            <ta e="T21" id="Seg_3242" s="T12">Der Hase ging in den Wald, als er in den Wald ging, und dann…</ta>
            <ta e="T28" id="Seg_3243" s="T22">Ein alter Mann zog einen Schlitten mit Fett.</ta>
            <ta e="T35" id="Seg_3244" s="T29">Dann, als er gegangen war, nahm er ein wenig Fett und ging nach Hause.</ta>
            <ta e="T44" id="Seg_3245" s="T36">Die Kinder des Hasen, dann kamen die Kinder des Raben.</ta>
            <ta e="T53" id="Seg_3246" s="T45">Dann der Hase… das Hasenjunge und das Rabenjunge verspotteten einander.</ta>
            <ta e="T60" id="Seg_3247" s="T54">Das Rabenjunge sagte, als es nach Hause kam, dies.</ta>
            <ta e="T67" id="Seg_3248" s="T61">"Der Hase brachte dieses Stück Fett."</ta>
            <ta e="T75" id="Seg_3249" s="T68">Ein kleiner Hase und ein kleiner Rabe lebten mit ihren Eltern, sie lebten.</ta>
            <ta e="T81" id="Seg_3250" s="T76">Der Hase ging in den Wald.</ta>
            <ta e="T87" id="Seg_3251" s="T82">Ein alter Mann zog einen Schlitten mit Fett.</ta>
            <ta e="T91" id="Seg_3252" s="T88">Der Hase lebte mit seinen Eltern.</ta>
            <ta e="T102" id="Seg_3253" s="T92">Ein alter Mann… der Hase ging in den Wald, ein alter Mann zog einen Schlitten mit Fett.</ta>
            <ta e="T112" id="Seg_3254" s="T103">Der Hase ging dorthin, dann griff er sich ein Stück Fett, dann ging er nach Hause.</ta>
            <ta e="T118" id="Seg_3255" s="T113">Dann ging er nach Hause und fütterte seine Jungen.</ta>
            <ta e="T126" id="Seg_3256" s="T119">Die Jungen, ein Hasenjunges verspottet den kleinen Raben.</ta>
            <ta e="T135" id="Seg_3257" s="T127">Der Rabe… der kleine Rabe kam nach Hause, er sagte seiner Mutter.</ta>
            <ta e="T141" id="Seg_3258" s="T136">"Der Hase… das Hasenjunge… griff sich ein Stück Fett."</ta>
            <ta e="T147" id="Seg_3259" s="T142">Dann ging der Rabe zum Haus.</ta>
            <ta e="T155" id="Seg_3260" s="T148">Der Rabe ging fort zum Haus, dann sagte er so.</ta>
            <ta e="T160" id="Seg_3261" s="T156">"Wo hast du dieses Stück Fett gefunden?"</ta>
            <ta e="T164" id="Seg_3262" s="T161">Der Hase sagte so.</ta>
            <ta e="T170" id="Seg_3263" s="T165">"Ich sah einen alten Mann im Wald.</ta>
            <ta e="T175" id="Seg_3264" s="T171">Er zog einen Schlitten mit Fett."</ta>
            <ta e="T179" id="Seg_3265" s="T176">Dann ging er in den Wald.</ta>
            <ta e="T187" id="Seg_3266" s="T180">Der Hase hatte den Kopf oben mit Schnee eingerieben, so sagte er.</ta>
            <ta e="T193" id="Seg_3267" s="T188">"Es stürmte so sehr, man konnte nichts sehen."</ta>
            <ta e="T201" id="Seg_3268" s="T194">Der Rabe setzte sich auf den Schlitten, krah, packte etwas, ein Stück Essen.</ta>
            <ta e="T210" id="Seg_3269" s="T202">Dann drehte sich der alte Mann um, schlug [den Raben] mit einem Stock, er starb.</ta>
            <ta e="T217" id="Seg_3270" s="T211">Der Hase ging dorthin.</ta>
            <ta e="T223" id="Seg_3271" s="T218">Er schnappte sich ein Stück Fett, dann rannte er nach Hause.</ta>
            <ta e="T226" id="Seg_3272" s="T224">Er fütterte seine Jungen.</ta>
            <ta e="T235" id="Seg_3273" s="T227">Der kleine Rabe sagte so: "Was macht Mutter?"</ta>
            <ta e="T238" id="Seg_3274" s="T235">"Deine Mutter kann dir kein Fett [bringen]."</ta>
            <ta e="T246" id="Seg_3275" s="T239">Dann ging der Hase glücklich nach Hause.</ta>
            <ta e="T255" id="Seg_3276" s="T247">Die Hasenjungen essen, dann fragt der Hase so: "Wohin ist deine Mutter gegangen?"</ta>
            <ta e="T262" id="Seg_3277" s="T256">Wohin deine Mutter gegangen ist, der alte Mann erschlug deine Mutter."</ta>
            <ta e="T270" id="Seg_3278" s="T263">Alle Kleinen liefen weinend in die Löcher.</ta>
            <ta e="T274" id="Seg_3279" s="T271">Der Hase und seine Eltern aßen.</ta>
            <ta e="T276" id="Seg_3280" s="T275">Das ist alles.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_3281" s="T1">[KuAI:] Рита Кунина. Сказка. Говори.</ta>
            <ta e="T11" id="Seg_3282" s="T277">[KR:] Заяц и ворон с родителями жили, жили.</ta>
            <ta e="T21" id="Seg_3283" s="T12">Заяц, в лес пойдя, в лес пошёл, и вот.</ta>
            <ta e="T28" id="Seg_3284" s="T22">Один мужик нарты с жиром вёз.</ta>
            <ta e="T35" id="Seg_3285" s="T29">И вот, пойдя, взял кусок сала и домой пришёл.</ta>
            <ta e="T44" id="Seg_3286" s="T36">Зайца дети, эти вот потом воронихи дети пришли.</ta>
            <ta e="T53" id="Seg_3287" s="T45">И вот заяц … зайчонок и вороненок друг друга дразнили.</ta>
            <ta e="T60" id="Seg_3288" s="T54">Вороненок, идя домой, так говорит.</ta>
            <ta e="T67" id="Seg_3289" s="T61">“Это заяц ведь жир принёс”.</ta>
            <ta e="T75" id="Seg_3290" s="T68">Зайчонок и вороненок с родителями жили, жили.</ta>
            <ta e="T81" id="Seg_3291" s="T76">Заяц вот пошёл в лес.</ta>
            <ta e="T87" id="Seg_3292" s="T82">Один мужик сало на нартах вёз.</ta>
            <ta e="T91" id="Seg_3293" s="T88">Заяц с родителями жил.</ta>
            <ta e="T102" id="Seg_3294" s="T92">Один мужик… пошёл заяц в лес, один мужик сало на нартах вёз.</ta>
            <ta e="T112" id="Seg_3295" s="T103">Заяц пошёл туда, потом взял кусок сала, потом пошёл домой.</ta>
            <ta e="T118" id="Seg_3296" s="T113">Потом пошёл домой, детей накормил.</ta>
            <ta e="T126" id="Seg_3297" s="T119">Дети – один детёныш зайца, вороненок дразнится.</ta>
            <ta e="T135" id="Seg_3298" s="T127">Ворониха… вороненок, придя домой, маме так сказали.</ta>
            <ta e="T141" id="Seg_3299" s="T136">“Заяц… зайца детёныши… кусок жира взял”.</ta>
            <ta e="T147" id="Seg_3300" s="T142">Потом ворониха прочь домой пошла.</ta>
            <ta e="T155" id="Seg_3301" s="T148">Ворониха прочь домой пошла, потом так сказала.</ta>
            <ta e="T160" id="Seg_3302" s="T156">“Ты где кусок сала нашёл?”</ta>
            <ta e="T164" id="Seg_3303" s="T161">Заяц так говорит.</ta>
            <ta e="T170" id="Seg_3304" s="T165">“Я в лесу мужика видел, вот.</ta>
            <ta e="T175" id="Seg_3305" s="T171">Жир на нартах вёз”.</ta>
            <ta e="T179" id="Seg_3306" s="T176">Потом он в лес пошёл.</ta>
            <ta e="T187" id="Seg_3307" s="T180">Заяц макушку снегом натёр, так говорит.</ta>
            <ta e="T193" id="Seg_3308" s="T188">“Так замело, [ничего] не видать”.</ta>
            <ta e="T201" id="Seg_3309" s="T194">Ворониха на нарты села, кар, что-то, еду схватила.</ta>
            <ta e="T210" id="Seg_3310" s="T202">Потом мужик, назад обернувшись, палкой стукнул, [ворониха] умерла.</ta>
            <ta e="T217" id="Seg_3311" s="T211">Заяц тот пошёл туда.</ta>
            <ta e="T223" id="Seg_3312" s="T218">Кусок сала взял, потом домой побежал.</ta>
            <ta e="T226" id="Seg_3313" s="T224">Детишек своих накормил.</ta>
            <ta e="T235" id="Seg_3314" s="T227">Вороненок так сказал: “Что мама делает?”</ta>
            <ta e="T238" id="Seg_3315" s="T235">“Твоя мама жир не может [донести]”.</ta>
            <ta e="T246" id="Seg_3316" s="T239">Потом заяц, домой пойдя, обрадовался.</ta>
            <ta e="T255" id="Seg_3317" s="T247">Зайца [детеныши] едят, этот потом [заяц] так говорит: “Мама твоя куда девалась?</ta>
            <ta e="T262" id="Seg_3318" s="T256">Мама куда девалась, твою маму мужик ударил”.</ta>
            <ta e="T270" id="Seg_3319" s="T263">Все [воронята] заплакали, по маленьким щелям разбежались.</ta>
            <ta e="T274" id="Seg_3320" s="T271">Зайяц с его родителями кушают.</ta>
            <ta e="T276" id="Seg_3321" s="T275">Всё.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_3322" s="T1">Рита Кунина. Сказка</ta>
            <ta e="T11" id="Seg_3323" s="T277">заяц и ворона с родителями жили, жили.</ta>
            <ta e="T21" id="Seg_3324" s="T12">заяц… заяц в лес пошёл, в лес пошёл и вот.</ta>
            <ta e="T28" id="Seg_3325" s="T22">один мужик нарты с жиром вёз.</ta>
            <ta e="T35" id="Seg_3326" s="T29">и вот пошёл, взял жир и домой пошёл.</ta>
            <ta e="T44" id="Seg_3327" s="T36">зайца дети эти вот потом вороны дети пришли</ta>
            <ta e="T53" id="Seg_3328" s="T45">и вот заяц … зайца детёныш и вороны детёныш друг друга дразнили.</ta>
            <ta e="T60" id="Seg_3329" s="T54">вороны детёныш идя домой говорит</ta>
            <ta e="T67" id="Seg_3330" s="T61">заяц ведь жир принёс.</ta>
            <ta e="T75" id="Seg_3331" s="T68">заяй и ворона с родителями жили, жили.</ta>
            <ta e="T81" id="Seg_3332" s="T76">заяц вот пошёл в лес.</ta>
            <ta e="T87" id="Seg_3333" s="T82">один мужик жир на нартах вёз.</ta>
            <ta e="T91" id="Seg_3334" s="T88">заяц с родителями жил.</ta>
            <ta e="T102" id="Seg_3335" s="T92">один мужик… пошёл заяц в лес, один мужик жир на нартах вёз.</ta>
            <ta e="T112" id="Seg_3336" s="T103">заяц пошёл туда, потом взял жир, потом пошёл домой.</ta>
            <ta e="T118" id="Seg_3337" s="T113">потом пошёл домой, детей накормил.</ta>
            <ta e="T126" id="Seg_3338" s="T119">дети - один детёныш зайца, вороны детишки дразнятся.</ta>
            <ta e="T135" id="Seg_3339" s="T127">ворона… вороны дети идя домой маме так сказали.</ta>
            <ta e="T141" id="Seg_3340" s="T136">заяц… зайца детёныш жир взял.</ta>
            <ta e="T147" id="Seg_3341" s="T142">потом ворона домой пошла.</ta>
            <ta e="T155" id="Seg_3342" s="T148">ворона домой пошла, потом говорит.</ta>
            <ta e="T160" id="Seg_3343" s="T156">ты где этот жир нашёл?</ta>
            <ta e="T164" id="Seg_3344" s="T161">заяц так говорит.</ta>
            <ta e="T170" id="Seg_3345" s="T165">я в лесу мужика видел, вот.</ta>
            <ta e="T175" id="Seg_3346" s="T171">жир на нартах вёз.</ta>
            <ta e="T179" id="Seg_3347" s="T176">в лес пошёл.</ta>
            <ta e="T187" id="Seg_3348" s="T180">заяц макушку снегом натёр, так говорит.</ta>
            <ta e="T193" id="Seg_3349" s="T188">так замело, ничего не видать.</ta>
            <ta e="T201" id="Seg_3350" s="T194">ворона на нарты села кар что-то еду схватила.</ta>
            <ta e="T210" id="Seg_3351" s="T202">потом мужик назад обернулся. палкой стукнул, та умерла.</ta>
            <ta e="T217" id="Seg_3352" s="T211">заяц пошёл туда.</ta>
            <ta e="T223" id="Seg_3353" s="T218">жир взял, и домой убежал.</ta>
            <ta e="T226" id="Seg_3354" s="T224">детишек накормил.</ta>
            <ta e="T235" id="Seg_3355" s="T227">ворона детёныш так сказал: что мама делает, т</ta>
            <ta e="T238" id="Seg_3356" s="T235">ворона детёныш так сказал: что мама делает, твоя мама жир не может донести (?).</ta>
            <ta e="T246" id="Seg_3357" s="T239">потом заяц пошёл домой, обрадовался.</ta>
            <ta e="T255" id="Seg_3358" s="T247">зайца едят, потом говорит: мама куда девалась?</ta>
            <ta e="T262" id="Seg_3359" s="T256">мама куда девалась, твою маму мужик убил.</ta>
            <ta e="T270" id="Seg_3360" s="T263">все заплакали, по щелям разбежались.</ta>
            <ta e="T274" id="Seg_3361" s="T271">зайца с его родителями кушают.</ta>
            <ta e="T276" id="Seg_3362" s="T275">всё.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T75" id="Seg_3363" s="T68">[BrM:] After a break in the recording, the story is being told once again from the beginning.</ta>
            <ta e="T87" id="Seg_3364" s="T82">[BrM:] Nominative in 'qaqlɨ' - 'sledges'?</ta>
            <ta e="T102" id="Seg_3365" s="T92">[BrM:] Tenative glossing of 'qənpanɨ'. Nominative in 'qaqlɨ' – 'sledges'?</ta>
            <ta e="T141" id="Seg_3366" s="T136">[BrM:] Tentative transcription of 'orqɨlʼpat'. Unclear agreemet.</ta>
            <ta e="T193" id="Seg_3367" s="T188">[BrM:] Tentative transcription and analysis of 'štɨl'.</ta>
            <ta e="T262" id="Seg_3368" s="T256">[BrM:] Objective conjugation in 'qattɨsat'? Shouldnʼt it be 'qättɨsɨtɨ'?</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T277" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
