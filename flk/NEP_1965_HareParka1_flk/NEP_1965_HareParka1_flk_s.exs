<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>NEP_1965_HareParka_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">NEP_1965_HareParka1_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">589</ud-information>
            <ud-information attribute-name="# HIAT:w">454</ud-information>
            <ud-information attribute-name="# e">454</ud-information>
            <ud-information attribute-name="# HIAT:u">92</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="NEP">
            <abbreviation>NEP</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
         <tli id="T204" />
         <tli id="T205" />
         <tli id="T206" />
         <tli id="T207" />
         <tli id="T208" />
         <tli id="T209" />
         <tli id="T210" />
         <tli id="T211" />
         <tli id="T212" />
         <tli id="T213" />
         <tli id="T214" />
         <tli id="T215" />
         <tli id="T216" />
         <tli id="T217" />
         <tli id="T218" />
         <tli id="T219" />
         <tli id="T220" />
         <tli id="T221" />
         <tli id="T222" />
         <tli id="T223" />
         <tli id="T224" />
         <tli id="T225" />
         <tli id="T226" />
         <tli id="T227" />
         <tli id="T228" />
         <tli id="T229" />
         <tli id="T230" />
         <tli id="T231" />
         <tli id="T232" />
         <tli id="T233" />
         <tli id="T234" />
         <tli id="T235" />
         <tli id="T236" />
         <tli id="T237" />
         <tli id="T238" />
         <tli id="T239" />
         <tli id="T240" />
         <tli id="T241" />
         <tli id="T242" />
         <tli id="T243" />
         <tli id="T244" />
         <tli id="T245" />
         <tli id="T246" />
         <tli id="T247" />
         <tli id="T248" />
         <tli id="T249" />
         <tli id="T250" />
         <tli id="T251" />
         <tli id="T252" />
         <tli id="T253" />
         <tli id="T254" />
         <tli id="T255" />
         <tli id="T256" />
         <tli id="T257" />
         <tli id="T258" />
         <tli id="T259" />
         <tli id="T260" />
         <tli id="T261" />
         <tli id="T262" />
         <tli id="T263" />
         <tli id="T264" />
         <tli id="T265" />
         <tli id="T266" />
         <tli id="T267" />
         <tli id="T268" />
         <tli id="T269" />
         <tli id="T270" />
         <tli id="T271" />
         <tli id="T272" />
         <tli id="T273" />
         <tli id="T274" />
         <tli id="T275" />
         <tli id="T276" />
         <tli id="T277" />
         <tli id="T278" />
         <tli id="T279" />
         <tli id="T280" />
         <tli id="T281" />
         <tli id="T282" />
         <tli id="T283" />
         <tli id="T284" />
         <tli id="T285" />
         <tli id="T286" />
         <tli id="T287" />
         <tli id="T288" />
         <tli id="T289" />
         <tli id="T290" />
         <tli id="T291" />
         <tli id="T292" />
         <tli id="T293" />
         <tli id="T294" />
         <tli id="T295" />
         <tli id="T296" />
         <tli id="T297" />
         <tli id="T298" />
         <tli id="T299" />
         <tli id="T300" />
         <tli id="T301" />
         <tli id="T302" />
         <tli id="T303" />
         <tli id="T304" />
         <tli id="T305" />
         <tli id="T306" />
         <tli id="T307" />
         <tli id="T308" />
         <tli id="T309" />
         <tli id="T310" />
         <tli id="T311" />
         <tli id="T312" />
         <tli id="T313" />
         <tli id="T314" />
         <tli id="T315" />
         <tli id="T316" />
         <tli id="T317" />
         <tli id="T318" />
         <tli id="T319" />
         <tli id="T320" />
         <tli id="T321" />
         <tli id="T322" />
         <tli id="T323" />
         <tli id="T324" />
         <tli id="T325" />
         <tli id="T326" />
         <tli id="T327" />
         <tli id="T328" />
         <tli id="T329" />
         <tli id="T330" />
         <tli id="T331" />
         <tli id="T332" />
         <tli id="T333" />
         <tli id="T334" />
         <tli id="T454" />
         <tli id="T335" />
         <tli id="T336" />
         <tli id="T337" />
         <tli id="T338" />
         <tli id="T339" />
         <tli id="T340" />
         <tli id="T341" />
         <tli id="T342" />
         <tli id="T343" />
         <tli id="T344" />
         <tli id="T345" />
         <tli id="T346" />
         <tli id="T347" />
         <tli id="T348" />
         <tli id="T349" />
         <tli id="T350" />
         <tli id="T351" />
         <tli id="T352" />
         <tli id="T353" />
         <tli id="T354" />
         <tli id="T355" />
         <tli id="T356" />
         <tli id="T357" />
         <tli id="T358" />
         <tli id="T359" />
         <tli id="T360" />
         <tli id="T361" />
         <tli id="T362" />
         <tli id="T363" />
         <tli id="T364" />
         <tli id="T365" />
         <tli id="T366" />
         <tli id="T367" />
         <tli id="T368" />
         <tli id="T369" />
         <tli id="T370" />
         <tli id="T371" />
         <tli id="T372" />
         <tli id="T373" />
         <tli id="T374" />
         <tli id="T375" />
         <tli id="T376" />
         <tli id="T377" />
         <tli id="T378" />
         <tli id="T379" />
         <tli id="T380" />
         <tli id="T381" />
         <tli id="T382" />
         <tli id="T383" />
         <tli id="T384" />
         <tli id="T385" />
         <tli id="T386" />
         <tli id="T387" />
         <tli id="T388" />
         <tli id="T389" />
         <tli id="T390" />
         <tli id="T391" />
         <tli id="T392" />
         <tli id="T393" />
         <tli id="T394" />
         <tli id="T395" />
         <tli id="T396" />
         <tli id="T397" />
         <tli id="T398" />
         <tli id="T399" />
         <tli id="T400" />
         <tli id="T401" />
         <tli id="T402" />
         <tli id="T403" />
         <tli id="T404" />
         <tli id="T405" />
         <tli id="T406" />
         <tli id="T407" />
         <tli id="T408" />
         <tli id="T409" />
         <tli id="T410" />
         <tli id="T411" />
         <tli id="T412" />
         <tli id="T413" />
         <tli id="T414" />
         <tli id="T415" />
         <tli id="T416" />
         <tli id="T417" />
         <tli id="T418" />
         <tli id="T419" />
         <tli id="T420" />
         <tli id="T421" />
         <tli id="T422" />
         <tli id="T423" />
         <tli id="T424" />
         <tli id="T425" />
         <tli id="T426" />
         <tli id="T427" />
         <tli id="T428" />
         <tli id="T429" />
         <tli id="T430" />
         <tli id="T431" />
         <tli id="T432" />
         <tli id="T433" />
         <tli id="T434" />
         <tli id="T435" />
         <tli id="T436" />
         <tli id="T437" />
         <tli id="T438" />
         <tli id="T439" />
         <tli id="T440" />
         <tli id="T441" />
         <tli id="T442" />
         <tli id="T443" />
         <tli id="T444" />
         <tli id="T445" />
         <tli id="T446" />
         <tli id="T447" />
         <tli id="T448" />
         <tli id="T449" />
         <tli id="T450" />
         <tli id="T451" />
         <tli id="T452" />
         <tli id="T453" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="NEP"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T453" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Nʼomalʼ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">porqɨ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">ira</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Seːlʼčʼi</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">ijatɨ</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">ɛːppäntɨ</ts>
                  <nts id="Seg_23" n="HIAT:ip">,</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">aj</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">seːlʼčʼi</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">amnätɨ</ts>
                  <nts id="Seg_33" n="HIAT:ip">.</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_36" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_38" n="HIAT:w" s="T9">Qaːlit</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_41" n="HIAT:w" s="T10">tüŋɔːtɨt</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">takkɨnɨ</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_46" n="HIAT:ip">(</nts>
                  <nts id="Seg_47" n="HIAT:ip">/</nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">takkɨntɨ</ts>
                  <nts id="Seg_50" n="HIAT:ip">)</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_53" n="HIAT:w" s="T13">nʼannä</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">täpɨp</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">qətqɨntɨtqo</ts>
                  <nts id="Seg_60" n="HIAT:ip">.</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_63" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_65" n="HIAT:w" s="T16">Qumijtɨ</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_68" n="HIAT:w" s="T17">ijaiːtɨ</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_71" n="HIAT:w" s="T18">mačʼontɨ</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_74" n="HIAT:w" s="T19">qənpɔːtɨt</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_77" n="HIAT:w" s="T20">täpälʼlʼä</ts>
                  <nts id="Seg_78" n="HIAT:ip">.</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_81" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_83" n="HIAT:w" s="T21">Qup</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_86" n="HIAT:w" s="T22">tiː</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_89" n="HIAT:w" s="T23">qəttiŋɨt</ts>
                  <nts id="Seg_90" n="HIAT:ip">.</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_93" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_95" n="HIAT:w" s="T24">Kuttar</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_98" n="HIAT:w" s="T25">ɛːŋa</ts>
                  <nts id="Seg_99" n="HIAT:ip">?</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_102" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_104" n="HIAT:w" s="T26">Köt</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_107" n="HIAT:w" s="T27">šolʼlʼiqumɨlʼ</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_110" n="HIAT:w" s="T28">ijakɨp</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_113" n="HIAT:w" s="T29">märɨmpɔːtɨt</ts>
                  <nts id="Seg_114" n="HIAT:ip">.</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_117" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_119" n="HIAT:w" s="T30">Na</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_122" n="HIAT:w" s="T31">qälit</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_125" n="HIAT:w" s="T32">namɨp</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_128" n="HIAT:w" s="T33">üːtɨntɨt</ts>
                  <nts id="Seg_129" n="HIAT:ip">.</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_132" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_134" n="HIAT:w" s="T34">Nʼomalʼ</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_137" n="HIAT:w" s="T35">porqɨ</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_140" n="HIAT:w" s="T36">iratkine</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_143" n="HIAT:w" s="T37">meː</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_146" n="HIAT:w" s="T38">täːlɨ</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_149" n="HIAT:w" s="T39">mɨta</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_152" n="HIAT:w" s="T40">tüntɔːmɨt</ts>
                  <nts id="Seg_153" n="HIAT:ip">.</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_156" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_158" n="HIAT:w" s="T41">Qonninɨlɨt</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_160" n="HIAT:ip">(</nts>
                  <nts id="Seg_161" n="HIAT:ip">/</nts>
                  <ts e="T43" id="Seg_163" n="HIAT:w" s="T42">qonninɨt</ts>
                  <nts id="Seg_164" n="HIAT:ip">)</nts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_167" n="HIAT:w" s="T43">əːtɨtqo</ts>
                  <nts id="Seg_168" n="HIAT:ip">.</nts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_171" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_173" n="HIAT:w" s="T44">Ijaqı</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_176" n="HIAT:w" s="T45">moqonä</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_179" n="HIAT:w" s="T46">qəlʼlʼä</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_182" n="HIAT:w" s="T47">nık</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_185" n="HIAT:w" s="T48">kətɨŋɨt</ts>
                  <nts id="Seg_186" n="HIAT:ip">:</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_189" n="HIAT:w" s="T49">Čʼilʼälʼ</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_192" n="HIAT:w" s="T50">ijaqıj</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_195" n="HIAT:w" s="T51">qälɨqqı</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_198" n="HIAT:w" s="T52">mɨrɨŋɨt</ts>
                  <nts id="Seg_199" n="HIAT:ip">.</nts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_202" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_204" n="HIAT:w" s="T53">Nɨːnɨ</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_207" n="HIAT:w" s="T54">šittelʼ</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_210" n="HIAT:w" s="T55">irra</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_213" n="HIAT:w" s="T56">kučʼe</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_216" n="HIAT:w" s="T57">qatɛnta</ts>
                  <nts id="Seg_217" n="HIAT:ip">?</nts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_220" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_222" n="HIAT:w" s="T58">Ira</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_225" n="HIAT:w" s="T59">nılʼčʼik</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_228" n="HIAT:w" s="T60">ɛssa</ts>
                  <nts id="Seg_229" n="HIAT:ip">,</nts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_232" n="HIAT:w" s="T61">na</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_235" n="HIAT:w" s="T62">nʼomalʼ</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_238" n="HIAT:w" s="T63">porqə</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_241" n="HIAT:w" s="T64">ira</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_244" n="HIAT:w" s="T65">amnäntɨlʼ</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_247" n="HIAT:w" s="T66">mɨtkine</ts>
                  <nts id="Seg_248" n="HIAT:ip">:</nts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_251" n="HIAT:w" s="T67">Ür</ts>
                  <nts id="Seg_252" n="HIAT:ip">,</nts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_255" n="HIAT:w" s="T68">apsɨ</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_258" n="HIAT:w" s="T69">tattɨŋɨt</ts>
                  <nts id="Seg_259" n="HIAT:ip">.</nts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_262" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_264" n="HIAT:w" s="T70">Kun</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_267" n="HIAT:w" s="T71">montɨlʼ</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_270" n="HIAT:w" s="T72">qaj</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_273" n="HIAT:w" s="T73">tattɨŋɨt</ts>
                  <nts id="Seg_274" n="HIAT:ip">.</nts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_277" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_279" n="HIAT:w" s="T74">Qälit</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_282" n="HIAT:w" s="T75">täːlʼe</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_285" n="HIAT:w" s="T76">tüntɔːtɨt</ts>
                  <nts id="Seg_286" n="HIAT:ip">,</nts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_289" n="HIAT:w" s="T77">amɨrtɛntɔːtɨt</ts>
                  <nts id="Seg_290" n="HIAT:ip">.</nts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_293" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_295" n="HIAT:w" s="T78">Ira</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_298" n="HIAT:w" s="T79">nık</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_301" n="HIAT:w" s="T80">kətɨt</ts>
                  <nts id="Seg_302" n="HIAT:ip">.</nts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_305" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_307" n="HIAT:w" s="T81">Apsɨp</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_310" n="HIAT:w" s="T82">tattɔːtɨt</ts>
                  <nts id="Seg_311" n="HIAT:ip">.</nts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_314" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_316" n="HIAT:w" s="T83">Tam</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_319" n="HIAT:w" s="T84">mɔːtɨt</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_322" n="HIAT:w" s="T85">šünʼčʼip</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_325" n="HIAT:w" s="T86">šentɨ</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_328" n="HIAT:w" s="T87">nʼuːtɨsä</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_331" n="HIAT:w" s="T88">tɔːqqɔːtɨt</ts>
                  <nts id="Seg_332" n="HIAT:ip">.</nts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_335" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_337" n="HIAT:w" s="T89">Ira</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_340" n="HIAT:w" s="T90">qälimtɨ</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_343" n="HIAT:w" s="T91">apsɨtɨqolamnɨt</ts>
                  <nts id="Seg_344" n="HIAT:ip">.</nts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_347" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_349" n="HIAT:w" s="T92">Əː</ts>
                  <nts id="Seg_350" n="HIAT:ip">!</nts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T99" id="Seg_353" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_355" n="HIAT:w" s="T93">Tɔːptɨlʼ</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_358" n="HIAT:w" s="T94">čʼeːlʼɨ</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_361" n="HIAT:w" s="T95">qälɨt</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_364" n="HIAT:w" s="T96">na</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_367" n="HIAT:w" s="T97">jep</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_370" n="HIAT:w" s="T98">tatɨntɔːtɨt</ts>
                  <nts id="Seg_371" n="HIAT:ip">.</nts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_374" n="HIAT:u" s="T99">
                  <ts e="T100" id="Seg_376" n="HIAT:w" s="T99">Nɔːssarɨlʼ</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_379" n="HIAT:w" s="T100">qumɨlʼ</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_382" n="HIAT:w" s="T101">mütɨ</ts>
                  <nts id="Seg_383" n="HIAT:ip">.</nts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_386" n="HIAT:u" s="T102">
                  <ts e="T103" id="Seg_388" n="HIAT:w" s="T102">Qapija</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_391" n="HIAT:w" s="T103">šünʼčʼipɨlʼ</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_394" n="HIAT:w" s="T104">mɔːtqɨt</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_397" n="HIAT:w" s="T105">nʼomalʼ</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_400" n="HIAT:w" s="T106">porqɨ</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_403" n="HIAT:w" s="T107">ira</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_406" n="HIAT:w" s="T108">ontɨqɨt</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_409" n="HIAT:w" s="T109">amnäiːntɨsä</ts>
                  <nts id="Seg_410" n="HIAT:ip">.</nts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T114" id="Seg_413" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_415" n="HIAT:w" s="T110">Šentɨ</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_418" n="HIAT:w" s="T111">nʼuːtɨsa</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_421" n="HIAT:w" s="T112">tɔːqɨralʼtɨt</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_423" n="HIAT:ip">(</nts>
                  <nts id="Seg_424" n="HIAT:ip">/</nts>
                  <ts e="T114" id="Seg_426" n="HIAT:w" s="T113">tɔːqɨralʼimpɔːtɨt</ts>
                  <nts id="Seg_427" n="HIAT:ip">)</nts>
                  <nts id="Seg_428" n="HIAT:ip">.</nts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T117" id="Seg_431" n="HIAT:u" s="T114">
                  <ts e="T115" id="Seg_433" n="HIAT:w" s="T114">Ira</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_436" n="HIAT:w" s="T115">nılʼčʼik</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_439" n="HIAT:w" s="T116">ɛsa</ts>
                  <nts id="Seg_440" n="HIAT:ip">.</nts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_443" n="HIAT:u" s="T117">
                  <ts e="T118" id="Seg_445" n="HIAT:w" s="T117">Amɨrqolamnɔːtɨt</ts>
                  <nts id="Seg_446" n="HIAT:ip">.</nts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_449" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_451" n="HIAT:w" s="T118">Apsɨp</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_454" n="HIAT:w" s="T119">qontɔːtɨt</ts>
                  <nts id="Seg_455" n="HIAT:ip">.</nts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_458" n="HIAT:u" s="T120">
                  <ts e="T121" id="Seg_460" n="HIAT:w" s="T120">Ira</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_463" n="HIAT:w" s="T121">nık</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_466" n="HIAT:w" s="T122">kəttɨŋɨt</ts>
                  <nts id="Seg_467" n="HIAT:ip">.</nts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_470" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_472" n="HIAT:w" s="T123">Imaqotatɨ</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_475" n="HIAT:w" s="T124">aj</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_478" n="HIAT:w" s="T125">ɛːŋa</ts>
                  <nts id="Seg_479" n="HIAT:ip">.</nts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T129" id="Seg_482" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_484" n="HIAT:w" s="T126">Amnäiːmtɨ</ts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_487" n="HIAT:w" s="T127">nık</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_490" n="HIAT:w" s="T128">kuraltɨt</ts>
                  <nts id="Seg_491" n="HIAT:ip">.</nts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_494" n="HIAT:u" s="T129">
                  <ts e="T130" id="Seg_496" n="HIAT:w" s="T129">Ponä</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_499" n="HIAT:w" s="T130">tantɨŋɨlɨt</ts>
                  <nts id="Seg_500" n="HIAT:ip">.</nts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T134" id="Seg_503" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_505" n="HIAT:w" s="T131">Ijatɨp</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_508" n="HIAT:w" s="T132">pona</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_511" n="HIAT:w" s="T133">tattɨŋɨlɨt</ts>
                  <nts id="Seg_512" n="HIAT:ip">.</nts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_515" n="HIAT:u" s="T134">
                  <ts e="T135" id="Seg_517" n="HIAT:w" s="T134">Ira</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_520" n="HIAT:w" s="T135">ontɨ</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_523" n="HIAT:w" s="T136">wärəkka</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_526" n="HIAT:w" s="T137">qumimtɨ</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_529" n="HIAT:w" s="T138">apsɨtɨmpɨlʼä</ts>
                  <nts id="Seg_530" n="HIAT:ip">.</nts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T145" id="Seg_533" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_535" n="HIAT:w" s="T139">Ukkur</ts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_538" n="HIAT:w" s="T140">tət</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_541" n="HIAT:w" s="T141">čʼontoːqɨt</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_544" n="HIAT:w" s="T142">tüntə</ts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_547" n="HIAT:w" s="T143">qaret</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_550" n="HIAT:w" s="T144">qup</ts>
                  <nts id="Seg_551" n="HIAT:ip">.</nts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T146" id="Seg_554" n="HIAT:u" s="T145">
                  <ts e="T146" id="Seg_556" n="HIAT:w" s="T145">Amɨrnɔːt</ts>
                  <nts id="Seg_557" n="HIAT:ip">.</nts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_560" n="HIAT:u" s="T146">
                  <ts e="T147" id="Seg_562" n="HIAT:w" s="T146">Šoŋolʼilʼ</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_564" n="HIAT:ip">(</nts>
                  <nts id="Seg_565" n="HIAT:ip">/</nts>
                  <ts e="T148" id="Seg_567" n="HIAT:w" s="T147">šoŋolʼ</ts>
                  <nts id="Seg_568" n="HIAT:ip">)</nts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_571" n="HIAT:w" s="T148">tümtɨ</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_574" n="HIAT:w" s="T149">nam</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_577" n="HIAT:w" s="T150">märɨq</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_580" n="HIAT:w" s="T151">poːsa</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_583" n="HIAT:w" s="T152">səlɨŋɨt</ts>
                  <nts id="Seg_584" n="HIAT:ip">.</nts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T154" id="Seg_587" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_589" n="HIAT:w" s="T153">Čʼɔːpɨnta</ts>
                  <nts id="Seg_590" n="HIAT:ip">.</nts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_593" n="HIAT:u" s="T154">
                  <ts e="T155" id="Seg_595" n="HIAT:w" s="T154">Amnäiːmtɨ</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_598" n="HIAT:w" s="T155">aša</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_601" n="HIAT:w" s="T156">qaː</ts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_603" n="HIAT:ip">(</nts>
                  <nts id="Seg_604" n="HIAT:ip">/</nts>
                  <ts e="T158" id="Seg_606" n="HIAT:w" s="T157">qajqo</ts>
                  <nts id="Seg_607" n="HIAT:ip">)</nts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_610" n="HIAT:w" s="T158">pona</ts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_613" n="HIAT:w" s="T159">üːteŋɨt</ts>
                  <nts id="Seg_614" n="HIAT:ip">.</nts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T163" id="Seg_617" n="HIAT:u" s="T160">
                  <ts e="T161" id="Seg_619" n="HIAT:w" s="T160">Poːqɨt</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_622" n="HIAT:w" s="T161">tü</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_625" n="HIAT:w" s="T162">čʼɔːtentɔːtet</ts>
                  <nts id="Seg_626" n="HIAT:ip">.</nts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_629" n="HIAT:u" s="T163">
                  <ts e="T164" id="Seg_631" n="HIAT:w" s="T163">Tüp</ts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_634" n="HIAT:w" s="T164">čʼɔːtälpɔːtet</ts>
                  <nts id="Seg_635" n="HIAT:ip">,</nts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_638" n="HIAT:w" s="T165">tintena</ts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_641" n="HIAT:w" s="T166">töp</ts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_644" n="HIAT:w" s="T167">purɨšiŋɔːlpɔːtɨt</ts>
                  <nts id="Seg_645" n="HIAT:ip">.</nts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T171" id="Seg_648" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_650" n="HIAT:w" s="T168">Nılʼčʼik</ts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_653" n="HIAT:w" s="T169">qaj</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_656" n="HIAT:w" s="T170">təːkɨrɨmpɔːtɨt</ts>
                  <nts id="Seg_657" n="HIAT:ip">.</nts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T177" id="Seg_660" n="HIAT:u" s="T171">
                  <ts e="T172" id="Seg_662" n="HIAT:w" s="T171">Tɛː</ts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_665" n="HIAT:w" s="T172">na</ts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_668" n="HIAT:w" s="T173">ɛːntɨlʼ</ts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_671" n="HIAT:w" s="T174">man</ts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_674" n="HIAT:w" s="T175">na</ts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_677" n="HIAT:w" s="T176">tannɛntak</ts>
                  <nts id="Seg_678" n="HIAT:ip">.</nts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T178" id="Seg_681" n="HIAT:u" s="T177">
                  <ts e="T178" id="Seg_683" n="HIAT:w" s="T177">Amɨrnɔːtɨt</ts>
                  <nts id="Seg_684" n="HIAT:ip">.</nts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T187" id="Seg_687" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_689" n="HIAT:w" s="T178">Qälit</ts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_692" n="HIAT:w" s="T179">nık</ts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_695" n="HIAT:w" s="T180">kətɔːtät</ts>
                  <nts id="Seg_696" n="HIAT:ip">:</nts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_699" n="HIAT:w" s="T181">Ilʼčʼa</ts>
                  <nts id="Seg_700" n="HIAT:ip">,</nts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_703" n="HIAT:w" s="T182">šoŋolʼaj</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_706" n="HIAT:w" s="T183">qajetqe</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_709" n="HIAT:w" s="T184">našak</ts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_712" n="HIAT:w" s="T185">poːsä</ts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_715" n="HIAT:w" s="T186">säːlal</ts>
                  <nts id="Seg_716" n="HIAT:ip">.</nts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T193" id="Seg_719" n="HIAT:u" s="T187">
                  <ts e="T188" id="Seg_721" n="HIAT:w" s="T187">Aša</ts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_724" n="HIAT:w" s="T188">šoŋol</ts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_727" n="HIAT:w" s="T189">mɨ</ts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_730" n="HIAT:w" s="T190">tü</ts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_733" n="HIAT:w" s="T191">na</ts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_736" n="HIAT:w" s="T192">amtɨt</ts>
                  <nts id="Seg_737" n="HIAT:ip">?</nts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T196" id="Seg_740" n="HIAT:u" s="T193">
                  <ts e="T194" id="Seg_742" n="HIAT:w" s="T193">Täm</ts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_745" n="HIAT:w" s="T194">aš</ts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_748" n="HIAT:w" s="T195">amtɨt</ts>
                  <nts id="Seg_749" n="HIAT:ip">.</nts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T202" id="Seg_752" n="HIAT:u" s="T196">
                  <ts e="T197" id="Seg_754" n="HIAT:w" s="T196">Nık</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_757" n="HIAT:w" s="T197">märkeptɨmpa</ts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_760" n="HIAT:w" s="T198">nʼomalʼ</ts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_763" n="HIAT:w" s="T199">porqɨntɨ</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_766" n="HIAT:w" s="T200">rašalʼ</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_769" n="HIAT:w" s="T201">lʼıːpɨ</ts>
                  <nts id="Seg_770" n="HIAT:ip">.</nts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T209" id="Seg_773" n="HIAT:u" s="T202">
                  <ts e="T203" id="Seg_775" n="HIAT:w" s="T202">Nammɨ</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_778" n="HIAT:w" s="T203">šeːrpɨntɨt</ts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_781" n="HIAT:w" s="T204">šoŋoltɨ</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_784" n="HIAT:w" s="T205">qanɨqqɨt</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_787" n="HIAT:w" s="T206">wärqa</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_790" n="HIAT:w" s="T207">mɔːtantɨ</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_793" n="HIAT:w" s="T208">qanɨqqɨt</ts>
                  <nts id="Seg_794" n="HIAT:ip">.</nts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T218" id="Seg_797" n="HIAT:u" s="T209">
                  <ts e="T210" id="Seg_799" n="HIAT:w" s="T209">Ukkur</ts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_802" n="HIAT:w" s="T210">tət</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_805" n="HIAT:w" s="T211">čʼontoːqɨt</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_808" n="HIAT:w" s="T212">poːqɨnä</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_811" n="HIAT:w" s="T213">mɔːttɨ</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_814" n="HIAT:w" s="T214">tülʼ</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_817" n="HIAT:w" s="T215">purɨšilʼ</ts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_820" n="HIAT:w" s="T216">laka</ts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_823" n="HIAT:w" s="T217">alʼčʼinta</ts>
                  <nts id="Seg_824" n="HIAT:ip">.</nts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T220" id="Seg_827" n="HIAT:u" s="T218">
                  <ts e="T219" id="Seg_829" n="HIAT:w" s="T218">No</ts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_832" n="HIAT:w" s="T219">paqtɨmmɨnta</ts>
                  <nts id="Seg_833" n="HIAT:ip">.</nts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T231" id="Seg_836" n="HIAT:u" s="T220">
                  <ts e="T221" id="Seg_838" n="HIAT:w" s="T220">Mɔːtantɨ</ts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_841" n="HIAT:w" s="T221">qälɨt</ts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_844" n="HIAT:w" s="T222">čʼap</ts>
                  <nts id="Seg_845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_847" n="HIAT:w" s="T223">orqɨlpɔːtɨt</ts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_850" n="HIAT:w" s="T224">irap</ts>
                  <nts id="Seg_851" n="HIAT:ip">,</nts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_854" n="HIAT:w" s="T225">ola</ts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_857" n="HIAT:w" s="T226">čʼalʼ</ts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_860" n="HIAT:w" s="T227">nʼomalʼ</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_863" n="HIAT:w" s="T228">porqɨ</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_866" n="HIAT:w" s="T229">lʼıːpɨp</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_869" n="HIAT:w" s="T230">orqɨlpɔːtät</ts>
                  <nts id="Seg_870" n="HIAT:ip">.</nts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T237" id="Seg_873" n="HIAT:u" s="T231">
                  <ts e="T232" id="Seg_875" n="HIAT:w" s="T231">Qapija</ts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_878" n="HIAT:w" s="T232">na</ts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_881" n="HIAT:w" s="T233">tüsa</ts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_884" n="HIAT:w" s="T234">okoškan</ts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_887" n="HIAT:w" s="T235">ɔːkmɨt</ts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_890" n="HIAT:w" s="T236">mɔːttɨ</ts>
                  <nts id="Seg_891" n="HIAT:ip">.</nts>
                  <nts id="Seg_892" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T240" id="Seg_894" n="HIAT:u" s="T237">
                  <ts e="T238" id="Seg_896" n="HIAT:w" s="T237">Šitə</ts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_899" n="HIAT:w" s="T238">okoškatə</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_902" n="HIAT:w" s="T239">ɛːpɨmpa</ts>
                  <nts id="Seg_903" n="HIAT:ip">.</nts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T244" id="Seg_906" n="HIAT:u" s="T240">
                  <ts e="T241" id="Seg_908" n="HIAT:w" s="T240">Qaj</ts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_911" n="HIAT:w" s="T241">märqɨ</ts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_914" n="HIAT:w" s="T242">mɔːttɨ</ts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_917" n="HIAT:w" s="T243">ɛːpɨmmɨnta</ts>
                  <nts id="Seg_918" n="HIAT:ip">!</nts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T253" id="Seg_921" n="HIAT:u" s="T244">
                  <ts e="T245" id="Seg_923" n="HIAT:w" s="T244">Tüsa</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_926" n="HIAT:w" s="T245">taqpa</ts>
                  <nts id="Seg_927" n="HIAT:ip">,</nts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_930" n="HIAT:w" s="T246">loːqɨra</ts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_933" n="HIAT:w" s="T247">poːlʼ</ts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_936" n="HIAT:w" s="T248">moːt</ts>
                  <nts id="Seg_937" n="HIAT:ip">,</nts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_940" n="HIAT:w" s="T249">pičʼit</ts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_943" n="HIAT:w" s="T250">paqɨt</ts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_946" n="HIAT:w" s="T251">piːrɨ</ts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_949" n="HIAT:w" s="T252">taqpa</ts>
                  <nts id="Seg_950" n="HIAT:ip">.</nts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T258" id="Seg_953" n="HIAT:u" s="T253">
                  <ts e="T254" id="Seg_955" n="HIAT:w" s="T253">Teŋtɨna</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_958" n="HIAT:w" s="T254">tüːp</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_961" n="HIAT:w" s="T255">na</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_964" n="HIAT:w" s="T256">qəːlpɨntɔːtɨt</ts>
                  <nts id="Seg_965" n="HIAT:ip">,</nts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_968" n="HIAT:w" s="T257">mɔːttɨ</ts>
                  <nts id="Seg_969" n="HIAT:ip">.</nts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T264" id="Seg_972" n="HIAT:u" s="T258">
                  <ts e="T259" id="Seg_974" n="HIAT:w" s="T258">Poroqsä</ts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_977" n="HIAT:w" s="T259">tomorot</ts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_980" n="HIAT:w" s="T260">qamnɨmpa</ts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_983" n="HIAT:w" s="T261">na</ts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_986" n="HIAT:w" s="T262">nʼuːtɨt</ts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_989" n="HIAT:w" s="T263">ɨːlʼtɨ</ts>
                  <nts id="Seg_990" n="HIAT:ip">.</nts>
                  <nts id="Seg_991" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T267" id="Seg_993" n="HIAT:u" s="T264">
                  <ts e="T265" id="Seg_995" n="HIAT:w" s="T264">Tü</ts>
                  <nts id="Seg_996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_998" n="HIAT:w" s="T265">na</ts>
                  <nts id="Seg_999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1001" n="HIAT:w" s="T266">qättɨmmɨntɨtɨ</ts>
                  <nts id="Seg_1002" n="HIAT:ip">.</nts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T273" id="Seg_1005" n="HIAT:u" s="T267">
                  <ts e="T268" id="Seg_1007" n="HIAT:w" s="T267">Tıntena</ts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1010" n="HIAT:w" s="T268">nɔːssarɨlʼ</ts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1013" n="HIAT:w" s="T269">qumɨlʼ</ts>
                  <nts id="Seg_1014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1016" n="HIAT:w" s="T270">mütɨ</ts>
                  <nts id="Seg_1017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1019" n="HIAT:w" s="T271">tü</ts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1022" n="HIAT:w" s="T272">ampatɨ</ts>
                  <nts id="Seg_1023" n="HIAT:ip">.</nts>
                  <nts id="Seg_1024" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T276" id="Seg_1026" n="HIAT:u" s="T273">
                  <ts e="T274" id="Seg_1028" n="HIAT:w" s="T273">Qonŋɔːtɨt</ts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1031" n="HIAT:w" s="T274">ınnänɨlʼe</ts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1034" n="HIAT:w" s="T275">amnäiːtɨ</ts>
                  <nts id="Seg_1035" n="HIAT:ip">.</nts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T282" id="Seg_1038" n="HIAT:u" s="T276">
                  <ts e="T277" id="Seg_1040" n="HIAT:w" s="T276">Ira</ts>
                  <nts id="Seg_1041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1043" n="HIAT:w" s="T277">ponä</ts>
                  <nts id="Seg_1044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1046" n="HIAT:w" s="T278">paktälʼä</ts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1049" n="HIAT:w" s="T279">pičʼilʼ</ts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1052" n="HIAT:w" s="T280">utät</ts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1055" n="HIAT:w" s="T281">märka</ts>
                  <nts id="Seg_1056" n="HIAT:ip">.</nts>
                  <nts id="Seg_1057" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T289" id="Seg_1059" n="HIAT:u" s="T282">
                  <ts e="T283" id="Seg_1061" n="HIAT:w" s="T282">Isɨ</ts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1064" n="HIAT:w" s="T283">čʼap</ts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1067" n="HIAT:w" s="T284">ponä</ts>
                  <nts id="Seg_1068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1070" n="HIAT:w" s="T285">qontɨšɛlʼčʼɔːtɨt</ts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1073" n="HIAT:w" s="T286">aj</ts>
                  <nts id="Seg_1074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1076" n="HIAT:w" s="T287">čʼar</ts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1079" n="HIAT:w" s="T288">qättɔːtɨt</ts>
                  <nts id="Seg_1080" n="HIAT:ip">.</nts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T293" id="Seg_1083" n="HIAT:u" s="T289">
                  <ts e="T290" id="Seg_1085" n="HIAT:w" s="T289">Amnaiːtɨ</ts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1088" n="HIAT:w" s="T290">tüp</ts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1091" n="HIAT:w" s="T291">na</ts>
                  <nts id="Seg_1092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1094" n="HIAT:w" s="T292">qämmɨntɔːtɨt</ts>
                  <nts id="Seg_1095" n="HIAT:ip">.</nts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T303" id="Seg_1098" n="HIAT:u" s="T293">
                  <ts e="T294" id="Seg_1100" n="HIAT:w" s="T293">Šoŋol</ts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1103" n="HIAT:w" s="T294">pɔːrɨ</ts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1106" n="HIAT:w" s="T295">šuː</ts>
                  <nts id="Seg_1107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1109" n="HIAT:w" s="T296">ıllä</ts>
                  <nts id="Seg_1110" n="HIAT:ip">,</nts>
                  <nts id="Seg_1111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1113" n="HIAT:w" s="T297">aj</ts>
                  <nts id="Seg_1114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1116" n="HIAT:w" s="T298">šitə</ts>
                  <nts id="Seg_1117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1119" n="HIAT:w" s="T299">okoškan</ts>
                  <nts id="Seg_1120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1122" n="HIAT:w" s="T300">ɔːkti</ts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1125" n="HIAT:w" s="T301">mɔːtti</ts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1128" n="HIAT:w" s="T302">qəːŋɔːtɨ</ts>
                  <nts id="Seg_1129" n="HIAT:ip">.</nts>
                  <nts id="Seg_1130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T306" id="Seg_1132" n="HIAT:u" s="T303">
                  <ts e="T304" id="Seg_1134" n="HIAT:w" s="T303">Mɔːtɨp</ts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1137" n="HIAT:w" s="T304">tüsä</ts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1140" n="HIAT:w" s="T305">čʼɔːtäjɔːtɨt</ts>
                  <nts id="Seg_1141" n="HIAT:ip">.</nts>
                  <nts id="Seg_1142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T313" id="Seg_1144" n="HIAT:u" s="T306">
                  <ts e="T307" id="Seg_1146" n="HIAT:w" s="T306">Na</ts>
                  <nts id="Seg_1147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1149" n="HIAT:w" s="T307">qälilʼ</ts>
                  <nts id="Seg_1150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1152" n="HIAT:w" s="T308">mütɨp</ts>
                  <nts id="Seg_1153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1154" n="HIAT:ip">(</nts>
                  <nts id="Seg_1155" n="HIAT:ip">/</nts>
                  <ts e="T310" id="Seg_1157" n="HIAT:w" s="T309">mütɨt</ts>
                  <nts id="Seg_1158" n="HIAT:ip">)</nts>
                  <nts id="Seg_1159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1161" n="HIAT:w" s="T310">olqup</ts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1164" n="HIAT:w" s="T311">šitä</ts>
                  <nts id="Seg_1165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1167" n="HIAT:w" s="T312">ɛːpɨntɔːtɨt</ts>
                  <nts id="Seg_1168" n="HIAT:ip">.</nts>
                  <nts id="Seg_1169" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T326" id="Seg_1171" n="HIAT:u" s="T313">
                  <ts e="T314" id="Seg_1173" n="HIAT:w" s="T313">Nɨnɨ</ts>
                  <nts id="Seg_1174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1176" n="HIAT:w" s="T314">kara</ts>
                  <nts id="Seg_1177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1179" n="HIAT:w" s="T315">qənna</ts>
                  <nts id="Seg_1180" n="HIAT:ip">,</nts>
                  <nts id="Seg_1181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1183" n="HIAT:w" s="T316">qälʼatɨt</ts>
                  <nts id="Seg_1184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1186" n="HIAT:w" s="T317">qaqlante</ts>
                  <nts id="Seg_1187" n="HIAT:ip">,</nts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1190" n="HIAT:w" s="T318">kara</ts>
                  <nts id="Seg_1191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1193" n="HIAT:w" s="T319">qaj</ts>
                  <nts id="Seg_1194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1196" n="HIAT:w" s="T320">qaqlantä</ts>
                  <nts id="Seg_1197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1199" n="HIAT:w" s="T321">qənna</ts>
                  <nts id="Seg_1200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1202" n="HIAT:w" s="T322">aj</ts>
                  <nts id="Seg_1203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1205" n="HIAT:w" s="T323">šittə</ts>
                  <nts id="Seg_1206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1208" n="HIAT:w" s="T324">ännaqıtɨ</ts>
                  <nts id="Seg_1209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1211" n="HIAT:w" s="T325">qənnɨtɨtɨ</ts>
                  <nts id="Seg_1212" n="HIAT:ip">.</nts>
                  <nts id="Seg_1213" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T331" id="Seg_1215" n="HIAT:u" s="T326">
                  <ts e="T327" id="Seg_1217" n="HIAT:w" s="T326">Šite</ts>
                  <nts id="Seg_1218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1220" n="HIAT:w" s="T327">ännaqıtɨ</ts>
                  <nts id="Seg_1221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1223" n="HIAT:w" s="T328">qənnɨtɨtɨ</ts>
                  <nts id="Seg_1224" n="HIAT:ip">,</nts>
                  <nts id="Seg_1225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1227" n="HIAT:w" s="T329">qətqilʼpɨti</ts>
                  <nts id="Seg_1228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1230" n="HIAT:w" s="T330">qumɨiːqɨntɨ</ts>
                  <nts id="Seg_1231" n="HIAT:ip">.</nts>
                  <nts id="Seg_1232" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T336" id="Seg_1234" n="HIAT:u" s="T331">
                  <ts e="T332" id="Seg_1236" n="HIAT:w" s="T331">Karanɨ</ts>
                  <nts id="Seg_1237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1239" n="HIAT:w" s="T332">qonna</ts>
                  <nts id="Seg_1240" n="HIAT:ip">,</nts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1243" n="HIAT:w" s="T333">moːčʼalʼ</ts>
                  <nts id="Seg_1244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1246" n="HIAT:w" s="T334">qälʼi</ts>
                  <nts id="Seg_1247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1249" n="HIAT:w" s="T454">ira</ts>
                  <nts id="Seg_1250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1252" n="HIAT:w" s="T335">qontɨssɛːnta</ts>
                  <nts id="Seg_1253" n="HIAT:ip">.</nts>
                  <nts id="Seg_1254" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T342" id="Seg_1256" n="HIAT:u" s="T336">
                  <ts e="T337" id="Seg_1258" n="HIAT:w" s="T336">Qaj</ts>
                  <nts id="Seg_1259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1261" n="HIAT:w" s="T337">nʼomalʼ</ts>
                  <nts id="Seg_1262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1264" n="HIAT:w" s="T338">porqaj</ts>
                  <nts id="Seg_1265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1267" n="HIAT:w" s="T339">irat</ts>
                  <nts id="Seg_1268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1270" n="HIAT:w" s="T340">imap</ts>
                  <nts id="Seg_1271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1273" n="HIAT:w" s="T341">qəːčʼisɨlɨt</ts>
                  <nts id="Seg_1274" n="HIAT:ip">.</nts>
                  <nts id="Seg_1275" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T345" id="Seg_1277" n="HIAT:u" s="T342">
                  <ts e="T343" id="Seg_1279" n="HIAT:w" s="T342">Ira</ts>
                  <nts id="Seg_1280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1282" n="HIAT:w" s="T343">čʼarrɨ</ts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1285" n="HIAT:w" s="T344">qättɨŋɨt</ts>
                  <nts id="Seg_1286" n="HIAT:ip">.</nts>
                  <nts id="Seg_1287" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T348" id="Seg_1289" n="HIAT:u" s="T345">
                  <ts e="T346" id="Seg_1291" n="HIAT:w" s="T345">Nɨːna</ts>
                  <nts id="Seg_1292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1294" n="HIAT:w" s="T346">kara</ts>
                  <nts id="Seg_1295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1297" n="HIAT:w" s="T347">kurolʼna</ts>
                  <nts id="Seg_1298" n="HIAT:ip">.</nts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T358" id="Seg_1301" n="HIAT:u" s="T348">
                  <ts e="T349" id="Seg_1303" n="HIAT:w" s="T348">Nɔːkɨr</ts>
                  <nts id="Seg_1304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1306" n="HIAT:w" s="T349">ämnasit</ts>
                  <nts id="Seg_1307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1309" n="HIAT:w" s="T350">takɨ</ts>
                  <nts id="Seg_1310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1312" n="HIAT:w" s="T351">qännajɔːt</ts>
                  <nts id="Seg_1313" n="HIAT:ip">,</nts>
                  <nts id="Seg_1314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1316" n="HIAT:w" s="T352">näčʼä</ts>
                  <nts id="Seg_1317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1319" n="HIAT:w" s="T353">laqaltɛlʼčʼɔːtɨ</ts>
                  <nts id="Seg_1320" n="HIAT:ip">,</nts>
                  <nts id="Seg_1321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1323" n="HIAT:w" s="T354">qalitɨt</ts>
                  <nts id="Seg_1324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1326" n="HIAT:w" s="T355">mɔːttɨ</ts>
                  <nts id="Seg_1327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1329" n="HIAT:w" s="T356">takkɨ</ts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1332" n="HIAT:w" s="T357">qənnɔːtɨt</ts>
                  <nts id="Seg_1333" n="HIAT:ip">.</nts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T363" id="Seg_1336" n="HIAT:u" s="T358">
                  <ts e="T359" id="Seg_1338" n="HIAT:w" s="T358">Qälitɨt</ts>
                  <nts id="Seg_1339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1341" n="HIAT:w" s="T359">mɔːttɨ</ts>
                  <nts id="Seg_1342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1344" n="HIAT:w" s="T360">imatɨt</ts>
                  <nts id="Seg_1345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1347" n="HIAT:w" s="T361">ılla</ts>
                  <nts id="Seg_1348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1350" n="HIAT:w" s="T362">qənnɔːtɨt</ts>
                  <nts id="Seg_1351" n="HIAT:ip">.</nts>
                  <nts id="Seg_1352" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T369" id="Seg_1354" n="HIAT:u" s="T363">
                  <ts e="T364" id="Seg_1356" n="HIAT:w" s="T363">Nɨmtɨ</ts>
                  <nts id="Seg_1357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1359" n="HIAT:w" s="T364">mɔːtqɨt</ts>
                  <nts id="Seg_1360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1362" n="HIAT:w" s="T365">šolʼqumɨlʼ</ts>
                  <nts id="Seg_1363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1365" n="HIAT:w" s="T366">šitɨ</ts>
                  <nts id="Seg_1366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1368" n="HIAT:w" s="T367">ijaqıp</ts>
                  <nts id="Seg_1369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1371" n="HIAT:w" s="T368">qəːčʼimpɔːtɨt</ts>
                  <nts id="Seg_1372" n="HIAT:ip">.</nts>
                  <nts id="Seg_1373" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T374" id="Seg_1375" n="HIAT:u" s="T369">
                  <ts e="T370" id="Seg_1377" n="HIAT:w" s="T369">Tomorot</ts>
                  <nts id="Seg_1378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1380" n="HIAT:w" s="T370">nʼomalʼ</ts>
                  <nts id="Seg_1381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1383" n="HIAT:w" s="T371">porqaj</ts>
                  <nts id="Seg_1384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1386" n="HIAT:w" s="T372">irat</ts>
                  <nts id="Seg_1387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1389" n="HIAT:w" s="T373">kətsatqı</ts>
                  <nts id="Seg_1390" n="HIAT:ip">.</nts>
                  <nts id="Seg_1391" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T380" id="Seg_1393" n="HIAT:u" s="T374">
                  <ts e="T375" id="Seg_1395" n="HIAT:w" s="T374">Namɨn</ts>
                  <nts id="Seg_1396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1398" n="HIAT:w" s="T375">ijaqı</ts>
                  <nts id="Seg_1399" n="HIAT:ip">,</nts>
                  <nts id="Seg_1400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1402" n="HIAT:w" s="T376">ɔːtap</ts>
                  <nts id="Seg_1403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1405" n="HIAT:w" s="T377">tɔːqqolʼä</ts>
                  <nts id="Seg_1406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1408" n="HIAT:w" s="T378">konnä</ts>
                  <nts id="Seg_1409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1411" n="HIAT:w" s="T379">qənnɔːtɨt</ts>
                  <nts id="Seg_1412" n="HIAT:ip">.</nts>
                  <nts id="Seg_1413" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T382" id="Seg_1415" n="HIAT:u" s="T380">
                  <ts e="T381" id="Seg_1417" n="HIAT:w" s="T380">Konna</ts>
                  <nts id="Seg_1418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1420" n="HIAT:w" s="T381">mɔːtɨlnɔːtɨt</ts>
                  <nts id="Seg_1421" n="HIAT:ip">.</nts>
                  <nts id="Seg_1422" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T385" id="Seg_1424" n="HIAT:u" s="T382">
                  <ts e="T383" id="Seg_1426" n="HIAT:w" s="T382">Ukkur</ts>
                  <nts id="Seg_1427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1429" n="HIAT:w" s="T383">pit</ts>
                  <nts id="Seg_1430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1432" n="HIAT:w" s="T384">šäqɔːtɨt</ts>
                  <nts id="Seg_1433" n="HIAT:ip">.</nts>
                  <nts id="Seg_1434" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T390" id="Seg_1436" n="HIAT:u" s="T385">
                  <ts e="T386" id="Seg_1438" n="HIAT:w" s="T385">Ijaiːtɨ</ts>
                  <nts id="Seg_1439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1441" n="HIAT:w" s="T386">täpa</ts>
                  <nts id="Seg_1442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1444" n="HIAT:w" s="T387">čʼeːlʼ</ts>
                  <nts id="Seg_1445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1447" n="HIAT:w" s="T388">moqonä</ts>
                  <nts id="Seg_1448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1450" n="HIAT:w" s="T389">tüŋɔːtät</ts>
                  <nts id="Seg_1451" n="HIAT:ip">.</nts>
                  <nts id="Seg_1452" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T395" id="Seg_1454" n="HIAT:u" s="T390">
                  <ts e="T391" id="Seg_1456" n="HIAT:w" s="T390">Əsɨ</ts>
                  <nts id="Seg_1457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1459" n="HIAT:w" s="T391">iramtɨ</ts>
                  <nts id="Seg_1460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1462" n="HIAT:w" s="T392">qəntalʼa</ts>
                  <nts id="Seg_1463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1465" n="HIAT:w" s="T393">tüŋɔːtät</ts>
                  <nts id="Seg_1466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1468" n="HIAT:w" s="T394">ijaiːtɨ</ts>
                  <nts id="Seg_1469" n="HIAT:ip">.</nts>
                  <nts id="Seg_1470" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T401" id="Seg_1472" n="HIAT:u" s="T395">
                  <ts e="T396" id="Seg_1474" n="HIAT:w" s="T395">Täpɨt</ts>
                  <nts id="Seg_1475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1477" n="HIAT:w" s="T396">nılʼčʼik</ts>
                  <nts id="Seg_1478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1480" n="HIAT:w" s="T397">tɛnirpɔːtät</ts>
                  <nts id="Seg_1481" n="HIAT:ip">:</nts>
                  <nts id="Seg_1482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1484" n="HIAT:w" s="T398">Mat</ts>
                  <nts id="Seg_1485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1487" n="HIAT:w" s="T399">äsamɨt</ts>
                  <nts id="Seg_1488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1490" n="HIAT:w" s="T400">qətpɔːtɨt</ts>
                  <nts id="Seg_1491" n="HIAT:ip">.</nts>
                  <nts id="Seg_1492" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T407" id="Seg_1494" n="HIAT:u" s="T401">
                  <ts e="T402" id="Seg_1496" n="HIAT:w" s="T401">Təp</ts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1499" n="HIAT:w" s="T402">seːlʼčʼi</ts>
                  <nts id="Seg_1500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1502" n="HIAT:w" s="T403">iːjatɨ</ts>
                  <nts id="Seg_1503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1505" n="HIAT:w" s="T404">ɛːppɨntɨ</ts>
                  <nts id="Seg_1506" n="HIAT:ip">,</nts>
                  <nts id="Seg_1507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1509" n="HIAT:w" s="T405">kɨpa</ts>
                  <nts id="Seg_1510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1512" n="HIAT:w" s="T406">ämnätɨ</ts>
                  <nts id="Seg_1513" n="HIAT:ip">.</nts>
                  <nts id="Seg_1514" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T410" id="Seg_1516" n="HIAT:u" s="T407">
                  <ts e="T408" id="Seg_1518" n="HIAT:w" s="T407">Täpɨt</ts>
                  <nts id="Seg_1519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1521" n="HIAT:w" s="T408">qäntalʼa</ts>
                  <nts id="Seg_1522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1524" n="HIAT:w" s="T409">taːtɔːtɨt</ts>
                  <nts id="Seg_1525" n="HIAT:ip">.</nts>
                  <nts id="Seg_1526" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T414" id="Seg_1528" n="HIAT:u" s="T410">
                  <ts e="T411" id="Seg_1530" n="HIAT:w" s="T410">Iːjantɨ</ts>
                  <nts id="Seg_1531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1533" n="HIAT:w" s="T411">kanap</ts>
                  <nts id="Seg_1534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1536" n="HIAT:w" s="T412">poːqot</ts>
                  <nts id="Seg_1537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1539" n="HIAT:w" s="T413">sɔːrɨmpɔːtɨn</ts>
                  <nts id="Seg_1540" n="HIAT:ip">.</nts>
                  <nts id="Seg_1541" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T420" id="Seg_1543" n="HIAT:u" s="T414">
                  <ts e="T415" id="Seg_1545" n="HIAT:w" s="T414">Ɔːtan</ts>
                  <nts id="Seg_1546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1548" n="HIAT:w" s="T415">ätɨmaptaqɨt</ts>
                  <nts id="Seg_1549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1551" n="HIAT:w" s="T416">täpɨt</ts>
                  <nts id="Seg_1552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1554" n="HIAT:w" s="T417">kanan</ts>
                  <nts id="Seg_1555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1557" n="HIAT:w" s="T418">topɨp</ts>
                  <nts id="Seg_1558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1560" n="HIAT:w" s="T419">čʼıqɨlʼnɔːtɨn</ts>
                  <nts id="Seg_1561" n="HIAT:ip">.</nts>
                  <nts id="Seg_1562" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T427" id="Seg_1564" n="HIAT:u" s="T420">
                  <ts e="T421" id="Seg_1566" n="HIAT:w" s="T420">Ira</ts>
                  <nts id="Seg_1567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1569" n="HIAT:w" s="T421">nılʼčʼik</ts>
                  <nts id="Seg_1570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1572" n="HIAT:w" s="T422">kətɨŋɨtɨ</ts>
                  <nts id="Seg_1573" n="HIAT:ip">:</nts>
                  <nts id="Seg_1574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1576" n="HIAT:w" s="T423">Kanat</ts>
                  <nts id="Seg_1577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1579" n="HIAT:w" s="T424">topɨp</ts>
                  <nts id="Seg_1580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1582" n="HIAT:w" s="T425">qaj</ts>
                  <nts id="Seg_1583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1585" n="HIAT:w" s="T426">čʼıqɨlnɨtɨ</ts>
                  <nts id="Seg_1586" n="HIAT:ip">?</nts>
                  <nts id="Seg_1587" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T432" id="Seg_1589" n="HIAT:u" s="T427">
                  <ts e="T428" id="Seg_1591" n="HIAT:w" s="T427">Kɨpa</ts>
                  <nts id="Seg_1592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1594" n="HIAT:w" s="T428">ijantɨ</ts>
                  <nts id="Seg_1595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1597" n="HIAT:w" s="T429">ima</ts>
                  <nts id="Seg_1598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1600" n="HIAT:w" s="T430">pona</ts>
                  <nts id="Seg_1601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1603" n="HIAT:w" s="T431">pakta</ts>
                  <nts id="Seg_1604" n="HIAT:ip">.</nts>
                  <nts id="Seg_1605" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T436" id="Seg_1607" n="HIAT:u" s="T432">
                  <ts e="T433" id="Seg_1609" n="HIAT:w" s="T432">Naqaj</ts>
                  <nts id="Seg_1610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1612" n="HIAT:w" s="T433">ontä</ts>
                  <nts id="Seg_1613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1615" n="HIAT:w" s="T434">iratɨ</ts>
                  <nts id="Seg_1616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1618" n="HIAT:w" s="T435">tüntɨ</ts>
                  <nts id="Seg_1619" n="HIAT:ip">.</nts>
                  <nts id="Seg_1620" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T440" id="Seg_1622" n="HIAT:u" s="T436">
                  <ts e="T437" id="Seg_1624" n="HIAT:w" s="T436">Naqaj</ts>
                  <nts id="Seg_1625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1627" n="HIAT:w" s="T437">ontɨ</ts>
                  <nts id="Seg_1628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1630" n="HIAT:w" s="T438">iːjatɨ</ts>
                  <nts id="Seg_1631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1633" n="HIAT:w" s="T439">tümpa</ts>
                  <nts id="Seg_1634" n="HIAT:ip">.</nts>
                  <nts id="Seg_1635" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T448" id="Seg_1637" n="HIAT:u" s="T440">
                  <ts e="T441" id="Seg_1639" n="HIAT:w" s="T440">Nɨːnɨ</ts>
                  <nts id="Seg_1640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1642" n="HIAT:w" s="T441">ilʼčʼatɨt</ts>
                  <nts id="Seg_1643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1645" n="HIAT:w" s="T442">nılʼčʼik</ts>
                  <nts id="Seg_1646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1648" n="HIAT:w" s="T443">kəttɨmpatɨ</ts>
                  <nts id="Seg_1649" n="HIAT:ip">:</nts>
                  <nts id="Seg_1650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1652" n="HIAT:w" s="T444">Montɨ</ts>
                  <nts id="Seg_1653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1655" n="HIAT:w" s="T445">onɨt</ts>
                  <nts id="Seg_1656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1658" n="HIAT:w" s="T446">tüsa</ts>
                  <nts id="Seg_1659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1661" n="HIAT:w" s="T447">čʼɔːtɨsɨmɨt</ts>
                  <nts id="Seg_1662" n="HIAT:ip">.</nts>
                  <nts id="Seg_1663" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T453" id="Seg_1665" n="HIAT:u" s="T448">
                  <ts e="T449" id="Seg_1667" n="HIAT:w" s="T448">Qälʼij</ts>
                  <nts id="Seg_1668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1670" n="HIAT:w" s="T449">təttan</ts>
                  <nts id="Seg_1671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1673" n="HIAT:w" s="T450">ɔːtap</ts>
                  <nts id="Seg_1674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1676" n="HIAT:w" s="T451">nʼenna</ts>
                  <nts id="Seg_1677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1679" n="HIAT:w" s="T452">tɔːqɨŋɨtɨ</ts>
                  <nts id="Seg_1680" n="HIAT:ip">.</nts>
                  <nts id="Seg_1681" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T453" id="Seg_1682" n="sc" s="T0">
               <ts e="T1" id="Seg_1684" n="e" s="T0">Nʼomalʼ </ts>
               <ts e="T2" id="Seg_1686" n="e" s="T1">porqɨ </ts>
               <ts e="T3" id="Seg_1688" n="e" s="T2">ira. </ts>
               <ts e="T4" id="Seg_1690" n="e" s="T3">Seːlʼčʼi </ts>
               <ts e="T5" id="Seg_1692" n="e" s="T4">ijatɨ </ts>
               <ts e="T6" id="Seg_1694" n="e" s="T5">ɛːppäntɨ, </ts>
               <ts e="T7" id="Seg_1696" n="e" s="T6">aj </ts>
               <ts e="T8" id="Seg_1698" n="e" s="T7">seːlʼčʼi </ts>
               <ts e="T9" id="Seg_1700" n="e" s="T8">amnätɨ. </ts>
               <ts e="T10" id="Seg_1702" n="e" s="T9">Qaːlit </ts>
               <ts e="T11" id="Seg_1704" n="e" s="T10">tüŋɔːtɨt </ts>
               <ts e="T12" id="Seg_1706" n="e" s="T11">takkɨnɨ </ts>
               <ts e="T13" id="Seg_1708" n="e" s="T12">(/takkɨntɨ) </ts>
               <ts e="T14" id="Seg_1710" n="e" s="T13">nʼannä </ts>
               <ts e="T15" id="Seg_1712" n="e" s="T14">täpɨp </ts>
               <ts e="T16" id="Seg_1714" n="e" s="T15">qətqɨntɨtqo. </ts>
               <ts e="T17" id="Seg_1716" n="e" s="T16">Qumijtɨ </ts>
               <ts e="T18" id="Seg_1718" n="e" s="T17">ijaiːtɨ </ts>
               <ts e="T19" id="Seg_1720" n="e" s="T18">mačʼontɨ </ts>
               <ts e="T20" id="Seg_1722" n="e" s="T19">qənpɔːtɨt </ts>
               <ts e="T21" id="Seg_1724" n="e" s="T20">täpälʼlʼä. </ts>
               <ts e="T22" id="Seg_1726" n="e" s="T21">Qup </ts>
               <ts e="T23" id="Seg_1728" n="e" s="T22">tiː </ts>
               <ts e="T24" id="Seg_1730" n="e" s="T23">qəttiŋɨt. </ts>
               <ts e="T25" id="Seg_1732" n="e" s="T24">Kuttar </ts>
               <ts e="T26" id="Seg_1734" n="e" s="T25">ɛːŋa? </ts>
               <ts e="T27" id="Seg_1736" n="e" s="T26">Köt </ts>
               <ts e="T28" id="Seg_1738" n="e" s="T27">šolʼlʼiqumɨlʼ </ts>
               <ts e="T29" id="Seg_1740" n="e" s="T28">ijakɨp </ts>
               <ts e="T30" id="Seg_1742" n="e" s="T29">märɨmpɔːtɨt. </ts>
               <ts e="T31" id="Seg_1744" n="e" s="T30">Na </ts>
               <ts e="T32" id="Seg_1746" n="e" s="T31">qälit </ts>
               <ts e="T33" id="Seg_1748" n="e" s="T32">namɨp </ts>
               <ts e="T34" id="Seg_1750" n="e" s="T33">üːtɨntɨt. </ts>
               <ts e="T35" id="Seg_1752" n="e" s="T34">Nʼomalʼ </ts>
               <ts e="T36" id="Seg_1754" n="e" s="T35">porqɨ </ts>
               <ts e="T37" id="Seg_1756" n="e" s="T36">iratkine </ts>
               <ts e="T38" id="Seg_1758" n="e" s="T37">meː </ts>
               <ts e="T39" id="Seg_1760" n="e" s="T38">täːlɨ </ts>
               <ts e="T40" id="Seg_1762" n="e" s="T39">mɨta </ts>
               <ts e="T41" id="Seg_1764" n="e" s="T40">tüntɔːmɨt. </ts>
               <ts e="T42" id="Seg_1766" n="e" s="T41">Qonninɨlɨt </ts>
               <ts e="T43" id="Seg_1768" n="e" s="T42">(/qonninɨt) </ts>
               <ts e="T44" id="Seg_1770" n="e" s="T43">əːtɨtqo. </ts>
               <ts e="T45" id="Seg_1772" n="e" s="T44">Ijaqı </ts>
               <ts e="T46" id="Seg_1774" n="e" s="T45">moqonä </ts>
               <ts e="T47" id="Seg_1776" n="e" s="T46">qəlʼlʼä </ts>
               <ts e="T48" id="Seg_1778" n="e" s="T47">nık </ts>
               <ts e="T49" id="Seg_1780" n="e" s="T48">kətɨŋɨt: </ts>
               <ts e="T50" id="Seg_1782" n="e" s="T49">Čʼilʼälʼ </ts>
               <ts e="T51" id="Seg_1784" n="e" s="T50">ijaqıj </ts>
               <ts e="T52" id="Seg_1786" n="e" s="T51">qälɨqqı </ts>
               <ts e="T53" id="Seg_1788" n="e" s="T52">mɨrɨŋɨt. </ts>
               <ts e="T54" id="Seg_1790" n="e" s="T53">Nɨːnɨ </ts>
               <ts e="T55" id="Seg_1792" n="e" s="T54">šittelʼ </ts>
               <ts e="T56" id="Seg_1794" n="e" s="T55">irra </ts>
               <ts e="T57" id="Seg_1796" n="e" s="T56">kučʼe </ts>
               <ts e="T58" id="Seg_1798" n="e" s="T57">qatɛnta? </ts>
               <ts e="T59" id="Seg_1800" n="e" s="T58">Ira </ts>
               <ts e="T60" id="Seg_1802" n="e" s="T59">nılʼčʼik </ts>
               <ts e="T61" id="Seg_1804" n="e" s="T60">ɛssa, </ts>
               <ts e="T62" id="Seg_1806" n="e" s="T61">na </ts>
               <ts e="T63" id="Seg_1808" n="e" s="T62">nʼomalʼ </ts>
               <ts e="T64" id="Seg_1810" n="e" s="T63">porqə </ts>
               <ts e="T65" id="Seg_1812" n="e" s="T64">ira </ts>
               <ts e="T66" id="Seg_1814" n="e" s="T65">amnäntɨlʼ </ts>
               <ts e="T67" id="Seg_1816" n="e" s="T66">mɨtkine: </ts>
               <ts e="T68" id="Seg_1818" n="e" s="T67">Ür, </ts>
               <ts e="T69" id="Seg_1820" n="e" s="T68">apsɨ </ts>
               <ts e="T70" id="Seg_1822" n="e" s="T69">tattɨŋɨt. </ts>
               <ts e="T71" id="Seg_1824" n="e" s="T70">Kun </ts>
               <ts e="T72" id="Seg_1826" n="e" s="T71">montɨlʼ </ts>
               <ts e="T73" id="Seg_1828" n="e" s="T72">qaj </ts>
               <ts e="T74" id="Seg_1830" n="e" s="T73">tattɨŋɨt. </ts>
               <ts e="T75" id="Seg_1832" n="e" s="T74">Qälit </ts>
               <ts e="T76" id="Seg_1834" n="e" s="T75">täːlʼe </ts>
               <ts e="T77" id="Seg_1836" n="e" s="T76">tüntɔːtɨt, </ts>
               <ts e="T78" id="Seg_1838" n="e" s="T77">amɨrtɛntɔːtɨt. </ts>
               <ts e="T79" id="Seg_1840" n="e" s="T78">Ira </ts>
               <ts e="T80" id="Seg_1842" n="e" s="T79">nık </ts>
               <ts e="T81" id="Seg_1844" n="e" s="T80">kətɨt. </ts>
               <ts e="T82" id="Seg_1846" n="e" s="T81">Apsɨp </ts>
               <ts e="T83" id="Seg_1848" n="e" s="T82">tattɔːtɨt. </ts>
               <ts e="T84" id="Seg_1850" n="e" s="T83">Tam </ts>
               <ts e="T85" id="Seg_1852" n="e" s="T84">mɔːtɨt </ts>
               <ts e="T86" id="Seg_1854" n="e" s="T85">šünʼčʼip </ts>
               <ts e="T87" id="Seg_1856" n="e" s="T86">šentɨ </ts>
               <ts e="T88" id="Seg_1858" n="e" s="T87">nʼuːtɨsä </ts>
               <ts e="T89" id="Seg_1860" n="e" s="T88">tɔːqqɔːtɨt. </ts>
               <ts e="T90" id="Seg_1862" n="e" s="T89">Ira </ts>
               <ts e="T91" id="Seg_1864" n="e" s="T90">qälimtɨ </ts>
               <ts e="T92" id="Seg_1866" n="e" s="T91">apsɨtɨqolamnɨt. </ts>
               <ts e="T93" id="Seg_1868" n="e" s="T92">Əː! </ts>
               <ts e="T94" id="Seg_1870" n="e" s="T93">Tɔːptɨlʼ </ts>
               <ts e="T95" id="Seg_1872" n="e" s="T94">čʼeːlʼɨ </ts>
               <ts e="T96" id="Seg_1874" n="e" s="T95">qälɨt </ts>
               <ts e="T97" id="Seg_1876" n="e" s="T96">na </ts>
               <ts e="T98" id="Seg_1878" n="e" s="T97">jep </ts>
               <ts e="T99" id="Seg_1880" n="e" s="T98">tatɨntɔːtɨt. </ts>
               <ts e="T100" id="Seg_1882" n="e" s="T99">Nɔːssarɨlʼ </ts>
               <ts e="T101" id="Seg_1884" n="e" s="T100">qumɨlʼ </ts>
               <ts e="T102" id="Seg_1886" n="e" s="T101">mütɨ. </ts>
               <ts e="T103" id="Seg_1888" n="e" s="T102">Qapija </ts>
               <ts e="T104" id="Seg_1890" n="e" s="T103">šünʼčʼipɨlʼ </ts>
               <ts e="T105" id="Seg_1892" n="e" s="T104">mɔːtqɨt </ts>
               <ts e="T106" id="Seg_1894" n="e" s="T105">nʼomalʼ </ts>
               <ts e="T107" id="Seg_1896" n="e" s="T106">porqɨ </ts>
               <ts e="T108" id="Seg_1898" n="e" s="T107">ira </ts>
               <ts e="T109" id="Seg_1900" n="e" s="T108">ontɨqɨt </ts>
               <ts e="T110" id="Seg_1902" n="e" s="T109">amnäiːntɨsä. </ts>
               <ts e="T111" id="Seg_1904" n="e" s="T110">Šentɨ </ts>
               <ts e="T112" id="Seg_1906" n="e" s="T111">nʼuːtɨsa </ts>
               <ts e="T113" id="Seg_1908" n="e" s="T112">tɔːqɨralʼtɨt </ts>
               <ts e="T114" id="Seg_1910" n="e" s="T113">(/tɔːqɨralʼimpɔːtɨt). </ts>
               <ts e="T115" id="Seg_1912" n="e" s="T114">Ira </ts>
               <ts e="T116" id="Seg_1914" n="e" s="T115">nılʼčʼik </ts>
               <ts e="T117" id="Seg_1916" n="e" s="T116">ɛsa. </ts>
               <ts e="T118" id="Seg_1918" n="e" s="T117">Amɨrqolamnɔːtɨt. </ts>
               <ts e="T119" id="Seg_1920" n="e" s="T118">Apsɨp </ts>
               <ts e="T120" id="Seg_1922" n="e" s="T119">qontɔːtɨt. </ts>
               <ts e="T121" id="Seg_1924" n="e" s="T120">Ira </ts>
               <ts e="T122" id="Seg_1926" n="e" s="T121">nık </ts>
               <ts e="T123" id="Seg_1928" n="e" s="T122">kəttɨŋɨt. </ts>
               <ts e="T124" id="Seg_1930" n="e" s="T123">Imaqotatɨ </ts>
               <ts e="T125" id="Seg_1932" n="e" s="T124">aj </ts>
               <ts e="T126" id="Seg_1934" n="e" s="T125">ɛːŋa. </ts>
               <ts e="T127" id="Seg_1936" n="e" s="T126">Amnäiːmtɨ </ts>
               <ts e="T128" id="Seg_1938" n="e" s="T127">nık </ts>
               <ts e="T129" id="Seg_1940" n="e" s="T128">kuraltɨt. </ts>
               <ts e="T130" id="Seg_1942" n="e" s="T129">Ponä </ts>
               <ts e="T131" id="Seg_1944" n="e" s="T130">tantɨŋɨlɨt. </ts>
               <ts e="T132" id="Seg_1946" n="e" s="T131">Ijatɨp </ts>
               <ts e="T133" id="Seg_1948" n="e" s="T132">pona </ts>
               <ts e="T134" id="Seg_1950" n="e" s="T133">tattɨŋɨlɨt. </ts>
               <ts e="T135" id="Seg_1952" n="e" s="T134">Ira </ts>
               <ts e="T136" id="Seg_1954" n="e" s="T135">ontɨ </ts>
               <ts e="T137" id="Seg_1956" n="e" s="T136">wärəkka </ts>
               <ts e="T138" id="Seg_1958" n="e" s="T137">qumimtɨ </ts>
               <ts e="T139" id="Seg_1960" n="e" s="T138">apsɨtɨmpɨlʼä. </ts>
               <ts e="T140" id="Seg_1962" n="e" s="T139">Ukkur </ts>
               <ts e="T141" id="Seg_1964" n="e" s="T140">tət </ts>
               <ts e="T142" id="Seg_1966" n="e" s="T141">čʼontoːqɨt </ts>
               <ts e="T143" id="Seg_1968" n="e" s="T142">tüntə </ts>
               <ts e="T144" id="Seg_1970" n="e" s="T143">qaret </ts>
               <ts e="T145" id="Seg_1972" n="e" s="T144">qup. </ts>
               <ts e="T146" id="Seg_1974" n="e" s="T145">Amɨrnɔːt. </ts>
               <ts e="T147" id="Seg_1976" n="e" s="T146">Šoŋolʼilʼ </ts>
               <ts e="T148" id="Seg_1978" n="e" s="T147">(/šoŋolʼ) </ts>
               <ts e="T149" id="Seg_1980" n="e" s="T148">tümtɨ </ts>
               <ts e="T150" id="Seg_1982" n="e" s="T149">nam </ts>
               <ts e="T151" id="Seg_1984" n="e" s="T150">märɨq </ts>
               <ts e="T152" id="Seg_1986" n="e" s="T151">poːsa </ts>
               <ts e="T153" id="Seg_1988" n="e" s="T152">səlɨŋɨt. </ts>
               <ts e="T154" id="Seg_1990" n="e" s="T153">Čʼɔːpɨnta. </ts>
               <ts e="T155" id="Seg_1992" n="e" s="T154">Amnäiːmtɨ </ts>
               <ts e="T156" id="Seg_1994" n="e" s="T155">aša </ts>
               <ts e="T157" id="Seg_1996" n="e" s="T156">qaː </ts>
               <ts e="T158" id="Seg_1998" n="e" s="T157">(/qajqo) </ts>
               <ts e="T159" id="Seg_2000" n="e" s="T158">pona </ts>
               <ts e="T160" id="Seg_2002" n="e" s="T159">üːteŋɨt. </ts>
               <ts e="T161" id="Seg_2004" n="e" s="T160">Poːqɨt </ts>
               <ts e="T162" id="Seg_2006" n="e" s="T161">tü </ts>
               <ts e="T163" id="Seg_2008" n="e" s="T162">čʼɔːtentɔːtet. </ts>
               <ts e="T164" id="Seg_2010" n="e" s="T163">Tüp </ts>
               <ts e="T165" id="Seg_2012" n="e" s="T164">čʼɔːtälpɔːtet, </ts>
               <ts e="T166" id="Seg_2014" n="e" s="T165">tintena </ts>
               <ts e="T167" id="Seg_2016" n="e" s="T166">töp </ts>
               <ts e="T168" id="Seg_2018" n="e" s="T167">purɨšiŋɔːlpɔːtɨt. </ts>
               <ts e="T169" id="Seg_2020" n="e" s="T168">Nılʼčʼik </ts>
               <ts e="T170" id="Seg_2022" n="e" s="T169">qaj </ts>
               <ts e="T171" id="Seg_2024" n="e" s="T170">təːkɨrɨmpɔːtɨt. </ts>
               <ts e="T172" id="Seg_2026" n="e" s="T171">Tɛː </ts>
               <ts e="T173" id="Seg_2028" n="e" s="T172">na </ts>
               <ts e="T174" id="Seg_2030" n="e" s="T173">ɛːntɨlʼ </ts>
               <ts e="T175" id="Seg_2032" n="e" s="T174">man </ts>
               <ts e="T176" id="Seg_2034" n="e" s="T175">na </ts>
               <ts e="T177" id="Seg_2036" n="e" s="T176">tannɛntak. </ts>
               <ts e="T178" id="Seg_2038" n="e" s="T177">Amɨrnɔːtɨt. </ts>
               <ts e="T179" id="Seg_2040" n="e" s="T178">Qälit </ts>
               <ts e="T180" id="Seg_2042" n="e" s="T179">nık </ts>
               <ts e="T181" id="Seg_2044" n="e" s="T180">kətɔːtät: </ts>
               <ts e="T182" id="Seg_2046" n="e" s="T181">Ilʼčʼa, </ts>
               <ts e="T183" id="Seg_2048" n="e" s="T182">šoŋolʼaj </ts>
               <ts e="T184" id="Seg_2050" n="e" s="T183">qajetqe </ts>
               <ts e="T185" id="Seg_2052" n="e" s="T184">našak </ts>
               <ts e="T186" id="Seg_2054" n="e" s="T185">poːsä </ts>
               <ts e="T187" id="Seg_2056" n="e" s="T186">säːlal. </ts>
               <ts e="T188" id="Seg_2058" n="e" s="T187">Aša </ts>
               <ts e="T189" id="Seg_2060" n="e" s="T188">šoŋol </ts>
               <ts e="T190" id="Seg_2062" n="e" s="T189">mɨ </ts>
               <ts e="T191" id="Seg_2064" n="e" s="T190">tü </ts>
               <ts e="T192" id="Seg_2066" n="e" s="T191">na </ts>
               <ts e="T193" id="Seg_2068" n="e" s="T192">amtɨt? </ts>
               <ts e="T194" id="Seg_2070" n="e" s="T193">Täm </ts>
               <ts e="T195" id="Seg_2072" n="e" s="T194">aš </ts>
               <ts e="T196" id="Seg_2074" n="e" s="T195">amtɨt. </ts>
               <ts e="T197" id="Seg_2076" n="e" s="T196">Nık </ts>
               <ts e="T198" id="Seg_2078" n="e" s="T197">märkeptɨmpa </ts>
               <ts e="T199" id="Seg_2080" n="e" s="T198">nʼomalʼ </ts>
               <ts e="T200" id="Seg_2082" n="e" s="T199">porqɨntɨ </ts>
               <ts e="T201" id="Seg_2084" n="e" s="T200">rašalʼ </ts>
               <ts e="T202" id="Seg_2086" n="e" s="T201">lʼıːpɨ. </ts>
               <ts e="T203" id="Seg_2088" n="e" s="T202">Nammɨ </ts>
               <ts e="T204" id="Seg_2090" n="e" s="T203">šeːrpɨntɨt </ts>
               <ts e="T205" id="Seg_2092" n="e" s="T204">šoŋoltɨ </ts>
               <ts e="T206" id="Seg_2094" n="e" s="T205">qanɨqqɨt </ts>
               <ts e="T207" id="Seg_2096" n="e" s="T206">wärqa </ts>
               <ts e="T208" id="Seg_2098" n="e" s="T207">mɔːtantɨ </ts>
               <ts e="T209" id="Seg_2100" n="e" s="T208">qanɨqqɨt. </ts>
               <ts e="T210" id="Seg_2102" n="e" s="T209">Ukkur </ts>
               <ts e="T211" id="Seg_2104" n="e" s="T210">tət </ts>
               <ts e="T212" id="Seg_2106" n="e" s="T211">čʼontoːqɨt </ts>
               <ts e="T213" id="Seg_2108" n="e" s="T212">poːqɨnä </ts>
               <ts e="T214" id="Seg_2110" n="e" s="T213">mɔːttɨ </ts>
               <ts e="T215" id="Seg_2112" n="e" s="T214">tülʼ </ts>
               <ts e="T216" id="Seg_2114" n="e" s="T215">purɨšilʼ </ts>
               <ts e="T217" id="Seg_2116" n="e" s="T216">laka </ts>
               <ts e="T218" id="Seg_2118" n="e" s="T217">alʼčʼinta. </ts>
               <ts e="T219" id="Seg_2120" n="e" s="T218">No </ts>
               <ts e="T220" id="Seg_2122" n="e" s="T219">paqtɨmmɨnta. </ts>
               <ts e="T221" id="Seg_2124" n="e" s="T220">Mɔːtantɨ </ts>
               <ts e="T222" id="Seg_2126" n="e" s="T221">qälɨt </ts>
               <ts e="T223" id="Seg_2128" n="e" s="T222">čʼap </ts>
               <ts e="T224" id="Seg_2130" n="e" s="T223">orqɨlpɔːtɨt </ts>
               <ts e="T225" id="Seg_2132" n="e" s="T224">irap, </ts>
               <ts e="T226" id="Seg_2134" n="e" s="T225">ola </ts>
               <ts e="T227" id="Seg_2136" n="e" s="T226">čʼalʼ </ts>
               <ts e="T228" id="Seg_2138" n="e" s="T227">nʼomalʼ </ts>
               <ts e="T229" id="Seg_2140" n="e" s="T228">porqɨ </ts>
               <ts e="T230" id="Seg_2142" n="e" s="T229">lʼıːpɨp </ts>
               <ts e="T231" id="Seg_2144" n="e" s="T230">orqɨlpɔːtät. </ts>
               <ts e="T232" id="Seg_2146" n="e" s="T231">Qapija </ts>
               <ts e="T233" id="Seg_2148" n="e" s="T232">na </ts>
               <ts e="T234" id="Seg_2150" n="e" s="T233">tüsa </ts>
               <ts e="T235" id="Seg_2152" n="e" s="T234">okoškan </ts>
               <ts e="T236" id="Seg_2154" n="e" s="T235">ɔːkmɨt </ts>
               <ts e="T237" id="Seg_2156" n="e" s="T236">mɔːttɨ. </ts>
               <ts e="T238" id="Seg_2158" n="e" s="T237">Šitə </ts>
               <ts e="T239" id="Seg_2160" n="e" s="T238">okoškatə </ts>
               <ts e="T240" id="Seg_2162" n="e" s="T239">ɛːpɨmpa. </ts>
               <ts e="T241" id="Seg_2164" n="e" s="T240">Qaj </ts>
               <ts e="T242" id="Seg_2166" n="e" s="T241">märqɨ </ts>
               <ts e="T243" id="Seg_2168" n="e" s="T242">mɔːttɨ </ts>
               <ts e="T244" id="Seg_2170" n="e" s="T243">ɛːpɨmmɨnta! </ts>
               <ts e="T245" id="Seg_2172" n="e" s="T244">Tüsa </ts>
               <ts e="T246" id="Seg_2174" n="e" s="T245">taqpa, </ts>
               <ts e="T247" id="Seg_2176" n="e" s="T246">loːqɨra </ts>
               <ts e="T248" id="Seg_2178" n="e" s="T247">poːlʼ </ts>
               <ts e="T249" id="Seg_2180" n="e" s="T248">moːt, </ts>
               <ts e="T250" id="Seg_2182" n="e" s="T249">pičʼit </ts>
               <ts e="T251" id="Seg_2184" n="e" s="T250">paqɨt </ts>
               <ts e="T252" id="Seg_2186" n="e" s="T251">piːrɨ </ts>
               <ts e="T253" id="Seg_2188" n="e" s="T252">taqpa. </ts>
               <ts e="T254" id="Seg_2190" n="e" s="T253">Teŋtɨna </ts>
               <ts e="T255" id="Seg_2192" n="e" s="T254">tüːp </ts>
               <ts e="T256" id="Seg_2194" n="e" s="T255">na </ts>
               <ts e="T257" id="Seg_2196" n="e" s="T256">qəːlpɨntɔːtɨt, </ts>
               <ts e="T258" id="Seg_2198" n="e" s="T257">mɔːttɨ. </ts>
               <ts e="T259" id="Seg_2200" n="e" s="T258">Poroqsä </ts>
               <ts e="T260" id="Seg_2202" n="e" s="T259">tomorot </ts>
               <ts e="T261" id="Seg_2204" n="e" s="T260">qamnɨmpa </ts>
               <ts e="T262" id="Seg_2206" n="e" s="T261">na </ts>
               <ts e="T263" id="Seg_2208" n="e" s="T262">nʼuːtɨt </ts>
               <ts e="T264" id="Seg_2210" n="e" s="T263">ɨːlʼtɨ. </ts>
               <ts e="T265" id="Seg_2212" n="e" s="T264">Tü </ts>
               <ts e="T266" id="Seg_2214" n="e" s="T265">na </ts>
               <ts e="T267" id="Seg_2216" n="e" s="T266">qättɨmmɨntɨtɨ. </ts>
               <ts e="T268" id="Seg_2218" n="e" s="T267">Tıntena </ts>
               <ts e="T269" id="Seg_2220" n="e" s="T268">nɔːssarɨlʼ </ts>
               <ts e="T270" id="Seg_2222" n="e" s="T269">qumɨlʼ </ts>
               <ts e="T271" id="Seg_2224" n="e" s="T270">mütɨ </ts>
               <ts e="T272" id="Seg_2226" n="e" s="T271">tü </ts>
               <ts e="T273" id="Seg_2228" n="e" s="T272">ampatɨ. </ts>
               <ts e="T274" id="Seg_2230" n="e" s="T273">Qonŋɔːtɨt </ts>
               <ts e="T275" id="Seg_2232" n="e" s="T274">ınnänɨlʼe </ts>
               <ts e="T276" id="Seg_2234" n="e" s="T275">amnäiːtɨ. </ts>
               <ts e="T277" id="Seg_2236" n="e" s="T276">Ira </ts>
               <ts e="T278" id="Seg_2238" n="e" s="T277">ponä </ts>
               <ts e="T279" id="Seg_2240" n="e" s="T278">paktälʼä </ts>
               <ts e="T280" id="Seg_2242" n="e" s="T279">pičʼilʼ </ts>
               <ts e="T281" id="Seg_2244" n="e" s="T280">utät </ts>
               <ts e="T282" id="Seg_2246" n="e" s="T281">märka. </ts>
               <ts e="T283" id="Seg_2248" n="e" s="T282">Isɨ </ts>
               <ts e="T284" id="Seg_2250" n="e" s="T283">čʼap </ts>
               <ts e="T285" id="Seg_2252" n="e" s="T284">ponä </ts>
               <ts e="T286" id="Seg_2254" n="e" s="T285">qontɨšɛlʼčʼɔːtɨt </ts>
               <ts e="T287" id="Seg_2256" n="e" s="T286">aj </ts>
               <ts e="T288" id="Seg_2258" n="e" s="T287">čʼar </ts>
               <ts e="T289" id="Seg_2260" n="e" s="T288">qättɔːtɨt. </ts>
               <ts e="T290" id="Seg_2262" n="e" s="T289">Amnaiːtɨ </ts>
               <ts e="T291" id="Seg_2264" n="e" s="T290">tüp </ts>
               <ts e="T292" id="Seg_2266" n="e" s="T291">na </ts>
               <ts e="T293" id="Seg_2268" n="e" s="T292">qämmɨntɔːtɨt. </ts>
               <ts e="T294" id="Seg_2270" n="e" s="T293">Šoŋol </ts>
               <ts e="T295" id="Seg_2272" n="e" s="T294">pɔːrɨ </ts>
               <ts e="T296" id="Seg_2274" n="e" s="T295">šuː </ts>
               <ts e="T297" id="Seg_2276" n="e" s="T296">ıllä, </ts>
               <ts e="T298" id="Seg_2278" n="e" s="T297">aj </ts>
               <ts e="T299" id="Seg_2280" n="e" s="T298">šitə </ts>
               <ts e="T300" id="Seg_2282" n="e" s="T299">okoškan </ts>
               <ts e="T301" id="Seg_2284" n="e" s="T300">ɔːkti </ts>
               <ts e="T302" id="Seg_2286" n="e" s="T301">mɔːtti </ts>
               <ts e="T303" id="Seg_2288" n="e" s="T302">qəːŋɔːtɨ. </ts>
               <ts e="T304" id="Seg_2290" n="e" s="T303">Mɔːtɨp </ts>
               <ts e="T305" id="Seg_2292" n="e" s="T304">tüsä </ts>
               <ts e="T306" id="Seg_2294" n="e" s="T305">čʼɔːtäjɔːtɨt. </ts>
               <ts e="T307" id="Seg_2296" n="e" s="T306">Na </ts>
               <ts e="T308" id="Seg_2298" n="e" s="T307">qälilʼ </ts>
               <ts e="T309" id="Seg_2300" n="e" s="T308">mütɨp </ts>
               <ts e="T310" id="Seg_2302" n="e" s="T309">(/mütɨt) </ts>
               <ts e="T311" id="Seg_2304" n="e" s="T310">olqup </ts>
               <ts e="T312" id="Seg_2306" n="e" s="T311">šitä </ts>
               <ts e="T313" id="Seg_2308" n="e" s="T312">ɛːpɨntɔːtɨt. </ts>
               <ts e="T314" id="Seg_2310" n="e" s="T313">Nɨnɨ </ts>
               <ts e="T315" id="Seg_2312" n="e" s="T314">kara </ts>
               <ts e="T316" id="Seg_2314" n="e" s="T315">qənna, </ts>
               <ts e="T317" id="Seg_2316" n="e" s="T316">qälʼatɨt </ts>
               <ts e="T318" id="Seg_2318" n="e" s="T317">qaqlante, </ts>
               <ts e="T319" id="Seg_2320" n="e" s="T318">kara </ts>
               <ts e="T320" id="Seg_2322" n="e" s="T319">qaj </ts>
               <ts e="T321" id="Seg_2324" n="e" s="T320">qaqlantä </ts>
               <ts e="T322" id="Seg_2326" n="e" s="T321">qənna </ts>
               <ts e="T323" id="Seg_2328" n="e" s="T322">aj </ts>
               <ts e="T324" id="Seg_2330" n="e" s="T323">šittə </ts>
               <ts e="T325" id="Seg_2332" n="e" s="T324">ännaqıtɨ </ts>
               <ts e="T326" id="Seg_2334" n="e" s="T325">qənnɨtɨtɨ. </ts>
               <ts e="T327" id="Seg_2336" n="e" s="T326">Šite </ts>
               <ts e="T328" id="Seg_2338" n="e" s="T327">ännaqıtɨ </ts>
               <ts e="T329" id="Seg_2340" n="e" s="T328">qənnɨtɨtɨ, </ts>
               <ts e="T330" id="Seg_2342" n="e" s="T329">qətqilʼpɨti </ts>
               <ts e="T331" id="Seg_2344" n="e" s="T330">qumɨiːqɨntɨ. </ts>
               <ts e="T332" id="Seg_2346" n="e" s="T331">Karanɨ </ts>
               <ts e="T333" id="Seg_2348" n="e" s="T332">qonna, </ts>
               <ts e="T334" id="Seg_2350" n="e" s="T333">moːčʼalʼ </ts>
               <ts e="T454" id="Seg_2352" n="e" s="T334">qälʼi </ts>
               <ts e="T335" id="Seg_2354" n="e" s="T454">ira </ts>
               <ts e="T336" id="Seg_2356" n="e" s="T335">qontɨssɛːnta. </ts>
               <ts e="T337" id="Seg_2358" n="e" s="T336">Qaj </ts>
               <ts e="T338" id="Seg_2360" n="e" s="T337">nʼomalʼ </ts>
               <ts e="T339" id="Seg_2362" n="e" s="T338">porqaj </ts>
               <ts e="T340" id="Seg_2364" n="e" s="T339">irat </ts>
               <ts e="T341" id="Seg_2366" n="e" s="T340">imap </ts>
               <ts e="T342" id="Seg_2368" n="e" s="T341">qəːčʼisɨlɨt. </ts>
               <ts e="T343" id="Seg_2370" n="e" s="T342">Ira </ts>
               <ts e="T344" id="Seg_2372" n="e" s="T343">čʼarrɨ </ts>
               <ts e="T345" id="Seg_2374" n="e" s="T344">qättɨŋɨt. </ts>
               <ts e="T346" id="Seg_2376" n="e" s="T345">Nɨːna </ts>
               <ts e="T347" id="Seg_2378" n="e" s="T346">kara </ts>
               <ts e="T348" id="Seg_2380" n="e" s="T347">kurolʼna. </ts>
               <ts e="T349" id="Seg_2382" n="e" s="T348">Nɔːkɨr </ts>
               <ts e="T350" id="Seg_2384" n="e" s="T349">ämnasit </ts>
               <ts e="T351" id="Seg_2386" n="e" s="T350">takɨ </ts>
               <ts e="T352" id="Seg_2388" n="e" s="T351">qännajɔːt, </ts>
               <ts e="T353" id="Seg_2390" n="e" s="T352">näčʼä </ts>
               <ts e="T354" id="Seg_2392" n="e" s="T353">laqaltɛlʼčʼɔːtɨ, </ts>
               <ts e="T355" id="Seg_2394" n="e" s="T354">qalitɨt </ts>
               <ts e="T356" id="Seg_2396" n="e" s="T355">mɔːttɨ </ts>
               <ts e="T357" id="Seg_2398" n="e" s="T356">takkɨ </ts>
               <ts e="T358" id="Seg_2400" n="e" s="T357">qənnɔːtɨt. </ts>
               <ts e="T359" id="Seg_2402" n="e" s="T358">Qälitɨt </ts>
               <ts e="T360" id="Seg_2404" n="e" s="T359">mɔːttɨ </ts>
               <ts e="T361" id="Seg_2406" n="e" s="T360">imatɨt </ts>
               <ts e="T362" id="Seg_2408" n="e" s="T361">ılla </ts>
               <ts e="T363" id="Seg_2410" n="e" s="T362">qənnɔːtɨt. </ts>
               <ts e="T364" id="Seg_2412" n="e" s="T363">Nɨmtɨ </ts>
               <ts e="T365" id="Seg_2414" n="e" s="T364">mɔːtqɨt </ts>
               <ts e="T366" id="Seg_2416" n="e" s="T365">šolʼqumɨlʼ </ts>
               <ts e="T367" id="Seg_2418" n="e" s="T366">šitɨ </ts>
               <ts e="T368" id="Seg_2420" n="e" s="T367">ijaqıp </ts>
               <ts e="T369" id="Seg_2422" n="e" s="T368">qəːčʼimpɔːtɨt. </ts>
               <ts e="T370" id="Seg_2424" n="e" s="T369">Tomorot </ts>
               <ts e="T371" id="Seg_2426" n="e" s="T370">nʼomalʼ </ts>
               <ts e="T372" id="Seg_2428" n="e" s="T371">porqaj </ts>
               <ts e="T373" id="Seg_2430" n="e" s="T372">irat </ts>
               <ts e="T374" id="Seg_2432" n="e" s="T373">kətsatqı. </ts>
               <ts e="T375" id="Seg_2434" n="e" s="T374">Namɨn </ts>
               <ts e="T376" id="Seg_2436" n="e" s="T375">ijaqı, </ts>
               <ts e="T377" id="Seg_2438" n="e" s="T376">ɔːtap </ts>
               <ts e="T378" id="Seg_2440" n="e" s="T377">tɔːqqolʼä </ts>
               <ts e="T379" id="Seg_2442" n="e" s="T378">konnä </ts>
               <ts e="T380" id="Seg_2444" n="e" s="T379">qənnɔːtɨt. </ts>
               <ts e="T381" id="Seg_2446" n="e" s="T380">Konna </ts>
               <ts e="T382" id="Seg_2448" n="e" s="T381">mɔːtɨlnɔːtɨt. </ts>
               <ts e="T383" id="Seg_2450" n="e" s="T382">Ukkur </ts>
               <ts e="T384" id="Seg_2452" n="e" s="T383">pit </ts>
               <ts e="T385" id="Seg_2454" n="e" s="T384">šäqɔːtɨt. </ts>
               <ts e="T386" id="Seg_2456" n="e" s="T385">Ijaiːtɨ </ts>
               <ts e="T387" id="Seg_2458" n="e" s="T386">täpa </ts>
               <ts e="T388" id="Seg_2460" n="e" s="T387">čʼeːlʼ </ts>
               <ts e="T389" id="Seg_2462" n="e" s="T388">moqonä </ts>
               <ts e="T390" id="Seg_2464" n="e" s="T389">tüŋɔːtät. </ts>
               <ts e="T391" id="Seg_2466" n="e" s="T390">Əsɨ </ts>
               <ts e="T392" id="Seg_2468" n="e" s="T391">iramtɨ </ts>
               <ts e="T393" id="Seg_2470" n="e" s="T392">qəntalʼa </ts>
               <ts e="T394" id="Seg_2472" n="e" s="T393">tüŋɔːtät </ts>
               <ts e="T395" id="Seg_2474" n="e" s="T394">ijaiːtɨ. </ts>
               <ts e="T396" id="Seg_2476" n="e" s="T395">Täpɨt </ts>
               <ts e="T397" id="Seg_2478" n="e" s="T396">nılʼčʼik </ts>
               <ts e="T398" id="Seg_2480" n="e" s="T397">tɛnirpɔːtät: </ts>
               <ts e="T399" id="Seg_2482" n="e" s="T398">Mat </ts>
               <ts e="T400" id="Seg_2484" n="e" s="T399">äsamɨt </ts>
               <ts e="T401" id="Seg_2486" n="e" s="T400">qətpɔːtɨt. </ts>
               <ts e="T402" id="Seg_2488" n="e" s="T401">Təp </ts>
               <ts e="T403" id="Seg_2490" n="e" s="T402">seːlʼčʼi </ts>
               <ts e="T404" id="Seg_2492" n="e" s="T403">iːjatɨ </ts>
               <ts e="T405" id="Seg_2494" n="e" s="T404">ɛːppɨntɨ, </ts>
               <ts e="T406" id="Seg_2496" n="e" s="T405">kɨpa </ts>
               <ts e="T407" id="Seg_2498" n="e" s="T406">ämnätɨ. </ts>
               <ts e="T408" id="Seg_2500" n="e" s="T407">Täpɨt </ts>
               <ts e="T409" id="Seg_2502" n="e" s="T408">qäntalʼa </ts>
               <ts e="T410" id="Seg_2504" n="e" s="T409">taːtɔːtɨt. </ts>
               <ts e="T411" id="Seg_2506" n="e" s="T410">Iːjantɨ </ts>
               <ts e="T412" id="Seg_2508" n="e" s="T411">kanap </ts>
               <ts e="T413" id="Seg_2510" n="e" s="T412">poːqot </ts>
               <ts e="T414" id="Seg_2512" n="e" s="T413">sɔːrɨmpɔːtɨn. </ts>
               <ts e="T415" id="Seg_2514" n="e" s="T414">Ɔːtan </ts>
               <ts e="T416" id="Seg_2516" n="e" s="T415">ätɨmaptaqɨt </ts>
               <ts e="T417" id="Seg_2518" n="e" s="T416">täpɨt </ts>
               <ts e="T418" id="Seg_2520" n="e" s="T417">kanan </ts>
               <ts e="T419" id="Seg_2522" n="e" s="T418">topɨp </ts>
               <ts e="T420" id="Seg_2524" n="e" s="T419">čʼıqɨlʼnɔːtɨn. </ts>
               <ts e="T421" id="Seg_2526" n="e" s="T420">Ira </ts>
               <ts e="T422" id="Seg_2528" n="e" s="T421">nılʼčʼik </ts>
               <ts e="T423" id="Seg_2530" n="e" s="T422">kətɨŋɨtɨ: </ts>
               <ts e="T424" id="Seg_2532" n="e" s="T423">Kanat </ts>
               <ts e="T425" id="Seg_2534" n="e" s="T424">topɨp </ts>
               <ts e="T426" id="Seg_2536" n="e" s="T425">qaj </ts>
               <ts e="T427" id="Seg_2538" n="e" s="T426">čʼıqɨlnɨtɨ? </ts>
               <ts e="T428" id="Seg_2540" n="e" s="T427">Kɨpa </ts>
               <ts e="T429" id="Seg_2542" n="e" s="T428">ijantɨ </ts>
               <ts e="T430" id="Seg_2544" n="e" s="T429">ima </ts>
               <ts e="T431" id="Seg_2546" n="e" s="T430">pona </ts>
               <ts e="T432" id="Seg_2548" n="e" s="T431">pakta. </ts>
               <ts e="T433" id="Seg_2550" n="e" s="T432">Naqaj </ts>
               <ts e="T434" id="Seg_2552" n="e" s="T433">ontä </ts>
               <ts e="T435" id="Seg_2554" n="e" s="T434">iratɨ </ts>
               <ts e="T436" id="Seg_2556" n="e" s="T435">tüntɨ. </ts>
               <ts e="T437" id="Seg_2558" n="e" s="T436">Naqaj </ts>
               <ts e="T438" id="Seg_2560" n="e" s="T437">ontɨ </ts>
               <ts e="T439" id="Seg_2562" n="e" s="T438">iːjatɨ </ts>
               <ts e="T440" id="Seg_2564" n="e" s="T439">tümpa. </ts>
               <ts e="T441" id="Seg_2566" n="e" s="T440">Nɨːnɨ </ts>
               <ts e="T442" id="Seg_2568" n="e" s="T441">ilʼčʼatɨt </ts>
               <ts e="T443" id="Seg_2570" n="e" s="T442">nılʼčʼik </ts>
               <ts e="T444" id="Seg_2572" n="e" s="T443">kəttɨmpatɨ: </ts>
               <ts e="T445" id="Seg_2574" n="e" s="T444">Montɨ </ts>
               <ts e="T446" id="Seg_2576" n="e" s="T445">onɨt </ts>
               <ts e="T447" id="Seg_2578" n="e" s="T446">tüsa </ts>
               <ts e="T448" id="Seg_2580" n="e" s="T447">čʼɔːtɨsɨmɨt. </ts>
               <ts e="T449" id="Seg_2582" n="e" s="T448">Qälʼij </ts>
               <ts e="T450" id="Seg_2584" n="e" s="T449">təttan </ts>
               <ts e="T451" id="Seg_2586" n="e" s="T450">ɔːtap </ts>
               <ts e="T452" id="Seg_2588" n="e" s="T451">nʼenna </ts>
               <ts e="T453" id="Seg_2590" n="e" s="T452">tɔːqɨŋɨtɨ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_2591" s="T0">NEP_1965_HareParka1_flk.001 (001)</ta>
            <ta e="T9" id="Seg_2592" s="T3">NEP_1965_HareParka1_flk.002 (002.001)</ta>
            <ta e="T16" id="Seg_2593" s="T9">NEP_1965_HareParka1_flk.003 (002.002)</ta>
            <ta e="T21" id="Seg_2594" s="T16">NEP_1965_HareParka1_flk.004 (002.003)</ta>
            <ta e="T24" id="Seg_2595" s="T21">NEP_1965_HareParka1_flk.005 (002.004)</ta>
            <ta e="T26" id="Seg_2596" s="T24">NEP_1965_HareParka1_flk.006 (002.005)</ta>
            <ta e="T30" id="Seg_2597" s="T26">NEP_1965_HareParka1_flk.007 (002.006)</ta>
            <ta e="T34" id="Seg_2598" s="T30">NEP_1965_HareParka1_flk.008 (002.007)</ta>
            <ta e="T41" id="Seg_2599" s="T34">NEP_1965_HareParka1_flk.009 (002.008)</ta>
            <ta e="T44" id="Seg_2600" s="T41">NEP_1965_HareParka1_flk.010 (002.009)</ta>
            <ta e="T53" id="Seg_2601" s="T44">NEP_1965_HareParka1_flk.011 (002.010)</ta>
            <ta e="T58" id="Seg_2602" s="T53">NEP_1965_HareParka1_flk.012 (002.011)</ta>
            <ta e="T70" id="Seg_2603" s="T58">NEP_1965_HareParka1_flk.013 (003.001)</ta>
            <ta e="T74" id="Seg_2604" s="T70">NEP_1965_HareParka1_flk.014 (003.002)</ta>
            <ta e="T78" id="Seg_2605" s="T74">NEP_1965_HareParka1_flk.015 (003.003)</ta>
            <ta e="T81" id="Seg_2606" s="T78">NEP_1965_HareParka1_flk.016 (003.004)</ta>
            <ta e="T83" id="Seg_2607" s="T81">NEP_1965_HareParka1_flk.017 (003.005)</ta>
            <ta e="T89" id="Seg_2608" s="T83">NEP_1965_HareParka1_flk.018 (003.006)</ta>
            <ta e="T92" id="Seg_2609" s="T89">NEP_1965_HareParka1_flk.019 (003.007)</ta>
            <ta e="T93" id="Seg_2610" s="T92">NEP_1965_HareParka1_flk.020 (003.008)</ta>
            <ta e="T99" id="Seg_2611" s="T93">NEP_1965_HareParka1_flk.021 (003.009)</ta>
            <ta e="T102" id="Seg_2612" s="T99">NEP_1965_HareParka1_flk.022 (003.010)</ta>
            <ta e="T110" id="Seg_2613" s="T102">NEP_1965_HareParka1_flk.023 (003.011)</ta>
            <ta e="T114" id="Seg_2614" s="T110">NEP_1965_HareParka1_flk.024 (003.012)</ta>
            <ta e="T117" id="Seg_2615" s="T114">NEP_1965_HareParka1_flk.025 (003.013)</ta>
            <ta e="T118" id="Seg_2616" s="T117">NEP_1965_HareParka1_flk.026 (003.014)</ta>
            <ta e="T120" id="Seg_2617" s="T118">NEP_1965_HareParka1_flk.027 (003.015)</ta>
            <ta e="T123" id="Seg_2618" s="T120">NEP_1965_HareParka1_flk.028 (003.016)</ta>
            <ta e="T126" id="Seg_2619" s="T123">NEP_1965_HareParka1_flk.029 (003.017)</ta>
            <ta e="T129" id="Seg_2620" s="T126">NEP_1965_HareParka1_flk.030 (003.018)</ta>
            <ta e="T131" id="Seg_2621" s="T129">NEP_1965_HareParka1_flk.031 (003.019)</ta>
            <ta e="T134" id="Seg_2622" s="T131">NEP_1965_HareParka1_flk.032 (003.020)</ta>
            <ta e="T139" id="Seg_2623" s="T134">NEP_1965_HareParka1_flk.033 (003.021)</ta>
            <ta e="T145" id="Seg_2624" s="T139">NEP_1965_HareParka1_flk.034 (003.022)</ta>
            <ta e="T146" id="Seg_2625" s="T145">NEP_1965_HareParka1_flk.035 (003.023)</ta>
            <ta e="T153" id="Seg_2626" s="T146">NEP_1965_HareParka1_flk.036 (003.024)</ta>
            <ta e="T154" id="Seg_2627" s="T153">NEP_1965_HareParka1_flk.037 (003.025)</ta>
            <ta e="T160" id="Seg_2628" s="T154">NEP_1965_HareParka1_flk.038 (003.026)</ta>
            <ta e="T163" id="Seg_2629" s="T160">NEP_1965_HareParka1_flk.039 (003.027)</ta>
            <ta e="T168" id="Seg_2630" s="T163">NEP_1965_HareParka1_flk.040 (003.028)</ta>
            <ta e="T171" id="Seg_2631" s="T168">NEP_1965_HareParka1_flk.041 (003.029)</ta>
            <ta e="T177" id="Seg_2632" s="T171">NEP_1965_HareParka1_flk.042 (003.030)</ta>
            <ta e="T178" id="Seg_2633" s="T177">NEP_1965_HareParka1_flk.043 (003.031)</ta>
            <ta e="T187" id="Seg_2634" s="T178">NEP_1965_HareParka1_flk.044 (003.032)</ta>
            <ta e="T193" id="Seg_2635" s="T187">NEP_1965_HareParka1_flk.045 (003.033)</ta>
            <ta e="T196" id="Seg_2636" s="T193">NEP_1965_HareParka1_flk.046 (003.034)</ta>
            <ta e="T202" id="Seg_2637" s="T196">NEP_1965_HareParka1_flk.047 (003.035)</ta>
            <ta e="T209" id="Seg_2638" s="T202">NEP_1965_HareParka1_flk.048 (003.036)</ta>
            <ta e="T218" id="Seg_2639" s="T209">NEP_1965_HareParka1_flk.049 (003.037)</ta>
            <ta e="T220" id="Seg_2640" s="T218">NEP_1965_HareParka1_flk.050 (003.038)</ta>
            <ta e="T231" id="Seg_2641" s="T220">NEP_1965_HareParka1_flk.051 (003.039)</ta>
            <ta e="T237" id="Seg_2642" s="T231">NEP_1965_HareParka1_flk.052 (003.040)</ta>
            <ta e="T240" id="Seg_2643" s="T237">NEP_1965_HareParka1_flk.053 (003.041)</ta>
            <ta e="T244" id="Seg_2644" s="T240">NEP_1965_HareParka1_flk.054 (003.042)</ta>
            <ta e="T253" id="Seg_2645" s="T244">NEP_1965_HareParka1_flk.055 (003.043)</ta>
            <ta e="T258" id="Seg_2646" s="T253">NEP_1965_HareParka1_flk.056 (003.044)</ta>
            <ta e="T264" id="Seg_2647" s="T258">NEP_1965_HareParka1_flk.057 (003.045)</ta>
            <ta e="T267" id="Seg_2648" s="T264">NEP_1965_HareParka1_flk.058 (003.046)</ta>
            <ta e="T273" id="Seg_2649" s="T267">NEP_1965_HareParka1_flk.059 (003.047)</ta>
            <ta e="T276" id="Seg_2650" s="T273">NEP_1965_HareParka1_flk.060 (003.048)</ta>
            <ta e="T282" id="Seg_2651" s="T276">NEP_1965_HareParka1_flk.061 (003.049)</ta>
            <ta e="T289" id="Seg_2652" s="T282">NEP_1965_HareParka1_flk.062 (003.050)</ta>
            <ta e="T293" id="Seg_2653" s="T289">NEP_1965_HareParka1_flk.063 (003.051)</ta>
            <ta e="T303" id="Seg_2654" s="T293">NEP_1965_HareParka1_flk.064 (003.052)</ta>
            <ta e="T306" id="Seg_2655" s="T303">NEP_1965_HareParka1_flk.065 (003.053)</ta>
            <ta e="T313" id="Seg_2656" s="T306">NEP_1965_HareParka1_flk.066 (003.054)</ta>
            <ta e="T326" id="Seg_2657" s="T313">NEP_1965_HareParka1_flk.067 (004.001)</ta>
            <ta e="T331" id="Seg_2658" s="T326">NEP_1965_HareParka1_flk.068 (004.002)</ta>
            <ta e="T336" id="Seg_2659" s="T331">NEP_1965_HareParka1_flk.069 (004.003)</ta>
            <ta e="T342" id="Seg_2660" s="T336">NEP_1965_HareParka1_flk.070 (004.004)</ta>
            <ta e="T345" id="Seg_2661" s="T342">NEP_1965_HareParka1_flk.071 (004.005)</ta>
            <ta e="T348" id="Seg_2662" s="T345">NEP_1965_HareParka1_flk.072 (004.006)</ta>
            <ta e="T358" id="Seg_2663" s="T348">NEP_1965_HareParka1_flk.073 (004.007)</ta>
            <ta e="T363" id="Seg_2664" s="T358">NEP_1965_HareParka1_flk.074 (004.008)</ta>
            <ta e="T369" id="Seg_2665" s="T363">NEP_1965_HareParka1_flk.075 (004.009)</ta>
            <ta e="T374" id="Seg_2666" s="T369">NEP_1965_HareParka1_flk.076 (004.010)</ta>
            <ta e="T380" id="Seg_2667" s="T374">NEP_1965_HareParka1_flk.077 (004.011)</ta>
            <ta e="T382" id="Seg_2668" s="T380">NEP_1965_HareParka1_flk.078 (004.012)</ta>
            <ta e="T385" id="Seg_2669" s="T382">NEP_1965_HareParka1_flk.079 (004.013)</ta>
            <ta e="T390" id="Seg_2670" s="T385">NEP_1965_HareParka1_flk.080 (004.014)</ta>
            <ta e="T395" id="Seg_2671" s="T390">NEP_1965_HareParka1_flk.081 (004.015)</ta>
            <ta e="T401" id="Seg_2672" s="T395">NEP_1965_HareParka1_flk.082 (004.016)</ta>
            <ta e="T407" id="Seg_2673" s="T401">NEP_1965_HareParka1_flk.083 (004.017)</ta>
            <ta e="T410" id="Seg_2674" s="T407">NEP_1965_HareParka1_flk.084 (004.018)</ta>
            <ta e="T414" id="Seg_2675" s="T410">NEP_1965_HareParka1_flk.085 (004.019)</ta>
            <ta e="T420" id="Seg_2676" s="T414">NEP_1965_HareParka1_flk.086 (004.020)</ta>
            <ta e="T427" id="Seg_2677" s="T420">NEP_1965_HareParka1_flk.087 (004.021)</ta>
            <ta e="T432" id="Seg_2678" s="T427">NEP_1965_HareParka1_flk.088 (004.022)</ta>
            <ta e="T436" id="Seg_2679" s="T432">NEP_1965_HareParka1_flk.089 (004.023)</ta>
            <ta e="T440" id="Seg_2680" s="T436">NEP_1965_HareParka1_flk.090 (004.024)</ta>
            <ta e="T448" id="Seg_2681" s="T440">NEP_1965_HareParka1_flk.091 (004.025)</ta>
            <ta e="T453" id="Seg_2682" s="T448">NEP_1965_HareParka1_flk.092 (004.026)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_2683" s="T0">нʼӧмаl ′порkы и′ра.</ta>
            <ta e="T9" id="Seg_2684" s="T3">′селʼчи и′jаты ′эппӓнды, ай ′селʼчи ам′нӓты.</ta>
            <ta e="T16" id="Seg_2685" s="T9">′kа̄lит тӱ′ңотыт таккын[н]ы [таkkынды] ′нʼаннӓ ′тӓпып ′kъ̊тkынтытkо.</ta>
            <ta e="T21" id="Seg_2686" s="T16">kу′мийты иjа′иты ма̄′чонты kъ̊н′потыт тӓ ′пелʼлʼе.</ta>
            <ta e="T24" id="Seg_2687" s="T21">kуп тӣ ′kъ̊ттиңыт.</ta>
            <ta e="T26" id="Seg_2688" s="T24">′kу′ттар ′еңа.</ta>
            <ta e="T30" id="Seg_2689" s="T26">кӧт шʼолʼлʼи kумылʼ и′jаkып ′мӓрым′потыт</ta>
            <ta e="T34" id="Seg_2690" s="T30">на kӓ̄lит, намып[м] ӱ̄тынд̂ыт.</ta>
            <ta e="T41" id="Seg_2691" s="T34">′нʼӧмалʼ ′порkы ′иратkине ме телымы′та тӱн′томыт.</ta>
            <ta e="T44" id="Seg_2692" s="T41">коннинныlыт [′конинныт] ′ъ̊̄тытkо.</ta>
            <ta e="T53" id="Seg_2693" s="T44">и′jаkи моkонӓ ′kъ̊лʼлʼе нӣк ′kъ̊тыңыт. чи′лʼӓлʼ и′jаkий, ′kə[ӓ]lыkи ′мырыңыт.</ta>
            <ta e="T58" id="Seg_2694" s="T53">ныны ′шʼиттелʼ ′ирра ку′че kа′тента.</ta>
            <ta e="T70" id="Seg_2695" s="T58">ира ниlчик ′е̨сса, на нʼӧмаl порkъ ′ира амнӓнтылʼмыт′кӣне. ӱ̄р ′апсы ′таттыңыт.</ta>
            <ta e="T74" id="Seg_2696" s="T70">кун монтыl kай ′таттыңыт.</ta>
            <ta e="T78" id="Seg_2697" s="T74">кӓ̄lит ′телʼе ‵тӱн′тотыт, амыртен′тотыт.</ta>
            <ta e="T81" id="Seg_2698" s="T78">ира нӣк ′kъ̊̄тыт.</ta>
            <ta e="T83" id="Seg_2699" s="T81">′апсып тат′тотыт.</ta>
            <ta e="T89" id="Seg_2700" s="T83">там[п] мо̄тыт шʼунʼчип шʼенты нʼӱ̄тысӓ тоk′котыт.</ta>
            <ta e="T92" id="Seg_2701" s="T89">ира ′kӓlимты апсытыко′lамныт.</ta>
            <ta e="T93" id="Seg_2702" s="T92">ъ̊̄!</ta>
            <ta e="T99" id="Seg_2703" s="T93">то̄птылʼ ′чеlы kӓ̄lыт ′наjеп татын′тотыт.</ta>
            <ta e="T102" id="Seg_2704" s="T99">но′ссарылʼ kумылʼ ′мӱ̄ты.</ta>
            <ta e="T110" id="Seg_2705" s="T102">′kапиjа шʼунʼчипылʼ мо̄тkыт нʼомалʼ порkы ира ′онтыкыт амне′интысӓ.</ta>
            <ta e="T114" id="Seg_2706" s="T110">шенты ′нʼӱтыса ′то̨kыральтыт [тоkыралʼимпатыт].</ta>
            <ta e="T117" id="Seg_2707" s="T114">ира ниlчик ӓ̄са.</ta>
            <ta e="T118" id="Seg_2708" s="T117">′амыркоlамнотыт.</ta>
            <ta e="T120" id="Seg_2709" s="T118">апсып kон′тотыт.</ta>
            <ta e="T123" id="Seg_2710" s="T120">ира нӣк ′kъ̊ттыңыт.</ta>
            <ta e="T126" id="Seg_2711" s="T123">имаkотаты ай ′е̄ңа.</ta>
            <ta e="T129" id="Seg_2712" s="T126">амне′имты нӣк кураlтыт.</ta>
            <ta e="T131" id="Seg_2713" s="T129">понӓ ′тантыңыlыт.</ta>
            <ta e="T134" id="Seg_2714" s="T131">иjатып ′пона ′таттыңылыт.</ta>
            <ta e="T139" id="Seg_2715" s="T134">и′ра ′онты в[м]ӓр[ъ]kа кумӣмты ′апсытымбы‵лʼе.</ta>
            <ta e="T145" id="Seg_2716" s="T139">уккур тъ̊т чон′тоkыт тӱнтъ kа′рре̨т kуп.</ta>
            <ta e="T146" id="Seg_2717" s="T145">амырнот.</ta>
            <ta e="T153" id="Seg_2718" s="T146">шʼ[с]о′ңоlиl [шʼоңолʼ] ′тӱмты нам мӓрыk ′по̄са ′сълыңыт.</ta>
            <ta e="T154" id="Seg_2719" s="T153">′чо̄пынта.</ta>
            <ta e="T160" id="Seg_2720" s="T154">‵амне′имты аша kа̄ [kайkо] пона ӱтеңыт.</ta>
            <ta e="T163" id="Seg_2721" s="T160">поkыт тӱ̄ чотеңтотет.</ta>
            <ta e="T168" id="Seg_2722" s="T163">тӱп чо̄тел′потет тинтена тъ̊[ӧ]п, ′пурышиңоl‵потыт.</ta>
            <ta e="T171" id="Seg_2723" s="T168">ниlчик kай ‵тъ̊кырым′по̄тыт.</ta>
            <ta e="T177" id="Seg_2724" s="T171">те на ′ентыl ман′на тан′нентак.</ta>
            <ta e="T178" id="Seg_2725" s="T177">′амыр‵нотыт.</ta>
            <ta e="T187" id="Seg_2726" s="T178">kӓ̄lит нӣк kъ̊′тотӓт, иlча, шоңоlай ′kаjетkе на′шак ′посӓ сӓ̄лал.</ta>
            <ta e="T193" id="Seg_2727" s="T187">′ашʼа шо′ңол мы тӱна ′амтыт.</ta>
            <ta e="T196" id="Seg_2728" s="T193">тӓм аш ′амтыт.</ta>
            <ta e="T202" id="Seg_2729" s="T196">нӣк мӓр′kе[а]птымпа, нʼомаl порkынты ра̄′шаl ′лʼӣпы.</ta>
            <ta e="T209" id="Seg_2730" s="T202">наммы ′шерпынтыт шо′ңоlты ′kаныkыт wӓрkа мо̄танты kаныkыт.</ta>
            <ta e="T218" id="Seg_2731" s="T209">уккур тъ̊т чон′тоkыт ′поkынӓ мо̄тты тӱl ‵пурыши′llака аlчинта.</ta>
            <ta e="T220" id="Seg_2732" s="T218">но по[а̊]kтымынта.</ta>
            <ta e="T231" id="Seg_2733" s="T220">мо̄танты k[к]ӓлыт чап оркыl′потыт, ӣрап оlа чаl нʼӧмаl порkы ′лʼипып орkыl′по̄тӓт.</ta>
            <ta e="T237" id="Seg_2734" s="T231">′kапиjа на тӱса о′кошкан[т] ′огмыт мо̄тты.</ta>
            <ta e="T240" id="Seg_2735" s="T237">шʼитъ окошкатъ ′епымпа.</ta>
            <ta e="T244" id="Seg_2736" s="T240">kай ′мӓрkы ′мо̄тты ′епымынта.</ta>
            <ta e="T253" id="Seg_2737" s="T244">′чӯса таkпа, lоkыра полʼ мо̄т ′пичит паkыт пӣры таkпа.</ta>
            <ta e="T258" id="Seg_2738" s="T253">теңды на тӱ̄п на kъ̊лпын′тотыт, мо̄тты.</ta>
            <ta e="T264" id="Seg_2739" s="T258">′пороkсӓ томо′рот ′kамнымпа на нʼӱ̄тыт ′ылʼты. </ta>
            <ta e="T267" id="Seg_2740" s="T264">тӱ на ′kӓттымынтыты. </ta>
            <ta e="T273" id="Seg_2741" s="T267">тинтена ′носсарылʼ kумыl мӱ̄ты тӱ ам′паты.</ta>
            <ta e="T276" id="Seg_2742" s="T273">kоңотыт ин′нӓнылʼе ам′не′ӣты.</ta>
            <ta e="T282" id="Seg_2743" s="T276">′ира понӓ паkтӓлʼе пиче[и]лʼ ′утӓт ′мӓрkа.</ta>
            <ta e="T289" id="Seg_2744" s="T282">исы чап ′понӓ kонтышелʼ′чотыт ай чар kӓт′тотыт.</ta>
            <ta e="T293" id="Seg_2745" s="T289">′амна ′иты тӱп на ‵kӓмын′тотыт.</ta>
            <ta e="T303" id="Seg_2746" s="T293">шо′ңоl поры шʼу иllӓ, ай шʼитъ окошкан ошʼи ′мотти kъ̊′ңоты.</ta>
            <ta e="T306" id="Seg_2747" s="T303">′мо̄тып ′тӱсе чо′тӓjотыт.</ta>
            <ta e="T313" id="Seg_2748" s="T306">на кӓlиl ′мӱ̄тып мӱтыт ол kуп ′шʼитӓ епынтотыт.</ta>
            <ta e="T326" id="Seg_2749" s="T313">ныны kара ′kъ̊нна, kӓ̄лʼаты[ӓ]т kа̄г[ɣ]ланте, kара kай ′kаɣлантӓ kъ̊нна ай шиттъ еннаk[ɣ]ыты kъ̊нытыты.</ta>
            <ta e="T331" id="Seg_2750" s="T326">′шите ′еннаɣыты kъ̊ннытыты, kъ̊тkъ̊[и]лʼбыти kумы′икынты.</ta>
            <ta e="T336" id="Seg_2751" s="T331">′kараны ′kонна мо̄чаl kӓ̄лʼи ира kонты′сента.</ta>
            <ta e="T342" id="Seg_2752" s="T336">kай ′нʼӧмаl порɣай ′ират имап ′kъ̊чисылыт.</ta>
            <ta e="T345" id="Seg_2753" s="T342">′ира ′чары ′kӓттыңыт.</ta>
            <ta e="T348" id="Seg_2754" s="T345">нына ′kара ку′ролʼна.</ta>
            <ta e="T358" id="Seg_2755" s="T348">′но̨̄кыр ′емнасит ′такы kӓ′ннаjот, нӓче лаkаlтеl′чоты, ′kа̄lитыт мотты ′таккы ′kъ̊ннотыт.</ta>
            <ta e="T363" id="Seg_2756" s="T358">kӓlитыт мо̄ты ӣматыт ′иllа kъ̊′ннотыт.</ta>
            <ta e="T369" id="Seg_2757" s="T363">′нымды ′мотkыт шʼолʼkумыl шиты и′jаɣып kъ̊̄чимпотыт.</ta>
            <ta e="T374" id="Seg_2758" s="T369">томо′роk нʼӧмаl ′порkай ′ират kъ̊̄т′сатkы.</ta>
            <ta e="T380" id="Seg_2759" s="T374">′намын и′jаɣы ′отап токколʼӓ kоннӓ kъ̊н′нотыт.</ta>
            <ta e="T382" id="Seg_2760" s="T380">′kонна ‵мо̄тыl′но̄тыт.</ta>
            <ta e="T385" id="Seg_2761" s="T382">уккур пит шʼӓ̄kо̄тыт.</ta>
            <ta e="T390" id="Seg_2762" s="T385">‵иjа′иты тӓпа чʼелʼ моkонӓ ′тӱ̄ңотӓт.</ta>
            <ta e="T395" id="Seg_2763" s="T390">ъ̊сы и′рамты ′kъ̊нталʼа тӱңотӓт иjаиты.</ta>
            <ta e="T401" id="Seg_2764" s="T395">тӓпыт нилʼчик тӓмир′потӓт. мат ‵ӓсамыт kъ̊т′потыт.</ta>
            <ta e="T407" id="Seg_2765" s="T401">тəп селʼдʼ[ч]и ӣjа′ты ′е̨ппынды, ′kыпа ′емнӓты.</ta>
            <ta e="T410" id="Seg_2766" s="T407">тӓпыт ′kӓнталʼа та′тотыт.</ta>
            <ta e="T414" id="Seg_2767" s="T410">′ӣjанты канап ′поɣот соры[о]мпотын.</ta>
            <ta e="T420" id="Seg_2768" s="T414">′о̨тан ‵ӓ̄тымаптаɣыт тӓпыт ка′нан ′топып ‵чӣɣылʼ′нотын.</ta>
            <ta e="T427" id="Seg_2769" s="T420">′ира ′ниlчик kъ̊тыңыты. ′kа̄нат ′топып kай чӣɣылныты.</ta>
            <ta e="T432" id="Seg_2770" s="T427">кыпа иjанты ′има пона ′паkта.</ta>
            <ta e="T436" id="Seg_2771" s="T432">′наkай онд̂ӓ ′ираты ′тӱнты.</ta>
            <ta e="T440" id="Seg_2772" s="T436">на kай ′онды ′ӣjаты тӱ̄мпа.</ta>
            <ta e="T448" id="Seg_2773" s="T440">ныны ′иlчатыт ниlчик ‵kъ̊ттым′паты, ′монты о̄ныт тӱ̄са чо̄ты′сымыт.</ta>
            <ta e="T453" id="Seg_2774" s="T448">′kӓ̄лʼий ′тъ̊ттан ′о̨̄тап ′ненна ′токыңыты.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T3" id="Seg_2775" s="T0">nʼömalʼ porqɨ ira.</ta>
            <ta e="T9" id="Seg_2776" s="T3">selʼči ijatɨ ɛppändɨ, aj selʼči amnätɨ.</ta>
            <ta e="T16" id="Seg_2777" s="T9">qaːlʼit tüŋotɨt takkɨn[n]ɨ [taqqɨndɨ] nʼannä täpɨp qətqɨntɨtqo.</ta>
            <ta e="T21" id="Seg_2778" s="T16">qumijtɨ ijaitɨ maːčontɨ qənpotɨt tä pelʼlʼe.</ta>
            <ta e="T24" id="Seg_2779" s="T21">qup tiː qəttiŋɨt.</ta>
            <ta e="T26" id="Seg_2780" s="T24">quttar eŋa.</ta>
            <ta e="T30" id="Seg_2781" s="T26">köt šolʼlʼi qumɨlʼ ijaqɨp märɨmpotɨt.</ta>
            <ta e="T34" id="Seg_2782" s="T30">na qäːlʼit, namɨp[m] üːtɨnd̂ɨt.</ta>
            <ta e="T41" id="Seg_2783" s="T34">nʼömalʼ porqɨ iratqine me telɨmɨta tüntomɨt.</ta>
            <ta e="T44" id="Seg_2784" s="T41">konninnɨlʼɨt [koninnɨt] əːtɨtqo.</ta>
            <ta e="T53" id="Seg_2785" s="T44">ijaqi moqonä qəlʼlʼe niːk qətɨŋɨt. čilʼälʼ ijaqij, qə[ä]lʼɨqi mɨrɨŋɨt.</ta>
            <ta e="T58" id="Seg_2786" s="T53">nɨnɨ šittelʼ irra kuče qatenta.</ta>
            <ta e="T70" id="Seg_2787" s="T58">ira nilʼčik essa, na nʼömalʼ porqə ira amnäntɨlʼmɨtkiːne. üːr apsɨ tattɨŋɨt.</ta>
            <ta e="T74" id="Seg_2788" s="T70">kun montɨlʼ qaj tattɨŋɨt.</ta>
            <ta e="T78" id="Seg_2789" s="T74">käːlʼit telʼe tüntotɨt, amɨrtentotɨt.</ta>
            <ta e="T81" id="Seg_2790" s="T78">ira niːk qəːtɨt.</ta>
            <ta e="T83" id="Seg_2791" s="T81">apsɨp tattotɨt.</ta>
            <ta e="T89" id="Seg_2792" s="T83">tam[p] moːtɨt šunʼčip šentɨ nʼüːtɨsä toqkotɨt.</ta>
            <ta e="T92" id="Seg_2793" s="T89">ira qälʼimtɨ apsɨtɨkolʼamnɨt.</ta>
            <ta e="T99" id="Seg_2794" s="T93">toːptɨlʼ čelʼɨ qäːlʼɨt najep tatɨntotɨt.</ta>
            <ta e="T102" id="Seg_2795" s="T99">nossarɨlʼ qumɨlʼ müːtɨ.</ta>
            <ta e="T110" id="Seg_2796" s="T102">qapija šunʼčipɨlʼ moːtqɨt nʼomalʼ porqɨ ira ontɨkɨt amneintɨsä</ta>
            <ta e="T114" id="Seg_2797" s="T110">šentɨ nʼütɨsa toqɨralʼtɨt [toqɨralʼimpatɨt].</ta>
            <ta e="T117" id="Seg_2798" s="T114">ira nilʼčik äːsa.</ta>
            <ta e="T118" id="Seg_2799" s="T117">amɨrkolʼamnotɨt.</ta>
            <ta e="T120" id="Seg_2800" s="T118">apsɨp qontotɨt.</ta>
            <ta e="T123" id="Seg_2801" s="T120">ira niːk qəttɨŋɨt.</ta>
            <ta e="T126" id="Seg_2802" s="T123">imaqotatɨ aj eːŋa.</ta>
            <ta e="T129" id="Seg_2803" s="T126">amneimtɨ niːk kuralʼtɨt.</ta>
            <ta e="T131" id="Seg_2804" s="T129">ponä tantɨŋɨlʼɨt.</ta>
            <ta e="T134" id="Seg_2805" s="T131">ijatɨp pona tattɨŋɨlɨt.</ta>
            <ta e="T139" id="Seg_2806" s="T134">ira ontɨ v[m]är[ə]qa kumiːmtɨ apsɨtɨmpɨlʼe.</ta>
            <ta e="T145" id="Seg_2807" s="T139">ukkur tət čontoqɨt tüntə qarret qup.</ta>
            <ta e="T146" id="Seg_2808" s="T145">amɨrnot.</ta>
            <ta e="T153" id="Seg_2809" s="T146">š[s]oŋolʼilʼ [šoŋolʼ] tümtɨ nam märɨq poːsa səlɨŋɨt.</ta>
            <ta e="T154" id="Seg_2810" s="T153">čoːpɨnta.</ta>
            <ta e="T160" id="Seg_2811" s="T154">amneimtɨ aša qaː [qajqo] pona üteŋɨt.</ta>
            <ta e="T163" id="Seg_2812" s="T160">poqɨt tüː čoteŋtotet.</ta>
            <ta e="T168" id="Seg_2813" s="T163">tüp čoːtelpotet tintena tə[ö]p, purɨšiŋolʼpotɨt.</ta>
            <ta e="T171" id="Seg_2814" s="T168">nilʼčik qaj təkɨrɨmpoːtɨt.</ta>
            <ta e="T177" id="Seg_2815" s="T171">te na entɨlʼ manna tannentak.</ta>
            <ta e="T178" id="Seg_2816" s="T177">amɨrnotɨt.</ta>
            <ta e="T187" id="Seg_2817" s="T178">qäːlʼit niːk qətotät, ilʼča, šoŋolʼaj qajetqe našak posä säːlal.</ta>
            <ta e="T193" id="Seg_2818" s="T187">aša šoŋol mɨ tüna amtɨt.</ta>
            <ta e="T196" id="Seg_2819" s="T193">täm aš amtɨt.</ta>
            <ta e="T202" id="Seg_2820" s="T196">niːk märqe[a]ptɨmpa, nʼomalʼ porqɨntɨ raːšalʼ lʼiːpɨ.</ta>
            <ta e="T209" id="Seg_2821" s="T202">nammɨ šerpɨntɨt šoŋolʼtɨ qanɨqɨt wärqa moːtantɨ qanɨqɨt.</ta>
            <ta e="T218" id="Seg_2822" s="T209">ukkur tət čontoqɨt poqɨnä moːttɨ tülʼ purɨšilʼlʼaka alʼčinta.</ta>
            <ta e="T220" id="Seg_2823" s="T218">no po[a]qtɨmɨnta.</ta>
            <ta e="T231" id="Seg_2824" s="T220">moːtantɨ q[k]älɨt čap orkɨlʼpotɨt, iːrap olʼa čalʼ nʼömalʼ porqɨ lʼipɨp orqɨlʼpoːtät.</ta>
            <ta e="T237" id="Seg_2825" s="T231">qapija na tüsa okoškan[t] ogmɨt moːttɨ.</ta>
            <ta e="T240" id="Seg_2826" s="T237">šitə okoškatə epɨmpa.</ta>
            <ta e="T244" id="Seg_2827" s="T240">qaj märqɨ moːttɨ epɨmɨnta.</ta>
            <ta e="T253" id="Seg_2828" s="T244">čuːsa taqpa, lʼoqɨra polʼ moːt pičit paqɨt piːrɨ taqpa.</ta>
            <ta e="T258" id="Seg_2829" s="T253">teŋdɨ na tüːp na qəlpɨntotɨt, moːttɨ.</ta>
            <ta e="T264" id="Seg_2830" s="T258">poroqsä tomorot qamnɨmpa na nʼüːtɨt ɨlʼtɨ.</ta>
            <ta e="T267" id="Seg_2831" s="T264">tü na qättɨmɨntɨtɨ.</ta>
            <ta e="T273" id="Seg_2832" s="T267">tintena nossarɨlʼ qumɨlʼ müːtɨ tü ampatɨ.</ta>
            <ta e="T276" id="Seg_2833" s="T273">qoŋotɨt innänɨlʼe amneiːtɨ.</ta>
            <ta e="T282" id="Seg_2834" s="T276">ira ponä paqtälʼe piče[i]lʼ utät märqa.</ta>
            <ta e="T289" id="Seg_2835" s="T282">isɨ čap ponä qontɨšelʼčotɨt aj čar qättotɨt.</ta>
            <ta e="T293" id="Seg_2836" s="T289">amna itɨ tüp na qämɨntotɨt.</ta>
            <ta e="T303" id="Seg_2837" s="T293">šoŋolʼ porɨ šu ilʼlʼä, aj šitə okoškan oši motti qəŋotɨ.</ta>
            <ta e="T306" id="Seg_2838" s="T303">moːtɨp tüse čotäjotɨt.</ta>
            <ta e="T313" id="Seg_2839" s="T306">na kälʼilʼ müːtɨp mütɨt ol qup šitä epɨntotɨt.</ta>
            <ta e="T326" id="Seg_2840" s="T313">nɨnɨ qara qənna, qäːlʼatɨ[ä]t qaːg[q]lante, qara qaj qaqlantä qənna aj šittə ennaq[q]ɨtɨ qənɨtɨtɨ.</ta>
            <ta e="T331" id="Seg_2841" s="T326">šite ennaqɨtɨ qənnɨtɨtɨ, qətqə[i]lʼpɨti qumɨikɨntɨ.</ta>
            <ta e="T336" id="Seg_2842" s="T331">qaranɨ qonna moːčalʼ qäːlʼi ira qontɨsenta.</ta>
            <ta e="T342" id="Seg_2843" s="T336">qaj nʼömalʼ porqaj irat imap qəčisɨlɨt.</ta>
            <ta e="T345" id="Seg_2844" s="T342">ira čarɨ qättɨŋɨt.</ta>
            <ta e="T348" id="Seg_2845" s="T345">nɨna qara kurolʼna.</ta>
            <ta e="T358" id="Seg_2846" s="T348">noːkɨr emnasit takɨ qännajot, näče laqalʼtelʼčotɨ, qaːlʼitɨt mottɨ takkɨ qənnotɨt.</ta>
            <ta e="T363" id="Seg_2847" s="T358">qälʼitɨt moːtɨ iːmatɨt ilʼlʼa qənnotɨt.</ta>
            <ta e="T369" id="Seg_2848" s="T363">nɨmdɨ motqɨt šolʼqumɨlʼ šitɨ ijaqɨp qəːčimpotɨt.</ta>
            <ta e="T374" id="Seg_2849" s="T369">tomoroq nʼömalʼ porqaj irat qəːtsatqɨ.</ta>
            <ta e="T380" id="Seg_2850" s="T374">namɨn ijaqɨ otap tokkolʼä qonnä qənnotɨt.</ta>
            <ta e="T382" id="Seg_2851" s="T380">qonna moːtɨlʼnoːtɨt.</ta>
            <ta e="T385" id="Seg_2852" s="T382">ukkur pit šäːqoːtɨt.</ta>
            <ta e="T390" id="Seg_2853" s="T385">ijaitɨ täpa čʼelʼ moqonä tüːŋotät.</ta>
            <ta e="T395" id="Seg_2854" s="T390">əsɨ iramtɨ qəntalʼa tüŋotät ijaitɨ.</ta>
            <ta e="T401" id="Seg_2855" s="T395">täpɨt nilʼčik tämirpotät. mat äsamɨt qətpotɨt.</ta>
            <ta e="T407" id="Seg_2856" s="T401">təp selʼdʼ[č]i iːjatɨ eppɨndɨ, qɨpa emnätɨ.</ta>
            <ta e="T410" id="Seg_2857" s="T407">täpɨt qäntalʼa tatotɨt.</ta>
            <ta e="T414" id="Seg_2858" s="T410">iːjantɨ kanap poqot sorɨ[o]mpotɨn.</ta>
            <ta e="T420" id="Seg_2859" s="T414">otan äːtɨmaptaqɨt täpɨt kanan topɨp čiːqɨlʼnotɨn.</ta>
            <ta e="T427" id="Seg_2860" s="T420">ira nilʼčik qətɨŋɨtɨ. qaːnat topɨp qaj čiːqɨlnɨtɨ.</ta>
            <ta e="T432" id="Seg_2861" s="T427">kɨpa ijantɨ ima pona paqta.</ta>
            <ta e="T436" id="Seg_2862" s="T432">naqaj ond̂ä iratɨ tüntɨ.</ta>
            <ta e="T440" id="Seg_2863" s="T436">na qaj ondɨ iːjatɨ tüːmpa.</ta>
            <ta e="T448" id="Seg_2864" s="T440">nɨnɨ ilʼčatɨt nilʼčik qəttɨmpatɨ, montɨ oːnɨt tüːsa čoːtɨsɨmɨt.</ta>
            <ta e="T453" id="Seg_2865" s="T448">qäːlʼij təttan oːtap nenna tokɨŋɨtɨ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_2866" s="T0">Nʼomalʼ porqɨ ira. </ta>
            <ta e="T9" id="Seg_2867" s="T3">Seːlʼčʼi ijatɨ ɛːppäntɨ, aj seːlʼčʼi amnätɨ. </ta>
            <ta e="T16" id="Seg_2868" s="T9">Qaːlit tüŋɔːtɨt takkɨnɨ (/takkɨntɨ) nʼannä täpɨp qətqɨntɨtqo. </ta>
            <ta e="T21" id="Seg_2869" s="T16">Qumijtɨ ijaiːtɨ mačʼontɨ qənpɔːtɨt täpälʼlʼä. </ta>
            <ta e="T24" id="Seg_2870" s="T21">Qup tiː qəttiŋɨt. </ta>
            <ta e="T26" id="Seg_2871" s="T24">Kuttar ɛːŋa? </ta>
            <ta e="T30" id="Seg_2872" s="T26">Köt šolʼlʼiqumɨlʼ ijakɨp märɨmpɔːtɨt. </ta>
            <ta e="T34" id="Seg_2873" s="T30">Na qälit namɨp üːtɨntɨt. </ta>
            <ta e="T41" id="Seg_2874" s="T34">Nʼomalʼ porqɨ iratkine meː täːlɨ mɨta tüntɔːmɨt. </ta>
            <ta e="T44" id="Seg_2875" s="T41">Qonninɨlɨt (/qonninɨt) əːtɨtqo. </ta>
            <ta e="T53" id="Seg_2876" s="T44">Ijaqı moqonä qəlʼlʼä nık kətɨŋɨt: "Čʼilʼälʼ ijaqıj qälɨqqı mɨrɨŋɨt." </ta>
            <ta e="T58" id="Seg_2877" s="T53">Nɨːnɨ šittelʼ irra kučʼe qatɛnta? </ta>
            <ta e="T70" id="Seg_2878" s="T58">Ira nılʼčʼik ɛssa, na nʼomalʼ porqə ira amnäntɨlʼ mɨtkine: Ür, apsɨ tattɨŋɨt. </ta>
            <ta e="T74" id="Seg_2879" s="T70">Kun montɨlʼ qaj tattɨŋɨt. </ta>
            <ta e="T78" id="Seg_2880" s="T74">Qälit täːlʼe tüntɔːtɨt, amɨrtɛntɔːtɨt. </ta>
            <ta e="T81" id="Seg_2881" s="T78">Ira nık kətɨt. </ta>
            <ta e="T83" id="Seg_2882" s="T81">Apsɨp tattɔːtɨt. </ta>
            <ta e="T89" id="Seg_2883" s="T83">Tam mɔːtɨt šünʼčʼip šentɨ nʼuːtɨsä tɔːqqɔːtɨt. </ta>
            <ta e="T92" id="Seg_2884" s="T89">Ira qälimtɨ apsɨtɨqolamnɨt. </ta>
            <ta e="T93" id="Seg_2885" s="T92">Əː! </ta>
            <ta e="T99" id="Seg_2886" s="T93">Tɔːptɨlʼ čʼeːlʼɨ qälɨt na jep tatɨntɔːtɨt. </ta>
            <ta e="T102" id="Seg_2887" s="T99">Nɔːssarɨlʼ qumɨlʼ mütɨ. </ta>
            <ta e="T110" id="Seg_2888" s="T102">Qapija šünʼčʼipɨlʼ mɔːtqɨt nʼomalʼ porqɨ ira ontɨqɨt amnäiːntɨsä. </ta>
            <ta e="T114" id="Seg_2889" s="T110">Šentɨ nʼuːtɨsa tɔːqɨralʼtɨt (/tɔːqɨralʼimpɔːtɨt). </ta>
            <ta e="T117" id="Seg_2890" s="T114">Ira nılʼčʼik ɛsa. </ta>
            <ta e="T118" id="Seg_2891" s="T117">Amɨrqolamnɔːtɨt. </ta>
            <ta e="T120" id="Seg_2892" s="T118">Apsɨp qontɔːtɨt. </ta>
            <ta e="T123" id="Seg_2893" s="T120">Ira nık kəttɨŋɨt. </ta>
            <ta e="T126" id="Seg_2894" s="T123">Imaqotatɨ aj ɛːŋa. </ta>
            <ta e="T129" id="Seg_2895" s="T126">Amnäiːmtɨ nık kuraltɨt. </ta>
            <ta e="T131" id="Seg_2896" s="T129">Ponä tantɨŋɨlɨt. </ta>
            <ta e="T134" id="Seg_2897" s="T131">Ijatɨp pona tattɨŋɨlɨt. </ta>
            <ta e="T139" id="Seg_2898" s="T134">Ira ontɨ wärəkka qumimtɨ apsɨtɨmpɨlʼä. </ta>
            <ta e="T145" id="Seg_2899" s="T139">Ukkur tət čʼontoːqɨt tüntə qaret qup. </ta>
            <ta e="T146" id="Seg_2900" s="T145">Amɨrnɔːt. </ta>
            <ta e="T153" id="Seg_2901" s="T146">Šoŋolʼilʼ (/šoŋolʼ) tümtɨ nam märɨq poːsa səlɨŋɨt. </ta>
            <ta e="T154" id="Seg_2902" s="T153">Čʼɔːpɨnta. </ta>
            <ta e="T160" id="Seg_2903" s="T154">Amnäiːmtɨ aša qaː (/qajqo) pona üːteŋɨt. </ta>
            <ta e="T163" id="Seg_2904" s="T160">Poːqɨt tü čʼɔːtentɔːtet. </ta>
            <ta e="T168" id="Seg_2905" s="T163">Tüp čʼɔːtälpɔːtet, tintena töp purɨšiŋɔːlpɔːtɨt. </ta>
            <ta e="T171" id="Seg_2906" s="T168">Nılʼčʼik qaj təːkɨrɨmpɔːtɨt. </ta>
            <ta e="T177" id="Seg_2907" s="T171">Tɛː na ɛːntɨlʼ man na tannɛntak. </ta>
            <ta e="T178" id="Seg_2908" s="T177">Amɨrnɔːtɨt. </ta>
            <ta e="T187" id="Seg_2909" s="T178">Qälit nık kətɔːtät: Ilʼčʼa, šoŋolʼaj qajetqe našak poːsä säːlal. </ta>
            <ta e="T193" id="Seg_2910" s="T187">Aša šoŋol mɨ tü na amtɨt? </ta>
            <ta e="T196" id="Seg_2911" s="T193">Täm aš amtɨt. </ta>
            <ta e="T202" id="Seg_2912" s="T196">Nık märkeptɨmpa nʼomalʼ porqɨntɨ rašalʼ lʼıːpɨ. </ta>
            <ta e="T209" id="Seg_2913" s="T202">Nammɨ šeːrpɨntɨt šoŋoltɨ qanɨqqɨt wärqa mɔːtantɨ qanɨqqɨt. </ta>
            <ta e="T218" id="Seg_2914" s="T209">Ukkur tət čʼontoːqɨt poːqɨnä mɔːttɨ tülʼ purɨšilʼ laka alʼčʼinta. </ta>
            <ta e="T220" id="Seg_2915" s="T218">No paqtɨmmɨnta. </ta>
            <ta e="T231" id="Seg_2916" s="T220">Mɔːtantɨ qälɨt čʼap orqɨlpɔːtɨt irap, ola čʼalʼ nʼomalʼ porqɨ lʼıːpɨp orqɨlpɔːtät. </ta>
            <ta e="T237" id="Seg_2917" s="T231">Qapija na tüsa okoškan ɔːkmɨt mɔːttɨ. </ta>
            <ta e="T240" id="Seg_2918" s="T237">Šitə okoškatə ɛːpɨmpa. </ta>
            <ta e="T244" id="Seg_2919" s="T240">Qaj märqɨ mɔːttɨ ɛːpɨmmɨnta! </ta>
            <ta e="T253" id="Seg_2920" s="T244">Tüsa taqpa, loːqɨra poːlʼ moːt, pičʼit paqɨt piːrɨ taqpa. </ta>
            <ta e="T258" id="Seg_2921" s="T253">Teŋtɨna tüːp na qəːlpɨntɔːtɨt, mɔːttɨ. </ta>
            <ta e="T264" id="Seg_2922" s="T258">Poroqsä tomorot qamnɨmpa na nʼuːtɨt ɨːlʼtɨ. </ta>
            <ta e="T267" id="Seg_2923" s="T264">Tü na qättɨmmɨntɨtɨ. </ta>
            <ta e="T273" id="Seg_2924" s="T267">Tıntena nɔːssarɨlʼ qumɨlʼ mütɨ tü ampatɨ. </ta>
            <ta e="T276" id="Seg_2925" s="T273">Qonŋɔːtɨt ınnänɨlʼe amnäiːtɨ. </ta>
            <ta e="T282" id="Seg_2926" s="T276">Ira ponä paktälʼä pičʼilʼ utät märka. </ta>
            <ta e="T289" id="Seg_2927" s="T282">Isɨ čʼap ponä qontɨšɛlʼčʼɔːtɨt aj čʼar qättɔːtɨt. </ta>
            <ta e="T293" id="Seg_2928" s="T289">Amnaiːtɨ tüp na qämmɨntɔːtɨt. </ta>
            <ta e="T303" id="Seg_2929" s="T293">Šoŋol pɔːrɨ šuː ıllä, aj šitə okoškan ɔːkti mɔːtti qəːŋɔːtɨ. </ta>
            <ta e="T306" id="Seg_2930" s="T303">Mɔːtɨp tüsä čʼɔːtäjɔːtɨt. </ta>
            <ta e="T313" id="Seg_2931" s="T306">Na qälilʼ mütɨp (/mütɨt) olqup šitä ɛːpɨntɔːtɨt. </ta>
            <ta e="T326" id="Seg_2932" s="T313">Nɨnɨ kara qənna, qälʼatɨt qaqlante, kara qaj qaqlantä qənna aj šittə ännaqıtɨ qənnɨtɨtɨ. </ta>
            <ta e="T331" id="Seg_2933" s="T326">Šite ännaqıtɨ qənnɨtɨtɨ, qətqilʼpɨti qumɨiːqɨntɨ. </ta>
            <ta e="T336" id="Seg_2934" s="T331">Karanɨ qonna, moːčʼalʼ qälʼi ira qontɨssɛːnta. </ta>
            <ta e="T342" id="Seg_2935" s="T336">Qaj nʼomalʼ porqaj irat imap qəːčʼisɨlɨt. </ta>
            <ta e="T345" id="Seg_2936" s="T342">Ira čʼarrɨ qättɨŋɨt. </ta>
            <ta e="T348" id="Seg_2937" s="T345">Nɨːna kara kurolʼna. </ta>
            <ta e="T358" id="Seg_2938" s="T348">Nɔːkɨr ämnasit takɨ qännajɔːt, näčʼä laqaltɛlʼčʼɔːtɨ, qalitɨt mɔːttɨ takkɨ qənnɔːtɨt. </ta>
            <ta e="T363" id="Seg_2939" s="T358">Qälitɨt mɔːttɨ imatɨt ılla qənnɔːtɨt. </ta>
            <ta e="T369" id="Seg_2940" s="T363">Nɨmtɨ mɔːtqɨt šolʼqumɨlʼ šitɨ ijaqıp qəːčʼimpɔːtɨt. </ta>
            <ta e="T374" id="Seg_2941" s="T369">Tomorot nʼomalʼ porqaj irat kətsatqı. </ta>
            <ta e="T380" id="Seg_2942" s="T374">Namɨn ijaqı, ɔːtap tɔːqqolʼä konnä qənnɔːtɨt. </ta>
            <ta e="T382" id="Seg_2943" s="T380">Konna mɔːtɨlnɔːtɨt. </ta>
            <ta e="T385" id="Seg_2944" s="T382">Ukkur pit šäqɔːtɨt. </ta>
            <ta e="T390" id="Seg_2945" s="T385">Ijaiːtɨ täpa čʼeːlʼ moqonä tüŋɔːtät. </ta>
            <ta e="T395" id="Seg_2946" s="T390">Əsɨ iramtɨ qəntalʼa tüŋɔːtät ijaiːtɨ. </ta>
            <ta e="T401" id="Seg_2947" s="T395">Täpɨt nılʼčʼik tɛnirpɔːtät: Mat äsamɨt qətpɔːtɨt. </ta>
            <ta e="T407" id="Seg_2948" s="T401">Təp seːlʼčʼi iːjatɨ ɛːppɨntɨ, kɨpa ämnätɨ. </ta>
            <ta e="T410" id="Seg_2949" s="T407">Täpɨt qäntalʼa taːtɔːtɨt. </ta>
            <ta e="T414" id="Seg_2950" s="T410">Iːjantɨ kanap poːqot sɔːrɨmpɔːtɨn. </ta>
            <ta e="T420" id="Seg_2951" s="T414">Ɔːtan ätɨmaptaqɨt täpɨt kanan topɨp čʼıqɨlʼnɔːtɨn. </ta>
            <ta e="T427" id="Seg_2952" s="T420">Ira nılʼčʼik kətɨŋɨtɨ: Kanat topɨp qaj čʼıqɨlnɨtɨ? </ta>
            <ta e="T432" id="Seg_2953" s="T427">Kɨpa ijantɨ ima pona pakta. </ta>
            <ta e="T436" id="Seg_2954" s="T432">Naqaj ontä iratɨ tüntɨ. </ta>
            <ta e="T440" id="Seg_2955" s="T436">Naqaj ontɨ iːjatɨ tümpa. </ta>
            <ta e="T448" id="Seg_2956" s="T440">Nɨːnɨ ilʼčʼatɨt nılʼčʼik kəttɨmpatɨ: Montɨ onɨt tüsa čʼɔːtɨsɨmɨt. </ta>
            <ta e="T453" id="Seg_2957" s="T448">Qälʼij təttan ɔːtap nʼenna tɔːqɨŋɨtɨ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_2958" s="T0">nʼoma-lʼ</ta>
            <ta e="T2" id="Seg_2959" s="T1">porqɨ</ta>
            <ta e="T3" id="Seg_2960" s="T2">ira</ta>
            <ta e="T4" id="Seg_2961" s="T3">seːlʼčʼi</ta>
            <ta e="T5" id="Seg_2962" s="T4">ija-tɨ</ta>
            <ta e="T6" id="Seg_2963" s="T5">ɛː-ppä-ntɨ</ta>
            <ta e="T7" id="Seg_2964" s="T6">aj</ta>
            <ta e="T8" id="Seg_2965" s="T7">seːlʼčʼi</ta>
            <ta e="T9" id="Seg_2966" s="T8">amnä-tɨ</ta>
            <ta e="T10" id="Seg_2967" s="T9">qaːli-t</ta>
            <ta e="T11" id="Seg_2968" s="T10">tü-ŋɔː-tɨt</ta>
            <ta e="T12" id="Seg_2969" s="T11">takkɨ-nɨ</ta>
            <ta e="T13" id="Seg_2970" s="T12">takkɨ-ntɨ</ta>
            <ta e="T14" id="Seg_2971" s="T13">nʼannä</ta>
            <ta e="T15" id="Seg_2972" s="T14">täp-ɨ-p</ta>
            <ta e="T16" id="Seg_2973" s="T15">qət-qɨntɨtqo</ta>
            <ta e="T17" id="Seg_2974" s="T16">qum-ij-tɨ</ta>
            <ta e="T18" id="Seg_2975" s="T17">ija-iː-tɨ</ta>
            <ta e="T19" id="Seg_2976" s="T18">mačʼo-ntɨ</ta>
            <ta e="T20" id="Seg_2977" s="T19">qən-pɔː-tɨt</ta>
            <ta e="T21" id="Seg_2978" s="T20">täpä-lʼ-lʼä</ta>
            <ta e="T22" id="Seg_2979" s="T21">qup</ta>
            <ta e="T23" id="Seg_2980" s="T22">tiː</ta>
            <ta e="T24" id="Seg_2981" s="T23">qət-ti-ŋɨ-t</ta>
            <ta e="T25" id="Seg_2982" s="T24">kuttar</ta>
            <ta e="T26" id="Seg_2983" s="T25">ɛː-ŋa</ta>
            <ta e="T27" id="Seg_2984" s="T26">köt</ta>
            <ta e="T28" id="Seg_2985" s="T27">šolʼlʼiqum-ɨ-lʼ</ta>
            <ta e="T29" id="Seg_2986" s="T28">ija-kɨ-p</ta>
            <ta e="T30" id="Seg_2987" s="T29">märɨ-mpɔː-tɨt</ta>
            <ta e="T31" id="Seg_2988" s="T30">na</ta>
            <ta e="T32" id="Seg_2989" s="T31">qäli-t</ta>
            <ta e="T33" id="Seg_2990" s="T32">namɨ-p</ta>
            <ta e="T34" id="Seg_2991" s="T33">üːtɨ-ntɨ-t</ta>
            <ta e="T35" id="Seg_2992" s="T34">nʼoma-lʼ</ta>
            <ta e="T36" id="Seg_2993" s="T35">porqɨ</ta>
            <ta e="T37" id="Seg_2994" s="T36">ira-tkine</ta>
            <ta e="T38" id="Seg_2995" s="T37">meː</ta>
            <ta e="T39" id="Seg_2996" s="T38">täːlɨ</ta>
            <ta e="T40" id="Seg_2997" s="T39">mɨta</ta>
            <ta e="T41" id="Seg_2998" s="T40">tü-ntɔː-mɨt</ta>
            <ta e="T42" id="Seg_2999" s="T41">qon-ni-nɨlɨt</ta>
            <ta e="T43" id="Seg_3000" s="T42">qon-ni-nɨt</ta>
            <ta e="T44" id="Seg_3001" s="T43">əːtɨ-tqo</ta>
            <ta e="T45" id="Seg_3002" s="T44">ija-qı</ta>
            <ta e="T46" id="Seg_3003" s="T45">moqonä</ta>
            <ta e="T47" id="Seg_3004" s="T46">qəlʼ-lʼä</ta>
            <ta e="T48" id="Seg_3005" s="T47">nık</ta>
            <ta e="T49" id="Seg_3006" s="T48">kətɨ-ŋɨ-t</ta>
            <ta e="T50" id="Seg_3007" s="T49">čʼilʼä-lʼ</ta>
            <ta e="T51" id="Seg_3008" s="T50">ija-qı-j</ta>
            <ta e="T52" id="Seg_3009" s="T51">qälɨq-qı</ta>
            <ta e="T53" id="Seg_3010" s="T52">mɨrɨ-ŋɨ-t</ta>
            <ta e="T54" id="Seg_3011" s="T53">nɨːnɨ</ta>
            <ta e="T55" id="Seg_3012" s="T54">šitte-lʼ</ta>
            <ta e="T56" id="Seg_3013" s="T55">irra</ta>
            <ta e="T57" id="Seg_3014" s="T56">kučʼe</ta>
            <ta e="T58" id="Seg_3015" s="T57">qat-ɛnta</ta>
            <ta e="T59" id="Seg_3016" s="T58">ira</ta>
            <ta e="T60" id="Seg_3017" s="T59">nılʼčʼi-k</ta>
            <ta e="T61" id="Seg_3018" s="T60">ɛssa</ta>
            <ta e="T62" id="Seg_3019" s="T61">na</ta>
            <ta e="T63" id="Seg_3020" s="T62">nʼoma-lʼ</ta>
            <ta e="T64" id="Seg_3021" s="T63">porqə</ta>
            <ta e="T65" id="Seg_3022" s="T64">ira</ta>
            <ta e="T66" id="Seg_3023" s="T65">amnä-ntɨ-lʼ</ta>
            <ta e="T67" id="Seg_3024" s="T66">mɨ-tkine</ta>
            <ta e="T68" id="Seg_3025" s="T67">ür</ta>
            <ta e="T69" id="Seg_3026" s="T68">apsɨ</ta>
            <ta e="T70" id="Seg_3027" s="T69">tattɨ-ŋɨt</ta>
            <ta e="T71" id="Seg_3028" s="T70">kun</ta>
            <ta e="T72" id="Seg_3029" s="T71">montɨ-lʼ</ta>
            <ta e="T73" id="Seg_3030" s="T72">qaj</ta>
            <ta e="T74" id="Seg_3031" s="T73">tattɨ-ŋɨt</ta>
            <ta e="T75" id="Seg_3032" s="T74">qäli-t</ta>
            <ta e="T76" id="Seg_3033" s="T75">täːlʼe</ta>
            <ta e="T77" id="Seg_3034" s="T76">tü-ntɔː-tɨt</ta>
            <ta e="T78" id="Seg_3035" s="T77">am-ɨ-r-tɛntɔː-tɨt</ta>
            <ta e="T79" id="Seg_3036" s="T78">ira</ta>
            <ta e="T80" id="Seg_3037" s="T79">nık</ta>
            <ta e="T81" id="Seg_3038" s="T80">kətɨ-t</ta>
            <ta e="T82" id="Seg_3039" s="T81">apsɨ-p</ta>
            <ta e="T83" id="Seg_3040" s="T82">tattɔː-tɨt</ta>
            <ta e="T84" id="Seg_3041" s="T83">tam</ta>
            <ta e="T85" id="Seg_3042" s="T84">mɔːt-ɨ-t</ta>
            <ta e="T86" id="Seg_3043" s="T85">šünʼčʼi-p</ta>
            <ta e="T87" id="Seg_3044" s="T86">šentɨ</ta>
            <ta e="T88" id="Seg_3045" s="T87">nʼuːtɨ-sä</ta>
            <ta e="T89" id="Seg_3046" s="T88">tɔːqqɔː-tɨt</ta>
            <ta e="T90" id="Seg_3047" s="T89">ira</ta>
            <ta e="T91" id="Seg_3048" s="T90">qäli-m-tɨ</ta>
            <ta e="T92" id="Seg_3049" s="T91">apsɨ-tɨ-q-olam-nɨ-t</ta>
            <ta e="T93" id="Seg_3050" s="T92">əː</ta>
            <ta e="T94" id="Seg_3051" s="T93">tɔːptɨlʼ</ta>
            <ta e="T95" id="Seg_3052" s="T94">čʼeːlʼɨ</ta>
            <ta e="T96" id="Seg_3053" s="T95">qälɨ-t</ta>
            <ta e="T97" id="Seg_3054" s="T96">na</ta>
            <ta e="T98" id="Seg_3055" s="T97">jep</ta>
            <ta e="T99" id="Seg_3056" s="T98">tatɨ-ntɔː-tɨt</ta>
            <ta e="T100" id="Seg_3057" s="T99">nɔːs-sar-ɨ-lʼ</ta>
            <ta e="T101" id="Seg_3058" s="T100">qum-ɨ-lʼ</ta>
            <ta e="T102" id="Seg_3059" s="T101">mütɨ</ta>
            <ta e="T103" id="Seg_3060" s="T102">qapija</ta>
            <ta e="T104" id="Seg_3061" s="T103">šünʼčʼi-pɨlʼ</ta>
            <ta e="T105" id="Seg_3062" s="T104">mɔːt-qɨt</ta>
            <ta e="T106" id="Seg_3063" s="T105">nʼoma-lʼ</ta>
            <ta e="T107" id="Seg_3064" s="T106">porqɨ</ta>
            <ta e="T108" id="Seg_3065" s="T107">ira</ta>
            <ta e="T109" id="Seg_3066" s="T108">ontɨ-qɨt</ta>
            <ta e="T110" id="Seg_3067" s="T109">amnä-iː-ntɨ-sä</ta>
            <ta e="T111" id="Seg_3068" s="T110">šentɨ</ta>
            <ta e="T112" id="Seg_3069" s="T111">nʼuːtɨ-sa</ta>
            <ta e="T113" id="Seg_3070" s="T112">tɔːqɨ-r-alʼ-tɨt</ta>
            <ta e="T114" id="Seg_3071" s="T113">tɔːqɨ-r-alʼ-i-mpɔː-tɨt</ta>
            <ta e="T115" id="Seg_3072" s="T114">ira</ta>
            <ta e="T116" id="Seg_3073" s="T115">nılʼčʼi-k</ta>
            <ta e="T117" id="Seg_3074" s="T116">ɛsa</ta>
            <ta e="T118" id="Seg_3075" s="T117">am-ɨ-r-q-olam-nɔː-tɨt</ta>
            <ta e="T119" id="Seg_3076" s="T118">apsɨ-p</ta>
            <ta e="T120" id="Seg_3077" s="T119">qo-ntɔː-tɨt</ta>
            <ta e="T121" id="Seg_3078" s="T120">ira</ta>
            <ta e="T122" id="Seg_3079" s="T121">nık</ta>
            <ta e="T123" id="Seg_3080" s="T122">kəttɨ-ŋɨ-t</ta>
            <ta e="T124" id="Seg_3081" s="T123">imaqota-tɨ</ta>
            <ta e="T125" id="Seg_3082" s="T124">aj</ta>
            <ta e="T126" id="Seg_3083" s="T125">ɛː-ŋa</ta>
            <ta e="T127" id="Seg_3084" s="T126">amnä-iː-m-tɨ</ta>
            <ta e="T128" id="Seg_3085" s="T127">nık</ta>
            <ta e="T129" id="Seg_3086" s="T128">kur-altɨ-t</ta>
            <ta e="T130" id="Seg_3087" s="T129">ponä</ta>
            <ta e="T131" id="Seg_3088" s="T130">tantɨ-ŋɨlɨt</ta>
            <ta e="T132" id="Seg_3089" s="T131">ija-t-ɨ-p</ta>
            <ta e="T133" id="Seg_3090" s="T132">pona</ta>
            <ta e="T134" id="Seg_3091" s="T133">tattɨ-ŋɨlɨt</ta>
            <ta e="T135" id="Seg_3092" s="T134">ira</ta>
            <ta e="T136" id="Seg_3093" s="T135">ontɨ</ta>
            <ta e="T137" id="Seg_3094" s="T136">wärə-kka</ta>
            <ta e="T138" id="Seg_3095" s="T137">qum-i-m-tɨ</ta>
            <ta e="T139" id="Seg_3096" s="T138">apsɨ-tɨ-mpɨ-lʼä</ta>
            <ta e="T140" id="Seg_3097" s="T139">ukkur</ta>
            <ta e="T141" id="Seg_3098" s="T140">tət</ta>
            <ta e="T142" id="Seg_3099" s="T141">čʼontoː-qɨt</ta>
            <ta e="T143" id="Seg_3100" s="T142">tü-ntə</ta>
            <ta e="T144" id="Seg_3101" s="T143">qare-t</ta>
            <ta e="T145" id="Seg_3102" s="T144">qup</ta>
            <ta e="T146" id="Seg_3103" s="T145">am-ɨ-r-nɔː-t</ta>
            <ta e="T147" id="Seg_3104" s="T146">šoŋolʼ-i-lʼ</ta>
            <ta e="T148" id="Seg_3105" s="T147">šoŋolʼ</ta>
            <ta e="T149" id="Seg_3106" s="T148">tü-m-tɨ</ta>
            <ta e="T150" id="Seg_3107" s="T149">na-m</ta>
            <ta e="T151" id="Seg_3108" s="T150">märɨq</ta>
            <ta e="T152" id="Seg_3109" s="T151">poː-sa</ta>
            <ta e="T153" id="Seg_3110" s="T152">səlɨ-ŋɨ-t</ta>
            <ta e="T154" id="Seg_3111" s="T153">čʼɔːpɨ-nta</ta>
            <ta e="T155" id="Seg_3112" s="T154">amnä-iː-m-tɨ</ta>
            <ta e="T156" id="Seg_3113" s="T155">aša</ta>
            <ta e="T157" id="Seg_3114" s="T156">qaː</ta>
            <ta e="T158" id="Seg_3115" s="T157">qaj-qo</ta>
            <ta e="T159" id="Seg_3116" s="T158">pona</ta>
            <ta e="T160" id="Seg_3117" s="T159">üːte-ŋɨ-t</ta>
            <ta e="T161" id="Seg_3118" s="T160">poː-qɨt</ta>
            <ta e="T162" id="Seg_3119" s="T161">tü</ta>
            <ta e="T163" id="Seg_3120" s="T162">čʼɔːte-ntɔː-tet</ta>
            <ta e="T164" id="Seg_3121" s="T163">tü-p</ta>
            <ta e="T165" id="Seg_3122" s="T164">čʼɔːt-äl-pɔː-tet</ta>
            <ta e="T166" id="Seg_3123" s="T165">tintena</ta>
            <ta e="T167" id="Seg_3124" s="T166">tö-p</ta>
            <ta e="T168" id="Seg_3125" s="T167">purɨš-i-ŋ-ɔːl-pɔː-tɨt</ta>
            <ta e="T169" id="Seg_3126" s="T168">nılʼčʼi-k</ta>
            <ta e="T170" id="Seg_3127" s="T169">qaj</ta>
            <ta e="T171" id="Seg_3128" s="T170">təːkɨ-rɨ-mpɔː-tɨt</ta>
            <ta e="T172" id="Seg_3129" s="T171">tɛː</ta>
            <ta e="T173" id="Seg_3130" s="T172">na</ta>
            <ta e="T174" id="Seg_3131" s="T173">ɛː-ntɨlʼ</ta>
            <ta e="T175" id="Seg_3132" s="T174">man</ta>
            <ta e="T176" id="Seg_3133" s="T175">na</ta>
            <ta e="T177" id="Seg_3134" s="T176">tann-ɛnta-k</ta>
            <ta e="T178" id="Seg_3135" s="T177">am-ɨ-r-nɔː-tɨt</ta>
            <ta e="T179" id="Seg_3136" s="T178">qäli-t</ta>
            <ta e="T180" id="Seg_3137" s="T179">nık</ta>
            <ta e="T181" id="Seg_3138" s="T180">kətɔː-tät</ta>
            <ta e="T182" id="Seg_3139" s="T181">ilʼčʼa</ta>
            <ta e="T183" id="Seg_3140" s="T182">šoŋolʼ-a-j</ta>
            <ta e="T184" id="Seg_3141" s="T183">qaj-e-tqe</ta>
            <ta e="T185" id="Seg_3142" s="T184">našak</ta>
            <ta e="T186" id="Seg_3143" s="T185">poː-sä</ta>
            <ta e="T187" id="Seg_3144" s="T186">säːla-l</ta>
            <ta e="T188" id="Seg_3145" s="T187">aša</ta>
            <ta e="T189" id="Seg_3146" s="T188">šoŋol</ta>
            <ta e="T190" id="Seg_3147" s="T189">mɨ</ta>
            <ta e="T191" id="Seg_3148" s="T190">tü</ta>
            <ta e="T192" id="Seg_3149" s="T191">na</ta>
            <ta e="T193" id="Seg_3150" s="T192">am-tɨ-t</ta>
            <ta e="T194" id="Seg_3151" s="T193">täm</ta>
            <ta e="T195" id="Seg_3152" s="T194">aš</ta>
            <ta e="T196" id="Seg_3153" s="T195">am-tɨ-t</ta>
            <ta e="T197" id="Seg_3154" s="T196">nık</ta>
            <ta e="T198" id="Seg_3155" s="T197">märke-ptɨ-mpa</ta>
            <ta e="T199" id="Seg_3156" s="T198">nʼoma-lʼ</ta>
            <ta e="T200" id="Seg_3157" s="T199">porqɨ-n-tɨ</ta>
            <ta e="T201" id="Seg_3158" s="T200">raša-lʼ</ta>
            <ta e="T202" id="Seg_3159" s="T201">lʼıːpɨ</ta>
            <ta e="T203" id="Seg_3160" s="T202">nammɨ</ta>
            <ta e="T204" id="Seg_3161" s="T203">šeːr-pɨ-ntɨ-t</ta>
            <ta e="T205" id="Seg_3162" s="T204">šoŋol-tɨ</ta>
            <ta e="T206" id="Seg_3163" s="T205">qanɨq-qɨt</ta>
            <ta e="T207" id="Seg_3164" s="T206">wärqa</ta>
            <ta e="T208" id="Seg_3165" s="T207">mɔːta-n-tɨ</ta>
            <ta e="T209" id="Seg_3166" s="T208">qanɨq-qɨt</ta>
            <ta e="T210" id="Seg_3167" s="T209">ukkur</ta>
            <ta e="T211" id="Seg_3168" s="T210">tət</ta>
            <ta e="T212" id="Seg_3169" s="T211">čʼontoː-qɨt</ta>
            <ta e="T213" id="Seg_3170" s="T212">poː-qɨnä</ta>
            <ta e="T214" id="Seg_3171" s="T213">mɔːt-tɨ</ta>
            <ta e="T215" id="Seg_3172" s="T214">tü-lʼ</ta>
            <ta e="T216" id="Seg_3173" s="T215">purɨš-i-lʼ</ta>
            <ta e="T217" id="Seg_3174" s="T216">laka</ta>
            <ta e="T218" id="Seg_3175" s="T217">alʼčʼi-nta</ta>
            <ta e="T219" id="Seg_3176" s="T218">no</ta>
            <ta e="T220" id="Seg_3177" s="T219">paqtɨ-mmɨ-nta</ta>
            <ta e="T221" id="Seg_3178" s="T220">mɔːta-ntɨ</ta>
            <ta e="T222" id="Seg_3179" s="T221">qälɨ-t</ta>
            <ta e="T223" id="Seg_3180" s="T222">čʼap</ta>
            <ta e="T224" id="Seg_3181" s="T223">orqɨl-pɔː-tɨt</ta>
            <ta e="T225" id="Seg_3182" s="T224">ira-p</ta>
            <ta e="T226" id="Seg_3183" s="T225">ola</ta>
            <ta e="T227" id="Seg_3184" s="T226">čʼalʼ</ta>
            <ta e="T228" id="Seg_3185" s="T227">nʼoma-lʼ</ta>
            <ta e="T229" id="Seg_3186" s="T228">porqɨ</ta>
            <ta e="T230" id="Seg_3187" s="T229">lʼıːpɨ-p</ta>
            <ta e="T231" id="Seg_3188" s="T230">orqɨl-pɔː-tät</ta>
            <ta e="T232" id="Seg_3189" s="T231">qapija</ta>
            <ta e="T233" id="Seg_3190" s="T232">na</ta>
            <ta e="T234" id="Seg_3191" s="T233">tü-sa</ta>
            <ta e="T235" id="Seg_3192" s="T234">okoška-n</ta>
            <ta e="T236" id="Seg_3193" s="T235">ɔːk-mɨt</ta>
            <ta e="T237" id="Seg_3194" s="T236">mɔːt-tɨ</ta>
            <ta e="T238" id="Seg_3195" s="T237">šitə</ta>
            <ta e="T239" id="Seg_3196" s="T238">okoška-tə</ta>
            <ta e="T240" id="Seg_3197" s="T239">ɛː-pɨ-mpa</ta>
            <ta e="T241" id="Seg_3198" s="T240">qaj</ta>
            <ta e="T242" id="Seg_3199" s="T241">märqɨ</ta>
            <ta e="T243" id="Seg_3200" s="T242">mɔːt-tɨ</ta>
            <ta e="T244" id="Seg_3201" s="T243">ɛː-pɨ-mmɨ-nta</ta>
            <ta e="T245" id="Seg_3202" s="T244">tü-sa</ta>
            <ta e="T246" id="Seg_3203" s="T245">taq-pa</ta>
            <ta e="T247" id="Seg_3204" s="T246">loːq-ɨ-ra</ta>
            <ta e="T248" id="Seg_3205" s="T247">poː-lʼ</ta>
            <ta e="T249" id="Seg_3206" s="T248">moː-t</ta>
            <ta e="T250" id="Seg_3207" s="T249">pičʼi-t</ta>
            <ta e="T251" id="Seg_3208" s="T250">paqɨ-t</ta>
            <ta e="T252" id="Seg_3209" s="T251">piːrɨ</ta>
            <ta e="T253" id="Seg_3210" s="T252">taq-pa</ta>
            <ta e="T254" id="Seg_3211" s="T253">teŋtɨna</ta>
            <ta e="T255" id="Seg_3212" s="T254">tü-p</ta>
            <ta e="T256" id="Seg_3213" s="T255">na</ta>
            <ta e="T257" id="Seg_3214" s="T256">qəː-l-pɨ-ntɔː-tɨt</ta>
            <ta e="T258" id="Seg_3215" s="T257">mɔːt-tɨ</ta>
            <ta e="T259" id="Seg_3216" s="T258">poroq-sä</ta>
            <ta e="T260" id="Seg_3217" s="T259">tomorot</ta>
            <ta e="T261" id="Seg_3218" s="T260">qam-nɨ-mpa</ta>
            <ta e="T262" id="Seg_3219" s="T261">na</ta>
            <ta e="T263" id="Seg_3220" s="T262">nʼuːtɨ-t</ta>
            <ta e="T264" id="Seg_3221" s="T263">ɨːlʼ-tɨ</ta>
            <ta e="T265" id="Seg_3222" s="T264">tü</ta>
            <ta e="T266" id="Seg_3223" s="T265">na</ta>
            <ta e="T267" id="Seg_3224" s="T266">qättɨ-mmɨ-ntɨ-tɨ</ta>
            <ta e="T268" id="Seg_3225" s="T267">tıntena</ta>
            <ta e="T269" id="Seg_3226" s="T268">nɔːs-sar-ɨ-lʼ</ta>
            <ta e="T270" id="Seg_3227" s="T269">qum-ɨ-lʼ</ta>
            <ta e="T271" id="Seg_3228" s="T270">mütɨ</ta>
            <ta e="T272" id="Seg_3229" s="T271">tü</ta>
            <ta e="T273" id="Seg_3230" s="T272">am-pa-tɨ</ta>
            <ta e="T274" id="Seg_3231" s="T273">qon-ŋɔː-tɨt</ta>
            <ta e="T275" id="Seg_3232" s="T274">ınnä-nɨ-lʼe</ta>
            <ta e="T276" id="Seg_3233" s="T275">amnä-iː-tɨ</ta>
            <ta e="T277" id="Seg_3234" s="T276">ira</ta>
            <ta e="T278" id="Seg_3235" s="T277">ponä</ta>
            <ta e="T279" id="Seg_3236" s="T278">paktä-lʼä</ta>
            <ta e="T280" id="Seg_3237" s="T279">pičʼi-lʼ</ta>
            <ta e="T281" id="Seg_3238" s="T280">utä-t</ta>
            <ta e="T282" id="Seg_3239" s="T281">mär-ka</ta>
            <ta e="T283" id="Seg_3240" s="T282">isɨ</ta>
            <ta e="T284" id="Seg_3241" s="T283">čʼap</ta>
            <ta e="T285" id="Seg_3242" s="T284">ponä</ta>
            <ta e="T286" id="Seg_3243" s="T285">qontɨ-š-ɛlʼčʼɔː-tɨt</ta>
            <ta e="T287" id="Seg_3244" s="T286">aj</ta>
            <ta e="T288" id="Seg_3245" s="T287">čʼar</ta>
            <ta e="T289" id="Seg_3246" s="T288">qättɔː-tɨt</ta>
            <ta e="T290" id="Seg_3247" s="T289">amna-iː-tɨ</ta>
            <ta e="T291" id="Seg_3248" s="T290">tü-p</ta>
            <ta e="T292" id="Seg_3249" s="T291">na</ta>
            <ta e="T293" id="Seg_3250" s="T292">qäm-mɨ-ntɔː-tɨt</ta>
            <ta e="T294" id="Seg_3251" s="T293">šoŋol</ta>
            <ta e="T295" id="Seg_3252" s="T294">pɔːrɨ</ta>
            <ta e="T296" id="Seg_3253" s="T295">šuː</ta>
            <ta e="T297" id="Seg_3254" s="T296">ıllä</ta>
            <ta e="T298" id="Seg_3255" s="T297">aj</ta>
            <ta e="T299" id="Seg_3256" s="T298">šitə</ta>
            <ta e="T300" id="Seg_3257" s="T299">okoška-n</ta>
            <ta e="T301" id="Seg_3258" s="T300">ɔːk-ti</ta>
            <ta e="T302" id="Seg_3259" s="T301">mɔːt-ti</ta>
            <ta e="T303" id="Seg_3260" s="T302">qəː-ŋɔː-tɨ</ta>
            <ta e="T304" id="Seg_3261" s="T303">mɔːt-ɨ-p</ta>
            <ta e="T305" id="Seg_3262" s="T304">tü-sä</ta>
            <ta e="T306" id="Seg_3263" s="T305">čʼɔːtä-jɔː-tɨt</ta>
            <ta e="T307" id="Seg_3264" s="T306">na</ta>
            <ta e="T308" id="Seg_3265" s="T307">qäli-lʼ</ta>
            <ta e="T309" id="Seg_3266" s="T308">mütɨ-p</ta>
            <ta e="T310" id="Seg_3267" s="T309">mütɨ-t</ta>
            <ta e="T311" id="Seg_3268" s="T310">ol-qup</ta>
            <ta e="T312" id="Seg_3269" s="T311">šitä</ta>
            <ta e="T313" id="Seg_3270" s="T312">ɛː-pɨ-ntɔː-tɨt</ta>
            <ta e="T314" id="Seg_3271" s="T313">nɨnɨ</ta>
            <ta e="T315" id="Seg_3272" s="T314">kara</ta>
            <ta e="T316" id="Seg_3273" s="T315">qən-na</ta>
            <ta e="T317" id="Seg_3274" s="T316">qälʼa-t-ɨ-t</ta>
            <ta e="T318" id="Seg_3275" s="T317">qaqla-nte</ta>
            <ta e="T319" id="Seg_3276" s="T318">kara</ta>
            <ta e="T320" id="Seg_3277" s="T319">qaj</ta>
            <ta e="T321" id="Seg_3278" s="T320">qaqla-ntä</ta>
            <ta e="T322" id="Seg_3279" s="T321">qən-na</ta>
            <ta e="T323" id="Seg_3280" s="T322">aj</ta>
            <ta e="T324" id="Seg_3281" s="T323">šittə</ta>
            <ta e="T325" id="Seg_3282" s="T324">änna-qı-tɨ</ta>
            <ta e="T326" id="Seg_3283" s="T325">qən-nɨ-tɨ-tɨ</ta>
            <ta e="T327" id="Seg_3284" s="T326">šite</ta>
            <ta e="T328" id="Seg_3285" s="T327">änna-qı-tɨ</ta>
            <ta e="T329" id="Seg_3286" s="T328">qən-nɨ-tɨ-tɨ</ta>
            <ta e="T330" id="Seg_3287" s="T329">qət-qilʼ-pɨ-ti</ta>
            <ta e="T331" id="Seg_3288" s="T330">qum-ɨ-iː-qɨn-tɨ</ta>
            <ta e="T332" id="Seg_3289" s="T331">kara-nɨ</ta>
            <ta e="T333" id="Seg_3290" s="T332">qon-na</ta>
            <ta e="T334" id="Seg_3291" s="T333">moːčʼ-alʼ</ta>
            <ta e="T454" id="Seg_3292" s="T334">qälʼi</ta>
            <ta e="T335" id="Seg_3293" s="T454">ira</ta>
            <ta e="T336" id="Seg_3294" s="T335">qontɨ-ss-ɛː-nta</ta>
            <ta e="T337" id="Seg_3295" s="T336">qaj</ta>
            <ta e="T338" id="Seg_3296" s="T337">nʼoma-lʼ</ta>
            <ta e="T339" id="Seg_3297" s="T338">porqa-j</ta>
            <ta e="T340" id="Seg_3298" s="T339">ira-t</ta>
            <ta e="T341" id="Seg_3299" s="T340">ima-p</ta>
            <ta e="T342" id="Seg_3300" s="T341">qəːčʼi-s-ɨ-lɨ-t</ta>
            <ta e="T343" id="Seg_3301" s="T342">ira</ta>
            <ta e="T344" id="Seg_3302" s="T343">čʼarrɨ</ta>
            <ta e="T345" id="Seg_3303" s="T344">qättɨ-ŋɨ-t</ta>
            <ta e="T346" id="Seg_3304" s="T345">nɨːna</ta>
            <ta e="T347" id="Seg_3305" s="T346">kara</ta>
            <ta e="T348" id="Seg_3306" s="T347">kur-olʼ-na</ta>
            <ta e="T349" id="Seg_3307" s="T348">nɔːkɨr</ta>
            <ta e="T350" id="Seg_3308" s="T349">ämna-si-t</ta>
            <ta e="T351" id="Seg_3309" s="T350">takɨ</ta>
            <ta e="T352" id="Seg_3310" s="T351">qän-na-jɔː-t</ta>
            <ta e="T353" id="Seg_3311" s="T352">näčʼä</ta>
            <ta e="T354" id="Seg_3312" s="T353">laqalt-ɛlʼčʼɔː-tɨ</ta>
            <ta e="T355" id="Seg_3313" s="T354">qali-t-ɨ-t</ta>
            <ta e="T356" id="Seg_3314" s="T355">mɔːt-tɨ</ta>
            <ta e="T357" id="Seg_3315" s="T356">takkɨ</ta>
            <ta e="T358" id="Seg_3316" s="T357">qən-nɔː-tɨt</ta>
            <ta e="T359" id="Seg_3317" s="T358">qäli-t-ɨ-t</ta>
            <ta e="T360" id="Seg_3318" s="T359">mɔːt-tɨ</ta>
            <ta e="T361" id="Seg_3319" s="T360">ima-tɨt</ta>
            <ta e="T362" id="Seg_3320" s="T361">ılla</ta>
            <ta e="T363" id="Seg_3321" s="T362">qən-nɔː-tɨt</ta>
            <ta e="T364" id="Seg_3322" s="T363">nɨmtɨ</ta>
            <ta e="T365" id="Seg_3323" s="T364">mɔːt-qɨt</ta>
            <ta e="T366" id="Seg_3324" s="T365">šolʼqum-ɨ-lʼ</ta>
            <ta e="T367" id="Seg_3325" s="T366">šitɨ</ta>
            <ta e="T368" id="Seg_3326" s="T367">ija-qı-p</ta>
            <ta e="T369" id="Seg_3327" s="T368">qəːčʼi-mpɔː-tɨt</ta>
            <ta e="T370" id="Seg_3328" s="T369">tomorot</ta>
            <ta e="T371" id="Seg_3329" s="T370">nʼoma-lʼ</ta>
            <ta e="T372" id="Seg_3330" s="T371">porqa-j</ta>
            <ta e="T373" id="Seg_3331" s="T372">ira-t</ta>
            <ta e="T374" id="Seg_3332" s="T373">kətsat-qı</ta>
            <ta e="T375" id="Seg_3333" s="T374">namɨ-n</ta>
            <ta e="T376" id="Seg_3334" s="T375">ija-qı</ta>
            <ta e="T377" id="Seg_3335" s="T376">ɔːta-p</ta>
            <ta e="T378" id="Seg_3336" s="T377">tɔːqqo-lʼä</ta>
            <ta e="T379" id="Seg_3337" s="T378">konnä</ta>
            <ta e="T380" id="Seg_3338" s="T379">qən-nɔː-tɨt</ta>
            <ta e="T381" id="Seg_3339" s="T380">konna</ta>
            <ta e="T382" id="Seg_3340" s="T381">mɔːt-ɨl-nɔː-tɨt</ta>
            <ta e="T383" id="Seg_3341" s="T382">ukkur</ta>
            <ta e="T384" id="Seg_3342" s="T383">pi-t</ta>
            <ta e="T385" id="Seg_3343" s="T384">šäqɔː-tɨt</ta>
            <ta e="T386" id="Seg_3344" s="T385">ija-iː-tɨ</ta>
            <ta e="T387" id="Seg_3345" s="T386">täpa</ta>
            <ta e="T388" id="Seg_3346" s="T387">čʼeːlʼ</ta>
            <ta e="T389" id="Seg_3347" s="T388">moqonä</ta>
            <ta e="T390" id="Seg_3348" s="T389">tü-ŋɔː-tät</ta>
            <ta e="T391" id="Seg_3349" s="T390">əsɨ</ta>
            <ta e="T392" id="Seg_3350" s="T391">ira-m-tɨ</ta>
            <ta e="T393" id="Seg_3351" s="T392">qən-ta-lʼa</ta>
            <ta e="T394" id="Seg_3352" s="T393">tü-ŋɔː-tät</ta>
            <ta e="T395" id="Seg_3353" s="T394">ija-iː-tɨ</ta>
            <ta e="T396" id="Seg_3354" s="T395">täp-ɨ-t</ta>
            <ta e="T397" id="Seg_3355" s="T396">nılʼčʼi-k</ta>
            <ta e="T398" id="Seg_3356" s="T397">tɛni-r-pɔː-tät</ta>
            <ta e="T399" id="Seg_3357" s="T398">Mat</ta>
            <ta e="T400" id="Seg_3358" s="T399">äsa-mɨt</ta>
            <ta e="T401" id="Seg_3359" s="T400">qət-pɔː-tɨt</ta>
            <ta e="T402" id="Seg_3360" s="T401">təp</ta>
            <ta e="T403" id="Seg_3361" s="T402">seːlʼčʼi</ta>
            <ta e="T404" id="Seg_3362" s="T403">iːja-tɨ</ta>
            <ta e="T405" id="Seg_3363" s="T404">ɛː-ppɨ-ntɨ</ta>
            <ta e="T406" id="Seg_3364" s="T405">kɨpa</ta>
            <ta e="T407" id="Seg_3365" s="T406">ämnä-tɨ</ta>
            <ta e="T408" id="Seg_3366" s="T407">täp-ɨ-t</ta>
            <ta e="T409" id="Seg_3367" s="T408">qän-ta-lʼa</ta>
            <ta e="T410" id="Seg_3368" s="T409">taːtɔː-tɨt</ta>
            <ta e="T411" id="Seg_3369" s="T410">iːja-n-tɨ</ta>
            <ta e="T412" id="Seg_3370" s="T411">kana-p</ta>
            <ta e="T413" id="Seg_3371" s="T412">poː-qot</ta>
            <ta e="T414" id="Seg_3372" s="T413">sɔːrɨ-mpɔː-tɨn</ta>
            <ta e="T415" id="Seg_3373" s="T414">ɔːta-n</ta>
            <ta e="T416" id="Seg_3374" s="T415">ätɨ-ma-pta-qɨt</ta>
            <ta e="T417" id="Seg_3375" s="T416">täp-ɨ-t</ta>
            <ta e="T418" id="Seg_3376" s="T417">kana-n</ta>
            <ta e="T419" id="Seg_3377" s="T418">topɨ-p</ta>
            <ta e="T420" id="Seg_3378" s="T419">čʼıqɨlʼ-nɔː-tɨn</ta>
            <ta e="T421" id="Seg_3379" s="T420">ira</ta>
            <ta e="T422" id="Seg_3380" s="T421">nılʼčʼi-k</ta>
            <ta e="T423" id="Seg_3381" s="T422">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T424" id="Seg_3382" s="T423">kana-t</ta>
            <ta e="T425" id="Seg_3383" s="T424">topɨ-p</ta>
            <ta e="T426" id="Seg_3384" s="T425">qaj</ta>
            <ta e="T427" id="Seg_3385" s="T426">čʼıqɨl-nɨ-tɨ</ta>
            <ta e="T428" id="Seg_3386" s="T427">kɨpa</ta>
            <ta e="T429" id="Seg_3387" s="T428">ija-n-tɨ</ta>
            <ta e="T430" id="Seg_3388" s="T429">ima</ta>
            <ta e="T431" id="Seg_3389" s="T430">pona</ta>
            <ta e="T432" id="Seg_3390" s="T431">pakta</ta>
            <ta e="T433" id="Seg_3391" s="T432">naqaj</ta>
            <ta e="T434" id="Seg_3392" s="T433">ontä</ta>
            <ta e="T435" id="Seg_3393" s="T434">ira-tɨ</ta>
            <ta e="T436" id="Seg_3394" s="T435">tü-ntɨ</ta>
            <ta e="T437" id="Seg_3395" s="T436">naqaj</ta>
            <ta e="T438" id="Seg_3396" s="T437">ontɨ</ta>
            <ta e="T439" id="Seg_3397" s="T438">iːja-tɨ</ta>
            <ta e="T440" id="Seg_3398" s="T439">tü-mpa</ta>
            <ta e="T441" id="Seg_3399" s="T440">nɨːnɨ</ta>
            <ta e="T442" id="Seg_3400" s="T441">ilʼčʼa-tɨt</ta>
            <ta e="T443" id="Seg_3401" s="T442">nılʼčʼi-k</ta>
            <ta e="T444" id="Seg_3402" s="T443">kəttɨ-mpa-tɨ</ta>
            <ta e="T445" id="Seg_3403" s="T444">montɨ</ta>
            <ta e="T446" id="Seg_3404" s="T445">onɨt</ta>
            <ta e="T447" id="Seg_3405" s="T446">tü-sa</ta>
            <ta e="T448" id="Seg_3406" s="T447">čʼɔːtɨ-sɨ-mɨt</ta>
            <ta e="T449" id="Seg_3407" s="T448">qälʼi-j</ta>
            <ta e="T450" id="Seg_3408" s="T449">tətta-n</ta>
            <ta e="T451" id="Seg_3409" s="T450">ɔːta-p</ta>
            <ta e="T452" id="Seg_3410" s="T451">nʼenna</ta>
            <ta e="T453" id="Seg_3411" s="T452">tɔːqɨ-ŋɨ-tɨ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_3412" s="T0">nʼoma-lʼ</ta>
            <ta e="T2" id="Seg_3413" s="T1">porqɨ</ta>
            <ta e="T3" id="Seg_3414" s="T2">ira</ta>
            <ta e="T4" id="Seg_3415" s="T3">seːlʼčʼɨ</ta>
            <ta e="T5" id="Seg_3416" s="T4">iːja-tɨ</ta>
            <ta e="T6" id="Seg_3417" s="T5">ɛː-mpɨ-ntɨ</ta>
            <ta e="T7" id="Seg_3418" s="T6">aj</ta>
            <ta e="T8" id="Seg_3419" s="T7">seːlʼčʼɨ</ta>
            <ta e="T9" id="Seg_3420" s="T8">ämnä-tɨ</ta>
            <ta e="T10" id="Seg_3421" s="T9">qälɨk-t</ta>
            <ta e="T11" id="Seg_3422" s="T10">tü-ŋɨ-tɨt</ta>
            <ta e="T12" id="Seg_3423" s="T11">takkɨ-nɨ</ta>
            <ta e="T13" id="Seg_3424" s="T12">takkɨ-ntɨ</ta>
            <ta e="T14" id="Seg_3425" s="T13">nʼennä</ta>
            <ta e="T15" id="Seg_3426" s="T14">təp-ɨ-m</ta>
            <ta e="T16" id="Seg_3427" s="T15">qət-qɨntɨtqo</ta>
            <ta e="T17" id="Seg_3428" s="T16">qum-iː-tɨ</ta>
            <ta e="T18" id="Seg_3429" s="T17">iːja-iː-tɨ</ta>
            <ta e="T19" id="Seg_3430" s="T18">mačʼɨ-ntɨ</ta>
            <ta e="T20" id="Seg_3431" s="T19">qən-mpɨ-tɨt</ta>
            <ta e="T21" id="Seg_3432" s="T20">täpäŋ-š-lä</ta>
            <ta e="T22" id="Seg_3433" s="T21">qum</ta>
            <ta e="T23" id="Seg_3434" s="T22">tiː</ta>
            <ta e="T24" id="Seg_3435" s="T23">qət-tɨ-ŋɨ-tɨ</ta>
            <ta e="T25" id="Seg_3436" s="T24">kuttar</ta>
            <ta e="T26" id="Seg_3437" s="T25">ɛː-ŋɨ</ta>
            <ta e="T27" id="Seg_3438" s="T26">köt</ta>
            <ta e="T28" id="Seg_3439" s="T27">šölʼqum-ɨ-lʼ</ta>
            <ta e="T29" id="Seg_3440" s="T28">iːja-ka-m</ta>
            <ta e="T30" id="Seg_3441" s="T29">wərɨ-mpɨ-tɨt</ta>
            <ta e="T31" id="Seg_3442" s="T30">na</ta>
            <ta e="T32" id="Seg_3443" s="T31">qälɨk-n</ta>
            <ta e="T33" id="Seg_3444" s="T32">na-m</ta>
            <ta e="T34" id="Seg_3445" s="T33">üːtɨ-ntɨ-tɨ</ta>
            <ta e="T35" id="Seg_3446" s="T34">nʼoma-lʼ</ta>
            <ta e="T36" id="Seg_3447" s="T35">porqɨ</ta>
            <ta e="T37" id="Seg_3448" s="T36">ira-nɨŋ</ta>
            <ta e="T38" id="Seg_3449" s="T37">meː</ta>
            <ta e="T39" id="Seg_3450" s="T38">täːlɨ</ta>
            <ta e="T40" id="Seg_3451" s="T39">mɨta</ta>
            <ta e="T41" id="Seg_3452" s="T40">tü-ɛntɨ-mɨt</ta>
            <ta e="T42" id="Seg_3453" s="T41">qən-ntɨ-ŋɨlɨt</ta>
            <ta e="T43" id="Seg_3454" s="T42">qən-ntɨ-ŋɨt</ta>
            <ta e="T44" id="Seg_3455" s="T43">əːtɨ-tqo</ta>
            <ta e="T45" id="Seg_3456" s="T44">iːja-qı</ta>
            <ta e="T46" id="Seg_3457" s="T45">moqɨnä</ta>
            <ta e="T47" id="Seg_3458" s="T46">qən-lä</ta>
            <ta e="T48" id="Seg_3459" s="T47">nık</ta>
            <ta e="T49" id="Seg_3460" s="T48">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T50" id="Seg_3461" s="T49">*čʼilʼa-lʼ</ta>
            <ta e="T51" id="Seg_3462" s="T50">iːja-qı-lʼ</ta>
            <ta e="T52" id="Seg_3463" s="T51">qälɨk-qı</ta>
            <ta e="T53" id="Seg_3464" s="T52">wərɨ-ŋɨ-tɨ</ta>
            <ta e="T54" id="Seg_3465" s="T53">nɨːnɨ</ta>
            <ta e="T55" id="Seg_3466" s="T54">šittɨ-lʼ</ta>
            <ta e="T56" id="Seg_3467" s="T55">ira</ta>
            <ta e="T57" id="Seg_3468" s="T56">kučʼčʼä</ta>
            <ta e="T58" id="Seg_3469" s="T57">qattɨ-ɛntɨ</ta>
            <ta e="T59" id="Seg_3470" s="T58">ira</ta>
            <ta e="T60" id="Seg_3471" s="T59">nılʼčʼɨ-k</ta>
            <ta e="T61" id="Seg_3472" s="T60">ɛsɨ</ta>
            <ta e="T62" id="Seg_3473" s="T61">na</ta>
            <ta e="T63" id="Seg_3474" s="T62">nʼoma-lʼ</ta>
            <ta e="T64" id="Seg_3475" s="T63">porqɨ</ta>
            <ta e="T65" id="Seg_3476" s="T64">ira</ta>
            <ta e="T66" id="Seg_3477" s="T65">ämnä-ntɨ-lʼ</ta>
            <ta e="T67" id="Seg_3478" s="T66">mɨ-nɨŋ</ta>
            <ta e="T68" id="Seg_3479" s="T67">ür</ta>
            <ta e="T69" id="Seg_3480" s="T68">apsɨ</ta>
            <ta e="T70" id="Seg_3481" s="T69">taːtɨ-ŋɨt</ta>
            <ta e="T71" id="Seg_3482" s="T70">kun</ta>
            <ta e="T72" id="Seg_3483" s="T71">muntɨk-lʼ</ta>
            <ta e="T73" id="Seg_3484" s="T72">qaj</ta>
            <ta e="T74" id="Seg_3485" s="T73">taːtɨ-ŋɨt</ta>
            <ta e="T75" id="Seg_3486" s="T74">qälɨk-t</ta>
            <ta e="T76" id="Seg_3487" s="T75">täːlɨ</ta>
            <ta e="T77" id="Seg_3488" s="T76">tü-ɛntɨ-tɨt</ta>
            <ta e="T78" id="Seg_3489" s="T77">am-ɨ-r-ɛntɨ-tɨt</ta>
            <ta e="T79" id="Seg_3490" s="T78">ira</ta>
            <ta e="T80" id="Seg_3491" s="T79">nık</ta>
            <ta e="T81" id="Seg_3492" s="T80">kətɨ-tɨ</ta>
            <ta e="T82" id="Seg_3493" s="T81">apsɨ-m</ta>
            <ta e="T83" id="Seg_3494" s="T82">taːtɨ-tɨt</ta>
            <ta e="T84" id="Seg_3495" s="T83">tam</ta>
            <ta e="T85" id="Seg_3496" s="T84">mɔːt-ɨ-n</ta>
            <ta e="T86" id="Seg_3497" s="T85">šünʼčʼɨ-m</ta>
            <ta e="T87" id="Seg_3498" s="T86">šentɨ</ta>
            <ta e="T88" id="Seg_3499" s="T87">nʼuːtɨ-sä</ta>
            <ta e="T89" id="Seg_3500" s="T88">tɔːqqɨ-tɨt</ta>
            <ta e="T90" id="Seg_3501" s="T89">ira</ta>
            <ta e="T91" id="Seg_3502" s="T90">qälɨk-m-tɨ</ta>
            <ta e="T92" id="Seg_3503" s="T91">apsɨ-tɨ-qo-olam-ŋɨ-tɨ</ta>
            <ta e="T93" id="Seg_3504" s="T92">əː</ta>
            <ta e="T94" id="Seg_3505" s="T93">tɔːptɨlʼ</ta>
            <ta e="T95" id="Seg_3506" s="T94">čʼeːlɨ</ta>
            <ta e="T96" id="Seg_3507" s="T95">qälɨk-t</ta>
            <ta e="T97" id="Seg_3508" s="T96">na</ta>
            <ta e="T98" id="Seg_3509" s="T97">jam</ta>
            <ta e="T99" id="Seg_3510" s="T98">taːtɨ-ntɨ-tɨt</ta>
            <ta e="T100" id="Seg_3511" s="T99">nɔːkɨr-sar-ɨ-lʼ</ta>
            <ta e="T101" id="Seg_3512" s="T100">qum-ɨ-lʼ</ta>
            <ta e="T102" id="Seg_3513" s="T101">mütɨ</ta>
            <ta e="T103" id="Seg_3514" s="T102">qapı</ta>
            <ta e="T104" id="Seg_3515" s="T103">šünʼčʼɨ-mpɨlʼ</ta>
            <ta e="T105" id="Seg_3516" s="T104">mɔːt-qɨn</ta>
            <ta e="T106" id="Seg_3517" s="T105">nʼoma-lʼ</ta>
            <ta e="T107" id="Seg_3518" s="T106">porqɨ</ta>
            <ta e="T108" id="Seg_3519" s="T107">ira</ta>
            <ta e="T109" id="Seg_3520" s="T108">ontɨ-qɨn</ta>
            <ta e="T110" id="Seg_3521" s="T109">ämnä-iː-ntɨ-sä</ta>
            <ta e="T111" id="Seg_3522" s="T110">šentɨ</ta>
            <ta e="T112" id="Seg_3523" s="T111">nʼuːtɨ-sä</ta>
            <ta e="T113" id="Seg_3524" s="T112">tɔːqqɨ-r-olʼ-tɨt</ta>
            <ta e="T114" id="Seg_3525" s="T113">tɔːqqɨ-r-olʼ-ɨ-mpɨ-tɨt</ta>
            <ta e="T115" id="Seg_3526" s="T114">ira</ta>
            <ta e="T116" id="Seg_3527" s="T115">nılʼčʼɨ-k</ta>
            <ta e="T117" id="Seg_3528" s="T116">ɛsɨ</ta>
            <ta e="T118" id="Seg_3529" s="T117">am-ɨ-r-qo-olam-ŋɨ-tɨt</ta>
            <ta e="T119" id="Seg_3530" s="T118">apsɨ-m</ta>
            <ta e="T120" id="Seg_3531" s="T119">qo-ntɨ-tɨt</ta>
            <ta e="T121" id="Seg_3532" s="T120">ira</ta>
            <ta e="T122" id="Seg_3533" s="T121">nık</ta>
            <ta e="T123" id="Seg_3534" s="T122">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T124" id="Seg_3535" s="T123">imaqota-tɨ</ta>
            <ta e="T125" id="Seg_3536" s="T124">aj</ta>
            <ta e="T126" id="Seg_3537" s="T125">ɛː-ŋɨ</ta>
            <ta e="T127" id="Seg_3538" s="T126">ämnä-iː-m-tɨ</ta>
            <ta e="T128" id="Seg_3539" s="T127">nık</ta>
            <ta e="T129" id="Seg_3540" s="T128">*kurɨ-altɨ-tɨ</ta>
            <ta e="T130" id="Seg_3541" s="T129">ponä</ta>
            <ta e="T131" id="Seg_3542" s="T130">tantɨ-ŋɨlɨt</ta>
            <ta e="T132" id="Seg_3543" s="T131">iːja-t-ɨ-m</ta>
            <ta e="T133" id="Seg_3544" s="T132">ponä</ta>
            <ta e="T134" id="Seg_3545" s="T133">taːtɨ-ŋɨlɨt</ta>
            <ta e="T135" id="Seg_3546" s="T134">ira</ta>
            <ta e="T136" id="Seg_3547" s="T135">ontɨ</ta>
            <ta e="T137" id="Seg_3548" s="T136">wərɨ-kkɨ</ta>
            <ta e="T138" id="Seg_3549" s="T137">qum-ɨ-m-tɨ</ta>
            <ta e="T139" id="Seg_3550" s="T138">apsɨ-tɨ-mpɨ-lä</ta>
            <ta e="T140" id="Seg_3551" s="T139">ukkɨr</ta>
            <ta e="T141" id="Seg_3552" s="T140">tɔːt</ta>
            <ta e="T142" id="Seg_3553" s="T141">čʼontɨ-qɨn</ta>
            <ta e="T143" id="Seg_3554" s="T142">tü-ntɨ</ta>
            <ta e="T144" id="Seg_3555" s="T143">qarɨ-n</ta>
            <ta e="T145" id="Seg_3556" s="T144">qum</ta>
            <ta e="T146" id="Seg_3557" s="T145">am-ɨ-r-ŋɨ-tɨt</ta>
            <ta e="T147" id="Seg_3558" s="T146">šoŋol-ɨ-lʼ</ta>
            <ta e="T148" id="Seg_3559" s="T147">šoŋol</ta>
            <ta e="T149" id="Seg_3560" s="T148">tü-m-tɨ</ta>
            <ta e="T150" id="Seg_3561" s="T149">na-m</ta>
            <ta e="T151" id="Seg_3562" s="T150">wərqɨ</ta>
            <ta e="T152" id="Seg_3563" s="T151">poː-sä</ta>
            <ta e="T153" id="Seg_3564" s="T152">saltɨ-ŋɨ-tɨ</ta>
            <ta e="T154" id="Seg_3565" s="T153">čʼɔːpɨ-ntɨ</ta>
            <ta e="T155" id="Seg_3566" s="T154">ämnä-iː-m-tɨ</ta>
            <ta e="T156" id="Seg_3567" s="T155">ašša</ta>
            <ta e="T157" id="Seg_3568" s="T156">qaː</ta>
            <ta e="T158" id="Seg_3569" s="T157">qaj-tqo</ta>
            <ta e="T159" id="Seg_3570" s="T158">ponä</ta>
            <ta e="T160" id="Seg_3571" s="T159">üːtɨ-ŋɨ-tɨ</ta>
            <ta e="T161" id="Seg_3572" s="T160">poː-qɨn</ta>
            <ta e="T162" id="Seg_3573" s="T161">tü</ta>
            <ta e="T163" id="Seg_3574" s="T162">čʼɔːtɨ-ntɨ-tɨt</ta>
            <ta e="T164" id="Seg_3575" s="T163">tü-m</ta>
            <ta e="T165" id="Seg_3576" s="T164">čʼɔːtɨ-äl-mpɨ-tɨt</ta>
            <ta e="T166" id="Seg_3577" s="T165">tına</ta>
            <ta e="T167" id="Seg_3578" s="T166">tö-m</ta>
            <ta e="T168" id="Seg_3579" s="T167">purɨš-ɨ-k-ɔːl-mpɨ-tɨt</ta>
            <ta e="T169" id="Seg_3580" s="T168">nılʼčʼɨ-k</ta>
            <ta e="T170" id="Seg_3581" s="T169">qaj</ta>
            <ta e="T171" id="Seg_3582" s="T170">təːkɨ-rɨ-mpɨ-tɨt</ta>
            <ta e="T172" id="Seg_3583" s="T171">tɛː</ta>
            <ta e="T173" id="Seg_3584" s="T172">na</ta>
            <ta e="T174" id="Seg_3585" s="T173">ɛː-ntɨlʼ</ta>
            <ta e="T175" id="Seg_3586" s="T174">man</ta>
            <ta e="T176" id="Seg_3587" s="T175">na</ta>
            <ta e="T177" id="Seg_3588" s="T176">tantɨ-ɛntɨ-k</ta>
            <ta e="T178" id="Seg_3589" s="T177">am-ɨ-r-ŋɨ-tɨt</ta>
            <ta e="T179" id="Seg_3590" s="T178">qälɨk-t</ta>
            <ta e="T180" id="Seg_3591" s="T179">nık</ta>
            <ta e="T181" id="Seg_3592" s="T180">kətɨ-tɨt</ta>
            <ta e="T182" id="Seg_3593" s="T181">ilʼčʼa</ta>
            <ta e="T183" id="Seg_3594" s="T182">šoŋol-ɨ-lʼ</ta>
            <ta e="T184" id="Seg_3595" s="T183">qaj-ɨ-tqo</ta>
            <ta e="T185" id="Seg_3596" s="T184">naššak</ta>
            <ta e="T186" id="Seg_3597" s="T185">poː-sä</ta>
            <ta e="T187" id="Seg_3598" s="T186">saltɨ-l</ta>
            <ta e="T188" id="Seg_3599" s="T187">ašša</ta>
            <ta e="T189" id="Seg_3600" s="T188">šoŋol</ta>
            <ta e="T190" id="Seg_3601" s="T189">mɨ</ta>
            <ta e="T191" id="Seg_3602" s="T190">tü</ta>
            <ta e="T192" id="Seg_3603" s="T191">na</ta>
            <ta e="T193" id="Seg_3604" s="T192">am-ɛntɨ-tɨ</ta>
            <ta e="T194" id="Seg_3605" s="T193">təp</ta>
            <ta e="T195" id="Seg_3606" s="T194">ašša</ta>
            <ta e="T196" id="Seg_3607" s="T195">am-ɛntɨ-tɨ</ta>
            <ta e="T197" id="Seg_3608" s="T196">nık</ta>
            <ta e="T198" id="Seg_3609" s="T197">wərkɨ-ptɨ-mpɨ</ta>
            <ta e="T199" id="Seg_3610" s="T198">nʼoma-lʼ</ta>
            <ta e="T200" id="Seg_3611" s="T199">porqɨ-n-tɨ</ta>
            <ta e="T201" id="Seg_3612" s="T200">raša-lʼ</ta>
            <ta e="T202" id="Seg_3613" s="T201">lıːpɨ</ta>
            <ta e="T203" id="Seg_3614" s="T202">na</ta>
            <ta e="T204" id="Seg_3615" s="T203">šeːr-mpɨ-ntɨ-tɨ</ta>
            <ta e="T205" id="Seg_3616" s="T204">šoŋol-tɨ</ta>
            <ta e="T206" id="Seg_3617" s="T205">qan-qɨn</ta>
            <ta e="T207" id="Seg_3618" s="T206">wərqɨ</ta>
            <ta e="T208" id="Seg_3619" s="T207">mɔːta-n-tɨ</ta>
            <ta e="T209" id="Seg_3620" s="T208">qan-qɨn</ta>
            <ta e="T210" id="Seg_3621" s="T209">ukkɨr</ta>
            <ta e="T211" id="Seg_3622" s="T210">tɔːt</ta>
            <ta e="T212" id="Seg_3623" s="T211">čʼontɨ-qɨn</ta>
            <ta e="T213" id="Seg_3624" s="T212">poː-qɨnɨ</ta>
            <ta e="T214" id="Seg_3625" s="T213">mɔːt-ntɨ</ta>
            <ta e="T215" id="Seg_3626" s="T214">tü-lʼ</ta>
            <ta e="T216" id="Seg_3627" s="T215">purɨš-ɨ-lʼ</ta>
            <ta e="T217" id="Seg_3628" s="T216">laka</ta>
            <ta e="T218" id="Seg_3629" s="T217">alʼčʼɨ-ntɨ</ta>
            <ta e="T219" id="Seg_3630" s="T218">na</ta>
            <ta e="T220" id="Seg_3631" s="T219">paktɨ-mpɨ-ntɨ</ta>
            <ta e="T221" id="Seg_3632" s="T220">mɔːta-ntɨ</ta>
            <ta e="T222" id="Seg_3633" s="T221">qälɨk-t</ta>
            <ta e="T223" id="Seg_3634" s="T222">čʼam</ta>
            <ta e="T224" id="Seg_3635" s="T223">orqɨl-mpɨ-tɨt</ta>
            <ta e="T225" id="Seg_3636" s="T224">ira-m</ta>
            <ta e="T226" id="Seg_3637" s="T225">olä</ta>
            <ta e="T227" id="Seg_3638" s="T226">čʼaŋalʼ</ta>
            <ta e="T228" id="Seg_3639" s="T227">nʼoma-lʼ</ta>
            <ta e="T229" id="Seg_3640" s="T228">porqɨ</ta>
            <ta e="T230" id="Seg_3641" s="T229">lıːpɨ-m</ta>
            <ta e="T231" id="Seg_3642" s="T230">orqɨl-mpɨ-tɨt</ta>
            <ta e="T232" id="Seg_3643" s="T231">qapı</ta>
            <ta e="T233" id="Seg_3644" s="T232">na</ta>
            <ta e="T234" id="Seg_3645" s="T233">tü-sɨ</ta>
            <ta e="T235" id="Seg_3646" s="T234">okoška-n</ta>
            <ta e="T236" id="Seg_3647" s="T235">ɔːŋ-mɨn</ta>
            <ta e="T237" id="Seg_3648" s="T236">mɔːt-tɨ</ta>
            <ta e="T238" id="Seg_3649" s="T237">šittɨ</ta>
            <ta e="T239" id="Seg_3650" s="T238">okoška-tɨ</ta>
            <ta e="T240" id="Seg_3651" s="T239">ɛː-mpɨ-mpɨ</ta>
            <ta e="T241" id="Seg_3652" s="T240">qaj</ta>
            <ta e="T242" id="Seg_3653" s="T241">wərqɨ</ta>
            <ta e="T243" id="Seg_3654" s="T242">mɔːt-tɨ</ta>
            <ta e="T244" id="Seg_3655" s="T243">ɛː-mpɨ-mpɨ-ntɨ</ta>
            <ta e="T245" id="Seg_3656" s="T244">tü-sä</ta>
            <ta e="T246" id="Seg_3657" s="T245">taqɨ-mpɨ</ta>
            <ta e="T247" id="Seg_3658" s="T246">loːq-ɨ-rɨ</ta>
            <ta e="T248" id="Seg_3659" s="T247">poː-lʼ</ta>
            <ta e="T249" id="Seg_3660" s="T248">moː-t</ta>
            <ta e="T250" id="Seg_3661" s="T249">pičʼɨ-n</ta>
            <ta e="T251" id="Seg_3662" s="T250">paqqɨ-n</ta>
            <ta e="T252" id="Seg_3663" s="T251">piːrɨ</ta>
            <ta e="T253" id="Seg_3664" s="T252">taqɨ-mpɨ</ta>
            <ta e="T254" id="Seg_3665" s="T253">tına</ta>
            <ta e="T255" id="Seg_3666" s="T254">tü-m</ta>
            <ta e="T256" id="Seg_3667" s="T255">na</ta>
            <ta e="T257" id="Seg_3668" s="T256">qəː-ɔːl-mpɨ-ntɨ-tɨt</ta>
            <ta e="T258" id="Seg_3669" s="T257">mɔːt-ntɨ</ta>
            <ta e="T259" id="Seg_3670" s="T258">poroq-sä</ta>
            <ta e="T260" id="Seg_3671" s="T259">tomoron</ta>
            <ta e="T261" id="Seg_3672" s="T260">*qam-ntɨ-mpɨ</ta>
            <ta e="T262" id="Seg_3673" s="T261">na</ta>
            <ta e="T263" id="Seg_3674" s="T262">nʼuːtɨ-n</ta>
            <ta e="T264" id="Seg_3675" s="T263">ɨːlɨ-ntɨ</ta>
            <ta e="T265" id="Seg_3676" s="T264">tü</ta>
            <ta e="T266" id="Seg_3677" s="T265">na</ta>
            <ta e="T267" id="Seg_3678" s="T266">qättɨ-mpɨ-ntɨ-tɨ</ta>
            <ta e="T268" id="Seg_3679" s="T267">tına</ta>
            <ta e="T269" id="Seg_3680" s="T268">nɔːkɨr-sar-ɨ-lʼ</ta>
            <ta e="T270" id="Seg_3681" s="T269">qum-ɨ-lʼ</ta>
            <ta e="T271" id="Seg_3682" s="T270">mütɨ</ta>
            <ta e="T272" id="Seg_3683" s="T271">tü</ta>
            <ta e="T273" id="Seg_3684" s="T272">am-mpɨ-tɨ</ta>
            <ta e="T274" id="Seg_3685" s="T273">qən-ŋɨ-tɨt</ta>
            <ta e="T275" id="Seg_3686" s="T274">ınnä-nɨ-lɔːqɨ</ta>
            <ta e="T276" id="Seg_3687" s="T275">ämnä-iː-tɨ</ta>
            <ta e="T277" id="Seg_3688" s="T276">ira</ta>
            <ta e="T278" id="Seg_3689" s="T277">ponä</ta>
            <ta e="T279" id="Seg_3690" s="T278">paktɨ-lä</ta>
            <ta e="T280" id="Seg_3691" s="T279">pičʼɨ-lʼ</ta>
            <ta e="T281" id="Seg_3692" s="T280">utɨ-tɨ</ta>
            <ta e="T282" id="Seg_3693" s="T281">wərɨ-kkɨ</ta>
            <ta e="T283" id="Seg_3694" s="T282">isɨ</ta>
            <ta e="T284" id="Seg_3695" s="T283">čʼam</ta>
            <ta e="T285" id="Seg_3696" s="T284">ponä</ta>
            <ta e="T286" id="Seg_3697" s="T285">qontɨ-š-lʼčʼɨ-tɨt</ta>
            <ta e="T287" id="Seg_3698" s="T286">aj</ta>
            <ta e="T288" id="Seg_3699" s="T287">čʼarrä</ta>
            <ta e="T289" id="Seg_3700" s="T288">qättɨ-tɨt</ta>
            <ta e="T290" id="Seg_3701" s="T289">ämnä-iː-tɨ</ta>
            <ta e="T291" id="Seg_3702" s="T290">tü-m</ta>
            <ta e="T292" id="Seg_3703" s="T291">na</ta>
            <ta e="T293" id="Seg_3704" s="T292">*qam-mpɨ-ntɨ-tɨt</ta>
            <ta e="T294" id="Seg_3705" s="T293">šoŋol</ta>
            <ta e="T295" id="Seg_3706" s="T294">pɔːrɨ</ta>
            <ta e="T296" id="Seg_3707" s="T295">šüː</ta>
            <ta e="T297" id="Seg_3708" s="T296">ıllä</ta>
            <ta e="T298" id="Seg_3709" s="T297">aj</ta>
            <ta e="T299" id="Seg_3710" s="T298">šittɨ</ta>
            <ta e="T300" id="Seg_3711" s="T299">okoška-n</ta>
            <ta e="T301" id="Seg_3712" s="T300">ɔːŋ-ntɨ</ta>
            <ta e="T302" id="Seg_3713" s="T301">mɔːt-ntɨ</ta>
            <ta e="T303" id="Seg_3714" s="T302">qəː-ŋɨ-tɨ</ta>
            <ta e="T304" id="Seg_3715" s="T303">mɔːt-ɨ-m</ta>
            <ta e="T305" id="Seg_3716" s="T304">tü-sä</ta>
            <ta e="T306" id="Seg_3717" s="T305">čʼɔːtɨ-ŋɨ-tɨt</ta>
            <ta e="T307" id="Seg_3718" s="T306">na</ta>
            <ta e="T308" id="Seg_3719" s="T307">qälɨk-lʼ</ta>
            <ta e="T309" id="Seg_3720" s="T308">mütɨ-m</ta>
            <ta e="T310" id="Seg_3721" s="T309">mütɨ-n</ta>
            <ta e="T311" id="Seg_3722" s="T310">olɨ-qum</ta>
            <ta e="T312" id="Seg_3723" s="T311">šittɨ</ta>
            <ta e="T313" id="Seg_3724" s="T312">ɛː-mpɨ-ntɨ-tɨt</ta>
            <ta e="T314" id="Seg_3725" s="T313">nɨːnɨ</ta>
            <ta e="T315" id="Seg_3726" s="T314">karrä</ta>
            <ta e="T316" id="Seg_3727" s="T315">qən-ŋɨ</ta>
            <ta e="T317" id="Seg_3728" s="T316">qälɨk-t-ɨ-n</ta>
            <ta e="T318" id="Seg_3729" s="T317">qaqlɨ-ntɨ</ta>
            <ta e="T319" id="Seg_3730" s="T318">karrä</ta>
            <ta e="T320" id="Seg_3731" s="T319">qaj</ta>
            <ta e="T321" id="Seg_3732" s="T320">qaqlɨ-ntɨ</ta>
            <ta e="T322" id="Seg_3733" s="T321">qən-ŋɨ</ta>
            <ta e="T323" id="Seg_3734" s="T322">aj</ta>
            <ta e="T324" id="Seg_3735" s="T323">šittɨ</ta>
            <ta e="T325" id="Seg_3736" s="T324">ämnä-qı-tɨ</ta>
            <ta e="T326" id="Seg_3737" s="T325">qən-ntɨ-tɨ-tɨ</ta>
            <ta e="T327" id="Seg_3738" s="T326">šittɨ</ta>
            <ta e="T328" id="Seg_3739" s="T327">ämnä-qı-tɨ</ta>
            <ta e="T329" id="Seg_3740" s="T328">qən-ntɨ-tɨ-tɨ</ta>
            <ta e="T330" id="Seg_3741" s="T329">qət-qɨl-mpɨ-ntɨlʼ</ta>
            <ta e="T331" id="Seg_3742" s="T330">qum-ɨ-iː-qɨn-tɨ</ta>
            <ta e="T332" id="Seg_3743" s="T331">karrä-nɨ</ta>
            <ta e="T333" id="Seg_3744" s="T332">qən-ŋɨ</ta>
            <ta e="T334" id="Seg_3745" s="T333">moːčʼa-ntɨlʼ</ta>
            <ta e="T454" id="Seg_3746" s="T334">qäl</ta>
            <ta e="T335" id="Seg_3747" s="T454">ira</ta>
            <ta e="T336" id="Seg_3748" s="T335">qontɨ-š-ɛː-ntɨ</ta>
            <ta e="T337" id="Seg_3749" s="T336">qaj</ta>
            <ta e="T338" id="Seg_3750" s="T337">nʼoma-lʼ</ta>
            <ta e="T339" id="Seg_3751" s="T338">porqɨ-lʼ</ta>
            <ta e="T340" id="Seg_3752" s="T339">ira-n</ta>
            <ta e="T341" id="Seg_3753" s="T340">ima-m</ta>
            <ta e="T342" id="Seg_3754" s="T341">qəːčʼɨ-š-ɨ-lɨ-tɨ</ta>
            <ta e="T343" id="Seg_3755" s="T342">ira</ta>
            <ta e="T344" id="Seg_3756" s="T343">čʼarrä</ta>
            <ta e="T345" id="Seg_3757" s="T344">qättɨ-ŋɨ-tɨ</ta>
            <ta e="T346" id="Seg_3758" s="T345">nɨːnɨ</ta>
            <ta e="T347" id="Seg_3759" s="T346">karrä</ta>
            <ta e="T348" id="Seg_3760" s="T347">*kurɨ-alʼ-ŋɨ</ta>
            <ta e="T349" id="Seg_3761" s="T348">nɔːkɨr</ta>
            <ta e="T350" id="Seg_3762" s="T349">ämnä-sɨ-t</ta>
            <ta e="T351" id="Seg_3763" s="T350">takkɨ</ta>
            <ta e="T352" id="Seg_3764" s="T351">qən-ntɨ-ŋɨ-tɨt</ta>
            <ta e="T353" id="Seg_3765" s="T352">näčʼčʼä</ta>
            <ta e="T354" id="Seg_3766" s="T353">laqaltɨ-lʼčʼɨ-tɨ</ta>
            <ta e="T355" id="Seg_3767" s="T354">qälɨk-t-ɨ-n</ta>
            <ta e="T356" id="Seg_3768" s="T355">mɔːt-ntɨ</ta>
            <ta e="T357" id="Seg_3769" s="T356">takkɨ</ta>
            <ta e="T358" id="Seg_3770" s="T357">qən-ŋɨ-tɨt</ta>
            <ta e="T359" id="Seg_3771" s="T358">qälɨk-t-ɨ-n</ta>
            <ta e="T360" id="Seg_3772" s="T359">mɔːt-ntɨ</ta>
            <ta e="T361" id="Seg_3773" s="T360">ima-tɨt</ta>
            <ta e="T362" id="Seg_3774" s="T361">ıllä</ta>
            <ta e="T363" id="Seg_3775" s="T362">qən-ŋɨ-tɨt</ta>
            <ta e="T364" id="Seg_3776" s="T363">nɨmtɨ</ta>
            <ta e="T365" id="Seg_3777" s="T364">mɔːt-qɨn</ta>
            <ta e="T366" id="Seg_3778" s="T365">šölʼqum-ɨ-lʼ</ta>
            <ta e="T367" id="Seg_3779" s="T366">šittɨ</ta>
            <ta e="T368" id="Seg_3780" s="T367">iːja-qı-m</ta>
            <ta e="T369" id="Seg_3781" s="T368">qəːčʼɨ-mpɨ-tɨt</ta>
            <ta e="T370" id="Seg_3782" s="T369">tomoron</ta>
            <ta e="T371" id="Seg_3783" s="T370">nʼoma-lʼ</ta>
            <ta e="T372" id="Seg_3784" s="T371">porqɨ-lʼ</ta>
            <ta e="T373" id="Seg_3785" s="T372">ira-n</ta>
            <ta e="T374" id="Seg_3786" s="T373">kətsan-qı</ta>
            <ta e="T375" id="Seg_3787" s="T374">na-n</ta>
            <ta e="T376" id="Seg_3788" s="T375">iːja-qı</ta>
            <ta e="T377" id="Seg_3789" s="T376">ɔːtä-m</ta>
            <ta e="T378" id="Seg_3790" s="T377">tɔːqqɨ-lä</ta>
            <ta e="T379" id="Seg_3791" s="T378">konnä</ta>
            <ta e="T380" id="Seg_3792" s="T379">qən-ŋɨ-tɨt</ta>
            <ta e="T381" id="Seg_3793" s="T380">konnä</ta>
            <ta e="T382" id="Seg_3794" s="T381">mɔːt-al-ŋɨ-tɨt</ta>
            <ta e="T383" id="Seg_3795" s="T382">ukkɨr</ta>
            <ta e="T384" id="Seg_3796" s="T383">pi-n</ta>
            <ta e="T385" id="Seg_3797" s="T384">šäqqɨ-tɨt</ta>
            <ta e="T386" id="Seg_3798" s="T385">iːja-iː-tɨ</ta>
            <ta e="T387" id="Seg_3799" s="T386">tam</ta>
            <ta e="T388" id="Seg_3800" s="T387">čʼeːlɨ</ta>
            <ta e="T389" id="Seg_3801" s="T388">moqɨnä</ta>
            <ta e="T390" id="Seg_3802" s="T389">tü-ŋɨ-tɨt</ta>
            <ta e="T391" id="Seg_3803" s="T390">əsɨ</ta>
            <ta e="T392" id="Seg_3804" s="T391">ira-m-tɨ</ta>
            <ta e="T393" id="Seg_3805" s="T392">qən-tɨ-lä</ta>
            <ta e="T394" id="Seg_3806" s="T393">tü-ŋɨ-tɨt</ta>
            <ta e="T395" id="Seg_3807" s="T394">iːja-iː-tɨ</ta>
            <ta e="T396" id="Seg_3808" s="T395">təp-ɨ-t</ta>
            <ta e="T397" id="Seg_3809" s="T396">nılʼčʼɨ-k</ta>
            <ta e="T398" id="Seg_3810" s="T397">tɛnɨ-r-mpɨ-tɨt</ta>
            <ta e="T399" id="Seg_3811" s="T398">man</ta>
            <ta e="T400" id="Seg_3812" s="T399">əsɨ-mɨt</ta>
            <ta e="T401" id="Seg_3813" s="T400">qət-mpɨ-tɨt</ta>
            <ta e="T402" id="Seg_3814" s="T401">təp</ta>
            <ta e="T403" id="Seg_3815" s="T402">seːlʼčʼɨ</ta>
            <ta e="T404" id="Seg_3816" s="T403">iːja-tɨ</ta>
            <ta e="T405" id="Seg_3817" s="T404">ɛː-mpɨ-ntɨ</ta>
            <ta e="T406" id="Seg_3818" s="T405">kɨpa</ta>
            <ta e="T407" id="Seg_3819" s="T406">ämnä-tɨ</ta>
            <ta e="T408" id="Seg_3820" s="T407">təp-ɨ-t</ta>
            <ta e="T409" id="Seg_3821" s="T408">qən-tɨ-lä</ta>
            <ta e="T410" id="Seg_3822" s="T409">taːtɨ-tɨt</ta>
            <ta e="T411" id="Seg_3823" s="T410">iːja-n-tɨ</ta>
            <ta e="T412" id="Seg_3824" s="T411">kanaŋ-m</ta>
            <ta e="T413" id="Seg_3825" s="T412">poː-qɨn</ta>
            <ta e="T414" id="Seg_3826" s="T413">sɔːrɨ-mpɨ-tɨt</ta>
            <ta e="T415" id="Seg_3827" s="T414">ɔːtä-t</ta>
            <ta e="T416" id="Seg_3828" s="T415">ətɨ-mpɨ-ptäː-qɨn</ta>
            <ta e="T417" id="Seg_3829" s="T416">təp-ɨ-t</ta>
            <ta e="T418" id="Seg_3830" s="T417">kanaŋ-n</ta>
            <ta e="T419" id="Seg_3831" s="T418">topɨ-m</ta>
            <ta e="T420" id="Seg_3832" s="T419">čʼıqɨl-ŋɨ-tɨt</ta>
            <ta e="T421" id="Seg_3833" s="T420">ira</ta>
            <ta e="T422" id="Seg_3834" s="T421">nılʼčʼɨ-k</ta>
            <ta e="T423" id="Seg_3835" s="T422">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T424" id="Seg_3836" s="T423">kanaŋ-n</ta>
            <ta e="T425" id="Seg_3837" s="T424">topɨ-m</ta>
            <ta e="T426" id="Seg_3838" s="T425">qaj</ta>
            <ta e="T427" id="Seg_3839" s="T426">čʼıqɨl-ŋɨ-tɨ</ta>
            <ta e="T428" id="Seg_3840" s="T427">kɨpa</ta>
            <ta e="T429" id="Seg_3841" s="T428">iːja-n-tɨ</ta>
            <ta e="T430" id="Seg_3842" s="T429">ima</ta>
            <ta e="T431" id="Seg_3843" s="T430">ponä</ta>
            <ta e="T432" id="Seg_3844" s="T431">paktɨ</ta>
            <ta e="T433" id="Seg_3845" s="T432">natqaj</ta>
            <ta e="T434" id="Seg_3846" s="T433">ontɨ</ta>
            <ta e="T435" id="Seg_3847" s="T434">ira-tɨ</ta>
            <ta e="T436" id="Seg_3848" s="T435">tü-ntɨ</ta>
            <ta e="T437" id="Seg_3849" s="T436">natqaj</ta>
            <ta e="T438" id="Seg_3850" s="T437">ontɨ</ta>
            <ta e="T439" id="Seg_3851" s="T438">iːja-tɨ</ta>
            <ta e="T440" id="Seg_3852" s="T439">tü-mpɨ</ta>
            <ta e="T441" id="Seg_3853" s="T440">nɨːnɨ</ta>
            <ta e="T442" id="Seg_3854" s="T441">ilʼčʼa-tɨt</ta>
            <ta e="T443" id="Seg_3855" s="T442">nılʼčʼɨ-k</ta>
            <ta e="T444" id="Seg_3856" s="T443">kətɨ-mpɨ-tɨ</ta>
            <ta e="T445" id="Seg_3857" s="T444">montɨ</ta>
            <ta e="T446" id="Seg_3858" s="T445">onɨt</ta>
            <ta e="T447" id="Seg_3859" s="T446">tü-sä</ta>
            <ta e="T448" id="Seg_3860" s="T447">čʼɔːtɨ-sɨ-mɨt</ta>
            <ta e="T449" id="Seg_3861" s="T448">qälɨk-lʼ</ta>
            <ta e="T450" id="Seg_3862" s="T449">təttɨ-n</ta>
            <ta e="T451" id="Seg_3863" s="T450">ɔːtä-m</ta>
            <ta e="T452" id="Seg_3864" s="T451">nʼennä</ta>
            <ta e="T453" id="Seg_3865" s="T452">tɔːqqɨ-ŋɨ-tɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_3866" s="T0">hare-ADJZ</ta>
            <ta e="T2" id="Seg_3867" s="T1">clothing.[NOM]</ta>
            <ta e="T3" id="Seg_3868" s="T2">old.man.[NOM]</ta>
            <ta e="T4" id="Seg_3869" s="T3">seven</ta>
            <ta e="T5" id="Seg_3870" s="T4">son.[NOM]-3SG</ta>
            <ta e="T6" id="Seg_3871" s="T5">be-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T7" id="Seg_3872" s="T6">and</ta>
            <ta e="T8" id="Seg_3873" s="T7">seven</ta>
            <ta e="T9" id="Seg_3874" s="T8">daughter_in_law.[NOM]-3SG</ta>
            <ta e="T10" id="Seg_3875" s="T9">Nenets-PL.[NOM]</ta>
            <ta e="T11" id="Seg_3876" s="T10">come-CO-3PL</ta>
            <ta e="T12" id="Seg_3877" s="T11">down.the.river-ADV.EL</ta>
            <ta e="T13" id="Seg_3878" s="T12">down.the.river-ADV.ILL</ta>
            <ta e="T14" id="Seg_3879" s="T13">forward</ta>
            <ta e="T15" id="Seg_3880" s="T14">(s)he-EP-ACC</ta>
            <ta e="T16" id="Seg_3881" s="T15">kill-SUP.2/3PL</ta>
            <ta e="T17" id="Seg_3882" s="T16">human.being-PL.[NOM]-3SG</ta>
            <ta e="T18" id="Seg_3883" s="T17">son-PL.[NOM]-3SG</ta>
            <ta e="T19" id="Seg_3884" s="T18">forest-ILL</ta>
            <ta e="T20" id="Seg_3885" s="T19">leave-PST.NAR-3PL</ta>
            <ta e="T21" id="Seg_3886" s="T20">squirrel-CAP-CVB</ta>
            <ta e="T22" id="Seg_3887" s="T21">human.being.[NOM]</ta>
            <ta e="T23" id="Seg_3888" s="T22">now</ta>
            <ta e="T24" id="Seg_3889" s="T23">kill-TR-CO-3SG.O</ta>
            <ta e="T25" id="Seg_3890" s="T24">how</ta>
            <ta e="T26" id="Seg_3891" s="T25">be-CO.[3SG.S]</ta>
            <ta e="T27" id="Seg_3892" s="T26">ten</ta>
            <ta e="T28" id="Seg_3893" s="T27">Selkup-EP-ADJZ</ta>
            <ta e="T29" id="Seg_3894" s="T28">child-DIM-ACC</ta>
            <ta e="T30" id="Seg_3895" s="T29">keep-PST.NAR-3PL</ta>
            <ta e="T31" id="Seg_3896" s="T30">this</ta>
            <ta e="T32" id="Seg_3897" s="T31">Nenets-GEN</ta>
            <ta e="T33" id="Seg_3898" s="T32">this-ACC</ta>
            <ta e="T34" id="Seg_3899" s="T33">sent-INFER-3SG.O</ta>
            <ta e="T35" id="Seg_3900" s="T34">hare-ADJZ</ta>
            <ta e="T36" id="Seg_3901" s="T35">clothing.[NOM]</ta>
            <ta e="T37" id="Seg_3902" s="T36">old.man-ALL</ta>
            <ta e="T38" id="Seg_3903" s="T37">we.PL.NOM</ta>
            <ta e="T39" id="Seg_3904" s="T38">tomorrow</ta>
            <ta e="T40" id="Seg_3905" s="T39">as.if</ta>
            <ta e="T41" id="Seg_3906" s="T40">come-FUT-1PL</ta>
            <ta e="T42" id="Seg_3907" s="T41">leave-IPFV-IMP.2PL</ta>
            <ta e="T43" id="Seg_3908" s="T42">leave-IPFV-IMP.2PL.S</ta>
            <ta e="T44" id="Seg_3909" s="T43">word-TRL</ta>
            <ta e="T45" id="Seg_3910" s="T44">guy-DU.[NOM]</ta>
            <ta e="T46" id="Seg_3911" s="T45">home</ta>
            <ta e="T47" id="Seg_3912" s="T46">leave-CVB</ta>
            <ta e="T48" id="Seg_3913" s="T47">so</ta>
            <ta e="T49" id="Seg_3914" s="T48">say-CO-3SG.O</ta>
            <ta e="T50" id="Seg_3915" s="T49">orphan-ADJZ</ta>
            <ta e="T51" id="Seg_3916" s="T50">child-DU-ADJZ</ta>
            <ta e="T52" id="Seg_3917" s="T51">Nenets-DU.[NOM]</ta>
            <ta e="T53" id="Seg_3918" s="T52">keep-CO-3SG.O</ta>
            <ta e="T54" id="Seg_3919" s="T53">then</ta>
            <ta e="T55" id="Seg_3920" s="T54">two-ADJZ</ta>
            <ta e="T56" id="Seg_3921" s="T55">old.man.[NOM]</ta>
            <ta e="T57" id="Seg_3922" s="T56">where</ta>
            <ta e="T58" id="Seg_3923" s="T57">where.to.go-FUT.[3SG.S]</ta>
            <ta e="T59" id="Seg_3924" s="T58">old.man.[NOM]</ta>
            <ta e="T60" id="Seg_3925" s="T59">such-ADVZ</ta>
            <ta e="T61" id="Seg_3926" s="T60">say.[3SG.S]</ta>
            <ta e="T62" id="Seg_3927" s="T61">this</ta>
            <ta e="T63" id="Seg_3928" s="T62">hare-ADJZ</ta>
            <ta e="T64" id="Seg_3929" s="T63">clothing.[NOM]</ta>
            <ta e="T65" id="Seg_3930" s="T64">old.man.[NOM]</ta>
            <ta e="T66" id="Seg_3931" s="T65">daughter_in_law-OBL.3SG-ADJZ</ta>
            <ta e="T67" id="Seg_3932" s="T66">something-ALL</ta>
            <ta e="T68" id="Seg_3933" s="T67">fat.[NOM]</ta>
            <ta e="T69" id="Seg_3934" s="T68">food.[NOM]</ta>
            <ta e="T70" id="Seg_3935" s="T69">bring-IMP.2PL.S</ta>
            <ta e="T71" id="Seg_3936" s="T70">where</ta>
            <ta e="T72" id="Seg_3937" s="T71">all-ADJZ</ta>
            <ta e="T73" id="Seg_3938" s="T72">what.[NOM]</ta>
            <ta e="T74" id="Seg_3939" s="T73">bring-IMP.2PL.S</ta>
            <ta e="T75" id="Seg_3940" s="T74">Nenets-PL.[NOM]</ta>
            <ta e="T76" id="Seg_3941" s="T75">tomorrow</ta>
            <ta e="T77" id="Seg_3942" s="T76">come-FUT-3PL</ta>
            <ta e="T78" id="Seg_3943" s="T77">eat-EP-FRQ-FUT-3PL</ta>
            <ta e="T79" id="Seg_3944" s="T78">old.man.[NOM]</ta>
            <ta e="T80" id="Seg_3945" s="T79">so</ta>
            <ta e="T81" id="Seg_3946" s="T80">say-3SG.O</ta>
            <ta e="T82" id="Seg_3947" s="T81">food-ACC</ta>
            <ta e="T83" id="Seg_3948" s="T82">bring-3PL</ta>
            <ta e="T84" id="Seg_3949" s="T83">this</ta>
            <ta e="T85" id="Seg_3950" s="T84">tent-EP-GEN</ta>
            <ta e="T86" id="Seg_3951" s="T85">inside-ACC</ta>
            <ta e="T87" id="Seg_3952" s="T86">new</ta>
            <ta e="T88" id="Seg_3953" s="T87">hay-INSTR</ta>
            <ta e="T89" id="Seg_3954" s="T88">lay-3PL</ta>
            <ta e="T90" id="Seg_3955" s="T89">old.man.[NOM]</ta>
            <ta e="T91" id="Seg_3956" s="T90">Nenets-ACC-3SG</ta>
            <ta e="T92" id="Seg_3957" s="T91">food-TR-INF-be.going.to-CO-3SG.O</ta>
            <ta e="T93" id="Seg_3958" s="T92">oh</ta>
            <ta e="T94" id="Seg_3959" s="T93">next</ta>
            <ta e="T95" id="Seg_3960" s="T94">day.[NOM]</ta>
            <ta e="T96" id="Seg_3961" s="T95">Nenets-PL.[NOM]</ta>
            <ta e="T97" id="Seg_3962" s="T96">INFER</ta>
            <ta e="T98" id="Seg_3963" s="T97">RFL</ta>
            <ta e="T99" id="Seg_3964" s="T98">bring-INFER-3PL</ta>
            <ta e="T100" id="Seg_3965" s="T99">three-ten-EP-ADJZ</ta>
            <ta e="T101" id="Seg_3966" s="T100">human.being-EP-ADJZ</ta>
            <ta e="T102" id="Seg_3967" s="T101">army.[NOM]</ta>
            <ta e="T103" id="Seg_3968" s="T102">supposedly</ta>
            <ta e="T104" id="Seg_3969" s="T103">run.out-PTCP.PST</ta>
            <ta e="T105" id="Seg_3970" s="T104">tent-LOC</ta>
            <ta e="T106" id="Seg_3971" s="T105">hare-ADJZ</ta>
            <ta e="T107" id="Seg_3972" s="T106">clothing.[NOM]</ta>
            <ta e="T108" id="Seg_3973" s="T107">old.man.[NOM]</ta>
            <ta e="T109" id="Seg_3974" s="T108">oneself.3SG-ADV.LOC</ta>
            <ta e="T110" id="Seg_3975" s="T109">daughter_in_law-PL-OBL.3SG-COM</ta>
            <ta e="T111" id="Seg_3976" s="T110">fresh</ta>
            <ta e="T112" id="Seg_3977" s="T111">hay-INSTR</ta>
            <ta e="T113" id="Seg_3978" s="T112">lay-FRQ-FRQ-3PL</ta>
            <ta e="T114" id="Seg_3979" s="T113">lay-FRQ-FRQ-EP-PST.NAR-3PL</ta>
            <ta e="T115" id="Seg_3980" s="T114">old.man.[NOM]</ta>
            <ta e="T116" id="Seg_3981" s="T115">such-ADVZ</ta>
            <ta e="T117" id="Seg_3982" s="T116">say.[3SG.S]</ta>
            <ta e="T118" id="Seg_3983" s="T117">eat-EP-FRQ-INF-begin-CO-3PL</ta>
            <ta e="T119" id="Seg_3984" s="T118">food-ACC</ta>
            <ta e="T120" id="Seg_3985" s="T119">find-INFER-3PL</ta>
            <ta e="T121" id="Seg_3986" s="T120">old.man.[NOM]</ta>
            <ta e="T122" id="Seg_3987" s="T121">so</ta>
            <ta e="T123" id="Seg_3988" s="T122">say-CO-3SG.O</ta>
            <ta e="T124" id="Seg_3989" s="T123">old.woman.[NOM]-3SG</ta>
            <ta e="T125" id="Seg_3990" s="T124">also</ta>
            <ta e="T126" id="Seg_3991" s="T125">be-CO.[3SG.S]</ta>
            <ta e="T127" id="Seg_3992" s="T126">daughter_in_law-PL-ACC-3SG</ta>
            <ta e="T128" id="Seg_3993" s="T127">so</ta>
            <ta e="T129" id="Seg_3994" s="T128">go-CAUS-3SG.O</ta>
            <ta e="T130" id="Seg_3995" s="T129">outwards</ta>
            <ta e="T131" id="Seg_3996" s="T130">go.out-IMP.2PL</ta>
            <ta e="T132" id="Seg_3997" s="T131">child-PL-EP-ACC</ta>
            <ta e="T133" id="Seg_3998" s="T132">outwards</ta>
            <ta e="T134" id="Seg_3999" s="T133">bring-IMP.2PL</ta>
            <ta e="T135" id="Seg_4000" s="T134">old.man.[NOM]</ta>
            <ta e="T136" id="Seg_4001" s="T135">oneself.3SG.[NOM]</ta>
            <ta e="T137" id="Seg_4002" s="T136">keep-HAB.[3SG.S]</ta>
            <ta e="T138" id="Seg_4003" s="T137">human.being-EP-ACC-3SG</ta>
            <ta e="T139" id="Seg_4004" s="T138">food-TR-DUR-CVB</ta>
            <ta e="T140" id="Seg_4005" s="T139">one</ta>
            <ta e="T141" id="Seg_4006" s="T140">whole</ta>
            <ta e="T142" id="Seg_4007" s="T141">middle-LOC</ta>
            <ta e="T143" id="Seg_4008" s="T142">come-INFER.[3SG.S]</ta>
            <ta e="T144" id="Seg_4009" s="T143">morning-ADV.LOC</ta>
            <ta e="T145" id="Seg_4010" s="T144">human.being.[NOM]</ta>
            <ta e="T146" id="Seg_4011" s="T145">eat-EP-FRQ-CO-3PL</ta>
            <ta e="T147" id="Seg_4012" s="T146">fireplace-EP-ADJZ</ta>
            <ta e="T148" id="Seg_4013" s="T147">fireplace.[NOM]</ta>
            <ta e="T149" id="Seg_4014" s="T148">fire-ACC-3SG</ta>
            <ta e="T150" id="Seg_4015" s="T149">this-ACC</ta>
            <ta e="T151" id="Seg_4016" s="T150">big</ta>
            <ta e="T152" id="Seg_4017" s="T151">firewood-INSTR</ta>
            <ta e="T153" id="Seg_4018" s="T152">stoke-CO-3SG.O</ta>
            <ta e="T154" id="Seg_4019" s="T153">burn-INFER.[3SG.S]</ta>
            <ta e="T155" id="Seg_4020" s="T154">daughter_in_law-PL-ACC-3SG</ta>
            <ta e="T156" id="Seg_4021" s="T155">NEG</ta>
            <ta e="T157" id="Seg_4022" s="T156">what.for</ta>
            <ta e="T158" id="Seg_4023" s="T157">what-TRL</ta>
            <ta e="T159" id="Seg_4024" s="T158">outwards</ta>
            <ta e="T160" id="Seg_4025" s="T159">sent-CO-3SG.O</ta>
            <ta e="T161" id="Seg_4026" s="T160">space.outside-ADV.LOC</ta>
            <ta e="T162" id="Seg_4027" s="T161">fire.[NOM]</ta>
            <ta e="T163" id="Seg_4028" s="T162">set.fire-INFER-3PL</ta>
            <ta e="T164" id="Seg_4029" s="T163">fire-ACC</ta>
            <ta e="T165" id="Seg_4030" s="T164">set.fire-MULO-PST.NAR-3PL</ta>
            <ta e="T166" id="Seg_4031" s="T165">that</ta>
            <ta e="T167" id="Seg_4032" s="T166">birchbark-ACC</ta>
            <ta e="T168" id="Seg_4033" s="T167">torch-EP-VBLZ-MOM-PST.NAR-3PL</ta>
            <ta e="T169" id="Seg_4034" s="T168">such-ADVZ</ta>
            <ta e="T170" id="Seg_4035" s="T169">what.[NOM]</ta>
            <ta e="T171" id="Seg_4036" s="T170">dry-CAUS-PST.NAR-3PL</ta>
            <ta e="T172" id="Seg_4037" s="T171">you.PL.NOM</ta>
            <ta e="T173" id="Seg_4038" s="T172">here</ta>
            <ta e="T174" id="Seg_4039" s="T173">be-PTCP.PRS</ta>
            <ta e="T175" id="Seg_4040" s="T174">I.NOM</ta>
            <ta e="T176" id="Seg_4041" s="T175">here</ta>
            <ta e="T177" id="Seg_4042" s="T176">go.out-FUT-1SG.S</ta>
            <ta e="T178" id="Seg_4043" s="T177">eat-EP-FRQ-CO-3PL</ta>
            <ta e="T179" id="Seg_4044" s="T178">Nenets-PL.[NOM]</ta>
            <ta e="T180" id="Seg_4045" s="T179">so</ta>
            <ta e="T181" id="Seg_4046" s="T180">say-3PL</ta>
            <ta e="T182" id="Seg_4047" s="T181">grandfather.[NOM]</ta>
            <ta e="T183" id="Seg_4048" s="T182">fireplace-EP-ADJZ</ta>
            <ta e="T184" id="Seg_4049" s="T183">what-EP-TRL</ta>
            <ta e="T185" id="Seg_4050" s="T184">so.much</ta>
            <ta e="T186" id="Seg_4051" s="T185">firewood-INSTR</ta>
            <ta e="T187" id="Seg_4052" s="T186">stoke-2SG.O</ta>
            <ta e="T188" id="Seg_4053" s="T187">NEG</ta>
            <ta e="T189" id="Seg_4054" s="T188">fireplace.[NOM]</ta>
            <ta e="T190" id="Seg_4055" s="T189">something.[NOM]</ta>
            <ta e="T191" id="Seg_4056" s="T190">fire.[NOM]</ta>
            <ta e="T192" id="Seg_4057" s="T191">here</ta>
            <ta e="T193" id="Seg_4058" s="T192">eat-FUT-3SG.O</ta>
            <ta e="T194" id="Seg_4059" s="T193">(s)he.[NOM]</ta>
            <ta e="T195" id="Seg_4060" s="T194">NEG</ta>
            <ta e="T196" id="Seg_4061" s="T195">eat-FUT-3SG.O</ta>
            <ta e="T197" id="Seg_4062" s="T196">so</ta>
            <ta e="T198" id="Seg_4063" s="T197">live-CAUS-PST.NAR.[3SG.S]</ta>
            <ta e="T199" id="Seg_4064" s="T198">hare-ADJZ</ta>
            <ta e="T200" id="Seg_4065" s="T199">clothing-GEN-3SG</ta>
            <ta e="T201" id="Seg_4066" s="T200">rags-ADJZ</ta>
            <ta e="T202" id="Seg_4067" s="T201">piece.[NOM]</ta>
            <ta e="T203" id="Seg_4068" s="T202">this.[NOM]</ta>
            <ta e="T204" id="Seg_4069" s="T203">come.in-PST.NAR-INFER-3SG.O</ta>
            <ta e="T205" id="Seg_4070" s="T204">fireplace-GEN.3SG</ta>
            <ta e="T206" id="Seg_4071" s="T205">near-LOC</ta>
            <ta e="T207" id="Seg_4072" s="T206">big</ta>
            <ta e="T208" id="Seg_4073" s="T207">door-GEN-3SG</ta>
            <ta e="T209" id="Seg_4074" s="T208">near-LOC</ta>
            <ta e="T210" id="Seg_4075" s="T209">one</ta>
            <ta e="T211" id="Seg_4076" s="T210">whole</ta>
            <ta e="T212" id="Seg_4077" s="T211">middle-LOC</ta>
            <ta e="T213" id="Seg_4078" s="T212">space.outside-ADV.EL</ta>
            <ta e="T214" id="Seg_4079" s="T213">house-ILL</ta>
            <ta e="T215" id="Seg_4080" s="T214">fire-ADJZ</ta>
            <ta e="T216" id="Seg_4081" s="T215">torch-EP-ADJZ</ta>
            <ta e="T217" id="Seg_4082" s="T216">piece.[NOM]</ta>
            <ta e="T218" id="Seg_4083" s="T217">fall-IPFV.[3SG.S]</ta>
            <ta e="T219" id="Seg_4084" s="T218">INFER</ta>
            <ta e="T220" id="Seg_4085" s="T219">run-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T221" id="Seg_4086" s="T220">door-ILL</ta>
            <ta e="T222" id="Seg_4087" s="T221">Nenets-PL.[NOM]</ta>
            <ta e="T223" id="Seg_4088" s="T222">hardly</ta>
            <ta e="T224" id="Seg_4089" s="T223">catch-PST.NAR-3PL</ta>
            <ta e="T225" id="Seg_4090" s="T224">old.man-ACC</ta>
            <ta e="T226" id="Seg_4091" s="T225">only</ta>
            <ta e="T227" id="Seg_4092" s="T226">only</ta>
            <ta e="T228" id="Seg_4093" s="T227">hare-ADJZ</ta>
            <ta e="T229" id="Seg_4094" s="T228">clothing.[NOM]</ta>
            <ta e="T230" id="Seg_4095" s="T229">piece-ACC</ta>
            <ta e="T231" id="Seg_4096" s="T230">catch-PST.NAR-3PL</ta>
            <ta e="T232" id="Seg_4097" s="T231">supposedly</ta>
            <ta e="T233" id="Seg_4098" s="T232">here</ta>
            <ta e="T234" id="Seg_4099" s="T233">come-PST.[3SG.S]</ta>
            <ta e="T235" id="Seg_4100" s="T234">window-GEN</ta>
            <ta e="T236" id="Seg_4101" s="T235">opening-PROL</ta>
            <ta e="T237" id="Seg_4102" s="T236">house-GEN.3SG</ta>
            <ta e="T238" id="Seg_4103" s="T237">two</ta>
            <ta e="T239" id="Seg_4104" s="T238">window.[NOM]-3SG</ta>
            <ta e="T240" id="Seg_4105" s="T239">be-DUR-PST.NAR.[3SG.S]</ta>
            <ta e="T241" id="Seg_4106" s="T240">whether</ta>
            <ta e="T242" id="Seg_4107" s="T241">big</ta>
            <ta e="T243" id="Seg_4108" s="T242">house.[NOM]-3SG</ta>
            <ta e="T244" id="Seg_4109" s="T243">be-DUR-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T245" id="Seg_4110" s="T244">fire-INSTR</ta>
            <ta e="T246" id="Seg_4111" s="T245">close-PST.NAR.[3SG.S]</ta>
            <ta e="T247" id="Seg_4112" s="T246">stick.up-EP-TR.[3SG.S]</ta>
            <ta e="T248" id="Seg_4113" s="T247">tree-ADJZ</ta>
            <ta e="T249" id="Seg_4114" s="T248">board-PL.[NOM]</ta>
            <ta e="T250" id="Seg_4115" s="T249">axe-GEN</ta>
            <ta e="T251" id="Seg_4116" s="T250">handle-GEN</ta>
            <ta e="T252" id="Seg_4117" s="T251">height.[NOM]</ta>
            <ta e="T253" id="Seg_4118" s="T252">close-PST.NAR.[3SG.S]</ta>
            <ta e="T254" id="Seg_4119" s="T253">that.[NOM]</ta>
            <ta e="T255" id="Seg_4120" s="T254">fire-ACC</ta>
            <ta e="T256" id="Seg_4121" s="T255">INFER</ta>
            <ta e="T257" id="Seg_4122" s="T256">throw-MOM-PST.NAR-INFER-3PL</ta>
            <ta e="T258" id="Seg_4123" s="T257">house-ILL</ta>
            <ta e="T259" id="Seg_4124" s="T258">gunpowder-INSTR</ta>
            <ta e="T260" id="Seg_4125" s="T259">long.ago</ta>
            <ta e="T261" id="Seg_4126" s="T260">pour-IPFV-PST.NAR.[3SG.S]</ta>
            <ta e="T262" id="Seg_4127" s="T261">this</ta>
            <ta e="T263" id="Seg_4128" s="T262">hay-GEN</ta>
            <ta e="T264" id="Seg_4129" s="T263">bottom-ILL</ta>
            <ta e="T265" id="Seg_4130" s="T264">fire.[NOM]</ta>
            <ta e="T266" id="Seg_4131" s="T265">INFER</ta>
            <ta e="T267" id="Seg_4132" s="T266">hit-PST.NAR-INFER-3SG.O</ta>
            <ta e="T268" id="Seg_4133" s="T267">that</ta>
            <ta e="T269" id="Seg_4134" s="T268">three-ten-EP-ADJZ</ta>
            <ta e="T270" id="Seg_4135" s="T269">human.being-EP-ADJZ</ta>
            <ta e="T271" id="Seg_4136" s="T270">army.[NOM]</ta>
            <ta e="T272" id="Seg_4137" s="T271">fire.[NOM]</ta>
            <ta e="T273" id="Seg_4138" s="T272">eat-PST.NAR-3SG.O</ta>
            <ta e="T274" id="Seg_4139" s="T273">leave-CO-3PL</ta>
            <ta e="T275" id="Seg_4140" s="T274">up-ADV.EL-in.some.degree</ta>
            <ta e="T276" id="Seg_4141" s="T275">daughter_in_law-PL.[NOM]-3SG</ta>
            <ta e="T277" id="Seg_4142" s="T276">old.man.[NOM]</ta>
            <ta e="T278" id="Seg_4143" s="T277">outwards</ta>
            <ta e="T279" id="Seg_4144" s="T278">run-CVB</ta>
            <ta e="T280" id="Seg_4145" s="T279">axe-ADJZ</ta>
            <ta e="T281" id="Seg_4146" s="T280">hand.[NOM]-3SG</ta>
            <ta e="T282" id="Seg_4147" s="T281">keep-HAB.[3SG.S]</ta>
            <ta e="T283" id="Seg_4148" s="T282">every</ta>
            <ta e="T284" id="Seg_4149" s="T283">hardly</ta>
            <ta e="T285" id="Seg_4150" s="T284">outwards</ta>
            <ta e="T286" id="Seg_4151" s="T285">appear-US-PFV-3PL</ta>
            <ta e="T287" id="Seg_4152" s="T286">also</ta>
            <ta e="T288" id="Seg_4153" s="T287">in.the.face</ta>
            <ta e="T289" id="Seg_4154" s="T288">hit-3PL</ta>
            <ta e="T290" id="Seg_4155" s="T289">daughter_in_law-PL.[NOM]-3SG</ta>
            <ta e="T291" id="Seg_4156" s="T290">fire-ACC</ta>
            <ta e="T292" id="Seg_4157" s="T291">INFER</ta>
            <ta e="T293" id="Seg_4158" s="T292">pour-PST.NAR-INFER-3PL</ta>
            <ta e="T294" id="Seg_4159" s="T293">fireplace.[NOM]</ta>
            <ta e="T295" id="Seg_4160" s="T294">top.[NOM]</ta>
            <ta e="T296" id="Seg_4161" s="T295">through</ta>
            <ta e="T297" id="Seg_4162" s="T296">down</ta>
            <ta e="T298" id="Seg_4163" s="T297">and</ta>
            <ta e="T299" id="Seg_4164" s="T298">two</ta>
            <ta e="T300" id="Seg_4165" s="T299">window-GEN</ta>
            <ta e="T301" id="Seg_4166" s="T300">opening-ILL</ta>
            <ta e="T302" id="Seg_4167" s="T301">house-ILL</ta>
            <ta e="T303" id="Seg_4168" s="T302">throw-CO-3SG.O</ta>
            <ta e="T304" id="Seg_4169" s="T303">house-EP-ACC</ta>
            <ta e="T305" id="Seg_4170" s="T304">fire-INSTR</ta>
            <ta e="T306" id="Seg_4171" s="T305">set.fire-CO-3PL</ta>
            <ta e="T307" id="Seg_4172" s="T306">this</ta>
            <ta e="T308" id="Seg_4173" s="T307">Nenets-ADJZ</ta>
            <ta e="T309" id="Seg_4174" s="T308">army-ACC</ta>
            <ta e="T310" id="Seg_4175" s="T309">army-GEN</ta>
            <ta e="T311" id="Seg_4176" s="T310">head-human.being.[NOM]</ta>
            <ta e="T312" id="Seg_4177" s="T311">two</ta>
            <ta e="T313" id="Seg_4178" s="T312">be-PST.NAR-INFER-3PL</ta>
            <ta e="T314" id="Seg_4179" s="T313">then</ta>
            <ta e="T315" id="Seg_4180" s="T314">down</ta>
            <ta e="T316" id="Seg_4181" s="T315">go.away-CO.[3SG.S]</ta>
            <ta e="T317" id="Seg_4182" s="T316">Nenets-PL-EP-GEN</ta>
            <ta e="T318" id="Seg_4183" s="T317">sledge-ILL</ta>
            <ta e="T319" id="Seg_4184" s="T318">down</ta>
            <ta e="T320" id="Seg_4185" s="T319">whether</ta>
            <ta e="T321" id="Seg_4186" s="T320">sledge-ILL</ta>
            <ta e="T322" id="Seg_4187" s="T321">go.away-CO.[3SG.S]</ta>
            <ta e="T323" id="Seg_4188" s="T322">and</ta>
            <ta e="T324" id="Seg_4189" s="T323">two</ta>
            <ta e="T325" id="Seg_4190" s="T324">daughter_in_law-DU.[NOM]-3SG</ta>
            <ta e="T326" id="Seg_4191" s="T325">leave-IPFV-TR-3SG.O</ta>
            <ta e="T327" id="Seg_4192" s="T326">two</ta>
            <ta e="T328" id="Seg_4193" s="T327">daughter_in_law-DU.[NOM]-3SG</ta>
            <ta e="T329" id="Seg_4194" s="T328">leave-IPFV-TR-3SG.O</ta>
            <ta e="T330" id="Seg_4195" s="T329">kill-MULO-DUR-PTCP.PRS</ta>
            <ta e="T331" id="Seg_4196" s="T330">human.being-EP-PL-ILL-3SG</ta>
            <ta e="T332" id="Seg_4197" s="T331">down-ADV.EL</ta>
            <ta e="T333" id="Seg_4198" s="T332">leave-CO.[3SG.S]</ta>
            <ta e="T334" id="Seg_4199" s="T333">limp-PTCP.PRS</ta>
            <ta e="T454" id="Seg_4200" s="T334">Nenets.[NOM]</ta>
            <ta e="T335" id="Seg_4201" s="T454">old.man.[NOM]</ta>
            <ta e="T336" id="Seg_4202" s="T335">appear-US-PFV-INFER.[3SG.S]</ta>
            <ta e="T337" id="Seg_4203" s="T336">whether</ta>
            <ta e="T338" id="Seg_4204" s="T337">hare-ADJZ</ta>
            <ta e="T339" id="Seg_4205" s="T338">clothing-ADJZ</ta>
            <ta e="T340" id="Seg_4206" s="T339">old.man-GEN</ta>
            <ta e="T341" id="Seg_4207" s="T340">woman-ACC</ta>
            <ta e="T342" id="Seg_4208" s="T341">leave-US-EP-RES-3SG.O</ta>
            <ta e="T343" id="Seg_4209" s="T342">old.man.[NOM]</ta>
            <ta e="T344" id="Seg_4210" s="T343">in.the.face</ta>
            <ta e="T345" id="Seg_4211" s="T344">hit-CO-3SG.O</ta>
            <ta e="T346" id="Seg_4212" s="T345">then</ta>
            <ta e="T347" id="Seg_4213" s="T346">down</ta>
            <ta e="T348" id="Seg_4214" s="T347">go-INCH-CO.[3SG.S]</ta>
            <ta e="T349" id="Seg_4215" s="T348">three</ta>
            <ta e="T350" id="Seg_4216" s="T349">daughter_in_law-DYA-PL.[NOM]</ta>
            <ta e="T351" id="Seg_4217" s="T350">down.the.river</ta>
            <ta e="T352" id="Seg_4218" s="T351">leave-IPFV-CO-3PL</ta>
            <ta e="T353" id="Seg_4219" s="T352">there</ta>
            <ta e="T354" id="Seg_4220" s="T353">start.for-PFV-3SG.O</ta>
            <ta e="T355" id="Seg_4221" s="T354">Nenets-PL-EP-GEN</ta>
            <ta e="T356" id="Seg_4222" s="T355">tent-ILL</ta>
            <ta e="T357" id="Seg_4223" s="T356">down.the.river</ta>
            <ta e="T358" id="Seg_4224" s="T357">go.away-CO-3PL</ta>
            <ta e="T359" id="Seg_4225" s="T358">Nenets-PL-EP-GEN</ta>
            <ta e="T360" id="Seg_4226" s="T359">tent-ILL</ta>
            <ta e="T361" id="Seg_4227" s="T360">woman.[NOM]-3PL</ta>
            <ta e="T362" id="Seg_4228" s="T361">down</ta>
            <ta e="T363" id="Seg_4229" s="T362">go.away-CO-3PL</ta>
            <ta e="T364" id="Seg_4230" s="T363">there</ta>
            <ta e="T365" id="Seg_4231" s="T364">tent-LOC</ta>
            <ta e="T366" id="Seg_4232" s="T365">Selkup-EP-ADJZ</ta>
            <ta e="T367" id="Seg_4233" s="T366">two</ta>
            <ta e="T368" id="Seg_4234" s="T367">child-DU-ACC</ta>
            <ta e="T369" id="Seg_4235" s="T368">leave-PST.NAR-3PL</ta>
            <ta e="T370" id="Seg_4236" s="T369">long.ago</ta>
            <ta e="T371" id="Seg_4237" s="T370">hare-ADJZ</ta>
            <ta e="T372" id="Seg_4238" s="T371">clothing-ADJZ</ta>
            <ta e="T373" id="Seg_4239" s="T372">old.man-GEN</ta>
            <ta e="T374" id="Seg_4240" s="T373">grandson-DU.[NOM]</ta>
            <ta e="T375" id="Seg_4241" s="T374">this-GEN</ta>
            <ta e="T376" id="Seg_4242" s="T375">child-DU.[NOM]</ta>
            <ta e="T377" id="Seg_4243" s="T376">reindeer-ACC</ta>
            <ta e="T378" id="Seg_4244" s="T377">shepherd-CVB</ta>
            <ta e="T379" id="Seg_4245" s="T378">upwards</ta>
            <ta e="T380" id="Seg_4246" s="T379">go.away-CO-3PL</ta>
            <ta e="T381" id="Seg_4247" s="T380">upwards</ta>
            <ta e="T382" id="Seg_4248" s="T381">tent-TR-CO-3PL</ta>
            <ta e="T383" id="Seg_4249" s="T382">one</ta>
            <ta e="T384" id="Seg_4250" s="T383">night-ADV.LOC</ta>
            <ta e="T385" id="Seg_4251" s="T384">spend.night-3PL</ta>
            <ta e="T386" id="Seg_4252" s="T385">son-PL.[NOM]-3SG</ta>
            <ta e="T387" id="Seg_4253" s="T386">this</ta>
            <ta e="T388" id="Seg_4254" s="T387">day.[NOM]</ta>
            <ta e="T389" id="Seg_4255" s="T388">home</ta>
            <ta e="T390" id="Seg_4256" s="T389">come-CO-3PL</ta>
            <ta e="T391" id="Seg_4257" s="T390">father.[NOM]</ta>
            <ta e="T392" id="Seg_4258" s="T391">old.man-ACC-3SG</ta>
            <ta e="T393" id="Seg_4259" s="T392">leave-TR-CVB</ta>
            <ta e="T394" id="Seg_4260" s="T393">come-CO-3PL</ta>
            <ta e="T395" id="Seg_4261" s="T394">son-PL.[NOM]-3SG</ta>
            <ta e="T396" id="Seg_4262" s="T395">(s)he-EP-PL.[NOM]</ta>
            <ta e="T397" id="Seg_4263" s="T396">such-ADVZ</ta>
            <ta e="T398" id="Seg_4264" s="T397">think-FRQ-PST.NAR-3PL</ta>
            <ta e="T399" id="Seg_4265" s="T398">I.GEN</ta>
            <ta e="T400" id="Seg_4266" s="T399">father.[NOM]-1PL</ta>
            <ta e="T401" id="Seg_4267" s="T400">kill-PST.NAR-3PL</ta>
            <ta e="T402" id="Seg_4268" s="T401">(s)he.[NOM]</ta>
            <ta e="T403" id="Seg_4269" s="T402">seven</ta>
            <ta e="T404" id="Seg_4270" s="T403">son.[NOM]-3SG</ta>
            <ta e="T405" id="Seg_4271" s="T404">be-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T406" id="Seg_4272" s="T405">young</ta>
            <ta e="T407" id="Seg_4273" s="T406">daughter_in_law.[NOM]-3SG</ta>
            <ta e="T408" id="Seg_4274" s="T407">(s)he-EP-PL.[NOM]</ta>
            <ta e="T409" id="Seg_4275" s="T408">leave-TR-CVB</ta>
            <ta e="T410" id="Seg_4276" s="T409">bring-3PL</ta>
            <ta e="T411" id="Seg_4277" s="T410">son-GEN-3SG</ta>
            <ta e="T412" id="Seg_4278" s="T411">dog-ACC</ta>
            <ta e="T413" id="Seg_4279" s="T412">space.outside-ADV.LOC</ta>
            <ta e="T414" id="Seg_4280" s="T413">bind-PST.NAR-3PL</ta>
            <ta e="T415" id="Seg_4281" s="T414">reindeer-PL.[NOM]</ta>
            <ta e="T416" id="Seg_4282" s="T415">wait-DUR-ACTN-LOC</ta>
            <ta e="T417" id="Seg_4283" s="T416">(s)he-EP-PL.[NOM]</ta>
            <ta e="T418" id="Seg_4284" s="T417">dog-GEN</ta>
            <ta e="T419" id="Seg_4285" s="T418">leg-ACC</ta>
            <ta e="T420" id="Seg_4286" s="T419">step-CO-3PL</ta>
            <ta e="T421" id="Seg_4287" s="T420">old.man.[NOM]</ta>
            <ta e="T422" id="Seg_4288" s="T421">such-ADVZ</ta>
            <ta e="T423" id="Seg_4289" s="T422">say-CO-3SG.O</ta>
            <ta e="T424" id="Seg_4290" s="T423">dog-GEN</ta>
            <ta e="T425" id="Seg_4291" s="T424">leg-ACC</ta>
            <ta e="T426" id="Seg_4292" s="T425">what.[NOM]</ta>
            <ta e="T427" id="Seg_4293" s="T426">step-CO-3SG.O</ta>
            <ta e="T428" id="Seg_4294" s="T427">young</ta>
            <ta e="T429" id="Seg_4295" s="T428">son-GEN-3SG</ta>
            <ta e="T430" id="Seg_4296" s="T429">wife.[NOM]</ta>
            <ta e="T431" id="Seg_4297" s="T430">outwards</ta>
            <ta e="T432" id="Seg_4298" s="T431">run.[3SG.S]</ta>
            <ta e="T433" id="Seg_4299" s="T432">well</ta>
            <ta e="T434" id="Seg_4300" s="T433">own.3SG</ta>
            <ta e="T435" id="Seg_4301" s="T434">husband.[NOM]-3SG</ta>
            <ta e="T436" id="Seg_4302" s="T435">come-INFER.[3SG.S]</ta>
            <ta e="T437" id="Seg_4303" s="T436">well</ta>
            <ta e="T438" id="Seg_4304" s="T437">own.3SG</ta>
            <ta e="T439" id="Seg_4305" s="T438">son.[NOM]-3SG</ta>
            <ta e="T440" id="Seg_4306" s="T439">come-PST.NAR.[3SG.S]</ta>
            <ta e="T441" id="Seg_4307" s="T440">then</ta>
            <ta e="T442" id="Seg_4308" s="T441">grandfather.[NOM]-3PL</ta>
            <ta e="T443" id="Seg_4309" s="T442">such-ADVZ</ta>
            <ta e="T444" id="Seg_4310" s="T443">say-PST.NAR-3SG.O</ta>
            <ta e="T445" id="Seg_4311" s="T444">apparently</ta>
            <ta e="T446" id="Seg_4312" s="T445">oneself.1PL.[NOM]</ta>
            <ta e="T447" id="Seg_4313" s="T446">fire-INSTR</ta>
            <ta e="T448" id="Seg_4314" s="T447">set.fire-PST-1PL</ta>
            <ta e="T449" id="Seg_4315" s="T448">Nenets-ADJZ</ta>
            <ta e="T450" id="Seg_4316" s="T449">earth-ADV.LOC</ta>
            <ta e="T451" id="Seg_4317" s="T450">reindeer-ACC</ta>
            <ta e="T452" id="Seg_4318" s="T451">forward</ta>
            <ta e="T453" id="Seg_4319" s="T452">shepherd-CO-3SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_4320" s="T0">заяц-ADJZ</ta>
            <ta e="T2" id="Seg_4321" s="T1">одежда.[NOM]</ta>
            <ta e="T3" id="Seg_4322" s="T2">старик.[NOM]</ta>
            <ta e="T4" id="Seg_4323" s="T3">семь</ta>
            <ta e="T5" id="Seg_4324" s="T4">сын.[NOM]-3SG</ta>
            <ta e="T6" id="Seg_4325" s="T5">быть-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T7" id="Seg_4326" s="T6">и</ta>
            <ta e="T8" id="Seg_4327" s="T7">семь</ta>
            <ta e="T9" id="Seg_4328" s="T8">жена.сына.[NOM]-3SG</ta>
            <ta e="T10" id="Seg_4329" s="T9">ненец-PL.[NOM]</ta>
            <ta e="T11" id="Seg_4330" s="T10">прийти-CO-3PL</ta>
            <ta e="T12" id="Seg_4331" s="T11">вниз.по.течению.реки-ADV.EL</ta>
            <ta e="T13" id="Seg_4332" s="T12">вниз.по.течению.реки-ADV.ILL</ta>
            <ta e="T14" id="Seg_4333" s="T13">вперёд</ta>
            <ta e="T15" id="Seg_4334" s="T14">он(а)-EP-ACC</ta>
            <ta e="T16" id="Seg_4335" s="T15">убить-SUP.2/3PL</ta>
            <ta e="T17" id="Seg_4336" s="T16">человек-PL.[NOM]-3SG</ta>
            <ta e="T18" id="Seg_4337" s="T17">сын-PL.[NOM]-3SG</ta>
            <ta e="T19" id="Seg_4338" s="T18">лес-ILL</ta>
            <ta e="T20" id="Seg_4339" s="T19">отправиться-PST.NAR-3PL</ta>
            <ta e="T21" id="Seg_4340" s="T20">белка-CAP-CVB</ta>
            <ta e="T22" id="Seg_4341" s="T21">человек.[NOM]</ta>
            <ta e="T23" id="Seg_4342" s="T22">теперь</ta>
            <ta e="T24" id="Seg_4343" s="T23">убить-TR-CO-3SG.O</ta>
            <ta e="T25" id="Seg_4344" s="T24">как</ta>
            <ta e="T26" id="Seg_4345" s="T25">быть-CO.[3SG.S]</ta>
            <ta e="T27" id="Seg_4346" s="T26">десять</ta>
            <ta e="T28" id="Seg_4347" s="T27">селькуп-EP-ADJZ</ta>
            <ta e="T29" id="Seg_4348" s="T28">ребенок-DIM-ACC</ta>
            <ta e="T30" id="Seg_4349" s="T29">держать-PST.NAR-3PL</ta>
            <ta e="T31" id="Seg_4350" s="T30">этот</ta>
            <ta e="T32" id="Seg_4351" s="T31">ненец-GEN</ta>
            <ta e="T33" id="Seg_4352" s="T32">это-ACC</ta>
            <ta e="T34" id="Seg_4353" s="T33">послать-INFER-3SG.O</ta>
            <ta e="T35" id="Seg_4354" s="T34">заяц-ADJZ</ta>
            <ta e="T36" id="Seg_4355" s="T35">одежда.[NOM]</ta>
            <ta e="T37" id="Seg_4356" s="T36">старик-ALL</ta>
            <ta e="T38" id="Seg_4357" s="T37">мы.PL.NOM</ta>
            <ta e="T39" id="Seg_4358" s="T38">завтра</ta>
            <ta e="T40" id="Seg_4359" s="T39">будто</ta>
            <ta e="T41" id="Seg_4360" s="T40">прийти-FUT-1PL</ta>
            <ta e="T42" id="Seg_4361" s="T41">отправиться-IPFV-IMP.2PL</ta>
            <ta e="T43" id="Seg_4362" s="T42">отправиться-IPFV-IMP.2PL.S</ta>
            <ta e="T44" id="Seg_4363" s="T43">слово-TRL</ta>
            <ta e="T45" id="Seg_4364" s="T44">парень-DU.[NOM]</ta>
            <ta e="T46" id="Seg_4365" s="T45">домой</ta>
            <ta e="T47" id="Seg_4366" s="T46">отправиться-CVB</ta>
            <ta e="T48" id="Seg_4367" s="T47">так</ta>
            <ta e="T49" id="Seg_4368" s="T48">сказать-CO-3SG.O</ta>
            <ta e="T50" id="Seg_4369" s="T49">сирота-ADJZ</ta>
            <ta e="T51" id="Seg_4370" s="T50">ребенок-DU-ADJZ</ta>
            <ta e="T52" id="Seg_4371" s="T51">ненец-DU.[NOM]</ta>
            <ta e="T53" id="Seg_4372" s="T52">держать-CO-3SG.O</ta>
            <ta e="T54" id="Seg_4373" s="T53">потом</ta>
            <ta e="T55" id="Seg_4374" s="T54">два-ADJZ</ta>
            <ta e="T56" id="Seg_4375" s="T55">старик.[NOM]</ta>
            <ta e="T57" id="Seg_4376" s="T56">куда</ta>
            <ta e="T58" id="Seg_4377" s="T57">куда.деваться-FUT.[3SG.S]</ta>
            <ta e="T59" id="Seg_4378" s="T58">старик.[NOM]</ta>
            <ta e="T60" id="Seg_4379" s="T59">такой-ADVZ</ta>
            <ta e="T61" id="Seg_4380" s="T60">сказать.[3SG.S]</ta>
            <ta e="T62" id="Seg_4381" s="T61">этот</ta>
            <ta e="T63" id="Seg_4382" s="T62">заяц-ADJZ</ta>
            <ta e="T64" id="Seg_4383" s="T63">одежда.[NOM]</ta>
            <ta e="T65" id="Seg_4384" s="T64">старик.[NOM]</ta>
            <ta e="T66" id="Seg_4385" s="T65">жена.сына-OBL.3SG-ADJZ</ta>
            <ta e="T67" id="Seg_4386" s="T66">нечто-ALL</ta>
            <ta e="T68" id="Seg_4387" s="T67">жир.[NOM]</ta>
            <ta e="T69" id="Seg_4388" s="T68">еда.[NOM]</ta>
            <ta e="T70" id="Seg_4389" s="T69">принести-IMP.2PL.S</ta>
            <ta e="T71" id="Seg_4390" s="T70">где</ta>
            <ta e="T72" id="Seg_4391" s="T71">всё-ADJZ</ta>
            <ta e="T73" id="Seg_4392" s="T72">что.[NOM]</ta>
            <ta e="T74" id="Seg_4393" s="T73">принести-IMP.2PL.S</ta>
            <ta e="T75" id="Seg_4394" s="T74">ненец-PL.[NOM]</ta>
            <ta e="T76" id="Seg_4395" s="T75">завтра</ta>
            <ta e="T77" id="Seg_4396" s="T76">прийти-FUT-3PL</ta>
            <ta e="T78" id="Seg_4397" s="T77">съесть-EP-FRQ-FUT-3PL</ta>
            <ta e="T79" id="Seg_4398" s="T78">старик.[NOM]</ta>
            <ta e="T80" id="Seg_4399" s="T79">так</ta>
            <ta e="T81" id="Seg_4400" s="T80">сказать-3SG.O</ta>
            <ta e="T82" id="Seg_4401" s="T81">еда-ACC</ta>
            <ta e="T83" id="Seg_4402" s="T82">принести-3PL</ta>
            <ta e="T84" id="Seg_4403" s="T83">этот</ta>
            <ta e="T85" id="Seg_4404" s="T84">чум-EP-GEN</ta>
            <ta e="T86" id="Seg_4405" s="T85">нутро-ACC</ta>
            <ta e="T87" id="Seg_4406" s="T86">новый</ta>
            <ta e="T88" id="Seg_4407" s="T87">сено-INSTR</ta>
            <ta e="T89" id="Seg_4408" s="T88">постелить-3PL</ta>
            <ta e="T90" id="Seg_4409" s="T89">старик.[NOM]</ta>
            <ta e="T91" id="Seg_4410" s="T90">ненец-ACC-3SG</ta>
            <ta e="T92" id="Seg_4411" s="T91">еда-TR-INF-собраться-CO-3SG.O</ta>
            <ta e="T93" id="Seg_4412" s="T92">ой</ta>
            <ta e="T94" id="Seg_4413" s="T93">следующий</ta>
            <ta e="T95" id="Seg_4414" s="T94">день.[NOM]</ta>
            <ta e="T96" id="Seg_4415" s="T95">ненец-PL.[NOM]</ta>
            <ta e="T97" id="Seg_4416" s="T96">INFER</ta>
            <ta e="T98" id="Seg_4417" s="T97">RFL</ta>
            <ta e="T99" id="Seg_4418" s="T98">принести-INFER-3PL</ta>
            <ta e="T100" id="Seg_4419" s="T99">три-десять-EP-ADJZ</ta>
            <ta e="T101" id="Seg_4420" s="T100">человек-EP-ADJZ</ta>
            <ta e="T102" id="Seg_4421" s="T101">войско.[NOM]</ta>
            <ta e="T103" id="Seg_4422" s="T102">вроде</ta>
            <ta e="T104" id="Seg_4423" s="T103">кончиться-PTCP.PST</ta>
            <ta e="T105" id="Seg_4424" s="T104">чум-LOC</ta>
            <ta e="T106" id="Seg_4425" s="T105">заяц-ADJZ</ta>
            <ta e="T107" id="Seg_4426" s="T106">одежда.[NOM]</ta>
            <ta e="T108" id="Seg_4427" s="T107">старик.[NOM]</ta>
            <ta e="T109" id="Seg_4428" s="T108">сам.3SG-ADV.LOC</ta>
            <ta e="T110" id="Seg_4429" s="T109">жена.сына-PL-OBL.3SG-COM</ta>
            <ta e="T111" id="Seg_4430" s="T110">свежий</ta>
            <ta e="T112" id="Seg_4431" s="T111">сено-INSTR</ta>
            <ta e="T113" id="Seg_4432" s="T112">постелить-FRQ-FRQ-3PL</ta>
            <ta e="T114" id="Seg_4433" s="T113">постелить-FRQ-FRQ-EP-PST.NAR-3PL</ta>
            <ta e="T115" id="Seg_4434" s="T114">старик.[NOM]</ta>
            <ta e="T116" id="Seg_4435" s="T115">такой-ADVZ</ta>
            <ta e="T117" id="Seg_4436" s="T116">сказать.[3SG.S]</ta>
            <ta e="T118" id="Seg_4437" s="T117">съесть-EP-FRQ-INF-начать-CO-3PL</ta>
            <ta e="T119" id="Seg_4438" s="T118">еда-ACC</ta>
            <ta e="T120" id="Seg_4439" s="T119">найти-INFER-3PL</ta>
            <ta e="T121" id="Seg_4440" s="T120">старик.[NOM]</ta>
            <ta e="T122" id="Seg_4441" s="T121">так</ta>
            <ta e="T123" id="Seg_4442" s="T122">сказать-CO-3SG.O</ta>
            <ta e="T124" id="Seg_4443" s="T123">старуха.[NOM]-3SG</ta>
            <ta e="T125" id="Seg_4444" s="T124">тоже</ta>
            <ta e="T126" id="Seg_4445" s="T125">быть-CO.[3SG.S]</ta>
            <ta e="T127" id="Seg_4446" s="T126">жена.сына-PL-ACC-3SG</ta>
            <ta e="T128" id="Seg_4447" s="T127">так</ta>
            <ta e="T129" id="Seg_4448" s="T128">идти-CAUS-3SG.O</ta>
            <ta e="T130" id="Seg_4449" s="T129">наружу</ta>
            <ta e="T131" id="Seg_4450" s="T130">выйти-IMP.2PL</ta>
            <ta e="T132" id="Seg_4451" s="T131">ребенок-PL-EP-ACC</ta>
            <ta e="T133" id="Seg_4452" s="T132">наружу</ta>
            <ta e="T134" id="Seg_4453" s="T133">принести-IMP.2PL</ta>
            <ta e="T135" id="Seg_4454" s="T134">старик.[NOM]</ta>
            <ta e="T136" id="Seg_4455" s="T135">сам.3SG.[NOM]</ta>
            <ta e="T137" id="Seg_4456" s="T136">держать-HAB.[3SG.S]</ta>
            <ta e="T138" id="Seg_4457" s="T137">человек-EP-ACC-3SG</ta>
            <ta e="T139" id="Seg_4458" s="T138">еда-TR-DUR-CVB</ta>
            <ta e="T140" id="Seg_4459" s="T139">один</ta>
            <ta e="T141" id="Seg_4460" s="T140">целый</ta>
            <ta e="T142" id="Seg_4461" s="T141">середина-LOC</ta>
            <ta e="T143" id="Seg_4462" s="T142">прийти-INFER.[3SG.S]</ta>
            <ta e="T144" id="Seg_4463" s="T143">утро-ADV.LOC</ta>
            <ta e="T145" id="Seg_4464" s="T144">человек.[NOM]</ta>
            <ta e="T146" id="Seg_4465" s="T145">съесть-EP-FRQ-CO-3PL</ta>
            <ta e="T147" id="Seg_4466" s="T146">очаг-EP-ADJZ</ta>
            <ta e="T148" id="Seg_4467" s="T147">очаг.[NOM]</ta>
            <ta e="T149" id="Seg_4468" s="T148">огонь-ACC-3SG</ta>
            <ta e="T150" id="Seg_4469" s="T149">это-ACC</ta>
            <ta e="T151" id="Seg_4470" s="T150">большой</ta>
            <ta e="T152" id="Seg_4471" s="T151">дрова-INSTR</ta>
            <ta e="T153" id="Seg_4472" s="T152">поддерживать.огонь-CO-3SG.O</ta>
            <ta e="T154" id="Seg_4473" s="T153">гореть-INFER.[3SG.S]</ta>
            <ta e="T155" id="Seg_4474" s="T154">жена.сына-PL-ACC-3SG</ta>
            <ta e="T156" id="Seg_4475" s="T155">NEG</ta>
            <ta e="T157" id="Seg_4476" s="T156">зачем</ta>
            <ta e="T158" id="Seg_4477" s="T157">что-TRL</ta>
            <ta e="T159" id="Seg_4478" s="T158">наружу</ta>
            <ta e="T160" id="Seg_4479" s="T159">послать-CO-3SG.O</ta>
            <ta e="T161" id="Seg_4480" s="T160">пространство.снаружи-ADV.LOC</ta>
            <ta e="T162" id="Seg_4481" s="T161">огонь.[NOM]</ta>
            <ta e="T163" id="Seg_4482" s="T162">зажечь-INFER-3PL</ta>
            <ta e="T164" id="Seg_4483" s="T163">огонь-ACC</ta>
            <ta e="T165" id="Seg_4484" s="T164">зажечь-MULO-PST.NAR-3PL</ta>
            <ta e="T166" id="Seg_4485" s="T165">тот</ta>
            <ta e="T167" id="Seg_4486" s="T166">береста-ACC</ta>
            <ta e="T168" id="Seg_4487" s="T167">факел-EP-VBLZ-MOM-PST.NAR-3PL</ta>
            <ta e="T169" id="Seg_4488" s="T168">такой-ADVZ</ta>
            <ta e="T170" id="Seg_4489" s="T169">что.[NOM]</ta>
            <ta e="T171" id="Seg_4490" s="T170">сохнуть-CAUS-PST.NAR-3PL</ta>
            <ta e="T172" id="Seg_4491" s="T171">вы.PL.NOM</ta>
            <ta e="T173" id="Seg_4492" s="T172">вот</ta>
            <ta e="T174" id="Seg_4493" s="T173">быть-PTCP.PRS</ta>
            <ta e="T175" id="Seg_4494" s="T174">я.NOM</ta>
            <ta e="T176" id="Seg_4495" s="T175">вот</ta>
            <ta e="T177" id="Seg_4496" s="T176">выйти-FUT-1SG.S</ta>
            <ta e="T178" id="Seg_4497" s="T177">съесть-EP-FRQ-CO-3PL</ta>
            <ta e="T179" id="Seg_4498" s="T178">ненец-PL.[NOM]</ta>
            <ta e="T180" id="Seg_4499" s="T179">так</ta>
            <ta e="T181" id="Seg_4500" s="T180">сказать-3PL</ta>
            <ta e="T182" id="Seg_4501" s="T181">дедушка.[NOM]</ta>
            <ta e="T183" id="Seg_4502" s="T182">очаг-EP-ADJZ</ta>
            <ta e="T184" id="Seg_4503" s="T183">что-EP-TRL</ta>
            <ta e="T185" id="Seg_4504" s="T184">столько</ta>
            <ta e="T186" id="Seg_4505" s="T185">дрова-INSTR</ta>
            <ta e="T187" id="Seg_4506" s="T186">поддерживать.огонь-2SG.O</ta>
            <ta e="T188" id="Seg_4507" s="T187">NEG</ta>
            <ta e="T189" id="Seg_4508" s="T188">очаг.[NOM]</ta>
            <ta e="T190" id="Seg_4509" s="T189">нечто.[NOM]</ta>
            <ta e="T191" id="Seg_4510" s="T190">огонь.[NOM]</ta>
            <ta e="T192" id="Seg_4511" s="T191">вот</ta>
            <ta e="T193" id="Seg_4512" s="T192">съесть-FUT-3SG.O</ta>
            <ta e="T194" id="Seg_4513" s="T193">он(а).[NOM]</ta>
            <ta e="T195" id="Seg_4514" s="T194">NEG</ta>
            <ta e="T196" id="Seg_4515" s="T195">съесть-FUT-3SG.O</ta>
            <ta e="T197" id="Seg_4516" s="T196">так</ta>
            <ta e="T198" id="Seg_4517" s="T197">находиться-CAUS-PST.NAR.[3SG.S]</ta>
            <ta e="T199" id="Seg_4518" s="T198">заяц-ADJZ</ta>
            <ta e="T200" id="Seg_4519" s="T199">одежда-GEN-3SG</ta>
            <ta e="T201" id="Seg_4520" s="T200">лохмотья-ADJZ</ta>
            <ta e="T202" id="Seg_4521" s="T201">кусок.[NOM]</ta>
            <ta e="T203" id="Seg_4522" s="T202">это.[NOM]</ta>
            <ta e="T204" id="Seg_4523" s="T203">войти-PST.NAR-INFER-3SG.O</ta>
            <ta e="T205" id="Seg_4524" s="T204">очаг-GEN.3SG</ta>
            <ta e="T206" id="Seg_4525" s="T205">рядом-LOC</ta>
            <ta e="T207" id="Seg_4526" s="T206">большой</ta>
            <ta e="T208" id="Seg_4527" s="T207">дверь-GEN-3SG</ta>
            <ta e="T209" id="Seg_4528" s="T208">рядом-LOC</ta>
            <ta e="T210" id="Seg_4529" s="T209">один</ta>
            <ta e="T211" id="Seg_4530" s="T210">целый</ta>
            <ta e="T212" id="Seg_4531" s="T211">середина-LOC</ta>
            <ta e="T213" id="Seg_4532" s="T212">пространство.снаружи-ADV.EL</ta>
            <ta e="T214" id="Seg_4533" s="T213">дом-ILL</ta>
            <ta e="T215" id="Seg_4534" s="T214">огонь-ADJZ</ta>
            <ta e="T216" id="Seg_4535" s="T215">факел-EP-ADJZ</ta>
            <ta e="T217" id="Seg_4536" s="T216">кусок.[NOM]</ta>
            <ta e="T218" id="Seg_4537" s="T217">упасть-IPFV.[3SG.S]</ta>
            <ta e="T219" id="Seg_4538" s="T218">INFER</ta>
            <ta e="T220" id="Seg_4539" s="T219">побежать-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T221" id="Seg_4540" s="T220">дверь-ILL</ta>
            <ta e="T222" id="Seg_4541" s="T221">ненец-PL.[NOM]</ta>
            <ta e="T223" id="Seg_4542" s="T222">едва</ta>
            <ta e="T224" id="Seg_4543" s="T223">схватить-PST.NAR-3PL</ta>
            <ta e="T225" id="Seg_4544" s="T224">старик-ACC</ta>
            <ta e="T226" id="Seg_4545" s="T225">только</ta>
            <ta e="T227" id="Seg_4546" s="T226">исключительный</ta>
            <ta e="T228" id="Seg_4547" s="T227">заяц-ADJZ</ta>
            <ta e="T229" id="Seg_4548" s="T228">одежда.[NOM]</ta>
            <ta e="T230" id="Seg_4549" s="T229">кусок-ACC</ta>
            <ta e="T231" id="Seg_4550" s="T230">схватить-PST.NAR-3PL</ta>
            <ta e="T232" id="Seg_4551" s="T231">вроде</ta>
            <ta e="T233" id="Seg_4552" s="T232">вот</ta>
            <ta e="T234" id="Seg_4553" s="T233">прийти-PST.[3SG.S]</ta>
            <ta e="T235" id="Seg_4554" s="T234">окно-GEN</ta>
            <ta e="T236" id="Seg_4555" s="T235">отверстие-PROL</ta>
            <ta e="T237" id="Seg_4556" s="T236">дом-GEN.3SG</ta>
            <ta e="T238" id="Seg_4557" s="T237">два</ta>
            <ta e="T239" id="Seg_4558" s="T238">окно.[NOM]-3SG</ta>
            <ta e="T240" id="Seg_4559" s="T239">быть-DUR-PST.NAR.[3SG.S]</ta>
            <ta e="T241" id="Seg_4560" s="T240">что.ли</ta>
            <ta e="T242" id="Seg_4561" s="T241">большой</ta>
            <ta e="T243" id="Seg_4562" s="T242">дом.[NOM]-3SG</ta>
            <ta e="T244" id="Seg_4563" s="T243">быть-DUR-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T245" id="Seg_4564" s="T244">огонь-INSTR</ta>
            <ta e="T246" id="Seg_4565" s="T245">закрыть-PST.NAR.[3SG.S]</ta>
            <ta e="T247" id="Seg_4566" s="T246">стоять.торчком-EP-TR.[3SG.S]</ta>
            <ta e="T248" id="Seg_4567" s="T247">дерево-ADJZ</ta>
            <ta e="T249" id="Seg_4568" s="T248">доска-PL.[NOM]</ta>
            <ta e="T250" id="Seg_4569" s="T249">топор-GEN</ta>
            <ta e="T251" id="Seg_4570" s="T250">ручка-GEN</ta>
            <ta e="T252" id="Seg_4571" s="T251">высота.[NOM]</ta>
            <ta e="T253" id="Seg_4572" s="T252">закрыть-PST.NAR.[3SG.S]</ta>
            <ta e="T254" id="Seg_4573" s="T253">тот.[NOM]</ta>
            <ta e="T255" id="Seg_4574" s="T254">огонь-ACC</ta>
            <ta e="T256" id="Seg_4575" s="T255">INFER</ta>
            <ta e="T257" id="Seg_4576" s="T256">бросать-MOM-PST.NAR-INFER-3PL</ta>
            <ta e="T258" id="Seg_4577" s="T257">дом-ILL</ta>
            <ta e="T259" id="Seg_4578" s="T258">порох-INSTR</ta>
            <ta e="T260" id="Seg_4579" s="T259">давно</ta>
            <ta e="T261" id="Seg_4580" s="T260">сыпать-IPFV-PST.NAR.[3SG.S]</ta>
            <ta e="T262" id="Seg_4581" s="T261">этот</ta>
            <ta e="T263" id="Seg_4582" s="T262">сено-GEN</ta>
            <ta e="T264" id="Seg_4583" s="T263">низ-ILL</ta>
            <ta e="T265" id="Seg_4584" s="T264">огонь.[NOM]</ta>
            <ta e="T266" id="Seg_4585" s="T265">INFER</ta>
            <ta e="T267" id="Seg_4586" s="T266">ударить-PST.NAR-INFER-3SG.O</ta>
            <ta e="T268" id="Seg_4587" s="T267">тот</ta>
            <ta e="T269" id="Seg_4588" s="T268">три-десять-EP-ADJZ</ta>
            <ta e="T270" id="Seg_4589" s="T269">человек-EP-ADJZ</ta>
            <ta e="T271" id="Seg_4590" s="T270">войско.[NOM]</ta>
            <ta e="T272" id="Seg_4591" s="T271">огонь.[NOM]</ta>
            <ta e="T273" id="Seg_4592" s="T272">съесть-PST.NAR-3SG.O</ta>
            <ta e="T274" id="Seg_4593" s="T273">отправиться-CO-3PL</ta>
            <ta e="T275" id="Seg_4594" s="T274">вверх-ADV.EL-в.некоторой.степени</ta>
            <ta e="T276" id="Seg_4595" s="T275">жена.сына-PL.[NOM]-3SG</ta>
            <ta e="T277" id="Seg_4596" s="T276">старик.[NOM]</ta>
            <ta e="T278" id="Seg_4597" s="T277">наружу</ta>
            <ta e="T279" id="Seg_4598" s="T278">побежать-CVB</ta>
            <ta e="T280" id="Seg_4599" s="T279">топор-ADJZ</ta>
            <ta e="T281" id="Seg_4600" s="T280">рука.[NOM]-3SG</ta>
            <ta e="T282" id="Seg_4601" s="T281">держать-HAB.[3SG.S]</ta>
            <ta e="T283" id="Seg_4602" s="T282">каждый</ta>
            <ta e="T284" id="Seg_4603" s="T283">едва</ta>
            <ta e="T285" id="Seg_4604" s="T284">наружу</ta>
            <ta e="T286" id="Seg_4605" s="T285">показаться-US-PFV-3PL</ta>
            <ta e="T287" id="Seg_4606" s="T286">тоже</ta>
            <ta e="T288" id="Seg_4607" s="T287">в.лицо</ta>
            <ta e="T289" id="Seg_4608" s="T288">ударить-3PL</ta>
            <ta e="T290" id="Seg_4609" s="T289">жена.сына-PL.[NOM]-3SG</ta>
            <ta e="T291" id="Seg_4610" s="T290">огонь-ACC</ta>
            <ta e="T292" id="Seg_4611" s="T291">INFER</ta>
            <ta e="T293" id="Seg_4612" s="T292">сыпать-PST.NAR-INFER-3PL</ta>
            <ta e="T294" id="Seg_4613" s="T293">очаг.[NOM]</ta>
            <ta e="T295" id="Seg_4614" s="T294">верх.[NOM]</ta>
            <ta e="T296" id="Seg_4615" s="T295">сквозь</ta>
            <ta e="T297" id="Seg_4616" s="T296">вниз</ta>
            <ta e="T298" id="Seg_4617" s="T297">и</ta>
            <ta e="T299" id="Seg_4618" s="T298">два</ta>
            <ta e="T300" id="Seg_4619" s="T299">окно-GEN</ta>
            <ta e="T301" id="Seg_4620" s="T300">отверстие-ILL</ta>
            <ta e="T302" id="Seg_4621" s="T301">дом-ILL</ta>
            <ta e="T303" id="Seg_4622" s="T302">бросать-CO-3SG.O</ta>
            <ta e="T304" id="Seg_4623" s="T303">дом-EP-ACC</ta>
            <ta e="T305" id="Seg_4624" s="T304">огонь-INSTR</ta>
            <ta e="T306" id="Seg_4625" s="T305">зажечь-CO-3PL</ta>
            <ta e="T307" id="Seg_4626" s="T306">этот</ta>
            <ta e="T308" id="Seg_4627" s="T307">ненец-ADJZ</ta>
            <ta e="T309" id="Seg_4628" s="T308">войско-ACC</ta>
            <ta e="T310" id="Seg_4629" s="T309">войско-GEN</ta>
            <ta e="T311" id="Seg_4630" s="T310">голова-человек.[NOM]</ta>
            <ta e="T312" id="Seg_4631" s="T311">два</ta>
            <ta e="T313" id="Seg_4632" s="T312">быть-PST.NAR-INFER-3PL</ta>
            <ta e="T314" id="Seg_4633" s="T313">потом</ta>
            <ta e="T315" id="Seg_4634" s="T314">вниз</ta>
            <ta e="T316" id="Seg_4635" s="T315">уйти-CO.[3SG.S]</ta>
            <ta e="T317" id="Seg_4636" s="T316">ненец-PL-EP-GEN</ta>
            <ta e="T318" id="Seg_4637" s="T317">нарты-ILL</ta>
            <ta e="T319" id="Seg_4638" s="T318">вниз</ta>
            <ta e="T320" id="Seg_4639" s="T319">что.ли</ta>
            <ta e="T321" id="Seg_4640" s="T320">нарты-ILL</ta>
            <ta e="T322" id="Seg_4641" s="T321">уйти-CO.[3SG.S]</ta>
            <ta e="T323" id="Seg_4642" s="T322">и</ta>
            <ta e="T324" id="Seg_4643" s="T323">два</ta>
            <ta e="T325" id="Seg_4644" s="T324">жена.сына-DU.[NOM]-3SG</ta>
            <ta e="T326" id="Seg_4645" s="T325">отправиться-IPFV-TR-3SG.O</ta>
            <ta e="T327" id="Seg_4646" s="T326">два</ta>
            <ta e="T328" id="Seg_4647" s="T327">жена.сына-DU.[NOM]-3SG</ta>
            <ta e="T329" id="Seg_4648" s="T328">отправиться-IPFV-TR-3SG.O</ta>
            <ta e="T330" id="Seg_4649" s="T329">убить-MULO-DUR-PTCP.PRS</ta>
            <ta e="T331" id="Seg_4650" s="T330">человек-EP-PL-ILL-3SG</ta>
            <ta e="T332" id="Seg_4651" s="T331">вниз-ADV.EL</ta>
            <ta e="T333" id="Seg_4652" s="T332">отправиться-CO.[3SG.S]</ta>
            <ta e="T334" id="Seg_4653" s="T333">хромать-PTCP.PRS</ta>
            <ta e="T454" id="Seg_4654" s="T334">ненец.[NOM]</ta>
            <ta e="T335" id="Seg_4655" s="T454">старик.[NOM]</ta>
            <ta e="T336" id="Seg_4656" s="T335">показаться-US-PFV-INFER.[3SG.S]</ta>
            <ta e="T337" id="Seg_4657" s="T336">что.ли</ta>
            <ta e="T338" id="Seg_4658" s="T337">заяц-ADJZ</ta>
            <ta e="T339" id="Seg_4659" s="T338">одежда-ADJZ</ta>
            <ta e="T340" id="Seg_4660" s="T339">старик-GEN</ta>
            <ta e="T341" id="Seg_4661" s="T340">женщина-ACC</ta>
            <ta e="T342" id="Seg_4662" s="T341">оставить-US-EP-RES-3SG.O</ta>
            <ta e="T343" id="Seg_4663" s="T342">старик.[NOM]</ta>
            <ta e="T344" id="Seg_4664" s="T343">в.лицо</ta>
            <ta e="T345" id="Seg_4665" s="T344">ударить-CO-3SG.O</ta>
            <ta e="T346" id="Seg_4666" s="T345">потом</ta>
            <ta e="T347" id="Seg_4667" s="T346">вниз</ta>
            <ta e="T348" id="Seg_4668" s="T347">идти-INCH-CO.[3SG.S]</ta>
            <ta e="T349" id="Seg_4669" s="T348">три</ta>
            <ta e="T350" id="Seg_4670" s="T349">жена.сына-DYA-PL.[NOM]</ta>
            <ta e="T351" id="Seg_4671" s="T350">вниз.по.течению.реки</ta>
            <ta e="T352" id="Seg_4672" s="T351">отправиться-IPFV-CO-3PL</ta>
            <ta e="T353" id="Seg_4673" s="T352">туда</ta>
            <ta e="T354" id="Seg_4674" s="T353">тронуться-PFV-3SG.O</ta>
            <ta e="T355" id="Seg_4675" s="T354">ненец-PL-EP-GEN</ta>
            <ta e="T356" id="Seg_4676" s="T355">чум-ILL</ta>
            <ta e="T357" id="Seg_4677" s="T356">вниз.по.течению.реки</ta>
            <ta e="T358" id="Seg_4678" s="T357">уйти-CO-3PL</ta>
            <ta e="T359" id="Seg_4679" s="T358">ненец-PL-EP-GEN</ta>
            <ta e="T360" id="Seg_4680" s="T359">чум-ILL</ta>
            <ta e="T361" id="Seg_4681" s="T360">женщина.[NOM]-3PL</ta>
            <ta e="T362" id="Seg_4682" s="T361">вниз</ta>
            <ta e="T363" id="Seg_4683" s="T362">уйти-CO-3PL</ta>
            <ta e="T364" id="Seg_4684" s="T363">там</ta>
            <ta e="T365" id="Seg_4685" s="T364">чум-LOC</ta>
            <ta e="T366" id="Seg_4686" s="T365">селькуп-EP-ADJZ</ta>
            <ta e="T367" id="Seg_4687" s="T366">два</ta>
            <ta e="T368" id="Seg_4688" s="T367">ребенок-DU-ACC</ta>
            <ta e="T369" id="Seg_4689" s="T368">оставить-PST.NAR-3PL</ta>
            <ta e="T370" id="Seg_4690" s="T369">давно</ta>
            <ta e="T371" id="Seg_4691" s="T370">заяц-ADJZ</ta>
            <ta e="T372" id="Seg_4692" s="T371">одежда-ADJZ</ta>
            <ta e="T373" id="Seg_4693" s="T372">старик-GEN</ta>
            <ta e="T374" id="Seg_4694" s="T373">внук-DU.[NOM]</ta>
            <ta e="T375" id="Seg_4695" s="T374">это-GEN</ta>
            <ta e="T376" id="Seg_4696" s="T375">ребенок-DU.[NOM]</ta>
            <ta e="T377" id="Seg_4697" s="T376">олень-ACC</ta>
            <ta e="T378" id="Seg_4698" s="T377">погнать-CVB</ta>
            <ta e="T379" id="Seg_4699" s="T378">вверх</ta>
            <ta e="T380" id="Seg_4700" s="T379">уйти-CO-3PL</ta>
            <ta e="T381" id="Seg_4701" s="T380">вверх</ta>
            <ta e="T382" id="Seg_4702" s="T381">чум-TR-CO-3PL</ta>
            <ta e="T383" id="Seg_4703" s="T382">один</ta>
            <ta e="T384" id="Seg_4704" s="T383">ночь-ADV.LOC</ta>
            <ta e="T385" id="Seg_4705" s="T384">ночевать-3PL</ta>
            <ta e="T386" id="Seg_4706" s="T385">сын-PL.[NOM]-3SG</ta>
            <ta e="T387" id="Seg_4707" s="T386">этот</ta>
            <ta e="T388" id="Seg_4708" s="T387">день.[NOM]</ta>
            <ta e="T389" id="Seg_4709" s="T388">домой</ta>
            <ta e="T390" id="Seg_4710" s="T389">прийти-CO-3PL</ta>
            <ta e="T391" id="Seg_4711" s="T390">отец.[NOM]</ta>
            <ta e="T392" id="Seg_4712" s="T391">старик-ACC-3SG</ta>
            <ta e="T393" id="Seg_4713" s="T392">отправиться-TR-CVB</ta>
            <ta e="T394" id="Seg_4714" s="T393">прийти-CO-3PL</ta>
            <ta e="T395" id="Seg_4715" s="T394">сын-PL.[NOM]-3SG</ta>
            <ta e="T396" id="Seg_4716" s="T395">он(а)-EP-PL.[NOM]</ta>
            <ta e="T397" id="Seg_4717" s="T396">такой-ADVZ</ta>
            <ta e="T398" id="Seg_4718" s="T397">думать-FRQ-PST.NAR-3PL</ta>
            <ta e="T399" id="Seg_4719" s="T398">я.GEN</ta>
            <ta e="T400" id="Seg_4720" s="T399">отец.[NOM]-1PL</ta>
            <ta e="T401" id="Seg_4721" s="T400">убить-PST.NAR-3PL</ta>
            <ta e="T402" id="Seg_4722" s="T401">он(а).[NOM]</ta>
            <ta e="T403" id="Seg_4723" s="T402">семь</ta>
            <ta e="T404" id="Seg_4724" s="T403">сын.[NOM]-3SG</ta>
            <ta e="T405" id="Seg_4725" s="T404">быть-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T406" id="Seg_4726" s="T405">младший</ta>
            <ta e="T407" id="Seg_4727" s="T406">жена.сына.[NOM]-3SG</ta>
            <ta e="T408" id="Seg_4728" s="T407">он(а)-EP-PL.[NOM]</ta>
            <ta e="T409" id="Seg_4729" s="T408">отправиться-TR-CVB</ta>
            <ta e="T410" id="Seg_4730" s="T409">привезти-3PL</ta>
            <ta e="T411" id="Seg_4731" s="T410">сын-GEN-3SG</ta>
            <ta e="T412" id="Seg_4732" s="T411">собака-ACC</ta>
            <ta e="T413" id="Seg_4733" s="T412">пространство.снаружи-ADV.LOC</ta>
            <ta e="T414" id="Seg_4734" s="T413">привязать-PST.NAR-3PL</ta>
            <ta e="T415" id="Seg_4735" s="T414">олень-PL.[NOM]</ta>
            <ta e="T416" id="Seg_4736" s="T415">подождать-DUR-ACTN-LOC</ta>
            <ta e="T417" id="Seg_4737" s="T416">он(а)-EP-PL.[NOM]</ta>
            <ta e="T418" id="Seg_4738" s="T417">собака-GEN</ta>
            <ta e="T419" id="Seg_4739" s="T418">нога-ACC</ta>
            <ta e="T420" id="Seg_4740" s="T419">наступить-CO-3PL</ta>
            <ta e="T421" id="Seg_4741" s="T420">старик.[NOM]</ta>
            <ta e="T422" id="Seg_4742" s="T421">такой-ADVZ</ta>
            <ta e="T423" id="Seg_4743" s="T422">сказать-CO-3SG.O</ta>
            <ta e="T424" id="Seg_4744" s="T423">собака-GEN</ta>
            <ta e="T425" id="Seg_4745" s="T424">нога-ACC</ta>
            <ta e="T426" id="Seg_4746" s="T425">что.[NOM]</ta>
            <ta e="T427" id="Seg_4747" s="T426">наступить-CO-3SG.O</ta>
            <ta e="T428" id="Seg_4748" s="T427">младший</ta>
            <ta e="T429" id="Seg_4749" s="T428">сын-GEN-3SG</ta>
            <ta e="T430" id="Seg_4750" s="T429">жена.[NOM]</ta>
            <ta e="T431" id="Seg_4751" s="T430">наружу</ta>
            <ta e="T432" id="Seg_4752" s="T431">побежать.[3SG.S]</ta>
            <ta e="T433" id="Seg_4753" s="T432">значит</ta>
            <ta e="T434" id="Seg_4754" s="T433">свой.3SG</ta>
            <ta e="T435" id="Seg_4755" s="T434">муж.[NOM]-3SG</ta>
            <ta e="T436" id="Seg_4756" s="T435">прийти-INFER.[3SG.S]</ta>
            <ta e="T437" id="Seg_4757" s="T436">значит</ta>
            <ta e="T438" id="Seg_4758" s="T437">свой.3SG</ta>
            <ta e="T439" id="Seg_4759" s="T438">сын.[NOM]-3SG</ta>
            <ta e="T440" id="Seg_4760" s="T439">прийти-PST.NAR.[3SG.S]</ta>
            <ta e="T441" id="Seg_4761" s="T440">потом</ta>
            <ta e="T442" id="Seg_4762" s="T441">дедушка.[NOM]-3PL</ta>
            <ta e="T443" id="Seg_4763" s="T442">такой-ADVZ</ta>
            <ta e="T444" id="Seg_4764" s="T443">сказать-PST.NAR-3SG.O</ta>
            <ta e="T445" id="Seg_4765" s="T444">видать</ta>
            <ta e="T446" id="Seg_4766" s="T445">сам.1PL.[NOM]</ta>
            <ta e="T447" id="Seg_4767" s="T446">огонь-INSTR</ta>
            <ta e="T448" id="Seg_4768" s="T447">зажечь-PST-1PL</ta>
            <ta e="T449" id="Seg_4769" s="T448">ненец-ADJZ</ta>
            <ta e="T450" id="Seg_4770" s="T449">земля-ADV.LOC</ta>
            <ta e="T451" id="Seg_4771" s="T450">олень-ACC</ta>
            <ta e="T452" id="Seg_4772" s="T451">вперёд</ta>
            <ta e="T453" id="Seg_4773" s="T452">погнать-CO-3SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_4774" s="T0">n-n&gt;adj</ta>
            <ta e="T2" id="Seg_4775" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_4776" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_4777" s="T3">num</ta>
            <ta e="T5" id="Seg_4778" s="T4">n-n:case-n:poss</ta>
            <ta e="T6" id="Seg_4779" s="T5">v-v:tense-v:mood-v:pn</ta>
            <ta e="T7" id="Seg_4780" s="T6">conj</ta>
            <ta e="T8" id="Seg_4781" s="T7">num</ta>
            <ta e="T9" id="Seg_4782" s="T8">n-n:case-n:poss</ta>
            <ta e="T10" id="Seg_4783" s="T9">n-n:num-n:case</ta>
            <ta e="T11" id="Seg_4784" s="T10">v-v:ins-v:pn</ta>
            <ta e="T12" id="Seg_4785" s="T11">adv-adv:case</ta>
            <ta e="T13" id="Seg_4786" s="T12">adv-adv:case</ta>
            <ta e="T14" id="Seg_4787" s="T13">adv</ta>
            <ta e="T15" id="Seg_4788" s="T14">pers-n:ins-n:case</ta>
            <ta e="T16" id="Seg_4789" s="T15">v-v:inf.poss</ta>
            <ta e="T17" id="Seg_4790" s="T16">n-n:num-n:case-n:poss</ta>
            <ta e="T18" id="Seg_4791" s="T17">n-n:num-n:case-n:poss</ta>
            <ta e="T19" id="Seg_4792" s="T18">n-n:case</ta>
            <ta e="T20" id="Seg_4793" s="T19">v-v:tense-v:pn</ta>
            <ta e="T21" id="Seg_4794" s="T20">n-n&gt;v-v&gt;adv</ta>
            <ta e="T22" id="Seg_4795" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_4796" s="T22">adv</ta>
            <ta e="T24" id="Seg_4797" s="T23">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T25" id="Seg_4798" s="T24">interrog</ta>
            <ta e="T26" id="Seg_4799" s="T25">v-v:ins-v:pn</ta>
            <ta e="T27" id="Seg_4800" s="T26">num</ta>
            <ta e="T28" id="Seg_4801" s="T27">n-n:ins-n&gt;adj</ta>
            <ta e="T29" id="Seg_4802" s="T28">n-n&gt;n-n:case</ta>
            <ta e="T30" id="Seg_4803" s="T29">v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_4804" s="T30">dem</ta>
            <ta e="T32" id="Seg_4805" s="T31">n-n:case</ta>
            <ta e="T33" id="Seg_4806" s="T32">pro-n:case</ta>
            <ta e="T34" id="Seg_4807" s="T33">v-v:tense.mood-v:pn</ta>
            <ta e="T35" id="Seg_4808" s="T34">n-n&gt;adj</ta>
            <ta e="T36" id="Seg_4809" s="T35">n-n:case</ta>
            <ta e="T37" id="Seg_4810" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_4811" s="T37">pers</ta>
            <ta e="T39" id="Seg_4812" s="T38">adv</ta>
            <ta e="T40" id="Seg_4813" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_4814" s="T40">v-v:tense-v:pn</ta>
            <ta e="T42" id="Seg_4815" s="T41">v-v&gt;v-v:mood.pn</ta>
            <ta e="T43" id="Seg_4816" s="T42">v-v&gt;v-v:mood.pn</ta>
            <ta e="T44" id="Seg_4817" s="T43">n-n:case</ta>
            <ta e="T45" id="Seg_4818" s="T44">n-n:num-n:case</ta>
            <ta e="T46" id="Seg_4819" s="T45">adv</ta>
            <ta e="T47" id="Seg_4820" s="T46">v-v&gt;adv</ta>
            <ta e="T48" id="Seg_4821" s="T47">adv</ta>
            <ta e="T49" id="Seg_4822" s="T48">v-v:ins-v:pn</ta>
            <ta e="T50" id="Seg_4823" s="T49">n-n&gt;adj</ta>
            <ta e="T51" id="Seg_4824" s="T50">n-n:num-n&gt;adj</ta>
            <ta e="T52" id="Seg_4825" s="T51">n-n:num-n:case</ta>
            <ta e="T53" id="Seg_4826" s="T52">v-v:ins-v:pn</ta>
            <ta e="T54" id="Seg_4827" s="T53">adv</ta>
            <ta e="T55" id="Seg_4828" s="T54">num-num&gt;adj</ta>
            <ta e="T56" id="Seg_4829" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_4830" s="T56">interrog</ta>
            <ta e="T58" id="Seg_4831" s="T57">qv-v:tense-v:pn</ta>
            <ta e="T59" id="Seg_4832" s="T58">n-n:case</ta>
            <ta e="T60" id="Seg_4833" s="T59">dem-adj&gt;adv</ta>
            <ta e="T61" id="Seg_4834" s="T60">v-v:pn</ta>
            <ta e="T62" id="Seg_4835" s="T61">dem</ta>
            <ta e="T63" id="Seg_4836" s="T62">n-n&gt;adj</ta>
            <ta e="T64" id="Seg_4837" s="T63">n-n:case</ta>
            <ta e="T65" id="Seg_4838" s="T64">n-n:case</ta>
            <ta e="T66" id="Seg_4839" s="T65">n-n:obl.poss-n&gt;adj</ta>
            <ta e="T67" id="Seg_4840" s="T66">n-n:case</ta>
            <ta e="T68" id="Seg_4841" s="T67">n-n:case</ta>
            <ta e="T69" id="Seg_4842" s="T68">n-n:case</ta>
            <ta e="T70" id="Seg_4843" s="T69">v-v:mood.pn</ta>
            <ta e="T71" id="Seg_4844" s="T70">interrog</ta>
            <ta e="T72" id="Seg_4845" s="T71">quant-n&gt;adj</ta>
            <ta e="T73" id="Seg_4846" s="T72">interrog-n:case</ta>
            <ta e="T74" id="Seg_4847" s="T73">v-v:mood.pn</ta>
            <ta e="T75" id="Seg_4848" s="T74">n-n:num-n:case</ta>
            <ta e="T76" id="Seg_4849" s="T75">adv</ta>
            <ta e="T77" id="Seg_4850" s="T76">v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_4851" s="T77">v-v:ins-v&gt;v-v:tense-v:pn</ta>
            <ta e="T79" id="Seg_4852" s="T78">n-n:case</ta>
            <ta e="T80" id="Seg_4853" s="T79">adv</ta>
            <ta e="T81" id="Seg_4854" s="T80">v-v:pn</ta>
            <ta e="T82" id="Seg_4855" s="T81">n-n:case</ta>
            <ta e="T83" id="Seg_4856" s="T82">v-v:pn</ta>
            <ta e="T84" id="Seg_4857" s="T83">dem</ta>
            <ta e="T85" id="Seg_4858" s="T84">n-n:ins-n:case</ta>
            <ta e="T86" id="Seg_4859" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_4860" s="T86">adj</ta>
            <ta e="T88" id="Seg_4861" s="T87">n-n:case</ta>
            <ta e="T89" id="Seg_4862" s="T88">v-v:pn</ta>
            <ta e="T90" id="Seg_4863" s="T89">n-n:case</ta>
            <ta e="T91" id="Seg_4864" s="T90">n-n:case-n:poss</ta>
            <ta e="T92" id="Seg_4865" s="T91">n-n&gt;v-v:inf-v-v:ins-v:pn</ta>
            <ta e="T93" id="Seg_4866" s="T92">interj</ta>
            <ta e="T94" id="Seg_4867" s="T93">adj</ta>
            <ta e="T95" id="Seg_4868" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_4869" s="T95">n-n:num-n:case</ta>
            <ta e="T97" id="Seg_4870" s="T96">ptcl</ta>
            <ta e="T98" id="Seg_4871" s="T97">preverb</ta>
            <ta e="T99" id="Seg_4872" s="T98">v-v:tense.mood-v:pn</ta>
            <ta e="T100" id="Seg_4873" s="T99">num-num&gt;num-n:ins-num&gt;adj</ta>
            <ta e="T101" id="Seg_4874" s="T100">n-n:ins-n&gt;adj</ta>
            <ta e="T102" id="Seg_4875" s="T101">n-n:case</ta>
            <ta e="T103" id="Seg_4876" s="T102">ptcl</ta>
            <ta e="T104" id="Seg_4877" s="T103">v-v&gt;ptcp</ta>
            <ta e="T105" id="Seg_4878" s="T104">n-n:case</ta>
            <ta e="T106" id="Seg_4879" s="T105">n-n&gt;adj</ta>
            <ta e="T107" id="Seg_4880" s="T106">n-n:case</ta>
            <ta e="T108" id="Seg_4881" s="T107">n-n:case</ta>
            <ta e="T109" id="Seg_4882" s="T108">emphpro-n&gt;adv</ta>
            <ta e="T110" id="Seg_4883" s="T109">n-n:num-n:obl.poss-n:case</ta>
            <ta e="T111" id="Seg_4884" s="T110">adj</ta>
            <ta e="T112" id="Seg_4885" s="T111">n-n:case</ta>
            <ta e="T113" id="Seg_4886" s="T112">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T114" id="Seg_4887" s="T113">v-v&gt;v-v&gt;v-v:ins-v:tense-v:pn</ta>
            <ta e="T115" id="Seg_4888" s="T114">n-n:case</ta>
            <ta e="T116" id="Seg_4889" s="T115">dem-adj&gt;adv</ta>
            <ta e="T117" id="Seg_4890" s="T116">v-v:pn</ta>
            <ta e="T118" id="Seg_4891" s="T117">v-n:ins-v&gt;v-v:inf-v-v:ins-v:pn</ta>
            <ta e="T119" id="Seg_4892" s="T118">n-n:case</ta>
            <ta e="T120" id="Seg_4893" s="T119">v-v:tense.mood-v:pn</ta>
            <ta e="T121" id="Seg_4894" s="T120">n-n:case</ta>
            <ta e="T122" id="Seg_4895" s="T121">adv</ta>
            <ta e="T123" id="Seg_4896" s="T122">v-v:ins-v:pn</ta>
            <ta e="T124" id="Seg_4897" s="T123">n-n:case-n:poss</ta>
            <ta e="T125" id="Seg_4898" s="T124">ptcl</ta>
            <ta e="T126" id="Seg_4899" s="T125">v-v:ins-v:pn</ta>
            <ta e="T127" id="Seg_4900" s="T126">n-n:num-n:case-n:poss</ta>
            <ta e="T128" id="Seg_4901" s="T127">adv</ta>
            <ta e="T129" id="Seg_4902" s="T128">v-v&gt;v-v:pn</ta>
            <ta e="T130" id="Seg_4903" s="T129">adv</ta>
            <ta e="T131" id="Seg_4904" s="T130">v-v:mood.pn</ta>
            <ta e="T132" id="Seg_4905" s="T131">n-n:num-n:ins-n:case</ta>
            <ta e="T133" id="Seg_4906" s="T132">adv</ta>
            <ta e="T134" id="Seg_4907" s="T133">v-v:mood.pn</ta>
            <ta e="T135" id="Seg_4908" s="T134">n-n:case</ta>
            <ta e="T136" id="Seg_4909" s="T135">emphpro-n:case</ta>
            <ta e="T137" id="Seg_4910" s="T136">v-v&gt;v-v:pn</ta>
            <ta e="T138" id="Seg_4911" s="T137">n-n:ins-n:case-n:poss</ta>
            <ta e="T139" id="Seg_4912" s="T138">n-n&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T140" id="Seg_4913" s="T139">num</ta>
            <ta e="T141" id="Seg_4914" s="T140">adj</ta>
            <ta e="T142" id="Seg_4915" s="T141">n-n:case</ta>
            <ta e="T143" id="Seg_4916" s="T142">v-v:tense.mood-v:pn</ta>
            <ta e="T144" id="Seg_4917" s="T143">n-n&gt;adv</ta>
            <ta e="T145" id="Seg_4918" s="T144">n-n:case</ta>
            <ta e="T146" id="Seg_4919" s="T145">v-v:ins-v&gt;v-v:ins-v:pn</ta>
            <ta e="T147" id="Seg_4920" s="T146">n-n:ins-n&gt;adj</ta>
            <ta e="T148" id="Seg_4921" s="T147">n-n:case</ta>
            <ta e="T149" id="Seg_4922" s="T148">n-n:case-n:poss</ta>
            <ta e="T150" id="Seg_4923" s="T149">pro-n:case</ta>
            <ta e="T151" id="Seg_4924" s="T150">adj</ta>
            <ta e="T152" id="Seg_4925" s="T151">n-n:case</ta>
            <ta e="T153" id="Seg_4926" s="T152">v-v:ins-v:pn</ta>
            <ta e="T154" id="Seg_4927" s="T153">v-v:tense.mood-v:pn</ta>
            <ta e="T155" id="Seg_4928" s="T154">n-n:num-n:case-n:poss</ta>
            <ta e="T156" id="Seg_4929" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_4930" s="T156">interrog</ta>
            <ta e="T158" id="Seg_4931" s="T157">interrog-n:case</ta>
            <ta e="T159" id="Seg_4932" s="T158">adv</ta>
            <ta e="T160" id="Seg_4933" s="T159">v-v:ins-v:pn</ta>
            <ta e="T161" id="Seg_4934" s="T160">n-n&gt;adv</ta>
            <ta e="T162" id="Seg_4935" s="T161">n-n:case</ta>
            <ta e="T163" id="Seg_4936" s="T162">v-v:tense.mood-v:pn</ta>
            <ta e="T164" id="Seg_4937" s="T163">n-n:case</ta>
            <ta e="T165" id="Seg_4938" s="T164">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T166" id="Seg_4939" s="T165">dem</ta>
            <ta e="T167" id="Seg_4940" s="T166">n-n:case</ta>
            <ta e="T168" id="Seg_4941" s="T167">n-n:ins-n&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T169" id="Seg_4942" s="T168">dem-adj&gt;adv</ta>
            <ta e="T170" id="Seg_4943" s="T169">interrog-n:case</ta>
            <ta e="T171" id="Seg_4944" s="T170">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T172" id="Seg_4945" s="T171">pers</ta>
            <ta e="T173" id="Seg_4946" s="T172">ptcl</ta>
            <ta e="T174" id="Seg_4947" s="T173">v-v&gt;ptcp</ta>
            <ta e="T175" id="Seg_4948" s="T174">pers</ta>
            <ta e="T176" id="Seg_4949" s="T175">ptcl</ta>
            <ta e="T177" id="Seg_4950" s="T176">v-v:tense-v:pn</ta>
            <ta e="T178" id="Seg_4951" s="T177">v-v:ins-v&gt;v-v:ins-v:pn</ta>
            <ta e="T179" id="Seg_4952" s="T178">n-n:num-n:case</ta>
            <ta e="T180" id="Seg_4953" s="T179">adv</ta>
            <ta e="T181" id="Seg_4954" s="T180">v-v:pn</ta>
            <ta e="T182" id="Seg_4955" s="T181">n-n:case</ta>
            <ta e="T183" id="Seg_4956" s="T182">n-n:ins-n&gt;adj</ta>
            <ta e="T184" id="Seg_4957" s="T183">interrog-n:ins-n:case</ta>
            <ta e="T185" id="Seg_4958" s="T184">quant</ta>
            <ta e="T186" id="Seg_4959" s="T185">n-n:case</ta>
            <ta e="T187" id="Seg_4960" s="T186">v-v:pn</ta>
            <ta e="T188" id="Seg_4961" s="T187">ptcl</ta>
            <ta e="T189" id="Seg_4962" s="T188">n-n:case</ta>
            <ta e="T190" id="Seg_4963" s="T189">n-n:case</ta>
            <ta e="T191" id="Seg_4964" s="T190">n-n:case</ta>
            <ta e="T192" id="Seg_4965" s="T191">ptcl</ta>
            <ta e="T193" id="Seg_4966" s="T192">v-v:tense-v:pn</ta>
            <ta e="T194" id="Seg_4967" s="T193">pers-n:case</ta>
            <ta e="T195" id="Seg_4968" s="T194">ptcl</ta>
            <ta e="T196" id="Seg_4969" s="T195">v-v:tense-v:pn</ta>
            <ta e="T197" id="Seg_4970" s="T196">adv</ta>
            <ta e="T198" id="Seg_4971" s="T197">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T199" id="Seg_4972" s="T198">n-n&gt;adj</ta>
            <ta e="T200" id="Seg_4973" s="T199">n-n:case-n:poss</ta>
            <ta e="T201" id="Seg_4974" s="T200">n-n&gt;adj</ta>
            <ta e="T202" id="Seg_4975" s="T201">n-n:case</ta>
            <ta e="T203" id="Seg_4976" s="T202">pro-n:case</ta>
            <ta e="T204" id="Seg_4977" s="T203">v-v:tense-v:mood-v:pn</ta>
            <ta e="T205" id="Seg_4978" s="T204">n-n:case-n:poss</ta>
            <ta e="T206" id="Seg_4979" s="T205">pp-n:case</ta>
            <ta e="T207" id="Seg_4980" s="T206">adj</ta>
            <ta e="T208" id="Seg_4981" s="T207">n-n:case-n:poss</ta>
            <ta e="T209" id="Seg_4982" s="T208">pp-n:case</ta>
            <ta e="T210" id="Seg_4983" s="T209">num</ta>
            <ta e="T211" id="Seg_4984" s="T210">adj</ta>
            <ta e="T212" id="Seg_4985" s="T211">n-n:case</ta>
            <ta e="T213" id="Seg_4986" s="T212">n-adv:case</ta>
            <ta e="T214" id="Seg_4987" s="T213">n-n:case</ta>
            <ta e="T215" id="Seg_4988" s="T214">n-n&gt;adj</ta>
            <ta e="T216" id="Seg_4989" s="T215">n-n:ins-n&gt;adj</ta>
            <ta e="T217" id="Seg_4990" s="T216">n-n:case</ta>
            <ta e="T218" id="Seg_4991" s="T217">v-v&gt;v-v:pn</ta>
            <ta e="T219" id="Seg_4992" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_4993" s="T219">v-v:tense-v:mood-v:pn</ta>
            <ta e="T221" id="Seg_4994" s="T220">n-n:case</ta>
            <ta e="T222" id="Seg_4995" s="T221">n-n:num-n:case</ta>
            <ta e="T223" id="Seg_4996" s="T222">ptcl</ta>
            <ta e="T224" id="Seg_4997" s="T223">v-v:tense-v:pn</ta>
            <ta e="T225" id="Seg_4998" s="T224">n-n:case</ta>
            <ta e="T226" id="Seg_4999" s="T225">ptcl</ta>
            <ta e="T227" id="Seg_5000" s="T226">adj</ta>
            <ta e="T228" id="Seg_5001" s="T227">n-n&gt;adj</ta>
            <ta e="T229" id="Seg_5002" s="T228">n-n:case</ta>
            <ta e="T230" id="Seg_5003" s="T229">n-n:case</ta>
            <ta e="T231" id="Seg_5004" s="T230">v-v:tense-v:pn</ta>
            <ta e="T232" id="Seg_5005" s="T231">ptcl</ta>
            <ta e="T233" id="Seg_5006" s="T232">ptcl</ta>
            <ta e="T234" id="Seg_5007" s="T233">v-v:tense-v:pn</ta>
            <ta e="T235" id="Seg_5008" s="T234">n-n:case</ta>
            <ta e="T236" id="Seg_5009" s="T235">n-n:case</ta>
            <ta e="T237" id="Seg_5010" s="T236">n-n:case-n:poss</ta>
            <ta e="T238" id="Seg_5011" s="T237">num</ta>
            <ta e="T239" id="Seg_5012" s="T238">n-n:case-n:poss</ta>
            <ta e="T240" id="Seg_5013" s="T239">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T241" id="Seg_5014" s="T240">ptcl</ta>
            <ta e="T242" id="Seg_5015" s="T241">adj</ta>
            <ta e="T243" id="Seg_5016" s="T242">n-n:case-n:poss</ta>
            <ta e="T244" id="Seg_5017" s="T243">v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T245" id="Seg_5018" s="T244">n-n:case</ta>
            <ta e="T246" id="Seg_5019" s="T245">v-v:tense-v:pn</ta>
            <ta e="T247" id="Seg_5020" s="T246">v-v:ins-v&gt;v-v:pn</ta>
            <ta e="T248" id="Seg_5021" s="T247">n-n&gt;adj</ta>
            <ta e="T249" id="Seg_5022" s="T248">n-n:num-n:case</ta>
            <ta e="T250" id="Seg_5023" s="T249">n-n:case</ta>
            <ta e="T251" id="Seg_5024" s="T250">n-n:case</ta>
            <ta e="T252" id="Seg_5025" s="T251">n-n:case</ta>
            <ta e="T253" id="Seg_5026" s="T252">v-v:tense-v:pn</ta>
            <ta e="T254" id="Seg_5027" s="T253">pro-n:case</ta>
            <ta e="T255" id="Seg_5028" s="T254">n-n:case</ta>
            <ta e="T256" id="Seg_5029" s="T255">ptcl</ta>
            <ta e="T257" id="Seg_5030" s="T256">v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T258" id="Seg_5031" s="T257">n-n:case</ta>
            <ta e="T259" id="Seg_5032" s="T258">n-n:case</ta>
            <ta e="T260" id="Seg_5033" s="T259">adv</ta>
            <ta e="T261" id="Seg_5034" s="T260">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T262" id="Seg_5035" s="T261">dem</ta>
            <ta e="T263" id="Seg_5036" s="T262">n-n:case</ta>
            <ta e="T264" id="Seg_5037" s="T263">pp-n:case</ta>
            <ta e="T265" id="Seg_5038" s="T264">n-n:case</ta>
            <ta e="T266" id="Seg_5039" s="T265">ptcl</ta>
            <ta e="T267" id="Seg_5040" s="T266">v-v:tense-v:mood-v:pn</ta>
            <ta e="T268" id="Seg_5041" s="T267">dem</ta>
            <ta e="T269" id="Seg_5042" s="T268">num-num&gt;num-n:ins-num&gt;adj</ta>
            <ta e="T270" id="Seg_5043" s="T269">n-n:ins-n&gt;adj</ta>
            <ta e="T271" id="Seg_5044" s="T270">n-n:case</ta>
            <ta e="T272" id="Seg_5045" s="T271">n-n:case</ta>
            <ta e="T273" id="Seg_5046" s="T272">v-v:tense-v:pn</ta>
            <ta e="T274" id="Seg_5047" s="T273">v-v:ins-v:pn</ta>
            <ta e="T275" id="Seg_5048" s="T274">adv-adv:case-adv&gt;adv</ta>
            <ta e="T276" id="Seg_5049" s="T275">n-n:num-n:case-n:poss</ta>
            <ta e="T277" id="Seg_5050" s="T276">n-n:case</ta>
            <ta e="T278" id="Seg_5051" s="T277">adv</ta>
            <ta e="T279" id="Seg_5052" s="T278">v-v&gt;adv</ta>
            <ta e="T280" id="Seg_5053" s="T279">n-n&gt;adj</ta>
            <ta e="T281" id="Seg_5054" s="T280">n-n:case-n:poss</ta>
            <ta e="T282" id="Seg_5055" s="T281">v-v&gt;v-v:pn</ta>
            <ta e="T283" id="Seg_5056" s="T282">adj</ta>
            <ta e="T284" id="Seg_5057" s="T283">ptcl</ta>
            <ta e="T285" id="Seg_5058" s="T284">adv</ta>
            <ta e="T286" id="Seg_5059" s="T285">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T287" id="Seg_5060" s="T286">ptcl</ta>
            <ta e="T288" id="Seg_5061" s="T287">adv</ta>
            <ta e="T289" id="Seg_5062" s="T288">v-v:pn</ta>
            <ta e="T290" id="Seg_5063" s="T289">n-n:num-n:case-n:poss</ta>
            <ta e="T291" id="Seg_5064" s="T290">n-n:case</ta>
            <ta e="T292" id="Seg_5065" s="T291">ptcl</ta>
            <ta e="T293" id="Seg_5066" s="T292">v-v:tense-v:mood-v:pn</ta>
            <ta e="T294" id="Seg_5067" s="T293">n-n:case</ta>
            <ta e="T295" id="Seg_5068" s="T294">n-n:case</ta>
            <ta e="T296" id="Seg_5069" s="T295">pp</ta>
            <ta e="T297" id="Seg_5070" s="T296">preverb</ta>
            <ta e="T298" id="Seg_5071" s="T297">conj</ta>
            <ta e="T299" id="Seg_5072" s="T298">num</ta>
            <ta e="T300" id="Seg_5073" s="T299">n-n:case</ta>
            <ta e="T301" id="Seg_5074" s="T300">n-n:case</ta>
            <ta e="T302" id="Seg_5075" s="T301">n-n:case</ta>
            <ta e="T303" id="Seg_5076" s="T302">v-v:ins-v:pn</ta>
            <ta e="T304" id="Seg_5077" s="T303">n-n:ins-n:case</ta>
            <ta e="T305" id="Seg_5078" s="T304">n-n:case</ta>
            <ta e="T306" id="Seg_5079" s="T305">v-v:ins-v:pn</ta>
            <ta e="T307" id="Seg_5080" s="T306">dem</ta>
            <ta e="T308" id="Seg_5081" s="T307">n-n&gt;adj</ta>
            <ta e="T309" id="Seg_5082" s="T308">n-n:case</ta>
            <ta e="T310" id="Seg_5083" s="T309">n-n:case</ta>
            <ta e="T311" id="Seg_5084" s="T310">n-n-n:case</ta>
            <ta e="T312" id="Seg_5085" s="T311">num</ta>
            <ta e="T313" id="Seg_5086" s="T312">v-v:tense-v:mood-v:pn</ta>
            <ta e="T314" id="Seg_5087" s="T313">adv</ta>
            <ta e="T315" id="Seg_5088" s="T314">adv</ta>
            <ta e="T316" id="Seg_5089" s="T315">v-v:ins-v:pn</ta>
            <ta e="T317" id="Seg_5090" s="T316">n-n:num-n:ins-n:case</ta>
            <ta e="T318" id="Seg_5091" s="T317">n-n:case</ta>
            <ta e="T319" id="Seg_5092" s="T318">adv</ta>
            <ta e="T320" id="Seg_5093" s="T319">ptcl</ta>
            <ta e="T321" id="Seg_5094" s="T320">n-n:case</ta>
            <ta e="T322" id="Seg_5095" s="T321">v-v:ins-v:pn</ta>
            <ta e="T323" id="Seg_5096" s="T322">conj</ta>
            <ta e="T324" id="Seg_5097" s="T323">num</ta>
            <ta e="T325" id="Seg_5098" s="T324">n-n:num-n:case-n:poss</ta>
            <ta e="T326" id="Seg_5099" s="T325">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T327" id="Seg_5100" s="T326">num</ta>
            <ta e="T328" id="Seg_5101" s="T327">n-n:num-n:case-n:poss</ta>
            <ta e="T329" id="Seg_5102" s="T328">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T330" id="Seg_5103" s="T329">v-v&gt;v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T331" id="Seg_5104" s="T330">n-n:ins-n:num-n:case-n:poss</ta>
            <ta e="T332" id="Seg_5105" s="T331">adv-adv:case</ta>
            <ta e="T333" id="Seg_5106" s="T332">v-v:ins-v:pn</ta>
            <ta e="T334" id="Seg_5107" s="T333">v-v&gt;ptcp</ta>
            <ta e="T454" id="Seg_5108" s="T334">n-n:case</ta>
            <ta e="T335" id="Seg_5109" s="T454">n-n:case</ta>
            <ta e="T336" id="Seg_5110" s="T335">v-v&gt;v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T337" id="Seg_5111" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_5112" s="T337">n-n&gt;adj</ta>
            <ta e="T339" id="Seg_5113" s="T338">n-n&gt;adj</ta>
            <ta e="T340" id="Seg_5114" s="T339">n-n:case</ta>
            <ta e="T341" id="Seg_5115" s="T340">n-n:case</ta>
            <ta e="T342" id="Seg_5116" s="T341">v-v&gt;v-v:ins-v&gt;v-v:pn</ta>
            <ta e="T343" id="Seg_5117" s="T342">n-n:case</ta>
            <ta e="T344" id="Seg_5118" s="T343">adv</ta>
            <ta e="T345" id="Seg_5119" s="T344">v-v:ins-v:pn</ta>
            <ta e="T346" id="Seg_5120" s="T345">adv</ta>
            <ta e="T347" id="Seg_5121" s="T346">adv</ta>
            <ta e="T348" id="Seg_5122" s="T347">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T349" id="Seg_5123" s="T348">num</ta>
            <ta e="T350" id="Seg_5124" s="T349">n-n&gt;n-n:num-n:case</ta>
            <ta e="T351" id="Seg_5125" s="T350">adv</ta>
            <ta e="T352" id="Seg_5126" s="T351">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T353" id="Seg_5127" s="T352">adv</ta>
            <ta e="T354" id="Seg_5128" s="T353">v-v&gt;v-v:pn</ta>
            <ta e="T355" id="Seg_5129" s="T354">n-n:num-n:ins-n:case</ta>
            <ta e="T356" id="Seg_5130" s="T355">n-n:case</ta>
            <ta e="T357" id="Seg_5131" s="T356">adv</ta>
            <ta e="T358" id="Seg_5132" s="T357">v-v:ins-v:pn</ta>
            <ta e="T359" id="Seg_5133" s="T358">n-n:num-n:ins-n:case</ta>
            <ta e="T360" id="Seg_5134" s="T359">n-n:case</ta>
            <ta e="T361" id="Seg_5135" s="T360">n-n:case-n:poss</ta>
            <ta e="T362" id="Seg_5136" s="T361">adv</ta>
            <ta e="T363" id="Seg_5137" s="T362">v-v:ins-v:pn</ta>
            <ta e="T364" id="Seg_5138" s="T363">adv</ta>
            <ta e="T365" id="Seg_5139" s="T364">n-n:case</ta>
            <ta e="T366" id="Seg_5140" s="T365">n-n:ins-n&gt;adj</ta>
            <ta e="T367" id="Seg_5141" s="T366">num</ta>
            <ta e="T368" id="Seg_5142" s="T367">n-n:num-n:case</ta>
            <ta e="T369" id="Seg_5143" s="T368">v-v:tense-v:pn</ta>
            <ta e="T370" id="Seg_5144" s="T369">adv</ta>
            <ta e="T371" id="Seg_5145" s="T370">n-n&gt;adj</ta>
            <ta e="T372" id="Seg_5146" s="T371">n-n&gt;adj</ta>
            <ta e="T373" id="Seg_5147" s="T372">n-n:case</ta>
            <ta e="T374" id="Seg_5148" s="T373">n-n:num-n:case</ta>
            <ta e="T375" id="Seg_5149" s="T374">pro-n:case</ta>
            <ta e="T376" id="Seg_5150" s="T375">n-n:num-n:case</ta>
            <ta e="T377" id="Seg_5151" s="T376">n-n:case</ta>
            <ta e="T378" id="Seg_5152" s="T377">v-v&gt;adv</ta>
            <ta e="T379" id="Seg_5153" s="T378">adv</ta>
            <ta e="T380" id="Seg_5154" s="T379">v-v:ins-v:pn</ta>
            <ta e="T381" id="Seg_5155" s="T380">adv</ta>
            <ta e="T382" id="Seg_5156" s="T381">n-n&gt;v-v:ins-v:pn</ta>
            <ta e="T383" id="Seg_5157" s="T382">num</ta>
            <ta e="T384" id="Seg_5158" s="T383">n-n&gt;adv</ta>
            <ta e="T385" id="Seg_5159" s="T384">v-v:pn</ta>
            <ta e="T386" id="Seg_5160" s="T385">n-n:num-n:case-n:poss</ta>
            <ta e="T387" id="Seg_5161" s="T386">dem</ta>
            <ta e="T388" id="Seg_5162" s="T387">n-n:case</ta>
            <ta e="T389" id="Seg_5163" s="T388">adv</ta>
            <ta e="T390" id="Seg_5164" s="T389">v-v:ins-v:pn</ta>
            <ta e="T391" id="Seg_5165" s="T390">n-n:case</ta>
            <ta e="T392" id="Seg_5166" s="T391">n-n:case-n:poss</ta>
            <ta e="T393" id="Seg_5167" s="T392">v-v&gt;v-v&gt;adv</ta>
            <ta e="T394" id="Seg_5168" s="T393">v-v:ins-v:pn</ta>
            <ta e="T395" id="Seg_5169" s="T394">n-n:num-n:case-n:poss</ta>
            <ta e="T396" id="Seg_5170" s="T395">pers-n:ins-n:num-n:case</ta>
            <ta e="T397" id="Seg_5171" s="T396">dem-adj&gt;adv</ta>
            <ta e="T398" id="Seg_5172" s="T397">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T399" id="Seg_5173" s="T398">pers</ta>
            <ta e="T400" id="Seg_5174" s="T399">n-n:case-n:poss</ta>
            <ta e="T401" id="Seg_5175" s="T400">v-v:tense-v:pn</ta>
            <ta e="T402" id="Seg_5176" s="T401">pers-n:case</ta>
            <ta e="T403" id="Seg_5177" s="T402">num</ta>
            <ta e="T404" id="Seg_5178" s="T403">n-n:case-n:poss</ta>
            <ta e="T405" id="Seg_5179" s="T404">v-v:tense-v:mood-v:pn</ta>
            <ta e="T406" id="Seg_5180" s="T405">adj</ta>
            <ta e="T407" id="Seg_5181" s="T406">n-n:case-n:poss</ta>
            <ta e="T408" id="Seg_5182" s="T407">pers-n:ins-n:num-n:case</ta>
            <ta e="T409" id="Seg_5183" s="T408">v-v&gt;v-v&gt;adv</ta>
            <ta e="T410" id="Seg_5184" s="T409">v-v:pn</ta>
            <ta e="T411" id="Seg_5185" s="T410">n-n:case-n:poss</ta>
            <ta e="T412" id="Seg_5186" s="T411">n-n:case</ta>
            <ta e="T413" id="Seg_5187" s="T412">n-n&gt;adv</ta>
            <ta e="T414" id="Seg_5188" s="T413">v-v:tense-v:pn</ta>
            <ta e="T415" id="Seg_5189" s="T414">n-n:num-n:case</ta>
            <ta e="T416" id="Seg_5190" s="T415">v-v&gt;v-v&gt;n-n:case</ta>
            <ta e="T417" id="Seg_5191" s="T416">pers-n:ins-n:num-n:case</ta>
            <ta e="T418" id="Seg_5192" s="T417">n-n:case</ta>
            <ta e="T419" id="Seg_5193" s="T418">n-n:case</ta>
            <ta e="T420" id="Seg_5194" s="T419">v-v:ins-v:pn</ta>
            <ta e="T421" id="Seg_5195" s="T420">n-n:case</ta>
            <ta e="T422" id="Seg_5196" s="T421">dem-adj&gt;adv</ta>
            <ta e="T423" id="Seg_5197" s="T422">v-v:ins-v:pn</ta>
            <ta e="T424" id="Seg_5198" s="T423">n-n:case</ta>
            <ta e="T425" id="Seg_5199" s="T424">n-n:case</ta>
            <ta e="T426" id="Seg_5200" s="T425">interrog-n:case</ta>
            <ta e="T427" id="Seg_5201" s="T426">v-v:ins-v:pn</ta>
            <ta e="T428" id="Seg_5202" s="T427">adj</ta>
            <ta e="T429" id="Seg_5203" s="T428">n-n:case-n:poss</ta>
            <ta e="T430" id="Seg_5204" s="T429">n-n:case</ta>
            <ta e="T431" id="Seg_5205" s="T430">adv</ta>
            <ta e="T432" id="Seg_5206" s="T431">v-v:pn</ta>
            <ta e="T433" id="Seg_5207" s="T432">ptcl</ta>
            <ta e="T434" id="Seg_5208" s="T433">emphpro</ta>
            <ta e="T435" id="Seg_5209" s="T434">n-n:case-n:poss</ta>
            <ta e="T436" id="Seg_5210" s="T435">v-v:tense.mood-v:pn</ta>
            <ta e="T437" id="Seg_5211" s="T436">ptcl</ta>
            <ta e="T438" id="Seg_5212" s="T437">emphpro</ta>
            <ta e="T439" id="Seg_5213" s="T438">n-n:case-n:poss</ta>
            <ta e="T440" id="Seg_5214" s="T439">v-v:tense-v:pn</ta>
            <ta e="T441" id="Seg_5215" s="T440">adv</ta>
            <ta e="T442" id="Seg_5216" s="T441">n-n:case-n:poss</ta>
            <ta e="T443" id="Seg_5217" s="T442">dem-adj&gt;adv</ta>
            <ta e="T444" id="Seg_5218" s="T443">v-v:tense-v:pn</ta>
            <ta e="T445" id="Seg_5219" s="T444">ptcl</ta>
            <ta e="T446" id="Seg_5220" s="T445">emphpro-n:case</ta>
            <ta e="T447" id="Seg_5221" s="T446">n-n:case</ta>
            <ta e="T448" id="Seg_5222" s="T447">v-v:tense-v:pn</ta>
            <ta e="T449" id="Seg_5223" s="T448">n-n&gt;adj</ta>
            <ta e="T450" id="Seg_5224" s="T449">n-n&gt;adv</ta>
            <ta e="T451" id="Seg_5225" s="T450">n-n:case</ta>
            <ta e="T452" id="Seg_5226" s="T451">adv</ta>
            <ta e="T453" id="Seg_5227" s="T452">v-v:ins-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_5228" s="T0">adj</ta>
            <ta e="T2" id="Seg_5229" s="T1">n</ta>
            <ta e="T3" id="Seg_5230" s="T2">n</ta>
            <ta e="T4" id="Seg_5231" s="T3">num</ta>
            <ta e="T5" id="Seg_5232" s="T4">n</ta>
            <ta e="T6" id="Seg_5233" s="T5">v</ta>
            <ta e="T7" id="Seg_5234" s="T6">conj</ta>
            <ta e="T8" id="Seg_5235" s="T7">num</ta>
            <ta e="T9" id="Seg_5236" s="T8">n</ta>
            <ta e="T10" id="Seg_5237" s="T9">n</ta>
            <ta e="T11" id="Seg_5238" s="T10">v</ta>
            <ta e="T12" id="Seg_5239" s="T11">adv</ta>
            <ta e="T13" id="Seg_5240" s="T12">adv</ta>
            <ta e="T14" id="Seg_5241" s="T13">adv</ta>
            <ta e="T15" id="Seg_5242" s="T14">pers</ta>
            <ta e="T16" id="Seg_5243" s="T15">v</ta>
            <ta e="T17" id="Seg_5244" s="T16">n</ta>
            <ta e="T18" id="Seg_5245" s="T17">n</ta>
            <ta e="T19" id="Seg_5246" s="T18">n</ta>
            <ta e="T20" id="Seg_5247" s="T19">v</ta>
            <ta e="T21" id="Seg_5248" s="T20">adv</ta>
            <ta e="T22" id="Seg_5249" s="T21">n</ta>
            <ta e="T23" id="Seg_5250" s="T22">adv</ta>
            <ta e="T24" id="Seg_5251" s="T23">v</ta>
            <ta e="T25" id="Seg_5252" s="T24">interrog</ta>
            <ta e="T26" id="Seg_5253" s="T25">v</ta>
            <ta e="T27" id="Seg_5254" s="T26">num</ta>
            <ta e="T28" id="Seg_5255" s="T27">adj</ta>
            <ta e="T29" id="Seg_5256" s="T28">n</ta>
            <ta e="T30" id="Seg_5257" s="T29">v</ta>
            <ta e="T31" id="Seg_5258" s="T30">dem</ta>
            <ta e="T32" id="Seg_5259" s="T31">n</ta>
            <ta e="T33" id="Seg_5260" s="T32">pro</ta>
            <ta e="T34" id="Seg_5261" s="T33">v</ta>
            <ta e="T35" id="Seg_5262" s="T34">adj</ta>
            <ta e="T36" id="Seg_5263" s="T35">n</ta>
            <ta e="T37" id="Seg_5264" s="T36">n</ta>
            <ta e="T38" id="Seg_5265" s="T37">pers</ta>
            <ta e="T39" id="Seg_5266" s="T38">adv</ta>
            <ta e="T40" id="Seg_5267" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_5268" s="T40">v</ta>
            <ta e="T42" id="Seg_5269" s="T41">v</ta>
            <ta e="T43" id="Seg_5270" s="T42">v</ta>
            <ta e="T44" id="Seg_5271" s="T43">n</ta>
            <ta e="T45" id="Seg_5272" s="T44">n</ta>
            <ta e="T46" id="Seg_5273" s="T45">adv</ta>
            <ta e="T47" id="Seg_5274" s="T46">adv</ta>
            <ta e="T48" id="Seg_5275" s="T47">adv</ta>
            <ta e="T49" id="Seg_5276" s="T48">v</ta>
            <ta e="T50" id="Seg_5277" s="T49">adj</ta>
            <ta e="T51" id="Seg_5278" s="T50">adj</ta>
            <ta e="T52" id="Seg_5279" s="T51">n</ta>
            <ta e="T53" id="Seg_5280" s="T52">v</ta>
            <ta e="T54" id="Seg_5281" s="T53">adv</ta>
            <ta e="T55" id="Seg_5282" s="T54">adj</ta>
            <ta e="T56" id="Seg_5283" s="T55">n</ta>
            <ta e="T57" id="Seg_5284" s="T56">interrog</ta>
            <ta e="T58" id="Seg_5285" s="T57">v</ta>
            <ta e="T59" id="Seg_5286" s="T58">n</ta>
            <ta e="T60" id="Seg_5287" s="T59">adv</ta>
            <ta e="T61" id="Seg_5288" s="T60">v</ta>
            <ta e="T62" id="Seg_5289" s="T61">dem</ta>
            <ta e="T63" id="Seg_5290" s="T62">adj</ta>
            <ta e="T64" id="Seg_5291" s="T63">n</ta>
            <ta e="T65" id="Seg_5292" s="T64">n</ta>
            <ta e="T66" id="Seg_5293" s="T65">adj</ta>
            <ta e="T67" id="Seg_5294" s="T66">n</ta>
            <ta e="T68" id="Seg_5295" s="T67">n</ta>
            <ta e="T69" id="Seg_5296" s="T68">n</ta>
            <ta e="T70" id="Seg_5297" s="T69">v</ta>
            <ta e="T71" id="Seg_5298" s="T70">interrog</ta>
            <ta e="T72" id="Seg_5299" s="T71">adj</ta>
            <ta e="T73" id="Seg_5300" s="T72">interrog</ta>
            <ta e="T74" id="Seg_5301" s="T73">v</ta>
            <ta e="T75" id="Seg_5302" s="T74">n</ta>
            <ta e="T76" id="Seg_5303" s="T75">adv</ta>
            <ta e="T77" id="Seg_5304" s="T76">v</ta>
            <ta e="T78" id="Seg_5305" s="T77">v</ta>
            <ta e="T79" id="Seg_5306" s="T78">n</ta>
            <ta e="T80" id="Seg_5307" s="T79">adv</ta>
            <ta e="T81" id="Seg_5308" s="T80">v</ta>
            <ta e="T82" id="Seg_5309" s="T81">n</ta>
            <ta e="T83" id="Seg_5310" s="T82">v</ta>
            <ta e="T84" id="Seg_5311" s="T83">dem</ta>
            <ta e="T85" id="Seg_5312" s="T84">n</ta>
            <ta e="T86" id="Seg_5313" s="T85">n</ta>
            <ta e="T87" id="Seg_5314" s="T86">adj</ta>
            <ta e="T88" id="Seg_5315" s="T87">n</ta>
            <ta e="T89" id="Seg_5316" s="T88">v</ta>
            <ta e="T90" id="Seg_5317" s="T89">n</ta>
            <ta e="T91" id="Seg_5318" s="T90">n</ta>
            <ta e="T92" id="Seg_5319" s="T91">v</ta>
            <ta e="T93" id="Seg_5320" s="T92">interj</ta>
            <ta e="T94" id="Seg_5321" s="T93">adj</ta>
            <ta e="T95" id="Seg_5322" s="T94">n</ta>
            <ta e="T96" id="Seg_5323" s="T95">n</ta>
            <ta e="T97" id="Seg_5324" s="T96">ptcl</ta>
            <ta e="T98" id="Seg_5325" s="T97">preverb</ta>
            <ta e="T99" id="Seg_5326" s="T98">v</ta>
            <ta e="T100" id="Seg_5327" s="T99">adj</ta>
            <ta e="T101" id="Seg_5328" s="T100">adj</ta>
            <ta e="T102" id="Seg_5329" s="T101">n</ta>
            <ta e="T103" id="Seg_5330" s="T102">ptcl</ta>
            <ta e="T104" id="Seg_5331" s="T103">ptcp</ta>
            <ta e="T105" id="Seg_5332" s="T104">n</ta>
            <ta e="T106" id="Seg_5333" s="T105">adj</ta>
            <ta e="T107" id="Seg_5334" s="T106">n</ta>
            <ta e="T108" id="Seg_5335" s="T107">n</ta>
            <ta e="T109" id="Seg_5336" s="T108">emphpro</ta>
            <ta e="T110" id="Seg_5337" s="T109">n</ta>
            <ta e="T111" id="Seg_5338" s="T110">adj</ta>
            <ta e="T112" id="Seg_5339" s="T111">n</ta>
            <ta e="T113" id="Seg_5340" s="T112">v</ta>
            <ta e="T114" id="Seg_5341" s="T113">v</ta>
            <ta e="T115" id="Seg_5342" s="T114">n</ta>
            <ta e="T116" id="Seg_5343" s="T115">adv</ta>
            <ta e="T117" id="Seg_5344" s="T116">v</ta>
            <ta e="T118" id="Seg_5345" s="T117">v</ta>
            <ta e="T119" id="Seg_5346" s="T118">n</ta>
            <ta e="T120" id="Seg_5347" s="T119">v</ta>
            <ta e="T121" id="Seg_5348" s="T120">n</ta>
            <ta e="T122" id="Seg_5349" s="T121">adv</ta>
            <ta e="T123" id="Seg_5350" s="T122">v</ta>
            <ta e="T124" id="Seg_5351" s="T123">n</ta>
            <ta e="T125" id="Seg_5352" s="T124">ptcl</ta>
            <ta e="T126" id="Seg_5353" s="T125">v</ta>
            <ta e="T127" id="Seg_5354" s="T126">n</ta>
            <ta e="T128" id="Seg_5355" s="T127">adv</ta>
            <ta e="T129" id="Seg_5356" s="T128">v</ta>
            <ta e="T130" id="Seg_5357" s="T129">adv</ta>
            <ta e="T131" id="Seg_5358" s="T130">v</ta>
            <ta e="T132" id="Seg_5359" s="T131">n</ta>
            <ta e="T133" id="Seg_5360" s="T132">adv</ta>
            <ta e="T134" id="Seg_5361" s="T133">v</ta>
            <ta e="T135" id="Seg_5362" s="T134">n</ta>
            <ta e="T136" id="Seg_5363" s="T135">emphpro</ta>
            <ta e="T137" id="Seg_5364" s="T136">v</ta>
            <ta e="T138" id="Seg_5365" s="T137">n</ta>
            <ta e="T139" id="Seg_5366" s="T138">adv</ta>
            <ta e="T140" id="Seg_5367" s="T139">num</ta>
            <ta e="T141" id="Seg_5368" s="T140">adj</ta>
            <ta e="T142" id="Seg_5369" s="T141">n</ta>
            <ta e="T143" id="Seg_5370" s="T142">v</ta>
            <ta e="T144" id="Seg_5371" s="T143">adv</ta>
            <ta e="T145" id="Seg_5372" s="T144">n</ta>
            <ta e="T146" id="Seg_5373" s="T145">v</ta>
            <ta e="T147" id="Seg_5374" s="T146">adj</ta>
            <ta e="T148" id="Seg_5375" s="T147">n</ta>
            <ta e="T149" id="Seg_5376" s="T148">n</ta>
            <ta e="T150" id="Seg_5377" s="T149">pro</ta>
            <ta e="T151" id="Seg_5378" s="T150">adj</ta>
            <ta e="T152" id="Seg_5379" s="T151">n</ta>
            <ta e="T153" id="Seg_5380" s="T152">n</ta>
            <ta e="T154" id="Seg_5381" s="T153">v</ta>
            <ta e="T155" id="Seg_5382" s="T154">n</ta>
            <ta e="T156" id="Seg_5383" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_5384" s="T156">interrog</ta>
            <ta e="T158" id="Seg_5385" s="T157">interrog</ta>
            <ta e="T159" id="Seg_5386" s="T158">adv</ta>
            <ta e="T160" id="Seg_5387" s="T159">v</ta>
            <ta e="T161" id="Seg_5388" s="T160">adv</ta>
            <ta e="T162" id="Seg_5389" s="T161">n</ta>
            <ta e="T163" id="Seg_5390" s="T162">v</ta>
            <ta e="T164" id="Seg_5391" s="T163">n</ta>
            <ta e="T165" id="Seg_5392" s="T164">v</ta>
            <ta e="T166" id="Seg_5393" s="T165">dem</ta>
            <ta e="T167" id="Seg_5394" s="T166">n</ta>
            <ta e="T168" id="Seg_5395" s="T167">v</ta>
            <ta e="T169" id="Seg_5396" s="T168">adv</ta>
            <ta e="T170" id="Seg_5397" s="T169">interrog</ta>
            <ta e="T171" id="Seg_5398" s="T170">v</ta>
            <ta e="T172" id="Seg_5399" s="T171">pers</ta>
            <ta e="T173" id="Seg_5400" s="T172">ptcl</ta>
            <ta e="T174" id="Seg_5401" s="T173">ptcp</ta>
            <ta e="T175" id="Seg_5402" s="T174">pers</ta>
            <ta e="T176" id="Seg_5403" s="T175">ptcl</ta>
            <ta e="T177" id="Seg_5404" s="T176">v</ta>
            <ta e="T178" id="Seg_5405" s="T177">v</ta>
            <ta e="T179" id="Seg_5406" s="T178">n</ta>
            <ta e="T180" id="Seg_5407" s="T179">adv</ta>
            <ta e="T181" id="Seg_5408" s="T180">v</ta>
            <ta e="T182" id="Seg_5409" s="T181">n</ta>
            <ta e="T183" id="Seg_5410" s="T182">adj</ta>
            <ta e="T184" id="Seg_5411" s="T183">interrog</ta>
            <ta e="T185" id="Seg_5412" s="T184">quant</ta>
            <ta e="T186" id="Seg_5413" s="T185">n</ta>
            <ta e="T187" id="Seg_5414" s="T186">n</ta>
            <ta e="T188" id="Seg_5415" s="T187">ptcl</ta>
            <ta e="T189" id="Seg_5416" s="T188">n</ta>
            <ta e="T190" id="Seg_5417" s="T189">n</ta>
            <ta e="T191" id="Seg_5418" s="T190">n</ta>
            <ta e="T192" id="Seg_5419" s="T191">ptcl</ta>
            <ta e="T193" id="Seg_5420" s="T192">v</ta>
            <ta e="T194" id="Seg_5421" s="T193">pers</ta>
            <ta e="T195" id="Seg_5422" s="T194">ptcl</ta>
            <ta e="T196" id="Seg_5423" s="T195">v</ta>
            <ta e="T197" id="Seg_5424" s="T196">adv</ta>
            <ta e="T198" id="Seg_5425" s="T197">v</ta>
            <ta e="T199" id="Seg_5426" s="T198">adj</ta>
            <ta e="T200" id="Seg_5427" s="T199">n</ta>
            <ta e="T201" id="Seg_5428" s="T200">adj</ta>
            <ta e="T202" id="Seg_5429" s="T201">n</ta>
            <ta e="T203" id="Seg_5430" s="T202">pro</ta>
            <ta e="T204" id="Seg_5431" s="T203">v</ta>
            <ta e="T205" id="Seg_5432" s="T204">n</ta>
            <ta e="T206" id="Seg_5433" s="T205">pp</ta>
            <ta e="T207" id="Seg_5434" s="T206">adj</ta>
            <ta e="T208" id="Seg_5435" s="T207">n</ta>
            <ta e="T209" id="Seg_5436" s="T208">pp</ta>
            <ta e="T210" id="Seg_5437" s="T209">num</ta>
            <ta e="T211" id="Seg_5438" s="T210">adj</ta>
            <ta e="T212" id="Seg_5439" s="T211">n</ta>
            <ta e="T213" id="Seg_5440" s="T212">adv</ta>
            <ta e="T214" id="Seg_5441" s="T213">n</ta>
            <ta e="T215" id="Seg_5442" s="T214">adj</ta>
            <ta e="T216" id="Seg_5443" s="T215">adj</ta>
            <ta e="T217" id="Seg_5444" s="T216">n</ta>
            <ta e="T218" id="Seg_5445" s="T217">v</ta>
            <ta e="T219" id="Seg_5446" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_5447" s="T219">v</ta>
            <ta e="T221" id="Seg_5448" s="T220">n</ta>
            <ta e="T222" id="Seg_5449" s="T221">n</ta>
            <ta e="T223" id="Seg_5450" s="T222">ptcl</ta>
            <ta e="T224" id="Seg_5451" s="T223">v</ta>
            <ta e="T225" id="Seg_5452" s="T224">n</ta>
            <ta e="T226" id="Seg_5453" s="T225">ptcl</ta>
            <ta e="T227" id="Seg_5454" s="T226">adj</ta>
            <ta e="T228" id="Seg_5455" s="T227">adj</ta>
            <ta e="T229" id="Seg_5456" s="T228">n</ta>
            <ta e="T230" id="Seg_5457" s="T229">n</ta>
            <ta e="T231" id="Seg_5458" s="T230">v</ta>
            <ta e="T232" id="Seg_5459" s="T231">ptcl</ta>
            <ta e="T233" id="Seg_5460" s="T232">ptcl</ta>
            <ta e="T234" id="Seg_5461" s="T233">v</ta>
            <ta e="T235" id="Seg_5462" s="T234">n</ta>
            <ta e="T236" id="Seg_5463" s="T235">n</ta>
            <ta e="T237" id="Seg_5464" s="T236">n</ta>
            <ta e="T238" id="Seg_5465" s="T237">num</ta>
            <ta e="T239" id="Seg_5466" s="T238">n</ta>
            <ta e="T240" id="Seg_5467" s="T239">v</ta>
            <ta e="T241" id="Seg_5468" s="T240">ptcl</ta>
            <ta e="T242" id="Seg_5469" s="T241">adj</ta>
            <ta e="T243" id="Seg_5470" s="T242">n</ta>
            <ta e="T244" id="Seg_5471" s="T243">v</ta>
            <ta e="T245" id="Seg_5472" s="T244">n</ta>
            <ta e="T246" id="Seg_5473" s="T245">v</ta>
            <ta e="T247" id="Seg_5474" s="T246">v</ta>
            <ta e="T248" id="Seg_5475" s="T247">adj</ta>
            <ta e="T249" id="Seg_5476" s="T248">n</ta>
            <ta e="T250" id="Seg_5477" s="T249">n</ta>
            <ta e="T251" id="Seg_5478" s="T250">n</ta>
            <ta e="T252" id="Seg_5479" s="T251">n</ta>
            <ta e="T253" id="Seg_5480" s="T252">v</ta>
            <ta e="T254" id="Seg_5481" s="T253">pro</ta>
            <ta e="T255" id="Seg_5482" s="T254">n</ta>
            <ta e="T256" id="Seg_5483" s="T255">ptcl</ta>
            <ta e="T257" id="Seg_5484" s="T256">v</ta>
            <ta e="T258" id="Seg_5485" s="T257">n</ta>
            <ta e="T259" id="Seg_5486" s="T258">n</ta>
            <ta e="T260" id="Seg_5487" s="T259">adv</ta>
            <ta e="T261" id="Seg_5488" s="T260">v</ta>
            <ta e="T262" id="Seg_5489" s="T261">dem</ta>
            <ta e="T263" id="Seg_5490" s="T262">n</ta>
            <ta e="T264" id="Seg_5491" s="T263">pp</ta>
            <ta e="T265" id="Seg_5492" s="T264">n</ta>
            <ta e="T266" id="Seg_5493" s="T265">ptcl</ta>
            <ta e="T267" id="Seg_5494" s="T266">v</ta>
            <ta e="T268" id="Seg_5495" s="T267">dem</ta>
            <ta e="T269" id="Seg_5496" s="T268">adj</ta>
            <ta e="T270" id="Seg_5497" s="T269">adj</ta>
            <ta e="T271" id="Seg_5498" s="T270">n</ta>
            <ta e="T272" id="Seg_5499" s="T271">n</ta>
            <ta e="T273" id="Seg_5500" s="T272">v</ta>
            <ta e="T274" id="Seg_5501" s="T273">v</ta>
            <ta e="T275" id="Seg_5502" s="T274">adv</ta>
            <ta e="T276" id="Seg_5503" s="T275">n</ta>
            <ta e="T277" id="Seg_5504" s="T276">n</ta>
            <ta e="T278" id="Seg_5505" s="T277">adv</ta>
            <ta e="T279" id="Seg_5506" s="T278">adv</ta>
            <ta e="T280" id="Seg_5507" s="T279">adj</ta>
            <ta e="T281" id="Seg_5508" s="T280">n</ta>
            <ta e="T282" id="Seg_5509" s="T281">v</ta>
            <ta e="T283" id="Seg_5510" s="T282">adj</ta>
            <ta e="T284" id="Seg_5511" s="T283">ptcl</ta>
            <ta e="T285" id="Seg_5512" s="T284">adv</ta>
            <ta e="T286" id="Seg_5513" s="T285">v</ta>
            <ta e="T287" id="Seg_5514" s="T286">ptcl</ta>
            <ta e="T288" id="Seg_5515" s="T287">adv</ta>
            <ta e="T289" id="Seg_5516" s="T288">v</ta>
            <ta e="T290" id="Seg_5517" s="T289">n</ta>
            <ta e="T291" id="Seg_5518" s="T290">n</ta>
            <ta e="T292" id="Seg_5519" s="T291">ptcl</ta>
            <ta e="T293" id="Seg_5520" s="T292">v</ta>
            <ta e="T294" id="Seg_5521" s="T293">n</ta>
            <ta e="T295" id="Seg_5522" s="T294">n</ta>
            <ta e="T296" id="Seg_5523" s="T295">pp</ta>
            <ta e="T297" id="Seg_5524" s="T296">preverb</ta>
            <ta e="T298" id="Seg_5525" s="T297">conj</ta>
            <ta e="T299" id="Seg_5526" s="T298">num</ta>
            <ta e="T300" id="Seg_5527" s="T299">n</ta>
            <ta e="T301" id="Seg_5528" s="T300">n</ta>
            <ta e="T302" id="Seg_5529" s="T301">n</ta>
            <ta e="T303" id="Seg_5530" s="T302">v</ta>
            <ta e="T304" id="Seg_5531" s="T303">n</ta>
            <ta e="T305" id="Seg_5532" s="T304">n</ta>
            <ta e="T306" id="Seg_5533" s="T305">v</ta>
            <ta e="T307" id="Seg_5534" s="T306">dem</ta>
            <ta e="T308" id="Seg_5535" s="T307">adj</ta>
            <ta e="T309" id="Seg_5536" s="T308">n</ta>
            <ta e="T310" id="Seg_5537" s="T309">n</ta>
            <ta e="T311" id="Seg_5538" s="T310">n</ta>
            <ta e="T312" id="Seg_5539" s="T311">num</ta>
            <ta e="T313" id="Seg_5540" s="T312">v</ta>
            <ta e="T314" id="Seg_5541" s="T313">adv</ta>
            <ta e="T315" id="Seg_5542" s="T314">adv</ta>
            <ta e="T316" id="Seg_5543" s="T315">v</ta>
            <ta e="T317" id="Seg_5544" s="T316">n</ta>
            <ta e="T318" id="Seg_5545" s="T317">n</ta>
            <ta e="T319" id="Seg_5546" s="T318">adv</ta>
            <ta e="T320" id="Seg_5547" s="T319">ptcl</ta>
            <ta e="T321" id="Seg_5548" s="T320">n</ta>
            <ta e="T322" id="Seg_5549" s="T321">v</ta>
            <ta e="T323" id="Seg_5550" s="T322">conj</ta>
            <ta e="T324" id="Seg_5551" s="T323">num</ta>
            <ta e="T325" id="Seg_5552" s="T324">n</ta>
            <ta e="T326" id="Seg_5553" s="T325">v</ta>
            <ta e="T327" id="Seg_5554" s="T326">num</ta>
            <ta e="T328" id="Seg_5555" s="T327">n</ta>
            <ta e="T329" id="Seg_5556" s="T328">v</ta>
            <ta e="T330" id="Seg_5557" s="T329">ptcp</ta>
            <ta e="T331" id="Seg_5558" s="T330">n</ta>
            <ta e="T332" id="Seg_5559" s="T331">adv</ta>
            <ta e="T333" id="Seg_5560" s="T332">v</ta>
            <ta e="T334" id="Seg_5561" s="T333">ptcp</ta>
            <ta e="T454" id="Seg_5562" s="T334">n</ta>
            <ta e="T335" id="Seg_5563" s="T454">n</ta>
            <ta e="T336" id="Seg_5564" s="T335">v</ta>
            <ta e="T337" id="Seg_5565" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_5566" s="T337">adj</ta>
            <ta e="T339" id="Seg_5567" s="T338">adj</ta>
            <ta e="T340" id="Seg_5568" s="T339">n</ta>
            <ta e="T341" id="Seg_5569" s="T340">n</ta>
            <ta e="T342" id="Seg_5570" s="T341">v</ta>
            <ta e="T343" id="Seg_5571" s="T342">n</ta>
            <ta e="T344" id="Seg_5572" s="T343">adv</ta>
            <ta e="T345" id="Seg_5573" s="T344">v</ta>
            <ta e="T346" id="Seg_5574" s="T345">adv</ta>
            <ta e="T347" id="Seg_5575" s="T346">adv</ta>
            <ta e="T348" id="Seg_5576" s="T347">v</ta>
            <ta e="T349" id="Seg_5577" s="T348">num</ta>
            <ta e="T350" id="Seg_5578" s="T349">n</ta>
            <ta e="T351" id="Seg_5579" s="T350">adv</ta>
            <ta e="T352" id="Seg_5580" s="T351">v</ta>
            <ta e="T353" id="Seg_5581" s="T352">adv</ta>
            <ta e="T354" id="Seg_5582" s="T353">v</ta>
            <ta e="T355" id="Seg_5583" s="T354">n</ta>
            <ta e="T356" id="Seg_5584" s="T355">n</ta>
            <ta e="T357" id="Seg_5585" s="T356">adv</ta>
            <ta e="T358" id="Seg_5586" s="T357">v</ta>
            <ta e="T359" id="Seg_5587" s="T358">n</ta>
            <ta e="T360" id="Seg_5588" s="T359">n</ta>
            <ta e="T361" id="Seg_5589" s="T360">n</ta>
            <ta e="T362" id="Seg_5590" s="T361">adv</ta>
            <ta e="T363" id="Seg_5591" s="T362">v</ta>
            <ta e="T364" id="Seg_5592" s="T363">adv</ta>
            <ta e="T365" id="Seg_5593" s="T364">n</ta>
            <ta e="T366" id="Seg_5594" s="T365">adj</ta>
            <ta e="T367" id="Seg_5595" s="T366">num</ta>
            <ta e="T368" id="Seg_5596" s="T367">n</ta>
            <ta e="T369" id="Seg_5597" s="T368">v</ta>
            <ta e="T370" id="Seg_5598" s="T369">adv</ta>
            <ta e="T371" id="Seg_5599" s="T370">adj</ta>
            <ta e="T372" id="Seg_5600" s="T371">adj</ta>
            <ta e="T373" id="Seg_5601" s="T372">n</ta>
            <ta e="T374" id="Seg_5602" s="T373">n</ta>
            <ta e="T375" id="Seg_5603" s="T374">pro</ta>
            <ta e="T376" id="Seg_5604" s="T375">n</ta>
            <ta e="T377" id="Seg_5605" s="T376">n</ta>
            <ta e="T378" id="Seg_5606" s="T377">adv</ta>
            <ta e="T379" id="Seg_5607" s="T378">adv</ta>
            <ta e="T380" id="Seg_5608" s="T379">v</ta>
            <ta e="T381" id="Seg_5609" s="T380">adv</ta>
            <ta e="T382" id="Seg_5610" s="T381">v</ta>
            <ta e="T383" id="Seg_5611" s="T382">num</ta>
            <ta e="T384" id="Seg_5612" s="T383">adv</ta>
            <ta e="T385" id="Seg_5613" s="T384">v</ta>
            <ta e="T386" id="Seg_5614" s="T385">n</ta>
            <ta e="T387" id="Seg_5615" s="T386">dem</ta>
            <ta e="T388" id="Seg_5616" s="T387">n</ta>
            <ta e="T389" id="Seg_5617" s="T388">adv</ta>
            <ta e="T390" id="Seg_5618" s="T389">v</ta>
            <ta e="T391" id="Seg_5619" s="T390">n</ta>
            <ta e="T392" id="Seg_5620" s="T391">n</ta>
            <ta e="T393" id="Seg_5621" s="T392">adv</ta>
            <ta e="T394" id="Seg_5622" s="T393">v</ta>
            <ta e="T395" id="Seg_5623" s="T394">n</ta>
            <ta e="T396" id="Seg_5624" s="T395">pers</ta>
            <ta e="T397" id="Seg_5625" s="T396">adv</ta>
            <ta e="T398" id="Seg_5626" s="T397">v</ta>
            <ta e="T399" id="Seg_5627" s="T398">pers</ta>
            <ta e="T400" id="Seg_5628" s="T399">n</ta>
            <ta e="T401" id="Seg_5629" s="T400">v</ta>
            <ta e="T402" id="Seg_5630" s="T401">pers</ta>
            <ta e="T403" id="Seg_5631" s="T402">num</ta>
            <ta e="T404" id="Seg_5632" s="T403">n</ta>
            <ta e="T405" id="Seg_5633" s="T404">v</ta>
            <ta e="T406" id="Seg_5634" s="T405">adj</ta>
            <ta e="T407" id="Seg_5635" s="T406">n</ta>
            <ta e="T408" id="Seg_5636" s="T407">pers</ta>
            <ta e="T409" id="Seg_5637" s="T408">adv</ta>
            <ta e="T410" id="Seg_5638" s="T409">v</ta>
            <ta e="T411" id="Seg_5639" s="T410">n</ta>
            <ta e="T412" id="Seg_5640" s="T411">n</ta>
            <ta e="T413" id="Seg_5641" s="T412">adv</ta>
            <ta e="T414" id="Seg_5642" s="T413">v</ta>
            <ta e="T415" id="Seg_5643" s="T414">n</ta>
            <ta e="T416" id="Seg_5644" s="T415">n</ta>
            <ta e="T417" id="Seg_5645" s="T416">pers</ta>
            <ta e="T418" id="Seg_5646" s="T417">n</ta>
            <ta e="T419" id="Seg_5647" s="T418">n</ta>
            <ta e="T420" id="Seg_5648" s="T419">v</ta>
            <ta e="T421" id="Seg_5649" s="T420">n</ta>
            <ta e="T422" id="Seg_5650" s="T421">adv</ta>
            <ta e="T423" id="Seg_5651" s="T422">v</ta>
            <ta e="T424" id="Seg_5652" s="T423">n</ta>
            <ta e="T425" id="Seg_5653" s="T424">n</ta>
            <ta e="T426" id="Seg_5654" s="T425">interrog</ta>
            <ta e="T427" id="Seg_5655" s="T426">v</ta>
            <ta e="T428" id="Seg_5656" s="T427">adj</ta>
            <ta e="T429" id="Seg_5657" s="T428">n</ta>
            <ta e="T430" id="Seg_5658" s="T429">n</ta>
            <ta e="T431" id="Seg_5659" s="T430">adv</ta>
            <ta e="T432" id="Seg_5660" s="T431">v</ta>
            <ta e="T433" id="Seg_5661" s="T432">ptcl</ta>
            <ta e="T434" id="Seg_5662" s="T433">emphpro</ta>
            <ta e="T435" id="Seg_5663" s="T434">n</ta>
            <ta e="T436" id="Seg_5664" s="T435">v</ta>
            <ta e="T437" id="Seg_5665" s="T436">ptcl</ta>
            <ta e="T438" id="Seg_5666" s="T437">emphpro</ta>
            <ta e="T439" id="Seg_5667" s="T438">n</ta>
            <ta e="T440" id="Seg_5668" s="T439">v</ta>
            <ta e="T441" id="Seg_5669" s="T440">adv</ta>
            <ta e="T442" id="Seg_5670" s="T441">n</ta>
            <ta e="T443" id="Seg_5671" s="T442">adv</ta>
            <ta e="T444" id="Seg_5672" s="T443">v</ta>
            <ta e="T445" id="Seg_5673" s="T444">ptcl</ta>
            <ta e="T446" id="Seg_5674" s="T445">emphpro</ta>
            <ta e="T447" id="Seg_5675" s="T446">n</ta>
            <ta e="T448" id="Seg_5676" s="T447">v</ta>
            <ta e="T449" id="Seg_5677" s="T448">adj</ta>
            <ta e="T450" id="Seg_5678" s="T449">adv</ta>
            <ta e="T451" id="Seg_5679" s="T450">n</ta>
            <ta e="T452" id="Seg_5680" s="T451">adv</ta>
            <ta e="T453" id="Seg_5681" s="T452">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T5" id="Seg_5682" s="T4">np.h:Th 0.3.h:Poss</ta>
            <ta e="T9" id="Seg_5683" s="T8">np.h:Th 0.3.h:Poss</ta>
            <ta e="T10" id="Seg_5684" s="T9">np.h:A</ta>
            <ta e="T12" id="Seg_5685" s="T11">adv:So</ta>
            <ta e="T15" id="Seg_5686" s="T14">pro.h:P</ta>
            <ta e="T16" id="Seg_5687" s="T15">0.3.h:A</ta>
            <ta e="T17" id="Seg_5688" s="T16">0.3.h:Poss</ta>
            <ta e="T18" id="Seg_5689" s="T17">np.h:A</ta>
            <ta e="T19" id="Seg_5690" s="T18">np:G</ta>
            <ta e="T22" id="Seg_5691" s="T21">np.h:P</ta>
            <ta e="T23" id="Seg_5692" s="T22">adv:Time</ta>
            <ta e="T24" id="Seg_5693" s="T23">0.3.h:A</ta>
            <ta e="T26" id="Seg_5694" s="T25">0.3:Th</ta>
            <ta e="T29" id="Seg_5695" s="T28">np.h:Th</ta>
            <ta e="T30" id="Seg_5696" s="T29">0.3.h:A</ta>
            <ta e="T32" id="Seg_5697" s="T31">np.h:Poss</ta>
            <ta e="T33" id="Seg_5698" s="T32">pro.h:Th</ta>
            <ta e="T34" id="Seg_5699" s="T33">0.3.h:A</ta>
            <ta e="T37" id="Seg_5700" s="T36">np.h:G</ta>
            <ta e="T38" id="Seg_5701" s="T37">pro.h:A</ta>
            <ta e="T39" id="Seg_5702" s="T38">adv:Time</ta>
            <ta e="T42" id="Seg_5703" s="T41">0.2.h:A</ta>
            <ta e="T45" id="Seg_5704" s="T44">np.h:A</ta>
            <ta e="T46" id="Seg_5705" s="T45">adv:G</ta>
            <ta e="T47" id="Seg_5706" s="T46">0.3.h:Th</ta>
            <ta e="T51" id="Seg_5707" s="T50">np.h:Th</ta>
            <ta e="T52" id="Seg_5708" s="T51">np.h:A</ta>
            <ta e="T54" id="Seg_5709" s="T53">adv:Time</ta>
            <ta e="T56" id="Seg_5710" s="T55">np.h:A</ta>
            <ta e="T57" id="Seg_5711" s="T56">adv:G</ta>
            <ta e="T59" id="Seg_5712" s="T58">np.h:A</ta>
            <ta e="T67" id="Seg_5713" s="T66">np.h:R</ta>
            <ta e="T68" id="Seg_5714" s="T67">np:Th</ta>
            <ta e="T69" id="Seg_5715" s="T68">np:Th</ta>
            <ta e="T70" id="Seg_5716" s="T69">0.2.h:A</ta>
            <ta e="T73" id="Seg_5717" s="T72">pro:Th</ta>
            <ta e="T74" id="Seg_5718" s="T73">0.2.h:A</ta>
            <ta e="T75" id="Seg_5719" s="T74">np.h:A</ta>
            <ta e="T76" id="Seg_5720" s="T75">adv:Time</ta>
            <ta e="T78" id="Seg_5721" s="T77">0.3.h:A</ta>
            <ta e="T79" id="Seg_5722" s="T78">np.h:A</ta>
            <ta e="T82" id="Seg_5723" s="T81">np:Th</ta>
            <ta e="T83" id="Seg_5724" s="T82">0.3.h:A</ta>
            <ta e="T85" id="Seg_5725" s="T84">np:Poss</ta>
            <ta e="T86" id="Seg_5726" s="T85">np:Th</ta>
            <ta e="T88" id="Seg_5727" s="T87">np:Ins</ta>
            <ta e="T89" id="Seg_5728" s="T88">0.3.h:A</ta>
            <ta e="T90" id="Seg_5729" s="T89">np.h:A</ta>
            <ta e="T91" id="Seg_5730" s="T90">np.h:R 0.3.h:Poss</ta>
            <ta e="T95" id="Seg_5731" s="T94">np:Time</ta>
            <ta e="T96" id="Seg_5732" s="T95">np.h:A</ta>
            <ta e="T105" id="Seg_5733" s="T104">np:L</ta>
            <ta e="T108" id="Seg_5734" s="T107">np.h:Th</ta>
            <ta e="T110" id="Seg_5735" s="T109">np:Com 0.3.h:Poss</ta>
            <ta e="T112" id="Seg_5736" s="T111">np:Ins</ta>
            <ta e="T113" id="Seg_5737" s="T112">0.3.h:A</ta>
            <ta e="T115" id="Seg_5738" s="T114">np.h:A</ta>
            <ta e="T118" id="Seg_5739" s="T117">0.3.h:A</ta>
            <ta e="T119" id="Seg_5740" s="T118">np:Th</ta>
            <ta e="T120" id="Seg_5741" s="T119">0.3.h:E</ta>
            <ta e="T121" id="Seg_5742" s="T120">np.h:A</ta>
            <ta e="T124" id="Seg_5743" s="T123">np.h:Th 0.3.h:Poss</ta>
            <ta e="T127" id="Seg_5744" s="T126">np.h:Th 0.3.h:Poss</ta>
            <ta e="T129" id="Seg_5745" s="T128">0.3.h:A</ta>
            <ta e="T130" id="Seg_5746" s="T129">adv:G</ta>
            <ta e="T131" id="Seg_5747" s="T130">0.2.h:A</ta>
            <ta e="T132" id="Seg_5748" s="T131">np.h:Th</ta>
            <ta e="T133" id="Seg_5749" s="T132">adv:G</ta>
            <ta e="T134" id="Seg_5750" s="T133">0.2.h:A</ta>
            <ta e="T135" id="Seg_5751" s="T134">np.h:Th</ta>
            <ta e="T138" id="Seg_5752" s="T137">np.h:R</ta>
            <ta e="T139" id="Seg_5753" s="T138">0.3.h:A</ta>
            <ta e="T142" id="Seg_5754" s="T141">np:Time</ta>
            <ta e="T144" id="Seg_5755" s="T143">adv:Time</ta>
            <ta e="T145" id="Seg_5756" s="T144">np.h:A</ta>
            <ta e="T146" id="Seg_5757" s="T145">0.3.h:A</ta>
            <ta e="T149" id="Seg_5758" s="T148">np:P 0.3.h:Poss</ta>
            <ta e="T152" id="Seg_5759" s="T151">np:Ins</ta>
            <ta e="T153" id="Seg_5760" s="T152">0.3.h:A</ta>
            <ta e="T154" id="Seg_5761" s="T153">0.3:P</ta>
            <ta e="T155" id="Seg_5762" s="T154">np.h:Th 0.3.h:Poss</ta>
            <ta e="T159" id="Seg_5763" s="T158">adv:G</ta>
            <ta e="T160" id="Seg_5764" s="T159">0.3.h:A</ta>
            <ta e="T161" id="Seg_5765" s="T160">adv:L</ta>
            <ta e="T162" id="Seg_5766" s="T161">np:P</ta>
            <ta e="T163" id="Seg_5767" s="T162">0.3.h:A</ta>
            <ta e="T164" id="Seg_5768" s="T163">np:P</ta>
            <ta e="T165" id="Seg_5769" s="T164">0.3.h:A</ta>
            <ta e="T167" id="Seg_5770" s="T166">np:P</ta>
            <ta e="T168" id="Seg_5771" s="T167">0.3.h:A</ta>
            <ta e="T170" id="Seg_5772" s="T169">pro:P</ta>
            <ta e="T171" id="Seg_5773" s="T170">0.3.h:A</ta>
            <ta e="T172" id="Seg_5774" s="T171">pro.h:Th</ta>
            <ta e="T173" id="Seg_5775" s="T172">adv:L</ta>
            <ta e="T175" id="Seg_5776" s="T174">pro.h:A</ta>
            <ta e="T176" id="Seg_5777" s="T175">adv:L</ta>
            <ta e="T178" id="Seg_5778" s="T177">0.3.h:A</ta>
            <ta e="T179" id="Seg_5779" s="T178">np.h:A</ta>
            <ta e="T183" id="Seg_5780" s="T182">np:P</ta>
            <ta e="T186" id="Seg_5781" s="T185">np:Ins</ta>
            <ta e="T187" id="Seg_5782" s="T186">0.2.h:A</ta>
            <ta e="T189" id="Seg_5783" s="T188">np:P</ta>
            <ta e="T191" id="Seg_5784" s="T190">np:A</ta>
            <ta e="T194" id="Seg_5785" s="T193">pro:A</ta>
            <ta e="T196" id="Seg_5786" s="T195">0.3:P</ta>
            <ta e="T202" id="Seg_5787" s="T201">np.h:A</ta>
            <ta e="T204" id="Seg_5788" s="T203">0.3.h:A</ta>
            <ta e="T205" id="Seg_5789" s="T204">pp:L</ta>
            <ta e="T208" id="Seg_5790" s="T207">pp:L</ta>
            <ta e="T212" id="Seg_5791" s="T211">np:Time</ta>
            <ta e="T213" id="Seg_5792" s="T212">adv:So</ta>
            <ta e="T214" id="Seg_5793" s="T213">np:G</ta>
            <ta e="T217" id="Seg_5794" s="T216">np:P</ta>
            <ta e="T220" id="Seg_5795" s="T219">0.3.h:A</ta>
            <ta e="T221" id="Seg_5796" s="T220">np:G</ta>
            <ta e="T222" id="Seg_5797" s="T221">np.h:A</ta>
            <ta e="T225" id="Seg_5798" s="T224">np.h:Th</ta>
            <ta e="T230" id="Seg_5799" s="T229">np:Th</ta>
            <ta e="T231" id="Seg_5800" s="T230">0.3.h:A</ta>
            <ta e="T234" id="Seg_5801" s="T233">0.3.h:A</ta>
            <ta e="T235" id="Seg_5802" s="T234">np:Poss</ta>
            <ta e="T236" id="Seg_5803" s="T235">np:Path</ta>
            <ta e="T237" id="Seg_5804" s="T236">np:Poss</ta>
            <ta e="T239" id="Seg_5805" s="T238">np:Th</ta>
            <ta e="T243" id="Seg_5806" s="T242">np:Th 0.3.h:Poss</ta>
            <ta e="T245" id="Seg_5807" s="T244">np:Ins</ta>
            <ta e="T246" id="Seg_5808" s="T245">0.3.h:A</ta>
            <ta e="T247" id="Seg_5809" s="T246">0.3.h:A</ta>
            <ta e="T249" id="Seg_5810" s="T248">np:Th</ta>
            <ta e="T250" id="Seg_5811" s="T249">np:Poss</ta>
            <ta e="T251" id="Seg_5812" s="T250">np:Poss</ta>
            <ta e="T253" id="Seg_5813" s="T252">0.3.h:A</ta>
            <ta e="T255" id="Seg_5814" s="T254">np:Th</ta>
            <ta e="T257" id="Seg_5815" s="T256">0.3.h:A</ta>
            <ta e="T258" id="Seg_5816" s="T257">np:G</ta>
            <ta e="T259" id="Seg_5817" s="T258">np:Ins</ta>
            <ta e="T260" id="Seg_5818" s="T259">adv:Time</ta>
            <ta e="T261" id="Seg_5819" s="T260">0.3.h:A</ta>
            <ta e="T263" id="Seg_5820" s="T262">pp:G</ta>
            <ta e="T265" id="Seg_5821" s="T264">np:P</ta>
            <ta e="T271" id="Seg_5822" s="T270">np.h:P</ta>
            <ta e="T272" id="Seg_5823" s="T271">np:A</ta>
            <ta e="T275" id="Seg_5824" s="T274">adv:G</ta>
            <ta e="T276" id="Seg_5825" s="T275">np.h:A</ta>
            <ta e="T277" id="Seg_5826" s="T276">np.h:A</ta>
            <ta e="T278" id="Seg_5827" s="T277">adv:G</ta>
            <ta e="T279" id="Seg_5828" s="T278">0.3.h:Th</ta>
            <ta e="T281" id="Seg_5829" s="T280">np:Th 0.3.h:Poss</ta>
            <ta e="T285" id="Seg_5830" s="T284">adv:G</ta>
            <ta e="T286" id="Seg_5831" s="T285">0.3.h:Th</ta>
            <ta e="T288" id="Seg_5832" s="T287">adv:G</ta>
            <ta e="T289" id="Seg_5833" s="T288">0.3.h:A 0.3.h:P</ta>
            <ta e="T290" id="Seg_5834" s="T289">np.h:A</ta>
            <ta e="T291" id="Seg_5835" s="T290">np:Th</ta>
            <ta e="T295" id="Seg_5836" s="T294">pp:Path</ta>
            <ta e="T300" id="Seg_5837" s="T299">np:Poss</ta>
            <ta e="T301" id="Seg_5838" s="T300">np:G</ta>
            <ta e="T302" id="Seg_5839" s="T301">np:G</ta>
            <ta e="T303" id="Seg_5840" s="T302">0.3.h:A 0.3:Th</ta>
            <ta e="T304" id="Seg_5841" s="T303">np:P</ta>
            <ta e="T305" id="Seg_5842" s="T304">np:Ins</ta>
            <ta e="T306" id="Seg_5843" s="T305">0.3.h:A</ta>
            <ta e="T310" id="Seg_5844" s="T309">np.h:Poss</ta>
            <ta e="T311" id="Seg_5845" s="T310">np.h:Th</ta>
            <ta e="T314" id="Seg_5846" s="T313">adv:Time</ta>
            <ta e="T315" id="Seg_5847" s="T314">adv:G</ta>
            <ta e="T316" id="Seg_5848" s="T315">0.3.h:A</ta>
            <ta e="T317" id="Seg_5849" s="T316">np.h:Poss</ta>
            <ta e="T318" id="Seg_5850" s="T317">np:G</ta>
            <ta e="T321" id="Seg_5851" s="T320">np:G</ta>
            <ta e="T322" id="Seg_5852" s="T321">0.3.h:A</ta>
            <ta e="T325" id="Seg_5853" s="T324">np.h:Th</ta>
            <ta e="T326" id="Seg_5854" s="T325">0.3.h:A</ta>
            <ta e="T328" id="Seg_5855" s="T327">np.h:Th</ta>
            <ta e="T329" id="Seg_5856" s="T328">0.3.h:A</ta>
            <ta e="T331" id="Seg_5857" s="T330">np:G 0.3.h:Poss</ta>
            <ta e="T332" id="Seg_5858" s="T331">adv:So</ta>
            <ta e="T333" id="Seg_5859" s="T332">0.3.h:A</ta>
            <ta e="T335" id="Seg_5860" s="T454">np.h:Th</ta>
            <ta e="T340" id="Seg_5861" s="T339">np.h:A</ta>
            <ta e="T341" id="Seg_5862" s="T340">np.h:Th</ta>
            <ta e="T343" id="Seg_5863" s="T342">np.h:A</ta>
            <ta e="T344" id="Seg_5864" s="T343">adv:G</ta>
            <ta e="T345" id="Seg_5865" s="T344">0.3.h:P</ta>
            <ta e="T346" id="Seg_5866" s="T345">adv:Time</ta>
            <ta e="T347" id="Seg_5867" s="T346">adv:G</ta>
            <ta e="T348" id="Seg_5868" s="T347">0.3.h:A</ta>
            <ta e="T350" id="Seg_5869" s="T349">np.h:A</ta>
            <ta e="T351" id="Seg_5870" s="T350">adv:G</ta>
            <ta e="T353" id="Seg_5871" s="T352">adv:G</ta>
            <ta e="T354" id="Seg_5872" s="T353">0.3.h:A</ta>
            <ta e="T355" id="Seg_5873" s="T354">np.h:Poss</ta>
            <ta e="T356" id="Seg_5874" s="T355">np:G</ta>
            <ta e="T357" id="Seg_5875" s="T356">adv:G</ta>
            <ta e="T358" id="Seg_5876" s="T357">0.3.h:A</ta>
            <ta e="T359" id="Seg_5877" s="T358">np.h:Poss</ta>
            <ta e="T360" id="Seg_5878" s="T359">np:G</ta>
            <ta e="T361" id="Seg_5879" s="T360">np.h:A 0.3.h:Poss</ta>
            <ta e="T362" id="Seg_5880" s="T361">adv:G</ta>
            <ta e="T364" id="Seg_5881" s="T363">adv:L</ta>
            <ta e="T365" id="Seg_5882" s="T364">np:L</ta>
            <ta e="T368" id="Seg_5883" s="T367">np.h:Th</ta>
            <ta e="T369" id="Seg_5884" s="T368">0.3.h:A</ta>
            <ta e="T370" id="Seg_5885" s="T369">adv:Time</ta>
            <ta e="T373" id="Seg_5886" s="T372">np.h:Poss</ta>
            <ta e="T374" id="Seg_5887" s="T373">np.h:Th</ta>
            <ta e="T376" id="Seg_5888" s="T375">np.h:Th</ta>
            <ta e="T377" id="Seg_5889" s="T376">np:Th</ta>
            <ta e="T378" id="Seg_5890" s="T377">0.3.h:A</ta>
            <ta e="T379" id="Seg_5891" s="T378">adv:G</ta>
            <ta e="T380" id="Seg_5892" s="T379">0.3.h:A</ta>
            <ta e="T381" id="Seg_5893" s="T380">adv:L</ta>
            <ta e="T382" id="Seg_5894" s="T381">0.3.h:A</ta>
            <ta e="T384" id="Seg_5895" s="T383">adv:Time</ta>
            <ta e="T385" id="Seg_5896" s="T384">0.3.h:Th</ta>
            <ta e="T386" id="Seg_5897" s="T385">np.h:A 0.3.h:Poss</ta>
            <ta e="T388" id="Seg_5898" s="T387">np:Time</ta>
            <ta e="T389" id="Seg_5899" s="T388">adv:G</ta>
            <ta e="T392" id="Seg_5900" s="T391">np.h:G</ta>
            <ta e="T393" id="Seg_5901" s="T392">0.3.h:A</ta>
            <ta e="T395" id="Seg_5902" s="T394">np.h:A 0.3.h:Poss</ta>
            <ta e="T396" id="Seg_5903" s="T395">pro.h:E</ta>
            <ta e="T399" id="Seg_5904" s="T398">0.1.h:Poss</ta>
            <ta e="T400" id="Seg_5905" s="T399">np.h:P</ta>
            <ta e="T401" id="Seg_5906" s="T400">0.3.h:A</ta>
            <ta e="T402" id="Seg_5907" s="T401">pro.h:Poss</ta>
            <ta e="T404" id="Seg_5908" s="T403">np.h:Th</ta>
            <ta e="T407" id="Seg_5909" s="T406">np.h:Th 0.3.h:Poss</ta>
            <ta e="T408" id="Seg_5910" s="T407">pro.h:A</ta>
            <ta e="T409" id="Seg_5911" s="T408">0.3.h:A</ta>
            <ta e="T411" id="Seg_5912" s="T410">np.h:Poss 0.3.h:Poss</ta>
            <ta e="T412" id="Seg_5913" s="T411">np:P</ta>
            <ta e="T413" id="Seg_5914" s="T412">adv:L</ta>
            <ta e="T414" id="Seg_5915" s="T413">0.3.h:A</ta>
            <ta e="T417" id="Seg_5916" s="T416">pro.h:A</ta>
            <ta e="T418" id="Seg_5917" s="T417">np:Poss</ta>
            <ta e="T419" id="Seg_5918" s="T418">np:P</ta>
            <ta e="T421" id="Seg_5919" s="T420">np.h:A</ta>
            <ta e="T424" id="Seg_5920" s="T423">np:Poss</ta>
            <ta e="T425" id="Seg_5921" s="T424">np:P</ta>
            <ta e="T426" id="Seg_5922" s="T425">pro.h:A</ta>
            <ta e="T429" id="Seg_5923" s="T428">np.h:Poss 0.3.h:Poss</ta>
            <ta e="T430" id="Seg_5924" s="T429">np.h:A</ta>
            <ta e="T431" id="Seg_5925" s="T430">adv:G</ta>
            <ta e="T435" id="Seg_5926" s="T434">np.h:A 0.3.h:Poss</ta>
            <ta e="T439" id="Seg_5927" s="T438">np.h:A 0.3.h:Poss</ta>
            <ta e="T441" id="Seg_5928" s="T440">adv:Time</ta>
            <ta e="T442" id="Seg_5929" s="T441">np.h:A</ta>
            <ta e="T446" id="Seg_5930" s="T445">pro.h:A</ta>
            <ta e="T447" id="Seg_5931" s="T446">np:Ins</ta>
            <ta e="T448" id="Seg_5932" s="T447">0.3:P</ta>
            <ta e="T450" id="Seg_5933" s="T449">adv:L</ta>
            <ta e="T451" id="Seg_5934" s="T450">np:Th</ta>
            <ta e="T452" id="Seg_5935" s="T451">adv:G</ta>
            <ta e="T453" id="Seg_5936" s="T452">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T5" id="Seg_5937" s="T4">np.h:S</ta>
            <ta e="T6" id="Seg_5938" s="T5">v:pred</ta>
            <ta e="T9" id="Seg_5939" s="T8">np.h:S</ta>
            <ta e="T10" id="Seg_5940" s="T9">np.h:S</ta>
            <ta e="T11" id="Seg_5941" s="T10">v:pred</ta>
            <ta e="T16" id="Seg_5942" s="T14">s:purp</ta>
            <ta e="T18" id="Seg_5943" s="T17">np.h:S</ta>
            <ta e="T20" id="Seg_5944" s="T19">v:pred</ta>
            <ta e="T21" id="Seg_5945" s="T20">s:purp</ta>
            <ta e="T22" id="Seg_5946" s="T21">np.h:O</ta>
            <ta e="T24" id="Seg_5947" s="T23">0.3.h:S v:pred</ta>
            <ta e="T26" id="Seg_5948" s="T25">0.3:S v:pred</ta>
            <ta e="T29" id="Seg_5949" s="T28">np.h:O</ta>
            <ta e="T30" id="Seg_5950" s="T29">0.3.h:S v:pred</ta>
            <ta e="T33" id="Seg_5951" s="T32">pro.h:O</ta>
            <ta e="T34" id="Seg_5952" s="T33">0.3.h:S v:pred</ta>
            <ta e="T38" id="Seg_5953" s="T37">pro.h:S</ta>
            <ta e="T41" id="Seg_5954" s="T40">v:pred</ta>
            <ta e="T42" id="Seg_5955" s="T41">0.2.h:S v:pred</ta>
            <ta e="T45" id="Seg_5956" s="T44">np.h:S</ta>
            <ta e="T47" id="Seg_5957" s="T45">s:temp</ta>
            <ta e="T49" id="Seg_5958" s="T48">v:pred</ta>
            <ta e="T51" id="Seg_5959" s="T50">np.h:O</ta>
            <ta e="T52" id="Seg_5960" s="T51">np.h:S</ta>
            <ta e="T53" id="Seg_5961" s="T52">v:pred</ta>
            <ta e="T56" id="Seg_5962" s="T55">np.h:S</ta>
            <ta e="T58" id="Seg_5963" s="T57">v:pred</ta>
            <ta e="T59" id="Seg_5964" s="T58">np.h:S</ta>
            <ta e="T61" id="Seg_5965" s="T60">v:pred</ta>
            <ta e="T68" id="Seg_5966" s="T67">np:O</ta>
            <ta e="T69" id="Seg_5967" s="T68">np:O</ta>
            <ta e="T70" id="Seg_5968" s="T69">0.2.h:S v:pred</ta>
            <ta e="T73" id="Seg_5969" s="T72">pro:O</ta>
            <ta e="T74" id="Seg_5970" s="T73">0.2.h:S v:pred</ta>
            <ta e="T75" id="Seg_5971" s="T74">np.h:S</ta>
            <ta e="T77" id="Seg_5972" s="T76">v:pred</ta>
            <ta e="T78" id="Seg_5973" s="T77">0.3.h:S v:pred</ta>
            <ta e="T79" id="Seg_5974" s="T78">np.h:S</ta>
            <ta e="T81" id="Seg_5975" s="T80">v:pred</ta>
            <ta e="T82" id="Seg_5976" s="T81">np:O</ta>
            <ta e="T83" id="Seg_5977" s="T82">0.3.h:S v:pred</ta>
            <ta e="T86" id="Seg_5978" s="T85">np:O</ta>
            <ta e="T89" id="Seg_5979" s="T88">0.3.h:S v:pred</ta>
            <ta e="T90" id="Seg_5980" s="T89">np.h:S</ta>
            <ta e="T91" id="Seg_5981" s="T90">np.h:O</ta>
            <ta e="T92" id="Seg_5982" s="T91">v:pred</ta>
            <ta e="T96" id="Seg_5983" s="T95">np.h:S</ta>
            <ta e="T99" id="Seg_5984" s="T98">v:pred</ta>
            <ta e="T108" id="Seg_5985" s="T107">np.h:S</ta>
            <ta e="T113" id="Seg_5986" s="T112">0.3.h:S v:pred</ta>
            <ta e="T115" id="Seg_5987" s="T114">np.h:S</ta>
            <ta e="T117" id="Seg_5988" s="T116">v:pred</ta>
            <ta e="T118" id="Seg_5989" s="T117">0.3.h:S v:pred</ta>
            <ta e="T119" id="Seg_5990" s="T118">np:O</ta>
            <ta e="T120" id="Seg_5991" s="T119">0.3.h:S v:pred</ta>
            <ta e="T121" id="Seg_5992" s="T120">np.h:S</ta>
            <ta e="T123" id="Seg_5993" s="T122">v:pred</ta>
            <ta e="T124" id="Seg_5994" s="T123">np.h:S</ta>
            <ta e="T126" id="Seg_5995" s="T125">v:pred</ta>
            <ta e="T127" id="Seg_5996" s="T126">np.h:O</ta>
            <ta e="T129" id="Seg_5997" s="T128">0.3.h:S v:pred</ta>
            <ta e="T131" id="Seg_5998" s="T130">0.2.h:S v:pred</ta>
            <ta e="T132" id="Seg_5999" s="T131">np.h:O</ta>
            <ta e="T134" id="Seg_6000" s="T133">0.2.h:S v:pred</ta>
            <ta e="T135" id="Seg_6001" s="T134">np.h:S</ta>
            <ta e="T137" id="Seg_6002" s="T136">v:pred</ta>
            <ta e="T139" id="Seg_6003" s="T137">s:temp</ta>
            <ta e="T143" id="Seg_6004" s="T142">v:pred</ta>
            <ta e="T145" id="Seg_6005" s="T144">np.h:S</ta>
            <ta e="T146" id="Seg_6006" s="T145">0.3.h:S v:pred</ta>
            <ta e="T149" id="Seg_6007" s="T148">np:O</ta>
            <ta e="T153" id="Seg_6008" s="T152">0.3.h:S v:pred</ta>
            <ta e="T154" id="Seg_6009" s="T153">0.3:S v:pred</ta>
            <ta e="T155" id="Seg_6010" s="T154">np.h:O</ta>
            <ta e="T160" id="Seg_6011" s="T159">0.3.h:S v:pred</ta>
            <ta e="T162" id="Seg_6012" s="T161">np:O</ta>
            <ta e="T163" id="Seg_6013" s="T162">0.3.h:S v:pred</ta>
            <ta e="T164" id="Seg_6014" s="T163">np:O</ta>
            <ta e="T165" id="Seg_6015" s="T164">0.3.h:S v:pred</ta>
            <ta e="T167" id="Seg_6016" s="T166">np:O</ta>
            <ta e="T168" id="Seg_6017" s="T167">0.3.h:S v:pred</ta>
            <ta e="T170" id="Seg_6018" s="T169">pro:O</ta>
            <ta e="T171" id="Seg_6019" s="T170">0.3.h:S v:pred</ta>
            <ta e="T172" id="Seg_6020" s="T171">pro.h:S</ta>
            <ta e="T174" id="Seg_6021" s="T173">adj:pred</ta>
            <ta e="T175" id="Seg_6022" s="T174">pro.h:S</ta>
            <ta e="T177" id="Seg_6023" s="T176">v:pred</ta>
            <ta e="T178" id="Seg_6024" s="T177">0.3.h:S v:pred</ta>
            <ta e="T179" id="Seg_6025" s="T178">np.h:S</ta>
            <ta e="T181" id="Seg_6026" s="T180">v:pred</ta>
            <ta e="T183" id="Seg_6027" s="T182">np:O</ta>
            <ta e="T187" id="Seg_6028" s="T186">0.2.h:S v:pred</ta>
            <ta e="T189" id="Seg_6029" s="T188">np:O</ta>
            <ta e="T191" id="Seg_6030" s="T190">np:S</ta>
            <ta e="T193" id="Seg_6031" s="T192">v:pred</ta>
            <ta e="T194" id="Seg_6032" s="T193">pro:S</ta>
            <ta e="T196" id="Seg_6033" s="T195">v:pred 0.3:O</ta>
            <ta e="T198" id="Seg_6034" s="T197">v:pred</ta>
            <ta e="T202" id="Seg_6035" s="T201">np.h:S</ta>
            <ta e="T204" id="Seg_6036" s="T203">0.3.h:S v:pred</ta>
            <ta e="T217" id="Seg_6037" s="T216">np:S</ta>
            <ta e="T218" id="Seg_6038" s="T217">v:pred</ta>
            <ta e="T220" id="Seg_6039" s="T219">0.3.h:S v:pred</ta>
            <ta e="T222" id="Seg_6040" s="T221">np.h:S</ta>
            <ta e="T224" id="Seg_6041" s="T223">v:pred</ta>
            <ta e="T225" id="Seg_6042" s="T224">np.h:O</ta>
            <ta e="T230" id="Seg_6043" s="T229">np:O</ta>
            <ta e="T231" id="Seg_6044" s="T230">0.3.h:S v:pred</ta>
            <ta e="T234" id="Seg_6045" s="T233">0.3.h:S v:pred</ta>
            <ta e="T239" id="Seg_6046" s="T238">np:S</ta>
            <ta e="T240" id="Seg_6047" s="T239">v:pred</ta>
            <ta e="T243" id="Seg_6048" s="T242">np:S</ta>
            <ta e="T244" id="Seg_6049" s="T243">v:pred</ta>
            <ta e="T246" id="Seg_6050" s="T245">0.3.h:S v:pred</ta>
            <ta e="T247" id="Seg_6051" s="T246">0.3.h:S v:pred</ta>
            <ta e="T249" id="Seg_6052" s="T248">np:O</ta>
            <ta e="T253" id="Seg_6053" s="T252">0.3.h:S v:pred</ta>
            <ta e="T255" id="Seg_6054" s="T254">np:O</ta>
            <ta e="T257" id="Seg_6055" s="T256">0.3.h:S v:pred</ta>
            <ta e="T261" id="Seg_6056" s="T260">0.3.h:S v:pred</ta>
            <ta e="T265" id="Seg_6057" s="T264">np:S</ta>
            <ta e="T267" id="Seg_6058" s="T266">v:pred</ta>
            <ta e="T271" id="Seg_6059" s="T270">np.h:O</ta>
            <ta e="T272" id="Seg_6060" s="T271">np:S</ta>
            <ta e="T273" id="Seg_6061" s="T272">v:pred</ta>
            <ta e="T274" id="Seg_6062" s="T273">v:pred</ta>
            <ta e="T276" id="Seg_6063" s="T275">np.h:S</ta>
            <ta e="T277" id="Seg_6064" s="T276">np.h:S</ta>
            <ta e="T279" id="Seg_6065" s="T277">s:temp</ta>
            <ta e="T281" id="Seg_6066" s="T280">np:O</ta>
            <ta e="T282" id="Seg_6067" s="T281">v:pred</ta>
            <ta e="T286" id="Seg_6068" s="T285">0.3.h:S v:pred</ta>
            <ta e="T289" id="Seg_6069" s="T288">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T290" id="Seg_6070" s="T289">np.h:S</ta>
            <ta e="T291" id="Seg_6071" s="T290">np:O</ta>
            <ta e="T293" id="Seg_6072" s="T292">v:pred</ta>
            <ta e="T303" id="Seg_6073" s="T302">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T304" id="Seg_6074" s="T303">np:O</ta>
            <ta e="T306" id="Seg_6075" s="T305">0.3.h:S v:pred</ta>
            <ta e="T311" id="Seg_6076" s="T310">np.h:S</ta>
            <ta e="T313" id="Seg_6077" s="T312">v:pred</ta>
            <ta e="T316" id="Seg_6078" s="T315">0.3.h:S v:pred</ta>
            <ta e="T322" id="Seg_6079" s="T321">0.3.h:S v:pred</ta>
            <ta e="T325" id="Seg_6080" s="T324">np.h:O</ta>
            <ta e="T326" id="Seg_6081" s="T325">0.3.h:S v:pred</ta>
            <ta e="T328" id="Seg_6082" s="T327">np.h:O</ta>
            <ta e="T329" id="Seg_6083" s="T328">0.3.h:S v:pred</ta>
            <ta e="T333" id="Seg_6084" s="T332">0.3.h:S v:pred</ta>
            <ta e="T335" id="Seg_6085" s="T454">np.h:S</ta>
            <ta e="T336" id="Seg_6086" s="T335">v:pred</ta>
            <ta e="T340" id="Seg_6087" s="T339">np.h:S</ta>
            <ta e="T341" id="Seg_6088" s="T340">np.h:O</ta>
            <ta e="T342" id="Seg_6089" s="T341">v:pred</ta>
            <ta e="T343" id="Seg_6090" s="T342">np.h:S</ta>
            <ta e="T345" id="Seg_6091" s="T344">v:pred 0.3.h:O</ta>
            <ta e="T348" id="Seg_6092" s="T347">0.3.h:S v:pred</ta>
            <ta e="T350" id="Seg_6093" s="T349">np.h:S</ta>
            <ta e="T352" id="Seg_6094" s="T351">v:pred</ta>
            <ta e="T354" id="Seg_6095" s="T353">0.3.h:S v:pred</ta>
            <ta e="T358" id="Seg_6096" s="T357">0.3.h:S v:pred</ta>
            <ta e="T361" id="Seg_6097" s="T360">np.h:S</ta>
            <ta e="T363" id="Seg_6098" s="T362">v:pred</ta>
            <ta e="T368" id="Seg_6099" s="T367">np.h:O</ta>
            <ta e="T369" id="Seg_6100" s="T368">0.3.h:S v:pred</ta>
            <ta e="T374" id="Seg_6101" s="T373">np.h:S</ta>
            <ta e="T378" id="Seg_6102" s="T374">s:temp</ta>
            <ta e="T380" id="Seg_6103" s="T379">0.3.h:S v:pred</ta>
            <ta e="T382" id="Seg_6104" s="T381">0.3.h:S v:pred</ta>
            <ta e="T385" id="Seg_6105" s="T384">0.3.h:S v:pred</ta>
            <ta e="T386" id="Seg_6106" s="T385">np.h:S</ta>
            <ta e="T390" id="Seg_6107" s="T389">v:pred</ta>
            <ta e="T393" id="Seg_6108" s="T390">s:temp</ta>
            <ta e="T394" id="Seg_6109" s="T393">v:pred</ta>
            <ta e="T395" id="Seg_6110" s="T394">np.h:S</ta>
            <ta e="T396" id="Seg_6111" s="T395">pro.h:S</ta>
            <ta e="T398" id="Seg_6112" s="T397">v:pred</ta>
            <ta e="T400" id="Seg_6113" s="T399">np.h:O</ta>
            <ta e="T401" id="Seg_6114" s="T400">0.3.h:S v:pred</ta>
            <ta e="T404" id="Seg_6115" s="T403">np.h:S</ta>
            <ta e="T405" id="Seg_6116" s="T404">v:pred</ta>
            <ta e="T407" id="Seg_6117" s="T406">np.h:S</ta>
            <ta e="T408" id="Seg_6118" s="T407">pro.h:S</ta>
            <ta e="T409" id="Seg_6119" s="T408">s:temp</ta>
            <ta e="T410" id="Seg_6120" s="T409">v:pred</ta>
            <ta e="T412" id="Seg_6121" s="T411">np:O</ta>
            <ta e="T414" id="Seg_6122" s="T413">0.3.h:S v:pred</ta>
            <ta e="T416" id="Seg_6123" s="T414">s:temp</ta>
            <ta e="T417" id="Seg_6124" s="T416">pro.h:S</ta>
            <ta e="T419" id="Seg_6125" s="T418">np:O</ta>
            <ta e="T420" id="Seg_6126" s="T419">v:pred</ta>
            <ta e="T421" id="Seg_6127" s="T420">np.h:S</ta>
            <ta e="T423" id="Seg_6128" s="T422">v:pred</ta>
            <ta e="T425" id="Seg_6129" s="T424">np:O</ta>
            <ta e="T426" id="Seg_6130" s="T425">pro.h:S</ta>
            <ta e="T427" id="Seg_6131" s="T426">v:pred</ta>
            <ta e="T430" id="Seg_6132" s="T429">np.h:S</ta>
            <ta e="T432" id="Seg_6133" s="T431">v:pred</ta>
            <ta e="T435" id="Seg_6134" s="T434">np.h:S</ta>
            <ta e="T436" id="Seg_6135" s="T435">v:pred</ta>
            <ta e="T439" id="Seg_6136" s="T438">np.h:S</ta>
            <ta e="T440" id="Seg_6137" s="T439">v:pred</ta>
            <ta e="T442" id="Seg_6138" s="T441">np.h:S</ta>
            <ta e="T444" id="Seg_6139" s="T443">v:pred</ta>
            <ta e="T446" id="Seg_6140" s="T445">pro.h:S</ta>
            <ta e="T448" id="Seg_6141" s="T447">v:pred 0.3:O</ta>
            <ta e="T451" id="Seg_6142" s="T450">np:O</ta>
            <ta e="T453" id="Seg_6143" s="T452">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_6144" s="T0">Старик "заячья шуба".</ta>
            <ta e="T9" id="Seg_6145" s="T3">[У старика] семь сыновей было, и семь невесток.</ta>
            <ta e="T16" id="Seg_6146" s="T9">Ненцы приехали снизу (/вниз) в верховье реки его убить.</ta>
            <ta e="T21" id="Seg_6147" s="T16">Сыновья ушли в лес белковать.</ta>
            <ta e="T24" id="Seg_6148" s="T21">Человека теперь убьют.</ta>
            <ta e="T26" id="Seg_6149" s="T24">Как быть? </ta>
            <ta e="T30" id="Seg_6150" s="T26">Десять селькупских детишек они воспитывали.</ta>
            <ta e="T34" id="Seg_6151" s="T30">Он [старик "заячья шуба"] посылает кого-то из ненцев [к ненцам].</ta>
            <ta e="T41" id="Seg_6152" s="T34">К старику "заячьей шубе" мы будто завтра придём.</ta>
            <ta e="T44" id="Seg_6153" s="T41">"Съездите для разговора."</ta>
            <ta e="T53" id="Seg_6154" s="T44">Парни [вдвоём], приехав домой, сказали: "Двух [селькупских] сирот-ребятишек ненцы [вдвоём?] держат".</ta>
            <ta e="T58" id="Seg_6155" s="T53">Потом два старика куда денутся?</ta>
            <ta e="T70" id="Seg_6156" s="T58">Старик, этот старик "заячья шуба", сказал невесткам: "Жир, еду принесите.</ta>
            <ta e="T74" id="Seg_6157" s="T70">Всё, что есть, принесите.</ta>
            <ta e="T78" id="Seg_6158" s="T74">Ненцы завтра приедут, есть будут."</ta>
            <ta e="T81" id="Seg_6159" s="T78">Старик так сказал.</ta>
            <ta e="T83" id="Seg_6160" s="T81">Еду принесли.</ta>
            <ta e="T89" id="Seg_6161" s="T83">Внутри этого дома новое сено постелили.</ta>
            <ta e="T92" id="Seg_6162" s="T89">Старик ненцев кормить начал.</ta>
            <ta e="T93" id="Seg_6163" s="T92">Ой!</ta>
            <ta e="T99" id="Seg_6164" s="T93">На следующий день ненцы собрались.</ta>
            <ta e="T102" id="Seg_6165" s="T99">Войско из тридцати людей. </ta>
            <ta e="T110" id="Seg_6166" s="T102">[Смотрят], в пустом чуме старик "заячья шуба" один с невестками [сидит].</ta>
            <ta e="T114" id="Seg_6167" s="T110">Они свежее сено постелили.</ta>
            <ta e="T117" id="Seg_6168" s="T114">Старик так [невесткам] сказал [сделать].</ta>
            <ta e="T118" id="Seg_6169" s="T117">Они начали есть.</ta>
            <ta e="T120" id="Seg_6170" s="T118">Еду нашли.</ta>
            <ta e="T123" id="Seg_6171" s="T120">Старик так сказал [невесткам найти].</ta>
            <ta e="T126" id="Seg_6172" s="T123">Старуха у него тоже была.</ta>
            <ta e="T129" id="Seg_6173" s="T126">Невесткам [старик] так сказал идти.</ta>
            <ta e="T131" id="Seg_6174" s="T129">"На улицу выходите.</ta>
            <ta e="T134" id="Seg_6175" s="T131">Детей на улицу унесите."</ta>
            <ta e="T139" id="Seg_6176" s="T134">Старик сам держится, людей кормит.</ta>
            <ta e="T145" id="Seg_6177" s="T139">Однажды утром приехали люди.</ta>
            <ta e="T146" id="Seg_6178" s="T145">Они едят.</ta>
            <ta e="T153" id="Seg_6179" s="T146">Он в печку большие брёвна подкладывает. </ta>
            <ta e="T154" id="Seg_6180" s="T153">[Огонь] горит.</ta>
            <ta e="T160" id="Seg_6181" s="T154">Невесток он просто так (ни за чем) на улицу отправил.</ta>
            <ta e="T163" id="Seg_6182" s="T160">На улице огонь зажгли.</ta>
            <ta e="T168" id="Seg_6183" s="T163">Огонь зажгли, им берестяные факелы зажгли.</ta>
            <ta e="T171" id="Seg_6184" s="T168">Таким образом что-то сушили.</ta>
            <ta e="T177" id="Seg_6185" s="T171">"Вы тут будете, я выйду."</ta>
            <ta e="T178" id="Seg_6186" s="T177">[Ненцы] едят.</ta>
            <ta e="T187" id="Seg_6187" s="T178">Ненцы говорят: "Дедушка, ты зачем в печку столько дров кладёшь?</ta>
            <ta e="T193" id="Seg_6188" s="T187">Огонь не охватит ли печку?"</ta>
            <ta e="T196" id="Seg_6189" s="T193">"Не охватит."</ta>
            <ta e="T202" id="Seg_6190" s="T196">Так заставил их быть [там] "заячья шуба" в лохмотьях.</ta>
            <ta e="T209" id="Seg_6191" s="T202">Он бегает [туда-сюда] рядом с печкой около большой двери.</ta>
            <ta e="T218" id="Seg_6192" s="T209">Один раз с улицы в дом головёшка упала.</ta>
            <ta e="T220" id="Seg_6193" s="T218">[Старик "заячья шуба"] побежал.</ta>
            <ta e="T231" id="Seg_6194" s="T220">В дверях ненцы чуть было не схватили старика, один только кусок заячьей шубы поймали.</ta>
            <ta e="T237" id="Seg_6195" s="T231">Вроде он вышел через окошко дома. </ta>
            <ta e="T240" id="Seg_6196" s="T237">Два окошка было.</ta>
            <ta e="T244" id="Seg_6197" s="T240">Большой что ли дом у него был!</ta>
            <ta e="T253" id="Seg_6198" s="T244">Огнём закрыл, поставил деревянные жерди, на глубину топорища зарыл.</ta>
            <ta e="T258" id="Seg_6199" s="T253">Те [невестки] огнём бросались, в дом.</ta>
            <ta e="T264" id="Seg_6200" s="T258">Порох раньше насыпали под сено.</ta>
            <ta e="T267" id="Seg_6201" s="T264">Огонь [порох] как рванёт!</ta>
            <ta e="T273" id="Seg_6202" s="T267">Войско из тех тридцати человек сгорело в огне.</ta>
            <ta e="T276" id="Seg_6203" s="T273">Невестки сверху отправились.</ta>
            <ta e="T282" id="Seg_6204" s="T276">Cтарик, выбежав наружу, держал в руках топор.</ta>
            <ta e="T289" id="Seg_6205" s="T282">Каждого, кто едва на улицу выскакивал, они в лицо били.</ta>
            <ta e="T293" id="Seg_6206" s="T289">Невестки огнём бросались.</ta>
            <ta e="T303" id="Seg_6207" s="T293">Сверху печки вниз и через два окошка в дом бросали.</ta>
            <ta e="T306" id="Seg_6208" s="T303">Дом сожгли.</ta>
            <ta e="T313" id="Seg_6209" s="T306">У этого ненецкого войска было два главаря.</ta>
            <ta e="T326" id="Seg_6210" s="T313">Потом [старик] на берег пошёл, к нартам ненцев, на берег что ли к нартам пошёл и двух невесток [с собой] повёл.</ta>
            <ta e="T331" id="Seg_6211" s="T326">Двух невесток повёл, к [убитым?] людям.</ta>
            <ta e="T336" id="Seg_6212" s="T331">С берега шёл, показался хромой старик-ненец.</ta>
            <ta e="T342" id="Seg_6213" s="T336">Старик "заячья шуба" женщин [сзади] что ли оставил.</ta>
            <ta e="T345" id="Seg_6214" s="T342">Старик в лицо [ненца] ударил.</ta>
            <ta e="T348" id="Seg_6215" s="T345">Потом на берег пошёл.</ta>
            <ta e="T358" id="Seg_6216" s="T348">Три невестки вниз [по реке] поехали, туда отправились, в чум ненцев вниз [по реке] поехали.</ta>
            <ta e="T363" id="Seg_6217" s="T358">В чум ненцев женщины вниз поехали.</ta>
            <ta e="T369" id="Seg_6218" s="T363">Там в чуме двух селькупских детей оставили [ненцы].</ta>
            <ta e="T374" id="Seg_6219" s="T369">Раньше – двух внуков старика "заячьей шубы". </ta>
            <ta e="T380" id="Seg_6220" s="T374">Они двух детей и оленей собрали и поехали вверх [в лес].</ta>
            <ta e="T382" id="Seg_6221" s="T380">Наверху [в лесу] чум поставили.</ta>
            <ta e="T385" id="Seg_6222" s="T382">Одну ночь переночевали.</ta>
            <ta e="T390" id="Seg_6223" s="T385">Сыновья в этот день домой приехали.</ta>
            <ta e="T395" id="Seg_6224" s="T390">Старика-отца [догоняя?], приехали сыновья.</ta>
            <ta e="T401" id="Seg_6225" s="T395">Они думали: "Нашего отца убили."</ta>
            <ta e="T407" id="Seg_6226" s="T401">У него [старика] семь сыновей было, и младшая невестка.</ta>
            <ta e="T410" id="Seg_6227" s="T407">Они, [догоняя?], собрались [вместе].</ta>
            <ta e="T414" id="Seg_6228" s="T410">Собаку сына на улице привязали.</ta>
            <ta e="T420" id="Seg_6229" s="T414">Поджидая оленей, они [сыновья] наступили на ногу собаки.</ta>
            <ta e="T427" id="Seg_6230" s="T420">Старик спросил: "Кто наступил на лапу собаки?"</ta>
            <ta e="T432" id="Seg_6231" s="T427">Жена младшего сына выбежала на улицу.</ta>
            <ta e="T436" id="Seg_6232" s="T432">Значит, её муж приехал.</ta>
            <ta e="T440" id="Seg_6233" s="T436">Значит, его сын приехал.</ta>
            <ta e="T448" id="Seg_6234" s="T440">Потом дедушка рассказал: "Мы сами [дом] сожгли.</ta>
            <ta e="T453" id="Seg_6235" s="T448">В ненецкую землю оленей вперёд перегнали".</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_6236" s="T0">Old man the “Hare Parka”.</ta>
            <ta e="T9" id="Seg_6237" s="T3">[The old man] had seven sons and seven daughters-in-law.</ta>
            <ta e="T16" id="Seg_6238" s="T9">Nenets people came from downstream to upstream to kill him.</ta>
            <ta e="T21" id="Seg_6239" s="T16">His sons went to the forest to hunt squirrels.</ta>
            <ta e="T24" id="Seg_6240" s="T21">They’ll kill the man now.</ta>
            <ta e="T26" id="Seg_6241" s="T24">What to do?</ta>
            <ta e="T30" id="Seg_6242" s="T26">They were raising ten Selkup children.</ta>
            <ta e="T34" id="Seg_6243" s="T30">He [the Hare Parka old man] is sending one of the Nenets men [to the Nenets people].</ta>
            <ta e="T41" id="Seg_6244" s="T34">[They say,] like, "We’ll come to the Hare Parka old man tomorrow."</ta>
            <ta e="T44" id="Seg_6245" s="T41">“Go there for conversation.”</ta>
            <ta e="T53" id="Seg_6246" s="T44">[Two] young men, having come home, said: “Two [Selkup] orphan children are held by [two?] Nenets.”</ta>
            <ta e="T58" id="Seg_6247" s="T53">Then where can two old men go?</ta>
            <ta e="T70" id="Seg_6248" s="T58">The old man, this Hare Parka old man, said to the daughters-in-law: “Bring lard, bring food.</ta>
            <ta e="T74" id="Seg_6249" s="T70">Bring everything there is.</ta>
            <ta e="T78" id="Seg_6250" s="T74">The Nenets will come tomorrow, they’ll eat.”</ta>
            <ta e="T81" id="Seg_6251" s="T78">The old man said so.</ta>
            <ta e="T83" id="Seg_6252" s="T81">They brought food.</ta>
            <ta e="T89" id="Seg_6253" s="T83">Inside this house they covered the floor with new hay. </ta>
            <ta e="T92" id="Seg_6254" s="T89">The old man started giving food to the Nenets.</ta>
            <ta e="T93" id="Seg_6255" s="T92">Ouch!</ta>
            <ta e="T99" id="Seg_6256" s="T93">The next day the Nenets gathered together.</ta>
            <ta e="T102" id="Seg_6257" s="T99">An army of thirty people.</ta>
            <ta e="T110" id="Seg_6258" s="T102">[They look], in the empty tent the Hare Parka old man is [sitting] together with the daughters-in-law.</ta>
            <ta e="T114" id="Seg_6259" s="T110">They lay down fresh hay. </ta>
            <ta e="T117" id="Seg_6260" s="T114">The old man told [to the daughters-in-law to do] so. </ta>
            <ta e="T118" id="Seg_6261" s="T117">They started to eat.</ta>
            <ta e="T120" id="Seg_6262" s="T118">They found food.</ta>
            <ta e="T123" id="Seg_6263" s="T120">The old man told [the daughters-in-law to find it]. </ta>
            <ta e="T126" id="Seg_6264" s="T123">He also had a wife [an old woman]. </ta>
            <ta e="T129" id="Seg_6265" s="T126">[The old man] told the daughters-in-law to go.</ta>
            <ta e="T131" id="Seg_6266" s="T129">“Go out into the street.</ta>
            <ta e="T134" id="Seg_6267" s="T131">Take the children into the street.”</ta>
            <ta e="T139" id="Seg_6268" s="T134">The old man is hanging on, giving the people to eat.</ta>
            <ta e="T145" id="Seg_6269" s="T139">One morning men arrived.</ta>
            <ta e="T146" id="Seg_6270" s="T145">They eat.</ta>
            <ta e="T153" id="Seg_6271" s="T146">He is putting big logs into the stove.</ta>
            <ta e="T154" id="Seg_6272" s="T153">[the fire] is burning.</ta>
            <ta e="T160" id="Seg_6273" s="T154">He sent the daughters-in-law outside for no reason.</ta>
            <ta e="T163" id="Seg_6274" s="T160">They lit a fire outdoors.</ta>
            <ta e="T168" id="Seg_6275" s="T163">They lit a fire, and from the fire they lit birchbark torches.</ta>
            <ta e="T171" id="Seg_6276" s="T168">This way they used to dry something.</ta>
            <ta e="T177" id="Seg_6277" s="T171">“You’ll be here, I’ll go out.”</ta>
            <ta e="T178" id="Seg_6278" s="T177">[The Nenets] are eating.</ta>
            <ta e="T187" id="Seg_6279" s="T178">The Nenets are saying: “Grandfather, why are you putting so much wood into the stove?</ta>
            <ta e="T193" id="Seg_6280" s="T187">Won’t the fire burn the stove?”</ta>
            <ta e="T196" id="Seg_6281" s="T193">“It won’t.”</ta>
            <ta e="T202" id="Seg_6282" s="T196">That is how the Hare Parka old man in rags made them be there.</ta>
            <ta e="T209" id="Seg_6283" s="T202">He is running [around] by the stove next to the big door.</ta>
            <ta e="T218" id="Seg_6284" s="T209">Once a burning log fell into the house from the outside.</ta>
            <ta e="T220" id="Seg_6285" s="T218">[The Hare Parka old man] rushed [away].</ta>
            <ta e="T231" id="Seg_6286" s="T220">The Nenets men almost caught the old man by the door, all they grabbed was a piece of the hare parka.</ta>
            <ta e="T237" id="Seg_6287" s="T231">It looks like he went out through the window of the house.</ta>
            <ta e="T240" id="Seg_6288" s="T237">There were two windows.</ta>
            <ta e="T244" id="Seg_6289" s="T240">He had a big house!</ta>
            <ta e="T253" id="Seg_6290" s="T244">He covered it with fire, put up wooden poles, [sticking them into the ground] a big axe handle deep.</ta>
            <ta e="T258" id="Seg_6291" s="T253">Those [daughters-in-law] were throwing fire into the house.</ta>
            <ta e="T264" id="Seg_6292" s="T258">Earlier they had thrown gunpowder under the hay.</ta>
            <ta e="T267" id="Seg_6293" s="T264">The fire [gunpowder] blew up suddenly!</ta>
            <ta e="T273" id="Seg_6294" s="T267">The army of those thirty men burned to death in the fire.</ta>
            <ta e="T276" id="Seg_6295" s="T273">The daughters-in-law set out from upstream.</ta>
            <ta e="T282" id="Seg_6296" s="T276">The old man, having run outside, held an axe in his hands.</ta>
            <ta e="T289" id="Seg_6297" s="T282">Everyone who had hardly made an appearance outdoors, they hit [those] in the face.</ta>
            <ta e="T293" id="Seg_6298" s="T289">The daughters-in-law were throwing fire.</ta>
            <ta e="T303" id="Seg_6299" s="T293">They were throwing it down the chimney and through the two windows into the house.</ta>
            <ta e="T306" id="Seg_6300" s="T303">They burned the house down.</ta>
            <ta e="T313" id="Seg_6301" s="T306">This Nenets army had two leaders.</ta>
            <ta e="T326" id="Seg_6302" s="T313">Then [the old man] went to the shore, to the sleds of the Nenets people, he went perhaps to the shore to the sleds and took two daughters-in-law with him.</ta>
            <ta e="T331" id="Seg_6303" s="T326">He took two daughters-in-law to the [killed?] men.</ta>
            <ta e="T336" id="Seg_6304" s="T331">As he was coming from the shore, a lame old Nenets man appeared.</ta>
            <ta e="T342" id="Seg_6305" s="T336">The Hare Parka old man left the women [behind].</ta>
            <ta e="T345" id="Seg_6306" s="T342">The old man hit [the Nenets man] in the face.</ta>
            <ta e="T348" id="Seg_6307" s="T345">Then he went to the shore.</ta>
            <ta e="T358" id="Seg_6308" s="T348">Three daughters-in-law traveled downstream, they set out that way, they went downstream to the tents of the Nenets people.</ta>
            <ta e="T363" id="Seg_6309" s="T358">To the tents of the Nenets people the women went downstream.</ta>
            <ta e="T369" id="Seg_6310" s="T363">They [the Nenets] had left two Selkup children in the tent.</ta>
            <ta e="T374" id="Seg_6311" s="T369">Earlier – two grandsons of the Hare Parka old man.</ta>
            <ta e="T380" id="Seg_6312" s="T374">They gathered the two children and the reindeer and went up [to the forest]. </ta>
            <ta e="T382" id="Seg_6313" s="T380">Up [in the forest] they set up the tent.</ta>
            <ta e="T385" id="Seg_6314" s="T382">They spent one night there.</ta>
            <ta e="T390" id="Seg_6315" s="T385">The sons came home that day.</ta>
            <ta e="T395" id="Seg_6316" s="T390">[Catching up with?] the old man, the sons arrived.</ta>
            <ta e="T401" id="Seg_6317" s="T395">They thought: “Our father was killed.”</ta>
            <ta e="T407" id="Seg_6318" s="T401">He [the old man] had seven sons and a young daughter-in-law.</ta>
            <ta e="T410" id="Seg_6319" s="T407">[Catching up?], they gathered [together]. </ta>
            <ta e="T414" id="Seg_6320" s="T410">They tied up the son’s dog outdoors.</ta>
            <ta e="T420" id="Seg_6321" s="T414">Waiting for the reindeer, they [the sons] stepped on the dog’s foot.</ta>
            <ta e="T427" id="Seg_6322" s="T420">The old man asked: “Who stepped on the dog’s paw?”</ta>
            <ta e="T432" id="Seg_6323" s="T427">The youngest son’s wife ran out into the street.</ta>
            <ta e="T436" id="Seg_6324" s="T432">Meaning, her husband arrived.</ta>
            <ta e="T440" id="Seg_6325" s="T436">Meaning, his son arrived.</ta>
            <ta e="T448" id="Seg_6326" s="T440">Then the grandfather told this story: “We burned down [the house] ourselves. </ta>
            <ta e="T453" id="Seg_6327" s="T448">We drove the reindeer forward, to Nenets lands."</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_6328" s="T0">Der alte Mann "Hasenmantel".</ta>
            <ta e="T9" id="Seg_6329" s="T3">[Der alte Mann] hatte sieben Söhne und sieben Schwiegertöchter.</ta>
            <ta e="T16" id="Seg_6330" s="T9">Nenzen kamen von weiter flussabwärts, um ihn zu töten.</ta>
            <ta e="T21" id="Seg_6331" s="T16">Seine Söhne gingen in den Wald, um Eichhörnchen zu jagen.</ta>
            <ta e="T24" id="Seg_6332" s="T21">Sie werden diesen Mann jetzt töten.</ta>
            <ta e="T26" id="Seg_6333" s="T24">Was soll man machen?</ta>
            <ta e="T30" id="Seg_6334" s="T26">Sie zogen zehn selkupische Kinder auf.</ta>
            <ta e="T34" id="Seg_6335" s="T30">Er [der alte Mann Hasenmantel] schickt einen der Nenzen [zu den Nenzen].</ta>
            <ta e="T41" id="Seg_6336" s="T34">"Wir kommen morgen zum alten Mann Hasenmantel."</ta>
            <ta e="T44" id="Seg_6337" s="T41">"Geht hin, um euch zu unterhalten."</ta>
            <ta e="T53" id="Seg_6338" s="T44">[Zwei] junge Männer kamen nach Hause und sagten: "Zwei [selkupische] Waisenkinder werden von [zwei?] Nenzen gehalten."</ta>
            <ta e="T58" id="Seg_6339" s="T53">Wo sollen die zwei alten Männer dann hin?</ta>
            <ta e="T70" id="Seg_6340" s="T58">Der alte Mann, dieser alte Mann Hasenmantel, sagte zu den Schwiegertöchtern: "Bringt Fett, bringt Essen.</ta>
            <ta e="T74" id="Seg_6341" s="T70">Bringt alles, was da ist.</ta>
            <ta e="T78" id="Seg_6342" s="T74">Die Nenzen kommen morgen, sie werden essen."</ta>
            <ta e="T81" id="Seg_6343" s="T78">So sagte der alte Mann.</ta>
            <ta e="T83" id="Seg_6344" s="T81">Sie brachten das Essen.</ta>
            <ta e="T89" id="Seg_6345" s="T83">In diesem Haus bedeckten sie den Boden mit neuem Heu.</ta>
            <ta e="T92" id="Seg_6346" s="T89">Der alte Mann fing an, den Nenzen Essen zu geben.</ta>
            <ta e="T93" id="Seg_6347" s="T92">Oh!</ta>
            <ta e="T99" id="Seg_6348" s="T93">Am nächsten Tag versammelten sich die Nenzen.</ta>
            <ta e="T102" id="Seg_6349" s="T99">Ein Heer von dreißig Leuten.</ta>
            <ta e="T110" id="Seg_6350" s="T102">[Sie schauen], im leeren Zelt [sitzt] der alte Mann Hasenmantel zusammen mit den Schwiegertöchtern.</ta>
            <ta e="T114" id="Seg_6351" s="T110">Sie legten frisches Heu hin.</ta>
            <ta e="T117" id="Seg_6352" s="T114">Der alte Mann sagte [das den Schwiegertöchtern].</ta>
            <ta e="T118" id="Seg_6353" s="T117">Sie fingen an zu essen.</ta>
            <ta e="T120" id="Seg_6354" s="T118">Sie fanden Essen.</ta>
            <ta e="T123" id="Seg_6355" s="T120">Der alte Mann sagte [es den Schwiegertöchtern].</ta>
            <ta e="T126" id="Seg_6356" s="T123">Er hatte auch eine Frau [eine alte Frau].</ta>
            <ta e="T129" id="Seg_6357" s="T126">[Der alte Mann] hieß die Schwiegertöchter zu gehen.</ta>
            <ta e="T131" id="Seg_6358" s="T129">"Geht nach draußen.</ta>
            <ta e="T134" id="Seg_6359" s="T131">Nehmt die Kinder mit nach draußen."</ta>
            <ta e="T139" id="Seg_6360" s="T134">Der alte Mann hält es aus, gibt den Leuten zu essen.</ta>
            <ta e="T145" id="Seg_6361" s="T139">An einem Morgen kamen Leute.</ta>
            <ta e="T146" id="Seg_6362" s="T145">Sie essen.</ta>
            <ta e="T153" id="Seg_6363" s="T146">Er legt große Holzscheite in den Ofen.</ta>
            <ta e="T154" id="Seg_6364" s="T153">Es [das Feuer] brennt.</ta>
            <ta e="T160" id="Seg_6365" s="T154">Er schickte die Schwiegertöchter einfach so hinaus.</ta>
            <ta e="T163" id="Seg_6366" s="T160">Sie entzündeten draußen ein Feuer.</ta>
            <ta e="T168" id="Seg_6367" s="T163">Sie entzündeten ein Feuer, und mit dem Feuer entzündeten sie Birkenrindenfackeln.</ta>
            <ta e="T171" id="Seg_6368" s="T168">So trockneten sie irgendetwas.</ta>
            <ta e="T177" id="Seg_6369" s="T171">"Ihr werdet hier sein, ich gehe hinaus."</ta>
            <ta e="T178" id="Seg_6370" s="T177">[Die Nenzen] essen.</ta>
            <ta e="T187" id="Seg_6371" s="T178">Die Nenzen sagen: "Großvater, warum legst du so viel Holz in den Ofen?</ta>
            <ta e="T193" id="Seg_6372" s="T187">Wird das Feuer den Ofen nicht verbrennen?"</ta>
            <ta e="T196" id="Seg_6373" s="T193">"Wird es nicht."</ta>
            <ta e="T202" id="Seg_6374" s="T196">So ließ der alte Mann Hasenmantel sie in Lumpen [dort] sein.</ta>
            <ta e="T209" id="Seg_6375" s="T202">Er läuft am Ofen neben dem großen Eingang umher.</ta>
            <ta e="T218" id="Seg_6376" s="T209">Einmal fiel von draußen ein brennender Scheit ins Haus.</ta>
            <ta e="T220" id="Seg_6377" s="T218">[Der alte Mann Hasenmantel] stürmte davon.</ta>
            <ta e="T231" id="Seg_6378" s="T220">Die Nenzen griffen den alten Mann fast an der Tür, sie fingen nur ein Stück des Hasenmantels.</ta>
            <ta e="T237" id="Seg_6379" s="T231">Er scheint durch das Fenster hinausgegangen zu sein.</ta>
            <ta e="T240" id="Seg_6380" s="T237">Es gab zwei Fenster.</ta>
            <ta e="T244" id="Seg_6381" s="T240">Er hatte ein großes Haus!</ta>
            <ta e="T253" id="Seg_6382" s="T244">Er bedeckte es mit Feuer, legte Holzbretter darauf, [bis zur] Höhe des Schaftes einer Axt bedeckte er es.</ta>
            <ta e="T258" id="Seg_6383" s="T253">Jene [Schwiegertöchter] warfen Feuer ins Haus.</ta>
            <ta e="T264" id="Seg_6384" s="T258">Vorher hatten sie Schießpulver unter das Heu gestreut.</ta>
            <ta e="T267" id="Seg_6385" s="T264">Das Feuer [Schießpulver] explodierte plötzlich!</ta>
            <ta e="T273" id="Seg_6386" s="T267">Die Armee aus dreißig Leuten verbrannte im Feuer.</ta>
            <ta e="T276" id="Seg_6387" s="T273">Die Schwiegertöchter machten sich auf den Weg flussabwärts.</ta>
            <ta e="T282" id="Seg_6388" s="T276">Der alte Mann lief hinaus und hielt eine Axt in den Händen.</ta>
            <ta e="T289" id="Seg_6389" s="T282">Jedem, der auch nur nach draußen kam, schlugen sie ins Gesicht.</ta>
            <ta e="T293" id="Seg_6390" s="T289">Die Schwiegertöchter warfen Feuer.</ta>
            <ta e="T303" id="Seg_6391" s="T293">Sie warfen es durch den Schornstein und durch die beiden Fenster ins Haus.</ta>
            <ta e="T306" id="Seg_6392" s="T303">Sie brannten das Haus nieder.</ta>
            <ta e="T313" id="Seg_6393" s="T306">Diese Nenzenarmee hatte zwei Anführer.</ta>
            <ta e="T326" id="Seg_6394" s="T313">Dann ging [der alte Mann] zum Ufer, zu den Schlitten der Nenzen, er ging wohl am Ufer zu den Schlitten und nahm sich zwei Schwiegertöchter mit.</ta>
            <ta e="T331" id="Seg_6395" s="T326">Er nahm zwei Schwiegertöchter mit zu den [getöten?] Leuten.</ta>
            <ta e="T336" id="Seg_6396" s="T331">Er kam vom Ufer, da erschein ein lahmer alter Nenze.</ta>
            <ta e="T342" id="Seg_6397" s="T336">Der alte Mann Hasenmantel ließ die Frauen [zurück].</ta>
            <ta e="T345" id="Seg_6398" s="T342">Der alte Mann schlug [dem Nenzen] ins Gesicht.</ta>
            <ta e="T348" id="Seg_6399" s="T345">Dann ging er zum Ufer.</ta>
            <ta e="T358" id="Seg_6400" s="T348">Drei Schwiegertöchter fuhren flussabwärts, sie machten sich auf den Weg dorthin, sie fuhren flussabwärts zum Zelt der Nenzen.</ta>
            <ta e="T363" id="Seg_6401" s="T358">Zum Zelt der Nenzen fuhren die Frauen flussabwärts.</ta>
            <ta e="T369" id="Seg_6402" s="T363">Sie [die Nenzen] hatten zwei selkupische Kinder im Zelt gelassen.</ta>
            <ta e="T374" id="Seg_6403" s="T369">Vorher – zwei Enkel des alten Mannes Hasenmantel.</ta>
            <ta e="T380" id="Seg_6404" s="T374">Sie sammelten die beiden Kinder und Rentiere ein und fuhren nach oben [in den Wald].</ta>
            <ta e="T382" id="Seg_6405" s="T380">Oben [im Wald] stellten sie das Zelt auf.</ta>
            <ta e="T385" id="Seg_6406" s="T382">Sie verbrachten dort eine Nacht.</ta>
            <ta e="T390" id="Seg_6407" s="T385">Die Söhne kamen an diesem Tag nach Hause.</ta>
            <ta e="T395" id="Seg_6408" s="T390">Die Söhne [erreichten?] den alten Mann und kamen.</ta>
            <ta e="T401" id="Seg_6409" s="T395">Sie dachten: "Unser Vater wurde getötet."</ta>
            <ta e="T407" id="Seg_6410" s="T401">Er [der alte Mann] hatte sieben Söhne und die jüngste Schwiegertochter.</ta>
            <ta e="T410" id="Seg_6411" s="T407">Sie [erreichten ihn?] und versammelten sich.</ta>
            <ta e="T414" id="Seg_6412" s="T410">Sie banden den Hund des Sohnes draußen fest.</ta>
            <ta e="T420" id="Seg_6413" s="T414">Als sie auf die Rentiere warteten, traten sie [die Söhne] dem Hund auf die Pfote.</ta>
            <ta e="T427" id="Seg_6414" s="T420">Der alte Mann fragte: "Wer ist meinem Hund auf die Pfote getreten?"</ta>
            <ta e="T432" id="Seg_6415" s="T427">Die Frau des jüngsten Sohnes lief nach draußen.</ta>
            <ta e="T436" id="Seg_6416" s="T432">Das heißt, ihr Mann kam an.</ta>
            <ta e="T440" id="Seg_6417" s="T436">Das heißt, ihr Sohn kam an.</ta>
            <ta e="T448" id="Seg_6418" s="T440">Dann erzählte der Großvater: "Wir selbst haben [das Haus] niedergebrannt.</ta>
            <ta e="T453" id="Seg_6419" s="T448">Wir trieben die Rentiere weiter, ins Land der Nenzen."</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_6420" s="T0">заячья шкура [парка] старика</ta>
            <ta e="T9" id="Seg_6421" s="T3">семь детей было и 7 невесток [снох]</ta>
            <ta e="T16" id="Seg_6422" s="T9">ненцы приехали снизу в верховье их убить</ta>
            <ta e="T21" id="Seg_6423" s="T16">люди сыновей в лес уехали белковать</ta>
            <ta e="T24" id="Seg_6424" s="T21">человек сейчас убьет</ta>
            <ta e="T26" id="Seg_6425" s="T24">как быть </ta>
            <ta e="T30" id="Seg_6426" s="T26">10 селькупских детей держали (воспитывали)</ta>
            <ta e="T34" id="Seg_6427" s="T30">этого ненца этого отправили</ta>
            <ta e="T41" id="Seg_6428" s="T34">заячье пальто старику мы завтра приедем</ta>
            <ta e="T44" id="Seg_6429" s="T41">съездите для разговоров [что они скажут]</ta>
            <ta e="T53" id="Seg_6430" s="T44">парень домой приехав так сказал сироты ребятишки (2их) ненцы воспитывают</ta>
            <ta e="T58" id="Seg_6431" s="T53">потом два старика куда деваются</ta>
            <ta e="T70" id="Seg_6432" s="T58">старик так сказал этот старик в заячьей парке невесткам [снохам] жир еду принесите</ta>
            <ta e="T74" id="Seg_6433" s="T70">все что есть (все что угодно) принесите</ta>
            <ta e="T78" id="Seg_6434" s="T74">ненцы завтра приедут кушать будут</ta>
            <ta e="T81" id="Seg_6435" s="T78">старик так сказал</ta>
            <ta e="T83" id="Seg_6436" s="T81">еду принесли</ta>
            <ta e="T89" id="Seg_6437" s="T83">этот дом внутри новым сеном покройте (постелили)</ta>
            <ta e="T92" id="Seg_6438" s="T89">старик ненцев кормить начал</ta>
            <ta e="T93" id="Seg_6439" s="T92">о </ta>
            <ta e="T99" id="Seg_6440" s="T93">на след. день ненцы только что приехали</ta>
            <ta e="T102" id="Seg_6441" s="T99">30 людей военные </ta>
            <ta e="T114" id="Seg_6442" s="T110">новым сеном постелил</ta>
            <ta e="T117" id="Seg_6443" s="T114">старик так был</ta>
            <ta e="T118" id="Seg_6444" s="T117">есть начали</ta>
            <ta e="T120" id="Seg_6445" s="T118">еду [нашли, найдут?]</ta>
            <ta e="T123" id="Seg_6446" s="T120">старик так сказал</ta>
            <ta e="T126" id="Seg_6447" s="T123">старуха тоже была [есть]</ta>
            <ta e="T129" id="Seg_6448" s="T126">снохам так сказал</ta>
            <ta e="T131" id="Seg_6449" s="T129">на улицу идите</ta>
            <ta e="T134" id="Seg_6450" s="T131">сыновей на улицу унесите</ta>
            <ta e="T139" id="Seg_6451" s="T134">он сам ходит людей кормит</ta>
            <ta e="T145" id="Seg_6452" s="T139">в одно время [однажды] приехали утром люди </ta>
            <ta e="T146" id="Seg_6453" s="T145">кушают</ta>
            <ta e="T153" id="Seg_6454" s="T146">печка огонь эту большими бревнами [поленами] подложил </ta>
            <ta e="T154" id="Seg_6455" s="T153">затопил</ta>
            <ta e="T160" id="Seg_6456" s="T154">снохи ни за что на улицу отправил</ta>
            <ta e="T163" id="Seg_6457" s="T160">на улице костер зажигают</ta>
            <ta e="T168" id="Seg_6458" s="T163">огонь зажгли этим факела жгут [бересту зажгли]</ta>
            <ta e="T171" id="Seg_6459" s="T168">зачем это сушат</ta>
            <ta e="T177" id="Seg_6460" s="T171">вы там будете я выйду</ta>
            <ta e="T178" id="Seg_6461" s="T177">едят</ta>
            <ta e="T187" id="Seg_6462" s="T178">ненцы так сказали дедушка печку зачем так дровами кладешь</ta>
            <ta e="T193" id="Seg_6463" s="T187">не сгорит ли печка</ta>
            <ta e="T196" id="Seg_6464" s="T193">не сгорит</ta>
            <ta e="T202" id="Seg_6465" s="T196">так быстро бегает заячьей парке лоскутья</ta>
            <ta e="T209" id="Seg_6466" s="T202">носит рядом с печкой возле двери</ta>
            <ta e="T218" id="Seg_6467" s="T209">однажды с улицы домой огненный факел [головешка] упал</ta>
            <ta e="T220" id="Seg_6468" s="T218">хотел сбежать</ta>
            <ta e="T231" id="Seg_6469" s="T220">в дверях ненцы чуть было не поймали только его парку поймали (схватили)</ta>
            <ta e="T237" id="Seg_6470" s="T231">правда он подошёл (огнём) к окошку</ta>
            <ta e="T240" id="Seg_6471" s="T237">два окошка было</ta>
            <ta e="T253" id="Seg_6472" s="T244">огнем закрыл, поставил деревянные жерди, на глубину топорища зарыл.</ta>
            <ta e="T258" id="Seg_6473" s="T253">они огонь бросали, в дом</ta>
            <ta e="T264" id="Seg_6474" s="T258">порохом раньше насыпали под сено</ta>
            <ta e="T267" id="Seg_6475" s="T264">порох (огонь) взорвался</ta>
            <ta e="T273" id="Seg_6476" s="T267">все тридцать человек сгорели (=огонь съел)</ta>
            <ta e="T276" id="Seg_6477" s="T273">невестки сверху пришли</ta>
            <ta e="T282" id="Seg_6478" s="T276">старик выскочил со скоростью ветра</ta>
            <ta e="T289" id="Seg_6479" s="T282">пытались выскочить на улицу, в лицо били</ta>
            <ta e="T293" id="Seg_6480" s="T289">невестки бросали огонь</ta>
            <ta e="T303" id="Seg_6481" s="T293">внутрь печки сверху вниз, в два окошка в дом бросали</ta>
            <ta e="T306" id="Seg_6482" s="T303">дом сгорел</ta>
            <ta e="T313" id="Seg_6483" s="T306">у ненецких воинов было два главаря</ta>
            <ta e="T326" id="Seg_6484" s="T313">потом на берег пошёл, к ненецким упряжкам, на берег к упряжкам пошёл и две невестки поехали</ta>
            <ta e="T331" id="Seg_6485" s="T326">две невестки поехали, к убитым людям</ta>
            <ta e="T336" id="Seg_6486" s="T331">с берега шёл хромой старик-ненец</ta>
            <ta e="T342" id="Seg_6487" s="T336">старик в заячьей парке привязал сзади нарты невестки</ta>
            <ta e="T345" id="Seg_6488" s="T342">старик крикнул</ta>
            <ta e="T348" id="Seg_6489" s="T345">потом на берег пошёл</ta>
            <ta e="T358" id="Seg_6490" s="T348">три невестки вниз поехали, туда поехали. в ненецкий чум вниз поехали</ta>
            <ta e="T363" id="Seg_6491" s="T358">в ненецкий чум женщины вниз поехали</ta>
            <ta e="T369" id="Seg_6492" s="T363">туда в доме селькупы двух детей завязали в упряжку</ta>
            <ta e="T374" id="Seg_6493" s="T369">наверное (?) два внука старика в заячьей парки</ta>
            <ta e="T380" id="Seg_6494" s="T374">они оленей посчитали и поехали в лес (наверх)</ta>
            <ta e="T382" id="Seg_6495" s="T380">в лесу (наверху) поставили чум</ta>
            <ta e="T385" id="Seg_6496" s="T382">одну ночь ночевали</ta>
            <ta e="T390" id="Seg_6497" s="T385">дети после охоты на белку домой приехали</ta>
            <ta e="T395" id="Seg_6498" s="T390">крадучись за отцом-стариком доехали дети</ta>
            <ta e="T401" id="Seg_6499" s="T395">они так думали. моего отца убили</ta>
            <ta e="T407" id="Seg_6500" s="T401">у него семь детей было младшая невестка</ta>
            <ta e="T414" id="Seg_6501" s="T410">собаку детей на улице привязали</ta>
            <ta e="T420" id="Seg_6502" s="T414">олени во дворе они ногой наступили на собаку</ta>
            <ta e="T427" id="Seg_6503" s="T420">старик так сказал. на лапу собаки кто наступили</ta>
            <ta e="T432" id="Seg_6504" s="T427">младшего сына невестка на улицу выбежала</ta>
            <ta e="T436" id="Seg_6505" s="T432">это её муж приехал</ta>
            <ta e="T440" id="Seg_6506" s="T436">это его сын приехал</ta>
            <ta e="T448" id="Seg_6507" s="T440">потом старик так сказал, дом наш огнём сожгли</ta>
            <ta e="T453" id="Seg_6508" s="T448">в ненецкую землю оленей вперёд перегнали</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T3" id="Seg_6509" s="T0">[OSV:] "Nomalʼ porqɨ" - the nickname of an old man in Selkup fairytales.</ta>
            <ta e="T16" id="Seg_6510" s="T9">‎[OSV:] The speaker alternates between Present and Past Tenses.</ta>
            <ta e="T41" id="Seg_6511" s="T34">[OSV:] This news was brought to "Hare Parka" from the Nenets by the sent men.</ta>
            <ta e="T99" id="Seg_6512" s="T93">[OSV:] "jap tattɨqo" - "to meet together".</ta>
            <ta e="T110" id="Seg_6513" s="T102">[OSV:] "šünʼčʼipɨlʼ" - "empty".</ta>
            <ta e="T303" id="Seg_6514" s="T293">[OSV:] 1) The verbal form "qəː-ŋɔː-tɨt" (throw-PRS-3PL) would be more correct here; 2) the nominal form "oši" has been edited into "ɔːk-tɨ".</ta>
            <ta e="T358" id="Seg_6515" s="T348">[OSV:] The verbal form "laqal-tɛlʼčʼɔː-tɨt" (start.for-PFV-3PL) would be more correct here.</ta>
            <ta e="T374" id="Seg_6516" s="T369"> ‎‎[OSV:] The adverb "tomoroq" has been edited into "tomorot".</ta>
            <ta e="T401" id="Seg_6517" s="T395">[OSV:] The verbal form "tämirpotät" has been edited into "tɛnirpɔːtät".</ta>
            <ta e="T410" id="Seg_6518" s="T407">[OSV:] "jap tattɨqo" - "to gather together", here the particle "jap" is omitted.</ta>
            <ta e="T453" id="Seg_6519" s="T448">[OSV:] The verbal form "tɔːqqɨ-ŋɨ-tɨt" (shepherd-PRS-3PL) would be more correct here.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T454" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
