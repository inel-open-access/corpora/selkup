<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>NEP_1965_NenetsAndWhiteBear1_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">NEP_1965_NenetsAndWhiteBear1_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">656</ud-information>
            <ud-information attribute-name="# HIAT:w">522</ud-information>
            <ud-information attribute-name="# e">523</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">1</ud-information>
            <ud-information attribute-name="# HIAT:u">95</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="NEP">
            <abbreviation>NEP</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T96" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T519" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T520" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
         <tli id="T204" />
         <tli id="T205" />
         <tli id="T206" />
         <tli id="T207" />
         <tli id="T208" />
         <tli id="T209" />
         <tli id="T210" />
         <tli id="T211" />
         <tli id="T212" />
         <tli id="T213" />
         <tli id="T214" />
         <tli id="T215" />
         <tli id="T216" />
         <tli id="T217" />
         <tli id="T218" />
         <tli id="T219" />
         <tli id="T220" />
         <tli id="T221" />
         <tli id="T222" />
         <tli id="T223" />
         <tli id="T224" />
         <tli id="T225" />
         <tli id="T226" />
         <tli id="T227" />
         <tli id="T228" />
         <tli id="T229" />
         <tli id="T230" />
         <tli id="T231" />
         <tli id="T232" />
         <tli id="T233" />
         <tli id="T234" />
         <tli id="T235" />
         <tli id="T236" />
         <tli id="T237" />
         <tli id="T238" />
         <tli id="T239" />
         <tli id="T240" />
         <tli id="T241" />
         <tli id="T242" />
         <tli id="T243" />
         <tli id="T244" />
         <tli id="T245" />
         <tli id="T246" />
         <tli id="T247" />
         <tli id="T248" />
         <tli id="T249" />
         <tli id="T250" />
         <tli id="T251" />
         <tli id="T252" />
         <tli id="T253" />
         <tli id="T254" />
         <tli id="T255" />
         <tli id="T256" />
         <tli id="T257" />
         <tli id="T258" />
         <tli id="T259" />
         <tli id="T260" />
         <tli id="T261" />
         <tli id="T262" />
         <tli id="T263" />
         <tli id="T264" />
         <tli id="T265" />
         <tli id="T266" />
         <tli id="T267" />
         <tli id="T268" />
         <tli id="T269" />
         <tli id="T270" />
         <tli id="T271" />
         <tli id="T272" />
         <tli id="T273" />
         <tli id="T274" />
         <tli id="T275" />
         <tli id="T276" />
         <tli id="T277" />
         <tli id="T278" />
         <tli id="T279" />
         <tli id="T280" />
         <tli id="T281" />
         <tli id="T282" />
         <tli id="T283" />
         <tli id="T284" />
         <tli id="T285" />
         <tli id="T286" />
         <tli id="T287" />
         <tli id="T288" />
         <tli id="T289" />
         <tli id="T290" />
         <tli id="T291" />
         <tli id="T292" />
         <tli id="T293" />
         <tli id="T294" />
         <tli id="T295" />
         <tli id="T296" />
         <tli id="T297" />
         <tli id="T298" />
         <tli id="T299" />
         <tli id="T300" />
         <tli id="T301" />
         <tli id="T302" />
         <tli id="T303" />
         <tli id="T304" />
         <tli id="T305" />
         <tli id="T306" />
         <tli id="T307" />
         <tli id="T308" />
         <tli id="T309" />
         <tli id="T310" />
         <tli id="T311" />
         <tli id="T312" />
         <tli id="T313" />
         <tli id="T314" />
         <tli id="T315" />
         <tli id="T316" />
         <tli id="T317" />
         <tli id="T318" />
         <tli id="T319" />
         <tli id="T320" />
         <tli id="T321" />
         <tli id="T322" />
         <tli id="T323" />
         <tli id="T324" />
         <tli id="T325" />
         <tli id="T326" />
         <tli id="T327" />
         <tli id="T328" />
         <tli id="T329" />
         <tli id="T330" />
         <tli id="T331" />
         <tli id="T332" />
         <tli id="T333" />
         <tli id="T334" />
         <tli id="T335" />
         <tli id="T336" />
         <tli id="T337" />
         <tli id="T338" />
         <tli id="T339" />
         <tli id="T340" />
         <tli id="T341" />
         <tli id="T342" />
         <tli id="T343" />
         <tli id="T521" />
         <tli id="T344" />
         <tli id="T345" />
         <tli id="T346" />
         <tli id="T347" />
         <tli id="T348" />
         <tli id="T349" />
         <tli id="T350" />
         <tli id="T351" />
         <tli id="T352" />
         <tli id="T353" />
         <tli id="T354" />
         <tli id="T355" />
         <tli id="T356" />
         <tli id="T357" />
         <tli id="T358" />
         <tli id="T359" />
         <tli id="T360" />
         <tli id="T361" />
         <tli id="T362" />
         <tli id="T363" />
         <tli id="T364" />
         <tli id="T365" />
         <tli id="T366" />
         <tli id="T367" />
         <tli id="T368" />
         <tli id="T369" />
         <tli id="T370" />
         <tli id="T371" />
         <tli id="T372" />
         <tli id="T373" />
         <tli id="T374" />
         <tli id="T375" />
         <tli id="T376" />
         <tli id="T377" />
         <tli id="T378" />
         <tli id="T379" />
         <tli id="T380" />
         <tli id="T381" />
         <tli id="T382" />
         <tli id="T383" />
         <tli id="T384" />
         <tli id="T385" />
         <tli id="T386" />
         <tli id="T387" />
         <tli id="T388" />
         <tli id="T389" />
         <tli id="T390" />
         <tli id="T391" />
         <tli id="T392" />
         <tli id="T393" />
         <tli id="T394" />
         <tli id="T395" />
         <tli id="T396" />
         <tli id="T397" />
         <tli id="T398" />
         <tli id="T399" />
         <tli id="T400" />
         <tli id="T401" />
         <tli id="T402" />
         <tli id="T403" />
         <tli id="T404" />
         <tli id="T405" />
         <tli id="T406" />
         <tli id="T407" />
         <tli id="T408" />
         <tli id="T409" />
         <tli id="T410" />
         <tli id="T411" />
         <tli id="T412" />
         <tli id="T413" />
         <tli id="T414" />
         <tli id="T415" />
         <tli id="T416" />
         <tli id="T417" />
         <tli id="T418" />
         <tli id="T419" />
         <tli id="T420" />
         <tli id="T421" />
         <tli id="T422" />
         <tli id="T423" />
         <tli id="T424" />
         <tli id="T425" />
         <tli id="T426" />
         <tli id="T427" />
         <tli id="T428" />
         <tli id="T429" />
         <tli id="T430" />
         <tli id="T431" />
         <tli id="T432" />
         <tli id="T433" />
         <tli id="T434" />
         <tli id="T435" />
         <tli id="T436" />
         <tli id="T437" />
         <tli id="T438" />
         <tli id="T439" />
         <tli id="T440" />
         <tli id="T441" />
         <tli id="T442" />
         <tli id="T443" />
         <tli id="T444" />
         <tli id="T445" />
         <tli id="T446" />
         <tli id="T447" />
         <tli id="T448" />
         <tli id="T449" />
         <tli id="T450" />
         <tli id="T451" />
         <tli id="T452" />
         <tli id="T453" />
         <tli id="T454" />
         <tli id="T455" />
         <tli id="T456" />
         <tli id="T522" />
         <tli id="T457" />
         <tli id="T458" />
         <tli id="T459" />
         <tli id="T460" />
         <tli id="T461" />
         <tli id="T462" />
         <tli id="T463" />
         <tli id="T464" />
         <tli id="T465" />
         <tli id="T466" />
         <tli id="T467" />
         <tli id="T468" />
         <tli id="T469" />
         <tli id="T470" />
         <tli id="T471" />
         <tli id="T472" />
         <tli id="T473" />
         <tli id="T474" />
         <tli id="T475" />
         <tli id="T476" />
         <tli id="T477" />
         <tli id="T478" />
         <tli id="T479" />
         <tli id="T523" />
         <tli id="T480" />
         <tli id="T481" />
         <tli id="T482" />
         <tli id="T483" />
         <tli id="T484" />
         <tli id="T485" />
         <tli id="T486" />
         <tli id="T487" />
         <tli id="T488" />
         <tli id="T489" />
         <tli id="T490" />
         <tli id="T491" />
         <tli id="T492" />
         <tli id="T493" />
         <tli id="T494" />
         <tli id="T495" />
         <tli id="T496" />
         <tli id="T497" />
         <tli id="T498" />
         <tli id="T499" />
         <tli id="T500" />
         <tli id="T501" />
         <tli id="T502" />
         <tli id="T503" />
         <tli id="T504" />
         <tli id="T505" />
         <tli id="T506" />
         <tli id="T507" />
         <tli id="T508" />
         <tli id="T509" />
         <tli id="T510" />
         <tli id="T511" />
         <tli id="T512" />
         <tli id="T513" />
         <tli id="T514" />
         <tli id="T515" />
         <tli id="T516" />
         <tli id="T517" />
         <tli id="T518" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="NEP"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T518" id="Seg_0" n="sc" s="T0">
               <ts e="T11" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Na</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_7" n="HIAT:w" s="T1">qäl</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_10" n="HIAT:w" s="T96">ira</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_13" n="HIAT:w" s="T2">ija</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">nılʼčʼik</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">kəttɨŋɨtɨ</ts>
                  <nts id="Seg_20" n="HIAT:ip">:</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_23" n="HIAT:w" s="T5">Mat</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">tap</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">moraqɨt</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">särɨ</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">qorqɨt</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">qəttat</ts>
                  <nts id="Seg_39" n="HIAT:ip">.</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_42" n="HIAT:u" s="T11">
                  <ts e="T519" id="Seg_44" n="HIAT:w" s="T11">Qal</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_47" n="HIAT:w" s="T519">ira</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_50" n="HIAT:w" s="T12">nılʼčʼik</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_53" n="HIAT:w" s="T13">kəttɨŋɨt</ts>
                  <nts id="Seg_54" n="HIAT:ip">:</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_57" n="HIAT:w" s="T14">Na</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_60" n="HIAT:w" s="T15">aša</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_63" n="HIAT:w" s="T16">qumɨt</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_66" n="HIAT:w" s="T17">qəssa</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_69" n="HIAT:w" s="T18">suːrup</ts>
                  <nts id="Seg_70" n="HIAT:ip">.</nts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_73" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_75" n="HIAT:w" s="T19">Tat</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_78" n="HIAT:w" s="T20">kutar</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_81" n="HIAT:w" s="T21">qənnantɨ</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_84" n="HIAT:w" s="T22">morat</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_87" n="HIAT:w" s="T23">tɔː</ts>
                  <nts id="Seg_88" n="HIAT:ip">.</nts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_91" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_93" n="HIAT:w" s="T24">Qapija</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_96" n="HIAT:w" s="T25">poːkɨtɨlʼ</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_99" n="HIAT:w" s="T26">tətaqɨt</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_102" n="HIAT:w" s="T27">ilʼitɨ</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_105" n="HIAT:w" s="T28">qälʼi</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_108" n="HIAT:w" s="T520">ira</ts>
                  <nts id="Seg_109" n="HIAT:ip">.</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_112" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_114" n="HIAT:w" s="T29">Täpɨt</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_117" n="HIAT:w" s="T30">nɔːkɨr</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_120" n="HIAT:w" s="T31">timnʼäsik</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_123" n="HIAT:w" s="T32">morat</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_126" n="HIAT:w" s="T33">təː</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_129" n="HIAT:w" s="T34">na</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_132" n="HIAT:w" s="T35">särɨ</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_135" n="HIAT:w" s="T36">qorqɨp</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_138" n="HIAT:w" s="T37">qätqa</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_141" n="HIAT:w" s="T38">qənnɔːtɨt</ts>
                  <nts id="Seg_142" n="HIAT:ip">.</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_145" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_147" n="HIAT:w" s="T39">Täpɨt</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_150" n="HIAT:w" s="T40">qännɔːtɨt</ts>
                  <nts id="Seg_151" n="HIAT:ip">.</nts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_154" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_156" n="HIAT:w" s="T41">Na</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_159" n="HIAT:w" s="T42">särɨ</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_162" n="HIAT:w" s="T43">qorqɨp</ts>
                  <nts id="Seg_163" n="HIAT:ip">,</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_166" n="HIAT:w" s="T44">əmɨntɨ</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_169" n="HIAT:w" s="T45">čʼatɨŋɨtɨ</ts>
                  <nts id="Seg_170" n="HIAT:ip">.</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_173" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_175" n="HIAT:w" s="T46">Nɨnɨ</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_178" n="HIAT:w" s="T47">sərɨ</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_181" n="HIAT:w" s="T48">qorqɨp</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_183" n="HIAT:ip">(</nts>
                  <nts id="Seg_184" n="HIAT:ip">(</nts>
                  <ats e="T50" id="Seg_185" n="HIAT:non-pho" s="T49">…</ats>
                  <nts id="Seg_186" n="HIAT:ip">)</nts>
                  <nts id="Seg_187" n="HIAT:ip">)</nts>
                  <nts id="Seg_188" n="HIAT:ip">,</nts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_191" n="HIAT:w" s="T50">na</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_194" n="HIAT:w" s="T51">särɨ</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_197" n="HIAT:w" s="T52">qorqɨ</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_200" n="HIAT:w" s="T53">mompa</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_203" n="HIAT:w" s="T54">märqɨ</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_206" n="HIAT:w" s="T55">ɛːŋa</ts>
                  <nts id="Seg_207" n="HIAT:ip">.</nts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_210" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_212" n="HIAT:w" s="T56">Nɨːnɨ</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_215" n="HIAT:w" s="T57">təpɨt</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_218" n="HIAT:w" s="T58">qəlla</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_221" n="HIAT:w" s="T59">nɔːnnɨntɨ</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_224" n="HIAT:w" s="T60">kɨmɨt</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_227" n="HIAT:w" s="T61">sitɨ</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_230" n="HIAT:w" s="T62">korɔːtɨt</ts>
                  <nts id="Seg_231" n="HIAT:ip">.</nts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_234" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_236" n="HIAT:w" s="T63">Aj</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_239" n="HIAT:w" s="T64">moqalmɨntɨ</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_242" n="HIAT:w" s="T65">sitɨ</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_245" n="HIAT:w" s="T66">korɔːt</ts>
                  <nts id="Seg_246" n="HIAT:ip">.</nts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_249" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_251" n="HIAT:w" s="T67">Nɨːnɨ</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_254" n="HIAT:w" s="T68">na</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_257" n="HIAT:w" s="T69">šitɨ</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_260" n="HIAT:w" s="T70">tɨmnʼat</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_263" n="HIAT:w" s="T71">ınna</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_266" n="HIAT:w" s="T72">qaqlaqɨntɨ</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_269" n="HIAT:w" s="T73">tɛltɔːtɨn</ts>
                  <nts id="Seg_270" n="HIAT:ip">.</nts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_273" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_275" n="HIAT:w" s="T74">Na</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_278" n="HIAT:w" s="T75">timnʼäsɨt</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_281" n="HIAT:w" s="T76">moqonä</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_284" n="HIAT:w" s="T77">laqaltɔːtɨt</ts>
                  <nts id="Seg_285" n="HIAT:ip">.</nts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_288" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_290" n="HIAT:w" s="T78">Tına</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_293" n="HIAT:w" s="T79">čʼontaqɨlʼ</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_296" n="HIAT:w" s="T80">sərɨ</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_299" n="HIAT:w" s="T81">qorqɨ</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_302" n="HIAT:w" s="T82">qätpilʼ</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_305" n="HIAT:w" s="T83">timnʼatɨt</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_308" n="HIAT:w" s="T84">čʼontaqɨt</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_311" n="HIAT:w" s="T85">moqɨnɨ</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_314" n="HIAT:w" s="T86">iːppa</ts>
                  <nts id="Seg_315" n="HIAT:ip">.</nts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_318" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_320" n="HIAT:w" s="T87">Nɨːnɨ</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_323" n="HIAT:w" s="T88">na</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_326" n="HIAT:w" s="T89">qup</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_329" n="HIAT:w" s="T90">ürukkumpa</ts>
                  <nts id="Seg_330" n="HIAT:ip">,</nts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_333" n="HIAT:w" s="T91">ompa</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_336" n="HIAT:w" s="T92">čʼupsa</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_339" n="HIAT:w" s="T93">montaqɨt</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_342" n="HIAT:w" s="T94">ɛːppa</ts>
                  <nts id="Seg_343" n="HIAT:ip">.</nts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_346" n="HIAT:u" s="T95">
                  <ts e="T97" id="Seg_348" n="HIAT:w" s="T95">Pusqatɔːlpa</ts>
                  <nts id="Seg_349" n="HIAT:ip">.</nts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_352" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_354" n="HIAT:w" s="T97">Təp</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_357" n="HIAT:w" s="T98">nɨːnɨ</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_360" n="HIAT:w" s="T99">sä</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_363" n="HIAT:w" s="T100">orɨmkɨlɨmpa</ts>
                  <nts id="Seg_364" n="HIAT:ip">.</nts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_367" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_369" n="HIAT:w" s="T101">Təp</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_372" n="HIAT:w" s="T102">na</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_375" n="HIAT:w" s="T103">morat</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_378" n="HIAT:w" s="T104">qanäqqɨt</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_381" n="HIAT:w" s="T105">nɨːntɨ</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_384" n="HIAT:w" s="T106">na</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_387" n="HIAT:w" s="T107">ippɨmmɨntɨ</ts>
                  <nts id="Seg_388" n="HIAT:ip">.</nts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T111" id="Seg_391" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_393" n="HIAT:w" s="T108">Təp</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_396" n="HIAT:w" s="T109">nɨmtɨ</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_399" n="HIAT:w" s="T110">ippɨmpa</ts>
                  <nts id="Seg_400" n="HIAT:ip">.</nts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T114" id="Seg_403" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_405" n="HIAT:w" s="T111">Nɔːtɨ</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_408" n="HIAT:w" s="T112">säːt</ts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_411" n="HIAT:w" s="T113">orɨmkɨlɨmpa</ts>
                  <nts id="Seg_412" n="HIAT:ip">.</nts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_415" n="HIAT:u" s="T114">
                  <ts e="T115" id="Seg_417" n="HIAT:w" s="T114">Nɔːt</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_420" n="HIAT:w" s="T115">čʼumpa</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_423" n="HIAT:w" s="T116">soqɨtɨ</ts>
                  <nts id="Seg_424" n="HIAT:ip">,</nts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_427" n="HIAT:w" s="T117">peːmɨtɨ</ts>
                  <nts id="Seg_428" n="HIAT:ip">.</nts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_431" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_433" n="HIAT:w" s="T118">Ola</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_436" n="HIAT:w" s="T119">sičʼi</ts>
                  <nts id="Seg_437" n="HIAT:ip">.</nts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_440" n="HIAT:u" s="T120">
                  <ts e="T121" id="Seg_442" n="HIAT:w" s="T120">Nɨːnɨ</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_445" n="HIAT:w" s="T121">nʼennä</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_448" n="HIAT:w" s="T122">qɨntalta</ts>
                  <nts id="Seg_449" n="HIAT:ip">,</nts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_452" n="HIAT:w" s="T123">üːnɨmtɨ</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_455" n="HIAT:w" s="T124">paŋɨsa</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_458" n="HIAT:w" s="T125">matɨŋɨtɨ</ts>
                  <nts id="Seg_459" n="HIAT:ip">.</nts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_462" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_464" n="HIAT:w" s="T126">Na</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_467" n="HIAT:w" s="T127">nɔːkɨr</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_470" n="HIAT:w" s="T128">quptɨtɨ</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_473" n="HIAT:w" s="T129">nɨːnɨ</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_476" n="HIAT:w" s="T130">pakta</ts>
                  <nts id="Seg_477" n="HIAT:ip">.</nts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T137" id="Seg_480" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_482" n="HIAT:w" s="T131">Təp</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_485" n="HIAT:w" s="T132">ontɨ</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_488" n="HIAT:w" s="T133">nɨmtɨ</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_491" n="HIAT:w" s="T134">ippɨmpa</ts>
                  <nts id="Seg_492" n="HIAT:ip">,</nts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_495" n="HIAT:w" s="T135">ɔːmäj</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_498" n="HIAT:w" s="T136">qontɨkkä</ts>
                  <nts id="Seg_499" n="HIAT:ip">.</nts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T141" id="Seg_502" n="HIAT:u" s="T137">
                  <ts e="T138" id="Seg_504" n="HIAT:w" s="T137">Nɨːnɨ</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_507" n="HIAT:w" s="T138">taŋɨmpa</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_510" n="HIAT:w" s="T139">namɨšak</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_513" n="HIAT:w" s="T140">apsɨtqa</ts>
                  <nts id="Seg_514" n="HIAT:ip">.</nts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T147" id="Seg_517" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_519" n="HIAT:w" s="T141">Qumpa</ts>
                  <nts id="Seg_520" n="HIAT:ip">,</nts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_523" n="HIAT:w" s="T142">nop</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_526" n="HIAT:w" s="T143">taŋɨmpa</ts>
                  <nts id="Seg_527" n="HIAT:ip">,</nts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_530" n="HIAT:w" s="T144">qaqlɨntɨ</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_533" n="HIAT:w" s="T145">šünčʼaqɨt</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_536" n="HIAT:w" s="T146">qontɨkka</ts>
                  <nts id="Seg_537" n="HIAT:ip">.</nts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_540" n="HIAT:u" s="T147">
                  <ts e="T148" id="Seg_542" n="HIAT:w" s="T147">Namɨšalʼ</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_545" n="HIAT:w" s="T148">ulqantɨ</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_548" n="HIAT:w" s="T149">laka</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_551" n="HIAT:w" s="T150">qalɨntɨ</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_554" n="HIAT:w" s="T151">ɨlqɨt</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_557" n="HIAT:w" s="T152">qalʼimmɨntɨ</ts>
                  <nts id="Seg_558" n="HIAT:ip">.</nts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T156" id="Seg_561" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_563" n="HIAT:w" s="T153">Mora</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_566" n="HIAT:w" s="T154">muntɨk</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_569" n="HIAT:w" s="T155">čʼumpa</ts>
                  <nts id="Seg_570" n="HIAT:ip">.</nts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T174" id="Seg_573" n="HIAT:u" s="T156">
                  <ts e="T157" id="Seg_575" n="HIAT:w" s="T156">Kur</ts>
                  <nts id="Seg_576" n="HIAT:ip">,</nts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_579" n="HIAT:w" s="T157">ukkur</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_582" n="HIAT:w" s="T158">čʼontaqɨt</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_585" n="HIAT:w" s="T159">täp</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_588" n="HIAT:w" s="T160">qos</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_591" n="HIAT:w" s="T161">qaj</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_594" n="HIAT:w" s="T162">nılʼčʼik</ts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_597" n="HIAT:w" s="T163">kətɨŋɨtɨ</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_600" n="HIAT:w" s="T164">qaj</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_603" n="HIAT:w" s="T165">nop</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_606" n="HIAT:w" s="T166">lʼi</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_609" n="HIAT:w" s="T167">qaj</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_612" n="HIAT:w" s="T168">qup</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_615" n="HIAT:w" s="T169">tämajsä</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_618" n="HIAT:w" s="T170">kurulʼä</ts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_621" n="HIAT:w" s="T171">alʼ</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_624" n="HIAT:w" s="T172">nılʼčʼik</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_627" n="HIAT:w" s="T173">kuka</ts>
                  <nts id="Seg_628" n="HIAT:ip">.</nts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T180" id="Seg_631" n="HIAT:u" s="T174">
                  <ts e="T175" id="Seg_633" n="HIAT:w" s="T174">Təp</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_636" n="HIAT:w" s="T175">ınna</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_639" n="HIAT:w" s="T176">sʼäp</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_642" n="HIAT:w" s="T177">antalumpa</ts>
                  <nts id="Seg_643" n="HIAT:ip">,</nts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_646" n="HIAT:w" s="T178">nılʼčʼik</ts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_649" n="HIAT:w" s="T179">kuka</ts>
                  <nts id="Seg_650" n="HIAT:ip">.</nts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_653" n="HIAT:u" s="T180">
                  <ts e="T181" id="Seg_655" n="HIAT:w" s="T180">Kekkɨsa</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_658" n="HIAT:w" s="T181">muːt</ts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_661" n="HIAT:w" s="T182">tınolʼa</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_664" n="HIAT:w" s="T183">atalimmɨntɔːtɨt</ts>
                  <nts id="Seg_665" n="HIAT:ip">.</nts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T190" id="Seg_668" n="HIAT:u" s="T184">
                  <ts e="T185" id="Seg_670" n="HIAT:w" s="T184">Na</ts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_673" n="HIAT:w" s="T185">qaqlʼintɨ</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_676" n="HIAT:w" s="T186">ilqɨlʼ</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_679" n="HIAT:w" s="T187">ulqalʼ</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_682" n="HIAT:w" s="T188">lakasä</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_685" n="HIAT:w" s="T189">qompɨrlʼä</ts>
                  <nts id="Seg_686" n="HIAT:ip">.</nts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T194" id="Seg_689" n="HIAT:u" s="T190">
                  <ts e="T191" id="Seg_691" n="HIAT:w" s="T190">Nɨːnä</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_694" n="HIAT:w" s="T191">aj</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_697" n="HIAT:w" s="T192">ılla</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_700" n="HIAT:w" s="T193">qonta</ts>
                  <nts id="Seg_701" n="HIAT:ip">.</nts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T206" id="Seg_704" n="HIAT:u" s="T194">
                  <ts e="T195" id="Seg_706" n="HIAT:w" s="T194">Aj</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_709" n="HIAT:w" s="T195">täp</ts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_712" n="HIAT:w" s="T196">qos</ts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_715" n="HIAT:w" s="T197">qaj</ts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_718" n="HIAT:w" s="T198">nılʼčʼik</ts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_721" n="HIAT:w" s="T199">šıp</ts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_724" n="HIAT:w" s="T200">soqočʼčʼi</ts>
                  <nts id="Seg_725" n="HIAT:ip">:</nts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_728" n="HIAT:w" s="T201">Qup</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_731" n="HIAT:w" s="T202">nılʼčʼik</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_734" n="HIAT:w" s="T203">ıla</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_737" n="HIAT:w" s="T204">kurola</ts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_740" n="HIAT:w" s="T205">kuka</ts>
                  <nts id="Seg_741" n="HIAT:ip">.</nts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T211" id="Seg_744" n="HIAT:u" s="T206">
                  <ts e="T207" id="Seg_746" n="HIAT:w" s="T206">Nɨːnɨ</ts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_749" n="HIAT:w" s="T207">qanɨqtɨ</ts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_752" n="HIAT:w" s="T208">konna</ts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_755" n="HIAT:w" s="T209">tanta</ts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_757" n="HIAT:ip">(</nts>
                  <nts id="Seg_758" n="HIAT:ip">/</nts>
                  <ts e="T211" id="Seg_760" n="HIAT:w" s="T210">tannɨmpa</ts>
                  <nts id="Seg_761" n="HIAT:ip">)</nts>
                  <nts id="Seg_762" n="HIAT:ip">.</nts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T215" id="Seg_765" n="HIAT:u" s="T211">
                  <ts e="T212" id="Seg_767" n="HIAT:w" s="T211">Sajitɨ</ts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_770" n="HIAT:w" s="T212">namɨšak</ts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_773" n="HIAT:w" s="T213">awsɨtqa</ts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_776" n="HIAT:w" s="T214">qumpa</ts>
                  <nts id="Seg_777" n="HIAT:ip">.</nts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T218" id="Seg_780" n="HIAT:u" s="T215">
                  <ts e="T216" id="Seg_782" n="HIAT:w" s="T215">Qɨlʼat</ts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_785" n="HIAT:w" s="T216">šünʼantɨ</ts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_788" n="HIAT:w" s="T217">tokɨmɔːtpa</ts>
                  <nts id="Seg_789" n="HIAT:ip">.</nts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T222" id="Seg_792" n="HIAT:u" s="T218">
                  <ts e="T219" id="Seg_794" n="HIAT:w" s="T218">Ukkur</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_797" n="HIAT:w" s="T219">čʼontaqɨt</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_800" n="HIAT:w" s="T220">nılʼčʼik</ts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_803" n="HIAT:w" s="T221">ɛːsa</ts>
                  <nts id="Seg_804" n="HIAT:ip">.</nts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T230" id="Seg_807" n="HIAT:u" s="T222">
                  <ts e="T223" id="Seg_809" n="HIAT:w" s="T222">Mitä</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_812" n="HIAT:w" s="T223">tətɨ</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_815" n="HIAT:w" s="T224">atäntɨ</ts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_817" n="HIAT:ip">(</nts>
                  <nts id="Seg_818" n="HIAT:ip">/</nts>
                  <ts e="T226" id="Seg_820" n="HIAT:w" s="T225">mittɨ</ts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_823" n="HIAT:w" s="T226">təttɨ</ts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_826" n="HIAT:w" s="T227">čʼurɨntɨ</ts>
                  <nts id="Seg_827" n="HIAT:ip">)</nts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_830" n="HIAT:w" s="T228">morat</ts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_833" n="HIAT:w" s="T229">qanɨqqɨt</ts>
                  <nts id="Seg_834" n="HIAT:ip">.</nts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T234" id="Seg_837" n="HIAT:u" s="T230">
                  <ts e="T231" id="Seg_839" n="HIAT:w" s="T230">Tep</ts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_842" n="HIAT:w" s="T231">namɨsʼak</ts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_845" n="HIAT:w" s="T232">apsɨtqa</ts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_848" n="HIAT:w" s="T233">qumpa</ts>
                  <nts id="Seg_849" n="HIAT:ip">.</nts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T242" id="Seg_852" n="HIAT:u" s="T234">
                  <ts e="T235" id="Seg_854" n="HIAT:w" s="T234">Nɨːnɨ</ts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_857" n="HIAT:w" s="T235">nılʼčʼik</ts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_860" n="HIAT:w" s="T236">sʼäp</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_863" n="HIAT:w" s="T237">qäŋŋa</ts>
                  <nts id="Seg_864" n="HIAT:ip">:</nts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_867" n="HIAT:w" s="T238">qaqlʼap</ts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_870" n="HIAT:w" s="T239">tətɨn</ts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_873" n="HIAT:w" s="T240">mənatɨ</ts>
                  <nts id="Seg_874" n="HIAT:ip">,</nts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_877" n="HIAT:w" s="T241">tulispa</ts>
                  <nts id="Seg_878" n="HIAT:ip">.</nts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T249" id="Seg_881" n="HIAT:u" s="T242">
                  <ts e="T243" id="Seg_883" n="HIAT:w" s="T242">Nɨːnɨ</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_886" n="HIAT:w" s="T243">qaqlɨntɨ</ts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_889" n="HIAT:w" s="T244">moqalit</ts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_892" n="HIAT:w" s="T245">iːmɨt</ts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_895" n="HIAT:w" s="T246">soqolʼä</ts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_898" n="HIAT:w" s="T247">kunna</ts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_901" n="HIAT:w" s="T248">tanta</ts>
                  <nts id="Seg_902" n="HIAT:ip">.</nts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T253" id="Seg_905" n="HIAT:u" s="T249">
                  <ts e="T250" id="Seg_907" n="HIAT:w" s="T249">Nɨːnɨ</ts>
                  <nts id="Seg_908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_910" n="HIAT:w" s="T250">konna</ts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_913" n="HIAT:w" s="T251">qanɨqtɨ</ts>
                  <nts id="Seg_914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_916" n="HIAT:w" s="T252">alʼčʼe</ts>
                  <nts id="Seg_917" n="HIAT:ip">.</nts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T256" id="Seg_920" n="HIAT:u" s="T253">
                  <ts e="T254" id="Seg_922" n="HIAT:w" s="T253">Nɨːnɨ</ts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_925" n="HIAT:w" s="T254">nɨmtɨ</ts>
                  <nts id="Seg_926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_928" n="HIAT:w" s="T255">ippa</ts>
                  <nts id="Seg_929" n="HIAT:ip">.</nts>
                  <nts id="Seg_930" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T265" id="Seg_932" n="HIAT:u" s="T256">
                  <ts e="T257" id="Seg_934" n="HIAT:w" s="T256">Aj</ts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_937" n="HIAT:w" s="T257">qos</ts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_940" n="HIAT:w" s="T258">qaj</ts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_943" n="HIAT:w" s="T259">nılʼčʼik</ts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_946" n="HIAT:w" s="T260">kətɨŋɨtɨ</ts>
                  <nts id="Seg_947" n="HIAT:ip">:</nts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_950" n="HIAT:w" s="T261">Qup</ts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_953" n="HIAT:w" s="T262">təmasä</ts>
                  <nts id="Seg_954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_956" n="HIAT:w" s="T263">kurɨla</ts>
                  <nts id="Seg_957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_959" n="HIAT:w" s="T264">qupna</ts>
                  <nts id="Seg_960" n="HIAT:ip">.</nts>
                  <nts id="Seg_961" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T272" id="Seg_963" n="HIAT:u" s="T265">
                  <ts e="T266" id="Seg_965" n="HIAT:w" s="T265">Konnat</ts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_968" n="HIAT:w" s="T266">nʼärqɨlʼ</ts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_971" n="HIAT:w" s="T267">markɨt</ts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_974" n="HIAT:w" s="T268">toːpɨntɨ</ts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_977" n="HIAT:w" s="T269">soqɨlʼa</ts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_980" n="HIAT:w" s="T270">konna</ts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_983" n="HIAT:w" s="T271">tanta</ts>
                  <nts id="Seg_984" n="HIAT:ip">.</nts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T276" id="Seg_987" n="HIAT:u" s="T272">
                  <ts e="T273" id="Seg_989" n="HIAT:w" s="T272">Nɨːna</ts>
                  <nts id="Seg_990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_992" n="HIAT:w" s="T273">konna</ts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_995" n="HIAT:w" s="T274">soqqolʼa</ts>
                  <nts id="Seg_996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_998" n="HIAT:w" s="T275">tanta</ts>
                  <nts id="Seg_999" n="HIAT:ip">.</nts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T281" id="Seg_1002" n="HIAT:u" s="T276">
                  <ts e="T277" id="Seg_1004" n="HIAT:w" s="T276">Mat</ts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1007" n="HIAT:w" s="T277">qotä</ts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1010" n="HIAT:w" s="T278">ınna</ts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1013" n="HIAT:w" s="T279">nɨlaıːsäŋ</ts>
                  <nts id="Seg_1014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1016" n="HIAT:w" s="T280">ɛna</ts>
                  <nts id="Seg_1017" n="HIAT:ip">.</nts>
                  <nts id="Seg_1018" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T284" id="Seg_1020" n="HIAT:u" s="T281">
                  <ts e="T282" id="Seg_1022" n="HIAT:w" s="T281">Konna</ts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1025" n="HIAT:w" s="T282">soqqolʼa</ts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1028" n="HIAT:w" s="T283">tanta</ts>
                  <nts id="Seg_1029" n="HIAT:ip">.</nts>
                  <nts id="Seg_1030" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T287" id="Seg_1032" n="HIAT:u" s="T284">
                  <ts e="T285" id="Seg_1034" n="HIAT:w" s="T284">Ortɨ</ts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1037" n="HIAT:w" s="T285">meːltɨ</ts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1040" n="HIAT:w" s="T286">čʼäːŋka</ts>
                  <nts id="Seg_1041" n="HIAT:ip">.</nts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T297" id="Seg_1044" n="HIAT:u" s="T287">
                  <ts e="T288" id="Seg_1046" n="HIAT:w" s="T287">Aj</ts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1049" n="HIAT:w" s="T288">qos</ts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1052" n="HIAT:w" s="T289">qaj</ts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1055" n="HIAT:w" s="T290">nılʼčʼik</ts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1058" n="HIAT:w" s="T291">soqalʼlʼitɨ</ts>
                  <nts id="Seg_1059" n="HIAT:ip">:</nts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1062" n="HIAT:w" s="T292">Qup</ts>
                  <nts id="Seg_1063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1065" n="HIAT:w" s="T293">aša</ts>
                  <nts id="Seg_1066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1068" n="HIAT:w" s="T294">nılʼčʼik</ts>
                  <nts id="Seg_1069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1071" n="HIAT:w" s="T295">kuka</ts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1074" n="HIAT:w" s="T296">kurolʼa</ts>
                  <nts id="Seg_1075" n="HIAT:ip">.</nts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T302" id="Seg_1078" n="HIAT:u" s="T297">
                  <ts e="T298" id="Seg_1080" n="HIAT:w" s="T297">Na</ts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1083" n="HIAT:w" s="T298">morat</ts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1086" n="HIAT:w" s="T299">qanɨqqät</ts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1089" n="HIAT:w" s="T300">pirqə</ts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1092" n="HIAT:w" s="T301">təttɨ</ts>
                  <nts id="Seg_1093" n="HIAT:ip">.</nts>
                  <nts id="Seg_1094" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T304" id="Seg_1096" n="HIAT:u" s="T302">
                  <ts e="T303" id="Seg_1098" n="HIAT:w" s="T302">İnna</ts>
                  <nts id="Seg_1099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1101" n="HIAT:w" s="T303">sɨqolʼna</ts>
                  <nts id="Seg_1102" n="HIAT:ip">.</nts>
                  <nts id="Seg_1103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T309" id="Seg_1105" n="HIAT:u" s="T304">
                  <ts e="T305" id="Seg_1107" n="HIAT:w" s="T304">Nɨːnɨ</ts>
                  <nts id="Seg_1108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1110" n="HIAT:w" s="T305">konna</ts>
                  <nts id="Seg_1111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1113" n="HIAT:w" s="T306">sɨqɨlʼä</ts>
                  <nts id="Seg_1114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1116" n="HIAT:w" s="T307">nılʼčʼik</ts>
                  <nts id="Seg_1117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1119" n="HIAT:w" s="T308">mannɨmpa</ts>
                  <nts id="Seg_1120" n="HIAT:ip">.</nts>
                  <nts id="Seg_1121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T315" id="Seg_1123" n="HIAT:u" s="T309">
                  <ts e="T310" id="Seg_1125" n="HIAT:w" s="T309">Konnat</ts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1128" n="HIAT:w" s="T310">nılʼčʼik</ts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1131" n="HIAT:w" s="T311">qoŋɨtɨ</ts>
                  <nts id="Seg_1132" n="HIAT:ip">:</nts>
                  <nts id="Seg_1133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1135" n="HIAT:w" s="T312">kotɨlʼ</ts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1138" n="HIAT:w" s="T313">mɔːt</ts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1141" n="HIAT:w" s="T314">ɔːmnɨntɨ</ts>
                  <nts id="Seg_1142" n="HIAT:ip">.</nts>
                  <nts id="Seg_1143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T319" id="Seg_1145" n="HIAT:u" s="T315">
                  <ts e="T316" id="Seg_1147" n="HIAT:w" s="T315">Nɨːnɨ</ts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1150" n="HIAT:w" s="T316">aj</ts>
                  <nts id="Seg_1151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1153" n="HIAT:w" s="T317">ılla</ts>
                  <nts id="Seg_1154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1156" n="HIAT:w" s="T318">olʼčʼeja</ts>
                  <nts id="Seg_1157" n="HIAT:ip">.</nts>
                  <nts id="Seg_1158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T322" id="Seg_1160" n="HIAT:u" s="T319">
                  <ts e="T320" id="Seg_1162" n="HIAT:w" s="T319">Kutar</ts>
                  <nts id="Seg_1163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1165" n="HIAT:w" s="T320">nɨčʼa</ts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1168" n="HIAT:w" s="T321">tuličʼčʼantɨk</ts>
                  <nts id="Seg_1169" n="HIAT:ip">?</nts>
                  <nts id="Seg_1170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T329" id="Seg_1172" n="HIAT:u" s="T322">
                  <ts e="T323" id="Seg_1174" n="HIAT:w" s="T322">Nɨːnɨ</ts>
                  <nts id="Seg_1175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1177" n="HIAT:w" s="T323">na</ts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1180" n="HIAT:w" s="T324">mɔːttɨ</ts>
                  <nts id="Seg_1181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1183" n="HIAT:w" s="T325">konna</ts>
                  <nts id="Seg_1184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1186" n="HIAT:w" s="T326">nɨlʼlʼa</ts>
                  <nts id="Seg_1187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1189" n="HIAT:w" s="T327">ılla</ts>
                  <nts id="Seg_1190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1192" n="HIAT:w" s="T328">tüŋa</ts>
                  <nts id="Seg_1193" n="HIAT:ip">.</nts>
                  <nts id="Seg_1194" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T332" id="Seg_1196" n="HIAT:u" s="T329">
                  <ts e="T330" id="Seg_1198" n="HIAT:w" s="T329">Qälʼi</ts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1201" n="HIAT:w" s="T330">mɔːt</ts>
                  <nts id="Seg_1202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1204" n="HIAT:w" s="T331">ɔːmnɨntɨ</ts>
                  <nts id="Seg_1205" n="HIAT:ip">.</nts>
                  <nts id="Seg_1206" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T335" id="Seg_1208" n="HIAT:u" s="T332">
                  <ts e="T333" id="Seg_1210" n="HIAT:w" s="T332">Ɔːtatɨ</ts>
                  <nts id="Seg_1211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1213" n="HIAT:w" s="T333">etɨmantoːqɨt</ts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1216" n="HIAT:w" s="T334">kuralimmɨntɔːtɨt</ts>
                  <nts id="Seg_1217" n="HIAT:ip">.</nts>
                  <nts id="Seg_1218" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T338" id="Seg_1220" n="HIAT:u" s="T335">
                  <ts e="T336" id="Seg_1222" n="HIAT:w" s="T335">Nɨːnɨ</ts>
                  <nts id="Seg_1223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1225" n="HIAT:w" s="T336">mɔːttɨ</ts>
                  <nts id="Seg_1226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1228" n="HIAT:w" s="T337">šerna</ts>
                  <nts id="Seg_1229" n="HIAT:ip">.</nts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T341" id="Seg_1232" n="HIAT:u" s="T338">
                  <ts e="T339" id="Seg_1234" n="HIAT:w" s="T338">Ira</ts>
                  <nts id="Seg_1235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1237" n="HIAT:w" s="T339">ukkur</ts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1240" n="HIAT:w" s="T340">nälʼatɨ</ts>
                  <nts id="Seg_1241" n="HIAT:ip">.</nts>
                  <nts id="Seg_1242" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T350" id="Seg_1244" n="HIAT:u" s="T341">
                  <ts e="T342" id="Seg_1246" n="HIAT:w" s="T341">Ukkur</ts>
                  <nts id="Seg_1247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1249" n="HIAT:w" s="T342">čʼontoːqɨt</ts>
                  <nts id="Seg_1250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1252" n="HIAT:w" s="T343">qäl</ts>
                  <nts id="Seg_1253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1255" n="HIAT:w" s="T521">ira</ts>
                  <nts id="Seg_1256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1258" n="HIAT:w" s="T344">nılʼčʼik</ts>
                  <nts id="Seg_1259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1261" n="HIAT:w" s="T345">ɛsa</ts>
                  <nts id="Seg_1262" n="HIAT:ip">:</nts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1265" n="HIAT:w" s="T346">Tat</ts>
                  <nts id="Seg_1266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1268" n="HIAT:w" s="T347">na</ts>
                  <nts id="Seg_1269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1271" n="HIAT:w" s="T348">qup</ts>
                  <nts id="Seg_1272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1274" n="HIAT:w" s="T349">awstäti</ts>
                  <nts id="Seg_1275" n="HIAT:ip">.</nts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T357" id="Seg_1278" n="HIAT:u" s="T350">
                  <ts e="T351" id="Seg_1280" n="HIAT:w" s="T350">Täp</ts>
                  <nts id="Seg_1281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1283" n="HIAT:w" s="T351">na</ts>
                  <nts id="Seg_1284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1286" n="HIAT:w" s="T352">iran</ts>
                  <nts id="Seg_1287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1289" n="HIAT:w" s="T353">mɨqɨt</ts>
                  <nts id="Seg_1290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1292" n="HIAT:w" s="T354">šitɨ</ts>
                  <nts id="Seg_1293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1295" n="HIAT:w" s="T355">toːt</ts>
                  <nts id="Seg_1296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1298" n="HIAT:w" s="T356">ɔːtat</ts>
                  <nts id="Seg_1299" n="HIAT:ip">.</nts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T367" id="Seg_1302" n="HIAT:u" s="T357">
                  <ts e="T358" id="Seg_1304" n="HIAT:w" s="T357">Mat</ts>
                  <nts id="Seg_1305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1307" n="HIAT:w" s="T358">ukkur</ts>
                  <nts id="Seg_1308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1310" n="HIAT:w" s="T359">toːt</ts>
                  <nts id="Seg_1311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1313" n="HIAT:w" s="T360">ɔːtamɨ</ts>
                  <nts id="Seg_1314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1316" n="HIAT:w" s="T361">suːrup</ts>
                  <nts id="Seg_1317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1319" n="HIAT:w" s="T362">muntɨk</ts>
                  <nts id="Seg_1320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1322" n="HIAT:w" s="T363">amnɨtɨ</ts>
                  <nts id="Seg_1323" n="HIAT:ip">,</nts>
                  <nts id="Seg_1324" n="HIAT:ip">–</nts>
                  <nts id="Seg_1325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1327" n="HIAT:w" s="T364">ira</ts>
                  <nts id="Seg_1328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1330" n="HIAT:w" s="T365">nılʼčʼik</ts>
                  <nts id="Seg_1331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1333" n="HIAT:w" s="T366">kətɨŋɨt</ts>
                  <nts id="Seg_1334" n="HIAT:ip">.</nts>
                  <nts id="Seg_1335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T373" id="Seg_1337" n="HIAT:u" s="T367">
                  <ts e="T368" id="Seg_1339" n="HIAT:w" s="T367">Tat</ts>
                  <nts id="Seg_1340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1342" n="HIAT:w" s="T368">pona</ts>
                  <nts id="Seg_1343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1345" n="HIAT:w" s="T369">tantɨlʼä</ts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1348" n="HIAT:w" s="T370">mat</ts>
                  <nts id="Seg_1349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1351" n="HIAT:w" s="T371">ɔːtamɨ</ts>
                  <nts id="Seg_1352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1354" n="HIAT:w" s="T372">ɔːtätɨ</ts>
                  <nts id="Seg_1355" n="HIAT:ip">.</nts>
                  <nts id="Seg_1356" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T374" id="Seg_1358" n="HIAT:u" s="T373">
                  <ts e="T374" id="Seg_1360" n="HIAT:w" s="T373">Lʼupkɨmɔːnnä</ts>
                  <nts id="Seg_1361" n="HIAT:ip">.</nts>
                  <nts id="Seg_1362" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T377" id="Seg_1364" n="HIAT:u" s="T374">
                  <ts e="T375" id="Seg_1366" n="HIAT:w" s="T374">Täp</ts>
                  <nts id="Seg_1367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1369" n="HIAT:w" s="T375">pona</ts>
                  <nts id="Seg_1370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1372" n="HIAT:w" s="T376">tanta</ts>
                  <nts id="Seg_1373" n="HIAT:ip">.</nts>
                  <nts id="Seg_1374" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T385" id="Seg_1376" n="HIAT:u" s="T377">
                  <ts e="T378" id="Seg_1378" n="HIAT:w" s="T377">Täp</ts>
                  <nts id="Seg_1379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1381" n="HIAT:w" s="T378">pona</ts>
                  <nts id="Seg_1382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1384" n="HIAT:w" s="T379">tanta</ts>
                  <nts id="Seg_1385" n="HIAT:ip">,</nts>
                  <nts id="Seg_1386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1388" n="HIAT:w" s="T380">namɨšak</ts>
                  <nts id="Seg_1389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1391" n="HIAT:w" s="T381">seqelʼimpɨ</ts>
                  <nts id="Seg_1392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1394" n="HIAT:w" s="T382">qup</ts>
                  <nts id="Seg_1395" n="HIAT:ip">,</nts>
                  <nts id="Seg_1396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1398" n="HIAT:w" s="T383">qajtɨ</ts>
                  <nts id="Seg_1399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1401" n="HIAT:w" s="T384">mannɨmmɛntɨt</ts>
                  <nts id="Seg_1402" n="HIAT:ip">?</nts>
                  <nts id="Seg_1403" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T390" id="Seg_1405" n="HIAT:u" s="T385">
                  <ts e="T386" id="Seg_1407" n="HIAT:w" s="T385">Nɨːnɨ</ts>
                  <nts id="Seg_1408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1410" n="HIAT:w" s="T386">täp</ts>
                  <nts id="Seg_1411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1413" n="HIAT:w" s="T387">aj</ts>
                  <nts id="Seg_1414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1416" n="HIAT:w" s="T388">ılla</ts>
                  <nts id="Seg_1417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1419" n="HIAT:w" s="T389">alʼčʼimpa</ts>
                  <nts id="Seg_1420" n="HIAT:ip">.</nts>
                  <nts id="Seg_1421" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T393" id="Seg_1423" n="HIAT:u" s="T390">
                  <ts e="T391" id="Seg_1425" n="HIAT:w" s="T390">Nɨmtɨ</ts>
                  <nts id="Seg_1426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1428" n="HIAT:w" s="T391">na</ts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1431" n="HIAT:w" s="T392">ippɨmmɨntɨ</ts>
                  <nts id="Seg_1432" n="HIAT:ip">.</nts>
                  <nts id="Seg_1433" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T400" id="Seg_1435" n="HIAT:u" s="T393">
                  <ts e="T394" id="Seg_1437" n="HIAT:w" s="T393">Na</ts>
                  <nts id="Seg_1438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1440" n="HIAT:w" s="T394">täpɨt</ts>
                  <nts id="Seg_1441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1443" n="HIAT:w" s="T395">tülʼä</ts>
                  <nts id="Seg_1444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1446" n="HIAT:w" s="T396">puːla</ts>
                  <nts id="Seg_1447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1449" n="HIAT:w" s="T397">suːrup</ts>
                  <nts id="Seg_1450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1452" n="HIAT:w" s="T398">na</ts>
                  <nts id="Seg_1453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1455" n="HIAT:w" s="T399">qənmɨntɨ</ts>
                  <nts id="Seg_1456" n="HIAT:ip">.</nts>
                  <nts id="Seg_1457" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T407" id="Seg_1459" n="HIAT:u" s="T400">
                  <ts e="T401" id="Seg_1461" n="HIAT:w" s="T400">Nɨː</ts>
                  <nts id="Seg_1462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1464" n="HIAT:w" s="T401">ukoːt</ts>
                  <nts id="Seg_1465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1467" n="HIAT:w" s="T402">äsäsɨqäːqi</ts>
                  <nts id="Seg_1468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1470" n="HIAT:w" s="T403">ɔːtantij</ts>
                  <nts id="Seg_1471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1473" n="HIAT:w" s="T404">ɔːtɨlä</ts>
                  <nts id="Seg_1474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1476" n="HIAT:w" s="T405">poːqot</ts>
                  <nts id="Seg_1477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1479" n="HIAT:w" s="T406">ilimpɔːqij</ts>
                  <nts id="Seg_1480" n="HIAT:ip">.</nts>
                  <nts id="Seg_1481" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T414" id="Seg_1483" n="HIAT:u" s="T407">
                  <ts e="T408" id="Seg_1485" n="HIAT:w" s="T407">Nɨːnɨ</ts>
                  <nts id="Seg_1486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1488" n="HIAT:w" s="T408">na</ts>
                  <nts id="Seg_1489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1491" n="HIAT:w" s="T409">qumup</ts>
                  <nts id="Seg_1492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1494" n="HIAT:w" s="T410">awstɨmpɨlʼä</ts>
                  <nts id="Seg_1495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1497" n="HIAT:w" s="T411">nɨmtɨ</ts>
                  <nts id="Seg_1498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1500" n="HIAT:w" s="T412">na</ts>
                  <nts id="Seg_1501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1503" n="HIAT:w" s="T413">illemmɨntɨ</ts>
                  <nts id="Seg_1504" n="HIAT:ip">.</nts>
                  <nts id="Seg_1505" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T424" id="Seg_1507" n="HIAT:u" s="T414">
                  <ts e="T415" id="Seg_1509" n="HIAT:w" s="T414">Nɨːnä</ts>
                  <nts id="Seg_1510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1512" n="HIAT:w" s="T415">na</ts>
                  <nts id="Seg_1513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1515" n="HIAT:w" s="T416">ire</ts>
                  <nts id="Seg_1516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1518" n="HIAT:w" s="T417">aj</ts>
                  <nts id="Seg_1519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1521" n="HIAT:w" s="T418">nılʼčʼik</ts>
                  <nts id="Seg_1522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1524" n="HIAT:w" s="T419">kətɨŋɨtɨ</ts>
                  <nts id="Seg_1525" n="HIAT:ip">:</nts>
                  <nts id="Seg_1526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1528" n="HIAT:w" s="T420">Aj</ts>
                  <nts id="Seg_1529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1531" n="HIAT:w" s="T421">qəlasik</ts>
                  <nts id="Seg_1532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1534" n="HIAT:w" s="T422">ɔːtamtɨ</ts>
                  <nts id="Seg_1535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1537" n="HIAT:w" s="T423">ɔːtɨlʼä</ts>
                  <nts id="Seg_1538" n="HIAT:ip">.</nts>
                  <nts id="Seg_1539" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T429" id="Seg_1541" n="HIAT:u" s="T424">
                  <ts e="T425" id="Seg_1543" n="HIAT:w" s="T424">Ɔːtatɨ</ts>
                  <nts id="Seg_1544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1546" n="HIAT:w" s="T425">qapije</ts>
                  <nts id="Seg_1547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1549" n="HIAT:w" s="T426">mačʼantɨ</ts>
                  <nts id="Seg_1550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1552" n="HIAT:w" s="T427">asʼa</ts>
                  <nts id="Seg_1553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1555" n="HIAT:w" s="T428">qənta</ts>
                  <nts id="Seg_1556" n="HIAT:ip">.</nts>
                  <nts id="Seg_1557" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T432" id="Seg_1559" n="HIAT:u" s="T429">
                  <ts e="T430" id="Seg_1561" n="HIAT:w" s="T429">Mɔːtɨt</ts>
                  <nts id="Seg_1562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1564" n="HIAT:w" s="T430">qanɨqqɨt</ts>
                  <nts id="Seg_1565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1567" n="HIAT:w" s="T431">qontɔːtɨt</ts>
                  <nts id="Seg_1568" n="HIAT:ip">.</nts>
                  <nts id="Seg_1569" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T442" id="Seg_1571" n="HIAT:u" s="T432">
                  <ts e="T433" id="Seg_1573" n="HIAT:w" s="T432">Nɨːnä</ts>
                  <nts id="Seg_1574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1576" n="HIAT:w" s="T433">täp</ts>
                  <nts id="Seg_1577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1579" n="HIAT:w" s="T434">pona</ts>
                  <nts id="Seg_1580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1582" n="HIAT:w" s="T435">tanta</ts>
                  <nts id="Seg_1583" n="HIAT:ip">,</nts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1586" n="HIAT:w" s="T436">seːpɨlʼalʼ</ts>
                  <nts id="Seg_1587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1589" n="HIAT:w" s="T437">mɨntɨ</ts>
                  <nts id="Seg_1590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1592" n="HIAT:w" s="T438">qəlla</ts>
                  <nts id="Seg_1593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1595" n="HIAT:w" s="T439">aj</ts>
                  <nts id="Seg_1596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1598" n="HIAT:w" s="T440">ılla</ts>
                  <nts id="Seg_1599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1601" n="HIAT:w" s="T441">olʼčʼi</ts>
                  <nts id="Seg_1602" n="HIAT:ip">.</nts>
                  <nts id="Seg_1603" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T448" id="Seg_1605" n="HIAT:u" s="T442">
                  <ts e="T443" id="Seg_1607" n="HIAT:w" s="T442">Nɨːnɨ</ts>
                  <nts id="Seg_1608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1610" n="HIAT:w" s="T443">təp</ts>
                  <nts id="Seg_1611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1613" n="HIAT:w" s="T444">ukur</ts>
                  <nts id="Seg_1614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1616" n="HIAT:w" s="T445">čʼontoːqɨt</ts>
                  <nts id="Seg_1617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1619" n="HIAT:w" s="T446">nılʼčʼik</ts>
                  <nts id="Seg_1620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1622" n="HIAT:w" s="T447">šitɨje</ts>
                  <nts id="Seg_1623" n="HIAT:ip">.</nts>
                  <nts id="Seg_1624" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T450" id="Seg_1626" n="HIAT:u" s="T448">
                  <ts e="T449" id="Seg_1628" n="HIAT:w" s="T448">Qaj</ts>
                  <nts id="Seg_1629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1631" n="HIAT:w" s="T449">qəntɨtɨ</ts>
                  <nts id="Seg_1632" n="HIAT:ip">?</nts>
                  <nts id="Seg_1633" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T452" id="Seg_1635" n="HIAT:u" s="T450">
                  <ts e="T451" id="Seg_1637" n="HIAT:w" s="T450">Montɨ</ts>
                  <nts id="Seg_1638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1640" n="HIAT:w" s="T451">ilɛčʼa</ts>
                  <nts id="Seg_1641" n="HIAT:ip">.</nts>
                  <nts id="Seg_1642" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T455" id="Seg_1644" n="HIAT:u" s="T452">
                  <ts e="T453" id="Seg_1646" n="HIAT:w" s="T452">Nɨːnɨ</ts>
                  <nts id="Seg_1647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1649" n="HIAT:w" s="T453">moqona</ts>
                  <nts id="Seg_1650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1652" n="HIAT:w" s="T454">tüŋa</ts>
                  <nts id="Seg_1653" n="HIAT:ip">.</nts>
                  <nts id="Seg_1654" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T458" id="Seg_1656" n="HIAT:u" s="T455">
                  <ts e="T456" id="Seg_1658" n="HIAT:w" s="T455">Mɔːttɨ</ts>
                  <nts id="Seg_1659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1661" n="HIAT:w" s="T456">qäli</ts>
                  <nts id="Seg_1662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1664" n="HIAT:w" s="T522">ira</ts>
                  <nts id="Seg_1665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1667" n="HIAT:w" s="T457">qonta</ts>
                  <nts id="Seg_1668" n="HIAT:ip">.</nts>
                  <nts id="Seg_1669" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T464" id="Seg_1671" n="HIAT:u" s="T458">
                  <ts e="T459" id="Seg_1673" n="HIAT:w" s="T458">Nɨːnɨ</ts>
                  <nts id="Seg_1674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1676" n="HIAT:w" s="T459">təpɨtɨt</ts>
                  <nts id="Seg_1677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1679" n="HIAT:w" s="T460">ɔːta</ts>
                  <nts id="Seg_1680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1682" n="HIAT:w" s="T461">meːltɨ</ts>
                  <nts id="Seg_1683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1685" n="HIAT:w" s="T462">etimontoːqɨt</ts>
                  <nts id="Seg_1686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1688" n="HIAT:w" s="T463">qontɔːtɨt</ts>
                  <nts id="Seg_1689" n="HIAT:ip">.</nts>
                  <nts id="Seg_1690" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T470" id="Seg_1692" n="HIAT:u" s="T464">
                  <ts e="T465" id="Seg_1694" n="HIAT:w" s="T464">Təp</ts>
                  <nts id="Seg_1695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1697" n="HIAT:w" s="T465">na</ts>
                  <nts id="Seg_1698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1700" n="HIAT:w" s="T466">tüpilʼ</ts>
                  <nts id="Seg_1701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1703" n="HIAT:w" s="T467">porqɨlʼ</ts>
                  <nts id="Seg_1704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1706" n="HIAT:w" s="T468">peːmɨtɨsa</ts>
                  <nts id="Seg_1707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1709" n="HIAT:w" s="T469">ila</ts>
                  <nts id="Seg_1710" n="HIAT:ip">.</nts>
                  <nts id="Seg_1711" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T475" id="Seg_1713" n="HIAT:u" s="T470">
                  <ts e="T471" id="Seg_1715" n="HIAT:w" s="T470">Nɨːnɨ</ts>
                  <nts id="Seg_1716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1718" n="HIAT:w" s="T471">nɔːkɨrɨmtälʼi</ts>
                  <nts id="Seg_1719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1721" n="HIAT:w" s="T472">pit</ts>
                  <nts id="Seg_1722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1724" n="HIAT:w" s="T473">nılʼčʼik</ts>
                  <nts id="Seg_1725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1727" n="HIAT:w" s="T474">ɛsa</ts>
                  <nts id="Seg_1728" n="HIAT:ip">.</nts>
                  <nts id="Seg_1729" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T479" id="Seg_1731" n="HIAT:u" s="T475">
                  <ts e="T476" id="Seg_1733" n="HIAT:w" s="T475">Mitɨ</ts>
                  <nts id="Seg_1734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1736" n="HIAT:w" s="T476">mol</ts>
                  <nts id="Seg_1737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1739" n="HIAT:w" s="T477">somak</ts>
                  <nts id="Seg_1740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1742" n="HIAT:w" s="T478">ɛsak</ts>
                  <nts id="Seg_1743" n="HIAT:ip">.</nts>
                  <nts id="Seg_1744" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T484" id="Seg_1746" n="HIAT:u" s="T479">
                  <ts e="T523" id="Seg_1748" n="HIAT:w" s="T479">Qäli</ts>
                  <nts id="Seg_1749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1751" n="HIAT:w" s="T523">ira</ts>
                  <nts id="Seg_1752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1754" n="HIAT:w" s="T480">ɔːtatɨ</ts>
                  <nts id="Seg_1755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1757" n="HIAT:w" s="T481">šöttɨ</ts>
                  <nts id="Seg_1758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1760" n="HIAT:w" s="T482">asʼa</ts>
                  <nts id="Seg_1761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1763" n="HIAT:w" s="T483">qənta</ts>
                  <nts id="Seg_1764" n="HIAT:ip">.</nts>
                  <nts id="Seg_1765" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T488" id="Seg_1767" n="HIAT:u" s="T484">
                  <ts e="T485" id="Seg_1769" n="HIAT:w" s="T484">Mɔːtɨt</ts>
                  <nts id="Seg_1770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1772" n="HIAT:w" s="T485">qanaqqɨt</ts>
                  <nts id="Seg_1773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1775" n="HIAT:w" s="T486">nɨmtɨ</ts>
                  <nts id="Seg_1776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1778" n="HIAT:w" s="T487">qəntɔːtɨt</ts>
                  <nts id="Seg_1779" n="HIAT:ip">.</nts>
                  <nts id="Seg_1780" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T493" id="Seg_1782" n="HIAT:u" s="T488">
                  <ts e="T489" id="Seg_1784" n="HIAT:w" s="T488">Täp</ts>
                  <nts id="Seg_1785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1787" n="HIAT:w" s="T489">tüla</ts>
                  <nts id="Seg_1788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1790" n="HIAT:w" s="T490">aj</ts>
                  <nts id="Seg_1791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1793" n="HIAT:w" s="T491">ılla</ts>
                  <nts id="Seg_1794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1796" n="HIAT:w" s="T492">qontaje</ts>
                  <nts id="Seg_1797" n="HIAT:ip">.</nts>
                  <nts id="Seg_1798" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T498" id="Seg_1800" n="HIAT:u" s="T493">
                  <ts e="T494" id="Seg_1802" n="HIAT:w" s="T493">Tɔːptɨlʼ</ts>
                  <nts id="Seg_1803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1805" n="HIAT:w" s="T494">qarɨt</ts>
                  <nts id="Seg_1806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1808" n="HIAT:w" s="T495">montɨ</ts>
                  <nts id="Seg_1809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1811" n="HIAT:w" s="T496">sɨrɨ</ts>
                  <nts id="Seg_1812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1814" n="HIAT:w" s="T497">pinnaıːntɨtɨ</ts>
                  <nts id="Seg_1815" n="HIAT:ip">.</nts>
                  <nts id="Seg_1816" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T506" id="Seg_1818" n="HIAT:u" s="T498">
                  <ts e="T499" id="Seg_1820" n="HIAT:w" s="T498">Mat</ts>
                  <nts id="Seg_1821" n="HIAT:ip">,</nts>
                  <nts id="Seg_1822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1824" n="HIAT:w" s="T499">täp</ts>
                  <nts id="Seg_1825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1827" n="HIAT:w" s="T500">kuttar</ts>
                  <nts id="Seg_1828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1830" n="HIAT:w" s="T501">tüsa</ts>
                  <nts id="Seg_1831" n="HIAT:ip">,</nts>
                  <nts id="Seg_1832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1834" n="HIAT:w" s="T502">ɔːtamɨ</ts>
                  <nts id="Seg_1835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1837" n="HIAT:w" s="T503">šöttɨ</ts>
                  <nts id="Seg_1838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1840" n="HIAT:w" s="T504">aša</ts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1843" n="HIAT:w" s="T505">qənta</ts>
                  <nts id="Seg_1844" n="HIAT:ip">.</nts>
                  <nts id="Seg_1845" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T510" id="Seg_1847" n="HIAT:u" s="T506">
                  <ts e="T507" id="Seg_1849" n="HIAT:w" s="T506">Kəraltättɨ</ts>
                  <nts id="Seg_1850" n="HIAT:ip">,</nts>
                  <nts id="Seg_1851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1853" n="HIAT:w" s="T507">sɨrɨp</ts>
                  <nts id="Seg_1854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1856" n="HIAT:w" s="T508">pilʼlʼa</ts>
                  <nts id="Seg_1857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1859" n="HIAT:w" s="T509">qəŋaje</ts>
                  <nts id="Seg_1860" n="HIAT:ip">.</nts>
                  <nts id="Seg_1861" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T514" id="Seg_1863" n="HIAT:u" s="T510">
                  <ts e="T511" id="Seg_1865" n="HIAT:w" s="T510">Somak</ts>
                  <nts id="Seg_1866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1868" n="HIAT:w" s="T511">ılla</ts>
                  <nts id="Seg_1869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1871" n="HIAT:w" s="T512">na</ts>
                  <nts id="Seg_1872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1874" n="HIAT:w" s="T513">qəŋtɨ</ts>
                  <nts id="Seg_1875" n="HIAT:ip">.</nts>
                  <nts id="Seg_1876" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T518" id="Seg_1878" n="HIAT:u" s="T514">
                  <ts e="T515" id="Seg_1880" n="HIAT:w" s="T514">Täp</ts>
                  <nts id="Seg_1881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1883" n="HIAT:w" s="T515">nɔːtɨ</ts>
                  <nts id="Seg_1884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1886" n="HIAT:w" s="T516">somak</ts>
                  <nts id="Seg_1887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1889" n="HIAT:w" s="T517">ɛsa</ts>
                  <nts id="Seg_1890" n="HIAT:ip">.</nts>
                  <nts id="Seg_1891" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T518" id="Seg_1892" n="sc" s="T0">
               <ts e="T1" id="Seg_1894" n="e" s="T0">Na </ts>
               <ts e="T96" id="Seg_1896" n="e" s="T1">qäl </ts>
               <ts e="T2" id="Seg_1898" n="e" s="T96">ira </ts>
               <ts e="T3" id="Seg_1900" n="e" s="T2">ija </ts>
               <ts e="T4" id="Seg_1902" n="e" s="T3">nılʼčʼik </ts>
               <ts e="T5" id="Seg_1904" n="e" s="T4">kəttɨŋɨtɨ: </ts>
               <ts e="T6" id="Seg_1906" n="e" s="T5">Mat </ts>
               <ts e="T7" id="Seg_1908" n="e" s="T6">tap </ts>
               <ts e="T8" id="Seg_1910" n="e" s="T7">moraqɨt </ts>
               <ts e="T9" id="Seg_1912" n="e" s="T8">särɨ </ts>
               <ts e="T10" id="Seg_1914" n="e" s="T9">qorqɨt </ts>
               <ts e="T11" id="Seg_1916" n="e" s="T10">qəttat. </ts>
               <ts e="T519" id="Seg_1918" n="e" s="T11">Qal </ts>
               <ts e="T12" id="Seg_1920" n="e" s="T519">ira </ts>
               <ts e="T13" id="Seg_1922" n="e" s="T12">nılʼčʼik </ts>
               <ts e="T14" id="Seg_1924" n="e" s="T13">kəttɨŋɨt: </ts>
               <ts e="T15" id="Seg_1926" n="e" s="T14">Na </ts>
               <ts e="T16" id="Seg_1928" n="e" s="T15">aša </ts>
               <ts e="T17" id="Seg_1930" n="e" s="T16">qumɨt </ts>
               <ts e="T18" id="Seg_1932" n="e" s="T17">qəssa </ts>
               <ts e="T19" id="Seg_1934" n="e" s="T18">suːrup. </ts>
               <ts e="T20" id="Seg_1936" n="e" s="T19">Tat </ts>
               <ts e="T21" id="Seg_1938" n="e" s="T20">kutar </ts>
               <ts e="T22" id="Seg_1940" n="e" s="T21">qənnantɨ </ts>
               <ts e="T23" id="Seg_1942" n="e" s="T22">morat </ts>
               <ts e="T24" id="Seg_1944" n="e" s="T23">tɔː. </ts>
               <ts e="T25" id="Seg_1946" n="e" s="T24">Qapija </ts>
               <ts e="T26" id="Seg_1948" n="e" s="T25">poːkɨtɨlʼ </ts>
               <ts e="T27" id="Seg_1950" n="e" s="T26">tətaqɨt </ts>
               <ts e="T28" id="Seg_1952" n="e" s="T27">ilʼitɨ </ts>
               <ts e="T520" id="Seg_1954" n="e" s="T28">qälʼi </ts>
               <ts e="T29" id="Seg_1956" n="e" s="T520">ira. </ts>
               <ts e="T30" id="Seg_1958" n="e" s="T29">Täpɨt </ts>
               <ts e="T31" id="Seg_1960" n="e" s="T30">nɔːkɨr </ts>
               <ts e="T32" id="Seg_1962" n="e" s="T31">timnʼäsik </ts>
               <ts e="T33" id="Seg_1964" n="e" s="T32">morat </ts>
               <ts e="T34" id="Seg_1966" n="e" s="T33">təː </ts>
               <ts e="T35" id="Seg_1968" n="e" s="T34">na </ts>
               <ts e="T36" id="Seg_1970" n="e" s="T35">särɨ </ts>
               <ts e="T37" id="Seg_1972" n="e" s="T36">qorqɨp </ts>
               <ts e="T38" id="Seg_1974" n="e" s="T37">qätqa </ts>
               <ts e="T39" id="Seg_1976" n="e" s="T38">qənnɔːtɨt. </ts>
               <ts e="T40" id="Seg_1978" n="e" s="T39">Täpɨt </ts>
               <ts e="T41" id="Seg_1980" n="e" s="T40">qännɔːtɨt. </ts>
               <ts e="T42" id="Seg_1982" n="e" s="T41">Na </ts>
               <ts e="T43" id="Seg_1984" n="e" s="T42">särɨ </ts>
               <ts e="T44" id="Seg_1986" n="e" s="T43">qorqɨp, </ts>
               <ts e="T45" id="Seg_1988" n="e" s="T44">əmɨntɨ </ts>
               <ts e="T46" id="Seg_1990" n="e" s="T45">čʼatɨŋɨtɨ. </ts>
               <ts e="T47" id="Seg_1992" n="e" s="T46">Nɨnɨ </ts>
               <ts e="T48" id="Seg_1994" n="e" s="T47">sərɨ </ts>
               <ts e="T49" id="Seg_1996" n="e" s="T48">qorqɨp </ts>
               <ts e="T50" id="Seg_1998" n="e" s="T49">((…)), </ts>
               <ts e="T51" id="Seg_2000" n="e" s="T50">na </ts>
               <ts e="T52" id="Seg_2002" n="e" s="T51">särɨ </ts>
               <ts e="T53" id="Seg_2004" n="e" s="T52">qorqɨ </ts>
               <ts e="T54" id="Seg_2006" n="e" s="T53">mompa </ts>
               <ts e="T55" id="Seg_2008" n="e" s="T54">märqɨ </ts>
               <ts e="T56" id="Seg_2010" n="e" s="T55">ɛːŋa. </ts>
               <ts e="T57" id="Seg_2012" n="e" s="T56">Nɨːnɨ </ts>
               <ts e="T58" id="Seg_2014" n="e" s="T57">təpɨt </ts>
               <ts e="T59" id="Seg_2016" n="e" s="T58">qəlla </ts>
               <ts e="T60" id="Seg_2018" n="e" s="T59">nɔːnnɨntɨ </ts>
               <ts e="T61" id="Seg_2020" n="e" s="T60">kɨmɨt </ts>
               <ts e="T62" id="Seg_2022" n="e" s="T61">sitɨ </ts>
               <ts e="T63" id="Seg_2024" n="e" s="T62">korɔːtɨt. </ts>
               <ts e="T64" id="Seg_2026" n="e" s="T63">Aj </ts>
               <ts e="T65" id="Seg_2028" n="e" s="T64">moqalmɨntɨ </ts>
               <ts e="T66" id="Seg_2030" n="e" s="T65">sitɨ </ts>
               <ts e="T67" id="Seg_2032" n="e" s="T66">korɔːt. </ts>
               <ts e="T68" id="Seg_2034" n="e" s="T67">Nɨːnɨ </ts>
               <ts e="T69" id="Seg_2036" n="e" s="T68">na </ts>
               <ts e="T70" id="Seg_2038" n="e" s="T69">šitɨ </ts>
               <ts e="T71" id="Seg_2040" n="e" s="T70">tɨmnʼat </ts>
               <ts e="T72" id="Seg_2042" n="e" s="T71">ınna </ts>
               <ts e="T73" id="Seg_2044" n="e" s="T72">qaqlaqɨntɨ </ts>
               <ts e="T74" id="Seg_2046" n="e" s="T73">tɛltɔːtɨn. </ts>
               <ts e="T75" id="Seg_2048" n="e" s="T74">Na </ts>
               <ts e="T76" id="Seg_2050" n="e" s="T75">timnʼäsɨt </ts>
               <ts e="T77" id="Seg_2052" n="e" s="T76">moqonä </ts>
               <ts e="T78" id="Seg_2054" n="e" s="T77">laqaltɔːtɨt. </ts>
               <ts e="T79" id="Seg_2056" n="e" s="T78">Tına </ts>
               <ts e="T80" id="Seg_2058" n="e" s="T79">čʼontaqɨlʼ </ts>
               <ts e="T81" id="Seg_2060" n="e" s="T80">sərɨ </ts>
               <ts e="T82" id="Seg_2062" n="e" s="T81">qorqɨ </ts>
               <ts e="T83" id="Seg_2064" n="e" s="T82">qätpilʼ </ts>
               <ts e="T84" id="Seg_2066" n="e" s="T83">timnʼatɨt </ts>
               <ts e="T85" id="Seg_2068" n="e" s="T84">čʼontaqɨt </ts>
               <ts e="T86" id="Seg_2070" n="e" s="T85">moqɨnɨ </ts>
               <ts e="T87" id="Seg_2072" n="e" s="T86">iːppa. </ts>
               <ts e="T88" id="Seg_2074" n="e" s="T87">Nɨːnɨ </ts>
               <ts e="T89" id="Seg_2076" n="e" s="T88">na </ts>
               <ts e="T90" id="Seg_2078" n="e" s="T89">qup </ts>
               <ts e="T91" id="Seg_2080" n="e" s="T90">ürukkumpa, </ts>
               <ts e="T92" id="Seg_2082" n="e" s="T91">ompa </ts>
               <ts e="T93" id="Seg_2084" n="e" s="T92">čʼupsa </ts>
               <ts e="T94" id="Seg_2086" n="e" s="T93">montaqɨt </ts>
               <ts e="T95" id="Seg_2088" n="e" s="T94">ɛːppa. </ts>
               <ts e="T97" id="Seg_2090" n="e" s="T95">Pusqatɔːlpa. </ts>
               <ts e="T98" id="Seg_2092" n="e" s="T97">Təp </ts>
               <ts e="T99" id="Seg_2094" n="e" s="T98">nɨːnɨ </ts>
               <ts e="T100" id="Seg_2096" n="e" s="T99">sä </ts>
               <ts e="T101" id="Seg_2098" n="e" s="T100">orɨmkɨlɨmpa. </ts>
               <ts e="T102" id="Seg_2100" n="e" s="T101">Təp </ts>
               <ts e="T103" id="Seg_2102" n="e" s="T102">na </ts>
               <ts e="T104" id="Seg_2104" n="e" s="T103">morat </ts>
               <ts e="T105" id="Seg_2106" n="e" s="T104">qanäqqɨt </ts>
               <ts e="T106" id="Seg_2108" n="e" s="T105">nɨːntɨ </ts>
               <ts e="T107" id="Seg_2110" n="e" s="T106">na </ts>
               <ts e="T108" id="Seg_2112" n="e" s="T107">ippɨmmɨntɨ. </ts>
               <ts e="T109" id="Seg_2114" n="e" s="T108">Təp </ts>
               <ts e="T110" id="Seg_2116" n="e" s="T109">nɨmtɨ </ts>
               <ts e="T111" id="Seg_2118" n="e" s="T110">ippɨmpa. </ts>
               <ts e="T112" id="Seg_2120" n="e" s="T111">Nɔːtɨ </ts>
               <ts e="T113" id="Seg_2122" n="e" s="T112">säːt </ts>
               <ts e="T114" id="Seg_2124" n="e" s="T113">orɨmkɨlɨmpa. </ts>
               <ts e="T115" id="Seg_2126" n="e" s="T114">Nɔːt </ts>
               <ts e="T116" id="Seg_2128" n="e" s="T115">čʼumpa </ts>
               <ts e="T117" id="Seg_2130" n="e" s="T116">soqɨtɨ, </ts>
               <ts e="T118" id="Seg_2132" n="e" s="T117">peːmɨtɨ. </ts>
               <ts e="T119" id="Seg_2134" n="e" s="T118">Ola </ts>
               <ts e="T120" id="Seg_2136" n="e" s="T119">sičʼi. </ts>
               <ts e="T121" id="Seg_2138" n="e" s="T120">Nɨːnɨ </ts>
               <ts e="T122" id="Seg_2140" n="e" s="T121">nʼennä </ts>
               <ts e="T123" id="Seg_2142" n="e" s="T122">qɨntalta, </ts>
               <ts e="T124" id="Seg_2144" n="e" s="T123">üːnɨmtɨ </ts>
               <ts e="T125" id="Seg_2146" n="e" s="T124">paŋɨsa </ts>
               <ts e="T126" id="Seg_2148" n="e" s="T125">matɨŋɨtɨ. </ts>
               <ts e="T127" id="Seg_2150" n="e" s="T126">Na </ts>
               <ts e="T128" id="Seg_2152" n="e" s="T127">nɔːkɨr </ts>
               <ts e="T129" id="Seg_2154" n="e" s="T128">quptɨtɨ </ts>
               <ts e="T130" id="Seg_2156" n="e" s="T129">nɨːnɨ </ts>
               <ts e="T131" id="Seg_2158" n="e" s="T130">pakta. </ts>
               <ts e="T132" id="Seg_2160" n="e" s="T131">Təp </ts>
               <ts e="T133" id="Seg_2162" n="e" s="T132">ontɨ </ts>
               <ts e="T134" id="Seg_2164" n="e" s="T133">nɨmtɨ </ts>
               <ts e="T135" id="Seg_2166" n="e" s="T134">ippɨmpa, </ts>
               <ts e="T136" id="Seg_2168" n="e" s="T135">ɔːmäj </ts>
               <ts e="T137" id="Seg_2170" n="e" s="T136">qontɨkkä. </ts>
               <ts e="T138" id="Seg_2172" n="e" s="T137">Nɨːnɨ </ts>
               <ts e="T139" id="Seg_2174" n="e" s="T138">taŋɨmpa </ts>
               <ts e="T140" id="Seg_2176" n="e" s="T139">namɨšak </ts>
               <ts e="T141" id="Seg_2178" n="e" s="T140">apsɨtqa. </ts>
               <ts e="T142" id="Seg_2180" n="e" s="T141">Qumpa, </ts>
               <ts e="T143" id="Seg_2182" n="e" s="T142">nop </ts>
               <ts e="T144" id="Seg_2184" n="e" s="T143">taŋɨmpa, </ts>
               <ts e="T145" id="Seg_2186" n="e" s="T144">qaqlɨntɨ </ts>
               <ts e="T146" id="Seg_2188" n="e" s="T145">šünčʼaqɨt </ts>
               <ts e="T147" id="Seg_2190" n="e" s="T146">qontɨkka. </ts>
               <ts e="T148" id="Seg_2192" n="e" s="T147">Namɨšalʼ </ts>
               <ts e="T149" id="Seg_2194" n="e" s="T148">ulqantɨ </ts>
               <ts e="T150" id="Seg_2196" n="e" s="T149">laka </ts>
               <ts e="T151" id="Seg_2198" n="e" s="T150">qalɨntɨ </ts>
               <ts e="T152" id="Seg_2200" n="e" s="T151">ɨlqɨt </ts>
               <ts e="T153" id="Seg_2202" n="e" s="T152">qalʼimmɨntɨ. </ts>
               <ts e="T154" id="Seg_2204" n="e" s="T153">Mora </ts>
               <ts e="T155" id="Seg_2206" n="e" s="T154">muntɨk </ts>
               <ts e="T156" id="Seg_2208" n="e" s="T155">čʼumpa. </ts>
               <ts e="T157" id="Seg_2210" n="e" s="T156">Kur, </ts>
               <ts e="T158" id="Seg_2212" n="e" s="T157">ukkur </ts>
               <ts e="T159" id="Seg_2214" n="e" s="T158">čʼontaqɨt </ts>
               <ts e="T160" id="Seg_2216" n="e" s="T159">täp </ts>
               <ts e="T161" id="Seg_2218" n="e" s="T160">qos </ts>
               <ts e="T162" id="Seg_2220" n="e" s="T161">qaj </ts>
               <ts e="T163" id="Seg_2222" n="e" s="T162">nılʼčʼik </ts>
               <ts e="T164" id="Seg_2224" n="e" s="T163">kətɨŋɨtɨ </ts>
               <ts e="T165" id="Seg_2226" n="e" s="T164">qaj </ts>
               <ts e="T166" id="Seg_2228" n="e" s="T165">nop </ts>
               <ts e="T167" id="Seg_2230" n="e" s="T166">lʼi </ts>
               <ts e="T168" id="Seg_2232" n="e" s="T167">qaj </ts>
               <ts e="T169" id="Seg_2234" n="e" s="T168">qup </ts>
               <ts e="T170" id="Seg_2236" n="e" s="T169">tämajsä </ts>
               <ts e="T171" id="Seg_2238" n="e" s="T170">kurulʼä </ts>
               <ts e="T172" id="Seg_2240" n="e" s="T171">alʼ </ts>
               <ts e="T173" id="Seg_2242" n="e" s="T172">nılʼčʼik </ts>
               <ts e="T174" id="Seg_2244" n="e" s="T173">kuka. </ts>
               <ts e="T175" id="Seg_2246" n="e" s="T174">Təp </ts>
               <ts e="T176" id="Seg_2248" n="e" s="T175">ınna </ts>
               <ts e="T177" id="Seg_2250" n="e" s="T176">sʼäp </ts>
               <ts e="T178" id="Seg_2252" n="e" s="T177">antalumpa, </ts>
               <ts e="T179" id="Seg_2254" n="e" s="T178">nılʼčʼik </ts>
               <ts e="T180" id="Seg_2256" n="e" s="T179">kuka. </ts>
               <ts e="T181" id="Seg_2258" n="e" s="T180">Kekkɨsa </ts>
               <ts e="T182" id="Seg_2260" n="e" s="T181">muːt </ts>
               <ts e="T183" id="Seg_2262" n="e" s="T182">tınolʼa </ts>
               <ts e="T184" id="Seg_2264" n="e" s="T183">atalimmɨntɔːtɨt. </ts>
               <ts e="T185" id="Seg_2266" n="e" s="T184">Na </ts>
               <ts e="T186" id="Seg_2268" n="e" s="T185">qaqlʼintɨ </ts>
               <ts e="T187" id="Seg_2270" n="e" s="T186">ilqɨlʼ </ts>
               <ts e="T188" id="Seg_2272" n="e" s="T187">ulqalʼ </ts>
               <ts e="T189" id="Seg_2274" n="e" s="T188">lakasä </ts>
               <ts e="T190" id="Seg_2276" n="e" s="T189">qompɨrlʼä. </ts>
               <ts e="T191" id="Seg_2278" n="e" s="T190">Nɨːnä </ts>
               <ts e="T192" id="Seg_2280" n="e" s="T191">aj </ts>
               <ts e="T193" id="Seg_2282" n="e" s="T192">ılla </ts>
               <ts e="T194" id="Seg_2284" n="e" s="T193">qonta. </ts>
               <ts e="T195" id="Seg_2286" n="e" s="T194">Aj </ts>
               <ts e="T196" id="Seg_2288" n="e" s="T195">täp </ts>
               <ts e="T197" id="Seg_2290" n="e" s="T196">qos </ts>
               <ts e="T198" id="Seg_2292" n="e" s="T197">qaj </ts>
               <ts e="T199" id="Seg_2294" n="e" s="T198">nılʼčʼik </ts>
               <ts e="T200" id="Seg_2296" n="e" s="T199">šıp </ts>
               <ts e="T201" id="Seg_2298" n="e" s="T200">soqočʼčʼi: </ts>
               <ts e="T202" id="Seg_2300" n="e" s="T201">Qup </ts>
               <ts e="T203" id="Seg_2302" n="e" s="T202">nılʼčʼik </ts>
               <ts e="T204" id="Seg_2304" n="e" s="T203">ıla </ts>
               <ts e="T205" id="Seg_2306" n="e" s="T204">kurola </ts>
               <ts e="T206" id="Seg_2308" n="e" s="T205">kuka. </ts>
               <ts e="T207" id="Seg_2310" n="e" s="T206">Nɨːnɨ </ts>
               <ts e="T208" id="Seg_2312" n="e" s="T207">qanɨqtɨ </ts>
               <ts e="T209" id="Seg_2314" n="e" s="T208">konna </ts>
               <ts e="T210" id="Seg_2316" n="e" s="T209">tanta </ts>
               <ts e="T211" id="Seg_2318" n="e" s="T210">(/tannɨmpa). </ts>
               <ts e="T212" id="Seg_2320" n="e" s="T211">Sajitɨ </ts>
               <ts e="T213" id="Seg_2322" n="e" s="T212">namɨšak </ts>
               <ts e="T214" id="Seg_2324" n="e" s="T213">awsɨtqa </ts>
               <ts e="T215" id="Seg_2326" n="e" s="T214">qumpa. </ts>
               <ts e="T216" id="Seg_2328" n="e" s="T215">Qɨlʼat </ts>
               <ts e="T217" id="Seg_2330" n="e" s="T216">šünʼantɨ </ts>
               <ts e="T218" id="Seg_2332" n="e" s="T217">tokɨmɔːtpa. </ts>
               <ts e="T219" id="Seg_2334" n="e" s="T218">Ukkur </ts>
               <ts e="T220" id="Seg_2336" n="e" s="T219">čʼontaqɨt </ts>
               <ts e="T221" id="Seg_2338" n="e" s="T220">nılʼčʼik </ts>
               <ts e="T222" id="Seg_2340" n="e" s="T221">ɛːsa. </ts>
               <ts e="T223" id="Seg_2342" n="e" s="T222">Mitä </ts>
               <ts e="T224" id="Seg_2344" n="e" s="T223">tətɨ </ts>
               <ts e="T225" id="Seg_2346" n="e" s="T224">atäntɨ </ts>
               <ts e="T226" id="Seg_2348" n="e" s="T225">(/mittɨ </ts>
               <ts e="T227" id="Seg_2350" n="e" s="T226">təttɨ </ts>
               <ts e="T228" id="Seg_2352" n="e" s="T227">čʼurɨntɨ) </ts>
               <ts e="T229" id="Seg_2354" n="e" s="T228">morat </ts>
               <ts e="T230" id="Seg_2356" n="e" s="T229">qanɨqqɨt. </ts>
               <ts e="T231" id="Seg_2358" n="e" s="T230">Tep </ts>
               <ts e="T232" id="Seg_2360" n="e" s="T231">namɨsʼak </ts>
               <ts e="T233" id="Seg_2362" n="e" s="T232">apsɨtqa </ts>
               <ts e="T234" id="Seg_2364" n="e" s="T233">qumpa. </ts>
               <ts e="T235" id="Seg_2366" n="e" s="T234">Nɨːnɨ </ts>
               <ts e="T236" id="Seg_2368" n="e" s="T235">nılʼčʼik </ts>
               <ts e="T237" id="Seg_2370" n="e" s="T236">sʼäp </ts>
               <ts e="T238" id="Seg_2372" n="e" s="T237">qäŋŋa: </ts>
               <ts e="T239" id="Seg_2374" n="e" s="T238">qaqlʼap </ts>
               <ts e="T240" id="Seg_2376" n="e" s="T239">tətɨn </ts>
               <ts e="T241" id="Seg_2378" n="e" s="T240">mənatɨ, </ts>
               <ts e="T242" id="Seg_2380" n="e" s="T241">tulispa. </ts>
               <ts e="T243" id="Seg_2382" n="e" s="T242">Nɨːnɨ </ts>
               <ts e="T244" id="Seg_2384" n="e" s="T243">qaqlɨntɨ </ts>
               <ts e="T245" id="Seg_2386" n="e" s="T244">moqalit </ts>
               <ts e="T246" id="Seg_2388" n="e" s="T245">iːmɨt </ts>
               <ts e="T247" id="Seg_2390" n="e" s="T246">soqolʼä </ts>
               <ts e="T248" id="Seg_2392" n="e" s="T247">kunna </ts>
               <ts e="T249" id="Seg_2394" n="e" s="T248">tanta. </ts>
               <ts e="T250" id="Seg_2396" n="e" s="T249">Nɨːnɨ </ts>
               <ts e="T251" id="Seg_2398" n="e" s="T250">konna </ts>
               <ts e="T252" id="Seg_2400" n="e" s="T251">qanɨqtɨ </ts>
               <ts e="T253" id="Seg_2402" n="e" s="T252">alʼčʼe. </ts>
               <ts e="T254" id="Seg_2404" n="e" s="T253">Nɨːnɨ </ts>
               <ts e="T255" id="Seg_2406" n="e" s="T254">nɨmtɨ </ts>
               <ts e="T256" id="Seg_2408" n="e" s="T255">ippa. </ts>
               <ts e="T257" id="Seg_2410" n="e" s="T256">Aj </ts>
               <ts e="T258" id="Seg_2412" n="e" s="T257">qos </ts>
               <ts e="T259" id="Seg_2414" n="e" s="T258">qaj </ts>
               <ts e="T260" id="Seg_2416" n="e" s="T259">nılʼčʼik </ts>
               <ts e="T261" id="Seg_2418" n="e" s="T260">kətɨŋɨtɨ: </ts>
               <ts e="T262" id="Seg_2420" n="e" s="T261">Qup </ts>
               <ts e="T263" id="Seg_2422" n="e" s="T262">təmasä </ts>
               <ts e="T264" id="Seg_2424" n="e" s="T263">kurɨla </ts>
               <ts e="T265" id="Seg_2426" n="e" s="T264">qupna. </ts>
               <ts e="T266" id="Seg_2428" n="e" s="T265">Konnat </ts>
               <ts e="T267" id="Seg_2430" n="e" s="T266">nʼärqɨlʼ </ts>
               <ts e="T268" id="Seg_2432" n="e" s="T267">markɨt </ts>
               <ts e="T269" id="Seg_2434" n="e" s="T268">toːpɨntɨ </ts>
               <ts e="T270" id="Seg_2436" n="e" s="T269">soqɨlʼa </ts>
               <ts e="T271" id="Seg_2438" n="e" s="T270">konna </ts>
               <ts e="T272" id="Seg_2440" n="e" s="T271">tanta. </ts>
               <ts e="T273" id="Seg_2442" n="e" s="T272">Nɨːna </ts>
               <ts e="T274" id="Seg_2444" n="e" s="T273">konna </ts>
               <ts e="T275" id="Seg_2446" n="e" s="T274">soqqolʼa </ts>
               <ts e="T276" id="Seg_2448" n="e" s="T275">tanta. </ts>
               <ts e="T277" id="Seg_2450" n="e" s="T276">Mat </ts>
               <ts e="T278" id="Seg_2452" n="e" s="T277">qotä </ts>
               <ts e="T279" id="Seg_2454" n="e" s="T278">ınna </ts>
               <ts e="T280" id="Seg_2456" n="e" s="T279">nɨlaıːsäŋ </ts>
               <ts e="T281" id="Seg_2458" n="e" s="T280">ɛna. </ts>
               <ts e="T282" id="Seg_2460" n="e" s="T281">Konna </ts>
               <ts e="T283" id="Seg_2462" n="e" s="T282">soqqolʼa </ts>
               <ts e="T284" id="Seg_2464" n="e" s="T283">tanta. </ts>
               <ts e="T285" id="Seg_2466" n="e" s="T284">Ortɨ </ts>
               <ts e="T286" id="Seg_2468" n="e" s="T285">meːltɨ </ts>
               <ts e="T287" id="Seg_2470" n="e" s="T286">čʼäːŋka. </ts>
               <ts e="T288" id="Seg_2472" n="e" s="T287">Aj </ts>
               <ts e="T289" id="Seg_2474" n="e" s="T288">qos </ts>
               <ts e="T290" id="Seg_2476" n="e" s="T289">qaj </ts>
               <ts e="T291" id="Seg_2478" n="e" s="T290">nılʼčʼik </ts>
               <ts e="T292" id="Seg_2480" n="e" s="T291">soqalʼlʼitɨ: </ts>
               <ts e="T293" id="Seg_2482" n="e" s="T292">Qup </ts>
               <ts e="T294" id="Seg_2484" n="e" s="T293">aša </ts>
               <ts e="T295" id="Seg_2486" n="e" s="T294">nılʼčʼik </ts>
               <ts e="T296" id="Seg_2488" n="e" s="T295">kuka </ts>
               <ts e="T297" id="Seg_2490" n="e" s="T296">kurolʼa. </ts>
               <ts e="T298" id="Seg_2492" n="e" s="T297">Na </ts>
               <ts e="T299" id="Seg_2494" n="e" s="T298">morat </ts>
               <ts e="T300" id="Seg_2496" n="e" s="T299">qanɨqqät </ts>
               <ts e="T301" id="Seg_2498" n="e" s="T300">pirqə </ts>
               <ts e="T302" id="Seg_2500" n="e" s="T301">təttɨ. </ts>
               <ts e="T303" id="Seg_2502" n="e" s="T302">İnna </ts>
               <ts e="T304" id="Seg_2504" n="e" s="T303">sɨqolʼna. </ts>
               <ts e="T305" id="Seg_2506" n="e" s="T304">Nɨːnɨ </ts>
               <ts e="T306" id="Seg_2508" n="e" s="T305">konna </ts>
               <ts e="T307" id="Seg_2510" n="e" s="T306">sɨqɨlʼä </ts>
               <ts e="T308" id="Seg_2512" n="e" s="T307">nılʼčʼik </ts>
               <ts e="T309" id="Seg_2514" n="e" s="T308">mannɨmpa. </ts>
               <ts e="T310" id="Seg_2516" n="e" s="T309">Konnat </ts>
               <ts e="T311" id="Seg_2518" n="e" s="T310">nılʼčʼik </ts>
               <ts e="T312" id="Seg_2520" n="e" s="T311">qoŋɨtɨ: </ts>
               <ts e="T313" id="Seg_2522" n="e" s="T312">kotɨlʼ </ts>
               <ts e="T314" id="Seg_2524" n="e" s="T313">mɔːt </ts>
               <ts e="T315" id="Seg_2526" n="e" s="T314">ɔːmnɨntɨ. </ts>
               <ts e="T316" id="Seg_2528" n="e" s="T315">Nɨːnɨ </ts>
               <ts e="T317" id="Seg_2530" n="e" s="T316">aj </ts>
               <ts e="T318" id="Seg_2532" n="e" s="T317">ılla </ts>
               <ts e="T319" id="Seg_2534" n="e" s="T318">olʼčʼeja. </ts>
               <ts e="T320" id="Seg_2536" n="e" s="T319">Kutar </ts>
               <ts e="T321" id="Seg_2538" n="e" s="T320">nɨčʼa </ts>
               <ts e="T322" id="Seg_2540" n="e" s="T321">tuličʼčʼantɨk? </ts>
               <ts e="T323" id="Seg_2542" n="e" s="T322">Nɨːnɨ </ts>
               <ts e="T324" id="Seg_2544" n="e" s="T323">na </ts>
               <ts e="T325" id="Seg_2546" n="e" s="T324">mɔːttɨ </ts>
               <ts e="T326" id="Seg_2548" n="e" s="T325">konna </ts>
               <ts e="T327" id="Seg_2550" n="e" s="T326">nɨlʼlʼa </ts>
               <ts e="T328" id="Seg_2552" n="e" s="T327">ılla </ts>
               <ts e="T329" id="Seg_2554" n="e" s="T328">tüŋa. </ts>
               <ts e="T330" id="Seg_2556" n="e" s="T329">Qälʼi </ts>
               <ts e="T331" id="Seg_2558" n="e" s="T330">mɔːt </ts>
               <ts e="T332" id="Seg_2560" n="e" s="T331">ɔːmnɨntɨ. </ts>
               <ts e="T333" id="Seg_2562" n="e" s="T332">Ɔːtatɨ </ts>
               <ts e="T334" id="Seg_2564" n="e" s="T333">etɨmantoːqɨt </ts>
               <ts e="T335" id="Seg_2566" n="e" s="T334">kuralimmɨntɔːtɨt. </ts>
               <ts e="T336" id="Seg_2568" n="e" s="T335">Nɨːnɨ </ts>
               <ts e="T337" id="Seg_2570" n="e" s="T336">mɔːttɨ </ts>
               <ts e="T338" id="Seg_2572" n="e" s="T337">šerna. </ts>
               <ts e="T339" id="Seg_2574" n="e" s="T338">Ira </ts>
               <ts e="T340" id="Seg_2576" n="e" s="T339">ukkur </ts>
               <ts e="T341" id="Seg_2578" n="e" s="T340">nälʼatɨ. </ts>
               <ts e="T342" id="Seg_2580" n="e" s="T341">Ukkur </ts>
               <ts e="T343" id="Seg_2582" n="e" s="T342">čʼontoːqɨt </ts>
               <ts e="T521" id="Seg_2584" n="e" s="T343">qäl </ts>
               <ts e="T344" id="Seg_2586" n="e" s="T521">ira </ts>
               <ts e="T345" id="Seg_2588" n="e" s="T344">nılʼčʼik </ts>
               <ts e="T346" id="Seg_2590" n="e" s="T345">ɛsa: </ts>
               <ts e="T347" id="Seg_2592" n="e" s="T346">Tat </ts>
               <ts e="T348" id="Seg_2594" n="e" s="T347">na </ts>
               <ts e="T349" id="Seg_2596" n="e" s="T348">qup </ts>
               <ts e="T350" id="Seg_2598" n="e" s="T349">awstäti. </ts>
               <ts e="T351" id="Seg_2600" n="e" s="T350">Täp </ts>
               <ts e="T352" id="Seg_2602" n="e" s="T351">na </ts>
               <ts e="T353" id="Seg_2604" n="e" s="T352">iran </ts>
               <ts e="T354" id="Seg_2606" n="e" s="T353">mɨqɨt </ts>
               <ts e="T355" id="Seg_2608" n="e" s="T354">šitɨ </ts>
               <ts e="T356" id="Seg_2610" n="e" s="T355">toːt </ts>
               <ts e="T357" id="Seg_2612" n="e" s="T356">ɔːtat. </ts>
               <ts e="T358" id="Seg_2614" n="e" s="T357">Mat </ts>
               <ts e="T359" id="Seg_2616" n="e" s="T358">ukkur </ts>
               <ts e="T360" id="Seg_2618" n="e" s="T359">toːt </ts>
               <ts e="T361" id="Seg_2620" n="e" s="T360">ɔːtamɨ </ts>
               <ts e="T362" id="Seg_2622" n="e" s="T361">suːrup </ts>
               <ts e="T363" id="Seg_2624" n="e" s="T362">muntɨk </ts>
               <ts e="T364" id="Seg_2626" n="e" s="T363">amnɨtɨ,– </ts>
               <ts e="T365" id="Seg_2628" n="e" s="T364">ira </ts>
               <ts e="T366" id="Seg_2630" n="e" s="T365">nılʼčʼik </ts>
               <ts e="T367" id="Seg_2632" n="e" s="T366">kətɨŋɨt. </ts>
               <ts e="T368" id="Seg_2634" n="e" s="T367">Tat </ts>
               <ts e="T369" id="Seg_2636" n="e" s="T368">pona </ts>
               <ts e="T370" id="Seg_2638" n="e" s="T369">tantɨlʼä </ts>
               <ts e="T371" id="Seg_2640" n="e" s="T370">mat </ts>
               <ts e="T372" id="Seg_2642" n="e" s="T371">ɔːtamɨ </ts>
               <ts e="T373" id="Seg_2644" n="e" s="T372">ɔːtätɨ. </ts>
               <ts e="T374" id="Seg_2646" n="e" s="T373">Lʼupkɨmɔːnnä. </ts>
               <ts e="T375" id="Seg_2648" n="e" s="T374">Täp </ts>
               <ts e="T376" id="Seg_2650" n="e" s="T375">pona </ts>
               <ts e="T377" id="Seg_2652" n="e" s="T376">tanta. </ts>
               <ts e="T378" id="Seg_2654" n="e" s="T377">Täp </ts>
               <ts e="T379" id="Seg_2656" n="e" s="T378">pona </ts>
               <ts e="T380" id="Seg_2658" n="e" s="T379">tanta, </ts>
               <ts e="T381" id="Seg_2660" n="e" s="T380">namɨšak </ts>
               <ts e="T382" id="Seg_2662" n="e" s="T381">seqelʼimpɨ </ts>
               <ts e="T383" id="Seg_2664" n="e" s="T382">qup, </ts>
               <ts e="T384" id="Seg_2666" n="e" s="T383">qajtɨ </ts>
               <ts e="T385" id="Seg_2668" n="e" s="T384">mannɨmmɛntɨt? </ts>
               <ts e="T386" id="Seg_2670" n="e" s="T385">Nɨːnɨ </ts>
               <ts e="T387" id="Seg_2672" n="e" s="T386">täp </ts>
               <ts e="T388" id="Seg_2674" n="e" s="T387">aj </ts>
               <ts e="T389" id="Seg_2676" n="e" s="T388">ılla </ts>
               <ts e="T390" id="Seg_2678" n="e" s="T389">alʼčʼimpa. </ts>
               <ts e="T391" id="Seg_2680" n="e" s="T390">Nɨmtɨ </ts>
               <ts e="T392" id="Seg_2682" n="e" s="T391">na </ts>
               <ts e="T393" id="Seg_2684" n="e" s="T392">ippɨmmɨntɨ. </ts>
               <ts e="T394" id="Seg_2686" n="e" s="T393">Na </ts>
               <ts e="T395" id="Seg_2688" n="e" s="T394">täpɨt </ts>
               <ts e="T396" id="Seg_2690" n="e" s="T395">tülʼä </ts>
               <ts e="T397" id="Seg_2692" n="e" s="T396">puːla </ts>
               <ts e="T398" id="Seg_2694" n="e" s="T397">suːrup </ts>
               <ts e="T399" id="Seg_2696" n="e" s="T398">na </ts>
               <ts e="T400" id="Seg_2698" n="e" s="T399">qənmɨntɨ. </ts>
               <ts e="T401" id="Seg_2700" n="e" s="T400">Nɨː </ts>
               <ts e="T402" id="Seg_2702" n="e" s="T401">ukoːt </ts>
               <ts e="T403" id="Seg_2704" n="e" s="T402">äsäsɨqäːqi </ts>
               <ts e="T404" id="Seg_2706" n="e" s="T403">ɔːtantij </ts>
               <ts e="T405" id="Seg_2708" n="e" s="T404">ɔːtɨlä </ts>
               <ts e="T406" id="Seg_2710" n="e" s="T405">poːqot </ts>
               <ts e="T407" id="Seg_2712" n="e" s="T406">ilimpɔːqij. </ts>
               <ts e="T408" id="Seg_2714" n="e" s="T407">Nɨːnɨ </ts>
               <ts e="T409" id="Seg_2716" n="e" s="T408">na </ts>
               <ts e="T410" id="Seg_2718" n="e" s="T409">qumup </ts>
               <ts e="T411" id="Seg_2720" n="e" s="T410">awstɨmpɨlʼä </ts>
               <ts e="T412" id="Seg_2722" n="e" s="T411">nɨmtɨ </ts>
               <ts e="T413" id="Seg_2724" n="e" s="T412">na </ts>
               <ts e="T414" id="Seg_2726" n="e" s="T413">illemmɨntɨ. </ts>
               <ts e="T415" id="Seg_2728" n="e" s="T414">Nɨːnä </ts>
               <ts e="T416" id="Seg_2730" n="e" s="T415">na </ts>
               <ts e="T417" id="Seg_2732" n="e" s="T416">ire </ts>
               <ts e="T418" id="Seg_2734" n="e" s="T417">aj </ts>
               <ts e="T419" id="Seg_2736" n="e" s="T418">nılʼčʼik </ts>
               <ts e="T420" id="Seg_2738" n="e" s="T419">kətɨŋɨtɨ: </ts>
               <ts e="T421" id="Seg_2740" n="e" s="T420">Aj </ts>
               <ts e="T422" id="Seg_2742" n="e" s="T421">qəlasik </ts>
               <ts e="T423" id="Seg_2744" n="e" s="T422">ɔːtamtɨ </ts>
               <ts e="T424" id="Seg_2746" n="e" s="T423">ɔːtɨlʼä. </ts>
               <ts e="T425" id="Seg_2748" n="e" s="T424">Ɔːtatɨ </ts>
               <ts e="T426" id="Seg_2750" n="e" s="T425">qapije </ts>
               <ts e="T427" id="Seg_2752" n="e" s="T426">mačʼantɨ </ts>
               <ts e="T428" id="Seg_2754" n="e" s="T427">asʼa </ts>
               <ts e="T429" id="Seg_2756" n="e" s="T428">qənta. </ts>
               <ts e="T430" id="Seg_2758" n="e" s="T429">Mɔːtɨt </ts>
               <ts e="T431" id="Seg_2760" n="e" s="T430">qanɨqqɨt </ts>
               <ts e="T432" id="Seg_2762" n="e" s="T431">qontɔːtɨt. </ts>
               <ts e="T433" id="Seg_2764" n="e" s="T432">Nɨːnä </ts>
               <ts e="T434" id="Seg_2766" n="e" s="T433">täp </ts>
               <ts e="T435" id="Seg_2768" n="e" s="T434">pona </ts>
               <ts e="T436" id="Seg_2770" n="e" s="T435">tanta, </ts>
               <ts e="T437" id="Seg_2772" n="e" s="T436">seːpɨlʼalʼ </ts>
               <ts e="T438" id="Seg_2774" n="e" s="T437">mɨntɨ </ts>
               <ts e="T439" id="Seg_2776" n="e" s="T438">qəlla </ts>
               <ts e="T440" id="Seg_2778" n="e" s="T439">aj </ts>
               <ts e="T441" id="Seg_2780" n="e" s="T440">ılla </ts>
               <ts e="T442" id="Seg_2782" n="e" s="T441">olʼčʼi. </ts>
               <ts e="T443" id="Seg_2784" n="e" s="T442">Nɨːnɨ </ts>
               <ts e="T444" id="Seg_2786" n="e" s="T443">təp </ts>
               <ts e="T445" id="Seg_2788" n="e" s="T444">ukur </ts>
               <ts e="T446" id="Seg_2790" n="e" s="T445">čʼontoːqɨt </ts>
               <ts e="T447" id="Seg_2792" n="e" s="T446">nılʼčʼik </ts>
               <ts e="T448" id="Seg_2794" n="e" s="T447">šitɨje. </ts>
               <ts e="T449" id="Seg_2796" n="e" s="T448">Qaj </ts>
               <ts e="T450" id="Seg_2798" n="e" s="T449">qəntɨtɨ? </ts>
               <ts e="T451" id="Seg_2800" n="e" s="T450">Montɨ </ts>
               <ts e="T452" id="Seg_2802" n="e" s="T451">ilɛčʼa. </ts>
               <ts e="T453" id="Seg_2804" n="e" s="T452">Nɨːnɨ </ts>
               <ts e="T454" id="Seg_2806" n="e" s="T453">moqona </ts>
               <ts e="T455" id="Seg_2808" n="e" s="T454">tüŋa. </ts>
               <ts e="T456" id="Seg_2810" n="e" s="T455">Mɔːttɨ </ts>
               <ts e="T522" id="Seg_2812" n="e" s="T456">qäli </ts>
               <ts e="T457" id="Seg_2814" n="e" s="T522">ira </ts>
               <ts e="T458" id="Seg_2816" n="e" s="T457">qonta. </ts>
               <ts e="T459" id="Seg_2818" n="e" s="T458">Nɨːnɨ </ts>
               <ts e="T460" id="Seg_2820" n="e" s="T459">təpɨtɨt </ts>
               <ts e="T461" id="Seg_2822" n="e" s="T460">ɔːta </ts>
               <ts e="T462" id="Seg_2824" n="e" s="T461">meːltɨ </ts>
               <ts e="T463" id="Seg_2826" n="e" s="T462">etimontoːqɨt </ts>
               <ts e="T464" id="Seg_2828" n="e" s="T463">qontɔːtɨt. </ts>
               <ts e="T465" id="Seg_2830" n="e" s="T464">Təp </ts>
               <ts e="T466" id="Seg_2832" n="e" s="T465">na </ts>
               <ts e="T467" id="Seg_2834" n="e" s="T466">tüpilʼ </ts>
               <ts e="T468" id="Seg_2836" n="e" s="T467">porqɨlʼ </ts>
               <ts e="T469" id="Seg_2838" n="e" s="T468">peːmɨtɨsa </ts>
               <ts e="T470" id="Seg_2840" n="e" s="T469">ila. </ts>
               <ts e="T471" id="Seg_2842" n="e" s="T470">Nɨːnɨ </ts>
               <ts e="T472" id="Seg_2844" n="e" s="T471">nɔːkɨrɨmtälʼi </ts>
               <ts e="T473" id="Seg_2846" n="e" s="T472">pit </ts>
               <ts e="T474" id="Seg_2848" n="e" s="T473">nılʼčʼik </ts>
               <ts e="T475" id="Seg_2850" n="e" s="T474">ɛsa. </ts>
               <ts e="T476" id="Seg_2852" n="e" s="T475">Mitɨ </ts>
               <ts e="T477" id="Seg_2854" n="e" s="T476">mol </ts>
               <ts e="T478" id="Seg_2856" n="e" s="T477">somak </ts>
               <ts e="T479" id="Seg_2858" n="e" s="T478">ɛsak. </ts>
               <ts e="T523" id="Seg_2860" n="e" s="T479">Qäli </ts>
               <ts e="T480" id="Seg_2862" n="e" s="T523">ira </ts>
               <ts e="T481" id="Seg_2864" n="e" s="T480">ɔːtatɨ </ts>
               <ts e="T482" id="Seg_2866" n="e" s="T481">šöttɨ </ts>
               <ts e="T483" id="Seg_2868" n="e" s="T482">asʼa </ts>
               <ts e="T484" id="Seg_2870" n="e" s="T483">qənta. </ts>
               <ts e="T485" id="Seg_2872" n="e" s="T484">Mɔːtɨt </ts>
               <ts e="T486" id="Seg_2874" n="e" s="T485">qanaqqɨt </ts>
               <ts e="T487" id="Seg_2876" n="e" s="T486">nɨmtɨ </ts>
               <ts e="T488" id="Seg_2878" n="e" s="T487">qəntɔːtɨt. </ts>
               <ts e="T489" id="Seg_2880" n="e" s="T488">Täp </ts>
               <ts e="T490" id="Seg_2882" n="e" s="T489">tüla </ts>
               <ts e="T491" id="Seg_2884" n="e" s="T490">aj </ts>
               <ts e="T492" id="Seg_2886" n="e" s="T491">ılla </ts>
               <ts e="T493" id="Seg_2888" n="e" s="T492">qontaje. </ts>
               <ts e="T494" id="Seg_2890" n="e" s="T493">Tɔːptɨlʼ </ts>
               <ts e="T495" id="Seg_2892" n="e" s="T494">qarɨt </ts>
               <ts e="T496" id="Seg_2894" n="e" s="T495">montɨ </ts>
               <ts e="T497" id="Seg_2896" n="e" s="T496">sɨrɨ </ts>
               <ts e="T498" id="Seg_2898" n="e" s="T497">pinnaıːntɨtɨ. </ts>
               <ts e="T499" id="Seg_2900" n="e" s="T498">Mat, </ts>
               <ts e="T500" id="Seg_2902" n="e" s="T499">täp </ts>
               <ts e="T501" id="Seg_2904" n="e" s="T500">kuttar </ts>
               <ts e="T502" id="Seg_2906" n="e" s="T501">tüsa, </ts>
               <ts e="T503" id="Seg_2908" n="e" s="T502">ɔːtamɨ </ts>
               <ts e="T504" id="Seg_2910" n="e" s="T503">šöttɨ </ts>
               <ts e="T505" id="Seg_2912" n="e" s="T504">aša </ts>
               <ts e="T506" id="Seg_2914" n="e" s="T505">qənta. </ts>
               <ts e="T507" id="Seg_2916" n="e" s="T506">Kəraltättɨ, </ts>
               <ts e="T508" id="Seg_2918" n="e" s="T507">sɨrɨp </ts>
               <ts e="T509" id="Seg_2920" n="e" s="T508">pilʼlʼa </ts>
               <ts e="T510" id="Seg_2922" n="e" s="T509">qəŋaje. </ts>
               <ts e="T511" id="Seg_2924" n="e" s="T510">Somak </ts>
               <ts e="T512" id="Seg_2926" n="e" s="T511">ılla </ts>
               <ts e="T513" id="Seg_2928" n="e" s="T512">na </ts>
               <ts e="T514" id="Seg_2930" n="e" s="T513">qəŋtɨ. </ts>
               <ts e="T515" id="Seg_2932" n="e" s="T514">Täp </ts>
               <ts e="T516" id="Seg_2934" n="e" s="T515">nɔːtɨ </ts>
               <ts e="T517" id="Seg_2936" n="e" s="T516">somak </ts>
               <ts e="T518" id="Seg_2938" n="e" s="T517">ɛsa. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T11" id="Seg_2939" s="T0">NEP_1965_NenetsAndWhiteBear1_flk.001 (001.001)</ta>
            <ta e="T19" id="Seg_2940" s="T11">NEP_1965_NenetsAndWhiteBear1_flk.002 (001.002)</ta>
            <ta e="T24" id="Seg_2941" s="T19">NEP_1965_NenetsAndWhiteBear1_flk.003 (001.003)</ta>
            <ta e="T29" id="Seg_2942" s="T24">NEP_1965_NenetsAndWhiteBear1_flk.004 (001.004)</ta>
            <ta e="T39" id="Seg_2943" s="T29">NEP_1965_NenetsAndWhiteBear1_flk.005 (002.001)</ta>
            <ta e="T41" id="Seg_2944" s="T39">NEP_1965_NenetsAndWhiteBear1_flk.006 (002.002)</ta>
            <ta e="T46" id="Seg_2945" s="T41">NEP_1965_NenetsAndWhiteBear1_flk.007 (002.003)</ta>
            <ta e="T56" id="Seg_2946" s="T46">NEP_1965_NenetsAndWhiteBear1_flk.008 (002.004)</ta>
            <ta e="T63" id="Seg_2947" s="T56">NEP_1965_NenetsAndWhiteBear1_flk.009 (002.005)</ta>
            <ta e="T67" id="Seg_2948" s="T63">NEP_1965_NenetsAndWhiteBear1_flk.010 (002.006)</ta>
            <ta e="T74" id="Seg_2949" s="T67">NEP_1965_NenetsAndWhiteBear1_flk.011 (002.007)</ta>
            <ta e="T78" id="Seg_2950" s="T74">NEP_1965_NenetsAndWhiteBear1_flk.012 (002.008)</ta>
            <ta e="T87" id="Seg_2951" s="T78">NEP_1965_NenetsAndWhiteBear1_flk.013 (002.009)</ta>
            <ta e="T95" id="Seg_2952" s="T87">NEP_1965_NenetsAndWhiteBear1_flk.014 (003.001)</ta>
            <ta e="T97" id="Seg_2953" s="T95">NEP_1965_NenetsAndWhiteBear1_flk.015 (003.002)</ta>
            <ta e="T101" id="Seg_2954" s="T97">NEP_1965_NenetsAndWhiteBear1_flk.016 (003.003)</ta>
            <ta e="T108" id="Seg_2955" s="T101">NEP_1965_NenetsAndWhiteBear1_flk.017 (003.004)</ta>
            <ta e="T111" id="Seg_2956" s="T108">NEP_1965_NenetsAndWhiteBear1_flk.018 (003.005)</ta>
            <ta e="T114" id="Seg_2957" s="T111">NEP_1965_NenetsAndWhiteBear1_flk.019 (003.006)</ta>
            <ta e="T118" id="Seg_2958" s="T114">NEP_1965_NenetsAndWhiteBear1_flk.020 (003.007)</ta>
            <ta e="T120" id="Seg_2959" s="T118">NEP_1965_NenetsAndWhiteBear1_flk.021 (003.008)</ta>
            <ta e="T126" id="Seg_2960" s="T120">NEP_1965_NenetsAndWhiteBear1_flk.022 (003.009)</ta>
            <ta e="T131" id="Seg_2961" s="T126">NEP_1965_NenetsAndWhiteBear1_flk.023 (003.010)</ta>
            <ta e="T137" id="Seg_2962" s="T131">NEP_1965_NenetsAndWhiteBear1_flk.024 (003.011)</ta>
            <ta e="T141" id="Seg_2963" s="T137">NEP_1965_NenetsAndWhiteBear1_flk.025 (003.012)</ta>
            <ta e="T147" id="Seg_2964" s="T141">NEP_1965_NenetsAndWhiteBear1_flk.026 (003.013)</ta>
            <ta e="T153" id="Seg_2965" s="T147">NEP_1965_NenetsAndWhiteBear1_flk.027 (003.014)</ta>
            <ta e="T156" id="Seg_2966" s="T153">NEP_1965_NenetsAndWhiteBear1_flk.028 (003.015)</ta>
            <ta e="T174" id="Seg_2967" s="T156">NEP_1965_NenetsAndWhiteBear1_flk.029 (003.016)</ta>
            <ta e="T180" id="Seg_2968" s="T174">NEP_1965_NenetsAndWhiteBear1_flk.030 (003.017)</ta>
            <ta e="T184" id="Seg_2969" s="T180">NEP_1965_NenetsAndWhiteBear1_flk.031 (003.018)</ta>
            <ta e="T190" id="Seg_2970" s="T184">NEP_1965_NenetsAndWhiteBear1_flk.032 (003.019)</ta>
            <ta e="T194" id="Seg_2971" s="T190">NEP_1965_NenetsAndWhiteBear1_flk.033 (003.020)</ta>
            <ta e="T206" id="Seg_2972" s="T194">NEP_1965_NenetsAndWhiteBear1_flk.034 (003.021)</ta>
            <ta e="T211" id="Seg_2973" s="T206">NEP_1965_NenetsAndWhiteBear1_flk.035 (003.022)</ta>
            <ta e="T215" id="Seg_2974" s="T211">NEP_1965_NenetsAndWhiteBear1_flk.036 (003.023)</ta>
            <ta e="T218" id="Seg_2975" s="T215">NEP_1965_NenetsAndWhiteBear1_flk.037 (003.024)</ta>
            <ta e="T222" id="Seg_2976" s="T218">NEP_1965_NenetsAndWhiteBear1_flk.038 (003.025)</ta>
            <ta e="T230" id="Seg_2977" s="T222">NEP_1965_NenetsAndWhiteBear1_flk.039 (003.026)</ta>
            <ta e="T234" id="Seg_2978" s="T230">NEP_1965_NenetsAndWhiteBear1_flk.040 (003.027)</ta>
            <ta e="T242" id="Seg_2979" s="T234">NEP_1965_NenetsAndWhiteBear1_flk.041 (003.028)</ta>
            <ta e="T249" id="Seg_2980" s="T242">NEP_1965_NenetsAndWhiteBear1_flk.042 (003.029)</ta>
            <ta e="T253" id="Seg_2981" s="T249">NEP_1965_NenetsAndWhiteBear1_flk.043 (003.030)</ta>
            <ta e="T256" id="Seg_2982" s="T253">NEP_1965_NenetsAndWhiteBear1_flk.044 (003.031)</ta>
            <ta e="T265" id="Seg_2983" s="T256">NEP_1965_NenetsAndWhiteBear1_flk.045 (003.032)</ta>
            <ta e="T272" id="Seg_2984" s="T265">NEP_1965_NenetsAndWhiteBear1_flk.046 (003.033)</ta>
            <ta e="T276" id="Seg_2985" s="T272">NEP_1965_NenetsAndWhiteBear1_flk.047 (003.034)</ta>
            <ta e="T281" id="Seg_2986" s="T276">NEP_1965_NenetsAndWhiteBear1_flk.048 (003.035)</ta>
            <ta e="T284" id="Seg_2987" s="T281">NEP_1965_NenetsAndWhiteBear1_flk.049 (003.036)</ta>
            <ta e="T287" id="Seg_2988" s="T284">NEP_1965_NenetsAndWhiteBear1_flk.050 (003.037)</ta>
            <ta e="T297" id="Seg_2989" s="T287">NEP_1965_NenetsAndWhiteBear1_flk.051 (003.038)</ta>
            <ta e="T302" id="Seg_2990" s="T297">NEP_1965_NenetsAndWhiteBear1_flk.052 (003.039)</ta>
            <ta e="T304" id="Seg_2991" s="T302">NEP_1965_NenetsAndWhiteBear1_flk.053 (003.040)</ta>
            <ta e="T309" id="Seg_2992" s="T304">NEP_1965_NenetsAndWhiteBear1_flk.054 (003.041)</ta>
            <ta e="T315" id="Seg_2993" s="T309">NEP_1965_NenetsAndWhiteBear1_flk.055 (003.042)</ta>
            <ta e="T319" id="Seg_2994" s="T315">NEP_1965_NenetsAndWhiteBear1_flk.056 (003.043)</ta>
            <ta e="T322" id="Seg_2995" s="T319">NEP_1965_NenetsAndWhiteBear1_flk.057 (003.044)</ta>
            <ta e="T329" id="Seg_2996" s="T322">NEP_1965_NenetsAndWhiteBear1_flk.058 (003.045)</ta>
            <ta e="T332" id="Seg_2997" s="T329">NEP_1965_NenetsAndWhiteBear1_flk.059 (003.046)</ta>
            <ta e="T335" id="Seg_2998" s="T332">NEP_1965_NenetsAndWhiteBear1_flk.060 (003.047)</ta>
            <ta e="T338" id="Seg_2999" s="T335">NEP_1965_NenetsAndWhiteBear1_flk.061 (003.048)</ta>
            <ta e="T341" id="Seg_3000" s="T338">NEP_1965_NenetsAndWhiteBear1_flk.062 (004.001)</ta>
            <ta e="T350" id="Seg_3001" s="T341">NEP_1965_NenetsAndWhiteBear1_flk.063 (004.002)</ta>
            <ta e="T357" id="Seg_3002" s="T350">NEP_1965_NenetsAndWhiteBear1_flk.064 (004.003)</ta>
            <ta e="T367" id="Seg_3003" s="T357">NEP_1965_NenetsAndWhiteBear1_flk.065 (004.004)</ta>
            <ta e="T373" id="Seg_3004" s="T367">NEP_1965_NenetsAndWhiteBear1_flk.066 (004.005)</ta>
            <ta e="T374" id="Seg_3005" s="T373">NEP_1965_NenetsAndWhiteBear1_flk.067 (004.006)</ta>
            <ta e="T377" id="Seg_3006" s="T374">NEP_1965_NenetsAndWhiteBear1_flk.068 (004.007)</ta>
            <ta e="T385" id="Seg_3007" s="T377">NEP_1965_NenetsAndWhiteBear1_flk.069 (004.008)</ta>
            <ta e="T390" id="Seg_3008" s="T385">NEP_1965_NenetsAndWhiteBear1_flk.070 (004.009)</ta>
            <ta e="T393" id="Seg_3009" s="T390">NEP_1965_NenetsAndWhiteBear1_flk.071 (004.010)</ta>
            <ta e="T400" id="Seg_3010" s="T393">NEP_1965_NenetsAndWhiteBear1_flk.072 (004.011)</ta>
            <ta e="T407" id="Seg_3011" s="T400">NEP_1965_NenetsAndWhiteBear1_flk.073 (004.012)</ta>
            <ta e="T414" id="Seg_3012" s="T407">NEP_1965_NenetsAndWhiteBear1_flk.074 (004.013)</ta>
            <ta e="T424" id="Seg_3013" s="T414">NEP_1965_NenetsAndWhiteBear1_flk.075 (004.014)</ta>
            <ta e="T429" id="Seg_3014" s="T424">NEP_1965_NenetsAndWhiteBear1_flk.076 (004.015)</ta>
            <ta e="T432" id="Seg_3015" s="T429">NEP_1965_NenetsAndWhiteBear1_flk.077 (004.016)</ta>
            <ta e="T442" id="Seg_3016" s="T432">NEP_1965_NenetsAndWhiteBear1_flk.078 (004.017)</ta>
            <ta e="T448" id="Seg_3017" s="T442">NEP_1965_NenetsAndWhiteBear1_flk.079 (004.018)</ta>
            <ta e="T450" id="Seg_3018" s="T448">NEP_1965_NenetsAndWhiteBear1_flk.080 (004.019)</ta>
            <ta e="T452" id="Seg_3019" s="T450">NEP_1965_NenetsAndWhiteBear1_flk.081 (004.020)</ta>
            <ta e="T455" id="Seg_3020" s="T452">NEP_1965_NenetsAndWhiteBear1_flk.082 (004.021)</ta>
            <ta e="T458" id="Seg_3021" s="T455">NEP_1965_NenetsAndWhiteBear1_flk.083 (004.022)</ta>
            <ta e="T464" id="Seg_3022" s="T458">NEP_1965_NenetsAndWhiteBear1_flk.084 (004.023)</ta>
            <ta e="T470" id="Seg_3023" s="T464">NEP_1965_NenetsAndWhiteBear1_flk.085 (004.024)</ta>
            <ta e="T475" id="Seg_3024" s="T470">NEP_1965_NenetsAndWhiteBear1_flk.086 (004.025)</ta>
            <ta e="T479" id="Seg_3025" s="T475">NEP_1965_NenetsAndWhiteBear1_flk.087 (004.026)</ta>
            <ta e="T484" id="Seg_3026" s="T479">NEP_1965_NenetsAndWhiteBear1_flk.088 (004.027)</ta>
            <ta e="T488" id="Seg_3027" s="T484">NEP_1965_NenetsAndWhiteBear1_flk.089 (004.028)</ta>
            <ta e="T493" id="Seg_3028" s="T488">NEP_1965_NenetsAndWhiteBear1_flk.090 (004.029)</ta>
            <ta e="T498" id="Seg_3029" s="T493">NEP_1965_NenetsAndWhiteBear1_flk.091 (004.030)</ta>
            <ta e="T506" id="Seg_3030" s="T498">NEP_1965_NenetsAndWhiteBear1_flk.092 (004.031)</ta>
            <ta e="T510" id="Seg_3031" s="T506">NEP_1965_NenetsAndWhiteBear1_flk.093 (004.032)</ta>
            <ta e="T514" id="Seg_3032" s="T510">NEP_1965_NenetsAndWhiteBear1_flk.094 (004.033)</ta>
            <ta e="T518" id="Seg_3033" s="T514">NEP_1965_NenetsAndWhiteBear1_flk.095 (004.034)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T11" id="Seg_3034" s="T0">на ′кӓ̄lира ′иjа ниlчик ′kъ̊ттыңыты. мат тап ′мораkыт ′сӓры kорɣыт kъ̊т′тат.</ta>
            <ta e="T19" id="Seg_3035" s="T11">ка̄лʼ ира ′нилʼчик ′kъ̊ттыңыт на ′ашʼа ′kумыт ′kъ̊сса ′суруп.</ta>
            <ta e="T24" id="Seg_3036" s="T19">тат ку′тар ′kъ̊ннанты ′морат ′то.</ta>
            <ta e="T29" id="Seg_3037" s="T24">kапиjа ′поkытыl тъ̊′таkыт ′илʼиты кӓ̄лʼи ′ира.</ta>
            <ta e="T39" id="Seg_3038" s="T29">тӓпыт ′но̄кы[у]р тим′нʼӓсик ′морат ′тъ̊̄ на ′сӓры по̄рkып ′kӓтка ‵kъ̊н′нотыт.</ta>
            <ta e="T41" id="Seg_3039" s="T39">тӓпыт kӓннотыт.</ta>
            <ta e="T46" id="Seg_3040" s="T41">на сӓры ′kорɣып ъ̊мынты ча̄′тыңыты.</ta>
            <ta e="T56" id="Seg_3041" s="T46">ныны ′съ̊[ӓ]ры ′kорɣу[ы]п. на ′сӓры ′kорɣы момпа мӓрɣы ′еңа.</ta>
            <ta e="T63" id="Seg_3042" s="T56">′ныны тəпыт ′къ̊llа но̄нынты kымыт ситы kо′ротыт.</ta>
            <ta e="T67" id="Seg_3043" s="T63">ай мо′kалʼмынты ′ситы kо′рот.</ta>
            <ta e="T74" id="Seg_3044" s="T67">′ныны на шʼиты ′тымнʼат ′инна ′kаɣлаɣынты теl′дотын.</ta>
            <ta e="T78" id="Seg_3045" s="T74">на ′тимнʼӓсыт моkонӓ lаkаl′тотыт.</ta>
            <ta e="T87" id="Seg_3046" s="T78">′тина ′чондаɣыl ′съ̊ры ′kорɣы ′kӓтпилʼ тимнʼатыт ′чонтаɣыт моkыны ′еппа.</ta>
            <ta e="T95" id="Seg_3047" s="T87">′ныны на kуп ′ӱ̄ругумпа омпа чупса ′монтаkыт ′еппа.</ta>
            <ta e="T97" id="Seg_3048" s="T95">‵пуска′тоlпа…</ta>
            <ta e="T101" id="Seg_3049" s="T97">′тəп ныны сӓ̄ ‵орымпыlымпа.</ta>
            <ta e="T108" id="Seg_3050" s="T101">тəп на ′морат ′kанӓ[ы]kыт ны[н]ты на ′иппымынты.</ta>
            <ta e="T111" id="Seg_3051" s="T108"> тəп ′нымды ′иппымна.</ta>
            <ta e="T114" id="Seg_3052" s="T111">но̄ты сӓ̄т ′орымкыlымпа.</ta>
            <ta e="T118" id="Seg_3053" s="T114">ноп ′чумпа ′соkыты ′пе̄мыты.</ta>
            <ta e="T120" id="Seg_3054" s="T118">′оlа ′сӣтʼӣ.</ta>
            <ta e="T126" id="Seg_3055" s="T120">ныны нʼӓннӓ ′kынталʼта ӱнымты ′паңыса ма̄тыңыты.</ta>
            <ta e="T131" id="Seg_3056" s="T126">на но̨̄kыр ′kуптыты ′ныны ′пакта.</ta>
            <ta e="T137" id="Seg_3057" s="T131">тəп онты нымты ′ӣппымпа ′омӓй ′кондыkӓ.</ta>
            <ta e="T141" id="Seg_3058" s="T137">ныны ′таңымпа намышʼак ′апсытка.</ta>
            <ta e="T147" id="Seg_3059" s="T141">kумпа ноп ′таңымпа ′kаɣлынты сʼӱнчаkыт kонтыка.</ta>
            <ta e="T153" id="Seg_3060" s="T147">намышʼалʼ ′уlканты lака kалынты ы̄ɣыт ′kалʼимынты.</ta>
            <ta e="T156" id="Seg_3061" s="T153">′мора ′мунтык чʼумпа.</ta>
            <ta e="T174" id="Seg_3062" s="T156">кур уккур чонтаɣыт тӓп kос kай ниlчик ′kъ̊̄тыңыты kай ноп лʼи kай kуп ′тӓмайсе ′курулʼе алʼ нилʼчик ′kуkа.</ta>
            <ta e="T180" id="Seg_3063" s="T174">тəп ′инна сʼӓп ан′таlумпа ниlчик кука.</ta>
            <ta e="T184" id="Seg_3064" s="T180">′кеккыса мӯт ′тӣнолʼа а‵таlимы′нтотыт.</ta>
            <ta e="T190" id="Seg_3065" s="T184">на ′kаɣлʼинты иlkылʼ укаlакасӓ ′компырлʼӓ.</ta>
            <ta e="T194" id="Seg_3066" s="T190">′нынӓ ай ′илʼлʼа kонта.</ta>
            <ta e="T206" id="Seg_3067" s="T194">ай тӓп kос kай ниlчик сʼип ′соkочи. kуп ниlчик ′ӣlа куроl[лʼ]а ′кука.</ta>
            <ta e="T211" id="Seg_3068" s="T206">′ныны каныкты ′конна ′танта ′таннымпа.</ta>
            <ta e="T215" id="Seg_3069" s="T211">′саjиты ′намышʼак ′авсытка kумп</ta>
            <ta e="T218" id="Seg_3070" s="T215">′кылʼат сʼӱнʼанты ‵токы′мотпа.</ta>
            <ta e="T222" id="Seg_3071" s="T218">уккур чон′таkыт ниlчик ӓ̄са.</ta>
            <ta e="T230" id="Seg_3072" s="T222">′мӣтӓ тъ̊ты а̄тӓнты [митты ′тъ̊тты ′чурынты] ′морат ′kаныkыт.</ta>
            <ta e="T234" id="Seg_3073" s="T230">теп ′намысʼак ′апсытка ′kумпа.</ta>
            <ta e="T242" id="Seg_3074" s="T234">ныны ′ниlчик сʼӓп kӓңа kаглʼап тъ̊тын, ′мъ̊ннаты ′тӯlиспа.</ta>
            <ta e="T249" id="Seg_3075" s="T242">ныны kаɣlынты мокаlит ӣмыт ′соkолʼе ′ку̊нна танта.</ta>
            <ta e="T253" id="Seg_3076" s="T249">ныны конна ′kаныкты ′алʼче.</ta>
            <ta e="T256" id="Seg_3077" s="T253">ныны нымты ӣппа.</ta>
            <ta e="T265" id="Seg_3078" s="T256">ай kос kай нилʼчик ′kъ̊тыңыты kуп тъ̊масʼе ′курыllа kупна.</ta>
            <ta e="T272" id="Seg_3079" s="T265">′коннат нʼӓрɣыl ′марГыт ′топынты ′соkылʼа ′конна ′танта.</ta>
            <ta e="T276" id="Seg_3080" s="T272">нына ′kонна ′соkkолʼа ′танта.</ta>
            <ta e="T281" id="Seg_3081" s="T276">мат ′kотӓ ′инна ′ныла‵исе′ңена.</ta>
            <ta e="T284" id="Seg_3082" s="T281">конна ′соkkолʼа танта.</ta>
            <ta e="T287" id="Seg_3083" s="T284">орты меlды ′чӓ̄ңка.</ta>
            <ta e="T297" id="Seg_3084" s="T287">ай kос kай ниlчик ‵соkалʼлʼиты kуп ′асʼя ниlчик ′k[к]ука ′kуролʼа.</ta>
            <ta e="T302" id="Seg_3085" s="T297">на ′морат ′kаныкӓт ′пиргъ тъ̊тты.</ta>
            <ta e="T304" id="Seg_3086" s="T302">′инна ′сыɣолна.</ta>
            <ta e="T309" id="Seg_3087" s="T304">ныны ′конна ′сыɣылʼлʼе ниlчик маннымпа.</ta>
            <ta e="T315" id="Seg_3088" s="T309">′коннат ниlчик ′kоңыты. котыl мо̄т омнынты.</ta>
            <ta e="T319" id="Seg_3089" s="T315">ныны ай ′иllа ′оlчеjа.</ta>
            <ta e="T322" id="Seg_3090" s="T319">кутар ′ныча ‵туlичантык.</ta>
            <ta e="T329" id="Seg_3091" s="T322">ныны на мо̄тты конна ′нылʼа ′иllа тӱңа.</ta>
            <ta e="T332" id="Seg_3092" s="T329">kӓ̄лʼи мо̄т ′омнынты.</ta>
            <ta e="T335" id="Seg_3093" s="T332">′отаты ‵е̄тымантоɣыт ку′раlимын′тотыт.</ta>
            <ta e="T338" id="Seg_3094" s="T335">ныны мо̄тты сʼерна.</ta>
            <ta e="T341" id="Seg_3095" s="T338">′ира у′ккур ′нӓлʼаты.</ta>
            <ta e="T350" id="Seg_3096" s="T341">уккур чонтоɣыт кӓ̄l ира ′ниlчик ӓ̄са тат на kуп ‵австӓд[т]и.</ta>
            <ta e="T357" id="Seg_3097" s="T350">тӓп на и′ранмыkыт шʼ[с]иты ′тот ′о̨тат.</ta>
            <ta e="T367" id="Seg_3098" s="T357">мат уккур тот ′отамы сӯруп мунтык ′амныты. ′ира ниlчик ′kъ̊тыңыт.</ta>
            <ta e="T373" id="Seg_3099" s="T367">тат ′пона ′тандылʼе мат ′о̨тамы о̄′тӓты.</ta>
            <ta e="T374" id="Seg_3100" s="T373">‵лʼупкы′монӓ.</ta>
            <ta e="T377" id="Seg_3101" s="T374">тӓп ′пона ′танта.</ta>
            <ta e="T385" id="Seg_3102" s="T377">тӓп ′пона танта, ′намышʼак ′се̄ɣелʼимпы kуп ′kайты ‵манны′ментыт.</ta>
            <ta e="T390" id="Seg_3103" s="T385">ныны тӓп ай ′иllа ′аlчимпа.</ta>
            <ta e="T393" id="Seg_3104" s="T390">нымты на иппымынты.</ta>
            <ta e="T400" id="Seg_3105" s="T393">на тӓпыт тӱ̄лʼе ′пӯlа ′сӯруп а ′kъ̊[ӓ]нмынты.</ta>
            <ta e="T407" id="Seg_3106" s="T400">ны у′кот ‵ӓсӓсы′kӓɣи ′отамтий ′отылӓ поɣот ‵иllим′поkий.</ta>
            <ta e="T414" id="Seg_3107" s="T407">ныны на ′kумуп ′австымпылʼӓ нымты на иllемынты.</ta>
            <ta e="T424" id="Seg_3108" s="T414">нынӓ на и′рре ай ниlчик ′kъ̊тыңыты ай ′kъ̊̄lасик ′о̨тамты ′отылʼлʼӓ.</ta>
            <ta e="T429" id="Seg_3109" s="T424">отаты kапиjе мачанты ′асʼа kънта.</ta>
            <ta e="T432" id="Seg_3110" s="T429">мо̄тыт ′kаныкыт кон′тотыт.</ta>
            <ta e="T442" id="Seg_3111" s="T432">нынӓ тӓп ′пона танта ′се̄пылʼаlмынты kъ̊̄lа ай ′иllа ′оlчи.</ta>
            <ta e="T448" id="Seg_3112" s="T442">ныны тəп у′кур чон′тоkыт ниlчик сʼ[шʼ]итыjе.</ta>
            <ta e="T450" id="Seg_3113" s="T448">kай ′kъ̊нтыты.</ta>
            <ta e="T452" id="Seg_3114" s="T450">монты ′илʼи[е]ча.</ta>
            <ta e="T455" id="Seg_3115" s="T452">ныны ′моkона ′тӱңа.</ta>
            <ta e="T458" id="Seg_3116" s="T455">′мо̄тты kӓлʼи ира kонта.</ta>
            <ta e="T464" id="Seg_3117" s="T458">ныны тəпытыт о̄та ме̄lды ′етимон′тоkыт kонтотыт.</ta>
            <ta e="T470" id="Seg_3118" s="T464">тəп на тӱ̄′б̂иl порɣыl ′пемытыса ′ӣlа.</ta>
            <ta e="T475" id="Seg_3119" s="T470">′ныны ′ногырымтелʼи пит ниlчик ′еса.</ta>
            <ta e="T479" id="Seg_3120" s="T475">‵митымоl сомак ӓсак.</ta>
            <ta e="T484" id="Seg_3121" s="T479">кӓ̄lи ира ′отаты ′шʼӧтты ′асʼа ′къ̊нта.</ta>
            <ta e="T488" id="Seg_3122" s="T484">мотыт kанакыт нымты kъ̊нтотыт.</ta>
            <ta e="T493" id="Seg_3123" s="T488">тӓп тӱ̄lа ай ′иllа ′kонтаjе.</ta>
            <ta e="T498" id="Seg_3124" s="T493">топтылʼ ′kарыт монды сы[ъ̊]ры пи′нна ′индыты.</ta>
            <ta e="T506" id="Seg_3125" s="T498">мат тӓп ′куттар тӱ̄са ′о̨тамы сʼӧтты ′асʼа ′kъ̊нта.</ta>
            <ta e="T510" id="Seg_3126" s="T506">′kъ̊раl тӓтты, сырып ′пилʼлʼа ′kъ̊ңаjе.</ta>
            <ta e="T514" id="Seg_3127" s="T510">сомак ′иlа на ′къ̊ңты.</ta>
            <ta e="T518" id="Seg_3128" s="T514">тӓп но̄ты сомак ӓса.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T11" id="Seg_3129" s="T0">na käːlʼira ija nilʼčik qəttɨŋɨtɨ. mat tap moraqɨt särɨ qorqɨt qəttat.</ta>
            <ta e="T19" id="Seg_3130" s="T11">kaːlʼ ira nilʼčik qəttɨŋɨt na aša qumɨt qəssa surup.</ta>
            <ta e="T24" id="Seg_3131" s="T19">tat kutar qənnantɨ morat to.</ta>
            <ta e="T29" id="Seg_3132" s="T24">qapija poqɨtɨlʼ tətaqɨt ilʼitɨ käːlʼi ira.</ta>
            <ta e="T39" id="Seg_3133" s="T29">täpɨt noːkɨ[u]r timnʼäsik morat təː na särɨ poːrqɨp qätka qənnotɨt.</ta>
            <ta e="T41" id="Seg_3134" s="T39">täpɨt qännotɨt.</ta>
            <ta e="T46" id="Seg_3135" s="T41">na särɨ qorqɨp əmɨntɨ čaːtɨŋɨtɨ.</ta>
            <ta e="T56" id="Seg_3136" s="T46">nɨnɨ sə[ä]rɨ qorqu[ɨ]p. na särɨ qorqɨ mompa märqɨ eŋa.</ta>
            <ta e="T63" id="Seg_3137" s="T56">nɨnɨ təpɨt kəlʼlʼa noːnɨntɨ qɨmɨt sitɨ qorotɨt.</ta>
            <ta e="T67" id="Seg_3138" s="T63">aj moqalʼmɨntɨ sitɨ qorot.</ta>
            <ta e="T74" id="Seg_3139" s="T67">nɨnɨ na šitɨ tɨmnʼat inna qaqlaqɨntɨ telʼdotɨn.</ta>
            <ta e="T78" id="Seg_3140" s="T74">na timnʼäsɨt moqonä lʼaqalʼtotɨt.</ta>
            <ta e="T87" id="Seg_3141" s="T78">tina čondaqɨlʼ sərɨ qorqɨ qätpilʼ timnʼatɨt čontaqɨt moqɨnɨ eppa.</ta>
            <ta e="T95" id="Seg_3142" s="T87">nɨnɨ na qup üːrugumpa ompa čupsa montaqɨt eppa.</ta>
            <ta e="T97" id="Seg_3143" s="T95">puskatolʼpa…</ta>
            <ta e="T101" id="Seg_3144" s="T97">təp nɨnɨ säː orɨmpɨlʼɨmpa.</ta>
            <ta e="T108" id="Seg_3145" s="T101">təp na morat qanä[ɨ]qɨt nɨ[n]tɨ na ippɨmɨntɨ.</ta>
            <ta e="T111" id="Seg_3146" s="T108">təp nɨmdɨ ippɨmna.</ta>
            <ta e="T114" id="Seg_3147" s="T111">noːtɨ säːt orɨmkɨlʼɨmpa.</ta>
            <ta e="T118" id="Seg_3148" s="T114">nop čumpa soqɨtɨ peːmɨtɨ.</ta>
            <ta e="T120" id="Seg_3149" s="T118">olʼa siːčiː.</ta>
            <ta e="T126" id="Seg_3150" s="T120">nɨnɨ nʼännä qɨntalʼta ünɨmtɨ paŋɨsa maːtɨŋɨtɨ.</ta>
            <ta e="T131" id="Seg_3151" s="T126">na noːqɨr quptɨtɨ nɨnɨ pakta.</ta>
            <ta e="T137" id="Seg_3152" s="T131">təp ontɨ nɨmtɨ iːppɨmpa omäj kondɨqä.</ta>
            <ta e="T141" id="Seg_3153" s="T137">nɨnɨ taŋɨmpa namɨšak apsɨtka.</ta>
            <ta e="T147" id="Seg_3154" s="T141">qumpa nop taŋɨmpa qaqlɨntɨ sʼünčaqɨt qontɨka.</ta>
            <ta e="T153" id="Seg_3155" s="T147">namɨšalʼ ulʼkantɨ lʼaka qalɨntɨ ɨːqɨt qalʼimɨntɨ.</ta>
            <ta e="T156" id="Seg_3156" s="T153">mora muntɨk čʼumpa.</ta>
            <ta e="T174" id="Seg_3157" s="T156">kur ukkur čontaqɨt täp qos qaj nilʼčik qəːtɨŋɨtɨ qaj nop lʼi qaj qup tämajse kurulʼe alʼ nilʼčik quqa.</ta>
            <ta e="T180" id="Seg_3158" s="T174">təp inna sʼäp antalʼumpa nilʼčik kuka.</ta>
            <ta e="T184" id="Seg_3159" s="T180">kekkɨsa muːt tiːnolʼa atalʼimɨntotɨt.</ta>
            <ta e="T190" id="Seg_3160" s="T184">na qaqlʼintɨ ilʼqɨlʼ ukalʼakasä kompɨrlʼä.</ta>
            <ta e="T194" id="Seg_3161" s="T190">nɨnä aj ilʼlʼa qonta.</ta>
            <ta e="T206" id="Seg_3162" s="T194">aj täp qos qaj nilʼčik sʼip soqoči. qup nilʼčik iːlʼa kurolʼ[lʼ]a kuka.</ta>
            <ta e="T211" id="Seg_3163" s="T206">nɨnɨ kanɨktɨ konna tanta tannɨmpa.</ta>
            <ta e="T215" id="Seg_3164" s="T211">sajitɨ namɨšak avsɨtka qumpa.</ta>
            <ta e="T218" id="Seg_3165" s="T215">kɨlʼat sʼünʼantɨ tokɨmotpa.</ta>
            <ta e="T222" id="Seg_3166" s="T218">ukkur čontaqɨt nilʼčik äːsa.</ta>
            <ta e="T230" id="Seg_3167" s="T222">miːtä tətɨ aːtäntɨ [mittɨ təttɨ čurɨntɨ] morat qanɨqɨt.</ta>
            <ta e="T234" id="Seg_3168" s="T230">tep namɨsʼak apsɨtka qumpa.</ta>
            <ta e="T242" id="Seg_3169" s="T234">nɨnɨ nilʼčik sʼäp qäŋa qaglʼap tətɨn, mənnatɨ tuːlʼispa.</ta>
            <ta e="T249" id="Seg_3170" s="T242">nɨnɨ qaqlʼɨntɨ mokalʼit iːmɨt soqolʼe kunna tanta.</ta>
            <ta e="T253" id="Seg_3171" s="T249">nɨnɨ konna qanɨktɨ alʼče.</ta>
            <ta e="T256" id="Seg_3172" s="T253">nɨnɨ nɨmtɨ iːppa.</ta>
            <ta e="T265" id="Seg_3173" s="T256">aj qos qaj nilʼčik qətɨŋɨtɨ qup təmasʼe kurɨlʼlʼa qupna.</ta>
            <ta e="T272" id="Seg_3174" s="T265">konnat nʼärqɨlʼ marГɨt topɨntɨ soqɨlʼa konna tanta.</ta>
            <ta e="T276" id="Seg_3175" s="T272">nɨna qonna soqqolʼa tanta.</ta>
            <ta e="T281" id="Seg_3176" s="T276">mat qotä inna nɨlaiseŋena.</ta>
            <ta e="T284" id="Seg_3177" s="T281">konna soqqolʼa tanta.</ta>
            <ta e="T287" id="Seg_3178" s="T284">ortɨ melʼdɨ čäːŋka.</ta>
            <ta e="T297" id="Seg_3179" s="T287">aj qos qaj nilʼčik soqalʼlʼitɨ qup asʼя nilʼčik q[k]uka qurolʼa.</ta>
            <ta e="T302" id="Seg_3180" s="T297">na morat qanɨkät pirgə təttɨ.</ta>
            <ta e="T304" id="Seg_3181" s="T302">inna sɨqolna.</ta>
            <ta e="T309" id="Seg_3182" s="T304">nɨnɨ konna sɨqɨlʼlʼe nilʼčik mannɨmpa.</ta>
            <ta e="T315" id="Seg_3183" s="T309">konnat nilʼčik qoŋɨtɨ. kotɨlʼ moːt omnɨntɨ.</ta>
            <ta e="T319" id="Seg_3184" s="T315">nɨnɨ aj ilʼlʼa olʼčeja.</ta>
            <ta e="T322" id="Seg_3185" s="T319">kutar nɨča tulʼičantɨk.</ta>
            <ta e="T329" id="Seg_3186" s="T322">nɨnɨ na moːttɨ konna nɨlʼa ilʼlʼa tüŋa.</ta>
            <ta e="T332" id="Seg_3187" s="T329">qäːlʼi moːt omnɨntɨ.</ta>
            <ta e="T335" id="Seg_3188" s="T332">otatɨ eːtɨmantoqɨt kuralʼimɨntotɨt.</ta>
            <ta e="T338" id="Seg_3189" s="T335">nɨnɨ moːttɨ sʼerna.</ta>
            <ta e="T341" id="Seg_3190" s="T338">ira ukkur nälʼatɨ.</ta>
            <ta e="T350" id="Seg_3191" s="T341">ukkur čontoqɨt käːlʼ ira nilʼčik äːsa tat na qup avstäd[t]i.</ta>
            <ta e="T357" id="Seg_3192" s="T350">täp na iranmɨqɨt š[s]itɨ tot otat.</ta>
            <ta e="T367" id="Seg_3193" s="T357">mat ukkur tot otamɨ suːrup muntɨk amnɨtɨ. ira nilʼčik qətɨŋɨt.</ta>
            <ta e="T373" id="Seg_3194" s="T367">tat pona tandɨlʼe mat otamɨ oːtätɨ.</ta>
            <ta e="T374" id="Seg_3195" s="T373">lʼupkɨmonä.</ta>
            <ta e="T377" id="Seg_3196" s="T374">täp pona tanta.</ta>
            <ta e="T385" id="Seg_3197" s="T377">täp pona tanta, namɨšak seːqelʼimpɨ qup qajtɨ mannɨmentɨt.</ta>
            <ta e="T390" id="Seg_3198" s="T385">nɨnɨ täp aj ilʼlʼa alʼčimpa.</ta>
            <ta e="T393" id="Seg_3199" s="T390">nɨmtɨ na ippɨmɨntɨ.</ta>
            <ta e="T400" id="Seg_3200" s="T393">na täpɨt tüːlʼe puːlʼa suːrup a qə[ä]nmɨntɨ.</ta>
            <ta e="T407" id="Seg_3201" s="T400">nɨ ukot äsäsɨqäqi otamtij otɨlä poqot ilʼlʼimpoqij.</ta>
            <ta e="T414" id="Seg_3202" s="T407">nɨnɨ na qumup avstɨmpɨlʼä nɨmtɨ na ilʼlʼemɨntɨ.</ta>
            <ta e="T424" id="Seg_3203" s="T414">nɨnä na irre aj nilʼčik qətɨŋɨtɨ aj qəːlʼasik otamtɨ otɨlʼlʼä.</ta>
            <ta e="T429" id="Seg_3204" s="T424">otatɨ qapije mačantɨ asʼa qənta.</ta>
            <ta e="T432" id="Seg_3205" s="T429">moːtɨt qanɨkɨt kontotɨt.</ta>
            <ta e="T442" id="Seg_3206" s="T432">nɨnä täp pona tanta seːpɨlʼalʼmɨntɨ qəːlʼa aj ilʼlʼa olʼči.</ta>
            <ta e="T448" id="Seg_3207" s="T442">nɨnɨ təp ukur čontoqɨt nilʼčik sʼ[š]itɨje.</ta>
            <ta e="T450" id="Seg_3208" s="T448">qaj qəntɨtɨ.</ta>
            <ta e="T452" id="Seg_3209" s="T450">montɨ ilʼi[e]ča.</ta>
            <ta e="T455" id="Seg_3210" s="T452">nɨnɨ moqona tüŋa.</ta>
            <ta e="T458" id="Seg_3211" s="T455">moːttɨ qälʼi ira qonta.</ta>
            <ta e="T464" id="Seg_3212" s="T458">nɨnɨ təpɨtɨt oːta meːlʼdɨ etimontoqɨt qontotɨt.</ta>
            <ta e="T470" id="Seg_3213" s="T464">təp na tüːp̂ilʼ porqɨlʼ pemɨtɨsa iːlʼa.</ta>
            <ta e="T475" id="Seg_3214" s="T470">nɨnɨ nogɨrɨmtelʼi pit nilʼčik esa.</ta>
            <ta e="T479" id="Seg_3215" s="T475">mitɨmolʼ somak äsak.</ta>
            <ta e="T484" id="Seg_3216" s="T479">käːlʼi ira otatɨ šöttɨ asʼa kənta.</ta>
            <ta e="T488" id="Seg_3217" s="T484">motɨt qanakɨt nɨmtɨ qəntotɨt.</ta>
            <ta e="T493" id="Seg_3218" s="T488">täp tüːlʼa aj ilʼlʼa qontaje.</ta>
            <ta e="T498" id="Seg_3219" s="T493">toptɨlʼ qarɨt mondɨ sɨ[ə]rɨ pinna indɨtɨ.</ta>
            <ta e="T506" id="Seg_3220" s="T498">mat täp kuttar tüːsa otamɨ sʼöttɨ asʼa qənta.</ta>
            <ta e="T510" id="Seg_3221" s="T506">qəralʼ tättɨ, sɨrɨp pilʼlʼa qəŋaje.</ta>
            <ta e="T514" id="Seg_3222" s="T510">somak ilʼa na kəŋtɨ.</ta>
            <ta e="T518" id="Seg_3223" s="T514">täp noːtɨ somak äsa.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T11" id="Seg_3224" s="T0">Na qäl ira ija nılʼčʼik kəttɨŋɨtɨ: Mat tap moraqɨt särɨ qorqɨt qəttat. </ta>
            <ta e="T19" id="Seg_3225" s="T11">Qal ira nılʼčʼik kəttɨŋɨt: Na aša qumɨt qəssa suːrup. </ta>
            <ta e="T24" id="Seg_3226" s="T19">Tat kutar qənnantɨ morat tɔː. </ta>
            <ta e="T29" id="Seg_3227" s="T24">Qapija poːkɨtɨlʼ tətaqɨt ilʼitɨ qälʼi ira. </ta>
            <ta e="T39" id="Seg_3228" s="T29">Täpɨt nɔːkɨr timnʼäsik morat təː na särɨ qorqɨp qätqa qənnɔːtɨt. </ta>
            <ta e="T41" id="Seg_3229" s="T39">Täpɨt qännɔːtɨt. </ta>
            <ta e="T46" id="Seg_3230" s="T41">Na särɨ qorqɨp, əmɨntɨ čʼatɨŋɨtɨ. </ta>
            <ta e="T56" id="Seg_3231" s="T46">Nɨnɨ sərɨ qorqɨp ((…)), na särɨ qorqɨ mompa märqɨ ɛːŋa. </ta>
            <ta e="T63" id="Seg_3232" s="T56">Nɨːnɨ təpɨt qəlla nɔːnnɨntɨ kɨmɨt sitɨ korɔːtɨt. </ta>
            <ta e="T67" id="Seg_3233" s="T63">Aj moqalmɨntɨ sitɨ korɔːt. </ta>
            <ta e="T74" id="Seg_3234" s="T67">Nɨːnɨ na šitɨ tɨmnʼat ınna qaqlaqɨntɨ tɛltɔːtɨn. </ta>
            <ta e="T78" id="Seg_3235" s="T74">Na timnʼäsɨt moqonä laqaltɔːtɨt. </ta>
            <ta e="T87" id="Seg_3236" s="T78">Tına čʼontaqɨlʼ sərɨ qorqɨ qätpilʼ timnʼatɨt čʼontaqɨt moqɨnɨ iːppa. </ta>
            <ta e="T95" id="Seg_3237" s="T87">Nɨːnɨ na qup ürukkumpa, ompa čʼupsa montaqɨt ɛːppa. </ta>
            <ta e="T97" id="Seg_3238" s="T95">Pusqatɔːlpa. </ta>
            <ta e="T101" id="Seg_3239" s="T97">Təp nɨːnɨ sä orɨmkɨlɨmpa. </ta>
            <ta e="T108" id="Seg_3240" s="T101">Təp na morat qanäqqɨt nɨːntɨ na ippɨmmɨntɨ. </ta>
            <ta e="T111" id="Seg_3241" s="T108">Təp nɨmtɨ ippɨmpa. </ta>
            <ta e="T114" id="Seg_3242" s="T111">Nɔːtɨ säːt orɨmkɨlɨmpa. </ta>
            <ta e="T118" id="Seg_3243" s="T114">Nɔːt čʼumpa soqɨtɨ, peːmɨtɨ. </ta>
            <ta e="T120" id="Seg_3244" s="T118">Ola sičʼi. </ta>
            <ta e="T126" id="Seg_3245" s="T120">Nɨːnɨ nʼennä qɨntalta, üːnɨmtɨ paŋɨsa matɨŋɨtɨ. </ta>
            <ta e="T131" id="Seg_3246" s="T126">Na nɔːkɨr quptɨtɨ nɨːnɨ pakta. </ta>
            <ta e="T137" id="Seg_3247" s="T131">Təp ontɨ nɨmtɨ ippɨmpa, ɔːmäj qontɨkkä. </ta>
            <ta e="T141" id="Seg_3248" s="T137">Nɨːnɨ taŋɨmpa namɨšak apsɨtqa. </ta>
            <ta e="T147" id="Seg_3249" s="T141">Qumpa, nop taŋɨmpa, qaqlɨntɨ šünčʼaqɨt qontɨkka. </ta>
            <ta e="T153" id="Seg_3250" s="T147">Namɨšalʼ ulqantɨ laka qalɨntɨ ɨlqɨt qalʼimmɨntɨ. </ta>
            <ta e="T156" id="Seg_3251" s="T153">Mora muntɨk čʼumpa. </ta>
            <ta e="T174" id="Seg_3252" s="T156">Kur, ukkur čʼontaqɨt täp qos qaj nılʼčʼik kətɨŋɨtɨ qaj nop lʼi qaj qup tämajsä kurulʼä alʼ nılʼčʼik kuka. </ta>
            <ta e="T180" id="Seg_3253" s="T174">Təp ınna sʼäp antalumpa, nılʼčʼik kuka. </ta>
            <ta e="T184" id="Seg_3254" s="T180">Kekkɨsa muːt tınolʼa atalimmɨntɔːtɨt. </ta>
            <ta e="T190" id="Seg_3255" s="T184">Na qaqlʼintɨ ilqɨlʼ ulqalʼ lakasä qompɨrlʼä. </ta>
            <ta e="T194" id="Seg_3256" s="T190">Nɨːnä aj ılla qonta. </ta>
            <ta e="T206" id="Seg_3257" s="T194">Aj täp qos qaj nılʼčʼik šıp soqočʼčʼi: Qup nılʼčʼik ıla kurola kuka. </ta>
            <ta e="T211" id="Seg_3258" s="T206">Nɨːnɨ qanɨqtɨ konna tanta (/tannɨmpa). </ta>
            <ta e="T215" id="Seg_3259" s="T211">Sajitɨ namɨšak awsɨtqa qumpa. </ta>
            <ta e="T218" id="Seg_3260" s="T215">Qɨlʼat šünʼantɨ tokɨmɔːtpa. </ta>
            <ta e="T222" id="Seg_3261" s="T218">Ukkur čʼontaqɨt nılʼčʼik ɛːsa. </ta>
            <ta e="T230" id="Seg_3262" s="T222">Mitä tətɨ atäntɨ (/mittɨ təttɨ čʼurɨntɨ) morat qanɨqqɨt. </ta>
            <ta e="T234" id="Seg_3263" s="T230">Tep namɨsʼak apsɨtqa qumpa. </ta>
            <ta e="T242" id="Seg_3264" s="T234">Nɨːnɨ nılʼčʼik sʼäp qäŋŋa: qaqlʼap tətɨn mənatɨ, tulispa. </ta>
            <ta e="T249" id="Seg_3265" s="T242">Nɨːnɨ qaqlɨntɨ moqalit iːmɨt soqolʼä kunna tanta. </ta>
            <ta e="T253" id="Seg_3266" s="T249">Nɨːnɨ konna qanɨqtɨ alʼčʼe. </ta>
            <ta e="T256" id="Seg_3267" s="T253">Nɨːnɨ nɨmtɨ ippa. </ta>
            <ta e="T265" id="Seg_3268" s="T256">Aj qos qaj nılʼčʼik kətɨŋɨtɨ: Qup təmasä kurɨla qupna. </ta>
            <ta e="T272" id="Seg_3269" s="T265">Konnat nʼärqɨlʼ markɨt toːpɨntɨ soqɨlʼa konna tanta. </ta>
            <ta e="T276" id="Seg_3270" s="T272">Nɨːna konna soqqolʼa tanta. </ta>
            <ta e="T281" id="Seg_3271" s="T276">Mat qotä ınna nɨlaıːsäŋ ɛna. </ta>
            <ta e="T284" id="Seg_3272" s="T281">Konna soqqolʼa tanta. </ta>
            <ta e="T287" id="Seg_3273" s="T284">Ortɨ meːltɨ čʼäːŋka. </ta>
            <ta e="T297" id="Seg_3274" s="T287">Aj qos qaj nılʼčʼik soqalʼlʼitɨ: Qup aša nılʼčʼik kuka kurolʼa. </ta>
            <ta e="T302" id="Seg_3275" s="T297">Na morat qanɨqqät pirqə təttɨ. </ta>
            <ta e="T304" id="Seg_3276" s="T302">İnna sɨqolʼna. </ta>
            <ta e="T309" id="Seg_3277" s="T304">Nɨːnɨ konna sɨqɨlʼä nılʼčʼik mannɨmpa. </ta>
            <ta e="T315" id="Seg_3278" s="T309">Konnat nılʼčʼik qoŋɨtɨ: kotɨlʼ mɔːt ɔːmnɨntɨ. </ta>
            <ta e="T319" id="Seg_3279" s="T315">Nɨːnɨ aj ılla olʼčʼeja. </ta>
            <ta e="T322" id="Seg_3280" s="T319">Kutar nɨčʼa tuličʼčʼantɨk? </ta>
            <ta e="T329" id="Seg_3281" s="T322">Nɨːnɨ na mɔːttɨ konna nɨlʼlʼa ılla tüŋa. </ta>
            <ta e="T332" id="Seg_3282" s="T329">Qälʼi mɔːt ɔːmnɨntɨ. </ta>
            <ta e="T335" id="Seg_3283" s="T332">Ɔːtatɨ etɨmantoːqɨt kuralimmɨntɔːtɨt. </ta>
            <ta e="T338" id="Seg_3284" s="T335">Nɨːnɨ mɔːttɨ šerna. </ta>
            <ta e="T341" id="Seg_3285" s="T338">Ira ukkur nälʼatɨ. </ta>
            <ta e="T350" id="Seg_3286" s="T341">Ukkur čʼontoːqɨt qäl ira nılʼčʼik ɛsa: Tat na qup awstäti. </ta>
            <ta e="T357" id="Seg_3287" s="T350">Täp na iran mɨqɨt šitɨ toːt ɔːtat. </ta>
            <ta e="T367" id="Seg_3288" s="T357">Mat ukkur toːt ɔːtamɨ suːrup muntɨk amnɨtɨ,– ira nılʼčʼik kətɨŋɨt. </ta>
            <ta e="T373" id="Seg_3289" s="T367">Tat pona tantɨlʼä mat ɔːtamɨ ɔːtätɨ. </ta>
            <ta e="T374" id="Seg_3290" s="T373">Lʼupkɨmɔːnnä. </ta>
            <ta e="T377" id="Seg_3291" s="T374">Täp pona tanta. </ta>
            <ta e="T385" id="Seg_3292" s="T377">Täp pona tanta, namɨšak seqelʼimpɨ qup, qajtɨ mannɨmmɛntɨt? </ta>
            <ta e="T390" id="Seg_3293" s="T385">Nɨːnɨ täp aj ılla alʼčʼimpa. </ta>
            <ta e="T393" id="Seg_3294" s="T390">Nɨmtɨ na ippɨmmɨntɨ. </ta>
            <ta e="T400" id="Seg_3295" s="T393">Na täpɨt tülʼä puːla suːrup na qənmɨntɨ. </ta>
            <ta e="T407" id="Seg_3296" s="T400">Nɨː ukoːt äsäsɨqäːqi ɔːtantij ɔːtɨlä poːqot ilimpɔːqij. </ta>
            <ta e="T414" id="Seg_3297" s="T407">Nɨːnɨ na qumup awstɨmpɨlʼä nɨmtɨ na illemmɨntɨ. </ta>
            <ta e="T424" id="Seg_3298" s="T414">Nɨːnä na ire aj nılʼčʼik kətɨŋɨtɨ: Aj qəlasik ɔːtamtɨ ɔːtɨlʼä. </ta>
            <ta e="T429" id="Seg_3299" s="T424">Ɔːtatɨ qapije mačʼantɨ asʼa qənta. </ta>
            <ta e="T432" id="Seg_3300" s="T429">Mɔːtɨt qanɨqqɨt qontɔːtɨt. </ta>
            <ta e="T442" id="Seg_3301" s="T432">Nɨːnä täp pona tanta, seːpɨlʼalʼ mɨntɨ qəlla aj ılla olʼčʼi. </ta>
            <ta e="T448" id="Seg_3302" s="T442">Nɨːnɨ təp ukur čʼontoːqɨt nılʼčʼik šitɨje. </ta>
            <ta e="T450" id="Seg_3303" s="T448">Qaj qəntɨtɨ? </ta>
            <ta e="T452" id="Seg_3304" s="T450">Montɨ ilɛčʼa. </ta>
            <ta e="T455" id="Seg_3305" s="T452">Nɨːnɨ moqona tüŋa. </ta>
            <ta e="T458" id="Seg_3306" s="T455">Mɔːttɨ qäli ira qonta. </ta>
            <ta e="T464" id="Seg_3307" s="T458">Nɨːnɨ təpɨtɨt ɔːta meːltɨ etimontoːqɨt qontɔːtɨt. </ta>
            <ta e="T470" id="Seg_3308" s="T464">Təp na tüpilʼ porqɨlʼ peːmɨtɨsa ila. </ta>
            <ta e="T475" id="Seg_3309" s="T470">Nɨːnɨ nɔːkɨrɨmtälʼi pit nılʼčʼik ɛsa. </ta>
            <ta e="T479" id="Seg_3310" s="T475">Mitɨ mol somak ɛsak. </ta>
            <ta e="T484" id="Seg_3311" s="T479">Qäli ira ɔːtatɨ šöttɨ asʼa qənta. </ta>
            <ta e="T488" id="Seg_3312" s="T484">Mɔːtɨt qanaqqɨt nɨmtɨ qəntɔːtɨt. </ta>
            <ta e="T493" id="Seg_3313" s="T488">Täp tüla aj ılla qontaje. </ta>
            <ta e="T498" id="Seg_3314" s="T493">Tɔːptɨlʼ qarɨt montɨ sɨrɨ pinnaıːntɨtɨ. </ta>
            <ta e="T506" id="Seg_3315" s="T498">Mat, täp kuttar tüsa, ɔːtamɨ šöttɨ aša qənta. </ta>
            <ta e="T510" id="Seg_3316" s="T506">Kəraltättɨ, sɨrɨp pilʼlʼa qəŋaje. </ta>
            <ta e="T514" id="Seg_3317" s="T510">Somak ılla na qəŋtɨ. </ta>
            <ta e="T518" id="Seg_3318" s="T514">Täp nɔːtɨ somak ɛsa. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_3319" s="T0">na</ta>
            <ta e="T96" id="Seg_3320" s="T1">qäl</ta>
            <ta e="T2" id="Seg_3321" s="T96">ira</ta>
            <ta e="T3" id="Seg_3322" s="T2">ija</ta>
            <ta e="T4" id="Seg_3323" s="T3">nılʼčʼi-k</ta>
            <ta e="T5" id="Seg_3324" s="T4">kəttɨ-ŋɨ-tɨ</ta>
            <ta e="T6" id="Seg_3325" s="T5">Mat</ta>
            <ta e="T7" id="Seg_3326" s="T6">tap</ta>
            <ta e="T8" id="Seg_3327" s="T7">mora-qɨt</ta>
            <ta e="T9" id="Seg_3328" s="T8">särɨ</ta>
            <ta e="T10" id="Seg_3329" s="T9">qorqɨ-t</ta>
            <ta e="T11" id="Seg_3330" s="T10">qət-ta-t</ta>
            <ta e="T519" id="Seg_3331" s="T11">Qal</ta>
            <ta e="T12" id="Seg_3332" s="T519">ira</ta>
            <ta e="T13" id="Seg_3333" s="T12">nılʼčʼi-k</ta>
            <ta e="T14" id="Seg_3334" s="T13">kəttɨ-ŋɨ-t</ta>
            <ta e="T15" id="Seg_3335" s="T14">Na</ta>
            <ta e="T16" id="Seg_3336" s="T15">aša</ta>
            <ta e="T17" id="Seg_3337" s="T16">qum-ɨ-t</ta>
            <ta e="T18" id="Seg_3338" s="T17">qəs-sa</ta>
            <ta e="T19" id="Seg_3339" s="T18">suːrup</ta>
            <ta e="T20" id="Seg_3340" s="T19">tan</ta>
            <ta e="T21" id="Seg_3341" s="T20">kutar</ta>
            <ta e="T22" id="Seg_3342" s="T21">qən-na-ntɨ</ta>
            <ta e="T23" id="Seg_3343" s="T22">mora-t</ta>
            <ta e="T24" id="Seg_3344" s="T23">tɔː</ta>
            <ta e="T25" id="Seg_3345" s="T24">qapija</ta>
            <ta e="T26" id="Seg_3346" s="T25">poː-kɨtɨ-lʼ</ta>
            <ta e="T27" id="Seg_3347" s="T26">təta-qɨt</ta>
            <ta e="T28" id="Seg_3348" s="T27">ilʼi-tɨ</ta>
            <ta e="T520" id="Seg_3349" s="T28">qälʼi</ta>
            <ta e="T29" id="Seg_3350" s="T520">ira</ta>
            <ta e="T30" id="Seg_3351" s="T29">täp-ɨ-t</ta>
            <ta e="T31" id="Seg_3352" s="T30">nɔːkɨr</ta>
            <ta e="T32" id="Seg_3353" s="T31">timnʼä-si-k</ta>
            <ta e="T33" id="Seg_3354" s="T32">mora-t</ta>
            <ta e="T34" id="Seg_3355" s="T33">təː</ta>
            <ta e="T35" id="Seg_3356" s="T34">na</ta>
            <ta e="T36" id="Seg_3357" s="T35">särɨ</ta>
            <ta e="T37" id="Seg_3358" s="T36">qorqɨ-p</ta>
            <ta e="T38" id="Seg_3359" s="T37">qät-qa</ta>
            <ta e="T39" id="Seg_3360" s="T38">qən-nɔː-tɨt</ta>
            <ta e="T40" id="Seg_3361" s="T39">täp-ɨ-t</ta>
            <ta e="T41" id="Seg_3362" s="T40">qän-nɔː-tɨt</ta>
            <ta e="T42" id="Seg_3363" s="T41">na</ta>
            <ta e="T43" id="Seg_3364" s="T42">särɨ</ta>
            <ta e="T44" id="Seg_3365" s="T43">qorqɨ-p</ta>
            <ta e="T45" id="Seg_3366" s="T44">əmɨ-ntɨ</ta>
            <ta e="T46" id="Seg_3367" s="T45">čʼatɨ-ŋɨ-tɨ</ta>
            <ta e="T47" id="Seg_3368" s="T46">nɨnɨ</ta>
            <ta e="T48" id="Seg_3369" s="T47">sərɨ</ta>
            <ta e="T49" id="Seg_3370" s="T48">qorqɨ-p</ta>
            <ta e="T51" id="Seg_3371" s="T50">na</ta>
            <ta e="T52" id="Seg_3372" s="T51">särɨ</ta>
            <ta e="T53" id="Seg_3373" s="T52">qorqɨ</ta>
            <ta e="T54" id="Seg_3374" s="T53">mompa</ta>
            <ta e="T55" id="Seg_3375" s="T54">märqɨ</ta>
            <ta e="T56" id="Seg_3376" s="T55">ɛː-ŋa</ta>
            <ta e="T57" id="Seg_3377" s="T56">nɨːnɨ</ta>
            <ta e="T58" id="Seg_3378" s="T57">təp-ɨ-t</ta>
            <ta e="T59" id="Seg_3379" s="T58">qəl-la</ta>
            <ta e="T60" id="Seg_3380" s="T59">nɔːnnɨ-n-tɨ</ta>
            <ta e="T61" id="Seg_3381" s="T60">kɨm-ɨ-t</ta>
            <ta e="T62" id="Seg_3382" s="T61">sitɨ</ta>
            <ta e="T63" id="Seg_3383" s="T62">korɔː-tɨt</ta>
            <ta e="T64" id="Seg_3384" s="T63">aj</ta>
            <ta e="T65" id="Seg_3385" s="T64">moqal-mɨn-tɨ</ta>
            <ta e="T66" id="Seg_3386" s="T65">sitɨ</ta>
            <ta e="T67" id="Seg_3387" s="T66">korɔː-t</ta>
            <ta e="T68" id="Seg_3388" s="T67">nɨːnɨ</ta>
            <ta e="T69" id="Seg_3389" s="T68">na</ta>
            <ta e="T70" id="Seg_3390" s="T69">šitɨ</ta>
            <ta e="T71" id="Seg_3391" s="T70">tɨmnʼa-t</ta>
            <ta e="T72" id="Seg_3392" s="T71">ınna</ta>
            <ta e="T73" id="Seg_3393" s="T72">qaqla-qɨn-tɨ</ta>
            <ta e="T74" id="Seg_3394" s="T73">tɛltɔː-tɨn</ta>
            <ta e="T75" id="Seg_3395" s="T74">na</ta>
            <ta e="T76" id="Seg_3396" s="T75">timnʼä-sɨ-t</ta>
            <ta e="T77" id="Seg_3397" s="T76">moqonä</ta>
            <ta e="T78" id="Seg_3398" s="T77">laqaltɔː-tɨt</ta>
            <ta e="T79" id="Seg_3399" s="T78">tına</ta>
            <ta e="T80" id="Seg_3400" s="T79">čʼonta-qɨ-lʼ</ta>
            <ta e="T81" id="Seg_3401" s="T80">sərɨ</ta>
            <ta e="T82" id="Seg_3402" s="T81">qorqɨ</ta>
            <ta e="T83" id="Seg_3403" s="T82">qät-pilʼ</ta>
            <ta e="T84" id="Seg_3404" s="T83">timnʼa-tɨt</ta>
            <ta e="T85" id="Seg_3405" s="T84">čʼonta-qɨt</ta>
            <ta e="T86" id="Seg_3406" s="T85">moqɨnɨ</ta>
            <ta e="T87" id="Seg_3407" s="T86">iː-ppa</ta>
            <ta e="T88" id="Seg_3408" s="T87">nɨːnɨ</ta>
            <ta e="T89" id="Seg_3409" s="T88">na</ta>
            <ta e="T90" id="Seg_3410" s="T89">qup</ta>
            <ta e="T91" id="Seg_3411" s="T90">üru-kku-mpa</ta>
            <ta e="T92" id="Seg_3412" s="T91">ompa</ta>
            <ta e="T93" id="Seg_3413" s="T92">čʼu-psa</ta>
            <ta e="T94" id="Seg_3414" s="T93">montɨ-qɨt</ta>
            <ta e="T95" id="Seg_3415" s="T94">ɛː-ppa</ta>
            <ta e="T97" id="Seg_3416" s="T95">pusqa-t-ɔːl-pa</ta>
            <ta e="T98" id="Seg_3417" s="T97">təp</ta>
            <ta e="T99" id="Seg_3418" s="T98">nɨːnɨ</ta>
            <ta e="T100" id="Seg_3419" s="T99">sä</ta>
            <ta e="T101" id="Seg_3420" s="T100">orɨm-kɨlɨm-pa</ta>
            <ta e="T102" id="Seg_3421" s="T101">təp</ta>
            <ta e="T103" id="Seg_3422" s="T102">na</ta>
            <ta e="T104" id="Seg_3423" s="T103">mora-t</ta>
            <ta e="T105" id="Seg_3424" s="T104">qanäq-qɨt</ta>
            <ta e="T106" id="Seg_3425" s="T105">nɨːntɨ</ta>
            <ta e="T107" id="Seg_3426" s="T106">na</ta>
            <ta e="T108" id="Seg_3427" s="T107">ippɨ-mmɨ-ntɨ</ta>
            <ta e="T109" id="Seg_3428" s="T108">təp</ta>
            <ta e="T110" id="Seg_3429" s="T109">nɨmtɨ</ta>
            <ta e="T111" id="Seg_3430" s="T110">ippɨ-mpa</ta>
            <ta e="T112" id="Seg_3431" s="T111">nɔːtɨ</ta>
            <ta e="T113" id="Seg_3432" s="T112">säː-t</ta>
            <ta e="T114" id="Seg_3433" s="T113">orɨm-kɨlɨm-pa</ta>
            <ta e="T115" id="Seg_3434" s="T114">nɔːt</ta>
            <ta e="T116" id="Seg_3435" s="T115">čʼu-mpa</ta>
            <ta e="T117" id="Seg_3436" s="T116">soqɨ-tɨ</ta>
            <ta e="T118" id="Seg_3437" s="T117">peːmɨ-tɨ</ta>
            <ta e="T119" id="Seg_3438" s="T118">ola</ta>
            <ta e="T120" id="Seg_3439" s="T119">sičʼi</ta>
            <ta e="T121" id="Seg_3440" s="T120">nɨːnɨ</ta>
            <ta e="T122" id="Seg_3441" s="T121">nʼennä</ta>
            <ta e="T123" id="Seg_3442" s="T122">qɨn-talta</ta>
            <ta e="T124" id="Seg_3443" s="T123">üːnɨ-m-tɨ</ta>
            <ta e="T125" id="Seg_3444" s="T124">paŋɨ-sa</ta>
            <ta e="T126" id="Seg_3445" s="T125">matɨ-ŋɨ-tɨ</ta>
            <ta e="T127" id="Seg_3446" s="T126">na</ta>
            <ta e="T128" id="Seg_3447" s="T127">nɔːkɨr</ta>
            <ta e="T129" id="Seg_3448" s="T128">quptɨ-tɨ</ta>
            <ta e="T130" id="Seg_3449" s="T129">nɨːnɨ</ta>
            <ta e="T131" id="Seg_3450" s="T130">pakta</ta>
            <ta e="T132" id="Seg_3451" s="T131">təp</ta>
            <ta e="T133" id="Seg_3452" s="T132">ontɨ</ta>
            <ta e="T134" id="Seg_3453" s="T133">nɨmtɨ</ta>
            <ta e="T135" id="Seg_3454" s="T134">ippɨ-mpa</ta>
            <ta e="T136" id="Seg_3455" s="T135">ɔːmä-j</ta>
            <ta e="T137" id="Seg_3456" s="T136">qontɨ-kkä</ta>
            <ta e="T138" id="Seg_3457" s="T137">nɨːnɨ</ta>
            <ta e="T139" id="Seg_3458" s="T138">taŋɨ-mpa</ta>
            <ta e="T140" id="Seg_3459" s="T139">namɨšak</ta>
            <ta e="T141" id="Seg_3460" s="T140">apsɨ-tqa</ta>
            <ta e="T142" id="Seg_3461" s="T141">qu-mpa</ta>
            <ta e="T143" id="Seg_3462" s="T142">nop</ta>
            <ta e="T144" id="Seg_3463" s="T143">taŋɨ-mpa</ta>
            <ta e="T145" id="Seg_3464" s="T144">qaqlɨ-n-tɨ</ta>
            <ta e="T146" id="Seg_3465" s="T145">šünčʼa-qɨt</ta>
            <ta e="T147" id="Seg_3466" s="T146">qontɨ-kka</ta>
            <ta e="T148" id="Seg_3467" s="T147">namɨša-lʼ</ta>
            <ta e="T149" id="Seg_3468" s="T148">ulqa-n-tɨ</ta>
            <ta e="T150" id="Seg_3469" s="T149">laka</ta>
            <ta e="T151" id="Seg_3470" s="T150">qalɨ-n-tɨ</ta>
            <ta e="T152" id="Seg_3471" s="T151">ɨl-qɨt</ta>
            <ta e="T153" id="Seg_3472" s="T152">qalʼi-mmɨ-ntɨ</ta>
            <ta e="T154" id="Seg_3473" s="T153">mora</ta>
            <ta e="T155" id="Seg_3474" s="T154">muntɨk</ta>
            <ta e="T156" id="Seg_3475" s="T155">čʼu-mpa</ta>
            <ta e="T157" id="Seg_3476" s="T156">kur</ta>
            <ta e="T158" id="Seg_3477" s="T157">ukkur</ta>
            <ta e="T159" id="Seg_3478" s="T158">čʼonta-qɨt</ta>
            <ta e="T160" id="Seg_3479" s="T159">täp</ta>
            <ta e="T161" id="Seg_3480" s="T160">qos</ta>
            <ta e="T162" id="Seg_3481" s="T161">qaj</ta>
            <ta e="T163" id="Seg_3482" s="T162">nılʼčʼi-k</ta>
            <ta e="T164" id="Seg_3483" s="T163">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T165" id="Seg_3484" s="T164">qaj</ta>
            <ta e="T166" id="Seg_3485" s="T165">nop</ta>
            <ta e="T167" id="Seg_3486" s="T166">lʼi</ta>
            <ta e="T168" id="Seg_3487" s="T167">qaj</ta>
            <ta e="T169" id="Seg_3488" s="T168">qup</ta>
            <ta e="T170" id="Seg_3489" s="T169">tämaj-sä</ta>
            <ta e="T171" id="Seg_3490" s="T170">kuru-lʼä</ta>
            <ta e="T172" id="Seg_3491" s="T171">alʼ</ta>
            <ta e="T173" id="Seg_3492" s="T172">nılʼčʼi-k</ta>
            <ta e="T174" id="Seg_3493" s="T173">kuka</ta>
            <ta e="T175" id="Seg_3494" s="T174">təp</ta>
            <ta e="T176" id="Seg_3495" s="T175">ınna</ta>
            <ta e="T177" id="Seg_3496" s="T176">sʼäp</ta>
            <ta e="T178" id="Seg_3497" s="T177">antalu-mpa</ta>
            <ta e="T179" id="Seg_3498" s="T178">nılʼčʼi-k</ta>
            <ta e="T180" id="Seg_3499" s="T179">kuka</ta>
            <ta e="T181" id="Seg_3500" s="T180">kekkɨsa</ta>
            <ta e="T182" id="Seg_3501" s="T181">muːt</ta>
            <ta e="T183" id="Seg_3502" s="T182">tınolʼa</ta>
            <ta e="T184" id="Seg_3503" s="T183">at-ali-mmɨ-ntɔː-tɨt</ta>
            <ta e="T185" id="Seg_3504" s="T184">na</ta>
            <ta e="T186" id="Seg_3505" s="T185">qaqlʼi-n-tɨ</ta>
            <ta e="T187" id="Seg_3506" s="T186">il-qɨ-lʼ</ta>
            <ta e="T188" id="Seg_3507" s="T187">ulqa-lʼ</ta>
            <ta e="T189" id="Seg_3508" s="T188">laka-sä</ta>
            <ta e="T190" id="Seg_3509" s="T189">qompɨ-r-lʼä</ta>
            <ta e="T191" id="Seg_3510" s="T190">nɨːnä</ta>
            <ta e="T192" id="Seg_3511" s="T191">aj</ta>
            <ta e="T193" id="Seg_3512" s="T192">ılla</ta>
            <ta e="T194" id="Seg_3513" s="T193">qonta</ta>
            <ta e="T195" id="Seg_3514" s="T194">aj</ta>
            <ta e="T196" id="Seg_3515" s="T195">täp</ta>
            <ta e="T197" id="Seg_3516" s="T196">qos</ta>
            <ta e="T198" id="Seg_3517" s="T197">qaj</ta>
            <ta e="T199" id="Seg_3518" s="T198">nılʼčʼi-k</ta>
            <ta e="T200" id="Seg_3519" s="T199">šıp</ta>
            <ta e="T201" id="Seg_3520" s="T200">soqočʼ-čʼi</ta>
            <ta e="T202" id="Seg_3521" s="T201">qup</ta>
            <ta e="T203" id="Seg_3522" s="T202">nılʼčʼi-k</ta>
            <ta e="T204" id="Seg_3523" s="T203">ıla</ta>
            <ta e="T205" id="Seg_3524" s="T204">kuro-la</ta>
            <ta e="T206" id="Seg_3525" s="T205">kuka</ta>
            <ta e="T207" id="Seg_3526" s="T206">nɨːnɨ</ta>
            <ta e="T208" id="Seg_3527" s="T207">qanɨq-tɨ</ta>
            <ta e="T209" id="Seg_3528" s="T208">konna</ta>
            <ta e="T210" id="Seg_3529" s="T209">tanta</ta>
            <ta e="T211" id="Seg_3530" s="T210">tannɨ-mpa</ta>
            <ta e="T212" id="Seg_3531" s="T211">saji-tɨ</ta>
            <ta e="T213" id="Seg_3532" s="T212">namɨšak</ta>
            <ta e="T214" id="Seg_3533" s="T213">awsɨ-tqa</ta>
            <ta e="T215" id="Seg_3534" s="T214">qu-mpa</ta>
            <ta e="T216" id="Seg_3535" s="T215">qɨlʼ-a-t</ta>
            <ta e="T217" id="Seg_3536" s="T216">šünʼa-ntɨ</ta>
            <ta e="T218" id="Seg_3537" s="T217">tokɨ-mɔːt-pa</ta>
            <ta e="T219" id="Seg_3538" s="T218">ukkur</ta>
            <ta e="T220" id="Seg_3539" s="T219">čʼonta-qɨt</ta>
            <ta e="T221" id="Seg_3540" s="T220">nılʼčʼi-k</ta>
            <ta e="T222" id="Seg_3541" s="T221">ɛː-sa</ta>
            <ta e="T223" id="Seg_3542" s="T222">mitä</ta>
            <ta e="T224" id="Seg_3543" s="T223">tətɨ</ta>
            <ta e="T225" id="Seg_3544" s="T224">atä-ntɨ</ta>
            <ta e="T226" id="Seg_3545" s="T225">mittɨ</ta>
            <ta e="T227" id="Seg_3546" s="T226">təttɨ</ta>
            <ta e="T228" id="Seg_3547" s="T227">čʼu-r-ɨ-ntɨ</ta>
            <ta e="T229" id="Seg_3548" s="T228">mora-t</ta>
            <ta e="T230" id="Seg_3549" s="T229">qanɨq-qɨt</ta>
            <ta e="T231" id="Seg_3550" s="T230">tep</ta>
            <ta e="T232" id="Seg_3551" s="T231">namɨsʼak</ta>
            <ta e="T233" id="Seg_3552" s="T232">apsɨ-tqa</ta>
            <ta e="T234" id="Seg_3553" s="T233">qu-mpa</ta>
            <ta e="T235" id="Seg_3554" s="T234">nɨːnɨ</ta>
            <ta e="T236" id="Seg_3555" s="T235">nılʼčʼi-k</ta>
            <ta e="T237" id="Seg_3556" s="T236">sʼäp</ta>
            <ta e="T238" id="Seg_3557" s="T237">qäŋ-ŋa</ta>
            <ta e="T239" id="Seg_3558" s="T238">qaqlʼa-p</ta>
            <ta e="T240" id="Seg_3559" s="T239">tətɨ-n</ta>
            <ta e="T241" id="Seg_3560" s="T240">məna-tɨ</ta>
            <ta e="T242" id="Seg_3561" s="T241">tuli-s-pa</ta>
            <ta e="T243" id="Seg_3562" s="T242">nɨːnɨ</ta>
            <ta e="T244" id="Seg_3563" s="T243">qaqlɨ-n-tɨ</ta>
            <ta e="T245" id="Seg_3564" s="T244">moqal-i-t</ta>
            <ta e="T246" id="Seg_3565" s="T245">iː-mɨt</ta>
            <ta e="T247" id="Seg_3566" s="T246">soqo-lʼä</ta>
            <ta e="T248" id="Seg_3567" s="T247">kunna</ta>
            <ta e="T249" id="Seg_3568" s="T248">tanta</ta>
            <ta e="T250" id="Seg_3569" s="T249">nɨːnɨ</ta>
            <ta e="T251" id="Seg_3570" s="T250">konna</ta>
            <ta e="T252" id="Seg_3571" s="T251">qanɨq-tɨ</ta>
            <ta e="T253" id="Seg_3572" s="T252">alʼčʼe</ta>
            <ta e="T254" id="Seg_3573" s="T253">nɨːnɨ</ta>
            <ta e="T255" id="Seg_3574" s="T254">nɨmtɨ</ta>
            <ta e="T256" id="Seg_3575" s="T255">ippa</ta>
            <ta e="T257" id="Seg_3576" s="T256">aj</ta>
            <ta e="T258" id="Seg_3577" s="T257">qos</ta>
            <ta e="T259" id="Seg_3578" s="T258">qaj</ta>
            <ta e="T260" id="Seg_3579" s="T259">nılʼčʼi-k</ta>
            <ta e="T261" id="Seg_3580" s="T260">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T262" id="Seg_3581" s="T261">qup</ta>
            <ta e="T263" id="Seg_3582" s="T262">təma-sä</ta>
            <ta e="T264" id="Seg_3583" s="T263">kurɨ-la</ta>
            <ta e="T265" id="Seg_3584" s="T264">*qup-na</ta>
            <ta e="T266" id="Seg_3585" s="T265">konna-t</ta>
            <ta e="T267" id="Seg_3586" s="T266">nʼärqɨ-lʼ</ta>
            <ta e="T268" id="Seg_3587" s="T267">markɨ-t</ta>
            <ta e="T269" id="Seg_3588" s="T268">toːp-ɨ-ntɨ</ta>
            <ta e="T270" id="Seg_3589" s="T269">soqɨ-lʼa</ta>
            <ta e="T271" id="Seg_3590" s="T270">konna</ta>
            <ta e="T272" id="Seg_3591" s="T271">tanta</ta>
            <ta e="T273" id="Seg_3592" s="T272">nɨːna</ta>
            <ta e="T274" id="Seg_3593" s="T273">konna</ta>
            <ta e="T275" id="Seg_3594" s="T274">soqqo-lʼa</ta>
            <ta e="T276" id="Seg_3595" s="T275">tanta</ta>
            <ta e="T277" id="Seg_3596" s="T276">mat</ta>
            <ta e="T278" id="Seg_3597" s="T277">qotä</ta>
            <ta e="T279" id="Seg_3598" s="T278">ınna</ta>
            <ta e="T280" id="Seg_3599" s="T279">nɨl-aıː-sä-ŋ</ta>
            <ta e="T281" id="Seg_3600" s="T280">ɛna</ta>
            <ta e="T282" id="Seg_3601" s="T281">konna</ta>
            <ta e="T283" id="Seg_3602" s="T282">soqqo-lʼa</ta>
            <ta e="T284" id="Seg_3603" s="T283">tanta</ta>
            <ta e="T285" id="Seg_3604" s="T284">or-tɨ</ta>
            <ta e="T286" id="Seg_3605" s="T285">meːltɨ</ta>
            <ta e="T287" id="Seg_3606" s="T286">čʼäːŋka</ta>
            <ta e="T288" id="Seg_3607" s="T287">aj</ta>
            <ta e="T289" id="Seg_3608" s="T288">qos</ta>
            <ta e="T290" id="Seg_3609" s="T289">qaj</ta>
            <ta e="T291" id="Seg_3610" s="T290">nılʼčʼi-k</ta>
            <ta e="T292" id="Seg_3611" s="T291">soqalʼ-lʼi-tɨ</ta>
            <ta e="T293" id="Seg_3612" s="T292">qup</ta>
            <ta e="T294" id="Seg_3613" s="T293">aša</ta>
            <ta e="T295" id="Seg_3614" s="T294">nılʼčʼi-k</ta>
            <ta e="T296" id="Seg_3615" s="T295">kuka</ta>
            <ta e="T297" id="Seg_3616" s="T296">kuro-lʼa</ta>
            <ta e="T298" id="Seg_3617" s="T297">na</ta>
            <ta e="T299" id="Seg_3618" s="T298">mora-t</ta>
            <ta e="T300" id="Seg_3619" s="T299">qanɨq-qät</ta>
            <ta e="T301" id="Seg_3620" s="T300">pirqə</ta>
            <ta e="T302" id="Seg_3621" s="T301">təttɨ</ta>
            <ta e="T303" id="Seg_3622" s="T302">i̇nna</ta>
            <ta e="T304" id="Seg_3623" s="T303">sɨq-olʼ-na</ta>
            <ta e="T305" id="Seg_3624" s="T304">nɨːnɨ</ta>
            <ta e="T306" id="Seg_3625" s="T305">konna</ta>
            <ta e="T307" id="Seg_3626" s="T306">sɨqɨ-lʼä</ta>
            <ta e="T308" id="Seg_3627" s="T307">nılʼčʼi-k</ta>
            <ta e="T309" id="Seg_3628" s="T308">mannɨ-mpa</ta>
            <ta e="T310" id="Seg_3629" s="T309">konna-t</ta>
            <ta e="T311" id="Seg_3630" s="T310">nılʼčʼi-k</ta>
            <ta e="T312" id="Seg_3631" s="T311">qo-ŋɨ-tɨ</ta>
            <ta e="T313" id="Seg_3632" s="T312">kotɨlʼ</ta>
            <ta e="T314" id="Seg_3633" s="T313">mɔːt</ta>
            <ta e="T315" id="Seg_3634" s="T314">ɔːmnɨ-ntɨ</ta>
            <ta e="T316" id="Seg_3635" s="T315">nɨːnɨ</ta>
            <ta e="T317" id="Seg_3636" s="T316">aj</ta>
            <ta e="T318" id="Seg_3637" s="T317">ılla</ta>
            <ta e="T319" id="Seg_3638" s="T318">olʼčʼe-ja</ta>
            <ta e="T320" id="Seg_3639" s="T319">kutar</ta>
            <ta e="T321" id="Seg_3640" s="T320">nɨčʼa</ta>
            <ta e="T322" id="Seg_3641" s="T321">tuli-čʼ-čʼ-antɨ-k</ta>
            <ta e="T323" id="Seg_3642" s="T322">nɨːnɨ</ta>
            <ta e="T324" id="Seg_3643" s="T323">na</ta>
            <ta e="T325" id="Seg_3644" s="T324">mɔːt-tɨ</ta>
            <ta e="T326" id="Seg_3645" s="T325">konna</ta>
            <ta e="T327" id="Seg_3646" s="T326">nɨlʼ-lʼa</ta>
            <ta e="T328" id="Seg_3647" s="T327">ılla</ta>
            <ta e="T329" id="Seg_3648" s="T328">tü-ŋa</ta>
            <ta e="T330" id="Seg_3649" s="T329">qälʼi</ta>
            <ta e="T331" id="Seg_3650" s="T330">mɔːt</ta>
            <ta e="T332" id="Seg_3651" s="T331">ɔːmnɨ-ntɨ</ta>
            <ta e="T333" id="Seg_3652" s="T332">ɔːta-tɨ</ta>
            <ta e="T334" id="Seg_3653" s="T333">etɨmantoː-qɨt</ta>
            <ta e="T335" id="Seg_3654" s="T334">kur-ali-mmɨ-ntɔː-tɨt</ta>
            <ta e="T336" id="Seg_3655" s="T335">nɨːnɨ</ta>
            <ta e="T337" id="Seg_3656" s="T336">mɔːt-tɨ</ta>
            <ta e="T338" id="Seg_3657" s="T337">šer-na</ta>
            <ta e="T339" id="Seg_3658" s="T338">ira</ta>
            <ta e="T340" id="Seg_3659" s="T339">ukkur</ta>
            <ta e="T341" id="Seg_3660" s="T340">nälʼa-tɨ</ta>
            <ta e="T342" id="Seg_3661" s="T341">ukkur</ta>
            <ta e="T343" id="Seg_3662" s="T342">čʼontoː-qɨt</ta>
            <ta e="T521" id="Seg_3663" s="T343">qäl</ta>
            <ta e="T344" id="Seg_3664" s="T521">ira</ta>
            <ta e="T345" id="Seg_3665" s="T344">nılʼčʼi-k</ta>
            <ta e="T346" id="Seg_3666" s="T345">ɛsa</ta>
            <ta e="T347" id="Seg_3667" s="T346">tat</ta>
            <ta e="T348" id="Seg_3668" s="T347">na</ta>
            <ta e="T349" id="Seg_3669" s="T348">qup</ta>
            <ta e="T350" id="Seg_3670" s="T349">aws-t-äti</ta>
            <ta e="T351" id="Seg_3671" s="T350">täp</ta>
            <ta e="T352" id="Seg_3672" s="T351">na</ta>
            <ta e="T353" id="Seg_3673" s="T352">ira-n</ta>
            <ta e="T354" id="Seg_3674" s="T353">mɨ-qɨt</ta>
            <ta e="T355" id="Seg_3675" s="T354">šitɨ</ta>
            <ta e="T356" id="Seg_3676" s="T355">toːt</ta>
            <ta e="T357" id="Seg_3677" s="T356">ɔːta-t</ta>
            <ta e="T358" id="Seg_3678" s="T357">mat</ta>
            <ta e="T359" id="Seg_3679" s="T358">ukkur</ta>
            <ta e="T360" id="Seg_3680" s="T359">toːt</ta>
            <ta e="T361" id="Seg_3681" s="T360">ɔːta-mɨ</ta>
            <ta e="T362" id="Seg_3682" s="T361">suːrup</ta>
            <ta e="T363" id="Seg_3683" s="T362">muntɨk</ta>
            <ta e="T364" id="Seg_3684" s="T363">am-nɨ-tɨ</ta>
            <ta e="T365" id="Seg_3685" s="T364">ira</ta>
            <ta e="T366" id="Seg_3686" s="T365">nılʼčʼi-k</ta>
            <ta e="T367" id="Seg_3687" s="T366">kətɨ-ŋɨ-t</ta>
            <ta e="T368" id="Seg_3688" s="T367">tan</ta>
            <ta e="T369" id="Seg_3689" s="T368">pona</ta>
            <ta e="T370" id="Seg_3690" s="T369">tantɨ-lʼä</ta>
            <ta e="T371" id="Seg_3691" s="T370">mat</ta>
            <ta e="T372" id="Seg_3692" s="T371">ɔːta-mɨ</ta>
            <ta e="T373" id="Seg_3693" s="T372">ɔːt-ätɨ</ta>
            <ta e="T374" id="Seg_3694" s="T373">lʼup-k-ɨ-mɔːn-nä</ta>
            <ta e="T375" id="Seg_3695" s="T374">täp</ta>
            <ta e="T376" id="Seg_3696" s="T375">pona</ta>
            <ta e="T377" id="Seg_3697" s="T376">tanta</ta>
            <ta e="T378" id="Seg_3698" s="T377">täp</ta>
            <ta e="T379" id="Seg_3699" s="T378">pona</ta>
            <ta e="T380" id="Seg_3700" s="T379">tanta</ta>
            <ta e="T381" id="Seg_3701" s="T380">namɨšak</ta>
            <ta e="T382" id="Seg_3702" s="T381">seqelʼi-mpɨ</ta>
            <ta e="T383" id="Seg_3703" s="T382">qup</ta>
            <ta e="T384" id="Seg_3704" s="T383">qaj-tɨ</ta>
            <ta e="T385" id="Seg_3705" s="T384">mannɨ-mm-ɛntɨ-t</ta>
            <ta e="T386" id="Seg_3706" s="T385">nɨːnɨ</ta>
            <ta e="T387" id="Seg_3707" s="T386">täp</ta>
            <ta e="T388" id="Seg_3708" s="T387">aj</ta>
            <ta e="T389" id="Seg_3709" s="T388">ılla</ta>
            <ta e="T390" id="Seg_3710" s="T389">alʼčʼi-mpa</ta>
            <ta e="T391" id="Seg_3711" s="T390">nɨmtɨ</ta>
            <ta e="T392" id="Seg_3712" s="T391">na</ta>
            <ta e="T393" id="Seg_3713" s="T392">ippɨ-mmɨ-ntɨ</ta>
            <ta e="T394" id="Seg_3714" s="T393">na</ta>
            <ta e="T395" id="Seg_3715" s="T394">täp-ɨ-t</ta>
            <ta e="T396" id="Seg_3716" s="T395">tü-lʼä</ta>
            <ta e="T397" id="Seg_3717" s="T396">puːla</ta>
            <ta e="T398" id="Seg_3718" s="T397">suːrup</ta>
            <ta e="T399" id="Seg_3719" s="T398">na</ta>
            <ta e="T400" id="Seg_3720" s="T399">qən-mɨ-ntɨ</ta>
            <ta e="T401" id="Seg_3721" s="T400">nɨː</ta>
            <ta e="T402" id="Seg_3722" s="T401">ukoːt</ta>
            <ta e="T403" id="Seg_3723" s="T402">äsä-sɨ-qäːqi</ta>
            <ta e="T404" id="Seg_3724" s="T403">ɔːta-nti-j</ta>
            <ta e="T405" id="Seg_3725" s="T404">ɔːtɨ-lä</ta>
            <ta e="T406" id="Seg_3726" s="T405">poː-qot</ta>
            <ta e="T407" id="Seg_3727" s="T406">ili-mpɔː-qij</ta>
            <ta e="T408" id="Seg_3728" s="T407">nɨːnɨ</ta>
            <ta e="T409" id="Seg_3729" s="T408">na</ta>
            <ta e="T410" id="Seg_3730" s="T409">qum-u-p</ta>
            <ta e="T411" id="Seg_3731" s="T410">aws-tɨ-mpɨ-lʼä</ta>
            <ta e="T412" id="Seg_3732" s="T411">nɨmtɨ</ta>
            <ta e="T413" id="Seg_3733" s="T412">na</ta>
            <ta e="T414" id="Seg_3734" s="T413">il-le-mmɨ-ntɨ</ta>
            <ta e="T415" id="Seg_3735" s="T414">nɨːnä</ta>
            <ta e="T416" id="Seg_3736" s="T415">na</ta>
            <ta e="T417" id="Seg_3737" s="T416">ire</ta>
            <ta e="T418" id="Seg_3738" s="T417">aj</ta>
            <ta e="T419" id="Seg_3739" s="T418">nılʼčʼi-k</ta>
            <ta e="T420" id="Seg_3740" s="T419">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T421" id="Seg_3741" s="T420">aj</ta>
            <ta e="T422" id="Seg_3742" s="T421">qəl-asik</ta>
            <ta e="T423" id="Seg_3743" s="T422">ɔːta-m-tɨ</ta>
            <ta e="T424" id="Seg_3744" s="T423">ɔːtɨ-lʼä</ta>
            <ta e="T425" id="Seg_3745" s="T424">ɔːta-tɨ</ta>
            <ta e="T426" id="Seg_3746" s="T425">qapije</ta>
            <ta e="T427" id="Seg_3747" s="T426">mačʼa-ntɨ</ta>
            <ta e="T428" id="Seg_3748" s="T427">asʼa</ta>
            <ta e="T429" id="Seg_3749" s="T428">qən-ta</ta>
            <ta e="T430" id="Seg_3750" s="T429">mɔːt-ɨ-t</ta>
            <ta e="T431" id="Seg_3751" s="T430">qanɨq-qɨt</ta>
            <ta e="T432" id="Seg_3752" s="T431">qontɔː-tɨt</ta>
            <ta e="T433" id="Seg_3753" s="T432">nɨːnä</ta>
            <ta e="T434" id="Seg_3754" s="T433">täp</ta>
            <ta e="T435" id="Seg_3755" s="T434">pona</ta>
            <ta e="T436" id="Seg_3756" s="T435">tanta</ta>
            <ta e="T437" id="Seg_3757" s="T436">seːpɨlʼa-lʼ</ta>
            <ta e="T438" id="Seg_3758" s="T437">mɨ-ntɨ</ta>
            <ta e="T439" id="Seg_3759" s="T438">qəl-la</ta>
            <ta e="T440" id="Seg_3760" s="T439">aj</ta>
            <ta e="T441" id="Seg_3761" s="T440">ılla</ta>
            <ta e="T442" id="Seg_3762" s="T441">olʼčʼi</ta>
            <ta e="T443" id="Seg_3763" s="T442">nɨːnɨ</ta>
            <ta e="T444" id="Seg_3764" s="T443">təp</ta>
            <ta e="T445" id="Seg_3765" s="T444">ukur</ta>
            <ta e="T446" id="Seg_3766" s="T445">čʼontoː-qɨt</ta>
            <ta e="T447" id="Seg_3767" s="T446">nılʼčʼi-k</ta>
            <ta e="T448" id="Seg_3768" s="T447">šitɨ-je</ta>
            <ta e="T449" id="Seg_3769" s="T448">qaj</ta>
            <ta e="T450" id="Seg_3770" s="T449">qən-tɨ-tɨ</ta>
            <ta e="T451" id="Seg_3771" s="T450">montɨ</ta>
            <ta e="T452" id="Seg_3772" s="T451">il-ɛčʼa</ta>
            <ta e="T453" id="Seg_3773" s="T452">nɨːnɨ</ta>
            <ta e="T454" id="Seg_3774" s="T453">moqona</ta>
            <ta e="T455" id="Seg_3775" s="T454">tü-ŋa</ta>
            <ta e="T456" id="Seg_3776" s="T455">mɔːt-tɨ</ta>
            <ta e="T522" id="Seg_3777" s="T456">qäli</ta>
            <ta e="T457" id="Seg_3778" s="T522">ira</ta>
            <ta e="T458" id="Seg_3779" s="T457">qonta</ta>
            <ta e="T459" id="Seg_3780" s="T458">nɨːnɨ</ta>
            <ta e="T460" id="Seg_3781" s="T459">təp-ɨ-tɨt</ta>
            <ta e="T461" id="Seg_3782" s="T460">ɔːta</ta>
            <ta e="T462" id="Seg_3783" s="T461">meːltɨ</ta>
            <ta e="T463" id="Seg_3784" s="T462">etimontoː-qɨt</ta>
            <ta e="T464" id="Seg_3785" s="T463">qontɔː-tɨt</ta>
            <ta e="T465" id="Seg_3786" s="T464">təp</ta>
            <ta e="T466" id="Seg_3787" s="T465">na</ta>
            <ta e="T467" id="Seg_3788" s="T466">tü-pilʼ</ta>
            <ta e="T468" id="Seg_3789" s="T467">porqɨ-lʼ</ta>
            <ta e="T469" id="Seg_3790" s="T468">peːmɨ-t-ɨ-sa</ta>
            <ta e="T470" id="Seg_3791" s="T469">ila</ta>
            <ta e="T471" id="Seg_3792" s="T470">nɨːnɨ</ta>
            <ta e="T472" id="Seg_3793" s="T471">nɔːkɨr-ɨ-mtälʼi</ta>
            <ta e="T473" id="Seg_3794" s="T472">pi-t</ta>
            <ta e="T474" id="Seg_3795" s="T473">nılʼčʼi-k</ta>
            <ta e="T475" id="Seg_3796" s="T474">ɛsa</ta>
            <ta e="T476" id="Seg_3797" s="T475">mitɨ</ta>
            <ta e="T477" id="Seg_3798" s="T476">mol</ta>
            <ta e="T478" id="Seg_3799" s="T477">soma-k</ta>
            <ta e="T479" id="Seg_3800" s="T478">ɛsa-k</ta>
            <ta e="T523" id="Seg_3801" s="T479">qäli</ta>
            <ta e="T480" id="Seg_3802" s="T523">ira</ta>
            <ta e="T481" id="Seg_3803" s="T480">ɔːta-tɨ</ta>
            <ta e="T482" id="Seg_3804" s="T481">šöt-tɨ</ta>
            <ta e="T483" id="Seg_3805" s="T482">asʼa</ta>
            <ta e="T484" id="Seg_3806" s="T483">qən-ta</ta>
            <ta e="T485" id="Seg_3807" s="T484">mɔːt-ɨ-t</ta>
            <ta e="T486" id="Seg_3808" s="T485">qanaq-qɨt</ta>
            <ta e="T487" id="Seg_3809" s="T486">nɨmtɨ</ta>
            <ta e="T488" id="Seg_3810" s="T487">qən-tɔː-tɨt</ta>
            <ta e="T489" id="Seg_3811" s="T488">täp</ta>
            <ta e="T490" id="Seg_3812" s="T489">tü-la</ta>
            <ta e="T491" id="Seg_3813" s="T490">aj</ta>
            <ta e="T492" id="Seg_3814" s="T491">ılla</ta>
            <ta e="T493" id="Seg_3815" s="T492">qonta-je</ta>
            <ta e="T494" id="Seg_3816" s="T493">tɔːptɨlʼ</ta>
            <ta e="T495" id="Seg_3817" s="T494">qarɨ-t</ta>
            <ta e="T496" id="Seg_3818" s="T495">montɨ</ta>
            <ta e="T497" id="Seg_3819" s="T496">sɨrɨ</ta>
            <ta e="T498" id="Seg_3820" s="T497">pinn-aıː-ntɨ-tɨ</ta>
            <ta e="T499" id="Seg_3821" s="T498">mat</ta>
            <ta e="T500" id="Seg_3822" s="T499">täp</ta>
            <ta e="T501" id="Seg_3823" s="T500">kuttar</ta>
            <ta e="T502" id="Seg_3824" s="T501">tü-sa</ta>
            <ta e="T503" id="Seg_3825" s="T502">ɔːta-mɨ</ta>
            <ta e="T504" id="Seg_3826" s="T503">šöt-tɨ</ta>
            <ta e="T505" id="Seg_3827" s="T504">aša</ta>
            <ta e="T506" id="Seg_3828" s="T505">qən-ta</ta>
            <ta e="T507" id="Seg_3829" s="T506">kər-altä-ttɨ</ta>
            <ta e="T508" id="Seg_3830" s="T507">sɨrɨ-p</ta>
            <ta e="T509" id="Seg_3831" s="T508">pilʼ-lʼa</ta>
            <ta e="T510" id="Seg_3832" s="T509">qəŋ-a-je</ta>
            <ta e="T511" id="Seg_3833" s="T510">soma-k</ta>
            <ta e="T512" id="Seg_3834" s="T511">ılla</ta>
            <ta e="T513" id="Seg_3835" s="T512">na</ta>
            <ta e="T514" id="Seg_3836" s="T513">qəŋ-tɨ</ta>
            <ta e="T515" id="Seg_3837" s="T514">täp</ta>
            <ta e="T516" id="Seg_3838" s="T515">nɔːtɨ</ta>
            <ta e="T517" id="Seg_3839" s="T516">soma-k</ta>
            <ta e="T518" id="Seg_3840" s="T517">ɛsa</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_3841" s="T0">na</ta>
            <ta e="T96" id="Seg_3842" s="T1">qäl</ta>
            <ta e="T2" id="Seg_3843" s="T96">ira</ta>
            <ta e="T3" id="Seg_3844" s="T2">iːja</ta>
            <ta e="T4" id="Seg_3845" s="T3">nılʼčʼɨ-k</ta>
            <ta e="T5" id="Seg_3846" s="T4">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T6" id="Seg_3847" s="T5">man</ta>
            <ta e="T7" id="Seg_3848" s="T6">tam</ta>
            <ta e="T8" id="Seg_3849" s="T7">morä-qɨn</ta>
            <ta e="T9" id="Seg_3850" s="T8">sərɨ</ta>
            <ta e="T10" id="Seg_3851" s="T9">qorqɨ-t</ta>
            <ta e="T11" id="Seg_3852" s="T10">qət-ɛntɨ-tɨ</ta>
            <ta e="T519" id="Seg_3853" s="T11">qäl</ta>
            <ta e="T12" id="Seg_3854" s="T519">ira</ta>
            <ta e="T13" id="Seg_3855" s="T12">nılʼčʼɨ-k</ta>
            <ta e="T14" id="Seg_3856" s="T13">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T15" id="Seg_3857" s="T14">na</ta>
            <ta e="T16" id="Seg_3858" s="T15">ašša</ta>
            <ta e="T17" id="Seg_3859" s="T16">qum-ɨ-t</ta>
            <ta e="T18" id="Seg_3860" s="T17">qət-sɨ</ta>
            <ta e="T19" id="Seg_3861" s="T18">suːrɨm</ta>
            <ta e="T20" id="Seg_3862" s="T19">tan</ta>
            <ta e="T21" id="Seg_3863" s="T20">kuttar</ta>
            <ta e="T22" id="Seg_3864" s="T21">qən-ŋɨ-ntɨ</ta>
            <ta e="T23" id="Seg_3865" s="T22">morä-n</ta>
            <ta e="T24" id="Seg_3866" s="T23">tɔː</ta>
            <ta e="T25" id="Seg_3867" s="T24">qapı</ta>
            <ta e="T26" id="Seg_3868" s="T25">poː-kɨtɨ-lʼ</ta>
            <ta e="T27" id="Seg_3869" s="T26">təttɨ-qɨn</ta>
            <ta e="T28" id="Seg_3870" s="T27">ilɨ-tɨ</ta>
            <ta e="T520" id="Seg_3871" s="T28">qäl</ta>
            <ta e="T29" id="Seg_3872" s="T520">ira</ta>
            <ta e="T30" id="Seg_3873" s="T29">təp-ɨ-n</ta>
            <ta e="T31" id="Seg_3874" s="T30">nɔːkɨr</ta>
            <ta e="T32" id="Seg_3875" s="T31">timnʼa-sɨ-t</ta>
            <ta e="T33" id="Seg_3876" s="T32">morä-n</ta>
            <ta e="T34" id="Seg_3877" s="T33">tɔː</ta>
            <ta e="T35" id="Seg_3878" s="T34">na</ta>
            <ta e="T36" id="Seg_3879" s="T35">sərɨ</ta>
            <ta e="T37" id="Seg_3880" s="T36">qorqɨ-m</ta>
            <ta e="T38" id="Seg_3881" s="T37">qət-qo</ta>
            <ta e="T39" id="Seg_3882" s="T38">qən-ŋɨ-tɨt</ta>
            <ta e="T40" id="Seg_3883" s="T39">təp-ɨ-t</ta>
            <ta e="T41" id="Seg_3884" s="T40">qən-ŋɨ-tɨt</ta>
            <ta e="T42" id="Seg_3885" s="T41">na</ta>
            <ta e="T43" id="Seg_3886" s="T42">sərɨ</ta>
            <ta e="T44" id="Seg_3887" s="T43">qorqɨ-m</ta>
            <ta e="T45" id="Seg_3888" s="T44">ama-ntɨ</ta>
            <ta e="T46" id="Seg_3889" s="T45">čʼattɨ-ŋɨ-tɨ</ta>
            <ta e="T47" id="Seg_3890" s="T46">nɨːnɨ</ta>
            <ta e="T48" id="Seg_3891" s="T47">sərɨ</ta>
            <ta e="T49" id="Seg_3892" s="T48">qorqɨ-m</ta>
            <ta e="T51" id="Seg_3893" s="T50">na</ta>
            <ta e="T52" id="Seg_3894" s="T51">sərɨ</ta>
            <ta e="T53" id="Seg_3895" s="T52">qorqɨ</ta>
            <ta e="T54" id="Seg_3896" s="T53">mompa</ta>
            <ta e="T55" id="Seg_3897" s="T54">wərqɨ</ta>
            <ta e="T56" id="Seg_3898" s="T55">ɛː-ŋɨ</ta>
            <ta e="T57" id="Seg_3899" s="T56">nɨːnɨ</ta>
            <ta e="T58" id="Seg_3900" s="T57">təp-ɨ-t</ta>
            <ta e="T59" id="Seg_3901" s="T58">qət-lä</ta>
            <ta e="T60" id="Seg_3902" s="T59">nɔːntɨ-n-tɨ</ta>
            <ta e="T61" id="Seg_3903" s="T60">kəm-ɨ-t</ta>
            <ta e="T62" id="Seg_3904" s="T61">šittɨ</ta>
            <ta e="T63" id="Seg_3905" s="T62">*korɨ-tɨt</ta>
            <ta e="T64" id="Seg_3906" s="T63">aj</ta>
            <ta e="T65" id="Seg_3907" s="T64">moqal-mɨn-ntɨ</ta>
            <ta e="T66" id="Seg_3908" s="T65">šittɨ</ta>
            <ta e="T67" id="Seg_3909" s="T66">*korɨ-tɨt</ta>
            <ta e="T68" id="Seg_3910" s="T67">nɨːnɨ</ta>
            <ta e="T69" id="Seg_3911" s="T68">na</ta>
            <ta e="T70" id="Seg_3912" s="T69">šittɨ</ta>
            <ta e="T71" id="Seg_3913" s="T70">timnʼa-t</ta>
            <ta e="T72" id="Seg_3914" s="T71">ınnä</ta>
            <ta e="T73" id="Seg_3915" s="T72">qaqlɨ-qɨn-ntɨ</ta>
            <ta e="T74" id="Seg_3916" s="T73">tɛltɨ-tɨt</ta>
            <ta e="T75" id="Seg_3917" s="T74">na</ta>
            <ta e="T76" id="Seg_3918" s="T75">timnʼa-sɨ-t</ta>
            <ta e="T77" id="Seg_3919" s="T76">moqɨnä</ta>
            <ta e="T78" id="Seg_3920" s="T77">laqaltɨ-tɨt</ta>
            <ta e="T79" id="Seg_3921" s="T78">tına</ta>
            <ta e="T80" id="Seg_3922" s="T79">čʼontɨ-qɨn-lʼ</ta>
            <ta e="T81" id="Seg_3923" s="T80">sərɨ</ta>
            <ta e="T82" id="Seg_3924" s="T81">qorqɨ</ta>
            <ta e="T83" id="Seg_3925" s="T82">qət-mpɨlʼ</ta>
            <ta e="T84" id="Seg_3926" s="T83">timnʼa-tɨt</ta>
            <ta e="T85" id="Seg_3927" s="T84">čʼontɨ-qɨn</ta>
            <ta e="T86" id="Seg_3928" s="T85">moqɨnä</ta>
            <ta e="T87" id="Seg_3929" s="T86">iː-mpɨ</ta>
            <ta e="T88" id="Seg_3930" s="T87">nɨːnɨ</ta>
            <ta e="T89" id="Seg_3931" s="T88">na</ta>
            <ta e="T90" id="Seg_3932" s="T89">qum</ta>
            <ta e="T91" id="Seg_3933" s="T90">ürɨ-kkɨ-mpɨ</ta>
            <ta e="T92" id="Seg_3934" s="T91">ompä</ta>
            <ta e="T93" id="Seg_3935" s="T92">čʼu-pso</ta>
            <ta e="T94" id="Seg_3936" s="T93">ətɨmantɨ-qɨn</ta>
            <ta e="T95" id="Seg_3937" s="T94">ɛː-mpɨ</ta>
            <ta e="T97" id="Seg_3938" s="T95">pusqa-tɨ-ɔːl-mpɨ</ta>
            <ta e="T98" id="Seg_3939" s="T97">təp</ta>
            <ta e="T99" id="Seg_3940" s="T98">nɨːnɨ</ta>
            <ta e="T100" id="Seg_3941" s="T99">sä</ta>
            <ta e="T101" id="Seg_3942" s="T100">orɨ-kɨlɨm-mpɨ</ta>
            <ta e="T102" id="Seg_3943" s="T101">təp</ta>
            <ta e="T103" id="Seg_3944" s="T102">na</ta>
            <ta e="T104" id="Seg_3945" s="T103">morä-n</ta>
            <ta e="T105" id="Seg_3946" s="T104">qanɨŋ-qɨn</ta>
            <ta e="T106" id="Seg_3947" s="T105">nɨːnɨ</ta>
            <ta e="T107" id="Seg_3948" s="T106">na</ta>
            <ta e="T108" id="Seg_3949" s="T107">ippɨ-mpɨ-ntɨ</ta>
            <ta e="T109" id="Seg_3950" s="T108">təp</ta>
            <ta e="T110" id="Seg_3951" s="T109">nɨmtɨ</ta>
            <ta e="T111" id="Seg_3952" s="T110">ippɨ-mpɨ</ta>
            <ta e="T112" id="Seg_3953" s="T111">nɔːtɨ</ta>
            <ta e="T113" id="Seg_3954" s="T112">sä-n</ta>
            <ta e="T114" id="Seg_3955" s="T113">orɨ-kɨlɨm-mpɨ</ta>
            <ta e="T115" id="Seg_3956" s="T114">nɔːtɨ</ta>
            <ta e="T116" id="Seg_3957" s="T115">čʼu-mpɨ</ta>
            <ta e="T117" id="Seg_3958" s="T116">soqqɨ-tɨ</ta>
            <ta e="T118" id="Seg_3959" s="T117">peːmɨ-tɨ</ta>
            <ta e="T119" id="Seg_3960" s="T118">olä</ta>
            <ta e="T120" id="Seg_3961" s="T119">šittɨ</ta>
            <ta e="T121" id="Seg_3962" s="T120">nɨːnɨ</ta>
            <ta e="T122" id="Seg_3963" s="T121">nʼennä</ta>
            <ta e="T123" id="Seg_3964" s="T122">qən-altɨ</ta>
            <ta e="T124" id="Seg_3965" s="T123">üːnɨ-m-tɨ</ta>
            <ta e="T125" id="Seg_3966" s="T124">paŋɨ-sä</ta>
            <ta e="T126" id="Seg_3967" s="T125">mattɨ-ŋɨ-tɨ</ta>
            <ta e="T127" id="Seg_3968" s="T126">na</ta>
            <ta e="T128" id="Seg_3969" s="T127">nɔːkɨr</ta>
            <ta e="T129" id="Seg_3970" s="T128">qoptɨ-tɨ</ta>
            <ta e="T130" id="Seg_3971" s="T129">nɨːnɨ</ta>
            <ta e="T131" id="Seg_3972" s="T130">paktɨ</ta>
            <ta e="T132" id="Seg_3973" s="T131">təp</ta>
            <ta e="T133" id="Seg_3974" s="T132">ontɨ</ta>
            <ta e="T134" id="Seg_3975" s="T133">nɨmtɨ</ta>
            <ta e="T135" id="Seg_3976" s="T134">ippɨ-mpɨ</ta>
            <ta e="T136" id="Seg_3977" s="T135">ɔːmɨ-lʼ</ta>
            <ta e="T137" id="Seg_3978" s="T136">qontɨ-kkɨ</ta>
            <ta e="T138" id="Seg_3979" s="T137">nɨːnɨ</ta>
            <ta e="T139" id="Seg_3980" s="T138">tɛnɨ-mpɨ</ta>
            <ta e="T140" id="Seg_3981" s="T139">namɨššak</ta>
            <ta e="T141" id="Seg_3982" s="T140">apsɨ-tqo</ta>
            <ta e="T142" id="Seg_3983" s="T141">qu-mpɨ</ta>
            <ta e="T143" id="Seg_3984" s="T142">nom</ta>
            <ta e="T144" id="Seg_3985" s="T143">tɛnɨ-mpɨ</ta>
            <ta e="T145" id="Seg_3986" s="T144">qaqlɨ-n-tɨ</ta>
            <ta e="T146" id="Seg_3987" s="T145">šünʼčʼɨ-qɨn</ta>
            <ta e="T147" id="Seg_3988" s="T146">qontɨ-kkɨ</ta>
            <ta e="T148" id="Seg_3989" s="T147">namɨššak-lʼ</ta>
            <ta e="T149" id="Seg_3990" s="T148">ulqa-n-tɨ</ta>
            <ta e="T150" id="Seg_3991" s="T149">laka</ta>
            <ta e="T151" id="Seg_3992" s="T150">qaqlɨ-n-tɨ</ta>
            <ta e="T152" id="Seg_3993" s="T151">*ɨl-qɨn</ta>
            <ta e="T153" id="Seg_3994" s="T152">qalɨ-mpɨ-ntɨ</ta>
            <ta e="T154" id="Seg_3995" s="T153">morä</ta>
            <ta e="T155" id="Seg_3996" s="T154">muntɨk</ta>
            <ta e="T156" id="Seg_3997" s="T155">čʼu-mpɨ</ta>
            <ta e="T157" id="Seg_3998" s="T156">ukkɨr</ta>
            <ta e="T158" id="Seg_3999" s="T157">ukkɨr</ta>
            <ta e="T159" id="Seg_4000" s="T158">čʼontɨ-qɨn</ta>
            <ta e="T160" id="Seg_4001" s="T159">təp</ta>
            <ta e="T161" id="Seg_4002" s="T160">kos</ta>
            <ta e="T162" id="Seg_4003" s="T161">qaj</ta>
            <ta e="T163" id="Seg_4004" s="T162">nılʼčʼɨ-k</ta>
            <ta e="T164" id="Seg_4005" s="T163">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T165" id="Seg_4006" s="T164">qaj</ta>
            <ta e="T166" id="Seg_4007" s="T165">nom</ta>
            <ta e="T167" id="Seg_4008" s="T166">lʼi</ta>
            <ta e="T168" id="Seg_4009" s="T167">qaj</ta>
            <ta e="T169" id="Seg_4010" s="T168">qum</ta>
            <ta e="T170" id="Seg_4011" s="T169">tama-sä</ta>
            <ta e="T171" id="Seg_4012" s="T170">*kurɨ-lä</ta>
            <ta e="T172" id="Seg_4013" s="T171">aj</ta>
            <ta e="T173" id="Seg_4014" s="T172">nılʼčʼɨ-k</ta>
            <ta e="T174" id="Seg_4015" s="T173">kukɨ</ta>
            <ta e="T175" id="Seg_4016" s="T174">təp</ta>
            <ta e="T176" id="Seg_4017" s="T175">ınnä</ta>
            <ta e="T177" id="Seg_4018" s="T176">čʼam</ta>
            <ta e="T178" id="Seg_4019" s="T177">antaltɨ-mpɨ</ta>
            <ta e="T179" id="Seg_4020" s="T178">nılʼčʼɨ-k</ta>
            <ta e="T180" id="Seg_4021" s="T179">kukɨ</ta>
            <ta e="T181" id="Seg_4022" s="T180">kekkɨsä</ta>
            <ta e="T182" id="Seg_4023" s="T181">muːt</ta>
            <ta e="T183" id="Seg_4024" s="T182">tınola</ta>
            <ta e="T184" id="Seg_4025" s="T183">atɨ-alʼ-mpɨ-ntɨ-tɨt</ta>
            <ta e="T185" id="Seg_4026" s="T184">na</ta>
            <ta e="T186" id="Seg_4027" s="T185">qaqlɨ-n-tɨ</ta>
            <ta e="T187" id="Seg_4028" s="T186">*ɨl-qɨn-lʼ</ta>
            <ta e="T188" id="Seg_4029" s="T187">ulqa-lʼ</ta>
            <ta e="T189" id="Seg_4030" s="T188">laka-sä</ta>
            <ta e="T190" id="Seg_4031" s="T189">qompɨ-r-lä</ta>
            <ta e="T191" id="Seg_4032" s="T190">nɨːnɨ</ta>
            <ta e="T192" id="Seg_4033" s="T191">aj</ta>
            <ta e="T193" id="Seg_4034" s="T192">ıllä</ta>
            <ta e="T194" id="Seg_4035" s="T193">qontɨ</ta>
            <ta e="T195" id="Seg_4036" s="T194">aj</ta>
            <ta e="T196" id="Seg_4037" s="T195">təp</ta>
            <ta e="T197" id="Seg_4038" s="T196">kos</ta>
            <ta e="T198" id="Seg_4039" s="T197">qaj</ta>
            <ta e="T199" id="Seg_4040" s="T198">nılʼčʼɨ-k</ta>
            <ta e="T200" id="Seg_4041" s="T199">mašım</ta>
            <ta e="T201" id="Seg_4042" s="T200">soqɨš-ntɨ</ta>
            <ta e="T202" id="Seg_4043" s="T201">qum</ta>
            <ta e="T203" id="Seg_4044" s="T202">nılʼčʼɨ-k</ta>
            <ta e="T204" id="Seg_4045" s="T203">ıllä</ta>
            <ta e="T205" id="Seg_4046" s="T204">*kurɨ-lä</ta>
            <ta e="T206" id="Seg_4047" s="T205">kukɨ</ta>
            <ta e="T207" id="Seg_4048" s="T206">nɨːnɨ</ta>
            <ta e="T208" id="Seg_4049" s="T207">qanɨŋ-ntɨ</ta>
            <ta e="T209" id="Seg_4050" s="T208">konnä</ta>
            <ta e="T210" id="Seg_4051" s="T209">tantɨ</ta>
            <ta e="T211" id="Seg_4052" s="T210">tantɨ-mpɨ</ta>
            <ta e="T212" id="Seg_4053" s="T211">sajɨ-tɨ</ta>
            <ta e="T213" id="Seg_4054" s="T212">namɨššak</ta>
            <ta e="T214" id="Seg_4055" s="T213">apsɨ-tqo</ta>
            <ta e="T215" id="Seg_4056" s="T214">qu-mpɨ</ta>
            <ta e="T216" id="Seg_4057" s="T215">qɨl-ɨ-n</ta>
            <ta e="T217" id="Seg_4058" s="T216">šünʼčʼɨ-ntɨ</ta>
            <ta e="T218" id="Seg_4059" s="T217">tokkɨ-mɔːt-mpɨ</ta>
            <ta e="T219" id="Seg_4060" s="T218">ukkɨr</ta>
            <ta e="T220" id="Seg_4061" s="T219">čʼontɨ-qɨn</ta>
            <ta e="T221" id="Seg_4062" s="T220">nılʼčʼɨ-k</ta>
            <ta e="T222" id="Seg_4063" s="T221">ɛː-sɨ</ta>
            <ta e="T223" id="Seg_4064" s="T222">mɨta</ta>
            <ta e="T224" id="Seg_4065" s="T223">təttɨ</ta>
            <ta e="T225" id="Seg_4066" s="T224">atɨ-ntɨ</ta>
            <ta e="T226" id="Seg_4067" s="T225">mitɨ</ta>
            <ta e="T227" id="Seg_4068" s="T226">təttɨ</ta>
            <ta e="T228" id="Seg_4069" s="T227">čʼu-r-ɨ-ntɨ</ta>
            <ta e="T229" id="Seg_4070" s="T228">morä-n</ta>
            <ta e="T230" id="Seg_4071" s="T229">qanɨŋ-qɨn</ta>
            <ta e="T231" id="Seg_4072" s="T230">təp</ta>
            <ta e="T232" id="Seg_4073" s="T231">namɨššak</ta>
            <ta e="T233" id="Seg_4074" s="T232">apsɨ-tqo</ta>
            <ta e="T234" id="Seg_4075" s="T233">qu-mpɨ</ta>
            <ta e="T235" id="Seg_4076" s="T234">nɨːnɨ</ta>
            <ta e="T236" id="Seg_4077" s="T235">nılʼčʼɨ-k</ta>
            <ta e="T237" id="Seg_4078" s="T236">čʼam</ta>
            <ta e="T238" id="Seg_4079" s="T237">qən-ŋɨ</ta>
            <ta e="T239" id="Seg_4080" s="T238">qaqlɨ-m</ta>
            <ta e="T240" id="Seg_4081" s="T239">təttɨ-n</ta>
            <ta e="T241" id="Seg_4082" s="T240">mɨnɨ-tɨ</ta>
            <ta e="T242" id="Seg_4083" s="T241">tulɨ-š-mpɨ</ta>
            <ta e="T243" id="Seg_4084" s="T242">nɨːnɨ</ta>
            <ta e="T244" id="Seg_4085" s="T243">qaqlɨ-n-tɨ</ta>
            <ta e="T245" id="Seg_4086" s="T244">moqal-ɨ-n</ta>
            <ta e="T246" id="Seg_4087" s="T245">*iː-mɨn</ta>
            <ta e="T247" id="Seg_4088" s="T246">soqqɨ-lä</ta>
            <ta e="T248" id="Seg_4089" s="T247">konnä</ta>
            <ta e="T249" id="Seg_4090" s="T248">tantɨ</ta>
            <ta e="T250" id="Seg_4091" s="T249">nɨːnɨ</ta>
            <ta e="T251" id="Seg_4092" s="T250">konnä</ta>
            <ta e="T252" id="Seg_4093" s="T251">qanɨŋ-ntɨ</ta>
            <ta e="T253" id="Seg_4094" s="T252">alʼčʼɨ</ta>
            <ta e="T254" id="Seg_4095" s="T253">nɨːnɨ</ta>
            <ta e="T255" id="Seg_4096" s="T254">nɨmtɨ</ta>
            <ta e="T256" id="Seg_4097" s="T255">ippɨ</ta>
            <ta e="T257" id="Seg_4098" s="T256">aj</ta>
            <ta e="T258" id="Seg_4099" s="T257">kos</ta>
            <ta e="T259" id="Seg_4100" s="T258">qaj</ta>
            <ta e="T260" id="Seg_4101" s="T259">nılʼčʼɨ-k</ta>
            <ta e="T261" id="Seg_4102" s="T260">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T262" id="Seg_4103" s="T261">qum</ta>
            <ta e="T263" id="Seg_4104" s="T262">tama-sä</ta>
            <ta e="T264" id="Seg_4105" s="T263">*kurɨ-lä</ta>
            <ta e="T265" id="Seg_4106" s="T264">*qup-ŋɨ</ta>
            <ta e="T266" id="Seg_4107" s="T265">konnä-n</ta>
            <ta e="T267" id="Seg_4108" s="T266">nʼarqɨ-lʼ</ta>
            <ta e="T268" id="Seg_4109" s="T267">markɨ-n</ta>
            <ta e="T269" id="Seg_4110" s="T268">toːp-ɨ-ntɨ</ta>
            <ta e="T270" id="Seg_4111" s="T269">soqqɨ-lä</ta>
            <ta e="T271" id="Seg_4112" s="T270">konnä</ta>
            <ta e="T272" id="Seg_4113" s="T271">tantɨ</ta>
            <ta e="T273" id="Seg_4114" s="T272">nɨːnɨ</ta>
            <ta e="T274" id="Seg_4115" s="T273">konnä</ta>
            <ta e="T275" id="Seg_4116" s="T274">soqqɨ-lä</ta>
            <ta e="T276" id="Seg_4117" s="T275">tantɨ</ta>
            <ta e="T277" id="Seg_4118" s="T276">man</ta>
            <ta e="T278" id="Seg_4119" s="T277">qottä</ta>
            <ta e="T279" id="Seg_4120" s="T278">ınnä</ta>
            <ta e="T280" id="Seg_4121" s="T279">*nɨl-ɛː-sɨ-k</ta>
            <ta e="T281" id="Seg_4122" s="T280">ɛnä</ta>
            <ta e="T282" id="Seg_4123" s="T281">konnä</ta>
            <ta e="T283" id="Seg_4124" s="T282">soqqɨ-lä</ta>
            <ta e="T284" id="Seg_4125" s="T283">tantɨ</ta>
            <ta e="T285" id="Seg_4126" s="T284">orɨ-tɨ</ta>
            <ta e="T286" id="Seg_4127" s="T285">meːltɨ</ta>
            <ta e="T287" id="Seg_4128" s="T286">čʼäːŋkɨ</ta>
            <ta e="T288" id="Seg_4129" s="T287">aj</ta>
            <ta e="T289" id="Seg_4130" s="T288">kos</ta>
            <ta e="T290" id="Seg_4131" s="T289">qaj</ta>
            <ta e="T291" id="Seg_4132" s="T290">nılʼčʼɨ-k</ta>
            <ta e="T292" id="Seg_4133" s="T291">soqɨš-lɨ-tɨ</ta>
            <ta e="T293" id="Seg_4134" s="T292">qum</ta>
            <ta e="T294" id="Seg_4135" s="T293">ašša</ta>
            <ta e="T295" id="Seg_4136" s="T294">nılʼčʼɨ-k</ta>
            <ta e="T296" id="Seg_4137" s="T295">kukɨ</ta>
            <ta e="T297" id="Seg_4138" s="T296">*kurɨ-lä</ta>
            <ta e="T298" id="Seg_4139" s="T297">na</ta>
            <ta e="T299" id="Seg_4140" s="T298">morä-n</ta>
            <ta e="T300" id="Seg_4141" s="T299">qan-qɨn</ta>
            <ta e="T301" id="Seg_4142" s="T300">pirqɨ</ta>
            <ta e="T302" id="Seg_4143" s="T301">təttɨ</ta>
            <ta e="T303" id="Seg_4144" s="T302">ınnä</ta>
            <ta e="T304" id="Seg_4145" s="T303">soqqɨ-alʼ-ŋɨ</ta>
            <ta e="T305" id="Seg_4146" s="T304">nɨːnɨ</ta>
            <ta e="T306" id="Seg_4147" s="T305">konnä</ta>
            <ta e="T307" id="Seg_4148" s="T306">soqqɨ-lä</ta>
            <ta e="T308" id="Seg_4149" s="T307">nılʼčʼɨ-k</ta>
            <ta e="T309" id="Seg_4150" s="T308">mantɨ-mpɨ</ta>
            <ta e="T310" id="Seg_4151" s="T309">konnä-n</ta>
            <ta e="T311" id="Seg_4152" s="T310">nılʼčʼɨ-k</ta>
            <ta e="T312" id="Seg_4153" s="T311">qo-ŋɨ-tɨ</ta>
            <ta e="T313" id="Seg_4154" s="T312">kotɨlʼ</ta>
            <ta e="T314" id="Seg_4155" s="T313">mɔːt</ta>
            <ta e="T315" id="Seg_4156" s="T314">ɔːmtɨ-ntɨ</ta>
            <ta e="T316" id="Seg_4157" s="T315">nɨːnɨ</ta>
            <ta e="T317" id="Seg_4158" s="T316">aj</ta>
            <ta e="T318" id="Seg_4159" s="T317">ıllä</ta>
            <ta e="T319" id="Seg_4160" s="T318">alʼčʼɨ-ŋɨ</ta>
            <ta e="T320" id="Seg_4161" s="T319">kuttar</ta>
            <ta e="T321" id="Seg_4162" s="T320">näčʼčʼä</ta>
            <ta e="T322" id="Seg_4163" s="T321">tulɨ-š-ntɨ-ɛntɨ-k</ta>
            <ta e="T323" id="Seg_4164" s="T322">nɨːnɨ</ta>
            <ta e="T324" id="Seg_4165" s="T323">na</ta>
            <ta e="T325" id="Seg_4166" s="T324">mɔːt-ntɨ</ta>
            <ta e="T326" id="Seg_4167" s="T325">konnä</ta>
            <ta e="T327" id="Seg_4168" s="T326">*nɨl-lä</ta>
            <ta e="T328" id="Seg_4169" s="T327">ıllä</ta>
            <ta e="T329" id="Seg_4170" s="T328">tü-ŋɨ</ta>
            <ta e="T330" id="Seg_4171" s="T329">qälɨk</ta>
            <ta e="T331" id="Seg_4172" s="T330">mɔːt</ta>
            <ta e="T332" id="Seg_4173" s="T331">ɔːmtɨ-ntɨ</ta>
            <ta e="T333" id="Seg_4174" s="T332">ɔːtä-tɨ</ta>
            <ta e="T334" id="Seg_4175" s="T333">ətɨmantɨ-qɨn</ta>
            <ta e="T335" id="Seg_4176" s="T334">*kurɨ-alʼ-mpɨ-ntɨ-tɨt</ta>
            <ta e="T336" id="Seg_4177" s="T335">nɨːnɨ</ta>
            <ta e="T337" id="Seg_4178" s="T336">mɔːt-ntɨ</ta>
            <ta e="T338" id="Seg_4179" s="T337">šeːr-ŋɨ</ta>
            <ta e="T339" id="Seg_4180" s="T338">ira</ta>
            <ta e="T340" id="Seg_4181" s="T339">ukkɨr</ta>
            <ta e="T341" id="Seg_4182" s="T340">nälʼa-tɨ</ta>
            <ta e="T342" id="Seg_4183" s="T341">ukkɨr</ta>
            <ta e="T343" id="Seg_4184" s="T342">čʼontɨ-qɨn</ta>
            <ta e="T521" id="Seg_4185" s="T343">qäl</ta>
            <ta e="T344" id="Seg_4186" s="T521">ira</ta>
            <ta e="T345" id="Seg_4187" s="T344">nılʼčʼɨ-k</ta>
            <ta e="T346" id="Seg_4188" s="T345">ɛsɨ</ta>
            <ta e="T347" id="Seg_4189" s="T346">tan</ta>
            <ta e="T348" id="Seg_4190" s="T347">na</ta>
            <ta e="T349" id="Seg_4191" s="T348">qum</ta>
            <ta e="T350" id="Seg_4192" s="T349">apsɨ-tɨ-ätɨ</ta>
            <ta e="T351" id="Seg_4193" s="T350">təp</ta>
            <ta e="T352" id="Seg_4194" s="T351">na</ta>
            <ta e="T353" id="Seg_4195" s="T352">ira-n</ta>
            <ta e="T354" id="Seg_4196" s="T353">mɨ-qɨn</ta>
            <ta e="T355" id="Seg_4197" s="T354">šittɨ</ta>
            <ta e="T356" id="Seg_4198" s="T355">toːn</ta>
            <ta e="T357" id="Seg_4199" s="T356">ɔːtä-t</ta>
            <ta e="T358" id="Seg_4200" s="T357">man</ta>
            <ta e="T359" id="Seg_4201" s="T358">ukkɨr</ta>
            <ta e="T360" id="Seg_4202" s="T359">toːn</ta>
            <ta e="T361" id="Seg_4203" s="T360">ɔːtä-mɨ</ta>
            <ta e="T362" id="Seg_4204" s="T361">suːrɨm</ta>
            <ta e="T363" id="Seg_4205" s="T362">muntɨk</ta>
            <ta e="T364" id="Seg_4206" s="T363">am-ŋɨ-tɨ</ta>
            <ta e="T365" id="Seg_4207" s="T364">ira</ta>
            <ta e="T366" id="Seg_4208" s="T365">nılʼčʼɨ-k</ta>
            <ta e="T367" id="Seg_4209" s="T366">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T368" id="Seg_4210" s="T367">tan</ta>
            <ta e="T369" id="Seg_4211" s="T368">ponä</ta>
            <ta e="T370" id="Seg_4212" s="T369">tantɨ-lä</ta>
            <ta e="T371" id="Seg_4213" s="T370">man</ta>
            <ta e="T372" id="Seg_4214" s="T371">ɔːtä-mɨ</ta>
            <ta e="T373" id="Seg_4215" s="T372">ɔːttɨ-ätɨ</ta>
            <ta e="T374" id="Seg_4216" s="T373">*lɨpɨ-k-ɨ-mɔːt-ŋɨ</ta>
            <ta e="T375" id="Seg_4217" s="T374">təp</ta>
            <ta e="T376" id="Seg_4218" s="T375">ponä</ta>
            <ta e="T377" id="Seg_4219" s="T376">tantɨ</ta>
            <ta e="T378" id="Seg_4220" s="T377">təp</ta>
            <ta e="T379" id="Seg_4221" s="T378">ponä</ta>
            <ta e="T380" id="Seg_4222" s="T379">tantɨ</ta>
            <ta e="T381" id="Seg_4223" s="T380">namɨššak</ta>
            <ta e="T382" id="Seg_4224" s="T381">sɨqɨlɨ-mpɨ</ta>
            <ta e="T383" id="Seg_4225" s="T382">qum</ta>
            <ta e="T384" id="Seg_4226" s="T383">qaj-tɨ</ta>
            <ta e="T385" id="Seg_4227" s="T384">mantɨ-mpɨ-ɛntɨ-tɨ</ta>
            <ta e="T386" id="Seg_4228" s="T385">nɨːnɨ</ta>
            <ta e="T387" id="Seg_4229" s="T386">təp</ta>
            <ta e="T388" id="Seg_4230" s="T387">aj</ta>
            <ta e="T389" id="Seg_4231" s="T388">ıllä</ta>
            <ta e="T390" id="Seg_4232" s="T389">alʼčʼɨ-mpɨ</ta>
            <ta e="T391" id="Seg_4233" s="T390">nɨmtɨ</ta>
            <ta e="T392" id="Seg_4234" s="T391">na</ta>
            <ta e="T393" id="Seg_4235" s="T392">ippɨ-mpɨ-ntɨ</ta>
            <ta e="T394" id="Seg_4236" s="T393">na</ta>
            <ta e="T395" id="Seg_4237" s="T394">təp-ɨ-n</ta>
            <ta e="T396" id="Seg_4238" s="T395">tü-lä</ta>
            <ta e="T397" id="Seg_4239" s="T396">puːlä</ta>
            <ta e="T398" id="Seg_4240" s="T397">suːrɨm</ta>
            <ta e="T399" id="Seg_4241" s="T398">na</ta>
            <ta e="T400" id="Seg_4242" s="T399">qən-mpɨ-ntɨ</ta>
            <ta e="T401" id="Seg_4243" s="T400">nɨː</ta>
            <ta e="T402" id="Seg_4244" s="T401">ukoːn</ta>
            <ta e="T403" id="Seg_4245" s="T402">əsɨ-sɨ-qı</ta>
            <ta e="T404" id="Seg_4246" s="T403">ɔːtä-ntɨ-lʼ</ta>
            <ta e="T405" id="Seg_4247" s="T404">ɔːttɨ-lä</ta>
            <ta e="T406" id="Seg_4248" s="T405">poː-qɨn</ta>
            <ta e="T407" id="Seg_4249" s="T406">ilɨ-mpɨ-qı</ta>
            <ta e="T408" id="Seg_4250" s="T407">nɨːnɨ</ta>
            <ta e="T409" id="Seg_4251" s="T408">na</ta>
            <ta e="T410" id="Seg_4252" s="T409">qum-ɨ-m</ta>
            <ta e="T411" id="Seg_4253" s="T410">apsɨ-tɨ-mpɨ-lä</ta>
            <ta e="T412" id="Seg_4254" s="T411">nɨmtɨ</ta>
            <ta e="T413" id="Seg_4255" s="T412">na</ta>
            <ta e="T414" id="Seg_4256" s="T413">ilɨ-lɨ-mpɨ-ntɨ</ta>
            <ta e="T415" id="Seg_4257" s="T414">nɨːnɨ</ta>
            <ta e="T416" id="Seg_4258" s="T415">na</ta>
            <ta e="T417" id="Seg_4259" s="T416">ira</ta>
            <ta e="T418" id="Seg_4260" s="T417">aj</ta>
            <ta e="T419" id="Seg_4261" s="T418">nılʼčʼɨ-k</ta>
            <ta e="T420" id="Seg_4262" s="T419">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T421" id="Seg_4263" s="T420">aj</ta>
            <ta e="T422" id="Seg_4264" s="T421">qäl-äšɨk</ta>
            <ta e="T423" id="Seg_4265" s="T422">ɔːtä-m-tɨ</ta>
            <ta e="T424" id="Seg_4266" s="T423">ɔːttɨ-lä</ta>
            <ta e="T425" id="Seg_4267" s="T424">ɔːtä-tɨ</ta>
            <ta e="T426" id="Seg_4268" s="T425">qapı</ta>
            <ta e="T427" id="Seg_4269" s="T426">mačʼɨ-ntɨ</ta>
            <ta e="T428" id="Seg_4270" s="T427">ašša</ta>
            <ta e="T429" id="Seg_4271" s="T428">qən-ɛntɨ</ta>
            <ta e="T430" id="Seg_4272" s="T429">mɔːt-ɨ-n</ta>
            <ta e="T431" id="Seg_4273" s="T430">qan-qɨn</ta>
            <ta e="T432" id="Seg_4274" s="T431">qontɨ-tɨt</ta>
            <ta e="T433" id="Seg_4275" s="T432">nɨːnɨ</ta>
            <ta e="T434" id="Seg_4276" s="T433">təp</ta>
            <ta e="T435" id="Seg_4277" s="T434">ponä</ta>
            <ta e="T436" id="Seg_4278" s="T435">tantɨ</ta>
            <ta e="T437" id="Seg_4279" s="T436">seːpɨlaŋ-lʼ</ta>
            <ta e="T438" id="Seg_4280" s="T437">mɨ-ntɨ</ta>
            <ta e="T439" id="Seg_4281" s="T438">qən-lä</ta>
            <ta e="T440" id="Seg_4282" s="T439">aj</ta>
            <ta e="T441" id="Seg_4283" s="T440">ıllä</ta>
            <ta e="T442" id="Seg_4284" s="T441">alʼčʼɨ</ta>
            <ta e="T443" id="Seg_4285" s="T442">nɨːnɨ</ta>
            <ta e="T444" id="Seg_4286" s="T443">təp</ta>
            <ta e="T445" id="Seg_4287" s="T444">ukkɨr</ta>
            <ta e="T446" id="Seg_4288" s="T445">čʼontɨ-qɨn</ta>
            <ta e="T447" id="Seg_4289" s="T446">nılʼčʼɨ-k</ta>
            <ta e="T448" id="Seg_4290" s="T447">šittɨ-ŋɨ</ta>
            <ta e="T449" id="Seg_4291" s="T448">qaj</ta>
            <ta e="T450" id="Seg_4292" s="T449">qən-tɨ-tɨ</ta>
            <ta e="T451" id="Seg_4293" s="T450">montɨ</ta>
            <ta e="T452" id="Seg_4294" s="T451">ilɨ-ɛčʼɨ</ta>
            <ta e="T453" id="Seg_4295" s="T452">nɨːnɨ</ta>
            <ta e="T454" id="Seg_4296" s="T453">moqɨnä</ta>
            <ta e="T455" id="Seg_4297" s="T454">tü-ŋɨ</ta>
            <ta e="T456" id="Seg_4298" s="T455">mɔːt-ntɨ</ta>
            <ta e="T522" id="Seg_4299" s="T456">qäl</ta>
            <ta e="T457" id="Seg_4300" s="T522">ira</ta>
            <ta e="T458" id="Seg_4301" s="T457">qontɨ</ta>
            <ta e="T459" id="Seg_4302" s="T458">nɨːnɨ</ta>
            <ta e="T460" id="Seg_4303" s="T459">təp-ɨ-tɨt</ta>
            <ta e="T461" id="Seg_4304" s="T460">ɔːtä</ta>
            <ta e="T462" id="Seg_4305" s="T461">meːltɨ</ta>
            <ta e="T463" id="Seg_4306" s="T462">ətɨmantɨ-qɨn</ta>
            <ta e="T464" id="Seg_4307" s="T463">qontɨ-tɨt</ta>
            <ta e="T465" id="Seg_4308" s="T464">təp</ta>
            <ta e="T466" id="Seg_4309" s="T465">na</ta>
            <ta e="T467" id="Seg_4310" s="T466">tü-mpɨlʼ</ta>
            <ta e="T468" id="Seg_4311" s="T467">porqɨ-lʼ</ta>
            <ta e="T469" id="Seg_4312" s="T468">peːmɨ-t-ɨ-sä</ta>
            <ta e="T470" id="Seg_4313" s="T469">ilɨ</ta>
            <ta e="T471" id="Seg_4314" s="T470">nɨːnɨ</ta>
            <ta e="T472" id="Seg_4315" s="T471">nɔːkɨr-ɨ-mtälɨlʼ</ta>
            <ta e="T473" id="Seg_4316" s="T472">pi-n</ta>
            <ta e="T474" id="Seg_4317" s="T473">nılʼčʼɨ-k</ta>
            <ta e="T475" id="Seg_4318" s="T474">ɛsɨ</ta>
            <ta e="T476" id="Seg_4319" s="T475">mɨta</ta>
            <ta e="T477" id="Seg_4320" s="T476">mol</ta>
            <ta e="T478" id="Seg_4321" s="T477">soma-k</ta>
            <ta e="T479" id="Seg_4322" s="T478">ɛsɨ-k</ta>
            <ta e="T523" id="Seg_4323" s="T479">qäl</ta>
            <ta e="T480" id="Seg_4324" s="T523">ira</ta>
            <ta e="T481" id="Seg_4325" s="T480">ɔːtä-tɨ</ta>
            <ta e="T482" id="Seg_4326" s="T481">šöt-ntɨ</ta>
            <ta e="T483" id="Seg_4327" s="T482">ašša</ta>
            <ta e="T484" id="Seg_4328" s="T483">qən-ɛntɨ</ta>
            <ta e="T485" id="Seg_4329" s="T484">mɔːt-ɨ-n</ta>
            <ta e="T486" id="Seg_4330" s="T485">qan-qɨn</ta>
            <ta e="T487" id="Seg_4331" s="T486">nɨmtɨ</ta>
            <ta e="T488" id="Seg_4332" s="T487">qən-ntɨ-tɨt</ta>
            <ta e="T489" id="Seg_4333" s="T488">təp</ta>
            <ta e="T490" id="Seg_4334" s="T489">tü-lä</ta>
            <ta e="T491" id="Seg_4335" s="T490">aj</ta>
            <ta e="T492" id="Seg_4336" s="T491">ıllä</ta>
            <ta e="T493" id="Seg_4337" s="T492">qontɨ-ŋɨ</ta>
            <ta e="T494" id="Seg_4338" s="T493">tɔːptɨlʼ</ta>
            <ta e="T495" id="Seg_4339" s="T494">qarɨ-n</ta>
            <ta e="T496" id="Seg_4340" s="T495">montɨ</ta>
            <ta e="T497" id="Seg_4341" s="T496">sɨrɨ</ta>
            <ta e="T498" id="Seg_4342" s="T497">pin-ɛː-ntɨ-tɨ</ta>
            <ta e="T499" id="Seg_4343" s="T498">man</ta>
            <ta e="T500" id="Seg_4344" s="T499">təp</ta>
            <ta e="T501" id="Seg_4345" s="T500">kuttar</ta>
            <ta e="T502" id="Seg_4346" s="T501">tü-sɨ</ta>
            <ta e="T503" id="Seg_4347" s="T502">ɔːtä-mɨ</ta>
            <ta e="T504" id="Seg_4348" s="T503">šöt-ntɨ</ta>
            <ta e="T505" id="Seg_4349" s="T504">ašša</ta>
            <ta e="T506" id="Seg_4350" s="T505">qən-ɛntɨ</ta>
            <ta e="T507" id="Seg_4351" s="T506">*korɨ-altɨ-tɨ</ta>
            <ta e="T508" id="Seg_4352" s="T507">sɨrɨ-m</ta>
            <ta e="T509" id="Seg_4353" s="T508">pin-lä</ta>
            <ta e="T510" id="Seg_4354" s="T509">qən-ɨ-ŋɨ</ta>
            <ta e="T511" id="Seg_4355" s="T510">soma-k</ta>
            <ta e="T512" id="Seg_4356" s="T511">ıllä</ta>
            <ta e="T513" id="Seg_4357" s="T512">na</ta>
            <ta e="T514" id="Seg_4358" s="T513">qən-ntɨ</ta>
            <ta e="T515" id="Seg_4359" s="T514">təp</ta>
            <ta e="T516" id="Seg_4360" s="T515">nɔːtɨ</ta>
            <ta e="T517" id="Seg_4361" s="T516">soma-k</ta>
            <ta e="T518" id="Seg_4362" s="T517">ɛsɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_4363" s="T0">this</ta>
            <ta e="T96" id="Seg_4364" s="T1">Nenets.[NOM]</ta>
            <ta e="T2" id="Seg_4365" s="T96">old.man.[NOM]</ta>
            <ta e="T3" id="Seg_4366" s="T2">guy.[NOM]</ta>
            <ta e="T4" id="Seg_4367" s="T3">such-ADVZ</ta>
            <ta e="T5" id="Seg_4368" s="T4">say-CO-3SG.O</ta>
            <ta e="T6" id="Seg_4369" s="T5">I.NOM</ta>
            <ta e="T7" id="Seg_4370" s="T6">this</ta>
            <ta e="T8" id="Seg_4371" s="T7">sea-LOC</ta>
            <ta e="T9" id="Seg_4372" s="T8">white</ta>
            <ta e="T10" id="Seg_4373" s="T9">bear-PL.[NOM]</ta>
            <ta e="T11" id="Seg_4374" s="T10">kill-FUT-3SG.O</ta>
            <ta e="T519" id="Seg_4375" s="T11">Nenets.[NOM]</ta>
            <ta e="T12" id="Seg_4376" s="T519">old.man.[NOM]</ta>
            <ta e="T13" id="Seg_4377" s="T12">such-ADVZ</ta>
            <ta e="T14" id="Seg_4378" s="T13">say-CO-3SG.O</ta>
            <ta e="T15" id="Seg_4379" s="T14">here</ta>
            <ta e="T16" id="Seg_4380" s="T15">NEG</ta>
            <ta e="T17" id="Seg_4381" s="T16">human.being-EP-PL.[NOM]</ta>
            <ta e="T18" id="Seg_4382" s="T17">kill-PST.[3SG.S]</ta>
            <ta e="T19" id="Seg_4383" s="T18">wild.animal.[NOM]</ta>
            <ta e="T20" id="Seg_4384" s="T19">you.SG.NOM</ta>
            <ta e="T21" id="Seg_4385" s="T20">how</ta>
            <ta e="T22" id="Seg_4386" s="T21">go.away-CO-2SG.S</ta>
            <ta e="T23" id="Seg_4387" s="T22">sea-GEN</ta>
            <ta e="T24" id="Seg_4388" s="T23">behind</ta>
            <ta e="T25" id="Seg_4389" s="T24">supposedly</ta>
            <ta e="T26" id="Seg_4390" s="T25">tree-CAR-ADJZ</ta>
            <ta e="T27" id="Seg_4391" s="T26">earth-LOC</ta>
            <ta e="T28" id="Seg_4392" s="T27">live-3SG.O</ta>
            <ta e="T520" id="Seg_4393" s="T28">Nenets.[NOM]</ta>
            <ta e="T29" id="Seg_4394" s="T520">old.man.[NOM]</ta>
            <ta e="T30" id="Seg_4395" s="T29">(s)he-EP-GEN</ta>
            <ta e="T31" id="Seg_4396" s="T30">three</ta>
            <ta e="T32" id="Seg_4397" s="T31">brother-DYA-PL.[NOM]</ta>
            <ta e="T33" id="Seg_4398" s="T32">sea-GEN</ta>
            <ta e="T34" id="Seg_4399" s="T33">behind</ta>
            <ta e="T35" id="Seg_4400" s="T34">this</ta>
            <ta e="T36" id="Seg_4401" s="T35">white</ta>
            <ta e="T37" id="Seg_4402" s="T36">bear-ACC</ta>
            <ta e="T38" id="Seg_4403" s="T37">kill-INF</ta>
            <ta e="T39" id="Seg_4404" s="T38">go.away-CO-3PL</ta>
            <ta e="T40" id="Seg_4405" s="T39">(s)he-EP-PL.[NOM]</ta>
            <ta e="T41" id="Seg_4406" s="T40">leave-CO-3PL</ta>
            <ta e="T42" id="Seg_4407" s="T41">this</ta>
            <ta e="T43" id="Seg_4408" s="T42">white</ta>
            <ta e="T44" id="Seg_4409" s="T43">bear-ACC</ta>
            <ta e="T45" id="Seg_4410" s="T44">mother-ILL</ta>
            <ta e="T46" id="Seg_4411" s="T45">shoot-CO-3SG.O</ta>
            <ta e="T47" id="Seg_4412" s="T46">then</ta>
            <ta e="T48" id="Seg_4413" s="T47">white</ta>
            <ta e="T49" id="Seg_4414" s="T48">bear-ACC</ta>
            <ta e="T51" id="Seg_4415" s="T50">this</ta>
            <ta e="T52" id="Seg_4416" s="T51">white</ta>
            <ta e="T53" id="Seg_4417" s="T52">bear.[NOM]</ta>
            <ta e="T54" id="Seg_4418" s="T53">it.is.said</ta>
            <ta e="T55" id="Seg_4419" s="T54">big</ta>
            <ta e="T56" id="Seg_4420" s="T55">be-CO.[3SG.S]</ta>
            <ta e="T57" id="Seg_4421" s="T56">then</ta>
            <ta e="T58" id="Seg_4422" s="T57">(s)he-EP-PL.[NOM]</ta>
            <ta e="T59" id="Seg_4423" s="T58">kill-CVB</ta>
            <ta e="T60" id="Seg_4424" s="T59">paunch-GEN-3SG</ta>
            <ta e="T61" id="Seg_4425" s="T60">blood-EP-PL.[NOM]</ta>
            <ta e="T62" id="Seg_4426" s="T61">into.parts</ta>
            <ta e="T63" id="Seg_4427" s="T62">rip.up-3PL</ta>
            <ta e="T64" id="Seg_4428" s="T63">and</ta>
            <ta e="T65" id="Seg_4429" s="T64">back-PROL-OBL.3SG</ta>
            <ta e="T66" id="Seg_4430" s="T65">into.parts</ta>
            <ta e="T67" id="Seg_4431" s="T66">rip.up-3PL</ta>
            <ta e="T68" id="Seg_4432" s="T67">then</ta>
            <ta e="T69" id="Seg_4433" s="T68">this</ta>
            <ta e="T70" id="Seg_4434" s="T69">two</ta>
            <ta e="T71" id="Seg_4435" s="T70">brother-PL.[NOM]</ta>
            <ta e="T72" id="Seg_4436" s="T71">up</ta>
            <ta e="T73" id="Seg_4437" s="T72">sledge-ILL-OBL.3SG</ta>
            <ta e="T74" id="Seg_4438" s="T73">load-3PL</ta>
            <ta e="T75" id="Seg_4439" s="T74">this</ta>
            <ta e="T76" id="Seg_4440" s="T75">brother-DYA-PL.[NOM]</ta>
            <ta e="T77" id="Seg_4441" s="T76">home</ta>
            <ta e="T78" id="Seg_4442" s="T77">start.for-3PL</ta>
            <ta e="T79" id="Seg_4443" s="T78">that</ta>
            <ta e="T80" id="Seg_4444" s="T79">middle-LOC-ADJZ</ta>
            <ta e="T81" id="Seg_4445" s="T80">white</ta>
            <ta e="T82" id="Seg_4446" s="T81">bear.[NOM]</ta>
            <ta e="T83" id="Seg_4447" s="T82">kill-PTCP.PST</ta>
            <ta e="T84" id="Seg_4448" s="T83">brother.[NOM]-3PL</ta>
            <ta e="T85" id="Seg_4449" s="T84">middle-LOC</ta>
            <ta e="T86" id="Seg_4450" s="T85">home</ta>
            <ta e="T87" id="Seg_4451" s="T86">take-PST.NAR.[3SG.S]</ta>
            <ta e="T88" id="Seg_4452" s="T87">then</ta>
            <ta e="T89" id="Seg_4453" s="T88">this</ta>
            <ta e="T90" id="Seg_4454" s="T89">human.being.[NOM]</ta>
            <ta e="T91" id="Seg_4455" s="T90">get.lost-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T92" id="Seg_4456" s="T91">soon</ta>
            <ta e="T93" id="Seg_4457" s="T92">melt-PTCP.NEC</ta>
            <ta e="T94" id="Seg_4458" s="T93">outside-LOC</ta>
            <ta e="T95" id="Seg_4459" s="T94">be-PST.NAR.[3SG.S]</ta>
            <ta e="T97" id="Seg_4460" s="T95">storm-TR-MOM-PST.NAR.[3SG.S]</ta>
            <ta e="T98" id="Seg_4461" s="T97">(s)he.[NOM]</ta>
            <ta e="T99" id="Seg_4462" s="T98">then</ta>
            <ta e="T100" id="Seg_4463" s="T99">quite</ta>
            <ta e="T101" id="Seg_4464" s="T100">force-CAR-PST.NAR.[3SG.S]</ta>
            <ta e="T102" id="Seg_4465" s="T101">(s)he.[NOM]</ta>
            <ta e="T103" id="Seg_4466" s="T102">here</ta>
            <ta e="T104" id="Seg_4467" s="T103">sea-GEN</ta>
            <ta e="T105" id="Seg_4468" s="T104">bank-LOC</ta>
            <ta e="T106" id="Seg_4469" s="T105">then</ta>
            <ta e="T107" id="Seg_4470" s="T106">INFER</ta>
            <ta e="T108" id="Seg_4471" s="T107">lie-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T109" id="Seg_4472" s="T108">(s)he.[NOM]</ta>
            <ta e="T110" id="Seg_4473" s="T109">there</ta>
            <ta e="T111" id="Seg_4474" s="T110">lie-PST.NAR.[3SG.S]</ta>
            <ta e="T112" id="Seg_4475" s="T111">then</ta>
            <ta e="T113" id="Seg_4476" s="T112">quite-ADV.LOC</ta>
            <ta e="T114" id="Seg_4477" s="T113">force-CAR-PST.NAR.[3SG.S]</ta>
            <ta e="T115" id="Seg_4478" s="T114">then</ta>
            <ta e="T116" id="Seg_4479" s="T115">melt-PST.NAR.[3SG.S]</ta>
            <ta e="T117" id="Seg_4480" s="T116">fur.clothing.[NOM]-3SG</ta>
            <ta e="T118" id="Seg_4481" s="T117">footwear.[NOM]-3SG</ta>
            <ta e="T119" id="Seg_4482" s="T118">only</ta>
            <ta e="T120" id="Seg_4483" s="T119">wake.up.[3SG.S]</ta>
            <ta e="T121" id="Seg_4484" s="T120">then</ta>
            <ta e="T122" id="Seg_4485" s="T121">forward</ta>
            <ta e="T123" id="Seg_4486" s="T122">leave-CAUS.[3SG.S]</ta>
            <ta e="T124" id="Seg_4487" s="T123">leash-ACC-3SG</ta>
            <ta e="T125" id="Seg_4488" s="T124">knife-INSTR</ta>
            <ta e="T126" id="Seg_4489" s="T125">cut-CO-3SG.O</ta>
            <ta e="T127" id="Seg_4490" s="T126">here</ta>
            <ta e="T128" id="Seg_4491" s="T127">three</ta>
            <ta e="T129" id="Seg_4492" s="T128">bull.[NOM]-3SG</ta>
            <ta e="T130" id="Seg_4493" s="T129">then</ta>
            <ta e="T131" id="Seg_4494" s="T130">run.[3SG.S]</ta>
            <ta e="T132" id="Seg_4495" s="T131">(s)he.[NOM]</ta>
            <ta e="T133" id="Seg_4496" s="T132">oneself.3SG.[NOM]</ta>
            <ta e="T134" id="Seg_4497" s="T133">there</ta>
            <ta e="T135" id="Seg_4498" s="T134">lie-PST.NAR.[3SG.S]</ta>
            <ta e="T136" id="Seg_4499" s="T135">other-ADJZ</ta>
            <ta e="T137" id="Seg_4500" s="T136">sleep-HAB.[3SG.S]</ta>
            <ta e="T138" id="Seg_4501" s="T137">then</ta>
            <ta e="T139" id="Seg_4502" s="T138">think-PST.NAR.[3SG.S]</ta>
            <ta e="T140" id="Seg_4503" s="T139">thus.much</ta>
            <ta e="T141" id="Seg_4504" s="T140">food-TRL</ta>
            <ta e="T142" id="Seg_4505" s="T141">die-PST.NAR.[3SG.S]</ta>
            <ta e="T143" id="Seg_4506" s="T142">god.[NOM]</ta>
            <ta e="T144" id="Seg_4507" s="T143">know-PST.NAR.[3SG.S]</ta>
            <ta e="T145" id="Seg_4508" s="T144">sledge-GEN-3SG</ta>
            <ta e="T146" id="Seg_4509" s="T145">inside-LOC</ta>
            <ta e="T147" id="Seg_4510" s="T146">sleep-HAB.[3SG.S]</ta>
            <ta e="T148" id="Seg_4511" s="T147">thus.much-ADJZ</ta>
            <ta e="T149" id="Seg_4512" s="T148">ice-GEN-3SG</ta>
            <ta e="T150" id="Seg_4513" s="T149">piece.[NOM]</ta>
            <ta e="T151" id="Seg_4514" s="T150">sledge-GEN-3SG</ta>
            <ta e="T152" id="Seg_4515" s="T151">under-LOC</ta>
            <ta e="T153" id="Seg_4516" s="T152">stay-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T154" id="Seg_4517" s="T153">sea.[NOM]</ta>
            <ta e="T155" id="Seg_4518" s="T154">all</ta>
            <ta e="T156" id="Seg_4519" s="T155">melt-PST.NAR.[3SG.S]</ta>
            <ta e="T157" id="Seg_4520" s="T156">one</ta>
            <ta e="T158" id="Seg_4521" s="T157">one</ta>
            <ta e="T159" id="Seg_4522" s="T158">middle-LOC</ta>
            <ta e="T160" id="Seg_4523" s="T159">(s)he.[NOM]</ta>
            <ta e="T161" id="Seg_4524" s="T160">INDEF3</ta>
            <ta e="T162" id="Seg_4525" s="T161">what.[NOM]</ta>
            <ta e="T163" id="Seg_4526" s="T162">such-ADVZ</ta>
            <ta e="T164" id="Seg_4527" s="T163">say-CO-3SG.O</ta>
            <ta e="T165" id="Seg_4528" s="T164">either.or.[NOM]</ta>
            <ta e="T166" id="Seg_4529" s="T165">god.[NOM]</ta>
            <ta e="T167" id="Seg_4530" s="T166">whether</ta>
            <ta e="T168" id="Seg_4531" s="T167">either.or.[NOM]</ta>
            <ta e="T169" id="Seg_4532" s="T168">human.being.[NOM]</ta>
            <ta e="T170" id="Seg_4533" s="T169">mouse-COM</ta>
            <ta e="T171" id="Seg_4534" s="T170">run-CVB</ta>
            <ta e="T172" id="Seg_4535" s="T171">and</ta>
            <ta e="T173" id="Seg_4536" s="T172">such-ADVZ</ta>
            <ta e="T174" id="Seg_4537" s="T173">swing.[3SG.S]</ta>
            <ta e="T175" id="Seg_4538" s="T174">(s)he.[NOM]</ta>
            <ta e="T176" id="Seg_4539" s="T175">up</ta>
            <ta e="T177" id="Seg_4540" s="T176">hardly</ta>
            <ta e="T178" id="Seg_4541" s="T177">try-PST.NAR.[3SG.S]</ta>
            <ta e="T179" id="Seg_4542" s="T178">such-ADVZ</ta>
            <ta e="T180" id="Seg_4543" s="T179">swing.[3SG.S]</ta>
            <ta e="T181" id="Seg_4544" s="T180">only</ta>
            <ta e="T182" id="Seg_4545" s="T181">river.bend.[NOM]</ta>
            <ta e="T183" id="Seg_4546" s="T182">cloud.[NOM]</ta>
            <ta e="T184" id="Seg_4547" s="T183">be.visible-INCH-PST.NAR-INFER-3PL</ta>
            <ta e="T185" id="Seg_4548" s="T184">this</ta>
            <ta e="T186" id="Seg_4549" s="T185">sledge-GEN-3SG</ta>
            <ta e="T187" id="Seg_4550" s="T186">under-LOC-ADJZ</ta>
            <ta e="T188" id="Seg_4551" s="T187">ice-ADJZ</ta>
            <ta e="T189" id="Seg_4552" s="T188">piece-COM</ta>
            <ta e="T190" id="Seg_4553" s="T189">appear-FRQ-CVB</ta>
            <ta e="T191" id="Seg_4554" s="T190">then</ta>
            <ta e="T192" id="Seg_4555" s="T191">and</ta>
            <ta e="T193" id="Seg_4556" s="T192">down</ta>
            <ta e="T194" id="Seg_4557" s="T193">sleep.[3SG.S]</ta>
            <ta e="T195" id="Seg_4558" s="T194">and</ta>
            <ta e="T196" id="Seg_4559" s="T195">(s)he.[NOM]</ta>
            <ta e="T197" id="Seg_4560" s="T196">INDEF3</ta>
            <ta e="T198" id="Seg_4561" s="T197">what.[NOM]</ta>
            <ta e="T199" id="Seg_4562" s="T198">such-ADVZ</ta>
            <ta e="T200" id="Seg_4563" s="T199">I.ACC</ta>
            <ta e="T201" id="Seg_4564" s="T200">ask-INFER.[3SG.S]</ta>
            <ta e="T202" id="Seg_4565" s="T201">human.being.[NOM]</ta>
            <ta e="T203" id="Seg_4566" s="T202">such-ADVZ</ta>
            <ta e="T204" id="Seg_4567" s="T203">down</ta>
            <ta e="T205" id="Seg_4568" s="T204">go-CVB</ta>
            <ta e="T206" id="Seg_4569" s="T205">swing.[3SG.S]</ta>
            <ta e="T207" id="Seg_4570" s="T206">then</ta>
            <ta e="T208" id="Seg_4571" s="T207">bank-ILL</ta>
            <ta e="T209" id="Seg_4572" s="T208">upwards</ta>
            <ta e="T210" id="Seg_4573" s="T209">go.out.[3SG.S]</ta>
            <ta e="T211" id="Seg_4574" s="T210">go.out-PST.NAR.[3SG.S]</ta>
            <ta e="T212" id="Seg_4575" s="T211">eye.[NOM]-3SG</ta>
            <ta e="T213" id="Seg_4576" s="T212">thus.much</ta>
            <ta e="T214" id="Seg_4577" s="T213">food-TRL</ta>
            <ta e="T215" id="Seg_4578" s="T214">die-PST.NAR.[3SG.S]</ta>
            <ta e="T216" id="Seg_4579" s="T215">hole-EP-GEN</ta>
            <ta e="T217" id="Seg_4580" s="T216">inside-ILL</ta>
            <ta e="T218" id="Seg_4581" s="T217">get.stuck-DRV-PST.NAR.[3SG.S]</ta>
            <ta e="T219" id="Seg_4582" s="T218">one</ta>
            <ta e="T220" id="Seg_4583" s="T219">middle-LOC</ta>
            <ta e="T221" id="Seg_4584" s="T220">such-ADVZ</ta>
            <ta e="T222" id="Seg_4585" s="T221">be-PST.[3SG.S]</ta>
            <ta e="T223" id="Seg_4586" s="T222">as.if</ta>
            <ta e="T224" id="Seg_4587" s="T223">earth.[NOM]</ta>
            <ta e="T225" id="Seg_4588" s="T224">be.visible-INFER.[3SG.S]</ta>
            <ta e="T226" id="Seg_4589" s="T225">as.if</ta>
            <ta e="T227" id="Seg_4590" s="T226">earth.[NOM]</ta>
            <ta e="T228" id="Seg_4591" s="T227">melt-FRQ-EP-INFER.[3SG.S]</ta>
            <ta e="T229" id="Seg_4592" s="T228">sea-GEN</ta>
            <ta e="T230" id="Seg_4593" s="T229">bank-LOC</ta>
            <ta e="T231" id="Seg_4594" s="T230">(s)he.[NOM]</ta>
            <ta e="T232" id="Seg_4595" s="T231">thus.much</ta>
            <ta e="T233" id="Seg_4596" s="T232">food-TRL</ta>
            <ta e="T234" id="Seg_4597" s="T233">die-PST.NAR.[3SG.S]</ta>
            <ta e="T235" id="Seg_4598" s="T234">then</ta>
            <ta e="T236" id="Seg_4599" s="T235">such-ADVZ</ta>
            <ta e="T237" id="Seg_4600" s="T236">hardly</ta>
            <ta e="T238" id="Seg_4601" s="T237">leave-CO.[3SG.S]</ta>
            <ta e="T239" id="Seg_4602" s="T238">sledge-ACC</ta>
            <ta e="T240" id="Seg_4603" s="T239">earth-ADV.LOC</ta>
            <ta e="T241" id="Seg_4604" s="T240">bend-3SG.O</ta>
            <ta e="T242" id="Seg_4605" s="T241">come-US-PST.NAR.[3SG.S]</ta>
            <ta e="T243" id="Seg_4606" s="T242">then</ta>
            <ta e="T244" id="Seg_4607" s="T243">sledge-GEN-3SG</ta>
            <ta e="T245" id="Seg_4608" s="T244">back-EP-GEN</ta>
            <ta e="T246" id="Seg_4609" s="T245">on-PROL</ta>
            <ta e="T247" id="Seg_4610" s="T246">crawl-CVB</ta>
            <ta e="T248" id="Seg_4611" s="T247">upwards</ta>
            <ta e="T249" id="Seg_4612" s="T248">go.out.[3SG.S]</ta>
            <ta e="T250" id="Seg_4613" s="T249">then</ta>
            <ta e="T251" id="Seg_4614" s="T250">upwards</ta>
            <ta e="T252" id="Seg_4615" s="T251">bank-ILL</ta>
            <ta e="T253" id="Seg_4616" s="T252">get.into.[3SG.S]</ta>
            <ta e="T254" id="Seg_4617" s="T253">then</ta>
            <ta e="T255" id="Seg_4618" s="T254">there</ta>
            <ta e="T256" id="Seg_4619" s="T255">lie.[3SG.S]</ta>
            <ta e="T257" id="Seg_4620" s="T256">again</ta>
            <ta e="T258" id="Seg_4621" s="T257">INDEF3</ta>
            <ta e="T259" id="Seg_4622" s="T258">what.[NOM]</ta>
            <ta e="T260" id="Seg_4623" s="T259">such-ADVZ</ta>
            <ta e="T261" id="Seg_4624" s="T260">say-CO-3SG.O</ta>
            <ta e="T262" id="Seg_4625" s="T261">human.being.[NOM]</ta>
            <ta e="T263" id="Seg_4626" s="T262">mouse-COM</ta>
            <ta e="T264" id="Seg_4627" s="T263">go-CVB</ta>
            <ta e="T265" id="Seg_4628" s="T264">go.down-CO.[3SG.S]</ta>
            <ta e="T266" id="Seg_4629" s="T265">upwards-ADV.LOC</ta>
            <ta e="T267" id="Seg_4630" s="T266">purple.osier-ADJZ</ta>
            <ta e="T268" id="Seg_4631" s="T267">island-GEN</ta>
            <ta e="T269" id="Seg_4632" s="T268">edge-EP-ILL</ta>
            <ta e="T270" id="Seg_4633" s="T269">crawl-CVB</ta>
            <ta e="T271" id="Seg_4634" s="T270">upwards</ta>
            <ta e="T272" id="Seg_4635" s="T271">go.out.[3SG.S]</ta>
            <ta e="T273" id="Seg_4636" s="T272">then</ta>
            <ta e="T274" id="Seg_4637" s="T273">upwards</ta>
            <ta e="T275" id="Seg_4638" s="T274">crawl-CVB</ta>
            <ta e="T276" id="Seg_4639" s="T275">go.out.[3SG.S]</ta>
            <ta e="T277" id="Seg_4640" s="T276">I.NOM</ta>
            <ta e="T278" id="Seg_4641" s="T277">on.one_s.back</ta>
            <ta e="T279" id="Seg_4642" s="T278">up</ta>
            <ta e="T280" id="Seg_4643" s="T279">stand-PFV-PST-1SG.S</ta>
            <ta e="T281" id="Seg_4644" s="T280">CONJ</ta>
            <ta e="T282" id="Seg_4645" s="T281">upwards</ta>
            <ta e="T283" id="Seg_4646" s="T282">crawl-CVB</ta>
            <ta e="T284" id="Seg_4647" s="T283">go.out.[3SG.S]</ta>
            <ta e="T285" id="Seg_4648" s="T284">force.[NOM]-3SG</ta>
            <ta e="T286" id="Seg_4649" s="T285">always</ta>
            <ta e="T287" id="Seg_4650" s="T286">NEG.EX.[3SG.S]</ta>
            <ta e="T288" id="Seg_4651" s="T287">again</ta>
            <ta e="T289" id="Seg_4652" s="T288">INDEF3</ta>
            <ta e="T290" id="Seg_4653" s="T289">what.[NOM]</ta>
            <ta e="T291" id="Seg_4654" s="T290">such-ADVZ</ta>
            <ta e="T292" id="Seg_4655" s="T291">ask-INCH-3SG.O</ta>
            <ta e="T293" id="Seg_4656" s="T292">human.being.[NOM]</ta>
            <ta e="T294" id="Seg_4657" s="T293">NEG</ta>
            <ta e="T295" id="Seg_4658" s="T294">such-ADVZ</ta>
            <ta e="T296" id="Seg_4659" s="T295">swing.[3SG.S]</ta>
            <ta e="T297" id="Seg_4660" s="T296">go-CVB</ta>
            <ta e="T298" id="Seg_4661" s="T297">this</ta>
            <ta e="T299" id="Seg_4662" s="T298">sea-GEN</ta>
            <ta e="T300" id="Seg_4663" s="T299">near-LOC</ta>
            <ta e="T301" id="Seg_4664" s="T300">high</ta>
            <ta e="T302" id="Seg_4665" s="T301">earth.[NOM]</ta>
            <ta e="T303" id="Seg_4666" s="T302">up</ta>
            <ta e="T304" id="Seg_4667" s="T303">crawl-INCH-CO.[3SG.S]</ta>
            <ta e="T305" id="Seg_4668" s="T304">then</ta>
            <ta e="T306" id="Seg_4669" s="T305">upwards</ta>
            <ta e="T307" id="Seg_4670" s="T306">crawl-CVB</ta>
            <ta e="T308" id="Seg_4671" s="T307">such-ADVZ</ta>
            <ta e="T309" id="Seg_4672" s="T308">give.a.look-DUR.[3SG.S]</ta>
            <ta e="T310" id="Seg_4673" s="T309">upwards-ADV.LOC</ta>
            <ta e="T311" id="Seg_4674" s="T310">such-ADVZ</ta>
            <ta e="T312" id="Seg_4675" s="T311">see-CO-3SG.O</ta>
            <ta e="T313" id="Seg_4676" s="T312">leather</ta>
            <ta e="T314" id="Seg_4677" s="T313">tent.[NOM]</ta>
            <ta e="T315" id="Seg_4678" s="T314">stand-INFER.[3SG.S]</ta>
            <ta e="T316" id="Seg_4679" s="T315">then</ta>
            <ta e="T317" id="Seg_4680" s="T316">again</ta>
            <ta e="T318" id="Seg_4681" s="T317">down</ta>
            <ta e="T319" id="Seg_4682" s="T318">fall-CO.[3SG.S]</ta>
            <ta e="T320" id="Seg_4683" s="T319">how</ta>
            <ta e="T321" id="Seg_4684" s="T320">there</ta>
            <ta e="T322" id="Seg_4685" s="T321">come-US-IPFV-FUT-1SG.S</ta>
            <ta e="T323" id="Seg_4686" s="T322">then</ta>
            <ta e="T324" id="Seg_4687" s="T323">this</ta>
            <ta e="T325" id="Seg_4688" s="T324">tent-ILL</ta>
            <ta e="T326" id="Seg_4689" s="T325">upwards</ta>
            <ta e="T327" id="Seg_4690" s="T326">stand-CVB</ta>
            <ta e="T328" id="Seg_4691" s="T327">down</ta>
            <ta e="T329" id="Seg_4692" s="T328">come-CO.[3SG.S]</ta>
            <ta e="T330" id="Seg_4693" s="T329">Nenets.[NOM]</ta>
            <ta e="T331" id="Seg_4694" s="T330">tent.[NOM]</ta>
            <ta e="T332" id="Seg_4695" s="T331">stand-INFER.[3SG.S]</ta>
            <ta e="T333" id="Seg_4696" s="T332">reindeer.[NOM]-3SG</ta>
            <ta e="T334" id="Seg_4697" s="T333">outside-LOC</ta>
            <ta e="T335" id="Seg_4698" s="T334">go-INCH-PST.NAR-INFER-3PL</ta>
            <ta e="T336" id="Seg_4699" s="T335">then</ta>
            <ta e="T337" id="Seg_4700" s="T336">tent-ILL</ta>
            <ta e="T338" id="Seg_4701" s="T337">come.in-CO.[3SG.S]</ta>
            <ta e="T339" id="Seg_4702" s="T338">old.man.[NOM]</ta>
            <ta e="T340" id="Seg_4703" s="T339">one</ta>
            <ta e="T341" id="Seg_4704" s="T340">daughter.[NOM]-3SG</ta>
            <ta e="T342" id="Seg_4705" s="T341">one</ta>
            <ta e="T343" id="Seg_4706" s="T342">middle-LOC</ta>
            <ta e="T521" id="Seg_4707" s="T343">Nenets.[NOM]</ta>
            <ta e="T344" id="Seg_4708" s="T521">old.man.[NOM]</ta>
            <ta e="T345" id="Seg_4709" s="T344">such-ADVZ</ta>
            <ta e="T346" id="Seg_4710" s="T345">say.[3SG.S]</ta>
            <ta e="T347" id="Seg_4711" s="T346">you.SG.NOM</ta>
            <ta e="T348" id="Seg_4712" s="T347">this</ta>
            <ta e="T349" id="Seg_4713" s="T348">human.being.[NOM]</ta>
            <ta e="T350" id="Seg_4714" s="T349">food-TR-IMP.2SG.O</ta>
            <ta e="T351" id="Seg_4715" s="T350">(s)he.[NOM]</ta>
            <ta e="T352" id="Seg_4716" s="T351">this</ta>
            <ta e="T353" id="Seg_4717" s="T352">old.man-GEN</ta>
            <ta e="T354" id="Seg_4718" s="T353">something-LOC</ta>
            <ta e="T355" id="Seg_4719" s="T354">two</ta>
            <ta e="T356" id="Seg_4720" s="T355">hundred</ta>
            <ta e="T357" id="Seg_4721" s="T356">reindeer-PL.[NOM]</ta>
            <ta e="T358" id="Seg_4722" s="T357">I.GEN</ta>
            <ta e="T359" id="Seg_4723" s="T358">one</ta>
            <ta e="T360" id="Seg_4724" s="T359">hundred</ta>
            <ta e="T361" id="Seg_4725" s="T360">reindeer.[NOM]-1SG</ta>
            <ta e="T362" id="Seg_4726" s="T361">wild.animal.[NOM]</ta>
            <ta e="T363" id="Seg_4727" s="T362">all</ta>
            <ta e="T364" id="Seg_4728" s="T363">eat-CO-3SG.O</ta>
            <ta e="T365" id="Seg_4729" s="T364">old.man.[NOM]</ta>
            <ta e="T366" id="Seg_4730" s="T365">such-ADVZ</ta>
            <ta e="T367" id="Seg_4731" s="T366">say-CO-3SG.O</ta>
            <ta e="T368" id="Seg_4732" s="T367">you.SG.NOM</ta>
            <ta e="T369" id="Seg_4733" s="T368">outwards</ta>
            <ta e="T370" id="Seg_4734" s="T369">go.out-CVB</ta>
            <ta e="T371" id="Seg_4735" s="T370">I.GEN</ta>
            <ta e="T372" id="Seg_4736" s="T371">reindeer.[NOM]-1SG</ta>
            <ta e="T373" id="Seg_4737" s="T372">watch.for-IMP.2SG.O</ta>
            <ta e="T374" id="Seg_4738" s="T373">darkness-VBLZ-EP-DRV-CO.[3SG.S]</ta>
            <ta e="T375" id="Seg_4739" s="T374">(s)he.[NOM]</ta>
            <ta e="T376" id="Seg_4740" s="T375">outwards</ta>
            <ta e="T377" id="Seg_4741" s="T376">go.out.[3SG.S]</ta>
            <ta e="T378" id="Seg_4742" s="T377">(s)he.[NOM]</ta>
            <ta e="T379" id="Seg_4743" s="T378">outwards</ta>
            <ta e="T380" id="Seg_4744" s="T379">go.out.[3SG.S]</ta>
            <ta e="T381" id="Seg_4745" s="T380">thus.much</ta>
            <ta e="T382" id="Seg_4746" s="T381">be.slow-DUR.[3SG.S]</ta>
            <ta e="T383" id="Seg_4747" s="T382">human.being.[NOM]</ta>
            <ta e="T384" id="Seg_4748" s="T383">what.[NOM]-3SG</ta>
            <ta e="T385" id="Seg_4749" s="T384">give.a.look-DUR-FUT-3SG.O</ta>
            <ta e="T386" id="Seg_4750" s="T385">then</ta>
            <ta e="T387" id="Seg_4751" s="T386">(s)he.[NOM]</ta>
            <ta e="T388" id="Seg_4752" s="T387">again</ta>
            <ta e="T389" id="Seg_4753" s="T388">down</ta>
            <ta e="T390" id="Seg_4754" s="T389">fall-PST.NAR.[3SG.S]</ta>
            <ta e="T391" id="Seg_4755" s="T390">there</ta>
            <ta e="T392" id="Seg_4756" s="T391">INFER</ta>
            <ta e="T393" id="Seg_4757" s="T392">lie-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T394" id="Seg_4758" s="T393">INFER</ta>
            <ta e="T395" id="Seg_4759" s="T394">(s)he-EP-GEN</ta>
            <ta e="T396" id="Seg_4760" s="T395">come-CVB</ta>
            <ta e="T397" id="Seg_4761" s="T396">after</ta>
            <ta e="T398" id="Seg_4762" s="T397">wild.animal.[NOM]</ta>
            <ta e="T399" id="Seg_4763" s="T398">INFER</ta>
            <ta e="T400" id="Seg_4764" s="T399">go.away-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T401" id="Seg_4765" s="T400">there</ta>
            <ta e="T402" id="Seg_4766" s="T401">earlier</ta>
            <ta e="T403" id="Seg_4767" s="T402">father-DYA-DU.[NOM]</ta>
            <ta e="T404" id="Seg_4768" s="T403">reindeer-OBL.3SG-ADJZ</ta>
            <ta e="T405" id="Seg_4769" s="T404">watch.for-CVB</ta>
            <ta e="T406" id="Seg_4770" s="T405">space.outside-LOC</ta>
            <ta e="T407" id="Seg_4771" s="T406">live-PST.NAR-3DU.S</ta>
            <ta e="T408" id="Seg_4772" s="T407">then</ta>
            <ta e="T409" id="Seg_4773" s="T408">this</ta>
            <ta e="T410" id="Seg_4774" s="T409">human.being-EP-ACC</ta>
            <ta e="T411" id="Seg_4775" s="T410">food-TR-DUR-CVB</ta>
            <ta e="T412" id="Seg_4776" s="T411">there</ta>
            <ta e="T413" id="Seg_4777" s="T412">INFER</ta>
            <ta e="T414" id="Seg_4778" s="T413">live-INCH-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T415" id="Seg_4779" s="T414">then</ta>
            <ta e="T416" id="Seg_4780" s="T415">this</ta>
            <ta e="T417" id="Seg_4781" s="T416">old.man.[NOM]</ta>
            <ta e="T418" id="Seg_4782" s="T417">again</ta>
            <ta e="T419" id="Seg_4783" s="T418">such-ADVZ</ta>
            <ta e="T420" id="Seg_4784" s="T419">say-CO-3SG.O</ta>
            <ta e="T421" id="Seg_4785" s="T420">again</ta>
            <ta e="T422" id="Seg_4786" s="T421">go-IMP.2SG.S</ta>
            <ta e="T423" id="Seg_4787" s="T422">reindeer-ACC-3SG</ta>
            <ta e="T424" id="Seg_4788" s="T423">watch.for-CVB</ta>
            <ta e="T425" id="Seg_4789" s="T424">reindeer.[NOM]-3SG</ta>
            <ta e="T426" id="Seg_4790" s="T425">supposedly</ta>
            <ta e="T427" id="Seg_4791" s="T426">forest-ILL</ta>
            <ta e="T428" id="Seg_4792" s="T427">NEG</ta>
            <ta e="T429" id="Seg_4793" s="T428">go.away-FUT.[3SG.S]</ta>
            <ta e="T430" id="Seg_4794" s="T429">tent-EP-GEN</ta>
            <ta e="T431" id="Seg_4795" s="T430">near-LOC</ta>
            <ta e="T432" id="Seg_4796" s="T431">sleep-3PL</ta>
            <ta e="T433" id="Seg_4797" s="T432">then</ta>
            <ta e="T434" id="Seg_4798" s="T433">(s)he.[NOM]</ta>
            <ta e="T435" id="Seg_4799" s="T434">outwards</ta>
            <ta e="T436" id="Seg_4800" s="T435">go.out.[3SG.S]</ta>
            <ta e="T437" id="Seg_4801" s="T436">enough-ADJZ</ta>
            <ta e="T438" id="Seg_4802" s="T437">something-ILL</ta>
            <ta e="T439" id="Seg_4803" s="T438">go.away-CVB</ta>
            <ta e="T440" id="Seg_4804" s="T439">again</ta>
            <ta e="T441" id="Seg_4805" s="T440">down</ta>
            <ta e="T442" id="Seg_4806" s="T441">fall.[3SG.S]</ta>
            <ta e="T443" id="Seg_4807" s="T442">then</ta>
            <ta e="T444" id="Seg_4808" s="T443">(s)he.[NOM]</ta>
            <ta e="T445" id="Seg_4809" s="T444">one</ta>
            <ta e="T446" id="Seg_4810" s="T445">middle-LOC</ta>
            <ta e="T447" id="Seg_4811" s="T446">such-ADVZ</ta>
            <ta e="T448" id="Seg_4812" s="T447">wake.up-CO.[3SG.S]</ta>
            <ta e="T449" id="Seg_4813" s="T448">what.[NOM]</ta>
            <ta e="T450" id="Seg_4814" s="T449">go.away-TR-3SG.O</ta>
            <ta e="T451" id="Seg_4815" s="T450">apparently</ta>
            <ta e="T452" id="Seg_4816" s="T451">live-ATTEN.[3SG.S]</ta>
            <ta e="T453" id="Seg_4817" s="T452">then</ta>
            <ta e="T454" id="Seg_4818" s="T453">home</ta>
            <ta e="T455" id="Seg_4819" s="T454">come-CO.[3SG.S]</ta>
            <ta e="T456" id="Seg_4820" s="T455">tent-ILL</ta>
            <ta e="T522" id="Seg_4821" s="T456">Nenets.[NOM]</ta>
            <ta e="T457" id="Seg_4822" s="T522">old.man.[NOM]</ta>
            <ta e="T458" id="Seg_4823" s="T457">sleep.[3SG.S]</ta>
            <ta e="T459" id="Seg_4824" s="T458">then</ta>
            <ta e="T460" id="Seg_4825" s="T459">(s)he-EP.[NOM]-3PL</ta>
            <ta e="T461" id="Seg_4826" s="T460">reindeer.[NOM]</ta>
            <ta e="T462" id="Seg_4827" s="T461">always</ta>
            <ta e="T463" id="Seg_4828" s="T462">outside-LOC</ta>
            <ta e="T464" id="Seg_4829" s="T463">sleep-3PL</ta>
            <ta e="T465" id="Seg_4830" s="T464">(s)he.[NOM]</ta>
            <ta e="T466" id="Seg_4831" s="T465">this</ta>
            <ta e="T467" id="Seg_4832" s="T466">come-PTCP.PST</ta>
            <ta e="T468" id="Seg_4833" s="T467">clothing-ADJZ</ta>
            <ta e="T469" id="Seg_4834" s="T468">footwear-PL-EP-COM</ta>
            <ta e="T470" id="Seg_4835" s="T469">live.[3SG.S]</ta>
            <ta e="T471" id="Seg_4836" s="T470">then</ta>
            <ta e="T472" id="Seg_4837" s="T471">three-EP-ORD</ta>
            <ta e="T473" id="Seg_4838" s="T472">night-ADV.LOC</ta>
            <ta e="T474" id="Seg_4839" s="T473">such-ADVZ</ta>
            <ta e="T475" id="Seg_4840" s="T474">become.[3SG.S]</ta>
            <ta e="T476" id="Seg_4841" s="T475">as.if</ta>
            <ta e="T477" id="Seg_4842" s="T476">he.says</ta>
            <ta e="T478" id="Seg_4843" s="T477">good-ADVZ</ta>
            <ta e="T479" id="Seg_4844" s="T478">become-1SG.S</ta>
            <ta e="T523" id="Seg_4845" s="T479">Nenets.[NOM]</ta>
            <ta e="T480" id="Seg_4846" s="T523">old.man.[NOM]</ta>
            <ta e="T481" id="Seg_4847" s="T480">reindeer.[NOM]-3SG</ta>
            <ta e="T482" id="Seg_4848" s="T481">forest-ILL</ta>
            <ta e="T483" id="Seg_4849" s="T482">NEG</ta>
            <ta e="T484" id="Seg_4850" s="T483">go.away-FUT.[3SG.S]</ta>
            <ta e="T485" id="Seg_4851" s="T484">tent-EP-GEN</ta>
            <ta e="T486" id="Seg_4852" s="T485">near-LOC</ta>
            <ta e="T487" id="Seg_4853" s="T486">there</ta>
            <ta e="T488" id="Seg_4854" s="T487">go.away-INFER-3PL</ta>
            <ta e="T489" id="Seg_4855" s="T488">(s)he.[NOM]</ta>
            <ta e="T490" id="Seg_4856" s="T489">come-CVB</ta>
            <ta e="T491" id="Seg_4857" s="T490">again</ta>
            <ta e="T492" id="Seg_4858" s="T491">down</ta>
            <ta e="T493" id="Seg_4859" s="T492">sleep-CO.[3SG.S]</ta>
            <ta e="T494" id="Seg_4860" s="T493">next</ta>
            <ta e="T495" id="Seg_4861" s="T494">morning-ADV.LOC</ta>
            <ta e="T496" id="Seg_4862" s="T495">apparently</ta>
            <ta e="T497" id="Seg_4863" s="T496">snow.[NOM]</ta>
            <ta e="T498" id="Seg_4864" s="T497">put-PFV-INFER-3SG.O</ta>
            <ta e="T499" id="Seg_4865" s="T498">I.NOM</ta>
            <ta e="T500" id="Seg_4866" s="T499">(s)he.[NOM]</ta>
            <ta e="T501" id="Seg_4867" s="T500">how</ta>
            <ta e="T502" id="Seg_4868" s="T501">come-PST.[3SG.S]</ta>
            <ta e="T503" id="Seg_4869" s="T502">reindeer.[NOM]-1SG</ta>
            <ta e="T504" id="Seg_4870" s="T503">forest-ILL</ta>
            <ta e="T505" id="Seg_4871" s="T504">NEG</ta>
            <ta e="T506" id="Seg_4872" s="T505">go.away-FUT.[3SG.S]</ta>
            <ta e="T507" id="Seg_4873" s="T506">turn-TR-3SG.O</ta>
            <ta e="T508" id="Seg_4874" s="T507">snow-ACC</ta>
            <ta e="T509" id="Seg_4875" s="T508">put-CVB</ta>
            <ta e="T510" id="Seg_4876" s="T509">leave-EP-CO.[3SG.S]</ta>
            <ta e="T511" id="Seg_4877" s="T510">good-ADVZ</ta>
            <ta e="T512" id="Seg_4878" s="T511">down</ta>
            <ta e="T513" id="Seg_4879" s="T512">INFER</ta>
            <ta e="T514" id="Seg_4880" s="T513">leave-INFER.[3SG.S]</ta>
            <ta e="T515" id="Seg_4881" s="T514">(s)he.[NOM]</ta>
            <ta e="T516" id="Seg_4882" s="T515">then</ta>
            <ta e="T517" id="Seg_4883" s="T516">good-ADVZ</ta>
            <ta e="T518" id="Seg_4884" s="T517">become.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_4885" s="T0">этот</ta>
            <ta e="T96" id="Seg_4886" s="T1">ненец.[NOM]</ta>
            <ta e="T2" id="Seg_4887" s="T96">старик.[NOM]</ta>
            <ta e="T3" id="Seg_4888" s="T2">парень.[NOM]</ta>
            <ta e="T4" id="Seg_4889" s="T3">такой-ADVZ</ta>
            <ta e="T5" id="Seg_4890" s="T4">сказать-CO-3SG.O</ta>
            <ta e="T6" id="Seg_4891" s="T5">я.NOM</ta>
            <ta e="T7" id="Seg_4892" s="T6">этот</ta>
            <ta e="T8" id="Seg_4893" s="T7">море-LOC</ta>
            <ta e="T9" id="Seg_4894" s="T8">белый</ta>
            <ta e="T10" id="Seg_4895" s="T9">медведь-PL.[NOM]</ta>
            <ta e="T11" id="Seg_4896" s="T10">убить-FUT-3SG.O</ta>
            <ta e="T519" id="Seg_4897" s="T11">ненец.[NOM]</ta>
            <ta e="T12" id="Seg_4898" s="T519">старик.[NOM]</ta>
            <ta e="T13" id="Seg_4899" s="T12">такой-ADVZ</ta>
            <ta e="T14" id="Seg_4900" s="T13">сказать-CO-3SG.O</ta>
            <ta e="T15" id="Seg_4901" s="T14">вот</ta>
            <ta e="T16" id="Seg_4902" s="T15">NEG</ta>
            <ta e="T17" id="Seg_4903" s="T16">человек-EP-PL.[NOM]</ta>
            <ta e="T18" id="Seg_4904" s="T17">убить-PST.[3SG.S]</ta>
            <ta e="T19" id="Seg_4905" s="T18">зверь.[NOM]</ta>
            <ta e="T20" id="Seg_4906" s="T19">ты.NOM</ta>
            <ta e="T21" id="Seg_4907" s="T20">как</ta>
            <ta e="T22" id="Seg_4908" s="T21">уйти-CO-2SG.S</ta>
            <ta e="T23" id="Seg_4909" s="T22">море-GEN</ta>
            <ta e="T24" id="Seg_4910" s="T23">за</ta>
            <ta e="T25" id="Seg_4911" s="T24">вроде</ta>
            <ta e="T26" id="Seg_4912" s="T25">дерево-CAR-ADJZ</ta>
            <ta e="T27" id="Seg_4913" s="T26">земля-LOC</ta>
            <ta e="T28" id="Seg_4914" s="T27">жить-3SG.O</ta>
            <ta e="T520" id="Seg_4915" s="T28">ненец.[NOM]</ta>
            <ta e="T29" id="Seg_4916" s="T520">старик.[NOM]</ta>
            <ta e="T30" id="Seg_4917" s="T29">он(а)-EP-GEN</ta>
            <ta e="T31" id="Seg_4918" s="T30">три</ta>
            <ta e="T32" id="Seg_4919" s="T31">брат-DYA-PL.[NOM]</ta>
            <ta e="T33" id="Seg_4920" s="T32">море-GEN</ta>
            <ta e="T34" id="Seg_4921" s="T33">за</ta>
            <ta e="T35" id="Seg_4922" s="T34">этот</ta>
            <ta e="T36" id="Seg_4923" s="T35">белый</ta>
            <ta e="T37" id="Seg_4924" s="T36">медведь-ACC</ta>
            <ta e="T38" id="Seg_4925" s="T37">убить-INF</ta>
            <ta e="T39" id="Seg_4926" s="T38">уйти-CO-3PL</ta>
            <ta e="T40" id="Seg_4927" s="T39">он(а)-EP-PL.[NOM]</ta>
            <ta e="T41" id="Seg_4928" s="T40">отправиться-CO-3PL</ta>
            <ta e="T42" id="Seg_4929" s="T41">этот</ta>
            <ta e="T43" id="Seg_4930" s="T42">белый</ta>
            <ta e="T44" id="Seg_4931" s="T43">медведь-ACC</ta>
            <ta e="T45" id="Seg_4932" s="T44">мать-ILL</ta>
            <ta e="T46" id="Seg_4933" s="T45">стрелять-CO-3SG.O</ta>
            <ta e="T47" id="Seg_4934" s="T46">потом</ta>
            <ta e="T48" id="Seg_4935" s="T47">белый</ta>
            <ta e="T49" id="Seg_4936" s="T48">медведь-ACC</ta>
            <ta e="T51" id="Seg_4937" s="T50">этот</ta>
            <ta e="T52" id="Seg_4938" s="T51">белый</ta>
            <ta e="T53" id="Seg_4939" s="T52">медведь.[NOM]</ta>
            <ta e="T54" id="Seg_4940" s="T53">мол</ta>
            <ta e="T55" id="Seg_4941" s="T54">большой</ta>
            <ta e="T56" id="Seg_4942" s="T55">быть-CO.[3SG.S]</ta>
            <ta e="T57" id="Seg_4943" s="T56">потом</ta>
            <ta e="T58" id="Seg_4944" s="T57">он(а)-EP-PL.[NOM]</ta>
            <ta e="T59" id="Seg_4945" s="T58">убить-CVB</ta>
            <ta e="T60" id="Seg_4946" s="T59">брюхо-GEN-3SG</ta>
            <ta e="T61" id="Seg_4947" s="T60">кровь-EP-PL.[NOM]</ta>
            <ta e="T62" id="Seg_4948" s="T61">на.части</ta>
            <ta e="T63" id="Seg_4949" s="T62">распороть-3PL</ta>
            <ta e="T64" id="Seg_4950" s="T63">и</ta>
            <ta e="T65" id="Seg_4951" s="T64">спина-PROL-OBL.3SG</ta>
            <ta e="T66" id="Seg_4952" s="T65">на.части</ta>
            <ta e="T67" id="Seg_4953" s="T66">распороть-3PL</ta>
            <ta e="T68" id="Seg_4954" s="T67">потом</ta>
            <ta e="T69" id="Seg_4955" s="T68">этот</ta>
            <ta e="T70" id="Seg_4956" s="T69">два</ta>
            <ta e="T71" id="Seg_4957" s="T70">брат-PL.[NOM]</ta>
            <ta e="T72" id="Seg_4958" s="T71">вверх</ta>
            <ta e="T73" id="Seg_4959" s="T72">нарты-ILL-OBL.3SG</ta>
            <ta e="T74" id="Seg_4960" s="T73">нагрузить-3PL</ta>
            <ta e="T75" id="Seg_4961" s="T74">этот</ta>
            <ta e="T76" id="Seg_4962" s="T75">брат-DYA-PL.[NOM]</ta>
            <ta e="T77" id="Seg_4963" s="T76">домой</ta>
            <ta e="T78" id="Seg_4964" s="T77">тронуться-3PL</ta>
            <ta e="T79" id="Seg_4965" s="T78">тот</ta>
            <ta e="T80" id="Seg_4966" s="T79">середина-LOC-ADJZ</ta>
            <ta e="T81" id="Seg_4967" s="T80">белый</ta>
            <ta e="T82" id="Seg_4968" s="T81">медведь.[NOM]</ta>
            <ta e="T83" id="Seg_4969" s="T82">убить-PTCP.PST</ta>
            <ta e="T84" id="Seg_4970" s="T83">брат.[NOM]-3PL</ta>
            <ta e="T85" id="Seg_4971" s="T84">середина-LOC</ta>
            <ta e="T86" id="Seg_4972" s="T85">домой</ta>
            <ta e="T87" id="Seg_4973" s="T86">взять-PST.NAR.[3SG.S]</ta>
            <ta e="T88" id="Seg_4974" s="T87">потом</ta>
            <ta e="T89" id="Seg_4975" s="T88">этот</ta>
            <ta e="T90" id="Seg_4976" s="T89">человек.[NOM]</ta>
            <ta e="T91" id="Seg_4977" s="T90">потеряться-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T92" id="Seg_4978" s="T91">скоро</ta>
            <ta e="T93" id="Seg_4979" s="T92">таять-PTCP.NEC</ta>
            <ta e="T94" id="Seg_4980" s="T93">улица-LOC</ta>
            <ta e="T95" id="Seg_4981" s="T94">быть-PST.NAR.[3SG.S]</ta>
            <ta e="T97" id="Seg_4982" s="T95">буря-TR-MOM-PST.NAR.[3SG.S]</ta>
            <ta e="T98" id="Seg_4983" s="T97">он(а).[NOM]</ta>
            <ta e="T99" id="Seg_4984" s="T98">потом</ta>
            <ta e="T100" id="Seg_4985" s="T99">совсем</ta>
            <ta e="T101" id="Seg_4986" s="T100">сила-CAR-PST.NAR.[3SG.S]</ta>
            <ta e="T102" id="Seg_4987" s="T101">он(а).[NOM]</ta>
            <ta e="T103" id="Seg_4988" s="T102">вот</ta>
            <ta e="T104" id="Seg_4989" s="T103">море-GEN</ta>
            <ta e="T105" id="Seg_4990" s="T104">берег-LOC</ta>
            <ta e="T106" id="Seg_4991" s="T105">потом</ta>
            <ta e="T107" id="Seg_4992" s="T106">INFER</ta>
            <ta e="T108" id="Seg_4993" s="T107">лежать-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T109" id="Seg_4994" s="T108">он(а).[NOM]</ta>
            <ta e="T110" id="Seg_4995" s="T109">там</ta>
            <ta e="T111" id="Seg_4996" s="T110">лежать-PST.NAR.[3SG.S]</ta>
            <ta e="T112" id="Seg_4997" s="T111">затем</ta>
            <ta e="T113" id="Seg_4998" s="T112">совсем-ADV.LOC</ta>
            <ta e="T114" id="Seg_4999" s="T113">сила-CAR-PST.NAR.[3SG.S]</ta>
            <ta e="T115" id="Seg_5000" s="T114">затем</ta>
            <ta e="T116" id="Seg_5001" s="T115">таять-PST.NAR.[3SG.S]</ta>
            <ta e="T117" id="Seg_5002" s="T116">сокуй.[NOM]-3SG</ta>
            <ta e="T118" id="Seg_5003" s="T117">обувь.[NOM]-3SG</ta>
            <ta e="T119" id="Seg_5004" s="T118">только</ta>
            <ta e="T120" id="Seg_5005" s="T119">проснуться.[3SG.S]</ta>
            <ta e="T121" id="Seg_5006" s="T120">потом</ta>
            <ta e="T122" id="Seg_5007" s="T121">вперёд</ta>
            <ta e="T123" id="Seg_5008" s="T122">отправиться-CAUS.[3SG.S]</ta>
            <ta e="T124" id="Seg_5009" s="T123">поводок.в.оленьей.упряжи-ACC-3SG</ta>
            <ta e="T125" id="Seg_5010" s="T124">нож-INSTR</ta>
            <ta e="T126" id="Seg_5011" s="T125">резать-CO-3SG.O</ta>
            <ta e="T127" id="Seg_5012" s="T126">вот</ta>
            <ta e="T128" id="Seg_5013" s="T127">три</ta>
            <ta e="T129" id="Seg_5014" s="T128">бык.[NOM]-3SG</ta>
            <ta e="T130" id="Seg_5015" s="T129">потом</ta>
            <ta e="T131" id="Seg_5016" s="T130">побежать.[3SG.S]</ta>
            <ta e="T132" id="Seg_5017" s="T131">он(а).[NOM]</ta>
            <ta e="T133" id="Seg_5018" s="T132">сам.3SG.[NOM]</ta>
            <ta e="T134" id="Seg_5019" s="T133">там</ta>
            <ta e="T135" id="Seg_5020" s="T134">лежать-PST.NAR.[3SG.S]</ta>
            <ta e="T136" id="Seg_5021" s="T135">остальной-ADJZ</ta>
            <ta e="T137" id="Seg_5022" s="T136">спать-HAB.[3SG.S]</ta>
            <ta e="T138" id="Seg_5023" s="T137">потом</ta>
            <ta e="T139" id="Seg_5024" s="T138">думать-PST.NAR.[3SG.S]</ta>
            <ta e="T140" id="Seg_5025" s="T139">настолько</ta>
            <ta e="T141" id="Seg_5026" s="T140">еда-TRL</ta>
            <ta e="T142" id="Seg_5027" s="T141">умереть-PST.NAR.[3SG.S]</ta>
            <ta e="T143" id="Seg_5028" s="T142">бог.[NOM]</ta>
            <ta e="T144" id="Seg_5029" s="T143">знать-PST.NAR.[3SG.S]</ta>
            <ta e="T145" id="Seg_5030" s="T144">нарты-GEN-3SG</ta>
            <ta e="T146" id="Seg_5031" s="T145">внутри-LOC</ta>
            <ta e="T147" id="Seg_5032" s="T146">спать-HAB.[3SG.S]</ta>
            <ta e="T148" id="Seg_5033" s="T147">настолько-ADJZ</ta>
            <ta e="T149" id="Seg_5034" s="T148">лёд-GEN-3SG</ta>
            <ta e="T150" id="Seg_5035" s="T149">кусок.[NOM]</ta>
            <ta e="T151" id="Seg_5036" s="T150">нарты-GEN-3SG</ta>
            <ta e="T152" id="Seg_5037" s="T151">под-LOC</ta>
            <ta e="T153" id="Seg_5038" s="T152">остаться-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T154" id="Seg_5039" s="T153">море.[NOM]</ta>
            <ta e="T155" id="Seg_5040" s="T154">всё</ta>
            <ta e="T156" id="Seg_5041" s="T155">таять-PST.NAR.[3SG.S]</ta>
            <ta e="T157" id="Seg_5042" s="T156">один</ta>
            <ta e="T158" id="Seg_5043" s="T157">один</ta>
            <ta e="T159" id="Seg_5044" s="T158">середина-LOC</ta>
            <ta e="T160" id="Seg_5045" s="T159">он(а).[NOM]</ta>
            <ta e="T161" id="Seg_5046" s="T160">INDEF3</ta>
            <ta e="T162" id="Seg_5047" s="T161">что.[NOM]</ta>
            <ta e="T163" id="Seg_5048" s="T162">такой-ADVZ</ta>
            <ta e="T164" id="Seg_5049" s="T163">сказать-CO-3SG.O</ta>
            <ta e="T165" id="Seg_5050" s="T164">то.ли.[NOM]</ta>
            <ta e="T166" id="Seg_5051" s="T165">бог.[NOM]</ta>
            <ta e="T167" id="Seg_5052" s="T166">что.ли</ta>
            <ta e="T168" id="Seg_5053" s="T167">то.ли.[NOM]</ta>
            <ta e="T169" id="Seg_5054" s="T168">человек.[NOM]</ta>
            <ta e="T170" id="Seg_5055" s="T169">мышь-COM</ta>
            <ta e="T171" id="Seg_5056" s="T170">бегать-CVB</ta>
            <ta e="T172" id="Seg_5057" s="T171">и</ta>
            <ta e="T173" id="Seg_5058" s="T172">такой-ADVZ</ta>
            <ta e="T174" id="Seg_5059" s="T173">качаться.[3SG.S]</ta>
            <ta e="T175" id="Seg_5060" s="T174">он(а).[NOM]</ta>
            <ta e="T176" id="Seg_5061" s="T175">вверх</ta>
            <ta e="T177" id="Seg_5062" s="T176">едва</ta>
            <ta e="T178" id="Seg_5063" s="T177">попытаться-PST.NAR.[3SG.S]</ta>
            <ta e="T179" id="Seg_5064" s="T178">такой-ADVZ</ta>
            <ta e="T180" id="Seg_5065" s="T179">качаться.[3SG.S]</ta>
            <ta e="T181" id="Seg_5066" s="T180">только</ta>
            <ta e="T182" id="Seg_5067" s="T181">изгиб.реки.[NOM]</ta>
            <ta e="T183" id="Seg_5068" s="T182">облако.[NOM]</ta>
            <ta e="T184" id="Seg_5069" s="T183">виднеться-INCH-PST.NAR-INFER-3PL</ta>
            <ta e="T185" id="Seg_5070" s="T184">этот</ta>
            <ta e="T186" id="Seg_5071" s="T185">нарты-GEN-3SG</ta>
            <ta e="T187" id="Seg_5072" s="T186">под-LOC-ADJZ</ta>
            <ta e="T188" id="Seg_5073" s="T187">лёд-ADJZ</ta>
            <ta e="T189" id="Seg_5074" s="T188">кусок-COM</ta>
            <ta e="T190" id="Seg_5075" s="T189">появиться-FRQ-CVB</ta>
            <ta e="T191" id="Seg_5076" s="T190">потом</ta>
            <ta e="T192" id="Seg_5077" s="T191">и</ta>
            <ta e="T193" id="Seg_5078" s="T192">вниз</ta>
            <ta e="T194" id="Seg_5079" s="T193">спать.[3SG.S]</ta>
            <ta e="T195" id="Seg_5080" s="T194">и</ta>
            <ta e="T196" id="Seg_5081" s="T195">он(а).[NOM]</ta>
            <ta e="T197" id="Seg_5082" s="T196">INDEF3</ta>
            <ta e="T198" id="Seg_5083" s="T197">что.[NOM]</ta>
            <ta e="T199" id="Seg_5084" s="T198">такой-ADVZ</ta>
            <ta e="T200" id="Seg_5085" s="T199">я.ACC</ta>
            <ta e="T201" id="Seg_5086" s="T200">спросить-INFER.[3SG.S]</ta>
            <ta e="T202" id="Seg_5087" s="T201">человек.[NOM]</ta>
            <ta e="T203" id="Seg_5088" s="T202">такой-ADVZ</ta>
            <ta e="T204" id="Seg_5089" s="T203">вниз</ta>
            <ta e="T205" id="Seg_5090" s="T204">идти-CVB</ta>
            <ta e="T206" id="Seg_5091" s="T205">качаться.[3SG.S]</ta>
            <ta e="T207" id="Seg_5092" s="T206">потом</ta>
            <ta e="T208" id="Seg_5093" s="T207">берег-ILL</ta>
            <ta e="T209" id="Seg_5094" s="T208">вверх</ta>
            <ta e="T210" id="Seg_5095" s="T209">выйти.[3SG.S]</ta>
            <ta e="T211" id="Seg_5096" s="T210">выйти-PST.NAR.[3SG.S]</ta>
            <ta e="T212" id="Seg_5097" s="T211">глаз.[NOM]-3SG</ta>
            <ta e="T213" id="Seg_5098" s="T212">настолько</ta>
            <ta e="T214" id="Seg_5099" s="T213">еда-TRL</ta>
            <ta e="T215" id="Seg_5100" s="T214">умереть-PST.NAR.[3SG.S]</ta>
            <ta e="T216" id="Seg_5101" s="T215">яма-EP-GEN</ta>
            <ta e="T217" id="Seg_5102" s="T216">внутри-ILL</ta>
            <ta e="T218" id="Seg_5103" s="T217">застрять-DRV-PST.NAR.[3SG.S]</ta>
            <ta e="T219" id="Seg_5104" s="T218">один</ta>
            <ta e="T220" id="Seg_5105" s="T219">середина-LOC</ta>
            <ta e="T221" id="Seg_5106" s="T220">такой-ADVZ</ta>
            <ta e="T222" id="Seg_5107" s="T221">быть-PST.[3SG.S]</ta>
            <ta e="T223" id="Seg_5108" s="T222">будто</ta>
            <ta e="T224" id="Seg_5109" s="T223">земля.[NOM]</ta>
            <ta e="T225" id="Seg_5110" s="T224">виднеться-INFER.[3SG.S]</ta>
            <ta e="T226" id="Seg_5111" s="T225">словно</ta>
            <ta e="T227" id="Seg_5112" s="T226">земля.[NOM]</ta>
            <ta e="T228" id="Seg_5113" s="T227">таять-FRQ-EP-INFER.[3SG.S]</ta>
            <ta e="T229" id="Seg_5114" s="T228">море-GEN</ta>
            <ta e="T230" id="Seg_5115" s="T229">берег-LOC</ta>
            <ta e="T231" id="Seg_5116" s="T230">он(а).[NOM]</ta>
            <ta e="T232" id="Seg_5117" s="T231">настолько</ta>
            <ta e="T233" id="Seg_5118" s="T232">еда-TRL</ta>
            <ta e="T234" id="Seg_5119" s="T233">умереть-PST.NAR.[3SG.S]</ta>
            <ta e="T235" id="Seg_5120" s="T234">потом</ta>
            <ta e="T236" id="Seg_5121" s="T235">такой-ADVZ</ta>
            <ta e="T237" id="Seg_5122" s="T236">едва</ta>
            <ta e="T238" id="Seg_5123" s="T237">отправиться-CO.[3SG.S]</ta>
            <ta e="T239" id="Seg_5124" s="T238">нарты-ACC</ta>
            <ta e="T240" id="Seg_5125" s="T239">земля-ADV.LOC</ta>
            <ta e="T241" id="Seg_5126" s="T240">согнуть-3SG.O</ta>
            <ta e="T242" id="Seg_5127" s="T241">прийти-US-PST.NAR.[3SG.S]</ta>
            <ta e="T243" id="Seg_5128" s="T242">потом</ta>
            <ta e="T244" id="Seg_5129" s="T243">нарты-GEN-3SG</ta>
            <ta e="T245" id="Seg_5130" s="T244">спина-EP-GEN</ta>
            <ta e="T246" id="Seg_5131" s="T245">на-PROL</ta>
            <ta e="T247" id="Seg_5132" s="T246">ползти-CVB</ta>
            <ta e="T248" id="Seg_5133" s="T247">вверх</ta>
            <ta e="T249" id="Seg_5134" s="T248">выйти.[3SG.S]</ta>
            <ta e="T250" id="Seg_5135" s="T249">потом</ta>
            <ta e="T251" id="Seg_5136" s="T250">вверх</ta>
            <ta e="T252" id="Seg_5137" s="T251">берег-ILL</ta>
            <ta e="T253" id="Seg_5138" s="T252">попасть.[3SG.S]</ta>
            <ta e="T254" id="Seg_5139" s="T253">потом</ta>
            <ta e="T255" id="Seg_5140" s="T254">там</ta>
            <ta e="T256" id="Seg_5141" s="T255">лежать.[3SG.S]</ta>
            <ta e="T257" id="Seg_5142" s="T256">опять</ta>
            <ta e="T258" id="Seg_5143" s="T257">INDEF3</ta>
            <ta e="T259" id="Seg_5144" s="T258">что.[NOM]</ta>
            <ta e="T260" id="Seg_5145" s="T259">такой-ADVZ</ta>
            <ta e="T261" id="Seg_5146" s="T260">сказать-CO-3SG.O</ta>
            <ta e="T262" id="Seg_5147" s="T261">человек.[NOM]</ta>
            <ta e="T263" id="Seg_5148" s="T262">мышь-COM</ta>
            <ta e="T264" id="Seg_5149" s="T263">идти-CVB</ta>
            <ta e="T265" id="Seg_5150" s="T264">спуститься-CO.[3SG.S]</ta>
            <ta e="T266" id="Seg_5151" s="T265">вверх-ADV.LOC</ta>
            <ta e="T267" id="Seg_5152" s="T266">тальник-ADJZ</ta>
            <ta e="T268" id="Seg_5153" s="T267">остров-GEN</ta>
            <ta e="T269" id="Seg_5154" s="T268">край-EP-ILL</ta>
            <ta e="T270" id="Seg_5155" s="T269">ползти-CVB</ta>
            <ta e="T271" id="Seg_5156" s="T270">вверх</ta>
            <ta e="T272" id="Seg_5157" s="T271">выйти.[3SG.S]</ta>
            <ta e="T273" id="Seg_5158" s="T272">потом</ta>
            <ta e="T274" id="Seg_5159" s="T273">вверх</ta>
            <ta e="T275" id="Seg_5160" s="T274">ползти-CVB</ta>
            <ta e="T276" id="Seg_5161" s="T275">выйти.[3SG.S]</ta>
            <ta e="T277" id="Seg_5162" s="T276">я.NOM</ta>
            <ta e="T278" id="Seg_5163" s="T277">на.спину</ta>
            <ta e="T279" id="Seg_5164" s="T278">вверх</ta>
            <ta e="T280" id="Seg_5165" s="T279">встать-PFV-PST-1SG.S</ta>
            <ta e="T281" id="Seg_5166" s="T280">CONJ</ta>
            <ta e="T282" id="Seg_5167" s="T281">вверх</ta>
            <ta e="T283" id="Seg_5168" s="T282">ползти-CVB</ta>
            <ta e="T284" id="Seg_5169" s="T283">выйти.[3SG.S]</ta>
            <ta e="T285" id="Seg_5170" s="T284">сила.[NOM]-3SG</ta>
            <ta e="T286" id="Seg_5171" s="T285">всегда</ta>
            <ta e="T287" id="Seg_5172" s="T286">NEG.EX.[3SG.S]</ta>
            <ta e="T288" id="Seg_5173" s="T287">опять</ta>
            <ta e="T289" id="Seg_5174" s="T288">INDEF3</ta>
            <ta e="T290" id="Seg_5175" s="T289">что.[NOM]</ta>
            <ta e="T291" id="Seg_5176" s="T290">такой-ADVZ</ta>
            <ta e="T292" id="Seg_5177" s="T291">спросить-INCH-3SG.O</ta>
            <ta e="T293" id="Seg_5178" s="T292">человек.[NOM]</ta>
            <ta e="T294" id="Seg_5179" s="T293">NEG</ta>
            <ta e="T295" id="Seg_5180" s="T294">такой-ADVZ</ta>
            <ta e="T296" id="Seg_5181" s="T295">качаться.[3SG.S]</ta>
            <ta e="T297" id="Seg_5182" s="T296">идти-CVB</ta>
            <ta e="T298" id="Seg_5183" s="T297">этот</ta>
            <ta e="T299" id="Seg_5184" s="T298">море-GEN</ta>
            <ta e="T300" id="Seg_5185" s="T299">рядом-LOC</ta>
            <ta e="T301" id="Seg_5186" s="T300">высокий</ta>
            <ta e="T302" id="Seg_5187" s="T301">земля.[NOM]</ta>
            <ta e="T303" id="Seg_5188" s="T302">вверх</ta>
            <ta e="T304" id="Seg_5189" s="T303">ползти-INCH-CO.[3SG.S]</ta>
            <ta e="T305" id="Seg_5190" s="T304">потом</ta>
            <ta e="T306" id="Seg_5191" s="T305">вверх</ta>
            <ta e="T307" id="Seg_5192" s="T306">ползти-CVB</ta>
            <ta e="T308" id="Seg_5193" s="T307">такой-ADVZ</ta>
            <ta e="T309" id="Seg_5194" s="T308">взглянуть-DUR.[3SG.S]</ta>
            <ta e="T310" id="Seg_5195" s="T309">вверх-ADV.LOC</ta>
            <ta e="T311" id="Seg_5196" s="T310">такой-ADVZ</ta>
            <ta e="T312" id="Seg_5197" s="T311">увидеть-CO-3SG.O</ta>
            <ta e="T313" id="Seg_5198" s="T312">кожаный</ta>
            <ta e="T314" id="Seg_5199" s="T313">чум.[NOM]</ta>
            <ta e="T315" id="Seg_5200" s="T314">стоять-INFER.[3SG.S]</ta>
            <ta e="T316" id="Seg_5201" s="T315">потом</ta>
            <ta e="T317" id="Seg_5202" s="T316">опять</ta>
            <ta e="T318" id="Seg_5203" s="T317">вниз</ta>
            <ta e="T319" id="Seg_5204" s="T318">упасть-CO.[3SG.S]</ta>
            <ta e="T320" id="Seg_5205" s="T319">как</ta>
            <ta e="T321" id="Seg_5206" s="T320">туда</ta>
            <ta e="T322" id="Seg_5207" s="T321">прийти-US-IPFV-FUT-1SG.S</ta>
            <ta e="T323" id="Seg_5208" s="T322">потом</ta>
            <ta e="T324" id="Seg_5209" s="T323">этот</ta>
            <ta e="T325" id="Seg_5210" s="T324">чум-ILL</ta>
            <ta e="T326" id="Seg_5211" s="T325">вверх</ta>
            <ta e="T327" id="Seg_5212" s="T326">встать-CVB</ta>
            <ta e="T328" id="Seg_5213" s="T327">вниз</ta>
            <ta e="T329" id="Seg_5214" s="T328">прийти-CO.[3SG.S]</ta>
            <ta e="T330" id="Seg_5215" s="T329">ненец.[NOM]</ta>
            <ta e="T331" id="Seg_5216" s="T330">чум.[NOM]</ta>
            <ta e="T332" id="Seg_5217" s="T331">стоять-INFER.[3SG.S]</ta>
            <ta e="T333" id="Seg_5218" s="T332">олень.[NOM]-3SG</ta>
            <ta e="T334" id="Seg_5219" s="T333">улица-LOC</ta>
            <ta e="T335" id="Seg_5220" s="T334">идти-INCH-PST.NAR-INFER-3PL</ta>
            <ta e="T336" id="Seg_5221" s="T335">потом</ta>
            <ta e="T337" id="Seg_5222" s="T336">чум-ILL</ta>
            <ta e="T338" id="Seg_5223" s="T337">войти-CO.[3SG.S]</ta>
            <ta e="T339" id="Seg_5224" s="T338">старик.[NOM]</ta>
            <ta e="T340" id="Seg_5225" s="T339">один</ta>
            <ta e="T341" id="Seg_5226" s="T340">дочь.[NOM]-3SG</ta>
            <ta e="T342" id="Seg_5227" s="T341">один</ta>
            <ta e="T343" id="Seg_5228" s="T342">середина-LOC</ta>
            <ta e="T521" id="Seg_5229" s="T343">ненец.[NOM]</ta>
            <ta e="T344" id="Seg_5230" s="T521">старик.[NOM]</ta>
            <ta e="T345" id="Seg_5231" s="T344">такой-ADVZ</ta>
            <ta e="T346" id="Seg_5232" s="T345">сказать.[3SG.S]</ta>
            <ta e="T347" id="Seg_5233" s="T346">ты.NOM</ta>
            <ta e="T348" id="Seg_5234" s="T347">этот</ta>
            <ta e="T349" id="Seg_5235" s="T348">человек.[NOM]</ta>
            <ta e="T350" id="Seg_5236" s="T349">еда-TR-IMP.2SG.O</ta>
            <ta e="T351" id="Seg_5237" s="T350">он(а).[NOM]</ta>
            <ta e="T352" id="Seg_5238" s="T351">этот</ta>
            <ta e="T353" id="Seg_5239" s="T352">старик-GEN</ta>
            <ta e="T354" id="Seg_5240" s="T353">нечто-LOC</ta>
            <ta e="T355" id="Seg_5241" s="T354">два</ta>
            <ta e="T356" id="Seg_5242" s="T355">сто</ta>
            <ta e="T357" id="Seg_5243" s="T356">олень-PL.[NOM]</ta>
            <ta e="T358" id="Seg_5244" s="T357">я.GEN</ta>
            <ta e="T359" id="Seg_5245" s="T358">один</ta>
            <ta e="T360" id="Seg_5246" s="T359">сто</ta>
            <ta e="T361" id="Seg_5247" s="T360">олень.[NOM]-1SG</ta>
            <ta e="T362" id="Seg_5248" s="T361">зверь.[NOM]</ta>
            <ta e="T363" id="Seg_5249" s="T362">всё</ta>
            <ta e="T364" id="Seg_5250" s="T363">съесть-CO-3SG.O</ta>
            <ta e="T365" id="Seg_5251" s="T364">старик.[NOM]</ta>
            <ta e="T366" id="Seg_5252" s="T365">такой-ADVZ</ta>
            <ta e="T367" id="Seg_5253" s="T366">сказать-CO-3SG.O</ta>
            <ta e="T368" id="Seg_5254" s="T367">ты.NOM</ta>
            <ta e="T369" id="Seg_5255" s="T368">наружу</ta>
            <ta e="T370" id="Seg_5256" s="T369">выйти-CVB</ta>
            <ta e="T371" id="Seg_5257" s="T370">я.GEN</ta>
            <ta e="T372" id="Seg_5258" s="T371">олень.[NOM]-1SG</ta>
            <ta e="T373" id="Seg_5259" s="T372">караулить-IMP.2SG.O</ta>
            <ta e="T374" id="Seg_5260" s="T373">темнота-VBLZ-EP-DRV-CO.[3SG.S]</ta>
            <ta e="T375" id="Seg_5261" s="T374">он(а).[NOM]</ta>
            <ta e="T376" id="Seg_5262" s="T375">наружу</ta>
            <ta e="T377" id="Seg_5263" s="T376">выйти.[3SG.S]</ta>
            <ta e="T378" id="Seg_5264" s="T377">он(а).[NOM]</ta>
            <ta e="T379" id="Seg_5265" s="T378">наружу</ta>
            <ta e="T380" id="Seg_5266" s="T379">выйти.[3SG.S]</ta>
            <ta e="T381" id="Seg_5267" s="T380">настолько</ta>
            <ta e="T382" id="Seg_5268" s="T381">медлить-DUR.[3SG.S]</ta>
            <ta e="T383" id="Seg_5269" s="T382">человек.[NOM]</ta>
            <ta e="T384" id="Seg_5270" s="T383">что.[NOM]-3SG</ta>
            <ta e="T385" id="Seg_5271" s="T384">взглянуть-DUR-FUT-3SG.O</ta>
            <ta e="T386" id="Seg_5272" s="T385">потом</ta>
            <ta e="T387" id="Seg_5273" s="T386">он(а).[NOM]</ta>
            <ta e="T388" id="Seg_5274" s="T387">опять</ta>
            <ta e="T389" id="Seg_5275" s="T388">вниз</ta>
            <ta e="T390" id="Seg_5276" s="T389">упасть-PST.NAR.[3SG.S]</ta>
            <ta e="T391" id="Seg_5277" s="T390">там</ta>
            <ta e="T392" id="Seg_5278" s="T391">INFER</ta>
            <ta e="T393" id="Seg_5279" s="T392">лежать-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T394" id="Seg_5280" s="T393">INFER</ta>
            <ta e="T395" id="Seg_5281" s="T394">он(а)-EP-GEN</ta>
            <ta e="T396" id="Seg_5282" s="T395">прийти-CVB</ta>
            <ta e="T397" id="Seg_5283" s="T396">после</ta>
            <ta e="T398" id="Seg_5284" s="T397">зверь.[NOM]</ta>
            <ta e="T399" id="Seg_5285" s="T398">INFER</ta>
            <ta e="T400" id="Seg_5286" s="T399">уйти-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T401" id="Seg_5287" s="T400">туда</ta>
            <ta e="T402" id="Seg_5288" s="T401">раньше</ta>
            <ta e="T403" id="Seg_5289" s="T402">отец-DYA-DU.[NOM]</ta>
            <ta e="T404" id="Seg_5290" s="T403">олень-OBL.3SG-ADJZ</ta>
            <ta e="T405" id="Seg_5291" s="T404">караулить-CVB</ta>
            <ta e="T406" id="Seg_5292" s="T405">пространство.снаружи-LOC</ta>
            <ta e="T407" id="Seg_5293" s="T406">жить-PST.NAR-3DU.S</ta>
            <ta e="T408" id="Seg_5294" s="T407">потом</ta>
            <ta e="T409" id="Seg_5295" s="T408">этот</ta>
            <ta e="T410" id="Seg_5296" s="T409">человек-EP-ACC</ta>
            <ta e="T411" id="Seg_5297" s="T410">еда-TR-DUR-CVB</ta>
            <ta e="T412" id="Seg_5298" s="T411">там</ta>
            <ta e="T413" id="Seg_5299" s="T412">INFER</ta>
            <ta e="T414" id="Seg_5300" s="T413">жить-INCH-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T415" id="Seg_5301" s="T414">потом</ta>
            <ta e="T416" id="Seg_5302" s="T415">этот</ta>
            <ta e="T417" id="Seg_5303" s="T416">старик.[NOM]</ta>
            <ta e="T418" id="Seg_5304" s="T417">опять</ta>
            <ta e="T419" id="Seg_5305" s="T418">такой-ADVZ</ta>
            <ta e="T420" id="Seg_5306" s="T419">сказать-CO-3SG.O</ta>
            <ta e="T421" id="Seg_5307" s="T420">опять</ta>
            <ta e="T422" id="Seg_5308" s="T421">ходить-IMP.2SG.S</ta>
            <ta e="T423" id="Seg_5309" s="T422">олень-ACC-3SG</ta>
            <ta e="T424" id="Seg_5310" s="T423">караулить-CVB</ta>
            <ta e="T425" id="Seg_5311" s="T424">олень.[NOM]-3SG</ta>
            <ta e="T426" id="Seg_5312" s="T425">вроде</ta>
            <ta e="T427" id="Seg_5313" s="T426">лес-ILL</ta>
            <ta e="T428" id="Seg_5314" s="T427">NEG</ta>
            <ta e="T429" id="Seg_5315" s="T428">уйти-FUT.[3SG.S]</ta>
            <ta e="T430" id="Seg_5316" s="T429">чум-EP-GEN</ta>
            <ta e="T431" id="Seg_5317" s="T430">рядом-LOC</ta>
            <ta e="T432" id="Seg_5318" s="T431">спать-3PL</ta>
            <ta e="T433" id="Seg_5319" s="T432">потом</ta>
            <ta e="T434" id="Seg_5320" s="T433">он(а).[NOM]</ta>
            <ta e="T435" id="Seg_5321" s="T434">наружу</ta>
            <ta e="T436" id="Seg_5322" s="T435">выйти.[3SG.S]</ta>
            <ta e="T437" id="Seg_5323" s="T436">достаточно-ADJZ</ta>
            <ta e="T438" id="Seg_5324" s="T437">нечто-ILL</ta>
            <ta e="T439" id="Seg_5325" s="T438">уйти-CVB</ta>
            <ta e="T440" id="Seg_5326" s="T439">опять</ta>
            <ta e="T441" id="Seg_5327" s="T440">вниз</ta>
            <ta e="T442" id="Seg_5328" s="T441">упасть.[3SG.S]</ta>
            <ta e="T443" id="Seg_5329" s="T442">потом</ta>
            <ta e="T444" id="Seg_5330" s="T443">он(а).[NOM]</ta>
            <ta e="T445" id="Seg_5331" s="T444">один</ta>
            <ta e="T446" id="Seg_5332" s="T445">середина-LOC</ta>
            <ta e="T447" id="Seg_5333" s="T446">такой-ADVZ</ta>
            <ta e="T448" id="Seg_5334" s="T447">проснуться-CO.[3SG.S]</ta>
            <ta e="T449" id="Seg_5335" s="T448">что.[NOM]</ta>
            <ta e="T450" id="Seg_5336" s="T449">уйти-TR-3SG.O</ta>
            <ta e="T451" id="Seg_5337" s="T450">видать</ta>
            <ta e="T452" id="Seg_5338" s="T451">жить-ATTEN.[3SG.S]</ta>
            <ta e="T453" id="Seg_5339" s="T452">потом</ta>
            <ta e="T454" id="Seg_5340" s="T453">домой</ta>
            <ta e="T455" id="Seg_5341" s="T454">прийти-CO.[3SG.S]</ta>
            <ta e="T456" id="Seg_5342" s="T455">чум-ILL</ta>
            <ta e="T522" id="Seg_5343" s="T456">ненец.[NOM]</ta>
            <ta e="T457" id="Seg_5344" s="T522">старик.[NOM]</ta>
            <ta e="T458" id="Seg_5345" s="T457">спать.[3SG.S]</ta>
            <ta e="T459" id="Seg_5346" s="T458">потом</ta>
            <ta e="T460" id="Seg_5347" s="T459">он(а)-EP.[NOM]-3PL</ta>
            <ta e="T461" id="Seg_5348" s="T460">олень.[NOM]</ta>
            <ta e="T462" id="Seg_5349" s="T461">всегда</ta>
            <ta e="T463" id="Seg_5350" s="T462">улица-LOC</ta>
            <ta e="T464" id="Seg_5351" s="T463">спать-3PL</ta>
            <ta e="T465" id="Seg_5352" s="T464">он(а).[NOM]</ta>
            <ta e="T466" id="Seg_5353" s="T465">этот</ta>
            <ta e="T467" id="Seg_5354" s="T466">прийти-PTCP.PST</ta>
            <ta e="T468" id="Seg_5355" s="T467">одежда-ADJZ</ta>
            <ta e="T469" id="Seg_5356" s="T468">обувь-PL-EP-COM</ta>
            <ta e="T470" id="Seg_5357" s="T469">жить.[3SG.S]</ta>
            <ta e="T471" id="Seg_5358" s="T470">потом</ta>
            <ta e="T472" id="Seg_5359" s="T471">три-EP-ORD</ta>
            <ta e="T473" id="Seg_5360" s="T472">ночь-ADV.LOC</ta>
            <ta e="T474" id="Seg_5361" s="T473">такой-ADVZ</ta>
            <ta e="T475" id="Seg_5362" s="T474">стать.[3SG.S]</ta>
            <ta e="T476" id="Seg_5363" s="T475">будто</ta>
            <ta e="T477" id="Seg_5364" s="T476">мол</ta>
            <ta e="T478" id="Seg_5365" s="T477">хороший-ADVZ</ta>
            <ta e="T479" id="Seg_5366" s="T478">стать-1SG.S</ta>
            <ta e="T523" id="Seg_5367" s="T479">ненец.[NOM]</ta>
            <ta e="T480" id="Seg_5368" s="T523">старик.[NOM]</ta>
            <ta e="T481" id="Seg_5369" s="T480">олень.[NOM]-3SG</ta>
            <ta e="T482" id="Seg_5370" s="T481">лес-ILL</ta>
            <ta e="T483" id="Seg_5371" s="T482">NEG</ta>
            <ta e="T484" id="Seg_5372" s="T483">уйти-FUT.[3SG.S]</ta>
            <ta e="T485" id="Seg_5373" s="T484">чум-EP-GEN</ta>
            <ta e="T486" id="Seg_5374" s="T485">рядом-LOC</ta>
            <ta e="T487" id="Seg_5375" s="T486">там</ta>
            <ta e="T488" id="Seg_5376" s="T487">уйти-INFER-3PL</ta>
            <ta e="T489" id="Seg_5377" s="T488">он(а).[NOM]</ta>
            <ta e="T490" id="Seg_5378" s="T489">прийти-CVB</ta>
            <ta e="T491" id="Seg_5379" s="T490">опять</ta>
            <ta e="T492" id="Seg_5380" s="T491">вниз</ta>
            <ta e="T493" id="Seg_5381" s="T492">спать-CO.[3SG.S]</ta>
            <ta e="T494" id="Seg_5382" s="T493">следующий</ta>
            <ta e="T495" id="Seg_5383" s="T494">утро-ADV.LOC</ta>
            <ta e="T496" id="Seg_5384" s="T495">видать</ta>
            <ta e="T497" id="Seg_5385" s="T496">снег.[NOM]</ta>
            <ta e="T498" id="Seg_5386" s="T497">положить-PFV-INFER-3SG.O</ta>
            <ta e="T499" id="Seg_5387" s="T498">я.NOM</ta>
            <ta e="T500" id="Seg_5388" s="T499">он(а).[NOM]</ta>
            <ta e="T501" id="Seg_5389" s="T500">как</ta>
            <ta e="T502" id="Seg_5390" s="T501">прийти-PST.[3SG.S]</ta>
            <ta e="T503" id="Seg_5391" s="T502">олень.[NOM]-1SG</ta>
            <ta e="T504" id="Seg_5392" s="T503">лес-ILL</ta>
            <ta e="T505" id="Seg_5393" s="T504">NEG</ta>
            <ta e="T506" id="Seg_5394" s="T505">уйти-FUT.[3SG.S]</ta>
            <ta e="T507" id="Seg_5395" s="T506">повернуть-TR-3SG.O</ta>
            <ta e="T508" id="Seg_5396" s="T507">снег-ACC</ta>
            <ta e="T509" id="Seg_5397" s="T508">положить-CVB</ta>
            <ta e="T510" id="Seg_5398" s="T509">отправиться-EP-CO.[3SG.S]</ta>
            <ta e="T511" id="Seg_5399" s="T510">хороший-ADVZ</ta>
            <ta e="T512" id="Seg_5400" s="T511">вниз</ta>
            <ta e="T513" id="Seg_5401" s="T512">INFER</ta>
            <ta e="T514" id="Seg_5402" s="T513">отправиться-INFER.[3SG.S]</ta>
            <ta e="T515" id="Seg_5403" s="T514">он(а).[NOM]</ta>
            <ta e="T516" id="Seg_5404" s="T515">затем</ta>
            <ta e="T517" id="Seg_5405" s="T516">хороший-ADVZ</ta>
            <ta e="T518" id="Seg_5406" s="T517">стать.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_5407" s="T0">dem</ta>
            <ta e="T96" id="Seg_5408" s="T1">n-n:case</ta>
            <ta e="T2" id="Seg_5409" s="T96">n-n:case</ta>
            <ta e="T3" id="Seg_5410" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_5411" s="T3">dem-adj&gt;adv</ta>
            <ta e="T5" id="Seg_5412" s="T4">v-v:ins-v:pn</ta>
            <ta e="T6" id="Seg_5413" s="T5">pers</ta>
            <ta e="T7" id="Seg_5414" s="T6">dem</ta>
            <ta e="T8" id="Seg_5415" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_5416" s="T8">adj</ta>
            <ta e="T10" id="Seg_5417" s="T9">n-n:num-n:case</ta>
            <ta e="T11" id="Seg_5418" s="T10">v-v:tense-v:pn</ta>
            <ta e="T519" id="Seg_5419" s="T11">n-n:case</ta>
            <ta e="T12" id="Seg_5420" s="T519">n-n:case</ta>
            <ta e="T13" id="Seg_5421" s="T12">dem-adj&gt;adv</ta>
            <ta e="T14" id="Seg_5422" s="T13">v-v:ins-v:pn</ta>
            <ta e="T15" id="Seg_5423" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_5424" s="T15">ptcl</ta>
            <ta e="T17" id="Seg_5425" s="T16">n-n:ins-n:num-n:case</ta>
            <ta e="T18" id="Seg_5426" s="T17">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_5427" s="T18">n-n:case</ta>
            <ta e="T20" id="Seg_5428" s="T19">pers</ta>
            <ta e="T21" id="Seg_5429" s="T20">interrog</ta>
            <ta e="T22" id="Seg_5430" s="T21">v-v:ins-v:pn</ta>
            <ta e="T23" id="Seg_5431" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_5432" s="T23">pp</ta>
            <ta e="T25" id="Seg_5433" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_5434" s="T25">n-n&gt;n-n&gt;adj</ta>
            <ta e="T27" id="Seg_5435" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_5436" s="T27">v-v:pn</ta>
            <ta e="T520" id="Seg_5437" s="T28">n-n:case</ta>
            <ta e="T29" id="Seg_5438" s="T520">n-n:case</ta>
            <ta e="T30" id="Seg_5439" s="T29">pers-n:ins-n:case</ta>
            <ta e="T31" id="Seg_5440" s="T30">num</ta>
            <ta e="T32" id="Seg_5441" s="T31">n-n&gt;n-n:num-n:case</ta>
            <ta e="T33" id="Seg_5442" s="T32">n-n:case</ta>
            <ta e="T34" id="Seg_5443" s="T33">pp</ta>
            <ta e="T35" id="Seg_5444" s="T34">dem</ta>
            <ta e="T36" id="Seg_5445" s="T35">adj</ta>
            <ta e="T37" id="Seg_5446" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_5447" s="T37">v-v:inf</ta>
            <ta e="T39" id="Seg_5448" s="T38">v-v:ins-v:pn</ta>
            <ta e="T40" id="Seg_5449" s="T39">pers-n:ins-n:num-n:case</ta>
            <ta e="T41" id="Seg_5450" s="T40">v-v:ins-v:pn</ta>
            <ta e="T42" id="Seg_5451" s="T41">dem</ta>
            <ta e="T43" id="Seg_5452" s="T42">adj</ta>
            <ta e="T44" id="Seg_5453" s="T43">n-n:case</ta>
            <ta e="T45" id="Seg_5454" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_5455" s="T45">v-v:ins-v:pn</ta>
            <ta e="T47" id="Seg_5456" s="T46">adv</ta>
            <ta e="T48" id="Seg_5457" s="T47">adj</ta>
            <ta e="T49" id="Seg_5458" s="T48">n-n:case</ta>
            <ta e="T51" id="Seg_5459" s="T50">dem</ta>
            <ta e="T52" id="Seg_5460" s="T51">adj</ta>
            <ta e="T53" id="Seg_5461" s="T52">n-n:case</ta>
            <ta e="T54" id="Seg_5462" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_5463" s="T54">adj</ta>
            <ta e="T56" id="Seg_5464" s="T55">v-v:ins-v:pn</ta>
            <ta e="T57" id="Seg_5465" s="T56">adv</ta>
            <ta e="T58" id="Seg_5466" s="T57">pers-n:ins-n:num-n:case</ta>
            <ta e="T59" id="Seg_5467" s="T58">v-v&gt;adv</ta>
            <ta e="T60" id="Seg_5468" s="T59">n-n:case-n:poss</ta>
            <ta e="T61" id="Seg_5469" s="T60">n-n:ins-n:num-n:case</ta>
            <ta e="T62" id="Seg_5470" s="T61">preverb</ta>
            <ta e="T63" id="Seg_5471" s="T62">v-v:pn</ta>
            <ta e="T64" id="Seg_5472" s="T63">conj</ta>
            <ta e="T65" id="Seg_5473" s="T64">n-n:case-n:obl.poss</ta>
            <ta e="T66" id="Seg_5474" s="T65">preverb</ta>
            <ta e="T67" id="Seg_5475" s="T66">v-v:pn</ta>
            <ta e="T68" id="Seg_5476" s="T67">adv</ta>
            <ta e="T69" id="Seg_5477" s="T68">dem</ta>
            <ta e="T70" id="Seg_5478" s="T69">num</ta>
            <ta e="T71" id="Seg_5479" s="T70">n-n:num-n:case</ta>
            <ta e="T72" id="Seg_5480" s="T71">preverb</ta>
            <ta e="T73" id="Seg_5481" s="T72">n-n:case-n:obl.poss</ta>
            <ta e="T74" id="Seg_5482" s="T73">v-v:pn</ta>
            <ta e="T75" id="Seg_5483" s="T74">dem</ta>
            <ta e="T76" id="Seg_5484" s="T75">n-n&gt;n-n:num-n:case</ta>
            <ta e="T77" id="Seg_5485" s="T76">adv</ta>
            <ta e="T78" id="Seg_5486" s="T77">v-v:pn</ta>
            <ta e="T79" id="Seg_5487" s="T78">dem</ta>
            <ta e="T80" id="Seg_5488" s="T79">n-n:case-n&gt;adj</ta>
            <ta e="T81" id="Seg_5489" s="T80">adj</ta>
            <ta e="T82" id="Seg_5490" s="T81">n-n:case</ta>
            <ta e="T83" id="Seg_5491" s="T82">v-v&gt;ptcp</ta>
            <ta e="T84" id="Seg_5492" s="T83">n-n:case-n:poss</ta>
            <ta e="T85" id="Seg_5493" s="T84">n-n:case</ta>
            <ta e="T86" id="Seg_5494" s="T85">adv</ta>
            <ta e="T87" id="Seg_5495" s="T86">v-v:tense-v:pn</ta>
            <ta e="T88" id="Seg_5496" s="T87">adv</ta>
            <ta e="T89" id="Seg_5497" s="T88">dem</ta>
            <ta e="T90" id="Seg_5498" s="T89">n-n:case</ta>
            <ta e="T91" id="Seg_5499" s="T90">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T92" id="Seg_5500" s="T91">adv</ta>
            <ta e="T93" id="Seg_5501" s="T92">v-v&gt;ptcp</ta>
            <ta e="T94" id="Seg_5502" s="T93">n-n:case</ta>
            <ta e="T95" id="Seg_5503" s="T94">v-v:tense-v:pn</ta>
            <ta e="T97" id="Seg_5504" s="T95">n-n&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T98" id="Seg_5505" s="T97">pers-n:case</ta>
            <ta e="T99" id="Seg_5506" s="T98">adv</ta>
            <ta e="T100" id="Seg_5507" s="T99">adv</ta>
            <ta e="T101" id="Seg_5508" s="T100">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T102" id="Seg_5509" s="T101">pers-n:case</ta>
            <ta e="T103" id="Seg_5510" s="T102">ptcl</ta>
            <ta e="T104" id="Seg_5511" s="T103">n-n:case</ta>
            <ta e="T105" id="Seg_5512" s="T104">n-n:case</ta>
            <ta e="T106" id="Seg_5513" s="T105">adv</ta>
            <ta e="T107" id="Seg_5514" s="T106">ptcl</ta>
            <ta e="T108" id="Seg_5515" s="T107">v-v:tense-v:mood-v:pn</ta>
            <ta e="T109" id="Seg_5516" s="T108">pers-n:case</ta>
            <ta e="T110" id="Seg_5517" s="T109">adv</ta>
            <ta e="T111" id="Seg_5518" s="T110">v-v:tense-v:pn</ta>
            <ta e="T112" id="Seg_5519" s="T111">adv</ta>
            <ta e="T113" id="Seg_5520" s="T112">adv-adv:case</ta>
            <ta e="T114" id="Seg_5521" s="T113">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T115" id="Seg_5522" s="T114">adv</ta>
            <ta e="T116" id="Seg_5523" s="T115">v-v:tense-v:pn</ta>
            <ta e="T117" id="Seg_5524" s="T116">n-n:case-n:poss</ta>
            <ta e="T118" id="Seg_5525" s="T117">n-n:case-n:poss</ta>
            <ta e="T119" id="Seg_5526" s="T118">ptcl</ta>
            <ta e="T120" id="Seg_5527" s="T119">v-v:pn</ta>
            <ta e="T121" id="Seg_5528" s="T120">adv</ta>
            <ta e="T122" id="Seg_5529" s="T121">adv</ta>
            <ta e="T123" id="Seg_5530" s="T122">v-v&gt;v-v:pn</ta>
            <ta e="T124" id="Seg_5531" s="T123">n-n:case-n:poss</ta>
            <ta e="T125" id="Seg_5532" s="T124">n-n:case</ta>
            <ta e="T126" id="Seg_5533" s="T125">v-v:ins-v:pn</ta>
            <ta e="T127" id="Seg_5534" s="T126">ptcl</ta>
            <ta e="T128" id="Seg_5535" s="T127">num</ta>
            <ta e="T129" id="Seg_5536" s="T128">n-n:case-n:poss</ta>
            <ta e="T130" id="Seg_5537" s="T129">adv</ta>
            <ta e="T131" id="Seg_5538" s="T130">v-v:pn</ta>
            <ta e="T132" id="Seg_5539" s="T131">pers-n:case</ta>
            <ta e="T133" id="Seg_5540" s="T132">emphpro-n:case</ta>
            <ta e="T134" id="Seg_5541" s="T133">adv</ta>
            <ta e="T135" id="Seg_5542" s="T134">v-v:tense-v:pn</ta>
            <ta e="T136" id="Seg_5543" s="T135">adj-n&gt;adj</ta>
            <ta e="T137" id="Seg_5544" s="T136">v-v&gt;v-v:pn</ta>
            <ta e="T138" id="Seg_5545" s="T137">adv</ta>
            <ta e="T139" id="Seg_5546" s="T138">v-v:tense-v:pn</ta>
            <ta e="T140" id="Seg_5547" s="T139">adv</ta>
            <ta e="T141" id="Seg_5548" s="T140">n-n:case</ta>
            <ta e="T142" id="Seg_5549" s="T141">v-v:tense-v:pn</ta>
            <ta e="T143" id="Seg_5550" s="T142">n-n:case</ta>
            <ta e="T144" id="Seg_5551" s="T143">v-v:tense-v:pn</ta>
            <ta e="T145" id="Seg_5552" s="T144">n-n:case-n:poss</ta>
            <ta e="T146" id="Seg_5553" s="T145">pp-n:case</ta>
            <ta e="T147" id="Seg_5554" s="T146">v-v&gt;v-v:pn</ta>
            <ta e="T148" id="Seg_5555" s="T147">adv-adv&gt;adj</ta>
            <ta e="T149" id="Seg_5556" s="T148">n-n:case-n:poss</ta>
            <ta e="T150" id="Seg_5557" s="T149">n-n:case</ta>
            <ta e="T151" id="Seg_5558" s="T150">n-n:case-n:poss</ta>
            <ta e="T152" id="Seg_5559" s="T151">pp-n:case</ta>
            <ta e="T153" id="Seg_5560" s="T152">v-v:tense-v:mood-v:pn</ta>
            <ta e="T154" id="Seg_5561" s="T153">n-n:case</ta>
            <ta e="T155" id="Seg_5562" s="T154">quant</ta>
            <ta e="T156" id="Seg_5563" s="T155">v-v:tense-v:pn</ta>
            <ta e="T157" id="Seg_5564" s="T156">num</ta>
            <ta e="T158" id="Seg_5565" s="T157">num</ta>
            <ta e="T159" id="Seg_5566" s="T158">n-n:case</ta>
            <ta e="T160" id="Seg_5567" s="T159">pers-n:case</ta>
            <ta e="T161" id="Seg_5568" s="T160">clit</ta>
            <ta e="T162" id="Seg_5569" s="T161">interrog-n:case</ta>
            <ta e="T163" id="Seg_5570" s="T162">dem-adj&gt;adv</ta>
            <ta e="T164" id="Seg_5571" s="T163">v-v:ins-v:pn</ta>
            <ta e="T165" id="Seg_5572" s="T164">conj-n:case</ta>
            <ta e="T166" id="Seg_5573" s="T165">n-n:case</ta>
            <ta e="T167" id="Seg_5574" s="T166">ptcl</ta>
            <ta e="T168" id="Seg_5575" s="T167">conj-n:case</ta>
            <ta e="T169" id="Seg_5576" s="T168">n-n:case</ta>
            <ta e="T170" id="Seg_5577" s="T169">n-n:case</ta>
            <ta e="T171" id="Seg_5578" s="T170">v-v&gt;adv</ta>
            <ta e="T172" id="Seg_5579" s="T171">conj</ta>
            <ta e="T173" id="Seg_5580" s="T172">dem-adj&gt;adv</ta>
            <ta e="T174" id="Seg_5581" s="T173">v-v:pn</ta>
            <ta e="T175" id="Seg_5582" s="T174">pers-n:case</ta>
            <ta e="T176" id="Seg_5583" s="T175">preverb</ta>
            <ta e="T177" id="Seg_5584" s="T176">ptcl</ta>
            <ta e="T178" id="Seg_5585" s="T177">v-v:tense-v:pn</ta>
            <ta e="T179" id="Seg_5586" s="T178">dem-adj&gt;adv</ta>
            <ta e="T180" id="Seg_5587" s="T179">v-v:pn</ta>
            <ta e="T181" id="Seg_5588" s="T180">adv</ta>
            <ta e="T182" id="Seg_5589" s="T181">n-n:case</ta>
            <ta e="T183" id="Seg_5590" s="T182">n-n:case</ta>
            <ta e="T184" id="Seg_5591" s="T183">v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T185" id="Seg_5592" s="T184">dem</ta>
            <ta e="T186" id="Seg_5593" s="T185">n-n:case-n:poss</ta>
            <ta e="T187" id="Seg_5594" s="T186">pp-n:case-pp&gt;adj</ta>
            <ta e="T188" id="Seg_5595" s="T187">n-n&gt;adj</ta>
            <ta e="T189" id="Seg_5596" s="T188">n-n:case</ta>
            <ta e="T190" id="Seg_5597" s="T189">v-v&gt;v-v&gt;adv</ta>
            <ta e="T191" id="Seg_5598" s="T190">adv</ta>
            <ta e="T192" id="Seg_5599" s="T191">conj</ta>
            <ta e="T193" id="Seg_5600" s="T192">preverb</ta>
            <ta e="T194" id="Seg_5601" s="T193">v-v:pn</ta>
            <ta e="T195" id="Seg_5602" s="T194">conj</ta>
            <ta e="T196" id="Seg_5603" s="T195">pers-n:case</ta>
            <ta e="T197" id="Seg_5604" s="T196">clit</ta>
            <ta e="T198" id="Seg_5605" s="T197">interrog-n:case</ta>
            <ta e="T199" id="Seg_5606" s="T198">dem-adj&gt;adv</ta>
            <ta e="T200" id="Seg_5607" s="T199">pers</ta>
            <ta e="T201" id="Seg_5608" s="T200">v-v:tense.mood-v:pn</ta>
            <ta e="T202" id="Seg_5609" s="T201">n-n:case</ta>
            <ta e="T203" id="Seg_5610" s="T202">dem-adj&gt;adv</ta>
            <ta e="T204" id="Seg_5611" s="T203">adv</ta>
            <ta e="T205" id="Seg_5612" s="T204">v-v&gt;adv</ta>
            <ta e="T206" id="Seg_5613" s="T205">v-v:pn</ta>
            <ta e="T207" id="Seg_5614" s="T206">adv</ta>
            <ta e="T208" id="Seg_5615" s="T207">n-n:case</ta>
            <ta e="T209" id="Seg_5616" s="T208">adv</ta>
            <ta e="T210" id="Seg_5617" s="T209">v-v:pn</ta>
            <ta e="T211" id="Seg_5618" s="T210">v-v:tense-v:pn</ta>
            <ta e="T212" id="Seg_5619" s="T211">n-n:case-n:poss</ta>
            <ta e="T213" id="Seg_5620" s="T212">adv</ta>
            <ta e="T214" id="Seg_5621" s="T213">n-n:case</ta>
            <ta e="T215" id="Seg_5622" s="T214">v-v:tense-v:pn</ta>
            <ta e="T216" id="Seg_5623" s="T215">n-n:ins-n:case</ta>
            <ta e="T217" id="Seg_5624" s="T216">pp-n:case</ta>
            <ta e="T218" id="Seg_5625" s="T217">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T219" id="Seg_5626" s="T218">num</ta>
            <ta e="T220" id="Seg_5627" s="T219">n-n:case</ta>
            <ta e="T221" id="Seg_5628" s="T220">dem-adj&gt;adv</ta>
            <ta e="T222" id="Seg_5629" s="T221">v-v:tense-v:pn</ta>
            <ta e="T223" id="Seg_5630" s="T222">ptcl</ta>
            <ta e="T224" id="Seg_5631" s="T223">n-n:case</ta>
            <ta e="T225" id="Seg_5632" s="T224">v-v:tense.mood-v:pn</ta>
            <ta e="T226" id="Seg_5633" s="T225">conj</ta>
            <ta e="T227" id="Seg_5634" s="T226">n-n:case</ta>
            <ta e="T228" id="Seg_5635" s="T227">v-v&gt;v-v:ins-v:tense.mood-v:pn</ta>
            <ta e="T229" id="Seg_5636" s="T228">n-n:case</ta>
            <ta e="T230" id="Seg_5637" s="T229">n-n:case</ta>
            <ta e="T231" id="Seg_5638" s="T230">pers-n:case</ta>
            <ta e="T232" id="Seg_5639" s="T231">adv</ta>
            <ta e="T233" id="Seg_5640" s="T232">n-n:case</ta>
            <ta e="T234" id="Seg_5641" s="T233">v-v:tense-v:pn</ta>
            <ta e="T235" id="Seg_5642" s="T234">adv</ta>
            <ta e="T236" id="Seg_5643" s="T235">dem-adj&gt;adv</ta>
            <ta e="T237" id="Seg_5644" s="T236">ptcl</ta>
            <ta e="T238" id="Seg_5645" s="T237">v-v:ins-v:pn</ta>
            <ta e="T239" id="Seg_5646" s="T238">n-n:case</ta>
            <ta e="T240" id="Seg_5647" s="T239">n-n&gt;adv</ta>
            <ta e="T241" id="Seg_5648" s="T240">v-v:pn</ta>
            <ta e="T242" id="Seg_5649" s="T241">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T243" id="Seg_5650" s="T242">adv</ta>
            <ta e="T244" id="Seg_5651" s="T243">n-n:case-n:poss</ta>
            <ta e="T245" id="Seg_5652" s="T244">n-n:ins-n:case</ta>
            <ta e="T246" id="Seg_5653" s="T245">pp-n:case</ta>
            <ta e="T247" id="Seg_5654" s="T246">v-v&gt;adv</ta>
            <ta e="T248" id="Seg_5655" s="T247">adv</ta>
            <ta e="T249" id="Seg_5656" s="T248">v-v:pn</ta>
            <ta e="T250" id="Seg_5657" s="T249">adv</ta>
            <ta e="T251" id="Seg_5658" s="T250">adv</ta>
            <ta e="T252" id="Seg_5659" s="T251">n-n:case</ta>
            <ta e="T253" id="Seg_5660" s="T252">v-v:pn</ta>
            <ta e="T254" id="Seg_5661" s="T253">adv</ta>
            <ta e="T255" id="Seg_5662" s="T254">adv</ta>
            <ta e="T256" id="Seg_5663" s="T255">v-v:pn</ta>
            <ta e="T257" id="Seg_5664" s="T256">adv</ta>
            <ta e="T258" id="Seg_5665" s="T257">clit</ta>
            <ta e="T259" id="Seg_5666" s="T258">interrog-n:case</ta>
            <ta e="T260" id="Seg_5667" s="T259">dem-adj&gt;adv</ta>
            <ta e="T261" id="Seg_5668" s="T260">v-v:ins-v:pn</ta>
            <ta e="T262" id="Seg_5669" s="T261">n-n:case</ta>
            <ta e="T263" id="Seg_5670" s="T262">n-n:case</ta>
            <ta e="T264" id="Seg_5671" s="T263">v-v&gt;adv</ta>
            <ta e="T265" id="Seg_5672" s="T264">v-v:ins-v:pn</ta>
            <ta e="T266" id="Seg_5673" s="T265">adv-adv:case</ta>
            <ta e="T267" id="Seg_5674" s="T266">n-n&gt;adj</ta>
            <ta e="T268" id="Seg_5675" s="T267">n-n:case</ta>
            <ta e="T269" id="Seg_5676" s="T268">n-n:ins-n:case</ta>
            <ta e="T270" id="Seg_5677" s="T269">v-v&gt;adv</ta>
            <ta e="T271" id="Seg_5678" s="T270">adv</ta>
            <ta e="T272" id="Seg_5679" s="T271">v-v:pn</ta>
            <ta e="T273" id="Seg_5680" s="T272">adv</ta>
            <ta e="T274" id="Seg_5681" s="T273">adv</ta>
            <ta e="T275" id="Seg_5682" s="T274">v-v&gt;adv</ta>
            <ta e="T276" id="Seg_5683" s="T275">v-v:pn</ta>
            <ta e="T277" id="Seg_5684" s="T276">pers</ta>
            <ta e="T278" id="Seg_5685" s="T277">adv</ta>
            <ta e="T279" id="Seg_5686" s="T278">preverb</ta>
            <ta e="T280" id="Seg_5687" s="T279">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T281" id="Seg_5688" s="T280">ptcl</ta>
            <ta e="T282" id="Seg_5689" s="T281">adv</ta>
            <ta e="T283" id="Seg_5690" s="T282">v-v&gt;adv</ta>
            <ta e="T284" id="Seg_5691" s="T283">v-v:pn</ta>
            <ta e="T285" id="Seg_5692" s="T284">n-n:case-n:poss</ta>
            <ta e="T286" id="Seg_5693" s="T285">adv</ta>
            <ta e="T287" id="Seg_5694" s="T286">v-v:pn</ta>
            <ta e="T288" id="Seg_5695" s="T287">adv</ta>
            <ta e="T289" id="Seg_5696" s="T288">clit</ta>
            <ta e="T290" id="Seg_5697" s="T289">interrog-n:case</ta>
            <ta e="T291" id="Seg_5698" s="T290">dem-adj&gt;adv</ta>
            <ta e="T292" id="Seg_5699" s="T291">v-v&gt;v-v:pn</ta>
            <ta e="T293" id="Seg_5700" s="T292">n-n:case</ta>
            <ta e="T294" id="Seg_5701" s="T293">ptcl</ta>
            <ta e="T295" id="Seg_5702" s="T294">dem-adj&gt;adv</ta>
            <ta e="T296" id="Seg_5703" s="T295">v-v:pn</ta>
            <ta e="T297" id="Seg_5704" s="T296">v-v&gt;adv</ta>
            <ta e="T298" id="Seg_5705" s="T297">dem</ta>
            <ta e="T299" id="Seg_5706" s="T298">n-n:case</ta>
            <ta e="T300" id="Seg_5707" s="T299">pp-n:case</ta>
            <ta e="T301" id="Seg_5708" s="T300">adj</ta>
            <ta e="T302" id="Seg_5709" s="T301">n-n:case</ta>
            <ta e="T303" id="Seg_5710" s="T302">adv</ta>
            <ta e="T304" id="Seg_5711" s="T303">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T305" id="Seg_5712" s="T304">adv</ta>
            <ta e="T306" id="Seg_5713" s="T305">adv</ta>
            <ta e="T307" id="Seg_5714" s="T306">v-v&gt;adv</ta>
            <ta e="T308" id="Seg_5715" s="T307">dem-adj&gt;adv</ta>
            <ta e="T309" id="Seg_5716" s="T308">v-v&gt;v-v:pn</ta>
            <ta e="T310" id="Seg_5717" s="T309">adv-adv:case</ta>
            <ta e="T311" id="Seg_5718" s="T310">dem-adj&gt;adv</ta>
            <ta e="T312" id="Seg_5719" s="T311">v-v:ins-v:pn</ta>
            <ta e="T313" id="Seg_5720" s="T312">adj</ta>
            <ta e="T314" id="Seg_5721" s="T313">n-n:case</ta>
            <ta e="T315" id="Seg_5722" s="T314">v-v:tense.mood-v:pn</ta>
            <ta e="T316" id="Seg_5723" s="T315">adv</ta>
            <ta e="T317" id="Seg_5724" s="T316">adv</ta>
            <ta e="T318" id="Seg_5725" s="T317">preverb</ta>
            <ta e="T319" id="Seg_5726" s="T318">v-v:ins-v:pn</ta>
            <ta e="T320" id="Seg_5727" s="T319">interrog</ta>
            <ta e="T321" id="Seg_5728" s="T320">adv</ta>
            <ta e="T322" id="Seg_5729" s="T321">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T323" id="Seg_5730" s="T322">adv</ta>
            <ta e="T324" id="Seg_5731" s="T323">dem</ta>
            <ta e="T325" id="Seg_5732" s="T324">n-n:case</ta>
            <ta e="T326" id="Seg_5733" s="T325">adv</ta>
            <ta e="T327" id="Seg_5734" s="T326">v-v&gt;adv</ta>
            <ta e="T328" id="Seg_5735" s="T327">adv</ta>
            <ta e="T329" id="Seg_5736" s="T328">v-v:ins-v:pn</ta>
            <ta e="T330" id="Seg_5737" s="T329">n-n:case</ta>
            <ta e="T331" id="Seg_5738" s="T330">n-n:case</ta>
            <ta e="T332" id="Seg_5739" s="T331">v-v:tense.mood-v:pn</ta>
            <ta e="T333" id="Seg_5740" s="T332">n-n:case-n:poss</ta>
            <ta e="T334" id="Seg_5741" s="T333">n-n:case</ta>
            <ta e="T335" id="Seg_5742" s="T334">v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T336" id="Seg_5743" s="T335">adv</ta>
            <ta e="T337" id="Seg_5744" s="T336">n-n:case</ta>
            <ta e="T338" id="Seg_5745" s="T337">v-v:ins-v:pn</ta>
            <ta e="T339" id="Seg_5746" s="T338">n-n:case</ta>
            <ta e="T340" id="Seg_5747" s="T339">num</ta>
            <ta e="T341" id="Seg_5748" s="T340">n-n:case-n:poss</ta>
            <ta e="T342" id="Seg_5749" s="T341">num</ta>
            <ta e="T343" id="Seg_5750" s="T342">n-n:case</ta>
            <ta e="T521" id="Seg_5751" s="T343">n-n:case</ta>
            <ta e="T344" id="Seg_5752" s="T521">n-n:case</ta>
            <ta e="T345" id="Seg_5753" s="T344">dem-adj&gt;adv</ta>
            <ta e="T346" id="Seg_5754" s="T345">v-v:pn</ta>
            <ta e="T347" id="Seg_5755" s="T346">pers</ta>
            <ta e="T348" id="Seg_5756" s="T347">dem</ta>
            <ta e="T349" id="Seg_5757" s="T348">n-n:case</ta>
            <ta e="T350" id="Seg_5758" s="T349">n-n&gt;v-v:mood.pn</ta>
            <ta e="T351" id="Seg_5759" s="T350">pers-n:case</ta>
            <ta e="T352" id="Seg_5760" s="T351">dem</ta>
            <ta e="T353" id="Seg_5761" s="T352">n-n:case</ta>
            <ta e="T354" id="Seg_5762" s="T353">n-n:case</ta>
            <ta e="T355" id="Seg_5763" s="T354">num</ta>
            <ta e="T356" id="Seg_5764" s="T355">num</ta>
            <ta e="T357" id="Seg_5765" s="T356">n-n:num-n:case</ta>
            <ta e="T358" id="Seg_5766" s="T357">pers</ta>
            <ta e="T359" id="Seg_5767" s="T358">num</ta>
            <ta e="T360" id="Seg_5768" s="T359">num</ta>
            <ta e="T361" id="Seg_5769" s="T360">n-n:case-n:poss</ta>
            <ta e="T362" id="Seg_5770" s="T361">n-n:case</ta>
            <ta e="T363" id="Seg_5771" s="T362">quant</ta>
            <ta e="T364" id="Seg_5772" s="T363">v-v:ins-v:pn</ta>
            <ta e="T365" id="Seg_5773" s="T364">n-n:case</ta>
            <ta e="T366" id="Seg_5774" s="T365">dem-adj&gt;adv</ta>
            <ta e="T367" id="Seg_5775" s="T366">v-v:ins-v:pn</ta>
            <ta e="T368" id="Seg_5776" s="T367">pers</ta>
            <ta e="T369" id="Seg_5777" s="T368">adv</ta>
            <ta e="T370" id="Seg_5778" s="T369">v-v&gt;adv</ta>
            <ta e="T371" id="Seg_5779" s="T370">pers</ta>
            <ta e="T372" id="Seg_5780" s="T371">n-n:case-n:poss</ta>
            <ta e="T373" id="Seg_5781" s="T372">v-v:mood.pn</ta>
            <ta e="T374" id="Seg_5782" s="T373">n-n&gt;v-v:ins-v&gt;v-v:ins-v:pn</ta>
            <ta e="T375" id="Seg_5783" s="T374">pers-n:case</ta>
            <ta e="T376" id="Seg_5784" s="T375">adv</ta>
            <ta e="T377" id="Seg_5785" s="T376">v-v:pn</ta>
            <ta e="T378" id="Seg_5786" s="T377">pers-n:case</ta>
            <ta e="T379" id="Seg_5787" s="T378">adv</ta>
            <ta e="T380" id="Seg_5788" s="T379">v-v:pn</ta>
            <ta e="T381" id="Seg_5789" s="T380">adv</ta>
            <ta e="T382" id="Seg_5790" s="T381">v-v&gt;v-v:pn</ta>
            <ta e="T383" id="Seg_5791" s="T382">n-n:case</ta>
            <ta e="T384" id="Seg_5792" s="T383">interrog-n:case-n:poss</ta>
            <ta e="T385" id="Seg_5793" s="T384">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T386" id="Seg_5794" s="T385">adv</ta>
            <ta e="T387" id="Seg_5795" s="T386">pers-n:case</ta>
            <ta e="T388" id="Seg_5796" s="T387">adv</ta>
            <ta e="T389" id="Seg_5797" s="T388">preverb</ta>
            <ta e="T390" id="Seg_5798" s="T389">v-v:tense-v:pn</ta>
            <ta e="T391" id="Seg_5799" s="T390">adv</ta>
            <ta e="T392" id="Seg_5800" s="T391">ptcl</ta>
            <ta e="T393" id="Seg_5801" s="T392">v-v:tense-v:mood-v:pn</ta>
            <ta e="T394" id="Seg_5802" s="T393">ptcl</ta>
            <ta e="T395" id="Seg_5803" s="T394">pers-n:ins-n:case</ta>
            <ta e="T396" id="Seg_5804" s="T395">v-v&gt;adv</ta>
            <ta e="T397" id="Seg_5805" s="T396">ptcl</ta>
            <ta e="T398" id="Seg_5806" s="T397">n-n:case</ta>
            <ta e="T399" id="Seg_5807" s="T398">ptcl</ta>
            <ta e="T400" id="Seg_5808" s="T399">v-v:tense-v:mood-v:pn</ta>
            <ta e="T401" id="Seg_5809" s="T400">adv</ta>
            <ta e="T402" id="Seg_5810" s="T401">adv</ta>
            <ta e="T403" id="Seg_5811" s="T402">n-n&gt;n-n:num-n:case</ta>
            <ta e="T404" id="Seg_5812" s="T403">n-n:obl.poss-n&gt;adj</ta>
            <ta e="T405" id="Seg_5813" s="T404">v-v&gt;adv</ta>
            <ta e="T406" id="Seg_5814" s="T405">n-n:case</ta>
            <ta e="T407" id="Seg_5815" s="T406">v-v:tense-v:pn</ta>
            <ta e="T408" id="Seg_5816" s="T407">adv</ta>
            <ta e="T409" id="Seg_5817" s="T408">dem</ta>
            <ta e="T410" id="Seg_5818" s="T409">n-n:ins-n:case</ta>
            <ta e="T411" id="Seg_5819" s="T410">n-n&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T412" id="Seg_5820" s="T411">adv</ta>
            <ta e="T413" id="Seg_5821" s="T412">ptcl</ta>
            <ta e="T414" id="Seg_5822" s="T413">v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T415" id="Seg_5823" s="T414">adv</ta>
            <ta e="T416" id="Seg_5824" s="T415">dem</ta>
            <ta e="T417" id="Seg_5825" s="T416">n-n:case</ta>
            <ta e="T418" id="Seg_5826" s="T417">adv</ta>
            <ta e="T419" id="Seg_5827" s="T418">dem-adj&gt;adv</ta>
            <ta e="T420" id="Seg_5828" s="T419">v-v:ins-v:pn</ta>
            <ta e="T421" id="Seg_5829" s="T420">adv</ta>
            <ta e="T422" id="Seg_5830" s="T421">v-v:mood.pn</ta>
            <ta e="T423" id="Seg_5831" s="T422">n-n:case-n:poss</ta>
            <ta e="T424" id="Seg_5832" s="T423">v-v&gt;adv</ta>
            <ta e="T425" id="Seg_5833" s="T424">n-n:case-n:poss</ta>
            <ta e="T426" id="Seg_5834" s="T425">ptcl</ta>
            <ta e="T427" id="Seg_5835" s="T426">n-n:case</ta>
            <ta e="T428" id="Seg_5836" s="T427">ptcl</ta>
            <ta e="T429" id="Seg_5837" s="T428">v-v:tense-v:pn</ta>
            <ta e="T430" id="Seg_5838" s="T429">n-n:ins-n:case</ta>
            <ta e="T431" id="Seg_5839" s="T430">pp-n:case</ta>
            <ta e="T432" id="Seg_5840" s="T431">v-v:pn</ta>
            <ta e="T433" id="Seg_5841" s="T432">adv</ta>
            <ta e="T434" id="Seg_5842" s="T433">pers-n:case</ta>
            <ta e="T435" id="Seg_5843" s="T434">adv</ta>
            <ta e="T436" id="Seg_5844" s="T435">v-v:pn</ta>
            <ta e="T437" id="Seg_5845" s="T436">adv-adv&gt;adj</ta>
            <ta e="T438" id="Seg_5846" s="T437">n-n:case</ta>
            <ta e="T439" id="Seg_5847" s="T438">v-v&gt;adv</ta>
            <ta e="T440" id="Seg_5848" s="T439">adv</ta>
            <ta e="T441" id="Seg_5849" s="T440">preverb</ta>
            <ta e="T442" id="Seg_5850" s="T441">v-v:pn</ta>
            <ta e="T443" id="Seg_5851" s="T442">adv</ta>
            <ta e="T444" id="Seg_5852" s="T443">pers-n:case</ta>
            <ta e="T445" id="Seg_5853" s="T444">num</ta>
            <ta e="T446" id="Seg_5854" s="T445">n-n:case</ta>
            <ta e="T447" id="Seg_5855" s="T446">dem-adj&gt;adv</ta>
            <ta e="T448" id="Seg_5856" s="T447">v-v:ins-v:pn</ta>
            <ta e="T449" id="Seg_5857" s="T448">interrog-n:case</ta>
            <ta e="T450" id="Seg_5858" s="T449">v-v&gt;v-v:pn</ta>
            <ta e="T451" id="Seg_5859" s="T450">ptcl</ta>
            <ta e="T452" id="Seg_5860" s="T451">v-v&gt;v-v:pn</ta>
            <ta e="T453" id="Seg_5861" s="T452">adv</ta>
            <ta e="T454" id="Seg_5862" s="T453">adv</ta>
            <ta e="T455" id="Seg_5863" s="T454">v-v:ins-v:pn</ta>
            <ta e="T456" id="Seg_5864" s="T455">n-n:case</ta>
            <ta e="T522" id="Seg_5865" s="T456">n-n:case</ta>
            <ta e="T457" id="Seg_5866" s="T522">n-n:case</ta>
            <ta e="T458" id="Seg_5867" s="T457">v-v:pn</ta>
            <ta e="T459" id="Seg_5868" s="T458">adv</ta>
            <ta e="T460" id="Seg_5869" s="T459">pers-n:ins-n:case-n:poss</ta>
            <ta e="T461" id="Seg_5870" s="T460">n-n:case</ta>
            <ta e="T462" id="Seg_5871" s="T461">adv</ta>
            <ta e="T463" id="Seg_5872" s="T462">n-n:case</ta>
            <ta e="T464" id="Seg_5873" s="T463">v-v:pn</ta>
            <ta e="T465" id="Seg_5874" s="T464">pers-n:case</ta>
            <ta e="T466" id="Seg_5875" s="T465">dem</ta>
            <ta e="T467" id="Seg_5876" s="T466">v-v&gt;ptcp</ta>
            <ta e="T468" id="Seg_5877" s="T467">n-n&gt;adj</ta>
            <ta e="T469" id="Seg_5878" s="T468">n-n:num-n:ins-n:case</ta>
            <ta e="T470" id="Seg_5879" s="T469">v-v:pn</ta>
            <ta e="T471" id="Seg_5880" s="T470">adv</ta>
            <ta e="T472" id="Seg_5881" s="T471">num-n:ins-num&gt;adj</ta>
            <ta e="T473" id="Seg_5882" s="T472">n-n&gt;adv</ta>
            <ta e="T474" id="Seg_5883" s="T473">dem-adj&gt;adv</ta>
            <ta e="T475" id="Seg_5884" s="T474">v-v:pn</ta>
            <ta e="T476" id="Seg_5885" s="T475">ptcl</ta>
            <ta e="T477" id="Seg_5886" s="T476">ptcl</ta>
            <ta e="T478" id="Seg_5887" s="T477">adj-adj&gt;adv</ta>
            <ta e="T479" id="Seg_5888" s="T478">v-v:pn</ta>
            <ta e="T523" id="Seg_5889" s="T479">n-n:case</ta>
            <ta e="T480" id="Seg_5890" s="T523">n-n:case</ta>
            <ta e="T481" id="Seg_5891" s="T480">n-n:case-n:poss</ta>
            <ta e="T482" id="Seg_5892" s="T481">n-n:case</ta>
            <ta e="T483" id="Seg_5893" s="T482">ptcl</ta>
            <ta e="T484" id="Seg_5894" s="T483">v-v:tense-v:pn</ta>
            <ta e="T485" id="Seg_5895" s="T484">n-n:ins-n:case</ta>
            <ta e="T486" id="Seg_5896" s="T485">pp-n:case</ta>
            <ta e="T487" id="Seg_5897" s="T486">adv</ta>
            <ta e="T488" id="Seg_5898" s="T487">v-v:tense.mood-v:pn</ta>
            <ta e="T489" id="Seg_5899" s="T488">pers-n:case</ta>
            <ta e="T490" id="Seg_5900" s="T489">v-v&gt;adv</ta>
            <ta e="T491" id="Seg_5901" s="T490">adv</ta>
            <ta e="T492" id="Seg_5902" s="T491">preverb</ta>
            <ta e="T493" id="Seg_5903" s="T492">v-v:ins-v:pn</ta>
            <ta e="T494" id="Seg_5904" s="T493">adj</ta>
            <ta e="T495" id="Seg_5905" s="T494">n-n&gt;adv</ta>
            <ta e="T496" id="Seg_5906" s="T495">ptcl</ta>
            <ta e="T497" id="Seg_5907" s="T496">n-n:case</ta>
            <ta e="T498" id="Seg_5908" s="T497">v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T499" id="Seg_5909" s="T498">pers</ta>
            <ta e="T500" id="Seg_5910" s="T499">pers-n:case</ta>
            <ta e="T501" id="Seg_5911" s="T500">conj</ta>
            <ta e="T502" id="Seg_5912" s="T501">v-v:tense-v:pn</ta>
            <ta e="T503" id="Seg_5913" s="T502">n-n:case-n:poss</ta>
            <ta e="T504" id="Seg_5914" s="T503">n-n:case</ta>
            <ta e="T505" id="Seg_5915" s="T504">ptcl</ta>
            <ta e="T506" id="Seg_5916" s="T505">v-v:tense-v:pn</ta>
            <ta e="T507" id="Seg_5917" s="T506">v-v&gt;v-v:pn</ta>
            <ta e="T508" id="Seg_5918" s="T507">n-n:case</ta>
            <ta e="T509" id="Seg_5919" s="T508">v-v&gt;adv</ta>
            <ta e="T510" id="Seg_5920" s="T509">v-v:ins-v:ins-v:pn</ta>
            <ta e="T511" id="Seg_5921" s="T510">adj-adj&gt;adv</ta>
            <ta e="T512" id="Seg_5922" s="T511">adv</ta>
            <ta e="T513" id="Seg_5923" s="T512">ptcl</ta>
            <ta e="T514" id="Seg_5924" s="T513">v-v:tense.mood-v:pn</ta>
            <ta e="T515" id="Seg_5925" s="T514">pers-n:case</ta>
            <ta e="T516" id="Seg_5926" s="T515">adv</ta>
            <ta e="T517" id="Seg_5927" s="T516">adj-adj&gt;adv</ta>
            <ta e="T518" id="Seg_5928" s="T517">v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_5929" s="T0">dem</ta>
            <ta e="T96" id="Seg_5930" s="T1">n</ta>
            <ta e="T2" id="Seg_5931" s="T96">n</ta>
            <ta e="T3" id="Seg_5932" s="T2">n</ta>
            <ta e="T4" id="Seg_5933" s="T3">adv</ta>
            <ta e="T5" id="Seg_5934" s="T4">v</ta>
            <ta e="T6" id="Seg_5935" s="T5">pers</ta>
            <ta e="T7" id="Seg_5936" s="T6">dem</ta>
            <ta e="T8" id="Seg_5937" s="T7">n</ta>
            <ta e="T9" id="Seg_5938" s="T8">adj</ta>
            <ta e="T10" id="Seg_5939" s="T9">n</ta>
            <ta e="T11" id="Seg_5940" s="T10">v</ta>
            <ta e="T519" id="Seg_5941" s="T11">n</ta>
            <ta e="T12" id="Seg_5942" s="T519">n</ta>
            <ta e="T13" id="Seg_5943" s="T12">adv</ta>
            <ta e="T14" id="Seg_5944" s="T13">v</ta>
            <ta e="T15" id="Seg_5945" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_5946" s="T15">ptcl</ta>
            <ta e="T17" id="Seg_5947" s="T16">n</ta>
            <ta e="T18" id="Seg_5948" s="T17">v</ta>
            <ta e="T19" id="Seg_5949" s="T18">n</ta>
            <ta e="T20" id="Seg_5950" s="T19">pers</ta>
            <ta e="T21" id="Seg_5951" s="T20">interrog</ta>
            <ta e="T22" id="Seg_5952" s="T21">v</ta>
            <ta e="T23" id="Seg_5953" s="T22">n</ta>
            <ta e="T24" id="Seg_5954" s="T23">pp</ta>
            <ta e="T25" id="Seg_5955" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_5956" s="T25">adj</ta>
            <ta e="T27" id="Seg_5957" s="T26">n</ta>
            <ta e="T28" id="Seg_5958" s="T27">v</ta>
            <ta e="T520" id="Seg_5959" s="T28">n</ta>
            <ta e="T29" id="Seg_5960" s="T520">n</ta>
            <ta e="T30" id="Seg_5961" s="T29">pers</ta>
            <ta e="T31" id="Seg_5962" s="T30">num</ta>
            <ta e="T32" id="Seg_5963" s="T31">n</ta>
            <ta e="T33" id="Seg_5964" s="T32">n</ta>
            <ta e="T34" id="Seg_5965" s="T33">pp</ta>
            <ta e="T35" id="Seg_5966" s="T34">dem</ta>
            <ta e="T36" id="Seg_5967" s="T35">adj</ta>
            <ta e="T37" id="Seg_5968" s="T36">n</ta>
            <ta e="T38" id="Seg_5969" s="T37">v</ta>
            <ta e="T39" id="Seg_5970" s="T38">v</ta>
            <ta e="T40" id="Seg_5971" s="T39">pers</ta>
            <ta e="T41" id="Seg_5972" s="T40">v</ta>
            <ta e="T42" id="Seg_5973" s="T41">dem</ta>
            <ta e="T43" id="Seg_5974" s="T42">adj</ta>
            <ta e="T44" id="Seg_5975" s="T43">n</ta>
            <ta e="T45" id="Seg_5976" s="T44">n</ta>
            <ta e="T46" id="Seg_5977" s="T45">v</ta>
            <ta e="T47" id="Seg_5978" s="T46">adv</ta>
            <ta e="T48" id="Seg_5979" s="T47">adj</ta>
            <ta e="T49" id="Seg_5980" s="T48">n</ta>
            <ta e="T51" id="Seg_5981" s="T50">dem</ta>
            <ta e="T52" id="Seg_5982" s="T51">adj</ta>
            <ta e="T53" id="Seg_5983" s="T52">n</ta>
            <ta e="T54" id="Seg_5984" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_5985" s="T54">adj</ta>
            <ta e="T56" id="Seg_5986" s="T55">v</ta>
            <ta e="T57" id="Seg_5987" s="T56">adv</ta>
            <ta e="T58" id="Seg_5988" s="T57">pers</ta>
            <ta e="T59" id="Seg_5989" s="T58">adv</ta>
            <ta e="T60" id="Seg_5990" s="T59">n</ta>
            <ta e="T61" id="Seg_5991" s="T60">n</ta>
            <ta e="T62" id="Seg_5992" s="T61">preverb</ta>
            <ta e="T63" id="Seg_5993" s="T62">v</ta>
            <ta e="T64" id="Seg_5994" s="T63">conj</ta>
            <ta e="T65" id="Seg_5995" s="T64">n</ta>
            <ta e="T66" id="Seg_5996" s="T65">preverb</ta>
            <ta e="T67" id="Seg_5997" s="T66">v</ta>
            <ta e="T68" id="Seg_5998" s="T67">adv</ta>
            <ta e="T69" id="Seg_5999" s="T68">dem</ta>
            <ta e="T70" id="Seg_6000" s="T69">num</ta>
            <ta e="T71" id="Seg_6001" s="T70">n</ta>
            <ta e="T72" id="Seg_6002" s="T71">preverb</ta>
            <ta e="T73" id="Seg_6003" s="T72">n</ta>
            <ta e="T74" id="Seg_6004" s="T73">v</ta>
            <ta e="T75" id="Seg_6005" s="T74">dem</ta>
            <ta e="T76" id="Seg_6006" s="T75">n</ta>
            <ta e="T77" id="Seg_6007" s="T76">adv</ta>
            <ta e="T78" id="Seg_6008" s="T77">v</ta>
            <ta e="T79" id="Seg_6009" s="T78">dem</ta>
            <ta e="T80" id="Seg_6010" s="T79">adj</ta>
            <ta e="T81" id="Seg_6011" s="T80">adj</ta>
            <ta e="T82" id="Seg_6012" s="T81">n</ta>
            <ta e="T83" id="Seg_6013" s="T82">ptcp</ta>
            <ta e="T84" id="Seg_6014" s="T83">n</ta>
            <ta e="T85" id="Seg_6015" s="T84">n</ta>
            <ta e="T86" id="Seg_6016" s="T85">adv</ta>
            <ta e="T87" id="Seg_6017" s="T86">v</ta>
            <ta e="T88" id="Seg_6018" s="T87">adv</ta>
            <ta e="T89" id="Seg_6019" s="T88">dem</ta>
            <ta e="T90" id="Seg_6020" s="T89">n</ta>
            <ta e="T91" id="Seg_6021" s="T90">v</ta>
            <ta e="T92" id="Seg_6022" s="T91">adv</ta>
            <ta e="T93" id="Seg_6023" s="T92">ptcp</ta>
            <ta e="T94" id="Seg_6024" s="T93">n</ta>
            <ta e="T95" id="Seg_6025" s="T94">v</ta>
            <ta e="T97" id="Seg_6026" s="T95">v</ta>
            <ta e="T98" id="Seg_6027" s="T97">pers</ta>
            <ta e="T99" id="Seg_6028" s="T98">adv</ta>
            <ta e="T100" id="Seg_6029" s="T99">adv</ta>
            <ta e="T101" id="Seg_6030" s="T100">v</ta>
            <ta e="T102" id="Seg_6031" s="T101">pers</ta>
            <ta e="T103" id="Seg_6032" s="T102">ptcl</ta>
            <ta e="T104" id="Seg_6033" s="T103">n</ta>
            <ta e="T105" id="Seg_6034" s="T104">n</ta>
            <ta e="T106" id="Seg_6035" s="T105">adv</ta>
            <ta e="T107" id="Seg_6036" s="T106">ptcl</ta>
            <ta e="T108" id="Seg_6037" s="T107">v</ta>
            <ta e="T109" id="Seg_6038" s="T108">pers</ta>
            <ta e="T110" id="Seg_6039" s="T109">adv</ta>
            <ta e="T111" id="Seg_6040" s="T110">v</ta>
            <ta e="T112" id="Seg_6041" s="T111">adv</ta>
            <ta e="T113" id="Seg_6042" s="T112">adv</ta>
            <ta e="T114" id="Seg_6043" s="T113">v</ta>
            <ta e="T115" id="Seg_6044" s="T114">adv</ta>
            <ta e="T116" id="Seg_6045" s="T115">v</ta>
            <ta e="T117" id="Seg_6046" s="T116">n</ta>
            <ta e="T118" id="Seg_6047" s="T117">n</ta>
            <ta e="T119" id="Seg_6048" s="T118">ptcl</ta>
            <ta e="T120" id="Seg_6049" s="T119">v</ta>
            <ta e="T121" id="Seg_6050" s="T120">adv</ta>
            <ta e="T122" id="Seg_6051" s="T121">adv</ta>
            <ta e="T123" id="Seg_6052" s="T122">v</ta>
            <ta e="T124" id="Seg_6053" s="T123">n</ta>
            <ta e="T125" id="Seg_6054" s="T124">n</ta>
            <ta e="T126" id="Seg_6055" s="T125">v</ta>
            <ta e="T127" id="Seg_6056" s="T126">ptcl</ta>
            <ta e="T128" id="Seg_6057" s="T127">num</ta>
            <ta e="T129" id="Seg_6058" s="T128">n</ta>
            <ta e="T130" id="Seg_6059" s="T129">adv</ta>
            <ta e="T131" id="Seg_6060" s="T130">v</ta>
            <ta e="T132" id="Seg_6061" s="T131">pers</ta>
            <ta e="T133" id="Seg_6062" s="T132">emphpro</ta>
            <ta e="T134" id="Seg_6063" s="T133">adv</ta>
            <ta e="T135" id="Seg_6064" s="T134">v</ta>
            <ta e="T136" id="Seg_6065" s="T135">adj</ta>
            <ta e="T137" id="Seg_6066" s="T136">v</ta>
            <ta e="T138" id="Seg_6067" s="T137">adv</ta>
            <ta e="T139" id="Seg_6068" s="T138">v</ta>
            <ta e="T140" id="Seg_6069" s="T139">adv</ta>
            <ta e="T141" id="Seg_6070" s="T140">n</ta>
            <ta e="T142" id="Seg_6071" s="T141">v</ta>
            <ta e="T143" id="Seg_6072" s="T142">n</ta>
            <ta e="T144" id="Seg_6073" s="T143">v</ta>
            <ta e="T145" id="Seg_6074" s="T144">n</ta>
            <ta e="T146" id="Seg_6075" s="T145">pp</ta>
            <ta e="T147" id="Seg_6076" s="T146">v</ta>
            <ta e="T148" id="Seg_6077" s="T147">adj</ta>
            <ta e="T149" id="Seg_6078" s="T148">n</ta>
            <ta e="T150" id="Seg_6079" s="T149">n</ta>
            <ta e="T151" id="Seg_6080" s="T150">n</ta>
            <ta e="T152" id="Seg_6081" s="T151">pp</ta>
            <ta e="T153" id="Seg_6082" s="T152">v</ta>
            <ta e="T154" id="Seg_6083" s="T153">n</ta>
            <ta e="T155" id="Seg_6084" s="T154">quant</ta>
            <ta e="T156" id="Seg_6085" s="T155">v</ta>
            <ta e="T157" id="Seg_6086" s="T156">num</ta>
            <ta e="T158" id="Seg_6087" s="T157">num</ta>
            <ta e="T159" id="Seg_6088" s="T158">n</ta>
            <ta e="T160" id="Seg_6089" s="T159">pers</ta>
            <ta e="T161" id="Seg_6090" s="T160">clit</ta>
            <ta e="T162" id="Seg_6091" s="T161">interrog</ta>
            <ta e="T163" id="Seg_6092" s="T162">adv</ta>
            <ta e="T164" id="Seg_6093" s="T163">v</ta>
            <ta e="T165" id="Seg_6094" s="T164">conj</ta>
            <ta e="T166" id="Seg_6095" s="T165">n</ta>
            <ta e="T167" id="Seg_6096" s="T166">ptcl</ta>
            <ta e="T168" id="Seg_6097" s="T167">conj</ta>
            <ta e="T169" id="Seg_6098" s="T168">n</ta>
            <ta e="T170" id="Seg_6099" s="T169">n</ta>
            <ta e="T171" id="Seg_6100" s="T170">adv</ta>
            <ta e="T172" id="Seg_6101" s="T171">conj</ta>
            <ta e="T173" id="Seg_6102" s="T172">adv</ta>
            <ta e="T174" id="Seg_6103" s="T173">v</ta>
            <ta e="T175" id="Seg_6104" s="T174">pers</ta>
            <ta e="T176" id="Seg_6105" s="T175">preverb</ta>
            <ta e="T177" id="Seg_6106" s="T176">ptcl</ta>
            <ta e="T178" id="Seg_6107" s="T177">v</ta>
            <ta e="T179" id="Seg_6108" s="T178">adv</ta>
            <ta e="T180" id="Seg_6109" s="T179">v</ta>
            <ta e="T181" id="Seg_6110" s="T180">adv</ta>
            <ta e="T182" id="Seg_6111" s="T181">n</ta>
            <ta e="T183" id="Seg_6112" s="T182">n</ta>
            <ta e="T184" id="Seg_6113" s="T183">v</ta>
            <ta e="T185" id="Seg_6114" s="T184">dem</ta>
            <ta e="T186" id="Seg_6115" s="T185">n</ta>
            <ta e="T187" id="Seg_6116" s="T186">adj</ta>
            <ta e="T188" id="Seg_6117" s="T187">adj</ta>
            <ta e="T189" id="Seg_6118" s="T188">n</ta>
            <ta e="T190" id="Seg_6119" s="T189">adv</ta>
            <ta e="T191" id="Seg_6120" s="T190">adv</ta>
            <ta e="T192" id="Seg_6121" s="T191">conj</ta>
            <ta e="T193" id="Seg_6122" s="T192">preverb</ta>
            <ta e="T194" id="Seg_6123" s="T193">v</ta>
            <ta e="T195" id="Seg_6124" s="T194">conj</ta>
            <ta e="T196" id="Seg_6125" s="T195">pers</ta>
            <ta e="T197" id="Seg_6126" s="T196">clit</ta>
            <ta e="T198" id="Seg_6127" s="T197">interrog</ta>
            <ta e="T199" id="Seg_6128" s="T198">adv</ta>
            <ta e="T200" id="Seg_6129" s="T199">pers</ta>
            <ta e="T201" id="Seg_6130" s="T200">v</ta>
            <ta e="T202" id="Seg_6131" s="T201">n</ta>
            <ta e="T203" id="Seg_6132" s="T202">adv</ta>
            <ta e="T204" id="Seg_6133" s="T203">adv</ta>
            <ta e="T205" id="Seg_6134" s="T204">adv</ta>
            <ta e="T206" id="Seg_6135" s="T205">v</ta>
            <ta e="T207" id="Seg_6136" s="T206">adv</ta>
            <ta e="T208" id="Seg_6137" s="T207">n</ta>
            <ta e="T209" id="Seg_6138" s="T208">adv</ta>
            <ta e="T210" id="Seg_6139" s="T209">v</ta>
            <ta e="T211" id="Seg_6140" s="T210">v</ta>
            <ta e="T212" id="Seg_6141" s="T211">n</ta>
            <ta e="T213" id="Seg_6142" s="T212">adv</ta>
            <ta e="T214" id="Seg_6143" s="T213">n</ta>
            <ta e="T215" id="Seg_6144" s="T214">v</ta>
            <ta e="T216" id="Seg_6145" s="T215">n</ta>
            <ta e="T217" id="Seg_6146" s="T216">pp</ta>
            <ta e="T218" id="Seg_6147" s="T217">v</ta>
            <ta e="T219" id="Seg_6148" s="T218">num</ta>
            <ta e="T220" id="Seg_6149" s="T219">n</ta>
            <ta e="T221" id="Seg_6150" s="T220">adv</ta>
            <ta e="T222" id="Seg_6151" s="T221">v</ta>
            <ta e="T223" id="Seg_6152" s="T222">ptcl</ta>
            <ta e="T224" id="Seg_6153" s="T223">n</ta>
            <ta e="T225" id="Seg_6154" s="T224">v</ta>
            <ta e="T226" id="Seg_6155" s="T225">conj</ta>
            <ta e="T227" id="Seg_6156" s="T226">n</ta>
            <ta e="T228" id="Seg_6157" s="T227">v</ta>
            <ta e="T229" id="Seg_6158" s="T228">n</ta>
            <ta e="T230" id="Seg_6159" s="T229">n</ta>
            <ta e="T231" id="Seg_6160" s="T230">pers</ta>
            <ta e="T232" id="Seg_6161" s="T231">adv</ta>
            <ta e="T233" id="Seg_6162" s="T232">n</ta>
            <ta e="T234" id="Seg_6163" s="T233">v</ta>
            <ta e="T235" id="Seg_6164" s="T234">adv</ta>
            <ta e="T236" id="Seg_6165" s="T235">adv</ta>
            <ta e="T237" id="Seg_6166" s="T236">ptcl</ta>
            <ta e="T238" id="Seg_6167" s="T237">v</ta>
            <ta e="T239" id="Seg_6168" s="T238">n</ta>
            <ta e="T240" id="Seg_6169" s="T239">adv</ta>
            <ta e="T241" id="Seg_6170" s="T240">v</ta>
            <ta e="T242" id="Seg_6171" s="T241">v</ta>
            <ta e="T243" id="Seg_6172" s="T242">adv</ta>
            <ta e="T244" id="Seg_6173" s="T243">n</ta>
            <ta e="T245" id="Seg_6174" s="T244">n</ta>
            <ta e="T246" id="Seg_6175" s="T245">pp</ta>
            <ta e="T247" id="Seg_6176" s="T246">adv</ta>
            <ta e="T248" id="Seg_6177" s="T247">adv</ta>
            <ta e="T249" id="Seg_6178" s="T248">v</ta>
            <ta e="T250" id="Seg_6179" s="T249">adv</ta>
            <ta e="T251" id="Seg_6180" s="T250">adv</ta>
            <ta e="T252" id="Seg_6181" s="T251">n</ta>
            <ta e="T253" id="Seg_6182" s="T252">v</ta>
            <ta e="T254" id="Seg_6183" s="T253">adv</ta>
            <ta e="T255" id="Seg_6184" s="T254">adv</ta>
            <ta e="T256" id="Seg_6185" s="T255">v</ta>
            <ta e="T257" id="Seg_6186" s="T256">adv</ta>
            <ta e="T258" id="Seg_6187" s="T257">clit</ta>
            <ta e="T259" id="Seg_6188" s="T258">interrog</ta>
            <ta e="T260" id="Seg_6189" s="T259">adv</ta>
            <ta e="T261" id="Seg_6190" s="T260">v</ta>
            <ta e="T262" id="Seg_6191" s="T261">n</ta>
            <ta e="T263" id="Seg_6192" s="T262">n</ta>
            <ta e="T264" id="Seg_6193" s="T263">adv</ta>
            <ta e="T265" id="Seg_6194" s="T264">v</ta>
            <ta e="T266" id="Seg_6195" s="T265">adv</ta>
            <ta e="T267" id="Seg_6196" s="T266">adj</ta>
            <ta e="T268" id="Seg_6197" s="T267">n</ta>
            <ta e="T269" id="Seg_6198" s="T268">n</ta>
            <ta e="T270" id="Seg_6199" s="T269">adv</ta>
            <ta e="T271" id="Seg_6200" s="T270">adv</ta>
            <ta e="T272" id="Seg_6201" s="T271">v</ta>
            <ta e="T273" id="Seg_6202" s="T272">adv</ta>
            <ta e="T274" id="Seg_6203" s="T273">adv</ta>
            <ta e="T275" id="Seg_6204" s="T274">adv</ta>
            <ta e="T276" id="Seg_6205" s="T275">v</ta>
            <ta e="T277" id="Seg_6206" s="T276">pers</ta>
            <ta e="T278" id="Seg_6207" s="T277">adv</ta>
            <ta e="T279" id="Seg_6208" s="T278">preverb</ta>
            <ta e="T280" id="Seg_6209" s="T279">v</ta>
            <ta e="T281" id="Seg_6210" s="T280">ptcl</ta>
            <ta e="T282" id="Seg_6211" s="T281">adv</ta>
            <ta e="T283" id="Seg_6212" s="T282">adv</ta>
            <ta e="T284" id="Seg_6213" s="T283">v</ta>
            <ta e="T285" id="Seg_6214" s="T284">n</ta>
            <ta e="T286" id="Seg_6215" s="T285">adv</ta>
            <ta e="T287" id="Seg_6216" s="T286">v</ta>
            <ta e="T288" id="Seg_6217" s="T287">adv</ta>
            <ta e="T289" id="Seg_6218" s="T288">clit</ta>
            <ta e="T290" id="Seg_6219" s="T289">interrog</ta>
            <ta e="T291" id="Seg_6220" s="T290">adv</ta>
            <ta e="T292" id="Seg_6221" s="T291">v</ta>
            <ta e="T293" id="Seg_6222" s="T292">n</ta>
            <ta e="T294" id="Seg_6223" s="T293">ptcl</ta>
            <ta e="T295" id="Seg_6224" s="T294">adv</ta>
            <ta e="T296" id="Seg_6225" s="T295">v</ta>
            <ta e="T297" id="Seg_6226" s="T296">adv</ta>
            <ta e="T298" id="Seg_6227" s="T297">dem</ta>
            <ta e="T299" id="Seg_6228" s="T298">n</ta>
            <ta e="T300" id="Seg_6229" s="T299">pp</ta>
            <ta e="T301" id="Seg_6230" s="T300">adj</ta>
            <ta e="T302" id="Seg_6231" s="T301">n</ta>
            <ta e="T303" id="Seg_6232" s="T302">adv</ta>
            <ta e="T304" id="Seg_6233" s="T303">v</ta>
            <ta e="T305" id="Seg_6234" s="T304">adv</ta>
            <ta e="T306" id="Seg_6235" s="T305">adv</ta>
            <ta e="T307" id="Seg_6236" s="T306">adv</ta>
            <ta e="T308" id="Seg_6237" s="T307">adv</ta>
            <ta e="T309" id="Seg_6238" s="T308">v</ta>
            <ta e="T310" id="Seg_6239" s="T309">adv</ta>
            <ta e="T311" id="Seg_6240" s="T310">adv</ta>
            <ta e="T312" id="Seg_6241" s="T311">v</ta>
            <ta e="T313" id="Seg_6242" s="T312">adj</ta>
            <ta e="T314" id="Seg_6243" s="T313">n</ta>
            <ta e="T315" id="Seg_6244" s="T314">v</ta>
            <ta e="T316" id="Seg_6245" s="T315">adv</ta>
            <ta e="T317" id="Seg_6246" s="T316">adv</ta>
            <ta e="T318" id="Seg_6247" s="T317">preverb</ta>
            <ta e="T319" id="Seg_6248" s="T318">v</ta>
            <ta e="T320" id="Seg_6249" s="T319">interrog</ta>
            <ta e="T321" id="Seg_6250" s="T320">adv</ta>
            <ta e="T322" id="Seg_6251" s="T321">v</ta>
            <ta e="T323" id="Seg_6252" s="T322">adv</ta>
            <ta e="T324" id="Seg_6253" s="T323">dem</ta>
            <ta e="T325" id="Seg_6254" s="T324">n</ta>
            <ta e="T326" id="Seg_6255" s="T325">adv</ta>
            <ta e="T327" id="Seg_6256" s="T326">adv</ta>
            <ta e="T328" id="Seg_6257" s="T327">adv</ta>
            <ta e="T329" id="Seg_6258" s="T328">v</ta>
            <ta e="T330" id="Seg_6259" s="T329">n</ta>
            <ta e="T331" id="Seg_6260" s="T330">n</ta>
            <ta e="T332" id="Seg_6261" s="T331">v</ta>
            <ta e="T333" id="Seg_6262" s="T332">n</ta>
            <ta e="T334" id="Seg_6263" s="T333">n</ta>
            <ta e="T335" id="Seg_6264" s="T334">v</ta>
            <ta e="T336" id="Seg_6265" s="T335">adv</ta>
            <ta e="T337" id="Seg_6266" s="T336">n</ta>
            <ta e="T338" id="Seg_6267" s="T337">v</ta>
            <ta e="T339" id="Seg_6268" s="T338">n</ta>
            <ta e="T340" id="Seg_6269" s="T339">num</ta>
            <ta e="T341" id="Seg_6270" s="T340">n</ta>
            <ta e="T342" id="Seg_6271" s="T341">num</ta>
            <ta e="T343" id="Seg_6272" s="T342">n</ta>
            <ta e="T521" id="Seg_6273" s="T343">n</ta>
            <ta e="T344" id="Seg_6274" s="T521">n</ta>
            <ta e="T345" id="Seg_6275" s="T344">adv</ta>
            <ta e="T346" id="Seg_6276" s="T345">v</ta>
            <ta e="T347" id="Seg_6277" s="T346">pers</ta>
            <ta e="T348" id="Seg_6278" s="T347">dem</ta>
            <ta e="T349" id="Seg_6279" s="T348">n</ta>
            <ta e="T350" id="Seg_6280" s="T349">v</ta>
            <ta e="T351" id="Seg_6281" s="T350">pers</ta>
            <ta e="T352" id="Seg_6282" s="T351">dem</ta>
            <ta e="T353" id="Seg_6283" s="T352">n</ta>
            <ta e="T354" id="Seg_6284" s="T353">n</ta>
            <ta e="T355" id="Seg_6285" s="T354">num</ta>
            <ta e="T356" id="Seg_6286" s="T355">num</ta>
            <ta e="T357" id="Seg_6287" s="T356">n</ta>
            <ta e="T358" id="Seg_6288" s="T357">pers</ta>
            <ta e="T359" id="Seg_6289" s="T358">num</ta>
            <ta e="T360" id="Seg_6290" s="T359">num</ta>
            <ta e="T361" id="Seg_6291" s="T360">n</ta>
            <ta e="T362" id="Seg_6292" s="T361">n</ta>
            <ta e="T363" id="Seg_6293" s="T362">quant</ta>
            <ta e="T364" id="Seg_6294" s="T363">v</ta>
            <ta e="T365" id="Seg_6295" s="T364">n</ta>
            <ta e="T366" id="Seg_6296" s="T365">adv</ta>
            <ta e="T367" id="Seg_6297" s="T366">v</ta>
            <ta e="T368" id="Seg_6298" s="T367">pers</ta>
            <ta e="T369" id="Seg_6299" s="T368">adv</ta>
            <ta e="T370" id="Seg_6300" s="T369">adv</ta>
            <ta e="T371" id="Seg_6301" s="T370">pers</ta>
            <ta e="T372" id="Seg_6302" s="T371">n</ta>
            <ta e="T373" id="Seg_6303" s="T372">v</ta>
            <ta e="T374" id="Seg_6304" s="T373">v</ta>
            <ta e="T375" id="Seg_6305" s="T374">pers</ta>
            <ta e="T376" id="Seg_6306" s="T375">adv</ta>
            <ta e="T377" id="Seg_6307" s="T376">v</ta>
            <ta e="T378" id="Seg_6308" s="T377">pers</ta>
            <ta e="T379" id="Seg_6309" s="T378">adv</ta>
            <ta e="T380" id="Seg_6310" s="T379">v</ta>
            <ta e="T381" id="Seg_6311" s="T380">adv</ta>
            <ta e="T382" id="Seg_6312" s="T381">v</ta>
            <ta e="T383" id="Seg_6313" s="T382">n</ta>
            <ta e="T384" id="Seg_6314" s="T383">interrog</ta>
            <ta e="T385" id="Seg_6315" s="T384">v</ta>
            <ta e="T386" id="Seg_6316" s="T385">adv</ta>
            <ta e="T387" id="Seg_6317" s="T386">pers</ta>
            <ta e="T388" id="Seg_6318" s="T387">adv</ta>
            <ta e="T389" id="Seg_6319" s="T388">preverb</ta>
            <ta e="T390" id="Seg_6320" s="T389">v</ta>
            <ta e="T391" id="Seg_6321" s="T390">adv</ta>
            <ta e="T392" id="Seg_6322" s="T391">ptcl</ta>
            <ta e="T393" id="Seg_6323" s="T392">v</ta>
            <ta e="T394" id="Seg_6324" s="T393">ptcl</ta>
            <ta e="T395" id="Seg_6325" s="T394">pers</ta>
            <ta e="T396" id="Seg_6326" s="T395">adv</ta>
            <ta e="T397" id="Seg_6327" s="T396">ptcl</ta>
            <ta e="T398" id="Seg_6328" s="T397">n</ta>
            <ta e="T399" id="Seg_6329" s="T398">ptcl</ta>
            <ta e="T400" id="Seg_6330" s="T399">v</ta>
            <ta e="T401" id="Seg_6331" s="T400">adv</ta>
            <ta e="T402" id="Seg_6332" s="T401">adv</ta>
            <ta e="T403" id="Seg_6333" s="T402">n</ta>
            <ta e="T404" id="Seg_6334" s="T403">adj</ta>
            <ta e="T405" id="Seg_6335" s="T404">adv</ta>
            <ta e="T406" id="Seg_6336" s="T405">n</ta>
            <ta e="T407" id="Seg_6337" s="T406">v</ta>
            <ta e="T408" id="Seg_6338" s="T407">adv</ta>
            <ta e="T409" id="Seg_6339" s="T408">dem</ta>
            <ta e="T410" id="Seg_6340" s="T409">n</ta>
            <ta e="T411" id="Seg_6341" s="T410">adv</ta>
            <ta e="T412" id="Seg_6342" s="T411">adv</ta>
            <ta e="T413" id="Seg_6343" s="T412">ptcl</ta>
            <ta e="T414" id="Seg_6344" s="T413">v</ta>
            <ta e="T415" id="Seg_6345" s="T414">adv</ta>
            <ta e="T416" id="Seg_6346" s="T415">dem</ta>
            <ta e="T417" id="Seg_6347" s="T416">n</ta>
            <ta e="T418" id="Seg_6348" s="T417">adv</ta>
            <ta e="T419" id="Seg_6349" s="T418">adv</ta>
            <ta e="T420" id="Seg_6350" s="T419">v</ta>
            <ta e="T421" id="Seg_6351" s="T420">adv</ta>
            <ta e="T422" id="Seg_6352" s="T421">v</ta>
            <ta e="T423" id="Seg_6353" s="T422">n</ta>
            <ta e="T424" id="Seg_6354" s="T423">adv</ta>
            <ta e="T425" id="Seg_6355" s="T424">n</ta>
            <ta e="T426" id="Seg_6356" s="T425">ptcl</ta>
            <ta e="T427" id="Seg_6357" s="T426">n</ta>
            <ta e="T428" id="Seg_6358" s="T427">ptcl</ta>
            <ta e="T429" id="Seg_6359" s="T428">v</ta>
            <ta e="T430" id="Seg_6360" s="T429">n</ta>
            <ta e="T431" id="Seg_6361" s="T430">pp</ta>
            <ta e="T432" id="Seg_6362" s="T431">v</ta>
            <ta e="T433" id="Seg_6363" s="T432">adv</ta>
            <ta e="T434" id="Seg_6364" s="T433">pers</ta>
            <ta e="T435" id="Seg_6365" s="T434">adv</ta>
            <ta e="T436" id="Seg_6366" s="T435">v</ta>
            <ta e="T437" id="Seg_6367" s="T436">adj</ta>
            <ta e="T438" id="Seg_6368" s="T437">n</ta>
            <ta e="T439" id="Seg_6369" s="T438">adv</ta>
            <ta e="T440" id="Seg_6370" s="T439">adv</ta>
            <ta e="T441" id="Seg_6371" s="T440">preverb</ta>
            <ta e="T442" id="Seg_6372" s="T441">v</ta>
            <ta e="T443" id="Seg_6373" s="T442">adv</ta>
            <ta e="T444" id="Seg_6374" s="T443">pers</ta>
            <ta e="T445" id="Seg_6375" s="T444">num</ta>
            <ta e="T446" id="Seg_6376" s="T445">n</ta>
            <ta e="T447" id="Seg_6377" s="T446">adv</ta>
            <ta e="T448" id="Seg_6378" s="T447">v</ta>
            <ta e="T449" id="Seg_6379" s="T448">interrog</ta>
            <ta e="T450" id="Seg_6380" s="T449">v</ta>
            <ta e="T451" id="Seg_6381" s="T450">ptcl</ta>
            <ta e="T452" id="Seg_6382" s="T451">v</ta>
            <ta e="T453" id="Seg_6383" s="T452">adv</ta>
            <ta e="T454" id="Seg_6384" s="T453">adv</ta>
            <ta e="T455" id="Seg_6385" s="T454">v</ta>
            <ta e="T456" id="Seg_6386" s="T455">n</ta>
            <ta e="T522" id="Seg_6387" s="T456">n</ta>
            <ta e="T457" id="Seg_6388" s="T522">n</ta>
            <ta e="T458" id="Seg_6389" s="T457">v</ta>
            <ta e="T459" id="Seg_6390" s="T458">adv</ta>
            <ta e="T460" id="Seg_6391" s="T459">pers</ta>
            <ta e="T461" id="Seg_6392" s="T460">n</ta>
            <ta e="T462" id="Seg_6393" s="T461">adv</ta>
            <ta e="T463" id="Seg_6394" s="T462">n</ta>
            <ta e="T464" id="Seg_6395" s="T463">v</ta>
            <ta e="T465" id="Seg_6396" s="T464">pers</ta>
            <ta e="T466" id="Seg_6397" s="T465">dem</ta>
            <ta e="T467" id="Seg_6398" s="T466">ptcp</ta>
            <ta e="T468" id="Seg_6399" s="T467">adj</ta>
            <ta e="T469" id="Seg_6400" s="T468">n</ta>
            <ta e="T470" id="Seg_6401" s="T469">v</ta>
            <ta e="T471" id="Seg_6402" s="T470">adv</ta>
            <ta e="T472" id="Seg_6403" s="T471">adj</ta>
            <ta e="T473" id="Seg_6404" s="T472">adv</ta>
            <ta e="T474" id="Seg_6405" s="T473">adv</ta>
            <ta e="T475" id="Seg_6406" s="T474">v</ta>
            <ta e="T476" id="Seg_6407" s="T475">ptcl</ta>
            <ta e="T477" id="Seg_6408" s="T476">ptcl</ta>
            <ta e="T478" id="Seg_6409" s="T477">adv</ta>
            <ta e="T479" id="Seg_6410" s="T478">v</ta>
            <ta e="T523" id="Seg_6411" s="T479">n</ta>
            <ta e="T480" id="Seg_6412" s="T523">n</ta>
            <ta e="T481" id="Seg_6413" s="T480">n</ta>
            <ta e="T482" id="Seg_6414" s="T481">n</ta>
            <ta e="T483" id="Seg_6415" s="T482">ptcl</ta>
            <ta e="T484" id="Seg_6416" s="T483">v</ta>
            <ta e="T485" id="Seg_6417" s="T484">n</ta>
            <ta e="T486" id="Seg_6418" s="T485">pp</ta>
            <ta e="T487" id="Seg_6419" s="T486">adv</ta>
            <ta e="T488" id="Seg_6420" s="T487">v</ta>
            <ta e="T489" id="Seg_6421" s="T488">pers</ta>
            <ta e="T490" id="Seg_6422" s="T489">adv</ta>
            <ta e="T491" id="Seg_6423" s="T490">adv</ta>
            <ta e="T492" id="Seg_6424" s="T491">preverb</ta>
            <ta e="T493" id="Seg_6425" s="T492">v</ta>
            <ta e="T494" id="Seg_6426" s="T493">adj</ta>
            <ta e="T495" id="Seg_6427" s="T494">adv</ta>
            <ta e="T496" id="Seg_6428" s="T495">ptcl</ta>
            <ta e="T497" id="Seg_6429" s="T496">n</ta>
            <ta e="T498" id="Seg_6430" s="T497">v</ta>
            <ta e="T499" id="Seg_6431" s="T498">pers</ta>
            <ta e="T500" id="Seg_6432" s="T499">pers</ta>
            <ta e="T501" id="Seg_6433" s="T500">conj</ta>
            <ta e="T502" id="Seg_6434" s="T501">v</ta>
            <ta e="T503" id="Seg_6435" s="T502">n</ta>
            <ta e="T504" id="Seg_6436" s="T503">n</ta>
            <ta e="T505" id="Seg_6437" s="T504">ptcl</ta>
            <ta e="T506" id="Seg_6438" s="T505">v</ta>
            <ta e="T507" id="Seg_6439" s="T506">v</ta>
            <ta e="T508" id="Seg_6440" s="T507">n</ta>
            <ta e="T509" id="Seg_6441" s="T508">adv</ta>
            <ta e="T510" id="Seg_6442" s="T509">v</ta>
            <ta e="T511" id="Seg_6443" s="T510">adv</ta>
            <ta e="T512" id="Seg_6444" s="T511">adv</ta>
            <ta e="T513" id="Seg_6445" s="T512">ptcl</ta>
            <ta e="T514" id="Seg_6446" s="T513">v</ta>
            <ta e="T515" id="Seg_6447" s="T514">pers</ta>
            <ta e="T516" id="Seg_6448" s="T515">adv</ta>
            <ta e="T517" id="Seg_6449" s="T516">adv</ta>
            <ta e="T518" id="Seg_6450" s="T517">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_6451" s="T2">np.h:A</ta>
            <ta e="T6" id="Seg_6452" s="T5">pro.h:A</ta>
            <ta e="T8" id="Seg_6453" s="T7">np:L</ta>
            <ta e="T10" id="Seg_6454" s="T9">np:P</ta>
            <ta e="T12" id="Seg_6455" s="T519">np.h:A</ta>
            <ta e="T17" id="Seg_6456" s="T16">np.h:A</ta>
            <ta e="T19" id="Seg_6457" s="T18">np:P</ta>
            <ta e="T20" id="Seg_6458" s="T19">pro.h:A</ta>
            <ta e="T23" id="Seg_6459" s="T22">pp:G</ta>
            <ta e="T27" id="Seg_6460" s="T26">np:L</ta>
            <ta e="T29" id="Seg_6461" s="T520">np.h:Th</ta>
            <ta e="T30" id="Seg_6462" s="T29">pro.h:Poss</ta>
            <ta e="T32" id="Seg_6463" s="T31">np.h:A</ta>
            <ta e="T33" id="Seg_6464" s="T32">pp:G</ta>
            <ta e="T37" id="Seg_6465" s="T36">np:P</ta>
            <ta e="T40" id="Seg_6466" s="T39">pro.h:A</ta>
            <ta e="T44" id="Seg_6467" s="T43">np:P</ta>
            <ta e="T46" id="Seg_6468" s="T45">0.3.h:A</ta>
            <ta e="T53" id="Seg_6469" s="T52">np:Th</ta>
            <ta e="T57" id="Seg_6470" s="T56">adv:Time</ta>
            <ta e="T58" id="Seg_6471" s="T57">pro.h:A</ta>
            <ta e="T59" id="Seg_6472" s="T58">0.3.h:A</ta>
            <ta e="T60" id="Seg_6473" s="T59">np:Poss 0.3:Poss</ta>
            <ta e="T61" id="Seg_6474" s="T60">np:P</ta>
            <ta e="T65" id="Seg_6475" s="T64">np:P 0.3:Poss</ta>
            <ta e="T67" id="Seg_6476" s="T66">0.3.h:A</ta>
            <ta e="T68" id="Seg_6477" s="T67">adv:Time</ta>
            <ta e="T71" id="Seg_6478" s="T70">np.h:A</ta>
            <ta e="T73" id="Seg_6479" s="T72">np:G 0.3.h:Poss</ta>
            <ta e="T76" id="Seg_6480" s="T75">np.h:A</ta>
            <ta e="T77" id="Seg_6481" s="T76">adv:G</ta>
            <ta e="T82" id="Seg_6482" s="T81">np:P</ta>
            <ta e="T84" id="Seg_6483" s="T83">np.h:A 0.3.h:Poss</ta>
            <ta e="T86" id="Seg_6484" s="T85">adv:G</ta>
            <ta e="T88" id="Seg_6485" s="T87">adv:Time</ta>
            <ta e="T90" id="Seg_6486" s="T89">np.h:P</ta>
            <ta e="T92" id="Seg_6487" s="T91">adv:Time</ta>
            <ta e="T94" id="Seg_6488" s="T93">np:L</ta>
            <ta e="T97" id="Seg_6489" s="T95">0.3:Th</ta>
            <ta e="T98" id="Seg_6490" s="T97">pro.h:P</ta>
            <ta e="T99" id="Seg_6491" s="T98">adv:Time</ta>
            <ta e="T102" id="Seg_6492" s="T101">pro.h:Th</ta>
            <ta e="T104" id="Seg_6493" s="T103">np:Poss</ta>
            <ta e="T105" id="Seg_6494" s="T104">np:L</ta>
            <ta e="T106" id="Seg_6495" s="T105">adv:Time</ta>
            <ta e="T109" id="Seg_6496" s="T108">pro.h:Th</ta>
            <ta e="T110" id="Seg_6497" s="T109">adv:L</ta>
            <ta e="T112" id="Seg_6498" s="T111">adv:Time</ta>
            <ta e="T114" id="Seg_6499" s="T113">0.3.h:P</ta>
            <ta e="T115" id="Seg_6500" s="T114">adv:Time</ta>
            <ta e="T117" id="Seg_6501" s="T116">np:P 0.3.h:Poss</ta>
            <ta e="T118" id="Seg_6502" s="T117">np:P 0.3.h:Poss</ta>
            <ta e="T120" id="Seg_6503" s="T119">0.3.h:P</ta>
            <ta e="T121" id="Seg_6504" s="T120">adv:Time</ta>
            <ta e="T122" id="Seg_6505" s="T121">adv:G</ta>
            <ta e="T123" id="Seg_6506" s="T122">0.3.h:A</ta>
            <ta e="T124" id="Seg_6507" s="T123">np:P 0.3:Poss</ta>
            <ta e="T125" id="Seg_6508" s="T124">np:Ins</ta>
            <ta e="T126" id="Seg_6509" s="T125">0.3.h:A</ta>
            <ta e="T129" id="Seg_6510" s="T128">np:A 0.3.h:Poss</ta>
            <ta e="T130" id="Seg_6511" s="T129">adv:Time</ta>
            <ta e="T132" id="Seg_6512" s="T131">pro.h:Th</ta>
            <ta e="T134" id="Seg_6513" s="T133">adv:L</ta>
            <ta e="T137" id="Seg_6514" s="T136">0.3.h:Th</ta>
            <ta e="T138" id="Seg_6515" s="T137">adv:Time</ta>
            <ta e="T139" id="Seg_6516" s="T138">0.3.h:E</ta>
            <ta e="T142" id="Seg_6517" s="T141">0.3.h:P</ta>
            <ta e="T143" id="Seg_6518" s="T142">np.h:E</ta>
            <ta e="T145" id="Seg_6519" s="T144">pp:L 0.3.h:Poss</ta>
            <ta e="T147" id="Seg_6520" s="T146">0.3.h:Th</ta>
            <ta e="T149" id="Seg_6521" s="T148">np:Poss</ta>
            <ta e="T150" id="Seg_6522" s="T149">np:Th</ta>
            <ta e="T151" id="Seg_6523" s="T150">pp:L 0.3.h:Poss</ta>
            <ta e="T154" id="Seg_6524" s="T153">np:P</ta>
            <ta e="T159" id="Seg_6525" s="T158">np:Time</ta>
            <ta e="T160" id="Seg_6526" s="T159">pro.h:A</ta>
            <ta e="T162" id="Seg_6527" s="T161">pro:Th</ta>
            <ta e="T170" id="Seg_6528" s="T169">np:Com</ta>
            <ta e="T174" id="Seg_6529" s="T173">0.3.h:Th</ta>
            <ta e="T175" id="Seg_6530" s="T174">pro.h:A</ta>
            <ta e="T180" id="Seg_6531" s="T179">0.3.h:Th</ta>
            <ta e="T183" id="Seg_6532" s="T182">np:Th</ta>
            <ta e="T186" id="Seg_6533" s="T185">pp:L 0.3.h:Poss</ta>
            <ta e="T189" id="Seg_6534" s="T188">np:Com</ta>
            <ta e="T191" id="Seg_6535" s="T190">adv:Time</ta>
            <ta e="T194" id="Seg_6536" s="T193">0.3.h:P</ta>
            <ta e="T196" id="Seg_6537" s="T195">pro.h:A</ta>
            <ta e="T198" id="Seg_6538" s="T197">pro:Th</ta>
            <ta e="T200" id="Seg_6539" s="T199">pro.h:R</ta>
            <ta e="T202" id="Seg_6540" s="T201">np.h:Th</ta>
            <ta e="T207" id="Seg_6541" s="T206">adv:Time</ta>
            <ta e="T208" id="Seg_6542" s="T207">np:G</ta>
            <ta e="T209" id="Seg_6543" s="T208">adv:G</ta>
            <ta e="T210" id="Seg_6544" s="T209">0.3.h:A</ta>
            <ta e="T212" id="Seg_6545" s="T211">0.3.h:Poss</ta>
            <ta e="T216" id="Seg_6546" s="T215">pp:G</ta>
            <ta e="T218" id="Seg_6547" s="T217">0.3.h:P</ta>
            <ta e="T220" id="Seg_6548" s="T219">np:Time</ta>
            <ta e="T222" id="Seg_6549" s="T221">0.3:Th</ta>
            <ta e="T224" id="Seg_6550" s="T223">np:Th</ta>
            <ta e="T227" id="Seg_6551" s="T226">np:P</ta>
            <ta e="T229" id="Seg_6552" s="T228">np:Poss</ta>
            <ta e="T230" id="Seg_6553" s="T229">np:L</ta>
            <ta e="T231" id="Seg_6554" s="T230">pro.h:E</ta>
            <ta e="T235" id="Seg_6555" s="T234">adv:Time</ta>
            <ta e="T238" id="Seg_6556" s="T237">0.3.h:A</ta>
            <ta e="T239" id="Seg_6557" s="T238">np:P</ta>
            <ta e="T240" id="Seg_6558" s="T239">adv:L</ta>
            <ta e="T241" id="Seg_6559" s="T240">0.3.h:A</ta>
            <ta e="T242" id="Seg_6560" s="T241">0.3.h:A</ta>
            <ta e="T243" id="Seg_6561" s="T242">adv:Time</ta>
            <ta e="T244" id="Seg_6562" s="T243">np:Poss 0.3.h:Poss</ta>
            <ta e="T245" id="Seg_6563" s="T244">pp:L</ta>
            <ta e="T248" id="Seg_6564" s="T247">adv:G</ta>
            <ta e="T249" id="Seg_6565" s="T248">0.3.h:A</ta>
            <ta e="T250" id="Seg_6566" s="T249">adv:Time</ta>
            <ta e="T252" id="Seg_6567" s="T251">np:G</ta>
            <ta e="T253" id="Seg_6568" s="T252">0.3.h:A</ta>
            <ta e="T254" id="Seg_6569" s="T253">adv:Time</ta>
            <ta e="T255" id="Seg_6570" s="T254">adv:L</ta>
            <ta e="T256" id="Seg_6571" s="T255">0.3.h:Th</ta>
            <ta e="T259" id="Seg_6572" s="T258">pro.h:A</ta>
            <ta e="T262" id="Seg_6573" s="T261">np.h:A</ta>
            <ta e="T263" id="Seg_6574" s="T262">np:Com</ta>
            <ta e="T266" id="Seg_6575" s="T265">adv:G</ta>
            <ta e="T268" id="Seg_6576" s="T267">np:Poss</ta>
            <ta e="T269" id="Seg_6577" s="T268">np:G</ta>
            <ta e="T271" id="Seg_6578" s="T270">adv:G</ta>
            <ta e="T272" id="Seg_6579" s="T271">0.3.h:A</ta>
            <ta e="T273" id="Seg_6580" s="T272">adv:Time</ta>
            <ta e="T274" id="Seg_6581" s="T273">adv:G</ta>
            <ta e="T276" id="Seg_6582" s="T275">0.3.h:Th</ta>
            <ta e="T277" id="Seg_6583" s="T276">pro.h:Th</ta>
            <ta e="T282" id="Seg_6584" s="T281">adv:G</ta>
            <ta e="T284" id="Seg_6585" s="T283">0.3.h:A</ta>
            <ta e="T285" id="Seg_6586" s="T284">np:Th 0.3.h:Poss</ta>
            <ta e="T290" id="Seg_6587" s="T289">pro.h:A</ta>
            <ta e="T293" id="Seg_6588" s="T292">np.h:Th</ta>
            <ta e="T299" id="Seg_6589" s="T298">pp:L</ta>
            <ta e="T302" id="Seg_6590" s="T301">np:Th</ta>
            <ta e="T304" id="Seg_6591" s="T303">0.3.h:A</ta>
            <ta e="T305" id="Seg_6592" s="T304">adv:Time</ta>
            <ta e="T306" id="Seg_6593" s="T305">adv:G</ta>
            <ta e="T309" id="Seg_6594" s="T308">0.3.h:A</ta>
            <ta e="T310" id="Seg_6595" s="T309">adv:L</ta>
            <ta e="T312" id="Seg_6596" s="T311">0.3.h:E</ta>
            <ta e="T314" id="Seg_6597" s="T313">np:Th</ta>
            <ta e="T316" id="Seg_6598" s="T315">adv:Time</ta>
            <ta e="T319" id="Seg_6599" s="T318">0.3.h:P</ta>
            <ta e="T321" id="Seg_6600" s="T320">adv:G</ta>
            <ta e="T322" id="Seg_6601" s="T321">0.1.h:A</ta>
            <ta e="T323" id="Seg_6602" s="T322">adv:Time</ta>
            <ta e="T325" id="Seg_6603" s="T324">np:G</ta>
            <ta e="T328" id="Seg_6604" s="T327">adv:G</ta>
            <ta e="T329" id="Seg_6605" s="T328">0.3.h:A</ta>
            <ta e="T331" id="Seg_6606" s="T330">np:Th</ta>
            <ta e="T333" id="Seg_6607" s="T332">np:Th</ta>
            <ta e="T334" id="Seg_6608" s="T333">np:L</ta>
            <ta e="T336" id="Seg_6609" s="T335">adv:Time</ta>
            <ta e="T337" id="Seg_6610" s="T336">np:G</ta>
            <ta e="T338" id="Seg_6611" s="T337">0.3.h:A</ta>
            <ta e="T339" id="Seg_6612" s="T338">np.h:Poss</ta>
            <ta e="T341" id="Seg_6613" s="T340">np.h:Th</ta>
            <ta e="T343" id="Seg_6614" s="T342">np:Time</ta>
            <ta e="T344" id="Seg_6615" s="T521">np.h:A</ta>
            <ta e="T347" id="Seg_6616" s="T346">pro.h:A</ta>
            <ta e="T349" id="Seg_6617" s="T348">np.h:B</ta>
            <ta e="T353" id="Seg_6618" s="T352">pp.h:Poss</ta>
            <ta e="T357" id="Seg_6619" s="T356">np:Th</ta>
            <ta e="T358" id="Seg_6620" s="T357">pro.h:Poss</ta>
            <ta e="T361" id="Seg_6621" s="T360">np:P</ta>
            <ta e="T362" id="Seg_6622" s="T361">np:A</ta>
            <ta e="T365" id="Seg_6623" s="T364">np.h:A</ta>
            <ta e="T368" id="Seg_6624" s="T367">pro.h:A</ta>
            <ta e="T369" id="Seg_6625" s="T368">adv:G</ta>
            <ta e="T371" id="Seg_6626" s="T370">pro.h:Poss</ta>
            <ta e="T372" id="Seg_6627" s="T371">np:Th</ta>
            <ta e="T374" id="Seg_6628" s="T373">0.3:P</ta>
            <ta e="T375" id="Seg_6629" s="T374">pro.h:A</ta>
            <ta e="T376" id="Seg_6630" s="T375">adv:G</ta>
            <ta e="T378" id="Seg_6631" s="T377">pro.h:A</ta>
            <ta e="T379" id="Seg_6632" s="T378">adv:G</ta>
            <ta e="T383" id="Seg_6633" s="T382">np.h:Th</ta>
            <ta e="T384" id="Seg_6634" s="T383">pro:Th</ta>
            <ta e="T385" id="Seg_6635" s="T384">0.3.h:E</ta>
            <ta e="T386" id="Seg_6636" s="T385">adv:Time</ta>
            <ta e="T387" id="Seg_6637" s="T386">pro.h:P</ta>
            <ta e="T391" id="Seg_6638" s="T390">adv:L</ta>
            <ta e="T393" id="Seg_6639" s="T392">0.3.h:Th</ta>
            <ta e="T395" id="Seg_6640" s="T394">pro.h:A</ta>
            <ta e="T398" id="Seg_6641" s="T397">np:A</ta>
            <ta e="T401" id="Seg_6642" s="T400">adv:L</ta>
            <ta e="T402" id="Seg_6643" s="T401">adv:Time</ta>
            <ta e="T403" id="Seg_6644" s="T402">np.h:Th</ta>
            <ta e="T406" id="Seg_6645" s="T405">np:L</ta>
            <ta e="T408" id="Seg_6646" s="T407">adv:Time</ta>
            <ta e="T410" id="Seg_6647" s="T409">np.h:B</ta>
            <ta e="T412" id="Seg_6648" s="T411">adv:L</ta>
            <ta e="T414" id="Seg_6649" s="T413">0.3.h:Th</ta>
            <ta e="T415" id="Seg_6650" s="T414">adv:Time</ta>
            <ta e="T417" id="Seg_6651" s="T416">np.h:A</ta>
            <ta e="T422" id="Seg_6652" s="T421">0.2.h:A</ta>
            <ta e="T423" id="Seg_6653" s="T422">np:Th</ta>
            <ta e="T425" id="Seg_6654" s="T424">np:A</ta>
            <ta e="T427" id="Seg_6655" s="T426">np:G</ta>
            <ta e="T430" id="Seg_6656" s="T429">pp:L</ta>
            <ta e="T432" id="Seg_6657" s="T431">0.3:Th</ta>
            <ta e="T433" id="Seg_6658" s="T432">adv:Time</ta>
            <ta e="T434" id="Seg_6659" s="T433">pro.h:A</ta>
            <ta e="T435" id="Seg_6660" s="T434">adv:G</ta>
            <ta e="T442" id="Seg_6661" s="T441">0.3.h:P</ta>
            <ta e="T443" id="Seg_6662" s="T442">adv:Time</ta>
            <ta e="T444" id="Seg_6663" s="T443">pro.h:P</ta>
            <ta e="T446" id="Seg_6664" s="T445">np:Time</ta>
            <ta e="T449" id="Seg_6665" s="T448">pro.h:A</ta>
            <ta e="T450" id="Seg_6666" s="T449">0.3:Th</ta>
            <ta e="T452" id="Seg_6667" s="T451">0.3.h:Th</ta>
            <ta e="T453" id="Seg_6668" s="T452">adv:Time</ta>
            <ta e="T454" id="Seg_6669" s="T453">adv:G</ta>
            <ta e="T455" id="Seg_6670" s="T454">0.3.h:A</ta>
            <ta e="T456" id="Seg_6671" s="T455">np:L</ta>
            <ta e="T457" id="Seg_6672" s="T522">np.h:Th</ta>
            <ta e="T459" id="Seg_6673" s="T458">adv:Time</ta>
            <ta e="T460" id="Seg_6674" s="T459">pro.h:Poss</ta>
            <ta e="T461" id="Seg_6675" s="T460">np:Th</ta>
            <ta e="T463" id="Seg_6676" s="T462">np:L</ta>
            <ta e="T465" id="Seg_6677" s="T464">pro.h:Th</ta>
            <ta e="T469" id="Seg_6678" s="T468">np:Com</ta>
            <ta e="T471" id="Seg_6679" s="T470">adv:Time</ta>
            <ta e="T473" id="Seg_6680" s="T472">adv:Time</ta>
            <ta e="T475" id="Seg_6681" s="T474">0.3:Th</ta>
            <ta e="T479" id="Seg_6682" s="T478">0.1.h:Th</ta>
            <ta e="T480" id="Seg_6683" s="T523">np.h:Poss</ta>
            <ta e="T481" id="Seg_6684" s="T480">np:A</ta>
            <ta e="T482" id="Seg_6685" s="T481">np:G</ta>
            <ta e="T485" id="Seg_6686" s="T484">pp:L</ta>
            <ta e="T487" id="Seg_6687" s="T486">adv:L</ta>
            <ta e="T488" id="Seg_6688" s="T487">0.3.h:A</ta>
            <ta e="T489" id="Seg_6689" s="T488">pro.h:A</ta>
            <ta e="T495" id="Seg_6690" s="T494">adv:Time</ta>
            <ta e="T497" id="Seg_6691" s="T496">np:Th</ta>
            <ta e="T500" id="Seg_6692" s="T499">pro.h:A</ta>
            <ta e="T503" id="Seg_6693" s="T502">np:A</ta>
            <ta e="T504" id="Seg_6694" s="T503">np:G</ta>
            <ta e="T507" id="Seg_6695" s="T506">0.3.h:A</ta>
            <ta e="T510" id="Seg_6696" s="T509">0.3.h:A</ta>
            <ta e="T514" id="Seg_6697" s="T513">0.3.h:A</ta>
            <ta e="T515" id="Seg_6698" s="T514">pro.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_6699" s="T2">np.h:S</ta>
            <ta e="T5" id="Seg_6700" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_6701" s="T5">pro.h:S</ta>
            <ta e="T10" id="Seg_6702" s="T9">np:O</ta>
            <ta e="T11" id="Seg_6703" s="T10">v:pred</ta>
            <ta e="T12" id="Seg_6704" s="T519">np.h:S</ta>
            <ta e="T14" id="Seg_6705" s="T13">v:pred</ta>
            <ta e="T17" id="Seg_6706" s="T16">np.h:S</ta>
            <ta e="T18" id="Seg_6707" s="T17">v:pred</ta>
            <ta e="T19" id="Seg_6708" s="T18">np:O</ta>
            <ta e="T20" id="Seg_6709" s="T19">pro.h:S</ta>
            <ta e="T22" id="Seg_6710" s="T21">v:pred</ta>
            <ta e="T28" id="Seg_6711" s="T27">v:pred</ta>
            <ta e="T29" id="Seg_6712" s="T520">np.h:S</ta>
            <ta e="T32" id="Seg_6713" s="T31">np.h:S</ta>
            <ta e="T38" id="Seg_6714" s="T34">s:purp</ta>
            <ta e="T39" id="Seg_6715" s="T38">v:pred</ta>
            <ta e="T40" id="Seg_6716" s="T39">pro.h:S</ta>
            <ta e="T41" id="Seg_6717" s="T40">v:pred</ta>
            <ta e="T44" id="Seg_6718" s="T43">np:O</ta>
            <ta e="T46" id="Seg_6719" s="T45">0.3.h:S v:pred</ta>
            <ta e="T53" id="Seg_6720" s="T52">np:S</ta>
            <ta e="T55" id="Seg_6721" s="T54">adj:pred</ta>
            <ta e="T56" id="Seg_6722" s="T55">cop</ta>
            <ta e="T58" id="Seg_6723" s="T57">pro.h:S</ta>
            <ta e="T59" id="Seg_6724" s="T58">s:temp</ta>
            <ta e="T61" id="Seg_6725" s="T60">np:O</ta>
            <ta e="T63" id="Seg_6726" s="T62">v:pred</ta>
            <ta e="T67" id="Seg_6727" s="T66">0.3.h:S v:pred</ta>
            <ta e="T71" id="Seg_6728" s="T70">np.h:S</ta>
            <ta e="T74" id="Seg_6729" s="T73">v:pred</ta>
            <ta e="T76" id="Seg_6730" s="T75">np.h:S</ta>
            <ta e="T78" id="Seg_6731" s="T77">v:pred</ta>
            <ta e="T83" id="Seg_6732" s="T80">s:rel</ta>
            <ta e="T84" id="Seg_6733" s="T83">np.h:S</ta>
            <ta e="T87" id="Seg_6734" s="T86">v:pred</ta>
            <ta e="T90" id="Seg_6735" s="T89">np.h:S</ta>
            <ta e="T91" id="Seg_6736" s="T90">v:pred</ta>
            <ta e="T95" id="Seg_6737" s="T91">s:temp</ta>
            <ta e="T97" id="Seg_6738" s="T95">0.3:S v:pred</ta>
            <ta e="T98" id="Seg_6739" s="T97">pro.h:S</ta>
            <ta e="T101" id="Seg_6740" s="T100">v:pred</ta>
            <ta e="T102" id="Seg_6741" s="T101">pro.h:S</ta>
            <ta e="T108" id="Seg_6742" s="T107">v:pred</ta>
            <ta e="T109" id="Seg_6743" s="T108">pro.h:S</ta>
            <ta e="T111" id="Seg_6744" s="T110">v:pred</ta>
            <ta e="T114" id="Seg_6745" s="T113">0.3.h:S v:pred</ta>
            <ta e="T116" id="Seg_6746" s="T115">v:pred</ta>
            <ta e="T117" id="Seg_6747" s="T116">np:S</ta>
            <ta e="T118" id="Seg_6748" s="T117">np:S</ta>
            <ta e="T120" id="Seg_6749" s="T119">0.3.h:S v:pred</ta>
            <ta e="T123" id="Seg_6750" s="T122">0.3.h:S v:pred</ta>
            <ta e="T124" id="Seg_6751" s="T123">np:O</ta>
            <ta e="T126" id="Seg_6752" s="T125">0.3.h:S v:pred</ta>
            <ta e="T129" id="Seg_6753" s="T128">np:S</ta>
            <ta e="T131" id="Seg_6754" s="T130">v:pred</ta>
            <ta e="T132" id="Seg_6755" s="T131">pro.h:S</ta>
            <ta e="T135" id="Seg_6756" s="T134">v:pred</ta>
            <ta e="T137" id="Seg_6757" s="T136">0.3.h:S v:pred</ta>
            <ta e="T139" id="Seg_6758" s="T138">0.3.h:S v:pred</ta>
            <ta e="T142" id="Seg_6759" s="T141">0.3.h:S v:pred</ta>
            <ta e="T143" id="Seg_6760" s="T142">np.h:S</ta>
            <ta e="T144" id="Seg_6761" s="T143">v:pred</ta>
            <ta e="T147" id="Seg_6762" s="T146">0.3.h:S v:pred</ta>
            <ta e="T150" id="Seg_6763" s="T149">np:S</ta>
            <ta e="T153" id="Seg_6764" s="T152">v:pred</ta>
            <ta e="T154" id="Seg_6765" s="T153">np:S</ta>
            <ta e="T156" id="Seg_6766" s="T155">v:pred</ta>
            <ta e="T160" id="Seg_6767" s="T159">pro.h:S</ta>
            <ta e="T162" id="Seg_6768" s="T161">pro:O</ta>
            <ta e="T164" id="Seg_6769" s="T163">v:pred</ta>
            <ta e="T171" id="Seg_6770" s="T169">s:adv</ta>
            <ta e="T174" id="Seg_6771" s="T173">0.3.h:S v:pred</ta>
            <ta e="T175" id="Seg_6772" s="T174">pro.h:S</ta>
            <ta e="T178" id="Seg_6773" s="T177">v:pred</ta>
            <ta e="T180" id="Seg_6774" s="T179">0.3.h:S v:pred</ta>
            <ta e="T183" id="Seg_6775" s="T182">np:S</ta>
            <ta e="T184" id="Seg_6776" s="T183">v:pred</ta>
            <ta e="T190" id="Seg_6777" s="T184">s:temp</ta>
            <ta e="T194" id="Seg_6778" s="T193">0.3.h:S v:pred</ta>
            <ta e="T196" id="Seg_6779" s="T195">pro.h:S</ta>
            <ta e="T198" id="Seg_6780" s="T197">pro:O</ta>
            <ta e="T201" id="Seg_6781" s="T200">v:pred</ta>
            <ta e="T202" id="Seg_6782" s="T201">np.h:S</ta>
            <ta e="T205" id="Seg_6783" s="T202">s:adv</ta>
            <ta e="T206" id="Seg_6784" s="T205">v:pred</ta>
            <ta e="T210" id="Seg_6785" s="T209">0.3.h:S v:pred</ta>
            <ta e="T212" id="Seg_6786" s="T211">np:S</ta>
            <ta e="T215" id="Seg_6787" s="T214">v:pred</ta>
            <ta e="T218" id="Seg_6788" s="T217">0.3.h:S v:pred</ta>
            <ta e="T222" id="Seg_6789" s="T221">0.3:S v:pred</ta>
            <ta e="T224" id="Seg_6790" s="T223">np:S</ta>
            <ta e="T225" id="Seg_6791" s="T224">v:pred</ta>
            <ta e="T227" id="Seg_6792" s="T226">np:S</ta>
            <ta e="T228" id="Seg_6793" s="T227">v:pred</ta>
            <ta e="T231" id="Seg_6794" s="T230">pro.h:S</ta>
            <ta e="T234" id="Seg_6795" s="T233">v:pred</ta>
            <ta e="T238" id="Seg_6796" s="T237">0.3.h:S v:pred</ta>
            <ta e="T239" id="Seg_6797" s="T238">np:O</ta>
            <ta e="T241" id="Seg_6798" s="T240">0.3.h:S v:pred</ta>
            <ta e="T242" id="Seg_6799" s="T241">0.3.h:S v:pred</ta>
            <ta e="T247" id="Seg_6800" s="T246">s:adv</ta>
            <ta e="T249" id="Seg_6801" s="T248">0.3.h:S v:pred</ta>
            <ta e="T253" id="Seg_6802" s="T252">0.3.h:S v:pred</ta>
            <ta e="T256" id="Seg_6803" s="T255">0.3.h:S v:pred</ta>
            <ta e="T259" id="Seg_6804" s="T258">pro.h:S</ta>
            <ta e="T261" id="Seg_6805" s="T260">v:pred</ta>
            <ta e="T262" id="Seg_6806" s="T261">np.h:S</ta>
            <ta e="T265" id="Seg_6807" s="T264">v:pred</ta>
            <ta e="T270" id="Seg_6808" s="T266">s:adv</ta>
            <ta e="T272" id="Seg_6809" s="T271">0.3.h:S v:pred</ta>
            <ta e="T275" id="Seg_6810" s="T274">s:adv</ta>
            <ta e="T276" id="Seg_6811" s="T275">0.3.h:S v:pred</ta>
            <ta e="T277" id="Seg_6812" s="T276">pro.h:S</ta>
            <ta e="T280" id="Seg_6813" s="T279">v:pred</ta>
            <ta e="T283" id="Seg_6814" s="T282">s:adv</ta>
            <ta e="T284" id="Seg_6815" s="T283">0.3.h:S v:pred</ta>
            <ta e="T285" id="Seg_6816" s="T284">np:S</ta>
            <ta e="T287" id="Seg_6817" s="T286">v:pred</ta>
            <ta e="T290" id="Seg_6818" s="T289">pro.h:S</ta>
            <ta e="T292" id="Seg_6819" s="T291">v:pred</ta>
            <ta e="T293" id="Seg_6820" s="T292">np.h:S</ta>
            <ta e="T296" id="Seg_6821" s="T295">v:pred</ta>
            <ta e="T297" id="Seg_6822" s="T296">s:adv</ta>
            <ta e="T302" id="Seg_6823" s="T301">np:S</ta>
            <ta e="T304" id="Seg_6824" s="T303">0.3.h:S v:pred</ta>
            <ta e="T307" id="Seg_6825" s="T305">s:temp</ta>
            <ta e="T309" id="Seg_6826" s="T308">0.3.h:S v:pred</ta>
            <ta e="T312" id="Seg_6827" s="T311">0.3.h:S v:pred</ta>
            <ta e="T314" id="Seg_6828" s="T313">np:S</ta>
            <ta e="T315" id="Seg_6829" s="T314">v:pred</ta>
            <ta e="T319" id="Seg_6830" s="T318">0.3.h:S v:pred</ta>
            <ta e="T322" id="Seg_6831" s="T321">0.1.h:S v:pred</ta>
            <ta e="T327" id="Seg_6832" s="T325">s:temp</ta>
            <ta e="T329" id="Seg_6833" s="T328">0.3.h:S v:pred</ta>
            <ta e="T331" id="Seg_6834" s="T330">np:S</ta>
            <ta e="T332" id="Seg_6835" s="T331">v:pred</ta>
            <ta e="T333" id="Seg_6836" s="T332">np:S</ta>
            <ta e="T335" id="Seg_6837" s="T334">v:pred</ta>
            <ta e="T338" id="Seg_6838" s="T337">0.3.h:S v:pred</ta>
            <ta e="T341" id="Seg_6839" s="T340">np.h:S</ta>
            <ta e="T344" id="Seg_6840" s="T521">np.h:S</ta>
            <ta e="T346" id="Seg_6841" s="T345">v:pred</ta>
            <ta e="T347" id="Seg_6842" s="T346">pro.h:S</ta>
            <ta e="T349" id="Seg_6843" s="T348">np.h:O</ta>
            <ta e="T350" id="Seg_6844" s="T349">v:pred</ta>
            <ta e="T357" id="Seg_6845" s="T356">np:S</ta>
            <ta e="T361" id="Seg_6846" s="T360">np:O</ta>
            <ta e="T362" id="Seg_6847" s="T361">np:S</ta>
            <ta e="T364" id="Seg_6848" s="T363">v:pred</ta>
            <ta e="T365" id="Seg_6849" s="T364">np.h:S</ta>
            <ta e="T367" id="Seg_6850" s="T366">v:pred</ta>
            <ta e="T368" id="Seg_6851" s="T367">pro.h:S</ta>
            <ta e="T370" id="Seg_6852" s="T368">s:temp</ta>
            <ta e="T372" id="Seg_6853" s="T371">np:O</ta>
            <ta e="T373" id="Seg_6854" s="T372">v:pred</ta>
            <ta e="T374" id="Seg_6855" s="T373">0.3:S v:pred</ta>
            <ta e="T375" id="Seg_6856" s="T374">pro.h:S</ta>
            <ta e="T377" id="Seg_6857" s="T376">v:pred</ta>
            <ta e="T378" id="Seg_6858" s="T377">pro.h:S</ta>
            <ta e="T380" id="Seg_6859" s="T379">v:pred</ta>
            <ta e="T382" id="Seg_6860" s="T381">v:pred</ta>
            <ta e="T383" id="Seg_6861" s="T382">np.h:S</ta>
            <ta e="T384" id="Seg_6862" s="T383">pro:O</ta>
            <ta e="T385" id="Seg_6863" s="T384">0.3.h:S v:pred</ta>
            <ta e="T387" id="Seg_6864" s="T386">pro.h:S</ta>
            <ta e="T390" id="Seg_6865" s="T389">v:pred</ta>
            <ta e="T393" id="Seg_6866" s="T392">0.3.h:S v:pred</ta>
            <ta e="T397" id="Seg_6867" s="T393">s:temp</ta>
            <ta e="T398" id="Seg_6868" s="T397">np:S</ta>
            <ta e="T400" id="Seg_6869" s="T399">v:pred</ta>
            <ta e="T403" id="Seg_6870" s="T402">np.h:S</ta>
            <ta e="T405" id="Seg_6871" s="T403">s:adv</ta>
            <ta e="T407" id="Seg_6872" s="T406">v:pred</ta>
            <ta e="T411" id="Seg_6873" s="T407">s:temp</ta>
            <ta e="T414" id="Seg_6874" s="T413">0.3.h:S v:pred</ta>
            <ta e="T417" id="Seg_6875" s="T416">np.h:S</ta>
            <ta e="T420" id="Seg_6876" s="T419">v:pred</ta>
            <ta e="T422" id="Seg_6877" s="T421">0.2.h:S v:pred</ta>
            <ta e="T424" id="Seg_6878" s="T422">s:purp</ta>
            <ta e="T425" id="Seg_6879" s="T424">np:S</ta>
            <ta e="T429" id="Seg_6880" s="T428">v:pred</ta>
            <ta e="T432" id="Seg_6881" s="T431">0.3:S v:pred</ta>
            <ta e="T434" id="Seg_6882" s="T433">pro.h:S</ta>
            <ta e="T436" id="Seg_6883" s="T435">v:pred</ta>
            <ta e="T439" id="Seg_6884" s="T436">s:temp</ta>
            <ta e="T442" id="Seg_6885" s="T441">0.3.h:S v:pred</ta>
            <ta e="T444" id="Seg_6886" s="T443">pro.h:S</ta>
            <ta e="T448" id="Seg_6887" s="T447">v:pred</ta>
            <ta e="T449" id="Seg_6888" s="T448">pro.h:S</ta>
            <ta e="T450" id="Seg_6889" s="T449">v:pred 0.3:O</ta>
            <ta e="T452" id="Seg_6890" s="T451">0.3.h:S v:pred</ta>
            <ta e="T455" id="Seg_6891" s="T454">0.3.h:S v:pred</ta>
            <ta e="T457" id="Seg_6892" s="T522">np.h:S</ta>
            <ta e="T458" id="Seg_6893" s="T457">v:pred</ta>
            <ta e="T461" id="Seg_6894" s="T460">np:S</ta>
            <ta e="T464" id="Seg_6895" s="T463">v:pred</ta>
            <ta e="T465" id="Seg_6896" s="T464">pro.h:S</ta>
            <ta e="T470" id="Seg_6897" s="T469">v:pred</ta>
            <ta e="T475" id="Seg_6898" s="T474">0.3:S v:pred</ta>
            <ta e="T479" id="Seg_6899" s="T478">0.1.h:S v:pred</ta>
            <ta e="T481" id="Seg_6900" s="T480">np:S</ta>
            <ta e="T484" id="Seg_6901" s="T483">v:pred</ta>
            <ta e="T488" id="Seg_6902" s="T487">0.3:S v:pred</ta>
            <ta e="T489" id="Seg_6903" s="T488">pro.h:S</ta>
            <ta e="T490" id="Seg_6904" s="T489">s:temp</ta>
            <ta e="T493" id="Seg_6905" s="T492">v:pred</ta>
            <ta e="T497" id="Seg_6906" s="T496">np:S</ta>
            <ta e="T498" id="Seg_6907" s="T497">v:pred</ta>
            <ta e="T500" id="Seg_6908" s="T499">pro.h:S</ta>
            <ta e="T502" id="Seg_6909" s="T501">v:pred</ta>
            <ta e="T503" id="Seg_6910" s="T502">np:S</ta>
            <ta e="T506" id="Seg_6911" s="T505">v:pred</ta>
            <ta e="T507" id="Seg_6912" s="T506">0.3.h:S v:pred</ta>
            <ta e="T509" id="Seg_6913" s="T507">s:temp</ta>
            <ta e="T510" id="Seg_6914" s="T509">0.3.h:S v:pred</ta>
            <ta e="T514" id="Seg_6915" s="T513">0.3.h:S v:pred</ta>
            <ta e="T515" id="Seg_6916" s="T514">pro.h:S</ta>
            <ta e="T518" id="Seg_6917" s="T517">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T11" id="Seg_6918" s="T0">Сын старика-ненца говорит: "Я в этом море белых медведей убью."</ta>
            <ta e="T19" id="Seg_6919" s="T11">Старик-ненец говорит: "Не человек [этого] зверя убивал.</ta>
            <ta e="T24" id="Seg_6920" s="T19">Как ты переедешь через море?</ta>
            <ta e="T29" id="Seg_6921" s="T24">Ненец живёт в безлесной земле."</ta>
            <ta e="T39" id="Seg_6922" s="T29">Трое его братьев поехали через море, чтобы убить белого медведя.</ta>
            <ta e="T41" id="Seg_6923" s="T39">Они поехали.</ta>
            <ta e="T46" id="Seg_6924" s="T41">Этого белого медведя, в медведицу он стрелял.</ta>
            <ta e="T56" id="Seg_6925" s="T46">Потом белого медведя (…), этот белый медведь был большой.</ta>
            <ta e="T63" id="Seg_6926" s="T56">Потом они, убив его, распороли брюхо [внутренности].</ta>
            <ta e="T67" id="Seg_6927" s="T63">И спину распороли на части.</ta>
            <ta e="T74" id="Seg_6928" s="T67">Потом двое братьев загрузили [мясо] на нарты.</ta>
            <ta e="T78" id="Seg_6929" s="T74">Эти братья поехали домой.</ta>
            <ta e="T87" id="Seg_6930" s="T78">Тот их средний брат, убивший белого медведя, [однажды] взял [часть мяса] домой.</ta>
            <ta e="T95" id="Seg_6931" s="T87">Потом этот человек потерялся, [когда] снаружи должен был [снег] таять.</ta>
            <ta e="T97" id="Seg_6932" s="T95">Метель поднялась.</ta>
            <ta e="T101" id="Seg_6933" s="T97">Он совсем обессилел.</ta>
            <ta e="T108" id="Seg_6934" s="T101">Потом он на берегу моря лежал.</ta>
            <ta e="T111" id="Seg_6935" s="T108">Он там лежал.</ta>
            <ta e="T114" id="Seg_6936" s="T111">Потом он совсем обессилел.</ta>
            <ta e="T118" id="Seg_6937" s="T114">Потом растаял его сокуй, бокари.</ta>
            <ta e="T120" id="Seg_6938" s="T118">Он только [тогда] очнулся.</ta>
            <ta e="T126" id="Seg_6939" s="T120">Потом он вперёд отправил [оленей], отрезал ножом поводок в оленьей упряжке.</ta>
            <ta e="T131" id="Seg_6940" s="T126">Три быка-оленя тогда побежали.</ta>
            <ta e="T137" id="Seg_6941" s="T131">Он сам там лежал, остальное [время] спал.</ta>
            <ta e="T141" id="Seg_6942" s="T137">Потом ему так есть захотелось.</ta>
            <ta e="T147" id="Seg_6943" s="T141">Он [то ли] умер, Бог знает, он на нартах спал.</ta>
            <ta e="T153" id="Seg_6944" s="T147">Такой большой кусок льда остался под нартами.</ta>
            <ta e="T156" id="Seg_6945" s="T153">Море всё растаяло.</ta>
            <ta e="T174" id="Seg_6946" s="T156">Один раз кто-то что-то [будто] сказал, то ли бог, то ли человек, пробегая мимо [с мышью?], и так качался.</ta>
            <ta e="T180" id="Seg_6947" s="T174">Он попытался подняться, так сильно качался.</ta>
            <ta e="T184" id="Seg_6948" s="T180">Тучи [из-за горизонта?] показались.</ta>
            <ta e="T190" id="Seg_6949" s="T184">Низ нарт вместе с куском льда показался.</ta>
            <ta e="T194" id="Seg_6950" s="T190">Потом он заснул.</ta>
            <ta e="T206" id="Seg_6951" s="T194">И кто-то что-то [будто] меня (/его) спрашивает: Человек так идёт вниз и качается (?).</ta>
            <ta e="T211" id="Seg_6952" s="T206">Потом он на берег наверх вышел.</ta>
            <ta e="T215" id="Seg_6953" s="T211">Он до смерти есть хотел [глазами еду ищет].</ta>
            <ta e="T218" id="Seg_6954" s="T215">Он в яме застрял.</ta>
            <ta e="T222" id="Seg_6955" s="T218">Однажды так было.</ta>
            <ta e="T230" id="Seg_6956" s="T222">Будто земля виднеется (/будто земля оттаяла) на берегу моря.</ta>
            <ta e="T234" id="Seg_6957" s="T230">Он до смерти есть хотел.</ta>
            <ta e="T242" id="Seg_6958" s="T234">Потом он с трудом так тронулся [с места]: нарты на земле согнул, сдвинулся.</ta>
            <ta e="T249" id="Seg_6959" s="T242">Потом по спинке нарт ползком выбрался на берег.</ta>
            <ta e="T253" id="Seg_6960" s="T249">Потом наверх на берег попал.</ta>
            <ta e="T256" id="Seg_6961" s="T253">Потом там лежал.</ta>
            <ta e="T265" id="Seg_6962" s="T256">Опять кто-то что-то такое сказал: "Человек с мышью вниз спускается".</ta>
            <ta e="T272" id="Seg_6963" s="T265">На берегу по краю островка с тальником он ползком наверх выбрался.</ta>
            <ta e="T276" id="Seg_6964" s="T272">Потом на берег ползком выбрался.</ta>
            <ta e="T281" id="Seg_6965" s="T276">"Я бы на спину вверх встал".</ta>
            <ta e="T284" id="Seg_6966" s="T281">Он ползком на берег выбрался.</ta>
            <ta e="T287" id="Seg_6967" s="T284">Сил всё нет.</ta>
            <ta e="T297" id="Seg_6968" s="T287">Опять кто-то так спросил: "Человек, когда идёт, не так качается. [?]"</ta>
            <ta e="T302" id="Seg_6969" s="T297">Рядом с этим морем высокая земля.</ta>
            <ta e="T304" id="Seg_6970" s="T302">Вверх пополз.</ta>
            <ta e="T309" id="Seg_6971" s="T304">Потом наверх заполз и смотрит. </ta>
            <ta e="T315" id="Seg_6972" s="T309">Наверху он видит: [зимний?] чум из кож стоит.</ta>
            <ta e="T319" id="Seg_6973" s="T315">Потом он опять упал.</ta>
            <ta e="T322" id="Seg_6974" s="T319">"Как я туда дойду?"</ta>
            <ta e="T329" id="Seg_6975" s="T322">Потом, наверху постояв, к чуму вниз пошёл.</ta>
            <ta e="T332" id="Seg_6976" s="T329">Ненецкий чум стоит.</ta>
            <ta e="T335" id="Seg_6977" s="T332">Олени во дворе стали ходить.</ta>
            <ta e="T338" id="Seg_6978" s="T335">Потом он в чум зашёл.</ta>
            <ta e="T341" id="Seg_6979" s="T338">У [ненца]-старика [была] одна дочь.</ta>
            <ta e="T350" id="Seg_6980" s="T341">Однажды старик-ненец говорит [дочери]: "Ты этого человека накорми."</ta>
            <ta e="T357" id="Seg_6981" s="T350">У него, у этого старика, двести оленей [было].</ta>
            <ta e="T367" id="Seg_6982" s="T357">"У меня сто оленей зверь съел", — сказал старик.</ta>
            <ta e="T373" id="Seg_6983" s="T367">"Ты на улицу выйди и моих оленей покарауль."</ta>
            <ta e="T374" id="Seg_6984" s="T373">Стемнело.</ta>
            <ta e="T377" id="Seg_6985" s="T374">Он вышел на улицу.</ta>
            <ta e="T385" id="Seg_6986" s="T377">Он вышел на улицу, такой медлительный, что он там увидит?</ta>
            <ta e="T390" id="Seg_6987" s="T385">Потом он упал.</ta>
            <ta e="T393" id="Seg_6988" s="T390">Там он лежал.</ta>
            <ta e="T400" id="Seg_6989" s="T393">После его прихода зверь ушёл.</ta>
            <ta e="T407" id="Seg_6990" s="T400">Там раньше отец с сыном караулили оленей и жили на улице.</ta>
            <ta e="T414" id="Seg_6991" s="T407">Потом этого человека накормили и он там стал жить.</ta>
            <ta e="T424" id="Seg_6992" s="T414">Потом этот старик снова сказал: "Опять иди покарауль оленей.</ta>
            <ta e="T429" id="Seg_6993" s="T424">Олени, может, в лес не уйдут.</ta>
            <ta e="T432" id="Seg_6994" s="T429">Они около чума спят."</ta>
            <ta e="T442" id="Seg_6995" s="T432">Потом он на улицу вышел, немного пройдя, опять упал.</ta>
            <ta e="T448" id="Seg_6996" s="T442">Потом он когда-то проснулся.</ta>
            <ta e="T450" id="Seg_6997" s="T448">Кто привёл [оленей]?</ta>
            <ta e="T452" id="Seg_6998" s="T450">Видать, он живой был.</ta>
            <ta e="T455" id="Seg_6999" s="T452">Потом он домой пришёл.</ta>
            <ta e="T458" id="Seg_7000" s="T455">В чуме старик-ненец спит.</ta>
            <ta e="T464" id="Seg_7001" s="T458">Потом их олени всегда во дворе спали.</ta>
            <ta e="T470" id="Seg_7002" s="T464">Он жил в той одежде и обуви, в которой пришёл.</ta>
            <ta e="T475" id="Seg_7003" s="T470">Потом на третью ночь так было.</ta>
            <ta e="T479" id="Seg_7004" s="T475">"Мне будто мол хорошо стало."</ta>
            <ta e="T484" id="Seg_7005" s="T479">Олени ненца-старика в лес не уйдут.</ta>
            <ta e="T488" id="Seg_7006" s="T484">Они около чума ходят.</ta>
            <ta e="T493" id="Seg_7007" s="T488">Он пришёл и снова лёг спать.</ta>
            <ta e="T498" id="Seg_7008" s="T493">На следующее утро, глядь, снег [выпал?]. </ta>
            <ta e="T506" id="Seg_7009" s="T498">[Если] я, он пришёл, олени в лес не пойдут. [?]</ta>
            <ta e="T510" id="Seg_7010" s="T506">Повернул [?], когда снег выпал, пошёл.</ta>
            <ta e="T514" id="Seg_7011" s="T510">Хорошо пошёл вниз.</ta>
            <ta e="T518" id="Seg_7012" s="T514">Ему потом хорошо стало.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T11" id="Seg_7013" s="T0">The old Nenets man’s son says: “I’ll kill polar bears [white bears] in this sea.</ta>
            <ta e="T19" id="Seg_7014" s="T11">The old Nenets man says: “It’s not a man who killed [this] beast.</ta>
            <ta e="T24" id="Seg_7015" s="T19">How will you cross the sea?</ta>
            <ta e="T29" id="Seg_7016" s="T24">The Nenets man lives in a forestless land.”</ta>
            <ta e="T39" id="Seg_7017" s="T29">His three brothers crossed the sea in order to kill the polar bear.</ta>
            <ta e="T41" id="Seg_7018" s="T39">They set out.</ta>
            <ta e="T46" id="Seg_7019" s="T41">This polar bear, he was shooting at the female bear.</ta>
            <ta e="T56" id="Seg_7020" s="T46">Then the male polar bear (…), this polar bear was big.</ta>
            <ta e="T63" id="Seg_7021" s="T56">Then after having killed it, they cut open its stomach [innards]. </ta>
            <ta e="T67" id="Seg_7022" s="T63">And they cut its back into pieces.</ta>
            <ta e="T74" id="Seg_7023" s="T67">Then two brothers loaded [the meat] onto the sled.</ta>
            <ta e="T78" id="Seg_7024" s="T74">These brothers went home.</ta>
            <ta e="T87" id="Seg_7025" s="T78">That middle brother of theirs, the one who killed the polar bear, [once] took [part of the meat] home.</ta>
            <ta e="T95" id="Seg_7026" s="T87">Then this man got lost, [when the snow] outside was about to start melting.</ta>
            <ta e="T97" id="Seg_7027" s="T95">A snowstorm started.</ta>
            <ta e="T101" id="Seg_7028" s="T97">He grew quite weak.</ta>
            <ta e="T108" id="Seg_7029" s="T101">Then he lay on the sea shore.</ta>
            <ta e="T111" id="Seg_7030" s="T108">He lay there.</ta>
            <ta e="T114" id="Seg_7031" s="T111">Then he grew quite weak.</ta>
            <ta e="T118" id="Seg_7032" s="T114">Then his fur clothing melted, his shoes.</ta>
            <ta e="T120" id="Seg_7033" s="T118">Only [then] did he come to.</ta>
            <ta e="T126" id="Seg_7034" s="T120">Then he sent [the reindeer] ahead, he cut off the lead in the reindeer harness.</ta>
            <ta e="T131" id="Seg_7035" s="T126">Three reindeer oxen started running then.</ta>
            <ta e="T137" id="Seg_7036" s="T131">He himself lay there, and slept the rest [of the time]. </ta>
            <ta e="T141" id="Seg_7037" s="T137">Then he became very hungry.</ta>
            <ta e="T147" id="Seg_7038" s="T141">He [maybe] died, God knows, he slept on the sled.</ta>
            <ta e="T153" id="Seg_7039" s="T147">Such a big piece of ice remained under the sled.</ta>
            <ta e="T156" id="Seg_7040" s="T153">The whole sea melted.</ta>
            <ta e="T174" id="Seg_7041" s="T156">Once somebody said [something], maybe god, maybe a man, running past [with a mouse?], and swaying like that.</ta>
            <ta e="T180" id="Seg_7042" s="T174">He tried to get up, he was swaying so badly.</ta>
            <ta e="T184" id="Seg_7043" s="T180">Clouds appeared [from behind the horizon?]. </ta>
            <ta e="T190" id="Seg_7044" s="T184">The bottom of the sled and a piece of ice became visible.</ta>
            <ta e="T194" id="Seg_7045" s="T190">Then he fell asleep.</ta>
            <ta e="T206" id="Seg_7046" s="T194">And somebody sort of asked me (/him) something: A man goes down like that and sways (?). </ta>
            <ta e="T211" id="Seg_7047" s="T206">Then he went out to the top of the shore. </ta>
            <ta e="T215" id="Seg_7048" s="T211">He was dying from hunger [looking for food with his eyes]. </ta>
            <ta e="T218" id="Seg_7049" s="T215">He got stuck in a hole in the ground.</ta>
            <ta e="T222" id="Seg_7050" s="T218">It happened once.</ta>
            <ta e="T230" id="Seg_7051" s="T222">As if the ground is visible (/as if the ground melted) on the sea shore.</ta>
            <ta e="T234" id="Seg_7052" s="T230">He was dying from hunger.</ta>
            <ta e="T242" id="Seg_7053" s="T234">Then he hardly set out like this: he bent the sled on the ground, he started moving.</ta>
            <ta e="T249" id="Seg_7054" s="T242">Then on the back of the sled he got out to the shore, crawling.</ta>
            <ta e="T253" id="Seg_7055" s="T249">Then he got to the top of the shore.</ta>
            <ta e="T256" id="Seg_7056" s="T253">Then he lay there.</ta>
            <ta e="T265" id="Seg_7057" s="T256">Again somebody said something like this: “A man and a mouse are going down.”</ta>
            <ta e="T272" id="Seg_7058" s="T265">On the shore, he got to the top by crawling along the small island with purple willow.</ta>
            <ta e="T276" id="Seg_7059" s="T272">Then he got to the shore crawling.</ta>
            <ta e="T281" id="Seg_7060" s="T276">"I would stand up on my back."</ta>
            <ta e="T284" id="Seg_7061" s="T281">He got to the shore crawling.</ta>
            <ta e="T287" id="Seg_7062" s="T284">Still no strength.</ta>
            <ta e="T297" id="Seg_7063" s="T287">Again somebody asked like this: “A man, when walking, doesn’t sway like this. [?]”</ta>
            <ta e="T302" id="Seg_7064" s="T297">Next to this sea there is a high land.</ta>
            <ta e="T304" id="Seg_7065" s="T302">He crawled up.</ta>
            <ta e="T309" id="Seg_7066" s="T304">Then he crawled up and looked.</ta>
            <ta e="T315" id="Seg_7067" s="T309">At the top he sees: [a winter?] tent made of leather is standing there.</ta>
            <ta e="T319" id="Seg_7068" s="T315">Then he fell again. </ta>
            <ta e="T322" id="Seg_7069" s="T319">“How will I go [walk] there?”</ta>
            <ta e="T329" id="Seg_7070" s="T322">Then, having stood for a while up there, he went down to the tent.</ta>
            <ta e="T332" id="Seg_7071" s="T329">A Nenets tent is standing there.</ta>
            <ta e="T335" id="Seg_7072" s="T332">The reindeer started walking around the yard.</ta>
            <ta e="T338" id="Seg_7073" s="T335">Then he went into the tent.</ta>
            <ta e="T341" id="Seg_7074" s="T338">The old [Nenets] man [had] one daughter.</ta>
            <ta e="T350" id="Seg_7075" s="T341">Once the old Nenets man said [to his daughter]: “Give this man something to eat.”</ta>
            <ta e="T357" id="Seg_7076" s="T350">He, this old man, [had] two hundred reindeer.</ta>
            <ta e="T367" id="Seg_7077" s="T357">“A beast ate one hundred of my reindeer”, said the old man.</ta>
            <ta e="T373" id="Seg_7078" s="T367">“You should go outdoors and guard my reindeer.”</ta>
            <ta e="T374" id="Seg_7079" s="T373">It got dark.</ta>
            <ta e="T377" id="Seg_7080" s="T374">He went outdoors.</ta>
            <ta e="T385" id="Seg_7081" s="T377">He went outdoors, he's so slow, what will he see there?</ta>
            <ta e="T390" id="Seg_7082" s="T385">Then he fell.</ta>
            <ta e="T393" id="Seg_7083" s="T390">He lay there.</ta>
            <ta e="T400" id="Seg_7084" s="T393">After he came, the beast left.</ta>
            <ta e="T407" id="Seg_7085" s="T400">Father and son had earlier guarded the reindeer there and lived outdoors.</ta>
            <ta e="T414" id="Seg_7086" s="T407">Then they gave this man something to eat and he began living there.</ta>
            <ta e="T424" id="Seg_7087" s="T414">Then this old man said again: “You should go again and guard the reindeer.”</ta>
            <ta e="T429" id="Seg_7088" s="T424">The reindeer may not go to the forest.</ta>
            <ta e="T432" id="Seg_7089" s="T429">They are sleeping around near the tent.”</ta>
            <ta e="T442" id="Seg_7090" s="T432">Then he went outdoors, and, having taken a few steps, he fell again.</ta>
            <ta e="T448" id="Seg_7091" s="T442">Then he woke up at some point.</ta>
            <ta e="T450" id="Seg_7092" s="T448">Who brought [the reindeer] here?</ta>
            <ta e="T452" id="Seg_7093" s="T450">It looks like he was alive.</ta>
            <ta e="T455" id="Seg_7094" s="T452">Then he came home.</ta>
            <ta e="T458" id="Seg_7095" s="T455">The old Nenets man is sleeping in the tent.</ta>
            <ta e="T464" id="Seg_7096" s="T458">Then their reindeer always slept in the yard.</ta>
            <ta e="T470" id="Seg_7097" s="T464">He wore the clothes and the shoes in which he came.</ta>
            <ta e="T475" id="Seg_7098" s="T470">Then the third night this happened.</ta>
            <ta e="T479" id="Seg_7099" s="T475">“It’s like I got well.”</ta>
            <ta e="T484" id="Seg_7100" s="T479">The old Nenets man’s reindeer will not go to the forest.</ta>
            <ta e="T488" id="Seg_7101" s="T484">They are walking around the tent.</ta>
            <ta e="T493" id="Seg_7102" s="T488">He came back and went to bed again.</ta>
            <ta e="T498" id="Seg_7103" s="T493">The next morning, you looked, and [there was?] snow.</ta>
            <ta e="T506" id="Seg_7104" s="T498">[If] I, he came, the reindeer will not go to the forest. [?]</ta>
            <ta e="T510" id="Seg_7105" s="T506">He turned around [?], when the snow came down, and set out.</ta>
            <ta e="T514" id="Seg_7106" s="T510">He went down fine.</ta>
            <ta e="T518" id="Seg_7107" s="T514">He became well then.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T11" id="Seg_7108" s="T0">Der Sohn des alten nenzischen Mannes sagte: Ich werde die Eisbären in diesem Meer töten.</ta>
            <ta e="T19" id="Seg_7109" s="T11">Der alte nenzische Mann sagt: "Es ist kein Mensch, der dieses Biest tötet.</ta>
            <ta e="T24" id="Seg_7110" s="T19">Wie willst du das Meer überqueren?</ta>
            <ta e="T29" id="Seg_7111" s="T24">Der Nenze lebt in einem waldlosen Land."</ta>
            <ta e="T39" id="Seg_7112" s="T29">Seine drei Brüder überquerten das Meer um den Eisbären zu töten.</ta>
            <ta e="T41" id="Seg_7113" s="T39">Sie fuhren los.</ta>
            <ta e="T46" id="Seg_7114" s="T41">Diesen Eisbärn, er schoß auf eine Bärin.</ta>
            <ta e="T56" id="Seg_7115" s="T46">Dann (…) der Eisbär, dieser Eisbär war groß.</ta>
            <ta e="T63" id="Seg_7116" s="T56">Nachdem sie ihn umgebracht hatten, öffneten sie seinen Bauch. </ta>
            <ta e="T67" id="Seg_7117" s="T63">Und sie schnitten seinen Rücken in Teile. </ta>
            <ta e="T74" id="Seg_7118" s="T67">Dann luden zwei Brüder [das Fleisch] auf den Schlitten.</ta>
            <ta e="T78" id="Seg_7119" s="T74">Diese Brüder gingen nach Hause.</ta>
            <ta e="T87" id="Seg_7120" s="T78">Der mittlere Bruder von ihnen, der, der den Eisbären getötet hatte, nahm [Teile des Fleischs] mit nach Hause. </ta>
            <ta e="T95" id="Seg_7121" s="T87">Dann verlief sich dieser Mann, [als der Schnee] draußen zu schmelzen anfing.</ta>
            <ta e="T97" id="Seg_7122" s="T95">Ein Schneesturm zog auf.</ta>
            <ta e="T101" id="Seg_7123" s="T97">Er wurde ganz schwach.</ta>
            <ta e="T108" id="Seg_7124" s="T101">Dann lag er an der Küste.</ta>
            <ta e="T111" id="Seg_7125" s="T108">Er lag dort.</ta>
            <ta e="T114" id="Seg_7126" s="T111">Dann wurde er ganz schwach.</ta>
            <ta e="T118" id="Seg_7127" s="T114">Dann schmolz seine Pelzkleidung, seine Schuhe.</ta>
            <ta e="T120" id="Seg_7128" s="T118">Erst dann erwachte er.</ta>
            <ta e="T126" id="Seg_7129" s="T120">Dann schickte er die Rentiere vor, er schnitt ihre Zügel mit einem Messer durch.</ta>
            <ta e="T131" id="Seg_7130" s="T126">Drei Rentierbullen fingen an zu rennen.</ta>
            <ta e="T137" id="Seg_7131" s="T131">Er selbst lag dort und schlief den Rest [der Zeit].</ta>
            <ta e="T141" id="Seg_7132" s="T137">Dann wurde er sehr hungrig.</ta>
            <ta e="T147" id="Seg_7133" s="T141">Er starb [vielleicht], Gott weiß, er schlief auf dem Schlitten.</ta>
            <ta e="T153" id="Seg_7134" s="T147">So ein großes Stück Eis blieb unter dem Schlitten.</ta>
            <ta e="T156" id="Seg_7135" s="T153">Das ganze Meer schmolz.</ta>
            <ta e="T174" id="Seg_7136" s="T156">Einmal sagte jemand [etwas], vielleicht Gott, vielleicht ein Mensch, vorbeirennend [mit einer Maus?] und so schwankend.</ta>
            <ta e="T180" id="Seg_7137" s="T174">Er versuchte aufzustehen, schwankte so schlimm.</ta>
            <ta e="T184" id="Seg_7138" s="T180">Wolken erschienen [hinterm Horizont?].</ta>
            <ta e="T190" id="Seg_7139" s="T184">Die Unterseite des Schlittens und ein Stück Eis wurden sichtbar.</ta>
            <ta e="T194" id="Seg_7140" s="T190">Dann schlief er ein.</ta>
            <ta e="T206" id="Seg_7141" s="T194">Und jemand fragte mich (/ihn) etwas: Ein Mann geht so unter und schwankt (?).</ta>
            <ta e="T211" id="Seg_7142" s="T206">Dann ging er hoch zum Ufer.</ta>
            <ta e="T215" id="Seg_7143" s="T211">Er starb vor Hunger [suchte mit seinen Augen nach Essen].</ta>
            <ta e="T218" id="Seg_7144" s="T215">Er blieb in einem Loch im Boden stecken.</ta>
            <ta e="T222" id="Seg_7145" s="T218">Es passierte einmal.</ta>
            <ta e="T230" id="Seg_7146" s="T222">Als wäre der Boden sichtbar (/als wäre der Boden geschmolzen) an der Küste.</ta>
            <ta e="T234" id="Seg_7147" s="T230">Er war sehr hungrig.</ta>
            <ta e="T242" id="Seg_7148" s="T234">Dann bewegte er sich kaum: er krümmte den Schlitten auf dem Boden, er begann sich zu bewegen.</ta>
            <ta e="T249" id="Seg_7149" s="T242">Dann auf dem Rücken des Schlittens kroch er auf die Küste zu.</ta>
            <ta e="T253" id="Seg_7150" s="T249">Dann kam er ans Ufer.</ta>
            <ta e="T256" id="Seg_7151" s="T253">Dann lag er dort.</ta>
            <ta e="T265" id="Seg_7152" s="T256">Wieder sagte jemand etwas wie: "Ein Mann und eine Maus gehen unter."</ta>
            <ta e="T272" id="Seg_7153" s="T265">An der Küste kam er nach oben kriechend am Rand der kleinen Insel mit roter Weide.</ta>
            <ta e="T276" id="Seg_7154" s="T272">Dann kam er kriechend zur Küste.</ta>
            <ta e="T281" id="Seg_7155" s="T276">"Ich würde auf meinem Rücken stehen."</ta>
            <ta e="T284" id="Seg_7156" s="T281">Er kam kriechend zur Küste.</ta>
            <ta e="T287" id="Seg_7157" s="T284">Immer noch keine Kraft.</ta>
            <ta e="T297" id="Seg_7158" s="T287">Wieder fragte jemand etwas wie: "Ein Mann, der läuft, schwankt nicht so. [?]"</ta>
            <ta e="T302" id="Seg_7159" s="T297">Neben dem Meer ist hohes Land.</ta>
            <ta e="T304" id="Seg_7160" s="T302">Er kroch hoch.</ta>
            <ta e="T309" id="Seg_7161" s="T304">Dann kroch er hoch und sah sich um.</ta>
            <ta e="T315" id="Seg_7162" s="T309">Oben sieht er: ein [Winter-?]zelt aus Leder steht dort.</ta>
            <ta e="T319" id="Seg_7163" s="T315">Dann fiel er wieder.</ta>
            <ta e="T322" id="Seg_7164" s="T319">"Wie soll ich dorthin laufen?"</ta>
            <ta e="T329" id="Seg_7165" s="T322">Dann, nachdem er eine Weile dort oben gestanden hatte, ging er hinunter zum Zelt.</ta>
            <ta e="T332" id="Seg_7166" s="T329">Ein nenzisches Zelt steht dort.</ta>
            <ta e="T335" id="Seg_7167" s="T332">Die Rentiere fingen an im Garten herumzulaufen.</ta>
            <ta e="T338" id="Seg_7168" s="T335">Dann ging er ins Zelt.</ta>
            <ta e="T341" id="Seg_7169" s="T338">Der alte [Nenze] [hatte] eine Tochter.</ta>
            <ta e="T350" id="Seg_7170" s="T341">Einmal sagte der alte Nenze [zu seiner Tochter]: "Gib diesem Mann etwas zu essen."</ta>
            <ta e="T357" id="Seg_7171" s="T350">Er, dieser alte Mann, [hatte] zweihundert Rentiere.</ta>
            <ta e="T367" id="Seg_7172" s="T357">"Ein Biest aß hunderte meiner Rentiere", sagte der alte Mann.</ta>
            <ta e="T373" id="Seg_7173" s="T367">"Du solltest rausgehen und meine Rentiere bewachen."</ta>
            <ta e="T374" id="Seg_7174" s="T373">Es wurde dunkel.</ta>
            <ta e="T377" id="Seg_7175" s="T374">Er ging raus.</ta>
            <ta e="T385" id="Seg_7176" s="T377">Er ging raus, er ist so langsam, was wird er dort sehen?</ta>
            <ta e="T390" id="Seg_7177" s="T385">Dann fiel er.</ta>
            <ta e="T393" id="Seg_7178" s="T390">Er lag dort.</ta>
            <ta e="T400" id="Seg_7179" s="T393">Nachdem er gekommen war, ist das Biest gegangen.</ta>
            <ta e="T407" id="Seg_7180" s="T400">Vater und Sohn hatten früher die Rentiere dort bewacht und draußen gelebt.</ta>
            <ta e="T414" id="Seg_7181" s="T407">Dann gaben sie diesem Mann etwas zu essen und er begann dort zu leben.</ta>
            <ta e="T424" id="Seg_7182" s="T414">Dann sagte der alte Mann wieder: "Du solltest wieder gehen und die Rentiere bewachen."</ta>
            <ta e="T429" id="Seg_7183" s="T424">Die Rentiere sollen nicht in den Wald gehen.</ta>
            <ta e="T432" id="Seg_7184" s="T429">Sie schlafen in der Nähe des Zeltes.</ta>
            <ta e="T442" id="Seg_7185" s="T432">Dann ging er raus und, nach ein paar Schritten, fiel er wieder.</ta>
            <ta e="T448" id="Seg_7186" s="T442">Dann wachte er irgendwann auf.</ta>
            <ta e="T450" id="Seg_7187" s="T448">Wer brachte [die Rentiere] hierher?</ta>
            <ta e="T452" id="Seg_7188" s="T450">Offenbar war er am Leben.</ta>
            <ta e="T455" id="Seg_7189" s="T452">Dann kam er nach Hause.</ta>
            <ta e="T458" id="Seg_7190" s="T455">Der alte nenzische Mann schläft im Zelt.</ta>
            <ta e="T464" id="Seg_7191" s="T458">Dann schliefen ihre Rentiere immer im Garten.</ta>
            <ta e="T470" id="Seg_7192" s="T464">Er trug die Kleidung und die Schuhe, in denen er gekommen war.</ta>
            <ta e="T475" id="Seg_7193" s="T470">Dann kam die dritte Nacht.</ta>
            <ta e="T479" id="Seg_7194" s="T475">"Offenbar bin ich gesund geworden."</ta>
            <ta e="T484" id="Seg_7195" s="T479">Die Rentiere des alten nenzischen Mannes werden nicht in den Wald gehen.</ta>
            <ta e="T488" id="Seg_7196" s="T484">Sie laufen um das Zelt herum.</ta>
            <ta e="T493" id="Seg_7197" s="T488">Er kam zurück und ging wieder ins Bett.</ta>
            <ta e="T498" id="Seg_7198" s="T493">Am nächsten Morgen, schau, der Schnee [ist gefallen?].</ta>
            <ta e="T506" id="Seg_7199" s="T498">Falls ich, er kam, werden die Rentiere nicht in den Wald gehen. [?]</ta>
            <ta e="T510" id="Seg_7200" s="T506">Er drehte sich um [?], als der Schnee fiel, und ging los.</ta>
            <ta e="T514" id="Seg_7201" s="T510">Er kam gut hinunter.</ta>
            <ta e="T518" id="Seg_7202" s="T514">Er wurde dann gesund.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T11" id="Seg_7203" s="T0">ненецкий сын так говорит я на море белого медведя убил</ta>
            <ta e="T19" id="Seg_7204" s="T11">старик-ненец так сказал: не человек этого зверя убил</ta>
            <ta e="T24" id="Seg_7205" s="T19">как ты поедешь на море</ta>
            <ta e="T29" id="Seg_7206" s="T24">ненец живёт на лесной земле</ta>
            <ta e="T39" id="Seg_7207" s="T29">у него трое братьев белого медведя убить поехали</ta>
            <ta e="T41" id="Seg_7208" s="T39">они убили</ta>
            <ta e="T46" id="Seg_7209" s="T41">этого белого медведя матери бросили</ta>
            <ta e="T56" id="Seg_7210" s="T46">потом белого медведя. этот белый медведь большой был</ta>
            <ta e="T63" id="Seg_7211" s="T56">потом они после убийства распороли брюхо (= брюхо кровь сердце распороли)</ta>
            <ta e="T67" id="Seg_7212" s="T63">спину распороли на две части</ta>
            <ta e="T74" id="Seg_7213" s="T67">потом двое братьев загрузили на нарты</ta>
            <ta e="T78" id="Seg_7214" s="T74">эти братья поехали домой на нартах</ta>
            <ta e="T87" id="Seg_7215" s="T78">половину белого медведя убивший средний брат домой взял</ta>
            <ta e="T95" id="Seg_7216" s="T87">потом этот человек заблудился, когда тает</ta>
            <ta e="T97" id="Seg_7217" s="T95">снег пошёл</ta>
            <ta e="T101" id="Seg_7218" s="T97">он совсем обессилел</ta>
            <ta e="T108" id="Seg_7219" s="T101">он около моря там и лежал</ta>
            <ta e="T111" id="Seg_7220" s="T108">он там лежал</ta>
            <ta e="T114" id="Seg_7221" s="T111">совсем обессилел</ta>
            <ta e="T118" id="Seg_7222" s="T114">снег растаял, сокуй</ta>
            <ta e="T120" id="Seg_7223" s="T118">и носок обуви</ta>
            <ta e="T126" id="Seg_7224" s="T120">потом вперёд наклонившись кожаную юфть отрезал от упряжки ножом</ta>
            <ta e="T131" id="Seg_7225" s="T126">три оленя вперед убежали</ta>
            <ta e="T137" id="Seg_7226" s="T131">он сам там лежал засыпал</ta>
            <ta e="T141" id="Seg_7227" s="T137">потом его трясет так есть хочет</ta>
            <ta e="T147" id="Seg_7228" s="T141">он спит на нартах</ta>
            <ta e="T153" id="Seg_7229" s="T147">такой большой лёд, он под нартами остался / шуршал </ta>
            <ta e="T156" id="Seg_7230" s="T153"> море всё растаяло</ta>
            <ta e="T174" id="Seg_7231" s="T156">человек? однажды что-то сказал, то ли Бог, то ли человек, то ли человек с мышью пробежала, или так качается</ta>
            <ta e="T180" id="Seg_7232" s="T174">он наверх (сесть) попытался, так качается</ta>
            <ta e="T184" id="Seg_7233" s="T180">туча едва показалась</ta>
            <ta e="T190" id="Seg_7234" s="T184">под нартами лёд плавает</ta>
            <ta e="T194" id="Seg_7235" s="T190">потом спать лёг</ta>
            <ta e="T206" id="Seg_7236" s="T194">не знает, кто меня спрашивает. человек так повернувшись качается</ta>
            <ta e="T211" id="Seg_7237" s="T206">потом на берег вышел</ta>
            <ta e="T215" id="Seg_7238" s="T211">глаза так кушать хотят</ta>
            <ta e="T218" id="Seg_7239" s="T215">в груди торчит</ta>
            <ta e="T222" id="Seg_7240" s="T218">однажды так было</ta>
            <ta e="T230" id="Seg_7241" s="T222">будто земля виднеется , будто земля плачет, на берегу моря</ta>
            <ta e="T234" id="Seg_7242" s="T230">он сильно исхудал</ta>
            <ta e="T242" id="Seg_7243" s="T234">потом он пошёл к нартам, домой наконец вернулся</ta>
            <ta e="T249" id="Seg_7244" s="T242">потом по спинке нарты ползком вышел на берег</ta>
            <ta e="T253" id="Seg_7245" s="T249">он на берег выпал</ta>
            <ta e="T256" id="Seg_7246" s="T253">потом там лежит</ta>
            <ta e="T265" id="Seg_7247" s="T256">кто-то ему так сказал, человек бежал</ta>
            <ta e="T272" id="Seg_7248" s="T265">на берег до кустарника тальника ползком добрался</ta>
            <ta e="T276" id="Seg_7249" s="T272">потом на берег ползком вышел</ta>
            <ta e="T281" id="Seg_7250" s="T276">стану (на ноги) / я назад повернулся</ta>
            <ta e="T284" id="Seg_7251" s="T281">на берег выполз</ta>
            <ta e="T287" id="Seg_7252" s="T284">силы всё нет</ta>
            <ta e="T297" id="Seg_7253" s="T287">кто-то так спрашивает ты не туда пошёл</ta>
            <ta e="T302" id="Seg_7254" s="T297">вот возле моря высокий берег</ta>
            <ta e="T304" id="Seg_7255" s="T302">вверх залез [поднялся]</ta>
            <ta e="T309" id="Seg_7256" s="T304">забравшись так смотрит</ta>
            <ta e="T315" id="Seg_7257" s="T309">так видит зимний чум стоит / на берегу видит. чум из кож (старый чум, кожа облысела) стоит</ta>
            <ta e="T319" id="Seg_7258" s="T315">опять обессилел / потом он прилёг</ta>
            <ta e="T322" id="Seg_7259" s="T319">как я туда дойду</ta>
            <ta e="T329" id="Seg_7260" s="T322">вот сверху (вниз) постоял пришёл</ta>
            <ta e="T332" id="Seg_7261" s="T329">ненецкий чум стоит</ta>
            <ta e="T335" id="Seg_7262" s="T332">ходят / возле чума ходят олени</ta>
            <ta e="T338" id="Seg_7263" s="T335">в чум зашла</ta>
            <ta e="T341" id="Seg_7264" s="T338">у ненца старика одна дочь</ta>
            <ta e="T350" id="Seg_7265" s="T341">однажды с ненцем стариком так стало "Ты этого гостя накорми" (старик говорит дочери)</ta>
            <ta e="T357" id="Seg_7266" s="T350">У этого старика было двести голов оленей</ta>
            <ta e="T367" id="Seg_7267" s="T357">"Моих сто оленей волки (звери) всех поранили (съели)". Старик так сказал</ta>
            <ta e="T373" id="Seg_7268" s="T367">я их оленей караулил / "Ты на улицу когда пойдёшь, моих оленей покарауль".</ta>
            <ta e="T374" id="Seg_7269" s="T373">Стемнело</ta>
            <ta e="T377" id="Seg_7270" s="T374">он на улицу вышел</ta>
            <ta e="T385" id="Seg_7271" s="T377">Он вышел на улицу, так он обессилел, что он там увидит (будет смотреть)</ta>
            <ta e="T390" id="Seg_7272" s="T385">Потом он на землю упал</ta>
            <ta e="T393" id="Seg_7273" s="T390">Там он лежал</ta>
            <ta e="T400" id="Seg_7274" s="T393">после его прихода звери ушли</ta>
            <ta e="T407" id="Seg_7275" s="T400">Давно отец с сыном караулили оленей и жили на улице</ta>
            <ta e="T414" id="Seg_7276" s="T407">Этот человек там поел и стал жить </ta>
            <ta e="T424" id="Seg_7277" s="T414">Потом этот старик так сказал: "Иди оленей карауль</ta>
            <ta e="T429" id="Seg_7278" s="T424">олени в лес чтобы не убежали</ta>
            <ta e="T432" id="Seg_7279" s="T429">(они) возле дома спят</ta>
            <ta e="T442" id="Seg_7280" s="T432">Он на улицу вышел, немного прошёл и опять лёг</ta>
            <ta e="T448" id="Seg_7281" s="T442">Вдруг он проснулся</ta>
            <ta e="T450" id="Seg_7282" s="T448">Стало светать</ta>
            <ta e="T455" id="Seg_7283" s="T452">Он домой пришёл</ta>
            <ta e="T458" id="Seg_7284" s="T455">Дома спит ненец старик</ta>
            <ta e="T464" id="Seg_7285" s="T458">Их олени всегда во дворе (на территории) спали</ta>
            <ta e="T470" id="Seg_7286" s="T464">Он жил в той одежде и обуви, в которой пришёл</ta>
            <ta e="T475" id="Seg_7287" s="T470">На третью ночь ему так было</ta>
            <ta e="T479" id="Seg_7288" s="T475">Будто ему хорошо стало</ta>
            <ta e="T484" id="Seg_7289" s="T479">Ненца старика олени не хотят идти в лес</ta>
            <ta e="T488" id="Seg_7290" s="T484">Возде дверей чума они спят</ta>
            <ta e="T493" id="Seg_7291" s="T488">Он пришёл и спать лёг </ta>
            <ta e="T498" id="Seg_7292" s="T493">на следующее утро в чуме снегом изнутри сверху намело</ta>
            <ta e="T506" id="Seg_7293" s="T498">"Я как пришёл, олени не пошли в лес опять</ta>
            <ta e="T510" id="Seg_7294" s="T506">"Поворачивай!" (он кричит), как снег выпал, (так они) поехали</ta>
            <ta e="T514" id="Seg_7295" s="T510">Хорошо вниз поехали</ta>
            <ta e="T518" id="Seg_7296" s="T514">ему стало хорошо</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T11" id="Seg_7297" s="T0">[OSV:] The verbal form "qət-ta-m" (kill-FUT-1SG.S) would be more correct here.</ta>
            <ta e="T39" id="Seg_7298" s="T29">[OSV:] The nominal form "porqɨp" has been edited into "qorqɨp".</ta>
            <ta e="T87" id="Seg_7299" s="T78">‎[OSV:] The verbal form "ɛppa" has been edited into "ippa".</ta>
            <ta e="T101" id="Seg_7300" s="T97">[OSV:] The verbal form "orɨmpɨlʼɨmpa" has been edited into "orɨmkɨlɨmpa".</ta>
            <ta e="T111" id="Seg_7301" s="T108">[OSV:] The verbal form "ippɨmna" has been edited into "ippɨmpa".</ta>
            <ta e="T118" id="Seg_7302" s="T114">[OSV:] "Nop" has been edited into "nɔːt".</ta>
            <ta e="T153" id="Seg_7303" s="T147">[OSV:] The postpositional form "ɨqɨt" has been edited into "ɨlqɨt".</ta>
            <ta e="T190" id="Seg_7304" s="T184">[OSV:] The nominal form "ukalʼ" has been edited into "ulqalʼ".</ta>
            <ta e="T206" id="Seg_7305" s="T194">[OSV:] The speaker alternates between the first and the third person singular denoting the protagonist.</ta>
            <ta e="T215" id="Seg_7306" s="T211">[OSV:] "aps(ɨt)qo quqo" - "to be famished".</ta>
            <ta e="T234" id="Seg_7307" s="T230">[OSV:] "aps(ɨt)qo quqo" - "to be famished".</ta>
            <ta e="T281" id="Seg_7308" s="T276">[OSV:] The speaker alternates between third and first person singular denoting the protagonist.</ta>
            <ta e="T385" id="Seg_7309" s="T377">[OSV:] The participle form "seqelʼimpɨlʼ" would be more correct here.</ta>
            <ta e="T407" id="Seg_7310" s="T400">[OSV:] The nominal form "ɔːtamtij" has been edited into "ɔːtantɨj".</ta>
            <ta e="T458" id="Seg_7311" s="T455">[OSV:] The nominal form "mɔːt-qɨn" (tent-LOC) would be here more correct.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T96" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T519" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T520" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T521" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T522" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T523" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
