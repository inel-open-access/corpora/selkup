<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>NEP_1965_OrphanBoyAndPanOldMan_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">NEP_1965_OrphanBoyAndPanOldMan1_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">862</ud-information>
            <ud-information attribute-name="# HIAT:w">694</ud-information>
            <ud-information attribute-name="# e">694</ud-information>
            <ud-information attribute-name="# HIAT:u">137</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="NEP">
            <abbreviation>NEP</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T693" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T694" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T695" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T691" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T692" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
         <tli id="T204" />
         <tli id="T205" />
         <tli id="T206" />
         <tli id="T207" />
         <tli id="T208" />
         <tli id="T209" />
         <tli id="T210" />
         <tli id="T211" />
         <tli id="T212" />
         <tli id="T213" />
         <tli id="T214" />
         <tli id="T215" />
         <tli id="T216" />
         <tli id="T217" />
         <tli id="T218" />
         <tli id="T219" />
         <tli id="T220" />
         <tli id="T221" />
         <tli id="T222" />
         <tli id="T223" />
         <tli id="T224" />
         <tli id="T225" />
         <tli id="T226" />
         <tli id="T227" />
         <tli id="T228" />
         <tli id="T229" />
         <tli id="T230" />
         <tli id="T231" />
         <tli id="T232" />
         <tli id="T233" />
         <tli id="T234" />
         <tli id="T235" />
         <tli id="T236" />
         <tli id="T237" />
         <tli id="T238" />
         <tli id="T239" />
         <tli id="T240" />
         <tli id="T241" />
         <tli id="T242" />
         <tli id="T243" />
         <tli id="T244" />
         <tli id="T245" />
         <tli id="T246" />
         <tli id="T247" />
         <tli id="T248" />
         <tli id="T249" />
         <tli id="T250" />
         <tli id="T251" />
         <tli id="T252" />
         <tli id="T253" />
         <tli id="T254" />
         <tli id="T255" />
         <tli id="T256" />
         <tli id="T257" />
         <tli id="T258" />
         <tli id="T259" />
         <tli id="T260" />
         <tli id="T261" />
         <tli id="T262" />
         <tli id="T263" />
         <tli id="T264" />
         <tli id="T265" />
         <tli id="T266" />
         <tli id="T267" />
         <tli id="T268" />
         <tli id="T269" />
         <tli id="T270" />
         <tli id="T271" />
         <tli id="T272" />
         <tli id="T273" />
         <tli id="T274" />
         <tli id="T275" />
         <tli id="T276" />
         <tli id="T277" />
         <tli id="T278" />
         <tli id="T279" />
         <tli id="T280" />
         <tli id="T281" />
         <tli id="T282" />
         <tli id="T283" />
         <tli id="T284" />
         <tli id="T285" />
         <tli id="T286" />
         <tli id="T287" />
         <tli id="T289" />
         <tli id="T290" />
         <tli id="T291" />
         <tli id="T292" />
         <tli id="T293" />
         <tli id="T294" />
         <tli id="T295" />
         <tli id="T296" />
         <tli id="T297" />
         <tli id="T298" />
         <tli id="T299" />
         <tli id="T300" />
         <tli id="T301" />
         <tli id="T302" />
         <tli id="T303" />
         <tli id="T304" />
         <tli id="T305" />
         <tli id="T306" />
         <tli id="T307" />
         <tli id="T308" />
         <tli id="T309" />
         <tli id="T310" />
         <tli id="T311" />
         <tli id="T312" />
         <tli id="T313" />
         <tli id="T314" />
         <tli id="T315" />
         <tli id="T316" />
         <tli id="T317" />
         <tli id="T318" />
         <tli id="T319" />
         <tli id="T320" />
         <tli id="T321" />
         <tli id="T322" />
         <tli id="T323" />
         <tli id="T324" />
         <tli id="T325" />
         <tli id="T326" />
         <tli id="T327" />
         <tli id="T328" />
         <tli id="T329" />
         <tli id="T330" />
         <tli id="T331" />
         <tli id="T332" />
         <tli id="T333" />
         <tli id="T334" />
         <tli id="T335" />
         <tli id="T336" />
         <tli id="T337" />
         <tli id="T338" />
         <tli id="T339" />
         <tli id="T340" />
         <tli id="T341" />
         <tli id="T342" />
         <tli id="T343" />
         <tli id="T344" />
         <tli id="T345" />
         <tli id="T346" />
         <tli id="T347" />
         <tli id="T348" />
         <tli id="T349" />
         <tli id="T350" />
         <tli id="T351" />
         <tli id="T352" />
         <tli id="T353" />
         <tli id="T354" />
         <tli id="T355" />
         <tli id="T356" />
         <tli id="T357" />
         <tli id="T358" />
         <tli id="T359" />
         <tli id="T360" />
         <tli id="T361" />
         <tli id="T362" />
         <tli id="T363" />
         <tli id="T364" />
         <tli id="T365" />
         <tli id="T366" />
         <tli id="T367" />
         <tli id="T368" />
         <tli id="T369" />
         <tli id="T370" />
         <tli id="T371" />
         <tli id="T372" />
         <tli id="T373" />
         <tli id="T374" />
         <tli id="T375" />
         <tli id="T376" />
         <tli id="T377" />
         <tli id="T378" />
         <tli id="T379" />
         <tli id="T380" />
         <tli id="T381" />
         <tli id="T382" />
         <tli id="T383" />
         <tli id="T384" />
         <tli id="T385" />
         <tli id="T386" />
         <tli id="T387" />
         <tli id="T388" />
         <tli id="T389" />
         <tli id="T390" />
         <tli id="T391" />
         <tli id="T392" />
         <tli id="T393" />
         <tli id="T394" />
         <tli id="T395" />
         <tli id="T396" />
         <tli id="T397" />
         <tli id="T398" />
         <tli id="T399" />
         <tli id="T400" />
         <tli id="T401" />
         <tli id="T402" />
         <tli id="T403" />
         <tli id="T404" />
         <tli id="T405" />
         <tli id="T406" />
         <tli id="T407" />
         <tli id="T408" />
         <tli id="T409" />
         <tli id="T410" />
         <tli id="T411" />
         <tli id="T412" />
         <tli id="T413" />
         <tli id="T414" />
         <tli id="T415" />
         <tli id="T416" />
         <tli id="T417" />
         <tli id="T418" />
         <tli id="T419" />
         <tli id="T420" />
         <tli id="T421" />
         <tli id="T422" />
         <tli id="T423" />
         <tli id="T424" />
         <tli id="T425" />
         <tli id="T426" />
         <tli id="T427" />
         <tli id="T428" />
         <tli id="T429" />
         <tli id="T430" />
         <tli id="T431" />
         <tli id="T432" />
         <tli id="T433" />
         <tli id="T434" />
         <tli id="T435" />
         <tli id="T436" />
         <tli id="T437" />
         <tli id="T438" />
         <tli id="T439" />
         <tli id="T440" />
         <tli id="T441" />
         <tli id="T442" />
         <tli id="T443" />
         <tli id="T444" />
         <tli id="T445" />
         <tli id="T446" />
         <tli id="T447" />
         <tli id="T448" />
         <tli id="T449" />
         <tli id="T450" />
         <tli id="T451" />
         <tli id="T452" />
         <tli id="T453" />
         <tli id="T454" />
         <tli id="T455" />
         <tli id="T456" />
         <tli id="T457" />
         <tli id="T458" />
         <tli id="T459" />
         <tli id="T460" />
         <tli id="T461" />
         <tli id="T462" />
         <tli id="T463" />
         <tli id="T464" />
         <tli id="T465" />
         <tli id="T466" />
         <tli id="T467" />
         <tli id="T468" />
         <tli id="T469" />
         <tli id="T470" />
         <tli id="T471" />
         <tli id="T472" />
         <tli id="T473" />
         <tli id="T474" />
         <tli id="T475" />
         <tli id="T476" />
         <tli id="T477" />
         <tli id="T478" />
         <tli id="T479" />
         <tli id="T480" />
         <tli id="T481" />
         <tli id="T482" />
         <tli id="T483" />
         <tli id="T484" />
         <tli id="T485" />
         <tli id="T486" />
         <tli id="T487" />
         <tli id="T488" />
         <tli id="T489" />
         <tli id="T490" />
         <tli id="T491" />
         <tli id="T492" />
         <tli id="T493" />
         <tli id="T494" />
         <tli id="T495" />
         <tli id="T496" />
         <tli id="T497" />
         <tli id="T498" />
         <tli id="T499" />
         <tli id="T500" />
         <tli id="T501" />
         <tli id="T502" />
         <tli id="T503" />
         <tli id="T504" />
         <tli id="T505" />
         <tli id="T506" />
         <tli id="T507" />
         <tli id="T508" />
         <tli id="T509" />
         <tli id="T510" />
         <tli id="T511" />
         <tli id="T512" />
         <tli id="T513" />
         <tli id="T514" />
         <tli id="T515" />
         <tli id="T516" />
         <tli id="T517" />
         <tli id="T518" />
         <tli id="T519" />
         <tli id="T520" />
         <tli id="T521" />
         <tli id="T522" />
         <tli id="T523" />
         <tli id="T524" />
         <tli id="T525" />
         <tli id="T526" />
         <tli id="T527" />
         <tli id="T528" />
         <tli id="T529" />
         <tli id="T530" />
         <tli id="T531" />
         <tli id="T532" />
         <tli id="T533" />
         <tli id="T534" />
         <tli id="T535" />
         <tli id="T536" />
         <tli id="T537" />
         <tli id="T538" />
         <tli id="T539" />
         <tli id="T540" />
         <tli id="T541" />
         <tli id="T542" />
         <tli id="T543" />
         <tli id="T544" />
         <tli id="T545" />
         <tli id="T546" />
         <tli id="T547" />
         <tli id="T548" />
         <tli id="T549" />
         <tli id="T550" />
         <tli id="T551" />
         <tli id="T552" />
         <tli id="T553" />
         <tli id="T554" />
         <tli id="T555" />
         <tli id="T556" />
         <tli id="T557" />
         <tli id="T558" />
         <tli id="T559" />
         <tli id="T560" />
         <tli id="T561" />
         <tli id="T562" />
         <tli id="T563" />
         <tli id="T564" />
         <tli id="T565" />
         <tli id="T566" />
         <tli id="T567" />
         <tli id="T568" />
         <tli id="T569" />
         <tli id="T570" />
         <tli id="T571" />
         <tli id="T572" />
         <tli id="T573" />
         <tli id="T574" />
         <tli id="T575" />
         <tli id="T576" />
         <tli id="T577" />
         <tli id="T578" />
         <tli id="T579" />
         <tli id="T580" />
         <tli id="T581" />
         <tli id="T582" />
         <tli id="T583" />
         <tli id="T584" />
         <tli id="T585" />
         <tli id="T586" />
         <tli id="T587" />
         <tli id="T588" />
         <tli id="T589" />
         <tli id="T590" />
         <tli id="T591" />
         <tli id="T592" />
         <tli id="T593" />
         <tli id="T594" />
         <tli id="T595" />
         <tli id="T596" />
         <tli id="T597" />
         <tli id="T598" />
         <tli id="T599" />
         <tli id="T600" />
         <tli id="T601" />
         <tli id="T602" />
         <tli id="T603" />
         <tli id="T604" />
         <tli id="T605" />
         <tli id="T606" />
         <tli id="T607" />
         <tli id="T608" />
         <tli id="T609" />
         <tli id="T610" />
         <tli id="T611" />
         <tli id="T612" />
         <tli id="T613" />
         <tli id="T614" />
         <tli id="T615" />
         <tli id="T616" />
         <tli id="T617" />
         <tli id="T618" />
         <tli id="T619" />
         <tli id="T620" />
         <tli id="T621" />
         <tli id="T622" />
         <tli id="T623" />
         <tli id="T624" />
         <tli id="T625" />
         <tli id="T626" />
         <tli id="T627" />
         <tli id="T628" />
         <tli id="T629" />
         <tli id="T630" />
         <tli id="T631" />
         <tli id="T632" />
         <tli id="T633" />
         <tli id="T634" />
         <tli id="T635" />
         <tli id="T636" />
         <tli id="T637" />
         <tli id="T638" />
         <tli id="T639" />
         <tli id="T640" />
         <tli id="T641" />
         <tli id="T642" />
         <tli id="T643" />
         <tli id="T644" />
         <tli id="T645" />
         <tli id="T646" />
         <tli id="T647" />
         <tli id="T648" />
         <tli id="T649" />
         <tli id="T650" />
         <tli id="T651" />
         <tli id="T652" />
         <tli id="T653" />
         <tli id="T654" />
         <tli id="T655" />
         <tli id="T656" />
         <tli id="T657" />
         <tli id="T658" />
         <tli id="T659" />
         <tli id="T660" />
         <tli id="T661" />
         <tli id="T662" />
         <tli id="T663" />
         <tli id="T664" />
         <tli id="T665" />
         <tli id="T666" />
         <tli id="T667" />
         <tli id="T668" />
         <tli id="T669" />
         <tli id="T670" />
         <tli id="T671" />
         <tli id="T672" />
         <tli id="T673" />
         <tli id="T674" />
         <tli id="T675" />
         <tli id="T676" />
         <tli id="T677" />
         <tli id="T678" />
         <tli id="T679" />
         <tli id="T680" />
         <tli id="T681" />
         <tli id="T682" />
         <tli id="T683" />
         <tli id="T684" />
         <tli id="T685" />
         <tli id="T686" />
         <tli id="T687" />
         <tli id="T688" />
         <tli id="T689" />
         <tli id="T690" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="NEP"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T690" id="Seg_0" n="sc" s="T0">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Ukkɨr</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">ijatɨ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">ɛppɨmmɨnta</ts>
                  <nts id="Seg_11" n="HIAT:ip">,</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T3">neːkɨtɨlʼ</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_17" n="HIAT:w" s="T4">pɛlɨkɨtɨlʼ</ts>
                  <nts id="Seg_18" n="HIAT:ip">.</nts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_21" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_23" n="HIAT:w" s="T5">Na</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">tɛntɨlʼ</ts>
                  <nts id="Seg_27" n="HIAT:ip">,</nts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_30" n="HIAT:w" s="T7">aše</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_33" n="HIAT:w" s="T8">čʼaptä</ts>
                  <nts id="Seg_34" n="HIAT:ip">.</nts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_37" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_39" n="HIAT:w" s="T9">İra</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_42" n="HIAT:w" s="T10">ilɨmpa</ts>
                  <nts id="Seg_43" n="HIAT:ip">,</nts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">quːtalʼpa</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_50" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_52" n="HIAT:w" s="T12">Na</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_55" n="HIAT:w" s="T13">šolʼqumɨlʼ</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_58" n="HIAT:w" s="T14">ira</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_61" n="HIAT:w" s="T15">tapplʼälʼ</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">qolʼtit</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17">pɛlʼaqqɨt</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">ilɨmpa</ts>
                  <nts id="Seg_71" n="HIAT:ip">.</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_74" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_76" n="HIAT:w" s="T19">Poːkɨtɨlʼ</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_79" n="HIAT:w" s="T20">tətɨt</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_82" n="HIAT:w" s="T21">toːčʼiqɨt</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_85" n="HIAT:w" s="T22">iːlɨmpa</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">tintena</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">qüːtälpa</ts>
                  <nts id="Seg_92" n="HIAT:ip">.</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_95" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_97" n="HIAT:w" s="T25">Ukkɨr</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_100" n="HIAT:w" s="T26">ijatɨ</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_103" n="HIAT:w" s="T27">pɛlɨkɔːtɨlʼ</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_106" n="HIAT:w" s="T28">neːkɨtɨlʼ</ts>
                  <nts id="Seg_107" n="HIAT:ip">.</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_110" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_112" n="HIAT:w" s="T29">Ni</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_115" n="HIAT:w" s="T30">nälʼatɨ</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_118" n="HIAT:w" s="T31">čʼäŋka</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_121" n="HIAT:w" s="T32">ni</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_124" n="HIAT:w" s="T33">qajtɨ</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_127" n="HIAT:w" s="T34">čʼäŋka</ts>
                  <nts id="Seg_128" n="HIAT:ip">.</nts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_131" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_133" n="HIAT:w" s="T35">Nɨnɨ</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_136" n="HIAT:w" s="T36">əsɨtɨ</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_139" n="HIAT:w" s="T37">qüːtäla</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_142" n="HIAT:w" s="T38">na</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_145" n="HIAT:w" s="T39">ijalʼa</ts>
                  <nts id="Seg_146" n="HIAT:ip">.</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_149" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_151" n="HIAT:w" s="T40">Na</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_154" n="HIAT:w" s="T41">ija</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_157" n="HIAT:w" s="T42">qapıja</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_160" n="HIAT:w" s="T43">nɨmtɨ</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_163" n="HIAT:w" s="T44">ila</ts>
                  <nts id="Seg_164" n="HIAT:ip">,</nts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_167" n="HIAT:w" s="T45">əsɨntɨsä</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_170" n="HIAT:w" s="T46">əmɨntɨsä</ts>
                  <nts id="Seg_171" n="HIAT:ip">.</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_174" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_176" n="HIAT:w" s="T47">Tontɨ</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_179" n="HIAT:w" s="T48">šitɨ</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_182" n="HIAT:w" s="T49">qäːlɨlʼ</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_185" n="HIAT:w" s="T50">timnʼäsɨt</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_188" n="HIAT:w" s="T51">ɛppɨntɔːtɨt</ts>
                  <nts id="Seg_189" n="HIAT:ip">.</nts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_192" n="HIAT:u" s="T52">
                  <ts e="T693" id="Seg_194" n="HIAT:w" s="T52">Qäːlʼ</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_197" n="HIAT:w" s="T693">irä</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_200" n="HIAT:w" s="T53">puːčʼčʼɛnta</ts>
                  <nts id="Seg_201" n="HIAT:ip">.</nts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_204" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_206" n="HIAT:w" s="T54">Tɨntäna</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_209" n="HIAT:w" s="T55">ira</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_212" n="HIAT:w" s="T56">ıllä</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_215" n="HIAT:w" s="T57">quŋa</ts>
                  <nts id="Seg_216" n="HIAT:ip">.</nts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_219" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_221" n="HIAT:w" s="T58">Tintena</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_224" n="HIAT:w" s="T59">qäːlʼ</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_227" n="HIAT:w" s="T694">ira</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_230" n="HIAT:w" s="T60">kučʼčʼä</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_233" n="HIAT:w" s="T61">qattɛnta</ts>
                  <nts id="Seg_234" n="HIAT:ip">.</nts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_237" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_239" n="HIAT:w" s="T62">Toː</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_242" n="HIAT:w" s="T63">timnʼäntɨnɨ</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_245" n="HIAT:w" s="T64">puːŋa</ts>
                  <nts id="Seg_246" n="HIAT:ip">.</nts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_249" n="HIAT:u" s="T65">
                  <nts id="Seg_250" n="HIAT:ip">“</nts>
                  <ts e="T66" id="Seg_252" n="HIAT:w" s="T65">İrra</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_255" n="HIAT:w" s="T66">qunta</ts>
                  <nts id="Seg_256" n="HIAT:ip">,</nts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_259" n="HIAT:w" s="T67">ıllä</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_262" n="HIAT:w" s="T68">meːŋɔːtɨt</ts>
                  <nts id="Seg_263" n="HIAT:ip">”</nts>
                  <nts id="Seg_264" n="HIAT:ip">.</nts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_267" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_269" n="HIAT:w" s="T69">Šittälʼ</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_272" n="HIAT:w" s="T70">imaqotatɨ</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_275" n="HIAT:w" s="T71">qüːtälʼlʼä</ts>
                  <nts id="Seg_276" n="HIAT:ip">,</nts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_279" n="HIAT:w" s="T72">aj</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_282" n="HIAT:w" s="T73">ıllä</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_285" n="HIAT:w" s="T74">quŋa</ts>
                  <nts id="Seg_286" n="HIAT:ip">.</nts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_289" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_291" n="HIAT:w" s="T75">İralʼ</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_294" n="HIAT:w" s="T76">mɨqä</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_297" n="HIAT:w" s="T77">ijatɨt</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_300" n="HIAT:w" s="T78">nejkɔːlɨ</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_303" n="HIAT:w" s="T79">pɛlɨkɔːlɨ</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_306" n="HIAT:w" s="T80">qala</ts>
                  <nts id="Seg_307" n="HIAT:ip">.</nts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_310" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_312" n="HIAT:w" s="T81">Kučʼčʼä</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_315" n="HIAT:w" s="T82">qattɛnta</ts>
                  <nts id="Seg_316" n="HIAT:ip">.</nts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_319" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_321" n="HIAT:w" s="T83">Na</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_324" n="HIAT:w" s="T84">qäːlʼi</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_327" n="HIAT:w" s="T695">ira</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_330" n="HIAT:w" s="T85">šittalʼ</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_333" n="HIAT:w" s="T86">tɔː</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_336" n="HIAT:w" s="T87">puːtɨmpatɨ</ts>
                  <nts id="Seg_337" n="HIAT:ip">.</nts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_340" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_342" n="HIAT:w" s="T88">Märqɨ</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_345" n="HIAT:w" s="T89">ijatɨ</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_348" n="HIAT:w" s="T90">nılʼčʼilʼ</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_351" n="HIAT:w" s="T91">ijatɨ</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_354" n="HIAT:w" s="T92">ɛppɨmmɨnta</ts>
                  <nts id="Seg_355" n="HIAT:ip">.</nts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_358" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_360" n="HIAT:w" s="T93">Puːtɨŋɨtɨ</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_363" n="HIAT:w" s="T94">ɔːtätɨ</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_366" n="HIAT:w" s="T95">aj</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_369" n="HIAT:w" s="T96">čʼäːŋka</ts>
                  <nts id="Seg_370" n="HIAT:ip">.</nts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_373" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_375" n="HIAT:w" s="T97">Ɔːtäkɔːlɨk</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_378" n="HIAT:w" s="T98">topɨsä</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_381" n="HIAT:w" s="T99">ilɨmpɔːtɨt</ts>
                  <nts id="Seg_382" n="HIAT:ip">.</nts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_385" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_387" n="HIAT:w" s="T100">Nɨmtɨ</ts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_390" n="HIAT:w" s="T101">ilɨmpɔːtɨt</ts>
                  <nts id="Seg_391" n="HIAT:ip">.</nts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_394" n="HIAT:u" s="T102">
                  <ts e="T103" id="Seg_396" n="HIAT:w" s="T102">İlɨmpɔːtɨt</ts>
                  <nts id="Seg_397" n="HIAT:ip">,</nts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_400" n="HIAT:w" s="T103">tɔː</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_403" n="HIAT:w" s="T104">puːlʼä</ts>
                  <nts id="Seg_404" n="HIAT:ip">,</nts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_407" n="HIAT:w" s="T105">qälʼɨ-irat</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_410" n="HIAT:w" s="T106">mɔːttɨ</ts>
                  <nts id="Seg_411" n="HIAT:ip">.</nts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_414" n="HIAT:u" s="T107">
                  <ts e="T691" id="Seg_416" n="HIAT:w" s="T107">Qälʼɨ</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_419" n="HIAT:w" s="T691">irra</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_422" n="HIAT:w" s="T108">ijatɨ</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_425" n="HIAT:w" s="T109">čʼäŋɨmpa</ts>
                  <nts id="Seg_426" n="HIAT:ip">.</nts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_429" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_431" n="HIAT:w" s="T110">İjakɔːtɨlʼ</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_434" n="HIAT:w" s="T111">imatɨ</ts>
                  <nts id="Seg_435" n="HIAT:ip">.</nts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_438" n="HIAT:u" s="T112">
                  <ts e="T113" id="Seg_440" n="HIAT:w" s="T112">Toːna</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_443" n="HIAT:w" s="T113">čʼontolʼ</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_446" n="HIAT:w" s="T114">timnʼatɨ</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_449" n="HIAT:w" s="T115">ukkɨr</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_452" n="HIAT:w" s="T116">nälʼätɨ</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_455" n="HIAT:w" s="T117">pɛlɨkɨtɨlʼ</ts>
                  <nts id="Seg_456" n="HIAT:ip">.</nts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_459" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_461" n="HIAT:w" s="T118">Takkɨnɨ</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_464" n="HIAT:w" s="T119">nʼänna</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_467" n="HIAT:w" s="T120">šitɨ</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_470" n="HIAT:w" s="T121">qälɨk</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_473" n="HIAT:w" s="T122">tüːŋa</ts>
                  <nts id="Seg_474" n="HIAT:ip">.</nts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T128" id="Seg_477" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_479" n="HIAT:w" s="T123">Poːkɨtɨlʼ</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_482" n="HIAT:w" s="T124">təttoqɨnɨ</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_485" n="HIAT:w" s="T125">na</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_488" n="HIAT:w" s="T126">qälʼi</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_491" n="HIAT:w" s="T692">ira</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_494" n="HIAT:w" s="T127">najɛnta</ts>
                  <nts id="Seg_495" n="HIAT:ip">.</nts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_498" n="HIAT:u" s="T128">
                  <ts e="T129" id="Seg_500" n="HIAT:w" s="T128">Kuššalʼ</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_503" n="HIAT:w" s="T129">tamtɨrtɨ</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_506" n="HIAT:w" s="T130">ɔːtäp</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_509" n="HIAT:w" s="T131">muntɨk</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_512" n="HIAT:w" s="T132">taqqɨšpatɨ</ts>
                  <nts id="Seg_513" n="HIAT:ip">.</nts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T135" id="Seg_516" n="HIAT:u" s="T133">
                  <ts e="T134" id="Seg_518" n="HIAT:w" s="T133">Nılʼčʼik</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_521" n="HIAT:w" s="T134">tüːŋa</ts>
                  <nts id="Seg_522" n="HIAT:ip">.</nts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T142" id="Seg_525" n="HIAT:u" s="T135">
                  <ts e="T136" id="Seg_527" n="HIAT:w" s="T135">Na</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_530" n="HIAT:w" s="T136">tɨmnʼasɨka</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_533" n="HIAT:w" s="T137">ilɔːtɨt</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_536" n="HIAT:w" s="T138">na</ts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_539" n="HIAT:w" s="T139">iralʼ</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_542" n="HIAT:w" s="T140">tɨmnʼäsɨka</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_545" n="HIAT:w" s="T141">šittɨ</ts>
                  <nts id="Seg_546" n="HIAT:ip">.</nts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T145" id="Seg_549" n="HIAT:u" s="T142">
                  <ts e="T143" id="Seg_551" n="HIAT:w" s="T142">Tɛːttɨ</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_554" n="HIAT:w" s="T143">toːt</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_557" n="HIAT:w" s="T144">ɔːtäqitɨ</ts>
                  <nts id="Seg_558" n="HIAT:ip">.</nts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T150" id="Seg_561" n="HIAT:u" s="T145">
                  <ts e="T146" id="Seg_563" n="HIAT:w" s="T145">Na</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_566" n="HIAT:w" s="T146">iran</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_569" n="HIAT:w" s="T147">ɔːtäp</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_572" n="HIAT:w" s="T148">iqɨntoːqo</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_575" n="HIAT:w" s="T149">tünta</ts>
                  <nts id="Seg_576" n="HIAT:ip">.</nts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_579" n="HIAT:u" s="T150">
                  <ts e="T151" id="Seg_581" n="HIAT:w" s="T150">Qumintɨsä</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_584" n="HIAT:w" s="T151">tüːnta</ts>
                  <nts id="Seg_585" n="HIAT:ip">.</nts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_588" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_590" n="HIAT:w" s="T152">Paːn</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_593" n="HIAT:w" s="T153">iːra</ts>
                  <nts id="Seg_594" n="HIAT:ip">,</nts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_597" n="HIAT:w" s="T154">nımtɨ</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_600" n="HIAT:w" s="T155">aj</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_603" n="HIAT:w" s="T156">najta</ts>
                  <nts id="Seg_604" n="HIAT:ip">.</nts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T163" id="Seg_607" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_609" n="HIAT:w" s="T157">Nɨnɨ</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_612" n="HIAT:w" s="T158">tüːlä</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_615" n="HIAT:w" s="T159">šäqqɔːtɨt</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_618" n="HIAT:w" s="T160">na</ts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_621" n="HIAT:w" s="T161">täpɨtɨt</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_624" n="HIAT:w" s="T162">mɔːtqɨt</ts>
                  <nts id="Seg_625" n="HIAT:ip">.</nts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T167" id="Seg_628" n="HIAT:u" s="T163">
                  <ts e="T164" id="Seg_630" n="HIAT:w" s="T163">Na</ts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_633" n="HIAT:w" s="T164">ija</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_636" n="HIAT:w" s="T165">aš</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_639" n="HIAT:w" s="T166">tɛnɨmɔːtɨt</ts>
                  <nts id="Seg_640" n="HIAT:ip">.</nts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T171" id="Seg_643" n="HIAT:u" s="T167">
                  <nts id="Seg_644" n="HIAT:ip">“</nts>
                  <ts e="T168" id="Seg_646" n="HIAT:w" s="T167">İːja</ts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_649" n="HIAT:w" s="T168">kuttar</ts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_652" n="HIAT:w" s="T169">taːpɨnɨk</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_655" n="HIAT:w" s="T170">alʼčʼimpa</ts>
                  <nts id="Seg_656" n="HIAT:ip">?</nts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T175" id="Seg_659" n="HIAT:u" s="T171">
                  <ts e="T172" id="Seg_661" n="HIAT:w" s="T171">Täp</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_664" n="HIAT:w" s="T172">qaj</ts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_667" n="HIAT:w" s="T173">muːtɨronak</ts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_670" n="HIAT:w" s="T174">ila</ts>
                  <nts id="Seg_671" n="HIAT:ip">”</nts>
                  <nts id="Seg_672" n="HIAT:ip">.</nts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T181" id="Seg_675" n="HIAT:u" s="T175">
                  <ts e="T176" id="Seg_677" n="HIAT:w" s="T175">Qup</ts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_680" n="HIAT:w" s="T176">kuttar</ts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_683" n="HIAT:w" s="T177">tɛnimɛntätɨ</ts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_686" n="HIAT:w" s="T178">alʼpet</ts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_689" n="HIAT:w" s="T179">ilʼäntɨlʼ</ts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_692" n="HIAT:w" s="T180">qup</ts>
                  <nts id="Seg_693" n="HIAT:ip">.</nts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T182" id="Seg_696" n="HIAT:u" s="T181">
                  <ts e="T182" id="Seg_698" n="HIAT:w" s="T181">Šäqqɔːtɨt</ts>
                  <nts id="Seg_699" n="HIAT:ip">.</nts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T192" id="Seg_702" n="HIAT:u" s="T182">
                  <ts e="T183" id="Seg_704" n="HIAT:w" s="T182">Mompa</ts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_707" n="HIAT:w" s="T183">nɔːrtälʼilʼ</ts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_710" n="HIAT:w" s="T184">čʼeːlʼɨt</ts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_713" n="HIAT:w" s="T185">toːqqɨt</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_716" n="HIAT:w" s="T186">mompa</ts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_719" n="HIAT:w" s="T187">Pan</ts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_722" n="HIAT:w" s="T188">ira</ts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_725" n="HIAT:w" s="T189">köt</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_728" n="HIAT:w" s="T190">qupsä</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_731" n="HIAT:w" s="T191">tüːŋa</ts>
                  <nts id="Seg_732" n="HIAT:ip">.</nts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T195" id="Seg_735" n="HIAT:u" s="T192">
                  <ts e="T193" id="Seg_737" n="HIAT:w" s="T192">Qapija</ts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_740" n="HIAT:w" s="T193">ɔːtäp</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_743" n="HIAT:w" s="T194">tɔːqqɨqontoːqo</ts>
                  <nts id="Seg_744" n="HIAT:ip">.</nts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T200" id="Seg_747" n="HIAT:u" s="T195">
                  <ts e="T196" id="Seg_749" n="HIAT:w" s="T195">Na</ts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_752" n="HIAT:w" s="T196">tintena</ts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_755" n="HIAT:w" s="T197">asästit</ts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_758" n="HIAT:w" s="T198">ɔːtäp</ts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_761" n="HIAT:w" s="T199">tɔːqqɨqo</ts>
                  <nts id="Seg_762" n="HIAT:ip">.</nts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T209" id="Seg_765" n="HIAT:u" s="T200">
                  <ts e="T201" id="Seg_767" n="HIAT:w" s="T200">Kun</ts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_770" n="HIAT:w" s="T201">mɔːntɨlʼ</ts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_773" n="HIAT:w" s="T202">mɔːttɨ</ts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_776" n="HIAT:w" s="T203">šentɨ</ts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_779" n="HIAT:w" s="T204">kun</ts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_782" n="HIAT:w" s="T205">mɔːntɨlʼ</ts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_785" n="HIAT:w" s="T206">qajtɨ</ts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_788" n="HIAT:w" s="T207">mun</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_791" n="HIAT:w" s="T208">taqqalʼtɛntɨt</ts>
                  <nts id="Seg_792" n="HIAT:ip">.</nts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T212" id="Seg_795" n="HIAT:u" s="T209">
                  <ts e="T210" id="Seg_797" n="HIAT:w" s="T209">Ukoːn</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_800" n="HIAT:w" s="T210">nɔːta</ts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_803" n="HIAT:w" s="T211">taqqɨlʼkolʼčʼimpat</ts>
                  <nts id="Seg_804" n="HIAT:ip">.</nts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T216" id="Seg_807" n="HIAT:u" s="T212">
                  <ts e="T213" id="Seg_809" n="HIAT:w" s="T212">Qälʼitɨt</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_812" n="HIAT:w" s="T213">ɔːtap</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_815" n="HIAT:w" s="T214">taqqɨllä</ts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_818" n="HIAT:w" s="T215">iːmpatɨ</ts>
                  <nts id="Seg_819" n="HIAT:ip">.</nts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T223" id="Seg_822" n="HIAT:u" s="T216">
                  <ts e="T217" id="Seg_824" n="HIAT:w" s="T216">Nɨnɨ</ts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_827" n="HIAT:w" s="T217">šittälʼ</ts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_830" n="HIAT:w" s="T218">mɨŋa</ts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_833" n="HIAT:w" s="T219">moqɨnä</ts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_836" n="HIAT:w" s="T220">qənnɔːqij</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_839" n="HIAT:w" s="T221">na</ts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_842" n="HIAT:w" s="T222">qälɨkqıː</ts>
                  <nts id="Seg_843" n="HIAT:ip">.</nts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T227" id="Seg_846" n="HIAT:u" s="T223">
                  <ts e="T224" id="Seg_848" n="HIAT:w" s="T223">Nɔːrtälʼilʼ</ts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_851" n="HIAT:w" s="T224">čʼeːlʼit</ts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_854" n="HIAT:w" s="T225">mompa</ts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_857" n="HIAT:w" s="T226">tüntɔːmɨt</ts>
                  <nts id="Seg_858" n="HIAT:ip">.</nts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T232" id="Seg_861" n="HIAT:u" s="T227">
                  <ts e="T228" id="Seg_863" n="HIAT:w" s="T227">Tɛː</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_866" n="HIAT:w" s="T228">mumpa</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_869" n="HIAT:w" s="T229">kučʼčʼä</ts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_872" n="HIAT:w" s="T230">ɨkɨ</ts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_875" n="HIAT:w" s="T231">qənnɨlɨt</ts>
                  <nts id="Seg_876" n="HIAT:ip">.</nts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T236" id="Seg_879" n="HIAT:u" s="T232">
                  <ts e="T233" id="Seg_881" n="HIAT:w" s="T232">Šittälʼɨlʼ</ts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_884" n="HIAT:w" s="T233">čʼeːlontɨ</ts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_887" n="HIAT:w" s="T234">ınnä</ts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_890" n="HIAT:w" s="T235">čʼeːlʼinna</ts>
                  <nts id="Seg_891" n="HIAT:ip">.</nts>
                  <nts id="Seg_892" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T241" id="Seg_894" n="HIAT:u" s="T236">
                  <ts e="T237" id="Seg_896" n="HIAT:w" s="T236">Na</ts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_899" n="HIAT:w" s="T237">iːja</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_902" n="HIAT:w" s="T238">nılʼ</ts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_905" n="HIAT:w" s="T239">kətɨtɨt</ts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_908" n="HIAT:w" s="T240">ilʼčʼantɨnɨk</ts>
                  <nts id="Seg_909" n="HIAT:ip">.</nts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T252" id="Seg_912" n="HIAT:u" s="T241">
                  <ts e="T242" id="Seg_914" n="HIAT:w" s="T241">Mɨta</ts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_917" n="HIAT:w" s="T242">nʼannät</ts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_920" n="HIAT:w" s="T243">tɨmtɨ</ts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_923" n="HIAT:w" s="T244">tɛːttɨ</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_926" n="HIAT:w" s="T245">toːt</ts>
                  <nts id="Seg_927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_929" n="HIAT:w" s="T246">ɔːtat</ts>
                  <nts id="Seg_930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_932" n="HIAT:w" s="T247">nɨŋqɨkušalʼ</ts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_935" n="HIAT:w" s="T248">nʼarop</ts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_938" n="HIAT:w" s="T249">ɛːnta</ts>
                  <nts id="Seg_939" n="HIAT:ip">,</nts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_942" n="HIAT:w" s="T250">načʼčʼä</ts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_945" n="HIAT:w" s="T251">üːtälʼɨmɨt</ts>
                  <nts id="Seg_946" n="HIAT:ip">.</nts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T256" id="Seg_949" n="HIAT:u" s="T252">
                  <ts e="T253" id="Seg_951" n="HIAT:w" s="T252">Qäːlʼit</ts>
                  <nts id="Seg_952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_954" n="HIAT:w" s="T253">tüːntɔːtɨt</ts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_957" n="HIAT:w" s="T254">ɔːtäp</ts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_960" n="HIAT:w" s="T255">tɔːqqɨqolamnɛntɔːtɨt</ts>
                  <nts id="Seg_961" n="HIAT:ip">.</nts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T263" id="Seg_964" n="HIAT:u" s="T256">
                  <ts e="T257" id="Seg_966" n="HIAT:w" s="T256">Ɔːtan</ts>
                  <nts id="Seg_967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_969" n="HIAT:w" s="T257">upkɨt</ts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_972" n="HIAT:w" s="T258">kəlʼčʼomɨntɨ</ts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_975" n="HIAT:w" s="T259">kuntɨ</ts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_978" n="HIAT:w" s="T260">kärɨpɨlʼ</ts>
                  <nts id="Seg_979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_981" n="HIAT:w" s="T261">kutar</ts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_984" n="HIAT:w" s="T262">meːtɔːmɨt</ts>
                  <nts id="Seg_985" n="HIAT:ip">.</nts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T267" id="Seg_988" n="HIAT:u" s="T263">
                  <ts e="T264" id="Seg_990" n="HIAT:w" s="T263">Na</ts>
                  <nts id="Seg_991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_993" n="HIAT:w" s="T264">iːja</ts>
                  <nts id="Seg_994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_996" n="HIAT:w" s="T265">nık</ts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_999" n="HIAT:w" s="T266">kəttɨŋɨt</ts>
                  <nts id="Seg_1000" n="HIAT:ip">.</nts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T270" id="Seg_1003" n="HIAT:u" s="T267">
                  <ts e="T268" id="Seg_1005" n="HIAT:w" s="T267">Nɨnɨ</ts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1008" n="HIAT:w" s="T268">nık</ts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1011" n="HIAT:w" s="T269">kəttɨŋɨtɨ</ts>
                  <nts id="Seg_1012" n="HIAT:ip">.</nts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T278" id="Seg_1015" n="HIAT:u" s="T270">
                  <ts e="T271" id="Seg_1017" n="HIAT:w" s="T270">Mäkkä</ts>
                  <nts id="Seg_1018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1020" n="HIAT:w" s="T271">mɨta</ts>
                  <nts id="Seg_1021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1023" n="HIAT:w" s="T272">šentɨ</ts>
                  <nts id="Seg_1024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1026" n="HIAT:w" s="T273">mɔːllʼet</ts>
                  <nts id="Seg_1027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1029" n="HIAT:w" s="T274">ɛːŋa</ts>
                  <nts id="Seg_1030" n="HIAT:ip">,</nts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1033" n="HIAT:w" s="T275">ilʼčʼa</ts>
                  <nts id="Seg_1034" n="HIAT:ip">,</nts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1037" n="HIAT:w" s="T276">mäkkä</ts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1040" n="HIAT:w" s="T277">mitɨ</ts>
                  <nts id="Seg_1041" n="HIAT:ip">.</nts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T286" id="Seg_1044" n="HIAT:u" s="T278">
                  <ts e="T279" id="Seg_1046" n="HIAT:w" s="T278">A</ts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1049" n="HIAT:w" s="T279">toːna</ts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1052" n="HIAT:w" s="T280">tınte</ts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1055" n="HIAT:w" s="T281">timnʼäntɨ</ts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1058" n="HIAT:w" s="T282">nälʼa</ts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1061" n="HIAT:w" s="T283">ilʼčʼatɨ</ts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1064" n="HIAT:w" s="T284">natɨp</ts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1067" n="HIAT:w" s="T285">kuralʼtɨmpatɨ</ts>
                  <nts id="Seg_1068" n="HIAT:ip">.</nts>
                  <nts id="Seg_1069" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T289" id="Seg_1071" n="HIAT:u" s="T286">
                  <ts e="T287" id="Seg_1073" n="HIAT:w" s="T286">Na</ts>
                  <nts id="Seg_1074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1076" n="HIAT:w" s="T287">iːjanɨk</ts>
                  <nts id="Seg_1077" n="HIAT:ip">…</nts>
                  <nts id="Seg_1078" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T294" id="Seg_1080" n="HIAT:u" s="T289">
                  <ts e="T290" id="Seg_1082" n="HIAT:w" s="T289">Nʼännät</ts>
                  <nts id="Seg_1083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1085" n="HIAT:w" s="T290">tɨmtɨ</ts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1088" n="HIAT:w" s="T291">maːčʼelʼ</ts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1091" n="HIAT:w" s="T292">tiːčʼi</ts>
                  <nts id="Seg_1092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1094" n="HIAT:w" s="T293">ɛːŋa</ts>
                  <nts id="Seg_1095" n="HIAT:ip">.</nts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T296" id="Seg_1098" n="HIAT:u" s="T294">
                  <ts e="T295" id="Seg_1100" n="HIAT:w" s="T294">Nɨmtɨ</ts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1103" n="HIAT:w" s="T295">mɔːtɨŋnɛntak</ts>
                  <nts id="Seg_1104" n="HIAT:ip">.</nts>
                  <nts id="Seg_1105" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T300" id="Seg_1107" n="HIAT:u" s="T296">
                  <ts e="T297" id="Seg_1109" n="HIAT:w" s="T296">Šentɨ</ts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1112" n="HIAT:w" s="T297">mɔːt</ts>
                  <nts id="Seg_1113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1115" n="HIAT:w" s="T298">tatɨŋɨt</ts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1118" n="HIAT:w" s="T299">mɨta</ts>
                  <nts id="Seg_1119" n="HIAT:ip">.</nts>
                  <nts id="Seg_1120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T302" id="Seg_1122" n="HIAT:u" s="T300">
                  <ts e="T301" id="Seg_1124" n="HIAT:w" s="T300">Üːtälnɔːtɨt</ts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1127" n="HIAT:w" s="T301">taːlʼ</ts>
                  <nts id="Seg_1128" n="HIAT:ip">.</nts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T314" id="Seg_1131" n="HIAT:u" s="T302">
                  <ts e="T303" id="Seg_1133" n="HIAT:w" s="T302">Qapıje</ts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1136" n="HIAT:w" s="T303">nɨ</ts>
                  <nts id="Seg_1137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1139" n="HIAT:w" s="T304">nɔːrtälʼilʼ</ts>
                  <nts id="Seg_1140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1142" n="HIAT:w" s="T305">čʼeːlɨ</ts>
                  <nts id="Seg_1143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1145" n="HIAT:w" s="T306">tüːntɔːtɨt</ts>
                  <nts id="Seg_1146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1148" n="HIAT:w" s="T307">Pan</ts>
                  <nts id="Seg_1149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1151" n="HIAT:w" s="T308">iralʼmɨt</ts>
                  <nts id="Seg_1152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1154" n="HIAT:w" s="T309">təpɨtɨtkiːne</ts>
                  <nts id="Seg_1155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1157" n="HIAT:w" s="T310">na</ts>
                  <nts id="Seg_1158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1160" n="HIAT:w" s="T311">qälɨ-irat</ts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1163" n="HIAT:w" s="T312">ɔːtap</ts>
                  <nts id="Seg_1164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1166" n="HIAT:w" s="T313">taqqalʼqo</ts>
                  <nts id="Seg_1167" n="HIAT:ip">.</nts>
                  <nts id="Seg_1168" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T319" id="Seg_1170" n="HIAT:u" s="T314">
                  <ts e="T315" id="Seg_1172" n="HIAT:w" s="T314">Timnʼäsɨqät</ts>
                  <nts id="Seg_1173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1175" n="HIAT:w" s="T315">ɔːtäp</ts>
                  <nts id="Seg_1176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1178" n="HIAT:w" s="T316">nɨnɨ</ts>
                  <nts id="Seg_1179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1181" n="HIAT:w" s="T317">šittelʼ</ts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1184" n="HIAT:w" s="T318">üːtältɔːtɨt</ts>
                  <nts id="Seg_1185" n="HIAT:ip">.</nts>
                  <nts id="Seg_1186" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T327" id="Seg_1188" n="HIAT:u" s="T319">
                  <ts e="T320" id="Seg_1190" n="HIAT:w" s="T319">Na</ts>
                  <nts id="Seg_1191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1193" n="HIAT:w" s="T320">ɔːtäp</ts>
                  <nts id="Seg_1194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1196" n="HIAT:w" s="T321">na</ts>
                  <nts id="Seg_1197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1199" n="HIAT:w" s="T322">tɔːqqäntɔːtɨt</ts>
                  <nts id="Seg_1200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1202" n="HIAT:w" s="T323">qapıj</ts>
                  <nts id="Seg_1203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1205" n="HIAT:w" s="T324">täm</ts>
                  <nts id="Seg_1206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1208" n="HIAT:w" s="T325">aj</ts>
                  <nts id="Seg_1209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1211" n="HIAT:w" s="T326">tɔːqqɨmpatɨ</ts>
                  <nts id="Seg_1212" n="HIAT:ip">.</nts>
                  <nts id="Seg_1213" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T330" id="Seg_1215" n="HIAT:u" s="T327">
                  <ts e="T328" id="Seg_1217" n="HIAT:w" s="T327">Mɔːssä</ts>
                  <nts id="Seg_1218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1220" n="HIAT:w" s="T328">na</ts>
                  <nts id="Seg_1221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1223" n="HIAT:w" s="T329">mintɔːtɨt</ts>
                  <nts id="Seg_1224" n="HIAT:ip">.</nts>
                  <nts id="Seg_1225" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T339" id="Seg_1227" n="HIAT:u" s="T330">
                  <ts e="T331" id="Seg_1229" n="HIAT:w" s="T330">Na</ts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1232" n="HIAT:w" s="T331">mačʼit</ts>
                  <nts id="Seg_1233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1235" n="HIAT:w" s="T332">tičʼit</ts>
                  <nts id="Seg_1236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1238" n="HIAT:w" s="T333">nʼännälʼ</ts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1241" n="HIAT:w" s="T334">pɛlʼäqqɨt</ts>
                  <nts id="Seg_1242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1244" n="HIAT:w" s="T335">nɨna</ts>
                  <nts id="Seg_1245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1247" n="HIAT:w" s="T336">mɔːtɨŋtɔːtɨt</ts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1250" n="HIAT:w" s="T337">na</ts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1253" n="HIAT:w" s="T338">imantɨsä</ts>
                  <nts id="Seg_1254" n="HIAT:ip">.</nts>
                  <nts id="Seg_1255" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T349" id="Seg_1257" n="HIAT:u" s="T339">
                  <ts e="T340" id="Seg_1259" n="HIAT:w" s="T339">Qapıje</ts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1262" n="HIAT:w" s="T340">təttalʼ</ts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1265" n="HIAT:w" s="T341">qälɨk</ts>
                  <nts id="Seg_1266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1268" n="HIAT:w" s="T342">qaj</ts>
                  <nts id="Seg_1269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1271" n="HIAT:w" s="T343">mɔːttɨ</ts>
                  <nts id="Seg_1272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1274" n="HIAT:w" s="T344">čʼaːŋka</ts>
                  <nts id="Seg_1275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1277" n="HIAT:w" s="T345">šentɨ</ts>
                  <nts id="Seg_1278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1280" n="HIAT:w" s="T346">mɔːttɨ</ts>
                  <nts id="Seg_1281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1283" n="HIAT:w" s="T347">qopɨlʼ</ts>
                  <nts id="Seg_1284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1286" n="HIAT:w" s="T348">mɔːttɨ</ts>
                  <nts id="Seg_1287" n="HIAT:ip">.</nts>
                  <nts id="Seg_1288" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T354" id="Seg_1290" n="HIAT:u" s="T349">
                  <ts e="T350" id="Seg_1292" n="HIAT:w" s="T349">Nɨnɨ</ts>
                  <nts id="Seg_1293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1295" n="HIAT:w" s="T350">šittälʼ</ts>
                  <nts id="Seg_1296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1298" n="HIAT:w" s="T351">šäqqɔːtɨt</ts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1301" n="HIAT:w" s="T352">na</ts>
                  <nts id="Seg_1302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1304" n="HIAT:w" s="T353">imantɨsä</ts>
                  <nts id="Seg_1305" n="HIAT:ip">.</nts>
                  <nts id="Seg_1306" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T358" id="Seg_1308" n="HIAT:u" s="T354">
                  <ts e="T355" id="Seg_1310" n="HIAT:w" s="T354">Təptɨlʼ</ts>
                  <nts id="Seg_1311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1313" n="HIAT:w" s="T355">qarɨt</ts>
                  <nts id="Seg_1314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1316" n="HIAT:w" s="T356">nıŋ</ts>
                  <nts id="Seg_1317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1319" n="HIAT:w" s="T357">ɛsa</ts>
                  <nts id="Seg_1320" n="HIAT:ip">.</nts>
                  <nts id="Seg_1321" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T365" id="Seg_1323" n="HIAT:u" s="T358">
                  <ts e="T359" id="Seg_1325" n="HIAT:w" s="T358">Tıntena</ts>
                  <nts id="Seg_1326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1328" n="HIAT:w" s="T359">imantɨnɨk</ts>
                  <nts id="Seg_1329" n="HIAT:ip">:</nts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1332" n="HIAT:w" s="T360">mat</ts>
                  <nts id="Seg_1333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1335" n="HIAT:w" s="T361">mɨta</ts>
                  <nts id="Seg_1336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1338" n="HIAT:w" s="T362">nʼänna</ts>
                  <nts id="Seg_1339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1341" n="HIAT:w" s="T363">qumiːqäk</ts>
                  <nts id="Seg_1342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1344" n="HIAT:w" s="T364">koːnɨlʼäk</ts>
                  <nts id="Seg_1345" n="HIAT:ip">.</nts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T370" id="Seg_1348" n="HIAT:u" s="T365">
                  <ts e="T366" id="Seg_1350" n="HIAT:w" s="T365">Qaj</ts>
                  <nts id="Seg_1351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1353" n="HIAT:w" s="T366">mɨta</ts>
                  <nts id="Seg_1354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1356" n="HIAT:w" s="T367">mɔːtɨnpɔːtɨt</ts>
                  <nts id="Seg_1357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1359" n="HIAT:w" s="T368">qaj</ts>
                  <nts id="Seg_1360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1362" n="HIAT:w" s="T369">qaːtɔːtɨt</ts>
                  <nts id="Seg_1363" n="HIAT:ip">.</nts>
                  <nts id="Seg_1364" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T372" id="Seg_1366" n="HIAT:u" s="T370">
                  <ts e="T371" id="Seg_1368" n="HIAT:w" s="T370">Nʼännä</ts>
                  <nts id="Seg_1369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1371" n="HIAT:w" s="T371">laqaltelʼčʼa</ts>
                  <nts id="Seg_1372" n="HIAT:ip">.</nts>
                  <nts id="Seg_1373" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T379" id="Seg_1375" n="HIAT:u" s="T372">
                  <ts e="T373" id="Seg_1377" n="HIAT:w" s="T372">Nɔːkɨr</ts>
                  <nts id="Seg_1378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1380" n="HIAT:w" s="T373">qoptɨp</ts>
                  <nts id="Seg_1381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1383" n="HIAT:w" s="T374">sɔːrɨlʼa</ts>
                  <nts id="Seg_1384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1386" n="HIAT:w" s="T375">qapıja</ts>
                  <nts id="Seg_1387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1389" n="HIAT:w" s="T376">nɔːssarɨlʼ</ts>
                  <nts id="Seg_1390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1392" n="HIAT:w" s="T377">ɔːtäp</ts>
                  <nts id="Seg_1393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1395" n="HIAT:w" s="T378">qaltɨmmɨntɨt</ts>
                  <nts id="Seg_1396" n="HIAT:ip">.</nts>
                  <nts id="Seg_1397" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T382" id="Seg_1399" n="HIAT:u" s="T379">
                  <ts e="T380" id="Seg_1401" n="HIAT:w" s="T379">İja</ts>
                  <nts id="Seg_1402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1404" n="HIAT:w" s="T380">nʼanna</ts>
                  <nts id="Seg_1405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1407" n="HIAT:w" s="T381">qənnɛːja</ts>
                  <nts id="Seg_1408" n="HIAT:ip">.</nts>
                  <nts id="Seg_1409" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T385" id="Seg_1411" n="HIAT:u" s="T382">
                  <ts e="T383" id="Seg_1413" n="HIAT:w" s="T382">İːmatɨ</ts>
                  <nts id="Seg_1414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1416" n="HIAT:w" s="T383">ontɨ</ts>
                  <nts id="Seg_1417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1419" n="HIAT:w" s="T384">qala</ts>
                  <nts id="Seg_1420" n="HIAT:ip">.</nts>
                  <nts id="Seg_1421" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T389" id="Seg_1423" n="HIAT:u" s="T385">
                  <ts e="T386" id="Seg_1425" n="HIAT:w" s="T385">Na</ts>
                  <nts id="Seg_1426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1428" n="HIAT:w" s="T386">omta</ts>
                  <nts id="Seg_1429" n="HIAT:ip">,</nts>
                  <nts id="Seg_1430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1432" n="HIAT:w" s="T387">na</ts>
                  <nts id="Seg_1433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1435" n="HIAT:w" s="T388">omta</ts>
                  <nts id="Seg_1436" n="HIAT:ip">.</nts>
                  <nts id="Seg_1437" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T396" id="Seg_1439" n="HIAT:u" s="T389">
                  <ts e="T390" id="Seg_1441" n="HIAT:w" s="T389">Ukkɨr</ts>
                  <nts id="Seg_1442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1444" n="HIAT:w" s="T390">tät</ts>
                  <nts id="Seg_1445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1447" n="HIAT:w" s="T391">čʼontoːqɨt</ts>
                  <nts id="Seg_1448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1450" n="HIAT:w" s="T392">čʼeːlʼɨtɨ</ts>
                  <nts id="Seg_1451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1453" n="HIAT:w" s="T393">üːtoːntɨ</ts>
                  <nts id="Seg_1454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1456" n="HIAT:w" s="T394">na</ts>
                  <nts id="Seg_1457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1459" n="HIAT:w" s="T395">qənna</ts>
                  <nts id="Seg_1460" n="HIAT:ip">.</nts>
                  <nts id="Seg_1461" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T407" id="Seg_1463" n="HIAT:u" s="T396">
                  <ts e="T397" id="Seg_1465" n="HIAT:w" s="T396">Na</ts>
                  <nts id="Seg_1466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1468" n="HIAT:w" s="T397">iːja</ts>
                  <nts id="Seg_1469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1471" n="HIAT:w" s="T398">lʼamɨk</ts>
                  <nts id="Seg_1472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1474" n="HIAT:w" s="T399">kəːŋa</ts>
                  <nts id="Seg_1475" n="HIAT:ip">,</nts>
                  <nts id="Seg_1476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1478" n="HIAT:w" s="T400">nʼännät</ts>
                  <nts id="Seg_1479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1481" n="HIAT:w" s="T401">ukkɨr</ts>
                  <nts id="Seg_1482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1484" n="HIAT:w" s="T402">tot</ts>
                  <nts id="Seg_1485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1487" n="HIAT:w" s="T403">čʼontoːqɨt</ts>
                  <nts id="Seg_1488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1490" n="HIAT:w" s="T404">qup</ts>
                  <nts id="Seg_1491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1493" n="HIAT:w" s="T405">najap</ts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1496" n="HIAT:w" s="T406">tatɨntɔːtɨt</ts>
                  <nts id="Seg_1497" n="HIAT:ip">.</nts>
                  <nts id="Seg_1498" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T411" id="Seg_1500" n="HIAT:u" s="T407">
                  <ts e="T408" id="Seg_1502" n="HIAT:w" s="T407">Takkɨnɨ</ts>
                  <nts id="Seg_1503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1505" n="HIAT:w" s="T408">Pan</ts>
                  <nts id="Seg_1506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1508" n="HIAT:w" s="T409">iralʼmɨt</ts>
                  <nts id="Seg_1509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1511" n="HIAT:w" s="T410">tatɨntɔːtɨt</ts>
                  <nts id="Seg_1512" n="HIAT:ip">.</nts>
                  <nts id="Seg_1513" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T416" id="Seg_1515" n="HIAT:u" s="T411">
                  <ts e="T412" id="Seg_1517" n="HIAT:w" s="T411">Köt</ts>
                  <nts id="Seg_1518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1520" n="HIAT:w" s="T412">qumtɨ</ts>
                  <nts id="Seg_1521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1523" n="HIAT:w" s="T413">qapıja</ts>
                  <nts id="Seg_1524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1526" n="HIAT:w" s="T414">ɔːtäp</ts>
                  <nts id="Seg_1527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1529" n="HIAT:w" s="T415">tɔːqqɨsɔːtɨlʼ</ts>
                  <nts id="Seg_1530" n="HIAT:ip">.</nts>
                  <nts id="Seg_1531" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T421" id="Seg_1533" n="HIAT:u" s="T416">
                  <ts e="T417" id="Seg_1535" n="HIAT:w" s="T416">Pan</ts>
                  <nts id="Seg_1536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1538" n="HIAT:w" s="T417">ira</ts>
                  <nts id="Seg_1539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1541" n="HIAT:w" s="T418">naššak</ts>
                  <nts id="Seg_1542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1544" n="HIAT:w" s="T419">qəːtɨsä</ts>
                  <nts id="Seg_1545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1547" n="HIAT:w" s="T420">ɛːŋa</ts>
                  <nts id="Seg_1548" n="HIAT:ip">.</nts>
                  <nts id="Seg_1549" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T426" id="Seg_1551" n="HIAT:u" s="T421">
                  <ts e="T422" id="Seg_1553" n="HIAT:w" s="T421">Nɨn</ts>
                  <nts id="Seg_1554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1556" n="HIAT:w" s="T422">šittalʼ</ts>
                  <nts id="Seg_1557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1559" n="HIAT:w" s="T423">na</ts>
                  <nts id="Seg_1560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1562" n="HIAT:w" s="T424">ija</ts>
                  <nts id="Seg_1563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1565" n="HIAT:w" s="T425">tülʼčʼikunä</ts>
                  <nts id="Seg_1566" n="HIAT:ip">.</nts>
                  <nts id="Seg_1567" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T428" id="Seg_1569" n="HIAT:u" s="T426">
                  <ts e="T427" id="Seg_1571" n="HIAT:w" s="T426">İmatä</ts>
                  <nts id="Seg_1572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1574" n="HIAT:w" s="T427">üŋkɨltimpatɨ</ts>
                  <nts id="Seg_1575" n="HIAT:ip">.</nts>
                  <nts id="Seg_1576" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T436" id="Seg_1578" n="HIAT:u" s="T428">
                  <ts e="T429" id="Seg_1580" n="HIAT:w" s="T428">Montɨ</ts>
                  <nts id="Seg_1581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1583" n="HIAT:w" s="T429">čʼap</ts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1586" n="HIAT:w" s="T430">tüːlʼčʼa</ts>
                  <nts id="Seg_1587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1589" n="HIAT:w" s="T431">Pan</ts>
                  <nts id="Seg_1590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1592" n="HIAT:w" s="T432">iralʼmɨt</ts>
                  <nts id="Seg_1593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1595" n="HIAT:w" s="T433">qaj</ts>
                  <nts id="Seg_1596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1598" n="HIAT:w" s="T434">montɨ</ts>
                  <nts id="Seg_1599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1601" n="HIAT:w" s="T435">tüŋɔːtɨt</ts>
                  <nts id="Seg_1602" n="HIAT:ip">.</nts>
                  <nts id="Seg_1603" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T442" id="Seg_1605" n="HIAT:u" s="T436">
                  <ts e="T437" id="Seg_1607" n="HIAT:w" s="T436">Qaqlʼilʼ</ts>
                  <nts id="Seg_1608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1610" n="HIAT:w" s="T437">mɨtɨ</ts>
                  <nts id="Seg_1611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1613" n="HIAT:w" s="T438">nɨk</ts>
                  <nts id="Seg_1614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1616" n="HIAT:w" s="T439">tottälnɔːtɨt</ts>
                  <nts id="Seg_1617" n="HIAT:ip">,</nts>
                  <nts id="Seg_1618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1620" n="HIAT:w" s="T440">poːqɨt</ts>
                  <nts id="Seg_1621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1623" n="HIAT:w" s="T441">eːtɨmantoːqɨt</ts>
                  <nts id="Seg_1624" n="HIAT:ip">.</nts>
                  <nts id="Seg_1625" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T444" id="Seg_1627" n="HIAT:u" s="T442">
                  <ts e="T443" id="Seg_1629" n="HIAT:w" s="T442">Mɔːttɨ</ts>
                  <nts id="Seg_1630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1632" n="HIAT:w" s="T443">šeːrna</ts>
                  <nts id="Seg_1633" n="HIAT:ip">.</nts>
                  <nts id="Seg_1634" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T452" id="Seg_1636" n="HIAT:u" s="T444">
                  <ts e="T445" id="Seg_1638" n="HIAT:w" s="T444">Mɔːtan</ts>
                  <nts id="Seg_1639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1641" n="HIAT:w" s="T445">ɔːt</ts>
                  <nts id="Seg_1642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1644" n="HIAT:w" s="T446">nɨŋkɨlʼa</ts>
                  <nts id="Seg_1645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1647" n="HIAT:w" s="T447">mɔːtan</ts>
                  <nts id="Seg_1648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1650" n="HIAT:w" s="T448">ɔːt</ts>
                  <nts id="Seg_1651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1653" n="HIAT:w" s="T449">kʼeː</ts>
                  <nts id="Seg_1654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1656" n="HIAT:w" s="T450">topɨmtɨ</ts>
                  <nts id="Seg_1657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1659" n="HIAT:w" s="T451">tupalkɨtɨ</ts>
                  <nts id="Seg_1660" n="HIAT:ip">.</nts>
                  <nts id="Seg_1661" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T455" id="Seg_1663" n="HIAT:u" s="T452">
                  <ts e="T453" id="Seg_1665" n="HIAT:w" s="T452">Peːmɨmtɨ</ts>
                  <nts id="Seg_1666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1668" n="HIAT:w" s="T453">na</ts>
                  <nts id="Seg_1669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1671" n="HIAT:w" s="T454">tupalnɨt</ts>
                  <nts id="Seg_1672" n="HIAT:ip">.</nts>
                  <nts id="Seg_1673" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T461" id="Seg_1675" n="HIAT:u" s="T455">
                  <ts e="T456" id="Seg_1677" n="HIAT:w" s="T455">Nı</ts>
                  <nts id="Seg_1678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1680" n="HIAT:w" s="T456">kəttɨŋɨtɨ</ts>
                  <nts id="Seg_1681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1683" n="HIAT:w" s="T457">Pan</ts>
                  <nts id="Seg_1684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1686" n="HIAT:w" s="T458">iranɨk</ts>
                  <nts id="Seg_1687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1689" n="HIAT:w" s="T459">nʼännä</ts>
                  <nts id="Seg_1690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1692" n="HIAT:w" s="T460">koraltɨlä</ts>
                  <nts id="Seg_1693" n="HIAT:ip">.</nts>
                  <nts id="Seg_1694" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T467" id="Seg_1696" n="HIAT:u" s="T461">
                  <ts e="T462" id="Seg_1698" n="HIAT:w" s="T461">Ɔːklɨ</ts>
                  <nts id="Seg_1699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1701" n="HIAT:w" s="T462">miːt</ts>
                  <nts id="Seg_1702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1704" n="HIAT:w" s="T463">olʼlʼa</ts>
                  <nts id="Seg_1705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1707" n="HIAT:w" s="T464">qolʼtɨt</ts>
                  <nts id="Seg_1708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1710" n="HIAT:w" s="T465">qəqɨt</ts>
                  <nts id="Seg_1711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1713" n="HIAT:w" s="T466">oːlʼa</ts>
                  <nts id="Seg_1714" n="HIAT:ip">.</nts>
                  <nts id="Seg_1715" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T474" id="Seg_1717" n="HIAT:u" s="T467">
                  <ts e="T468" id="Seg_1719" n="HIAT:w" s="T467">Taːlʼ</ts>
                  <nts id="Seg_1720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1722" n="HIAT:w" s="T468">na</ts>
                  <nts id="Seg_1723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1725" n="HIAT:w" s="T469">nʼänna</ts>
                  <nts id="Seg_1726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1728" n="HIAT:w" s="T470">paqtɨmpa</ts>
                  <nts id="Seg_1729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1731" n="HIAT:w" s="T471">na</ts>
                  <nts id="Seg_1732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1734" n="HIAT:w" s="T472">kupaksä</ts>
                  <nts id="Seg_1735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1737" n="HIAT:w" s="T473">täqalemmɨntɨtɨ</ts>
                  <nts id="Seg_1738" n="HIAT:ip">.</nts>
                  <nts id="Seg_1739" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T480" id="Seg_1741" n="HIAT:u" s="T474">
                  <ts e="T475" id="Seg_1743" n="HIAT:w" s="T474">Pan</ts>
                  <nts id="Seg_1744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1746" n="HIAT:w" s="T475">iralʼ</ts>
                  <nts id="Seg_1747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1749" n="HIAT:w" s="T476">mɨtɨp</ts>
                  <nts id="Seg_1750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1752" n="HIAT:w" s="T477">muːntɨkə</ts>
                  <nts id="Seg_1753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1755" n="HIAT:w" s="T478">lʼäpek</ts>
                  <nts id="Seg_1756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1758" n="HIAT:w" s="T479">qättɨmpat</ts>
                  <nts id="Seg_1759" n="HIAT:ip">.</nts>
                  <nts id="Seg_1760" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T486" id="Seg_1762" n="HIAT:u" s="T480">
                  <ts e="T481" id="Seg_1764" n="HIAT:w" s="T480">Ɔːmmɨtɨ</ts>
                  <nts id="Seg_1765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1767" n="HIAT:w" s="T481">qumpa</ts>
                  <nts id="Seg_1768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1770" n="HIAT:w" s="T482">Pan</ts>
                  <nts id="Seg_1771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1773" n="HIAT:w" s="T483">ira</ts>
                  <nts id="Seg_1774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1776" n="HIAT:w" s="T484">ilʼilä</ts>
                  <nts id="Seg_1777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1779" n="HIAT:w" s="T485">qalɨmpa</ts>
                  <nts id="Seg_1780" n="HIAT:ip">.</nts>
                  <nts id="Seg_1781" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T489" id="Seg_1783" n="HIAT:u" s="T486">
                  <ts e="T487" id="Seg_1785" n="HIAT:w" s="T486">Šittäqij</ts>
                  <nts id="Seg_1786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1788" n="HIAT:w" s="T487">ilɨlä</ts>
                  <nts id="Seg_1789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1791" n="HIAT:w" s="T488">qaːlɨmmɨnta</ts>
                  <nts id="Seg_1792" n="HIAT:ip">.</nts>
                  <nts id="Seg_1793" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T491" id="Seg_1795" n="HIAT:u" s="T489">
                  <ts e="T490" id="Seg_1797" n="HIAT:w" s="T489">Ponä</ts>
                  <nts id="Seg_1798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1800" n="HIAT:w" s="T490">qəlläitɨ</ts>
                  <nts id="Seg_1801" n="HIAT:ip">.</nts>
                  <nts id="Seg_1802" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T497" id="Seg_1804" n="HIAT:u" s="T491">
                  <ts e="T492" id="Seg_1806" n="HIAT:w" s="T491">Na</ts>
                  <nts id="Seg_1807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1809" n="HIAT:w" s="T492">iːja</ts>
                  <nts id="Seg_1810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1812" n="HIAT:w" s="T493">ukkur</ts>
                  <nts id="Seg_1813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1815" n="HIAT:w" s="T494">pɔːrɨ</ts>
                  <nts id="Seg_1816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1818" n="HIAT:w" s="T495">kupaksä</ts>
                  <nts id="Seg_1819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1821" n="HIAT:w" s="T496">täqalempatɨ</ts>
                  <nts id="Seg_1822" n="HIAT:ip">.</nts>
                  <nts id="Seg_1823" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T503" id="Seg_1825" n="HIAT:u" s="T497">
                  <ts e="T498" id="Seg_1827" n="HIAT:w" s="T497">Muntɨk</ts>
                  <nts id="Seg_1828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1830" n="HIAT:w" s="T498">porqäntɨ</ts>
                  <nts id="Seg_1831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1833" n="HIAT:w" s="T499">šunʼnʼontɨ</ts>
                  <nts id="Seg_1834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1835" n="HIAT:ip">(</nts>
                  <ts e="T501" id="Seg_1837" n="HIAT:w" s="T500">pümmɨntɨ</ts>
                  <nts id="Seg_1838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1840" n="HIAT:w" s="T501">šʼu</ts>
                  <nts id="Seg_1841" n="HIAT:ip">)</nts>
                  <nts id="Seg_1842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1844" n="HIAT:w" s="T502">nʼölʼqɨmɔːtpotät</ts>
                  <nts id="Seg_1845" n="HIAT:ip">.</nts>
                  <nts id="Seg_1846" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T509" id="Seg_1848" n="HIAT:u" s="T503">
                  <ts e="T504" id="Seg_1850" n="HIAT:w" s="T503">Na</ts>
                  <nts id="Seg_1851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1853" n="HIAT:w" s="T504">qupɨlʼmɨt</ts>
                  <nts id="Seg_1854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1856" n="HIAT:w" s="T505">nɨnɨ</ts>
                  <nts id="Seg_1857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1859" n="HIAT:w" s="T506">šitälʼ</ts>
                  <nts id="Seg_1860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1862" n="HIAT:w" s="T507">ponä</ts>
                  <nts id="Seg_1863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1865" n="HIAT:w" s="T508">kälʼlʼeitɨ</ts>
                  <nts id="Seg_1866" n="HIAT:ip">.</nts>
                  <nts id="Seg_1867" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T516" id="Seg_1869" n="HIAT:u" s="T509">
                  <ts e="T510" id="Seg_1871" n="HIAT:w" s="T509">Eːtɨmannoːntɨ</ts>
                  <nts id="Seg_1872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1874" n="HIAT:w" s="T510">tintena</ts>
                  <nts id="Seg_1875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1877" n="HIAT:w" s="T511">qaqlɨ</ts>
                  <nts id="Seg_1878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1880" n="HIAT:w" s="T512">tüːr</ts>
                  <nts id="Seg_1881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1883" n="HIAT:w" s="T513">tolʼčʼitɨ</ts>
                  <nts id="Seg_1884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1886" n="HIAT:w" s="T514">na</ts>
                  <nts id="Seg_1887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1889" n="HIAT:w" s="T515">sɔːrälɨmpa</ts>
                  <nts id="Seg_1890" n="HIAT:ip">.</nts>
                  <nts id="Seg_1891" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T522" id="Seg_1893" n="HIAT:u" s="T516">
                  <ts e="T517" id="Seg_1895" n="HIAT:w" s="T516">Pan</ts>
                  <nts id="Seg_1896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1898" n="HIAT:w" s="T517">irap</ts>
                  <nts id="Seg_1899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1901" n="HIAT:w" s="T518">aj</ts>
                  <nts id="Seg_1902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1904" n="HIAT:w" s="T519">ponä</ts>
                  <nts id="Seg_1905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1907" n="HIAT:w" s="T520">itɨmpat</ts>
                  <nts id="Seg_1908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1909" n="HIAT:ip">(</nts>
                  <ts e="T522" id="Seg_1911" n="HIAT:w" s="T521">čʼatɨmpat</ts>
                  <nts id="Seg_1912" n="HIAT:ip">)</nts>
                  <nts id="Seg_1913" n="HIAT:ip">.</nts>
                  <nts id="Seg_1914" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T526" id="Seg_1916" n="HIAT:u" s="T522">
                  <ts e="T523" id="Seg_1918" n="HIAT:w" s="T522">Qapija</ts>
                  <nts id="Seg_1919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1921" n="HIAT:w" s="T523">muntɨk</ts>
                  <nts id="Seg_1922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1924" n="HIAT:w" s="T524">pona</ts>
                  <nts id="Seg_1925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1927" n="HIAT:w" s="T525">iːtɨmpat</ts>
                  <nts id="Seg_1928" n="HIAT:ip">.</nts>
                  <nts id="Seg_1929" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T532" id="Seg_1931" n="HIAT:u" s="T526">
                  <ts e="T527" id="Seg_1933" n="HIAT:w" s="T526">Illa</ts>
                  <nts id="Seg_1934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_1936" n="HIAT:w" s="T527">omta</ts>
                  <nts id="Seg_1937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1939" n="HIAT:w" s="T528">malʼčʼalʼ</ts>
                  <nts id="Seg_1940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_1942" n="HIAT:w" s="T529">tɔːqtɨ</ts>
                  <nts id="Seg_1943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_1945" n="HIAT:w" s="T530">toː</ts>
                  <nts id="Seg_1946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_1948" n="HIAT:w" s="T531">iːlʼä</ts>
                  <nts id="Seg_1949" n="HIAT:ip">.</nts>
                  <nts id="Seg_1950" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T536" id="Seg_1952" n="HIAT:u" s="T532">
                  <ts e="T533" id="Seg_1954" n="HIAT:w" s="T532">Amɨrlʼä</ts>
                  <nts id="Seg_1955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_1957" n="HIAT:w" s="T533">ıllä</ts>
                  <nts id="Seg_1958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_1960" n="HIAT:w" s="T534">ommɨnta</ts>
                  <nts id="Seg_1961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1963" n="HIAT:w" s="T535">šäqqa</ts>
                  <nts id="Seg_1964" n="HIAT:ip">.</nts>
                  <nts id="Seg_1965" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T541" id="Seg_1967" n="HIAT:u" s="T536">
                  <ts e="T537" id="Seg_1969" n="HIAT:w" s="T536">Na</ts>
                  <nts id="Seg_1970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_1972" n="HIAT:w" s="T537">qumiːtɨ</ts>
                  <nts id="Seg_1973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_1975" n="HIAT:w" s="T538">poːqɨt</ts>
                  <nts id="Seg_1976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_1978" n="HIAT:w" s="T539">na</ts>
                  <nts id="Seg_1979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_1981" n="HIAT:w" s="T540">ippälɨmpɔːtɨt</ts>
                  <nts id="Seg_1982" n="HIAT:ip">.</nts>
                  <nts id="Seg_1983" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T543" id="Seg_1985" n="HIAT:u" s="T541">
                  <ts e="T542" id="Seg_1987" n="HIAT:w" s="T541">Kuttar</ts>
                  <nts id="Seg_1988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_1990" n="HIAT:w" s="T542">sarälɨmpa</ts>
                  <nts id="Seg_1991" n="HIAT:ip">.</nts>
                  <nts id="Seg_1992" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T545" id="Seg_1994" n="HIAT:u" s="T543">
                  <ts e="T544" id="Seg_1996" n="HIAT:w" s="T543">Nılʼčʼik</ts>
                  <nts id="Seg_1997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_1999" n="HIAT:w" s="T544">šäqqa</ts>
                  <nts id="Seg_2000" n="HIAT:ip">.</nts>
                  <nts id="Seg_2001" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T549" id="Seg_2003" n="HIAT:u" s="T545">
                  <ts e="T546" id="Seg_2005" n="HIAT:w" s="T545">Tɔːptɨlʼ</ts>
                  <nts id="Seg_2006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_2008" n="HIAT:w" s="T546">qarɨt</ts>
                  <nts id="Seg_2009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_2011" n="HIAT:w" s="T547">ɨnnä</ts>
                  <nts id="Seg_2012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_2014" n="HIAT:w" s="T548">wäša</ts>
                  <nts id="Seg_2015" n="HIAT:ip">.</nts>
                  <nts id="Seg_2016" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T560" id="Seg_2018" n="HIAT:u" s="T549">
                  <ts e="T550" id="Seg_2020" n="HIAT:w" s="T549">Pan</ts>
                  <nts id="Seg_2021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_2023" n="HIAT:w" s="T550">ira</ts>
                  <nts id="Seg_2024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2026" n="HIAT:w" s="T551">nılʼčʼik</ts>
                  <nts id="Seg_2027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_2029" n="HIAT:w" s="T552">tap</ts>
                  <nts id="Seg_2030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2032" n="HIAT:w" s="T553">wäntɨlʼ</ts>
                  <nts id="Seg_2033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_2035" n="HIAT:w" s="T554">tɔːktɨ</ts>
                  <nts id="Seg_2036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2038" n="HIAT:w" s="T555">tɔːtɨk</ts>
                  <nts id="Seg_2039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_2041" n="HIAT:w" s="T556">taŋkıːčʼimpa</ts>
                  <nts id="Seg_2042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_2044" n="HIAT:w" s="T557">kekkɨsä</ts>
                  <nts id="Seg_2045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2047" n="HIAT:w" s="T558">sailʼatɨ</ts>
                  <nts id="Seg_2048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_2050" n="HIAT:w" s="T559">qətɨmpa</ts>
                  <nts id="Seg_2051" n="HIAT:ip">.</nts>
                  <nts id="Seg_2052" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T565" id="Seg_2054" n="HIAT:u" s="T560">
                  <ts e="T561" id="Seg_2056" n="HIAT:w" s="T560">Toːna</ts>
                  <nts id="Seg_2057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2059" n="HIAT:w" s="T561">qälɨkqıtɨ</ts>
                  <nts id="Seg_2060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_2062" n="HIAT:w" s="T562">ešo</ts>
                  <nts id="Seg_2063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2065" n="HIAT:w" s="T563">somalʼoqı</ts>
                  <nts id="Seg_2066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2068" n="HIAT:w" s="T564">ɛppɨmpa</ts>
                  <nts id="Seg_2069" n="HIAT:ip">.</nts>
                  <nts id="Seg_2070" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T572" id="Seg_2072" n="HIAT:u" s="T565">
                  <ts e="T566" id="Seg_2074" n="HIAT:w" s="T565">Nılʼ</ts>
                  <nts id="Seg_2075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2077" n="HIAT:w" s="T566">kətɨŋɨtɨ</ts>
                  <nts id="Seg_2078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_2080" n="HIAT:w" s="T567">na</ts>
                  <nts id="Seg_2081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_2083" n="HIAT:w" s="T568">ija</ts>
                  <nts id="Seg_2084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2086" n="HIAT:w" s="T569">mɨta</ts>
                  <nts id="Seg_2087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2089" n="HIAT:w" s="T570">na</ts>
                  <nts id="Seg_2090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2092" n="HIAT:w" s="T571">qälʼitɨtkiːnä</ts>
                  <nts id="Seg_2093" n="HIAT:ip">.</nts>
                  <nts id="Seg_2094" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T577" id="Seg_2096" n="HIAT:u" s="T572">
                  <ts e="T573" id="Seg_2098" n="HIAT:w" s="T572">Moqonä</ts>
                  <nts id="Seg_2099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2101" n="HIAT:w" s="T573">qəntat</ts>
                  <nts id="Seg_2102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_2104" n="HIAT:w" s="T574">talʼtälʼlʼä</ts>
                  <nts id="Seg_2105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2107" n="HIAT:w" s="T575">untɨt</ts>
                  <nts id="Seg_2108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2110" n="HIAT:w" s="T576">qaqlɨntɨsä</ts>
                  <nts id="Seg_2111" n="HIAT:ip">.</nts>
                  <nts id="Seg_2112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T583" id="Seg_2114" n="HIAT:u" s="T577">
                  <ts e="T578" id="Seg_2116" n="HIAT:w" s="T577">Nɨnɨ</ts>
                  <nts id="Seg_2117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2119" n="HIAT:w" s="T578">šittälʼ</ts>
                  <nts id="Seg_2120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2122" n="HIAT:w" s="T579">toːna</ts>
                  <nts id="Seg_2123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2125" n="HIAT:w" s="T580">meːqıj</ts>
                  <nts id="Seg_2126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2128" n="HIAT:w" s="T581">mɔːttɨ</ts>
                  <nts id="Seg_2129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2131" n="HIAT:w" s="T582">šeːrɨšpɔːtɨt</ts>
                  <nts id="Seg_2132" n="HIAT:ip">.</nts>
                  <nts id="Seg_2133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T587" id="Seg_2135" n="HIAT:u" s="T583">
                  <ts e="T584" id="Seg_2137" n="HIAT:w" s="T583">Qaj</ts>
                  <nts id="Seg_2138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2140" n="HIAT:w" s="T584">amɨrpɔːtɨt</ts>
                  <nts id="Seg_2141" n="HIAT:ip">,</nts>
                  <nts id="Seg_2142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2144" n="HIAT:w" s="T585">qaj</ts>
                  <nts id="Seg_2145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2147" n="HIAT:w" s="T586">aš</ts>
                  <nts id="Seg_2148" n="HIAT:ip">.</nts>
                  <nts id="Seg_2149" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T600" id="Seg_2151" n="HIAT:u" s="T587">
                  <ts e="T588" id="Seg_2153" n="HIAT:w" s="T587">Nɨnɨ</ts>
                  <nts id="Seg_2154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2156" n="HIAT:w" s="T588">tintena</ts>
                  <nts id="Seg_2157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2159" n="HIAT:w" s="T589">saralpɨtɨlʼ</ts>
                  <nts id="Seg_2160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2162" n="HIAT:w" s="T590">qaqlɨmtɨt</ts>
                  <nts id="Seg_2163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2165" n="HIAT:w" s="T591">nɨnɨ</ts>
                  <nts id="Seg_2166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2168" n="HIAT:w" s="T592">moqonä</ts>
                  <nts id="Seg_2169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2171" n="HIAT:w" s="T593">qəːčʼelʼlʼä</ts>
                  <nts id="Seg_2172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2174" n="HIAT:w" s="T594">na</ts>
                  <nts id="Seg_2175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2177" n="HIAT:w" s="T595">qumiːmtɨt</ts>
                  <nts id="Seg_2178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2180" n="HIAT:w" s="T596">qupɨlʼ</ts>
                  <nts id="Seg_2181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2183" n="HIAT:w" s="T597">qumiːmtɨ</ts>
                  <nts id="Seg_2184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2186" n="HIAT:w" s="T598">nık</ts>
                  <nts id="Seg_2187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2189" n="HIAT:w" s="T599">qəntɨŋɨt</ts>
                  <nts id="Seg_2190" n="HIAT:ip">.</nts>
                  <nts id="Seg_2191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T607" id="Seg_2193" n="HIAT:u" s="T600">
                  <ts e="T601" id="Seg_2195" n="HIAT:w" s="T600">Takkɨ</ts>
                  <nts id="Seg_2196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2198" n="HIAT:w" s="T601">moqonä</ts>
                  <nts id="Seg_2199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2201" n="HIAT:w" s="T602">Pan</ts>
                  <nts id="Seg_2202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2204" n="HIAT:w" s="T603">iramtɨ</ts>
                  <nts id="Seg_2205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2207" n="HIAT:w" s="T604">aj</ts>
                  <nts id="Seg_2208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2210" n="HIAT:w" s="T605">kočʼilʼä</ts>
                  <nts id="Seg_2211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2213" n="HIAT:w" s="T606">qəntɨtɨ</ts>
                  <nts id="Seg_2214" n="HIAT:ip">.</nts>
                  <nts id="Seg_2215" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T611" id="Seg_2217" n="HIAT:u" s="T607">
                  <ts e="T608" id="Seg_2219" n="HIAT:w" s="T607">Qaj</ts>
                  <nts id="Seg_2220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2222" n="HIAT:w" s="T608">kɨka</ts>
                  <nts id="Seg_2223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2225" n="HIAT:w" s="T609">qəjitɨ</ts>
                  <nts id="Seg_2226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2228" n="HIAT:w" s="T610">qəntɨt</ts>
                  <nts id="Seg_2229" n="HIAT:ip">.</nts>
                  <nts id="Seg_2230" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T613" id="Seg_2232" n="HIAT:u" s="T611">
                  <ts e="T612" id="Seg_2234" n="HIAT:w" s="T611">Qənnatɨt</ts>
                  <nts id="Seg_2235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2237" n="HIAT:w" s="T612">tına</ts>
                  <nts id="Seg_2238" n="HIAT:ip">.</nts>
                  <nts id="Seg_2239" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T619" id="Seg_2241" n="HIAT:u" s="T613">
                  <ts e="T614" id="Seg_2243" n="HIAT:w" s="T613">Qumat</ts>
                  <nts id="Seg_2244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2246" n="HIAT:w" s="T614">somak</ts>
                  <nts id="Seg_2247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2249" n="HIAT:w" s="T615">qənnɔːtɨt</ts>
                  <nts id="Seg_2250" n="HIAT:ip">,</nts>
                  <nts id="Seg_2251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2253" n="HIAT:w" s="T616">imantɨnɨ</ts>
                  <nts id="Seg_2254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2256" n="HIAT:w" s="T617">nık</ts>
                  <nts id="Seg_2257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2259" n="HIAT:w" s="T618">kətɨŋɨt</ts>
                  <nts id="Seg_2260" n="HIAT:ip">.</nts>
                  <nts id="Seg_2261" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T626" id="Seg_2263" n="HIAT:u" s="T619">
                  <nts id="Seg_2264" n="HIAT:ip">“</nts>
                  <ts e="T620" id="Seg_2266" n="HIAT:w" s="T619">Mɔːlle</ts>
                  <nts id="Seg_2267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2269" n="HIAT:w" s="T620">toː</ts>
                  <nts id="Seg_2270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2272" n="HIAT:w" s="T621">tılätɨ</ts>
                  <nts id="Seg_2273" n="HIAT:ip">,</nts>
                  <nts id="Seg_2274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2276" n="HIAT:w" s="T622">me</ts>
                  <nts id="Seg_2277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2279" n="HIAT:w" s="T623">nʼänna</ts>
                  <nts id="Seg_2280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2282" n="HIAT:w" s="T624">qəlʼlʼä</ts>
                  <nts id="Seg_2283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2285" n="HIAT:w" s="T625">qumɨtɨtkiːne</ts>
                  <nts id="Seg_2286" n="HIAT:ip">”</nts>
                  <nts id="Seg_2287" n="HIAT:ip">.</nts>
                  <nts id="Seg_2288" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T632" id="Seg_2290" n="HIAT:u" s="T626">
                  <ts e="T627" id="Seg_2292" n="HIAT:w" s="T626">Aš</ts>
                  <nts id="Seg_2293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2295" n="HIAT:w" s="T627">tɛnimɨmpatɨ</ts>
                  <nts id="Seg_2296" n="HIAT:ip">,</nts>
                  <nts id="Seg_2297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2299" n="HIAT:w" s="T628">ilʼčʼantɨlʼ</ts>
                  <nts id="Seg_2300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2302" n="HIAT:w" s="T629">mɨt</ts>
                  <nts id="Seg_2303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2305" n="HIAT:w" s="T630">qos</ts>
                  <nts id="Seg_2306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2308" n="HIAT:w" s="T631">qaj</ts>
                  <nts id="Seg_2309" n="HIAT:ip">.</nts>
                  <nts id="Seg_2310" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T635" id="Seg_2312" n="HIAT:u" s="T632">
                  <ts e="T633" id="Seg_2314" n="HIAT:w" s="T632">Nʼenna</ts>
                  <nts id="Seg_2315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2317" n="HIAT:w" s="T633">qənna</ts>
                  <nts id="Seg_2318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2320" n="HIAT:w" s="T634">tintena</ts>
                  <nts id="Seg_2321" n="HIAT:ip">.</nts>
                  <nts id="Seg_2322" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T638" id="Seg_2324" n="HIAT:u" s="T635">
                  <ts e="T636" id="Seg_2326" n="HIAT:w" s="T635">Onta</ts>
                  <nts id="Seg_2327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2329" n="HIAT:w" s="T636">aša</ts>
                  <nts id="Seg_2330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2332" n="HIAT:w" s="T637">koŋɨmpa</ts>
                  <nts id="Seg_2333" n="HIAT:ip">.</nts>
                  <nts id="Seg_2334" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T642" id="Seg_2336" n="HIAT:u" s="T638">
                  <ts e="T639" id="Seg_2338" n="HIAT:w" s="T638">Toːna</ts>
                  <nts id="Seg_2339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2341" n="HIAT:w" s="T639">imatɨ</ts>
                  <nts id="Seg_2342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2344" n="HIAT:w" s="T640">na</ts>
                  <nts id="Seg_2345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2347" n="HIAT:w" s="T641">kətälmɨntɨtɨ</ts>
                  <nts id="Seg_2348" n="HIAT:ip">.</nts>
                  <nts id="Seg_2349" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T645" id="Seg_2351" n="HIAT:u" s="T642">
                  <ts e="T643" id="Seg_2353" n="HIAT:w" s="T642">Əsɨntɨlʼ</ts>
                  <nts id="Seg_2354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2356" n="HIAT:w" s="T643">ilʼčʼantɨlʼ</ts>
                  <nts id="Seg_2357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2359" n="HIAT:w" s="T644">mɨqätkiːne</ts>
                  <nts id="Seg_2360" n="HIAT:ip">.</nts>
                  <nts id="Seg_2361" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T649" id="Seg_2363" n="HIAT:u" s="T645">
                  <ts e="T646" id="Seg_2365" n="HIAT:w" s="T645">İralʼ</ts>
                  <nts id="Seg_2366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2368" n="HIAT:w" s="T646">mɨt</ts>
                  <nts id="Seg_2369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2371" n="HIAT:w" s="T647">moqonä</ts>
                  <nts id="Seg_2372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2374" n="HIAT:w" s="T648">qəssɔːtɨt</ts>
                  <nts id="Seg_2375" n="HIAT:ip">.</nts>
                  <nts id="Seg_2376" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T658" id="Seg_2378" n="HIAT:u" s="T649">
                  <ts e="T650" id="Seg_2380" n="HIAT:w" s="T649">A</ts>
                  <nts id="Seg_2381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_2383" n="HIAT:w" s="T650">nɨnä</ts>
                  <nts id="Seg_2384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2386" n="HIAT:w" s="T651">šittelʼ</ts>
                  <nts id="Seg_2387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2389" n="HIAT:w" s="T652">pijaka</ts>
                  <nts id="Seg_2390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2392" n="HIAT:w" s="T653">pija</ts>
                  <nts id="Seg_2393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2395" n="HIAT:w" s="T654">ni</ts>
                  <nts id="Seg_2396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2398" n="HIAT:w" s="T655">qaj</ts>
                  <nts id="Seg_2399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2401" n="HIAT:w" s="T656">aš</ts>
                  <nts id="Seg_2402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2404" n="HIAT:w" s="T657">tɛnimɔːtɨt</ts>
                  <nts id="Seg_2405" n="HIAT:ip">.</nts>
                  <nts id="Seg_2406" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T660" id="Seg_2408" n="HIAT:u" s="T658">
                  <ts e="T659" id="Seg_2410" n="HIAT:w" s="T658">Ta</ts>
                  <nts id="Seg_2411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2413" n="HIAT:w" s="T659">qənna</ts>
                  <nts id="Seg_2414" n="HIAT:ip">.</nts>
                  <nts id="Seg_2415" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T664" id="Seg_2417" n="HIAT:u" s="T660">
                  <ts e="T661" id="Seg_2419" n="HIAT:w" s="T660">Täpɨt</ts>
                  <nts id="Seg_2420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2422" n="HIAT:w" s="T661">ilʼčʼantɨ</ts>
                  <nts id="Seg_2423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2425" n="HIAT:w" s="T662">mɔːttɨ</ts>
                  <nts id="Seg_2426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2428" n="HIAT:w" s="T663">šernɔːqı</ts>
                  <nts id="Seg_2429" n="HIAT:ip">.</nts>
                  <nts id="Seg_2430" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T669" id="Seg_2432" n="HIAT:u" s="T664">
                  <ts e="T665" id="Seg_2434" n="HIAT:w" s="T664">Qaqlɨlʼ</ts>
                  <nts id="Seg_2435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2437" n="HIAT:w" s="T665">tɔːktɨ</ts>
                  <nts id="Seg_2438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2440" n="HIAT:w" s="T666">kurɨlʼä</ts>
                  <nts id="Seg_2441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2443" n="HIAT:w" s="T667">nılʼčʼik</ts>
                  <nts id="Seg_2444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2446" n="HIAT:w" s="T668">tottɔːtɨt</ts>
                  <nts id="Seg_2447" n="HIAT:ip">.</nts>
                  <nts id="Seg_2448" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T674" id="Seg_2450" n="HIAT:u" s="T669">
                  <ts e="T670" id="Seg_2452" n="HIAT:w" s="T669">Toːna</ts>
                  <nts id="Seg_2453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2455" n="HIAT:w" s="T670">qənmɨntɔːtɨt</ts>
                  <nts id="Seg_2456" n="HIAT:ip">,</nts>
                  <nts id="Seg_2457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2459" n="HIAT:w" s="T671">moqɨnä</ts>
                  <nts id="Seg_2460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2462" n="HIAT:w" s="T672">na</ts>
                  <nts id="Seg_2463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2465" n="HIAT:w" s="T673">qənmɨntɔːtɨt</ts>
                  <nts id="Seg_2466" n="HIAT:ip">.</nts>
                  <nts id="Seg_2467" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T682" id="Seg_2469" n="HIAT:u" s="T674">
                  <ts e="T675" id="Seg_2471" n="HIAT:w" s="T674">Pan</ts>
                  <nts id="Seg_2472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2474" n="HIAT:w" s="T675">irä</ts>
                  <nts id="Seg_2475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2477" n="HIAT:w" s="T676">takkɨt</ts>
                  <nts id="Seg_2478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2480" n="HIAT:w" s="T677">kekkɨsa</ts>
                  <nts id="Seg_2481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2483" n="HIAT:w" s="T678">mɔːtqɨntɨ</ts>
                  <nts id="Seg_2484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2486" n="HIAT:w" s="T679">tulʼišpa</ts>
                  <nts id="Seg_2487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2489" n="HIAT:w" s="T680">ıllä</ts>
                  <nts id="Seg_2490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2492" n="HIAT:w" s="T681">qumpa</ts>
                  <nts id="Seg_2493" n="HIAT:ip">.</nts>
                  <nts id="Seg_2494" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T686" id="Seg_2496" n="HIAT:u" s="T682">
                  <ts e="T683" id="Seg_2498" n="HIAT:w" s="T682">Toːna</ts>
                  <nts id="Seg_2499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2501" n="HIAT:w" s="T683">šittɨ</ts>
                  <nts id="Seg_2502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2504" n="HIAT:w" s="T684">qumoːqı</ts>
                  <nts id="Seg_2505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2507" n="HIAT:w" s="T685">ilɨmpɔːqı</ts>
                  <nts id="Seg_2508" n="HIAT:ip">.</nts>
                  <nts id="Seg_2509" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T690" id="Seg_2511" n="HIAT:u" s="T686">
                  <ts e="T687" id="Seg_2513" n="HIAT:w" s="T686">Pan</ts>
                  <nts id="Seg_2514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2516" n="HIAT:w" s="T687">ira</ts>
                  <nts id="Seg_2517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2519" n="HIAT:w" s="T688">nimtɨ</ts>
                  <nts id="Seg_2520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2522" n="HIAT:w" s="T689">ɛːŋa</ts>
                  <nts id="Seg_2523" n="HIAT:ip">.</nts>
                  <nts id="Seg_2524" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T690" id="Seg_2525" n="sc" s="T0">
               <ts e="T1" id="Seg_2527" n="e" s="T0">Ukkɨr </ts>
               <ts e="T2" id="Seg_2529" n="e" s="T1">ijatɨ </ts>
               <ts e="T3" id="Seg_2531" n="e" s="T2">ɛppɨmmɨnta, </ts>
               <ts e="T4" id="Seg_2533" n="e" s="T3">neːkɨtɨlʼ </ts>
               <ts e="T5" id="Seg_2535" n="e" s="T4">pɛlɨkɨtɨlʼ. </ts>
               <ts e="T6" id="Seg_2537" n="e" s="T5">Na </ts>
               <ts e="T7" id="Seg_2539" n="e" s="T6">tɛntɨlʼ, </ts>
               <ts e="T8" id="Seg_2541" n="e" s="T7">aše </ts>
               <ts e="T9" id="Seg_2543" n="e" s="T8">čʼaptä. </ts>
               <ts e="T10" id="Seg_2545" n="e" s="T9">İra </ts>
               <ts e="T11" id="Seg_2547" n="e" s="T10">ilɨmpa, </ts>
               <ts e="T12" id="Seg_2549" n="e" s="T11">quːtalʼpa. </ts>
               <ts e="T13" id="Seg_2551" n="e" s="T12">Na </ts>
               <ts e="T14" id="Seg_2553" n="e" s="T13">šolʼqumɨlʼ </ts>
               <ts e="T15" id="Seg_2555" n="e" s="T14">ira </ts>
               <ts e="T16" id="Seg_2557" n="e" s="T15">tapplʼälʼ </ts>
               <ts e="T17" id="Seg_2559" n="e" s="T16">qolʼtit </ts>
               <ts e="T18" id="Seg_2561" n="e" s="T17">pɛlʼaqqɨt </ts>
               <ts e="T19" id="Seg_2563" n="e" s="T18">ilɨmpa. </ts>
               <ts e="T20" id="Seg_2565" n="e" s="T19">Poːkɨtɨlʼ </ts>
               <ts e="T21" id="Seg_2567" n="e" s="T20">tətɨt </ts>
               <ts e="T22" id="Seg_2569" n="e" s="T21">toːčʼiqɨt </ts>
               <ts e="T23" id="Seg_2571" n="e" s="T22">iːlɨmpa </ts>
               <ts e="T24" id="Seg_2573" n="e" s="T23">tintena </ts>
               <ts e="T25" id="Seg_2575" n="e" s="T24">qüːtälpa. </ts>
               <ts e="T26" id="Seg_2577" n="e" s="T25">Ukkɨr </ts>
               <ts e="T27" id="Seg_2579" n="e" s="T26">ijatɨ </ts>
               <ts e="T28" id="Seg_2581" n="e" s="T27">pɛlɨkɔːtɨlʼ </ts>
               <ts e="T29" id="Seg_2583" n="e" s="T28">neːkɨtɨlʼ. </ts>
               <ts e="T30" id="Seg_2585" n="e" s="T29">Ni </ts>
               <ts e="T31" id="Seg_2587" n="e" s="T30">nälʼatɨ </ts>
               <ts e="T32" id="Seg_2589" n="e" s="T31">čʼäŋka </ts>
               <ts e="T33" id="Seg_2591" n="e" s="T32">ni </ts>
               <ts e="T34" id="Seg_2593" n="e" s="T33">qajtɨ </ts>
               <ts e="T35" id="Seg_2595" n="e" s="T34">čʼäŋka. </ts>
               <ts e="T36" id="Seg_2597" n="e" s="T35">Nɨnɨ </ts>
               <ts e="T37" id="Seg_2599" n="e" s="T36">əsɨtɨ </ts>
               <ts e="T38" id="Seg_2601" n="e" s="T37">qüːtäla </ts>
               <ts e="T39" id="Seg_2603" n="e" s="T38">na </ts>
               <ts e="T40" id="Seg_2605" n="e" s="T39">ijalʼa. </ts>
               <ts e="T41" id="Seg_2607" n="e" s="T40">Na </ts>
               <ts e="T42" id="Seg_2609" n="e" s="T41">ija </ts>
               <ts e="T43" id="Seg_2611" n="e" s="T42">qapıja </ts>
               <ts e="T44" id="Seg_2613" n="e" s="T43">nɨmtɨ </ts>
               <ts e="T45" id="Seg_2615" n="e" s="T44">ila, </ts>
               <ts e="T46" id="Seg_2617" n="e" s="T45">əsɨntɨsä </ts>
               <ts e="T47" id="Seg_2619" n="e" s="T46">əmɨntɨsä. </ts>
               <ts e="T48" id="Seg_2621" n="e" s="T47">Tontɨ </ts>
               <ts e="T49" id="Seg_2623" n="e" s="T48">šitɨ </ts>
               <ts e="T50" id="Seg_2625" n="e" s="T49">qäːlɨlʼ </ts>
               <ts e="T51" id="Seg_2627" n="e" s="T50">timnʼäsɨt </ts>
               <ts e="T52" id="Seg_2629" n="e" s="T51">ɛppɨntɔːtɨt. </ts>
               <ts e="T693" id="Seg_2631" n="e" s="T52">Qäːlʼ </ts>
               <ts e="T53" id="Seg_2633" n="e" s="T693">irä </ts>
               <ts e="T54" id="Seg_2635" n="e" s="T53">puːčʼčʼɛnta. </ts>
               <ts e="T55" id="Seg_2637" n="e" s="T54">Tɨntäna </ts>
               <ts e="T56" id="Seg_2639" n="e" s="T55">ira </ts>
               <ts e="T57" id="Seg_2641" n="e" s="T56">ıllä </ts>
               <ts e="T58" id="Seg_2643" n="e" s="T57">quŋa. </ts>
               <ts e="T59" id="Seg_2645" n="e" s="T58">Tintena </ts>
               <ts e="T694" id="Seg_2647" n="e" s="T59">qäːlʼ </ts>
               <ts e="T60" id="Seg_2649" n="e" s="T694">ira </ts>
               <ts e="T61" id="Seg_2651" n="e" s="T60">kučʼčʼä </ts>
               <ts e="T62" id="Seg_2653" n="e" s="T61">qattɛnta. </ts>
               <ts e="T63" id="Seg_2655" n="e" s="T62">Toː </ts>
               <ts e="T64" id="Seg_2657" n="e" s="T63">timnʼäntɨnɨ </ts>
               <ts e="T65" id="Seg_2659" n="e" s="T64">puːŋa. </ts>
               <ts e="T66" id="Seg_2661" n="e" s="T65">“İrra </ts>
               <ts e="T67" id="Seg_2663" n="e" s="T66">qunta, </ts>
               <ts e="T68" id="Seg_2665" n="e" s="T67">ıllä </ts>
               <ts e="T69" id="Seg_2667" n="e" s="T68">meːŋɔːtɨt”. </ts>
               <ts e="T70" id="Seg_2669" n="e" s="T69">Šittälʼ </ts>
               <ts e="T71" id="Seg_2671" n="e" s="T70">imaqotatɨ </ts>
               <ts e="T72" id="Seg_2673" n="e" s="T71">qüːtälʼlʼä, </ts>
               <ts e="T73" id="Seg_2675" n="e" s="T72">aj </ts>
               <ts e="T74" id="Seg_2677" n="e" s="T73">ıllä </ts>
               <ts e="T75" id="Seg_2679" n="e" s="T74">quŋa. </ts>
               <ts e="T76" id="Seg_2681" n="e" s="T75">İralʼ </ts>
               <ts e="T77" id="Seg_2683" n="e" s="T76">mɨqä </ts>
               <ts e="T78" id="Seg_2685" n="e" s="T77">ijatɨt </ts>
               <ts e="T79" id="Seg_2687" n="e" s="T78">nejkɔːlɨ </ts>
               <ts e="T80" id="Seg_2689" n="e" s="T79">pɛlɨkɔːlɨ </ts>
               <ts e="T81" id="Seg_2691" n="e" s="T80">qala. </ts>
               <ts e="T82" id="Seg_2693" n="e" s="T81">Kučʼčʼä </ts>
               <ts e="T83" id="Seg_2695" n="e" s="T82">qattɛnta. </ts>
               <ts e="T84" id="Seg_2697" n="e" s="T83">Na </ts>
               <ts e="T695" id="Seg_2699" n="e" s="T84">qäːlʼi </ts>
               <ts e="T85" id="Seg_2701" n="e" s="T695">ira </ts>
               <ts e="T86" id="Seg_2703" n="e" s="T85">šittalʼ </ts>
               <ts e="T87" id="Seg_2705" n="e" s="T86">tɔː </ts>
               <ts e="T88" id="Seg_2707" n="e" s="T87">puːtɨmpatɨ. </ts>
               <ts e="T89" id="Seg_2709" n="e" s="T88">Märqɨ </ts>
               <ts e="T90" id="Seg_2711" n="e" s="T89">ijatɨ </ts>
               <ts e="T91" id="Seg_2713" n="e" s="T90">nılʼčʼilʼ </ts>
               <ts e="T92" id="Seg_2715" n="e" s="T91">ijatɨ </ts>
               <ts e="T93" id="Seg_2717" n="e" s="T92">ɛppɨmmɨnta. </ts>
               <ts e="T94" id="Seg_2719" n="e" s="T93">Puːtɨŋɨtɨ </ts>
               <ts e="T95" id="Seg_2721" n="e" s="T94">ɔːtätɨ </ts>
               <ts e="T96" id="Seg_2723" n="e" s="T95">aj </ts>
               <ts e="T97" id="Seg_2725" n="e" s="T96">čʼäːŋka. </ts>
               <ts e="T98" id="Seg_2727" n="e" s="T97">Ɔːtäkɔːlɨk </ts>
               <ts e="T99" id="Seg_2729" n="e" s="T98">topɨsä </ts>
               <ts e="T100" id="Seg_2731" n="e" s="T99">ilɨmpɔːtɨt. </ts>
               <ts e="T101" id="Seg_2733" n="e" s="T100">Nɨmtɨ </ts>
               <ts e="T102" id="Seg_2735" n="e" s="T101">ilɨmpɔːtɨt. </ts>
               <ts e="T103" id="Seg_2737" n="e" s="T102">İlɨmpɔːtɨt, </ts>
               <ts e="T104" id="Seg_2739" n="e" s="T103">tɔː </ts>
               <ts e="T105" id="Seg_2741" n="e" s="T104">puːlʼä, </ts>
               <ts e="T106" id="Seg_2743" n="e" s="T105">qälʼɨ-irat </ts>
               <ts e="T107" id="Seg_2745" n="e" s="T106">mɔːttɨ. </ts>
               <ts e="T691" id="Seg_2747" n="e" s="T107">Qälʼɨ </ts>
               <ts e="T108" id="Seg_2749" n="e" s="T691">irra </ts>
               <ts e="T109" id="Seg_2751" n="e" s="T108">ijatɨ </ts>
               <ts e="T110" id="Seg_2753" n="e" s="T109">čʼäŋɨmpa. </ts>
               <ts e="T111" id="Seg_2755" n="e" s="T110">İjakɔːtɨlʼ </ts>
               <ts e="T112" id="Seg_2757" n="e" s="T111">imatɨ. </ts>
               <ts e="T113" id="Seg_2759" n="e" s="T112">Toːna </ts>
               <ts e="T114" id="Seg_2761" n="e" s="T113">čʼontolʼ </ts>
               <ts e="T115" id="Seg_2763" n="e" s="T114">timnʼatɨ </ts>
               <ts e="T116" id="Seg_2765" n="e" s="T115">ukkɨr </ts>
               <ts e="T117" id="Seg_2767" n="e" s="T116">nälʼätɨ </ts>
               <ts e="T118" id="Seg_2769" n="e" s="T117">pɛlɨkɨtɨlʼ. </ts>
               <ts e="T119" id="Seg_2771" n="e" s="T118">Takkɨnɨ </ts>
               <ts e="T120" id="Seg_2773" n="e" s="T119">nʼänna </ts>
               <ts e="T121" id="Seg_2775" n="e" s="T120">šitɨ </ts>
               <ts e="T122" id="Seg_2777" n="e" s="T121">qälɨk </ts>
               <ts e="T123" id="Seg_2779" n="e" s="T122">tüːŋa. </ts>
               <ts e="T124" id="Seg_2781" n="e" s="T123">Poːkɨtɨlʼ </ts>
               <ts e="T125" id="Seg_2783" n="e" s="T124">təttoqɨnɨ </ts>
               <ts e="T126" id="Seg_2785" n="e" s="T125">na </ts>
               <ts e="T692" id="Seg_2787" n="e" s="T126">qälʼi </ts>
               <ts e="T127" id="Seg_2789" n="e" s="T692">ira </ts>
               <ts e="T128" id="Seg_2791" n="e" s="T127">najɛnta. </ts>
               <ts e="T129" id="Seg_2793" n="e" s="T128">Kuššalʼ </ts>
               <ts e="T130" id="Seg_2795" n="e" s="T129">tamtɨrtɨ </ts>
               <ts e="T131" id="Seg_2797" n="e" s="T130">ɔːtäp </ts>
               <ts e="T132" id="Seg_2799" n="e" s="T131">muntɨk </ts>
               <ts e="T133" id="Seg_2801" n="e" s="T132">taqqɨšpatɨ. </ts>
               <ts e="T134" id="Seg_2803" n="e" s="T133">Nılʼčʼik </ts>
               <ts e="T135" id="Seg_2805" n="e" s="T134">tüːŋa. </ts>
               <ts e="T136" id="Seg_2807" n="e" s="T135">Na </ts>
               <ts e="T137" id="Seg_2809" n="e" s="T136">tɨmnʼasɨka </ts>
               <ts e="T138" id="Seg_2811" n="e" s="T137">ilɔːtɨt </ts>
               <ts e="T139" id="Seg_2813" n="e" s="T138">na </ts>
               <ts e="T140" id="Seg_2815" n="e" s="T139">iralʼ </ts>
               <ts e="T141" id="Seg_2817" n="e" s="T140">tɨmnʼäsɨka </ts>
               <ts e="T142" id="Seg_2819" n="e" s="T141">šittɨ. </ts>
               <ts e="T143" id="Seg_2821" n="e" s="T142">Tɛːttɨ </ts>
               <ts e="T144" id="Seg_2823" n="e" s="T143">toːt </ts>
               <ts e="T145" id="Seg_2825" n="e" s="T144">ɔːtäqitɨ. </ts>
               <ts e="T146" id="Seg_2827" n="e" s="T145">Na </ts>
               <ts e="T147" id="Seg_2829" n="e" s="T146">iran </ts>
               <ts e="T148" id="Seg_2831" n="e" s="T147">ɔːtäp </ts>
               <ts e="T149" id="Seg_2833" n="e" s="T148">iqɨntoːqo </ts>
               <ts e="T150" id="Seg_2835" n="e" s="T149">tünta. </ts>
               <ts e="T151" id="Seg_2837" n="e" s="T150">Qumintɨsä </ts>
               <ts e="T152" id="Seg_2839" n="e" s="T151">tüːnta. </ts>
               <ts e="T153" id="Seg_2841" n="e" s="T152">Paːn </ts>
               <ts e="T154" id="Seg_2843" n="e" s="T153">iːra, </ts>
               <ts e="T155" id="Seg_2845" n="e" s="T154">nımtɨ </ts>
               <ts e="T156" id="Seg_2847" n="e" s="T155">aj </ts>
               <ts e="T157" id="Seg_2849" n="e" s="T156">najta. </ts>
               <ts e="T158" id="Seg_2851" n="e" s="T157">Nɨnɨ </ts>
               <ts e="T159" id="Seg_2853" n="e" s="T158">tüːlä </ts>
               <ts e="T160" id="Seg_2855" n="e" s="T159">šäqqɔːtɨt </ts>
               <ts e="T161" id="Seg_2857" n="e" s="T160">na </ts>
               <ts e="T162" id="Seg_2859" n="e" s="T161">täpɨtɨt </ts>
               <ts e="T163" id="Seg_2861" n="e" s="T162">mɔːtqɨt. </ts>
               <ts e="T164" id="Seg_2863" n="e" s="T163">Na </ts>
               <ts e="T165" id="Seg_2865" n="e" s="T164">ija </ts>
               <ts e="T166" id="Seg_2867" n="e" s="T165">aš </ts>
               <ts e="T167" id="Seg_2869" n="e" s="T166">tɛnɨmɔːtɨt. </ts>
               <ts e="T168" id="Seg_2871" n="e" s="T167">“İːja </ts>
               <ts e="T169" id="Seg_2873" n="e" s="T168">kuttar </ts>
               <ts e="T170" id="Seg_2875" n="e" s="T169">taːpɨnɨk </ts>
               <ts e="T171" id="Seg_2877" n="e" s="T170">alʼčʼimpa? </ts>
               <ts e="T172" id="Seg_2879" n="e" s="T171">Täp </ts>
               <ts e="T173" id="Seg_2881" n="e" s="T172">qaj </ts>
               <ts e="T174" id="Seg_2883" n="e" s="T173">muːtɨronak </ts>
               <ts e="T175" id="Seg_2885" n="e" s="T174">ila”. </ts>
               <ts e="T176" id="Seg_2887" n="e" s="T175">Qup </ts>
               <ts e="T177" id="Seg_2889" n="e" s="T176">kuttar </ts>
               <ts e="T178" id="Seg_2891" n="e" s="T177">tɛnimɛntätɨ </ts>
               <ts e="T179" id="Seg_2893" n="e" s="T178">alʼpet </ts>
               <ts e="T180" id="Seg_2895" n="e" s="T179">ilʼäntɨlʼ </ts>
               <ts e="T181" id="Seg_2897" n="e" s="T180">qup. </ts>
               <ts e="T182" id="Seg_2899" n="e" s="T181">Šäqqɔːtɨt. </ts>
               <ts e="T183" id="Seg_2901" n="e" s="T182">Mompa </ts>
               <ts e="T184" id="Seg_2903" n="e" s="T183">nɔːrtälʼilʼ </ts>
               <ts e="T185" id="Seg_2905" n="e" s="T184">čʼeːlʼɨt </ts>
               <ts e="T186" id="Seg_2907" n="e" s="T185">toːqqɨt </ts>
               <ts e="T187" id="Seg_2909" n="e" s="T186">mompa </ts>
               <ts e="T188" id="Seg_2911" n="e" s="T187">Pan </ts>
               <ts e="T189" id="Seg_2913" n="e" s="T188">ira </ts>
               <ts e="T190" id="Seg_2915" n="e" s="T189">köt </ts>
               <ts e="T191" id="Seg_2917" n="e" s="T190">qupsä </ts>
               <ts e="T192" id="Seg_2919" n="e" s="T191">tüːŋa. </ts>
               <ts e="T193" id="Seg_2921" n="e" s="T192">Qapija </ts>
               <ts e="T194" id="Seg_2923" n="e" s="T193">ɔːtäp </ts>
               <ts e="T195" id="Seg_2925" n="e" s="T194">tɔːqqɨqontoːqo. </ts>
               <ts e="T196" id="Seg_2927" n="e" s="T195">Na </ts>
               <ts e="T197" id="Seg_2929" n="e" s="T196">tintena </ts>
               <ts e="T198" id="Seg_2931" n="e" s="T197">asästit </ts>
               <ts e="T199" id="Seg_2933" n="e" s="T198">ɔːtäp </ts>
               <ts e="T200" id="Seg_2935" n="e" s="T199">tɔːqqɨqo. </ts>
               <ts e="T201" id="Seg_2937" n="e" s="T200">Kun </ts>
               <ts e="T202" id="Seg_2939" n="e" s="T201">mɔːntɨlʼ </ts>
               <ts e="T203" id="Seg_2941" n="e" s="T202">mɔːttɨ </ts>
               <ts e="T204" id="Seg_2943" n="e" s="T203">šentɨ </ts>
               <ts e="T205" id="Seg_2945" n="e" s="T204">kun </ts>
               <ts e="T206" id="Seg_2947" n="e" s="T205">mɔːntɨlʼ </ts>
               <ts e="T207" id="Seg_2949" n="e" s="T206">qajtɨ </ts>
               <ts e="T208" id="Seg_2951" n="e" s="T207">mun </ts>
               <ts e="T209" id="Seg_2953" n="e" s="T208">taqqalʼtɛntɨt. </ts>
               <ts e="T210" id="Seg_2955" n="e" s="T209">Ukoːn </ts>
               <ts e="T211" id="Seg_2957" n="e" s="T210">nɔːta </ts>
               <ts e="T212" id="Seg_2959" n="e" s="T211">taqqɨlʼkolʼčʼimpat. </ts>
               <ts e="T213" id="Seg_2961" n="e" s="T212">Qälʼitɨt </ts>
               <ts e="T214" id="Seg_2963" n="e" s="T213">ɔːtap </ts>
               <ts e="T215" id="Seg_2965" n="e" s="T214">taqqɨllä </ts>
               <ts e="T216" id="Seg_2967" n="e" s="T215">iːmpatɨ. </ts>
               <ts e="T217" id="Seg_2969" n="e" s="T216">Nɨnɨ </ts>
               <ts e="T218" id="Seg_2971" n="e" s="T217">šittälʼ </ts>
               <ts e="T219" id="Seg_2973" n="e" s="T218">mɨŋa </ts>
               <ts e="T220" id="Seg_2975" n="e" s="T219">moqɨnä </ts>
               <ts e="T221" id="Seg_2977" n="e" s="T220">qənnɔːqij </ts>
               <ts e="T222" id="Seg_2979" n="e" s="T221">na </ts>
               <ts e="T223" id="Seg_2981" n="e" s="T222">qälɨkqıː. </ts>
               <ts e="T224" id="Seg_2983" n="e" s="T223">Nɔːrtälʼilʼ </ts>
               <ts e="T225" id="Seg_2985" n="e" s="T224">čʼeːlʼit </ts>
               <ts e="T226" id="Seg_2987" n="e" s="T225">mompa </ts>
               <ts e="T227" id="Seg_2989" n="e" s="T226">tüntɔːmɨt. </ts>
               <ts e="T228" id="Seg_2991" n="e" s="T227">Tɛː </ts>
               <ts e="T229" id="Seg_2993" n="e" s="T228">mumpa </ts>
               <ts e="T230" id="Seg_2995" n="e" s="T229">kučʼčʼä </ts>
               <ts e="T231" id="Seg_2997" n="e" s="T230">ɨkɨ </ts>
               <ts e="T232" id="Seg_2999" n="e" s="T231">qənnɨlɨt. </ts>
               <ts e="T233" id="Seg_3001" n="e" s="T232">Šittälʼɨlʼ </ts>
               <ts e="T234" id="Seg_3003" n="e" s="T233">čʼeːlontɨ </ts>
               <ts e="T235" id="Seg_3005" n="e" s="T234">ınnä </ts>
               <ts e="T236" id="Seg_3007" n="e" s="T235">čʼeːlʼinna. </ts>
               <ts e="T237" id="Seg_3009" n="e" s="T236">Na </ts>
               <ts e="T238" id="Seg_3011" n="e" s="T237">iːja </ts>
               <ts e="T239" id="Seg_3013" n="e" s="T238">nılʼ </ts>
               <ts e="T240" id="Seg_3015" n="e" s="T239">kətɨtɨt </ts>
               <ts e="T241" id="Seg_3017" n="e" s="T240">ilʼčʼantɨnɨk. </ts>
               <ts e="T242" id="Seg_3019" n="e" s="T241">Mɨta </ts>
               <ts e="T243" id="Seg_3021" n="e" s="T242">nʼannät </ts>
               <ts e="T244" id="Seg_3023" n="e" s="T243">tɨmtɨ </ts>
               <ts e="T245" id="Seg_3025" n="e" s="T244">tɛːttɨ </ts>
               <ts e="T246" id="Seg_3027" n="e" s="T245">toːt </ts>
               <ts e="T247" id="Seg_3029" n="e" s="T246">ɔːtat </ts>
               <ts e="T248" id="Seg_3031" n="e" s="T247">nɨŋqɨkušalʼ </ts>
               <ts e="T249" id="Seg_3033" n="e" s="T248">nʼarop </ts>
               <ts e="T250" id="Seg_3035" n="e" s="T249">ɛːnta, </ts>
               <ts e="T251" id="Seg_3037" n="e" s="T250">načʼčʼä </ts>
               <ts e="T252" id="Seg_3039" n="e" s="T251">üːtälʼɨmɨt. </ts>
               <ts e="T253" id="Seg_3041" n="e" s="T252">Qäːlʼit </ts>
               <ts e="T254" id="Seg_3043" n="e" s="T253">tüːntɔːtɨt </ts>
               <ts e="T255" id="Seg_3045" n="e" s="T254">ɔːtäp </ts>
               <ts e="T256" id="Seg_3047" n="e" s="T255">tɔːqqɨqolamnɛntɔːtɨt. </ts>
               <ts e="T257" id="Seg_3049" n="e" s="T256">Ɔːtan </ts>
               <ts e="T258" id="Seg_3051" n="e" s="T257">upkɨt </ts>
               <ts e="T259" id="Seg_3053" n="e" s="T258">kəlʼčʼomɨntɨ </ts>
               <ts e="T260" id="Seg_3055" n="e" s="T259">kuntɨ </ts>
               <ts e="T261" id="Seg_3057" n="e" s="T260">kärɨpɨlʼ </ts>
               <ts e="T262" id="Seg_3059" n="e" s="T261">kutar </ts>
               <ts e="T263" id="Seg_3061" n="e" s="T262">meːtɔːmɨt. </ts>
               <ts e="T264" id="Seg_3063" n="e" s="T263">Na </ts>
               <ts e="T265" id="Seg_3065" n="e" s="T264">iːja </ts>
               <ts e="T266" id="Seg_3067" n="e" s="T265">nık </ts>
               <ts e="T267" id="Seg_3069" n="e" s="T266">kəttɨŋɨt. </ts>
               <ts e="T268" id="Seg_3071" n="e" s="T267">Nɨnɨ </ts>
               <ts e="T269" id="Seg_3073" n="e" s="T268">nık </ts>
               <ts e="T270" id="Seg_3075" n="e" s="T269">kəttɨŋɨtɨ. </ts>
               <ts e="T271" id="Seg_3077" n="e" s="T270">Mäkkä </ts>
               <ts e="T272" id="Seg_3079" n="e" s="T271">mɨta </ts>
               <ts e="T273" id="Seg_3081" n="e" s="T272">šentɨ </ts>
               <ts e="T274" id="Seg_3083" n="e" s="T273">mɔːllʼet </ts>
               <ts e="T275" id="Seg_3085" n="e" s="T274">ɛːŋa, </ts>
               <ts e="T276" id="Seg_3087" n="e" s="T275">ilʼčʼa, </ts>
               <ts e="T277" id="Seg_3089" n="e" s="T276">mäkkä </ts>
               <ts e="T278" id="Seg_3091" n="e" s="T277">mitɨ. </ts>
               <ts e="T279" id="Seg_3093" n="e" s="T278">A </ts>
               <ts e="T280" id="Seg_3095" n="e" s="T279">toːna </ts>
               <ts e="T281" id="Seg_3097" n="e" s="T280">tınte </ts>
               <ts e="T282" id="Seg_3099" n="e" s="T281">timnʼäntɨ </ts>
               <ts e="T283" id="Seg_3101" n="e" s="T282">nälʼa </ts>
               <ts e="T284" id="Seg_3103" n="e" s="T283">ilʼčʼatɨ </ts>
               <ts e="T285" id="Seg_3105" n="e" s="T284">natɨp </ts>
               <ts e="T286" id="Seg_3107" n="e" s="T285">kuralʼtɨmpatɨ. </ts>
               <ts e="T287" id="Seg_3109" n="e" s="T286">Na </ts>
               <ts e="T289" id="Seg_3111" n="e" s="T287">iːjanɨk… </ts>
               <ts e="T290" id="Seg_3113" n="e" s="T289">Nʼännät </ts>
               <ts e="T291" id="Seg_3115" n="e" s="T290">tɨmtɨ </ts>
               <ts e="T292" id="Seg_3117" n="e" s="T291">maːčʼelʼ </ts>
               <ts e="T293" id="Seg_3119" n="e" s="T292">tiːčʼi </ts>
               <ts e="T294" id="Seg_3121" n="e" s="T293">ɛːŋa. </ts>
               <ts e="T295" id="Seg_3123" n="e" s="T294">Nɨmtɨ </ts>
               <ts e="T296" id="Seg_3125" n="e" s="T295">mɔːtɨŋnɛntak. </ts>
               <ts e="T297" id="Seg_3127" n="e" s="T296">Šentɨ </ts>
               <ts e="T298" id="Seg_3129" n="e" s="T297">mɔːt </ts>
               <ts e="T299" id="Seg_3131" n="e" s="T298">tatɨŋɨt </ts>
               <ts e="T300" id="Seg_3133" n="e" s="T299">mɨta. </ts>
               <ts e="T301" id="Seg_3135" n="e" s="T300">Üːtälnɔːtɨt </ts>
               <ts e="T302" id="Seg_3137" n="e" s="T301">taːlʼ. </ts>
               <ts e="T303" id="Seg_3139" n="e" s="T302">Qapıje </ts>
               <ts e="T304" id="Seg_3141" n="e" s="T303">nɨ </ts>
               <ts e="T305" id="Seg_3143" n="e" s="T304">nɔːrtälʼilʼ </ts>
               <ts e="T306" id="Seg_3145" n="e" s="T305">čʼeːlɨ </ts>
               <ts e="T307" id="Seg_3147" n="e" s="T306">tüːntɔːtɨt </ts>
               <ts e="T308" id="Seg_3149" n="e" s="T307">Pan </ts>
               <ts e="T309" id="Seg_3151" n="e" s="T308">iralʼmɨt </ts>
               <ts e="T310" id="Seg_3153" n="e" s="T309">təpɨtɨtkiːne </ts>
               <ts e="T311" id="Seg_3155" n="e" s="T310">na </ts>
               <ts e="T312" id="Seg_3157" n="e" s="T311">qälɨ-irat </ts>
               <ts e="T313" id="Seg_3159" n="e" s="T312">ɔːtap </ts>
               <ts e="T314" id="Seg_3161" n="e" s="T313">taqqalʼqo. </ts>
               <ts e="T315" id="Seg_3163" n="e" s="T314">Timnʼäsɨqät </ts>
               <ts e="T316" id="Seg_3165" n="e" s="T315">ɔːtäp </ts>
               <ts e="T317" id="Seg_3167" n="e" s="T316">nɨnɨ </ts>
               <ts e="T318" id="Seg_3169" n="e" s="T317">šittelʼ </ts>
               <ts e="T319" id="Seg_3171" n="e" s="T318">üːtältɔːtɨt. </ts>
               <ts e="T320" id="Seg_3173" n="e" s="T319">Na </ts>
               <ts e="T321" id="Seg_3175" n="e" s="T320">ɔːtäp </ts>
               <ts e="T322" id="Seg_3177" n="e" s="T321">na </ts>
               <ts e="T323" id="Seg_3179" n="e" s="T322">tɔːqqäntɔːtɨt </ts>
               <ts e="T324" id="Seg_3181" n="e" s="T323">qapıj </ts>
               <ts e="T325" id="Seg_3183" n="e" s="T324">täm </ts>
               <ts e="T326" id="Seg_3185" n="e" s="T325">aj </ts>
               <ts e="T327" id="Seg_3187" n="e" s="T326">tɔːqqɨmpatɨ. </ts>
               <ts e="T328" id="Seg_3189" n="e" s="T327">Mɔːssä </ts>
               <ts e="T329" id="Seg_3191" n="e" s="T328">na </ts>
               <ts e="T330" id="Seg_3193" n="e" s="T329">mintɔːtɨt. </ts>
               <ts e="T331" id="Seg_3195" n="e" s="T330">Na </ts>
               <ts e="T332" id="Seg_3197" n="e" s="T331">mačʼit </ts>
               <ts e="T333" id="Seg_3199" n="e" s="T332">tičʼit </ts>
               <ts e="T334" id="Seg_3201" n="e" s="T333">nʼännälʼ </ts>
               <ts e="T335" id="Seg_3203" n="e" s="T334">pɛlʼäqqɨt </ts>
               <ts e="T336" id="Seg_3205" n="e" s="T335">nɨna </ts>
               <ts e="T337" id="Seg_3207" n="e" s="T336">mɔːtɨŋtɔːtɨt </ts>
               <ts e="T338" id="Seg_3209" n="e" s="T337">na </ts>
               <ts e="T339" id="Seg_3211" n="e" s="T338">imantɨsä. </ts>
               <ts e="T340" id="Seg_3213" n="e" s="T339">Qapıje </ts>
               <ts e="T341" id="Seg_3215" n="e" s="T340">təttalʼ </ts>
               <ts e="T342" id="Seg_3217" n="e" s="T341">qälɨk </ts>
               <ts e="T343" id="Seg_3219" n="e" s="T342">qaj </ts>
               <ts e="T344" id="Seg_3221" n="e" s="T343">mɔːttɨ </ts>
               <ts e="T345" id="Seg_3223" n="e" s="T344">čʼaːŋka </ts>
               <ts e="T346" id="Seg_3225" n="e" s="T345">šentɨ </ts>
               <ts e="T347" id="Seg_3227" n="e" s="T346">mɔːttɨ </ts>
               <ts e="T348" id="Seg_3229" n="e" s="T347">qopɨlʼ </ts>
               <ts e="T349" id="Seg_3231" n="e" s="T348">mɔːttɨ. </ts>
               <ts e="T350" id="Seg_3233" n="e" s="T349">Nɨnɨ </ts>
               <ts e="T351" id="Seg_3235" n="e" s="T350">šittälʼ </ts>
               <ts e="T352" id="Seg_3237" n="e" s="T351">šäqqɔːtɨt </ts>
               <ts e="T353" id="Seg_3239" n="e" s="T352">na </ts>
               <ts e="T354" id="Seg_3241" n="e" s="T353">imantɨsä. </ts>
               <ts e="T355" id="Seg_3243" n="e" s="T354">Təptɨlʼ </ts>
               <ts e="T356" id="Seg_3245" n="e" s="T355">qarɨt </ts>
               <ts e="T357" id="Seg_3247" n="e" s="T356">nıŋ </ts>
               <ts e="T358" id="Seg_3249" n="e" s="T357">ɛsa. </ts>
               <ts e="T359" id="Seg_3251" n="e" s="T358">Tıntena </ts>
               <ts e="T360" id="Seg_3253" n="e" s="T359">imantɨnɨk: </ts>
               <ts e="T361" id="Seg_3255" n="e" s="T360">mat </ts>
               <ts e="T362" id="Seg_3257" n="e" s="T361">mɨta </ts>
               <ts e="T363" id="Seg_3259" n="e" s="T362">nʼänna </ts>
               <ts e="T364" id="Seg_3261" n="e" s="T363">qumiːqäk </ts>
               <ts e="T365" id="Seg_3263" n="e" s="T364">koːnɨlʼäk. </ts>
               <ts e="T366" id="Seg_3265" n="e" s="T365">Qaj </ts>
               <ts e="T367" id="Seg_3267" n="e" s="T366">mɨta </ts>
               <ts e="T368" id="Seg_3269" n="e" s="T367">mɔːtɨnpɔːtɨt </ts>
               <ts e="T369" id="Seg_3271" n="e" s="T368">qaj </ts>
               <ts e="T370" id="Seg_3273" n="e" s="T369">qaːtɔːtɨt. </ts>
               <ts e="T371" id="Seg_3275" n="e" s="T370">Nʼännä </ts>
               <ts e="T372" id="Seg_3277" n="e" s="T371">laqaltelʼčʼa. </ts>
               <ts e="T373" id="Seg_3279" n="e" s="T372">Nɔːkɨr </ts>
               <ts e="T374" id="Seg_3281" n="e" s="T373">qoptɨp </ts>
               <ts e="T375" id="Seg_3283" n="e" s="T374">sɔːrɨlʼa </ts>
               <ts e="T376" id="Seg_3285" n="e" s="T375">qapıja </ts>
               <ts e="T377" id="Seg_3287" n="e" s="T376">nɔːssarɨlʼ </ts>
               <ts e="T378" id="Seg_3289" n="e" s="T377">ɔːtäp </ts>
               <ts e="T379" id="Seg_3291" n="e" s="T378">qaltɨmmɨntɨt. </ts>
               <ts e="T380" id="Seg_3293" n="e" s="T379">İja </ts>
               <ts e="T381" id="Seg_3295" n="e" s="T380">nʼanna </ts>
               <ts e="T382" id="Seg_3297" n="e" s="T381">qənnɛːja. </ts>
               <ts e="T383" id="Seg_3299" n="e" s="T382">İːmatɨ </ts>
               <ts e="T384" id="Seg_3301" n="e" s="T383">ontɨ </ts>
               <ts e="T385" id="Seg_3303" n="e" s="T384">qala. </ts>
               <ts e="T386" id="Seg_3305" n="e" s="T385">Na </ts>
               <ts e="T387" id="Seg_3307" n="e" s="T386">omta, </ts>
               <ts e="T388" id="Seg_3309" n="e" s="T387">na </ts>
               <ts e="T389" id="Seg_3311" n="e" s="T388">omta. </ts>
               <ts e="T390" id="Seg_3313" n="e" s="T389">Ukkɨr </ts>
               <ts e="T391" id="Seg_3315" n="e" s="T390">tät </ts>
               <ts e="T392" id="Seg_3317" n="e" s="T391">čʼontoːqɨt </ts>
               <ts e="T393" id="Seg_3319" n="e" s="T392">čʼeːlʼɨtɨ </ts>
               <ts e="T394" id="Seg_3321" n="e" s="T393">üːtoːntɨ </ts>
               <ts e="T395" id="Seg_3323" n="e" s="T394">na </ts>
               <ts e="T396" id="Seg_3325" n="e" s="T395">qənna. </ts>
               <ts e="T397" id="Seg_3327" n="e" s="T396">Na </ts>
               <ts e="T398" id="Seg_3329" n="e" s="T397">iːja </ts>
               <ts e="T399" id="Seg_3331" n="e" s="T398">lʼamɨk </ts>
               <ts e="T400" id="Seg_3333" n="e" s="T399">kəːŋa, </ts>
               <ts e="T401" id="Seg_3335" n="e" s="T400">nʼännät </ts>
               <ts e="T402" id="Seg_3337" n="e" s="T401">ukkɨr </ts>
               <ts e="T403" id="Seg_3339" n="e" s="T402">tot </ts>
               <ts e="T404" id="Seg_3341" n="e" s="T403">čʼontoːqɨt </ts>
               <ts e="T405" id="Seg_3343" n="e" s="T404">qup </ts>
               <ts e="T406" id="Seg_3345" n="e" s="T405">najap </ts>
               <ts e="T407" id="Seg_3347" n="e" s="T406">tatɨntɔːtɨt. </ts>
               <ts e="T408" id="Seg_3349" n="e" s="T407">Takkɨnɨ </ts>
               <ts e="T409" id="Seg_3351" n="e" s="T408">Pan </ts>
               <ts e="T410" id="Seg_3353" n="e" s="T409">iralʼmɨt </ts>
               <ts e="T411" id="Seg_3355" n="e" s="T410">tatɨntɔːtɨt. </ts>
               <ts e="T412" id="Seg_3357" n="e" s="T411">Köt </ts>
               <ts e="T413" id="Seg_3359" n="e" s="T412">qumtɨ </ts>
               <ts e="T414" id="Seg_3361" n="e" s="T413">qapıja </ts>
               <ts e="T415" id="Seg_3363" n="e" s="T414">ɔːtäp </ts>
               <ts e="T416" id="Seg_3365" n="e" s="T415">tɔːqqɨsɔːtɨlʼ. </ts>
               <ts e="T417" id="Seg_3367" n="e" s="T416">Pan </ts>
               <ts e="T418" id="Seg_3369" n="e" s="T417">ira </ts>
               <ts e="T419" id="Seg_3371" n="e" s="T418">naššak </ts>
               <ts e="T420" id="Seg_3373" n="e" s="T419">qəːtɨsä </ts>
               <ts e="T421" id="Seg_3375" n="e" s="T420">ɛːŋa. </ts>
               <ts e="T422" id="Seg_3377" n="e" s="T421">Nɨn </ts>
               <ts e="T423" id="Seg_3379" n="e" s="T422">šittalʼ </ts>
               <ts e="T424" id="Seg_3381" n="e" s="T423">na </ts>
               <ts e="T425" id="Seg_3383" n="e" s="T424">ija </ts>
               <ts e="T426" id="Seg_3385" n="e" s="T425">tülʼčʼikunä. </ts>
               <ts e="T427" id="Seg_3387" n="e" s="T426">İmatä </ts>
               <ts e="T428" id="Seg_3389" n="e" s="T427">üŋkɨltimpatɨ. </ts>
               <ts e="T429" id="Seg_3391" n="e" s="T428">Montɨ </ts>
               <ts e="T430" id="Seg_3393" n="e" s="T429">čʼap </ts>
               <ts e="T431" id="Seg_3395" n="e" s="T430">tüːlʼčʼa </ts>
               <ts e="T432" id="Seg_3397" n="e" s="T431">Pan </ts>
               <ts e="T433" id="Seg_3399" n="e" s="T432">iralʼmɨt </ts>
               <ts e="T434" id="Seg_3401" n="e" s="T433">qaj </ts>
               <ts e="T435" id="Seg_3403" n="e" s="T434">montɨ </ts>
               <ts e="T436" id="Seg_3405" n="e" s="T435">tüŋɔːtɨt. </ts>
               <ts e="T437" id="Seg_3407" n="e" s="T436">Qaqlʼilʼ </ts>
               <ts e="T438" id="Seg_3409" n="e" s="T437">mɨtɨ </ts>
               <ts e="T439" id="Seg_3411" n="e" s="T438">nɨk </ts>
               <ts e="T440" id="Seg_3413" n="e" s="T439">tottälnɔːtɨt, </ts>
               <ts e="T441" id="Seg_3415" n="e" s="T440">poːqɨt </ts>
               <ts e="T442" id="Seg_3417" n="e" s="T441">eːtɨmantoːqɨt. </ts>
               <ts e="T443" id="Seg_3419" n="e" s="T442">Mɔːttɨ </ts>
               <ts e="T444" id="Seg_3421" n="e" s="T443">šeːrna. </ts>
               <ts e="T445" id="Seg_3423" n="e" s="T444">Mɔːtan </ts>
               <ts e="T446" id="Seg_3425" n="e" s="T445">ɔːt </ts>
               <ts e="T447" id="Seg_3427" n="e" s="T446">nɨŋkɨlʼa </ts>
               <ts e="T448" id="Seg_3429" n="e" s="T447">mɔːtan </ts>
               <ts e="T449" id="Seg_3431" n="e" s="T448">ɔːt </ts>
               <ts e="T450" id="Seg_3433" n="e" s="T449">kʼeː </ts>
               <ts e="T451" id="Seg_3435" n="e" s="T450">topɨmtɨ </ts>
               <ts e="T452" id="Seg_3437" n="e" s="T451">tupalkɨtɨ. </ts>
               <ts e="T453" id="Seg_3439" n="e" s="T452">Peːmɨmtɨ </ts>
               <ts e="T454" id="Seg_3441" n="e" s="T453">na </ts>
               <ts e="T455" id="Seg_3443" n="e" s="T454">tupalnɨt. </ts>
               <ts e="T456" id="Seg_3445" n="e" s="T455">Nı </ts>
               <ts e="T457" id="Seg_3447" n="e" s="T456">kəttɨŋɨtɨ </ts>
               <ts e="T458" id="Seg_3449" n="e" s="T457">Pan </ts>
               <ts e="T459" id="Seg_3451" n="e" s="T458">iranɨk </ts>
               <ts e="T460" id="Seg_3453" n="e" s="T459">nʼännä </ts>
               <ts e="T461" id="Seg_3455" n="e" s="T460">koraltɨlä. </ts>
               <ts e="T462" id="Seg_3457" n="e" s="T461">Ɔːklɨ </ts>
               <ts e="T463" id="Seg_3459" n="e" s="T462">miːt </ts>
               <ts e="T464" id="Seg_3461" n="e" s="T463">olʼlʼa </ts>
               <ts e="T465" id="Seg_3463" n="e" s="T464">qolʼtɨt </ts>
               <ts e="T466" id="Seg_3465" n="e" s="T465">qəqɨt </ts>
               <ts e="T467" id="Seg_3467" n="e" s="T466">oːlʼa. </ts>
               <ts e="T468" id="Seg_3469" n="e" s="T467">Taːlʼ </ts>
               <ts e="T469" id="Seg_3471" n="e" s="T468">na </ts>
               <ts e="T470" id="Seg_3473" n="e" s="T469">nʼänna </ts>
               <ts e="T471" id="Seg_3475" n="e" s="T470">paqtɨmpa </ts>
               <ts e="T472" id="Seg_3477" n="e" s="T471">na </ts>
               <ts e="T473" id="Seg_3479" n="e" s="T472">kupaksä </ts>
               <ts e="T474" id="Seg_3481" n="e" s="T473">täqalemmɨntɨtɨ. </ts>
               <ts e="T475" id="Seg_3483" n="e" s="T474">Pan </ts>
               <ts e="T476" id="Seg_3485" n="e" s="T475">iralʼ </ts>
               <ts e="T477" id="Seg_3487" n="e" s="T476">mɨtɨp </ts>
               <ts e="T478" id="Seg_3489" n="e" s="T477">muːntɨkə </ts>
               <ts e="T479" id="Seg_3491" n="e" s="T478">lʼäpek </ts>
               <ts e="T480" id="Seg_3493" n="e" s="T479">qättɨmpat. </ts>
               <ts e="T481" id="Seg_3495" n="e" s="T480">Ɔːmmɨtɨ </ts>
               <ts e="T482" id="Seg_3497" n="e" s="T481">qumpa </ts>
               <ts e="T483" id="Seg_3499" n="e" s="T482">Pan </ts>
               <ts e="T484" id="Seg_3501" n="e" s="T483">ira </ts>
               <ts e="T485" id="Seg_3503" n="e" s="T484">ilʼilä </ts>
               <ts e="T486" id="Seg_3505" n="e" s="T485">qalɨmpa. </ts>
               <ts e="T487" id="Seg_3507" n="e" s="T486">Šittäqij </ts>
               <ts e="T488" id="Seg_3509" n="e" s="T487">ilɨlä </ts>
               <ts e="T489" id="Seg_3511" n="e" s="T488">qaːlɨmmɨnta. </ts>
               <ts e="T490" id="Seg_3513" n="e" s="T489">Ponä </ts>
               <ts e="T491" id="Seg_3515" n="e" s="T490">qəlläitɨ. </ts>
               <ts e="T492" id="Seg_3517" n="e" s="T491">Na </ts>
               <ts e="T493" id="Seg_3519" n="e" s="T492">iːja </ts>
               <ts e="T494" id="Seg_3521" n="e" s="T493">ukkur </ts>
               <ts e="T495" id="Seg_3523" n="e" s="T494">pɔːrɨ </ts>
               <ts e="T496" id="Seg_3525" n="e" s="T495">kupaksä </ts>
               <ts e="T497" id="Seg_3527" n="e" s="T496">täqalempatɨ. </ts>
               <ts e="T498" id="Seg_3529" n="e" s="T497">Muntɨk </ts>
               <ts e="T499" id="Seg_3531" n="e" s="T498">porqäntɨ </ts>
               <ts e="T500" id="Seg_3533" n="e" s="T499">šunʼnʼontɨ </ts>
               <ts e="T501" id="Seg_3535" n="e" s="T500">(pümmɨntɨ </ts>
               <ts e="T502" id="Seg_3537" n="e" s="T501">šʼu) </ts>
               <ts e="T503" id="Seg_3539" n="e" s="T502">nʼölʼqɨmɔːtpotät. </ts>
               <ts e="T504" id="Seg_3541" n="e" s="T503">Na </ts>
               <ts e="T505" id="Seg_3543" n="e" s="T504">qupɨlʼmɨt </ts>
               <ts e="T506" id="Seg_3545" n="e" s="T505">nɨnɨ </ts>
               <ts e="T507" id="Seg_3547" n="e" s="T506">šitälʼ </ts>
               <ts e="T508" id="Seg_3549" n="e" s="T507">ponä </ts>
               <ts e="T509" id="Seg_3551" n="e" s="T508">kälʼlʼeitɨ. </ts>
               <ts e="T510" id="Seg_3553" n="e" s="T509">Eːtɨmannoːntɨ </ts>
               <ts e="T511" id="Seg_3555" n="e" s="T510">tintena </ts>
               <ts e="T512" id="Seg_3557" n="e" s="T511">qaqlɨ </ts>
               <ts e="T513" id="Seg_3559" n="e" s="T512">tüːr </ts>
               <ts e="T514" id="Seg_3561" n="e" s="T513">tolʼčʼitɨ </ts>
               <ts e="T515" id="Seg_3563" n="e" s="T514">na </ts>
               <ts e="T516" id="Seg_3565" n="e" s="T515">sɔːrälɨmpa. </ts>
               <ts e="T517" id="Seg_3567" n="e" s="T516">Pan </ts>
               <ts e="T518" id="Seg_3569" n="e" s="T517">irap </ts>
               <ts e="T519" id="Seg_3571" n="e" s="T518">aj </ts>
               <ts e="T520" id="Seg_3573" n="e" s="T519">ponä </ts>
               <ts e="T521" id="Seg_3575" n="e" s="T520">itɨmpat </ts>
               <ts e="T522" id="Seg_3577" n="e" s="T521">(čʼatɨmpat). </ts>
               <ts e="T523" id="Seg_3579" n="e" s="T522">Qapija </ts>
               <ts e="T524" id="Seg_3581" n="e" s="T523">muntɨk </ts>
               <ts e="T525" id="Seg_3583" n="e" s="T524">pona </ts>
               <ts e="T526" id="Seg_3585" n="e" s="T525">iːtɨmpat. </ts>
               <ts e="T527" id="Seg_3587" n="e" s="T526">Illa </ts>
               <ts e="T528" id="Seg_3589" n="e" s="T527">omta </ts>
               <ts e="T529" id="Seg_3591" n="e" s="T528">malʼčʼalʼ </ts>
               <ts e="T530" id="Seg_3593" n="e" s="T529">tɔːqtɨ </ts>
               <ts e="T531" id="Seg_3595" n="e" s="T530">toː </ts>
               <ts e="T532" id="Seg_3597" n="e" s="T531">iːlʼä. </ts>
               <ts e="T533" id="Seg_3599" n="e" s="T532">Amɨrlʼä </ts>
               <ts e="T534" id="Seg_3601" n="e" s="T533">ıllä </ts>
               <ts e="T535" id="Seg_3603" n="e" s="T534">ommɨnta </ts>
               <ts e="T536" id="Seg_3605" n="e" s="T535">šäqqa. </ts>
               <ts e="T537" id="Seg_3607" n="e" s="T536">Na </ts>
               <ts e="T538" id="Seg_3609" n="e" s="T537">qumiːtɨ </ts>
               <ts e="T539" id="Seg_3611" n="e" s="T538">poːqɨt </ts>
               <ts e="T540" id="Seg_3613" n="e" s="T539">na </ts>
               <ts e="T541" id="Seg_3615" n="e" s="T540">ippälɨmpɔːtɨt. </ts>
               <ts e="T542" id="Seg_3617" n="e" s="T541">Kuttar </ts>
               <ts e="T543" id="Seg_3619" n="e" s="T542">sarälɨmpa. </ts>
               <ts e="T544" id="Seg_3621" n="e" s="T543">Nılʼčʼik </ts>
               <ts e="T545" id="Seg_3623" n="e" s="T544">šäqqa. </ts>
               <ts e="T546" id="Seg_3625" n="e" s="T545">Tɔːptɨlʼ </ts>
               <ts e="T547" id="Seg_3627" n="e" s="T546">qarɨt </ts>
               <ts e="T548" id="Seg_3629" n="e" s="T547">ɨnnä </ts>
               <ts e="T549" id="Seg_3631" n="e" s="T548">wäša. </ts>
               <ts e="T550" id="Seg_3633" n="e" s="T549">Pan </ts>
               <ts e="T551" id="Seg_3635" n="e" s="T550">ira </ts>
               <ts e="T552" id="Seg_3637" n="e" s="T551">nılʼčʼik </ts>
               <ts e="T553" id="Seg_3639" n="e" s="T552">tap </ts>
               <ts e="T554" id="Seg_3641" n="e" s="T553">wäntɨlʼ </ts>
               <ts e="T555" id="Seg_3643" n="e" s="T554">tɔːktɨ </ts>
               <ts e="T556" id="Seg_3645" n="e" s="T555">tɔːtɨk </ts>
               <ts e="T557" id="Seg_3647" n="e" s="T556">taŋkıːčʼimpa </ts>
               <ts e="T558" id="Seg_3649" n="e" s="T557">kekkɨsä </ts>
               <ts e="T559" id="Seg_3651" n="e" s="T558">sailʼatɨ </ts>
               <ts e="T560" id="Seg_3653" n="e" s="T559">qətɨmpa. </ts>
               <ts e="T561" id="Seg_3655" n="e" s="T560">Toːna </ts>
               <ts e="T562" id="Seg_3657" n="e" s="T561">qälɨkqıtɨ </ts>
               <ts e="T563" id="Seg_3659" n="e" s="T562">ešo </ts>
               <ts e="T564" id="Seg_3661" n="e" s="T563">somalʼoqı </ts>
               <ts e="T565" id="Seg_3663" n="e" s="T564">ɛppɨmpa. </ts>
               <ts e="T566" id="Seg_3665" n="e" s="T565">Nılʼ </ts>
               <ts e="T567" id="Seg_3667" n="e" s="T566">kətɨŋɨtɨ </ts>
               <ts e="T568" id="Seg_3669" n="e" s="T567">na </ts>
               <ts e="T569" id="Seg_3671" n="e" s="T568">ija </ts>
               <ts e="T570" id="Seg_3673" n="e" s="T569">mɨta </ts>
               <ts e="T571" id="Seg_3675" n="e" s="T570">na </ts>
               <ts e="T572" id="Seg_3677" n="e" s="T571">qälʼitɨtkiːnä. </ts>
               <ts e="T573" id="Seg_3679" n="e" s="T572">Moqonä </ts>
               <ts e="T574" id="Seg_3681" n="e" s="T573">qəntat </ts>
               <ts e="T575" id="Seg_3683" n="e" s="T574">talʼtälʼlʼä </ts>
               <ts e="T576" id="Seg_3685" n="e" s="T575">untɨt </ts>
               <ts e="T577" id="Seg_3687" n="e" s="T576">qaqlɨntɨsä. </ts>
               <ts e="T578" id="Seg_3689" n="e" s="T577">Nɨnɨ </ts>
               <ts e="T579" id="Seg_3691" n="e" s="T578">šittälʼ </ts>
               <ts e="T580" id="Seg_3693" n="e" s="T579">toːna </ts>
               <ts e="T581" id="Seg_3695" n="e" s="T580">meːqıj </ts>
               <ts e="T582" id="Seg_3697" n="e" s="T581">mɔːttɨ </ts>
               <ts e="T583" id="Seg_3699" n="e" s="T582">šeːrɨšpɔːtɨt. </ts>
               <ts e="T584" id="Seg_3701" n="e" s="T583">Qaj </ts>
               <ts e="T585" id="Seg_3703" n="e" s="T584">amɨrpɔːtɨt, </ts>
               <ts e="T586" id="Seg_3705" n="e" s="T585">qaj </ts>
               <ts e="T587" id="Seg_3707" n="e" s="T586">aš. </ts>
               <ts e="T588" id="Seg_3709" n="e" s="T587">Nɨnɨ </ts>
               <ts e="T589" id="Seg_3711" n="e" s="T588">tintena </ts>
               <ts e="T590" id="Seg_3713" n="e" s="T589">saralpɨtɨlʼ </ts>
               <ts e="T591" id="Seg_3715" n="e" s="T590">qaqlɨmtɨt </ts>
               <ts e="T592" id="Seg_3717" n="e" s="T591">nɨnɨ </ts>
               <ts e="T593" id="Seg_3719" n="e" s="T592">moqonä </ts>
               <ts e="T594" id="Seg_3721" n="e" s="T593">qəːčʼelʼlʼä </ts>
               <ts e="T595" id="Seg_3723" n="e" s="T594">na </ts>
               <ts e="T596" id="Seg_3725" n="e" s="T595">qumiːmtɨt </ts>
               <ts e="T597" id="Seg_3727" n="e" s="T596">qupɨlʼ </ts>
               <ts e="T598" id="Seg_3729" n="e" s="T597">qumiːmtɨ </ts>
               <ts e="T599" id="Seg_3731" n="e" s="T598">nık </ts>
               <ts e="T600" id="Seg_3733" n="e" s="T599">qəntɨŋɨt. </ts>
               <ts e="T601" id="Seg_3735" n="e" s="T600">Takkɨ </ts>
               <ts e="T602" id="Seg_3737" n="e" s="T601">moqonä </ts>
               <ts e="T603" id="Seg_3739" n="e" s="T602">Pan </ts>
               <ts e="T604" id="Seg_3741" n="e" s="T603">iramtɨ </ts>
               <ts e="T605" id="Seg_3743" n="e" s="T604">aj </ts>
               <ts e="T606" id="Seg_3745" n="e" s="T605">kočʼilʼä </ts>
               <ts e="T607" id="Seg_3747" n="e" s="T606">qəntɨtɨ. </ts>
               <ts e="T608" id="Seg_3749" n="e" s="T607">Qaj </ts>
               <ts e="T609" id="Seg_3751" n="e" s="T608">kɨka </ts>
               <ts e="T610" id="Seg_3753" n="e" s="T609">qəjitɨ </ts>
               <ts e="T611" id="Seg_3755" n="e" s="T610">qəntɨt. </ts>
               <ts e="T612" id="Seg_3757" n="e" s="T611">Qənnatɨt </ts>
               <ts e="T613" id="Seg_3759" n="e" s="T612">tına. </ts>
               <ts e="T614" id="Seg_3761" n="e" s="T613">Qumat </ts>
               <ts e="T615" id="Seg_3763" n="e" s="T614">somak </ts>
               <ts e="T616" id="Seg_3765" n="e" s="T615">qənnɔːtɨt, </ts>
               <ts e="T617" id="Seg_3767" n="e" s="T616">imantɨnɨ </ts>
               <ts e="T618" id="Seg_3769" n="e" s="T617">nık </ts>
               <ts e="T619" id="Seg_3771" n="e" s="T618">kətɨŋɨt. </ts>
               <ts e="T620" id="Seg_3773" n="e" s="T619">“Mɔːlle </ts>
               <ts e="T621" id="Seg_3775" n="e" s="T620">toː </ts>
               <ts e="T622" id="Seg_3777" n="e" s="T621">tılätɨ, </ts>
               <ts e="T623" id="Seg_3779" n="e" s="T622">me </ts>
               <ts e="T624" id="Seg_3781" n="e" s="T623">nʼänna </ts>
               <ts e="T625" id="Seg_3783" n="e" s="T624">qəlʼlʼä </ts>
               <ts e="T626" id="Seg_3785" n="e" s="T625">qumɨtɨtkiːne”. </ts>
               <ts e="T627" id="Seg_3787" n="e" s="T626">Aš </ts>
               <ts e="T628" id="Seg_3789" n="e" s="T627">tɛnimɨmpatɨ, </ts>
               <ts e="T629" id="Seg_3791" n="e" s="T628">ilʼčʼantɨlʼ </ts>
               <ts e="T630" id="Seg_3793" n="e" s="T629">mɨt </ts>
               <ts e="T631" id="Seg_3795" n="e" s="T630">qos </ts>
               <ts e="T632" id="Seg_3797" n="e" s="T631">qaj. </ts>
               <ts e="T633" id="Seg_3799" n="e" s="T632">Nʼenna </ts>
               <ts e="T634" id="Seg_3801" n="e" s="T633">qənna </ts>
               <ts e="T635" id="Seg_3803" n="e" s="T634">tintena. </ts>
               <ts e="T636" id="Seg_3805" n="e" s="T635">Onta </ts>
               <ts e="T637" id="Seg_3807" n="e" s="T636">aša </ts>
               <ts e="T638" id="Seg_3809" n="e" s="T637">koŋɨmpa. </ts>
               <ts e="T639" id="Seg_3811" n="e" s="T638">Toːna </ts>
               <ts e="T640" id="Seg_3813" n="e" s="T639">imatɨ </ts>
               <ts e="T641" id="Seg_3815" n="e" s="T640">na </ts>
               <ts e="T642" id="Seg_3817" n="e" s="T641">kətälmɨntɨtɨ. </ts>
               <ts e="T643" id="Seg_3819" n="e" s="T642">Əsɨntɨlʼ </ts>
               <ts e="T644" id="Seg_3821" n="e" s="T643">ilʼčʼantɨlʼ </ts>
               <ts e="T645" id="Seg_3823" n="e" s="T644">mɨqätkiːne. </ts>
               <ts e="T646" id="Seg_3825" n="e" s="T645">İralʼ </ts>
               <ts e="T647" id="Seg_3827" n="e" s="T646">mɨt </ts>
               <ts e="T648" id="Seg_3829" n="e" s="T647">moqonä </ts>
               <ts e="T649" id="Seg_3831" n="e" s="T648">qəssɔːtɨt. </ts>
               <ts e="T650" id="Seg_3833" n="e" s="T649">A </ts>
               <ts e="T651" id="Seg_3835" n="e" s="T650">nɨnä </ts>
               <ts e="T652" id="Seg_3837" n="e" s="T651">šittelʼ </ts>
               <ts e="T653" id="Seg_3839" n="e" s="T652">pijaka </ts>
               <ts e="T654" id="Seg_3841" n="e" s="T653">pija </ts>
               <ts e="T655" id="Seg_3843" n="e" s="T654">ni </ts>
               <ts e="T656" id="Seg_3845" n="e" s="T655">qaj </ts>
               <ts e="T657" id="Seg_3847" n="e" s="T656">aš </ts>
               <ts e="T658" id="Seg_3849" n="e" s="T657">tɛnimɔːtɨt. </ts>
               <ts e="T659" id="Seg_3851" n="e" s="T658">Ta </ts>
               <ts e="T660" id="Seg_3853" n="e" s="T659">qənna. </ts>
               <ts e="T661" id="Seg_3855" n="e" s="T660">Täpɨt </ts>
               <ts e="T662" id="Seg_3857" n="e" s="T661">ilʼčʼantɨ </ts>
               <ts e="T663" id="Seg_3859" n="e" s="T662">mɔːttɨ </ts>
               <ts e="T664" id="Seg_3861" n="e" s="T663">šernɔːqı. </ts>
               <ts e="T665" id="Seg_3863" n="e" s="T664">Qaqlɨlʼ </ts>
               <ts e="T666" id="Seg_3865" n="e" s="T665">tɔːktɨ </ts>
               <ts e="T667" id="Seg_3867" n="e" s="T666">kurɨlʼä </ts>
               <ts e="T668" id="Seg_3869" n="e" s="T667">nılʼčʼik </ts>
               <ts e="T669" id="Seg_3871" n="e" s="T668">tottɔːtɨt. </ts>
               <ts e="T670" id="Seg_3873" n="e" s="T669">Toːna </ts>
               <ts e="T671" id="Seg_3875" n="e" s="T670">qənmɨntɔːtɨt, </ts>
               <ts e="T672" id="Seg_3877" n="e" s="T671">moqɨnä </ts>
               <ts e="T673" id="Seg_3879" n="e" s="T672">na </ts>
               <ts e="T674" id="Seg_3881" n="e" s="T673">qənmɨntɔːtɨt. </ts>
               <ts e="T675" id="Seg_3883" n="e" s="T674">Pan </ts>
               <ts e="T676" id="Seg_3885" n="e" s="T675">irä </ts>
               <ts e="T677" id="Seg_3887" n="e" s="T676">takkɨt </ts>
               <ts e="T678" id="Seg_3889" n="e" s="T677">kekkɨsa </ts>
               <ts e="T679" id="Seg_3891" n="e" s="T678">mɔːtqɨntɨ </ts>
               <ts e="T680" id="Seg_3893" n="e" s="T679">tulʼišpa </ts>
               <ts e="T681" id="Seg_3895" n="e" s="T680">ıllä </ts>
               <ts e="T682" id="Seg_3897" n="e" s="T681">qumpa. </ts>
               <ts e="T683" id="Seg_3899" n="e" s="T682">Toːna </ts>
               <ts e="T684" id="Seg_3901" n="e" s="T683">šittɨ </ts>
               <ts e="T685" id="Seg_3903" n="e" s="T684">qumoːqı </ts>
               <ts e="T686" id="Seg_3905" n="e" s="T685">ilɨmpɔːqı. </ts>
               <ts e="T687" id="Seg_3907" n="e" s="T686">Pan </ts>
               <ts e="T688" id="Seg_3909" n="e" s="T687">ira </ts>
               <ts e="T689" id="Seg_3911" n="e" s="T688">nimtɨ </ts>
               <ts e="T690" id="Seg_3913" n="e" s="T689">ɛːŋa. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_3914" s="T0">NEP_1965_OrphanBoyAndPanOldMan1_flk.001 (001.001)</ta>
            <ta e="T9" id="Seg_3915" s="T5">NEP_1965_OrphanBoyAndPanOldMan1_flk.002 (001.002)</ta>
            <ta e="T12" id="Seg_3916" s="T9">NEP_1965_OrphanBoyAndPanOldMan1_flk.003 (001.003)</ta>
            <ta e="T19" id="Seg_3917" s="T12">NEP_1965_OrphanBoyAndPanOldMan1_flk.004 (001.004)</ta>
            <ta e="T25" id="Seg_3918" s="T19">NEP_1965_OrphanBoyAndPanOldMan1_flk.005 (001.005)</ta>
            <ta e="T29" id="Seg_3919" s="T25">NEP_1965_OrphanBoyAndPanOldMan1_flk.006 (001.006)</ta>
            <ta e="T35" id="Seg_3920" s="T29">NEP_1965_OrphanBoyAndPanOldMan1_flk.007 (001.007)</ta>
            <ta e="T40" id="Seg_3921" s="T35">NEP_1965_OrphanBoyAndPanOldMan1_flk.008 (001.008)</ta>
            <ta e="T47" id="Seg_3922" s="T40">NEP_1965_OrphanBoyAndPanOldMan1_flk.009 (001.009)</ta>
            <ta e="T52" id="Seg_3923" s="T47">NEP_1965_OrphanBoyAndPanOldMan1_flk.010 (001.010)</ta>
            <ta e="T54" id="Seg_3924" s="T52">NEP_1965_OrphanBoyAndPanOldMan1_flk.011 (001.011)</ta>
            <ta e="T58" id="Seg_3925" s="T54">NEP_1965_OrphanBoyAndPanOldMan1_flk.012 (001.012)</ta>
            <ta e="T62" id="Seg_3926" s="T58">NEP_1965_OrphanBoyAndPanOldMan1_flk.013 (001.013)</ta>
            <ta e="T65" id="Seg_3927" s="T62">NEP_1965_OrphanBoyAndPanOldMan1_flk.014 (001.014)</ta>
            <ta e="T69" id="Seg_3928" s="T65">NEP_1965_OrphanBoyAndPanOldMan1_flk.015 (001.015)</ta>
            <ta e="T75" id="Seg_3929" s="T69">NEP_1965_OrphanBoyAndPanOldMan1_flk.016 (001.016)</ta>
            <ta e="T81" id="Seg_3930" s="T75">NEP_1965_OrphanBoyAndPanOldMan1_flk.017 (001.017)</ta>
            <ta e="T83" id="Seg_3931" s="T81">NEP_1965_OrphanBoyAndPanOldMan1_flk.018 (001.018)</ta>
            <ta e="T88" id="Seg_3932" s="T83">NEP_1965_OrphanBoyAndPanOldMan1_flk.019 (001.019)</ta>
            <ta e="T93" id="Seg_3933" s="T88">NEP_1965_OrphanBoyAndPanOldMan1_flk.020 (001.020)</ta>
            <ta e="T97" id="Seg_3934" s="T93">NEP_1965_OrphanBoyAndPanOldMan1_flk.021 (001.021)</ta>
            <ta e="T100" id="Seg_3935" s="T97">NEP_1965_OrphanBoyAndPanOldMan1_flk.022 (001.022)</ta>
            <ta e="T102" id="Seg_3936" s="T100">NEP_1965_OrphanBoyAndPanOldMan1_flk.023 (001.023)</ta>
            <ta e="T107" id="Seg_3937" s="T102">NEP_1965_OrphanBoyAndPanOldMan1_flk.024 (001.024)</ta>
            <ta e="T110" id="Seg_3938" s="T107">NEP_1965_OrphanBoyAndPanOldMan1_flk.025 (001.025)</ta>
            <ta e="T112" id="Seg_3939" s="T110">NEP_1965_OrphanBoyAndPanOldMan1_flk.026 (001.026)</ta>
            <ta e="T118" id="Seg_3940" s="T112">NEP_1965_OrphanBoyAndPanOldMan1_flk.027 (001.027)</ta>
            <ta e="T123" id="Seg_3941" s="T118">NEP_1965_OrphanBoyAndPanOldMan1_flk.028 (001.028)</ta>
            <ta e="T128" id="Seg_3942" s="T123">NEP_1965_OrphanBoyAndPanOldMan1_flk.029 (001.029)</ta>
            <ta e="T133" id="Seg_3943" s="T128">NEP_1965_OrphanBoyAndPanOldMan1_flk.030 (001.030)</ta>
            <ta e="T135" id="Seg_3944" s="T133">NEP_1965_OrphanBoyAndPanOldMan1_flk.031 (001.031)</ta>
            <ta e="T142" id="Seg_3945" s="T135">NEP_1965_OrphanBoyAndPanOldMan1_flk.032 (001.032)</ta>
            <ta e="T145" id="Seg_3946" s="T142">NEP_1965_OrphanBoyAndPanOldMan1_flk.033 (001.033)</ta>
            <ta e="T150" id="Seg_3947" s="T145">NEP_1965_OrphanBoyAndPanOldMan1_flk.034 (001.034)</ta>
            <ta e="T152" id="Seg_3948" s="T150">NEP_1965_OrphanBoyAndPanOldMan1_flk.035 (001.035)</ta>
            <ta e="T157" id="Seg_3949" s="T152">NEP_1965_OrphanBoyAndPanOldMan1_flk.036 (001.036)</ta>
            <ta e="T163" id="Seg_3950" s="T157">NEP_1965_OrphanBoyAndPanOldMan1_flk.037 (001.037)</ta>
            <ta e="T167" id="Seg_3951" s="T163">NEP_1965_OrphanBoyAndPanOldMan1_flk.038 (001.038)</ta>
            <ta e="T171" id="Seg_3952" s="T167">NEP_1965_OrphanBoyAndPanOldMan1_flk.039 (001.039)</ta>
            <ta e="T175" id="Seg_3953" s="T171">NEP_1965_OrphanBoyAndPanOldMan1_flk.040 (001.040)</ta>
            <ta e="T181" id="Seg_3954" s="T175">NEP_1965_OrphanBoyAndPanOldMan1_flk.041 (001.041)</ta>
            <ta e="T182" id="Seg_3955" s="T181">NEP_1965_OrphanBoyAndPanOldMan1_flk.042 (001.042)</ta>
            <ta e="T192" id="Seg_3956" s="T182">NEP_1965_OrphanBoyAndPanOldMan1_flk.043 (001.043)</ta>
            <ta e="T195" id="Seg_3957" s="T192">NEP_1965_OrphanBoyAndPanOldMan1_flk.044 (001.044)</ta>
            <ta e="T200" id="Seg_3958" s="T195">NEP_1965_OrphanBoyAndPanOldMan1_flk.045 (001.045)</ta>
            <ta e="T209" id="Seg_3959" s="T200">NEP_1965_OrphanBoyAndPanOldMan1_flk.046 (001.046)</ta>
            <ta e="T212" id="Seg_3960" s="T209">NEP_1965_OrphanBoyAndPanOldMan1_flk.047 (001.047)</ta>
            <ta e="T216" id="Seg_3961" s="T212">NEP_1965_OrphanBoyAndPanOldMan1_flk.048 (001.048)</ta>
            <ta e="T223" id="Seg_3962" s="T216">NEP_1965_OrphanBoyAndPanOldMan1_flk.049 (001.049)</ta>
            <ta e="T227" id="Seg_3963" s="T223">NEP_1965_OrphanBoyAndPanOldMan1_flk.050 (001.050)</ta>
            <ta e="T232" id="Seg_3964" s="T227">NEP_1965_OrphanBoyAndPanOldMan1_flk.051 (001.051)</ta>
            <ta e="T236" id="Seg_3965" s="T232">NEP_1965_OrphanBoyAndPanOldMan1_flk.052 (001.052)</ta>
            <ta e="T241" id="Seg_3966" s="T236">NEP_1965_OrphanBoyAndPanOldMan1_flk.053 (001.053)</ta>
            <ta e="T252" id="Seg_3967" s="T241">NEP_1965_OrphanBoyAndPanOldMan1_flk.054 (001.054)</ta>
            <ta e="T256" id="Seg_3968" s="T252">NEP_1965_OrphanBoyAndPanOldMan1_flk.055 (001.055)</ta>
            <ta e="T263" id="Seg_3969" s="T256">NEP_1965_OrphanBoyAndPanOldMan1_flk.056 (001.056)</ta>
            <ta e="T267" id="Seg_3970" s="T263">NEP_1965_OrphanBoyAndPanOldMan1_flk.057 (001.057)</ta>
            <ta e="T270" id="Seg_3971" s="T267">NEP_1965_OrphanBoyAndPanOldMan1_flk.058 (001.058)</ta>
            <ta e="T278" id="Seg_3972" s="T270">NEP_1965_OrphanBoyAndPanOldMan1_flk.059 (001.059)</ta>
            <ta e="T286" id="Seg_3973" s="T278">NEP_1965_OrphanBoyAndPanOldMan1_flk.060 (001.060)</ta>
            <ta e="T289" id="Seg_3974" s="T286">NEP_1965_OrphanBoyAndPanOldMan1_flk.061 (001.061)</ta>
            <ta e="T294" id="Seg_3975" s="T289">NEP_1965_OrphanBoyAndPanOldMan1_flk.062 (001.062)</ta>
            <ta e="T296" id="Seg_3976" s="T294">NEP_1965_OrphanBoyAndPanOldMan1_flk.063 (001.063)</ta>
            <ta e="T300" id="Seg_3977" s="T296">NEP_1965_OrphanBoyAndPanOldMan1_flk.064 (001.064)</ta>
            <ta e="T302" id="Seg_3978" s="T300">NEP_1965_OrphanBoyAndPanOldMan1_flk.065 (001.065)</ta>
            <ta e="T314" id="Seg_3979" s="T302">NEP_1965_OrphanBoyAndPanOldMan1_flk.066 (001.066)</ta>
            <ta e="T319" id="Seg_3980" s="T314">NEP_1965_OrphanBoyAndPanOldMan1_flk.067 (001.067)</ta>
            <ta e="T327" id="Seg_3981" s="T319">NEP_1965_OrphanBoyAndPanOldMan1_flk.068 (001.068)</ta>
            <ta e="T330" id="Seg_3982" s="T327">NEP_1965_OrphanBoyAndPanOldMan1_flk.069 (001.069)</ta>
            <ta e="T339" id="Seg_3983" s="T330">NEP_1965_OrphanBoyAndPanOldMan1_flk.070 (001.070)</ta>
            <ta e="T349" id="Seg_3984" s="T339">NEP_1965_OrphanBoyAndPanOldMan1_flk.071 (001.071)</ta>
            <ta e="T354" id="Seg_3985" s="T349">NEP_1965_OrphanBoyAndPanOldMan1_flk.072 (001.072)</ta>
            <ta e="T358" id="Seg_3986" s="T354">NEP_1965_OrphanBoyAndPanOldMan1_flk.073 (001.073)</ta>
            <ta e="T365" id="Seg_3987" s="T358">NEP_1965_OrphanBoyAndPanOldMan1_flk.074 (001.074)</ta>
            <ta e="T370" id="Seg_3988" s="T365">NEP_1965_OrphanBoyAndPanOldMan1_flk.075 (001.075)</ta>
            <ta e="T372" id="Seg_3989" s="T370">NEP_1965_OrphanBoyAndPanOldMan1_flk.076 (001.076)</ta>
            <ta e="T379" id="Seg_3990" s="T372">NEP_1965_OrphanBoyAndPanOldMan1_flk.077 (001.077)</ta>
            <ta e="T382" id="Seg_3991" s="T379">NEP_1965_OrphanBoyAndPanOldMan1_flk.078 (001.078)</ta>
            <ta e="T385" id="Seg_3992" s="T382">NEP_1965_OrphanBoyAndPanOldMan1_flk.079 (001.079)</ta>
            <ta e="T389" id="Seg_3993" s="T385">NEP_1965_OrphanBoyAndPanOldMan1_flk.080 (001.080)</ta>
            <ta e="T396" id="Seg_3994" s="T389">NEP_1965_OrphanBoyAndPanOldMan1_flk.081 (001.081)</ta>
            <ta e="T407" id="Seg_3995" s="T396">NEP_1965_OrphanBoyAndPanOldMan1_flk.082 (001.082)</ta>
            <ta e="T411" id="Seg_3996" s="T407">NEP_1965_OrphanBoyAndPanOldMan1_flk.083 (001.083)</ta>
            <ta e="T416" id="Seg_3997" s="T411">NEP_1965_OrphanBoyAndPanOldMan1_flk.084 (001.084)</ta>
            <ta e="T421" id="Seg_3998" s="T416">NEP_1965_OrphanBoyAndPanOldMan1_flk.085 (001.085)</ta>
            <ta e="T426" id="Seg_3999" s="T421">NEP_1965_OrphanBoyAndPanOldMan1_flk.086 (001.086)</ta>
            <ta e="T428" id="Seg_4000" s="T426">NEP_1965_OrphanBoyAndPanOldMan1_flk.087 (001.087)</ta>
            <ta e="T436" id="Seg_4001" s="T428">NEP_1965_OrphanBoyAndPanOldMan1_flk.088 (001.088)</ta>
            <ta e="T442" id="Seg_4002" s="T436">NEP_1965_OrphanBoyAndPanOldMan1_flk.089 (001.089)</ta>
            <ta e="T444" id="Seg_4003" s="T442">NEP_1965_OrphanBoyAndPanOldMan1_flk.090 (001.090)</ta>
            <ta e="T452" id="Seg_4004" s="T444">NEP_1965_OrphanBoyAndPanOldMan1_flk.091 (001.091)</ta>
            <ta e="T455" id="Seg_4005" s="T452">NEP_1965_OrphanBoyAndPanOldMan1_flk.092 (001.092)</ta>
            <ta e="T461" id="Seg_4006" s="T455">NEP_1965_OrphanBoyAndPanOldMan1_flk.093 (001.093)</ta>
            <ta e="T467" id="Seg_4007" s="T461">NEP_1965_OrphanBoyAndPanOldMan1_flk.094 (001.094)</ta>
            <ta e="T474" id="Seg_4008" s="T467">NEP_1965_OrphanBoyAndPanOldMan1_flk.095 (001.095)</ta>
            <ta e="T480" id="Seg_4009" s="T474">NEP_1965_OrphanBoyAndPanOldMan1_flk.096 (001.096)</ta>
            <ta e="T486" id="Seg_4010" s="T480">NEP_1965_OrphanBoyAndPanOldMan1_flk.097 (001.097)</ta>
            <ta e="T489" id="Seg_4011" s="T486">NEP_1965_OrphanBoyAndPanOldMan1_flk.098 (001.098)</ta>
            <ta e="T491" id="Seg_4012" s="T489">NEP_1965_OrphanBoyAndPanOldMan1_flk.099 (001.099)</ta>
            <ta e="T497" id="Seg_4013" s="T491">NEP_1965_OrphanBoyAndPanOldMan1_flk.100 (001.100)</ta>
            <ta e="T503" id="Seg_4014" s="T497">NEP_1965_OrphanBoyAndPanOldMan1_flk.101 (001.101)</ta>
            <ta e="T509" id="Seg_4015" s="T503">NEP_1965_OrphanBoyAndPanOldMan1_flk.102 (001.102)</ta>
            <ta e="T516" id="Seg_4016" s="T509">NEP_1965_OrphanBoyAndPanOldMan1_flk.103 (001.103)</ta>
            <ta e="T522" id="Seg_4017" s="T516">NEP_1965_OrphanBoyAndPanOldMan1_flk.104 (001.104)</ta>
            <ta e="T526" id="Seg_4018" s="T522">NEP_1965_OrphanBoyAndPanOldMan1_flk.105 (001.105)</ta>
            <ta e="T532" id="Seg_4019" s="T526">NEP_1965_OrphanBoyAndPanOldMan1_flk.106 (001.106)</ta>
            <ta e="T536" id="Seg_4020" s="T532">NEP_1965_OrphanBoyAndPanOldMan1_flk.107 (001.107)</ta>
            <ta e="T541" id="Seg_4021" s="T536">NEP_1965_OrphanBoyAndPanOldMan1_flk.108 (001.108)</ta>
            <ta e="T543" id="Seg_4022" s="T541">NEP_1965_OrphanBoyAndPanOldMan1_flk.109 (001.109)</ta>
            <ta e="T545" id="Seg_4023" s="T543">NEP_1965_OrphanBoyAndPanOldMan1_flk.110 (001.110)</ta>
            <ta e="T549" id="Seg_4024" s="T545">NEP_1965_OrphanBoyAndPanOldMan1_flk.111 (001.111)</ta>
            <ta e="T560" id="Seg_4025" s="T549">NEP_1965_OrphanBoyAndPanOldMan1_flk.112 (001.112)</ta>
            <ta e="T565" id="Seg_4026" s="T560">NEP_1965_OrphanBoyAndPanOldMan1_flk.113 (001.113)</ta>
            <ta e="T572" id="Seg_4027" s="T565">NEP_1965_OrphanBoyAndPanOldMan1_flk.114 (001.114)</ta>
            <ta e="T577" id="Seg_4028" s="T572">NEP_1965_OrphanBoyAndPanOldMan1_flk.115 (001.115)</ta>
            <ta e="T583" id="Seg_4029" s="T577">NEP_1965_OrphanBoyAndPanOldMan1_flk.116 (001.116)</ta>
            <ta e="T587" id="Seg_4030" s="T583">NEP_1965_OrphanBoyAndPanOldMan1_flk.117 (001.117)</ta>
            <ta e="T600" id="Seg_4031" s="T587">NEP_1965_OrphanBoyAndPanOldMan1_flk.118 (001.118)</ta>
            <ta e="T607" id="Seg_4032" s="T600">NEP_1965_OrphanBoyAndPanOldMan1_flk.119 (001.119)</ta>
            <ta e="T611" id="Seg_4033" s="T607">NEP_1965_OrphanBoyAndPanOldMan1_flk.120 (001.120)</ta>
            <ta e="T613" id="Seg_4034" s="T611">NEP_1965_OrphanBoyAndPanOldMan1_flk.121 (001.121)</ta>
            <ta e="T619" id="Seg_4035" s="T613">NEP_1965_OrphanBoyAndPanOldMan1_flk.122 (001.122)</ta>
            <ta e="T626" id="Seg_4036" s="T619">NEP_1965_OrphanBoyAndPanOldMan1_flk.123 (001.123)</ta>
            <ta e="T632" id="Seg_4037" s="T626">NEP_1965_OrphanBoyAndPanOldMan1_flk.124 (001.124)</ta>
            <ta e="T635" id="Seg_4038" s="T632">NEP_1965_OrphanBoyAndPanOldMan1_flk.125 (001.125)</ta>
            <ta e="T638" id="Seg_4039" s="T635">NEP_1965_OrphanBoyAndPanOldMan1_flk.126 (001.126)</ta>
            <ta e="T642" id="Seg_4040" s="T638">NEP_1965_OrphanBoyAndPanOldMan1_flk.127 (001.127)</ta>
            <ta e="T645" id="Seg_4041" s="T642">NEP_1965_OrphanBoyAndPanOldMan1_flk.128 (001.128)</ta>
            <ta e="T649" id="Seg_4042" s="T645">NEP_1965_OrphanBoyAndPanOldMan1_flk.129 (001.129)</ta>
            <ta e="T658" id="Seg_4043" s="T649">NEP_1965_OrphanBoyAndPanOldMan1_flk.130 (001.130)</ta>
            <ta e="T660" id="Seg_4044" s="T658">NEP_1965_OrphanBoyAndPanOldMan1_flk.131 (001.131)</ta>
            <ta e="T664" id="Seg_4045" s="T660">NEP_1965_OrphanBoyAndPanOldMan1_flk.132 (001.132)</ta>
            <ta e="T669" id="Seg_4046" s="T664">NEP_1965_OrphanBoyAndPanOldMan1_flk.133 (001.133)</ta>
            <ta e="T674" id="Seg_4047" s="T669">NEP_1965_OrphanBoyAndPanOldMan1_flk.134 (001.134)</ta>
            <ta e="T682" id="Seg_4048" s="T674">NEP_1965_OrphanBoyAndPanOldMan1_flk.135 (001.135)</ta>
            <ta e="T686" id="Seg_4049" s="T682">NEP_1965_OrphanBoyAndPanOldMan1_flk.136 (001.136)</ta>
            <ta e="T690" id="Seg_4050" s="T686">NEP_1965_OrphanBoyAndPanOldMan1_flk.137 (001.137)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_4051" s="T0">уккыр и′jаты еппымынта, ′не̨кытыль ′пелыкытыль.</ta>
            <ta e="T9" id="Seg_4052" s="T5">на ′тенти[ы]лʼ а′ше чаптӓ.</ta>
            <ta e="T12" id="Seg_4053" s="T9">ира ′илымпа kӯталʼпа.</ta>
            <ta e="T19" id="Seg_4054" s="T12">на ′шʼолʼ kумылʼ ира тапплʼӓлʼ ′kоlтит пе′лʼаkkыт ′илымпа.</ta>
            <ta e="T25" id="Seg_4055" s="T19">′по̄kытылʼ ′тъ̊тыт ′то̄чиkыт ′ӣлымпа тинтена kӱ̄тӓлпа.</ta>
            <ta e="T29" id="Seg_4056" s="T25">уккыр и′яты ′пелыкы[о]тылʼ ′некытылʼ.</ta>
            <ta e="T35" id="Seg_4057" s="T29">ни ′нӓлʼаты ′чӓңка ни ′kайты ′че[ӓ]ңка.</ta>
            <ta e="T40" id="Seg_4058" s="T35">ныны ə[о̄]сыты кӱ̄′те̨ла на ′иjалʼа.</ta>
            <ta e="T47" id="Seg_4059" s="T40">на ′иjа ′kапиjа ′нымты ӣllа, ъ̊̄сынтысӓ ъ̊̄мынтысӓ.</ta>
            <ta e="T52" id="Seg_4060" s="T47">′тонты шʼиты ′кӓ̄лылʼ тимнʼӓсыт ′е̨ппын′тотыт.</ta>
            <ta e="T54" id="Seg_4061" s="T52">′кӓ̄лʼирӓ пут ′че̄нта.</ta>
            <ta e="T58" id="Seg_4062" s="T54">тындӓна ′ирра ′иllӓ kӯңа.</ta>
            <ta e="T62" id="Seg_4063" s="T58">тиндена ′kӓ̄лʼира ′куче kа̄′тенда.</ta>
            <ta e="T65" id="Seg_4064" s="T62">то̄ ты[и]мнʼӓнтыны ′пӯңа.</ta>
            <ta e="T69" id="Seg_4065" s="T65">и′рра kӯнта, иllӓ ме̄′ңотыт.</ta>
            <ta e="T75" id="Seg_4066" s="T69">шʼиттӓl ′имаkотаты ′кӱ̄тӓlе, ай ′иllӓ kӯңа.</ta>
            <ta e="T81" id="Seg_4067" s="T75">ираl мы′kӓ и′jатыт ′ней′колы пелыколы ′kа̄lа.</ta>
            <ta e="T83" id="Seg_4068" s="T81">ку′че kа′тента.</ta>
            <ta e="T88" id="Seg_4069" s="T83">на кӓ̄lи и′ра шʼиттаl то ′пӯтымпаты.</ta>
            <ta e="T93" id="Seg_4070" s="T88">′мӓрkы и′jаты ниlчиl и′jаты ′е̨ппымынта.</ta>
            <ta e="T97" id="Seg_4071" s="T93">пӯтыңыты ′о̨̄тӓты ай ′чӓ̄ңка.</ta>
            <ta e="T100" id="Seg_4072" s="T97">отӓколык ′топысӓ ‵илым′по̄тыт.</ta>
            <ta e="T102" id="Seg_4073" s="T100">нымты иlымпотыт.</ta>
            <ta e="T107" id="Seg_4074" s="T102">иlымпотыт, то ′пӯлʼе, k[к]ӓlы ират ′мотты.</ta>
            <ta e="T110" id="Seg_4075" s="T107">′кӓlы ′ирра иjаты ′чӓңымпа.</ta>
            <ta e="T112" id="Seg_4076" s="T110">и′jакы[о]тыl и′маты.</ta>
            <ta e="T118" id="Seg_4077" s="T112">то̄на чонтолʼ тим′нʼаты уккыр нӓлʼӓты ′пе̄лыкытыl.</ta>
            <ta e="T123" id="Seg_4078" s="T118">′таккыны ′нʼӓнна шʼиты ′kӓ̄lык ′тӱ̄ңа.</ta>
            <ta e="T128" id="Seg_4079" s="T123">′поkкытыl ′тъ̊ттокы[о]′ны на ′kӓlи ира ′наjента.</ta>
            <ta e="T133" id="Seg_4080" s="T128">′кушаlʼ там′тырты ′о̨тӓп ′мунтык ‵таkkышпаты.</ta>
            <ta e="T135" id="Seg_4081" s="T133">ниlчик ′тӱ̄ңа.</ta>
            <ta e="T142" id="Seg_4082" s="T135">на тымнʼасыка ӣ′llотыт на ′ираl тымнʼ′ӓсыка ′шʼитты.</ta>
            <ta e="T145" id="Seg_4083" s="T142">тӓтты тот ′о̨тӓkиты.</ta>
            <ta e="T150" id="Seg_4084" s="T145">на ′иран ′о̨тӓп иɣ[k]ынтоkо ′тӱнта.</ta>
            <ta e="T152" id="Seg_4085" s="T150">ку′минтысӓ ′тӱ̄нта.</ta>
            <ta e="T157" id="Seg_4086" s="T152">па̄н ′ӣра, ′нимты ай ′найта.</ta>
            <ta e="T163" id="Seg_4087" s="T157">ныны тӱ̄lе шʼе̨̄′kотыт на тӓпытыт ′мотkыт.</ta>
            <ta e="T167" id="Seg_4088" s="T163">на ′иjа аш ′тӓнымотыт.</ta>
            <ta e="T171" id="Seg_4089" s="T167">ӣjа куттар та̊̄пынык ′аlчимпа.</ta>
            <ta e="T175" id="Seg_4090" s="T171">тӓп kай мӯды′ронак ӣlа.</ta>
            <ta e="T181" id="Seg_4091" s="T175">kуп куттар ′тӓни‵ментӓты аl′пет ′иlӓнтыl kуп.</ta>
            <ta e="T182" id="Seg_4092" s="T181">шʼе̄к ′kоты[ӓ]т.</ta>
            <ta e="T192" id="Seg_4093" s="T182">момпа но̄ртӓlиl че̄lыт то̄kkыт момпа пан ира кӧт kупсӓ тӱ̄ңа.</ta>
            <ta e="T195" id="Seg_4094" s="T192">kапиjа ′о̨тӓп ′тоkkи kонто′kо.</ta>
            <ta e="T200" id="Seg_4095" s="T195">на ′тинтена а′сӓстит ′о̨̄тӓп ′тоkkыkо.</ta>
            <ta e="T209" id="Seg_4096" s="T200">кун′монтыl ′мо̄тты шʼе̨̄[ӓ]нты кун ′монтыl kайты мӯн ′таkkалʼ′те̄нтыт.</ta>
            <ta e="T212" id="Seg_4097" s="T209">у′кон[т] нота ′таkkыlkоlчимпат.</ta>
            <ta e="T216" id="Seg_4098" s="T212">‵kелʼитыт ′о̨̄тап ′таkkылʼе ′ӣмпаты.</ta>
            <ta e="T223" id="Seg_4099" s="T216">ныны ′шʼиттӓl ′мыңа ′моkынӓ kъ̊нноkий на kӓлыкkӣ.</ta>
            <ta e="T227" id="Seg_4100" s="T223">′нортӓlиl че̄лʼит момпа тӱн′томыт.</ta>
            <ta e="T232" id="Seg_4101" s="T227">те̄мумпа ′кучче ыкы ′kъ̊ннылыт.</ta>
            <ta e="T236" id="Seg_4102" s="T232">шʼиттеlыl че̄лонты ыннӓ ′че̄лʼинна.</ta>
            <ta e="T241" id="Seg_4103" s="T236">на ӣjа ны̄l ′kъ̊̄тытыт иl′чантынык.</ta>
            <ta e="T252" id="Seg_4104" s="T241">′мыта нʼа′ннӓт ′тымты тӓтты тот[н] ′о̨̄тат ′ныңkы ку′шалʼ нʼа′роп е̄нта, на′че ′ӱ̄тӓlымыт.</ta>
            <ta e="T256" id="Seg_4105" s="T252">′кӓ̄lит ′тӱ̄нтотыт ′о̨̄тӓп ‵тоkkыколамнӓн′тотыт.</ta>
            <ta e="T263" id="Seg_4106" s="T256">′о̨̄тан ′упкыт къl′чомынты кунты ′kӓрыпылʼ кутар ме̄′томыт.</ta>
            <ta e="T267" id="Seg_4107" s="T263">на ′ӣjа нӣк ′kъ̊ттыңыт.</ta>
            <ta e="T270" id="Seg_4108" s="T267">ныны нӣк kъ̊ттыңыты.</ta>
            <ta e="T278" id="Seg_4109" s="T270">′мʼӓкке ′мыта ′шʼенты ′моllе̨т е̄ңа, иlча ′мʼӓккʼе ′митты.</ta>
            <ta e="T286" id="Seg_4110" s="T278">а тона ′тинте тимнʼӓнты нӓlа иl′чаты ′натып кӯраlтымпаты.</ta>
            <ta e="T289" id="Seg_4111" s="T286">на ӣ′jанык …</ta>
            <ta e="T294" id="Seg_4112" s="T289">′нʼӓ′ннӓт ′тымты ′ма̄чеl ′тӣчи е̄ңа.</ta>
            <ta e="T296" id="Seg_4113" s="T294">нымты ′мо̄тың′нентак.</ta>
            <ta e="T300" id="Seg_4114" s="T296">шʼенд̂ы мо̄т ′татыңыт мы′та.</ta>
            <ta e="T302" id="Seg_4115" s="T300">‵ӱ̄тӓl′нотыт та̄l.</ta>
            <ta e="T314" id="Seg_4116" s="T302">′kапиjе ны ′нортӓlиl чʼе̄лы тӱ̄н′то̄тыт пан и′ралʼмыт тъ̊пытыткӣне, на кӓлы и′рат ′отап та̄′kаlkо.</ta>
            <ta e="T319" id="Seg_4117" s="T314">ты[и]мнʼӓсыkӓт ′о̨̄тӓп ныны шʼи′ттеl ӱ̄тӓlто̄тыт.</ta>
            <ta e="T327" id="Seg_4118" s="T319">на о̨̄тӓп на ‵тоkӓн′то̄тыт ′kапиj тӓм[п] ай ′то̄kымпаты.</ta>
            <ta e="T330" id="Seg_4119" s="T327">′моссӓ на мин′тотыт.</ta>
            <ta e="T339" id="Seg_4120" s="T330">на ′ма̄чит тичит нʼӓннӓl пӓlʼӓkыт ны на ′мо̄тың′то̄тыт на ӣ′мантысӓ.</ta>
            <ta e="T349" id="Seg_4121" s="T339">kапиjе тӓ[ъ̊]ттаl ′кӓlык kай ′мотты ′ча̊̄ңка ′шʼенты мо̄ты ′kопылʼ ′мо̄тты.</ta>
            <ta e="T354" id="Seg_4122" s="T349">′ныны шʼиттеl шʼӓk′kо̄тыт на и′мантысӓ.</ta>
            <ta e="T358" id="Seg_4123" s="T354">тəптыl kарыт нӣң ӓ̄са.</ta>
            <ta e="T365" id="Seg_4124" s="T358">′тинтена и′мантынык мат ′мыта ′нʼӓнна kу′мӣkӓк ‵ко̄ны′лʼек.</ta>
            <ta e="T370" id="Seg_4125" s="T365">kай ′мыта мо̄тынпотыт kай kа̄′тотыт.</ta>
            <ta e="T372" id="Seg_4126" s="T370">нʼӓннӓ lакаl′телʼча.</ta>
            <ta e="T379" id="Seg_4127" s="T372">но̨̄кыр ′kоптып ′со̄рылʼа kапиjа ′носсарылʼ ′о̨тӓп ′kалтымынтыт.</ta>
            <ta e="T382" id="Seg_4128" s="T379">иjа нʼанна kъ̊′ннеjа.</ta>
            <ta e="T385" id="Seg_4129" s="T382">ӣ′маты онты kа̊̄lа.</ta>
            <ta e="T389" id="Seg_4130" s="T385">на ′омта на ′омта.</ta>
            <ta e="T396" id="Seg_4131" s="T389">уккыр тӓт чонтоkыт че̄lыты ӱ̄тонты на ′къ̊нна.</ta>
            <ta e="T407" id="Seg_4132" s="T396">на ӣjа лʼамы къ̊̄ңа, нʼӓ′ннӓт уккыр та[ъ̊т] чон′тоkыт kуп на′jап татын′тотыт.</ta>
            <ta e="T411" id="Seg_4133" s="T407">таkkыны пан и′ралʼмыт ′та̊тынто̄тыт.</ta>
            <ta e="T416" id="Seg_4134" s="T411">кӧт ′kумты kапиjа ′отӓп ‵тоkы′сотыl.</ta>
            <ta e="T421" id="Seg_4135" s="T416">пан ира на′шак kъ̊̄тысӓ е̄ңа.</ta>
            <ta e="T426" id="Seg_4136" s="T421">нын ′шʼиттаl на ′иjа тӱlчикунӓ.</ta>
            <ta e="T428" id="Seg_4137" s="T426">′иматӓ ′ӱңкыlтимпаты.</ta>
            <ta e="T436" id="Seg_4138" s="T428">монты чап тӱ̄lча пан и′ралʼмыт kай монты тӱ′ңотыт.</ta>
            <ta e="T442" id="Seg_4139" s="T436">kаГlилʼ мыты нык тот′телʼ′ноты[т], ′поkыт ′е̨̄ты ман′тоkыт .</ta>
            <ta e="T444" id="Seg_4140" s="T442">′мотты шʼе̄рна.</ta>
            <ta e="T452" id="Seg_4141" s="T444">′мо̄та нот ′ныңkы[ӓ]лʼа мота′нот кʼе̄ ′топымты тӯ′палкыты.</ta>
            <ta e="T455" id="Seg_4142" s="T452">′пе̄мымты на тӯ′паlныт.</ta>
            <ta e="T461" id="Seg_4143" s="T455">нӣ ′kъ̊ттыңыты пан и′ранык нʼӓннӓ kо′ралʼтылʼе.</ta>
            <ta e="T467" id="Seg_4144" s="T461">′оглы ′мӣтолʼлʼа ′коlтыт kъ̊kыт о̨̄lа.</ta>
            <ta e="T474" id="Seg_4145" s="T467">та̄l на ′нʼӓнна паkтымпа на ′купаксӓ ′тӓɣыlемынтыты.</ta>
            <ta e="T480" id="Seg_4146" s="T474">пан ′ираl ′мытып мӯнтыкъ ′лʼӓпе̨к ′kӓттымб̂ат.</ta>
            <ta e="T486" id="Seg_4147" s="T480">′оммыты kумпа пан ира ′иlилʼе kа̄лымпа.</ta>
            <ta e="T489" id="Seg_4148" s="T486">шʼиттӓkий ′иlи[ы]лʼлʼе ′kа̄лымы‵нта.</ta>
            <ta e="T491" id="Seg_4149" s="T489">′понӓ kъ̊′llӓиты.</ta>
            <ta e="T497" id="Seg_4150" s="T491">на ′ӣjа ук′кур ′поры kу′паксӓ тӓɣы′l[лʼ]емпаты.</ta>
            <ta e="T503" id="Seg_4151" s="T497">′мунтык порɣ[k]ӓнты щʼу′нʼӧнты [пӱммынты щʼу] нʼӧlkы′мот ′по̄тӓт.</ta>
            <ta e="T509" id="Seg_4152" s="T503">на ′kупылʼмыт ныны шʼитӓl по̄нӓ кӓ′llеиты.</ta>
            <ta e="T516" id="Seg_4153" s="T509">е̨̄тыман′ноzты тинтена ′kаглы тӱ̄р ′тоlчиты на со̄′релымпа.</ta>
            <ta e="T522" id="Seg_4154" s="T516">пан ирап ай понӓ ′итымпат [′чатымпат].</ta>
            <ta e="T526" id="Seg_4155" s="T522">′kапиjа ′мунтык ′пона ′иттымпат.</ta>
            <ta e="T532" id="Seg_4156" s="T526">′иllа ′омта ‵малʼчалʼ′тоkты то̄ ′ӣlӓ.</ta>
            <ta e="T536" id="Seg_4157" s="T532">′амырлʼе ′иllӓ омынта ′шӓ̄кка.</ta>
            <ta e="T541" id="Seg_4158" s="T536">на ку′миты ′поkыт на и′ппеlым′потыт.</ta>
            <ta e="T543" id="Seg_4159" s="T541">куттар са′релымпа.</ta>
            <ta e="T545" id="Seg_4160" s="T543">ниlчик шʼӓkkа.</ta>
            <ta e="T549" id="Seg_4161" s="T545">топтыl ′kарыт ынzӓ ′вӓша.</ta>
            <ta e="T560" id="Seg_4162" s="T549">пан и′ра ниlчик тап ′wӓндыl′токты ′тотык та′нкӣчимпа ′кʼеккысӓ ‵саи′лʼа[я]ты ′kъ̊тымпа.</ta>
            <ta e="T565" id="Seg_4163" s="T560">то̄на ′kӓ̄лыкkыты ещо ‵сомаlоkы ′епымпа.</ta>
            <ta e="T572" id="Seg_4164" s="T565">ниl ′kъ̊̄тыңыты на и′jа мыта на kӓlитыт ′кӣнӓ.</ta>
            <ta e="T577" id="Seg_4165" s="T572">′моkонӓ ′kъ̊нтат таl′тӓll[лʼлʼ]е[ӓ] ′унтыт ′kаглынтысӓ.</ta>
            <ta e="T583" id="Seg_4166" s="T577">ныны шʼиттӓl то̄нам ′мӓkkий мо̄тты ′шʼерышпотыт.</ta>
            <ta e="T587" id="Seg_4167" s="T583">kай ′а̄мыр′потыт, kай аш.</ta>
            <ta e="T600" id="Seg_4168" s="T587">ныны ′тинтена са′ралʼпытыl ′kаглымтыт ныны моkонӓ ′kъ̊̄челʼлʼе на kу′мимтыт kӯпылʼ kумимты нӣк ′kъ̊нтыңыт.</ta>
            <ta e="T607" id="Seg_4169" s="T600">′таkkы ′моkонӓ пан и′рамты ай ′кочилʼе ′kъ̊нтыты.</ta>
            <ta e="T611" id="Seg_4170" s="T607">kай ′kыка ′kъ̊jиты ′kъ̊нтыт.</ta>
            <ta e="T613" id="Seg_4171" s="T611">kъ̊′ннатыт ′тина.</ta>
            <ta e="T619" id="Seg_4172" s="T613">kумат ′сомак kъ̊′ннотыт, и′мантыны ни ′kъ̊̄тыңыт.</ta>
            <ta e="T626" id="Seg_4173" s="T619">′моllе ′тотилʼе̨[ӓ]ты, ме ′нʼӓнна kъ̊′лʼлʼе ‵kумытыткӣне.</ta>
            <ta e="T632" id="Seg_4174" s="T626">аш ′тӓнимым′паты, илʼ′чантылʼ мыт kос kай.</ta>
            <ta e="T635" id="Seg_4175" s="T632">′ненна ′kъ̊нна ′тинтена.</ta>
            <ta e="T638" id="Seg_4176" s="T635">онта ′аша ′kоңымпа.</ta>
            <ta e="T642" id="Seg_4177" s="T638">′тона ′иматы на kъ̊′тӓлʼмынтыты.</ta>
            <ta e="T645" id="Seg_4178" s="T642">′ъ̊сынтыl и′lчантыl мы′kӓткӣне.</ta>
            <ta e="T649" id="Seg_4179" s="T645">ираl мыт моkонӓ kъ̊′ссотыт.</ta>
            <ta e="T658" id="Seg_4180" s="T649">а нынӓ ′шиттеl пиjа′ка пи′jа ни kай аш ′тӓнимотыт.</ta>
            <ta e="T660" id="Seg_4181" s="T658">та kъ′нна.</ta>
            <ta e="T664" id="Seg_4182" s="T660">тӓпыт илʼчанты ′мо̄тты шерноkи.</ta>
            <ta e="T669" id="Seg_4183" s="T664">kа′ɣlыl тоkты курылʼе ниlчик тот′тотыт.</ta>
            <ta e="T674" id="Seg_4184" s="T669">тона kъ̊нмын′тотыт, моkынӓ на kъ̊нмын′тотыт.</ta>
            <ta e="T682" id="Seg_4185" s="T674">пан ′ирӓ ′таккыт ′кеккыса мотkынты ′тулʼишпа ′иllӓ kумпа.</ta>
            <ta e="T686" id="Seg_4186" s="T682">′тона шʼиты kу′моkи иllим′поты[kи]т.</ta>
            <ta e="T690" id="Seg_4187" s="T686">пан ира нимты ′е̄ңа.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_4188" s="T0">ukkɨr ijatɨ eppɨmɨnta, nekɨtɨlʼ pelɨkɨtɨlʼ.</ta>
            <ta e="T9" id="Seg_4189" s="T5">na tenti[ɨ]lʼ aše čaptä.</ta>
            <ta e="T12" id="Seg_4190" s="T9">ira ilɨmpa quːtalʼpa.</ta>
            <ta e="T19" id="Seg_4191" s="T12">na šolʼ qumɨlʼ ira tapplʼälʼ qolʼtit pelʼaqqɨt ilɨmpa.</ta>
            <ta e="T25" id="Seg_4192" s="T19">poːqɨtɨlʼ tətɨt toːčiqɨt iːlɨmpa tintena qüːtälpa.</ta>
            <ta e="T29" id="Seg_4193" s="T25">ukkɨr ijatɨ pelɨkɨ[o]tɨlʼ nekɨtɨlʼ.</ta>
            <ta e="T35" id="Seg_4194" s="T29">ni nälʼatɨ čäŋka ni qajtɨ če[ä]ŋka.</ta>
            <ta e="T40" id="Seg_4195" s="T35">nɨnɨ ə[oː]sɨtɨ küːtela na ijalʼa.</ta>
            <ta e="T47" id="Seg_4196" s="T40">na ija qapija nɨmtɨ iːlʼlʼa, əːsɨntɨsä əːmɨntɨsä.</ta>
            <ta e="T52" id="Seg_4197" s="T47">tontɨ šitɨ käːlɨlʼ timnʼäsɨt eppɨntotɨt.</ta>
            <ta e="T54" id="Seg_4198" s="T52">käːlʼirä put čeːnta.</ta>
            <ta e="T58" id="Seg_4199" s="T54">tɨndäna irra ilʼlʼä quːŋa.</ta>
            <ta e="T62" id="Seg_4200" s="T58">tindena qäːlʼira kuče qaːtenda.</ta>
            <ta e="T65" id="Seg_4201" s="T62">toː tɨ[i]mnʼäntɨnɨ puːŋa.</ta>
            <ta e="T69" id="Seg_4202" s="T65">irra quːnta, ilʼlʼä meːŋotɨt.</ta>
            <ta e="T75" id="Seg_4203" s="T69">šittälʼ imaqotatɨ küːtälʼe, aj ilʼlʼä quːŋa.</ta>
            <ta e="T81" id="Seg_4204" s="T75">iralʼ mɨqä ijatɨt nejkolɨ pelɨkolɨ qaːlʼa.</ta>
            <ta e="T83" id="Seg_4205" s="T81">kuče qatenta.</ta>
            <ta e="T88" id="Seg_4206" s="T83">na käːlʼi ira šittalʼ to puːtɨmpatɨ.</ta>
            <ta e="T93" id="Seg_4207" s="T88">märqɨ ijatɨ nilʼčilʼ ijatɨ eppɨmɨnta.</ta>
            <ta e="T97" id="Seg_4208" s="T93">puːtɨŋɨtɨ oːtätɨ aj čäːŋka.</ta>
            <ta e="T100" id="Seg_4209" s="T97">otäkolɨk topɨsä ilɨmpoːtɨt.</ta>
            <ta e="T102" id="Seg_4210" s="T100">nɨmtɨ ilʼɨmpotɨt.</ta>
            <ta e="T107" id="Seg_4211" s="T102">ilʼɨmpotɨt, to puːlʼe, q[k]älʼɨ irat mottɨ.</ta>
            <ta e="T110" id="Seg_4212" s="T107">kälʼɨ irra ijatɨ čäŋɨmpa.</ta>
            <ta e="T112" id="Seg_4213" s="T110">ijakɨ[o]tɨlʼ imatɨ.</ta>
            <ta e="T118" id="Seg_4214" s="T112">toːna čontolʼ timnʼatɨ ukkɨr nälʼätɨ peːlɨkɨtɨlʼ.</ta>
            <ta e="T123" id="Seg_4215" s="T118">takkɨnɨ nʼänna šitɨ qäːlʼɨk tüːŋa.</ta>
            <ta e="T128" id="Seg_4216" s="T123">poqkɨtɨlʼ təttokɨ[o]nɨ na qälʼi ira najenta.</ta>
            <ta e="T133" id="Seg_4217" s="T128">kušalʼʼ tamtɨrtɨ otäp muntɨk taqqɨšpatɨ.</ta>
            <ta e="T135" id="Seg_4218" s="T133">nilʼčik tüːŋa.</ta>
            <ta e="T142" id="Seg_4219" s="T135">na tɨmnʼasɨka iːlʼlʼotɨt na iralʼ tɨmnʼäsɨka šittɨ.</ta>
            <ta e="T145" id="Seg_4220" s="T142">tättɨ tot otäqitɨ.</ta>
            <ta e="T150" id="Seg_4221" s="T145">na iran otäp iq[q]ɨntoqo tünta.</ta>
            <ta e="T152" id="Seg_4222" s="T150">kumintɨsä tüːnta.</ta>
            <ta e="T157" id="Seg_4223" s="T152">paːn iːra, nimtɨ aj najta.</ta>
            <ta e="T163" id="Seg_4224" s="T157">nɨnɨ tüːlʼe šeːqotɨt na täpɨtɨt motqɨt.</ta>
            <ta e="T167" id="Seg_4225" s="T163">na ija aš tänɨmotɨt.</ta>
            <ta e="T171" id="Seg_4226" s="T167">iːja kuttar taːpɨnɨk alʼčimpa.</ta>
            <ta e="T175" id="Seg_4227" s="T171">täp qaj muːdɨronak iːlʼa.</ta>
            <ta e="T181" id="Seg_4228" s="T175">qup kuttar tänimentätɨ alʼpet ilʼäntɨlʼ qup.</ta>
            <ta e="T182" id="Seg_4229" s="T181">šeːk qotɨ[ä]t.</ta>
            <ta e="T192" id="Seg_4230" s="T182">mompa noːrtälʼilʼ čeːlʼɨt toːqqɨt mompa pan ira köt qupsä tüːŋa.</ta>
            <ta e="T195" id="Seg_4231" s="T192">qapija otäp toqqi qontoqo.</ta>
            <ta e="T200" id="Seg_4232" s="T195">na tintena asästit oːtäp toqqɨqo.</ta>
            <ta e="T209" id="Seg_4233" s="T200">kunmontɨlʼ moːttɨ šeː[ä]ntɨ kun montɨlʼ qajtɨ muːn taqqalʼteːntɨt.</ta>
            <ta e="T212" id="Seg_4234" s="T209">ukon[t] nota taqqɨlʼqolʼčimpat.</ta>
            <ta e="T216" id="Seg_4235" s="T212">qelʼitɨt oːtap taqqɨlʼe iːmpatɨ.</ta>
            <ta e="T223" id="Seg_4236" s="T216">nɨnɨ šittälʼ mɨŋa moqɨnä qənnoqij na qälɨkqiː.</ta>
            <ta e="T227" id="Seg_4237" s="T223">nortälʼilʼ čeːlʼit mompa tüntomɨt.</ta>
            <ta e="T232" id="Seg_4238" s="T227">teːmumpa kučče ɨkɨ qənnɨlɨt.</ta>
            <ta e="T236" id="Seg_4239" s="T232">šittelʼɨlʼ čeːlontɨ ɨnnä čeːlʼinna.</ta>
            <ta e="T241" id="Seg_4240" s="T236">na iːja nɨːlʼ qəːtɨtɨt ilʼčantɨnɨk.</ta>
            <ta e="T252" id="Seg_4241" s="T241">mɨta nʼannät tɨmtɨ tättɨ tot[n] oːtat nɨŋqɨ kušalʼ nʼarop eːnta, nače üːtälʼɨmɨt.</ta>
            <ta e="T256" id="Seg_4242" s="T252">käːlʼit tüːntotɨt oːtäp toqqɨkolamnäntotɨt.</ta>
            <ta e="T263" id="Seg_4243" s="T256">oːtan upkɨt kəlʼčomɨntɨ kuntɨ qärɨpɨlʼ kutar meːtomɨt.</ta>
            <ta e="T267" id="Seg_4244" s="T263">na iːja niːk qəttɨŋɨt.</ta>
            <ta e="T270" id="Seg_4245" s="T267">nɨnɨ niːk qəttɨŋɨtɨ.</ta>
            <ta e="T278" id="Seg_4246" s="T270">mʼäkke mɨta šentɨ molʼlʼet eːŋa, ilʼča mʼäkkʼe mittɨ.</ta>
            <ta e="T286" id="Seg_4247" s="T278">a tona tinte timnʼäntɨ nälʼa ilʼčatɨ natɨp kuːralʼtɨmpatɨ.</ta>
            <ta e="T289" id="Seg_4248" s="T286">na iːjanɨk …</ta>
            <ta e="T294" id="Seg_4249" s="T289">nʼännät tɨmtɨ maːčelʼ tiːči eːŋa.</ta>
            <ta e="T296" id="Seg_4250" s="T294">nɨmtɨ moːtɨŋnentak.</ta>
            <ta e="T300" id="Seg_4251" s="T296">šend̂ɨ moːt tatɨŋɨt mɨta.</ta>
            <ta e="T302" id="Seg_4252" s="T300">üːtälʼnotɨt taːlʼ.</ta>
            <ta e="T314" id="Seg_4253" s="T302">qapije nɨ nortälʼilʼ čʼeːlɨ tüːntoːtɨt pan iralʼmɨt təpɨtɨtkiːne, na kälɨ irat otap taːqalʼqo.</ta>
            <ta e="T319" id="Seg_4254" s="T314">tɨ[i]mnʼäsɨqät oːtäp nɨnɨ šittelʼ üːtälʼtoːtɨt.</ta>
            <ta e="T327" id="Seg_4255" s="T319">na oːtäp na toqäntoːtɨt qapij täm[p] aj toːqɨmpatɨ.</ta>
            <ta e="T330" id="Seg_4256" s="T327">mossä na mintotɨt.</ta>
            <ta e="T339" id="Seg_4257" s="T330">na maːčit tičit nʼännälʼ pälʼʼäqɨt nɨ na moːtɨŋtoːtɨt na iːmantɨsä.</ta>
            <ta e="T349" id="Seg_4258" s="T339">qapije tä[ə]ttalʼ kälʼɨk qaj mottɨ čaːŋka šentɨ moːtɨ qopɨlʼ moːttɨ.</ta>
            <ta e="T354" id="Seg_4259" s="T349">nɨnɨ šittelʼ šäqqoːtɨt na imantɨsä.</ta>
            <ta e="T358" id="Seg_4260" s="T354">təptɨlʼ qarɨt niːŋ äːsa.</ta>
            <ta e="T365" id="Seg_4261" s="T358">tintena imantɨnɨk mat mɨta nʼänna qumiːqäk koːnɨlʼek.</ta>
            <ta e="T370" id="Seg_4262" s="T365">qaj mɨta moːtɨnpotɨt qaj qaːtotɨt.</ta>
            <ta e="T372" id="Seg_4263" s="T370">nʼännä lʼakalʼtelʼča.</ta>
            <ta e="T379" id="Seg_4264" s="T372">noːkɨr qoptɨp soːrɨlʼa qapija nossarɨlʼ otäp qaltɨmɨntɨt.</ta>
            <ta e="T382" id="Seg_4265" s="T379">ija nʼanna qənneja.</ta>
            <ta e="T385" id="Seg_4266" s="T382">iːmatɨ ontɨ qaːlʼa.</ta>
            <ta e="T389" id="Seg_4267" s="T385">na omta na omta.</ta>
            <ta e="T396" id="Seg_4268" s="T389">ukkɨr tät čontoqɨt čeːlʼɨtɨ üːtontɨ na kənna.</ta>
            <ta e="T407" id="Seg_4269" s="T396">na iːja lʼamɨ kəːŋa, nʼännät ukkɨr ta[ət] čontoqɨt qup najap tatɨntotɨt.</ta>
            <ta e="T411" id="Seg_4270" s="T407">taqqɨnɨ pan iralʼmɨt tatɨntoːtɨt.</ta>
            <ta e="T416" id="Seg_4271" s="T411">köt qumtɨ qapija otäp toqɨsotɨlʼ.</ta>
            <ta e="T421" id="Seg_4272" s="T416">pan ira našak qəːtɨsä eːŋa.</ta>
            <ta e="T426" id="Seg_4273" s="T421">nɨn šittalʼ na ija tülʼčikunä.</ta>
            <ta e="T428" id="Seg_4274" s="T426">imatä üŋkɨlʼtimpatɨ.</ta>
            <ta e="T436" id="Seg_4275" s="T428">montɨ čap tüːlʼča pan iralʼmɨt qaj montɨ tüŋotɨt.</ta>
            <ta e="T442" id="Seg_4276" s="T436">qaГlʼilʼ mɨtɨ nɨk tottelʼnotɨ[t], poqɨt eːtɨ ‎mantoqɨt.</ta>
            <ta e="T444" id="Seg_4277" s="T442">mottɨ šeːrna.</ta>
            <ta e="T452" id="Seg_4278" s="T444">moːta not nɨŋqɨ[ä]lʼa motanot kʼeː topɨmtɨ tuːpalkɨtɨ.</ta>
            <ta e="T455" id="Seg_4279" s="T452">peːmɨmtɨ na tuːpalʼnɨt.</ta>
            <ta e="T461" id="Seg_4280" s="T455">niː qəttɨŋɨtɨ pan iranɨk nʼännä qoralʼtɨlʼe.</ta>
            <ta e="T467" id="Seg_4281" s="T461">oglɨ miːtolʼlʼa kolʼtɨt qəqɨt oːlʼa.</ta>
            <ta e="T474" id="Seg_4282" s="T467">taːlʼ na nʼänna paqtɨmpa na kupaksä täqɨlʼemɨntɨtɨ.</ta>
            <ta e="T480" id="Seg_4283" s="T474">pan iralʼ mɨtɨp muːntɨkə lʼäpek qättɨmp̂at.</ta>
            <ta e="T486" id="Seg_4284" s="T480">ommɨtɨ qumpa pan ira ilʼilʼe qaːlɨmpa.</ta>
            <ta e="T489" id="Seg_4285" s="T486">šittäqij ilʼi[ɨ]lʼlʼe qaːlɨmɨnta.</ta>
            <ta e="T491" id="Seg_4286" s="T489">ponä qəlʼlʼäitɨ.</ta>
            <ta e="T497" id="Seg_4287" s="T491">na iːja ukkur porɨ qupaksä täqɨlʼ[lʼ]empatɨ.</ta>
            <ta e="T503" id="Seg_4288" s="T497">muntɨk porq[q]äntɨ šʼunʼöntɨ [pümmɨntɨ šʼu] nʼölʼqɨmot poːtät.</ta>
            <ta e="T509" id="Seg_4289" s="T503">na qupɨlʼmɨt nɨnɨ šitälʼ poːnä kälʼlʼeitɨ.</ta>
            <ta e="T516" id="Seg_4290" s="T509">eːtɨmannontɨ tintena qaglɨ tüːr tolʼčitɨ na soːrelɨmpa.</ta>
            <ta e="T522" id="Seg_4291" s="T516">pan irap aj ponä itɨmpat [čatɨmpat].</ta>
            <ta e="T526" id="Seg_4292" s="T522">qapija muntɨk pona ittɨmpat.</ta>
            <ta e="T532" id="Seg_4293" s="T526">ilʼlʼa omta malʼčalʼtoqtɨ toː iːlʼä.</ta>
            <ta e="T536" id="Seg_4294" s="T532">amɨrlʼe ilʼlʼä omɨnta šäːkka.</ta>
            <ta e="T541" id="Seg_4295" s="T536">na kumitɨ poqɨt na ippelʼɨmpotɨt.</ta>
            <ta e="T543" id="Seg_4296" s="T541">kuttar sarelɨmpa.</ta>
            <ta e="T545" id="Seg_4297" s="T543">nilʼčik šäqqa.</ta>
            <ta e="T549" id="Seg_4298" s="T545">toptɨlʼ qarɨt ɨnnä väša.</ta>
            <ta e="T560" id="Seg_4299" s="T549">pan ira nilʼčik tap wändɨlʼtoktɨ totɨk tankiːčimpa kʼekkɨsä sailʼa[я]tɨ qətɨmpa.</ta>
            <ta e="T565" id="Seg_4300" s="T560">toːna qäːlɨkqɨtɨ ešo somalʼoqɨ epɨmpa.</ta>
            <ta e="T572" id="Seg_4301" s="T565">nilʼ qəːtɨŋɨtɨ na ija mɨta na qälʼitɨt kiːnä.</ta>
            <ta e="T577" id="Seg_4302" s="T572">moqonä qəntat talʼtälʼlʼ[lʼlʼ]e[ä] untɨt qaglɨntɨsä.</ta>
            <ta e="T583" id="Seg_4303" s="T577">nɨnɨ šittälʼ toːnam mäqqij moːttɨ šerɨšpotɨt.</ta>
            <ta e="T587" id="Seg_4304" s="T583">qaj aːmɨrpotɨt, qaj aš.</ta>
            <ta e="T600" id="Seg_4305" s="T587">nɨnɨ tintena saralʼpɨtɨlʼ qaglɨmtɨt nɨnɨ moqonä qəːčelʼlʼe na qumimtɨt quːpɨlʼ qumimtɨ niːk qəntɨŋɨt.</ta>
            <ta e="T607" id="Seg_4306" s="T600">taqqɨ moqonä pan iramtɨ aj kočilʼe qəntɨtɨ.</ta>
            <ta e="T611" id="Seg_4307" s="T607">qaj qɨka qəjitɨ qəntɨt.</ta>
            <ta e="T613" id="Seg_4308" s="T611">qənnatɨt tina.</ta>
            <ta e="T619" id="Seg_4309" s="T613">qumat somak qənnotɨt, imantɨnɨ ni qəːtɨŋɨt.</ta>
            <ta e="T626" id="Seg_4310" s="T619">molʼlʼe totilʼe[ä]tɨ, me nʼänna qəlʼlʼe qumɨtɨtkiːne.</ta>
            <ta e="T632" id="Seg_4311" s="T626">aš tänimɨmpatɨ, ilʼčantɨlʼ mɨt qos qaj.</ta>
            <ta e="T635" id="Seg_4312" s="T632">nenna qənna tintena.</ta>
            <ta e="T638" id="Seg_4313" s="T635">onta aša qoŋɨmpa.</ta>
            <ta e="T642" id="Seg_4314" s="T638">tona imatɨ na qətälʼmɨntɨtɨ.</ta>
            <ta e="T645" id="Seg_4315" s="T642">əsɨntɨlʼ ilʼčantɨlʼ mɨqätkiːne.</ta>
            <ta e="T649" id="Seg_4316" s="T645">iralʼ mɨt moqonä qəssotɨt.</ta>
            <ta e="T658" id="Seg_4317" s="T649">a nɨnä šittelʼ pijaka pija ni qaj aš tänimotɨt.</ta>
            <ta e="T660" id="Seg_4318" s="T658">ta qənna.</ta>
            <ta e="T664" id="Seg_4319" s="T660">täpɨt ilʼčantɨ moːttɨ šernoqi.</ta>
            <ta e="T669" id="Seg_4320" s="T664">qaqlʼɨlʼ toqtɨ kurɨlʼe nilʼčik tottotɨt.</ta>
            <ta e="T674" id="Seg_4321" s="T669">tona qənmɨntotɨt, moqɨnä na qənmɨntotɨt.</ta>
            <ta e="T682" id="Seg_4322" s="T674">pan irä takkɨt kekkɨsa motqɨntɨ tulʼišpa ilʼlʼä qumpa.</ta>
            <ta e="T686" id="Seg_4323" s="T682">tona šitɨ qumoqi ilʼlʼimpotɨ[qi]t.</ta>
            <ta e="T690" id="Seg_4324" s="T686">pan ira nimtɨ eːŋa.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_4325" s="T0">Ukkɨr ijatɨ ɛppɨmmɨnta, neːkɨtɨlʼ pɛlɨkɨtɨlʼ. </ta>
            <ta e="T9" id="Seg_4326" s="T5">Na tɛntɨlʼ, aše čʼaptä. </ta>
            <ta e="T12" id="Seg_4327" s="T9">İra ilɨmpa, quːtalʼpa. </ta>
            <ta e="T19" id="Seg_4328" s="T12">Na šolʼqumɨlʼ ira tapplʼälʼ qolʼtit pɛlʼaqqɨt ilɨmpa. </ta>
            <ta e="T25" id="Seg_4329" s="T19">Poːkɨtɨlʼ tətɨt toːčʼiqɨt iːlɨmpa tintena qüːtälpa. </ta>
            <ta e="T29" id="Seg_4330" s="T25">Ukkɨr ijatɨ pɛlɨkɔːtɨlʼ neːkɨtɨlʼ. </ta>
            <ta e="T35" id="Seg_4331" s="T29">Ni nälʼatɨ čʼäŋka ni qajtɨ čʼäŋka. </ta>
            <ta e="T40" id="Seg_4332" s="T35">Nɨnɨ əsɨtɨ qüːtäla na ijalʼa. </ta>
            <ta e="T47" id="Seg_4333" s="T40">Na ija qapıja nɨmtɨ ila, əsɨntɨsä əmɨntɨsä. </ta>
            <ta e="T52" id="Seg_4334" s="T47">Tontɨ šitɨ qäːlɨlʼ timnʼäsɨt ɛppɨntɔːtɨt. </ta>
            <ta e="T54" id="Seg_4335" s="T52">Qäːlʼ irä puːčʼčʼɛnta. </ta>
            <ta e="T58" id="Seg_4336" s="T54">Tɨntäna ira ıllä quŋa. </ta>
            <ta e="T62" id="Seg_4337" s="T58">Tintena qäːlʼ ira kučʼčʼä qattɛnta. </ta>
            <ta e="T65" id="Seg_4338" s="T62">Toː timnʼäntɨnɨ puːŋa. </ta>
            <ta e="T69" id="Seg_4339" s="T65">“İrra qunta, ıllä meːŋɔːtɨt”. </ta>
            <ta e="T75" id="Seg_4340" s="T69">Šittälʼ imaqotatɨ qüːtälʼlʼä, aj ıllä quŋa. </ta>
            <ta e="T81" id="Seg_4341" s="T75">İralʼ mɨqä ijatɨt nejkɔːlɨ pɛlɨkɔːlɨ qala. </ta>
            <ta e="T83" id="Seg_4342" s="T81">Kučʼčʼä qattɛnta. </ta>
            <ta e="T88" id="Seg_4343" s="T83">Na qäːlʼi ira šittalʼ tɔː puːtɨmpatɨ. </ta>
            <ta e="T93" id="Seg_4344" s="T88">Märqɨ ijatɨ nılʼčʼilʼ ijatɨ ɛppɨmmɨnta. </ta>
            <ta e="T97" id="Seg_4345" s="T93">Puːtɨŋɨtɨ ɔːtätɨ aj čʼäːŋka. </ta>
            <ta e="T100" id="Seg_4346" s="T97">Ɔːtäkɔːlɨk topɨsä ilɨmpɔːtɨt. </ta>
            <ta e="T102" id="Seg_4347" s="T100">Nɨmtɨ ilɨmpɔːtɨt. </ta>
            <ta e="T107" id="Seg_4348" s="T102">İlɨmpɔːtɨt, tɔː puːlʼä, qälʼɨ-irat mɔːttɨ. </ta>
            <ta e="T110" id="Seg_4349" s="T107">Qälʼɨ irra ijatɨ čʼäŋɨmpa. </ta>
            <ta e="T112" id="Seg_4350" s="T110">İjakɔːtɨlʼ imatɨ. </ta>
            <ta e="T118" id="Seg_4351" s="T112">Toːna čʼontolʼ timnʼatɨ ukkɨr nälʼätɨ pɛlɨkɨtɨlʼ. </ta>
            <ta e="T123" id="Seg_4352" s="T118">Takkɨnɨ nʼänna šitɨ qälɨk tüːŋa. </ta>
            <ta e="T128" id="Seg_4353" s="T123">Poːkɨtɨlʼ təttoqɨnɨ na qälʼi ira najɛnta. </ta>
            <ta e="T133" id="Seg_4354" s="T128">Kuššalʼ tamtɨrtɨ ɔːtäp muntɨk taqqɨšpatɨ. </ta>
            <ta e="T135" id="Seg_4355" s="T133">Nılʼčʼik tüːŋa. </ta>
            <ta e="T142" id="Seg_4356" s="T135">Na tɨmnʼasɨka ilɔːtɨt na iralʼ tɨmnʼäsɨka šittɨ. </ta>
            <ta e="T145" id="Seg_4357" s="T142">Tɛːttɨ toːt ɔːtäqitɨ. </ta>
            <ta e="T150" id="Seg_4358" s="T145">Na iran ɔːtäp iqɨntoːqo tünta. </ta>
            <ta e="T152" id="Seg_4359" s="T150">Qumintɨsä tüːnta. </ta>
            <ta e="T157" id="Seg_4360" s="T152">Paːn iːra, nımtɨ aj najta. </ta>
            <ta e="T163" id="Seg_4361" s="T157">Nɨnɨ tüːlä šäqqɔːtɨt na täpɨtɨt mɔːtqɨt. </ta>
            <ta e="T167" id="Seg_4362" s="T163">Na ija aš tɛnɨmɔːtɨt. </ta>
            <ta e="T171" id="Seg_4363" s="T167">“İːja kuttar taːpɨnɨk alʼčʼimpa? </ta>
            <ta e="T175" id="Seg_4364" s="T171">Täp qaj muːtɨronak ila”. </ta>
            <ta e="T181" id="Seg_4365" s="T175">Qup kuttar tɛnimɛntätɨ alʼpet ilʼäntɨlʼ qup. </ta>
            <ta e="T182" id="Seg_4366" s="T181">Šäqqɔːtɨt. </ta>
            <ta e="T192" id="Seg_4367" s="T182">Mompa nɔːrtälʼilʼ čʼeːlʼɨt toːqqɨt mompa Pan ira köt qupsä tüːŋa. </ta>
            <ta e="T195" id="Seg_4368" s="T192">Qapija ɔːtäp tɔːqqɨqontoːqo. </ta>
            <ta e="T200" id="Seg_4369" s="T195">Na tintena asästit ɔːtäp tɔːqqɨqo. </ta>
            <ta e="T209" id="Seg_4370" s="T200">Kun mɔːntɨlʼ mɔːttɨ šentɨ kun mɔːntɨlʼ qajtɨ mun taqqalʼtɛntɨt. </ta>
            <ta e="T212" id="Seg_4371" s="T209">Ukoːn nɔːta taqqɨlʼkolʼčʼimpat. </ta>
            <ta e="T216" id="Seg_4372" s="T212">Qälʼitɨt ɔːtap taqqɨllä iːmpatɨ. </ta>
            <ta e="T223" id="Seg_4373" s="T216">Nɨnɨ šittälʼ mɨŋa moqɨnä qənnɔːqij na qälɨkqıː. </ta>
            <ta e="T227" id="Seg_4374" s="T223">Nɔːrtälʼilʼ čʼeːlʼit mompa tüntɔːmɨt. </ta>
            <ta e="T232" id="Seg_4375" s="T227">Tɛː mumpa kučʼčʼä ɨkɨ qənnɨlɨt. </ta>
            <ta e="T236" id="Seg_4376" s="T232">Šittälʼɨlʼ čʼeːlontɨ ınnä čʼeːlʼinna. </ta>
            <ta e="T241" id="Seg_4377" s="T236">Na iːja nılʼ kətɨtɨt ilʼčʼantɨnɨk. </ta>
            <ta e="T252" id="Seg_4378" s="T241">Mɨta nʼannät tɨmtɨ tɛːttɨ toːt ɔːtat nɨŋqɨkušalʼ nʼarop ɛːnta, načʼčʼä üːtälʼɨmɨt. </ta>
            <ta e="T256" id="Seg_4379" s="T252">Qäːlʼit tüːntɔːtɨt ɔːtäp tɔːqqɨqolamnɛntɔːtɨt. </ta>
            <ta e="T263" id="Seg_4380" s="T256">Ɔːtan upkɨt kəlʼčʼomɨntɨ kuntɨ kärɨpɨlʼ kutar meːtɔːmɨt. </ta>
            <ta e="T267" id="Seg_4381" s="T263">Na iːja nık kəttɨŋɨt. </ta>
            <ta e="T270" id="Seg_4382" s="T267">Nɨnɨ nık kəttɨŋɨtɨ. </ta>
            <ta e="T278" id="Seg_4383" s="T270">Mäkkä mɨta šentɨ mɔːllʼet ɛːŋa, ilʼčʼa, mäkkä mitɨ. </ta>
            <ta e="T286" id="Seg_4384" s="T278">A toːna tınte timnʼäntɨ nälʼa ilʼčʼatɨ natɨp kuralʼtɨmpatɨ. </ta>
            <ta e="T289" id="Seg_4385" s="T286">Na iːjanɨk… </ta>
            <ta e="T294" id="Seg_4386" s="T289">Nʼännät tɨmtɨ maːčʼelʼ tiːčʼi ɛːŋa. </ta>
            <ta e="T296" id="Seg_4387" s="T294">Nɨmtɨ mɔːtɨŋnɛntak. </ta>
            <ta e="T300" id="Seg_4388" s="T296">Šentɨ mɔːt tatɨŋɨt mɨta. </ta>
            <ta e="T302" id="Seg_4389" s="T300">Üːtälnɔːtɨt taːlʼ. </ta>
            <ta e="T314" id="Seg_4390" s="T302">Qapıje nɨ nɔːrtälʼilʼ čʼeːlɨ tüːntɔːtɨt Pan iralʼmɨt təpɨtɨtkiːne na qälɨ-irat ɔːtap taqqalʼqo. </ta>
            <ta e="T319" id="Seg_4391" s="T314">Timnʼäsɨqät ɔːtäp nɨnɨ šittelʼ üːtältɔːtɨt. </ta>
            <ta e="T327" id="Seg_4392" s="T319">Na ɔːtäp na tɔːqqäntɔːtɨt qapıj täm aj tɔːqqɨmpatɨ. </ta>
            <ta e="T330" id="Seg_4393" s="T327">Mɔːssä na mintɔːtɨt. </ta>
            <ta e="T339" id="Seg_4394" s="T330">Na mačʼit tičʼit nʼännälʼ pɛlʼäqqɨt nɨna mɔːtɨŋtɔːtɨt na imantɨsä. </ta>
            <ta e="T349" id="Seg_4395" s="T339">Qapıje təttalʼ qälɨk qaj mɔːttɨ čʼaːŋka šentɨ mɔːttɨ qopɨlʼ mɔːttɨ. </ta>
            <ta e="T354" id="Seg_4396" s="T349">Nɨnɨ šittälʼ šäqqɔːtɨt na imantɨsä. </ta>
            <ta e="T358" id="Seg_4397" s="T354">Təptɨlʼ qarɨt nıŋ ɛsa. </ta>
            <ta e="T365" id="Seg_4398" s="T358">Tıntena imantɨnɨk: mat mɨta nʼänna qumiːqäk koːnɨlʼäk. </ta>
            <ta e="T370" id="Seg_4399" s="T365">Qaj mɨta mɔːtɨnpɔːtɨt qaj qaːtɔːtɨt. </ta>
            <ta e="T372" id="Seg_4400" s="T370">Nʼännä laqaltelʼčʼa. </ta>
            <ta e="T379" id="Seg_4401" s="T372">Nɔːkɨr qoptɨp sɔːrɨlʼa qapıja nɔːssarɨlʼ ɔːtäp qaltɨmmɨntɨt. </ta>
            <ta e="T382" id="Seg_4402" s="T379">İja nʼanna qənnɛːja. </ta>
            <ta e="T385" id="Seg_4403" s="T382">İːmatɨ ontɨ qala. </ta>
            <ta e="T389" id="Seg_4404" s="T385">Na omta, na omta. </ta>
            <ta e="T396" id="Seg_4405" s="T389">Ukkɨr tät čʼontoːqɨt čʼeːlʼɨtɨ üːtoːntɨ na qənna. </ta>
            <ta e="T407" id="Seg_4406" s="T396">Na iːja lʼamɨk kəːŋa, nʼännät ukkɨr tot čʼontoːqɨt qup najap tatɨntɔːtɨt. </ta>
            <ta e="T411" id="Seg_4407" s="T407">Takkɨnɨ Pan iralʼmɨt tatɨntɔːtɨt. </ta>
            <ta e="T416" id="Seg_4408" s="T411">Köt qumtɨ qapıja ɔːtäp tɔːqqɨsɔːtɨlʼ. </ta>
            <ta e="T421" id="Seg_4409" s="T416">Pan ira naššak qəːtɨsä ɛːŋa. </ta>
            <ta e="T426" id="Seg_4410" s="T421">Nɨn šittalʼ na ija tülʼčʼikunä. </ta>
            <ta e="T428" id="Seg_4411" s="T426">İmatä üŋkɨltimpatɨ. </ta>
            <ta e="T436" id="Seg_4412" s="T428">Montɨ čʼap tüːlʼčʼa Pan iralʼmɨt qaj montɨ tüŋɔːtɨt. </ta>
            <ta e="T442" id="Seg_4413" s="T436">Qaqlʼilʼ mɨtɨ nɨk tottälnɔːtɨt, poːqɨt eːtɨmantoːqɨt. </ta>
            <ta e="T444" id="Seg_4414" s="T442">Mɔːttɨ šeːrna. </ta>
            <ta e="T452" id="Seg_4415" s="T444">Mɔːtan ɔːt nɨŋkɨlʼa mɔːtan ɔːt kʼeː topɨmtɨ tupalkɨtɨ. </ta>
            <ta e="T455" id="Seg_4416" s="T452">Peːmɨmtɨ na tupalnɨt. </ta>
            <ta e="T461" id="Seg_4417" s="T455">Nı kəttɨŋɨtɨ Pan iranɨk nʼännä koraltɨlä. </ta>
            <ta e="T467" id="Seg_4418" s="T461">Ɔːklɨ miːt olʼlʼa qolʼtɨt qəqɨt oːlʼa. </ta>
            <ta e="T474" id="Seg_4419" s="T467">Taːlʼ na nʼänna paqtɨmpa na kupaksä täqalemmɨntɨtɨ. </ta>
            <ta e="T480" id="Seg_4420" s="T474">Pan iralʼ mɨtɨp muːntɨkə lʼäpek qättɨmpat. </ta>
            <ta e="T486" id="Seg_4421" s="T480">Ɔːmmɨtɨ qumpa Pan ira ilʼilä qalɨmpa. </ta>
            <ta e="T489" id="Seg_4422" s="T486">Šittäqij ilɨlä qaːlɨmmɨnta. </ta>
            <ta e="T491" id="Seg_4423" s="T489">Ponä qəlläitɨ. </ta>
            <ta e="T497" id="Seg_4424" s="T491">Na iːja ukkur pɔːrɨ kupaksä täqalempatɨ. </ta>
            <ta e="T503" id="Seg_4425" s="T497">Muntɨk porqäntɨ šunʼnʼontɨ (pümmɨntɨ šʼu) nʼölʼqɨmɔːtpotät. </ta>
            <ta e="T509" id="Seg_4426" s="T503">Na qupɨlʼmɨt nɨnɨ šitälʼ ponä kälʼlʼeitɨ. </ta>
            <ta e="T516" id="Seg_4427" s="T509">Eːtɨmannoːntɨ tintena qaqlɨ tüːr tolʼčʼitɨ na sɔːrälɨmpa. </ta>
            <ta e="T522" id="Seg_4428" s="T516">Pan irap aj ponä itɨmpat (čʼatɨmpat). </ta>
            <ta e="T526" id="Seg_4429" s="T522">Qapija muntɨk pona iːtɨmpat. </ta>
            <ta e="T532" id="Seg_4430" s="T526">Illa omta malʼčʼalʼ tɔːqtɨ toː iːlʼä. </ta>
            <ta e="T536" id="Seg_4431" s="T532">Amɨrlʼä ıllä ommɨnta šäqqa. </ta>
            <ta e="T541" id="Seg_4432" s="T536">Na qumiːtɨ poːqɨt na ippälɨmpɔːtɨt. </ta>
            <ta e="T543" id="Seg_4433" s="T541">Kuttar sarälɨmpa. </ta>
            <ta e="T545" id="Seg_4434" s="T543">Nılʼčʼik šäqqa. </ta>
            <ta e="T549" id="Seg_4435" s="T545">Tɔːptɨlʼ qarɨt ɨnnä wäša. </ta>
            <ta e="T560" id="Seg_4436" s="T549">Pan ira nılʼčʼik tap wäntɨlʼ tɔːktɨ tɔːtɨk taŋkıːčʼimpa kekkɨsä sailʼatɨ qətɨmpa. </ta>
            <ta e="T565" id="Seg_4437" s="T560">Toːna qälɨkqıtɨ ešo somalʼoqı ɛppɨmpa. </ta>
            <ta e="T572" id="Seg_4438" s="T565">Nılʼ kətɨŋɨtɨ na ija mɨta na qälʼitɨtkiːnä. </ta>
            <ta e="T577" id="Seg_4439" s="T572">Moqonä qəntat talʼtälʼlʼä untɨt qaqlɨntɨsä. </ta>
            <ta e="T583" id="Seg_4440" s="T577">Nɨnɨ šittälʼ toːna meːqıj mɔːttɨ šeːrɨšpɔːtɨt. </ta>
            <ta e="T587" id="Seg_4441" s="T583">Qaj amɨrpɔːtɨt, qaj aš. </ta>
            <ta e="T600" id="Seg_4442" s="T587">Nɨnɨ tintena saralpɨtɨlʼ qaqlɨmtɨt nɨnɨ moqonä qəːčʼelʼlʼä na qumiːmtɨt qupɨlʼ qumiːmtɨ nık qəntɨŋɨt. </ta>
            <ta e="T607" id="Seg_4443" s="T600">Takkɨ moqonä Pan iramtɨ aj kočʼilʼä qəntɨtɨ. </ta>
            <ta e="T611" id="Seg_4444" s="T607">Qaj kɨka qəjitɨ qəntɨt. </ta>
            <ta e="T613" id="Seg_4445" s="T611">Qənnatɨt tına. </ta>
            <ta e="T619" id="Seg_4446" s="T613">Qumat somak qənnɔːtɨt, imantɨnɨ nık kətɨŋɨt. </ta>
            <ta e="T626" id="Seg_4447" s="T619">“Mɔːlle toː tılätɨ, me nʼänna qəlʼlʼä qumɨtɨtkiːne”. </ta>
            <ta e="T632" id="Seg_4448" s="T626">Aš tɛnimɨmpatɨ, ilʼčʼantɨlʼ mɨt qos qaj. </ta>
            <ta e="T635" id="Seg_4449" s="T632">Nʼenna qənna tintena. </ta>
            <ta e="T638" id="Seg_4450" s="T635">Onta aša koŋɨmpa. </ta>
            <ta e="T642" id="Seg_4451" s="T638">Toːna imatɨ na kətälmɨntɨtɨ. </ta>
            <ta e="T645" id="Seg_4452" s="T642">Əsɨntɨlʼ ilʼčʼantɨlʼ mɨqätkiːne. </ta>
            <ta e="T649" id="Seg_4453" s="T645">İralʼ mɨt moqonä qəssɔːtɨt. </ta>
            <ta e="T658" id="Seg_4454" s="T649">A nɨnä šittelʼ pijaka pija ni qaj aš tɛnimɔːtɨt. </ta>
            <ta e="T660" id="Seg_4455" s="T658">Ta qənna. </ta>
            <ta e="T664" id="Seg_4456" s="T660">Täpɨt ilʼčʼantɨ mɔːttɨ šernɔːqı. </ta>
            <ta e="T669" id="Seg_4457" s="T664">Qaqlɨlʼ tɔːktɨ kurɨlʼä nılʼčʼik tottɔːtɨt. </ta>
            <ta e="T674" id="Seg_4458" s="T669">Toːna qənmɨntɔːtɨt, moqɨnä na qənmɨntɔːtɨt. </ta>
            <ta e="T682" id="Seg_4459" s="T674">Pan irä takkɨt kekkɨsa mɔːtqɨntɨ tulʼišpa ıllä qumpa. </ta>
            <ta e="T686" id="Seg_4460" s="T682">Toːna šittɨ qumoːqı ilɨmpɔːqı. </ta>
            <ta e="T690" id="Seg_4461" s="T686">Pan ira nimtɨ ɛːŋa. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_4462" s="T0">ukkɨr</ta>
            <ta e="T2" id="Seg_4463" s="T1">ija-tɨ</ta>
            <ta e="T3" id="Seg_4464" s="T2">ɛ-ppɨ-mmɨ-nta</ta>
            <ta e="T4" id="Seg_4465" s="T3">neː-kɨtɨ-lʼ</ta>
            <ta e="T5" id="Seg_4466" s="T4">pɛlɨ-kɨtɨ-lʼ</ta>
            <ta e="T6" id="Seg_4467" s="T5">na</ta>
            <ta e="T7" id="Seg_4468" s="T6">tɛntɨlʼ</ta>
            <ta e="T8" id="Seg_4469" s="T7">aše</ta>
            <ta e="T9" id="Seg_4470" s="T8">čʼaptä</ta>
            <ta e="T10" id="Seg_4471" s="T9">ira</ta>
            <ta e="T11" id="Seg_4472" s="T10">ilɨ-mpa</ta>
            <ta e="T12" id="Seg_4473" s="T11">quːta-lʼ-pa</ta>
            <ta e="T13" id="Seg_4474" s="T12">na</ta>
            <ta e="T14" id="Seg_4475" s="T13">šolʼqum-ɨ-lʼ</ta>
            <ta e="T15" id="Seg_4476" s="T14">ira</ta>
            <ta e="T16" id="Seg_4477" s="T15">tapplʼälʼ</ta>
            <ta e="T17" id="Seg_4478" s="T16">qolʼti-t</ta>
            <ta e="T18" id="Seg_4479" s="T17">pɛlʼaq-qɨt</ta>
            <ta e="T19" id="Seg_4480" s="T18">ilɨ-mpa</ta>
            <ta e="T20" id="Seg_4481" s="T19">poː-kɨtɨ-lʼ</ta>
            <ta e="T21" id="Seg_4482" s="T20">tətɨ-t</ta>
            <ta e="T22" id="Seg_4483" s="T21">toːčʼi-qɨt</ta>
            <ta e="T23" id="Seg_4484" s="T22">iːlɨ-mpa</ta>
            <ta e="T24" id="Seg_4485" s="T23">tintena</ta>
            <ta e="T25" id="Seg_4486" s="T24">qüːtä-l-pa</ta>
            <ta e="T26" id="Seg_4487" s="T25">ukkɨr</ta>
            <ta e="T27" id="Seg_4488" s="T26">ija-tɨ</ta>
            <ta e="T28" id="Seg_4489" s="T27">pɛlɨ-kɔːtɨ-lʼ</ta>
            <ta e="T29" id="Seg_4490" s="T28">neː-kɨtɨ-lʼ</ta>
            <ta e="T30" id="Seg_4491" s="T29">ni</ta>
            <ta e="T31" id="Seg_4492" s="T30">nälʼa-tɨ</ta>
            <ta e="T32" id="Seg_4493" s="T31">čʼäŋka</ta>
            <ta e="T33" id="Seg_4494" s="T32">ni</ta>
            <ta e="T34" id="Seg_4495" s="T33">qaj-tɨ</ta>
            <ta e="T35" id="Seg_4496" s="T34">čʼäŋka</ta>
            <ta e="T36" id="Seg_4497" s="T35">nɨnɨ</ta>
            <ta e="T37" id="Seg_4498" s="T36">əsɨ-tɨ</ta>
            <ta e="T38" id="Seg_4499" s="T37">qüːt-äla</ta>
            <ta e="T39" id="Seg_4500" s="T38">na</ta>
            <ta e="T40" id="Seg_4501" s="T39">ija-lʼa</ta>
            <ta e="T41" id="Seg_4502" s="T40">na</ta>
            <ta e="T42" id="Seg_4503" s="T41">ija</ta>
            <ta e="T43" id="Seg_4504" s="T42">qapı-ja</ta>
            <ta e="T44" id="Seg_4505" s="T43">nɨmtɨ</ta>
            <ta e="T45" id="Seg_4506" s="T44">ila</ta>
            <ta e="T46" id="Seg_4507" s="T45">əsɨ-ntɨ-sä</ta>
            <ta e="T47" id="Seg_4508" s="T46">əmɨ-ntɨ-sä</ta>
            <ta e="T48" id="Seg_4509" s="T47">to-ntɨ</ta>
            <ta e="T49" id="Seg_4510" s="T48">šitɨ</ta>
            <ta e="T50" id="Seg_4511" s="T49">qəːlɨ-lʼ</ta>
            <ta e="T51" id="Seg_4512" s="T50">timnʼä-sɨ-t</ta>
            <ta e="T52" id="Seg_4513" s="T51">ɛ-ppɨ-ntɔː-tɨt</ta>
            <ta e="T693" id="Seg_4514" s="T52">qäːlʼ</ta>
            <ta e="T53" id="Seg_4515" s="T693">irä</ta>
            <ta e="T54" id="Seg_4516" s="T53">puː-čʼ-čʼɛnta</ta>
            <ta e="T55" id="Seg_4517" s="T54">tɨntäna</ta>
            <ta e="T56" id="Seg_4518" s="T55">ira</ta>
            <ta e="T57" id="Seg_4519" s="T56">ıllä</ta>
            <ta e="T58" id="Seg_4520" s="T57">qu-ŋa</ta>
            <ta e="T59" id="Seg_4521" s="T58">tintena</ta>
            <ta e="T694" id="Seg_4522" s="T59">qälʼ</ta>
            <ta e="T60" id="Seg_4523" s="T694">ira</ta>
            <ta e="T61" id="Seg_4524" s="T60">kučʼčʼä</ta>
            <ta e="T62" id="Seg_4525" s="T61">qatt-ɛnta</ta>
            <ta e="T63" id="Seg_4526" s="T62">toː</ta>
            <ta e="T64" id="Seg_4527" s="T63">timnʼä-ntɨ-nɨ</ta>
            <ta e="T65" id="Seg_4528" s="T64">puː-ŋa</ta>
            <ta e="T66" id="Seg_4529" s="T65">i̇rra</ta>
            <ta e="T67" id="Seg_4530" s="T66">qu-nta</ta>
            <ta e="T68" id="Seg_4531" s="T67">ıllä</ta>
            <ta e="T69" id="Seg_4532" s="T68">meː-ŋɔːtɨt</ta>
            <ta e="T70" id="Seg_4533" s="T69">šittälʼ</ta>
            <ta e="T71" id="Seg_4534" s="T70">imaqota-tɨ</ta>
            <ta e="T72" id="Seg_4535" s="T71">qüːt-älʼ-lʼä</ta>
            <ta e="T73" id="Seg_4536" s="T72">aj</ta>
            <ta e="T74" id="Seg_4537" s="T73">ıllä</ta>
            <ta e="T75" id="Seg_4538" s="T74">qu-ŋa</ta>
            <ta e="T76" id="Seg_4539" s="T75">ira-lʼ</ta>
            <ta e="T77" id="Seg_4540" s="T76">mɨ-qä</ta>
            <ta e="T78" id="Seg_4541" s="T77">ija-tɨt</ta>
            <ta e="T79" id="Seg_4542" s="T78">nej-kɔːlɨ</ta>
            <ta e="T80" id="Seg_4543" s="T79">pɛlɨ-kɔːlɨ</ta>
            <ta e="T81" id="Seg_4544" s="T80">qala</ta>
            <ta e="T82" id="Seg_4545" s="T81">kučʼčʼä</ta>
            <ta e="T83" id="Seg_4546" s="T82">qatt-ɛnta</ta>
            <ta e="T84" id="Seg_4547" s="T83">na</ta>
            <ta e="T695" id="Seg_4548" s="T84">qäːlʼi</ta>
            <ta e="T85" id="Seg_4549" s="T695">ira</ta>
            <ta e="T86" id="Seg_4550" s="T85">šit-talʼ</ta>
            <ta e="T87" id="Seg_4551" s="T86">tɔː</ta>
            <ta e="T88" id="Seg_4552" s="T87">puː-tɨ-mpa-tɨ</ta>
            <ta e="T89" id="Seg_4553" s="T88">märqɨ</ta>
            <ta e="T90" id="Seg_4554" s="T89">ija-tɨ</ta>
            <ta e="T91" id="Seg_4555" s="T90">nılʼčʼi-lʼ</ta>
            <ta e="T92" id="Seg_4556" s="T91">ija-tɨ</ta>
            <ta e="T93" id="Seg_4557" s="T92">ɛ-ppɨ-mmɨ-nta</ta>
            <ta e="T94" id="Seg_4558" s="T93">puː-tɨ-ŋɨ-tɨ</ta>
            <ta e="T95" id="Seg_4559" s="T94">ɔːtä-tɨ</ta>
            <ta e="T96" id="Seg_4560" s="T95">aj</ta>
            <ta e="T97" id="Seg_4561" s="T96">čʼäːŋka</ta>
            <ta e="T98" id="Seg_4562" s="T97">ɔːtä-kɔːlɨ-k</ta>
            <ta e="T99" id="Seg_4563" s="T98">topɨ-sä</ta>
            <ta e="T100" id="Seg_4564" s="T99">ilɨ-mpɔː-tɨt</ta>
            <ta e="T101" id="Seg_4565" s="T100">nɨmtɨ</ta>
            <ta e="T102" id="Seg_4566" s="T101">ilɨ-mpɔː-tɨt</ta>
            <ta e="T103" id="Seg_4567" s="T102">ilɨ-mpɔː-tɨt</ta>
            <ta e="T104" id="Seg_4568" s="T103">tɔː</ta>
            <ta e="T105" id="Seg_4569" s="T104">puː-lʼä</ta>
            <ta e="T106" id="Seg_4570" s="T105">qälʼɨ_ira-t</ta>
            <ta e="T107" id="Seg_4571" s="T106">mɔːt-tɨ</ta>
            <ta e="T691" id="Seg_4572" s="T107">qälʼɨ</ta>
            <ta e="T108" id="Seg_4573" s="T691">irra</ta>
            <ta e="T109" id="Seg_4574" s="T108">ija-tɨ</ta>
            <ta e="T110" id="Seg_4575" s="T109">čʼäŋɨ-mpa</ta>
            <ta e="T111" id="Seg_4576" s="T110">ija-kɔːtɨ-lʼ</ta>
            <ta e="T112" id="Seg_4577" s="T111">ima-tɨ</ta>
            <ta e="T113" id="Seg_4578" s="T112">toːna</ta>
            <ta e="T114" id="Seg_4579" s="T113">čʼonto-lʼ</ta>
            <ta e="T115" id="Seg_4580" s="T114">timnʼa-tɨ</ta>
            <ta e="T116" id="Seg_4581" s="T115">ukkɨr</ta>
            <ta e="T117" id="Seg_4582" s="T116">nälʼä-tɨ</ta>
            <ta e="T118" id="Seg_4583" s="T117">pɛlɨ-kɨtɨ-lʼ</ta>
            <ta e="T119" id="Seg_4584" s="T118">takkɨ-nɨ</ta>
            <ta e="T120" id="Seg_4585" s="T119">nʼänna</ta>
            <ta e="T121" id="Seg_4586" s="T120">šitɨ</ta>
            <ta e="T122" id="Seg_4587" s="T121">qälɨk</ta>
            <ta e="T123" id="Seg_4588" s="T122">tüː-ŋa</ta>
            <ta e="T124" id="Seg_4589" s="T123">poː-kɨtɨ-lʼ</ta>
            <ta e="T125" id="Seg_4590" s="T124">tətto-qɨnɨ</ta>
            <ta e="T126" id="Seg_4591" s="T125">na</ta>
            <ta e="T692" id="Seg_4592" s="T126">qälʼi</ta>
            <ta e="T127" id="Seg_4593" s="T692">ira</ta>
            <ta e="T128" id="Seg_4594" s="T127">najɛnta</ta>
            <ta e="T129" id="Seg_4595" s="T128">kušša-lʼ</ta>
            <ta e="T130" id="Seg_4596" s="T129">tamtɨr-tɨ</ta>
            <ta e="T131" id="Seg_4597" s="T130">ɔːtä-p</ta>
            <ta e="T132" id="Seg_4598" s="T131">muntɨk</ta>
            <ta e="T133" id="Seg_4599" s="T132">taqqɨ-š-pa-tɨ</ta>
            <ta e="T134" id="Seg_4600" s="T133">nılʼčʼi-k</ta>
            <ta e="T135" id="Seg_4601" s="T134">tüː-ŋa</ta>
            <ta e="T136" id="Seg_4602" s="T135">na</ta>
            <ta e="T137" id="Seg_4603" s="T136">tɨmnʼa-sɨ-ka</ta>
            <ta e="T138" id="Seg_4604" s="T137">ilɔː-tɨt</ta>
            <ta e="T139" id="Seg_4605" s="T138">na</ta>
            <ta e="T140" id="Seg_4606" s="T139">ira-lʼ</ta>
            <ta e="T141" id="Seg_4607" s="T140">tɨmnʼä-sɨ-ka</ta>
            <ta e="T142" id="Seg_4608" s="T141">šittɨ</ta>
            <ta e="T143" id="Seg_4609" s="T142">tɛːttɨ</ta>
            <ta e="T144" id="Seg_4610" s="T143">toːt</ta>
            <ta e="T145" id="Seg_4611" s="T144">ɔːtä-qi-tɨ</ta>
            <ta e="T146" id="Seg_4612" s="T145">na</ta>
            <ta e="T147" id="Seg_4613" s="T146">ira-n</ta>
            <ta e="T148" id="Seg_4614" s="T147">ɔːtä-p</ta>
            <ta e="T149" id="Seg_4615" s="T148">i-qɨntoːqo</ta>
            <ta e="T150" id="Seg_4616" s="T149">tü-nta</ta>
            <ta e="T151" id="Seg_4617" s="T150">qum-i-ntɨ-sä</ta>
            <ta e="T152" id="Seg_4618" s="T151">tüː-nta</ta>
            <ta e="T153" id="Seg_4619" s="T152">Paːn</ta>
            <ta e="T154" id="Seg_4620" s="T153">iːra</ta>
            <ta e="T155" id="Seg_4621" s="T154">nım-tɨ</ta>
            <ta e="T156" id="Seg_4622" s="T155">aj</ta>
            <ta e="T157" id="Seg_4623" s="T156">najta</ta>
            <ta e="T158" id="Seg_4624" s="T157">nɨnɨ</ta>
            <ta e="T159" id="Seg_4625" s="T158">tüː-lä</ta>
            <ta e="T160" id="Seg_4626" s="T159">šäqqɔː-tɨt</ta>
            <ta e="T161" id="Seg_4627" s="T160">na</ta>
            <ta e="T162" id="Seg_4628" s="T161">täp-ɨ-t-ɨ-t</ta>
            <ta e="T163" id="Seg_4629" s="T162">mɔːt-qɨt</ta>
            <ta e="T164" id="Seg_4630" s="T163">na</ta>
            <ta e="T165" id="Seg_4631" s="T164">ija</ta>
            <ta e="T166" id="Seg_4632" s="T165">aš</ta>
            <ta e="T167" id="Seg_4633" s="T166">tɛnɨmɔː-tɨt</ta>
            <ta e="T168" id="Seg_4634" s="T167">iːja</ta>
            <ta e="T169" id="Seg_4635" s="T168">kuttar</ta>
            <ta e="T170" id="Seg_4636" s="T169">taːp-ɨ-nɨk</ta>
            <ta e="T171" id="Seg_4637" s="T170">alʼčʼi-mpa</ta>
            <ta e="T172" id="Seg_4638" s="T171">täp</ta>
            <ta e="T173" id="Seg_4639" s="T172">qaj</ta>
            <ta e="T174" id="Seg_4640" s="T173">muːtɨrona-k</ta>
            <ta e="T175" id="Seg_4641" s="T174">ila</ta>
            <ta e="T176" id="Seg_4642" s="T175">qup</ta>
            <ta e="T177" id="Seg_4643" s="T176">kuttar</ta>
            <ta e="T178" id="Seg_4644" s="T177">tɛnim-ɛntä-tɨ</ta>
            <ta e="T179" id="Seg_4645" s="T178">alʼpet</ta>
            <ta e="T180" id="Seg_4646" s="T179">ilʼä-ntɨlʼ</ta>
            <ta e="T181" id="Seg_4647" s="T180">qup</ta>
            <ta e="T182" id="Seg_4648" s="T181">šäqqɔː-tɨt</ta>
            <ta e="T183" id="Seg_4649" s="T182">mompa</ta>
            <ta e="T184" id="Seg_4650" s="T183">nɔːr-tälʼilʼ</ta>
            <ta e="T185" id="Seg_4651" s="T184">čʼeːlʼɨ-t</ta>
            <ta e="T186" id="Seg_4652" s="T185">toːqqɨt</ta>
            <ta e="T187" id="Seg_4653" s="T186">mompa</ta>
            <ta e="T188" id="Seg_4654" s="T187">Pan</ta>
            <ta e="T189" id="Seg_4655" s="T188">ira</ta>
            <ta e="T190" id="Seg_4656" s="T189">köt</ta>
            <ta e="T191" id="Seg_4657" s="T190">qup-sä</ta>
            <ta e="T192" id="Seg_4658" s="T191">tüː-ŋa</ta>
            <ta e="T193" id="Seg_4659" s="T192">qapija</ta>
            <ta e="T194" id="Seg_4660" s="T193">ɔːtä-p</ta>
            <ta e="T195" id="Seg_4661" s="T194">tɔːqqɨ-qontoːqo</ta>
            <ta e="T196" id="Seg_4662" s="T195">na</ta>
            <ta e="T197" id="Seg_4663" s="T196">tintena</ta>
            <ta e="T198" id="Seg_4664" s="T197">asä-s-t-i-t</ta>
            <ta e="T199" id="Seg_4665" s="T198">ɔːtä-p</ta>
            <ta e="T200" id="Seg_4666" s="T199">tɔːqqɨ-qo</ta>
            <ta e="T201" id="Seg_4667" s="T200">ku-n</ta>
            <ta e="T202" id="Seg_4668" s="T201">mɔːntɨ-lʼ</ta>
            <ta e="T203" id="Seg_4669" s="T202">mɔːt-tɨ</ta>
            <ta e="T204" id="Seg_4670" s="T203">šentɨ</ta>
            <ta e="T205" id="Seg_4671" s="T204">ku-n</ta>
            <ta e="T206" id="Seg_4672" s="T205">mɔːntɨ-lʼ</ta>
            <ta e="T207" id="Seg_4673" s="T206">qaj-tɨ</ta>
            <ta e="T208" id="Seg_4674" s="T207">mun</ta>
            <ta e="T209" id="Seg_4675" s="T208">taq-qalʼ-tɛntɨ-t</ta>
            <ta e="T210" id="Seg_4676" s="T209">ukoːn</ta>
            <ta e="T211" id="Seg_4677" s="T210">nɔːta</ta>
            <ta e="T212" id="Seg_4678" s="T211">taq-qɨlʼ-kolʼčʼimpa-t</ta>
            <ta e="T213" id="Seg_4679" s="T212">qälʼi-t-ɨ-t</ta>
            <ta e="T214" id="Seg_4680" s="T213">ɔːta-p</ta>
            <ta e="T215" id="Seg_4681" s="T214">taq-qɨl-lä</ta>
            <ta e="T216" id="Seg_4682" s="T215">iː-mpa-tɨ</ta>
            <ta e="T217" id="Seg_4683" s="T216">nɨnɨ</ta>
            <ta e="T218" id="Seg_4684" s="T217">šittälʼ</ta>
            <ta e="T219" id="Seg_4685" s="T218">mɨŋa</ta>
            <ta e="T220" id="Seg_4686" s="T219">moqɨnä</ta>
            <ta e="T221" id="Seg_4687" s="T220">qən-nɔː-qij</ta>
            <ta e="T222" id="Seg_4688" s="T221">na</ta>
            <ta e="T223" id="Seg_4689" s="T222">qälɨk-qıː</ta>
            <ta e="T224" id="Seg_4690" s="T223">nɔːr-tälʼilʼ</ta>
            <ta e="T225" id="Seg_4691" s="T224">čʼeːlʼi-t</ta>
            <ta e="T226" id="Seg_4692" s="T225">mompa</ta>
            <ta e="T227" id="Seg_4693" s="T226">tü-ntɔː-mɨt</ta>
            <ta e="T228" id="Seg_4694" s="T227">tɛː</ta>
            <ta e="T229" id="Seg_4695" s="T228">mumpa</ta>
            <ta e="T230" id="Seg_4696" s="T229">kučʼčʼä</ta>
            <ta e="T231" id="Seg_4697" s="T230">ɨkɨ</ta>
            <ta e="T232" id="Seg_4698" s="T231">qən-nɨlɨt</ta>
            <ta e="T233" id="Seg_4699" s="T232">šit-tälʼɨlʼ</ta>
            <ta e="T234" id="Seg_4700" s="T233">čʼeːlo-ntɨ</ta>
            <ta e="T235" id="Seg_4701" s="T234">ınnä</ta>
            <ta e="T236" id="Seg_4702" s="T235">čʼeːlʼi-n-na</ta>
            <ta e="T237" id="Seg_4703" s="T236">na</ta>
            <ta e="T238" id="Seg_4704" s="T237">iːja</ta>
            <ta e="T239" id="Seg_4705" s="T238">nı-lʼ</ta>
            <ta e="T240" id="Seg_4706" s="T239">kətɨ-tɨ-t</ta>
            <ta e="T241" id="Seg_4707" s="T240">ilʼčʼa-ntɨ-nɨk</ta>
            <ta e="T242" id="Seg_4708" s="T241">mɨta</ta>
            <ta e="T243" id="Seg_4709" s="T242">nʼannä-t</ta>
            <ta e="T244" id="Seg_4710" s="T243">tɨmtɨ</ta>
            <ta e="T245" id="Seg_4711" s="T244">tɛːttɨ</ta>
            <ta e="T246" id="Seg_4712" s="T245">toːt</ta>
            <ta e="T247" id="Seg_4713" s="T246">ɔːta-t</ta>
            <ta e="T248" id="Seg_4714" s="T247">nɨŋ-qɨ-ku-ša-lʼ</ta>
            <ta e="T249" id="Seg_4715" s="T248">nʼaro-p</ta>
            <ta e="T250" id="Seg_4716" s="T249">ɛː-nta</ta>
            <ta e="T251" id="Seg_4717" s="T250">načʼčʼä</ta>
            <ta e="T252" id="Seg_4718" s="T251">üːtä-lʼɨ-mɨt</ta>
            <ta e="T253" id="Seg_4719" s="T252">qäːlʼi-t</ta>
            <ta e="T254" id="Seg_4720" s="T253">tüː-ntɔː-tɨt</ta>
            <ta e="T255" id="Seg_4721" s="T254">ɔːtä-p</ta>
            <ta e="T256" id="Seg_4722" s="T255">tɔːqqɨ-q-olam-nɛntɔː-tɨt</ta>
            <ta e="T257" id="Seg_4723" s="T256">ɔːta-n</ta>
            <ta e="T258" id="Seg_4724" s="T257">up-kɨt</ta>
            <ta e="T259" id="Seg_4725" s="T258">kə-lʼčʼo-mɨ-n-tɨ</ta>
            <ta e="T260" id="Seg_4726" s="T259">kuntɨ</ta>
            <ta e="T261" id="Seg_4727" s="T260">kä-rɨ-pɨlʼ</ta>
            <ta e="T262" id="Seg_4728" s="T261">kutar</ta>
            <ta e="T263" id="Seg_4729" s="T262">meː-tɔː-mɨt</ta>
            <ta e="T264" id="Seg_4730" s="T263">na</ta>
            <ta e="T265" id="Seg_4731" s="T264">iːja</ta>
            <ta e="T266" id="Seg_4732" s="T265">nık</ta>
            <ta e="T267" id="Seg_4733" s="T266">kəttɨ-ŋɨ-t</ta>
            <ta e="T268" id="Seg_4734" s="T267">nɨnɨ</ta>
            <ta e="T269" id="Seg_4735" s="T268">nık</ta>
            <ta e="T270" id="Seg_4736" s="T269">kəttɨ-ŋɨ-tɨ</ta>
            <ta e="T271" id="Seg_4737" s="T270">mäkkä</ta>
            <ta e="T272" id="Seg_4738" s="T271">mɨta</ta>
            <ta e="T273" id="Seg_4739" s="T272">šentɨ</ta>
            <ta e="T274" id="Seg_4740" s="T273">mɔːl-lʼet</ta>
            <ta e="T275" id="Seg_4741" s="T274">ɛː-ŋa</ta>
            <ta e="T276" id="Seg_4742" s="T275">ilʼčʼa</ta>
            <ta e="T277" id="Seg_4743" s="T276">mäkkä</ta>
            <ta e="T278" id="Seg_4744" s="T277">mi-tɨ</ta>
            <ta e="T279" id="Seg_4745" s="T278">a</ta>
            <ta e="T280" id="Seg_4746" s="T279">toːna</ta>
            <ta e="T281" id="Seg_4747" s="T280">tınte</ta>
            <ta e="T282" id="Seg_4748" s="T281">timnʼä-n-tɨ</ta>
            <ta e="T283" id="Seg_4749" s="T282">nälʼa</ta>
            <ta e="T284" id="Seg_4750" s="T283">ilʼčʼa-tɨ</ta>
            <ta e="T285" id="Seg_4751" s="T284">na-tɨ-p</ta>
            <ta e="T286" id="Seg_4752" s="T285">kur-alʼtɨ-mpa-tɨ</ta>
            <ta e="T287" id="Seg_4753" s="T286">na</ta>
            <ta e="T289" id="Seg_4754" s="T287">iːja-nɨk</ta>
            <ta e="T290" id="Seg_4755" s="T289">nʼännä-t</ta>
            <ta e="T291" id="Seg_4756" s="T290">tɨmtɨ</ta>
            <ta e="T292" id="Seg_4757" s="T291">maːčʼe-lʼ</ta>
            <ta e="T293" id="Seg_4758" s="T292">tiːčʼi</ta>
            <ta e="T294" id="Seg_4759" s="T293">ɛː-ŋa</ta>
            <ta e="T295" id="Seg_4760" s="T294">nɨmtɨ</ta>
            <ta e="T296" id="Seg_4761" s="T295">mɔːt-ɨ-ŋ-nɛnta-k</ta>
            <ta e="T297" id="Seg_4762" s="T296">šentɨ</ta>
            <ta e="T298" id="Seg_4763" s="T297">mɔːt</ta>
            <ta e="T299" id="Seg_4764" s="T298">tatɨ-ŋɨt</ta>
            <ta e="T300" id="Seg_4765" s="T299">mɨta</ta>
            <ta e="T301" id="Seg_4766" s="T300">üːt-äl-nɔː-tɨt</ta>
            <ta e="T302" id="Seg_4767" s="T301">taːlʼ</ta>
            <ta e="T303" id="Seg_4768" s="T302">Qapıje</ta>
            <ta e="T304" id="Seg_4769" s="T303">nɨ</ta>
            <ta e="T305" id="Seg_4770" s="T304">nɔːr-tälʼilʼ</ta>
            <ta e="T306" id="Seg_4771" s="T305">čʼeːlɨ</ta>
            <ta e="T307" id="Seg_4772" s="T306">tüː-ntɔː-tɨt</ta>
            <ta e="T308" id="Seg_4773" s="T307">Pan</ta>
            <ta e="T309" id="Seg_4774" s="T308">ira-lʼ-mɨ-t</ta>
            <ta e="T310" id="Seg_4775" s="T309">təp-ɨ-t-ɨ-tkiːne</ta>
            <ta e="T311" id="Seg_4776" s="T310">na</ta>
            <ta e="T312" id="Seg_4777" s="T311">qälɨ_ira-t</ta>
            <ta e="T313" id="Seg_4778" s="T312">ɔːta-p</ta>
            <ta e="T314" id="Seg_4779" s="T313">taq-qalʼ-qo</ta>
            <ta e="T315" id="Seg_4780" s="T314">timnʼä-sɨ-qä-t</ta>
            <ta e="T316" id="Seg_4781" s="T315">ɔːtä-p</ta>
            <ta e="T317" id="Seg_4782" s="T316">nɨnɨ</ta>
            <ta e="T318" id="Seg_4783" s="T317">šitte-lʼ</ta>
            <ta e="T319" id="Seg_4784" s="T318">üːt-äl-tɔː-tɨt</ta>
            <ta e="T320" id="Seg_4785" s="T319">na</ta>
            <ta e="T321" id="Seg_4786" s="T320">ɔːtä-p</ta>
            <ta e="T322" id="Seg_4787" s="T321">na</ta>
            <ta e="T323" id="Seg_4788" s="T322">tɔːqqä-ntɔː-tɨt</ta>
            <ta e="T324" id="Seg_4789" s="T323">qapıj</ta>
            <ta e="T325" id="Seg_4790" s="T324">täm</ta>
            <ta e="T326" id="Seg_4791" s="T325">aj</ta>
            <ta e="T327" id="Seg_4792" s="T326">tɔːqqɨ-mpa-tɨ</ta>
            <ta e="T328" id="Seg_4793" s="T327">mɔːs-sä</ta>
            <ta e="T329" id="Seg_4794" s="T328">na</ta>
            <ta e="T330" id="Seg_4795" s="T329">mi-ntɔː-tɨt</ta>
            <ta e="T331" id="Seg_4796" s="T330">na</ta>
            <ta e="T332" id="Seg_4797" s="T331">mačʼi-t</ta>
            <ta e="T333" id="Seg_4798" s="T332">tičʼi-t</ta>
            <ta e="T334" id="Seg_4799" s="T333">nʼännä-lʼ</ta>
            <ta e="T335" id="Seg_4800" s="T334">pɛlʼäq-qɨt</ta>
            <ta e="T336" id="Seg_4801" s="T335">nɨna</ta>
            <ta e="T337" id="Seg_4802" s="T336">mɔːt-ɨ-ŋ-tɔː-tɨt</ta>
            <ta e="T338" id="Seg_4803" s="T337">na</ta>
            <ta e="T339" id="Seg_4804" s="T338">ima-ntɨ-sä</ta>
            <ta e="T340" id="Seg_4805" s="T339">Qapıje</ta>
            <ta e="T341" id="Seg_4806" s="T340">tətta-lʼ</ta>
            <ta e="T342" id="Seg_4807" s="T341">qälɨk</ta>
            <ta e="T343" id="Seg_4808" s="T342">qaj</ta>
            <ta e="T344" id="Seg_4809" s="T343">mɔːt-tɨ</ta>
            <ta e="T345" id="Seg_4810" s="T344">čʼaːŋka</ta>
            <ta e="T346" id="Seg_4811" s="T345">šentɨ</ta>
            <ta e="T347" id="Seg_4812" s="T346">mɔːt-tɨ</ta>
            <ta e="T348" id="Seg_4813" s="T347">qopɨ-lʼ</ta>
            <ta e="T349" id="Seg_4814" s="T348">mɔːt-tɨ</ta>
            <ta e="T350" id="Seg_4815" s="T349">nɨnɨ</ta>
            <ta e="T351" id="Seg_4816" s="T350">šit-tälʼ</ta>
            <ta e="T352" id="Seg_4817" s="T351">šäqqɔː-tɨt</ta>
            <ta e="T353" id="Seg_4818" s="T352">na</ta>
            <ta e="T354" id="Seg_4819" s="T353">ima-ntɨ-sä</ta>
            <ta e="T355" id="Seg_4820" s="T354">təptɨlʼ</ta>
            <ta e="T356" id="Seg_4821" s="T355">qarɨ-t</ta>
            <ta e="T357" id="Seg_4822" s="T356">nı-ŋ</ta>
            <ta e="T358" id="Seg_4823" s="T357">ɛsa</ta>
            <ta e="T359" id="Seg_4824" s="T358">tıntena</ta>
            <ta e="T360" id="Seg_4825" s="T359">ima-ntɨ-nɨk</ta>
            <ta e="T361" id="Seg_4826" s="T360">mat</ta>
            <ta e="T362" id="Seg_4827" s="T361">mɨta</ta>
            <ta e="T363" id="Seg_4828" s="T362">nʼänna</ta>
            <ta e="T364" id="Seg_4829" s="T363">qum-iː-qäk</ta>
            <ta e="T365" id="Seg_4830" s="T364">*koːnɨ-lʼä-k</ta>
            <ta e="T366" id="Seg_4831" s="T365">qaj</ta>
            <ta e="T367" id="Seg_4832" s="T366">mɨta</ta>
            <ta e="T368" id="Seg_4833" s="T367">mɔːt-ɨ-n-pɔː-tɨt</ta>
            <ta e="T369" id="Seg_4834" s="T368">qaj</ta>
            <ta e="T370" id="Seg_4835" s="T369">qaːtɔː-tɨt</ta>
            <ta e="T371" id="Seg_4836" s="T370">nʼännä</ta>
            <ta e="T372" id="Seg_4837" s="T371">laq-alt-elʼčʼa</ta>
            <ta e="T373" id="Seg_4838" s="T372">nɔːkɨr</ta>
            <ta e="T374" id="Seg_4839" s="T373">qoptɨ-p</ta>
            <ta e="T375" id="Seg_4840" s="T374">sɔːrɨ-lʼa</ta>
            <ta e="T376" id="Seg_4841" s="T375">qapı-ja</ta>
            <ta e="T377" id="Seg_4842" s="T376">nɔːs-sar-ɨ-lʼ</ta>
            <ta e="T378" id="Seg_4843" s="T377">ɔːtä-p</ta>
            <ta e="T379" id="Seg_4844" s="T378">qal-tɨ-mmɨ-ntɨ-t</ta>
            <ta e="T380" id="Seg_4845" s="T379">ija</ta>
            <ta e="T381" id="Seg_4846" s="T380">nʼanna</ta>
            <ta e="T382" id="Seg_4847" s="T381">qənn-ɛː-ja</ta>
            <ta e="T383" id="Seg_4848" s="T382">iːma-tɨ</ta>
            <ta e="T384" id="Seg_4849" s="T383">ontɨ</ta>
            <ta e="T385" id="Seg_4850" s="T384">qala</ta>
            <ta e="T386" id="Seg_4851" s="T385">na</ta>
            <ta e="T387" id="Seg_4852" s="T386">omta</ta>
            <ta e="T388" id="Seg_4853" s="T387">na</ta>
            <ta e="T389" id="Seg_4854" s="T388">omta</ta>
            <ta e="T390" id="Seg_4855" s="T389">ukkɨr</ta>
            <ta e="T391" id="Seg_4856" s="T390">tä-t</ta>
            <ta e="T392" id="Seg_4857" s="T391">čʼontoː-qɨt</ta>
            <ta e="T393" id="Seg_4858" s="T392">čʼeːlʼɨ-tɨ</ta>
            <ta e="T394" id="Seg_4859" s="T393">üːtoː-ntɨ</ta>
            <ta e="T395" id="Seg_4860" s="T394">na</ta>
            <ta e="T396" id="Seg_4861" s="T395">qən-na</ta>
            <ta e="T397" id="Seg_4862" s="T396">na</ta>
            <ta e="T398" id="Seg_4863" s="T397">iːja</ta>
            <ta e="T399" id="Seg_4864" s="T398">lʼamɨ-k</ta>
            <ta e="T400" id="Seg_4865" s="T399">kəː-ŋa</ta>
            <ta e="T401" id="Seg_4866" s="T400">nʼännä-t</ta>
            <ta e="T402" id="Seg_4867" s="T401">ukkɨr</ta>
            <ta e="T403" id="Seg_4868" s="T402">to-t</ta>
            <ta e="T404" id="Seg_4869" s="T403">čʼontoː-qɨt</ta>
            <ta e="T405" id="Seg_4870" s="T404">qup</ta>
            <ta e="T406" id="Seg_4871" s="T405">na-ja-p</ta>
            <ta e="T407" id="Seg_4872" s="T406">tatɨ-ntɔː-tɨt</ta>
            <ta e="T408" id="Seg_4873" s="T407">takkɨ-nɨ</ta>
            <ta e="T409" id="Seg_4874" s="T408">Pan</ta>
            <ta e="T410" id="Seg_4875" s="T409">ira-lʼ-mɨ-t</ta>
            <ta e="T411" id="Seg_4876" s="T410">tatɨ-ntɔː-tɨt</ta>
            <ta e="T412" id="Seg_4877" s="T411">köt</ta>
            <ta e="T413" id="Seg_4878" s="T412">qum-tɨ</ta>
            <ta e="T414" id="Seg_4879" s="T413">qapı-ja</ta>
            <ta e="T415" id="Seg_4880" s="T414">ɔːtä-p</ta>
            <ta e="T416" id="Seg_4881" s="T415">tɔːqqɨ-sɔːt-ɨlʼ</ta>
            <ta e="T417" id="Seg_4882" s="T416">Pan</ta>
            <ta e="T418" id="Seg_4883" s="T417">ira</ta>
            <ta e="T419" id="Seg_4884" s="T418">naššak</ta>
            <ta e="T420" id="Seg_4885" s="T419">qəːtɨ-sä</ta>
            <ta e="T421" id="Seg_4886" s="T420">ɛː-ŋa</ta>
            <ta e="T422" id="Seg_4887" s="T421">nɨn</ta>
            <ta e="T423" id="Seg_4888" s="T422">šit-talʼ</ta>
            <ta e="T424" id="Seg_4889" s="T423">na</ta>
            <ta e="T425" id="Seg_4890" s="T424">ija</ta>
            <ta e="T426" id="Seg_4891" s="T425">tü-lʼčʼi-kunä</ta>
            <ta e="T427" id="Seg_4892" s="T426">ima-tä</ta>
            <ta e="T428" id="Seg_4893" s="T427">üŋkɨl-ti-mpa-tɨ</ta>
            <ta e="T429" id="Seg_4894" s="T428">montɨ</ta>
            <ta e="T430" id="Seg_4895" s="T429">čʼap</ta>
            <ta e="T431" id="Seg_4896" s="T430">tüː-lʼčʼa</ta>
            <ta e="T432" id="Seg_4897" s="T431">Pan</ta>
            <ta e="T433" id="Seg_4898" s="T432">ira-lʼ-mɨ-t</ta>
            <ta e="T434" id="Seg_4899" s="T433">qaj</ta>
            <ta e="T435" id="Seg_4900" s="T434">montɨ</ta>
            <ta e="T436" id="Seg_4901" s="T435">tü-ŋɔː-tɨt</ta>
            <ta e="T437" id="Seg_4902" s="T436">qaqlʼi-lʼ</ta>
            <ta e="T438" id="Seg_4903" s="T437">mɨ-tɨ</ta>
            <ta e="T439" id="Seg_4904" s="T438">nɨ-k</ta>
            <ta e="T440" id="Seg_4905" s="T439">tott-äl-nɔː-tɨt</ta>
            <ta e="T441" id="Seg_4906" s="T440">poː-qɨt</ta>
            <ta e="T442" id="Seg_4907" s="T441">eːtɨmantoː-qɨt</ta>
            <ta e="T443" id="Seg_4908" s="T442">mɔːt-tɨ</ta>
            <ta e="T444" id="Seg_4909" s="T443">šeːr-na</ta>
            <ta e="T445" id="Seg_4910" s="T444">mɔːta-n</ta>
            <ta e="T446" id="Seg_4911" s="T445">ɔː-t</ta>
            <ta e="T447" id="Seg_4912" s="T446">nɨŋ-kɨ-lʼa</ta>
            <ta e="T448" id="Seg_4913" s="T447">mɔːta-n</ta>
            <ta e="T449" id="Seg_4914" s="T448">ɔː-t</ta>
            <ta e="T450" id="Seg_4915" s="T449">kʼeː</ta>
            <ta e="T451" id="Seg_4916" s="T450">topɨ-m-tɨ</ta>
            <ta e="T452" id="Seg_4917" s="T451">tup-al-kɨ-tɨ</ta>
            <ta e="T453" id="Seg_4918" s="T452">peːmɨ-m-tɨ</ta>
            <ta e="T454" id="Seg_4919" s="T453">na</ta>
            <ta e="T455" id="Seg_4920" s="T454">tup-al-nɨ-t</ta>
            <ta e="T456" id="Seg_4921" s="T455">nı</ta>
            <ta e="T457" id="Seg_4922" s="T456">kəttɨ-ŋɨ-tɨ</ta>
            <ta e="T458" id="Seg_4923" s="T457">Pan</ta>
            <ta e="T459" id="Seg_4924" s="T458">ira-nɨk</ta>
            <ta e="T460" id="Seg_4925" s="T459">nʼännä</ta>
            <ta e="T461" id="Seg_4926" s="T460">kor-altɨ-lä</ta>
            <ta e="T462" id="Seg_4927" s="T461">ɔːk-lɨ</ta>
            <ta e="T463" id="Seg_4928" s="T462">miːt</ta>
            <ta e="T464" id="Seg_4929" s="T463">olʼlʼa</ta>
            <ta e="T465" id="Seg_4930" s="T464">qolʼtɨ-t</ta>
            <ta e="T466" id="Seg_4931" s="T465">qəqɨ-t</ta>
            <ta e="T467" id="Seg_4932" s="T466">oːlʼa</ta>
            <ta e="T468" id="Seg_4933" s="T467">taːlʼ</ta>
            <ta e="T469" id="Seg_4934" s="T468">na</ta>
            <ta e="T470" id="Seg_4935" s="T469">nʼänna</ta>
            <ta e="T471" id="Seg_4936" s="T470">paqtɨ-mpa</ta>
            <ta e="T472" id="Seg_4937" s="T471">na</ta>
            <ta e="T473" id="Seg_4938" s="T472">kupak-sä</ta>
            <ta e="T474" id="Seg_4939" s="T473">täq-al-e-mmɨ-ntɨ-tɨ</ta>
            <ta e="T475" id="Seg_4940" s="T474">Pan</ta>
            <ta e="T476" id="Seg_4941" s="T475">ira-lʼ</ta>
            <ta e="T477" id="Seg_4942" s="T476">mɨ-t-ɨ-p</ta>
            <ta e="T478" id="Seg_4943" s="T477">muːntɨkə</ta>
            <ta e="T479" id="Seg_4944" s="T478">lʼäpek</ta>
            <ta e="T480" id="Seg_4945" s="T479">qättɨ-mpa-t</ta>
            <ta e="T481" id="Seg_4946" s="T480">ɔːmmɨ-tɨ</ta>
            <ta e="T482" id="Seg_4947" s="T481">qu-mpa</ta>
            <ta e="T483" id="Seg_4948" s="T482">Pan</ta>
            <ta e="T484" id="Seg_4949" s="T483">ira</ta>
            <ta e="T485" id="Seg_4950" s="T484">ilʼi-lä</ta>
            <ta e="T486" id="Seg_4951" s="T485">qalɨ-mpa</ta>
            <ta e="T487" id="Seg_4952" s="T486">šittä-qij</ta>
            <ta e="T488" id="Seg_4953" s="T487">ilɨ-lä</ta>
            <ta e="T489" id="Seg_4954" s="T488">qaːlɨ-mmɨ-nta</ta>
            <ta e="T490" id="Seg_4955" s="T489">ponä</ta>
            <ta e="T491" id="Seg_4956" s="T490">qə-ll-äi-tɨ</ta>
            <ta e="T492" id="Seg_4957" s="T491">na</ta>
            <ta e="T493" id="Seg_4958" s="T492">iːja</ta>
            <ta e="T494" id="Seg_4959" s="T493">ukkur</ta>
            <ta e="T495" id="Seg_4960" s="T494">pɔːrɨ</ta>
            <ta e="T496" id="Seg_4961" s="T495">kupak-sä</ta>
            <ta e="T497" id="Seg_4962" s="T496">täq-al-e-mpa-tɨ</ta>
            <ta e="T498" id="Seg_4963" s="T497">muntɨk</ta>
            <ta e="T499" id="Seg_4964" s="T498">porqä-n-tɨ</ta>
            <ta e="T500" id="Seg_4965" s="T499">šunʼnʼo-ntɨ</ta>
            <ta e="T501" id="Seg_4966" s="T500">pümmɨ-n-tɨ</ta>
            <ta e="T502" id="Seg_4967" s="T501">šʼu</ta>
            <ta e="T503" id="Seg_4968" s="T502">nʼölʼqɨ-mɔːt-po-tät</ta>
            <ta e="T504" id="Seg_4969" s="T503">na</ta>
            <ta e="T505" id="Seg_4970" s="T504">qu-pɨlʼ-mɨ-t</ta>
            <ta e="T506" id="Seg_4971" s="T505">nɨnɨ</ta>
            <ta e="T507" id="Seg_4972" s="T506">šitälʼ</ta>
            <ta e="T508" id="Seg_4973" s="T507">ponä</ta>
            <ta e="T509" id="Seg_4974" s="T508">kä-lʼlʼ-ei-tɨ</ta>
            <ta e="T510" id="Seg_4975" s="T509">eːtɨmannoː-ntɨ</ta>
            <ta e="T511" id="Seg_4976" s="T510">tintena</ta>
            <ta e="T512" id="Seg_4977" s="T511">qaqlɨ</ta>
            <ta e="T513" id="Seg_4978" s="T512">tüːr</ta>
            <ta e="T514" id="Seg_4979" s="T513">tolʼčʼi-tɨ</ta>
            <ta e="T515" id="Seg_4980" s="T514">na</ta>
            <ta e="T516" id="Seg_4981" s="T515">sɔːr-älɨ-mpa</ta>
            <ta e="T517" id="Seg_4982" s="T516">Pan</ta>
            <ta e="T518" id="Seg_4983" s="T517">ira-p</ta>
            <ta e="T519" id="Seg_4984" s="T518">aj</ta>
            <ta e="T520" id="Seg_4985" s="T519">ponä</ta>
            <ta e="T521" id="Seg_4986" s="T520">i-tɨ-mpa-t</ta>
            <ta e="T522" id="Seg_4987" s="T521">čʼatɨ-mpa-t</ta>
            <ta e="T523" id="Seg_4988" s="T522">qapija</ta>
            <ta e="T524" id="Seg_4989" s="T523">muntɨk</ta>
            <ta e="T525" id="Seg_4990" s="T524">pona</ta>
            <ta e="T526" id="Seg_4991" s="T525">iː-tɨ-mpa-t</ta>
            <ta e="T527" id="Seg_4992" s="T526">ılla</ta>
            <ta e="T528" id="Seg_4993" s="T527">omta</ta>
            <ta e="T529" id="Seg_4994" s="T528">malʼčʼa-lʼ</ta>
            <ta e="T530" id="Seg_4995" s="T529">tɔːq-tɨ</ta>
            <ta e="T531" id="Seg_4996" s="T530">toː</ta>
            <ta e="T532" id="Seg_4997" s="T531">iː-lʼä</ta>
            <ta e="T533" id="Seg_4998" s="T532">am-ɨ-r-lʼä</ta>
            <ta e="T534" id="Seg_4999" s="T533">ıllä</ta>
            <ta e="T535" id="Seg_5000" s="T534">om-mɨ-nta</ta>
            <ta e="T536" id="Seg_5001" s="T535">šäqqa</ta>
            <ta e="T537" id="Seg_5002" s="T536">na</ta>
            <ta e="T538" id="Seg_5003" s="T537">qum-iː-tɨ</ta>
            <ta e="T539" id="Seg_5004" s="T538">poː-qɨt</ta>
            <ta e="T540" id="Seg_5005" s="T539">na</ta>
            <ta e="T541" id="Seg_5006" s="T540">ipp-älɨ-mpɔː-tɨt</ta>
            <ta e="T542" id="Seg_5007" s="T541">kuttar</ta>
            <ta e="T543" id="Seg_5008" s="T542">sar-älɨ-mpa</ta>
            <ta e="T544" id="Seg_5009" s="T543">nılʼčʼi-k</ta>
            <ta e="T545" id="Seg_5010" s="T544">šäqqa</ta>
            <ta e="T546" id="Seg_5011" s="T545">tɔːptɨlʼ</ta>
            <ta e="T547" id="Seg_5012" s="T546">qarɨ-t</ta>
            <ta e="T548" id="Seg_5013" s="T547">ɨnnä</ta>
            <ta e="T549" id="Seg_5014" s="T548">wäša</ta>
            <ta e="T550" id="Seg_5015" s="T549">Pan</ta>
            <ta e="T551" id="Seg_5016" s="T550">ira</ta>
            <ta e="T552" id="Seg_5017" s="T551">nılʼčʼi-k</ta>
            <ta e="T553" id="Seg_5018" s="T552">tap</ta>
            <ta e="T554" id="Seg_5019" s="T553">wäntɨ-lʼ</ta>
            <ta e="T555" id="Seg_5020" s="T554">tɔːk-tɨ</ta>
            <ta e="T556" id="Seg_5021" s="T555">tɔːt-ɨ-k</ta>
            <ta e="T557" id="Seg_5022" s="T556">taŋk-ıːčʼi-mpa</ta>
            <ta e="T558" id="Seg_5023" s="T557">kekkɨsä</ta>
            <ta e="T559" id="Seg_5024" s="T558">sai-lʼa-tɨ</ta>
            <ta e="T560" id="Seg_5025" s="T559">qə-tɨ-mpa</ta>
            <ta e="T561" id="Seg_5026" s="T560">toːna</ta>
            <ta e="T562" id="Seg_5027" s="T561">qälɨk-qı-tɨ</ta>
            <ta e="T563" id="Seg_5028" s="T562">ešo</ta>
            <ta e="T564" id="Seg_5029" s="T563">soma-lʼoqı</ta>
            <ta e="T565" id="Seg_5030" s="T564">ɛ-ppɨ-mpa</ta>
            <ta e="T566" id="Seg_5031" s="T565">nı-lʼ</ta>
            <ta e="T567" id="Seg_5032" s="T566">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T568" id="Seg_5033" s="T567">na</ta>
            <ta e="T569" id="Seg_5034" s="T568">ija</ta>
            <ta e="T570" id="Seg_5035" s="T569">mɨta</ta>
            <ta e="T571" id="Seg_5036" s="T570">na</ta>
            <ta e="T572" id="Seg_5037" s="T571">qälʼi-t-ɨ-tkiːnä</ta>
            <ta e="T573" id="Seg_5038" s="T572">moqonä</ta>
            <ta e="T574" id="Seg_5039" s="T573">qən-ta-t</ta>
            <ta e="T575" id="Seg_5040" s="T574">talʼtälʼlʼä</ta>
            <ta e="T576" id="Seg_5041" s="T575">untɨt</ta>
            <ta e="T577" id="Seg_5042" s="T576">qaqlɨ-ntɨ-sä</ta>
            <ta e="T578" id="Seg_5043" s="T577">nɨnɨ</ta>
            <ta e="T579" id="Seg_5044" s="T578">šit-tälʼ</ta>
            <ta e="T580" id="Seg_5045" s="T579">toːna</ta>
            <ta e="T581" id="Seg_5046" s="T580">meː-qıj</ta>
            <ta e="T582" id="Seg_5047" s="T581">mɔːt-tɨ</ta>
            <ta e="T583" id="Seg_5048" s="T582">šeːr-ɨ-š-pɔː-tɨt</ta>
            <ta e="T584" id="Seg_5049" s="T583">qaj</ta>
            <ta e="T585" id="Seg_5050" s="T584">am-ɨ-r-pɔː-tɨt</ta>
            <ta e="T586" id="Seg_5051" s="T585">qaj</ta>
            <ta e="T587" id="Seg_5052" s="T586">aš</ta>
            <ta e="T588" id="Seg_5053" s="T587">nɨnɨ</ta>
            <ta e="T589" id="Seg_5054" s="T588">tintena</ta>
            <ta e="T590" id="Seg_5055" s="T589">sar-al-pɨ-tɨlʼ</ta>
            <ta e="T591" id="Seg_5056" s="T590">qaqlɨ-m-tɨt</ta>
            <ta e="T592" id="Seg_5057" s="T591">nɨnɨ</ta>
            <ta e="T593" id="Seg_5058" s="T592">moqonä</ta>
            <ta e="T594" id="Seg_5059" s="T593">qəːčʼelʼ-lʼä</ta>
            <ta e="T595" id="Seg_5060" s="T594">na</ta>
            <ta e="T596" id="Seg_5061" s="T595">qum-iː-m-tɨt</ta>
            <ta e="T597" id="Seg_5062" s="T596">qu-pɨlʼ</ta>
            <ta e="T598" id="Seg_5063" s="T597">qum-iː-m-tɨ</ta>
            <ta e="T599" id="Seg_5064" s="T598">nık</ta>
            <ta e="T600" id="Seg_5065" s="T599">qən-tɨ-ŋɨ-t</ta>
            <ta e="T601" id="Seg_5066" s="T600">takkɨ</ta>
            <ta e="T602" id="Seg_5067" s="T601">moqonä</ta>
            <ta e="T603" id="Seg_5068" s="T602">Pan</ta>
            <ta e="T604" id="Seg_5069" s="T603">ira-m-tɨ</ta>
            <ta e="T605" id="Seg_5070" s="T604">aj</ta>
            <ta e="T606" id="Seg_5071" s="T605">kočʼi-lʼä</ta>
            <ta e="T607" id="Seg_5072" s="T606">qən-tɨ-tɨ</ta>
            <ta e="T608" id="Seg_5073" s="T607">qaj</ta>
            <ta e="T609" id="Seg_5074" s="T608">kɨkɨ</ta>
            <ta e="T611" id="Seg_5075" s="T610">qən-tɨ-t</ta>
            <ta e="T612" id="Seg_5076" s="T611">qən-na-tɨt</ta>
            <ta e="T613" id="Seg_5077" s="T612">tına</ta>
            <ta e="T614" id="Seg_5078" s="T613">qum-a-t</ta>
            <ta e="T615" id="Seg_5079" s="T614">soma-k</ta>
            <ta e="T616" id="Seg_5080" s="T615">qən-nɔː-tɨt</ta>
            <ta e="T617" id="Seg_5081" s="T616">ima-ntɨ-nɨ</ta>
            <ta e="T618" id="Seg_5082" s="T617">nık</ta>
            <ta e="T619" id="Seg_5083" s="T618">kətɨ-ŋɨ-t</ta>
            <ta e="T620" id="Seg_5084" s="T619">mɔːl-lɨ</ta>
            <ta e="T621" id="Seg_5085" s="T620">toː</ta>
            <ta e="T622" id="Seg_5086" s="T621">tıl-ätɨ</ta>
            <ta e="T623" id="Seg_5087" s="T622">me</ta>
            <ta e="T624" id="Seg_5088" s="T623">nʼänna</ta>
            <ta e="T625" id="Seg_5089" s="T624">qəlʼ-lʼä</ta>
            <ta e="T626" id="Seg_5090" s="T625">qum-ɨ-t-ɨ-tkiːne</ta>
            <ta e="T627" id="Seg_5091" s="T626">aš</ta>
            <ta e="T628" id="Seg_5092" s="T627">tɛnimɨ-mpa-tɨ</ta>
            <ta e="T629" id="Seg_5093" s="T628">ilʼčʼa-ntɨ-lʼ</ta>
            <ta e="T630" id="Seg_5094" s="T629">mɨ-t</ta>
            <ta e="T631" id="Seg_5095" s="T630">qos</ta>
            <ta e="T632" id="Seg_5096" s="T631">qaj</ta>
            <ta e="T633" id="Seg_5097" s="T632">nʼenna</ta>
            <ta e="T634" id="Seg_5098" s="T633">qən-na</ta>
            <ta e="T635" id="Seg_5099" s="T634">tintena</ta>
            <ta e="T636" id="Seg_5100" s="T635">onta</ta>
            <ta e="T637" id="Seg_5101" s="T636">aša</ta>
            <ta e="T638" id="Seg_5102" s="T637">koŋɨ-mpa</ta>
            <ta e="T639" id="Seg_5103" s="T638">toːna</ta>
            <ta e="T640" id="Seg_5104" s="T639">ima-tɨ</ta>
            <ta e="T641" id="Seg_5105" s="T640">na</ta>
            <ta e="T642" id="Seg_5106" s="T641">kət-äl-mɨ-ntɨ-tɨ</ta>
            <ta e="T643" id="Seg_5107" s="T642">əsɨ-ntɨ-lʼ</ta>
            <ta e="T644" id="Seg_5108" s="T643">ilʼčʼa-ntɨ-lʼ</ta>
            <ta e="T645" id="Seg_5109" s="T644">mɨ-qä-tkiːne</ta>
            <ta e="T646" id="Seg_5110" s="T645">ira-lʼ</ta>
            <ta e="T647" id="Seg_5111" s="T646">mɨ-t</ta>
            <ta e="T648" id="Seg_5112" s="T647">moqonä</ta>
            <ta e="T649" id="Seg_5113" s="T648">qəs-sɔː-tɨt</ta>
            <ta e="T650" id="Seg_5114" s="T649">a</ta>
            <ta e="T651" id="Seg_5115" s="T650">nɨnä</ta>
            <ta e="T652" id="Seg_5116" s="T651">šitte-lʼ</ta>
            <ta e="T653" id="Seg_5117" s="T652">pija-ka</ta>
            <ta e="T654" id="Seg_5118" s="T653">pija</ta>
            <ta e="T655" id="Seg_5119" s="T654">ni</ta>
            <ta e="T656" id="Seg_5120" s="T655">qaj</ta>
            <ta e="T657" id="Seg_5121" s="T656">aš</ta>
            <ta e="T658" id="Seg_5122" s="T657">tɛnimɔː-tɨt</ta>
            <ta e="T659" id="Seg_5123" s="T658">ta</ta>
            <ta e="T660" id="Seg_5124" s="T659">qən-na</ta>
            <ta e="T661" id="Seg_5125" s="T660">täp-ɨ-t</ta>
            <ta e="T662" id="Seg_5126" s="T661">ilʼčʼa-n-tɨ</ta>
            <ta e="T663" id="Seg_5127" s="T662">mɔːt-tɨ</ta>
            <ta e="T664" id="Seg_5128" s="T663">šer-nɔː-qı</ta>
            <ta e="T665" id="Seg_5129" s="T664">qaqlɨ-lʼ</ta>
            <ta e="T666" id="Seg_5130" s="T665">tɔːk-tɨ</ta>
            <ta e="T667" id="Seg_5131" s="T666">kurɨ-lʼä</ta>
            <ta e="T668" id="Seg_5132" s="T667">nılʼčʼi-k</ta>
            <ta e="T669" id="Seg_5133" s="T668">tottɔː-tɨt</ta>
            <ta e="T670" id="Seg_5134" s="T669">toːna</ta>
            <ta e="T671" id="Seg_5135" s="T670">qən-mɨ-ntɔː-tɨt</ta>
            <ta e="T672" id="Seg_5136" s="T671">moqɨnä</ta>
            <ta e="T673" id="Seg_5137" s="T672">na</ta>
            <ta e="T674" id="Seg_5138" s="T673">qən-mɨ-ntɔː-tɨt</ta>
            <ta e="T675" id="Seg_5139" s="T674">Pan</ta>
            <ta e="T676" id="Seg_5140" s="T675">irä</ta>
            <ta e="T677" id="Seg_5141" s="T676">takkɨt</ta>
            <ta e="T678" id="Seg_5142" s="T677">kekkɨsa</ta>
            <ta e="T679" id="Seg_5143" s="T678">mɔːt-qɨn-tɨ</ta>
            <ta e="T680" id="Seg_5144" s="T679">tulʼi-š-pa</ta>
            <ta e="T681" id="Seg_5145" s="T680">ıllä</ta>
            <ta e="T682" id="Seg_5146" s="T681">qu-mpa</ta>
            <ta e="T683" id="Seg_5147" s="T682">toːna</ta>
            <ta e="T684" id="Seg_5148" s="T683">šittɨ</ta>
            <ta e="T685" id="Seg_5149" s="T684">qum-oː-qı</ta>
            <ta e="T686" id="Seg_5150" s="T685">ilɨ-mpɔː-qı</ta>
            <ta e="T687" id="Seg_5151" s="T686">Pan</ta>
            <ta e="T688" id="Seg_5152" s="T687">ira</ta>
            <ta e="T689" id="Seg_5153" s="T688">nim-tɨ</ta>
            <ta e="T690" id="Seg_5154" s="T689">ɛː-ŋa</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_5155" s="T0">ukkɨr</ta>
            <ta e="T2" id="Seg_5156" s="T1">iːja-tɨ</ta>
            <ta e="T3" id="Seg_5157" s="T2">ɛː-mpɨ-mpɨ-ntɨ</ta>
            <ta e="T4" id="Seg_5158" s="T3">neː-kɨtɨ-lʼ</ta>
            <ta e="T5" id="Seg_5159" s="T4">pɛlɨ-kɨtɨ-lʼ</ta>
            <ta e="T6" id="Seg_5160" s="T5">na</ta>
            <ta e="T7" id="Seg_5161" s="T6">tɛntɨlʼ</ta>
            <ta e="T8" id="Seg_5162" s="T7">ašša</ta>
            <ta e="T9" id="Seg_5163" s="T8">čʼaptä</ta>
            <ta e="T10" id="Seg_5164" s="T9">ira</ta>
            <ta e="T11" id="Seg_5165" s="T10">ilɨ-mpɨ</ta>
            <ta e="T12" id="Seg_5166" s="T11">qüːtɨ-lɨ-mpɨ</ta>
            <ta e="T13" id="Seg_5167" s="T12">na</ta>
            <ta e="T14" id="Seg_5168" s="T13">šölʼqum-ɨ-lʼ</ta>
            <ta e="T15" id="Seg_5169" s="T14">ira</ta>
            <ta e="T16" id="Seg_5170" s="T15">taplʼälʼ</ta>
            <ta e="T17" id="Seg_5171" s="T16">qoltɨ-n</ta>
            <ta e="T18" id="Seg_5172" s="T17">pɛläk-qɨn</ta>
            <ta e="T19" id="Seg_5173" s="T18">ilɨ-mpɨ</ta>
            <ta e="T20" id="Seg_5174" s="T19">poː-kɨtɨ-lʼ</ta>
            <ta e="T21" id="Seg_5175" s="T20">təttɨ-n</ta>
            <ta e="T22" id="Seg_5176" s="T21">točʼčʼä-qɨn</ta>
            <ta e="T23" id="Seg_5177" s="T22">ilɨ-mpɨ</ta>
            <ta e="T24" id="Seg_5178" s="T23">tına</ta>
            <ta e="T25" id="Seg_5179" s="T24">qüːtɨ-lɨ-mpɨ</ta>
            <ta e="T26" id="Seg_5180" s="T25">ukkɨr</ta>
            <ta e="T27" id="Seg_5181" s="T26">iːja-tɨ</ta>
            <ta e="T28" id="Seg_5182" s="T27">pɛlɨ-kɨtɨ-lʼ</ta>
            <ta e="T29" id="Seg_5183" s="T28">neː-kɨtɨ-lʼ</ta>
            <ta e="T30" id="Seg_5184" s="T29">nʼi</ta>
            <ta e="T31" id="Seg_5185" s="T30">nälʼa-tɨ</ta>
            <ta e="T32" id="Seg_5186" s="T31">čʼäːŋkɨ</ta>
            <ta e="T33" id="Seg_5187" s="T32">nʼi</ta>
            <ta e="T34" id="Seg_5188" s="T33">qaj-tɨ</ta>
            <ta e="T35" id="Seg_5189" s="T34">čʼäːŋkɨ</ta>
            <ta e="T36" id="Seg_5190" s="T35">nɨːnɨ</ta>
            <ta e="T37" id="Seg_5191" s="T36">əsɨ-tɨ</ta>
            <ta e="T38" id="Seg_5192" s="T37">qüːtɨ-llälɨ</ta>
            <ta e="T39" id="Seg_5193" s="T38">na</ta>
            <ta e="T40" id="Seg_5194" s="T39">iːja-lʼa</ta>
            <ta e="T41" id="Seg_5195" s="T40">na</ta>
            <ta e="T42" id="Seg_5196" s="T41">iːja</ta>
            <ta e="T43" id="Seg_5197" s="T42">qapı-ja</ta>
            <ta e="T44" id="Seg_5198" s="T43">nɨmtɨ</ta>
            <ta e="T45" id="Seg_5199" s="T44">ilɨ</ta>
            <ta e="T46" id="Seg_5200" s="T45">əsɨ-ntɨ-sä</ta>
            <ta e="T47" id="Seg_5201" s="T46">ama-ntɨ-sä</ta>
            <ta e="T48" id="Seg_5202" s="T47">to-ntɨ</ta>
            <ta e="T49" id="Seg_5203" s="T48">šittɨ</ta>
            <ta e="T50" id="Seg_5204" s="T49">qälɨk-lʼ</ta>
            <ta e="T51" id="Seg_5205" s="T50">timnʼa-sɨ-t</ta>
            <ta e="T52" id="Seg_5206" s="T51">ɛː-mpɨ-ntɨ-tɨt</ta>
            <ta e="T693" id="Seg_5207" s="T52">qäl</ta>
            <ta e="T53" id="Seg_5208" s="T693">ira</ta>
            <ta e="T54" id="Seg_5209" s="T53">puː-š-ɛntɨ</ta>
            <ta e="T55" id="Seg_5210" s="T54">tına</ta>
            <ta e="T56" id="Seg_5211" s="T55">ira</ta>
            <ta e="T57" id="Seg_5212" s="T56">ıllä</ta>
            <ta e="T58" id="Seg_5213" s="T57">qu-ŋɨ</ta>
            <ta e="T59" id="Seg_5214" s="T58">tına</ta>
            <ta e="T694" id="Seg_5215" s="T59">qäl</ta>
            <ta e="T60" id="Seg_5216" s="T694">ira</ta>
            <ta e="T61" id="Seg_5217" s="T60">kučʼčʼä</ta>
            <ta e="T62" id="Seg_5218" s="T61">qattɨ-ɛntɨ</ta>
            <ta e="T63" id="Seg_5219" s="T62">toː</ta>
            <ta e="T64" id="Seg_5220" s="T63">timnʼa-ntɨ-nɨŋ</ta>
            <ta e="T65" id="Seg_5221" s="T64">puː-ŋɨ</ta>
            <ta e="T66" id="Seg_5222" s="T65">ira</ta>
            <ta e="T67" id="Seg_5223" s="T66">qu-ntɨ</ta>
            <ta e="T68" id="Seg_5224" s="T67">ıllä</ta>
            <ta e="T69" id="Seg_5225" s="T68">meː-ŋɔːtɨ</ta>
            <ta e="T70" id="Seg_5226" s="T69">šittälʼ</ta>
            <ta e="T71" id="Seg_5227" s="T70">imaqota-tɨ</ta>
            <ta e="T72" id="Seg_5228" s="T71">qüːtɨ-alʼ-lä</ta>
            <ta e="T73" id="Seg_5229" s="T72">aj</ta>
            <ta e="T74" id="Seg_5230" s="T73">ıllä</ta>
            <ta e="T75" id="Seg_5231" s="T74">qu-ŋɨ</ta>
            <ta e="T76" id="Seg_5232" s="T75">ira-lʼ</ta>
            <ta e="T77" id="Seg_5233" s="T76">mɨ-qı</ta>
            <ta e="T78" id="Seg_5234" s="T77">iːja-tɨt</ta>
            <ta e="T79" id="Seg_5235" s="T78">neː-kɔːlɨ</ta>
            <ta e="T80" id="Seg_5236" s="T79">pɛlɨ-kɔːlɨ</ta>
            <ta e="T81" id="Seg_5237" s="T80">qalɨ</ta>
            <ta e="T82" id="Seg_5238" s="T81">kučʼčʼä</ta>
            <ta e="T83" id="Seg_5239" s="T82">qattɨ-ɛntɨ</ta>
            <ta e="T84" id="Seg_5240" s="T83">na</ta>
            <ta e="T695" id="Seg_5241" s="T84">qäl</ta>
            <ta e="T85" id="Seg_5242" s="T695">ira</ta>
            <ta e="T86" id="Seg_5243" s="T85">šittɨ-mtäl</ta>
            <ta e="T87" id="Seg_5244" s="T86">tɔː</ta>
            <ta e="T88" id="Seg_5245" s="T87">puː-tɨ-mpɨ-tɨ</ta>
            <ta e="T89" id="Seg_5246" s="T88">wərqɨ</ta>
            <ta e="T90" id="Seg_5247" s="T89">iːja-tɨ</ta>
            <ta e="T91" id="Seg_5248" s="T90">nılʼčʼɨ-lʼ</ta>
            <ta e="T92" id="Seg_5249" s="T91">iːja-tɨ</ta>
            <ta e="T93" id="Seg_5250" s="T92">ɛː-mpɨ-mpɨ-ntɨ</ta>
            <ta e="T94" id="Seg_5251" s="T93">puː-tɨ-ŋɨ-tɨ</ta>
            <ta e="T95" id="Seg_5252" s="T94">ɔːtä-tɨ</ta>
            <ta e="T96" id="Seg_5253" s="T95">aj</ta>
            <ta e="T97" id="Seg_5254" s="T96">čʼäːŋkɨ</ta>
            <ta e="T98" id="Seg_5255" s="T97">ɔːtä-kɔːlɨ-k</ta>
            <ta e="T99" id="Seg_5256" s="T98">topɨ-sä</ta>
            <ta e="T100" id="Seg_5257" s="T99">ilɨ-mpɨ-tɨt</ta>
            <ta e="T101" id="Seg_5258" s="T100">nɨmtɨ</ta>
            <ta e="T102" id="Seg_5259" s="T101">ilɨ-mpɨ-tɨt</ta>
            <ta e="T103" id="Seg_5260" s="T102">ilɨ-mpɨ-tɨt</ta>
            <ta e="T104" id="Seg_5261" s="T103">tɔː</ta>
            <ta e="T105" id="Seg_5262" s="T104">puː-lä</ta>
            <ta e="T106" id="Seg_5263" s="T105">qäl_ira-n</ta>
            <ta e="T107" id="Seg_5264" s="T106">mɔːt-tɨ</ta>
            <ta e="T691" id="Seg_5265" s="T107">qäl</ta>
            <ta e="T108" id="Seg_5266" s="T691">ira</ta>
            <ta e="T109" id="Seg_5267" s="T108">iːja-tɨ</ta>
            <ta e="T110" id="Seg_5268" s="T109">čʼäːŋkɨ-mpɨ</ta>
            <ta e="T111" id="Seg_5269" s="T110">iːja-kɨtɨ-lʼ</ta>
            <ta e="T112" id="Seg_5270" s="T111">ima-tɨ</ta>
            <ta e="T113" id="Seg_5271" s="T112">toːnna</ta>
            <ta e="T114" id="Seg_5272" s="T113">čʼontɨ-lʼ</ta>
            <ta e="T115" id="Seg_5273" s="T114">timnʼa-tɨ</ta>
            <ta e="T116" id="Seg_5274" s="T115">ukkɨr</ta>
            <ta e="T117" id="Seg_5275" s="T116">nälʼa-tɨ</ta>
            <ta e="T118" id="Seg_5276" s="T117">pɛlɨ-kɨtɨ-lʼ</ta>
            <ta e="T119" id="Seg_5277" s="T118">takkɨ-nɨ</ta>
            <ta e="T120" id="Seg_5278" s="T119">nʼennä</ta>
            <ta e="T121" id="Seg_5279" s="T120">šittɨ</ta>
            <ta e="T122" id="Seg_5280" s="T121">qälɨk</ta>
            <ta e="T123" id="Seg_5281" s="T122">tü-ŋɨ</ta>
            <ta e="T124" id="Seg_5282" s="T123">poː-kɨtɨ-lʼ</ta>
            <ta e="T125" id="Seg_5283" s="T124">təttɨ-qɨnɨ</ta>
            <ta e="T126" id="Seg_5284" s="T125">na</ta>
            <ta e="T692" id="Seg_5285" s="T126">qäl</ta>
            <ta e="T127" id="Seg_5286" s="T692">ira</ta>
            <ta e="T128" id="Seg_5287" s="T127">najntɨ</ta>
            <ta e="T129" id="Seg_5288" s="T128">*kušša-lʼ</ta>
            <ta e="T130" id="Seg_5289" s="T129">tamtɨr-tɨ</ta>
            <ta e="T131" id="Seg_5290" s="T130">ɔːtä-m</ta>
            <ta e="T132" id="Seg_5291" s="T131">muntɨk</ta>
            <ta e="T133" id="Seg_5292" s="T132">*taqɨ-š-mpɨ-tɨ</ta>
            <ta e="T134" id="Seg_5293" s="T133">nılʼčʼɨ-k</ta>
            <ta e="T135" id="Seg_5294" s="T134">tü-ŋɨ</ta>
            <ta e="T136" id="Seg_5295" s="T135">na</ta>
            <ta e="T137" id="Seg_5296" s="T136">timnʼa-sɨ-qı</ta>
            <ta e="T138" id="Seg_5297" s="T137">ilɨ-tɨt</ta>
            <ta e="T139" id="Seg_5298" s="T138">na</ta>
            <ta e="T140" id="Seg_5299" s="T139">ira-lʼ</ta>
            <ta e="T141" id="Seg_5300" s="T140">timnʼa-sɨ-ka</ta>
            <ta e="T142" id="Seg_5301" s="T141">šittɨ</ta>
            <ta e="T143" id="Seg_5302" s="T142">tɛːttɨ</ta>
            <ta e="T144" id="Seg_5303" s="T143">toːn</ta>
            <ta e="T145" id="Seg_5304" s="T144">ɔːtä-qı-tɨ</ta>
            <ta e="T146" id="Seg_5305" s="T145">na</ta>
            <ta e="T147" id="Seg_5306" s="T146">ira-n</ta>
            <ta e="T148" id="Seg_5307" s="T147">ɔːtä-m</ta>
            <ta e="T149" id="Seg_5308" s="T148">iː-qɨntoːqo</ta>
            <ta e="T150" id="Seg_5309" s="T149">tü-ntɨ</ta>
            <ta e="T151" id="Seg_5310" s="T150">qum-ɨ-ntɨ-sä</ta>
            <ta e="T152" id="Seg_5311" s="T151">tü-ntɨ</ta>
            <ta e="T153" id="Seg_5312" s="T152">Pan</ta>
            <ta e="T154" id="Seg_5313" s="T153">ira</ta>
            <ta e="T155" id="Seg_5314" s="T154">nım-tɨ</ta>
            <ta e="T156" id="Seg_5315" s="T155">aj</ta>
            <ta e="T157" id="Seg_5316" s="T156">najntɨ</ta>
            <ta e="T158" id="Seg_5317" s="T157">nɨːnɨ</ta>
            <ta e="T159" id="Seg_5318" s="T158">tü-lä</ta>
            <ta e="T160" id="Seg_5319" s="T159">šäqqɨ-tɨt</ta>
            <ta e="T161" id="Seg_5320" s="T160">na</ta>
            <ta e="T162" id="Seg_5321" s="T161">təp-ɨ-t-ɨ-n</ta>
            <ta e="T163" id="Seg_5322" s="T162">mɔːt-qɨn</ta>
            <ta e="T164" id="Seg_5323" s="T163">na</ta>
            <ta e="T165" id="Seg_5324" s="T164">iːja</ta>
            <ta e="T166" id="Seg_5325" s="T165">ašša</ta>
            <ta e="T167" id="Seg_5326" s="T166">tɛnɨmɨ-tɨt</ta>
            <ta e="T168" id="Seg_5327" s="T167">iːja</ta>
            <ta e="T169" id="Seg_5328" s="T168">kuttar</ta>
            <ta e="T170" id="Seg_5329" s="T169">təp-ɨ-nɨŋ</ta>
            <ta e="T171" id="Seg_5330" s="T170">alʼčʼɨ-mpɨ</ta>
            <ta e="T172" id="Seg_5331" s="T171">təp</ta>
            <ta e="T173" id="Seg_5332" s="T172">qaj</ta>
            <ta e="T174" id="Seg_5333" s="T173">muːtɨrona-k</ta>
            <ta e="T175" id="Seg_5334" s="T174">ilɨ</ta>
            <ta e="T176" id="Seg_5335" s="T175">qum</ta>
            <ta e="T177" id="Seg_5336" s="T176">kuttar</ta>
            <ta e="T178" id="Seg_5337" s="T177">tɛnɨmɨ-ɛntɨ-tɨ</ta>
            <ta e="T179" id="Seg_5338" s="T178">alpäj</ta>
            <ta e="T180" id="Seg_5339" s="T179">ilɨ-ntɨlʼ</ta>
            <ta e="T181" id="Seg_5340" s="T180">qum</ta>
            <ta e="T182" id="Seg_5341" s="T181">šäqqɨ-tɨt</ta>
            <ta e="T183" id="Seg_5342" s="T182">mompa</ta>
            <ta e="T184" id="Seg_5343" s="T183">nɔːkɨr-mtälɨlʼ</ta>
            <ta e="T185" id="Seg_5344" s="T184">čʼeːlɨ-n</ta>
            <ta e="T186" id="Seg_5345" s="T185">toːqqɨt</ta>
            <ta e="T187" id="Seg_5346" s="T186">mompa</ta>
            <ta e="T188" id="Seg_5347" s="T187">Pan</ta>
            <ta e="T189" id="Seg_5348" s="T188">ira</ta>
            <ta e="T190" id="Seg_5349" s="T189">köt</ta>
            <ta e="T191" id="Seg_5350" s="T190">qum-sä</ta>
            <ta e="T192" id="Seg_5351" s="T191">tü-ŋɨ</ta>
            <ta e="T193" id="Seg_5352" s="T192">qapı</ta>
            <ta e="T194" id="Seg_5353" s="T193">ɔːtä-m</ta>
            <ta e="T195" id="Seg_5354" s="T194">tɔːqqɨ-qɨntoːqo</ta>
            <ta e="T196" id="Seg_5355" s="T195">na</ta>
            <ta e="T197" id="Seg_5356" s="T196">tına</ta>
            <ta e="T198" id="Seg_5357" s="T197">əsɨ-sɨ-t-ɨ-n</ta>
            <ta e="T199" id="Seg_5358" s="T198">ɔːtä-m</ta>
            <ta e="T200" id="Seg_5359" s="T199">tɔːqqɨ-qo</ta>
            <ta e="T201" id="Seg_5360" s="T200">*ku-n</ta>
            <ta e="T202" id="Seg_5361" s="T201">mɔːntɨ-lʼ</ta>
            <ta e="T203" id="Seg_5362" s="T202">mɔːt-tɨ</ta>
            <ta e="T204" id="Seg_5363" s="T203">šentɨ</ta>
            <ta e="T205" id="Seg_5364" s="T204">*ku-n</ta>
            <ta e="T206" id="Seg_5365" s="T205">mɔːntɨ-lʼ</ta>
            <ta e="T207" id="Seg_5366" s="T206">qaj-tɨ</ta>
            <ta e="T208" id="Seg_5367" s="T207">muntɨk</ta>
            <ta e="T209" id="Seg_5368" s="T208">*taqɨ-qɨl-ɛntɨ-tɨ</ta>
            <ta e="T210" id="Seg_5369" s="T209">ukoːn</ta>
            <ta e="T211" id="Seg_5370" s="T210">nɔːtɨ</ta>
            <ta e="T212" id="Seg_5371" s="T211">*taqɨ-qɨl-kolʼčʼimpɨ-tɨ</ta>
            <ta e="T213" id="Seg_5372" s="T212">qälɨk-t-ɨ-n</ta>
            <ta e="T214" id="Seg_5373" s="T213">ɔːtä-m</ta>
            <ta e="T215" id="Seg_5374" s="T214">*taqɨ-qɨl-lä</ta>
            <ta e="T216" id="Seg_5375" s="T215">iː-mpɨ-tɨ</ta>
            <ta e="T217" id="Seg_5376" s="T216">nɨːnɨ</ta>
            <ta e="T218" id="Seg_5377" s="T217">šittälʼ</ta>
            <ta e="T219" id="Seg_5378" s="T218">mɨŋa</ta>
            <ta e="T220" id="Seg_5379" s="T219">moqɨnä</ta>
            <ta e="T221" id="Seg_5380" s="T220">qən-ŋɨ-qı</ta>
            <ta e="T222" id="Seg_5381" s="T221">na</ta>
            <ta e="T223" id="Seg_5382" s="T222">qälɨk-qı</ta>
            <ta e="T224" id="Seg_5383" s="T223">nɔːkɨr-mtälɨlʼ</ta>
            <ta e="T225" id="Seg_5384" s="T224">čʼeːlɨ-n</ta>
            <ta e="T226" id="Seg_5385" s="T225">mompa</ta>
            <ta e="T227" id="Seg_5386" s="T226">tü-ɛntɨ-mɨt</ta>
            <ta e="T228" id="Seg_5387" s="T227">tɛː</ta>
            <ta e="T229" id="Seg_5388" s="T228">mompa</ta>
            <ta e="T230" id="Seg_5389" s="T229">kučʼčʼä</ta>
            <ta e="T231" id="Seg_5390" s="T230">ɨkɨ</ta>
            <ta e="T232" id="Seg_5391" s="T231">qən-ŋɨlɨt</ta>
            <ta e="T233" id="Seg_5392" s="T232">šittɨ-mtälɨlʼ</ta>
            <ta e="T234" id="Seg_5393" s="T233">čʼeːlɨ-ntɨ</ta>
            <ta e="T235" id="Seg_5394" s="T234">ınnä</ta>
            <ta e="T236" id="Seg_5395" s="T235">čʼeːlɨ-m-ŋɨ</ta>
            <ta e="T237" id="Seg_5396" s="T236">na</ta>
            <ta e="T238" id="Seg_5397" s="T237">iːja</ta>
            <ta e="T239" id="Seg_5398" s="T238">nılʼčʼɨ-lʼ</ta>
            <ta e="T240" id="Seg_5399" s="T239">kətɨ-ntɨ-tɨ</ta>
            <ta e="T241" id="Seg_5400" s="T240">ilʼčʼa-ntɨ-nɨŋ</ta>
            <ta e="T242" id="Seg_5401" s="T241">mɨta</ta>
            <ta e="T243" id="Seg_5402" s="T242">nʼennä-n</ta>
            <ta e="T244" id="Seg_5403" s="T243">tɨmtɨ</ta>
            <ta e="T245" id="Seg_5404" s="T244">tɛːttɨ</ta>
            <ta e="T246" id="Seg_5405" s="T245">toːn</ta>
            <ta e="T247" id="Seg_5406" s="T246">ɔːtä-t</ta>
            <ta e="T248" id="Seg_5407" s="T247">nɨŋ-kkɨ-ku-ššak-lʼ</ta>
            <ta e="T249" id="Seg_5408" s="T248">nʼarɨ-mɨ</ta>
            <ta e="T250" id="Seg_5409" s="T249">ɛː-ntɨ</ta>
            <ta e="T251" id="Seg_5410" s="T250">näčʼčʼä</ta>
            <ta e="T252" id="Seg_5411" s="T251">üːtɨ-lɨ-mɨt</ta>
            <ta e="T253" id="Seg_5412" s="T252">qälɨk-t</ta>
            <ta e="T254" id="Seg_5413" s="T253">tü-ɛntɨ-tɨt</ta>
            <ta e="T255" id="Seg_5414" s="T254">ɔːtä-m</ta>
            <ta e="T256" id="Seg_5415" s="T255">tɔːqqɨ-qo-olam-ɛntɨ-tɨt</ta>
            <ta e="T257" id="Seg_5416" s="T256">ɔːtä-n</ta>
            <ta e="T258" id="Seg_5417" s="T257">up-qɨn</ta>
            <ta e="T259" id="Seg_5418" s="T258">kə-lʼčʼo-mə-n-tɨ</ta>
            <ta e="T260" id="Seg_5419" s="T259">kuntɨ</ta>
            <ta e="T261" id="Seg_5420" s="T260">kə-rɨ-mpɨlʼ</ta>
            <ta e="T262" id="Seg_5421" s="T261">kuttar</ta>
            <ta e="T263" id="Seg_5422" s="T262">meː-ɛntɨ-mɨt</ta>
            <ta e="T264" id="Seg_5423" s="T263">na</ta>
            <ta e="T265" id="Seg_5424" s="T264">iːja</ta>
            <ta e="T266" id="Seg_5425" s="T265">nık</ta>
            <ta e="T267" id="Seg_5426" s="T266">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T268" id="Seg_5427" s="T267">nɨːnɨ</ta>
            <ta e="T269" id="Seg_5428" s="T268">nık</ta>
            <ta e="T270" id="Seg_5429" s="T269">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T271" id="Seg_5430" s="T270">mäkkä</ta>
            <ta e="T272" id="Seg_5431" s="T271">mɨta</ta>
            <ta e="T273" id="Seg_5432" s="T272">šentɨ</ta>
            <ta e="T274" id="Seg_5433" s="T273">mɔːt-lɨt</ta>
            <ta e="T275" id="Seg_5434" s="T274">ɛː-ŋɨ</ta>
            <ta e="T276" id="Seg_5435" s="T275">ilʼčʼa</ta>
            <ta e="T277" id="Seg_5436" s="T276">mäkkä</ta>
            <ta e="T278" id="Seg_5437" s="T277">mi-ätɨ</ta>
            <ta e="T279" id="Seg_5438" s="T278">a</ta>
            <ta e="T280" id="Seg_5439" s="T279">toːnna</ta>
            <ta e="T281" id="Seg_5440" s="T280">tına</ta>
            <ta e="T282" id="Seg_5441" s="T281">timnʼa-n-tɨ</ta>
            <ta e="T283" id="Seg_5442" s="T282">nälʼa</ta>
            <ta e="T284" id="Seg_5443" s="T283">ilʼčʼa-tɨ</ta>
            <ta e="T285" id="Seg_5444" s="T284">na-tɨ-m</ta>
            <ta e="T286" id="Seg_5445" s="T285">*kurɨ-altɨ-mpɨ-tɨ</ta>
            <ta e="T287" id="Seg_5446" s="T286">na</ta>
            <ta e="T289" id="Seg_5447" s="T287">iːja-nɨŋ</ta>
            <ta e="T290" id="Seg_5448" s="T289">nʼennä-n</ta>
            <ta e="T291" id="Seg_5449" s="T290">tɨmtɨ</ta>
            <ta e="T292" id="Seg_5450" s="T291">mačʼɨ-lʼ</ta>
            <ta e="T293" id="Seg_5451" s="T292">tıčʼɨ</ta>
            <ta e="T294" id="Seg_5452" s="T293">ɛː-ŋɨ</ta>
            <ta e="T295" id="Seg_5453" s="T294">nɨmtɨ</ta>
            <ta e="T296" id="Seg_5454" s="T295">mɔːt-ɨ-k-ɛntɨ-k</ta>
            <ta e="T297" id="Seg_5455" s="T296">šentɨ</ta>
            <ta e="T298" id="Seg_5456" s="T297">mɔːt</ta>
            <ta e="T299" id="Seg_5457" s="T298">taːtɨ-ŋɨt</ta>
            <ta e="T300" id="Seg_5458" s="T299">mɨta</ta>
            <ta e="T301" id="Seg_5459" s="T300">üːtɨ-äl-ŋɨ-tɨt</ta>
            <ta e="T302" id="Seg_5460" s="T301">täːlɨ</ta>
            <ta e="T303" id="Seg_5461" s="T302">qapı</ta>
            <ta e="T304" id="Seg_5462" s="T303">na</ta>
            <ta e="T305" id="Seg_5463" s="T304">nɔːkɨr-mtälɨlʼ</ta>
            <ta e="T306" id="Seg_5464" s="T305">čʼeːlɨ</ta>
            <ta e="T307" id="Seg_5465" s="T306">tü-ɛntɨ-tɨt</ta>
            <ta e="T308" id="Seg_5466" s="T307">Pan</ta>
            <ta e="T309" id="Seg_5467" s="T308">ira-lʼ-mɨ-t</ta>
            <ta e="T310" id="Seg_5468" s="T309">təp-ɨ-t-ɨ-nkinı</ta>
            <ta e="T311" id="Seg_5469" s="T310">na</ta>
            <ta e="T312" id="Seg_5470" s="T311">qäl_ira-n</ta>
            <ta e="T313" id="Seg_5471" s="T312">ɔːtä-m</ta>
            <ta e="T314" id="Seg_5472" s="T313">*taqɨ-qɨl-qo</ta>
            <ta e="T315" id="Seg_5473" s="T314">timnʼa-sɨ-qı-n</ta>
            <ta e="T316" id="Seg_5474" s="T315">ɔːtä-m</ta>
            <ta e="T317" id="Seg_5475" s="T316">nɨːnɨ</ta>
            <ta e="T318" id="Seg_5476" s="T317">šittɨ-lʼ</ta>
            <ta e="T319" id="Seg_5477" s="T318">üːtɨ-äl-ntɨ-tɨt</ta>
            <ta e="T320" id="Seg_5478" s="T319">na</ta>
            <ta e="T321" id="Seg_5479" s="T320">ɔːtä-m</ta>
            <ta e="T322" id="Seg_5480" s="T321">na</ta>
            <ta e="T323" id="Seg_5481" s="T322">tɔːqqɨ-ntɨ-tɨt</ta>
            <ta e="T324" id="Seg_5482" s="T323">qapı</ta>
            <ta e="T325" id="Seg_5483" s="T324">təp</ta>
            <ta e="T326" id="Seg_5484" s="T325">aj</ta>
            <ta e="T327" id="Seg_5485" s="T326">tɔːqqɨ-mpɨ-tɨ</ta>
            <ta e="T328" id="Seg_5486" s="T327">mɔːt-sä</ta>
            <ta e="T329" id="Seg_5487" s="T328">na</ta>
            <ta e="T330" id="Seg_5488" s="T329">mi-ntɨ-tɨt</ta>
            <ta e="T331" id="Seg_5489" s="T330">na</ta>
            <ta e="T332" id="Seg_5490" s="T331">mačʼɨ-n</ta>
            <ta e="T333" id="Seg_5491" s="T332">tıčʼɨ-n</ta>
            <ta e="T334" id="Seg_5492" s="T333">nʼennä-lʼ</ta>
            <ta e="T335" id="Seg_5493" s="T334">pɛläk-qɨn</ta>
            <ta e="T336" id="Seg_5494" s="T335">nɨːnɨ</ta>
            <ta e="T337" id="Seg_5495" s="T336">mɔːt-ɨ-k-ntɨ-tɨt</ta>
            <ta e="T338" id="Seg_5496" s="T337">na</ta>
            <ta e="T339" id="Seg_5497" s="T338">ima-ntɨ-sä</ta>
            <ta e="T340" id="Seg_5498" s="T339">qapı</ta>
            <ta e="T341" id="Seg_5499" s="T340">tətta-lʼ</ta>
            <ta e="T342" id="Seg_5500" s="T341">qälɨk</ta>
            <ta e="T343" id="Seg_5501" s="T342">qaj</ta>
            <ta e="T344" id="Seg_5502" s="T343">mɔːt-tɨ</ta>
            <ta e="T345" id="Seg_5503" s="T344">čʼäːŋkɨ</ta>
            <ta e="T346" id="Seg_5504" s="T345">šentɨ</ta>
            <ta e="T347" id="Seg_5505" s="T346">mɔːt-tɨ</ta>
            <ta e="T348" id="Seg_5506" s="T347">qopɨ-lʼ</ta>
            <ta e="T349" id="Seg_5507" s="T348">mɔːt-tɨ</ta>
            <ta e="T350" id="Seg_5508" s="T349">nɨːnɨ</ta>
            <ta e="T351" id="Seg_5509" s="T350">šittɨ-mtäl</ta>
            <ta e="T352" id="Seg_5510" s="T351">šäqqɨ-tɨt</ta>
            <ta e="T353" id="Seg_5511" s="T352">na</ta>
            <ta e="T354" id="Seg_5512" s="T353">ima-ntɨ-sä</ta>
            <ta e="T355" id="Seg_5513" s="T354">tɔːptɨlʼ</ta>
            <ta e="T356" id="Seg_5514" s="T355">qarɨ-ntɨ</ta>
            <ta e="T357" id="Seg_5515" s="T356">nılʼčʼɨ-k</ta>
            <ta e="T358" id="Seg_5516" s="T357">ɛsɨ</ta>
            <ta e="T359" id="Seg_5517" s="T358">tına</ta>
            <ta e="T360" id="Seg_5518" s="T359">ima-ntɨ-nɨŋ</ta>
            <ta e="T361" id="Seg_5519" s="T360">man</ta>
            <ta e="T362" id="Seg_5520" s="T361">mɨta</ta>
            <ta e="T363" id="Seg_5521" s="T362">nʼennä</ta>
            <ta e="T364" id="Seg_5522" s="T363">qum-iː-qäk</ta>
            <ta e="T365" id="Seg_5523" s="T364">*qonı-lä-k</ta>
            <ta e="T366" id="Seg_5524" s="T365">qaj</ta>
            <ta e="T367" id="Seg_5525" s="T366">mɨta</ta>
            <ta e="T368" id="Seg_5526" s="T367">mɔːt-ɨ-k-mpɨ-tɨt</ta>
            <ta e="T369" id="Seg_5527" s="T368">qaj</ta>
            <ta e="T370" id="Seg_5528" s="T369">qattɨ-tɨt</ta>
            <ta e="T371" id="Seg_5529" s="T370">nʼennä</ta>
            <ta e="T372" id="Seg_5530" s="T371">laqɨ-altɨ-lʼčʼɨ</ta>
            <ta e="T373" id="Seg_5531" s="T372">nɔːkɨr</ta>
            <ta e="T374" id="Seg_5532" s="T373">qoptɨ-m</ta>
            <ta e="T375" id="Seg_5533" s="T374">sɔːrɨ-lä</ta>
            <ta e="T376" id="Seg_5534" s="T375">qapı-ja</ta>
            <ta e="T377" id="Seg_5535" s="T376">nɔːkɨr-sar-ɨ-lʼ</ta>
            <ta e="T378" id="Seg_5536" s="T377">ɔːtä-m</ta>
            <ta e="T379" id="Seg_5537" s="T378">qalɨ-tɨ-mpɨ-ntɨ-tɨ</ta>
            <ta e="T380" id="Seg_5538" s="T379">iːja</ta>
            <ta e="T381" id="Seg_5539" s="T380">nʼennä</ta>
            <ta e="T382" id="Seg_5540" s="T381">qən-ɛː-ŋɨ</ta>
            <ta e="T383" id="Seg_5541" s="T382">ima-tɨ</ta>
            <ta e="T384" id="Seg_5542" s="T383">ontɨ</ta>
            <ta e="T385" id="Seg_5543" s="T384">qalɨ</ta>
            <ta e="T386" id="Seg_5544" s="T385">na</ta>
            <ta e="T387" id="Seg_5545" s="T386">omtɨ</ta>
            <ta e="T388" id="Seg_5546" s="T387">na</ta>
            <ta e="T389" id="Seg_5547" s="T388">omtɨ</ta>
            <ta e="T390" id="Seg_5548" s="T389">ukkɨr</ta>
            <ta e="T391" id="Seg_5549" s="T390">to-n</ta>
            <ta e="T392" id="Seg_5550" s="T391">čʼontɨ-qɨn</ta>
            <ta e="T393" id="Seg_5551" s="T392">čʼeːlɨ-tɨ</ta>
            <ta e="T394" id="Seg_5552" s="T393">üːtɨ-ntɨ</ta>
            <ta e="T395" id="Seg_5553" s="T394">na</ta>
            <ta e="T396" id="Seg_5554" s="T395">qən-ŋɨ</ta>
            <ta e="T397" id="Seg_5555" s="T396">na</ta>
            <ta e="T398" id="Seg_5556" s="T397">iːja</ta>
            <ta e="T399" id="Seg_5557" s="T398">*lʼamɨ-k</ta>
            <ta e="T400" id="Seg_5558" s="T399">kə-ŋɨ</ta>
            <ta e="T401" id="Seg_5559" s="T400">nʼennä-n</ta>
            <ta e="T402" id="Seg_5560" s="T401">ukkɨr</ta>
            <ta e="T403" id="Seg_5561" s="T402">to-n</ta>
            <ta e="T404" id="Seg_5562" s="T403">čʼontɨ-qɨn</ta>
            <ta e="T405" id="Seg_5563" s="T404">qum</ta>
            <ta e="T406" id="Seg_5564" s="T405">na-ja-m</ta>
            <ta e="T407" id="Seg_5565" s="T406">taːtɨ-ntɨ-tɨt</ta>
            <ta e="T408" id="Seg_5566" s="T407">takkɨ-nɨ</ta>
            <ta e="T409" id="Seg_5567" s="T408">Pan</ta>
            <ta e="T410" id="Seg_5568" s="T409">ira-lʼ-mɨ-t</ta>
            <ta e="T411" id="Seg_5569" s="T410">taːtɨ-ntɨ-tɨt</ta>
            <ta e="T412" id="Seg_5570" s="T411">köt</ta>
            <ta e="T413" id="Seg_5571" s="T412">qum-tɨ</ta>
            <ta e="T414" id="Seg_5572" s="T413">qapı-ja</ta>
            <ta e="T415" id="Seg_5573" s="T414">ɔːtä-m</ta>
            <ta e="T416" id="Seg_5574" s="T415">tɔːqqɨ-psɔːt-ntɨlʼ</ta>
            <ta e="T417" id="Seg_5575" s="T416">Pan</ta>
            <ta e="T418" id="Seg_5576" s="T417">ira</ta>
            <ta e="T419" id="Seg_5577" s="T418">naššak</ta>
            <ta e="T420" id="Seg_5578" s="T419">qəːtɨ-sä</ta>
            <ta e="T421" id="Seg_5579" s="T420">ɛː-ŋɨ</ta>
            <ta e="T422" id="Seg_5580" s="T421">nɨːnɨ</ta>
            <ta e="T423" id="Seg_5581" s="T422">šittɨ-mtäl</ta>
            <ta e="T424" id="Seg_5582" s="T423">na</ta>
            <ta e="T425" id="Seg_5583" s="T424">iːja</ta>
            <ta e="T426" id="Seg_5584" s="T425">tü-lʼčʼɨ-kunä</ta>
            <ta e="T427" id="Seg_5585" s="T426">ima-tɨ</ta>
            <ta e="T428" id="Seg_5586" s="T427">üŋkɨl-tɨ-mpɨ-tɨ</ta>
            <ta e="T429" id="Seg_5587" s="T428">montɨ</ta>
            <ta e="T430" id="Seg_5588" s="T429">čʼam</ta>
            <ta e="T431" id="Seg_5589" s="T430">tü-lʼčʼɨ</ta>
            <ta e="T432" id="Seg_5590" s="T431">Pan</ta>
            <ta e="T433" id="Seg_5591" s="T432">ira-lʼ-mɨ-t</ta>
            <ta e="T434" id="Seg_5592" s="T433">qaj</ta>
            <ta e="T435" id="Seg_5593" s="T434">montɨ</ta>
            <ta e="T436" id="Seg_5594" s="T435">tü-ŋɨ-tɨt</ta>
            <ta e="T437" id="Seg_5595" s="T436">qaqlɨ-lʼ</ta>
            <ta e="T438" id="Seg_5596" s="T437">mɨ-tɨ</ta>
            <ta e="T439" id="Seg_5597" s="T438">nılʼčʼɨ-k</ta>
            <ta e="T440" id="Seg_5598" s="T439">tottɨ-äl-ŋɨ-tɨt</ta>
            <ta e="T441" id="Seg_5599" s="T440">poː-qɨn</ta>
            <ta e="T442" id="Seg_5600" s="T441">ətɨmantɨ-qɨn</ta>
            <ta e="T443" id="Seg_5601" s="T442">mɔːt-ntɨ</ta>
            <ta e="T444" id="Seg_5602" s="T443">šeːr-ŋɨ</ta>
            <ta e="T445" id="Seg_5603" s="T444">mɔːta-n</ta>
            <ta e="T446" id="Seg_5604" s="T445">ɔːŋ-n</ta>
            <ta e="T447" id="Seg_5605" s="T446">nɨŋ-kkɨ-lä</ta>
            <ta e="T448" id="Seg_5606" s="T447">mɔːta-n</ta>
            <ta e="T449" id="Seg_5607" s="T448">ɔːŋ-k</ta>
            <ta e="T450" id="Seg_5608" s="T449">keː</ta>
            <ta e="T451" id="Seg_5609" s="T450">topɨ-m-tɨ</ta>
            <ta e="T452" id="Seg_5610" s="T451">*tupɨ-ätɔːl-kkɨ-tɨ</ta>
            <ta e="T453" id="Seg_5611" s="T452">peːmɨ-m-tɨ</ta>
            <ta e="T454" id="Seg_5612" s="T453">na</ta>
            <ta e="T455" id="Seg_5613" s="T454">*tupɨ-ätɔːl-ntɨ-tɨ</ta>
            <ta e="T456" id="Seg_5614" s="T455">nık</ta>
            <ta e="T457" id="Seg_5615" s="T456">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T458" id="Seg_5616" s="T457">Pan</ta>
            <ta e="T459" id="Seg_5617" s="T458">ira-nɨŋ</ta>
            <ta e="T460" id="Seg_5618" s="T459">nʼennä</ta>
            <ta e="T461" id="Seg_5619" s="T460">*korɨ-altɨ-lä</ta>
            <ta e="T462" id="Seg_5620" s="T461">ɔːŋ-lɨ</ta>
            <ta e="T463" id="Seg_5621" s="T462">mitɨ</ta>
            <ta e="T464" id="Seg_5622" s="T463">olä</ta>
            <ta e="T465" id="Seg_5623" s="T464">qoltɨ-n</ta>
            <ta e="T466" id="Seg_5624" s="T465">qəqqɨ-n</ta>
            <ta e="T467" id="Seg_5625" s="T466">olä</ta>
            <ta e="T468" id="Seg_5626" s="T467">täːlɨ</ta>
            <ta e="T469" id="Seg_5627" s="T468">na</ta>
            <ta e="T470" id="Seg_5628" s="T469">nʼennä</ta>
            <ta e="T471" id="Seg_5629" s="T470">paktɨ-mpɨ</ta>
            <ta e="T472" id="Seg_5630" s="T471">na</ta>
            <ta e="T473" id="Seg_5631" s="T472">kupak-sä</ta>
            <ta e="T474" id="Seg_5632" s="T473">təːqa-ätɔːl-ɛː-mpɨ-ntɨ-tɨ</ta>
            <ta e="T475" id="Seg_5633" s="T474">Pan</ta>
            <ta e="T476" id="Seg_5634" s="T475">ira-lʼ</ta>
            <ta e="T477" id="Seg_5635" s="T476">mɨ-t-ɨ-m</ta>
            <ta e="T478" id="Seg_5636" s="T477">muntɨk</ta>
            <ta e="T479" id="Seg_5637" s="T478">läpäŋ</ta>
            <ta e="T480" id="Seg_5638" s="T479">qättɨ-mpɨ-tɨ</ta>
            <ta e="T481" id="Seg_5639" s="T480">ɔːmɨ-tɨ</ta>
            <ta e="T482" id="Seg_5640" s="T481">qu-mpɨ</ta>
            <ta e="T483" id="Seg_5641" s="T482">Pan</ta>
            <ta e="T484" id="Seg_5642" s="T483">ira</ta>
            <ta e="T485" id="Seg_5643" s="T484">ilɨ-lä</ta>
            <ta e="T486" id="Seg_5644" s="T485">qalɨ-mpɨ</ta>
            <ta e="T487" id="Seg_5645" s="T486">šittɨ-qı</ta>
            <ta e="T488" id="Seg_5646" s="T487">ilɨ-lä</ta>
            <ta e="T489" id="Seg_5647" s="T488">qalɨ-mpɨ-ntɨ</ta>
            <ta e="T490" id="Seg_5648" s="T489">ponä</ta>
            <ta e="T491" id="Seg_5649" s="T490">qəː-äl-ɛː-tɨ</ta>
            <ta e="T492" id="Seg_5650" s="T491">na</ta>
            <ta e="T493" id="Seg_5651" s="T492">iːja</ta>
            <ta e="T494" id="Seg_5652" s="T493">ukkɨr</ta>
            <ta e="T495" id="Seg_5653" s="T494">pɔːrɨ</ta>
            <ta e="T496" id="Seg_5654" s="T495">kupak-sä</ta>
            <ta e="T497" id="Seg_5655" s="T496">təːqa-ätɔːl-ɛː-mpɨ-tɨ</ta>
            <ta e="T498" id="Seg_5656" s="T497">muntɨk</ta>
            <ta e="T499" id="Seg_5657" s="T498">porqɨ-n-tɨ</ta>
            <ta e="T500" id="Seg_5658" s="T499">šünʼčʼɨ-ntɨ</ta>
            <ta e="T501" id="Seg_5659" s="T500">pimmɨ-n-tɨ</ta>
            <ta e="T502" id="Seg_5660" s="T501">šüː</ta>
            <ta e="T503" id="Seg_5661" s="T502">nʼölʼqɨ-mɔːt-mpɨ-tɨt</ta>
            <ta e="T504" id="Seg_5662" s="T503">na</ta>
            <ta e="T505" id="Seg_5663" s="T504">qu-mpɨlʼ-mɨ-t</ta>
            <ta e="T506" id="Seg_5664" s="T505">nɨːnɨ</ta>
            <ta e="T507" id="Seg_5665" s="T506">šittälʼ</ta>
            <ta e="T508" id="Seg_5666" s="T507">ponä</ta>
            <ta e="T509" id="Seg_5667" s="T508">qəː-äl-ɛː-tɨ</ta>
            <ta e="T510" id="Seg_5668" s="T509">ətɨmantɨ-ntɨ</ta>
            <ta e="T511" id="Seg_5669" s="T510">tına</ta>
            <ta e="T512" id="Seg_5670" s="T511">qaqlɨ</ta>
            <ta e="T513" id="Seg_5671" s="T512">türɨ</ta>
            <ta e="T514" id="Seg_5672" s="T513">tolʼčʼɨ-tɨ</ta>
            <ta e="T515" id="Seg_5673" s="T514">na</ta>
            <ta e="T516" id="Seg_5674" s="T515">sɔːrɨ-qɨlɨ-mpɨ</ta>
            <ta e="T517" id="Seg_5675" s="T516">Pan</ta>
            <ta e="T518" id="Seg_5676" s="T517">ira-m</ta>
            <ta e="T519" id="Seg_5677" s="T518">aj</ta>
            <ta e="T520" id="Seg_5678" s="T519">ponä</ta>
            <ta e="T521" id="Seg_5679" s="T520">iː-tɨ-mpɨ-tɨ</ta>
            <ta e="T522" id="Seg_5680" s="T521">čʼattɨ-mpɨ-tɨ</ta>
            <ta e="T523" id="Seg_5681" s="T522">qapı</ta>
            <ta e="T524" id="Seg_5682" s="T523">muntɨk</ta>
            <ta e="T525" id="Seg_5683" s="T524">ponä</ta>
            <ta e="T526" id="Seg_5684" s="T525">iː-tɨ-mpɨ-tɨ</ta>
            <ta e="T527" id="Seg_5685" s="T526">ıllä</ta>
            <ta e="T528" id="Seg_5686" s="T527">omtɨ</ta>
            <ta e="T529" id="Seg_5687" s="T528">malʼčʼä-lʼ</ta>
            <ta e="T530" id="Seg_5688" s="T529">tɔːq-tɨ</ta>
            <ta e="T531" id="Seg_5689" s="T530">toː</ta>
            <ta e="T532" id="Seg_5690" s="T531">iː-lä</ta>
            <ta e="T533" id="Seg_5691" s="T532">am-ɨ-r-lä</ta>
            <ta e="T534" id="Seg_5692" s="T533">ıllä</ta>
            <ta e="T535" id="Seg_5693" s="T534">omtɨ-mpɨ-ntɨ</ta>
            <ta e="T536" id="Seg_5694" s="T535">šäqqɨ</ta>
            <ta e="T537" id="Seg_5695" s="T536">na</ta>
            <ta e="T538" id="Seg_5696" s="T537">qum-iː-tɨ</ta>
            <ta e="T539" id="Seg_5697" s="T538">poː-qɨn</ta>
            <ta e="T540" id="Seg_5698" s="T539">na</ta>
            <ta e="T541" id="Seg_5699" s="T540">ippɨ-qɨlɨ-mpɨ-tɨt</ta>
            <ta e="T542" id="Seg_5700" s="T541">kuttar</ta>
            <ta e="T543" id="Seg_5701" s="T542">sɔːrɨ-alɨ-mpɨ</ta>
            <ta e="T544" id="Seg_5702" s="T543">nılʼčʼɨ-k</ta>
            <ta e="T545" id="Seg_5703" s="T544">šäqqɨ</ta>
            <ta e="T546" id="Seg_5704" s="T545">tɔːptɨlʼ</ta>
            <ta e="T547" id="Seg_5705" s="T546">qarɨ-ntɨ</ta>
            <ta e="T548" id="Seg_5706" s="T547">ınnä</ta>
            <ta e="T549" id="Seg_5707" s="T548">wəšɨ</ta>
            <ta e="T550" id="Seg_5708" s="T549">Pan</ta>
            <ta e="T551" id="Seg_5709" s="T550">ira</ta>
            <ta e="T552" id="Seg_5710" s="T551">nılʼčʼɨ-k</ta>
            <ta e="T553" id="Seg_5711" s="T552">tam</ta>
            <ta e="T554" id="Seg_5712" s="T553">wəntɨ-lʼ</ta>
            <ta e="T555" id="Seg_5713" s="T554">tɔːŋ-tɨ</ta>
            <ta e="T556" id="Seg_5714" s="T555">tɔːt-ɨ-k</ta>
            <ta e="T557" id="Seg_5715" s="T556">taŋkɨ-ıː-mpɨ</ta>
            <ta e="T558" id="Seg_5716" s="T557">kekkɨsä</ta>
            <ta e="T559" id="Seg_5717" s="T558">sajɨ-lʼa-tɨ</ta>
            <ta e="T560" id="Seg_5718" s="T559">qo-čʼɨ-mpɨ</ta>
            <ta e="T561" id="Seg_5719" s="T560">toːnna</ta>
            <ta e="T562" id="Seg_5720" s="T561">qälɨk-qı-tɨ</ta>
            <ta e="T563" id="Seg_5721" s="T562">ešo</ta>
            <ta e="T564" id="Seg_5722" s="T563">soma-lɔːqɨ</ta>
            <ta e="T565" id="Seg_5723" s="T564">ɛː-mpɨ-mpɨ</ta>
            <ta e="T566" id="Seg_5724" s="T565">nılʼčʼɨ-lʼ</ta>
            <ta e="T567" id="Seg_5725" s="T566">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T568" id="Seg_5726" s="T567">na</ta>
            <ta e="T569" id="Seg_5727" s="T568">iːja</ta>
            <ta e="T570" id="Seg_5728" s="T569">mɨta</ta>
            <ta e="T571" id="Seg_5729" s="T570">na</ta>
            <ta e="T572" id="Seg_5730" s="T571">qälɨk-t-ɨ-nkinı</ta>
            <ta e="T573" id="Seg_5731" s="T572">moqɨnä</ta>
            <ta e="T574" id="Seg_5732" s="T573">qən-tɨ-tɨt</ta>
            <ta e="T575" id="Seg_5733" s="T574">talʼtälʼlʼä</ta>
            <ta e="T576" id="Seg_5734" s="T575">ontɨt</ta>
            <ta e="T577" id="Seg_5735" s="T576">qaqlɨ-ntɨ-sä</ta>
            <ta e="T578" id="Seg_5736" s="T577">nɨːnɨ</ta>
            <ta e="T579" id="Seg_5737" s="T578">šittɨ-mtäl</ta>
            <ta e="T580" id="Seg_5738" s="T579">toːnna</ta>
            <ta e="T581" id="Seg_5739" s="T580">meː-qı</ta>
            <ta e="T582" id="Seg_5740" s="T581">mɔːt-ntɨ</ta>
            <ta e="T583" id="Seg_5741" s="T582">šeːr-ɨ-š-mpɨ-tɨt</ta>
            <ta e="T584" id="Seg_5742" s="T583">qaj</ta>
            <ta e="T585" id="Seg_5743" s="T584">am-ɨ-r-mpɨ-tɨt</ta>
            <ta e="T586" id="Seg_5744" s="T585">qaj</ta>
            <ta e="T587" id="Seg_5745" s="T586">ašša</ta>
            <ta e="T588" id="Seg_5746" s="T587">nɨːnɨ</ta>
            <ta e="T589" id="Seg_5747" s="T588">tına</ta>
            <ta e="T590" id="Seg_5748" s="T589">*sar-ätɔːl-mpɨ-ntɨlʼ</ta>
            <ta e="T591" id="Seg_5749" s="T590">qaqlɨ-m-tɨt</ta>
            <ta e="T592" id="Seg_5750" s="T591">nɨːnɨ</ta>
            <ta e="T593" id="Seg_5751" s="T592">moqɨnä</ta>
            <ta e="T594" id="Seg_5752" s="T593">qəːčʼelʼ-lä</ta>
            <ta e="T595" id="Seg_5753" s="T594">na</ta>
            <ta e="T596" id="Seg_5754" s="T595">qum-iː-m-tɨt</ta>
            <ta e="T597" id="Seg_5755" s="T596">qu-mpɨlʼ</ta>
            <ta e="T598" id="Seg_5756" s="T597">qum-iː-m-tɨ</ta>
            <ta e="T599" id="Seg_5757" s="T598">nık</ta>
            <ta e="T600" id="Seg_5758" s="T599">qən-tɨ-ŋɨ-tɨ</ta>
            <ta e="T601" id="Seg_5759" s="T600">takkɨ</ta>
            <ta e="T602" id="Seg_5760" s="T601">moqɨnä</ta>
            <ta e="T603" id="Seg_5761" s="T602">Pan</ta>
            <ta e="T604" id="Seg_5762" s="T603">ira-m-tɨ</ta>
            <ta e="T605" id="Seg_5763" s="T604">aj</ta>
            <ta e="T606" id="Seg_5764" s="T605">qəːčʼelʼ-lä</ta>
            <ta e="T607" id="Seg_5765" s="T606">qən-tɨ-tɨ</ta>
            <ta e="T608" id="Seg_5766" s="T607">qaj</ta>
            <ta e="T609" id="Seg_5767" s="T608">kekkɨsä</ta>
            <ta e="T611" id="Seg_5768" s="T610">qən-tɨ-tɨ</ta>
            <ta e="T612" id="Seg_5769" s="T611">qən-ŋɨ-tɨt</ta>
            <ta e="T613" id="Seg_5770" s="T612">tına</ta>
            <ta e="T614" id="Seg_5771" s="T613">qum-ɨ-t</ta>
            <ta e="T615" id="Seg_5772" s="T614">soma-k</ta>
            <ta e="T616" id="Seg_5773" s="T615">qən-ŋɨ-tɨt</ta>
            <ta e="T617" id="Seg_5774" s="T616">ima-ntɨ-nɨŋ</ta>
            <ta e="T618" id="Seg_5775" s="T617">nık</ta>
            <ta e="T619" id="Seg_5776" s="T618">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T620" id="Seg_5777" s="T619">mɔːt-lɨ</ta>
            <ta e="T621" id="Seg_5778" s="T620">toː</ta>
            <ta e="T622" id="Seg_5779" s="T621">tılɨ-ätɨ</ta>
            <ta e="T623" id="Seg_5780" s="T622">meː</ta>
            <ta e="T624" id="Seg_5781" s="T623">nʼennä</ta>
            <ta e="T625" id="Seg_5782" s="T624">qən-lä</ta>
            <ta e="T626" id="Seg_5783" s="T625">qum-ɨ-t-ɨ-nkinı</ta>
            <ta e="T627" id="Seg_5784" s="T626">ašša</ta>
            <ta e="T628" id="Seg_5785" s="T627">tɛnɨmɨ-mpɨ-tɨ</ta>
            <ta e="T629" id="Seg_5786" s="T628">ilʼčʼa-ntɨ-lʼ</ta>
            <ta e="T630" id="Seg_5787" s="T629">mɨ-tɨ</ta>
            <ta e="T631" id="Seg_5788" s="T630">kos</ta>
            <ta e="T632" id="Seg_5789" s="T631">qaj</ta>
            <ta e="T633" id="Seg_5790" s="T632">nʼennä</ta>
            <ta e="T634" id="Seg_5791" s="T633">qən-ŋɨ</ta>
            <ta e="T635" id="Seg_5792" s="T634">tına</ta>
            <ta e="T636" id="Seg_5793" s="T635">ontɨ</ta>
            <ta e="T637" id="Seg_5794" s="T636">ašša</ta>
            <ta e="T638" id="Seg_5795" s="T637">*koŋɨ-mpɨ</ta>
            <ta e="T639" id="Seg_5796" s="T638">toːnna</ta>
            <ta e="T640" id="Seg_5797" s="T639">ima-tɨ</ta>
            <ta e="T641" id="Seg_5798" s="T640">na</ta>
            <ta e="T642" id="Seg_5799" s="T641">kətɨ-äl-mpɨ-ntɨ-tɨ</ta>
            <ta e="T643" id="Seg_5800" s="T642">əsɨ-ntɨ-lʼ</ta>
            <ta e="T644" id="Seg_5801" s="T643">ilʼčʼa-ntɨ-lʼ</ta>
            <ta e="T645" id="Seg_5802" s="T644">mɨ-qı-nkinı</ta>
            <ta e="T646" id="Seg_5803" s="T645">ira-lʼ</ta>
            <ta e="T647" id="Seg_5804" s="T646">mɨ-t</ta>
            <ta e="T648" id="Seg_5805" s="T647">moqɨnä</ta>
            <ta e="T649" id="Seg_5806" s="T648">qən-sɨ-tɨt</ta>
            <ta e="T650" id="Seg_5807" s="T649">a</ta>
            <ta e="T651" id="Seg_5808" s="T650">nɨːnɨ</ta>
            <ta e="T652" id="Seg_5809" s="T651">šittɨ-lʼ</ta>
            <ta e="T653" id="Seg_5810" s="T652">pija-ka</ta>
            <ta e="T654" id="Seg_5811" s="T653">pija</ta>
            <ta e="T655" id="Seg_5812" s="T654">nʼi</ta>
            <ta e="T656" id="Seg_5813" s="T655">qaj</ta>
            <ta e="T657" id="Seg_5814" s="T656">ašša</ta>
            <ta e="T658" id="Seg_5815" s="T657">tɛnɨmɨ-tɨt</ta>
            <ta e="T659" id="Seg_5816" s="T658">ta</ta>
            <ta e="T660" id="Seg_5817" s="T659">qən-ŋɨ</ta>
            <ta e="T661" id="Seg_5818" s="T660">təp-ɨ-t</ta>
            <ta e="T662" id="Seg_5819" s="T661">ilʼčʼa-n-ntɨ</ta>
            <ta e="T663" id="Seg_5820" s="T662">mɔːt-ntɨ</ta>
            <ta e="T664" id="Seg_5821" s="T663">šeːr-ŋɨ-qı</ta>
            <ta e="T665" id="Seg_5822" s="T664">qaqlɨ-lʼ</ta>
            <ta e="T666" id="Seg_5823" s="T665">tɔːŋ-tɨ</ta>
            <ta e="T667" id="Seg_5824" s="T666">kurɨ-lä</ta>
            <ta e="T668" id="Seg_5825" s="T667">nılʼčʼɨ-k</ta>
            <ta e="T669" id="Seg_5826" s="T668">tottɨ-tɨt</ta>
            <ta e="T670" id="Seg_5827" s="T669">toːnna</ta>
            <ta e="T671" id="Seg_5828" s="T670">qən-mpɨ-ntɨ-tɨt</ta>
            <ta e="T672" id="Seg_5829" s="T671">moqɨnä</ta>
            <ta e="T673" id="Seg_5830" s="T672">na</ta>
            <ta e="T674" id="Seg_5831" s="T673">qən-mpɨ-ntɨ-tɨt</ta>
            <ta e="T675" id="Seg_5832" s="T674">Pan</ta>
            <ta e="T676" id="Seg_5833" s="T675">ira</ta>
            <ta e="T677" id="Seg_5834" s="T676">takkɨt</ta>
            <ta e="T678" id="Seg_5835" s="T677">kekkɨsä</ta>
            <ta e="T679" id="Seg_5836" s="T678">mɔːt-qɨn-ntɨ</ta>
            <ta e="T680" id="Seg_5837" s="T679">tulɨ-š-mpɨ</ta>
            <ta e="T681" id="Seg_5838" s="T680">ıllä</ta>
            <ta e="T682" id="Seg_5839" s="T681">qu-mpɨ</ta>
            <ta e="T683" id="Seg_5840" s="T682">toːnna</ta>
            <ta e="T684" id="Seg_5841" s="T683">šittɨ</ta>
            <ta e="T685" id="Seg_5842" s="T684">qum-ɨ-qı</ta>
            <ta e="T686" id="Seg_5843" s="T685">ilɨ-mpɨ-qı</ta>
            <ta e="T687" id="Seg_5844" s="T686">Pan</ta>
            <ta e="T688" id="Seg_5845" s="T687">ira</ta>
            <ta e="T689" id="Seg_5846" s="T688">nım-tɨ</ta>
            <ta e="T690" id="Seg_5847" s="T689">ɛː-ŋɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_5848" s="T0">one</ta>
            <ta e="T2" id="Seg_5849" s="T1">son.[NOM]-3SG</ta>
            <ta e="T3" id="Seg_5850" s="T2">be-DUR-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T4" id="Seg_5851" s="T3">someone-CAR-ADJZ</ta>
            <ta e="T5" id="Seg_5852" s="T4">friend-CAR-ADJZ</ta>
            <ta e="T6" id="Seg_5853" s="T5">this</ta>
            <ta e="T7" id="Seg_5854" s="T6">true.story.[NOM]</ta>
            <ta e="T8" id="Seg_5855" s="T7">NEG</ta>
            <ta e="T9" id="Seg_5856" s="T8">tale.[NOM]</ta>
            <ta e="T10" id="Seg_5857" s="T9">old.man.[NOM]</ta>
            <ta e="T11" id="Seg_5858" s="T10">live-PST.NAR.[3SG.S]</ta>
            <ta e="T12" id="Seg_5859" s="T11">be.sick-INCH-PST.NAR.[3SG.S]</ta>
            <ta e="T13" id="Seg_5860" s="T12">this</ta>
            <ta e="T14" id="Seg_5861" s="T13">Selkup-EP-ADJZ</ta>
            <ta e="T15" id="Seg_5862" s="T14">old.man.[NOM]</ta>
            <ta e="T16" id="Seg_5863" s="T15">located.on.this.side</ta>
            <ta e="T17" id="Seg_5864" s="T16">big.river-GEN</ta>
            <ta e="T18" id="Seg_5865" s="T17">side-LOC</ta>
            <ta e="T19" id="Seg_5866" s="T18">live-PST.NAR.[3SG.S]</ta>
            <ta e="T20" id="Seg_5867" s="T19">tree-CAR-ADJZ</ta>
            <ta e="T21" id="Seg_5868" s="T20">earth-GEN</ta>
            <ta e="T22" id="Seg_5869" s="T21">there-LOC</ta>
            <ta e="T23" id="Seg_5870" s="T22">live-PST.NAR.[3SG.S]</ta>
            <ta e="T24" id="Seg_5871" s="T23">that</ta>
            <ta e="T25" id="Seg_5872" s="T24">be.sick-INCH-PST.NAR.[3SG.S]</ta>
            <ta e="T26" id="Seg_5873" s="T25">one</ta>
            <ta e="T27" id="Seg_5874" s="T26">son.[NOM]-3SG</ta>
            <ta e="T28" id="Seg_5875" s="T27">friend-CAR-ADJZ</ta>
            <ta e="T29" id="Seg_5876" s="T28">someone-CAR-ADJZ</ta>
            <ta e="T30" id="Seg_5877" s="T29">NEG</ta>
            <ta e="T31" id="Seg_5878" s="T30">daughter.[NOM]-3SG</ta>
            <ta e="T32" id="Seg_5879" s="T31">NEG.EX.[3SG.S]</ta>
            <ta e="T33" id="Seg_5880" s="T32">NEG</ta>
            <ta e="T34" id="Seg_5881" s="T33">what.[NOM]-3SG</ta>
            <ta e="T35" id="Seg_5882" s="T34">NEG.EX.[3SG.S]</ta>
            <ta e="T36" id="Seg_5883" s="T35">then</ta>
            <ta e="T37" id="Seg_5884" s="T36">father.[NOM]-3SG</ta>
            <ta e="T38" id="Seg_5885" s="T37">be.sick-INCH.[3SG.S]</ta>
            <ta e="T39" id="Seg_5886" s="T38">this</ta>
            <ta e="T40" id="Seg_5887" s="T39">child-DIM.[NOM]</ta>
            <ta e="T41" id="Seg_5888" s="T40">this</ta>
            <ta e="T42" id="Seg_5889" s="T41">guy.[NOM]</ta>
            <ta e="T43" id="Seg_5890" s="T42">supposedly-DIM</ta>
            <ta e="T44" id="Seg_5891" s="T43">there</ta>
            <ta e="T45" id="Seg_5892" s="T44">live.[3SG.S]</ta>
            <ta e="T46" id="Seg_5893" s="T45">father-OBL.3SG-COM</ta>
            <ta e="T47" id="Seg_5894" s="T46">mother-OBL.3SG-COM</ta>
            <ta e="T48" id="Seg_5895" s="T47">that-ADV.ILL</ta>
            <ta e="T49" id="Seg_5896" s="T48">two</ta>
            <ta e="T50" id="Seg_5897" s="T49">Nenets-ADJZ</ta>
            <ta e="T51" id="Seg_5898" s="T50">brother-DYA-PL.[NOM]</ta>
            <ta e="T52" id="Seg_5899" s="T51">be-PST.NAR-INFER-3PL</ta>
            <ta e="T693" id="Seg_5900" s="T52">Nenets.[NOM]</ta>
            <ta e="T53" id="Seg_5901" s="T693">old.man.[NOM]</ta>
            <ta e="T54" id="Seg_5902" s="T53">cross-US-FUT.[3SG.S]</ta>
            <ta e="T55" id="Seg_5903" s="T54">that</ta>
            <ta e="T56" id="Seg_5904" s="T55">old.man.[NOM]</ta>
            <ta e="T57" id="Seg_5905" s="T56">down</ta>
            <ta e="T58" id="Seg_5906" s="T57">die-CO.[3SG.S]</ta>
            <ta e="T59" id="Seg_5907" s="T58">that</ta>
            <ta e="T694" id="Seg_5908" s="T59">Nenets.[NOM]</ta>
            <ta e="T60" id="Seg_5909" s="T694">old.man.[NOM]</ta>
            <ta e="T61" id="Seg_5910" s="T60">where</ta>
            <ta e="T62" id="Seg_5911" s="T61">where.to.go-FUT.[3SG.S]</ta>
            <ta e="T63" id="Seg_5912" s="T62">there</ta>
            <ta e="T64" id="Seg_5913" s="T63">brother-OBL.3SG-ALL</ta>
            <ta e="T65" id="Seg_5914" s="T64">cross-CO.[3SG.S]</ta>
            <ta e="T66" id="Seg_5915" s="T65">old.man.[NOM]</ta>
            <ta e="T67" id="Seg_5916" s="T66">die-INFER.[3SG.S]</ta>
            <ta e="T68" id="Seg_5917" s="T67">down</ta>
            <ta e="T69" id="Seg_5918" s="T68">make-IMP.2PL.O</ta>
            <ta e="T70" id="Seg_5919" s="T69">then</ta>
            <ta e="T71" id="Seg_5920" s="T70">old.woman.[NOM]-3SG</ta>
            <ta e="T72" id="Seg_5921" s="T71">be.sick-INCH-CVB</ta>
            <ta e="T73" id="Seg_5922" s="T72">also</ta>
            <ta e="T74" id="Seg_5923" s="T73">down</ta>
            <ta e="T75" id="Seg_5924" s="T74">die-CO.[3SG.S]</ta>
            <ta e="T76" id="Seg_5925" s="T75">old.man-ADJZ</ta>
            <ta e="T77" id="Seg_5926" s="T76">something-DU.[NOM]</ta>
            <ta e="T78" id="Seg_5927" s="T77">son.[NOM]-3PL</ta>
            <ta e="T79" id="Seg_5928" s="T78">someone-CAR</ta>
            <ta e="T80" id="Seg_5929" s="T79">friend-CAR</ta>
            <ta e="T81" id="Seg_5930" s="T80">stay.[3SG.S]</ta>
            <ta e="T82" id="Seg_5931" s="T81">where</ta>
            <ta e="T83" id="Seg_5932" s="T82">where.to.go-FUT.[3SG.S]</ta>
            <ta e="T84" id="Seg_5933" s="T83">this</ta>
            <ta e="T695" id="Seg_5934" s="T84">Nenets.[NOM]</ta>
            <ta e="T85" id="Seg_5935" s="T695">old.man.[NOM]</ta>
            <ta e="T86" id="Seg_5936" s="T85">two-ITER.NUM</ta>
            <ta e="T87" id="Seg_5937" s="T86">to.the.other.side</ta>
            <ta e="T88" id="Seg_5938" s="T87">cross-TR-PST.NAR-3SG.O</ta>
            <ta e="T89" id="Seg_5939" s="T88">elder</ta>
            <ta e="T90" id="Seg_5940" s="T89">son.[NOM]-3SG</ta>
            <ta e="T91" id="Seg_5941" s="T90">such-ADJZ</ta>
            <ta e="T92" id="Seg_5942" s="T91">son.[NOM]-3SG</ta>
            <ta e="T93" id="Seg_5943" s="T92">be-DUR-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T94" id="Seg_5944" s="T93">cross-TR-CO-3SG.O</ta>
            <ta e="T95" id="Seg_5945" s="T94">reindeer.[NOM]-3SG</ta>
            <ta e="T96" id="Seg_5946" s="T95">and</ta>
            <ta e="T97" id="Seg_5947" s="T96">NEG.EX.[3SG.S]</ta>
            <ta e="T98" id="Seg_5948" s="T97">reindeer-CAR-ADVZ</ta>
            <ta e="T99" id="Seg_5949" s="T98">leg-INSTR</ta>
            <ta e="T100" id="Seg_5950" s="T99">live-DUR-3PL</ta>
            <ta e="T101" id="Seg_5951" s="T100">there</ta>
            <ta e="T102" id="Seg_5952" s="T101">live-DUR-3PL</ta>
            <ta e="T103" id="Seg_5953" s="T102">live-DUR-3PL</ta>
            <ta e="T104" id="Seg_5954" s="T103">to.the.other.side</ta>
            <ta e="T105" id="Seg_5955" s="T104">cross-CVB</ta>
            <ta e="T106" id="Seg_5956" s="T105">Nenets.old.man-GEN</ta>
            <ta e="T107" id="Seg_5957" s="T106">tent.[NOM]-3SG</ta>
            <ta e="T691" id="Seg_5958" s="T107">Nenets.[NOM]</ta>
            <ta e="T108" id="Seg_5959" s="T691">old.man.[NOM]</ta>
            <ta e="T109" id="Seg_5960" s="T108">son.[NOM]-3SG</ta>
            <ta e="T110" id="Seg_5961" s="T109">NEG.EX-PST.NAR.[3SG.S]</ta>
            <ta e="T111" id="Seg_5962" s="T110">child-CAR-ADJZ</ta>
            <ta e="T112" id="Seg_5963" s="T111">woman.[NOM]-3SG</ta>
            <ta e="T113" id="Seg_5964" s="T112">that</ta>
            <ta e="T114" id="Seg_5965" s="T113">middle-ADJZ</ta>
            <ta e="T115" id="Seg_5966" s="T114">brother.[NOM]-3SG</ta>
            <ta e="T116" id="Seg_5967" s="T115">one</ta>
            <ta e="T117" id="Seg_5968" s="T116">daughter.[NOM]-3SG</ta>
            <ta e="T118" id="Seg_5969" s="T117">friend-CAR-ADJZ</ta>
            <ta e="T119" id="Seg_5970" s="T118">down.the.river-ADV.EL</ta>
            <ta e="T120" id="Seg_5971" s="T119">forward</ta>
            <ta e="T121" id="Seg_5972" s="T120">two</ta>
            <ta e="T122" id="Seg_5973" s="T121">Nenets.[NOM]</ta>
            <ta e="T123" id="Seg_5974" s="T122">come-CO.[3SG.S]</ta>
            <ta e="T124" id="Seg_5975" s="T123">tree-CAR-ADJZ</ta>
            <ta e="T125" id="Seg_5976" s="T124">earth-EL</ta>
            <ta e="T126" id="Seg_5977" s="T125">this</ta>
            <ta e="T692" id="Seg_5978" s="T126">Nenets.[NOM]</ta>
            <ta e="T127" id="Seg_5979" s="T692">old.man.[NOM]</ta>
            <ta e="T128" id="Seg_5980" s="T127">here.it.is.[3SG.S]</ta>
            <ta e="T129" id="Seg_5981" s="T128">interrog.pron.stem-ADJZ</ta>
            <ta e="T130" id="Seg_5982" s="T129">clan-GEN.3SG</ta>
            <ta e="T131" id="Seg_5983" s="T130">reindeer-ACC</ta>
            <ta e="T132" id="Seg_5984" s="T131">all</ta>
            <ta e="T133" id="Seg_5985" s="T132">gather-US-PST.NAR-3SG.O</ta>
            <ta e="T134" id="Seg_5986" s="T133">such-ADVZ</ta>
            <ta e="T135" id="Seg_5987" s="T134">come-CO.[3SG.S]</ta>
            <ta e="T136" id="Seg_5988" s="T135">here</ta>
            <ta e="T137" id="Seg_5989" s="T136">brother-DYA-DU.[NOM]</ta>
            <ta e="T138" id="Seg_5990" s="T137">live-3PL</ta>
            <ta e="T139" id="Seg_5991" s="T138">this</ta>
            <ta e="T140" id="Seg_5992" s="T139">old.man-ADJZ</ta>
            <ta e="T141" id="Seg_5993" s="T140">brother-DYA-DU.[NOM]</ta>
            <ta e="T142" id="Seg_5994" s="T141">two</ta>
            <ta e="T143" id="Seg_5995" s="T142">four</ta>
            <ta e="T144" id="Seg_5996" s="T143">hundred</ta>
            <ta e="T145" id="Seg_5997" s="T144">reindeer-DU.[NOM]-3SG</ta>
            <ta e="T146" id="Seg_5998" s="T145">this</ta>
            <ta e="T147" id="Seg_5999" s="T146">old.man-GEN</ta>
            <ta e="T148" id="Seg_6000" s="T147">reindeer-ACC</ta>
            <ta e="T149" id="Seg_6001" s="T148">take-SUP.2/3SG</ta>
            <ta e="T150" id="Seg_6002" s="T149">come-INFER.[3SG.S]</ta>
            <ta e="T151" id="Seg_6003" s="T150">human.being-EP-OBL.3SG-COM</ta>
            <ta e="T152" id="Seg_6004" s="T151">come-INFER.[3SG.S]</ta>
            <ta e="T153" id="Seg_6005" s="T152">Pan.[NOM]</ta>
            <ta e="T154" id="Seg_6006" s="T153">old.man.[NOM]</ta>
            <ta e="T155" id="Seg_6007" s="T154">name.[NOM]-3SG</ta>
            <ta e="T156" id="Seg_6008" s="T155">also</ta>
            <ta e="T157" id="Seg_6009" s="T156">here.it.is</ta>
            <ta e="T158" id="Seg_6010" s="T157">then</ta>
            <ta e="T159" id="Seg_6011" s="T158">come-CVB</ta>
            <ta e="T160" id="Seg_6012" s="T159">spend.night-3PL</ta>
            <ta e="T161" id="Seg_6013" s="T160">here</ta>
            <ta e="T162" id="Seg_6014" s="T161">(s)he-EP-PL-EP-GEN</ta>
            <ta e="T163" id="Seg_6015" s="T162">tent-LOC</ta>
            <ta e="T164" id="Seg_6016" s="T163">this</ta>
            <ta e="T165" id="Seg_6017" s="T164">guy.[NOM]</ta>
            <ta e="T166" id="Seg_6018" s="T165">NEG</ta>
            <ta e="T167" id="Seg_6019" s="T166">know-3PL</ta>
            <ta e="T168" id="Seg_6020" s="T167">guy.[NOM]</ta>
            <ta e="T169" id="Seg_6021" s="T168">how</ta>
            <ta e="T170" id="Seg_6022" s="T169">(s)he-EP-ALL</ta>
            <ta e="T171" id="Seg_6023" s="T170">get.into-PST.NAR.[3SG.S]</ta>
            <ta e="T172" id="Seg_6024" s="T171">(s)he.[NOM]</ta>
            <ta e="T173" id="Seg_6025" s="T172">what.[NOM]</ta>
            <ta e="T174" id="Seg_6026" s="T173">wise-ADVZ</ta>
            <ta e="T175" id="Seg_6027" s="T174">live.[3SG.S]</ta>
            <ta e="T176" id="Seg_6028" s="T175">human.being.[NOM]</ta>
            <ta e="T177" id="Seg_6029" s="T176">how</ta>
            <ta e="T178" id="Seg_6030" s="T177">know-FUT-3SG.O</ta>
            <ta e="T179" id="Seg_6031" s="T178">other</ta>
            <ta e="T180" id="Seg_6032" s="T179">live-PTCP.PRS</ta>
            <ta e="T181" id="Seg_6033" s="T180">human.being.[NOM]</ta>
            <ta e="T182" id="Seg_6034" s="T181">spend.night-3PL</ta>
            <ta e="T183" id="Seg_6035" s="T182">it.is.said</ta>
            <ta e="T184" id="Seg_6036" s="T183">three-ORD</ta>
            <ta e="T185" id="Seg_6037" s="T184">day-ADV.LOC</ta>
            <ta e="T186" id="Seg_6038" s="T185">%%</ta>
            <ta e="T187" id="Seg_6039" s="T186">it.is.said</ta>
            <ta e="T188" id="Seg_6040" s="T187">Pan.[NOM]</ta>
            <ta e="T189" id="Seg_6041" s="T188">old.man.[NOM]</ta>
            <ta e="T190" id="Seg_6042" s="T189">ten</ta>
            <ta e="T191" id="Seg_6043" s="T190">human.being-COM</ta>
            <ta e="T192" id="Seg_6044" s="T191">come-CO.[3SG.S]</ta>
            <ta e="T193" id="Seg_6045" s="T192">supposedly</ta>
            <ta e="T194" id="Seg_6046" s="T193">reindeer-ACC</ta>
            <ta e="T195" id="Seg_6047" s="T194">shepherd-SUP.2/3SG</ta>
            <ta e="T196" id="Seg_6048" s="T195">this</ta>
            <ta e="T197" id="Seg_6049" s="T196">that</ta>
            <ta e="T198" id="Seg_6050" s="T197">father-DYA-PL-EP-GEN</ta>
            <ta e="T199" id="Seg_6051" s="T198">reindeer-ACC</ta>
            <ta e="T200" id="Seg_6052" s="T199">shepherd-INF</ta>
            <ta e="T201" id="Seg_6053" s="T200">QUEST-GEN</ta>
            <ta e="T202" id="Seg_6054" s="T201">to.the.extent.of-ADJZ</ta>
            <ta e="T203" id="Seg_6055" s="T202">tent.[NOM]-3SG</ta>
            <ta e="T204" id="Seg_6056" s="T203">new</ta>
            <ta e="T205" id="Seg_6057" s="T204">QUEST-GEN</ta>
            <ta e="T206" id="Seg_6058" s="T205">to.the.extent.of-ADJZ</ta>
            <ta e="T207" id="Seg_6059" s="T206">what.[NOM]-3SG</ta>
            <ta e="T208" id="Seg_6060" s="T207">all</ta>
            <ta e="T209" id="Seg_6061" s="T208">gather-MULO-FUT-3SG.O</ta>
            <ta e="T210" id="Seg_6062" s="T209">earlier</ta>
            <ta e="T211" id="Seg_6063" s="T210">more</ta>
            <ta e="T212" id="Seg_6064" s="T211">gather-MULO-MULT-3SG.O</ta>
            <ta e="T213" id="Seg_6065" s="T212">Nenets-PL-EP-GEN</ta>
            <ta e="T214" id="Seg_6066" s="T213">reindeer-ACC</ta>
            <ta e="T215" id="Seg_6067" s="T214">gather-MULO-CVB</ta>
            <ta e="T216" id="Seg_6068" s="T215">take-PST.NAR-3SG.O</ta>
            <ta e="T217" id="Seg_6069" s="T216">then</ta>
            <ta e="T218" id="Seg_6070" s="T217">then</ta>
            <ta e="T219" id="Seg_6071" s="T218">%%</ta>
            <ta e="T220" id="Seg_6072" s="T219">back</ta>
            <ta e="T221" id="Seg_6073" s="T220">leave-CO-3DU.S</ta>
            <ta e="T222" id="Seg_6074" s="T221">this</ta>
            <ta e="T223" id="Seg_6075" s="T222">Nenets-DU.[NOM]</ta>
            <ta e="T224" id="Seg_6076" s="T223">three-ORD</ta>
            <ta e="T225" id="Seg_6077" s="T224">day-ADV.LOC</ta>
            <ta e="T226" id="Seg_6078" s="T225">it.is.said</ta>
            <ta e="T227" id="Seg_6079" s="T226">come-FUT-1PL</ta>
            <ta e="T228" id="Seg_6080" s="T227">you.PL.NOM</ta>
            <ta e="T229" id="Seg_6081" s="T228">it.is.said</ta>
            <ta e="T230" id="Seg_6082" s="T229">where</ta>
            <ta e="T231" id="Seg_6083" s="T230">NEG.IMP</ta>
            <ta e="T232" id="Seg_6084" s="T231">leave-IMP.2PL</ta>
            <ta e="T233" id="Seg_6085" s="T232">two-ORD</ta>
            <ta e="T234" id="Seg_6086" s="T233">day-ILL</ta>
            <ta e="T235" id="Seg_6087" s="T234">up</ta>
            <ta e="T236" id="Seg_6088" s="T235">sun-TRL-CO.[3SG.S]</ta>
            <ta e="T237" id="Seg_6089" s="T236">this</ta>
            <ta e="T238" id="Seg_6090" s="T237">guy.[NOM]</ta>
            <ta e="T239" id="Seg_6091" s="T238">such-ADJZ</ta>
            <ta e="T240" id="Seg_6092" s="T239">say-INFER-3SG.O</ta>
            <ta e="T241" id="Seg_6093" s="T240">grandfather-OBL.3SG-ALL</ta>
            <ta e="T242" id="Seg_6094" s="T241">as.if</ta>
            <ta e="T243" id="Seg_6095" s="T242">upstream-ADV.LOC</ta>
            <ta e="T244" id="Seg_6096" s="T243">here</ta>
            <ta e="T245" id="Seg_6097" s="T244">four</ta>
            <ta e="T246" id="Seg_6098" s="T245">hundred</ta>
            <ta e="T247" id="Seg_6099" s="T246">reindeer-PL.[NOM]</ta>
            <ta e="T248" id="Seg_6100" s="T247">stand-HAB-TEMPN-COR-ADJZ</ta>
            <ta e="T249" id="Seg_6101" s="T248">tundra.[NOM]-1SG</ta>
            <ta e="T250" id="Seg_6102" s="T249">be-INFER.[3SG.S]</ta>
            <ta e="T251" id="Seg_6103" s="T250">there</ta>
            <ta e="T252" id="Seg_6104" s="T251">sent-INCH-1PL</ta>
            <ta e="T253" id="Seg_6105" s="T252">Nenets-PL.[NOM]</ta>
            <ta e="T254" id="Seg_6106" s="T253">come-FUT-3PL</ta>
            <ta e="T255" id="Seg_6107" s="T254">reindeer-ACC</ta>
            <ta e="T256" id="Seg_6108" s="T255">shepherd-INF-be.going.to-FUT-3PL</ta>
            <ta e="T257" id="Seg_6109" s="T256">reindeer-GEN</ta>
            <ta e="T258" id="Seg_6110" s="T257">pasture-LOC</ta>
            <ta e="T259" id="Seg_6111" s="T258">winter-%%-ABST-GEN-3SG.O</ta>
            <ta e="T260" id="Seg_6112" s="T259">during</ta>
            <ta e="T261" id="Seg_6113" s="T260">winter-VBLZ-PTCP.PST</ta>
            <ta e="T262" id="Seg_6114" s="T261">how</ta>
            <ta e="T263" id="Seg_6115" s="T262">make-FUT-1PL</ta>
            <ta e="T264" id="Seg_6116" s="T263">this</ta>
            <ta e="T265" id="Seg_6117" s="T264">guy.[NOM]</ta>
            <ta e="T266" id="Seg_6118" s="T265">so</ta>
            <ta e="T267" id="Seg_6119" s="T266">say-CO-3SG.O</ta>
            <ta e="T268" id="Seg_6120" s="T267">then</ta>
            <ta e="T269" id="Seg_6121" s="T268">so</ta>
            <ta e="T270" id="Seg_6122" s="T269">say-CO-3SG.O</ta>
            <ta e="T271" id="Seg_6123" s="T270">I.ALL</ta>
            <ta e="T272" id="Seg_6124" s="T271">as.if</ta>
            <ta e="T273" id="Seg_6125" s="T272">new</ta>
            <ta e="T274" id="Seg_6126" s="T273">tent.[NOM]-2PL</ta>
            <ta e="T275" id="Seg_6127" s="T274">be-CO.[3SG.S]</ta>
            <ta e="T276" id="Seg_6128" s="T275">grandfather.[NOM]</ta>
            <ta e="T277" id="Seg_6129" s="T276">I.ALL</ta>
            <ta e="T278" id="Seg_6130" s="T277">give-IMP.2SG.O</ta>
            <ta e="T279" id="Seg_6131" s="T278">but</ta>
            <ta e="T280" id="Seg_6132" s="T279">that</ta>
            <ta e="T281" id="Seg_6133" s="T280">that</ta>
            <ta e="T282" id="Seg_6134" s="T281">brother-GEN-3SG</ta>
            <ta e="T283" id="Seg_6135" s="T282">daughter.[NOM]</ta>
            <ta e="T284" id="Seg_6136" s="T283">grandfather.[NOM]-3SG</ta>
            <ta e="T285" id="Seg_6137" s="T284">this-%%-ACC</ta>
            <ta e="T286" id="Seg_6138" s="T285">go-CAUS-DUR-3SG.O</ta>
            <ta e="T287" id="Seg_6139" s="T286">this</ta>
            <ta e="T289" id="Seg_6140" s="T287">guy-ALL</ta>
            <ta e="T290" id="Seg_6141" s="T289">upstream-ADV.LOC</ta>
            <ta e="T291" id="Seg_6142" s="T290">here</ta>
            <ta e="T292" id="Seg_6143" s="T291">forest-ADJZ</ta>
            <ta e="T293" id="Seg_6144" s="T292">isthmus.[NOM]</ta>
            <ta e="T294" id="Seg_6145" s="T293">be-CO.[3SG.S]</ta>
            <ta e="T295" id="Seg_6146" s="T294">there</ta>
            <ta e="T296" id="Seg_6147" s="T295">tent-EP-VBLZ-FUT-1SG.S</ta>
            <ta e="T297" id="Seg_6148" s="T296">new</ta>
            <ta e="T298" id="Seg_6149" s="T297">house.[NOM]</ta>
            <ta e="T299" id="Seg_6150" s="T298">bring-IMP.2PL.S</ta>
            <ta e="T300" id="Seg_6151" s="T299">as.if</ta>
            <ta e="T301" id="Seg_6152" s="T300">let.go-MULO-CO-3PL</ta>
            <ta e="T302" id="Seg_6153" s="T301">the.other.day</ta>
            <ta e="T303" id="Seg_6154" s="T302">supposedly</ta>
            <ta e="T304" id="Seg_6155" s="T303">here</ta>
            <ta e="T305" id="Seg_6156" s="T304">three-ORD</ta>
            <ta e="T306" id="Seg_6157" s="T305">day.[NOM]</ta>
            <ta e="T307" id="Seg_6158" s="T306">come-FUT-3PL</ta>
            <ta e="T308" id="Seg_6159" s="T307">Pan.[NOM]</ta>
            <ta e="T309" id="Seg_6160" s="T308">old.man-ADJZ-something-PL.[NOM]</ta>
            <ta e="T310" id="Seg_6161" s="T309">(s)he-EP-PL-EP-ALL</ta>
            <ta e="T311" id="Seg_6162" s="T310">this</ta>
            <ta e="T312" id="Seg_6163" s="T311">Nenets.old.man-GEN</ta>
            <ta e="T313" id="Seg_6164" s="T312">reindeer-ACC</ta>
            <ta e="T314" id="Seg_6165" s="T313">gather-MULO-INF</ta>
            <ta e="T315" id="Seg_6166" s="T314">brother-DYA-DU-GEN</ta>
            <ta e="T316" id="Seg_6167" s="T315">reindeer-ACC</ta>
            <ta e="T317" id="Seg_6168" s="T316">then</ta>
            <ta e="T318" id="Seg_6169" s="T317">every.which.way-ADJZ</ta>
            <ta e="T319" id="Seg_6170" s="T318">let.go-MULO-IPFV-3PL</ta>
            <ta e="T320" id="Seg_6171" s="T319">this</ta>
            <ta e="T321" id="Seg_6172" s="T320">reindeer-ACC</ta>
            <ta e="T322" id="Seg_6173" s="T321">INFER</ta>
            <ta e="T323" id="Seg_6174" s="T322">shepherd-INFER-3PL</ta>
            <ta e="T324" id="Seg_6175" s="T323">supposedly</ta>
            <ta e="T325" id="Seg_6176" s="T324">(s)he.[NOM]</ta>
            <ta e="T326" id="Seg_6177" s="T325">also</ta>
            <ta e="T327" id="Seg_6178" s="T326">shepherd-DUR-3SG.O</ta>
            <ta e="T328" id="Seg_6179" s="T327">tent-INSTR</ta>
            <ta e="T329" id="Seg_6180" s="T328">INFER</ta>
            <ta e="T330" id="Seg_6181" s="T329">give-INFER-3PL</ta>
            <ta e="T331" id="Seg_6182" s="T330">this</ta>
            <ta e="T332" id="Seg_6183" s="T331">forest-GEN</ta>
            <ta e="T333" id="Seg_6184" s="T332">isthmus-GEN</ta>
            <ta e="T334" id="Seg_6185" s="T333">forward-ADJZ</ta>
            <ta e="T335" id="Seg_6186" s="T334">side-LOC</ta>
            <ta e="T336" id="Seg_6187" s="T335">then</ta>
            <ta e="T337" id="Seg_6188" s="T336">tent-EP-VBLZ-INFER-3PL</ta>
            <ta e="T338" id="Seg_6189" s="T337">this</ta>
            <ta e="T339" id="Seg_6190" s="T338">wife-OBL.3SG-COM</ta>
            <ta e="T340" id="Seg_6191" s="T339">supposedly</ta>
            <ta e="T341" id="Seg_6192" s="T340">rich.man-ADJZ</ta>
            <ta e="T342" id="Seg_6193" s="T341">Nenets.[NOM]</ta>
            <ta e="T343" id="Seg_6194" s="T342">what.[NOM]</ta>
            <ta e="T344" id="Seg_6195" s="T343">tent.[NOM]-3SG</ta>
            <ta e="T345" id="Seg_6196" s="T344">NEG.EX.[3SG.S]</ta>
            <ta e="T346" id="Seg_6197" s="T345">new</ta>
            <ta e="T347" id="Seg_6198" s="T346">tent.[NOM]-3SG</ta>
            <ta e="T348" id="Seg_6199" s="T347">skin-ADJZ</ta>
            <ta e="T349" id="Seg_6200" s="T348">tent.[NOM]-3SG</ta>
            <ta e="T350" id="Seg_6201" s="T349">then</ta>
            <ta e="T351" id="Seg_6202" s="T350">two-ITER.NUM</ta>
            <ta e="T352" id="Seg_6203" s="T351">spend.night-3PL</ta>
            <ta e="T353" id="Seg_6204" s="T352">this</ta>
            <ta e="T354" id="Seg_6205" s="T353">wife-OBL.3SG-COM</ta>
            <ta e="T355" id="Seg_6206" s="T354">next</ta>
            <ta e="T356" id="Seg_6207" s="T355">morning-ADV.LOC</ta>
            <ta e="T357" id="Seg_6208" s="T356">such-ADVZ</ta>
            <ta e="T358" id="Seg_6209" s="T357">become.[3SG.S]</ta>
            <ta e="T359" id="Seg_6210" s="T358">that</ta>
            <ta e="T360" id="Seg_6211" s="T359">wife-OBL.3SG-ALL</ta>
            <ta e="T361" id="Seg_6212" s="T360">I.NOM</ta>
            <ta e="T362" id="Seg_6213" s="T361">as.if</ta>
            <ta e="T363" id="Seg_6214" s="T362">upstream</ta>
            <ta e="T364" id="Seg_6215" s="T363">human.being-PL-ILL.1SG</ta>
            <ta e="T365" id="Seg_6216" s="T364">go.to-OPT-1SG.S</ta>
            <ta e="T366" id="Seg_6217" s="T365">either.or</ta>
            <ta e="T367" id="Seg_6218" s="T366">as.if</ta>
            <ta e="T368" id="Seg_6219" s="T367">tent-EP-VBLZ-PST.NAR-3PL</ta>
            <ta e="T369" id="Seg_6220" s="T368">either.or</ta>
            <ta e="T370" id="Seg_6221" s="T369">where.to.go-3PL</ta>
            <ta e="T371" id="Seg_6222" s="T370">upstream</ta>
            <ta e="T372" id="Seg_6223" s="T371">move-TR-PFV.[3SG.S]</ta>
            <ta e="T373" id="Seg_6224" s="T372">three</ta>
            <ta e="T374" id="Seg_6225" s="T373">bull-ACC</ta>
            <ta e="T375" id="Seg_6226" s="T374">harness-CVB</ta>
            <ta e="T376" id="Seg_6227" s="T375">supposedly-DIM</ta>
            <ta e="T377" id="Seg_6228" s="T376">three-ten-EP-ADJZ</ta>
            <ta e="T378" id="Seg_6229" s="T377">reindeer-ACC</ta>
            <ta e="T379" id="Seg_6230" s="T378">stay-TR-PST.NAR-INFER-3SG.O</ta>
            <ta e="T380" id="Seg_6231" s="T379">guy.[NOM]</ta>
            <ta e="T381" id="Seg_6232" s="T380">upstream</ta>
            <ta e="T382" id="Seg_6233" s="T381">go.away-PFV-CO.[3SG.S]</ta>
            <ta e="T383" id="Seg_6234" s="T382">wife.[NOM]-3SG</ta>
            <ta e="T384" id="Seg_6235" s="T383">oneself.3SG</ta>
            <ta e="T385" id="Seg_6236" s="T384">stay.[3SG.S]</ta>
            <ta e="T386" id="Seg_6237" s="T385">here</ta>
            <ta e="T387" id="Seg_6238" s="T386">sit.[3SG.S]</ta>
            <ta e="T388" id="Seg_6239" s="T387">here</ta>
            <ta e="T389" id="Seg_6240" s="T388">sit.[3SG.S]</ta>
            <ta e="T390" id="Seg_6241" s="T389">one</ta>
            <ta e="T391" id="Seg_6242" s="T390">that-GEN</ta>
            <ta e="T392" id="Seg_6243" s="T391">middle-LOC</ta>
            <ta e="T393" id="Seg_6244" s="T392">day.[NOM]-3SG</ta>
            <ta e="T394" id="Seg_6245" s="T393">evening-ILL</ta>
            <ta e="T395" id="Seg_6246" s="T394">here</ta>
            <ta e="T396" id="Seg_6247" s="T395">go.away-CO.[3SG.S]</ta>
            <ta e="T397" id="Seg_6248" s="T396">this</ta>
            <ta e="T398" id="Seg_6249" s="T397">guy.[NOM]</ta>
            <ta e="T399" id="Seg_6250" s="T398">quiet-ADVZ</ta>
            <ta e="T400" id="Seg_6251" s="T399">feel-CO.[3SG.S]</ta>
            <ta e="T401" id="Seg_6252" s="T400">upstream-ADV.LOC</ta>
            <ta e="T402" id="Seg_6253" s="T401">one</ta>
            <ta e="T403" id="Seg_6254" s="T402">that-GEN</ta>
            <ta e="T404" id="Seg_6255" s="T403">middle-LOC</ta>
            <ta e="T405" id="Seg_6256" s="T404">human.being.[NOM]</ta>
            <ta e="T406" id="Seg_6257" s="T405">this-DIM-ACC</ta>
            <ta e="T407" id="Seg_6258" s="T406">bring-INFER-3PL</ta>
            <ta e="T408" id="Seg_6259" s="T407">down.the.river-ADV.EL</ta>
            <ta e="T409" id="Seg_6260" s="T408">Pan.[NOM]</ta>
            <ta e="T410" id="Seg_6261" s="T409">old.man-ADJZ-something-PL.[NOM]</ta>
            <ta e="T411" id="Seg_6262" s="T410">bring-INFER-3PL</ta>
            <ta e="T412" id="Seg_6263" s="T411">ten</ta>
            <ta e="T413" id="Seg_6264" s="T412">human.being.[NOM]-3SG</ta>
            <ta e="T414" id="Seg_6265" s="T413">supposedly-DIM</ta>
            <ta e="T415" id="Seg_6266" s="T414">reindeer-ACC</ta>
            <ta e="T416" id="Seg_6267" s="T415">shepherd-DEB-PTCP.PRS</ta>
            <ta e="T417" id="Seg_6268" s="T416">Pan.[NOM]</ta>
            <ta e="T418" id="Seg_6269" s="T417">old.man.[NOM]</ta>
            <ta e="T419" id="Seg_6270" s="T418">so.much</ta>
            <ta e="T420" id="Seg_6271" s="T419">shaman.wisdom-COM</ta>
            <ta e="T421" id="Seg_6272" s="T420">be-CO.[3SG.S]</ta>
            <ta e="T422" id="Seg_6273" s="T421">then</ta>
            <ta e="T423" id="Seg_6274" s="T422">two-ITER.NUM</ta>
            <ta e="T424" id="Seg_6275" s="T423">this</ta>
            <ta e="T425" id="Seg_6276" s="T424">guy.[NOM]</ta>
            <ta e="T426" id="Seg_6277" s="T425">come-PFV-AUD.[3SG.S]</ta>
            <ta e="T427" id="Seg_6278" s="T426">wife.[NOM]-3SG</ta>
            <ta e="T428" id="Seg_6279" s="T427">hear-TR-PST.NAR-3SG.O</ta>
            <ta e="T429" id="Seg_6280" s="T428">apparently</ta>
            <ta e="T430" id="Seg_6281" s="T429">hardly</ta>
            <ta e="T431" id="Seg_6282" s="T430">come-PFV.[3SG.S]</ta>
            <ta e="T432" id="Seg_6283" s="T431">Pan.[NOM]</ta>
            <ta e="T433" id="Seg_6284" s="T432">old.man-ADJZ-something-PL.[NOM]</ta>
            <ta e="T434" id="Seg_6285" s="T433">whether</ta>
            <ta e="T435" id="Seg_6286" s="T434">apparently</ta>
            <ta e="T436" id="Seg_6287" s="T435">come-CO-3PL</ta>
            <ta e="T437" id="Seg_6288" s="T436">sledge-ADJZ</ta>
            <ta e="T438" id="Seg_6289" s="T437">something.[NOM]-3SG</ta>
            <ta e="T439" id="Seg_6290" s="T438">such-ADVZ</ta>
            <ta e="T440" id="Seg_6291" s="T439">put-MULO-CO-3PL</ta>
            <ta e="T441" id="Seg_6292" s="T440">space.outside-LOC</ta>
            <ta e="T442" id="Seg_6293" s="T441">outside-LOC</ta>
            <ta e="T443" id="Seg_6294" s="T442">tent-ILL</ta>
            <ta e="T444" id="Seg_6295" s="T443">come.in-CO.[3SG.S]</ta>
            <ta e="T445" id="Seg_6296" s="T444">door-GEN</ta>
            <ta e="T446" id="Seg_6297" s="T445">opening-ADV.LOC</ta>
            <ta e="T447" id="Seg_6298" s="T446">stand-HAB-CVB</ta>
            <ta e="T448" id="Seg_6299" s="T447">door-GEN</ta>
            <ta e="T449" id="Seg_6300" s="T448">opening-ADVZ</ta>
            <ta e="T450" id="Seg_6301" s="T449">%%</ta>
            <ta e="T451" id="Seg_6302" s="T450">leg-ACC-3SG</ta>
            <ta e="T452" id="Seg_6303" s="T451">shake-MOM-HAB-3SG.O</ta>
            <ta e="T453" id="Seg_6304" s="T452">high.fur.boots-ACC-3SG</ta>
            <ta e="T454" id="Seg_6305" s="T453">INFER</ta>
            <ta e="T455" id="Seg_6306" s="T454">shake-MOM-INFER-3SG.O</ta>
            <ta e="T456" id="Seg_6307" s="T455">so</ta>
            <ta e="T457" id="Seg_6308" s="T456">say-CO-3SG.O</ta>
            <ta e="T458" id="Seg_6309" s="T457">Pan.[NOM]</ta>
            <ta e="T459" id="Seg_6310" s="T458">old.man-ALL</ta>
            <ta e="T460" id="Seg_6311" s="T459">forward</ta>
            <ta e="T461" id="Seg_6312" s="T460">turn-TR-CVB</ta>
            <ta e="T462" id="Seg_6313" s="T461">mouth.[NOM]-2SG</ta>
            <ta e="T463" id="Seg_6314" s="T462">like</ta>
            <ta e="T464" id="Seg_6315" s="T463">only</ta>
            <ta e="T465" id="Seg_6316" s="T464">big.river-GEN</ta>
            <ta e="T466" id="Seg_6317" s="T465">ice_hole-GEN</ta>
            <ta e="T467" id="Seg_6318" s="T466">as.if</ta>
            <ta e="T468" id="Seg_6319" s="T467">the.other.day</ta>
            <ta e="T469" id="Seg_6320" s="T468">here</ta>
            <ta e="T470" id="Seg_6321" s="T469">forward</ta>
            <ta e="T471" id="Seg_6322" s="T470">jump-PST.NAR.[3SG.S]</ta>
            <ta e="T472" id="Seg_6323" s="T471">INFER</ta>
            <ta e="T473" id="Seg_6324" s="T472">fist-INSTR</ta>
            <ta e="T474" id="Seg_6325" s="T473">caress-MOM-PFV-PST.NAR-INFER-3SG.O</ta>
            <ta e="T475" id="Seg_6326" s="T474">Pan.[NOM]</ta>
            <ta e="T476" id="Seg_6327" s="T475">old.man-ADJZ</ta>
            <ta e="T477" id="Seg_6328" s="T476">something-PL-EP-ACC</ta>
            <ta e="T478" id="Seg_6329" s="T477">all</ta>
            <ta e="T479" id="Seg_6330" s="T478">flat</ta>
            <ta e="T480" id="Seg_6331" s="T479">hit-PST.NAR-3SG.O</ta>
            <ta e="T481" id="Seg_6332" s="T480">some.[NOM]-3SG</ta>
            <ta e="T482" id="Seg_6333" s="T481">die-PST.NAR.[3SG.S]</ta>
            <ta e="T483" id="Seg_6334" s="T482">Pan.[NOM]</ta>
            <ta e="T484" id="Seg_6335" s="T483">old.man.[NOM]</ta>
            <ta e="T485" id="Seg_6336" s="T484">live-CVB</ta>
            <ta e="T486" id="Seg_6337" s="T485">stay-PST.NAR.[3SG.S]</ta>
            <ta e="T487" id="Seg_6338" s="T486">two-DU.[NOM]</ta>
            <ta e="T488" id="Seg_6339" s="T487">live-CVB</ta>
            <ta e="T489" id="Seg_6340" s="T488">stay-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T490" id="Seg_6341" s="T489">outwards</ta>
            <ta e="T491" id="Seg_6342" s="T490">throw-MULO-PFV-3SG.O</ta>
            <ta e="T492" id="Seg_6343" s="T491">this</ta>
            <ta e="T493" id="Seg_6344" s="T492">guy.[NOM]</ta>
            <ta e="T494" id="Seg_6345" s="T493">one</ta>
            <ta e="T495" id="Seg_6346" s="T494">time.[NOM]</ta>
            <ta e="T496" id="Seg_6347" s="T495">fist-INSTR</ta>
            <ta e="T497" id="Seg_6348" s="T496">caress-MOM-PFV-PST.NAR-3SG.O</ta>
            <ta e="T498" id="Seg_6349" s="T497">all</ta>
            <ta e="T499" id="Seg_6350" s="T498">clothing-GEN-3SG</ta>
            <ta e="T500" id="Seg_6351" s="T499">inside-ADV.ILL</ta>
            <ta e="T501" id="Seg_6352" s="T500">trousers-GEN-3SG</ta>
            <ta e="T502" id="Seg_6353" s="T501">into</ta>
            <ta e="T503" id="Seg_6354" s="T502">%%-DRV-PST.NAR-3PL</ta>
            <ta e="T504" id="Seg_6355" s="T503">this</ta>
            <ta e="T505" id="Seg_6356" s="T504">die-PTCP.PST-something-PL.[NOM]</ta>
            <ta e="T506" id="Seg_6357" s="T505">then</ta>
            <ta e="T507" id="Seg_6358" s="T506">then</ta>
            <ta e="T508" id="Seg_6359" s="T507">outwards</ta>
            <ta e="T509" id="Seg_6360" s="T508">throw-MULO-PFV-3SG.O</ta>
            <ta e="T510" id="Seg_6361" s="T509">outside-ILL</ta>
            <ta e="T511" id="Seg_6362" s="T510">that</ta>
            <ta e="T512" id="Seg_6363" s="T511">sledge.[NOM]</ta>
            <ta e="T513" id="Seg_6364" s="T512">stock.[NOM]</ta>
            <ta e="T514" id="Seg_6365" s="T513">ski.[NOM]-3SG</ta>
            <ta e="T515" id="Seg_6366" s="T514">here</ta>
            <ta e="T516" id="Seg_6367" s="T515">bind-MULS-RES.[3SG.S]</ta>
            <ta e="T517" id="Seg_6368" s="T516">Pan.[NOM]</ta>
            <ta e="T518" id="Seg_6369" s="T517">old.man-ACC</ta>
            <ta e="T519" id="Seg_6370" s="T518">also</ta>
            <ta e="T520" id="Seg_6371" s="T519">outwards</ta>
            <ta e="T521" id="Seg_6372" s="T520">take-TR-PST.NAR-3SG.O</ta>
            <ta e="T522" id="Seg_6373" s="T521">throw-PST.NAR-3SG.O</ta>
            <ta e="T523" id="Seg_6374" s="T522">supposedly</ta>
            <ta e="T524" id="Seg_6375" s="T523">all</ta>
            <ta e="T525" id="Seg_6376" s="T524">outwards</ta>
            <ta e="T526" id="Seg_6377" s="T525">take-TR-PST.NAR-3SG.O</ta>
            <ta e="T527" id="Seg_6378" s="T526">down</ta>
            <ta e="T528" id="Seg_6379" s="T527">sit.down.[3SG.S]</ta>
            <ta e="T529" id="Seg_6380" s="T528">overclothes.with.deer.fur.inside-ADJZ</ta>
            <ta e="T530" id="Seg_6381" s="T529">goods.ACC-3SG</ta>
            <ta e="T531" id="Seg_6382" s="T530">away</ta>
            <ta e="T532" id="Seg_6383" s="T531">take-CVB</ta>
            <ta e="T533" id="Seg_6384" s="T532">eat-EP-FRQ-CVB</ta>
            <ta e="T534" id="Seg_6385" s="T533">down</ta>
            <ta e="T535" id="Seg_6386" s="T534">sit.down-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T536" id="Seg_6387" s="T535">spend.night.[3SG.S]</ta>
            <ta e="T537" id="Seg_6388" s="T536">this</ta>
            <ta e="T538" id="Seg_6389" s="T537">human.being-PL.[NOM]-3SG</ta>
            <ta e="T539" id="Seg_6390" s="T538">space.outside-LOC</ta>
            <ta e="T540" id="Seg_6391" s="T539">here</ta>
            <ta e="T541" id="Seg_6392" s="T540">lie-MULS-DUR-3PL</ta>
            <ta e="T542" id="Seg_6393" s="T541">how</ta>
            <ta e="T543" id="Seg_6394" s="T542">bind-RFL-RES.[3SG.S]</ta>
            <ta e="T544" id="Seg_6395" s="T543">such-ADVZ</ta>
            <ta e="T545" id="Seg_6396" s="T544">spend.night.[3SG.S]</ta>
            <ta e="T546" id="Seg_6397" s="T545">next</ta>
            <ta e="T547" id="Seg_6398" s="T546">morning-ADV.LOC</ta>
            <ta e="T548" id="Seg_6399" s="T547">up</ta>
            <ta e="T549" id="Seg_6400" s="T548">get.up.[3SG.S]</ta>
            <ta e="T550" id="Seg_6401" s="T549">Pan.[NOM]</ta>
            <ta e="T551" id="Seg_6402" s="T550">old.man.[NOM]</ta>
            <ta e="T552" id="Seg_6403" s="T551">such-ADVZ</ta>
            <ta e="T553" id="Seg_6404" s="T552">this.[NOM]</ta>
            <ta e="T554" id="Seg_6405" s="T553">face-ADJZ</ta>
            <ta e="T555" id="Seg_6406" s="T554">all.the.rest.[NOM]-3SG</ta>
            <ta e="T556" id="Seg_6407" s="T555">whole-EP-ADVZ</ta>
            <ta e="T557" id="Seg_6408" s="T556">swell.up-RFL.PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T558" id="Seg_6409" s="T557">barely</ta>
            <ta e="T559" id="Seg_6410" s="T558">eye-DIM.[NOM]-3SG</ta>
            <ta e="T560" id="Seg_6411" s="T559">see-RFL-DUR.[3SG.S]</ta>
            <ta e="T561" id="Seg_6412" s="T560">that</ta>
            <ta e="T562" id="Seg_6413" s="T561">Nenets-DU.[NOM]-3SG</ta>
            <ta e="T563" id="Seg_6414" s="T562">more</ta>
            <ta e="T564" id="Seg_6415" s="T563">good-in.some.degree</ta>
            <ta e="T565" id="Seg_6416" s="T564">be-DUR-PST.NAR.[3SG.S]</ta>
            <ta e="T566" id="Seg_6417" s="T565">such-ADJZ</ta>
            <ta e="T567" id="Seg_6418" s="T566">say-CO-3SG.O</ta>
            <ta e="T568" id="Seg_6419" s="T567">this</ta>
            <ta e="T569" id="Seg_6420" s="T568">guy.[NOM]</ta>
            <ta e="T570" id="Seg_6421" s="T569">as.if</ta>
            <ta e="T571" id="Seg_6422" s="T570">this</ta>
            <ta e="T572" id="Seg_6423" s="T571">Nenets-PL-EP-ALL</ta>
            <ta e="T573" id="Seg_6424" s="T572">home</ta>
            <ta e="T574" id="Seg_6425" s="T573">leave-TR-3PL</ta>
            <ta e="T575" id="Seg_6426" s="T574">%%</ta>
            <ta e="T576" id="Seg_6427" s="T575">oneself.3PL</ta>
            <ta e="T577" id="Seg_6428" s="T576">sledge-OBL.2SG-INSTR</ta>
            <ta e="T578" id="Seg_6429" s="T577">then</ta>
            <ta e="T579" id="Seg_6430" s="T578">two-ITER.NUM</ta>
            <ta e="T580" id="Seg_6431" s="T579">that</ta>
            <ta e="T581" id="Seg_6432" s="T580">we.DU.NOM-DU</ta>
            <ta e="T582" id="Seg_6433" s="T581">tent-ILL</ta>
            <ta e="T583" id="Seg_6434" s="T582">come.in-EP-US-PST.NAR-3PL</ta>
            <ta e="T584" id="Seg_6435" s="T583">either.or</ta>
            <ta e="T585" id="Seg_6436" s="T584">eat-EP-FRQ-PST.NAR-3PL</ta>
            <ta e="T586" id="Seg_6437" s="T585">either.or</ta>
            <ta e="T587" id="Seg_6438" s="T586">NEG</ta>
            <ta e="T588" id="Seg_6439" s="T587">then</ta>
            <ta e="T589" id="Seg_6440" s="T588">that</ta>
            <ta e="T590" id="Seg_6441" s="T589">bind-MOM-DUR-PTCP.PRS</ta>
            <ta e="T591" id="Seg_6442" s="T590">sledge-ACC-3PL</ta>
            <ta e="T592" id="Seg_6443" s="T591">then</ta>
            <ta e="T593" id="Seg_6444" s="T592">home</ta>
            <ta e="T594" id="Seg_6445" s="T593">%%-CVB</ta>
            <ta e="T595" id="Seg_6446" s="T594">this</ta>
            <ta e="T596" id="Seg_6447" s="T595">human.being-PL-ACC-3PL</ta>
            <ta e="T597" id="Seg_6448" s="T596">die-PTCP.PST</ta>
            <ta e="T598" id="Seg_6449" s="T597">human.being-PL-ACC-3SG</ta>
            <ta e="T599" id="Seg_6450" s="T598">so</ta>
            <ta e="T600" id="Seg_6451" s="T599">leave-TR-CO-3SG.O</ta>
            <ta e="T601" id="Seg_6452" s="T600">down.the.river</ta>
            <ta e="T602" id="Seg_6453" s="T601">home</ta>
            <ta e="T603" id="Seg_6454" s="T602">Pan.[NOM]</ta>
            <ta e="T604" id="Seg_6455" s="T603">old.man-ACC-3SG</ta>
            <ta e="T605" id="Seg_6456" s="T604">also</ta>
            <ta e="T606" id="Seg_6457" s="T605">%%-CVB</ta>
            <ta e="T607" id="Seg_6458" s="T606">leave-TR-3SG.O</ta>
            <ta e="T608" id="Seg_6459" s="T607">what.[NOM]</ta>
            <ta e="T609" id="Seg_6460" s="T608">barely</ta>
            <ta e="T611" id="Seg_6461" s="T610">leave-TR-3SG.O</ta>
            <ta e="T612" id="Seg_6462" s="T611">leave-CO-3PL</ta>
            <ta e="T613" id="Seg_6463" s="T612">that</ta>
            <ta e="T614" id="Seg_6464" s="T613">human.being-EP-PL.[NOM]</ta>
            <ta e="T615" id="Seg_6465" s="T614">good-ADVZ</ta>
            <ta e="T616" id="Seg_6466" s="T615">go.away-CO-3PL</ta>
            <ta e="T617" id="Seg_6467" s="T616">wife-OBL.3SG-ALL</ta>
            <ta e="T618" id="Seg_6468" s="T617">so</ta>
            <ta e="T619" id="Seg_6469" s="T618">say-CO-3SG.O</ta>
            <ta e="T620" id="Seg_6470" s="T619">tent.[NOM]-2SG</ta>
            <ta e="T621" id="Seg_6471" s="T620">away</ta>
            <ta e="T622" id="Seg_6472" s="T621">remove-IMP.2SG.O</ta>
            <ta e="T623" id="Seg_6473" s="T622">we.PL.NOM</ta>
            <ta e="T624" id="Seg_6474" s="T623">upstream</ta>
            <ta e="T625" id="Seg_6475" s="T624">leave-OPT</ta>
            <ta e="T626" id="Seg_6476" s="T625">human.being-EP-PL-EP-ALL</ta>
            <ta e="T627" id="Seg_6477" s="T626">NEG</ta>
            <ta e="T628" id="Seg_6478" s="T627">know-PST.NAR-3SG.O</ta>
            <ta e="T629" id="Seg_6479" s="T628">grandfather-OBL.3SG-ADJZ</ta>
            <ta e="T630" id="Seg_6480" s="T629">something.[NOM]-3SG</ta>
            <ta e="T631" id="Seg_6481" s="T630">INDEF3</ta>
            <ta e="T632" id="Seg_6482" s="T631">what.[NOM]</ta>
            <ta e="T633" id="Seg_6483" s="T632">upstream</ta>
            <ta e="T634" id="Seg_6484" s="T633">go.away-CO.[3SG.S]</ta>
            <ta e="T635" id="Seg_6485" s="T634">that</ta>
            <ta e="T636" id="Seg_6486" s="T635">oneself.3SG</ta>
            <ta e="T637" id="Seg_6487" s="T636">NEG</ta>
            <ta e="T638" id="Seg_6488" s="T637">say-DUR.[3SG.S]</ta>
            <ta e="T639" id="Seg_6489" s="T638">that</ta>
            <ta e="T640" id="Seg_6490" s="T639">woman.[NOM]-3SG</ta>
            <ta e="T641" id="Seg_6491" s="T640">INFER</ta>
            <ta e="T642" id="Seg_6492" s="T641">say-MULO-PST.NAR-INFER-3SG.O</ta>
            <ta e="T643" id="Seg_6493" s="T642">father-OBL.3SG-ADJZ</ta>
            <ta e="T644" id="Seg_6494" s="T643">grandfather-OBL.3SG-ADJZ</ta>
            <ta e="T645" id="Seg_6495" s="T644">something-DU-ALL</ta>
            <ta e="T646" id="Seg_6496" s="T645">old.man-ADJZ</ta>
            <ta e="T647" id="Seg_6497" s="T646">something-PL.[NOM]</ta>
            <ta e="T648" id="Seg_6498" s="T647">home</ta>
            <ta e="T649" id="Seg_6499" s="T648">go.away-PST-3PL</ta>
            <ta e="T650" id="Seg_6500" s="T649">but</ta>
            <ta e="T651" id="Seg_6501" s="T650">then</ta>
            <ta e="T652" id="Seg_6502" s="T651">two-ADJZ</ta>
            <ta e="T653" id="Seg_6503" s="T652">%%-DIM</ta>
            <ta e="T654" id="Seg_6504" s="T653">%%</ta>
            <ta e="T655" id="Seg_6505" s="T654">NEG</ta>
            <ta e="T656" id="Seg_6506" s="T655">what.[NOM]</ta>
            <ta e="T657" id="Seg_6507" s="T656">NEG</ta>
            <ta e="T658" id="Seg_6508" s="T657">know-3PL</ta>
            <ta e="T659" id="Seg_6509" s="T658">hrmph</ta>
            <ta e="T660" id="Seg_6510" s="T659">go.away-CO.[3SG.S]</ta>
            <ta e="T661" id="Seg_6511" s="T660">(s)he-EP-PL.[NOM]</ta>
            <ta e="T662" id="Seg_6512" s="T661">grandfather-GEN-OBL.3SG</ta>
            <ta e="T663" id="Seg_6513" s="T662">tent-ILL</ta>
            <ta e="T664" id="Seg_6514" s="T663">come.in-CO-3DU.S</ta>
            <ta e="T665" id="Seg_6515" s="T664">sledge-ADJZ</ta>
            <ta e="T666" id="Seg_6516" s="T665">all.the.rest.[NOM]-3SG</ta>
            <ta e="T667" id="Seg_6517" s="T666">wind.round-CVB</ta>
            <ta e="T668" id="Seg_6518" s="T667">such-ADVZ</ta>
            <ta e="T669" id="Seg_6519" s="T668">put-3PL</ta>
            <ta e="T670" id="Seg_6520" s="T669">that</ta>
            <ta e="T671" id="Seg_6521" s="T670">go.away-PST.NAR-INFER-3PL</ta>
            <ta e="T672" id="Seg_6522" s="T671">home</ta>
            <ta e="T673" id="Seg_6523" s="T672">INFER</ta>
            <ta e="T674" id="Seg_6524" s="T673">go.away-PST.NAR-INFER-3PL</ta>
            <ta e="T675" id="Seg_6525" s="T674">Pan.[NOM]</ta>
            <ta e="T676" id="Seg_6526" s="T675">old.man.[NOM]</ta>
            <ta e="T677" id="Seg_6527" s="T676">in.the.lower.reach</ta>
            <ta e="T678" id="Seg_6528" s="T677">only</ta>
            <ta e="T679" id="Seg_6529" s="T678">house-ILL-OBL.3SG</ta>
            <ta e="T680" id="Seg_6530" s="T679">come-US-PST.NAR.[3SG.S]</ta>
            <ta e="T681" id="Seg_6531" s="T680">down</ta>
            <ta e="T682" id="Seg_6532" s="T681">die-PST.NAR.[3SG.S]</ta>
            <ta e="T683" id="Seg_6533" s="T682">that</ta>
            <ta e="T684" id="Seg_6534" s="T683">two</ta>
            <ta e="T685" id="Seg_6535" s="T684">human.being-EP-DU.[NOM]</ta>
            <ta e="T686" id="Seg_6536" s="T685">live-PST.NAR-3DU.S</ta>
            <ta e="T687" id="Seg_6537" s="T686">Pan.[NOM]</ta>
            <ta e="T688" id="Seg_6538" s="T687">old.man.[NOM]</ta>
            <ta e="T689" id="Seg_6539" s="T688">name.[NOM]-3SG</ta>
            <ta e="T690" id="Seg_6540" s="T689">be-CO.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_6541" s="T0">один</ta>
            <ta e="T2" id="Seg_6542" s="T1">сын.[NOM]-3SG</ta>
            <ta e="T3" id="Seg_6543" s="T2">быть-DUR-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T4" id="Seg_6544" s="T3">некто-CAR-ADJZ</ta>
            <ta e="T5" id="Seg_6545" s="T4">друг-CAR-ADJZ</ta>
            <ta e="T6" id="Seg_6546" s="T5">этот</ta>
            <ta e="T7" id="Seg_6547" s="T6">быль.[NOM]</ta>
            <ta e="T8" id="Seg_6548" s="T7">NEG</ta>
            <ta e="T9" id="Seg_6549" s="T8">сказка.[NOM]</ta>
            <ta e="T10" id="Seg_6550" s="T9">старик.[NOM]</ta>
            <ta e="T11" id="Seg_6551" s="T10">жить-PST.NAR.[3SG.S]</ta>
            <ta e="T12" id="Seg_6552" s="T11">болеть-INCH-PST.NAR.[3SG.S]</ta>
            <ta e="T13" id="Seg_6553" s="T12">этот</ta>
            <ta e="T14" id="Seg_6554" s="T13">селькуп-EP-ADJZ</ta>
            <ta e="T15" id="Seg_6555" s="T14">старик.[NOM]</ta>
            <ta e="T16" id="Seg_6556" s="T15">посюсторонний</ta>
            <ta e="T17" id="Seg_6557" s="T16">большая.река-GEN</ta>
            <ta e="T18" id="Seg_6558" s="T17">сторона-LOC</ta>
            <ta e="T19" id="Seg_6559" s="T18">жить-PST.NAR.[3SG.S]</ta>
            <ta e="T20" id="Seg_6560" s="T19">дерево-CAR-ADJZ</ta>
            <ta e="T21" id="Seg_6561" s="T20">земля-GEN</ta>
            <ta e="T22" id="Seg_6562" s="T21">туда-LOC</ta>
            <ta e="T23" id="Seg_6563" s="T22">жить-PST.NAR.[3SG.S]</ta>
            <ta e="T24" id="Seg_6564" s="T23">тот</ta>
            <ta e="T25" id="Seg_6565" s="T24">болеть-INCH-PST.NAR.[3SG.S]</ta>
            <ta e="T26" id="Seg_6566" s="T25">один</ta>
            <ta e="T27" id="Seg_6567" s="T26">сын.[NOM]-3SG</ta>
            <ta e="T28" id="Seg_6568" s="T27">друг-CAR-ADJZ</ta>
            <ta e="T29" id="Seg_6569" s="T28">некто-CAR-ADJZ</ta>
            <ta e="T30" id="Seg_6570" s="T29">NEG</ta>
            <ta e="T31" id="Seg_6571" s="T30">дочь.[NOM]-3SG</ta>
            <ta e="T32" id="Seg_6572" s="T31">NEG.EX.[3SG.S]</ta>
            <ta e="T33" id="Seg_6573" s="T32">NEG</ta>
            <ta e="T34" id="Seg_6574" s="T33">что.[NOM]-3SG</ta>
            <ta e="T35" id="Seg_6575" s="T34">NEG.EX.[3SG.S]</ta>
            <ta e="T36" id="Seg_6576" s="T35">потом</ta>
            <ta e="T37" id="Seg_6577" s="T36">отец.[NOM]-3SG</ta>
            <ta e="T38" id="Seg_6578" s="T37">болеть-INCH.[3SG.S]</ta>
            <ta e="T39" id="Seg_6579" s="T38">этот</ta>
            <ta e="T40" id="Seg_6580" s="T39">ребенок-DIM.[NOM]</ta>
            <ta e="T41" id="Seg_6581" s="T40">этот</ta>
            <ta e="T42" id="Seg_6582" s="T41">парень.[NOM]</ta>
            <ta e="T43" id="Seg_6583" s="T42">вроде-DIM</ta>
            <ta e="T44" id="Seg_6584" s="T43">там</ta>
            <ta e="T45" id="Seg_6585" s="T44">жить.[3SG.S]</ta>
            <ta e="T46" id="Seg_6586" s="T45">отец-OBL.3SG-COM</ta>
            <ta e="T47" id="Seg_6587" s="T46">мать-OBL.3SG-COM</ta>
            <ta e="T48" id="Seg_6588" s="T47">тот-ADV.ILL</ta>
            <ta e="T49" id="Seg_6589" s="T48">два</ta>
            <ta e="T50" id="Seg_6590" s="T49">ненец-ADJZ</ta>
            <ta e="T51" id="Seg_6591" s="T50">брат-DYA-PL.[NOM]</ta>
            <ta e="T52" id="Seg_6592" s="T51">быть-PST.NAR-INFER-3PL</ta>
            <ta e="T693" id="Seg_6593" s="T52">ненец.[NOM]</ta>
            <ta e="T53" id="Seg_6594" s="T693">старик.[NOM]</ta>
            <ta e="T54" id="Seg_6595" s="T53">перейти-US-FUT.[3SG.S]</ta>
            <ta e="T55" id="Seg_6596" s="T54">тот</ta>
            <ta e="T56" id="Seg_6597" s="T55">старик.[NOM]</ta>
            <ta e="T57" id="Seg_6598" s="T56">вниз</ta>
            <ta e="T58" id="Seg_6599" s="T57">умереть-CO.[3SG.S]</ta>
            <ta e="T59" id="Seg_6600" s="T58">тот</ta>
            <ta e="T694" id="Seg_6601" s="T59">ненец.[NOM]</ta>
            <ta e="T60" id="Seg_6602" s="T694">старик.[NOM]</ta>
            <ta e="T61" id="Seg_6603" s="T60">куда</ta>
            <ta e="T62" id="Seg_6604" s="T61">куда.деваться-FUT.[3SG.S]</ta>
            <ta e="T63" id="Seg_6605" s="T62">туда</ta>
            <ta e="T64" id="Seg_6606" s="T63">брат-OBL.3SG-ALL</ta>
            <ta e="T65" id="Seg_6607" s="T64">перейти-CO.[3SG.S]</ta>
            <ta e="T66" id="Seg_6608" s="T65">старик.[NOM]</ta>
            <ta e="T67" id="Seg_6609" s="T66">умереть-INFER.[3SG.S]</ta>
            <ta e="T68" id="Seg_6610" s="T67">вниз</ta>
            <ta e="T69" id="Seg_6611" s="T68">сделать-IMP.2PL.O</ta>
            <ta e="T70" id="Seg_6612" s="T69">потом</ta>
            <ta e="T71" id="Seg_6613" s="T70">старуха.[NOM]-3SG</ta>
            <ta e="T72" id="Seg_6614" s="T71">болеть-INCH-CVB</ta>
            <ta e="T73" id="Seg_6615" s="T72">тоже</ta>
            <ta e="T74" id="Seg_6616" s="T73">вниз</ta>
            <ta e="T75" id="Seg_6617" s="T74">умереть-CO.[3SG.S]</ta>
            <ta e="T76" id="Seg_6618" s="T75">старик-ADJZ</ta>
            <ta e="T77" id="Seg_6619" s="T76">нечто-DU.[NOM]</ta>
            <ta e="T78" id="Seg_6620" s="T77">сын.[NOM]-3PL</ta>
            <ta e="T79" id="Seg_6621" s="T78">некто-CAR</ta>
            <ta e="T80" id="Seg_6622" s="T79">друг-CAR</ta>
            <ta e="T81" id="Seg_6623" s="T80">остаться.[3SG.S]</ta>
            <ta e="T82" id="Seg_6624" s="T81">куда</ta>
            <ta e="T83" id="Seg_6625" s="T82">куда.деваться-FUT.[3SG.S]</ta>
            <ta e="T84" id="Seg_6626" s="T83">этот</ta>
            <ta e="T695" id="Seg_6627" s="T84">ненец.[NOM]</ta>
            <ta e="T85" id="Seg_6628" s="T695">старик.[NOM]</ta>
            <ta e="T86" id="Seg_6629" s="T85">два-ITER.NUM</ta>
            <ta e="T87" id="Seg_6630" s="T86">на.другую.сторону</ta>
            <ta e="T88" id="Seg_6631" s="T87">перейти-TR-PST.NAR-3SG.O</ta>
            <ta e="T89" id="Seg_6632" s="T88">старший</ta>
            <ta e="T90" id="Seg_6633" s="T89">сын.[NOM]-3SG</ta>
            <ta e="T91" id="Seg_6634" s="T90">такой-ADJZ</ta>
            <ta e="T92" id="Seg_6635" s="T91">сын.[NOM]-3SG</ta>
            <ta e="T93" id="Seg_6636" s="T92">быть-DUR-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T94" id="Seg_6637" s="T93">перейти-TR-CO-3SG.O</ta>
            <ta e="T95" id="Seg_6638" s="T94">олень.[NOM]-3SG</ta>
            <ta e="T96" id="Seg_6639" s="T95">и</ta>
            <ta e="T97" id="Seg_6640" s="T96">NEG.EX.[3SG.S]</ta>
            <ta e="T98" id="Seg_6641" s="T97">олень-CAR-ADVZ</ta>
            <ta e="T99" id="Seg_6642" s="T98">нога-INSTR</ta>
            <ta e="T100" id="Seg_6643" s="T99">жить-DUR-3PL</ta>
            <ta e="T101" id="Seg_6644" s="T100">там</ta>
            <ta e="T102" id="Seg_6645" s="T101">жить-DUR-3PL</ta>
            <ta e="T103" id="Seg_6646" s="T102">жить-DUR-3PL</ta>
            <ta e="T104" id="Seg_6647" s="T103">на.другую.сторону</ta>
            <ta e="T105" id="Seg_6648" s="T104">перейти-CVB</ta>
            <ta e="T106" id="Seg_6649" s="T105">ненец_старик-GEN</ta>
            <ta e="T107" id="Seg_6650" s="T106">чум.[NOM]-3SG</ta>
            <ta e="T691" id="Seg_6651" s="T107">ненец.[NOM]</ta>
            <ta e="T108" id="Seg_6652" s="T691">старик.[NOM]</ta>
            <ta e="T109" id="Seg_6653" s="T108">сын.[NOM]-3SG</ta>
            <ta e="T110" id="Seg_6654" s="T109">NEG.EX-PST.NAR.[3SG.S]</ta>
            <ta e="T111" id="Seg_6655" s="T110">ребенок-CAR-ADJZ</ta>
            <ta e="T112" id="Seg_6656" s="T111">женщина.[NOM]-3SG</ta>
            <ta e="T113" id="Seg_6657" s="T112">тот</ta>
            <ta e="T114" id="Seg_6658" s="T113">середина-ADJZ</ta>
            <ta e="T115" id="Seg_6659" s="T114">брат.[NOM]-3SG</ta>
            <ta e="T116" id="Seg_6660" s="T115">один</ta>
            <ta e="T117" id="Seg_6661" s="T116">дочь.[NOM]-3SG</ta>
            <ta e="T118" id="Seg_6662" s="T117">друг-CAR-ADJZ</ta>
            <ta e="T119" id="Seg_6663" s="T118">вниз.по.течению.реки-ADV.EL</ta>
            <ta e="T120" id="Seg_6664" s="T119">вперёд</ta>
            <ta e="T121" id="Seg_6665" s="T120">два</ta>
            <ta e="T122" id="Seg_6666" s="T121">ненец.[NOM]</ta>
            <ta e="T123" id="Seg_6667" s="T122">прийти-CO.[3SG.S]</ta>
            <ta e="T124" id="Seg_6668" s="T123">дерево-CAR-ADJZ</ta>
            <ta e="T125" id="Seg_6669" s="T124">земля-EL</ta>
            <ta e="T126" id="Seg_6670" s="T125">этот</ta>
            <ta e="T692" id="Seg_6671" s="T126">ненец.[NOM]</ta>
            <ta e="T127" id="Seg_6672" s="T692">старик.[NOM]</ta>
            <ta e="T128" id="Seg_6673" s="T127">вот.он.есть.[3SG.S]</ta>
            <ta e="T129" id="Seg_6674" s="T128">interrog.pron.stem-ADJZ</ta>
            <ta e="T130" id="Seg_6675" s="T129">род-GEN.3SG</ta>
            <ta e="T131" id="Seg_6676" s="T130">олень-ACC</ta>
            <ta e="T132" id="Seg_6677" s="T131">всё</ta>
            <ta e="T133" id="Seg_6678" s="T132">собирать-US-PST.NAR-3SG.O</ta>
            <ta e="T134" id="Seg_6679" s="T133">такой-ADVZ</ta>
            <ta e="T135" id="Seg_6680" s="T134">прийти-CO.[3SG.S]</ta>
            <ta e="T136" id="Seg_6681" s="T135">вот</ta>
            <ta e="T137" id="Seg_6682" s="T136">брат-DYA-DU.[NOM]</ta>
            <ta e="T138" id="Seg_6683" s="T137">жить-3PL</ta>
            <ta e="T139" id="Seg_6684" s="T138">этот</ta>
            <ta e="T140" id="Seg_6685" s="T139">старик-ADJZ</ta>
            <ta e="T141" id="Seg_6686" s="T140">брат-DYA-DU.[NOM]</ta>
            <ta e="T142" id="Seg_6687" s="T141">два</ta>
            <ta e="T143" id="Seg_6688" s="T142">четыре</ta>
            <ta e="T144" id="Seg_6689" s="T143">сто</ta>
            <ta e="T145" id="Seg_6690" s="T144">олень-DU.[NOM]-3SG</ta>
            <ta e="T146" id="Seg_6691" s="T145">этот</ta>
            <ta e="T147" id="Seg_6692" s="T146">старик-GEN</ta>
            <ta e="T148" id="Seg_6693" s="T147">олень-ACC</ta>
            <ta e="T149" id="Seg_6694" s="T148">взять-SUP.2/3SG</ta>
            <ta e="T150" id="Seg_6695" s="T149">прийти-INFER.[3SG.S]</ta>
            <ta e="T151" id="Seg_6696" s="T150">человек-EP-OBL.3SG-COM</ta>
            <ta e="T152" id="Seg_6697" s="T151">прийти-INFER.[3SG.S]</ta>
            <ta e="T153" id="Seg_6698" s="T152">Пан.[NOM]</ta>
            <ta e="T154" id="Seg_6699" s="T153">старик.[NOM]</ta>
            <ta e="T155" id="Seg_6700" s="T154">имя.[NOM]-3SG</ta>
            <ta e="T156" id="Seg_6701" s="T155">тоже</ta>
            <ta e="T157" id="Seg_6702" s="T156">вот.он.есть</ta>
            <ta e="T158" id="Seg_6703" s="T157">потом</ta>
            <ta e="T159" id="Seg_6704" s="T158">прийти-CVB</ta>
            <ta e="T160" id="Seg_6705" s="T159">ночевать-3PL</ta>
            <ta e="T161" id="Seg_6706" s="T160">вот</ta>
            <ta e="T162" id="Seg_6707" s="T161">он(а)-EP-PL-EP-GEN</ta>
            <ta e="T163" id="Seg_6708" s="T162">чум-LOC</ta>
            <ta e="T164" id="Seg_6709" s="T163">этот</ta>
            <ta e="T165" id="Seg_6710" s="T164">парень.[NOM]</ta>
            <ta e="T166" id="Seg_6711" s="T165">NEG</ta>
            <ta e="T167" id="Seg_6712" s="T166">знать-3PL</ta>
            <ta e="T168" id="Seg_6713" s="T167">парень.[NOM]</ta>
            <ta e="T169" id="Seg_6714" s="T168">как</ta>
            <ta e="T170" id="Seg_6715" s="T169">он(а)-EP-ALL</ta>
            <ta e="T171" id="Seg_6716" s="T170">попасть-PST.NAR.[3SG.S]</ta>
            <ta e="T172" id="Seg_6717" s="T171">он(а).[NOM]</ta>
            <ta e="T173" id="Seg_6718" s="T172">что.[NOM]</ta>
            <ta e="T174" id="Seg_6719" s="T173">мудрёный-ADVZ</ta>
            <ta e="T175" id="Seg_6720" s="T174">жить.[3SG.S]</ta>
            <ta e="T176" id="Seg_6721" s="T175">человек.[NOM]</ta>
            <ta e="T177" id="Seg_6722" s="T176">как</ta>
            <ta e="T178" id="Seg_6723" s="T177">знать-FUT-3SG.O</ta>
            <ta e="T179" id="Seg_6724" s="T178">другой</ta>
            <ta e="T180" id="Seg_6725" s="T179">жить-PTCP.PRS</ta>
            <ta e="T181" id="Seg_6726" s="T180">человек.[NOM]</ta>
            <ta e="T182" id="Seg_6727" s="T181">ночевать-3PL</ta>
            <ta e="T183" id="Seg_6728" s="T182">мол</ta>
            <ta e="T184" id="Seg_6729" s="T183">три-ORD</ta>
            <ta e="T185" id="Seg_6730" s="T184">день-ADV.LOC</ta>
            <ta e="T186" id="Seg_6731" s="T185">%%</ta>
            <ta e="T187" id="Seg_6732" s="T186">мол</ta>
            <ta e="T188" id="Seg_6733" s="T187">Пан.[NOM]</ta>
            <ta e="T189" id="Seg_6734" s="T188">старик.[NOM]</ta>
            <ta e="T190" id="Seg_6735" s="T189">десять</ta>
            <ta e="T191" id="Seg_6736" s="T190">человек-COM</ta>
            <ta e="T192" id="Seg_6737" s="T191">прийти-CO.[3SG.S]</ta>
            <ta e="T193" id="Seg_6738" s="T192">вроде</ta>
            <ta e="T194" id="Seg_6739" s="T193">олень-ACC</ta>
            <ta e="T195" id="Seg_6740" s="T194">погнать-SUP.2/3SG</ta>
            <ta e="T196" id="Seg_6741" s="T195">этот</ta>
            <ta e="T197" id="Seg_6742" s="T196">тот</ta>
            <ta e="T198" id="Seg_6743" s="T197">отец-DYA-PL-EP-GEN</ta>
            <ta e="T199" id="Seg_6744" s="T198">олень-ACC</ta>
            <ta e="T200" id="Seg_6745" s="T199">погнать-INF</ta>
            <ta e="T201" id="Seg_6746" s="T200">QUEST-GEN</ta>
            <ta e="T202" id="Seg_6747" s="T201">в.меру-ADJZ</ta>
            <ta e="T203" id="Seg_6748" s="T202">чум.[NOM]-3SG</ta>
            <ta e="T204" id="Seg_6749" s="T203">новый</ta>
            <ta e="T205" id="Seg_6750" s="T204">QUEST-GEN</ta>
            <ta e="T206" id="Seg_6751" s="T205">в.меру-ADJZ</ta>
            <ta e="T207" id="Seg_6752" s="T206">что.[NOM]-3SG</ta>
            <ta e="T208" id="Seg_6753" s="T207">всё</ta>
            <ta e="T209" id="Seg_6754" s="T208">собирать-MULO-FUT-3SG.O</ta>
            <ta e="T210" id="Seg_6755" s="T209">раньше</ta>
            <ta e="T211" id="Seg_6756" s="T210">еще</ta>
            <ta e="T212" id="Seg_6757" s="T211">собирать-MULO-MULT-3SG.O</ta>
            <ta e="T213" id="Seg_6758" s="T212">ненец-PL-EP-GEN</ta>
            <ta e="T214" id="Seg_6759" s="T213">олень-ACC</ta>
            <ta e="T215" id="Seg_6760" s="T214">собирать-MULO-CVB</ta>
            <ta e="T216" id="Seg_6761" s="T215">взять-PST.NAR-3SG.O</ta>
            <ta e="T217" id="Seg_6762" s="T216">потом</ta>
            <ta e="T218" id="Seg_6763" s="T217">потом</ta>
            <ta e="T219" id="Seg_6764" s="T218">%%</ta>
            <ta e="T220" id="Seg_6765" s="T219">назад</ta>
            <ta e="T221" id="Seg_6766" s="T220">отправиться-CO-3DU.S</ta>
            <ta e="T222" id="Seg_6767" s="T221">этот</ta>
            <ta e="T223" id="Seg_6768" s="T222">ненец-DU.[NOM]</ta>
            <ta e="T224" id="Seg_6769" s="T223">три-ORD</ta>
            <ta e="T225" id="Seg_6770" s="T224">день-ADV.LOC</ta>
            <ta e="T226" id="Seg_6771" s="T225">мол</ta>
            <ta e="T227" id="Seg_6772" s="T226">прийти-FUT-1PL</ta>
            <ta e="T228" id="Seg_6773" s="T227">вы.PL.NOM</ta>
            <ta e="T229" id="Seg_6774" s="T228">мол</ta>
            <ta e="T230" id="Seg_6775" s="T229">куда</ta>
            <ta e="T231" id="Seg_6776" s="T230">NEG.IMP</ta>
            <ta e="T232" id="Seg_6777" s="T231">отправиться-IMP.2PL</ta>
            <ta e="T233" id="Seg_6778" s="T232">два-ORD</ta>
            <ta e="T234" id="Seg_6779" s="T233">день-ILL</ta>
            <ta e="T235" id="Seg_6780" s="T234">вверх</ta>
            <ta e="T236" id="Seg_6781" s="T235">солнце-TRL-CO.[3SG.S]</ta>
            <ta e="T237" id="Seg_6782" s="T236">это</ta>
            <ta e="T238" id="Seg_6783" s="T237">парень.[NOM]</ta>
            <ta e="T239" id="Seg_6784" s="T238">такой-ADJZ</ta>
            <ta e="T240" id="Seg_6785" s="T239">сказать-INFER-3SG.O</ta>
            <ta e="T241" id="Seg_6786" s="T240">дедушка-OBL.3SG-ALL</ta>
            <ta e="T242" id="Seg_6787" s="T241">будто</ta>
            <ta e="T243" id="Seg_6788" s="T242">вверх.по.течению-ADV.LOC</ta>
            <ta e="T244" id="Seg_6789" s="T243">здесь</ta>
            <ta e="T245" id="Seg_6790" s="T244">четыре</ta>
            <ta e="T246" id="Seg_6791" s="T245">сто</ta>
            <ta e="T247" id="Seg_6792" s="T246">олень-PL.[NOM]</ta>
            <ta e="T248" id="Seg_6793" s="T247">стоять-HAB-TEMPN-COR-ADJZ</ta>
            <ta e="T249" id="Seg_6794" s="T248">тундра.[NOM]-1SG</ta>
            <ta e="T250" id="Seg_6795" s="T249">быть-INFER.[3SG.S]</ta>
            <ta e="T251" id="Seg_6796" s="T250">туда</ta>
            <ta e="T252" id="Seg_6797" s="T251">послать-INCH-1PL</ta>
            <ta e="T253" id="Seg_6798" s="T252">ненец-PL.[NOM]</ta>
            <ta e="T254" id="Seg_6799" s="T253">прийти-FUT-3PL</ta>
            <ta e="T255" id="Seg_6800" s="T254">олень-ACC</ta>
            <ta e="T256" id="Seg_6801" s="T255">погнать-INF-собраться-FUT-3PL</ta>
            <ta e="T257" id="Seg_6802" s="T256">олень-GEN</ta>
            <ta e="T258" id="Seg_6803" s="T257">пастбище-LOC</ta>
            <ta e="T259" id="Seg_6804" s="T258">зима-%%-ABST-GEN-3SG.O</ta>
            <ta e="T260" id="Seg_6805" s="T259">в.течение</ta>
            <ta e="T261" id="Seg_6806" s="T260">зима-VBLZ-PTCP.PST</ta>
            <ta e="T262" id="Seg_6807" s="T261">как</ta>
            <ta e="T263" id="Seg_6808" s="T262">сделать-FUT-1PL</ta>
            <ta e="T264" id="Seg_6809" s="T263">этот</ta>
            <ta e="T265" id="Seg_6810" s="T264">парень.[NOM]</ta>
            <ta e="T266" id="Seg_6811" s="T265">так</ta>
            <ta e="T267" id="Seg_6812" s="T266">сказать-CO-3SG.O</ta>
            <ta e="T268" id="Seg_6813" s="T267">потом</ta>
            <ta e="T269" id="Seg_6814" s="T268">так</ta>
            <ta e="T270" id="Seg_6815" s="T269">сказать-CO-3SG.O</ta>
            <ta e="T271" id="Seg_6816" s="T270">я.ALL</ta>
            <ta e="T272" id="Seg_6817" s="T271">будто</ta>
            <ta e="T273" id="Seg_6818" s="T272">новый</ta>
            <ta e="T274" id="Seg_6819" s="T273">чум.[NOM]-2PL</ta>
            <ta e="T275" id="Seg_6820" s="T274">быть-CO.[3SG.S]</ta>
            <ta e="T276" id="Seg_6821" s="T275">дедушка.[NOM]</ta>
            <ta e="T277" id="Seg_6822" s="T276">я.ALL</ta>
            <ta e="T278" id="Seg_6823" s="T277">дать-IMP.2SG.O</ta>
            <ta e="T279" id="Seg_6824" s="T278">а</ta>
            <ta e="T280" id="Seg_6825" s="T279">тот</ta>
            <ta e="T281" id="Seg_6826" s="T280">тот</ta>
            <ta e="T282" id="Seg_6827" s="T281">брат-GEN-3SG</ta>
            <ta e="T283" id="Seg_6828" s="T282">дочь.[NOM]</ta>
            <ta e="T284" id="Seg_6829" s="T283">дедушка.[NOM]-3SG</ta>
            <ta e="T285" id="Seg_6830" s="T284">этот-%%-ACC</ta>
            <ta e="T286" id="Seg_6831" s="T285">идти-CAUS-DUR-3SG.O</ta>
            <ta e="T287" id="Seg_6832" s="T286">этот</ta>
            <ta e="T289" id="Seg_6833" s="T287">парень-ALL</ta>
            <ta e="T290" id="Seg_6834" s="T289">вверх.по.течению-ADV.LOC</ta>
            <ta e="T291" id="Seg_6835" s="T290">здесь</ta>
            <ta e="T292" id="Seg_6836" s="T291">лес-ADJZ</ta>
            <ta e="T293" id="Seg_6837" s="T292">перешеек.[NOM]</ta>
            <ta e="T294" id="Seg_6838" s="T293">быть-CO.[3SG.S]</ta>
            <ta e="T295" id="Seg_6839" s="T294">там</ta>
            <ta e="T296" id="Seg_6840" s="T295">чум-EP-VBLZ-FUT-1SG.S</ta>
            <ta e="T297" id="Seg_6841" s="T296">новый</ta>
            <ta e="T298" id="Seg_6842" s="T297">дом.[NOM]</ta>
            <ta e="T299" id="Seg_6843" s="T298">принести-IMP.2PL.S</ta>
            <ta e="T300" id="Seg_6844" s="T299">будто</ta>
            <ta e="T301" id="Seg_6845" s="T300">пустить-MULO-CO-3PL</ta>
            <ta e="T302" id="Seg_6846" s="T301">на.другой.день</ta>
            <ta e="T303" id="Seg_6847" s="T302">вроде</ta>
            <ta e="T304" id="Seg_6848" s="T303">вот</ta>
            <ta e="T305" id="Seg_6849" s="T304">три-ORD</ta>
            <ta e="T306" id="Seg_6850" s="T305">день.[NOM]</ta>
            <ta e="T307" id="Seg_6851" s="T306">прийти-FUT-3PL</ta>
            <ta e="T308" id="Seg_6852" s="T307">Пан.[NOM]</ta>
            <ta e="T309" id="Seg_6853" s="T308">старик-ADJZ-нечто-PL.[NOM]</ta>
            <ta e="T310" id="Seg_6854" s="T309">он(а)-EP-PL-EP-ALL</ta>
            <ta e="T311" id="Seg_6855" s="T310">этот</ta>
            <ta e="T312" id="Seg_6856" s="T311">ненец_старик-GEN</ta>
            <ta e="T313" id="Seg_6857" s="T312">олень-ACC</ta>
            <ta e="T314" id="Seg_6858" s="T313">собирать-MULO-INF</ta>
            <ta e="T315" id="Seg_6859" s="T314">брат-DYA-DU-GEN</ta>
            <ta e="T316" id="Seg_6860" s="T315">олень-ACC</ta>
            <ta e="T317" id="Seg_6861" s="T316">потом</ta>
            <ta e="T318" id="Seg_6862" s="T317">в.разные.стороны-ADJZ</ta>
            <ta e="T319" id="Seg_6863" s="T318">пустить-MULO-IPFV-3PL</ta>
            <ta e="T320" id="Seg_6864" s="T319">этот</ta>
            <ta e="T321" id="Seg_6865" s="T320">олень-ACC</ta>
            <ta e="T322" id="Seg_6866" s="T321">INFER</ta>
            <ta e="T323" id="Seg_6867" s="T322">погнать-INFER-3PL</ta>
            <ta e="T324" id="Seg_6868" s="T323">вроде</ta>
            <ta e="T325" id="Seg_6869" s="T324">он(а).[NOM]</ta>
            <ta e="T326" id="Seg_6870" s="T325">тоже</ta>
            <ta e="T327" id="Seg_6871" s="T326">погнать-DUR-3SG.O</ta>
            <ta e="T328" id="Seg_6872" s="T327">чум-INSTR</ta>
            <ta e="T329" id="Seg_6873" s="T328">INFER</ta>
            <ta e="T330" id="Seg_6874" s="T329">дать-INFER-3PL</ta>
            <ta e="T331" id="Seg_6875" s="T330">этот</ta>
            <ta e="T332" id="Seg_6876" s="T331">лес-GEN</ta>
            <ta e="T333" id="Seg_6877" s="T332">перешеек-GEN</ta>
            <ta e="T334" id="Seg_6878" s="T333">вперёд-ADJZ</ta>
            <ta e="T335" id="Seg_6879" s="T334">сторона-LOC</ta>
            <ta e="T336" id="Seg_6880" s="T335">потом</ta>
            <ta e="T337" id="Seg_6881" s="T336">чум-EP-VBLZ-INFER-3PL</ta>
            <ta e="T338" id="Seg_6882" s="T337">этот</ta>
            <ta e="T339" id="Seg_6883" s="T338">жена-OBL.3SG-COM</ta>
            <ta e="T340" id="Seg_6884" s="T339">вроде</ta>
            <ta e="T341" id="Seg_6885" s="T340">богач-ADJZ</ta>
            <ta e="T342" id="Seg_6886" s="T341">ненец.[NOM]</ta>
            <ta e="T343" id="Seg_6887" s="T342">что.[NOM]</ta>
            <ta e="T344" id="Seg_6888" s="T343">чум.[NOM]-3SG</ta>
            <ta e="T345" id="Seg_6889" s="T344">NEG.EX.[3SG.S]</ta>
            <ta e="T346" id="Seg_6890" s="T345">новый</ta>
            <ta e="T347" id="Seg_6891" s="T346">чум.[NOM]-3SG</ta>
            <ta e="T348" id="Seg_6892" s="T347">шкура-ADJZ</ta>
            <ta e="T349" id="Seg_6893" s="T348">чум.[NOM]-3SG</ta>
            <ta e="T350" id="Seg_6894" s="T349">потом</ta>
            <ta e="T351" id="Seg_6895" s="T350">два-ITER.NUM</ta>
            <ta e="T352" id="Seg_6896" s="T351">ночевать-3PL</ta>
            <ta e="T353" id="Seg_6897" s="T352">этот</ta>
            <ta e="T354" id="Seg_6898" s="T353">жена-OBL.3SG-COM</ta>
            <ta e="T355" id="Seg_6899" s="T354">следующий</ta>
            <ta e="T356" id="Seg_6900" s="T355">утро-ADV.LOC</ta>
            <ta e="T357" id="Seg_6901" s="T356">такой-ADVZ</ta>
            <ta e="T358" id="Seg_6902" s="T357">стать.[3SG.S]</ta>
            <ta e="T359" id="Seg_6903" s="T358">тот</ta>
            <ta e="T360" id="Seg_6904" s="T359">жена-OBL.3SG-ALL</ta>
            <ta e="T361" id="Seg_6905" s="T360">я.NOM</ta>
            <ta e="T362" id="Seg_6906" s="T361">будто</ta>
            <ta e="T363" id="Seg_6907" s="T362">вверх.по.течению</ta>
            <ta e="T364" id="Seg_6908" s="T363">человек-PL-ILL.1SG</ta>
            <ta e="T365" id="Seg_6909" s="T364">ходить.за.чем_либо-OPT-1SG.S</ta>
            <ta e="T366" id="Seg_6910" s="T365">то.ли</ta>
            <ta e="T367" id="Seg_6911" s="T366">будто</ta>
            <ta e="T368" id="Seg_6912" s="T367">чум-EP-VBLZ-PST.NAR-3PL</ta>
            <ta e="T369" id="Seg_6913" s="T368">то.ли</ta>
            <ta e="T370" id="Seg_6914" s="T369">куда.деваться-3PL</ta>
            <ta e="T371" id="Seg_6915" s="T370">вверх.по.течению</ta>
            <ta e="T372" id="Seg_6916" s="T371">двигаться-TR-PFV.[3SG.S]</ta>
            <ta e="T373" id="Seg_6917" s="T372">три</ta>
            <ta e="T374" id="Seg_6918" s="T373">бык-ACC</ta>
            <ta e="T375" id="Seg_6919" s="T374">запрягать-CVB</ta>
            <ta e="T376" id="Seg_6920" s="T375">вроде-DIM</ta>
            <ta e="T377" id="Seg_6921" s="T376">три-десять-EP-ADJZ</ta>
            <ta e="T378" id="Seg_6922" s="T377">олень-ACC</ta>
            <ta e="T379" id="Seg_6923" s="T378">остаться-TR-PST.NAR-INFER-3SG.O</ta>
            <ta e="T380" id="Seg_6924" s="T379">парень.[NOM]</ta>
            <ta e="T381" id="Seg_6925" s="T380">вверх.по.течению</ta>
            <ta e="T382" id="Seg_6926" s="T381">уйти-PFV-CO.[3SG.S]</ta>
            <ta e="T383" id="Seg_6927" s="T382">жена.[NOM]-3SG</ta>
            <ta e="T384" id="Seg_6928" s="T383">сам.3SG</ta>
            <ta e="T385" id="Seg_6929" s="T384">остаться.[3SG.S]</ta>
            <ta e="T386" id="Seg_6930" s="T385">вот</ta>
            <ta e="T387" id="Seg_6931" s="T386">сидеть.[3SG.S]</ta>
            <ta e="T388" id="Seg_6932" s="T387">вот</ta>
            <ta e="T389" id="Seg_6933" s="T388">сидеть.[3SG.S]</ta>
            <ta e="T390" id="Seg_6934" s="T389">один</ta>
            <ta e="T391" id="Seg_6935" s="T390">тот-GEN</ta>
            <ta e="T392" id="Seg_6936" s="T391">середина-LOC</ta>
            <ta e="T393" id="Seg_6937" s="T392">день.[NOM]-3SG</ta>
            <ta e="T394" id="Seg_6938" s="T393">вечер-ILL</ta>
            <ta e="T395" id="Seg_6939" s="T394">вот</ta>
            <ta e="T396" id="Seg_6940" s="T395">уйти-CO.[3SG.S]</ta>
            <ta e="T397" id="Seg_6941" s="T396">этот</ta>
            <ta e="T398" id="Seg_6942" s="T397">парень.[NOM]</ta>
            <ta e="T399" id="Seg_6943" s="T398">тихий-ADVZ</ta>
            <ta e="T400" id="Seg_6944" s="T399">почувствовать-CO.[3SG.S]</ta>
            <ta e="T401" id="Seg_6945" s="T400">вверх.по.течению-ADV.LOC</ta>
            <ta e="T402" id="Seg_6946" s="T401">один</ta>
            <ta e="T403" id="Seg_6947" s="T402">тот-GEN</ta>
            <ta e="T404" id="Seg_6948" s="T403">середина-LOC</ta>
            <ta e="T405" id="Seg_6949" s="T404">человек.[NOM]</ta>
            <ta e="T406" id="Seg_6950" s="T405">это-DIM-ACC</ta>
            <ta e="T407" id="Seg_6951" s="T406">принести-INFER-3PL</ta>
            <ta e="T408" id="Seg_6952" s="T407">вниз.по.течению.реки-ADV.EL</ta>
            <ta e="T409" id="Seg_6953" s="T408">Пан.[NOM]</ta>
            <ta e="T410" id="Seg_6954" s="T409">старик-ADJZ-нечто-PL.[NOM]</ta>
            <ta e="T411" id="Seg_6955" s="T410">принести-INFER-3PL</ta>
            <ta e="T412" id="Seg_6956" s="T411">десять</ta>
            <ta e="T413" id="Seg_6957" s="T412">человек.[NOM]-3SG</ta>
            <ta e="T414" id="Seg_6958" s="T413">вроде-DIM</ta>
            <ta e="T415" id="Seg_6959" s="T414">олень-ACC</ta>
            <ta e="T416" id="Seg_6960" s="T415">погнать-DEB-PTCP.PRS</ta>
            <ta e="T417" id="Seg_6961" s="T416">Пан.[NOM]</ta>
            <ta e="T418" id="Seg_6962" s="T417">старик.[NOM]</ta>
            <ta e="T419" id="Seg_6963" s="T418">столько</ta>
            <ta e="T420" id="Seg_6964" s="T419">шаманская.мудрость-COM</ta>
            <ta e="T421" id="Seg_6965" s="T420">быть-CO.[3SG.S]</ta>
            <ta e="T422" id="Seg_6966" s="T421">потом</ta>
            <ta e="T423" id="Seg_6967" s="T422">два-ITER.NUM</ta>
            <ta e="T424" id="Seg_6968" s="T423">этот</ta>
            <ta e="T425" id="Seg_6969" s="T424">парень.[NOM]</ta>
            <ta e="T426" id="Seg_6970" s="T425">прийти-PFV-AUD.[3SG.S]</ta>
            <ta e="T427" id="Seg_6971" s="T426">жена.[NOM]-3SG</ta>
            <ta e="T428" id="Seg_6972" s="T427">слушать-TR-PST.NAR-3SG.O</ta>
            <ta e="T429" id="Seg_6973" s="T428">видать</ta>
            <ta e="T430" id="Seg_6974" s="T429">едва</ta>
            <ta e="T431" id="Seg_6975" s="T430">прийти-PFV.[3SG.S]</ta>
            <ta e="T432" id="Seg_6976" s="T431">Пан.[NOM]</ta>
            <ta e="T433" id="Seg_6977" s="T432">старик-ADJZ-нечто-PL.[NOM]</ta>
            <ta e="T434" id="Seg_6978" s="T433">что.ли</ta>
            <ta e="T435" id="Seg_6979" s="T434">видать</ta>
            <ta e="T436" id="Seg_6980" s="T435">прийти-CO-3PL</ta>
            <ta e="T437" id="Seg_6981" s="T436">нарты-ADJZ</ta>
            <ta e="T438" id="Seg_6982" s="T437">нечто.[NOM]-3SG</ta>
            <ta e="T439" id="Seg_6983" s="T438">такой-ADVZ</ta>
            <ta e="T440" id="Seg_6984" s="T439">положить-MULO-CO-3PL</ta>
            <ta e="T441" id="Seg_6985" s="T440">пространство.снаружи-LOC</ta>
            <ta e="T442" id="Seg_6986" s="T441">улица-LOC</ta>
            <ta e="T443" id="Seg_6987" s="T442">чум-ILL</ta>
            <ta e="T444" id="Seg_6988" s="T443">войти-CO.[3SG.S]</ta>
            <ta e="T445" id="Seg_6989" s="T444">дверь-GEN</ta>
            <ta e="T446" id="Seg_6990" s="T445">отверстие-ADV.LOC</ta>
            <ta e="T447" id="Seg_6991" s="T446">стоять-HAB-CVB</ta>
            <ta e="T448" id="Seg_6992" s="T447">дверь-GEN</ta>
            <ta e="T449" id="Seg_6993" s="T448">отверстие-ADVZ</ta>
            <ta e="T450" id="Seg_6994" s="T449">%%</ta>
            <ta e="T451" id="Seg_6995" s="T450">нога-ACC-3SG</ta>
            <ta e="T452" id="Seg_6996" s="T451">трясти-MOM-HAB-3SG.O</ta>
            <ta e="T453" id="Seg_6997" s="T452">бокари-ACC-3SG</ta>
            <ta e="T454" id="Seg_6998" s="T453">INFER</ta>
            <ta e="T455" id="Seg_6999" s="T454">трясти-MOM-INFER-3SG.O</ta>
            <ta e="T456" id="Seg_7000" s="T455">так</ta>
            <ta e="T457" id="Seg_7001" s="T456">сказать-CO-3SG.O</ta>
            <ta e="T458" id="Seg_7002" s="T457">Пан.[NOM]</ta>
            <ta e="T459" id="Seg_7003" s="T458">старик-ALL</ta>
            <ta e="T460" id="Seg_7004" s="T459">вперёд</ta>
            <ta e="T461" id="Seg_7005" s="T460">повернуть-TR-CVB</ta>
            <ta e="T462" id="Seg_7006" s="T461">рот.[NOM]-2SG</ta>
            <ta e="T463" id="Seg_7007" s="T462">словно</ta>
            <ta e="T464" id="Seg_7008" s="T463">только</ta>
            <ta e="T465" id="Seg_7009" s="T464">большая.река-GEN</ta>
            <ta e="T466" id="Seg_7010" s="T465">прорубь-GEN</ta>
            <ta e="T467" id="Seg_7011" s="T466">будто</ta>
            <ta e="T468" id="Seg_7012" s="T467">на.другой.день</ta>
            <ta e="T469" id="Seg_7013" s="T468">вот</ta>
            <ta e="T470" id="Seg_7014" s="T469">вперёд</ta>
            <ta e="T471" id="Seg_7015" s="T470">прыгнуть-PST.NAR.[3SG.S]</ta>
            <ta e="T472" id="Seg_7016" s="T471">INFER</ta>
            <ta e="T473" id="Seg_7017" s="T472">кулак-INSTR</ta>
            <ta e="T474" id="Seg_7018" s="T473">гладить-MOM-PFV-PST.NAR-INFER-3SG.O</ta>
            <ta e="T475" id="Seg_7019" s="T474">Пан.[NOM]</ta>
            <ta e="T476" id="Seg_7020" s="T475">старик-ADJZ</ta>
            <ta e="T477" id="Seg_7021" s="T476">нечто-PL-EP-ACC</ta>
            <ta e="T478" id="Seg_7022" s="T477">всё</ta>
            <ta e="T479" id="Seg_7023" s="T478">плоско</ta>
            <ta e="T480" id="Seg_7024" s="T479">ударить-PST.NAR-3SG.O</ta>
            <ta e="T481" id="Seg_7025" s="T480">некоторый.[NOM]-3SG</ta>
            <ta e="T482" id="Seg_7026" s="T481">умереть-PST.NAR.[3SG.S]</ta>
            <ta e="T483" id="Seg_7027" s="T482">Пан.[NOM]</ta>
            <ta e="T484" id="Seg_7028" s="T483">старик.[NOM]</ta>
            <ta e="T485" id="Seg_7029" s="T484">жить-CVB</ta>
            <ta e="T486" id="Seg_7030" s="T485">остаться-PST.NAR.[3SG.S]</ta>
            <ta e="T487" id="Seg_7031" s="T486">два-DU.[NOM]</ta>
            <ta e="T488" id="Seg_7032" s="T487">жить-CVB</ta>
            <ta e="T489" id="Seg_7033" s="T488">остаться-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T490" id="Seg_7034" s="T489">наружу</ta>
            <ta e="T491" id="Seg_7035" s="T490">бросать-MULO-PFV-3SG.O</ta>
            <ta e="T492" id="Seg_7036" s="T491">этот</ta>
            <ta e="T493" id="Seg_7037" s="T492">парень.[NOM]</ta>
            <ta e="T494" id="Seg_7038" s="T493">один</ta>
            <ta e="T495" id="Seg_7039" s="T494">раз.[NOM]</ta>
            <ta e="T496" id="Seg_7040" s="T495">кулак-INSTR</ta>
            <ta e="T497" id="Seg_7041" s="T496">гладить-MOM-PFV-PST.NAR-3SG.O</ta>
            <ta e="T498" id="Seg_7042" s="T497">всё</ta>
            <ta e="T499" id="Seg_7043" s="T498">одежда-GEN-3SG</ta>
            <ta e="T500" id="Seg_7044" s="T499">внутри-ADV.ILL</ta>
            <ta e="T501" id="Seg_7045" s="T500">штаны-GEN-3SG</ta>
            <ta e="T502" id="Seg_7046" s="T501">в</ta>
            <ta e="T503" id="Seg_7047" s="T502">%%-DRV-PST.NAR-3PL</ta>
            <ta e="T504" id="Seg_7048" s="T503">этот</ta>
            <ta e="T505" id="Seg_7049" s="T504">умереть-PTCP.PST-нечто-PL.[NOM]</ta>
            <ta e="T506" id="Seg_7050" s="T505">потом</ta>
            <ta e="T507" id="Seg_7051" s="T506">потом</ta>
            <ta e="T508" id="Seg_7052" s="T507">наружу</ta>
            <ta e="T509" id="Seg_7053" s="T508">бросать-MULO-PFV-3SG.O</ta>
            <ta e="T510" id="Seg_7054" s="T509">улица-ILL</ta>
            <ta e="T511" id="Seg_7055" s="T510">тот</ta>
            <ta e="T512" id="Seg_7056" s="T511">нарты.[NOM]</ta>
            <ta e="T513" id="Seg_7057" s="T512">палка.[NOM]</ta>
            <ta e="T514" id="Seg_7058" s="T513">лыжи.[NOM]-3SG</ta>
            <ta e="T515" id="Seg_7059" s="T514">вот</ta>
            <ta e="T516" id="Seg_7060" s="T515">привязать-MULS-RES.[3SG.S]</ta>
            <ta e="T517" id="Seg_7061" s="T516">Пан.[NOM]</ta>
            <ta e="T518" id="Seg_7062" s="T517">старик-ACC</ta>
            <ta e="T519" id="Seg_7063" s="T518">тоже</ta>
            <ta e="T520" id="Seg_7064" s="T519">наружу</ta>
            <ta e="T521" id="Seg_7065" s="T520">взять-TR-PST.NAR-3SG.O</ta>
            <ta e="T522" id="Seg_7066" s="T521">бросать-PST.NAR-3SG.O</ta>
            <ta e="T523" id="Seg_7067" s="T522">вроде</ta>
            <ta e="T524" id="Seg_7068" s="T523">всё</ta>
            <ta e="T525" id="Seg_7069" s="T524">наружу</ta>
            <ta e="T526" id="Seg_7070" s="T525">взять-TR-PST.NAR-3SG.O</ta>
            <ta e="T527" id="Seg_7071" s="T526">вниз</ta>
            <ta e="T528" id="Seg_7072" s="T527">сесть.[3SG.S]</ta>
            <ta e="T529" id="Seg_7073" s="T528">малица-ADJZ</ta>
            <ta e="T530" id="Seg_7074" s="T529">товар.ACC-3SG</ta>
            <ta e="T531" id="Seg_7075" s="T530">прочь</ta>
            <ta e="T532" id="Seg_7076" s="T531">взять-CVB</ta>
            <ta e="T533" id="Seg_7077" s="T532">съесть-EP-FRQ-CVB</ta>
            <ta e="T534" id="Seg_7078" s="T533">вниз</ta>
            <ta e="T535" id="Seg_7079" s="T534">сесть-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T536" id="Seg_7080" s="T535">ночевать.[3SG.S]</ta>
            <ta e="T537" id="Seg_7081" s="T536">этот</ta>
            <ta e="T538" id="Seg_7082" s="T537">человек-PL.[NOM]-3SG</ta>
            <ta e="T539" id="Seg_7083" s="T538">пространство.снаружи-LOC</ta>
            <ta e="T540" id="Seg_7084" s="T539">вот</ta>
            <ta e="T541" id="Seg_7085" s="T540">лежать-MULS-DUR-3PL</ta>
            <ta e="T542" id="Seg_7086" s="T541">как</ta>
            <ta e="T543" id="Seg_7087" s="T542">привязать-RFL-RES.[3SG.S]</ta>
            <ta e="T544" id="Seg_7088" s="T543">такой-ADVZ</ta>
            <ta e="T545" id="Seg_7089" s="T544">ночевать.[3SG.S]</ta>
            <ta e="T546" id="Seg_7090" s="T545">следующий</ta>
            <ta e="T547" id="Seg_7091" s="T546">утро-ADV.LOC</ta>
            <ta e="T548" id="Seg_7092" s="T547">вверх</ta>
            <ta e="T549" id="Seg_7093" s="T548">встать.[3SG.S]</ta>
            <ta e="T550" id="Seg_7094" s="T549">Пан.[NOM]</ta>
            <ta e="T551" id="Seg_7095" s="T550">старик.[NOM]</ta>
            <ta e="T552" id="Seg_7096" s="T551">такой-ADVZ</ta>
            <ta e="T553" id="Seg_7097" s="T552">этот.[NOM]</ta>
            <ta e="T554" id="Seg_7098" s="T553">лицо-ADJZ</ta>
            <ta e="T555" id="Seg_7099" s="T554">всё.прочее.[NOM]-3SG</ta>
            <ta e="T556" id="Seg_7100" s="T555">целый-EP-ADVZ</ta>
            <ta e="T557" id="Seg_7101" s="T556">распухнуть-RFL.PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T558" id="Seg_7102" s="T557">еле.еле</ta>
            <ta e="T559" id="Seg_7103" s="T558">глаз-DIM.[NOM]-3SG</ta>
            <ta e="T560" id="Seg_7104" s="T559">увидеть-RFL-DUR.[3SG.S]</ta>
            <ta e="T561" id="Seg_7105" s="T560">тот</ta>
            <ta e="T562" id="Seg_7106" s="T561">ненец-DU.[NOM]-3SG</ta>
            <ta e="T563" id="Seg_7107" s="T562">еще</ta>
            <ta e="T564" id="Seg_7108" s="T563">хороший-в.некоторой.степени</ta>
            <ta e="T565" id="Seg_7109" s="T564">быть-DUR-PST.NAR.[3SG.S]</ta>
            <ta e="T566" id="Seg_7110" s="T565">такой-ADJZ</ta>
            <ta e="T567" id="Seg_7111" s="T566">сказать-CO-3SG.O</ta>
            <ta e="T568" id="Seg_7112" s="T567">этот</ta>
            <ta e="T569" id="Seg_7113" s="T568">парень.[NOM]</ta>
            <ta e="T570" id="Seg_7114" s="T569">будто</ta>
            <ta e="T571" id="Seg_7115" s="T570">этот</ta>
            <ta e="T572" id="Seg_7116" s="T571">ненец-PL-EP-ALL</ta>
            <ta e="T573" id="Seg_7117" s="T572">домой</ta>
            <ta e="T574" id="Seg_7118" s="T573">отправиться-TR-3PL</ta>
            <ta e="T575" id="Seg_7119" s="T574">%%</ta>
            <ta e="T576" id="Seg_7120" s="T575">сам.3PL</ta>
            <ta e="T577" id="Seg_7121" s="T576">нарты-OBL.2SG-INSTR</ta>
            <ta e="T578" id="Seg_7122" s="T577">потом</ta>
            <ta e="T579" id="Seg_7123" s="T578">два-ITER.NUM</ta>
            <ta e="T580" id="Seg_7124" s="T579">тот</ta>
            <ta e="T581" id="Seg_7125" s="T580">мы.DU.NOM-DU</ta>
            <ta e="T582" id="Seg_7126" s="T581">чум-ILL</ta>
            <ta e="T583" id="Seg_7127" s="T582">войти-EP-US-PST.NAR-3PL</ta>
            <ta e="T584" id="Seg_7128" s="T583">то.ли</ta>
            <ta e="T585" id="Seg_7129" s="T584">съесть-EP-FRQ-PST.NAR-3PL</ta>
            <ta e="T586" id="Seg_7130" s="T585">то.ли</ta>
            <ta e="T587" id="Seg_7131" s="T586">NEG</ta>
            <ta e="T588" id="Seg_7132" s="T587">потом</ta>
            <ta e="T589" id="Seg_7133" s="T588">тот</ta>
            <ta e="T590" id="Seg_7134" s="T589">завязать-MOM-DUR-PTCP.PRS</ta>
            <ta e="T591" id="Seg_7135" s="T590">нарты-ACC-3PL</ta>
            <ta e="T592" id="Seg_7136" s="T591">потом</ta>
            <ta e="T593" id="Seg_7137" s="T592">домой</ta>
            <ta e="T594" id="Seg_7138" s="T593">%%-CVB</ta>
            <ta e="T595" id="Seg_7139" s="T594">этот</ta>
            <ta e="T596" id="Seg_7140" s="T595">человек-PL-ACC-3PL</ta>
            <ta e="T597" id="Seg_7141" s="T596">умереть-PTCP.PST</ta>
            <ta e="T598" id="Seg_7142" s="T597">человек-PL-ACC-3SG</ta>
            <ta e="T599" id="Seg_7143" s="T598">так</ta>
            <ta e="T600" id="Seg_7144" s="T599">отправиться-TR-CO-3SG.O</ta>
            <ta e="T601" id="Seg_7145" s="T600">вниз.по.течению.реки</ta>
            <ta e="T602" id="Seg_7146" s="T601">домой</ta>
            <ta e="T603" id="Seg_7147" s="T602">Пан.[NOM]</ta>
            <ta e="T604" id="Seg_7148" s="T603">старик-ACC-3SG</ta>
            <ta e="T605" id="Seg_7149" s="T604">тоже</ta>
            <ta e="T606" id="Seg_7150" s="T605">%%-CVB</ta>
            <ta e="T607" id="Seg_7151" s="T606">отправиться-TR-3SG.O</ta>
            <ta e="T608" id="Seg_7152" s="T607">что.[NOM]</ta>
            <ta e="T609" id="Seg_7153" s="T608">еле.еле</ta>
            <ta e="T611" id="Seg_7154" s="T610">отправиться-TR-3SG.O</ta>
            <ta e="T612" id="Seg_7155" s="T611">отправиться-CO-3PL</ta>
            <ta e="T613" id="Seg_7156" s="T612">тот</ta>
            <ta e="T614" id="Seg_7157" s="T613">человек-EP-PL.[NOM]</ta>
            <ta e="T615" id="Seg_7158" s="T614">хороший-ADVZ</ta>
            <ta e="T616" id="Seg_7159" s="T615">уйти-CO-3PL</ta>
            <ta e="T617" id="Seg_7160" s="T616">жена-OBL.3SG-ALL</ta>
            <ta e="T618" id="Seg_7161" s="T617">так</ta>
            <ta e="T619" id="Seg_7162" s="T618">сказать-CO-3SG.O</ta>
            <ta e="T620" id="Seg_7163" s="T619">чум.[NOM]-2SG</ta>
            <ta e="T621" id="Seg_7164" s="T620">прочь</ta>
            <ta e="T622" id="Seg_7165" s="T621">снять-IMP.2SG.O</ta>
            <ta e="T623" id="Seg_7166" s="T622">мы.PL.NOM</ta>
            <ta e="T624" id="Seg_7167" s="T623">вверх.по.течению</ta>
            <ta e="T625" id="Seg_7168" s="T624">отправиться-OPT</ta>
            <ta e="T626" id="Seg_7169" s="T625">человек-EP-PL-EP-ALL</ta>
            <ta e="T627" id="Seg_7170" s="T626">NEG</ta>
            <ta e="T628" id="Seg_7171" s="T627">знать-PST.NAR-3SG.O</ta>
            <ta e="T629" id="Seg_7172" s="T628">дедушка-OBL.3SG-ADJZ</ta>
            <ta e="T630" id="Seg_7173" s="T629">нечто.[NOM]-3SG</ta>
            <ta e="T631" id="Seg_7174" s="T630">INDEF3</ta>
            <ta e="T632" id="Seg_7175" s="T631">что.[NOM]</ta>
            <ta e="T633" id="Seg_7176" s="T632">вверх.по.течению</ta>
            <ta e="T634" id="Seg_7177" s="T633">уйти-CO.[3SG.S]</ta>
            <ta e="T635" id="Seg_7178" s="T634">тот</ta>
            <ta e="T636" id="Seg_7179" s="T635">сам.3SG</ta>
            <ta e="T637" id="Seg_7180" s="T636">NEG</ta>
            <ta e="T638" id="Seg_7181" s="T637">сказать-DUR.[3SG.S]</ta>
            <ta e="T639" id="Seg_7182" s="T638">тот</ta>
            <ta e="T640" id="Seg_7183" s="T639">женщина.[NOM]-3SG</ta>
            <ta e="T641" id="Seg_7184" s="T640">INFER</ta>
            <ta e="T642" id="Seg_7185" s="T641">сказать-MULO-PST.NAR-INFER-3SG.O</ta>
            <ta e="T643" id="Seg_7186" s="T642">отец-OBL.3SG-ADJZ</ta>
            <ta e="T644" id="Seg_7187" s="T643">дедушка-OBL.3SG-ADJZ</ta>
            <ta e="T645" id="Seg_7188" s="T644">нечто-DU-ALL</ta>
            <ta e="T646" id="Seg_7189" s="T645">старик-ADJZ</ta>
            <ta e="T647" id="Seg_7190" s="T646">нечто-PL.[NOM]</ta>
            <ta e="T648" id="Seg_7191" s="T647">домой</ta>
            <ta e="T649" id="Seg_7192" s="T648">уйти-PST-3PL</ta>
            <ta e="T650" id="Seg_7193" s="T649">а</ta>
            <ta e="T651" id="Seg_7194" s="T650">потом</ta>
            <ta e="T652" id="Seg_7195" s="T651">два-ADJZ</ta>
            <ta e="T653" id="Seg_7196" s="T652">%%-DIM</ta>
            <ta e="T654" id="Seg_7197" s="T653">%%</ta>
            <ta e="T655" id="Seg_7198" s="T654">NEG</ta>
            <ta e="T656" id="Seg_7199" s="T655">что.[NOM]</ta>
            <ta e="T657" id="Seg_7200" s="T656">NEG</ta>
            <ta e="T658" id="Seg_7201" s="T657">знать-3PL</ta>
            <ta e="T659" id="Seg_7202" s="T658">мда</ta>
            <ta e="T660" id="Seg_7203" s="T659">уйти-CO.[3SG.S]</ta>
            <ta e="T661" id="Seg_7204" s="T660">он(а)-EP-PL.[NOM]</ta>
            <ta e="T662" id="Seg_7205" s="T661">дедушка-GEN-OBL.3SG</ta>
            <ta e="T663" id="Seg_7206" s="T662">чум-ILL</ta>
            <ta e="T664" id="Seg_7207" s="T663">войти-CO-3DU.S</ta>
            <ta e="T665" id="Seg_7208" s="T664">нарты-ADJZ</ta>
            <ta e="T666" id="Seg_7209" s="T665">всё.прочее.[NOM]-3SG</ta>
            <ta e="T667" id="Seg_7210" s="T666">замотать-CVB</ta>
            <ta e="T668" id="Seg_7211" s="T667">такой-ADVZ</ta>
            <ta e="T669" id="Seg_7212" s="T668">положить-3PL</ta>
            <ta e="T670" id="Seg_7213" s="T669">тот</ta>
            <ta e="T671" id="Seg_7214" s="T670">уйти-PST.NAR-INFER-3PL</ta>
            <ta e="T672" id="Seg_7215" s="T671">домой</ta>
            <ta e="T673" id="Seg_7216" s="T672">INFER</ta>
            <ta e="T674" id="Seg_7217" s="T673">уйти-PST.NAR-INFER-3PL</ta>
            <ta e="T675" id="Seg_7218" s="T674">Пан.[NOM]</ta>
            <ta e="T676" id="Seg_7219" s="T675">старик.[NOM]</ta>
            <ta e="T677" id="Seg_7220" s="T676">в.низовье.реки</ta>
            <ta e="T678" id="Seg_7221" s="T677">только</ta>
            <ta e="T679" id="Seg_7222" s="T678">дом-ILL-OBL.3SG</ta>
            <ta e="T680" id="Seg_7223" s="T679">прийти-US-PST.NAR.[3SG.S]</ta>
            <ta e="T681" id="Seg_7224" s="T680">вниз</ta>
            <ta e="T682" id="Seg_7225" s="T681">умереть-PST.NAR.[3SG.S]</ta>
            <ta e="T683" id="Seg_7226" s="T682">тот</ta>
            <ta e="T684" id="Seg_7227" s="T683">два</ta>
            <ta e="T685" id="Seg_7228" s="T684">человек-EP-DU.[NOM]</ta>
            <ta e="T686" id="Seg_7229" s="T685">жить-PST.NAR-3DU.S</ta>
            <ta e="T687" id="Seg_7230" s="T686">Пан.[NOM]</ta>
            <ta e="T688" id="Seg_7231" s="T687">старик.[NOM]</ta>
            <ta e="T689" id="Seg_7232" s="T688">имя.[NOM]-3SG</ta>
            <ta e="T690" id="Seg_7233" s="T689">быть-CO.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_7234" s="T0">num</ta>
            <ta e="T2" id="Seg_7235" s="T1">n-n:case-n:poss</ta>
            <ta e="T3" id="Seg_7236" s="T2">v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T4" id="Seg_7237" s="T3">n-n&gt;n-n&gt;adj</ta>
            <ta e="T5" id="Seg_7238" s="T4">n-n&gt;n-n&gt;adj</ta>
            <ta e="T6" id="Seg_7239" s="T5">dem</ta>
            <ta e="T7" id="Seg_7240" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_7241" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_7242" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_7243" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_7244" s="T10">v-v:tense-v:pn</ta>
            <ta e="T12" id="Seg_7245" s="T11">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_7246" s="T12">dem</ta>
            <ta e="T14" id="Seg_7247" s="T13">n-n:ins-n&gt;adj</ta>
            <ta e="T15" id="Seg_7248" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_7249" s="T15">adj</ta>
            <ta e="T17" id="Seg_7250" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_7251" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_7252" s="T18">v-v:tense-v:pn</ta>
            <ta e="T20" id="Seg_7253" s="T19">n-n&gt;n-n&gt;adj</ta>
            <ta e="T21" id="Seg_7254" s="T20">n-n:case</ta>
            <ta e="T22" id="Seg_7255" s="T21">adv-n:case</ta>
            <ta e="T23" id="Seg_7256" s="T22">v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_7257" s="T23">dem</ta>
            <ta e="T25" id="Seg_7258" s="T24">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T26" id="Seg_7259" s="T25">num</ta>
            <ta e="T27" id="Seg_7260" s="T26">n-n:case-n:poss</ta>
            <ta e="T28" id="Seg_7261" s="T27">n-n&gt;n-n&gt;adj</ta>
            <ta e="T29" id="Seg_7262" s="T28">n-n&gt;n-n&gt;adj</ta>
            <ta e="T30" id="Seg_7263" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_7264" s="T30">n-n:case-n:poss</ta>
            <ta e="T32" id="Seg_7265" s="T31">v-v:pn</ta>
            <ta e="T33" id="Seg_7266" s="T32">ptcl</ta>
            <ta e="T34" id="Seg_7267" s="T33">interrog-n:case-n:poss</ta>
            <ta e="T35" id="Seg_7268" s="T34">v-v:pn</ta>
            <ta e="T36" id="Seg_7269" s="T35">adv</ta>
            <ta e="T37" id="Seg_7270" s="T36">n-n:case-n:poss</ta>
            <ta e="T38" id="Seg_7271" s="T37">v-v&gt;v-v:pn</ta>
            <ta e="T39" id="Seg_7272" s="T38">dem</ta>
            <ta e="T40" id="Seg_7273" s="T39">n-n&gt;n-n:case</ta>
            <ta e="T41" id="Seg_7274" s="T40">dem</ta>
            <ta e="T42" id="Seg_7275" s="T41">n-n:case</ta>
            <ta e="T43" id="Seg_7276" s="T42">ptcl-adj&gt;adj</ta>
            <ta e="T44" id="Seg_7277" s="T43">adv</ta>
            <ta e="T45" id="Seg_7278" s="T44">v-v:pn</ta>
            <ta e="T46" id="Seg_7279" s="T45">n-n:obl.poss-n:case</ta>
            <ta e="T47" id="Seg_7280" s="T46">n-n:obl.poss-n:case</ta>
            <ta e="T48" id="Seg_7281" s="T47">dem-adv:case</ta>
            <ta e="T49" id="Seg_7282" s="T48">num</ta>
            <ta e="T50" id="Seg_7283" s="T49">n-n&gt;adj</ta>
            <ta e="T51" id="Seg_7284" s="T50">n-n&gt;n-n:num-n:case</ta>
            <ta e="T52" id="Seg_7285" s="T51">v-v:tense-v:mood-v:pn</ta>
            <ta e="T693" id="Seg_7286" s="T52">n-n:case</ta>
            <ta e="T53" id="Seg_7287" s="T693">n-n:case</ta>
            <ta e="T54" id="Seg_7288" s="T53">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T55" id="Seg_7289" s="T54">dem</ta>
            <ta e="T56" id="Seg_7290" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_7291" s="T56">preverb</ta>
            <ta e="T58" id="Seg_7292" s="T57">v-v:ins-v:pn</ta>
            <ta e="T59" id="Seg_7293" s="T58">dem</ta>
            <ta e="T694" id="Seg_7294" s="T59">n-n:case</ta>
            <ta e="T60" id="Seg_7295" s="T694">n-n:case</ta>
            <ta e="T61" id="Seg_7296" s="T60">interrog</ta>
            <ta e="T62" id="Seg_7297" s="T61">qv-v:tense-v:pn</ta>
            <ta e="T63" id="Seg_7298" s="T62">adv</ta>
            <ta e="T64" id="Seg_7299" s="T63">n-n:obl.poss-n:case</ta>
            <ta e="T65" id="Seg_7300" s="T64">v-v:ins-v:pn</ta>
            <ta e="T66" id="Seg_7301" s="T65">n-n:case</ta>
            <ta e="T67" id="Seg_7302" s="T66">v-v:tense.mood-v:pn</ta>
            <ta e="T68" id="Seg_7303" s="T67">preverb</ta>
            <ta e="T69" id="Seg_7304" s="T68">v-v:mood.pn</ta>
            <ta e="T70" id="Seg_7305" s="T69">adv</ta>
            <ta e="T71" id="Seg_7306" s="T70">n-n:case-n:poss</ta>
            <ta e="T72" id="Seg_7307" s="T71">v-v&gt;v-v&gt;adv</ta>
            <ta e="T73" id="Seg_7308" s="T72">ptcl</ta>
            <ta e="T74" id="Seg_7309" s="T73">preverb</ta>
            <ta e="T75" id="Seg_7310" s="T74">v-v:ins-v:pn</ta>
            <ta e="T76" id="Seg_7311" s="T75">n-n&gt;adj</ta>
            <ta e="T77" id="Seg_7312" s="T76">n-n:num-n:case</ta>
            <ta e="T78" id="Seg_7313" s="T77">n-n:case-n:poss</ta>
            <ta e="T79" id="Seg_7314" s="T78">n-n&gt;adj</ta>
            <ta e="T80" id="Seg_7315" s="T79">n-n&gt;adj</ta>
            <ta e="T81" id="Seg_7316" s="T80">v-v:pn</ta>
            <ta e="T82" id="Seg_7317" s="T81">interrog</ta>
            <ta e="T83" id="Seg_7318" s="T82">qv-v:tense-v:pn</ta>
            <ta e="T84" id="Seg_7319" s="T83">dem</ta>
            <ta e="T695" id="Seg_7320" s="T84">n-n:case</ta>
            <ta e="T85" id="Seg_7321" s="T695">n-n:case</ta>
            <ta e="T86" id="Seg_7322" s="T85">num-num&gt;adv</ta>
            <ta e="T87" id="Seg_7323" s="T86">adv</ta>
            <ta e="T88" id="Seg_7324" s="T87">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T89" id="Seg_7325" s="T88">adj</ta>
            <ta e="T90" id="Seg_7326" s="T89">n-n:case-n:poss</ta>
            <ta e="T91" id="Seg_7327" s="T90">dem-adj&gt;adj</ta>
            <ta e="T92" id="Seg_7328" s="T91">n-n:case-n:poss</ta>
            <ta e="T93" id="Seg_7329" s="T92">v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T94" id="Seg_7330" s="T93">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T95" id="Seg_7331" s="T94">n-n:case-n:poss</ta>
            <ta e="T96" id="Seg_7332" s="T95">conj</ta>
            <ta e="T97" id="Seg_7333" s="T96">v-v:pn</ta>
            <ta e="T98" id="Seg_7334" s="T97">n-n&gt;adj-adj&gt;adv</ta>
            <ta e="T99" id="Seg_7335" s="T98">n-n:case</ta>
            <ta e="T100" id="Seg_7336" s="T99">v-v&gt;v-v:pn</ta>
            <ta e="T101" id="Seg_7337" s="T100">adv</ta>
            <ta e="T102" id="Seg_7338" s="T101">v-v&gt;v-v:pn</ta>
            <ta e="T103" id="Seg_7339" s="T102">v-v&gt;v-v:pn</ta>
            <ta e="T104" id="Seg_7340" s="T103">adv</ta>
            <ta e="T105" id="Seg_7341" s="T104">v-v&gt;adv</ta>
            <ta e="T106" id="Seg_7342" s="T105">n-n:case</ta>
            <ta e="T107" id="Seg_7343" s="T106">n-n:case-n:poss</ta>
            <ta e="T691" id="Seg_7344" s="T107">n-n:case</ta>
            <ta e="T108" id="Seg_7345" s="T691">n-n:case</ta>
            <ta e="T109" id="Seg_7346" s="T108">n-n:case-n:poss</ta>
            <ta e="T110" id="Seg_7347" s="T109">v-v:tense-v:pn</ta>
            <ta e="T111" id="Seg_7348" s="T110">n-n&gt;n-n&gt;adj</ta>
            <ta e="T112" id="Seg_7349" s="T111">n-n:case-n:poss</ta>
            <ta e="T113" id="Seg_7350" s="T112">dem</ta>
            <ta e="T114" id="Seg_7351" s="T113">n-n&gt;adj</ta>
            <ta e="T115" id="Seg_7352" s="T114">n-n:case-n:poss</ta>
            <ta e="T116" id="Seg_7353" s="T115">num</ta>
            <ta e="T117" id="Seg_7354" s="T116">n-n:case-n:poss</ta>
            <ta e="T118" id="Seg_7355" s="T117">n-n&gt;n-n&gt;adj</ta>
            <ta e="T119" id="Seg_7356" s="T118">adv-adv:case</ta>
            <ta e="T120" id="Seg_7357" s="T119">adv</ta>
            <ta e="T121" id="Seg_7358" s="T120">num</ta>
            <ta e="T122" id="Seg_7359" s="T121">n-n:case</ta>
            <ta e="T123" id="Seg_7360" s="T122">v-v:ins-v:pn</ta>
            <ta e="T124" id="Seg_7361" s="T123">n-n&gt;n-n&gt;adj</ta>
            <ta e="T125" id="Seg_7362" s="T124">n-n:case</ta>
            <ta e="T126" id="Seg_7363" s="T125">dem</ta>
            <ta e="T692" id="Seg_7364" s="T126">n-n:case</ta>
            <ta e="T127" id="Seg_7365" s="T692">n-n:case</ta>
            <ta e="T128" id="Seg_7366" s="T127">v--v:pn</ta>
            <ta e="T129" id="Seg_7367" s="T128">interrog-n&gt;adj</ta>
            <ta e="T130" id="Seg_7368" s="T129">n-n:case-n:poss</ta>
            <ta e="T131" id="Seg_7369" s="T130">n-n:case</ta>
            <ta e="T132" id="Seg_7370" s="T131">quant</ta>
            <ta e="T133" id="Seg_7371" s="T132">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T134" id="Seg_7372" s="T133">dem-adj&gt;adv</ta>
            <ta e="T135" id="Seg_7373" s="T134">v-v:ins-v:pn</ta>
            <ta e="T136" id="Seg_7374" s="T135">ptcl</ta>
            <ta e="T137" id="Seg_7375" s="T136">n-n&gt;n-n:num-n:case</ta>
            <ta e="T138" id="Seg_7376" s="T137">v-v:pn</ta>
            <ta e="T139" id="Seg_7377" s="T138">dem</ta>
            <ta e="T140" id="Seg_7378" s="T139">n-n&gt;adj</ta>
            <ta e="T141" id="Seg_7379" s="T140">n-n&gt;n-n:num-n:case</ta>
            <ta e="T142" id="Seg_7380" s="T141">num</ta>
            <ta e="T143" id="Seg_7381" s="T142">num</ta>
            <ta e="T144" id="Seg_7382" s="T143">num</ta>
            <ta e="T145" id="Seg_7383" s="T144">n-n:num-n:case-n:poss</ta>
            <ta e="T146" id="Seg_7384" s="T145">dem</ta>
            <ta e="T147" id="Seg_7385" s="T146">n-n:case</ta>
            <ta e="T148" id="Seg_7386" s="T147">n-n:case</ta>
            <ta e="T149" id="Seg_7387" s="T148">v-v:inf.poss</ta>
            <ta e="T150" id="Seg_7388" s="T149">v-v:tense.mood-v:pn</ta>
            <ta e="T151" id="Seg_7389" s="T150">n-n:ins-n:obl.poss-n:case</ta>
            <ta e="T152" id="Seg_7390" s="T151">v-v:tense.mood-v:pn</ta>
            <ta e="T153" id="Seg_7391" s="T152">nprop-n:case</ta>
            <ta e="T154" id="Seg_7392" s="T153">n-n:case</ta>
            <ta e="T155" id="Seg_7393" s="T154">n-n:case-n:poss</ta>
            <ta e="T156" id="Seg_7394" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_7395" s="T156">v</ta>
            <ta e="T158" id="Seg_7396" s="T157">adv</ta>
            <ta e="T159" id="Seg_7397" s="T158">v-v&gt;adv</ta>
            <ta e="T160" id="Seg_7398" s="T159">v-v:pn</ta>
            <ta e="T161" id="Seg_7399" s="T160">ptcl</ta>
            <ta e="T162" id="Seg_7400" s="T161">pers-n:ins-n:num-n:ins-n:case</ta>
            <ta e="T163" id="Seg_7401" s="T162">n-n:case</ta>
            <ta e="T164" id="Seg_7402" s="T163">dem</ta>
            <ta e="T165" id="Seg_7403" s="T164">n-n:case</ta>
            <ta e="T166" id="Seg_7404" s="T165">ptcl</ta>
            <ta e="T167" id="Seg_7405" s="T166">v-v:pn</ta>
            <ta e="T168" id="Seg_7406" s="T167">n-n:case</ta>
            <ta e="T169" id="Seg_7407" s="T168">conj</ta>
            <ta e="T170" id="Seg_7408" s="T169">pers-n:ins-n:case</ta>
            <ta e="T171" id="Seg_7409" s="T170">v-v:tense-v:pn</ta>
            <ta e="T172" id="Seg_7410" s="T171">pers-n:case</ta>
            <ta e="T173" id="Seg_7411" s="T172">interrog-n:case</ta>
            <ta e="T174" id="Seg_7412" s="T173">adj-adj&gt;adv</ta>
            <ta e="T175" id="Seg_7413" s="T174">v-v:pn</ta>
            <ta e="T176" id="Seg_7414" s="T175">n-n:case</ta>
            <ta e="T177" id="Seg_7415" s="T176">conj</ta>
            <ta e="T178" id="Seg_7416" s="T177">v-v:tense-v:pn</ta>
            <ta e="T179" id="Seg_7417" s="T178">adj</ta>
            <ta e="T180" id="Seg_7418" s="T179">v-v&gt;ptcp</ta>
            <ta e="T181" id="Seg_7419" s="T180">n-n:case</ta>
            <ta e="T182" id="Seg_7420" s="T181">v-v:pn</ta>
            <ta e="T183" id="Seg_7421" s="T182">ptcl</ta>
            <ta e="T184" id="Seg_7422" s="T183">num-num&gt;adj</ta>
            <ta e="T185" id="Seg_7423" s="T184">n-adv:case</ta>
            <ta e="T186" id="Seg_7424" s="T185">%%</ta>
            <ta e="T187" id="Seg_7425" s="T186">ptcl</ta>
            <ta e="T188" id="Seg_7426" s="T187">nprop-n:case</ta>
            <ta e="T189" id="Seg_7427" s="T188">n-n:case</ta>
            <ta e="T190" id="Seg_7428" s="T189">num</ta>
            <ta e="T191" id="Seg_7429" s="T190">n-n:case</ta>
            <ta e="T192" id="Seg_7430" s="T191">v-v:ins-v:pn</ta>
            <ta e="T193" id="Seg_7431" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_7432" s="T193">n-n:case</ta>
            <ta e="T195" id="Seg_7433" s="T194">v-v:inf.poss</ta>
            <ta e="T196" id="Seg_7434" s="T195">dem</ta>
            <ta e="T197" id="Seg_7435" s="T196">dem</ta>
            <ta e="T198" id="Seg_7436" s="T197">n-n&gt;n-n:num-n:ins-n:case</ta>
            <ta e="T199" id="Seg_7437" s="T198">n-n:case</ta>
            <ta e="T200" id="Seg_7438" s="T199">v-v:inf</ta>
            <ta e="T201" id="Seg_7439" s="T200">interrog-n:case</ta>
            <ta e="T202" id="Seg_7440" s="T201">pp-pp&gt;adj</ta>
            <ta e="T203" id="Seg_7441" s="T202">n-n:case-n:poss</ta>
            <ta e="T204" id="Seg_7442" s="T203">adj</ta>
            <ta e="T205" id="Seg_7443" s="T204">interrog-n:case</ta>
            <ta e="T206" id="Seg_7444" s="T205">pp-pp&gt;adj</ta>
            <ta e="T207" id="Seg_7445" s="T206">interrog-n:case-n:poss</ta>
            <ta e="T208" id="Seg_7446" s="T207">quant</ta>
            <ta e="T209" id="Seg_7447" s="T208">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T210" id="Seg_7448" s="T209">adv</ta>
            <ta e="T211" id="Seg_7449" s="T210">adv</ta>
            <ta e="T212" id="Seg_7450" s="T211">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T213" id="Seg_7451" s="T212">n-n:num-n:ins-n:case</ta>
            <ta e="T214" id="Seg_7452" s="T213">n-n:case</ta>
            <ta e="T215" id="Seg_7453" s="T214">v-v&gt;v-v&gt;adv</ta>
            <ta e="T216" id="Seg_7454" s="T215">v-v:tense-v:pn</ta>
            <ta e="T217" id="Seg_7455" s="T216">adv</ta>
            <ta e="T218" id="Seg_7456" s="T217">adv</ta>
            <ta e="T219" id="Seg_7457" s="T218">%%</ta>
            <ta e="T220" id="Seg_7458" s="T219">adv</ta>
            <ta e="T221" id="Seg_7459" s="T220">v-v:ins-v:pn</ta>
            <ta e="T222" id="Seg_7460" s="T221">dem</ta>
            <ta e="T223" id="Seg_7461" s="T222">n-n:num-n:case</ta>
            <ta e="T224" id="Seg_7462" s="T223">num-num&gt;adj</ta>
            <ta e="T225" id="Seg_7463" s="T224">n-n&gt;adv</ta>
            <ta e="T226" id="Seg_7464" s="T225">ptcl</ta>
            <ta e="T227" id="Seg_7465" s="T226">v-v:tense-v:pn</ta>
            <ta e="T228" id="Seg_7466" s="T227">pers</ta>
            <ta e="T229" id="Seg_7467" s="T228">ptcl</ta>
            <ta e="T230" id="Seg_7468" s="T229">interrog</ta>
            <ta e="T231" id="Seg_7469" s="T230">ptcl</ta>
            <ta e="T232" id="Seg_7470" s="T231">v-v:mood.pn</ta>
            <ta e="T233" id="Seg_7471" s="T232">num-num&gt;adj</ta>
            <ta e="T234" id="Seg_7472" s="T233">n-n:case</ta>
            <ta e="T235" id="Seg_7473" s="T234">preverb</ta>
            <ta e="T236" id="Seg_7474" s="T235">n-n&gt;v-v:ins-v:pn</ta>
            <ta e="T237" id="Seg_7475" s="T236">pro</ta>
            <ta e="T238" id="Seg_7476" s="T237">n-n:case</ta>
            <ta e="T239" id="Seg_7477" s="T238">dem-n&gt;adj</ta>
            <ta e="T240" id="Seg_7478" s="T239">v-v:tense.mood-v:pn</ta>
            <ta e="T241" id="Seg_7479" s="T240">n-n:obl.poss-n:case</ta>
            <ta e="T242" id="Seg_7480" s="T241">ptcl</ta>
            <ta e="T243" id="Seg_7481" s="T242">adv-adv:case</ta>
            <ta e="T244" id="Seg_7482" s="T243">adv</ta>
            <ta e="T245" id="Seg_7483" s="T244">num</ta>
            <ta e="T246" id="Seg_7484" s="T245">num</ta>
            <ta e="T247" id="Seg_7485" s="T246">n-n:num-n:case</ta>
            <ta e="T248" id="Seg_7486" s="T247">v-v&gt;v-v&gt;n-n:case-n&gt;adj</ta>
            <ta e="T249" id="Seg_7487" s="T248">n-n:case-n:poss</ta>
            <ta e="T250" id="Seg_7488" s="T249">v-v:tense.mood-v:pn</ta>
            <ta e="T251" id="Seg_7489" s="T250">adv</ta>
            <ta e="T252" id="Seg_7490" s="T251">v-v&gt;v-v:pn</ta>
            <ta e="T253" id="Seg_7491" s="T252">n-n:num-n:case</ta>
            <ta e="T254" id="Seg_7492" s="T253">v-v:tense-v:pn</ta>
            <ta e="T255" id="Seg_7493" s="T254">n-n:case</ta>
            <ta e="T256" id="Seg_7494" s="T255">v-v:inf-v-v:tense-v:pn</ta>
            <ta e="T257" id="Seg_7495" s="T256">n-n:case</ta>
            <ta e="T258" id="Seg_7496" s="T257">n-n:case</ta>
            <ta e="T259" id="Seg_7497" s="T258">n-%%-v&gt;n-n:case-v:pn</ta>
            <ta e="T260" id="Seg_7498" s="T259">pp</ta>
            <ta e="T261" id="Seg_7499" s="T260">n-n&gt;v-v&gt;ptcp</ta>
            <ta e="T262" id="Seg_7500" s="T261">interrog</ta>
            <ta e="T263" id="Seg_7501" s="T262">v-v:tense-v:pn</ta>
            <ta e="T264" id="Seg_7502" s="T263">dem</ta>
            <ta e="T265" id="Seg_7503" s="T264">n-n:case</ta>
            <ta e="T266" id="Seg_7504" s="T265">adv</ta>
            <ta e="T267" id="Seg_7505" s="T266">v-v:ins-v:pn</ta>
            <ta e="T268" id="Seg_7506" s="T267">adv</ta>
            <ta e="T269" id="Seg_7507" s="T268">adv</ta>
            <ta e="T270" id="Seg_7508" s="T269">v-v:ins-v:pn</ta>
            <ta e="T271" id="Seg_7509" s="T270">pers</ta>
            <ta e="T272" id="Seg_7510" s="T271">ptcl</ta>
            <ta e="T273" id="Seg_7511" s="T272">adj</ta>
            <ta e="T274" id="Seg_7512" s="T273">n-n:case-n:poss</ta>
            <ta e="T275" id="Seg_7513" s="T274">v-v:ins-v:pn</ta>
            <ta e="T276" id="Seg_7514" s="T275">n-n:case</ta>
            <ta e="T277" id="Seg_7515" s="T276">pers</ta>
            <ta e="T278" id="Seg_7516" s="T277">v-v:mood.pn</ta>
            <ta e="T279" id="Seg_7517" s="T278">conj</ta>
            <ta e="T280" id="Seg_7518" s="T279">dem</ta>
            <ta e="T281" id="Seg_7519" s="T280">dem</ta>
            <ta e="T282" id="Seg_7520" s="T281">n-n:case-n:poss</ta>
            <ta e="T283" id="Seg_7521" s="T282">n-n:case</ta>
            <ta e="T284" id="Seg_7522" s="T283">n-n:case-n:poss</ta>
            <ta e="T285" id="Seg_7523" s="T284">dem-%%-n:case</ta>
            <ta e="T286" id="Seg_7524" s="T285">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T287" id="Seg_7525" s="T286">dem</ta>
            <ta e="T289" id="Seg_7526" s="T287">n-n:case</ta>
            <ta e="T290" id="Seg_7527" s="T289">adv-adv:case</ta>
            <ta e="T291" id="Seg_7528" s="T290">adv</ta>
            <ta e="T292" id="Seg_7529" s="T291">n-n&gt;adj</ta>
            <ta e="T293" id="Seg_7530" s="T292">n-n:case</ta>
            <ta e="T294" id="Seg_7531" s="T293">v-v:ins-v:pn</ta>
            <ta e="T295" id="Seg_7532" s="T294">adv</ta>
            <ta e="T296" id="Seg_7533" s="T295">n-n:ins-n&gt;v-v:tense-v:pn</ta>
            <ta e="T297" id="Seg_7534" s="T296">adj</ta>
            <ta e="T298" id="Seg_7535" s="T297">n-n:case</ta>
            <ta e="T299" id="Seg_7536" s="T298">v-v:mood.pn</ta>
            <ta e="T300" id="Seg_7537" s="T299">ptcl</ta>
            <ta e="T301" id="Seg_7538" s="T300">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T302" id="Seg_7539" s="T301">adv</ta>
            <ta e="T303" id="Seg_7540" s="T302">ptcl</ta>
            <ta e="T304" id="Seg_7541" s="T303">ptcl</ta>
            <ta e="T305" id="Seg_7542" s="T304">num-num&gt;adj</ta>
            <ta e="T306" id="Seg_7543" s="T305">n-n:case</ta>
            <ta e="T307" id="Seg_7544" s="T306">v-v:tense-v:pn</ta>
            <ta e="T308" id="Seg_7545" s="T307">nprop-n:case</ta>
            <ta e="T309" id="Seg_7546" s="T308">n-n&gt;adj-n-n:num-n:case</ta>
            <ta e="T310" id="Seg_7547" s="T309">pers-n:ins-n:num-n:ins-n:case</ta>
            <ta e="T311" id="Seg_7548" s="T310">dem</ta>
            <ta e="T312" id="Seg_7549" s="T311">n-n:case</ta>
            <ta e="T313" id="Seg_7550" s="T312">n-n:case</ta>
            <ta e="T314" id="Seg_7551" s="T313">v-v&gt;v-v:inf</ta>
            <ta e="T315" id="Seg_7552" s="T314">n-n&gt;n-n:num-n:case</ta>
            <ta e="T316" id="Seg_7553" s="T315">n-n:case</ta>
            <ta e="T317" id="Seg_7554" s="T316">adv</ta>
            <ta e="T318" id="Seg_7555" s="T317">preverb-pp&gt;adj</ta>
            <ta e="T319" id="Seg_7556" s="T318">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T320" id="Seg_7557" s="T319">dem</ta>
            <ta e="T321" id="Seg_7558" s="T320">n-n:case</ta>
            <ta e="T322" id="Seg_7559" s="T321">ptcl</ta>
            <ta e="T323" id="Seg_7560" s="T322">v-v:tense.mood-v:pn</ta>
            <ta e="T324" id="Seg_7561" s="T323">ptcl</ta>
            <ta e="T325" id="Seg_7562" s="T324">pers-n:case</ta>
            <ta e="T326" id="Seg_7563" s="T325">ptcl</ta>
            <ta e="T327" id="Seg_7564" s="T326">v-v&gt;v-v:pn</ta>
            <ta e="T328" id="Seg_7565" s="T327">n-n:case</ta>
            <ta e="T329" id="Seg_7566" s="T328">ptcl</ta>
            <ta e="T330" id="Seg_7567" s="T329">v-v:tense.mood-v:pn</ta>
            <ta e="T331" id="Seg_7568" s="T330">dem</ta>
            <ta e="T332" id="Seg_7569" s="T331">n-n:case</ta>
            <ta e="T333" id="Seg_7570" s="T332">n-n:case</ta>
            <ta e="T334" id="Seg_7571" s="T333">adv-adv&gt;adj</ta>
            <ta e="T335" id="Seg_7572" s="T334">n-n:case</ta>
            <ta e="T336" id="Seg_7573" s="T335">adv</ta>
            <ta e="T337" id="Seg_7574" s="T336">n-n:ins-n&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T338" id="Seg_7575" s="T337">dem</ta>
            <ta e="T339" id="Seg_7576" s="T338">n-n:obl.poss-n:case</ta>
            <ta e="T340" id="Seg_7577" s="T339">ptcl</ta>
            <ta e="T341" id="Seg_7578" s="T340">n-n&gt;adj</ta>
            <ta e="T342" id="Seg_7579" s="T341">n-n:case</ta>
            <ta e="T343" id="Seg_7580" s="T342">interrog-n:case</ta>
            <ta e="T344" id="Seg_7581" s="T343">n-n:case-n:poss</ta>
            <ta e="T345" id="Seg_7582" s="T344">v-v:pn</ta>
            <ta e="T346" id="Seg_7583" s="T345">adj</ta>
            <ta e="T347" id="Seg_7584" s="T346">n-n:case-n:poss</ta>
            <ta e="T348" id="Seg_7585" s="T347">n-n&gt;adj</ta>
            <ta e="T349" id="Seg_7586" s="T348">n-n:case-n:poss</ta>
            <ta e="T350" id="Seg_7587" s="T349">adv</ta>
            <ta e="T351" id="Seg_7588" s="T350">num-num&gt;adv</ta>
            <ta e="T352" id="Seg_7589" s="T351">v-v:pn</ta>
            <ta e="T353" id="Seg_7590" s="T352">dem</ta>
            <ta e="T354" id="Seg_7591" s="T353">n-n:obl.poss-n:case</ta>
            <ta e="T355" id="Seg_7592" s="T354">adj</ta>
            <ta e="T356" id="Seg_7593" s="T355">n-adv:case</ta>
            <ta e="T357" id="Seg_7594" s="T356">dem-adj&gt;adv</ta>
            <ta e="T358" id="Seg_7595" s="T357">v-v:pn</ta>
            <ta e="T359" id="Seg_7596" s="T358">dem</ta>
            <ta e="T360" id="Seg_7597" s="T359">n-n:obl.poss-n:case</ta>
            <ta e="T361" id="Seg_7598" s="T360">pers</ta>
            <ta e="T362" id="Seg_7599" s="T361">ptcl</ta>
            <ta e="T363" id="Seg_7600" s="T362">adv</ta>
            <ta e="T364" id="Seg_7601" s="T363">n-n:num-n:case.poss</ta>
            <ta e="T365" id="Seg_7602" s="T364">v-v:mood-v:pn</ta>
            <ta e="T366" id="Seg_7603" s="T365">conj</ta>
            <ta e="T367" id="Seg_7604" s="T366">ptcl</ta>
            <ta e="T368" id="Seg_7605" s="T367">n-n:ins-n&gt;v-v:tense-v:pn</ta>
            <ta e="T369" id="Seg_7606" s="T368">conj</ta>
            <ta e="T370" id="Seg_7607" s="T369">qv-v:pn</ta>
            <ta e="T371" id="Seg_7608" s="T370">adv</ta>
            <ta e="T372" id="Seg_7609" s="T371">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T373" id="Seg_7610" s="T372">num</ta>
            <ta e="T374" id="Seg_7611" s="T373">n-n:case</ta>
            <ta e="T375" id="Seg_7612" s="T374">v-v&gt;adv</ta>
            <ta e="T376" id="Seg_7613" s="T375">ptcl-adj&gt;adj</ta>
            <ta e="T377" id="Seg_7614" s="T376">num-num&gt;num-n:ins-num&gt;adj</ta>
            <ta e="T378" id="Seg_7615" s="T377">n-n:case</ta>
            <ta e="T379" id="Seg_7616" s="T378">v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T380" id="Seg_7617" s="T379">n-n:case</ta>
            <ta e="T381" id="Seg_7618" s="T380">adv</ta>
            <ta e="T382" id="Seg_7619" s="T381">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T383" id="Seg_7620" s="T382">n-n:case-n:poss</ta>
            <ta e="T384" id="Seg_7621" s="T383">emphpro</ta>
            <ta e="T385" id="Seg_7622" s="T384">v-v:pn</ta>
            <ta e="T386" id="Seg_7623" s="T385">ptcl</ta>
            <ta e="T387" id="Seg_7624" s="T386">v-v:pn</ta>
            <ta e="T388" id="Seg_7625" s="T387">ptcl</ta>
            <ta e="T389" id="Seg_7626" s="T388">v-v:pn</ta>
            <ta e="T390" id="Seg_7627" s="T389">num</ta>
            <ta e="T391" id="Seg_7628" s="T390">dem-n:case</ta>
            <ta e="T392" id="Seg_7629" s="T391">n-n:case</ta>
            <ta e="T393" id="Seg_7630" s="T392">n-n:case-n:poss</ta>
            <ta e="T394" id="Seg_7631" s="T393">n-n:case</ta>
            <ta e="T395" id="Seg_7632" s="T394">ptcl</ta>
            <ta e="T396" id="Seg_7633" s="T395">v-v:ins-v:pn</ta>
            <ta e="T397" id="Seg_7634" s="T396">dem</ta>
            <ta e="T398" id="Seg_7635" s="T397">n-n:case</ta>
            <ta e="T399" id="Seg_7636" s="T398">adj-adj&gt;adv</ta>
            <ta e="T400" id="Seg_7637" s="T399">v-v:ins-v:pn</ta>
            <ta e="T401" id="Seg_7638" s="T400">adv-adv:case</ta>
            <ta e="T402" id="Seg_7639" s="T401">num</ta>
            <ta e="T403" id="Seg_7640" s="T402">dem-n:case</ta>
            <ta e="T404" id="Seg_7641" s="T403">n-n:case</ta>
            <ta e="T405" id="Seg_7642" s="T404">n-n:case</ta>
            <ta e="T406" id="Seg_7643" s="T405">pro-n&gt;n-n:case</ta>
            <ta e="T407" id="Seg_7644" s="T406">v-v:tense.mood-v:pn</ta>
            <ta e="T408" id="Seg_7645" s="T407">adv-adv:case</ta>
            <ta e="T409" id="Seg_7646" s="T408">nprop-n:case</ta>
            <ta e="T410" id="Seg_7647" s="T409">n-n&gt;adj-n-n:num-n:case</ta>
            <ta e="T411" id="Seg_7648" s="T410">v-v:tense.mood-v:pn</ta>
            <ta e="T412" id="Seg_7649" s="T411">num</ta>
            <ta e="T413" id="Seg_7650" s="T412">n-n:case-n:poss</ta>
            <ta e="T414" id="Seg_7651" s="T413">ptcl-adj&gt;adj</ta>
            <ta e="T415" id="Seg_7652" s="T414">n-n:case</ta>
            <ta e="T416" id="Seg_7653" s="T415">v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T417" id="Seg_7654" s="T416">nprop-n:case</ta>
            <ta e="T418" id="Seg_7655" s="T417">n-n:case</ta>
            <ta e="T419" id="Seg_7656" s="T418">quant</ta>
            <ta e="T420" id="Seg_7657" s="T419">n-n:case</ta>
            <ta e="T421" id="Seg_7658" s="T420">v-v:ins-v:pn</ta>
            <ta e="T422" id="Seg_7659" s="T421">adv</ta>
            <ta e="T423" id="Seg_7660" s="T422">num-num&gt;adv</ta>
            <ta e="T424" id="Seg_7661" s="T423">dem</ta>
            <ta e="T425" id="Seg_7662" s="T424">n-n:case</ta>
            <ta e="T426" id="Seg_7663" s="T425">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T427" id="Seg_7664" s="T426">n-n:case-n:poss</ta>
            <ta e="T428" id="Seg_7665" s="T427">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T429" id="Seg_7666" s="T428">ptcl</ta>
            <ta e="T430" id="Seg_7667" s="T429">ptcl</ta>
            <ta e="T431" id="Seg_7668" s="T430">v-v&gt;v-v:pn</ta>
            <ta e="T432" id="Seg_7669" s="T431">nprop-n:case</ta>
            <ta e="T433" id="Seg_7670" s="T432">n-n&gt;adj-n-n:num-n:case</ta>
            <ta e="T434" id="Seg_7671" s="T433">ptcl</ta>
            <ta e="T435" id="Seg_7672" s="T434">ptcl</ta>
            <ta e="T436" id="Seg_7673" s="T435">v-v:ins-v:pn</ta>
            <ta e="T437" id="Seg_7674" s="T436">n-n&gt;adj</ta>
            <ta e="T438" id="Seg_7675" s="T437">n-n:case-n:poss</ta>
            <ta e="T439" id="Seg_7676" s="T438">dem-adj&gt;adv</ta>
            <ta e="T440" id="Seg_7677" s="T439">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T441" id="Seg_7678" s="T440">n-n:case</ta>
            <ta e="T442" id="Seg_7679" s="T441">n-n:case</ta>
            <ta e="T443" id="Seg_7680" s="T442">n-n:case</ta>
            <ta e="T444" id="Seg_7681" s="T443">v-v:ins-v:pn</ta>
            <ta e="T445" id="Seg_7682" s="T444">n-n:case</ta>
            <ta e="T446" id="Seg_7683" s="T445">n-n&gt;adv</ta>
            <ta e="T447" id="Seg_7684" s="T446">v-v&gt;v-v&gt;adv</ta>
            <ta e="T448" id="Seg_7685" s="T447">n-n:case</ta>
            <ta e="T449" id="Seg_7686" s="T448">n-n&gt;adv</ta>
            <ta e="T450" id="Seg_7687" s="T449">ptcl</ta>
            <ta e="T451" id="Seg_7688" s="T450">n-n:case-n:poss</ta>
            <ta e="T452" id="Seg_7689" s="T451">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T453" id="Seg_7690" s="T452">n-n:case-n:poss</ta>
            <ta e="T454" id="Seg_7691" s="T453">ptcl</ta>
            <ta e="T455" id="Seg_7692" s="T454">v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T456" id="Seg_7693" s="T455">adv</ta>
            <ta e="T457" id="Seg_7694" s="T456">v-v:ins-v:pn</ta>
            <ta e="T458" id="Seg_7695" s="T457">nprop-n:case</ta>
            <ta e="T459" id="Seg_7696" s="T458">n-n:case</ta>
            <ta e="T460" id="Seg_7697" s="T459">adv</ta>
            <ta e="T461" id="Seg_7698" s="T460">v-v&gt;v-v&gt;adv</ta>
            <ta e="T462" id="Seg_7699" s="T461">n-n:case-n:poss</ta>
            <ta e="T463" id="Seg_7700" s="T462">conj</ta>
            <ta e="T464" id="Seg_7701" s="T463">ptcl</ta>
            <ta e="T465" id="Seg_7702" s="T464">n-n:case</ta>
            <ta e="T466" id="Seg_7703" s="T465">n-n:case</ta>
            <ta e="T467" id="Seg_7704" s="T466">ptcl</ta>
            <ta e="T468" id="Seg_7705" s="T467">adv</ta>
            <ta e="T469" id="Seg_7706" s="T468">ptcl</ta>
            <ta e="T470" id="Seg_7707" s="T469">adv</ta>
            <ta e="T471" id="Seg_7708" s="T470">v-v:tense-v:pn</ta>
            <ta e="T472" id="Seg_7709" s="T471">ptcl</ta>
            <ta e="T473" id="Seg_7710" s="T472">n-n:case</ta>
            <ta e="T474" id="Seg_7711" s="T473">v-v&gt;v-v&gt;v-v-v:tense-v:mood-v:pn</ta>
            <ta e="T475" id="Seg_7712" s="T474">nprop-n:case</ta>
            <ta e="T476" id="Seg_7713" s="T475">n-n&gt;adj</ta>
            <ta e="T477" id="Seg_7714" s="T476">n-n:num-n:ins-n:case</ta>
            <ta e="T478" id="Seg_7715" s="T477">quant</ta>
            <ta e="T479" id="Seg_7716" s="T478">adv</ta>
            <ta e="T480" id="Seg_7717" s="T479">v-v:tense-v:pn</ta>
            <ta e="T481" id="Seg_7718" s="T480">adj-n:case-n:poss</ta>
            <ta e="T482" id="Seg_7719" s="T481">v-v:tense-v:pn</ta>
            <ta e="T483" id="Seg_7720" s="T482">nprop-n:case</ta>
            <ta e="T484" id="Seg_7721" s="T483">n-n:case</ta>
            <ta e="T485" id="Seg_7722" s="T484">v-v&gt;adv</ta>
            <ta e="T486" id="Seg_7723" s="T485">v-v:tense-v:pn</ta>
            <ta e="T487" id="Seg_7724" s="T486">num-n:num-n:case</ta>
            <ta e="T488" id="Seg_7725" s="T487">v-v&gt;adv</ta>
            <ta e="T489" id="Seg_7726" s="T488">v-v:tense-v:mood-v:pn</ta>
            <ta e="T490" id="Seg_7727" s="T489">adv</ta>
            <ta e="T491" id="Seg_7728" s="T490">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T492" id="Seg_7729" s="T491">dem</ta>
            <ta e="T493" id="Seg_7730" s="T492">n-n:case</ta>
            <ta e="T494" id="Seg_7731" s="T493">num</ta>
            <ta e="T495" id="Seg_7732" s="T494">n-n:case</ta>
            <ta e="T496" id="Seg_7733" s="T495">n-n:case</ta>
            <ta e="T497" id="Seg_7734" s="T496">v-v&gt;v-v&gt;v-v-v:tense-v:pn</ta>
            <ta e="T498" id="Seg_7735" s="T497">quant</ta>
            <ta e="T499" id="Seg_7736" s="T498">n-n:case-n:poss</ta>
            <ta e="T500" id="Seg_7737" s="T499">pp-adv:case</ta>
            <ta e="T501" id="Seg_7738" s="T500">n-n:case-n:poss</ta>
            <ta e="T502" id="Seg_7739" s="T501">pp</ta>
            <ta e="T503" id="Seg_7740" s="T502">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T504" id="Seg_7741" s="T503">dem</ta>
            <ta e="T505" id="Seg_7742" s="T504">v-v&gt;ptcp-n-n:num-n:case</ta>
            <ta e="T506" id="Seg_7743" s="T505">adv</ta>
            <ta e="T507" id="Seg_7744" s="T506">adv</ta>
            <ta e="T508" id="Seg_7745" s="T507">adv</ta>
            <ta e="T509" id="Seg_7746" s="T508">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T510" id="Seg_7747" s="T509">n-n:case</ta>
            <ta e="T511" id="Seg_7748" s="T510">dem</ta>
            <ta e="T512" id="Seg_7749" s="T511">n-n:case</ta>
            <ta e="T513" id="Seg_7750" s="T512">n-n:case</ta>
            <ta e="T514" id="Seg_7751" s="T513">n-n:case-n:poss</ta>
            <ta e="T515" id="Seg_7752" s="T514">ptcl</ta>
            <ta e="T516" id="Seg_7753" s="T515">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T517" id="Seg_7754" s="T516">nprop-n:case</ta>
            <ta e="T518" id="Seg_7755" s="T517">n-n:case</ta>
            <ta e="T519" id="Seg_7756" s="T518">ptcl</ta>
            <ta e="T520" id="Seg_7757" s="T519">adv</ta>
            <ta e="T521" id="Seg_7758" s="T520">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T522" id="Seg_7759" s="T521">v-v:tense-v:pn</ta>
            <ta e="T523" id="Seg_7760" s="T522">ptcl</ta>
            <ta e="T524" id="Seg_7761" s="T523">quant</ta>
            <ta e="T525" id="Seg_7762" s="T524">adv</ta>
            <ta e="T526" id="Seg_7763" s="T525">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T527" id="Seg_7764" s="T526">preverb</ta>
            <ta e="T528" id="Seg_7765" s="T527">v-v:pn</ta>
            <ta e="T529" id="Seg_7766" s="T528">n-n&gt;adj</ta>
            <ta e="T530" id="Seg_7767" s="T529">n-n:case-n:poss</ta>
            <ta e="T531" id="Seg_7768" s="T530">preverb</ta>
            <ta e="T532" id="Seg_7769" s="T531">v-v&gt;adv</ta>
            <ta e="T533" id="Seg_7770" s="T532">v-v:ins-v&gt;v-v&gt;adv</ta>
            <ta e="T534" id="Seg_7771" s="T533">preverb</ta>
            <ta e="T535" id="Seg_7772" s="T534">v-v:tense-v:mood-v:pn</ta>
            <ta e="T536" id="Seg_7773" s="T535">v-v:pn</ta>
            <ta e="T537" id="Seg_7774" s="T536">dem</ta>
            <ta e="T538" id="Seg_7775" s="T537">n-n:num-n:case-n:poss</ta>
            <ta e="T539" id="Seg_7776" s="T538">n-n:case</ta>
            <ta e="T540" id="Seg_7777" s="T539">ptcl</ta>
            <ta e="T541" id="Seg_7778" s="T540">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T542" id="Seg_7779" s="T541">conj</ta>
            <ta e="T543" id="Seg_7780" s="T542">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T544" id="Seg_7781" s="T543">dem-adj&gt;adv</ta>
            <ta e="T545" id="Seg_7782" s="T544">v-v:pn</ta>
            <ta e="T546" id="Seg_7783" s="T545">adj</ta>
            <ta e="T547" id="Seg_7784" s="T546">n-adv:case</ta>
            <ta e="T548" id="Seg_7785" s="T547">preverb</ta>
            <ta e="T549" id="Seg_7786" s="T548">v-v:pn</ta>
            <ta e="T550" id="Seg_7787" s="T549">nprop-n:case</ta>
            <ta e="T551" id="Seg_7788" s="T550">n-n:case</ta>
            <ta e="T552" id="Seg_7789" s="T551">dem-adj&gt;adv</ta>
            <ta e="T553" id="Seg_7790" s="T552">dem-n:case</ta>
            <ta e="T554" id="Seg_7791" s="T553">n-n&gt;adj</ta>
            <ta e="T555" id="Seg_7792" s="T554">n-n:case-n:poss</ta>
            <ta e="T556" id="Seg_7793" s="T555">adj-n:ins-adj&gt;adv</ta>
            <ta e="T557" id="Seg_7794" s="T556">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T558" id="Seg_7795" s="T557">adv</ta>
            <ta e="T559" id="Seg_7796" s="T558">n-n&gt;n-n:case-n:poss</ta>
            <ta e="T560" id="Seg_7797" s="T559">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T561" id="Seg_7798" s="T560">dem</ta>
            <ta e="T562" id="Seg_7799" s="T561">n-n:num-n:case-n:poss</ta>
            <ta e="T563" id="Seg_7800" s="T562">adv</ta>
            <ta e="T564" id="Seg_7801" s="T563">adj-adj&gt;adj</ta>
            <ta e="T565" id="Seg_7802" s="T564">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T566" id="Seg_7803" s="T565">dem-n&gt;adj</ta>
            <ta e="T567" id="Seg_7804" s="T566">v-v:ins-v:pn</ta>
            <ta e="T568" id="Seg_7805" s="T567">dem</ta>
            <ta e="T569" id="Seg_7806" s="T568">n-n:case</ta>
            <ta e="T570" id="Seg_7807" s="T569">ptcl</ta>
            <ta e="T571" id="Seg_7808" s="T570">dem</ta>
            <ta e="T572" id="Seg_7809" s="T571">n-n:num-n:ins-n:case</ta>
            <ta e="T573" id="Seg_7810" s="T572">adv</ta>
            <ta e="T574" id="Seg_7811" s="T573">v-v&gt;v-v:pn</ta>
            <ta e="T575" id="Seg_7812" s="T574">%%</ta>
            <ta e="T576" id="Seg_7813" s="T575">emphpro</ta>
            <ta e="T577" id="Seg_7814" s="T576">n-n:obl.poss-n:case</ta>
            <ta e="T578" id="Seg_7815" s="T577">adv</ta>
            <ta e="T579" id="Seg_7816" s="T578">num-num&gt;adv</ta>
            <ta e="T580" id="Seg_7817" s="T579">dem</ta>
            <ta e="T581" id="Seg_7818" s="T580">pers-n:num</ta>
            <ta e="T582" id="Seg_7819" s="T581">n-n:case</ta>
            <ta e="T583" id="Seg_7820" s="T582">v-v:ins-v&gt;v-v:tense-v:pn</ta>
            <ta e="T584" id="Seg_7821" s="T583">conj</ta>
            <ta e="T585" id="Seg_7822" s="T584">v-v:ins-v&gt;v-v:tense-v:pn</ta>
            <ta e="T586" id="Seg_7823" s="T585">conj</ta>
            <ta e="T587" id="Seg_7824" s="T586">ptcl</ta>
            <ta e="T588" id="Seg_7825" s="T587">adv</ta>
            <ta e="T589" id="Seg_7826" s="T588">dem</ta>
            <ta e="T590" id="Seg_7827" s="T589">v-v&gt;v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T591" id="Seg_7828" s="T590">n-n:case-n:poss</ta>
            <ta e="T592" id="Seg_7829" s="T591">adv</ta>
            <ta e="T593" id="Seg_7830" s="T592">adv</ta>
            <ta e="T594" id="Seg_7831" s="T593">v-v&gt;adv</ta>
            <ta e="T595" id="Seg_7832" s="T594">dem</ta>
            <ta e="T596" id="Seg_7833" s="T595">n-n:num-n:case-n:poss</ta>
            <ta e="T597" id="Seg_7834" s="T596">v-v&gt;ptcp</ta>
            <ta e="T598" id="Seg_7835" s="T597">n-n:num-n:case-n:poss</ta>
            <ta e="T599" id="Seg_7836" s="T598">adv</ta>
            <ta e="T600" id="Seg_7837" s="T599">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T601" id="Seg_7838" s="T600">adv</ta>
            <ta e="T602" id="Seg_7839" s="T601">adv</ta>
            <ta e="T603" id="Seg_7840" s="T602">nprop-n:case</ta>
            <ta e="T604" id="Seg_7841" s="T603">n-n:case-n:poss</ta>
            <ta e="T605" id="Seg_7842" s="T604">ptcl</ta>
            <ta e="T606" id="Seg_7843" s="T605">v-v&gt;adv</ta>
            <ta e="T607" id="Seg_7844" s="T606">v-v&gt;v-v:pn</ta>
            <ta e="T608" id="Seg_7845" s="T607">interrog-n:case</ta>
            <ta e="T609" id="Seg_7846" s="T608">adv</ta>
            <ta e="T611" id="Seg_7847" s="T610">v-v&gt;v-v:pn</ta>
            <ta e="T612" id="Seg_7848" s="T611">v-v:ins-v:pn</ta>
            <ta e="T613" id="Seg_7849" s="T612">dem</ta>
            <ta e="T614" id="Seg_7850" s="T613">n-n:ins-n:num-n:case</ta>
            <ta e="T615" id="Seg_7851" s="T614">adj-adj&gt;adv</ta>
            <ta e="T616" id="Seg_7852" s="T615">v-v:ins-v:pn</ta>
            <ta e="T617" id="Seg_7853" s="T616">n-n:obl.poss-n:case</ta>
            <ta e="T618" id="Seg_7854" s="T617">adv</ta>
            <ta e="T619" id="Seg_7855" s="T618">v-v:ins-v:pn</ta>
            <ta e="T620" id="Seg_7856" s="T619">n-n:case-n:poss</ta>
            <ta e="T621" id="Seg_7857" s="T620">preverb</ta>
            <ta e="T622" id="Seg_7858" s="T621">v-v:mood.pn</ta>
            <ta e="T623" id="Seg_7859" s="T622">pers</ta>
            <ta e="T624" id="Seg_7860" s="T623">adv</ta>
            <ta e="T625" id="Seg_7861" s="T624">v-v:mood</ta>
            <ta e="T626" id="Seg_7862" s="T625">n-n:ins-n:num-n:ins-n:case</ta>
            <ta e="T627" id="Seg_7863" s="T626">ptcl</ta>
            <ta e="T628" id="Seg_7864" s="T627">v-v:tense-v:pn</ta>
            <ta e="T629" id="Seg_7865" s="T628">n-n:obl.poss-n&gt;adj</ta>
            <ta e="T630" id="Seg_7866" s="T629">n-n:case-n:poss</ta>
            <ta e="T631" id="Seg_7867" s="T630">clit</ta>
            <ta e="T632" id="Seg_7868" s="T631">interrog-n:case</ta>
            <ta e="T633" id="Seg_7869" s="T632">adv</ta>
            <ta e="T634" id="Seg_7870" s="T633">v-v:ins-v:pn</ta>
            <ta e="T635" id="Seg_7871" s="T634">dem</ta>
            <ta e="T636" id="Seg_7872" s="T635">emphpro</ta>
            <ta e="T637" id="Seg_7873" s="T636">ptcl</ta>
            <ta e="T638" id="Seg_7874" s="T637">v-v&gt;v-v:pn</ta>
            <ta e="T639" id="Seg_7875" s="T638">dem</ta>
            <ta e="T640" id="Seg_7876" s="T639">n-n:case-n:poss</ta>
            <ta e="T641" id="Seg_7877" s="T640">ptcl</ta>
            <ta e="T642" id="Seg_7878" s="T641">v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T643" id="Seg_7879" s="T642">n-n:obl.poss-n&gt;adj</ta>
            <ta e="T644" id="Seg_7880" s="T643">n-n:obl.poss-n&gt;adj</ta>
            <ta e="T645" id="Seg_7881" s="T644">n-n:num-n:case</ta>
            <ta e="T646" id="Seg_7882" s="T645">n-n&gt;adj</ta>
            <ta e="T647" id="Seg_7883" s="T646">n-n:num-n:case</ta>
            <ta e="T648" id="Seg_7884" s="T647">adv</ta>
            <ta e="T649" id="Seg_7885" s="T648">v-v:tense-v:pn</ta>
            <ta e="T650" id="Seg_7886" s="T649">conj</ta>
            <ta e="T651" id="Seg_7887" s="T650">adv</ta>
            <ta e="T652" id="Seg_7888" s="T651">num-num&gt;adj</ta>
            <ta e="T653" id="Seg_7889" s="T652">adj-n&gt;n</ta>
            <ta e="T654" id="Seg_7890" s="T653">adj</ta>
            <ta e="T655" id="Seg_7891" s="T654">ptcl</ta>
            <ta e="T656" id="Seg_7892" s="T655">interrog-n:case</ta>
            <ta e="T657" id="Seg_7893" s="T656">ptcl</ta>
            <ta e="T658" id="Seg_7894" s="T657">v-v:pn</ta>
            <ta e="T659" id="Seg_7895" s="T658">interj</ta>
            <ta e="T660" id="Seg_7896" s="T659">v-v:ins-v:pn</ta>
            <ta e="T661" id="Seg_7897" s="T660">pers-n:ins-n:num-n:case</ta>
            <ta e="T662" id="Seg_7898" s="T661">n-n:case-n:obl.poss</ta>
            <ta e="T663" id="Seg_7899" s="T662">n-n:case</ta>
            <ta e="T664" id="Seg_7900" s="T663">v-v:ins-v:pn</ta>
            <ta e="T665" id="Seg_7901" s="T664">n-n&gt;adj</ta>
            <ta e="T666" id="Seg_7902" s="T665">n-n:case-n:poss</ta>
            <ta e="T667" id="Seg_7903" s="T666">v-v&gt;adv</ta>
            <ta e="T668" id="Seg_7904" s="T667">dem-adj&gt;adv</ta>
            <ta e="T669" id="Seg_7905" s="T668">v-v:pn</ta>
            <ta e="T670" id="Seg_7906" s="T669">dem</ta>
            <ta e="T671" id="Seg_7907" s="T670">v-v:tense-v:mood-v:pn</ta>
            <ta e="T672" id="Seg_7908" s="T671">adv</ta>
            <ta e="T673" id="Seg_7909" s="T672">ptcl</ta>
            <ta e="T674" id="Seg_7910" s="T673">v-v:tense-v:mood-v:pn</ta>
            <ta e="T675" id="Seg_7911" s="T674">nprop-n:case</ta>
            <ta e="T676" id="Seg_7912" s="T675">n-n:case</ta>
            <ta e="T677" id="Seg_7913" s="T676">adv</ta>
            <ta e="T678" id="Seg_7914" s="T677">adv</ta>
            <ta e="T679" id="Seg_7915" s="T678">n-n:case-n:obl.poss</ta>
            <ta e="T680" id="Seg_7916" s="T679">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T681" id="Seg_7917" s="T680">preverb</ta>
            <ta e="T682" id="Seg_7918" s="T681">v-v:tense-v:pn</ta>
            <ta e="T683" id="Seg_7919" s="T682">dem</ta>
            <ta e="T684" id="Seg_7920" s="T683">num</ta>
            <ta e="T685" id="Seg_7921" s="T684">n-n:ins-n:num-n:case</ta>
            <ta e="T686" id="Seg_7922" s="T685">v-v:tense-v:pn</ta>
            <ta e="T687" id="Seg_7923" s="T686">nprop-n:case</ta>
            <ta e="T688" id="Seg_7924" s="T687">n-n:case</ta>
            <ta e="T689" id="Seg_7925" s="T688">n-n:case-n:poss</ta>
            <ta e="T690" id="Seg_7926" s="T689">v-v:ins-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_7927" s="T0">num</ta>
            <ta e="T2" id="Seg_7928" s="T1">n</ta>
            <ta e="T3" id="Seg_7929" s="T2">v</ta>
            <ta e="T4" id="Seg_7930" s="T3">adj</ta>
            <ta e="T5" id="Seg_7931" s="T4">adj</ta>
            <ta e="T6" id="Seg_7932" s="T5">dem</ta>
            <ta e="T7" id="Seg_7933" s="T6">n</ta>
            <ta e="T8" id="Seg_7934" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_7935" s="T8">n</ta>
            <ta e="T10" id="Seg_7936" s="T9">n</ta>
            <ta e="T11" id="Seg_7937" s="T10">v</ta>
            <ta e="T12" id="Seg_7938" s="T11">v</ta>
            <ta e="T13" id="Seg_7939" s="T12">dem</ta>
            <ta e="T14" id="Seg_7940" s="T13">adj</ta>
            <ta e="T15" id="Seg_7941" s="T14">n</ta>
            <ta e="T16" id="Seg_7942" s="T15">adj</ta>
            <ta e="T17" id="Seg_7943" s="T16">n</ta>
            <ta e="T18" id="Seg_7944" s="T17">n</ta>
            <ta e="T19" id="Seg_7945" s="T18">v</ta>
            <ta e="T20" id="Seg_7946" s="T19">n</ta>
            <ta e="T21" id="Seg_7947" s="T20">adj</ta>
            <ta e="T22" id="Seg_7948" s="T21">adv</ta>
            <ta e="T23" id="Seg_7949" s="T22">v</ta>
            <ta e="T24" id="Seg_7950" s="T23">dem</ta>
            <ta e="T25" id="Seg_7951" s="T24">v</ta>
            <ta e="T26" id="Seg_7952" s="T25">num</ta>
            <ta e="T27" id="Seg_7953" s="T26">n</ta>
            <ta e="T28" id="Seg_7954" s="T27">adj</ta>
            <ta e="T29" id="Seg_7955" s="T28">adj</ta>
            <ta e="T30" id="Seg_7956" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_7957" s="T30">n</ta>
            <ta e="T32" id="Seg_7958" s="T31">v</ta>
            <ta e="T33" id="Seg_7959" s="T32">ptcl</ta>
            <ta e="T34" id="Seg_7960" s="T33">interrog</ta>
            <ta e="T35" id="Seg_7961" s="T34">v</ta>
            <ta e="T36" id="Seg_7962" s="T35">adv</ta>
            <ta e="T37" id="Seg_7963" s="T36">n</ta>
            <ta e="T38" id="Seg_7964" s="T37">v</ta>
            <ta e="T39" id="Seg_7965" s="T38">dem</ta>
            <ta e="T40" id="Seg_7966" s="T39">n</ta>
            <ta e="T41" id="Seg_7967" s="T40">dem</ta>
            <ta e="T42" id="Seg_7968" s="T41">n</ta>
            <ta e="T43" id="Seg_7969" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_7970" s="T43">adv</ta>
            <ta e="T45" id="Seg_7971" s="T44">v</ta>
            <ta e="T46" id="Seg_7972" s="T45">n</ta>
            <ta e="T47" id="Seg_7973" s="T46">n</ta>
            <ta e="T48" id="Seg_7974" s="T47">dem</ta>
            <ta e="T49" id="Seg_7975" s="T48">num</ta>
            <ta e="T50" id="Seg_7976" s="T49">adj</ta>
            <ta e="T51" id="Seg_7977" s="T50">n</ta>
            <ta e="T52" id="Seg_7978" s="T51">v</ta>
            <ta e="T693" id="Seg_7979" s="T52">n</ta>
            <ta e="T53" id="Seg_7980" s="T693">n</ta>
            <ta e="T54" id="Seg_7981" s="T53">v</ta>
            <ta e="T55" id="Seg_7982" s="T54">dem</ta>
            <ta e="T56" id="Seg_7983" s="T55">n</ta>
            <ta e="T57" id="Seg_7984" s="T56">preverb</ta>
            <ta e="T58" id="Seg_7985" s="T57">v</ta>
            <ta e="T59" id="Seg_7986" s="T58">dem</ta>
            <ta e="T694" id="Seg_7987" s="T59">n</ta>
            <ta e="T60" id="Seg_7988" s="T694">n</ta>
            <ta e="T61" id="Seg_7989" s="T60">interrog</ta>
            <ta e="T62" id="Seg_7990" s="T61">qv</ta>
            <ta e="T63" id="Seg_7991" s="T62">adv</ta>
            <ta e="T64" id="Seg_7992" s="T63">n</ta>
            <ta e="T65" id="Seg_7993" s="T64">v</ta>
            <ta e="T66" id="Seg_7994" s="T65">n</ta>
            <ta e="T67" id="Seg_7995" s="T66">v</ta>
            <ta e="T68" id="Seg_7996" s="T67">preverb</ta>
            <ta e="T69" id="Seg_7997" s="T68">v</ta>
            <ta e="T70" id="Seg_7998" s="T69">adv</ta>
            <ta e="T71" id="Seg_7999" s="T70">n</ta>
            <ta e="T72" id="Seg_8000" s="T71">adv</ta>
            <ta e="T73" id="Seg_8001" s="T72">ptcl</ta>
            <ta e="T74" id="Seg_8002" s="T73">preverb</ta>
            <ta e="T75" id="Seg_8003" s="T74">v</ta>
            <ta e="T76" id="Seg_8004" s="T75">adj</ta>
            <ta e="T77" id="Seg_8005" s="T76">n</ta>
            <ta e="T78" id="Seg_8006" s="T77">n</ta>
            <ta e="T79" id="Seg_8007" s="T78">adj</ta>
            <ta e="T80" id="Seg_8008" s="T79">adj</ta>
            <ta e="T81" id="Seg_8009" s="T80">v</ta>
            <ta e="T82" id="Seg_8010" s="T81">interrog</ta>
            <ta e="T83" id="Seg_8011" s="T82">qv</ta>
            <ta e="T84" id="Seg_8012" s="T83">dem</ta>
            <ta e="T695" id="Seg_8013" s="T84">n</ta>
            <ta e="T85" id="Seg_8014" s="T695">n</ta>
            <ta e="T86" id="Seg_8015" s="T85">adv</ta>
            <ta e="T87" id="Seg_8016" s="T86">adv</ta>
            <ta e="T88" id="Seg_8017" s="T87">v</ta>
            <ta e="T89" id="Seg_8018" s="T88">adj</ta>
            <ta e="T90" id="Seg_8019" s="T89">n</ta>
            <ta e="T91" id="Seg_8020" s="T90">adj</ta>
            <ta e="T92" id="Seg_8021" s="T91">n</ta>
            <ta e="T93" id="Seg_8022" s="T92">v</ta>
            <ta e="T94" id="Seg_8023" s="T93">v</ta>
            <ta e="T95" id="Seg_8024" s="T94">n</ta>
            <ta e="T96" id="Seg_8025" s="T95">conj</ta>
            <ta e="T97" id="Seg_8026" s="T96">v</ta>
            <ta e="T98" id="Seg_8027" s="T97">adv</ta>
            <ta e="T99" id="Seg_8028" s="T98">n</ta>
            <ta e="T100" id="Seg_8029" s="T99">v</ta>
            <ta e="T101" id="Seg_8030" s="T100">adv</ta>
            <ta e="T102" id="Seg_8031" s="T101">v</ta>
            <ta e="T103" id="Seg_8032" s="T102">v</ta>
            <ta e="T104" id="Seg_8033" s="T103">dem</ta>
            <ta e="T105" id="Seg_8034" s="T104">adv</ta>
            <ta e="T106" id="Seg_8035" s="T105">n</ta>
            <ta e="T107" id="Seg_8036" s="T106">n</ta>
            <ta e="T691" id="Seg_8037" s="T107">n</ta>
            <ta e="T108" id="Seg_8038" s="T691">n</ta>
            <ta e="T109" id="Seg_8039" s="T108">n</ta>
            <ta e="T110" id="Seg_8040" s="T109">v</ta>
            <ta e="T111" id="Seg_8041" s="T110">adj</ta>
            <ta e="T112" id="Seg_8042" s="T111">n</ta>
            <ta e="T113" id="Seg_8043" s="T112">dem</ta>
            <ta e="T114" id="Seg_8044" s="T113">adj</ta>
            <ta e="T115" id="Seg_8045" s="T114">n</ta>
            <ta e="T116" id="Seg_8046" s="T115">num</ta>
            <ta e="T117" id="Seg_8047" s="T116">n</ta>
            <ta e="T118" id="Seg_8048" s="T117">adj</ta>
            <ta e="T119" id="Seg_8049" s="T118">adv</ta>
            <ta e="T120" id="Seg_8050" s="T119">adv</ta>
            <ta e="T121" id="Seg_8051" s="T120">num</ta>
            <ta e="T122" id="Seg_8052" s="T121">n</ta>
            <ta e="T123" id="Seg_8053" s="T122">v</ta>
            <ta e="T124" id="Seg_8054" s="T123">n</ta>
            <ta e="T125" id="Seg_8055" s="T124">n</ta>
            <ta e="T126" id="Seg_8056" s="T125">dem</ta>
            <ta e="T692" id="Seg_8057" s="T126">n</ta>
            <ta e="T127" id="Seg_8058" s="T692">n</ta>
            <ta e="T128" id="Seg_8059" s="T127">v</ta>
            <ta e="T129" id="Seg_8060" s="T128">interrog</ta>
            <ta e="T130" id="Seg_8061" s="T129">n</ta>
            <ta e="T131" id="Seg_8062" s="T130">n</ta>
            <ta e="T132" id="Seg_8063" s="T131">quant</ta>
            <ta e="T133" id="Seg_8064" s="T132">v</ta>
            <ta e="T134" id="Seg_8065" s="T133">adv</ta>
            <ta e="T135" id="Seg_8066" s="T134">v</ta>
            <ta e="T136" id="Seg_8067" s="T135">ptcl</ta>
            <ta e="T137" id="Seg_8068" s="T136">n</ta>
            <ta e="T138" id="Seg_8069" s="T137">v</ta>
            <ta e="T139" id="Seg_8070" s="T138">dem</ta>
            <ta e="T140" id="Seg_8071" s="T139">adj</ta>
            <ta e="T141" id="Seg_8072" s="T140">n</ta>
            <ta e="T142" id="Seg_8073" s="T141">num</ta>
            <ta e="T143" id="Seg_8074" s="T142">num</ta>
            <ta e="T144" id="Seg_8075" s="T143">num</ta>
            <ta e="T145" id="Seg_8076" s="T144">n</ta>
            <ta e="T146" id="Seg_8077" s="T145">dem</ta>
            <ta e="T147" id="Seg_8078" s="T146">n</ta>
            <ta e="T148" id="Seg_8079" s="T147">n</ta>
            <ta e="T149" id="Seg_8080" s="T148">v</ta>
            <ta e="T150" id="Seg_8081" s="T149">v</ta>
            <ta e="T151" id="Seg_8082" s="T150">n</ta>
            <ta e="T152" id="Seg_8083" s="T151">v</ta>
            <ta e="T153" id="Seg_8084" s="T152">nprop</ta>
            <ta e="T154" id="Seg_8085" s="T153">n</ta>
            <ta e="T155" id="Seg_8086" s="T154">n</ta>
            <ta e="T156" id="Seg_8087" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_8088" s="T156">v</ta>
            <ta e="T158" id="Seg_8089" s="T157">adv</ta>
            <ta e="T159" id="Seg_8090" s="T158">adv</ta>
            <ta e="T160" id="Seg_8091" s="T159">v</ta>
            <ta e="T161" id="Seg_8092" s="T160">ptcl</ta>
            <ta e="T162" id="Seg_8093" s="T161">pers</ta>
            <ta e="T163" id="Seg_8094" s="T162">n</ta>
            <ta e="T164" id="Seg_8095" s="T163">dem</ta>
            <ta e="T165" id="Seg_8096" s="T164">n</ta>
            <ta e="T166" id="Seg_8097" s="T165">ptcl</ta>
            <ta e="T167" id="Seg_8098" s="T166">v</ta>
            <ta e="T168" id="Seg_8099" s="T167">n</ta>
            <ta e="T169" id="Seg_8100" s="T168">conj</ta>
            <ta e="T170" id="Seg_8101" s="T169">pers</ta>
            <ta e="T171" id="Seg_8102" s="T170">v</ta>
            <ta e="T172" id="Seg_8103" s="T171">pers</ta>
            <ta e="T173" id="Seg_8104" s="T172">interrog</ta>
            <ta e="T174" id="Seg_8105" s="T173">adv</ta>
            <ta e="T175" id="Seg_8106" s="T174">v</ta>
            <ta e="T176" id="Seg_8107" s="T175">n</ta>
            <ta e="T177" id="Seg_8108" s="T176">conj</ta>
            <ta e="T178" id="Seg_8109" s="T177">v</ta>
            <ta e="T179" id="Seg_8110" s="T178">adj</ta>
            <ta e="T180" id="Seg_8111" s="T179">ptcp</ta>
            <ta e="T181" id="Seg_8112" s="T180">n</ta>
            <ta e="T182" id="Seg_8113" s="T181">v</ta>
            <ta e="T183" id="Seg_8114" s="T182">ptcl</ta>
            <ta e="T184" id="Seg_8115" s="T183">adj</ta>
            <ta e="T185" id="Seg_8116" s="T184">n</ta>
            <ta e="T187" id="Seg_8117" s="T186">ptcl</ta>
            <ta e="T188" id="Seg_8118" s="T187">nprop</ta>
            <ta e="T189" id="Seg_8119" s="T188">n</ta>
            <ta e="T190" id="Seg_8120" s="T189">num</ta>
            <ta e="T191" id="Seg_8121" s="T190">n</ta>
            <ta e="T192" id="Seg_8122" s="T191">v</ta>
            <ta e="T193" id="Seg_8123" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_8124" s="T193">n</ta>
            <ta e="T195" id="Seg_8125" s="T194">v</ta>
            <ta e="T196" id="Seg_8126" s="T195">dem</ta>
            <ta e="T197" id="Seg_8127" s="T196">dem</ta>
            <ta e="T198" id="Seg_8128" s="T197">n</ta>
            <ta e="T199" id="Seg_8129" s="T198">n</ta>
            <ta e="T200" id="Seg_8130" s="T199">v</ta>
            <ta e="T201" id="Seg_8131" s="T200">interrog</ta>
            <ta e="T202" id="Seg_8132" s="T201">adj</ta>
            <ta e="T203" id="Seg_8133" s="T202">n</ta>
            <ta e="T204" id="Seg_8134" s="T203">adj</ta>
            <ta e="T205" id="Seg_8135" s="T204">interrog</ta>
            <ta e="T206" id="Seg_8136" s="T205">adj</ta>
            <ta e="T207" id="Seg_8137" s="T206">interrog</ta>
            <ta e="T208" id="Seg_8138" s="T207">quant</ta>
            <ta e="T209" id="Seg_8139" s="T208">v</ta>
            <ta e="T210" id="Seg_8140" s="T209">adv</ta>
            <ta e="T211" id="Seg_8141" s="T210">adv</ta>
            <ta e="T212" id="Seg_8142" s="T211">v</ta>
            <ta e="T213" id="Seg_8143" s="T212">n</ta>
            <ta e="T214" id="Seg_8144" s="T213">n</ta>
            <ta e="T215" id="Seg_8145" s="T214">adv</ta>
            <ta e="T216" id="Seg_8146" s="T215">v</ta>
            <ta e="T217" id="Seg_8147" s="T216">adv</ta>
            <ta e="T218" id="Seg_8148" s="T217">adv</ta>
            <ta e="T220" id="Seg_8149" s="T219">adv</ta>
            <ta e="T221" id="Seg_8150" s="T220">v</ta>
            <ta e="T222" id="Seg_8151" s="T221">dem</ta>
            <ta e="T223" id="Seg_8152" s="T222">n</ta>
            <ta e="T224" id="Seg_8153" s="T223">adj</ta>
            <ta e="T225" id="Seg_8154" s="T224">adv</ta>
            <ta e="T226" id="Seg_8155" s="T225">ptcl</ta>
            <ta e="T227" id="Seg_8156" s="T226">v</ta>
            <ta e="T228" id="Seg_8157" s="T227">pers</ta>
            <ta e="T229" id="Seg_8158" s="T228">ptcl</ta>
            <ta e="T230" id="Seg_8159" s="T229">interrog</ta>
            <ta e="T231" id="Seg_8160" s="T230">ptcl</ta>
            <ta e="T232" id="Seg_8161" s="T231">v</ta>
            <ta e="T233" id="Seg_8162" s="T232">adj</ta>
            <ta e="T234" id="Seg_8163" s="T233">n</ta>
            <ta e="T235" id="Seg_8164" s="T234">preverb</ta>
            <ta e="T236" id="Seg_8165" s="T235">v</ta>
            <ta e="T237" id="Seg_8166" s="T236">pro</ta>
            <ta e="T238" id="Seg_8167" s="T237">n</ta>
            <ta e="T239" id="Seg_8168" s="T238">adj</ta>
            <ta e="T240" id="Seg_8169" s="T239">v</ta>
            <ta e="T241" id="Seg_8170" s="T240">n</ta>
            <ta e="T242" id="Seg_8171" s="T241">ptcl</ta>
            <ta e="T243" id="Seg_8172" s="T242">adv</ta>
            <ta e="T244" id="Seg_8173" s="T243">adv</ta>
            <ta e="T245" id="Seg_8174" s="T244">num</ta>
            <ta e="T246" id="Seg_8175" s="T245">num</ta>
            <ta e="T247" id="Seg_8176" s="T246">n</ta>
            <ta e="T248" id="Seg_8177" s="T247">adj</ta>
            <ta e="T249" id="Seg_8178" s="T248">n</ta>
            <ta e="T250" id="Seg_8179" s="T249">v</ta>
            <ta e="T251" id="Seg_8180" s="T250">adv</ta>
            <ta e="T252" id="Seg_8181" s="T251">v</ta>
            <ta e="T253" id="Seg_8182" s="T252">n</ta>
            <ta e="T254" id="Seg_8183" s="T253">v</ta>
            <ta e="T255" id="Seg_8184" s="T254">n</ta>
            <ta e="T256" id="Seg_8185" s="T255">v</ta>
            <ta e="T257" id="Seg_8186" s="T256">n</ta>
            <ta e="T258" id="Seg_8187" s="T257">n</ta>
            <ta e="T259" id="Seg_8188" s="T258">n</ta>
            <ta e="T260" id="Seg_8189" s="T259">pp</ta>
            <ta e="T261" id="Seg_8190" s="T260">ptcp</ta>
            <ta e="T262" id="Seg_8191" s="T261">interrog</ta>
            <ta e="T263" id="Seg_8192" s="T262">v</ta>
            <ta e="T264" id="Seg_8193" s="T263">dem</ta>
            <ta e="T265" id="Seg_8194" s="T264">n</ta>
            <ta e="T266" id="Seg_8195" s="T265">adv</ta>
            <ta e="T267" id="Seg_8196" s="T266">v</ta>
            <ta e="T268" id="Seg_8197" s="T267">adv</ta>
            <ta e="T269" id="Seg_8198" s="T268">adv</ta>
            <ta e="T270" id="Seg_8199" s="T269">v</ta>
            <ta e="T271" id="Seg_8200" s="T270">pers</ta>
            <ta e="T272" id="Seg_8201" s="T271">ptcl</ta>
            <ta e="T273" id="Seg_8202" s="T272">adj</ta>
            <ta e="T274" id="Seg_8203" s="T273">n</ta>
            <ta e="T275" id="Seg_8204" s="T274">v</ta>
            <ta e="T276" id="Seg_8205" s="T275">n</ta>
            <ta e="T277" id="Seg_8206" s="T276">pers</ta>
            <ta e="T278" id="Seg_8207" s="T277">v</ta>
            <ta e="T279" id="Seg_8208" s="T278">conj</ta>
            <ta e="T280" id="Seg_8209" s="T279">dem</ta>
            <ta e="T281" id="Seg_8210" s="T280">dem</ta>
            <ta e="T282" id="Seg_8211" s="T281">n</ta>
            <ta e="T283" id="Seg_8212" s="T282">n</ta>
            <ta e="T284" id="Seg_8213" s="T283">n</ta>
            <ta e="T285" id="Seg_8214" s="T284">dem</ta>
            <ta e="T286" id="Seg_8215" s="T285">v</ta>
            <ta e="T287" id="Seg_8216" s="T286">dem</ta>
            <ta e="T289" id="Seg_8217" s="T287">n</ta>
            <ta e="T290" id="Seg_8218" s="T289">adv</ta>
            <ta e="T291" id="Seg_8219" s="T290">adv</ta>
            <ta e="T292" id="Seg_8220" s="T291">adj</ta>
            <ta e="T293" id="Seg_8221" s="T292">n</ta>
            <ta e="T294" id="Seg_8222" s="T293">n</ta>
            <ta e="T295" id="Seg_8223" s="T294">adv</ta>
            <ta e="T296" id="Seg_8224" s="T295">v</ta>
            <ta e="T297" id="Seg_8225" s="T296">adj</ta>
            <ta e="T298" id="Seg_8226" s="T297">n</ta>
            <ta e="T299" id="Seg_8227" s="T298">v</ta>
            <ta e="T300" id="Seg_8228" s="T299">ptcl</ta>
            <ta e="T301" id="Seg_8229" s="T300">v</ta>
            <ta e="T302" id="Seg_8230" s="T301">adv</ta>
            <ta e="T303" id="Seg_8231" s="T302">ptcl</ta>
            <ta e="T304" id="Seg_8232" s="T303">ptcl</ta>
            <ta e="T305" id="Seg_8233" s="T304">adj</ta>
            <ta e="T306" id="Seg_8234" s="T305">n</ta>
            <ta e="T307" id="Seg_8235" s="T306">v</ta>
            <ta e="T308" id="Seg_8236" s="T307">nprop</ta>
            <ta e="T309" id="Seg_8237" s="T308">n</ta>
            <ta e="T310" id="Seg_8238" s="T309">pers</ta>
            <ta e="T311" id="Seg_8239" s="T310">dem</ta>
            <ta e="T312" id="Seg_8240" s="T311">n</ta>
            <ta e="T313" id="Seg_8241" s="T312">n</ta>
            <ta e="T314" id="Seg_8242" s="T313">v</ta>
            <ta e="T315" id="Seg_8243" s="T314">n</ta>
            <ta e="T316" id="Seg_8244" s="T315">n</ta>
            <ta e="T317" id="Seg_8245" s="T316">adv</ta>
            <ta e="T318" id="Seg_8246" s="T317">adj</ta>
            <ta e="T319" id="Seg_8247" s="T318">v</ta>
            <ta e="T320" id="Seg_8248" s="T319">dem</ta>
            <ta e="T321" id="Seg_8249" s="T320">n</ta>
            <ta e="T322" id="Seg_8250" s="T321">ptcl</ta>
            <ta e="T323" id="Seg_8251" s="T322">v</ta>
            <ta e="T324" id="Seg_8252" s="T323">ptcl</ta>
            <ta e="T325" id="Seg_8253" s="T324">pers</ta>
            <ta e="T326" id="Seg_8254" s="T325">ptcl</ta>
            <ta e="T327" id="Seg_8255" s="T326">v</ta>
            <ta e="T328" id="Seg_8256" s="T327">n</ta>
            <ta e="T329" id="Seg_8257" s="T328">ptcl</ta>
            <ta e="T330" id="Seg_8258" s="T329">v</ta>
            <ta e="T331" id="Seg_8259" s="T330">dem</ta>
            <ta e="T332" id="Seg_8260" s="T331">n</ta>
            <ta e="T333" id="Seg_8261" s="T332">n</ta>
            <ta e="T334" id="Seg_8262" s="T333">adj</ta>
            <ta e="T335" id="Seg_8263" s="T334">n</ta>
            <ta e="T336" id="Seg_8264" s="T335">adv</ta>
            <ta e="T337" id="Seg_8265" s="T336">v</ta>
            <ta e="T338" id="Seg_8266" s="T337">dem</ta>
            <ta e="T339" id="Seg_8267" s="T338">n</ta>
            <ta e="T340" id="Seg_8268" s="T339">ptcl</ta>
            <ta e="T341" id="Seg_8269" s="T340">adj</ta>
            <ta e="T342" id="Seg_8270" s="T341">n</ta>
            <ta e="T343" id="Seg_8271" s="T342">interrog</ta>
            <ta e="T344" id="Seg_8272" s="T343">n</ta>
            <ta e="T345" id="Seg_8273" s="T344">v</ta>
            <ta e="T346" id="Seg_8274" s="T345">adj</ta>
            <ta e="T347" id="Seg_8275" s="T346">n</ta>
            <ta e="T348" id="Seg_8276" s="T347">adj</ta>
            <ta e="T349" id="Seg_8277" s="T348">n</ta>
            <ta e="T350" id="Seg_8278" s="T349">adv</ta>
            <ta e="T351" id="Seg_8279" s="T350">adv</ta>
            <ta e="T352" id="Seg_8280" s="T351">v</ta>
            <ta e="T353" id="Seg_8281" s="T352">dem</ta>
            <ta e="T354" id="Seg_8282" s="T353">n</ta>
            <ta e="T355" id="Seg_8283" s="T354">adj</ta>
            <ta e="T356" id="Seg_8284" s="T355">adv</ta>
            <ta e="T357" id="Seg_8285" s="T356">adv</ta>
            <ta e="T358" id="Seg_8286" s="T357">v</ta>
            <ta e="T359" id="Seg_8287" s="T358">dem</ta>
            <ta e="T360" id="Seg_8288" s="T359">n</ta>
            <ta e="T361" id="Seg_8289" s="T360">pers</ta>
            <ta e="T362" id="Seg_8290" s="T361">ptcl</ta>
            <ta e="T363" id="Seg_8291" s="T362">adv</ta>
            <ta e="T364" id="Seg_8292" s="T363">n</ta>
            <ta e="T365" id="Seg_8293" s="T364">v</ta>
            <ta e="T366" id="Seg_8294" s="T365">conj</ta>
            <ta e="T367" id="Seg_8295" s="T366">ptcl</ta>
            <ta e="T368" id="Seg_8296" s="T367">v</ta>
            <ta e="T369" id="Seg_8297" s="T368">conj</ta>
            <ta e="T370" id="Seg_8298" s="T369">v</ta>
            <ta e="T371" id="Seg_8299" s="T370">adv</ta>
            <ta e="T372" id="Seg_8300" s="T371">v</ta>
            <ta e="T373" id="Seg_8301" s="T372">num</ta>
            <ta e="T374" id="Seg_8302" s="T373">n</ta>
            <ta e="T375" id="Seg_8303" s="T374">n</ta>
            <ta e="T376" id="Seg_8304" s="T375">ptcl</ta>
            <ta e="T377" id="Seg_8305" s="T376">adj</ta>
            <ta e="T378" id="Seg_8306" s="T377">n</ta>
            <ta e="T379" id="Seg_8307" s="T378">v</ta>
            <ta e="T380" id="Seg_8308" s="T379">n</ta>
            <ta e="T381" id="Seg_8309" s="T380">adv</ta>
            <ta e="T382" id="Seg_8310" s="T381">v</ta>
            <ta e="T383" id="Seg_8311" s="T382">n</ta>
            <ta e="T384" id="Seg_8312" s="T383">emphpro</ta>
            <ta e="T385" id="Seg_8313" s="T384">v</ta>
            <ta e="T386" id="Seg_8314" s="T385">ptcl</ta>
            <ta e="T387" id="Seg_8315" s="T386">v</ta>
            <ta e="T388" id="Seg_8316" s="T387">ptcl</ta>
            <ta e="T389" id="Seg_8317" s="T388">v</ta>
            <ta e="T390" id="Seg_8318" s="T389">num</ta>
            <ta e="T391" id="Seg_8319" s="T390">dem</ta>
            <ta e="T392" id="Seg_8320" s="T391">n</ta>
            <ta e="T393" id="Seg_8321" s="T392">n</ta>
            <ta e="T394" id="Seg_8322" s="T393">n</ta>
            <ta e="T395" id="Seg_8323" s="T394">ptcl</ta>
            <ta e="T396" id="Seg_8324" s="T395">v</ta>
            <ta e="T397" id="Seg_8325" s="T396">dem</ta>
            <ta e="T398" id="Seg_8326" s="T397">n</ta>
            <ta e="T399" id="Seg_8327" s="T398">adv</ta>
            <ta e="T400" id="Seg_8328" s="T399">v</ta>
            <ta e="T401" id="Seg_8329" s="T400">adv</ta>
            <ta e="T402" id="Seg_8330" s="T401">num</ta>
            <ta e="T403" id="Seg_8331" s="T402">dem</ta>
            <ta e="T404" id="Seg_8332" s="T403">n</ta>
            <ta e="T405" id="Seg_8333" s="T404">n</ta>
            <ta e="T406" id="Seg_8334" s="T405">pro</ta>
            <ta e="T407" id="Seg_8335" s="T406">v</ta>
            <ta e="T408" id="Seg_8336" s="T407">adv</ta>
            <ta e="T409" id="Seg_8337" s="T408">nprop</ta>
            <ta e="T410" id="Seg_8338" s="T409">n</ta>
            <ta e="T411" id="Seg_8339" s="T410">v</ta>
            <ta e="T412" id="Seg_8340" s="T411">num</ta>
            <ta e="T413" id="Seg_8341" s="T412">n</ta>
            <ta e="T414" id="Seg_8342" s="T413">ptcl</ta>
            <ta e="T415" id="Seg_8343" s="T414">n</ta>
            <ta e="T416" id="Seg_8344" s="T415">v</ta>
            <ta e="T417" id="Seg_8345" s="T416">n</ta>
            <ta e="T418" id="Seg_8346" s="T417">n</ta>
            <ta e="T419" id="Seg_8347" s="T418">quant</ta>
            <ta e="T420" id="Seg_8348" s="T419">n</ta>
            <ta e="T421" id="Seg_8349" s="T420">v</ta>
            <ta e="T422" id="Seg_8350" s="T421">adv</ta>
            <ta e="T423" id="Seg_8351" s="T422">adv</ta>
            <ta e="T424" id="Seg_8352" s="T423">dem</ta>
            <ta e="T425" id="Seg_8353" s="T424">n</ta>
            <ta e="T426" id="Seg_8354" s="T425">v</ta>
            <ta e="T427" id="Seg_8355" s="T426">n</ta>
            <ta e="T428" id="Seg_8356" s="T427">v</ta>
            <ta e="T429" id="Seg_8357" s="T428">ptcl</ta>
            <ta e="T430" id="Seg_8358" s="T429">ptcl</ta>
            <ta e="T431" id="Seg_8359" s="T430">v</ta>
            <ta e="T432" id="Seg_8360" s="T431">nprop</ta>
            <ta e="T433" id="Seg_8361" s="T432">n</ta>
            <ta e="T434" id="Seg_8362" s="T433">ptcl</ta>
            <ta e="T435" id="Seg_8363" s="T434">ptcl</ta>
            <ta e="T436" id="Seg_8364" s="T435">v</ta>
            <ta e="T437" id="Seg_8365" s="T436">adj</ta>
            <ta e="T438" id="Seg_8366" s="T437">n</ta>
            <ta e="T439" id="Seg_8367" s="T438">adv</ta>
            <ta e="T440" id="Seg_8368" s="T439">v</ta>
            <ta e="T441" id="Seg_8369" s="T440">n</ta>
            <ta e="T442" id="Seg_8370" s="T441">n</ta>
            <ta e="T443" id="Seg_8371" s="T442">n</ta>
            <ta e="T444" id="Seg_8372" s="T443">v</ta>
            <ta e="T445" id="Seg_8373" s="T444">n</ta>
            <ta e="T446" id="Seg_8374" s="T445">adv</ta>
            <ta e="T447" id="Seg_8375" s="T446">adv</ta>
            <ta e="T448" id="Seg_8376" s="T447">n</ta>
            <ta e="T449" id="Seg_8377" s="T448">adv</ta>
            <ta e="T450" id="Seg_8378" s="T449">ptcl</ta>
            <ta e="T451" id="Seg_8379" s="T450">n</ta>
            <ta e="T452" id="Seg_8380" s="T451">v</ta>
            <ta e="T453" id="Seg_8381" s="T452">n</ta>
            <ta e="T454" id="Seg_8382" s="T453">ptcl</ta>
            <ta e="T455" id="Seg_8383" s="T454">v</ta>
            <ta e="T456" id="Seg_8384" s="T455">adv</ta>
            <ta e="T457" id="Seg_8385" s="T456">v</ta>
            <ta e="T458" id="Seg_8386" s="T457">nprop</ta>
            <ta e="T459" id="Seg_8387" s="T458">n</ta>
            <ta e="T460" id="Seg_8388" s="T459">adv</ta>
            <ta e="T461" id="Seg_8389" s="T460">adv</ta>
            <ta e="T462" id="Seg_8390" s="T461">v</ta>
            <ta e="T463" id="Seg_8391" s="T462">conj</ta>
            <ta e="T464" id="Seg_8392" s="T463">ptcl</ta>
            <ta e="T465" id="Seg_8393" s="T464">n</ta>
            <ta e="T466" id="Seg_8394" s="T465">n</ta>
            <ta e="T467" id="Seg_8395" s="T466">ptcl</ta>
            <ta e="T468" id="Seg_8396" s="T467">adv</ta>
            <ta e="T469" id="Seg_8397" s="T468">ptcl</ta>
            <ta e="T470" id="Seg_8398" s="T469">adv</ta>
            <ta e="T471" id="Seg_8399" s="T470">v</ta>
            <ta e="T472" id="Seg_8400" s="T471">ptcl</ta>
            <ta e="T473" id="Seg_8401" s="T472">n</ta>
            <ta e="T474" id="Seg_8402" s="T473">v</ta>
            <ta e="T475" id="Seg_8403" s="T474">nprop</ta>
            <ta e="T476" id="Seg_8404" s="T475">adj</ta>
            <ta e="T477" id="Seg_8405" s="T476">n</ta>
            <ta e="T478" id="Seg_8406" s="T477">quant</ta>
            <ta e="T479" id="Seg_8407" s="T478">adv</ta>
            <ta e="T480" id="Seg_8408" s="T479">v</ta>
            <ta e="T481" id="Seg_8409" s="T480">adj</ta>
            <ta e="T482" id="Seg_8410" s="T481">v</ta>
            <ta e="T483" id="Seg_8411" s="T482">nprop</ta>
            <ta e="T484" id="Seg_8412" s="T483">n</ta>
            <ta e="T485" id="Seg_8413" s="T484">adv</ta>
            <ta e="T486" id="Seg_8414" s="T485">v</ta>
            <ta e="T487" id="Seg_8415" s="T486">num</ta>
            <ta e="T488" id="Seg_8416" s="T487">adv</ta>
            <ta e="T489" id="Seg_8417" s="T488">v</ta>
            <ta e="T490" id="Seg_8418" s="T489">adv</ta>
            <ta e="T491" id="Seg_8419" s="T490">v</ta>
            <ta e="T492" id="Seg_8420" s="T491">dem</ta>
            <ta e="T493" id="Seg_8421" s="T492">n</ta>
            <ta e="T494" id="Seg_8422" s="T493">num</ta>
            <ta e="T495" id="Seg_8423" s="T494">n</ta>
            <ta e="T496" id="Seg_8424" s="T495">n</ta>
            <ta e="T497" id="Seg_8425" s="T496">v</ta>
            <ta e="T498" id="Seg_8426" s="T497">quant</ta>
            <ta e="T499" id="Seg_8427" s="T498">n</ta>
            <ta e="T500" id="Seg_8428" s="T499">pp</ta>
            <ta e="T501" id="Seg_8429" s="T500">n</ta>
            <ta e="T502" id="Seg_8430" s="T501">pp</ta>
            <ta e="T503" id="Seg_8431" s="T502">v</ta>
            <ta e="T504" id="Seg_8432" s="T503">dem</ta>
            <ta e="T505" id="Seg_8433" s="T504">n</ta>
            <ta e="T506" id="Seg_8434" s="T505">adv</ta>
            <ta e="T507" id="Seg_8435" s="T506">adv</ta>
            <ta e="T508" id="Seg_8436" s="T507">adv</ta>
            <ta e="T509" id="Seg_8437" s="T508">v</ta>
            <ta e="T510" id="Seg_8438" s="T509">n</ta>
            <ta e="T511" id="Seg_8439" s="T510">dem</ta>
            <ta e="T512" id="Seg_8440" s="T511">n</ta>
            <ta e="T513" id="Seg_8441" s="T512">n</ta>
            <ta e="T514" id="Seg_8442" s="T513">n</ta>
            <ta e="T515" id="Seg_8443" s="T514">ptcl</ta>
            <ta e="T516" id="Seg_8444" s="T515">v</ta>
            <ta e="T517" id="Seg_8445" s="T516">v</ta>
            <ta e="T518" id="Seg_8446" s="T517">n</ta>
            <ta e="T519" id="Seg_8447" s="T518">ptcl</ta>
            <ta e="T520" id="Seg_8448" s="T519">adv</ta>
            <ta e="T521" id="Seg_8449" s="T520">v</ta>
            <ta e="T522" id="Seg_8450" s="T521">v</ta>
            <ta e="T523" id="Seg_8451" s="T522">ptcl</ta>
            <ta e="T524" id="Seg_8452" s="T523">quant</ta>
            <ta e="T525" id="Seg_8453" s="T524">adv</ta>
            <ta e="T526" id="Seg_8454" s="T525">v</ta>
            <ta e="T527" id="Seg_8455" s="T526">preverb</ta>
            <ta e="T528" id="Seg_8456" s="T527">v</ta>
            <ta e="T529" id="Seg_8457" s="T528">adj</ta>
            <ta e="T530" id="Seg_8458" s="T529">n</ta>
            <ta e="T531" id="Seg_8459" s="T530">preverb</ta>
            <ta e="T532" id="Seg_8460" s="T531">adv</ta>
            <ta e="T533" id="Seg_8461" s="T532">adv</ta>
            <ta e="T534" id="Seg_8462" s="T533">preverb</ta>
            <ta e="T535" id="Seg_8463" s="T534">v</ta>
            <ta e="T536" id="Seg_8464" s="T535">v</ta>
            <ta e="T537" id="Seg_8465" s="T536">dem</ta>
            <ta e="T538" id="Seg_8466" s="T537">n</ta>
            <ta e="T539" id="Seg_8467" s="T538">n</ta>
            <ta e="T540" id="Seg_8468" s="T539">ptcl</ta>
            <ta e="T541" id="Seg_8469" s="T540">v</ta>
            <ta e="T542" id="Seg_8470" s="T541">conj</ta>
            <ta e="T543" id="Seg_8471" s="T542">v</ta>
            <ta e="T544" id="Seg_8472" s="T543">adv</ta>
            <ta e="T545" id="Seg_8473" s="T544">v</ta>
            <ta e="T546" id="Seg_8474" s="T545">adj</ta>
            <ta e="T547" id="Seg_8475" s="T546">adv</ta>
            <ta e="T548" id="Seg_8476" s="T547">preverb</ta>
            <ta e="T549" id="Seg_8477" s="T548">v</ta>
            <ta e="T550" id="Seg_8478" s="T549">n</ta>
            <ta e="T551" id="Seg_8479" s="T550">n</ta>
            <ta e="T552" id="Seg_8480" s="T551">adv</ta>
            <ta e="T553" id="Seg_8481" s="T552">pers</ta>
            <ta e="T554" id="Seg_8482" s="T553">adj</ta>
            <ta e="T555" id="Seg_8483" s="T554">n</ta>
            <ta e="T556" id="Seg_8484" s="T555">adv</ta>
            <ta e="T557" id="Seg_8485" s="T556">v</ta>
            <ta e="T558" id="Seg_8486" s="T557">adv</ta>
            <ta e="T559" id="Seg_8487" s="T558">n</ta>
            <ta e="T560" id="Seg_8488" s="T559">v</ta>
            <ta e="T561" id="Seg_8489" s="T560">dem</ta>
            <ta e="T562" id="Seg_8490" s="T561">n</ta>
            <ta e="T563" id="Seg_8491" s="T562">adv</ta>
            <ta e="T564" id="Seg_8492" s="T563">adj</ta>
            <ta e="T565" id="Seg_8493" s="T564">v</ta>
            <ta e="T566" id="Seg_8494" s="T565">adj</ta>
            <ta e="T567" id="Seg_8495" s="T566">v</ta>
            <ta e="T568" id="Seg_8496" s="T567">dem</ta>
            <ta e="T569" id="Seg_8497" s="T568">n</ta>
            <ta e="T570" id="Seg_8498" s="T569">ptcl</ta>
            <ta e="T571" id="Seg_8499" s="T570">dem</ta>
            <ta e="T572" id="Seg_8500" s="T571">n</ta>
            <ta e="T573" id="Seg_8501" s="T572">adv</ta>
            <ta e="T574" id="Seg_8502" s="T573">v</ta>
            <ta e="T576" id="Seg_8503" s="T575">emphpro</ta>
            <ta e="T577" id="Seg_8504" s="T576">n</ta>
            <ta e="T578" id="Seg_8505" s="T577">adv</ta>
            <ta e="T579" id="Seg_8506" s="T578">adv</ta>
            <ta e="T580" id="Seg_8507" s="T579">dem</ta>
            <ta e="T581" id="Seg_8508" s="T580">pers</ta>
            <ta e="T582" id="Seg_8509" s="T581">n</ta>
            <ta e="T583" id="Seg_8510" s="T582">v</ta>
            <ta e="T584" id="Seg_8511" s="T583">conj</ta>
            <ta e="T585" id="Seg_8512" s="T584">v</ta>
            <ta e="T586" id="Seg_8513" s="T585">conj</ta>
            <ta e="T587" id="Seg_8514" s="T586">ptcl</ta>
            <ta e="T588" id="Seg_8515" s="T587">adv</ta>
            <ta e="T589" id="Seg_8516" s="T588">dem</ta>
            <ta e="T590" id="Seg_8517" s="T589">ptcp</ta>
            <ta e="T591" id="Seg_8518" s="T590">n</ta>
            <ta e="T592" id="Seg_8519" s="T591">adv</ta>
            <ta e="T593" id="Seg_8520" s="T592">adv</ta>
            <ta e="T594" id="Seg_8521" s="T593">adv</ta>
            <ta e="T595" id="Seg_8522" s="T594">dem</ta>
            <ta e="T596" id="Seg_8523" s="T595">n</ta>
            <ta e="T597" id="Seg_8524" s="T596">ptcp</ta>
            <ta e="T598" id="Seg_8525" s="T597">n</ta>
            <ta e="T599" id="Seg_8526" s="T598">adv</ta>
            <ta e="T600" id="Seg_8527" s="T599">v</ta>
            <ta e="T601" id="Seg_8528" s="T600">adv</ta>
            <ta e="T602" id="Seg_8529" s="T601">adv</ta>
            <ta e="T603" id="Seg_8530" s="T602">nprop</ta>
            <ta e="T604" id="Seg_8531" s="T603">n</ta>
            <ta e="T605" id="Seg_8532" s="T604">ptcl</ta>
            <ta e="T606" id="Seg_8533" s="T605">adv</ta>
            <ta e="T607" id="Seg_8534" s="T606">v</ta>
            <ta e="T608" id="Seg_8535" s="T607">interrog</ta>
            <ta e="T609" id="Seg_8536" s="T608">v</ta>
            <ta e="T611" id="Seg_8537" s="T610">v</ta>
            <ta e="T612" id="Seg_8538" s="T611">v</ta>
            <ta e="T613" id="Seg_8539" s="T612">dem</ta>
            <ta e="T614" id="Seg_8540" s="T613">n</ta>
            <ta e="T615" id="Seg_8541" s="T614">adv</ta>
            <ta e="T616" id="Seg_8542" s="T615">v</ta>
            <ta e="T617" id="Seg_8543" s="T616">n</ta>
            <ta e="T618" id="Seg_8544" s="T617">adv</ta>
            <ta e="T619" id="Seg_8545" s="T618">v</ta>
            <ta e="T620" id="Seg_8546" s="T619">v</ta>
            <ta e="T621" id="Seg_8547" s="T620">preverb</ta>
            <ta e="T622" id="Seg_8548" s="T621">v</ta>
            <ta e="T623" id="Seg_8549" s="T622">pers</ta>
            <ta e="T624" id="Seg_8550" s="T623">adv</ta>
            <ta e="T625" id="Seg_8551" s="T624">v</ta>
            <ta e="T626" id="Seg_8552" s="T625">n</ta>
            <ta e="T627" id="Seg_8553" s="T626">ptcl</ta>
            <ta e="T628" id="Seg_8554" s="T627">v</ta>
            <ta e="T629" id="Seg_8555" s="T628">adj</ta>
            <ta e="T630" id="Seg_8556" s="T629">n</ta>
            <ta e="T631" id="Seg_8557" s="T630">clit</ta>
            <ta e="T632" id="Seg_8558" s="T631">interrog</ta>
            <ta e="T633" id="Seg_8559" s="T632">adv</ta>
            <ta e="T634" id="Seg_8560" s="T633">v</ta>
            <ta e="T635" id="Seg_8561" s="T634">dem</ta>
            <ta e="T636" id="Seg_8562" s="T635">emphpro</ta>
            <ta e="T637" id="Seg_8563" s="T636">ptcl</ta>
            <ta e="T638" id="Seg_8564" s="T637">v</ta>
            <ta e="T639" id="Seg_8565" s="T638">dem</ta>
            <ta e="T640" id="Seg_8566" s="T639">n</ta>
            <ta e="T641" id="Seg_8567" s="T640">ptcl</ta>
            <ta e="T642" id="Seg_8568" s="T641">v</ta>
            <ta e="T643" id="Seg_8569" s="T642">adj</ta>
            <ta e="T644" id="Seg_8570" s="T643">adj</ta>
            <ta e="T645" id="Seg_8571" s="T644">n</ta>
            <ta e="T646" id="Seg_8572" s="T645">adj</ta>
            <ta e="T647" id="Seg_8573" s="T646">n</ta>
            <ta e="T648" id="Seg_8574" s="T647">adv</ta>
            <ta e="T649" id="Seg_8575" s="T648">v</ta>
            <ta e="T650" id="Seg_8576" s="T649">conj</ta>
            <ta e="T651" id="Seg_8577" s="T650">adv</ta>
            <ta e="T652" id="Seg_8578" s="T651">adj</ta>
            <ta e="T653" id="Seg_8579" s="T652">n</ta>
            <ta e="T654" id="Seg_8580" s="T653">adj</ta>
            <ta e="T655" id="Seg_8581" s="T654">ptcl</ta>
            <ta e="T656" id="Seg_8582" s="T655">interrog</ta>
            <ta e="T657" id="Seg_8583" s="T656">ptcl</ta>
            <ta e="T658" id="Seg_8584" s="T657">v</ta>
            <ta e="T659" id="Seg_8585" s="T658">interj</ta>
            <ta e="T660" id="Seg_8586" s="T659">v</ta>
            <ta e="T661" id="Seg_8587" s="T660">pers</ta>
            <ta e="T662" id="Seg_8588" s="T661">n</ta>
            <ta e="T663" id="Seg_8589" s="T662">n</ta>
            <ta e="T664" id="Seg_8590" s="T663">v</ta>
            <ta e="T665" id="Seg_8591" s="T664">adj</ta>
            <ta e="T666" id="Seg_8592" s="T665">n</ta>
            <ta e="T667" id="Seg_8593" s="T666">adv</ta>
            <ta e="T668" id="Seg_8594" s="T667">adv</ta>
            <ta e="T669" id="Seg_8595" s="T668">v</ta>
            <ta e="T670" id="Seg_8596" s="T669">dem</ta>
            <ta e="T671" id="Seg_8597" s="T670">v</ta>
            <ta e="T672" id="Seg_8598" s="T671">adv</ta>
            <ta e="T673" id="Seg_8599" s="T672">ptcl</ta>
            <ta e="T674" id="Seg_8600" s="T673">v</ta>
            <ta e="T675" id="Seg_8601" s="T674">n</ta>
            <ta e="T676" id="Seg_8602" s="T675">n</ta>
            <ta e="T677" id="Seg_8603" s="T676">adv</ta>
            <ta e="T678" id="Seg_8604" s="T677">adv</ta>
            <ta e="T679" id="Seg_8605" s="T678">n</ta>
            <ta e="T680" id="Seg_8606" s="T679">v</ta>
            <ta e="T681" id="Seg_8607" s="T680">preverb</ta>
            <ta e="T682" id="Seg_8608" s="T681">v</ta>
            <ta e="T683" id="Seg_8609" s="T682">dem</ta>
            <ta e="T684" id="Seg_8610" s="T683">num</ta>
            <ta e="T685" id="Seg_8611" s="T684">n</ta>
            <ta e="T686" id="Seg_8612" s="T685">v</ta>
            <ta e="T687" id="Seg_8613" s="T686">nprop</ta>
            <ta e="T688" id="Seg_8614" s="T687">n</ta>
            <ta e="T689" id="Seg_8615" s="T688">n</ta>
            <ta e="T690" id="Seg_8616" s="T689">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_8617" s="T1">np.h:Th 0.3.h:Poss</ta>
            <ta e="T6" id="Seg_8618" s="T5">pro:Th</ta>
            <ta e="T10" id="Seg_8619" s="T9">np.h:Th</ta>
            <ta e="T12" id="Seg_8620" s="T11">0.3.h:P</ta>
            <ta e="T15" id="Seg_8621" s="T14">np.h:Th</ta>
            <ta e="T17" id="Seg_8622" s="T16">np:Poss</ta>
            <ta e="T18" id="Seg_8623" s="T17">np:L</ta>
            <ta e="T21" id="Seg_8624" s="T20">np:Poss</ta>
            <ta e="T22" id="Seg_8625" s="T21">adv:L</ta>
            <ta e="T23" id="Seg_8626" s="T22">0.3.h:Th</ta>
            <ta e="T25" id="Seg_8627" s="T24">0.3.h:P</ta>
            <ta e="T27" id="Seg_8628" s="T26">np.h:Th 0.3.h:Poss</ta>
            <ta e="T31" id="Seg_8629" s="T30">np.h:Th 0.3.h:Poss</ta>
            <ta e="T34" id="Seg_8630" s="T33">pro.h:Th 0.3.h:Poss</ta>
            <ta e="T36" id="Seg_8631" s="T35">adv:Time</ta>
            <ta e="T37" id="Seg_8632" s="T36">np.h:P</ta>
            <ta e="T40" id="Seg_8633" s="T39">np.h:Poss</ta>
            <ta e="T42" id="Seg_8634" s="T41">np.h:Th</ta>
            <ta e="T46" id="Seg_8635" s="T45">np:Com 0.3.h:Poss</ta>
            <ta e="T47" id="Seg_8636" s="T46">np:Com 0.3.h:Poss</ta>
            <ta e="T48" id="Seg_8637" s="T47">pro:L</ta>
            <ta e="T51" id="Seg_8638" s="T50">np.h:Th</ta>
            <ta e="T53" id="Seg_8639" s="T693">np.h:A</ta>
            <ta e="T56" id="Seg_8640" s="T55">np.h:P</ta>
            <ta e="T60" id="Seg_8641" s="T694">np.h:A</ta>
            <ta e="T63" id="Seg_8642" s="T62">adv:G</ta>
            <ta e="T64" id="Seg_8643" s="T63">np:G 0.3.h:Poss</ta>
            <ta e="T65" id="Seg_8644" s="T64">0.3.h:A</ta>
            <ta e="T66" id="Seg_8645" s="T65">np.h:P</ta>
            <ta e="T69" id="Seg_8646" s="T68">0.2.h:A 0.3.h:P</ta>
            <ta e="T70" id="Seg_8647" s="T69">adv:Time</ta>
            <ta e="T71" id="Seg_8648" s="T70">np.h:P 0.3.h:Poss</ta>
            <ta e="T72" id="Seg_8649" s="T71">0.3.h:P</ta>
            <ta e="T78" id="Seg_8650" s="T77">np.h:Th 0.3.h:Poss</ta>
            <ta e="T82" id="Seg_8651" s="T81">pro:G</ta>
            <ta e="T83" id="Seg_8652" s="T82">0.3.h:A</ta>
            <ta e="T85" id="Seg_8653" s="T695">np.h:A</ta>
            <ta e="T87" id="Seg_8654" s="T86">adv:G</ta>
            <ta e="T88" id="Seg_8655" s="T87">0.3.h:Th</ta>
            <ta e="T90" id="Seg_8656" s="T89">np.h:Th 0.3.h:Poss</ta>
            <ta e="T92" id="Seg_8657" s="T91">np.h:Th 0.3.h:Poss</ta>
            <ta e="T94" id="Seg_8658" s="T93">0.3.h:A 0.3.h:Th</ta>
            <ta e="T95" id="Seg_8659" s="T94">np:Th 0.3.h:Poss</ta>
            <ta e="T99" id="Seg_8660" s="T98">np:Ins</ta>
            <ta e="T100" id="Seg_8661" s="T99">0.3.h:Th</ta>
            <ta e="T101" id="Seg_8662" s="T100">adv:L</ta>
            <ta e="T102" id="Seg_8663" s="T101">0.3.h:Th</ta>
            <ta e="T103" id="Seg_8664" s="T102">0.3.h:Th</ta>
            <ta e="T104" id="Seg_8665" s="T103">adv:G</ta>
            <ta e="T106" id="Seg_8666" s="T105">np.h:Poss</ta>
            <ta e="T107" id="Seg_8667" s="T106">np:L</ta>
            <ta e="T108" id="Seg_8668" s="T691">np.h:Poss</ta>
            <ta e="T109" id="Seg_8669" s="T108">np.h:Th</ta>
            <ta e="T112" id="Seg_8670" s="T111">np.h:Th 0.3.h:Poss</ta>
            <ta e="T115" id="Seg_8671" s="T114">np.h:Poss 0.3.h:Poss</ta>
            <ta e="T117" id="Seg_8672" s="T116">np.h:Th</ta>
            <ta e="T119" id="Seg_8673" s="T118">adv:So</ta>
            <ta e="T120" id="Seg_8674" s="T119">adv:G</ta>
            <ta e="T122" id="Seg_8675" s="T121">np.h:A</ta>
            <ta e="T125" id="Seg_8676" s="T124">np:L</ta>
            <ta e="T127" id="Seg_8677" s="T692">np.h:Th</ta>
            <ta e="T130" id="Seg_8678" s="T129">np:Poss 0.3.h:Poss</ta>
            <ta e="T131" id="Seg_8679" s="T130">np:Th</ta>
            <ta e="T133" id="Seg_8680" s="T132">0.3.h:A</ta>
            <ta e="T135" id="Seg_8681" s="T134">0.3.h:A</ta>
            <ta e="T136" id="Seg_8682" s="T135">adv:L</ta>
            <ta e="T137" id="Seg_8683" s="T136">np.h:Th</ta>
            <ta e="T145" id="Seg_8684" s="T144">np:Th 0.3.h:Poss</ta>
            <ta e="T147" id="Seg_8685" s="T146">np.h:Poss</ta>
            <ta e="T148" id="Seg_8686" s="T147">np:Th</ta>
            <ta e="T149" id="Seg_8687" s="T148">0.3.h:A</ta>
            <ta e="T150" id="Seg_8688" s="T149">0.3.h:A</ta>
            <ta e="T151" id="Seg_8689" s="T150">np:Com 0.3.h:Poss</ta>
            <ta e="T152" id="Seg_8690" s="T151">0.3.h:A</ta>
            <ta e="T155" id="Seg_8691" s="T154">np:Th 0.3.h:Poss</ta>
            <ta e="T158" id="Seg_8692" s="T157">adv:Time</ta>
            <ta e="T160" id="Seg_8693" s="T159">0.3.h:Th</ta>
            <ta e="T161" id="Seg_8694" s="T160">adv:L</ta>
            <ta e="T162" id="Seg_8695" s="T161">pro.h:Poss</ta>
            <ta e="T163" id="Seg_8696" s="T162">np:L</ta>
            <ta e="T165" id="Seg_8697" s="T164">np.h:Th</ta>
            <ta e="T167" id="Seg_8698" s="T166">0.3.h:E</ta>
            <ta e="T168" id="Seg_8699" s="T167">np.h:Th</ta>
            <ta e="T170" id="Seg_8700" s="T169">pro.h:G</ta>
            <ta e="T172" id="Seg_8701" s="T171">pro.h:Th</ta>
            <ta e="T176" id="Seg_8702" s="T175">np.h:E</ta>
            <ta e="T182" id="Seg_8703" s="T181">0.3.h:Th</ta>
            <ta e="T185" id="Seg_8704" s="T184">adv:Time</ta>
            <ta e="T189" id="Seg_8705" s="T188">np.h:A</ta>
            <ta e="T191" id="Seg_8706" s="T190">np:Com</ta>
            <ta e="T194" id="Seg_8707" s="T193">np:Th</ta>
            <ta e="T195" id="Seg_8708" s="T194">0.3.h:A</ta>
            <ta e="T198" id="Seg_8709" s="T197">np.h:Poss</ta>
            <ta e="T199" id="Seg_8710" s="T198">np:Th</ta>
            <ta e="T203" id="Seg_8711" s="T202">np:Th</ta>
            <ta e="T207" id="Seg_8712" s="T206">pro:Th</ta>
            <ta e="T208" id="Seg_8713" s="T207">pro:Th</ta>
            <ta e="T209" id="Seg_8714" s="T208">0.3.h:A</ta>
            <ta e="T210" id="Seg_8715" s="T209">adv:Time</ta>
            <ta e="T212" id="Seg_8716" s="T211">0.3.h:A 0.3:Th</ta>
            <ta e="T213" id="Seg_8717" s="T212">np.h:Poss</ta>
            <ta e="T214" id="Seg_8718" s="T213">np:Th</ta>
            <ta e="T215" id="Seg_8719" s="T214">0.3.h:A</ta>
            <ta e="T216" id="Seg_8720" s="T215">0.3.h:A</ta>
            <ta e="T217" id="Seg_8721" s="T216">adv:Time</ta>
            <ta e="T218" id="Seg_8722" s="T217">adv:Time</ta>
            <ta e="T223" id="Seg_8723" s="T222">np.h:A</ta>
            <ta e="T225" id="Seg_8724" s="T224">adv:Time</ta>
            <ta e="T227" id="Seg_8725" s="T226">0.1.h:A</ta>
            <ta e="T228" id="Seg_8726" s="T227">pro.h:A</ta>
            <ta e="T230" id="Seg_8727" s="T229">pro:G</ta>
            <ta e="T234" id="Seg_8728" s="T233">np:Time</ta>
            <ta e="T236" id="Seg_8729" s="T235">0.3:Th</ta>
            <ta e="T238" id="Seg_8730" s="T237">np.h:A</ta>
            <ta e="T241" id="Seg_8731" s="T240">np.h:R 0.3.h:Poss</ta>
            <ta e="T243" id="Seg_8732" s="T242">adv:L</ta>
            <ta e="T247" id="Seg_8733" s="T246">np:Th</ta>
            <ta e="T249" id="Seg_8734" s="T248">np:Th 0.1.h:Poss</ta>
            <ta e="T251" id="Seg_8735" s="T250">adv:L</ta>
            <ta e="T252" id="Seg_8736" s="T251">0.1.h:A</ta>
            <ta e="T253" id="Seg_8737" s="T252">np.h:A</ta>
            <ta e="T255" id="Seg_8738" s="T254">np:Th</ta>
            <ta e="T256" id="Seg_8739" s="T255">0.3.h:A</ta>
            <ta e="T257" id="Seg_8740" s="T256">np:Poss</ta>
            <ta e="T258" id="Seg_8741" s="T257">np:L</ta>
            <ta e="T259" id="Seg_8742" s="T258">pp:Time</ta>
            <ta e="T263" id="Seg_8743" s="T262">0.1.h:A</ta>
            <ta e="T265" id="Seg_8744" s="T264">np.h:A</ta>
            <ta e="T268" id="Seg_8745" s="T267">adv:Time</ta>
            <ta e="T270" id="Seg_8746" s="T269">0.3.h:A</ta>
            <ta e="T274" id="Seg_8747" s="T273">np:Th 0.2.h:Poss</ta>
            <ta e="T277" id="Seg_8748" s="T276">pro.h:R</ta>
            <ta e="T278" id="Seg_8749" s="T277">0.2.h:A 0.3:Th</ta>
            <ta e="T282" id="Seg_8750" s="T281">np.h:Poss 0.3.h:Poss</ta>
            <ta e="T284" id="Seg_8751" s="T283">np.h:A 0.3.h:Poss</ta>
            <ta e="T285" id="Seg_8752" s="T284">pro.h:Th</ta>
            <ta e="T289" id="Seg_8753" s="T287">np:G</ta>
            <ta e="T290" id="Seg_8754" s="T289">adv:L</ta>
            <ta e="T291" id="Seg_8755" s="T290">adv:L</ta>
            <ta e="T293" id="Seg_8756" s="T292">np:Th</ta>
            <ta e="T295" id="Seg_8757" s="T294">adv:L</ta>
            <ta e="T296" id="Seg_8758" s="T295">0.1.h:A</ta>
            <ta e="T298" id="Seg_8759" s="T297">np:Th</ta>
            <ta e="T299" id="Seg_8760" s="T298">0.2.h:A</ta>
            <ta e="T301" id="Seg_8761" s="T300">0.3.h:A </ta>
            <ta e="T302" id="Seg_8762" s="T301">adv:Time</ta>
            <ta e="T304" id="Seg_8763" s="T303">adv:L</ta>
            <ta e="T306" id="Seg_8764" s="T305">np:Time</ta>
            <ta e="T309" id="Seg_8765" s="T308">np.h:A</ta>
            <ta e="T310" id="Seg_8766" s="T309">pro.h:G</ta>
            <ta e="T312" id="Seg_8767" s="T311">np.h:Poss</ta>
            <ta e="T313" id="Seg_8768" s="T312">np:Th</ta>
            <ta e="T314" id="Seg_8769" s="T313">0.3.h:A</ta>
            <ta e="T315" id="Seg_8770" s="T314">np.h:Poss</ta>
            <ta e="T316" id="Seg_8771" s="T315">np:Th</ta>
            <ta e="T317" id="Seg_8772" s="T316">adv:Time</ta>
            <ta e="T319" id="Seg_8773" s="T318">0.3.h:A</ta>
            <ta e="T321" id="Seg_8774" s="T320">np:Th</ta>
            <ta e="T323" id="Seg_8775" s="T322">0.3.h:A</ta>
            <ta e="T325" id="Seg_8776" s="T324">pro.h:A</ta>
            <ta e="T327" id="Seg_8777" s="T326">0.3:Th</ta>
            <ta e="T328" id="Seg_8778" s="T327">np:Th</ta>
            <ta e="T330" id="Seg_8779" s="T329">0.3.h:A</ta>
            <ta e="T332" id="Seg_8780" s="T331">np:Poss</ta>
            <ta e="T333" id="Seg_8781" s="T332">np:Poss</ta>
            <ta e="T335" id="Seg_8782" s="T334">np:L</ta>
            <ta e="T336" id="Seg_8783" s="T335">adv:Time</ta>
            <ta e="T337" id="Seg_8784" s="T336">0.3.h:A</ta>
            <ta e="T339" id="Seg_8785" s="T338">np:Com</ta>
            <ta e="T342" id="Seg_8786" s="T341">np.h:Poss</ta>
            <ta e="T344" id="Seg_8787" s="T343">np:Th</ta>
            <ta e="T347" id="Seg_8788" s="T346">np:Th 0.3.h:Poss</ta>
            <ta e="T349" id="Seg_8789" s="T348">np:Th 0.3.h:Poss</ta>
            <ta e="T350" id="Seg_8790" s="T349">adv:Time</ta>
            <ta e="T352" id="Seg_8791" s="T351">0.3.h:Th</ta>
            <ta e="T354" id="Seg_8792" s="T353">np:Com</ta>
            <ta e="T356" id="Seg_8793" s="T355">adv:Time</ta>
            <ta e="T358" id="Seg_8794" s="T357">0.3:Th</ta>
            <ta e="T360" id="Seg_8795" s="T359">np.h:R</ta>
            <ta e="T361" id="Seg_8796" s="T360">pro.h:A</ta>
            <ta e="T363" id="Seg_8797" s="T362">adv:G</ta>
            <ta e="T364" id="Seg_8798" s="T363">np:G 0.1.h:Poss</ta>
            <ta e="T368" id="Seg_8799" s="T367">0.3.h:A</ta>
            <ta e="T370" id="Seg_8800" s="T369">0.3.h:A</ta>
            <ta e="T371" id="Seg_8801" s="T370">adv:G</ta>
            <ta e="T372" id="Seg_8802" s="T371">0.3.h:A</ta>
            <ta e="T374" id="Seg_8803" s="T373">np:Th</ta>
            <ta e="T375" id="Seg_8804" s="T374">0.3.h:A</ta>
            <ta e="T378" id="Seg_8805" s="T377">np:Th</ta>
            <ta e="T379" id="Seg_8806" s="T378">0.3.h:A</ta>
            <ta e="T380" id="Seg_8807" s="T379">np.h:A</ta>
            <ta e="T381" id="Seg_8808" s="T380">adv:G</ta>
            <ta e="T383" id="Seg_8809" s="T382">np.h:Th 0.3.h:Poss</ta>
            <ta e="T386" id="Seg_8810" s="T385">adv:L</ta>
            <ta e="T387" id="Seg_8811" s="T386">0.3.h:Th</ta>
            <ta e="T389" id="Seg_8812" s="T388">0.3.h:Th</ta>
            <ta e="T392" id="Seg_8813" s="T391">np:Time</ta>
            <ta e="T393" id="Seg_8814" s="T392">np:Th</ta>
            <ta e="T394" id="Seg_8815" s="T393">np:G</ta>
            <ta e="T398" id="Seg_8816" s="T397">np.h:E</ta>
            <ta e="T401" id="Seg_8817" s="T400">adv:L</ta>
            <ta e="T404" id="Seg_8818" s="T403">np:Time</ta>
            <ta e="T405" id="Seg_8819" s="T404">np.h:A</ta>
            <ta e="T406" id="Seg_8820" s="T405">pro:Th</ta>
            <ta e="T408" id="Seg_8821" s="T407">adv:So</ta>
            <ta e="T410" id="Seg_8822" s="T409">np.h:A</ta>
            <ta e="T413" id="Seg_8823" s="T412">0.3.h:Poss np.h:A</ta>
            <ta e="T415" id="Seg_8824" s="T414">np:Th</ta>
            <ta e="T418" id="Seg_8825" s="T417">np.h:Poss</ta>
            <ta e="T420" id="Seg_8826" s="T419">np:Th</ta>
            <ta e="T422" id="Seg_8827" s="T421">adv:Time</ta>
            <ta e="T425" id="Seg_8828" s="T424">np.h:Th</ta>
            <ta e="T427" id="Seg_8829" s="T426">np.h:E 0.3.h:Poss</ta>
            <ta e="T428" id="Seg_8830" s="T427">0.3.h:Th</ta>
            <ta e="T431" id="Seg_8831" s="T430">0.3.h:A</ta>
            <ta e="T433" id="Seg_8832" s="T432">np.h:A</ta>
            <ta e="T438" id="Seg_8833" s="T437">np:Th 0.3.h:Poss</ta>
            <ta e="T440" id="Seg_8834" s="T439">0.3.h:A</ta>
            <ta e="T441" id="Seg_8835" s="T440">np:L</ta>
            <ta e="T442" id="Seg_8836" s="T441">np:L</ta>
            <ta e="T443" id="Seg_8837" s="T442">np:G</ta>
            <ta e="T444" id="Seg_8838" s="T443">0.3.h:A</ta>
            <ta e="T445" id="Seg_8839" s="T444">np:Poss</ta>
            <ta e="T446" id="Seg_8840" s="T445">adv:L</ta>
            <ta e="T447" id="Seg_8841" s="T446">0.3.h:Th</ta>
            <ta e="T448" id="Seg_8842" s="T447">np:Poss</ta>
            <ta e="T451" id="Seg_8843" s="T450">np:Th 0.3.h:Poss</ta>
            <ta e="T452" id="Seg_8844" s="T451">0.3.h:A</ta>
            <ta e="T453" id="Seg_8845" s="T452">np:Th 0.3.h:Poss</ta>
            <ta e="T455" id="Seg_8846" s="T454">0.3.h:A</ta>
            <ta e="T457" id="Seg_8847" s="T456">0.3.h:A</ta>
            <ta e="T459" id="Seg_8848" s="T458">np.h:R</ta>
            <ta e="T460" id="Seg_8849" s="T459">adv:G</ta>
            <ta e="T461" id="Seg_8850" s="T460">0.3.h:Th</ta>
            <ta e="T462" id="Seg_8851" s="T461">np:Th 0.2.h:Poss</ta>
            <ta e="T465" id="Seg_8852" s="T464">np:Poss</ta>
            <ta e="T468" id="Seg_8853" s="T467">adv:Time</ta>
            <ta e="T471" id="Seg_8854" s="T470">0.3.h:A</ta>
            <ta e="T473" id="Seg_8855" s="T472">np:Ins</ta>
            <ta e="T474" id="Seg_8856" s="T473">0.3.h:A 0.3.h:P</ta>
            <ta e="T476" id="Seg_8857" s="T475">np.h:Poss</ta>
            <ta e="T477" id="Seg_8858" s="T476">np.h:P</ta>
            <ta e="T480" id="Seg_8859" s="T479">0.3.h:A</ta>
            <ta e="T481" id="Seg_8860" s="T480">pro.h:P</ta>
            <ta e="T484" id="Seg_8861" s="T483">np.h:Th</ta>
            <ta e="T487" id="Seg_8862" s="T486">pro.h:Th</ta>
            <ta e="T490" id="Seg_8863" s="T489">adv:G</ta>
            <ta e="T491" id="Seg_8864" s="T490">0.3.h:A 0.3.h:Th</ta>
            <ta e="T493" id="Seg_8865" s="T492">np.h:A</ta>
            <ta e="T495" id="Seg_8866" s="T494">np:Time</ta>
            <ta e="T496" id="Seg_8867" s="T495">np:Ins</ta>
            <ta e="T497" id="Seg_8868" s="T496">0.3.h:P</ta>
            <ta e="T498" id="Seg_8869" s="T497">pro.h:A</ta>
            <ta e="T499" id="Seg_8870" s="T498">pp:G 0.3.h:Poss</ta>
            <ta e="T501" id="Seg_8871" s="T500">pp:G 0.3.h:Poss</ta>
            <ta e="T505" id="Seg_8872" s="T504">np.h:Th</ta>
            <ta e="T506" id="Seg_8873" s="T505">adv:Time</ta>
            <ta e="T508" id="Seg_8874" s="T507">adv:G</ta>
            <ta e="T509" id="Seg_8875" s="T508">0.3.h:A</ta>
            <ta e="T510" id="Seg_8876" s="T509">np:G</ta>
            <ta e="T512" id="Seg_8877" s="T511">np:Th</ta>
            <ta e="T513" id="Seg_8878" s="T512">np:Th</ta>
            <ta e="T514" id="Seg_8879" s="T513">np:Th 0.3.h:Poss</ta>
            <ta e="T516" id="Seg_8880" s="T515">0.3.h:A</ta>
            <ta e="T518" id="Seg_8881" s="T517">np.h:Th</ta>
            <ta e="T520" id="Seg_8882" s="T519">adv:G</ta>
            <ta e="T521" id="Seg_8883" s="T520">0.3.h:A</ta>
            <ta e="T522" id="Seg_8884" s="T521">0.3.h:A</ta>
            <ta e="T524" id="Seg_8885" s="T523">pro.h:Th</ta>
            <ta e="T525" id="Seg_8886" s="T524">adv:G</ta>
            <ta e="T526" id="Seg_8887" s="T525">0.3.h:A</ta>
            <ta e="T528" id="Seg_8888" s="T527">0.3.h:A</ta>
            <ta e="T530" id="Seg_8889" s="T529">np:Th 0.3.h:Poss</ta>
            <ta e="T532" id="Seg_8890" s="T531">0.3.h:A</ta>
            <ta e="T533" id="Seg_8891" s="T532">0.3.h:A</ta>
            <ta e="T535" id="Seg_8892" s="T534">0.3.h:A</ta>
            <ta e="T536" id="Seg_8893" s="T535">0.3.h:Th</ta>
            <ta e="T538" id="Seg_8894" s="T537">np.h:Th</ta>
            <ta e="T539" id="Seg_8895" s="T538">np:L</ta>
            <ta e="T543" id="Seg_8896" s="T542">0.3.h:Th</ta>
            <ta e="T545" id="Seg_8897" s="T544">0.3.h:Th</ta>
            <ta e="T547" id="Seg_8898" s="T546">adv:Time</ta>
            <ta e="T549" id="Seg_8899" s="T548">0.3.h:A</ta>
            <ta e="T555" id="Seg_8900" s="T554">np:P 0.3.h:Poss</ta>
            <ta e="T559" id="Seg_8901" s="T558">np:Th 0.3.h:Poss</ta>
            <ta e="T562" id="Seg_8902" s="T561">np.h:Th</ta>
            <ta e="T569" id="Seg_8903" s="T568">np.h:A</ta>
            <ta e="T572" id="Seg_8904" s="T571">np.h:R</ta>
            <ta e="T573" id="Seg_8905" s="T572">adv:G</ta>
            <ta e="T574" id="Seg_8906" s="T573">0.3.h:A</ta>
            <ta e="T576" id="Seg_8907" s="T575">pro.h:Poss</ta>
            <ta e="T577" id="Seg_8908" s="T576">np:Ins</ta>
            <ta e="T578" id="Seg_8909" s="T577">adv:Time</ta>
            <ta e="T581" id="Seg_8910" s="T580">pro.h:A</ta>
            <ta e="T582" id="Seg_8911" s="T581">np:G</ta>
            <ta e="T585" id="Seg_8912" s="T584">0.3.h:A</ta>
            <ta e="T588" id="Seg_8913" s="T587">adv:Time</ta>
            <ta e="T591" id="Seg_8914" s="T590">np:Th 0.3.h:Poss</ta>
            <ta e="T592" id="Seg_8915" s="T591">adv:Time</ta>
            <ta e="T593" id="Seg_8916" s="T592">adv:G</ta>
            <ta e="T596" id="Seg_8917" s="T595">np.h:Th</ta>
            <ta e="T598" id="Seg_8918" s="T597">np.h:Th</ta>
            <ta e="T600" id="Seg_8919" s="T599">0.3.h:A</ta>
            <ta e="T601" id="Seg_8920" s="T600">adv:G</ta>
            <ta e="T602" id="Seg_8921" s="T601">adv:G</ta>
            <ta e="T604" id="Seg_8922" s="T603">np.h:Th</ta>
            <ta e="T607" id="Seg_8923" s="T606">0.3.h:A</ta>
            <ta e="T611" id="Seg_8924" s="T610">0.3.h:A 0.3.h:Th</ta>
            <ta e="T612" id="Seg_8925" s="T611">0.3.h:A</ta>
            <ta e="T614" id="Seg_8926" s="T613">np.h:A</ta>
            <ta e="T617" id="Seg_8927" s="T616">np.h:R</ta>
            <ta e="T619" id="Seg_8928" s="T618">0.3.h:A</ta>
            <ta e="T620" id="Seg_8929" s="T619">np:P 0.2.h:Poss</ta>
            <ta e="T622" id="Seg_8930" s="T621">0.2.h:A</ta>
            <ta e="T623" id="Seg_8931" s="T622">pro.h:A</ta>
            <ta e="T624" id="Seg_8932" s="T623">adv:G</ta>
            <ta e="T626" id="Seg_8933" s="T625">np.h:G</ta>
            <ta e="T628" id="Seg_8934" s="T627">0.3.h:E</ta>
            <ta e="T629" id="Seg_8935" s="T628">0.3.h:Poss</ta>
            <ta e="T630" id="Seg_8936" s="T629">np:Th</ta>
            <ta e="T633" id="Seg_8937" s="T632">adv:G</ta>
            <ta e="T634" id="Seg_8938" s="T633">0.3.h:A</ta>
            <ta e="T636" id="Seg_8939" s="T635">pro.h:A</ta>
            <ta e="T640" id="Seg_8940" s="T639">np.h:A 0.3.h:Poss</ta>
            <ta e="T642" id="Seg_8941" s="T641">0.3:Th</ta>
            <ta e="T643" id="Seg_8942" s="T642">0.3.h:Poss</ta>
            <ta e="T644" id="Seg_8943" s="T643">0.3.h:Poss</ta>
            <ta e="T645" id="Seg_8944" s="T644">np.h:R</ta>
            <ta e="T647" id="Seg_8945" s="T646">np.h:A</ta>
            <ta e="T648" id="Seg_8946" s="T647">adv:G</ta>
            <ta e="T651" id="Seg_8947" s="T650">adv:Time</ta>
            <ta e="T656" id="Seg_8948" s="T655">pro:Th</ta>
            <ta e="T658" id="Seg_8949" s="T657">0.3.h:E</ta>
            <ta e="T660" id="Seg_8950" s="T659">0.3.h:A</ta>
            <ta e="T661" id="Seg_8951" s="T660">pro.h:A</ta>
            <ta e="T662" id="Seg_8952" s="T661">np.h:Poss 0.3.h:Poss</ta>
            <ta e="T663" id="Seg_8953" s="T662">np:G</ta>
            <ta e="T666" id="Seg_8954" s="T665">np:Th</ta>
            <ta e="T669" id="Seg_8955" s="T668">0.3.h:A</ta>
            <ta e="T671" id="Seg_8956" s="T670">0.3.h:A</ta>
            <ta e="T672" id="Seg_8957" s="T671">adv:G</ta>
            <ta e="T674" id="Seg_8958" s="T673">0.3.h:A</ta>
            <ta e="T676" id="Seg_8959" s="T675">np.h:A</ta>
            <ta e="T679" id="Seg_8960" s="T678">np:G 0.3.h:Poss</ta>
            <ta e="T682" id="Seg_8961" s="T681">0.3.h:P</ta>
            <ta e="T685" id="Seg_8962" s="T684">np.h:Th</ta>
            <ta e="T689" id="Seg_8963" s="T688">np:Th 0.3.h:Poss</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_8964" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_8965" s="T2">cop</ta>
            <ta e="T4" id="Seg_8966" s="T3">adj:pred</ta>
            <ta e="T5" id="Seg_8967" s="T4">adj:pred</ta>
            <ta e="T6" id="Seg_8968" s="T5">pro:S</ta>
            <ta e="T7" id="Seg_8969" s="T6">n:pred</ta>
            <ta e="T9" id="Seg_8970" s="T8">n:pred</ta>
            <ta e="T10" id="Seg_8971" s="T9">np.h:S</ta>
            <ta e="T11" id="Seg_8972" s="T10">v:pred</ta>
            <ta e="T12" id="Seg_8973" s="T11">0.3.h:S v:pred</ta>
            <ta e="T15" id="Seg_8974" s="T14">np.h:S</ta>
            <ta e="T19" id="Seg_8975" s="T18">v:pred</ta>
            <ta e="T23" id="Seg_8976" s="T22">0.3.h:S v:pred</ta>
            <ta e="T25" id="Seg_8977" s="T24">0.3.h:S v:pred</ta>
            <ta e="T27" id="Seg_8978" s="T26">np.h:S</ta>
            <ta e="T31" id="Seg_8979" s="T30">np.h:S</ta>
            <ta e="T32" id="Seg_8980" s="T31">v:pred</ta>
            <ta e="T34" id="Seg_8981" s="T33">pro.h:S</ta>
            <ta e="T35" id="Seg_8982" s="T34">v:pred</ta>
            <ta e="T37" id="Seg_8983" s="T36">np.h:S</ta>
            <ta e="T38" id="Seg_8984" s="T37">v:pred</ta>
            <ta e="T42" id="Seg_8985" s="T41">np.h:S</ta>
            <ta e="T45" id="Seg_8986" s="T44">v:pred</ta>
            <ta e="T51" id="Seg_8987" s="T50">np.h:S</ta>
            <ta e="T52" id="Seg_8988" s="T51">v:pred</ta>
            <ta e="T53" id="Seg_8989" s="T693">np.h:S</ta>
            <ta e="T54" id="Seg_8990" s="T53">v:pred</ta>
            <ta e="T56" id="Seg_8991" s="T55">np.h:S</ta>
            <ta e="T58" id="Seg_8992" s="T57">v:pred</ta>
            <ta e="T60" id="Seg_8993" s="T694">np.h:S</ta>
            <ta e="T62" id="Seg_8994" s="T61">v:pred</ta>
            <ta e="T65" id="Seg_8995" s="T64">0.3.h:S v:pred</ta>
            <ta e="T66" id="Seg_8996" s="T65">np.h:S</ta>
            <ta e="T67" id="Seg_8997" s="T66">v:pred</ta>
            <ta e="T69" id="Seg_8998" s="T68">0.2.h:S v:pred 0.3.h:O</ta>
            <ta e="T71" id="Seg_8999" s="T70">np.h:S</ta>
            <ta e="T72" id="Seg_9000" s="T71">s:temp</ta>
            <ta e="T75" id="Seg_9001" s="T74">v:pred</ta>
            <ta e="T78" id="Seg_9002" s="T77">np.h:S</ta>
            <ta e="T81" id="Seg_9003" s="T80">v:pred</ta>
            <ta e="T83" id="Seg_9004" s="T82">0.3.h:S v:pred</ta>
            <ta e="T85" id="Seg_9005" s="T695">np.h:S</ta>
            <ta e="T88" id="Seg_9006" s="T87">v:pred 0.3.h:O</ta>
            <ta e="T90" id="Seg_9007" s="T89">np.h:S</ta>
            <ta e="T92" id="Seg_9008" s="T91">np.h:S</ta>
            <ta e="T93" id="Seg_9009" s="T92">v:pred</ta>
            <ta e="T94" id="Seg_9010" s="T93">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T95" id="Seg_9011" s="T94">np:S</ta>
            <ta e="T97" id="Seg_9012" s="T96">v:pred</ta>
            <ta e="T100" id="Seg_9013" s="T99">0.3.h:S v:pred</ta>
            <ta e="T102" id="Seg_9014" s="T101">0.3.h:S v:pred</ta>
            <ta e="T103" id="Seg_9015" s="T102">0.3.h:S v:pred</ta>
            <ta e="T105" id="Seg_9016" s="T103">s:temp</ta>
            <ta e="T109" id="Seg_9017" s="T108">np.h:S</ta>
            <ta e="T110" id="Seg_9018" s="T109">v:pred</ta>
            <ta e="T111" id="Seg_9019" s="T110">adj:pred</ta>
            <ta e="T112" id="Seg_9020" s="T111">np.h:S</ta>
            <ta e="T117" id="Seg_9021" s="T116">np.h:S</ta>
            <ta e="T122" id="Seg_9022" s="T121">np.h:S</ta>
            <ta e="T123" id="Seg_9023" s="T122">v:pred</ta>
            <ta e="T127" id="Seg_9024" s="T692">np.h:S</ta>
            <ta e="T128" id="Seg_9025" s="T127">v:pred</ta>
            <ta e="T131" id="Seg_9026" s="T130">np:O</ta>
            <ta e="T133" id="Seg_9027" s="T132">0.3.h:S v:pred</ta>
            <ta e="T135" id="Seg_9028" s="T134">0.3.h:S v:pred</ta>
            <ta e="T137" id="Seg_9029" s="T136">np.h:S</ta>
            <ta e="T138" id="Seg_9030" s="T137">v:pred</ta>
            <ta e="T145" id="Seg_9031" s="T144">np:S</ta>
            <ta e="T149" id="Seg_9032" s="T146">s:purp</ta>
            <ta e="T150" id="Seg_9033" s="T149">0.3.h:S v:pred</ta>
            <ta e="T152" id="Seg_9034" s="T151">0.3.h:S v:pred</ta>
            <ta e="T155" id="Seg_9035" s="T154">np:S</ta>
            <ta e="T157" id="Seg_9036" s="T156">v:pred</ta>
            <ta e="T159" id="Seg_9037" s="T158">s:temp</ta>
            <ta e="T160" id="Seg_9038" s="T159">0.3.h:S v:pred</ta>
            <ta e="T165" id="Seg_9039" s="T164">np.h:O</ta>
            <ta e="T167" id="Seg_9040" s="T166">0.3.h:S v:pred</ta>
            <ta e="T168" id="Seg_9041" s="T167">np.h:S</ta>
            <ta e="T171" id="Seg_9042" s="T170">v:pred</ta>
            <ta e="T172" id="Seg_9043" s="T171">pro.h:S</ta>
            <ta e="T175" id="Seg_9044" s="T174">v:pred</ta>
            <ta e="T176" id="Seg_9045" s="T175">np.h:S</ta>
            <ta e="T178" id="Seg_9046" s="T177">v:pred</ta>
            <ta e="T182" id="Seg_9047" s="T181">0.3.h:S v:pred</ta>
            <ta e="T189" id="Seg_9048" s="T188">np.h:S</ta>
            <ta e="T192" id="Seg_9049" s="T191">v:pred</ta>
            <ta e="T195" id="Seg_9050" s="T192">s:purp</ta>
            <ta e="T200" id="Seg_9051" s="T197">s:purp</ta>
            <ta e="T203" id="Seg_9052" s="T202">np:S</ta>
            <ta e="T207" id="Seg_9053" s="T206">pro:S</ta>
            <ta e="T208" id="Seg_9054" s="T207">pro:O</ta>
            <ta e="T209" id="Seg_9055" s="T208">0.3.h:S v:pred</ta>
            <ta e="T212" id="Seg_9056" s="T211">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T214" id="Seg_9057" s="T213">np:O</ta>
            <ta e="T215" id="Seg_9058" s="T214">s:temp</ta>
            <ta e="T216" id="Seg_9059" s="T215">0.3.h:S v:pred</ta>
            <ta e="T221" id="Seg_9060" s="T220">v:pred</ta>
            <ta e="T223" id="Seg_9061" s="T222">np.h:S</ta>
            <ta e="T227" id="Seg_9062" s="T226">0.1.h:S v:pred</ta>
            <ta e="T228" id="Seg_9063" s="T227">pro.h:S</ta>
            <ta e="T232" id="Seg_9064" s="T231">v:pred</ta>
            <ta e="T236" id="Seg_9065" s="T235">0.3:S v:pred</ta>
            <ta e="T238" id="Seg_9066" s="T237">np.h:S</ta>
            <ta e="T240" id="Seg_9067" s="T239">v:pred</ta>
            <ta e="T248" id="Seg_9068" s="T244">s:rel</ta>
            <ta e="T249" id="Seg_9069" s="T248">np:S</ta>
            <ta e="T250" id="Seg_9070" s="T249">v:pred</ta>
            <ta e="T252" id="Seg_9071" s="T251">0.1.h:S v:pred</ta>
            <ta e="T253" id="Seg_9072" s="T252">np.h:S</ta>
            <ta e="T254" id="Seg_9073" s="T253">v:pred</ta>
            <ta e="T255" id="Seg_9074" s="T254">np:O</ta>
            <ta e="T256" id="Seg_9075" s="T255">0.3.h:S v:pred</ta>
            <ta e="T261" id="Seg_9076" s="T256">s:temp</ta>
            <ta e="T263" id="Seg_9077" s="T262">0.1.h:S v:pred</ta>
            <ta e="T265" id="Seg_9078" s="T264">np.h:S</ta>
            <ta e="T267" id="Seg_9079" s="T266">v:pred</ta>
            <ta e="T270" id="Seg_9080" s="T269">0.3.h:S v:pred</ta>
            <ta e="T274" id="Seg_9081" s="T273">np:S</ta>
            <ta e="T275" id="Seg_9082" s="T274">v:pred</ta>
            <ta e="T278" id="Seg_9083" s="T277">0.2.h:S v:pred 0.3:O</ta>
            <ta e="T284" id="Seg_9084" s="T283">np.h:S</ta>
            <ta e="T285" id="Seg_9085" s="T284">pro.h:O</ta>
            <ta e="T286" id="Seg_9086" s="T285">v:pred</ta>
            <ta e="T293" id="Seg_9087" s="T292">np:S</ta>
            <ta e="T294" id="Seg_9088" s="T293">v:pred</ta>
            <ta e="T296" id="Seg_9089" s="T295">0.1.h:S v:pred</ta>
            <ta e="T298" id="Seg_9090" s="T297">np:O</ta>
            <ta e="T299" id="Seg_9091" s="T298">0.2.h:S v:pred</ta>
            <ta e="T301" id="Seg_9092" s="T300">0.3.h:S v:pred</ta>
            <ta e="T307" id="Seg_9093" s="T306">v:pred</ta>
            <ta e="T309" id="Seg_9094" s="T308">np.h:S</ta>
            <ta e="T314" id="Seg_9095" s="T310">s:purp</ta>
            <ta e="T316" id="Seg_9096" s="T315">np:O</ta>
            <ta e="T319" id="Seg_9097" s="T318">0.3.h:S v:pred</ta>
            <ta e="T321" id="Seg_9098" s="T320">np:O</ta>
            <ta e="T323" id="Seg_9099" s="T322">0.3.h:S v:pred</ta>
            <ta e="T325" id="Seg_9100" s="T324">pro.h:S</ta>
            <ta e="T327" id="Seg_9101" s="T326">v:pred 0.3:O</ta>
            <ta e="T330" id="Seg_9102" s="T329">0.3.h:S v:pred</ta>
            <ta e="T337" id="Seg_9103" s="T336">0.3.h:S v:pred</ta>
            <ta e="T344" id="Seg_9104" s="T343">np:S</ta>
            <ta e="T345" id="Seg_9105" s="T344">v:pred</ta>
            <ta e="T347" id="Seg_9106" s="T346">np:S</ta>
            <ta e="T349" id="Seg_9107" s="T348">np:S</ta>
            <ta e="T352" id="Seg_9108" s="T351">0.3.h:S v:pred</ta>
            <ta e="T358" id="Seg_9109" s="T357">0.3:S v:pred</ta>
            <ta e="T361" id="Seg_9110" s="T360">pro.h:S</ta>
            <ta e="T365" id="Seg_9111" s="T364">v:pred</ta>
            <ta e="T368" id="Seg_9112" s="T367">0.3.h:S v:pred</ta>
            <ta e="T370" id="Seg_9113" s="T369">0.3.h:S v:pred</ta>
            <ta e="T372" id="Seg_9114" s="T371">0.3.h:S v:pred</ta>
            <ta e="T375" id="Seg_9115" s="T372">s:temp</ta>
            <ta e="T378" id="Seg_9116" s="T377">np:O</ta>
            <ta e="T379" id="Seg_9117" s="T378">0.3.h:S v:pred</ta>
            <ta e="T380" id="Seg_9118" s="T379">np.h:S</ta>
            <ta e="T382" id="Seg_9119" s="T381">v:pred</ta>
            <ta e="T383" id="Seg_9120" s="T382">np.h:S</ta>
            <ta e="T385" id="Seg_9121" s="T384">v:pred</ta>
            <ta e="T387" id="Seg_9122" s="T386">0.3.h:S v:pred</ta>
            <ta e="T389" id="Seg_9123" s="T388">0.3.h:S v:pred</ta>
            <ta e="T393" id="Seg_9124" s="T392">np:S</ta>
            <ta e="T396" id="Seg_9125" s="T395">v:pred</ta>
            <ta e="T398" id="Seg_9126" s="T397">np.h:S</ta>
            <ta e="T400" id="Seg_9127" s="T399">v:pred</ta>
            <ta e="T405" id="Seg_9128" s="T404">np.h:S</ta>
            <ta e="T406" id="Seg_9129" s="T405">pro:O</ta>
            <ta e="T407" id="Seg_9130" s="T406">v:pred</ta>
            <ta e="T410" id="Seg_9131" s="T409">np.h:S</ta>
            <ta e="T411" id="Seg_9132" s="T410">v:pred</ta>
            <ta e="T413" id="Seg_9133" s="T412">np.h:S</ta>
            <ta e="T416" id="Seg_9134" s="T413">s:rel</ta>
            <ta e="T418" id="Seg_9135" s="T417">np.h:S</ta>
            <ta e="T420" id="Seg_9136" s="T419">n:pred</ta>
            <ta e="T421" id="Seg_9137" s="T420">cop</ta>
            <ta e="T425" id="Seg_9138" s="T424">np.h:S</ta>
            <ta e="T426" id="Seg_9139" s="T425">v:pred</ta>
            <ta e="T427" id="Seg_9140" s="T426">np.h:S</ta>
            <ta e="T428" id="Seg_9141" s="T427">v:pred 0.3.h:O</ta>
            <ta e="T431" id="Seg_9142" s="T430">0.3.h:S v:pred</ta>
            <ta e="T433" id="Seg_9143" s="T432">np.h:S</ta>
            <ta e="T436" id="Seg_9144" s="T435">v:pred</ta>
            <ta e="T438" id="Seg_9145" s="T437">np:O</ta>
            <ta e="T440" id="Seg_9146" s="T439">0.3.h:S v:pred</ta>
            <ta e="T444" id="Seg_9147" s="T443">0.3.h:S v:pred</ta>
            <ta e="T447" id="Seg_9148" s="T444">s:adv</ta>
            <ta e="T451" id="Seg_9149" s="T450">np:O</ta>
            <ta e="T452" id="Seg_9150" s="T451">0.3.h:S v:pred</ta>
            <ta e="T453" id="Seg_9151" s="T452">np:O</ta>
            <ta e="T455" id="Seg_9152" s="T454">0.3.h:S v:pred</ta>
            <ta e="T457" id="Seg_9153" s="T456">0.3.h:S v:pred</ta>
            <ta e="T461" id="Seg_9154" s="T459">s:temp</ta>
            <ta e="T462" id="Seg_9155" s="T461">np:S</ta>
            <ta e="T471" id="Seg_9156" s="T470">0.3.h:S v:pred</ta>
            <ta e="T474" id="Seg_9157" s="T473">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T477" id="Seg_9158" s="T476">np.h:O</ta>
            <ta e="T480" id="Seg_9159" s="T479">0.3.h:S v:pred</ta>
            <ta e="T481" id="Seg_9160" s="T480">pro.h:S</ta>
            <ta e="T482" id="Seg_9161" s="T481">v:pred</ta>
            <ta e="T484" id="Seg_9162" s="T483">np.h:S</ta>
            <ta e="T486" id="Seg_9163" s="T485">v:pred</ta>
            <ta e="T487" id="Seg_9164" s="T486">pro.h:S</ta>
            <ta e="T489" id="Seg_9165" s="T488">v:pred</ta>
            <ta e="T491" id="Seg_9166" s="T490">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T493" id="Seg_9167" s="T492">np.h:S</ta>
            <ta e="T497" id="Seg_9168" s="T496">v:pred 0.3.h:O</ta>
            <ta e="T498" id="Seg_9169" s="T497">pro.h:S</ta>
            <ta e="T503" id="Seg_9170" s="T502">v:pred</ta>
            <ta e="T505" id="Seg_9171" s="T504">np.h:O</ta>
            <ta e="T509" id="Seg_9172" s="T508">0.3.h:S v:pred</ta>
            <ta e="T512" id="Seg_9173" s="T511">np:O</ta>
            <ta e="T513" id="Seg_9174" s="T512">np:O</ta>
            <ta e="T514" id="Seg_9175" s="T513">np:O</ta>
            <ta e="T516" id="Seg_9176" s="T515">0.3.h:S v:pred</ta>
            <ta e="T518" id="Seg_9177" s="T517">np.h:O</ta>
            <ta e="T521" id="Seg_9178" s="T520">0.3.h:S v:pred</ta>
            <ta e="T522" id="Seg_9179" s="T521">0.3.h:S v:pred</ta>
            <ta e="T524" id="Seg_9180" s="T523">pro.h:O</ta>
            <ta e="T526" id="Seg_9181" s="T525">0.3.h:S v:pred</ta>
            <ta e="T528" id="Seg_9182" s="T527">0.3.h:S v:pred</ta>
            <ta e="T532" id="Seg_9183" s="T528">s:temp</ta>
            <ta e="T533" id="Seg_9184" s="T532">s:purp</ta>
            <ta e="T535" id="Seg_9185" s="T534">0.3.h:S v:pred</ta>
            <ta e="T536" id="Seg_9186" s="T535">0.3.h:S v:pred</ta>
            <ta e="T538" id="Seg_9187" s="T537">np.h:S</ta>
            <ta e="T541" id="Seg_9188" s="T540">v:pred</ta>
            <ta e="T543" id="Seg_9189" s="T542">0.3.h:S v:pred</ta>
            <ta e="T545" id="Seg_9190" s="T544">0.3.h:S v:pred</ta>
            <ta e="T549" id="Seg_9191" s="T548">0.3.h:S v:pred</ta>
            <ta e="T555" id="Seg_9192" s="T554">np:S</ta>
            <ta e="T557" id="Seg_9193" s="T556">v:pred</ta>
            <ta e="T559" id="Seg_9194" s="T558">np:S</ta>
            <ta e="T560" id="Seg_9195" s="T559">v:pred</ta>
            <ta e="T562" id="Seg_9196" s="T561">np.h:S</ta>
            <ta e="T564" id="Seg_9197" s="T563">adj:pred</ta>
            <ta e="T565" id="Seg_9198" s="T564">cop</ta>
            <ta e="T567" id="Seg_9199" s="T566">v:pred</ta>
            <ta e="T569" id="Seg_9200" s="T568">np.h:S</ta>
            <ta e="T574" id="Seg_9201" s="T573">0.3.h:S v:pred</ta>
            <ta e="T581" id="Seg_9202" s="T580">pro.h:S</ta>
            <ta e="T583" id="Seg_9203" s="T582">v:pred</ta>
            <ta e="T585" id="Seg_9204" s="T584">0.3.h:S v:pred</ta>
            <ta e="T594" id="Seg_9205" s="T588">s:temp</ta>
            <ta e="T596" id="Seg_9206" s="T595">np.h:O</ta>
            <ta e="T598" id="Seg_9207" s="T597">np.h:O</ta>
            <ta e="T600" id="Seg_9208" s="T599">0.3.h:S v:pred</ta>
            <ta e="T604" id="Seg_9209" s="T603">np.h:O</ta>
            <ta e="T606" id="Seg_9210" s="T605">s:temp</ta>
            <ta e="T607" id="Seg_9211" s="T606">0.3.h:S v:pred</ta>
            <ta e="T611" id="Seg_9212" s="T610">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T612" id="Seg_9213" s="T611">0.3.h:S v:pred</ta>
            <ta e="T614" id="Seg_9214" s="T613">np.h:S</ta>
            <ta e="T616" id="Seg_9215" s="T615">v:pred</ta>
            <ta e="T619" id="Seg_9216" s="T618">0.3.h:S v:pred</ta>
            <ta e="T620" id="Seg_9217" s="T619">np:O</ta>
            <ta e="T622" id="Seg_9218" s="T621">0.2.h:S v:pred</ta>
            <ta e="T623" id="Seg_9219" s="T622">pro.h:S</ta>
            <ta e="T625" id="Seg_9220" s="T624">v:pred</ta>
            <ta e="T628" id="Seg_9221" s="T627">0.3.h:S v:pred</ta>
            <ta e="T630" id="Seg_9222" s="T629">np:O</ta>
            <ta e="T634" id="Seg_9223" s="T633">0.3.h:S v:pred</ta>
            <ta e="T636" id="Seg_9224" s="T635">pro.h:S</ta>
            <ta e="T638" id="Seg_9225" s="T637">v:pred</ta>
            <ta e="T640" id="Seg_9226" s="T639">np.h:S</ta>
            <ta e="T642" id="Seg_9227" s="T641">v:pred 0.3:O</ta>
            <ta e="T647" id="Seg_9228" s="T646">np.h:S</ta>
            <ta e="T649" id="Seg_9229" s="T648">v:pred</ta>
            <ta e="T656" id="Seg_9230" s="T655">pro:O</ta>
            <ta e="T658" id="Seg_9231" s="T657">0.3.h:S v:pred</ta>
            <ta e="T660" id="Seg_9232" s="T659">0.3.h:S v:pred</ta>
            <ta e="T661" id="Seg_9233" s="T660">pro.h:S</ta>
            <ta e="T664" id="Seg_9234" s="T663">v:pred</ta>
            <ta e="T666" id="Seg_9235" s="T665">np:O</ta>
            <ta e="T667" id="Seg_9236" s="T666">s:temp</ta>
            <ta e="T669" id="Seg_9237" s="T668">0.3.h:S v:pred</ta>
            <ta e="T671" id="Seg_9238" s="T670">0.3.h:S v:pred</ta>
            <ta e="T674" id="Seg_9239" s="T673">0.3.h:S v:pred</ta>
            <ta e="T676" id="Seg_9240" s="T675">np.h:S</ta>
            <ta e="T680" id="Seg_9241" s="T679">v:pred</ta>
            <ta e="T682" id="Seg_9242" s="T681">0.3.h:S v:pred</ta>
            <ta e="T685" id="Seg_9243" s="T684">np.h:S</ta>
            <ta e="T686" id="Seg_9244" s="T685">v:pred</ta>
            <ta e="T688" id="Seg_9245" s="T687">n:pred</ta>
            <ta e="T689" id="Seg_9246" s="T688">np:S</ta>
            <ta e="T690" id="Seg_9247" s="T689">cop</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T174" id="Seg_9248" s="T173">RUS:cult</ta>
            <ta e="T279" id="Seg_9249" s="T278">RUS:gram</ta>
            <ta e="T473" id="Seg_9250" s="T472">RUS?</ta>
            <ta e="T496" id="Seg_9251" s="T495">RUS?</ta>
            <ta e="T563" id="Seg_9252" s="T562">RUS:gram</ta>
            <ta e="T650" id="Seg_9253" s="T649">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_9254" s="T0">Один сын у него был, без никого, без друга.</ta>
            <ta e="T9" id="Seg_9255" s="T5">Это быль, не сказка.</ta>
            <ta e="T12" id="Seg_9256" s="T9">Старик жил, заболел.</ta>
            <ta e="T19" id="Seg_9257" s="T12">Этот селькупский старик на этой стороне реки жил.</ta>
            <ta e="T25" id="Seg_9258" s="T19">Там, где деревьев совсем нет, жил тот, который заболел.</ta>
            <ta e="T29" id="Seg_9259" s="T25">Один сын у него, без товарища, без никого.</ta>
            <ta e="T35" id="Seg_9260" s="T29">Ни дочери у него не было, никого у него не было.</ta>
            <ta e="T40" id="Seg_9261" s="T35">Потом отец заболел у этого парня.</ta>
            <ta e="T47" id="Seg_9262" s="T40">Этот парень вроде там живет с отцом, с матерью.</ta>
            <ta e="T52" id="Seg_9263" s="T47">На той [другой] стороне двое ненецких братьев были.</ta>
            <ta e="T54" id="Seg_9264" s="T52">Старик-ненец переправится [на эту сторону].</ta>
            <ta e="T58" id="Seg_9265" s="T54">Тот старик [селькуп] умер.</ta>
            <ta e="T62" id="Seg_9266" s="T58">Тот старик-ненец куда денется.</ta>
            <ta e="T65" id="Seg_9267" s="T62">Туда к брату переехал.</ta>
            <ta e="T69" id="Seg_9268" s="T65">«Старик умер, схороните».</ta>
            <ta e="T75" id="Seg_9269" s="T69">Потом ещё старуха, заболев, тоже умерла.</ta>
            <ta e="T81" id="Seg_9270" s="T75">Сын их один-одинёшенек (/без никого, без друга) остался.</ta>
            <ta e="T83" id="Seg_9271" s="T81">Куда он денется.</ta>
            <ta e="T88" id="Seg_9272" s="T83">Этот старик-ненец второй раз на ту сторону переправил [этого парня].</ta>
            <ta e="T93" id="Seg_9273" s="T88">Большой у него сын, такой у него сын был.</ta>
            <ta e="T97" id="Seg_9274" s="T93">Он [его] перевёз, оленей-то у него нет.</ta>
            <ta e="T100" id="Seg_9275" s="T97">Без оленей жили, пешком [ходили].</ta>
            <ta e="T102" id="Seg_9276" s="T100">Там жили.</ta>
            <ta e="T107" id="Seg_9277" s="T102">Они жили, на ту сторону если переехать, в чуме старика-ненца.</ta>
            <ta e="T110" id="Seg_9278" s="T107">У старика-ненца сына не было.</ta>
            <ta e="T112" id="Seg_9279" s="T110">Бездетная жена у него.</ta>
            <ta e="T118" id="Seg_9280" s="T112">У того среднего брата одна дочь единственная.</ta>
            <ta e="T123" id="Seg_9281" s="T118">Снизу вверх по реке [к ним] два ненца пришли.</ta>
            <ta e="T128" id="Seg_9282" s="T123">На безлесой земле этот старик-ненец вот существует.</ta>
            <ta e="T133" id="Seg_9283" s="T128">Сколько у его рода оленей [было], всех отбирал.</ta>
            <ta e="T135" id="Seg_9284" s="T133">Так пришёл.</ta>
            <ta e="T142" id="Seg_9285" s="T135">Вот братья жили, эти двое братьев-стариков.</ta>
            <ta e="T145" id="Seg_9286" s="T142">У них четыреста оленей.</ta>
            <ta e="T150" id="Seg_9287" s="T145">У этого старика (/стариков) оленей отбирать пришёл.</ta>
            <ta e="T152" id="Seg_9288" s="T150">С людьми пришёл.</ta>
            <ta e="T157" id="Seg_9289" s="T152">Старик Пан, имя тоже вот оно.</ta>
            <ta e="T163" id="Seg_9290" s="T157">Потом приехав, ночевали [Пан и его люди] вот в их чумах.</ta>
            <ta e="T167" id="Seg_9291" s="T163">Этого парня не знают.</ta>
            <ta e="T171" id="Seg_9292" s="T167">«Парень как к нему попал?</ta>
            <ta e="T175" id="Seg_9293" s="T171">Он мудрецом что ли живет».</ta>
            <ta e="T181" id="Seg_9294" s="T175">Человек как будет знать, в другом месте живущий человек [об этом парне].</ta>
            <ta e="T182" id="Seg_9295" s="T181">Они ночевали.</ta>
            <ta e="T192" id="Seg_9296" s="T182">Мол, на третий день Пан-старик с десятью людьми придет.</ta>
            <ta e="T195" id="Seg_9297" s="T192">Ну, чтобы оленей прогнать.</ta>
            <ta e="T200" id="Seg_9298" s="T195">Это тот, отца с сыном [?] оленей прогнать.</ta>
            <ta e="T209" id="Seg_9299" s="T200">Сколько домов новых [?], сколько чего есть, всё соберет.</ta>
            <ta e="T212" id="Seg_9300" s="T209">Раньше он тоже собирал.</ta>
            <ta e="T216" id="Seg_9301" s="T212">У ненцев оленей, собирая, отбирал.</ta>
            <ta e="T223" id="Seg_9302" s="T216">Потом обратно уехали эти ненцы [Пан и его люди].</ta>
            <ta e="T227" id="Seg_9303" s="T223">На третий день, мол, мы придем.</ta>
            <ta e="T232" id="Seg_9304" s="T227">Вы, мол, никуда не уходите.</ta>
            <ta e="T236" id="Seg_9305" s="T232">На второй день рассвело.</ta>
            <ta e="T241" id="Seg_9306" s="T236">Этот парень так сказал дедушке.</ta>
            <ta e="T252" id="Seg_9307" s="T241">Мол, вверх по течению есть тундра, где четыреста оленей стояло, туда их загоним.</ta>
            <ta e="T256" id="Seg_9308" s="T252">Ненцы [ведь] придут, оленей угонять начнут.</ta>
            <ta e="T263" id="Seg_9309" s="T256">На оленьем пастбище перезимовав, что мы [иначе] будем делать.</ta>
            <ta e="T267" id="Seg_9310" s="T263">Этот парень так сказал.</ta>
            <ta e="T270" id="Seg_9311" s="T267">Потом так сказал.</ta>
            <ta e="T278" id="Seg_9312" s="T270">Мне, мол,… новый чум у вас есть, дедушка, мне отдай.</ta>
            <ta e="T286" id="Seg_9313" s="T278">А тот(?), того брата дочь, дедушка её(?) отправляет.</ta>
            <ta e="T289" id="Seg_9314" s="T286">Этому парню…</ta>
            <ta e="T294" id="Seg_9315" s="T289">Вверх по течению здесь есть перешеек с лесом.</ta>
            <ta e="T296" id="Seg_9316" s="T294">Там чум поставлю.</ta>
            <ta e="T300" id="Seg_9317" s="T296">Мол, новый чум принесите мне.</ta>
            <ta e="T302" id="Seg_9318" s="T300">На другой день распустили [оленей?].</ta>
            <ta e="T314" id="Seg_9319" s="T302">Вроде вот на третий день придут Пана-старика [люди] к этим, у этого ненца старика оленей отбирать.</ta>
            <ta e="T319" id="Seg_9320" s="T314">Оленей братьев тогда в разные стороны распускали.</ta>
            <ta e="T327" id="Seg_9321" s="T319">Этих оленей погнали вроде, он тоже гонит.</ta>
            <ta e="T330" id="Seg_9322" s="T327">Чум [ему] отдали.</ta>
            <ta e="T339" id="Seg_9323" s="T330">На переднем краю этого лесного перешейка потом чум поставили с этой женщиной.</ta>
            <ta e="T349" id="Seg_9324" s="T339">Конечно, у богатого ненца что, чума не будет [разве], новый чум, с [новой] шкурой чум.</ta>
            <ta e="T354" id="Seg_9325" s="T349">Потом опять ночевали с этой женщиной.</ta>
            <ta e="T358" id="Seg_9326" s="T354">На следующее утро так стало.</ta>
            <ta e="T365" id="Seg_9327" s="T358">Той жене [говорит], я, мол, вверх по реке к людям своим схожу.</ta>
            <ta e="T370" id="Seg_9328" s="T365">Что, мол, чум ли сделали или куда делись. [?]</ta>
            <ta e="T372" id="Seg_9329" s="T370">Вверх по реке двинулся.</ta>
            <ta e="T379" id="Seg_9330" s="T372">Трех быков запряг, вроде тридцать оленей оставлял.</ta>
            <ta e="T382" id="Seg_9331" s="T379">Парень вверх по реке уехал.</ta>
            <ta e="T385" id="Seg_9332" s="T382">Жена его одна осталась.</ta>
            <ta e="T389" id="Seg_9333" s="T385">Вот сидит, сидит.</ta>
            <ta e="T396" id="Seg_9334" s="T389">Однажды день к вечеру вот пошёл [?].</ta>
            <ta e="T407" id="Seg_9335" s="T396">Этот парень понемногу почувствовал: выше по течению [однажды] люди приехали.</ta>
            <ta e="T411" id="Seg_9336" s="T407">Снизу по течению Пана-старика [люди] приехали. </ta>
            <ta e="T416" id="Seg_9337" s="T411">Десять человек у него вроде, которые оленей загонять должны.</ta>
            <ta e="T421" id="Seg_9338" s="T416">Пан-старик такой с шаманской мудростью(?).</ta>
            <ta e="T426" id="Seg_9339" s="T421">Потом второй раз этот парень приехал, слышно.</ta>
            <ta e="T428" id="Seg_9340" s="T426">Жена его услышала.</ta>
            <ta e="T436" id="Seg_9341" s="T428">Видно, только он приехал, Пана-старика люди приехали, что ли.</ta>
            <ta e="T442" id="Seg_9342" s="T436">Нарты со всеми вещами так поставили, снаружи, на улице.</ta>
            <ta e="T444" id="Seg_9343" s="T442">В дом зашёл.</ta>
            <ta e="T452" id="Seg_9344" s="T444">У двери стоя, у двери ноги отряхивает.</ta>
            <ta e="T455" id="Seg_9345" s="T452">Бокари свои отряхивает.</ta>
            <ta e="T461" id="Seg_9346" s="T455">Так сказал Пану старику, вперед повернувшись:</ta>
            <ta e="T467" id="Seg_9347" s="T461">Рот твой как будто прорубь речки.</ta>
            <ta e="T474" id="Seg_9348" s="T467">Потом (/на другой день) вперед прыгнул, кулаком провёл(?).</ta>
            <ta e="T480" id="Seg_9349" s="T474">Людей Пан-старика всех плоско [на землю] уложил.</ta>
            <ta e="T486" id="Seg_9350" s="T480">Некоторые умерли, Пан-старик живой остался.</ta>
            <ta e="T489" id="Seg_9351" s="T486">Двое [ещё] в живых остались.</ta>
            <ta e="T491" id="Seg_9352" s="T489">На улицу [их] побросал.</ta>
            <ta e="T497" id="Seg_9353" s="T491">Этот парень один раз кулаком провёл.</ta>
            <ta e="T503" id="Seg_9354" s="T497">Все в одежду (/в штаны) наложили.</ta>
            <ta e="T509" id="Seg_9355" s="T503">Этих мёртвых потом он наружу побросал.</ta>
            <ta e="T516" id="Seg_9356" s="T509">На улицу, [где?] те санки, палки, лыжи привязанные.</ta>
            <ta e="T522" id="Seg_9357" s="T516">Пана-старика тоже на улицу взял (/бросил).</ta>
            <ta e="T526" id="Seg_9358" s="T522">Ну, всех на улицу взял.</ta>
            <ta e="T532" id="Seg_9359" s="T526">Сел, малицу сняв.</ta>
            <ta e="T536" id="Seg_9360" s="T532">Сел поесть, переночевал.</ta>
            <ta e="T541" id="Seg_9361" s="T536">Эти [его] люди на улице лежат.</ta>
            <ta e="T543" id="Seg_9362" s="T541">Как привязаны.</ta>
            <ta e="T545" id="Seg_9363" s="T543">Так переночевал.</ta>
            <ta e="T549" id="Seg_9364" s="T545">На следующее утро встал.</ta>
            <ta e="T560" id="Seg_9365" s="T549">Пан-старик, у него так лицо всё распухло, еле глазки видно.</ta>
            <ta e="T565" id="Seg_9366" s="T560">Те двое ненцев ещё получше были.</ta>
            <ta e="T572" id="Seg_9367" s="T565">Так сказал, мол, этот парень этим ненцам.</ta>
            <ta e="T577" id="Seg_9368" s="T572">Домой [пусть, мол] отправят (?) [Пана-старика?] на своих нартах.</ta>
            <ta e="T583" id="Seg_9369" s="T577">Потом во второй раз те двое в дом зашли.</ta>
            <ta e="T587" id="Seg_9370" s="T583">То ли ели, то ли нет.</ta>
            <ta e="T600" id="Seg_9371" s="T587">Потом те привязанные их нарты домой, зацепив [на буксир за свои нарты] этих людей их, мёртвых людей так увёз.</ta>
            <ta e="T607" id="Seg_9372" s="T600">Вниз домой Пана-старика тоже зацепив так увез.</ta>
            <ta e="T611" id="Seg_9373" s="T607">Что, еле дышавшего увез.</ta>
            <ta e="T613" id="Seg_9374" s="T611">Уехали (?).</ta>
            <ta e="T619" id="Seg_9375" s="T613">Люди хорошо уехали, жене так сказал.</ta>
            <ta e="T626" id="Seg_9376" s="T619">«Чум сними, мы вверх поедем к людям».</ta>
            <ta e="T632" id="Seg_9377" s="T626">Не знал ни про дедушку, ничего.</ta>
            <ta e="T635" id="Seg_9378" s="T632">Вверх уехал тот.</ta>
            <ta e="T638" id="Seg_9379" s="T635">Сам не разговаривает.</ta>
            <ta e="T642" id="Seg_9380" s="T638">Та жена его порассказала.</ta>
            <ta e="T645" id="Seg_9381" s="T642">Отцу своему и дедушке [им двоим рассказала].</ta>
            <ta e="T649" id="Seg_9382" s="T645">Старики домой уехали.</ta>
            <ta e="T658" id="Seg_9383" s="T649">А потом (…) ничего не знает.</ta>
            <ta e="T660" id="Seg_9384" s="T658">Ушёл.</ta>
            <ta e="T664" id="Seg_9385" s="T660">Они к дедушке в чум зашли.</ta>
            <ta e="T669" id="Seg_9386" s="T664">Нарты и всё прочее [веревкой] запутав так поставили.</ta>
            <ta e="T674" id="Seg_9387" s="T669">Уехали, домой уехали.</ta>
            <ta e="T682" id="Seg_9388" s="T674">Пан-старик в низовье реки еле домой доехал, умер.</ta>
            <ta e="T686" id="Seg_9389" s="T682">Те два человека жили.</ta>
            <ta e="T690" id="Seg_9390" s="T686">Пан-старик его имя. </ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_9391" s="T0">He had one son visiting him, with no one, with no friends.</ta>
            <ta e="T9" id="Seg_9392" s="T5">This is a true story, not a fable.</ta>
            <ta e="T12" id="Seg_9393" s="T9">The old man lived, he became ill.</ta>
            <ta e="T19" id="Seg_9394" s="T12">This Selkup old man lived on this side of the river. </ta>
            <ta e="T25" id="Seg_9395" s="T19">There, where there are no trees at all, that’s where the man lived who became ill.</ta>
            <ta e="T29" id="Seg_9396" s="T25">He had one son, with no friends, with nobody at all.</ta>
            <ta e="T35" id="Seg_9397" s="T29">He didn’t have any daughter, he didn’t have anybody.</ta>
            <ta e="T40" id="Seg_9398" s="T35">Then this boy’s father became ill.</ta>
            <ta e="T47" id="Seg_9399" s="T40">This boy maybe is living there with his father and his mother.</ta>
            <ta e="T52" id="Seg_9400" s="T47">On that [other] side two Nenets brothers lived.</ta>
            <ta e="T54" id="Seg_9401" s="T52">The old Nenets man gets over [to this side]. </ta>
            <ta e="T58" id="Seg_9402" s="T54">That old man [the Selkup] died.</ta>
            <ta e="T62" id="Seg_9403" s="T58">The Nenets oldman, where can he go.</ta>
            <ta e="T65" id="Seg_9404" s="T62">He crossed over to his brother's.</ta>
            <ta e="T69" id="Seg_9405" s="T65">“The old man died, bury him.”</ta>
            <ta e="T75" id="Seg_9406" s="T69">Then again the old woman, having become ill, also died.</ta>
            <ta e="T81" id="Seg_9407" s="T75">Their son is left all alone (/with no friends, with nobody).</ta>
            <ta e="T83" id="Seg_9408" s="T81">Where will he go.</ta>
            <ta e="T88" id="Seg_9409" s="T83">This old Nenets man got [this boy] over to the other side the second time.</ta>
            <ta e="T93" id="Seg_9410" s="T88">He had a big son, that’s the kind of son he had.</ta>
            <ta e="T97" id="Seg_9411" s="T93">He took [him] along, since he doesn’t have reindeer.</ta>
            <ta e="T100" id="Seg_9412" s="T97">Without reindeer they lived, [walking] on foot.</ta>
            <ta e="T102" id="Seg_9413" s="T100">They lived there.</ta>
            <ta e="T107" id="Seg_9414" s="T102">They lived, if you cross over to the other side, in the tent of the old Nenets man.</ta>
            <ta e="T110" id="Seg_9415" s="T107">The old Nenets man had no son.</ta>
            <ta e="T112" id="Seg_9416" s="T110">His wife is childless.</ta>
            <ta e="T118" id="Seg_9417" s="T112">That middle brother has one daughter only.</ta>
            <ta e="T123" id="Seg_9418" s="T118">From lower down the river, two Nenets men came [to them]. </ta>
            <ta e="T128" id="Seg_9419" s="T123">This old Nenets man is surviving on land without a forest.</ta>
            <ta e="T133" id="Seg_9420" s="T128">How many reindeer his family [had], he took them all away.</ta>
            <ta e="T135" id="Seg_9421" s="T133">So he came. </ta>
            <ta e="T142" id="Seg_9422" s="T135">Here the brothers lived, these two old brothers. </ta>
            <ta e="T145" id="Seg_9423" s="T142">They have four hundred reindeer.</ta>
            <ta e="T150" id="Seg_9424" s="T145">He came to take away the reindeer of this old man (/of these old men). </ta>
            <ta e="T152" id="Seg_9425" s="T150">He came with people.</ta>
            <ta e="T157" id="Seg_9426" s="T152">Old man Pan, that’s his name. </ta>
            <ta e="T163" id="Seg_9427" s="T157">Having arrived, they [Pan and his people] spent the night in their tents.</ta>
            <ta e="T167" id="Seg_9428" s="T163">They don’t know this boy.</ta>
            <ta e="T171" id="Seg_9429" s="T167">“How did the boy get to him?</ta>
            <ta e="T175" id="Seg_9430" s="T171">He lives like an old wise man.”</ta>
            <ta e="T181" id="Seg_9431" s="T175">How will the man know, a man living somewhere else [about this boy]. </ta>
            <ta e="T182" id="Seg_9432" s="T181">They spent the nights.</ta>
            <ta e="T192" id="Seg_9433" s="T182">They say, on the third day the old man Pan will come with ten people. </ta>
            <ta e="T195" id="Seg_9434" s="T192">Well, to herd away the reindeer.</ta>
            <ta e="T200" id="Seg_9435" s="T195">It’s that one, he drove away the reindeer of the father and son [?].</ta>
            <ta e="T209" id="Seg_9436" s="T200">How many new houses there are [?], however many things there are, he’ll collect everything.</ta>
            <ta e="T212" id="Seg_9437" s="T209">He had also collected [things] earlier.</ta>
            <ta e="T216" id="Seg_9438" s="T212">He had collected reindeer from the Nenets, he took them away.</ta>
            <ta e="T223" id="Seg_9439" s="T216">Then these Nenets men [Pan and his people] went back.</ta>
            <ta e="T227" id="Seg_9440" s="T223">On the third day, we’ll come, they say.</ta>
            <ta e="T232" id="Seg_9441" s="T227">Don’t go anywhere, they say.</ta>
            <ta e="T236" id="Seg_9442" s="T232">On the second day it dawned.</ta>
            <ta e="T241" id="Seg_9443" s="T236">This boy said this to his [adopted] grandfather.</ta>
            <ta e="T252" id="Seg_9444" s="T241">Up river, he says, there is a [place in] tundra where four hundred reindeer could stay, we’ll herd them there.</ta>
            <ta e="T256" id="Seg_9445" s="T252">[Otherwise] the Nenets men will come and start taking away the reindeer.</ta>
            <ta e="T263" id="Seg_9446" s="T256">Having wintered at the reindeer herding place, what [else] will we do.</ta>
            <ta e="T267" id="Seg_9447" s="T263">The boy said so.</ta>
            <ta e="T270" id="Seg_9448" s="T267">Then he said so.</ta>
            <ta e="T278" id="Seg_9449" s="T270">To me, he says,… you have a new tent, grandfather, give it to me.</ta>
            <ta e="T286" id="Seg_9450" s="T278">And that one, that brother’s daughter, his grandfather is sending her(?).</ta>
            <ta e="T289" id="Seg_9451" s="T286">To this boy…</ta>
            <ta e="T294" id="Seg_9452" s="T289">Up the river there is a neck of land with a forest.</ta>
            <ta e="T296" id="Seg_9453" s="T294">I’ll put the tent there.</ta>
            <ta e="T300" id="Seg_9454" s="T296">He says, bring me the new tent.</ta>
            <ta e="T302" id="Seg_9455" s="T300">The next day they let [the reindeer?] go free.</ta>
            <ta e="T314" id="Seg_9456" s="T302">Maybe on the third day the old man Pan’s [people] will come to them, to take away the reindeer of the old Nenets man. </ta>
            <ta e="T319" id="Seg_9457" s="T314">They then let free the brothers’ reindeer in various directions. </ta>
            <ta e="T327" id="Seg_9458" s="T319">These reindeer were maybe driven, he drives them, too.</ta>
            <ta e="T330" id="Seg_9459" s="T327">[He] was given the tent.</ta>
            <ta e="T339" id="Seg_9460" s="T330">At the front edge of this forested neck of land they set up the tent with this woman.</ta>
            <ta e="T349" id="Seg_9461" s="T339">Of course, a rich Nenets man would [hardly] have no tent, a new tent [it was], a tent with a [new] skin.</ta>
            <ta e="T354" id="Seg_9462" s="T349">Then again they spent the night with this woman.</ta>
            <ta e="T358" id="Seg_9463" s="T354">The next morning it became like this.</ta>
            <ta e="T365" id="Seg_9464" s="T358">To that wife of his [he says], I will go up the river to my people.</ta>
            <ta e="T370" id="Seg_9465" s="T365">Did they put the tent or where did they go. [?]</ta>
            <ta e="T372" id="Seg_9466" s="T370">He moved up the river.</ta>
            <ta e="T379" id="Seg_9467" s="T372">He harnessed three oxen, it looks like thirty reindeer were left.</ta>
            <ta e="T382" id="Seg_9468" s="T379">The boy went up the river.</ta>
            <ta e="T385" id="Seg_9469" s="T382">His wife stayed alone.</ta>
            <ta e="T389" id="Seg_9470" s="T385">She(?) sits and sits.</ta>
            <ta e="T396" id="Seg_9471" s="T389">One day the evening was coming on [?].</ta>
            <ta e="T407" id="Seg_9472" s="T396">This boy slowly felt: up the river [once] people arrived.</ta>
            <ta e="T411" id="Seg_9473" s="T407">From lower down the river old man Pan's [people] arrived.</ta>
            <ta e="T416" id="Seg_9474" s="T411">He has ten men maybe who have to drive the reindeer in.</ta>
            <ta e="T421" id="Seg_9475" s="T416">The old man Pan has such a shaman’s wisdom(?).</ta>
            <ta e="T426" id="Seg_9476" s="T421">Then the second time this boy came, one hears.</ta>
            <ta e="T428" id="Seg_9477" s="T426">His wife heard him.</ta>
            <ta e="T436" id="Seg_9478" s="T428">It seems, he has just arrived, whether the old man Pan’s people came, it seems.</ta>
            <ta e="T442" id="Seg_9479" s="T436">They left the sleds with all their things so, outside, outdoors.</ta>
            <ta e="T444" id="Seg_9480" s="T442">He went into the house.</ta>
            <ta e="T452" id="Seg_9481" s="T444">Standing at the door, at the door he is shaking the dirt off his feet.</ta>
            <ta e="T455" id="Seg_9482" s="T452">He is shaking off his shoes.</ta>
            <ta e="T461" id="Seg_9483" s="T455">So he said to the old man Pan, turning forward:</ta>
            <ta e="T467" id="Seg_9484" s="T461">Your mouth is like an ice-hole on the river. </ta>
            <ta e="T474" id="Seg_9485" s="T467">After that (/the next day) he jumped forward, he moved(?) with his fist.</ta>
            <ta e="T480" id="Seg_9486" s="T474">He lay down all the men of the old man Pan flat [on the ground]. </ta>
            <ta e="T486" id="Seg_9487" s="T480">Some died, the old man Pan remained alive.</ta>
            <ta e="T489" id="Seg_9488" s="T486">Two [more] remained alive.</ta>
            <ta e="T491" id="Seg_9489" s="T489">He threw [them] outdoors.</ta>
            <ta e="T497" id="Seg_9490" s="T491">This boy had stroke [=caressed] once with his fist. </ta>
            <ta e="T503" id="Seg_9491" s="T497">They all shat their clothes (/their pants). </ta>
            <ta e="T509" id="Seg_9492" s="T503">These dead ones, he then threw them outside.</ta>
            <ta e="T516" id="Seg_9493" s="T509">Onto the street, [where?] those sledges, sticks, skis were tied up.</ta>
            <ta e="T522" id="Seg_9494" s="T516">He also took (/threw) the old man Pan outside.</ta>
            <ta e="T526" id="Seg_9495" s="T522">Well, he took them all outdoors.</ta>
            <ta e="T532" id="Seg_9496" s="T526">He sat down and took off his deer hide clothes.</ta>
            <ta e="T536" id="Seg_9497" s="T532">He sat down to eat, spent the night.</ta>
            <ta e="T541" id="Seg_9498" s="T536">These people [of his] are lying outdoors.</ta>
            <ta e="T543" id="Seg_9499" s="T541">As if they had been tied down.</ta>
            <ta e="T545" id="Seg_9500" s="T543">That’s how he spent the night.</ta>
            <ta e="T549" id="Seg_9501" s="T545">The next morning he got up.</ta>
            <ta e="T560" id="Seg_9502" s="T549">The old man Pan, his face got so swollen, his eyes are hardly visible.</ta>
            <ta e="T565" id="Seg_9503" s="T560">Those two Nenets men were still better.</ta>
            <ta e="T572" id="Seg_9504" s="T565">That’s what this boy said to these Nenets men. </ta>
            <ta e="T577" id="Seg_9505" s="T572">[He told them to] take (?) [the old man Pan?] home on their own sledges.</ta>
            <ta e="T583" id="Seg_9506" s="T577">Then the second time those two went into the house.</ta>
            <ta e="T587" id="Seg_9507" s="T583">Maybe they ate, maybe they didn’t.</ta>
            <ta e="T600" id="Seg_9508" s="T587">Then their sleds which were tied down, [he brought them] home, towing [them with his sled], he carried these people, these dead people away like this. </ta>
            <ta e="T607" id="Seg_9509" s="T600">He carried away the old man Pan down home like that, hooked up, too.</ta>
            <ta e="T611" id="Seg_9510" s="T607">What, he took him, hardly breathing, away.</ta>
            <ta e="T613" id="Seg_9511" s="T611">They left (?).</ta>
            <ta e="T619" id="Seg_9512" s="T613">The people left fine, so he told his wife.</ta>
            <ta e="T626" id="Seg_9513" s="T619">“Take the tent down, we’ll go upstream to the people.”</ta>
            <ta e="T632" id="Seg_9514" s="T626">He didn’t know of the grandfather, anything.</ta>
            <ta e="T635" id="Seg_9515" s="T632">That one left upstream. </ta>
            <ta e="T638" id="Seg_9516" s="T635">He is not talking.</ta>
            <ta e="T642" id="Seg_9517" s="T638">His wife told [this story].</ta>
            <ta e="T645" id="Seg_9518" s="T642">To her father and grandfather.</ta>
            <ta e="T649" id="Seg_9519" s="T645">The old men left for home.</ta>
            <ta e="T658" id="Seg_9520" s="T649">And then (…) s/he doesn’t know anything.</ta>
            <ta e="T660" id="Seg_9521" s="T658">He left.</ta>
            <ta e="T664" id="Seg_9522" s="T660">They went into grandfather’s tent.</ta>
            <ta e="T669" id="Seg_9523" s="T664">They put the sled and all the rest up, tied over [with a rope].</ta>
            <ta e="T674" id="Seg_9524" s="T669">They left, they left for home.</ta>
            <ta e="T682" id="Seg_9525" s="T674">The old man Pan hardly reached the lower part of the river, he died.</ta>
            <ta e="T686" id="Seg_9526" s="T682">Those two people lived.</ta>
            <ta e="T690" id="Seg_9527" s="T686">His name is old man Pan.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_9528" s="T0">Einer seiner Söhne war bei ihm, mit niemandem, ohne Freunde.</ta>
            <ta e="T9" id="Seg_9529" s="T5">Das ist eine wahre Geschichte, kein Märchen.</ta>
            <ta e="T12" id="Seg_9530" s="T9">Der alte Mann lebte, er wurde krank.</ta>
            <ta e="T19" id="Seg_9531" s="T12">Dieser alte selkupische Mann lebte auf dieser Seite des Flusses.</ta>
            <ta e="T25" id="Seg_9532" s="T19">Dort, wo es überhaupt keine Bäume gibt, lebte er, der krank wurde.</ta>
            <ta e="T29" id="Seg_9533" s="T25">Er hatte einen Sohn, ohne Freund, ohne irgendjemanden.</ta>
            <ta e="T35" id="Seg_9534" s="T29">Er hatte keine Tochter, er hatte niemanden.</ta>
            <ta e="T40" id="Seg_9535" s="T35">Dann wurde der Vater dieses Jungen krank.</ta>
            <ta e="T47" id="Seg_9536" s="T40">Dieser Junge lebt dort mit seinem Vater und seiner Mutter.</ta>
            <ta e="T52" id="Seg_9537" s="T47">Auf jener [der anderen] Seite gab es zwei nenzische Brüder.</ta>
            <ta e="T54" id="Seg_9538" s="T52">Der alte nenzische Mann kommt [auf diese Seite] herüber.</ta>
            <ta e="T58" id="Seg_9539" s="T54">Jener alte Mann [der Selkupe] starb.</ta>
            <ta e="T62" id="Seg_9540" s="T58">Der alte Nenze, wo kann er hin.</ta>
            <ta e="T65" id="Seg_9541" s="T62">Dorthin zum Bruder fuhr er hinüber.</ta>
            <ta e="T69" id="Seg_9542" s="T65">"Der alte Mann starb, beerdigt ihn."</ta>
            <ta e="T75" id="Seg_9543" s="T69">Dann wurde auch die alte Frau krank und starb.</ta>
            <ta e="T81" id="Seg_9544" s="T75">Ihr Sohn blieb ganz alleine (/ohne jemanden, ohne Freund).</ta>
            <ta e="T83" id="Seg_9545" s="T81">Wo soll er hin.</ta>
            <ta e="T88" id="Seg_9546" s="T83">Dieser alte Nenze brachte [diesen Jungen] zum zweiten Mal auf jene Seite.</ta>
            <ta e="T93" id="Seg_9547" s="T88">Er hatte einen großen Sohn, so einen Sohn hatte er.</ta>
            <ta e="T97" id="Seg_9548" s="T93">Er brachte [ihn] hinüber, Rentiere hat er nicht.</ta>
            <ta e="T100" id="Seg_9549" s="T97">Sie lebten ohne Rentiere, [sie gingen] zu Fuß.</ta>
            <ta e="T102" id="Seg_9550" s="T100">Sie lebten dort.</ta>
            <ta e="T107" id="Seg_9551" s="T102">Sie lebten, wenn man da auf die andere Seite fährt, im Zelt des alten Nenzen.</ta>
            <ta e="T110" id="Seg_9552" s="T107">Der alte Nenze hatte keinen Sohn.</ta>
            <ta e="T112" id="Seg_9553" s="T110">Er hat eine kinderlose Frau.</ta>
            <ta e="T118" id="Seg_9554" s="T112">Jener mittlere Bruder hat eine einzige Tochter.</ta>
            <ta e="T123" id="Seg_9555" s="T118">Von weiter flussabwärts kamen zwei Nenzen [zu ihm].</ta>
            <ta e="T128" id="Seg_9556" s="T123">Dieser alte Nenze lebt in diesem baumlosen Land vor sich hin.</ta>
            <ta e="T133" id="Seg_9557" s="T128">Wieviele Rentiere sein Clan [hatte], er nahm sie alle weg.</ta>
            <ta e="T135" id="Seg_9558" s="T133">So kam er.</ta>
            <ta e="T142" id="Seg_9559" s="T135">Hier lebten die Brüder, die beiden alten Brüder.</ta>
            <ta e="T145" id="Seg_9560" s="T142">Sie haben vierhundert Rentiere.</ta>
            <ta e="T150" id="Seg_9561" s="T145">Er kam, um diesem alten Mann (/alten Männern) die Rentiere wegzunehmen.</ta>
            <ta e="T152" id="Seg_9562" s="T150">Er kam mit Leuten.</ta>
            <ta e="T157" id="Seg_9563" s="T152">Alter Mann Pan, das ist sein Name.</ta>
            <ta e="T163" id="Seg_9564" s="T157">Sie [Pan und seine Leute] kamen an und übernachteten in ihren Zelten.</ta>
            <ta e="T167" id="Seg_9565" s="T163">Sie kennen diesen Jungen nicht.</ta>
            <ta e="T171" id="Seg_9566" s="T167">"Wie kam der Junge zu ihm?</ta>
            <ta e="T175" id="Seg_9567" s="T171">Er lebt wie ein Weiser."</ta>
            <ta e="T181" id="Seg_9568" s="T175">Wie weiß ein Mensch, ein an einem anderen Ort lebender Mensch [von diesem Jungen].</ta>
            <ta e="T182" id="Seg_9569" s="T181">Sie übernachteten.</ta>
            <ta e="T192" id="Seg_9570" s="T182">Man sagt, am dritten Tag kommt der alte Mann Pan mit zehn Leute gefahren.</ta>
            <ta e="T195" id="Seg_9571" s="T192">Nun, um die Rentiere wegzutreiben.</ta>
            <ta e="T200" id="Seg_9572" s="T195">Das ist jener, um die Rentiere des Vaters und des Sohn [?] wegzutreiben.</ta>
            <ta e="T209" id="Seg_9573" s="T200">Wieviele neue Häuser [?], wieviele Sachen gibt es, er wird alles einsammeln.</ta>
            <ta e="T212" id="Seg_9574" s="T209">Früher hat er auch gesammelt.</ta>
            <ta e="T216" id="Seg_9575" s="T212">Er hat Rentiere von den Nenzen eingesammelt und weggenommen.</ta>
            <ta e="T223" id="Seg_9576" s="T216">Dann gingen diese Nenzen [Pan und seine Leute] zurück.</ta>
            <ta e="T227" id="Seg_9577" s="T223">"Am dritten Tag kommen wir, sagt man.</ta>
            <ta e="T232" id="Seg_9578" s="T227">Geht nirgendwo hin, sagt man."</ta>
            <ta e="T236" id="Seg_9579" s="T232">Der zweite Tag dämmerte.</ta>
            <ta e="T241" id="Seg_9580" s="T236">Dieser Junge sagte zu seinem Großvater:</ta>
            <ta e="T252" id="Seg_9581" s="T241">"Flussaufwärts ist [ein Platz in der] Tundra, wo vierhundert Rentiere stehen [können], lass sie uns dorthin treiben.</ta>
            <ta e="T256" id="Seg_9582" s="T252">Die Nenzen kommen [doch] und fangen an, die Rentiere wegzutreiben.</ta>
            <ta e="T263" id="Seg_9583" s="T256">Wenn wir auf dem Weideplatz der Rentiere überwintert haben, was werden wir dann noch tun."</ta>
            <ta e="T267" id="Seg_9584" s="T263">So sprach der Junge.</ta>
            <ta e="T270" id="Seg_9585" s="T267">Dann sagt er:</ta>
            <ta e="T278" id="Seg_9586" s="T270">Mir, als ob,… du hast ein neues Zelt, Großvater, gib es mir.</ta>
            <ta e="T286" id="Seg_9587" s="T278">Und jener(?), die Tochter jenes Bruders, ihr(?) Großvater schickt sie(?).</ta>
            <ta e="T289" id="Seg_9588" s="T286">Zu diesem Jungen…</ta>
            <ta e="T294" id="Seg_9589" s="T289">Flussaufwärts ist eine bewaldete Landenge.</ta>
            <ta e="T296" id="Seg_9590" s="T294">Dort werde ich das Zelt aufstellen.</ta>
            <ta e="T300" id="Seg_9591" s="T296">Bringt mir wohl das neue Zelt."</ta>
            <ta e="T302" id="Seg_9592" s="T300">Am nächsten Tag ließen sie [die Rentiere?] los.</ta>
            <ta e="T314" id="Seg_9593" s="T302">Vielleicht am dritten Tag werden [die Leute] des alten Mannes Pan kommen, um diesem alten Nenzen Rentiere wegzunehmen.</ta>
            <ta e="T319" id="Seg_9594" s="T314">Die Rentiere der Brüder ließen sie da in unterschiedliche Richtungen laufen.</ta>
            <ta e="T327" id="Seg_9595" s="T319">Diese Rentiere trieben sie wohl, er treibt [sie] auch.</ta>
            <ta e="T330" id="Seg_9596" s="T327">[Ihm] wurde das Zelt gegeben.</ta>
            <ta e="T339" id="Seg_9597" s="T330">Am vorderen Ende dieser bewaldeten Landenge stellte er mit der Frau dann das Zelt auf.</ta>
            <ta e="T349" id="Seg_9598" s="T339">Natürlich, ein reicher Nenze hat wohl kaum kein Zelt, [es war] ein neues Zelt, ein Zelt mit [neuem] Fell.</ta>
            <ta e="T354" id="Seg_9599" s="T349">Dann übernachtete er wieder mit dieser Frau.</ta>
            <ta e="T358" id="Seg_9600" s="T354">Am nächsten Morgen geschah dies.</ta>
            <ta e="T365" id="Seg_9601" s="T358">Zu dieser Frau [sagt er]: "Ich werde flussaufwärts zu meinen Leuten gehen."</ta>
            <ta e="T370" id="Seg_9602" s="T365">Stellten sie das Zelt auf oder wo sind sie hin [?].</ta>
            <ta e="T372" id="Seg_9603" s="T370">Er fuhr flussaufwärts.</ta>
            <ta e="T379" id="Seg_9604" s="T372">Er spannte drei Bullen an, ungefähr dreißig Rentiere ließ er zurück.</ta>
            <ta e="T382" id="Seg_9605" s="T379">Der Junge fuhr flussaufwärts.</ta>
            <ta e="T385" id="Seg_9606" s="T382">Seine Frau blieb allein.</ta>
            <ta e="T389" id="Seg_9607" s="T385">Sie(?) sitzt und sitzt.</ta>
            <ta e="T396" id="Seg_9608" s="T389">Einmal wurde es Abend [?].</ta>
            <ta e="T407" id="Seg_9609" s="T396">Dieser Junge fühlte: flussaufwärts kamen [einmal] Leute.</ta>
            <ta e="T411" id="Seg_9610" s="T407">Von weiter flussabwärts kamen die [Leute] des alten Mannes Pan.</ta>
            <ta e="T416" id="Seg_9611" s="T411">Er hat ungefähr zehn Leute, die die Rentiere treiben sollen.</ta>
            <ta e="T421" id="Seg_9612" s="T416">Der alte Mann Pan hat schamanisches Wissen.</ta>
            <ta e="T426" id="Seg_9613" s="T421">Dann ist zu hören, dass dieser Junge zum zweiten Mal kommt.</ta>
            <ta e="T428" id="Seg_9614" s="T426">Seine Frau hörte ihn.</ta>
            <ta e="T436" id="Seg_9615" s="T428">Er ist offenbar gerade angekommen, die Leute des alten Mannes Pan kamen wohl.</ta>
            <ta e="T442" id="Seg_9616" s="T436">Die Schlitten mit allen Sachen ließen sie stehen, draußen.</ta>
            <ta e="T444" id="Seg_9617" s="T442">Er ging ins Haus.</ta>
            <ta e="T452" id="Seg_9618" s="T444">An der Tür stehend streift er sich an der Tür die Schuhe ab.</ta>
            <ta e="T455" id="Seg_9619" s="T452">Er streift seine Schuhe ab.</ta>
            <ta e="T461" id="Seg_9620" s="T455">Er drehte sich nach vorne und sagte zum alten Mann Pan:</ta>
            <ta e="T467" id="Seg_9621" s="T461">"Dein Mund ist wie ein Eisloch im Fluss."</ta>
            <ta e="T474" id="Seg_9622" s="T467">Danach (/am anderen Tag) sprang er weiter, er führte(?) mit der Faust.</ta>
            <ta e="T480" id="Seg_9623" s="T474">Die Leute des alten Mannes Pan legte er alle flach [auf die Erde].</ta>
            <ta e="T486" id="Seg_9624" s="T480">Einige starben, der alte Mann Pan blieb am Leben.</ta>
            <ta e="T489" id="Seg_9625" s="T486">Zwei blieben [noch] am Leben.</ta>
            <ta e="T491" id="Seg_9626" s="T489">Er warf sie nach draußen.</ta>
            <ta e="T497" id="Seg_9627" s="T491">Dieser Junge schlug einmal mit seiner Faust.</ta>
            <ta e="T503" id="Seg_9628" s="T497">Alle machten sich in die Hose.</ta>
            <ta e="T509" id="Seg_9629" s="T503">Diese Toten warf er dann nach draußen.</ta>
            <ta e="T516" id="Seg_9630" s="T509">Nach draußen, [wo?] jene Schlitten, Stöcker und Skier angebunden waren.</ta>
            <ta e="T522" id="Seg_9631" s="T516">Er nahm (/warf) auch den alten Mann Pan nach draußen.</ta>
            <ta e="T526" id="Seg_9632" s="T522">Nun, alle nahm er nach draußen.</ta>
            <ta e="T532" id="Seg_9633" s="T526">Er setzte sich und zog seinen Pelzmantel aus.</ta>
            <ta e="T536" id="Seg_9634" s="T532">Er setzt sich zum Essen und übernachtete.</ta>
            <ta e="T541" id="Seg_9635" s="T536">Diese [seine] Leute liegen draußen.</ta>
            <ta e="T543" id="Seg_9636" s="T541">Wie angebunden.</ta>
            <ta e="T545" id="Seg_9637" s="T543">So übernachtete er.</ta>
            <ta e="T549" id="Seg_9638" s="T545">Am nächsten Morgen stand er auf.</ta>
            <ta e="T560" id="Seg_9639" s="T549">Der alte Mann Pan, sein Gesicht schwoll so an, seine Augen sind kaum zu sehen.</ta>
            <ta e="T565" id="Seg_9640" s="T560">Diese beiden Nenzen waren noch besser.</ta>
            <ta e="T572" id="Seg_9641" s="T565">So sagte es dieser Junge den Nenzen.</ta>
            <ta e="T577" id="Seg_9642" s="T572">Sie [sollen] [den alten Mann Pan?] auf seinen Schlitten nach Hause schicken (?).</ta>
            <ta e="T583" id="Seg_9643" s="T577">Dann gingen die beiden zum zweiten Mal ins Haus.</ta>
            <ta e="T587" id="Seg_9644" s="T583">Vielleicht aßen sie, vielleicht nicht.</ta>
            <ta e="T600" id="Seg_9645" s="T587">Dann ihre angebundenen Schlitten nach Hause, er band diese Leute, diese toten Leute fest und brachte sie weg.</ta>
            <ta e="T607" id="Seg_9646" s="T600">Er band auch den alten Mann Pan fest und brachte ihn [fluss]abwärts nach Hause.</ta>
            <ta e="T611" id="Seg_9647" s="T607">Was, er brachte ihn, kaum atmend.</ta>
            <ta e="T613" id="Seg_9648" s="T611">Sie fuhren weg (?).</ta>
            <ta e="T619" id="Seg_9649" s="T613">Die Leute fuhren gut weg, sagte er der Frau.</ta>
            <ta e="T626" id="Seg_9650" s="T619">"Bau das Zelt ab, wir fahren flussaufwärts zu den Leuten."</ta>
            <ta e="T632" id="Seg_9651" s="T626">Er wusste nicht vom Großvater, von nichts.</ta>
            <ta e="T635" id="Seg_9652" s="T632">Jener fuhr flussaufwärts.</ta>
            <ta e="T638" id="Seg_9653" s="T635">Er selbst spricht nicht.</ta>
            <ta e="T642" id="Seg_9654" s="T638">Seine Frau erzählte das.</ta>
            <ta e="T645" id="Seg_9655" s="T642">Ihrem Vater und Großvater.</ta>
            <ta e="T649" id="Seg_9656" s="T645">Die alten Männer fuhren nach Hause.</ta>
            <ta e="T658" id="Seg_9657" s="T649">Und dann (…) er/sie weiß nichts.</ta>
            <ta e="T660" id="Seg_9658" s="T658">Er ging weg.</ta>
            <ta e="T664" id="Seg_9659" s="T660">Sie gingen ins Zelt des Großvaters.</ta>
            <ta e="T669" id="Seg_9660" s="T664">Sie machten den Schlitten und alles andere [mit einer Schnur] zusammen und stellten es hin.</ta>
            <ta e="T674" id="Seg_9661" s="T669">Sie fuhren los, sie fuhren nach Hause.</ta>
            <ta e="T682" id="Seg_9662" s="T674">Der alte Mann Pan erreichte kaum den Unterlauf des Flusses, er starb.</ta>
            <ta e="T686" id="Seg_9663" s="T682">Diese beiden Leute lebten.</ta>
            <ta e="T690" id="Seg_9664" s="T686">Sein Name ist alter Mann Pan.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_9665" s="T0">один сын бывал без товарища один единственный (никого, кроме него, не было)</ta>
            <ta e="T9" id="Seg_9666" s="T5">это быль, не сказка</ta>
            <ta e="T12" id="Seg_9667" s="T9">старик жил заболел</ta>
            <ta e="T19" id="Seg_9668" s="T12">этот селькупский старик на этой стороне реки жил</ta>
            <ta e="T25" id="Seg_9669" s="T19">там, где деревьев совсем нет (на ровном месте), на этом краю жил тот, который заболел</ta>
            <ta e="T29" id="Seg_9670" s="T25">один сын единственный без товарища</ta>
            <ta e="T35" id="Seg_9671" s="T29">ни дочери не было никого [чего] не было</ta>
            <ta e="T40" id="Seg_9672" s="T35">потом отец заболел этого парня</ta>
            <ta e="T47" id="Seg_9673" s="T40">этот парень там же живет с отцом и с матерью</ta>
            <ta e="T52" id="Seg_9674" s="T47">на той стороне двое ненца братьев были</ta>
            <ta e="T54" id="Seg_9675" s="T52">ненец старик съездит (на ту сторону)</ta>
            <ta e="T58" id="Seg_9676" s="T54">этот старик (селькуп) умер</ta>
            <ta e="T62" id="Seg_9677" s="T58">этот ненец куда денется</ta>
            <ta e="T65" id="Seg_9678" s="T62">обратно к брату переехал</ta>
            <ta e="T69" id="Seg_9679" s="T65">старик умер схороните</ta>
            <ta e="T75" id="Seg_9680" s="T69">во-вторых старуха заболела, тоже [опять] умерла.</ta>
            <ta e="T81" id="Seg_9681" s="T75">сын один одинешенек (беспризорный) остался</ta>
            <ta e="T83" id="Seg_9682" s="T81">куда денется</ta>
            <ta e="T88" id="Seg_9683" s="T83">этот ненец старик вторично на ту сторону (через реку) повез (этого парня)</ta>
            <ta e="T93" id="Seg_9684" s="T88">большой сын такой сын бывал.</ta>
            <ta e="T97" id="Seg_9685" s="T93">повез оленей-то нет</ta>
            <ta e="T100" id="Seg_9686" s="T97">без оленей пешком жили</ta>
            <ta e="T102" id="Seg_9687" s="T100">там жили</ta>
            <ta e="T107" id="Seg_9688" s="T102">жили на ту сторону переехав ненец старик чум</ta>
            <ta e="T110" id="Seg_9689" s="T107">ненец старик сына не было</ta>
            <ta e="T112" id="Seg_9690" s="T110">бездетная жена была</ta>
            <ta e="T118" id="Seg_9691" s="T112">тот средний брат одна дочь единственная</ta>
            <ta e="T123" id="Seg_9692" s="T118">снизу вверх (к ним) два ненца пришли</ta>
            <ta e="T128" id="Seg_9693" s="T123">без лесной земли этот ненец есть</ta>
            <ta e="T133" id="Seg_9694" s="T128">сколько у своих людей оленей всех отбирал</ta>
            <ta e="T135" id="Seg_9695" s="T133">так пришел</ta>
            <ta e="T142" id="Seg_9696" s="T135">братья жили этого старика двое братьев</ta>
            <ta e="T145" id="Seg_9697" s="T142">у них 400 оленей</ta>
            <ta e="T150" id="Seg_9698" s="T145">этого старика оленей отбирать пришел</ta>
            <ta e="T152" id="Seg_9699" s="T150">с людьми пришел</ta>
            <ta e="T157" id="Seg_9700" s="T152">пан старик там тоже есть</ta>
            <ta e="T163" id="Seg_9701" s="T157">там приехали ночевали в их чумах</ta>
            <ta e="T167" id="Seg_9702" s="T163">этого парня не знают</ta>
            <ta e="T171" id="Seg_9703" s="T167">парень как к нему попал</ta>
            <ta e="T175" id="Seg_9704" s="T171">он что мудрецом живет</ta>
            <ta e="T181" id="Seg_9705" s="T175">человек как будет знать в другом месте живущий человек</ta>
            <ta e="T182" id="Seg_9706" s="T181">ночевали</ta>
            <ta e="T192" id="Seg_9707" s="T182">говорят что на третий день что пан старик с 10ю людьми придет</ta>
            <ta e="T195" id="Seg_9708" s="T192">ну, оленей прогнать</ta>
            <ta e="T200" id="Seg_9709" s="T195">этот тот, которого двоих отца с сыном оленей прогнать</ta>
            <ta e="T209" id="Seg_9710" s="T200">сколько дом новый сколько что есть и все соберет</ta>
            <ta e="T212" id="Seg_9711" s="T209">раньше тоже он собирал</ta>
            <ta e="T216" id="Seg_9712" s="T212">у ненцев тоже оленей собравши брал</ta>
            <ta e="T223" id="Seg_9713" s="T216">потом обратно уехали эти ненцы</ta>
            <ta e="T227" id="Seg_9714" s="T223">на 3ий день говорят что [будто бы] придем</ta>
            <ta e="T232" id="Seg_9715" s="T227">вы говорят никуда не уходите</ta>
            <ta e="T236" id="Seg_9716" s="T232">на второй день рассвело</ta>
            <ta e="T241" id="Seg_9717" s="T236">этот парень так сказал дедушке</ta>
            <ta e="T252" id="Seg_9718" s="T241">здесь в верховьях там 400 оленей стоявшие тундра есть, туда загоним</ta>
            <ta e="T256" id="Seg_9719" s="T252">если ненцы пришли [придут] оленей угонять начнут</ta>
            <ta e="T263" id="Seg_9720" s="T256">оленей пастбище [в продолжение] всю[ей] зиму[ы] зимовали [на этом месте] как будем делать</ta>
            <ta e="T267" id="Seg_9721" s="T263">этот парень так сказал</ta>
            <ta e="T270" id="Seg_9722" s="T267">потом так сказал</ta>
            <ta e="T278" id="Seg_9723" s="T270">мне говорит новый чум есть дедушка мне отдай</ta>
            <ta e="T286" id="Seg_9724" s="T278">а тот того братова дочь дедушка этого заставляет</ta>
            <ta e="T294" id="Seg_9725" s="T289">вверху здесь лесная граница есть</ta>
            <ta e="T296" id="Seg_9726" s="T294">там чум поставлю</ta>
            <ta e="T300" id="Seg_9727" s="T296">новый чум дайте мне</ta>
            <ta e="T302" id="Seg_9728" s="T300">распустили</ta>
            <ta e="T314" id="Seg_9729" s="T302">потом на третий день придут к пану старику их этого ненца старика олень отобрать</ta>
            <ta e="T319" id="Seg_9730" s="T314">братьевых оленей потом распустили (в разные стороны)</ta>
            <ta e="T327" id="Seg_9731" s="T319">этих оленей погнали он тоже гонит</ta>
            <ta e="T330" id="Seg_9732" s="T327">чумом отдали</ta>
            <ta e="T339" id="Seg_9733" s="T330">на этом лесном краю передней стороне туда чум поставили с этой женой</ta>
            <ta e="T349" id="Seg_9734" s="T339">конечно богатый ненец что чума нет с [из] новой шкурки сделанный чум</ta>
            <ta e="T354" id="Seg_9735" s="T349">потом двое [опять] проночевали с этой женой</ta>
            <ta e="T358" id="Seg_9736" s="T354">на другое утро так стало</ta>
            <ta e="T365" id="Seg_9737" s="T358">этой жене я говорит вверх (по реке) к людям схожу</ta>
            <ta e="T370" id="Seg_9738" s="T365">что чум сделали или куда делись</ta>
            <ta e="T372" id="Seg_9739" s="T370">вверх по реке поехал</ta>
            <ta e="T379" id="Seg_9740" s="T372">3 быка запряг же ведь 300 оленей оставил</ta>
            <ta e="T382" id="Seg_9741" s="T379">парень вверх по реке уехал</ta>
            <ta e="T385" id="Seg_9742" s="T382">жена его одна осталась</ta>
            <ta e="T389" id="Seg_9743" s="T385">сидит сидит</ta>
            <ta e="T396" id="Seg_9744" s="T389">в одно время день к вечеру пошла</ta>
            <ta e="T407" id="Seg_9745" s="T396">этот паренек очнулся (чувствует) вверху люди приехали</ta>
            <ta e="T411" id="Seg_9746" s="T407">с низу пан старика приехали</ta>
            <ta e="T416" id="Seg_9747" s="T411">10 человек у него для загонки оленей</ta>
            <ta e="T421" id="Seg_9748" s="T416">пан старик такой чувствительный есть</ta>
            <ta e="T426" id="Seg_9749" s="T421">потом вторично этот парень приехал</ta>
            <ta e="T428" id="Seg_9750" s="T426">жена слышит</ta>
            <ta e="T436" id="Seg_9751" s="T428">увидел он приехал [что] Пан старики уже приехали</ta>
            <ta e="T442" id="Seg_9752" s="T436">нарты со всеми вещами туда поставили на улице</ta>
            <ta e="T444" id="Seg_9753" s="T442">в дом зашел</ta>
            <ta e="T452" id="Seg_9754" s="T444">у двери стоя у двери ноги отряхивает</ta>
            <ta e="T455" id="Seg_9755" s="T452">бокари отряхивает</ta>
            <ta e="T461" id="Seg_9756" s="T455">так сказал пану старику вперед повернувшись</ta>
            <ta e="T467" id="Seg_9757" s="T461">рот твой как будто прорубь речки</ta>
            <ta e="T474" id="Seg_9758" s="T467">вперед прыгнув кулаком провел</ta>
            <ta e="T480" id="Seg_9759" s="T474">пан стариков всех (на землю) на бок уложил</ta>
            <ta e="T486" id="Seg_9760" s="T480">некоторые умерли живой остался</ta>
            <ta e="T489" id="Seg_9761" s="T486">двое живые остались</ta>
            <ta e="T491" id="Seg_9762" s="T489">на улицу побросал</ta>
            <ta e="T497" id="Seg_9763" s="T491">этот парень один раз кулаком провел</ta>
            <ta e="T503" id="Seg_9764" s="T497">и все в штаны [в штаны] наклали</ta>
            <ta e="T509" id="Seg_9765" s="T503">эти мертвые потом на улицу побросал</ta>
            <ta e="T516" id="Seg_9766" s="T509">на улицу [все имущество] эти [те] сани палки лыжи попривязанные</ta>
            <ta e="T522" id="Seg_9767" s="T516">пана старика тоже на улицу бросил</ta>
            <ta e="T526" id="Seg_9768" s="T522">ну [в общем] всех на улицу побросал</ta>
            <ta e="T532" id="Seg_9769" s="T526">‎‎сел взяв [сняв]</ta>
            <ta e="T536" id="Seg_9770" s="T532">кушать сел ночевал</ta>
            <ta e="T541" id="Seg_9771" s="T536">эти люди на улице лежат</ta>
            <ta e="T543" id="Seg_9772" s="T541">как привязаны</ta>
            <ta e="T545" id="Seg_9773" s="T543">так ночевал</ta>
            <ta e="T549" id="Seg_9774" s="T545">на другое утро встал</ta>
            <ta e="T560" id="Seg_9775" s="T549">так его лицо все распухло еле глазок видать</ta>
            <ta e="T565" id="Seg_9776" s="T560">те ненцы еще получше были</ta>
            <ta e="T572" id="Seg_9777" s="T565">так сказал этот парень этим ненцам</ta>
            <ta e="T577" id="Seg_9778" s="T572">одевайте, домой повезите своими санками [нартами]</ta>
            <ta e="T583" id="Seg_9779" s="T577">потом вторично эти [те] двое в дом заходили</ta>
            <ta e="T587" id="Seg_9780" s="T583">что кушали или нет</ta>
            <ta e="T600" id="Seg_9781" s="T587">потом те нарты, которые были на привязи, потом домой он зацепив [на буксир за свои нарты] тех людей мертвых людей так увез</ta>
            <ta e="T607" id="Seg_9782" s="T600">вниз домой пана старика тоже зацепив [сзади] так увез</ta>
            <ta e="T611" id="Seg_9783" s="T607">только еле дышавшего увез</ta>
            <ta e="T613" id="Seg_9784" s="T611">уехали</ta>
            <ta e="T619" id="Seg_9785" s="T613">люди хор уехали жене так сказал</ta>
            <ta e="T626" id="Seg_9786" s="T619">чум сними мы вверх поедем к своим людям</ta>
            <ta e="T632" id="Seg_9787" s="T626">не знал дедушки что</ta>
            <ta e="T635" id="Seg_9788" s="T632">вверх уехал тогда</ta>
            <ta e="T638" id="Seg_9789" s="T635">сам не разговаривает</ta>
            <ta e="T642" id="Seg_9790" s="T638">та жена рассказывала (наговорила)</ta>
            <ta e="T645" id="Seg_9791" s="T642">отцу и дедушке им двоим рассказала</ta>
            <ta e="T649" id="Seg_9792" s="T645">старики домой уехали</ta>
            <ta e="T658" id="Seg_9793" s="T649">‎‎(…) филин (…) ничего не знает</ta>
            <ta e="T664" id="Seg_9794" s="T660">они к дедушке в чум зашли</ta>
            <ta e="T669" id="Seg_9795" s="T664">нарты [санки] веревкой запутав так поставили</ta>
            <ta e="T674" id="Seg_9796" s="T669">туда уехали домой уехали</ta>
            <ta e="T682" id="Seg_9797" s="T674">еле домой доехал умер</ta>
            <ta e="T686" id="Seg_9798" s="T682">те двое жили</ta>
            <ta e="T690" id="Seg_9799" s="T686">пан старик имя есть</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T52" id="Seg_9800" s="T47">[BrM:] Unclear usage of ADV.ILL in 'tontɨ'.</ta>
            <ta e="T69" id="Seg_9801" s="T65">[KuAI:] He is speaking to his brother.</ta>
            <ta e="T145" id="Seg_9802" s="T142">[BrM:] Unclear number and possessor person in 'ɔːtäqitɨ'.</ta>
            <ta e="T157" id="Seg_9803" s="T152">[BrM:] 1) It is unclear, whether '′нимты' should be transcribed as nımtɨ (his name) or nɨmtɨ (there). The second variant corresponds better the translation from the archive, the first one, which we have chosen, fits maybe a bit better to the context.</ta>
            <ta e="T195" id="Seg_9804" s="T192">[KJ:] This sentence is probably part of the previous sentence.</ta>
            <ta e="T289" id="Seg_9805" s="T286">[OSV:] The sentence is not finished.</ta>
            <ta e="T365" id="Seg_9806" s="T358">[AAV:] Unclear was this upstream or downstream from the tents of the Nenets brothers. If he's hiding the reindeer upstream, he'd be going downstream now. </ta>
            <ta e="T379" id="Seg_9807" s="T372">[BrM:] In the original translation - 300 reindeer.</ta>
            <ta e="T407" id="Seg_9808" s="T396">[BrM:] 'lʼamɨ kəːŋa' was replaced with 'lʼamɨk kəːŋa'. [BrM:] Unclear usage of the verb taːtɨ- 'to bring', which does not correspond the translation.</ta>
            <ta e="T411" id="Seg_9809" s="T407">[BrM:] Unclear usage of the verb taːtɨ- 'to bring', which does not correspond the translation.</ta>
            <ta e="T583" id="Seg_9810" s="T577">[BrM:] 'meːqıj'???</ta>
            <ta e="T626" id="Seg_9811" s="T619">[BrM:] Unclear usage of optative in 'qəlʼlʼä'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T693" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T694" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T695" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T691" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T692" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
