<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>NEP_196X_HareParka2_flk-afterFW</transcription-name>
         <referenced-file url="NEP_196X_HareParka2_flk.wav" />
         <referenced-file url="NEP_196X_HareParka2_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">NEP_196X_HareParka2_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1153</ud-information>
            <ud-information attribute-name="# HIAT:w">786</ud-information>
            <ud-information attribute-name="# e">796</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">24</ud-information>
            <ud-information attribute-name="# HIAT:u">132</ud-information>
            <ud-information attribute-name="# sc">254</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="NEP">
            <abbreviation>NEP</abbreviation>
            <sex value="f" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.0" type="appl" />
         <tli id="T2" time="0.97" type="appl" />
         <tli id="T3" time="1.94" type="appl" />
         <tli id="T4" time="2.91" type="appl" />
         <tli id="T5" time="3.87333326507125" />
         <tli id="T6" time="4.01" type="appl" />
         <tli id="T7" time="4.413333333333333" type="appl" />
         <tli id="T8" time="4.816666666666666" type="appl" />
         <tli id="T9" time="5.22" type="appl" />
         <tli id="T921" time="5.3195" type="intp" />
         <tli id="T10" time="5.419" type="appl" />
         <tli id="T11" time="5.732399999999999" type="appl" />
         <tli id="T12" time="6.0458" type="appl" />
         <tli id="T13" time="6.3591999999999995" type="appl" />
         <tli id="T14" time="6.672599999999999" type="appl" />
         <tli id="T15" time="6.986" type="appl" />
         <tli id="T922" time="7.116" type="intp" />
         <tli id="T16" time="7.246" type="appl" />
         <tli id="T17" time="7.728000000000001" type="appl" />
         <tli id="T18" time="8.21" type="appl" />
         <tli id="T19" time="8.692" type="appl" />
         <tli id="T20" time="9.174" type="appl" />
         <tli id="T21" time="9.655999999999999" type="appl" />
         <tli id="T22" time="10.138" type="appl" />
         <tli id="T23" time="10.62" type="appl" />
         <tli id="T24" time="11.833" type="appl" />
         <tli id="T25" time="12.509125000000001" type="appl" />
         <tli id="T26" time="13.18525" type="appl" />
         <tli id="T27" time="13.861375" type="appl" />
         <tli id="T28" time="14.537500000000001" type="appl" />
         <tli id="T29" time="15.213625" type="appl" />
         <tli id="T30" time="15.889750000000001" type="appl" />
         <tli id="T31" time="16.565875000000002" type="appl" />
         <tli id="T32" time="17.242" type="appl" />
         <tli id="T33" time="17.559" type="appl" />
         <tli id="T34" time="18.19666666666667" type="appl" />
         <tli id="T35" time="18.834333333333333" type="appl" />
         <tli id="T36" time="19.472" type="appl" />
         <tli id="T37" time="20.193" type="appl" />
         <tli id="T38" time="21.163" type="appl" />
         <tli id="T39" time="22.133" type="appl" />
         <tli id="T40" time="22.766" type="appl" />
         <tli id="T41" time="23.316" type="appl" />
         <tli id="T42" time="23.866" type="appl" />
         <tli id="T43" time="24.415999999999997" type="appl" />
         <tli id="T44" time="24.965999999999998" type="appl" />
         <tli id="T45" time="25.516" type="appl" />
         <tli id="T46" time="26.066" type="appl" />
         <tli id="T47" time="26.332" type="appl" />
         <tli id="T48" time="26.942800000000002" type="appl" />
         <tli id="T49" time="27.5536" type="appl" />
         <tli id="T50" time="28.1644" type="appl" />
         <tli id="T51" time="28.775199999999998" type="appl" />
         <tli id="T52" time="29.386" type="appl" />
         <tli id="T53" time="29.853" type="appl" />
         <tli id="T54" time="30.166363636363638" type="appl" />
         <tli id="T55" time="30.479727272727274" type="appl" />
         <tli id="T56" time="30.79309090909091" type="appl" />
         <tli id="T57" time="31.106454545454547" type="appl" />
         <tli id="T58" time="31.41981818181818" type="appl" />
         <tli id="T59" time="31.733181818181816" type="appl" />
         <tli id="T60" time="32.04654545454545" type="appl" />
         <tli id="T61" time="32.35990909090909" type="appl" />
         <tli id="T62" time="32.673272727272725" type="appl" />
         <tli id="T63" time="32.986636363636364" type="appl" />
         <tli id="T64" time="33.3" type="appl" />
         <tli id="T65" time="34.977" type="appl" />
         <tli id="T66" time="35.52385714285714" type="appl" />
         <tli id="T67" time="36.07071428571428" type="appl" />
         <tli id="T68" time="36.617571428571424" type="appl" />
         <tli id="T69" time="37.16442857142857" type="appl" />
         <tli id="T70" time="37.711285714285715" type="appl" />
         <tli id="T71" time="38.25814285714286" type="appl" />
         <tli id="T72" time="38.805" type="appl" />
         <tli id="T73" time="38.988" type="appl" />
         <tli id="T74" time="39.8245" type="appl" />
         <tli id="T75" time="40.661" type="appl" />
         <tli id="T76" time="41.055" type="appl" />
         <tli id="T77" time="41.4765" type="appl" />
         <tli id="T78" time="41.898" type="appl" />
         <tli id="T79" time="42.3195" type="appl" />
         <tli id="T80" time="42.741" type="appl" />
         <tli id="T81" time="43.1625" type="appl" />
         <tli id="T82" time="43.584" type="appl" />
         <tli id="T83" time="44.0055" type="appl" />
         <tli id="T84" time="44.427" type="appl" />
         <tli id="T85" time="44.8485" type="appl" />
         <tli id="T86" time="45.27" type="appl" />
         <tli id="T87" time="45.691500000000005" type="appl" />
         <tli id="T88" time="46.113" type="appl" />
         <tli id="T89" time="46.5345" type="appl" />
         <tli id="T90" time="46.956" type="appl" />
         <tli id="T91" time="47.638" type="appl" />
         <tli id="T92" time="48.150888888888886" type="appl" />
         <tli id="T93" time="48.663777777777774" type="appl" />
         <tli id="T94" time="49.17666666666666" type="appl" />
         <tli id="T95" time="49.68955555555555" type="appl" />
         <tli id="T96" time="50.202444444444446" type="appl" />
         <tli id="T97" time="50.715333333333334" type="appl" />
         <tli id="T98" time="51.22822222222222" type="appl" />
         <tli id="T99" time="51.74111111111111" type="appl" />
         <tli id="T100" time="52.254" type="appl" />
         <tli id="T101" time="53.272" type="appl" />
         <tli id="T102" time="53.74633333333333" type="appl" />
         <tli id="T103" time="54.220666666666666" type="appl" />
         <tli id="T104" time="54.695" type="appl" />
         <tli id="T105" time="54.753" type="appl" />
         <tli id="T106" time="55.292" type="appl" />
         <tli id="T107" time="55.831" type="appl" />
         <tli id="T108" time="56.370000000000005" type="appl" />
         <tli id="T109" time="56.909" type="appl" />
         <tli id="T110" time="57.448" type="appl" />
         <tli id="T111" time="57.987" type="appl" />
         <tli id="T112" time="58.526" type="appl" />
         <tli id="T113" time="58.761" type="appl" />
         <tli id="T114" time="59.611" type="appl" />
         <tli id="T115" time="59.938" type="appl" />
         <tli id="T116" time="60.647" type="appl" />
         <tli id="T117" time="61.356" type="appl" />
         <tli id="T118" time="62.065" type="appl" />
         <tli id="T119" time="62.774" type="appl" />
         <tli id="T120" time="63.483" type="appl" />
         <tli id="T121" time="63.511" type="appl" />
         <tli id="T122" time="63.9854" type="appl" />
         <tli id="T123" time="64.4598" type="appl" />
         <tli id="T124" time="64.9342" type="appl" />
         <tli id="T125" time="65.40859999999999" type="appl" />
         <tli id="T126" time="65.883" type="appl" />
         <tli id="T127" time="66.266" type="appl" />
         <tli id="T128" time="66.689625" type="appl" />
         <tli id="T129" time="67.11325000000001" type="appl" />
         <tli id="T130" time="67.53687500000001" type="appl" />
         <tli id="T131" time="67.9605" type="appl" />
         <tli id="T132" time="68.384125" type="appl" />
         <tli id="T133" time="68.80775" type="appl" />
         <tli id="T134" time="69.231375" type="appl" />
         <tli id="T135" time="69.655" type="appl" />
         <tli id="T136" time="69.766" type="appl" />
         <tli id="T137" time="70.31271428571429" type="appl" />
         <tli id="T138" time="70.85942857142858" type="appl" />
         <tli id="T139" time="71.40614285714287" type="appl" />
         <tli id="T140" time="71.95285714285714" type="appl" />
         <tli id="T141" time="72.49957142857143" type="appl" />
         <tli id="T142" time="73.04628571428572" type="appl" />
         <tli id="T143" time="73.593" type="appl" />
         <tli id="T144" time="73.866" type="appl" />
         <tli id="T145" time="74.2743" type="appl" />
         <tli id="T146" time="74.6826" type="appl" />
         <tli id="T147" time="75.0909" type="appl" />
         <tli id="T148" time="75.4992" type="appl" />
         <tli id="T149" time="75.9075" type="appl" />
         <tli id="T150" time="76.3158" type="appl" />
         <tli id="T151" time="76.72409999999999" type="appl" />
         <tli id="T152" time="77.1324" type="appl" />
         <tli id="T153" time="77.5407" type="appl" />
         <tli id="T154" time="77.949" type="appl" />
         <tli id="T155" time="78.088" type="appl" />
         <tli id="T156" time="78.66425" type="appl" />
         <tli id="T157" time="79.2405" type="appl" />
         <tli id="T158" time="79.81675" type="appl" />
         <tli id="T159" time="80.393" type="appl" />
         <tli id="T160" time="81.338" type="appl" />
         <tli id="T161" time="81.70633333333333" type="appl" />
         <tli id="T162" time="82.07466666666666" type="appl" />
         <tli id="T163" time="82.443" type="appl" />
         <tli id="T164" time="82.493" type="appl" />
         <tli id="T165" time="83.199" type="appl" />
         <tli id="T166" time="83.905" type="appl" />
         <tli id="T167" time="83.977" type="appl" />
         <tli id="T168" time="84.45700000000001" type="appl" />
         <tli id="T169" time="84.937" type="appl" />
         <tli id="T170" time="85.417" type="appl" />
         <tli id="T171" time="85.897" type="appl" />
         <tli id="T172" time="86.37700000000001" type="appl" />
         <tli id="T173" time="86.857" type="appl" />
         <tli id="T174" time="87.337" type="appl" />
         <tli id="T175" time="87.81700000000001" type="appl" />
         <tli id="T176" time="88.297" type="appl" />
         <tli id="T177" time="88.777" type="appl" />
         <tli id="T178" time="91.466" type="appl" />
         <tli id="T179" time="92.18266666666666" type="appl" />
         <tli id="T180" time="92.89933333333333" type="appl" />
         <tli id="T181" time="93.616" type="appl" />
         <tli id="T182" time="94.216" type="appl" />
         <tli id="T183" time="94.64377777777777" type="appl" />
         <tli id="T184" time="95.07155555555555" type="appl" />
         <tli id="T185" time="95.49933333333333" type="appl" />
         <tli id="T186" time="95.9271111111111" type="appl" />
         <tli id="T187" time="96.3548888888889" type="appl" />
         <tli id="T188" time="96.78266666666667" type="appl" />
         <tli id="T189" time="97.21044444444445" type="appl" />
         <tli id="T190" time="97.63822222222223" type="appl" />
         <tli id="T191" time="98.066" type="appl" />
         <tli id="T192" time="98.744" type="appl" />
         <tli id="T193" time="99.1618" type="appl" />
         <tli id="T194" time="99.5796" type="appl" />
         <tli id="T195" time="99.9974" type="appl" />
         <tli id="T196" time="100.4152" type="appl" />
         <tli id="T197" time="100.833" type="appl" />
         <tli id="T198" time="101.2508" type="appl" />
         <tli id="T199" time="101.6686" type="appl" />
         <tli id="T200" time="102.0864" type="appl" />
         <tli id="T201" time="102.5042" type="appl" />
         <tli id="T202" time="102.922" type="appl" />
         <tli id="T203" time="104.994" type="appl" />
         <tli id="T204" time="105.79966666666667" type="appl" />
         <tli id="T205" time="106.60533333333333" type="appl" />
         <tli id="T206" time="107.411" type="appl" />
         <tli id="T207" time="107.877" type="appl" />
         <tli id="T208" time="108.60666666666667" type="appl" />
         <tli id="T209" time="109.33633333333333" type="appl" />
         <tli id="T210" time="110.066" type="appl" />
         <tli id="T211" time="110.177" type="appl" />
         <tli id="T212" time="110.65457142857143" type="appl" />
         <tli id="T213" time="111.13214285714287" type="appl" />
         <tli id="T214" time="111.60971428571429" type="appl" />
         <tli id="T215" time="112.08728571428571" type="appl" />
         <tli id="T216" time="112.56485714285714" type="appl" />
         <tli id="T217" time="113.04242857142857" type="appl" />
         <tli id="T218" time="113.52" type="appl" />
         <tli id="T219" time="113.74" type="appl" />
         <tli id="T220" time="115.177" type="appl" />
         <tli id="T221" time="115.327" type="appl" />
         <tli id="T222" time="115.991" type="appl" />
         <tli id="T223" time="116.655" type="appl" />
         <tli id="T224" time="117.433" type="appl" />
         <tli id="T225" time="117.88968750000001" type="appl" />
         <tli id="T226" time="118.34637500000001" type="appl" />
         <tli id="T227" time="118.80306250000001" type="appl" />
         <tli id="T228" time="119.25975" type="appl" />
         <tli id="T229" time="119.7164375" type="appl" />
         <tli id="T230" time="120.173125" type="appl" />
         <tli id="T231" time="120.6298125" type="appl" />
         <tli id="T232" time="121.0865" type="appl" />
         <tli id="T233" time="121.5431875" type="appl" />
         <tli id="T234" time="121.999875" type="appl" />
         <tli id="T235" time="122.4565625" type="appl" />
         <tli id="T236" time="122.91325" type="appl" />
         <tli id="T237" time="123.36993749999999" type="appl" />
         <tli id="T238" time="123.82662499999999" type="appl" />
         <tli id="T239" time="124.2833125" type="appl" />
         <tli id="T240" time="124.74" type="appl" />
         <tli id="T241" time="127.96" type="appl" />
         <tli id="T242" time="128.5518" type="appl" />
         <tli id="T243" time="129.1436" type="appl" />
         <tli id="T244" time="129.7354" type="appl" />
         <tli id="T245" time="130.3272" type="appl" />
         <tli id="T246" time="130.91899999999998" type="appl" />
         <tli id="T247" time="131.5108" type="appl" />
         <tli id="T248" time="132.1026" type="appl" />
         <tli id="T249" time="132.69439999999997" type="appl" />
         <tli id="T250" time="133.28619999999998" type="appl" />
         <tli id="T251" time="133.878" type="appl" />
         <tli id="T252" time="134.077" type="appl" />
         <tli id="T253" time="134.96" type="appl" />
         <tli id="T254" time="135.455" type="appl" />
         <tli id="T255" time="135.86700000000002" type="appl" />
         <tli id="T256" time="136.279" type="appl" />
         <tli id="T257" time="136.691" type="appl" />
         <tli id="T258" time="137.103" type="appl" />
         <tli id="T259" time="137.515" type="appl" />
         <tli id="T260" time="137.927" type="appl" />
         <tli id="T261" time="138.077" type="appl" />
         <tli id="T262" time="138.57" type="appl" />
         <tli id="T263" time="139.063" type="appl" />
         <tli id="T264" time="139.556" type="appl" />
         <tli id="T265" time="140.049" type="appl" />
         <tli id="T266" time="140.404" type="appl" />
         <tli id="T267" time="140.9596" type="appl" />
         <tli id="T268" time="141.5152" type="appl" />
         <tli id="T269" time="142.0708" type="appl" />
         <tli id="T270" time="142.6264" type="appl" />
         <tli id="T271" time="143.182" type="appl" />
         <tli id="T272" time="143.799" type="appl" />
         <tli id="T273" time="144.30857142857144" type="appl" />
         <tli id="T274" time="144.81814285714287" type="appl" />
         <tli id="T275" time="145.3277142857143" type="appl" />
         <tli id="T276" time="145.8372857142857" type="appl" />
         <tli id="T277" time="146.34685714285715" type="appl" />
         <tli id="T278" time="146.85642857142858" type="appl" />
         <tli id="T279" time="147.366" type="appl" />
         <tli id="T280" time="148.21" type="appl" />
         <tli id="T281" time="148.71125" type="appl" />
         <tli id="T282" time="149.2125" type="appl" />
         <tli id="T283" time="149.71375" type="appl" />
         <tli id="T284" time="150.215" type="appl" />
         <tli id="T285" time="150.71625" type="appl" />
         <tli id="T286" time="151.2175" type="appl" />
         <tli id="T287" time="151.71875" type="appl" />
         <tli id="T288" time="152.22" type="appl" />
         <tli id="T289" time="152.64025" type="appl" />
         <tli id="T290" time="153.0605" type="appl" />
         <tli id="T291" time="153.48075" type="appl" />
         <tli id="T292" time="153.901" type="appl" />
         <tli id="T293" time="154.32125" type="appl" />
         <tli id="T294" time="154.7415" type="appl" />
         <tli id="T295" time="155.16174999999998" type="appl" />
         <tli id="T296" time="155.582" type="appl" />
         <tli id="T297" time="156.128" type="appl" />
         <tli id="T298" time="156.74681818181816" type="appl" />
         <tli id="T299" time="157.36563636363636" type="appl" />
         <tli id="T300" time="157.98445454545453" type="appl" />
         <tli id="T301" time="158.60327272727272" type="appl" />
         <tli id="T302" time="159.2220909090909" type="appl" />
         <tli id="T303" time="159.8409090909091" type="appl" />
         <tli id="T304" time="160.45972727272726" type="appl" />
         <tli id="T305" time="161.07854545454546" type="appl" />
         <tli id="T306" time="161.69736363636363" type="appl" />
         <tli id="T307" time="162.31618181818183" type="appl" />
         <tli id="T308" time="162.935" type="appl" />
         <tli id="T309" time="163.606" type="appl" />
         <tli id="T310" time="164.2188" type="appl" />
         <tli id="T311" time="164.83159999999998" type="appl" />
         <tli id="T312" time="165.4444" type="appl" />
         <tli id="T313" time="166.0572" type="appl" />
         <tli id="T314" time="166.67" type="appl" />
         <tli id="T315" time="167.88" type="appl" />
         <tli id="T316" time="168.7825" type="appl" />
         <tli id="T317" time="169.685" type="appl" />
         <tli id="T318" time="170.113" type="appl" />
         <tli id="T319" time="171.492" type="appl" />
         <tli id="T320" time="172.064" type="appl" />
         <tli id="T321" time="172.44244444444445" type="appl" />
         <tli id="T322" time="172.82088888888887" type="appl" />
         <tli id="T323" time="173.19933333333333" type="appl" />
         <tli id="T324" time="173.57777777777778" type="appl" />
         <tli id="T325" time="173.9562222222222" type="appl" />
         <tli id="T326" time="174.33466666666666" type="appl" />
         <tli id="T327" time="174.71311111111112" type="appl" />
         <tli id="T328" time="175.09155555555554" type="appl" />
         <tli id="T329" time="175.47" type="appl" />
         <tli id="T330" time="176.27" type="appl" />
         <tli id="T331" time="176.9215" type="appl" />
         <tli id="T332" time="177.573" type="appl" />
         <tli id="T333" time="178.2245" type="appl" />
         <tli id="T334" time="178.876" type="appl" />
         <tli id="T335" time="179.092" type="appl" />
         <tli id="T336" time="179.53060000000002" type="appl" />
         <tli id="T337" time="179.9692" type="appl" />
         <tli id="T338" time="180.4078" type="appl" />
         <tli id="T339" time="180.8464" type="appl" />
         <tli id="T340" time="181.285" type="appl" />
         <tli id="T341" time="181.528" type="appl" />
         <tli id="T342" time="181.9325714285714" type="appl" />
         <tli id="T343" time="182.33714285714285" type="appl" />
         <tli id="T344" time="182.7417142857143" type="appl" />
         <tli id="T345" time="183.1462857142857" type="appl" />
         <tli id="T346" time="183.55085714285713" type="appl" />
         <tli id="T347" time="183.95542857142857" type="appl" />
         <tli id="T348" time="184.36" type="appl" />
         <tli id="T349" time="184.76457142857143" type="appl" />
         <tli id="T350" time="185.16914285714284" type="appl" />
         <tli id="T351" time="185.5737142857143" type="appl" />
         <tli id="T352" time="185.97828571428573" type="appl" />
         <tli id="T353" time="186.38285714285715" type="appl" />
         <tli id="T354" time="186.78742857142856" type="appl" />
         <tli id="T355" time="187.192" type="appl" />
         <tli id="T356" time="187.392" type="appl" />
         <tli id="T357" time="188.23" type="appl" />
         <tli id="T358" time="189.068" type="appl" />
         <tli id="T359" time="189.906" type="appl" />
         <tli id="T360" time="190.24" type="appl" />
         <tli id="T361" time="190.79600000000002" type="appl" />
         <tli id="T362" time="191.352" type="appl" />
         <tli id="T363" time="191.90800000000002" type="appl" />
         <tli id="T364" time="192.464" type="appl" />
         <tli id="T365" time="193.02" type="appl" />
         <tli id="T366" time="194.346" type="appl" />
         <tli id="T367" time="194.84455555555556" type="appl" />
         <tli id="T368" time="195.3431111111111" type="appl" />
         <tli id="T369" time="195.84166666666667" type="appl" />
         <tli id="T370" time="196.34022222222222" type="appl" />
         <tli id="T371" time="196.83877777777778" type="appl" />
         <tli id="T372" time="197.33733333333333" type="appl" />
         <tli id="T373" time="197.8358888888889" type="appl" />
         <tli id="T374" time="198.33444444444444" type="appl" />
         <tli id="T375" time="198.833" type="appl" />
         <tli id="T376" time="199.586" type="appl" />
         <tli id="T377" time="200.03457142857144" type="appl" />
         <tli id="T378" time="200.48314285714287" type="appl" />
         <tli id="T379" time="200.9317142857143" type="appl" />
         <tli id="T380" time="201.38028571428572" type="appl" />
         <tli id="T381" time="201.82885714285715" type="appl" />
         <tli id="T382" time="202.27742857142857" type="appl" />
         <tli id="T383" time="202.726" type="appl" />
         <tli id="T384" time="203.132" type="appl" />
         <tli id="T385" time="203.55746153846155" type="appl" />
         <tli id="T386" time="203.98292307692307" type="appl" />
         <tli id="T387" time="204.40838461538462" type="appl" />
         <tli id="T388" time="204.83384615384617" type="appl" />
         <tli id="T389" time="205.2593076923077" type="appl" />
         <tli id="T390" time="205.68476923076923" type="appl" />
         <tli id="T391" time="206.11023076923078" type="appl" />
         <tli id="T392" time="206.53569230769233" type="appl" />
         <tli id="T393" time="206.96115384615385" type="appl" />
         <tli id="T394" time="207.3866153846154" type="appl" />
         <tli id="T395" time="207.81207692307694" type="appl" />
         <tli id="T396" time="208.23753846153846" type="appl" />
         <tli id="T397" time="208.663" type="appl" />
         <tli id="T398" time="209.128" type="appl" />
         <tli id="T399" time="209.52042857142857" type="appl" />
         <tli id="T400" time="209.91285714285712" type="appl" />
         <tli id="T401" time="210.3052857142857" type="appl" />
         <tli id="T402" time="210.69771428571428" type="appl" />
         <tli id="T403" time="211.09014285714287" type="appl" />
         <tli id="T404" time="211.48257142857142" type="appl" />
         <tli id="T405" time="211.875" type="appl" />
         <tli id="T406" time="212.26742857142858" type="appl" />
         <tli id="T407" time="212.65985714285713" type="appl" />
         <tli id="T408" time="213.05228571428572" type="appl" />
         <tli id="T409" time="213.4447142857143" type="appl" />
         <tli id="T410" time="213.83714285714288" type="appl" />
         <tli id="T411" time="214.22957142857143" type="appl" />
         <tli id="T412" time="214.622" type="appl" />
         <tli id="T413" time="215.034" type="appl" />
         <tli id="T414" time="215.4665" type="appl" />
         <tli id="T415" time="215.899" type="appl" />
         <tli id="T416" time="216.3315" type="appl" />
         <tli id="T417" time="216.764" type="appl" />
         <tli id="T418" time="217.522" type="appl" />
         <tli id="T419" time="218.00533333333334" type="appl" />
         <tli id="T420" time="218.48866666666666" type="appl" />
         <tli id="T421" time="218.97199999999998" type="appl" />
         <tli id="T422" time="219.45533333333333" type="appl" />
         <tli id="T423" time="219.93866666666668" type="appl" />
         <tli id="T424" time="220.422" type="appl" />
         <tli id="T425" time="220.90533333333332" type="appl" />
         <tli id="T426" time="221.38866666666667" type="appl" />
         <tli id="T427" time="221.872" type="appl" />
         <tli id="T428" time="222.35533333333333" type="appl" />
         <tli id="T429" time="222.83866666666665" type="appl" />
         <tli id="T430" time="223.322" type="appl" />
         <tli id="T431" time="223.989" type="appl" />
         <tli id="T432" time="224.24633333333333" type="appl" />
         <tli id="T433" time="224.50366666666667" type="appl" />
         <tli id="T434" time="224.761" type="appl" />
         <tli id="T435" time="225.01833333333335" type="appl" />
         <tli id="T436" time="225.27566666666667" type="appl" />
         <tli id="T437" time="225.53300000000002" type="appl" />
         <tli id="T438" time="225.79033333333334" type="appl" />
         <tli id="T439" time="226.04766666666666" type="appl" />
         <tli id="T440" time="226.305" type="appl" />
         <tli id="T441" time="226.56233333333333" type="appl" />
         <tli id="T442" time="226.81966666666668" type="appl" />
         <tli id="T923" time="229.4706293806144" />
         <tli id="T443" time="229.5772947903959" />
         <tli id="T444" time="229.73062631695686" />
         <tli id="T445" time="229.9239573721859" />
         <tli id="T446" time="230.21728724908508" />
         <tli id="T924" time="230.277" type="intp" />
         <tli id="T447" time="230.388" type="appl" />
         <tli id="T448" time="230.829" type="appl" />
         <tli id="T449" time="230.835" type="appl" />
         <tli id="T450" time="231.19733333333335" type="appl" />
         <tli id="T451" time="231.5596666666667" type="appl" />
         <tli id="T452" time="231.922" type="appl" />
         <tli id="T453" time="232.28433333333334" type="appl" />
         <tli id="T454" time="232.64666666666668" type="appl" />
         <tli id="T455" time="233.00900000000001" type="appl" />
         <tli id="T456" time="233.37133333333335" type="appl" />
         <tli id="T457" time="233.73366666666666" type="appl" />
         <tli id="T458" time="234.096" type="appl" />
         <tli id="T459" time="234.45833333333334" type="appl" />
         <tli id="T460" time="234.82066666666668" type="appl" />
         <tli id="T461" time="235.18300000000002" type="appl" />
         <tli id="T462" time="235.54533333333333" type="appl" />
         <tli id="T463" time="235.90766666666667" type="appl" />
         <tli id="T464" time="236.27" type="appl" />
         <tli id="T465" time="236.858" type="appl" />
         <tli id="T466" time="237.27827272727274" type="appl" />
         <tli id="T467" time="237.69854545454547" type="appl" />
         <tli id="T468" time="238.11881818181817" type="appl" />
         <tli id="T469" time="238.5390909090909" type="appl" />
         <tli id="T470" time="238.95936363636363" type="appl" />
         <tli id="T471" time="239.37963636363637" type="appl" />
         <tli id="T472" time="239.7999090909091" type="appl" />
         <tli id="T473" time="240.22018181818183" type="appl" />
         <tli id="T474" time="240.64045454545453" type="appl" />
         <tli id="T475" time="241.06072727272726" type="appl" />
         <tli id="T476" time="241.481" type="appl" />
         <tli id="T477" time="241.94" type="appl" />
         <tli id="T478" time="242.57133333333334" type="appl" />
         <tli id="T479" time="243.20266666666666" type="appl" />
         <tli id="T480" time="243.834" type="appl" />
         <tli id="T481" time="243.975" type="appl" />
         <tli id="T482" time="244.3944" type="appl" />
         <tli id="T483" time="244.8138" type="appl" />
         <tli id="T484" time="245.2332" type="appl" />
         <tli id="T485" time="245.6526" type="appl" />
         <tli id="T486" time="246.072" type="appl" />
         <tli id="T487" time="246.4914" type="appl" />
         <tli id="T488" time="246.9108" type="appl" />
         <tli id="T489" time="247.33020000000002" type="appl" />
         <tli id="T490" time="247.74960000000002" type="appl" />
         <tli id="T491" time="248.169" type="appl" />
         <tli id="T492" time="248.223" type="appl" />
         <tli id="T493" time="248.70457142857143" type="appl" />
         <tli id="T494" time="249.18614285714287" type="appl" />
         <tli id="T495" time="249.66771428571428" type="appl" />
         <tli id="T496" time="250.14928571428572" type="appl" />
         <tli id="T497" time="250.63085714285714" type="appl" />
         <tli id="T498" time="251.11242857142858" type="appl" />
         <tli id="T499" time="251.594" type="appl" />
         <tli id="T500" time="252.217" type="appl" />
         <tli id="T501" time="252.59568750000003" type="appl" />
         <tli id="T502" time="252.974375" type="appl" />
         <tli id="T503" time="253.35306250000002" type="appl" />
         <tli id="T504" time="253.73175" type="appl" />
         <tli id="T505" time="254.11043750000002" type="appl" />
         <tli id="T506" time="254.489125" type="appl" />
         <tli id="T507" time="254.8678125" type="appl" />
         <tli id="T508" time="255.24650000000003" type="appl" />
         <tli id="T509" time="255.6251875" type="appl" />
         <tli id="T510" time="256.003875" type="appl" />
         <tli id="T511" time="256.3825625" type="appl" />
         <tli id="T512" time="256.76125" type="appl" />
         <tli id="T513" time="257.13993750000003" type="appl" />
         <tli id="T514" time="257.518625" type="appl" />
         <tli id="T515" time="257.8973125" type="appl" />
         <tli id="T516" time="258.276" type="appl" />
         <tli id="T517" time="258.899" type="appl" />
         <tli id="T518" time="259.75" type="appl" />
         <tli id="T519" time="260.601" type="appl" />
         <tli id="T520" time="261.452" type="appl" />
         <tli id="T521" time="262.303" type="appl" />
         <tli id="T522" time="263.154" type="appl" />
         <tli id="T523" time="264.005" type="appl" />
         <tli id="T524" time="264.217" type="appl" />
         <tli id="T525" time="264.791" type="appl" />
         <tli id="T526" time="265.365" type="appl" />
         <tli id="T527" time="265.93899999999996" type="appl" />
         <tli id="T528" time="266.513" type="appl" />
         <tli id="T529" time="267.087" type="appl" />
         <tli id="T530" time="271.993" type="appl" />
         <tli id="T531" time="272.935" type="appl" />
         <tli id="T926" time="273.329" type="intp" />
         <tli id="T532" time="273.723" type="appl" />
         <tli id="T533" time="274.48199999999997" type="appl" />
         <tli id="T534" time="275.241" type="appl" />
         <tli id="T925" time="275.35249999999996" type="intp" />
         <tli id="T535" time="275.464" type="appl" />
         <tli id="T536" time="275.9905" type="appl" />
         <tli id="T537" time="276.517" type="appl" />
         <tli id="T538" time="277.0435" type="appl" />
         <tli id="T539" time="277.57" type="appl" />
         <tli id="T540" time="278.764" type="appl" />
         <tli id="T541" time="279.29018181818185" type="appl" />
         <tli id="T542" time="279.81636363636363" type="appl" />
         <tli id="T543" time="280.3425454545455" type="appl" />
         <tli id="T544" time="280.8687272727273" type="appl" />
         <tli id="T545" time="281.3949090909091" type="appl" />
         <tli id="T546" time="281.92109090909094" type="appl" />
         <tli id="T547" time="282.4472727272727" type="appl" />
         <tli id="T548" time="282.97345454545456" type="appl" />
         <tli id="T549" time="283.4996363636364" type="appl" />
         <tli id="T550" time="284.0258181818182" type="appl" />
         <tli id="T551" time="284.552" type="appl" />
         <tli id="T552" time="285.099" type="appl" />
         <tli id="T553" time="285.53162499999996" type="appl" />
         <tli id="T554" time="285.96425" type="appl" />
         <tli id="T555" time="286.396875" type="appl" />
         <tli id="T556" time="286.8295" type="appl" />
         <tli id="T557" time="287.26212499999997" type="appl" />
         <tli id="T558" time="287.69475" type="appl" />
         <tli id="T559" time="288.12737500000003" type="appl" />
         <tli id="T927" time="288.3436875" type="intp" />
         <tli id="T560" time="288.56" type="appl" />
         <tli id="T561" time="288.84749999999997" type="appl" />
         <tli id="T562" time="289.135" type="appl" />
         <tli id="T928" time="289.226" type="intp" />
         <tli id="T563" time="289.317" type="appl" />
         <tli id="T564" time="290.017" type="appl" />
         <tli id="T565" time="290.717" type="appl" />
         <tli id="T566" time="291.41700000000003" type="appl" />
         <tli id="T567" time="292.117" type="appl" />
         <tli id="T568" time="293.346" type="appl" />
         <tli id="T569" time="293.812" type="appl" />
         <tli id="T570" time="294.278" type="appl" />
         <tli id="T571" time="294.744" type="appl" />
         <tli id="T572" time="295.21000000000004" type="appl" />
         <tli id="T573" time="295.676" type="appl" />
         <tli id="T574" time="296.142" type="appl" />
         <tli id="T575" time="296.608" type="appl" />
         <tli id="T576" time="297.074" type="appl" />
         <tli id="T577" time="297.54" type="appl" />
         <tli id="T578" time="298.012" type="appl" />
         <tli id="T579" time="298.552" type="appl" />
         <tli id="T580" time="299.399" type="appl" />
         <tli id="T581" time="300.06766666666664" type="appl" />
         <tli id="T582" time="300.73633333333333" type="appl" />
         <tli id="T583" time="301.405" type="appl" />
         <tli id="T584" time="301.975" type="appl" />
         <tli id="T585" time="302.79400000000004" type="appl" />
         <tli id="T586" time="303.613" type="appl" />
         <tli id="T587" time="304.432" type="appl" />
         <tli id="T588" time="305.251" type="appl" />
         <tli id="T589" time="306.07" type="appl" />
         <tli id="T590" time="306.258" type="appl" />
         <tli id="T591" time="306.73777777777775" type="appl" />
         <tli id="T592" time="307.2175555555555" type="appl" />
         <tli id="T593" time="307.69733333333335" type="appl" />
         <tli id="T594" time="308.1771111111111" type="appl" />
         <tli id="T595" time="308.6568888888889" type="appl" />
         <tli id="T596" time="309.13666666666666" type="appl" />
         <tli id="T597" time="309.6164444444445" type="appl" />
         <tli id="T598" time="310.09622222222225" type="appl" />
         <tli id="T599" time="310.576" type="appl" />
         <tli id="T600" time="311.38" type="appl" />
         <tli id="T601" time="311.94" type="appl" />
         <tli id="T602" time="312.5" type="appl" />
         <tli id="T603" time="313.06" type="appl" />
         <tli id="T604" time="315.717" type="appl" />
         <tli id="T605" time="316.22714285714284" type="appl" />
         <tli id="T606" time="316.7372857142857" type="appl" />
         <tli id="T607" time="317.24742857142854" type="appl" />
         <tli id="T608" time="317.75757142857145" type="appl" />
         <tli id="T609" time="318.2677142857143" type="appl" />
         <tli id="T610" time="318.77785714285716" type="appl" />
         <tli id="T611" time="319.288" type="appl" />
         <tli id="T612" time="319.405" type="appl" />
         <tli id="T613" time="319.8708333333333" type="appl" />
         <tli id="T614" time="320.33666666666664" type="appl" />
         <tli id="T615" time="320.8025" type="appl" />
         <tli id="T616" time="321.2683333333333" type="appl" />
         <tli id="T617" time="321.7341666666666" type="appl" />
         <tli id="T618" time="322.2" type="appl" />
         <tli id="T619" time="324.91" type="appl" />
         <tli id="T620" time="325.4996153846154" type="appl" />
         <tli id="T621" time="326.08923076923077" type="appl" />
         <tli id="T622" time="326.67884615384617" type="appl" />
         <tli id="T623" time="327.26846153846157" type="appl" />
         <tli id="T624" time="327.8580769230769" type="appl" />
         <tli id="T625" time="328.4476923076923" type="appl" />
         <tli id="T626" time="329.0373076923077" type="appl" />
         <tli id="T627" time="329.6269230769231" type="appl" />
         <tli id="T628" time="330.21653846153845" type="appl" />
         <tli id="T629" time="330.80615384615385" type="appl" />
         <tli id="T630" time="331.39576923076925" type="appl" />
         <tli id="T631" time="331.9853846153846" type="appl" />
         <tli id="T632" time="332.575" type="appl" />
         <tli id="T633" time="334.715" type="appl" />
         <tli id="T634" time="335.3016666666667" type="appl" />
         <tli id="T635" time="335.8883333333333" type="appl" />
         <tli id="T636" time="336.475" type="appl" />
         <tli id="T637" time="336.565" type="appl" />
         <tli id="T638" time="337.0125" type="appl" />
         <tli id="T639" time="337.46" type="appl" />
         <tli id="T640" time="337.90749999999997" type="appl" />
         <tli id="T641" time="338.355" type="appl" />
         <tli id="T642" time="338.8025" type="appl" />
         <tli id="T643" time="339.25" type="appl" />
         <tli id="T644" time="339.6975" type="appl" />
         <tli id="T645" time="340.145" type="appl" />
         <tli id="T646" time="340.147" type="appl" />
         <tli id="T647" time="340.647" type="appl" />
         <tli id="T648" time="341.147" type="appl" />
         <tli id="T649" time="341.647" type="appl" />
         <tli id="T650" time="342.147" type="appl" />
         <tli id="T651" time="342.647" type="appl" />
         <tli id="T652" time="343.147" type="appl" />
         <tli id="T653" time="343.647" type="appl" />
         <tli id="T654" time="344.147" type="appl" />
         <tli id="T655" time="344.647" type="appl" />
         <tli id="T656" time="345.147" type="appl" />
         <tli id="T657" time="347.185" type="appl" />
         <tli id="T658" time="347.755" type="appl" />
         <tli id="T659" time="348.325" type="appl" />
         <tli id="T660" time="348.895" type="appl" />
         <tli id="T661" time="349.46500000000003" type="appl" />
         <tli id="T662" time="350.035" type="appl" />
         <tli id="T663" time="350.605" type="appl" />
         <tli id="T664" time="351.175" type="appl" />
         <tli id="T665" time="352.2" type="appl" />
         <tli id="T666" time="352.64625" type="appl" />
         <tli id="T667" time="353.0925" type="appl" />
         <tli id="T668" time="353.53875" type="appl" />
         <tli id="T669" time="353.985" type="appl" />
         <tli id="T670" time="354.43125" type="appl" />
         <tli id="T671" time="354.8775" type="appl" />
         <tli id="T672" time="355.32375" type="appl" />
         <tli id="T673" time="355.77" type="appl" />
         <tli id="T674" time="356.21625" type="appl" />
         <tli id="T675" time="356.6625" type="appl" />
         <tli id="T676" time="357.10875" type="appl" />
         <tli id="T677" time="357.555" type="appl" />
         <tli id="T678" time="358.00124999999997" type="appl" />
         <tli id="T679" time="358.4475" type="appl" />
         <tli id="T680" time="358.89375" type="appl" />
         <tli id="T681" time="359.34" type="appl" />
         <tli id="T682" time="359.78625" type="appl" />
         <tli id="T683" time="360.2325" type="appl" />
         <tli id="T684" time="360.67875" type="appl" />
         <tli id="T685" time="361.125" type="appl" />
         <tli id="T686" time="361.975" type="appl" />
         <tli id="T687" time="362.45214285714286" type="appl" />
         <tli id="T688" time="362.92928571428575" type="appl" />
         <tli id="T689" time="363.4064285714286" type="appl" />
         <tli id="T690" time="363.88357142857143" type="appl" />
         <tli id="T691" time="364.36071428571427" type="appl" />
         <tli id="T692" time="364.83785714285716" type="appl" />
         <tli id="T693" time="365.315" type="appl" />
         <tli id="T694" time="365.46" type="appl" />
         <tli id="T695" time="366.0083333333333" type="appl" />
         <tli id="T696" time="366.5566666666667" type="appl" />
         <tli id="T697" time="367.105" type="appl" />
         <tli id="T698" time="367.6533333333333" type="appl" />
         <tli id="T699" time="368.20166666666665" type="appl" />
         <tli id="T700" time="368.75" type="appl" />
         <tli id="T701" time="369.165" type="appl" />
         <tli id="T702" time="369.624" type="appl" />
         <tli id="T703" time="370.083" type="appl" />
         <tli id="T704" time="370.542" type="appl" />
         <tli id="T705" time="371.001" type="appl" />
         <tli id="T706" time="371.46" type="appl" />
         <tli id="T707" time="371.75" type="appl" />
         <tli id="T708" time="372.375" type="appl" />
         <tli id="T709" time="373.0" type="appl" />
         <tli id="T710" time="373.625" type="appl" />
         <tli id="T711" time="374.335" type="appl" />
         <tli id="T712" time="374.7633333333333" type="appl" />
         <tli id="T713" time="375.19166666666666" type="appl" />
         <tli id="T714" time="375.62" type="appl" />
         <tli id="T715" time="376.895" type="appl" />
         <tli id="T716" time="377.37" type="appl" />
         <tli id="T717" time="377.84499999999997" type="appl" />
         <tli id="T718" time="378.32" type="appl" />
         <tli id="T719" time="378.795" type="appl" />
         <tli id="T720" time="379.27000000000004" type="appl" />
         <tli id="T721" time="379.745" type="appl" />
         <tli id="T722" time="380.22" type="appl" />
         <tli id="T723" time="382.13" type="appl" />
         <tli id="T724" time="382.72749999999996" type="appl" />
         <tli id="T725" time="383.325" type="appl" />
         <tli id="T930" time="383.33" type="intp" />
         <tli id="T726" time="383.335" type="appl" />
         <tli id="T727" time="383.633" type="appl" />
         <tli id="T728" time="383.931" type="appl" />
         <tli id="T729" time="384.229" type="appl" />
         <tli id="T730" time="384.527" type="appl" />
         <tli id="T731" time="384.825" type="appl" />
         <tli id="T929" time="384.86249999999995" type="intp" />
         <tli id="T732" time="384.9" type="appl" />
         <tli id="T733" time="385.4107142857143" type="appl" />
         <tli id="T734" time="385.9214285714286" type="appl" />
         <tli id="T735" time="386.4321428571429" type="appl" />
         <tli id="T736" time="386.9428571428571" type="appl" />
         <tli id="T737" time="387.4535714285714" type="appl" />
         <tli id="T738" time="387.9642857142857" type="appl" />
         <tli id="T739" time="388.475" type="appl" />
         <tli id="T740" time="388.765" type="appl" />
         <tli id="T741" time="390.145" type="appl" />
         <tli id="T742" time="390.595" type="appl" />
         <tli id="T743" time="391.035" type="appl" />
         <tli id="T744" time="391.475" type="appl" />
         <tli id="T745" time="391.915" type="appl" />
         <tli id="T746" time="392.355" type="appl" />
         <tli id="T747" time="392.795" type="appl" />
         <tli id="T748" time="393.235" type="appl" />
         <tli id="T749" time="394.215" type="appl" />
         <tli id="T750" time="395.405" type="appl" />
         <tli id="T751" time="395.44" type="appl" />
         <tli id="T752" time="396.07916666666665" type="appl" />
         <tli id="T753" time="396.7183333333333" type="appl" />
         <tli id="T754" time="397.35749999999996" type="appl" />
         <tli id="T755" time="397.99666666666667" type="appl" />
         <tli id="T756" time="398.6358333333333" type="appl" />
         <tli id="T757" time="399.275" type="appl" />
         <tli id="T758" time="399.9" type="appl" />
         <tli id="T759" time="400.74249999999995" type="appl" />
         <tli id="T760" time="401.585" type="appl" />
         <tli id="T761" time="401.785" type="appl" />
         <tli id="T762" time="402.0675" type="appl" />
         <tli id="T763" time="402.35" type="appl" />
         <tli id="T764" time="402.37" type="appl" />
         <tli id="T765" time="402.73833333333334" type="appl" />
         <tli id="T766" time="403.1066666666667" type="appl" />
         <tli id="T767" time="403.475" type="appl" />
         <tli id="T768" time="403.8433333333333" type="appl" />
         <tli id="T769" time="404.21166666666664" type="appl" />
         <tli id="T770" time="404.58" type="appl" />
         <tli id="T771" time="404.585" type="appl" />
         <tli id="T772" time="405.06916666666666" type="appl" />
         <tli id="T773" time="405.55333333333334" type="appl" />
         <tli id="T774" time="406.0375" type="appl" />
         <tli id="T775" time="406.52166666666665" type="appl" />
         <tli id="T776" time="407.0058333333333" type="appl" />
         <tli id="T777" time="407.49" type="appl" />
         <tli id="T778" time="407.88142857142856" type="appl" />
         <tli id="T779" time="408.27285714285716" type="appl" />
         <tli id="T780" time="408.6642857142857" type="appl" />
         <tli id="T781" time="409.0557142857143" type="appl" />
         <tli id="T782" time="409.44714285714286" type="appl" />
         <tli id="T783" time="409.83857142857147" type="appl" />
         <tli id="T784" time="410.23" type="appl" />
         <tli id="T785" time="410.755" type="appl" />
         <tli id="T786" time="411.27666666666664" type="appl" />
         <tli id="T787" time="411.79833333333335" type="appl" />
         <tli id="T788" time="412.32" type="appl" />
         <tli id="T789" time="416.225" type="appl" />
         <tli id="T790" time="416.785" type="appl" />
         <tli id="T791" time="416.8" type="appl" />
         <tli id="T792" time="417.38750000000005" type="appl" />
         <tli id="T793" time="417.975" type="appl" />
         <tli id="T794" time="418.19" type="appl" />
         <tli id="T795" time="418.985" type="appl" />
         <tli id="T796" time="419.78" type="appl" />
         <tli id="T797" time="420.025" type="appl" />
         <tli id="T798" time="420.52916666666664" type="appl" />
         <tli id="T799" time="421.0333333333333" type="appl" />
         <tli id="T800" time="421.5375" type="appl" />
         <tli id="T801" time="422.0416666666667" type="appl" />
         <tli id="T802" time="422.54583333333335" type="appl" />
         <tli id="T803" time="423.05" type="appl" />
         <tli id="T804" time="423.23" type="appl" />
         <tli id="T805" time="423.8283333333333" type="appl" />
         <tli id="T806" time="424.4266666666667" type="appl" />
         <tli id="T807" time="425.025" type="appl" />
         <tli id="T808" time="425.515" type="appl" />
         <tli id="T809" time="425.985" type="appl" />
         <tli id="T810" time="426.455" type="appl" />
         <tli id="T811" time="426.925" type="appl" />
         <tli id="T812" time="427.395" type="appl" />
         <tli id="T813" time="427.865" type="appl" />
         <tli id="T814" time="428.335" type="appl" />
         <tli id="T815" time="428.805" type="appl" />
         <tli id="T816" time="430.17" type="appl" />
         <tli id="T817" time="430.84000000000003" type="appl" />
         <tli id="T818" time="431.51" type="appl" />
         <tli id="T819" time="432.18" type="appl" />
         <tli id="T820" time="432.84999999999997" type="appl" />
         <tli id="T821" time="433.52" type="appl" />
         <tli id="T822" time="433.91" type="appl" />
         <tli id="T823" time="434.3421428571429" type="appl" />
         <tli id="T824" time="434.7742857142857" type="appl" />
         <tli id="T825" time="435.2064285714286" type="appl" />
         <tli id="T826" time="435.6385714285714" type="appl" />
         <tli id="T827" time="436.0707142857143" type="appl" />
         <tli id="T828" time="436.5028571428571" type="appl" />
         <tli id="T829" time="436.935" type="appl" />
         <tli id="T830" time="437.045" type="appl" />
         <tli id="T831" time="437.46083333333337" type="appl" />
         <tli id="T832" time="437.87666666666667" type="appl" />
         <tli id="T833" time="438.2925" type="appl" />
         <tli id="T834" time="438.70833333333337" type="appl" />
         <tli id="T835" time="439.12416666666667" type="appl" />
         <tli id="T836" time="439.54" type="appl" />
         <tli id="T837" time="440.25" type="appl" />
         <tli id="T838" time="440.64666666666665" type="appl" />
         <tli id="T839" time="441.04333333333335" type="appl" />
         <tli id="T840" time="441.44" type="appl" />
         <tli id="T841" time="441.83666666666664" type="appl" />
         <tli id="T842" time="442.23333333333335" type="appl" />
         <tli id="T843" time="442.63" type="appl" />
         <tli id="T844" time="443.28" type="appl" />
         <tli id="T845" time="443.625" type="appl" />
         <tli id="T846" time="443.96999999999997" type="appl" />
         <tli id="T847" time="444.315" type="appl" />
         <tli id="T848" time="444.66" type="appl" />
         <tli id="T849" time="445.005" type="appl" />
         <tli id="T850" time="445.35" type="appl" />
         <tli id="T851" time="445.995" type="appl" />
         <tli id="T852" time="446.375" type="appl" />
         <tli id="T853" time="446.755" type="appl" />
         <tli id="T854" time="447.135" type="appl" />
         <tli id="T855" time="447.515" type="appl" />
         <tli id="T856" time="447.895" type="appl" />
         <tli id="T857" time="448.275" type="appl" />
         <tli id="T858" time="448.604" type="appl" />
         <tli id="T859" time="448.989" type="appl" />
         <tli id="T860" time="449.374" type="appl" />
         <tli id="T861" time="449.759" type="appl" />
         <tli id="T862" time="450.144" type="appl" />
         <tli id="T863" time="450.36" type="appl" />
         <tli id="T864" time="450.72333333333336" type="appl" />
         <tli id="T865" time="451.0866666666667" type="appl" />
         <tli id="T866" time="451.45000000000005" type="appl" />
         <tli id="T867" time="451.81333333333333" type="appl" />
         <tli id="T868" time="452.1766666666667" type="appl" />
         <tli id="T869" time="452.54" type="appl" />
         <tli id="T870" time="456.388" type="appl" />
         <tli id="T871" time="456.8351111111111" type="appl" />
         <tli id="T872" time="457.2822222222222" type="appl" />
         <tli id="T873" time="457.72933333333333" type="appl" />
         <tli id="T874" time="458.1764444444444" type="appl" />
         <tli id="T875" time="458.6235555555555" type="appl" />
         <tli id="T876" time="459.0706666666666" type="appl" />
         <tli id="T877" time="459.5177777777778" type="appl" />
         <tli id="T878" time="459.9648888888889" type="appl" />
         <tli id="T879" time="460.412" type="appl" />
         <tli id="T880" time="460.432" type="appl" />
         <tli id="T881" time="460.8272" type="appl" />
         <tli id="T882" time="461.2224" type="appl" />
         <tli id="T883" time="461.61760000000004" type="appl" />
         <tli id="T884" time="462.0128" type="appl" />
         <tli id="T885" time="462.408" type="appl" />
         <tli id="T886" time="465.192" type="appl" />
         <tli id="T887" time="465.476" type="appl" />
         <tli id="T888" time="465.76" type="appl" />
         <tli id="T889" time="466.044" type="appl" />
         <tli id="T890" time="466.328" type="appl" />
         <tli id="T891" time="466.772" type="appl" />
         <tli id="T892" time="467.216" type="appl" />
         <tli id="T931" time="467.668" type="intp" />
         <tli id="T893" time="468.12" type="appl" />
         <tli id="T894" time="468.484" type="appl" />
         <tli id="T895" time="468.848" type="appl" />
         <tli id="T896" time="469.212" type="appl" />
         <tli id="T932" time="469.428" type="intp" />
         <tli id="T897" time="469.644" type="appl" />
         <tli id="T898" time="470.0993846153846" type="appl" />
         <tli id="T899" time="470.55476923076924" type="appl" />
         <tli id="T900" time="471.01015384615386" type="appl" />
         <tli id="T901" time="471.4655384615385" type="appl" />
         <tli id="T902" time="471.9209230769231" type="appl" />
         <tli id="T903" time="472.3763076923077" type="appl" />
         <tli id="T904" time="472.8316923076923" type="appl" />
         <tli id="T905" time="473.28707692307694" type="appl" />
         <tli id="T906" time="473.74246153846155" type="appl" />
         <tli id="T907" time="474.19784615384617" type="appl" />
         <tli id="T908" time="474.6532307692308" type="appl" />
         <tli id="T909" time="475.1086153846154" type="appl" />
         <tli id="T910" time="475.564" type="appl" />
         <tli id="T911" time="476.74" type="appl" />
         <tli id="T912" time="477.2512" type="appl" />
         <tli id="T913" time="477.7624" type="appl" />
         <tli id="T914" time="478.2736" type="appl" />
         <tli id="T915" time="478.7848" type="appl" />
         <tli id="T916" time="479.296" type="appl" />
         <tli id="T917" time="479.78" type="appl" />
         <tli id="T918" time="480.116" type="appl" />
         <tli id="T919" time="480.452" type="appl" />
         <tli id="T920" time="480.788" type="appl" />
         <tli id="T0" time="480.9" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="NEP"
                      type="t">
         <timeline-fork end="T5" start="T1">
            <tli id="T1.tx.1" />
            <tli id="T1.tx.2" />
            <tli id="T1.tx.3" />
            <tli id="T1.tx.4" />
         </timeline-fork>
         <timeline-fork end="T9" start="T6">
            <tli id="T6.tx.1" />
            <tli id="T6.tx.2" />
         </timeline-fork>
         <timeline-fork end="T531" start="T530">
            <tli id="T530.tx.1" />
         </timeline-fork>
         <timeline-fork end="T741" start="T740">
            <tli id="T740.tx.1" />
         </timeline-fork>
         <timeline-fork end="T890" start="T886">
            <tli id="T886.tx.1" />
            <tli id="T886.tx.2" />
            <tli id="T886.tx.3" />
            <tli id="T886.tx.4" />
         </timeline-fork>
         <timeline-fork end="T892" start="T890">
            <tli id="T890.tx.1" />
            <tli id="T890.tx.2" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T5" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <nts id="Seg_3" n="HIAT:ip">(</nts>
                  <nts id="Seg_4" n="HIAT:ip">(</nts>
                  <ats e="T1.tx.1" id="Seg_5" n="HIAT:non-pho" s="T1">KuAI:</ats>
                  <nts id="Seg_6" n="HIAT:ip">)</nts>
                  <nts id="Seg_7" n="HIAT:ip">)</nts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1.tx.2" id="Seg_10" n="HIAT:w" s="T1.tx.1">Сказка</ts>
                  <nts id="Seg_11" n="HIAT:ip">,</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1.tx.3" id="Seg_14" n="HIAT:w" s="T1.tx.2">записанная</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1.tx.4" id="Seg_17" n="HIAT:w" s="T1.tx.3">Елизаветой</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_20" n="HIAT:w" s="T1.tx.4">Павловной</ts>
                  <nts id="Seg_21" n="HIAT:ip">.</nts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T9" id="Seg_23" n="sc" s="T6">
               <ts e="T9" id="Seg_25" n="HIAT:u" s="T6">
                  <ts e="T6.tx.1" id="Seg_27" n="HIAT:w" s="T6">Ну</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6.tx.2" id="Seg_30" n="HIAT:w" s="T6.tx.1">пожалуйста</ts>
                  <nts id="Seg_31" n="HIAT:ip">,</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T6.tx.2">говорите</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T15" id="Seg_37" n="sc" s="T921">
               <ts e="T15" id="Seg_39" n="HIAT:u" s="T921">
                  <nts id="Seg_40" n="HIAT:ip">(</nts>
                  <nts id="Seg_41" n="HIAT:ip">(</nts>
                  <ats e="T10" id="Seg_42" n="HIAT:non-pho" s="T921">NN:</ats>
                  <nts id="Seg_43" n="HIAT:ip">)</nts>
                  <nts id="Seg_44" n="HIAT:ip">)</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_47" n="HIAT:w" s="T10">Ksa</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_50" n="HIAT:w" s="T11">kətät</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_53" n="HIAT:w" s="T12">nɔːt</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_55" n="HIAT:ip">(</nts>
                  <ts e="T14" id="Seg_57" n="HIAT:w" s="T13">jarɨk</ts>
                  <nts id="Seg_58" n="HIAT:ip">)</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_61" n="HIAT:w" s="T14">qaj</ts>
                  <nts id="Seg_62" n="HIAT:ip">.</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T23" id="Seg_64" n="sc" s="T922">
               <ts e="T23" id="Seg_66" n="HIAT:u" s="T922">
                  <nts id="Seg_67" n="HIAT:ip">(</nts>
                  <nts id="Seg_68" n="HIAT:ip">(</nts>
                  <ats e="T16" id="Seg_69" n="HIAT:non-pho" s="T922">NEP:</ats>
                  <nts id="Seg_70" n="HIAT:ip">)</nts>
                  <nts id="Seg_71" n="HIAT:ip">)</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_74" n="HIAT:w" s="T16">Mɨ</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_77" n="HIAT:w" s="T17">tına</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_80" n="HIAT:w" s="T18">Nʼomalʼ</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_83" n="HIAT:w" s="T19">porqɨ</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_86" n="HIAT:w" s="T20">ira</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_89" n="HIAT:w" s="T21">qos</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_92" n="HIAT:w" s="T22">kətɨšak</ts>
                  <nts id="Seg_93" n="HIAT:ip">.</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T32" id="Seg_95" n="sc" s="T24">
               <ts e="T32" id="Seg_97" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_99" n="HIAT:w" s="T24">Nʼomalʼ</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_102" n="HIAT:w" s="T25">porqɨ</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_105" n="HIAT:w" s="T26">ira</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_108" n="HIAT:w" s="T27">tına</ts>
                  <nts id="Seg_109" n="HIAT:ip">,</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_112" n="HIAT:w" s="T28">isɛčʼar</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_115" n="HIAT:w" s="T29">selʼčʼɨ</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_118" n="HIAT:w" s="T30">ijatɨ</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_121" n="HIAT:w" s="T31">ɛppɨntɨ</ts>
                  <nts id="Seg_122" n="HIAT:ip">.</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T36" id="Seg_124" n="sc" s="T33">
               <ts e="T36" id="Seg_126" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_128" n="HIAT:w" s="T33">Aj</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_131" n="HIAT:w" s="T34">selʼčʼi</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_134" n="HIAT:w" s="T35">ämnätɨ</ts>
                  <nts id="Seg_135" n="HIAT:ip">.</nts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T39" id="Seg_137" n="sc" s="T37">
               <ts e="T39" id="Seg_139" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_141" n="HIAT:w" s="T37">Qarɨt</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_144" n="HIAT:w" s="T38">tüŋɔːtɨt</ts>
                  <nts id="Seg_145" n="HIAT:ip">.</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T46" id="Seg_147" n="sc" s="T40">
               <ts e="T46" id="Seg_149" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_151" n="HIAT:w" s="T40">Qälɨt</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_154" n="HIAT:w" s="T41">takkɨn</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_157" n="HIAT:w" s="T42">nʼenna</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_160" n="HIAT:w" s="T43">tüŋɔːtɨn</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_163" n="HIAT:w" s="T44">təpɨp</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_166" n="HIAT:w" s="T45">qətqɨntoːqo</ts>
                  <nts id="Seg_167" n="HIAT:ip">.</nts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T52" id="Seg_169" n="sc" s="T47">
               <ts e="T52" id="Seg_171" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_173" n="HIAT:w" s="T47">Qumiːtɨ</ts>
                  <nts id="Seg_174" n="HIAT:ip">,</nts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_177" n="HIAT:w" s="T48">ijaiːtɨ</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_180" n="HIAT:w" s="T49">mačʼa</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_183" n="HIAT:w" s="T50">qənpɔːtɨt</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_186" n="HIAT:w" s="T51">täpällä</ts>
                  <nts id="Seg_187" n="HIAT:ip">.</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T64" id="Seg_189" n="sc" s="T53">
               <ts e="T64" id="Seg_191" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_193" n="HIAT:w" s="T53">Täpällä</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_196" n="HIAT:w" s="T54">mačʼo</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_199" n="HIAT:w" s="T55">qənpɔːt</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_202" n="HIAT:w" s="T56">tıː</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_205" n="HIAT:w" s="T57">qup</ts>
                  <nts id="Seg_206" n="HIAT:ip">,</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_209" n="HIAT:w" s="T58">tıː</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_212" n="HIAT:w" s="T59">kət</ts>
                  <nts id="Seg_213" n="HIAT:ip">,</nts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_216" n="HIAT:w" s="T60">tiː</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_219" n="HIAT:w" s="T61">kuttar</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_222" n="HIAT:w" s="T62">ɛkka</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_225" n="HIAT:w" s="T63">kət</ts>
                  <nts id="Seg_226" n="HIAT:ip">.</nts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T72" id="Seg_228" n="sc" s="T65">
               <ts e="T72" id="Seg_230" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_232" n="HIAT:w" s="T65">Tına</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_235" n="HIAT:w" s="T66">ijap</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_238" n="HIAT:w" s="T67">šʼolqumɨj</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_241" n="HIAT:w" s="T68">ijap</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_244" n="HIAT:w" s="T69">wərɨmmɨntɨ</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_247" n="HIAT:w" s="T70">tına</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_250" n="HIAT:w" s="T71">qälɨk</ts>
                  <nts id="Seg_251" n="HIAT:ip">.</nts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T75" id="Seg_253" n="sc" s="T73">
               <ts e="T75" id="Seg_255" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_257" n="HIAT:w" s="T73">Na</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_260" n="HIAT:w" s="T74">mütɨntɨt</ts>
                  <nts id="Seg_261" n="HIAT:ip">.</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T90" id="Seg_263" n="sc" s="T76">
               <ts e="T90" id="Seg_265" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_267" n="HIAT:w" s="T76">Nʼomalʼ</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_270" n="HIAT:w" s="T77">porqɨ</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_273" n="HIAT:w" s="T78">ira</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_276" n="HIAT:w" s="T79">mɨta</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_279" n="HIAT:w" s="T80">täːlɨ</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_282" n="HIAT:w" s="T81">mɨta</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_285" n="HIAT:w" s="T82">meː</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_288" n="HIAT:w" s="T83">tüntɔːmɨt</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_291" n="HIAT:w" s="T84">təpɨn</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_294" n="HIAT:w" s="T85">mɔːttɨ</ts>
                  <nts id="Seg_295" n="HIAT:ip">,</nts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_298" n="HIAT:w" s="T86">tɛː</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_301" n="HIAT:w" s="T87">mɨta</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_304" n="HIAT:w" s="T88">qonniŋɨlɨt</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_307" n="HIAT:w" s="T89">toː</ts>
                  <nts id="Seg_308" n="HIAT:ip">.</nts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T100" id="Seg_310" n="sc" s="T91">
               <ts e="T100" id="Seg_312" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_314" n="HIAT:w" s="T91">Ijaqı</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_317" n="HIAT:w" s="T92">moqonä</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_320" n="HIAT:w" s="T93">qəllä</ts>
                  <nts id="Seg_321" n="HIAT:ip">,</nts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_324" n="HIAT:w" s="T94">nık</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_327" n="HIAT:w" s="T95">kətɨŋɨtɨ</ts>
                  <nts id="Seg_328" n="HIAT:ip">,</nts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_331" n="HIAT:w" s="T96">čʼilʼalʼ</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_334" n="HIAT:w" s="T97">ijaqı</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_337" n="HIAT:w" s="T98">qälɨk</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_340" n="HIAT:w" s="T99">mərɨmɨntɨj</ts>
                  <nts id="Seg_341" n="HIAT:ip">.</nts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T104" id="Seg_343" n="sc" s="T101">
               <ts e="T104" id="Seg_345" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_347" n="HIAT:w" s="T101">Tam</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_350" n="HIAT:w" s="T102">šölʼqumɨj</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_353" n="HIAT:w" s="T103">ijaqı</ts>
                  <nts id="Seg_354" n="HIAT:ip">…</nts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T112" id="Seg_356" n="sc" s="T105">
               <ts e="T112" id="Seg_358" n="HIAT:u" s="T105">
                  <ts e="T106" id="Seg_360" n="HIAT:w" s="T105">Taŋaltäš</ts>
                  <nts id="Seg_361" n="HIAT:ip">,</nts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_364" n="HIAT:w" s="T106">taŋaltäš</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_367" n="HIAT:w" s="T107">lʼi</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_370" n="HIAT:w" s="T108">qaj</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_373" n="HIAT:w" s="T109">kanap</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_376" n="HIAT:w" s="T110">qaj</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_378" n="HIAT:ip">(</nts>
                  <ts e="T112" id="Seg_380" n="HIAT:w" s="T111">čʼütalʼalta</ts>
                  <nts id="Seg_381" n="HIAT:ip">)</nts>
                  <nts id="Seg_382" n="HIAT:ip">.</nts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T114" id="Seg_384" n="sc" s="T113">
               <ts e="T114" id="Seg_386" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_388" n="HIAT:w" s="T113">Tal</ts>
                  <nts id="Seg_389" n="HIAT:ip">.</nts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T120" id="Seg_391" n="sc" s="T115">
               <ts e="T120" id="Seg_393" n="HIAT:u" s="T115">
                  <ts e="T116" id="Seg_395" n="HIAT:w" s="T115">Nɨnɨ</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_398" n="HIAT:w" s="T116">šittäl</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_401" n="HIAT:w" s="T117">ira</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_404" n="HIAT:w" s="T118">kučʼčʼa</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_407" n="HIAT:w" s="T119">qattɛnta</ts>
                  <nts id="Seg_408" n="HIAT:ip">?</nts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T126" id="Seg_410" n="sc" s="T121">
               <ts e="T126" id="Seg_412" n="HIAT:u" s="T121">
                  <ts e="T122" id="Seg_414" n="HIAT:w" s="T121">Ira</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_417" n="HIAT:w" s="T122">qapija</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_420" n="HIAT:w" s="T123">kekkɨsa</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_423" n="HIAT:w" s="T124">na</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_426" n="HIAT:w" s="T125">ippɨnta</ts>
                  <nts id="Seg_427" n="HIAT:ip">.</nts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T135" id="Seg_429" n="sc" s="T127">
               <ts e="T135" id="Seg_431" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_433" n="HIAT:w" s="T127">Ira</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_436" n="HIAT:w" s="T128">nılʼčʼiŋ</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_439" n="HIAT:w" s="T129">na</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_442" n="HIAT:w" s="T130">ɛsɨsɨ</ts>
                  <nts id="Seg_443" n="HIAT:ip">,</nts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_446" n="HIAT:w" s="T131">na</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_449" n="HIAT:w" s="T132">Nʼomalʼ</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_452" n="HIAT:w" s="T133">porqɨlʼ</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_455" n="HIAT:w" s="T134">ira</ts>
                  <nts id="Seg_456" n="HIAT:ip">.</nts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T143" id="Seg_458" n="sc" s="T136">
               <ts e="T143" id="Seg_460" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_462" n="HIAT:w" s="T136">Nılʼčʼik</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_465" n="HIAT:w" s="T137">ɛsa</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_468" n="HIAT:w" s="T138">amnäntɨlʼ</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_471" n="HIAT:w" s="T139">mɨtkine</ts>
                  <nts id="Seg_472" n="HIAT:ip">:</nts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_474" n="HIAT:ip">“</nts>
                  <ts e="T141" id="Seg_476" n="HIAT:w" s="T140">Apsɨp</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_479" n="HIAT:w" s="T141">tattɨŋɨt</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_482" n="HIAT:w" s="T142">karrä</ts>
                  <nts id="Seg_483" n="HIAT:ip">.</nts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T154" id="Seg_485" n="sc" s="T144">
               <ts e="T154" id="Seg_487" n="HIAT:u" s="T144">
                  <ts e="T145" id="Seg_489" n="HIAT:w" s="T144">Ür</ts>
                  <nts id="Seg_490" n="HIAT:ip">,</nts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_493" n="HIAT:w" s="T145">apsɨp</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_496" n="HIAT:w" s="T146">tattɨŋɨt</ts>
                  <nts id="Seg_497" n="HIAT:ip">,</nts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_500" n="HIAT:w" s="T147">kuššalʼ</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_503" n="HIAT:w" s="T148">tɛlɨlʼ</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_506" n="HIAT:w" s="T149">apsɨp</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_509" n="HIAT:w" s="T150">kun</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_512" n="HIAT:w" s="T151">montɨ</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_515" n="HIAT:w" s="T152">qaj</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_518" n="HIAT:w" s="T153">tattɨŋɨt</ts>
                  <nts id="Seg_519" n="HIAT:ip">.</nts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T159" id="Seg_521" n="sc" s="T155">
               <ts e="T159" id="Seg_523" n="HIAT:u" s="T155">
                  <ts e="T156" id="Seg_525" n="HIAT:w" s="T155">Qälɨt</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_528" n="HIAT:w" s="T156">täːlɨ</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_531" n="HIAT:w" s="T157">tüntɔːtɨt</ts>
                  <nts id="Seg_532" n="HIAT:ip">,</nts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_535" n="HIAT:w" s="T158">amɨrtɛntɔːtɨt</ts>
                  <nts id="Seg_536" n="HIAT:ip">”</nts>
                  <nts id="Seg_537" n="HIAT:ip">.</nts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T163" id="Seg_539" n="sc" s="T160">
               <ts e="T163" id="Seg_541" n="HIAT:u" s="T160">
                  <ts e="T161" id="Seg_543" n="HIAT:w" s="T160">Ira</ts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_546" n="HIAT:w" s="T161">nık</ts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_549" n="HIAT:w" s="T162">kətɨŋɨt</ts>
                  <nts id="Seg_550" n="HIAT:ip">.</nts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T166" id="Seg_552" n="sc" s="T164">
               <ts e="T166" id="Seg_554" n="HIAT:u" s="T164">
                  <ts e="T165" id="Seg_556" n="HIAT:w" s="T164">Apsɨp</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_559" n="HIAT:w" s="T165">tattɔːtɨt</ts>
                  <nts id="Seg_560" n="HIAT:ip">.</nts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T177" id="Seg_562" n="sc" s="T167">
               <ts e="T177" id="Seg_564" n="HIAT:u" s="T167">
                  <ts e="T168" id="Seg_566" n="HIAT:w" s="T167">Tıntena</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_569" n="HIAT:w" s="T168">tap</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_572" n="HIAT:w" s="T169">nʼuːtɨsa</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_575" n="HIAT:w" s="T170">tap</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_578" n="HIAT:w" s="T171">mɔːt</ts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_581" n="HIAT:w" s="T172">šünʼčʼɨ</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_584" n="HIAT:w" s="T173">šentɨ</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_587" n="HIAT:w" s="T174">nʼuːtɨsa</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_590" n="HIAT:w" s="T175">tɔːqqɔːtɨt</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_593" n="HIAT:w" s="T176">tına</ts>
                  <nts id="Seg_594" n="HIAT:ip">.</nts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T181" id="Seg_596" n="sc" s="T178">
               <ts e="T181" id="Seg_598" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_600" n="HIAT:w" s="T178">Ira</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_603" n="HIAT:w" s="T179">qälim</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_606" n="HIAT:w" s="T180">apsɨtɨqolamnɨt</ts>
                  <nts id="Seg_607" n="HIAT:ip">.</nts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T191" id="Seg_609" n="sc" s="T182">
               <ts e="T191" id="Seg_611" n="HIAT:u" s="T182">
                  <ts e="T183" id="Seg_613" n="HIAT:w" s="T182">Qəə</ts>
                  <nts id="Seg_614" n="HIAT:ip">,</nts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_617" n="HIAT:w" s="T183">toptɨlʼ</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_620" n="HIAT:w" s="T184">čʼeːlɨ</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_623" n="HIAT:w" s="T185">qälɨn</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_625" n="HIAT:ip">(</nts>
                  <ts e="T187" id="Seg_627" n="HIAT:w" s="T186">na</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_630" n="HIAT:w" s="T187">jap</ts>
                  <nts id="Seg_631" n="HIAT:ip">)</nts>
                  <nts id="Seg_632" n="HIAT:ip">,</nts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_635" n="HIAT:w" s="T188">nɔːssarɨlʼ</ts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_638" n="HIAT:w" s="T189">qumɨlʼ</ts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_641" n="HIAT:w" s="T190">müːtɨ</ts>
                  <nts id="Seg_642" n="HIAT:ip">.</nts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T202" id="Seg_644" n="sc" s="T192">
               <ts e="T202" id="Seg_646" n="HIAT:u" s="T192">
                  <ts e="T193" id="Seg_648" n="HIAT:w" s="T192">Tomorot</ts>
                  <nts id="Seg_649" n="HIAT:ip">,</nts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_652" n="HIAT:w" s="T193">qapı</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_655" n="HIAT:w" s="T194">šünʼčʼipɨlʼ</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_658" n="HIAT:w" s="T195">mɔːtqɨt</ts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_661" n="HIAT:w" s="T196">Nʼomalʼ</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_664" n="HIAT:w" s="T197">porqɨ</ts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_667" n="HIAT:w" s="T198">ira</ts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_670" n="HIAT:w" s="T199">ontɨ</ts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_673" n="HIAT:w" s="T200">qala</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_676" n="HIAT:w" s="T201">ämnäiːntɨsa</ts>
                  <nts id="Seg_677" n="HIAT:ip">.</nts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T206" id="Seg_679" n="sc" s="T203">
               <ts e="T206" id="Seg_681" n="HIAT:u" s="T203">
                  <ts e="T204" id="Seg_683" n="HIAT:w" s="T203">Selʼčʼi</ts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_686" n="HIAT:w" s="T204">nʼuːtɨsa</ts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_689" n="HIAT:w" s="T205">tɔːqqɨraltɨŋɨt</ts>
                  <nts id="Seg_690" n="HIAT:ip">.</nts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T210" id="Seg_692" n="sc" s="T207">
               <ts e="T210" id="Seg_694" n="HIAT:u" s="T207">
                  <ts e="T208" id="Seg_696" n="HIAT:w" s="T207">Ira</ts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_699" n="HIAT:w" s="T208">nılʼčʼik</ts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_702" n="HIAT:w" s="T209">ɛsa</ts>
                  <nts id="Seg_703" n="HIAT:ip">.</nts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T218" id="Seg_705" n="sc" s="T211">
               <ts e="T218" id="Seg_707" n="HIAT:u" s="T211">
                  <ts e="T212" id="Seg_709" n="HIAT:w" s="T211">Mɔːt</ts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_712" n="HIAT:w" s="T212">jap</ts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_715" n="HIAT:w" s="T213">tultɔːt</ts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_718" n="HIAT:w" s="T214">qapija</ts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_721" n="HIAT:w" s="T215">tına</ts>
                  <nts id="Seg_722" n="HIAT:ip">,</nts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_725" n="HIAT:w" s="T216">na</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_728" n="HIAT:w" s="T217">qälɨt</ts>
                  <nts id="Seg_729" n="HIAT:ip">.</nts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T220" id="Seg_731" n="sc" s="T219">
               <ts e="T220" id="Seg_733" n="HIAT:u" s="T219">
                  <ts e="T220" id="Seg_735" n="HIAT:w" s="T219">Amɨrqolamnɔːt</ts>
                  <nts id="Seg_736" n="HIAT:ip">.</nts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T223" id="Seg_738" n="sc" s="T221">
               <ts e="T223" id="Seg_740" n="HIAT:u" s="T221">
                  <ts e="T222" id="Seg_742" n="HIAT:w" s="T221">Apsɨ</ts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_745" n="HIAT:w" s="T222">qontɔːt</ts>
                  <nts id="Seg_746" n="HIAT:ip">.</nts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T240" id="Seg_748" n="sc" s="T224">
               <ts e="T240" id="Seg_750" n="HIAT:u" s="T224">
                  <ts e="T225" id="Seg_752" n="HIAT:w" s="T224">Ämnäiːtɨ</ts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_755" n="HIAT:w" s="T225">čʼoj</ts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_758" n="HIAT:w" s="T226">tap</ts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_761" n="HIAT:w" s="T227">mušɨrɔːlnɔːt</ts>
                  <nts id="Seg_762" n="HIAT:ip">,</nts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_765" n="HIAT:w" s="T228">karrʼat</ts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_768" n="HIAT:w" s="T229">kun</ts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_771" n="HIAT:w" s="T230">mɔːntɨlʼ</ts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_774" n="HIAT:w" s="T231">apsɨ</ts>
                  <nts id="Seg_775" n="HIAT:ip">,</nts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_778" n="HIAT:w" s="T232">ür</ts>
                  <nts id="Seg_779" n="HIAT:ip">,</nts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_782" n="HIAT:w" s="T233">ür</ts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_785" n="HIAT:w" s="T234">kun</ts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_788" n="HIAT:w" s="T235">mɔːntɨlʼ</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_791" n="HIAT:w" s="T236">qaj</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_794" n="HIAT:w" s="T237">tına</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_797" n="HIAT:w" s="T238">təːkɨmpɨlʼ</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_800" n="HIAT:w" s="T239">apsɨ</ts>
                  <nts id="Seg_801" n="HIAT:ip">.</nts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T251" id="Seg_803" n="sc" s="T241">
               <ts e="T251" id="Seg_805" n="HIAT:u" s="T241">
                  <ts e="T242" id="Seg_807" n="HIAT:w" s="T241">Ira</ts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_810" n="HIAT:w" s="T242">nık</ts>
                  <nts id="Seg_811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_813" n="HIAT:w" s="T243">kətɨŋɨtɨ</ts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_816" n="HIAT:w" s="T244">imaqotantɨ</ts>
                  <nts id="Seg_817" n="HIAT:ip">,</nts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_820" n="HIAT:w" s="T245">Nʼomalʼ</ts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_823" n="HIAT:w" s="T246">porqɨ</ts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_826" n="HIAT:w" s="T247">ira</ts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_829" n="HIAT:w" s="T248">imaqotatɨ</ts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_832" n="HIAT:w" s="T249">ɛj</ts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_835" n="HIAT:w" s="T250">ɛːppa</ts>
                  <nts id="Seg_836" n="HIAT:ip">.</nts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T253" id="Seg_838" n="sc" s="T252">
               <ts e="T253" id="Seg_840" n="HIAT:u" s="T252">
                  <ts e="T253" id="Seg_842" n="HIAT:w" s="T252">Tal</ts>
                  <nts id="Seg_843" n="HIAT:ip">.</nts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T260" id="Seg_845" n="sc" s="T254">
               <ts e="T260" id="Seg_847" n="HIAT:u" s="T254">
                  <ts e="T255" id="Seg_849" n="HIAT:w" s="T254">Ämnäiːmtɨ</ts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_852" n="HIAT:w" s="T255">nık</ts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_855" n="HIAT:w" s="T256">kuraltɨŋɨt</ts>
                  <nts id="Seg_856" n="HIAT:ip">:</nts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_858" n="HIAT:ip">“</nts>
                  <ts e="T258" id="Seg_860" n="HIAT:w" s="T257">Tɛː</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_863" n="HIAT:w" s="T258">ponä</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_866" n="HIAT:w" s="T259">tantɨŋɨl</ts>
                  <nts id="Seg_867" n="HIAT:ip">.</nts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T265" id="Seg_869" n="sc" s="T261">
               <ts e="T265" id="Seg_871" n="HIAT:u" s="T261">
                  <ts e="T262" id="Seg_873" n="HIAT:w" s="T261">Ijat</ts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_876" n="HIAT:w" s="T262">kuti</ts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_879" n="HIAT:w" s="T263">ponä</ts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_882" n="HIAT:w" s="T264">tattätɨ</ts>
                  <nts id="Seg_883" n="HIAT:ip">.</nts>
                  <nts id="Seg_884" n="HIAT:ip">”</nts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T271" id="Seg_886" n="sc" s="T266">
               <ts e="T271" id="Seg_888" n="HIAT:u" s="T266">
                  <ts e="T267" id="Seg_890" n="HIAT:w" s="T266">Ira</ts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_893" n="HIAT:w" s="T267">kuntɨ</ts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_896" n="HIAT:w" s="T268">wərka</ts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_899" n="HIAT:w" s="T269">qumiːmtɨ</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_902" n="HIAT:w" s="T270">apsɨtɨmpɨla</ts>
                  <nts id="Seg_903" n="HIAT:ip">.</nts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T279" id="Seg_905" n="sc" s="T272">
               <ts e="T279" id="Seg_907" n="HIAT:u" s="T272">
                  <ts e="T273" id="Seg_909" n="HIAT:w" s="T272">Ukkɨr</ts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_912" n="HIAT:w" s="T273">tät</ts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_915" n="HIAT:w" s="T274">čʼontot</ts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_918" n="HIAT:w" s="T275">tümpɨj</ts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_921" n="HIAT:w" s="T276">karrʼat</ts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_924" n="HIAT:w" s="T277">qumɨn</ts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_927" n="HIAT:w" s="T278">amɨrnɔːtɨt</ts>
                  <nts id="Seg_928" n="HIAT:ip">.</nts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T296" id="Seg_930" n="sc" s="T280">
               <ts e="T288" id="Seg_932" n="HIAT:u" s="T280">
                  <ts e="T281" id="Seg_934" n="HIAT:w" s="T280">Karrʼa</ts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_937" n="HIAT:w" s="T281">na</ts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_940" n="HIAT:w" s="T282">šoːqɨrɨj</ts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_943" n="HIAT:w" s="T283">tüntɨ</ts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_946" n="HIAT:w" s="T284">nannɛrɨk</ts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_949" n="HIAT:w" s="T285">poːsä</ts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_951" n="HIAT:ip">(</nts>
                  <ts e="T287" id="Seg_953" n="HIAT:w" s="T286">sälɨŋɨtɨ</ts>
                  <nts id="Seg_954" n="HIAT:ip">)</nts>
                  <nts id="Seg_955" n="HIAT:ip">,</nts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_958" n="HIAT:w" s="T287">čʼɔːtɨŋɨtɨ</ts>
                  <nts id="Seg_959" n="HIAT:ip">.</nts>
                  <nts id="Seg_960" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T296" id="Seg_962" n="HIAT:u" s="T288">
                  <ts e="T289" id="Seg_964" n="HIAT:w" s="T288">Ämnäiːmtɨ</ts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_967" n="HIAT:w" s="T289">ašša</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_970" n="HIAT:w" s="T290">qaː</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_973" n="HIAT:w" s="T291">pona</ts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_976" n="HIAT:w" s="T292">üːtɨmpatɨ</ts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_979" n="HIAT:w" s="T293">poːqɨn</ts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_982" n="HIAT:w" s="T294">tü</ts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_985" n="HIAT:w" s="T295">čʼɔːtaltɛntɔːt</ts>
                  <nts id="Seg_986" n="HIAT:ip">.</nts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T308" id="Seg_988" n="sc" s="T297">
               <ts e="T308" id="Seg_990" n="HIAT:u" s="T297">
                  <ts e="T298" id="Seg_992" n="HIAT:w" s="T297">Tü</ts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_995" n="HIAT:w" s="T298">čʼɔːtalpɨntɔːtɨt</ts>
                  <nts id="Seg_996" n="HIAT:ip">,</nts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_999" n="HIAT:w" s="T299">tına</ts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1002" n="HIAT:w" s="T300">tölʼ</ts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1005" n="HIAT:w" s="T301">purɨš</ts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1008" n="HIAT:w" s="T302">olɨlʼa</ts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1011" n="HIAT:w" s="T303">nılʼčʼik</ts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1014" n="HIAT:w" s="T304">təːkɨrɨmpɨlʼ</ts>
                  <nts id="Seg_1015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1017" n="HIAT:w" s="T305">tomarot</ts>
                  <nts id="Seg_1018" n="HIAT:ip">,</nts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1021" n="HIAT:w" s="T306">mɔːt</ts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1023" n="HIAT:ip">(</nts>
                  <ts e="T308" id="Seg_1025" n="HIAT:w" s="T307">tümtɨ</ts>
                  <nts id="Seg_1026" n="HIAT:ip">)</nts>
                  <nts id="Seg_1027" n="HIAT:ip">.</nts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T314" id="Seg_1029" n="sc" s="T309">
               <ts e="T314" id="Seg_1031" n="HIAT:u" s="T309">
                  <nts id="Seg_1032" n="HIAT:ip">“</nts>
                  <ts e="T310" id="Seg_1034" n="HIAT:w" s="T309">Tɛː</ts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1037" n="HIAT:w" s="T310">najenta</ts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1040" n="HIAT:w" s="T311">man</ts>
                  <nts id="Seg_1041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1043" n="HIAT:w" s="T312">na</ts>
                  <nts id="Seg_1044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1046" n="HIAT:w" s="T313">tannɛntak</ts>
                  <nts id="Seg_1047" n="HIAT:ip">.</nts>
                  <nts id="Seg_1048" n="HIAT:ip">”</nts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T317" id="Seg_1050" n="sc" s="T315">
               <ts e="T317" id="Seg_1052" n="HIAT:u" s="T315">
                  <ts e="T316" id="Seg_1054" n="HIAT:w" s="T315">Qumɨn</ts>
                  <nts id="Seg_1055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1057" n="HIAT:w" s="T316">amɨrsɔːtɨt</ts>
                  <nts id="Seg_1058" n="HIAT:ip">.</nts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T319" id="Seg_1060" n="sc" s="T318">
               <ts e="T319" id="Seg_1062" n="HIAT:u" s="T318">
                  <ts e="T319" id="Seg_1064" n="HIAT:w" s="T318">Amɨrnɔːtɨt</ts>
                  <nts id="Seg_1065" n="HIAT:ip">.</nts>
                  <nts id="Seg_1066" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T329" id="Seg_1067" n="sc" s="T320">
               <ts e="T329" id="Seg_1069" n="HIAT:u" s="T320">
                  <ts e="T321" id="Seg_1071" n="HIAT:w" s="T320">Qälɨt</ts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1074" n="HIAT:w" s="T321">nık</ts>
                  <nts id="Seg_1075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1077" n="HIAT:w" s="T322">kətɔːt</ts>
                  <nts id="Seg_1078" n="HIAT:ip">:</nts>
                  <nts id="Seg_1079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1080" n="HIAT:ip">“</nts>
                  <ts e="T324" id="Seg_1082" n="HIAT:w" s="T323">Ilʼčʼa</ts>
                  <nts id="Seg_1083" n="HIAT:ip">,</nts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1086" n="HIAT:w" s="T324">šoːqɨrlɨ</ts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1089" n="HIAT:w" s="T325">naššak</ts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1092" n="HIAT:w" s="T326">qajqo</ts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1095" n="HIAT:w" s="T327">poːsä</ts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1098" n="HIAT:w" s="T328">sälal</ts>
                  <nts id="Seg_1099" n="HIAT:ip">?</nts>
                  <nts id="Seg_1100" n="HIAT:ip">”</nts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T334" id="Seg_1102" n="sc" s="T330">
               <ts e="T334" id="Seg_1104" n="HIAT:u" s="T330">
                  <nts id="Seg_1105" n="HIAT:ip">“</nts>
                  <ts e="T331" id="Seg_1107" n="HIAT:w" s="T330">Ašša</ts>
                  <nts id="Seg_1108" n="HIAT:ip">,</nts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1111" n="HIAT:w" s="T331">šoːqɨr</ts>
                  <nts id="Seg_1112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1114" n="HIAT:w" s="T332">tü</ts>
                  <nts id="Seg_1115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1117" n="HIAT:w" s="T333">amtɨŋɨt</ts>
                  <nts id="Seg_1118" n="HIAT:ip">.</nts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T340" id="Seg_1120" n="sc" s="T335">
               <ts e="T340" id="Seg_1122" n="HIAT:u" s="T335">
                  <ts e="T336" id="Seg_1124" n="HIAT:w" s="T335">Ašša</ts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1127" n="HIAT:w" s="T336">mɨta</ts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1130" n="HIAT:w" s="T337">man</ts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1133" n="HIAT:w" s="T338">mɨta</ts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1136" n="HIAT:w" s="T339">mɨ</ts>
                  <nts id="Seg_1137" n="HIAT:ip">…</nts>
                  <nts id="Seg_1138" n="HIAT:ip">”</nts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T355" id="Seg_1140" n="sc" s="T341">
               <ts e="T355" id="Seg_1142" n="HIAT:u" s="T341">
                  <ts e="T342" id="Seg_1144" n="HIAT:w" s="T341">Tɛm</ts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1147" n="HIAT:w" s="T342">ašša</ts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1150" n="HIAT:w" s="T343">amtɨtɨ</ts>
                  <nts id="Seg_1151" n="HIAT:ip">,</nts>
                  <nts id="Seg_1152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1154" n="HIAT:w" s="T344">tɛm</ts>
                  <nts id="Seg_1155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1157" n="HIAT:w" s="T345">ašša</ts>
                  <nts id="Seg_1158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1160" n="HIAT:w" s="T346">amtɨtɨ</ts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1163" n="HIAT:w" s="T347">nılʼ</ts>
                  <nts id="Seg_1164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1166" n="HIAT:w" s="T348">mərkata</ts>
                  <nts id="Seg_1167" n="HIAT:ip">,</nts>
                  <nts id="Seg_1168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1170" n="HIAT:w" s="T349">nılʼ</ts>
                  <nts id="Seg_1171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1173" n="HIAT:w" s="T350">nʼomalʼ</ts>
                  <nts id="Seg_1174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1176" n="HIAT:w" s="T351">porqɨ</ts>
                  <nts id="Seg_1177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1179" n="HIAT:w" s="T352">raša</ts>
                  <nts id="Seg_1180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1182" n="HIAT:w" s="T353">lıːpɨ</ts>
                  <nts id="Seg_1183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1185" n="HIAT:w" s="T354">tına</ts>
                  <nts id="Seg_1186" n="HIAT:ip">.</nts>
                  <nts id="Seg_1187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T359" id="Seg_1188" n="sc" s="T356">
               <ts e="T359" id="Seg_1190" n="HIAT:u" s="T356">
                  <ts e="T357" id="Seg_1192" n="HIAT:w" s="T356">Meːntalʼ</ts>
                  <nts id="Seg_1193" n="HIAT:ip">,</nts>
                  <nts id="Seg_1194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1196" n="HIAT:w" s="T357">namɨp</ts>
                  <nts id="Seg_1197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1199" n="HIAT:w" s="T358">šeːrpɨntɨt</ts>
                  <nts id="Seg_1200" n="HIAT:ip">.</nts>
                  <nts id="Seg_1201" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T365" id="Seg_1202" n="sc" s="T360">
               <ts e="T365" id="Seg_1204" n="HIAT:u" s="T360">
                  <ts e="T361" id="Seg_1206" n="HIAT:w" s="T360">Šoŋoltɨ</ts>
                  <nts id="Seg_1207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1209" n="HIAT:w" s="T361">qanɨqqɨt</ts>
                  <nts id="Seg_1210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1212" n="HIAT:w" s="T362">mərqa</ts>
                  <nts id="Seg_1213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1215" n="HIAT:w" s="T363">mɔːtantɨ</ts>
                  <nts id="Seg_1216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1218" n="HIAT:w" s="T364">qanɨqqɨt</ts>
                  <nts id="Seg_1219" n="HIAT:ip">.</nts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T375" id="Seg_1221" n="sc" s="T366">
               <ts e="T375" id="Seg_1223" n="HIAT:u" s="T366">
                  <ts e="T367" id="Seg_1225" n="HIAT:w" s="T366">Ukkɨr</ts>
                  <nts id="Seg_1226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1228" n="HIAT:w" s="T367">tot</ts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1231" n="HIAT:w" s="T368">čʼontot</ts>
                  <nts id="Seg_1232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1234" n="HIAT:w" s="T369">poːqɨnɨ</ts>
                  <nts id="Seg_1235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1237" n="HIAT:w" s="T370">mɔːttɨ</ts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1240" n="HIAT:w" s="T371">tına</ts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1243" n="HIAT:w" s="T372">tülʼ</ts>
                  <nts id="Seg_1244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1246" n="HIAT:w" s="T373">purɨšlaka</ts>
                  <nts id="Seg_1247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1249" n="HIAT:w" s="T374">alʼčʼintɨ</ts>
                  <nts id="Seg_1250" n="HIAT:ip">.</nts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T383" id="Seg_1252" n="sc" s="T376">
               <ts e="T383" id="Seg_1254" n="HIAT:u" s="T376">
                  <ts e="T377" id="Seg_1256" n="HIAT:w" s="T376">Na</ts>
                  <nts id="Seg_1257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1259" n="HIAT:w" s="T377">paktɨmmɨnta</ts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1262" n="HIAT:w" s="T378">tına</ts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1265" n="HIAT:w" s="T379">mɔːtantɨ</ts>
                  <nts id="Seg_1266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1268" n="HIAT:w" s="T380">tına</ts>
                  <nts id="Seg_1269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1271" n="HIAT:w" s="T381">nılʼčʼi</ts>
                  <nts id="Seg_1272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1274" n="HIAT:w" s="T382">ämnäiːtɨ</ts>
                  <nts id="Seg_1275" n="HIAT:ip">.</nts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T397" id="Seg_1277" n="sc" s="T384">
               <ts e="T397" id="Seg_1279" n="HIAT:u" s="T384">
                  <ts e="T385" id="Seg_1281" n="HIAT:w" s="T384">Qälɨn</ts>
                  <nts id="Seg_1282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1284" n="HIAT:w" s="T385">čʼam</ts>
                  <nts id="Seg_1285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1287" n="HIAT:w" s="T386">orqɨlpɔːt</ts>
                  <nts id="Seg_1288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1290" n="HIAT:w" s="T387">nılʼ</ts>
                  <nts id="Seg_1291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1293" n="HIAT:w" s="T388">irap</ts>
                  <nts id="Seg_1294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1296" n="HIAT:w" s="T389">ola</ts>
                  <nts id="Seg_1297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1299" n="HIAT:w" s="T390">čʼaŋaj</ts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1302" n="HIAT:w" s="T391">nʼomalʼ</ts>
                  <nts id="Seg_1303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1305" n="HIAT:w" s="T392">porqɨntɨ</ts>
                  <nts id="Seg_1306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1308" n="HIAT:w" s="T393">lıːp</ts>
                  <nts id="Seg_1309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1311" n="HIAT:w" s="T394">nɨtqɨlɛːla</ts>
                  <nts id="Seg_1312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1314" n="HIAT:w" s="T395">orqɨltɔːt</ts>
                  <nts id="Seg_1315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1317" n="HIAT:w" s="T396">mɔːtqɨn</ts>
                  <nts id="Seg_1318" n="HIAT:ip">.</nts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T412" id="Seg_1320" n="sc" s="T398">
               <ts e="T412" id="Seg_1322" n="HIAT:u" s="T398">
                  <ts e="T399" id="Seg_1324" n="HIAT:w" s="T398">Qapi</ts>
                  <nts id="Seg_1325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1327" n="HIAT:w" s="T399">na</ts>
                  <nts id="Seg_1328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1330" n="HIAT:w" s="T400">tüsa</ts>
                  <nts id="Seg_1331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1333" n="HIAT:w" s="T401">nɨnɨ</ts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1336" n="HIAT:w" s="T402">näːtɨ</ts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1339" n="HIAT:w" s="T403">okoškan</ts>
                  <nts id="Seg_1340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1342" n="HIAT:w" s="T404">ɔːš</ts>
                  <nts id="Seg_1343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1345" n="HIAT:w" s="T405">šüːmɨt</ts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1348" n="HIAT:w" s="T406">mɔːttɨ</ts>
                  <nts id="Seg_1349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1350" n="HIAT:ip">(</nts>
                  <ts e="T408" id="Seg_1352" n="HIAT:w" s="T407">tılʼčʼɨ</ts>
                  <nts id="Seg_1353" n="HIAT:ip">)</nts>
                  <nts id="Seg_1354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1356" n="HIAT:w" s="T408">šittɨlʼ</ts>
                  <nts id="Seg_1357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1359" n="HIAT:w" s="T409">pɛlalʼ</ts>
                  <nts id="Seg_1360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1362" n="HIAT:w" s="T410">okoška</ts>
                  <nts id="Seg_1363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1365" n="HIAT:w" s="T411">ɛːppa</ts>
                  <nts id="Seg_1366" n="HIAT:ip">.</nts>
                  <nts id="Seg_1367" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T417" id="Seg_1368" n="sc" s="T413">
               <ts e="T417" id="Seg_1370" n="HIAT:u" s="T413">
                  <ts e="T414" id="Seg_1372" n="HIAT:w" s="T413">Qaj</ts>
                  <nts id="Seg_1373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1375" n="HIAT:w" s="T414">wərqɨ</ts>
                  <nts id="Seg_1376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1378" n="HIAT:w" s="T415">mɔːttɨ</ts>
                  <nts id="Seg_1379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1381" n="HIAT:w" s="T416">ɛːppa</ts>
                  <nts id="Seg_1382" n="HIAT:ip">.</nts>
                  <nts id="Seg_1383" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T430" id="Seg_1384" n="sc" s="T418">
               <ts e="T430" id="Seg_1386" n="HIAT:u" s="T418">
                  <ts e="T419" id="Seg_1388" n="HIAT:w" s="T418">Mɔːttɨ</ts>
                  <nts id="Seg_1389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1391" n="HIAT:w" s="T419">najentɨ</ts>
                  <nts id="Seg_1392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1394" n="HIAT:w" s="T420">čʼusa</ts>
                  <nts id="Seg_1395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1397" n="HIAT:w" s="T421">taqpɨmpa</ts>
                  <nts id="Seg_1398" n="HIAT:ip">,</nts>
                  <nts id="Seg_1399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1401" n="HIAT:w" s="T422">tɨna</ts>
                  <nts id="Seg_1402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1404" n="HIAT:w" s="T423">loːqɨra</ts>
                  <nts id="Seg_1405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1407" n="HIAT:w" s="T424">polʼ</ts>
                  <nts id="Seg_1408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1410" n="HIAT:w" s="T425">mɔːt</ts>
                  <nts id="Seg_1411" n="HIAT:ip">,</nts>
                  <nts id="Seg_1412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1414" n="HIAT:w" s="T426">pičʼit</ts>
                  <nts id="Seg_1415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1417" n="HIAT:w" s="T427">paqɨt</ts>
                  <nts id="Seg_1418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1420" n="HIAT:w" s="T428">piːrɨlʼ</ts>
                  <nts id="Seg_1421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1423" n="HIAT:w" s="T429">taqpa</ts>
                  <nts id="Seg_1424" n="HIAT:ip">.</nts>
                  <nts id="Seg_1425" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T446" id="Seg_1426" n="sc" s="T431">
               <ts e="T923" id="Seg_1428" n="HIAT:u" s="T431">
                  <ts e="T432" id="Seg_1430" n="HIAT:w" s="T431">Mompa</ts>
                  <nts id="Seg_1431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1433" n="HIAT:w" s="T432">mɔːtqɨt</ts>
                  <nts id="Seg_1434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1436" n="HIAT:w" s="T433">tına</ts>
                  <nts id="Seg_1437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1439" n="HIAT:w" s="T434">na</ts>
                  <nts id="Seg_1440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1442" n="HIAT:w" s="T435">tüm</ts>
                  <nts id="Seg_1443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1445" n="HIAT:w" s="T436">na</ts>
                  <nts id="Seg_1446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1448" n="HIAT:w" s="T437">qəːlpɨntɔːtɨt</ts>
                  <nts id="Seg_1449" n="HIAT:ip">,</nts>
                  <nts id="Seg_1450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1452" n="HIAT:w" s="T438">na</ts>
                  <nts id="Seg_1453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1455" n="HIAT:w" s="T439">qättɨmmɨntɨtɨ</ts>
                  <nts id="Seg_1456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1458" n="HIAT:w" s="T440">qapi</ts>
                  <nts id="Seg_1459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1461" n="HIAT:w" s="T441">ton</ts>
                  <nts id="Seg_1462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T923" id="Seg_1464" n="HIAT:w" s="T442">irap</ts>
                  <nts id="Seg_1465" n="HIAT:ip">.</nts>
                  <nts id="Seg_1466" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T446" id="Seg_1468" n="HIAT:u" s="T923">
                  <nts id="Seg_1469" n="HIAT:ip">(</nts>
                  <nts id="Seg_1470" n="HIAT:ip">(</nts>
                  <ats e="T443" id="Seg_1471" n="HIAT:non-pho" s="T923">NN:</ats>
                  <nts id="Seg_1472" n="HIAT:ip">)</nts>
                  <nts id="Seg_1473" n="HIAT:ip">)</nts>
                  <nts id="Seg_1474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1476" n="HIAT:w" s="T443">Qapi</ts>
                  <nts id="Seg_1477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1479" n="HIAT:w" s="T444">tam</ts>
                  <nts id="Seg_1480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1482" n="HIAT:w" s="T445">amn</ts>
                  <nts id="Seg_1483" n="HIAT:ip">.</nts>
                  <nts id="Seg_1484" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T448" id="Seg_1485" n="sc" s="T924">
               <ts e="T448" id="Seg_1487" n="HIAT:u" s="T924">
                  <nts id="Seg_1488" n="HIAT:ip">(</nts>
                  <nts id="Seg_1489" n="HIAT:ip">(</nts>
                  <ats e="T447" id="Seg_1490" n="HIAT:non-pho" s="T924">NEP:</ats>
                  <nts id="Seg_1491" n="HIAT:ip">)</nts>
                  <nts id="Seg_1492" n="HIAT:ip">)</nts>
                  <nts id="Seg_1493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1495" n="HIAT:w" s="T447">Tal</ts>
                  <nts id="Seg_1496" n="HIAT:ip">.</nts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T464" id="Seg_1498" n="sc" s="T449">
               <ts e="T464" id="Seg_1500" n="HIAT:u" s="T449">
                  <ts e="T450" id="Seg_1502" n="HIAT:w" s="T449">Poraksa</ts>
                  <nts id="Seg_1503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1505" n="HIAT:w" s="T450">qaj</ts>
                  <nts id="Seg_1506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1508" n="HIAT:w" s="T451">təm</ts>
                  <nts id="Seg_1509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1511" n="HIAT:w" s="T452">mɨta</ts>
                  <nts id="Seg_1512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1514" n="HIAT:w" s="T453">qamnɨmpat</ts>
                  <nts id="Seg_1515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1516" n="HIAT:ip">(</nts>
                  <ts e="T455" id="Seg_1518" n="HIAT:w" s="T454">na=</ts>
                  <nts id="Seg_1519" n="HIAT:ip">)</nts>
                  <nts id="Seg_1520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1522" n="HIAT:w" s="T455">na</ts>
                  <nts id="Seg_1523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1525" n="HIAT:w" s="T456">nʼuːtɨt</ts>
                  <nts id="Seg_1526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1528" n="HIAT:w" s="T457">ɨlqɨlʼ</ts>
                  <nts id="Seg_1529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1531" n="HIAT:w" s="T458">pɛlat</ts>
                  <nts id="Seg_1532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1534" n="HIAT:w" s="T459">tilʼčʼi</ts>
                  <nts id="Seg_1535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1537" n="HIAT:w" s="T460">šentɨ</ts>
                  <nts id="Seg_1538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1540" n="HIAT:w" s="T461">nʼuːtɨ</ts>
                  <nts id="Seg_1541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1543" n="HIAT:w" s="T462">ɨlqɨlʼ</ts>
                  <nts id="Seg_1544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1546" n="HIAT:w" s="T463">pɛlat</ts>
                  <nts id="Seg_1547" n="HIAT:ip">.</nts>
                  <nts id="Seg_1548" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T476" id="Seg_1549" n="sc" s="T465">
               <ts e="T476" id="Seg_1551" n="HIAT:u" s="T465">
                  <ts e="T466" id="Seg_1553" n="HIAT:w" s="T465">Na</ts>
                  <nts id="Seg_1554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1556" n="HIAT:w" s="T466">tü</ts>
                  <nts id="Seg_1557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1559" n="HIAT:w" s="T467">na</ts>
                  <nts id="Seg_1560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1562" n="HIAT:w" s="T468">qättɨmmɨntɨtɨ</ts>
                  <nts id="Seg_1563" n="HIAT:ip">,</nts>
                  <nts id="Seg_1564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1566" n="HIAT:w" s="T469">tına</ts>
                  <nts id="Seg_1567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1569" n="HIAT:w" s="T470">nɔːssaralʼ</ts>
                  <nts id="Seg_1570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1572" n="HIAT:w" s="T471">qumɨlʼ</ts>
                  <nts id="Seg_1573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1575" n="HIAT:w" s="T472">mütɨp</ts>
                  <nts id="Seg_1576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1578" n="HIAT:w" s="T473">muntɨk</ts>
                  <nts id="Seg_1579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1581" n="HIAT:w" s="T474">tü</ts>
                  <nts id="Seg_1582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1584" n="HIAT:w" s="T475">ampat</ts>
                  <nts id="Seg_1585" n="HIAT:ip">.</nts>
                  <nts id="Seg_1586" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T480" id="Seg_1587" n="sc" s="T477">
               <ts e="T480" id="Seg_1589" n="HIAT:u" s="T477">
                  <ts e="T478" id="Seg_1591" n="HIAT:w" s="T477">Qən</ts>
                  <nts id="Seg_1592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1594" n="HIAT:w" s="T478">ɔːlʼčʼɔːt</ts>
                  <nts id="Seg_1595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1597" n="HIAT:w" s="T479">nʼennat</ts>
                  <nts id="Seg_1598" n="HIAT:ip">.</nts>
                  <nts id="Seg_1599" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T491" id="Seg_1600" n="sc" s="T481">
               <ts e="T491" id="Seg_1602" n="HIAT:u" s="T481">
                  <ts e="T482" id="Seg_1604" n="HIAT:w" s="T481">Nʼennä</ts>
                  <nts id="Seg_1605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1607" n="HIAT:w" s="T482">pon</ts>
                  <nts id="Seg_1608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1610" n="HIAT:w" s="T483">ira</ts>
                  <nts id="Seg_1611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1613" n="HIAT:w" s="T484">pona</ts>
                  <nts id="Seg_1614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1616" n="HIAT:w" s="T485">pakta</ts>
                  <nts id="Seg_1617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1619" n="HIAT:w" s="T486">mɔːtan</ts>
                  <nts id="Seg_1620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1622" n="HIAT:w" s="T487">ɔːt</ts>
                  <nts id="Seg_1623" n="HIAT:ip">,</nts>
                  <nts id="Seg_1624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1626" n="HIAT:w" s="T488">pičʼilʼ</ts>
                  <nts id="Seg_1627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1629" n="HIAT:w" s="T489">ut</ts>
                  <nts id="Seg_1630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1632" n="HIAT:w" s="T490">mərka</ts>
                  <nts id="Seg_1633" n="HIAT:ip">.</nts>
                  <nts id="Seg_1634" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T499" id="Seg_1635" n="sc" s="T492">
               <ts e="T499" id="Seg_1637" n="HIAT:u" s="T492">
                  <ts e="T493" id="Seg_1639" n="HIAT:w" s="T492">Isɨ</ts>
                  <nts id="Seg_1640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1642" n="HIAT:w" s="T493">čʼap</ts>
                  <nts id="Seg_1643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1645" n="HIAT:w" s="T494">pona</ts>
                  <nts id="Seg_1646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1648" n="HIAT:w" s="T495">qontɨšɛlʼčʼɔːt</ts>
                  <nts id="Seg_1649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1651" n="HIAT:w" s="T496">aj</ts>
                  <nts id="Seg_1652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1654" n="HIAT:w" s="T497">čʼari</ts>
                  <nts id="Seg_1655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1657" n="HIAT:w" s="T498">qättɨŋɨt</ts>
                  <nts id="Seg_1658" n="HIAT:ip">.</nts>
                  <nts id="Seg_1659" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T516" id="Seg_1660" n="sc" s="T500">
               <ts e="T516" id="Seg_1662" n="HIAT:u" s="T500">
                  <ts e="T501" id="Seg_1664" n="HIAT:w" s="T500">Ämnäiːtɨ</ts>
                  <nts id="Seg_1665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1667" n="HIAT:w" s="T501">na</ts>
                  <nts id="Seg_1668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1670" n="HIAT:w" s="T502">tü</ts>
                  <nts id="Seg_1671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1673" n="HIAT:w" s="T503">na</ts>
                  <nts id="Seg_1674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1676" n="HIAT:w" s="T504">qəːmmɨntɨt</ts>
                  <nts id="Seg_1677" n="HIAT:ip">,</nts>
                  <nts id="Seg_1678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1680" n="HIAT:w" s="T505">šoːqɨr</ts>
                  <nts id="Seg_1681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1683" n="HIAT:w" s="T506">pɔːr</ts>
                  <nts id="Seg_1684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1686" n="HIAT:w" s="T507">šüːmɨt</ts>
                  <nts id="Seg_1687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1689" n="HIAT:w" s="T508">qəːŋɔːt</ts>
                  <nts id="Seg_1690" n="HIAT:ip">,</nts>
                  <nts id="Seg_1691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1693" n="HIAT:w" s="T509">nɨn</ts>
                  <nts id="Seg_1694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1696" n="HIAT:w" s="T510">ɛj</ts>
                  <nts id="Seg_1697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1699" n="HIAT:w" s="T511">šit</ts>
                  <nts id="Seg_1700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1702" n="HIAT:w" s="T512">okoškon</ts>
                  <nts id="Seg_1703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1705" n="HIAT:w" s="T513">ɔːš</ts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1708" n="HIAT:w" s="T514">šüːmɨt</ts>
                  <nts id="Seg_1709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1711" n="HIAT:w" s="T515">qəːŋɔːtɨn</ts>
                  <nts id="Seg_1712" n="HIAT:ip">.</nts>
                  <nts id="Seg_1713" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T523" id="Seg_1714" n="sc" s="T517">
               <ts e="T523" id="Seg_1716" n="HIAT:u" s="T517">
                  <ts e="T518" id="Seg_1718" n="HIAT:w" s="T517">Muntɨk</ts>
                  <nts id="Seg_1719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1721" n="HIAT:w" s="T518">tüs</ts>
                  <nts id="Seg_1722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1724" n="HIAT:w" s="T519">čʼɔːttɛıːmpatɨ</ts>
                  <nts id="Seg_1725" n="HIAT:ip">,</nts>
                  <nts id="Seg_1726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1728" n="HIAT:w" s="T520">na</ts>
                  <nts id="Seg_1729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1731" n="HIAT:w" s="T521">qälɨlʼ</ts>
                  <nts id="Seg_1732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_1734" n="HIAT:w" s="T522">mütɨp</ts>
                  <nts id="Seg_1735" n="HIAT:ip">.</nts>
                  <nts id="Seg_1736" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T529" id="Seg_1737" n="sc" s="T524">
               <ts e="T529" id="Seg_1739" n="HIAT:u" s="T524">
                  <ts e="T525" id="Seg_1741" n="HIAT:w" s="T524">Na</ts>
                  <nts id="Seg_1742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1744" n="HIAT:w" s="T525">mütɨ</ts>
                  <nts id="Seg_1745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1747" n="HIAT:w" s="T526">qɔːtɨ</ts>
                  <nts id="Seg_1748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_1750" n="HIAT:w" s="T527">šittä</ts>
                  <nts id="Seg_1751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1753" n="HIAT:w" s="T528">ɛːppɨntɔːt</ts>
                  <nts id="Seg_1754" n="HIAT:ip">.</nts>
                  <nts id="Seg_1755" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T531" id="Seg_1756" n="sc" s="T530">
               <ts e="T531" id="Seg_1758" n="HIAT:u" s="T530">
                  <nts id="Seg_1759" n="HIAT:ip">(</nts>
                  <nts id="Seg_1760" n="HIAT:ip">(</nts>
                  <ats e="T530.tx.1" id="Seg_1761" n="HIAT:non-pho" s="T530">KuAI:</ats>
                  <nts id="Seg_1762" n="HIAT:ip">)</nts>
                  <nts id="Seg_1763" n="HIAT:ip">)</nts>
                  <nts id="Seg_1764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_1766" n="HIAT:w" s="T530.tx.1">Говорите</ts>
                  <nts id="Seg_1767" n="HIAT:ip">.</nts>
                  <nts id="Seg_1768" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T534" id="Seg_1769" n="sc" s="T926">
               <ts e="T534" id="Seg_1771" n="HIAT:u" s="T926">
                  <nts id="Seg_1772" n="HIAT:ip">(</nts>
                  <nts id="Seg_1773" n="HIAT:ip">(</nts>
                  <ats e="T532" id="Seg_1774" n="HIAT:non-pho" s="T926">NN:</ats>
                  <nts id="Seg_1775" n="HIAT:ip">)</nts>
                  <nts id="Seg_1776" n="HIAT:ip">)</nts>
                  <nts id="Seg_1777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_1779" n="HIAT:w" s="T532">Na</ts>
                  <nts id="Seg_1780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_1782" n="HIAT:w" s="T533">kətät</ts>
                  <nts id="Seg_1783" n="HIAT:ip">.</nts>
                  <nts id="Seg_1784" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T539" id="Seg_1785" n="sc" s="T925">
               <ts e="T539" id="Seg_1787" n="HIAT:u" s="T925">
                  <nts id="Seg_1788" n="HIAT:ip">(</nts>
                  <nts id="Seg_1789" n="HIAT:ip">(</nts>
                  <ats e="T535" id="Seg_1790" n="HIAT:non-pho" s="T925">NEP:</ats>
                  <nts id="Seg_1791" n="HIAT:ip">)</nts>
                  <nts id="Seg_1792" n="HIAT:ip">)</nts>
                  <nts id="Seg_1793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1795" n="HIAT:w" s="T535">Pɛlakɨtɨj</ts>
                  <nts id="Seg_1796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_1798" n="HIAT:w" s="T536">ira</ts>
                  <nts id="Seg_1799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_1801" n="HIAT:w" s="T537">kučʼčʼa</ts>
                  <nts id="Seg_1802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_1804" n="HIAT:w" s="T538">qattɛnta</ts>
                  <nts id="Seg_1805" n="HIAT:ip">?</nts>
                  <nts id="Seg_1806" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T551" id="Seg_1807" n="sc" s="T540">
               <ts e="T551" id="Seg_1809" n="HIAT:u" s="T540">
                  <ts e="T541" id="Seg_1811" n="HIAT:w" s="T540">Ira</ts>
                  <nts id="Seg_1812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_1814" n="HIAT:w" s="T541">nɨnɨ</ts>
                  <nts id="Seg_1815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_1817" n="HIAT:w" s="T542">šʼittalʼ</ts>
                  <nts id="Seg_1818" n="HIAT:ip">,</nts>
                  <nts id="Seg_1819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_1821" n="HIAT:w" s="T543">mɨŋa</ts>
                  <nts id="Seg_1822" n="HIAT:ip">,</nts>
                  <nts id="Seg_1823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_1825" n="HIAT:w" s="T544">Nʼomalʼ</ts>
                  <nts id="Seg_1826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_1828" n="HIAT:w" s="T545">porqɨ</ts>
                  <nts id="Seg_1829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_1831" n="HIAT:w" s="T546">ira</ts>
                  <nts id="Seg_1832" n="HIAT:ip">,</nts>
                  <nts id="Seg_1833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_1835" n="HIAT:w" s="T547">karrä</ts>
                  <nts id="Seg_1836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_1838" n="HIAT:w" s="T548">qälɨiːntɨ</ts>
                  <nts id="Seg_1839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_1841" n="HIAT:w" s="T549">qaqlontɨ</ts>
                  <nts id="Seg_1842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1843" n="HIAT:ip">(</nts>
                  <ts e="T551" id="Seg_1845" n="HIAT:w" s="T550">qəŋa</ts>
                  <nts id="Seg_1846" n="HIAT:ip">)</nts>
                  <nts id="Seg_1847" n="HIAT:ip">.</nts>
                  <nts id="Seg_1848" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T562" id="Seg_1849" n="sc" s="T552">
               <ts e="T927" id="Seg_1851" n="HIAT:u" s="T552">
                  <ts e="T553" id="Seg_1853" n="HIAT:w" s="T552">Aj</ts>
                  <nts id="Seg_1854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_1856" n="HIAT:w" s="T553">šittämɨ</ts>
                  <nts id="Seg_1857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1858" n="HIAT:ip">(</nts>
                  <nts id="Seg_1859" n="HIAT:ip">(</nts>
                  <ats e="T555" id="Seg_1860" n="HIAT:non-pho" s="T554">…</ats>
                  <nts id="Seg_1861" n="HIAT:ip">)</nts>
                  <nts id="Seg_1862" n="HIAT:ip">)</nts>
                  <nts id="Seg_1863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_1865" n="HIAT:w" s="T555">takkɨ</ts>
                  <nts id="Seg_1866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1867" n="HIAT:ip">(</nts>
                  <nts id="Seg_1868" n="HIAT:ip">(</nts>
                  <ats e="T557" id="Seg_1869" n="HIAT:non-pho" s="T556">…</ats>
                  <nts id="Seg_1870" n="HIAT:ip">)</nts>
                  <nts id="Seg_1871" n="HIAT:ip">)</nts>
                  <nts id="Seg_1872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_1874" n="HIAT:w" s="T557">qälɨj</ts>
                  <nts id="Seg_1875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_1877" n="HIAT:w" s="T558">mɔːttɨ</ts>
                  <nts id="Seg_1878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1879" n="HIAT:ip">(</nts>
                  <ts e="T927" id="Seg_1881" n="HIAT:w" s="T559">qənqo</ts>
                  <nts id="Seg_1882" n="HIAT:ip">)</nts>
                  <nts id="Seg_1883" n="HIAT:ip">.</nts>
                  <nts id="Seg_1884" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T562" id="Seg_1886" n="HIAT:u" s="T927">
                  <nts id="Seg_1887" n="HIAT:ip">(</nts>
                  <nts id="Seg_1888" n="HIAT:ip">(</nts>
                  <ats e="T560" id="Seg_1889" n="HIAT:non-pho" s="T927">NN:</ats>
                  <nts id="Seg_1890" n="HIAT:ip">)</nts>
                  <nts id="Seg_1891" n="HIAT:ip">)</nts>
                  <nts id="Seg_1892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_1894" n="HIAT:w" s="T560">Illa</ts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_1897" n="HIAT:w" s="T561">omtäš</ts>
                  <nts id="Seg_1898" n="HIAT:ip">.</nts>
                  <nts id="Seg_1899" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T567" id="Seg_1900" n="sc" s="T928">
               <ts e="T567" id="Seg_1902" n="HIAT:u" s="T928">
                  <nts id="Seg_1903" n="HIAT:ip">(</nts>
                  <nts id="Seg_1904" n="HIAT:ip">(</nts>
                  <ats e="T563" id="Seg_1905" n="HIAT:non-pho" s="T928">NEP:</ats>
                  <nts id="Seg_1906" n="HIAT:ip">)</nts>
                  <nts id="Seg_1907" n="HIAT:ip">)</nts>
                  <nts id="Seg_1908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_1910" n="HIAT:w" s="T563">Na</ts>
                  <nts id="Seg_1911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_1913" n="HIAT:w" s="T564">qətqɨlpɨtɨj</ts>
                  <nts id="Seg_1914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_1916" n="HIAT:w" s="T565">qumɨiːqɨntɨ</ts>
                  <nts id="Seg_1917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_1919" n="HIAT:w" s="T566">qənqɨntoːqo</ts>
                  <nts id="Seg_1920" n="HIAT:ip">.</nts>
                  <nts id="Seg_1921" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T577" id="Seg_1922" n="sc" s="T568">
               <ts e="T577" id="Seg_1924" n="HIAT:u" s="T568">
                  <ts e="T569" id="Seg_1926" n="HIAT:w" s="T568">Karrä</ts>
                  <nts id="Seg_1927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_1929" n="HIAT:w" s="T569">qənna</ts>
                  <nts id="Seg_1930" n="HIAT:ip">,</nts>
                  <nts id="Seg_1931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_1933" n="HIAT:w" s="T570">ooo</ts>
                  <nts id="Seg_1934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_1936" n="HIAT:w" s="T571">karrän</ts>
                  <nts id="Seg_1937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_1939" n="HIAT:w" s="T572">konna</ts>
                  <nts id="Seg_1940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_1942" n="HIAT:w" s="T573">moːčʼalʼ</ts>
                  <nts id="Seg_1943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_1945" n="HIAT:w" s="T574">qälɨ</ts>
                  <nts id="Seg_1946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_1948" n="HIAT:w" s="T575">ira</ts>
                  <nts id="Seg_1949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_1951" n="HIAT:w" s="T576">qontɨšeintɨ</ts>
                  <nts id="Seg_1952" n="HIAT:ip">.</nts>
                  <nts id="Seg_1953" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T579" id="Seg_1954" n="sc" s="T578">
               <ts e="T579" id="Seg_1956" n="HIAT:u" s="T578">
                  <nts id="Seg_1957" n="HIAT:ip">(</nts>
                  <nts id="Seg_1958" n="HIAT:ip">(</nts>
                  <ats e="T579" id="Seg_1959" n="HIAT:non-pho" s="T578">LAUGH</ats>
                  <nts id="Seg_1960" n="HIAT:ip">)</nts>
                  <nts id="Seg_1961" n="HIAT:ip">)</nts>
                  <nts id="Seg_1962" n="HIAT:ip">.</nts>
                  <nts id="Seg_1963" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T583" id="Seg_1964" n="sc" s="T580">
               <ts e="T583" id="Seg_1966" n="HIAT:u" s="T580">
                  <ts e="T581" id="Seg_1968" n="HIAT:w" s="T580">Qaj</ts>
                  <nts id="Seg_1969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_1971" n="HIAT:w" s="T581">na</ts>
                  <nts id="Seg_1972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_1974" n="HIAT:w" s="T582">laqɨmɔːttak</ts>
                  <nts id="Seg_1975" n="HIAT:ip">.</nts>
                  <nts id="Seg_1976" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T589" id="Seg_1977" n="sc" s="T584">
               <ts e="T589" id="Seg_1979" n="HIAT:u" s="T584">
                  <ts e="T585" id="Seg_1981" n="HIAT:w" s="T584">Qaj</ts>
                  <nts id="Seg_1982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_1984" n="HIAT:w" s="T585">Nʼomalʼ</ts>
                  <nts id="Seg_1985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_1987" n="HIAT:w" s="T586">porqɨ</ts>
                  <nts id="Seg_1988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_1990" n="HIAT:w" s="T587">irat</ts>
                  <nts id="Seg_1991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1992" n="HIAT:ip">(</nts>
                  <nts id="Seg_1993" n="HIAT:ip">(</nts>
                  <ats e="T589" id="Seg_1994" n="HIAT:non-pho" s="T588">…</ats>
                  <nts id="Seg_1995" n="HIAT:ip">)</nts>
                  <nts id="Seg_1996" n="HIAT:ip">)</nts>
                  <nts id="Seg_1997" n="HIAT:ip">.</nts>
                  <nts id="Seg_1998" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T599" id="Seg_1999" n="sc" s="T590">
               <ts e="T599" id="Seg_2001" n="HIAT:u" s="T590">
                  <ts e="T591" id="Seg_2003" n="HIAT:w" s="T590">Nʼomalʼ</ts>
                  <nts id="Seg_2004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2006" n="HIAT:w" s="T591">porqɨ</ts>
                  <nts id="Seg_2007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2009" n="HIAT:w" s="T592">ira</ts>
                  <nts id="Seg_2010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2012" n="HIAT:w" s="T593">čʼarrä</ts>
                  <nts id="Seg_2013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2015" n="HIAT:w" s="T594">qättɨŋɨtɨ</ts>
                  <nts id="Seg_2016" n="HIAT:ip">,</nts>
                  <nts id="Seg_2017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2019" n="HIAT:w" s="T595">päqɨlʼ</ts>
                  <nts id="Seg_2020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2022" n="HIAT:w" s="T596">šuktɨ</ts>
                  <nts id="Seg_2023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2025" n="HIAT:w" s="T597">qottä</ts>
                  <nts id="Seg_2026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2027" n="HIAT:ip">(</nts>
                  <ts e="T599" id="Seg_2029" n="HIAT:w" s="T598">säppaja</ts>
                  <nts id="Seg_2030" n="HIAT:ip">)</nts>
                  <nts id="Seg_2031" n="HIAT:ip">.</nts>
                  <nts id="Seg_2032" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T603" id="Seg_2033" n="sc" s="T600">
               <ts e="T603" id="Seg_2035" n="HIAT:u" s="T600">
                  <ts e="T601" id="Seg_2037" n="HIAT:w" s="T600">Nɨnɨ</ts>
                  <nts id="Seg_2038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2040" n="HIAT:w" s="T601">karrä</ts>
                  <nts id="Seg_2041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2043" n="HIAT:w" s="T602">kurɔːlna</ts>
                  <nts id="Seg_2044" n="HIAT:ip">.</nts>
                  <nts id="Seg_2045" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T611" id="Seg_2046" n="sc" s="T604">
               <ts e="T611" id="Seg_2048" n="HIAT:u" s="T604">
                  <ts e="T605" id="Seg_2050" n="HIAT:w" s="T604">Karrä</ts>
                  <nts id="Seg_2051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2053" n="HIAT:w" s="T605">kurɔːlna</ts>
                  <nts id="Seg_2054" n="HIAT:ip">,</nts>
                  <nts id="Seg_2055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2057" n="HIAT:w" s="T606">nɨnɨ</ts>
                  <nts id="Seg_2058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2060" n="HIAT:w" s="T607">nɔːkɨr</ts>
                  <nts id="Seg_2061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2063" n="HIAT:w" s="T608">ämnäsɨt</ts>
                  <nts id="Seg_2064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2066" n="HIAT:w" s="T609">takkɨ</ts>
                  <nts id="Seg_2067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2069" n="HIAT:w" s="T610">qənnɔːt</ts>
                  <nts id="Seg_2070" n="HIAT:ip">.</nts>
                  <nts id="Seg_2071" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T618" id="Seg_2072" n="sc" s="T612">
               <ts e="T618" id="Seg_2074" n="HIAT:u" s="T612">
                  <ts e="T613" id="Seg_2076" n="HIAT:w" s="T612">Takkɨ</ts>
                  <nts id="Seg_2077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2079" n="HIAT:w" s="T613">qälɨn</ts>
                  <nts id="Seg_2080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2082" n="HIAT:w" s="T614">mɔːt</ts>
                  <nts id="Seg_2083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2085" n="HIAT:w" s="T615">takkɨn</ts>
                  <nts id="Seg_2086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2088" n="HIAT:w" s="T616">ɛːppa</ts>
                  <nts id="Seg_2089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2091" n="HIAT:w" s="T617">točʼčʼotot</ts>
                  <nts id="Seg_2092" n="HIAT:ip">.</nts>
                  <nts id="Seg_2093" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T632" id="Seg_2094" n="sc" s="T619">
               <ts e="T632" id="Seg_2096" n="HIAT:u" s="T619">
                  <ts e="T620" id="Seg_2098" n="HIAT:w" s="T619">Karrʼa</ts>
                  <nts id="Seg_2099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2101" n="HIAT:w" s="T620">qənnɔːt</ts>
                  <nts id="Seg_2102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2104" n="HIAT:w" s="T621">nɨmtɨ</ts>
                  <nts id="Seg_2105" n="HIAT:ip">,</nts>
                  <nts id="Seg_2106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2108" n="HIAT:w" s="T622">näčʼčʼa</ts>
                  <nts id="Seg_2109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2111" n="HIAT:w" s="T623">laqaltɛlʼčʼɔːt</ts>
                  <nts id="Seg_2112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2114" n="HIAT:w" s="T624">qälɨtɨt</ts>
                  <nts id="Seg_2115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2117" n="HIAT:w" s="T625">qaqlɨsa</ts>
                  <nts id="Seg_2118" n="HIAT:ip">,</nts>
                  <nts id="Seg_2119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2121" n="HIAT:w" s="T626">na</ts>
                  <nts id="Seg_2122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2124" n="HIAT:w" s="T627">qumiːt</ts>
                  <nts id="Seg_2125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2127" n="HIAT:w" s="T628">qaqlɨsa</ts>
                  <nts id="Seg_2128" n="HIAT:ip">,</nts>
                  <nts id="Seg_2129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2131" n="HIAT:w" s="T629">na</ts>
                  <nts id="Seg_2132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2134" n="HIAT:w" s="T630">nɔːkɨr</ts>
                  <nts id="Seg_2135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2137" n="HIAT:w" s="T631">ilʼčʼasɨt</ts>
                  <nts id="Seg_2138" n="HIAT:ip">.</nts>
                  <nts id="Seg_2139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T636" id="Seg_2140" n="sc" s="T633">
               <ts e="T636" id="Seg_2142" n="HIAT:u" s="T633">
                  <ts e="T634" id="Seg_2144" n="HIAT:w" s="T633">Takkɨ</ts>
                  <nts id="Seg_2145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2147" n="HIAT:w" s="T634">laqaltɛːla</ts>
                  <nts id="Seg_2148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2150" n="HIAT:w" s="T635">qənnɔːt</ts>
                  <nts id="Seg_2151" n="HIAT:ip">.</nts>
                  <nts id="Seg_2152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T645" id="Seg_2153" n="sc" s="T637">
               <ts e="T645" id="Seg_2155" n="HIAT:u" s="T637">
                  <ts e="T638" id="Seg_2157" n="HIAT:w" s="T637">Tına</ts>
                  <nts id="Seg_2158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2160" n="HIAT:w" s="T638">takkɨn</ts>
                  <nts id="Seg_2161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2163" n="HIAT:w" s="T639">na</ts>
                  <nts id="Seg_2164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2166" n="HIAT:w" s="T640">qälɨlʼ</ts>
                  <nts id="Seg_2167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2168" n="HIAT:ip">(</nts>
                  <nts id="Seg_2169" n="HIAT:ip">(</nts>
                  <ats e="T642" id="Seg_2170" n="HIAT:non-pho" s="T641">…</ats>
                  <nts id="Seg_2171" n="HIAT:ip">)</nts>
                  <nts id="Seg_2172" n="HIAT:ip">)</nts>
                  <nts id="Seg_2173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2175" n="HIAT:w" s="T642">imatɨtɨp</ts>
                  <nts id="Seg_2176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2178" n="HIAT:w" s="T643">na</ts>
                  <nts id="Seg_2179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2181" n="HIAT:w" s="T644">šuqɨlpɨntɨ</ts>
                  <nts id="Seg_2182" n="HIAT:ip">.</nts>
                  <nts id="Seg_2183" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T656" id="Seg_2184" n="sc" s="T646">
               <ts e="T656" id="Seg_2186" n="HIAT:u" s="T646">
                  <ts e="T647" id="Seg_2188" n="HIAT:w" s="T646">Tına</ts>
                  <nts id="Seg_2189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2191" n="HIAT:w" s="T647">šʼolqumɨj</ts>
                  <nts id="Seg_2192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2194" n="HIAT:w" s="T648">ijaqı</ts>
                  <nts id="Seg_2195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2197" n="HIAT:w" s="T649">näčʼčʼatɨqɨt</ts>
                  <nts id="Seg_2198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_2200" n="HIAT:w" s="T650">namä</ts>
                  <nts id="Seg_2201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2203" n="HIAT:w" s="T651">qalɨmpɔːqı</ts>
                  <nts id="Seg_2204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2206" n="HIAT:w" s="T652">tomoroqɨt</ts>
                  <nts id="Seg_2207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2209" n="HIAT:w" s="T653">na</ts>
                  <nts id="Seg_2210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2212" n="HIAT:w" s="T654">ilʼčʼantıːn</ts>
                  <nts id="Seg_2213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2215" n="HIAT:w" s="T655">tümpɨqı</ts>
                  <nts id="Seg_2216" n="HIAT:ip">.</nts>
                  <nts id="Seg_2217" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T664" id="Seg_2218" n="sc" s="T657">
               <ts e="T664" id="Seg_2220" n="HIAT:u" s="T657">
                  <ts e="T658" id="Seg_2222" n="HIAT:w" s="T657">Konnätɨ</ts>
                  <nts id="Seg_2223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2225" n="HIAT:w" s="T658">tına</ts>
                  <nts id="Seg_2226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2227" n="HIAT:ip">(</nts>
                  <ts e="T660" id="Seg_2229" n="HIAT:w" s="T659">mɨntɨ</ts>
                  <nts id="Seg_2230" n="HIAT:ip">)</nts>
                  <nts id="Seg_2231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2233" n="HIAT:w" s="T660">šittɨ</ts>
                  <nts id="Seg_2234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2236" n="HIAT:w" s="T661">qälɨk</ts>
                  <nts id="Seg_2237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2239" n="HIAT:w" s="T662">moqonä</ts>
                  <nts id="Seg_2240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2242" n="HIAT:w" s="T663">üːtɨmmɨntɔːt</ts>
                  <nts id="Seg_2243" n="HIAT:ip">.</nts>
                  <nts id="Seg_2244" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T685" id="Seg_2245" n="sc" s="T665">
               <ts e="T685" id="Seg_2247" n="HIAT:u" s="T665">
                  <ts e="T666" id="Seg_2249" n="HIAT:w" s="T665">Mɔːttɨ</ts>
                  <nts id="Seg_2250" n="HIAT:ip">,</nts>
                  <nts id="Seg_2251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2253" n="HIAT:w" s="T666">tɛːpɨktɨ</ts>
                  <nts id="Seg_2254" n="HIAT:ip">,</nts>
                  <nts id="Seg_2255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2256" n="HIAT:ip">(</nts>
                  <ts e="T668" id="Seg_2258" n="HIAT:w" s="T667">kun</ts>
                  <nts id="Seg_2259" n="HIAT:ip">)</nts>
                  <nts id="Seg_2260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2262" n="HIAT:w" s="T668">qajtɨ</ts>
                  <nts id="Seg_2263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2265" n="HIAT:w" s="T669">naɛntɨ</ts>
                  <nts id="Seg_2266" n="HIAT:ip">,</nts>
                  <nts id="Seg_2267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2269" n="HIAT:w" s="T670">Nʼomalʼ</ts>
                  <nts id="Seg_2270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2272" n="HIAT:w" s="T671">porqɨ</ts>
                  <nts id="Seg_2273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2275" n="HIAT:w" s="T672">ira</ts>
                  <nts id="Seg_2276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2278" n="HIAT:w" s="T673">ɔːtäsa</ts>
                  <nts id="Seg_2279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2281" n="HIAT:w" s="T674">sɨr</ts>
                  <nts id="Seg_2282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2284" n="HIAT:w" s="T675">por</ts>
                  <nts id="Seg_2285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2287" n="HIAT:w" s="T676">nʼenna</ts>
                  <nts id="Seg_2288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2290" n="HIAT:w" s="T677">tɔːqqɨŋɨt</ts>
                  <nts id="Seg_2291" n="HIAT:ip">,</nts>
                  <nts id="Seg_2292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2294" n="HIAT:w" s="T678">na</ts>
                  <nts id="Seg_2295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2297" n="HIAT:w" s="T679">na</ts>
                  <nts id="Seg_2298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2300" n="HIAT:w" s="T680">čʼilʼalʼ</ts>
                  <nts id="Seg_2301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2303" n="HIAT:w" s="T681">ija</ts>
                  <nts id="Seg_2304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2306" n="HIAT:w" s="T682">ɛj</ts>
                  <nts id="Seg_2307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2309" n="HIAT:w" s="T683">təpɨsa</ts>
                  <nts id="Seg_2310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2312" n="HIAT:w" s="T684">ɛːsɔːqı</ts>
                  <nts id="Seg_2313" n="HIAT:ip">.</nts>
                  <nts id="Seg_2314" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T693" id="Seg_2315" n="sc" s="T686">
               <ts e="T693" id="Seg_2317" n="HIAT:u" s="T686">
                  <ts e="T687" id="Seg_2319" n="HIAT:w" s="T686">Tomorot</ts>
                  <nts id="Seg_2320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2322" n="HIAT:w" s="T687">təttɨčʼat</ts>
                  <nts id="Seg_2323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2325" n="HIAT:w" s="T688">təpɨt</ts>
                  <nts id="Seg_2326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2328" n="HIAT:w" s="T689">mɨqɨt</ts>
                  <nts id="Seg_2329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2331" n="HIAT:w" s="T690">qälɨj</ts>
                  <nts id="Seg_2332" n="HIAT:ip">,</nts>
                  <nts id="Seg_2333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2335" n="HIAT:w" s="T691">na</ts>
                  <nts id="Seg_2336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2338" n="HIAT:w" s="T692">mɨ</ts>
                  <nts id="Seg_2339" n="HIAT:ip">.</nts>
                  <nts id="Seg_2340" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T700" id="Seg_2341" n="sc" s="T694">
               <ts e="T700" id="Seg_2343" n="HIAT:u" s="T694">
                  <ts e="T695" id="Seg_2345" n="HIAT:w" s="T694">Nʼomalʼ</ts>
                  <nts id="Seg_2346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2348" n="HIAT:w" s="T695">porqɨ</ts>
                  <nts id="Seg_2349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2351" n="HIAT:w" s="T696">ira</ts>
                  <nts id="Seg_2352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2354" n="HIAT:w" s="T697">təttɨčʼälɨ</ts>
                  <nts id="Seg_2355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2357" n="HIAT:w" s="T698">kətsanqıtɨ</ts>
                  <nts id="Seg_2358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2360" n="HIAT:w" s="T699">ɛːppɨntɔːqı</ts>
                  <nts id="Seg_2361" n="HIAT:ip">.</nts>
                  <nts id="Seg_2362" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T706" id="Seg_2363" n="sc" s="T701">
               <ts e="T706" id="Seg_2365" n="HIAT:u" s="T701">
                  <ts e="T702" id="Seg_2367" n="HIAT:w" s="T701">Na</ts>
                  <nts id="Seg_2368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2370" n="HIAT:w" s="T702">qətpɨlʼ</ts>
                  <nts id="Seg_2371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_2373" n="HIAT:w" s="T703">qumɨtɨt</ts>
                  <nts id="Seg_2374" n="HIAT:ip">,</nts>
                  <nts id="Seg_2375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2377" n="HIAT:w" s="T704">na</ts>
                  <nts id="Seg_2378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2380" n="HIAT:w" s="T705">qälɨn</ts>
                  <nts id="Seg_2381" n="HIAT:ip">.</nts>
                  <nts id="Seg_2382" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T710" id="Seg_2383" n="sc" s="T707">
               <ts e="T710" id="Seg_2385" n="HIAT:u" s="T707">
                  <ts e="T708" id="Seg_2387" n="HIAT:w" s="T707">Namɨn</ts>
                  <nts id="Seg_2388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_2390" n="HIAT:w" s="T708">ijaqı</ts>
                  <nts id="Seg_2391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2393" n="HIAT:w" s="T709">šʼittɨ</ts>
                  <nts id="Seg_2394" n="HIAT:ip">.</nts>
                  <nts id="Seg_2395" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T714" id="Seg_2396" n="sc" s="T711">
               <ts e="T714" id="Seg_2398" n="HIAT:u" s="T711">
                  <ts e="T712" id="Seg_2400" n="HIAT:w" s="T711">Nılʼčʼi</ts>
                  <nts id="Seg_2401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2403" n="HIAT:w" s="T712">šʼittɨ</ts>
                  <nts id="Seg_2404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2406" n="HIAT:w" s="T713">ijaqı</ts>
                  <nts id="Seg_2407" n="HIAT:ip">.</nts>
                  <nts id="Seg_2408" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T722" id="Seg_2409" n="sc" s="T715">
               <ts e="T722" id="Seg_2411" n="HIAT:u" s="T715">
                  <ts e="T716" id="Seg_2413" n="HIAT:w" s="T715">Ɔːtap</ts>
                  <nts id="Seg_2414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2416" n="HIAT:w" s="T716">tɔːqqɨla</ts>
                  <nts id="Seg_2417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2419" n="HIAT:w" s="T717">moqɨn</ts>
                  <nts id="Seg_2420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2422" n="HIAT:w" s="T718">tattɔːt</ts>
                  <nts id="Seg_2423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2425" n="HIAT:w" s="T719">tıntena</ts>
                  <nts id="Seg_2426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2427" n="HIAT:ip">(</nts>
                  <ts e="T721" id="Seg_2429" n="HIAT:w" s="T720">tına</ts>
                  <nts id="Seg_2430" n="HIAT:ip">)</nts>
                  <nts id="Seg_2431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_2433" n="HIAT:w" s="T721">mɔːttɨ</ts>
                  <nts id="Seg_2434" n="HIAT:ip">.</nts>
                  <nts id="Seg_2435" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T725" id="Seg_2436" n="sc" s="T723">
               <ts e="T725" id="Seg_2438" n="HIAT:u" s="T723">
                  <ts e="T724" id="Seg_2440" n="HIAT:w" s="T723">Tına</ts>
                  <nts id="Seg_2441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_2443" n="HIAT:w" s="T724">mɔːtɨŋnɔːt</ts>
                  <nts id="Seg_2444" n="HIAT:ip">.</nts>
                  <nts id="Seg_2445" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T731" id="Seg_2446" n="sc" s="T930">
               <ts e="T731" id="Seg_2448" n="HIAT:u" s="T930">
                  <nts id="Seg_2449" n="HIAT:ip">(</nts>
                  <nts id="Seg_2450" n="HIAT:ip">(</nts>
                  <ats e="T726" id="Seg_2451" n="HIAT:non-pho" s="T930">NN:</ats>
                  <nts id="Seg_2452" n="HIAT:ip">)</nts>
                  <nts id="Seg_2453" n="HIAT:ip">)</nts>
                  <nts id="Seg_2454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2456" n="HIAT:w" s="T726">Lʼa</ts>
                  <nts id="Seg_2457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_2459" n="HIAT:w" s="T727">na</ts>
                  <nts id="Seg_2460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2462" n="HIAT:w" s="T728">namɨlʼa</ts>
                  <nts id="Seg_2463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2465" n="HIAT:w" s="T729">nannɛr</ts>
                  <nts id="Seg_2466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2468" n="HIAT:w" s="T730">qəša</ts>
                  <nts id="Seg_2469" n="HIAT:ip">.</nts>
                  <nts id="Seg_2470" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T739" id="Seg_2471" n="sc" s="T929">
               <ts e="T739" id="Seg_2473" n="HIAT:u" s="T929">
                  <nts id="Seg_2474" n="HIAT:ip">(</nts>
                  <nts id="Seg_2475" n="HIAT:ip">(</nts>
                  <ats e="T732" id="Seg_2476" n="HIAT:non-pho" s="T929">NEP:</ats>
                  <nts id="Seg_2477" n="HIAT:ip">)</nts>
                  <nts id="Seg_2478" n="HIAT:ip">)</nts>
                  <nts id="Seg_2479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2481" n="HIAT:w" s="T732">Ijaiːtɨ</ts>
                  <nts id="Seg_2482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2484" n="HIAT:w" s="T733">tam</ts>
                  <nts id="Seg_2485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_2487" n="HIAT:w" s="T734">na</ts>
                  <nts id="Seg_2488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_2490" n="HIAT:w" s="T735">qənmmɨntɔːt</ts>
                  <nts id="Seg_2491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_2493" n="HIAT:w" s="T736">šöttɨ</ts>
                  <nts id="Seg_2494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_2496" n="HIAT:w" s="T737">täpällä</ts>
                  <nts id="Seg_2497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_2499" n="HIAT:w" s="T738">surɨllä</ts>
                  <nts id="Seg_2500" n="HIAT:ip">.</nts>
                  <nts id="Seg_2501" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T741" id="Seg_2502" n="sc" s="T740">
               <ts e="T741" id="Seg_2504" n="HIAT:u" s="T740">
                  <nts id="Seg_2505" n="HIAT:ip">(</nts>
                  <nts id="Seg_2506" n="HIAT:ip">(</nts>
                  <ats e="T740.tx.1" id="Seg_2507" n="HIAT:non-pho" s="T740">NN:</ats>
                  <nts id="Seg_2508" n="HIAT:ip">)</nts>
                  <nts id="Seg_2509" n="HIAT:ip">)</nts>
                  <nts id="Seg_2510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2511" n="HIAT:ip">(</nts>
                  <nts id="Seg_2512" n="HIAT:ip">(</nts>
                  <ats e="T741" id="Seg_2513" n="HIAT:non-pho" s="T740.tx.1">…</ats>
                  <nts id="Seg_2514" n="HIAT:ip">)</nts>
                  <nts id="Seg_2515" n="HIAT:ip">)</nts>
                  <nts id="Seg_2516" n="HIAT:ip">.</nts>
                  <nts id="Seg_2517" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T748" id="Seg_2518" n="sc" s="T742">
               <ts e="T748" id="Seg_2520" n="HIAT:u" s="T742">
                  <ts e="T743" id="Seg_2522" n="HIAT:w" s="T742">Konnä</ts>
                  <nts id="Seg_2523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_2525" n="HIAT:w" s="T743">mɔːtɨŋnɔːt</ts>
                  <nts id="Seg_2526" n="HIAT:ip">,</nts>
                  <nts id="Seg_2527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_2529" n="HIAT:w" s="T744">tal</ts>
                  <nts id="Seg_2530" n="HIAT:ip">,</nts>
                  <nts id="Seg_2531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_2533" n="HIAT:w" s="T745">ukkɨr</ts>
                  <nts id="Seg_2534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_2536" n="HIAT:w" s="T746">pit</ts>
                  <nts id="Seg_2537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_2539" n="HIAT:w" s="T747">šäqɔːt</ts>
                  <nts id="Seg_2540" n="HIAT:ip">.</nts>
                  <nts id="Seg_2541" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T750" id="Seg_2542" n="sc" s="T749">
               <ts e="T750" id="Seg_2544" n="HIAT:u" s="T749">
                  <ts e="T750" id="Seg_2546" n="HIAT:w" s="T749">Šäqɨqolamnɔːt</ts>
                  <nts id="Seg_2547" n="HIAT:ip">.</nts>
                  <nts id="Seg_2548" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T757" id="Seg_2549" n="sc" s="T751">
               <ts e="T757" id="Seg_2551" n="HIAT:u" s="T751">
                  <ts e="T752" id="Seg_2553" n="HIAT:w" s="T751">Ijaiːtɨ</ts>
                  <nts id="Seg_2554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_2556" n="HIAT:w" s="T752">šötqɨnɨ</ts>
                  <nts id="Seg_2557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_2559" n="HIAT:w" s="T753">jap</ts>
                  <nts id="Seg_2560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_2562" n="HIAT:w" s="T754">tattɔːtɨt</ts>
                  <nts id="Seg_2563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_2565" n="HIAT:w" s="T755">täpäčʼilʼmɨn</ts>
                  <nts id="Seg_2566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_2568" n="HIAT:w" s="T756">moqonä</ts>
                  <nts id="Seg_2569" n="HIAT:ip">.</nts>
                  <nts id="Seg_2570" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T760" id="Seg_2571" n="sc" s="T758">
               <ts e="T760" id="Seg_2573" n="HIAT:u" s="T758">
                  <ts e="T759" id="Seg_2575" n="HIAT:w" s="T758">Kəntalla</ts>
                  <nts id="Seg_2576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_2578" n="HIAT:w" s="T759">tattɔːt</ts>
                  <nts id="Seg_2579" n="HIAT:ip">.</nts>
                  <nts id="Seg_2580" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T763" id="Seg_2581" n="sc" s="T761">
               <ts e="T763" id="Seg_2583" n="HIAT:u" s="T761">
                  <ts e="T762" id="Seg_2585" n="HIAT:w" s="T761">Qaj</ts>
                  <nts id="Seg_2586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_2588" n="HIAT:w" s="T762">ɛsɨ</ts>
                  <nts id="Seg_2589" n="HIAT:ip">?</nts>
                  <nts id="Seg_2590" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T770" id="Seg_2591" n="sc" s="T764">
               <ts e="T770" id="Seg_2593" n="HIAT:u" s="T764">
                  <ts e="T765" id="Seg_2595" n="HIAT:w" s="T764">Əsɨ</ts>
                  <nts id="Seg_2596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_2598" n="HIAT:w" s="T765">ira</ts>
                  <nts id="Seg_2599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_2601" n="HIAT:w" s="T766">mɨt</ts>
                  <nts id="Seg_2602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T768" id="Seg_2604" n="HIAT:w" s="T767">tıntena</ts>
                  <nts id="Seg_2605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_2607" n="HIAT:w" s="T768">mɔːttɨ</ts>
                  <nts id="Seg_2608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_2610" n="HIAT:w" s="T769">təptɨt</ts>
                  <nts id="Seg_2611" n="HIAT:ip">.</nts>
                  <nts id="Seg_2612" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T784" id="Seg_2613" n="sc" s="T771">
               <ts e="T777" id="Seg_2615" n="HIAT:u" s="T771">
                  <ts e="T772" id="Seg_2617" n="HIAT:w" s="T771">Karrä</ts>
                  <nts id="Seg_2618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_2620" n="HIAT:w" s="T772">čʼam</ts>
                  <nts id="Seg_2621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_2623" n="HIAT:w" s="T773">mannɨmpɔːtɨt</ts>
                  <nts id="Seg_2624" n="HIAT:ip">,</nts>
                  <nts id="Seg_2625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_2627" n="HIAT:w" s="T774">koptɨkɔːl</ts>
                  <nts id="Seg_2628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_2630" n="HIAT:w" s="T775">tü</ts>
                  <nts id="Seg_2631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_2633" n="HIAT:w" s="T776">ampatɨ</ts>
                  <nts id="Seg_2634" n="HIAT:ip">.</nts>
                  <nts id="Seg_2635" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T784" id="Seg_2637" n="HIAT:u" s="T777">
                  <nts id="Seg_2638" n="HIAT:ip">“</nts>
                  <ts e="T778" id="Seg_2640" n="HIAT:w" s="T777">Əsɨ</ts>
                  <nts id="Seg_2641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_2643" n="HIAT:w" s="T778">ira</ts>
                  <nts id="Seg_2644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_2646" n="HIAT:w" s="T779">mɨt</ts>
                  <nts id="Seg_2647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_2649" n="HIAT:w" s="T780">qətpɔːt</ts>
                  <nts id="Seg_2650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_2652" n="HIAT:w" s="T781">təpɨn</ts>
                  <nts id="Seg_2653" n="HIAT:ip">”</nts>
                  <nts id="Seg_2654" n="HIAT:ip">,</nts>
                  <nts id="Seg_2655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2656" n="HIAT:ip">–</nts>
                  <nts id="Seg_2657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_2659" n="HIAT:w" s="T782">nık</ts>
                  <nts id="Seg_2660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_2662" n="HIAT:w" s="T783">tɛnɨrpɔːtɨt</ts>
                  <nts id="Seg_2663" n="HIAT:ip">.</nts>
                  <nts id="Seg_2664" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T788" id="Seg_2665" n="sc" s="T785">
               <ts e="T788" id="Seg_2667" n="HIAT:u" s="T785">
                  <ts e="T786" id="Seg_2669" n="HIAT:w" s="T785">Na</ts>
                  <nts id="Seg_2670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_2672" n="HIAT:w" s="T786">seːlʼčʼi</ts>
                  <nts id="Seg_2673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_2675" n="HIAT:w" s="T787">ijatɨ</ts>
                  <nts id="Seg_2676" n="HIAT:ip">.</nts>
                  <nts id="Seg_2677" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T790" id="Seg_2678" n="sc" s="T789">
               <ts e="T790" id="Seg_2680" n="HIAT:u" s="T789">
                  <ts e="T790" id="Seg_2682" n="HIAT:w" s="T789">Tal</ts>
                  <nts id="Seg_2683" n="HIAT:ip">.</nts>
                  <nts id="Seg_2684" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T793" id="Seg_2685" n="sc" s="T791">
               <ts e="T793" id="Seg_2687" n="HIAT:u" s="T791">
                  <ts e="T792" id="Seg_2689" n="HIAT:w" s="T791">Nɨnɨ</ts>
                  <nts id="Seg_2690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T793" id="Seg_2692" n="HIAT:w" s="T792">šittɨmtäl</ts>
                  <nts id="Seg_2693" n="HIAT:ip">…</nts>
                  <nts id="Seg_2694" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T796" id="Seg_2695" n="sc" s="T794">
               <ts e="T796" id="Seg_2697" n="HIAT:u" s="T794">
                  <ts e="T795" id="Seg_2699" n="HIAT:w" s="T794">Qəntɨla</ts>
                  <nts id="Seg_2700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_2702" n="HIAT:w" s="T795">tattɔːtɨt</ts>
                  <nts id="Seg_2703" n="HIAT:ip">.</nts>
                  <nts id="Seg_2704" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T803" id="Seg_2705" n="sc" s="T797">
               <ts e="T803" id="Seg_2707" n="HIAT:u" s="T797">
                  <ts e="T798" id="Seg_2709" n="HIAT:w" s="T797">Poqɨt</ts>
                  <nts id="Seg_2710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_2712" n="HIAT:w" s="T798">kanaktɨ</ts>
                  <nts id="Seg_2713" n="HIAT:ip">,</nts>
                  <nts id="Seg_2714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_2716" n="HIAT:w" s="T799">na</ts>
                  <nts id="Seg_2717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_2719" n="HIAT:w" s="T800">kɨpa</ts>
                  <nts id="Seg_2720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_2722" n="HIAT:w" s="T801">ijantɨ</ts>
                  <nts id="Seg_2723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_2725" n="HIAT:w" s="T802">kanak</ts>
                  <nts id="Seg_2726" n="HIAT:ip">.</nts>
                  <nts id="Seg_2727" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T807" id="Seg_2728" n="sc" s="T804">
               <ts e="T807" id="Seg_2730" n="HIAT:u" s="T804">
                  <ts e="T805" id="Seg_2732" n="HIAT:w" s="T804">Imatɨ</ts>
                  <nts id="Seg_2733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_2735" n="HIAT:w" s="T805">pon</ts>
                  <nts id="Seg_2736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_2738" n="HIAT:w" s="T806">sɔːrɨmpat</ts>
                  <nts id="Seg_2739" n="HIAT:ip">.</nts>
                  <nts id="Seg_2740" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T815" id="Seg_2741" n="sc" s="T808">
               <ts e="T815" id="Seg_2743" n="HIAT:u" s="T808">
                  <ts e="T809" id="Seg_2745" n="HIAT:w" s="T808">Ɔːtap</ts>
                  <nts id="Seg_2746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_2748" n="HIAT:w" s="T809">kutɨlʼ</ts>
                  <nts id="Seg_2749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_2751" n="HIAT:w" s="T810">ətɨmantɨn</ts>
                  <nts id="Seg_2752" n="HIAT:ip">,</nts>
                  <nts id="Seg_2753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_2755" n="HIAT:w" s="T811">na</ts>
                  <nts id="Seg_2756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T813" id="Seg_2758" n="HIAT:w" s="T812">qälɨn</ts>
                  <nts id="Seg_2759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T814" id="Seg_2761" n="HIAT:w" s="T813">ɔːtap</ts>
                  <nts id="Seg_2762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_2764" n="HIAT:w" s="T814">tɔːqqɔːtɨt</ts>
                  <nts id="Seg_2765" n="HIAT:ip">.</nts>
                  <nts id="Seg_2766" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T821" id="Seg_2767" n="sc" s="T816">
               <ts e="T821" id="Seg_2769" n="HIAT:u" s="T816">
                  <ts e="T817" id="Seg_2771" n="HIAT:w" s="T816">Kanat</ts>
                  <nts id="Seg_2772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_2774" n="HIAT:w" s="T817">topɨp</ts>
                  <nts id="Seg_2775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_2777" n="HIAT:w" s="T818">čʼıqɨlʼnɔːt</ts>
                  <nts id="Seg_2778" n="HIAT:ip">,</nts>
                  <nts id="Seg_2779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T820" id="Seg_2781" n="HIAT:w" s="T819">kanak</ts>
                  <nts id="Seg_2782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T821" id="Seg_2784" n="HIAT:w" s="T820">läqalʼnʼa</ts>
                  <nts id="Seg_2785" n="HIAT:ip">.</nts>
                  <nts id="Seg_2786" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T829" id="Seg_2787" n="sc" s="T822">
               <ts e="T829" id="Seg_2789" n="HIAT:u" s="T822">
                  <ts e="T823" id="Seg_2791" n="HIAT:w" s="T822">Ira</ts>
                  <nts id="Seg_2792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T824" id="Seg_2794" n="HIAT:w" s="T823">nılʼ</ts>
                  <nts id="Seg_2795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_2797" n="HIAT:w" s="T824">nülʼčʼa</ts>
                  <nts id="Seg_2798" n="HIAT:ip">,</nts>
                  <nts id="Seg_2799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_2801" n="HIAT:w" s="T825">Nʼomalʼ</ts>
                  <nts id="Seg_2802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_2804" n="HIAT:w" s="T826">porqɨ</ts>
                  <nts id="Seg_2805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T828" id="Seg_2807" n="HIAT:w" s="T827">ira</ts>
                  <nts id="Seg_2808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T829" id="Seg_2810" n="HIAT:w" s="T828">mɨta</ts>
                  <nts id="Seg_2811" n="HIAT:ip">.</nts>
                  <nts id="Seg_2812" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T836" id="Seg_2813" n="sc" s="T830">
               <ts e="T836" id="Seg_2815" n="HIAT:u" s="T830">
                  <ts e="T831" id="Seg_2817" n="HIAT:w" s="T830">Kanamɨ</ts>
                  <nts id="Seg_2818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_2820" n="HIAT:w" s="T831">qaj</ts>
                  <nts id="Seg_2821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2822" n="HIAT:ip">(</nts>
                  <ts e="T833" id="Seg_2824" n="HIAT:w" s="T832">ampatɨ</ts>
                  <nts id="Seg_2825" n="HIAT:ip">)</nts>
                  <nts id="Seg_2826" n="HIAT:ip">,</nts>
                  <nts id="Seg_2827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_2829" n="HIAT:w" s="T833">qaj</ts>
                  <nts id="Seg_2830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_2832" n="HIAT:w" s="T834">ɔːtä</ts>
                  <nts id="Seg_2833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T836" id="Seg_2835" n="HIAT:w" s="T835">pačʼalnɨt</ts>
                  <nts id="Seg_2836" n="HIAT:ip">?</nts>
                  <nts id="Seg_2837" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T843" id="Seg_2838" n="sc" s="T837">
               <ts e="T843" id="Seg_2840" n="HIAT:u" s="T837">
                  <ts e="T838" id="Seg_2842" n="HIAT:w" s="T837">Pija</ts>
                  <nts id="Seg_2843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T839" id="Seg_2845" n="HIAT:w" s="T838">kɨpa</ts>
                  <nts id="Seg_2846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_2848" n="HIAT:w" s="T839">ijat</ts>
                  <nts id="Seg_2849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T841" id="Seg_2851" n="HIAT:w" s="T840">ima</ts>
                  <nts id="Seg_2852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_2854" n="HIAT:w" s="T841">ponä</ts>
                  <nts id="Seg_2855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_2857" n="HIAT:w" s="T842">pakta</ts>
                  <nts id="Seg_2858" n="HIAT:ip">.</nts>
                  <nts id="Seg_2859" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T850" id="Seg_2860" n="sc" s="T844">
               <ts e="T850" id="Seg_2862" n="HIAT:u" s="T844">
                  <ts e="T845" id="Seg_2864" n="HIAT:w" s="T844">Na</ts>
                  <nts id="Seg_2865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_2867" n="HIAT:w" s="T845">nʼomalʼ</ts>
                  <nts id="Seg_2868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_2870" n="HIAT:w" s="T846">porqɨ</ts>
                  <nts id="Seg_2871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T848" id="Seg_2873" n="HIAT:w" s="T847">iratɨ</ts>
                  <nts id="Seg_2874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T849" id="Seg_2876" n="HIAT:w" s="T848">kɨpa</ts>
                  <nts id="Seg_2877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T850" id="Seg_2879" n="HIAT:w" s="T849">ijatɨ</ts>
                  <nts id="Seg_2880" n="HIAT:ip">.</nts>
                  <nts id="Seg_2881" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T857" id="Seg_2882" n="sc" s="T851">
               <ts e="T857" id="Seg_2884" n="HIAT:u" s="T851">
                  <ts e="T852" id="Seg_2886" n="HIAT:w" s="T851">Ponä</ts>
                  <nts id="Seg_2887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T853" id="Seg_2889" n="HIAT:w" s="T852">čʼam</ts>
                  <nts id="Seg_2890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_2892" n="HIAT:w" s="T853">pakta</ts>
                  <nts id="Seg_2893" n="HIAT:ip">,</nts>
                  <nts id="Seg_2894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T855" id="Seg_2896" n="HIAT:w" s="T854">qaj</ts>
                  <nts id="Seg_2897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856" id="Seg_2899" n="HIAT:w" s="T855">ɛsa</ts>
                  <nts id="Seg_2900" n="HIAT:ip">,</nts>
                  <nts id="Seg_2901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T857" id="Seg_2903" n="HIAT:w" s="T856">iraqotatɨ</ts>
                  <nts id="Seg_2904" n="HIAT:ip">?</nts>
                  <nts id="Seg_2905" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T862" id="Seg_2906" n="sc" s="T858">
               <ts e="T862" id="Seg_2908" n="HIAT:u" s="T858">
                  <ts e="T859" id="Seg_2910" n="HIAT:w" s="T858">Na</ts>
                  <nts id="Seg_2911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T860" id="Seg_2913" n="HIAT:w" s="T859">qaj</ts>
                  <nts id="Seg_2914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T861" id="Seg_2916" n="HIAT:w" s="T860">qumɨt</ts>
                  <nts id="Seg_2917" n="HIAT:ip">,</nts>
                  <nts id="Seg_2918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T862" id="Seg_2920" n="HIAT:w" s="T861">kəːŋa</ts>
                  <nts id="Seg_2921" n="HIAT:ip">.</nts>
                  <nts id="Seg_2922" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T869" id="Seg_2923" n="sc" s="T863">
               <ts e="T869" id="Seg_2925" n="HIAT:u" s="T863">
                  <ts e="T864" id="Seg_2927" n="HIAT:w" s="T863">Əsɨ</ts>
                  <nts id="Seg_2928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T865" id="Seg_2930" n="HIAT:w" s="T864">irat</ts>
                  <nts id="Seg_2931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T866" id="Seg_2933" n="HIAT:w" s="T865">tam</ts>
                  <nts id="Seg_2934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T867" id="Seg_2936" n="HIAT:w" s="T866">nülʼčʼɨku</ts>
                  <nts id="Seg_2937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T868" id="Seg_2939" n="HIAT:w" s="T867">na</ts>
                  <nts id="Seg_2940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T869" id="Seg_2942" n="HIAT:w" s="T868">mɔːtqɨn</ts>
                  <nts id="Seg_2943" n="HIAT:ip">.</nts>
                  <nts id="Seg_2944" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T879" id="Seg_2945" n="sc" s="T870">
               <ts e="T879" id="Seg_2947" n="HIAT:u" s="T870">
                  <ts e="T871" id="Seg_2949" n="HIAT:w" s="T870">Mompa</ts>
                  <nts id="Seg_2950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T872" id="Seg_2952" n="HIAT:w" s="T871">meː</ts>
                  <nts id="Seg_2953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T873" id="Seg_2955" n="HIAT:w" s="T872">mompa</ts>
                  <nts id="Seg_2956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T874" id="Seg_2958" n="HIAT:w" s="T873">mɔːnmɨn</ts>
                  <nts id="Seg_2959" n="HIAT:ip">,</nts>
                  <nts id="Seg_2960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T875" id="Seg_2962" n="HIAT:w" s="T874">mompa</ts>
                  <nts id="Seg_2963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_2965" n="HIAT:w" s="T875">meː</ts>
                  <nts id="Seg_2966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T877" id="Seg_2968" n="HIAT:w" s="T876">tüs</ts>
                  <nts id="Seg_2969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T878" id="Seg_2971" n="HIAT:w" s="T877">čʼɔːtɨsɨmɨn</ts>
                  <nts id="Seg_2972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T879" id="Seg_2974" n="HIAT:w" s="T878">mɨta</ts>
                  <nts id="Seg_2975" n="HIAT:ip">.</nts>
                  <nts id="Seg_2976" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T885" id="Seg_2977" n="sc" s="T880">
               <ts e="T885" id="Seg_2979" n="HIAT:u" s="T880">
                  <ts e="T881" id="Seg_2981" n="HIAT:w" s="T880">Ilʼčʼa</ts>
                  <nts id="Seg_2982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T882" id="Seg_2984" n="HIAT:w" s="T881">mompa</ts>
                  <nts id="Seg_2985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T883" id="Seg_2987" n="HIAT:w" s="T882">nılʼ</ts>
                  <nts id="Seg_2988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T884" id="Seg_2990" n="HIAT:w" s="T883">šımɨt</ts>
                  <nts id="Seg_2991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2992" n="HIAT:ip">(</nts>
                  <nts id="Seg_2993" n="HIAT:ip">(</nts>
                  <ats e="T885" id="Seg_2994" n="HIAT:non-pho" s="T884">…</ats>
                  <nts id="Seg_2995" n="HIAT:ip">)</nts>
                  <nts id="Seg_2996" n="HIAT:ip">)</nts>
                  <nts id="Seg_2997" n="HIAT:ip">.</nts>
                  <nts id="Seg_2998" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T892" id="Seg_2999" n="sc" s="T886">
               <ts e="T890" id="Seg_3001" n="HIAT:u" s="T886">
                  <nts id="Seg_3002" n="HIAT:ip">(</nts>
                  <nts id="Seg_3003" n="HIAT:ip">(</nts>
                  <ats e="T886.tx.1" id="Seg_3004" n="HIAT:non-pho" s="T886">NN:</ats>
                  <nts id="Seg_3005" n="HIAT:ip">)</nts>
                  <nts id="Seg_3006" n="HIAT:ip">)</nts>
                  <nts id="Seg_3007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T886.tx.2" id="Seg_3009" n="HIAT:w" s="T886.tx.1">Сначала</ts>
                  <nts id="Seg_3010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T886.tx.3" id="Seg_3012" n="HIAT:w" s="T886.tx.2">ли</ts>
                  <nts id="Seg_3013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T886.tx.4" id="Seg_3015" n="HIAT:w" s="T886.tx.3">что</ts>
                  <nts id="Seg_3016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T890" id="Seg_3018" n="HIAT:w" s="T886.tx.4">ли</ts>
                  <nts id="Seg_3019" n="HIAT:ip">?</nts>
                  <nts id="Seg_3020" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T892" id="Seg_3022" n="HIAT:u" s="T890">
                  <nts id="Seg_3023" n="HIAT:ip">(</nts>
                  <nts id="Seg_3024" n="HIAT:ip">(</nts>
                  <ats e="T890.tx.1" id="Seg_3025" n="HIAT:non-pho" s="T890">KuAI:</ats>
                  <nts id="Seg_3026" n="HIAT:ip">)</nts>
                  <nts id="Seg_3027" n="HIAT:ip">)</nts>
                  <nts id="Seg_3028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T890.tx.2" id="Seg_3030" n="HIAT:w" s="T890.tx.1">Говорите</ts>
                  <nts id="Seg_3031" n="HIAT:ip">,</nts>
                  <nts id="Seg_3032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T892" id="Seg_3034" n="HIAT:w" s="T890.tx.2">нет</ts>
                  <nts id="Seg_3035" n="HIAT:ip">.</nts>
                  <nts id="Seg_3036" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T896" id="Seg_3037" n="sc" s="T931">
               <ts e="T896" id="Seg_3039" n="HIAT:u" s="T931">
                  <nts id="Seg_3040" n="HIAT:ip">(</nts>
                  <nts id="Seg_3041" n="HIAT:ip">(</nts>
                  <ats e="T893" id="Seg_3042" n="HIAT:non-pho" s="T931">NN:</ats>
                  <nts id="Seg_3043" n="HIAT:ip">)</nts>
                  <nts id="Seg_3044" n="HIAT:ip">)</nts>
                  <nts id="Seg_3045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T894" id="Seg_3047" n="HIAT:w" s="T893">Kɨsa</ts>
                  <nts id="Seg_3048" n="HIAT:ip">,</nts>
                  <nts id="Seg_3049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T895" id="Seg_3051" n="HIAT:w" s="T894">nʼenna</ts>
                  <nts id="Seg_3052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T896" id="Seg_3054" n="HIAT:w" s="T895">tomtɨ</ts>
                  <nts id="Seg_3055" n="HIAT:ip">.</nts>
                  <nts id="Seg_3056" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T910" id="Seg_3057" n="sc" s="T932">
               <ts e="T910" id="Seg_3059" n="HIAT:u" s="T932">
                  <nts id="Seg_3060" n="HIAT:ip">(</nts>
                  <nts id="Seg_3061" n="HIAT:ip">(</nts>
                  <ats e="T897" id="Seg_3062" n="HIAT:non-pho" s="T932">NEP:</ats>
                  <nts id="Seg_3063" n="HIAT:ip">)</nts>
                  <nts id="Seg_3064" n="HIAT:ip">)</nts>
                  <nts id="Seg_3065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898" id="Seg_3067" n="HIAT:w" s="T897">Nɨn</ts>
                  <nts id="Seg_3068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T899" id="Seg_3070" n="HIAT:w" s="T898">nɔːtɨ</ts>
                  <nts id="Seg_3071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T900" id="Seg_3073" n="HIAT:w" s="T899">na</ts>
                  <nts id="Seg_3074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T901" id="Seg_3076" n="HIAT:w" s="T900">ilɨmmɨntɔːt</ts>
                  <nts id="Seg_3077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T902" id="Seg_3079" n="HIAT:w" s="T901">nɨmtɨ</ts>
                  <nts id="Seg_3080" n="HIAT:ip">,</nts>
                  <nts id="Seg_3081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T903" id="Seg_3083" n="HIAT:w" s="T902">na</ts>
                  <nts id="Seg_3084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T904" id="Seg_3086" n="HIAT:w" s="T903">qapija</ts>
                  <nts id="Seg_3087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T905" id="Seg_3089" n="HIAT:w" s="T904">ɛj</ts>
                  <nts id="Seg_3090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T906" id="Seg_3092" n="HIAT:w" s="T905">älpä</ts>
                  <nts id="Seg_3093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T907" id="Seg_3095" n="HIAT:w" s="T906">qənpɔːtɨt</ts>
                  <nts id="Seg_3096" n="HIAT:ip">,</nts>
                  <nts id="Seg_3097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T908" id="Seg_3099" n="HIAT:w" s="T907">na</ts>
                  <nts id="Seg_3100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909" id="Seg_3102" n="HIAT:w" s="T908">šölʼqumɨt</ts>
                  <nts id="Seg_3103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T910" id="Seg_3105" n="HIAT:w" s="T909">tına</ts>
                  <nts id="Seg_3106" n="HIAT:ip">.</nts>
                  <nts id="Seg_3107" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T916" id="Seg_3108" n="sc" s="T911">
               <ts e="T916" id="Seg_3110" n="HIAT:u" s="T911">
                  <ts e="T912" id="Seg_3112" n="HIAT:w" s="T911">Qapija</ts>
                  <nts id="Seg_3113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T913" id="Seg_3115" n="HIAT:w" s="T912">qälɨj</ts>
                  <nts id="Seg_3116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T914" id="Seg_3118" n="HIAT:w" s="T913">təttot</ts>
                  <nts id="Seg_3119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T915" id="Seg_3121" n="HIAT:w" s="T914">ɔːtap</ts>
                  <nts id="Seg_3122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T916" id="Seg_3124" n="HIAT:w" s="T915">tɔːqqɨmmɨntɔːt</ts>
                  <nts id="Seg_3125" n="HIAT:ip">.</nts>
                  <nts id="Seg_3126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T920" id="Seg_3127" n="sc" s="T917">
               <ts e="T920" id="Seg_3129" n="HIAT:u" s="T917">
                  <ts e="T918" id="Seg_3131" n="HIAT:w" s="T917">Aj</ts>
                  <nts id="Seg_3132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T919" id="Seg_3134" n="HIAT:w" s="T918">na</ts>
                  <nts id="Seg_3135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T920" id="Seg_3137" n="HIAT:w" s="T919">šölʼqup</ts>
                  <nts id="Seg_3138" n="HIAT:ip">…</nts>
                  <nts id="Seg_3139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T5" id="Seg_3140" n="sc" s="T1">
               <ts e="T5" id="Seg_3142" n="e" s="T1">((KuAI:)) Сказка, записанная Елизаветой Павловной. </ts>
            </ts>
            <ts e="T9" id="Seg_3143" n="sc" s="T6">
               <ts e="T9" id="Seg_3145" n="e" s="T6">Ну пожалуйста, говорите. </ts>
            </ts>
            <ts e="T15" id="Seg_3146" n="sc" s="T921">
               <ts e="T10" id="Seg_3148" n="e" s="T921">((NN:)) </ts>
               <ts e="T11" id="Seg_3150" n="e" s="T10">Ksa </ts>
               <ts e="T12" id="Seg_3152" n="e" s="T11">kətät </ts>
               <ts e="T13" id="Seg_3154" n="e" s="T12">nɔːt </ts>
               <ts e="T14" id="Seg_3156" n="e" s="T13">(jarɨk) </ts>
               <ts e="T15" id="Seg_3158" n="e" s="T14">qaj. </ts>
            </ts>
            <ts e="T23" id="Seg_3159" n="sc" s="T922">
               <ts e="T16" id="Seg_3161" n="e" s="T922">((NEP:)) </ts>
               <ts e="T17" id="Seg_3163" n="e" s="T16">Mɨ </ts>
               <ts e="T18" id="Seg_3165" n="e" s="T17">tına </ts>
               <ts e="T19" id="Seg_3167" n="e" s="T18">Nʼomalʼ </ts>
               <ts e="T20" id="Seg_3169" n="e" s="T19">porqɨ </ts>
               <ts e="T21" id="Seg_3171" n="e" s="T20">ira </ts>
               <ts e="T22" id="Seg_3173" n="e" s="T21">qos </ts>
               <ts e="T23" id="Seg_3175" n="e" s="T22">kətɨšak. </ts>
            </ts>
            <ts e="T32" id="Seg_3176" n="sc" s="T24">
               <ts e="T25" id="Seg_3178" n="e" s="T24">Nʼomalʼ </ts>
               <ts e="T26" id="Seg_3180" n="e" s="T25">porqɨ </ts>
               <ts e="T27" id="Seg_3182" n="e" s="T26">ira </ts>
               <ts e="T28" id="Seg_3184" n="e" s="T27">tına, </ts>
               <ts e="T29" id="Seg_3186" n="e" s="T28">isɛčʼar </ts>
               <ts e="T30" id="Seg_3188" n="e" s="T29">selʼčʼɨ </ts>
               <ts e="T31" id="Seg_3190" n="e" s="T30">ijatɨ </ts>
               <ts e="T32" id="Seg_3192" n="e" s="T31">ɛppɨntɨ. </ts>
            </ts>
            <ts e="T36" id="Seg_3193" n="sc" s="T33">
               <ts e="T34" id="Seg_3195" n="e" s="T33">Aj </ts>
               <ts e="T35" id="Seg_3197" n="e" s="T34">selʼčʼi </ts>
               <ts e="T36" id="Seg_3199" n="e" s="T35">ämnätɨ. </ts>
            </ts>
            <ts e="T39" id="Seg_3200" n="sc" s="T37">
               <ts e="T38" id="Seg_3202" n="e" s="T37">Qarɨt </ts>
               <ts e="T39" id="Seg_3204" n="e" s="T38">tüŋɔːtɨt. </ts>
            </ts>
            <ts e="T46" id="Seg_3205" n="sc" s="T40">
               <ts e="T41" id="Seg_3207" n="e" s="T40">Qälɨt </ts>
               <ts e="T42" id="Seg_3209" n="e" s="T41">takkɨn </ts>
               <ts e="T43" id="Seg_3211" n="e" s="T42">nʼenna </ts>
               <ts e="T44" id="Seg_3213" n="e" s="T43">tüŋɔːtɨn </ts>
               <ts e="T45" id="Seg_3215" n="e" s="T44">təpɨp </ts>
               <ts e="T46" id="Seg_3217" n="e" s="T45">qətqɨntoːqo. </ts>
            </ts>
            <ts e="T52" id="Seg_3218" n="sc" s="T47">
               <ts e="T48" id="Seg_3220" n="e" s="T47">Qumiːtɨ, </ts>
               <ts e="T49" id="Seg_3222" n="e" s="T48">ijaiːtɨ </ts>
               <ts e="T50" id="Seg_3224" n="e" s="T49">mačʼa </ts>
               <ts e="T51" id="Seg_3226" n="e" s="T50">qənpɔːtɨt </ts>
               <ts e="T52" id="Seg_3228" n="e" s="T51">täpällä. </ts>
            </ts>
            <ts e="T64" id="Seg_3229" n="sc" s="T53">
               <ts e="T54" id="Seg_3231" n="e" s="T53">Täpällä </ts>
               <ts e="T55" id="Seg_3233" n="e" s="T54">mačʼo </ts>
               <ts e="T56" id="Seg_3235" n="e" s="T55">qənpɔːt </ts>
               <ts e="T57" id="Seg_3237" n="e" s="T56">tıː </ts>
               <ts e="T58" id="Seg_3239" n="e" s="T57">qup, </ts>
               <ts e="T59" id="Seg_3241" n="e" s="T58">tıː </ts>
               <ts e="T60" id="Seg_3243" n="e" s="T59">kət, </ts>
               <ts e="T61" id="Seg_3245" n="e" s="T60">tiː </ts>
               <ts e="T62" id="Seg_3247" n="e" s="T61">kuttar </ts>
               <ts e="T63" id="Seg_3249" n="e" s="T62">ɛkka </ts>
               <ts e="T64" id="Seg_3251" n="e" s="T63">kət. </ts>
            </ts>
            <ts e="T72" id="Seg_3252" n="sc" s="T65">
               <ts e="T66" id="Seg_3254" n="e" s="T65">Tına </ts>
               <ts e="T67" id="Seg_3256" n="e" s="T66">ijap </ts>
               <ts e="T68" id="Seg_3258" n="e" s="T67">šʼolqumɨj </ts>
               <ts e="T69" id="Seg_3260" n="e" s="T68">ijap </ts>
               <ts e="T70" id="Seg_3262" n="e" s="T69">wərɨmmɨntɨ </ts>
               <ts e="T71" id="Seg_3264" n="e" s="T70">tına </ts>
               <ts e="T72" id="Seg_3266" n="e" s="T71">qälɨk. </ts>
            </ts>
            <ts e="T75" id="Seg_3267" n="sc" s="T73">
               <ts e="T74" id="Seg_3269" n="e" s="T73">Na </ts>
               <ts e="T75" id="Seg_3271" n="e" s="T74">mütɨntɨt. </ts>
            </ts>
            <ts e="T90" id="Seg_3272" n="sc" s="T76">
               <ts e="T77" id="Seg_3274" n="e" s="T76">Nʼomalʼ </ts>
               <ts e="T78" id="Seg_3276" n="e" s="T77">porqɨ </ts>
               <ts e="T79" id="Seg_3278" n="e" s="T78">ira </ts>
               <ts e="T80" id="Seg_3280" n="e" s="T79">mɨta </ts>
               <ts e="T81" id="Seg_3282" n="e" s="T80">täːlɨ </ts>
               <ts e="T82" id="Seg_3284" n="e" s="T81">mɨta </ts>
               <ts e="T83" id="Seg_3286" n="e" s="T82">meː </ts>
               <ts e="T84" id="Seg_3288" n="e" s="T83">tüntɔːmɨt </ts>
               <ts e="T85" id="Seg_3290" n="e" s="T84">təpɨn </ts>
               <ts e="T86" id="Seg_3292" n="e" s="T85">mɔːttɨ, </ts>
               <ts e="T87" id="Seg_3294" n="e" s="T86">tɛː </ts>
               <ts e="T88" id="Seg_3296" n="e" s="T87">mɨta </ts>
               <ts e="T89" id="Seg_3298" n="e" s="T88">qonniŋɨlɨt </ts>
               <ts e="T90" id="Seg_3300" n="e" s="T89">toː. </ts>
            </ts>
            <ts e="T100" id="Seg_3301" n="sc" s="T91">
               <ts e="T92" id="Seg_3303" n="e" s="T91">Ijaqı </ts>
               <ts e="T93" id="Seg_3305" n="e" s="T92">moqonä </ts>
               <ts e="T94" id="Seg_3307" n="e" s="T93">qəllä, </ts>
               <ts e="T95" id="Seg_3309" n="e" s="T94">nık </ts>
               <ts e="T96" id="Seg_3311" n="e" s="T95">kətɨŋɨtɨ, </ts>
               <ts e="T97" id="Seg_3313" n="e" s="T96">čʼilʼalʼ </ts>
               <ts e="T98" id="Seg_3315" n="e" s="T97">ijaqı </ts>
               <ts e="T99" id="Seg_3317" n="e" s="T98">qälɨk </ts>
               <ts e="T100" id="Seg_3319" n="e" s="T99">mərɨmɨntɨj. </ts>
            </ts>
            <ts e="T104" id="Seg_3320" n="sc" s="T101">
               <ts e="T102" id="Seg_3322" n="e" s="T101">Tam </ts>
               <ts e="T103" id="Seg_3324" n="e" s="T102">šölʼqumɨj </ts>
               <ts e="T104" id="Seg_3326" n="e" s="T103">ijaqı… </ts>
            </ts>
            <ts e="T112" id="Seg_3327" n="sc" s="T105">
               <ts e="T106" id="Seg_3329" n="e" s="T105">Taŋaltäš, </ts>
               <ts e="T107" id="Seg_3331" n="e" s="T106">taŋaltäš </ts>
               <ts e="T108" id="Seg_3333" n="e" s="T107">lʼi </ts>
               <ts e="T109" id="Seg_3335" n="e" s="T108">qaj </ts>
               <ts e="T110" id="Seg_3337" n="e" s="T109">kanap </ts>
               <ts e="T111" id="Seg_3339" n="e" s="T110">qaj </ts>
               <ts e="T112" id="Seg_3341" n="e" s="T111">(čʼütalʼalta). </ts>
            </ts>
            <ts e="T114" id="Seg_3342" n="sc" s="T113">
               <ts e="T114" id="Seg_3344" n="e" s="T113">Tal. </ts>
            </ts>
            <ts e="T120" id="Seg_3345" n="sc" s="T115">
               <ts e="T116" id="Seg_3347" n="e" s="T115">Nɨnɨ </ts>
               <ts e="T117" id="Seg_3349" n="e" s="T116">šittäl </ts>
               <ts e="T118" id="Seg_3351" n="e" s="T117">ira </ts>
               <ts e="T119" id="Seg_3353" n="e" s="T118">kučʼčʼa </ts>
               <ts e="T120" id="Seg_3355" n="e" s="T119">qattɛnta? </ts>
            </ts>
            <ts e="T126" id="Seg_3356" n="sc" s="T121">
               <ts e="T122" id="Seg_3358" n="e" s="T121">Ira </ts>
               <ts e="T123" id="Seg_3360" n="e" s="T122">qapija </ts>
               <ts e="T124" id="Seg_3362" n="e" s="T123">kekkɨsa </ts>
               <ts e="T125" id="Seg_3364" n="e" s="T124">na </ts>
               <ts e="T126" id="Seg_3366" n="e" s="T125">ippɨnta. </ts>
            </ts>
            <ts e="T135" id="Seg_3367" n="sc" s="T127">
               <ts e="T128" id="Seg_3369" n="e" s="T127">Ira </ts>
               <ts e="T129" id="Seg_3371" n="e" s="T128">nılʼčʼiŋ </ts>
               <ts e="T130" id="Seg_3373" n="e" s="T129">na </ts>
               <ts e="T131" id="Seg_3375" n="e" s="T130">ɛsɨsɨ, </ts>
               <ts e="T132" id="Seg_3377" n="e" s="T131">na </ts>
               <ts e="T133" id="Seg_3379" n="e" s="T132">Nʼomalʼ </ts>
               <ts e="T134" id="Seg_3381" n="e" s="T133">porqɨlʼ </ts>
               <ts e="T135" id="Seg_3383" n="e" s="T134">ira. </ts>
            </ts>
            <ts e="T143" id="Seg_3384" n="sc" s="T136">
               <ts e="T137" id="Seg_3386" n="e" s="T136">Nılʼčʼik </ts>
               <ts e="T138" id="Seg_3388" n="e" s="T137">ɛsa </ts>
               <ts e="T139" id="Seg_3390" n="e" s="T138">amnäntɨlʼ </ts>
               <ts e="T140" id="Seg_3392" n="e" s="T139">mɨtkine: </ts>
               <ts e="T141" id="Seg_3394" n="e" s="T140">“Apsɨp </ts>
               <ts e="T142" id="Seg_3396" n="e" s="T141">tattɨŋɨt </ts>
               <ts e="T143" id="Seg_3398" n="e" s="T142">karrä. </ts>
            </ts>
            <ts e="T154" id="Seg_3399" n="sc" s="T144">
               <ts e="T145" id="Seg_3401" n="e" s="T144">Ür, </ts>
               <ts e="T146" id="Seg_3403" n="e" s="T145">apsɨp </ts>
               <ts e="T147" id="Seg_3405" n="e" s="T146">tattɨŋɨt, </ts>
               <ts e="T148" id="Seg_3407" n="e" s="T147">kuššalʼ </ts>
               <ts e="T149" id="Seg_3409" n="e" s="T148">tɛlɨlʼ </ts>
               <ts e="T150" id="Seg_3411" n="e" s="T149">apsɨp </ts>
               <ts e="T151" id="Seg_3413" n="e" s="T150">kun </ts>
               <ts e="T152" id="Seg_3415" n="e" s="T151">montɨ </ts>
               <ts e="T153" id="Seg_3417" n="e" s="T152">qaj </ts>
               <ts e="T154" id="Seg_3419" n="e" s="T153">tattɨŋɨt. </ts>
            </ts>
            <ts e="T159" id="Seg_3420" n="sc" s="T155">
               <ts e="T156" id="Seg_3422" n="e" s="T155">Qälɨt </ts>
               <ts e="T157" id="Seg_3424" n="e" s="T156">täːlɨ </ts>
               <ts e="T158" id="Seg_3426" n="e" s="T157">tüntɔːtɨt, </ts>
               <ts e="T159" id="Seg_3428" n="e" s="T158">amɨrtɛntɔːtɨt”. </ts>
            </ts>
            <ts e="T163" id="Seg_3429" n="sc" s="T160">
               <ts e="T161" id="Seg_3431" n="e" s="T160">Ira </ts>
               <ts e="T162" id="Seg_3433" n="e" s="T161">nık </ts>
               <ts e="T163" id="Seg_3435" n="e" s="T162">kətɨŋɨt. </ts>
            </ts>
            <ts e="T166" id="Seg_3436" n="sc" s="T164">
               <ts e="T165" id="Seg_3438" n="e" s="T164">Apsɨp </ts>
               <ts e="T166" id="Seg_3440" n="e" s="T165">tattɔːtɨt. </ts>
            </ts>
            <ts e="T177" id="Seg_3441" n="sc" s="T167">
               <ts e="T168" id="Seg_3443" n="e" s="T167">Tıntena </ts>
               <ts e="T169" id="Seg_3445" n="e" s="T168">tap </ts>
               <ts e="T170" id="Seg_3447" n="e" s="T169">nʼuːtɨsa </ts>
               <ts e="T171" id="Seg_3449" n="e" s="T170">tap </ts>
               <ts e="T172" id="Seg_3451" n="e" s="T171">mɔːt </ts>
               <ts e="T173" id="Seg_3453" n="e" s="T172">šünʼčʼɨ </ts>
               <ts e="T174" id="Seg_3455" n="e" s="T173">šentɨ </ts>
               <ts e="T175" id="Seg_3457" n="e" s="T174">nʼuːtɨsa </ts>
               <ts e="T176" id="Seg_3459" n="e" s="T175">tɔːqqɔːtɨt </ts>
               <ts e="T177" id="Seg_3461" n="e" s="T176">tına. </ts>
            </ts>
            <ts e="T181" id="Seg_3462" n="sc" s="T178">
               <ts e="T179" id="Seg_3464" n="e" s="T178">Ira </ts>
               <ts e="T180" id="Seg_3466" n="e" s="T179">qälim </ts>
               <ts e="T181" id="Seg_3468" n="e" s="T180">apsɨtɨqolamnɨt. </ts>
            </ts>
            <ts e="T191" id="Seg_3469" n="sc" s="T182">
               <ts e="T183" id="Seg_3471" n="e" s="T182">Qəə, </ts>
               <ts e="T184" id="Seg_3473" n="e" s="T183">toptɨlʼ </ts>
               <ts e="T185" id="Seg_3475" n="e" s="T184">čʼeːlɨ </ts>
               <ts e="T186" id="Seg_3477" n="e" s="T185">qälɨn </ts>
               <ts e="T187" id="Seg_3479" n="e" s="T186">(na </ts>
               <ts e="T188" id="Seg_3481" n="e" s="T187">jap), </ts>
               <ts e="T189" id="Seg_3483" n="e" s="T188">nɔːssarɨlʼ </ts>
               <ts e="T190" id="Seg_3485" n="e" s="T189">qumɨlʼ </ts>
               <ts e="T191" id="Seg_3487" n="e" s="T190">müːtɨ. </ts>
            </ts>
            <ts e="T202" id="Seg_3488" n="sc" s="T192">
               <ts e="T193" id="Seg_3490" n="e" s="T192">Tomorot, </ts>
               <ts e="T194" id="Seg_3492" n="e" s="T193">qapı </ts>
               <ts e="T195" id="Seg_3494" n="e" s="T194">šünʼčʼipɨlʼ </ts>
               <ts e="T196" id="Seg_3496" n="e" s="T195">mɔːtqɨt </ts>
               <ts e="T197" id="Seg_3498" n="e" s="T196">Nʼomalʼ </ts>
               <ts e="T198" id="Seg_3500" n="e" s="T197">porqɨ </ts>
               <ts e="T199" id="Seg_3502" n="e" s="T198">ira </ts>
               <ts e="T200" id="Seg_3504" n="e" s="T199">ontɨ </ts>
               <ts e="T201" id="Seg_3506" n="e" s="T200">qala </ts>
               <ts e="T202" id="Seg_3508" n="e" s="T201">ämnäiːntɨsa. </ts>
            </ts>
            <ts e="T206" id="Seg_3509" n="sc" s="T203">
               <ts e="T204" id="Seg_3511" n="e" s="T203">Selʼčʼi </ts>
               <ts e="T205" id="Seg_3513" n="e" s="T204">nʼuːtɨsa </ts>
               <ts e="T206" id="Seg_3515" n="e" s="T205">tɔːqqɨraltɨŋɨt. </ts>
            </ts>
            <ts e="T210" id="Seg_3516" n="sc" s="T207">
               <ts e="T208" id="Seg_3518" n="e" s="T207">Ira </ts>
               <ts e="T209" id="Seg_3520" n="e" s="T208">nılʼčʼik </ts>
               <ts e="T210" id="Seg_3522" n="e" s="T209">ɛsa. </ts>
            </ts>
            <ts e="T218" id="Seg_3523" n="sc" s="T211">
               <ts e="T212" id="Seg_3525" n="e" s="T211">Mɔːt </ts>
               <ts e="T213" id="Seg_3527" n="e" s="T212">jap </ts>
               <ts e="T214" id="Seg_3529" n="e" s="T213">tultɔːt </ts>
               <ts e="T215" id="Seg_3531" n="e" s="T214">qapija </ts>
               <ts e="T216" id="Seg_3533" n="e" s="T215">tına, </ts>
               <ts e="T217" id="Seg_3535" n="e" s="T216">na </ts>
               <ts e="T218" id="Seg_3537" n="e" s="T217">qälɨt. </ts>
            </ts>
            <ts e="T220" id="Seg_3538" n="sc" s="T219">
               <ts e="T220" id="Seg_3540" n="e" s="T219">Amɨrqolamnɔːt. </ts>
            </ts>
            <ts e="T223" id="Seg_3541" n="sc" s="T221">
               <ts e="T222" id="Seg_3543" n="e" s="T221">Apsɨ </ts>
               <ts e="T223" id="Seg_3545" n="e" s="T222">qontɔːt. </ts>
            </ts>
            <ts e="T240" id="Seg_3546" n="sc" s="T224">
               <ts e="T225" id="Seg_3548" n="e" s="T224">Ämnäiːtɨ </ts>
               <ts e="T226" id="Seg_3550" n="e" s="T225">čʼoj </ts>
               <ts e="T227" id="Seg_3552" n="e" s="T226">tap </ts>
               <ts e="T228" id="Seg_3554" n="e" s="T227">mušɨrɔːlnɔːt, </ts>
               <ts e="T229" id="Seg_3556" n="e" s="T228">karrʼat </ts>
               <ts e="T230" id="Seg_3558" n="e" s="T229">kun </ts>
               <ts e="T231" id="Seg_3560" n="e" s="T230">mɔːntɨlʼ </ts>
               <ts e="T232" id="Seg_3562" n="e" s="T231">apsɨ, </ts>
               <ts e="T233" id="Seg_3564" n="e" s="T232">ür, </ts>
               <ts e="T234" id="Seg_3566" n="e" s="T233">ür </ts>
               <ts e="T235" id="Seg_3568" n="e" s="T234">kun </ts>
               <ts e="T236" id="Seg_3570" n="e" s="T235">mɔːntɨlʼ </ts>
               <ts e="T237" id="Seg_3572" n="e" s="T236">qaj </ts>
               <ts e="T238" id="Seg_3574" n="e" s="T237">tına </ts>
               <ts e="T239" id="Seg_3576" n="e" s="T238">təːkɨmpɨlʼ </ts>
               <ts e="T240" id="Seg_3578" n="e" s="T239">apsɨ. </ts>
            </ts>
            <ts e="T251" id="Seg_3579" n="sc" s="T241">
               <ts e="T242" id="Seg_3581" n="e" s="T241">Ira </ts>
               <ts e="T243" id="Seg_3583" n="e" s="T242">nık </ts>
               <ts e="T244" id="Seg_3585" n="e" s="T243">kətɨŋɨtɨ </ts>
               <ts e="T245" id="Seg_3587" n="e" s="T244">imaqotantɨ, </ts>
               <ts e="T246" id="Seg_3589" n="e" s="T245">Nʼomalʼ </ts>
               <ts e="T247" id="Seg_3591" n="e" s="T246">porqɨ </ts>
               <ts e="T248" id="Seg_3593" n="e" s="T247">ira </ts>
               <ts e="T249" id="Seg_3595" n="e" s="T248">imaqotatɨ </ts>
               <ts e="T250" id="Seg_3597" n="e" s="T249">ɛj </ts>
               <ts e="T251" id="Seg_3599" n="e" s="T250">ɛːppa. </ts>
            </ts>
            <ts e="T253" id="Seg_3600" n="sc" s="T252">
               <ts e="T253" id="Seg_3602" n="e" s="T252">Tal. </ts>
            </ts>
            <ts e="T260" id="Seg_3603" n="sc" s="T254">
               <ts e="T255" id="Seg_3605" n="e" s="T254">Ämnäiːmtɨ </ts>
               <ts e="T256" id="Seg_3607" n="e" s="T255">nık </ts>
               <ts e="T257" id="Seg_3609" n="e" s="T256">kuraltɨŋɨt: </ts>
               <ts e="T258" id="Seg_3611" n="e" s="T257">“Tɛː </ts>
               <ts e="T259" id="Seg_3613" n="e" s="T258">ponä </ts>
               <ts e="T260" id="Seg_3615" n="e" s="T259">tantɨŋɨl. </ts>
            </ts>
            <ts e="T265" id="Seg_3616" n="sc" s="T261">
               <ts e="T262" id="Seg_3618" n="e" s="T261">Ijat </ts>
               <ts e="T263" id="Seg_3620" n="e" s="T262">kuti </ts>
               <ts e="T264" id="Seg_3622" n="e" s="T263">ponä </ts>
               <ts e="T265" id="Seg_3624" n="e" s="T264">tattätɨ.” </ts>
            </ts>
            <ts e="T271" id="Seg_3625" n="sc" s="T266">
               <ts e="T267" id="Seg_3627" n="e" s="T266">Ira </ts>
               <ts e="T268" id="Seg_3629" n="e" s="T267">kuntɨ </ts>
               <ts e="T269" id="Seg_3631" n="e" s="T268">wərka </ts>
               <ts e="T270" id="Seg_3633" n="e" s="T269">qumiːmtɨ </ts>
               <ts e="T271" id="Seg_3635" n="e" s="T270">apsɨtɨmpɨla. </ts>
            </ts>
            <ts e="T279" id="Seg_3636" n="sc" s="T272">
               <ts e="T273" id="Seg_3638" n="e" s="T272">Ukkɨr </ts>
               <ts e="T274" id="Seg_3640" n="e" s="T273">tät </ts>
               <ts e="T275" id="Seg_3642" n="e" s="T274">čʼontot </ts>
               <ts e="T276" id="Seg_3644" n="e" s="T275">tümpɨj </ts>
               <ts e="T277" id="Seg_3646" n="e" s="T276">karrʼat </ts>
               <ts e="T278" id="Seg_3648" n="e" s="T277">qumɨn </ts>
               <ts e="T279" id="Seg_3650" n="e" s="T278">amɨrnɔːtɨt. </ts>
            </ts>
            <ts e="T296" id="Seg_3651" n="sc" s="T280">
               <ts e="T281" id="Seg_3653" n="e" s="T280">Karrʼa </ts>
               <ts e="T282" id="Seg_3655" n="e" s="T281">na </ts>
               <ts e="T283" id="Seg_3657" n="e" s="T282">šoːqɨrɨj </ts>
               <ts e="T284" id="Seg_3659" n="e" s="T283">tüntɨ </ts>
               <ts e="T285" id="Seg_3661" n="e" s="T284">nannɛrɨk </ts>
               <ts e="T286" id="Seg_3663" n="e" s="T285">poːsä </ts>
               <ts e="T287" id="Seg_3665" n="e" s="T286">(sälɨŋɨtɨ), </ts>
               <ts e="T288" id="Seg_3667" n="e" s="T287">čʼɔːtɨŋɨtɨ. </ts>
               <ts e="T289" id="Seg_3669" n="e" s="T288">Ämnäiːmtɨ </ts>
               <ts e="T290" id="Seg_3671" n="e" s="T289">ašša </ts>
               <ts e="T291" id="Seg_3673" n="e" s="T290">qaː </ts>
               <ts e="T292" id="Seg_3675" n="e" s="T291">pona </ts>
               <ts e="T293" id="Seg_3677" n="e" s="T292">üːtɨmpatɨ </ts>
               <ts e="T294" id="Seg_3679" n="e" s="T293">poːqɨn </ts>
               <ts e="T295" id="Seg_3681" n="e" s="T294">tü </ts>
               <ts e="T296" id="Seg_3683" n="e" s="T295">čʼɔːtaltɛntɔːt. </ts>
            </ts>
            <ts e="T308" id="Seg_3684" n="sc" s="T297">
               <ts e="T298" id="Seg_3686" n="e" s="T297">Tü </ts>
               <ts e="T299" id="Seg_3688" n="e" s="T298">čʼɔːtalpɨntɔːtɨt, </ts>
               <ts e="T300" id="Seg_3690" n="e" s="T299">tına </ts>
               <ts e="T301" id="Seg_3692" n="e" s="T300">tölʼ </ts>
               <ts e="T302" id="Seg_3694" n="e" s="T301">purɨš </ts>
               <ts e="T303" id="Seg_3696" n="e" s="T302">olɨlʼa </ts>
               <ts e="T304" id="Seg_3698" n="e" s="T303">nılʼčʼik </ts>
               <ts e="T305" id="Seg_3700" n="e" s="T304">təːkɨrɨmpɨlʼ </ts>
               <ts e="T306" id="Seg_3702" n="e" s="T305">tomarot, </ts>
               <ts e="T307" id="Seg_3704" n="e" s="T306">mɔːt </ts>
               <ts e="T308" id="Seg_3706" n="e" s="T307">(tümtɨ). </ts>
            </ts>
            <ts e="T314" id="Seg_3707" n="sc" s="T309">
               <ts e="T310" id="Seg_3709" n="e" s="T309">“Tɛː </ts>
               <ts e="T311" id="Seg_3711" n="e" s="T310">najenta </ts>
               <ts e="T312" id="Seg_3713" n="e" s="T311">man </ts>
               <ts e="T313" id="Seg_3715" n="e" s="T312">na </ts>
               <ts e="T314" id="Seg_3717" n="e" s="T313">tannɛntak.” </ts>
            </ts>
            <ts e="T317" id="Seg_3718" n="sc" s="T315">
               <ts e="T316" id="Seg_3720" n="e" s="T315">Qumɨn </ts>
               <ts e="T317" id="Seg_3722" n="e" s="T316">amɨrsɔːtɨt. </ts>
            </ts>
            <ts e="T319" id="Seg_3723" n="sc" s="T318">
               <ts e="T319" id="Seg_3725" n="e" s="T318">Amɨrnɔːtɨt. </ts>
            </ts>
            <ts e="T329" id="Seg_3726" n="sc" s="T320">
               <ts e="T321" id="Seg_3728" n="e" s="T320">Qälɨt </ts>
               <ts e="T322" id="Seg_3730" n="e" s="T321">nık </ts>
               <ts e="T323" id="Seg_3732" n="e" s="T322">kətɔːt: </ts>
               <ts e="T324" id="Seg_3734" n="e" s="T323">“Ilʼčʼa, </ts>
               <ts e="T325" id="Seg_3736" n="e" s="T324">šoːqɨrlɨ </ts>
               <ts e="T326" id="Seg_3738" n="e" s="T325">naššak </ts>
               <ts e="T327" id="Seg_3740" n="e" s="T326">qajqo </ts>
               <ts e="T328" id="Seg_3742" n="e" s="T327">poːsä </ts>
               <ts e="T329" id="Seg_3744" n="e" s="T328">sälal?” </ts>
            </ts>
            <ts e="T334" id="Seg_3745" n="sc" s="T330">
               <ts e="T331" id="Seg_3747" n="e" s="T330">“Ašša, </ts>
               <ts e="T332" id="Seg_3749" n="e" s="T331">šoːqɨr </ts>
               <ts e="T333" id="Seg_3751" n="e" s="T332">tü </ts>
               <ts e="T334" id="Seg_3753" n="e" s="T333">amtɨŋɨt. </ts>
            </ts>
            <ts e="T340" id="Seg_3754" n="sc" s="T335">
               <ts e="T336" id="Seg_3756" n="e" s="T335">Ašša </ts>
               <ts e="T337" id="Seg_3758" n="e" s="T336">mɨta </ts>
               <ts e="T338" id="Seg_3760" n="e" s="T337">man </ts>
               <ts e="T339" id="Seg_3762" n="e" s="T338">mɨta </ts>
               <ts e="T340" id="Seg_3764" n="e" s="T339">mɨ…” </ts>
            </ts>
            <ts e="T355" id="Seg_3765" n="sc" s="T341">
               <ts e="T342" id="Seg_3767" n="e" s="T341">Tɛm </ts>
               <ts e="T343" id="Seg_3769" n="e" s="T342">ašša </ts>
               <ts e="T344" id="Seg_3771" n="e" s="T343">amtɨtɨ, </ts>
               <ts e="T345" id="Seg_3773" n="e" s="T344">tɛm </ts>
               <ts e="T346" id="Seg_3775" n="e" s="T345">ašša </ts>
               <ts e="T347" id="Seg_3777" n="e" s="T346">amtɨtɨ </ts>
               <ts e="T348" id="Seg_3779" n="e" s="T347">nılʼ </ts>
               <ts e="T349" id="Seg_3781" n="e" s="T348">mərkata, </ts>
               <ts e="T350" id="Seg_3783" n="e" s="T349">nılʼ </ts>
               <ts e="T351" id="Seg_3785" n="e" s="T350">nʼomalʼ </ts>
               <ts e="T352" id="Seg_3787" n="e" s="T351">porqɨ </ts>
               <ts e="T353" id="Seg_3789" n="e" s="T352">raša </ts>
               <ts e="T354" id="Seg_3791" n="e" s="T353">lıːpɨ </ts>
               <ts e="T355" id="Seg_3793" n="e" s="T354">tına. </ts>
            </ts>
            <ts e="T359" id="Seg_3794" n="sc" s="T356">
               <ts e="T357" id="Seg_3796" n="e" s="T356">Meːntalʼ, </ts>
               <ts e="T358" id="Seg_3798" n="e" s="T357">namɨp </ts>
               <ts e="T359" id="Seg_3800" n="e" s="T358">šeːrpɨntɨt. </ts>
            </ts>
            <ts e="T365" id="Seg_3801" n="sc" s="T360">
               <ts e="T361" id="Seg_3803" n="e" s="T360">Šoŋoltɨ </ts>
               <ts e="T362" id="Seg_3805" n="e" s="T361">qanɨqqɨt </ts>
               <ts e="T363" id="Seg_3807" n="e" s="T362">mərqa </ts>
               <ts e="T364" id="Seg_3809" n="e" s="T363">mɔːtantɨ </ts>
               <ts e="T365" id="Seg_3811" n="e" s="T364">qanɨqqɨt. </ts>
            </ts>
            <ts e="T375" id="Seg_3812" n="sc" s="T366">
               <ts e="T367" id="Seg_3814" n="e" s="T366">Ukkɨr </ts>
               <ts e="T368" id="Seg_3816" n="e" s="T367">tot </ts>
               <ts e="T369" id="Seg_3818" n="e" s="T368">čʼontot </ts>
               <ts e="T370" id="Seg_3820" n="e" s="T369">poːqɨnɨ </ts>
               <ts e="T371" id="Seg_3822" n="e" s="T370">mɔːttɨ </ts>
               <ts e="T372" id="Seg_3824" n="e" s="T371">tına </ts>
               <ts e="T373" id="Seg_3826" n="e" s="T372">tülʼ </ts>
               <ts e="T374" id="Seg_3828" n="e" s="T373">purɨšlaka </ts>
               <ts e="T375" id="Seg_3830" n="e" s="T374">alʼčʼintɨ. </ts>
            </ts>
            <ts e="T383" id="Seg_3831" n="sc" s="T376">
               <ts e="T377" id="Seg_3833" n="e" s="T376">Na </ts>
               <ts e="T378" id="Seg_3835" n="e" s="T377">paktɨmmɨnta </ts>
               <ts e="T379" id="Seg_3837" n="e" s="T378">tına </ts>
               <ts e="T380" id="Seg_3839" n="e" s="T379">mɔːtantɨ </ts>
               <ts e="T381" id="Seg_3841" n="e" s="T380">tına </ts>
               <ts e="T382" id="Seg_3843" n="e" s="T381">nılʼčʼi </ts>
               <ts e="T383" id="Seg_3845" n="e" s="T382">ämnäiːtɨ. </ts>
            </ts>
            <ts e="T397" id="Seg_3846" n="sc" s="T384">
               <ts e="T385" id="Seg_3848" n="e" s="T384">Qälɨn </ts>
               <ts e="T386" id="Seg_3850" n="e" s="T385">čʼam </ts>
               <ts e="T387" id="Seg_3852" n="e" s="T386">orqɨlpɔːt </ts>
               <ts e="T388" id="Seg_3854" n="e" s="T387">nılʼ </ts>
               <ts e="T389" id="Seg_3856" n="e" s="T388">irap </ts>
               <ts e="T390" id="Seg_3858" n="e" s="T389">ola </ts>
               <ts e="T391" id="Seg_3860" n="e" s="T390">čʼaŋaj </ts>
               <ts e="T392" id="Seg_3862" n="e" s="T391">nʼomalʼ </ts>
               <ts e="T393" id="Seg_3864" n="e" s="T392">porqɨntɨ </ts>
               <ts e="T394" id="Seg_3866" n="e" s="T393">lıːp </ts>
               <ts e="T395" id="Seg_3868" n="e" s="T394">nɨtqɨlɛːla </ts>
               <ts e="T396" id="Seg_3870" n="e" s="T395">orqɨltɔːt </ts>
               <ts e="T397" id="Seg_3872" n="e" s="T396">mɔːtqɨn. </ts>
            </ts>
            <ts e="T412" id="Seg_3873" n="sc" s="T398">
               <ts e="T399" id="Seg_3875" n="e" s="T398">Qapi </ts>
               <ts e="T400" id="Seg_3877" n="e" s="T399">na </ts>
               <ts e="T401" id="Seg_3879" n="e" s="T400">tüsa </ts>
               <ts e="T402" id="Seg_3881" n="e" s="T401">nɨnɨ </ts>
               <ts e="T403" id="Seg_3883" n="e" s="T402">näːtɨ </ts>
               <ts e="T404" id="Seg_3885" n="e" s="T403">okoškan </ts>
               <ts e="T405" id="Seg_3887" n="e" s="T404">ɔːš </ts>
               <ts e="T406" id="Seg_3889" n="e" s="T405">šüːmɨt </ts>
               <ts e="T407" id="Seg_3891" n="e" s="T406">mɔːttɨ </ts>
               <ts e="T408" id="Seg_3893" n="e" s="T407">(tılʼčʼɨ) </ts>
               <ts e="T409" id="Seg_3895" n="e" s="T408">šittɨlʼ </ts>
               <ts e="T410" id="Seg_3897" n="e" s="T409">pɛlalʼ </ts>
               <ts e="T411" id="Seg_3899" n="e" s="T410">okoška </ts>
               <ts e="T412" id="Seg_3901" n="e" s="T411">ɛːppa. </ts>
            </ts>
            <ts e="T417" id="Seg_3902" n="sc" s="T413">
               <ts e="T414" id="Seg_3904" n="e" s="T413">Qaj </ts>
               <ts e="T415" id="Seg_3906" n="e" s="T414">wərqɨ </ts>
               <ts e="T416" id="Seg_3908" n="e" s="T415">mɔːttɨ </ts>
               <ts e="T417" id="Seg_3910" n="e" s="T416">ɛːppa. </ts>
            </ts>
            <ts e="T430" id="Seg_3911" n="sc" s="T418">
               <ts e="T419" id="Seg_3913" n="e" s="T418">Mɔːttɨ </ts>
               <ts e="T420" id="Seg_3915" n="e" s="T419">najentɨ </ts>
               <ts e="T421" id="Seg_3917" n="e" s="T420">čʼusa </ts>
               <ts e="T422" id="Seg_3919" n="e" s="T421">taqpɨmpa, </ts>
               <ts e="T423" id="Seg_3921" n="e" s="T422">tɨna </ts>
               <ts e="T424" id="Seg_3923" n="e" s="T423">loːqɨra </ts>
               <ts e="T425" id="Seg_3925" n="e" s="T424">polʼ </ts>
               <ts e="T426" id="Seg_3927" n="e" s="T425">mɔːt, </ts>
               <ts e="T427" id="Seg_3929" n="e" s="T426">pičʼit </ts>
               <ts e="T428" id="Seg_3931" n="e" s="T427">paqɨt </ts>
               <ts e="T429" id="Seg_3933" n="e" s="T428">piːrɨlʼ </ts>
               <ts e="T430" id="Seg_3935" n="e" s="T429">taqpa. </ts>
            </ts>
            <ts e="T446" id="Seg_3936" n="sc" s="T431">
               <ts e="T432" id="Seg_3938" n="e" s="T431">Mompa </ts>
               <ts e="T433" id="Seg_3940" n="e" s="T432">mɔːtqɨt </ts>
               <ts e="T434" id="Seg_3942" n="e" s="T433">tına </ts>
               <ts e="T435" id="Seg_3944" n="e" s="T434">na </ts>
               <ts e="T436" id="Seg_3946" n="e" s="T435">tüm </ts>
               <ts e="T437" id="Seg_3948" n="e" s="T436">na </ts>
               <ts e="T438" id="Seg_3950" n="e" s="T437">qəːlpɨntɔːtɨt, </ts>
               <ts e="T439" id="Seg_3952" n="e" s="T438">na </ts>
               <ts e="T440" id="Seg_3954" n="e" s="T439">qättɨmmɨntɨtɨ </ts>
               <ts e="T441" id="Seg_3956" n="e" s="T440">qapi </ts>
               <ts e="T442" id="Seg_3958" n="e" s="T441">ton </ts>
               <ts e="T923" id="Seg_3960" n="e" s="T442">irap. </ts>
               <ts e="T443" id="Seg_3962" n="e" s="T923">((NN:)) </ts>
               <ts e="T444" id="Seg_3964" n="e" s="T443">Qapi </ts>
               <ts e="T445" id="Seg_3966" n="e" s="T444">tam </ts>
               <ts e="T446" id="Seg_3968" n="e" s="T445">amn. </ts>
            </ts>
            <ts e="T448" id="Seg_3969" n="sc" s="T924">
               <ts e="T447" id="Seg_3971" n="e" s="T924">((NEP:)) </ts>
               <ts e="T448" id="Seg_3973" n="e" s="T447">Tal. </ts>
            </ts>
            <ts e="T464" id="Seg_3974" n="sc" s="T449">
               <ts e="T450" id="Seg_3976" n="e" s="T449">Poraksa </ts>
               <ts e="T451" id="Seg_3978" n="e" s="T450">qaj </ts>
               <ts e="T452" id="Seg_3980" n="e" s="T451">təm </ts>
               <ts e="T453" id="Seg_3982" n="e" s="T452">mɨta </ts>
               <ts e="T454" id="Seg_3984" n="e" s="T453">qamnɨmpat </ts>
               <ts e="T455" id="Seg_3986" n="e" s="T454">(na=) </ts>
               <ts e="T456" id="Seg_3988" n="e" s="T455">na </ts>
               <ts e="T457" id="Seg_3990" n="e" s="T456">nʼuːtɨt </ts>
               <ts e="T458" id="Seg_3992" n="e" s="T457">ɨlqɨlʼ </ts>
               <ts e="T459" id="Seg_3994" n="e" s="T458">pɛlat </ts>
               <ts e="T460" id="Seg_3996" n="e" s="T459">tilʼčʼi </ts>
               <ts e="T461" id="Seg_3998" n="e" s="T460">šentɨ </ts>
               <ts e="T462" id="Seg_4000" n="e" s="T461">nʼuːtɨ </ts>
               <ts e="T463" id="Seg_4002" n="e" s="T462">ɨlqɨlʼ </ts>
               <ts e="T464" id="Seg_4004" n="e" s="T463">pɛlat. </ts>
            </ts>
            <ts e="T476" id="Seg_4005" n="sc" s="T465">
               <ts e="T466" id="Seg_4007" n="e" s="T465">Na </ts>
               <ts e="T467" id="Seg_4009" n="e" s="T466">tü </ts>
               <ts e="T468" id="Seg_4011" n="e" s="T467">na </ts>
               <ts e="T469" id="Seg_4013" n="e" s="T468">qättɨmmɨntɨtɨ, </ts>
               <ts e="T470" id="Seg_4015" n="e" s="T469">tına </ts>
               <ts e="T471" id="Seg_4017" n="e" s="T470">nɔːssaralʼ </ts>
               <ts e="T472" id="Seg_4019" n="e" s="T471">qumɨlʼ </ts>
               <ts e="T473" id="Seg_4021" n="e" s="T472">mütɨp </ts>
               <ts e="T474" id="Seg_4023" n="e" s="T473">muntɨk </ts>
               <ts e="T475" id="Seg_4025" n="e" s="T474">tü </ts>
               <ts e="T476" id="Seg_4027" n="e" s="T475">ampat. </ts>
            </ts>
            <ts e="T480" id="Seg_4028" n="sc" s="T477">
               <ts e="T478" id="Seg_4030" n="e" s="T477">Qən </ts>
               <ts e="T479" id="Seg_4032" n="e" s="T478">ɔːlʼčʼɔːt </ts>
               <ts e="T480" id="Seg_4034" n="e" s="T479">nʼennat. </ts>
            </ts>
            <ts e="T491" id="Seg_4035" n="sc" s="T481">
               <ts e="T482" id="Seg_4037" n="e" s="T481">Nʼennä </ts>
               <ts e="T483" id="Seg_4039" n="e" s="T482">pon </ts>
               <ts e="T484" id="Seg_4041" n="e" s="T483">ira </ts>
               <ts e="T485" id="Seg_4043" n="e" s="T484">pona </ts>
               <ts e="T486" id="Seg_4045" n="e" s="T485">pakta </ts>
               <ts e="T487" id="Seg_4047" n="e" s="T486">mɔːtan </ts>
               <ts e="T488" id="Seg_4049" n="e" s="T487">ɔːt, </ts>
               <ts e="T489" id="Seg_4051" n="e" s="T488">pičʼilʼ </ts>
               <ts e="T490" id="Seg_4053" n="e" s="T489">ut </ts>
               <ts e="T491" id="Seg_4055" n="e" s="T490">mərka. </ts>
            </ts>
            <ts e="T499" id="Seg_4056" n="sc" s="T492">
               <ts e="T493" id="Seg_4058" n="e" s="T492">Isɨ </ts>
               <ts e="T494" id="Seg_4060" n="e" s="T493">čʼap </ts>
               <ts e="T495" id="Seg_4062" n="e" s="T494">pona </ts>
               <ts e="T496" id="Seg_4064" n="e" s="T495">qontɨšɛlʼčʼɔːt </ts>
               <ts e="T497" id="Seg_4066" n="e" s="T496">aj </ts>
               <ts e="T498" id="Seg_4068" n="e" s="T497">čʼari </ts>
               <ts e="T499" id="Seg_4070" n="e" s="T498">qättɨŋɨt. </ts>
            </ts>
            <ts e="T516" id="Seg_4071" n="sc" s="T500">
               <ts e="T501" id="Seg_4073" n="e" s="T500">Ämnäiːtɨ </ts>
               <ts e="T502" id="Seg_4075" n="e" s="T501">na </ts>
               <ts e="T503" id="Seg_4077" n="e" s="T502">tü </ts>
               <ts e="T504" id="Seg_4079" n="e" s="T503">na </ts>
               <ts e="T505" id="Seg_4081" n="e" s="T504">qəːmmɨntɨt, </ts>
               <ts e="T506" id="Seg_4083" n="e" s="T505">šoːqɨr </ts>
               <ts e="T507" id="Seg_4085" n="e" s="T506">pɔːr </ts>
               <ts e="T508" id="Seg_4087" n="e" s="T507">šüːmɨt </ts>
               <ts e="T509" id="Seg_4089" n="e" s="T508">qəːŋɔːt, </ts>
               <ts e="T510" id="Seg_4091" n="e" s="T509">nɨn </ts>
               <ts e="T511" id="Seg_4093" n="e" s="T510">ɛj </ts>
               <ts e="T512" id="Seg_4095" n="e" s="T511">šit </ts>
               <ts e="T513" id="Seg_4097" n="e" s="T512">okoškon </ts>
               <ts e="T514" id="Seg_4099" n="e" s="T513">ɔːš </ts>
               <ts e="T515" id="Seg_4101" n="e" s="T514">šüːmɨt </ts>
               <ts e="T516" id="Seg_4103" n="e" s="T515">qəːŋɔːtɨn. </ts>
            </ts>
            <ts e="T523" id="Seg_4104" n="sc" s="T517">
               <ts e="T518" id="Seg_4106" n="e" s="T517">Muntɨk </ts>
               <ts e="T519" id="Seg_4108" n="e" s="T518">tüs </ts>
               <ts e="T520" id="Seg_4110" n="e" s="T519">čʼɔːttɛıːmpatɨ, </ts>
               <ts e="T521" id="Seg_4112" n="e" s="T520">na </ts>
               <ts e="T522" id="Seg_4114" n="e" s="T521">qälɨlʼ </ts>
               <ts e="T523" id="Seg_4116" n="e" s="T522">mütɨp. </ts>
            </ts>
            <ts e="T529" id="Seg_4117" n="sc" s="T524">
               <ts e="T525" id="Seg_4119" n="e" s="T524">Na </ts>
               <ts e="T526" id="Seg_4121" n="e" s="T525">mütɨ </ts>
               <ts e="T527" id="Seg_4123" n="e" s="T526">qɔːtɨ </ts>
               <ts e="T528" id="Seg_4125" n="e" s="T527">šittä </ts>
               <ts e="T529" id="Seg_4127" n="e" s="T528">ɛːppɨntɔːt. </ts>
            </ts>
            <ts e="T531" id="Seg_4128" n="sc" s="T530">
               <ts e="T531" id="Seg_4130" n="e" s="T530">((KuAI:)) Говорите. </ts>
            </ts>
            <ts e="T534" id="Seg_4131" n="sc" s="T926">
               <ts e="T532" id="Seg_4133" n="e" s="T926">((NN:)) </ts>
               <ts e="T533" id="Seg_4135" n="e" s="T532">Na </ts>
               <ts e="T534" id="Seg_4137" n="e" s="T533">kətät. </ts>
            </ts>
            <ts e="T539" id="Seg_4138" n="sc" s="T925">
               <ts e="T535" id="Seg_4140" n="e" s="T925">((NEP:)) </ts>
               <ts e="T536" id="Seg_4142" n="e" s="T535">Pɛlakɨtɨj </ts>
               <ts e="T537" id="Seg_4144" n="e" s="T536">ira </ts>
               <ts e="T538" id="Seg_4146" n="e" s="T537">kučʼčʼa </ts>
               <ts e="T539" id="Seg_4148" n="e" s="T538">qattɛnta? </ts>
            </ts>
            <ts e="T551" id="Seg_4149" n="sc" s="T540">
               <ts e="T541" id="Seg_4151" n="e" s="T540">Ira </ts>
               <ts e="T542" id="Seg_4153" n="e" s="T541">nɨnɨ </ts>
               <ts e="T543" id="Seg_4155" n="e" s="T542">šʼittalʼ, </ts>
               <ts e="T544" id="Seg_4157" n="e" s="T543">mɨŋa, </ts>
               <ts e="T545" id="Seg_4159" n="e" s="T544">Nʼomalʼ </ts>
               <ts e="T546" id="Seg_4161" n="e" s="T545">porqɨ </ts>
               <ts e="T547" id="Seg_4163" n="e" s="T546">ira, </ts>
               <ts e="T548" id="Seg_4165" n="e" s="T547">karrä </ts>
               <ts e="T549" id="Seg_4167" n="e" s="T548">qälɨiːntɨ </ts>
               <ts e="T550" id="Seg_4169" n="e" s="T549">qaqlontɨ </ts>
               <ts e="T551" id="Seg_4171" n="e" s="T550">(qəŋa). </ts>
            </ts>
            <ts e="T562" id="Seg_4172" n="sc" s="T552">
               <ts e="T553" id="Seg_4174" n="e" s="T552">Aj </ts>
               <ts e="T554" id="Seg_4176" n="e" s="T553">šittämɨ </ts>
               <ts e="T555" id="Seg_4178" n="e" s="T554">((…)) </ts>
               <ts e="T556" id="Seg_4180" n="e" s="T555">takkɨ </ts>
               <ts e="T557" id="Seg_4182" n="e" s="T556">((…)) </ts>
               <ts e="T558" id="Seg_4184" n="e" s="T557">qälɨj </ts>
               <ts e="T559" id="Seg_4186" n="e" s="T558">mɔːttɨ </ts>
               <ts e="T927" id="Seg_4188" n="e" s="T559">(qənqo). </ts>
               <ts e="T560" id="Seg_4190" n="e" s="T927">((NN:)) </ts>
               <ts e="T561" id="Seg_4192" n="e" s="T560">Illa </ts>
               <ts e="T562" id="Seg_4194" n="e" s="T561">omtäš. </ts>
            </ts>
            <ts e="T567" id="Seg_4195" n="sc" s="T928">
               <ts e="T563" id="Seg_4197" n="e" s="T928">((NEP:)) </ts>
               <ts e="T564" id="Seg_4199" n="e" s="T563">Na </ts>
               <ts e="T565" id="Seg_4201" n="e" s="T564">qətqɨlpɨtɨj </ts>
               <ts e="T566" id="Seg_4203" n="e" s="T565">qumɨiːqɨntɨ </ts>
               <ts e="T567" id="Seg_4205" n="e" s="T566">qənqɨntoːqo. </ts>
            </ts>
            <ts e="T577" id="Seg_4206" n="sc" s="T568">
               <ts e="T569" id="Seg_4208" n="e" s="T568">Karrä </ts>
               <ts e="T570" id="Seg_4210" n="e" s="T569">qənna, </ts>
               <ts e="T571" id="Seg_4212" n="e" s="T570">ooo </ts>
               <ts e="T572" id="Seg_4214" n="e" s="T571">karrän </ts>
               <ts e="T573" id="Seg_4216" n="e" s="T572">konna </ts>
               <ts e="T574" id="Seg_4218" n="e" s="T573">moːčʼalʼ </ts>
               <ts e="T575" id="Seg_4220" n="e" s="T574">qälɨ </ts>
               <ts e="T576" id="Seg_4222" n="e" s="T575">ira </ts>
               <ts e="T577" id="Seg_4224" n="e" s="T576">qontɨšeintɨ. </ts>
            </ts>
            <ts e="T579" id="Seg_4225" n="sc" s="T578">
               <ts e="T579" id="Seg_4227" n="e" s="T578">((LAUGH)). </ts>
            </ts>
            <ts e="T583" id="Seg_4228" n="sc" s="T580">
               <ts e="T581" id="Seg_4230" n="e" s="T580">Qaj </ts>
               <ts e="T582" id="Seg_4232" n="e" s="T581">na </ts>
               <ts e="T583" id="Seg_4234" n="e" s="T582">laqɨmɔːttak. </ts>
            </ts>
            <ts e="T589" id="Seg_4235" n="sc" s="T584">
               <ts e="T585" id="Seg_4237" n="e" s="T584">Qaj </ts>
               <ts e="T586" id="Seg_4239" n="e" s="T585">Nʼomalʼ </ts>
               <ts e="T587" id="Seg_4241" n="e" s="T586">porqɨ </ts>
               <ts e="T588" id="Seg_4243" n="e" s="T587">irat </ts>
               <ts e="T589" id="Seg_4245" n="e" s="T588">((…)). </ts>
            </ts>
            <ts e="T599" id="Seg_4246" n="sc" s="T590">
               <ts e="T591" id="Seg_4248" n="e" s="T590">Nʼomalʼ </ts>
               <ts e="T592" id="Seg_4250" n="e" s="T591">porqɨ </ts>
               <ts e="T593" id="Seg_4252" n="e" s="T592">ira </ts>
               <ts e="T594" id="Seg_4254" n="e" s="T593">čʼarrä </ts>
               <ts e="T595" id="Seg_4256" n="e" s="T594">qättɨŋɨtɨ, </ts>
               <ts e="T596" id="Seg_4258" n="e" s="T595">päqɨlʼ </ts>
               <ts e="T597" id="Seg_4260" n="e" s="T596">šuktɨ </ts>
               <ts e="T598" id="Seg_4262" n="e" s="T597">qottä </ts>
               <ts e="T599" id="Seg_4264" n="e" s="T598">(säppaja). </ts>
            </ts>
            <ts e="T603" id="Seg_4265" n="sc" s="T600">
               <ts e="T601" id="Seg_4267" n="e" s="T600">Nɨnɨ </ts>
               <ts e="T602" id="Seg_4269" n="e" s="T601">karrä </ts>
               <ts e="T603" id="Seg_4271" n="e" s="T602">kurɔːlna. </ts>
            </ts>
            <ts e="T611" id="Seg_4272" n="sc" s="T604">
               <ts e="T605" id="Seg_4274" n="e" s="T604">Karrä </ts>
               <ts e="T606" id="Seg_4276" n="e" s="T605">kurɔːlna, </ts>
               <ts e="T607" id="Seg_4278" n="e" s="T606">nɨnɨ </ts>
               <ts e="T608" id="Seg_4280" n="e" s="T607">nɔːkɨr </ts>
               <ts e="T609" id="Seg_4282" n="e" s="T608">ämnäsɨt </ts>
               <ts e="T610" id="Seg_4284" n="e" s="T609">takkɨ </ts>
               <ts e="T611" id="Seg_4286" n="e" s="T610">qənnɔːt. </ts>
            </ts>
            <ts e="T618" id="Seg_4287" n="sc" s="T612">
               <ts e="T613" id="Seg_4289" n="e" s="T612">Takkɨ </ts>
               <ts e="T614" id="Seg_4291" n="e" s="T613">qälɨn </ts>
               <ts e="T615" id="Seg_4293" n="e" s="T614">mɔːt </ts>
               <ts e="T616" id="Seg_4295" n="e" s="T615">takkɨn </ts>
               <ts e="T617" id="Seg_4297" n="e" s="T616">ɛːppa </ts>
               <ts e="T618" id="Seg_4299" n="e" s="T617">točʼčʼotot. </ts>
            </ts>
            <ts e="T632" id="Seg_4300" n="sc" s="T619">
               <ts e="T620" id="Seg_4302" n="e" s="T619">Karrʼa </ts>
               <ts e="T621" id="Seg_4304" n="e" s="T620">qənnɔːt </ts>
               <ts e="T622" id="Seg_4306" n="e" s="T621">nɨmtɨ, </ts>
               <ts e="T623" id="Seg_4308" n="e" s="T622">näčʼčʼa </ts>
               <ts e="T624" id="Seg_4310" n="e" s="T623">laqaltɛlʼčʼɔːt </ts>
               <ts e="T625" id="Seg_4312" n="e" s="T624">qälɨtɨt </ts>
               <ts e="T626" id="Seg_4314" n="e" s="T625">qaqlɨsa, </ts>
               <ts e="T627" id="Seg_4316" n="e" s="T626">na </ts>
               <ts e="T628" id="Seg_4318" n="e" s="T627">qumiːt </ts>
               <ts e="T629" id="Seg_4320" n="e" s="T628">qaqlɨsa, </ts>
               <ts e="T630" id="Seg_4322" n="e" s="T629">na </ts>
               <ts e="T631" id="Seg_4324" n="e" s="T630">nɔːkɨr </ts>
               <ts e="T632" id="Seg_4326" n="e" s="T631">ilʼčʼasɨt. </ts>
            </ts>
            <ts e="T636" id="Seg_4327" n="sc" s="T633">
               <ts e="T634" id="Seg_4329" n="e" s="T633">Takkɨ </ts>
               <ts e="T635" id="Seg_4331" n="e" s="T634">laqaltɛːla </ts>
               <ts e="T636" id="Seg_4333" n="e" s="T635">qənnɔːt. </ts>
            </ts>
            <ts e="T645" id="Seg_4334" n="sc" s="T637">
               <ts e="T638" id="Seg_4336" n="e" s="T637">Tına </ts>
               <ts e="T639" id="Seg_4338" n="e" s="T638">takkɨn </ts>
               <ts e="T640" id="Seg_4340" n="e" s="T639">na </ts>
               <ts e="T641" id="Seg_4342" n="e" s="T640">qälɨlʼ </ts>
               <ts e="T642" id="Seg_4344" n="e" s="T641">((…)) </ts>
               <ts e="T643" id="Seg_4346" n="e" s="T642">imatɨtɨp </ts>
               <ts e="T644" id="Seg_4348" n="e" s="T643">na </ts>
               <ts e="T645" id="Seg_4350" n="e" s="T644">šuqɨlpɨntɨ. </ts>
            </ts>
            <ts e="T656" id="Seg_4351" n="sc" s="T646">
               <ts e="T647" id="Seg_4353" n="e" s="T646">Tına </ts>
               <ts e="T648" id="Seg_4355" n="e" s="T647">šʼolqumɨj </ts>
               <ts e="T649" id="Seg_4357" n="e" s="T648">ijaqı </ts>
               <ts e="T650" id="Seg_4359" n="e" s="T649">näčʼčʼatɨqɨt </ts>
               <ts e="T651" id="Seg_4361" n="e" s="T650">namä </ts>
               <ts e="T652" id="Seg_4363" n="e" s="T651">qalɨmpɔːqı </ts>
               <ts e="T653" id="Seg_4365" n="e" s="T652">tomoroqɨt </ts>
               <ts e="T654" id="Seg_4367" n="e" s="T653">na </ts>
               <ts e="T655" id="Seg_4369" n="e" s="T654">ilʼčʼantıːn </ts>
               <ts e="T656" id="Seg_4371" n="e" s="T655">tümpɨqı. </ts>
            </ts>
            <ts e="T664" id="Seg_4372" n="sc" s="T657">
               <ts e="T658" id="Seg_4374" n="e" s="T657">Konnätɨ </ts>
               <ts e="T659" id="Seg_4376" n="e" s="T658">tına </ts>
               <ts e="T660" id="Seg_4378" n="e" s="T659">(mɨntɨ) </ts>
               <ts e="T661" id="Seg_4380" n="e" s="T660">šittɨ </ts>
               <ts e="T662" id="Seg_4382" n="e" s="T661">qälɨk </ts>
               <ts e="T663" id="Seg_4384" n="e" s="T662">moqonä </ts>
               <ts e="T664" id="Seg_4386" n="e" s="T663">üːtɨmmɨntɔːt. </ts>
            </ts>
            <ts e="T685" id="Seg_4387" n="sc" s="T665">
               <ts e="T666" id="Seg_4389" n="e" s="T665">Mɔːttɨ, </ts>
               <ts e="T667" id="Seg_4391" n="e" s="T666">tɛːpɨktɨ, </ts>
               <ts e="T668" id="Seg_4393" n="e" s="T667">(kun) </ts>
               <ts e="T669" id="Seg_4395" n="e" s="T668">qajtɨ </ts>
               <ts e="T670" id="Seg_4397" n="e" s="T669">naɛntɨ, </ts>
               <ts e="T671" id="Seg_4399" n="e" s="T670">Nʼomalʼ </ts>
               <ts e="T672" id="Seg_4401" n="e" s="T671">porqɨ </ts>
               <ts e="T673" id="Seg_4403" n="e" s="T672">ira </ts>
               <ts e="T674" id="Seg_4405" n="e" s="T673">ɔːtäsa </ts>
               <ts e="T675" id="Seg_4407" n="e" s="T674">sɨr </ts>
               <ts e="T676" id="Seg_4409" n="e" s="T675">por </ts>
               <ts e="T677" id="Seg_4411" n="e" s="T676">nʼenna </ts>
               <ts e="T678" id="Seg_4413" n="e" s="T677">tɔːqqɨŋɨt, </ts>
               <ts e="T679" id="Seg_4415" n="e" s="T678">na </ts>
               <ts e="T680" id="Seg_4417" n="e" s="T679">na </ts>
               <ts e="T681" id="Seg_4419" n="e" s="T680">čʼilʼalʼ </ts>
               <ts e="T682" id="Seg_4421" n="e" s="T681">ija </ts>
               <ts e="T683" id="Seg_4423" n="e" s="T682">ɛj </ts>
               <ts e="T684" id="Seg_4425" n="e" s="T683">təpɨsa </ts>
               <ts e="T685" id="Seg_4427" n="e" s="T684">ɛːsɔːqı. </ts>
            </ts>
            <ts e="T693" id="Seg_4428" n="sc" s="T686">
               <ts e="T687" id="Seg_4430" n="e" s="T686">Tomorot </ts>
               <ts e="T688" id="Seg_4432" n="e" s="T687">təttɨčʼat </ts>
               <ts e="T689" id="Seg_4434" n="e" s="T688">təpɨt </ts>
               <ts e="T690" id="Seg_4436" n="e" s="T689">mɨqɨt </ts>
               <ts e="T691" id="Seg_4438" n="e" s="T690">qälɨj, </ts>
               <ts e="T692" id="Seg_4440" n="e" s="T691">na </ts>
               <ts e="T693" id="Seg_4442" n="e" s="T692">mɨ. </ts>
            </ts>
            <ts e="T700" id="Seg_4443" n="sc" s="T694">
               <ts e="T695" id="Seg_4445" n="e" s="T694">Nʼomalʼ </ts>
               <ts e="T696" id="Seg_4447" n="e" s="T695">porqɨ </ts>
               <ts e="T697" id="Seg_4449" n="e" s="T696">ira </ts>
               <ts e="T698" id="Seg_4451" n="e" s="T697">təttɨčʼälɨ </ts>
               <ts e="T699" id="Seg_4453" n="e" s="T698">kətsanqıtɨ </ts>
               <ts e="T700" id="Seg_4455" n="e" s="T699">ɛːppɨntɔːqı. </ts>
            </ts>
            <ts e="T706" id="Seg_4456" n="sc" s="T701">
               <ts e="T702" id="Seg_4458" n="e" s="T701">Na </ts>
               <ts e="T703" id="Seg_4460" n="e" s="T702">qətpɨlʼ </ts>
               <ts e="T704" id="Seg_4462" n="e" s="T703">qumɨtɨt, </ts>
               <ts e="T705" id="Seg_4464" n="e" s="T704">na </ts>
               <ts e="T706" id="Seg_4466" n="e" s="T705">qälɨn. </ts>
            </ts>
            <ts e="T710" id="Seg_4467" n="sc" s="T707">
               <ts e="T708" id="Seg_4469" n="e" s="T707">Namɨn </ts>
               <ts e="T709" id="Seg_4471" n="e" s="T708">ijaqı </ts>
               <ts e="T710" id="Seg_4473" n="e" s="T709">šʼittɨ. </ts>
            </ts>
            <ts e="T714" id="Seg_4474" n="sc" s="T711">
               <ts e="T712" id="Seg_4476" n="e" s="T711">Nılʼčʼi </ts>
               <ts e="T713" id="Seg_4478" n="e" s="T712">šʼittɨ </ts>
               <ts e="T714" id="Seg_4480" n="e" s="T713">ijaqı. </ts>
            </ts>
            <ts e="T722" id="Seg_4481" n="sc" s="T715">
               <ts e="T716" id="Seg_4483" n="e" s="T715">Ɔːtap </ts>
               <ts e="T717" id="Seg_4485" n="e" s="T716">tɔːqqɨla </ts>
               <ts e="T718" id="Seg_4487" n="e" s="T717">moqɨn </ts>
               <ts e="T719" id="Seg_4489" n="e" s="T718">tattɔːt </ts>
               <ts e="T720" id="Seg_4491" n="e" s="T719">tıntena </ts>
               <ts e="T721" id="Seg_4493" n="e" s="T720">(tına) </ts>
               <ts e="T722" id="Seg_4495" n="e" s="T721">mɔːttɨ. </ts>
            </ts>
            <ts e="T725" id="Seg_4496" n="sc" s="T723">
               <ts e="T724" id="Seg_4498" n="e" s="T723">Tına </ts>
               <ts e="T725" id="Seg_4500" n="e" s="T724">mɔːtɨŋnɔːt. </ts>
            </ts>
            <ts e="T731" id="Seg_4501" n="sc" s="T930">
               <ts e="T726" id="Seg_4503" n="e" s="T930">((NN:)) </ts>
               <ts e="T727" id="Seg_4505" n="e" s="T726">Lʼa </ts>
               <ts e="T728" id="Seg_4507" n="e" s="T727">na </ts>
               <ts e="T729" id="Seg_4509" n="e" s="T728">namɨlʼa </ts>
               <ts e="T730" id="Seg_4511" n="e" s="T729">nannɛr </ts>
               <ts e="T731" id="Seg_4513" n="e" s="T730">qəša. </ts>
            </ts>
            <ts e="T739" id="Seg_4514" n="sc" s="T929">
               <ts e="T732" id="Seg_4516" n="e" s="T929">((NEP:)) </ts>
               <ts e="T733" id="Seg_4518" n="e" s="T732">Ijaiːtɨ </ts>
               <ts e="T734" id="Seg_4520" n="e" s="T733">tam </ts>
               <ts e="T735" id="Seg_4522" n="e" s="T734">na </ts>
               <ts e="T736" id="Seg_4524" n="e" s="T735">qənmmɨntɔːt </ts>
               <ts e="T737" id="Seg_4526" n="e" s="T736">šöttɨ </ts>
               <ts e="T738" id="Seg_4528" n="e" s="T737">täpällä </ts>
               <ts e="T739" id="Seg_4530" n="e" s="T738">surɨllä. </ts>
            </ts>
            <ts e="T741" id="Seg_4531" n="sc" s="T740">
               <ts e="T741" id="Seg_4533" n="e" s="T740">((NN:)) ((…)). </ts>
            </ts>
            <ts e="T748" id="Seg_4534" n="sc" s="T742">
               <ts e="T743" id="Seg_4536" n="e" s="T742">Konnä </ts>
               <ts e="T744" id="Seg_4538" n="e" s="T743">mɔːtɨŋnɔːt, </ts>
               <ts e="T745" id="Seg_4540" n="e" s="T744">tal, </ts>
               <ts e="T746" id="Seg_4542" n="e" s="T745">ukkɨr </ts>
               <ts e="T747" id="Seg_4544" n="e" s="T746">pit </ts>
               <ts e="T748" id="Seg_4546" n="e" s="T747">šäqɔːt. </ts>
            </ts>
            <ts e="T750" id="Seg_4547" n="sc" s="T749">
               <ts e="T750" id="Seg_4549" n="e" s="T749">Šäqɨqolamnɔːt. </ts>
            </ts>
            <ts e="T757" id="Seg_4550" n="sc" s="T751">
               <ts e="T752" id="Seg_4552" n="e" s="T751">Ijaiːtɨ </ts>
               <ts e="T753" id="Seg_4554" n="e" s="T752">šötqɨnɨ </ts>
               <ts e="T754" id="Seg_4556" n="e" s="T753">jap </ts>
               <ts e="T755" id="Seg_4558" n="e" s="T754">tattɔːtɨt </ts>
               <ts e="T756" id="Seg_4560" n="e" s="T755">täpäčʼilʼmɨn </ts>
               <ts e="T757" id="Seg_4562" n="e" s="T756">moqonä. </ts>
            </ts>
            <ts e="T760" id="Seg_4563" n="sc" s="T758">
               <ts e="T759" id="Seg_4565" n="e" s="T758">Kəntalla </ts>
               <ts e="T760" id="Seg_4567" n="e" s="T759">tattɔːt. </ts>
            </ts>
            <ts e="T763" id="Seg_4568" n="sc" s="T761">
               <ts e="T762" id="Seg_4570" n="e" s="T761">Qaj </ts>
               <ts e="T763" id="Seg_4572" n="e" s="T762">ɛsɨ? </ts>
            </ts>
            <ts e="T770" id="Seg_4573" n="sc" s="T764">
               <ts e="T765" id="Seg_4575" n="e" s="T764">Əsɨ </ts>
               <ts e="T766" id="Seg_4577" n="e" s="T765">ira </ts>
               <ts e="T767" id="Seg_4579" n="e" s="T766">mɨt </ts>
               <ts e="T768" id="Seg_4581" n="e" s="T767">tıntena </ts>
               <ts e="T769" id="Seg_4583" n="e" s="T768">mɔːttɨ </ts>
               <ts e="T770" id="Seg_4585" n="e" s="T769">təptɨt. </ts>
            </ts>
            <ts e="T784" id="Seg_4586" n="sc" s="T771">
               <ts e="T772" id="Seg_4588" n="e" s="T771">Karrä </ts>
               <ts e="T773" id="Seg_4590" n="e" s="T772">čʼam </ts>
               <ts e="T774" id="Seg_4592" n="e" s="T773">mannɨmpɔːtɨt, </ts>
               <ts e="T775" id="Seg_4594" n="e" s="T774">koptɨkɔːl </ts>
               <ts e="T776" id="Seg_4596" n="e" s="T775">tü </ts>
               <ts e="T777" id="Seg_4598" n="e" s="T776">ampatɨ. </ts>
               <ts e="T778" id="Seg_4600" n="e" s="T777">“Əsɨ </ts>
               <ts e="T779" id="Seg_4602" n="e" s="T778">ira </ts>
               <ts e="T780" id="Seg_4604" n="e" s="T779">mɨt </ts>
               <ts e="T781" id="Seg_4606" n="e" s="T780">qətpɔːt </ts>
               <ts e="T782" id="Seg_4608" n="e" s="T781">təpɨn”, – </ts>
               <ts e="T783" id="Seg_4610" n="e" s="T782">nık </ts>
               <ts e="T784" id="Seg_4612" n="e" s="T783">tɛnɨrpɔːtɨt. </ts>
            </ts>
            <ts e="T788" id="Seg_4613" n="sc" s="T785">
               <ts e="T786" id="Seg_4615" n="e" s="T785">Na </ts>
               <ts e="T787" id="Seg_4617" n="e" s="T786">seːlʼčʼi </ts>
               <ts e="T788" id="Seg_4619" n="e" s="T787">ijatɨ. </ts>
            </ts>
            <ts e="T790" id="Seg_4620" n="sc" s="T789">
               <ts e="T790" id="Seg_4622" n="e" s="T789">Tal. </ts>
            </ts>
            <ts e="T793" id="Seg_4623" n="sc" s="T791">
               <ts e="T792" id="Seg_4625" n="e" s="T791">Nɨnɨ </ts>
               <ts e="T793" id="Seg_4627" n="e" s="T792">šittɨmtäl… </ts>
            </ts>
            <ts e="T796" id="Seg_4628" n="sc" s="T794">
               <ts e="T795" id="Seg_4630" n="e" s="T794">Qəntɨla </ts>
               <ts e="T796" id="Seg_4632" n="e" s="T795">tattɔːtɨt. </ts>
            </ts>
            <ts e="T803" id="Seg_4633" n="sc" s="T797">
               <ts e="T798" id="Seg_4635" n="e" s="T797">Poqɨt </ts>
               <ts e="T799" id="Seg_4637" n="e" s="T798">kanaktɨ, </ts>
               <ts e="T800" id="Seg_4639" n="e" s="T799">na </ts>
               <ts e="T801" id="Seg_4641" n="e" s="T800">kɨpa </ts>
               <ts e="T802" id="Seg_4643" n="e" s="T801">ijantɨ </ts>
               <ts e="T803" id="Seg_4645" n="e" s="T802">kanak. </ts>
            </ts>
            <ts e="T807" id="Seg_4646" n="sc" s="T804">
               <ts e="T805" id="Seg_4648" n="e" s="T804">Imatɨ </ts>
               <ts e="T806" id="Seg_4650" n="e" s="T805">pon </ts>
               <ts e="T807" id="Seg_4652" n="e" s="T806">sɔːrɨmpat. </ts>
            </ts>
            <ts e="T815" id="Seg_4653" n="sc" s="T808">
               <ts e="T809" id="Seg_4655" n="e" s="T808">Ɔːtap </ts>
               <ts e="T810" id="Seg_4657" n="e" s="T809">kutɨlʼ </ts>
               <ts e="T811" id="Seg_4659" n="e" s="T810">ətɨmantɨn, </ts>
               <ts e="T812" id="Seg_4661" n="e" s="T811">na </ts>
               <ts e="T813" id="Seg_4663" n="e" s="T812">qälɨn </ts>
               <ts e="T814" id="Seg_4665" n="e" s="T813">ɔːtap </ts>
               <ts e="T815" id="Seg_4667" n="e" s="T814">tɔːqqɔːtɨt. </ts>
            </ts>
            <ts e="T821" id="Seg_4668" n="sc" s="T816">
               <ts e="T817" id="Seg_4670" n="e" s="T816">Kanat </ts>
               <ts e="T818" id="Seg_4672" n="e" s="T817">topɨp </ts>
               <ts e="T819" id="Seg_4674" n="e" s="T818">čʼıqɨlʼnɔːt, </ts>
               <ts e="T820" id="Seg_4676" n="e" s="T819">kanak </ts>
               <ts e="T821" id="Seg_4678" n="e" s="T820">läqalʼnʼa. </ts>
            </ts>
            <ts e="T829" id="Seg_4679" n="sc" s="T822">
               <ts e="T823" id="Seg_4681" n="e" s="T822">Ira </ts>
               <ts e="T824" id="Seg_4683" n="e" s="T823">nılʼ </ts>
               <ts e="T825" id="Seg_4685" n="e" s="T824">nülʼčʼa, </ts>
               <ts e="T826" id="Seg_4687" n="e" s="T825">Nʼomalʼ </ts>
               <ts e="T827" id="Seg_4689" n="e" s="T826">porqɨ </ts>
               <ts e="T828" id="Seg_4691" n="e" s="T827">ira </ts>
               <ts e="T829" id="Seg_4693" n="e" s="T828">mɨta. </ts>
            </ts>
            <ts e="T836" id="Seg_4694" n="sc" s="T830">
               <ts e="T831" id="Seg_4696" n="e" s="T830">Kanamɨ </ts>
               <ts e="T832" id="Seg_4698" n="e" s="T831">qaj </ts>
               <ts e="T833" id="Seg_4700" n="e" s="T832">(ampatɨ), </ts>
               <ts e="T834" id="Seg_4702" n="e" s="T833">qaj </ts>
               <ts e="T835" id="Seg_4704" n="e" s="T834">ɔːtä </ts>
               <ts e="T836" id="Seg_4706" n="e" s="T835">pačʼalnɨt? </ts>
            </ts>
            <ts e="T843" id="Seg_4707" n="sc" s="T837">
               <ts e="T838" id="Seg_4709" n="e" s="T837">Pija </ts>
               <ts e="T839" id="Seg_4711" n="e" s="T838">kɨpa </ts>
               <ts e="T840" id="Seg_4713" n="e" s="T839">ijat </ts>
               <ts e="T841" id="Seg_4715" n="e" s="T840">ima </ts>
               <ts e="T842" id="Seg_4717" n="e" s="T841">ponä </ts>
               <ts e="T843" id="Seg_4719" n="e" s="T842">pakta. </ts>
            </ts>
            <ts e="T850" id="Seg_4720" n="sc" s="T844">
               <ts e="T845" id="Seg_4722" n="e" s="T844">Na </ts>
               <ts e="T846" id="Seg_4724" n="e" s="T845">nʼomalʼ </ts>
               <ts e="T847" id="Seg_4726" n="e" s="T846">porqɨ </ts>
               <ts e="T848" id="Seg_4728" n="e" s="T847">iratɨ </ts>
               <ts e="T849" id="Seg_4730" n="e" s="T848">kɨpa </ts>
               <ts e="T850" id="Seg_4732" n="e" s="T849">ijatɨ. </ts>
            </ts>
            <ts e="T857" id="Seg_4733" n="sc" s="T851">
               <ts e="T852" id="Seg_4735" n="e" s="T851">Ponä </ts>
               <ts e="T853" id="Seg_4737" n="e" s="T852">čʼam </ts>
               <ts e="T854" id="Seg_4739" n="e" s="T853">pakta, </ts>
               <ts e="T855" id="Seg_4741" n="e" s="T854">qaj </ts>
               <ts e="T856" id="Seg_4743" n="e" s="T855">ɛsa, </ts>
               <ts e="T857" id="Seg_4745" n="e" s="T856">iraqotatɨ? </ts>
            </ts>
            <ts e="T862" id="Seg_4746" n="sc" s="T858">
               <ts e="T859" id="Seg_4748" n="e" s="T858">Na </ts>
               <ts e="T860" id="Seg_4750" n="e" s="T859">qaj </ts>
               <ts e="T861" id="Seg_4752" n="e" s="T860">qumɨt, </ts>
               <ts e="T862" id="Seg_4754" n="e" s="T861">kəːŋa. </ts>
            </ts>
            <ts e="T869" id="Seg_4755" n="sc" s="T863">
               <ts e="T864" id="Seg_4757" n="e" s="T863">Əsɨ </ts>
               <ts e="T865" id="Seg_4759" n="e" s="T864">irat </ts>
               <ts e="T866" id="Seg_4761" n="e" s="T865">tam </ts>
               <ts e="T867" id="Seg_4763" n="e" s="T866">nülʼčʼɨku </ts>
               <ts e="T868" id="Seg_4765" n="e" s="T867">na </ts>
               <ts e="T869" id="Seg_4767" n="e" s="T868">mɔːtqɨn. </ts>
            </ts>
            <ts e="T879" id="Seg_4768" n="sc" s="T870">
               <ts e="T871" id="Seg_4770" n="e" s="T870">Mompa </ts>
               <ts e="T872" id="Seg_4772" n="e" s="T871">meː </ts>
               <ts e="T873" id="Seg_4774" n="e" s="T872">mompa </ts>
               <ts e="T874" id="Seg_4776" n="e" s="T873">mɔːnmɨn, </ts>
               <ts e="T875" id="Seg_4778" n="e" s="T874">mompa </ts>
               <ts e="T876" id="Seg_4780" n="e" s="T875">meː </ts>
               <ts e="T877" id="Seg_4782" n="e" s="T876">tüs </ts>
               <ts e="T878" id="Seg_4784" n="e" s="T877">čʼɔːtɨsɨmɨn </ts>
               <ts e="T879" id="Seg_4786" n="e" s="T878">mɨta. </ts>
            </ts>
            <ts e="T885" id="Seg_4787" n="sc" s="T880">
               <ts e="T881" id="Seg_4789" n="e" s="T880">Ilʼčʼa </ts>
               <ts e="T882" id="Seg_4791" n="e" s="T881">mompa </ts>
               <ts e="T883" id="Seg_4793" n="e" s="T882">nılʼ </ts>
               <ts e="T884" id="Seg_4795" n="e" s="T883">šımɨt </ts>
               <ts e="T885" id="Seg_4797" n="e" s="T884">((…)). </ts>
            </ts>
            <ts e="T892" id="Seg_4798" n="sc" s="T886">
               <ts e="T890" id="Seg_4800" n="e" s="T886">((NN:)) Сначала ли что ли? </ts>
               <ts e="T892" id="Seg_4802" n="e" s="T890">((KuAI:)) Говорите, нет. </ts>
            </ts>
            <ts e="T896" id="Seg_4803" n="sc" s="T931">
               <ts e="T893" id="Seg_4805" n="e" s="T931">((NN:)) </ts>
               <ts e="T894" id="Seg_4807" n="e" s="T893">Kɨsa, </ts>
               <ts e="T895" id="Seg_4809" n="e" s="T894">nʼenna </ts>
               <ts e="T896" id="Seg_4811" n="e" s="T895">tomtɨ. </ts>
            </ts>
            <ts e="T910" id="Seg_4812" n="sc" s="T932">
               <ts e="T897" id="Seg_4814" n="e" s="T932">((NEP:)) </ts>
               <ts e="T898" id="Seg_4816" n="e" s="T897">Nɨn </ts>
               <ts e="T899" id="Seg_4818" n="e" s="T898">nɔːtɨ </ts>
               <ts e="T900" id="Seg_4820" n="e" s="T899">na </ts>
               <ts e="T901" id="Seg_4822" n="e" s="T900">ilɨmmɨntɔːt </ts>
               <ts e="T902" id="Seg_4824" n="e" s="T901">nɨmtɨ, </ts>
               <ts e="T903" id="Seg_4826" n="e" s="T902">na </ts>
               <ts e="T904" id="Seg_4828" n="e" s="T903">qapija </ts>
               <ts e="T905" id="Seg_4830" n="e" s="T904">ɛj </ts>
               <ts e="T906" id="Seg_4832" n="e" s="T905">älpä </ts>
               <ts e="T907" id="Seg_4834" n="e" s="T906">qənpɔːtɨt, </ts>
               <ts e="T908" id="Seg_4836" n="e" s="T907">na </ts>
               <ts e="T909" id="Seg_4838" n="e" s="T908">šölʼqumɨt </ts>
               <ts e="T910" id="Seg_4840" n="e" s="T909">tına. </ts>
            </ts>
            <ts e="T916" id="Seg_4841" n="sc" s="T911">
               <ts e="T912" id="Seg_4843" n="e" s="T911">Qapija </ts>
               <ts e="T913" id="Seg_4845" n="e" s="T912">qälɨj </ts>
               <ts e="T914" id="Seg_4847" n="e" s="T913">təttot </ts>
               <ts e="T915" id="Seg_4849" n="e" s="T914">ɔːtap </ts>
               <ts e="T916" id="Seg_4851" n="e" s="T915">tɔːqqɨmmɨntɔːt. </ts>
            </ts>
            <ts e="T920" id="Seg_4852" n="sc" s="T917">
               <ts e="T918" id="Seg_4854" n="e" s="T917">Aj </ts>
               <ts e="T919" id="Seg_4856" n="e" s="T918">na </ts>
               <ts e="T920" id="Seg_4858" n="e" s="T919">šölʼqup… </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_4859" s="T1">NEP_196X_HareParka2_flk.001 (001)</ta>
            <ta e="T9" id="Seg_4860" s="T6">NEP_196X_HareParka2_flk.002 (002)</ta>
            <ta e="T15" id="Seg_4861" s="T921">NEP_196X_HareParka2_flk.003 (003)</ta>
            <ta e="T23" id="Seg_4862" s="T922">NEP_196X_HareParka2_flk.004 (004)</ta>
            <ta e="T32" id="Seg_4863" s="T24">NEP_196X_HareParka2_flk.005 (005)</ta>
            <ta e="T36" id="Seg_4864" s="T33">NEP_196X_HareParka2_flk.006 (006)</ta>
            <ta e="T39" id="Seg_4865" s="T37">NEP_196X_HareParka2_flk.007 (007)</ta>
            <ta e="T46" id="Seg_4866" s="T40">NEP_196X_HareParka2_flk.008 (008)</ta>
            <ta e="T52" id="Seg_4867" s="T47">NEP_196X_HareParka2_flk.009 (009)</ta>
            <ta e="T64" id="Seg_4868" s="T53">NEP_196X_HareParka2_flk.010 (010)</ta>
            <ta e="T72" id="Seg_4869" s="T65">NEP_196X_HareParka2_flk.011 (011)</ta>
            <ta e="T75" id="Seg_4870" s="T73">NEP_196X_HareParka2_flk.012 (012)</ta>
            <ta e="T90" id="Seg_4871" s="T76">NEP_196X_HareParka2_flk.013 (013)</ta>
            <ta e="T100" id="Seg_4872" s="T91">NEP_196X_HareParka2_flk.014 (014)</ta>
            <ta e="T104" id="Seg_4873" s="T101">NEP_196X_HareParka2_flk.015 (015)</ta>
            <ta e="T112" id="Seg_4874" s="T105">NEP_196X_HareParka2_flk.016 (016)</ta>
            <ta e="T114" id="Seg_4875" s="T113">NEP_196X_HareParka2_flk.017 (017)</ta>
            <ta e="T120" id="Seg_4876" s="T115">NEP_196X_HareParka2_flk.018 (018)</ta>
            <ta e="T126" id="Seg_4877" s="T121">NEP_196X_HareParka2_flk.019 (019)</ta>
            <ta e="T135" id="Seg_4878" s="T127">NEP_196X_HareParka2_flk.020 (020)</ta>
            <ta e="T143" id="Seg_4879" s="T136">NEP_196X_HareParka2_flk.021 (021)</ta>
            <ta e="T154" id="Seg_4880" s="T144">NEP_196X_HareParka2_flk.022 (022)</ta>
            <ta e="T159" id="Seg_4881" s="T155">NEP_196X_HareParka2_flk.023 (023)</ta>
            <ta e="T163" id="Seg_4882" s="T160">NEP_196X_HareParka2_flk.024 (024)</ta>
            <ta e="T166" id="Seg_4883" s="T164">NEP_196X_HareParka2_flk.025 (025)</ta>
            <ta e="T177" id="Seg_4884" s="T167">NEP_196X_HareParka2_flk.026 (026)</ta>
            <ta e="T181" id="Seg_4885" s="T178">NEP_196X_HareParka2_flk.027 (027)</ta>
            <ta e="T191" id="Seg_4886" s="T182">NEP_196X_HareParka2_flk.028 (028)</ta>
            <ta e="T202" id="Seg_4887" s="T192">NEP_196X_HareParka2_flk.029 (029)</ta>
            <ta e="T206" id="Seg_4888" s="T203">NEP_196X_HareParka2_flk.030 (030)</ta>
            <ta e="T210" id="Seg_4889" s="T207">NEP_196X_HareParka2_flk.031 (031)</ta>
            <ta e="T218" id="Seg_4890" s="T211">NEP_196X_HareParka2_flk.032 (032)</ta>
            <ta e="T220" id="Seg_4891" s="T219">NEP_196X_HareParka2_flk.033 (033)</ta>
            <ta e="T223" id="Seg_4892" s="T221">NEP_196X_HareParka2_flk.034 (034)</ta>
            <ta e="T240" id="Seg_4893" s="T224">NEP_196X_HareParka2_flk.035 (035)</ta>
            <ta e="T251" id="Seg_4894" s="T241">NEP_196X_HareParka2_flk.036 (036)</ta>
            <ta e="T253" id="Seg_4895" s="T252">NEP_196X_HareParka2_flk.037 (037)</ta>
            <ta e="T260" id="Seg_4896" s="T254">NEP_196X_HareParka2_flk.038 (038)</ta>
            <ta e="T265" id="Seg_4897" s="T261">NEP_196X_HareParka2_flk.039 (039)</ta>
            <ta e="T271" id="Seg_4898" s="T266">NEP_196X_HareParka2_flk.040 (040)</ta>
            <ta e="T279" id="Seg_4899" s="T272">NEP_196X_HareParka2_flk.041 (041)</ta>
            <ta e="T288" id="Seg_4900" s="T280">NEP_196X_HareParka2_flk.042 (042)</ta>
            <ta e="T296" id="Seg_4901" s="T288">NEP_196X_HareParka2_flk.043 (043)</ta>
            <ta e="T308" id="Seg_4902" s="T297">NEP_196X_HareParka2_flk.044 (044)</ta>
            <ta e="T314" id="Seg_4903" s="T309">NEP_196X_HareParka2_flk.045 (045)</ta>
            <ta e="T317" id="Seg_4904" s="T315">NEP_196X_HareParka2_flk.046 (046)</ta>
            <ta e="T319" id="Seg_4905" s="T318">NEP_196X_HareParka2_flk.047 (047)</ta>
            <ta e="T329" id="Seg_4906" s="T320">NEP_196X_HareParka2_flk.048 (048)</ta>
            <ta e="T334" id="Seg_4907" s="T330">NEP_196X_HareParka2_flk.049 (049)</ta>
            <ta e="T340" id="Seg_4908" s="T335">NEP_196X_HareParka2_flk.050 (050)</ta>
            <ta e="T355" id="Seg_4909" s="T341">NEP_196X_HareParka2_flk.051 (051)</ta>
            <ta e="T359" id="Seg_4910" s="T356">NEP_196X_HareParka2_flk.052 (052)</ta>
            <ta e="T365" id="Seg_4911" s="T360">NEP_196X_HareParka2_flk.053 (053)</ta>
            <ta e="T375" id="Seg_4912" s="T366">NEP_196X_HareParka2_flk.054 (054)</ta>
            <ta e="T383" id="Seg_4913" s="T376">NEP_196X_HareParka2_flk.055 (055)</ta>
            <ta e="T397" id="Seg_4914" s="T384">NEP_196X_HareParka2_flk.056 (056)</ta>
            <ta e="T412" id="Seg_4915" s="T398">NEP_196X_HareParka2_flk.057 (057)</ta>
            <ta e="T417" id="Seg_4916" s="T413">NEP_196X_HareParka2_flk.058 (058)</ta>
            <ta e="T430" id="Seg_4917" s="T418">NEP_196X_HareParka2_flk.059 (059)</ta>
            <ta e="T923" id="Seg_4918" s="T431">NEP_196X_HareParka2_flk.060 (060)</ta>
            <ta e="T446" id="Seg_4919" s="T923">NEP_196X_HareParka2_flk.061 (061)</ta>
            <ta e="T448" id="Seg_4920" s="T924">NEP_196X_HareParka2_flk.062 (062)</ta>
            <ta e="T464" id="Seg_4921" s="T449">NEP_196X_HareParka2_flk.063 (063)</ta>
            <ta e="T476" id="Seg_4922" s="T465">NEP_196X_HareParka2_flk.064 (064)</ta>
            <ta e="T480" id="Seg_4923" s="T477">NEP_196X_HareParka2_flk.065 (065)</ta>
            <ta e="T491" id="Seg_4924" s="T481">NEP_196X_HareParka2_flk.066 (066)</ta>
            <ta e="T499" id="Seg_4925" s="T492">NEP_196X_HareParka2_flk.067 (067)</ta>
            <ta e="T516" id="Seg_4926" s="T500">NEP_196X_HareParka2_flk.068 (068)</ta>
            <ta e="T523" id="Seg_4927" s="T517">NEP_196X_HareParka2_flk.069 (069)</ta>
            <ta e="T529" id="Seg_4928" s="T524">NEP_196X_HareParka2_flk.070 (070)</ta>
            <ta e="T531" id="Seg_4929" s="T530">NEP_196X_HareParka2_flk.071 (071)</ta>
            <ta e="T534" id="Seg_4930" s="T926">NEP_196X_HareParka2_flk.072 (072)</ta>
            <ta e="T539" id="Seg_4931" s="T925">NEP_196X_HareParka2_flk.073 (073)</ta>
            <ta e="T551" id="Seg_4932" s="T540">NEP_196X_HareParka2_flk.074 (074)</ta>
            <ta e="T927" id="Seg_4933" s="T552">NEP_196X_HareParka2_flk.075 (075)</ta>
            <ta e="T562" id="Seg_4934" s="T927">NEP_196X_HareParka2_flk.076 (076)</ta>
            <ta e="T567" id="Seg_4935" s="T928">NEP_196X_HareParka2_flk.077 (077)</ta>
            <ta e="T577" id="Seg_4936" s="T568">NEP_196X_HareParka2_flk.078 (078)</ta>
            <ta e="T579" id="Seg_4937" s="T578">NEP_196X_HareParka2_flk.079 (079)</ta>
            <ta e="T583" id="Seg_4938" s="T580">NEP_196X_HareParka2_flk.080 (080)</ta>
            <ta e="T589" id="Seg_4939" s="T584">NEP_196X_HareParka2_flk.081 (081)</ta>
            <ta e="T599" id="Seg_4940" s="T590">NEP_196X_HareParka2_flk.082 (082)</ta>
            <ta e="T603" id="Seg_4941" s="T600">NEP_196X_HareParka2_flk.083 (083)</ta>
            <ta e="T611" id="Seg_4942" s="T604">NEP_196X_HareParka2_flk.084 (084)</ta>
            <ta e="T618" id="Seg_4943" s="T612">NEP_196X_HareParka2_flk.085 (085)</ta>
            <ta e="T632" id="Seg_4944" s="T619">NEP_196X_HareParka2_flk.086 (086)</ta>
            <ta e="T636" id="Seg_4945" s="T633">NEP_196X_HareParka2_flk.087 (087)</ta>
            <ta e="T645" id="Seg_4946" s="T637">NEP_196X_HareParka2_flk.088 (088)</ta>
            <ta e="T656" id="Seg_4947" s="T646">NEP_196X_HareParka2_flk.089 (089)</ta>
            <ta e="T664" id="Seg_4948" s="T657">NEP_196X_HareParka2_flk.090 (090)</ta>
            <ta e="T685" id="Seg_4949" s="T665">NEP_196X_HareParka2_flk.091 (091)</ta>
            <ta e="T693" id="Seg_4950" s="T686">NEP_196X_HareParka2_flk.092 (092)</ta>
            <ta e="T700" id="Seg_4951" s="T694">NEP_196X_HareParka2_flk.093 (093)</ta>
            <ta e="T706" id="Seg_4952" s="T701">NEP_196X_HareParka2_flk.094 (094)</ta>
            <ta e="T710" id="Seg_4953" s="T707">NEP_196X_HareParka2_flk.095 (095)</ta>
            <ta e="T714" id="Seg_4954" s="T711">NEP_196X_HareParka2_flk.096 (096)</ta>
            <ta e="T722" id="Seg_4955" s="T715">NEP_196X_HareParka2_flk.097 (097)</ta>
            <ta e="T725" id="Seg_4956" s="T723">NEP_196X_HareParka2_flk.098 (098)</ta>
            <ta e="T731" id="Seg_4957" s="T930">NEP_196X_HareParka2_flk.099 (099)</ta>
            <ta e="T739" id="Seg_4958" s="T929">NEP_196X_HareParka2_flk.100 (100)</ta>
            <ta e="T741" id="Seg_4959" s="T740">NEP_196X_HareParka2_flk.101 (101)</ta>
            <ta e="T748" id="Seg_4960" s="T742">NEP_196X_HareParka2_flk.102 (102)</ta>
            <ta e="T750" id="Seg_4961" s="T749">NEP_196X_HareParka2_flk.103 (103)</ta>
            <ta e="T757" id="Seg_4962" s="T751">NEP_196X_HareParka2_flk.104 (104)</ta>
            <ta e="T760" id="Seg_4963" s="T758">NEP_196X_HareParka2_flk.105 (105)</ta>
            <ta e="T763" id="Seg_4964" s="T761">NEP_196X_HareParka2_flk.106 (106)</ta>
            <ta e="T770" id="Seg_4965" s="T764">NEP_196X_HareParka2_flk.107 (107)</ta>
            <ta e="T777" id="Seg_4966" s="T771">NEP_196X_HareParka2_flk.108 (108)</ta>
            <ta e="T784" id="Seg_4967" s="T777">NEP_196X_HareParka2_flk.109 (109)</ta>
            <ta e="T788" id="Seg_4968" s="T785">NEP_196X_HareParka2_flk.110 (110)</ta>
            <ta e="T790" id="Seg_4969" s="T789">NEP_196X_HareParka2_flk.111 (111)</ta>
            <ta e="T793" id="Seg_4970" s="T791">NEP_196X_HareParka2_flk.112 (112)</ta>
            <ta e="T796" id="Seg_4971" s="T794">NEP_196X_HareParka2_flk.113 (113)</ta>
            <ta e="T803" id="Seg_4972" s="T797">NEP_196X_HareParka2_flk.114 (114)</ta>
            <ta e="T807" id="Seg_4973" s="T804">NEP_196X_HareParka2_flk.115 (115)</ta>
            <ta e="T815" id="Seg_4974" s="T808">NEP_196X_HareParka2_flk.116 (116)</ta>
            <ta e="T821" id="Seg_4975" s="T816">NEP_196X_HareParka2_flk.117 (117)</ta>
            <ta e="T829" id="Seg_4976" s="T822">NEP_196X_HareParka2_flk.118 (118)</ta>
            <ta e="T836" id="Seg_4977" s="T830">NEP_196X_HareParka2_flk.119 (119)</ta>
            <ta e="T843" id="Seg_4978" s="T837">NEP_196X_HareParka2_flk.120 (120)</ta>
            <ta e="T850" id="Seg_4979" s="T844">NEP_196X_HareParka2_flk.121 (121)</ta>
            <ta e="T857" id="Seg_4980" s="T851">NEP_196X_HareParka2_flk.122 (122)</ta>
            <ta e="T862" id="Seg_4981" s="T858">NEP_196X_HareParka2_flk.123 (123)</ta>
            <ta e="T869" id="Seg_4982" s="T863">NEP_196X_HareParka2_flk.124 (124)</ta>
            <ta e="T879" id="Seg_4983" s="T870">NEP_196X_HareParka2_flk.125 (125)</ta>
            <ta e="T885" id="Seg_4984" s="T880">NEP_196X_HareParka2_flk.126 (126)</ta>
            <ta e="T890" id="Seg_4985" s="T886">NEP_196X_HareParka2_flk.127 (127)</ta>
            <ta e="T892" id="Seg_4986" s="T890">NEP_196X_HareParka2_flk.128 (128)</ta>
            <ta e="T896" id="Seg_4987" s="T931">NEP_196X_HareParka2_flk.129 (129)</ta>
            <ta e="T910" id="Seg_4988" s="T932">NEP_196X_HareParka2_flk.130 (130)</ta>
            <ta e="T916" id="Seg_4989" s="T911">NEP_196X_HareParka2_flk.131 (131)</ta>
            <ta e="T920" id="Seg_4990" s="T917">NEP_196X_HareParka2_flk.132 (132)</ta>
         </annotation>
         <annotation name="st" tierref="st" />
         <annotation name="stl" tierref="stl">
            <ta e="T15" id="Seg_4991" s="T921">Ksa kətät nɔːt qaj.</ta>
            <ta e="T23" id="Seg_4992" s="T922">Mɨ tına nʼomalʼ porqɨ ira qos kətɨsak.</ta>
            <ta e="T32" id="Seg_4993" s="T24">Nʼomalʼ porqɨ ira tına, isɛčʼar selʼčʼɨ ijatɨ ɛppɨntɨ.</ta>
            <ta e="T36" id="Seg_4994" s="T33">Aj selʼčʼi ämnätɨ.</ta>
            <ta e="T39" id="Seg_4995" s="T37">Qarɨt tüŋɔːtɨt.</ta>
            <ta e="T46" id="Seg_4996" s="T40">Qälɨt takkɨn nʼenna tüntɔːtɨn təpɨp qətqa.</ta>
            <ta e="T52" id="Seg_4997" s="T47">Qumɨıːtɨ, ijaıːtɨ mačʼa qənpɔːt - täpälla.</ta>
            <ta e="T64" id="Seg_4998" s="T53">Täpälla mačʼo qənpɔːt tı qup, tı kət, kuttar ɛkka kət.</ta>
            <ta e="T72" id="Seg_4999" s="T65">Tına ijap šʼolqumɨ ijap wərɨmmɨntɨ tına qälɨk.</ta>
            <ta e="T75" id="Seg_5000" s="T73">Na mütɨntɨt.</ta>
            <ta e="T90" id="Seg_5001" s="T76">Nʼomalʼ porqɨ ira mɨta tälɨ mɨta meː tüntɔːmɨt təpɨn mɔːttɨ, tɛː mɨta qonilɨli to.</ta>
            <ta e="T100" id="Seg_5002" s="T91">Ijaqı moqonä qəla, nilʼ kətɨŋɨtɨ, čʼilʼalʼ ijaqı qälɨk mərɨmɨntɨj. </ta>
            <ta e="T104" id="Seg_5003" s="T101">Tam šölʼqumɨj ijaqı…</ta>
            <ta e="T112" id="Seg_5004" s="T105">Taŋaltäš, taŋaltäš lʼi qaj kanap qaj čʼutalalʼta.</ta>
            <ta e="T114" id="Seg_5005" s="T113">Tal.</ta>
            <ta e="T120" id="Seg_5006" s="T115">Nɨnɨ šʼittäl ira kučʼčʼa qattɛnta?</ta>
            <ta e="T126" id="Seg_5007" s="T121">Ira qapija kekɨsan na ippɨntɨ.</ta>
            <ta e="T135" id="Seg_5008" s="T127">Ira nılʼčʼiŋ na ɛsɨsɨ, na nʼomalʼ porqalʼ ira.</ta>
            <ta e="T143" id="Seg_5009" s="T136">Nılʼčʼik ɛsantɨ nɛntɨ sɨtqɨn apsɨp tattɨŋit karrä.</ta>
            <ta e="T154" id="Seg_5010" s="T144">Ira apsɨp tattɨŋit, kušʼšʼal tɛlɨlʼ apsɨp kun montɨ qaj tattɨŋit.</ta>
            <ta e="T159" id="Seg_5011" s="T155">Qälɨt tälɨ tüntɔːtɨt, amɨrtɛntɔːtɨt.</ta>
            <ta e="T163" id="Seg_5012" s="T160">Ira nık kətɨŋɨt.</ta>
            <ta e="T166" id="Seg_5013" s="T164">Apsɨp tattɔːtɨt.</ta>
            <ta e="T177" id="Seg_5014" s="T167">Tıntena tap nʼutɨsa tap mɔːt šʼunčʼɨ šʼentɨ nʼutɨsa toqqɔːtɨt tına.</ta>
            <ta e="T181" id="Seg_5015" s="T178">Ira qälɨıːtɨ apstɨqolamnɨt.</ta>
            <ta e="T191" id="Seg_5016" s="T182">Qəə, toptɨ čʼeːlɨ qälɨn na jap, nɔːssarɨlʼ qumɨlʼ mütɨ.</ta>
            <ta e="T202" id="Seg_5017" s="T192">Tomorot qapı šʼunʼčʼipɨlʼ mɔːtqɨt nʼomalʼ porqɨ ira ontɨ qala ämnäıːntɨsa.</ta>
            <ta e="T206" id="Seg_5018" s="T203">Selʼčʼi nʼutɨsa toqqɨraltɨŋɨt.</ta>
            <ta e="T210" id="Seg_5019" s="T207">Ira nılʼčʼik ɛsa.</ta>
            <ta e="T218" id="Seg_5020" s="T211">Mɔːt jap tultɔːt qapija tına. Na qälɨt amɨrqolamnɔːt.</ta>
            <ta e="T220" id="Seg_5021" s="T219">Na qälɨt amɨrqolamnɔːt.</ta>
            <ta e="T223" id="Seg_5022" s="T221">Apsɨ qontɔːt.</ta>
            <ta e="T240" id="Seg_5023" s="T224">Ämnäıːtɨ čʼoj tap mušɨrolnɔːt, karrʼat kun montɨlʼ apsɨ, ür, ür kun montɨlʼ qaj tına tɛkɨmpɨlʼ apsɨ.</ta>
            <ta e="T251" id="Seg_5024" s="T241">Ira nık kətɨŋɨtɨ imaqotantɨ, nʼomalʼ porqɨ ira imaqotatɨ ɛj ɛːppa.</ta>
            <ta e="T253" id="Seg_5025" s="T252">Tal.</ta>
            <ta e="T260" id="Seg_5026" s="T254">Ämnäımtɨ nık kuraltɨŋɨt: Tɛ ponä tantɨŋɨl.</ta>
            <ta e="T265" id="Seg_5027" s="T261">Ijat kutɨ ponä tattätɨ.</ta>
            <ta e="T271" id="Seg_5028" s="T266">Ira kuntɨ wərka qumɨımtɨ apstɨmpɨla.</ta>
            <ta e="T279" id="Seg_5029" s="T272">Ukkɨr tät čʼontot tümpɨj karrʼat qumɨn amɨrnɔːtɨt.</ta>
            <ta e="T288" id="Seg_5030" s="T280">Karrʼa na šʼoqɨrɨj tüntɨ nannɛrɨk poːs sälɨŋɨtɨ, čʼɔːtɨŋɨtɨ. </ta>
            <ta e="T296" id="Seg_5031" s="T288">Ämnäıːmtɨ ašša qaj pona ütɨmpatɨ poqɨn tü čʼotaltɛntɔːt.</ta>
            <ta e="T308" id="Seg_5032" s="T297">Tü čʼɔːtalpɨntɔːt, tına tölʼ purɨš olɨlʼa nılʼčʼik tɛkkɨrɨmpɨlʼ tomarot, mɔːt tümtɨ.</ta>
            <ta e="T314" id="Seg_5033" s="T309">Tɛː nɛintɨ man na tannɨntak.</ta>
            <ta e="T317" id="Seg_5034" s="T315">Qumɨ amɨrs[ɔːt].</ta>
            <ta e="T319" id="Seg_5035" s="T318">Amɨrnɔːt.</ta>
            <ta e="T329" id="Seg_5036" s="T320">Qälɨt nık kətɔːt: Ilʼčʼa, šʼoqɨrlɨ našʼšʼak qa poːs sälal?</ta>
            <ta e="T334" id="Seg_5037" s="T330">Ašša, šʼoqɨr tü amtɨŋit.</ta>
            <ta e="T340" id="Seg_5038" s="T335">Ašʼšʼa mɨta man mɨta mɨ …</ta>
            <ta e="T355" id="Seg_5039" s="T341">Tɛm ašʼšʼa amtɨtɨ, tɛm ašʼšʼa amtɨtɨ nılʼ mərkata, nılʼ nʼomalʼ porqɨ raša lıpɨ tına.</ta>
            <ta e="T359" id="Seg_5040" s="T356">Meːntalʼ, namɨp šʼerpɨntɨt.</ta>
            <ta e="T365" id="Seg_5041" s="T360">Sʼoŋɨltɨ qanɨqqɨt mərqa mɔːtan ɔːqqɨt.</ta>
            <ta e="T375" id="Seg_5042" s="T366">Ukkɨr tot čʼontot poqɨnɨ mɔːttɨ tına tülʼ purɨš laka alʼčʼintɨ.</ta>
            <ta e="T383" id="Seg_5043" s="T376">Na paktɨmmɨnta tına mɔːtantɨ tına nılʼčʼi ämnäıːtɨ.</ta>
            <ta e="T397" id="Seg_5044" s="T384">Qälɨn čʼam orqɨlpɔːt nılʼ irap ola čʼaŋaj nʼomalʼ porqɨntɨ lıp nɨtqɨlɛːla orqɨltɔːt mɔːtqɨn.</ta>
            <ta e="T412" id="Seg_5045" s="T398">Qapi na tüsa nɨnɨ näːtɨ na okoškan ɔː šʼumɨt mɔːttɨ tılʼčʼi šʼittɨlʼ pɛlalʼ okoška ɛːppa.</ta>
            <ta e="T417" id="Seg_5046" s="T413">Qaj wərqɨ mɔːttɨ ɛːppa.</ta>
            <ta e="T430" id="Seg_5047" s="T418">Mɔːttɨ nɛintɨ čʼusa taqpɨmpa, tɨna loːqira polʼ mɔːt pičʼit paqɨt pirɨlʼ taqpa.</ta>
            <ta e="T923" id="Seg_5048" s="T431">Mompa mɔːtqɨn tına na tüm na qəlpɨntɔːt, na qəttɨmmɨntɨtɨ qapi ton irap.</ta>
            <ta e="T446" id="Seg_5049" s="T923">((NN:)) Qapi tam amn. </ta>
            <ta e="T448" id="Seg_5050" s="T924">((NEP:)) Tal.</ta>
            <ta e="T464" id="Seg_5051" s="T449">Poraksa qaj təm mɨta qamnɨmpat na; na nʼutɨt ɨlqəlʼ pɛlat tɨlʼčʼi šʼentɨ nʼutɨ ɨlqɨlʼ pɛlat.</ta>
            <ta e="T476" id="Seg_5052" s="T465">Na tü na qəttɨmmɨntɨtɨ, tına nɔːssaralʼ qumɨlʼ mütɨp muntɨk tü ampat.</ta>
            <ta e="T480" id="Seg_5053" s="T477">Qən olʼčʼot nʼennat.</ta>
            <ta e="T491" id="Seg_5054" s="T481">Nʼenna pon ira pona pakta mɔːtan ɔːt, pičʼilʼ ut mərka.</ta>
            <ta e="T499" id="Seg_5055" s="T492">Isačʼa pona qontšʼelʼčʼɔːt aj čʼari qəttɨŋɨt.</ta>
            <ta e="T516" id="Seg_5056" s="T500">Ämnäıːtɨ na tü na qənmmɨntɨt, šʼoqɨr por šʼumɨt qəŋɔːt, nɨn ɛj šʼit okoškon ɔː šʼumɨt qəŋɔːtɨn.</ta>
            <ta e="T523" id="Seg_5057" s="T517">Muntɨk tüs čʼɔːttɛıːmpatɨ, na qälɨlʼ mütɨp.</ta>
            <ta e="T529" id="Seg_5058" s="T524">Na mütɨ qotɨ šʼittä ɛːppɨntɔːt.</ta>
            <ta e="T534" id="Seg_5059" s="T926">Na kətät.</ta>
            <ta e="T539" id="Seg_5060" s="T925">Pɛlakɨtɨj ira kučʼčʼa qattɛnta? </ta>
            <ta e="T551" id="Seg_5061" s="T540">Ira nɨnɨ šʼittalʼ, mɨŋa, nʼomalʼ porqɨ ira karrʼa qälɨıːntɨ qaqlontɨ qəŋa.</ta>
            <ta e="T927" id="Seg_5062" s="T552">Aj šʼittämi na qat timɨntɨ takkɨ qälɨj mɔːttɨ qənqo.</ta>
            <ta e="T562" id="Seg_5063" s="T927">ılla omtäš.</ta>
            <ta e="T567" id="Seg_5064" s="T928">Na qətqɨlpɨtɨj qumɨıːqɨntɨ qənŋɨntɔːqa.</ta>
            <ta e="T577" id="Seg_5065" s="T568">Karrʼa qənna, ooo karrʼan konna močʼalʼ qälɨ ira qontɨšʼentɨ.</ta>
            <ta e="T583" id="Seg_5066" s="T580">Qaj na lakɨmɔːttak.</ta>
            <ta e="T589" id="Seg_5067" s="T584">Qaj nʼomalʼ porqɨ irat qapija imap qəčʼisɨlɨ.</ta>
            <ta e="T599" id="Seg_5068" s="T590">Nʼomalʼ porqɨ ira čʼarrä qəttɨŋɨtɨ, päqɨlʼ šuktɨ qottä säppaja.</ta>
            <ta e="T603" id="Seg_5069" s="T600">Nɨnɨ karrä kurolna.</ta>
            <ta e="T611" id="Seg_5070" s="T604">Karrä kurolna, nɨnɨ nɔːkɨr ämnäsɨt takkɨ qənnɔːt.</ta>
            <ta e="T618" id="Seg_5071" s="T612">Takkɨ qälɨn mɔːt takkɨn ɛːppa točʼčʼotot.</ta>
            <ta e="T632" id="Seg_5072" s="T619">Karrʼa qənnɔːt nɨmtɨ, näčʼčʼa laqaltɛlʼčʼɔːt qälɨtɨt qaqlɨsa, na qumɨıːt qaqlɨsa, na nɔkɨr ilʼčʼasɨt.</ta>
            <ta e="T636" id="Seg_5073" s="T633">Takkɨ laqaltɛla qənnɔːt.</ta>
            <ta e="T645" id="Seg_5074" s="T637">Tına takkɨn na qälɨlʼ imatɨtɨp na šʼuqɨlpɨntɨ.</ta>
            <ta e="T656" id="Seg_5075" s="T646">Tına šʼolqumɨj ijaqı näčʼčʼatɨqɨt namä qalɨmpɔː tomoroqɨt na ilʼčʼantɨn tüptɨ.</ta>
            <ta e="T664" id="Seg_5076" s="T657">Konnätɨ tına mɨntɨ šittɨ qälɨk moqonä ütɨmmɨntɔːt.</ta>
            <ta e="T685" id="Seg_5077" s="T665">Mɔːttɨ tɛpɨktɨ, kun montɨ qajtɨ naɛntɨ, nʼomalʼ porqɨ ira ɔːtäsa sɨr por nʼenna toqɨŋɨt, na na čʼʼilʼalʼ ija ɛj təpɨsa ɛːsɔːqı. </ta>
            <ta e="T693" id="Seg_5078" s="T686">Tomorot təttɨčʼat təpɨt mɨqɨt qälɨj, na mɨ.</ta>
            <ta e="T700" id="Seg_5079" s="T694">Nʼomalʼ porqɨ ira təttɨčʼälɨ kətɨsanqıtɨ ɛːppɨntɔ.</ta>
            <ta e="T706" id="Seg_5080" s="T701">Na qətpɨlʼ qumɨıːtɨt, na qälɨn.</ta>
            <ta e="T710" id="Seg_5081" s="T707">Namɨn ijaqı šʼittɨ.</ta>
            <ta e="T714" id="Seg_5082" s="T711">Nılʼčʼi šʼittɨ ijaqı.</ta>
            <ta e="T722" id="Seg_5083" s="T715">Ɔːtap toqɨla moqɨn tattɔːt tıntena tına mɔːttɨ.</ta>
            <ta e="T725" id="Seg_5084" s="T723">Tına mɔːtɨŋɔːt.</ta>
            <ta e="T731" id="Seg_5085" s="T930">Lʼa na namɨlʼa nannɛr qəša.</ta>
            <ta e="T739" id="Seg_5086" s="T929">Ijaıːtɨ tam na qənmmɨntɔːt šʼöttɨ täpällä surɨlllä.</ta>
            <ta e="T748" id="Seg_5087" s="T742">Konnä mɔːtɨŋnɔːt, tal, ukkɨr pit šʼäqɔːt.</ta>
            <ta e="T750" id="Seg_5088" s="T749">Šʼäqɨqolamnɔːt.</ta>
            <ta e="T757" id="Seg_5089" s="T751">Ijaıːtɨ sʼötqɨn jap tattɔːtɨn täpäčʼilʼmɨn moqonä.</ta>
            <ta e="T760" id="Seg_5090" s="T758">Qəntala tattɔːt.</ta>
            <ta e="T763" id="Seg_5091" s="T761">Qaj ɛsɨ? </ta>
            <ta e="T770" id="Seg_5092" s="T764">Əsɨ ira mɨt tıntena mɔːttɨ təptɨ. </ta>
            <ta e="T777" id="Seg_5093" s="T771">Karrä čʼam mannɨmpɔːtɨt, koptɨkol tü ampatɨ.</ta>
            <ta e="T784" id="Seg_5094" s="T777">Əsɨ ira mɨt qətpɔːt təpɨn, - nık tɛnɨrpɔːtɨt.</ta>
            <ta e="T788" id="Seg_5095" s="T785">Na seːlʼčʼi ijatɨ.</ta>
            <ta e="T790" id="Seg_5096" s="T789">Tal.</ta>
            <ta e="T793" id="Seg_5097" s="T791">Nɨnɨ šʼittɨmtäl…</ta>
            <ta e="T796" id="Seg_5098" s="T794">Qəmpɨla tattɔːt.</ta>
            <ta e="T803" id="Seg_5099" s="T797">Poqɨt kanaktɨ, na kɨpa ijantɨ kanak. </ta>
            <ta e="T807" id="Seg_5100" s="T804">Imatɨ pon sorɨmpat.</ta>
            <ta e="T815" id="Seg_5101" s="T808">Ɔːtap kutɨlʼ etɨmantona, na qälɨn ɔːtap toqqɔːtɨt.</ta>
            <ta e="T821" id="Seg_5102" s="T816">Kanat topɨp čʼɨqɨlnɔːt, kanak läqalʼnʼa.</ta>
            <ta e="T829" id="Seg_5103" s="T822">Ira nılʼ nülʼčʼa, nʼomalʼ porqɨ ira mɨta. </ta>
            <ta e="T836" id="Seg_5104" s="T830">Kanamɨ qaj ampatɨ qaj ɔːtä pačʼalnɨt?</ta>
            <ta e="T843" id="Seg_5105" s="T837">Pija kɨpa ijat ima ponä pakta.</ta>
            <ta e="T850" id="Seg_5106" s="T844">Nʼomalʼ porqɨ iratɨ kɨpa ijatɨ.</ta>
            <ta e="T857" id="Seg_5107" s="T851">Ponä čʼam pakta, qaj ɛsa? Iraqotatɨ.</ta>
            <ta e="T862" id="Seg_5108" s="T858">Na qaj qumɨt - kəqa.</ta>
            <ta e="T869" id="Seg_5109" s="T863">Əsɨ irat tam nülʼčikun na mɔːtqɨn.</ta>
            <ta e="T879" id="Seg_5110" s="T870">Mompa meː mompa mɔːnmɨn, mompa meː tüs čʼotɨsɨmɨn mɨta. </ta>
            <ta e="T885" id="Seg_5111" s="T880">Ilʼčʼan mɨta nılʼ šʼımɨt pɨntaltɨsɨ.</ta>
            <ta e="T896" id="Seg_5112" s="T931">Kɨsa, nʼenna tomtɨ.</ta>
            <ta e="T910" id="Seg_5113" s="T932">Nɨn nɔːtɨ na ilɨmmɨntɔːt nɨmtɨ, na qapija ɛj älpä qənpɔːtɨt, na šʼölqumɨt tına.</ta>
            <ta e="T916" id="Seg_5114" s="T911">Qapija qälɨj təttot ɔːtap toqqɨmmɨntɔːt.</ta>
            <ta e="T920" id="Seg_5115" s="T917">Aj na šʼölʼqup…</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_5116" s="T1">((KuAI:)) Сказка, записанная Елизаветой Павловной. </ta>
            <ta e="T9" id="Seg_5117" s="T6">Ну пожалуйста, говорите. </ta>
            <ta e="T15" id="Seg_5118" s="T921">((NN:)) Ksa kətät nɔːt (jarɨk) qaj. </ta>
            <ta e="T23" id="Seg_5119" s="T922">((NEP:)) Mɨ tına Nʼomalʼ porqɨ ira qos kətɨšak. </ta>
            <ta e="T32" id="Seg_5120" s="T24">Nʼomalʼ porqɨ ira tına, isɛčʼar selʼčʼɨ ijatɨ ɛppɨntɨ. </ta>
            <ta e="T36" id="Seg_5121" s="T33">Aj selʼčʼi ämnätɨ. </ta>
            <ta e="T39" id="Seg_5122" s="T37">Qarɨt tüŋɔːtɨt. </ta>
            <ta e="T46" id="Seg_5123" s="T40">Qälɨt takkɨn nʼenna tüŋɔːtɨn təpɨp qətqɨntoːqo. </ta>
            <ta e="T52" id="Seg_5124" s="T47">Qumiːtɨ, ijaiːtɨ mačʼa qənpɔːtɨt täpällä. </ta>
            <ta e="T64" id="Seg_5125" s="T53">Täpällä mačʼo qənpɔːt tıː qup, tıː kət, tiː kuttar ɛkka kət. </ta>
            <ta e="T72" id="Seg_5126" s="T65">Tına ijap šʼolqumɨj ijap wərɨmmɨntɨ tına qälɨk. </ta>
            <ta e="T75" id="Seg_5127" s="T73">Na mütɨntɨt. </ta>
            <ta e="T90" id="Seg_5128" s="T76">Nʼomalʼ porqɨ ira mɨta täːlɨ mɨta meː tüntɔːmɨt təpɨn mɔːttɨ, tɛː mɨta qonniŋɨlɨt toː. </ta>
            <ta e="T100" id="Seg_5129" s="T91">Ijaqı moqonä qəllä, nık kətɨŋɨtɨ, čʼilʼalʼ ijaqı qälɨk mərɨmɨntɨj. </ta>
            <ta e="T104" id="Seg_5130" s="T101">Tam šölʼqumɨj ijaqı… </ta>
            <ta e="T112" id="Seg_5131" s="T105">Taŋaltäš, taŋaltäš lʼi qaj kanap qaj (čʼütalʼalta). </ta>
            <ta e="T114" id="Seg_5132" s="T113">Tal. </ta>
            <ta e="T120" id="Seg_5133" s="T115">Nɨnɨ šittäl ira kučʼčʼa qattɛnta? </ta>
            <ta e="T126" id="Seg_5134" s="T121">Ira qapija kekkɨsa na ippɨnta. </ta>
            <ta e="T135" id="Seg_5135" s="T127">Ira nılʼčʼiŋ na ɛsɨsɨ, na Nʼomalʼ porqɨlʼ ira. </ta>
            <ta e="T143" id="Seg_5136" s="T136">Nılʼčʼik ɛsa amnäntɨlʼ mɨtkine: “Apsɨp tattɨŋɨt karrä. </ta>
            <ta e="T154" id="Seg_5137" s="T144">Ür, apsɨp tattɨŋɨt, kuššalʼ tɛlɨlʼ apsɨp kun montɨ qaj tattɨŋɨt. </ta>
            <ta e="T159" id="Seg_5138" s="T155">Qälɨt täːlɨ tüntɔːtɨt, amɨrtɛntɔːtɨt”. </ta>
            <ta e="T163" id="Seg_5139" s="T160">Ira nık kətɨŋɨt. </ta>
            <ta e="T166" id="Seg_5140" s="T164">Apsɨp tattɔːtɨt. </ta>
            <ta e="T177" id="Seg_5141" s="T167">Tıntena tap nʼuːtɨsa tap mɔːt šünʼčʼɨ šentɨ nʼuːtɨsa tɔːqqɔːtɨt tına. </ta>
            <ta e="T181" id="Seg_5142" s="T178">Ira qälim apsɨtɨqolamnɨt. </ta>
            <ta e="T191" id="Seg_5143" s="T182">Qəə, toptɨlʼ čʼeːlɨ qälɨn (na jap), nɔːssarɨlʼ qumɨlʼ müːtɨ. </ta>
            <ta e="T202" id="Seg_5144" s="T192">Tomorot, qapı šünʼčʼipɨlʼ mɔːtqɨt Nʼomalʼ porqɨ ira ontɨ qala ämnäiːntɨsa. </ta>
            <ta e="T206" id="Seg_5145" s="T203">Selʼčʼi nʼuːtɨsa tɔːqqɨraltɨŋɨt. </ta>
            <ta e="T210" id="Seg_5146" s="T207">Ira nılʼčʼik ɛsa. </ta>
            <ta e="T218" id="Seg_5147" s="T211">Mɔːt jap tultɔːt qapija tına, na qälɨt. </ta>
            <ta e="T220" id="Seg_5148" s="T219">Amɨrqolamnɔːt. </ta>
            <ta e="T223" id="Seg_5149" s="T221">Apsɨ qontɔːt. </ta>
            <ta e="T240" id="Seg_5150" s="T224">Ämnäiːtɨ čʼoj tap mušɨrɔːlnɔːt, karrʼat kun mɔːntɨlʼ apsɨ, ür, ür kun mɔːntɨlʼ qaj tına təːkɨmpɨlʼ apsɨ. </ta>
            <ta e="T251" id="Seg_5151" s="T241">Ira nık kətɨŋɨtɨ imaqotantɨ, Nʼomalʼ porqɨ ira imaqotatɨ ɛj ɛːppa. </ta>
            <ta e="T253" id="Seg_5152" s="T252">Tal. </ta>
            <ta e="T260" id="Seg_5153" s="T254">Ämnäiːmtɨ nık kuraltɨŋɨt: “Tɛː ponä tantɨŋɨl. </ta>
            <ta e="T265" id="Seg_5154" s="T261">Ijat kuti ponä tattätɨ.” </ta>
            <ta e="T271" id="Seg_5155" s="T266">Ira kuntɨ wərka qumiːmtɨ apsɨtɨmpɨla. </ta>
            <ta e="T279" id="Seg_5156" s="T272">Ukkɨr tät čʼontot tümpɨj karrʼat qumɨn amɨrnɔːtɨt. </ta>
            <ta e="T288" id="Seg_5157" s="T280">Karrʼa na šoːqɨrɨj tüntɨ nannɛrɨk poːsä (sälɨŋɨtɨ), čʼɔːtɨŋɨtɨ. </ta>
            <ta e="T296" id="Seg_5158" s="T288">Ämnäiːmtɨ ašša qaː pona üːtɨmpatɨ poːqɨn tü čʼɔːtaltɛntɔːt. </ta>
            <ta e="T308" id="Seg_5159" s="T297">Tü čʼɔːtalpɨntɔːtɨt, tına tölʼ purɨš olɨlʼa nılʼčʼik təːkɨrɨmpɨlʼ tomarot, mɔːt (tümtɨ). </ta>
            <ta e="T314" id="Seg_5160" s="T309">“Tɛː najenta man na tannɛntak.” </ta>
            <ta e="T317" id="Seg_5161" s="T315">Qumɨn amɨrsɔːtɨt. </ta>
            <ta e="T319" id="Seg_5162" s="T318">Amɨrnɔːtɨt. </ta>
            <ta e="T329" id="Seg_5163" s="T320">Qälɨt nık kətɔːt: “Ilʼčʼa, šoːqɨrlɨ naššak qajqo poːsä sälal?” </ta>
            <ta e="T334" id="Seg_5164" s="T330">“Ašša, šoːqɨr tü amtɨŋɨt. </ta>
            <ta e="T340" id="Seg_5165" s="T335">Ašša mɨta man mɨta mɨ…” </ta>
            <ta e="T355" id="Seg_5166" s="T341">Tɛm ašša amtɨtɨ, tɛm ašša amtɨtɨ nılʼ mərkata, nılʼ nʼomalʼ porqɨ raša lıːpɨ tına. </ta>
            <ta e="T359" id="Seg_5167" s="T356">Meːntalʼ, namɨp šeːrpɨntɨt. </ta>
            <ta e="T365" id="Seg_5168" s="T360">Šoŋoltɨ qanɨqqɨt mərqa mɔːtantɨ qanɨqqɨt. </ta>
            <ta e="T375" id="Seg_5169" s="T366">Ukkɨr tot čʼontot poːqɨnɨ mɔːttɨ tına tülʼ purɨšlaka alʼčʼintɨ. </ta>
            <ta e="T383" id="Seg_5170" s="T376">Na paktɨmmɨnta tına mɔːtantɨ tına nılʼčʼi ämnäiːtɨ. </ta>
            <ta e="T397" id="Seg_5171" s="T384">Qälɨn čʼam orqɨlpɔːt nılʼ irap ola čʼaŋaj nʼomalʼ porqɨntɨ lıːp nɨtqɨlɛːla orqɨltɔːt mɔːtqɨn. </ta>
            <ta e="T412" id="Seg_5172" s="T398">Qapi na tüsa nɨnɨ näːtɨ okoškan ɔːš šüːmɨt mɔːttɨ (tılʼčʼɨ) šittɨlʼ pɛlalʼ okoška ɛːppa. </ta>
            <ta e="T417" id="Seg_5173" s="T413">Qaj wərqɨ mɔːttɨ ɛːppa. </ta>
            <ta e="T430" id="Seg_5174" s="T418">Mɔːttɨ najentɨ čʼusa taqpɨmpa, tɨna loːqɨra polʼ mɔːt, pičʼit paqɨt piːrɨlʼ taqpa. </ta>
            <ta e="T923" id="Seg_5175" s="T431">Mompa mɔːtqɨt tına na tüm na qəːlpɨntɔːtɨt, na qättɨmmɨntɨtɨ qapi ton irap. </ta>
            <ta e="T446" id="Seg_5176" s="T923">((NN:)) Qapi tam amn. </ta>
            <ta e="T448" id="Seg_5177" s="T924">((NEP:)) Tal. </ta>
            <ta e="T464" id="Seg_5178" s="T449">Poraksa qaj təm mɨta qamnɨmpat (na=) na nʼuːtɨt ɨlqɨlʼ pɛlat tilʼčʼi šentɨ nʼuːtɨ ɨlqɨlʼ pɛlat. </ta>
            <ta e="T476" id="Seg_5179" s="T465">Na tü na qättɨmmɨntɨtɨ, tına nɔːssaralʼ qumɨlʼ mütɨp muntɨk tü ampat. </ta>
            <ta e="T480" id="Seg_5180" s="T477">Qən ɔːlʼčʼɔːt nʼennat. </ta>
            <ta e="T491" id="Seg_5181" s="T481">Nʼennä pon ira pona pakta mɔːtan ɔːt, pičʼilʼ ut mərka. </ta>
            <ta e="T499" id="Seg_5182" s="T492">Isɨ čʼap pona qontɨšɛlʼčʼɔːt aj čʼari qättɨŋɨt. </ta>
            <ta e="T516" id="Seg_5183" s="T500">Ämnäiːtɨ na tü na qəːmmɨntɨt, šoːqɨr pɔːr šüːmɨt qəːŋɔːt, nɨn ɛj šit okoškon ɔːš šüːmɨt qəːŋɔːtɨn. </ta>
            <ta e="T523" id="Seg_5184" s="T517">Muntɨk tüs čʼɔːttɛıːmpatɨ, na qälɨlʼ mütɨp. </ta>
            <ta e="T529" id="Seg_5185" s="T524">Na mütɨ qɔːtɨ šittä ɛːppɨntɔːt. </ta>
            <ta e="T531" id="Seg_5186" s="T530">((KuAI:)) Говорите. </ta>
            <ta e="T534" id="Seg_5187" s="T926">((NN:)) Na kətät. </ta>
            <ta e="T539" id="Seg_5188" s="T925">((NEP:)) Pɛlakɨtɨj ira kučʼčʼa qattɛnta? </ta>
            <ta e="T551" id="Seg_5189" s="T540">Ira nɨnɨ šʼittalʼ, mɨŋa, Nʼomalʼ porqɨ ira, karrä qälɨiːntɨ qaqlontɨ (qəŋa). </ta>
            <ta e="T927" id="Seg_5190" s="T552">Aj šittämɨ ((…)) takkɨ ((…)) qälɨj mɔːttɨ (qənqo). </ta>
            <ta e="T562" id="Seg_5191" s="T927">((NN:)) Illa omtäš. </ta>
            <ta e="T567" id="Seg_5192" s="T928">((NEP:)) Na qətqɨlpɨtɨj qumɨiːqɨntɨ qənqɨntoːqo. </ta>
            <ta e="T577" id="Seg_5193" s="T568">Karrä qənna, ooo karrän konna moːčʼalʼ qälɨ ira qontɨšeintɨ. </ta>
            <ta e="T579" id="Seg_5194" s="T578">((LAUGH)). </ta>
            <ta e="T583" id="Seg_5195" s="T580">Qaj na laqɨmɔːttak. </ta>
            <ta e="T589" id="Seg_5196" s="T584">Qaj Nʼomalʼ porqɨ irat ((…)). </ta>
            <ta e="T599" id="Seg_5197" s="T590">Nʼomalʼ porqɨ ira čʼarrä qättɨŋɨtɨ, päqɨlʼ šuktɨ qottä (säppaja). </ta>
            <ta e="T603" id="Seg_5198" s="T600">Nɨnɨ karrä kurɔːlna. </ta>
            <ta e="T611" id="Seg_5199" s="T604">Karrä kurɔːlna, nɨnɨ nɔːkɨr ämnäsɨt takkɨ qənnɔːt. </ta>
            <ta e="T618" id="Seg_5200" s="T612">Takkɨ qälɨn mɔːt takkɨn ɛːppa točʼčʼotot. </ta>
            <ta e="T632" id="Seg_5201" s="T619">Karrʼa qənnɔːt nɨmtɨ, näčʼčʼa laqaltɛlʼčʼɔːt qälɨtɨt qaqlɨsa, na qumiːt qaqlɨsa, na nɔːkɨr ilʼčʼasɨt. </ta>
            <ta e="T636" id="Seg_5202" s="T633">Takkɨ laqaltɛːla qənnɔːt. </ta>
            <ta e="T645" id="Seg_5203" s="T637">Tına takkɨn na qälɨlʼ ((…)) imatɨtɨp na šuqɨlpɨntɨ. </ta>
            <ta e="T656" id="Seg_5204" s="T646">Tına šʼolqumɨj ijaqı näčʼčʼatɨqɨt namä qalɨmpɔːqı tomoroqɨt na ilʼčʼantıːn tümpɨqı. </ta>
            <ta e="T664" id="Seg_5205" s="T657">Konnätɨ tına (mɨntɨ) šittɨ qälɨk moqonä üːtɨmmɨntɔːt. </ta>
            <ta e="T685" id="Seg_5206" s="T665">Mɔːttɨ, tɛːpɨktɨ, (kun) qajtɨ naɛntɨ, Nʼomalʼ porqɨ ira ɔːtäsa sɨr por nʼenna tɔːqqɨŋɨt, na na čʼilʼalʼ ija ɛj təpɨsa ɛːsɔːqı. </ta>
            <ta e="T693" id="Seg_5207" s="T686">Tomorot təttɨčʼat təpɨt mɨqɨt qälɨj, na mɨ. </ta>
            <ta e="T700" id="Seg_5208" s="T694">Nʼomalʼ porqɨ ira təttɨčʼälɨ kətsanqıtɨ ɛːppɨntɔːqı. </ta>
            <ta e="T706" id="Seg_5209" s="T701">Na qətpɨlʼ qumɨtɨt, na qälɨn. </ta>
            <ta e="T710" id="Seg_5210" s="T707">Namɨn ijaqı šʼittɨ. </ta>
            <ta e="T714" id="Seg_5211" s="T711">Nılʼčʼi šʼittɨ ijaqı. </ta>
            <ta e="T722" id="Seg_5212" s="T715">Ɔːtap tɔːqqɨla moqɨn tattɔːt tıntena (tına) mɔːttɨ. </ta>
            <ta e="T725" id="Seg_5213" s="T723">Tına mɔːtɨŋnɔːt. </ta>
            <ta e="T731" id="Seg_5214" s="T930">((NN:)) Lʼa na namɨlʼa nannɛr qəša. </ta>
            <ta e="T739" id="Seg_5215" s="T929">((NEP:)) Ijaiːtɨ tam na qənmmɨntɔːt šöttɨ täpällä surɨllä. </ta>
            <ta e="T741" id="Seg_5216" s="T740">((NN:)) ((…)). </ta>
            <ta e="T748" id="Seg_5217" s="T742">Konnä mɔːtɨŋnɔːt, tal, ukkɨr pit šäqɔːt. </ta>
            <ta e="T750" id="Seg_5218" s="T749">Šäqɨqolamnɔːt. </ta>
            <ta e="T757" id="Seg_5219" s="T751">Ijaiːtɨ šötqɨnɨ jap tattɔːtɨt täpäčʼilʼmɨn moqonä. </ta>
            <ta e="T760" id="Seg_5220" s="T758">Kəntalla tattɔːt. </ta>
            <ta e="T763" id="Seg_5221" s="T761">Qaj ɛsɨ? </ta>
            <ta e="T770" id="Seg_5222" s="T764">Əsɨ ira mɨt tıntena mɔːttɨ təptɨt. </ta>
            <ta e="T777" id="Seg_5223" s="T771">Karrä čʼam mannɨmpɔːtɨt, koptɨkɔːl tü ampatɨ. </ta>
            <ta e="T784" id="Seg_5224" s="T777">“Əsɨ ira mɨt qətpɔːt təpɨn”, – nık tɛnɨrpɔːtɨt. </ta>
            <ta e="T788" id="Seg_5225" s="T785">Na seːlʼčʼi ijatɨ. </ta>
            <ta e="T790" id="Seg_5226" s="T789">Tal. </ta>
            <ta e="T793" id="Seg_5227" s="T791">Nɨnɨ šittɨmtäl… </ta>
            <ta e="T796" id="Seg_5228" s="T794">Qəntɨla tattɔːtɨt. </ta>
            <ta e="T803" id="Seg_5229" s="T797">Poqɨt kanaktɨ, na kɨpa ijantɨ kanak. </ta>
            <ta e="T807" id="Seg_5230" s="T804">Imatɨ pon sɔːrɨmpat. </ta>
            <ta e="T815" id="Seg_5231" s="T808">Ɔːtap kutɨlʼ ətɨmantɨn, na qälɨn ɔːtap tɔːqqɔːtɨt. </ta>
            <ta e="T821" id="Seg_5232" s="T816">Kanat topɨp čʼıqɨlʼnɔːt, kanak läqalʼnʼa. </ta>
            <ta e="T829" id="Seg_5233" s="T822">Ira nılʼ nülʼčʼa, Nʼomalʼ porqɨ ira mɨta. </ta>
            <ta e="T836" id="Seg_5234" s="T830">Kanamɨ qaj (ampatɨ), qaj ɔːtä pačʼalnɨt? </ta>
            <ta e="T843" id="Seg_5235" s="T837">Pija kɨpa ijat ima ponä pakta. </ta>
            <ta e="T850" id="Seg_5236" s="T844">Na nʼomalʼ porqɨ iratɨ kɨpa ijatɨ. </ta>
            <ta e="T857" id="Seg_5237" s="T851">Ponä čʼam pakta, qaj ɛsa, iraqotatɨ? </ta>
            <ta e="T862" id="Seg_5238" s="T858">Na qaj qumɨt, kəːŋa. </ta>
            <ta e="T869" id="Seg_5239" s="T863">Əsɨ irat tam nülʼčʼɨku na mɔːtqɨn. </ta>
            <ta e="T879" id="Seg_5240" s="T870">Mompa meː mompa mɔːnmɨn, mompa meː tüs čʼɔːtɨsɨmɨn mɨta. </ta>
            <ta e="T885" id="Seg_5241" s="T880">Ilʼčʼa mompa nılʼ šımɨt ((…)). </ta>
            <ta e="T890" id="Seg_5242" s="T886">((NN:)) Сначала ли что ли? </ta>
            <ta e="T892" id="Seg_5243" s="T890">((KuAI:)) Говорите, нет. </ta>
            <ta e="T896" id="Seg_5244" s="T931">((NN:)) Kɨsa, nʼenna tomtɨ. </ta>
            <ta e="T910" id="Seg_5245" s="T932">((NEP:)) Nɨn nɔːtɨ na ilɨmmɨntɔːt nɨmtɨ, na qapija ɛj älpä qənpɔːtɨt, na šölʼqumɨt tına. </ta>
            <ta e="T916" id="Seg_5246" s="T911">Qapija qälɨj təttot ɔːtap tɔːqqɨmmɨntɔːt. </ta>
            <ta e="T920" id="Seg_5247" s="T917">Aj na šölʼqup… </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T11" id="Seg_5248" s="T10">ksa</ta>
            <ta e="T12" id="Seg_5249" s="T11">kət-ät</ta>
            <ta e="T13" id="Seg_5250" s="T12">nɔːt</ta>
            <ta e="T14" id="Seg_5251" s="T13">jarɨk</ta>
            <ta e="T15" id="Seg_5252" s="T14">qaj</ta>
            <ta e="T17" id="Seg_5253" s="T16">mɨ</ta>
            <ta e="T18" id="Seg_5254" s="T17">tına</ta>
            <ta e="T19" id="Seg_5255" s="T18">nʼoma-lʼ</ta>
            <ta e="T20" id="Seg_5256" s="T19">porqɨ</ta>
            <ta e="T21" id="Seg_5257" s="T20">ira</ta>
            <ta e="T22" id="Seg_5258" s="T21">qos</ta>
            <ta e="T23" id="Seg_5259" s="T22">kətɨ-ša-k</ta>
            <ta e="T25" id="Seg_5260" s="T24">nʼoma-lʼ</ta>
            <ta e="T26" id="Seg_5261" s="T25">porqɨ</ta>
            <ta e="T27" id="Seg_5262" s="T26">ira</ta>
            <ta e="T28" id="Seg_5263" s="T27">tına</ta>
            <ta e="T29" id="Seg_5264" s="T28">isɛčʼar</ta>
            <ta e="T30" id="Seg_5265" s="T29">selʼčʼɨ</ta>
            <ta e="T31" id="Seg_5266" s="T30">ija-tɨ</ta>
            <ta e="T32" id="Seg_5267" s="T31">ɛ-ppɨ-ntɨ</ta>
            <ta e="T34" id="Seg_5268" s="T33">aj</ta>
            <ta e="T35" id="Seg_5269" s="T34">selʼčʼi</ta>
            <ta e="T36" id="Seg_5270" s="T35">ämnä-tɨ</ta>
            <ta e="T38" id="Seg_5271" s="T37">qarɨ-t</ta>
            <ta e="T39" id="Seg_5272" s="T38">tü-ŋɔː-tɨt</ta>
            <ta e="T41" id="Seg_5273" s="T40">qälɨ-t</ta>
            <ta e="T42" id="Seg_5274" s="T41">takkɨ-n</ta>
            <ta e="T43" id="Seg_5275" s="T42">nʼenna</ta>
            <ta e="T44" id="Seg_5276" s="T43">tü-ŋɔː-tɨn</ta>
            <ta e="T45" id="Seg_5277" s="T44">təp-ɨ-p</ta>
            <ta e="T46" id="Seg_5278" s="T45">qət-qɨntoːqo</ta>
            <ta e="T48" id="Seg_5279" s="T47">qum-iː-tɨ</ta>
            <ta e="T49" id="Seg_5280" s="T48">ija-iː-tɨ</ta>
            <ta e="T50" id="Seg_5281" s="T49">mačʼa</ta>
            <ta e="T51" id="Seg_5282" s="T50">qən-pɔː-tɨt</ta>
            <ta e="T52" id="Seg_5283" s="T51">täpä-l-lä</ta>
            <ta e="T54" id="Seg_5284" s="T53">täpä-l-lä</ta>
            <ta e="T55" id="Seg_5285" s="T54">mačʼo</ta>
            <ta e="T56" id="Seg_5286" s="T55">qən-pɔː-t</ta>
            <ta e="T57" id="Seg_5287" s="T56">tıː</ta>
            <ta e="T58" id="Seg_5288" s="T57">qup</ta>
            <ta e="T59" id="Seg_5289" s="T58">tıː</ta>
            <ta e="T60" id="Seg_5290" s="T59">kə-t</ta>
            <ta e="T61" id="Seg_5291" s="T60">tiː</ta>
            <ta e="T62" id="Seg_5292" s="T61">kuttar</ta>
            <ta e="T63" id="Seg_5293" s="T62">ɛ-kka</ta>
            <ta e="T64" id="Seg_5294" s="T63">kə-t</ta>
            <ta e="T66" id="Seg_5295" s="T65">tına</ta>
            <ta e="T67" id="Seg_5296" s="T66">ija-p</ta>
            <ta e="T68" id="Seg_5297" s="T67">šʼolqum-ɨ-j</ta>
            <ta e="T69" id="Seg_5298" s="T68">ija-p</ta>
            <ta e="T70" id="Seg_5299" s="T69">wərɨ-mmɨ-ntɨ</ta>
            <ta e="T71" id="Seg_5300" s="T70">tına</ta>
            <ta e="T72" id="Seg_5301" s="T71">qälɨk</ta>
            <ta e="T74" id="Seg_5302" s="T73">na</ta>
            <ta e="T75" id="Seg_5303" s="T74">mütɨ-ntɨ-t</ta>
            <ta e="T77" id="Seg_5304" s="T76">nʼoma-lʼ</ta>
            <ta e="T78" id="Seg_5305" s="T77">porqɨ</ta>
            <ta e="T79" id="Seg_5306" s="T78">ira</ta>
            <ta e="T80" id="Seg_5307" s="T79">mɨta</ta>
            <ta e="T81" id="Seg_5308" s="T80">täːlɨ</ta>
            <ta e="T82" id="Seg_5309" s="T81">mɨta</ta>
            <ta e="T83" id="Seg_5310" s="T82">meː</ta>
            <ta e="T84" id="Seg_5311" s="T83">tü-ntɔː-mɨt</ta>
            <ta e="T85" id="Seg_5312" s="T84">təp-ɨ-n</ta>
            <ta e="T86" id="Seg_5313" s="T85">mɔːt-tɨ</ta>
            <ta e="T87" id="Seg_5314" s="T86">tɛː</ta>
            <ta e="T88" id="Seg_5315" s="T87">mɨta</ta>
            <ta e="T89" id="Seg_5316" s="T88">qon-ni-ŋɨlɨt</ta>
            <ta e="T90" id="Seg_5317" s="T89">toː</ta>
            <ta e="T92" id="Seg_5318" s="T91">ija-qı</ta>
            <ta e="T93" id="Seg_5319" s="T92">moqonä</ta>
            <ta e="T94" id="Seg_5320" s="T93">qəl-lä</ta>
            <ta e="T95" id="Seg_5321" s="T94">nık</ta>
            <ta e="T96" id="Seg_5322" s="T95">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T97" id="Seg_5323" s="T96">čʼilʼa-lʼ</ta>
            <ta e="T98" id="Seg_5324" s="T97">ija-qı</ta>
            <ta e="T99" id="Seg_5325" s="T98">qälɨk</ta>
            <ta e="T100" id="Seg_5326" s="T99">mərɨ-mɨ-ntɨj</ta>
            <ta e="T102" id="Seg_5327" s="T101">tam</ta>
            <ta e="T103" id="Seg_5328" s="T102">šölʼqum-ɨ-j</ta>
            <ta e="T104" id="Seg_5329" s="T103">ija-qı</ta>
            <ta e="T106" id="Seg_5330" s="T105">taŋ-alt-äš</ta>
            <ta e="T107" id="Seg_5331" s="T106">taŋ-alt-äš</ta>
            <ta e="T108" id="Seg_5332" s="T107">lʼi</ta>
            <ta e="T109" id="Seg_5333" s="T108">qaj</ta>
            <ta e="T110" id="Seg_5334" s="T109">kana-p</ta>
            <ta e="T111" id="Seg_5335" s="T110">qaj</ta>
            <ta e="T112" id="Seg_5336" s="T111">čʼü-t-alʼ-alta</ta>
            <ta e="T114" id="Seg_5337" s="T113">ta-l</ta>
            <ta e="T116" id="Seg_5338" s="T115">nɨnɨ</ta>
            <ta e="T117" id="Seg_5339" s="T116">šittäl</ta>
            <ta e="T118" id="Seg_5340" s="T117">ira</ta>
            <ta e="T119" id="Seg_5341" s="T118">kučʼčʼa</ta>
            <ta e="T120" id="Seg_5342" s="T119">qatt-ɛnta</ta>
            <ta e="T122" id="Seg_5343" s="T121">ira</ta>
            <ta e="T123" id="Seg_5344" s="T122">qapija</ta>
            <ta e="T124" id="Seg_5345" s="T123">kekkɨsa</ta>
            <ta e="T125" id="Seg_5346" s="T124">na</ta>
            <ta e="T126" id="Seg_5347" s="T125">ippɨ-nta</ta>
            <ta e="T128" id="Seg_5348" s="T127">ira</ta>
            <ta e="T129" id="Seg_5349" s="T128">nılʼčʼi-ŋ</ta>
            <ta e="T130" id="Seg_5350" s="T129">na</ta>
            <ta e="T131" id="Seg_5351" s="T130">ɛsɨ-sɨ</ta>
            <ta e="T132" id="Seg_5352" s="T131">na</ta>
            <ta e="T133" id="Seg_5353" s="T132">nʼoma-lʼ</ta>
            <ta e="T134" id="Seg_5354" s="T133">porqɨ-lʼ</ta>
            <ta e="T135" id="Seg_5355" s="T134">ira</ta>
            <ta e="T137" id="Seg_5356" s="T136">nılʼčʼi-k</ta>
            <ta e="T138" id="Seg_5357" s="T137">ɛsa</ta>
            <ta e="T139" id="Seg_5358" s="T138">amnä-ntɨ-lʼ</ta>
            <ta e="T140" id="Seg_5359" s="T139">mɨ-tkine</ta>
            <ta e="T141" id="Seg_5360" s="T140">apsɨ-p</ta>
            <ta e="T142" id="Seg_5361" s="T141">tattɨ-ŋɨt</ta>
            <ta e="T143" id="Seg_5362" s="T142">karrä</ta>
            <ta e="T145" id="Seg_5363" s="T144">ür</ta>
            <ta e="T146" id="Seg_5364" s="T145">apsɨ-p</ta>
            <ta e="T147" id="Seg_5365" s="T146">tattɨ-ŋɨt</ta>
            <ta e="T148" id="Seg_5366" s="T147">*kušša-lʼ</ta>
            <ta e="T149" id="Seg_5367" s="T148">tɛlɨlʼ</ta>
            <ta e="T150" id="Seg_5368" s="T149">apsɨ-p</ta>
            <ta e="T151" id="Seg_5369" s="T150">ku-n</ta>
            <ta e="T152" id="Seg_5370" s="T151">montɨ</ta>
            <ta e="T153" id="Seg_5371" s="T152">qaj</ta>
            <ta e="T154" id="Seg_5372" s="T153">tattɨ-ŋɨt</ta>
            <ta e="T156" id="Seg_5373" s="T155">qälɨ-t</ta>
            <ta e="T157" id="Seg_5374" s="T156">täːlɨ</ta>
            <ta e="T158" id="Seg_5375" s="T157">tü-ntɔː-tɨt</ta>
            <ta e="T159" id="Seg_5376" s="T158">am-ɨ-r-tɛntɔː-tɨt</ta>
            <ta e="T161" id="Seg_5377" s="T160">ira</ta>
            <ta e="T162" id="Seg_5378" s="T161">nık</ta>
            <ta e="T163" id="Seg_5379" s="T162">kətɨ-ŋɨ-t</ta>
            <ta e="T165" id="Seg_5380" s="T164">apsɨ-p</ta>
            <ta e="T166" id="Seg_5381" s="T165">tattɔː-tɨt</ta>
            <ta e="T168" id="Seg_5382" s="T167">tıntena</ta>
            <ta e="T169" id="Seg_5383" s="T168">tap</ta>
            <ta e="T170" id="Seg_5384" s="T169">nʼuːtɨ-sa</ta>
            <ta e="T171" id="Seg_5385" s="T170">tap</ta>
            <ta e="T172" id="Seg_5386" s="T171">mɔːt</ta>
            <ta e="T173" id="Seg_5387" s="T172">šünʼčʼɨ</ta>
            <ta e="T174" id="Seg_5388" s="T173">šentɨ</ta>
            <ta e="T175" id="Seg_5389" s="T174">nʼuːtɨ-sa</ta>
            <ta e="T176" id="Seg_5390" s="T175">tɔːqqɔː-tɨt</ta>
            <ta e="T177" id="Seg_5391" s="T176">tına</ta>
            <ta e="T179" id="Seg_5392" s="T178">ira</ta>
            <ta e="T180" id="Seg_5393" s="T179">qäli-m</ta>
            <ta e="T181" id="Seg_5394" s="T180">apsɨ-tɨ-q-olam-nɨ-t</ta>
            <ta e="T183" id="Seg_5395" s="T182">qəə</ta>
            <ta e="T184" id="Seg_5396" s="T183">top-tɨlʼ</ta>
            <ta e="T185" id="Seg_5397" s="T184">čʼeːlɨ</ta>
            <ta e="T186" id="Seg_5398" s="T185">qälɨ-n</ta>
            <ta e="T187" id="Seg_5399" s="T186">na</ta>
            <ta e="T188" id="Seg_5400" s="T187">jap</ta>
            <ta e="T189" id="Seg_5401" s="T188">nɔːs-sar-ɨ-lʼ</ta>
            <ta e="T190" id="Seg_5402" s="T189">qum-ɨ-lʼ</ta>
            <ta e="T191" id="Seg_5403" s="T190">müːtɨ</ta>
            <ta e="T193" id="Seg_5404" s="T192">tomoron</ta>
            <ta e="T194" id="Seg_5405" s="T193">qapı</ta>
            <ta e="T195" id="Seg_5406" s="T194">šünʼčʼi-pɨlʼ</ta>
            <ta e="T196" id="Seg_5407" s="T195">mɔːt-qɨt</ta>
            <ta e="T197" id="Seg_5408" s="T196">nʼoma-lʼ</ta>
            <ta e="T198" id="Seg_5409" s="T197">porqɨ</ta>
            <ta e="T199" id="Seg_5410" s="T198">ira</ta>
            <ta e="T200" id="Seg_5411" s="T199">ontɨ</ta>
            <ta e="T201" id="Seg_5412" s="T200">qala</ta>
            <ta e="T202" id="Seg_5413" s="T201">ämnä-iː-ntɨ-sa</ta>
            <ta e="T204" id="Seg_5414" s="T203">selʼčʼi</ta>
            <ta e="T205" id="Seg_5415" s="T204">nʼuːtɨ-sa</ta>
            <ta e="T206" id="Seg_5416" s="T205">tɔːqqɨ-raltɨ-ŋɨ-t</ta>
            <ta e="T208" id="Seg_5417" s="T207">ira</ta>
            <ta e="T209" id="Seg_5418" s="T208">nılʼčʼi-k</ta>
            <ta e="T210" id="Seg_5419" s="T209">ɛsa</ta>
            <ta e="T212" id="Seg_5420" s="T211">mɔːt</ta>
            <ta e="T213" id="Seg_5421" s="T212">jap</ta>
            <ta e="T214" id="Seg_5422" s="T213">tul-tɔː-t</ta>
            <ta e="T215" id="Seg_5423" s="T214">qapija</ta>
            <ta e="T216" id="Seg_5424" s="T215">tına</ta>
            <ta e="T217" id="Seg_5425" s="T216">na</ta>
            <ta e="T218" id="Seg_5426" s="T217">qälɨ-t</ta>
            <ta e="T220" id="Seg_5427" s="T219">am-ɨ-r-q-olam-nɔː-t</ta>
            <ta e="T222" id="Seg_5428" s="T221">apsɨ</ta>
            <ta e="T223" id="Seg_5429" s="T222">qo-ntɔː-t</ta>
            <ta e="T225" id="Seg_5430" s="T224">ämnä-iː-tɨ</ta>
            <ta e="T226" id="Seg_5431" s="T225">čʼoj</ta>
            <ta e="T227" id="Seg_5432" s="T226">tap</ta>
            <ta e="T228" id="Seg_5433" s="T227">mušɨ-r-ɔːl-nɔː-t</ta>
            <ta e="T229" id="Seg_5434" s="T228">karrʼa-t</ta>
            <ta e="T230" id="Seg_5435" s="T229">ku-n</ta>
            <ta e="T231" id="Seg_5436" s="T230">mɔːntɨ-lʼ</ta>
            <ta e="T232" id="Seg_5437" s="T231">apsɨ</ta>
            <ta e="T233" id="Seg_5438" s="T232">ür</ta>
            <ta e="T234" id="Seg_5439" s="T233">ür</ta>
            <ta e="T235" id="Seg_5440" s="T234">ku-n</ta>
            <ta e="T236" id="Seg_5441" s="T235">mɔːntɨ-lʼ</ta>
            <ta e="T237" id="Seg_5442" s="T236">qaj</ta>
            <ta e="T238" id="Seg_5443" s="T237">tına</ta>
            <ta e="T239" id="Seg_5444" s="T238">təːkɨ-mpɨlʼ</ta>
            <ta e="T240" id="Seg_5445" s="T239">apsɨ</ta>
            <ta e="T242" id="Seg_5446" s="T241">ira</ta>
            <ta e="T243" id="Seg_5447" s="T242">nık</ta>
            <ta e="T244" id="Seg_5448" s="T243">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T245" id="Seg_5449" s="T244">imaqota-ntɨ</ta>
            <ta e="T246" id="Seg_5450" s="T245">nʼoma-lʼ</ta>
            <ta e="T247" id="Seg_5451" s="T246">porqɨ</ta>
            <ta e="T248" id="Seg_5452" s="T247">ira</ta>
            <ta e="T249" id="Seg_5453" s="T248">imaqota-tɨ</ta>
            <ta e="T250" id="Seg_5454" s="T249">ɛj</ta>
            <ta e="T251" id="Seg_5455" s="T250">ɛː-ppa</ta>
            <ta e="T253" id="Seg_5456" s="T252">ta-l</ta>
            <ta e="T255" id="Seg_5457" s="T254">ämnä-iː-m-tɨ</ta>
            <ta e="T256" id="Seg_5458" s="T255">nık</ta>
            <ta e="T257" id="Seg_5459" s="T256">kur-altɨ-ŋɨ-t</ta>
            <ta e="T258" id="Seg_5460" s="T257">tɛː</ta>
            <ta e="T259" id="Seg_5461" s="T258">ponä</ta>
            <ta e="T260" id="Seg_5462" s="T259">tantɨ-ŋɨl</ta>
            <ta e="T262" id="Seg_5463" s="T261">ija-t</ta>
            <ta e="T263" id="Seg_5464" s="T262">kuti</ta>
            <ta e="T264" id="Seg_5465" s="T263">ponä</ta>
            <ta e="T265" id="Seg_5466" s="T264">tat-t-ätɨ</ta>
            <ta e="T267" id="Seg_5467" s="T266">ira</ta>
            <ta e="T268" id="Seg_5468" s="T267">kuntɨ</ta>
            <ta e="T269" id="Seg_5469" s="T268">wərka</ta>
            <ta e="T270" id="Seg_5470" s="T269">qum-iː-m-tɨ</ta>
            <ta e="T271" id="Seg_5471" s="T270">apsɨ-tɨ-mpɨ-la</ta>
            <ta e="T273" id="Seg_5472" s="T272">ukkɨr</ta>
            <ta e="T274" id="Seg_5473" s="T273">tät</ta>
            <ta e="T275" id="Seg_5474" s="T274">čʼonto-t</ta>
            <ta e="T276" id="Seg_5475" s="T275">tü-mpɨj</ta>
            <ta e="T277" id="Seg_5476" s="T276">karrʼa-t</ta>
            <ta e="T278" id="Seg_5477" s="T277">qum-ɨ-n</ta>
            <ta e="T279" id="Seg_5478" s="T278">am-ɨ-r-nɔː-tɨt</ta>
            <ta e="T281" id="Seg_5479" s="T280">karrʼa</ta>
            <ta e="T282" id="Seg_5480" s="T281">na</ta>
            <ta e="T283" id="Seg_5481" s="T282">šoːqɨr-ɨ-j</ta>
            <ta e="T284" id="Seg_5482" s="T283">tü-ntɨ</ta>
            <ta e="T285" id="Seg_5483" s="T284">nannɛrɨk</ta>
            <ta e="T286" id="Seg_5484" s="T285">poː-sä</ta>
            <ta e="T287" id="Seg_5485" s="T286">sälɨ-ŋɨ-tɨ</ta>
            <ta e="T288" id="Seg_5486" s="T287">čʼɔːtɨ-ŋɨ-tɨ</ta>
            <ta e="T289" id="Seg_5487" s="T288">ämnä-iː-m-tɨ</ta>
            <ta e="T290" id="Seg_5488" s="T289">ašša</ta>
            <ta e="T291" id="Seg_5489" s="T290">qaː</ta>
            <ta e="T292" id="Seg_5490" s="T291">pona</ta>
            <ta e="T293" id="Seg_5491" s="T292">üːtɨ-mpa-tɨ</ta>
            <ta e="T294" id="Seg_5492" s="T293">poː-qɨn</ta>
            <ta e="T295" id="Seg_5493" s="T294">tü</ta>
            <ta e="T296" id="Seg_5494" s="T295">čʼɔːt-al-tɛntɔː-t</ta>
            <ta e="T298" id="Seg_5495" s="T297">tü</ta>
            <ta e="T299" id="Seg_5496" s="T298">čʼɔːt-al-pɨ-ntɔː-tɨt</ta>
            <ta e="T300" id="Seg_5497" s="T299">tına</ta>
            <ta e="T301" id="Seg_5498" s="T300">tö-lʼ</ta>
            <ta e="T302" id="Seg_5499" s="T301">purɨš</ta>
            <ta e="T303" id="Seg_5500" s="T302">olɨ-lʼa</ta>
            <ta e="T304" id="Seg_5501" s="T303">nılʼčʼi-k</ta>
            <ta e="T305" id="Seg_5502" s="T304">təːkɨ-rɨ-mpɨlʼ</ta>
            <ta e="T306" id="Seg_5503" s="T305">tomarot</ta>
            <ta e="T307" id="Seg_5504" s="T306">mɔːt</ta>
            <ta e="T308" id="Seg_5505" s="T307">tü-m-tɨ</ta>
            <ta e="T310" id="Seg_5506" s="T309">tɛː</ta>
            <ta e="T311" id="Seg_5507" s="T310">najenta</ta>
            <ta e="T312" id="Seg_5508" s="T311">man</ta>
            <ta e="T313" id="Seg_5509" s="T312">na</ta>
            <ta e="T314" id="Seg_5510" s="T313">tann-ɛnta-k</ta>
            <ta e="T316" id="Seg_5511" s="T315">qum-ɨ-n</ta>
            <ta e="T317" id="Seg_5512" s="T316">am-ɨ-r-sɔː-tɨt</ta>
            <ta e="T319" id="Seg_5513" s="T318">am-ɨ-r-nɔː-tɨt</ta>
            <ta e="T321" id="Seg_5514" s="T320">qälɨ-t</ta>
            <ta e="T322" id="Seg_5515" s="T321">nık</ta>
            <ta e="T323" id="Seg_5516" s="T322">kətɔː-t</ta>
            <ta e="T324" id="Seg_5517" s="T323">ilʼčʼa</ta>
            <ta e="T325" id="Seg_5518" s="T324">šoːqɨr-lɨ</ta>
            <ta e="T326" id="Seg_5519" s="T325">naššak</ta>
            <ta e="T327" id="Seg_5520" s="T326">qajqo</ta>
            <ta e="T328" id="Seg_5521" s="T327">poː-sä</ta>
            <ta e="T329" id="Seg_5522" s="T328">säla-l</ta>
            <ta e="T331" id="Seg_5523" s="T330">ašša</ta>
            <ta e="T332" id="Seg_5524" s="T331">šoːqɨr</ta>
            <ta e="T333" id="Seg_5525" s="T332">tü</ta>
            <ta e="T334" id="Seg_5526" s="T333">am-tɨ-ŋɨ-t</ta>
            <ta e="T336" id="Seg_5527" s="T335">ašša</ta>
            <ta e="T337" id="Seg_5528" s="T336">mɨta</ta>
            <ta e="T338" id="Seg_5529" s="T337">man</ta>
            <ta e="T339" id="Seg_5530" s="T338">mɨta</ta>
            <ta e="T340" id="Seg_5531" s="T339">mɨ</ta>
            <ta e="T342" id="Seg_5532" s="T341">tɛm</ta>
            <ta e="T343" id="Seg_5533" s="T342">ašša</ta>
            <ta e="T344" id="Seg_5534" s="T343">am-tɨ-tɨ</ta>
            <ta e="T345" id="Seg_5535" s="T344">tɛm</ta>
            <ta e="T346" id="Seg_5536" s="T345">ašša</ta>
            <ta e="T347" id="Seg_5537" s="T346">am-tɨ-tɨ</ta>
            <ta e="T348" id="Seg_5538" s="T347">nı-lʼ</ta>
            <ta e="T349" id="Seg_5539" s="T348">mərka-ta</ta>
            <ta e="T350" id="Seg_5540" s="T349">nı-lʼ</ta>
            <ta e="T351" id="Seg_5541" s="T350">nʼoma-lʼ</ta>
            <ta e="T352" id="Seg_5542" s="T351">porqɨ</ta>
            <ta e="T353" id="Seg_5543" s="T352">raša</ta>
            <ta e="T354" id="Seg_5544" s="T353">lıːpɨ</ta>
            <ta e="T355" id="Seg_5545" s="T354">tına</ta>
            <ta e="T357" id="Seg_5546" s="T356">meːntalʼ</ta>
            <ta e="T358" id="Seg_5547" s="T357">namɨ-p</ta>
            <ta e="T359" id="Seg_5548" s="T358">šeːr-pɨ-ntɨ-t</ta>
            <ta e="T361" id="Seg_5549" s="T360">šoŋol-tɨ</ta>
            <ta e="T362" id="Seg_5550" s="T361">qanɨq-qɨt</ta>
            <ta e="T363" id="Seg_5551" s="T362">mərqa</ta>
            <ta e="T364" id="Seg_5552" s="T363">mɔːta-n-tɨ</ta>
            <ta e="T365" id="Seg_5553" s="T364">qanɨq-qɨt</ta>
            <ta e="T367" id="Seg_5554" s="T366">ukkɨr</ta>
            <ta e="T368" id="Seg_5555" s="T367">tot</ta>
            <ta e="T369" id="Seg_5556" s="T368">čʼonto-t</ta>
            <ta e="T370" id="Seg_5557" s="T369">poː-qɨnɨ</ta>
            <ta e="T371" id="Seg_5558" s="T370">mɔːt-tɨ</ta>
            <ta e="T372" id="Seg_5559" s="T371">tına</ta>
            <ta e="T373" id="Seg_5560" s="T372">tü-lʼ</ta>
            <ta e="T374" id="Seg_5561" s="T373">purɨš-laka</ta>
            <ta e="T375" id="Seg_5562" s="T374">alʼčʼi-ntɨ</ta>
            <ta e="T377" id="Seg_5563" s="T376">na</ta>
            <ta e="T378" id="Seg_5564" s="T377">paktɨ-mmɨ-nta</ta>
            <ta e="T379" id="Seg_5565" s="T378">tına</ta>
            <ta e="T380" id="Seg_5566" s="T379">mɔːta-ntɨ</ta>
            <ta e="T381" id="Seg_5567" s="T380">tına</ta>
            <ta e="T382" id="Seg_5568" s="T381">nılʼčʼi</ta>
            <ta e="T383" id="Seg_5569" s="T382">ämnä-iː-tɨ</ta>
            <ta e="T385" id="Seg_5570" s="T384">qälɨ-n</ta>
            <ta e="T386" id="Seg_5571" s="T385">čʼam</ta>
            <ta e="T387" id="Seg_5572" s="T386">orqɨl-pɔː-t</ta>
            <ta e="T388" id="Seg_5573" s="T387">nı-lʼ</ta>
            <ta e="T389" id="Seg_5574" s="T388">ira-p</ta>
            <ta e="T390" id="Seg_5575" s="T389">ola</ta>
            <ta e="T391" id="Seg_5576" s="T390">čʼaŋaj</ta>
            <ta e="T392" id="Seg_5577" s="T391">nʼoma-lʼ</ta>
            <ta e="T393" id="Seg_5578" s="T392">porqɨ-n-tɨ</ta>
            <ta e="T394" id="Seg_5579" s="T393">lıːp</ta>
            <ta e="T395" id="Seg_5580" s="T394">nɨt-qɨl-ɛː-la</ta>
            <ta e="T396" id="Seg_5581" s="T395">orqɨl-tɔː-t</ta>
            <ta e="T397" id="Seg_5582" s="T396">mɔːt-qɨn</ta>
            <ta e="T399" id="Seg_5583" s="T398">qapi</ta>
            <ta e="T400" id="Seg_5584" s="T399">na</ta>
            <ta e="T401" id="Seg_5585" s="T400">tü-sa</ta>
            <ta e="T402" id="Seg_5586" s="T401">nɨnɨ</ta>
            <ta e="T403" id="Seg_5587" s="T402">näːtɨ</ta>
            <ta e="T404" id="Seg_5588" s="T403">okoška-n</ta>
            <ta e="T405" id="Seg_5589" s="T404">ɔː-š</ta>
            <ta e="T406" id="Seg_5590" s="T405">šüː-mɨt</ta>
            <ta e="T407" id="Seg_5591" s="T406">mɔːt-tɨ</ta>
            <ta e="T408" id="Seg_5592" s="T407">tılʼčʼɨ</ta>
            <ta e="T409" id="Seg_5593" s="T408">šittɨ-lʼ</ta>
            <ta e="T410" id="Seg_5594" s="T409">pɛla-lʼ</ta>
            <ta e="T411" id="Seg_5595" s="T410">okoška</ta>
            <ta e="T412" id="Seg_5596" s="T411">ɛː-ppa</ta>
            <ta e="T414" id="Seg_5597" s="T413">qaj</ta>
            <ta e="T415" id="Seg_5598" s="T414">wərqɨ</ta>
            <ta e="T416" id="Seg_5599" s="T415">mɔːt-tɨ</ta>
            <ta e="T417" id="Seg_5600" s="T416">ɛː-ppa</ta>
            <ta e="T419" id="Seg_5601" s="T418">mɔːt-tɨ</ta>
            <ta e="T420" id="Seg_5602" s="T419">najentɨ</ta>
            <ta e="T421" id="Seg_5603" s="T420">čʼu-sa</ta>
            <ta e="T422" id="Seg_5604" s="T421">taq-pɨ-mpa</ta>
            <ta e="T423" id="Seg_5605" s="T422">tɨna</ta>
            <ta e="T424" id="Seg_5606" s="T423">loːq-ɨ-ra</ta>
            <ta e="T425" id="Seg_5607" s="T424">po-lʼ</ta>
            <ta e="T426" id="Seg_5608" s="T425">mɔːt</ta>
            <ta e="T427" id="Seg_5609" s="T426">pičʼi-t</ta>
            <ta e="T428" id="Seg_5610" s="T427">paqɨ-t</ta>
            <ta e="T429" id="Seg_5611" s="T428">piːrɨ-lʼ</ta>
            <ta e="T430" id="Seg_5612" s="T429">taq-pa</ta>
            <ta e="T432" id="Seg_5613" s="T431">mompa</ta>
            <ta e="T433" id="Seg_5614" s="T432">mɔːt-qɨt</ta>
            <ta e="T434" id="Seg_5615" s="T433">tına</ta>
            <ta e="T435" id="Seg_5616" s="T434">na</ta>
            <ta e="T436" id="Seg_5617" s="T435">tü-m</ta>
            <ta e="T437" id="Seg_5618" s="T436">na</ta>
            <ta e="T438" id="Seg_5619" s="T437">qəː-l-pɨ-ntɔː-tɨt</ta>
            <ta e="T439" id="Seg_5620" s="T438">na</ta>
            <ta e="T440" id="Seg_5621" s="T439">qättɨ-mmɨ-ntɨ-tɨ</ta>
            <ta e="T441" id="Seg_5622" s="T440">qapi</ta>
            <ta e="T442" id="Seg_5623" s="T441">ton</ta>
            <ta e="T923" id="Seg_5624" s="T442">ira-p</ta>
            <ta e="T444" id="Seg_5625" s="T443">qapi</ta>
            <ta e="T445" id="Seg_5626" s="T444">tam</ta>
            <ta e="T446" id="Seg_5627" s="T445">am-n</ta>
            <ta e="T448" id="Seg_5628" s="T447">ta-l</ta>
            <ta e="T450" id="Seg_5629" s="T449">porak-sa</ta>
            <ta e="T451" id="Seg_5630" s="T450">qaj</ta>
            <ta e="T452" id="Seg_5631" s="T451">təm</ta>
            <ta e="T453" id="Seg_5632" s="T452">mɨta</ta>
            <ta e="T454" id="Seg_5633" s="T453">qam-nɨ-mpa-t</ta>
            <ta e="T455" id="Seg_5634" s="T454">na</ta>
            <ta e="T456" id="Seg_5635" s="T455">na</ta>
            <ta e="T457" id="Seg_5636" s="T456">nʼuːtɨ-t</ta>
            <ta e="T458" id="Seg_5637" s="T457">ɨl-qɨ-lʼ</ta>
            <ta e="T459" id="Seg_5638" s="T458">pɛla-t</ta>
            <ta e="T460" id="Seg_5639" s="T459">tilʼčʼi</ta>
            <ta e="T461" id="Seg_5640" s="T460">šentɨ</ta>
            <ta e="T462" id="Seg_5641" s="T461">nʼuːtɨ</ta>
            <ta e="T463" id="Seg_5642" s="T462">ɨl-qɨ-lʼ</ta>
            <ta e="T464" id="Seg_5643" s="T463">pɛla-t</ta>
            <ta e="T466" id="Seg_5644" s="T465">na</ta>
            <ta e="T467" id="Seg_5645" s="T466">tü</ta>
            <ta e="T468" id="Seg_5646" s="T467">na</ta>
            <ta e="T469" id="Seg_5647" s="T468">qättɨ-mmɨ-ntɨ-tɨ</ta>
            <ta e="T470" id="Seg_5648" s="T469">tına</ta>
            <ta e="T471" id="Seg_5649" s="T470">nɔːs-sar-a-lʼ</ta>
            <ta e="T472" id="Seg_5650" s="T471">qum-ɨ-lʼ</ta>
            <ta e="T473" id="Seg_5651" s="T472">mütɨ-p</ta>
            <ta e="T474" id="Seg_5652" s="T473">muntɨk</ta>
            <ta e="T475" id="Seg_5653" s="T474">tü</ta>
            <ta e="T476" id="Seg_5654" s="T475">am-pa-t</ta>
            <ta e="T478" id="Seg_5655" s="T477">qə-n</ta>
            <ta e="T479" id="Seg_5656" s="T478">ɔːlʼčʼɔː-t</ta>
            <ta e="T480" id="Seg_5657" s="T479">nʼenna-t</ta>
            <ta e="T482" id="Seg_5658" s="T481">nʼennä</ta>
            <ta e="T483" id="Seg_5659" s="T482">pon</ta>
            <ta e="T484" id="Seg_5660" s="T483">ira</ta>
            <ta e="T485" id="Seg_5661" s="T484">pona</ta>
            <ta e="T486" id="Seg_5662" s="T485">pakta</ta>
            <ta e="T487" id="Seg_5663" s="T486">mɔːta-n</ta>
            <ta e="T488" id="Seg_5664" s="T487">ɔː-t</ta>
            <ta e="T489" id="Seg_5665" s="T488">pičʼi-lʼ</ta>
            <ta e="T490" id="Seg_5666" s="T489">ut</ta>
            <ta e="T491" id="Seg_5667" s="T490">mərka</ta>
            <ta e="T493" id="Seg_5668" s="T492">isɨ</ta>
            <ta e="T494" id="Seg_5669" s="T493">čʼap</ta>
            <ta e="T495" id="Seg_5670" s="T494">pona</ta>
            <ta e="T496" id="Seg_5671" s="T495">qontɨ-š-ɛlʼčʼɔː-t</ta>
            <ta e="T497" id="Seg_5672" s="T496">aj</ta>
            <ta e="T498" id="Seg_5673" s="T497">čʼari</ta>
            <ta e="T499" id="Seg_5674" s="T498">qättɨ-ŋɨ-t</ta>
            <ta e="T501" id="Seg_5675" s="T500">ämnä-iː-tɨ</ta>
            <ta e="T502" id="Seg_5676" s="T501">na</ta>
            <ta e="T503" id="Seg_5677" s="T502">tü</ta>
            <ta e="T504" id="Seg_5678" s="T503">na</ta>
            <ta e="T505" id="Seg_5679" s="T504">qəː-mmɨ-ntɨ-t</ta>
            <ta e="T506" id="Seg_5680" s="T505">šoːqɨr</ta>
            <ta e="T507" id="Seg_5681" s="T506">pɔːr</ta>
            <ta e="T508" id="Seg_5682" s="T507">šüː-mɨt</ta>
            <ta e="T509" id="Seg_5683" s="T508">qəː-ŋɔː-t</ta>
            <ta e="T510" id="Seg_5684" s="T509">nɨn</ta>
            <ta e="T511" id="Seg_5685" s="T510">ɛj</ta>
            <ta e="T512" id="Seg_5686" s="T511">šit</ta>
            <ta e="T513" id="Seg_5687" s="T512">okoško-n</ta>
            <ta e="T514" id="Seg_5688" s="T513">ɔː-š</ta>
            <ta e="T515" id="Seg_5689" s="T514">šüː-mɨt</ta>
            <ta e="T516" id="Seg_5690" s="T515">qəː-ŋɔː-tɨn</ta>
            <ta e="T518" id="Seg_5691" s="T517">muntɨk</ta>
            <ta e="T519" id="Seg_5692" s="T518">tü-s</ta>
            <ta e="T520" id="Seg_5693" s="T519">čʼɔːtt-ɛıː-mpa-tɨ</ta>
            <ta e="T521" id="Seg_5694" s="T520">na</ta>
            <ta e="T522" id="Seg_5695" s="T521">qälɨ-lʼ</ta>
            <ta e="T523" id="Seg_5696" s="T522">mütɨ-p</ta>
            <ta e="T525" id="Seg_5697" s="T524">na</ta>
            <ta e="T526" id="Seg_5698" s="T525">mütɨ</ta>
            <ta e="T527" id="Seg_5699" s="T526">qɔːtɨ</ta>
            <ta e="T528" id="Seg_5700" s="T527">šittä</ta>
            <ta e="T529" id="Seg_5701" s="T528">ɛː-ppɨ-ntɔː-t</ta>
            <ta e="T533" id="Seg_5702" s="T532">na</ta>
            <ta e="T534" id="Seg_5703" s="T533">kət-ät</ta>
            <ta e="T536" id="Seg_5704" s="T535">pɛla-kɨtɨ-j</ta>
            <ta e="T537" id="Seg_5705" s="T536">ira</ta>
            <ta e="T538" id="Seg_5706" s="T537">kučʼčʼa</ta>
            <ta e="T539" id="Seg_5707" s="T538">qatt-ɛnta</ta>
            <ta e="T541" id="Seg_5708" s="T540">ira</ta>
            <ta e="T542" id="Seg_5709" s="T541">nɨnɨ</ta>
            <ta e="T543" id="Seg_5710" s="T542">šʼittalʼ</ta>
            <ta e="T544" id="Seg_5711" s="T543">mɨ-ŋa</ta>
            <ta e="T545" id="Seg_5712" s="T544">nʼoma-lʼ</ta>
            <ta e="T546" id="Seg_5713" s="T545">porqɨ</ta>
            <ta e="T547" id="Seg_5714" s="T546">ira</ta>
            <ta e="T548" id="Seg_5715" s="T547">karrä</ta>
            <ta e="T549" id="Seg_5716" s="T548">qälɨ-iː-ntɨ</ta>
            <ta e="T550" id="Seg_5717" s="T549">qaqlo-ntɨ</ta>
            <ta e="T551" id="Seg_5718" s="T550">qə-ŋa</ta>
            <ta e="T553" id="Seg_5719" s="T552">aj</ta>
            <ta e="T554" id="Seg_5720" s="T553">šittä-mɨ</ta>
            <ta e="T556" id="Seg_5721" s="T555">takkɨ</ta>
            <ta e="T558" id="Seg_5722" s="T557">qälɨ-j</ta>
            <ta e="T559" id="Seg_5723" s="T558">mɔːt-tɨ</ta>
            <ta e="T927" id="Seg_5724" s="T559">qən-qo</ta>
            <ta e="T561" id="Seg_5725" s="T560">ılla</ta>
            <ta e="T562" id="Seg_5726" s="T561">omt-äš</ta>
            <ta e="T564" id="Seg_5727" s="T563">na</ta>
            <ta e="T565" id="Seg_5728" s="T564">qət-qɨl-pɨtɨj</ta>
            <ta e="T566" id="Seg_5729" s="T565">qum-ɨ-iː-qɨn-tɨ</ta>
            <ta e="T567" id="Seg_5730" s="T566">qən-qɨntoːqo</ta>
            <ta e="T569" id="Seg_5731" s="T568">karrä</ta>
            <ta e="T570" id="Seg_5732" s="T569">qən-na</ta>
            <ta e="T571" id="Seg_5733" s="T570">ooo</ta>
            <ta e="T572" id="Seg_5734" s="T571">karrä-n</ta>
            <ta e="T573" id="Seg_5735" s="T572">konna</ta>
            <ta e="T574" id="Seg_5736" s="T573">moːčʼ-alʼ</ta>
            <ta e="T575" id="Seg_5737" s="T574">qälɨ</ta>
            <ta e="T576" id="Seg_5738" s="T575">ira</ta>
            <ta e="T577" id="Seg_5739" s="T576">qontɨ-š-ei-ntɨ</ta>
            <ta e="T581" id="Seg_5740" s="T580">qaj</ta>
            <ta e="T582" id="Seg_5741" s="T581">na</ta>
            <ta e="T583" id="Seg_5742" s="T582">laqɨ-mɔːt-ta-k</ta>
            <ta e="T585" id="Seg_5743" s="T584">qaj</ta>
            <ta e="T586" id="Seg_5744" s="T585">nʼoma-lʼ</ta>
            <ta e="T587" id="Seg_5745" s="T586">porqɨ</ta>
            <ta e="T588" id="Seg_5746" s="T587">ira-t</ta>
            <ta e="T591" id="Seg_5747" s="T590">nʼoma-lʼ</ta>
            <ta e="T592" id="Seg_5748" s="T591">porqɨ</ta>
            <ta e="T593" id="Seg_5749" s="T592">ira</ta>
            <ta e="T594" id="Seg_5750" s="T593">čʼarrä</ta>
            <ta e="T595" id="Seg_5751" s="T594">qättɨ-ŋɨ-tɨ</ta>
            <ta e="T596" id="Seg_5752" s="T595">päqɨ-lʼ</ta>
            <ta e="T597" id="Seg_5753" s="T596">šuk-tɨ</ta>
            <ta e="T598" id="Seg_5754" s="T597">qottä</ta>
            <ta e="T599" id="Seg_5755" s="T598">säp-pa-ja</ta>
            <ta e="T601" id="Seg_5756" s="T600">nɨnɨ</ta>
            <ta e="T602" id="Seg_5757" s="T601">karrä</ta>
            <ta e="T603" id="Seg_5758" s="T602">kur-ɔːl-na</ta>
            <ta e="T605" id="Seg_5759" s="T604">karrä</ta>
            <ta e="T606" id="Seg_5760" s="T605">kur-ɔːl-na</ta>
            <ta e="T607" id="Seg_5761" s="T606">nɨnɨ</ta>
            <ta e="T608" id="Seg_5762" s="T607">nɔːkɨr</ta>
            <ta e="T609" id="Seg_5763" s="T608">ämnä-sɨ-t</ta>
            <ta e="T610" id="Seg_5764" s="T609">takkɨ</ta>
            <ta e="T611" id="Seg_5765" s="T610">qən-nɔː-t</ta>
            <ta e="T613" id="Seg_5766" s="T612">takkɨ</ta>
            <ta e="T614" id="Seg_5767" s="T613">qälɨ-n</ta>
            <ta e="T615" id="Seg_5768" s="T614">mɔːt</ta>
            <ta e="T616" id="Seg_5769" s="T615">takkɨ-n</ta>
            <ta e="T617" id="Seg_5770" s="T616">ɛː-ppa</ta>
            <ta e="T618" id="Seg_5771" s="T617">točʼčʼ-oto-t</ta>
            <ta e="T620" id="Seg_5772" s="T619">karrʼa</ta>
            <ta e="T621" id="Seg_5773" s="T620">qən-nɔː-t</ta>
            <ta e="T622" id="Seg_5774" s="T621">nɨmtɨ</ta>
            <ta e="T623" id="Seg_5775" s="T622">näčʼčʼa</ta>
            <ta e="T624" id="Seg_5776" s="T623">laq-alt-ɛlʼčʼɔː-t</ta>
            <ta e="T625" id="Seg_5777" s="T624">qälɨ-t-ɨ-t</ta>
            <ta e="T626" id="Seg_5778" s="T625">qaqlɨ-sa</ta>
            <ta e="T627" id="Seg_5779" s="T626">na</ta>
            <ta e="T628" id="Seg_5780" s="T627">qum-iː-t</ta>
            <ta e="T629" id="Seg_5781" s="T628">qaqlɨ-sa</ta>
            <ta e="T630" id="Seg_5782" s="T629">na</ta>
            <ta e="T631" id="Seg_5783" s="T630">nɔːkɨr</ta>
            <ta e="T632" id="Seg_5784" s="T631">ilʼčʼa-sɨ-t</ta>
            <ta e="T634" id="Seg_5785" s="T633">takkɨ</ta>
            <ta e="T635" id="Seg_5786" s="T634">laq-alt-ɛː-la</ta>
            <ta e="T636" id="Seg_5787" s="T635">qən-nɔː-t</ta>
            <ta e="T638" id="Seg_5788" s="T637">tına</ta>
            <ta e="T639" id="Seg_5789" s="T638">takkɨ-n</ta>
            <ta e="T640" id="Seg_5790" s="T639">na</ta>
            <ta e="T641" id="Seg_5791" s="T640">qälɨ-lʼ</ta>
            <ta e="T643" id="Seg_5792" s="T642">ima-tɨ-t-ɨ-p</ta>
            <ta e="T644" id="Seg_5793" s="T643">na</ta>
            <ta e="T645" id="Seg_5794" s="T644">šu-qɨl-pɨ-ntɨ</ta>
            <ta e="T647" id="Seg_5795" s="T646">tına</ta>
            <ta e="T648" id="Seg_5796" s="T647">šʼolqum-ɨ-j</ta>
            <ta e="T649" id="Seg_5797" s="T648">ija-qı</ta>
            <ta e="T650" id="Seg_5798" s="T649">näčʼčʼatɨ-qɨt</ta>
            <ta e="T651" id="Seg_5799" s="T650">na-mä</ta>
            <ta e="T652" id="Seg_5800" s="T651">qalɨ-mpɔː-qı</ta>
            <ta e="T653" id="Seg_5801" s="T652">tomoro-qɨt</ta>
            <ta e="T654" id="Seg_5802" s="T653">na</ta>
            <ta e="T655" id="Seg_5803" s="T654">ilʼčʼa-ntıː-n</ta>
            <ta e="T656" id="Seg_5804" s="T655">tü-mpɨ-qı</ta>
            <ta e="T658" id="Seg_5805" s="T657">konnä-tɨ</ta>
            <ta e="T659" id="Seg_5806" s="T658">tına</ta>
            <ta e="T660" id="Seg_5807" s="T659">mɨ-ntɨ</ta>
            <ta e="T661" id="Seg_5808" s="T660">šittɨ</ta>
            <ta e="T662" id="Seg_5809" s="T661">qälɨk</ta>
            <ta e="T663" id="Seg_5810" s="T662">moqonä</ta>
            <ta e="T664" id="Seg_5811" s="T663">üːtɨ-mmɨ-ntɔː-t</ta>
            <ta e="T666" id="Seg_5812" s="T665">mɔːt-tɨ</ta>
            <ta e="T667" id="Seg_5813" s="T666">tɛː-pɨktɨ</ta>
            <ta e="T669" id="Seg_5814" s="T668">qaj-tɨ</ta>
            <ta e="T670" id="Seg_5815" s="T669">naɛntɨ</ta>
            <ta e="T671" id="Seg_5816" s="T670">nʼoma-lʼ</ta>
            <ta e="T672" id="Seg_5817" s="T671">porqɨ</ta>
            <ta e="T673" id="Seg_5818" s="T672">ira</ta>
            <ta e="T674" id="Seg_5819" s="T673">ɔːtä-sa</ta>
            <ta e="T675" id="Seg_5820" s="T674">sɨr</ta>
            <ta e="T676" id="Seg_5821" s="T675">por</ta>
            <ta e="T677" id="Seg_5822" s="T676">nʼenna</ta>
            <ta e="T678" id="Seg_5823" s="T677">tɔːqqɨ-ŋɨ-t</ta>
            <ta e="T679" id="Seg_5824" s="T678">na</ta>
            <ta e="T680" id="Seg_5825" s="T679">na</ta>
            <ta e="T681" id="Seg_5826" s="T680">čʼilʼa-lʼ</ta>
            <ta e="T682" id="Seg_5827" s="T681">ija</ta>
            <ta e="T683" id="Seg_5828" s="T682">ɛj</ta>
            <ta e="T684" id="Seg_5829" s="T683">təp-ɨ-sa</ta>
            <ta e="T685" id="Seg_5830" s="T684">ɛː-sɔː-qı</ta>
            <ta e="T687" id="Seg_5831" s="T686">tomoron</ta>
            <ta e="T688" id="Seg_5832" s="T687">təttɨčʼa-t</ta>
            <ta e="T689" id="Seg_5833" s="T688">təp-ɨ-t</ta>
            <ta e="T690" id="Seg_5834" s="T689">mɨ-qɨt</ta>
            <ta e="T691" id="Seg_5835" s="T690">qälɨ-j</ta>
            <ta e="T692" id="Seg_5836" s="T691">na</ta>
            <ta e="T693" id="Seg_5837" s="T692">mɨ</ta>
            <ta e="T695" id="Seg_5838" s="T694">nʼoma-lʼ</ta>
            <ta e="T696" id="Seg_5839" s="T695">porqɨ</ta>
            <ta e="T697" id="Seg_5840" s="T696">ira</ta>
            <ta e="T698" id="Seg_5841" s="T697">təttɨčʼä-lɨ</ta>
            <ta e="T699" id="Seg_5842" s="T698">kətsan-qı-tɨ</ta>
            <ta e="T700" id="Seg_5843" s="T699">ɛː-ppɨ-ntɔː-qı</ta>
            <ta e="T702" id="Seg_5844" s="T701">na</ta>
            <ta e="T703" id="Seg_5845" s="T702">qət-pɨlʼ</ta>
            <ta e="T704" id="Seg_5846" s="T703">qum-ɨ-t-ɨ-t</ta>
            <ta e="T705" id="Seg_5847" s="T704">na</ta>
            <ta e="T706" id="Seg_5848" s="T705">qälɨ-n</ta>
            <ta e="T708" id="Seg_5849" s="T707">namɨ-n</ta>
            <ta e="T709" id="Seg_5850" s="T708">ija-qı</ta>
            <ta e="T710" id="Seg_5851" s="T709">šittɨ</ta>
            <ta e="T712" id="Seg_5852" s="T711">nılʼčʼi</ta>
            <ta e="T713" id="Seg_5853" s="T712">šittɨ</ta>
            <ta e="T714" id="Seg_5854" s="T713">ija-qı</ta>
            <ta e="T716" id="Seg_5855" s="T715">ɔːta-p</ta>
            <ta e="T717" id="Seg_5856" s="T716">tɔːqqɨ-la</ta>
            <ta e="T718" id="Seg_5857" s="T717">moqɨn</ta>
            <ta e="T719" id="Seg_5858" s="T718">tattɔː-t</ta>
            <ta e="T720" id="Seg_5859" s="T719">tıntena</ta>
            <ta e="T721" id="Seg_5860" s="T720">tına</ta>
            <ta e="T722" id="Seg_5861" s="T721">mɔːt-tɨ</ta>
            <ta e="T724" id="Seg_5862" s="T723">tına</ta>
            <ta e="T725" id="Seg_5863" s="T724">mɔːt-ɨ-ŋ-nɔː-t</ta>
            <ta e="T727" id="Seg_5864" s="T726">lʼa</ta>
            <ta e="T728" id="Seg_5865" s="T727">na</ta>
            <ta e="T729" id="Seg_5866" s="T728">namɨ-lʼa</ta>
            <ta e="T730" id="Seg_5867" s="T729">nannɛr</ta>
            <ta e="T733" id="Seg_5868" s="T732">ija-iː-tɨ</ta>
            <ta e="T734" id="Seg_5869" s="T733">tam</ta>
            <ta e="T735" id="Seg_5870" s="T734">na</ta>
            <ta e="T736" id="Seg_5871" s="T735">qən-mmɨ-ntɔː-t</ta>
            <ta e="T737" id="Seg_5872" s="T736">šöt-tɨ</ta>
            <ta e="T738" id="Seg_5873" s="T737">täpä-l-lä</ta>
            <ta e="T739" id="Seg_5874" s="T738">surɨ-l-lä</ta>
            <ta e="T743" id="Seg_5875" s="T742">konnä</ta>
            <ta e="T744" id="Seg_5876" s="T743">mɔːt-ɨ-ŋ-nɔː-t</ta>
            <ta e="T745" id="Seg_5877" s="T744">ta-l</ta>
            <ta e="T746" id="Seg_5878" s="T745">ukkɨr</ta>
            <ta e="T747" id="Seg_5879" s="T746">pi-t</ta>
            <ta e="T748" id="Seg_5880" s="T747">šäqɔː-t</ta>
            <ta e="T750" id="Seg_5881" s="T749">šäqɨ-q-olam-nɔː-t</ta>
            <ta e="T752" id="Seg_5882" s="T751">ija-iː-tɨ</ta>
            <ta e="T753" id="Seg_5883" s="T752">šöt-qɨnɨ</ta>
            <ta e="T754" id="Seg_5884" s="T753">jap</ta>
            <ta e="T755" id="Seg_5885" s="T754">tattɔː-tɨt</ta>
            <ta e="T756" id="Seg_5886" s="T755">täpä-čʼilʼmɨn</ta>
            <ta e="T757" id="Seg_5887" s="T756">moqonä</ta>
            <ta e="T759" id="Seg_5888" s="T758">kəntal-la</ta>
            <ta e="T760" id="Seg_5889" s="T759">tattɔː-t</ta>
            <ta e="T762" id="Seg_5890" s="T761">qaj</ta>
            <ta e="T763" id="Seg_5891" s="T762">ɛsɨ</ta>
            <ta e="T765" id="Seg_5892" s="T764">əsɨ</ta>
            <ta e="T766" id="Seg_5893" s="T765">ira</ta>
            <ta e="T767" id="Seg_5894" s="T766">mɨ-t</ta>
            <ta e="T768" id="Seg_5895" s="T767">tıntena</ta>
            <ta e="T769" id="Seg_5896" s="T768">mɔːt-tɨ</ta>
            <ta e="T770" id="Seg_5897" s="T769">təptɨ-t</ta>
            <ta e="T772" id="Seg_5898" s="T771">karrä</ta>
            <ta e="T773" id="Seg_5899" s="T772">čʼam</ta>
            <ta e="T774" id="Seg_5900" s="T773">mannɨ-mpɔː-tɨt</ta>
            <ta e="T775" id="Seg_5901" s="T774">koptɨ-kɔːl</ta>
            <ta e="T776" id="Seg_5902" s="T775">tü</ta>
            <ta e="T777" id="Seg_5903" s="T776">am-pa-tɨ</ta>
            <ta e="T778" id="Seg_5904" s="T777">əsɨ</ta>
            <ta e="T779" id="Seg_5905" s="T778">ira</ta>
            <ta e="T780" id="Seg_5906" s="T779">mɨ-t</ta>
            <ta e="T781" id="Seg_5907" s="T780">qət-pɔː-t</ta>
            <ta e="T782" id="Seg_5908" s="T781">təp-ɨ-n</ta>
            <ta e="T783" id="Seg_5909" s="T782">nık</ta>
            <ta e="T784" id="Seg_5910" s="T783">tɛnɨ-r-pɔː-tɨt</ta>
            <ta e="T786" id="Seg_5911" s="T785">na</ta>
            <ta e="T787" id="Seg_5912" s="T786">seːlʼčʼi</ta>
            <ta e="T788" id="Seg_5913" s="T787">ija-tɨ</ta>
            <ta e="T790" id="Seg_5914" s="T789">ta-l</ta>
            <ta e="T792" id="Seg_5915" s="T791">nɨnɨ</ta>
            <ta e="T793" id="Seg_5916" s="T792">šittɨ-mtäl</ta>
            <ta e="T795" id="Seg_5917" s="T794">qən-tɨ-la</ta>
            <ta e="T796" id="Seg_5918" s="T795">tattɔː-tɨt</ta>
            <ta e="T798" id="Seg_5919" s="T797">po-qɨt</ta>
            <ta e="T799" id="Seg_5920" s="T798">kanak-tɨ</ta>
            <ta e="T800" id="Seg_5921" s="T799">na</ta>
            <ta e="T801" id="Seg_5922" s="T800">kɨpa</ta>
            <ta e="T802" id="Seg_5923" s="T801">ija-n-tɨ</ta>
            <ta e="T803" id="Seg_5924" s="T802">kanak</ta>
            <ta e="T805" id="Seg_5925" s="T804">ima-tɨ</ta>
            <ta e="T806" id="Seg_5926" s="T805">pon</ta>
            <ta e="T807" id="Seg_5927" s="T806">sɔːrɨ-mpa-t</ta>
            <ta e="T809" id="Seg_5928" s="T808">ɔːta-p</ta>
            <ta e="T810" id="Seg_5929" s="T809">kutɨ-lʼ</ta>
            <ta e="T811" id="Seg_5930" s="T810">ətɨmantɨ-n</ta>
            <ta e="T812" id="Seg_5931" s="T811">na</ta>
            <ta e="T813" id="Seg_5932" s="T812">qälɨ-n</ta>
            <ta e="T814" id="Seg_5933" s="T813">ɔːta-p</ta>
            <ta e="T815" id="Seg_5934" s="T814">tɔːqqɔː-tɨt</ta>
            <ta e="T817" id="Seg_5935" s="T816">kana-t</ta>
            <ta e="T818" id="Seg_5936" s="T817">topɨ-p</ta>
            <ta e="T819" id="Seg_5937" s="T818">čʼıqɨlʼ-nɔː-t</ta>
            <ta e="T820" id="Seg_5938" s="T819">kanak</ta>
            <ta e="T821" id="Seg_5939" s="T820">läq-alʼ-nʼa</ta>
            <ta e="T823" id="Seg_5940" s="T822">ira</ta>
            <ta e="T824" id="Seg_5941" s="T823">nı-lʼ</ta>
            <ta e="T825" id="Seg_5942" s="T824">nü-lʼčʼa</ta>
            <ta e="T826" id="Seg_5943" s="T825">nʼoma-lʼ</ta>
            <ta e="T827" id="Seg_5944" s="T826">porqɨ</ta>
            <ta e="T828" id="Seg_5945" s="T827">ira</ta>
            <ta e="T829" id="Seg_5946" s="T828">mɨta</ta>
            <ta e="T831" id="Seg_5947" s="T830">kana-mɨ</ta>
            <ta e="T832" id="Seg_5948" s="T831">qaj</ta>
            <ta e="T833" id="Seg_5949" s="T832">am-pa-tɨ</ta>
            <ta e="T834" id="Seg_5950" s="T833">qaj</ta>
            <ta e="T835" id="Seg_5951" s="T834">ɔːtä</ta>
            <ta e="T836" id="Seg_5952" s="T835">pačʼ-al-nɨ-t</ta>
            <ta e="T838" id="Seg_5953" s="T837">pija</ta>
            <ta e="T839" id="Seg_5954" s="T838">kɨpa</ta>
            <ta e="T840" id="Seg_5955" s="T839">ija-t</ta>
            <ta e="T841" id="Seg_5956" s="T840">ima</ta>
            <ta e="T842" id="Seg_5957" s="T841">ponä</ta>
            <ta e="T843" id="Seg_5958" s="T842">pakta</ta>
            <ta e="T845" id="Seg_5959" s="T844">na</ta>
            <ta e="T846" id="Seg_5960" s="T845">nʼoma-lʼ</ta>
            <ta e="T847" id="Seg_5961" s="T846">porqɨ</ta>
            <ta e="T848" id="Seg_5962" s="T847">ira-tɨ</ta>
            <ta e="T849" id="Seg_5963" s="T848">kɨpa</ta>
            <ta e="T850" id="Seg_5964" s="T849">ija-tɨ</ta>
            <ta e="T852" id="Seg_5965" s="T851">ponä</ta>
            <ta e="T853" id="Seg_5966" s="T852">čʼam</ta>
            <ta e="T854" id="Seg_5967" s="T853">pakta</ta>
            <ta e="T855" id="Seg_5968" s="T854">qaj</ta>
            <ta e="T856" id="Seg_5969" s="T855">ɛsa</ta>
            <ta e="T857" id="Seg_5970" s="T856">iraqota-tɨ</ta>
            <ta e="T859" id="Seg_5971" s="T858">na</ta>
            <ta e="T860" id="Seg_5972" s="T859">qaj</ta>
            <ta e="T861" id="Seg_5973" s="T860">qum-ɨ-t</ta>
            <ta e="T862" id="Seg_5974" s="T861">kəː-ŋa</ta>
            <ta e="T864" id="Seg_5975" s="T863">əsɨ</ta>
            <ta e="T865" id="Seg_5976" s="T864">ira-t</ta>
            <ta e="T866" id="Seg_5977" s="T865">tam</ta>
            <ta e="T867" id="Seg_5978" s="T866">nü-lʼčʼɨ-ku</ta>
            <ta e="T868" id="Seg_5979" s="T867">na</ta>
            <ta e="T869" id="Seg_5980" s="T868">mɔːt-qɨn</ta>
            <ta e="T871" id="Seg_5981" s="T870">mompa</ta>
            <ta e="T872" id="Seg_5982" s="T871">meː</ta>
            <ta e="T873" id="Seg_5983" s="T872">mompa</ta>
            <ta e="T874" id="Seg_5984" s="T873">mɔːn-mɨn</ta>
            <ta e="T875" id="Seg_5985" s="T874">mompa</ta>
            <ta e="T876" id="Seg_5986" s="T875">meː</ta>
            <ta e="T877" id="Seg_5987" s="T876">tü-s</ta>
            <ta e="T878" id="Seg_5988" s="T877">čʼɔːtɨ-sɨ-mɨn</ta>
            <ta e="T879" id="Seg_5989" s="T878">mɨta</ta>
            <ta e="T881" id="Seg_5990" s="T880">ilʼčʼa</ta>
            <ta e="T882" id="Seg_5991" s="T881">mompa</ta>
            <ta e="T883" id="Seg_5992" s="T882">nı-lʼ</ta>
            <ta e="T884" id="Seg_5993" s="T883">šımɨt</ta>
            <ta e="T894" id="Seg_5994" s="T893">kɨsa</ta>
            <ta e="T895" id="Seg_5995" s="T894">nʼenna</ta>
            <ta e="T896" id="Seg_5996" s="T895">tom-tɨ</ta>
            <ta e="T898" id="Seg_5997" s="T897">nɨn</ta>
            <ta e="T899" id="Seg_5998" s="T898">nɔːtɨ</ta>
            <ta e="T900" id="Seg_5999" s="T899">na</ta>
            <ta e="T901" id="Seg_6000" s="T900">ilɨ-mmɨ-ntɔː-t</ta>
            <ta e="T902" id="Seg_6001" s="T901">nɨmtɨ</ta>
            <ta e="T903" id="Seg_6002" s="T902">na</ta>
            <ta e="T904" id="Seg_6003" s="T903">qapija</ta>
            <ta e="T905" id="Seg_6004" s="T904">ɛj</ta>
            <ta e="T906" id="Seg_6005" s="T905">älpä</ta>
            <ta e="T907" id="Seg_6006" s="T906">qən-pɔː-tɨt</ta>
            <ta e="T908" id="Seg_6007" s="T907">na</ta>
            <ta e="T909" id="Seg_6008" s="T908">šölʼqum-ɨ-t</ta>
            <ta e="T910" id="Seg_6009" s="T909">tına</ta>
            <ta e="T912" id="Seg_6010" s="T911">qapija</ta>
            <ta e="T913" id="Seg_6011" s="T912">qälɨ-j</ta>
            <ta e="T914" id="Seg_6012" s="T913">tətto-t</ta>
            <ta e="T915" id="Seg_6013" s="T914">ɔːta-p</ta>
            <ta e="T916" id="Seg_6014" s="T915">tɔːqqɨ-mmɨ-ntɔː-t</ta>
            <ta e="T918" id="Seg_6015" s="T917">aj</ta>
            <ta e="T919" id="Seg_6016" s="T918">na</ta>
            <ta e="T920" id="Seg_6017" s="T919">šölʼqup</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T11" id="Seg_6018" s="T10">kɨssa</ta>
            <ta e="T12" id="Seg_6019" s="T11">kətɨ-ätɨ</ta>
            <ta e="T13" id="Seg_6020" s="T12">nɔːtɨ</ta>
            <ta e="T14" id="Seg_6021" s="T13">jarɨk</ta>
            <ta e="T15" id="Seg_6022" s="T14">qaj</ta>
            <ta e="T17" id="Seg_6023" s="T16">mɨ</ta>
            <ta e="T18" id="Seg_6024" s="T17">tına</ta>
            <ta e="T19" id="Seg_6025" s="T18">nʼoma-lʼ</ta>
            <ta e="T20" id="Seg_6026" s="T19">porqɨ</ta>
            <ta e="T21" id="Seg_6027" s="T20">ira</ta>
            <ta e="T22" id="Seg_6028" s="T21">kos</ta>
            <ta e="T23" id="Seg_6029" s="T22">kətɨ-ša-k</ta>
            <ta e="T25" id="Seg_6030" s="T24">nʼoma-lʼ</ta>
            <ta e="T26" id="Seg_6031" s="T25">porqɨ</ta>
            <ta e="T27" id="Seg_6032" s="T26">ira</ta>
            <ta e="T28" id="Seg_6033" s="T27">tına</ta>
            <ta e="T29" id="Seg_6034" s="T28">isɛčʼarɨ</ta>
            <ta e="T30" id="Seg_6035" s="T29">seːlʼčʼɨ</ta>
            <ta e="T31" id="Seg_6036" s="T30">iːja-tɨ</ta>
            <ta e="T32" id="Seg_6037" s="T31">ɛː-mpɨ-ntɨ</ta>
            <ta e="T34" id="Seg_6038" s="T33">aj</ta>
            <ta e="T35" id="Seg_6039" s="T34">seːlʼčʼɨ</ta>
            <ta e="T36" id="Seg_6040" s="T35">ämnä-tɨ</ta>
            <ta e="T38" id="Seg_6041" s="T37">qarɨ-n</ta>
            <ta e="T39" id="Seg_6042" s="T38">tü-ŋɨ-tɨt</ta>
            <ta e="T41" id="Seg_6043" s="T40">qälɨk-t</ta>
            <ta e="T42" id="Seg_6044" s="T41">takkɨ-nɨ</ta>
            <ta e="T43" id="Seg_6045" s="T42">nʼennä</ta>
            <ta e="T44" id="Seg_6046" s="T43">tü-ŋɨ-tɨt</ta>
            <ta e="T45" id="Seg_6047" s="T44">təp-ɨ-m</ta>
            <ta e="T46" id="Seg_6048" s="T45">qət-qɨntoːqo</ta>
            <ta e="T48" id="Seg_6049" s="T47">qum-iː-tɨ</ta>
            <ta e="T49" id="Seg_6050" s="T48">iːja-iː-tɨ</ta>
            <ta e="T50" id="Seg_6051" s="T49">mačʼä</ta>
            <ta e="T51" id="Seg_6052" s="T50">qən-mpɨ-tɨt</ta>
            <ta e="T52" id="Seg_6053" s="T51">täpäŋ-š-lä</ta>
            <ta e="T54" id="Seg_6054" s="T53">täpäŋ-š-lä</ta>
            <ta e="T55" id="Seg_6055" s="T54">mačʼä</ta>
            <ta e="T56" id="Seg_6056" s="T55">qən-mpɨ-tɨt</ta>
            <ta e="T57" id="Seg_6057" s="T56">tıː</ta>
            <ta e="T58" id="Seg_6058" s="T57">qum</ta>
            <ta e="T59" id="Seg_6059" s="T58">tıː</ta>
            <ta e="T60" id="Seg_6060" s="T59">kə-n</ta>
            <ta e="T61" id="Seg_6061" s="T60">tiː</ta>
            <ta e="T62" id="Seg_6062" s="T61">kuttar</ta>
            <ta e="T63" id="Seg_6063" s="T62">ɛː-kkɨ</ta>
            <ta e="T64" id="Seg_6064" s="T63">kə-n</ta>
            <ta e="T66" id="Seg_6065" s="T65">tına</ta>
            <ta e="T67" id="Seg_6066" s="T66">iːja-m</ta>
            <ta e="T68" id="Seg_6067" s="T67">šölʼqum-ɨ-lʼ</ta>
            <ta e="T69" id="Seg_6068" s="T68">iːja-m</ta>
            <ta e="T70" id="Seg_6069" s="T69">wərɨ-mpɨ-ntɨ</ta>
            <ta e="T71" id="Seg_6070" s="T70">tına</ta>
            <ta e="T72" id="Seg_6071" s="T71">qälɨk</ta>
            <ta e="T74" id="Seg_6072" s="T73">na</ta>
            <ta e="T75" id="Seg_6073" s="T74">mütɨ-ntɨ-ntɨ</ta>
            <ta e="T77" id="Seg_6074" s="T76">nʼoma-lʼ</ta>
            <ta e="T78" id="Seg_6075" s="T77">porqɨ</ta>
            <ta e="T79" id="Seg_6076" s="T78">ira</ta>
            <ta e="T80" id="Seg_6077" s="T79">mɨta</ta>
            <ta e="T81" id="Seg_6078" s="T80">täːlɨ</ta>
            <ta e="T82" id="Seg_6079" s="T81">mɨta</ta>
            <ta e="T83" id="Seg_6080" s="T82">meː</ta>
            <ta e="T84" id="Seg_6081" s="T83">tü-ɛntɨ-mɨt</ta>
            <ta e="T85" id="Seg_6082" s="T84">təp-ɨ-n</ta>
            <ta e="T86" id="Seg_6083" s="T85">mɔːt-ntɨ</ta>
            <ta e="T87" id="Seg_6084" s="T86">tɛː</ta>
            <ta e="T88" id="Seg_6085" s="T87">mɨta</ta>
            <ta e="T89" id="Seg_6086" s="T88">qən-ntɨ-ŋɨlɨt</ta>
            <ta e="T90" id="Seg_6087" s="T89">toː</ta>
            <ta e="T92" id="Seg_6088" s="T91">iːja-qı</ta>
            <ta e="T93" id="Seg_6089" s="T92">moqɨnä</ta>
            <ta e="T94" id="Seg_6090" s="T93">qən-lä</ta>
            <ta e="T95" id="Seg_6091" s="T94">nık</ta>
            <ta e="T96" id="Seg_6092" s="T95">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T97" id="Seg_6093" s="T96">čʼilʼaŋ-lʼ</ta>
            <ta e="T98" id="Seg_6094" s="T97">iːja-qı</ta>
            <ta e="T99" id="Seg_6095" s="T98">qälɨk</ta>
            <ta e="T100" id="Seg_6096" s="T99">wərɨ-mpɨ-ntɨlʼ</ta>
            <ta e="T102" id="Seg_6097" s="T101">tam</ta>
            <ta e="T103" id="Seg_6098" s="T102">šölʼqum-ɨ-lʼ</ta>
            <ta e="T104" id="Seg_6099" s="T103">iːja-qı</ta>
            <ta e="T106" id="Seg_6100" s="T105">*taŋɨ-altɨ-äšɨk</ta>
            <ta e="T107" id="Seg_6101" s="T106">*taŋɨ-altɨ-äšɨk</ta>
            <ta e="T108" id="Seg_6102" s="T107">lʼi</ta>
            <ta e="T109" id="Seg_6103" s="T108">qaj</ta>
            <ta e="T110" id="Seg_6104" s="T109">kanaŋ-m</ta>
            <ta e="T111" id="Seg_6105" s="T110">qaj</ta>
            <ta e="T112" id="Seg_6106" s="T111">čʼü-tɨ-ɔːl-altɨ</ta>
            <ta e="T114" id="Seg_6107" s="T113">ta-l</ta>
            <ta e="T116" id="Seg_6108" s="T115">nɨːnɨ</ta>
            <ta e="T117" id="Seg_6109" s="T116">šittälʼ</ta>
            <ta e="T118" id="Seg_6110" s="T117">ira</ta>
            <ta e="T119" id="Seg_6111" s="T118">kučʼčʼä</ta>
            <ta e="T120" id="Seg_6112" s="T119">qattɨ-ɛntɨ</ta>
            <ta e="T122" id="Seg_6113" s="T121">ira</ta>
            <ta e="T123" id="Seg_6114" s="T122">qapı</ta>
            <ta e="T124" id="Seg_6115" s="T123">kekkɨsä</ta>
            <ta e="T125" id="Seg_6116" s="T124">na</ta>
            <ta e="T126" id="Seg_6117" s="T125">ippɨ-ntɨ</ta>
            <ta e="T128" id="Seg_6118" s="T127">ira</ta>
            <ta e="T129" id="Seg_6119" s="T128">nılʼčʼɨ-k</ta>
            <ta e="T130" id="Seg_6120" s="T129">na</ta>
            <ta e="T131" id="Seg_6121" s="T130">ɛsɨ-sɨ</ta>
            <ta e="T132" id="Seg_6122" s="T131">na</ta>
            <ta e="T133" id="Seg_6123" s="T132">nʼoma-lʼ</ta>
            <ta e="T134" id="Seg_6124" s="T133">porqɨ-lʼ</ta>
            <ta e="T135" id="Seg_6125" s="T134">ira</ta>
            <ta e="T137" id="Seg_6126" s="T136">nılʼčʼɨ-k</ta>
            <ta e="T138" id="Seg_6127" s="T137">ɛsɨ</ta>
            <ta e="T139" id="Seg_6128" s="T138">ämnä-ntɨ-lʼ</ta>
            <ta e="T140" id="Seg_6129" s="T139">mɨ-nkinı</ta>
            <ta e="T141" id="Seg_6130" s="T140">apsɨ-m</ta>
            <ta e="T142" id="Seg_6131" s="T141">taːtɨ-ŋɨt</ta>
            <ta e="T143" id="Seg_6132" s="T142">karrä</ta>
            <ta e="T145" id="Seg_6133" s="T144">ür</ta>
            <ta e="T146" id="Seg_6134" s="T145">apsɨ-m</ta>
            <ta e="T147" id="Seg_6135" s="T146">taːtɨ-ŋɨt</ta>
            <ta e="T148" id="Seg_6136" s="T147">*kušša-lʼ</ta>
            <ta e="T149" id="Seg_6137" s="T148">tɛlɨlʼ</ta>
            <ta e="T150" id="Seg_6138" s="T149">apsɨ-m</ta>
            <ta e="T151" id="Seg_6139" s="T150">*ku-n</ta>
            <ta e="T152" id="Seg_6140" s="T151">muntɨk</ta>
            <ta e="T153" id="Seg_6141" s="T152">qaj</ta>
            <ta e="T154" id="Seg_6142" s="T153">taːtɨ-ŋɨt</ta>
            <ta e="T156" id="Seg_6143" s="T155">qälɨk-t</ta>
            <ta e="T157" id="Seg_6144" s="T156">täːlɨ</ta>
            <ta e="T158" id="Seg_6145" s="T157">tü-ɛntɨ-tɨt</ta>
            <ta e="T159" id="Seg_6146" s="T158">am-ɨ-r-ɛntɨ-tɨt</ta>
            <ta e="T161" id="Seg_6147" s="T160">ira</ta>
            <ta e="T162" id="Seg_6148" s="T161">nık</ta>
            <ta e="T163" id="Seg_6149" s="T162">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T165" id="Seg_6150" s="T164">apsɨ-m</ta>
            <ta e="T166" id="Seg_6151" s="T165">taːtɨ-tɨt</ta>
            <ta e="T168" id="Seg_6152" s="T167">tına</ta>
            <ta e="T169" id="Seg_6153" s="T168">tam</ta>
            <ta e="T170" id="Seg_6154" s="T169">nʼuːtɨ-sä</ta>
            <ta e="T171" id="Seg_6155" s="T170">tam</ta>
            <ta e="T172" id="Seg_6156" s="T171">mɔːt</ta>
            <ta e="T173" id="Seg_6157" s="T172">šünʼčʼɨ</ta>
            <ta e="T174" id="Seg_6158" s="T173">šentɨ</ta>
            <ta e="T175" id="Seg_6159" s="T174">nʼuːtɨ-sä</ta>
            <ta e="T176" id="Seg_6160" s="T175">tɔːqqɨ-tɨt</ta>
            <ta e="T177" id="Seg_6161" s="T176">tına</ta>
            <ta e="T179" id="Seg_6162" s="T178">ira</ta>
            <ta e="T180" id="Seg_6163" s="T179">qälɨk-m</ta>
            <ta e="T181" id="Seg_6164" s="T180">apsɨ-tɨ-qo-olam-ŋɨ-tɨ</ta>
            <ta e="T183" id="Seg_6165" s="T182">qəəəə</ta>
            <ta e="T184" id="Seg_6166" s="T183">toːp-tɨlʼ</ta>
            <ta e="T185" id="Seg_6167" s="T184">čʼeːlɨ</ta>
            <ta e="T186" id="Seg_6168" s="T185">qälɨk-t</ta>
            <ta e="T187" id="Seg_6169" s="T186">na</ta>
            <ta e="T188" id="Seg_6170" s="T187">jam</ta>
            <ta e="T189" id="Seg_6171" s="T188">nɔːkɨr-sar-ɨ-lʼ</ta>
            <ta e="T190" id="Seg_6172" s="T189">qum-ɨ-lʼ</ta>
            <ta e="T191" id="Seg_6173" s="T190">mütɨ</ta>
            <ta e="T193" id="Seg_6174" s="T192">tomoron</ta>
            <ta e="T194" id="Seg_6175" s="T193">qapı</ta>
            <ta e="T195" id="Seg_6176" s="T194">šünʼčʼɨ-mpɨlʼ</ta>
            <ta e="T196" id="Seg_6177" s="T195">mɔːt-qɨn</ta>
            <ta e="T197" id="Seg_6178" s="T196">nʼoma-lʼ</ta>
            <ta e="T198" id="Seg_6179" s="T197">porqɨ</ta>
            <ta e="T199" id="Seg_6180" s="T198">ira</ta>
            <ta e="T200" id="Seg_6181" s="T199">ontɨ</ta>
            <ta e="T201" id="Seg_6182" s="T200">qalɨ</ta>
            <ta e="T202" id="Seg_6183" s="T201">ämnä-iː-ntɨ-sä</ta>
            <ta e="T204" id="Seg_6184" s="T203">seːlʼčʼɨ</ta>
            <ta e="T205" id="Seg_6185" s="T204">nʼuːtɨ-sä</ta>
            <ta e="T206" id="Seg_6186" s="T205">tɔːqqɨ-altɨ-ŋɨ-tɨ</ta>
            <ta e="T208" id="Seg_6187" s="T207">ira</ta>
            <ta e="T209" id="Seg_6188" s="T208">nılʼčʼɨ-k</ta>
            <ta e="T210" id="Seg_6189" s="T209">ɛsɨ</ta>
            <ta e="T212" id="Seg_6190" s="T211">mɔːt</ta>
            <ta e="T213" id="Seg_6191" s="T212">jam</ta>
            <ta e="T214" id="Seg_6192" s="T213">*tul-tɨ-tɨt</ta>
            <ta e="T215" id="Seg_6193" s="T214">qapı</ta>
            <ta e="T216" id="Seg_6194" s="T215">tına</ta>
            <ta e="T217" id="Seg_6195" s="T216">na</ta>
            <ta e="T218" id="Seg_6196" s="T217">qälɨk-t</ta>
            <ta e="T220" id="Seg_6197" s="T219">am-ɨ-r-qo-olam-ŋɨ-n</ta>
            <ta e="T222" id="Seg_6198" s="T221">apsɨ</ta>
            <ta e="T223" id="Seg_6199" s="T222">qo-ntɨ-tɨt</ta>
            <ta e="T225" id="Seg_6200" s="T224">ämnä-iː-tɨ</ta>
            <ta e="T226" id="Seg_6201" s="T225">čʼaːj</ta>
            <ta e="T227" id="Seg_6202" s="T226">tam</ta>
            <ta e="T228" id="Seg_6203" s="T227">mušɨ-rɨ-qɨl-ŋɨ-tɨt</ta>
            <ta e="T229" id="Seg_6204" s="T228">karrä-n</ta>
            <ta e="T230" id="Seg_6205" s="T229">*ku-n</ta>
            <ta e="T231" id="Seg_6206" s="T230">mɔːntɨ-lʼ</ta>
            <ta e="T232" id="Seg_6207" s="T231">apsɨ</ta>
            <ta e="T233" id="Seg_6208" s="T232">ür</ta>
            <ta e="T234" id="Seg_6209" s="T233">ür</ta>
            <ta e="T235" id="Seg_6210" s="T234">*ku-n</ta>
            <ta e="T236" id="Seg_6211" s="T235">mɔːntɨ-lʼ</ta>
            <ta e="T237" id="Seg_6212" s="T236">qaj</ta>
            <ta e="T238" id="Seg_6213" s="T237">tına</ta>
            <ta e="T239" id="Seg_6214" s="T238">təːkɨ-mpɨlʼ</ta>
            <ta e="T240" id="Seg_6215" s="T239">apsɨ</ta>
            <ta e="T242" id="Seg_6216" s="T241">ira</ta>
            <ta e="T243" id="Seg_6217" s="T242">nık</ta>
            <ta e="T244" id="Seg_6218" s="T243">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T245" id="Seg_6219" s="T244">imaqota-ntɨ</ta>
            <ta e="T246" id="Seg_6220" s="T245">nʼoma-lʼ</ta>
            <ta e="T247" id="Seg_6221" s="T246">porqɨ</ta>
            <ta e="T248" id="Seg_6222" s="T247">ira</ta>
            <ta e="T249" id="Seg_6223" s="T248">imaqota-tɨ</ta>
            <ta e="T250" id="Seg_6224" s="T249">aj</ta>
            <ta e="T251" id="Seg_6225" s="T250">ɛː-mpɨ</ta>
            <ta e="T253" id="Seg_6226" s="T252">ta-l</ta>
            <ta e="T255" id="Seg_6227" s="T254">ämnä-iː-m-tɨ</ta>
            <ta e="T256" id="Seg_6228" s="T255">nık</ta>
            <ta e="T257" id="Seg_6229" s="T256">*kurɨ-altɨ-ŋɨ-tɨ</ta>
            <ta e="T258" id="Seg_6230" s="T257">tɛː</ta>
            <ta e="T259" id="Seg_6231" s="T258">ponä</ta>
            <ta e="T260" id="Seg_6232" s="T259">tantɨ-ŋɨlɨt</ta>
            <ta e="T262" id="Seg_6233" s="T261">iːja-t</ta>
            <ta e="T263" id="Seg_6234" s="T262">kuti</ta>
            <ta e="T264" id="Seg_6235" s="T263">ponä</ta>
            <ta e="T265" id="Seg_6236" s="T264">taːtɨ-tɨ-ätɨ</ta>
            <ta e="T267" id="Seg_6237" s="T266">ira</ta>
            <ta e="T268" id="Seg_6238" s="T267">kuntɨ</ta>
            <ta e="T269" id="Seg_6239" s="T268">wərkɨ</ta>
            <ta e="T270" id="Seg_6240" s="T269">qum-iː-m-tɨ</ta>
            <ta e="T271" id="Seg_6241" s="T270">apsɨ-tɨ-mpɨ-lä</ta>
            <ta e="T273" id="Seg_6242" s="T272">ukkɨr</ta>
            <ta e="T274" id="Seg_6243" s="T273">tɔːt</ta>
            <ta e="T275" id="Seg_6244" s="T274">čʼontɨ-n</ta>
            <ta e="T276" id="Seg_6245" s="T275">tü-mpɨlʼ</ta>
            <ta e="T277" id="Seg_6246" s="T276">karrä-n</ta>
            <ta e="T278" id="Seg_6247" s="T277">qum-ɨ-t</ta>
            <ta e="T279" id="Seg_6248" s="T278">am-ɨ-r-ŋɨ-tɨt</ta>
            <ta e="T281" id="Seg_6249" s="T280">karrä</ta>
            <ta e="T282" id="Seg_6250" s="T281">na</ta>
            <ta e="T283" id="Seg_6251" s="T282">šoːqɨr-ɨ-lʼ</ta>
            <ta e="T284" id="Seg_6252" s="T283">tü-ntɨ</ta>
            <ta e="T285" id="Seg_6253" s="T284">nannɛr</ta>
            <ta e="T286" id="Seg_6254" s="T285">poː-sä</ta>
            <ta e="T287" id="Seg_6255" s="T286">sälɨ-ŋɨ-tɨ</ta>
            <ta e="T288" id="Seg_6256" s="T287">čʼɔːtɨ-ŋɨ-tɨ</ta>
            <ta e="T289" id="Seg_6257" s="T288">ämnä-iː-m-tɨ</ta>
            <ta e="T290" id="Seg_6258" s="T289">ašša</ta>
            <ta e="T291" id="Seg_6259" s="T290">qaː</ta>
            <ta e="T292" id="Seg_6260" s="T291">ponä</ta>
            <ta e="T293" id="Seg_6261" s="T292">üːtɨ-mpɨ-tɨ</ta>
            <ta e="T294" id="Seg_6262" s="T293">poː-qɨn</ta>
            <ta e="T295" id="Seg_6263" s="T294">tü</ta>
            <ta e="T296" id="Seg_6264" s="T295">čʼɔːtɨ-ätɔːl-ɛntɨ-tɨt</ta>
            <ta e="T298" id="Seg_6265" s="T297">tü</ta>
            <ta e="T299" id="Seg_6266" s="T298">čʼɔːtɨ-alʼ-mpɨ-ntɨ-tɨt</ta>
            <ta e="T300" id="Seg_6267" s="T299">tına</ta>
            <ta e="T301" id="Seg_6268" s="T300">tö-lʼ</ta>
            <ta e="T302" id="Seg_6269" s="T301">purɨš</ta>
            <ta e="T303" id="Seg_6270" s="T302">olɨ-lʼa</ta>
            <ta e="T304" id="Seg_6271" s="T303">nılʼčʼɨ-k</ta>
            <ta e="T305" id="Seg_6272" s="T304">təːkɨ-rɨ-mpɨlʼ</ta>
            <ta e="T306" id="Seg_6273" s="T305">tomoron</ta>
            <ta e="T307" id="Seg_6274" s="T306">mɔːt</ta>
            <ta e="T308" id="Seg_6275" s="T307">tü-m-tɨ</ta>
            <ta e="T310" id="Seg_6276" s="T309">tɛː</ta>
            <ta e="T311" id="Seg_6277" s="T310">najntɨ</ta>
            <ta e="T312" id="Seg_6278" s="T311">man</ta>
            <ta e="T313" id="Seg_6279" s="T312">na</ta>
            <ta e="T314" id="Seg_6280" s="T313">tantɨ-ɛntɨ-k</ta>
            <ta e="T316" id="Seg_6281" s="T315">qum-ɨ-t</ta>
            <ta e="T317" id="Seg_6282" s="T316">am-ɨ-r-sɨ-tɨt</ta>
            <ta e="T319" id="Seg_6283" s="T318">am-ɨ-r-ŋɨ-tɨt</ta>
            <ta e="T321" id="Seg_6284" s="T320">qälɨk-t</ta>
            <ta e="T322" id="Seg_6285" s="T321">nık</ta>
            <ta e="T323" id="Seg_6286" s="T322">kətɨ-tɨt</ta>
            <ta e="T324" id="Seg_6287" s="T323">ilʼčʼa</ta>
            <ta e="T325" id="Seg_6288" s="T324">šoːqɨr-lɨ</ta>
            <ta e="T326" id="Seg_6289" s="T325">naššak</ta>
            <ta e="T327" id="Seg_6290" s="T326">qajqo</ta>
            <ta e="T328" id="Seg_6291" s="T327">poː-sä</ta>
            <ta e="T329" id="Seg_6292" s="T328">sälɨ-l</ta>
            <ta e="T331" id="Seg_6293" s="T330">ašša</ta>
            <ta e="T332" id="Seg_6294" s="T331">šoːqɨr</ta>
            <ta e="T333" id="Seg_6295" s="T332">tü</ta>
            <ta e="T334" id="Seg_6296" s="T333">am-tɨ-ŋɨ-tɨ</ta>
            <ta e="T336" id="Seg_6297" s="T335">ašša</ta>
            <ta e="T337" id="Seg_6298" s="T336">mɨta</ta>
            <ta e="T338" id="Seg_6299" s="T337">man</ta>
            <ta e="T339" id="Seg_6300" s="T338">mɨta</ta>
            <ta e="T340" id="Seg_6301" s="T339">mɨ</ta>
            <ta e="T342" id="Seg_6302" s="T341">təp</ta>
            <ta e="T343" id="Seg_6303" s="T342">ašša</ta>
            <ta e="T344" id="Seg_6304" s="T343">am-ntɨ-tɨ</ta>
            <ta e="T345" id="Seg_6305" s="T344">təp</ta>
            <ta e="T346" id="Seg_6306" s="T345">ašša</ta>
            <ta e="T347" id="Seg_6307" s="T346">am-ntɨ-tɨ</ta>
            <ta e="T348" id="Seg_6308" s="T347">nılʼčʼɨ-lʼ</ta>
            <ta e="T349" id="Seg_6309" s="T348">wərkɨ-ntɨ</ta>
            <ta e="T350" id="Seg_6310" s="T349">nılʼčʼɨ-lʼ</ta>
            <ta e="T351" id="Seg_6311" s="T350">nʼoma-lʼ</ta>
            <ta e="T352" id="Seg_6312" s="T351">porqɨ</ta>
            <ta e="T353" id="Seg_6313" s="T352">raša</ta>
            <ta e="T354" id="Seg_6314" s="T353">lıːpɨ</ta>
            <ta e="T355" id="Seg_6315" s="T354">tına</ta>
            <ta e="T357" id="Seg_6316" s="T356">mɛntälʼ</ta>
            <ta e="T358" id="Seg_6317" s="T357">na-m</ta>
            <ta e="T359" id="Seg_6318" s="T358">šeːr-mpɨ-ntɨ-tɨ</ta>
            <ta e="T361" id="Seg_6319" s="T360">šoŋol-tɨ</ta>
            <ta e="T362" id="Seg_6320" s="T361">qan-qɨn</ta>
            <ta e="T363" id="Seg_6321" s="T362">wərkɨ</ta>
            <ta e="T364" id="Seg_6322" s="T363">mɔːta-n-tɨ</ta>
            <ta e="T365" id="Seg_6323" s="T364">qan-qɨn</ta>
            <ta e="T367" id="Seg_6324" s="T366">ukkɨr</ta>
            <ta e="T368" id="Seg_6325" s="T367">tɔːt</ta>
            <ta e="T369" id="Seg_6326" s="T368">čʼontɨ-n</ta>
            <ta e="T370" id="Seg_6327" s="T369">poː-qɨnɨ</ta>
            <ta e="T371" id="Seg_6328" s="T370">mɔːt-ntɨ</ta>
            <ta e="T372" id="Seg_6329" s="T371">tına</ta>
            <ta e="T373" id="Seg_6330" s="T372">tü-lʼ</ta>
            <ta e="T374" id="Seg_6331" s="T373">purɨš-laka</ta>
            <ta e="T375" id="Seg_6332" s="T374">alʼčʼɨ-ntɨ</ta>
            <ta e="T377" id="Seg_6333" s="T376">na</ta>
            <ta e="T378" id="Seg_6334" s="T377">paktɨ-mpɨ-ntɨ</ta>
            <ta e="T379" id="Seg_6335" s="T378">tına</ta>
            <ta e="T380" id="Seg_6336" s="T379">mɔːta-ntɨ</ta>
            <ta e="T381" id="Seg_6337" s="T380">tına</ta>
            <ta e="T382" id="Seg_6338" s="T381">nılʼčʼɨ</ta>
            <ta e="T383" id="Seg_6339" s="T382">ämnä-iː-tɨ</ta>
            <ta e="T385" id="Seg_6340" s="T384">qälɨk-t</ta>
            <ta e="T386" id="Seg_6341" s="T385">čʼam</ta>
            <ta e="T387" id="Seg_6342" s="T386">orqɨl-mpɨ-tɨt</ta>
            <ta e="T388" id="Seg_6343" s="T387">nılʼčʼɨ-lʼ</ta>
            <ta e="T389" id="Seg_6344" s="T388">ira-m</ta>
            <ta e="T390" id="Seg_6345" s="T389">olä</ta>
            <ta e="T391" id="Seg_6346" s="T390">čʼaŋalʼ</ta>
            <ta e="T392" id="Seg_6347" s="T391">nʼoma-lʼ</ta>
            <ta e="T393" id="Seg_6348" s="T392">porqɨ-n-tɨ</ta>
            <ta e="T394" id="Seg_6349" s="T393">lıːpɨ</ta>
            <ta e="T395" id="Seg_6350" s="T394">nɨta-qɨl-ɛː-lä</ta>
            <ta e="T396" id="Seg_6351" s="T395">orqɨl-ntɨ-tɨt</ta>
            <ta e="T397" id="Seg_6352" s="T396">mɔːt-qɨn</ta>
            <ta e="T399" id="Seg_6353" s="T398">qapı</ta>
            <ta e="T400" id="Seg_6354" s="T399">na</ta>
            <ta e="T401" id="Seg_6355" s="T400">tü-sä</ta>
            <ta e="T402" id="Seg_6356" s="T401">nɨːnɨ</ta>
            <ta e="T403" id="Seg_6357" s="T402">naːtɨ</ta>
            <ta e="T404" id="Seg_6358" s="T403">okoška-n</ta>
            <ta e="T405" id="Seg_6359" s="T404">ɔːŋ-n</ta>
            <ta e="T406" id="Seg_6360" s="T405">šüː-mɨn</ta>
            <ta e="T407" id="Seg_6361" s="T406">mɔːt-ntɨ</ta>
            <ta e="T408" id="Seg_6362" s="T407">tılʼčʼɨ</ta>
            <ta e="T409" id="Seg_6363" s="T408">šittɨ-lʼ</ta>
            <ta e="T410" id="Seg_6364" s="T409">pɛläk-lʼ</ta>
            <ta e="T411" id="Seg_6365" s="T410">okoška</ta>
            <ta e="T412" id="Seg_6366" s="T411">ɛː-mpɨ</ta>
            <ta e="T414" id="Seg_6367" s="T413">qaj</ta>
            <ta e="T415" id="Seg_6368" s="T414">wərqɨ</ta>
            <ta e="T416" id="Seg_6369" s="T415">mɔːt-tɨ</ta>
            <ta e="T417" id="Seg_6370" s="T416">ɛː-mpɨ</ta>
            <ta e="T419" id="Seg_6371" s="T418">mɔːt-tɨ</ta>
            <ta e="T420" id="Seg_6372" s="T419">najntɨ</ta>
            <ta e="T421" id="Seg_6373" s="T420">čʼu-sä</ta>
            <ta e="T422" id="Seg_6374" s="T421">taqɨ-mpɨ-mpɨ</ta>
            <ta e="T423" id="Seg_6375" s="T422">tına</ta>
            <ta e="T424" id="Seg_6376" s="T423">loːq-ɨ-rɨ</ta>
            <ta e="T425" id="Seg_6377" s="T424">poː-lʼ</ta>
            <ta e="T426" id="Seg_6378" s="T425">mɔːt</ta>
            <ta e="T427" id="Seg_6379" s="T426">pičʼɨ-n</ta>
            <ta e="T428" id="Seg_6380" s="T427">paqqɨ-n</ta>
            <ta e="T429" id="Seg_6381" s="T428">piːrɨ-lʼ</ta>
            <ta e="T430" id="Seg_6382" s="T429">taqɨ-mpɨ</ta>
            <ta e="T432" id="Seg_6383" s="T431">mompa</ta>
            <ta e="T433" id="Seg_6384" s="T432">mɔːt-qɨn</ta>
            <ta e="T434" id="Seg_6385" s="T433">tına</ta>
            <ta e="T435" id="Seg_6386" s="T434">na</ta>
            <ta e="T436" id="Seg_6387" s="T435">tü-m</ta>
            <ta e="T437" id="Seg_6388" s="T436">na</ta>
            <ta e="T438" id="Seg_6389" s="T437">qəː-ɔːl-mpɨ-ntɨ-tɨt</ta>
            <ta e="T439" id="Seg_6390" s="T438">na</ta>
            <ta e="T440" id="Seg_6391" s="T439">qättɨ-mpɨ-ntɨ-tɨ</ta>
            <ta e="T441" id="Seg_6392" s="T440">qapı</ta>
            <ta e="T442" id="Seg_6393" s="T441">toːnna</ta>
            <ta e="T923" id="Seg_6394" s="T442">ira-m</ta>
            <ta e="T444" id="Seg_6395" s="T443">qapı</ta>
            <ta e="T445" id="Seg_6396" s="T444">tam</ta>
            <ta e="T446" id="Seg_6397" s="T445">am-n</ta>
            <ta e="T448" id="Seg_6398" s="T447">ta-l</ta>
            <ta e="T450" id="Seg_6399" s="T449">poroq-sä</ta>
            <ta e="T451" id="Seg_6400" s="T450">qaj</ta>
            <ta e="T452" id="Seg_6401" s="T451">təp</ta>
            <ta e="T453" id="Seg_6402" s="T452">mɨta</ta>
            <ta e="T454" id="Seg_6403" s="T453">*qam-ntɨ-mpɨ-tɨ</ta>
            <ta e="T455" id="Seg_6404" s="T454">na</ta>
            <ta e="T456" id="Seg_6405" s="T455">na</ta>
            <ta e="T457" id="Seg_6406" s="T456">nʼuːtɨ-n</ta>
            <ta e="T458" id="Seg_6407" s="T457">*ɨl-qɨn-lʼ</ta>
            <ta e="T459" id="Seg_6408" s="T458">pɛläk-n</ta>
            <ta e="T460" id="Seg_6409" s="T459">tılʼčʼɨ</ta>
            <ta e="T461" id="Seg_6410" s="T460">šentɨ</ta>
            <ta e="T462" id="Seg_6411" s="T461">nʼuːtɨ</ta>
            <ta e="T463" id="Seg_6412" s="T462">*ɨl-qɨn-lʼ</ta>
            <ta e="T464" id="Seg_6413" s="T463">pɛläk-n</ta>
            <ta e="T466" id="Seg_6414" s="T465">na</ta>
            <ta e="T467" id="Seg_6415" s="T466">tü</ta>
            <ta e="T468" id="Seg_6416" s="T467">na</ta>
            <ta e="T469" id="Seg_6417" s="T468">qättɨ-mpɨ-ntɨ-tɨ</ta>
            <ta e="T470" id="Seg_6418" s="T469">tına</ta>
            <ta e="T471" id="Seg_6419" s="T470">nɔːkɨr-sar-ɨ-lʼ</ta>
            <ta e="T472" id="Seg_6420" s="T471">qum-ɨ-lʼ</ta>
            <ta e="T473" id="Seg_6421" s="T472">mütɨ-m</ta>
            <ta e="T474" id="Seg_6422" s="T473">muntɨk</ta>
            <ta e="T475" id="Seg_6423" s="T474">tü</ta>
            <ta e="T476" id="Seg_6424" s="T475">am-mpɨ-tɨ</ta>
            <ta e="T478" id="Seg_6425" s="T477">qöː-n</ta>
            <ta e="T479" id="Seg_6426" s="T478">ɔːlʼčʼɨ-tɨt</ta>
            <ta e="T480" id="Seg_6427" s="T479">nʼennä-n</ta>
            <ta e="T482" id="Seg_6428" s="T481">nʼennä</ta>
            <ta e="T483" id="Seg_6429" s="T482">ponä</ta>
            <ta e="T484" id="Seg_6430" s="T483">ira</ta>
            <ta e="T485" id="Seg_6431" s="T484">ponä</ta>
            <ta e="T486" id="Seg_6432" s="T485">paktɨ</ta>
            <ta e="T487" id="Seg_6433" s="T486">mɔːta-n</ta>
            <ta e="T488" id="Seg_6434" s="T487">ɔːŋ-n</ta>
            <ta e="T489" id="Seg_6435" s="T488">pičʼɨ-lʼ</ta>
            <ta e="T490" id="Seg_6436" s="T489">utɨ</ta>
            <ta e="T491" id="Seg_6437" s="T490">wərkɨ</ta>
            <ta e="T493" id="Seg_6438" s="T492">isɨ</ta>
            <ta e="T494" id="Seg_6439" s="T493">čʼam</ta>
            <ta e="T495" id="Seg_6440" s="T494">ponä</ta>
            <ta e="T496" id="Seg_6441" s="T495">qontɨ-š-lʼčʼɨ-tɨt</ta>
            <ta e="T497" id="Seg_6442" s="T496">aj</ta>
            <ta e="T498" id="Seg_6443" s="T497">čʼarrä</ta>
            <ta e="T499" id="Seg_6444" s="T498">qättɨ-ŋɨ-tɨ</ta>
            <ta e="T501" id="Seg_6445" s="T500">ämnä-iː-tɨ</ta>
            <ta e="T502" id="Seg_6446" s="T501">na</ta>
            <ta e="T503" id="Seg_6447" s="T502">tü</ta>
            <ta e="T504" id="Seg_6448" s="T503">na</ta>
            <ta e="T505" id="Seg_6449" s="T504">qəː-mpɨ-ntɨ-tɨt</ta>
            <ta e="T506" id="Seg_6450" s="T505">šoːqɨr</ta>
            <ta e="T507" id="Seg_6451" s="T506">pɔːrɨ</ta>
            <ta e="T508" id="Seg_6452" s="T507">šüː-mɨn</ta>
            <ta e="T509" id="Seg_6453" s="T508">qəː-ŋɨ-tɨt</ta>
            <ta e="T510" id="Seg_6454" s="T509">nɨːnɨ</ta>
            <ta e="T511" id="Seg_6455" s="T510">aj</ta>
            <ta e="T512" id="Seg_6456" s="T511">šittɨ</ta>
            <ta e="T513" id="Seg_6457" s="T512">okoška-n</ta>
            <ta e="T514" id="Seg_6458" s="T513">ɔːŋ-n</ta>
            <ta e="T515" id="Seg_6459" s="T514">šüː-mɨn</ta>
            <ta e="T516" id="Seg_6460" s="T515">qəː-ŋɨ-tɨt</ta>
            <ta e="T518" id="Seg_6461" s="T517">muntɨk</ta>
            <ta e="T519" id="Seg_6462" s="T518">tü-sä</ta>
            <ta e="T520" id="Seg_6463" s="T519">čʼɔːtɨ-ɛː-mpɨ-tɨ</ta>
            <ta e="T521" id="Seg_6464" s="T520">na</ta>
            <ta e="T522" id="Seg_6465" s="T521">qälɨk-lʼ</ta>
            <ta e="T523" id="Seg_6466" s="T522">mütɨ-m</ta>
            <ta e="T525" id="Seg_6467" s="T524">na</ta>
            <ta e="T526" id="Seg_6468" s="T525">mütɨ</ta>
            <ta e="T527" id="Seg_6469" s="T526">qɔːtɨ</ta>
            <ta e="T528" id="Seg_6470" s="T527">šittɨ</ta>
            <ta e="T529" id="Seg_6471" s="T528">ɛː-mpɨ-ntɨ-tɨt</ta>
            <ta e="T533" id="Seg_6472" s="T532">na</ta>
            <ta e="T534" id="Seg_6473" s="T533">kətɨ-ätɨ</ta>
            <ta e="T536" id="Seg_6474" s="T535">pɛlɨ-kɨtɨ-lʼ</ta>
            <ta e="T537" id="Seg_6475" s="T536">ira</ta>
            <ta e="T538" id="Seg_6476" s="T537">kučʼčʼä</ta>
            <ta e="T539" id="Seg_6477" s="T538">qattɨ-ɛntɨ</ta>
            <ta e="T541" id="Seg_6478" s="T540">ira</ta>
            <ta e="T542" id="Seg_6479" s="T541">nɨːnɨ</ta>
            <ta e="T543" id="Seg_6480" s="T542">šittälʼ</ta>
            <ta e="T544" id="Seg_6481" s="T543">mɨ-ŋɨ</ta>
            <ta e="T545" id="Seg_6482" s="T544">nʼoma-lʼ</ta>
            <ta e="T546" id="Seg_6483" s="T545">porqɨ</ta>
            <ta e="T547" id="Seg_6484" s="T546">ira</ta>
            <ta e="T548" id="Seg_6485" s="T547">karrä</ta>
            <ta e="T549" id="Seg_6486" s="T548">qälɨk-iː-ntɨ</ta>
            <ta e="T550" id="Seg_6487" s="T549">qaqlɨ-ntɨ</ta>
            <ta e="T551" id="Seg_6488" s="T550">qən-ŋɨ</ta>
            <ta e="T553" id="Seg_6489" s="T552">aj</ta>
            <ta e="T554" id="Seg_6490" s="T553">šittɨ-mɨ</ta>
            <ta e="T556" id="Seg_6491" s="T555">takkɨ</ta>
            <ta e="T558" id="Seg_6492" s="T557">qälɨk-lʼ</ta>
            <ta e="T559" id="Seg_6493" s="T558">mɔːt-ntɨ</ta>
            <ta e="T927" id="Seg_6494" s="T559">qən-qo</ta>
            <ta e="T561" id="Seg_6495" s="T560">ıllä</ta>
            <ta e="T562" id="Seg_6496" s="T561">omtɨ-äšɨk</ta>
            <ta e="T564" id="Seg_6497" s="T563">na</ta>
            <ta e="T565" id="Seg_6498" s="T564">qət-qɨl-mpɨlʼ</ta>
            <ta e="T566" id="Seg_6499" s="T565">qum-ɨ-iː-qɨn-tɨ</ta>
            <ta e="T567" id="Seg_6500" s="T566">qən-qɨntoːqo</ta>
            <ta e="T569" id="Seg_6501" s="T568">karrä</ta>
            <ta e="T570" id="Seg_6502" s="T569">qən-ŋɨ</ta>
            <ta e="T571" id="Seg_6503" s="T570">o</ta>
            <ta e="T572" id="Seg_6504" s="T571">karrä-n</ta>
            <ta e="T573" id="Seg_6505" s="T572">konnä</ta>
            <ta e="T574" id="Seg_6506" s="T573">moːčʼa-ntɨlʼ</ta>
            <ta e="T575" id="Seg_6507" s="T574">qälɨk</ta>
            <ta e="T576" id="Seg_6508" s="T575">ira</ta>
            <ta e="T577" id="Seg_6509" s="T576">qontɨ-š-ɛː-ntɨ</ta>
            <ta e="T581" id="Seg_6510" s="T580">qaj</ta>
            <ta e="T582" id="Seg_6511" s="T581">na</ta>
            <ta e="T583" id="Seg_6512" s="T582">laqɨ-mɔːt-ntɨ-k</ta>
            <ta e="T585" id="Seg_6513" s="T584">qaj</ta>
            <ta e="T586" id="Seg_6514" s="T585">nʼoma-lʼ</ta>
            <ta e="T587" id="Seg_6515" s="T586">porqɨ</ta>
            <ta e="T588" id="Seg_6516" s="T587">ira-n</ta>
            <ta e="T591" id="Seg_6517" s="T590">nʼoma-lʼ</ta>
            <ta e="T592" id="Seg_6518" s="T591">porqɨ</ta>
            <ta e="T593" id="Seg_6519" s="T592">ira</ta>
            <ta e="T594" id="Seg_6520" s="T593">čʼarrä</ta>
            <ta e="T595" id="Seg_6521" s="T594">qättɨ-ŋɨ-tɨ</ta>
            <ta e="T596" id="Seg_6522" s="T595">päqqɨ-lʼ</ta>
            <ta e="T597" id="Seg_6523" s="T596">šuk-tɨ</ta>
            <ta e="T598" id="Seg_6524" s="T597">qottä</ta>
            <ta e="T599" id="Seg_6525" s="T598">*səpɨ-mpɨ-ŋɨ</ta>
            <ta e="T601" id="Seg_6526" s="T600">nɨːnɨ</ta>
            <ta e="T602" id="Seg_6527" s="T601">karrä</ta>
            <ta e="T603" id="Seg_6528" s="T602">*kurɨ-ɔːl-ŋɨ</ta>
            <ta e="T605" id="Seg_6529" s="T604">karrä</ta>
            <ta e="T606" id="Seg_6530" s="T605">*kurɨ-ɔːl-ŋɨ</ta>
            <ta e="T607" id="Seg_6531" s="T606">nɨːnɨ</ta>
            <ta e="T608" id="Seg_6532" s="T607">nɔːkɨr</ta>
            <ta e="T609" id="Seg_6533" s="T608">ämnä-sɨ-t</ta>
            <ta e="T610" id="Seg_6534" s="T609">takkɨ</ta>
            <ta e="T611" id="Seg_6535" s="T610">qən-ŋɨ-tɨt</ta>
            <ta e="T613" id="Seg_6536" s="T612">takkɨ</ta>
            <ta e="T614" id="Seg_6537" s="T613">qälɨk-n</ta>
            <ta e="T615" id="Seg_6538" s="T614">mɔːt</ta>
            <ta e="T616" id="Seg_6539" s="T615">takkɨ-n</ta>
            <ta e="T617" id="Seg_6540" s="T616">ɛː-mpɨ</ta>
            <ta e="T618" id="Seg_6541" s="T617">točʼčʼä-oto-n</ta>
            <ta e="T620" id="Seg_6542" s="T619">karrä</ta>
            <ta e="T621" id="Seg_6543" s="T620">qən-ŋɨ-tɨt</ta>
            <ta e="T622" id="Seg_6544" s="T621">nɨmtɨ</ta>
            <ta e="T623" id="Seg_6545" s="T622">näčʼčʼä</ta>
            <ta e="T624" id="Seg_6546" s="T623">laqɨ-altɨ-lʼčʼɨ-tɨt</ta>
            <ta e="T625" id="Seg_6547" s="T624">qälɨk-t-ɨ-n</ta>
            <ta e="T626" id="Seg_6548" s="T625">qaqlɨ-sä</ta>
            <ta e="T627" id="Seg_6549" s="T626">na</ta>
            <ta e="T628" id="Seg_6550" s="T627">qum-iː-n</ta>
            <ta e="T629" id="Seg_6551" s="T628">qaqlɨ-sä</ta>
            <ta e="T630" id="Seg_6552" s="T629">na</ta>
            <ta e="T631" id="Seg_6553" s="T630">nɔːkɨr</ta>
            <ta e="T632" id="Seg_6554" s="T631">ilʼčʼa-sɨ-t</ta>
            <ta e="T634" id="Seg_6555" s="T633">takkɨ</ta>
            <ta e="T635" id="Seg_6556" s="T634">laqɨ-altɨ-ɛː-lä</ta>
            <ta e="T636" id="Seg_6557" s="T635">qən-ŋɨ-tɨt</ta>
            <ta e="T638" id="Seg_6558" s="T637">tına</ta>
            <ta e="T639" id="Seg_6559" s="T638">takkɨ-n</ta>
            <ta e="T640" id="Seg_6560" s="T639">na</ta>
            <ta e="T641" id="Seg_6561" s="T640">qälɨk-lʼ</ta>
            <ta e="T643" id="Seg_6562" s="T642">ima-tɨ-t-ɨ-m</ta>
            <ta e="T644" id="Seg_6563" s="T643">na</ta>
            <ta e="T645" id="Seg_6564" s="T644">šu-qɨl-mpɨ-ntɨ</ta>
            <ta e="T647" id="Seg_6565" s="T646">tına</ta>
            <ta e="T648" id="Seg_6566" s="T647">šölʼqum-ɨ-lʼ</ta>
            <ta e="T649" id="Seg_6567" s="T648">iːja-qı</ta>
            <ta e="T650" id="Seg_6568" s="T649">näčʼčʼatɨ-qɨn</ta>
            <ta e="T651" id="Seg_6569" s="T650">na-mä</ta>
            <ta e="T652" id="Seg_6570" s="T651">qalɨ-mpɨ-qı</ta>
            <ta e="T653" id="Seg_6571" s="T652">tomoron-qɨn</ta>
            <ta e="T654" id="Seg_6572" s="T653">na</ta>
            <ta e="T655" id="Seg_6573" s="T654">ilʼčʼa-ntıː-nɨŋ</ta>
            <ta e="T656" id="Seg_6574" s="T655">tü-mpɨ-qı</ta>
            <ta e="T658" id="Seg_6575" s="T657">konnä-ntɨ</ta>
            <ta e="T659" id="Seg_6576" s="T658">tına</ta>
            <ta e="T660" id="Seg_6577" s="T659">mɨ-ntɨ</ta>
            <ta e="T661" id="Seg_6578" s="T660">šittɨ</ta>
            <ta e="T662" id="Seg_6579" s="T661">qälɨk</ta>
            <ta e="T663" id="Seg_6580" s="T662">moqɨnä</ta>
            <ta e="T664" id="Seg_6581" s="T663">üːtɨ-mpɨ-ntɨ-tɨt</ta>
            <ta e="T666" id="Seg_6582" s="T665">mɔːt-tɨ</ta>
            <ta e="T667" id="Seg_6583" s="T666">tɛː-pɨktɨ</ta>
            <ta e="T669" id="Seg_6584" s="T668">qaj-tɨ</ta>
            <ta e="T670" id="Seg_6585" s="T669">najntɨ</ta>
            <ta e="T671" id="Seg_6586" s="T670">nʼoma-lʼ</ta>
            <ta e="T672" id="Seg_6587" s="T671">porqɨ</ta>
            <ta e="T673" id="Seg_6588" s="T672">ira</ta>
            <ta e="T674" id="Seg_6589" s="T673">ɔːtä-sä</ta>
            <ta e="T675" id="Seg_6590" s="T674">sɨrɨ</ta>
            <ta e="T676" id="Seg_6591" s="T675">por</ta>
            <ta e="T677" id="Seg_6592" s="T676">nʼennä</ta>
            <ta e="T678" id="Seg_6593" s="T677">tɔːqqɨ-ŋɨ-tɨ</ta>
            <ta e="T679" id="Seg_6594" s="T678">na</ta>
            <ta e="T680" id="Seg_6595" s="T679">na</ta>
            <ta e="T681" id="Seg_6596" s="T680">čʼilʼaŋ-lʼ</ta>
            <ta e="T682" id="Seg_6597" s="T681">iːja</ta>
            <ta e="T683" id="Seg_6598" s="T682">aj</ta>
            <ta e="T684" id="Seg_6599" s="T683">təp-ɨ-sä</ta>
            <ta e="T685" id="Seg_6600" s="T684">ɛː-sɨ-qı</ta>
            <ta e="T687" id="Seg_6601" s="T686">tomoron</ta>
            <ta e="T688" id="Seg_6602" s="T687">təttɨčʼa-n</ta>
            <ta e="T689" id="Seg_6603" s="T688">təp-ɨ-n</ta>
            <ta e="T690" id="Seg_6604" s="T689">mɨ-qɨn</ta>
            <ta e="T691" id="Seg_6605" s="T690">qälɨk-lʼ</ta>
            <ta e="T692" id="Seg_6606" s="T691">na</ta>
            <ta e="T693" id="Seg_6607" s="T692">mɨ</ta>
            <ta e="T695" id="Seg_6608" s="T694">nʼoma-lʼ</ta>
            <ta e="T696" id="Seg_6609" s="T695">porqɨ</ta>
            <ta e="T697" id="Seg_6610" s="T696">ira</ta>
            <ta e="T698" id="Seg_6611" s="T697">təttɨčʼa-lɨ</ta>
            <ta e="T699" id="Seg_6612" s="T698">kətsan-qı-tɨ</ta>
            <ta e="T700" id="Seg_6613" s="T699">ɛː-mpɨ-ntɨ-qı</ta>
            <ta e="T702" id="Seg_6614" s="T701">na</ta>
            <ta e="T703" id="Seg_6615" s="T702">qət-mpɨlʼ</ta>
            <ta e="T704" id="Seg_6616" s="T703">qum-ɨ-t-ɨ-n</ta>
            <ta e="T705" id="Seg_6617" s="T704">na</ta>
            <ta e="T706" id="Seg_6618" s="T705">qälɨk-n</ta>
            <ta e="T708" id="Seg_6619" s="T707">na-n</ta>
            <ta e="T709" id="Seg_6620" s="T708">iːja-qı</ta>
            <ta e="T710" id="Seg_6621" s="T709">šittɨ</ta>
            <ta e="T712" id="Seg_6622" s="T711">nılʼčʼɨ</ta>
            <ta e="T713" id="Seg_6623" s="T712">šittɨ</ta>
            <ta e="T714" id="Seg_6624" s="T713">iːja-qı</ta>
            <ta e="T716" id="Seg_6625" s="T715">ɔːtä-m</ta>
            <ta e="T717" id="Seg_6626" s="T716">tɔːqqɨ-lä</ta>
            <ta e="T718" id="Seg_6627" s="T717">moqɨnä</ta>
            <ta e="T719" id="Seg_6628" s="T718">taːtɨ-tɨt</ta>
            <ta e="T720" id="Seg_6629" s="T719">tına</ta>
            <ta e="T721" id="Seg_6630" s="T720">tına</ta>
            <ta e="T722" id="Seg_6631" s="T721">mɔːt-ntɨ</ta>
            <ta e="T724" id="Seg_6632" s="T723">tına</ta>
            <ta e="T725" id="Seg_6633" s="T724">mɔːt-ɨ-k-ŋɨ-tɨt</ta>
            <ta e="T727" id="Seg_6634" s="T726">lʼa</ta>
            <ta e="T728" id="Seg_6635" s="T727">na</ta>
            <ta e="T729" id="Seg_6636" s="T728">na-lʼa</ta>
            <ta e="T730" id="Seg_6637" s="T729">nannɛr</ta>
            <ta e="T733" id="Seg_6638" s="T732">iːja-iː-tɨ</ta>
            <ta e="T734" id="Seg_6639" s="T733">tam</ta>
            <ta e="T735" id="Seg_6640" s="T734">na</ta>
            <ta e="T736" id="Seg_6641" s="T735">qən-mpɨ-ntɨ-tɨt</ta>
            <ta e="T737" id="Seg_6642" s="T736">šöt-ntɨ</ta>
            <ta e="T738" id="Seg_6643" s="T737">täpäŋ-š-lä</ta>
            <ta e="T739" id="Seg_6644" s="T738">suːrɨm-š-lä</ta>
            <ta e="T743" id="Seg_6645" s="T742">konnä</ta>
            <ta e="T744" id="Seg_6646" s="T743">mɔːt-ɨ-k-ŋɨ-tɨt</ta>
            <ta e="T745" id="Seg_6647" s="T744">ta-l</ta>
            <ta e="T746" id="Seg_6648" s="T745">ukkɨr</ta>
            <ta e="T747" id="Seg_6649" s="T746">pi-n</ta>
            <ta e="T748" id="Seg_6650" s="T747">šäqqɨ-tɨt</ta>
            <ta e="T750" id="Seg_6651" s="T749">šäqqɨ-qo-olam-ŋɨ-tɨt</ta>
            <ta e="T752" id="Seg_6652" s="T751">iːja-iː-tɨ</ta>
            <ta e="T753" id="Seg_6653" s="T752">šöt-qɨnɨ</ta>
            <ta e="T754" id="Seg_6654" s="T753">jam</ta>
            <ta e="T755" id="Seg_6655" s="T754">taːtɨ-tɨt</ta>
            <ta e="T756" id="Seg_6656" s="T755">täpäŋ-čʼilʼmɨn</ta>
            <ta e="T757" id="Seg_6657" s="T756">moqɨnä</ta>
            <ta e="T759" id="Seg_6658" s="T758">kəntal-lä</ta>
            <ta e="T760" id="Seg_6659" s="T759">taːtɨ-tɨt</ta>
            <ta e="T762" id="Seg_6660" s="T761">qaj</ta>
            <ta e="T763" id="Seg_6661" s="T762">ɛsɨ</ta>
            <ta e="T765" id="Seg_6662" s="T764">əsɨ</ta>
            <ta e="T766" id="Seg_6663" s="T765">ira</ta>
            <ta e="T767" id="Seg_6664" s="T766">mɨ-n</ta>
            <ta e="T768" id="Seg_6665" s="T767">tına</ta>
            <ta e="T769" id="Seg_6666" s="T768">mɔːt-ntɨ</ta>
            <ta e="T770" id="Seg_6667" s="T769">təptɨ-t</ta>
            <ta e="T772" id="Seg_6668" s="T771">karrä</ta>
            <ta e="T773" id="Seg_6669" s="T772">čʼam</ta>
            <ta e="T774" id="Seg_6670" s="T773">mantɨ-mpɨ-tɨt</ta>
            <ta e="T775" id="Seg_6671" s="T774">koptɨ-kɔːlɨ</ta>
            <ta e="T776" id="Seg_6672" s="T775">tü</ta>
            <ta e="T777" id="Seg_6673" s="T776">am-mpɨ-tɨ</ta>
            <ta e="T778" id="Seg_6674" s="T777">əsɨ</ta>
            <ta e="T779" id="Seg_6675" s="T778">ira</ta>
            <ta e="T780" id="Seg_6676" s="T779">mɨ-t</ta>
            <ta e="T781" id="Seg_6677" s="T780">qət-mpɨ-tɨt</ta>
            <ta e="T782" id="Seg_6678" s="T781">təp-ɨ-t</ta>
            <ta e="T783" id="Seg_6679" s="T782">nık</ta>
            <ta e="T784" id="Seg_6680" s="T783">tɛnɨ-r-mpɨ-tɨt</ta>
            <ta e="T786" id="Seg_6681" s="T785">na</ta>
            <ta e="T787" id="Seg_6682" s="T786">seːlʼčʼɨ</ta>
            <ta e="T788" id="Seg_6683" s="T787">iːja-tɨ</ta>
            <ta e="T790" id="Seg_6684" s="T789">ta-l</ta>
            <ta e="T792" id="Seg_6685" s="T791">nɨːnɨ</ta>
            <ta e="T793" id="Seg_6686" s="T792">šittɨ-mtäl</ta>
            <ta e="T795" id="Seg_6687" s="T794">qən-ntɨ-lä</ta>
            <ta e="T796" id="Seg_6688" s="T795">taːtɨ-tɨt</ta>
            <ta e="T798" id="Seg_6689" s="T797">poː-qɨn</ta>
            <ta e="T799" id="Seg_6690" s="T798">kanaŋ-tɨ</ta>
            <ta e="T800" id="Seg_6691" s="T799">na</ta>
            <ta e="T801" id="Seg_6692" s="T800">kɨpa</ta>
            <ta e="T802" id="Seg_6693" s="T801">iːja-n-tɨ</ta>
            <ta e="T803" id="Seg_6694" s="T802">kanaŋ</ta>
            <ta e="T805" id="Seg_6695" s="T804">ima-tɨ</ta>
            <ta e="T806" id="Seg_6696" s="T805">ponä</ta>
            <ta e="T807" id="Seg_6697" s="T806">sɔːrɨ-mpɨ-tɨ</ta>
            <ta e="T809" id="Seg_6698" s="T808">ɔːtä-m</ta>
            <ta e="T810" id="Seg_6699" s="T809">kutɨ-lʼ</ta>
            <ta e="T811" id="Seg_6700" s="T810">ətɨmantɨ-n</ta>
            <ta e="T812" id="Seg_6701" s="T811">na</ta>
            <ta e="T813" id="Seg_6702" s="T812">qälɨk-n</ta>
            <ta e="T814" id="Seg_6703" s="T813">ɔːtä-m</ta>
            <ta e="T815" id="Seg_6704" s="T814">tɔːqqɨ-tɨt</ta>
            <ta e="T817" id="Seg_6705" s="T816">kanaŋ-n</ta>
            <ta e="T818" id="Seg_6706" s="T817">topɨ-m</ta>
            <ta e="T819" id="Seg_6707" s="T818">čʼıqɨlʼ-ŋɨ-tɨt</ta>
            <ta e="T820" id="Seg_6708" s="T819">kanaŋ</ta>
            <ta e="T821" id="Seg_6709" s="T820">läq-alʼ-ŋɨ</ta>
            <ta e="T823" id="Seg_6710" s="T822">ira</ta>
            <ta e="T824" id="Seg_6711" s="T823">nılʼčʼɨ-lʼ</ta>
            <ta e="T825" id="Seg_6712" s="T824">nüː-ıː</ta>
            <ta e="T826" id="Seg_6713" s="T825">nʼoma-lʼ</ta>
            <ta e="T827" id="Seg_6714" s="T826">porqɨ</ta>
            <ta e="T828" id="Seg_6715" s="T827">ira</ta>
            <ta e="T829" id="Seg_6716" s="T828">mɨta</ta>
            <ta e="T831" id="Seg_6717" s="T830">kanaŋ-mɨ</ta>
            <ta e="T832" id="Seg_6718" s="T831">qaj</ta>
            <ta e="T833" id="Seg_6719" s="T832">am-mpɨ-tɨ</ta>
            <ta e="T834" id="Seg_6720" s="T833">qaj</ta>
            <ta e="T835" id="Seg_6721" s="T834">ɔːtä</ta>
            <ta e="T836" id="Seg_6722" s="T835">pačʼčʼɨ-ätɔːl-ŋɨ-tɨ</ta>
            <ta e="T838" id="Seg_6723" s="T837">pija</ta>
            <ta e="T839" id="Seg_6724" s="T838">kɨpa</ta>
            <ta e="T840" id="Seg_6725" s="T839">iːja-n</ta>
            <ta e="T841" id="Seg_6726" s="T840">ima</ta>
            <ta e="T842" id="Seg_6727" s="T841">ponä</ta>
            <ta e="T843" id="Seg_6728" s="T842">paktɨ</ta>
            <ta e="T845" id="Seg_6729" s="T844">na</ta>
            <ta e="T846" id="Seg_6730" s="T845">nʼoma-lʼ</ta>
            <ta e="T847" id="Seg_6731" s="T846">porqɨ</ta>
            <ta e="T848" id="Seg_6732" s="T847">ira-ntɨ</ta>
            <ta e="T849" id="Seg_6733" s="T848">kɨpa</ta>
            <ta e="T850" id="Seg_6734" s="T849">iːja-tɨ</ta>
            <ta e="T852" id="Seg_6735" s="T851">ponä</ta>
            <ta e="T853" id="Seg_6736" s="T852">čʼam</ta>
            <ta e="T854" id="Seg_6737" s="T853">paktɨ</ta>
            <ta e="T855" id="Seg_6738" s="T854">qaj</ta>
            <ta e="T856" id="Seg_6739" s="T855">ɛsɨ</ta>
            <ta e="T857" id="Seg_6740" s="T856">iraqota-tɨ</ta>
            <ta e="T859" id="Seg_6741" s="T858">na</ta>
            <ta e="T860" id="Seg_6742" s="T859">qaj</ta>
            <ta e="T861" id="Seg_6743" s="T860">qum-ɨ-t</ta>
            <ta e="T862" id="Seg_6744" s="T861">kə-ŋɨ</ta>
            <ta e="T864" id="Seg_6745" s="T863">əsɨ</ta>
            <ta e="T865" id="Seg_6746" s="T864">ira-tɨ</ta>
            <ta e="T866" id="Seg_6747" s="T865">tam</ta>
            <ta e="T867" id="Seg_6748" s="T866">nüː-ıː-kkɨ</ta>
            <ta e="T868" id="Seg_6749" s="T867">na</ta>
            <ta e="T869" id="Seg_6750" s="T868">mɔːt-qɨn</ta>
            <ta e="T871" id="Seg_6751" s="T870">mompa</ta>
            <ta e="T872" id="Seg_6752" s="T871">meː</ta>
            <ta e="T873" id="Seg_6753" s="T872">mompa</ta>
            <ta e="T874" id="Seg_6754" s="T873">mɔːt-mɨt</ta>
            <ta e="T875" id="Seg_6755" s="T874">mompa</ta>
            <ta e="T876" id="Seg_6756" s="T875">meː</ta>
            <ta e="T877" id="Seg_6757" s="T876">tü-sä</ta>
            <ta e="T878" id="Seg_6758" s="T877">čʼɔːtɨ-sɨ-mɨt</ta>
            <ta e="T879" id="Seg_6759" s="T878">mɨta</ta>
            <ta e="T881" id="Seg_6760" s="T880">ilʼčʼa</ta>
            <ta e="T882" id="Seg_6761" s="T881">mompa</ta>
            <ta e="T883" id="Seg_6762" s="T882">nılʼčʼɨ-lʼ</ta>
            <ta e="T884" id="Seg_6763" s="T883">meːšımɨt</ta>
            <ta e="T894" id="Seg_6764" s="T893">kɨssa</ta>
            <ta e="T895" id="Seg_6765" s="T894">nʼennä</ta>
            <ta e="T896" id="Seg_6766" s="T895">tom-ätɨ</ta>
            <ta e="T898" id="Seg_6767" s="T897">nɨːnɨ</ta>
            <ta e="T899" id="Seg_6768" s="T898">nɔːtɨ</ta>
            <ta e="T900" id="Seg_6769" s="T899">na</ta>
            <ta e="T901" id="Seg_6770" s="T900">ilɨ-mpɨ-ntɨ-tɨt</ta>
            <ta e="T902" id="Seg_6771" s="T901">nɨmtɨ</ta>
            <ta e="T903" id="Seg_6772" s="T902">na</ta>
            <ta e="T904" id="Seg_6773" s="T903">qapı</ta>
            <ta e="T905" id="Seg_6774" s="T904">aj</ta>
            <ta e="T906" id="Seg_6775" s="T905">älpä</ta>
            <ta e="T907" id="Seg_6776" s="T906">qən-mpɨ-tɨt</ta>
            <ta e="T908" id="Seg_6777" s="T907">na</ta>
            <ta e="T909" id="Seg_6778" s="T908">šölʼqum-ɨ-t</ta>
            <ta e="T910" id="Seg_6779" s="T909">tına</ta>
            <ta e="T912" id="Seg_6780" s="T911">qapı</ta>
            <ta e="T913" id="Seg_6781" s="T912">qälɨk-lʼ</ta>
            <ta e="T914" id="Seg_6782" s="T913">təttɨ-n</ta>
            <ta e="T915" id="Seg_6783" s="T914">ɔːtä-m</ta>
            <ta e="T916" id="Seg_6784" s="T915">tɔːqqɨ-mpɨ-ntɨ-tɨt</ta>
            <ta e="T918" id="Seg_6785" s="T917">aj</ta>
            <ta e="T919" id="Seg_6786" s="T918">na</ta>
            <ta e="T920" id="Seg_6787" s="T919">šölʼqum</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T11" id="Seg_6788" s="T10">come.on</ta>
            <ta e="T12" id="Seg_6789" s="T11">say-IMP.2SG.O</ta>
            <ta e="T13" id="Seg_6790" s="T12">then</ta>
            <ta e="T14" id="Seg_6791" s="T13">other</ta>
            <ta e="T15" id="Seg_6792" s="T14">what.[NOM]</ta>
            <ta e="T17" id="Seg_6793" s="T16">something.[NOM]</ta>
            <ta e="T18" id="Seg_6794" s="T17">that</ta>
            <ta e="T19" id="Seg_6795" s="T18">hare-ADJZ</ta>
            <ta e="T20" id="Seg_6796" s="T19">clothing.[NOM]</ta>
            <ta e="T21" id="Seg_6797" s="T20">old.man.[NOM]</ta>
            <ta e="T22" id="Seg_6798" s="T21">INDEF3</ta>
            <ta e="T23" id="Seg_6799" s="T22">say-%%-1SG.S</ta>
            <ta e="T25" id="Seg_6800" s="T24">hare-ADJZ</ta>
            <ta e="T26" id="Seg_6801" s="T25">clothing.[NOM]</ta>
            <ta e="T27" id="Seg_6802" s="T26">old.man.[NOM]</ta>
            <ta e="T28" id="Seg_6803" s="T27">that</ta>
            <ta e="T29" id="Seg_6804" s="T28">long.time.ago</ta>
            <ta e="T30" id="Seg_6805" s="T29">seven</ta>
            <ta e="T31" id="Seg_6806" s="T30">child.[NOM]-3SG</ta>
            <ta e="T32" id="Seg_6807" s="T31">be-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T34" id="Seg_6808" s="T33">and</ta>
            <ta e="T35" id="Seg_6809" s="T34">seven</ta>
            <ta e="T36" id="Seg_6810" s="T35">daughter_in_law.[NOM]-3SG</ta>
            <ta e="T38" id="Seg_6811" s="T37">morning-ADV.LOC</ta>
            <ta e="T39" id="Seg_6812" s="T38">come-CO-3PL</ta>
            <ta e="T41" id="Seg_6813" s="T40">Nenets-PL.[NOM]</ta>
            <ta e="T42" id="Seg_6814" s="T41">to.the.north-ADV.EL</ta>
            <ta e="T43" id="Seg_6815" s="T42">upstream</ta>
            <ta e="T44" id="Seg_6816" s="T43">come-CO-3PL</ta>
            <ta e="T45" id="Seg_6817" s="T44">(s)he-EP-ACC</ta>
            <ta e="T46" id="Seg_6818" s="T45">kill-SUP.2/3SG</ta>
            <ta e="T48" id="Seg_6819" s="T47">human.being-PL.[NOM]-3SG</ta>
            <ta e="T49" id="Seg_6820" s="T48">son-PL.[NOM]-3SG</ta>
            <ta e="T50" id="Seg_6821" s="T49">to.the.forest</ta>
            <ta e="T51" id="Seg_6822" s="T50">leave-PST.NAR-3PL</ta>
            <ta e="T52" id="Seg_6823" s="T51">squirrel-CAP-CVB</ta>
            <ta e="T54" id="Seg_6824" s="T53">squirrel-VBLZ-CVB</ta>
            <ta e="T55" id="Seg_6825" s="T54">to.the.forest</ta>
            <ta e="T56" id="Seg_6826" s="T55">leave-PST.NAR-3PL</ta>
            <ta e="T57" id="Seg_6827" s="T56">now</ta>
            <ta e="T58" id="Seg_6828" s="T57">human.being.[NOM]</ta>
            <ta e="T59" id="Seg_6829" s="T58">now</ta>
            <ta e="T60" id="Seg_6830" s="T59">winter-ADV.LOC</ta>
            <ta e="T61" id="Seg_6831" s="T60">now</ta>
            <ta e="T62" id="Seg_6832" s="T61">how</ta>
            <ta e="T63" id="Seg_6833" s="T62">be-HAB.[3SG.S]</ta>
            <ta e="T64" id="Seg_6834" s="T63">winter-ADV.LOC</ta>
            <ta e="T66" id="Seg_6835" s="T65">that</ta>
            <ta e="T67" id="Seg_6836" s="T66">guy-ACC</ta>
            <ta e="T68" id="Seg_6837" s="T67">Selkup-EP-ADJZ</ta>
            <ta e="T69" id="Seg_6838" s="T68">guy-ACC</ta>
            <ta e="T70" id="Seg_6839" s="T69">support-DUR-INFER.[3SG.S]</ta>
            <ta e="T71" id="Seg_6840" s="T70">that</ta>
            <ta e="T72" id="Seg_6841" s="T71">Nenets.[NOM]</ta>
            <ta e="T74" id="Seg_6842" s="T73">this</ta>
            <ta e="T75" id="Seg_6843" s="T74">war-ADV.ILL-OBL.3SG</ta>
            <ta e="T77" id="Seg_6844" s="T76">hare-ADJZ</ta>
            <ta e="T78" id="Seg_6845" s="T77">clothing.[NOM]</ta>
            <ta e="T79" id="Seg_6846" s="T78">old.man.[NOM]</ta>
            <ta e="T80" id="Seg_6847" s="T79">as.if</ta>
            <ta e="T81" id="Seg_6848" s="T80">tomorrow</ta>
            <ta e="T82" id="Seg_6849" s="T81">as.if</ta>
            <ta e="T83" id="Seg_6850" s="T82">we.PL.NOM</ta>
            <ta e="T84" id="Seg_6851" s="T83">come-FUT-1PL</ta>
            <ta e="T85" id="Seg_6852" s="T84">(s)he-EP-GEN</ta>
            <ta e="T86" id="Seg_6853" s="T85">tent-ILL</ta>
            <ta e="T87" id="Seg_6854" s="T86">you.PL.NOM</ta>
            <ta e="T88" id="Seg_6855" s="T87">as.if</ta>
            <ta e="T89" id="Seg_6856" s="T88">leave-IPFV-IMP.2PL</ta>
            <ta e="T90" id="Seg_6857" s="T89">there</ta>
            <ta e="T92" id="Seg_6858" s="T91">child-DU.[NOM]</ta>
            <ta e="T93" id="Seg_6859" s="T92">home</ta>
            <ta e="T94" id="Seg_6860" s="T93">go.away-CVB</ta>
            <ta e="T95" id="Seg_6861" s="T94">so</ta>
            <ta e="T96" id="Seg_6862" s="T95">say-CO-3SG.O</ta>
            <ta e="T97" id="Seg_6863" s="T96">orphan-ADJZ</ta>
            <ta e="T98" id="Seg_6864" s="T97">child-DU.[NOM]</ta>
            <ta e="T99" id="Seg_6865" s="T98">Nenets.[NOM]</ta>
            <ta e="T100" id="Seg_6866" s="T99">keep-DUR-PTCP.PRS</ta>
            <ta e="T102" id="Seg_6867" s="T101">this</ta>
            <ta e="T103" id="Seg_6868" s="T102">Selkup-EP-ADJZ</ta>
            <ta e="T104" id="Seg_6869" s="T103">child-DU.[NOM]</ta>
            <ta e="T106" id="Seg_6870" s="T105">go.quiet-TR-IMP.2SG.S</ta>
            <ta e="T107" id="Seg_6871" s="T106">go.quiet-TR-IMP.2SG.S</ta>
            <ta e="T108" id="Seg_6872" s="T107">whether</ta>
            <ta e="T109" id="Seg_6873" s="T108">whether</ta>
            <ta e="T110" id="Seg_6874" s="T109">dog-ACC</ta>
            <ta e="T111" id="Seg_6875" s="T110">whether</ta>
            <ta e="T112" id="Seg_6876" s="T111">throw-TR-MOM-TR.[3SG.S]</ta>
            <ta e="T114" id="Seg_6877" s="T113">%%-2SG.O</ta>
            <ta e="T116" id="Seg_6878" s="T115">then</ta>
            <ta e="T117" id="Seg_6879" s="T116">then</ta>
            <ta e="T118" id="Seg_6880" s="T117">old.man.[NOM]</ta>
            <ta e="T119" id="Seg_6881" s="T118">where</ta>
            <ta e="T120" id="Seg_6882" s="T119">where.to.go-FUT.[3SG.S]</ta>
            <ta e="T122" id="Seg_6883" s="T121">old.man.[NOM]</ta>
            <ta e="T123" id="Seg_6884" s="T122">supposedly</ta>
            <ta e="T124" id="Seg_6885" s="T123">only</ta>
            <ta e="T125" id="Seg_6886" s="T124">INFER</ta>
            <ta e="T126" id="Seg_6887" s="T125">lie-INFER.[3SG.S]</ta>
            <ta e="T128" id="Seg_6888" s="T127">old.man.[NOM]</ta>
            <ta e="T129" id="Seg_6889" s="T128">such-ADVZ</ta>
            <ta e="T130" id="Seg_6890" s="T129">here</ta>
            <ta e="T131" id="Seg_6891" s="T130">become-PST.[3SG.S]</ta>
            <ta e="T132" id="Seg_6892" s="T131">this</ta>
            <ta e="T133" id="Seg_6893" s="T132">hare-ADJZ</ta>
            <ta e="T134" id="Seg_6894" s="T133">clothing-ADJZ</ta>
            <ta e="T135" id="Seg_6895" s="T134">old.man.[NOM]</ta>
            <ta e="T137" id="Seg_6896" s="T136">such-ADVZ</ta>
            <ta e="T138" id="Seg_6897" s="T137">become.[3SG.S]</ta>
            <ta e="T139" id="Seg_6898" s="T138">daughter_in_law-OBL.3SG-ADJZ</ta>
            <ta e="T140" id="Seg_6899" s="T139">something-ALL</ta>
            <ta e="T141" id="Seg_6900" s="T140">food-ACC</ta>
            <ta e="T142" id="Seg_6901" s="T141">bring-IMP.2PL.S</ta>
            <ta e="T143" id="Seg_6902" s="T142">down</ta>
            <ta e="T145" id="Seg_6903" s="T144">fat.[NOM]</ta>
            <ta e="T146" id="Seg_6904" s="T145">food-ACC</ta>
            <ta e="T147" id="Seg_6905" s="T146">bring-IMP.2PL.S</ta>
            <ta e="T148" id="Seg_6906" s="T147">interrog.pron.stem-ADJZ</ta>
            <ta e="T149" id="Seg_6907" s="T148">%%</ta>
            <ta e="T150" id="Seg_6908" s="T149">food-ACC</ta>
            <ta e="T151" id="Seg_6909" s="T150">QUEST-GEN</ta>
            <ta e="T152" id="Seg_6910" s="T151">all</ta>
            <ta e="T153" id="Seg_6911" s="T152">what.[NOM]</ta>
            <ta e="T154" id="Seg_6912" s="T153">bring-IMP.2PL.S</ta>
            <ta e="T156" id="Seg_6913" s="T155">Nenets-PL.[NOM]</ta>
            <ta e="T157" id="Seg_6914" s="T156">tomorrow</ta>
            <ta e="T158" id="Seg_6915" s="T157">come-FUT-3PL</ta>
            <ta e="T159" id="Seg_6916" s="T158">eat-EP-FRQ-FUT-3PL</ta>
            <ta e="T161" id="Seg_6917" s="T160">old.man.[NOM]</ta>
            <ta e="T162" id="Seg_6918" s="T161">so</ta>
            <ta e="T163" id="Seg_6919" s="T162">say-CO-3SG.O</ta>
            <ta e="T165" id="Seg_6920" s="T164">food-ACC</ta>
            <ta e="T166" id="Seg_6921" s="T165">bring-3PL</ta>
            <ta e="T168" id="Seg_6922" s="T167">that</ta>
            <ta e="T169" id="Seg_6923" s="T168">this</ta>
            <ta e="T170" id="Seg_6924" s="T169">grass-INSTR</ta>
            <ta e="T171" id="Seg_6925" s="T170">this</ta>
            <ta e="T172" id="Seg_6926" s="T171">house.[NOM]</ta>
            <ta e="T173" id="Seg_6927" s="T172">inside.[NOM]</ta>
            <ta e="T174" id="Seg_6928" s="T173">new</ta>
            <ta e="T175" id="Seg_6929" s="T174">grass-INSTR</ta>
            <ta e="T176" id="Seg_6930" s="T175">lay-3PL</ta>
            <ta e="T177" id="Seg_6931" s="T176">that</ta>
            <ta e="T179" id="Seg_6932" s="T178">old.man.[NOM]</ta>
            <ta e="T180" id="Seg_6933" s="T179">Nenets-ACC</ta>
            <ta e="T181" id="Seg_6934" s="T180">food-TR-INF-be.going.to-CO-3SG.O</ta>
            <ta e="T183" id="Seg_6935" s="T182">INTERJ</ta>
            <ta e="T184" id="Seg_6936" s="T183">edge-ADJZ</ta>
            <ta e="T185" id="Seg_6937" s="T184">day.[NOM]</ta>
            <ta e="T186" id="Seg_6938" s="T185">Nenets-PL.[NOM]</ta>
            <ta e="T187" id="Seg_6939" s="T186">this</ta>
            <ta e="T188" id="Seg_6940" s="T187">RFL</ta>
            <ta e="T189" id="Seg_6941" s="T188">three-ten-EP-ADJZ</ta>
            <ta e="T190" id="Seg_6942" s="T189">human.being-EP-ADJZ</ta>
            <ta e="T191" id="Seg_6943" s="T190">army.[NOM]</ta>
            <ta e="T193" id="Seg_6944" s="T192">long.ago</ta>
            <ta e="T194" id="Seg_6945" s="T193">supposedly</ta>
            <ta e="T195" id="Seg_6946" s="T194">run.out-PTCP.PST</ta>
            <ta e="T196" id="Seg_6947" s="T195">tent-LOC</ta>
            <ta e="T197" id="Seg_6948" s="T196">hare-ADJZ</ta>
            <ta e="T198" id="Seg_6949" s="T197">clothing.[NOM]</ta>
            <ta e="T199" id="Seg_6950" s="T198">old.man.[NOM]</ta>
            <ta e="T200" id="Seg_6951" s="T199">oneself.3SG</ta>
            <ta e="T201" id="Seg_6952" s="T200">stay.[3SG.S]</ta>
            <ta e="T202" id="Seg_6953" s="T201">daughter_in_law-PL-OBL.3SG-COM</ta>
            <ta e="T204" id="Seg_6954" s="T203">seven</ta>
            <ta e="T205" id="Seg_6955" s="T204">grass-INSTR</ta>
            <ta e="T206" id="Seg_6956" s="T205">lay-CAUS-CO-3SG.O</ta>
            <ta e="T208" id="Seg_6957" s="T207">old.man.[NOM]</ta>
            <ta e="T209" id="Seg_6958" s="T208">such-ADVZ</ta>
            <ta e="T210" id="Seg_6959" s="T209">become.[3SG.S]</ta>
            <ta e="T212" id="Seg_6960" s="T211">house.[NOM]</ta>
            <ta e="T213" id="Seg_6961" s="T212">RFL</ta>
            <ta e="T214" id="Seg_6962" s="T213">bring-TR-3PL</ta>
            <ta e="T215" id="Seg_6963" s="T214">supposedly</ta>
            <ta e="T216" id="Seg_6964" s="T215">that</ta>
            <ta e="T217" id="Seg_6965" s="T216">this</ta>
            <ta e="T218" id="Seg_6966" s="T217">Nenets-PL.[NOM]</ta>
            <ta e="T220" id="Seg_6967" s="T219">eat-EP-FRQ-INF-be.going.to-CO-GEN</ta>
            <ta e="T222" id="Seg_6968" s="T221">food.[NOM]</ta>
            <ta e="T223" id="Seg_6969" s="T222">find-INFER-3PL</ta>
            <ta e="T225" id="Seg_6970" s="T224">daughter_in_law-PL.[NOM]-3SG</ta>
            <ta e="T226" id="Seg_6971" s="T225">tea.[NOM]</ta>
            <ta e="T227" id="Seg_6972" s="T226">this</ta>
            <ta e="T228" id="Seg_6973" s="T227">be.cooking-CAUS-MULO-CO-3PL</ta>
            <ta e="T229" id="Seg_6974" s="T228">down-ADV.LOC</ta>
            <ta e="T230" id="Seg_6975" s="T229">QUEST-GEN</ta>
            <ta e="T231" id="Seg_6976" s="T230">to.the.extent.of-ADJZ</ta>
            <ta e="T232" id="Seg_6977" s="T231">food.[NOM]</ta>
            <ta e="T233" id="Seg_6978" s="T232">fat.[NOM]</ta>
            <ta e="T234" id="Seg_6979" s="T233">fat.[NOM]</ta>
            <ta e="T235" id="Seg_6980" s="T234">QUEST-GEN</ta>
            <ta e="T236" id="Seg_6981" s="T235">to.the.extent.of-ADJZ</ta>
            <ta e="T237" id="Seg_6982" s="T236">what.[NOM]</ta>
            <ta e="T238" id="Seg_6983" s="T237">that</ta>
            <ta e="T239" id="Seg_6984" s="T238">dry-PTCP.PST</ta>
            <ta e="T240" id="Seg_6985" s="T239">food.[NOM]</ta>
            <ta e="T242" id="Seg_6986" s="T241">old.man.[NOM]</ta>
            <ta e="T243" id="Seg_6987" s="T242">so</ta>
            <ta e="T244" id="Seg_6988" s="T243">say-CO-3SG.O</ta>
            <ta e="T245" id="Seg_6989" s="T244">wife-ILL</ta>
            <ta e="T246" id="Seg_6990" s="T245">hare-ADJZ</ta>
            <ta e="T247" id="Seg_6991" s="T246">clothing.[NOM]</ta>
            <ta e="T248" id="Seg_6992" s="T247">old.man.[NOM]</ta>
            <ta e="T249" id="Seg_6993" s="T248">wife.[NOM]-3SG</ta>
            <ta e="T250" id="Seg_6994" s="T249">also</ta>
            <ta e="T251" id="Seg_6995" s="T250">be-PST.NAR.[3SG.S]</ta>
            <ta e="T253" id="Seg_6996" s="T252">%%-2SG.O</ta>
            <ta e="T255" id="Seg_6997" s="T254">daughter_in_law-PL-ACC-3SG</ta>
            <ta e="T256" id="Seg_6998" s="T255">so</ta>
            <ta e="T257" id="Seg_6999" s="T256">go-TR-CO-3SG.O</ta>
            <ta e="T258" id="Seg_7000" s="T257">you.PL.NOM</ta>
            <ta e="T259" id="Seg_7001" s="T258">outwards</ta>
            <ta e="T260" id="Seg_7002" s="T259">go.out-IMP.2PL</ta>
            <ta e="T262" id="Seg_7003" s="T261">child-PL.[NOM]</ta>
            <ta e="T263" id="Seg_7004" s="T262">%%</ta>
            <ta e="T264" id="Seg_7005" s="T263">outwards</ta>
            <ta e="T265" id="Seg_7006" s="T264">bring-TR-IMP.2SG.O</ta>
            <ta e="T267" id="Seg_7007" s="T266">old.man.[NOM]</ta>
            <ta e="T268" id="Seg_7008" s="T267">long</ta>
            <ta e="T269" id="Seg_7009" s="T268">be.situated.[3SG.S]</ta>
            <ta e="T270" id="Seg_7010" s="T269">human.being-PL-ACC-3SG</ta>
            <ta e="T271" id="Seg_7011" s="T270">food-TR-DUR-CVB</ta>
            <ta e="T273" id="Seg_7012" s="T272">one</ta>
            <ta e="T274" id="Seg_7013" s="T273">whole</ta>
            <ta e="T275" id="Seg_7014" s="T274">middle-ADV.LOC</ta>
            <ta e="T276" id="Seg_7015" s="T275">come-PTCP.PST</ta>
            <ta e="T277" id="Seg_7016" s="T276">down-ADV.LOC</ta>
            <ta e="T278" id="Seg_7017" s="T277">human.being-EP-PL.[NOM]</ta>
            <ta e="T279" id="Seg_7018" s="T278">eat-EP-FRQ-CO-3PL</ta>
            <ta e="T281" id="Seg_7019" s="T280">down</ta>
            <ta e="T282" id="Seg_7020" s="T281">this</ta>
            <ta e="T283" id="Seg_7021" s="T282">oven-EP-ADJZ</ta>
            <ta e="T284" id="Seg_7022" s="T283">fire-ILL</ta>
            <ta e="T285" id="Seg_7023" s="T284">so.much</ta>
            <ta e="T286" id="Seg_7024" s="T285">tree-INSTR</ta>
            <ta e="T287" id="Seg_7025" s="T286">%%-CO-3SG.O</ta>
            <ta e="T288" id="Seg_7026" s="T287">set.fire-CO-3SG.O</ta>
            <ta e="T289" id="Seg_7027" s="T288">daughter_in_law-PL-ACC-3SG</ta>
            <ta e="T290" id="Seg_7028" s="T289">NEG</ta>
            <ta e="T291" id="Seg_7029" s="T290">what.for</ta>
            <ta e="T292" id="Seg_7030" s="T291">outwards</ta>
            <ta e="T293" id="Seg_7031" s="T292">sent-PST.NAR-3SG.O</ta>
            <ta e="T294" id="Seg_7032" s="T293">space.outside-LOC</ta>
            <ta e="T295" id="Seg_7033" s="T294">fire.[NOM]</ta>
            <ta e="T296" id="Seg_7034" s="T295">set.fire-MOM-FUT-3PL</ta>
            <ta e="T298" id="Seg_7035" s="T297">fire.[NOM]</ta>
            <ta e="T299" id="Seg_7036" s="T298">set.fire-INCH-PST.NAR-INFER-3PL</ta>
            <ta e="T300" id="Seg_7037" s="T299">that</ta>
            <ta e="T301" id="Seg_7038" s="T300">birchbark-ADJZ</ta>
            <ta e="T302" id="Seg_7039" s="T301">torch.[NOM]</ta>
            <ta e="T303" id="Seg_7040" s="T302">head-DIM.[NOM]</ta>
            <ta e="T304" id="Seg_7041" s="T303">such-ADVZ</ta>
            <ta e="T305" id="Seg_7042" s="T304">dry-CAUS-PTCP.PST</ta>
            <ta e="T306" id="Seg_7043" s="T305">long.ago</ta>
            <ta e="T307" id="Seg_7044" s="T306">house.[NOM]</ta>
            <ta e="T308" id="Seg_7045" s="T307">fire-ACC-3SG</ta>
            <ta e="T310" id="Seg_7046" s="T309">you.PL.NOM</ta>
            <ta e="T311" id="Seg_7047" s="T310">here.it.is</ta>
            <ta e="T312" id="Seg_7048" s="T311">I.NOM</ta>
            <ta e="T313" id="Seg_7049" s="T312">here</ta>
            <ta e="T314" id="Seg_7050" s="T313">go.out-FUT-1SG.S</ta>
            <ta e="T316" id="Seg_7051" s="T315">human.being-EP-PL.[NOM]</ta>
            <ta e="T317" id="Seg_7052" s="T316">eat-EP-FRQ-PST-3PL</ta>
            <ta e="T319" id="Seg_7053" s="T318">eat-EP-FRQ-CO-3PL</ta>
            <ta e="T321" id="Seg_7054" s="T320">Nenets-PL.[NOM]</ta>
            <ta e="T322" id="Seg_7055" s="T321">so</ta>
            <ta e="T323" id="Seg_7056" s="T322">say-3PL</ta>
            <ta e="T324" id="Seg_7057" s="T323">grandfather.[NOM]</ta>
            <ta e="T325" id="Seg_7058" s="T324">oven.[NOM]-2SG</ta>
            <ta e="T326" id="Seg_7059" s="T325">so.much</ta>
            <ta e="T327" id="Seg_7060" s="T326">why</ta>
            <ta e="T328" id="Seg_7061" s="T327">tree-INSTR</ta>
            <ta e="T329" id="Seg_7062" s="T328">%%-2SG.O</ta>
            <ta e="T331" id="Seg_7063" s="T330">NEG</ta>
            <ta e="T332" id="Seg_7064" s="T331">oven.[NOM]</ta>
            <ta e="T333" id="Seg_7065" s="T332">fire.[NOM]</ta>
            <ta e="T334" id="Seg_7066" s="T333">eat-TR-CO-3SG.O</ta>
            <ta e="T336" id="Seg_7067" s="T335">NEG</ta>
            <ta e="T337" id="Seg_7068" s="T336">as.if</ta>
            <ta e="T338" id="Seg_7069" s="T337">I.NOM</ta>
            <ta e="T339" id="Seg_7070" s="T338">as.if</ta>
            <ta e="T340" id="Seg_7071" s="T339">something.[NOM]</ta>
            <ta e="T342" id="Seg_7072" s="T341">(s)he.[NOM]</ta>
            <ta e="T343" id="Seg_7073" s="T342">NEG</ta>
            <ta e="T344" id="Seg_7074" s="T343">eat-IPFV-3SG.O</ta>
            <ta e="T345" id="Seg_7075" s="T344">(s)he.[NOM]</ta>
            <ta e="T346" id="Seg_7076" s="T345">NEG</ta>
            <ta e="T347" id="Seg_7077" s="T346">eat-IPFV-3SG.O</ta>
            <ta e="T348" id="Seg_7078" s="T347">such-ADJZ</ta>
            <ta e="T349" id="Seg_7079" s="T348">be.situated-IPFV.[3SG.S]</ta>
            <ta e="T350" id="Seg_7080" s="T349">such-ADJZ</ta>
            <ta e="T351" id="Seg_7081" s="T350">hare-ADJZ</ta>
            <ta e="T352" id="Seg_7082" s="T351">clothing.[NOM]</ta>
            <ta e="T353" id="Seg_7083" s="T352">rags.[NOM]</ta>
            <ta e="T354" id="Seg_7084" s="T353">piece.[NOM]</ta>
            <ta e="T355" id="Seg_7085" s="T354">that</ta>
            <ta e="T357" id="Seg_7086" s="T356">old</ta>
            <ta e="T358" id="Seg_7087" s="T357">this-ACC</ta>
            <ta e="T359" id="Seg_7088" s="T358">put.on-PST.NAR-INFER-3SG.O</ta>
            <ta e="T361" id="Seg_7089" s="T360">fireplace-GEN.3SG</ta>
            <ta e="T362" id="Seg_7090" s="T361">near-LOC</ta>
            <ta e="T363" id="Seg_7091" s="T362">be.situated.[3SG.S]</ta>
            <ta e="T364" id="Seg_7092" s="T363">door-GEN-3SG</ta>
            <ta e="T365" id="Seg_7093" s="T364">near-LOC</ta>
            <ta e="T367" id="Seg_7094" s="T366">one</ta>
            <ta e="T368" id="Seg_7095" s="T367">whole</ta>
            <ta e="T369" id="Seg_7096" s="T368">middle-ADV.LOC</ta>
            <ta e="T370" id="Seg_7097" s="T369">space.outside-EL</ta>
            <ta e="T371" id="Seg_7098" s="T370">tent-ILL</ta>
            <ta e="T372" id="Seg_7099" s="T371">that</ta>
            <ta e="T373" id="Seg_7100" s="T372">fire-ADJZ</ta>
            <ta e="T374" id="Seg_7101" s="T373">torch-SNGL.[NOM]</ta>
            <ta e="T375" id="Seg_7102" s="T374">get.into-INFER.[3SG.S]</ta>
            <ta e="T377" id="Seg_7103" s="T376">INFER</ta>
            <ta e="T378" id="Seg_7104" s="T377">run-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T379" id="Seg_7105" s="T378">that</ta>
            <ta e="T380" id="Seg_7106" s="T379">door-ILL</ta>
            <ta e="T381" id="Seg_7107" s="T380">that</ta>
            <ta e="T382" id="Seg_7108" s="T381">such</ta>
            <ta e="T383" id="Seg_7109" s="T382">daughter_in_law-PL.[NOM]-3SG</ta>
            <ta e="T385" id="Seg_7110" s="T384">Nenets-PL.[NOM]</ta>
            <ta e="T386" id="Seg_7111" s="T385">hardly</ta>
            <ta e="T387" id="Seg_7112" s="T386">catch-PST.NAR-3PL</ta>
            <ta e="T388" id="Seg_7113" s="T387">such-ADJZ</ta>
            <ta e="T389" id="Seg_7114" s="T388">old.man-ACC</ta>
            <ta e="T390" id="Seg_7115" s="T389">only</ta>
            <ta e="T391" id="Seg_7116" s="T390">only</ta>
            <ta e="T392" id="Seg_7117" s="T391">hare-ADJZ</ta>
            <ta e="T393" id="Seg_7118" s="T392">clothing-GEN-3SG</ta>
            <ta e="T394" id="Seg_7119" s="T393">piece.[NOM]</ta>
            <ta e="T395" id="Seg_7120" s="T394">tear-MULO-PFV-CVB</ta>
            <ta e="T396" id="Seg_7121" s="T395">catch-INFER-3PL</ta>
            <ta e="T397" id="Seg_7122" s="T396">house-LOC</ta>
            <ta e="T399" id="Seg_7123" s="T398">supposedly</ta>
            <ta e="T400" id="Seg_7124" s="T399">this</ta>
            <ta e="T401" id="Seg_7125" s="T400">fire-INSTR</ta>
            <ta e="T402" id="Seg_7126" s="T401">then</ta>
            <ta e="T403" id="Seg_7127" s="T402">%%</ta>
            <ta e="T404" id="Seg_7128" s="T403">window-GEN</ta>
            <ta e="T405" id="Seg_7129" s="T404">mouth-GEN</ta>
            <ta e="T406" id="Seg_7130" s="T405">through-PROL</ta>
            <ta e="T407" id="Seg_7131" s="T406">tent-ILL</ta>
            <ta e="T408" id="Seg_7132" s="T407">such</ta>
            <ta e="T409" id="Seg_7133" s="T408">two-ADJZ</ta>
            <ta e="T410" id="Seg_7134" s="T409">side-ADJZ</ta>
            <ta e="T411" id="Seg_7135" s="T410">window.[NOM]</ta>
            <ta e="T412" id="Seg_7136" s="T411">be-PST.NAR.[3SG.S]</ta>
            <ta e="T414" id="Seg_7137" s="T413">whether.[NOM]</ta>
            <ta e="T415" id="Seg_7138" s="T414">big</ta>
            <ta e="T416" id="Seg_7139" s="T415">house.[NOM]-3SG</ta>
            <ta e="T417" id="Seg_7140" s="T416">be-PST.NAR.[3SG.S]</ta>
            <ta e="T419" id="Seg_7141" s="T418">tent.[NOM]-3SG</ta>
            <ta e="T420" id="Seg_7142" s="T419">here.it.is</ta>
            <ta e="T421" id="Seg_7143" s="T420">earth-INSTR</ta>
            <ta e="T422" id="Seg_7144" s="T421">close-DUR-PST.NAR.[3SG.S]</ta>
            <ta e="T423" id="Seg_7145" s="T422">that</ta>
            <ta e="T424" id="Seg_7146" s="T423">stick.up-EP-TR</ta>
            <ta e="T425" id="Seg_7147" s="T424">stick-ADJZ</ta>
            <ta e="T426" id="Seg_7148" s="T425">house.[NOM]</ta>
            <ta e="T427" id="Seg_7149" s="T426">axe-GEN</ta>
            <ta e="T428" id="Seg_7150" s="T427">handle-GEN</ta>
            <ta e="T429" id="Seg_7151" s="T428">height-ADJZ</ta>
            <ta e="T430" id="Seg_7152" s="T429">close-RES.[3SG.S]</ta>
            <ta e="T432" id="Seg_7153" s="T431">it.is.said</ta>
            <ta e="T433" id="Seg_7154" s="T432">tent-LOC</ta>
            <ta e="T434" id="Seg_7155" s="T433">that</ta>
            <ta e="T435" id="Seg_7156" s="T434">this</ta>
            <ta e="T436" id="Seg_7157" s="T435">fire-ACC</ta>
            <ta e="T437" id="Seg_7158" s="T436">INFER</ta>
            <ta e="T438" id="Seg_7159" s="T437">throw-MOM-PST.NAR-INFER-3PL</ta>
            <ta e="T439" id="Seg_7160" s="T438">INFER</ta>
            <ta e="T440" id="Seg_7161" s="T439">hit-PST.NAR-INFER-3SG.O</ta>
            <ta e="T441" id="Seg_7162" s="T440">supposedly</ta>
            <ta e="T442" id="Seg_7163" s="T441">that</ta>
            <ta e="T923" id="Seg_7164" s="T442">old.man-ACC</ta>
            <ta e="T444" id="Seg_7165" s="T443">supposedly</ta>
            <ta e="T445" id="Seg_7166" s="T444">this</ta>
            <ta e="T446" id="Seg_7167" s="T445">eat-3SG.S</ta>
            <ta e="T448" id="Seg_7168" s="T447">%%-2SG.O</ta>
            <ta e="T450" id="Seg_7169" s="T449">gunpowder-INSTR</ta>
            <ta e="T451" id="Seg_7170" s="T450">whether</ta>
            <ta e="T452" id="Seg_7171" s="T451">(s)he</ta>
            <ta e="T453" id="Seg_7172" s="T452">as.if</ta>
            <ta e="T454" id="Seg_7173" s="T453">pour-IPFV-PST.NAR-3SG.O</ta>
            <ta e="T455" id="Seg_7174" s="T454">this</ta>
            <ta e="T456" id="Seg_7175" s="T455">this</ta>
            <ta e="T457" id="Seg_7176" s="T456">grass-GEN</ta>
            <ta e="T458" id="Seg_7177" s="T457">under-ADV.LOC-ADJZ</ta>
            <ta e="T459" id="Seg_7178" s="T458">side-ADV.LOC</ta>
            <ta e="T460" id="Seg_7179" s="T459">such</ta>
            <ta e="T461" id="Seg_7180" s="T460">new</ta>
            <ta e="T462" id="Seg_7181" s="T461">hay.[NOM]</ta>
            <ta e="T463" id="Seg_7182" s="T462">under-ADV.LOC-ADJZ</ta>
            <ta e="T464" id="Seg_7183" s="T463">side-ADV.LOC</ta>
            <ta e="T466" id="Seg_7184" s="T465">this</ta>
            <ta e="T467" id="Seg_7185" s="T466">fire.[NOM]</ta>
            <ta e="T468" id="Seg_7186" s="T467">INFER</ta>
            <ta e="T469" id="Seg_7187" s="T468">hit-PST.NAR-INFER-3SG.O</ta>
            <ta e="T470" id="Seg_7188" s="T469">that</ta>
            <ta e="T471" id="Seg_7189" s="T470">three-ten-EP-ADJZ</ta>
            <ta e="T472" id="Seg_7190" s="T471">human.being-EP-ADJZ</ta>
            <ta e="T473" id="Seg_7191" s="T472">army-ACC</ta>
            <ta e="T474" id="Seg_7192" s="T473">all</ta>
            <ta e="T475" id="Seg_7193" s="T474">fire.[NOM]</ta>
            <ta e="T476" id="Seg_7194" s="T475">eat-PST.NAR-3SG.O</ta>
            <ta e="T478" id="Seg_7195" s="T477">side-ADV.LOC</ta>
            <ta e="T479" id="Seg_7196" s="T478">fall.down-3PL</ta>
            <ta e="T480" id="Seg_7197" s="T479">forward-ADV.LOC</ta>
            <ta e="T482" id="Seg_7198" s="T481">forward</ta>
            <ta e="T483" id="Seg_7199" s="T482">outwards</ta>
            <ta e="T484" id="Seg_7200" s="T483">old.man.[NOM]</ta>
            <ta e="T485" id="Seg_7201" s="T484">outwards</ta>
            <ta e="T486" id="Seg_7202" s="T485">run.[3SG.S]</ta>
            <ta e="T487" id="Seg_7203" s="T486">door-GEN</ta>
            <ta e="T488" id="Seg_7204" s="T487">opening-ADV.LOC</ta>
            <ta e="T489" id="Seg_7205" s="T488">axe-ADJZ</ta>
            <ta e="T490" id="Seg_7206" s="T489">hand.[NOM]</ta>
            <ta e="T491" id="Seg_7207" s="T490">be.situated.[3SG.S]</ta>
            <ta e="T493" id="Seg_7208" s="T492">every</ta>
            <ta e="T494" id="Seg_7209" s="T493">hardly</ta>
            <ta e="T495" id="Seg_7210" s="T494">outwards</ta>
            <ta e="T496" id="Seg_7211" s="T495">appear-US-PFV-3PL</ta>
            <ta e="T497" id="Seg_7212" s="T496">also</ta>
            <ta e="T498" id="Seg_7213" s="T497">in.the.face</ta>
            <ta e="T499" id="Seg_7214" s="T498">hit-CO-3SG.O</ta>
            <ta e="T501" id="Seg_7215" s="T500">daughter_in_law-PL.[NOM]-3SG</ta>
            <ta e="T502" id="Seg_7216" s="T501">this</ta>
            <ta e="T503" id="Seg_7217" s="T502">fire.[NOM]</ta>
            <ta e="T504" id="Seg_7218" s="T503">INFER</ta>
            <ta e="T505" id="Seg_7219" s="T504">throw-PST.NAR-INFER-3PL</ta>
            <ta e="T506" id="Seg_7220" s="T505">oven.[NOM]</ta>
            <ta e="T507" id="Seg_7221" s="T506">top.[NOM]</ta>
            <ta e="T508" id="Seg_7222" s="T507">through-PROL</ta>
            <ta e="T509" id="Seg_7223" s="T508">throw-CO-3PL</ta>
            <ta e="T510" id="Seg_7224" s="T509">then</ta>
            <ta e="T511" id="Seg_7225" s="T510">also</ta>
            <ta e="T512" id="Seg_7226" s="T511">two</ta>
            <ta e="T513" id="Seg_7227" s="T512">window-GEN</ta>
            <ta e="T514" id="Seg_7228" s="T513">mouth-GEN</ta>
            <ta e="T515" id="Seg_7229" s="T514">through-PROL</ta>
            <ta e="T516" id="Seg_7230" s="T515">throw-CO-3PL</ta>
            <ta e="T518" id="Seg_7231" s="T517">all</ta>
            <ta e="T519" id="Seg_7232" s="T518">fire-INSTR</ta>
            <ta e="T520" id="Seg_7233" s="T519">set.fire-PFV-PST.NAR-3SG.O</ta>
            <ta e="T521" id="Seg_7234" s="T520">this</ta>
            <ta e="T522" id="Seg_7235" s="T521">Nenets-ADJZ</ta>
            <ta e="T523" id="Seg_7236" s="T522">army-ACC</ta>
            <ta e="T525" id="Seg_7237" s="T524">this</ta>
            <ta e="T526" id="Seg_7238" s="T525">war.[NOM]</ta>
            <ta e="T527" id="Seg_7239" s="T526">probably</ta>
            <ta e="T528" id="Seg_7240" s="T527">two</ta>
            <ta e="T529" id="Seg_7241" s="T528">be-PST.NAR-INFER-3PL</ta>
            <ta e="T533" id="Seg_7242" s="T532">here</ta>
            <ta e="T534" id="Seg_7243" s="T533">say-IMP.2SG.O</ta>
            <ta e="T536" id="Seg_7244" s="T535">friend-CAR-ADJZ</ta>
            <ta e="T537" id="Seg_7245" s="T536">old.man.[NOM]</ta>
            <ta e="T538" id="Seg_7246" s="T537">where</ta>
            <ta e="T539" id="Seg_7247" s="T538">where.to.go-FUT.[3SG.S]</ta>
            <ta e="T541" id="Seg_7248" s="T540">old.man.[NOM]</ta>
            <ta e="T542" id="Seg_7249" s="T541">then</ta>
            <ta e="T543" id="Seg_7250" s="T542">then</ta>
            <ta e="T544" id="Seg_7251" s="T543">make.something-CO.[3SG.S]</ta>
            <ta e="T545" id="Seg_7252" s="T544">hare-ADJZ</ta>
            <ta e="T546" id="Seg_7253" s="T545">clothing.[NOM]</ta>
            <ta e="T547" id="Seg_7254" s="T546">old.man.[NOM]</ta>
            <ta e="T548" id="Seg_7255" s="T547">down</ta>
            <ta e="T549" id="Seg_7256" s="T548">Nenets-PL-ILL</ta>
            <ta e="T550" id="Seg_7257" s="T549">sledge-ILL</ta>
            <ta e="T551" id="Seg_7258" s="T550">leave-CO.[3SG.S]</ta>
            <ta e="T553" id="Seg_7259" s="T552">also</ta>
            <ta e="T554" id="Seg_7260" s="T553">two-something.[NOM]</ta>
            <ta e="T556" id="Seg_7261" s="T555">to.the.north</ta>
            <ta e="T558" id="Seg_7262" s="T557">Nenets-ADJZ</ta>
            <ta e="T559" id="Seg_7263" s="T558">tent-ILL</ta>
            <ta e="T927" id="Seg_7264" s="T559">go.away-INF</ta>
            <ta e="T561" id="Seg_7265" s="T560">down</ta>
            <ta e="T562" id="Seg_7266" s="T561">sit.down-IMP.2SG.S</ta>
            <ta e="T564" id="Seg_7267" s="T563">this</ta>
            <ta e="T565" id="Seg_7268" s="T564">kill-DRV-PTCP.PST</ta>
            <ta e="T566" id="Seg_7269" s="T565">human.being-EP-PL-ILL-3SG</ta>
            <ta e="T567" id="Seg_7270" s="T566">go.away-SUP.2/3SG</ta>
            <ta e="T569" id="Seg_7271" s="T568">down</ta>
            <ta e="T570" id="Seg_7272" s="T569">go.away-CO.[3SG.S]</ta>
            <ta e="T571" id="Seg_7273" s="T570">oh</ta>
            <ta e="T572" id="Seg_7274" s="T571">down-ADV.LOC</ta>
            <ta e="T573" id="Seg_7275" s="T572">upwards</ta>
            <ta e="T574" id="Seg_7276" s="T573">limp-PTCP.PRS</ta>
            <ta e="T575" id="Seg_7277" s="T574">Nenets.[NOM]</ta>
            <ta e="T576" id="Seg_7278" s="T575">old.man.[NOM]</ta>
            <ta e="T577" id="Seg_7279" s="T576">appear-US-PFV-INFER.[3SG.S]</ta>
            <ta e="T581" id="Seg_7280" s="T580">whether</ta>
            <ta e="T582" id="Seg_7281" s="T581">INFER</ta>
            <ta e="T583" id="Seg_7282" s="T582">laugh-DRV-INFER-1SG.S</ta>
            <ta e="T585" id="Seg_7283" s="T584">whether</ta>
            <ta e="T586" id="Seg_7284" s="T585">hare-ADJZ</ta>
            <ta e="T587" id="Seg_7285" s="T586">clothing.[NOM]</ta>
            <ta e="T588" id="Seg_7286" s="T587">old.man-GEN</ta>
            <ta e="T591" id="Seg_7287" s="T590">hare-ADJZ</ta>
            <ta e="T592" id="Seg_7288" s="T591">clothing.[NOM]</ta>
            <ta e="T593" id="Seg_7289" s="T592">old.man.[NOM]</ta>
            <ta e="T594" id="Seg_7290" s="T593">in.the.face</ta>
            <ta e="T595" id="Seg_7291" s="T594">hit-CO-3SG.O</ta>
            <ta e="T596" id="Seg_7292" s="T595">elk-ADJZ</ta>
            <ta e="T597" id="Seg_7293" s="T596">hindhead.[NOM]-3SG</ta>
            <ta e="T598" id="Seg_7294" s="T597">on.one_s.back</ta>
            <ta e="T599" id="Seg_7295" s="T598">break-RES-CO.[3SG.S]</ta>
            <ta e="T601" id="Seg_7296" s="T600">then</ta>
            <ta e="T602" id="Seg_7297" s="T601">down</ta>
            <ta e="T603" id="Seg_7298" s="T602">go-MOM-CO.[3SG.S]</ta>
            <ta e="T605" id="Seg_7299" s="T604">down</ta>
            <ta e="T606" id="Seg_7300" s="T605">go-MOM-CO.[3SG.S]</ta>
            <ta e="T607" id="Seg_7301" s="T606">then</ta>
            <ta e="T608" id="Seg_7302" s="T607">three</ta>
            <ta e="T609" id="Seg_7303" s="T608">daughter_in_law-DYA-PL.[NOM]</ta>
            <ta e="T610" id="Seg_7304" s="T609">to.the.north</ta>
            <ta e="T611" id="Seg_7305" s="T610">go.away-CO-3PL</ta>
            <ta e="T613" id="Seg_7306" s="T612">to.the.north</ta>
            <ta e="T614" id="Seg_7307" s="T613">Nenets-GEN</ta>
            <ta e="T615" id="Seg_7308" s="T614">house.[NOM]</ta>
            <ta e="T616" id="Seg_7309" s="T615">to.the.north-ADV.LOC</ta>
            <ta e="T617" id="Seg_7310" s="T616">be-PST.NAR.[3SG.S]</ta>
            <ta e="T618" id="Seg_7311" s="T617">there-%%-ADV.LOC</ta>
            <ta e="T620" id="Seg_7312" s="T619">down</ta>
            <ta e="T621" id="Seg_7313" s="T620">go.away-CO-3PL</ta>
            <ta e="T622" id="Seg_7314" s="T621">there</ta>
            <ta e="T623" id="Seg_7315" s="T622">there</ta>
            <ta e="T624" id="Seg_7316" s="T623">move-TR-PFV-3PL</ta>
            <ta e="T625" id="Seg_7317" s="T624">Nenets-PL-EP-GEN</ta>
            <ta e="T626" id="Seg_7318" s="T625">sledge-INSTR</ta>
            <ta e="T627" id="Seg_7319" s="T626">this</ta>
            <ta e="T628" id="Seg_7320" s="T627">human.being-PL-GEN</ta>
            <ta e="T629" id="Seg_7321" s="T628">sledge-INSTR</ta>
            <ta e="T630" id="Seg_7322" s="T629">this</ta>
            <ta e="T631" id="Seg_7323" s="T630">three</ta>
            <ta e="T632" id="Seg_7324" s="T631">grandfather-DYA-PL.[NOM]</ta>
            <ta e="T634" id="Seg_7325" s="T633">to.the.north</ta>
            <ta e="T635" id="Seg_7326" s="T634">move-TR-PFV-CVB</ta>
            <ta e="T636" id="Seg_7327" s="T635">go.away-CO-3PL</ta>
            <ta e="T638" id="Seg_7328" s="T637">that</ta>
            <ta e="T639" id="Seg_7329" s="T638">to.the.north-ADV.LOC</ta>
            <ta e="T640" id="Seg_7330" s="T639">this</ta>
            <ta e="T641" id="Seg_7331" s="T640">Nenets-ADJZ</ta>
            <ta e="T643" id="Seg_7332" s="T642">woman-%%-PL-EP-ACC</ta>
            <ta e="T644" id="Seg_7333" s="T643">INFER</ta>
            <ta e="T645" id="Seg_7334" s="T644">%%-MULO-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T647" id="Seg_7335" s="T646">that</ta>
            <ta e="T648" id="Seg_7336" s="T647">Selkup-EP-ADJZ</ta>
            <ta e="T649" id="Seg_7337" s="T648">child-DU.[NOM]</ta>
            <ta e="T650" id="Seg_7338" s="T649">there-ADV.LOC</ta>
            <ta e="T651" id="Seg_7339" s="T650">this-%%</ta>
            <ta e="T652" id="Seg_7340" s="T651">stay-PST.NAR-3DU.S</ta>
            <ta e="T653" id="Seg_7341" s="T652">long.ago-ADV.LOC</ta>
            <ta e="T654" id="Seg_7342" s="T653">this</ta>
            <ta e="T655" id="Seg_7343" s="T654">grandfather-OBL.3DU-ALL</ta>
            <ta e="T656" id="Seg_7344" s="T655">come-PST.NAR-3DU.S</ta>
            <ta e="T658" id="Seg_7345" s="T657">upwards-ADV.ILL</ta>
            <ta e="T659" id="Seg_7346" s="T658">that</ta>
            <ta e="T660" id="Seg_7347" s="T659">something-ILL</ta>
            <ta e="T661" id="Seg_7348" s="T660">two</ta>
            <ta e="T662" id="Seg_7349" s="T661">Nenets.[NOM]</ta>
            <ta e="T663" id="Seg_7350" s="T662">home</ta>
            <ta e="T664" id="Seg_7351" s="T663">sent-PST.NAR-INFER-3PL</ta>
            <ta e="T666" id="Seg_7352" s="T665">tent.[NOM]-3SG</ta>
            <ta e="T667" id="Seg_7353" s="T666">rot-%%</ta>
            <ta e="T669" id="Seg_7354" s="T668">what.[NOM]-3SG</ta>
            <ta e="T670" id="Seg_7355" s="T669">here.it.is</ta>
            <ta e="T671" id="Seg_7356" s="T670">hare-ADJZ</ta>
            <ta e="T672" id="Seg_7357" s="T671">clothing.[NOM]</ta>
            <ta e="T673" id="Seg_7358" s="T672">old.man.[NOM]</ta>
            <ta e="T674" id="Seg_7359" s="T673">reindeer-COM</ta>
            <ta e="T675" id="Seg_7360" s="T674">snow.[NOM]</ta>
            <ta e="T676" id="Seg_7361" s="T675">%%</ta>
            <ta e="T677" id="Seg_7362" s="T676">forward</ta>
            <ta e="T678" id="Seg_7363" s="T677">shepherd-CO-3SG.O</ta>
            <ta e="T679" id="Seg_7364" s="T678">this</ta>
            <ta e="T680" id="Seg_7365" s="T679">this</ta>
            <ta e="T681" id="Seg_7366" s="T680">orphan-ADJZ</ta>
            <ta e="T682" id="Seg_7367" s="T681">child.[NOM]</ta>
            <ta e="T683" id="Seg_7368" s="T682">also</ta>
            <ta e="T684" id="Seg_7369" s="T683">(s)he-EP-COM</ta>
            <ta e="T685" id="Seg_7370" s="T684">be-PST-3DU.S</ta>
            <ta e="T687" id="Seg_7371" s="T686">long.ago</ta>
            <ta e="T688" id="Seg_7372" s="T687">relative-GEN</ta>
            <ta e="T689" id="Seg_7373" s="T688">(s)he-EP-GEN</ta>
            <ta e="T690" id="Seg_7374" s="T689">something-LOC</ta>
            <ta e="T691" id="Seg_7375" s="T690">Nenets-ADJZ</ta>
            <ta e="T692" id="Seg_7376" s="T691">this</ta>
            <ta e="T693" id="Seg_7377" s="T692">something.[NOM]</ta>
            <ta e="T695" id="Seg_7378" s="T694">hare-ADJZ</ta>
            <ta e="T696" id="Seg_7379" s="T695">clothing.[NOM]</ta>
            <ta e="T697" id="Seg_7380" s="T696">old.man.[NOM]</ta>
            <ta e="T698" id="Seg_7381" s="T697">relative-%%</ta>
            <ta e="T699" id="Seg_7382" s="T698">grandson-DU.[NOM]-3SG</ta>
            <ta e="T700" id="Seg_7383" s="T699">be-PST.NAR-INFER-3DU.S</ta>
            <ta e="T702" id="Seg_7384" s="T701">this</ta>
            <ta e="T703" id="Seg_7385" s="T702">kill-PTCP.PST</ta>
            <ta e="T704" id="Seg_7386" s="T703">human.being-EP-PL-EP-GEN</ta>
            <ta e="T705" id="Seg_7387" s="T704">this</ta>
            <ta e="T706" id="Seg_7388" s="T705">Nenets-GEN</ta>
            <ta e="T708" id="Seg_7389" s="T707">this-GEN</ta>
            <ta e="T709" id="Seg_7390" s="T708">child-DU.[NOM]</ta>
            <ta e="T710" id="Seg_7391" s="T709">two</ta>
            <ta e="T712" id="Seg_7392" s="T711">such</ta>
            <ta e="T713" id="Seg_7393" s="T712">two</ta>
            <ta e="T714" id="Seg_7394" s="T713">child-DU.[NOM]</ta>
            <ta e="T716" id="Seg_7395" s="T715">reindeer-ACC</ta>
            <ta e="T717" id="Seg_7396" s="T716">shepherd-CVB</ta>
            <ta e="T718" id="Seg_7397" s="T717">home</ta>
            <ta e="T719" id="Seg_7398" s="T718">bring-3PL</ta>
            <ta e="T720" id="Seg_7399" s="T719">that</ta>
            <ta e="T721" id="Seg_7400" s="T720">that</ta>
            <ta e="T722" id="Seg_7401" s="T721">house-ILL</ta>
            <ta e="T724" id="Seg_7402" s="T723">that</ta>
            <ta e="T725" id="Seg_7403" s="T724">tent-EP-VBLZ-CO-3PL</ta>
            <ta e="T727" id="Seg_7404" s="T726">hey</ta>
            <ta e="T728" id="Seg_7405" s="T727">this</ta>
            <ta e="T729" id="Seg_7406" s="T728">this-DIM.[NOM]</ta>
            <ta e="T730" id="Seg_7407" s="T729">so.much</ta>
            <ta e="T733" id="Seg_7408" s="T732">son-PL.[NOM]-3SG</ta>
            <ta e="T734" id="Seg_7409" s="T733">this</ta>
            <ta e="T735" id="Seg_7410" s="T734">this</ta>
            <ta e="T736" id="Seg_7411" s="T735">leave-PST.NAR-INFER-3PL</ta>
            <ta e="T737" id="Seg_7412" s="T736">forest-ILL</ta>
            <ta e="T738" id="Seg_7413" s="T737">squirrel-CAP-CVB</ta>
            <ta e="T739" id="Seg_7414" s="T738">wild.animal-VBLZ-CVB</ta>
            <ta e="T743" id="Seg_7415" s="T742">upwards</ta>
            <ta e="T744" id="Seg_7416" s="T743">tent-EP-VBLZ-CO-3PL</ta>
            <ta e="T745" id="Seg_7417" s="T744">%%-2SG.O</ta>
            <ta e="T746" id="Seg_7418" s="T745">one</ta>
            <ta e="T747" id="Seg_7419" s="T746">night-ADV.LOC</ta>
            <ta e="T748" id="Seg_7420" s="T747">spend.night-3PL</ta>
            <ta e="T750" id="Seg_7421" s="T749">spend.night-INF-be.going.to-CO-3PL</ta>
            <ta e="T752" id="Seg_7422" s="T751">son-PL.[NOM]-3SG</ta>
            <ta e="T753" id="Seg_7423" s="T752">forest-EL</ta>
            <ta e="T754" id="Seg_7424" s="T753">RFL</ta>
            <ta e="T755" id="Seg_7425" s="T754">bring-3PL</ta>
            <ta e="T756" id="Seg_7426" s="T755">squirrel-%%</ta>
            <ta e="T757" id="Seg_7427" s="T756">home</ta>
            <ta e="T759" id="Seg_7428" s="T758">slink-CVB</ta>
            <ta e="T760" id="Seg_7429" s="T759">bring-3PL</ta>
            <ta e="T762" id="Seg_7430" s="T761">what.[NOM]</ta>
            <ta e="T763" id="Seg_7431" s="T762">become.[3SG.S]</ta>
            <ta e="T765" id="Seg_7432" s="T764">father.[NOM]</ta>
            <ta e="T766" id="Seg_7433" s="T765">old.man.[NOM]</ta>
            <ta e="T767" id="Seg_7434" s="T766">something-GEN</ta>
            <ta e="T768" id="Seg_7435" s="T767">that</ta>
            <ta e="T769" id="Seg_7436" s="T768">tent-GEN.3SG</ta>
            <ta e="T770" id="Seg_7437" s="T769">%%-PL.[NOM]</ta>
            <ta e="T772" id="Seg_7438" s="T771">down</ta>
            <ta e="T773" id="Seg_7439" s="T772">hardly</ta>
            <ta e="T774" id="Seg_7440" s="T773">give.a.look-PST.NAR-3PL</ta>
            <ta e="T775" id="Seg_7441" s="T774">place-CAR</ta>
            <ta e="T776" id="Seg_7442" s="T775">fire.[NOM]</ta>
            <ta e="T777" id="Seg_7443" s="T776">eat-PST.NAR-3SG.O</ta>
            <ta e="T778" id="Seg_7444" s="T777">father.[NOM]</ta>
            <ta e="T779" id="Seg_7445" s="T778">old.man.[NOM]</ta>
            <ta e="T780" id="Seg_7446" s="T779">something-PL.[NOM]</ta>
            <ta e="T781" id="Seg_7447" s="T780">kill-PST.NAR-3PL</ta>
            <ta e="T782" id="Seg_7448" s="T781">(s)he-EP-PL.[NOM]</ta>
            <ta e="T783" id="Seg_7449" s="T782">so</ta>
            <ta e="T784" id="Seg_7450" s="T783">think-FRQ-DUR-3PL</ta>
            <ta e="T786" id="Seg_7451" s="T785">this</ta>
            <ta e="T787" id="Seg_7452" s="T786">seven</ta>
            <ta e="T788" id="Seg_7453" s="T787">son.[NOM]-3SG</ta>
            <ta e="T790" id="Seg_7454" s="T789">%%-2SG.O</ta>
            <ta e="T792" id="Seg_7455" s="T791">then</ta>
            <ta e="T793" id="Seg_7456" s="T792">two-ITER.NUM</ta>
            <ta e="T795" id="Seg_7457" s="T794">leave-IPFV-CVB</ta>
            <ta e="T796" id="Seg_7458" s="T795">bring-3PL</ta>
            <ta e="T798" id="Seg_7459" s="T797">space.outside-LOC</ta>
            <ta e="T799" id="Seg_7460" s="T798">dog.[NOM]-3SG</ta>
            <ta e="T800" id="Seg_7461" s="T799">this</ta>
            <ta e="T801" id="Seg_7462" s="T800">young</ta>
            <ta e="T802" id="Seg_7463" s="T801">son-GEN-3SG</ta>
            <ta e="T803" id="Seg_7464" s="T802">dog.[NOM]</ta>
            <ta e="T805" id="Seg_7465" s="T804">wife.[NOM]-3SG</ta>
            <ta e="T806" id="Seg_7466" s="T805">outwards</ta>
            <ta e="T807" id="Seg_7467" s="T806">bind-PST.NAR-3SG.O</ta>
            <ta e="T809" id="Seg_7468" s="T808">reindeer-ACC</ta>
            <ta e="T810" id="Seg_7469" s="T809">who-ADJZ</ta>
            <ta e="T811" id="Seg_7470" s="T810">outside-ADV.LOC</ta>
            <ta e="T812" id="Seg_7471" s="T811">this</ta>
            <ta e="T813" id="Seg_7472" s="T812">Nenets-GEN</ta>
            <ta e="T814" id="Seg_7473" s="T813">reindeer-ACC</ta>
            <ta e="T815" id="Seg_7474" s="T814">shepherd-3PL</ta>
            <ta e="T817" id="Seg_7475" s="T816">dog-GEN</ta>
            <ta e="T818" id="Seg_7476" s="T817">leg-ACC</ta>
            <ta e="T819" id="Seg_7477" s="T818">step-CO-3PL</ta>
            <ta e="T820" id="Seg_7478" s="T819">dog.[NOM]</ta>
            <ta e="T821" id="Seg_7479" s="T820">dogs.yelp-INCH-CO.[3SG.S]</ta>
            <ta e="T823" id="Seg_7480" s="T822">old.man.[NOM]</ta>
            <ta e="T824" id="Seg_7481" s="T823">such-ADJZ</ta>
            <ta e="T825" id="Seg_7482" s="T824">open-RFL.PFV.[3SG.S]</ta>
            <ta e="T826" id="Seg_7483" s="T825">hare-ADJZ</ta>
            <ta e="T827" id="Seg_7484" s="T826">clothing.[NOM]</ta>
            <ta e="T828" id="Seg_7485" s="T827">old.man.[NOM]</ta>
            <ta e="T829" id="Seg_7486" s="T828">as.if</ta>
            <ta e="T831" id="Seg_7487" s="T830">dog.[NOM]-1SG</ta>
            <ta e="T832" id="Seg_7488" s="T831">what.[NOM]</ta>
            <ta e="T833" id="Seg_7489" s="T832">eat-PST.NAR-3SG.O</ta>
            <ta e="T834" id="Seg_7490" s="T833">whether</ta>
            <ta e="T835" id="Seg_7491" s="T834">reindeer.[NOM]</ta>
            <ta e="T836" id="Seg_7492" s="T835">chop-MOM-CO-3SG.O</ta>
            <ta e="T838" id="Seg_7493" s="T837">%%</ta>
            <ta e="T839" id="Seg_7494" s="T838">young</ta>
            <ta e="T840" id="Seg_7495" s="T839">son-GEN</ta>
            <ta e="T841" id="Seg_7496" s="T840">woman.[NOM]</ta>
            <ta e="T842" id="Seg_7497" s="T841">outwards</ta>
            <ta e="T843" id="Seg_7498" s="T842">run.[3SG.S]</ta>
            <ta e="T845" id="Seg_7499" s="T844">this</ta>
            <ta e="T846" id="Seg_7500" s="T845">hare-ADJZ</ta>
            <ta e="T847" id="Seg_7501" s="T846">clothing.[NOM]</ta>
            <ta e="T848" id="Seg_7502" s="T847">old.man-GEN.3SG</ta>
            <ta e="T849" id="Seg_7503" s="T848">young</ta>
            <ta e="T850" id="Seg_7504" s="T849">son.[NOM]-3SG</ta>
            <ta e="T852" id="Seg_7505" s="T851">outwards</ta>
            <ta e="T853" id="Seg_7506" s="T852">hardly</ta>
            <ta e="T854" id="Seg_7507" s="T853">run.[3SG.S]</ta>
            <ta e="T855" id="Seg_7508" s="T854">what.[NOM]</ta>
            <ta e="T856" id="Seg_7509" s="T855">become.[3SG.S]</ta>
            <ta e="T857" id="Seg_7510" s="T856">husband.[NOM]-3SG</ta>
            <ta e="T859" id="Seg_7511" s="T858">this</ta>
            <ta e="T860" id="Seg_7512" s="T859">what.[NOM]</ta>
            <ta e="T861" id="Seg_7513" s="T860">human.being-EP-PL.[NOM]</ta>
            <ta e="T862" id="Seg_7514" s="T861">feel-CO.[3SG.S]</ta>
            <ta e="T864" id="Seg_7515" s="T863">father.[NOM]</ta>
            <ta e="T865" id="Seg_7516" s="T864">old.man.[NOM]-3SG</ta>
            <ta e="T866" id="Seg_7517" s="T865">this</ta>
            <ta e="T867" id="Seg_7518" s="T866">open-RFL.PFV-HAB.[3SG.S]</ta>
            <ta e="T868" id="Seg_7519" s="T867">this</ta>
            <ta e="T869" id="Seg_7520" s="T868">house-LOC</ta>
            <ta e="T871" id="Seg_7521" s="T870">it.is.said</ta>
            <ta e="T872" id="Seg_7522" s="T871">we.PL.NOM</ta>
            <ta e="T873" id="Seg_7523" s="T872">it.is.said</ta>
            <ta e="T874" id="Seg_7524" s="T873">tent.[NOM]-1PL</ta>
            <ta e="T875" id="Seg_7525" s="T874">it.is.said</ta>
            <ta e="T876" id="Seg_7526" s="T875">we.PL.NOM</ta>
            <ta e="T877" id="Seg_7527" s="T876">fire-INSTR</ta>
            <ta e="T878" id="Seg_7528" s="T877">set.fire-PST-1PL</ta>
            <ta e="T879" id="Seg_7529" s="T878">as.if</ta>
            <ta e="T881" id="Seg_7530" s="T880">grandfather.[NOM]</ta>
            <ta e="T882" id="Seg_7531" s="T881">it.is.said</ta>
            <ta e="T883" id="Seg_7532" s="T882">such-ADJZ</ta>
            <ta e="T884" id="Seg_7533" s="T883">we.PL.ACC</ta>
            <ta e="T894" id="Seg_7534" s="T893">come.on</ta>
            <ta e="T895" id="Seg_7535" s="T894">forward</ta>
            <ta e="T896" id="Seg_7536" s="T895">speak-IMP.2SG.O</ta>
            <ta e="T898" id="Seg_7537" s="T897">then</ta>
            <ta e="T899" id="Seg_7538" s="T898">then</ta>
            <ta e="T900" id="Seg_7539" s="T899">INFER</ta>
            <ta e="T901" id="Seg_7540" s="T900">live-PST.NAR-INFER-3PL</ta>
            <ta e="T902" id="Seg_7541" s="T901">there</ta>
            <ta e="T903" id="Seg_7542" s="T902">here</ta>
            <ta e="T904" id="Seg_7543" s="T903">supposedly</ta>
            <ta e="T905" id="Seg_7544" s="T904">again</ta>
            <ta e="T906" id="Seg_7545" s="T905">away</ta>
            <ta e="T907" id="Seg_7546" s="T906">leave-PST.NAR-3PL</ta>
            <ta e="T908" id="Seg_7547" s="T907">this</ta>
            <ta e="T909" id="Seg_7548" s="T908">Selkup-EP-PL.[NOM]</ta>
            <ta e="T910" id="Seg_7549" s="T909">that</ta>
            <ta e="T912" id="Seg_7550" s="T911">supposedly</ta>
            <ta e="T913" id="Seg_7551" s="T912">Nenets-ADJZ</ta>
            <ta e="T914" id="Seg_7552" s="T913">earth-GEN</ta>
            <ta e="T915" id="Seg_7553" s="T914">reindeer-ACC</ta>
            <ta e="T916" id="Seg_7554" s="T915">shepherd-PST.NAR-INFER-3PL</ta>
            <ta e="T918" id="Seg_7555" s="T917">and</ta>
            <ta e="T919" id="Seg_7556" s="T918">this</ta>
            <ta e="T920" id="Seg_7557" s="T919">Selkup.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T11" id="Seg_7558" s="T10">ну.ка</ta>
            <ta e="T12" id="Seg_7559" s="T11">сказать-IMP.2SG.O</ta>
            <ta e="T13" id="Seg_7560" s="T12">затем</ta>
            <ta e="T14" id="Seg_7561" s="T13">другой</ta>
            <ta e="T15" id="Seg_7562" s="T14">что.[NOM]</ta>
            <ta e="T17" id="Seg_7563" s="T16">нечто.[NOM]</ta>
            <ta e="T18" id="Seg_7564" s="T17">тот</ta>
            <ta e="T19" id="Seg_7565" s="T18">заяц-ADJZ</ta>
            <ta e="T20" id="Seg_7566" s="T19">одежда.[NOM]</ta>
            <ta e="T21" id="Seg_7567" s="T20">старик.[NOM]</ta>
            <ta e="T22" id="Seg_7568" s="T21">INDEF3</ta>
            <ta e="T23" id="Seg_7569" s="T22">сказать-%%-1SG.S</ta>
            <ta e="T25" id="Seg_7570" s="T24">заяц-ADJZ</ta>
            <ta e="T26" id="Seg_7571" s="T25">одежда.[NOM]</ta>
            <ta e="T27" id="Seg_7572" s="T26">старик.[NOM]</ta>
            <ta e="T28" id="Seg_7573" s="T27">тот</ta>
            <ta e="T29" id="Seg_7574" s="T28">в.старину</ta>
            <ta e="T30" id="Seg_7575" s="T29">семь</ta>
            <ta e="T31" id="Seg_7576" s="T30">ребенок.[NOM]-3SG</ta>
            <ta e="T32" id="Seg_7577" s="T31">быть-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T34" id="Seg_7578" s="T33">и</ta>
            <ta e="T35" id="Seg_7579" s="T34">семь</ta>
            <ta e="T36" id="Seg_7580" s="T35">жена.сына.[NOM]-3SG</ta>
            <ta e="T38" id="Seg_7581" s="T37">утро-ADV.LOC</ta>
            <ta e="T39" id="Seg_7582" s="T38">прийти-CO-3PL</ta>
            <ta e="T41" id="Seg_7583" s="T40">ненец-PL.[NOM]</ta>
            <ta e="T42" id="Seg_7584" s="T41">на.север-ADV.EL</ta>
            <ta e="T43" id="Seg_7585" s="T42">вверх.по.течению</ta>
            <ta e="T44" id="Seg_7586" s="T43">прийти-CO-3PL</ta>
            <ta e="T45" id="Seg_7587" s="T44">он(а)-EP-ACC</ta>
            <ta e="T46" id="Seg_7588" s="T45">убить-SUP.2/3SG</ta>
            <ta e="T48" id="Seg_7589" s="T47">человек-PL.[NOM]-3SG</ta>
            <ta e="T49" id="Seg_7590" s="T48">сын-PL.[NOM]-3SG</ta>
            <ta e="T50" id="Seg_7591" s="T49">в.лес</ta>
            <ta e="T51" id="Seg_7592" s="T50">отправиться-PST.NAR-3PL</ta>
            <ta e="T52" id="Seg_7593" s="T51">белка-CAP-CVB</ta>
            <ta e="T54" id="Seg_7594" s="T53">белка-VBLZ-CVB</ta>
            <ta e="T55" id="Seg_7595" s="T54">в.лес</ta>
            <ta e="T56" id="Seg_7596" s="T55">отправиться-PST.NAR-3PL</ta>
            <ta e="T57" id="Seg_7597" s="T56">сейчас</ta>
            <ta e="T58" id="Seg_7598" s="T57">человек.[NOM]</ta>
            <ta e="T59" id="Seg_7599" s="T58">сейчас</ta>
            <ta e="T60" id="Seg_7600" s="T59">зима-ADV.LOC</ta>
            <ta e="T61" id="Seg_7601" s="T60">теперь</ta>
            <ta e="T62" id="Seg_7602" s="T61">как</ta>
            <ta e="T63" id="Seg_7603" s="T62">быть-HAB.[3SG.S]</ta>
            <ta e="T64" id="Seg_7604" s="T63">зима-ADV.LOC</ta>
            <ta e="T66" id="Seg_7605" s="T65">тот</ta>
            <ta e="T67" id="Seg_7606" s="T66">парень-ACC</ta>
            <ta e="T68" id="Seg_7607" s="T67">селькуп-EP-ADJZ</ta>
            <ta e="T69" id="Seg_7608" s="T68">парень-ACC</ta>
            <ta e="T70" id="Seg_7609" s="T69">содержать-DUR-INFER.[3SG.S]</ta>
            <ta e="T71" id="Seg_7610" s="T70">тот</ta>
            <ta e="T72" id="Seg_7611" s="T71">ненец.[NOM]</ta>
            <ta e="T74" id="Seg_7612" s="T73">этот</ta>
            <ta e="T75" id="Seg_7613" s="T74">война-ADV.ILL-OBL.3SG</ta>
            <ta e="T77" id="Seg_7614" s="T76">заяц-ADJZ</ta>
            <ta e="T78" id="Seg_7615" s="T77">одежда.[NOM]</ta>
            <ta e="T79" id="Seg_7616" s="T78">старик.[NOM]</ta>
            <ta e="T80" id="Seg_7617" s="T79">будто</ta>
            <ta e="T81" id="Seg_7618" s="T80">завтра</ta>
            <ta e="T82" id="Seg_7619" s="T81">будто</ta>
            <ta e="T83" id="Seg_7620" s="T82">мы.PL.NOM</ta>
            <ta e="T84" id="Seg_7621" s="T83">прийти-FUT-1PL</ta>
            <ta e="T85" id="Seg_7622" s="T84">он(а)-EP-GEN</ta>
            <ta e="T86" id="Seg_7623" s="T85">чум-ILL</ta>
            <ta e="T87" id="Seg_7624" s="T86">вы.PL.NOM</ta>
            <ta e="T88" id="Seg_7625" s="T87">будто</ta>
            <ta e="T89" id="Seg_7626" s="T88">отправиться-IPFV-IMP.2PL</ta>
            <ta e="T90" id="Seg_7627" s="T89">туда</ta>
            <ta e="T92" id="Seg_7628" s="T91">ребенок-DU.[NOM]</ta>
            <ta e="T93" id="Seg_7629" s="T92">домой</ta>
            <ta e="T94" id="Seg_7630" s="T93">уйти-CVB</ta>
            <ta e="T95" id="Seg_7631" s="T94">так</ta>
            <ta e="T96" id="Seg_7632" s="T95">сказать-CO-3SG.O</ta>
            <ta e="T97" id="Seg_7633" s="T96">сирота-ADJZ</ta>
            <ta e="T98" id="Seg_7634" s="T97">ребенок-DU.[NOM]</ta>
            <ta e="T99" id="Seg_7635" s="T98">ненец.[NOM]</ta>
            <ta e="T100" id="Seg_7636" s="T99">держать-DUR-PTCP.PRS</ta>
            <ta e="T102" id="Seg_7637" s="T101">этот</ta>
            <ta e="T103" id="Seg_7638" s="T102">селькуп-EP-ADJZ</ta>
            <ta e="T104" id="Seg_7639" s="T103">ребенок-DU.[NOM]</ta>
            <ta e="T106" id="Seg_7640" s="T105">притихнуть-TR-IMP.2SG.S</ta>
            <ta e="T107" id="Seg_7641" s="T106">притихнуть-TR-IMP.2SG.S</ta>
            <ta e="T108" id="Seg_7642" s="T107">что.ли</ta>
            <ta e="T109" id="Seg_7643" s="T108">что.ли</ta>
            <ta e="T110" id="Seg_7644" s="T109">собака-ACC</ta>
            <ta e="T111" id="Seg_7645" s="T110">что.ли</ta>
            <ta e="T112" id="Seg_7646" s="T111">бросать-TR-MOM-TR.[3SG.S]</ta>
            <ta e="T114" id="Seg_7647" s="T113">%%-2SG.O</ta>
            <ta e="T116" id="Seg_7648" s="T115">потом</ta>
            <ta e="T117" id="Seg_7649" s="T116">потом</ta>
            <ta e="T118" id="Seg_7650" s="T117">старик.[NOM]</ta>
            <ta e="T119" id="Seg_7651" s="T118">куда</ta>
            <ta e="T120" id="Seg_7652" s="T119">куда.деваться-FUT.[3SG.S]</ta>
            <ta e="T122" id="Seg_7653" s="T121">старик.[NOM]</ta>
            <ta e="T123" id="Seg_7654" s="T122">вроде</ta>
            <ta e="T124" id="Seg_7655" s="T123">только</ta>
            <ta e="T125" id="Seg_7656" s="T124">INFER</ta>
            <ta e="T126" id="Seg_7657" s="T125">лежать-INFER.[3SG.S]</ta>
            <ta e="T128" id="Seg_7658" s="T127">старик.[NOM]</ta>
            <ta e="T129" id="Seg_7659" s="T128">такой-ADVZ</ta>
            <ta e="T130" id="Seg_7660" s="T129">вот</ta>
            <ta e="T131" id="Seg_7661" s="T130">стать-PST.[3SG.S]</ta>
            <ta e="T132" id="Seg_7662" s="T131">этот</ta>
            <ta e="T133" id="Seg_7663" s="T132">заяц-ADJZ</ta>
            <ta e="T134" id="Seg_7664" s="T133">одежда-ADJZ</ta>
            <ta e="T135" id="Seg_7665" s="T134">старик.[NOM]</ta>
            <ta e="T137" id="Seg_7666" s="T136">такой-ADVZ</ta>
            <ta e="T138" id="Seg_7667" s="T137">стать.[3SG.S]</ta>
            <ta e="T139" id="Seg_7668" s="T138">жена.сына-OBL.3SG-ADJZ</ta>
            <ta e="T140" id="Seg_7669" s="T139">нечто-ALL</ta>
            <ta e="T141" id="Seg_7670" s="T140">еда-ACC</ta>
            <ta e="T142" id="Seg_7671" s="T141">принести-IMP.2PL.S</ta>
            <ta e="T143" id="Seg_7672" s="T142">вниз</ta>
            <ta e="T145" id="Seg_7673" s="T144">жир.[NOM]</ta>
            <ta e="T146" id="Seg_7674" s="T145">еда-ACC</ta>
            <ta e="T147" id="Seg_7675" s="T146">принести-IMP.2PL.S</ta>
            <ta e="T148" id="Seg_7676" s="T147">interrog.pron.stem-ADJZ</ta>
            <ta e="T149" id="Seg_7677" s="T148">%%</ta>
            <ta e="T150" id="Seg_7678" s="T149">еда-ACC</ta>
            <ta e="T151" id="Seg_7679" s="T150">QUEST-GEN</ta>
            <ta e="T152" id="Seg_7680" s="T151">всё</ta>
            <ta e="T153" id="Seg_7681" s="T152">что.[NOM]</ta>
            <ta e="T154" id="Seg_7682" s="T153">принести-IMP.2PL.S</ta>
            <ta e="T156" id="Seg_7683" s="T155">ненец-PL.[NOM]</ta>
            <ta e="T157" id="Seg_7684" s="T156">завтра</ta>
            <ta e="T158" id="Seg_7685" s="T157">прийти-FUT-3PL</ta>
            <ta e="T159" id="Seg_7686" s="T158">съесть-EP-FRQ-FUT-3PL</ta>
            <ta e="T161" id="Seg_7687" s="T160">старик.[NOM]</ta>
            <ta e="T162" id="Seg_7688" s="T161">так</ta>
            <ta e="T163" id="Seg_7689" s="T162">сказать-CO-3SG.O</ta>
            <ta e="T165" id="Seg_7690" s="T164">еда-ACC</ta>
            <ta e="T166" id="Seg_7691" s="T165">принести-3PL</ta>
            <ta e="T168" id="Seg_7692" s="T167">тот</ta>
            <ta e="T169" id="Seg_7693" s="T168">этот</ta>
            <ta e="T170" id="Seg_7694" s="T169">трава-INSTR</ta>
            <ta e="T171" id="Seg_7695" s="T170">этот</ta>
            <ta e="T172" id="Seg_7696" s="T171">дом.[NOM]</ta>
            <ta e="T173" id="Seg_7697" s="T172">нутро.[NOM]</ta>
            <ta e="T174" id="Seg_7698" s="T173">новый</ta>
            <ta e="T175" id="Seg_7699" s="T174">трава-INSTR</ta>
            <ta e="T176" id="Seg_7700" s="T175">постелить-3PL</ta>
            <ta e="T177" id="Seg_7701" s="T176">тот</ta>
            <ta e="T179" id="Seg_7702" s="T178">старик.[NOM]</ta>
            <ta e="T180" id="Seg_7703" s="T179">ненец-ACC</ta>
            <ta e="T181" id="Seg_7704" s="T180">еда-TR-INF-собраться-CO-3SG.O</ta>
            <ta e="T183" id="Seg_7705" s="T182">INTERJ</ta>
            <ta e="T184" id="Seg_7706" s="T183">край-ADJZ</ta>
            <ta e="T185" id="Seg_7707" s="T184">день.[NOM]</ta>
            <ta e="T186" id="Seg_7708" s="T185">ненец-PL.[NOM]</ta>
            <ta e="T187" id="Seg_7709" s="T186">этот</ta>
            <ta e="T188" id="Seg_7710" s="T187">RFL</ta>
            <ta e="T189" id="Seg_7711" s="T188">три-десять-EP-ADJZ</ta>
            <ta e="T190" id="Seg_7712" s="T189">человек-EP-ADJZ</ta>
            <ta e="T191" id="Seg_7713" s="T190">войско.[NOM]</ta>
            <ta e="T193" id="Seg_7714" s="T192">давно</ta>
            <ta e="T194" id="Seg_7715" s="T193">вроде</ta>
            <ta e="T195" id="Seg_7716" s="T194">кончиться-PTCP.PST</ta>
            <ta e="T196" id="Seg_7717" s="T195">чум-LOC</ta>
            <ta e="T197" id="Seg_7718" s="T196">заяц-ADJZ</ta>
            <ta e="T198" id="Seg_7719" s="T197">одежда.[NOM]</ta>
            <ta e="T199" id="Seg_7720" s="T198">старик.[NOM]</ta>
            <ta e="T200" id="Seg_7721" s="T199">сам.3SG</ta>
            <ta e="T201" id="Seg_7722" s="T200">остаться.[3SG.S]</ta>
            <ta e="T202" id="Seg_7723" s="T201">жена.сына-PL-OBL.3SG-COM</ta>
            <ta e="T204" id="Seg_7724" s="T203">семь</ta>
            <ta e="T205" id="Seg_7725" s="T204">трава-INSTR</ta>
            <ta e="T206" id="Seg_7726" s="T205">постелить-CAUS-CO-3SG.O</ta>
            <ta e="T208" id="Seg_7727" s="T207">старик.[NOM]</ta>
            <ta e="T209" id="Seg_7728" s="T208">такой-ADVZ</ta>
            <ta e="T210" id="Seg_7729" s="T209">стать.[3SG.S]</ta>
            <ta e="T212" id="Seg_7730" s="T211">дом.[NOM]</ta>
            <ta e="T213" id="Seg_7731" s="T212">RFL</ta>
            <ta e="T214" id="Seg_7732" s="T213">занести-TR-3PL</ta>
            <ta e="T215" id="Seg_7733" s="T214">вроде</ta>
            <ta e="T216" id="Seg_7734" s="T215">тот</ta>
            <ta e="T217" id="Seg_7735" s="T216">этот</ta>
            <ta e="T218" id="Seg_7736" s="T217">ненец-PL.[NOM]</ta>
            <ta e="T220" id="Seg_7737" s="T219">съесть-EP-FRQ-INF-собраться-CO-GEN</ta>
            <ta e="T222" id="Seg_7738" s="T221">еда.[NOM]</ta>
            <ta e="T223" id="Seg_7739" s="T222">найти-INFER-3PL</ta>
            <ta e="T225" id="Seg_7740" s="T224">жена.сына-PL.[NOM]-3SG</ta>
            <ta e="T226" id="Seg_7741" s="T225">чай.[NOM]</ta>
            <ta e="T227" id="Seg_7742" s="T226">этот</ta>
            <ta e="T228" id="Seg_7743" s="T227">свариться-CAUS-MULO-CO-3PL</ta>
            <ta e="T229" id="Seg_7744" s="T228">вниз-ADV.LOC</ta>
            <ta e="T230" id="Seg_7745" s="T229">QUEST-GEN</ta>
            <ta e="T231" id="Seg_7746" s="T230">в.меру-ADJZ</ta>
            <ta e="T232" id="Seg_7747" s="T231">еда.[NOM]</ta>
            <ta e="T233" id="Seg_7748" s="T232">жир.[NOM]</ta>
            <ta e="T234" id="Seg_7749" s="T233">жир.[NOM]</ta>
            <ta e="T235" id="Seg_7750" s="T234">QUEST-GEN</ta>
            <ta e="T236" id="Seg_7751" s="T235">в.меру-ADJZ</ta>
            <ta e="T237" id="Seg_7752" s="T236">что.[NOM]</ta>
            <ta e="T238" id="Seg_7753" s="T237">тот</ta>
            <ta e="T239" id="Seg_7754" s="T238">сохнуть-PTCP.PST</ta>
            <ta e="T240" id="Seg_7755" s="T239">еда.[NOM]</ta>
            <ta e="T242" id="Seg_7756" s="T241">старик.[NOM]</ta>
            <ta e="T243" id="Seg_7757" s="T242">так</ta>
            <ta e="T244" id="Seg_7758" s="T243">сказать-CO-3SG.O</ta>
            <ta e="T245" id="Seg_7759" s="T244">жена-ILL</ta>
            <ta e="T246" id="Seg_7760" s="T245">заяц-ADJZ</ta>
            <ta e="T247" id="Seg_7761" s="T246">одежда.[NOM]</ta>
            <ta e="T248" id="Seg_7762" s="T247">старик.[NOM]</ta>
            <ta e="T249" id="Seg_7763" s="T248">жена.[NOM]-3SG</ta>
            <ta e="T250" id="Seg_7764" s="T249">тоже</ta>
            <ta e="T251" id="Seg_7765" s="T250">быть-PST.NAR.[3SG.S]</ta>
            <ta e="T253" id="Seg_7766" s="T252">%%-2SG.O</ta>
            <ta e="T255" id="Seg_7767" s="T254">жена.сына-PL-ACC-3SG</ta>
            <ta e="T256" id="Seg_7768" s="T255">так</ta>
            <ta e="T257" id="Seg_7769" s="T256">идти-TR-CO-3SG.O</ta>
            <ta e="T258" id="Seg_7770" s="T257">вы.PL.NOM</ta>
            <ta e="T259" id="Seg_7771" s="T258">наружу</ta>
            <ta e="T260" id="Seg_7772" s="T259">выйти-IMP.2PL</ta>
            <ta e="T262" id="Seg_7773" s="T261">ребенок-PL.[NOM]</ta>
            <ta e="T263" id="Seg_7774" s="T262">%%</ta>
            <ta e="T264" id="Seg_7775" s="T263">наружу</ta>
            <ta e="T265" id="Seg_7776" s="T264">принести-TR-IMP.2SG.O</ta>
            <ta e="T267" id="Seg_7777" s="T266">старик.[NOM]</ta>
            <ta e="T268" id="Seg_7778" s="T267">долго</ta>
            <ta e="T269" id="Seg_7779" s="T268">находиться.[3SG.S]</ta>
            <ta e="T270" id="Seg_7780" s="T269">человек-PL-ACC-3SG</ta>
            <ta e="T271" id="Seg_7781" s="T270">еда-TR-DUR-CVB</ta>
            <ta e="T273" id="Seg_7782" s="T272">один</ta>
            <ta e="T274" id="Seg_7783" s="T273">целый</ta>
            <ta e="T275" id="Seg_7784" s="T274">середина-ADV.LOC</ta>
            <ta e="T276" id="Seg_7785" s="T275">прийти-PTCP.PST</ta>
            <ta e="T277" id="Seg_7786" s="T276">вниз-ADV.LOC</ta>
            <ta e="T278" id="Seg_7787" s="T277">человек-EP-PL.[NOM]</ta>
            <ta e="T279" id="Seg_7788" s="T278">съесть-EP-FRQ-CO-3PL</ta>
            <ta e="T281" id="Seg_7789" s="T280">вниз</ta>
            <ta e="T282" id="Seg_7790" s="T281">этот</ta>
            <ta e="T283" id="Seg_7791" s="T282">печь-EP-ADJZ</ta>
            <ta e="T284" id="Seg_7792" s="T283">огонь-ILL</ta>
            <ta e="T285" id="Seg_7793" s="T284">настолько</ta>
            <ta e="T286" id="Seg_7794" s="T285">дерево-INSTR</ta>
            <ta e="T287" id="Seg_7795" s="T286">%%-CO-3SG.O</ta>
            <ta e="T288" id="Seg_7796" s="T287">зажечь-CO-3SG.O</ta>
            <ta e="T289" id="Seg_7797" s="T288">жена.сына-PL-ACC-3SG</ta>
            <ta e="T290" id="Seg_7798" s="T289">NEG</ta>
            <ta e="T291" id="Seg_7799" s="T290">зачем</ta>
            <ta e="T292" id="Seg_7800" s="T291">наружу</ta>
            <ta e="T293" id="Seg_7801" s="T292">послать-PST.NAR-3SG.O</ta>
            <ta e="T294" id="Seg_7802" s="T293">пространство.снаружи-LOC</ta>
            <ta e="T295" id="Seg_7803" s="T294">огонь.[NOM]</ta>
            <ta e="T296" id="Seg_7804" s="T295">зажечь-MOM-FUT-3PL</ta>
            <ta e="T298" id="Seg_7805" s="T297">огонь.[NOM]</ta>
            <ta e="T299" id="Seg_7806" s="T298">зажечь-INCH-PST.NAR-INFER-3PL</ta>
            <ta e="T300" id="Seg_7807" s="T299">тот</ta>
            <ta e="T301" id="Seg_7808" s="T300">береста-ADJZ</ta>
            <ta e="T302" id="Seg_7809" s="T301">факел.[NOM]</ta>
            <ta e="T303" id="Seg_7810" s="T302">голова-DIM.[NOM]</ta>
            <ta e="T304" id="Seg_7811" s="T303">такой-ADVZ</ta>
            <ta e="T305" id="Seg_7812" s="T304">сохнуть-CAUS-PTCP.PST</ta>
            <ta e="T306" id="Seg_7813" s="T305">давно</ta>
            <ta e="T307" id="Seg_7814" s="T306">дом.[NOM]</ta>
            <ta e="T308" id="Seg_7815" s="T307">огонь-ACC-3SG</ta>
            <ta e="T310" id="Seg_7816" s="T309">вы.PL.NOM</ta>
            <ta e="T311" id="Seg_7817" s="T310">вот.он.есть</ta>
            <ta e="T312" id="Seg_7818" s="T311">я.NOM</ta>
            <ta e="T313" id="Seg_7819" s="T312">вот</ta>
            <ta e="T314" id="Seg_7820" s="T313">выйти-FUT-1SG.S</ta>
            <ta e="T316" id="Seg_7821" s="T315">человек-EP-PL.[NOM]</ta>
            <ta e="T317" id="Seg_7822" s="T316">съесть-EP-FRQ-PST-3PL</ta>
            <ta e="T319" id="Seg_7823" s="T318">съесть-EP-FRQ-CO-3PL</ta>
            <ta e="T321" id="Seg_7824" s="T320">ненец-PL.[NOM]</ta>
            <ta e="T322" id="Seg_7825" s="T321">так</ta>
            <ta e="T323" id="Seg_7826" s="T322">сказать-3PL</ta>
            <ta e="T324" id="Seg_7827" s="T323">дедушка.[NOM]</ta>
            <ta e="T325" id="Seg_7828" s="T324">печь.[NOM]-2SG</ta>
            <ta e="T326" id="Seg_7829" s="T325">столько</ta>
            <ta e="T327" id="Seg_7830" s="T326">почему</ta>
            <ta e="T328" id="Seg_7831" s="T327">дерево-INSTR</ta>
            <ta e="T329" id="Seg_7832" s="T328">%%-2SG.O</ta>
            <ta e="T331" id="Seg_7833" s="T330">NEG</ta>
            <ta e="T332" id="Seg_7834" s="T331">печь.[NOM]</ta>
            <ta e="T333" id="Seg_7835" s="T332">огонь.[NOM]</ta>
            <ta e="T334" id="Seg_7836" s="T333">съесть-TR-CO-3SG.O</ta>
            <ta e="T336" id="Seg_7837" s="T335">NEG</ta>
            <ta e="T337" id="Seg_7838" s="T336">будто</ta>
            <ta e="T338" id="Seg_7839" s="T337">я.NOM</ta>
            <ta e="T339" id="Seg_7840" s="T338">будто</ta>
            <ta e="T340" id="Seg_7841" s="T339">нечто.[NOM]</ta>
            <ta e="T342" id="Seg_7842" s="T341">он(а).[NOM]</ta>
            <ta e="T343" id="Seg_7843" s="T342">NEG</ta>
            <ta e="T344" id="Seg_7844" s="T343">съесть-IPFV-3SG.O</ta>
            <ta e="T345" id="Seg_7845" s="T344">он(а).[NOM]</ta>
            <ta e="T346" id="Seg_7846" s="T345">NEG</ta>
            <ta e="T347" id="Seg_7847" s="T346">съесть-IPFV-3SG.O</ta>
            <ta e="T348" id="Seg_7848" s="T347">такой-ADJZ</ta>
            <ta e="T349" id="Seg_7849" s="T348">находиться-IPFV.[3SG.S]</ta>
            <ta e="T350" id="Seg_7850" s="T349">такой-ADJZ</ta>
            <ta e="T351" id="Seg_7851" s="T350">заяц-ADJZ</ta>
            <ta e="T352" id="Seg_7852" s="T351">одежда.[NOM]</ta>
            <ta e="T353" id="Seg_7853" s="T352">лохмотья.[NOM]</ta>
            <ta e="T354" id="Seg_7854" s="T353">кусок.[NOM]</ta>
            <ta e="T355" id="Seg_7855" s="T354">тот</ta>
            <ta e="T357" id="Seg_7856" s="T356">старый</ta>
            <ta e="T358" id="Seg_7857" s="T357">это-ACC</ta>
            <ta e="T359" id="Seg_7858" s="T358">надеть-PST.NAR-INFER-3SG.O</ta>
            <ta e="T361" id="Seg_7859" s="T360">очаг-GEN.3SG</ta>
            <ta e="T362" id="Seg_7860" s="T361">рядом-LOC</ta>
            <ta e="T363" id="Seg_7861" s="T362">находиться.[3SG.S]</ta>
            <ta e="T364" id="Seg_7862" s="T363">дверь-GEN-3SG</ta>
            <ta e="T365" id="Seg_7863" s="T364">рядом-LOC</ta>
            <ta e="T367" id="Seg_7864" s="T366">один</ta>
            <ta e="T368" id="Seg_7865" s="T367">целый</ta>
            <ta e="T369" id="Seg_7866" s="T368">середина-ADV.LOC</ta>
            <ta e="T370" id="Seg_7867" s="T369">пространство.снаружи-EL</ta>
            <ta e="T371" id="Seg_7868" s="T370">чум-ILL</ta>
            <ta e="T372" id="Seg_7869" s="T371">тот</ta>
            <ta e="T373" id="Seg_7870" s="T372">огонь-ADJZ</ta>
            <ta e="T374" id="Seg_7871" s="T373">факел-SNGL.[NOM]</ta>
            <ta e="T375" id="Seg_7872" s="T374">попасть-INFER.[3SG.S]</ta>
            <ta e="T377" id="Seg_7873" s="T376">INFER</ta>
            <ta e="T378" id="Seg_7874" s="T377">побежать-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T379" id="Seg_7875" s="T378">тот</ta>
            <ta e="T380" id="Seg_7876" s="T379">дверь-ILL</ta>
            <ta e="T381" id="Seg_7877" s="T380">тот</ta>
            <ta e="T382" id="Seg_7878" s="T381">такой</ta>
            <ta e="T383" id="Seg_7879" s="T382">жена.сына-PL.[NOM]-3SG</ta>
            <ta e="T385" id="Seg_7880" s="T384">ненец-PL.[NOM]</ta>
            <ta e="T386" id="Seg_7881" s="T385">едва</ta>
            <ta e="T387" id="Seg_7882" s="T386">схватить-PST.NAR-3PL</ta>
            <ta e="T388" id="Seg_7883" s="T387">такой-ADJZ</ta>
            <ta e="T389" id="Seg_7884" s="T388">старик-ACC</ta>
            <ta e="T390" id="Seg_7885" s="T389">только</ta>
            <ta e="T391" id="Seg_7886" s="T390">исключительный</ta>
            <ta e="T392" id="Seg_7887" s="T391">заяц-ADJZ</ta>
            <ta e="T393" id="Seg_7888" s="T392">одежда-GEN-3SG</ta>
            <ta e="T394" id="Seg_7889" s="T393">кусок.[NOM]</ta>
            <ta e="T395" id="Seg_7890" s="T394">рвать-MULO-PFV-CVB</ta>
            <ta e="T396" id="Seg_7891" s="T395">схватить-INFER-3PL</ta>
            <ta e="T397" id="Seg_7892" s="T396">дом-LOC</ta>
            <ta e="T399" id="Seg_7893" s="T398">вроде</ta>
            <ta e="T400" id="Seg_7894" s="T399">этот</ta>
            <ta e="T401" id="Seg_7895" s="T400">огонь-INSTR</ta>
            <ta e="T402" id="Seg_7896" s="T401">потом</ta>
            <ta e="T403" id="Seg_7897" s="T402">%%</ta>
            <ta e="T404" id="Seg_7898" s="T403">окно-GEN</ta>
            <ta e="T405" id="Seg_7899" s="T404">рот-GEN</ta>
            <ta e="T406" id="Seg_7900" s="T405">сквозь-PROL</ta>
            <ta e="T407" id="Seg_7901" s="T406">чум-ILL</ta>
            <ta e="T408" id="Seg_7902" s="T407">такой</ta>
            <ta e="T409" id="Seg_7903" s="T408">два-ADJZ</ta>
            <ta e="T410" id="Seg_7904" s="T409">сторона-ADJZ</ta>
            <ta e="T411" id="Seg_7905" s="T410">окно.[NOM]</ta>
            <ta e="T412" id="Seg_7906" s="T411">быть-PST.NAR.[3SG.S]</ta>
            <ta e="T414" id="Seg_7907" s="T413">что.ли.[NOM]</ta>
            <ta e="T415" id="Seg_7908" s="T414">большой</ta>
            <ta e="T416" id="Seg_7909" s="T415">дом.[NOM]-3SG</ta>
            <ta e="T417" id="Seg_7910" s="T416">быть-PST.NAR.[3SG.S]</ta>
            <ta e="T419" id="Seg_7911" s="T418">чум.[NOM]-3SG</ta>
            <ta e="T420" id="Seg_7912" s="T419">вот.он.есть</ta>
            <ta e="T421" id="Seg_7913" s="T420">земля-INSTR</ta>
            <ta e="T422" id="Seg_7914" s="T421">закрыть-DUR-PST.NAR.[3SG.S]</ta>
            <ta e="T423" id="Seg_7915" s="T422">тот</ta>
            <ta e="T424" id="Seg_7916" s="T423">стоять.торчком-EP-TR</ta>
            <ta e="T425" id="Seg_7917" s="T424">палка-ADJZ</ta>
            <ta e="T426" id="Seg_7918" s="T425">дом.[NOM]</ta>
            <ta e="T427" id="Seg_7919" s="T426">топор-GEN</ta>
            <ta e="T428" id="Seg_7920" s="T427">ручка-GEN</ta>
            <ta e="T429" id="Seg_7921" s="T428">высота-ADJZ</ta>
            <ta e="T430" id="Seg_7922" s="T429">закрыть-RES.[3SG.S]</ta>
            <ta e="T432" id="Seg_7923" s="T431">мол</ta>
            <ta e="T433" id="Seg_7924" s="T432">чум-LOC</ta>
            <ta e="T434" id="Seg_7925" s="T433">тот</ta>
            <ta e="T435" id="Seg_7926" s="T434">этот</ta>
            <ta e="T436" id="Seg_7927" s="T435">огонь-ACC</ta>
            <ta e="T437" id="Seg_7928" s="T436">INFER</ta>
            <ta e="T438" id="Seg_7929" s="T437">бросать-MOM-PST.NAR-INFER-3PL</ta>
            <ta e="T439" id="Seg_7930" s="T438">INFER</ta>
            <ta e="T440" id="Seg_7931" s="T439">ударить-PST.NAR-INFER-3SG.O</ta>
            <ta e="T441" id="Seg_7932" s="T440">вроде</ta>
            <ta e="T442" id="Seg_7933" s="T441">тот</ta>
            <ta e="T923" id="Seg_7934" s="T442">старик-ACC</ta>
            <ta e="T444" id="Seg_7935" s="T443">вроде</ta>
            <ta e="T445" id="Seg_7936" s="T444">этот</ta>
            <ta e="T446" id="Seg_7937" s="T445">съесть-3SG.S</ta>
            <ta e="T448" id="Seg_7938" s="T447">%%-2SG.O</ta>
            <ta e="T450" id="Seg_7939" s="T449">порох-INSTR</ta>
            <ta e="T451" id="Seg_7940" s="T450">что.ли</ta>
            <ta e="T452" id="Seg_7941" s="T451">он(а)</ta>
            <ta e="T453" id="Seg_7942" s="T452">будто</ta>
            <ta e="T454" id="Seg_7943" s="T453">сыпать-IPFV-PST.NAR-3SG.O</ta>
            <ta e="T455" id="Seg_7944" s="T454">этот</ta>
            <ta e="T456" id="Seg_7945" s="T455">этот</ta>
            <ta e="T457" id="Seg_7946" s="T456">трава-GEN</ta>
            <ta e="T458" id="Seg_7947" s="T457">под-ADV.LOC-ADJZ</ta>
            <ta e="T459" id="Seg_7948" s="T458">сторона-ADV.LOC</ta>
            <ta e="T460" id="Seg_7949" s="T459">такой</ta>
            <ta e="T461" id="Seg_7950" s="T460">новый</ta>
            <ta e="T462" id="Seg_7951" s="T461">сено.[NOM]</ta>
            <ta e="T463" id="Seg_7952" s="T462">под-ADV.LOC-ADJZ</ta>
            <ta e="T464" id="Seg_7953" s="T463">сторона-ADV.LOC</ta>
            <ta e="T466" id="Seg_7954" s="T465">этот</ta>
            <ta e="T467" id="Seg_7955" s="T466">огонь.[NOM]</ta>
            <ta e="T468" id="Seg_7956" s="T467">INFER</ta>
            <ta e="T469" id="Seg_7957" s="T468">ударить-PST.NAR-INFER-3SG.O</ta>
            <ta e="T470" id="Seg_7958" s="T469">тот</ta>
            <ta e="T471" id="Seg_7959" s="T470">три-десять-EP-ADJZ</ta>
            <ta e="T472" id="Seg_7960" s="T471">человек-EP-ADJZ</ta>
            <ta e="T473" id="Seg_7961" s="T472">войско-ACC</ta>
            <ta e="T474" id="Seg_7962" s="T473">всё</ta>
            <ta e="T475" id="Seg_7963" s="T474">огонь.[NOM]</ta>
            <ta e="T476" id="Seg_7964" s="T475">съесть-PST.NAR-3SG.O</ta>
            <ta e="T478" id="Seg_7965" s="T477">бок-ADV.LOC</ta>
            <ta e="T479" id="Seg_7966" s="T478">упасть-3PL</ta>
            <ta e="T480" id="Seg_7967" s="T479">вперёд-ADV.LOC</ta>
            <ta e="T482" id="Seg_7968" s="T481">вперёд</ta>
            <ta e="T483" id="Seg_7969" s="T482">наружу</ta>
            <ta e="T484" id="Seg_7970" s="T483">старик.[NOM]</ta>
            <ta e="T485" id="Seg_7971" s="T484">наружу</ta>
            <ta e="T486" id="Seg_7972" s="T485">побежать.[3SG.S]</ta>
            <ta e="T487" id="Seg_7973" s="T486">дверь-GEN</ta>
            <ta e="T488" id="Seg_7974" s="T487">отверстие-ADV.LOC</ta>
            <ta e="T489" id="Seg_7975" s="T488">топор-ADJZ</ta>
            <ta e="T490" id="Seg_7976" s="T489">рука.[NOM]</ta>
            <ta e="T491" id="Seg_7977" s="T490">находиться.[3SG.S]</ta>
            <ta e="T493" id="Seg_7978" s="T492">каждый</ta>
            <ta e="T494" id="Seg_7979" s="T493">едва</ta>
            <ta e="T495" id="Seg_7980" s="T494">наружу</ta>
            <ta e="T496" id="Seg_7981" s="T495">показаться-US-PFV-3PL</ta>
            <ta e="T497" id="Seg_7982" s="T496">тоже</ta>
            <ta e="T498" id="Seg_7983" s="T497">в.лицо</ta>
            <ta e="T499" id="Seg_7984" s="T498">ударить-CO-3SG.O</ta>
            <ta e="T501" id="Seg_7985" s="T500">жена.сына-PL.[NOM]-3SG</ta>
            <ta e="T502" id="Seg_7986" s="T501">этот</ta>
            <ta e="T503" id="Seg_7987" s="T502">огонь.[NOM]</ta>
            <ta e="T504" id="Seg_7988" s="T503">INFER</ta>
            <ta e="T505" id="Seg_7989" s="T504">бросать-PST.NAR-INFER-3PL</ta>
            <ta e="T506" id="Seg_7990" s="T505">печь.[NOM]</ta>
            <ta e="T507" id="Seg_7991" s="T506">верх.[NOM]</ta>
            <ta e="T508" id="Seg_7992" s="T507">сквозь-PROL</ta>
            <ta e="T509" id="Seg_7993" s="T508">бросать-CO-3PL</ta>
            <ta e="T510" id="Seg_7994" s="T509">потом</ta>
            <ta e="T511" id="Seg_7995" s="T510">тоже</ta>
            <ta e="T512" id="Seg_7996" s="T511">два</ta>
            <ta e="T513" id="Seg_7997" s="T512">окно-GEN</ta>
            <ta e="T514" id="Seg_7998" s="T513">рот-GEN</ta>
            <ta e="T515" id="Seg_7999" s="T514">сквозь-PROL</ta>
            <ta e="T516" id="Seg_8000" s="T515">бросать-CO-3PL</ta>
            <ta e="T518" id="Seg_8001" s="T517">всё</ta>
            <ta e="T519" id="Seg_8002" s="T518">огонь-INSTR</ta>
            <ta e="T520" id="Seg_8003" s="T519">зажечь-PFV-PST.NAR-3SG.O</ta>
            <ta e="T521" id="Seg_8004" s="T520">этот</ta>
            <ta e="T522" id="Seg_8005" s="T521">ненец-ADJZ</ta>
            <ta e="T523" id="Seg_8006" s="T522">войско-ACC</ta>
            <ta e="T525" id="Seg_8007" s="T524">этот</ta>
            <ta e="T526" id="Seg_8008" s="T525">война.[NOM]</ta>
            <ta e="T527" id="Seg_8009" s="T526">наверное</ta>
            <ta e="T528" id="Seg_8010" s="T527">два</ta>
            <ta e="T529" id="Seg_8011" s="T528">быть-PST.NAR-INFER-3PL</ta>
            <ta e="T533" id="Seg_8012" s="T532">вот</ta>
            <ta e="T534" id="Seg_8013" s="T533">сказать-IMP.2SG.O</ta>
            <ta e="T536" id="Seg_8014" s="T535">друг-CAR-ADJZ</ta>
            <ta e="T537" id="Seg_8015" s="T536">старик.[NOM]</ta>
            <ta e="T538" id="Seg_8016" s="T537">куда</ta>
            <ta e="T539" id="Seg_8017" s="T538">куда.деваться-FUT.[3SG.S]</ta>
            <ta e="T541" id="Seg_8018" s="T540">старик.[NOM]</ta>
            <ta e="T542" id="Seg_8019" s="T541">потом</ta>
            <ta e="T543" id="Seg_8020" s="T542">потом</ta>
            <ta e="T544" id="Seg_8021" s="T543">сделать.нечто-CO.[3SG.S]</ta>
            <ta e="T545" id="Seg_8022" s="T544">заяц-ADJZ</ta>
            <ta e="T546" id="Seg_8023" s="T545">одежда.[NOM]</ta>
            <ta e="T547" id="Seg_8024" s="T546">старик.[NOM]</ta>
            <ta e="T548" id="Seg_8025" s="T547">вниз</ta>
            <ta e="T549" id="Seg_8026" s="T548">ненец-PL-ILL</ta>
            <ta e="T550" id="Seg_8027" s="T549">нарты-ILL</ta>
            <ta e="T551" id="Seg_8028" s="T550">отправиться-CO.[3SG.S]</ta>
            <ta e="T553" id="Seg_8029" s="T552">тоже</ta>
            <ta e="T554" id="Seg_8030" s="T553">два-нечто.[NOM]</ta>
            <ta e="T556" id="Seg_8031" s="T555">на.север</ta>
            <ta e="T558" id="Seg_8032" s="T557">ненец-ADJZ</ta>
            <ta e="T559" id="Seg_8033" s="T558">чум-ILL</ta>
            <ta e="T927" id="Seg_8034" s="T559">уйти-INF</ta>
            <ta e="T561" id="Seg_8035" s="T560">вниз</ta>
            <ta e="T562" id="Seg_8036" s="T561">сесть-IMP.2SG.S</ta>
            <ta e="T564" id="Seg_8037" s="T563">этот</ta>
            <ta e="T565" id="Seg_8038" s="T564">убить-DRV-PTCP.PST</ta>
            <ta e="T566" id="Seg_8039" s="T565">человек-EP-PL-ILL-3SG</ta>
            <ta e="T567" id="Seg_8040" s="T566">уйти-SUP.2/3SG</ta>
            <ta e="T569" id="Seg_8041" s="T568">вниз</ta>
            <ta e="T570" id="Seg_8042" s="T569">уйти-CO.[3SG.S]</ta>
            <ta e="T571" id="Seg_8043" s="T570">о</ta>
            <ta e="T572" id="Seg_8044" s="T571">вниз-ADV.LOC</ta>
            <ta e="T573" id="Seg_8045" s="T572">вверх</ta>
            <ta e="T574" id="Seg_8046" s="T573">хромать-PTCP.PRS</ta>
            <ta e="T575" id="Seg_8047" s="T574">ненец.[NOM]</ta>
            <ta e="T576" id="Seg_8048" s="T575">старик.[NOM]</ta>
            <ta e="T577" id="Seg_8049" s="T576">показаться-US-PFV-INFER.[3SG.S]</ta>
            <ta e="T581" id="Seg_8050" s="T580">что.ли</ta>
            <ta e="T582" id="Seg_8051" s="T581">INFER</ta>
            <ta e="T583" id="Seg_8052" s="T582">хохотать-DRV-INFER-1SG.S</ta>
            <ta e="T585" id="Seg_8053" s="T584">что.ли</ta>
            <ta e="T586" id="Seg_8054" s="T585">заяц-ADJZ</ta>
            <ta e="T587" id="Seg_8055" s="T586">одежда.[NOM]</ta>
            <ta e="T588" id="Seg_8056" s="T587">старик-GEN</ta>
            <ta e="T591" id="Seg_8057" s="T590">заяц-ADJZ</ta>
            <ta e="T592" id="Seg_8058" s="T591">одежда.[NOM]</ta>
            <ta e="T593" id="Seg_8059" s="T592">старик.[NOM]</ta>
            <ta e="T594" id="Seg_8060" s="T593">в.лицо</ta>
            <ta e="T595" id="Seg_8061" s="T594">ударить-CO-3SG.O</ta>
            <ta e="T596" id="Seg_8062" s="T595">лось-ADJZ</ta>
            <ta e="T597" id="Seg_8063" s="T596">затылок.[NOM]-3SG</ta>
            <ta e="T598" id="Seg_8064" s="T597">на.спину</ta>
            <ta e="T599" id="Seg_8065" s="T598">ломать-RES-CO.[3SG.S]</ta>
            <ta e="T601" id="Seg_8066" s="T600">потом</ta>
            <ta e="T602" id="Seg_8067" s="T601">вниз</ta>
            <ta e="T603" id="Seg_8068" s="T602">идти-MOM-CO.[3SG.S]</ta>
            <ta e="T605" id="Seg_8069" s="T604">вниз</ta>
            <ta e="T606" id="Seg_8070" s="T605">идти-MOM-CO.[3SG.S]</ta>
            <ta e="T607" id="Seg_8071" s="T606">потом</ta>
            <ta e="T608" id="Seg_8072" s="T607">три</ta>
            <ta e="T609" id="Seg_8073" s="T608">жена.сына-DYA-PL.[NOM]</ta>
            <ta e="T610" id="Seg_8074" s="T609">на.север</ta>
            <ta e="T611" id="Seg_8075" s="T610">уйти-CO-3PL</ta>
            <ta e="T613" id="Seg_8076" s="T612">на.север</ta>
            <ta e="T614" id="Seg_8077" s="T613">ненец-GEN</ta>
            <ta e="T615" id="Seg_8078" s="T614">дом.[NOM]</ta>
            <ta e="T616" id="Seg_8079" s="T615">на.север-ADV.LOC</ta>
            <ta e="T617" id="Seg_8080" s="T616">быть-PST.NAR.[3SG.S]</ta>
            <ta e="T618" id="Seg_8081" s="T617">туда-%%-ADV.LOC</ta>
            <ta e="T620" id="Seg_8082" s="T619">вниз</ta>
            <ta e="T621" id="Seg_8083" s="T620">уйти-CO-3PL</ta>
            <ta e="T622" id="Seg_8084" s="T621">там</ta>
            <ta e="T623" id="Seg_8085" s="T622">туда</ta>
            <ta e="T624" id="Seg_8086" s="T623">двигаться-TR-PFV-3PL</ta>
            <ta e="T625" id="Seg_8087" s="T624">ненец-PL-EP-GEN</ta>
            <ta e="T626" id="Seg_8088" s="T625">нарты-INSTR</ta>
            <ta e="T627" id="Seg_8089" s="T626">этот</ta>
            <ta e="T628" id="Seg_8090" s="T627">человек-PL-GEN</ta>
            <ta e="T629" id="Seg_8091" s="T628">нарты-INSTR</ta>
            <ta e="T630" id="Seg_8092" s="T629">этот</ta>
            <ta e="T631" id="Seg_8093" s="T630">три</ta>
            <ta e="T632" id="Seg_8094" s="T631">дедушка-DYA-PL.[NOM]</ta>
            <ta e="T634" id="Seg_8095" s="T633">на.север</ta>
            <ta e="T635" id="Seg_8096" s="T634">двигаться-TR-PFV-CVB</ta>
            <ta e="T636" id="Seg_8097" s="T635">уйти-CO-3PL</ta>
            <ta e="T638" id="Seg_8098" s="T637">тот</ta>
            <ta e="T639" id="Seg_8099" s="T638">на.север-ADV.LOC</ta>
            <ta e="T640" id="Seg_8100" s="T639">этот</ta>
            <ta e="T641" id="Seg_8101" s="T640">ненец-ADJZ</ta>
            <ta e="T643" id="Seg_8102" s="T642">женщина-%%-PL-EP-ACC</ta>
            <ta e="T644" id="Seg_8103" s="T643">INFER</ta>
            <ta e="T645" id="Seg_8104" s="T644">%%-MULO-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T647" id="Seg_8105" s="T646">тот</ta>
            <ta e="T648" id="Seg_8106" s="T647">селькуп-EP-ADJZ</ta>
            <ta e="T649" id="Seg_8107" s="T648">ребенок-DU.[NOM]</ta>
            <ta e="T650" id="Seg_8108" s="T649">туда-ADV.LOC</ta>
            <ta e="T651" id="Seg_8109" s="T650">этот-%%</ta>
            <ta e="T652" id="Seg_8110" s="T651">остаться-PST.NAR-3DU.S</ta>
            <ta e="T653" id="Seg_8111" s="T652">давно-ADV.LOC</ta>
            <ta e="T654" id="Seg_8112" s="T653">этот</ta>
            <ta e="T655" id="Seg_8113" s="T654">дедушка-OBL.3DU-ALL</ta>
            <ta e="T656" id="Seg_8114" s="T655">прийти-PST.NAR-3DU.S</ta>
            <ta e="T658" id="Seg_8115" s="T657">вверх-ADV.ILL</ta>
            <ta e="T659" id="Seg_8116" s="T658">тот</ta>
            <ta e="T660" id="Seg_8117" s="T659">нечто-ILL</ta>
            <ta e="T661" id="Seg_8118" s="T660">два</ta>
            <ta e="T662" id="Seg_8119" s="T661">ненец.[NOM]</ta>
            <ta e="T663" id="Seg_8120" s="T662">домой</ta>
            <ta e="T664" id="Seg_8121" s="T663">послать-PST.NAR-INFER-3PL</ta>
            <ta e="T666" id="Seg_8122" s="T665">чум.[NOM]-3SG</ta>
            <ta e="T667" id="Seg_8123" s="T666">сгнить-%%</ta>
            <ta e="T669" id="Seg_8124" s="T668">что.[NOM]-3SG</ta>
            <ta e="T670" id="Seg_8125" s="T669">вот.он.есть</ta>
            <ta e="T671" id="Seg_8126" s="T670">заяц-ADJZ</ta>
            <ta e="T672" id="Seg_8127" s="T671">одежда.[NOM]</ta>
            <ta e="T673" id="Seg_8128" s="T672">старик.[NOM]</ta>
            <ta e="T674" id="Seg_8129" s="T673">олень-COM</ta>
            <ta e="T675" id="Seg_8130" s="T674">снег.[NOM]</ta>
            <ta e="T676" id="Seg_8131" s="T675">%%</ta>
            <ta e="T677" id="Seg_8132" s="T676">вперёд</ta>
            <ta e="T678" id="Seg_8133" s="T677">погнать-CO-3SG.O</ta>
            <ta e="T679" id="Seg_8134" s="T678">этот</ta>
            <ta e="T680" id="Seg_8135" s="T679">этот</ta>
            <ta e="T681" id="Seg_8136" s="T680">сирота-ADJZ</ta>
            <ta e="T682" id="Seg_8137" s="T681">ребенок.[NOM]</ta>
            <ta e="T683" id="Seg_8138" s="T682">тоже</ta>
            <ta e="T684" id="Seg_8139" s="T683">он(а)-EP-COM</ta>
            <ta e="T685" id="Seg_8140" s="T684">быть-PST-3DU.S</ta>
            <ta e="T687" id="Seg_8141" s="T686">давно</ta>
            <ta e="T688" id="Seg_8142" s="T687">родня-GEN</ta>
            <ta e="T689" id="Seg_8143" s="T688">он(а)-EP-GEN</ta>
            <ta e="T690" id="Seg_8144" s="T689">нечто-LOC</ta>
            <ta e="T691" id="Seg_8145" s="T690">ненец-ADJZ</ta>
            <ta e="T692" id="Seg_8146" s="T691">этот</ta>
            <ta e="T693" id="Seg_8147" s="T692">нечто.[NOM]</ta>
            <ta e="T695" id="Seg_8148" s="T694">заяц-ADJZ</ta>
            <ta e="T696" id="Seg_8149" s="T695">одежда.[NOM]</ta>
            <ta e="T697" id="Seg_8150" s="T696">старик.[NOM]</ta>
            <ta e="T698" id="Seg_8151" s="T697">родня-%%</ta>
            <ta e="T699" id="Seg_8152" s="T698">внук-DU.[NOM]-3SG</ta>
            <ta e="T700" id="Seg_8153" s="T699">быть-PST.NAR-INFER-3DU.S</ta>
            <ta e="T702" id="Seg_8154" s="T701">этот</ta>
            <ta e="T703" id="Seg_8155" s="T702">убить-PTCP.PST</ta>
            <ta e="T704" id="Seg_8156" s="T703">человек-EP-PL-EP-GEN</ta>
            <ta e="T705" id="Seg_8157" s="T704">этот</ta>
            <ta e="T706" id="Seg_8158" s="T705">ненец-GEN</ta>
            <ta e="T708" id="Seg_8159" s="T707">это-GEN</ta>
            <ta e="T709" id="Seg_8160" s="T708">ребенок-DU.[NOM]</ta>
            <ta e="T710" id="Seg_8161" s="T709">два</ta>
            <ta e="T712" id="Seg_8162" s="T711">такой</ta>
            <ta e="T713" id="Seg_8163" s="T712">два</ta>
            <ta e="T714" id="Seg_8164" s="T713">ребенок-DU.[NOM]</ta>
            <ta e="T716" id="Seg_8165" s="T715">олень-ACC</ta>
            <ta e="T717" id="Seg_8166" s="T716">погнать-CVB</ta>
            <ta e="T718" id="Seg_8167" s="T717">домой</ta>
            <ta e="T719" id="Seg_8168" s="T718">привезти-3PL</ta>
            <ta e="T720" id="Seg_8169" s="T719">тот</ta>
            <ta e="T721" id="Seg_8170" s="T720">тот</ta>
            <ta e="T722" id="Seg_8171" s="T721">дом-ILL</ta>
            <ta e="T724" id="Seg_8172" s="T723">тот</ta>
            <ta e="T725" id="Seg_8173" s="T724">чум-EP-VBLZ-CO-3PL</ta>
            <ta e="T727" id="Seg_8174" s="T726">эй</ta>
            <ta e="T728" id="Seg_8175" s="T727">этот</ta>
            <ta e="T729" id="Seg_8176" s="T728">этот-DIM.[NOM]</ta>
            <ta e="T730" id="Seg_8177" s="T729">настолько</ta>
            <ta e="T733" id="Seg_8178" s="T732">сын-PL.[NOM]-3SG</ta>
            <ta e="T734" id="Seg_8179" s="T733">этот</ta>
            <ta e="T735" id="Seg_8180" s="T734">этот</ta>
            <ta e="T736" id="Seg_8181" s="T735">отправиться-PST.NAR-INFER-3PL</ta>
            <ta e="T737" id="Seg_8182" s="T736">лес-ILL</ta>
            <ta e="T738" id="Seg_8183" s="T737">белка-CAP-CVB</ta>
            <ta e="T739" id="Seg_8184" s="T738">зверь-VBLZ-CVB</ta>
            <ta e="T743" id="Seg_8185" s="T742">вверх</ta>
            <ta e="T744" id="Seg_8186" s="T743">чум-EP-VBLZ-CO-3PL</ta>
            <ta e="T745" id="Seg_8187" s="T744">%%-2SG.O</ta>
            <ta e="T746" id="Seg_8188" s="T745">один</ta>
            <ta e="T747" id="Seg_8189" s="T746">ночь-ADV.LOC</ta>
            <ta e="T748" id="Seg_8190" s="T747">ночевать-3PL</ta>
            <ta e="T750" id="Seg_8191" s="T749">ночевать-INF-собраться-CO-3PL</ta>
            <ta e="T752" id="Seg_8192" s="T751">сын-PL.[NOM]-3SG</ta>
            <ta e="T753" id="Seg_8193" s="T752">лес-EL</ta>
            <ta e="T754" id="Seg_8194" s="T753">RFL</ta>
            <ta e="T755" id="Seg_8195" s="T754">принести-3PL</ta>
            <ta e="T756" id="Seg_8196" s="T755">белка-%%</ta>
            <ta e="T757" id="Seg_8197" s="T756">домой</ta>
            <ta e="T759" id="Seg_8198" s="T758">подкрасться-CVB</ta>
            <ta e="T760" id="Seg_8199" s="T759">привезти-3PL</ta>
            <ta e="T762" id="Seg_8200" s="T761">что.[NOM]</ta>
            <ta e="T763" id="Seg_8201" s="T762">стать.[3SG.S]</ta>
            <ta e="T765" id="Seg_8202" s="T764">отец.[NOM]</ta>
            <ta e="T766" id="Seg_8203" s="T765">старик.[NOM]</ta>
            <ta e="T767" id="Seg_8204" s="T766">нечто-GEN</ta>
            <ta e="T768" id="Seg_8205" s="T767">тот</ta>
            <ta e="T769" id="Seg_8206" s="T768">чум-GEN.3SG</ta>
            <ta e="T770" id="Seg_8207" s="T769">%%-PL.[NOM]</ta>
            <ta e="T772" id="Seg_8208" s="T771">вниз</ta>
            <ta e="T773" id="Seg_8209" s="T772">едва</ta>
            <ta e="T774" id="Seg_8210" s="T773">взглянуть-PST.NAR-3PL</ta>
            <ta e="T775" id="Seg_8211" s="T774">место-CAR</ta>
            <ta e="T776" id="Seg_8212" s="T775">огонь.[NOM]</ta>
            <ta e="T777" id="Seg_8213" s="T776">съесть-PST.NAR-3SG.O</ta>
            <ta e="T778" id="Seg_8214" s="T777">отец.[NOM]</ta>
            <ta e="T779" id="Seg_8215" s="T778">старик.[NOM]</ta>
            <ta e="T780" id="Seg_8216" s="T779">нечто-PL.[NOM]</ta>
            <ta e="T781" id="Seg_8217" s="T780">убить-PST.NAR-3PL</ta>
            <ta e="T782" id="Seg_8218" s="T781">он(а)-EP-PL.[NOM]</ta>
            <ta e="T783" id="Seg_8219" s="T782">так</ta>
            <ta e="T784" id="Seg_8220" s="T783">думать-FRQ-DUR-3PL</ta>
            <ta e="T786" id="Seg_8221" s="T785">этот</ta>
            <ta e="T787" id="Seg_8222" s="T786">семь</ta>
            <ta e="T788" id="Seg_8223" s="T787">сын.[NOM]-3SG</ta>
            <ta e="T790" id="Seg_8224" s="T789">%%-2SG.O</ta>
            <ta e="T792" id="Seg_8225" s="T791">потом</ta>
            <ta e="T793" id="Seg_8226" s="T792">два-ITER.NUM</ta>
            <ta e="T795" id="Seg_8227" s="T794">отправиться-IPFV-CVB</ta>
            <ta e="T796" id="Seg_8228" s="T795">принести-3PL</ta>
            <ta e="T798" id="Seg_8229" s="T797">пространство.снаружи-LOC</ta>
            <ta e="T799" id="Seg_8230" s="T798">собака.[NOM]-3SG</ta>
            <ta e="T800" id="Seg_8231" s="T799">этот</ta>
            <ta e="T801" id="Seg_8232" s="T800">младший</ta>
            <ta e="T802" id="Seg_8233" s="T801">сын-GEN-3SG</ta>
            <ta e="T803" id="Seg_8234" s="T802">собака.[NOM]</ta>
            <ta e="T805" id="Seg_8235" s="T804">жена.[NOM]-3SG</ta>
            <ta e="T806" id="Seg_8236" s="T805">наружу</ta>
            <ta e="T807" id="Seg_8237" s="T806">привязать-PST.NAR-3SG.O</ta>
            <ta e="T809" id="Seg_8238" s="T808">олень-ACC</ta>
            <ta e="T810" id="Seg_8239" s="T809">кто-ADJZ</ta>
            <ta e="T811" id="Seg_8240" s="T810">улица-ADV.LOC</ta>
            <ta e="T812" id="Seg_8241" s="T811">этот</ta>
            <ta e="T813" id="Seg_8242" s="T812">ненец-GEN</ta>
            <ta e="T814" id="Seg_8243" s="T813">олень-ACC</ta>
            <ta e="T815" id="Seg_8244" s="T814">погнать-3PL</ta>
            <ta e="T817" id="Seg_8245" s="T816">собака-GEN</ta>
            <ta e="T818" id="Seg_8246" s="T817">нога-ACC</ta>
            <ta e="T819" id="Seg_8247" s="T818">наступить-CO-3PL</ta>
            <ta e="T820" id="Seg_8248" s="T819">собака.[NOM]</ta>
            <ta e="T821" id="Seg_8249" s="T820">визг.собаки-INCH-CO.[3SG.S]</ta>
            <ta e="T823" id="Seg_8250" s="T822">старик.[NOM]</ta>
            <ta e="T824" id="Seg_8251" s="T823">такой-ADJZ</ta>
            <ta e="T825" id="Seg_8252" s="T824">открыть-RFL.PFV.[3SG.S]</ta>
            <ta e="T826" id="Seg_8253" s="T825">заяц-ADJZ</ta>
            <ta e="T827" id="Seg_8254" s="T826">одежда.[NOM]</ta>
            <ta e="T828" id="Seg_8255" s="T827">старик.[NOM]</ta>
            <ta e="T829" id="Seg_8256" s="T828">будто</ta>
            <ta e="T831" id="Seg_8257" s="T830">собака.[NOM]-1SG</ta>
            <ta e="T832" id="Seg_8258" s="T831">что.[NOM]</ta>
            <ta e="T833" id="Seg_8259" s="T832">съесть-PST.NAR-3SG.O</ta>
            <ta e="T834" id="Seg_8260" s="T833">что.ли</ta>
            <ta e="T835" id="Seg_8261" s="T834">олень.[NOM]</ta>
            <ta e="T836" id="Seg_8262" s="T835">рубить-MOM-CO-3SG.O</ta>
            <ta e="T838" id="Seg_8263" s="T837">%%</ta>
            <ta e="T839" id="Seg_8264" s="T838">младший</ta>
            <ta e="T840" id="Seg_8265" s="T839">сын-GEN</ta>
            <ta e="T841" id="Seg_8266" s="T840">женщина.[NOM]</ta>
            <ta e="T842" id="Seg_8267" s="T841">наружу</ta>
            <ta e="T843" id="Seg_8268" s="T842">побежать.[3SG.S]</ta>
            <ta e="T845" id="Seg_8269" s="T844">этот</ta>
            <ta e="T846" id="Seg_8270" s="T845">заяц-ADJZ</ta>
            <ta e="T847" id="Seg_8271" s="T846">одежда.[NOM]</ta>
            <ta e="T848" id="Seg_8272" s="T847">старик-GEN.3SG</ta>
            <ta e="T849" id="Seg_8273" s="T848">младший</ta>
            <ta e="T850" id="Seg_8274" s="T849">сын.[NOM]-3SG</ta>
            <ta e="T852" id="Seg_8275" s="T851">наружу</ta>
            <ta e="T853" id="Seg_8276" s="T852">едва</ta>
            <ta e="T854" id="Seg_8277" s="T853">побежать.[3SG.S]</ta>
            <ta e="T855" id="Seg_8278" s="T854">что.[NOM]</ta>
            <ta e="T856" id="Seg_8279" s="T855">стать.[3SG.S]</ta>
            <ta e="T857" id="Seg_8280" s="T856">муж.[NOM]-3SG</ta>
            <ta e="T859" id="Seg_8281" s="T858">этот</ta>
            <ta e="T860" id="Seg_8282" s="T859">что.[NOM]</ta>
            <ta e="T861" id="Seg_8283" s="T860">человек-EP-PL.[NOM]</ta>
            <ta e="T862" id="Seg_8284" s="T861">почувствовать-CO.[3SG.S]</ta>
            <ta e="T864" id="Seg_8285" s="T863">отец.[NOM]</ta>
            <ta e="T865" id="Seg_8286" s="T864">старик.[NOM]-3SG</ta>
            <ta e="T866" id="Seg_8287" s="T865">этот</ta>
            <ta e="T867" id="Seg_8288" s="T866">открыть-RFL.PFV-HAB.[3SG.S]</ta>
            <ta e="T868" id="Seg_8289" s="T867">этот</ta>
            <ta e="T869" id="Seg_8290" s="T868">дом-LOC</ta>
            <ta e="T871" id="Seg_8291" s="T870">мол</ta>
            <ta e="T872" id="Seg_8292" s="T871">мы.PL.NOM</ta>
            <ta e="T873" id="Seg_8293" s="T872">мол</ta>
            <ta e="T874" id="Seg_8294" s="T873">чум.[NOM]-1PL</ta>
            <ta e="T875" id="Seg_8295" s="T874">мол</ta>
            <ta e="T876" id="Seg_8296" s="T875">мы.PL.NOM</ta>
            <ta e="T877" id="Seg_8297" s="T876">огонь-INSTR</ta>
            <ta e="T878" id="Seg_8298" s="T877">зажечь-PST-1PL</ta>
            <ta e="T879" id="Seg_8299" s="T878">будто</ta>
            <ta e="T881" id="Seg_8300" s="T880">дедушка.[NOM]</ta>
            <ta e="T882" id="Seg_8301" s="T881">мол</ta>
            <ta e="T883" id="Seg_8302" s="T882">такой-ADJZ</ta>
            <ta e="T884" id="Seg_8303" s="T883">мы.PL.ACC</ta>
            <ta e="T894" id="Seg_8304" s="T893">ну.ка</ta>
            <ta e="T895" id="Seg_8305" s="T894">вперёд</ta>
            <ta e="T896" id="Seg_8306" s="T895">сказать-IMP.2SG.O</ta>
            <ta e="T898" id="Seg_8307" s="T897">потом</ta>
            <ta e="T899" id="Seg_8308" s="T898">затем</ta>
            <ta e="T900" id="Seg_8309" s="T899">INFER</ta>
            <ta e="T901" id="Seg_8310" s="T900">жить-PST.NAR-INFER-3PL</ta>
            <ta e="T902" id="Seg_8311" s="T901">там</ta>
            <ta e="T903" id="Seg_8312" s="T902">вот</ta>
            <ta e="T904" id="Seg_8313" s="T903">вроде</ta>
            <ta e="T905" id="Seg_8314" s="T904">опять</ta>
            <ta e="T906" id="Seg_8315" s="T905">прочь</ta>
            <ta e="T907" id="Seg_8316" s="T906">отправиться-PST.NAR-3PL</ta>
            <ta e="T908" id="Seg_8317" s="T907">этот</ta>
            <ta e="T909" id="Seg_8318" s="T908">селькуп-EP-PL.[NOM]</ta>
            <ta e="T910" id="Seg_8319" s="T909">тот</ta>
            <ta e="T912" id="Seg_8320" s="T911">вроде</ta>
            <ta e="T913" id="Seg_8321" s="T912">ненец-ADJZ</ta>
            <ta e="T914" id="Seg_8322" s="T913">земля-GEN</ta>
            <ta e="T915" id="Seg_8323" s="T914">олень-ACC</ta>
            <ta e="T916" id="Seg_8324" s="T915">погнать-PST.NAR-INFER-3PL</ta>
            <ta e="T918" id="Seg_8325" s="T917">и</ta>
            <ta e="T919" id="Seg_8326" s="T918">этот</ta>
            <ta e="T920" id="Seg_8327" s="T919">селькуп.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T11" id="Seg_8328" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_8329" s="T11">v-v:mood.pn</ta>
            <ta e="T13" id="Seg_8330" s="T12">adv</ta>
            <ta e="T14" id="Seg_8331" s="T13">adj</ta>
            <ta e="T15" id="Seg_8332" s="T14">interrog.[n:case]</ta>
            <ta e="T17" id="Seg_8333" s="T16">n.[n:case]</ta>
            <ta e="T18" id="Seg_8334" s="T17">dem</ta>
            <ta e="T19" id="Seg_8335" s="T18">n-n&gt;adj</ta>
            <ta e="T20" id="Seg_8336" s="T19">n.[n:case]</ta>
            <ta e="T21" id="Seg_8337" s="T20">n.[n:case]</ta>
            <ta e="T22" id="Seg_8338" s="T21">clit</ta>
            <ta e="T23" id="Seg_8339" s="T22">v-%%-v:pn</ta>
            <ta e="T25" id="Seg_8340" s="T24">n-n&gt;adj</ta>
            <ta e="T26" id="Seg_8341" s="T25">n.[n:case]</ta>
            <ta e="T27" id="Seg_8342" s="T26">n.[n:case]</ta>
            <ta e="T28" id="Seg_8343" s="T27">dem</ta>
            <ta e="T29" id="Seg_8344" s="T28">adv</ta>
            <ta e="T30" id="Seg_8345" s="T29">num</ta>
            <ta e="T31" id="Seg_8346" s="T30">n.[n:case]-n:poss</ta>
            <ta e="T32" id="Seg_8347" s="T31">v-v:tense-v:mood.[v:pn]</ta>
            <ta e="T34" id="Seg_8348" s="T33">conj</ta>
            <ta e="T35" id="Seg_8349" s="T34">num</ta>
            <ta e="T36" id="Seg_8350" s="T35">n.[n:case]-n:poss</ta>
            <ta e="T38" id="Seg_8351" s="T37">n-n&gt;adv</ta>
            <ta e="T39" id="Seg_8352" s="T38">v-v:ins-v:pn</ta>
            <ta e="T41" id="Seg_8353" s="T40">n-n:num.[n:case]</ta>
            <ta e="T42" id="Seg_8354" s="T41">adv-adv:case</ta>
            <ta e="T43" id="Seg_8355" s="T42">adv</ta>
            <ta e="T44" id="Seg_8356" s="T43">v-v:ins-v:pn</ta>
            <ta e="T45" id="Seg_8357" s="T44">pers-v:ins-n:case</ta>
            <ta e="T46" id="Seg_8358" s="T45">v-v:inf.poss</ta>
            <ta e="T48" id="Seg_8359" s="T47">n-n:num.[n:case]-n:poss</ta>
            <ta e="T49" id="Seg_8360" s="T48">n-n:num.[n:case]-n:poss</ta>
            <ta e="T50" id="Seg_8361" s="T49">adv</ta>
            <ta e="T51" id="Seg_8362" s="T50">v-v:tense-v:pn</ta>
            <ta e="T52" id="Seg_8363" s="T51">n-n&gt;v-v&gt;adv</ta>
            <ta e="T54" id="Seg_8364" s="T53">n-n&gt;v-v&gt;adv</ta>
            <ta e="T55" id="Seg_8365" s="T54">adv</ta>
            <ta e="T56" id="Seg_8366" s="T55">v-v:tense-v:pn</ta>
            <ta e="T57" id="Seg_8367" s="T56">adv</ta>
            <ta e="T58" id="Seg_8368" s="T57">n.[n:case]</ta>
            <ta e="T59" id="Seg_8369" s="T58">adv</ta>
            <ta e="T60" id="Seg_8370" s="T59">n-n&gt;adv</ta>
            <ta e="T61" id="Seg_8371" s="T60">adv</ta>
            <ta e="T62" id="Seg_8372" s="T61">interrog</ta>
            <ta e="T63" id="Seg_8373" s="T62">v-v&gt;v.[v:pn]</ta>
            <ta e="T64" id="Seg_8374" s="T63">n-n&gt;adv</ta>
            <ta e="T66" id="Seg_8375" s="T65">dem</ta>
            <ta e="T67" id="Seg_8376" s="T66">n-n:case</ta>
            <ta e="T68" id="Seg_8377" s="T67">n-n:ins-n&gt;adj</ta>
            <ta e="T69" id="Seg_8378" s="T68">n-n:case</ta>
            <ta e="T70" id="Seg_8379" s="T69">v-v&gt;v-v:tense.mood.[v:pn]</ta>
            <ta e="T71" id="Seg_8380" s="T70">dem</ta>
            <ta e="T72" id="Seg_8381" s="T71">n.[n:case]</ta>
            <ta e="T74" id="Seg_8382" s="T73">dem</ta>
            <ta e="T75" id="Seg_8383" s="T74">n-adv:case-n:obl.poss</ta>
            <ta e="T77" id="Seg_8384" s="T76">n-n&gt;adj</ta>
            <ta e="T78" id="Seg_8385" s="T77">n.[n:case]</ta>
            <ta e="T79" id="Seg_8386" s="T78">n.[n:case]</ta>
            <ta e="T80" id="Seg_8387" s="T79">ptcl</ta>
            <ta e="T81" id="Seg_8388" s="T80">adv</ta>
            <ta e="T82" id="Seg_8389" s="T81">ptcl</ta>
            <ta e="T83" id="Seg_8390" s="T82">pers</ta>
            <ta e="T84" id="Seg_8391" s="T83">v-v:tense-v:pn</ta>
            <ta e="T85" id="Seg_8392" s="T84">pers-v:ins-n:case</ta>
            <ta e="T86" id="Seg_8393" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_8394" s="T86">pers</ta>
            <ta e="T88" id="Seg_8395" s="T87">ptcl</ta>
            <ta e="T89" id="Seg_8396" s="T88">v-v&gt;v-v:mood.pn</ta>
            <ta e="T90" id="Seg_8397" s="T89">adv</ta>
            <ta e="T92" id="Seg_8398" s="T91">n-n:num.[n:case]</ta>
            <ta e="T93" id="Seg_8399" s="T92">adv</ta>
            <ta e="T94" id="Seg_8400" s="T93">v-v&gt;adv</ta>
            <ta e="T95" id="Seg_8401" s="T94">adv</ta>
            <ta e="T96" id="Seg_8402" s="T95">v-v:ins-v:pn</ta>
            <ta e="T97" id="Seg_8403" s="T96">n-n&gt;adj</ta>
            <ta e="T98" id="Seg_8404" s="T97">n-n:num.[n:case]</ta>
            <ta e="T99" id="Seg_8405" s="T98">n.[n:case]</ta>
            <ta e="T100" id="Seg_8406" s="T99">v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T102" id="Seg_8407" s="T101">dem</ta>
            <ta e="T103" id="Seg_8408" s="T102">n-n:ins-n&gt;adj</ta>
            <ta e="T104" id="Seg_8409" s="T103">n-n:num.[n:case]</ta>
            <ta e="T106" id="Seg_8410" s="T105">v-v&gt;v-v:mood.pn</ta>
            <ta e="T107" id="Seg_8411" s="T106">v-v&gt;v-v:mood.pn</ta>
            <ta e="T108" id="Seg_8412" s="T107">ptcl</ta>
            <ta e="T109" id="Seg_8413" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_8414" s="T109">n-n:case</ta>
            <ta e="T111" id="Seg_8415" s="T110">ptcl</ta>
            <ta e="T112" id="Seg_8416" s="T111">v-v&gt;v-v&gt;v-v&gt;v.[v:pn]</ta>
            <ta e="T114" id="Seg_8417" s="T113">%%-v:pn</ta>
            <ta e="T116" id="Seg_8418" s="T115">adv</ta>
            <ta e="T117" id="Seg_8419" s="T116">adv</ta>
            <ta e="T118" id="Seg_8420" s="T117">n.[n:case]</ta>
            <ta e="T119" id="Seg_8421" s="T118">interrog</ta>
            <ta e="T120" id="Seg_8422" s="T119">qv-v:tense.[v:pn]</ta>
            <ta e="T122" id="Seg_8423" s="T121">n.[n:case]</ta>
            <ta e="T123" id="Seg_8424" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_8425" s="T123">adv</ta>
            <ta e="T125" id="Seg_8426" s="T124">ptcl</ta>
            <ta e="T126" id="Seg_8427" s="T125">v-v:tense.mood.[v:pn]</ta>
            <ta e="T128" id="Seg_8428" s="T127">n.[n:case]</ta>
            <ta e="T129" id="Seg_8429" s="T128">dem-adj&gt;adv</ta>
            <ta e="T130" id="Seg_8430" s="T129">ptcl</ta>
            <ta e="T131" id="Seg_8431" s="T130">v-v:tense.[v:pn]</ta>
            <ta e="T132" id="Seg_8432" s="T131">dem</ta>
            <ta e="T133" id="Seg_8433" s="T132">n-n&gt;adj</ta>
            <ta e="T134" id="Seg_8434" s="T133">n-n&gt;adj</ta>
            <ta e="T135" id="Seg_8435" s="T134">n.[n:case]</ta>
            <ta e="T137" id="Seg_8436" s="T136">dem-adj&gt;adv</ta>
            <ta e="T138" id="Seg_8437" s="T137">v.[v:pn]</ta>
            <ta e="T139" id="Seg_8438" s="T138">n-n:obl.poss-n&gt;adj</ta>
            <ta e="T140" id="Seg_8439" s="T139">n-n:case</ta>
            <ta e="T141" id="Seg_8440" s="T140">n-n:case</ta>
            <ta e="T142" id="Seg_8441" s="T141">v-v:mood.pn</ta>
            <ta e="T143" id="Seg_8442" s="T142">adv</ta>
            <ta e="T145" id="Seg_8443" s="T144">n.[n:case]</ta>
            <ta e="T146" id="Seg_8444" s="T145">n-n:case</ta>
            <ta e="T147" id="Seg_8445" s="T146">v-v:mood.pn</ta>
            <ta e="T148" id="Seg_8446" s="T147">interrog-n&gt;adj</ta>
            <ta e="T149" id="Seg_8447" s="T148">%%</ta>
            <ta e="T150" id="Seg_8448" s="T149">n-n:case</ta>
            <ta e="T151" id="Seg_8449" s="T150">interrog-n:case</ta>
            <ta e="T152" id="Seg_8450" s="T151">quant</ta>
            <ta e="T153" id="Seg_8451" s="T152">interrog.[n:case]</ta>
            <ta e="T154" id="Seg_8452" s="T153">v-v:mood.pn</ta>
            <ta e="T156" id="Seg_8453" s="T155">n-n:num.[n:case]</ta>
            <ta e="T157" id="Seg_8454" s="T156">adv</ta>
            <ta e="T158" id="Seg_8455" s="T157">v-v:tense-v:pn</ta>
            <ta e="T159" id="Seg_8456" s="T158">v-v:ins-v&gt;v-v:tense-v:pn</ta>
            <ta e="T161" id="Seg_8457" s="T160">n.[n:case]</ta>
            <ta e="T162" id="Seg_8458" s="T161">adv</ta>
            <ta e="T163" id="Seg_8459" s="T162">v-v:ins-v:pn</ta>
            <ta e="T165" id="Seg_8460" s="T164">n-n:case</ta>
            <ta e="T166" id="Seg_8461" s="T165">v-v:pn</ta>
            <ta e="T168" id="Seg_8462" s="T167">dem</ta>
            <ta e="T169" id="Seg_8463" s="T168">dem</ta>
            <ta e="T170" id="Seg_8464" s="T169">n-n:case</ta>
            <ta e="T171" id="Seg_8465" s="T170">dem</ta>
            <ta e="T172" id="Seg_8466" s="T171">n.[n:case]</ta>
            <ta e="T173" id="Seg_8467" s="T172">n.[n:case]</ta>
            <ta e="T174" id="Seg_8468" s="T173">adj</ta>
            <ta e="T175" id="Seg_8469" s="T174">n-n:case</ta>
            <ta e="T176" id="Seg_8470" s="T175">v-v:pn</ta>
            <ta e="T177" id="Seg_8471" s="T176">dem</ta>
            <ta e="T179" id="Seg_8472" s="T178">n.[n:case]</ta>
            <ta e="T180" id="Seg_8473" s="T179">n-n:case</ta>
            <ta e="T181" id="Seg_8474" s="T180">n-n&gt;v-v:inf-v-v:ins-v:pn</ta>
            <ta e="T183" id="Seg_8475" s="T182">interj</ta>
            <ta e="T184" id="Seg_8476" s="T183">n-n&gt;adj</ta>
            <ta e="T185" id="Seg_8477" s="T184">n.[n:case]</ta>
            <ta e="T186" id="Seg_8478" s="T185">n-n:num.[n:case]</ta>
            <ta e="T187" id="Seg_8479" s="T186">dem</ta>
            <ta e="T188" id="Seg_8480" s="T187">preverb</ta>
            <ta e="T189" id="Seg_8481" s="T188">num-num&gt;num-n:ins-num&gt;adj</ta>
            <ta e="T190" id="Seg_8482" s="T189">n-n:ins-n&gt;adj</ta>
            <ta e="T191" id="Seg_8483" s="T190">n.[n:case]</ta>
            <ta e="T193" id="Seg_8484" s="T192">adv</ta>
            <ta e="T194" id="Seg_8485" s="T193">ptcl</ta>
            <ta e="T195" id="Seg_8486" s="T194">v-v&gt;ptcp</ta>
            <ta e="T196" id="Seg_8487" s="T195">n-n:case</ta>
            <ta e="T197" id="Seg_8488" s="T196">n-n&gt;adj</ta>
            <ta e="T198" id="Seg_8489" s="T197">n.[n:case]</ta>
            <ta e="T199" id="Seg_8490" s="T198">n.[n:case]</ta>
            <ta e="T200" id="Seg_8491" s="T199">emphpro</ta>
            <ta e="T201" id="Seg_8492" s="T200">v.[v:pn]</ta>
            <ta e="T202" id="Seg_8493" s="T201">n-n:num-n:obl.poss-n:case</ta>
            <ta e="T204" id="Seg_8494" s="T203">num</ta>
            <ta e="T205" id="Seg_8495" s="T204">n-n:case</ta>
            <ta e="T206" id="Seg_8496" s="T205">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T208" id="Seg_8497" s="T207">n.[n:case]</ta>
            <ta e="T209" id="Seg_8498" s="T208">dem-adj&gt;adv</ta>
            <ta e="T210" id="Seg_8499" s="T209">v.[v:pn]</ta>
            <ta e="T212" id="Seg_8500" s="T211">n.[n:case]</ta>
            <ta e="T213" id="Seg_8501" s="T212">preverb</ta>
            <ta e="T214" id="Seg_8502" s="T213">v-v&gt;v-v:pn</ta>
            <ta e="T215" id="Seg_8503" s="T214">ptcl</ta>
            <ta e="T216" id="Seg_8504" s="T215">dem</ta>
            <ta e="T217" id="Seg_8505" s="T216">dem</ta>
            <ta e="T218" id="Seg_8506" s="T217">n-n:num.[n:case]</ta>
            <ta e="T220" id="Seg_8507" s="T219">v-n:ins-v&gt;v-v:inf-v-v:ins-n:case</ta>
            <ta e="T222" id="Seg_8508" s="T221">n.[n:case]</ta>
            <ta e="T223" id="Seg_8509" s="T222">v-v:tense.mood-v:pn</ta>
            <ta e="T225" id="Seg_8510" s="T224">n-n:num.[n:case]-n:poss</ta>
            <ta e="T226" id="Seg_8511" s="T225">n.[n:case]</ta>
            <ta e="T227" id="Seg_8512" s="T226">dem</ta>
            <ta e="T228" id="Seg_8513" s="T227">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T229" id="Seg_8514" s="T228">adv-adv:case</ta>
            <ta e="T230" id="Seg_8515" s="T229">interrog-n:case</ta>
            <ta e="T231" id="Seg_8516" s="T230">pp-pp&gt;adj</ta>
            <ta e="T232" id="Seg_8517" s="T231">n.[n:case]</ta>
            <ta e="T233" id="Seg_8518" s="T232">n.[n:case]</ta>
            <ta e="T234" id="Seg_8519" s="T233">n.[n:case]</ta>
            <ta e="T235" id="Seg_8520" s="T234">interrog-n:case</ta>
            <ta e="T236" id="Seg_8521" s="T235">pp-pp&gt;adj</ta>
            <ta e="T237" id="Seg_8522" s="T236">interrog.[n:case]</ta>
            <ta e="T238" id="Seg_8523" s="T237">dem</ta>
            <ta e="T239" id="Seg_8524" s="T238">v-v&gt;ptcp</ta>
            <ta e="T240" id="Seg_8525" s="T239">n.[n:case]</ta>
            <ta e="T242" id="Seg_8526" s="T241">n.[n:case]</ta>
            <ta e="T243" id="Seg_8527" s="T242">adv</ta>
            <ta e="T244" id="Seg_8528" s="T243">v-v:ins-v:pn</ta>
            <ta e="T245" id="Seg_8529" s="T244">n-n:case</ta>
            <ta e="T246" id="Seg_8530" s="T245">n-n&gt;adj</ta>
            <ta e="T247" id="Seg_8531" s="T246">n.[n:case]</ta>
            <ta e="T248" id="Seg_8532" s="T247">n.[n:case]</ta>
            <ta e="T249" id="Seg_8533" s="T248">n.[n:case]-n:poss</ta>
            <ta e="T250" id="Seg_8534" s="T249">ptcl</ta>
            <ta e="T251" id="Seg_8535" s="T250">v-v:tense.[v:pn]</ta>
            <ta e="T253" id="Seg_8536" s="T252">%%-v:pn</ta>
            <ta e="T255" id="Seg_8537" s="T254">n-n:num-n:case-n:poss</ta>
            <ta e="T256" id="Seg_8538" s="T255">adv</ta>
            <ta e="T257" id="Seg_8539" s="T256">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T258" id="Seg_8540" s="T257">pers</ta>
            <ta e="T259" id="Seg_8541" s="T258">adv</ta>
            <ta e="T260" id="Seg_8542" s="T259">v-v:mood.pn</ta>
            <ta e="T262" id="Seg_8543" s="T261">n-n:num.[n:case]</ta>
            <ta e="T263" id="Seg_8544" s="T262">%%</ta>
            <ta e="T264" id="Seg_8545" s="T263">adv</ta>
            <ta e="T265" id="Seg_8546" s="T264">v-v&gt;v-v:mood.pn</ta>
            <ta e="T267" id="Seg_8547" s="T266">n.[n:case]</ta>
            <ta e="T268" id="Seg_8548" s="T267">adv</ta>
            <ta e="T269" id="Seg_8549" s="T268">v.[v:pn]</ta>
            <ta e="T270" id="Seg_8550" s="T269">n-n:num-n:case-n:poss</ta>
            <ta e="T271" id="Seg_8551" s="T270">n-n&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T273" id="Seg_8552" s="T272">num</ta>
            <ta e="T274" id="Seg_8553" s="T273">adj</ta>
            <ta e="T275" id="Seg_8554" s="T274">n-n&gt;adv</ta>
            <ta e="T276" id="Seg_8555" s="T275">v-v&gt;ptcp</ta>
            <ta e="T277" id="Seg_8556" s="T276">adv-adv:case</ta>
            <ta e="T278" id="Seg_8557" s="T277">n-n:ins-n:num.[n:case]</ta>
            <ta e="T279" id="Seg_8558" s="T278">v-v:ins-v&gt;v-v:ins-v:pn</ta>
            <ta e="T281" id="Seg_8559" s="T280">adv</ta>
            <ta e="T282" id="Seg_8560" s="T281">dem</ta>
            <ta e="T283" id="Seg_8561" s="T282">n-n:ins-n&gt;adj</ta>
            <ta e="T284" id="Seg_8562" s="T283">n-n:case</ta>
            <ta e="T285" id="Seg_8563" s="T284">adv</ta>
            <ta e="T286" id="Seg_8564" s="T285">n-n:case</ta>
            <ta e="T287" id="Seg_8565" s="T286">v-v:ins-v:pn</ta>
            <ta e="T288" id="Seg_8566" s="T287">v-v:ins-v:pn</ta>
            <ta e="T289" id="Seg_8567" s="T288">n-n:num-n:case-n:poss</ta>
            <ta e="T290" id="Seg_8568" s="T289">ptcl</ta>
            <ta e="T291" id="Seg_8569" s="T290">interrog</ta>
            <ta e="T292" id="Seg_8570" s="T291">adv</ta>
            <ta e="T293" id="Seg_8571" s="T292">v-v:tense-v:pn</ta>
            <ta e="T294" id="Seg_8572" s="T293">n-n:case</ta>
            <ta e="T295" id="Seg_8573" s="T294">n.[n:case]</ta>
            <ta e="T296" id="Seg_8574" s="T295">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T298" id="Seg_8575" s="T297">n.[n:case]</ta>
            <ta e="T299" id="Seg_8576" s="T298">v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T300" id="Seg_8577" s="T299">dem</ta>
            <ta e="T301" id="Seg_8578" s="T300">n-n&gt;adj</ta>
            <ta e="T302" id="Seg_8579" s="T301">n.[n:case]</ta>
            <ta e="T303" id="Seg_8580" s="T302">n-n&gt;n.[n:case]</ta>
            <ta e="T304" id="Seg_8581" s="T303">dem-adj&gt;adv</ta>
            <ta e="T305" id="Seg_8582" s="T304">v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T306" id="Seg_8583" s="T305">adv</ta>
            <ta e="T307" id="Seg_8584" s="T306">n.[n:case]</ta>
            <ta e="T308" id="Seg_8585" s="T307">n-n:case-n:poss</ta>
            <ta e="T310" id="Seg_8586" s="T309">pers</ta>
            <ta e="T311" id="Seg_8587" s="T310">v</ta>
            <ta e="T312" id="Seg_8588" s="T311">pers</ta>
            <ta e="T313" id="Seg_8589" s="T312">ptcl</ta>
            <ta e="T314" id="Seg_8590" s="T313">v-v:tense-v:pn</ta>
            <ta e="T316" id="Seg_8591" s="T315">n-n:ins-n:num.[n:case]</ta>
            <ta e="T317" id="Seg_8592" s="T316">v-v:ins-v&gt;v-v:tense-v:pn</ta>
            <ta e="T319" id="Seg_8593" s="T318">v-v:ins-v&gt;v-v:ins-v:pn</ta>
            <ta e="T321" id="Seg_8594" s="T320">n-n:num.[n:case]</ta>
            <ta e="T322" id="Seg_8595" s="T321">adv</ta>
            <ta e="T323" id="Seg_8596" s="T322">v-v:pn</ta>
            <ta e="T324" id="Seg_8597" s="T323">n.[n:case]</ta>
            <ta e="T325" id="Seg_8598" s="T324">n.[n:case]-n:poss</ta>
            <ta e="T326" id="Seg_8599" s="T325">quant</ta>
            <ta e="T327" id="Seg_8600" s="T326">interrog</ta>
            <ta e="T328" id="Seg_8601" s="T327">n-n:case</ta>
            <ta e="T329" id="Seg_8602" s="T328">v-v:pn</ta>
            <ta e="T331" id="Seg_8603" s="T330">ptcl</ta>
            <ta e="T332" id="Seg_8604" s="T331">n.[n:case]</ta>
            <ta e="T333" id="Seg_8605" s="T332">n.[n:case]</ta>
            <ta e="T334" id="Seg_8606" s="T333">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T336" id="Seg_8607" s="T335">ptcl</ta>
            <ta e="T337" id="Seg_8608" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_8609" s="T337">pers</ta>
            <ta e="T339" id="Seg_8610" s="T338">ptcl</ta>
            <ta e="T340" id="Seg_8611" s="T339">n.[n:case]</ta>
            <ta e="T342" id="Seg_8612" s="T341">pers.[n:case]</ta>
            <ta e="T343" id="Seg_8613" s="T342">ptcl</ta>
            <ta e="T344" id="Seg_8614" s="T343">v-v&gt;v-v:pn</ta>
            <ta e="T345" id="Seg_8615" s="T344">pers.[n:case]</ta>
            <ta e="T346" id="Seg_8616" s="T345">ptcl</ta>
            <ta e="T347" id="Seg_8617" s="T346">v-v&gt;v-v:pn</ta>
            <ta e="T348" id="Seg_8618" s="T347">dem-n&gt;adj</ta>
            <ta e="T349" id="Seg_8619" s="T348">v-v&gt;v.[v:pn]</ta>
            <ta e="T350" id="Seg_8620" s="T349">dem-n&gt;adj</ta>
            <ta e="T351" id="Seg_8621" s="T350">n-n&gt;adj</ta>
            <ta e="T352" id="Seg_8622" s="T351">n.[n:case]</ta>
            <ta e="T353" id="Seg_8623" s="T352">n.[n:case]</ta>
            <ta e="T354" id="Seg_8624" s="T353">n.[n:case]</ta>
            <ta e="T355" id="Seg_8625" s="T354">dem</ta>
            <ta e="T357" id="Seg_8626" s="T356">adj</ta>
            <ta e="T358" id="Seg_8627" s="T357">pro-n:case</ta>
            <ta e="T359" id="Seg_8628" s="T358">v-v:tense-v:mood-v:pn</ta>
            <ta e="T361" id="Seg_8629" s="T360">n-n:case-n:poss</ta>
            <ta e="T362" id="Seg_8630" s="T361">pp-n:case</ta>
            <ta e="T363" id="Seg_8631" s="T362">v.[v:pn]</ta>
            <ta e="T364" id="Seg_8632" s="T363">n-n:case-n:poss</ta>
            <ta e="T365" id="Seg_8633" s="T364">pp-n:case</ta>
            <ta e="T367" id="Seg_8634" s="T366">num</ta>
            <ta e="T368" id="Seg_8635" s="T367">adj</ta>
            <ta e="T369" id="Seg_8636" s="T368">n-n&gt;adv</ta>
            <ta e="T370" id="Seg_8637" s="T369">n-n:case</ta>
            <ta e="T371" id="Seg_8638" s="T370">n-n:case</ta>
            <ta e="T372" id="Seg_8639" s="T371">dem</ta>
            <ta e="T373" id="Seg_8640" s="T372">n-n&gt;adj</ta>
            <ta e="T374" id="Seg_8641" s="T373">n-n&gt;n.[n:case]</ta>
            <ta e="T375" id="Seg_8642" s="T374">v-v:tense.mood.[v:pn]</ta>
            <ta e="T377" id="Seg_8643" s="T376">ptcl</ta>
            <ta e="T378" id="Seg_8644" s="T377">v-v:tense-v:mood.[v:pn]</ta>
            <ta e="T379" id="Seg_8645" s="T378">dem</ta>
            <ta e="T380" id="Seg_8646" s="T379">n-n:case</ta>
            <ta e="T381" id="Seg_8647" s="T380">dem</ta>
            <ta e="T382" id="Seg_8648" s="T381">dem</ta>
            <ta e="T383" id="Seg_8649" s="T382">n-n:num.[n:case]-n:poss</ta>
            <ta e="T385" id="Seg_8650" s="T384">n-n:num.[n:case]</ta>
            <ta e="T386" id="Seg_8651" s="T385">ptcl</ta>
            <ta e="T387" id="Seg_8652" s="T386">v-v:tense-v:pn</ta>
            <ta e="T388" id="Seg_8653" s="T387">dem-n&gt;adj</ta>
            <ta e="T389" id="Seg_8654" s="T388">n-n:case</ta>
            <ta e="T390" id="Seg_8655" s="T389">ptcl</ta>
            <ta e="T391" id="Seg_8656" s="T390">adj</ta>
            <ta e="T392" id="Seg_8657" s="T391">n-n&gt;adj</ta>
            <ta e="T393" id="Seg_8658" s="T392">n-n:case-n:poss</ta>
            <ta e="T394" id="Seg_8659" s="T393">n.[n:case]</ta>
            <ta e="T395" id="Seg_8660" s="T394">v-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T396" id="Seg_8661" s="T395">v-v:tense.mood-v:pn</ta>
            <ta e="T397" id="Seg_8662" s="T396">n-n:case</ta>
            <ta e="T399" id="Seg_8663" s="T398">ptcl</ta>
            <ta e="T400" id="Seg_8664" s="T399">dem</ta>
            <ta e="T401" id="Seg_8665" s="T400">n-n:case</ta>
            <ta e="T402" id="Seg_8666" s="T401">adv</ta>
            <ta e="T403" id="Seg_8667" s="T402">%%</ta>
            <ta e="T404" id="Seg_8668" s="T403">n-n:case</ta>
            <ta e="T405" id="Seg_8669" s="T404">n-n:case</ta>
            <ta e="T406" id="Seg_8670" s="T405">pp-n:case</ta>
            <ta e="T407" id="Seg_8671" s="T406">n-n:case</ta>
            <ta e="T408" id="Seg_8672" s="T407">dem</ta>
            <ta e="T409" id="Seg_8673" s="T408">num-n&gt;adj</ta>
            <ta e="T410" id="Seg_8674" s="T409">n-n&gt;adj</ta>
            <ta e="T411" id="Seg_8675" s="T410">n.[n:case]</ta>
            <ta e="T412" id="Seg_8676" s="T411">v-v:tense.[v:pn]</ta>
            <ta e="T414" id="Seg_8677" s="T413">ptcl.[n:case]</ta>
            <ta e="T415" id="Seg_8678" s="T414">adj</ta>
            <ta e="T416" id="Seg_8679" s="T415">n.[n:case]-n:poss</ta>
            <ta e="T417" id="Seg_8680" s="T416">v-v:tense.[v:pn]</ta>
            <ta e="T419" id="Seg_8681" s="T418">n.[n:case]-n:poss</ta>
            <ta e="T420" id="Seg_8682" s="T419">v</ta>
            <ta e="T421" id="Seg_8683" s="T420">n-n:case</ta>
            <ta e="T422" id="Seg_8684" s="T421">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T423" id="Seg_8685" s="T422">dem</ta>
            <ta e="T424" id="Seg_8686" s="T423">v-v:ins-v&gt;v</ta>
            <ta e="T425" id="Seg_8687" s="T424">n-n&gt;adj</ta>
            <ta e="T426" id="Seg_8688" s="T425">n.[n:case]</ta>
            <ta e="T427" id="Seg_8689" s="T426">n-n:case</ta>
            <ta e="T428" id="Seg_8690" s="T427">n-n:case</ta>
            <ta e="T429" id="Seg_8691" s="T428">n-n&gt;adj</ta>
            <ta e="T430" id="Seg_8692" s="T429">v-v&gt;v.[v:pn]</ta>
            <ta e="T432" id="Seg_8693" s="T431">ptcl</ta>
            <ta e="T433" id="Seg_8694" s="T432">n-n:case</ta>
            <ta e="T434" id="Seg_8695" s="T433">dem</ta>
            <ta e="T435" id="Seg_8696" s="T434">dem</ta>
            <ta e="T436" id="Seg_8697" s="T435">n-n:case</ta>
            <ta e="T437" id="Seg_8698" s="T436">ptcl</ta>
            <ta e="T438" id="Seg_8699" s="T437">v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T439" id="Seg_8700" s="T438">ptcl</ta>
            <ta e="T440" id="Seg_8701" s="T439">v-v:tense-v:mood-v:pn</ta>
            <ta e="T441" id="Seg_8702" s="T440">ptcl</ta>
            <ta e="T442" id="Seg_8703" s="T441">dem</ta>
            <ta e="T923" id="Seg_8704" s="T442">n-n:case</ta>
            <ta e="T444" id="Seg_8705" s="T443">ptcl</ta>
            <ta e="T445" id="Seg_8706" s="T444">dem</ta>
            <ta e="T446" id="Seg_8707" s="T445">v-v:pn</ta>
            <ta e="T448" id="Seg_8708" s="T447">%%-v:pn</ta>
            <ta e="T450" id="Seg_8709" s="T449">n-n:case</ta>
            <ta e="T451" id="Seg_8710" s="T450">ptcl</ta>
            <ta e="T452" id="Seg_8711" s="T451">pers</ta>
            <ta e="T453" id="Seg_8712" s="T452">ptcl</ta>
            <ta e="T454" id="Seg_8713" s="T453">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T455" id="Seg_8714" s="T454">dem</ta>
            <ta e="T456" id="Seg_8715" s="T455">dem</ta>
            <ta e="T457" id="Seg_8716" s="T456">n-n:case</ta>
            <ta e="T458" id="Seg_8717" s="T457">pp-adv&gt;adv-n&gt;adj</ta>
            <ta e="T459" id="Seg_8718" s="T458">n-n&gt;adv</ta>
            <ta e="T460" id="Seg_8719" s="T459">dem</ta>
            <ta e="T461" id="Seg_8720" s="T460">adj</ta>
            <ta e="T462" id="Seg_8721" s="T461">n.[n:case]</ta>
            <ta e="T463" id="Seg_8722" s="T462">pp-adv&gt;adv-n&gt;adj</ta>
            <ta e="T464" id="Seg_8723" s="T463">n-n&gt;adv</ta>
            <ta e="T466" id="Seg_8724" s="T465">dem</ta>
            <ta e="T467" id="Seg_8725" s="T466">n.[n:case]</ta>
            <ta e="T468" id="Seg_8726" s="T467">ptcl</ta>
            <ta e="T469" id="Seg_8727" s="T468">v-v:tense-v:mood-v:pn</ta>
            <ta e="T470" id="Seg_8728" s="T469">dem</ta>
            <ta e="T471" id="Seg_8729" s="T470">num-num&gt;num-n:ins-n&gt;adj</ta>
            <ta e="T472" id="Seg_8730" s="T471">n-n:ins-n&gt;adj</ta>
            <ta e="T473" id="Seg_8731" s="T472">n-n:case</ta>
            <ta e="T474" id="Seg_8732" s="T473">quant</ta>
            <ta e="T475" id="Seg_8733" s="T474">n.[n:case]</ta>
            <ta e="T476" id="Seg_8734" s="T475">v-v:tense-v:pn</ta>
            <ta e="T478" id="Seg_8735" s="T477">n-n&gt;adv</ta>
            <ta e="T479" id="Seg_8736" s="T478">v-n:poss</ta>
            <ta e="T480" id="Seg_8737" s="T479">adv-adv:case</ta>
            <ta e="T482" id="Seg_8738" s="T481">adv</ta>
            <ta e="T483" id="Seg_8739" s="T482">adv</ta>
            <ta e="T484" id="Seg_8740" s="T483">n.[n:case]</ta>
            <ta e="T485" id="Seg_8741" s="T484">adv</ta>
            <ta e="T486" id="Seg_8742" s="T485">v.[v:pn]</ta>
            <ta e="T487" id="Seg_8743" s="T486">n-n:case</ta>
            <ta e="T488" id="Seg_8744" s="T487">n-n&gt;adv</ta>
            <ta e="T489" id="Seg_8745" s="T488">n-n&gt;adj</ta>
            <ta e="T490" id="Seg_8746" s="T489">n.[n:case]</ta>
            <ta e="T491" id="Seg_8747" s="T490">v.[v:pn]</ta>
            <ta e="T493" id="Seg_8748" s="T492">adj</ta>
            <ta e="T494" id="Seg_8749" s="T493">ptcl</ta>
            <ta e="T495" id="Seg_8750" s="T494">adv</ta>
            <ta e="T496" id="Seg_8751" s="T495">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T497" id="Seg_8752" s="T496">ptcl</ta>
            <ta e="T498" id="Seg_8753" s="T497">adv</ta>
            <ta e="T499" id="Seg_8754" s="T498">v-v:ins-v:pn</ta>
            <ta e="T501" id="Seg_8755" s="T500">n-n:num.[n:case]-n:poss</ta>
            <ta e="T502" id="Seg_8756" s="T501">dem</ta>
            <ta e="T503" id="Seg_8757" s="T502">n.[n:case]</ta>
            <ta e="T504" id="Seg_8758" s="T503">ptcl</ta>
            <ta e="T505" id="Seg_8759" s="T504">v-v:tense-v:mood-v:pn</ta>
            <ta e="T506" id="Seg_8760" s="T505">n.[n:case]</ta>
            <ta e="T507" id="Seg_8761" s="T506">n.[n:case]</ta>
            <ta e="T508" id="Seg_8762" s="T507">pp-n:case</ta>
            <ta e="T509" id="Seg_8763" s="T508">v-v:ins-v:pn</ta>
            <ta e="T510" id="Seg_8764" s="T509">adv</ta>
            <ta e="T511" id="Seg_8765" s="T510">ptcl</ta>
            <ta e="T512" id="Seg_8766" s="T511">num</ta>
            <ta e="T513" id="Seg_8767" s="T512">n-n:case</ta>
            <ta e="T514" id="Seg_8768" s="T513">n-n:case</ta>
            <ta e="T515" id="Seg_8769" s="T514">pp-n:case</ta>
            <ta e="T516" id="Seg_8770" s="T515">v-v:ins-v:pn</ta>
            <ta e="T518" id="Seg_8771" s="T517">quant</ta>
            <ta e="T519" id="Seg_8772" s="T518">n-n:case</ta>
            <ta e="T520" id="Seg_8773" s="T519">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T521" id="Seg_8774" s="T520">dem</ta>
            <ta e="T522" id="Seg_8775" s="T521">n-n&gt;adj</ta>
            <ta e="T523" id="Seg_8776" s="T522">n-n:case</ta>
            <ta e="T525" id="Seg_8777" s="T524">dem</ta>
            <ta e="T526" id="Seg_8778" s="T525">n.[n:case]</ta>
            <ta e="T527" id="Seg_8779" s="T526">ptcl</ta>
            <ta e="T528" id="Seg_8780" s="T527">num</ta>
            <ta e="T529" id="Seg_8781" s="T528">v-v:tense-v:mood-v:pn</ta>
            <ta e="T533" id="Seg_8782" s="T532">ptcl</ta>
            <ta e="T534" id="Seg_8783" s="T533">v-v:mood.pn</ta>
            <ta e="T536" id="Seg_8784" s="T535">n-n&gt;n-n&gt;adj</ta>
            <ta e="T537" id="Seg_8785" s="T536">n.[n:case]</ta>
            <ta e="T538" id="Seg_8786" s="T537">interrog</ta>
            <ta e="T539" id="Seg_8787" s="T538">qv-v:tense.[v:pn]</ta>
            <ta e="T541" id="Seg_8788" s="T540">n.[n:case]</ta>
            <ta e="T542" id="Seg_8789" s="T541">adv</ta>
            <ta e="T543" id="Seg_8790" s="T542">adv</ta>
            <ta e="T544" id="Seg_8791" s="T543">v-v:ins.[v:pn]</ta>
            <ta e="T545" id="Seg_8792" s="T544">n-n&gt;adj</ta>
            <ta e="T546" id="Seg_8793" s="T545">n.[n:case]</ta>
            <ta e="T547" id="Seg_8794" s="T546">n.[n:case]</ta>
            <ta e="T548" id="Seg_8795" s="T547">adv</ta>
            <ta e="T549" id="Seg_8796" s="T548">n-n:num-n:case</ta>
            <ta e="T550" id="Seg_8797" s="T549">n-n:case</ta>
            <ta e="T551" id="Seg_8798" s="T550">v-v:ins.[v:pn]</ta>
            <ta e="T553" id="Seg_8799" s="T552">ptcl</ta>
            <ta e="T554" id="Seg_8800" s="T553">num-n.[n:case]</ta>
            <ta e="T556" id="Seg_8801" s="T555">adv</ta>
            <ta e="T558" id="Seg_8802" s="T557">n-n&gt;adj</ta>
            <ta e="T559" id="Seg_8803" s="T558">n-n:case</ta>
            <ta e="T927" id="Seg_8804" s="T559">v-v:inf</ta>
            <ta e="T561" id="Seg_8805" s="T560">preverb</ta>
            <ta e="T562" id="Seg_8806" s="T561">v-v:mood.pn</ta>
            <ta e="T564" id="Seg_8807" s="T563">dem</ta>
            <ta e="T565" id="Seg_8808" s="T564">v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T566" id="Seg_8809" s="T565">n-n:ins-n:num-n:case-n:poss</ta>
            <ta e="T567" id="Seg_8810" s="T566">v-v:inf.poss</ta>
            <ta e="T569" id="Seg_8811" s="T568">adv</ta>
            <ta e="T570" id="Seg_8812" s="T569">v-v:ins.[v:pn]</ta>
            <ta e="T571" id="Seg_8813" s="T570">interj</ta>
            <ta e="T572" id="Seg_8814" s="T571">adv-adv:case</ta>
            <ta e="T573" id="Seg_8815" s="T572">adv</ta>
            <ta e="T574" id="Seg_8816" s="T573">v-v&gt;ptcp</ta>
            <ta e="T575" id="Seg_8817" s="T574">n.[n:case]</ta>
            <ta e="T576" id="Seg_8818" s="T575">n.[n:case]</ta>
            <ta e="T577" id="Seg_8819" s="T576">v-v&gt;v-v&gt;v-v:tense.mood.[v:pn]</ta>
            <ta e="T581" id="Seg_8820" s="T580">ptcl</ta>
            <ta e="T582" id="Seg_8821" s="T581">ptcl</ta>
            <ta e="T583" id="Seg_8822" s="T582">v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T585" id="Seg_8823" s="T584">ptcl</ta>
            <ta e="T586" id="Seg_8824" s="T585">n-n&gt;adj</ta>
            <ta e="T587" id="Seg_8825" s="T586">n.[n:case]</ta>
            <ta e="T588" id="Seg_8826" s="T587">n-n:case</ta>
            <ta e="T591" id="Seg_8827" s="T590">n-n&gt;adj</ta>
            <ta e="T592" id="Seg_8828" s="T591">n.[n:case]</ta>
            <ta e="T593" id="Seg_8829" s="T592">n.[n:case]</ta>
            <ta e="T594" id="Seg_8830" s="T593">adv</ta>
            <ta e="T595" id="Seg_8831" s="T594">v-v:ins-v:pn</ta>
            <ta e="T596" id="Seg_8832" s="T595">n-n&gt;adj</ta>
            <ta e="T597" id="Seg_8833" s="T596">n.[n:case]-n:poss</ta>
            <ta e="T598" id="Seg_8834" s="T597">adv</ta>
            <ta e="T599" id="Seg_8835" s="T598">v-v&gt;v-v:ins.[v:pn]</ta>
            <ta e="T601" id="Seg_8836" s="T600">adv</ta>
            <ta e="T602" id="Seg_8837" s="T601">adv</ta>
            <ta e="T603" id="Seg_8838" s="T602">v-v&gt;v-v:ins.[v:pn]</ta>
            <ta e="T605" id="Seg_8839" s="T604">adv</ta>
            <ta e="T606" id="Seg_8840" s="T605">v-v&gt;v-v:ins.[v:pn]</ta>
            <ta e="T607" id="Seg_8841" s="T606">adv</ta>
            <ta e="T608" id="Seg_8842" s="T607">num</ta>
            <ta e="T609" id="Seg_8843" s="T608">n-n&gt;n-n:num.[n:case]</ta>
            <ta e="T610" id="Seg_8844" s="T609">adv</ta>
            <ta e="T611" id="Seg_8845" s="T610">v-v:ins-v:pn</ta>
            <ta e="T613" id="Seg_8846" s="T612">adv</ta>
            <ta e="T614" id="Seg_8847" s="T613">n-n:case</ta>
            <ta e="T615" id="Seg_8848" s="T614">n.[n:case]</ta>
            <ta e="T616" id="Seg_8849" s="T615">adv-adv:case</ta>
            <ta e="T617" id="Seg_8850" s="T616">v-v:tense.[v:pn]</ta>
            <ta e="T618" id="Seg_8851" s="T617">adv-%%-adv:case</ta>
            <ta e="T620" id="Seg_8852" s="T619">adv</ta>
            <ta e="T621" id="Seg_8853" s="T620">v-v:ins-v:pn</ta>
            <ta e="T622" id="Seg_8854" s="T621">adv</ta>
            <ta e="T623" id="Seg_8855" s="T622">adv</ta>
            <ta e="T624" id="Seg_8856" s="T623">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T625" id="Seg_8857" s="T624">n-n:num-n:ins-n:case</ta>
            <ta e="T626" id="Seg_8858" s="T625">n-n:case</ta>
            <ta e="T627" id="Seg_8859" s="T626">dem</ta>
            <ta e="T628" id="Seg_8860" s="T627">n-n:num-n:case</ta>
            <ta e="T629" id="Seg_8861" s="T628">n-n:case</ta>
            <ta e="T630" id="Seg_8862" s="T629">dem</ta>
            <ta e="T631" id="Seg_8863" s="T630">num</ta>
            <ta e="T632" id="Seg_8864" s="T631">n-n&gt;n-n:num.[n:case]</ta>
            <ta e="T634" id="Seg_8865" s="T633">adv</ta>
            <ta e="T635" id="Seg_8866" s="T634">v-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T636" id="Seg_8867" s="T635">v-v:ins-v:pn</ta>
            <ta e="T638" id="Seg_8868" s="T637">dem</ta>
            <ta e="T639" id="Seg_8869" s="T638">adv-adv:case</ta>
            <ta e="T640" id="Seg_8870" s="T639">dem</ta>
            <ta e="T641" id="Seg_8871" s="T640">n-n&gt;adj</ta>
            <ta e="T643" id="Seg_8872" s="T642">n-%%-n:num-n:ins-n:case</ta>
            <ta e="T644" id="Seg_8873" s="T643">ptcl</ta>
            <ta e="T645" id="Seg_8874" s="T644">v-v&gt;v-v:tense-v:mood.[v:pn]</ta>
            <ta e="T647" id="Seg_8875" s="T646">dem</ta>
            <ta e="T648" id="Seg_8876" s="T647">n-n:ins-n&gt;adj</ta>
            <ta e="T649" id="Seg_8877" s="T648">n-n:num.[n:case]</ta>
            <ta e="T650" id="Seg_8878" s="T649">adv-adv&gt;adv</ta>
            <ta e="T651" id="Seg_8879" s="T650">dem-%%</ta>
            <ta e="T652" id="Seg_8880" s="T651">v-v:tense-v:pn</ta>
            <ta e="T653" id="Seg_8881" s="T652">adv-adv&gt;adv</ta>
            <ta e="T654" id="Seg_8882" s="T653">dem</ta>
            <ta e="T655" id="Seg_8883" s="T654">n-n:obl.poss-n:case</ta>
            <ta e="T656" id="Seg_8884" s="T655">v-v:tense-v:pn</ta>
            <ta e="T658" id="Seg_8885" s="T657">adv-adv:case</ta>
            <ta e="T659" id="Seg_8886" s="T658">dem</ta>
            <ta e="T660" id="Seg_8887" s="T659">n-n:case</ta>
            <ta e="T661" id="Seg_8888" s="T660">num</ta>
            <ta e="T662" id="Seg_8889" s="T661">n.[n:case]</ta>
            <ta e="T663" id="Seg_8890" s="T662">adv</ta>
            <ta e="T664" id="Seg_8891" s="T663">v-v:tense-v:mood-v:pn</ta>
            <ta e="T666" id="Seg_8892" s="T665">n.[n:case]-n:poss</ta>
            <ta e="T667" id="Seg_8893" s="T666">v-%%</ta>
            <ta e="T669" id="Seg_8894" s="T668">interrog.[n:case]-n:poss</ta>
            <ta e="T670" id="Seg_8895" s="T669">v</ta>
            <ta e="T671" id="Seg_8896" s="T670">n-n&gt;adj</ta>
            <ta e="T672" id="Seg_8897" s="T671">n.[n:case]</ta>
            <ta e="T673" id="Seg_8898" s="T672">n.[n:case]</ta>
            <ta e="T674" id="Seg_8899" s="T673">n-n:case</ta>
            <ta e="T675" id="Seg_8900" s="T674">n.[n:case]</ta>
            <ta e="T676" id="Seg_8901" s="T675">%%</ta>
            <ta e="T677" id="Seg_8902" s="T676">adv</ta>
            <ta e="T678" id="Seg_8903" s="T677">v-v:ins-v:pn</ta>
            <ta e="T679" id="Seg_8904" s="T678">dem</ta>
            <ta e="T680" id="Seg_8905" s="T679">dem</ta>
            <ta e="T681" id="Seg_8906" s="T680">n-n&gt;adj</ta>
            <ta e="T682" id="Seg_8907" s="T681">n.[n:case]</ta>
            <ta e="T683" id="Seg_8908" s="T682">ptcl</ta>
            <ta e="T684" id="Seg_8909" s="T683">pers-n:ins-n:case</ta>
            <ta e="T685" id="Seg_8910" s="T684">v-v:tense-v:pn</ta>
            <ta e="T687" id="Seg_8911" s="T686">adv</ta>
            <ta e="T688" id="Seg_8912" s="T687">n-n:case</ta>
            <ta e="T689" id="Seg_8913" s="T688">pers-n:ins-n:case</ta>
            <ta e="T690" id="Seg_8914" s="T689">n-n:case</ta>
            <ta e="T691" id="Seg_8915" s="T690">n-n&gt;adj</ta>
            <ta e="T692" id="Seg_8916" s="T691">dem</ta>
            <ta e="T693" id="Seg_8917" s="T692">n.[n:case]</ta>
            <ta e="T695" id="Seg_8918" s="T694">n-n&gt;adj</ta>
            <ta e="T696" id="Seg_8919" s="T695">n.[n:case]</ta>
            <ta e="T697" id="Seg_8920" s="T696">n.[n:case]</ta>
            <ta e="T698" id="Seg_8921" s="T697">n-%%</ta>
            <ta e="T699" id="Seg_8922" s="T698">n-n:num.[n:case]-n:poss</ta>
            <ta e="T700" id="Seg_8923" s="T699">v-v:tense-v:mood-v:pn</ta>
            <ta e="T702" id="Seg_8924" s="T701">dem</ta>
            <ta e="T703" id="Seg_8925" s="T702">v-v&gt;ptcp</ta>
            <ta e="T704" id="Seg_8926" s="T703">n-n:ins-n:num-n:ins-n:case</ta>
            <ta e="T705" id="Seg_8927" s="T704">dem</ta>
            <ta e="T706" id="Seg_8928" s="T705">n-n:case</ta>
            <ta e="T708" id="Seg_8929" s="T707">pro-n:case</ta>
            <ta e="T709" id="Seg_8930" s="T708">n-n:num.[n:case]</ta>
            <ta e="T710" id="Seg_8931" s="T709">num</ta>
            <ta e="T712" id="Seg_8932" s="T711">dem</ta>
            <ta e="T713" id="Seg_8933" s="T712">num</ta>
            <ta e="T714" id="Seg_8934" s="T713">n-n:num.[n:case]</ta>
            <ta e="T716" id="Seg_8935" s="T715">n-n:case</ta>
            <ta e="T717" id="Seg_8936" s="T716">v-v&gt;adv</ta>
            <ta e="T718" id="Seg_8937" s="T717">adv</ta>
            <ta e="T719" id="Seg_8938" s="T718">v-v:pn</ta>
            <ta e="T720" id="Seg_8939" s="T719">dem</ta>
            <ta e="T721" id="Seg_8940" s="T720">dem</ta>
            <ta e="T722" id="Seg_8941" s="T721">n-n:case</ta>
            <ta e="T724" id="Seg_8942" s="T723">dem</ta>
            <ta e="T725" id="Seg_8943" s="T724">n-n:ins-n&gt;v-v:ins-v:pn</ta>
            <ta e="T727" id="Seg_8944" s="T726">ptcl</ta>
            <ta e="T728" id="Seg_8945" s="T727">dem</ta>
            <ta e="T729" id="Seg_8946" s="T728">dem-n&gt;n.[n:case]</ta>
            <ta e="T730" id="Seg_8947" s="T729">adv</ta>
            <ta e="T733" id="Seg_8948" s="T732">n-n:num.[n:case]-n:poss</ta>
            <ta e="T734" id="Seg_8949" s="T733">dem</ta>
            <ta e="T735" id="Seg_8950" s="T734">dem</ta>
            <ta e="T736" id="Seg_8951" s="T735">v-v:tense-v:mood-v:pn</ta>
            <ta e="T737" id="Seg_8952" s="T736">n-n:case</ta>
            <ta e="T738" id="Seg_8953" s="T737">n-n&gt;v-v&gt;adv</ta>
            <ta e="T739" id="Seg_8954" s="T738">n-n&gt;v-v&gt;adv</ta>
            <ta e="T743" id="Seg_8955" s="T742">adv</ta>
            <ta e="T744" id="Seg_8956" s="T743">n-n:ins-n&gt;v-v:ins-v:pn</ta>
            <ta e="T745" id="Seg_8957" s="T744">%%-v:pn</ta>
            <ta e="T746" id="Seg_8958" s="T745">num</ta>
            <ta e="T747" id="Seg_8959" s="T746">n-n&gt;adv</ta>
            <ta e="T748" id="Seg_8960" s="T747">v-v:pn</ta>
            <ta e="T750" id="Seg_8961" s="T749">v-v:inf-v-v:ins-v:pn</ta>
            <ta e="T752" id="Seg_8962" s="T751">n-n:num.[n:case]-n:poss</ta>
            <ta e="T753" id="Seg_8963" s="T752">n-n:case</ta>
            <ta e="T754" id="Seg_8964" s="T753">preverb</ta>
            <ta e="T755" id="Seg_8965" s="T754">v-v:pn</ta>
            <ta e="T756" id="Seg_8966" s="T755">n-%%</ta>
            <ta e="T757" id="Seg_8967" s="T756">adv</ta>
            <ta e="T759" id="Seg_8968" s="T758">v-v&gt;adv</ta>
            <ta e="T760" id="Seg_8969" s="T759">v-v:pn</ta>
            <ta e="T762" id="Seg_8970" s="T761">interrog.[n:case]</ta>
            <ta e="T763" id="Seg_8971" s="T762">v.[v:pn]</ta>
            <ta e="T765" id="Seg_8972" s="T764">n.[n:case]</ta>
            <ta e="T766" id="Seg_8973" s="T765">n.[n:case]</ta>
            <ta e="T767" id="Seg_8974" s="T766">n-n:case</ta>
            <ta e="T768" id="Seg_8975" s="T767">dem</ta>
            <ta e="T769" id="Seg_8976" s="T768">n-n:case.poss</ta>
            <ta e="T770" id="Seg_8977" s="T769">%%-n:num.[n:case]</ta>
            <ta e="T772" id="Seg_8978" s="T771">adv</ta>
            <ta e="T773" id="Seg_8979" s="T772">ptcl</ta>
            <ta e="T774" id="Seg_8980" s="T773">v-v:tense-v:pn</ta>
            <ta e="T775" id="Seg_8981" s="T774">n-n&gt;adj</ta>
            <ta e="T776" id="Seg_8982" s="T775">n.[n:case]</ta>
            <ta e="T777" id="Seg_8983" s="T776">v-v:tense-v:pn</ta>
            <ta e="T778" id="Seg_8984" s="T777">n.[n:case]</ta>
            <ta e="T779" id="Seg_8985" s="T778">n.[n:case]</ta>
            <ta e="T780" id="Seg_8986" s="T779">n-n:num.[n:case]</ta>
            <ta e="T781" id="Seg_8987" s="T780">v-v:tense-v:pn</ta>
            <ta e="T782" id="Seg_8988" s="T781">pers-v:ins-n:num.[n:case]</ta>
            <ta e="T783" id="Seg_8989" s="T782">adv</ta>
            <ta e="T784" id="Seg_8990" s="T783">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T786" id="Seg_8991" s="T785">dem</ta>
            <ta e="T787" id="Seg_8992" s="T786">num</ta>
            <ta e="T788" id="Seg_8993" s="T787">n.[n:case]-n:poss</ta>
            <ta e="T790" id="Seg_8994" s="T789">%%-v:pn</ta>
            <ta e="T792" id="Seg_8995" s="T791">adv</ta>
            <ta e="T793" id="Seg_8996" s="T792">num-num&gt;adv</ta>
            <ta e="T795" id="Seg_8997" s="T794">v-v&gt;v-v&gt;adv</ta>
            <ta e="T796" id="Seg_8998" s="T795">v-v:pn</ta>
            <ta e="T798" id="Seg_8999" s="T797">n-n:case</ta>
            <ta e="T799" id="Seg_9000" s="T798">n.[n:case]-n:poss</ta>
            <ta e="T800" id="Seg_9001" s="T799">dem</ta>
            <ta e="T801" id="Seg_9002" s="T800">adj</ta>
            <ta e="T802" id="Seg_9003" s="T801">n-n:case-n:poss</ta>
            <ta e="T803" id="Seg_9004" s="T802">n.[n:case]</ta>
            <ta e="T805" id="Seg_9005" s="T804">n.[n:case]-n:poss</ta>
            <ta e="T806" id="Seg_9006" s="T805">adv</ta>
            <ta e="T807" id="Seg_9007" s="T806">v-v:tense-v:pn</ta>
            <ta e="T809" id="Seg_9008" s="T808">n-n:case</ta>
            <ta e="T810" id="Seg_9009" s="T809">interrog-n&gt;adj</ta>
            <ta e="T811" id="Seg_9010" s="T810">n-n&gt;adv</ta>
            <ta e="T812" id="Seg_9011" s="T811">dem</ta>
            <ta e="T813" id="Seg_9012" s="T812">n-n:case</ta>
            <ta e="T814" id="Seg_9013" s="T813">n-n:case</ta>
            <ta e="T815" id="Seg_9014" s="T814">v-v:pn</ta>
            <ta e="T817" id="Seg_9015" s="T816">n-n:case</ta>
            <ta e="T818" id="Seg_9016" s="T817">n-n:case</ta>
            <ta e="T819" id="Seg_9017" s="T818">v-v:ins-v:pn</ta>
            <ta e="T820" id="Seg_9018" s="T819">n.[n:case]</ta>
            <ta e="T821" id="Seg_9019" s="T820">interj-v&gt;v-v:ins.[v:pn]</ta>
            <ta e="T823" id="Seg_9020" s="T822">n.[n:case]</ta>
            <ta e="T824" id="Seg_9021" s="T823">dem-n&gt;adj</ta>
            <ta e="T825" id="Seg_9022" s="T824">v-v&gt;v.[v:pn]</ta>
            <ta e="T826" id="Seg_9023" s="T825">n-n&gt;adj</ta>
            <ta e="T827" id="Seg_9024" s="T826">n.[n:case]</ta>
            <ta e="T828" id="Seg_9025" s="T827">n.[n:case]</ta>
            <ta e="T829" id="Seg_9026" s="T828">ptcl</ta>
            <ta e="T831" id="Seg_9027" s="T830">n.[n:case]-n:poss</ta>
            <ta e="T832" id="Seg_9028" s="T831">interrog.[n:case]</ta>
            <ta e="T833" id="Seg_9029" s="T832">v-v:tense-v:pn</ta>
            <ta e="T834" id="Seg_9030" s="T833">ptcl</ta>
            <ta e="T835" id="Seg_9031" s="T834">n.[n:case]</ta>
            <ta e="T836" id="Seg_9032" s="T835">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T838" id="Seg_9033" s="T837">ptcl</ta>
            <ta e="T839" id="Seg_9034" s="T838">adj</ta>
            <ta e="T840" id="Seg_9035" s="T839">n-n:case</ta>
            <ta e="T841" id="Seg_9036" s="T840">n.[n:case]</ta>
            <ta e="T842" id="Seg_9037" s="T841">adv</ta>
            <ta e="T843" id="Seg_9038" s="T842">v.[v:pn]</ta>
            <ta e="T845" id="Seg_9039" s="T844">dem</ta>
            <ta e="T846" id="Seg_9040" s="T845">n-n&gt;adj</ta>
            <ta e="T847" id="Seg_9041" s="T846">n.[n:case]</ta>
            <ta e="T848" id="Seg_9042" s="T847">n-n:case.poss</ta>
            <ta e="T849" id="Seg_9043" s="T848">adj</ta>
            <ta e="T850" id="Seg_9044" s="T849">n.[n:case]-n:poss</ta>
            <ta e="T852" id="Seg_9045" s="T851">adv</ta>
            <ta e="T853" id="Seg_9046" s="T852">ptcl</ta>
            <ta e="T854" id="Seg_9047" s="T853">v.[v:pn]</ta>
            <ta e="T855" id="Seg_9048" s="T854">interrog.[n:case]</ta>
            <ta e="T856" id="Seg_9049" s="T855">v.[v:pn]</ta>
            <ta e="T857" id="Seg_9050" s="T856">n.[n:case]-n:poss</ta>
            <ta e="T859" id="Seg_9051" s="T858">dem</ta>
            <ta e="T860" id="Seg_9052" s="T859">interrog.[n:case]</ta>
            <ta e="T861" id="Seg_9053" s="T860">n-n:ins-n:num.[n:case]</ta>
            <ta e="T862" id="Seg_9054" s="T861">v-v:ins.[v:pn]</ta>
            <ta e="T864" id="Seg_9055" s="T863">n.[n:case]</ta>
            <ta e="T865" id="Seg_9056" s="T864">n.[n:case]-n:poss</ta>
            <ta e="T866" id="Seg_9057" s="T865">dem</ta>
            <ta e="T867" id="Seg_9058" s="T866">v-v&gt;v-v&gt;v.[v:pn]</ta>
            <ta e="T868" id="Seg_9059" s="T867">dem</ta>
            <ta e="T869" id="Seg_9060" s="T868">n-n:case</ta>
            <ta e="T871" id="Seg_9061" s="T870">ptcl</ta>
            <ta e="T872" id="Seg_9062" s="T871">pers</ta>
            <ta e="T873" id="Seg_9063" s="T872">ptcl</ta>
            <ta e="T874" id="Seg_9064" s="T873">n.[n:case]-n:poss</ta>
            <ta e="T875" id="Seg_9065" s="T874">ptcl</ta>
            <ta e="T876" id="Seg_9066" s="T875">pers</ta>
            <ta e="T877" id="Seg_9067" s="T876">n-n:case</ta>
            <ta e="T878" id="Seg_9068" s="T877">v-v:tense-v:pn</ta>
            <ta e="T879" id="Seg_9069" s="T878">ptcl</ta>
            <ta e="T881" id="Seg_9070" s="T880">n.[n:case]</ta>
            <ta e="T882" id="Seg_9071" s="T881">ptcl</ta>
            <ta e="T883" id="Seg_9072" s="T882">dem-n&gt;adj</ta>
            <ta e="T884" id="Seg_9073" s="T883">pers</ta>
            <ta e="T894" id="Seg_9074" s="T893">ptcl</ta>
            <ta e="T895" id="Seg_9075" s="T894">adv</ta>
            <ta e="T896" id="Seg_9076" s="T895">v-v:mood.pn</ta>
            <ta e="T898" id="Seg_9077" s="T897">adv</ta>
            <ta e="T899" id="Seg_9078" s="T898">adv</ta>
            <ta e="T900" id="Seg_9079" s="T899">ptcl</ta>
            <ta e="T901" id="Seg_9080" s="T900">v-v:tense-v:mood-v:pn</ta>
            <ta e="T902" id="Seg_9081" s="T901">adv</ta>
            <ta e="T903" id="Seg_9082" s="T902">ptcl</ta>
            <ta e="T904" id="Seg_9083" s="T903">ptcl</ta>
            <ta e="T905" id="Seg_9084" s="T904">adv</ta>
            <ta e="T906" id="Seg_9085" s="T905">adv</ta>
            <ta e="T907" id="Seg_9086" s="T906">v-v:tense-v:pn</ta>
            <ta e="T908" id="Seg_9087" s="T907">dem</ta>
            <ta e="T909" id="Seg_9088" s="T908">n-n:ins-n:num.[n:case]</ta>
            <ta e="T910" id="Seg_9089" s="T909">dem</ta>
            <ta e="T912" id="Seg_9090" s="T911">ptcl</ta>
            <ta e="T913" id="Seg_9091" s="T912">n-n&gt;adj</ta>
            <ta e="T914" id="Seg_9092" s="T913">n-n:case</ta>
            <ta e="T915" id="Seg_9093" s="T914">n-n:case</ta>
            <ta e="T916" id="Seg_9094" s="T915">v-v:tense-v:mood-v:pn</ta>
            <ta e="T918" id="Seg_9095" s="T917">conj</ta>
            <ta e="T919" id="Seg_9096" s="T918">dem</ta>
            <ta e="T920" id="Seg_9097" s="T919">n.[n:case]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T11" id="Seg_9098" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_9099" s="T11">v</ta>
            <ta e="T13" id="Seg_9100" s="T12">adv</ta>
            <ta e="T14" id="Seg_9101" s="T13">adj</ta>
            <ta e="T15" id="Seg_9102" s="T14">interrog</ta>
            <ta e="T17" id="Seg_9103" s="T16">n</ta>
            <ta e="T18" id="Seg_9104" s="T17">dem</ta>
            <ta e="T19" id="Seg_9105" s="T18">adj</ta>
            <ta e="T20" id="Seg_9106" s="T19">n</ta>
            <ta e="T21" id="Seg_9107" s="T20">n</ta>
            <ta e="T22" id="Seg_9108" s="T21">clit</ta>
            <ta e="T23" id="Seg_9109" s="T22">v</ta>
            <ta e="T25" id="Seg_9110" s="T24">adj</ta>
            <ta e="T26" id="Seg_9111" s="T25">n</ta>
            <ta e="T27" id="Seg_9112" s="T26">n</ta>
            <ta e="T28" id="Seg_9113" s="T27">dem</ta>
            <ta e="T29" id="Seg_9114" s="T28">adv</ta>
            <ta e="T30" id="Seg_9115" s="T29">num</ta>
            <ta e="T31" id="Seg_9116" s="T30">n</ta>
            <ta e="T32" id="Seg_9117" s="T31">v</ta>
            <ta e="T34" id="Seg_9118" s="T33">conj</ta>
            <ta e="T35" id="Seg_9119" s="T34">num</ta>
            <ta e="T36" id="Seg_9120" s="T35">n</ta>
            <ta e="T38" id="Seg_9121" s="T37">adv</ta>
            <ta e="T39" id="Seg_9122" s="T38">v</ta>
            <ta e="T41" id="Seg_9123" s="T40">n</ta>
            <ta e="T42" id="Seg_9124" s="T41">adv</ta>
            <ta e="T43" id="Seg_9125" s="T42">adv</ta>
            <ta e="T44" id="Seg_9126" s="T43">v</ta>
            <ta e="T45" id="Seg_9127" s="T44">pers</ta>
            <ta e="T46" id="Seg_9128" s="T45">v</ta>
            <ta e="T48" id="Seg_9129" s="T47">n</ta>
            <ta e="T49" id="Seg_9130" s="T48">n</ta>
            <ta e="T50" id="Seg_9131" s="T49">adv</ta>
            <ta e="T51" id="Seg_9132" s="T50">v</ta>
            <ta e="T52" id="Seg_9133" s="T51">adv</ta>
            <ta e="T54" id="Seg_9134" s="T53">adv</ta>
            <ta e="T55" id="Seg_9135" s="T54">adv</ta>
            <ta e="T56" id="Seg_9136" s="T55">v</ta>
            <ta e="T57" id="Seg_9137" s="T56">adv</ta>
            <ta e="T58" id="Seg_9138" s="T57">n</ta>
            <ta e="T59" id="Seg_9139" s="T58">adv</ta>
            <ta e="T60" id="Seg_9140" s="T59">adv</ta>
            <ta e="T61" id="Seg_9141" s="T60">adv</ta>
            <ta e="T62" id="Seg_9142" s="T61">interrog</ta>
            <ta e="T63" id="Seg_9143" s="T62">v</ta>
            <ta e="T64" id="Seg_9144" s="T63">adv</ta>
            <ta e="T66" id="Seg_9145" s="T65">dem</ta>
            <ta e="T67" id="Seg_9146" s="T66">n</ta>
            <ta e="T68" id="Seg_9147" s="T67">adj</ta>
            <ta e="T69" id="Seg_9148" s="T68">n</ta>
            <ta e="T70" id="Seg_9149" s="T69">v</ta>
            <ta e="T71" id="Seg_9150" s="T70">dem</ta>
            <ta e="T72" id="Seg_9151" s="T71">n</ta>
            <ta e="T74" id="Seg_9152" s="T73">dem</ta>
            <ta e="T75" id="Seg_9153" s="T74">n</ta>
            <ta e="T77" id="Seg_9154" s="T76">adj</ta>
            <ta e="T78" id="Seg_9155" s="T77">n</ta>
            <ta e="T79" id="Seg_9156" s="T78">n</ta>
            <ta e="T80" id="Seg_9157" s="T79">ptcl</ta>
            <ta e="T81" id="Seg_9158" s="T80">adv</ta>
            <ta e="T82" id="Seg_9159" s="T81">ptcl</ta>
            <ta e="T83" id="Seg_9160" s="T82">pers</ta>
            <ta e="T84" id="Seg_9161" s="T83">v</ta>
            <ta e="T85" id="Seg_9162" s="T84">pers</ta>
            <ta e="T86" id="Seg_9163" s="T85">n</ta>
            <ta e="T87" id="Seg_9164" s="T86">pers</ta>
            <ta e="T88" id="Seg_9165" s="T87">ptcl</ta>
            <ta e="T89" id="Seg_9166" s="T88">v</ta>
            <ta e="T90" id="Seg_9167" s="T89">adv</ta>
            <ta e="T92" id="Seg_9168" s="T91">n</ta>
            <ta e="T93" id="Seg_9169" s="T92">adv</ta>
            <ta e="T94" id="Seg_9170" s="T93">adv</ta>
            <ta e="T95" id="Seg_9171" s="T94">adv</ta>
            <ta e="T96" id="Seg_9172" s="T95">v</ta>
            <ta e="T97" id="Seg_9173" s="T96">adj</ta>
            <ta e="T98" id="Seg_9174" s="T97">n</ta>
            <ta e="T99" id="Seg_9175" s="T98">n</ta>
            <ta e="T100" id="Seg_9176" s="T99">ptcp</ta>
            <ta e="T102" id="Seg_9177" s="T101">dem</ta>
            <ta e="T103" id="Seg_9178" s="T102">adj</ta>
            <ta e="T104" id="Seg_9179" s="T103">n</ta>
            <ta e="T106" id="Seg_9180" s="T105">v</ta>
            <ta e="T107" id="Seg_9181" s="T106">v</ta>
            <ta e="T108" id="Seg_9182" s="T107">ptcl</ta>
            <ta e="T109" id="Seg_9183" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_9184" s="T109">n</ta>
            <ta e="T111" id="Seg_9185" s="T110">ptcl</ta>
            <ta e="T112" id="Seg_9186" s="T111">v</ta>
            <ta e="T114" id="Seg_9187" s="T113">v</ta>
            <ta e="T116" id="Seg_9188" s="T115">adv</ta>
            <ta e="T117" id="Seg_9189" s="T116">adv</ta>
            <ta e="T118" id="Seg_9190" s="T117">n</ta>
            <ta e="T119" id="Seg_9191" s="T118">interrog</ta>
            <ta e="T120" id="Seg_9192" s="T119">qv</ta>
            <ta e="T122" id="Seg_9193" s="T121">n</ta>
            <ta e="T123" id="Seg_9194" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_9195" s="T123">adv</ta>
            <ta e="T125" id="Seg_9196" s="T124">ptcl</ta>
            <ta e="T126" id="Seg_9197" s="T125">v</ta>
            <ta e="T128" id="Seg_9198" s="T127">n</ta>
            <ta e="T129" id="Seg_9199" s="T128">adv</ta>
            <ta e="T130" id="Seg_9200" s="T129">ptcl</ta>
            <ta e="T131" id="Seg_9201" s="T130">v</ta>
            <ta e="T132" id="Seg_9202" s="T131">dem</ta>
            <ta e="T133" id="Seg_9203" s="T132">adj</ta>
            <ta e="T134" id="Seg_9204" s="T133">adj</ta>
            <ta e="T135" id="Seg_9205" s="T134">n</ta>
            <ta e="T137" id="Seg_9206" s="T136">adv</ta>
            <ta e="T138" id="Seg_9207" s="T137">v</ta>
            <ta e="T139" id="Seg_9208" s="T138">adj</ta>
            <ta e="T140" id="Seg_9209" s="T139">n</ta>
            <ta e="T141" id="Seg_9210" s="T140">n</ta>
            <ta e="T142" id="Seg_9211" s="T141">v</ta>
            <ta e="T143" id="Seg_9212" s="T142">adv</ta>
            <ta e="T145" id="Seg_9213" s="T144">n</ta>
            <ta e="T146" id="Seg_9214" s="T145">n</ta>
            <ta e="T147" id="Seg_9215" s="T146">v</ta>
            <ta e="T148" id="Seg_9216" s="T147">interrog</ta>
            <ta e="T150" id="Seg_9217" s="T149">n</ta>
            <ta e="T151" id="Seg_9218" s="T150">interrog</ta>
            <ta e="T152" id="Seg_9219" s="T151">quant</ta>
            <ta e="T153" id="Seg_9220" s="T152">interrog</ta>
            <ta e="T154" id="Seg_9221" s="T153">v</ta>
            <ta e="T156" id="Seg_9222" s="T155">n</ta>
            <ta e="T157" id="Seg_9223" s="T156">adv</ta>
            <ta e="T158" id="Seg_9224" s="T157">v</ta>
            <ta e="T159" id="Seg_9225" s="T158">v</ta>
            <ta e="T161" id="Seg_9226" s="T160">n</ta>
            <ta e="T162" id="Seg_9227" s="T161">adv</ta>
            <ta e="T163" id="Seg_9228" s="T162">v</ta>
            <ta e="T165" id="Seg_9229" s="T164">n</ta>
            <ta e="T166" id="Seg_9230" s="T165">v</ta>
            <ta e="T168" id="Seg_9231" s="T167">dem</ta>
            <ta e="T169" id="Seg_9232" s="T168">dem</ta>
            <ta e="T170" id="Seg_9233" s="T169">n</ta>
            <ta e="T171" id="Seg_9234" s="T170">dem</ta>
            <ta e="T172" id="Seg_9235" s="T171">n</ta>
            <ta e="T173" id="Seg_9236" s="T172">n</ta>
            <ta e="T174" id="Seg_9237" s="T173">adj</ta>
            <ta e="T175" id="Seg_9238" s="T174">n</ta>
            <ta e="T176" id="Seg_9239" s="T175">v</ta>
            <ta e="T177" id="Seg_9240" s="T176">dem</ta>
            <ta e="T179" id="Seg_9241" s="T178">n</ta>
            <ta e="T180" id="Seg_9242" s="T179">n</ta>
            <ta e="T181" id="Seg_9243" s="T180">v</ta>
            <ta e="T183" id="Seg_9244" s="T182">interj</ta>
            <ta e="T184" id="Seg_9245" s="T183">adj</ta>
            <ta e="T185" id="Seg_9246" s="T184">n</ta>
            <ta e="T186" id="Seg_9247" s="T185">n</ta>
            <ta e="T187" id="Seg_9248" s="T186">dem</ta>
            <ta e="T188" id="Seg_9249" s="T187">preverb</ta>
            <ta e="T189" id="Seg_9250" s="T188">adj</ta>
            <ta e="T190" id="Seg_9251" s="T189">adj</ta>
            <ta e="T191" id="Seg_9252" s="T190">n</ta>
            <ta e="T193" id="Seg_9253" s="T192">adv</ta>
            <ta e="T194" id="Seg_9254" s="T193">ptcl</ta>
            <ta e="T195" id="Seg_9255" s="T194">ptcp</ta>
            <ta e="T196" id="Seg_9256" s="T195">n</ta>
            <ta e="T197" id="Seg_9257" s="T196">adj</ta>
            <ta e="T198" id="Seg_9258" s="T197">n</ta>
            <ta e="T199" id="Seg_9259" s="T198">n</ta>
            <ta e="T200" id="Seg_9260" s="T199">emphpro</ta>
            <ta e="T201" id="Seg_9261" s="T200">v</ta>
            <ta e="T202" id="Seg_9262" s="T201">n</ta>
            <ta e="T204" id="Seg_9263" s="T203">num</ta>
            <ta e="T205" id="Seg_9264" s="T204">n</ta>
            <ta e="T206" id="Seg_9265" s="T205">v</ta>
            <ta e="T208" id="Seg_9266" s="T207">n</ta>
            <ta e="T209" id="Seg_9267" s="T208">adv</ta>
            <ta e="T210" id="Seg_9268" s="T209">v</ta>
            <ta e="T212" id="Seg_9269" s="T211">n</ta>
            <ta e="T213" id="Seg_9270" s="T212">preverb</ta>
            <ta e="T214" id="Seg_9271" s="T213">v</ta>
            <ta e="T215" id="Seg_9272" s="T214">ptcl</ta>
            <ta e="T216" id="Seg_9273" s="T215">dem</ta>
            <ta e="T217" id="Seg_9274" s="T216">dem</ta>
            <ta e="T218" id="Seg_9275" s="T217">n</ta>
            <ta e="T220" id="Seg_9276" s="T219">v</ta>
            <ta e="T222" id="Seg_9277" s="T221">n</ta>
            <ta e="T223" id="Seg_9278" s="T222">v</ta>
            <ta e="T225" id="Seg_9279" s="T224">n</ta>
            <ta e="T226" id="Seg_9280" s="T225">n</ta>
            <ta e="T227" id="Seg_9281" s="T226">dem</ta>
            <ta e="T228" id="Seg_9282" s="T227">v</ta>
            <ta e="T229" id="Seg_9283" s="T228">adv</ta>
            <ta e="T230" id="Seg_9284" s="T229">interrog</ta>
            <ta e="T231" id="Seg_9285" s="T230">adj</ta>
            <ta e="T232" id="Seg_9286" s="T231">n</ta>
            <ta e="T233" id="Seg_9287" s="T232">n</ta>
            <ta e="T234" id="Seg_9288" s="T233">n</ta>
            <ta e="T235" id="Seg_9289" s="T234">interrog</ta>
            <ta e="T236" id="Seg_9290" s="T235">adj</ta>
            <ta e="T237" id="Seg_9291" s="T236">interrog</ta>
            <ta e="T238" id="Seg_9292" s="T237">dem</ta>
            <ta e="T239" id="Seg_9293" s="T238">ptcp</ta>
            <ta e="T240" id="Seg_9294" s="T239">n</ta>
            <ta e="T242" id="Seg_9295" s="T241">n</ta>
            <ta e="T243" id="Seg_9296" s="T242">adv</ta>
            <ta e="T244" id="Seg_9297" s="T243">v</ta>
            <ta e="T245" id="Seg_9298" s="T244">n</ta>
            <ta e="T246" id="Seg_9299" s="T245">adj</ta>
            <ta e="T247" id="Seg_9300" s="T246">n</ta>
            <ta e="T248" id="Seg_9301" s="T247">n</ta>
            <ta e="T249" id="Seg_9302" s="T248">n</ta>
            <ta e="T250" id="Seg_9303" s="T249">ptcl</ta>
            <ta e="T251" id="Seg_9304" s="T250">v</ta>
            <ta e="T253" id="Seg_9305" s="T252">v</ta>
            <ta e="T255" id="Seg_9306" s="T254">n</ta>
            <ta e="T256" id="Seg_9307" s="T255">adv</ta>
            <ta e="T257" id="Seg_9308" s="T256">v</ta>
            <ta e="T258" id="Seg_9309" s="T257">pers</ta>
            <ta e="T259" id="Seg_9310" s="T258">adv</ta>
            <ta e="T260" id="Seg_9311" s="T259">v</ta>
            <ta e="T262" id="Seg_9312" s="T261">n</ta>
            <ta e="T264" id="Seg_9313" s="T263">adv</ta>
            <ta e="T265" id="Seg_9314" s="T264">v</ta>
            <ta e="T267" id="Seg_9315" s="T266">n</ta>
            <ta e="T268" id="Seg_9316" s="T267">adv</ta>
            <ta e="T269" id="Seg_9317" s="T268">v</ta>
            <ta e="T270" id="Seg_9318" s="T269">n</ta>
            <ta e="T271" id="Seg_9319" s="T270">adv</ta>
            <ta e="T273" id="Seg_9320" s="T272">num</ta>
            <ta e="T274" id="Seg_9321" s="T273">adj</ta>
            <ta e="T275" id="Seg_9322" s="T274">adv</ta>
            <ta e="T276" id="Seg_9323" s="T275">ptcp</ta>
            <ta e="T277" id="Seg_9324" s="T276">adv</ta>
            <ta e="T278" id="Seg_9325" s="T277">n</ta>
            <ta e="T279" id="Seg_9326" s="T278">v</ta>
            <ta e="T281" id="Seg_9327" s="T280">adv</ta>
            <ta e="T282" id="Seg_9328" s="T281">dem</ta>
            <ta e="T283" id="Seg_9329" s="T282">adj</ta>
            <ta e="T284" id="Seg_9330" s="T283">n</ta>
            <ta e="T285" id="Seg_9331" s="T284">adv</ta>
            <ta e="T286" id="Seg_9332" s="T285">n</ta>
            <ta e="T287" id="Seg_9333" s="T286">v</ta>
            <ta e="T288" id="Seg_9334" s="T287">v</ta>
            <ta e="T289" id="Seg_9335" s="T288">n</ta>
            <ta e="T290" id="Seg_9336" s="T289">ptcl</ta>
            <ta e="T291" id="Seg_9337" s="T290">interrog</ta>
            <ta e="T292" id="Seg_9338" s="T291">adv</ta>
            <ta e="T293" id="Seg_9339" s="T292">v</ta>
            <ta e="T294" id="Seg_9340" s="T293">n</ta>
            <ta e="T295" id="Seg_9341" s="T294">n</ta>
            <ta e="T296" id="Seg_9342" s="T295">v</ta>
            <ta e="T298" id="Seg_9343" s="T297">n</ta>
            <ta e="T299" id="Seg_9344" s="T298">v</ta>
            <ta e="T300" id="Seg_9345" s="T299">dem</ta>
            <ta e="T301" id="Seg_9346" s="T300">adj</ta>
            <ta e="T302" id="Seg_9347" s="T301">n</ta>
            <ta e="T303" id="Seg_9348" s="T302">n</ta>
            <ta e="T304" id="Seg_9349" s="T303">adv</ta>
            <ta e="T305" id="Seg_9350" s="T304">ptcp</ta>
            <ta e="T306" id="Seg_9351" s="T305">adv</ta>
            <ta e="T307" id="Seg_9352" s="T306">n</ta>
            <ta e="T308" id="Seg_9353" s="T307">n</ta>
            <ta e="T310" id="Seg_9354" s="T309">pers</ta>
            <ta e="T311" id="Seg_9355" s="T310">v</ta>
            <ta e="T312" id="Seg_9356" s="T311">pers</ta>
            <ta e="T313" id="Seg_9357" s="T312">ptcl</ta>
            <ta e="T314" id="Seg_9358" s="T313">v</ta>
            <ta e="T316" id="Seg_9359" s="T315">n</ta>
            <ta e="T317" id="Seg_9360" s="T316">v</ta>
            <ta e="T319" id="Seg_9361" s="T318">v</ta>
            <ta e="T321" id="Seg_9362" s="T320">n</ta>
            <ta e="T322" id="Seg_9363" s="T321">adv</ta>
            <ta e="T323" id="Seg_9364" s="T322">v</ta>
            <ta e="T324" id="Seg_9365" s="T323">n</ta>
            <ta e="T325" id="Seg_9366" s="T324">v</ta>
            <ta e="T326" id="Seg_9367" s="T325">quant</ta>
            <ta e="T327" id="Seg_9368" s="T326">interrog</ta>
            <ta e="T328" id="Seg_9369" s="T327">adv</ta>
            <ta e="T329" id="Seg_9370" s="T328">v</ta>
            <ta e="T331" id="Seg_9371" s="T330">ptcl</ta>
            <ta e="T332" id="Seg_9372" s="T331">n</ta>
            <ta e="T333" id="Seg_9373" s="T332">n</ta>
            <ta e="T334" id="Seg_9374" s="T333">v</ta>
            <ta e="T336" id="Seg_9375" s="T335">ptcl</ta>
            <ta e="T337" id="Seg_9376" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_9377" s="T337">pers</ta>
            <ta e="T339" id="Seg_9378" s="T338">ptcl</ta>
            <ta e="T340" id="Seg_9379" s="T339">n</ta>
            <ta e="T342" id="Seg_9380" s="T341">pers</ta>
            <ta e="T343" id="Seg_9381" s="T342">ptcl</ta>
            <ta e="T344" id="Seg_9382" s="T343">v</ta>
            <ta e="T345" id="Seg_9383" s="T344">pers</ta>
            <ta e="T346" id="Seg_9384" s="T345">ptcl</ta>
            <ta e="T347" id="Seg_9385" s="T346">v</ta>
            <ta e="T348" id="Seg_9386" s="T347">adj</ta>
            <ta e="T349" id="Seg_9387" s="T348">v</ta>
            <ta e="T350" id="Seg_9388" s="T349">adj</ta>
            <ta e="T351" id="Seg_9389" s="T350">adj</ta>
            <ta e="T352" id="Seg_9390" s="T351">n</ta>
            <ta e="T353" id="Seg_9391" s="T352">n</ta>
            <ta e="T354" id="Seg_9392" s="T353">n</ta>
            <ta e="T355" id="Seg_9393" s="T354">dem</ta>
            <ta e="T357" id="Seg_9394" s="T356">adj</ta>
            <ta e="T358" id="Seg_9395" s="T357">pro</ta>
            <ta e="T359" id="Seg_9396" s="T358">v</ta>
            <ta e="T361" id="Seg_9397" s="T360">n</ta>
            <ta e="T362" id="Seg_9398" s="T361">pp</ta>
            <ta e="T363" id="Seg_9399" s="T362">v</ta>
            <ta e="T364" id="Seg_9400" s="T363">n</ta>
            <ta e="T365" id="Seg_9401" s="T364">pp</ta>
            <ta e="T367" id="Seg_9402" s="T366">num</ta>
            <ta e="T368" id="Seg_9403" s="T367">adj</ta>
            <ta e="T369" id="Seg_9404" s="T368">adv</ta>
            <ta e="T370" id="Seg_9405" s="T369">n</ta>
            <ta e="T371" id="Seg_9406" s="T370">n</ta>
            <ta e="T372" id="Seg_9407" s="T371">dem</ta>
            <ta e="T373" id="Seg_9408" s="T372">adj</ta>
            <ta e="T374" id="Seg_9409" s="T373">n</ta>
            <ta e="T375" id="Seg_9410" s="T374">v</ta>
            <ta e="T377" id="Seg_9411" s="T376">ptcl</ta>
            <ta e="T378" id="Seg_9412" s="T377">v</ta>
            <ta e="T379" id="Seg_9413" s="T378">dem</ta>
            <ta e="T380" id="Seg_9414" s="T379">n</ta>
            <ta e="T381" id="Seg_9415" s="T380">dem</ta>
            <ta e="T382" id="Seg_9416" s="T381">dem</ta>
            <ta e="T383" id="Seg_9417" s="T382">n</ta>
            <ta e="T385" id="Seg_9418" s="T384">n</ta>
            <ta e="T386" id="Seg_9419" s="T385">adv</ta>
            <ta e="T387" id="Seg_9420" s="T386">v</ta>
            <ta e="T388" id="Seg_9421" s="T387">adj</ta>
            <ta e="T389" id="Seg_9422" s="T388">n</ta>
            <ta e="T390" id="Seg_9423" s="T389">ptcl</ta>
            <ta e="T391" id="Seg_9424" s="T390">adj</ta>
            <ta e="T392" id="Seg_9425" s="T391">adj</ta>
            <ta e="T393" id="Seg_9426" s="T392">n</ta>
            <ta e="T394" id="Seg_9427" s="T393">n</ta>
            <ta e="T395" id="Seg_9428" s="T394">adv</ta>
            <ta e="T396" id="Seg_9429" s="T395">v</ta>
            <ta e="T397" id="Seg_9430" s="T396">n</ta>
            <ta e="T399" id="Seg_9431" s="T398">ptcl</ta>
            <ta e="T400" id="Seg_9432" s="T399">dem</ta>
            <ta e="T401" id="Seg_9433" s="T400">v</ta>
            <ta e="T402" id="Seg_9434" s="T401">adv</ta>
            <ta e="T404" id="Seg_9435" s="T403">n</ta>
            <ta e="T405" id="Seg_9436" s="T404">n</ta>
            <ta e="T406" id="Seg_9437" s="T405">pp</ta>
            <ta e="T407" id="Seg_9438" s="T406">n</ta>
            <ta e="T408" id="Seg_9439" s="T407">adv</ta>
            <ta e="T409" id="Seg_9440" s="T408">n</ta>
            <ta e="T410" id="Seg_9441" s="T409">adj</ta>
            <ta e="T411" id="Seg_9442" s="T410">n</ta>
            <ta e="T412" id="Seg_9443" s="T411">v</ta>
            <ta e="T414" id="Seg_9444" s="T413">ptcl</ta>
            <ta e="T415" id="Seg_9445" s="T414">adj</ta>
            <ta e="T416" id="Seg_9446" s="T415">n</ta>
            <ta e="T417" id="Seg_9447" s="T416">v</ta>
            <ta e="T419" id="Seg_9448" s="T418">n</ta>
            <ta e="T420" id="Seg_9449" s="T419">v</ta>
            <ta e="T421" id="Seg_9450" s="T420">v</ta>
            <ta e="T422" id="Seg_9451" s="T421">v</ta>
            <ta e="T423" id="Seg_9452" s="T422">dem</ta>
            <ta e="T424" id="Seg_9453" s="T423">v</ta>
            <ta e="T425" id="Seg_9454" s="T424">adj</ta>
            <ta e="T426" id="Seg_9455" s="T425">n</ta>
            <ta e="T427" id="Seg_9456" s="T426">n</ta>
            <ta e="T428" id="Seg_9457" s="T427">n</ta>
            <ta e="T429" id="Seg_9458" s="T428">adj</ta>
            <ta e="T430" id="Seg_9459" s="T429">v</ta>
            <ta e="T432" id="Seg_9460" s="T431">ptcl</ta>
            <ta e="T433" id="Seg_9461" s="T432">n</ta>
            <ta e="T434" id="Seg_9462" s="T433">dem</ta>
            <ta e="T435" id="Seg_9463" s="T434">dem</ta>
            <ta e="T436" id="Seg_9464" s="T435">n</ta>
            <ta e="T437" id="Seg_9465" s="T436">ptcl</ta>
            <ta e="T438" id="Seg_9466" s="T437">v</ta>
            <ta e="T439" id="Seg_9467" s="T438">ptcl</ta>
            <ta e="T440" id="Seg_9468" s="T439">v</ta>
            <ta e="T441" id="Seg_9469" s="T440">ptcl</ta>
            <ta e="T442" id="Seg_9470" s="T441">n</ta>
            <ta e="T923" id="Seg_9471" s="T442">n</ta>
            <ta e="T444" id="Seg_9472" s="T443">ptcl</ta>
            <ta e="T445" id="Seg_9473" s="T444">dem</ta>
            <ta e="T446" id="Seg_9474" s="T445">v</ta>
            <ta e="T448" id="Seg_9475" s="T447">v</ta>
            <ta e="T450" id="Seg_9476" s="T449">n</ta>
            <ta e="T451" id="Seg_9477" s="T450">ptcl</ta>
            <ta e="T452" id="Seg_9478" s="T451">pers</ta>
            <ta e="T453" id="Seg_9479" s="T452">ptcl</ta>
            <ta e="T454" id="Seg_9480" s="T453">v</ta>
            <ta e="T455" id="Seg_9481" s="T454">dem</ta>
            <ta e="T456" id="Seg_9482" s="T455">dem</ta>
            <ta e="T457" id="Seg_9483" s="T456">n</ta>
            <ta e="T458" id="Seg_9484" s="T457">adj</ta>
            <ta e="T459" id="Seg_9485" s="T458">n</ta>
            <ta e="T460" id="Seg_9486" s="T459">adv</ta>
            <ta e="T461" id="Seg_9487" s="T460">adj</ta>
            <ta e="T462" id="Seg_9488" s="T461">n</ta>
            <ta e="T463" id="Seg_9489" s="T462">adj</ta>
            <ta e="T464" id="Seg_9490" s="T463">n</ta>
            <ta e="T466" id="Seg_9491" s="T465">dem</ta>
            <ta e="T467" id="Seg_9492" s="T466">n</ta>
            <ta e="T468" id="Seg_9493" s="T467">ptcl</ta>
            <ta e="T469" id="Seg_9494" s="T468">v</ta>
            <ta e="T470" id="Seg_9495" s="T469">dem</ta>
            <ta e="T471" id="Seg_9496" s="T470">adj</ta>
            <ta e="T472" id="Seg_9497" s="T471">adj</ta>
            <ta e="T473" id="Seg_9498" s="T472">n</ta>
            <ta e="T474" id="Seg_9499" s="T473">quant</ta>
            <ta e="T475" id="Seg_9500" s="T474">n</ta>
            <ta e="T476" id="Seg_9501" s="T475">v</ta>
            <ta e="T478" id="Seg_9502" s="T477">v</ta>
            <ta e="T479" id="Seg_9503" s="T478">v</ta>
            <ta e="T480" id="Seg_9504" s="T479">adv</ta>
            <ta e="T482" id="Seg_9505" s="T481">adv</ta>
            <ta e="T483" id="Seg_9506" s="T482">adv</ta>
            <ta e="T484" id="Seg_9507" s="T483">n</ta>
            <ta e="T485" id="Seg_9508" s="T484">adv</ta>
            <ta e="T486" id="Seg_9509" s="T485">v</ta>
            <ta e="T487" id="Seg_9510" s="T486">n</ta>
            <ta e="T488" id="Seg_9511" s="T487">adv</ta>
            <ta e="T489" id="Seg_9512" s="T488">adj</ta>
            <ta e="T490" id="Seg_9513" s="T489">v</ta>
            <ta e="T491" id="Seg_9514" s="T490">v</ta>
            <ta e="T493" id="Seg_9515" s="T492">adj</ta>
            <ta e="T494" id="Seg_9516" s="T493">conj</ta>
            <ta e="T495" id="Seg_9517" s="T494">adv</ta>
            <ta e="T497" id="Seg_9518" s="T496">ptcl</ta>
            <ta e="T498" id="Seg_9519" s="T497">adv</ta>
            <ta e="T499" id="Seg_9520" s="T498">v</ta>
            <ta e="T501" id="Seg_9521" s="T500">n</ta>
            <ta e="T502" id="Seg_9522" s="T501">dem</ta>
            <ta e="T503" id="Seg_9523" s="T502">n</ta>
            <ta e="T504" id="Seg_9524" s="T503">ptcl</ta>
            <ta e="T505" id="Seg_9525" s="T504">v</ta>
            <ta e="T506" id="Seg_9526" s="T505">n</ta>
            <ta e="T507" id="Seg_9527" s="T506">n</ta>
            <ta e="T508" id="Seg_9528" s="T507">pp</ta>
            <ta e="T509" id="Seg_9529" s="T508">v</ta>
            <ta e="T510" id="Seg_9530" s="T509">adv</ta>
            <ta e="T511" id="Seg_9531" s="T510">ptcl</ta>
            <ta e="T512" id="Seg_9532" s="T511">num</ta>
            <ta e="T513" id="Seg_9533" s="T512">n</ta>
            <ta e="T514" id="Seg_9534" s="T513">n</ta>
            <ta e="T515" id="Seg_9535" s="T514">pp</ta>
            <ta e="T516" id="Seg_9536" s="T515">v</ta>
            <ta e="T518" id="Seg_9537" s="T517">quant</ta>
            <ta e="T519" id="Seg_9538" s="T518">n</ta>
            <ta e="T520" id="Seg_9539" s="T519">v</ta>
            <ta e="T521" id="Seg_9540" s="T520">dem</ta>
            <ta e="T522" id="Seg_9541" s="T521">adj</ta>
            <ta e="T523" id="Seg_9542" s="T522">n</ta>
            <ta e="T525" id="Seg_9543" s="T524">dem</ta>
            <ta e="T526" id="Seg_9544" s="T525">n</ta>
            <ta e="T527" id="Seg_9545" s="T526">ptcl</ta>
            <ta e="T528" id="Seg_9546" s="T527">num</ta>
            <ta e="T529" id="Seg_9547" s="T528">v</ta>
            <ta e="T533" id="Seg_9548" s="T532">ptcl</ta>
            <ta e="T534" id="Seg_9549" s="T533">v</ta>
            <ta e="T536" id="Seg_9550" s="T535">adj</ta>
            <ta e="T537" id="Seg_9551" s="T536">n</ta>
            <ta e="T538" id="Seg_9552" s="T537">interrog</ta>
            <ta e="T539" id="Seg_9553" s="T538">qv</ta>
            <ta e="T541" id="Seg_9554" s="T540">n</ta>
            <ta e="T542" id="Seg_9555" s="T541">adv</ta>
            <ta e="T543" id="Seg_9556" s="T542">adv</ta>
            <ta e="T544" id="Seg_9557" s="T543">v</ta>
            <ta e="T545" id="Seg_9558" s="T544">adj</ta>
            <ta e="T546" id="Seg_9559" s="T545">n</ta>
            <ta e="T547" id="Seg_9560" s="T546">n</ta>
            <ta e="T548" id="Seg_9561" s="T547">adv</ta>
            <ta e="T549" id="Seg_9562" s="T548">n</ta>
            <ta e="T550" id="Seg_9563" s="T549">n</ta>
            <ta e="T551" id="Seg_9564" s="T550">v</ta>
            <ta e="T553" id="Seg_9565" s="T552">ptcl</ta>
            <ta e="T554" id="Seg_9566" s="T553">n</ta>
            <ta e="T556" id="Seg_9567" s="T555">adv</ta>
            <ta e="T558" id="Seg_9568" s="T557">adj</ta>
            <ta e="T559" id="Seg_9569" s="T558">n</ta>
            <ta e="T927" id="Seg_9570" s="T559">v</ta>
            <ta e="T561" id="Seg_9571" s="T560">adv</ta>
            <ta e="T562" id="Seg_9572" s="T561">v</ta>
            <ta e="T564" id="Seg_9573" s="T563">dem</ta>
            <ta e="T565" id="Seg_9574" s="T564">ptcp</ta>
            <ta e="T566" id="Seg_9575" s="T565">n</ta>
            <ta e="T567" id="Seg_9576" s="T566">v</ta>
            <ta e="T569" id="Seg_9577" s="T568">adv</ta>
            <ta e="T570" id="Seg_9578" s="T569">v</ta>
            <ta e="T571" id="Seg_9579" s="T570">interj</ta>
            <ta e="T572" id="Seg_9580" s="T571">adv</ta>
            <ta e="T573" id="Seg_9581" s="T572">adv</ta>
            <ta e="T574" id="Seg_9582" s="T573">ptcp</ta>
            <ta e="T575" id="Seg_9583" s="T574">n</ta>
            <ta e="T576" id="Seg_9584" s="T575">n</ta>
            <ta e="T577" id="Seg_9585" s="T576">v</ta>
            <ta e="T581" id="Seg_9586" s="T580">ptcl</ta>
            <ta e="T582" id="Seg_9587" s="T581">ptcl</ta>
            <ta e="T583" id="Seg_9588" s="T582">v</ta>
            <ta e="T585" id="Seg_9589" s="T584">ptcl</ta>
            <ta e="T586" id="Seg_9590" s="T585">adj</ta>
            <ta e="T587" id="Seg_9591" s="T586">n</ta>
            <ta e="T588" id="Seg_9592" s="T587">n</ta>
            <ta e="T591" id="Seg_9593" s="T590">adj</ta>
            <ta e="T592" id="Seg_9594" s="T591">n</ta>
            <ta e="T593" id="Seg_9595" s="T592">n</ta>
            <ta e="T594" id="Seg_9596" s="T593">adv</ta>
            <ta e="T595" id="Seg_9597" s="T594">v</ta>
            <ta e="T596" id="Seg_9598" s="T595">adj</ta>
            <ta e="T597" id="Seg_9599" s="T596">n</ta>
            <ta e="T598" id="Seg_9600" s="T597">adv</ta>
            <ta e="T599" id="Seg_9601" s="T598">v</ta>
            <ta e="T601" id="Seg_9602" s="T600">adv</ta>
            <ta e="T602" id="Seg_9603" s="T601">adv</ta>
            <ta e="T603" id="Seg_9604" s="T602">v</ta>
            <ta e="T605" id="Seg_9605" s="T604">adv</ta>
            <ta e="T606" id="Seg_9606" s="T605">v</ta>
            <ta e="T607" id="Seg_9607" s="T606">adv</ta>
            <ta e="T608" id="Seg_9608" s="T607">num</ta>
            <ta e="T609" id="Seg_9609" s="T608">n</ta>
            <ta e="T610" id="Seg_9610" s="T609">adv</ta>
            <ta e="T611" id="Seg_9611" s="T610">v</ta>
            <ta e="T613" id="Seg_9612" s="T612">adv</ta>
            <ta e="T614" id="Seg_9613" s="T613">n</ta>
            <ta e="T615" id="Seg_9614" s="T614">n</ta>
            <ta e="T616" id="Seg_9615" s="T615">adv</ta>
            <ta e="T617" id="Seg_9616" s="T616">v</ta>
            <ta e="T618" id="Seg_9617" s="T617">adv</ta>
            <ta e="T620" id="Seg_9618" s="T619">adv</ta>
            <ta e="T621" id="Seg_9619" s="T620">v</ta>
            <ta e="T622" id="Seg_9620" s="T621">adv</ta>
            <ta e="T623" id="Seg_9621" s="T622">adv</ta>
            <ta e="T624" id="Seg_9622" s="T623">v</ta>
            <ta e="T625" id="Seg_9623" s="T624">n</ta>
            <ta e="T626" id="Seg_9624" s="T625">n</ta>
            <ta e="T627" id="Seg_9625" s="T626">dem</ta>
            <ta e="T628" id="Seg_9626" s="T627">n</ta>
            <ta e="T629" id="Seg_9627" s="T628">n</ta>
            <ta e="T630" id="Seg_9628" s="T629">dem</ta>
            <ta e="T631" id="Seg_9629" s="T630">num</ta>
            <ta e="T632" id="Seg_9630" s="T631">n</ta>
            <ta e="T634" id="Seg_9631" s="T633">adv</ta>
            <ta e="T635" id="Seg_9632" s="T634">adv</ta>
            <ta e="T636" id="Seg_9633" s="T635">v</ta>
            <ta e="T638" id="Seg_9634" s="T637">dem</ta>
            <ta e="T639" id="Seg_9635" s="T638">adv</ta>
            <ta e="T640" id="Seg_9636" s="T639">dem</ta>
            <ta e="T641" id="Seg_9637" s="T640">adj</ta>
            <ta e="T643" id="Seg_9638" s="T642">n</ta>
            <ta e="T644" id="Seg_9639" s="T643">ptcl</ta>
            <ta e="T645" id="Seg_9640" s="T644">v</ta>
            <ta e="T647" id="Seg_9641" s="T646">dem</ta>
            <ta e="T648" id="Seg_9642" s="T647">adj</ta>
            <ta e="T649" id="Seg_9643" s="T648">n</ta>
            <ta e="T650" id="Seg_9644" s="T649">adv</ta>
            <ta e="T651" id="Seg_9645" s="T650">adv</ta>
            <ta e="T652" id="Seg_9646" s="T651">v</ta>
            <ta e="T653" id="Seg_9647" s="T652">adv</ta>
            <ta e="T654" id="Seg_9648" s="T653">dem</ta>
            <ta e="T655" id="Seg_9649" s="T654">n</ta>
            <ta e="T656" id="Seg_9650" s="T655">v</ta>
            <ta e="T658" id="Seg_9651" s="T657">adv</ta>
            <ta e="T659" id="Seg_9652" s="T658">dem</ta>
            <ta e="T660" id="Seg_9653" s="T659">n</ta>
            <ta e="T661" id="Seg_9654" s="T660">num</ta>
            <ta e="T662" id="Seg_9655" s="T661">n</ta>
            <ta e="T663" id="Seg_9656" s="T662">adv</ta>
            <ta e="T664" id="Seg_9657" s="T663">v</ta>
            <ta e="T666" id="Seg_9658" s="T665">n</ta>
            <ta e="T667" id="Seg_9659" s="T666">v</ta>
            <ta e="T669" id="Seg_9660" s="T668">interrog</ta>
            <ta e="T670" id="Seg_9661" s="T669">v</ta>
            <ta e="T671" id="Seg_9662" s="T670">adj</ta>
            <ta e="T672" id="Seg_9663" s="T671">n</ta>
            <ta e="T673" id="Seg_9664" s="T672">n</ta>
            <ta e="T674" id="Seg_9665" s="T673">n</ta>
            <ta e="T675" id="Seg_9666" s="T674">n</ta>
            <ta e="T677" id="Seg_9667" s="T676">adv</ta>
            <ta e="T678" id="Seg_9668" s="T677">v</ta>
            <ta e="T679" id="Seg_9669" s="T678">dem</ta>
            <ta e="T680" id="Seg_9670" s="T679">dem</ta>
            <ta e="T681" id="Seg_9671" s="T680">adj</ta>
            <ta e="T682" id="Seg_9672" s="T681">n</ta>
            <ta e="T683" id="Seg_9673" s="T682">ptcl</ta>
            <ta e="T684" id="Seg_9674" s="T683">pers</ta>
            <ta e="T685" id="Seg_9675" s="T684">v</ta>
            <ta e="T687" id="Seg_9676" s="T686">adv</ta>
            <ta e="T688" id="Seg_9677" s="T687">n</ta>
            <ta e="T689" id="Seg_9678" s="T688">pers</ta>
            <ta e="T690" id="Seg_9679" s="T689">n</ta>
            <ta e="T691" id="Seg_9680" s="T690">adj</ta>
            <ta e="T692" id="Seg_9681" s="T691">dem</ta>
            <ta e="T693" id="Seg_9682" s="T692">n</ta>
            <ta e="T695" id="Seg_9683" s="T694">adj</ta>
            <ta e="T696" id="Seg_9684" s="T695">n</ta>
            <ta e="T697" id="Seg_9685" s="T696">n</ta>
            <ta e="T698" id="Seg_9686" s="T697">v</ta>
            <ta e="T699" id="Seg_9687" s="T698">n</ta>
            <ta e="T700" id="Seg_9688" s="T699">v</ta>
            <ta e="T702" id="Seg_9689" s="T701">dem</ta>
            <ta e="T703" id="Seg_9690" s="T702">ptcp</ta>
            <ta e="T704" id="Seg_9691" s="T703">n</ta>
            <ta e="T705" id="Seg_9692" s="T704">dem</ta>
            <ta e="T706" id="Seg_9693" s="T705">n</ta>
            <ta e="T708" id="Seg_9694" s="T707">pro</ta>
            <ta e="T709" id="Seg_9695" s="T708">n</ta>
            <ta e="T710" id="Seg_9696" s="T709">num</ta>
            <ta e="T712" id="Seg_9697" s="T711">dem</ta>
            <ta e="T713" id="Seg_9698" s="T712">num</ta>
            <ta e="T714" id="Seg_9699" s="T713">n</ta>
            <ta e="T716" id="Seg_9700" s="T715">n</ta>
            <ta e="T717" id="Seg_9701" s="T716">adv</ta>
            <ta e="T718" id="Seg_9702" s="T717">adv</ta>
            <ta e="T719" id="Seg_9703" s="T718">v</ta>
            <ta e="T720" id="Seg_9704" s="T719">dem</ta>
            <ta e="T721" id="Seg_9705" s="T720">dem</ta>
            <ta e="T722" id="Seg_9706" s="T721">n</ta>
            <ta e="T724" id="Seg_9707" s="T723">dem</ta>
            <ta e="T725" id="Seg_9708" s="T724">v</ta>
            <ta e="T727" id="Seg_9709" s="T726">ptcl</ta>
            <ta e="T728" id="Seg_9710" s="T727">dem</ta>
            <ta e="T729" id="Seg_9711" s="T728">dem</ta>
            <ta e="T730" id="Seg_9712" s="T729">adv</ta>
            <ta e="T733" id="Seg_9713" s="T732">n</ta>
            <ta e="T734" id="Seg_9714" s="T733">dem</ta>
            <ta e="T735" id="Seg_9715" s="T734">dem</ta>
            <ta e="T736" id="Seg_9716" s="T735">v</ta>
            <ta e="T737" id="Seg_9717" s="T736">n</ta>
            <ta e="T738" id="Seg_9718" s="T737">adv</ta>
            <ta e="T739" id="Seg_9719" s="T738">adv</ta>
            <ta e="T743" id="Seg_9720" s="T742">adv</ta>
            <ta e="T744" id="Seg_9721" s="T743">v</ta>
            <ta e="T745" id="Seg_9722" s="T744">v</ta>
            <ta e="T746" id="Seg_9723" s="T745">num</ta>
            <ta e="T747" id="Seg_9724" s="T746">adv</ta>
            <ta e="T748" id="Seg_9725" s="T747">v</ta>
            <ta e="T750" id="Seg_9726" s="T749">v</ta>
            <ta e="T752" id="Seg_9727" s="T751">n</ta>
            <ta e="T753" id="Seg_9728" s="T752">n</ta>
            <ta e="T754" id="Seg_9729" s="T753">preverb</ta>
            <ta e="T755" id="Seg_9730" s="T754">v</ta>
            <ta e="T756" id="Seg_9731" s="T755">n</ta>
            <ta e="T757" id="Seg_9732" s="T756">adv</ta>
            <ta e="T759" id="Seg_9733" s="T758">adv</ta>
            <ta e="T760" id="Seg_9734" s="T759">v</ta>
            <ta e="T762" id="Seg_9735" s="T761">interrog</ta>
            <ta e="T763" id="Seg_9736" s="T762">v</ta>
            <ta e="T765" id="Seg_9737" s="T764">n</ta>
            <ta e="T766" id="Seg_9738" s="T765">n</ta>
            <ta e="T767" id="Seg_9739" s="T766">n</ta>
            <ta e="T768" id="Seg_9740" s="T767">dem</ta>
            <ta e="T769" id="Seg_9741" s="T768">n</ta>
            <ta e="T772" id="Seg_9742" s="T771">adv</ta>
            <ta e="T773" id="Seg_9743" s="T772">adv</ta>
            <ta e="T774" id="Seg_9744" s="T773">v</ta>
            <ta e="T775" id="Seg_9745" s="T774">adj</ta>
            <ta e="T776" id="Seg_9746" s="T775">n</ta>
            <ta e="T777" id="Seg_9747" s="T776">v</ta>
            <ta e="T778" id="Seg_9748" s="T777">n</ta>
            <ta e="T779" id="Seg_9749" s="T778">n</ta>
            <ta e="T780" id="Seg_9750" s="T779">n</ta>
            <ta e="T781" id="Seg_9751" s="T780">v</ta>
            <ta e="T782" id="Seg_9752" s="T781">pers</ta>
            <ta e="T783" id="Seg_9753" s="T782">adv</ta>
            <ta e="T784" id="Seg_9754" s="T783">v</ta>
            <ta e="T786" id="Seg_9755" s="T785">dem</ta>
            <ta e="T787" id="Seg_9756" s="T786">num</ta>
            <ta e="T788" id="Seg_9757" s="T787">n</ta>
            <ta e="T790" id="Seg_9758" s="T789">v</ta>
            <ta e="T792" id="Seg_9759" s="T791">adv</ta>
            <ta e="T793" id="Seg_9760" s="T792">adv</ta>
            <ta e="T795" id="Seg_9761" s="T794">adv</ta>
            <ta e="T796" id="Seg_9762" s="T795">v</ta>
            <ta e="T798" id="Seg_9763" s="T797">n</ta>
            <ta e="T799" id="Seg_9764" s="T798">n</ta>
            <ta e="T800" id="Seg_9765" s="T799">dem</ta>
            <ta e="T801" id="Seg_9766" s="T800">adj</ta>
            <ta e="T802" id="Seg_9767" s="T801">n</ta>
            <ta e="T803" id="Seg_9768" s="T802">n</ta>
            <ta e="T805" id="Seg_9769" s="T804">n</ta>
            <ta e="T806" id="Seg_9770" s="T805">adv</ta>
            <ta e="T807" id="Seg_9771" s="T806">v</ta>
            <ta e="T809" id="Seg_9772" s="T808">n</ta>
            <ta e="T810" id="Seg_9773" s="T809">interrog</ta>
            <ta e="T811" id="Seg_9774" s="T810">adv</ta>
            <ta e="T812" id="Seg_9775" s="T811">dem</ta>
            <ta e="T813" id="Seg_9776" s="T812">n</ta>
            <ta e="T814" id="Seg_9777" s="T813">n</ta>
            <ta e="T815" id="Seg_9778" s="T814">v</ta>
            <ta e="T817" id="Seg_9779" s="T816">n</ta>
            <ta e="T818" id="Seg_9780" s="T817">n</ta>
            <ta e="T819" id="Seg_9781" s="T818">v</ta>
            <ta e="T820" id="Seg_9782" s="T819">n</ta>
            <ta e="T821" id="Seg_9783" s="T820">v</ta>
            <ta e="T823" id="Seg_9784" s="T822">n</ta>
            <ta e="T824" id="Seg_9785" s="T823">adj</ta>
            <ta e="T825" id="Seg_9786" s="T824">v</ta>
            <ta e="T826" id="Seg_9787" s="T825">adj</ta>
            <ta e="T827" id="Seg_9788" s="T826">n</ta>
            <ta e="T828" id="Seg_9789" s="T827">n</ta>
            <ta e="T829" id="Seg_9790" s="T828">ptcl</ta>
            <ta e="T831" id="Seg_9791" s="T830">n</ta>
            <ta e="T832" id="Seg_9792" s="T831">interrog</ta>
            <ta e="T833" id="Seg_9793" s="T832">v</ta>
            <ta e="T834" id="Seg_9794" s="T833">ptcl</ta>
            <ta e="T835" id="Seg_9795" s="T834">n</ta>
            <ta e="T836" id="Seg_9796" s="T835">v</ta>
            <ta e="T839" id="Seg_9797" s="T838">adj</ta>
            <ta e="T840" id="Seg_9798" s="T839">n</ta>
            <ta e="T841" id="Seg_9799" s="T840">n</ta>
            <ta e="T842" id="Seg_9800" s="T841">adv</ta>
            <ta e="T843" id="Seg_9801" s="T842">v</ta>
            <ta e="T845" id="Seg_9802" s="T844">dem</ta>
            <ta e="T846" id="Seg_9803" s="T845">adj</ta>
            <ta e="T847" id="Seg_9804" s="T846">n</ta>
            <ta e="T848" id="Seg_9805" s="T847">n</ta>
            <ta e="T849" id="Seg_9806" s="T848">adj</ta>
            <ta e="T850" id="Seg_9807" s="T849">n</ta>
            <ta e="T852" id="Seg_9808" s="T851">adv</ta>
            <ta e="T853" id="Seg_9809" s="T852">adv</ta>
            <ta e="T854" id="Seg_9810" s="T853">v</ta>
            <ta e="T855" id="Seg_9811" s="T854">interrog</ta>
            <ta e="T856" id="Seg_9812" s="T855">v</ta>
            <ta e="T857" id="Seg_9813" s="T856">n</ta>
            <ta e="T859" id="Seg_9814" s="T858">dem</ta>
            <ta e="T860" id="Seg_9815" s="T859">interrog</ta>
            <ta e="T861" id="Seg_9816" s="T860">n</ta>
            <ta e="T862" id="Seg_9817" s="T861">v</ta>
            <ta e="T864" id="Seg_9818" s="T863">n</ta>
            <ta e="T865" id="Seg_9819" s="T864">n</ta>
            <ta e="T866" id="Seg_9820" s="T865">dem</ta>
            <ta e="T867" id="Seg_9821" s="T866">v</ta>
            <ta e="T868" id="Seg_9822" s="T867">dem</ta>
            <ta e="T869" id="Seg_9823" s="T868">n</ta>
            <ta e="T871" id="Seg_9824" s="T870">ptcl</ta>
            <ta e="T872" id="Seg_9825" s="T871">pers</ta>
            <ta e="T873" id="Seg_9826" s="T872">ptcl</ta>
            <ta e="T874" id="Seg_9827" s="T873">n</ta>
            <ta e="T875" id="Seg_9828" s="T874">ptcl</ta>
            <ta e="T876" id="Seg_9829" s="T875">pers</ta>
            <ta e="T877" id="Seg_9830" s="T876">n</ta>
            <ta e="T878" id="Seg_9831" s="T877">v</ta>
            <ta e="T879" id="Seg_9832" s="T878">ptcl</ta>
            <ta e="T881" id="Seg_9833" s="T880">n</ta>
            <ta e="T882" id="Seg_9834" s="T881">ptcl</ta>
            <ta e="T883" id="Seg_9835" s="T882">adj</ta>
            <ta e="T884" id="Seg_9836" s="T883">pers</ta>
            <ta e="T894" id="Seg_9837" s="T893">ptcl</ta>
            <ta e="T895" id="Seg_9838" s="T894">adv</ta>
            <ta e="T896" id="Seg_9839" s="T895">v</ta>
            <ta e="T898" id="Seg_9840" s="T897">adv</ta>
            <ta e="T899" id="Seg_9841" s="T898">adv</ta>
            <ta e="T900" id="Seg_9842" s="T899">ptcl</ta>
            <ta e="T901" id="Seg_9843" s="T900">v</ta>
            <ta e="T902" id="Seg_9844" s="T901">adv</ta>
            <ta e="T903" id="Seg_9845" s="T902">ptcl</ta>
            <ta e="T904" id="Seg_9846" s="T903">ptcl</ta>
            <ta e="T905" id="Seg_9847" s="T904">adv</ta>
            <ta e="T906" id="Seg_9848" s="T905">adv</ta>
            <ta e="T907" id="Seg_9849" s="T906">v</ta>
            <ta e="T908" id="Seg_9850" s="T907">dem</ta>
            <ta e="T909" id="Seg_9851" s="T908">n</ta>
            <ta e="T910" id="Seg_9852" s="T909">dem</ta>
            <ta e="T912" id="Seg_9853" s="T911">ptcl</ta>
            <ta e="T913" id="Seg_9854" s="T912">adj</ta>
            <ta e="T914" id="Seg_9855" s="T913">n</ta>
            <ta e="T915" id="Seg_9856" s="T914">n</ta>
            <ta e="T916" id="Seg_9857" s="T915">v</ta>
            <ta e="T918" id="Seg_9858" s="T917">conj</ta>
            <ta e="T919" id="Seg_9859" s="T918">dem</ta>
            <ta e="T920" id="Seg_9860" s="T919">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T12" id="Seg_9861" s="T11">0.2.h:A</ta>
            <ta e="T13" id="Seg_9862" s="T12">adv:Time</ta>
            <ta e="T15" id="Seg_9863" s="T14">pro:Th</ta>
            <ta e="T23" id="Seg_9864" s="T22">0.1.h:A</ta>
            <ta e="T29" id="Seg_9865" s="T28">adv:Time</ta>
            <ta e="T31" id="Seg_9866" s="T30">np.h:Th 0.3.h:Poss</ta>
            <ta e="T36" id="Seg_9867" s="T35">0.3.h:Poss</ta>
            <ta e="T38" id="Seg_9868" s="T37">adv:Time</ta>
            <ta e="T39" id="Seg_9869" s="T38">0.3.h:A</ta>
            <ta e="T41" id="Seg_9870" s="T40">np.h:A</ta>
            <ta e="T42" id="Seg_9871" s="T41">adv:So</ta>
            <ta e="T43" id="Seg_9872" s="T42">adv:G</ta>
            <ta e="T45" id="Seg_9873" s="T44">pro.h:P</ta>
            <ta e="T46" id="Seg_9874" s="T45">0.3.h:A</ta>
            <ta e="T48" id="Seg_9875" s="T47">np.h:A 0.3.h:Poss</ta>
            <ta e="T49" id="Seg_9876" s="T48">np.h:A 0.3.h:Poss</ta>
            <ta e="T50" id="Seg_9877" s="T49">adv:G</ta>
            <ta e="T52" id="Seg_9878" s="T51">0.3.h:A</ta>
            <ta e="T55" id="Seg_9879" s="T54">adv:G</ta>
            <ta e="T56" id="Seg_9880" s="T55">0.3.h:A</ta>
            <ta e="T57" id="Seg_9881" s="T56">adv:Time</ta>
            <ta e="T60" id="Seg_9882" s="T59">adv:Time</ta>
            <ta e="T64" id="Seg_9883" s="T63">adv:Time</ta>
            <ta e="T69" id="Seg_9884" s="T68">np.h:Th</ta>
            <ta e="T72" id="Seg_9885" s="T71">np.h:A</ta>
            <ta e="T81" id="Seg_9886" s="T80">adv:Time</ta>
            <ta e="T83" id="Seg_9887" s="T82">pro.h:A</ta>
            <ta e="T85" id="Seg_9888" s="T84">pro.h:Poss</ta>
            <ta e="T86" id="Seg_9889" s="T85">np:G</ta>
            <ta e="T87" id="Seg_9890" s="T86">pro.h:A</ta>
            <ta e="T90" id="Seg_9891" s="T89">adv:G</ta>
            <ta e="T92" id="Seg_9892" s="T91">np.h:A</ta>
            <ta e="T93" id="Seg_9893" s="T92">adv:G</ta>
            <ta e="T94" id="Seg_9894" s="T93">0.3.h:A</ta>
            <ta e="T106" id="Seg_9895" s="T105">0.2.h:A</ta>
            <ta e="T107" id="Seg_9896" s="T106">0.2.h:A</ta>
            <ta e="T110" id="Seg_9897" s="T109">np:P</ta>
            <ta e="T112" id="Seg_9898" s="T111">0.3.h:A</ta>
            <ta e="T114" id="Seg_9899" s="T113">0.2.h:A</ta>
            <ta e="T116" id="Seg_9900" s="T115">adv:Time</ta>
            <ta e="T118" id="Seg_9901" s="T117">np.h:A</ta>
            <ta e="T119" id="Seg_9902" s="T118">pro:G</ta>
            <ta e="T122" id="Seg_9903" s="T121">np.h:A</ta>
            <ta e="T128" id="Seg_9904" s="T127">np.h:Th</ta>
            <ta e="T138" id="Seg_9905" s="T137">0.3.h:A</ta>
            <ta e="T139" id="Seg_9906" s="T138">0.3.h:Poss</ta>
            <ta e="T140" id="Seg_9907" s="T139">np.h:R</ta>
            <ta e="T141" id="Seg_9908" s="T140">np:Th</ta>
            <ta e="T142" id="Seg_9909" s="T141">0.2.h:A</ta>
            <ta e="T143" id="Seg_9910" s="T142">adv:G</ta>
            <ta e="T145" id="Seg_9911" s="T144">np:Th</ta>
            <ta e="T146" id="Seg_9912" s="T145">np:Th</ta>
            <ta e="T147" id="Seg_9913" s="T146">0.2.h:A</ta>
            <ta e="T150" id="Seg_9914" s="T149">np:Th</ta>
            <ta e="T153" id="Seg_9915" s="T152">pro:Th</ta>
            <ta e="T154" id="Seg_9916" s="T153">0.2.h:A</ta>
            <ta e="T156" id="Seg_9917" s="T155">np.h:A</ta>
            <ta e="T157" id="Seg_9918" s="T156">adv:Time</ta>
            <ta e="T159" id="Seg_9919" s="T158">0.3.h:A</ta>
            <ta e="T161" id="Seg_9920" s="T160">np.h:A</ta>
            <ta e="T165" id="Seg_9921" s="T164">np:Th</ta>
            <ta e="T166" id="Seg_9922" s="T165">0.3.h:A</ta>
            <ta e="T173" id="Seg_9923" s="T172">np:Th</ta>
            <ta e="T175" id="Seg_9924" s="T174">np:Ins</ta>
            <ta e="T176" id="Seg_9925" s="T175">0.3.h:A</ta>
            <ta e="T179" id="Seg_9926" s="T178">np.h:A</ta>
            <ta e="T180" id="Seg_9927" s="T179">np.h:B</ta>
            <ta e="T185" id="Seg_9928" s="T184">np:Time</ta>
            <ta e="T193" id="Seg_9929" s="T192">adv:Time</ta>
            <ta e="T196" id="Seg_9930" s="T195">np:L</ta>
            <ta e="T199" id="Seg_9931" s="T198">np.h:Th</ta>
            <ta e="T202" id="Seg_9932" s="T201">np:Com 0.3.h:Poss</ta>
            <ta e="T205" id="Seg_9933" s="T204">np:Ins</ta>
            <ta e="T206" id="Seg_9934" s="T205">0.3.h:A 0.3:Th</ta>
            <ta e="T208" id="Seg_9935" s="T207">np.h:Th</ta>
            <ta e="T212" id="Seg_9936" s="T211">np:L</ta>
            <ta e="T214" id="Seg_9937" s="T213">0.3.h:A</ta>
            <ta e="T220" id="Seg_9938" s="T219">0.3.h:A</ta>
            <ta e="T222" id="Seg_9939" s="T221">np:Th</ta>
            <ta e="T223" id="Seg_9940" s="T222">0.3.h:B</ta>
            <ta e="T225" id="Seg_9941" s="T224">np.h:A 0.3.h:Poss</ta>
            <ta e="T226" id="Seg_9942" s="T225">np:P</ta>
            <ta e="T229" id="Seg_9943" s="T228">adv:L</ta>
            <ta e="T232" id="Seg_9944" s="T231">np:Th</ta>
            <ta e="T233" id="Seg_9945" s="T232">np:Th</ta>
            <ta e="T240" id="Seg_9946" s="T239">np:Th</ta>
            <ta e="T242" id="Seg_9947" s="T241">np.h:A</ta>
            <ta e="T245" id="Seg_9948" s="T244">np.h:R</ta>
            <ta e="T248" id="Seg_9949" s="T247">np.h:Poss</ta>
            <ta e="T249" id="Seg_9950" s="T248">np.h:Th</ta>
            <ta e="T253" id="Seg_9951" s="T252">0.2.h:A</ta>
            <ta e="T255" id="Seg_9952" s="T254">np.h:Th 0.3.h:Poss</ta>
            <ta e="T257" id="Seg_9953" s="T256">0.3.h:A</ta>
            <ta e="T258" id="Seg_9954" s="T257">pro.h:A</ta>
            <ta e="T259" id="Seg_9955" s="T258">adv:G</ta>
            <ta e="T262" id="Seg_9956" s="T261">np.h:Th</ta>
            <ta e="T264" id="Seg_9957" s="T263">adv:G</ta>
            <ta e="T265" id="Seg_9958" s="T264">0.2.h:A</ta>
            <ta e="T267" id="Seg_9959" s="T266">np.h:Th</ta>
            <ta e="T268" id="Seg_9960" s="T267">adv:Time</ta>
            <ta e="T270" id="Seg_9961" s="T269">np.h:B</ta>
            <ta e="T271" id="Seg_9962" s="T270">0.3.h:A</ta>
            <ta e="T275" id="Seg_9963" s="T274">adv:Time</ta>
            <ta e="T278" id="Seg_9964" s="T277">np.h:A</ta>
            <ta e="T284" id="Seg_9965" s="T283">np:G</ta>
            <ta e="T286" id="Seg_9966" s="T285">np:Ins</ta>
            <ta e="T288" id="Seg_9967" s="T287">0.3.h:A</ta>
            <ta e="T289" id="Seg_9968" s="T288">np.h:Th 0.3.h:Poss</ta>
            <ta e="T292" id="Seg_9969" s="T291">adv:G</ta>
            <ta e="T293" id="Seg_9970" s="T292">0.3.h:A</ta>
            <ta e="T294" id="Seg_9971" s="T293">np:L</ta>
            <ta e="T295" id="Seg_9972" s="T294">np:P</ta>
            <ta e="T296" id="Seg_9973" s="T295">0.3.h:A</ta>
            <ta e="T298" id="Seg_9974" s="T297">np:P</ta>
            <ta e="T299" id="Seg_9975" s="T298">0.3.h:A</ta>
            <ta e="T312" id="Seg_9976" s="T311">pro.h:A</ta>
            <ta e="T316" id="Seg_9977" s="T315">np.h:A</ta>
            <ta e="T319" id="Seg_9978" s="T318">0.3.h:A</ta>
            <ta e="T321" id="Seg_9979" s="T320">np.h:A</ta>
            <ta e="T325" id="Seg_9980" s="T324">np:G 0.2.h:Poss</ta>
            <ta e="T328" id="Seg_9981" s="T327">np:Ins</ta>
            <ta e="T329" id="Seg_9982" s="T328">0.2.h:A</ta>
            <ta e="T333" id="Seg_9983" s="T332">np:Cau</ta>
            <ta e="T334" id="Seg_9984" s="T333">0.3:P</ta>
            <ta e="T342" id="Seg_9985" s="T341">pro.h:A</ta>
            <ta e="T345" id="Seg_9986" s="T344">pro.h:A</ta>
            <ta e="T349" id="Seg_9987" s="T348">0.3.h:Th</ta>
            <ta e="T358" id="Seg_9988" s="T357">pro:Th</ta>
            <ta e="T359" id="Seg_9989" s="T358">0.3.h:A</ta>
            <ta e="T361" id="Seg_9990" s="T360">pp:L</ta>
            <ta e="T363" id="Seg_9991" s="T362">0.3.h:Th</ta>
            <ta e="T364" id="Seg_9992" s="T363">pp:L</ta>
            <ta e="T369" id="Seg_9993" s="T368">adv:Time</ta>
            <ta e="T370" id="Seg_9994" s="T369">np:So</ta>
            <ta e="T371" id="Seg_9995" s="T370">np:G</ta>
            <ta e="T374" id="Seg_9996" s="T373">np:P</ta>
            <ta e="T378" id="Seg_9997" s="T377">0.3.h:A</ta>
            <ta e="T380" id="Seg_9998" s="T379">np:G</ta>
            <ta e="T383" id="Seg_9999" s="T382">np.h:G 0.3.h:Poss</ta>
            <ta e="T385" id="Seg_10000" s="T384">np.h:A</ta>
            <ta e="T389" id="Seg_10001" s="T388">np.h:P</ta>
            <ta e="T393" id="Seg_10002" s="T392">np:Poss 0.3.h:Poss</ta>
            <ta e="T394" id="Seg_10003" s="T393">np:P</ta>
            <ta e="T395" id="Seg_10004" s="T394">0.3.h:A</ta>
            <ta e="T396" id="Seg_10005" s="T395">0.3.h:A</ta>
            <ta e="T397" id="Seg_10006" s="T396">np:L</ta>
            <ta e="T401" id="Seg_10007" s="T400">np:Ins</ta>
            <ta e="T404" id="Seg_10008" s="T403">np:Poss</ta>
            <ta e="T405" id="Seg_10009" s="T404">pp:Path</ta>
            <ta e="T407" id="Seg_10010" s="T406">np:G</ta>
            <ta e="T411" id="Seg_10011" s="T410">np:Th</ta>
            <ta e="T416" id="Seg_10012" s="T415">np:Th 0.3.h:Poss</ta>
            <ta e="T419" id="Seg_10013" s="T418">np:Th 0.3.h:Poss</ta>
            <ta e="T421" id="Seg_10014" s="T420">np:Ins</ta>
            <ta e="T424" id="Seg_10015" s="T423">0.3.h:A</ta>
            <ta e="T425" id="Seg_10016" s="T424">np:P</ta>
            <ta e="T427" id="Seg_10017" s="T426">np:Poss</ta>
            <ta e="T428" id="Seg_10018" s="T427">np:Poss</ta>
            <ta e="T433" id="Seg_10019" s="T432">np:G</ta>
            <ta e="T436" id="Seg_10020" s="T435">np:Th</ta>
            <ta e="T438" id="Seg_10021" s="T437">0.3.h:A</ta>
            <ta e="T440" id="Seg_10022" s="T439">0.3:Cau</ta>
            <ta e="T923" id="Seg_10023" s="T442">np.h:P</ta>
            <ta e="T446" id="Seg_10024" s="T445">0.3:Cau</ta>
            <ta e="T448" id="Seg_10025" s="T447">0.2.h:A</ta>
            <ta e="T450" id="Seg_10026" s="T449">np:Ins</ta>
            <ta e="T454" id="Seg_10027" s="T453">0.3.h:A</ta>
            <ta e="T457" id="Seg_10028" s="T456">np:Poss</ta>
            <ta e="T459" id="Seg_10029" s="T458">np:L</ta>
            <ta e="T464" id="Seg_10030" s="T463">np:L</ta>
            <ta e="T467" id="Seg_10031" s="T466">np:Cau</ta>
            <ta e="T469" id="Seg_10032" s="T468">0.3:P</ta>
            <ta e="T473" id="Seg_10033" s="T472">np:P</ta>
            <ta e="T475" id="Seg_10034" s="T474">np:Cau</ta>
            <ta e="T479" id="Seg_10035" s="T478">0.3.h:P</ta>
            <ta e="T480" id="Seg_10036" s="T479">adv:G</ta>
            <ta e="T484" id="Seg_10037" s="T483">np.h:A</ta>
            <ta e="T485" id="Seg_10038" s="T484">adv:G</ta>
            <ta e="T487" id="Seg_10039" s="T486">np:Poss</ta>
            <ta e="T488" id="Seg_10040" s="T487">adv:G</ta>
            <ta e="T491" id="Seg_10041" s="T490">0.3.h:Th</ta>
            <ta e="T495" id="Seg_10042" s="T494">adv:L</ta>
            <ta e="T496" id="Seg_10043" s="T495">0.3.h:Th</ta>
            <ta e="T498" id="Seg_10044" s="T497">adv:G</ta>
            <ta e="T499" id="Seg_10045" s="T498">0.3.h:A 0.3.h:P</ta>
            <ta e="T501" id="Seg_10046" s="T500">np.h:A 0.3.h:Poss</ta>
            <ta e="T503" id="Seg_10047" s="T502">np:Th</ta>
            <ta e="T507" id="Seg_10048" s="T506">pp:Path</ta>
            <ta e="T509" id="Seg_10049" s="T508">0.3.h:A 0.3:Th</ta>
            <ta e="T510" id="Seg_10050" s="T509">adv:Time</ta>
            <ta e="T513" id="Seg_10051" s="T512">np:Poss</ta>
            <ta e="T514" id="Seg_10052" s="T513">pp:Path</ta>
            <ta e="T516" id="Seg_10053" s="T515">0.3.h:A 0.3:Th</ta>
            <ta e="T518" id="Seg_10054" s="T517">np:P</ta>
            <ta e="T519" id="Seg_10055" s="T518">np:Ins</ta>
            <ta e="T520" id="Seg_10056" s="T519">0.3.h:A</ta>
            <ta e="T526" id="Seg_10057" s="T525">np:Th</ta>
            <ta e="T534" id="Seg_10058" s="T533">0.2.h:A</ta>
            <ta e="T537" id="Seg_10059" s="T536">np.h:A</ta>
            <ta e="T538" id="Seg_10060" s="T537">adv:G</ta>
            <ta e="T541" id="Seg_10061" s="T540">np.h:A</ta>
            <ta e="T542" id="Seg_10062" s="T541">adv:Time</ta>
            <ta e="T547" id="Seg_10063" s="T546">np.h:A</ta>
            <ta e="T548" id="Seg_10064" s="T547">adv:G</ta>
            <ta e="T550" id="Seg_10065" s="T549">np:G</ta>
            <ta e="T556" id="Seg_10066" s="T555">adv:G</ta>
            <ta e="T559" id="Seg_10067" s="T558">np:G</ta>
            <ta e="T562" id="Seg_10068" s="T561">0.2.h:A</ta>
            <ta e="T566" id="Seg_10069" s="T565">np.h:G</ta>
            <ta e="T567" id="Seg_10070" s="T566">0.3.h:A</ta>
            <ta e="T569" id="Seg_10071" s="T568">adv:G</ta>
            <ta e="T570" id="Seg_10072" s="T569">0.3.h:A</ta>
            <ta e="T572" id="Seg_10073" s="T571">adv:L</ta>
            <ta e="T576" id="Seg_10074" s="T575">np.h:Th</ta>
            <ta e="T583" id="Seg_10075" s="T582">0.1.h:A</ta>
            <ta e="T593" id="Seg_10076" s="T592">np.h:A</ta>
            <ta e="T594" id="Seg_10077" s="T593">adv:G</ta>
            <ta e="T595" id="Seg_10078" s="T594">0.3.h:P</ta>
            <ta e="T597" id="Seg_10079" s="T596">np:P 0.3.h:Poss</ta>
            <ta e="T601" id="Seg_10080" s="T600">adv:Time</ta>
            <ta e="T602" id="Seg_10081" s="T601">adv:G</ta>
            <ta e="T603" id="Seg_10082" s="T602">0.3.h:A</ta>
            <ta e="T605" id="Seg_10083" s="T604">adv:G</ta>
            <ta e="T606" id="Seg_10084" s="T605">0.3.h:A</ta>
            <ta e="T607" id="Seg_10085" s="T606">adv:Time</ta>
            <ta e="T609" id="Seg_10086" s="T608">np.h:A</ta>
            <ta e="T610" id="Seg_10087" s="T609">adv:G</ta>
            <ta e="T614" id="Seg_10088" s="T613">np.h:Poss</ta>
            <ta e="T615" id="Seg_10089" s="T614">np:Th</ta>
            <ta e="T616" id="Seg_10090" s="T615">adv:L</ta>
            <ta e="T618" id="Seg_10091" s="T617">adv:L</ta>
            <ta e="T620" id="Seg_10092" s="T619">adv:G</ta>
            <ta e="T621" id="Seg_10093" s="T620">0.3.h:A</ta>
            <ta e="T623" id="Seg_10094" s="T622">adv:G</ta>
            <ta e="T624" id="Seg_10095" s="T623">0.3.h:A</ta>
            <ta e="T625" id="Seg_10096" s="T624">np.h:Poss</ta>
            <ta e="T626" id="Seg_10097" s="T625">np:Ins</ta>
            <ta e="T628" id="Seg_10098" s="T627">np.h:Poss</ta>
            <ta e="T629" id="Seg_10099" s="T628">np:Ins</ta>
            <ta e="T634" id="Seg_10100" s="T633">adv:G</ta>
            <ta e="T636" id="Seg_10101" s="T635">0.3.h:A</ta>
            <ta e="T639" id="Seg_10102" s="T638">adv:G</ta>
            <ta e="T643" id="Seg_10103" s="T642">np.h:Th</ta>
            <ta e="T645" id="Seg_10104" s="T644">0.3.h:A</ta>
            <ta e="T649" id="Seg_10105" s="T648">np.h:Th</ta>
            <ta e="T650" id="Seg_10106" s="T649">adv:L</ta>
            <ta e="T653" id="Seg_10107" s="T652">adv:Time</ta>
            <ta e="T655" id="Seg_10108" s="T654">np.h:G 0.3.h:Poss</ta>
            <ta e="T656" id="Seg_10109" s="T655">0.3.h:A</ta>
            <ta e="T658" id="Seg_10110" s="T657">adv:G</ta>
            <ta e="T662" id="Seg_10111" s="T661">np.h:Th</ta>
            <ta e="T663" id="Seg_10112" s="T662">adv:G</ta>
            <ta e="T664" id="Seg_10113" s="T663">0.3.h:A</ta>
            <ta e="T673" id="Seg_10114" s="T672">np.h:A</ta>
            <ta e="T674" id="Seg_10115" s="T673">np:Com</ta>
            <ta e="T677" id="Seg_10116" s="T676">adv:G</ta>
            <ta e="T682" id="Seg_10117" s="T681">np.h:A</ta>
            <ta e="T684" id="Seg_10118" s="T683">pro:Com</ta>
            <ta e="T697" id="Seg_10119" s="T696">np.h:Poss</ta>
            <ta e="T699" id="Seg_10120" s="T698">np.h:Th</ta>
            <ta e="T716" id="Seg_10121" s="T715">np:Th</ta>
            <ta e="T717" id="Seg_10122" s="T716">0.3.h:A</ta>
            <ta e="T718" id="Seg_10123" s="T717">adv:G</ta>
            <ta e="T719" id="Seg_10124" s="T718">0.3.h:A</ta>
            <ta e="T722" id="Seg_10125" s="T721">np:G</ta>
            <ta e="T725" id="Seg_10126" s="T724">0.3.h:A</ta>
            <ta e="T733" id="Seg_10127" s="T732">np.h:A 0.3.h:Poss</ta>
            <ta e="T737" id="Seg_10128" s="T736">np:G</ta>
            <ta e="T738" id="Seg_10129" s="T737">0.3.h:A</ta>
            <ta e="T739" id="Seg_10130" s="T738">0.3.h:A</ta>
            <ta e="T744" id="Seg_10131" s="T743">0.3.h:A</ta>
            <ta e="T747" id="Seg_10132" s="T746">adv:Time</ta>
            <ta e="T748" id="Seg_10133" s="T747">0.3.h:Th</ta>
            <ta e="T750" id="Seg_10134" s="T749">0.3.h:Th</ta>
            <ta e="T752" id="Seg_10135" s="T751">np.h:A 0.3.h:Poss</ta>
            <ta e="T753" id="Seg_10136" s="T752">np:So</ta>
            <ta e="T757" id="Seg_10137" s="T756">adv:G</ta>
            <ta e="T760" id="Seg_10138" s="T759">0.3.h:A</ta>
            <ta e="T762" id="Seg_10139" s="T761">pro:Th</ta>
            <ta e="T767" id="Seg_10140" s="T766">np.h:Poss</ta>
            <ta e="T769" id="Seg_10141" s="T768">np:Poss</ta>
            <ta e="T770" id="Seg_10142" s="T769">np:Th</ta>
            <ta e="T772" id="Seg_10143" s="T771">adv:G</ta>
            <ta e="T774" id="Seg_10144" s="T773">0.3.h:A</ta>
            <ta e="T776" id="Seg_10145" s="T775">np:Cau</ta>
            <ta e="T777" id="Seg_10146" s="T776">0.3:P</ta>
            <ta e="T779" id="Seg_10147" s="T778">np.h:P</ta>
            <ta e="T782" id="Seg_10148" s="T781">pro.h:A</ta>
            <ta e="T784" id="Seg_10149" s="T783">0.3.h:E</ta>
            <ta e="T790" id="Seg_10150" s="T789">0.2.h:A</ta>
            <ta e="T796" id="Seg_10151" s="T795">0.3.h:A</ta>
            <ta e="T798" id="Seg_10152" s="T797">np:L</ta>
            <ta e="T799" id="Seg_10153" s="T798">np:Th 0.3.h:Poss</ta>
            <ta e="T802" id="Seg_10154" s="T801">np.h:Poss 0.3.h:Poss</ta>
            <ta e="T803" id="Seg_10155" s="T802">np:Th</ta>
            <ta e="T805" id="Seg_10156" s="T804">np.h:A 0.3.h:Poss</ta>
            <ta e="T806" id="Seg_10157" s="T805">adv:L</ta>
            <ta e="T807" id="Seg_10158" s="T806">0.3:Th</ta>
            <ta e="T809" id="Seg_10159" s="T808">np:Th</ta>
            <ta e="T811" id="Seg_10160" s="T810">adv:L</ta>
            <ta e="T813" id="Seg_10161" s="T812">np.h:Poss</ta>
            <ta e="T814" id="Seg_10162" s="T813">np:Th</ta>
            <ta e="T815" id="Seg_10163" s="T814">0.3.h:A</ta>
            <ta e="T817" id="Seg_10164" s="T816">np:Poss</ta>
            <ta e="T818" id="Seg_10165" s="T817">np:P</ta>
            <ta e="T819" id="Seg_10166" s="T818">0.3.h:A</ta>
            <ta e="T820" id="Seg_10167" s="T819">np:A</ta>
            <ta e="T823" id="Seg_10168" s="T822">np.h:P</ta>
            <ta e="T831" id="Seg_10169" s="T830">np:A 0.1.h:Poss</ta>
            <ta e="T835" id="Seg_10170" s="T834">np:A</ta>
            <ta e="T836" id="Seg_10171" s="T835">0.3:Th</ta>
            <ta e="T840" id="Seg_10172" s="T839">np.h:Poss</ta>
            <ta e="T841" id="Seg_10173" s="T840">np.h:A</ta>
            <ta e="T842" id="Seg_10174" s="T841">adv:G</ta>
            <ta e="T848" id="Seg_10175" s="T847">np.h:Poss</ta>
            <ta e="T850" id="Seg_10176" s="T849">np.h:Th</ta>
            <ta e="T852" id="Seg_10177" s="T851">adv:G</ta>
            <ta e="T854" id="Seg_10178" s="T853">0.3.h:A</ta>
            <ta e="T855" id="Seg_10179" s="T854">pro:Th</ta>
            <ta e="T857" id="Seg_10180" s="T856">np.h:Th 0.3.h:Poss</ta>
            <ta e="T862" id="Seg_10181" s="T861">0.3.h:E</ta>
            <ta e="T865" id="Seg_10182" s="T864">np.h:P 0.3.h:Poss</ta>
            <ta e="T869" id="Seg_10183" s="T868">np:L</ta>
            <ta e="T872" id="Seg_10184" s="T871">pro.h:A</ta>
            <ta e="T874" id="Seg_10185" s="T873">np:P 0.1.h:Poss</ta>
            <ta e="T876" id="Seg_10186" s="T875">pro.h:A</ta>
            <ta e="T877" id="Seg_10187" s="T876">np:Ins</ta>
            <ta e="T881" id="Seg_10188" s="T880">np.h:A</ta>
            <ta e="T884" id="Seg_10189" s="T883">pro.h:R</ta>
            <ta e="T896" id="Seg_10190" s="T895">0.2.h:A</ta>
            <ta e="T898" id="Seg_10191" s="T897">adv:Time</ta>
            <ta e="T901" id="Seg_10192" s="T900">0.3.h:Th</ta>
            <ta e="T902" id="Seg_10193" s="T901">adv:L</ta>
            <ta e="T907" id="Seg_10194" s="T906">0.3.h:A</ta>
            <ta e="T914" id="Seg_10195" s="T913">np:Poss</ta>
            <ta e="T915" id="Seg_10196" s="T914">np:Th</ta>
            <ta e="T916" id="Seg_10197" s="T915">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T12" id="Seg_10198" s="T11">0.2.h:S v:pred</ta>
            <ta e="T15" id="Seg_10199" s="T14">pro:O</ta>
            <ta e="T23" id="Seg_10200" s="T22">0.1.h:S v:pred</ta>
            <ta e="T31" id="Seg_10201" s="T30">np.h:S</ta>
            <ta e="T32" id="Seg_10202" s="T31">v:pred</ta>
            <ta e="T39" id="Seg_10203" s="T38">0.3.h:S v:pred</ta>
            <ta e="T41" id="Seg_10204" s="T40">np.h:S</ta>
            <ta e="T44" id="Seg_10205" s="T43">v:pred</ta>
            <ta e="T46" id="Seg_10206" s="T44">s:purp</ta>
            <ta e="T48" id="Seg_10207" s="T47">np.h:S</ta>
            <ta e="T49" id="Seg_10208" s="T48">np.h:S</ta>
            <ta e="T51" id="Seg_10209" s="T50">v:pred</ta>
            <ta e="T52" id="Seg_10210" s="T51">s:purp</ta>
            <ta e="T54" id="Seg_10211" s="T53">s:purp</ta>
            <ta e="T56" id="Seg_10212" s="T55">0.3.h:S v:pred</ta>
            <ta e="T64" id="Seg_10213" s="T60">s:adv</ta>
            <ta e="T69" id="Seg_10214" s="T68">np.h:O</ta>
            <ta e="T70" id="Seg_10215" s="T69">v:pred</ta>
            <ta e="T72" id="Seg_10216" s="T71">np.h:S</ta>
            <ta e="T83" id="Seg_10217" s="T82">pro.h:S</ta>
            <ta e="T84" id="Seg_10218" s="T83">v:pred</ta>
            <ta e="T87" id="Seg_10219" s="T86">pro.h:S</ta>
            <ta e="T89" id="Seg_10220" s="T88">v:pred</ta>
            <ta e="T92" id="Seg_10221" s="T91">np.h:S</ta>
            <ta e="T94" id="Seg_10222" s="T92">s:temp</ta>
            <ta e="T96" id="Seg_10223" s="T95">v:pred</ta>
            <ta e="T100" id="Seg_10224" s="T98">s:rel</ta>
            <ta e="T106" id="Seg_10225" s="T105">0.2.h:S v:pred</ta>
            <ta e="T107" id="Seg_10226" s="T106">0.2.h:S v:pred</ta>
            <ta e="T110" id="Seg_10227" s="T109">np:O</ta>
            <ta e="T112" id="Seg_10228" s="T111">0.3.h:S v:pred</ta>
            <ta e="T114" id="Seg_10229" s="T113">0.2.h:S v:pred</ta>
            <ta e="T118" id="Seg_10230" s="T117">np.h:S</ta>
            <ta e="T120" id="Seg_10231" s="T119">v:pred</ta>
            <ta e="T122" id="Seg_10232" s="T121">np.h:S</ta>
            <ta e="T126" id="Seg_10233" s="T125">v:pred</ta>
            <ta e="T128" id="Seg_10234" s="T127">np.h:S</ta>
            <ta e="T131" id="Seg_10235" s="T130">v:pred</ta>
            <ta e="T138" id="Seg_10236" s="T137">0.3.h:S v:pred</ta>
            <ta e="T141" id="Seg_10237" s="T140">np:O</ta>
            <ta e="T142" id="Seg_10238" s="T141">0.2.h:S v:pred</ta>
            <ta e="T145" id="Seg_10239" s="T144">np:O</ta>
            <ta e="T146" id="Seg_10240" s="T145">np:O</ta>
            <ta e="T147" id="Seg_10241" s="T146">0.2.h:S v:pred</ta>
            <ta e="T150" id="Seg_10242" s="T149">np:O</ta>
            <ta e="T153" id="Seg_10243" s="T152">pro:O</ta>
            <ta e="T154" id="Seg_10244" s="T153">0.2.h:S v:pred</ta>
            <ta e="T156" id="Seg_10245" s="T155">np.h:S</ta>
            <ta e="T158" id="Seg_10246" s="T157">v:pred</ta>
            <ta e="T159" id="Seg_10247" s="T158">0.3.h:S v:pred</ta>
            <ta e="T161" id="Seg_10248" s="T160">np.h:S</ta>
            <ta e="T163" id="Seg_10249" s="T162">v:pred</ta>
            <ta e="T165" id="Seg_10250" s="T164">np:O</ta>
            <ta e="T166" id="Seg_10251" s="T165">0.3.h:S v:pred</ta>
            <ta e="T173" id="Seg_10252" s="T172">np:O</ta>
            <ta e="T176" id="Seg_10253" s="T175">0.3.h:S v:pred</ta>
            <ta e="T179" id="Seg_10254" s="T178">np.h:S</ta>
            <ta e="T180" id="Seg_10255" s="T179">np.h:O</ta>
            <ta e="T181" id="Seg_10256" s="T180">v:pred</ta>
            <ta e="T199" id="Seg_10257" s="T198">np.h:S</ta>
            <ta e="T201" id="Seg_10258" s="T200">v:pred</ta>
            <ta e="T206" id="Seg_10259" s="T205">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T208" id="Seg_10260" s="T207">np.h:S</ta>
            <ta e="T210" id="Seg_10261" s="T209">v:pred</ta>
            <ta e="T214" id="Seg_10262" s="T213">0.3.h:S v:pred</ta>
            <ta e="T220" id="Seg_10263" s="T219">0.3.h:S v:pred</ta>
            <ta e="T222" id="Seg_10264" s="T221">np:O</ta>
            <ta e="T223" id="Seg_10265" s="T222">0.3.h:S v:pred</ta>
            <ta e="T225" id="Seg_10266" s="T224">np.h:S</ta>
            <ta e="T226" id="Seg_10267" s="T225">np:O</ta>
            <ta e="T228" id="Seg_10268" s="T227">v:pred</ta>
            <ta e="T232" id="Seg_10269" s="T231">np:S</ta>
            <ta e="T233" id="Seg_10270" s="T232">np:S</ta>
            <ta e="T240" id="Seg_10271" s="T239">np:S</ta>
            <ta e="T242" id="Seg_10272" s="T241">np.h:S</ta>
            <ta e="T244" id="Seg_10273" s="T243">v:pred</ta>
            <ta e="T249" id="Seg_10274" s="T248">np.h:S</ta>
            <ta e="T251" id="Seg_10275" s="T250">v:pred</ta>
            <ta e="T253" id="Seg_10276" s="T252">0.2.h:S v:pred</ta>
            <ta e="T255" id="Seg_10277" s="T254">np.h:O</ta>
            <ta e="T257" id="Seg_10278" s="T256">0.3.h:S v:pred</ta>
            <ta e="T258" id="Seg_10279" s="T257">pro.h:S</ta>
            <ta e="T260" id="Seg_10280" s="T259">v:pred</ta>
            <ta e="T262" id="Seg_10281" s="T261">np.h:O</ta>
            <ta e="T265" id="Seg_10282" s="T264">0.2.h:S v:pred</ta>
            <ta e="T267" id="Seg_10283" s="T266">np.h:S</ta>
            <ta e="T269" id="Seg_10284" s="T268">v:pred</ta>
            <ta e="T271" id="Seg_10285" s="T269">s:temp</ta>
            <ta e="T277" id="Seg_10286" s="T275">s:rel</ta>
            <ta e="T278" id="Seg_10287" s="T277">np.h:S</ta>
            <ta e="T279" id="Seg_10288" s="T278">v:pred</ta>
            <ta e="T288" id="Seg_10289" s="T287">0.3.h:S v:pred</ta>
            <ta e="T289" id="Seg_10290" s="T288">np.h:O</ta>
            <ta e="T293" id="Seg_10291" s="T292">0.3.h:S v:pred</ta>
            <ta e="T295" id="Seg_10292" s="T294">np:O</ta>
            <ta e="T296" id="Seg_10293" s="T295">0.3.h:S v:pred</ta>
            <ta e="T298" id="Seg_10294" s="T297">np:O</ta>
            <ta e="T299" id="Seg_10295" s="T298">0.3.h:S v:pred</ta>
            <ta e="T312" id="Seg_10296" s="T311">pro.h:S</ta>
            <ta e="T314" id="Seg_10297" s="T313">v:pred</ta>
            <ta e="T316" id="Seg_10298" s="T315">np.h:S</ta>
            <ta e="T317" id="Seg_10299" s="T316">v:pred</ta>
            <ta e="T319" id="Seg_10300" s="T318">0.3.h:S v:pred</ta>
            <ta e="T321" id="Seg_10301" s="T320">np.h:S</ta>
            <ta e="T323" id="Seg_10302" s="T322">v:pred</ta>
            <ta e="T329" id="Seg_10303" s="T328">0.2.h:S v:pred</ta>
            <ta e="T333" id="Seg_10304" s="T332">np:S</ta>
            <ta e="T334" id="Seg_10305" s="T333">v:pred 0.3:O</ta>
            <ta e="T342" id="Seg_10306" s="T341">pro.h:S</ta>
            <ta e="T344" id="Seg_10307" s="T343">v:pred</ta>
            <ta e="T345" id="Seg_10308" s="T344">pro.h:S</ta>
            <ta e="T347" id="Seg_10309" s="T346">v:pred</ta>
            <ta e="T349" id="Seg_10310" s="T348">0.3.h:S v:pred</ta>
            <ta e="T358" id="Seg_10311" s="T357">pro:O</ta>
            <ta e="T359" id="Seg_10312" s="T358">0.3.h:S v:pred</ta>
            <ta e="T363" id="Seg_10313" s="T362">0.3.h:S v:pred</ta>
            <ta e="T374" id="Seg_10314" s="T373">np:S</ta>
            <ta e="T375" id="Seg_10315" s="T374">v:pred</ta>
            <ta e="T378" id="Seg_10316" s="T377">0.3.h:S v:pred</ta>
            <ta e="T385" id="Seg_10317" s="T384">np.h:S</ta>
            <ta e="T387" id="Seg_10318" s="T386">v:pred</ta>
            <ta e="T389" id="Seg_10319" s="T388">np.h:O</ta>
            <ta e="T395" id="Seg_10320" s="T389">s:temp</ta>
            <ta e="T396" id="Seg_10321" s="T395">0.3.h:S v:pred</ta>
            <ta e="T411" id="Seg_10322" s="T410">np:S</ta>
            <ta e="T412" id="Seg_10323" s="T411">v:pred</ta>
            <ta e="T416" id="Seg_10324" s="T415">np:S</ta>
            <ta e="T417" id="Seg_10325" s="T416">v:pred</ta>
            <ta e="T419" id="Seg_10326" s="T418">np:S</ta>
            <ta e="T422" id="Seg_10327" s="T421">v:pred</ta>
            <ta e="T424" id="Seg_10328" s="T423">0.3.h:S v:pred</ta>
            <ta e="T425" id="Seg_10329" s="T424">np:O</ta>
            <ta e="T430" id="Seg_10330" s="T429">v:pred</ta>
            <ta e="T436" id="Seg_10331" s="T435">np:O</ta>
            <ta e="T438" id="Seg_10332" s="T437">0.3.h:S v:pred</ta>
            <ta e="T440" id="Seg_10333" s="T439">0.3:S v:pred</ta>
            <ta e="T923" id="Seg_10334" s="T442">np.h:O</ta>
            <ta e="T446" id="Seg_10335" s="T445">0.3:S v:pred</ta>
            <ta e="T448" id="Seg_10336" s="T447">0.2.h:S v:pred</ta>
            <ta e="T454" id="Seg_10337" s="T453">0.3.h:S v:pred</ta>
            <ta e="T467" id="Seg_10338" s="T466">np:S</ta>
            <ta e="T469" id="Seg_10339" s="T468">v:pred 0.3:O</ta>
            <ta e="T473" id="Seg_10340" s="T472">np:O</ta>
            <ta e="T475" id="Seg_10341" s="T474">np:S</ta>
            <ta e="T476" id="Seg_10342" s="T475">v:pred</ta>
            <ta e="T479" id="Seg_10343" s="T478">0.3.h:S v:pred</ta>
            <ta e="T484" id="Seg_10344" s="T483">np.h:S</ta>
            <ta e="T486" id="Seg_10345" s="T485">v:pred</ta>
            <ta e="T491" id="Seg_10346" s="T490">0.3.h:S v:pred</ta>
            <ta e="T496" id="Seg_10347" s="T495">0.3.h:S v:pred</ta>
            <ta e="T499" id="Seg_10348" s="T498">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T501" id="Seg_10349" s="T500">np.h:S</ta>
            <ta e="T503" id="Seg_10350" s="T502">np:O</ta>
            <ta e="T505" id="Seg_10351" s="T504">v:pred</ta>
            <ta e="T509" id="Seg_10352" s="T508">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T516" id="Seg_10353" s="T515">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T518" id="Seg_10354" s="T517">np:O</ta>
            <ta e="T520" id="Seg_10355" s="T519">0.3.h:S v:pred</ta>
            <ta e="T526" id="Seg_10356" s="T525">np:S</ta>
            <ta e="T528" id="Seg_10357" s="T527">adj:pred</ta>
            <ta e="T529" id="Seg_10358" s="T528">cop</ta>
            <ta e="T534" id="Seg_10359" s="T533">0.2.h:S v:pred</ta>
            <ta e="T537" id="Seg_10360" s="T536">np.h:S</ta>
            <ta e="T539" id="Seg_10361" s="T538">v:pred</ta>
            <ta e="T541" id="Seg_10362" s="T540">np.h:S</ta>
            <ta e="T544" id="Seg_10363" s="T543">v:pred</ta>
            <ta e="T547" id="Seg_10364" s="T546">np.h:S</ta>
            <ta e="T551" id="Seg_10365" s="T550">v:pred</ta>
            <ta e="T562" id="Seg_10366" s="T561">0.2.h:S v:pred</ta>
            <ta e="T567" id="Seg_10367" s="T563">s:purp</ta>
            <ta e="T570" id="Seg_10368" s="T569">0.3.h:S v:pred</ta>
            <ta e="T576" id="Seg_10369" s="T575">np.h:S</ta>
            <ta e="T577" id="Seg_10370" s="T576">v:pred</ta>
            <ta e="T583" id="Seg_10371" s="T582">0.1.h:S v:pred</ta>
            <ta e="T593" id="Seg_10372" s="T592">np.h:S</ta>
            <ta e="T595" id="Seg_10373" s="T594">v:pred 0.3.h:O</ta>
            <ta e="T597" id="Seg_10374" s="T596">np:S</ta>
            <ta e="T599" id="Seg_10375" s="T598">v:pred</ta>
            <ta e="T603" id="Seg_10376" s="T602">0.3.h:S v:pred</ta>
            <ta e="T606" id="Seg_10377" s="T605">0.3.h:S v:pred</ta>
            <ta e="T609" id="Seg_10378" s="T608">np.h:S</ta>
            <ta e="T611" id="Seg_10379" s="T610">v:pred</ta>
            <ta e="T615" id="Seg_10380" s="T614">np:S</ta>
            <ta e="T617" id="Seg_10381" s="T616">v:pred</ta>
            <ta e="T621" id="Seg_10382" s="T620">0.3.h:S v:pred</ta>
            <ta e="T624" id="Seg_10383" s="T623">0.3.h:S v:pred</ta>
            <ta e="T635" id="Seg_10384" s="T634">s:temp</ta>
            <ta e="T636" id="Seg_10385" s="T635">0.3.h:S v:pred</ta>
            <ta e="T643" id="Seg_10386" s="T642">np.h:O</ta>
            <ta e="T645" id="Seg_10387" s="T644">0.3.h:S v:pred</ta>
            <ta e="T649" id="Seg_10388" s="T648">np.h:S</ta>
            <ta e="T652" id="Seg_10389" s="T651">v:pred</ta>
            <ta e="T656" id="Seg_10390" s="T655">0.3.h:S v:pred</ta>
            <ta e="T662" id="Seg_10391" s="T661">np.h:O</ta>
            <ta e="T664" id="Seg_10392" s="T663">0.3.h:S v:pred</ta>
            <ta e="T673" id="Seg_10393" s="T672">np.h:S</ta>
            <ta e="T678" id="Seg_10394" s="T677">v:pred</ta>
            <ta e="T682" id="Seg_10395" s="T681">np.h:S</ta>
            <ta e="T685" id="Seg_10396" s="T684">v:pred</ta>
            <ta e="T699" id="Seg_10397" s="T698">np.h:S</ta>
            <ta e="T700" id="Seg_10398" s="T699">v:pred</ta>
            <ta e="T717" id="Seg_10399" s="T715">s:temp</ta>
            <ta e="T719" id="Seg_10400" s="T718">0.3.h:S v:pred</ta>
            <ta e="T725" id="Seg_10401" s="T724">0.3.h:S v:pred</ta>
            <ta e="T733" id="Seg_10402" s="T732">np.h:S</ta>
            <ta e="T736" id="Seg_10403" s="T735">v:pred</ta>
            <ta e="T738" id="Seg_10404" s="T737">s:purp</ta>
            <ta e="T739" id="Seg_10405" s="T738">s:purp</ta>
            <ta e="T744" id="Seg_10406" s="T743">0.3.h:S v:pred</ta>
            <ta e="T748" id="Seg_10407" s="T747">0.3.h:S v:pred</ta>
            <ta e="T750" id="Seg_10408" s="T749">0.3.h:S v:pred</ta>
            <ta e="T752" id="Seg_10409" s="T751">np.h:S</ta>
            <ta e="T755" id="Seg_10410" s="T754">v:pred</ta>
            <ta e="T759" id="Seg_10411" s="T758">s:adv</ta>
            <ta e="T760" id="Seg_10412" s="T759">0.3.h:S v:pred</ta>
            <ta e="T762" id="Seg_10413" s="T761">pro:S</ta>
            <ta e="T763" id="Seg_10414" s="T762">v:pred</ta>
            <ta e="T770" id="Seg_10415" s="T769">np:S</ta>
            <ta e="T774" id="Seg_10416" s="T773">0.3.h:S v:pred</ta>
            <ta e="T776" id="Seg_10417" s="T775">np:S</ta>
            <ta e="T777" id="Seg_10418" s="T776">v:pred 0.3:O</ta>
            <ta e="T779" id="Seg_10419" s="T778">np.h:O</ta>
            <ta e="T781" id="Seg_10420" s="T780">v:pred</ta>
            <ta e="T782" id="Seg_10421" s="T781">pro.h:S</ta>
            <ta e="T784" id="Seg_10422" s="T783">0.3.h:S v:pred</ta>
            <ta e="T790" id="Seg_10423" s="T789">0.2.h:S v:pred</ta>
            <ta e="T795" id="Seg_10424" s="T794">s:temp</ta>
            <ta e="T796" id="Seg_10425" s="T795">0.3.h:S v:pred</ta>
            <ta e="T799" id="Seg_10426" s="T798">np:S</ta>
            <ta e="T803" id="Seg_10427" s="T802">np:S</ta>
            <ta e="T805" id="Seg_10428" s="T804">np.h:S</ta>
            <ta e="T807" id="Seg_10429" s="T806">v:pred 0.3:O</ta>
            <ta e="T809" id="Seg_10430" s="T808">np:O</ta>
            <ta e="T811" id="Seg_10431" s="T809">s:rel</ta>
            <ta e="T815" id="Seg_10432" s="T814">0.3.h:S v:pred</ta>
            <ta e="T818" id="Seg_10433" s="T817">np:O</ta>
            <ta e="T819" id="Seg_10434" s="T818">0.3.h:S v:pred</ta>
            <ta e="T820" id="Seg_10435" s="T819">np:S</ta>
            <ta e="T821" id="Seg_10436" s="T820">v:pred</ta>
            <ta e="T823" id="Seg_10437" s="T822">np.h:S</ta>
            <ta e="T825" id="Seg_10438" s="T824">v:pred</ta>
            <ta e="T831" id="Seg_10439" s="T830">np:S</ta>
            <ta e="T833" id="Seg_10440" s="T832">v:pred</ta>
            <ta e="T835" id="Seg_10441" s="T834">np:S</ta>
            <ta e="T836" id="Seg_10442" s="T835">v:pred 0.3:O</ta>
            <ta e="T841" id="Seg_10443" s="T840">np.h:S</ta>
            <ta e="T843" id="Seg_10444" s="T842">v:pred</ta>
            <ta e="T850" id="Seg_10445" s="T849">np.h:S</ta>
            <ta e="T854" id="Seg_10446" s="T853">0.3.h:S v:pred</ta>
            <ta e="T855" id="Seg_10447" s="T854">pro:S</ta>
            <ta e="T856" id="Seg_10448" s="T855">v:pred</ta>
            <ta e="T857" id="Seg_10449" s="T856">np.h:S</ta>
            <ta e="T862" id="Seg_10450" s="T861">0.3.h:S v:pred</ta>
            <ta e="T865" id="Seg_10451" s="T864">np.h:S</ta>
            <ta e="T867" id="Seg_10452" s="T866">v:pred</ta>
            <ta e="T872" id="Seg_10453" s="T871">pro.h:S</ta>
            <ta e="T874" id="Seg_10454" s="T873">np:O</ta>
            <ta e="T876" id="Seg_10455" s="T875">pro.h:S</ta>
            <ta e="T878" id="Seg_10456" s="T877">v:pred</ta>
            <ta e="T881" id="Seg_10457" s="T880">np.h:S</ta>
            <ta e="T896" id="Seg_10458" s="T895">0.2.h:S v:pred</ta>
            <ta e="T901" id="Seg_10459" s="T900">0.3.h:S v:pred</ta>
            <ta e="T907" id="Seg_10460" s="T906">0.3.h:S v:pred</ta>
            <ta e="T915" id="Seg_10461" s="T914">np:O</ta>
            <ta e="T916" id="Seg_10462" s="T915">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T108" id="Seg_10463" s="T107">RUS:gram</ta>
            <ta e="T226" id="Seg_10464" s="T225">RUS:cult</ta>
            <ta e="T404" id="Seg_10465" s="T403">RUS:cult</ta>
            <ta e="T411" id="Seg_10466" s="T410">RUS:cult</ta>
            <ta e="T450" id="Seg_10467" s="T449">RUS:cult</ta>
            <ta e="T513" id="Seg_10468" s="T512">RUS:cult</ta>
            <ta e="T571" id="Seg_10469" s="T570">RUS:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_10470" s="T1">[KuAI:] A fairy tale, written by Elizaveta Pavlovna.</ta>
            <ta e="T9" id="Seg_10471" s="T6">Well, speak, please.</ta>
            <ta e="T15" id="Seg_10472" s="T921">[NN:] Well, say something else.</ta>
            <ta e="T23" id="Seg_10473" s="T922">[NEP:] So what, I'll tell a story about The Hare Parka old man.</ta>
            <ta e="T32" id="Seg_10474" s="T24">This Hare Parka old man, long time ago he had seven sons.</ta>
            <ta e="T36" id="Seg_10475" s="T33">And seven daughters-in-law.</ta>
            <ta e="T39" id="Seg_10476" s="T37">They came in the morning.</ta>
            <ta e="T46" id="Seg_10477" s="T40">The Nenets from the North came upstream to kill him.</ta>
            <ta e="T52" id="Seg_10478" s="T47">His people, his sons went to the forest to hunt squirrels.</ta>
            <ta e="T64" id="Seg_10479" s="T53">Now they went to the forest to hunt squirrels_ now in winter, how it is usually in winter.</ta>
            <ta e="T72" id="Seg_10480" s="T65">This guy, that Nenets was raising a Selkup guy.</ta>
            <ta e="T75" id="Seg_10481" s="T73">From that war.</ta>
            <ta e="T90" id="Seg_10482" s="T76">The Hare Parka old man, tomorrow, he says, we'll go to his house, you should go there, he says.</ta>
            <ta e="T100" id="Seg_10483" s="T91">Two guys, having gone home, they say so, the two orphaned guys, whom the Nenets had been raising.</ta>
            <ta e="T104" id="Seg_10484" s="T101">These two Selkup guys…</ta>
            <ta e="T112" id="Seg_10485" s="T105">Be quiet, be quiet, as if someone had beaten a dog.</ta>
            <ta e="T114" id="Seg_10486" s="T113">Wait.</ta>
            <ta e="T120" id="Seg_10487" s="T115">Then the old man, where can he go?</ta>
            <ta e="T126" id="Seg_10488" s="T121">The old man was probably lying.</ta>
            <ta e="T135" id="Seg_10489" s="T127">The old man became so, this Hare Parka old man.</ta>
            <ta e="T143" id="Seg_10490" s="T136">He became [=said] so to his daughters-in-law: “Bring some food down [=from the rear part of the tent].</ta>
            <ta e="T154" id="Seg_10491" s="T144">Bring some fat, food, whichever (hidden?) food, bring whatever.</ta>
            <ta e="T159" id="Seg_10492" s="T155">The Nenets men will come tomorrow, they'll eat it.”</ta>
            <ta e="T163" id="Seg_10493" s="T160">The old man said so.</ta>
            <ta e="T166" id="Seg_10494" s="T164">They brought food.</ta>
            <ta e="T177" id="Seg_10495" s="T167">Like this with grass, they laid the new grass inside the house.</ta>
            <ta e="T181" id="Seg_10496" s="T178">The old man starts giving food to the Nenets men.</ta>
            <ta e="T191" id="Seg_10497" s="T182">Well, the next day, an army of thirty Nenets men.</ta>
            <ta e="T202" id="Seg_10498" s="T192">Long ago, probably the Hare Parka old man stayed alone in the house with his daughters-in-law.</ta>
            <ta e="T206" id="Seg_10499" s="T203">He laid seven [types of] grass.</ta>
            <ta e="T210" id="Seg_10500" s="T207">The old man became so.</ta>
            <ta e="T218" id="Seg_10501" s="T211">They gathered in the house, those Nenets men.</ta>
            <ta e="T220" id="Seg_10502" s="T219">They started eating.</ta>
            <ta e="T223" id="Seg_10503" s="T221">They found food.</ta>
            <ta e="T240" id="Seg_10504" s="T224">His daughters-in-law cooked tea, there was whatever food down there, fat, whatever dried food.</ta>
            <ta e="T251" id="Seg_10505" s="T241">The old man said so to his wife, the Hare Parka old man had also a wife.</ta>
            <ta e="T253" id="Seg_10506" s="T252">Wait.</ta>
            <ta e="T260" id="Seg_10507" s="T254">He sent his daughters-in-law like this: “ You go outside.</ta>
            <ta e="T265" id="Seg_10508" s="T261">Take your children outside (as well?).”</ta>
            <ta e="T271" id="Seg_10509" s="T266">The old man stayed [there] for a long time, feeding people.</ta>
            <ta e="T279" id="Seg_10510" s="T272">Once the people who had come (down?) were eating.</ta>
            <ta e="T288" id="Seg_10511" s="T280">He (put?) much firewood into the oven and set fire.</ta>
            <ta e="T296" id="Seg_10512" s="T288">He sent away his daughters-in-law not without purpose, they will set fire outside.</ta>
            <ta e="T308" id="Seg_10513" s="T297">They set fire, the earlier dried birchbark ends of the torches, the house (fire?).</ta>
            <ta e="T314" id="Seg_10514" s="T309">“You should do like so, I'll come out.”</ta>
            <ta e="T317" id="Seg_10515" s="T315">People were eating.</ta>
            <ta e="T319" id="Seg_10516" s="T318">They are eating.</ta>
            <ta e="T329" id="Seg_10517" s="T320">The Nenets men say so: “Grandfather, why did you put so much firewood into the oven?”</ta>
            <ta e="T334" id="Seg_10518" s="T330">“No, the fire in the oven will eat them.</ta>
            <ta e="T340" id="Seg_10519" s="T335">No, he says, I…”</ta>
            <ta e="T355" id="Seg_10520" s="T341">He is not eating, he is not eating, he is just there, that Rags of the Hare Parka.</ta>
            <ta e="T359" id="Seg_10521" s="T356">It is old, he put this one on.</ta>
            <ta e="T365" id="Seg_10522" s="T360">He is there near the oven, near the door.</ta>
            <ta e="T375" id="Seg_10523" s="T366">Once a fire torch fell into the house from the outside.</ta>
            <ta e="T383" id="Seg_10524" s="T376">He ran to the door, [to] his daughters-in-law.</ta>
            <ta e="T397" id="Seg_10525" s="T384">The Nenets men almost caught that old man, all they grabbed was a piece of his hare parka, they caught [=tried to catch] him in the house.</ta>
            <ta e="T412" id="Seg_10526" s="T398">Probably then with fire through the windows… there were such windows on the both sides of the house.</ta>
            <ta e="T417" id="Seg_10527" s="T413">Probably he had a big house.</ta>
            <ta e="T430" id="Seg_10528" s="T418">His house was covered with soil, he put wooden poles, [sticking them into the ground] a big axe handle deep.</ta>
            <ta e="T923" id="Seg_10529" s="T431">Those [daughters-in-law] were throwing fire into the house, [the fire] probably hit that old man (?).</ta>
            <ta e="T446" id="Seg_10530" s="T923">[NN:] It probably ate (him?).</ta>
            <ta e="T448" id="Seg_10531" s="T924">[NEP:] Wait.</ta>
            <ta e="T464" id="Seg_10532" s="T449">He had thrown gunpowder under the grass, under this new grass.</ta>
            <ta e="T476" id="Seg_10533" s="T465">The fire [gunpowder] blew up, the fire ate the army of those thirty men [= they burned to death in the fire].</ta>
            <ta e="T480" id="Seg_10534" s="T477">[The Nenets men] fell down (forward?).</ta>
            <ta e="T491" id="Seg_10535" s="T481">The old man ran outside to the door, he stood there with an axe in his hands.</ta>
            <ta e="T499" id="Seg_10536" s="T492">Everyone who had hardly made an appearance outdoors, he hit [those] in the face.</ta>
            <ta e="T516" id="Seg_10537" s="T500">His daughters-in-law were throwing that fire, they threw it down the chimney and they threw it through the two windows.</ta>
            <ta e="T523" id="Seg_10538" s="T517">They burned everyone with this fire, this Nenets army.</ta>
            <ta e="T529" id="Seg_10539" s="T524">There were probably two wars.</ta>
            <ta e="T531" id="Seg_10540" s="T530">[KuAI:] Speak.</ta>
            <ta e="T534" id="Seg_10541" s="T926">[NN:] Well, speak.</ta>
            <ta e="T539" id="Seg_10542" s="T925">[NEP:] The old man, being alone, where would he go?</ta>
            <ta e="T551" id="Seg_10543" s="T540">Then the old man (went?) down to the Nenets sleds.</ta>
            <ta e="T927" id="Seg_10544" s="T552">Two more (…) (went?) to the North (…), to the houses of Nenets.</ta>
            <ta e="T562" id="Seg_10545" s="T927">[NN:] Sit down.</ta>
            <ta e="T567" id="Seg_10546" s="T928">[NEP:] To go to those killed people.</ta>
            <ta e="T577" id="Seg_10547" s="T568">He went down (/to the shore), oh, a lame Nenets old man appeared.</ta>
            <ta e="T583" id="Seg_10548" s="T580">Didn't I laugh?</ta>
            <ta e="T589" id="Seg_10549" s="T584">(They left the wife) of the Hare Parka old man.</ta>
            <ta e="T599" id="Seg_10550" s="T590">The Hare Parka old man hit him in the face, his elk(?) hindhead broke(?).</ta>
            <ta e="T603" id="Seg_10551" s="T600">Then he went down (/to the shore).</ta>
            <ta e="T611" id="Seg_10552" s="T604">He went down (/to the shore)_ with his three daughters-in-law they went to the North.</ta>
            <ta e="T618" id="Seg_10553" s="T612">There was the Nenets house in the North.</ta>
            <ta e="T632" id="Seg_10554" s="T619">They went down (/to the shore), they traveled there on the Nenets sleds, on the sleds of these people, these three [daughters-in-law] and the grandfather.</ta>
            <ta e="T636" id="Seg_10555" s="T633">They traveled to the North.</ta>
            <ta e="T645" id="Seg_10556" s="T637">To the North (?) he moved the women.</ta>
            <ta e="T656" id="Seg_10557" s="T646">Those two Selkup boys, they stayed there long ago, they came to their grandfather.</ta>
            <ta e="T664" id="Seg_10558" s="T657">Up [to the forest], they sent two Nenets man home.</ta>
            <ta e="T685" id="Seg_10559" s="T665">The tent… everything… the Hare Parka old man went forward with the reindeers, those orphaned boys were with him as well.</ta>
            <ta e="T693" id="Seg_10560" s="T686">(?).</ta>
            <ta e="T700" id="Seg_10561" s="T694">The Hare Parka old man had two (?) grandsons.</ta>
            <ta e="T706" id="Seg_10562" s="T701">Of these killed men, of the Nenets men.</ta>
            <ta e="T710" id="Seg_10563" s="T707">These two children.</ta>
            <ta e="T714" id="Seg_10564" s="T711">Such two children.</ta>
            <ta e="T722" id="Seg_10565" s="T715">Having gathered the reindeers, they drove them home.</ta>
            <ta e="T725" id="Seg_10566" s="T723">They made a tent.</ta>
            <ta e="T731" id="Seg_10567" s="T930">[NN:] This little one (wants to eat?) so much.</ta>
            <ta e="T739" id="Seg_10568" s="T929">[NEP:] His sons had gone to the forest to hunt squirrels, to hunt wild animals.</ta>
            <ta e="T748" id="Seg_10569" s="T742">They made a tent and spent one night there.</ta>
            <ta e="T750" id="Seg_10570" s="T749">They were going to overnight.</ta>
            <ta e="T757" id="Seg_10571" s="T751">The sons came back home from the hunt.</ta>
            <ta e="T760" id="Seg_10572" s="T758">They came back slinking.</ta>
            <ta e="T763" id="Seg_10573" s="T761">What has happened?</ta>
            <ta e="T770" id="Seg_10574" s="T764">[There are only] rags from the father's tent.</ta>
            <ta e="T777" id="Seg_10575" s="T771">They looked down, it was burned (=the fire ate it).</ta>
            <ta e="T784" id="Seg_10576" s="T777">“The father was killed”, – they think so.</ta>
            <ta e="T788" id="Seg_10577" s="T785">These seven sons.</ta>
            <ta e="T790" id="Seg_10578" s="T789">Wait.</ta>
            <ta e="T793" id="Seg_10579" s="T791">Then…</ta>
            <ta e="T796" id="Seg_10580" s="T794">They came [quickly] (= they brought themselves).</ta>
            <ta e="T803" id="Seg_10581" s="T797">There is a dog outside, the dog of the youngest son.</ta>
            <ta e="T807" id="Seg_10582" s="T804">His wife tied it up outdoors.</ta>
            <ta e="T815" id="Seg_10583" s="T808">They gathered those reindeers, that were outdoors, those Nenets reindeers.</ta>
            <ta e="T821" id="Seg_10584" s="T816">They [the sons] stepped on the dog’s foot, the dog began to yelp.</ta>
            <ta e="T829" id="Seg_10585" s="T822">The old man awakened, the Hare Parka old man.</ta>
            <ta e="T836" id="Seg_10586" s="T830">What has happened(?) to the dog, did the reindeer butt it?</ta>
            <ta e="T843" id="Seg_10587" s="T837">The wife of the youngest son ran out to the street.</ta>
            <ta e="T850" id="Seg_10588" s="T844">This youngest son of the Hare Parka old man.</ta>
            <ta e="T857" id="Seg_10589" s="T851">She ran out: what has happened, is it the husband?</ta>
            <ta e="T862" id="Seg_10590" s="T858">He felt, that there were people there.</ta>
            <ta e="T869" id="Seg_10591" s="T863">The father awakened in the house. </ta>
            <ta e="T879" id="Seg_10592" s="T870">We burned down our house [ourselves].</ta>
            <ta e="T885" id="Seg_10593" s="T880">The grandfather (taught)? us so.</ta>
            <ta e="T890" id="Seg_10594" s="T886">[NN:] From the beginning?</ta>
            <ta e="T892" id="Seg_10595" s="T890">[KuAI:] No, speak.</ta>
            <ta e="T896" id="Seg_10596" s="T931">[NN:] Well, speak further.</ta>
            <ta e="T910" id="Seg_10597" s="T932">[NEP:] Then they lived there, and again they left, the Selkup people.</ta>
            <ta e="T916" id="Seg_10598" s="T911">They drove the reindeers from Nenets lands.</ta>
            <ta e="T920" id="Seg_10599" s="T917">This Selkup…</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_10600" s="T1">[KuAI:] Ein Märchen, geschrieben von Elizaveta Pavlona.</ta>
            <ta e="T9" id="Seg_10601" s="T6">Nun, sprich, bitte.</ta>
            <ta e="T15" id="Seg_10602" s="T921">[NN:] Nun, sag etwas anderes.</ta>
            <ta e="T23" id="Seg_10603" s="T922">[NEP:] Also, ich erzähle eine Geschichte über den alten Hasenmantelmann.</ta>
            <ta e="T32" id="Seg_10604" s="T24">Dieser alte Hasenmantelmann, vor langer Zeit hatte er sieben Söhne.</ta>
            <ta e="T36" id="Seg_10605" s="T33">Und sieben Schwiegertöchter.</ta>
            <ta e="T39" id="Seg_10606" s="T37">Sie kamen am Morgen.</ta>
            <ta e="T46" id="Seg_10607" s="T40">Die Nenzen aus dem Norden kamen flussaufwärts um ihn zu töten.</ta>
            <ta e="T52" id="Seg_10608" s="T47">Seine Leute, seine Söhne gingen in den Wald Eichhörnchen jagen.</ta>
            <ta e="T64" id="Seg_10609" s="T53">Jetzt gingen sie in den Wald Eichhörnchen jagen, jetzt im Winter, wie es gewöhnlich im Winter ist.</ta>
            <ta e="T72" id="Seg_10610" s="T65">Dieser Bursche, dieser Nenze zog einen selkupischen Burschen auf.</ta>
            <ta e="T75" id="Seg_10611" s="T73">Aus dem Krieg.</ta>
            <ta e="T90" id="Seg_10612" s="T76">Der alte Hasenmantelmann, morgen, sagt er, werden wir zu seinem Haus gehen, du solltest dorthin gehen, sagt er.</ta>
            <ta e="T100" id="Seg_10613" s="T91">Zwei Burschen, als sie nach Hause kommen, sagen sie so, zwei Waisenkinder, die die Nenzen aufgezogen hatten.</ta>
            <ta e="T104" id="Seg_10614" s="T101">Diese beiden selkupischen Burschen…</ta>
            <ta e="T112" id="Seg_10615" s="T105">Sei leise, sei leise, als ob jemand einen Hund geschlagen hätte.</ta>
            <ta e="T114" id="Seg_10616" s="T113">Warte.</ta>
            <ta e="T120" id="Seg_10617" s="T115">Dann der alte Mann, wohin kann er gehen?</ta>
            <ta e="T126" id="Seg_10618" s="T121">Der alte Mann hat vielleicht gelogen.</ta>
            <ta e="T135" id="Seg_10619" s="T127">Der alte Mann wurde so, dieser alte Hasenmantelmann.</ta>
            <ta e="T143" id="Seg_10620" s="T136">Er wurde [=sagte] so zu seinen Schwiegertöchtern: "Bringt etwas Essen herunter [=aus dem hinteren Teil des Zelts].</ta>
            <ta e="T154" id="Seg_10621" s="T144">Bringt etwas Fett, Essen, irgendwelches (verstecktes?) Essen, bringt irgendwas.</ta>
            <ta e="T159" id="Seg_10622" s="T155">Die Nenzen werden morgen kommen, sie werden es essen."</ta>
            <ta e="T163" id="Seg_10623" s="T160">Der alte Mann sagte so.</ta>
            <ta e="T166" id="Seg_10624" s="T164">Sie brachten Essen.</ta>
            <ta e="T177" id="Seg_10625" s="T167">Wie mit dem Gras, sie legten das Innere des Hauses mit neuem Gras aus.</ta>
            <ta e="T181" id="Seg_10626" s="T178">Der alte Mann beginnt den Nenzen Essen zu geben.</ta>
            <ta e="T191" id="Seg_10627" s="T182">Nun, am nächsten Tag, eine Armee von dreißig Nenzen.</ta>
            <ta e="T202" id="Seg_10628" s="T192">Vor langer Zeit blieb der alte Hasenmantelmann allein im Haus mit den Schwiegertöchtern.</ta>
            <ta e="T206" id="Seg_10629" s="T203">Er legte sieben [Sorten] Gras</ta>
            <ta e="T210" id="Seg_10630" s="T207">Der alte Mann wurde so.</ta>
            <ta e="T218" id="Seg_10631" s="T211">Sie sammelten sich im Haus, diese Nenzen.</ta>
            <ta e="T220" id="Seg_10632" s="T219">Sie begannen zu essen.</ta>
            <ta e="T223" id="Seg_10633" s="T221">Sie fanden Essen.</ta>
            <ta e="T240" id="Seg_10634" s="T224">Seine Schwiegertöchter kochten Tee, dort unten war irgendwelches Essen, Fett, irgendwelches getrocknetes Essen.</ta>
            <ta e="T251" id="Seg_10635" s="T241">Der alte Mann sagte so zu seiner Frau, der alte Hasenmantelmann hatte auch eine Frau.</ta>
            <ta e="T253" id="Seg_10636" s="T252">Warte.</ta>
            <ta e="T260" id="Seg_10637" s="T254">Er schickte seine Schwiegertöchter so: "Ihr, geht nach draußen.</ta>
            <ta e="T265" id="Seg_10638" s="T261">Bringt (auch?) eure Kinder nach draußen."</ta>
            <ta e="T271" id="Seg_10639" s="T266">Der alte Mann blieb lange, versorgte die Leute mit Essen.</ta>
            <ta e="T279" id="Seg_10640" s="T272">Einmal aßen die Leute, die (herunter?) gekommen waren.</ta>
            <ta e="T288" id="Seg_10641" s="T280">Er (legte?) viel Feuerholz in den Ofen und zündete es an.</ta>
            <ta e="T296" id="Seg_10642" s="T288">Seine Schwiegertöchter schickte er nicht ohne Grund nach draußen, sie werden draußen ein Feuer legen.</ta>
            <ta e="T308" id="Seg_10643" s="T297">Sie legten ein Feuer, die vorher getrockneten Enden der Birkenrindenfackeln, das Haus (Feuer?).</ta>
            <ta e="T314" id="Seg_10644" s="T309">"Ihr solltet es so machen, ich komme heraus."</ta>
            <ta e="T317" id="Seg_10645" s="T315">Die Leute aßen.</ta>
            <ta e="T319" id="Seg_10646" s="T318">Sie essen.</ta>
            <ta e="T329" id="Seg_10647" s="T320">Die Nenzen sagen so: "Großvater, warum hast du so viel Feuerholz in deinen Ofen gelegt?"</ta>
            <ta e="T334" id="Seg_10648" s="T330">"Nein, das Feuer im Ofen wird es verbrennen.</ta>
            <ta e="T340" id="Seg_10649" s="T335">Nein, sagt er, ich…"</ta>
            <ta e="T355" id="Seg_10650" s="T341">Er isst nicht, er isst nicht, er sitzt nur da, diese Fetzen des Hasenmantel.</ta>
            <ta e="T359" id="Seg_10651" s="T356">Er ist alt, er zieht diesen an.</ta>
            <ta e="T365" id="Seg_10652" s="T360">Er ist dort neben dem Ofen, nahe der Tür.</ta>
            <ta e="T375" id="Seg_10653" s="T366">Auf einmal fiel eine Fackel von draußen ins Haus.</ta>
            <ta e="T383" id="Seg_10654" s="T376">Er rannte zur Tür, [zu] seinen Schwiegertöchtern.</ta>
            <ta e="T397" id="Seg_10655" s="T384">Die Nenzen fingen den alten Mann fast, sie griffen nur ein Stück seines Hasenmantels, sie fingen [=versuchten zu fangen] ihn im Haus.</ta>
            <ta e="T412" id="Seg_10656" s="T398">Vielleicht dann mit Feuer durch die Fenster… da waren solche Fenster auf beiden Seiten des Hauses.</ta>
            <ta e="T417" id="Seg_10657" s="T413">Vielleicht war sein Haus groß.</ta>
            <ta e="T430" id="Seg_10658" s="T418">Sein Haus war bedeckt mit Erde, er stellte hölzerne Stangen [steckte sie in den Boden] den Griff einer großen Axt tief.</ta>
            <ta e="T923" id="Seg_10659" s="T431">Diese [Schwiegertöchter] warfen Feuer ins Haus, [das Feuer] traf vermutlich den alten Mann.</ta>
            <ta e="T446" id="Seg_10660" s="T923">[NN:] Es verzehrte ihn vielleicht.</ta>
            <ta e="T448" id="Seg_10661" s="T924">[NEP:] Warte.</ta>
            <ta e="T464" id="Seg_10662" s="T449">Er hatte Schießpulver unter dem Gras verteilt, unter diesem neuen Gras.</ta>
            <ta e="T476" id="Seg_10663" s="T465">Das Feuer [Schießpulver] explodierte, das Feuer verzehrte die ganze Armee von diesen dreißig Mann [= sie verbrannten im Feuer].</ta>
            <ta e="T480" id="Seg_10664" s="T477">[Die Nenzen] fielen herunter (vorwärts?).</ta>
            <ta e="T491" id="Seg_10665" s="T481">Der alte Mann rannte nach draußen zur Tür, er stand dort mit einer Axt in Händen.</ta>
            <ta e="T499" id="Seg_10666" s="T492">Jedem, der kaum draußen erschien, schlug er ins Gesicht.</ta>
            <ta e="T516" id="Seg_10667" s="T500">Seine Schwiegertöchter warfen dieses Feuer, sie warfen es durch den Kamin und sie warfen es durch die beiden Fenster.</ta>
            <ta e="T523" id="Seg_10668" s="T517">Sie verbrannten alle mit diesem Feuer, die nenzische Armee.</ta>
            <ta e="T529" id="Seg_10669" s="T524">Es waren vielleicht zwei Kriege.</ta>
            <ta e="T531" id="Seg_10670" s="T530">[KuAI:] Sprich.</ta>
            <ta e="T534" id="Seg_10671" s="T926">[NN:] Nun, sprich.</ta>
            <ta e="T539" id="Seg_10672" s="T925">[NEP:] Der einsame alte Mann, wohin soll er gehen?</ta>
            <ta e="T551" id="Seg_10673" s="T540">Der alte Mann (ging?) hinunter zu den nenzischen Schlitten.</ta>
            <ta e="T927" id="Seg_10674" s="T552">Noch zwei (…) (gingen?) nach Norden (…) zu den Häusern der Nenzen.</ta>
            <ta e="T562" id="Seg_10675" s="T927">[NN:] Setz dich.</ta>
            <ta e="T567" id="Seg_10676" s="T928">[NEP:] Um zu diesen getöteten Menschen zu gehen.</ta>
            <ta e="T577" id="Seg_10677" s="T568">Er ging hinunter (/zum Ufer), oh, ein lahmer alter Nenze erschien.</ta>
            <ta e="T583" id="Seg_10678" s="T580">Habe ich nicht gelacht?</ta>
            <ta e="T589" id="Seg_10679" s="T584">(Sie ließen die Frau) des alten Hasenmantelmann (zurück).</ta>
            <ta e="T599" id="Seg_10680" s="T590">Der alte Hasenmantelmann schlug ihm ins Gesicht, sein Elch(?) Hinterkopf brach.</ta>
            <ta e="T603" id="Seg_10681" s="T600">Dann ging er hinunter (/zum Ufer).</ta>
            <ta e="T611" id="Seg_10682" s="T604">Er ging hinunter (/zum Ufer), mit seinen drei Schwiegertöchtern gingen sie nordwärts.</ta>
            <ta e="T618" id="Seg_10683" s="T612">Dort war das Nenzenhaus im Norden.</ta>
            <ta e="T632" id="Seg_10684" s="T619">Sie gingen hinunter (/zum Ufer), sie reisten dorthin auf den nenzischen Schlitten, auf den Schlitten dieser Leute, diese drei [Schwiegertöchter] und der Großvater.</ta>
            <ta e="T636" id="Seg_10685" s="T633">Sie reisten nordwärts.</ta>
            <ta e="T645" id="Seg_10686" s="T637">Nach Norden brachte er die Frauen.</ta>
            <ta e="T656" id="Seg_10687" s="T646">Diese beiden selkupischen Jungen, sie blieben dort lange, sie kamen zu ihrem Großvater.</ta>
            <ta e="T664" id="Seg_10688" s="T657">Nach oben [in den Wald] schickten sie zwei Nenzen nach Hause.</ta>
            <ta e="T685" id="Seg_10689" s="T665">Das Zelt … alles … der alte Hasenmantelmann ging mit den Rentieren voraus, diese Waisenjungen waren auch bei ihm.</ta>
            <ta e="T693" id="Seg_10690" s="T686">(?).</ta>
            <ta e="T700" id="Seg_10691" s="T694">Der alte Hasenmantelmann hatte zwei Enkel.</ta>
            <ta e="T706" id="Seg_10692" s="T701">Von diesen getöteten Männern, von den Nenzen.</ta>
            <ta e="T710" id="Seg_10693" s="T707">Diese beiden Kinder.</ta>
            <ta e="T714" id="Seg_10694" s="T711">Solche zwei Kinder.</ta>
            <ta e="T722" id="Seg_10695" s="T715">Als sie die Rentiere zusammengetrieben hatten, brachten sie sie nach Hause.</ta>
            <ta e="T725" id="Seg_10696" s="T723">Sie bauten ein Zelt auf.</ta>
            <ta e="T731" id="Seg_10697" s="T930">[NN:] Der Kleine (will) so viel (essen).</ta>
            <ta e="T739" id="Seg_10698" s="T929">[NEP:] Seine Söhne waren in den Wald gegangen um Eichhörnchen zu jagen, um wilde Tiere zu jagen.</ta>
            <ta e="T748" id="Seg_10699" s="T742">Sie bauten ein Zelt auf und verbrachten die Nacht dort.</ta>
            <ta e="T750" id="Seg_10700" s="T749">Sie würden übernachten.</ta>
            <ta e="T757" id="Seg_10701" s="T751">Die Söhne kamen von der Jagd nach Hause.</ta>
            <ta e="T760" id="Seg_10702" s="T758">Sie kamen schleichend.</ta>
            <ta e="T763" id="Seg_10703" s="T761">Was ist passiert?</ta>
            <ta e="T770" id="Seg_10704" s="T764">[Es gab nur noch] Fetzen von Vaters Zelt.</ta>
            <ta e="T777" id="Seg_10705" s="T771">Sie sahen hinunter, es war verbrannt (=das Feuer aß es).</ta>
            <ta e="T784" id="Seg_10706" s="T777">"Vater wurde getötet", -- dachten sie so.</ta>
            <ta e="T788" id="Seg_10707" s="T785">Diese sieben Söhne.</ta>
            <ta e="T790" id="Seg_10708" s="T789">Warte.</ta>
            <ta e="T793" id="Seg_10709" s="T791">Dann…</ta>
            <ta e="T796" id="Seg_10710" s="T794">Sie kamen [schnell] (=sie brachten sich selbst).</ta>
            <ta e="T803" id="Seg_10711" s="T797">Draußen ist ein Hund, der Hund des jüngsten Sohns.</ta>
            <ta e="T807" id="Seg_10712" s="T804">Seine Frau band ihn draußen fest.</ta>
            <ta e="T815" id="Seg_10713" s="T808">Sie trieben die Rentiere zusammen, die draußen waren, diese Rentiere der Nenzen.</ta>
            <ta e="T821" id="Seg_10714" s="T816">Sie [die Söhne] traten auf den Fuß des Hundes, der Hund begann zu winseln.</ta>
            <ta e="T829" id="Seg_10715" s="T822">Der alte Mann wachte auf, der alte Hasenmantelmann.</ta>
            <ta e="T836" id="Seg_10716" s="T830">Was ist dem Hund passiert(?), hat ihn ein Rentier gestoßen?</ta>
            <ta e="T843" id="Seg_10717" s="T837">Die Frau des jüngsten Sohn rannte nach draußen.</ta>
            <ta e="T850" id="Seg_10718" s="T844">Dieser jüngste Sohn des alten Hasenmantelmann.</ta>
            <ta e="T857" id="Seg_10719" s="T851">Sie rannte nach draußen: was ist passiert, ist es der Ehemann?</ta>
            <ta e="T862" id="Seg_10720" s="T858">Er fühlte, dass dort Menschen waren.</ta>
            <ta e="T869" id="Seg_10721" s="T863">Der Vater wachte im Haus auf.</ta>
            <ta e="T879" id="Seg_10722" s="T870">Wir haben das Haus [selbst] mit Feuer niedergebrannt.</ta>
            <ta e="T885" id="Seg_10723" s="T880">Großvater (sagte?) es uns so.</ta>
            <ta e="T890" id="Seg_10724" s="T886">[NN:] Vom Anfang?</ta>
            <ta e="T892" id="Seg_10725" s="T890">[KuAI:] Nein, sprich.</ta>
            <ta e="T896" id="Seg_10726" s="T931">[NN:] Nun, sprich weiter.</ta>
            <ta e="T910" id="Seg_10727" s="T932">[NEP:] Dann lebten sie dort und gingen wieder, die Selkupen.</ta>
            <ta e="T916" id="Seg_10728" s="T911">Sie trieben die Rentiere vom nenzischen Land.</ta>
            <ta e="T920" id="Seg_10729" s="T917">Diese Selkupen…</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_10730" s="T1">[KuAI:] Сказка, записанная Елизаветой Павловной.</ta>
            <ta e="T9" id="Seg_10731" s="T6">Ну пожалуйста, говорите.</ta>
            <ta e="T15" id="Seg_10732" s="T921">[NN:] Ну, говори что другое.</ta>
            <ta e="T23" id="Seg_10733" s="T922">[NEP:] Это, про старика Заячью шубу хоть расскажу. </ta>
            <ta e="T32" id="Seg_10734" s="T24">Этот старик Заячья шуба, в старину семь сыновей у него было.</ta>
            <ta e="T36" id="Seg_10735" s="T33">И семь невесток.</ta>
            <ta e="T39" id="Seg_10736" s="T37">Утром они пришли.</ta>
            <ta e="T46" id="Seg_10737" s="T40">Ненцы с северной стороны в верховье реки пришли его убивать.</ta>
            <ta e="T52" id="Seg_10738" s="T47">Его люди, его сыновья в лес пошли белковать.</ta>
            <ta e="T64" id="Seg_10739" s="T53">Белковать в лес ушли теперь люди, теперь зимой, теперь как бывает зимой.</ta>
            <ta e="T72" id="Seg_10740" s="T65">Этого мальчика, селькупского мальчика, воспитывал этот ненец. </ta>
            <ta e="T75" id="Seg_10741" s="T73">С той войны.</ta>
            <ta e="T90" id="Seg_10742" s="T76">Старик заячья шуба, мол, завтра, мол, мы придём в его дом, вы, мол, сходите туда.</ta>
            <ta e="T100" id="Seg_10743" s="T91">Двое детей, домой отправившись, так говорят, двоей детей-сирот, которых ненец держал. </ta>
            <ta e="T104" id="Seg_10744" s="T101">Эти двое селькупских детей…</ta>
            <ta e="T112" id="Seg_10745" s="T105">Спокойно, спокойно, собаку что ли кто (взгрел?).</ta>
            <ta e="T114" id="Seg_10746" s="T113">Подожди.</ta>
            <ta e="T120" id="Seg_10747" s="T115">Потом старик куда денется?</ta>
            <ta e="T126" id="Seg_10748" s="T121">Старик, вроде, кое-как лежит.</ta>
            <ta e="T135" id="Seg_10749" s="T127">Старик так стал [себя вести], этот старик Заячья шуба.</ta>
            <ta e="T143" id="Seg_10750" s="T136">Так стал [= сказал] невесткам: “Вниз [= с задней стороны чума] еду принесите.</ta>
            <ta e="T154" id="Seg_10751" s="T144">Жир, еду принесите, всякую (припрятанную?) еду, (всякую разную?) принесите.</ta>
            <ta e="T159" id="Seg_10752" s="T155">Ненцы завтра придут, поедят”.</ta>
            <ta e="T163" id="Seg_10753" s="T160">Дед так сказал.</ta>
            <ta e="T166" id="Seg_10754" s="T164">Еду достали.</ta>
            <ta e="T177" id="Seg_10755" s="T167">Вот так этой травой, эту внутренность дома новой травой они застелили.</ta>
            <ta e="T181" id="Seg_10756" s="T178">Старик ненцев кормить собирается.</ta>
            <ta e="T191" id="Seg_10757" s="T182">Ну, на следующий день ненцев (столько?), тридцать людей войско.</ta>
            <ta e="T202" id="Seg_10758" s="T192">Так было, вроде в пустом доме старик Заячья шуба один остался с невестками.</ta>
            <ta e="T206" id="Seg_10759" s="T203">Семью [разными] травами постелил.</ta>
            <ta e="T210" id="Seg_10760" s="T207">Дед так стал [вести себя].</ta>
            <ta e="T218" id="Seg_10761" s="T211">Домой впихнулись они вроде, эти ненцы.</ta>
            <ta e="T220" id="Seg_10762" s="T219">Кушать начали [ненцы].</ta>
            <ta e="T223" id="Seg_10763" s="T221">Еду нашли.</ta>
            <ta e="T240" id="Seg_10764" s="T224">Невестки чая наварили, внизу всякая еда, жир, жир всякий и сухая еда.</ta>
            <ta e="T251" id="Seg_10765" s="T241">Дед так говорит жене, у старика Заячьей шубы жена тоже была.</ta>
            <ta e="T253" id="Seg_10766" s="T252">Подожди.</ta>
            <ta e="T260" id="Seg_10767" s="T254">Невесток так отправляет: “Вы на улицу идите.</ta>
            <ta e="T265" id="Seg_10768" s="T261">Детей (тоже?) на улицу вынеси”.</ta>
            <ta e="T271" id="Seg_10769" s="T266">Дед долго [там] был, людей кормя.</ta>
            <ta e="T279" id="Seg_10770" s="T272">В одно время пришедшие (вниз?) люди кушают.</ta>
            <ta e="T288" id="Seg_10771" s="T280">Внизу в печи огонь так дровами (набросал?), разжег. </ta>
            <ta e="T296" id="Seg_10772" s="T288">Невесток не просто на улицу отправил, на улице огонь разожгут.</ta>
            <ta e="T308" id="Seg_10773" s="T297">Они огонь разожгли, берестяных факелов головки так когда-то высушенные, домашний (огонь?). </ta>
            <ta e="T314" id="Seg_10774" s="T309">“Вы это [сделайте], когда я выйду”.</ta>
            <ta e="T317" id="Seg_10775" s="T315">Люди ели.</ta>
            <ta e="T319" id="Seg_10776" s="T318">Едят.</ta>
            <ta e="T329" id="Seg_10777" s="T320">Ненцы так говорят: “Дед, печь зачем так дровами заполнил?”</ta>
            <ta e="T334" id="Seg_10778" s="T330">“Нет, в печи сгорит [всё] (букв. печь огонь съест). </ta>
            <ta e="T340" id="Seg_10779" s="T335">Нет, мол, я, мол, это …”</ta>
            <ta e="T355" id="Seg_10780" s="T341">Он не ест, он не ест, так [там] находится, такая заячья шуба рваная. </ta>
            <ta e="T359" id="Seg_10781" s="T356">Старая, эту одел.</ta>
            <ta e="T365" id="Seg_10782" s="T360">Возле печки находится, возле двери.</ta>
            <ta e="T375" id="Seg_10783" s="T366">В одно время с улицы в дом горящий факел упал.</ta>
            <ta e="T383" id="Seg_10784" s="T376">Так побежал к двери, его невестки.</ta>
            <ta e="T397" id="Seg_10785" s="T384">Ненцы только схватили такого старика, только кусок заячей шубы оторвав, схватили, дома.</ta>
            <ta e="T412" id="Seg_10786" s="T398">Вроди этим огнём потом (?) в проем окошка, в дом, (такие?) окошки с обеих сторон [дома] были.</ta>
            <ta e="T417" id="Seg_10787" s="T413">Большой что ли дом у него был.</ta>
            <ta e="T430" id="Seg_10788" s="T418">Дом его землей покрыт, вот этот дом с деревянными подпорками, высотой с ручку топора покрыт [землей]. </ta>
            <ta e="T923" id="Seg_10789" s="T431">В доме огонь забросали, [огонь] ударил того старика.</ta>
            <ta e="T446" id="Seg_10790" s="T923">[NN:] Вроде это съел. </ta>
            <ta e="T448" id="Seg_10791" s="T924">[NEP:] Подожди.</ta>
            <ta e="T464" id="Seg_10792" s="T449">Порох он рассыпал под траву, снизу вот такой новой травы.</ta>
            <ta e="T476" id="Seg_10793" s="T465">Этот огонь ударил, это войско из тридцати человек все огонь съел.</ta>
            <ta e="T480" id="Seg_10794" s="T477">На бок упали впереди [ненцы].</ta>
            <ta e="T491" id="Seg_10795" s="T481">Вперёд на улицу старик побежал к проёму двери, с топором в руках находится..</ta>
            <ta e="T499" id="Seg_10796" s="T492">Каждого, только на улицу они высунутся, опять в лицо ударяет.</ta>
            <ta e="T516" id="Seg_10797" s="T500">Невестки его этот огонь забрасывали, через верх печки забрасывают и в отверстия двух окон забрасывают.</ta>
            <ta e="T523" id="Seg_10798" s="T517">Всех огнём сжёг, это ненецкое войско.</ta>
            <ta e="T529" id="Seg_10799" s="T524">Этих войн вроде две было.</ta>
            <ta e="T531" id="Seg_10800" s="T530">[KuAI:] Говорите.</ta>
            <ta e="T534" id="Seg_10801" s="T926">[NN:] Ну, говори.</ta>
            <ta e="T539" id="Seg_10802" s="T925">[NEP:] Один старик куда денется?</ta>
            <ta e="T551" id="Seg_10803" s="T540">Старик потом, ну, старик Заячья шуба вниз к ненцам, к нартам (пошёл?).</ta>
            <ta e="T927" id="Seg_10804" s="T552">Ещё двое (…) на север (…) в ненецкий дом (поехать?).</ta>
            <ta e="T562" id="Seg_10805" s="T927">[NN:] Садись.</ta>
            <ta e="T567" id="Seg_10806" s="T928">[NEP:] К эти убитым людям поехать.</ta>
            <ta e="T577" id="Seg_10807" s="T568">Вниз пошёл, о, внизу вверх хромой ненец-старик появился.</ta>
            <ta e="T583" id="Seg_10808" s="T580">Я засмеялась.</ta>
            <ta e="T589" id="Seg_10809" s="T584">Старика Заячьей шубы (жену оставили?).</ta>
            <ta e="T599" id="Seg_10810" s="T590">Старик заячья шуба в лицо ударил, [у того] лосиный затылок назад (сломался?). </ta>
            <ta e="T603" id="Seg_10811" s="T600">Потом вниз пошёл.</ta>
            <ta e="T611" id="Seg_10812" s="T604">Вниз пошёл, потом с тремя невестками на север уехали.</ta>
            <ta e="T618" id="Seg_10813" s="T612">На севере ненецкий дом на севере был там.</ta>
            <ta e="T632" id="Seg_10814" s="T619">Вниз пошли там, туда двинулись на ненецких нартах, на этих людей нартах, эти три [невестки] с дедом.</ta>
            <ta e="T636" id="Seg_10815" s="T633">На север двинувшись, поехали.</ta>
            <ta e="T645" id="Seg_10816" s="T637">На севере ненецких (?) женщин (погонял?).</ta>
            <ta e="T656" id="Seg_10817" s="T646">Эти двое селькупских мальчиков [сирот] там эти остались [тогда] давно, к этому своему деду пришли.</ta>
            <ta e="T664" id="Seg_10818" s="T657">Вверх, (ну это?), двух ненцев домой отправили. </ta>
            <ta e="T685" id="Seg_10819" s="T665">Чум, (лохмотья [нюка] чума?), всё что есть, старик Заячья шуба оленей по снегу вперёд погнал, эти сироты-мальчики тоже с ним были. </ta>
            <ta e="T693" id="Seg_10820" s="T686">Тогда давно (недалеко?) от них ненцы, ну это.</ta>
            <ta e="T700" id="Seg_10821" s="T694">У старика Заячьей шубы (недалеко?/в родстве?) два внука были. </ta>
            <ta e="T706" id="Seg_10822" s="T701">Этих убитых людей, этих ненцев.</ta>
            <ta e="T710" id="Seg_10823" s="T707">Этих двое детей.</ta>
            <ta e="T714" id="Seg_10824" s="T711">Такие два ребёнка.</ta>
            <ta e="T722" id="Seg_10825" s="T715">Оленей согнав, домой привели в тот дом. </ta>
            <ta e="T725" id="Seg_10826" s="T723">Чум поставили.</ta>
            <ta e="T731" id="Seg_10827" s="T930">[NN:] Этот маленький так (хочет кушать?).</ta>
            <ta e="T739" id="Seg_10828" s="T929">[NEP:] Сыновья его до сих пор [были] ушедшие в лес белковать, охотиться. </ta>
            <ta e="T748" id="Seg_10829" s="T742">Вверх чум поставили, подожди, одну ночь переночевали. </ta>
            <ta e="T750" id="Seg_10830" s="T749">Собираются переночевать.</ta>
            <ta e="T757" id="Seg_10831" s="T751">Сыновья из лесу вернулись (=себя привели) с белкования домой.</ta>
            <ta e="T760" id="Seg_10832" s="T758">Крадясь вернулись (=привели (себя)).</ta>
            <ta e="T763" id="Seg_10833" s="T761">Что случилось?</ta>
            <ta e="T770" id="Seg_10834" s="T764">Отца чума лохмотья [нюка].</ta>
            <ta e="T777" id="Seg_10835" s="T771">Вниз только посмотрели, полностью огонь съел.</ta>
            <ta e="T784" id="Seg_10836" s="T777">“Отца, наверное, убили они”, – так думают.</ta>
            <ta e="T788" id="Seg_10837" s="T785">Эти семь его сыновей.</ta>
            <ta e="T790" id="Seg_10838" s="T789">Подожди.</ta>
            <ta e="T793" id="Seg_10839" s="T791">Потом…</ta>
            <ta e="T796" id="Seg_10840" s="T794">[Быстро] пришли (=привели себя).</ta>
            <ta e="T803" id="Seg_10841" s="T797">На улице собака, младшего сына собака.</ta>
            <ta e="T807" id="Seg_10842" s="T804">Жена его на улице привязала.</ta>
            <ta e="T815" id="Seg_10843" s="T808">Оленей, которые на улице, этих ненецких оленей погнали. </ta>
            <ta e="T821" id="Seg_10844" s="T816">На ногу собаки наступили, собака завизжала.</ta>
            <ta e="T829" id="Seg_10845" s="T822">Старик такой очнулся, старик Заячья шуба, мол.</ta>
            <ta e="T836" id="Seg_10846" s="T830">С собакой что (случилось?) , что, олень боднул (= рубанул)? </ta>
            <ta e="T843" id="Seg_10847" s="T837">Младшего сына жена на улицу выскочила.</ta>
            <ta e="T850" id="Seg_10848" s="T844">Этот младший сын старика Заячья шуба.</ta>
            <ta e="T857" id="Seg_10849" s="T851">На улицу только выскочила: что такое, муж?</ta>
            <ta e="T862" id="Seg_10850" s="T858">Это люди [там], догадался.</ta>
            <ta e="T869" id="Seg_10851" s="T863">Отец их очнулся в доме.</ta>
            <ta e="T879" id="Seg_10852" s="T870">Мол, мы, мол, дом, мол, мы огнём сожгли, мол.</ta>
            <ta e="T885" id="Seg_10853" s="T880">Дед, мол, так нас (научил?).</ta>
            <ta e="T890" id="Seg_10854" s="T886">[NN:] Сначала что ли?</ta>
            <ta e="T892" id="Seg_10855" s="T890">[KuAI:] Говорите, нет.</ta>
            <ta e="T896" id="Seg_10856" s="T931">[NN:] Давай, дальше говори.</ta>
            <ta e="T910" id="Seg_10857" s="T932">[NEP:] Ну потом жили они там и опять вроде прочь уехали, эти селькупы вот.</ta>
            <ta e="T916" id="Seg_10858" s="T911">Вроде с ненецкой земли оленей согнали.</ta>
            <ta e="T920" id="Seg_10859" s="T917">Этот селькуп…</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_10860" s="T1">Сказка, записанная Елизаветой Павловной.</ta>
            <ta e="T9" id="Seg_10861" s="T6">Ну пожалуйста, говорите.</ta>
            <ta e="T15" id="Seg_10862" s="T921">Давай говори.</ta>
            <ta e="T23" id="Seg_10863" s="T922">Это про старика заячью шубу хоть расскажу. </ta>
            <ta e="T32" id="Seg_10864" s="T24">Старик заячья шуба, в старину семь детей было.</ta>
            <ta e="T36" id="Seg_10865" s="T33">И семь невесток.</ta>
            <ta e="T39" id="Seg_10866" s="T37">Утром пришли.</ta>
            <ta e="T46" id="Seg_10867" s="T40">Ненцы с северной стороны придут его убивать.</ta>
            <ta e="T52" id="Seg_10868" s="T47">Люди, сыновья в лес пошли белковать.</ta>
            <ta e="T64" id="Seg_10869" s="T53">Белковать в лес ушли люди, зимой, как бывает зимой.</ta>
            <ta e="T72" id="Seg_10870" s="T65">Этого мальчика селькупского мальчика воспитывал ненец. </ta>
            <ta e="T75" id="Seg_10871" s="T73">С войны.</ta>
            <ta e="T90" id="Seg_10872" s="T76">Старик заячья шуба, завтра мы придём в его дом, вы сходите туда.</ta>
            <ta e="T100" id="Seg_10873" s="T91">Дети [двое] домой пошли, так говорят, сироты-дети, ненец которых держал. </ta>
            <ta e="T104" id="Seg_10874" s="T101">Эти селькупские дети [двое]… </ta>
            <ta e="T112" id="Seg_10875" s="T105">Спокойно, спокойно, собаку кто взгрел.</ta>
            <ta e="T114" id="Seg_10876" s="T113">Подожди.</ta>
            <ta e="T120" id="Seg_10877" s="T115">Потом дед куда денется?</ta>
            <ta e="T126" id="Seg_10878" s="T121">Дед кое-как лежит.</ta>
            <ta e="T135" id="Seg_10879" s="T127">Дед так стал [себя вести], этот дед заячья шуба.</ta>
            <ta e="T143" id="Seg_10880" s="T136">Так стал [себя вести] с задней стороны чума еду вытащил [вниз].</ta>
            <ta e="T154" id="Seg_10881" s="T144">Дед еду достал, всякую припрятанную еду, всякую разную достал.</ta>
            <ta e="T159" id="Seg_10882" s="T155">Ненцы завтра придут, поедят.</ta>
            <ta e="T163" id="Seg_10883" s="T160">Дед так сказал.</ta>
            <ta e="T166" id="Seg_10884" s="T164">Еду достали.</ta>
            <ta e="T177" id="Seg_10885" s="T167">Вот так этой травой эту внутренность дома новой травой постелили.</ta>
            <ta e="T181" id="Seg_10886" s="T178">Дед кормить собирается / будет.</ta>
            <ta e="T191" id="Seg_10887" s="T182">Эээ, на следующий день ненцев столько, тридцать людей войнов.</ta>
            <ta e="T202" id="Seg_10888" s="T192">Так было, в пустом доме старик заячья шуба один остался с невестками.</ta>
            <ta e="T206" id="Seg_10889" s="T203">Семью [разными] травами постелил.</ta>
            <ta e="T210" id="Seg_10890" s="T207">Дед так стал [вести себя].</ta>
            <ta e="T218" id="Seg_10891" s="T211">Домой впихнулись [они]. Эти ненцы кушать начали.</ta>
            <ta e="T220" id="Seg_10892" s="T219">Эти ненцы кушать начали.</ta>
            <ta e="T223" id="Seg_10893" s="T221">Еду нашли.</ta>
            <ta e="T240" id="Seg_10894" s="T224">Невестки чая наварили, внизу всякая еда, жир, жир всякий и сухая еда.</ta>
            <ta e="T251" id="Seg_10895" s="T241">Дед так говорит жене, у старика заячьей шубы жена тоже была.</ta>
            <ta e="T253" id="Seg_10896" s="T252">Подожди.</ta>
            <ta e="T260" id="Seg_10897" s="T254">Невесток так отправляет: Вы на улицу идите.</ta>
            <ta e="T265" id="Seg_10898" s="T261">Детей тоже на улицу вынесите.</ta>
            <ta e="T271" id="Seg_10899" s="T266">Дед долго там был, людей кормя.</ta>
            <ta e="T279" id="Seg_10900" s="T272">В одно время пришедшие [вниз] люди кушают.</ta>
            <ta e="T288" id="Seg_10901" s="T280">Внизу в печи огонь так дровами набросали, разожгли. </ta>
            <ta e="T296" id="Seg_10902" s="T288">Невесток не просто на улицу отправил, на улице огонь разожгут.. </ta>
            <ta e="T308" id="Seg_10903" s="T297">Огонь разожгли, берестяных факелов головки так когда-то высушенные, домашний огонь. </ta>
            <ta e="T314" id="Seg_10904" s="T309">Вы это [сделайте], когда я выйду.</ta>
            <ta e="T317" id="Seg_10905" s="T315">Люди едят.</ta>
            <ta e="T319" id="Seg_10906" s="T318">Кушают.</ta>
            <ta e="T329" id="Seg_10907" s="T320">Ненцы так говорят: Дед, печь зачем так дровами заполнил?</ta>
            <ta e="T334" id="Seg_10908" s="T330"> - Нет, в печи сгорит [всё]. </ta>
            <ta e="T340" id="Seg_10909" s="T335">Нет, мол я это …</ta>
            <ta e="T355" id="Seg_10910" s="T341">Он не съест, он не съест, так делается [=находится], такая заячья шуба рваная. </ta>
            <ta e="T359" id="Seg_10911" s="T356">Старая, эту одел.</ta>
            <ta e="T365" id="Seg_10912" s="T360">Возле печки находится возле двери.</ta>
            <ta e="T375" id="Seg_10913" s="T366">В одно время с улицы в дом горящий факел упал.</ta>
            <ta e="T383" id="Seg_10914" s="T376">Так побежал к двери к невесткам.</ta>
            <ta e="T397" id="Seg_10915" s="T384">Ненцы только схватили старика, только вся старая заячья шуба оторвалась, схватили дома.</ta>
            <ta e="T412" id="Seg_10916" s="T398">Этим огнём в это окошко в проём в дом, такие окошки с обеих сторон [дома] были.</ta>
            <ta e="T417" id="Seg_10917" s="T413">Большой дом был.</ta>
            <ta e="T430" id="Seg_10918" s="T418">Дом землёй покрыт, вот эта из дерева землянка размером с топора ручки закрыта [землёй]. </ta>
            <ta e="T923" id="Seg_10919" s="T431">В дом огонь забросали, [огонь] ударил того старика.</ta>
            <ta e="T446" id="Seg_10920" s="T923">- Вроде это съел. </ta>
            <ta e="T448" id="Seg_10921" s="T924">Подожди.</ta>
            <ta e="T464" id="Seg_10922" s="T449">Порох он рассыпал под траву, вот такой новой травы под низ.</ta>
            <ta e="T476" id="Seg_10923" s="T465">Огонь разгорелся (=ударил), этих тридцать войнов всех огонь съел.</ta>
            <ta e="T480" id="Seg_10924" s="T477">На бок упали впереди [ненцы].</ta>
            <ta e="T491" id="Seg_10925" s="T481">Вперёд на улицу старик побежал к проёму двери, с топором в руках находится..</ta>
            <ta e="T499" id="Seg_10926" s="T492">Только на улицу высунутся, опять в лицо ударяет.</ta>
            <ta e="T516" id="Seg_10927" s="T500">Невестки этот огонь забрасывали, с верха печки забрасывают и в два окна отверстия забрасывают.</ta>
            <ta e="T523" id="Seg_10928" s="T517">Всех огнём сжёг, этих ненецких войнов.</ta>
            <ta e="T529" id="Seg_10929" s="T524">Эти войны вроде две было.</ta>
            <ta e="T531" id="Seg_10930" s="T530">Говорите.</ta>
            <ta e="T534" id="Seg_10931" s="T926">Ну говори.</ta>
            <ta e="T539" id="Seg_10932" s="T925">Один старик куда денется?</ta>
            <ta e="T551" id="Seg_10933" s="T540">Старик потом, ну, старик заячья шуба вниз к ненцев нартам пошёл.</ta>
            <ta e="T927" id="Seg_10934" s="T552">Ещё двое летят с севера в ненецкий дом поехать.</ta>
            <ta e="T562" id="Seg_10935" s="T927">– Садись.</ta>
            <ta e="T567" id="Seg_10936" s="T928">К эти убитым людям поехать.</ta>
            <ta e="T577" id="Seg_10937" s="T568">Вниз пошёл, о, снизу вверх хромой ненец-старик появился.</ta>
            <ta e="T583" id="Seg_10938" s="T580">Я засмеялась.</ta>
            <ta e="T589" id="Seg_10939" s="T584">Старика заячьей шубы жену оставили.</ta>
            <ta e="T599" id="Seg_10940" s="T590">Старик заячья шуба напотмашь ударил, [у того] лосиная шея назад сломалась. </ta>
            <ta e="T603" id="Seg_10941" s="T600">Потом вниз пошёл.</ta>
            <ta e="T611" id="Seg_10942" s="T604">Вниз пошёл, потом с тремя невестками на север уехали.</ta>
            <ta e="T618" id="Seg_10943" s="T612">На севере ненецкий дом на севере был там.</ta>
            <ta e="T632" id="Seg_10944" s="T619">Вниз пошли туда, туда поехали на ненецких нартах, на этих людей нартах, эти три [невестки] с дедом.</ta>
            <ta e="T636" id="Seg_10945" s="T633">На север отправившись, поехали.</ta>
            <ta e="T645" id="Seg_10946" s="T637">На севере ненецких женщин погонял.</ta>
            <ta e="T656" id="Seg_10947" s="T646">Эти селькупские мальчики [сироты] там эти остались [тогда] давно, к этому деду пришли.</ta>
            <ta e="T664" id="Seg_10948" s="T657">Вверх, ну это, двух ненцев домой отправили. </ta>
            <ta e="T685" id="Seg_10949" s="T665">Лохмотья [нюка] чума, всё что есть, старик заячья шуба оленей по снегу вперёд погнал, эти сироты-мальчики с ним были. </ta>
            <ta e="T693" id="Seg_10950" s="T686">Тогда давно недалеко от них ненцы, ну это.</ta>
            <ta e="T700" id="Seg_10951" s="T694">От старика заячьей шубы недалеко два внука были. </ta>
            <ta e="T706" id="Seg_10952" s="T701">Этих убитых людей, этих ненцев.</ta>
            <ta e="T710" id="Seg_10953" s="T707">Этих двое детей.</ta>
            <ta e="T714" id="Seg_10954" s="T711">Такие два ребёнка.</ta>
            <ta e="T722" id="Seg_10955" s="T715">Оленей согнав домой привели в тот дом. </ta>
            <ta e="T725" id="Seg_10956" s="T723">Чум поставили.</ta>
            <ta e="T731" id="Seg_10957" s="T930">Этот маленький так хочет кушать.</ta>
            <ta e="T739" id="Seg_10958" s="T929">Сыновья до сих пор [были] ушедшие в лес белковать, охотничать. </ta>
            <ta e="T741" id="Seg_10959" s="T740">слова кого-то другого [ребёнку]</ta>
            <ta e="T748" id="Seg_10960" s="T742">Вверх чум поставили, подожди, одну ночь переночевали. </ta>
            <ta e="T750" id="Seg_10961" s="T749">Собираются переночевать.</ta>
            <ta e="T757" id="Seg_10962" s="T751">Сыновья из лесу вернулись (=себя привели) с белкования домой.</ta>
            <ta e="T760" id="Seg_10963" s="T758">Крадясь вернулись (=привели себя).</ta>
            <ta e="T763" id="Seg_10964" s="T761">Что случилось?</ta>
            <ta e="T770" id="Seg_10965" s="T764">Отца чума лохмотья [нюка].</ta>
            <ta e="T777" id="Seg_10966" s="T771">Вниз только посмотрели, полностью огонь съел.</ta>
            <ta e="T784" id="Seg_10967" s="T777">Отца наверное убили, - так думают.</ta>
            <ta e="T788" id="Seg_10968" s="T785">Эти семь сыновей.</ta>
            <ta e="T790" id="Seg_10969" s="T789">Подожди.</ta>
            <ta e="T793" id="Seg_10970" s="T791">Потом…</ta>
            <ta e="T796" id="Seg_10971" s="T794">[Быстро] пришли (=привели себя).</ta>
            <ta e="T803" id="Seg_10972" s="T797">На улице собака, младшего сына собака.</ta>
            <ta e="T807" id="Seg_10973" s="T804">Жена на улицу привязала.</ta>
            <ta e="T815" id="Seg_10974" s="T808">Оленей которые на улице, этих ненецких оленей погнали. </ta>
            <ta e="T821" id="Seg_10975" s="T816">Собаки на ногу наступили, собака завизжала.</ta>
            <ta e="T829" id="Seg_10976" s="T822">Старик так очнулся, старик заячья шуба.</ta>
            <ta e="T836" id="Seg_10977" s="T830">С собакой что случилось, что, олень боднул? </ta>
            <ta e="T843" id="Seg_10978" s="T837">Младшего сына жена на улицу выскочила.</ta>
            <ta e="T850" id="Seg_10979" s="T844">Младший сын старика заячья шуба.</ta>
            <ta e="T857" id="Seg_10980" s="T851">На улицу только выскочила, что такое? Мой муж.</ta>
            <ta e="T862" id="Seg_10981" s="T858">Это люди там - догадался.</ta>
            <ta e="T869" id="Seg_10982" s="T863">Отец их очнулся в доме.</ta>
            <ta e="T879" id="Seg_10983" s="T870">Мы дом, мы огнём сожгли.</ta>
            <ta e="T885" id="Seg_10984" s="T880">Дед нас так научил.</ta>
            <ta e="T890" id="Seg_10985" s="T886">Сначала что ли?</ta>
            <ta e="T892" id="Seg_10986" s="T890">Говорите. Нет.</ta>
            <ta e="T896" id="Seg_10987" s="T931">Давай, дальше говори.</ta>
            <ta e="T910" id="Seg_10988" s="T932">Ну потом жили там и опять уехали, эти селькупы.</ta>
            <ta e="T916" id="Seg_10989" s="T911">Ну с ненецкой земли оленей согнали.</ta>
            <ta e="T920" id="Seg_10990" s="T917">Этот селькуп…</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T46" id="Seg_10991" s="T40">[BrM:] SUP.3SG instead of SUP.3PL?</ta>
            <ta e="T112" id="Seg_10992" s="T105">[IES:] OFF-topic: The storyteller thinks it was the dog screaming (and in fact it was a child). [BrM:] Tentative analysis of 'čʼütalʼalta'.</ta>
            <ta e="T154" id="Seg_10993" s="T144">[BrM:] Tentative analysis of 'kun montɨ'</ta>
            <ta e="T240" id="Seg_10994" s="T224">[IES:] 'dried food' denotes boiled meat or fish.</ta>
            <ta e="T288" id="Seg_10995" s="T280">[BrM:] Tentative analysis of 'sälɨŋɨtɨ'.</ta>
            <ta e="T355" id="Seg_10996" s="T341">[BrM:] Unclear syntax of the last part.</ta>
            <ta e="T430" id="Seg_10997" s="T418">[BrM:] Unclear form 'loːqɨra'.</ta>
            <ta e="T464" id="Seg_10998" s="T449">[BrM:] NB usage of Locative.</ta>
            <ta e="T491" id="Seg_10999" s="T481">[BrM:] Unclear construction 'pičʼilʼ ut'.</ta>
            <ta e="T531" id="Seg_11000" s="T530">[BrM:] Said by A.I.Kuzmina after a break in the recording.</ta>
            <ta e="T599" id="Seg_11001" s="T590">[BrM:] Tentative analysis of 'säppaja'.</ta>
            <ta e="T664" id="Seg_11002" s="T657">[BrM:] Uncertain analysis of 'konnätɨ'.</ta>
            <ta e="T693" id="Seg_11003" s="T686">[BrM:] Unclear translation and sentence strucure.</ta>
            <ta e="T741" id="Seg_11004" s="T740">[BrM:] Said by someone else to a child.</ta>
            <ta e="T829" id="Seg_11005" s="T822">[BrM:] Tentative translation and analysis of 'nülʼčʼa'.</ta>
            <ta e="T869" id="Seg_11006" s="T863">[BrM:] Tentative analysis of 'nülʼčʼɨkun'.</ta>
            <ta e="T890" id="Seg_11007" s="T886">[BrM:] Said by someone else after a break in the recording.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T921" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T922" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T923" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T924" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T926" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T925" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T927" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T928" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T930" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T929" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T931" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T932" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
