<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>NEP_196X_OrphanBoyAndPanOldMan2_flk</transcription-name>
         <referenced-file url="NEP_196X_OrphanBoyAndPanOldMan2_flk.wav" />
         <referenced-file url="NEP_196X_OrphanBoyAndPanOldMan2_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">NEP_196X_OrphanBoyAndPanOldMan2_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1502</ud-information>
            <ud-information attribute-name="# HIAT:w">961</ud-information>
            <ud-information attribute-name="# e">989</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">54</ud-information>
            <ud-information attribute-name="# HIAT:u">177</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="NEP">
            <abbreviation>NEP</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.51" type="appl" />
         <tli id="T968" time="2.04" type="intp" />
         <tli id="T1" time="3.57" type="appl" />
         <tli id="T2" time="4.434" type="appl" />
         <tli id="T3" time="5.108" type="appl" />
         <tli id="T4" time="5.783" type="appl" />
         <tli id="T5" time="6.457" type="appl" />
         <tli id="T6" time="7.132" type="appl" />
         <tli id="T7" time="7.938" type="appl" />
         <tli id="T8" time="8.563" type="appl" />
         <tli id="T9" time="9.189" type="appl" />
         <tli id="T10" time="9.815" type="appl" />
         <tli id="T11" time="10.441" type="appl" />
         <tli id="T12" time="11.066" type="appl" />
         <tli id="T13" time="11.692" type="appl" />
         <tli id="T14" time="12.509" type="appl" />
         <tli id="T15" time="12.973" type="appl" />
         <tli id="T16" time="13.436" type="appl" />
         <tli id="T17" time="13.899" type="appl" />
         <tli id="T18" time="14.363" type="appl" />
         <tli id="T19" time="14.826" type="appl" />
         <tli id="T969" time="16.566000000000003" type="intp" />
         <tli id="T20" time="18.306" type="appl" />
         <tli id="T21" time="18.867" type="appl" />
         <tli id="T22" time="19.248" type="appl" />
         <tli id="T23" time="19.63" type="appl" />
         <tli id="T24" time="20.011" type="appl" />
         <tli id="T25" time="20.392" type="appl" />
         <tli id="T970" time="20.5825" type="intp" />
         <tli id="T26" time="20.773" type="appl" />
         <tli id="T27" time="22.451" type="appl" />
         <tli id="T28" time="22.822" type="appl" />
         <tli id="T29" time="23.194" type="appl" />
         <tli id="T30" time="23.566" type="appl" />
         <tli id="T31" time="23.937" type="appl" />
         <tli id="T32" time="24.309" type="appl" />
         <tli id="T33" time="24.68" type="appl" />
         <tli id="T971" time="24.866" type="intp" />
         <tli id="T34" time="25.052" type="appl" />
         <tli id="T972" time="25.547" type="intp" />
         <tli id="T35" time="26.042" type="appl" />
         <tli id="T36" time="26.784" type="appl" />
         <tli id="T973" time="27.11" type="intp" />
         <tli id="T37" time="27.436" type="appl" />
         <tli id="T38" time="28.175" type="appl" />
         <tli id="T39" time="28.904" type="appl" />
         <tli id="T40" time="29.632" type="appl" />
         <tli id="T41" time="30.361" type="appl" />
         <tli id="T42" time="31.599" type="appl" />
         <tli id="T43" time="32.424" type="appl" />
         <tli id="T44" time="32.889" type="appl" />
         <tli id="T45" time="33.353" type="appl" />
         <tli id="T46" time="33.915" type="appl" />
         <tli id="T47" time="34.36" type="appl" />
         <tli id="T48" time="34.806" type="appl" />
         <tli id="T49" time="35.252" type="appl" />
         <tli id="T50" time="35.697" type="appl" />
         <tli id="T51" time="36.143" type="appl" />
         <tli id="T52" time="36.588" type="appl" />
         <tli id="T53" time="37.034" type="appl" />
         <tli id="T54" time="37.396" type="appl" />
         <tli id="T55" time="37.757" type="appl" />
         <tli id="T56" time="38.118" type="appl" />
         <tli id="T57" time="38.48" type="appl" />
         <tli id="T58" time="38.842" type="appl" />
         <tli id="T992" time="39.0225" type="intp" />
         <tli id="T59" time="42.333237605482424" />
         <tli id="T60" time="42.991" type="appl" />
         <tli id="T61" time="43.668" type="appl" />
         <tli id="T62" time="46.23" type="appl" />
         <tli id="T63" time="46.864" type="appl" />
         <tli id="T64" time="47.437" type="appl" />
         <tli id="T65" time="48.009" type="appl" />
         <tli id="T66" time="48.581" type="appl" />
         <tli id="T67" time="49.154" type="appl" />
         <tli id="T68" time="49.726" type="appl" />
         <tli id="T69" time="51.869" type="appl" />
         <tli id="T70" time="52.271" type="appl" />
         <tli id="T71" time="52.672" type="appl" />
         <tli id="T72" time="53.073" type="appl" />
         <tli id="T73" time="53.475" type="appl" />
         <tli id="T74" time="53.876" type="appl" />
         <tli id="T75" time="54.97" type="appl" />
         <tli id="T76" time="55.425" type="appl" />
         <tli id="T77" time="55.881" type="appl" />
         <tli id="T78" time="56.337" type="appl" />
         <tli id="T79" time="56.792" type="appl" />
         <tli id="T80" time="57.248" type="appl" />
         <tli id="T81" time="57.704" type="appl" />
         <tli id="T82" time="58.159" type="appl" />
         <tli id="T83" time="58.615" type="appl" />
         <tli id="T84" time="59.467" type="appl" />
         <tli id="T85" time="59.9" type="appl" />
         <tli id="T86" time="60.333" type="appl" />
         <tli id="T87" time="60.765" type="appl" />
         <tli id="T88" time="61.198" type="appl" />
         <tli id="T89" time="61.631" type="appl" />
         <tli id="T90" time="62.064" type="appl" />
         <tli id="T91" time="64.034" type="appl" />
         <tli id="T92" time="64.577" type="appl" />
         <tli id="T93" time="65.119" type="appl" />
         <tli id="T94" time="65.661" type="appl" />
         <tli id="T95" time="66.204" type="appl" />
         <tli id="T96" time="66.746" type="appl" />
         <tli id="T97" time="67.944" type="appl" />
         <tli id="T98" time="68.285" type="appl" />
         <tli id="T99" time="68.625" type="appl" />
         <tli id="T100" time="68.966" type="appl" />
         <tli id="T101" time="69.307" type="appl" />
         <tli id="T102" time="71.544" type="appl" />
         <tli id="T103" time="71.935" type="appl" />
         <tli id="T104" time="72.326" type="appl" />
         <tli id="T105" time="72.717" type="appl" />
         <tli id="T106" time="73.109" type="appl" />
         <tli id="T107" time="73.5" type="appl" />
         <tli id="T108" time="73.891" type="appl" />
         <tli id="T109" time="74.282" type="appl" />
         <tli id="T110" time="74.673" type="appl" />
         <tli id="T111" time="76.498" type="appl" />
         <tli id="T112" time="76.846" type="appl" />
         <tli id="T113" time="77.194" type="appl" />
         <tli id="T114" time="77.543" type="appl" />
         <tli id="T115" time="77.892" type="appl" />
         <tli id="T116" time="78.24" type="appl" />
         <tli id="T117" time="78.588" type="appl" />
         <tli id="T118" time="78.937" type="appl" />
         <tli id="T119" time="79.286" type="appl" />
         <tli id="T120" time="79.634" type="appl" />
         <tli id="T121" time="79.982" type="appl" />
         <tli id="T122" time="80.331" type="appl" />
         <tli id="T123" time="81.58" type="appl" />
         <tli id="T124" time="82.268" type="appl" />
         <tli id="T125" time="82.956" type="appl" />
         <tli id="T126" time="84.108" type="appl" />
         <tli id="T127" time="84.828" type="appl" />
         <tli id="T128" time="86.314" type="appl" />
         <tli id="T129" time="86.985" type="appl" />
         <tli id="T130" time="88.048" type="appl" />
         <tli id="T131" time="88.618" type="appl" />
         <tli id="T132" time="89.189" type="appl" />
         <tli id="T133" time="89.759" type="appl" />
         <tli id="T134" time="90.329" type="appl" />
         <tli id="T135" time="90.899" type="appl" />
         <tli id="T136" time="91.47" type="appl" />
         <tli id="T137" time="92.04" type="appl" />
         <tli id="T138" time="92.61" type="appl" />
         <tli id="T139" time="95.027" type="appl" />
         <tli id="T140" time="95.442" type="appl" />
         <tli id="T141" time="95.856" type="appl" />
         <tli id="T142" time="96.27" type="appl" />
         <tli id="T143" time="96.684" type="appl" />
         <tli id="T144" time="97.099" type="appl" />
         <tli id="T145" time="97.513" type="appl" />
         <tli id="T146" time="98.604" type="appl" />
         <tli id="T147" time="99.199" type="appl" />
         <tli id="T148" time="101.081" type="appl" />
         <tli id="T149" time="101.502" type="appl" />
         <tli id="T150" time="101.923" type="appl" />
         <tli id="T151" time="102.344" type="appl" />
         <tli id="T152" time="102.765" type="appl" />
         <tli id="T153" time="103.186" type="appl" />
         <tli id="T154" time="103.607" type="appl" />
         <tli id="T155" time="104.028" type="appl" />
         <tli id="T156" time="104.449" type="appl" />
         <tli id="T157" time="104.87" type="appl" />
         <tli id="T158" time="105.85" type="appl" />
         <tli id="T159" time="106.217" type="appl" />
         <tli id="T160" time="106.585" type="appl" />
         <tli id="T161" time="106.952" type="appl" />
         <tli id="T162" time="107.32" type="appl" />
         <tli id="T163" time="107.687" type="appl" />
         <tli id="T164" time="108.055" type="appl" />
         <tli id="T165" time="108.422" type="appl" />
         <tli id="T166" time="108.79" type="appl" />
         <tli id="T167" time="109.157" type="appl" />
         <tli id="T168" time="109.525" type="appl" />
         <tli id="T169" time="109.892" type="appl" />
         <tli id="T170" time="110.26" type="appl" />
         <tli id="T171" time="110.687" type="appl" />
         <tli id="T172" time="111.033" type="appl" />
         <tli id="T173" time="111.378" type="appl" />
         <tli id="T174" time="113.331" type="appl" />
         <tli id="T175" time="114.352" type="appl" />
         <tli id="T176" time="115.374" type="appl" />
         <tli id="T177" time="116.395" type="appl" />
         <tli id="T178" time="117.107" type="appl" />
         <tli id="T179" time="117.65" type="appl" />
         <tli id="T180" time="118.194" type="appl" />
         <tli id="T181" time="118.737" type="appl" />
         <tli id="T182" time="119.281" type="appl" />
         <tli id="T183" time="121.507" type="appl" />
         <tli id="T184" time="122.097" type="appl" />
         <tli id="T185" time="122.687" type="appl" />
         <tli id="T186" time="123.277" type="appl" />
         <tli id="T187" time="123.867" type="appl" />
         <tli id="T188" time="124.457" type="appl" />
         <tli id="T189" time="125.324" type="appl" />
         <tli id="T190" time="125.663" type="appl" />
         <tli id="T191" time="126.002" type="appl" />
         <tli id="T192" time="126.341" type="appl" />
         <tli id="T193" time="126.68" type="appl" />
         <tli id="T194" time="127.018" type="appl" />
         <tli id="T195" time="127.357" type="appl" />
         <tli id="T196" time="127.696" type="appl" />
         <tli id="T197" time="128.035" type="appl" />
         <tli id="T198" time="128.374" type="appl" />
         <tli id="T199" time="128.713" type="appl" />
         <tli id="T200" time="130.423" type="appl" />
         <tli id="T201" time="131.182" type="appl" />
         <tli id="T202" time="132.016" type="appl" />
         <tli id="T203" time="132.607" type="appl" />
         <tli id="T204" time="133.198" type="appl" />
         <tli id="T205" time="133.79" type="appl" />
         <tli id="T206" time="134.381" type="appl" />
         <tli id="T207" time="134.972" type="appl" />
         <tli id="T208" time="135.563" type="appl" />
         <tli id="T209" time="143.162" type="appl" />
         <tli id="T210" time="143.632" type="appl" />
         <tli id="T211" time="144.101" type="appl" />
         <tli id="T212" time="144.571" type="appl" />
         <tli id="T213" time="146.01" type="appl" />
         <tli id="T214" time="146.521" type="appl" />
         <tli id="T215" time="147.032" type="appl" />
         <tli id="T216" time="147.72" type="appl" />
         <tli id="T217" time="148.214" type="appl" />
         <tli id="T218" time="148.709" type="appl" />
         <tli id="T219" time="149.204" type="appl" />
         <tli id="T220" time="149.699" type="appl" />
         <tli id="T221" time="150.193" type="appl" />
         <tli id="T222" time="150.688" type="appl" />
         <tli id="T223" time="151.183" type="appl" />
         <tli id="T224" time="151.678" type="appl" />
         <tli id="T225" time="152.172" type="appl" />
         <tli id="T226" time="152.667" type="appl" />
         <tli id="T227" time="154.663" type="appl" />
         <tli id="T228" time="156.003" type="appl" />
         <tli id="T229" time="156.505" type="appl" />
         <tli id="T230" time="157.006" type="appl" />
         <tli id="T231" time="157.507" type="appl" />
         <tli id="T232" time="158.009" type="appl" />
         <tli id="T233" time="158.51" type="appl" />
         <tli id="T234" time="159.012" type="appl" />
         <tli id="T235" time="159.513" type="appl" />
         <tli id="T236" time="160.014" type="appl" />
         <tli id="T237" time="160.516" type="appl" />
         <tli id="T238" time="161.017" type="appl" />
         <tli id="T239" time="161.807" type="appl" />
         <tli id="T240" time="162.259" type="appl" />
         <tli id="T241" time="162.71" type="appl" />
         <tli id="T242" time="163.734" type="appl" />
         <tli id="T243" time="164.321" type="appl" />
         <tli id="T244" time="164.908" type="appl" />
         <tli id="T245" time="165.496" type="appl" />
         <tli id="T246" time="166.116" type="appl" />
         <tli id="T247" time="166.638" type="appl" />
         <tli id="T248" time="167.159" type="appl" />
         <tli id="T249" time="167.681" type="appl" />
         <tli id="T250" time="168.202" type="appl" />
         <tli id="T251" time="168.724" type="appl" />
         <tli id="T252" time="172.137" type="appl" />
         <tli id="T253" time="172.714" type="appl" />
         <tli id="T254" time="173.324" type="appl" />
         <tli id="T255" time="173.769" type="appl" />
         <tli id="T256" time="174.215" type="appl" />
         <tli id="T257" time="174.66" type="appl" />
         <tli id="T258" time="175.106" type="appl" />
         <tli id="T259" time="176.086" type="appl" />
         <tli id="T260" time="176.464" type="appl" />
         <tli id="T261" time="176.842" type="appl" />
         <tli id="T262" time="177.221" type="appl" />
         <tli id="T263" time="177.6" type="appl" />
         <tli id="T264" time="177.978" type="appl" />
         <tli id="T265" time="178.356" type="appl" />
         <tli id="T266" time="178.735" type="appl" />
         <tli id="T267" time="179.462" type="appl" />
         <tli id="T268" time="179.753" type="appl" />
         <tli id="T269" time="180.044" type="appl" />
         <tli id="T270" time="180.336" type="appl" />
         <tli id="T271" time="180.627" type="appl" />
         <tli id="T272" time="180.918" type="appl" />
         <tli id="T273" time="181.209" type="appl" />
         <tli id="T274" time="181.5" type="appl" />
         <tli id="T275" time="181.992" type="appl" />
         <tli id="T276" time="182.435" type="appl" />
         <tli id="T277" time="182.878" type="appl" />
         <tli id="T278" time="184.021" type="appl" />
         <tli id="T279" time="184.753" type="appl" />
         <tli id="T280" time="187.172" type="appl" />
         <tli id="T281" time="187.594" type="appl" />
         <tli id="T282" time="188.017" type="appl" />
         <tli id="T283" time="188.529" type="appl" />
         <tli id="T284" time="188.904" type="appl" />
         <tli id="T285" time="189.28" type="appl" />
         <tli id="T286" time="189.655" type="appl" />
         <tli id="T287" time="190.031" type="appl" />
         <tli id="T288" time="190.406" type="appl" />
         <tli id="T289" time="190.782" type="appl" />
         <tli id="T290" time="191.378" type="appl" />
         <tli id="T291" time="191.801" type="appl" />
         <tli id="T292" time="192.223" type="appl" />
         <tli id="T293" time="192.646" type="appl" />
         <tli id="T294" time="193.068" type="appl" />
         <tli id="T295" time="193.491" type="appl" />
         <tli id="T296" time="193.913" type="appl" />
         <tli id="T297" time="194.926" type="appl" />
         <tli id="T298" time="195.402" type="appl" />
         <tli id="T299" time="195.879" type="appl" />
         <tli id="T300" time="196.356" type="appl" />
         <tli id="T301" time="197.522" type="appl" />
         <tli id="T302" time="197.826" type="appl" />
         <tli id="T303" time="198.13" type="appl" />
         <tli id="T304" time="198.435" type="appl" />
         <tli id="T305" time="198.74" type="appl" />
         <tli id="T306" time="199.044" type="appl" />
         <tli id="T307" time="199.348" type="appl" />
         <tli id="T308" time="199.653" type="appl" />
         <tli id="T309" time="200.884" type="appl" />
         <tli id="T310" time="201.325" type="appl" />
         <tli id="T311" time="201.767" type="appl" />
         <tli id="T312" time="202.209" type="appl" />
         <tli id="T313" time="202.65" type="appl" />
         <tli id="T314" time="203.092" type="appl" />
         <tli id="T315" time="203.533" type="appl" />
         <tli id="T316" time="203.975" type="appl" />
         <tli id="T317" time="204.417" type="appl" />
         <tli id="T318" time="204.858" type="appl" />
         <tli id="T319" time="205.3" type="appl" />
         <tli id="T320" time="206.347" type="appl" />
         <tli id="T321" time="206.764" type="appl" />
         <tli id="T322" time="207.18" type="appl" />
         <tli id="T323" time="207.596" type="appl" />
         <tli id="T324" time="208.013" type="appl" />
         <tli id="T325" time="208.429" type="appl" />
         <tli id="T326" time="208.845" type="appl" />
         <tli id="T327" time="209.262" type="appl" />
         <tli id="T328" time="209.678" type="appl" />
         <tli id="T329" time="214.871" type="appl" />
         <tli id="T330" time="215.371" type="appl" />
         <tli id="T331" time="215.871" type="appl" />
         <tli id="T332" time="216.765" type="appl" />
         <tli id="T333" time="217.157" type="appl" />
         <tli id="T334" time="217.548" type="appl" />
         <tli id="T335" time="217.94" type="appl" />
         <tli id="T336" time="218.331" type="appl" />
         <tli id="T337" time="219.023" type="appl" />
         <tli id="T338" time="219.454" type="appl" />
         <tli id="T339" time="219.884" type="appl" />
         <tli id="T340" time="220.315" type="appl" />
         <tli id="T341" time="220.746" type="appl" />
         <tli id="T342" time="221.389" type="appl" />
         <tli id="T343" time="221.758" type="appl" />
         <tli id="T344" time="222.126" type="appl" />
         <tli id="T345" time="222.495" type="appl" />
         <tli id="T346" time="222.863" type="appl" />
         <tli id="T347" time="225.742" type="appl" />
         <tli id="T348" time="226.185" type="appl" />
         <tli id="T349" time="226.628" type="appl" />
         <tli id="T350" time="227.071" type="appl" />
         <tli id="T351" time="227.514" type="appl" />
         <tli id="T352" time="227.956" type="appl" />
         <tli id="T353" time="228.399" type="appl" />
         <tli id="T354" time="228.842" type="appl" />
         <tli id="T355" time="229.285" type="appl" />
         <tli id="T356" time="229.728" type="appl" />
         <tli id="T357" time="230.475" type="appl" />
         <tli id="T358" time="230.91" type="appl" />
         <tli id="T359" time="231.346" type="appl" />
         <tli id="T360" time="232.406" type="appl" />
         <tli id="T361" time="232.859" type="appl" />
         <tli id="T362" time="233.312" type="appl" />
         <tli id="T363" time="233.764" type="appl" />
         <tli id="T364" time="234.217" type="appl" />
         <tli id="T365" time="234.67" type="appl" />
         <tli id="T366" time="237.221" type="appl" />
         <tli id="T367" time="237.609" type="appl" />
         <tli id="T368" time="237.998" type="appl" />
         <tli id="T369" time="238.386" type="appl" />
         <tli id="T370" time="238.775" type="appl" />
         <tli id="T371" time="239.163" type="appl" />
         <tli id="T372" time="239.552" type="appl" />
         <tli id="T373" time="239.94" type="appl" />
         <tli id="T374" time="240.329" type="appl" />
         <tli id="T375" time="240.717" type="appl" />
         <tli id="T376" time="241.106" type="appl" />
         <tli id="T377" time="242.486" type="appl" />
         <tli id="T378" time="242.947" type="appl" />
         <tli id="T379" time="243.409" type="appl" />
         <tli id="T380" time="243.87" type="appl" />
         <tli id="T381" time="244.332" type="appl" />
         <tli id="T382" time="244.794" type="appl" />
         <tli id="T383" time="245.255" type="appl" />
         <tli id="T384" time="245.717" type="appl" />
         <tli id="T385" time="247.171" type="appl" />
         <tli id="T386" time="247.669" type="appl" />
         <tli id="T387" time="248.166" type="appl" />
         <tli id="T388" time="248.664" type="appl" />
         <tli id="T389" time="249.161" type="appl" />
         <tli id="T390" time="249.659" type="appl" />
         <tli id="T391" time="250.156" type="appl" />
         <tli id="T392" time="251.073" type="appl" />
         <tli id="T393" time="251.415" type="appl" />
         <tli id="T394" time="251.757" type="appl" />
         <tli id="T395" time="252.099" type="appl" />
         <tli id="T396" time="254.504" type="appl" />
         <tli id="T397" time="254.955" type="appl" />
         <tli id="T398" time="255.405" type="appl" />
         <tli id="T399" time="255.856" type="appl" />
         <tli id="T400" time="256.307" type="appl" />
         <tli id="T401" time="257.076" type="appl" />
         <tli id="T402" time="257.514" type="appl" />
         <tli id="T403" time="257.953" type="appl" />
         <tli id="T404" time="258.392" type="appl" />
         <tli id="T405" time="258.83" type="appl" />
         <tli id="T406" time="259.269" type="appl" />
         <tli id="T407" time="259.568" type="appl" />
         <tli id="T408" time="259.865" type="appl" />
         <tli id="T409" time="260.161" type="appl" />
         <tli id="T410" time="260.457" type="appl" />
         <tli id="T411" time="260.754" type="appl" />
         <tli id="T412" time="261.05" type="appl" />
         <tli id="T413" time="261.527" type="appl" />
         <tli id="T414" time="261.886" type="appl" />
         <tli id="T415" time="262.245" type="appl" />
         <tli id="T416" time="263.805" type="appl" />
         <tli id="T417" time="264.457" type="appl" />
         <tli id="T418" time="265.109" type="appl" />
         <tli id="T419" time="265.761" type="appl" />
         <tli id="T420" time="266.414" type="appl" />
         <tli id="T421" time="267.066" type="appl" />
         <tli id="T422" time="267.718" type="appl" />
         <tli id="T423" time="268.37" type="appl" />
         <tli id="T424" time="269.022" type="appl" />
         <tli id="T425" time="269.674" type="appl" />
         <tli id="T426" time="270.30266666666665" type="intp" />
         <tli id="T427" time="270.9313333333333" type="intp" />
         <tli id="T428" time="271.56" type="intp" />
         <tli id="T429" time="272.1886666666667" type="intp" />
         <tli id="T430" time="272.81733333333335" type="intp" />
         <tli id="T431" time="273.446" type="intp" />
         <tli id="T432" time="274.0746666666667" type="intp" />
         <tli id="T433" time="274.7033333333334" type="intp" />
         <tli id="T434" time="275.33200000000005" type="intp" />
         <tli id="T435" time="275.9606666666667" type="intp" />
         <tli id="T436" time="276.5893333333334" type="intp" />
         <tli id="T437" time="277.2180000000001" type="intp" />
         <tli id="T438" time="277.8466666666667" type="intp" />
         <tli id="T439" time="278.4753333333333" type="intp" />
         <tli id="T440" time="279.104" type="appl" />
         <tli id="T441" time="279.498" type="appl" />
         <tli id="T442" time="279.891" type="appl" />
         <tli id="T443" time="280.285" type="appl" />
         <tli id="T444" time="283.6" type="appl" />
         <tli id="T445" time="284.156" type="appl" />
         <tli id="T446" time="285.41" type="appl" />
         <tli id="T447" time="286.321" type="appl" />
         <tli id="T448" time="287.214" type="appl" />
         <tli id="T449" time="287.775" type="appl" />
         <tli id="T450" time="288.336" type="appl" />
         <tli id="T451" time="288.897" type="appl" />
         <tli id="T452" time="289.458" type="appl" />
         <tli id="T453" time="290.02" type="appl" />
         <tli id="T454" time="290.581" type="appl" />
         <tli id="T455" time="291.142" type="appl" />
         <tli id="T456" time="291.703" type="appl" />
         <tli id="T457" time="292.264" type="appl" />
         <tli id="T458" time="293.127" type="appl" />
         <tli id="T459" time="293.499" type="appl" />
         <tli id="T460" time="293.87" type="appl" />
         <tli id="T461" time="294.242" type="appl" />
         <tli id="T462" time="294.613" type="appl" />
         <tli id="T463" time="294.984" type="appl" />
         <tli id="T464" time="295.356" type="appl" />
         <tli id="T465" time="295.727" type="appl" />
         <tli id="T466" time="296.099" type="appl" />
         <tli id="T467" time="300.62598686324793" />
         <tli id="T468" time="301.388" type="appl" />
         <tli id="T469" time="302.114" type="appl" />
         <tli id="T470" time="302.839" type="appl" />
         <tli id="T471" time="303.564" type="appl" />
         <tli id="T472" time="304.29" type="appl" />
         <tli id="T473" time="305.015" type="appl" />
         <tli id="T474" time="305.741" type="appl" />
         <tli id="T475" time="306.466" type="appl" />
         <tli id="T476" time="307.191" type="appl" />
         <tli id="T477" time="307.917" type="appl" />
         <tli id="T478" time="308.642" type="appl" />
         <tli id="T479" time="309.574" type="appl" />
         <tli id="T480" time="310.174" type="appl" />
         <tli id="T481" time="310.774" type="appl" />
         <tli id="T482" time="311.629" type="appl" />
         <tli id="T483" time="311.895" type="appl" />
         <tli id="T484" time="312.161" type="appl" />
         <tli id="T485" time="312.427" type="appl" />
         <tli id="T486" time="312.692" type="appl" />
         <tli id="T487" time="312.958" type="appl" />
         <tli id="T488" time="313.224" type="appl" />
         <tli id="T489" time="313.49" type="appl" />
         <tli id="T490" time="313.756" type="appl" />
         <tli id="T491" time="315.798" type="appl" />
         <tli id="T492" time="316.492" type="appl" />
         <tli id="T493" time="317.185" type="appl" />
         <tli id="T494" time="317.879" type="appl" />
         <tli id="T495" time="318.572" type="appl" />
         <tli id="T496" time="320.188" type="appl" />
         <tli id="T497" time="320.546" type="appl" />
         <tli id="T993" time="320.7448888888889" type="intp" />
         <tli id="T498" time="320.904" type="appl" />
         <tli id="T499" time="321.263" type="appl" />
         <tli id="T500" time="321.622" type="appl" />
         <tli id="T501" time="321.98" type="appl" />
         <tli id="T502" time="322.382" type="appl" />
         <tli id="T503" time="322.783" type="appl" />
         <tli id="T504" time="323.185" type="appl" />
         <tli id="T505" time="323.586" type="appl" />
         <tli id="T506" time="323.988" type="appl" />
         <tli id="T507" time="325.072" type="appl" />
         <tli id="T975" time="325.94592960744046" />
         <tli id="T508" time="326.2525955806455" />
         <tli id="T509" time="326.728" type="appl" />
         <tli id="T976" time="328.06592481351026" />
         <tli id="T991" time="328.18592454215576" />
         <tli id="T990" time="328.98592273312545" />
         <tli id="T510" time="329.06592255222245" />
         <tli id="T977" time="329.83925413682647" />
         <tli id="T511" time="330.0792535941174" />
         <tli id="T512" time="330.281" type="appl" />
         <tli id="T513" time="330.726" type="appl" />
         <tli id="T514" time="331.17" type="appl" />
         <tli id="T515" time="333.1392466745767" />
         <tli id="T978" time="335.3459083513349" />
         <tli id="T516" time="335.8259072659168" />
         <tli id="T517" time="336.084" type="appl" />
         <tli id="T518" time="336.585" type="appl" />
         <tli id="T979" time="336.8355" type="intp" />
         <tli id="T519" time="337.086" type="appl" />
         <tli id="T520" time="337.442" type="appl" />
         <tli id="T521" time="337.783" type="appl" />
         <tli id="T522" time="338.125" type="appl" />
         <tli id="T523" time="338.466" type="appl" />
         <tli id="T980" time="338.77256726932205" />
         <tli id="T524" time="338.8325671336448" />
         <tli id="T525" time="339.115" type="appl" />
         <tli id="T526" time="339.407" type="appl" />
         <tli id="T981" time="339.6992318405287" />
         <tli id="T527" time="339.8392315239484" />
         <tli id="T528" time="340.218" type="appl" />
         <tli id="T529" time="340.732" type="appl" />
         <tli id="T982" time="341.4992277702106" />
         <tli id="T530" time="341.7192272727272" />
         <tli id="T531" time="341.994" type="appl" />
         <tli id="T532" time="342.529" type="appl" />
         <tli id="T533" time="343.064" type="appl" />
         <tli id="T534" time="343.599" type="appl" />
         <tli id="T983" time="344.239221574282" />
         <tli id="T535" time="346.45921655422296" />
         <tli id="T974" time="346.7525492242453" />
         <tli id="T536" time="346.901" type="appl" />
         <tli id="T537" time="347.422" type="appl" />
         <tli id="T538" time="347.943" type="appl" />
         <tli id="T539" time="348.465" type="appl" />
         <tli id="T540" time="348.986" type="appl" />
         <tli id="T541" time="349.507" type="appl" />
         <tli id="T542" time="350.028" type="appl" />
         <tli id="T543" time="350.805" type="appl" />
         <tli id="T544" time="351.206" type="appl" />
         <tli id="T545" time="351.606" type="appl" />
         <tli id="T546" time="352.006" type="appl" />
         <tli id="T547" time="352.406" type="appl" />
         <tli id="T548" time="352.807" type="appl" />
         <tli id="T549" time="353.207" type="appl" />
         <tli id="T550" time="353.565" type="appl" />
         <tli id="T551" time="353.92" type="appl" />
         <tli id="T552" time="354.275" type="appl" />
         <tli id="T553" time="354.629" type="appl" />
         <tli id="T554" time="354.984" type="appl" />
         <tli id="T555" time="355.339" type="appl" />
         <tli id="T556" time="355.694" type="appl" />
         <tli id="T557" time="356.777" type="appl" />
         <tli id="T558" time="357.527" type="appl" />
         <tli id="T559" time="358.278" type="appl" />
         <tli id="T560" time="359.583" type="appl" />
         <tli id="T561" time="360.053" type="appl" />
         <tli id="T562" time="360.524" type="appl" />
         <tli id="T563" time="360.994" type="appl" />
         <tli id="T564" time="361.464" type="appl" />
         <tli id="T565" time="363.018" type="appl" />
         <tli id="T566" time="363.541" type="appl" />
         <tli id="T567" time="364.063" type="appl" />
         <tli id="T568" time="364.586" type="appl" />
         <tli id="T569" time="365.108" type="appl" />
         <tli id="T570" time="366.944" type="appl" />
         <tli id="T571" time="367.366" type="appl" />
         <tli id="T572" time="367.787" type="appl" />
         <tli id="T573" time="368.208" type="appl" />
         <tli id="T574" time="368.629" type="appl" />
         <tli id="T575" time="369.051" type="appl" />
         <tli id="T576" time="369.472" type="appl" />
         <tli id="T577" time="370.138" type="appl" />
         <tli id="T578" time="370.799" type="appl" />
         <tli id="T579" time="371.439" type="appl" />
         <tli id="T580" time="371.637" type="appl" />
         <tli id="T581" time="371.834" type="appl" />
         <tli id="T582" time="375.436" type="appl" />
         <tli id="T583" time="375.79" type="appl" />
         <tli id="T584" time="376.143" type="appl" />
         <tli id="T585" time="376.496" type="appl" />
         <tli id="T586" time="376.849" type="appl" />
         <tli id="T587" time="377.203" type="appl" />
         <tli id="T588" time="377.556" type="appl" />
         <tli id="T589" time="378.908" type="appl" />
         <tli id="T590" time="379.284" type="appl" />
         <tli id="T591" time="379.66" type="appl" />
         <tli id="T592" time="380.036" type="appl" />
         <tli id="T593" time="380.412" type="appl" />
         <tli id="T594" time="380.788" type="appl" />
         <tli id="T595" time="381.164" type="appl" />
         <tli id="T596" time="382.168" type="appl" />
         <tli id="T597" time="382.611" type="appl" />
         <tli id="T598" time="383.053" type="appl" />
         <tli id="T599" time="383.495" type="appl" />
         <tli id="T600" time="383.938" type="appl" />
         <tli id="T601" time="384.38" type="appl" />
         <tli id="T602" time="384.822" type="appl" />
         <tli id="T603" time="385.265" type="appl" />
         <tli id="T604" time="385.707" type="appl" />
         <tli id="T605" time="386.281" type="appl" />
         <tli id="T606" time="386.847" type="appl" />
         <tli id="T967" time="387.1305" type="intp" />
         <tli id="T607" time="387.414" type="appl" />
         <tli id="T608" time="387.98" type="appl" />
         <tli id="T609" time="389.647" type="appl" />
         <tli id="T610" time="390.201" type="appl" />
         <tli id="T611" time="390.754" type="appl" />
         <tli id="T612" time="391.308" type="appl" />
         <tli id="T613" time="391.861" type="appl" />
         <tli id="T614" time="393.37" type="appl" />
         <tli id="T615" time="393.792" type="appl" />
         <tli id="T616" time="394.214" type="appl" />
         <tli id="T617" time="394.636" type="appl" />
         <tli id="T618" time="395.058" type="appl" />
         <tli id="T619" time="395.48" type="appl" />
         <tli id="T620" time="395.902" type="appl" />
         <tli id="T621" time="396.324" type="appl" />
         <tli id="T622" time="399.513" type="appl" />
         <tli id="T623" time="400.027" type="appl" />
         <tli id="T624" time="400.542" type="appl" />
         <tli id="T625" time="401.056" type="appl" />
         <tli id="T626" time="401.57" type="appl" />
         <tli id="T627" time="402.084" type="appl" />
         <tli id="T628" time="402.599" type="appl" />
         <tli id="T629" time="403.113" type="appl" />
         <tli id="T630" time="403.627" type="appl" />
         <tli id="T631" time="405.514" type="appl" />
         <tli id="T632" time="405.901" type="appl" />
         <tli id="T633" time="406.288" type="appl" />
         <tli id="T634" time="406.676" type="appl" />
         <tli id="T635" time="407.063" type="appl" />
         <tli id="T636" time="407.45" type="appl" />
         <tli id="T984" time="407.8825" type="intp" />
         <tli id="T637" time="408.315" type="appl" />
         <tli id="T638" time="408.867" type="appl" />
         <tli id="T639" time="409.246" type="appl" />
         <tli id="T640" time="409.625" type="appl" />
         <tli id="T641" time="410.004" type="appl" />
         <tli id="T642" time="410.732" type="appl" />
         <tli id="T643" time="411.128" type="appl" />
         <tli id="T644" time="411.523" type="appl" />
         <tli id="T645" time="411.919" type="appl" />
         <tli id="T646" time="412.314" type="appl" />
         <tli id="T647" time="412.709" type="appl" />
         <tli id="T648" time="413.105" type="appl" />
         <tli id="T649" time="413.5" type="appl" />
         <tli id="T650" time="413.896" type="appl" />
         <tli id="T651" time="414.291" type="appl" />
         <tli id="T652" time="414.914" type="appl" />
         <tli id="T653" time="415.427" type="appl" />
         <tli id="T654" time="415.939" type="appl" />
         <tli id="T655" time="416.451" type="appl" />
         <tli id="T656" time="420.481" type="appl" />
         <tli id="T657" time="421.031" type="appl" />
         <tli id="T658" time="421.581" type="appl" />
         <tli id="T659" time="422.132" type="appl" />
         <tli id="T660" time="422.682" type="appl" />
         <tli id="T661" time="423.232" type="appl" />
         <tli id="T662" time="423.782" type="appl" />
         <tli id="T663" time="424.171" type="appl" />
         <tli id="T664" time="424.523" type="appl" />
         <tli id="T665" time="424.874" type="appl" />
         <tli id="T666" time="425.225" type="appl" />
         <tli id="T667" time="425.577" type="appl" />
         <tli id="T668" time="425.928" type="appl" />
         <tli id="T669" time="426.495" type="appl" />
         <tli id="T670" time="426.96" type="appl" />
         <tli id="T671" time="427.426" type="appl" />
         <tli id="T672" time="428.672" type="appl" />
         <tli id="T673" time="429.194" type="appl" />
         <tli id="T674" time="429.716" type="appl" />
         <tli id="T675" time="430.237" type="appl" />
         <tli id="T676" time="430.72" type="appl" />
         <tli id="T677" time="431.171" type="appl" />
         <tli id="T678" time="431.622" type="appl" />
         <tli id="T679" time="432.073" type="appl" />
         <tli id="T680" time="432.524" type="appl" />
         <tli id="T681" time="432.975" type="appl" />
         <tli id="T682" time="433.426" type="appl" />
         <tli id="T683" time="433.955" type="appl" />
         <tli id="T684" time="434.39" type="appl" />
         <tli id="T685" time="434.824" type="appl" />
         <tli id="T686" time="435.258" type="appl" />
         <tli id="T687" time="435.693" type="appl" />
         <tli id="T688" time="436.127" type="appl" />
         <tli id="T689" time="439.556" type="appl" />
         <tli id="T690" time="440.22" type="appl" />
         <tli id="T691" time="440.664" type="appl" />
         <tli id="T692" time="441.107" type="appl" />
         <tli id="T693" time="441.551" type="appl" />
         <tli id="T694" time="441.994" type="appl" />
         <tli id="T695" time="442.843" type="appl" />
         <tli id="T696" time="443.646" type="appl" />
         <tli id="T697" time="444.45" type="appl" />
         <tli id="T698" time="445.253" type="appl" />
         <tli id="T699" time="445.709" type="appl" />
         <tli id="T700" time="446.127" type="appl" />
         <tli id="T701" time="446.546" type="appl" />
         <tli id="T702" time="446.964" type="appl" />
         <tli id="T703" time="447.382" type="appl" />
         <tli id="T704" time="447.8" type="appl" />
         <tli id="T705" time="448.652" type="appl" />
         <tli id="T706" time="449.378" type="appl" />
         <tli id="T707" time="451.322" type="appl" />
         <tli id="T708" time="451.867" type="appl" />
         <tli id="T709" time="452.412" type="appl" />
         <tli id="T710" time="452.957" type="appl" />
         <tli id="T711" time="453.502" type="appl" />
         <tli id="T712" time="454.048" type="appl" />
         <tli id="T713" time="454.593" type="appl" />
         <tli id="T714" time="455.138" type="appl" />
         <tli id="T715" time="455.683" type="appl" />
         <tli id="T716" time="456.228" type="appl" />
         <tli id="T717" time="456.773" type="appl" />
         <tli id="T718" time="457.318" type="appl" />
         <tli id="T719" time="457.937" type="appl" />
         <tli id="T720" time="458.386" type="appl" />
         <tli id="T721" time="458.834" type="appl" />
         <tli id="T722" time="459.283" type="appl" />
         <tli id="T723" time="460.834" type="appl" />
         <tli id="T724" time="461.484" type="appl" />
         <tli id="T725" time="462.134" type="appl" />
         <tli id="T726" time="462.784" type="appl" />
         <tli id="T727" time="463.435" type="appl" />
         <tli id="T728" time="464.085" type="appl" />
         <tli id="T729" time="464.735" type="appl" />
         <tli id="T730" time="465.385" type="appl" />
         <tli id="T731" time="469.164" type="appl" />
         <tli id="T732" time="469.732" type="appl" />
         <tli id="T733" time="470.183" type="appl" />
         <tli id="T734" time="470.634" type="appl" />
         <tli id="T735" time="471.084" type="appl" />
         <tli id="T736" time="471.535" type="appl" />
         <tli id="T737" time="471.986" type="appl" />
         <tli id="T738" time="472.437" type="appl" />
         <tli id="T739" time="472.888" type="appl" />
         <tli id="T740" time="473.339" type="appl" />
         <tli id="T741" time="473.789" type="appl" />
         <tli id="T742" time="474.24" type="appl" />
         <tli id="T743" time="474.691" type="appl" />
         <tli id="T744" time="475.142" type="appl" />
         <tli id="T745" time="477.396" type="appl" />
         <tli id="T746" time="477.743" type="appl" />
         <tli id="T747" time="478.089" type="appl" />
         <tli id="T748" time="478.436" type="appl" />
         <tli id="T749" time="478.782" type="appl" />
         <tli id="T750" time="479.129" type="appl" />
         <tli id="T751" time="479.475" type="appl" />
         <tli id="T752" time="479.822" type="appl" />
         <tli id="T753" time="480.168" type="appl" />
         <tli id="T754" time="481.174" type="appl" />
         <tli id="T755" time="481.655" type="appl" />
         <tli id="T756" time="482.135" type="appl" />
         <tli id="T757" time="482.616" type="appl" />
         <tli id="T758" time="483.097" type="appl" />
         <tli id="T759" time="483.578" type="appl" />
         <tli id="T760" time="484.059" type="appl" />
         <tli id="T761" time="484.539" type="appl" />
         <tli id="T762" time="485.02" type="appl" />
         <tli id="T763" time="485.501" type="appl" />
         <tli id="T764" time="486.224" type="appl" />
         <tli id="T765" time="486.75" type="appl" />
         <tli id="T766" time="487.275" type="appl" />
         <tli id="T767" time="487.801" type="appl" />
         <tli id="T768" time="488.326" type="appl" />
         <tli id="T769" time="488.852" type="appl" />
         <tli id="T770" time="489.377" type="appl" />
         <tli id="T771" time="492.446" type="appl" />
         <tli id="T772" time="492.98" type="appl" />
         <tli id="T773" time="493.513" type="appl" />
         <tli id="T774" time="495.383" type="appl" />
         <tli id="T775" time="495.789" type="appl" />
         <tli id="T776" time="496.194" type="appl" />
         <tli id="T777" time="496.6" type="appl" />
         <tli id="T778" time="497.005" type="appl" />
         <tli id="T779" time="497.951" type="appl" />
         <tli id="T780" time="498.362" type="appl" />
         <tli id="T781" time="498.774" type="appl" />
         <tli id="T782" time="499.185" type="appl" />
         <tli id="T783" time="499.596" type="appl" />
         <tli id="T784" time="500.008" type="appl" />
         <tli id="T785" time="500.419" type="appl" />
         <tli id="T786" time="500.83" type="appl" />
         <tli id="T787" time="501.241" type="appl" />
         <tli id="T788" time="501.653" type="appl" />
         <tli id="T789" time="502.064" type="appl" />
         <tli id="T790" time="502.475" type="appl" />
         <tli id="T791" time="503.845" type="appl" />
         <tli id="T792" time="504.285" type="appl" />
         <tli id="T793" time="504.726" type="appl" />
         <tli id="T794" time="505.167" type="appl" />
         <tli id="T795" time="505.607" type="appl" />
         <tli id="T796" time="506.048" type="appl" />
         <tli id="T797" time="507.384" type="appl" />
         <tli id="T798" time="507.988" type="appl" />
         <tli id="T799" time="508.592" type="appl" />
         <tli id="T800" time="509.195" type="appl" />
         <tli id="T801" time="509.799" type="appl" />
         <tli id="T802" time="510.403" type="appl" />
         <tli id="T803" time="511.007" type="appl" />
         <tli id="T804" time="511.735" type="appl" />
         <tli id="T805" time="512.24" type="appl" />
         <tli id="T806" time="512.744" type="appl" />
         <tli id="T807" time="513.248" type="appl" />
         <tli id="T808" time="513.753" type="appl" />
         <tli id="T809" time="514.257" type="appl" />
         <tli id="T810" time="514.762" type="appl" />
         <tli id="T811" time="515.266" type="appl" />
         <tli id="T812" time="515.88" type="appl" />
         <tli id="T813" time="516.28" type="appl" />
         <tli id="T814" time="521.678" type="appl" />
         <tli id="T815" time="522.043" type="appl" />
         <tli id="T816" time="522.408" type="appl" />
         <tli id="T817" time="522.773" type="appl" />
         <tli id="T818" time="523.138" type="appl" />
         <tli id="T819" time="523.503" type="appl" />
         <tli id="T820" time="523.868" type="appl" />
         <tli id="T821" time="524.233" type="appl" />
         <tli id="T822" time="524.598" type="appl" />
         <tli id="T823" time="524.963" type="appl" />
         <tli id="T824" time="525.328" type="appl" />
         <tli id="T825" time="525.693" type="appl" />
         <tli id="T826" time="526.058" type="appl" />
         <tli id="T827" time="526.423" type="appl" />
         <tli id="T828" time="526.788" type="appl" />
         <tli id="T829" time="527.153" type="appl" />
         <tli id="T830" time="527.518" type="appl" />
         <tli id="T831" time="529.492" type="appl" />
         <tli id="T832" time="529.996" type="appl" />
         <tli id="T833" time="530.501" type="appl" />
         <tli id="T834" time="531.005" type="appl" />
         <tli id="T835" time="531.509" type="appl" />
         <tli id="T836" time="532.013" type="appl" />
         <tli id="T837" time="532.543" type="appl" />
         <tli id="T838" time="533.051" type="appl" />
         <tli id="T839" time="533.558" type="appl" />
         <tli id="T840" time="534.066" type="appl" />
         <tli id="T841" time="534.574" type="appl" />
         <tli id="T842" time="535.082" type="appl" />
         <tli id="T843" time="535.59" type="appl" />
         <tli id="T844" time="536.098" type="appl" />
         <tli id="T845" time="536.605" type="appl" />
         <tli id="T846" time="537.113" type="appl" />
         <tli id="T847" time="537.621" type="appl" />
         <tli id="T848" time="539.074" type="appl" />
         <tli id="T849" time="539.71" type="appl" />
         <tli id="T850" time="541.252" type="appl" />
         <tli id="T851" time="541.658" type="appl" />
         <tli id="T852" time="542.065" type="appl" />
         <tli id="T853" time="542.471" type="appl" />
         <tli id="T854" time="542.878" type="appl" />
         <tli id="T855" time="544.332" type="appl" />
         <tli id="T856" time="544.77" type="appl" />
         <tli id="T857" time="545.207" type="appl" />
         <tli id="T858" time="545.645" type="appl" />
         <tli id="T859" time="549.685" type="appl" />
         <tli id="T860" time="550.256" type="appl" />
         <tli id="T861" time="550.826" type="appl" />
         <tli id="T862" time="552.317" type="appl" />
         <tli id="T863" time="552.798" type="appl" />
         <tli id="T864" time="553.28" type="appl" />
         <tli id="T865" time="553.762" type="appl" />
         <tli id="T866" time="554.378" type="appl" />
         <tli id="T867" time="554.753" type="appl" />
         <tli id="T868" time="555.129" type="appl" />
         <tli id="T869" time="555.504" type="appl" />
         <tli id="T870" time="555.88" type="appl" />
         <tli id="T871" time="556.256" type="appl" />
         <tli id="T872" time="556.631" type="appl" />
         <tli id="T873" time="557.007" type="appl" />
         <tli id="T874" time="557.382" type="appl" />
         <tli id="T875" time="557.758" type="appl" />
         <tli id="T876" time="560.822" type="appl" />
         <tli id="T877" time="561.246" type="appl" />
         <tli id="T878" time="561.67" type="appl" />
         <tli id="T879" time="562.094" type="appl" />
         <tli id="T880" time="562.517" type="appl" />
         <tli id="T881" time="562.941" type="appl" />
         <tli id="T882" time="563.365" type="appl" />
         <tli id="T883" time="563.789" type="appl" />
         <tli id="T884" time="564.213" type="appl" />
         <tli id="T885" time="566.617" type="appl" />
         <tli id="T886" time="567.035" type="appl" />
         <tli id="T887" time="567.453" type="appl" />
         <tli id="T888" time="567.872" type="appl" />
         <tli id="T889" time="568.29" type="appl" />
         <tli id="T890" time="568.708" type="appl" />
         <tli id="T891" time="569.126" type="appl" />
         <tli id="T892" time="570.878" type="appl" />
         <tli id="T893" time="571.334" type="appl" />
         <tli id="T894" time="571.791" type="appl" />
         <tli id="T895" time="572.248" type="appl" />
         <tli id="T896" time="572.705" type="appl" />
         <tli id="T897" time="573.161" type="appl" />
         <tli id="T898" time="573.618" type="appl" />
         <tli id="T899" time="574.882" type="appl" />
         <tli id="T900" time="575.243" type="appl" />
         <tli id="T901" time="575.604" type="appl" />
         <tli id="T902" time="575.965" type="appl" />
         <tli id="T903" time="576.326" type="appl" />
         <tli id="T904" time="576.687" type="appl" />
         <tli id="T905" time="577.048" type="appl" />
         <tli id="T906" time="583.483" type="appl" />
         <tli id="T907" time="583.86" type="appl" />
         <tli id="T908" time="584.221" type="appl" />
         <tli id="T909" time="584.583" type="appl" />
         <tli id="T910" time="584.944" type="appl" />
         <tli id="T911" time="585.305" type="appl" />
         <tli id="T912" time="586.323" type="appl" />
         <tli id="T985" time="586.775" type="intp" />
         <tli id="T913" time="587.227" type="appl" />
         <tli id="T914" time="587.889" type="appl" />
         <tli id="T986" time="588.2065" type="intp" />
         <tli id="T915" time="588.524" type="appl" />
         <tli id="T916" time="589.382" type="appl" />
         <tli id="T987" time="589.7484999999999" type="intp" />
         <tli id="T917" time="590.115" type="appl" />
         <tli id="T918" time="590.603" type="appl" />
         <tli id="T919" time="591.063" type="appl" />
         <tli id="T920" time="591.524" type="appl" />
         <tli id="T921" time="591.985" type="appl" />
         <tli id="T922" time="592.446" type="appl" />
         <tli id="T923" time="592.906" type="appl" />
         <tli id="T924" time="593.367" type="appl" />
         <tli id="T925" time="593.828" type="appl" />
         <tli id="T926" time="594.289" type="appl" />
         <tli id="T927" time="594.749" type="appl" />
         <tli id="T928" time="595.21" type="appl" />
         <tli id="T929" time="595.671" type="appl" />
         <tli id="T930" time="596.132" type="appl" />
         <tli id="T931" time="596.592" type="appl" />
         <tli id="T932" time="597.053" type="appl" />
         <tli id="T933" time="599.562" type="appl" />
         <tli id="T934" time="600.253" type="appl" />
         <tli id="T935" time="600.944" type="appl" />
         <tli id="T936" time="601.634" type="appl" />
         <tli id="T937" time="603.223" type="appl" />
         <tli id="T938" time="603.7" type="appl" />
         <tli id="T939" time="604.178" type="appl" />
         <tli id="T940" time="604.656" type="appl" />
         <tli id="T941" time="605.134" type="appl" />
         <tli id="T942" time="605.612" type="appl" />
         <tli id="T943" time="606.089" type="appl" />
         <tli id="T944" time="606.567" type="appl" />
         <tli id="T945" time="608.348" type="appl" />
         <tli id="T946" time="608.789" type="appl" />
         <tli id="T947" time="609.23" type="appl" />
         <tli id="T948" time="609.671" type="appl" />
         <tli id="T949" time="610.112" type="appl" />
         <tli id="T950" time="614.432" type="appl" />
         <tli id="T951" time="614.782" type="appl" />
         <tli id="T952" time="615.131" type="appl" />
         <tli id="T953" time="615.481" type="appl" />
         <tli id="T954" time="615.83" type="appl" />
         <tli id="T955" time="616.18" type="appl" />
         <tli id="T988" time="616.3544999999999" type="intp" />
         <tli id="T956" time="616.529" type="appl" />
         <tli id="T957" time="616.787" type="appl" />
         <tli id="T958" time="617.04" type="appl" />
         <tli id="T959" time="617.293" type="appl" />
         <tli id="T960" time="617.546" type="appl" />
         <tli id="T989" time="617.6725" type="intp" />
         <tli id="T961" time="617.799" type="appl" />
         <tli id="T962" time="618.221" type="appl" />
         <tli id="T963" time="618.642" type="appl" />
         <tli id="T964" time="619.064" type="appl" />
         <tli id="T965" time="624.74" type="appl" />
         <tli id="T966" time="626.485" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="NEP"
                      type="t">
         <timeline-fork end="T968" start="T0">
            <tli id="T0.tx.1" />
            <tli id="T0.tx.2" />
            <tli id="T0.tx.3" />
         </timeline-fork>
         <timeline-fork end="T969" start="T19">
            <tli id="T19.tx.1" />
            <tli id="T19.tx.2" />
            <tli id="T19.tx.3" />
            <tli id="T19.tx.4" />
            <tli id="T19.tx.5" />
            <tli id="T19.tx.6" />
            <tli id="T19.tx.7" />
            <tli id="T19.tx.8" />
         </timeline-fork>
         <timeline-fork end="T515" start="T514">
            <tli id="T514.tx.1" />
            <tli id="T514.tx.2" />
            <tli id="T514.tx.3" />
         </timeline-fork>
         <timeline-fork end="T978" start="T515">
            <tli id="T515.tx.1" />
            <tli id="T515.tx.2" />
            <tli id="T515.tx.3" />
            <tli id="T515.tx.4" />
            <tli id="T515.tx.5" />
         </timeline-fork>
         <timeline-fork end="T533" start="T532">
            <tli id="T532.tx.1" />
         </timeline-fork>
         <timeline-fork end="T984" start="T636">
            <tli id="T636.tx.1" />
         </timeline-fork>
         <timeline-fork end="T965" start="T964">
            <tli id="T964.tx.1" />
            <tli id="T964.tx.2" />
            <tli id="T964.tx.3" />
            <tli id="T964.tx.4" />
            <tli id="T964.tx.5" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T965" id="Seg_0" n="sc" s="T0">
               <ts e="T968" id="Seg_2" n="HIAT:u" s="T0">
                  <nts id="Seg_3" n="HIAT:ip">(</nts>
                  <nts id="Seg_4" n="HIAT:ip">(</nts>
                  <ats e="T0.tx.1" id="Seg_5" n="HIAT:non-pho" s="T0">KuAI:</ats>
                  <nts id="Seg_6" n="HIAT:ip">)</nts>
                  <nts id="Seg_7" n="HIAT:ip">)</nts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx.2" id="Seg_10" n="HIAT:w" s="T0.tx.1">Лентяю</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx.3" id="Seg_13" n="HIAT:w" s="T0.tx.2">всегда</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T968" id="Seg_16" n="HIAT:w" s="T0.tx.3">праздник</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T6" id="Seg_20" n="HIAT:u" s="T968">
                  <nts id="Seg_21" n="HIAT:ip">(</nts>
                  <nts id="Seg_22" n="HIAT:ip">(</nts>
                  <ats e="T1" id="Seg_23" n="HIAT:non-pho" s="T968">NEP:</ats>
                  <nts id="Seg_24" n="HIAT:ip">)</nts>
                  <nts id="Seg_25" n="HIAT:ip">)</nts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_27" n="HIAT:ip">(</nts>
                  <nts id="Seg_28" n="HIAT:ip">(</nts>
                  <ats e="T2" id="Seg_29" n="HIAT:non-pho" s="T1">…</ats>
                  <nts id="Seg_30" n="HIAT:ip">)</nts>
                  <nts id="Seg_31" n="HIAT:ip">)</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_34" n="HIAT:w" s="T2">Ukkɨr</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_37" n="HIAT:w" s="T3">ijatɨ</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_40" n="HIAT:w" s="T4">ɛppantɨ</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_42" n="HIAT:ip">(</nts>
                  <nts id="Seg_43" n="HIAT:ip">(</nts>
                  <ats e="T6" id="Seg_44" n="HIAT:non-pho" s="T5">…</ats>
                  <nts id="Seg_45" n="HIAT:ip">)</nts>
                  <nts id="Seg_46" n="HIAT:ip">)</nts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_50" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_52" n="HIAT:w" s="T6">Na</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_55" n="HIAT:w" s="T7">qos</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_58" n="HIAT:w" s="T8">ket</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_60" n="HIAT:ip">(</nts>
                  <nts id="Seg_61" n="HIAT:ip">(</nts>
                  <ats e="T10" id="Seg_62" n="HIAT:non-pho" s="T9">…</ats>
                  <nts id="Seg_63" n="HIAT:ip">)</nts>
                  <nts id="Seg_64" n="HIAT:ip">)</nts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_67" n="HIAT:w" s="T10">qət</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_70" n="HIAT:w" s="T11">kətɨkak</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_73" n="HIAT:w" s="T12">namɨp</ts>
                  <nts id="Seg_74" n="HIAT:ip">.</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_77" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_79" n="HIAT:w" s="T13">Na</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_82" n="HIAT:w" s="T14">tɛntɨlʼ</ts>
                  <nts id="Seg_83" n="HIAT:ip">,</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_86" n="HIAT:w" s="T15">na</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_89" n="HIAT:w" s="T16">tɛntɨlʼ</ts>
                  <nts id="Seg_90" n="HIAT:ip">,</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_93" n="HIAT:w" s="T17">ašša</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_96" n="HIAT:w" s="T18">čʼaptä</ts>
                  <nts id="Seg_97" n="HIAT:ip">.</nts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T969" id="Seg_100" n="HIAT:u" s="T19">
                  <nts id="Seg_101" n="HIAT:ip">(</nts>
                  <nts id="Seg_102" n="HIAT:ip">(</nts>
                  <ats e="T19.tx.1" id="Seg_103" n="HIAT:non-pho" s="T19">KuAI:</ats>
                  <nts id="Seg_104" n="HIAT:ip">)</nts>
                  <nts id="Seg_105" n="HIAT:ip">)</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19.tx.2" id="Seg_108" n="HIAT:w" s="T19.tx.1">Подождите</ts>
                  <nts id="Seg_109" n="HIAT:ip">,</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19.tx.3" id="Seg_112" n="HIAT:w" s="T19.tx.2">пусть</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19.tx.4" id="Seg_115" n="HIAT:w" s="T19.tx.3">она</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19.tx.5" id="Seg_118" n="HIAT:w" s="T19.tx.4">подождёт</ts>
                  <nts id="Seg_119" n="HIAT:ip">,</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19.tx.6" id="Seg_122" n="HIAT:w" s="T19.tx.5">я</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19.tx.7" id="Seg_125" n="HIAT:w" s="T19.tx.6">попробую</ts>
                  <nts id="Seg_126" n="HIAT:ip">,</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19.tx.8" id="Seg_129" n="HIAT:w" s="T19.tx.7">посмотрю</ts>
                  <nts id="Seg_130" n="HIAT:ip">,</nts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T969" id="Seg_133" n="HIAT:w" s="T19.tx.8">проверю</ts>
                  <nts id="Seg_134" n="HIAT:ip">.</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T970" id="Seg_137" n="HIAT:u" s="T969">
                  <nts id="Seg_138" n="HIAT:ip">(</nts>
                  <nts id="Seg_139" n="HIAT:ip">(</nts>
                  <ats e="T20" id="Seg_140" n="HIAT:non-pho" s="T969">NN:</ats>
                  <nts id="Seg_141" n="HIAT:ip">)</nts>
                  <nts id="Seg_142" n="HIAT:ip">)</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_145" n="HIAT:w" s="T20">Tan</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_148" n="HIAT:w" s="T21">ılqɨt</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_150" n="HIAT:ip">(</nts>
                  <nts id="Seg_151" n="HIAT:ip">(</nts>
                  <ats e="T23" id="Seg_152" n="HIAT:non-pho" s="T22">…</ats>
                  <nts id="Seg_153" n="HIAT:ip">)</nts>
                  <nts id="Seg_154" n="HIAT:ip">)</nts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_157" n="HIAT:w" s="T23">šʼolqumɨt</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_160" n="HIAT:w" s="T24">näčʼčʼa</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_162" n="HIAT:ip">(</nts>
                  <nts id="Seg_163" n="HIAT:ip">(</nts>
                  <ats e="T970" id="Seg_164" n="HIAT:non-pho" s="T25">…</ats>
                  <nts id="Seg_165" n="HIAT:ip">)</nts>
                  <nts id="Seg_166" n="HIAT:ip">)</nts>
                  <nts id="Seg_167" n="HIAT:ip">.</nts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T971" id="Seg_170" n="HIAT:u" s="T970">
                  <nts id="Seg_171" n="HIAT:ip">(</nts>
                  <nts id="Seg_172" n="HIAT:ip">(</nts>
                  <ats e="T26" id="Seg_173" n="HIAT:non-pho" s="T970">NN:</ats>
                  <nts id="Seg_174" n="HIAT:ip">)</nts>
                  <nts id="Seg_175" n="HIAT:ip">)</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_178" n="HIAT:w" s="T26">Tam</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_181" n="HIAT:w" s="T27">na</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_184" n="HIAT:w" s="T28">šintɨ</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_187" n="HIAT:w" s="T29">qolʼčʼanta</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_189" n="HIAT:ip">(</nts>
                  <nts id="Seg_190" n="HIAT:ip">(</nts>
                  <ats e="T31" id="Seg_191" n="HIAT:non-pho" s="T30">…</ats>
                  <nts id="Seg_192" n="HIAT:ip">)</nts>
                  <nts id="Seg_193" n="HIAT:ip">)</nts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_196" n="HIAT:w" s="T31">tan</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_199" n="HIAT:w" s="T32">əːtal</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T971" id="Seg_202" n="HIAT:w" s="T33">utrejqɨjojčʼimpät</ts>
                  <nts id="Seg_203" n="HIAT:ip">.</nts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T972" id="Seg_206" n="HIAT:u" s="T971">
                  <nts id="Seg_207" n="HIAT:ip">(</nts>
                  <nts id="Seg_208" n="HIAT:ip">(</nts>
                  <ats e="T34" id="Seg_209" n="HIAT:non-pho" s="T971">NN:</ats>
                  <nts id="Seg_210" n="HIAT:ip">)</nts>
                  <nts id="Seg_211" n="HIAT:ip">)</nts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T972" id="Seg_214" n="HIAT:w" s="T34">Tuttaltät</ts>
                  <nts id="Seg_215" n="HIAT:ip">.</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T973" id="Seg_218" n="HIAT:u" s="T972">
                  <nts id="Seg_219" n="HIAT:ip">(</nts>
                  <nts id="Seg_220" n="HIAT:ip">(</nts>
                  <ats e="T35" id="Seg_221" n="HIAT:non-pho" s="T972">NN:</ats>
                  <nts id="Seg_222" n="HIAT:ip">)</nts>
                  <nts id="Seg_223" n="HIAT:ip">)</nts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_226" n="HIAT:w" s="T35">Čʼontɨkak</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T973" id="Seg_229" n="HIAT:w" s="T36">kətätɨ</ts>
                  <nts id="Seg_230" n="HIAT:ip">.</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_233" n="HIAT:u" s="T973">
                  <nts id="Seg_234" n="HIAT:ip">(</nts>
                  <nts id="Seg_235" n="HIAT:ip">(</nts>
                  <ats e="T37" id="Seg_236" n="HIAT:non-pho" s="T973">NEP:</ats>
                  <nts id="Seg_237" n="HIAT:ip">)</nts>
                  <nts id="Seg_238" n="HIAT:ip">)</nts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_241" n="HIAT:w" s="T37">Pija</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_244" n="HIAT:w" s="T38">na</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_247" n="HIAT:w" s="T39">ira</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_250" n="HIAT:w" s="T40">ilɨmpan</ts>
                  <nts id="Seg_251" n="HIAT:ip">.</nts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_254" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_256" n="HIAT:w" s="T41">Qütalpa</ts>
                  <nts id="Seg_257" n="HIAT:ip">.</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_260" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_262" n="HIAT:w" s="T42">Na</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_265" n="HIAT:w" s="T43">šʼojqumɨj</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_268" n="HIAT:w" s="T44">ira</ts>
                  <nts id="Seg_269" n="HIAT:ip">.</nts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_272" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_274" n="HIAT:w" s="T45">Tapɨlʼalʼ</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_277" n="HIAT:w" s="T46">pɛlalʼ</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_280" n="HIAT:w" s="T47">kortɨ</ts>
                  <nts id="Seg_281" n="HIAT:ip">,</nts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_284" n="HIAT:w" s="T48">tɔːlʼ</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_287" n="HIAT:w" s="T49">takkɨlʼelʼ</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_290" n="HIAT:w" s="T50">qolt</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_293" n="HIAT:w" s="T51">pɛlakqɨlʼ</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_296" n="HIAT:w" s="T52">ilɨmpa</ts>
                  <nts id="Seg_297" n="HIAT:ip">.</nts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T992" id="Seg_300" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_302" n="HIAT:w" s="T53">Toːqɨlʼ</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_305" n="HIAT:w" s="T54">təp</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_308" n="HIAT:w" s="T55">poːkɨtɨlʼ</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_311" n="HIAT:w" s="T56">təttɨt</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_313" n="HIAT:ip">(</nts>
                  <ts e="T58" id="Seg_315" n="HIAT:w" s="T57">tošʼa</ts>
                  <nts id="Seg_316" n="HIAT:ip">)</nts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T992" id="Seg_319" n="HIAT:w" s="T58">qaj</ts>
                  <nts id="Seg_320" n="HIAT:ip">.</nts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_323" n="HIAT:u" s="T992">
                  <nts id="Seg_324" n="HIAT:ip">(</nts>
                  <nts id="Seg_325" n="HIAT:ip">(</nts>
                  <ats e="T59" id="Seg_326" n="HIAT:non-pho" s="T992">…</ats>
                  <nts id="Seg_327" n="HIAT:ip">)</nts>
                  <nts id="Seg_328" n="HIAT:ip">)</nts>
                  <nts id="Seg_329" n="HIAT:ip">.</nts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_332" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_334" n="HIAT:w" s="T59">Ilɨmpa</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_337" n="HIAT:w" s="T60">tına</ts>
                  <nts id="Seg_338" n="HIAT:ip">.</nts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_341" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_343" n="HIAT:w" s="T61">Qütalpa</ts>
                  <nts id="Seg_344" n="HIAT:ip">.</nts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_347" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_349" n="HIAT:w" s="T62">Ijatɨ</ts>
                  <nts id="Seg_350" n="HIAT:ip">,</nts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_353" n="HIAT:w" s="T63">ukkɨr</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_356" n="HIAT:w" s="T64">ijatɨ</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_359" n="HIAT:w" s="T65">pɛläkɨtɨj</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_362" n="HIAT:w" s="T66">neːkɨtɨj</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_365" n="HIAT:w" s="T67">pɛlätɨ</ts>
                  <nts id="Seg_366" n="HIAT:ip">.</nts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_369" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_371" n="HIAT:w" s="T68">Nʼi</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_374" n="HIAT:w" s="T69">nälatɨ</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_377" n="HIAT:w" s="T70">čʼäŋka</ts>
                  <nts id="Seg_378" n="HIAT:ip">,</nts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_381" n="HIAT:w" s="T71">nʼi</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_384" n="HIAT:w" s="T72">qajtɨ</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_387" n="HIAT:w" s="T73">čʼäŋka</ts>
                  <nts id="Seg_388" n="HIAT:ip">.</nts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_391" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_393" n="HIAT:w" s="T74">Nɨn</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_396" n="HIAT:w" s="T75">na</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_399" n="HIAT:w" s="T76">əsɨtɨ</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_402" n="HIAT:w" s="T77">qütalna</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_405" n="HIAT:w" s="T78">na</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_408" n="HIAT:w" s="T79">ijalʼa</ts>
                  <nts id="Seg_409" n="HIAT:ip">,</nts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_411" n="HIAT:ip">(</nts>
                  <ts e="T81" id="Seg_413" n="HIAT:w" s="T80">naintɨ</ts>
                  <nts id="Seg_414" n="HIAT:ip">)</nts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_417" n="HIAT:w" s="T81">na</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_420" n="HIAT:w" s="T82">ijan</ts>
                  <nts id="Seg_421" n="HIAT:ip">.</nts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_424" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_426" n="HIAT:w" s="T83">Nɨmtɨ</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_429" n="HIAT:w" s="T84">ila</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_432" n="HIAT:w" s="T85">qapij</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_435" n="HIAT:w" s="T86">na</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_438" n="HIAT:w" s="T87">əsɨntɨ</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_441" n="HIAT:w" s="T88">əmɨntɨ</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_444" n="HIAT:w" s="T89">qamqɨn</ts>
                  <nts id="Seg_445" n="HIAT:ip">.</nts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_448" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_450" n="HIAT:w" s="T90">Toːntɨ</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_453" n="HIAT:w" s="T91">tına</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_456" n="HIAT:w" s="T92">šʼittɨ</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_459" n="HIAT:w" s="T93">qälɨ</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_462" n="HIAT:w" s="T94">timnʼäsɨqı</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_465" n="HIAT:w" s="T95">ɛːppɨntɔːqı</ts>
                  <nts id="Seg_466" n="HIAT:ip">.</nts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_469" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_471" n="HIAT:w" s="T96">Qälɨ</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_474" n="HIAT:w" s="T97">ira</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_477" n="HIAT:w" s="T98">kučʼčʼa</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_480" n="HIAT:w" s="T99">ɛːnta</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_482" n="HIAT:ip">(</nts>
                  <ts e="T101" id="Seg_484" n="HIAT:w" s="T100">tıː</ts>
                  <nts id="Seg_485" n="HIAT:ip">)</nts>
                  <nts id="Seg_486" n="HIAT:ip">?</nts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_489" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_491" n="HIAT:w" s="T101">Qaj</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_494" n="HIAT:w" s="T102">ila</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_497" n="HIAT:w" s="T103">kučʼčʼä</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_500" n="HIAT:w" s="T104">izhe</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_503" n="HIAT:w" s="T105">tıntena</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_506" n="HIAT:w" s="T106">na</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_509" n="HIAT:w" s="T107">ira</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_512" n="HIAT:w" s="T108">ılla</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_515" n="HIAT:w" s="T109">quŋa</ts>
                  <nts id="Seg_516" n="HIAT:ip">.</nts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_519" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_521" n="HIAT:w" s="T110">İlla</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_524" n="HIAT:w" s="T111">quŋa</ts>
                  <nts id="Seg_525" n="HIAT:ip">,</nts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_528" n="HIAT:w" s="T112">tına</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_531" n="HIAT:w" s="T113">nɨn</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_534" n="HIAT:w" s="T114">šittal</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_537" n="HIAT:w" s="T115">tına</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_540" n="HIAT:w" s="T116">ija</ts>
                  <nts id="Seg_541" n="HIAT:ip">,</nts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_544" n="HIAT:w" s="T117">qälɨ</ts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_547" n="HIAT:w" s="T118">ira</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_550" n="HIAT:w" s="T119">nɔːtɨ</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_553" n="HIAT:w" s="T120">kučʼčʼa</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_556" n="HIAT:w" s="T121">qatɛnta</ts>
                  <nts id="Seg_557" n="HIAT:ip">?</nts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_560" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_562" n="HIAT:w" s="T122">Toː</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_565" n="HIAT:w" s="T123">tɨmnʼantɨn</ts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_568" n="HIAT:w" s="T124">tulɨnʼnʼa</ts>
                  <nts id="Seg_569" n="HIAT:ip">.</nts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_572" n="HIAT:u" s="T125">
                  <ts e="T126" id="Seg_574" n="HIAT:w" s="T125">Ira</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_577" n="HIAT:w" s="T126">qumpa</ts>
                  <nts id="Seg_578" n="HIAT:ip">.</nts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T129" id="Seg_581" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_583" n="HIAT:w" s="T127">İlla</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_586" n="HIAT:w" s="T128">meːŋɔːt</ts>
                  <nts id="Seg_587" n="HIAT:ip">.</nts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T138" id="Seg_590" n="HIAT:u" s="T129">
                  <ts e="T130" id="Seg_592" n="HIAT:w" s="T129">Šittal</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_595" n="HIAT:w" s="T130">imaqotatɨ</ts>
                  <nts id="Seg_596" n="HIAT:ip">,</nts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_599" n="HIAT:w" s="T131">tına</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_602" n="HIAT:w" s="T132">imaqotatɨ</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_605" n="HIAT:w" s="T133">qütalna</ts>
                  <nts id="Seg_606" n="HIAT:ip">,</nts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_609" n="HIAT:w" s="T134">imaqotatɨ</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_612" n="HIAT:w" s="T135">ɛj</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_615" n="HIAT:w" s="T136">ılla</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_618" n="HIAT:w" s="T137">quŋa</ts>
                  <nts id="Seg_619" n="HIAT:ip">.</nts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T145" id="Seg_622" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_624" n="HIAT:w" s="T138">Iralʼ</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_627" n="HIAT:w" s="T139">mɨqä</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_630" n="HIAT:w" s="T140">tına</ts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_633" n="HIAT:w" s="T141">ijatɨ</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_636" n="HIAT:w" s="T142">nʼeːkɔːl</ts>
                  <nts id="Seg_637" n="HIAT:ip">,</nts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_640" n="HIAT:w" s="T143">pɛlɨkɔːl</ts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_643" n="HIAT:w" s="T144">qala</ts>
                  <nts id="Seg_644" n="HIAT:ip">.</nts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T147" id="Seg_647" n="HIAT:u" s="T145">
                  <ts e="T146" id="Seg_649" n="HIAT:w" s="T145">Kučʼčʼa</ts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_652" n="HIAT:w" s="T146">qattɛnta</ts>
                  <nts id="Seg_653" n="HIAT:ip">?</nts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_656" n="HIAT:u" s="T147">
                  <ts e="T148" id="Seg_658" n="HIAT:w" s="T147">Na</ts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_661" n="HIAT:w" s="T148">qälɨ</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_664" n="HIAT:w" s="T149">ira</ts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_667" n="HIAT:w" s="T150">nɨnɨ</ts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_670" n="HIAT:w" s="T151">šittäl</ts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_673" n="HIAT:w" s="T152">tɔː</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_676" n="HIAT:w" s="T153">puːčʼɨmpatɨ</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_679" n="HIAT:w" s="T154">na</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_682" n="HIAT:w" s="T155">čʼilʼaj</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_685" n="HIAT:w" s="T156">ija</ts>
                  <nts id="Seg_686" n="HIAT:ip">.</nts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T170" id="Seg_689" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_691" n="HIAT:w" s="T157">Mərqɨ</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_694" n="HIAT:w" s="T158">ijatɨ</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_696" n="HIAT:ip">(</nts>
                  <ts e="T160" id="Seg_698" n="HIAT:w" s="T159">na</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_701" n="HIAT:w" s="T160">istap=</ts>
                  <nts id="Seg_702" n="HIAT:ip">)</nts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_704" n="HIAT:ip">(</nts>
                  <ts e="T163" id="Seg_706" n="HIAT:w" s="T161">mat-</ts>
                  <nts id="Seg_707" n="HIAT:ip">)</nts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_709" n="HIAT:ip">(</nts>
                  <ts e="T166" id="Seg_711" n="HIAT:w" s="T163">mɨt-</ts>
                  <nts id="Seg_712" n="HIAT:ip">)</nts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_715" n="HIAT:w" s="T166">istap</ts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_718" n="HIAT:w" s="T167">Genalʼ</ts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_721" n="HIAT:w" s="T168">mɨt</ts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_724" n="HIAT:w" s="T169">tarı</ts>
                  <nts id="Seg_725" n="HIAT:ip">.</nts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T173" id="Seg_728" n="HIAT:u" s="T170">
                  <ts e="T171" id="Seg_730" n="HIAT:w" s="T170">Nılʼčʼij</ts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_733" n="HIAT:w" s="T171">ijatɨj</ts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_736" n="HIAT:w" s="T172">ɛppa</ts>
                  <nts id="Seg_737" n="HIAT:ip">.</nts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T177" id="Seg_740" n="HIAT:u" s="T173">
                  <ts e="T174" id="Seg_742" n="HIAT:w" s="T173">Tɔː</ts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_745" n="HIAT:w" s="T174">puːtɨŋɨtɨ</ts>
                  <nts id="Seg_746" n="HIAT:ip">,</nts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_749" n="HIAT:w" s="T175">ɔːtätɨj</ts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_752" n="HIAT:w" s="T176">čʼäŋka</ts>
                  <nts id="Seg_753" n="HIAT:ip">.</nts>
                  <nts id="Seg_754" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T182" id="Seg_756" n="HIAT:u" s="T177">
                  <ts e="T178" id="Seg_758" n="HIAT:w" s="T177">Ɔːtäkɔːl</ts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_761" n="HIAT:w" s="T178">topɨsa</ts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_764" n="HIAT:w" s="T179">ilɨmpɔːt</ts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_767" n="HIAT:w" s="T180">na</ts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_770" n="HIAT:w" s="T181">äsasɨt</ts>
                  <nts id="Seg_771" n="HIAT:ip">.</nts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T188" id="Seg_774" n="HIAT:u" s="T182">
                  <ts e="T183" id="Seg_776" n="HIAT:w" s="T182">Nɨmtɨ</ts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_779" n="HIAT:w" s="T183">ilɨmpɔːtɨn</ts>
                  <nts id="Seg_780" n="HIAT:ip">,</nts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_783" n="HIAT:w" s="T184">ilɨmpɔːtɨn</ts>
                  <nts id="Seg_784" n="HIAT:ip">,</nts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_787" n="HIAT:w" s="T185">nɨnɨ</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_790" n="HIAT:w" s="T186">tɔː</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_793" n="HIAT:w" s="T187">puːčʼɨsɔːt</ts>
                  <nts id="Seg_794" n="HIAT:ip">.</nts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T199" id="Seg_797" n="HIAT:u" s="T188">
                  <ts e="T189" id="Seg_799" n="HIAT:w" s="T188">Tɔː</ts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_802" n="HIAT:w" s="T189">puːlä</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_805" n="HIAT:w" s="T190">na</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_808" n="HIAT:w" s="T191">ija</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_811" n="HIAT:w" s="T192">qälɨ</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_814" n="HIAT:w" s="T193">ira</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_817" n="HIAT:w" s="T194">mɔːttɨ</ts>
                  <nts id="Seg_818" n="HIAT:ip">,</nts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_821" n="HIAT:w" s="T195">qälɨ</ts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_824" n="HIAT:w" s="T196">ira</ts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_827" n="HIAT:w" s="T197">ijatɨ</ts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_830" n="HIAT:w" s="T198">čʼäŋka</ts>
                  <nts id="Seg_831" n="HIAT:ip">.</nts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T201" id="Seg_834" n="HIAT:u" s="T199">
                  <ts e="T200" id="Seg_836" n="HIAT:w" s="T199">Ijakɨtɨlʼ</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_839" n="HIAT:w" s="T200">imatɨ</ts>
                  <nts id="Seg_840" n="HIAT:ip">.</nts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T208" id="Seg_843" n="HIAT:u" s="T201">
                  <ts e="T202" id="Seg_845" n="HIAT:w" s="T201">Toːnna</ts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_848" n="HIAT:w" s="T202">čʼontolʼ</ts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_851" n="HIAT:w" s="T203">tɨmnʼatɨ</ts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_853" n="HIAT:ip">(</nts>
                  <ts e="T205" id="Seg_855" n="HIAT:w" s="T204">naintɨ</ts>
                  <nts id="Seg_856" n="HIAT:ip">)</nts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_859" n="HIAT:w" s="T205">ukkɨr</ts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_862" n="HIAT:w" s="T206">nälʼatɨ</ts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_865" n="HIAT:w" s="T207">pɛläkɨtɨj</ts>
                  <nts id="Seg_866" n="HIAT:ip">.</nts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T212" id="Seg_869" n="HIAT:u" s="T208">
                  <ts e="T209" id="Seg_871" n="HIAT:w" s="T208">Takkɨn</ts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_874" n="HIAT:w" s="T209">nʼenna</ts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_877" n="HIAT:w" s="T210">šʼittɨ</ts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_880" n="HIAT:w" s="T211">qälɨqı</ts>
                  <nts id="Seg_881" n="HIAT:ip">.</nts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T215" id="Seg_884" n="HIAT:u" s="T212">
                  <ts e="T213" id="Seg_886" n="HIAT:w" s="T212">Təp</ts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_889" n="HIAT:w" s="T213">poːkɨtɨlʼ</ts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_892" n="HIAT:w" s="T214">təttot</ts>
                  <nts id="Seg_893" n="HIAT:ip">.</nts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T226" id="Seg_896" n="HIAT:u" s="T215">
                  <nts id="Seg_897" n="HIAT:ip">(</nts>
                  <ts e="T216" id="Seg_899" n="HIAT:w" s="T215">Ponʼi</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_902" n="HIAT:w" s="T216">ira=</ts>
                  <nts id="Seg_903" n="HIAT:ip">)</nts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_906" n="HIAT:w" s="T217">Ponʼi</ts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_909" n="HIAT:w" s="T218">ira</ts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_912" n="HIAT:w" s="T219">naɛːntɨ</ts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_915" n="HIAT:w" s="T220">kun</ts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_918" n="HIAT:w" s="T221">montɨlʼ</ts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_921" n="HIAT:w" s="T222">tamtɨrtɨ</ts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_924" n="HIAT:w" s="T223">ɔːtäp</ts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_927" n="HIAT:w" s="T224">muntɨk</ts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_930" n="HIAT:w" s="T225">taqqɨšpatɨ</ts>
                  <nts id="Seg_931" n="HIAT:ip">.</nts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T227" id="Seg_934" n="HIAT:u" s="T226">
                  <ts e="T227" id="Seg_936" n="HIAT:w" s="T226">Tal</ts>
                  <nts id="Seg_937" n="HIAT:ip">.</nts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T238" id="Seg_940" n="HIAT:u" s="T227">
                  <ts e="T228" id="Seg_942" n="HIAT:w" s="T227">Nɨlčʼik</ts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_945" n="HIAT:w" s="T228">tüŋa</ts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_948" n="HIAT:w" s="T229">na</ts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_951" n="HIAT:w" s="T230">timnʼasɨqä</ts>
                  <nts id="Seg_952" n="HIAT:ip">,</nts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_955" n="HIAT:w" s="T231">na</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_958" n="HIAT:w" s="T232">timnʼasɨqä</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_961" n="HIAT:w" s="T233">ilɔːqı</ts>
                  <nts id="Seg_962" n="HIAT:ip">,</nts>
                  <nts id="Seg_963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_965" n="HIAT:w" s="T234">na</ts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_968" n="HIAT:w" s="T235">iralʼ</ts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_971" n="HIAT:w" s="T236">timnʼasɨqä</ts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_974" n="HIAT:w" s="T237">šʼittɨ</ts>
                  <nts id="Seg_975" n="HIAT:ip">.</nts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T241" id="Seg_978" n="HIAT:u" s="T238">
                  <ts e="T239" id="Seg_980" n="HIAT:w" s="T238">Tət</ts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_983" n="HIAT:w" s="T239">toːn</ts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_986" n="HIAT:w" s="T240">ɔːtätɨ</ts>
                  <nts id="Seg_987" n="HIAT:ip">.</nts>
                  <nts id="Seg_988" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T245" id="Seg_990" n="HIAT:u" s="T241">
                  <ts e="T242" id="Seg_992" n="HIAT:w" s="T241">Na</ts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_995" n="HIAT:w" s="T242">iran</ts>
                  <nts id="Seg_996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_998" n="HIAT:w" s="T243">ɔːtäm</ts>
                  <nts id="Seg_999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_1001" n="HIAT:w" s="T244">iqɨntɔːqı</ts>
                  <nts id="Seg_1002" n="HIAT:ip">.</nts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T251" id="Seg_1005" n="HIAT:u" s="T245">
                  <ts e="T246" id="Seg_1007" n="HIAT:w" s="T245">Qumiːntɨsa</ts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_1010" n="HIAT:w" s="T246">tünta</ts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_1013" n="HIAT:w" s="T247">Poni</ts>
                  <nts id="Seg_1014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_1016" n="HIAT:w" s="T248">ira</ts>
                  <nts id="Seg_1017" n="HIAT:ip">,</nts>
                  <nts id="Seg_1018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_1020" n="HIAT:w" s="T249">nimtɨ</ts>
                  <nts id="Seg_1021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_1023" n="HIAT:w" s="T250">najntɨ</ts>
                  <nts id="Seg_1024" n="HIAT:ip">.</nts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T253" id="Seg_1027" n="HIAT:u" s="T251">
                  <ts e="T252" id="Seg_1029" n="HIAT:w" s="T251">Nɨnɨ</ts>
                  <nts id="Seg_1030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_1032" n="HIAT:w" s="T252">tal</ts>
                  <nts id="Seg_1033" n="HIAT:ip">.</nts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T258" id="Seg_1036" n="HIAT:u" s="T253">
                  <ts e="T254" id="Seg_1038" n="HIAT:w" s="T253">Tüla</ts>
                  <nts id="Seg_1039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_1041" n="HIAT:w" s="T254">šäqqɔːtɨt</ts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_1044" n="HIAT:w" s="T255">na</ts>
                  <nts id="Seg_1045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_1047" n="HIAT:w" s="T256">təptɨn</ts>
                  <nts id="Seg_1048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_1050" n="HIAT:w" s="T257">mɔːt</ts>
                  <nts id="Seg_1051" n="HIAT:ip">.</nts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T266" id="Seg_1054" n="HIAT:u" s="T258">
                  <ts e="T259" id="Seg_1056" n="HIAT:w" s="T258">Na</ts>
                  <nts id="Seg_1057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_1059" n="HIAT:w" s="T259">ijap</ts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_1062" n="HIAT:w" s="T260">qapı</ts>
                  <nts id="Seg_1063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1065" n="HIAT:w" s="T261">aš</ts>
                  <nts id="Seg_1066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_1068" n="HIAT:w" s="T262">tɛnɨmɔːt</ts>
                  <nts id="Seg_1069" n="HIAT:ip">,</nts>
                  <nts id="Seg_1070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1071" n="HIAT:ip">(</nts>
                  <nts id="Seg_1072" n="HIAT:ip">(</nts>
                  <ats e="T264" id="Seg_1073" n="HIAT:non-pho" s="T263">…</ats>
                  <nts id="Seg_1074" n="HIAT:ip">)</nts>
                  <nts id="Seg_1075" n="HIAT:ip">)</nts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_1078" n="HIAT:w" s="T264">kuttar</ts>
                  <nts id="Seg_1079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1081" n="HIAT:w" s="T265">koŋɨmmɛnta</ts>
                  <nts id="Seg_1082" n="HIAT:ip">?</nts>
                  <nts id="Seg_1083" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T274" id="Seg_1085" n="HIAT:u" s="T266">
                  <ts e="T267" id="Seg_1087" n="HIAT:w" s="T266">Täp</ts>
                  <nts id="Seg_1088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1090" n="HIAT:w" s="T267">qaj</ts>
                  <nts id="Seg_1091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1092" n="HIAT:ip">(</nts>
                  <nts id="Seg_1093" n="HIAT:ip">(</nts>
                  <ats e="T269" id="Seg_1094" n="HIAT:non-pho" s="T268">…</ats>
                  <nts id="Seg_1095" n="HIAT:ip">)</nts>
                  <nts id="Seg_1096" n="HIAT:ip">)</nts>
                  <nts id="Seg_1097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1099" n="HIAT:w" s="T269">mutronak</ts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1101" n="HIAT:ip">(</nts>
                  <nts id="Seg_1102" n="HIAT:ip">(</nts>
                  <ats e="T271" id="Seg_1103" n="HIAT:non-pho" s="T270">…</ats>
                  <nts id="Seg_1104" n="HIAT:ip">)</nts>
                  <nts id="Seg_1105" n="HIAT:ip">)</nts>
                  <nts id="Seg_1106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1108" n="HIAT:w" s="T271">ilʼ</ts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1111" n="HIAT:w" s="T272">kuttar</ts>
                  <nts id="Seg_1112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1114" n="HIAT:w" s="T273">ila</ts>
                  <nts id="Seg_1115" n="HIAT:ip">.</nts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T277" id="Seg_1118" n="HIAT:u" s="T274">
                  <ts e="T275" id="Seg_1120" n="HIAT:w" s="T274">Tıː</ts>
                  <nts id="Seg_1121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1123" n="HIAT:w" s="T275">kuttar</ts>
                  <nts id="Seg_1124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1126" n="HIAT:w" s="T276">tɛnɨmmɛntal</ts>
                  <nts id="Seg_1127" n="HIAT:ip">?</nts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T279" id="Seg_1130" n="HIAT:u" s="T277">
                  <ts e="T278" id="Seg_1132" n="HIAT:w" s="T277">Alpän</ts>
                  <nts id="Seg_1133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1135" n="HIAT:w" s="T278">iläntɨj</ts>
                  <nts id="Seg_1136" n="HIAT:ip">.</nts>
                  <nts id="Seg_1137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T282" id="Seg_1139" n="HIAT:u" s="T279">
                  <ts e="T280" id="Seg_1141" n="HIAT:w" s="T279">Šäqqɔːqı</ts>
                  <nts id="Seg_1142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1144" n="HIAT:w" s="T280">na</ts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1147" n="HIAT:w" s="T281">qaj</ts>
                  <nts id="Seg_1148" n="HIAT:ip">.</nts>
                  <nts id="Seg_1149" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T289" id="Seg_1151" n="HIAT:u" s="T282">
                  <ts e="T283" id="Seg_1153" n="HIAT:w" s="T282">Mompa</ts>
                  <nts id="Seg_1154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1156" n="HIAT:w" s="T283">nɔːrtälɨj</ts>
                  <nts id="Seg_1157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1159" n="HIAT:w" s="T284">čʼeːlɨ</ts>
                  <nts id="Seg_1160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1162" n="HIAT:w" s="T285">toqɨn</ts>
                  <nts id="Seg_1163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1165" n="HIAT:w" s="T286">mompa</ts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1168" n="HIAT:w" s="T287">na</ts>
                  <nts id="Seg_1169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1171" n="HIAT:w" s="T288">tüŋɨ</ts>
                  <nts id="Seg_1172" n="HIAT:ip">.</nts>
                  <nts id="Seg_1173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T296" id="Seg_1175" n="HIAT:u" s="T289">
                  <ts e="T290" id="Seg_1177" n="HIAT:w" s="T289">Mompa</ts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1180" n="HIAT:w" s="T290">Poni</ts>
                  <nts id="Seg_1181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1183" n="HIAT:w" s="T291">ira</ts>
                  <nts id="Seg_1184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1185" n="HIAT:ip">(</nts>
                  <nts id="Seg_1186" n="HIAT:ip">(</nts>
                  <ats e="T293" id="Seg_1187" n="HIAT:non-pho" s="T292">…</ats>
                  <nts id="Seg_1188" n="HIAT:ip">)</nts>
                  <nts id="Seg_1189" n="HIAT:ip">)</nts>
                  <nts id="Seg_1190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1192" n="HIAT:w" s="T293">köt</ts>
                  <nts id="Seg_1193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1195" n="HIAT:w" s="T294">qumsä</ts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1197" n="HIAT:ip">(</nts>
                  <ts e="T296" id="Seg_1199" n="HIAT:w" s="T295">tümpa</ts>
                  <nts id="Seg_1200" n="HIAT:ip">)</nts>
                  <nts id="Seg_1201" n="HIAT:ip">.</nts>
                  <nts id="Seg_1202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T300" id="Seg_1204" n="HIAT:u" s="T296">
                  <ts e="T297" id="Seg_1206" n="HIAT:w" s="T296">Qapı</ts>
                  <nts id="Seg_1207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1209" n="HIAT:w" s="T297">na</ts>
                  <nts id="Seg_1210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1212" n="HIAT:w" s="T298">ɔːtät</ts>
                  <nts id="Seg_1213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1215" n="HIAT:w" s="T299">tɔːqqɨqɨntɔːqo</ts>
                  <nts id="Seg_1216" n="HIAT:ip">.</nts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T308" id="Seg_1219" n="HIAT:u" s="T300">
                  <ts e="T301" id="Seg_1221" n="HIAT:w" s="T300">Na</ts>
                  <nts id="Seg_1222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1224" n="HIAT:w" s="T301">qumot</ts>
                  <nts id="Seg_1225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1227" n="HIAT:w" s="T302">nɔːt</ts>
                  <nts id="Seg_1228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1230" n="HIAT:w" s="T303">na</ts>
                  <nts id="Seg_1231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1233" n="HIAT:w" s="T304">timnʼasɨqän</ts>
                  <nts id="Seg_1234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1236" n="HIAT:w" s="T305">na</ts>
                  <nts id="Seg_1237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1239" n="HIAT:w" s="T306">ɔːtäp</ts>
                  <nts id="Seg_1240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1242" n="HIAT:w" s="T307">tɔːqqɨmpɔːt</ts>
                  <nts id="Seg_1243" n="HIAT:ip">.</nts>
                  <nts id="Seg_1244" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T319" id="Seg_1246" n="HIAT:u" s="T308">
                  <ts e="T309" id="Seg_1248" n="HIAT:w" s="T308">Kun</ts>
                  <nts id="Seg_1249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1251" n="HIAT:w" s="T309">montɨlʼ</ts>
                  <nts id="Seg_1252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1254" n="HIAT:w" s="T310">mɔːttɨ</ts>
                  <nts id="Seg_1255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1257" n="HIAT:w" s="T311">tına</ts>
                  <nts id="Seg_1258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1260" n="HIAT:w" s="T312">šentɨ</ts>
                  <nts id="Seg_1261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1263" n="HIAT:w" s="T313">mɔːttɨ</ts>
                  <nts id="Seg_1264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1266" n="HIAT:w" s="T314">kun</ts>
                  <nts id="Seg_1267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1269" n="HIAT:w" s="T315">montɨlʼ</ts>
                  <nts id="Seg_1270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1272" n="HIAT:w" s="T316">qajtɨ</ts>
                  <nts id="Seg_1273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1275" n="HIAT:w" s="T317">muntɨ</ts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1278" n="HIAT:w" s="T318">taqqɨltɛntɨŋit</ts>
                  <nts id="Seg_1279" n="HIAT:ip">.</nts>
                  <nts id="Seg_1280" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T328" id="Seg_1282" n="HIAT:u" s="T319">
                  <ts e="T320" id="Seg_1284" n="HIAT:w" s="T319">Ukoːn</ts>
                  <nts id="Seg_1285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1287" n="HIAT:w" s="T320">nɔːta</ts>
                  <nts id="Seg_1288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1290" n="HIAT:w" s="T321">takkɨlʼ</ts>
                  <nts id="Seg_1291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1293" n="HIAT:w" s="T322">qälɨiːt</ts>
                  <nts id="Seg_1294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1296" n="HIAT:w" s="T323">ɔːtäp</ts>
                  <nts id="Seg_1297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1299" n="HIAT:w" s="T324">taqqɨllä</ts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1302" n="HIAT:w" s="T325">nɔːtɨ</ts>
                  <nts id="Seg_1303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1305" n="HIAT:w" s="T326">muntɨk</ts>
                  <nts id="Seg_1306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1308" n="HIAT:w" s="T327">iːmpɔːt</ts>
                  <nts id="Seg_1309" n="HIAT:ip">.</nts>
                  <nts id="Seg_1310" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T331" id="Seg_1312" n="HIAT:u" s="T328">
                  <ts e="T329" id="Seg_1314" n="HIAT:w" s="T328">Nɨnɨ</ts>
                  <nts id="Seg_1315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1317" n="HIAT:w" s="T329">šittäl</ts>
                  <nts id="Seg_1318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1320" n="HIAT:w" s="T330">mɨŋa</ts>
                  <nts id="Seg_1321" n="HIAT:ip">.</nts>
                  <nts id="Seg_1322" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T336" id="Seg_1324" n="HIAT:u" s="T331">
                  <ts e="T332" id="Seg_1326" n="HIAT:w" s="T331">Moqɨnä</ts>
                  <nts id="Seg_1327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1329" n="HIAT:w" s="T332">qənnɔːt</ts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1332" n="HIAT:w" s="T333">na</ts>
                  <nts id="Seg_1333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1335" n="HIAT:w" s="T334">qälɨt</ts>
                  <nts id="Seg_1336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1338" n="HIAT:w" s="T335">tıšša</ts>
                  <nts id="Seg_1339" n="HIAT:ip">.</nts>
                  <nts id="Seg_1340" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T341" id="Seg_1342" n="HIAT:u" s="T336">
                  <ts e="T337" id="Seg_1344" n="HIAT:w" s="T336">Nɔːrtäl</ts>
                  <nts id="Seg_1345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1347" n="HIAT:w" s="T337">čʼeːlɨt</ts>
                  <nts id="Seg_1348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1350" n="HIAT:w" s="T338">mompa</ts>
                  <nts id="Seg_1351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1353" n="HIAT:w" s="T339">tüntɔːmɨn</ts>
                  <nts id="Seg_1354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1356" n="HIAT:w" s="T340">mɨta</ts>
                  <nts id="Seg_1357" n="HIAT:ip">.</nts>
                  <nts id="Seg_1358" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T346" id="Seg_1360" n="HIAT:u" s="T341">
                  <ts e="T342" id="Seg_1362" n="HIAT:w" s="T341">Tɛː</ts>
                  <nts id="Seg_1363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1365" n="HIAT:w" s="T342">mompa</ts>
                  <nts id="Seg_1366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1368" n="HIAT:w" s="T343">kučʼčʼät</ts>
                  <nts id="Seg_1369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1371" n="HIAT:w" s="T344">ɨkɨ</ts>
                  <nts id="Seg_1372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1374" n="HIAT:w" s="T345">qənnɨlɨt</ts>
                  <nts id="Seg_1375" n="HIAT:ip">.</nts>
                  <nts id="Seg_1376" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T356" id="Seg_1378" n="HIAT:u" s="T346">
                  <ts e="T347" id="Seg_1380" n="HIAT:w" s="T346">Šittälɨ</ts>
                  <nts id="Seg_1381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1383" n="HIAT:w" s="T347">čʼeːlontɨ</ts>
                  <nts id="Seg_1384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1386" n="HIAT:w" s="T348">na</ts>
                  <nts id="Seg_1387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1389" n="HIAT:w" s="T349">čʼeːlɨnna</ts>
                  <nts id="Seg_1390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1392" n="HIAT:w" s="T350">na</ts>
                  <nts id="Seg_1393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1395" n="HIAT:w" s="T351">ija</ts>
                  <nts id="Seg_1396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1398" n="HIAT:w" s="T352">nılʼčʼiŋ</ts>
                  <nts id="Seg_1399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1401" n="HIAT:w" s="T353">ɛsa</ts>
                  <nts id="Seg_1402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1404" n="HIAT:w" s="T354">na</ts>
                  <nts id="Seg_1405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1407" n="HIAT:w" s="T355">ilʼčʼantɨnɨk</ts>
                  <nts id="Seg_1408" n="HIAT:ip">.</nts>
                  <nts id="Seg_1409" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T359" id="Seg_1411" n="HIAT:u" s="T356">
                  <ts e="T357" id="Seg_1413" n="HIAT:w" s="T356">Na</ts>
                  <nts id="Seg_1414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1416" n="HIAT:w" s="T357">gostʼan</ts>
                  <nts id="Seg_1417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1419" n="HIAT:w" s="T358">ilʼčʼant</ts>
                  <nts id="Seg_1420" n="HIAT:ip">.</nts>
                  <nts id="Seg_1421" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T365" id="Seg_1423" n="HIAT:u" s="T359">
                  <ts e="T360" id="Seg_1425" n="HIAT:w" s="T359">Mɨta</ts>
                  <nts id="Seg_1426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1428" n="HIAT:w" s="T360">nʼenna</ts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1431" n="HIAT:w" s="T361">tɨmtɨ</ts>
                  <nts id="Seg_1432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1434" n="HIAT:w" s="T362">mɨ</ts>
                  <nts id="Seg_1435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1437" n="HIAT:w" s="T363">ɛːnta</ts>
                  <nts id="Seg_1438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1440" n="HIAT:w" s="T364">mɨta</ts>
                  <nts id="Seg_1441" n="HIAT:ip">.</nts>
                  <nts id="Seg_1442" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T376" id="Seg_1444" n="HIAT:u" s="T365">
                  <ts e="T366" id="Seg_1446" n="HIAT:w" s="T365">Tɛːt</ts>
                  <nts id="Seg_1447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1449" n="HIAT:w" s="T366">toːn</ts>
                  <nts id="Seg_1450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1452" n="HIAT:w" s="T367">ɔːtän</ts>
                  <nts id="Seg_1453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1455" n="HIAT:w" s="T368">nɨŋqɨkuššaŋ</ts>
                  <nts id="Seg_1456" n="HIAT:ip">,</nts>
                  <nts id="Seg_1457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1459" n="HIAT:w" s="T369">mɨta</ts>
                  <nts id="Seg_1460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1462" n="HIAT:w" s="T370">nʼaraŋ</ts>
                  <nts id="Seg_1463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1465" n="HIAT:w" s="T371">ɛːntɨ</ts>
                  <nts id="Seg_1466" n="HIAT:ip">,</nts>
                  <nts id="Seg_1467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1469" n="HIAT:w" s="T372">mɨta</ts>
                  <nts id="Seg_1470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1472" n="HIAT:w" s="T373">näčʼčʼatɨ</ts>
                  <nts id="Seg_1473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1475" n="HIAT:w" s="T374">üːtalɔːmɨn</ts>
                  <nts id="Seg_1476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1478" n="HIAT:w" s="T375">mɨta</ts>
                  <nts id="Seg_1479" n="HIAT:ip">.</nts>
                  <nts id="Seg_1480" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T384" id="Seg_1482" n="HIAT:u" s="T376">
                  <ts e="T377" id="Seg_1484" n="HIAT:w" s="T376">Qälɨt</ts>
                  <nts id="Seg_1485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1487" n="HIAT:w" s="T377">tüntɔːtɨn</ts>
                  <nts id="Seg_1488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1490" n="HIAT:w" s="T378">nɨntɨ</ts>
                  <nts id="Seg_1491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1493" n="HIAT:w" s="T379">ɔːtäp</ts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1496" n="HIAT:w" s="T380">tɔːqqɨqolamnɛntɔːtɨn</ts>
                  <nts id="Seg_1497" n="HIAT:ip">,</nts>
                  <nts id="Seg_1498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1500" n="HIAT:w" s="T381">nın</ts>
                  <nts id="Seg_1501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1503" n="HIAT:w" s="T382">mɨta</ts>
                  <nts id="Seg_1504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1506" n="HIAT:w" s="T383">tına</ts>
                  <nts id="Seg_1507" n="HIAT:ip">.</nts>
                  <nts id="Seg_1508" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T391" id="Seg_1510" n="HIAT:u" s="T384">
                  <ts e="T385" id="Seg_1512" n="HIAT:w" s="T384">Ɔːtän</ts>
                  <nts id="Seg_1513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1515" n="HIAT:w" s="T385">uptɨ</ts>
                  <nts id="Seg_1516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1518" n="HIAT:w" s="T386">kəlʼčʼomɨnto</ts>
                  <nts id="Seg_1519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1521" n="HIAT:w" s="T387">kərɨmpɨlʼ</ts>
                  <nts id="Seg_1522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1524" n="HIAT:w" s="T388">mɨta</ts>
                  <nts id="Seg_1525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1527" n="HIAT:w" s="T389">kučʼčʼa</ts>
                  <nts id="Seg_1528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1530" n="HIAT:w" s="T390">meːntɔːmɨn</ts>
                  <nts id="Seg_1531" n="HIAT:ip">?</nts>
                  <nts id="Seg_1532" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T395" id="Seg_1534" n="HIAT:u" s="T391">
                  <ts e="T392" id="Seg_1536" n="HIAT:w" s="T391">Na</ts>
                  <nts id="Seg_1537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1539" n="HIAT:w" s="T392">ija</ts>
                  <nts id="Seg_1540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1542" n="HIAT:w" s="T393">nık</ts>
                  <nts id="Seg_1543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1545" n="HIAT:w" s="T394">kətɨŋɨtɨ</ts>
                  <nts id="Seg_1546" n="HIAT:ip">.</nts>
                  <nts id="Seg_1547" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T400" id="Seg_1549" n="HIAT:u" s="T395">
                  <ts e="T396" id="Seg_1551" n="HIAT:w" s="T395">Nɨnɨ</ts>
                  <nts id="Seg_1552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1554" n="HIAT:w" s="T396">nık</ts>
                  <nts id="Seg_1555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1557" n="HIAT:w" s="T397">kətɨŋɨtɨ</ts>
                  <nts id="Seg_1558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1560" n="HIAT:w" s="T398">mɨta</ts>
                  <nts id="Seg_1561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1562" n="HIAT:ip">(</nts>
                  <nts id="Seg_1563" n="HIAT:ip">(</nts>
                  <ats e="T400" id="Seg_1564" n="HIAT:non-pho" s="T399">…</ats>
                  <nts id="Seg_1565" n="HIAT:ip">)</nts>
                  <nts id="Seg_1566" n="HIAT:ip">)</nts>
                  <nts id="Seg_1567" n="HIAT:ip">.</nts>
                  <nts id="Seg_1568" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T406" id="Seg_1570" n="HIAT:u" s="T400">
                  <ts e="T401" id="Seg_1572" n="HIAT:w" s="T400">Mäkka</ts>
                  <nts id="Seg_1573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1575" n="HIAT:w" s="T401">mitɨ</ts>
                  <nts id="Seg_1576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1578" n="HIAT:w" s="T402">šentɨ</ts>
                  <nts id="Seg_1579" n="HIAT:ip">,</nts>
                  <nts id="Seg_1580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1582" n="HIAT:w" s="T403">šentɨ</ts>
                  <nts id="Seg_1583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1585" n="HIAT:w" s="T404">mɔːllɨ</ts>
                  <nts id="Seg_1586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1588" n="HIAT:w" s="T405">ɛːŋa</ts>
                  <nts id="Seg_1589" n="HIAT:ip">?</nts>
                  <nts id="Seg_1590" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T412" id="Seg_1592" n="HIAT:u" s="T406">
                  <ts e="T407" id="Seg_1594" n="HIAT:w" s="T406">Ilʼčʼa</ts>
                  <nts id="Seg_1595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1597" n="HIAT:w" s="T407">mɨta</ts>
                  <nts id="Seg_1598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1600" n="HIAT:w" s="T408">qaj</ts>
                  <nts id="Seg_1601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1603" n="HIAT:w" s="T409">šentɨ</ts>
                  <nts id="Seg_1604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1606" n="HIAT:w" s="T410">mɔːllɨ</ts>
                  <nts id="Seg_1607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1609" n="HIAT:w" s="T411">ɛːŋa</ts>
                  <nts id="Seg_1610" n="HIAT:ip">?</nts>
                  <nts id="Seg_1611" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T415" id="Seg_1613" n="HIAT:u" s="T412">
                  <ts e="T413" id="Seg_1615" n="HIAT:w" s="T412">Mäkka</ts>
                  <nts id="Seg_1616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1618" n="HIAT:w" s="T413">mitɨ</ts>
                  <nts id="Seg_1619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1621" n="HIAT:w" s="T414">mɨta</ts>
                  <nts id="Seg_1622" n="HIAT:ip">.</nts>
                  <nts id="Seg_1623" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T425" id="Seg_1625" n="HIAT:u" s="T415">
                  <ts e="T416" id="Seg_1627" n="HIAT:w" s="T415">A</ts>
                  <nts id="Seg_1628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1630" n="HIAT:w" s="T416">toːnna</ts>
                  <nts id="Seg_1631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1633" n="HIAT:w" s="T417">tına</ts>
                  <nts id="Seg_1634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1636" n="HIAT:w" s="T418">timnʼantɨ</ts>
                  <nts id="Seg_1637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1639" n="HIAT:w" s="T419">nälʼa</ts>
                  <nts id="Seg_1640" n="HIAT:ip">,</nts>
                  <nts id="Seg_1641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1643" n="HIAT:w" s="T420">ilʼčʼatɨ</ts>
                  <nts id="Seg_1644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1646" n="HIAT:w" s="T421">namɨp</ts>
                  <nts id="Seg_1647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1649" n="HIAT:w" s="T422">quraltɨmmɨntɨt</ts>
                  <nts id="Seg_1650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1652" n="HIAT:w" s="T423">na</ts>
                  <nts id="Seg_1653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1655" n="HIAT:w" s="T424">ijanɨk</ts>
                  <nts id="Seg_1656" n="HIAT:ip">.</nts>
                  <nts id="Seg_1657" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T439" id="Seg_1659" n="HIAT:u" s="T425">
                  <ts e="T426" id="Seg_1661" n="HIAT:w" s="T425">Man</ts>
                  <nts id="Seg_1662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1664" n="HIAT:w" s="T426">mɨta</ts>
                  <nts id="Seg_1665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1667" n="HIAT:w" s="T427">prosto</ts>
                  <nts id="Seg_1668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1670" n="HIAT:w" s="T428">məčʼimnɛntak</ts>
                  <nts id="Seg_1671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1673" n="HIAT:w" s="T429">mɔːt</ts>
                  <nts id="Seg_1674" n="HIAT:ip">,</nts>
                  <nts id="Seg_1675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1677" n="HIAT:w" s="T430">nʼennat</ts>
                  <nts id="Seg_1678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1680" n="HIAT:w" s="T431">mɨntɨ</ts>
                  <nts id="Seg_1681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1683" n="HIAT:w" s="T432">mačʼilʼ</ts>
                  <nts id="Seg_1684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1686" n="HIAT:w" s="T433">tıčʼi</ts>
                  <nts id="Seg_1687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1689" n="HIAT:w" s="T434">ɛnta</ts>
                  <nts id="Seg_1690" n="HIAT:ip">,</nts>
                  <nts id="Seg_1691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1693" n="HIAT:w" s="T435">na</ts>
                  <nts id="Seg_1694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1696" n="HIAT:w" s="T436">nʼennälʼ</ts>
                  <nts id="Seg_1697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1699" n="HIAT:w" s="T437">pɛlaktɨ</ts>
                  <nts id="Seg_1700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1702" n="HIAT:w" s="T438">mɔːtɨŋnɛntak</ts>
                  <nts id="Seg_1703" n="HIAT:ip">.</nts>
                  <nts id="Seg_1704" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T443" id="Seg_1706" n="HIAT:u" s="T439">
                  <ts e="T440" id="Seg_1708" n="HIAT:w" s="T439">Šentɨ</ts>
                  <nts id="Seg_1709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1711" n="HIAT:w" s="T440">mɔːt</ts>
                  <nts id="Seg_1712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1714" n="HIAT:w" s="T441">taːtɨŋa</ts>
                  <nts id="Seg_1715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1717" n="HIAT:w" s="T442">nɛntɨ</ts>
                  <nts id="Seg_1718" n="HIAT:ip">.</nts>
                  <nts id="Seg_1719" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T444" id="Seg_1721" n="HIAT:u" s="T443">
                  <ts e="T444" id="Seg_1723" n="HIAT:w" s="T443">Üːtalnɔːtɨt</ts>
                  <nts id="Seg_1724" n="HIAT:ip">.</nts>
                  <nts id="Seg_1725" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T445" id="Seg_1727" n="HIAT:u" s="T444">
                  <ts e="T445" id="Seg_1729" n="HIAT:w" s="T444">Tal</ts>
                  <nts id="Seg_1730" n="HIAT:ip">.</nts>
                  <nts id="Seg_1731" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T447" id="Seg_1733" n="HIAT:u" s="T445">
                  <ts e="T446" id="Seg_1735" n="HIAT:w" s="T445">Üːtaltɔːt</ts>
                  <nts id="Seg_1736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1738" n="HIAT:w" s="T446">na</ts>
                  <nts id="Seg_1739" n="HIAT:ip">.</nts>
                  <nts id="Seg_1740" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T457" id="Seg_1742" n="HIAT:u" s="T447">
                  <ts e="T448" id="Seg_1744" n="HIAT:w" s="T447">Qapija</ts>
                  <nts id="Seg_1745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1747" n="HIAT:w" s="T448">nuːnɨčʼɨsɨ</ts>
                  <nts id="Seg_1748" n="HIAT:ip">,</nts>
                  <nts id="Seg_1749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1751" n="HIAT:w" s="T449">šittɨmtäl</ts>
                  <nts id="Seg_1752" n="HIAT:ip">,</nts>
                  <nts id="Seg_1753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1755" n="HIAT:w" s="T450">nɔːrtäl</ts>
                  <nts id="Seg_1756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1758" n="HIAT:w" s="T451">čʼeːlɨ</ts>
                  <nts id="Seg_1759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1761" n="HIAT:w" s="T452">tüntɔːt</ts>
                  <nts id="Seg_1762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1764" n="HIAT:w" s="T453">Poni</ts>
                  <nts id="Seg_1765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1767" n="HIAT:w" s="T454">iralʼ</ts>
                  <nts id="Seg_1768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1770" n="HIAT:w" s="T455">mɨ</ts>
                  <nts id="Seg_1771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1773" n="HIAT:w" s="T456">təpɨtɨtkin</ts>
                  <nts id="Seg_1774" n="HIAT:ip">.</nts>
                  <nts id="Seg_1775" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T467" id="Seg_1777" n="HIAT:u" s="T457">
                  <ts e="T458" id="Seg_1779" n="HIAT:w" s="T457">Na</ts>
                  <nts id="Seg_1780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1782" n="HIAT:w" s="T458">qälɨ</ts>
                  <nts id="Seg_1783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1785" n="HIAT:w" s="T459">iralʼ</ts>
                  <nts id="Seg_1786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1788" n="HIAT:w" s="T460">mɨqä</ts>
                  <nts id="Seg_1789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1791" n="HIAT:w" s="T461">nɔːtɨ</ts>
                  <nts id="Seg_1792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1794" n="HIAT:w" s="T462">taqalqɨnto</ts>
                  <nts id="Seg_1795" n="HIAT:ip">,</nts>
                  <nts id="Seg_1796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1798" n="HIAT:w" s="T463">timnʼasɨqä</ts>
                  <nts id="Seg_1799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1801" n="HIAT:w" s="T464">nɔːtɨ</ts>
                  <nts id="Seg_1802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1803" n="HIAT:ip">(</nts>
                  <ts e="T466" id="Seg_1805" n="HIAT:w" s="T465">tap</ts>
                  <nts id="Seg_1806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1808" n="HIAT:w" s="T466">čʼeːlqot</ts>
                  <nts id="Seg_1809" n="HIAT:ip">)</nts>
                  <nts id="Seg_1810" n="HIAT:ip">.</nts>
                  <nts id="Seg_1811" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T478" id="Seg_1813" n="HIAT:u" s="T467">
                  <ts e="T468" id="Seg_1815" n="HIAT:w" s="T467">Nɨnɨ</ts>
                  <nts id="Seg_1816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1818" n="HIAT:w" s="T468">šʼittalʼ</ts>
                  <nts id="Seg_1819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1821" n="HIAT:w" s="T469">üːtaltɔːt</ts>
                  <nts id="Seg_1822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1824" n="HIAT:w" s="T470">na</ts>
                  <nts id="Seg_1825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1827" n="HIAT:w" s="T471">ɔːtam</ts>
                  <nts id="Seg_1828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1830" n="HIAT:w" s="T472">na</ts>
                  <nts id="Seg_1831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1833" n="HIAT:w" s="T473">tɔːqqɨntɔːt</ts>
                  <nts id="Seg_1834" n="HIAT:ip">,</nts>
                  <nts id="Seg_1835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1837" n="HIAT:w" s="T474">qapij</ts>
                  <nts id="Seg_1838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1840" n="HIAT:w" s="T475">tɛm</ts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1843" n="HIAT:w" s="T476">ɛj</ts>
                  <nts id="Seg_1844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1845" n="HIAT:ip">(</nts>
                  <nts id="Seg_1846" n="HIAT:ip">(</nts>
                  <ats e="T478" id="Seg_1847" n="HIAT:non-pho" s="T477">…</ats>
                  <nts id="Seg_1848" n="HIAT:ip">)</nts>
                  <nts id="Seg_1849" n="HIAT:ip">)</nts>
                  <nts id="Seg_1850" n="HIAT:ip">.</nts>
                  <nts id="Seg_1851" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T481" id="Seg_1853" n="HIAT:u" s="T478">
                  <ts e="T479" id="Seg_1855" n="HIAT:w" s="T478">Mɔːssa</ts>
                  <nts id="Seg_1856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1858" n="HIAT:w" s="T479">na</ts>
                  <nts id="Seg_1859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1861" n="HIAT:w" s="T480">mintɔːt</ts>
                  <nts id="Seg_1862" n="HIAT:ip">.</nts>
                  <nts id="Seg_1863" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T490" id="Seg_1865" n="HIAT:u" s="T481">
                  <ts e="T482" id="Seg_1867" n="HIAT:w" s="T481">Mɔːt</ts>
                  <nts id="Seg_1868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1870" n="HIAT:w" s="T482">šerna</ts>
                  <nts id="Seg_1871" n="HIAT:ip">,</nts>
                  <nts id="Seg_1872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1874" n="HIAT:w" s="T483">na</ts>
                  <nts id="Seg_1875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1877" n="HIAT:w" s="T484">mačʼit</ts>
                  <nts id="Seg_1878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1880" n="HIAT:w" s="T485">tıčʼɨt</ts>
                  <nts id="Seg_1881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1883" n="HIAT:w" s="T486">jennalʼ</ts>
                  <nts id="Seg_1884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1886" n="HIAT:w" s="T487">pɛlalʼ</ts>
                  <nts id="Seg_1887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1889" n="HIAT:w" s="T488">mɔːt</ts>
                  <nts id="Seg_1890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1892" n="HIAT:w" s="T489">šerna</ts>
                  <nts id="Seg_1893" n="HIAT:ip">.</nts>
                  <nts id="Seg_1894" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T495" id="Seg_1896" n="HIAT:u" s="T490">
                  <ts e="T491" id="Seg_1898" n="HIAT:w" s="T490">Nɨn</ts>
                  <nts id="Seg_1899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1901" n="HIAT:w" s="T491">na</ts>
                  <nts id="Seg_1902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1904" n="HIAT:w" s="T492">mɔːtɨŋtɔːqı</ts>
                  <nts id="Seg_1905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1907" n="HIAT:w" s="T493">na</ts>
                  <nts id="Seg_1908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1910" n="HIAT:w" s="T494">imantɨsa</ts>
                  <nts id="Seg_1911" n="HIAT:ip">.</nts>
                  <nts id="Seg_1912" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T501" id="Seg_1914" n="HIAT:u" s="T495">
                  <ts e="T496" id="Seg_1916" n="HIAT:w" s="T495">Qapı</ts>
                  <nts id="Seg_1917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1919" n="HIAT:w" s="T496">təttalʼ</ts>
                  <nts id="Seg_1920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T993" id="Seg_1922" n="HIAT:w" s="T497">qälʼ</ts>
                  <nts id="Seg_1923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1925" n="HIAT:w" s="T993">ira</ts>
                  <nts id="Seg_1926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1928" n="HIAT:w" s="T498">qaj</ts>
                  <nts id="Seg_1929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1931" n="HIAT:w" s="T499">mɔːttɨ</ts>
                  <nts id="Seg_1932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1934" n="HIAT:w" s="T500">čʼäŋka</ts>
                  <nts id="Seg_1935" n="HIAT:ip">.</nts>
                  <nts id="Seg_1936" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T506" id="Seg_1938" n="HIAT:u" s="T501">
                  <ts e="T502" id="Seg_1940" n="HIAT:w" s="T501">Šentɨ</ts>
                  <nts id="Seg_1941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1943" n="HIAT:w" s="T502">mɔːttɨ</ts>
                  <nts id="Seg_1944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1946" n="HIAT:w" s="T503">qopɨlʼ</ts>
                  <nts id="Seg_1947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1949" n="HIAT:w" s="T504">mɔːttɨ</ts>
                  <nts id="Seg_1950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1952" n="HIAT:w" s="T505">tına</ts>
                  <nts id="Seg_1953" n="HIAT:ip">.</nts>
                  <nts id="Seg_1954" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T975" id="Seg_1956" n="HIAT:u" s="T506">
                  <ts e="T507" id="Seg_1958" n="HIAT:w" s="T506">Wərqɨlɔː</ts>
                  <nts id="Seg_1959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T975" id="Seg_1961" n="HIAT:w" s="T507">čʼesɨŋɨtɨ</ts>
                  <nts id="Seg_1962" n="HIAT:ip">.</nts>
                  <nts id="Seg_1963" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T976" id="Seg_1965" n="HIAT:u" s="T975">
                  <nts id="Seg_1966" n="HIAT:ip">(</nts>
                  <nts id="Seg_1967" n="HIAT:ip">(</nts>
                  <ats e="T508" id="Seg_1968" n="HIAT:non-pho" s="T975">NN:</ats>
                  <nts id="Seg_1969" n="HIAT:ip">)</nts>
                  <nts id="Seg_1970" n="HIAT:ip">)</nts>
                  <nts id="Seg_1971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1972" n="HIAT:ip">(</nts>
                  <nts id="Seg_1973" n="HIAT:ip">(</nts>
                  <ats e="T509" id="Seg_1974" n="HIAT:non-pho" s="T508">…</ats>
                  <nts id="Seg_1975" n="HIAT:ip">)</nts>
                  <nts id="Seg_1976" n="HIAT:ip">)</nts>
                  <nts id="Seg_1977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T976" id="Seg_1979" n="HIAT:w" s="T509">koŋɨmpäš</ts>
                  <nts id="Seg_1980" n="HIAT:ip">.</nts>
                  <nts id="Seg_1981" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T990" id="Seg_1983" n="HIAT:u" s="T976">
                  <nts id="Seg_1984" n="HIAT:ip">(</nts>
                  <nts id="Seg_1985" n="HIAT:ip">(</nts>
                  <ats e="T991" id="Seg_1986" n="HIAT:non-pho" s="T976">NN:</ats>
                  <nts id="Seg_1987" n="HIAT:ip">)</nts>
                  <nts id="Seg_1988" n="HIAT:ip">)</nts>
                  <nts id="Seg_1989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T990" id="Seg_1991" n="HIAT:w" s="T991">Бабушка</ts>
                  <nts id="Seg_1992" n="HIAT:ip">?</nts>
                  <nts id="Seg_1993" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T977" id="Seg_1995" n="HIAT:u" s="T990">
                  <nts id="Seg_1996" n="HIAT:ip">(</nts>
                  <nts id="Seg_1997" n="HIAT:ip">(</nts>
                  <ats e="T510" id="Seg_1998" n="HIAT:non-pho" s="T990">NEP:</ats>
                  <nts id="Seg_1999" n="HIAT:ip">)</nts>
                  <nts id="Seg_2000" n="HIAT:ip">)</nts>
                  <nts id="Seg_2001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T977" id="Seg_2003" n="HIAT:w" s="T510">А</ts>
                  <nts id="Seg_2004" n="HIAT:ip">?</nts>
                  <nts id="Seg_2005" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T514" id="Seg_2007" n="HIAT:u" s="T977">
                  <nts id="Seg_2008" n="HIAT:ip">(</nts>
                  <nts id="Seg_2009" n="HIAT:ip">(</nts>
                  <ats e="T511" id="Seg_2010" n="HIAT:non-pho" s="T977">NN:</ats>
                  <nts id="Seg_2011" n="HIAT:ip">)</nts>
                  <nts id="Seg_2012" n="HIAT:ip">)</nts>
                  <nts id="Seg_2013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_2015" n="HIAT:w" s="T511">Na</ts>
                  <nts id="Seg_2016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_2018" n="HIAT:w" s="T512">ıllalɔː</ts>
                  <nts id="Seg_2019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_2021" n="HIAT:w" s="T513">uːkaltäš</ts>
                  <nts id="Seg_2022" n="HIAT:ip">.</nts>
                  <nts id="Seg_2023" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T514.tx.3" id="Seg_2025" n="HIAT:u" s="T514">
                  <nts id="Seg_2026" n="HIAT:ip">(</nts>
                  <nts id="Seg_2027" n="HIAT:ip">(</nts>
                  <ats e="T514.tx.1" id="Seg_2028" n="HIAT:non-pho" s="T514">KuAI:</ats>
                  <nts id="Seg_2029" n="HIAT:ip">)</nts>
                  <nts id="Seg_2030" n="HIAT:ip">)</nts>
                  <nts id="Seg_2031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514.tx.2" id="Seg_2033" n="HIAT:w" s="T514.tx.1">Сейчас</ts>
                  <nts id="Seg_2034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514.tx.3" id="Seg_2036" n="HIAT:w" s="T514.tx.2">попробуем</ts>
                  <nts id="Seg_2037" n="HIAT:ip">.</nts>
                  <nts id="Seg_2038" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T515" id="Seg_2040" n="HIAT:u" s="T514.tx.3">
                  <nts id="Seg_2041" n="HIAT:ip">(</nts>
                  <nts id="Seg_2042" n="HIAT:ip">(</nts>
                  <ats e="T515" id="Seg_2043" n="HIAT:non-pho" s="T514.tx.3">BRK</ats>
                  <nts id="Seg_2044" n="HIAT:ip">)</nts>
                  <nts id="Seg_2045" n="HIAT:ip">)</nts>
                  <nts id="Seg_2046" n="HIAT:ip">.</nts>
                  <nts id="Seg_2047" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T978" id="Seg_2049" n="HIAT:u" s="T515">
                  <nts id="Seg_2050" n="HIAT:ip">(</nts>
                  <nts id="Seg_2051" n="HIAT:ip">(</nts>
                  <ats e="T515.tx.1" id="Seg_2052" n="HIAT:non-pho" s="T515">NN:</ats>
                  <nts id="Seg_2053" n="HIAT:ip">)</nts>
                  <nts id="Seg_2054" n="HIAT:ip">)</nts>
                  <nts id="Seg_2055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2056" n="HIAT:ip">(</nts>
                  <nts id="Seg_2057" n="HIAT:ip">(</nts>
                  <ats e="T515.tx.2" id="Seg_2058" n="HIAT:non-pho" s="T515.tx.1">…</ats>
                  <nts id="Seg_2059" n="HIAT:ip">)</nts>
                  <nts id="Seg_2060" n="HIAT:ip">)</nts>
                  <nts id="Seg_2061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515.tx.3" id="Seg_2063" n="HIAT:w" s="T515.tx.2">а</ts>
                  <nts id="Seg_2064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515.tx.4" id="Seg_2066" n="HIAT:w" s="T515.tx.3">на</ts>
                  <nts id="Seg_2067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515.tx.5" id="Seg_2069" n="HIAT:w" s="T515.tx.4">радио</ts>
                  <nts id="Seg_2070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2071" n="HIAT:ip">(</nts>
                  <nts id="Seg_2072" n="HIAT:ip">(</nts>
                  <ats e="T978" id="Seg_2073" n="HIAT:non-pho" s="T515.tx.5">LAUGH</ats>
                  <nts id="Seg_2074" n="HIAT:ip">)</nts>
                  <nts id="Seg_2075" n="HIAT:ip">)</nts>
                  <nts id="Seg_2076" n="HIAT:ip">…</nts>
                  <nts id="Seg_2077" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T979" id="Seg_2079" n="HIAT:u" s="T978">
                  <nts id="Seg_2080" n="HIAT:ip">(</nts>
                  <nts id="Seg_2081" n="HIAT:ip">(</nts>
                  <ats e="T516" id="Seg_2082" n="HIAT:non-pho" s="T978">NN:</ats>
                  <nts id="Seg_2083" n="HIAT:ip">)</nts>
                  <nts id="Seg_2084" n="HIAT:ip">)</nts>
                  <nts id="Seg_2085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_2087" n="HIAT:w" s="T516">Na</ts>
                  <nts id="Seg_2088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_2090" n="HIAT:w" s="T517">mantɨ</ts>
                  <nts id="Seg_2091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T979" id="Seg_2093" n="HIAT:w" s="T518">sprašivaet</ts>
                  <nts id="Seg_2094" n="HIAT:ip">.</nts>
                  <nts id="Seg_2095" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T980" id="Seg_2097" n="HIAT:u" s="T979">
                  <nts id="Seg_2098" n="HIAT:ip">(</nts>
                  <nts id="Seg_2099" n="HIAT:ip">(</nts>
                  <ats e="T519" id="Seg_2100" n="HIAT:non-pho" s="T979">NN:</ats>
                  <nts id="Seg_2101" n="HIAT:ip">)</nts>
                  <nts id="Seg_2102" n="HIAT:ip">)</nts>
                  <nts id="Seg_2103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_2105" n="HIAT:w" s="T519">Somaŋ</ts>
                  <nts id="Seg_2106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_2108" n="HIAT:w" s="T520">na</ts>
                  <nts id="Seg_2109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_2111" n="HIAT:w" s="T521">koŋɨmmɨntɨ</ts>
                  <nts id="Seg_2112" n="HIAT:ip">,</nts>
                  <nts id="Seg_2113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_2115" n="HIAT:w" s="T522">na</ts>
                  <nts id="Seg_2116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T980" id="Seg_2118" n="HIAT:w" s="T523">koŋaltɨmmɨntɨ</ts>
                  <nts id="Seg_2119" n="HIAT:ip">.</nts>
                  <nts id="Seg_2120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T981" id="Seg_2122" n="HIAT:u" s="T980">
                  <nts id="Seg_2123" n="HIAT:ip">(</nts>
                  <nts id="Seg_2124" n="HIAT:ip">(</nts>
                  <ats e="T524" id="Seg_2125" n="HIAT:non-pho" s="T980">NN:</ats>
                  <nts id="Seg_2126" n="HIAT:ip">)</nts>
                  <nts id="Seg_2127" n="HIAT:ip">)</nts>
                  <nts id="Seg_2128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_2130" n="HIAT:w" s="T524">Tal</ts>
                  <nts id="Seg_2131" n="HIAT:ip">,</nts>
                  <nts id="Seg_2132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_2134" n="HIAT:w" s="T525">ɨkɨ</ts>
                  <nts id="Seg_2135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T981" id="Seg_2137" n="HIAT:w" s="T526">mortäš</ts>
                  <nts id="Seg_2138" n="HIAT:ip">.</nts>
                  <nts id="Seg_2139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T982" id="Seg_2141" n="HIAT:u" s="T981">
                  <nts id="Seg_2142" n="HIAT:ip">(</nts>
                  <nts id="Seg_2143" n="HIAT:ip">(</nts>
                  <ats e="T527" id="Seg_2144" n="HIAT:non-pho" s="T981">NN:</ats>
                  <nts id="Seg_2145" n="HIAT:ip">)</nts>
                  <nts id="Seg_2146" n="HIAT:ip">)</nts>
                  <nts id="Seg_2147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_2149" n="HIAT:w" s="T527">Qapija</ts>
                  <nts id="Seg_2150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_2152" n="HIAT:w" s="T528">qoːkɨtalʼ</ts>
                  <nts id="Seg_2153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2154" n="HIAT:ip">(</nts>
                  <nts id="Seg_2155" n="HIAT:ip">(</nts>
                  <ats e="T982" id="Seg_2156" n="HIAT:non-pho" s="T529">…</ats>
                  <nts id="Seg_2157" n="HIAT:ip">)</nts>
                  <nts id="Seg_2158" n="HIAT:ip">)</nts>
                  <nts id="Seg_2159" n="HIAT:ip">.</nts>
                  <nts id="Seg_2160" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T983" id="Seg_2162" n="HIAT:u" s="T982">
                  <nts id="Seg_2163" n="HIAT:ip">(</nts>
                  <nts id="Seg_2164" n="HIAT:ip">(</nts>
                  <ats e="T530" id="Seg_2165" n="HIAT:non-pho" s="T982">NN:</ats>
                  <nts id="Seg_2166" n="HIAT:ip">)</nts>
                  <nts id="Seg_2167" n="HIAT:ip">)</nts>
                  <nts id="Seg_2168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_2170" n="HIAT:w" s="T530">Qapı</ts>
                  <nts id="Seg_2171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_2173" n="HIAT:w" s="T531">üŋkɨltɨmpatɨ</ts>
                  <nts id="Seg_2174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532.tx.1" id="Seg_2176" n="HIAT:w" s="T532">moʒet</ts>
                  <nts id="Seg_2177" n="HIAT:ip">_</nts>
                  <ts e="T533" id="Seg_2179" n="HIAT:w" s="T532.tx.1">pɨtʼ</ts>
                  <nts id="Seg_2180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_2182" n="HIAT:w" s="T533">qoškɔːl</ts>
                  <nts id="Seg_2183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T983" id="Seg_2185" n="HIAT:w" s="T534">kətɨŋɨtɨ</ts>
                  <nts id="Seg_2186" n="HIAT:ip">.</nts>
                  <nts id="Seg_2187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T974" id="Seg_2189" n="HIAT:u" s="T983">
                  <nts id="Seg_2190" n="HIAT:ip">(</nts>
                  <nts id="Seg_2191" n="HIAT:ip">(</nts>
                  <ats e="T535" id="Seg_2192" n="HIAT:non-pho" s="T983">NN:</ats>
                  <nts id="Seg_2193" n="HIAT:ip">)</nts>
                  <nts id="Seg_2194" n="HIAT:ip">)</nts>
                  <nts id="Seg_2195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T974" id="Seg_2197" n="HIAT:w" s="T535">Əːtɨkɔːl</ts>
                  <nts id="Seg_2198" n="HIAT:ip">.</nts>
                  <nts id="Seg_2199" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T542" id="Seg_2201" n="HIAT:u" s="T974">
                  <nts id="Seg_2202" n="HIAT:ip">(</nts>
                  <nts id="Seg_2203" n="HIAT:ip">(</nts>
                  <ats e="T536" id="Seg_2204" n="HIAT:non-pho" s="T974">NEP:</ats>
                  <nts id="Seg_2205" n="HIAT:ip">)</nts>
                  <nts id="Seg_2206" n="HIAT:ip">)</nts>
                  <nts id="Seg_2207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_2209" n="HIAT:w" s="T536">Nɨnɨ</ts>
                  <nts id="Seg_2210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_2212" n="HIAT:w" s="T537">šittäl</ts>
                  <nts id="Seg_2213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_2215" n="HIAT:w" s="T538">šäqqɔːj</ts>
                  <nts id="Seg_2216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_2218" n="HIAT:w" s="T539">na</ts>
                  <nts id="Seg_2219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_2221" n="HIAT:w" s="T540">ija</ts>
                  <nts id="Seg_2222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_2224" n="HIAT:w" s="T541">imantɨsa</ts>
                  <nts id="Seg_2225" n="HIAT:ip">.</nts>
                  <nts id="Seg_2226" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T549" id="Seg_2228" n="HIAT:u" s="T542">
                  <ts e="T543" id="Seg_2230" n="HIAT:w" s="T542">Šäqqɔːt</ts>
                  <nts id="Seg_2231" n="HIAT:ip">,</nts>
                  <nts id="Seg_2232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_2234" n="HIAT:w" s="T543">toptɨlʼ</ts>
                  <nts id="Seg_2235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_2237" n="HIAT:w" s="T544">qar</ts>
                  <nts id="Seg_2238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_2240" n="HIAT:w" s="T545">nılʼ</ts>
                  <nts id="Seg_2241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_2243" n="HIAT:w" s="T546">ɛsa</ts>
                  <nts id="Seg_2244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_2246" n="HIAT:w" s="T547">tına</ts>
                  <nts id="Seg_2247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_2249" n="HIAT:w" s="T548">imantɨn</ts>
                  <nts id="Seg_2250" n="HIAT:ip">.</nts>
                  <nts id="Seg_2251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T556" id="Seg_2253" n="HIAT:u" s="T549">
                  <ts e="T550" id="Seg_2255" n="HIAT:w" s="T549">Man</ts>
                  <nts id="Seg_2256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_2258" n="HIAT:w" s="T550">mɨta</ts>
                  <nts id="Seg_2259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2261" n="HIAT:w" s="T551">nʼenna</ts>
                  <nts id="Seg_2262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_2264" n="HIAT:w" s="T552">qumiːqan</ts>
                  <nts id="Seg_2265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2267" n="HIAT:w" s="T553">qonʼišak</ts>
                  <nts id="Seg_2268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_2270" n="HIAT:w" s="T554">qaj</ts>
                  <nts id="Seg_2271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2273" n="HIAT:w" s="T555">mɨta</ts>
                  <nts id="Seg_2274" n="HIAT:ip">.</nts>
                  <nts id="Seg_2275" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T559" id="Seg_2277" n="HIAT:u" s="T556">
                  <ts e="T557" id="Seg_2279" n="HIAT:w" s="T556">Mɔːtɨŋpɔːtɨn</ts>
                  <nts id="Seg_2280" n="HIAT:ip">,</nts>
                  <nts id="Seg_2281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_2283" n="HIAT:w" s="T557">qaj</ts>
                  <nts id="Seg_2284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2286" n="HIAT:w" s="T558">qattɨmpɔːtɨn</ts>
                  <nts id="Seg_2287" n="HIAT:ip">.</nts>
                  <nts id="Seg_2288" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T564" id="Seg_2290" n="HIAT:u" s="T559">
                  <ts e="T560" id="Seg_2292" n="HIAT:w" s="T559">Nʼenna</ts>
                  <nts id="Seg_2293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2295" n="HIAT:w" s="T560">laqaltɛlʼčʼa</ts>
                  <nts id="Seg_2296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2298" n="HIAT:w" s="T561">nɔːkɨr</ts>
                  <nts id="Seg_2299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_2301" n="HIAT:w" s="T562">qoptɨp</ts>
                  <nts id="Seg_2302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2304" n="HIAT:w" s="T563">sɔːrɨla</ts>
                  <nts id="Seg_2305" n="HIAT:ip">.</nts>
                  <nts id="Seg_2306" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T569" id="Seg_2308" n="HIAT:u" s="T564">
                  <ts e="T565" id="Seg_2310" n="HIAT:w" s="T564">Qapı</ts>
                  <nts id="Seg_2311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_2313" n="HIAT:w" s="T565">nɔːssarɨlʼ</ts>
                  <nts id="Seg_2314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2316" n="HIAT:w" s="T566">ɔːta</ts>
                  <nts id="Seg_2317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_2319" n="HIAT:w" s="T567">qaltɨmɨntɨt</ts>
                  <nts id="Seg_2320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_2322" n="HIAT:w" s="T568">na</ts>
                  <nts id="Seg_2323" n="HIAT:ip">.</nts>
                  <nts id="Seg_2324" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T576" id="Seg_2326" n="HIAT:u" s="T569">
                  <ts e="T570" id="Seg_2328" n="HIAT:w" s="T569">Ija</ts>
                  <nts id="Seg_2329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2331" n="HIAT:w" s="T570">nʼenna</ts>
                  <nts id="Seg_2332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2334" n="HIAT:w" s="T571">qənaisɨ</ts>
                  <nts id="Seg_2335" n="HIAT:ip">,</nts>
                  <nts id="Seg_2336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_2338" n="HIAT:w" s="T572">imatɨ</ts>
                  <nts id="Seg_2339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2341" n="HIAT:w" s="T573">ontɨ</ts>
                  <nts id="Seg_2342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_2344" n="HIAT:w" s="T574">qala</ts>
                  <nts id="Seg_2345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2347" n="HIAT:w" s="T575">pɛläkɔːl</ts>
                  <nts id="Seg_2348" n="HIAT:ip">.</nts>
                  <nts id="Seg_2349" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T578" id="Seg_2351" n="HIAT:u" s="T576">
                  <ts e="T577" id="Seg_2353" n="HIAT:w" s="T576">Na</ts>
                  <nts id="Seg_2354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2356" n="HIAT:w" s="T577">ɔːmtɨŋa</ts>
                  <nts id="Seg_2357" n="HIAT:ip">.</nts>
                  <nts id="Seg_2358" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T581" id="Seg_2360" n="HIAT:u" s="T578">
                  <ts e="T579" id="Seg_2362" n="HIAT:w" s="T578">Ompa</ts>
                  <nts id="Seg_2363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2365" n="HIAT:w" s="T579">na</ts>
                  <nts id="Seg_2366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2367" n="HIAT:ip">(</nts>
                  <nts id="Seg_2368" n="HIAT:ip">(</nts>
                  <ats e="T581" id="Seg_2369" n="HIAT:non-pho" s="T580">…</ats>
                  <nts id="Seg_2370" n="HIAT:ip">)</nts>
                  <nts id="Seg_2371" n="HIAT:ip">)</nts>
                  <nts id="Seg_2372" n="HIAT:ip">.</nts>
                  <nts id="Seg_2373" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T588" id="Seg_2375" n="HIAT:u" s="T581">
                  <ts e="T582" id="Seg_2377" n="HIAT:w" s="T581">Ukkɨr</ts>
                  <nts id="Seg_2378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2380" n="HIAT:w" s="T582">tot</ts>
                  <nts id="Seg_2381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2383" n="HIAT:w" s="T583">čʼontot</ts>
                  <nts id="Seg_2384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2386" n="HIAT:w" s="T584">čʼeːlɨtɨ</ts>
                  <nts id="Seg_2387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2389" n="HIAT:w" s="T585">üːtontɨ</ts>
                  <nts id="Seg_2390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2392" n="HIAT:w" s="T586">na</ts>
                  <nts id="Seg_2393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2395" n="HIAT:w" s="T587">qəntɨ</ts>
                  <nts id="Seg_2396" n="HIAT:ip">.</nts>
                  <nts id="Seg_2397" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T595" id="Seg_2399" n="HIAT:u" s="T588">
                  <ts e="T589" id="Seg_2401" n="HIAT:w" s="T588">Üːtontɨqɨn</ts>
                  <nts id="Seg_2402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2404" n="HIAT:w" s="T589">na</ts>
                  <nts id="Seg_2405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2407" n="HIAT:w" s="T590">ija</ts>
                  <nts id="Seg_2408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2410" n="HIAT:w" s="T591">lʼamɨk</ts>
                  <nts id="Seg_2411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2413" n="HIAT:w" s="T592">ɛːŋa</ts>
                  <nts id="Seg_2414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2416" n="HIAT:w" s="T593">qapı</ts>
                  <nts id="Seg_2417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2419" n="HIAT:w" s="T594">nʼenna</ts>
                  <nts id="Seg_2420" n="HIAT:ip">.</nts>
                  <nts id="Seg_2421" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T604" id="Seg_2423" n="HIAT:u" s="T595">
                  <ts e="T596" id="Seg_2425" n="HIAT:w" s="T595">Ukkɨr</ts>
                  <nts id="Seg_2426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2428" n="HIAT:w" s="T596">tät</ts>
                  <nts id="Seg_2429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2431" n="HIAT:w" s="T597">čʼontot</ts>
                  <nts id="Seg_2432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2434" n="HIAT:w" s="T598">mɨ</ts>
                  <nts id="Seg_2435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2437" n="HIAT:w" s="T599">qum</ts>
                  <nts id="Seg_2438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2439" n="HIAT:ip">(</nts>
                  <ts e="T601" id="Seg_2441" n="HIAT:w" s="T600">na</ts>
                  <nts id="Seg_2442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2444" n="HIAT:w" s="T601">jap</ts>
                  <nts id="Seg_2445" n="HIAT:ip">)</nts>
                  <nts id="Seg_2446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2448" n="HIAT:w" s="T602">tattɨntɔːt</ts>
                  <nts id="Seg_2449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2451" n="HIAT:w" s="T603">takkɨntɨt</ts>
                  <nts id="Seg_2452" n="HIAT:ip">.</nts>
                  <nts id="Seg_2453" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T608" id="Seg_2455" n="HIAT:u" s="T604">
                  <ts e="T605" id="Seg_2457" n="HIAT:w" s="T604">Poni</ts>
                  <nts id="Seg_2458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2460" n="HIAT:w" s="T605">iralʼ</ts>
                  <nts id="Seg_2461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2462" n="HIAT:ip">(</nts>
                  <ts e="T967" id="Seg_2464" n="HIAT:w" s="T606">na</ts>
                  <nts id="Seg_2465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2467" n="HIAT:w" s="T967">jap</ts>
                  <nts id="Seg_2468" n="HIAT:ip">)</nts>
                  <nts id="Seg_2469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2471" n="HIAT:w" s="T607">tattɨntɔːtɨt</ts>
                  <nts id="Seg_2472" n="HIAT:ip">.</nts>
                  <nts id="Seg_2473" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T613" id="Seg_2475" n="HIAT:u" s="T608">
                  <ts e="T609" id="Seg_2477" n="HIAT:w" s="T608">Köt</ts>
                  <nts id="Seg_2478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2480" n="HIAT:w" s="T609">qumiːtɨ</ts>
                  <nts id="Seg_2481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2483" n="HIAT:w" s="T610">qapı</ts>
                  <nts id="Seg_2484" n="HIAT:ip">–</nts>
                  <nts id="Seg_2485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2487" n="HIAT:w" s="T611">ɔːtap</ts>
                  <nts id="Seg_2488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2490" n="HIAT:w" s="T612">tɔːqqɨmpɨlʼ</ts>
                  <nts id="Seg_2491" n="HIAT:ip">.</nts>
                  <nts id="Seg_2492" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T621" id="Seg_2494" n="HIAT:u" s="T613">
                  <ts e="T614" id="Seg_2496" n="HIAT:w" s="T613">Poni</ts>
                  <nts id="Seg_2497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2499" n="HIAT:w" s="T614">ira</ts>
                  <nts id="Seg_2500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2502" n="HIAT:w" s="T615">naššak</ts>
                  <nts id="Seg_2503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2505" n="HIAT:w" s="T616">qotɨlʼ</ts>
                  <nts id="Seg_2506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2508" n="HIAT:w" s="T617">ɛːŋa</ts>
                  <nts id="Seg_2509" n="HIAT:ip">,</nts>
                  <nts id="Seg_2510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2512" n="HIAT:w" s="T618">naššak</ts>
                  <nts id="Seg_2513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2515" n="HIAT:w" s="T619">qəːtɨsä</ts>
                  <nts id="Seg_2516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2518" n="HIAT:w" s="T620">ɛːŋa</ts>
                  <nts id="Seg_2519" n="HIAT:ip">.</nts>
                  <nts id="Seg_2520" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T630" id="Seg_2522" n="HIAT:u" s="T621">
                  <ts e="T622" id="Seg_2524" n="HIAT:w" s="T621">Nɨnɨ</ts>
                  <nts id="Seg_2525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2527" n="HIAT:w" s="T622">šittäl</ts>
                  <nts id="Seg_2528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2530" n="HIAT:w" s="T623">tal</ts>
                  <nts id="Seg_2531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2533" n="HIAT:w" s="T624">na</ts>
                  <nts id="Seg_2534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2536" n="HIAT:w" s="T625">ija</ts>
                  <nts id="Seg_2537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2539" n="HIAT:w" s="T626">tülʼčʼikunä</ts>
                  <nts id="Seg_2540" n="HIAT:ip">–</nts>
                  <nts id="Seg_2541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2543" n="HIAT:w" s="T627">ponä</ts>
                  <nts id="Seg_2544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2546" n="HIAT:w" s="T628">imatɨ</ts>
                  <nts id="Seg_2547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2549" n="HIAT:w" s="T629">üŋkɨltɨmpa</ts>
                  <nts id="Seg_2550" n="HIAT:ip">.</nts>
                  <nts id="Seg_2551" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T636" id="Seg_2553" n="HIAT:u" s="T630">
                  <ts e="T631" id="Seg_2555" n="HIAT:w" s="T630">Tülʼčʼikunä</ts>
                  <nts id="Seg_2556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2558" n="HIAT:w" s="T631">na</ts>
                  <nts id="Seg_2559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2561" n="HIAT:w" s="T632">ɔːtamtɨ</ts>
                  <nts id="Seg_2562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2564" n="HIAT:w" s="T633">toː</ts>
                  <nts id="Seg_2565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2567" n="HIAT:w" s="T634">sɔːra</ts>
                  <nts id="Seg_2568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2570" n="HIAT:w" s="T635">ɔːtamtɨ</ts>
                  <nts id="Seg_2571" n="HIAT:ip">.</nts>
                  <nts id="Seg_2572" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T984" id="Seg_2574" n="HIAT:u" s="T636">
                  <nts id="Seg_2575" n="HIAT:ip">(</nts>
                  <nts id="Seg_2576" n="HIAT:ip">(</nts>
                  <ats e="T636.tx.1" id="Seg_2577" n="HIAT:non-pho" s="T636">NN:</ats>
                  <nts id="Seg_2578" n="HIAT:ip">)</nts>
                  <nts id="Seg_2579" n="HIAT:ip">)</nts>
                  <nts id="Seg_2580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2581" n="HIAT:ip">(</nts>
                  <nts id="Seg_2582" n="HIAT:ip">(</nts>
                  <ats e="T984" id="Seg_2583" n="HIAT:non-pho" s="T636.tx.1">…</ats>
                  <nts id="Seg_2584" n="HIAT:ip">)</nts>
                  <nts id="Seg_2585" n="HIAT:ip">)</nts>
                  <nts id="Seg_2586" n="HIAT:ip">.</nts>
                  <nts id="Seg_2587" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T641" id="Seg_2589" n="HIAT:u" s="T984">
                  <nts id="Seg_2590" n="HIAT:ip">(</nts>
                  <nts id="Seg_2591" n="HIAT:ip">(</nts>
                  <ats e="T637" id="Seg_2592" n="HIAT:non-pho" s="T984">NEP:</ats>
                  <nts id="Seg_2593" n="HIAT:ip">)</nts>
                  <nts id="Seg_2594" n="HIAT:ip">)</nts>
                  <nts id="Seg_2595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2597" n="HIAT:w" s="T637">Montɨ</ts>
                  <nts id="Seg_2598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2600" n="HIAT:w" s="T638">čʼap</ts>
                  <nts id="Seg_2601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2603" n="HIAT:w" s="T639">tülʼčʼa</ts>
                  <nts id="Seg_2604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2606" n="HIAT:w" s="T640">montɨ</ts>
                  <nts id="Seg_2607" n="HIAT:ip">.</nts>
                  <nts id="Seg_2608" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T651" id="Seg_2610" n="HIAT:u" s="T641">
                  <ts e="T642" id="Seg_2612" n="HIAT:w" s="T641">Poni</ts>
                  <nts id="Seg_2613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2615" n="HIAT:w" s="T642">iralʼ</ts>
                  <nts id="Seg_2616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2618" n="HIAT:w" s="T643">mɨt</ts>
                  <nts id="Seg_2619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2621" n="HIAT:w" s="T644">qaj</ts>
                  <nts id="Seg_2622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2624" n="HIAT:w" s="T645">montɨ</ts>
                  <nts id="Seg_2625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2627" n="HIAT:w" s="T646">tüŋɔːtɨt</ts>
                  <nts id="Seg_2628" n="HIAT:ip">,</nts>
                  <nts id="Seg_2629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2631" n="HIAT:w" s="T647">qaqlɨlʼ</ts>
                  <nts id="Seg_2632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2634" n="HIAT:w" s="T648">mɨtɨt</ts>
                  <nts id="Seg_2635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2637" n="HIAT:w" s="T649">nık</ts>
                  <nts id="Seg_2638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_2640" n="HIAT:w" s="T650">tottälɨmpɔːt</ts>
                  <nts id="Seg_2641" n="HIAT:ip">.</nts>
                  <nts id="Seg_2642" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T655" id="Seg_2644" n="HIAT:u" s="T651">
                  <ts e="T652" id="Seg_2646" n="HIAT:w" s="T651">Tına</ts>
                  <nts id="Seg_2647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2649" n="HIAT:w" s="T652">ətɨmantot</ts>
                  <nts id="Seg_2650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2652" n="HIAT:w" s="T653">təpɨt</ts>
                  <nts id="Seg_2653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2655" n="HIAT:w" s="T654">ətɨmantot</ts>
                  <nts id="Seg_2656" n="HIAT:ip">.</nts>
                  <nts id="Seg_2657" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T662" id="Seg_2659" n="HIAT:u" s="T655">
                  <ts e="T656" id="Seg_2661" n="HIAT:w" s="T655">Šerna</ts>
                  <nts id="Seg_2662" n="HIAT:ip">,</nts>
                  <nts id="Seg_2663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2665" n="HIAT:w" s="T656">mɔːt</ts>
                  <nts id="Seg_2666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2668" n="HIAT:w" s="T657">šerna</ts>
                  <nts id="Seg_2669" n="HIAT:ip">,</nts>
                  <nts id="Seg_2670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2672" n="HIAT:w" s="T658">mɔːtan</ts>
                  <nts id="Seg_2673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2675" n="HIAT:w" s="T659">ɔːt</ts>
                  <nts id="Seg_2676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2678" n="HIAT:w" s="T660">nɨŋkɨla</ts>
                  <nts id="Seg_2679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2681" n="HIAT:w" s="T661">qälɨk</ts>
                  <nts id="Seg_2682" n="HIAT:ip">.</nts>
                  <nts id="Seg_2683" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T668" id="Seg_2685" n="HIAT:u" s="T662">
                  <ts e="T663" id="Seg_2687" n="HIAT:w" s="T662">Tokalʼ</ts>
                  <nts id="Seg_2688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2690" n="HIAT:w" s="T663">mɔːtan</ts>
                  <nts id="Seg_2691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2693" n="HIAT:w" s="T664">ɔːt</ts>
                  <nts id="Seg_2694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2696" n="HIAT:w" s="T665">qät</ts>
                  <nts id="Seg_2697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2699" n="HIAT:w" s="T666">topɨmt</ts>
                  <nts id="Seg_2700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2702" n="HIAT:w" s="T667">tupalna</ts>
                  <nts id="Seg_2703" n="HIAT:ip">.</nts>
                  <nts id="Seg_2704" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T671" id="Seg_2706" n="HIAT:u" s="T668">
                  <ts e="T669" id="Seg_2708" n="HIAT:w" s="T668">Peːmɨmtɨ</ts>
                  <nts id="Seg_2709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2711" n="HIAT:w" s="T669">na</ts>
                  <nts id="Seg_2712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2714" n="HIAT:w" s="T670">tupaltɨ</ts>
                  <nts id="Seg_2715" n="HIAT:ip">.</nts>
                  <nts id="Seg_2716" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T675" id="Seg_2718" n="HIAT:u" s="T671">
                  <ts e="T672" id="Seg_2720" n="HIAT:w" s="T671">Qannɨmpɨlʼ</ts>
                  <nts id="Seg_2721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2723" n="HIAT:w" s="T672">pɛlalʼ</ts>
                  <nts id="Seg_2724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2726" n="HIAT:w" s="T673">peːmɨtɨ</ts>
                  <nts id="Seg_2727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2729" n="HIAT:w" s="T674">tupalnɨt</ts>
                  <nts id="Seg_2730" n="HIAT:ip">.</nts>
                  <nts id="Seg_2731" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T682" id="Seg_2733" n="HIAT:u" s="T675">
                  <ts e="T676" id="Seg_2735" n="HIAT:w" s="T675">Nık</ts>
                  <nts id="Seg_2736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2738" n="HIAT:w" s="T676">kətɨŋɨtɨ</ts>
                  <nts id="Seg_2739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2741" n="HIAT:w" s="T677">nʼenna</ts>
                  <nts id="Seg_2742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2744" n="HIAT:w" s="T678">koralla</ts>
                  <nts id="Seg_2745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2747" n="HIAT:w" s="T679">Poni</ts>
                  <nts id="Seg_2748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2750" n="HIAT:w" s="T680">iran</ts>
                  <nts id="Seg_2751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2753" n="HIAT:w" s="T681">mɨta</ts>
                  <nts id="Seg_2754" n="HIAT:ip">.</nts>
                  <nts id="Seg_2755" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T688" id="Seg_2757" n="HIAT:u" s="T682">
                  <ts e="T683" id="Seg_2759" n="HIAT:w" s="T682">Ɔːklɨ</ts>
                  <nts id="Seg_2760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2762" n="HIAT:w" s="T683">mitɨ</ts>
                  <nts id="Seg_2763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2765" n="HIAT:w" s="T684">mɨta</ts>
                  <nts id="Seg_2766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2768" n="HIAT:w" s="T685">qolqol</ts>
                  <nts id="Seg_2769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2771" n="HIAT:w" s="T686">olʼa</ts>
                  <nts id="Seg_2772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2774" n="HIAT:w" s="T687">mɨ</ts>
                  <nts id="Seg_2775" n="HIAT:ip">.</nts>
                  <nts id="Seg_2776" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T689" id="Seg_2778" n="HIAT:u" s="T688">
                  <ts e="T689" id="Seg_2780" n="HIAT:w" s="T688">Taŋal</ts>
                  <nts id="Seg_2781" n="HIAT:ip">.</nts>
                  <nts id="Seg_2782" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T694" id="Seg_2784" n="HIAT:u" s="T689">
                  <ts e="T690" id="Seg_2786" n="HIAT:w" s="T689">Na</ts>
                  <nts id="Seg_2787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2789" n="HIAT:w" s="T690">nʼenna</ts>
                  <nts id="Seg_2790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2792" n="HIAT:w" s="T691">paktɨmmɨntɨ</ts>
                  <nts id="Seg_2793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2795" n="HIAT:w" s="T692">na</ts>
                  <nts id="Seg_2796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2798" n="HIAT:w" s="T693">kupaks</ts>
                  <nts id="Seg_2799" n="HIAT:ip">.</nts>
                  <nts id="Seg_2800" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T698" id="Seg_2802" n="HIAT:u" s="T694">
                  <ts e="T695" id="Seg_2804" n="HIAT:w" s="T694">Təːqalaimmɨntɨtɨ</ts>
                  <nts id="Seg_2805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2807" n="HIAT:w" s="T695">pɛlaj</ts>
                  <nts id="Seg_2808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2810" n="HIAT:w" s="T696">utɨntɨ</ts>
                  <nts id="Seg_2811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2813" n="HIAT:w" s="T697">təːqalaimpat</ts>
                  <nts id="Seg_2814" n="HIAT:ip">.</nts>
                  <nts id="Seg_2815" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T704" id="Seg_2817" n="HIAT:u" s="T698">
                  <ts e="T699" id="Seg_2819" n="HIAT:w" s="T698">Poni</ts>
                  <nts id="Seg_2820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2822" n="HIAT:w" s="T699">iralʼ</ts>
                  <nts id="Seg_2823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2825" n="HIAT:w" s="T700">mɨtɨp</ts>
                  <nts id="Seg_2826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2828" n="HIAT:w" s="T701">muntɨk</ts>
                  <nts id="Seg_2829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2831" n="HIAT:w" s="T702">lɛpäk</ts>
                  <nts id="Seg_2832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_2834" n="HIAT:w" s="T703">qättɨmpatɨ</ts>
                  <nts id="Seg_2835" n="HIAT:ip">.</nts>
                  <nts id="Seg_2836" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T706" id="Seg_2838" n="HIAT:u" s="T704">
                  <ts e="T705" id="Seg_2840" n="HIAT:w" s="T704">Ɔːmɨtɨ</ts>
                  <nts id="Seg_2841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2843" n="HIAT:w" s="T705">qumpa</ts>
                  <nts id="Seg_2844" n="HIAT:ip">.</nts>
                  <nts id="Seg_2845" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T718" id="Seg_2847" n="HIAT:u" s="T706">
                  <ts e="T707" id="Seg_2849" n="HIAT:w" s="T706">Poni</ts>
                  <nts id="Seg_2850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_2852" n="HIAT:w" s="T707">ira</ts>
                  <nts id="Seg_2853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_2855" n="HIAT:w" s="T708">illa</ts>
                  <nts id="Seg_2856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2858" n="HIAT:w" s="T709">qalɨmtɨ</ts>
                  <nts id="Seg_2859" n="HIAT:ip">,</nts>
                  <nts id="Seg_2860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_2862" n="HIAT:w" s="T710">šittɨ</ts>
                  <nts id="Seg_2863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2865" n="HIAT:w" s="T711">qälɨqı</ts>
                  <nts id="Seg_2866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2868" n="HIAT:w" s="T712">illa</ts>
                  <nts id="Seg_2869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2871" n="HIAT:w" s="T713">qalɨmtɨ</ts>
                  <nts id="Seg_2872" n="HIAT:ip">,</nts>
                  <nts id="Seg_2873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2875" n="HIAT:w" s="T714">pona</ts>
                  <nts id="Seg_2876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_2878" n="HIAT:w" s="T715">qəːllaıːntɨ</ts>
                  <nts id="Seg_2879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2881" n="HIAT:w" s="T716">na</ts>
                  <nts id="Seg_2882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2884" n="HIAT:w" s="T717">ija</ts>
                  <nts id="Seg_2885" n="HIAT:ip">.</nts>
                  <nts id="Seg_2886" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T722" id="Seg_2888" n="HIAT:u" s="T718">
                  <ts e="T719" id="Seg_2890" n="HIAT:w" s="T718">Ukkɨr</ts>
                  <nts id="Seg_2891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2893" n="HIAT:w" s="T719">pɔːr</ts>
                  <nts id="Seg_2894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_2896" n="HIAT:w" s="T720">kupaksä</ts>
                  <nts id="Seg_2897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_2899" n="HIAT:w" s="T721">təːqalaimpat</ts>
                  <nts id="Seg_2900" n="HIAT:ip">.</nts>
                  <nts id="Seg_2901" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T730" id="Seg_2903" n="HIAT:u" s="T722">
                  <ts e="T723" id="Seg_2905" n="HIAT:w" s="T722">Muntɨk</ts>
                  <nts id="Seg_2906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_2908" n="HIAT:w" s="T723">moqɨnät</ts>
                  <nts id="Seg_2909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_2911" n="HIAT:w" s="T724">porqɨn</ts>
                  <nts id="Seg_2912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_2914" n="HIAT:w" s="T725">šunʼnʼontɨ</ts>
                  <nts id="Seg_2915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2917" n="HIAT:w" s="T726">nʼolʼqɨmɔːtpɔːtɨn</ts>
                  <nts id="Seg_2918" n="HIAT:ip">,</nts>
                  <nts id="Seg_2919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_2921" n="HIAT:w" s="T727">na</ts>
                  <nts id="Seg_2922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2924" n="HIAT:w" s="T728">qumpɨlʼ</ts>
                  <nts id="Seg_2925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2927" n="HIAT:w" s="T729">mɨ</ts>
                  <nts id="Seg_2928" n="HIAT:ip">.</nts>
                  <nts id="Seg_2929" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T731" id="Seg_2931" n="HIAT:u" s="T730">
                  <ts e="T731" id="Seg_2933" n="HIAT:w" s="T730">Pista</ts>
                  <nts id="Seg_2934" n="HIAT:ip">.</nts>
                  <nts id="Seg_2935" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T744" id="Seg_2937" n="HIAT:u" s="T731">
                  <ts e="T732" id="Seg_2939" n="HIAT:w" s="T731">Nɨnɨ</ts>
                  <nts id="Seg_2940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2942" n="HIAT:w" s="T732">šittäl</ts>
                  <nts id="Seg_2943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2945" n="HIAT:w" s="T733">ponä</ts>
                  <nts id="Seg_2946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_2948" n="HIAT:w" s="T734">qəllaıːŋɨtɨ</ts>
                  <nts id="Seg_2949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_2951" n="HIAT:w" s="T735">ətɨmantontɨ</ts>
                  <nts id="Seg_2952" n="HIAT:ip">,</nts>
                  <nts id="Seg_2953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_2955" n="HIAT:w" s="T736">tınta</ts>
                  <nts id="Seg_2956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_2958" n="HIAT:w" s="T737">na</ts>
                  <nts id="Seg_2959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_2961" n="HIAT:w" s="T738">qaqlɨt</ts>
                  <nts id="Seg_2962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2963" n="HIAT:ip">(</nts>
                  <ts e="T740" id="Seg_2965" n="HIAT:w" s="T739">tolʼ</ts>
                  <nts id="Seg_2966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_2968" n="HIAT:w" s="T740">kečʼit</ts>
                  <nts id="Seg_2969" n="HIAT:ip">)</nts>
                  <nts id="Seg_2970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_2972" n="HIAT:w" s="T741">poːqɨn</ts>
                  <nts id="Seg_2973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_2975" n="HIAT:w" s="T742">na</ts>
                  <nts id="Seg_2976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_2978" n="HIAT:w" s="T743">sɔːralɨmmɨntɔːtɨt</ts>
                  <nts id="Seg_2979" n="HIAT:ip">.</nts>
                  <nts id="Seg_2980" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T753" id="Seg_2982" n="HIAT:u" s="T744">
                  <ts e="T745" id="Seg_2984" n="HIAT:w" s="T744">Poni</ts>
                  <nts id="Seg_2985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_2987" n="HIAT:w" s="T745">ira</ts>
                  <nts id="Seg_2988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_2990" n="HIAT:w" s="T746">ɛj</ts>
                  <nts id="Seg_2991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_2993" n="HIAT:w" s="T747">ponä</ts>
                  <nts id="Seg_2994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_2996" n="HIAT:w" s="T748">iːtɨmpatɨ</ts>
                  <nts id="Seg_2997" n="HIAT:ip">,</nts>
                  <nts id="Seg_2998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_3000" n="HIAT:w" s="T749">qaj</ts>
                  <nts id="Seg_3001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_3003" n="HIAT:w" s="T750">muntɨk</ts>
                  <nts id="Seg_3004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_3006" n="HIAT:w" s="T751">ponä</ts>
                  <nts id="Seg_3007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_3009" n="HIAT:w" s="T752">iːtɨmpatɨ</ts>
                  <nts id="Seg_3010" n="HIAT:ip">.</nts>
                  <nts id="Seg_3011" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T763" id="Seg_3013" n="HIAT:u" s="T753">
                  <ts e="T754" id="Seg_3015" n="HIAT:w" s="T753">İlla</ts>
                  <nts id="Seg_3016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_3018" n="HIAT:w" s="T754">omta</ts>
                  <nts id="Seg_3019" n="HIAT:ip">,</nts>
                  <nts id="Seg_3020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_3022" n="HIAT:w" s="T755">tına</ts>
                  <nts id="Seg_3023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_3025" n="HIAT:w" s="T756">malʼčʼalʼ</ts>
                  <nts id="Seg_3026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_3028" n="HIAT:w" s="T757">toktɨ</ts>
                  <nts id="Seg_3029" n="HIAT:ip">,</nts>
                  <nts id="Seg_3030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_3032" n="HIAT:w" s="T758">ılla</ts>
                  <nts id="Seg_3033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_3035" n="HIAT:w" s="T759">amɨrla</ts>
                  <nts id="Seg_3036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3037" n="HIAT:ip">(</nts>
                  <nts id="Seg_3038" n="HIAT:ip">(</nts>
                  <ats e="T761" id="Seg_3039" n="HIAT:non-pho" s="T760">…</ats>
                  <nts id="Seg_3040" n="HIAT:ip">)</nts>
                  <nts id="Seg_3041" n="HIAT:ip">)</nts>
                  <nts id="Seg_3042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_3044" n="HIAT:w" s="T761">ılla</ts>
                  <nts id="Seg_3045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_3047" n="HIAT:w" s="T762">šäqqa</ts>
                  <nts id="Seg_3048" n="HIAT:ip">.</nts>
                  <nts id="Seg_3049" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T770" id="Seg_3051" n="HIAT:u" s="T763">
                  <ts e="T764" id="Seg_3053" n="HIAT:w" s="T763">Na</ts>
                  <nts id="Seg_3054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_3056" n="HIAT:w" s="T764">qumiːtɨ</ts>
                  <nts id="Seg_3057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_3059" n="HIAT:w" s="T765">poːqɨn</ts>
                  <nts id="Seg_3060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_3062" n="HIAT:w" s="T766">na</ts>
                  <nts id="Seg_3063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T768" id="Seg_3065" n="HIAT:w" s="T767">ippälɨmmɨntɔːt</ts>
                  <nts id="Seg_3066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_3068" n="HIAT:w" s="T768">nɔːtɨ</ts>
                  <nts id="Seg_3069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_3071" n="HIAT:w" s="T769">sɔːrɨmpkol</ts>
                  <nts id="Seg_3072" n="HIAT:ip">.</nts>
                  <nts id="Seg_3073" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T773" id="Seg_3075" n="HIAT:u" s="T770">
                  <ts e="T771" id="Seg_3077" n="HIAT:w" s="T770">Kuttar</ts>
                  <nts id="Seg_3078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_3080" n="HIAT:w" s="T771">sɔːralɨmpa</ts>
                  <nts id="Seg_3081" n="HIAT:ip">,</nts>
                  <nts id="Seg_3082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_3084" n="HIAT:w" s="T772">nılʼčʼik</ts>
                  <nts id="Seg_3085" n="HIAT:ip">.</nts>
                  <nts id="Seg_3086" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T778" id="Seg_3088" n="HIAT:u" s="T773">
                  <ts e="T774" id="Seg_3090" n="HIAT:w" s="T773">Šäqqa</ts>
                  <nts id="Seg_3091" n="HIAT:ip">,</nts>
                  <nts id="Seg_3092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_3094" n="HIAT:w" s="T774">toptɨlʼ</ts>
                  <nts id="Seg_3095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_3097" n="HIAT:w" s="T775">qar</ts>
                  <nts id="Seg_3098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_3100" n="HIAT:w" s="T776">ınna</ts>
                  <nts id="Seg_3101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_3103" n="HIAT:w" s="T777">wəša</ts>
                  <nts id="Seg_3104" n="HIAT:ip">.</nts>
                  <nts id="Seg_3105" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T790" id="Seg_3107" n="HIAT:u" s="T778">
                  <ts e="T779" id="Seg_3109" n="HIAT:w" s="T778">Poni</ts>
                  <nts id="Seg_3110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_3112" n="HIAT:w" s="T779">ira</ts>
                  <nts id="Seg_3113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_3115" n="HIAT:w" s="T780">nılʼčʼi</ts>
                  <nts id="Seg_3116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_3118" n="HIAT:w" s="T781">mɨta</ts>
                  <nts id="Seg_3119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_3121" n="HIAT:w" s="T782">məntɨlʼ</ts>
                  <nts id="Seg_3122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_3124" n="HIAT:w" s="T783">olɨtɨ</ts>
                  <nts id="Seg_3125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_3127" n="HIAT:w" s="T784">tot</ts>
                  <nts id="Seg_3128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T786" id="Seg_3130" n="HIAT:w" s="T785">taŋkɨıːmpa</ts>
                  <nts id="Seg_3131" n="HIAT:ip">,</nts>
                  <nts id="Seg_3132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_3134" n="HIAT:w" s="T786">tına</ts>
                  <nts id="Seg_3135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_3137" n="HIAT:w" s="T787">keksa</ts>
                  <nts id="Seg_3138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_3140" n="HIAT:w" s="T788">saijatɨ</ts>
                  <nts id="Seg_3141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3142" n="HIAT:ip">(</nts>
                  <nts id="Seg_3143" n="HIAT:ip">(</nts>
                  <ats e="T790" id="Seg_3144" n="HIAT:non-pho" s="T789">…</ats>
                  <nts id="Seg_3145" n="HIAT:ip">)</nts>
                  <nts id="Seg_3146" n="HIAT:ip">)</nts>
                  <nts id="Seg_3147" n="HIAT:ip">.</nts>
                  <nts id="Seg_3148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T796" id="Seg_3150" n="HIAT:u" s="T790">
                  <ts e="T791" id="Seg_3152" n="HIAT:w" s="T790">Toːnna</ts>
                  <nts id="Seg_3153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_3155" n="HIAT:w" s="T791">qälɨt</ts>
                  <nts id="Seg_3156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3157" n="HIAT:ip">(</nts>
                  <nts id="Seg_3158" n="HIAT:ip">(</nts>
                  <ats e="T793" id="Seg_3159" n="HIAT:non-pho" s="T792">…</ats>
                  <nts id="Seg_3160" n="HIAT:ip">)</nts>
                  <nts id="Seg_3161" n="HIAT:ip">)</nts>
                  <nts id="Seg_3162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_3164" n="HIAT:w" s="T793">iše</ts>
                  <nts id="Seg_3165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_3167" n="HIAT:w" s="T794">somalɔːqɨ</ts>
                  <nts id="Seg_3168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_3170" n="HIAT:w" s="T795">ɛppa</ts>
                  <nts id="Seg_3171" n="HIAT:ip">.</nts>
                  <nts id="Seg_3172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T803" id="Seg_3174" n="HIAT:u" s="T796">
                  <ts e="T797" id="Seg_3176" n="HIAT:w" s="T796">Nılʼ</ts>
                  <nts id="Seg_3177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_3179" n="HIAT:w" s="T797">kətɨŋɨtɨ</ts>
                  <nts id="Seg_3180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_3182" n="HIAT:w" s="T798">na</ts>
                  <nts id="Seg_3183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_3185" n="HIAT:w" s="T799">ija</ts>
                  <nts id="Seg_3186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_3188" n="HIAT:w" s="T800">mɨta</ts>
                  <nts id="Seg_3189" n="HIAT:ip">,</nts>
                  <nts id="Seg_3190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_3192" n="HIAT:w" s="T801">na</ts>
                  <nts id="Seg_3193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_3195" n="HIAT:w" s="T802">qälɨtɨtkinitɨ</ts>
                  <nts id="Seg_3196" n="HIAT:ip">.</nts>
                  <nts id="Seg_3197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T811" id="Seg_3199" n="HIAT:u" s="T803">
                  <ts e="T804" id="Seg_3201" n="HIAT:w" s="T803">Qumiːmtɨ</ts>
                  <nts id="Seg_3202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_3204" n="HIAT:w" s="T804">moqɨnä</ts>
                  <nts id="Seg_3205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_3207" n="HIAT:w" s="T805">qəntät</ts>
                  <nts id="Seg_3208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_3210" n="HIAT:w" s="T806">mɨta</ts>
                  <nts id="Seg_3211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_3213" n="HIAT:w" s="T807">qaqloqantɨ</ts>
                  <nts id="Seg_3214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_3216" n="HIAT:w" s="T808">tɛltalla</ts>
                  <nts id="Seg_3217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_3219" n="HIAT:w" s="T809">mɨta</ts>
                  <nts id="Seg_3220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_3222" n="HIAT:w" s="T810">kuralla</ts>
                  <nts id="Seg_3223" n="HIAT:ip">.</nts>
                  <nts id="Seg_3224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T813" id="Seg_3226" n="HIAT:u" s="T811">
                  <ts e="T812" id="Seg_3228" n="HIAT:w" s="T811">Ontɨt</ts>
                  <nts id="Seg_3229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T813" id="Seg_3231" n="HIAT:w" s="T812">qaqloqɨntɨt</ts>
                  <nts id="Seg_3232" n="HIAT:ip">.</nts>
                  <nts id="Seg_3233" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T830" id="Seg_3235" n="HIAT:u" s="T813">
                  <ts e="T814" id="Seg_3237" n="HIAT:w" s="T813">Nɨnɨ</ts>
                  <nts id="Seg_3238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_3240" n="HIAT:w" s="T814">šittal</ts>
                  <nts id="Seg_3241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_3243" n="HIAT:w" s="T815">toː</ts>
                  <nts id="Seg_3244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_3246" n="HIAT:w" s="T816">namä</ts>
                  <nts id="Seg_3247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_3249" n="HIAT:w" s="T817">pona</ts>
                  <nts id="Seg_3250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3251" n="HIAT:ip">(</nts>
                  <ts e="T819" id="Seg_3253" n="HIAT:w" s="T818">qə</ts>
                  <nts id="Seg_3254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T820" id="Seg_3256" n="HIAT:w" s="T819">qəja</ts>
                  <nts id="Seg_3257" n="HIAT:ip">)</nts>
                  <nts id="Seg_3258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T821" id="Seg_3260" n="HIAT:w" s="T820">qapı</ts>
                  <nts id="Seg_3261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_3263" n="HIAT:w" s="T821">mɔːt</ts>
                  <nts id="Seg_3264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_3266" n="HIAT:w" s="T822">šerıːšqo</ts>
                  <nts id="Seg_3267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T824" id="Seg_3269" n="HIAT:w" s="T823">aj</ts>
                  <nts id="Seg_3270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_3272" n="HIAT:w" s="T824">qaj</ts>
                  <nts id="Seg_3273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_3275" n="HIAT:w" s="T825">amɨrqo</ts>
                  <nts id="Seg_3276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_3278" n="HIAT:w" s="T826">aj</ts>
                  <nts id="Seg_3279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T828" id="Seg_3281" n="HIAT:w" s="T827">qaj</ts>
                  <nts id="Seg_3282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T829" id="Seg_3284" n="HIAT:w" s="T828">ašša</ts>
                  <nts id="Seg_3285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_3287" n="HIAT:w" s="T829">amɨrqo</ts>
                  <nts id="Seg_3288" n="HIAT:ip">.</nts>
                  <nts id="Seg_3289" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T836" id="Seg_3291" n="HIAT:u" s="T830">
                  <ts e="T831" id="Seg_3293" n="HIAT:w" s="T830">Ninɨ</ts>
                  <nts id="Seg_3294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_3296" n="HIAT:w" s="T831">na</ts>
                  <nts id="Seg_3297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_3299" n="HIAT:w" s="T832">qum</ts>
                  <nts id="Seg_3300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_3302" n="HIAT:w" s="T833">tıntɨna</ts>
                  <nts id="Seg_3303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_3305" n="HIAT:w" s="T834">sɔːralpɨtɨlʼ</ts>
                  <nts id="Seg_3306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T836" id="Seg_3308" n="HIAT:w" s="T835">qaqlɨntɨt</ts>
                  <nts id="Seg_3309" n="HIAT:ip">.</nts>
                  <nts id="Seg_3310" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T847" id="Seg_3312" n="HIAT:u" s="T836">
                  <ts e="T837" id="Seg_3314" n="HIAT:w" s="T836">Tına</ts>
                  <nts id="Seg_3315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_3317" n="HIAT:w" s="T837">nɨmtɨ</ts>
                  <nts id="Seg_3318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T839" id="Seg_3320" n="HIAT:w" s="T838">moqonä</ts>
                  <nts id="Seg_3321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_3323" n="HIAT:w" s="T839">kəːčʼälä</ts>
                  <nts id="Seg_3324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T841" id="Seg_3326" n="HIAT:w" s="T840">qumiːmtɨ</ts>
                  <nts id="Seg_3327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_3329" n="HIAT:w" s="T841">na</ts>
                  <nts id="Seg_3330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_3332" n="HIAT:w" s="T842">qumpɨj</ts>
                  <nts id="Seg_3333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T844" id="Seg_3335" n="HIAT:w" s="T843">qumiːmtɨ</ts>
                  <nts id="Seg_3336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T845" id="Seg_3338" n="HIAT:w" s="T844">nık</ts>
                  <nts id="Seg_3339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_3341" n="HIAT:w" s="T845">qəntɨŋɨtɨ</ts>
                  <nts id="Seg_3342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_3344" n="HIAT:w" s="T846">moqonä</ts>
                  <nts id="Seg_3345" n="HIAT:ip">.</nts>
                  <nts id="Seg_3346" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T849" id="Seg_3348" n="HIAT:u" s="T847">
                  <ts e="T848" id="Seg_3350" n="HIAT:w" s="T847">Takkɨ</ts>
                  <nts id="Seg_3351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T849" id="Seg_3353" n="HIAT:w" s="T848">moqonä</ts>
                  <nts id="Seg_3354" n="HIAT:ip">.</nts>
                  <nts id="Seg_3355" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T854" id="Seg_3357" n="HIAT:u" s="T849">
                  <ts e="T850" id="Seg_3359" n="HIAT:w" s="T849">Poni</ts>
                  <nts id="Seg_3360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T851" id="Seg_3362" n="HIAT:w" s="T850">iram</ts>
                  <nts id="Seg_3363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_3365" n="HIAT:w" s="T851">tɛ</ts>
                  <nts id="Seg_3366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T853" id="Seg_3368" n="HIAT:w" s="T852">kəːčʼɨlä</ts>
                  <nts id="Seg_3369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_3371" n="HIAT:w" s="T853">qəntɨŋɨtɨ</ts>
                  <nts id="Seg_3372" n="HIAT:ip">.</nts>
                  <nts id="Seg_3373" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T858" id="Seg_3375" n="HIAT:u" s="T854">
                  <ts e="T855" id="Seg_3377" n="HIAT:w" s="T854">Qapı</ts>
                  <nts id="Seg_3378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856" id="Seg_3380" n="HIAT:w" s="T855">kɨpɨka</ts>
                  <nts id="Seg_3381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T857" id="Seg_3383" n="HIAT:w" s="T856">kəitɨ</ts>
                  <nts id="Seg_3384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T858" id="Seg_3386" n="HIAT:w" s="T857">ola</ts>
                  <nts id="Seg_3387" n="HIAT:ip">.</nts>
                  <nts id="Seg_3388" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T861" id="Seg_3390" n="HIAT:u" s="T858">
                  <ts e="T859" id="Seg_3392" n="HIAT:w" s="T858">Təmol</ts>
                  <nts id="Seg_3393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T860" id="Seg_3395" n="HIAT:w" s="T859">qənnɔːtɨt</ts>
                  <nts id="Seg_3396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T861" id="Seg_3398" n="HIAT:w" s="T860">tına</ts>
                  <nts id="Seg_3399" n="HIAT:ip">.</nts>
                  <nts id="Seg_3400" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T865" id="Seg_3402" n="HIAT:u" s="T861">
                  <ts e="T862" id="Seg_3404" n="HIAT:w" s="T861">Nɨnɨ</ts>
                  <nts id="Seg_3405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T863" id="Seg_3407" n="HIAT:w" s="T862">somak</ts>
                  <nts id="Seg_3408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864" id="Seg_3410" n="HIAT:w" s="T863">qənnɔːtɨn</ts>
                  <nts id="Seg_3411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T865" id="Seg_3413" n="HIAT:w" s="T864">nɨmtɨ</ts>
                  <nts id="Seg_3414" n="HIAT:ip">.</nts>
                  <nts id="Seg_3415" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T875" id="Seg_3417" n="HIAT:u" s="T865">
                  <ts e="T866" id="Seg_3419" n="HIAT:w" s="T865">Imantɨn</ts>
                  <nts id="Seg_3420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T867" id="Seg_3422" n="HIAT:w" s="T866">nık</ts>
                  <nts id="Seg_3423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T868" id="Seg_3425" n="HIAT:w" s="T867">kətɨŋɨt</ts>
                  <nts id="Seg_3426" n="HIAT:ip">:</nts>
                  <nts id="Seg_3427" n="HIAT:ip">“</nts>
                  <nts id="Seg_3428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T869" id="Seg_3430" n="HIAT:w" s="T868">Mɔːllɨ</ts>
                  <nts id="Seg_3431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T870" id="Seg_3433" n="HIAT:w" s="T869">toː</ts>
                  <nts id="Seg_3434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T871" id="Seg_3436" n="HIAT:w" s="T870">tılät</ts>
                  <nts id="Seg_3437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T872" id="Seg_3439" n="HIAT:w" s="T871">me</ts>
                  <nts id="Seg_3440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T873" id="Seg_3442" n="HIAT:w" s="T872">nʼenna</ts>
                  <nts id="Seg_3443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T874" id="Seg_3445" n="HIAT:w" s="T873">qəntɨsɔːmıː</ts>
                  <nts id="Seg_3446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T875" id="Seg_3448" n="HIAT:w" s="T874">qumɨtɨtkin</ts>
                  <nts id="Seg_3449" n="HIAT:ip">”</nts>
                  <nts id="Seg_3450" n="HIAT:ip">.</nts>
                  <nts id="Seg_3451" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T884" id="Seg_3453" n="HIAT:u" s="T875">
                  <ts e="T876" id="Seg_3455" n="HIAT:w" s="T875">Aš</ts>
                  <nts id="Seg_3456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T877" id="Seg_3458" n="HIAT:w" s="T876">tɛnɨmɨmpɔːt</ts>
                  <nts id="Seg_3459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T878" id="Seg_3461" n="HIAT:w" s="T877">qapı</ts>
                  <nts id="Seg_3462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T879" id="Seg_3464" n="HIAT:w" s="T878">ontɨn</ts>
                  <nts id="Seg_3465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T880" id="Seg_3467" n="HIAT:w" s="T879">na</ts>
                  <nts id="Seg_3468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T881" id="Seg_3470" n="HIAT:w" s="T880">ilʼčʼantɨlʼ</ts>
                  <nts id="Seg_3471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T882" id="Seg_3473" n="HIAT:w" s="T881">mɨt</ts>
                  <nts id="Seg_3474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T883" id="Seg_3476" n="HIAT:w" s="T882">qos</ts>
                  <nts id="Seg_3477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T884" id="Seg_3479" n="HIAT:w" s="T883">qaj</ts>
                  <nts id="Seg_3480" n="HIAT:ip">.</nts>
                  <nts id="Seg_3481" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T891" id="Seg_3483" n="HIAT:u" s="T884">
                  <ts e="T885" id="Seg_3485" n="HIAT:w" s="T884">Nʼenna</ts>
                  <nts id="Seg_3486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T886" id="Seg_3488" n="HIAT:w" s="T885">qənna</ts>
                  <nts id="Seg_3489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T887" id="Seg_3491" n="HIAT:w" s="T886">tıntena</ts>
                  <nts id="Seg_3492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T888" id="Seg_3494" n="HIAT:w" s="T887">ontɨ</ts>
                  <nts id="Seg_3495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T889" id="Seg_3497" n="HIAT:w" s="T888">ašʼa</ts>
                  <nts id="Seg_3498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T890" id="Seg_3500" n="HIAT:w" s="T889">koŋɨmpa</ts>
                  <nts id="Seg_3501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3502" n="HIAT:ip">(</nts>
                  <nts id="Seg_3503" n="HIAT:ip">(</nts>
                  <ats e="T891" id="Seg_3504" n="HIAT:non-pho" s="T890">…</ats>
                  <nts id="Seg_3505" n="HIAT:ip">)</nts>
                  <nts id="Seg_3506" n="HIAT:ip">)</nts>
                  <nts id="Seg_3507" n="HIAT:ip">.</nts>
                  <nts id="Seg_3508" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T898" id="Seg_3510" n="HIAT:u" s="T891">
                  <ts e="T892" id="Seg_3512" n="HIAT:w" s="T891">Toːna</ts>
                  <nts id="Seg_3513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T893" id="Seg_3515" n="HIAT:w" s="T892">imatɨ</ts>
                  <nts id="Seg_3516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T894" id="Seg_3518" n="HIAT:w" s="T893">na</ts>
                  <nts id="Seg_3519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T895" id="Seg_3521" n="HIAT:w" s="T894">kətɛntɨtɨ</ts>
                  <nts id="Seg_3522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T896" id="Seg_3524" n="HIAT:w" s="T895">äsantɨ</ts>
                  <nts id="Seg_3525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T897" id="Seg_3527" n="HIAT:w" s="T896">ilʼčʼantɨ</ts>
                  <nts id="Seg_3528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898" id="Seg_3530" n="HIAT:w" s="T897">mɨqätkiːn</ts>
                  <nts id="Seg_3531" n="HIAT:ip">.</nts>
                  <nts id="Seg_3532" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T905" id="Seg_3534" n="HIAT:u" s="T898">
                  <ts e="T899" id="Seg_3536" n="HIAT:w" s="T898">Poni</ts>
                  <nts id="Seg_3537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T900" id="Seg_3539" n="HIAT:w" s="T899">iralʼ</ts>
                  <nts id="Seg_3540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T901" id="Seg_3542" n="HIAT:w" s="T900">mɨt</ts>
                  <nts id="Seg_3543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T902" id="Seg_3545" n="HIAT:w" s="T901">tına</ts>
                  <nts id="Seg_3546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T903" id="Seg_3548" n="HIAT:w" s="T902">moqonä</ts>
                  <nts id="Seg_3549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T904" id="Seg_3551" n="HIAT:w" s="T903">qonisɔːtɨt</ts>
                  <nts id="Seg_3552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T905" id="Seg_3554" n="HIAT:w" s="T904">mompa</ts>
                  <nts id="Seg_3555" n="HIAT:ip">.</nts>
                  <nts id="Seg_3556" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T906" id="Seg_3558" n="HIAT:u" s="T905">
                  <ts e="T906" id="Seg_3560" n="HIAT:w" s="T905">Tal</ts>
                  <nts id="Seg_3561" n="HIAT:ip">.</nts>
                  <nts id="Seg_3562" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T911" id="Seg_3564" n="HIAT:u" s="T906">
                  <ts e="T907" id="Seg_3566" n="HIAT:w" s="T906">Nɨnɨ</ts>
                  <nts id="Seg_3567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T908" id="Seg_3569" n="HIAT:w" s="T907">šʼittalʼ</ts>
                  <nts id="Seg_3570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909" id="Seg_3572" n="HIAT:w" s="T908">pija</ts>
                  <nts id="Seg_3573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T910" id="Seg_3575" n="HIAT:w" s="T909">qapija</ts>
                  <nts id="Seg_3576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3577" n="HIAT:ip">(</nts>
                  <nts id="Seg_3578" n="HIAT:ip">(</nts>
                  <ats e="T911" id="Seg_3579" n="HIAT:non-pho" s="T910">…</ats>
                  <nts id="Seg_3580" n="HIAT:ip">)</nts>
                  <nts id="Seg_3581" n="HIAT:ip">)</nts>
                  <nts id="Seg_3582" n="HIAT:ip">.</nts>
                  <nts id="Seg_3583" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T985" id="Seg_3585" n="HIAT:u" s="T911">
                  <ts e="T912" id="Seg_3587" n="HIAT:w" s="T911">Aš</ts>
                  <nts id="Seg_3588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T985" id="Seg_3590" n="HIAT:w" s="T912">tɛnɨmɔːtɨt</ts>
                  <nts id="Seg_3591" n="HIAT:ip">.</nts>
                  <nts id="Seg_3592" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T986" id="Seg_3594" n="HIAT:u" s="T985">
                  <nts id="Seg_3595" n="HIAT:ip">(</nts>
                  <nts id="Seg_3596" n="HIAT:ip">(</nts>
                  <ats e="T913" id="Seg_3597" n="HIAT:non-pho" s="T985">NN:</ats>
                  <nts id="Seg_3598" n="HIAT:ip">)</nts>
                  <nts id="Seg_3599" n="HIAT:ip">)</nts>
                  <nts id="Seg_3600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T914" id="Seg_3602" n="HIAT:w" s="T913">Karrä</ts>
                  <nts id="Seg_3603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T986" id="Seg_3605" n="HIAT:w" s="T914">koŋɨmpäš</ts>
                  <nts id="Seg_3606" n="HIAT:ip">.</nts>
                  <nts id="Seg_3607" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T987" id="Seg_3609" n="HIAT:u" s="T986">
                  <nts id="Seg_3610" n="HIAT:ip">(</nts>
                  <nts id="Seg_3611" n="HIAT:ip">(</nts>
                  <ats e="T915" id="Seg_3612" n="HIAT:non-pho" s="T986">NN:</ats>
                  <nts id="Seg_3613" n="HIAT:ip">)</nts>
                  <nts id="Seg_3614" n="HIAT:ip">)</nts>
                  <nts id="Seg_3615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T916" id="Seg_3617" n="HIAT:w" s="T915">Karrä</ts>
                  <nts id="Seg_3618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T987" id="Seg_3620" n="HIAT:w" s="T916">radioqantɨ</ts>
                  <nts id="Seg_3621" n="HIAT:ip">.</nts>
                  <nts id="Seg_3622" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T932" id="Seg_3624" n="HIAT:u" s="T987">
                  <nts id="Seg_3625" n="HIAT:ip">(</nts>
                  <nts id="Seg_3626" n="HIAT:ip">(</nts>
                  <ats e="T917" id="Seg_3627" n="HIAT:non-pho" s="T987">NEP:</ats>
                  <nts id="Seg_3628" n="HIAT:ip">)</nts>
                  <nts id="Seg_3629" n="HIAT:ip">)</nts>
                  <nts id="Seg_3630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T918" id="Seg_3632" n="HIAT:w" s="T917">Takkɨ</ts>
                  <nts id="Seg_3633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T919" id="Seg_3635" n="HIAT:w" s="T918">nʼenna</ts>
                  <nts id="Seg_3636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T920" id="Seg_3638" n="HIAT:w" s="T919">ɨpalnoj</ts>
                  <nts id="Seg_3639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T921" id="Seg_3641" n="HIAT:w" s="T920">təpɨjat</ts>
                  <nts id="Seg_3642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T922" id="Seg_3644" n="HIAT:w" s="T921">näčʼčʼa</ts>
                  <nts id="Seg_3645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T923" id="Seg_3647" n="HIAT:w" s="T922">ilʼčʼantɨ</ts>
                  <nts id="Seg_3648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T924" id="Seg_3650" n="HIAT:w" s="T923">mɔːt</ts>
                  <nts id="Seg_3651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T925" id="Seg_3653" n="HIAT:w" s="T924">mɔːt</ts>
                  <nts id="Seg_3654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T926" id="Seg_3656" n="HIAT:w" s="T925">šerna</ts>
                  <nts id="Seg_3657" n="HIAT:ip">,</nts>
                  <nts id="Seg_3658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T927" id="Seg_3660" n="HIAT:w" s="T926">qaqlɨt</ts>
                  <nts id="Seg_3661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T928" id="Seg_3663" n="HIAT:w" s="T927">to</ts>
                  <nts id="Seg_3664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T929" id="Seg_3666" n="HIAT:w" s="T928">nıːmɨt</ts>
                  <nts id="Seg_3667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T930" id="Seg_3669" n="HIAT:w" s="T929">kurɨla</ts>
                  <nts id="Seg_3670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T931" id="Seg_3672" n="HIAT:w" s="T930">nık</ts>
                  <nts id="Seg_3673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T932" id="Seg_3675" n="HIAT:w" s="T931">tottɨŋɨtɨ</ts>
                  <nts id="Seg_3676" n="HIAT:ip">.</nts>
                  <nts id="Seg_3677" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T936" id="Seg_3679" n="HIAT:u" s="T932">
                  <ts e="T933" id="Seg_3681" n="HIAT:w" s="T932">Toːnna</ts>
                  <nts id="Seg_3682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T934" id="Seg_3684" n="HIAT:w" s="T933">qənmɨntɔːtɨn</ts>
                  <nts id="Seg_3685" n="HIAT:ip">,</nts>
                  <nts id="Seg_3686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T935" id="Seg_3688" n="HIAT:w" s="T934">moqonä</ts>
                  <nts id="Seg_3689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T936" id="Seg_3691" n="HIAT:w" s="T935">qənmɨntɔːtɨt</ts>
                  <nts id="Seg_3692" n="HIAT:ip">.</nts>
                  <nts id="Seg_3693" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T944" id="Seg_3695" n="HIAT:u" s="T936">
                  <ts e="T937" id="Seg_3697" n="HIAT:w" s="T936">Poni</ts>
                  <nts id="Seg_3698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T938" id="Seg_3700" n="HIAT:w" s="T937">ira</ts>
                  <nts id="Seg_3701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T939" id="Seg_3703" n="HIAT:w" s="T938">kekɨsa</ts>
                  <nts id="Seg_3704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T940" id="Seg_3706" n="HIAT:w" s="T939">takkɨn</ts>
                  <nts id="Seg_3707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T941" id="Seg_3709" n="HIAT:w" s="T940">mɔːtqɨn</ts>
                  <nts id="Seg_3710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T942" id="Seg_3712" n="HIAT:w" s="T941">tulɨšpa</ts>
                  <nts id="Seg_3713" n="HIAT:ip">,</nts>
                  <nts id="Seg_3714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T943" id="Seg_3716" n="HIAT:w" s="T942">ılla</ts>
                  <nts id="Seg_3717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T944" id="Seg_3719" n="HIAT:w" s="T943">qumpa</ts>
                  <nts id="Seg_3720" n="HIAT:ip">.</nts>
                  <nts id="Seg_3721" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T949" id="Seg_3723" n="HIAT:u" s="T944">
                  <ts e="T945" id="Seg_3725" n="HIAT:w" s="T944">Toːnna</ts>
                  <nts id="Seg_3726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T946" id="Seg_3728" n="HIAT:w" s="T945">šittɨ</ts>
                  <nts id="Seg_3729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T947" id="Seg_3731" n="HIAT:w" s="T946">qumoːqıː</ts>
                  <nts id="Seg_3732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T948" id="Seg_3734" n="HIAT:w" s="T947">tıː</ts>
                  <nts id="Seg_3735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T949" id="Seg_3737" n="HIAT:w" s="T948">ilɨmpɔːqı</ts>
                  <nts id="Seg_3738" n="HIAT:ip">.</nts>
                  <nts id="Seg_3739" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T988" id="Seg_3741" n="HIAT:u" s="T949">
                  <ts e="T950" id="Seg_3743" n="HIAT:w" s="T949">Poni</ts>
                  <nts id="Seg_3744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T951" id="Seg_3746" n="HIAT:w" s="T950">ira</ts>
                  <nts id="Seg_3747" n="HIAT:ip">,</nts>
                  <nts id="Seg_3748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T952" id="Seg_3750" n="HIAT:w" s="T951">Poni</ts>
                  <nts id="Seg_3751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T953" id="Seg_3753" n="HIAT:w" s="T952">iralʼ</ts>
                  <nts id="Seg_3754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T954" id="Seg_3756" n="HIAT:w" s="T953">mɨt</ts>
                  <nts id="Seg_3757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T955" id="Seg_3759" n="HIAT:w" s="T954">nɨmtɨ</ts>
                  <nts id="Seg_3760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T988" id="Seg_3762" n="HIAT:w" s="T955">ɛːŋa</ts>
                  <nts id="Seg_3763" n="HIAT:ip">.</nts>
                  <nts id="Seg_3764" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T989" id="Seg_3766" n="HIAT:u" s="T988">
                  <nts id="Seg_3767" n="HIAT:ip">(</nts>
                  <nts id="Seg_3768" n="HIAT:ip">(</nts>
                  <ats e="T956" id="Seg_3769" n="HIAT:non-pho" s="T988">NN:</ats>
                  <nts id="Seg_3770" n="HIAT:ip">)</nts>
                  <nts id="Seg_3771" n="HIAT:ip">)</nts>
                  <nts id="Seg_3772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T957" id="Seg_3774" n="HIAT:w" s="T956">Jarɨ</ts>
                  <nts id="Seg_3775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T958" id="Seg_3777" n="HIAT:w" s="T957">mɨ</ts>
                  <nts id="Seg_3778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T959" id="Seg_3780" n="HIAT:w" s="T958">kətät</ts>
                  <nts id="Seg_3781" n="HIAT:ip">,</nts>
                  <nts id="Seg_3782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T960" id="Seg_3784" n="HIAT:w" s="T959">jarɨk</ts>
                  <nts id="Seg_3785" n="HIAT:ip">,</nts>
                  <nts id="Seg_3786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T989" id="Seg_3788" n="HIAT:w" s="T960">jarɨk</ts>
                  <nts id="Seg_3789" n="HIAT:ip">.</nts>
                  <nts id="Seg_3790" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T964" id="Seg_3792" n="HIAT:u" s="T989">
                  <nts id="Seg_3793" n="HIAT:ip">(</nts>
                  <nts id="Seg_3794" n="HIAT:ip">(</nts>
                  <ats e="T961" id="Seg_3795" n="HIAT:non-pho" s="T989">NN:</ats>
                  <nts id="Seg_3796" n="HIAT:ip">)</nts>
                  <nts id="Seg_3797" n="HIAT:ip">)</nts>
                  <nts id="Seg_3798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T962" id="Seg_3800" n="HIAT:w" s="T961">Jarɨ</ts>
                  <nts id="Seg_3801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T963" id="Seg_3803" n="HIAT:w" s="T962">mɨ</ts>
                  <nts id="Seg_3804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T964" id="Seg_3806" n="HIAT:w" s="T963">kətät</ts>
                  <nts id="Seg_3807" n="HIAT:ip">.</nts>
                  <nts id="Seg_3808" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T965" id="Seg_3810" n="HIAT:u" s="T964">
                  <nts id="Seg_3811" n="HIAT:ip">(</nts>
                  <nts id="Seg_3812" n="HIAT:ip">(</nts>
                  <ats e="T964.tx.1" id="Seg_3813" n="HIAT:non-pho" s="T964">NN:</ats>
                  <nts id="Seg_3814" n="HIAT:ip">)</nts>
                  <nts id="Seg_3815" n="HIAT:ip">)</nts>
                  <nts id="Seg_3816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T964.tx.2" id="Seg_3818" n="HIAT:w" s="T964.tx.1">Тут</ts>
                  <nts id="Seg_3819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T964.tx.3" id="Seg_3821" n="HIAT:w" s="T964.tx.2">рассказанное</ts>
                  <nts id="Seg_3822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T964.tx.4" id="Seg_3824" n="HIAT:w" s="T964.tx.3">всё</ts>
                  <nts id="Seg_3825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T964.tx.5" id="Seg_3827" n="HIAT:w" s="T964.tx.4">и</ts>
                  <nts id="Seg_3828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T965" id="Seg_3830" n="HIAT:w" s="T964.tx.5">есть</ts>
                  <nts id="Seg_3831" n="HIAT:ip">.</nts>
                  <nts id="Seg_3832" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T965" id="Seg_3833" n="sc" s="T0">
               <ts e="T968" id="Seg_3835" n="e" s="T0">((KuAI:)) Лентяю всегда праздник. </ts>
               <ts e="T1" id="Seg_3837" n="e" s="T968">((NEP:)) </ts>
               <ts e="T2" id="Seg_3839" n="e" s="T1">((…)) </ts>
               <ts e="T3" id="Seg_3841" n="e" s="T2">Ukkɨr </ts>
               <ts e="T4" id="Seg_3843" n="e" s="T3">ijatɨ </ts>
               <ts e="T5" id="Seg_3845" n="e" s="T4">ɛppantɨ </ts>
               <ts e="T6" id="Seg_3847" n="e" s="T5">((…)). </ts>
               <ts e="T7" id="Seg_3849" n="e" s="T6">Na </ts>
               <ts e="T8" id="Seg_3851" n="e" s="T7">qos </ts>
               <ts e="T9" id="Seg_3853" n="e" s="T8">ket </ts>
               <ts e="T10" id="Seg_3855" n="e" s="T9">((…)) </ts>
               <ts e="T11" id="Seg_3857" n="e" s="T10">qət </ts>
               <ts e="T12" id="Seg_3859" n="e" s="T11">kətɨkak </ts>
               <ts e="T13" id="Seg_3861" n="e" s="T12">namɨp. </ts>
               <ts e="T14" id="Seg_3863" n="e" s="T13">Na </ts>
               <ts e="T15" id="Seg_3865" n="e" s="T14">tɛntɨlʼ, </ts>
               <ts e="T16" id="Seg_3867" n="e" s="T15">na </ts>
               <ts e="T17" id="Seg_3869" n="e" s="T16">tɛntɨlʼ, </ts>
               <ts e="T18" id="Seg_3871" n="e" s="T17">ašša </ts>
               <ts e="T19" id="Seg_3873" n="e" s="T18">čʼaptä. </ts>
               <ts e="T969" id="Seg_3875" n="e" s="T19">((KuAI:)) Подождите, пусть она подождёт, я попробую, посмотрю, проверю. </ts>
               <ts e="T20" id="Seg_3877" n="e" s="T969">((NN:)) </ts>
               <ts e="T21" id="Seg_3879" n="e" s="T20">Tan </ts>
               <ts e="T22" id="Seg_3881" n="e" s="T21">ılqɨt </ts>
               <ts e="T23" id="Seg_3883" n="e" s="T22">((…)) </ts>
               <ts e="T24" id="Seg_3885" n="e" s="T23">šʼolqumɨt </ts>
               <ts e="T25" id="Seg_3887" n="e" s="T24">näčʼčʼa </ts>
               <ts e="T970" id="Seg_3889" n="e" s="T25">((…)). </ts>
               <ts e="T26" id="Seg_3891" n="e" s="T970">((NN:)) </ts>
               <ts e="T27" id="Seg_3893" n="e" s="T26">Tam </ts>
               <ts e="T28" id="Seg_3895" n="e" s="T27">na </ts>
               <ts e="T29" id="Seg_3897" n="e" s="T28">šintɨ </ts>
               <ts e="T30" id="Seg_3899" n="e" s="T29">qolʼčʼanta </ts>
               <ts e="T31" id="Seg_3901" n="e" s="T30">((…)) </ts>
               <ts e="T32" id="Seg_3903" n="e" s="T31">tan </ts>
               <ts e="T33" id="Seg_3905" n="e" s="T32">əːtal </ts>
               <ts e="T971" id="Seg_3907" n="e" s="T33">utrejqɨjojčʼimpät. </ts>
               <ts e="T34" id="Seg_3909" n="e" s="T971">((NN:)) </ts>
               <ts e="T972" id="Seg_3911" n="e" s="T34">Tuttaltät. </ts>
               <ts e="T35" id="Seg_3913" n="e" s="T972">((NN:)) </ts>
               <ts e="T36" id="Seg_3915" n="e" s="T35">Čʼontɨkak </ts>
               <ts e="T973" id="Seg_3917" n="e" s="T36">kətätɨ. </ts>
               <ts e="T37" id="Seg_3919" n="e" s="T973">((NEP:)) </ts>
               <ts e="T38" id="Seg_3921" n="e" s="T37">Pija </ts>
               <ts e="T39" id="Seg_3923" n="e" s="T38">na </ts>
               <ts e="T40" id="Seg_3925" n="e" s="T39">ira </ts>
               <ts e="T41" id="Seg_3927" n="e" s="T40">ilɨmpan. </ts>
               <ts e="T42" id="Seg_3929" n="e" s="T41">Qütalpa. </ts>
               <ts e="T43" id="Seg_3931" n="e" s="T42">Na </ts>
               <ts e="T44" id="Seg_3933" n="e" s="T43">šʼojqumɨj </ts>
               <ts e="T45" id="Seg_3935" n="e" s="T44">ira. </ts>
               <ts e="T46" id="Seg_3937" n="e" s="T45">Tapɨlʼalʼ </ts>
               <ts e="T47" id="Seg_3939" n="e" s="T46">pɛlalʼ </ts>
               <ts e="T48" id="Seg_3941" n="e" s="T47">kortɨ, </ts>
               <ts e="T49" id="Seg_3943" n="e" s="T48">tɔːlʼ </ts>
               <ts e="T50" id="Seg_3945" n="e" s="T49">takkɨlʼelʼ </ts>
               <ts e="T51" id="Seg_3947" n="e" s="T50">qolt </ts>
               <ts e="T52" id="Seg_3949" n="e" s="T51">pɛlakqɨlʼ </ts>
               <ts e="T53" id="Seg_3951" n="e" s="T52">ilɨmpa. </ts>
               <ts e="T54" id="Seg_3953" n="e" s="T53">Toːqɨlʼ </ts>
               <ts e="T55" id="Seg_3955" n="e" s="T54">təp </ts>
               <ts e="T56" id="Seg_3957" n="e" s="T55">poːkɨtɨlʼ </ts>
               <ts e="T57" id="Seg_3959" n="e" s="T56">təttɨt </ts>
               <ts e="T58" id="Seg_3961" n="e" s="T57">(tošʼa) </ts>
               <ts e="T992" id="Seg_3963" n="e" s="T58">qaj. </ts>
               <ts e="T59" id="Seg_3965" n="e" s="T992">((…)). </ts>
               <ts e="T60" id="Seg_3967" n="e" s="T59">Ilɨmpa </ts>
               <ts e="T61" id="Seg_3969" n="e" s="T60">tına. </ts>
               <ts e="T62" id="Seg_3971" n="e" s="T61">Qütalpa. </ts>
               <ts e="T63" id="Seg_3973" n="e" s="T62">Ijatɨ, </ts>
               <ts e="T64" id="Seg_3975" n="e" s="T63">ukkɨr </ts>
               <ts e="T65" id="Seg_3977" n="e" s="T64">ijatɨ </ts>
               <ts e="T66" id="Seg_3979" n="e" s="T65">pɛläkɨtɨj </ts>
               <ts e="T67" id="Seg_3981" n="e" s="T66">neːkɨtɨj </ts>
               <ts e="T68" id="Seg_3983" n="e" s="T67">pɛlätɨ. </ts>
               <ts e="T69" id="Seg_3985" n="e" s="T68">Nʼi </ts>
               <ts e="T70" id="Seg_3987" n="e" s="T69">nälatɨ </ts>
               <ts e="T71" id="Seg_3989" n="e" s="T70">čʼäŋka, </ts>
               <ts e="T72" id="Seg_3991" n="e" s="T71">nʼi </ts>
               <ts e="T73" id="Seg_3993" n="e" s="T72">qajtɨ </ts>
               <ts e="T74" id="Seg_3995" n="e" s="T73">čʼäŋka. </ts>
               <ts e="T75" id="Seg_3997" n="e" s="T74">Nɨn </ts>
               <ts e="T76" id="Seg_3999" n="e" s="T75">na </ts>
               <ts e="T77" id="Seg_4001" n="e" s="T76">əsɨtɨ </ts>
               <ts e="T78" id="Seg_4003" n="e" s="T77">qütalna </ts>
               <ts e="T79" id="Seg_4005" n="e" s="T78">na </ts>
               <ts e="T80" id="Seg_4007" n="e" s="T79">ijalʼa, </ts>
               <ts e="T81" id="Seg_4009" n="e" s="T80">(naintɨ) </ts>
               <ts e="T82" id="Seg_4011" n="e" s="T81">na </ts>
               <ts e="T83" id="Seg_4013" n="e" s="T82">ijan. </ts>
               <ts e="T84" id="Seg_4015" n="e" s="T83">Nɨmtɨ </ts>
               <ts e="T85" id="Seg_4017" n="e" s="T84">ila </ts>
               <ts e="T86" id="Seg_4019" n="e" s="T85">qapij </ts>
               <ts e="T87" id="Seg_4021" n="e" s="T86">na </ts>
               <ts e="T88" id="Seg_4023" n="e" s="T87">əsɨntɨ </ts>
               <ts e="T89" id="Seg_4025" n="e" s="T88">əmɨntɨ </ts>
               <ts e="T90" id="Seg_4027" n="e" s="T89">qamqɨn. </ts>
               <ts e="T91" id="Seg_4029" n="e" s="T90">Toːntɨ </ts>
               <ts e="T92" id="Seg_4031" n="e" s="T91">tına </ts>
               <ts e="T93" id="Seg_4033" n="e" s="T92">šʼittɨ </ts>
               <ts e="T94" id="Seg_4035" n="e" s="T93">qälɨ </ts>
               <ts e="T95" id="Seg_4037" n="e" s="T94">timnʼäsɨqı </ts>
               <ts e="T96" id="Seg_4039" n="e" s="T95">ɛːppɨntɔːqı. </ts>
               <ts e="T97" id="Seg_4041" n="e" s="T96">Qälɨ </ts>
               <ts e="T98" id="Seg_4043" n="e" s="T97">ira </ts>
               <ts e="T99" id="Seg_4045" n="e" s="T98">kučʼčʼa </ts>
               <ts e="T100" id="Seg_4047" n="e" s="T99">ɛːnta </ts>
               <ts e="T101" id="Seg_4049" n="e" s="T100">(tıː)? </ts>
               <ts e="T102" id="Seg_4051" n="e" s="T101">Qaj </ts>
               <ts e="T103" id="Seg_4053" n="e" s="T102">ila </ts>
               <ts e="T104" id="Seg_4055" n="e" s="T103">kučʼčʼä </ts>
               <ts e="T105" id="Seg_4057" n="e" s="T104">izhe </ts>
               <ts e="T106" id="Seg_4059" n="e" s="T105">tıntena </ts>
               <ts e="T107" id="Seg_4061" n="e" s="T106">na </ts>
               <ts e="T108" id="Seg_4063" n="e" s="T107">ira </ts>
               <ts e="T109" id="Seg_4065" n="e" s="T108">ılla </ts>
               <ts e="T110" id="Seg_4067" n="e" s="T109">quŋa. </ts>
               <ts e="T111" id="Seg_4069" n="e" s="T110">İlla </ts>
               <ts e="T112" id="Seg_4071" n="e" s="T111">quŋa, </ts>
               <ts e="T113" id="Seg_4073" n="e" s="T112">tına </ts>
               <ts e="T114" id="Seg_4075" n="e" s="T113">nɨn </ts>
               <ts e="T115" id="Seg_4077" n="e" s="T114">šittal </ts>
               <ts e="T116" id="Seg_4079" n="e" s="T115">tına </ts>
               <ts e="T117" id="Seg_4081" n="e" s="T116">ija, </ts>
               <ts e="T118" id="Seg_4083" n="e" s="T117">qälɨ </ts>
               <ts e="T119" id="Seg_4085" n="e" s="T118">ira </ts>
               <ts e="T120" id="Seg_4087" n="e" s="T119">nɔːtɨ </ts>
               <ts e="T121" id="Seg_4089" n="e" s="T120">kučʼčʼa </ts>
               <ts e="T122" id="Seg_4091" n="e" s="T121">qatɛnta? </ts>
               <ts e="T123" id="Seg_4093" n="e" s="T122">Toː </ts>
               <ts e="T124" id="Seg_4095" n="e" s="T123">tɨmnʼantɨn </ts>
               <ts e="T125" id="Seg_4097" n="e" s="T124">tulɨnʼnʼa. </ts>
               <ts e="T126" id="Seg_4099" n="e" s="T125">Ira </ts>
               <ts e="T127" id="Seg_4101" n="e" s="T126">qumpa. </ts>
               <ts e="T128" id="Seg_4103" n="e" s="T127">İlla </ts>
               <ts e="T129" id="Seg_4105" n="e" s="T128">meːŋɔːt. </ts>
               <ts e="T130" id="Seg_4107" n="e" s="T129">Šittal </ts>
               <ts e="T131" id="Seg_4109" n="e" s="T130">imaqotatɨ, </ts>
               <ts e="T132" id="Seg_4111" n="e" s="T131">tına </ts>
               <ts e="T133" id="Seg_4113" n="e" s="T132">imaqotatɨ </ts>
               <ts e="T134" id="Seg_4115" n="e" s="T133">qütalna, </ts>
               <ts e="T135" id="Seg_4117" n="e" s="T134">imaqotatɨ </ts>
               <ts e="T136" id="Seg_4119" n="e" s="T135">ɛj </ts>
               <ts e="T137" id="Seg_4121" n="e" s="T136">ılla </ts>
               <ts e="T138" id="Seg_4123" n="e" s="T137">quŋa. </ts>
               <ts e="T139" id="Seg_4125" n="e" s="T138">Iralʼ </ts>
               <ts e="T140" id="Seg_4127" n="e" s="T139">mɨqä </ts>
               <ts e="T141" id="Seg_4129" n="e" s="T140">tına </ts>
               <ts e="T142" id="Seg_4131" n="e" s="T141">ijatɨ </ts>
               <ts e="T143" id="Seg_4133" n="e" s="T142">nʼeːkɔːl, </ts>
               <ts e="T144" id="Seg_4135" n="e" s="T143">pɛlɨkɔːl </ts>
               <ts e="T145" id="Seg_4137" n="e" s="T144">qala. </ts>
               <ts e="T146" id="Seg_4139" n="e" s="T145">Kučʼčʼa </ts>
               <ts e="T147" id="Seg_4141" n="e" s="T146">qattɛnta? </ts>
               <ts e="T148" id="Seg_4143" n="e" s="T147">Na </ts>
               <ts e="T149" id="Seg_4145" n="e" s="T148">qälɨ </ts>
               <ts e="T150" id="Seg_4147" n="e" s="T149">ira </ts>
               <ts e="T151" id="Seg_4149" n="e" s="T150">nɨnɨ </ts>
               <ts e="T152" id="Seg_4151" n="e" s="T151">šittäl </ts>
               <ts e="T153" id="Seg_4153" n="e" s="T152">tɔː </ts>
               <ts e="T154" id="Seg_4155" n="e" s="T153">puːčʼɨmpatɨ </ts>
               <ts e="T155" id="Seg_4157" n="e" s="T154">na </ts>
               <ts e="T156" id="Seg_4159" n="e" s="T155">čʼilʼaj </ts>
               <ts e="T157" id="Seg_4161" n="e" s="T156">ija. </ts>
               <ts e="T158" id="Seg_4163" n="e" s="T157">Mərqɨ </ts>
               <ts e="T159" id="Seg_4165" n="e" s="T158">ijatɨ </ts>
               <ts e="T160" id="Seg_4167" n="e" s="T159">(na </ts>
               <ts e="T161" id="Seg_4169" n="e" s="T160">istap=) </ts>
               <ts e="T163" id="Seg_4171" n="e" s="T161">(mat-) </ts>
               <ts e="T166" id="Seg_4173" n="e" s="T163">(mɨt-) </ts>
               <ts e="T167" id="Seg_4175" n="e" s="T166">istap </ts>
               <ts e="T168" id="Seg_4177" n="e" s="T167">Genalʼ </ts>
               <ts e="T169" id="Seg_4179" n="e" s="T168">mɨt </ts>
               <ts e="T170" id="Seg_4181" n="e" s="T169">tarı. </ts>
               <ts e="T171" id="Seg_4183" n="e" s="T170">Nılʼčʼij </ts>
               <ts e="T172" id="Seg_4185" n="e" s="T171">ijatɨj </ts>
               <ts e="T173" id="Seg_4187" n="e" s="T172">ɛppa. </ts>
               <ts e="T174" id="Seg_4189" n="e" s="T173">Tɔː </ts>
               <ts e="T175" id="Seg_4191" n="e" s="T174">puːtɨŋɨtɨ, </ts>
               <ts e="T176" id="Seg_4193" n="e" s="T175">ɔːtätɨj </ts>
               <ts e="T177" id="Seg_4195" n="e" s="T176">čʼäŋka. </ts>
               <ts e="T178" id="Seg_4197" n="e" s="T177">Ɔːtäkɔːl </ts>
               <ts e="T179" id="Seg_4199" n="e" s="T178">topɨsa </ts>
               <ts e="T180" id="Seg_4201" n="e" s="T179">ilɨmpɔːt </ts>
               <ts e="T181" id="Seg_4203" n="e" s="T180">na </ts>
               <ts e="T182" id="Seg_4205" n="e" s="T181">äsasɨt. </ts>
               <ts e="T183" id="Seg_4207" n="e" s="T182">Nɨmtɨ </ts>
               <ts e="T184" id="Seg_4209" n="e" s="T183">ilɨmpɔːtɨn, </ts>
               <ts e="T185" id="Seg_4211" n="e" s="T184">ilɨmpɔːtɨn, </ts>
               <ts e="T186" id="Seg_4213" n="e" s="T185">nɨnɨ </ts>
               <ts e="T187" id="Seg_4215" n="e" s="T186">tɔː </ts>
               <ts e="T188" id="Seg_4217" n="e" s="T187">puːčʼɨsɔːt. </ts>
               <ts e="T189" id="Seg_4219" n="e" s="T188">Tɔː </ts>
               <ts e="T190" id="Seg_4221" n="e" s="T189">puːlä </ts>
               <ts e="T191" id="Seg_4223" n="e" s="T190">na </ts>
               <ts e="T192" id="Seg_4225" n="e" s="T191">ija </ts>
               <ts e="T193" id="Seg_4227" n="e" s="T192">qälɨ </ts>
               <ts e="T194" id="Seg_4229" n="e" s="T193">ira </ts>
               <ts e="T195" id="Seg_4231" n="e" s="T194">mɔːttɨ, </ts>
               <ts e="T196" id="Seg_4233" n="e" s="T195">qälɨ </ts>
               <ts e="T197" id="Seg_4235" n="e" s="T196">ira </ts>
               <ts e="T198" id="Seg_4237" n="e" s="T197">ijatɨ </ts>
               <ts e="T199" id="Seg_4239" n="e" s="T198">čʼäŋka. </ts>
               <ts e="T200" id="Seg_4241" n="e" s="T199">Ijakɨtɨlʼ </ts>
               <ts e="T201" id="Seg_4243" n="e" s="T200">imatɨ. </ts>
               <ts e="T202" id="Seg_4245" n="e" s="T201">Toːnna </ts>
               <ts e="T203" id="Seg_4247" n="e" s="T202">čʼontolʼ </ts>
               <ts e="T204" id="Seg_4249" n="e" s="T203">tɨmnʼatɨ </ts>
               <ts e="T205" id="Seg_4251" n="e" s="T204">(naintɨ) </ts>
               <ts e="T206" id="Seg_4253" n="e" s="T205">ukkɨr </ts>
               <ts e="T207" id="Seg_4255" n="e" s="T206">nälʼatɨ </ts>
               <ts e="T208" id="Seg_4257" n="e" s="T207">pɛläkɨtɨj. </ts>
               <ts e="T209" id="Seg_4259" n="e" s="T208">Takkɨn </ts>
               <ts e="T210" id="Seg_4261" n="e" s="T209">nʼenna </ts>
               <ts e="T211" id="Seg_4263" n="e" s="T210">šʼittɨ </ts>
               <ts e="T212" id="Seg_4265" n="e" s="T211">qälɨqı. </ts>
               <ts e="T213" id="Seg_4267" n="e" s="T212">Təp </ts>
               <ts e="T214" id="Seg_4269" n="e" s="T213">poːkɨtɨlʼ </ts>
               <ts e="T215" id="Seg_4271" n="e" s="T214">təttot. </ts>
               <ts e="T216" id="Seg_4273" n="e" s="T215">(Ponʼi </ts>
               <ts e="T217" id="Seg_4275" n="e" s="T216">ira=) </ts>
               <ts e="T218" id="Seg_4277" n="e" s="T217">Ponʼi </ts>
               <ts e="T219" id="Seg_4279" n="e" s="T218">ira </ts>
               <ts e="T220" id="Seg_4281" n="e" s="T219">naɛːntɨ </ts>
               <ts e="T221" id="Seg_4283" n="e" s="T220">kun </ts>
               <ts e="T222" id="Seg_4285" n="e" s="T221">montɨlʼ </ts>
               <ts e="T223" id="Seg_4287" n="e" s="T222">tamtɨrtɨ </ts>
               <ts e="T224" id="Seg_4289" n="e" s="T223">ɔːtäp </ts>
               <ts e="T225" id="Seg_4291" n="e" s="T224">muntɨk </ts>
               <ts e="T226" id="Seg_4293" n="e" s="T225">taqqɨšpatɨ. </ts>
               <ts e="T227" id="Seg_4295" n="e" s="T226">Tal. </ts>
               <ts e="T228" id="Seg_4297" n="e" s="T227">Nɨlčʼik </ts>
               <ts e="T229" id="Seg_4299" n="e" s="T228">tüŋa </ts>
               <ts e="T230" id="Seg_4301" n="e" s="T229">na </ts>
               <ts e="T231" id="Seg_4303" n="e" s="T230">timnʼasɨqä, </ts>
               <ts e="T232" id="Seg_4305" n="e" s="T231">na </ts>
               <ts e="T233" id="Seg_4307" n="e" s="T232">timnʼasɨqä </ts>
               <ts e="T234" id="Seg_4309" n="e" s="T233">ilɔːqı, </ts>
               <ts e="T235" id="Seg_4311" n="e" s="T234">na </ts>
               <ts e="T236" id="Seg_4313" n="e" s="T235">iralʼ </ts>
               <ts e="T237" id="Seg_4315" n="e" s="T236">timnʼasɨqä </ts>
               <ts e="T238" id="Seg_4317" n="e" s="T237">šʼittɨ. </ts>
               <ts e="T239" id="Seg_4319" n="e" s="T238">Tət </ts>
               <ts e="T240" id="Seg_4321" n="e" s="T239">toːn </ts>
               <ts e="T241" id="Seg_4323" n="e" s="T240">ɔːtätɨ. </ts>
               <ts e="T242" id="Seg_4325" n="e" s="T241">Na </ts>
               <ts e="T243" id="Seg_4327" n="e" s="T242">iran </ts>
               <ts e="T244" id="Seg_4329" n="e" s="T243">ɔːtäm </ts>
               <ts e="T245" id="Seg_4331" n="e" s="T244">iqɨntɔːqı. </ts>
               <ts e="T246" id="Seg_4333" n="e" s="T245">Qumiːntɨsa </ts>
               <ts e="T247" id="Seg_4335" n="e" s="T246">tünta </ts>
               <ts e="T248" id="Seg_4337" n="e" s="T247">Poni </ts>
               <ts e="T249" id="Seg_4339" n="e" s="T248">ira, </ts>
               <ts e="T250" id="Seg_4341" n="e" s="T249">nimtɨ </ts>
               <ts e="T251" id="Seg_4343" n="e" s="T250">najntɨ. </ts>
               <ts e="T252" id="Seg_4345" n="e" s="T251">Nɨnɨ </ts>
               <ts e="T253" id="Seg_4347" n="e" s="T252">tal. </ts>
               <ts e="T254" id="Seg_4349" n="e" s="T253">Tüla </ts>
               <ts e="T255" id="Seg_4351" n="e" s="T254">šäqqɔːtɨt </ts>
               <ts e="T256" id="Seg_4353" n="e" s="T255">na </ts>
               <ts e="T257" id="Seg_4355" n="e" s="T256">təptɨn </ts>
               <ts e="T258" id="Seg_4357" n="e" s="T257">mɔːt. </ts>
               <ts e="T259" id="Seg_4359" n="e" s="T258">Na </ts>
               <ts e="T260" id="Seg_4361" n="e" s="T259">ijap </ts>
               <ts e="T261" id="Seg_4363" n="e" s="T260">qapı </ts>
               <ts e="T262" id="Seg_4365" n="e" s="T261">aš </ts>
               <ts e="T263" id="Seg_4367" n="e" s="T262">tɛnɨmɔːt, </ts>
               <ts e="T264" id="Seg_4369" n="e" s="T263">((…)) </ts>
               <ts e="T265" id="Seg_4371" n="e" s="T264">kuttar </ts>
               <ts e="T266" id="Seg_4373" n="e" s="T265">koŋɨmmɛnta? </ts>
               <ts e="T267" id="Seg_4375" n="e" s="T266">Täp </ts>
               <ts e="T268" id="Seg_4377" n="e" s="T267">qaj </ts>
               <ts e="T269" id="Seg_4379" n="e" s="T268">((…)) </ts>
               <ts e="T270" id="Seg_4381" n="e" s="T269">mutronak </ts>
               <ts e="T271" id="Seg_4383" n="e" s="T270">((…)) </ts>
               <ts e="T272" id="Seg_4385" n="e" s="T271">ilʼ </ts>
               <ts e="T273" id="Seg_4387" n="e" s="T272">kuttar </ts>
               <ts e="T274" id="Seg_4389" n="e" s="T273">ila. </ts>
               <ts e="T275" id="Seg_4391" n="e" s="T274">Tıː </ts>
               <ts e="T276" id="Seg_4393" n="e" s="T275">kuttar </ts>
               <ts e="T277" id="Seg_4395" n="e" s="T276">tɛnɨmmɛntal? </ts>
               <ts e="T278" id="Seg_4397" n="e" s="T277">Alpän </ts>
               <ts e="T279" id="Seg_4399" n="e" s="T278">iläntɨj. </ts>
               <ts e="T280" id="Seg_4401" n="e" s="T279">Šäqqɔːqı </ts>
               <ts e="T281" id="Seg_4403" n="e" s="T280">na </ts>
               <ts e="T282" id="Seg_4405" n="e" s="T281">qaj. </ts>
               <ts e="T283" id="Seg_4407" n="e" s="T282">Mompa </ts>
               <ts e="T284" id="Seg_4409" n="e" s="T283">nɔːrtälɨj </ts>
               <ts e="T285" id="Seg_4411" n="e" s="T284">čʼeːlɨ </ts>
               <ts e="T286" id="Seg_4413" n="e" s="T285">toqɨn </ts>
               <ts e="T287" id="Seg_4415" n="e" s="T286">mompa </ts>
               <ts e="T288" id="Seg_4417" n="e" s="T287">na </ts>
               <ts e="T289" id="Seg_4419" n="e" s="T288">tüŋɨ. </ts>
               <ts e="T290" id="Seg_4421" n="e" s="T289">Mompa </ts>
               <ts e="T291" id="Seg_4423" n="e" s="T290">Poni </ts>
               <ts e="T292" id="Seg_4425" n="e" s="T291">ira </ts>
               <ts e="T293" id="Seg_4427" n="e" s="T292">((…)) </ts>
               <ts e="T294" id="Seg_4429" n="e" s="T293">köt </ts>
               <ts e="T295" id="Seg_4431" n="e" s="T294">qumsä </ts>
               <ts e="T296" id="Seg_4433" n="e" s="T295">(tümpa). </ts>
               <ts e="T297" id="Seg_4435" n="e" s="T296">Qapı </ts>
               <ts e="T298" id="Seg_4437" n="e" s="T297">na </ts>
               <ts e="T299" id="Seg_4439" n="e" s="T298">ɔːtät </ts>
               <ts e="T300" id="Seg_4441" n="e" s="T299">tɔːqqɨqɨntɔːqo. </ts>
               <ts e="T301" id="Seg_4443" n="e" s="T300">Na </ts>
               <ts e="T302" id="Seg_4445" n="e" s="T301">qumot </ts>
               <ts e="T303" id="Seg_4447" n="e" s="T302">nɔːt </ts>
               <ts e="T304" id="Seg_4449" n="e" s="T303">na </ts>
               <ts e="T305" id="Seg_4451" n="e" s="T304">timnʼasɨqän </ts>
               <ts e="T306" id="Seg_4453" n="e" s="T305">na </ts>
               <ts e="T307" id="Seg_4455" n="e" s="T306">ɔːtäp </ts>
               <ts e="T308" id="Seg_4457" n="e" s="T307">tɔːqqɨmpɔːt. </ts>
               <ts e="T309" id="Seg_4459" n="e" s="T308">Kun </ts>
               <ts e="T310" id="Seg_4461" n="e" s="T309">montɨlʼ </ts>
               <ts e="T311" id="Seg_4463" n="e" s="T310">mɔːttɨ </ts>
               <ts e="T312" id="Seg_4465" n="e" s="T311">tına </ts>
               <ts e="T313" id="Seg_4467" n="e" s="T312">šentɨ </ts>
               <ts e="T314" id="Seg_4469" n="e" s="T313">mɔːttɨ </ts>
               <ts e="T315" id="Seg_4471" n="e" s="T314">kun </ts>
               <ts e="T316" id="Seg_4473" n="e" s="T315">montɨlʼ </ts>
               <ts e="T317" id="Seg_4475" n="e" s="T316">qajtɨ </ts>
               <ts e="T318" id="Seg_4477" n="e" s="T317">muntɨ </ts>
               <ts e="T319" id="Seg_4479" n="e" s="T318">taqqɨltɛntɨŋit. </ts>
               <ts e="T320" id="Seg_4481" n="e" s="T319">Ukoːn </ts>
               <ts e="T321" id="Seg_4483" n="e" s="T320">nɔːta </ts>
               <ts e="T322" id="Seg_4485" n="e" s="T321">takkɨlʼ </ts>
               <ts e="T323" id="Seg_4487" n="e" s="T322">qälɨiːt </ts>
               <ts e="T324" id="Seg_4489" n="e" s="T323">ɔːtäp </ts>
               <ts e="T325" id="Seg_4491" n="e" s="T324">taqqɨllä </ts>
               <ts e="T326" id="Seg_4493" n="e" s="T325">nɔːtɨ </ts>
               <ts e="T327" id="Seg_4495" n="e" s="T326">muntɨk </ts>
               <ts e="T328" id="Seg_4497" n="e" s="T327">iːmpɔːt. </ts>
               <ts e="T329" id="Seg_4499" n="e" s="T328">Nɨnɨ </ts>
               <ts e="T330" id="Seg_4501" n="e" s="T329">šittäl </ts>
               <ts e="T331" id="Seg_4503" n="e" s="T330">mɨŋa. </ts>
               <ts e="T332" id="Seg_4505" n="e" s="T331">Moqɨnä </ts>
               <ts e="T333" id="Seg_4507" n="e" s="T332">qənnɔːt </ts>
               <ts e="T334" id="Seg_4509" n="e" s="T333">na </ts>
               <ts e="T335" id="Seg_4511" n="e" s="T334">qälɨt </ts>
               <ts e="T336" id="Seg_4513" n="e" s="T335">tıšša. </ts>
               <ts e="T337" id="Seg_4515" n="e" s="T336">Nɔːrtäl </ts>
               <ts e="T338" id="Seg_4517" n="e" s="T337">čʼeːlɨt </ts>
               <ts e="T339" id="Seg_4519" n="e" s="T338">mompa </ts>
               <ts e="T340" id="Seg_4521" n="e" s="T339">tüntɔːmɨn </ts>
               <ts e="T341" id="Seg_4523" n="e" s="T340">mɨta. </ts>
               <ts e="T342" id="Seg_4525" n="e" s="T341">Tɛː </ts>
               <ts e="T343" id="Seg_4527" n="e" s="T342">mompa </ts>
               <ts e="T344" id="Seg_4529" n="e" s="T343">kučʼčʼät </ts>
               <ts e="T345" id="Seg_4531" n="e" s="T344">ɨkɨ </ts>
               <ts e="T346" id="Seg_4533" n="e" s="T345">qənnɨlɨt. </ts>
               <ts e="T347" id="Seg_4535" n="e" s="T346">Šittälɨ </ts>
               <ts e="T348" id="Seg_4537" n="e" s="T347">čʼeːlontɨ </ts>
               <ts e="T349" id="Seg_4539" n="e" s="T348">na </ts>
               <ts e="T350" id="Seg_4541" n="e" s="T349">čʼeːlɨnna </ts>
               <ts e="T351" id="Seg_4543" n="e" s="T350">na </ts>
               <ts e="T352" id="Seg_4545" n="e" s="T351">ija </ts>
               <ts e="T353" id="Seg_4547" n="e" s="T352">nılʼčʼiŋ </ts>
               <ts e="T354" id="Seg_4549" n="e" s="T353">ɛsa </ts>
               <ts e="T355" id="Seg_4551" n="e" s="T354">na </ts>
               <ts e="T356" id="Seg_4553" n="e" s="T355">ilʼčʼantɨnɨk. </ts>
               <ts e="T357" id="Seg_4555" n="e" s="T356">Na </ts>
               <ts e="T358" id="Seg_4557" n="e" s="T357">gostʼan </ts>
               <ts e="T359" id="Seg_4559" n="e" s="T358">ilʼčʼant. </ts>
               <ts e="T360" id="Seg_4561" n="e" s="T359">Mɨta </ts>
               <ts e="T361" id="Seg_4563" n="e" s="T360">nʼenna </ts>
               <ts e="T362" id="Seg_4565" n="e" s="T361">tɨmtɨ </ts>
               <ts e="T363" id="Seg_4567" n="e" s="T362">mɨ </ts>
               <ts e="T364" id="Seg_4569" n="e" s="T363">ɛːnta </ts>
               <ts e="T365" id="Seg_4571" n="e" s="T364">mɨta. </ts>
               <ts e="T366" id="Seg_4573" n="e" s="T365">Tɛːt </ts>
               <ts e="T367" id="Seg_4575" n="e" s="T366">toːn </ts>
               <ts e="T368" id="Seg_4577" n="e" s="T367">ɔːtän </ts>
               <ts e="T369" id="Seg_4579" n="e" s="T368">nɨŋqɨkuššaŋ, </ts>
               <ts e="T370" id="Seg_4581" n="e" s="T369">mɨta </ts>
               <ts e="T371" id="Seg_4583" n="e" s="T370">nʼaraŋ </ts>
               <ts e="T372" id="Seg_4585" n="e" s="T371">ɛːntɨ, </ts>
               <ts e="T373" id="Seg_4587" n="e" s="T372">mɨta </ts>
               <ts e="T374" id="Seg_4589" n="e" s="T373">näčʼčʼatɨ </ts>
               <ts e="T375" id="Seg_4591" n="e" s="T374">üːtalɔːmɨn </ts>
               <ts e="T376" id="Seg_4593" n="e" s="T375">mɨta. </ts>
               <ts e="T377" id="Seg_4595" n="e" s="T376">Qälɨt </ts>
               <ts e="T378" id="Seg_4597" n="e" s="T377">tüntɔːtɨn </ts>
               <ts e="T379" id="Seg_4599" n="e" s="T378">nɨntɨ </ts>
               <ts e="T380" id="Seg_4601" n="e" s="T379">ɔːtäp </ts>
               <ts e="T381" id="Seg_4603" n="e" s="T380">tɔːqqɨqolamnɛntɔːtɨn, </ts>
               <ts e="T382" id="Seg_4605" n="e" s="T381">nın </ts>
               <ts e="T383" id="Seg_4607" n="e" s="T382">mɨta </ts>
               <ts e="T384" id="Seg_4609" n="e" s="T383">tına. </ts>
               <ts e="T385" id="Seg_4611" n="e" s="T384">Ɔːtän </ts>
               <ts e="T386" id="Seg_4613" n="e" s="T385">uptɨ </ts>
               <ts e="T387" id="Seg_4615" n="e" s="T386">kəlʼčʼomɨnto </ts>
               <ts e="T388" id="Seg_4617" n="e" s="T387">kərɨmpɨlʼ </ts>
               <ts e="T389" id="Seg_4619" n="e" s="T388">mɨta </ts>
               <ts e="T390" id="Seg_4621" n="e" s="T389">kučʼčʼa </ts>
               <ts e="T391" id="Seg_4623" n="e" s="T390">meːntɔːmɨn? </ts>
               <ts e="T392" id="Seg_4625" n="e" s="T391">Na </ts>
               <ts e="T393" id="Seg_4627" n="e" s="T392">ija </ts>
               <ts e="T394" id="Seg_4629" n="e" s="T393">nık </ts>
               <ts e="T395" id="Seg_4631" n="e" s="T394">kətɨŋɨtɨ. </ts>
               <ts e="T396" id="Seg_4633" n="e" s="T395">Nɨnɨ </ts>
               <ts e="T397" id="Seg_4635" n="e" s="T396">nık </ts>
               <ts e="T398" id="Seg_4637" n="e" s="T397">kətɨŋɨtɨ </ts>
               <ts e="T399" id="Seg_4639" n="e" s="T398">mɨta </ts>
               <ts e="T400" id="Seg_4641" n="e" s="T399">((…)). </ts>
               <ts e="T401" id="Seg_4643" n="e" s="T400">Mäkka </ts>
               <ts e="T402" id="Seg_4645" n="e" s="T401">mitɨ </ts>
               <ts e="T403" id="Seg_4647" n="e" s="T402">šentɨ, </ts>
               <ts e="T404" id="Seg_4649" n="e" s="T403">šentɨ </ts>
               <ts e="T405" id="Seg_4651" n="e" s="T404">mɔːllɨ </ts>
               <ts e="T406" id="Seg_4653" n="e" s="T405">ɛːŋa? </ts>
               <ts e="T407" id="Seg_4655" n="e" s="T406">Ilʼčʼa </ts>
               <ts e="T408" id="Seg_4657" n="e" s="T407">mɨta </ts>
               <ts e="T409" id="Seg_4659" n="e" s="T408">qaj </ts>
               <ts e="T410" id="Seg_4661" n="e" s="T409">šentɨ </ts>
               <ts e="T411" id="Seg_4663" n="e" s="T410">mɔːllɨ </ts>
               <ts e="T412" id="Seg_4665" n="e" s="T411">ɛːŋa? </ts>
               <ts e="T413" id="Seg_4667" n="e" s="T412">Mäkka </ts>
               <ts e="T414" id="Seg_4669" n="e" s="T413">mitɨ </ts>
               <ts e="T415" id="Seg_4671" n="e" s="T414">mɨta. </ts>
               <ts e="T416" id="Seg_4673" n="e" s="T415">A </ts>
               <ts e="T417" id="Seg_4675" n="e" s="T416">toːnna </ts>
               <ts e="T418" id="Seg_4677" n="e" s="T417">tına </ts>
               <ts e="T419" id="Seg_4679" n="e" s="T418">timnʼantɨ </ts>
               <ts e="T420" id="Seg_4681" n="e" s="T419">nälʼa, </ts>
               <ts e="T421" id="Seg_4683" n="e" s="T420">ilʼčʼatɨ </ts>
               <ts e="T422" id="Seg_4685" n="e" s="T421">namɨp </ts>
               <ts e="T423" id="Seg_4687" n="e" s="T422">quraltɨmmɨntɨt </ts>
               <ts e="T424" id="Seg_4689" n="e" s="T423">na </ts>
               <ts e="T425" id="Seg_4691" n="e" s="T424">ijanɨk. </ts>
               <ts e="T426" id="Seg_4693" n="e" s="T425">Man </ts>
               <ts e="T427" id="Seg_4695" n="e" s="T426">mɨta </ts>
               <ts e="T428" id="Seg_4697" n="e" s="T427">prosto </ts>
               <ts e="T429" id="Seg_4699" n="e" s="T428">məčʼimnɛntak </ts>
               <ts e="T430" id="Seg_4701" n="e" s="T429">mɔːt, </ts>
               <ts e="T431" id="Seg_4703" n="e" s="T430">nʼennat </ts>
               <ts e="T432" id="Seg_4705" n="e" s="T431">mɨntɨ </ts>
               <ts e="T433" id="Seg_4707" n="e" s="T432">mačʼilʼ </ts>
               <ts e="T434" id="Seg_4709" n="e" s="T433">tıčʼi </ts>
               <ts e="T435" id="Seg_4711" n="e" s="T434">ɛnta, </ts>
               <ts e="T436" id="Seg_4713" n="e" s="T435">na </ts>
               <ts e="T437" id="Seg_4715" n="e" s="T436">nʼennälʼ </ts>
               <ts e="T438" id="Seg_4717" n="e" s="T437">pɛlaktɨ </ts>
               <ts e="T439" id="Seg_4719" n="e" s="T438">mɔːtɨŋnɛntak. </ts>
               <ts e="T440" id="Seg_4721" n="e" s="T439">Šentɨ </ts>
               <ts e="T441" id="Seg_4723" n="e" s="T440">mɔːt </ts>
               <ts e="T442" id="Seg_4725" n="e" s="T441">taːtɨŋa </ts>
               <ts e="T443" id="Seg_4727" n="e" s="T442">nɛntɨ. </ts>
               <ts e="T444" id="Seg_4729" n="e" s="T443">Üːtalnɔːtɨt. </ts>
               <ts e="T445" id="Seg_4731" n="e" s="T444">Tal. </ts>
               <ts e="T446" id="Seg_4733" n="e" s="T445">Üːtaltɔːt </ts>
               <ts e="T447" id="Seg_4735" n="e" s="T446">na. </ts>
               <ts e="T448" id="Seg_4737" n="e" s="T447">Qapija </ts>
               <ts e="T449" id="Seg_4739" n="e" s="T448">nuːnɨčʼɨsɨ, </ts>
               <ts e="T450" id="Seg_4741" n="e" s="T449">šittɨmtäl, </ts>
               <ts e="T451" id="Seg_4743" n="e" s="T450">nɔːrtäl </ts>
               <ts e="T452" id="Seg_4745" n="e" s="T451">čʼeːlɨ </ts>
               <ts e="T453" id="Seg_4747" n="e" s="T452">tüntɔːt </ts>
               <ts e="T454" id="Seg_4749" n="e" s="T453">Poni </ts>
               <ts e="T455" id="Seg_4751" n="e" s="T454">iralʼ </ts>
               <ts e="T456" id="Seg_4753" n="e" s="T455">mɨ </ts>
               <ts e="T457" id="Seg_4755" n="e" s="T456">təpɨtɨtkin. </ts>
               <ts e="T458" id="Seg_4757" n="e" s="T457">Na </ts>
               <ts e="T459" id="Seg_4759" n="e" s="T458">qälɨ </ts>
               <ts e="T460" id="Seg_4761" n="e" s="T459">iralʼ </ts>
               <ts e="T461" id="Seg_4763" n="e" s="T460">mɨqä </ts>
               <ts e="T462" id="Seg_4765" n="e" s="T461">nɔːtɨ </ts>
               <ts e="T463" id="Seg_4767" n="e" s="T462">taqalqɨnto, </ts>
               <ts e="T464" id="Seg_4769" n="e" s="T463">timnʼasɨqä </ts>
               <ts e="T465" id="Seg_4771" n="e" s="T464">nɔːtɨ </ts>
               <ts e="T466" id="Seg_4773" n="e" s="T465">(tap </ts>
               <ts e="T467" id="Seg_4775" n="e" s="T466">čʼeːlqot). </ts>
               <ts e="T468" id="Seg_4777" n="e" s="T467">Nɨnɨ </ts>
               <ts e="T469" id="Seg_4779" n="e" s="T468">šʼittalʼ </ts>
               <ts e="T470" id="Seg_4781" n="e" s="T469">üːtaltɔːt </ts>
               <ts e="T471" id="Seg_4783" n="e" s="T470">na </ts>
               <ts e="T472" id="Seg_4785" n="e" s="T471">ɔːtam </ts>
               <ts e="T473" id="Seg_4787" n="e" s="T472">na </ts>
               <ts e="T474" id="Seg_4789" n="e" s="T473">tɔːqqɨntɔːt, </ts>
               <ts e="T475" id="Seg_4791" n="e" s="T474">qapij </ts>
               <ts e="T476" id="Seg_4793" n="e" s="T475">tɛm </ts>
               <ts e="T477" id="Seg_4795" n="e" s="T476">ɛj </ts>
               <ts e="T478" id="Seg_4797" n="e" s="T477">((…)). </ts>
               <ts e="T479" id="Seg_4799" n="e" s="T478">Mɔːssa </ts>
               <ts e="T480" id="Seg_4801" n="e" s="T479">na </ts>
               <ts e="T481" id="Seg_4803" n="e" s="T480">mintɔːt. </ts>
               <ts e="T482" id="Seg_4805" n="e" s="T481">Mɔːt </ts>
               <ts e="T483" id="Seg_4807" n="e" s="T482">šerna, </ts>
               <ts e="T484" id="Seg_4809" n="e" s="T483">na </ts>
               <ts e="T485" id="Seg_4811" n="e" s="T484">mačʼit </ts>
               <ts e="T486" id="Seg_4813" n="e" s="T485">tıčʼɨt </ts>
               <ts e="T487" id="Seg_4815" n="e" s="T486">jennalʼ </ts>
               <ts e="T488" id="Seg_4817" n="e" s="T487">pɛlalʼ </ts>
               <ts e="T489" id="Seg_4819" n="e" s="T488">mɔːt </ts>
               <ts e="T490" id="Seg_4821" n="e" s="T489">šerna. </ts>
               <ts e="T491" id="Seg_4823" n="e" s="T490">Nɨn </ts>
               <ts e="T492" id="Seg_4825" n="e" s="T491">na </ts>
               <ts e="T493" id="Seg_4827" n="e" s="T492">mɔːtɨŋtɔːqı </ts>
               <ts e="T494" id="Seg_4829" n="e" s="T493">na </ts>
               <ts e="T495" id="Seg_4831" n="e" s="T494">imantɨsa. </ts>
               <ts e="T496" id="Seg_4833" n="e" s="T495">Qapı </ts>
               <ts e="T497" id="Seg_4835" n="e" s="T496">təttalʼ </ts>
               <ts e="T993" id="Seg_4837" n="e" s="T497">qälʼ </ts>
               <ts e="T498" id="Seg_4839" n="e" s="T993">ira </ts>
               <ts e="T499" id="Seg_4841" n="e" s="T498">qaj </ts>
               <ts e="T500" id="Seg_4843" n="e" s="T499">mɔːttɨ </ts>
               <ts e="T501" id="Seg_4845" n="e" s="T500">čʼäŋka. </ts>
               <ts e="T502" id="Seg_4847" n="e" s="T501">Šentɨ </ts>
               <ts e="T503" id="Seg_4849" n="e" s="T502">mɔːttɨ </ts>
               <ts e="T504" id="Seg_4851" n="e" s="T503">qopɨlʼ </ts>
               <ts e="T505" id="Seg_4853" n="e" s="T504">mɔːttɨ </ts>
               <ts e="T506" id="Seg_4855" n="e" s="T505">tına. </ts>
               <ts e="T507" id="Seg_4857" n="e" s="T506">Wərqɨlɔː </ts>
               <ts e="T975" id="Seg_4859" n="e" s="T507">čʼesɨŋɨtɨ. </ts>
               <ts e="T508" id="Seg_4861" n="e" s="T975">((NN:)) </ts>
               <ts e="T509" id="Seg_4863" n="e" s="T508">((…)) </ts>
               <ts e="T976" id="Seg_4865" n="e" s="T509">koŋɨmpäš. </ts>
               <ts e="T991" id="Seg_4867" n="e" s="T976">((NN:)) </ts>
               <ts e="T990" id="Seg_4869" n="e" s="T991">Бабушка? </ts>
               <ts e="T510" id="Seg_4871" n="e" s="T990">((NEP:)) </ts>
               <ts e="T977" id="Seg_4873" n="e" s="T510">А? </ts>
               <ts e="T511" id="Seg_4875" n="e" s="T977">((NN:)) </ts>
               <ts e="T512" id="Seg_4877" n="e" s="T511">Na </ts>
               <ts e="T513" id="Seg_4879" n="e" s="T512">ıllalɔː </ts>
               <ts e="T514" id="Seg_4881" n="e" s="T513">uːkaltäš. </ts>
               <ts e="T515" id="Seg_4883" n="e" s="T514">((KuAI:)) Сейчас попробуем. ((BRK)). </ts>
               <ts e="T978" id="Seg_4885" n="e" s="T515">((NN:)) ((…)) а на радио ((LAUGH))… </ts>
               <ts e="T516" id="Seg_4887" n="e" s="T978">((NN:)) </ts>
               <ts e="T517" id="Seg_4889" n="e" s="T516">Na </ts>
               <ts e="T518" id="Seg_4891" n="e" s="T517">mantɨ </ts>
               <ts e="T979" id="Seg_4893" n="e" s="T518">sprašivaet. </ts>
               <ts e="T519" id="Seg_4895" n="e" s="T979">((NN:)) </ts>
               <ts e="T520" id="Seg_4897" n="e" s="T519">Somaŋ </ts>
               <ts e="T521" id="Seg_4899" n="e" s="T520">na </ts>
               <ts e="T522" id="Seg_4901" n="e" s="T521">koŋɨmmɨntɨ, </ts>
               <ts e="T523" id="Seg_4903" n="e" s="T522">na </ts>
               <ts e="T980" id="Seg_4905" n="e" s="T523">koŋaltɨmmɨntɨ. </ts>
               <ts e="T524" id="Seg_4907" n="e" s="T980">((NN:)) </ts>
               <ts e="T525" id="Seg_4909" n="e" s="T524">Tal, </ts>
               <ts e="T526" id="Seg_4911" n="e" s="T525">ɨkɨ </ts>
               <ts e="T981" id="Seg_4913" n="e" s="T526">mortäš. </ts>
               <ts e="T527" id="Seg_4915" n="e" s="T981">((NN:)) </ts>
               <ts e="T528" id="Seg_4917" n="e" s="T527">Qapija </ts>
               <ts e="T529" id="Seg_4919" n="e" s="T528">qoːkɨtalʼ </ts>
               <ts e="T982" id="Seg_4921" n="e" s="T529">((…)). </ts>
               <ts e="T530" id="Seg_4923" n="e" s="T982">((NN:)) </ts>
               <ts e="T531" id="Seg_4925" n="e" s="T530">Qapı </ts>
               <ts e="T532" id="Seg_4927" n="e" s="T531">üŋkɨltɨmpatɨ </ts>
               <ts e="T533" id="Seg_4929" n="e" s="T532">moʒet_pɨtʼ </ts>
               <ts e="T534" id="Seg_4931" n="e" s="T533">qoškɔːl </ts>
               <ts e="T983" id="Seg_4933" n="e" s="T534">kətɨŋɨtɨ. </ts>
               <ts e="T535" id="Seg_4935" n="e" s="T983">((NN:)) </ts>
               <ts e="T974" id="Seg_4937" n="e" s="T535">Əːtɨkɔːl. </ts>
               <ts e="T536" id="Seg_4939" n="e" s="T974">((NEP:)) </ts>
               <ts e="T537" id="Seg_4941" n="e" s="T536">Nɨnɨ </ts>
               <ts e="T538" id="Seg_4943" n="e" s="T537">šittäl </ts>
               <ts e="T539" id="Seg_4945" n="e" s="T538">šäqqɔːj </ts>
               <ts e="T540" id="Seg_4947" n="e" s="T539">na </ts>
               <ts e="T541" id="Seg_4949" n="e" s="T540">ija </ts>
               <ts e="T542" id="Seg_4951" n="e" s="T541">imantɨsa. </ts>
               <ts e="T543" id="Seg_4953" n="e" s="T542">Šäqqɔːt, </ts>
               <ts e="T544" id="Seg_4955" n="e" s="T543">toptɨlʼ </ts>
               <ts e="T545" id="Seg_4957" n="e" s="T544">qar </ts>
               <ts e="T546" id="Seg_4959" n="e" s="T545">nılʼ </ts>
               <ts e="T547" id="Seg_4961" n="e" s="T546">ɛsa </ts>
               <ts e="T548" id="Seg_4963" n="e" s="T547">tına </ts>
               <ts e="T549" id="Seg_4965" n="e" s="T548">imantɨn. </ts>
               <ts e="T550" id="Seg_4967" n="e" s="T549">Man </ts>
               <ts e="T551" id="Seg_4969" n="e" s="T550">mɨta </ts>
               <ts e="T552" id="Seg_4971" n="e" s="T551">nʼenna </ts>
               <ts e="T553" id="Seg_4973" n="e" s="T552">qumiːqan </ts>
               <ts e="T554" id="Seg_4975" n="e" s="T553">qonʼišak </ts>
               <ts e="T555" id="Seg_4977" n="e" s="T554">qaj </ts>
               <ts e="T556" id="Seg_4979" n="e" s="T555">mɨta. </ts>
               <ts e="T557" id="Seg_4981" n="e" s="T556">Mɔːtɨŋpɔːtɨn, </ts>
               <ts e="T558" id="Seg_4983" n="e" s="T557">qaj </ts>
               <ts e="T559" id="Seg_4985" n="e" s="T558">qattɨmpɔːtɨn. </ts>
               <ts e="T560" id="Seg_4987" n="e" s="T559">Nʼenna </ts>
               <ts e="T561" id="Seg_4989" n="e" s="T560">laqaltɛlʼčʼa </ts>
               <ts e="T562" id="Seg_4991" n="e" s="T561">nɔːkɨr </ts>
               <ts e="T563" id="Seg_4993" n="e" s="T562">qoptɨp </ts>
               <ts e="T564" id="Seg_4995" n="e" s="T563">sɔːrɨla. </ts>
               <ts e="T565" id="Seg_4997" n="e" s="T564">Qapı </ts>
               <ts e="T566" id="Seg_4999" n="e" s="T565">nɔːssarɨlʼ </ts>
               <ts e="T567" id="Seg_5001" n="e" s="T566">ɔːta </ts>
               <ts e="T568" id="Seg_5003" n="e" s="T567">qaltɨmɨntɨt </ts>
               <ts e="T569" id="Seg_5005" n="e" s="T568">na. </ts>
               <ts e="T570" id="Seg_5007" n="e" s="T569">Ija </ts>
               <ts e="T571" id="Seg_5009" n="e" s="T570">nʼenna </ts>
               <ts e="T572" id="Seg_5011" n="e" s="T571">qənaisɨ, </ts>
               <ts e="T573" id="Seg_5013" n="e" s="T572">imatɨ </ts>
               <ts e="T574" id="Seg_5015" n="e" s="T573">ontɨ </ts>
               <ts e="T575" id="Seg_5017" n="e" s="T574">qala </ts>
               <ts e="T576" id="Seg_5019" n="e" s="T575">pɛläkɔːl. </ts>
               <ts e="T577" id="Seg_5021" n="e" s="T576">Na </ts>
               <ts e="T578" id="Seg_5023" n="e" s="T577">ɔːmtɨŋa. </ts>
               <ts e="T579" id="Seg_5025" n="e" s="T578">Ompa </ts>
               <ts e="T580" id="Seg_5027" n="e" s="T579">na </ts>
               <ts e="T581" id="Seg_5029" n="e" s="T580">((…)). </ts>
               <ts e="T582" id="Seg_5031" n="e" s="T581">Ukkɨr </ts>
               <ts e="T583" id="Seg_5033" n="e" s="T582">tot </ts>
               <ts e="T584" id="Seg_5035" n="e" s="T583">čʼontot </ts>
               <ts e="T585" id="Seg_5037" n="e" s="T584">čʼeːlɨtɨ </ts>
               <ts e="T586" id="Seg_5039" n="e" s="T585">üːtontɨ </ts>
               <ts e="T587" id="Seg_5041" n="e" s="T586">na </ts>
               <ts e="T588" id="Seg_5043" n="e" s="T587">qəntɨ. </ts>
               <ts e="T589" id="Seg_5045" n="e" s="T588">Üːtontɨqɨn </ts>
               <ts e="T590" id="Seg_5047" n="e" s="T589">na </ts>
               <ts e="T591" id="Seg_5049" n="e" s="T590">ija </ts>
               <ts e="T592" id="Seg_5051" n="e" s="T591">lʼamɨk </ts>
               <ts e="T593" id="Seg_5053" n="e" s="T592">ɛːŋa </ts>
               <ts e="T594" id="Seg_5055" n="e" s="T593">qapı </ts>
               <ts e="T595" id="Seg_5057" n="e" s="T594">nʼenna. </ts>
               <ts e="T596" id="Seg_5059" n="e" s="T595">Ukkɨr </ts>
               <ts e="T597" id="Seg_5061" n="e" s="T596">tät </ts>
               <ts e="T598" id="Seg_5063" n="e" s="T597">čʼontot </ts>
               <ts e="T599" id="Seg_5065" n="e" s="T598">mɨ </ts>
               <ts e="T600" id="Seg_5067" n="e" s="T599">qum </ts>
               <ts e="T601" id="Seg_5069" n="e" s="T600">(na </ts>
               <ts e="T602" id="Seg_5071" n="e" s="T601">jap) </ts>
               <ts e="T603" id="Seg_5073" n="e" s="T602">tattɨntɔːt </ts>
               <ts e="T604" id="Seg_5075" n="e" s="T603">takkɨntɨt. </ts>
               <ts e="T605" id="Seg_5077" n="e" s="T604">Poni </ts>
               <ts e="T606" id="Seg_5079" n="e" s="T605">iralʼ </ts>
               <ts e="T967" id="Seg_5081" n="e" s="T606">(na </ts>
               <ts e="T607" id="Seg_5083" n="e" s="T967">jap) </ts>
               <ts e="T608" id="Seg_5085" n="e" s="T607">tattɨntɔːtɨt. </ts>
               <ts e="T609" id="Seg_5087" n="e" s="T608">Köt </ts>
               <ts e="T610" id="Seg_5089" n="e" s="T609">qumiːtɨ </ts>
               <ts e="T611" id="Seg_5091" n="e" s="T610">qapı– </ts>
               <ts e="T612" id="Seg_5093" n="e" s="T611">ɔːtap </ts>
               <ts e="T613" id="Seg_5095" n="e" s="T612">tɔːqqɨmpɨlʼ. </ts>
               <ts e="T614" id="Seg_5097" n="e" s="T613">Poni </ts>
               <ts e="T615" id="Seg_5099" n="e" s="T614">ira </ts>
               <ts e="T616" id="Seg_5101" n="e" s="T615">naššak </ts>
               <ts e="T617" id="Seg_5103" n="e" s="T616">qotɨlʼ </ts>
               <ts e="T618" id="Seg_5105" n="e" s="T617">ɛːŋa, </ts>
               <ts e="T619" id="Seg_5107" n="e" s="T618">naššak </ts>
               <ts e="T620" id="Seg_5109" n="e" s="T619">qəːtɨsä </ts>
               <ts e="T621" id="Seg_5111" n="e" s="T620">ɛːŋa. </ts>
               <ts e="T622" id="Seg_5113" n="e" s="T621">Nɨnɨ </ts>
               <ts e="T623" id="Seg_5115" n="e" s="T622">šittäl </ts>
               <ts e="T624" id="Seg_5117" n="e" s="T623">tal </ts>
               <ts e="T625" id="Seg_5119" n="e" s="T624">na </ts>
               <ts e="T626" id="Seg_5121" n="e" s="T625">ija </ts>
               <ts e="T627" id="Seg_5123" n="e" s="T626">tülʼčʼikunä– </ts>
               <ts e="T628" id="Seg_5125" n="e" s="T627">ponä </ts>
               <ts e="T629" id="Seg_5127" n="e" s="T628">imatɨ </ts>
               <ts e="T630" id="Seg_5129" n="e" s="T629">üŋkɨltɨmpa. </ts>
               <ts e="T631" id="Seg_5131" n="e" s="T630">Tülʼčʼikunä </ts>
               <ts e="T632" id="Seg_5133" n="e" s="T631">na </ts>
               <ts e="T633" id="Seg_5135" n="e" s="T632">ɔːtamtɨ </ts>
               <ts e="T634" id="Seg_5137" n="e" s="T633">toː </ts>
               <ts e="T635" id="Seg_5139" n="e" s="T634">sɔːra </ts>
               <ts e="T636" id="Seg_5141" n="e" s="T635">ɔːtamtɨ. </ts>
               <ts e="T984" id="Seg_5143" n="e" s="T636">((NN:)) ((…)). </ts>
               <ts e="T637" id="Seg_5145" n="e" s="T984">((NEP:)) </ts>
               <ts e="T638" id="Seg_5147" n="e" s="T637">Montɨ </ts>
               <ts e="T639" id="Seg_5149" n="e" s="T638">čʼap </ts>
               <ts e="T640" id="Seg_5151" n="e" s="T639">tülʼčʼa </ts>
               <ts e="T641" id="Seg_5153" n="e" s="T640">montɨ. </ts>
               <ts e="T642" id="Seg_5155" n="e" s="T641">Poni </ts>
               <ts e="T643" id="Seg_5157" n="e" s="T642">iralʼ </ts>
               <ts e="T644" id="Seg_5159" n="e" s="T643">mɨt </ts>
               <ts e="T645" id="Seg_5161" n="e" s="T644">qaj </ts>
               <ts e="T646" id="Seg_5163" n="e" s="T645">montɨ </ts>
               <ts e="T647" id="Seg_5165" n="e" s="T646">tüŋɔːtɨt, </ts>
               <ts e="T648" id="Seg_5167" n="e" s="T647">qaqlɨlʼ </ts>
               <ts e="T649" id="Seg_5169" n="e" s="T648">mɨtɨt </ts>
               <ts e="T650" id="Seg_5171" n="e" s="T649">nık </ts>
               <ts e="T651" id="Seg_5173" n="e" s="T650">tottälɨmpɔːt. </ts>
               <ts e="T652" id="Seg_5175" n="e" s="T651">Tına </ts>
               <ts e="T653" id="Seg_5177" n="e" s="T652">ətɨmantot </ts>
               <ts e="T654" id="Seg_5179" n="e" s="T653">təpɨt </ts>
               <ts e="T655" id="Seg_5181" n="e" s="T654">ətɨmantot. </ts>
               <ts e="T656" id="Seg_5183" n="e" s="T655">Šerna, </ts>
               <ts e="T657" id="Seg_5185" n="e" s="T656">mɔːt </ts>
               <ts e="T658" id="Seg_5187" n="e" s="T657">šerna, </ts>
               <ts e="T659" id="Seg_5189" n="e" s="T658">mɔːtan </ts>
               <ts e="T660" id="Seg_5191" n="e" s="T659">ɔːt </ts>
               <ts e="T661" id="Seg_5193" n="e" s="T660">nɨŋkɨla </ts>
               <ts e="T662" id="Seg_5195" n="e" s="T661">qälɨk. </ts>
               <ts e="T663" id="Seg_5197" n="e" s="T662">Tokalʼ </ts>
               <ts e="T664" id="Seg_5199" n="e" s="T663">mɔːtan </ts>
               <ts e="T665" id="Seg_5201" n="e" s="T664">ɔːt </ts>
               <ts e="T666" id="Seg_5203" n="e" s="T665">qät </ts>
               <ts e="T667" id="Seg_5205" n="e" s="T666">topɨmt </ts>
               <ts e="T668" id="Seg_5207" n="e" s="T667">tupalna. </ts>
               <ts e="T669" id="Seg_5209" n="e" s="T668">Peːmɨmtɨ </ts>
               <ts e="T670" id="Seg_5211" n="e" s="T669">na </ts>
               <ts e="T671" id="Seg_5213" n="e" s="T670">tupaltɨ. </ts>
               <ts e="T672" id="Seg_5215" n="e" s="T671">Qannɨmpɨlʼ </ts>
               <ts e="T673" id="Seg_5217" n="e" s="T672">pɛlalʼ </ts>
               <ts e="T674" id="Seg_5219" n="e" s="T673">peːmɨtɨ </ts>
               <ts e="T675" id="Seg_5221" n="e" s="T674">tupalnɨt. </ts>
               <ts e="T676" id="Seg_5223" n="e" s="T675">Nık </ts>
               <ts e="T677" id="Seg_5225" n="e" s="T676">kətɨŋɨtɨ </ts>
               <ts e="T678" id="Seg_5227" n="e" s="T677">nʼenna </ts>
               <ts e="T679" id="Seg_5229" n="e" s="T678">koralla </ts>
               <ts e="T680" id="Seg_5231" n="e" s="T679">Poni </ts>
               <ts e="T681" id="Seg_5233" n="e" s="T680">iran </ts>
               <ts e="T682" id="Seg_5235" n="e" s="T681">mɨta. </ts>
               <ts e="T683" id="Seg_5237" n="e" s="T682">Ɔːklɨ </ts>
               <ts e="T684" id="Seg_5239" n="e" s="T683">mitɨ </ts>
               <ts e="T685" id="Seg_5241" n="e" s="T684">mɨta </ts>
               <ts e="T686" id="Seg_5243" n="e" s="T685">qolqol </ts>
               <ts e="T687" id="Seg_5245" n="e" s="T686">olʼa </ts>
               <ts e="T688" id="Seg_5247" n="e" s="T687">mɨ. </ts>
               <ts e="T689" id="Seg_5249" n="e" s="T688">Taŋal. </ts>
               <ts e="T690" id="Seg_5251" n="e" s="T689">Na </ts>
               <ts e="T691" id="Seg_5253" n="e" s="T690">nʼenna </ts>
               <ts e="T692" id="Seg_5255" n="e" s="T691">paktɨmmɨntɨ </ts>
               <ts e="T693" id="Seg_5257" n="e" s="T692">na </ts>
               <ts e="T694" id="Seg_5259" n="e" s="T693">kupaks. </ts>
               <ts e="T695" id="Seg_5261" n="e" s="T694">Təːqalaimmɨntɨtɨ </ts>
               <ts e="T696" id="Seg_5263" n="e" s="T695">pɛlaj </ts>
               <ts e="T697" id="Seg_5265" n="e" s="T696">utɨntɨ </ts>
               <ts e="T698" id="Seg_5267" n="e" s="T697">təːqalaimpat. </ts>
               <ts e="T699" id="Seg_5269" n="e" s="T698">Poni </ts>
               <ts e="T700" id="Seg_5271" n="e" s="T699">iralʼ </ts>
               <ts e="T701" id="Seg_5273" n="e" s="T700">mɨtɨp </ts>
               <ts e="T702" id="Seg_5275" n="e" s="T701">muntɨk </ts>
               <ts e="T703" id="Seg_5277" n="e" s="T702">lɛpäk </ts>
               <ts e="T704" id="Seg_5279" n="e" s="T703">qättɨmpatɨ. </ts>
               <ts e="T705" id="Seg_5281" n="e" s="T704">Ɔːmɨtɨ </ts>
               <ts e="T706" id="Seg_5283" n="e" s="T705">qumpa. </ts>
               <ts e="T707" id="Seg_5285" n="e" s="T706">Poni </ts>
               <ts e="T708" id="Seg_5287" n="e" s="T707">ira </ts>
               <ts e="T709" id="Seg_5289" n="e" s="T708">illa </ts>
               <ts e="T710" id="Seg_5291" n="e" s="T709">qalɨmtɨ, </ts>
               <ts e="T711" id="Seg_5293" n="e" s="T710">šittɨ </ts>
               <ts e="T712" id="Seg_5295" n="e" s="T711">qälɨqı </ts>
               <ts e="T713" id="Seg_5297" n="e" s="T712">illa </ts>
               <ts e="T714" id="Seg_5299" n="e" s="T713">qalɨmtɨ, </ts>
               <ts e="T715" id="Seg_5301" n="e" s="T714">pona </ts>
               <ts e="T716" id="Seg_5303" n="e" s="T715">qəːllaıːntɨ </ts>
               <ts e="T717" id="Seg_5305" n="e" s="T716">na </ts>
               <ts e="T718" id="Seg_5307" n="e" s="T717">ija. </ts>
               <ts e="T719" id="Seg_5309" n="e" s="T718">Ukkɨr </ts>
               <ts e="T720" id="Seg_5311" n="e" s="T719">pɔːr </ts>
               <ts e="T721" id="Seg_5313" n="e" s="T720">kupaksä </ts>
               <ts e="T722" id="Seg_5315" n="e" s="T721">təːqalaimpat. </ts>
               <ts e="T723" id="Seg_5317" n="e" s="T722">Muntɨk </ts>
               <ts e="T724" id="Seg_5319" n="e" s="T723">moqɨnät </ts>
               <ts e="T725" id="Seg_5321" n="e" s="T724">porqɨn </ts>
               <ts e="T726" id="Seg_5323" n="e" s="T725">šunʼnʼontɨ </ts>
               <ts e="T727" id="Seg_5325" n="e" s="T726">nʼolʼqɨmɔːtpɔːtɨn, </ts>
               <ts e="T728" id="Seg_5327" n="e" s="T727">na </ts>
               <ts e="T729" id="Seg_5329" n="e" s="T728">qumpɨlʼ </ts>
               <ts e="T730" id="Seg_5331" n="e" s="T729">mɨ. </ts>
               <ts e="T731" id="Seg_5333" n="e" s="T730">Pista. </ts>
               <ts e="T732" id="Seg_5335" n="e" s="T731">Nɨnɨ </ts>
               <ts e="T733" id="Seg_5337" n="e" s="T732">šittäl </ts>
               <ts e="T734" id="Seg_5339" n="e" s="T733">ponä </ts>
               <ts e="T735" id="Seg_5341" n="e" s="T734">qəllaıːŋɨtɨ </ts>
               <ts e="T736" id="Seg_5343" n="e" s="T735">ətɨmantontɨ, </ts>
               <ts e="T737" id="Seg_5345" n="e" s="T736">tınta </ts>
               <ts e="T738" id="Seg_5347" n="e" s="T737">na </ts>
               <ts e="T739" id="Seg_5349" n="e" s="T738">qaqlɨt </ts>
               <ts e="T740" id="Seg_5351" n="e" s="T739">(tolʼ </ts>
               <ts e="T741" id="Seg_5353" n="e" s="T740">kečʼit) </ts>
               <ts e="T742" id="Seg_5355" n="e" s="T741">poːqɨn </ts>
               <ts e="T743" id="Seg_5357" n="e" s="T742">na </ts>
               <ts e="T744" id="Seg_5359" n="e" s="T743">sɔːralɨmmɨntɔːtɨt. </ts>
               <ts e="T745" id="Seg_5361" n="e" s="T744">Poni </ts>
               <ts e="T746" id="Seg_5363" n="e" s="T745">ira </ts>
               <ts e="T747" id="Seg_5365" n="e" s="T746">ɛj </ts>
               <ts e="T748" id="Seg_5367" n="e" s="T747">ponä </ts>
               <ts e="T749" id="Seg_5369" n="e" s="T748">iːtɨmpatɨ, </ts>
               <ts e="T750" id="Seg_5371" n="e" s="T749">qaj </ts>
               <ts e="T751" id="Seg_5373" n="e" s="T750">muntɨk </ts>
               <ts e="T752" id="Seg_5375" n="e" s="T751">ponä </ts>
               <ts e="T753" id="Seg_5377" n="e" s="T752">iːtɨmpatɨ. </ts>
               <ts e="T754" id="Seg_5379" n="e" s="T753">İlla </ts>
               <ts e="T755" id="Seg_5381" n="e" s="T754">omta, </ts>
               <ts e="T756" id="Seg_5383" n="e" s="T755">tına </ts>
               <ts e="T757" id="Seg_5385" n="e" s="T756">malʼčʼalʼ </ts>
               <ts e="T758" id="Seg_5387" n="e" s="T757">toktɨ, </ts>
               <ts e="T759" id="Seg_5389" n="e" s="T758">ılla </ts>
               <ts e="T760" id="Seg_5391" n="e" s="T759">amɨrla </ts>
               <ts e="T761" id="Seg_5393" n="e" s="T760">((…)) </ts>
               <ts e="T762" id="Seg_5395" n="e" s="T761">ılla </ts>
               <ts e="T763" id="Seg_5397" n="e" s="T762">šäqqa. </ts>
               <ts e="T764" id="Seg_5399" n="e" s="T763">Na </ts>
               <ts e="T765" id="Seg_5401" n="e" s="T764">qumiːtɨ </ts>
               <ts e="T766" id="Seg_5403" n="e" s="T765">poːqɨn </ts>
               <ts e="T767" id="Seg_5405" n="e" s="T766">na </ts>
               <ts e="T768" id="Seg_5407" n="e" s="T767">ippälɨmmɨntɔːt </ts>
               <ts e="T769" id="Seg_5409" n="e" s="T768">nɔːtɨ </ts>
               <ts e="T770" id="Seg_5411" n="e" s="T769">sɔːrɨmpkol. </ts>
               <ts e="T771" id="Seg_5413" n="e" s="T770">Kuttar </ts>
               <ts e="T772" id="Seg_5415" n="e" s="T771">sɔːralɨmpa, </ts>
               <ts e="T773" id="Seg_5417" n="e" s="T772">nılʼčʼik. </ts>
               <ts e="T774" id="Seg_5419" n="e" s="T773">Šäqqa, </ts>
               <ts e="T775" id="Seg_5421" n="e" s="T774">toptɨlʼ </ts>
               <ts e="T776" id="Seg_5423" n="e" s="T775">qar </ts>
               <ts e="T777" id="Seg_5425" n="e" s="T776">ınna </ts>
               <ts e="T778" id="Seg_5427" n="e" s="T777">wəša. </ts>
               <ts e="T779" id="Seg_5429" n="e" s="T778">Poni </ts>
               <ts e="T780" id="Seg_5431" n="e" s="T779">ira </ts>
               <ts e="T781" id="Seg_5433" n="e" s="T780">nılʼčʼi </ts>
               <ts e="T782" id="Seg_5435" n="e" s="T781">mɨta </ts>
               <ts e="T783" id="Seg_5437" n="e" s="T782">məntɨlʼ </ts>
               <ts e="T784" id="Seg_5439" n="e" s="T783">olɨtɨ </ts>
               <ts e="T785" id="Seg_5441" n="e" s="T784">tot </ts>
               <ts e="T786" id="Seg_5443" n="e" s="T785">taŋkɨıːmpa, </ts>
               <ts e="T787" id="Seg_5445" n="e" s="T786">tına </ts>
               <ts e="T788" id="Seg_5447" n="e" s="T787">keksa </ts>
               <ts e="T789" id="Seg_5449" n="e" s="T788">saijatɨ </ts>
               <ts e="T790" id="Seg_5451" n="e" s="T789">((…)). </ts>
               <ts e="T791" id="Seg_5453" n="e" s="T790">Toːnna </ts>
               <ts e="T792" id="Seg_5455" n="e" s="T791">qälɨt </ts>
               <ts e="T793" id="Seg_5457" n="e" s="T792">((…)) </ts>
               <ts e="T794" id="Seg_5459" n="e" s="T793">iše </ts>
               <ts e="T795" id="Seg_5461" n="e" s="T794">somalɔːqɨ </ts>
               <ts e="T796" id="Seg_5463" n="e" s="T795">ɛppa. </ts>
               <ts e="T797" id="Seg_5465" n="e" s="T796">Nılʼ </ts>
               <ts e="T798" id="Seg_5467" n="e" s="T797">kətɨŋɨtɨ </ts>
               <ts e="T799" id="Seg_5469" n="e" s="T798">na </ts>
               <ts e="T800" id="Seg_5471" n="e" s="T799">ija </ts>
               <ts e="T801" id="Seg_5473" n="e" s="T800">mɨta, </ts>
               <ts e="T802" id="Seg_5475" n="e" s="T801">na </ts>
               <ts e="T803" id="Seg_5477" n="e" s="T802">qälɨtɨtkinitɨ. </ts>
               <ts e="T804" id="Seg_5479" n="e" s="T803">Qumiːmtɨ </ts>
               <ts e="T805" id="Seg_5481" n="e" s="T804">moqɨnä </ts>
               <ts e="T806" id="Seg_5483" n="e" s="T805">qəntät </ts>
               <ts e="T807" id="Seg_5485" n="e" s="T806">mɨta </ts>
               <ts e="T808" id="Seg_5487" n="e" s="T807">qaqloqantɨ </ts>
               <ts e="T809" id="Seg_5489" n="e" s="T808">tɛltalla </ts>
               <ts e="T810" id="Seg_5491" n="e" s="T809">mɨta </ts>
               <ts e="T811" id="Seg_5493" n="e" s="T810">kuralla. </ts>
               <ts e="T812" id="Seg_5495" n="e" s="T811">Ontɨt </ts>
               <ts e="T813" id="Seg_5497" n="e" s="T812">qaqloqɨntɨt. </ts>
               <ts e="T814" id="Seg_5499" n="e" s="T813">Nɨnɨ </ts>
               <ts e="T815" id="Seg_5501" n="e" s="T814">šittal </ts>
               <ts e="T816" id="Seg_5503" n="e" s="T815">toː </ts>
               <ts e="T817" id="Seg_5505" n="e" s="T816">namä </ts>
               <ts e="T818" id="Seg_5507" n="e" s="T817">pona </ts>
               <ts e="T819" id="Seg_5509" n="e" s="T818">(qə </ts>
               <ts e="T820" id="Seg_5511" n="e" s="T819">qəja) </ts>
               <ts e="T821" id="Seg_5513" n="e" s="T820">qapı </ts>
               <ts e="T822" id="Seg_5515" n="e" s="T821">mɔːt </ts>
               <ts e="T823" id="Seg_5517" n="e" s="T822">šerıːšqo </ts>
               <ts e="T824" id="Seg_5519" n="e" s="T823">aj </ts>
               <ts e="T825" id="Seg_5521" n="e" s="T824">qaj </ts>
               <ts e="T826" id="Seg_5523" n="e" s="T825">amɨrqo </ts>
               <ts e="T827" id="Seg_5525" n="e" s="T826">aj </ts>
               <ts e="T828" id="Seg_5527" n="e" s="T827">qaj </ts>
               <ts e="T829" id="Seg_5529" n="e" s="T828">ašša </ts>
               <ts e="T830" id="Seg_5531" n="e" s="T829">amɨrqo. </ts>
               <ts e="T831" id="Seg_5533" n="e" s="T830">Ninɨ </ts>
               <ts e="T832" id="Seg_5535" n="e" s="T831">na </ts>
               <ts e="T833" id="Seg_5537" n="e" s="T832">qum </ts>
               <ts e="T834" id="Seg_5539" n="e" s="T833">tıntɨna </ts>
               <ts e="T835" id="Seg_5541" n="e" s="T834">sɔːralpɨtɨlʼ </ts>
               <ts e="T836" id="Seg_5543" n="e" s="T835">qaqlɨntɨt. </ts>
               <ts e="T837" id="Seg_5545" n="e" s="T836">Tına </ts>
               <ts e="T838" id="Seg_5547" n="e" s="T837">nɨmtɨ </ts>
               <ts e="T839" id="Seg_5549" n="e" s="T838">moqonä </ts>
               <ts e="T840" id="Seg_5551" n="e" s="T839">kəːčʼälä </ts>
               <ts e="T841" id="Seg_5553" n="e" s="T840">qumiːmtɨ </ts>
               <ts e="T842" id="Seg_5555" n="e" s="T841">na </ts>
               <ts e="T843" id="Seg_5557" n="e" s="T842">qumpɨj </ts>
               <ts e="T844" id="Seg_5559" n="e" s="T843">qumiːmtɨ </ts>
               <ts e="T845" id="Seg_5561" n="e" s="T844">nık </ts>
               <ts e="T846" id="Seg_5563" n="e" s="T845">qəntɨŋɨtɨ </ts>
               <ts e="T847" id="Seg_5565" n="e" s="T846">moqonä. </ts>
               <ts e="T848" id="Seg_5567" n="e" s="T847">Takkɨ </ts>
               <ts e="T849" id="Seg_5569" n="e" s="T848">moqonä. </ts>
               <ts e="T850" id="Seg_5571" n="e" s="T849">Poni </ts>
               <ts e="T851" id="Seg_5573" n="e" s="T850">iram </ts>
               <ts e="T852" id="Seg_5575" n="e" s="T851">tɛ </ts>
               <ts e="T853" id="Seg_5577" n="e" s="T852">kəːčʼɨlä </ts>
               <ts e="T854" id="Seg_5579" n="e" s="T853">qəntɨŋɨtɨ. </ts>
               <ts e="T855" id="Seg_5581" n="e" s="T854">Qapı </ts>
               <ts e="T856" id="Seg_5583" n="e" s="T855">kɨpɨka </ts>
               <ts e="T857" id="Seg_5585" n="e" s="T856">kəitɨ </ts>
               <ts e="T858" id="Seg_5587" n="e" s="T857">ola. </ts>
               <ts e="T859" id="Seg_5589" n="e" s="T858">Təmol </ts>
               <ts e="T860" id="Seg_5591" n="e" s="T859">qənnɔːtɨt </ts>
               <ts e="T861" id="Seg_5593" n="e" s="T860">tına. </ts>
               <ts e="T862" id="Seg_5595" n="e" s="T861">Nɨnɨ </ts>
               <ts e="T863" id="Seg_5597" n="e" s="T862">somak </ts>
               <ts e="T864" id="Seg_5599" n="e" s="T863">qənnɔːtɨn </ts>
               <ts e="T865" id="Seg_5601" n="e" s="T864">nɨmtɨ. </ts>
               <ts e="T866" id="Seg_5603" n="e" s="T865">Imantɨn </ts>
               <ts e="T867" id="Seg_5605" n="e" s="T866">nık </ts>
               <ts e="T868" id="Seg_5607" n="e" s="T867">kətɨŋɨt:“ </ts>
               <ts e="T869" id="Seg_5609" n="e" s="T868">Mɔːllɨ </ts>
               <ts e="T870" id="Seg_5611" n="e" s="T869">toː </ts>
               <ts e="T871" id="Seg_5613" n="e" s="T870">tılät </ts>
               <ts e="T872" id="Seg_5615" n="e" s="T871">me </ts>
               <ts e="T873" id="Seg_5617" n="e" s="T872">nʼenna </ts>
               <ts e="T874" id="Seg_5619" n="e" s="T873">qəntɨsɔːmıː </ts>
               <ts e="T875" id="Seg_5621" n="e" s="T874">qumɨtɨtkin”. </ts>
               <ts e="T876" id="Seg_5623" n="e" s="T875">Aš </ts>
               <ts e="T877" id="Seg_5625" n="e" s="T876">tɛnɨmɨmpɔːt </ts>
               <ts e="T878" id="Seg_5627" n="e" s="T877">qapı </ts>
               <ts e="T879" id="Seg_5629" n="e" s="T878">ontɨn </ts>
               <ts e="T880" id="Seg_5631" n="e" s="T879">na </ts>
               <ts e="T881" id="Seg_5633" n="e" s="T880">ilʼčʼantɨlʼ </ts>
               <ts e="T882" id="Seg_5635" n="e" s="T881">mɨt </ts>
               <ts e="T883" id="Seg_5637" n="e" s="T882">qos </ts>
               <ts e="T884" id="Seg_5639" n="e" s="T883">qaj. </ts>
               <ts e="T885" id="Seg_5641" n="e" s="T884">Nʼenna </ts>
               <ts e="T886" id="Seg_5643" n="e" s="T885">qənna </ts>
               <ts e="T887" id="Seg_5645" n="e" s="T886">tıntena </ts>
               <ts e="T888" id="Seg_5647" n="e" s="T887">ontɨ </ts>
               <ts e="T889" id="Seg_5649" n="e" s="T888">ašʼa </ts>
               <ts e="T890" id="Seg_5651" n="e" s="T889">koŋɨmpa </ts>
               <ts e="T891" id="Seg_5653" n="e" s="T890">((…)). </ts>
               <ts e="T892" id="Seg_5655" n="e" s="T891">Toːna </ts>
               <ts e="T893" id="Seg_5657" n="e" s="T892">imatɨ </ts>
               <ts e="T894" id="Seg_5659" n="e" s="T893">na </ts>
               <ts e="T895" id="Seg_5661" n="e" s="T894">kətɛntɨtɨ </ts>
               <ts e="T896" id="Seg_5663" n="e" s="T895">äsantɨ </ts>
               <ts e="T897" id="Seg_5665" n="e" s="T896">ilʼčʼantɨ </ts>
               <ts e="T898" id="Seg_5667" n="e" s="T897">mɨqätkiːn. </ts>
               <ts e="T899" id="Seg_5669" n="e" s="T898">Poni </ts>
               <ts e="T900" id="Seg_5671" n="e" s="T899">iralʼ </ts>
               <ts e="T901" id="Seg_5673" n="e" s="T900">mɨt </ts>
               <ts e="T902" id="Seg_5675" n="e" s="T901">tına </ts>
               <ts e="T903" id="Seg_5677" n="e" s="T902">moqonä </ts>
               <ts e="T904" id="Seg_5679" n="e" s="T903">qonisɔːtɨt </ts>
               <ts e="T905" id="Seg_5681" n="e" s="T904">mompa. </ts>
               <ts e="T906" id="Seg_5683" n="e" s="T905">Tal. </ts>
               <ts e="T907" id="Seg_5685" n="e" s="T906">Nɨnɨ </ts>
               <ts e="T908" id="Seg_5687" n="e" s="T907">šʼittalʼ </ts>
               <ts e="T909" id="Seg_5689" n="e" s="T908">pija </ts>
               <ts e="T910" id="Seg_5691" n="e" s="T909">qapija </ts>
               <ts e="T911" id="Seg_5693" n="e" s="T910">((…)). </ts>
               <ts e="T912" id="Seg_5695" n="e" s="T911">Aš </ts>
               <ts e="T985" id="Seg_5697" n="e" s="T912">tɛnɨmɔːtɨt. </ts>
               <ts e="T913" id="Seg_5699" n="e" s="T985">((NN:)) </ts>
               <ts e="T914" id="Seg_5701" n="e" s="T913">Karrä </ts>
               <ts e="T986" id="Seg_5703" n="e" s="T914">koŋɨmpäš. </ts>
               <ts e="T915" id="Seg_5705" n="e" s="T986">((NN:)) </ts>
               <ts e="T916" id="Seg_5707" n="e" s="T915">Karrä </ts>
               <ts e="T987" id="Seg_5709" n="e" s="T916">radioqantɨ. </ts>
               <ts e="T917" id="Seg_5711" n="e" s="T987">((NEP:)) </ts>
               <ts e="T918" id="Seg_5713" n="e" s="T917">Takkɨ </ts>
               <ts e="T919" id="Seg_5715" n="e" s="T918">nʼenna </ts>
               <ts e="T920" id="Seg_5717" n="e" s="T919">ɨpalnoj </ts>
               <ts e="T921" id="Seg_5719" n="e" s="T920">təpɨjat </ts>
               <ts e="T922" id="Seg_5721" n="e" s="T921">näčʼčʼa </ts>
               <ts e="T923" id="Seg_5723" n="e" s="T922">ilʼčʼantɨ </ts>
               <ts e="T924" id="Seg_5725" n="e" s="T923">mɔːt </ts>
               <ts e="T925" id="Seg_5727" n="e" s="T924">mɔːt </ts>
               <ts e="T926" id="Seg_5729" n="e" s="T925">šerna, </ts>
               <ts e="T927" id="Seg_5731" n="e" s="T926">qaqlɨt </ts>
               <ts e="T928" id="Seg_5733" n="e" s="T927">to </ts>
               <ts e="T929" id="Seg_5735" n="e" s="T928">nıːmɨt </ts>
               <ts e="T930" id="Seg_5737" n="e" s="T929">kurɨla </ts>
               <ts e="T931" id="Seg_5739" n="e" s="T930">nık </ts>
               <ts e="T932" id="Seg_5741" n="e" s="T931">tottɨŋɨtɨ. </ts>
               <ts e="T933" id="Seg_5743" n="e" s="T932">Toːnna </ts>
               <ts e="T934" id="Seg_5745" n="e" s="T933">qənmɨntɔːtɨn, </ts>
               <ts e="T935" id="Seg_5747" n="e" s="T934">moqonä </ts>
               <ts e="T936" id="Seg_5749" n="e" s="T935">qənmɨntɔːtɨt. </ts>
               <ts e="T937" id="Seg_5751" n="e" s="T936">Poni </ts>
               <ts e="T938" id="Seg_5753" n="e" s="T937">ira </ts>
               <ts e="T939" id="Seg_5755" n="e" s="T938">kekɨsa </ts>
               <ts e="T940" id="Seg_5757" n="e" s="T939">takkɨn </ts>
               <ts e="T941" id="Seg_5759" n="e" s="T940">mɔːtqɨn </ts>
               <ts e="T942" id="Seg_5761" n="e" s="T941">tulɨšpa, </ts>
               <ts e="T943" id="Seg_5763" n="e" s="T942">ılla </ts>
               <ts e="T944" id="Seg_5765" n="e" s="T943">qumpa. </ts>
               <ts e="T945" id="Seg_5767" n="e" s="T944">Toːnna </ts>
               <ts e="T946" id="Seg_5769" n="e" s="T945">šittɨ </ts>
               <ts e="T947" id="Seg_5771" n="e" s="T946">qumoːqıː </ts>
               <ts e="T948" id="Seg_5773" n="e" s="T947">tıː </ts>
               <ts e="T949" id="Seg_5775" n="e" s="T948">ilɨmpɔːqı. </ts>
               <ts e="T950" id="Seg_5777" n="e" s="T949">Poni </ts>
               <ts e="T951" id="Seg_5779" n="e" s="T950">ira, </ts>
               <ts e="T952" id="Seg_5781" n="e" s="T951">Poni </ts>
               <ts e="T953" id="Seg_5783" n="e" s="T952">iralʼ </ts>
               <ts e="T954" id="Seg_5785" n="e" s="T953">mɨt </ts>
               <ts e="T955" id="Seg_5787" n="e" s="T954">nɨmtɨ </ts>
               <ts e="T988" id="Seg_5789" n="e" s="T955">ɛːŋa. </ts>
               <ts e="T956" id="Seg_5791" n="e" s="T988">((NN:)) </ts>
               <ts e="T957" id="Seg_5793" n="e" s="T956">Jarɨ </ts>
               <ts e="T958" id="Seg_5795" n="e" s="T957">mɨ </ts>
               <ts e="T959" id="Seg_5797" n="e" s="T958">kətät, </ts>
               <ts e="T960" id="Seg_5799" n="e" s="T959">jarɨk, </ts>
               <ts e="T989" id="Seg_5801" n="e" s="T960">jarɨk. </ts>
               <ts e="T961" id="Seg_5803" n="e" s="T989">((NN:)) </ts>
               <ts e="T962" id="Seg_5805" n="e" s="T961">Jarɨ </ts>
               <ts e="T963" id="Seg_5807" n="e" s="T962">mɨ </ts>
               <ts e="T964" id="Seg_5809" n="e" s="T963">kətät. </ts>
               <ts e="T965" id="Seg_5811" n="e" s="T964">((NN:)) Тут рассказанное всё и есть. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T968" id="Seg_5812" s="T0">NEP_196X_OrphanBoyAndPanOldMan2_flk.001 (001)</ta>
            <ta e="T6" id="Seg_5813" s="T968">NEP_196X_OrphanBoyAndPanOldMan2_flk.002 (002)</ta>
            <ta e="T13" id="Seg_5814" s="T6">NEP_196X_OrphanBoyAndPanOldMan2_flk.003 (003)</ta>
            <ta e="T19" id="Seg_5815" s="T13">NEP_196X_OrphanBoyAndPanOldMan2_flk.004 (004)</ta>
            <ta e="T969" id="Seg_5816" s="T19">NEP_196X_OrphanBoyAndPanOldMan2_flk.005 (005)</ta>
            <ta e="T970" id="Seg_5817" s="T969">NEP_196X_OrphanBoyAndPanOldMan2_flk.006 (006)</ta>
            <ta e="T971" id="Seg_5818" s="T970">NEP_196X_OrphanBoyAndPanOldMan2_flk.007 (007)</ta>
            <ta e="T972" id="Seg_5819" s="T971">NEP_196X_OrphanBoyAndPanOldMan2_flk.008 (008)</ta>
            <ta e="T973" id="Seg_5820" s="T972">NEP_196X_OrphanBoyAndPanOldMan2_flk.009 (009)</ta>
            <ta e="T41" id="Seg_5821" s="T973">NEP_196X_OrphanBoyAndPanOldMan2_flk.010 (010)</ta>
            <ta e="T42" id="Seg_5822" s="T41">NEP_196X_OrphanBoyAndPanOldMan2_flk.011 (011)</ta>
            <ta e="T45" id="Seg_5823" s="T42">NEP_196X_OrphanBoyAndPanOldMan2_flk.012 (012)</ta>
            <ta e="T53" id="Seg_5824" s="T45">NEP_196X_OrphanBoyAndPanOldMan2_flk.013 (013)</ta>
            <ta e="T992" id="Seg_5825" s="T53">NEP_196X_OrphanBoyAndPanOldMan2_flk.014 (014)</ta>
            <ta e="T59" id="Seg_5826" s="T992">NEP_196X_OrphanBoyAndPanOldMan2_flk.015 (014)</ta>
            <ta e="T61" id="Seg_5827" s="T59">NEP_196X_OrphanBoyAndPanOldMan2_flk.016 (015)</ta>
            <ta e="T62" id="Seg_5828" s="T61">NEP_196X_OrphanBoyAndPanOldMan2_flk.017 (016)</ta>
            <ta e="T68" id="Seg_5829" s="T62">NEP_196X_OrphanBoyAndPanOldMan2_flk.018 (017)</ta>
            <ta e="T74" id="Seg_5830" s="T68">NEP_196X_OrphanBoyAndPanOldMan2_flk.019 (018)</ta>
            <ta e="T83" id="Seg_5831" s="T74">NEP_196X_OrphanBoyAndPanOldMan2_flk.020 (019)</ta>
            <ta e="T90" id="Seg_5832" s="T83">NEP_196X_OrphanBoyAndPanOldMan2_flk.021 (020)</ta>
            <ta e="T96" id="Seg_5833" s="T90">NEP_196X_OrphanBoyAndPanOldMan2_flk.022 (021)</ta>
            <ta e="T101" id="Seg_5834" s="T96">NEP_196X_OrphanBoyAndPanOldMan2_flk.023 (022)</ta>
            <ta e="T110" id="Seg_5835" s="T101">NEP_196X_OrphanBoyAndPanOldMan2_flk.024 (023)</ta>
            <ta e="T122" id="Seg_5836" s="T110">NEP_196X_OrphanBoyAndPanOldMan2_flk.025 (024)</ta>
            <ta e="T125" id="Seg_5837" s="T122">NEP_196X_OrphanBoyAndPanOldMan2_flk.026 (025)</ta>
            <ta e="T127" id="Seg_5838" s="T125">NEP_196X_OrphanBoyAndPanOldMan2_flk.027 (026)</ta>
            <ta e="T129" id="Seg_5839" s="T127">NEP_196X_OrphanBoyAndPanOldMan2_flk.028 (027)</ta>
            <ta e="T138" id="Seg_5840" s="T129">NEP_196X_OrphanBoyAndPanOldMan2_flk.029 (028)</ta>
            <ta e="T145" id="Seg_5841" s="T138">NEP_196X_OrphanBoyAndPanOldMan2_flk.030 (029)</ta>
            <ta e="T147" id="Seg_5842" s="T145">NEP_196X_OrphanBoyAndPanOldMan2_flk.031 (030)</ta>
            <ta e="T157" id="Seg_5843" s="T147">NEP_196X_OrphanBoyAndPanOldMan2_flk.032 (031)</ta>
            <ta e="T170" id="Seg_5844" s="T157">NEP_196X_OrphanBoyAndPanOldMan2_flk.033 (032)</ta>
            <ta e="T173" id="Seg_5845" s="T170">NEP_196X_OrphanBoyAndPanOldMan2_flk.034 (033)</ta>
            <ta e="T177" id="Seg_5846" s="T173">NEP_196X_OrphanBoyAndPanOldMan2_flk.035 (034)</ta>
            <ta e="T182" id="Seg_5847" s="T177">NEP_196X_OrphanBoyAndPanOldMan2_flk.036 (035)</ta>
            <ta e="T188" id="Seg_5848" s="T182">NEP_196X_OrphanBoyAndPanOldMan2_flk.037 (036)</ta>
            <ta e="T199" id="Seg_5849" s="T188">NEP_196X_OrphanBoyAndPanOldMan2_flk.038 (037)</ta>
            <ta e="T201" id="Seg_5850" s="T199">NEP_196X_OrphanBoyAndPanOldMan2_flk.039 (038)</ta>
            <ta e="T208" id="Seg_5851" s="T201">NEP_196X_OrphanBoyAndPanOldMan2_flk.040 (039)</ta>
            <ta e="T212" id="Seg_5852" s="T208">NEP_196X_OrphanBoyAndPanOldMan2_flk.041 (040)</ta>
            <ta e="T215" id="Seg_5853" s="T212">NEP_196X_OrphanBoyAndPanOldMan2_flk.042 (041)</ta>
            <ta e="T226" id="Seg_5854" s="T215">NEP_196X_OrphanBoyAndPanOldMan2_flk.043 (042)</ta>
            <ta e="T227" id="Seg_5855" s="T226">NEP_196X_OrphanBoyAndPanOldMan2_flk.044 (043)</ta>
            <ta e="T238" id="Seg_5856" s="T227">NEP_196X_OrphanBoyAndPanOldMan2_flk.045 (044)</ta>
            <ta e="T241" id="Seg_5857" s="T238">NEP_196X_OrphanBoyAndPanOldMan2_flk.046 (045)</ta>
            <ta e="T245" id="Seg_5858" s="T241">NEP_196X_OrphanBoyAndPanOldMan2_flk.047 (046)</ta>
            <ta e="T251" id="Seg_5859" s="T245">NEP_196X_OrphanBoyAndPanOldMan2_flk.048 (047)</ta>
            <ta e="T253" id="Seg_5860" s="T251">NEP_196X_OrphanBoyAndPanOldMan2_flk.049 (048)</ta>
            <ta e="T258" id="Seg_5861" s="T253">NEP_196X_OrphanBoyAndPanOldMan2_flk.050 (049)</ta>
            <ta e="T266" id="Seg_5862" s="T258">NEP_196X_OrphanBoyAndPanOldMan2_flk.051 (050)</ta>
            <ta e="T274" id="Seg_5863" s="T266">NEP_196X_OrphanBoyAndPanOldMan2_flk.052 (051)</ta>
            <ta e="T277" id="Seg_5864" s="T274">NEP_196X_OrphanBoyAndPanOldMan2_flk.053 (052)</ta>
            <ta e="T279" id="Seg_5865" s="T277">NEP_196X_OrphanBoyAndPanOldMan2_flk.054 (053)</ta>
            <ta e="T282" id="Seg_5866" s="T279">NEP_196X_OrphanBoyAndPanOldMan2_flk.055 (054)</ta>
            <ta e="T289" id="Seg_5867" s="T282">NEP_196X_OrphanBoyAndPanOldMan2_flk.056 (055)</ta>
            <ta e="T296" id="Seg_5868" s="T289">NEP_196X_OrphanBoyAndPanOldMan2_flk.057 (056)</ta>
            <ta e="T300" id="Seg_5869" s="T296">NEP_196X_OrphanBoyAndPanOldMan2_flk.058 (057)</ta>
            <ta e="T308" id="Seg_5870" s="T300">NEP_196X_OrphanBoyAndPanOldMan2_flk.059 (058)</ta>
            <ta e="T319" id="Seg_5871" s="T308">NEP_196X_OrphanBoyAndPanOldMan2_flk.060 (059)</ta>
            <ta e="T328" id="Seg_5872" s="T319">NEP_196X_OrphanBoyAndPanOldMan2_flk.061 (060)</ta>
            <ta e="T331" id="Seg_5873" s="T328">NEP_196X_OrphanBoyAndPanOldMan2_flk.062 (061)</ta>
            <ta e="T336" id="Seg_5874" s="T331">NEP_196X_OrphanBoyAndPanOldMan2_flk.063 (062)</ta>
            <ta e="T341" id="Seg_5875" s="T336">NEP_196X_OrphanBoyAndPanOldMan2_flk.064 (063)</ta>
            <ta e="T346" id="Seg_5876" s="T341">NEP_196X_OrphanBoyAndPanOldMan2_flk.065 (064)</ta>
            <ta e="T356" id="Seg_5877" s="T346">NEP_196X_OrphanBoyAndPanOldMan2_flk.066 (065)</ta>
            <ta e="T359" id="Seg_5878" s="T356">NEP_196X_OrphanBoyAndPanOldMan2_flk.067 (066)</ta>
            <ta e="T365" id="Seg_5879" s="T359">NEP_196X_OrphanBoyAndPanOldMan2_flk.068 (067)</ta>
            <ta e="T376" id="Seg_5880" s="T365">NEP_196X_OrphanBoyAndPanOldMan2_flk.069 (068)</ta>
            <ta e="T384" id="Seg_5881" s="T376">NEP_196X_OrphanBoyAndPanOldMan2_flk.070 (069)</ta>
            <ta e="T391" id="Seg_5882" s="T384">NEP_196X_OrphanBoyAndPanOldMan2_flk.071 (070)</ta>
            <ta e="T395" id="Seg_5883" s="T391">NEP_196X_OrphanBoyAndPanOldMan2_flk.072 (071)</ta>
            <ta e="T400" id="Seg_5884" s="T395">NEP_196X_OrphanBoyAndPanOldMan2_flk.073 (072)</ta>
            <ta e="T406" id="Seg_5885" s="T400">NEP_196X_OrphanBoyAndPanOldMan2_flk.074 (073)</ta>
            <ta e="T412" id="Seg_5886" s="T406">NEP_196X_OrphanBoyAndPanOldMan2_flk.075 (074)</ta>
            <ta e="T415" id="Seg_5887" s="T412">NEP_196X_OrphanBoyAndPanOldMan2_flk.076 (075)</ta>
            <ta e="T425" id="Seg_5888" s="T415">NEP_196X_OrphanBoyAndPanOldMan2_flk.077 (076)</ta>
            <ta e="T439" id="Seg_5889" s="T425">NEP_196X_OrphanBoyAndPanOldMan2_flk.078 (077)</ta>
            <ta e="T443" id="Seg_5890" s="T439">NEP_196X_OrphanBoyAndPanOldMan2_flk.079 (078)</ta>
            <ta e="T444" id="Seg_5891" s="T443">NEP_196X_OrphanBoyAndPanOldMan2_flk.080 (079)</ta>
            <ta e="T445" id="Seg_5892" s="T444">NEP_196X_OrphanBoyAndPanOldMan2_flk.081 (080)</ta>
            <ta e="T447" id="Seg_5893" s="T445">NEP_196X_OrphanBoyAndPanOldMan2_flk.082 (081)</ta>
            <ta e="T457" id="Seg_5894" s="T447">NEP_196X_OrphanBoyAndPanOldMan2_flk.083 (082)</ta>
            <ta e="T467" id="Seg_5895" s="T457">NEP_196X_OrphanBoyAndPanOldMan2_flk.084 (083)</ta>
            <ta e="T478" id="Seg_5896" s="T467">NEP_196X_OrphanBoyAndPanOldMan2_flk.085 (084)</ta>
            <ta e="T481" id="Seg_5897" s="T478">NEP_196X_OrphanBoyAndPanOldMan2_flk.086 (085)</ta>
            <ta e="T490" id="Seg_5898" s="T481">NEP_196X_OrphanBoyAndPanOldMan2_flk.087 (086)</ta>
            <ta e="T495" id="Seg_5899" s="T490">NEP_196X_OrphanBoyAndPanOldMan2_flk.088 (087)</ta>
            <ta e="T501" id="Seg_5900" s="T495">NEP_196X_OrphanBoyAndPanOldMan2_flk.089 (088)</ta>
            <ta e="T506" id="Seg_5901" s="T501">NEP_196X_OrphanBoyAndPanOldMan2_flk.090 (089)</ta>
            <ta e="T975" id="Seg_5902" s="T506">NEP_196X_OrphanBoyAndPanOldMan2_flk.091 (090)</ta>
            <ta e="T976" id="Seg_5903" s="T975">NEP_196X_OrphanBoyAndPanOldMan2_flk.092 (091)</ta>
            <ta e="T990" id="Seg_5904" s="T976">NEP_196X_OrphanBoyAndPanOldMan2_flk.093 (092)</ta>
            <ta e="T977" id="Seg_5905" s="T990">NEP_196X_OrphanBoyAndPanOldMan2_flk.094 (092)</ta>
            <ta e="T514" id="Seg_5906" s="T977">NEP_196X_OrphanBoyAndPanOldMan2_flk.095 (093)</ta>
            <ta e="T515" id="Seg_5907" s="T514">NEP_196X_OrphanBoyAndPanOldMan2_flk.096 (094)</ta>
            <ta e="T978" id="Seg_5908" s="T515">NEP_196X_OrphanBoyAndPanOldMan2_flk.097 (095)</ta>
            <ta e="T979" id="Seg_5909" s="T978">NEP_196X_OrphanBoyAndPanOldMan2_flk.098 (096)</ta>
            <ta e="T980" id="Seg_5910" s="T979">NEP_196X_OrphanBoyAndPanOldMan2_flk.099 (097)</ta>
            <ta e="T981" id="Seg_5911" s="T980">NEP_196X_OrphanBoyAndPanOldMan2_flk.100 (098)</ta>
            <ta e="T982" id="Seg_5912" s="T981">NEP_196X_OrphanBoyAndPanOldMan2_flk.101 (099)</ta>
            <ta e="T983" id="Seg_5913" s="T982">NEP_196X_OrphanBoyAndPanOldMan2_flk.102 (100)</ta>
            <ta e="T974" id="Seg_5914" s="T983">NEP_196X_OrphanBoyAndPanOldMan2_flk.103 (101)</ta>
            <ta e="T542" id="Seg_5915" s="T974">NEP_196X_OrphanBoyAndPanOldMan2_flk.104 (102)</ta>
            <ta e="T549" id="Seg_5916" s="T542">NEP_196X_OrphanBoyAndPanOldMan2_flk.105 (103)</ta>
            <ta e="T556" id="Seg_5917" s="T549">NEP_196X_OrphanBoyAndPanOldMan2_flk.106 (104)</ta>
            <ta e="T559" id="Seg_5918" s="T556">NEP_196X_OrphanBoyAndPanOldMan2_flk.107 (105)</ta>
            <ta e="T564" id="Seg_5919" s="T559">NEP_196X_OrphanBoyAndPanOldMan2_flk.108 (106)</ta>
            <ta e="T569" id="Seg_5920" s="T564">NEP_196X_OrphanBoyAndPanOldMan2_flk.109 (107)</ta>
            <ta e="T576" id="Seg_5921" s="T569">NEP_196X_OrphanBoyAndPanOldMan2_flk.110 (108)</ta>
            <ta e="T578" id="Seg_5922" s="T576">NEP_196X_OrphanBoyAndPanOldMan2_flk.111 (109)</ta>
            <ta e="T581" id="Seg_5923" s="T578">NEP_196X_OrphanBoyAndPanOldMan2_flk.112 (110)</ta>
            <ta e="T588" id="Seg_5924" s="T581">NEP_196X_OrphanBoyAndPanOldMan2_flk.113 (111)</ta>
            <ta e="T595" id="Seg_5925" s="T588">NEP_196X_OrphanBoyAndPanOldMan2_flk.114 (112)</ta>
            <ta e="T604" id="Seg_5926" s="T595">NEP_196X_OrphanBoyAndPanOldMan2_flk.115 (113)</ta>
            <ta e="T608" id="Seg_5927" s="T604">NEP_196X_OrphanBoyAndPanOldMan2_flk.116 (114)</ta>
            <ta e="T613" id="Seg_5928" s="T608">NEP_196X_OrphanBoyAndPanOldMan2_flk.117 (115)</ta>
            <ta e="T621" id="Seg_5929" s="T613">NEP_196X_OrphanBoyAndPanOldMan2_flk.118 (116)</ta>
            <ta e="T630" id="Seg_5930" s="T621">NEP_196X_OrphanBoyAndPanOldMan2_flk.119 (117)</ta>
            <ta e="T636" id="Seg_5931" s="T630">NEP_196X_OrphanBoyAndPanOldMan2_flk.120 (118)</ta>
            <ta e="T984" id="Seg_5932" s="T636">NEP_196X_OrphanBoyAndPanOldMan2_flk.121 (119)</ta>
            <ta e="T641" id="Seg_5933" s="T984">NEP_196X_OrphanBoyAndPanOldMan2_flk.122 (120)</ta>
            <ta e="T651" id="Seg_5934" s="T641">NEP_196X_OrphanBoyAndPanOldMan2_flk.123 (121)</ta>
            <ta e="T655" id="Seg_5935" s="T651">NEP_196X_OrphanBoyAndPanOldMan2_flk.124 (122)</ta>
            <ta e="T662" id="Seg_5936" s="T655">NEP_196X_OrphanBoyAndPanOldMan2_flk.125 (123)</ta>
            <ta e="T668" id="Seg_5937" s="T662">NEP_196X_OrphanBoyAndPanOldMan2_flk.126 (124)</ta>
            <ta e="T671" id="Seg_5938" s="T668">NEP_196X_OrphanBoyAndPanOldMan2_flk.127 (125)</ta>
            <ta e="T675" id="Seg_5939" s="T671">NEP_196X_OrphanBoyAndPanOldMan2_flk.128 (126)</ta>
            <ta e="T682" id="Seg_5940" s="T675">NEP_196X_OrphanBoyAndPanOldMan2_flk.129 (127)</ta>
            <ta e="T688" id="Seg_5941" s="T682">NEP_196X_OrphanBoyAndPanOldMan2_flk.130 (128)</ta>
            <ta e="T689" id="Seg_5942" s="T688">NEP_196X_OrphanBoyAndPanOldMan2_flk.131 (129)</ta>
            <ta e="T694" id="Seg_5943" s="T689">NEP_196X_OrphanBoyAndPanOldMan2_flk.132 (130)</ta>
            <ta e="T698" id="Seg_5944" s="T694">NEP_196X_OrphanBoyAndPanOldMan2_flk.133 (131)</ta>
            <ta e="T704" id="Seg_5945" s="T698">NEP_196X_OrphanBoyAndPanOldMan2_flk.134 (132)</ta>
            <ta e="T706" id="Seg_5946" s="T704">NEP_196X_OrphanBoyAndPanOldMan2_flk.135 (133)</ta>
            <ta e="T718" id="Seg_5947" s="T706">NEP_196X_OrphanBoyAndPanOldMan2_flk.136 (134)</ta>
            <ta e="T722" id="Seg_5948" s="T718">NEP_196X_OrphanBoyAndPanOldMan2_flk.137 (135)</ta>
            <ta e="T730" id="Seg_5949" s="T722">NEP_196X_OrphanBoyAndPanOldMan2_flk.138 (136)</ta>
            <ta e="T731" id="Seg_5950" s="T730">NEP_196X_OrphanBoyAndPanOldMan2_flk.139 (137)</ta>
            <ta e="T744" id="Seg_5951" s="T731">NEP_196X_OrphanBoyAndPanOldMan2_flk.140 (138)</ta>
            <ta e="T753" id="Seg_5952" s="T744">NEP_196X_OrphanBoyAndPanOldMan2_flk.141 (139)</ta>
            <ta e="T763" id="Seg_5953" s="T753">NEP_196X_OrphanBoyAndPanOldMan2_flk.142 (140)</ta>
            <ta e="T770" id="Seg_5954" s="T763">NEP_196X_OrphanBoyAndPanOldMan2_flk.143 (141)</ta>
            <ta e="T773" id="Seg_5955" s="T770">NEP_196X_OrphanBoyAndPanOldMan2_flk.144 (142)</ta>
            <ta e="T778" id="Seg_5956" s="T773">NEP_196X_OrphanBoyAndPanOldMan2_flk.145 (143)</ta>
            <ta e="T790" id="Seg_5957" s="T778">NEP_196X_OrphanBoyAndPanOldMan2_flk.146 (144)</ta>
            <ta e="T796" id="Seg_5958" s="T790">NEP_196X_OrphanBoyAndPanOldMan2_flk.147 (145)</ta>
            <ta e="T803" id="Seg_5959" s="T796">NEP_196X_OrphanBoyAndPanOldMan2_flk.148 (146)</ta>
            <ta e="T811" id="Seg_5960" s="T803">NEP_196X_OrphanBoyAndPanOldMan2_flk.149 (147)</ta>
            <ta e="T813" id="Seg_5961" s="T811">NEP_196X_OrphanBoyAndPanOldMan2_flk.150 (148)</ta>
            <ta e="T830" id="Seg_5962" s="T813">NEP_196X_OrphanBoyAndPanOldMan2_flk.151 (149)</ta>
            <ta e="T836" id="Seg_5963" s="T830">NEP_196X_OrphanBoyAndPanOldMan2_flk.152 (150)</ta>
            <ta e="T847" id="Seg_5964" s="T836">NEP_196X_OrphanBoyAndPanOldMan2_flk.153 (151)</ta>
            <ta e="T849" id="Seg_5965" s="T847">NEP_196X_OrphanBoyAndPanOldMan2_flk.154 (152)</ta>
            <ta e="T854" id="Seg_5966" s="T849">NEP_196X_OrphanBoyAndPanOldMan2_flk.155 (153)</ta>
            <ta e="T858" id="Seg_5967" s="T854">NEP_196X_OrphanBoyAndPanOldMan2_flk.156 (154)</ta>
            <ta e="T861" id="Seg_5968" s="T858">NEP_196X_OrphanBoyAndPanOldMan2_flk.157 (155)</ta>
            <ta e="T865" id="Seg_5969" s="T861">NEP_196X_OrphanBoyAndPanOldMan2_flk.158 (156)</ta>
            <ta e="T875" id="Seg_5970" s="T865">NEP_196X_OrphanBoyAndPanOldMan2_flk.159 (157)</ta>
            <ta e="T884" id="Seg_5971" s="T875">NEP_196X_OrphanBoyAndPanOldMan2_flk.160 (158)</ta>
            <ta e="T891" id="Seg_5972" s="T884">NEP_196X_OrphanBoyAndPanOldMan2_flk.161 (159)</ta>
            <ta e="T898" id="Seg_5973" s="T891">NEP_196X_OrphanBoyAndPanOldMan2_flk.162 (160)</ta>
            <ta e="T905" id="Seg_5974" s="T898">NEP_196X_OrphanBoyAndPanOldMan2_flk.163 (161)</ta>
            <ta e="T906" id="Seg_5975" s="T905">NEP_196X_OrphanBoyAndPanOldMan2_flk.164 (162)</ta>
            <ta e="T911" id="Seg_5976" s="T906">NEP_196X_OrphanBoyAndPanOldMan2_flk.165 (163)</ta>
            <ta e="T985" id="Seg_5977" s="T911">NEP_196X_OrphanBoyAndPanOldMan2_flk.166 (164)</ta>
            <ta e="T986" id="Seg_5978" s="T985">NEP_196X_OrphanBoyAndPanOldMan2_flk.167 (165)</ta>
            <ta e="T987" id="Seg_5979" s="T986">NEP_196X_OrphanBoyAndPanOldMan2_flk.168 (166)</ta>
            <ta e="T932" id="Seg_5980" s="T987">NEP_196X_OrphanBoyAndPanOldMan2_flk.169 (167)</ta>
            <ta e="T936" id="Seg_5981" s="T932">NEP_196X_OrphanBoyAndPanOldMan2_flk.170 (168)</ta>
            <ta e="T944" id="Seg_5982" s="T936">NEP_196X_OrphanBoyAndPanOldMan2_flk.171 (169)</ta>
            <ta e="T949" id="Seg_5983" s="T944">NEP_196X_OrphanBoyAndPanOldMan2_flk.172 (170)</ta>
            <ta e="T988" id="Seg_5984" s="T949">NEP_196X_OrphanBoyAndPanOldMan2_flk.173 (171)</ta>
            <ta e="T989" id="Seg_5985" s="T988">NEP_196X_OrphanBoyAndPanOldMan2_flk.174 (172)</ta>
            <ta e="T964" id="Seg_5986" s="T989">NEP_196X_OrphanBoyAndPanOldMan2_flk.175 (173)</ta>
            <ta e="T965" id="Seg_5987" s="T964">NEP_196X_OrphanBoyAndPanOldMan2_flk.176 (174)</ta>
         </annotation>
         <annotation name="st" tierref="st" />
         <annotation name="stl" tierref="stl">
            <ta e="T968" id="Seg_5988" s="T0">((KuAI:)) Лентяю всегда праздник.</ta>
            <ta e="T6" id="Seg_5989" s="T968">(…) Ukkɨr ijatɨ ɛppantɨ.</ta>
            <ta e="T13" id="Seg_5990" s="T6">Na qos ket qət kətɨkak namɨp.</ta>
            <ta e="T19" id="Seg_5991" s="T13">Na tentɨlʼ, na tentɨlʼ, ašša čʼaptä.</ta>
            <ta e="T969" id="Seg_5992" s="T19">((KuAI:)) Подождите, пусть она подождёт, я попробую, посмотрю, проверю.</ta>
            <ta e="T970" id="Seg_5993" s="T969">((NN:)) Tan ılqɨt… šʼolqumɨt nečʼa…</ta>
            <ta e="T971" id="Seg_5994" s="T970">((NN:)) Tam na šʼintɨ qolʼčʼanta tan etal utrejqɨjojčʼimpʼat.</ta>
            <ta e="T972" id="Seg_5995" s="T971">((NN:)) Tuttaltät.</ta>
            <ta e="T973" id="Seg_5996" s="T972">((NN:)) Čʼontɨkak kətät.</ta>
            <ta e="T41" id="Seg_5997" s="T973">((NEP:)) Pija na ira ilɨmpan.</ta>
            <ta e="T42" id="Seg_5998" s="T41">Qütalpa.</ta>
            <ta e="T45" id="Seg_5999" s="T42">Na šʼolqumɨj ira.</ta>
            <ta e="T53" id="Seg_6000" s="T45">Tapɨlʼalʼ pɛlalʼ kortɨ, tolʼ takkɨlʼelʼ qolt pɛlakqɨlʼ ilɨmpa.</ta>
            <ta e="T992" id="Seg_6001" s="T53">Toqɨlʼ təp pokɨtɨlʼ təttɨt tošʼa qaj. </ta>
            <ta e="T61" id="Seg_6002" s="T59">Ilɨmpa tına.</ta>
            <ta e="T62" id="Seg_6003" s="T61">Qütalpa.</ta>
            <ta e="T68" id="Seg_6004" s="T62">Ijatɨ, ukkɨr ijatɨ pɛläkɨtɨj ukkɨr pɛläkɨtɨj neːkɨtɨj pɛlätɨ.</ta>
            <ta e="T74" id="Seg_6005" s="T68">Nʼi nälatɨ čʼäŋka, nʼi qajtɨ čʼäŋka.</ta>
            <ta e="T83" id="Seg_6006" s="T74">Nɨn na əsɨtɨ qütalna na ijalʼa, nɛntɨ na ijan.</ta>
            <ta e="T90" id="Seg_6007" s="T83">Nɨmtɨ ila qapij na əsɨntɨ əmintɨ qamqɨn.</ta>
            <ta e="T96" id="Seg_6008" s="T90">Tontɨ tına šʼittɨ qälɨ timnʼäsɨqä ɛppɨntɔːqı.</ta>
            <ta e="T101" id="Seg_6009" s="T96">Qälɨ ira kučʼčʼa ɛnta?</ta>
            <ta e="T110" id="Seg_6010" s="T101">Qaj ila kučʼčʼä izhe tınena na ira ılla quŋa.</ta>
            <ta e="T122" id="Seg_6011" s="T110">İlla quŋa. Tına nɨn šʼittal tına ija … qälɨ ira nɔːtɨ kučʼčʼa qatɛnta?</ta>
            <ta e="T125" id="Seg_6012" s="T122">To tɨmnʼantɨn tulɨnʼnʼa.</ta>
            <ta e="T127" id="Seg_6013" s="T125">Ira qumpa.</ta>
            <ta e="T129" id="Seg_6014" s="T127">İlla meːŋɔːt.</ta>
            <ta e="T138" id="Seg_6015" s="T129">Šʼittal imaqotatɨ, tına imaqotatɨ qütalna, imaqotatɨ ɛj ılla quŋa.</ta>
            <ta e="T145" id="Seg_6016" s="T138">Iralʼ mɨqä tına ijatɨ nʼekɔːl, pɛlɨkɔːl qala.</ta>
            <ta e="T147" id="Seg_6017" s="T145">Kučʼčʼa qattɛnta?</ta>
            <ta e="T157" id="Seg_6018" s="T147">Na qälɨ ira nɨnɨ šʼittäl to pučʼimpatɨ na čʼilʼaj ija.</ta>
            <ta e="T170" id="Seg_6019" s="T157">Mərqɨ ijatɨ … na istap … mat … mɨt … Istap Genɨlʼ mɨt tara.</ta>
            <ta e="T173" id="Seg_6020" s="T170">Nılʼčʼi ijatɨj ɛppa.</ta>
            <ta e="T177" id="Seg_6021" s="T173">To putɨŋɨtɨ, ɔːtätɨj čʼäŋka.</ta>
            <ta e="T182" id="Seg_6022" s="T177">Ɔːtäkɔːl topɨsa ilɨmpɔːt na äsasɨt.</ta>
            <ta e="T188" id="Seg_6023" s="T182">Nɨntɨ ilɨmpɔːt, ilɨmpɔːt, nɨnɨ to pučʼisɔːt. </ta>
            <ta e="T199" id="Seg_6024" s="T188">To pula na ija qälɨ ira mɔːttɨ, qälɨ ira ijatɨ čʼäŋka. </ta>
            <ta e="T201" id="Seg_6025" s="T199">Ijakɨtɨlʼ imatɨ.</ta>
            <ta e="T208" id="Seg_6026" s="T201">Tonna čʼontolʼ tɨmnʼatɨ nɛntɨ ukkɨr nälʼatɨ pɛläkɨtɨj.</ta>
            <ta e="T212" id="Seg_6027" s="T208">Takkɨn nʼenna šʼittɨ qälɨqı.</ta>
            <ta e="T215" id="Seg_6028" s="T212">Təp pokɨtɨl təttot.</ta>
            <ta e="T226" id="Seg_6029" s="T215">Ponʼi ira naɛntɨ kun montɨlʼ tamtɨrtɨ ɔːtäp muntɨk taqqɨšpɔːtɨt.</ta>
            <ta e="T227" id="Seg_6030" s="T226">Tal.</ta>
            <ta e="T238" id="Seg_6031" s="T227">Nɨlčʼik tüŋa na tɨmnʼasɨqä, na tɨmnʼasɨqä ilɔːqı, na iralʼ tɨmnʼasɨqä šʼittɨ.</ta>
            <ta e="T241" id="Seg_6032" s="T238">Tət to ɔːtätɨ.</ta>
            <ta e="T245" id="Seg_6033" s="T241">Na iran ɔːtäm iqɨntɔːqı.</ta>
            <ta e="T251" id="Seg_6034" s="T245">Qumɨıtɨsa tünta na Pon-ira, nimtɨ nɛintɨ. </ta>
            <ta e="T253" id="Seg_6035" s="T251">Nɨnɨ tal.</ta>
            <ta e="T258" id="Seg_6036" s="T253">Tüla šʼäqɔːt na təpɨtɨt mɔːt.</ta>
            <ta e="T266" id="Seg_6037" s="T258">Na ijap qapi aš tɛnɨmɔːt, qapɨ kuttar koŋɨmmɛnta?</ta>
            <ta e="T274" id="Seg_6038" s="T266">Täp qaj mutronak ilʼ kuttar ila.</ta>
            <ta e="T277" id="Seg_6039" s="T274">Tɨ kuttar tɛnnɨmɛntal?</ta>
            <ta e="T279" id="Seg_6040" s="T277">Alpän iläntɨj.</ta>
            <ta e="T282" id="Seg_6041" s="T279">Šʼäqqɔːqı na qaj.</ta>
            <ta e="T289" id="Seg_6042" s="T282">Mompa nɔːr pälɨt čʼelɨ toqɨn mompa na tüŋi.</ta>
            <ta e="T296" id="Seg_6043" s="T289">Mompa Pon-ira köt qumsä tümpa.</ta>
            <ta e="T300" id="Seg_6044" s="T296">Qapi na ɔːtät toqɨkɨntɔː.</ta>
            <ta e="T308" id="Seg_6045" s="T300">Na qumot nɔːt na tɨmnʼasɨqän na ɔːtäp toqqɨmpɔːt. </ta>
            <ta e="T319" id="Seg_6046" s="T308">Kun montɨlʼ mɔːttɨ tına šʼentɨ mɔːttɨ kun montɨlʼ qajtɨ muntɨ taqqaltɛntɨŋit.</ta>
            <ta e="T328" id="Seg_6047" s="T319">Ukolʼ nɔːtɨ takkɨlʼ qälɨıːtɨ ɔːtäp taqqɨlä nɔːtɨ muntɨk impɔːt.</ta>
            <ta e="T331" id="Seg_6048" s="T328">Nɨnɨ šʼittäl mɨŋa.</ta>
            <ta e="T336" id="Seg_6049" s="T331">Moqɨnä qənnɔːt na qälɨt tɨšʼa.</ta>
            <ta e="T341" id="Seg_6050" s="T336">Nɔːrtäl čʼeːlɨt mompa tüntɔːm mɨta.</ta>
            <ta e="T346" id="Seg_6051" s="T341">Tɛ mompa kučʼčʼät ɨkɨ qənnɨlɨt.</ta>
            <ta e="T356" id="Seg_6052" s="T346">Šʼittälɨ čʼeːlontɨ čʼeːlɨŋa na ija nılʼčʼiŋ ɛsa na ilʼčʼantɨntɨnɨk.</ta>
            <ta e="T359" id="Seg_6053" s="T356">Na gostʼan ilʼčʼant.</ta>
            <ta e="T365" id="Seg_6054" s="T359">Mɨta nʼenna tɨmtɨ mɨ ɛnta mɨta.</ta>
            <ta e="T376" id="Seg_6055" s="T365">Tət to ɔːtän nɨŋqɨkušʼšʼaŋ, mɨta nʼaraŋ ɛntɨ, mɨta nečʼatɨ ütalɔːmɨn mɨta.</ta>
            <ta e="T384" id="Seg_6056" s="T376">Qälɨ tüntɔːtɨ nɨntɨ ɔːtäp toqaqolamnɛntɔːt, nɨn mɨta tına.</ta>
            <ta e="T391" id="Seg_6057" s="T384">Ɔːtän uptɨ kəlʼčʼomto kərɨmpɨlʼ mɨta kučʼčʼa meːntɔːmɨn?</ta>
            <ta e="T395" id="Seg_6058" s="T391">Na ija nık kətɨŋɨtɨ:</ta>
            <ta e="T400" id="Seg_6059" s="T395">nık kətɨŋɨtɨ mɨta</ta>
            <ta e="T406" id="Seg_6060" s="T400">Mäka mɨtɨ šentɨ, šentɨ mɔːllɨ ɛŋa? </ta>
            <ta e="T412" id="Seg_6061" s="T406">Il'čʼa mɨta šentɨ mɔːllɨ ɛŋa?</ta>
            <ta e="T415" id="Seg_6062" s="T412">Mäqa mitɨ.</ta>
            <ta e="T425" id="Seg_6063" s="T415">A tonna tına tɨmnʼantɨ nälʼa, ilʼčʼatɨ namɨp quraltɨmmɨntɨt na ijanɨk.</ta>
            <ta e="T439" id="Seg_6064" s="T425">Man mɨta (pušto/prosto?) məčʼimnɛntak mɔːt, nʼenna mɨntɨ mačʼilʼ tıčʼi ɛnta, na nʼennälʼ pɛlaktɨ mɔːttɨŋnɛntak. </ta>
            <ta e="T443" id="Seg_6065" s="T439">Šʼentɨ mɔːt tatɨŋa nɛntɨ.</ta>
            <ta e="T444" id="Seg_6066" s="T443">Ütalnɔːtɨt.</ta>
            <ta e="T445" id="Seg_6067" s="T444">Tal.</ta>
            <ta e="T447" id="Seg_6068" s="T445">Ütaltɔːt na.</ta>
            <ta e="T457" id="Seg_6069" s="T447">Qapija nuːnɨčʼisɨ, šʼittɨmtäl, nɔːrtäl čʼeʼlɨ tüntɔːt Poni-iralʼ mɨ təpɨtɨtkin.</ta>
            <ta e="T467" id="Seg_6070" s="T457">Na qälɨ iralʼ mɨqä nɔːtɨ qalɨqɨnto, tɨmnʼasɨqä nɔːtɨ tap čʼeːlqot.</ta>
            <ta e="T478" id="Seg_6071" s="T467">Nɨnɨ šʼittalʼ ütaltɔːt na ɔːtam na toqqɨntɔːt, qapij tɛmʼe, kučʼčʼatɨl. </ta>
            <ta e="T481" id="Seg_6072" s="T478">Mɔːssa na mintɔːt.</ta>
            <ta e="T490" id="Seg_6073" s="T481">Mɔːt šʼerna, na mačʼit tıčʼit jennalʼ pɛlalʼ mɔːt šʼer[na].</ta>
            <ta e="T495" id="Seg_6074" s="T490">Nɨn na mɔːtɨŋtɔː na imantɨsa.</ta>
            <ta e="T501" id="Seg_6075" s="T495">Qapi təttalʼ qälʼ ira qaj mɔːttɨ čʼäŋka.</ta>
            <ta e="T506" id="Seg_6076" s="T501">Šʼentɨ mɔːttɨ qopɨlʼ mɔːttɨ tına.</ta>
            <ta e="T975" id="Seg_6077" s="T506">Wərqɨlɔː čʼesɨŋɨtɨ.</ta>
            <ta e="T976" id="Seg_6078" s="T975">((NN:)) (…) koŋinpäš.</ta>
            <ta e="T990" id="Seg_6079" s="T976">((NN:)) Бабушка? </ta>
            <ta e="T977" id="Seg_6080" s="T990">((NEP:)) А?</ta>
            <ta e="T514" id="Seg_6081" s="T977">((NN:)) Na ıllalɔː ukaltäš.</ta>
            <ta e="T515" id="Seg_6082" s="T514">((KuAI:)) Сейчас попробуем.</ta>
            <ta e="T978" id="Seg_6083" s="T515">((NN:)) … а на радио…</ta>
            <ta e="T979" id="Seg_6084" s="T978">((NN:)) na mantɨ sprashivaet.</ta>
            <ta e="T980" id="Seg_6085" s="T979">((NN:)) somaŋ na koŋɨmintɨ, na koŋaltɨmɨntɨ.</ta>
            <ta e="T981" id="Seg_6086" s="T980">((NN:)) Taltɨ, ɨkɨ mortäš.</ta>
            <ta e="T982" id="Seg_6087" s="T981">((NN:)) qapija qoqɨta</ta>
            <ta e="T983" id="Seg_6088" s="T982">((NN:)) Qapi üŋɨltɨmpatɨ mozhet bɨtʼ qoškol kətɨŋɨtɨ.</ta>
            <ta e="T974" id="Seg_6089" s="T983">((NN:)) etɨkɔːl</ta>
            <ta e="T542" id="Seg_6090" s="T974">((NEP:)) Nɨnɨ šʼittäl šʼäqqɔːj na ija imantɨsa.</ta>
            <ta e="T549" id="Seg_6091" s="T542">Šʼäqqɔː, toptɨlʼ qar nɨlʼ ɛsa imantɨn.</ta>
            <ta e="T556" id="Seg_6092" s="T549">Man mɨta nʼenna qumɨıqan qonʼišʼak qaj mɨta.</ta>
            <ta e="T559" id="Seg_6093" s="T556">Mɔːtɨŋpɔːtɨn, qaj qatɨmpɔːtɨn.</ta>
            <ta e="T564" id="Seg_6094" s="T559">Nʼenna laqaltɛlʼčʼa nɔːkɨr qoptɨp sorɨlla.</ta>
            <ta e="T569" id="Seg_6095" s="T564">Qapi nɔːssarɨlʼ ɔːta qaltɨmɨtɨt na.</ta>
            <ta e="T576" id="Seg_6096" s="T569">Ija nʼenna qənaıːsi, imatɨ ontɨ qala pɛläkkɔːl. </ta>
            <ta e="T578" id="Seg_6097" s="T576">Na ɔːmtɨŋa.</ta>
            <ta e="T581" id="Seg_6098" s="T578">Ompa na…</ta>
            <ta e="T588" id="Seg_6099" s="T581">Ukkɨr tɛ čʼontot čʼeːlɨtɨ ütontɨ na qəntɨ.</ta>
            <ta e="T595" id="Seg_6100" s="T588">Ütontɨqɨn na ija lʼamɨqɛŋa qapɨ nʼenna.</ta>
            <ta e="T604" id="Seg_6101" s="T595">Ukkɨr tät čʼontot mɨ qum na jap tattɨntɔːt takkɨntɨt.</ta>
            <ta e="T608" id="Seg_6102" s="T604">Poni-irat nälʼap tattɨntɔːtɨt.</ta>
            <ta e="T613" id="Seg_6103" s="T608">Köt qumtɨ qapi - ɔːtap toqqɨmpɨlʼ.</ta>
            <ta e="T621" id="Seg_6104" s="T613">Poni-ira našʼšʼak qotɨlʼ ɛŋa, našʼšʼak qəːtɨsä ɛːŋa.</ta>
            <ta e="T630" id="Seg_6105" s="T621">Nɨnɨ šʼittäl tal na ija tülʼčʼikuŋä - ponä imatɨ üŋkiltɨmpa.</ta>
            <ta e="T636" id="Seg_6106" s="T630">Tülʼčʼiqɨŋä na ɔːtamtɨ to sora ɔːtamtɨ.</ta>
            <ta e="T984" id="Seg_6107" s="T636">((NN:)) To qəšɨmpa.</ta>
            <ta e="T641" id="Seg_6108" s="T984">((NEP:)) Montɨ čʼap tülʼčʼa montɨ.</ta>
            <ta e="T651" id="Seg_6109" s="T641">Poni-ira qaj montɨ tüŋɔːtɨt, qaqlɨlʼ mɨtɨt nık tottalɨmpɔːt.</ta>
            <ta e="T655" id="Seg_6110" s="T651">Tına etɨmantot təpɨt etɨmantot.</ta>
            <ta e="T662" id="Seg_6111" s="T655">Šʼerna, mɔːt šʼerna, motanot nɨŋkila qälɨk. </ta>
            <ta e="T668" id="Seg_6112" s="T662">Tokalʼ mɔːtanot qät topɨmt tupalna.</ta>
            <ta e="T671" id="Seg_6113" s="T668">Pemɨmtɨ na tupaltɨ.</ta>
            <ta e="T675" id="Seg_6114" s="T671">Qannɨmpɨlʼ pemɨtɨ pɛlalʼ tupalnɨt.</ta>
            <ta e="T682" id="Seg_6115" s="T675">Nık kətɨŋɨtɨ nʼenna korala Poni-iran mɨta.</ta>
            <ta e="T688" id="Seg_6116" s="T682">Ɔːklɨ mitɨ mɨta qolqol olʼa mɨ.</ta>
            <ta e="T689" id="Seg_6117" s="T688">Taŋal.</ta>
            <ta e="T694" id="Seg_6118" s="T689">Na nʼenna paktɨmmɨntɨ na kupaks.</ta>
            <ta e="T698" id="Seg_6119" s="T694">Täqɨlɛːmmɨntɨtɨ tɨ pɛlaj utɨntɨ täqɨlaıːmpat. </ta>
            <ta e="T704" id="Seg_6120" s="T698">Poni-iralʼ mɨtɨp muntɨk lɛpäk qättɨmpatɨ.</ta>
            <ta e="T706" id="Seg_6121" s="T704">Ɔːmɨtɨ qumpa.</ta>
            <ta e="T718" id="Seg_6122" s="T706">Poni-ira illa qalɨmtɨ, šʼittɨ qälɨqı ilɨla qälɨqı, pona qəllaıːntɨ na ija.</ta>
            <ta e="T722" id="Seg_6123" s="T718">Ukkɨr por kupaksä täqɨlɛıːmpat.</ta>
            <ta e="T730" id="Seg_6124" s="T722">Muntɨk moqɨnät porqɨn šʼunʼontɨ nʼolʼqɨmɔːtpɔːt na qumpɨlʼ mɨ.</ta>
            <ta e="T731" id="Seg_6125" s="T730">Pista.</ta>
            <ta e="T744" id="Seg_6126" s="T731">Nɨnɨ šʼittäl ponä qəllaıːŋɨtɨ etɨmantontɨ, tınta na qaqlɨt tolʼ kečʼit poqɨn na soralɨmmɨntɔːt.</ta>
            <ta e="T753" id="Seg_6127" s="T744">Poni-ira ɛj ponä ittɨmpatɨ, qaj muntɨk ponä ittɨmpatɨ. </ta>
            <ta e="T763" id="Seg_6128" s="T753">İlla omta, tına malʼčʼalʼ toktɨ, to ila amɨrla ılla šäqa.</ta>
            <ta e="T770" id="Seg_6129" s="T763">Na qumɨıːtɨ poqɨn na ippalɨmmɨntɔːt nɔːtɨ sorɨmpkol.</ta>
            <ta e="T773" id="Seg_6130" s="T770">Kuttar soralɨmpa, nılʼčʼik.</ta>
            <ta e="T778" id="Seg_6131" s="T773">Šʼäqqa, toptɨlʼ qar ınna wəša.</ta>
            <ta e="T790" id="Seg_6132" s="T778">Poni-ira nılʼčʼi mɨta məntɨlʼ olɨtɨ tot taŋqɨımpa, tına keksa saijatɨ qətɨmpa.</ta>
            <ta e="T796" id="Seg_6133" s="T790">Tonna qälɨt iše somalɔqɨ ɛppa.</ta>
            <ta e="T803" id="Seg_6134" s="T796">Nılʼ kətɨŋɨtɨ na ija mɨta na qälɨtkinʼ.</ta>
            <ta e="T811" id="Seg_6135" s="T803">Qumɨıːmtɨ moqɨnä qəntät mɨta qaqloqantɨ tɛltala mɨta kurala.</ta>
            <ta e="T813" id="Seg_6136" s="T811">Ontɨt qaqloqɨntɨt.</ta>
            <ta e="T830" id="Seg_6137" s="T813">Nɨnɨ šʼittal to namʼä pona qə qəja qapi mɔːt šʼerɨšqo aj qaj amɨrqo aj qaj ašʼa. </ta>
            <ta e="T836" id="Seg_6138" s="T830">Ninɨ na qum tıntɨna soralpɨtɨlʼ qaqlɨntɨt.</ta>
            <ta e="T847" id="Seg_6139" s="T836">Tına nɨmtɨ moqonä kəčʼala qumɨıntɨ na qumpɨj qumɨıntɨ nılʼ qəntɨŋɨtɨ moqonä.</ta>
            <ta e="T849" id="Seg_6140" s="T847">Takkɨ moqonä.</ta>
            <ta e="T854" id="Seg_6141" s="T849">Poni-iram tɛ kəčʼila qəntɨŋɨtɨ.</ta>
            <ta e="T858" id="Seg_6142" s="T854">Qapi kɨpɨka kəitɨ ola.</ta>
            <ta e="T861" id="Seg_6143" s="T858">Təmol qənnɔːtɨt tına.</ta>
            <ta e="T865" id="Seg_6144" s="T861">Nɨnɨ somak qənnɔːtɨn nɨmtɨ.</ta>
            <ta e="T875" id="Seg_6145" s="T865">Imantɨn nık kətɨŋɨt: Mɔːllɨ to tɨlät me nʼenna qəntɨsɔːmi qumɨtɨtkin.</ta>
            <ta e="T884" id="Seg_6146" s="T875">Aš tɛnɨmɨmpɔːt qapi ontɨn na ilʼčʼantɨlʼ mɨt.</ta>
            <ta e="T891" id="Seg_6147" s="T884">Nʼenna qənna tıntena ontɨ ašʼa kɔːŋɨmpa.</ta>
            <ta e="T898" id="Seg_6148" s="T891">Tına imatɨ na kətantɨtɨ äsantɨ ilʼčʼantɨ mɨqätqı.</ta>
            <ta e="T905" id="Seg_6149" s="T898">Poni-iralʼ mɨt tına moqonä qonišʼɔːtɨ mompa.</ta>
            <ta e="T906" id="Seg_6150" s="T905">Tal.</ta>
            <ta e="T911" id="Seg_6151" s="T906">Nɨnɨ šʼittalʼ pija qapija (…)</ta>
            <ta e="T985" id="Seg_6152" s="T911">Jakɔːlɨn aš tɛnɨmɔːtɨt.</ta>
            <ta e="T986" id="Seg_6153" s="T985">((NN:)) Qarräl koŋɨmpäš.</ta>
            <ta e="T987" id="Seg_6154" s="T986">((NN:)) Qarrä radioqantɨ.</ta>
            <ta e="T932" id="Seg_6155" s="T987">((NEP:)) Takkɨ nʼenna ɨpalnoj täpɨjat nečʼa ilčʼantɨ mɔːt mɔːt šerna, qaqlɨt to nıːmɨt kurɨla nık tottɨŋɨt.</ta>
            <ta e="T936" id="Seg_6156" s="T932">Tonna qənmɨntɔːt, moqonä qənmɨntɔːt.</ta>
            <ta e="T944" id="Seg_6157" s="T936">Poni-ira kekɨsa takkɨn mɔːtqɨn tulɨšpa, ılla qumpa.</ta>
            <ta e="T949" id="Seg_6158" s="T944">Tonna šʼittɨ qumɔːqııːtɨ ilɨmpɔː.</ta>
            <ta e="T988" id="Seg_6159" s="T949">Poni-ira, Poni-iralʼ mɨt nɨmtɨ ɛŋa.</ta>
            <ta e="T989" id="Seg_6160" s="T988">((NN:)) Jarɨ mɨ kətät, jarɨk, jarɨk.</ta>
            <ta e="T964" id="Seg_6161" s="T989">((NN:)) Jarɨ mɨ kətät.</ta>
            <ta e="T965" id="Seg_6162" s="T964">((NN:)) Тут рассказанное всё и есть.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T968" id="Seg_6163" s="T0">((KuAI:)) Лентяю всегда праздник. </ta>
            <ta e="T6" id="Seg_6164" s="T968">((NEP:)) ((…)) Ukkɨr ijatɨ ɛppantɨ ((…)). </ta>
            <ta e="T13" id="Seg_6165" s="T6">Na qos ket ((…)) qət kətɨkak namɨp. </ta>
            <ta e="T19" id="Seg_6166" s="T13">Na tɛntɨlʼ, na tɛntɨlʼ, ašša čʼaptä. </ta>
            <ta e="T969" id="Seg_6167" s="T19">((KuAI:)) Подождите, пусть она подождёт, я попробую, посмотрю, проверю. </ta>
            <ta e="T970" id="Seg_6168" s="T969">((NN:)) Tan ılqɨt ((…)) šʼolqumɨt näčʼčʼa ((…)). </ta>
            <ta e="T971" id="Seg_6169" s="T970">((NN:)) Tam na šintɨ qolʼčʼanta ((…)) tan əːtal utrejqɨjojčʼimpät. </ta>
            <ta e="T972" id="Seg_6170" s="T971">((NN:)) Tuttaltät. </ta>
            <ta e="T973" id="Seg_6171" s="T972">((NN:)) Čʼontɨkak kətätɨ. </ta>
            <ta e="T41" id="Seg_6172" s="T973">((NEP:)) Pija na ira ilɨmpan. </ta>
            <ta e="T42" id="Seg_6173" s="T41">Qütalpa. </ta>
            <ta e="T45" id="Seg_6174" s="T42">Na šʼojqumɨj ira. </ta>
            <ta e="T53" id="Seg_6175" s="T45">Tapɨlʼalʼ pɛlalʼ kortɨ, tɔːlʼ takkɨlʼelʼ qolt pɛlakqɨlʼ ilɨmpa. </ta>
            <ta e="T992" id="Seg_6176" s="T53">Toːqɨlʼ təp poːkɨtɨlʼ təttɨt (tošʼa) qaj. </ta>
            <ta e="T59" id="Seg_6177" s="T992">((…)). </ta>
            <ta e="T61" id="Seg_6178" s="T59">Ilɨmpa tına. </ta>
            <ta e="T62" id="Seg_6179" s="T61">Qütalpa. </ta>
            <ta e="T68" id="Seg_6180" s="T62">Ijatɨ, ukkɨr ijatɨ pɛläkɨtɨj neːkɨtɨj pɛlätɨ. </ta>
            <ta e="T74" id="Seg_6181" s="T68">Nʼi nälatɨ čʼäŋka, nʼi qajtɨ čʼäŋka. </ta>
            <ta e="T83" id="Seg_6182" s="T74">Nɨn na əsɨtɨ qütalna na ijalʼa, (naintɨ) na ijan. </ta>
            <ta e="T90" id="Seg_6183" s="T83">Nɨmtɨ ila qapij na əsɨntɨ əmɨntɨ qamqɨn. </ta>
            <ta e="T96" id="Seg_6184" s="T90">Toːntɨ tına šʼittɨ qälɨ timnʼäsɨqı ɛːppɨntɔːqı. </ta>
            <ta e="T101" id="Seg_6185" s="T96">Qälɨ ira kučʼčʼa ɛːnta (tıː)? </ta>
            <ta e="T110" id="Seg_6186" s="T101">Qaj ila kučʼčʼä izhe tıntena na ira ılla quŋa. </ta>
            <ta e="T122" id="Seg_6187" s="T110">İlla quŋa, tına nɨn šittal tına ija, qälɨ ira nɔːtɨ kučʼčʼa qatɛnta? </ta>
            <ta e="T125" id="Seg_6188" s="T122">Toː tɨmnʼantɨn tulɨnʼnʼa. </ta>
            <ta e="T127" id="Seg_6189" s="T125">Ira qumpa. </ta>
            <ta e="T129" id="Seg_6190" s="T127">İlla meːŋɔːt. </ta>
            <ta e="T138" id="Seg_6191" s="T129">Šittal imaqotatɨ, tına imaqotatɨ qütalna, imaqotatɨ ɛj ılla quŋa. </ta>
            <ta e="T145" id="Seg_6192" s="T138">Iralʼ mɨqä tına ijatɨ nʼeːkɔːl, pɛlɨkɔːl qala. </ta>
            <ta e="T147" id="Seg_6193" s="T145">Kučʼčʼa qattɛnta? </ta>
            <ta e="T157" id="Seg_6194" s="T147">Na qälɨ ira nɨnɨ šittäl tɔː puːčʼɨmpatɨ na čʼilʼaj ija. </ta>
            <ta e="T170" id="Seg_6195" s="T157">Mərqɨ ijatɨ (na istap= mat- mɨt-) istap Genɨlʼ mɨt tarı. </ta>
            <ta e="T173" id="Seg_6196" s="T170">Nılʼčʼij ijatɨj ɛppa. </ta>
            <ta e="T177" id="Seg_6197" s="T173">Tɔː puːtɨŋɨtɨ, ɔːtätɨj čʼäŋka. </ta>
            <ta e="T182" id="Seg_6198" s="T177">Ɔːtäkɔːl topɨsa ilɨmpɔːt na äsasɨt. </ta>
            <ta e="T188" id="Seg_6199" s="T182">Nɨmtɨ ilɨmpɔːtɨn, ilɨmpɔːtɨn, nɨnɨ tɔː puːčʼɨsɔːt. </ta>
            <ta e="T199" id="Seg_6200" s="T188">Tɔː puːlä na ija qälɨ ira mɔːttɨ, qälɨ ira ijatɨ čʼäŋka. </ta>
            <ta e="T201" id="Seg_6201" s="T199">Ijakɨtɨlʼ imatɨ. </ta>
            <ta e="T208" id="Seg_6202" s="T201">Toːnna čʼontolʼ tɨmnʼatɨ (naintɨ) ukkɨr nälʼatɨ pɛläkɨtɨj. </ta>
            <ta e="T212" id="Seg_6203" s="T208">Takkɨn nʼenna šʼittɨ qälɨqı. </ta>
            <ta e="T215" id="Seg_6204" s="T212">Təp poːkɨtɨlʼ təttot. </ta>
            <ta e="T226" id="Seg_6205" s="T215">(Ponʼi ira=) Ponʼi ira naɛːntɨ kun montɨlʼ tamtɨrtɨ ɔːtäp muntɨk taqqɨšpatɨ. </ta>
            <ta e="T227" id="Seg_6206" s="T226">Tal. </ta>
            <ta e="T238" id="Seg_6207" s="T227">Nɨlčʼik tüŋa na timnʼasɨqä, na timnʼasɨqä ilɔːqı, na iralʼ timnʼasɨqä šʼittɨ. </ta>
            <ta e="T241" id="Seg_6208" s="T238">Tət toːn ɔːtätɨ. </ta>
            <ta e="T245" id="Seg_6209" s="T241">Na iran ɔːtäm iqɨntɔːqı. </ta>
            <ta e="T251" id="Seg_6210" s="T245">Qumiːntɨsa tünta Poni ira, nimtɨ najntɨ. </ta>
            <ta e="T253" id="Seg_6211" s="T251">Nɨnɨ tal. </ta>
            <ta e="T258" id="Seg_6212" s="T253">Tüla šäqqɔːtɨt na təptɨn mɔːt. </ta>
            <ta e="T266" id="Seg_6213" s="T258">Na ijap qapı aš tɛnɨmɔːt, ((…)) kuttar koŋɨmmɛnta? </ta>
            <ta e="T274" id="Seg_6214" s="T266">Täp qaj ((…)) mutronak ((…)) ilʼ kuttar ila. </ta>
            <ta e="T277" id="Seg_6215" s="T274">Tıː kuttar tɛnɨmmɛntal? </ta>
            <ta e="T279" id="Seg_6216" s="T277">Alpän iläntɨj. </ta>
            <ta e="T282" id="Seg_6217" s="T279">Šäqqɔːqı na qaj. </ta>
            <ta e="T289" id="Seg_6218" s="T282">Mompa nɔːrtälɨj čʼeːlɨ toqɨn mompa na tüŋɨ. </ta>
            <ta e="T296" id="Seg_6219" s="T289">Mompa Poni ira ((…)) köt qumsä (tümpa). </ta>
            <ta e="T300" id="Seg_6220" s="T296">Qapı na ɔːtät tɔːqqɨqɨntɔːqo. </ta>
            <ta e="T308" id="Seg_6221" s="T300">Na qumot nɔːt na timnʼasɨqän na ɔːtäp tɔːqqɨmpɔːt. </ta>
            <ta e="T319" id="Seg_6222" s="T308">Kun montɨlʼ mɔːttɨ tına šentɨ mɔːttɨ kun montɨlʼ qajtɨ muntɨ taqqɨltɛntɨŋit. </ta>
            <ta e="T328" id="Seg_6223" s="T319">Ukoːn nɔːta takkɨlʼ qälɨiːt ɔːtäp taqqɨllä nɔːtɨ muntɨk iːmpɔːt. </ta>
            <ta e="T331" id="Seg_6224" s="T328">Nɨnɨ šittäl mɨŋa. </ta>
            <ta e="T336" id="Seg_6225" s="T331">Moqɨnä qənnɔːt na qälɨt tıšša. </ta>
            <ta e="T341" id="Seg_6226" s="T336">Nɔːrtäl čʼeːlɨt mompa tüntɔːmɨn mɨta. </ta>
            <ta e="T346" id="Seg_6227" s="T341">Tɛː mompa kučʼčʼät ɨkɨ qənnɨlɨt. </ta>
            <ta e="T356" id="Seg_6228" s="T346">Šittälɨ čʼeːlontɨ na čʼeːlɨnna na ija nılʼčʼiŋ ɛsa na ilʼčʼantɨnɨk. </ta>
            <ta e="T359" id="Seg_6229" s="T356">Na gostʼan ilʼčʼant. </ta>
            <ta e="T365" id="Seg_6230" s="T359">Mɨta nʼenna tɨmtɨ mɨ ɛːnta mɨta. </ta>
            <ta e="T376" id="Seg_6231" s="T365">Tɛːt toːn ɔːtän nɨŋqɨkuššaŋ, mɨta nʼaraŋ ɛːntɨ, mɨta näčʼčʼatɨ üːtalɔːmɨn mɨta. </ta>
            <ta e="T384" id="Seg_6232" s="T376">Qälɨt tüntɔːtɨn nɨntɨ ɔːtäp tɔːqqɨqolamnɛntɔːtɨn, nın mɨta tına. </ta>
            <ta e="T391" id="Seg_6233" s="T384">Ɔːtän uptɨ kəlʼčʼomɨnto kərɨmpɨlʼ mɨta kučʼčʼa meːntɔːmɨn? </ta>
            <ta e="T395" id="Seg_6234" s="T391">Na ija nık kətɨŋɨtɨ. </ta>
            <ta e="T400" id="Seg_6235" s="T395">Nɨnɨ nık kətɨŋɨtɨ mɨta ((…)). </ta>
            <ta e="T406" id="Seg_6236" s="T400">Mäkka mitɨ šentɨ, šentɨ mɔːllɨ ɛːŋa? </ta>
            <ta e="T412" id="Seg_6237" s="T406">Ilʼčʼa mɨta qaj šentɨ mɔːllɨ ɛːŋa? </ta>
            <ta e="T415" id="Seg_6238" s="T412">Mäkka mitɨ mɨta. </ta>
            <ta e="T425" id="Seg_6239" s="T415">A toːnna tına timnʼantɨ nälʼa, ilʼčʼatɨ namɨp quraltɨmmɨntɨt na ijanɨk. </ta>
            <ta e="T439" id="Seg_6240" s="T425">Man mɨta prosto məčʼimnɛntak mɔːt, nʼennat mɨntɨ mačʼilʼ tıčʼi ɛnta, na nʼennälʼ pɛlaktɨ mɔːtɨŋnɛntak. </ta>
            <ta e="T443" id="Seg_6241" s="T439">Šentɨ mɔːt taːtɨŋa nɛntɨ. </ta>
            <ta e="T444" id="Seg_6242" s="T443">Üːtalnɔːtɨt. </ta>
            <ta e="T445" id="Seg_6243" s="T444">Tal. </ta>
            <ta e="T447" id="Seg_6244" s="T445">Üːtaltɔːt na. </ta>
            <ta e="T457" id="Seg_6245" s="T447">Qapija nuːnɨčʼɨsɨ, šittɨmtäl, nɔːrtäl čʼeːlɨ tüntɔːt Poni iralʼ mɨ təpɨtɨtkin. </ta>
            <ta e="T467" id="Seg_6246" s="T457">Na qälɨ iralʼ mɨqä nɔːtɨ taqalqɨnto, timnʼasɨqä nɔːtɨ (tap čʼeːlqot). </ta>
            <ta e="T478" id="Seg_6247" s="T467">Nɨnɨ šʼittalʼ üːtaltɔːt na ɔːtam na tɔːqqɨntɔːt, qapij tɛm ɛj ((…)). </ta>
            <ta e="T481" id="Seg_6248" s="T478">Mɔːssa na mintɔːt. </ta>
            <ta e="T490" id="Seg_6249" s="T481">Mɔːt šerna, na mačʼit tıčʼɨt jennalʼ pɛlalʼ mɔːt šerna. </ta>
            <ta e="T495" id="Seg_6250" s="T490">Nɨn na mɔːtɨŋtɔːqı na imantɨsa. </ta>
            <ta e="T501" id="Seg_6251" s="T495">Qapı təttalʼ qälʼ ira qaj mɔːttɨ čʼäŋka. </ta>
            <ta e="T506" id="Seg_6252" s="T501">Šentɨ mɔːttɨ qopɨlʼ mɔːttɨ tına. </ta>
            <ta e="T975" id="Seg_6253" s="T506">Wərqɨlɔː čʼesɨŋɨtɨ. </ta>
            <ta e="T976" id="Seg_6254" s="T975">((NN:)) ((…)) koŋɨmpäš.</ta>
            <ta e="T990" id="Seg_6255" s="T976">((NN:)) Бабушка? </ta>
            <ta e="T977" id="Seg_6256" s="T990">((NEP:)) А? </ta>
            <ta e="T514" id="Seg_6257" s="T977">((NN:)) Na ıllalɔː uːkaltäš. </ta>
            <ta e="T515" id="Seg_6258" s="T514">((KuAI:)) Сейчас попробуем. ((BRK)).</ta>
            <ta e="T978" id="Seg_6259" s="T515">((NN:)) ((…)) а на радио ((LAUGH))…</ta>
            <ta e="T979" id="Seg_6260" s="T978">((NN:)) Na mantɨ sprašivaet. </ta>
            <ta e="T980" id="Seg_6261" s="T979">((NN:)) Somaŋ na koŋɨmmɨntɨ, na koŋaltɨmmɨntɨ. </ta>
            <ta e="T981" id="Seg_6262" s="T980">((NN:)) Tal, ɨkɨ mortäš. </ta>
            <ta e="T982" id="Seg_6263" s="T981">((NN:)) Qapija qoːkɨtalʼ ((…)). </ta>
            <ta e="T983" id="Seg_6264" s="T982">((NN:)) Qapı üŋkɨltɨmpatɨ moʒet pɨtʼ qoškɔːl kətɨŋɨtɨ. </ta>
            <ta e="T974" id="Seg_6265" s="T983">((NN:)) Əːtɨkɔːl. </ta>
            <ta e="T542" id="Seg_6266" s="T974">((NEP:)) Nɨnɨ šittäl šäqqɔːj na ija imantɨsa. </ta>
            <ta e="T549" id="Seg_6267" s="T542">Šäqqɔːt, toptɨlʼ qar nılʼ ɛsa tına imantɨn. </ta>
            <ta e="T556" id="Seg_6268" s="T549">Man mɨta nʼenna qumiːqan qonʼišak qaj mɨta. </ta>
            <ta e="T559" id="Seg_6269" s="T556">Mɔːtɨŋpɔːtɨn, qaj qattɨmpɔːtɨn. </ta>
            <ta e="T564" id="Seg_6270" s="T559">Nʼenna laqaltɛlʼčʼa nɔːkɨr qoptɨp sɔːrɨla. </ta>
            <ta e="T569" id="Seg_6271" s="T564">Qapı nɔːssarɨlʼ ɔːta qaltɨmɨntɨt na. </ta>
            <ta e="T576" id="Seg_6272" s="T569">Ija nʼenna qənaisɨ, imatɨ ontɨ qala pɛläkɔːl. </ta>
            <ta e="T578" id="Seg_6273" s="T576">Na ɔːmtɨŋa. </ta>
            <ta e="T581" id="Seg_6274" s="T578">Ompa na … </ta>
            <ta e="T588" id="Seg_6275" s="T581">Ukkɨr tot čʼontot čʼeːlɨtɨ üːtontɨ na qəntɨ. </ta>
            <ta e="T595" id="Seg_6276" s="T588">Üːtontɨqɨn na ija lʼamɨk ɛːŋa qapı nʼenna. </ta>
            <ta e="T604" id="Seg_6277" s="T595">Ukkɨr tät čʼontot mɨ qum (na jap) tattɨntɔːt takkɨntɨt. </ta>
            <ta e="T608" id="Seg_6278" s="T604">Poni iralʼ (na jap) tattɨntɔːtɨt. </ta>
            <ta e="T613" id="Seg_6279" s="T608">Köt qumiːtɨ qapı– ɔːtap tɔːqqɨmpɨlʼ. </ta>
            <ta e="T621" id="Seg_6280" s="T613">Poni ira naššak qotɨlʼ ɛːŋa, naššak qəːtɨsä ɛːŋa. </ta>
            <ta e="T630" id="Seg_6281" s="T621">Nɨnɨ šittäl tal na ija tülʼčʼikunä– ponä imatɨ üŋkɨltɨmpa. </ta>
            <ta e="T636" id="Seg_6282" s="T630">Tülʼčʼikunä na ɔːtamtɨ toː sɔːra ɔːtamtɨ. </ta>
            <ta e="T984" id="Seg_6283" s="T636">((NN:)) ((…)). </ta>
            <ta e="T641" id="Seg_6284" s="T984">((NEP:)) Montɨ čʼap tülʼčʼa montɨ. </ta>
            <ta e="T651" id="Seg_6285" s="T641">Poni iralʼ mɨt qaj montɨ tüŋɔːtɨt, qaqlɨlʼ mɨtɨt nık tottälɨmpɔːt. </ta>
            <ta e="T655" id="Seg_6286" s="T651">Tına ətɨmantot təpɨt ətɨmantot. </ta>
            <ta e="T662" id="Seg_6287" s="T655">Šerna, mɔːt šerna, mɔːtan ɔːt nɨŋkɨla qälɨk. </ta>
            <ta e="T668" id="Seg_6288" s="T662">Tokalʼ mɔːtan ɔːt qät topɨmt tupalna. </ta>
            <ta e="T671" id="Seg_6289" s="T668">Peːmɨmtɨ na tupaltɨ. </ta>
            <ta e="T675" id="Seg_6290" s="T671">Qannɨmpɨlʼ pɛlalʼ peːmɨtɨ tupalnɨt. </ta>
            <ta e="T682" id="Seg_6291" s="T675">Nık kətɨŋɨtɨ nʼenna koralla Poni iran mɨta. </ta>
            <ta e="T688" id="Seg_6292" s="T682">Ɔːklɨ mitɨ mɨta qolqol olʼa mɨ. </ta>
            <ta e="T689" id="Seg_6293" s="T688">Taŋal. </ta>
            <ta e="T694" id="Seg_6294" s="T689">Na nʼenna paktɨmmɨntɨ na kupaks. </ta>
            <ta e="T698" id="Seg_6295" s="T694">Təːqalaimmɨntɨtɨ pɛlaj utɨntɨ təːqalaimpat. </ta>
            <ta e="T704" id="Seg_6296" s="T698">Poni iralʼ mɨtɨp muntɨk lɛpäk qättɨmpatɨ. </ta>
            <ta e="T706" id="Seg_6297" s="T704">Ɔːmɨtɨ qumpa. </ta>
            <ta e="T718" id="Seg_6298" s="T706">Poni ira illa qalɨmtɨ, šittɨ qälɨqı illa qalɨmtɨ, pona qəːllaıːntɨ na ija. </ta>
            <ta e="T722" id="Seg_6299" s="T718">Ukkɨr pɔːr kupaksä təːqalaimpat. </ta>
            <ta e="T730" id="Seg_6300" s="T722">Muntɨk moqɨnät porqɨn šunʼnʼontɨ nʼolʼqɨmɔːtpɔːtɨn, na qumpɨlʼ mɨ. </ta>
            <ta e="T731" id="Seg_6301" s="T730">Pista. </ta>
            <ta e="T744" id="Seg_6302" s="T731">Nɨnɨ šittäl ponä qəllaıːŋɨtɨ ətɨmantontɨ, tınta na qaqlɨt (tolʼ kečʼit) poːqɨn na sɔːralɨmmɨntɔːtɨt. </ta>
            <ta e="T753" id="Seg_6303" s="T744">Poni ira ɛj ponä iːtɨmpatɨ, qaj muntɨk ponä iːtɨmpatɨ. </ta>
            <ta e="T763" id="Seg_6304" s="T753">İlla omta, tına malʼčʼalʼ toktɨ, ılla amɨrla ((…)) ılla šäqqa. </ta>
            <ta e="T770" id="Seg_6305" s="T763">Na qumiːtɨ poːqɨn na ippälɨmmɨntɔːt nɔːtɨ sɔːrɨmpkol. </ta>
            <ta e="T773" id="Seg_6306" s="T770">Kuttar sɔːralɨmpa, nılʼčʼik. </ta>
            <ta e="T778" id="Seg_6307" s="T773">Šäqqa, toptɨlʼ qar ınna wəša. </ta>
            <ta e="T790" id="Seg_6308" s="T778">Poni ira nılʼčʼi mɨta məntɨlʼ olɨtɨ tot taŋkɨıːmpa, tına keksa saijatɨ ((…)). </ta>
            <ta e="T796" id="Seg_6309" s="T790">Toːnna qälɨt ((…)) iše somalɔːqɨ ɛppa. </ta>
            <ta e="T803" id="Seg_6310" s="T796">Nılʼ kətɨŋɨtɨ na ija mɨta, na qälɨtɨtkinitɨ. </ta>
            <ta e="T811" id="Seg_6311" s="T803">Qumiːmtɨ moqɨnä qəntät mɨta qaqloqantɨ tɛltalla mɨta kuralla. </ta>
            <ta e="T813" id="Seg_6312" s="T811">Ontɨt qaqloqɨntɨt. </ta>
            <ta e="T830" id="Seg_6313" s="T813">Nɨnɨ šittal toː namä pona (qə qəja) qapı mɔːt šerıːšqo aj qaj amɨrqo aj qaj ašša amɨrqo. </ta>
            <ta e="T836" id="Seg_6314" s="T830">Ninɨ na qum tıntɨna sɔːralpɨtɨlʼ qaqlɨntɨt. </ta>
            <ta e="T847" id="Seg_6315" s="T836">Tına nɨmtɨ moqonä kəːčʼälä qumiːmtɨ na qumpɨj qumiːmtɨ nık qəntɨŋɨtɨ moqonä. </ta>
            <ta e="T849" id="Seg_6316" s="T847">Takkɨ moqonä. </ta>
            <ta e="T854" id="Seg_6317" s="T849">Poni iram tɛ kəːčʼɨlä qəntɨŋɨtɨ. </ta>
            <ta e="T858" id="Seg_6318" s="T854">Qapı kɨpɨka kəitɨ ola. </ta>
            <ta e="T861" id="Seg_6319" s="T858">Təmol qənnɔːtɨt tına. </ta>
            <ta e="T865" id="Seg_6320" s="T861">Nɨnɨ somak qənnɔːtɨn nɨmtɨ. </ta>
            <ta e="T875" id="Seg_6321" s="T865">Imantɨn nık kətɨŋɨt:“ Mɔːllɨ toː tılät me nʼenna qəntɨsɔːmıː qumɨtɨtkin”. </ta>
            <ta e="T884" id="Seg_6322" s="T875">Aš tɛnɨmɨmpɔːt qapı ontɨn na ilʼčʼantɨlʼ mɨt qos qaj. </ta>
            <ta e="T891" id="Seg_6323" s="T884">Nʼenna qənna tıntena ontɨ ašʼa koŋɨmpa ((…)). </ta>
            <ta e="T898" id="Seg_6324" s="T891">Toːna imatɨ na kətɛntɨtɨ äsantɨ ilʼčʼantɨ mɨqätkiːn. </ta>
            <ta e="T905" id="Seg_6325" s="T898">Poni iralʼ mɨt tına moqonä qonisɔːtɨt mompa. </ta>
            <ta e="T906" id="Seg_6326" s="T905">Tal. </ta>
            <ta e="T911" id="Seg_6327" s="T906">Nɨnɨ šʼittalʼ pija qapija ((…)). </ta>
            <ta e="T985" id="Seg_6328" s="T911">Aš tɛnɨmɔːtɨt. </ta>
            <ta e="T986" id="Seg_6329" s="T985">((NN:)) Karrä koŋɨmpäš. </ta>
            <ta e="T987" id="Seg_6330" s="T986">((NN:)) Karrä radioqantɨ. </ta>
            <ta e="T932" id="Seg_6331" s="T987">((NEP:)) Takkɨ nʼenna ɨpalnoj təpɨjat näčʼčʼa ilʼčʼantɨ mɔːt mɔːt šerna, qaqlɨt to nıːmɨt kurɨla nık tottɨŋɨtɨ. </ta>
            <ta e="T936" id="Seg_6332" s="T932">Toːnna qənmɨntɔːtɨn, moqonä qənmɨntɔːtɨt. </ta>
            <ta e="T944" id="Seg_6333" s="T936">Poni ira kekɨsa takkɨn mɔːtqɨn tulɨšpa, ılla qumpa. </ta>
            <ta e="T949" id="Seg_6334" s="T944">Toːnna šittɨ qumoːqıː tıː ilɨmpɔːqı. </ta>
            <ta e="T988" id="Seg_6335" s="T949">Poni ira, Poni iralʼ mɨt nɨmtɨ ɛːŋa. </ta>
            <ta e="T989" id="Seg_6336" s="T988">((NN:)) Jarɨ mɨ kətät, jarɨk, jarɨk. </ta>
            <ta e="T964" id="Seg_6337" s="T989">((NN:)) Jarɨ mɨ kətät. </ta>
            <ta e="T965" id="Seg_6338" s="T964">((NN:)) Тут рассказанное всё и есть. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T3" id="Seg_6339" s="T2">ukkɨr</ta>
            <ta e="T4" id="Seg_6340" s="T3">ija-tɨ</ta>
            <ta e="T5" id="Seg_6341" s="T4">ɛ-ppa-ntɨ</ta>
            <ta e="T7" id="Seg_6342" s="T6">na</ta>
            <ta e="T8" id="Seg_6343" s="T7">qos</ta>
            <ta e="T9" id="Seg_6344" s="T8">ket</ta>
            <ta e="T11" id="Seg_6345" s="T10">qət</ta>
            <ta e="T12" id="Seg_6346" s="T11">kətɨ-ka-k</ta>
            <ta e="T13" id="Seg_6347" s="T12">namɨ-p</ta>
            <ta e="T14" id="Seg_6348" s="T13">na</ta>
            <ta e="T15" id="Seg_6349" s="T14">tɛntɨlʼ</ta>
            <ta e="T16" id="Seg_6350" s="T15">na</ta>
            <ta e="T17" id="Seg_6351" s="T16">tɛntɨlʼ</ta>
            <ta e="T18" id="Seg_6352" s="T17">ašša</ta>
            <ta e="T19" id="Seg_6353" s="T18">čʼaptä</ta>
            <ta e="T21" id="Seg_6354" s="T20">tat</ta>
            <ta e="T22" id="Seg_6355" s="T21">ılqɨt</ta>
            <ta e="T24" id="Seg_6356" s="T23">šʼolqum-ɨ-t</ta>
            <ta e="T25" id="Seg_6357" s="T24">näčʼčʼa</ta>
            <ta e="T27" id="Seg_6358" s="T26">tam</ta>
            <ta e="T28" id="Seg_6359" s="T27">na</ta>
            <ta e="T29" id="Seg_6360" s="T28">šintɨ</ta>
            <ta e="T30" id="Seg_6361" s="T29">qo-lʼčʼa-nta</ta>
            <ta e="T32" id="Seg_6362" s="T31">tan</ta>
            <ta e="T33" id="Seg_6363" s="T32">əːta-l</ta>
            <ta e="T971" id="Seg_6364" s="T33">ut-r-ej-qɨ-joj-čʼi-mp-ät</ta>
            <ta e="T972" id="Seg_6365" s="T34">tu-tt-alt-ät</ta>
            <ta e="T36" id="Seg_6366" s="T35">čʼontɨ-ka-k</ta>
            <ta e="T973" id="Seg_6367" s="T36">kət-ätɨ</ta>
            <ta e="T38" id="Seg_6368" s="T37">pija</ta>
            <ta e="T39" id="Seg_6369" s="T38">na</ta>
            <ta e="T40" id="Seg_6370" s="T39">ira</ta>
            <ta e="T41" id="Seg_6371" s="T40">ilɨ-mpa-n</ta>
            <ta e="T42" id="Seg_6372" s="T41">qüta-l-pa</ta>
            <ta e="T43" id="Seg_6373" s="T42">na</ta>
            <ta e="T44" id="Seg_6374" s="T43">šʼojqum-ɨ-j</ta>
            <ta e="T45" id="Seg_6375" s="T44">ira</ta>
            <ta e="T46" id="Seg_6376" s="T45">tapɨlʼalʼ</ta>
            <ta e="T47" id="Seg_6377" s="T46">pɛla-lʼ</ta>
            <ta e="T48" id="Seg_6378" s="T47">kor-tɨ</ta>
            <ta e="T49" id="Seg_6379" s="T48">tɔː-lʼ</ta>
            <ta e="T50" id="Seg_6380" s="T49">takkɨ-lʼelʼ</ta>
            <ta e="T51" id="Seg_6381" s="T50">qolt</ta>
            <ta e="T52" id="Seg_6382" s="T51">pɛlak-qɨ-lʼ</ta>
            <ta e="T53" id="Seg_6383" s="T52">ilɨ-mpa</ta>
            <ta e="T54" id="Seg_6384" s="T53">toː-qɨ-lʼ</ta>
            <ta e="T55" id="Seg_6385" s="T54">təp</ta>
            <ta e="T56" id="Seg_6386" s="T55">poː-kɨtɨ-lʼ</ta>
            <ta e="T57" id="Seg_6387" s="T56">təttɨ-t</ta>
            <ta e="T992" id="Seg_6388" s="T58">qaj</ta>
            <ta e="T60" id="Seg_6389" s="T59">ilɨ-mpa</ta>
            <ta e="T61" id="Seg_6390" s="T60">tına</ta>
            <ta e="T62" id="Seg_6391" s="T61">qüta-l-pa</ta>
            <ta e="T63" id="Seg_6392" s="T62">ija-tɨ</ta>
            <ta e="T64" id="Seg_6393" s="T63">ukkɨr</ta>
            <ta e="T65" id="Seg_6394" s="T64">ija-tɨ</ta>
            <ta e="T66" id="Seg_6395" s="T65">pɛlä-kɨtɨ-j</ta>
            <ta e="T67" id="Seg_6396" s="T66">neː-kɨtɨ-j</ta>
            <ta e="T68" id="Seg_6397" s="T67">pɛlä-tɨ</ta>
            <ta e="T69" id="Seg_6398" s="T68">nʼi</ta>
            <ta e="T70" id="Seg_6399" s="T69">näla-tɨ</ta>
            <ta e="T71" id="Seg_6400" s="T70">čʼäŋka</ta>
            <ta e="T72" id="Seg_6401" s="T71">nʼi</ta>
            <ta e="T73" id="Seg_6402" s="T72">qaj-tɨ</ta>
            <ta e="T74" id="Seg_6403" s="T73">čʼäŋka</ta>
            <ta e="T75" id="Seg_6404" s="T74">nɨn</ta>
            <ta e="T76" id="Seg_6405" s="T75">na</ta>
            <ta e="T77" id="Seg_6406" s="T76">əsɨ-tɨ</ta>
            <ta e="T78" id="Seg_6407" s="T77">qüta-l-na</ta>
            <ta e="T79" id="Seg_6408" s="T78">na</ta>
            <ta e="T80" id="Seg_6409" s="T79">ija-lʼa</ta>
            <ta e="T82" id="Seg_6410" s="T81">na</ta>
            <ta e="T83" id="Seg_6411" s="T82">ija-n</ta>
            <ta e="T84" id="Seg_6412" s="T83">nɨmtɨ</ta>
            <ta e="T85" id="Seg_6413" s="T84">ila</ta>
            <ta e="T86" id="Seg_6414" s="T85">qapij</ta>
            <ta e="T87" id="Seg_6415" s="T86">na</ta>
            <ta e="T88" id="Seg_6416" s="T87">əsɨ-n-tɨ</ta>
            <ta e="T89" id="Seg_6417" s="T88">əmɨ-n-tɨ</ta>
            <ta e="T90" id="Seg_6418" s="T89">qam-qɨn</ta>
            <ta e="T91" id="Seg_6419" s="T90">toː-ntɨ</ta>
            <ta e="T92" id="Seg_6420" s="T91">tına</ta>
            <ta e="T93" id="Seg_6421" s="T92">šittɨ</ta>
            <ta e="T94" id="Seg_6422" s="T93">qälɨ</ta>
            <ta e="T95" id="Seg_6423" s="T94">timnʼä-sɨ-qı</ta>
            <ta e="T96" id="Seg_6424" s="T95">ɛː-ppɨ-ntɔː-qı</ta>
            <ta e="T97" id="Seg_6425" s="T96">qälɨ</ta>
            <ta e="T98" id="Seg_6426" s="T97">ira</ta>
            <ta e="T99" id="Seg_6427" s="T98">kučʼčʼa</ta>
            <ta e="T100" id="Seg_6428" s="T99">ɛː-nta</ta>
            <ta e="T102" id="Seg_6429" s="T101">qaj</ta>
            <ta e="T103" id="Seg_6430" s="T102">ila</ta>
            <ta e="T104" id="Seg_6431" s="T103">kučʼčʼä</ta>
            <ta e="T105" id="Seg_6432" s="T104">izhe</ta>
            <ta e="T106" id="Seg_6433" s="T105">tıntena</ta>
            <ta e="T107" id="Seg_6434" s="T106">na</ta>
            <ta e="T108" id="Seg_6435" s="T107">ira</ta>
            <ta e="T109" id="Seg_6436" s="T108">ılla</ta>
            <ta e="T110" id="Seg_6437" s="T109">qu-ŋa</ta>
            <ta e="T111" id="Seg_6438" s="T110">i̇lla</ta>
            <ta e="T112" id="Seg_6439" s="T111">qu-ŋa</ta>
            <ta e="T113" id="Seg_6440" s="T112">tına</ta>
            <ta e="T114" id="Seg_6441" s="T113">nɨn</ta>
            <ta e="T115" id="Seg_6442" s="T114">šittal</ta>
            <ta e="T116" id="Seg_6443" s="T115">tına</ta>
            <ta e="T117" id="Seg_6444" s="T116">ija</ta>
            <ta e="T118" id="Seg_6445" s="T117">qälɨ</ta>
            <ta e="T119" id="Seg_6446" s="T118">ira</ta>
            <ta e="T120" id="Seg_6447" s="T119">nɔːtɨ</ta>
            <ta e="T121" id="Seg_6448" s="T120">kučʼčʼa</ta>
            <ta e="T122" id="Seg_6449" s="T121">qat-ɛnta</ta>
            <ta e="T123" id="Seg_6450" s="T122">toː</ta>
            <ta e="T124" id="Seg_6451" s="T123">tɨmnʼa-ntɨ-n</ta>
            <ta e="T125" id="Seg_6452" s="T124">tulɨ-nʼ-nʼa</ta>
            <ta e="T126" id="Seg_6453" s="T125">ira</ta>
            <ta e="T127" id="Seg_6454" s="T126">qu-mpa</ta>
            <ta e="T128" id="Seg_6455" s="T127">i̇lla</ta>
            <ta e="T129" id="Seg_6456" s="T128">meː-ŋɔː-t</ta>
            <ta e="T130" id="Seg_6457" s="T129">šittal</ta>
            <ta e="T131" id="Seg_6458" s="T130">imaqota-tɨ</ta>
            <ta e="T132" id="Seg_6459" s="T131">tına</ta>
            <ta e="T133" id="Seg_6460" s="T132">imaqota-tɨ</ta>
            <ta e="T134" id="Seg_6461" s="T133">qüta-l-na</ta>
            <ta e="T135" id="Seg_6462" s="T134">imaqota-tɨ</ta>
            <ta e="T136" id="Seg_6463" s="T135">ɛj</ta>
            <ta e="T137" id="Seg_6464" s="T136">ılla</ta>
            <ta e="T138" id="Seg_6465" s="T137">qu-ŋa</ta>
            <ta e="T139" id="Seg_6466" s="T138">ira-lʼ</ta>
            <ta e="T140" id="Seg_6467" s="T139">mɨ-qä</ta>
            <ta e="T141" id="Seg_6468" s="T140">tına</ta>
            <ta e="T142" id="Seg_6469" s="T141">ija-tɨ</ta>
            <ta e="T143" id="Seg_6470" s="T142">nʼeː-kɔːl</ta>
            <ta e="T144" id="Seg_6471" s="T143">pɛlɨ-kɔːl</ta>
            <ta e="T145" id="Seg_6472" s="T144">qala</ta>
            <ta e="T146" id="Seg_6473" s="T145">kučʼčʼa</ta>
            <ta e="T147" id="Seg_6474" s="T146">qatt-ɛnta</ta>
            <ta e="T148" id="Seg_6475" s="T147">na</ta>
            <ta e="T149" id="Seg_6476" s="T148">qälɨ</ta>
            <ta e="T150" id="Seg_6477" s="T149">ira</ta>
            <ta e="T151" id="Seg_6478" s="T150">nɨnɨ</ta>
            <ta e="T152" id="Seg_6479" s="T151">šittäl</ta>
            <ta e="T153" id="Seg_6480" s="T152">tɔː</ta>
            <ta e="T154" id="Seg_6481" s="T153">puː-čʼɨ-mpa-tɨ</ta>
            <ta e="T155" id="Seg_6482" s="T154">na</ta>
            <ta e="T156" id="Seg_6483" s="T155">čʼilʼa-j</ta>
            <ta e="T157" id="Seg_6484" s="T156">ija</ta>
            <ta e="T158" id="Seg_6485" s="T157">mərqɨ</ta>
            <ta e="T159" id="Seg_6486" s="T158">ija-tɨ</ta>
            <ta e="T167" id="Seg_6487" s="T166">istap</ta>
            <ta e="T168" id="Seg_6488" s="T167">Gena-lʼ</ta>
            <ta e="T169" id="Seg_6489" s="T168">mɨ-t</ta>
            <ta e="T170" id="Seg_6490" s="T169">tarı</ta>
            <ta e="T171" id="Seg_6491" s="T170">nılʼčʼi-j</ta>
            <ta e="T172" id="Seg_6492" s="T171">ija-tɨ-j</ta>
            <ta e="T173" id="Seg_6493" s="T172">ɛ-ppa</ta>
            <ta e="T174" id="Seg_6494" s="T173">tɔː</ta>
            <ta e="T175" id="Seg_6495" s="T174">puː-tɨ-ŋɨ-tɨ</ta>
            <ta e="T176" id="Seg_6496" s="T175">ɔːtä-tɨ-j</ta>
            <ta e="T177" id="Seg_6497" s="T176">čʼäŋka</ta>
            <ta e="T178" id="Seg_6498" s="T177">ɔːtä-kɔːl</ta>
            <ta e="T179" id="Seg_6499" s="T178">topɨ-sa</ta>
            <ta e="T180" id="Seg_6500" s="T179">ilɨ-mpɔː-t</ta>
            <ta e="T181" id="Seg_6501" s="T180">na</ta>
            <ta e="T182" id="Seg_6502" s="T181">äsa-sɨ-t</ta>
            <ta e="T183" id="Seg_6503" s="T182">nɨmtɨ</ta>
            <ta e="T184" id="Seg_6504" s="T183">ilɨ-mpɔː-tɨn</ta>
            <ta e="T185" id="Seg_6505" s="T184">ilɨ-mpɔː-tɨn</ta>
            <ta e="T186" id="Seg_6506" s="T185">nɨnɨ</ta>
            <ta e="T187" id="Seg_6507" s="T186">tɔː</ta>
            <ta e="T188" id="Seg_6508" s="T187">puː-čʼɨ-sɔː-t</ta>
            <ta e="T189" id="Seg_6509" s="T188">tɔː</ta>
            <ta e="T190" id="Seg_6510" s="T189">puː-lä</ta>
            <ta e="T191" id="Seg_6511" s="T190">na</ta>
            <ta e="T192" id="Seg_6512" s="T191">ija</ta>
            <ta e="T193" id="Seg_6513" s="T192">qälɨ</ta>
            <ta e="T194" id="Seg_6514" s="T193">ira</ta>
            <ta e="T195" id="Seg_6515" s="T194">mɔːt-tɨ</ta>
            <ta e="T196" id="Seg_6516" s="T195">qälɨ</ta>
            <ta e="T197" id="Seg_6517" s="T196">ira</ta>
            <ta e="T198" id="Seg_6518" s="T197">ija-tɨ</ta>
            <ta e="T199" id="Seg_6519" s="T198">čʼäŋka</ta>
            <ta e="T200" id="Seg_6520" s="T199">ija-kɨtɨ-lʼ</ta>
            <ta e="T201" id="Seg_6521" s="T200">ima-tɨ</ta>
            <ta e="T202" id="Seg_6522" s="T201">toːnna</ta>
            <ta e="T203" id="Seg_6523" s="T202">čʼonto-lʼ</ta>
            <ta e="T204" id="Seg_6524" s="T203">tɨmnʼa-tɨ</ta>
            <ta e="T206" id="Seg_6525" s="T205">ukkɨr</ta>
            <ta e="T207" id="Seg_6526" s="T206">nälʼa-tɨ</ta>
            <ta e="T208" id="Seg_6527" s="T207">pɛlä-kɨtɨ-j</ta>
            <ta e="T209" id="Seg_6528" s="T208">takkɨ-n</ta>
            <ta e="T210" id="Seg_6529" s="T209">nʼenna</ta>
            <ta e="T211" id="Seg_6530" s="T210">šittɨ</ta>
            <ta e="T212" id="Seg_6531" s="T211">qälɨ-qı</ta>
            <ta e="T213" id="Seg_6532" s="T212">təp</ta>
            <ta e="T214" id="Seg_6533" s="T213">poː-kɨtɨ-lʼ</ta>
            <ta e="T215" id="Seg_6534" s="T214">tətto-t</ta>
            <ta e="T216" id="Seg_6535" s="T215">Ponʼi</ta>
            <ta e="T217" id="Seg_6536" s="T216">ira</ta>
            <ta e="T218" id="Seg_6537" s="T217">Ponʼi</ta>
            <ta e="T219" id="Seg_6538" s="T218">ira</ta>
            <ta e="T220" id="Seg_6539" s="T219">naɛːntɨ</ta>
            <ta e="T221" id="Seg_6540" s="T220">kun</ta>
            <ta e="T222" id="Seg_6541" s="T221">montɨ-lʼ</ta>
            <ta e="T223" id="Seg_6542" s="T222">tamtɨr-tɨ</ta>
            <ta e="T224" id="Seg_6543" s="T223">ɔːtä-p</ta>
            <ta e="T225" id="Seg_6544" s="T224">muntɨk</ta>
            <ta e="T226" id="Seg_6545" s="T225">taqqɨ-š-pa-tɨ</ta>
            <ta e="T227" id="Seg_6546" s="T226">ta-l</ta>
            <ta e="T228" id="Seg_6547" s="T227">nɨlčʼi-k</ta>
            <ta e="T229" id="Seg_6548" s="T228">tü-ŋa</ta>
            <ta e="T230" id="Seg_6549" s="T229">na</ta>
            <ta e="T231" id="Seg_6550" s="T230">timnʼa-sɨ-qä</ta>
            <ta e="T232" id="Seg_6551" s="T231">na</ta>
            <ta e="T233" id="Seg_6552" s="T232">timnʼa-sɨ-qä</ta>
            <ta e="T234" id="Seg_6553" s="T233">ilɔː-qı</ta>
            <ta e="T235" id="Seg_6554" s="T234">na</ta>
            <ta e="T236" id="Seg_6555" s="T235">ira-lʼ</ta>
            <ta e="T237" id="Seg_6556" s="T236">timnʼa-sɨ-qä</ta>
            <ta e="T238" id="Seg_6557" s="T237">šittɨ</ta>
            <ta e="T239" id="Seg_6558" s="T238">tət</ta>
            <ta e="T240" id="Seg_6559" s="T239">toːn</ta>
            <ta e="T241" id="Seg_6560" s="T240">ɔːtä-tɨ</ta>
            <ta e="T242" id="Seg_6561" s="T241">na</ta>
            <ta e="T243" id="Seg_6562" s="T242">ira-n</ta>
            <ta e="T244" id="Seg_6563" s="T243">ɔːtä-m</ta>
            <ta e="T245" id="Seg_6564" s="T244">i-qɨntɔːqı</ta>
            <ta e="T246" id="Seg_6565" s="T245">qum-iː-ntɨ-sa</ta>
            <ta e="T247" id="Seg_6566" s="T246">tü-nta</ta>
            <ta e="T248" id="Seg_6567" s="T247">Poni</ta>
            <ta e="T249" id="Seg_6568" s="T248">ira</ta>
            <ta e="T250" id="Seg_6569" s="T249">nim-tɨ</ta>
            <ta e="T251" id="Seg_6570" s="T250">najntɨ</ta>
            <ta e="T252" id="Seg_6571" s="T251">nɨnɨ</ta>
            <ta e="T253" id="Seg_6572" s="T252">ta-l</ta>
            <ta e="T254" id="Seg_6573" s="T253">tü-la</ta>
            <ta e="T255" id="Seg_6574" s="T254">šäqqɔː-tɨt</ta>
            <ta e="T256" id="Seg_6575" s="T255">na</ta>
            <ta e="T257" id="Seg_6576" s="T256">təp-t-ɨ-n</ta>
            <ta e="T258" id="Seg_6577" s="T257">mɔːt</ta>
            <ta e="T259" id="Seg_6578" s="T258">na</ta>
            <ta e="T260" id="Seg_6579" s="T259">ija-p</ta>
            <ta e="T261" id="Seg_6580" s="T260">qapı</ta>
            <ta e="T262" id="Seg_6581" s="T261">aš</ta>
            <ta e="T263" id="Seg_6582" s="T262">tɛnɨmɔː-t</ta>
            <ta e="T265" id="Seg_6583" s="T264">kuttar</ta>
            <ta e="T266" id="Seg_6584" s="T265">koŋɨ-mm-ɛnta</ta>
            <ta e="T267" id="Seg_6585" s="T266">täp</ta>
            <ta e="T268" id="Seg_6586" s="T267">qaj</ta>
            <ta e="T270" id="Seg_6587" s="T269">mutrona-k</ta>
            <ta e="T272" id="Seg_6588" s="T271">ilʼ</ta>
            <ta e="T273" id="Seg_6589" s="T272">kuttar</ta>
            <ta e="T274" id="Seg_6590" s="T273">ila</ta>
            <ta e="T275" id="Seg_6591" s="T274">tıː</ta>
            <ta e="T276" id="Seg_6592" s="T275">kuttar</ta>
            <ta e="T277" id="Seg_6593" s="T276">tɛnɨ-mm-ɛnta-l</ta>
            <ta e="T278" id="Seg_6594" s="T277">alpä-n</ta>
            <ta e="T279" id="Seg_6595" s="T278">ilä-ntɨj</ta>
            <ta e="T280" id="Seg_6596" s="T279">šäqqɔː-qı</ta>
            <ta e="T281" id="Seg_6597" s="T280">na</ta>
            <ta e="T282" id="Seg_6598" s="T281">qaj</ta>
            <ta e="T283" id="Seg_6599" s="T282">mompa</ta>
            <ta e="T284" id="Seg_6600" s="T283">nɔːr-tälɨj</ta>
            <ta e="T285" id="Seg_6601" s="T284">čʼeːlɨ</ta>
            <ta e="T286" id="Seg_6602" s="T285">toqɨn</ta>
            <ta e="T287" id="Seg_6603" s="T286">mompa</ta>
            <ta e="T288" id="Seg_6604" s="T287">na</ta>
            <ta e="T289" id="Seg_6605" s="T288">tü-ŋɨ</ta>
            <ta e="T290" id="Seg_6606" s="T289">mompa</ta>
            <ta e="T291" id="Seg_6607" s="T290">Poni</ta>
            <ta e="T292" id="Seg_6608" s="T291">ira</ta>
            <ta e="T294" id="Seg_6609" s="T293">köt</ta>
            <ta e="T295" id="Seg_6610" s="T294">qum-sä</ta>
            <ta e="T296" id="Seg_6611" s="T295">tü-mpa</ta>
            <ta e="T297" id="Seg_6612" s="T296">qapı</ta>
            <ta e="T298" id="Seg_6613" s="T297">na</ta>
            <ta e="T299" id="Seg_6614" s="T298">ɔːtä-t</ta>
            <ta e="T300" id="Seg_6615" s="T299">tɔːqqɨ-qɨntɔːqo</ta>
            <ta e="T301" id="Seg_6616" s="T300">na</ta>
            <ta e="T302" id="Seg_6617" s="T301">qum-o-t</ta>
            <ta e="T303" id="Seg_6618" s="T302">nɔːt</ta>
            <ta e="T304" id="Seg_6619" s="T303">na</ta>
            <ta e="T305" id="Seg_6620" s="T304">timnʼa-sɨ-qä-n</ta>
            <ta e="T306" id="Seg_6621" s="T305">na</ta>
            <ta e="T307" id="Seg_6622" s="T306">ɔːtä-p</ta>
            <ta e="T308" id="Seg_6623" s="T307">tɔːqqɨ-mpɔː-t</ta>
            <ta e="T309" id="Seg_6624" s="T308">ku-n</ta>
            <ta e="T310" id="Seg_6625" s="T309">montɨ-lʼ</ta>
            <ta e="T311" id="Seg_6626" s="T310">mɔːt-tɨ</ta>
            <ta e="T312" id="Seg_6627" s="T311">tına</ta>
            <ta e="T313" id="Seg_6628" s="T312">šentɨ</ta>
            <ta e="T314" id="Seg_6629" s="T313">mɔːt-tɨ</ta>
            <ta e="T315" id="Seg_6630" s="T314">ku-n</ta>
            <ta e="T316" id="Seg_6631" s="T315">montɨ-lʼ</ta>
            <ta e="T317" id="Seg_6632" s="T316">qaj-tɨ</ta>
            <ta e="T318" id="Seg_6633" s="T317">muntɨ</ta>
            <ta e="T319" id="Seg_6634" s="T318">taq-qɨl-tɛntɨ-ŋi-t</ta>
            <ta e="T320" id="Seg_6635" s="T319">ukoːn</ta>
            <ta e="T321" id="Seg_6636" s="T320">nɔːta</ta>
            <ta e="T322" id="Seg_6637" s="T321">takkɨ-lʼ</ta>
            <ta e="T323" id="Seg_6638" s="T322">qälɨ-iː-t</ta>
            <ta e="T324" id="Seg_6639" s="T323">ɔːtä-p</ta>
            <ta e="T325" id="Seg_6640" s="T324">taq-qɨl-lä</ta>
            <ta e="T326" id="Seg_6641" s="T325">nɔːtɨ</ta>
            <ta e="T327" id="Seg_6642" s="T326">muntɨk</ta>
            <ta e="T328" id="Seg_6643" s="T327">iː-mpɔː-t</ta>
            <ta e="T329" id="Seg_6644" s="T328">nɨnɨ</ta>
            <ta e="T330" id="Seg_6645" s="T329">šittäl</ta>
            <ta e="T331" id="Seg_6646" s="T330">mɨŋa</ta>
            <ta e="T332" id="Seg_6647" s="T331">moqɨnä</ta>
            <ta e="T333" id="Seg_6648" s="T332">qən-nɔː-t</ta>
            <ta e="T334" id="Seg_6649" s="T333">na</ta>
            <ta e="T335" id="Seg_6650" s="T334">qälɨ-t</ta>
            <ta e="T336" id="Seg_6651" s="T335">tıšša</ta>
            <ta e="T337" id="Seg_6652" s="T336">nɔːr-täl</ta>
            <ta e="T338" id="Seg_6653" s="T337">čʼeːlɨ-t</ta>
            <ta e="T339" id="Seg_6654" s="T338">mompa</ta>
            <ta e="T340" id="Seg_6655" s="T339">tü-ntɔː-mɨn</ta>
            <ta e="T341" id="Seg_6656" s="T340">mɨta</ta>
            <ta e="T342" id="Seg_6657" s="T341">tɛː</ta>
            <ta e="T343" id="Seg_6658" s="T342">mompa</ta>
            <ta e="T344" id="Seg_6659" s="T343">kučʼčʼä-t</ta>
            <ta e="T345" id="Seg_6660" s="T344">ɨkɨ</ta>
            <ta e="T346" id="Seg_6661" s="T345">qən-nɨlɨt</ta>
            <ta e="T347" id="Seg_6662" s="T346">šit-tälɨ</ta>
            <ta e="T348" id="Seg_6663" s="T347">čʼeːlo-ntɨ</ta>
            <ta e="T349" id="Seg_6664" s="T348">na</ta>
            <ta e="T350" id="Seg_6665" s="T349">čʼeːlɨ-n-na</ta>
            <ta e="T351" id="Seg_6666" s="T350">na</ta>
            <ta e="T352" id="Seg_6667" s="T351">ija</ta>
            <ta e="T353" id="Seg_6668" s="T352">nılʼčʼi-ŋ</ta>
            <ta e="T354" id="Seg_6669" s="T353">ɛsa</ta>
            <ta e="T355" id="Seg_6670" s="T354">na</ta>
            <ta e="T356" id="Seg_6671" s="T355">ilʼčʼa-ntɨ-nɨk</ta>
            <ta e="T357" id="Seg_6672" s="T356">na</ta>
            <ta e="T358" id="Seg_6673" s="T357">gostʼ-a-n</ta>
            <ta e="T359" id="Seg_6674" s="T358">ilʼčʼa-nt</ta>
            <ta e="T360" id="Seg_6675" s="T359">mɨta</ta>
            <ta e="T361" id="Seg_6676" s="T360">nʼenna</ta>
            <ta e="T362" id="Seg_6677" s="T361">tɨmtɨ</ta>
            <ta e="T363" id="Seg_6678" s="T362">mɨ</ta>
            <ta e="T364" id="Seg_6679" s="T363">ɛː-nta</ta>
            <ta e="T365" id="Seg_6680" s="T364">mɨta</ta>
            <ta e="T366" id="Seg_6681" s="T365">tɛːt</ta>
            <ta e="T367" id="Seg_6682" s="T366">toːn</ta>
            <ta e="T368" id="Seg_6683" s="T367">ɔːtä-n</ta>
            <ta e="T369" id="Seg_6684" s="T368">nɨŋ-qɨ-ku-ššaŋ</ta>
            <ta e="T370" id="Seg_6685" s="T369">mɨta</ta>
            <ta e="T371" id="Seg_6686" s="T370">nʼaraŋ</ta>
            <ta e="T372" id="Seg_6687" s="T371">ɛː-ntɨ</ta>
            <ta e="T373" id="Seg_6688" s="T372">mɨta</ta>
            <ta e="T374" id="Seg_6689" s="T373">näčʼčʼa-tɨ</ta>
            <ta e="T375" id="Seg_6690" s="T374">üːta-lɔː-mɨn</ta>
            <ta e="T376" id="Seg_6691" s="T375">mɨta</ta>
            <ta e="T377" id="Seg_6692" s="T376">qälɨ-t</ta>
            <ta e="T378" id="Seg_6693" s="T377">tü-ntɔː-tɨn</ta>
            <ta e="T379" id="Seg_6694" s="T378">nɨntɨ</ta>
            <ta e="T380" id="Seg_6695" s="T379">ɔːtä-p</ta>
            <ta e="T381" id="Seg_6696" s="T380">tɔːqqɨ-q-olam-nɛntɔː-tɨn</ta>
            <ta e="T382" id="Seg_6697" s="T381">nın</ta>
            <ta e="T383" id="Seg_6698" s="T382">mɨta</ta>
            <ta e="T384" id="Seg_6699" s="T383">tına</ta>
            <ta e="T385" id="Seg_6700" s="T384">ɔːtä-n</ta>
            <ta e="T386" id="Seg_6701" s="T385">up-tɨ</ta>
            <ta e="T387" id="Seg_6702" s="T386">kə-lʼčʼomɨnto</ta>
            <ta e="T388" id="Seg_6703" s="T387">kə-rɨ-mpɨlʼ</ta>
            <ta e="T389" id="Seg_6704" s="T388">mɨta</ta>
            <ta e="T390" id="Seg_6705" s="T389">kučʼčʼa</ta>
            <ta e="T391" id="Seg_6706" s="T390">meː-ntɔː-mɨn</ta>
            <ta e="T392" id="Seg_6707" s="T391">na</ta>
            <ta e="T393" id="Seg_6708" s="T392">ija</ta>
            <ta e="T394" id="Seg_6709" s="T393">nık</ta>
            <ta e="T395" id="Seg_6710" s="T394">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T396" id="Seg_6711" s="T395">nɨnɨ</ta>
            <ta e="T397" id="Seg_6712" s="T396">nık</ta>
            <ta e="T398" id="Seg_6713" s="T397">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T399" id="Seg_6714" s="T398">mɨta</ta>
            <ta e="T401" id="Seg_6715" s="T400">mäkka</ta>
            <ta e="T402" id="Seg_6716" s="T401">mi-tɨ</ta>
            <ta e="T403" id="Seg_6717" s="T402">šentɨ</ta>
            <ta e="T404" id="Seg_6718" s="T403">šentɨ</ta>
            <ta e="T405" id="Seg_6719" s="T404">mɔːl-lɨ</ta>
            <ta e="T406" id="Seg_6720" s="T405">ɛː-ŋa</ta>
            <ta e="T407" id="Seg_6721" s="T406">ilʼčʼa</ta>
            <ta e="T408" id="Seg_6722" s="T407">mɨta</ta>
            <ta e="T409" id="Seg_6723" s="T408">qaj</ta>
            <ta e="T410" id="Seg_6724" s="T409">šentɨ</ta>
            <ta e="T411" id="Seg_6725" s="T410">mɔːl-lɨ</ta>
            <ta e="T412" id="Seg_6726" s="T411">ɛː-ŋa</ta>
            <ta e="T413" id="Seg_6727" s="T412">mäkka</ta>
            <ta e="T414" id="Seg_6728" s="T413">mi-tɨ</ta>
            <ta e="T415" id="Seg_6729" s="T414">mɨta</ta>
            <ta e="T416" id="Seg_6730" s="T415">a</ta>
            <ta e="T417" id="Seg_6731" s="T416">toːnna</ta>
            <ta e="T418" id="Seg_6732" s="T417">tına</ta>
            <ta e="T419" id="Seg_6733" s="T418">timnʼa-n-tɨ</ta>
            <ta e="T420" id="Seg_6734" s="T419">nälʼa</ta>
            <ta e="T421" id="Seg_6735" s="T420">ilʼčʼa-tɨ</ta>
            <ta e="T422" id="Seg_6736" s="T421">namɨ-p</ta>
            <ta e="T423" id="Seg_6737" s="T422">qur-altɨ-mmɨ-ntɨ-t</ta>
            <ta e="T424" id="Seg_6738" s="T423">na</ta>
            <ta e="T425" id="Seg_6739" s="T424">ija-nɨk</ta>
            <ta e="T426" id="Seg_6740" s="T425">man</ta>
            <ta e="T427" id="Seg_6741" s="T426">mɨta</ta>
            <ta e="T428" id="Seg_6742" s="T427">prosto</ta>
            <ta e="T429" id="Seg_6743" s="T428">məčʼi-m-nɛnta-k</ta>
            <ta e="T430" id="Seg_6744" s="T429">mɔːt</ta>
            <ta e="T431" id="Seg_6745" s="T430">nʼenna-t</ta>
            <ta e="T432" id="Seg_6746" s="T431">mɨntɨ</ta>
            <ta e="T433" id="Seg_6747" s="T432">mačʼi-lʼ</ta>
            <ta e="T434" id="Seg_6748" s="T433">tıčʼi</ta>
            <ta e="T435" id="Seg_6749" s="T434">ɛ-nta</ta>
            <ta e="T436" id="Seg_6750" s="T435">na</ta>
            <ta e="T437" id="Seg_6751" s="T436">nʼennä-lʼ</ta>
            <ta e="T438" id="Seg_6752" s="T437">pɛlak-tɨ</ta>
            <ta e="T439" id="Seg_6753" s="T438">mɔːt-ɨ-ŋ-nɛnta-k</ta>
            <ta e="T440" id="Seg_6754" s="T439">šentɨ</ta>
            <ta e="T441" id="Seg_6755" s="T440">mɔːt</ta>
            <ta e="T442" id="Seg_6756" s="T441">taːtɨ-ŋa</ta>
            <ta e="T443" id="Seg_6757" s="T442">nɛntɨ</ta>
            <ta e="T444" id="Seg_6758" s="T443">üːt-al-nɔː-tɨt</ta>
            <ta e="T445" id="Seg_6759" s="T444">ta-l</ta>
            <ta e="T446" id="Seg_6760" s="T445">üːt-al-tɔː-t</ta>
            <ta e="T447" id="Seg_6761" s="T446">na</ta>
            <ta e="T448" id="Seg_6762" s="T447">qapija</ta>
            <ta e="T449" id="Seg_6763" s="T448">nuːnɨčʼɨ-sɨ</ta>
            <ta e="T450" id="Seg_6764" s="T449">šittɨ-mtäl</ta>
            <ta e="T451" id="Seg_6765" s="T450">nɔːr-täl</ta>
            <ta e="T452" id="Seg_6766" s="T451">čʼeːlɨ</ta>
            <ta e="T453" id="Seg_6767" s="T452">tü-ntɔː-t</ta>
            <ta e="T454" id="Seg_6768" s="T453">Poni</ta>
            <ta e="T455" id="Seg_6769" s="T454">ira-lʼ</ta>
            <ta e="T456" id="Seg_6770" s="T455">mɨ</ta>
            <ta e="T457" id="Seg_6771" s="T456">təp-ɨ-t-ɨ-tkin</ta>
            <ta e="T458" id="Seg_6772" s="T457">na</ta>
            <ta e="T459" id="Seg_6773" s="T458">qälɨ</ta>
            <ta e="T460" id="Seg_6774" s="T459">ira-lʼ</ta>
            <ta e="T461" id="Seg_6775" s="T460">mɨ-qä</ta>
            <ta e="T462" id="Seg_6776" s="T461">nɔːtɨ</ta>
            <ta e="T463" id="Seg_6777" s="T462">taq-al-qɨnto</ta>
            <ta e="T464" id="Seg_6778" s="T463">timnʼa-sɨ-qä</ta>
            <ta e="T465" id="Seg_6779" s="T464">nɔːtɨ</ta>
            <ta e="T466" id="Seg_6780" s="T465">tap</ta>
            <ta e="T467" id="Seg_6781" s="T466">čʼeːl-qot</ta>
            <ta e="T468" id="Seg_6782" s="T467">nɨnɨ</ta>
            <ta e="T469" id="Seg_6783" s="T468">šʼittalʼ</ta>
            <ta e="T470" id="Seg_6784" s="T469">üːt-al-tɔː-t</ta>
            <ta e="T471" id="Seg_6785" s="T470">na</ta>
            <ta e="T472" id="Seg_6786" s="T471">ɔːta-m</ta>
            <ta e="T473" id="Seg_6787" s="T472">na</ta>
            <ta e="T474" id="Seg_6788" s="T473">tɔːqqɨ-ntɔː-t</ta>
            <ta e="T475" id="Seg_6789" s="T474">qapij</ta>
            <ta e="T476" id="Seg_6790" s="T475">tɛm</ta>
            <ta e="T477" id="Seg_6791" s="T476">ɛj</ta>
            <ta e="T479" id="Seg_6792" s="T478">mɔːs-sa</ta>
            <ta e="T480" id="Seg_6793" s="T479">na</ta>
            <ta e="T481" id="Seg_6794" s="T480">mi-ntɔː-t</ta>
            <ta e="T482" id="Seg_6795" s="T481">mɔːt</ta>
            <ta e="T483" id="Seg_6796" s="T482">šer-na</ta>
            <ta e="T484" id="Seg_6797" s="T483">na</ta>
            <ta e="T485" id="Seg_6798" s="T484">mačʼi-t</ta>
            <ta e="T486" id="Seg_6799" s="T485">tıčʼɨ-t</ta>
            <ta e="T487" id="Seg_6800" s="T486">jenna-lʼ</ta>
            <ta e="T488" id="Seg_6801" s="T487">pɛla-lʼ</ta>
            <ta e="T489" id="Seg_6802" s="T488">mɔːt</ta>
            <ta e="T490" id="Seg_6803" s="T489">šer-na</ta>
            <ta e="T491" id="Seg_6804" s="T490">nɨn</ta>
            <ta e="T492" id="Seg_6805" s="T491">na</ta>
            <ta e="T493" id="Seg_6806" s="T492">mɔːt-ɨ-ŋ-tɔː-qı</ta>
            <ta e="T494" id="Seg_6807" s="T493">na</ta>
            <ta e="T495" id="Seg_6808" s="T494">ima-ntɨ-sa</ta>
            <ta e="T496" id="Seg_6809" s="T495">qapı</ta>
            <ta e="T497" id="Seg_6810" s="T496">tətta-lʼ</ta>
            <ta e="T993" id="Seg_6811" s="T497">qälʼ</ta>
            <ta e="T498" id="Seg_6812" s="T993">ira</ta>
            <ta e="T499" id="Seg_6813" s="T498">qaj</ta>
            <ta e="T500" id="Seg_6814" s="T499">mɔːt-tɨ</ta>
            <ta e="T501" id="Seg_6815" s="T500">čʼäŋka</ta>
            <ta e="T502" id="Seg_6816" s="T501">šentɨ</ta>
            <ta e="T503" id="Seg_6817" s="T502">mɔːt-tɨ</ta>
            <ta e="T504" id="Seg_6818" s="T503">qopɨ-lʼ</ta>
            <ta e="T505" id="Seg_6819" s="T504">mɔːt-tɨ</ta>
            <ta e="T506" id="Seg_6820" s="T505">tına</ta>
            <ta e="T507" id="Seg_6821" s="T506">wərqɨ-lɔː</ta>
            <ta e="T975" id="Seg_6822" s="T507">čʼesɨ-ŋɨ-tɨ</ta>
            <ta e="T976" id="Seg_6823" s="T509">koŋɨ-mp-äš</ta>
            <ta e="T512" id="Seg_6824" s="T511">na</ta>
            <ta e="T513" id="Seg_6825" s="T512">ılla-lɔː</ta>
            <ta e="T514" id="Seg_6826" s="T513">uːk-al-t-äš</ta>
            <ta e="T517" id="Seg_6827" s="T516">na</ta>
            <ta e="T518" id="Seg_6828" s="T517">mantɨ</ta>
            <ta e="T520" id="Seg_6829" s="T519">soma-ŋ</ta>
            <ta e="T521" id="Seg_6830" s="T520">na</ta>
            <ta e="T522" id="Seg_6831" s="T521">koŋɨ-mmɨ-ntɨ</ta>
            <ta e="T523" id="Seg_6832" s="T522">na</ta>
            <ta e="T980" id="Seg_6833" s="T523">koŋ-altɨ-mmɨ-ntɨ</ta>
            <ta e="T525" id="Seg_6834" s="T524">ta-l</ta>
            <ta e="T526" id="Seg_6835" s="T525">ɨkɨ</ta>
            <ta e="T981" id="Seg_6836" s="T526">mort-äš</ta>
            <ta e="T528" id="Seg_6837" s="T527">qapija</ta>
            <ta e="T529" id="Seg_6838" s="T528">qoː-kɨta-lʼ</ta>
            <ta e="T531" id="Seg_6839" s="T530">qapı</ta>
            <ta e="T532" id="Seg_6840" s="T531">üŋkɨl-tɨ-mpa-tɨ</ta>
            <ta e="T533" id="Seg_6841" s="T532">moʒetpɨtʼ</ta>
            <ta e="T534" id="Seg_6842" s="T533">qoškɔːl</ta>
            <ta e="T983" id="Seg_6843" s="T534">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T974" id="Seg_6844" s="T535">əːtɨ-kɔːl</ta>
            <ta e="T537" id="Seg_6845" s="T536">nɨnɨ</ta>
            <ta e="T538" id="Seg_6846" s="T537">šittäl</ta>
            <ta e="T539" id="Seg_6847" s="T538">šäqqɔː-j</ta>
            <ta e="T540" id="Seg_6848" s="T539">na</ta>
            <ta e="T541" id="Seg_6849" s="T540">ija</ta>
            <ta e="T542" id="Seg_6850" s="T541">ima-ntɨ-sa</ta>
            <ta e="T543" id="Seg_6851" s="T542">šäqqɔː-t</ta>
            <ta e="T544" id="Seg_6852" s="T543">top-tɨlʼ</ta>
            <ta e="T545" id="Seg_6853" s="T544">qar</ta>
            <ta e="T546" id="Seg_6854" s="T545">nı-lʼ</ta>
            <ta e="T547" id="Seg_6855" s="T546">ɛsa</ta>
            <ta e="T548" id="Seg_6856" s="T547">tına</ta>
            <ta e="T549" id="Seg_6857" s="T548">ima-ntɨ-n</ta>
            <ta e="T550" id="Seg_6858" s="T549">man</ta>
            <ta e="T551" id="Seg_6859" s="T550">mɨta</ta>
            <ta e="T552" id="Seg_6860" s="T551">nʼenna</ta>
            <ta e="T553" id="Seg_6861" s="T552">qum-iː-qan</ta>
            <ta e="T554" id="Seg_6862" s="T553">qonʼi-ša-k</ta>
            <ta e="T555" id="Seg_6863" s="T554">qaj</ta>
            <ta e="T556" id="Seg_6864" s="T555">mɨta</ta>
            <ta e="T557" id="Seg_6865" s="T556">mɔːt-ɨ-ŋ-pɔː-tɨn</ta>
            <ta e="T558" id="Seg_6866" s="T557">qaj</ta>
            <ta e="T559" id="Seg_6867" s="T558">qattɨ-mpɔː-tɨn</ta>
            <ta e="T560" id="Seg_6868" s="T559">nʼenna</ta>
            <ta e="T561" id="Seg_6869" s="T560">laqalt-ɛlʼčʼa</ta>
            <ta e="T562" id="Seg_6870" s="T561">nɔːkɨr</ta>
            <ta e="T563" id="Seg_6871" s="T562">qoptɨ-p</ta>
            <ta e="T564" id="Seg_6872" s="T563">sɔːrɨ-la</ta>
            <ta e="T565" id="Seg_6873" s="T564">qapı</ta>
            <ta e="T566" id="Seg_6874" s="T565">nɔːs-sar-ɨ-lʼ</ta>
            <ta e="T567" id="Seg_6875" s="T566">ɔːta</ta>
            <ta e="T568" id="Seg_6876" s="T567">qal-tɨ-mɨ-ntɨ-t</ta>
            <ta e="T569" id="Seg_6877" s="T568">na</ta>
            <ta e="T570" id="Seg_6878" s="T569">ija</ta>
            <ta e="T571" id="Seg_6879" s="T570">nʼenna</ta>
            <ta e="T572" id="Seg_6880" s="T571">qən-ai-sɨ</ta>
            <ta e="T573" id="Seg_6881" s="T572">ima-tɨ</ta>
            <ta e="T574" id="Seg_6882" s="T573">ontɨ</ta>
            <ta e="T575" id="Seg_6883" s="T574">qala</ta>
            <ta e="T576" id="Seg_6884" s="T575">pɛlä-kɔːl</ta>
            <ta e="T577" id="Seg_6885" s="T576">na</ta>
            <ta e="T578" id="Seg_6886" s="T577">ɔːmtɨ-ŋa</ta>
            <ta e="T579" id="Seg_6887" s="T578">ompa</ta>
            <ta e="T580" id="Seg_6888" s="T579">na</ta>
            <ta e="T582" id="Seg_6889" s="T581">ukkɨr</ta>
            <ta e="T583" id="Seg_6890" s="T582">to-t</ta>
            <ta e="T584" id="Seg_6891" s="T583">čʼonto-t</ta>
            <ta e="T585" id="Seg_6892" s="T584">čʼeːlɨ-tɨ</ta>
            <ta e="T586" id="Seg_6893" s="T585">üːto-ntɨ</ta>
            <ta e="T587" id="Seg_6894" s="T586">na</ta>
            <ta e="T588" id="Seg_6895" s="T587">qən-tɨ</ta>
            <ta e="T589" id="Seg_6896" s="T588">üːto-ntɨ-qɨn</ta>
            <ta e="T590" id="Seg_6897" s="T589">na</ta>
            <ta e="T591" id="Seg_6898" s="T590">ija</ta>
            <ta e="T592" id="Seg_6899" s="T591">lʼamɨ-k</ta>
            <ta e="T593" id="Seg_6900" s="T592">ɛː-ŋa</ta>
            <ta e="T594" id="Seg_6901" s="T593">qapı</ta>
            <ta e="T595" id="Seg_6902" s="T594">nʼenna</ta>
            <ta e="T596" id="Seg_6903" s="T595">ukkɨr</ta>
            <ta e="T597" id="Seg_6904" s="T596">tä-t</ta>
            <ta e="T598" id="Seg_6905" s="T597">čʼonto-t</ta>
            <ta e="T599" id="Seg_6906" s="T598">mɨ</ta>
            <ta e="T600" id="Seg_6907" s="T599">qum</ta>
            <ta e="T601" id="Seg_6908" s="T600">na</ta>
            <ta e="T602" id="Seg_6909" s="T601">jap</ta>
            <ta e="T603" id="Seg_6910" s="T602">tattɨ-ntɔː-t</ta>
            <ta e="T604" id="Seg_6911" s="T603">takkɨ-ntɨt</ta>
            <ta e="T605" id="Seg_6912" s="T604">Poni</ta>
            <ta e="T606" id="Seg_6913" s="T605">ira-lʼ</ta>
            <ta e="T967" id="Seg_6914" s="T606">na</ta>
            <ta e="T607" id="Seg_6915" s="T967">jap</ta>
            <ta e="T608" id="Seg_6916" s="T607">tattɨ-ntɔː-tɨt</ta>
            <ta e="T609" id="Seg_6917" s="T608">köt</ta>
            <ta e="T610" id="Seg_6918" s="T609">qum-iː-tɨ</ta>
            <ta e="T611" id="Seg_6919" s="T610">qapı</ta>
            <ta e="T612" id="Seg_6920" s="T611">ɔːta-p</ta>
            <ta e="T613" id="Seg_6921" s="T612">tɔːqqɨ-mpɨlʼ</ta>
            <ta e="T614" id="Seg_6922" s="T613">Poni</ta>
            <ta e="T615" id="Seg_6923" s="T614">ira</ta>
            <ta e="T616" id="Seg_6924" s="T615">naššak</ta>
            <ta e="T617" id="Seg_6925" s="T616">qo-tɨlʼ</ta>
            <ta e="T618" id="Seg_6926" s="T617">ɛː-ŋa</ta>
            <ta e="T619" id="Seg_6927" s="T618">naššak</ta>
            <ta e="T620" id="Seg_6928" s="T619">qəːtɨ-sä</ta>
            <ta e="T621" id="Seg_6929" s="T620">ɛː-ŋa</ta>
            <ta e="T622" id="Seg_6930" s="T621">nɨnɨ</ta>
            <ta e="T623" id="Seg_6931" s="T622">šittäl</ta>
            <ta e="T624" id="Seg_6932" s="T623">ta-l</ta>
            <ta e="T625" id="Seg_6933" s="T624">na</ta>
            <ta e="T626" id="Seg_6934" s="T625">ija</ta>
            <ta e="T627" id="Seg_6935" s="T626">tü-lʼčʼi-kunä</ta>
            <ta e="T628" id="Seg_6936" s="T627">ponä</ta>
            <ta e="T629" id="Seg_6937" s="T628">ima-tɨ</ta>
            <ta e="T630" id="Seg_6938" s="T629">üŋkɨl-tɨ-mpa</ta>
            <ta e="T631" id="Seg_6939" s="T630">tü-lʼčʼi-kunä</ta>
            <ta e="T632" id="Seg_6940" s="T631">na</ta>
            <ta e="T633" id="Seg_6941" s="T632">ɔːta-m-tɨ</ta>
            <ta e="T634" id="Seg_6942" s="T633">toː</ta>
            <ta e="T635" id="Seg_6943" s="T634">sɔːra</ta>
            <ta e="T636" id="Seg_6944" s="T635">ɔːta-m-tɨ</ta>
            <ta e="T638" id="Seg_6945" s="T637">montɨ</ta>
            <ta e="T639" id="Seg_6946" s="T638">čʼap</ta>
            <ta e="T640" id="Seg_6947" s="T639">tü-lʼčʼa</ta>
            <ta e="T641" id="Seg_6948" s="T640">montɨ</ta>
            <ta e="T642" id="Seg_6949" s="T641">Poni</ta>
            <ta e="T643" id="Seg_6950" s="T642">ira-lʼ</ta>
            <ta e="T644" id="Seg_6951" s="T643">mɨ-t</ta>
            <ta e="T645" id="Seg_6952" s="T644">qaj</ta>
            <ta e="T646" id="Seg_6953" s="T645">montɨ</ta>
            <ta e="T647" id="Seg_6954" s="T646">tü-ŋɔː-tɨt</ta>
            <ta e="T648" id="Seg_6955" s="T647">qaqlɨ-lʼ</ta>
            <ta e="T649" id="Seg_6956" s="T648">mɨ-t-ɨ-t</ta>
            <ta e="T650" id="Seg_6957" s="T649">nık</ta>
            <ta e="T651" id="Seg_6958" s="T650">tott-älɨ-mpɔː-t</ta>
            <ta e="T652" id="Seg_6959" s="T651">tına</ta>
            <ta e="T653" id="Seg_6960" s="T652">ətɨmanto-t</ta>
            <ta e="T654" id="Seg_6961" s="T653">təp-ɨ-t</ta>
            <ta e="T655" id="Seg_6962" s="T654">ətɨmanto-t</ta>
            <ta e="T656" id="Seg_6963" s="T655">šer-na</ta>
            <ta e="T657" id="Seg_6964" s="T656">mɔːt</ta>
            <ta e="T658" id="Seg_6965" s="T657">šer-na</ta>
            <ta e="T659" id="Seg_6966" s="T658">mɔːta-n</ta>
            <ta e="T660" id="Seg_6967" s="T659">ɔː-t</ta>
            <ta e="T661" id="Seg_6968" s="T660">nɨŋ-kɨ-la</ta>
            <ta e="T662" id="Seg_6969" s="T661">qälɨk</ta>
            <ta e="T663" id="Seg_6970" s="T662">tokalʼ</ta>
            <ta e="T664" id="Seg_6971" s="T663">mɔːta-n</ta>
            <ta e="T665" id="Seg_6972" s="T664">ɔː-t</ta>
            <ta e="T666" id="Seg_6973" s="T665">qät</ta>
            <ta e="T667" id="Seg_6974" s="T666">topɨ-m-t</ta>
            <ta e="T668" id="Seg_6975" s="T667">tup-al-na</ta>
            <ta e="T669" id="Seg_6976" s="T668">peːmɨ-m-tɨ</ta>
            <ta e="T670" id="Seg_6977" s="T669">na</ta>
            <ta e="T671" id="Seg_6978" s="T670">tup-al-tɨ</ta>
            <ta e="T672" id="Seg_6979" s="T671">qannɨ-mpɨlʼ</ta>
            <ta e="T673" id="Seg_6980" s="T672">pɛla-lʼ</ta>
            <ta e="T674" id="Seg_6981" s="T673">peːmɨ-tɨ</ta>
            <ta e="T675" id="Seg_6982" s="T674">tup-al-nɨ-t</ta>
            <ta e="T676" id="Seg_6983" s="T675">nık</ta>
            <ta e="T677" id="Seg_6984" s="T676">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T678" id="Seg_6985" s="T677">nʼenna</ta>
            <ta e="T679" id="Seg_6986" s="T678">kor-al-la</ta>
            <ta e="T680" id="Seg_6987" s="T679">Poni</ta>
            <ta e="T681" id="Seg_6988" s="T680">ira-n</ta>
            <ta e="T682" id="Seg_6989" s="T681">mɨta</ta>
            <ta e="T683" id="Seg_6990" s="T682">ɔːk-lɨ</ta>
            <ta e="T684" id="Seg_6991" s="T683">mitɨ</ta>
            <ta e="T685" id="Seg_6992" s="T684">mɨta</ta>
            <ta e="T686" id="Seg_6993" s="T685">qolqo-l</ta>
            <ta e="T687" id="Seg_6994" s="T686">olʼa</ta>
            <ta e="T688" id="Seg_6995" s="T687">mɨ</ta>
            <ta e="T689" id="Seg_6996" s="T688">taŋa-l</ta>
            <ta e="T690" id="Seg_6997" s="T689">na</ta>
            <ta e="T691" id="Seg_6998" s="T690">nʼenna</ta>
            <ta e="T692" id="Seg_6999" s="T691">paktɨ-mmɨ-ntɨ</ta>
            <ta e="T693" id="Seg_7000" s="T692">na</ta>
            <ta e="T694" id="Seg_7001" s="T693">kupak-s</ta>
            <ta e="T695" id="Seg_7002" s="T694">təːq-al-ai-mmɨ-ntɨ-tɨ</ta>
            <ta e="T696" id="Seg_7003" s="T695">pɛla-j</ta>
            <ta e="T697" id="Seg_7004" s="T696">utɨ-n-tɨ</ta>
            <ta e="T698" id="Seg_7005" s="T697">təːq-al-ai-mpa-t</ta>
            <ta e="T699" id="Seg_7006" s="T698">Poni</ta>
            <ta e="T700" id="Seg_7007" s="T699">ira-lʼ</ta>
            <ta e="T701" id="Seg_7008" s="T700">mɨ-t-ɨ-p</ta>
            <ta e="T702" id="Seg_7009" s="T701">muntɨk</ta>
            <ta e="T703" id="Seg_7010" s="T702">lɛpäk</ta>
            <ta e="T704" id="Seg_7011" s="T703">qättɨ-mpa-tɨ</ta>
            <ta e="T705" id="Seg_7012" s="T704">ɔːmɨ-tɨ</ta>
            <ta e="T706" id="Seg_7013" s="T705">qu-mpa</ta>
            <ta e="T707" id="Seg_7014" s="T706">Poni</ta>
            <ta e="T708" id="Seg_7015" s="T707">ira</ta>
            <ta e="T709" id="Seg_7016" s="T708">il-la</ta>
            <ta e="T710" id="Seg_7017" s="T709">qalɨ-m-tɨ</ta>
            <ta e="T711" id="Seg_7018" s="T710">šittɨ</ta>
            <ta e="T712" id="Seg_7019" s="T711">qälɨ-qı</ta>
            <ta e="T713" id="Seg_7020" s="T712">il-la</ta>
            <ta e="T714" id="Seg_7021" s="T713">qalɨ-m-tɨ</ta>
            <ta e="T715" id="Seg_7022" s="T714">pona</ta>
            <ta e="T716" id="Seg_7023" s="T715">qəː-ll-aıː-ntɨ</ta>
            <ta e="T717" id="Seg_7024" s="T716">na</ta>
            <ta e="T718" id="Seg_7025" s="T717">ija</ta>
            <ta e="T719" id="Seg_7026" s="T718">ukkɨr</ta>
            <ta e="T720" id="Seg_7027" s="T719">pɔːr</ta>
            <ta e="T721" id="Seg_7028" s="T720">kupak-sä</ta>
            <ta e="T722" id="Seg_7029" s="T721">təːq-al-ai-mpa-t</ta>
            <ta e="T723" id="Seg_7030" s="T722">muntɨk</ta>
            <ta e="T724" id="Seg_7031" s="T723">moqɨnä-t</ta>
            <ta e="T725" id="Seg_7032" s="T724">porqɨ-n</ta>
            <ta e="T726" id="Seg_7033" s="T725">šunʼnʼo-ntɨ</ta>
            <ta e="T727" id="Seg_7034" s="T726">nʼolʼqɨ-mɔːt-pɔː-tɨn</ta>
            <ta e="T728" id="Seg_7035" s="T727">na</ta>
            <ta e="T729" id="Seg_7036" s="T728">qu-mpɨlʼ</ta>
            <ta e="T730" id="Seg_7037" s="T729">mɨ</ta>
            <ta e="T731" id="Seg_7038" s="T730">pista</ta>
            <ta e="T732" id="Seg_7039" s="T731">nɨnɨ</ta>
            <ta e="T733" id="Seg_7040" s="T732">šittäl</ta>
            <ta e="T734" id="Seg_7041" s="T733">ponä</ta>
            <ta e="T735" id="Seg_7042" s="T734">qə-ll-aıː-ŋɨ-tɨ</ta>
            <ta e="T736" id="Seg_7043" s="T735">ətɨmanto-ntɨ</ta>
            <ta e="T737" id="Seg_7044" s="T736">tınta</ta>
            <ta e="T738" id="Seg_7045" s="T737">na</ta>
            <ta e="T739" id="Seg_7046" s="T738">qaqlɨ-t</ta>
            <ta e="T742" id="Seg_7047" s="T741">poː-qɨn</ta>
            <ta e="T743" id="Seg_7048" s="T742">na</ta>
            <ta e="T744" id="Seg_7049" s="T743">sɔːr-alɨ-mmɨ-ntɔː-tɨt</ta>
            <ta e="T745" id="Seg_7050" s="T744">Poni</ta>
            <ta e="T746" id="Seg_7051" s="T745">ira</ta>
            <ta e="T747" id="Seg_7052" s="T746">ɛj</ta>
            <ta e="T748" id="Seg_7053" s="T747">ponä</ta>
            <ta e="T749" id="Seg_7054" s="T748">iː-tɨ-mpa-tɨ</ta>
            <ta e="T750" id="Seg_7055" s="T749">qaj</ta>
            <ta e="T751" id="Seg_7056" s="T750">muntɨk</ta>
            <ta e="T752" id="Seg_7057" s="T751">ponä</ta>
            <ta e="T753" id="Seg_7058" s="T752">iː-tɨ-mpa-tɨ</ta>
            <ta e="T754" id="Seg_7059" s="T753">i̇lla</ta>
            <ta e="T755" id="Seg_7060" s="T754">omta</ta>
            <ta e="T756" id="Seg_7061" s="T755">tına</ta>
            <ta e="T757" id="Seg_7062" s="T756">malʼčʼa-lʼ</ta>
            <ta e="T758" id="Seg_7063" s="T757">tok-tɨ</ta>
            <ta e="T759" id="Seg_7064" s="T758">ılla</ta>
            <ta e="T760" id="Seg_7065" s="T759">am-ɨ-r-la</ta>
            <ta e="T762" id="Seg_7066" s="T761">ılla</ta>
            <ta e="T763" id="Seg_7067" s="T762">šäqqa</ta>
            <ta e="T764" id="Seg_7068" s="T763">na</ta>
            <ta e="T765" id="Seg_7069" s="T764">qum-iː-tɨ</ta>
            <ta e="T766" id="Seg_7070" s="T765">poː-qɨn</ta>
            <ta e="T767" id="Seg_7071" s="T766">na</ta>
            <ta e="T768" id="Seg_7072" s="T767">ipp-älɨ-mmɨ-ntɔː-t</ta>
            <ta e="T769" id="Seg_7073" s="T768">nɔːtɨ</ta>
            <ta e="T770" id="Seg_7074" s="T769">sɔːrɨ-mp-kol</ta>
            <ta e="T771" id="Seg_7075" s="T770">kuttar</ta>
            <ta e="T772" id="Seg_7076" s="T771">sɔːr-alɨ-mpa</ta>
            <ta e="T773" id="Seg_7077" s="T772">nılʼčʼi-k</ta>
            <ta e="T774" id="Seg_7078" s="T773">šäqqa</ta>
            <ta e="T775" id="Seg_7079" s="T774">top-tɨlʼ</ta>
            <ta e="T776" id="Seg_7080" s="T775">qar</ta>
            <ta e="T777" id="Seg_7081" s="T776">ınna</ta>
            <ta e="T778" id="Seg_7082" s="T777">wəša</ta>
            <ta e="T779" id="Seg_7083" s="T778">Poni</ta>
            <ta e="T780" id="Seg_7084" s="T779">ira</ta>
            <ta e="T781" id="Seg_7085" s="T780">nılʼčʼi</ta>
            <ta e="T782" id="Seg_7086" s="T781">mɨta</ta>
            <ta e="T783" id="Seg_7087" s="T782">məntɨ-lʼ</ta>
            <ta e="T784" id="Seg_7088" s="T783">olɨ-tɨ</ta>
            <ta e="T785" id="Seg_7089" s="T784">tot</ta>
            <ta e="T786" id="Seg_7090" s="T785">taŋkɨ-ıː-mpa</ta>
            <ta e="T787" id="Seg_7091" s="T786">tına</ta>
            <ta e="T788" id="Seg_7092" s="T787">keksa</ta>
            <ta e="T789" id="Seg_7093" s="T788">sai-ja-tɨ</ta>
            <ta e="T791" id="Seg_7094" s="T790">toːnna</ta>
            <ta e="T792" id="Seg_7095" s="T791">qälɨ-t</ta>
            <ta e="T794" id="Seg_7096" s="T793">iše</ta>
            <ta e="T795" id="Seg_7097" s="T794">soma-lɔːqɨ</ta>
            <ta e="T796" id="Seg_7098" s="T795">ɛ-ppa</ta>
            <ta e="T797" id="Seg_7099" s="T796">nı-lʼ</ta>
            <ta e="T798" id="Seg_7100" s="T797">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T799" id="Seg_7101" s="T798">na</ta>
            <ta e="T800" id="Seg_7102" s="T799">ija</ta>
            <ta e="T801" id="Seg_7103" s="T800">mɨta</ta>
            <ta e="T802" id="Seg_7104" s="T801">na</ta>
            <ta e="T803" id="Seg_7105" s="T802">qälɨ-t-ɨ-tkini-tɨ</ta>
            <ta e="T804" id="Seg_7106" s="T803">qum-iː-m-tɨ</ta>
            <ta e="T805" id="Seg_7107" s="T804">moqɨnä</ta>
            <ta e="T806" id="Seg_7108" s="T805">qən-t-ät</ta>
            <ta e="T807" id="Seg_7109" s="T806">mɨta</ta>
            <ta e="T808" id="Seg_7110" s="T807">qaqlo-qantɨ</ta>
            <ta e="T809" id="Seg_7111" s="T808">tɛlt-al-la</ta>
            <ta e="T810" id="Seg_7112" s="T809">mɨta</ta>
            <ta e="T811" id="Seg_7113" s="T810">kur-al-la</ta>
            <ta e="T812" id="Seg_7114" s="T811">ontɨt</ta>
            <ta e="T813" id="Seg_7115" s="T812">qaqlo-qɨn-tɨt</ta>
            <ta e="T814" id="Seg_7116" s="T813">nɨnɨ</ta>
            <ta e="T815" id="Seg_7117" s="T814">šittal</ta>
            <ta e="T816" id="Seg_7118" s="T815">toː</ta>
            <ta e="T817" id="Seg_7119" s="T816">namä</ta>
            <ta e="T818" id="Seg_7120" s="T817">pona</ta>
            <ta e="T821" id="Seg_7121" s="T820">qapı</ta>
            <ta e="T822" id="Seg_7122" s="T821">mɔːt</ta>
            <ta e="T823" id="Seg_7123" s="T822">šer-ıː-š-qo</ta>
            <ta e="T824" id="Seg_7124" s="T823">aj</ta>
            <ta e="T825" id="Seg_7125" s="T824">qaj</ta>
            <ta e="T826" id="Seg_7126" s="T825">am-ɨ-r-qo</ta>
            <ta e="T827" id="Seg_7127" s="T826">aj</ta>
            <ta e="T828" id="Seg_7128" s="T827">qaj</ta>
            <ta e="T829" id="Seg_7129" s="T828">ašša</ta>
            <ta e="T830" id="Seg_7130" s="T829">am-ɨ-r-qo</ta>
            <ta e="T831" id="Seg_7131" s="T830">ninɨ</ta>
            <ta e="T832" id="Seg_7132" s="T831">na</ta>
            <ta e="T833" id="Seg_7133" s="T832">qum</ta>
            <ta e="T834" id="Seg_7134" s="T833">tıntɨna</ta>
            <ta e="T835" id="Seg_7135" s="T834">sɔːr-al-pɨtɨlʼ</ta>
            <ta e="T836" id="Seg_7136" s="T835">qaqlɨ-ntɨ-t</ta>
            <ta e="T837" id="Seg_7137" s="T836">tına</ta>
            <ta e="T838" id="Seg_7138" s="T837">nɨmtɨ</ta>
            <ta e="T839" id="Seg_7139" s="T838">moqonä</ta>
            <ta e="T840" id="Seg_7140" s="T839">kəːčʼä-lä</ta>
            <ta e="T841" id="Seg_7141" s="T840">qum-iː-m-tɨ</ta>
            <ta e="T842" id="Seg_7142" s="T841">na</ta>
            <ta e="T843" id="Seg_7143" s="T842">qu-mpɨj</ta>
            <ta e="T844" id="Seg_7144" s="T843">qum-iː-m-tɨ</ta>
            <ta e="T845" id="Seg_7145" s="T844">nık</ta>
            <ta e="T846" id="Seg_7146" s="T845">qən-tɨ-ŋɨ-tɨ</ta>
            <ta e="T847" id="Seg_7147" s="T846">moqonä</ta>
            <ta e="T848" id="Seg_7148" s="T847">takkɨ</ta>
            <ta e="T849" id="Seg_7149" s="T848">moqonä</ta>
            <ta e="T850" id="Seg_7150" s="T849">Poni</ta>
            <ta e="T851" id="Seg_7151" s="T850">ira-m</ta>
            <ta e="T852" id="Seg_7152" s="T851">tɛ</ta>
            <ta e="T853" id="Seg_7153" s="T852">kəːčʼɨ-lä</ta>
            <ta e="T854" id="Seg_7154" s="T853">qən-tɨ-ŋɨ-tɨ</ta>
            <ta e="T855" id="Seg_7155" s="T854">qapı</ta>
            <ta e="T856" id="Seg_7156" s="T855">kɨpɨka</ta>
            <ta e="T857" id="Seg_7157" s="T856">kəi-tɨ</ta>
            <ta e="T858" id="Seg_7158" s="T857">ola</ta>
            <ta e="T859" id="Seg_7159" s="T858">təmol</ta>
            <ta e="T860" id="Seg_7160" s="T859">qən-nɔː-tɨt</ta>
            <ta e="T861" id="Seg_7161" s="T860">tına</ta>
            <ta e="T862" id="Seg_7162" s="T861">nɨnɨ</ta>
            <ta e="T863" id="Seg_7163" s="T862">soma-k</ta>
            <ta e="T864" id="Seg_7164" s="T863">qən-nɔː-tɨn</ta>
            <ta e="T865" id="Seg_7165" s="T864">nɨmtɨ</ta>
            <ta e="T866" id="Seg_7166" s="T865">ima-ntɨ-n</ta>
            <ta e="T867" id="Seg_7167" s="T866">nık</ta>
            <ta e="T868" id="Seg_7168" s="T867">kətɨ-ŋɨ-t</ta>
            <ta e="T869" id="Seg_7169" s="T868">mɔːl-lɨ</ta>
            <ta e="T870" id="Seg_7170" s="T869">toː</ta>
            <ta e="T871" id="Seg_7171" s="T870">tıl-ät</ta>
            <ta e="T872" id="Seg_7172" s="T871">me</ta>
            <ta e="T873" id="Seg_7173" s="T872">nʼenna</ta>
            <ta e="T874" id="Seg_7174" s="T873">qən-tɨ-sɔː-mıː</ta>
            <ta e="T875" id="Seg_7175" s="T874">qum-ɨ-t-ɨ-tkin</ta>
            <ta e="T876" id="Seg_7176" s="T875">aš</ta>
            <ta e="T877" id="Seg_7177" s="T876">tɛnɨmɨ-mpɔː-t</ta>
            <ta e="T878" id="Seg_7178" s="T877">qapı</ta>
            <ta e="T879" id="Seg_7179" s="T878">ontɨn</ta>
            <ta e="T880" id="Seg_7180" s="T879">na</ta>
            <ta e="T881" id="Seg_7181" s="T880">ilʼčʼa-ntɨ-lʼ</ta>
            <ta e="T882" id="Seg_7182" s="T881">mɨ-t</ta>
            <ta e="T883" id="Seg_7183" s="T882">qos</ta>
            <ta e="T884" id="Seg_7184" s="T883">qaj</ta>
            <ta e="T885" id="Seg_7185" s="T884">nʼenna</ta>
            <ta e="T886" id="Seg_7186" s="T885">qən-na</ta>
            <ta e="T887" id="Seg_7187" s="T886">tıntena</ta>
            <ta e="T888" id="Seg_7188" s="T887">ontɨ</ta>
            <ta e="T889" id="Seg_7189" s="T888">ašʼa</ta>
            <ta e="T890" id="Seg_7190" s="T889">koŋɨ-mpa</ta>
            <ta e="T892" id="Seg_7191" s="T891">toːna</ta>
            <ta e="T893" id="Seg_7192" s="T892">ima-tɨ</ta>
            <ta e="T894" id="Seg_7193" s="T893">na</ta>
            <ta e="T895" id="Seg_7194" s="T894">kət-ɛntɨ-tɨ</ta>
            <ta e="T896" id="Seg_7195" s="T895">äsa-n-tɨ</ta>
            <ta e="T897" id="Seg_7196" s="T896">ilʼčʼa-n-tɨ</ta>
            <ta e="T898" id="Seg_7197" s="T897">mɨ-qä-tkiːn</ta>
            <ta e="T899" id="Seg_7198" s="T898">Poni</ta>
            <ta e="T900" id="Seg_7199" s="T899">ira-lʼ</ta>
            <ta e="T901" id="Seg_7200" s="T900">mɨ-t</ta>
            <ta e="T902" id="Seg_7201" s="T901">tına</ta>
            <ta e="T903" id="Seg_7202" s="T902">moqonä</ta>
            <ta e="T904" id="Seg_7203" s="T903">qoni-sɔː-tɨt</ta>
            <ta e="T905" id="Seg_7204" s="T904">mompa</ta>
            <ta e="T906" id="Seg_7205" s="T905">ta-l</ta>
            <ta e="T907" id="Seg_7206" s="T906">nɨnɨ</ta>
            <ta e="T908" id="Seg_7207" s="T907">šʼittalʼ</ta>
            <ta e="T909" id="Seg_7208" s="T908">pija</ta>
            <ta e="T910" id="Seg_7209" s="T909">qapija</ta>
            <ta e="T912" id="Seg_7210" s="T911">aš</ta>
            <ta e="T985" id="Seg_7211" s="T912">tɛnɨmɔː-tɨt</ta>
            <ta e="T914" id="Seg_7212" s="T913">karrä</ta>
            <ta e="T986" id="Seg_7213" s="T914">koŋɨ-mp-äš</ta>
            <ta e="T916" id="Seg_7214" s="T915">karrä</ta>
            <ta e="T987" id="Seg_7215" s="T916">radio-qantɨ</ta>
            <ta e="T918" id="Seg_7216" s="T917">takkɨ</ta>
            <ta e="T919" id="Seg_7217" s="T918">nʼenna</ta>
            <ta e="T920" id="Seg_7218" s="T919">ɨpalnoj</ta>
            <ta e="T921" id="Seg_7219" s="T920">təp-ɨ-ja-t</ta>
            <ta e="T922" id="Seg_7220" s="T921">näčʼčʼa</ta>
            <ta e="T923" id="Seg_7221" s="T922">ilʼčʼa-n-tɨ</ta>
            <ta e="T924" id="Seg_7222" s="T923">mɔːt</ta>
            <ta e="T925" id="Seg_7223" s="T924">mɔːt</ta>
            <ta e="T926" id="Seg_7224" s="T925">šer-na</ta>
            <ta e="T927" id="Seg_7225" s="T926">qaqlɨ-t</ta>
            <ta e="T928" id="Seg_7226" s="T927">to</ta>
            <ta e="T929" id="Seg_7227" s="T928">nıːmɨt</ta>
            <ta e="T930" id="Seg_7228" s="T929">kurɨ-la</ta>
            <ta e="T931" id="Seg_7229" s="T930">nık</ta>
            <ta e="T932" id="Seg_7230" s="T931">tottɨ-ŋɨ-tɨ</ta>
            <ta e="T933" id="Seg_7231" s="T932">toːnna</ta>
            <ta e="T934" id="Seg_7232" s="T933">qən-mɨ-ntɔː-tɨn</ta>
            <ta e="T935" id="Seg_7233" s="T934">moqonä</ta>
            <ta e="T936" id="Seg_7234" s="T935">qən-mɨ-ntɔː-tɨt</ta>
            <ta e="T937" id="Seg_7235" s="T936">Poni</ta>
            <ta e="T938" id="Seg_7236" s="T937">ira</ta>
            <ta e="T939" id="Seg_7237" s="T938">kekɨsa</ta>
            <ta e="T940" id="Seg_7238" s="T939">takkɨ-n</ta>
            <ta e="T941" id="Seg_7239" s="T940">mɔːt-qɨn</ta>
            <ta e="T942" id="Seg_7240" s="T941">tulɨ-š-pa</ta>
            <ta e="T943" id="Seg_7241" s="T942">ılla</ta>
            <ta e="T944" id="Seg_7242" s="T943">qu-mpa</ta>
            <ta e="T945" id="Seg_7243" s="T944">toːnna</ta>
            <ta e="T946" id="Seg_7244" s="T945">šittɨ</ta>
            <ta e="T947" id="Seg_7245" s="T946">qum-oː-qıː</ta>
            <ta e="T948" id="Seg_7246" s="T947">tıː</ta>
            <ta e="T949" id="Seg_7247" s="T948">ilɨ-mpɔː-qı</ta>
            <ta e="T950" id="Seg_7248" s="T949">Poni</ta>
            <ta e="T951" id="Seg_7249" s="T950">ira</ta>
            <ta e="T952" id="Seg_7250" s="T951">Poni</ta>
            <ta e="T953" id="Seg_7251" s="T952">ira-lʼ</ta>
            <ta e="T954" id="Seg_7252" s="T953">mɨ-t</ta>
            <ta e="T955" id="Seg_7253" s="T954">nɨmtɨ</ta>
            <ta e="T988" id="Seg_7254" s="T955">ɛː-ŋa</ta>
            <ta e="T957" id="Seg_7255" s="T956">jarɨ</ta>
            <ta e="T958" id="Seg_7256" s="T957">mɨ</ta>
            <ta e="T959" id="Seg_7257" s="T958">kət-ät</ta>
            <ta e="T960" id="Seg_7258" s="T959">jarɨk</ta>
            <ta e="T989" id="Seg_7259" s="T960">jarɨk</ta>
            <ta e="T962" id="Seg_7260" s="T961">jarɨ</ta>
            <ta e="T963" id="Seg_7261" s="T962">mɨ</ta>
            <ta e="T964" id="Seg_7262" s="T963">kət-ät</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T3" id="Seg_7263" s="T2">ukkɨr</ta>
            <ta e="T4" id="Seg_7264" s="T3">iːja-tɨ</ta>
            <ta e="T5" id="Seg_7265" s="T4">ɛː-mpɨ-ntɨ</ta>
            <ta e="T7" id="Seg_7266" s="T6">na</ta>
            <ta e="T8" id="Seg_7267" s="T7">kos</ta>
            <ta e="T9" id="Seg_7268" s="T8">ket</ta>
            <ta e="T11" id="Seg_7269" s="T10">ket</ta>
            <ta e="T12" id="Seg_7270" s="T11">kətɨ-kkɨ-k</ta>
            <ta e="T13" id="Seg_7271" s="T12">na-m</ta>
            <ta e="T14" id="Seg_7272" s="T13">na</ta>
            <ta e="T15" id="Seg_7273" s="T14">tɛntɨlʼ</ta>
            <ta e="T16" id="Seg_7274" s="T15">na</ta>
            <ta e="T17" id="Seg_7275" s="T16">tɛntɨlʼ</ta>
            <ta e="T18" id="Seg_7276" s="T17">ašša</ta>
            <ta e="T19" id="Seg_7277" s="T18">čʼaptä</ta>
            <ta e="T21" id="Seg_7278" s="T20">tan</ta>
            <ta e="T22" id="Seg_7279" s="T21">ılqɨn</ta>
            <ta e="T24" id="Seg_7280" s="T23">šölʼqum-ɨ-t</ta>
            <ta e="T25" id="Seg_7281" s="T24">näčʼčʼä</ta>
            <ta e="T27" id="Seg_7282" s="T26">tam</ta>
            <ta e="T28" id="Seg_7283" s="T27">na</ta>
            <ta e="T29" id="Seg_7284" s="T28">tašıntɨ</ta>
            <ta e="T30" id="Seg_7285" s="T29">qo-lʼčʼɨ-ɛntɨ</ta>
            <ta e="T32" id="Seg_7286" s="T31">tan</ta>
            <ta e="T33" id="Seg_7287" s="T32">əːtɨ-lɨ</ta>
            <ta e="T971" id="Seg_7288" s="T33">utɨ-rɨ-ɛː-qo-joji-ntɨ-mpɨ-ätɨ</ta>
            <ta e="T972" id="Seg_7289" s="T34">tuː-ttɨ-altɨ-ätɨ</ta>
            <ta e="T36" id="Seg_7290" s="T35">čʼontɨ-ka-k</ta>
            <ta e="T973" id="Seg_7291" s="T36">kətɨ-ätɨ</ta>
            <ta e="T38" id="Seg_7292" s="T37">pija</ta>
            <ta e="T39" id="Seg_7293" s="T38">na</ta>
            <ta e="T40" id="Seg_7294" s="T39">ira</ta>
            <ta e="T41" id="Seg_7295" s="T40">ilɨ-mpɨ-n</ta>
            <ta e="T42" id="Seg_7296" s="T41">qüːtɨ-lɨ-mpɨ</ta>
            <ta e="T43" id="Seg_7297" s="T42">na</ta>
            <ta e="T44" id="Seg_7298" s="T43">šölʼqum-ɨ-lʼ</ta>
            <ta e="T45" id="Seg_7299" s="T44">ira</ta>
            <ta e="T46" id="Seg_7300" s="T45">taplʼälʼ</ta>
            <ta e="T47" id="Seg_7301" s="T46">pɛläk-lʼ</ta>
            <ta e="T48" id="Seg_7302" s="T47">kor-tɨ</ta>
            <ta e="T49" id="Seg_7303" s="T48">tɔː-lʼ</ta>
            <ta e="T50" id="Seg_7304" s="T49">takkɨ-lʼelʼ</ta>
            <ta e="T51" id="Seg_7305" s="T50">qoltɨ</ta>
            <ta e="T52" id="Seg_7306" s="T51">pɛläk-qɨn-lʼ</ta>
            <ta e="T53" id="Seg_7307" s="T52">ilɨ-mpɨ</ta>
            <ta e="T54" id="Seg_7308" s="T53">toː-qɨn-lʼ</ta>
            <ta e="T55" id="Seg_7309" s="T54">təp</ta>
            <ta e="T56" id="Seg_7310" s="T55">poː-kɨtɨ-lʼ</ta>
            <ta e="T57" id="Seg_7311" s="T56">təttɨ-n</ta>
            <ta e="T992" id="Seg_7312" s="T58">qaj</ta>
            <ta e="T60" id="Seg_7313" s="T59">ilɨ-mpɨ</ta>
            <ta e="T61" id="Seg_7314" s="T60">tına</ta>
            <ta e="T62" id="Seg_7315" s="T61">qüːtɨ-lɨ-mpɨ</ta>
            <ta e="T63" id="Seg_7316" s="T62">iːja-tɨ</ta>
            <ta e="T64" id="Seg_7317" s="T63">ukkɨr</ta>
            <ta e="T65" id="Seg_7318" s="T64">iːja-tɨ</ta>
            <ta e="T66" id="Seg_7319" s="T65">pɛlɨ-kɨtɨ-lʼ</ta>
            <ta e="T67" id="Seg_7320" s="T66">neː-kɨtɨ-lʼ</ta>
            <ta e="T68" id="Seg_7321" s="T67">pɛlɨ-tɨ</ta>
            <ta e="T69" id="Seg_7322" s="T68">nʼi</ta>
            <ta e="T70" id="Seg_7323" s="T69">nälʼa-tɨ</ta>
            <ta e="T71" id="Seg_7324" s="T70">čʼäːŋkɨ</ta>
            <ta e="T72" id="Seg_7325" s="T71">nʼi</ta>
            <ta e="T73" id="Seg_7326" s="T72">qaj-tɨ</ta>
            <ta e="T74" id="Seg_7327" s="T73">čʼäːŋkɨ</ta>
            <ta e="T75" id="Seg_7328" s="T74">nɨːnɨ</ta>
            <ta e="T76" id="Seg_7329" s="T75">na</ta>
            <ta e="T77" id="Seg_7330" s="T76">əsɨ-tɨ</ta>
            <ta e="T78" id="Seg_7331" s="T77">qüːtɨ-lɨ-ŋɨ</ta>
            <ta e="T79" id="Seg_7332" s="T78">na</ta>
            <ta e="T80" id="Seg_7333" s="T79">iːja-lʼa</ta>
            <ta e="T82" id="Seg_7334" s="T81">na</ta>
            <ta e="T83" id="Seg_7335" s="T82">iːja-n</ta>
            <ta e="T84" id="Seg_7336" s="T83">nɨmtɨ</ta>
            <ta e="T85" id="Seg_7337" s="T84">ilɨ</ta>
            <ta e="T86" id="Seg_7338" s="T85">qapı</ta>
            <ta e="T87" id="Seg_7339" s="T86">na</ta>
            <ta e="T88" id="Seg_7340" s="T87">əsɨ-n-tɨ</ta>
            <ta e="T89" id="Seg_7341" s="T88">ama-n-tɨ</ta>
            <ta e="T90" id="Seg_7342" s="T89">qam-qɨn</ta>
            <ta e="T91" id="Seg_7343" s="T90">toː-ntɨ</ta>
            <ta e="T92" id="Seg_7344" s="T91">tına</ta>
            <ta e="T93" id="Seg_7345" s="T92">šittɨ</ta>
            <ta e="T94" id="Seg_7346" s="T93">qälɨk</ta>
            <ta e="T95" id="Seg_7347" s="T94">timnʼa-sɨ-qı</ta>
            <ta e="T96" id="Seg_7348" s="T95">ɛː-mpɨ-ntɨ-qı</ta>
            <ta e="T97" id="Seg_7349" s="T96">qälɨk</ta>
            <ta e="T98" id="Seg_7350" s="T97">ira</ta>
            <ta e="T99" id="Seg_7351" s="T98">kučʼčʼä</ta>
            <ta e="T100" id="Seg_7352" s="T99">ɛː-ɛntɨ</ta>
            <ta e="T102" id="Seg_7353" s="T101">qaj</ta>
            <ta e="T103" id="Seg_7354" s="T102">ilɨ</ta>
            <ta e="T104" id="Seg_7355" s="T103">kučʼčʼä</ta>
            <ta e="T105" id="Seg_7356" s="T104">izhe</ta>
            <ta e="T106" id="Seg_7357" s="T105">tına</ta>
            <ta e="T107" id="Seg_7358" s="T106">na</ta>
            <ta e="T108" id="Seg_7359" s="T107">ira</ta>
            <ta e="T109" id="Seg_7360" s="T108">ıllä</ta>
            <ta e="T110" id="Seg_7361" s="T109">qu-ŋɨ</ta>
            <ta e="T111" id="Seg_7362" s="T110">ıllä</ta>
            <ta e="T112" id="Seg_7363" s="T111">qu-ŋɨ</ta>
            <ta e="T113" id="Seg_7364" s="T112">tına</ta>
            <ta e="T114" id="Seg_7365" s="T113">nɨːnɨ</ta>
            <ta e="T115" id="Seg_7366" s="T114">šittälʼ</ta>
            <ta e="T116" id="Seg_7367" s="T115">tına</ta>
            <ta e="T117" id="Seg_7368" s="T116">iːja</ta>
            <ta e="T118" id="Seg_7369" s="T117">qälɨk</ta>
            <ta e="T119" id="Seg_7370" s="T118">ira</ta>
            <ta e="T120" id="Seg_7371" s="T119">nɔːtɨ</ta>
            <ta e="T121" id="Seg_7372" s="T120">kučʼčʼä</ta>
            <ta e="T122" id="Seg_7373" s="T121">qattɨ-ɛntɨ</ta>
            <ta e="T123" id="Seg_7374" s="T122">toː</ta>
            <ta e="T124" id="Seg_7375" s="T123">timnʼa-ntɨ-nɨ</ta>
            <ta e="T125" id="Seg_7376" s="T124">tulɨ-š-ŋɨ</ta>
            <ta e="T126" id="Seg_7377" s="T125">ira</ta>
            <ta e="T127" id="Seg_7378" s="T126">qu-mpɨ</ta>
            <ta e="T128" id="Seg_7379" s="T127">ıllä</ta>
            <ta e="T129" id="Seg_7380" s="T128">meː-ŋɨ-tɨt</ta>
            <ta e="T130" id="Seg_7381" s="T129">šittälʼ</ta>
            <ta e="T131" id="Seg_7382" s="T130">imaqota-tɨ</ta>
            <ta e="T132" id="Seg_7383" s="T131">tına</ta>
            <ta e="T133" id="Seg_7384" s="T132">imaqota-tɨ</ta>
            <ta e="T134" id="Seg_7385" s="T133">qüːtɨ-lɨ-ŋɨ</ta>
            <ta e="T135" id="Seg_7386" s="T134">imaqota-tɨ</ta>
            <ta e="T136" id="Seg_7387" s="T135">aj</ta>
            <ta e="T137" id="Seg_7388" s="T136">ıllä</ta>
            <ta e="T138" id="Seg_7389" s="T137">qu-ŋɨ</ta>
            <ta e="T139" id="Seg_7390" s="T138">ira-lʼ</ta>
            <ta e="T140" id="Seg_7391" s="T139">mɨ-qı</ta>
            <ta e="T141" id="Seg_7392" s="T140">tına</ta>
            <ta e="T142" id="Seg_7393" s="T141">iːja-tɨ</ta>
            <ta e="T143" id="Seg_7394" s="T142">nʼeː-kɔːlɨ</ta>
            <ta e="T144" id="Seg_7395" s="T143">pɛlɨ-kɔːlɨ</ta>
            <ta e="T145" id="Seg_7396" s="T144">qalɨ</ta>
            <ta e="T146" id="Seg_7397" s="T145">kučʼčʼä</ta>
            <ta e="T147" id="Seg_7398" s="T146">qattɨ-ɛntɨ</ta>
            <ta e="T148" id="Seg_7399" s="T147">na</ta>
            <ta e="T149" id="Seg_7400" s="T148">qälɨk</ta>
            <ta e="T150" id="Seg_7401" s="T149">ira</ta>
            <ta e="T151" id="Seg_7402" s="T150">nɨːnɨ</ta>
            <ta e="T152" id="Seg_7403" s="T151">šittälʼ</ta>
            <ta e="T153" id="Seg_7404" s="T152">tɔː</ta>
            <ta e="T154" id="Seg_7405" s="T153">puː-čʼɨ-mpɨ-tɨ</ta>
            <ta e="T155" id="Seg_7406" s="T154">na</ta>
            <ta e="T156" id="Seg_7407" s="T155">čʼilʼaŋ-lʼ</ta>
            <ta e="T157" id="Seg_7408" s="T156">iːja</ta>
            <ta e="T158" id="Seg_7409" s="T157">wərqɨ</ta>
            <ta e="T159" id="Seg_7410" s="T158">iːja-tɨ</ta>
            <ta e="T167" id="Seg_7411" s="T166">istap</ta>
            <ta e="T168" id="Seg_7412" s="T167">Gena-lʼ</ta>
            <ta e="T169" id="Seg_7413" s="T168">mɨ-n</ta>
            <ta e="T170" id="Seg_7414" s="T169">tarä</ta>
            <ta e="T171" id="Seg_7415" s="T170">nılʼčʼɨ-lʼ</ta>
            <ta e="T172" id="Seg_7416" s="T171">iːja-tɨ-lʼ</ta>
            <ta e="T173" id="Seg_7417" s="T172">ɛː-mpɨ</ta>
            <ta e="T174" id="Seg_7418" s="T173">tɔː</ta>
            <ta e="T175" id="Seg_7419" s="T174">puː-tɨ-ŋɨ-tɨ</ta>
            <ta e="T176" id="Seg_7420" s="T175">ɔːtä-ntɨ-lʼ</ta>
            <ta e="T177" id="Seg_7421" s="T176">čʼäːŋkɨ</ta>
            <ta e="T178" id="Seg_7422" s="T177">ɔːtä-kɔːlɨ</ta>
            <ta e="T179" id="Seg_7423" s="T178">topɨ-sä</ta>
            <ta e="T180" id="Seg_7424" s="T179">ilɨ-mpɨ-tɨt</ta>
            <ta e="T181" id="Seg_7425" s="T180">na</ta>
            <ta e="T182" id="Seg_7426" s="T181">əsɨ-sɨ-t</ta>
            <ta e="T183" id="Seg_7427" s="T182">nɨmtɨ</ta>
            <ta e="T184" id="Seg_7428" s="T183">ilɨ-mpɨ-tɨt</ta>
            <ta e="T185" id="Seg_7429" s="T184">ilɨ-mpɨ-tɨt</ta>
            <ta e="T186" id="Seg_7430" s="T185">nɨːnɨ</ta>
            <ta e="T187" id="Seg_7431" s="T186">tɔː</ta>
            <ta e="T188" id="Seg_7432" s="T187">puː-čʼɨ-sɨ-tɨt</ta>
            <ta e="T189" id="Seg_7433" s="T188">tɔː</ta>
            <ta e="T190" id="Seg_7434" s="T189">puː-lä</ta>
            <ta e="T191" id="Seg_7435" s="T190">na</ta>
            <ta e="T192" id="Seg_7436" s="T191">iːja</ta>
            <ta e="T193" id="Seg_7437" s="T192">qälɨk</ta>
            <ta e="T194" id="Seg_7438" s="T193">ira</ta>
            <ta e="T195" id="Seg_7439" s="T194">mɔːt-ntɨ</ta>
            <ta e="T196" id="Seg_7440" s="T195">qälɨk</ta>
            <ta e="T197" id="Seg_7441" s="T196">ira</ta>
            <ta e="T198" id="Seg_7442" s="T197">iːja-tɨ</ta>
            <ta e="T199" id="Seg_7443" s="T198">čʼäːŋkɨ</ta>
            <ta e="T200" id="Seg_7444" s="T199">iːja-kɨtɨ-lʼ</ta>
            <ta e="T201" id="Seg_7445" s="T200">ima-tɨ</ta>
            <ta e="T202" id="Seg_7446" s="T201">toːnna</ta>
            <ta e="T203" id="Seg_7447" s="T202">čʼontɨ-lʼ</ta>
            <ta e="T204" id="Seg_7448" s="T203">timnʼa-tɨ</ta>
            <ta e="T206" id="Seg_7449" s="T205">ukkɨr</ta>
            <ta e="T207" id="Seg_7450" s="T206">nälʼa-tɨ</ta>
            <ta e="T208" id="Seg_7451" s="T207">pɛlɨ-kɨtɨ-lʼ</ta>
            <ta e="T209" id="Seg_7452" s="T208">takkɨ-nɨ</ta>
            <ta e="T210" id="Seg_7453" s="T209">nʼennä</ta>
            <ta e="T211" id="Seg_7454" s="T210">šittɨ</ta>
            <ta e="T212" id="Seg_7455" s="T211">qälɨk-qı</ta>
            <ta e="T213" id="Seg_7456" s="T212">təp</ta>
            <ta e="T214" id="Seg_7457" s="T213">poː-kɨtɨ-lʼ</ta>
            <ta e="T215" id="Seg_7458" s="T214">təttɨ-t</ta>
            <ta e="T216" id="Seg_7459" s="T215">Pan</ta>
            <ta e="T217" id="Seg_7460" s="T216">ira</ta>
            <ta e="T218" id="Seg_7461" s="T217">Ponʼi</ta>
            <ta e="T219" id="Seg_7462" s="T218">ira</ta>
            <ta e="T220" id="Seg_7463" s="T219">najntɨ</ta>
            <ta e="T221" id="Seg_7464" s="T220">kun</ta>
            <ta e="T222" id="Seg_7465" s="T221">muntɨk-lʼ</ta>
            <ta e="T223" id="Seg_7466" s="T222">tamtɨr-tɨ</ta>
            <ta e="T224" id="Seg_7467" s="T223">ɔːtä-m</ta>
            <ta e="T225" id="Seg_7468" s="T224">muntɨk</ta>
            <ta e="T226" id="Seg_7469" s="T225">*taqɨ-š-mpɨ-tɨ</ta>
            <ta e="T227" id="Seg_7470" s="T226">ta-l</ta>
            <ta e="T228" id="Seg_7471" s="T227">nılʼčʼɨ-k</ta>
            <ta e="T229" id="Seg_7472" s="T228">tü-ŋɨ</ta>
            <ta e="T230" id="Seg_7473" s="T229">na</ta>
            <ta e="T231" id="Seg_7474" s="T230">timnʼa-sɨ-qı</ta>
            <ta e="T232" id="Seg_7475" s="T231">na</ta>
            <ta e="T233" id="Seg_7476" s="T232">timnʼa-sɨ-qı</ta>
            <ta e="T234" id="Seg_7477" s="T233">ilɨ-qı</ta>
            <ta e="T235" id="Seg_7478" s="T234">na</ta>
            <ta e="T236" id="Seg_7479" s="T235">ira-lʼ</ta>
            <ta e="T237" id="Seg_7480" s="T236">timnʼa-sɨ-qı</ta>
            <ta e="T238" id="Seg_7481" s="T237">šittɨ</ta>
            <ta e="T239" id="Seg_7482" s="T238">tɛːttɨ</ta>
            <ta e="T240" id="Seg_7483" s="T239">toːn</ta>
            <ta e="T241" id="Seg_7484" s="T240">ɔːtä-tɨ</ta>
            <ta e="T242" id="Seg_7485" s="T241">na</ta>
            <ta e="T243" id="Seg_7486" s="T242">ira-n</ta>
            <ta e="T244" id="Seg_7487" s="T243">ɔːtä-m</ta>
            <ta e="T245" id="Seg_7488" s="T244">iː-qɨntoːqo</ta>
            <ta e="T246" id="Seg_7489" s="T245">qum-iː-ntɨ-sä</ta>
            <ta e="T247" id="Seg_7490" s="T246">tü-ɛntɨ</ta>
            <ta e="T248" id="Seg_7491" s="T247">Ponʼi</ta>
            <ta e="T249" id="Seg_7492" s="T248">ira</ta>
            <ta e="T250" id="Seg_7493" s="T249">nım-tɨ</ta>
            <ta e="T251" id="Seg_7494" s="T250">najntɨ</ta>
            <ta e="T252" id="Seg_7495" s="T251">nɨːnɨ</ta>
            <ta e="T253" id="Seg_7496" s="T252">ta-l</ta>
            <ta e="T254" id="Seg_7497" s="T253">tü-lä</ta>
            <ta e="T255" id="Seg_7498" s="T254">šäqqɨ-tɨt</ta>
            <ta e="T256" id="Seg_7499" s="T255">na</ta>
            <ta e="T257" id="Seg_7500" s="T256">təp-t-ɨ-n</ta>
            <ta e="T258" id="Seg_7501" s="T257">mɔːt</ta>
            <ta e="T259" id="Seg_7502" s="T258">na</ta>
            <ta e="T260" id="Seg_7503" s="T259">iːja-m</ta>
            <ta e="T261" id="Seg_7504" s="T260">qapı</ta>
            <ta e="T262" id="Seg_7505" s="T261">ašša</ta>
            <ta e="T263" id="Seg_7506" s="T262">tɛnɨmɨ-tɨt</ta>
            <ta e="T265" id="Seg_7507" s="T264">kuttar</ta>
            <ta e="T266" id="Seg_7508" s="T265">*koŋɨ-mpɨ-ɛntɨ</ta>
            <ta e="T267" id="Seg_7509" s="T266">təp</ta>
            <ta e="T268" id="Seg_7510" s="T267">qaj</ta>
            <ta e="T270" id="Seg_7511" s="T269">muːtɨrona-k</ta>
            <ta e="T272" id="Seg_7512" s="T271">ilʼi</ta>
            <ta e="T273" id="Seg_7513" s="T272">kuttar</ta>
            <ta e="T274" id="Seg_7514" s="T273">ilɨ</ta>
            <ta e="T275" id="Seg_7515" s="T274">tıː</ta>
            <ta e="T276" id="Seg_7516" s="T275">kuttar</ta>
            <ta e="T277" id="Seg_7517" s="T276">tɛnɨ-mpɨ-ɛntɨ-l</ta>
            <ta e="T278" id="Seg_7518" s="T277">älpä-n</ta>
            <ta e="T279" id="Seg_7519" s="T278">ilɨ-ntɨlʼ</ta>
            <ta e="T280" id="Seg_7520" s="T279">šäqqɨ-qı</ta>
            <ta e="T281" id="Seg_7521" s="T280">na</ta>
            <ta e="T282" id="Seg_7522" s="T281">qaj</ta>
            <ta e="T283" id="Seg_7523" s="T282">mompa</ta>
            <ta e="T284" id="Seg_7524" s="T283">nɔːkɨr-mtälɨlʼ</ta>
            <ta e="T285" id="Seg_7525" s="T284">čʼeːlɨ</ta>
            <ta e="T286" id="Seg_7526" s="T285">toːqqɨt</ta>
            <ta e="T287" id="Seg_7527" s="T286">mompa</ta>
            <ta e="T288" id="Seg_7528" s="T287">na</ta>
            <ta e="T289" id="Seg_7529" s="T288">tü-ŋɨ</ta>
            <ta e="T290" id="Seg_7530" s="T289">mompa</ta>
            <ta e="T291" id="Seg_7531" s="T290">Ponʼi</ta>
            <ta e="T292" id="Seg_7532" s="T291">ira</ta>
            <ta e="T294" id="Seg_7533" s="T293">köt</ta>
            <ta e="T295" id="Seg_7534" s="T294">qum-sä</ta>
            <ta e="T296" id="Seg_7535" s="T295">tü-mpɨ</ta>
            <ta e="T297" id="Seg_7536" s="T296">qapı</ta>
            <ta e="T298" id="Seg_7537" s="T297">na</ta>
            <ta e="T299" id="Seg_7538" s="T298">ɔːtä-t</ta>
            <ta e="T300" id="Seg_7539" s="T299">tɔːqqɨ-qɨntoːqo</ta>
            <ta e="T301" id="Seg_7540" s="T300">na</ta>
            <ta e="T302" id="Seg_7541" s="T301">qum-ɨ-t</ta>
            <ta e="T303" id="Seg_7542" s="T302">nɔːtɨ</ta>
            <ta e="T304" id="Seg_7543" s="T303">na</ta>
            <ta e="T305" id="Seg_7544" s="T304">timnʼa-sɨ-qı-n</ta>
            <ta e="T306" id="Seg_7545" s="T305">na</ta>
            <ta e="T307" id="Seg_7546" s="T306">ɔːtä-m</ta>
            <ta e="T308" id="Seg_7547" s="T307">tɔːqqɨ-mpɨ-tɨt</ta>
            <ta e="T309" id="Seg_7548" s="T308">*ku-n</ta>
            <ta e="T310" id="Seg_7549" s="T309">muntɨk-lʼ</ta>
            <ta e="T311" id="Seg_7550" s="T310">mɔːt-tɨ</ta>
            <ta e="T312" id="Seg_7551" s="T311">tına</ta>
            <ta e="T313" id="Seg_7552" s="T312">šentɨ</ta>
            <ta e="T314" id="Seg_7553" s="T313">mɔːt-tɨ</ta>
            <ta e="T315" id="Seg_7554" s="T314">*ku-n</ta>
            <ta e="T316" id="Seg_7555" s="T315">muntɨk-lʼ</ta>
            <ta e="T317" id="Seg_7556" s="T316">qaj-tɨ</ta>
            <ta e="T318" id="Seg_7557" s="T317">muntɨk</ta>
            <ta e="T319" id="Seg_7558" s="T318">*taqɨ-qɨl-ntɨ-ŋɨ-tɨt</ta>
            <ta e="T320" id="Seg_7559" s="T319">ukoːn</ta>
            <ta e="T321" id="Seg_7560" s="T320">nɔːtɨ</ta>
            <ta e="T322" id="Seg_7561" s="T321">takkɨ-lʼ</ta>
            <ta e="T323" id="Seg_7562" s="T322">qälɨk-iː-n</ta>
            <ta e="T324" id="Seg_7563" s="T323">ɔːtä-m</ta>
            <ta e="T325" id="Seg_7564" s="T324">*taqɨ-qɨl-lä</ta>
            <ta e="T326" id="Seg_7565" s="T325">nɔːtɨ</ta>
            <ta e="T327" id="Seg_7566" s="T326">muntɨk</ta>
            <ta e="T328" id="Seg_7567" s="T327">iː-mpɨ-tɨt</ta>
            <ta e="T329" id="Seg_7568" s="T328">nɨːnɨ</ta>
            <ta e="T330" id="Seg_7569" s="T329">šittälʼ</ta>
            <ta e="T331" id="Seg_7570" s="T330">mɨŋa</ta>
            <ta e="T332" id="Seg_7571" s="T331">moqɨnä</ta>
            <ta e="T333" id="Seg_7572" s="T332">qən-ŋɨ-tɨt</ta>
            <ta e="T334" id="Seg_7573" s="T333">na</ta>
            <ta e="T335" id="Seg_7574" s="T334">qälɨk-n</ta>
            <ta e="T336" id="Seg_7575" s="T335">tıšša</ta>
            <ta e="T337" id="Seg_7576" s="T336">nɔːkɨr-mtälɨlʼ</ta>
            <ta e="T338" id="Seg_7577" s="T337">čʼeːlɨ-tɨ</ta>
            <ta e="T339" id="Seg_7578" s="T338">mompa</ta>
            <ta e="T340" id="Seg_7579" s="T339">tü-ɛntɨ-mɨt</ta>
            <ta e="T341" id="Seg_7580" s="T340">mɨta</ta>
            <ta e="T342" id="Seg_7581" s="T341">tɛː</ta>
            <ta e="T343" id="Seg_7582" s="T342">mompa</ta>
            <ta e="T344" id="Seg_7583" s="T343">kučʼčʼä-t</ta>
            <ta e="T345" id="Seg_7584" s="T344">ɨkɨ</ta>
            <ta e="T346" id="Seg_7585" s="T345">qən-ŋɨlɨt</ta>
            <ta e="T347" id="Seg_7586" s="T346">šittɨ-mtälɨlʼ</ta>
            <ta e="T348" id="Seg_7587" s="T347">čʼeːlɨ-ntɨ</ta>
            <ta e="T349" id="Seg_7588" s="T348">na</ta>
            <ta e="T350" id="Seg_7589" s="T349">čʼeːlɨ-m-ŋɨ</ta>
            <ta e="T351" id="Seg_7590" s="T350">na</ta>
            <ta e="T352" id="Seg_7591" s="T351">iːja</ta>
            <ta e="T353" id="Seg_7592" s="T352">nılʼčʼɨ-k</ta>
            <ta e="T354" id="Seg_7593" s="T353">ɛsɨ</ta>
            <ta e="T355" id="Seg_7594" s="T354">na</ta>
            <ta e="T356" id="Seg_7595" s="T355">ilʼčʼa-ntɨ-nɨŋ</ta>
            <ta e="T357" id="Seg_7596" s="T356">na</ta>
            <ta e="T358" id="Seg_7597" s="T357">gostʼ-ɨ-n</ta>
            <ta e="T359" id="Seg_7598" s="T358">ilʼčʼa-ntɨ</ta>
            <ta e="T360" id="Seg_7599" s="T359">mɨta</ta>
            <ta e="T361" id="Seg_7600" s="T360">nʼennä</ta>
            <ta e="T362" id="Seg_7601" s="T361">tɨmtɨ</ta>
            <ta e="T363" id="Seg_7602" s="T362">mɨ</ta>
            <ta e="T364" id="Seg_7603" s="T363">ɛː-ntɨ</ta>
            <ta e="T365" id="Seg_7604" s="T364">mɨta</ta>
            <ta e="T366" id="Seg_7605" s="T365">tɛːttɨ</ta>
            <ta e="T367" id="Seg_7606" s="T366">toːn</ta>
            <ta e="T368" id="Seg_7607" s="T367">ɔːtä-n</ta>
            <ta e="T369" id="Seg_7608" s="T368">nɨŋ-kkɨ-ku-ššak</ta>
            <ta e="T370" id="Seg_7609" s="T369">mɨta</ta>
            <ta e="T371" id="Seg_7610" s="T370">nʼarɨ</ta>
            <ta e="T372" id="Seg_7611" s="T371">ɛː-ntɨ</ta>
            <ta e="T373" id="Seg_7612" s="T372">mɨta</ta>
            <ta e="T374" id="Seg_7613" s="T373">näčʼčʼä-ntɨ</ta>
            <ta e="T375" id="Seg_7614" s="T374">üːtɨ-lä-mɨt</ta>
            <ta e="T376" id="Seg_7615" s="T375">mɨta</ta>
            <ta e="T377" id="Seg_7616" s="T376">qälɨk-t</ta>
            <ta e="T378" id="Seg_7617" s="T377">tü-ɛntɨ-tɨt</ta>
            <ta e="T379" id="Seg_7618" s="T378">nɨmtɨ</ta>
            <ta e="T380" id="Seg_7619" s="T379">ɔːtä-m</ta>
            <ta e="T381" id="Seg_7620" s="T380">tɔːqqɨ-qo-olam-ɛntɨ-tɨt</ta>
            <ta e="T382" id="Seg_7621" s="T381">nık</ta>
            <ta e="T383" id="Seg_7622" s="T382">mɨta</ta>
            <ta e="T384" id="Seg_7623" s="T383">tına</ta>
            <ta e="T385" id="Seg_7624" s="T384">ɔːtä-n</ta>
            <ta e="T386" id="Seg_7625" s="T385">up-tɨ</ta>
            <ta e="T387" id="Seg_7626" s="T386">kə-lʼčʼomɨnto</ta>
            <ta e="T388" id="Seg_7627" s="T387">kə-rɨ-mpɨlʼ</ta>
            <ta e="T389" id="Seg_7628" s="T388">mɨta</ta>
            <ta e="T390" id="Seg_7629" s="T389">kučʼčʼä</ta>
            <ta e="T391" id="Seg_7630" s="T390">meː-ɛntɨ-mɨt</ta>
            <ta e="T392" id="Seg_7631" s="T391">na</ta>
            <ta e="T393" id="Seg_7632" s="T392">iːja</ta>
            <ta e="T394" id="Seg_7633" s="T393">nık</ta>
            <ta e="T395" id="Seg_7634" s="T394">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T396" id="Seg_7635" s="T395">nɨːnɨ</ta>
            <ta e="T397" id="Seg_7636" s="T396">nık</ta>
            <ta e="T398" id="Seg_7637" s="T397">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T399" id="Seg_7638" s="T398">mɨta</ta>
            <ta e="T401" id="Seg_7639" s="T400">mäkkä</ta>
            <ta e="T402" id="Seg_7640" s="T401">mi-ätɨ</ta>
            <ta e="T403" id="Seg_7641" s="T402">šentɨ</ta>
            <ta e="T404" id="Seg_7642" s="T403">šentɨ</ta>
            <ta e="T405" id="Seg_7643" s="T404">mɔːt-lɨ</ta>
            <ta e="T406" id="Seg_7644" s="T405">ɛː-ŋɨ</ta>
            <ta e="T407" id="Seg_7645" s="T406">ilʼčʼa</ta>
            <ta e="T408" id="Seg_7646" s="T407">mɨta</ta>
            <ta e="T409" id="Seg_7647" s="T408">qaj</ta>
            <ta e="T410" id="Seg_7648" s="T409">šentɨ</ta>
            <ta e="T411" id="Seg_7649" s="T410">mɔːt-lɨ</ta>
            <ta e="T412" id="Seg_7650" s="T411">ɛː-ŋɨ</ta>
            <ta e="T413" id="Seg_7651" s="T412">mäkkä</ta>
            <ta e="T414" id="Seg_7652" s="T413">mi-ätɨ</ta>
            <ta e="T415" id="Seg_7653" s="T414">mɨta</ta>
            <ta e="T416" id="Seg_7654" s="T415">a</ta>
            <ta e="T417" id="Seg_7655" s="T416">toːnna</ta>
            <ta e="T418" id="Seg_7656" s="T417">tına</ta>
            <ta e="T419" id="Seg_7657" s="T418">timnʼa-n-tɨ</ta>
            <ta e="T420" id="Seg_7658" s="T419">nälʼa</ta>
            <ta e="T421" id="Seg_7659" s="T420">ilʼčʼa-tɨ</ta>
            <ta e="T422" id="Seg_7660" s="T421">na-m</ta>
            <ta e="T423" id="Seg_7661" s="T422">*kurɨ-altɨ-mpɨ-ntɨ-tɨ</ta>
            <ta e="T424" id="Seg_7662" s="T423">na</ta>
            <ta e="T425" id="Seg_7663" s="T424">iːja-nɨŋ</ta>
            <ta e="T426" id="Seg_7664" s="T425">man</ta>
            <ta e="T427" id="Seg_7665" s="T426">mɨta</ta>
            <ta e="T428" id="Seg_7666" s="T427">prosto</ta>
            <ta e="T429" id="Seg_7667" s="T428">wəčʼčʼɨ-m-ɛntɨ-k</ta>
            <ta e="T430" id="Seg_7668" s="T429">mɔːt</ta>
            <ta e="T431" id="Seg_7669" s="T430">nʼennä-n</ta>
            <ta e="T432" id="Seg_7670" s="T431">mɨntɨ</ta>
            <ta e="T433" id="Seg_7671" s="T432">mačʼɨ-lʼ</ta>
            <ta e="T434" id="Seg_7672" s="T433">tıčʼɨ</ta>
            <ta e="T435" id="Seg_7673" s="T434">ɛː-ɛntɨ</ta>
            <ta e="T436" id="Seg_7674" s="T435">na</ta>
            <ta e="T437" id="Seg_7675" s="T436">nʼennä-lʼ</ta>
            <ta e="T438" id="Seg_7676" s="T437">pɛläk-ntɨ</ta>
            <ta e="T439" id="Seg_7677" s="T438">mɔːt-ɨ-k-ɛntɨ-k</ta>
            <ta e="T440" id="Seg_7678" s="T439">šentɨ</ta>
            <ta e="T441" id="Seg_7679" s="T440">mɔːt</ta>
            <ta e="T442" id="Seg_7680" s="T441">taːtɨ-ŋɨ</ta>
            <ta e="T443" id="Seg_7681" s="T442">najntɨ</ta>
            <ta e="T444" id="Seg_7682" s="T443">üːtɨ-ätɔːl-ŋɨ-tɨt</ta>
            <ta e="T445" id="Seg_7683" s="T444">ta-l</ta>
            <ta e="T446" id="Seg_7684" s="T445">üːtɨ-ätɔːl-tɨ-tɨt</ta>
            <ta e="T447" id="Seg_7685" s="T446">na</ta>
            <ta e="T448" id="Seg_7686" s="T447">qapı</ta>
            <ta e="T449" id="Seg_7687" s="T448">nuːnɨčʼɨ-sɨ</ta>
            <ta e="T450" id="Seg_7688" s="T449">šittɨ-mtälɨlʼ</ta>
            <ta e="T451" id="Seg_7689" s="T450">nɔːkɨr-mtälɨlʼ</ta>
            <ta e="T452" id="Seg_7690" s="T451">čʼeːlɨ</ta>
            <ta e="T453" id="Seg_7691" s="T452">tü-ɛntɨ-tɨt</ta>
            <ta e="T454" id="Seg_7692" s="T453">Ponʼi</ta>
            <ta e="T455" id="Seg_7693" s="T454">ira-lʼ</ta>
            <ta e="T456" id="Seg_7694" s="T455">mɨ</ta>
            <ta e="T457" id="Seg_7695" s="T456">təp-ɨ-t-ɨ-nkinı</ta>
            <ta e="T458" id="Seg_7696" s="T457">na</ta>
            <ta e="T459" id="Seg_7697" s="T458">qälɨk</ta>
            <ta e="T460" id="Seg_7698" s="T459">ira-lʼ</ta>
            <ta e="T461" id="Seg_7699" s="T460">mɨ-qı</ta>
            <ta e="T462" id="Seg_7700" s="T461">nɔːtɨ</ta>
            <ta e="T463" id="Seg_7701" s="T462">*taqɨ-ätɔːl-qɨntoːqo</ta>
            <ta e="T464" id="Seg_7702" s="T463">timnʼa-sɨ-qı</ta>
            <ta e="T465" id="Seg_7703" s="T464">nɔːtɨ</ta>
            <ta e="T466" id="Seg_7704" s="T465">tam</ta>
            <ta e="T467" id="Seg_7705" s="T466">čʼeːlɨ-qɨn</ta>
            <ta e="T468" id="Seg_7706" s="T467">nɨːnɨ</ta>
            <ta e="T469" id="Seg_7707" s="T468">šittälʼ</ta>
            <ta e="T470" id="Seg_7708" s="T469">üːtɨ-ätɔːl-ntɨ-tɨt</ta>
            <ta e="T471" id="Seg_7709" s="T470">na</ta>
            <ta e="T472" id="Seg_7710" s="T471">ɔːtä-m</ta>
            <ta e="T473" id="Seg_7711" s="T472">na</ta>
            <ta e="T474" id="Seg_7712" s="T473">tɔːqqɨ-ntɨ-tɨt</ta>
            <ta e="T475" id="Seg_7713" s="T474">qapı</ta>
            <ta e="T476" id="Seg_7714" s="T475">təp</ta>
            <ta e="T477" id="Seg_7715" s="T476">aj</ta>
            <ta e="T479" id="Seg_7716" s="T478">mɔːt-sä</ta>
            <ta e="T480" id="Seg_7717" s="T479">na</ta>
            <ta e="T481" id="Seg_7718" s="T480">mi-ntɨ-tɨt</ta>
            <ta e="T482" id="Seg_7719" s="T481">mɔːt</ta>
            <ta e="T483" id="Seg_7720" s="T482">šeːr-ŋɨ</ta>
            <ta e="T484" id="Seg_7721" s="T483">na</ta>
            <ta e="T485" id="Seg_7722" s="T484">mačʼɨ-n</ta>
            <ta e="T486" id="Seg_7723" s="T485">tıčʼɨ-n</ta>
            <ta e="T487" id="Seg_7724" s="T486">nʼennä-lʼ</ta>
            <ta e="T488" id="Seg_7725" s="T487">pɛläk-lʼ</ta>
            <ta e="T489" id="Seg_7726" s="T488">mɔːt</ta>
            <ta e="T490" id="Seg_7727" s="T489">šeːr-ŋɨ</ta>
            <ta e="T491" id="Seg_7728" s="T490">nɨːnɨ</ta>
            <ta e="T492" id="Seg_7729" s="T491">na</ta>
            <ta e="T493" id="Seg_7730" s="T492">mɔːt-ɨ-k-ntɨ-qı</ta>
            <ta e="T494" id="Seg_7731" s="T493">na</ta>
            <ta e="T495" id="Seg_7732" s="T494">ima-ntɨ-sä</ta>
            <ta e="T496" id="Seg_7733" s="T495">qapı</ta>
            <ta e="T497" id="Seg_7734" s="T496">tətta-lʼ</ta>
            <ta e="T993" id="Seg_7735" s="T497">qäl</ta>
            <ta e="T498" id="Seg_7736" s="T993">ira</ta>
            <ta e="T499" id="Seg_7737" s="T498">qaj</ta>
            <ta e="T500" id="Seg_7738" s="T499">mɔːt-tɨ</ta>
            <ta e="T501" id="Seg_7739" s="T500">čʼäːŋkɨ</ta>
            <ta e="T502" id="Seg_7740" s="T501">šentɨ</ta>
            <ta e="T503" id="Seg_7741" s="T502">mɔːt-tɨ</ta>
            <ta e="T504" id="Seg_7742" s="T503">qopɨ-lʼ</ta>
            <ta e="T505" id="Seg_7743" s="T504">mɔːt-tɨ</ta>
            <ta e="T506" id="Seg_7744" s="T505">tına</ta>
            <ta e="T507" id="Seg_7745" s="T506">wərqɨ-lɔːqɨ</ta>
            <ta e="T975" id="Seg_7746" s="T507">čʼesɨ-ŋɨ-tɨ</ta>
            <ta e="T976" id="Seg_7747" s="T509">*koŋɨ-mpɨ-äšɨk</ta>
            <ta e="T512" id="Seg_7748" s="T511">na</ta>
            <ta e="T513" id="Seg_7749" s="T512">ıllä-lɔːqɨ</ta>
            <ta e="T514" id="Seg_7750" s="T513">uːkɨ-al-tɨ-äšɨk</ta>
            <ta e="T517" id="Seg_7751" s="T516">na</ta>
            <ta e="T518" id="Seg_7752" s="T517">montɨ</ta>
            <ta e="T520" id="Seg_7753" s="T519">soma-k</ta>
            <ta e="T521" id="Seg_7754" s="T520">na</ta>
            <ta e="T522" id="Seg_7755" s="T521">*koŋɨ-mpɨ-ntɨ</ta>
            <ta e="T523" id="Seg_7756" s="T522">na</ta>
            <ta e="T980" id="Seg_7757" s="T523">*koŋɨ-altɨ-mpɨ-ntɨ</ta>
            <ta e="T525" id="Seg_7758" s="T524">ta-l</ta>
            <ta e="T526" id="Seg_7759" s="T525">ɨkɨ</ta>
            <ta e="T981" id="Seg_7760" s="T526">mortɨ-äšɨk</ta>
            <ta e="T528" id="Seg_7761" s="T527">qapı</ta>
            <ta e="T529" id="Seg_7762" s="T528">qok-kɨtɨ-lʼ</ta>
            <ta e="T531" id="Seg_7763" s="T530">qapı</ta>
            <ta e="T532" id="Seg_7764" s="T531">üŋkɨl-tɨ-mpɨ-tɨ</ta>
            <ta e="T533" id="Seg_7765" s="T532">moʒet</ta>
            <ta e="T534" id="Seg_7766" s="T533">qoškɔːl</ta>
            <ta e="T983" id="Seg_7767" s="T534">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T974" id="Seg_7768" s="T535">əːtɨ-kɔːlɨ</ta>
            <ta e="T537" id="Seg_7769" s="T536">nɨːnɨ</ta>
            <ta e="T538" id="Seg_7770" s="T537">šittälʼ</ta>
            <ta e="T539" id="Seg_7771" s="T538">šäqqɨ-j</ta>
            <ta e="T540" id="Seg_7772" s="T539">na</ta>
            <ta e="T541" id="Seg_7773" s="T540">iːja</ta>
            <ta e="T542" id="Seg_7774" s="T541">ima-ntɨ-sä</ta>
            <ta e="T543" id="Seg_7775" s="T542">šäqqɨ-tɨt</ta>
            <ta e="T544" id="Seg_7776" s="T543">toːp-tɨlʼ</ta>
            <ta e="T545" id="Seg_7777" s="T544">qarɨ</ta>
            <ta e="T546" id="Seg_7778" s="T545">nılʼčʼɨ-lʼ</ta>
            <ta e="T547" id="Seg_7779" s="T546">ɛsɨ</ta>
            <ta e="T548" id="Seg_7780" s="T547">tına</ta>
            <ta e="T549" id="Seg_7781" s="T548">ima-ntɨ-nɨ</ta>
            <ta e="T550" id="Seg_7782" s="T549">man</ta>
            <ta e="T551" id="Seg_7783" s="T550">mɨta</ta>
            <ta e="T552" id="Seg_7784" s="T551">nʼennä</ta>
            <ta e="T553" id="Seg_7785" s="T552">qum-iː-qäk</ta>
            <ta e="T554" id="Seg_7786" s="T553">*qonı-ša-k</ta>
            <ta e="T555" id="Seg_7787" s="T554">qaj</ta>
            <ta e="T556" id="Seg_7788" s="T555">mɨta</ta>
            <ta e="T557" id="Seg_7789" s="T556">mɔːt-ɨ-k-mpɨ-tɨt</ta>
            <ta e="T558" id="Seg_7790" s="T557">qaj</ta>
            <ta e="T559" id="Seg_7791" s="T558">qattɨ-mpɨ-tɨt</ta>
            <ta e="T560" id="Seg_7792" s="T559">nʼennä</ta>
            <ta e="T561" id="Seg_7793" s="T560">laqaltɨ-ɛː</ta>
            <ta e="T562" id="Seg_7794" s="T561">nɔːkɨr</ta>
            <ta e="T563" id="Seg_7795" s="T562">qoptɨ-m</ta>
            <ta e="T564" id="Seg_7796" s="T563">sɔːrɨ-lä</ta>
            <ta e="T565" id="Seg_7797" s="T564">qapı</ta>
            <ta e="T566" id="Seg_7798" s="T565">nɔːkɨr-sar-ɨ-lʼ</ta>
            <ta e="T567" id="Seg_7799" s="T566">ɔːtä</ta>
            <ta e="T568" id="Seg_7800" s="T567">qalɨ-tɨ-mpɨ-ntɨ-tɨ</ta>
            <ta e="T569" id="Seg_7801" s="T568">na</ta>
            <ta e="T570" id="Seg_7802" s="T569">iːja</ta>
            <ta e="T571" id="Seg_7803" s="T570">nʼennä</ta>
            <ta e="T572" id="Seg_7804" s="T571">qən-ɛː-sɨ</ta>
            <ta e="T573" id="Seg_7805" s="T572">ima-tɨ</ta>
            <ta e="T574" id="Seg_7806" s="T573">ontɨ</ta>
            <ta e="T575" id="Seg_7807" s="T574">qalɨ</ta>
            <ta e="T576" id="Seg_7808" s="T575">pɛlɨ-kɔːlɨ</ta>
            <ta e="T577" id="Seg_7809" s="T576">na</ta>
            <ta e="T578" id="Seg_7810" s="T577">ɔːmtɨ-ŋɨ</ta>
            <ta e="T579" id="Seg_7811" s="T578">ompä</ta>
            <ta e="T580" id="Seg_7812" s="T579">na</ta>
            <ta e="T582" id="Seg_7813" s="T581">ukkɨr</ta>
            <ta e="T583" id="Seg_7814" s="T582">to-n</ta>
            <ta e="T584" id="Seg_7815" s="T583">čʼontɨ-n</ta>
            <ta e="T585" id="Seg_7816" s="T584">čʼeːlɨ-tɨ</ta>
            <ta e="T586" id="Seg_7817" s="T585">üːtɨ-ntɨ</ta>
            <ta e="T587" id="Seg_7818" s="T586">na</ta>
            <ta e="T588" id="Seg_7819" s="T587">qən-ntɨ</ta>
            <ta e="T589" id="Seg_7820" s="T588">üːtɨ-ntɨ-qɨn</ta>
            <ta e="T590" id="Seg_7821" s="T589">na</ta>
            <ta e="T591" id="Seg_7822" s="T590">iːja</ta>
            <ta e="T592" id="Seg_7823" s="T591">*lʼamɨ-k</ta>
            <ta e="T593" id="Seg_7824" s="T592">ɛː-ŋɨ</ta>
            <ta e="T594" id="Seg_7825" s="T593">qapı</ta>
            <ta e="T595" id="Seg_7826" s="T594">nʼennä</ta>
            <ta e="T596" id="Seg_7827" s="T595">ukkɨr</ta>
            <ta e="T597" id="Seg_7828" s="T596">to-n</ta>
            <ta e="T598" id="Seg_7829" s="T597">čʼontɨ-n</ta>
            <ta e="T599" id="Seg_7830" s="T598">mɨ</ta>
            <ta e="T600" id="Seg_7831" s="T599">qum</ta>
            <ta e="T601" id="Seg_7832" s="T600">na</ta>
            <ta e="T602" id="Seg_7833" s="T601">jam</ta>
            <ta e="T603" id="Seg_7834" s="T602">taːtɨ-ntɨ-tɨt</ta>
            <ta e="T604" id="Seg_7835" s="T603">takkɨ-ntɨt</ta>
            <ta e="T605" id="Seg_7836" s="T604">Pan</ta>
            <ta e="T606" id="Seg_7837" s="T605">ira-lʼ</ta>
            <ta e="T967" id="Seg_7838" s="T606">na</ta>
            <ta e="T607" id="Seg_7839" s="T967">jam</ta>
            <ta e="T608" id="Seg_7840" s="T607">taːtɨ-ntɨ-tɨt</ta>
            <ta e="T609" id="Seg_7841" s="T608">köt</ta>
            <ta e="T610" id="Seg_7842" s="T609">qum-iː-tɨ</ta>
            <ta e="T611" id="Seg_7843" s="T610">qapı</ta>
            <ta e="T612" id="Seg_7844" s="T611">ɔːtä-m</ta>
            <ta e="T613" id="Seg_7845" s="T612">tɔːqqɨ-mpɨlʼ</ta>
            <ta e="T614" id="Seg_7846" s="T613">Pan</ta>
            <ta e="T615" id="Seg_7847" s="T614">ira</ta>
            <ta e="T616" id="Seg_7848" s="T615">naššak</ta>
            <ta e="T617" id="Seg_7849" s="T616">qo-ntɨlʼ</ta>
            <ta e="T618" id="Seg_7850" s="T617">ɛː-ŋɨ</ta>
            <ta e="T619" id="Seg_7851" s="T618">naššak</ta>
            <ta e="T620" id="Seg_7852" s="T619">qəːtɨ-sä</ta>
            <ta e="T621" id="Seg_7853" s="T620">ɛː-ŋɨ</ta>
            <ta e="T622" id="Seg_7854" s="T621">nɨːnɨ</ta>
            <ta e="T623" id="Seg_7855" s="T622">šittälʼ</ta>
            <ta e="T624" id="Seg_7856" s="T623">ta-l</ta>
            <ta e="T625" id="Seg_7857" s="T624">na</ta>
            <ta e="T626" id="Seg_7858" s="T625">iːja</ta>
            <ta e="T627" id="Seg_7859" s="T626">tü-lʼčʼɨ-kunä</ta>
            <ta e="T628" id="Seg_7860" s="T627">ponä</ta>
            <ta e="T629" id="Seg_7861" s="T628">ima-tɨ</ta>
            <ta e="T630" id="Seg_7862" s="T629">üŋkɨl-tɨ-mpɨ</ta>
            <ta e="T631" id="Seg_7863" s="T630">tü-lʼčʼɨ-kunä</ta>
            <ta e="T632" id="Seg_7864" s="T631">na</ta>
            <ta e="T633" id="Seg_7865" s="T632">ɔːtä-m-tɨ</ta>
            <ta e="T634" id="Seg_7866" s="T633">toː</ta>
            <ta e="T635" id="Seg_7867" s="T634">sɔːrɨ</ta>
            <ta e="T636" id="Seg_7868" s="T635">ɔːtä-m-tɨ</ta>
            <ta e="T638" id="Seg_7869" s="T637">montɨ</ta>
            <ta e="T639" id="Seg_7870" s="T638">čʼam</ta>
            <ta e="T640" id="Seg_7871" s="T639">tü-lʼčʼɨ</ta>
            <ta e="T641" id="Seg_7872" s="T640">montɨ</ta>
            <ta e="T642" id="Seg_7873" s="T641">Ponʼi</ta>
            <ta e="T643" id="Seg_7874" s="T642">ira-lʼ</ta>
            <ta e="T644" id="Seg_7875" s="T643">mɨ-t</ta>
            <ta e="T645" id="Seg_7876" s="T644">qaj</ta>
            <ta e="T646" id="Seg_7877" s="T645">montɨ</ta>
            <ta e="T647" id="Seg_7878" s="T646">tü-ŋɨ-tɨt</ta>
            <ta e="T648" id="Seg_7879" s="T647">qaqlɨ-lʼ</ta>
            <ta e="T649" id="Seg_7880" s="T648">mɨ-t-ɨ-tɨ</ta>
            <ta e="T650" id="Seg_7881" s="T649">nık</ta>
            <ta e="T651" id="Seg_7882" s="T650">tottɨ-alɨ-mpɨ-tɨt</ta>
            <ta e="T652" id="Seg_7883" s="T651">tına</ta>
            <ta e="T653" id="Seg_7884" s="T652">ətɨmantɨ-n</ta>
            <ta e="T654" id="Seg_7885" s="T653">təp-ɨ-n</ta>
            <ta e="T655" id="Seg_7886" s="T654">ətɨmantɨ-n</ta>
            <ta e="T656" id="Seg_7887" s="T655">šeːr-ŋɨ</ta>
            <ta e="T657" id="Seg_7888" s="T656">mɔːt</ta>
            <ta e="T658" id="Seg_7889" s="T657">šeːr-ŋɨ</ta>
            <ta e="T659" id="Seg_7890" s="T658">mɔːta-n</ta>
            <ta e="T660" id="Seg_7891" s="T659">ɔːŋ-n</ta>
            <ta e="T661" id="Seg_7892" s="T660">nɨŋ-kkɨ-lä</ta>
            <ta e="T662" id="Seg_7893" s="T661">qälɨk</ta>
            <ta e="T663" id="Seg_7894" s="T662">tokalʼ</ta>
            <ta e="T664" id="Seg_7895" s="T663">mɔːta-n</ta>
            <ta e="T665" id="Seg_7896" s="T664">ɔːŋ-n</ta>
            <ta e="T666" id="Seg_7897" s="T665">qät</ta>
            <ta e="T667" id="Seg_7898" s="T666">topɨ-m-tɨ</ta>
            <ta e="T668" id="Seg_7899" s="T667">*tupɨ-ätɔːl-ŋɨ</ta>
            <ta e="T669" id="Seg_7900" s="T668">peːmɨ-m-tɨ</ta>
            <ta e="T670" id="Seg_7901" s="T669">na</ta>
            <ta e="T671" id="Seg_7902" s="T670">*tupɨ-ätɔːl-tɨ</ta>
            <ta e="T672" id="Seg_7903" s="T671">qantɨ-mpɨlʼ</ta>
            <ta e="T673" id="Seg_7904" s="T672">pɛläk-lʼ</ta>
            <ta e="T674" id="Seg_7905" s="T673">peːmɨ-tɨ</ta>
            <ta e="T675" id="Seg_7906" s="T674">*tupɨ-ätɔːl-ntɨ-tɨ</ta>
            <ta e="T676" id="Seg_7907" s="T675">nık</ta>
            <ta e="T677" id="Seg_7908" s="T676">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T678" id="Seg_7909" s="T677">nʼennä</ta>
            <ta e="T679" id="Seg_7910" s="T678">*korɨ-ätɔːl-lä</ta>
            <ta e="T680" id="Seg_7911" s="T679">Ponʼi</ta>
            <ta e="T681" id="Seg_7912" s="T680">ira-nɨ</ta>
            <ta e="T682" id="Seg_7913" s="T681">mɨta</ta>
            <ta e="T683" id="Seg_7914" s="T682">ɔːŋ-lɨ</ta>
            <ta e="T684" id="Seg_7915" s="T683">mɨta</ta>
            <ta e="T685" id="Seg_7916" s="T684">mɨta</ta>
            <ta e="T686" id="Seg_7917" s="T685">qolqan-lʼ</ta>
            <ta e="T687" id="Seg_7918" s="T686">olä</ta>
            <ta e="T688" id="Seg_7919" s="T687">mɨ</ta>
            <ta e="T689" id="Seg_7920" s="T688">ta-l</ta>
            <ta e="T690" id="Seg_7921" s="T689">na</ta>
            <ta e="T691" id="Seg_7922" s="T690">nʼennä</ta>
            <ta e="T692" id="Seg_7923" s="T691">paktɨ-mpɨ-ntɨ</ta>
            <ta e="T693" id="Seg_7924" s="T692">na</ta>
            <ta e="T694" id="Seg_7925" s="T693">kupak-sä</ta>
            <ta e="T695" id="Seg_7926" s="T694">təːqa-ätɔːl-ɛː-mpɨ-ntɨ-tɨ</ta>
            <ta e="T696" id="Seg_7927" s="T695">pɛläk-lʼ</ta>
            <ta e="T697" id="Seg_7928" s="T696">utɨ-n-tɨ</ta>
            <ta e="T698" id="Seg_7929" s="T697">təːqa-ätɔːl-ɛː-mpɨ-tɨ</ta>
            <ta e="T699" id="Seg_7930" s="T698">Ponʼi</ta>
            <ta e="T700" id="Seg_7931" s="T699">ira-lʼ</ta>
            <ta e="T701" id="Seg_7932" s="T700">mɨ-t-ɨ-m</ta>
            <ta e="T702" id="Seg_7933" s="T701">muntɨk</ta>
            <ta e="T703" id="Seg_7934" s="T702">läpäŋ</ta>
            <ta e="T704" id="Seg_7935" s="T703">qättɨ-mpɨ-tɨ</ta>
            <ta e="T705" id="Seg_7936" s="T704">ɔːmɨ-tɨ</ta>
            <ta e="T706" id="Seg_7937" s="T705">qu-mpɨ</ta>
            <ta e="T707" id="Seg_7938" s="T706">Ponʼi</ta>
            <ta e="T708" id="Seg_7939" s="T707">ira</ta>
            <ta e="T709" id="Seg_7940" s="T708">ilɨ-lä</ta>
            <ta e="T710" id="Seg_7941" s="T709">qalɨ-mpɨ-ntɨ</ta>
            <ta e="T711" id="Seg_7942" s="T710">šittɨ</ta>
            <ta e="T712" id="Seg_7943" s="T711">qälɨk-qı</ta>
            <ta e="T713" id="Seg_7944" s="T712">ilɨ-lä</ta>
            <ta e="T714" id="Seg_7945" s="T713">qalɨ-mpɨ-ntɨ</ta>
            <ta e="T715" id="Seg_7946" s="T714">ponä</ta>
            <ta e="T716" id="Seg_7947" s="T715">qəː-äl-ɛː-ntɨ</ta>
            <ta e="T717" id="Seg_7948" s="T716">na</ta>
            <ta e="T718" id="Seg_7949" s="T717">iːja</ta>
            <ta e="T719" id="Seg_7950" s="T718">ukkɨr</ta>
            <ta e="T720" id="Seg_7951" s="T719">pɔːrɨ</ta>
            <ta e="T721" id="Seg_7952" s="T720">kupak-sä</ta>
            <ta e="T722" id="Seg_7953" s="T721">təːqa-ätɔːl-ɛː-mpɨ-tɨ</ta>
            <ta e="T723" id="Seg_7954" s="T722">muntɨk</ta>
            <ta e="T724" id="Seg_7955" s="T723">moqɨnä-n</ta>
            <ta e="T725" id="Seg_7956" s="T724">porqɨ-n</ta>
            <ta e="T726" id="Seg_7957" s="T725">šünʼčʼɨ-ntɨ</ta>
            <ta e="T727" id="Seg_7958" s="T726">nʼölʼqɨ-mɔːt-mpɨ-tɨt</ta>
            <ta e="T728" id="Seg_7959" s="T727">na</ta>
            <ta e="T729" id="Seg_7960" s="T728">qu-mpɨlʼ</ta>
            <ta e="T730" id="Seg_7961" s="T729">mɨ</ta>
            <ta e="T731" id="Seg_7962" s="T730">pista</ta>
            <ta e="T732" id="Seg_7963" s="T731">nɨːnɨ</ta>
            <ta e="T733" id="Seg_7964" s="T732">šittälʼ</ta>
            <ta e="T734" id="Seg_7965" s="T733">ponä</ta>
            <ta e="T735" id="Seg_7966" s="T734">qəː-äl-ɛː-ŋɨ-tɨ</ta>
            <ta e="T736" id="Seg_7967" s="T735">ətɨmantɨ-ntɨ</ta>
            <ta e="T737" id="Seg_7968" s="T736">tıntä</ta>
            <ta e="T738" id="Seg_7969" s="T737">na</ta>
            <ta e="T739" id="Seg_7970" s="T738">qaqlɨ-n</ta>
            <ta e="T742" id="Seg_7971" s="T741">poː-qɨn</ta>
            <ta e="T743" id="Seg_7972" s="T742">na</ta>
            <ta e="T744" id="Seg_7973" s="T743">sɔːrɨ-alɨ-mpɨ-ntɨ-tɨt</ta>
            <ta e="T745" id="Seg_7974" s="T744">Ponʼi</ta>
            <ta e="T746" id="Seg_7975" s="T745">ira</ta>
            <ta e="T747" id="Seg_7976" s="T746">aj</ta>
            <ta e="T748" id="Seg_7977" s="T747">ponä</ta>
            <ta e="T749" id="Seg_7978" s="T748">iː-tɨ-mpɨ-tɨ</ta>
            <ta e="T750" id="Seg_7979" s="T749">qaj</ta>
            <ta e="T751" id="Seg_7980" s="T750">muntɨk</ta>
            <ta e="T752" id="Seg_7981" s="T751">ponä</ta>
            <ta e="T753" id="Seg_7982" s="T752">iː-tɨ-mpɨ-tɨ</ta>
            <ta e="T754" id="Seg_7983" s="T753">ıllä</ta>
            <ta e="T755" id="Seg_7984" s="T754">omtɨ</ta>
            <ta e="T756" id="Seg_7985" s="T755">tına</ta>
            <ta e="T757" id="Seg_7986" s="T756">malʼčʼä-lʼ</ta>
            <ta e="T758" id="Seg_7987" s="T757">tok-tɨ</ta>
            <ta e="T759" id="Seg_7988" s="T758">ıllä</ta>
            <ta e="T760" id="Seg_7989" s="T759">am-ɨ-r-lä</ta>
            <ta e="T762" id="Seg_7990" s="T761">ıllä</ta>
            <ta e="T763" id="Seg_7991" s="T762">šäqqɨ</ta>
            <ta e="T764" id="Seg_7992" s="T763">na</ta>
            <ta e="T765" id="Seg_7993" s="T764">qum-iː-tɨ</ta>
            <ta e="T766" id="Seg_7994" s="T765">poː-qɨn</ta>
            <ta e="T767" id="Seg_7995" s="T766">na</ta>
            <ta e="T768" id="Seg_7996" s="T767">ippɨ-qɨlɨ-mpɨ-ntɨ-tɨt</ta>
            <ta e="T769" id="Seg_7997" s="T768">nɔːtɨ</ta>
            <ta e="T770" id="Seg_7998" s="T769">sɔːrɨ-mpɨ-kol</ta>
            <ta e="T771" id="Seg_7999" s="T770">kuttar</ta>
            <ta e="T772" id="Seg_8000" s="T771">sɔːrɨ-alɨ-mpɨ</ta>
            <ta e="T773" id="Seg_8001" s="T772">nılʼčʼɨ-k</ta>
            <ta e="T774" id="Seg_8002" s="T773">šäqqɨ</ta>
            <ta e="T775" id="Seg_8003" s="T774">toːp-tɨlʼ</ta>
            <ta e="T776" id="Seg_8004" s="T775">qarɨ</ta>
            <ta e="T777" id="Seg_8005" s="T776">ınnä</ta>
            <ta e="T778" id="Seg_8006" s="T777">wəšɨ</ta>
            <ta e="T779" id="Seg_8007" s="T778">Ponʼi</ta>
            <ta e="T780" id="Seg_8008" s="T779">ira</ta>
            <ta e="T781" id="Seg_8009" s="T780">nılʼčʼɨ</ta>
            <ta e="T782" id="Seg_8010" s="T781">mɨta</ta>
            <ta e="T783" id="Seg_8011" s="T782">wəntɨ-lʼ</ta>
            <ta e="T784" id="Seg_8012" s="T783">olɨ-tɨ</ta>
            <ta e="T785" id="Seg_8013" s="T784">tɔːt</ta>
            <ta e="T786" id="Seg_8014" s="T785">taŋkɨ-ıː-mpɨ</ta>
            <ta e="T787" id="Seg_8015" s="T786">tına</ta>
            <ta e="T788" id="Seg_8016" s="T787">kekkɨsä</ta>
            <ta e="T789" id="Seg_8017" s="T788">sajɨ-ja-tɨ</ta>
            <ta e="T791" id="Seg_8018" s="T790">toːnna</ta>
            <ta e="T792" id="Seg_8019" s="T791">qälɨk-t</ta>
            <ta e="T794" id="Seg_8020" s="T793">ešo</ta>
            <ta e="T795" id="Seg_8021" s="T794">soma-lɔːqɨ</ta>
            <ta e="T796" id="Seg_8022" s="T795">ɛː-mpɨ</ta>
            <ta e="T797" id="Seg_8023" s="T796">nılʼčʼɨ-lʼ</ta>
            <ta e="T798" id="Seg_8024" s="T797">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T799" id="Seg_8025" s="T798">na</ta>
            <ta e="T800" id="Seg_8026" s="T799">iːja</ta>
            <ta e="T801" id="Seg_8027" s="T800">mɨta</ta>
            <ta e="T802" id="Seg_8028" s="T801">na</ta>
            <ta e="T803" id="Seg_8029" s="T802">qälɨk-t-ɨ-nkinı-ntɨ</ta>
            <ta e="T804" id="Seg_8030" s="T803">qum-iː-m-tɨ</ta>
            <ta e="T805" id="Seg_8031" s="T804">moqɨnä</ta>
            <ta e="T806" id="Seg_8032" s="T805">qən-tɨ-ätɨ</ta>
            <ta e="T807" id="Seg_8033" s="T806">mɨta</ta>
            <ta e="T808" id="Seg_8034" s="T807">qaqlɨ-qäntɨ</ta>
            <ta e="T809" id="Seg_8035" s="T808">tɛltɨ-ätɔːl-lä</ta>
            <ta e="T810" id="Seg_8036" s="T809">mɨta</ta>
            <ta e="T811" id="Seg_8037" s="T810">kurɨ-ätɔːl-lä</ta>
            <ta e="T812" id="Seg_8038" s="T811">ontɨt</ta>
            <ta e="T813" id="Seg_8039" s="T812">qaqlɨ-qɨn-ntɨt</ta>
            <ta e="T814" id="Seg_8040" s="T813">nɨːnɨ</ta>
            <ta e="T815" id="Seg_8041" s="T814">šittälʼ</ta>
            <ta e="T816" id="Seg_8042" s="T815">toː</ta>
            <ta e="T817" id="Seg_8043" s="T816">namantɨ</ta>
            <ta e="T818" id="Seg_8044" s="T817">ponä</ta>
            <ta e="T821" id="Seg_8045" s="T820">qapı</ta>
            <ta e="T822" id="Seg_8046" s="T821">mɔːt</ta>
            <ta e="T823" id="Seg_8047" s="T822">šeːr-ıː-š-qo</ta>
            <ta e="T824" id="Seg_8048" s="T823">aj</ta>
            <ta e="T825" id="Seg_8049" s="T824">qaj</ta>
            <ta e="T826" id="Seg_8050" s="T825">am-ɨ-r-qo</ta>
            <ta e="T827" id="Seg_8051" s="T826">aj</ta>
            <ta e="T828" id="Seg_8052" s="T827">qaj</ta>
            <ta e="T829" id="Seg_8053" s="T828">ašša</ta>
            <ta e="T830" id="Seg_8054" s="T829">am-ɨ-r-qo</ta>
            <ta e="T831" id="Seg_8055" s="T830">nɨːnɨ</ta>
            <ta e="T832" id="Seg_8056" s="T831">na</ta>
            <ta e="T833" id="Seg_8057" s="T832">qum</ta>
            <ta e="T834" id="Seg_8058" s="T833">tına</ta>
            <ta e="T835" id="Seg_8059" s="T834">sɔːrɨ-ätɔːl-mpɨlʼ</ta>
            <ta e="T836" id="Seg_8060" s="T835">qaqlɨ-ntɨ-tɨ</ta>
            <ta e="T837" id="Seg_8061" s="T836">tına</ta>
            <ta e="T838" id="Seg_8062" s="T837">nɨmtɨ</ta>
            <ta e="T839" id="Seg_8063" s="T838">moqɨnä</ta>
            <ta e="T840" id="Seg_8064" s="T839">qəːčʼelʼ-lä</ta>
            <ta e="T841" id="Seg_8065" s="T840">qum-iː-m-tɨ</ta>
            <ta e="T842" id="Seg_8066" s="T841">na</ta>
            <ta e="T843" id="Seg_8067" s="T842">qu-mpɨlʼ</ta>
            <ta e="T844" id="Seg_8068" s="T843">qum-iː-m-tɨ</ta>
            <ta e="T845" id="Seg_8069" s="T844">nık</ta>
            <ta e="T846" id="Seg_8070" s="T845">qən-tɨ-ŋɨ-tɨ</ta>
            <ta e="T847" id="Seg_8071" s="T846">moqɨnä</ta>
            <ta e="T848" id="Seg_8072" s="T847">takkɨ</ta>
            <ta e="T849" id="Seg_8073" s="T848">moqɨnä</ta>
            <ta e="T850" id="Seg_8074" s="T849">Ponʼi</ta>
            <ta e="T851" id="Seg_8075" s="T850">ira-m</ta>
            <ta e="T852" id="Seg_8076" s="T851">tɛ</ta>
            <ta e="T853" id="Seg_8077" s="T852">qəːčʼelʼ-lä</ta>
            <ta e="T854" id="Seg_8078" s="T853">qən-tɨ-ŋɨ-tɨ</ta>
            <ta e="T855" id="Seg_8079" s="T854">qapı</ta>
            <ta e="T856" id="Seg_8080" s="T855">kɨpaka</ta>
            <ta e="T857" id="Seg_8081" s="T856">kəjɨ-tɨ</ta>
            <ta e="T858" id="Seg_8082" s="T857">olä</ta>
            <ta e="T859" id="Seg_8083" s="T858">təmol</ta>
            <ta e="T860" id="Seg_8084" s="T859">qən-ŋɨ-tɨt</ta>
            <ta e="T861" id="Seg_8085" s="T860">tına</ta>
            <ta e="T862" id="Seg_8086" s="T861">nɨːnɨ</ta>
            <ta e="T863" id="Seg_8087" s="T862">soma-k</ta>
            <ta e="T864" id="Seg_8088" s="T863">qən-ŋɨ-tɨt</ta>
            <ta e="T865" id="Seg_8089" s="T864">nɨmtɨ</ta>
            <ta e="T866" id="Seg_8090" s="T865">ima-ntɨ-nɨ</ta>
            <ta e="T867" id="Seg_8091" s="T866">nık</ta>
            <ta e="T868" id="Seg_8092" s="T867">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T869" id="Seg_8093" s="T868">mɔːt-lɨ</ta>
            <ta e="T870" id="Seg_8094" s="T869">toː</ta>
            <ta e="T871" id="Seg_8095" s="T870">tılɨ-ätɨ</ta>
            <ta e="T872" id="Seg_8096" s="T871">meː</ta>
            <ta e="T873" id="Seg_8097" s="T872">nʼennä</ta>
            <ta e="T874" id="Seg_8098" s="T873">qən-ntɨ-psɔːt-mıː</ta>
            <ta e="T875" id="Seg_8099" s="T874">qum-ɨ-t-ɨ-nkinı</ta>
            <ta e="T876" id="Seg_8100" s="T875">ašša</ta>
            <ta e="T877" id="Seg_8101" s="T876">tɛnɨmɨ-mpɨ-tɨt</ta>
            <ta e="T878" id="Seg_8102" s="T877">qapı</ta>
            <ta e="T879" id="Seg_8103" s="T878">ontɨt</ta>
            <ta e="T880" id="Seg_8104" s="T879">na</ta>
            <ta e="T881" id="Seg_8105" s="T880">ilʼčʼa-ntɨ-lʼ</ta>
            <ta e="T882" id="Seg_8106" s="T881">mɨ-t</ta>
            <ta e="T883" id="Seg_8107" s="T882">kos</ta>
            <ta e="T884" id="Seg_8108" s="T883">qaj</ta>
            <ta e="T885" id="Seg_8109" s="T884">nʼennä</ta>
            <ta e="T886" id="Seg_8110" s="T885">qən-ŋɨ</ta>
            <ta e="T887" id="Seg_8111" s="T886">tına</ta>
            <ta e="T888" id="Seg_8112" s="T887">ontɨ</ta>
            <ta e="T889" id="Seg_8113" s="T888">ašša</ta>
            <ta e="T890" id="Seg_8114" s="T889">*koŋɨ-mpɨ</ta>
            <ta e="T892" id="Seg_8115" s="T891">toːnna</ta>
            <ta e="T893" id="Seg_8116" s="T892">ima-tɨ</ta>
            <ta e="T894" id="Seg_8117" s="T893">na</ta>
            <ta e="T895" id="Seg_8118" s="T894">kətɨ-ɛntɨ-tɨ</ta>
            <ta e="T896" id="Seg_8119" s="T895">əsɨ-n-tɨ</ta>
            <ta e="T897" id="Seg_8120" s="T896">ilʼčʼa-n-ntɨ</ta>
            <ta e="T898" id="Seg_8121" s="T897">mɨ-qı-nkinı</ta>
            <ta e="T899" id="Seg_8122" s="T898">Ponʼi</ta>
            <ta e="T900" id="Seg_8123" s="T899">ira-lʼ</ta>
            <ta e="T901" id="Seg_8124" s="T900">mɨ-t</ta>
            <ta e="T902" id="Seg_8125" s="T901">tına</ta>
            <ta e="T903" id="Seg_8126" s="T902">moqɨnä</ta>
            <ta e="T904" id="Seg_8127" s="T903">*qonı-sɨ-tɨt</ta>
            <ta e="T905" id="Seg_8128" s="T904">mompa</ta>
            <ta e="T906" id="Seg_8129" s="T905">ta-l</ta>
            <ta e="T907" id="Seg_8130" s="T906">nɨːnɨ</ta>
            <ta e="T908" id="Seg_8131" s="T907">šittälʼ</ta>
            <ta e="T909" id="Seg_8132" s="T908">pija</ta>
            <ta e="T910" id="Seg_8133" s="T909">qapı</ta>
            <ta e="T912" id="Seg_8134" s="T911">ašša</ta>
            <ta e="T985" id="Seg_8135" s="T912">tɛnɨmɨ-tɨt</ta>
            <ta e="T914" id="Seg_8136" s="T913">karrä</ta>
            <ta e="T986" id="Seg_8137" s="T914">*koŋɨ-mpɨ-äšɨk</ta>
            <ta e="T916" id="Seg_8138" s="T915">karrä</ta>
            <ta e="T987" id="Seg_8139" s="T916">radio-qäntɨ</ta>
            <ta e="T918" id="Seg_8140" s="T917">takkɨ</ta>
            <ta e="T919" id="Seg_8141" s="T918">nʼennä</ta>
            <ta e="T920" id="Seg_8142" s="T919">ɨpalnoj</ta>
            <ta e="T921" id="Seg_8143" s="T920">təp-ɨ-ja-t</ta>
            <ta e="T922" id="Seg_8144" s="T921">näčʼčʼä</ta>
            <ta e="T923" id="Seg_8145" s="T922">ilʼčʼa-n-ntɨ</ta>
            <ta e="T924" id="Seg_8146" s="T923">mɔːt</ta>
            <ta e="T925" id="Seg_8147" s="T924">mɔːt</ta>
            <ta e="T926" id="Seg_8148" s="T925">šeːr-ŋɨ</ta>
            <ta e="T927" id="Seg_8149" s="T926">qaqlɨ-t</ta>
            <ta e="T928" id="Seg_8150" s="T927">to</ta>
            <ta e="T929" id="Seg_8151" s="T928">nıːmɨt</ta>
            <ta e="T930" id="Seg_8152" s="T929">kurɨ-lä</ta>
            <ta e="T931" id="Seg_8153" s="T930">nık</ta>
            <ta e="T932" id="Seg_8154" s="T931">tottɨ-ŋɨ-tɨ</ta>
            <ta e="T933" id="Seg_8155" s="T932">toːnna</ta>
            <ta e="T934" id="Seg_8156" s="T933">qən-mpɨ-ntɨ-tɨt</ta>
            <ta e="T935" id="Seg_8157" s="T934">moqɨnä</ta>
            <ta e="T936" id="Seg_8158" s="T935">qən-mpɨ-ntɨ-tɨt</ta>
            <ta e="T937" id="Seg_8159" s="T936">Ponʼi</ta>
            <ta e="T938" id="Seg_8160" s="T937">ira</ta>
            <ta e="T939" id="Seg_8161" s="T938">kekkɨsä</ta>
            <ta e="T940" id="Seg_8162" s="T939">takkɨ-n</ta>
            <ta e="T941" id="Seg_8163" s="T940">mɔːt-qɨn</ta>
            <ta e="T942" id="Seg_8164" s="T941">tulɨ-š-mpɨ</ta>
            <ta e="T943" id="Seg_8165" s="T942">ıllä</ta>
            <ta e="T944" id="Seg_8166" s="T943">qu-mpɨ</ta>
            <ta e="T945" id="Seg_8167" s="T944">toːnna</ta>
            <ta e="T946" id="Seg_8168" s="T945">šittɨ</ta>
            <ta e="T947" id="Seg_8169" s="T946">qum-ɨ-qı</ta>
            <ta e="T948" id="Seg_8170" s="T947">tıː</ta>
            <ta e="T949" id="Seg_8171" s="T948">ilɨ-mpɨ-qı</ta>
            <ta e="T950" id="Seg_8172" s="T949">Ponʼi</ta>
            <ta e="T951" id="Seg_8173" s="T950">ira</ta>
            <ta e="T952" id="Seg_8174" s="T951">Ponʼi</ta>
            <ta e="T953" id="Seg_8175" s="T952">ira-lʼ</ta>
            <ta e="T954" id="Seg_8176" s="T953">mɨ-t</ta>
            <ta e="T955" id="Seg_8177" s="T954">nɨmtɨ</ta>
            <ta e="T988" id="Seg_8178" s="T955">ɛː-ŋɨ</ta>
            <ta e="T957" id="Seg_8179" s="T956">jarɨk</ta>
            <ta e="T958" id="Seg_8180" s="T957">mɨ</ta>
            <ta e="T959" id="Seg_8181" s="T958">kətɨ-ätɨ</ta>
            <ta e="T960" id="Seg_8182" s="T959">jarɨk</ta>
            <ta e="T989" id="Seg_8183" s="T960">jarɨk</ta>
            <ta e="T962" id="Seg_8184" s="T961">jarɨk</ta>
            <ta e="T963" id="Seg_8185" s="T962">mɨ</ta>
            <ta e="T964" id="Seg_8186" s="T963">kətɨ-ätɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T3" id="Seg_8187" s="T2">one</ta>
            <ta e="T4" id="Seg_8188" s="T3">child.[NOM]-3SG</ta>
            <ta e="T5" id="Seg_8189" s="T4">be-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T7" id="Seg_8190" s="T6">this</ta>
            <ta e="T8" id="Seg_8191" s="T7">INDEF3</ta>
            <ta e="T9" id="Seg_8192" s="T8">after.all</ta>
            <ta e="T11" id="Seg_8193" s="T10">after.all</ta>
            <ta e="T12" id="Seg_8194" s="T11">say-HAB-1SG.S</ta>
            <ta e="T13" id="Seg_8195" s="T12">this-ACC</ta>
            <ta e="T14" id="Seg_8196" s="T13">this</ta>
            <ta e="T15" id="Seg_8197" s="T14">true.story.[3SG.S]</ta>
            <ta e="T16" id="Seg_8198" s="T15">this</ta>
            <ta e="T17" id="Seg_8199" s="T16">true.story.[3SG.S]</ta>
            <ta e="T18" id="Seg_8200" s="T17">NEG</ta>
            <ta e="T19" id="Seg_8201" s="T18">tale.[3SG.S]</ta>
            <ta e="T21" id="Seg_8202" s="T20">you.SG.GEN</ta>
            <ta e="T22" id="Seg_8203" s="T21">below</ta>
            <ta e="T24" id="Seg_8204" s="T23">Selkup-EP-PL.[NOM]</ta>
            <ta e="T25" id="Seg_8205" s="T24">there</ta>
            <ta e="T27" id="Seg_8206" s="T26">this</ta>
            <ta e="T28" id="Seg_8207" s="T27">here</ta>
            <ta e="T29" id="Seg_8208" s="T28">you.SG.ACC</ta>
            <ta e="T30" id="Seg_8209" s="T29">see-PFV-FUT.[3SG.S]</ta>
            <ta e="T32" id="Seg_8210" s="T31">you.SG.NOM</ta>
            <ta e="T33" id="Seg_8211" s="T32">speech.[NOM]-2SG</ta>
            <ta e="T971" id="Seg_8212" s="T33">stop-TR-PFV-INF-%%-IPFV-DUR-IMP.2SG.O</ta>
            <ta e="T972" id="Seg_8213" s="T34">carry-DETR-TR-IMP.2SG.O</ta>
            <ta e="T36" id="Seg_8214" s="T35">middle-DIM-ADVZ</ta>
            <ta e="T973" id="Seg_8215" s="T36">say-IMP.2SG.O</ta>
            <ta e="T38" id="Seg_8216" s="T37">%%</ta>
            <ta e="T39" id="Seg_8217" s="T38">this</ta>
            <ta e="T40" id="Seg_8218" s="T39">old.man.[NOM]</ta>
            <ta e="T41" id="Seg_8219" s="T40">live-PST.NAR-3SG.S</ta>
            <ta e="T42" id="Seg_8220" s="T41">be.sick-INCH-PST.NAR.[3SG.S]</ta>
            <ta e="T43" id="Seg_8221" s="T42">this</ta>
            <ta e="T44" id="Seg_8222" s="T43">Selkup-EP-ADJZ</ta>
            <ta e="T45" id="Seg_8223" s="T44">old.man.[3SG.S]</ta>
            <ta e="T46" id="Seg_8224" s="T45">located.on.this.side</ta>
            <ta e="T47" id="Seg_8225" s="T46">side-ADJZ</ta>
            <ta e="T48" id="Seg_8226" s="T47">storehouse-3SG.O</ta>
            <ta e="T49" id="Seg_8227" s="T48">to.the.other.side-ADJZ</ta>
            <ta e="T50" id="Seg_8228" s="T49">down.the.river-ADJZ</ta>
            <ta e="T51" id="Seg_8229" s="T50">big.river.[NOM]</ta>
            <ta e="T52" id="Seg_8230" s="T51">side-LOC-ADJZ</ta>
            <ta e="T53" id="Seg_8231" s="T52">live-PST.NAR.[3SG.S]</ta>
            <ta e="T54" id="Seg_8232" s="T53">there-ADV.LOC-ADJZ</ta>
            <ta e="T55" id="Seg_8233" s="T54">(s)he.[NOM]</ta>
            <ta e="T56" id="Seg_8234" s="T55">tree-CAR-ADJZ</ta>
            <ta e="T57" id="Seg_8235" s="T56">earth-GEN</ta>
            <ta e="T992" id="Seg_8236" s="T58">what.[NOM]</ta>
            <ta e="T60" id="Seg_8237" s="T59">live-PST.NAR.[3SG.S]</ta>
            <ta e="T61" id="Seg_8238" s="T60">that</ta>
            <ta e="T62" id="Seg_8239" s="T61">be.sick-INCH-PST.NAR.[3SG.S]</ta>
            <ta e="T63" id="Seg_8240" s="T62">son.[NOM]-3SG</ta>
            <ta e="T64" id="Seg_8241" s="T63">one</ta>
            <ta e="T65" id="Seg_8242" s="T64">son.[NOM]-3SG</ta>
            <ta e="T66" id="Seg_8243" s="T65">friend-CAR-ADJZ</ta>
            <ta e="T67" id="Seg_8244" s="T66">someone-CAR-ADJZ</ta>
            <ta e="T68" id="Seg_8245" s="T67">friend.[NOM]-3SG</ta>
            <ta e="T69" id="Seg_8246" s="T68">NEG</ta>
            <ta e="T70" id="Seg_8247" s="T69">daughter.[NOM]-3SG</ta>
            <ta e="T71" id="Seg_8248" s="T70">NEG.EX.[3SG.S]</ta>
            <ta e="T72" id="Seg_8249" s="T71">NEG</ta>
            <ta e="T73" id="Seg_8250" s="T72">what.[NOM]-3SG</ta>
            <ta e="T74" id="Seg_8251" s="T73">NEG.EX.[3SG.S]</ta>
            <ta e="T75" id="Seg_8252" s="T74">then</ta>
            <ta e="T76" id="Seg_8253" s="T75">this</ta>
            <ta e="T77" id="Seg_8254" s="T76">father.[NOM]-3SG</ta>
            <ta e="T78" id="Seg_8255" s="T77">be.sick-INCH-CO.[3SG.S]</ta>
            <ta e="T79" id="Seg_8256" s="T78">this</ta>
            <ta e="T80" id="Seg_8257" s="T79">child-DIM.[NOM]</ta>
            <ta e="T82" id="Seg_8258" s="T81">this</ta>
            <ta e="T83" id="Seg_8259" s="T82">guy-GEN</ta>
            <ta e="T84" id="Seg_8260" s="T83">there</ta>
            <ta e="T85" id="Seg_8261" s="T84">live.[3SG.S]</ta>
            <ta e="T86" id="Seg_8262" s="T85">supposedly</ta>
            <ta e="T87" id="Seg_8263" s="T86">this</ta>
            <ta e="T88" id="Seg_8264" s="T87">father-GEN-3SG</ta>
            <ta e="T89" id="Seg_8265" s="T88">mother-GEN-3SG</ta>
            <ta e="T90" id="Seg_8266" s="T89">curtains-LOC</ta>
            <ta e="T91" id="Seg_8267" s="T90">there-ADV.ILL</ta>
            <ta e="T92" id="Seg_8268" s="T91">that</ta>
            <ta e="T93" id="Seg_8269" s="T92">two</ta>
            <ta e="T94" id="Seg_8270" s="T93">Nenets.[NOM]</ta>
            <ta e="T95" id="Seg_8271" s="T94">brother-DYA-DU.[NOM]</ta>
            <ta e="T96" id="Seg_8272" s="T95">be-PST.NAR-INFER-3DU.S</ta>
            <ta e="T97" id="Seg_8273" s="T96">Nenets.[NOM]</ta>
            <ta e="T98" id="Seg_8274" s="T97">old.man.[NOM]</ta>
            <ta e="T99" id="Seg_8275" s="T98">where</ta>
            <ta e="T100" id="Seg_8276" s="T99">be-FUT.[3SG.S]</ta>
            <ta e="T102" id="Seg_8277" s="T101">whether.[NOM]</ta>
            <ta e="T103" id="Seg_8278" s="T102">live.[3SG.S]</ta>
            <ta e="T104" id="Seg_8279" s="T103">where</ta>
            <ta e="T105" id="Seg_8280" s="T104">%%</ta>
            <ta e="T106" id="Seg_8281" s="T105">that</ta>
            <ta e="T107" id="Seg_8282" s="T106">this</ta>
            <ta e="T108" id="Seg_8283" s="T107">old.man.[NOM]</ta>
            <ta e="T109" id="Seg_8284" s="T108">down</ta>
            <ta e="T110" id="Seg_8285" s="T109">die-CO.[3SG.S]</ta>
            <ta e="T111" id="Seg_8286" s="T110">down</ta>
            <ta e="T112" id="Seg_8287" s="T111">die-CO.[3SG.S]</ta>
            <ta e="T113" id="Seg_8288" s="T112">that</ta>
            <ta e="T114" id="Seg_8289" s="T113">then</ta>
            <ta e="T115" id="Seg_8290" s="T114">then</ta>
            <ta e="T116" id="Seg_8291" s="T115">that</ta>
            <ta e="T117" id="Seg_8292" s="T116">guy.[NOM]</ta>
            <ta e="T118" id="Seg_8293" s="T117">Nenets.[NOM]</ta>
            <ta e="T119" id="Seg_8294" s="T118">old.man.[NOM]</ta>
            <ta e="T120" id="Seg_8295" s="T119">then</ta>
            <ta e="T121" id="Seg_8296" s="T120">where</ta>
            <ta e="T122" id="Seg_8297" s="T121">where.to.go-FUT.[3SG.S]</ta>
            <ta e="T123" id="Seg_8298" s="T122">there</ta>
            <ta e="T124" id="Seg_8299" s="T123">brother-OBL.3SG-%%</ta>
            <ta e="T125" id="Seg_8300" s="T124">come-US-CO.[3SG.S]</ta>
            <ta e="T126" id="Seg_8301" s="T125">old.man.[NOM]</ta>
            <ta e="T127" id="Seg_8302" s="T126">die-PST.NAR.[3SG.S]</ta>
            <ta e="T128" id="Seg_8303" s="T127">down</ta>
            <ta e="T129" id="Seg_8304" s="T128">make-CO-3PL</ta>
            <ta e="T130" id="Seg_8305" s="T129">then</ta>
            <ta e="T131" id="Seg_8306" s="T130">wife.[NOM]-3SG</ta>
            <ta e="T132" id="Seg_8307" s="T131">that</ta>
            <ta e="T133" id="Seg_8308" s="T132">wife.[NOM]-3SG</ta>
            <ta e="T134" id="Seg_8309" s="T133">be.sick-INCH-CO.[3SG.S]</ta>
            <ta e="T135" id="Seg_8310" s="T134">wife.[NOM]-3SG</ta>
            <ta e="T136" id="Seg_8311" s="T135">also</ta>
            <ta e="T137" id="Seg_8312" s="T136">down</ta>
            <ta e="T138" id="Seg_8313" s="T137">die-CO.[3SG.S]</ta>
            <ta e="T139" id="Seg_8314" s="T138">old.man-ADJZ</ta>
            <ta e="T140" id="Seg_8315" s="T139">something-DU.[NOM]</ta>
            <ta e="T141" id="Seg_8316" s="T140">that</ta>
            <ta e="T142" id="Seg_8317" s="T141">son.[NOM]-3SG</ta>
            <ta e="T143" id="Seg_8318" s="T142">somebody-CAR</ta>
            <ta e="T144" id="Seg_8319" s="T143">friend-CAR</ta>
            <ta e="T145" id="Seg_8320" s="T144">stay.[3SG.S]</ta>
            <ta e="T146" id="Seg_8321" s="T145">where</ta>
            <ta e="T147" id="Seg_8322" s="T146">where.to.go-FUT.[3SG.S]</ta>
            <ta e="T148" id="Seg_8323" s="T147">this</ta>
            <ta e="T149" id="Seg_8324" s="T148">Nenets.[NOM]</ta>
            <ta e="T150" id="Seg_8325" s="T149">old.man.[NOM]</ta>
            <ta e="T151" id="Seg_8326" s="T150">then</ta>
            <ta e="T152" id="Seg_8327" s="T151">then</ta>
            <ta e="T153" id="Seg_8328" s="T152">to.the.other.side</ta>
            <ta e="T154" id="Seg_8329" s="T153">cross-DRV-PST.NAR-3SG.O</ta>
            <ta e="T155" id="Seg_8330" s="T154">this</ta>
            <ta e="T156" id="Seg_8331" s="T155">orphan-ADJZ</ta>
            <ta e="T157" id="Seg_8332" s="T156">guy.[NOM]</ta>
            <ta e="T158" id="Seg_8333" s="T157">big</ta>
            <ta e="T159" id="Seg_8334" s="T158">son.[NOM]-3SG</ta>
            <ta e="T167" id="Seg_8335" s="T166">whichever</ta>
            <ta e="T168" id="Seg_8336" s="T167">Gena-ADJZ</ta>
            <ta e="T169" id="Seg_8337" s="T168">something-GEN</ta>
            <ta e="T170" id="Seg_8338" s="T169">as</ta>
            <ta e="T171" id="Seg_8339" s="T170">such-ADJZ</ta>
            <ta e="T172" id="Seg_8340" s="T171">son.[NOM]-3SG-ADJZ</ta>
            <ta e="T173" id="Seg_8341" s="T172">be-PST.NAR.[3SG.S]</ta>
            <ta e="T174" id="Seg_8342" s="T173">to.the.other.side</ta>
            <ta e="T175" id="Seg_8343" s="T174">cross-TR-CO-3SG.O</ta>
            <ta e="T176" id="Seg_8344" s="T175">reindeer.[NOM]-ILL-ADJZ</ta>
            <ta e="T177" id="Seg_8345" s="T176">NEG.EX.[3SG.S]</ta>
            <ta e="T178" id="Seg_8346" s="T177">reindeer-CAR</ta>
            <ta e="T179" id="Seg_8347" s="T178">leg-INSTR</ta>
            <ta e="T180" id="Seg_8348" s="T179">live-PST.NAR-3PL</ta>
            <ta e="T181" id="Seg_8349" s="T180">this</ta>
            <ta e="T182" id="Seg_8350" s="T181">father-DYA-PL.[NOM]</ta>
            <ta e="T183" id="Seg_8351" s="T182">there</ta>
            <ta e="T184" id="Seg_8352" s="T183">live-PST.NAR-3PL</ta>
            <ta e="T185" id="Seg_8353" s="T184">live-PST.NAR-3PL</ta>
            <ta e="T186" id="Seg_8354" s="T185">then</ta>
            <ta e="T187" id="Seg_8355" s="T186">to.the.other.side</ta>
            <ta e="T188" id="Seg_8356" s="T187">cross-RFL-PST-3PL</ta>
            <ta e="T189" id="Seg_8357" s="T188">to.the.other.side</ta>
            <ta e="T190" id="Seg_8358" s="T189">cross-CVB</ta>
            <ta e="T191" id="Seg_8359" s="T190">this</ta>
            <ta e="T192" id="Seg_8360" s="T191">guy.[NOM]</ta>
            <ta e="T193" id="Seg_8361" s="T192">Nenets.[NOM]</ta>
            <ta e="T194" id="Seg_8362" s="T193">old.man.[NOM]</ta>
            <ta e="T195" id="Seg_8363" s="T194">tent-ILL</ta>
            <ta e="T196" id="Seg_8364" s="T195">Nenets.[NOM]</ta>
            <ta e="T197" id="Seg_8365" s="T196">old.man.[NOM]</ta>
            <ta e="T198" id="Seg_8366" s="T197">child.[NOM]-3SG</ta>
            <ta e="T199" id="Seg_8367" s="T198">NEG.EX.[3SG.S]</ta>
            <ta e="T200" id="Seg_8368" s="T199">child-CAR-ADJZ</ta>
            <ta e="T201" id="Seg_8369" s="T200">wife.[NOM]-3SG</ta>
            <ta e="T202" id="Seg_8370" s="T201">that</ta>
            <ta e="T203" id="Seg_8371" s="T202">middle-ADJZ</ta>
            <ta e="T204" id="Seg_8372" s="T203">brother.[NOM]-3SG</ta>
            <ta e="T206" id="Seg_8373" s="T205">one</ta>
            <ta e="T207" id="Seg_8374" s="T206">daughter.[NOM]-3SG</ta>
            <ta e="T208" id="Seg_8375" s="T207">friend-CAR-ADJZ</ta>
            <ta e="T209" id="Seg_8376" s="T208">down.the.river-ADV.EL</ta>
            <ta e="T210" id="Seg_8377" s="T209">forward</ta>
            <ta e="T211" id="Seg_8378" s="T210">two</ta>
            <ta e="T212" id="Seg_8379" s="T211">Nenets-DU.[NOM]</ta>
            <ta e="T213" id="Seg_8380" s="T212">(s)he.[NOM]</ta>
            <ta e="T214" id="Seg_8381" s="T213">tree-CAR-ADJZ</ta>
            <ta e="T215" id="Seg_8382" s="T214">earth-%%</ta>
            <ta e="T216" id="Seg_8383" s="T215">Pan.[NOM]</ta>
            <ta e="T217" id="Seg_8384" s="T216">old.man.[NOM]</ta>
            <ta e="T218" id="Seg_8385" s="T217">Poni.[NOM]</ta>
            <ta e="T219" id="Seg_8386" s="T218">old.man.[NOM]</ta>
            <ta e="T220" id="Seg_8387" s="T219">here.it.is.[3SG.S]</ta>
            <ta e="T221" id="Seg_8388" s="T220">where</ta>
            <ta e="T222" id="Seg_8389" s="T221">all-ADJZ</ta>
            <ta e="T223" id="Seg_8390" s="T222">clan.[NOM]-3SG</ta>
            <ta e="T224" id="Seg_8391" s="T223">reindeer-ACC</ta>
            <ta e="T225" id="Seg_8392" s="T224">all</ta>
            <ta e="T226" id="Seg_8393" s="T225">gather-US-PST.NAR-3SG.O</ta>
            <ta e="T227" id="Seg_8394" s="T226">%%-2SG.O</ta>
            <ta e="T228" id="Seg_8395" s="T227">such-ADVZ</ta>
            <ta e="T229" id="Seg_8396" s="T228">come-CO.[3SG.S]</ta>
            <ta e="T230" id="Seg_8397" s="T229">this</ta>
            <ta e="T231" id="Seg_8398" s="T230">brother-DYA-DU.[NOM]</ta>
            <ta e="T232" id="Seg_8399" s="T231">this</ta>
            <ta e="T233" id="Seg_8400" s="T232">brother-DYA-DU.[NOM]</ta>
            <ta e="T234" id="Seg_8401" s="T233">live-3DU.S</ta>
            <ta e="T235" id="Seg_8402" s="T234">this</ta>
            <ta e="T236" id="Seg_8403" s="T235">old.man-ADJZ</ta>
            <ta e="T237" id="Seg_8404" s="T236">brother-DYA-DU.[NOM]</ta>
            <ta e="T238" id="Seg_8405" s="T237">two</ta>
            <ta e="T239" id="Seg_8406" s="T238">four</ta>
            <ta e="T240" id="Seg_8407" s="T239">hundred</ta>
            <ta e="T241" id="Seg_8408" s="T240">reindeer.[NOM]-3SG</ta>
            <ta e="T242" id="Seg_8409" s="T241">this</ta>
            <ta e="T243" id="Seg_8410" s="T242">old.man-GEN</ta>
            <ta e="T244" id="Seg_8411" s="T243">reindeer-ACC</ta>
            <ta e="T245" id="Seg_8412" s="T244">take-SUP.2/3SG</ta>
            <ta e="T246" id="Seg_8413" s="T245">human.being-PL-OBL.3SG-COM</ta>
            <ta e="T247" id="Seg_8414" s="T246">come-FUT.[3SG.S]</ta>
            <ta e="T248" id="Seg_8415" s="T247">Poni.[NOM]</ta>
            <ta e="T249" id="Seg_8416" s="T248">old.man.[NOM]</ta>
            <ta e="T250" id="Seg_8417" s="T249">name.[NOM]-3SG</ta>
            <ta e="T251" id="Seg_8418" s="T250">here.it.is.[3SG.S]</ta>
            <ta e="T252" id="Seg_8419" s="T251">then</ta>
            <ta e="T253" id="Seg_8420" s="T252">%%-2SG.O</ta>
            <ta e="T254" id="Seg_8421" s="T253">come-CVB</ta>
            <ta e="T255" id="Seg_8422" s="T254">spend.night-3PL</ta>
            <ta e="T256" id="Seg_8423" s="T255">this</ta>
            <ta e="T257" id="Seg_8424" s="T256">(s)he-PL-EP-GEN</ta>
            <ta e="T258" id="Seg_8425" s="T257">house.[NOM]</ta>
            <ta e="T259" id="Seg_8426" s="T258">this</ta>
            <ta e="T260" id="Seg_8427" s="T259">guy-ACC</ta>
            <ta e="T261" id="Seg_8428" s="T260">supposedly</ta>
            <ta e="T262" id="Seg_8429" s="T261">NEG</ta>
            <ta e="T263" id="Seg_8430" s="T262">know-3PL</ta>
            <ta e="T265" id="Seg_8431" s="T264">how</ta>
            <ta e="T266" id="Seg_8432" s="T265">say-DUR-FUT.[3SG.S]</ta>
            <ta e="T267" id="Seg_8433" s="T266">(s)he.[NOM]</ta>
            <ta e="T268" id="Seg_8434" s="T267">what.[NOM]</ta>
            <ta e="T270" id="Seg_8435" s="T269">wise-ADVZ</ta>
            <ta e="T272" id="Seg_8436" s="T271">or</ta>
            <ta e="T273" id="Seg_8437" s="T272">how</ta>
            <ta e="T274" id="Seg_8438" s="T273">live.[3SG.S]</ta>
            <ta e="T275" id="Seg_8439" s="T274">now</ta>
            <ta e="T276" id="Seg_8440" s="T275">how</ta>
            <ta e="T277" id="Seg_8441" s="T276">know-DUR-FUT-2SG.O</ta>
            <ta e="T278" id="Seg_8442" s="T277">aside-ADV.LOC</ta>
            <ta e="T279" id="Seg_8443" s="T278">live-PTCP.PRS.[NOM]</ta>
            <ta e="T280" id="Seg_8444" s="T279">spend.night-3DU.S</ta>
            <ta e="T281" id="Seg_8445" s="T280">here</ta>
            <ta e="T282" id="Seg_8446" s="T281">what.[NOM]</ta>
            <ta e="T283" id="Seg_8447" s="T282">it.is.said</ta>
            <ta e="T284" id="Seg_8448" s="T283">three-ORD</ta>
            <ta e="T285" id="Seg_8449" s="T284">day.[NOM]</ta>
            <ta e="T286" id="Seg_8450" s="T285">%%</ta>
            <ta e="T287" id="Seg_8451" s="T286">it.is.said</ta>
            <ta e="T288" id="Seg_8452" s="T287">here</ta>
            <ta e="T289" id="Seg_8453" s="T288">come-CO.[3SG.S]</ta>
            <ta e="T290" id="Seg_8454" s="T289">it.is.said</ta>
            <ta e="T291" id="Seg_8455" s="T290">Poni.[NOM]</ta>
            <ta e="T292" id="Seg_8456" s="T291">old.man.[NOM]</ta>
            <ta e="T294" id="Seg_8457" s="T293">ten</ta>
            <ta e="T295" id="Seg_8458" s="T294">human.being-COM</ta>
            <ta e="T296" id="Seg_8459" s="T295">come-PST.NAR.[3SG.S]</ta>
            <ta e="T297" id="Seg_8460" s="T296">supposedly</ta>
            <ta e="T298" id="Seg_8461" s="T297">this</ta>
            <ta e="T299" id="Seg_8462" s="T298">reindeer-PL.[NOM]</ta>
            <ta e="T300" id="Seg_8463" s="T299">shepherd-SUP.2/3SG</ta>
            <ta e="T301" id="Seg_8464" s="T300">this</ta>
            <ta e="T302" id="Seg_8465" s="T301">human.being-EP-PL.[NOM]</ta>
            <ta e="T303" id="Seg_8466" s="T302">then</ta>
            <ta e="T304" id="Seg_8467" s="T303">this</ta>
            <ta e="T305" id="Seg_8468" s="T304">brother-DYA-DU-GEN</ta>
            <ta e="T306" id="Seg_8469" s="T305">this</ta>
            <ta e="T307" id="Seg_8470" s="T306">reindeer-ACC</ta>
            <ta e="T308" id="Seg_8471" s="T307">shepherd-DUR-3PL</ta>
            <ta e="T309" id="Seg_8472" s="T308">QUEST-GEN</ta>
            <ta e="T310" id="Seg_8473" s="T309">all-ADJZ</ta>
            <ta e="T311" id="Seg_8474" s="T310">house.[NOM]-3SG</ta>
            <ta e="T312" id="Seg_8475" s="T311">that</ta>
            <ta e="T313" id="Seg_8476" s="T312">new</ta>
            <ta e="T314" id="Seg_8477" s="T313">house.[NOM]-3SG</ta>
            <ta e="T315" id="Seg_8478" s="T314">QUEST-GEN</ta>
            <ta e="T316" id="Seg_8479" s="T315">all-ADJZ</ta>
            <ta e="T317" id="Seg_8480" s="T316">what.[NOM]-3SG</ta>
            <ta e="T318" id="Seg_8481" s="T317">all</ta>
            <ta e="T319" id="Seg_8482" s="T318">gather-MULO-IPFV-CO-3PL</ta>
            <ta e="T320" id="Seg_8483" s="T319">earlier</ta>
            <ta e="T321" id="Seg_8484" s="T320">more</ta>
            <ta e="T322" id="Seg_8485" s="T321">to.the.north-ADJZ</ta>
            <ta e="T323" id="Seg_8486" s="T322">Nenets-PL-GEN</ta>
            <ta e="T324" id="Seg_8487" s="T323">reindeer-ACC</ta>
            <ta e="T325" id="Seg_8488" s="T324">gather-MULO-CVB</ta>
            <ta e="T326" id="Seg_8489" s="T325">more</ta>
            <ta e="T327" id="Seg_8490" s="T326">all</ta>
            <ta e="T328" id="Seg_8491" s="T327">take-PST.NAR-3PL</ta>
            <ta e="T329" id="Seg_8492" s="T328">then</ta>
            <ta e="T330" id="Seg_8493" s="T329">then</ta>
            <ta e="T331" id="Seg_8494" s="T330">%%</ta>
            <ta e="T332" id="Seg_8495" s="T331">home</ta>
            <ta e="T333" id="Seg_8496" s="T332">go.away-CO-3PL</ta>
            <ta e="T334" id="Seg_8497" s="T333">this</ta>
            <ta e="T335" id="Seg_8498" s="T334">Nenets-GEN</ta>
            <ta e="T336" id="Seg_8499" s="T335">arrow.[NOM]</ta>
            <ta e="T337" id="Seg_8500" s="T336">three-ORD</ta>
            <ta e="T338" id="Seg_8501" s="T337">day.[NOM]-3SG</ta>
            <ta e="T339" id="Seg_8502" s="T338">it.is.said</ta>
            <ta e="T340" id="Seg_8503" s="T339">come-FUT-1PL</ta>
            <ta e="T341" id="Seg_8504" s="T340">as.if</ta>
            <ta e="T342" id="Seg_8505" s="T341">you.PL.NOM</ta>
            <ta e="T343" id="Seg_8506" s="T342">it.is.said</ta>
            <ta e="T344" id="Seg_8507" s="T343">where-%%</ta>
            <ta e="T345" id="Seg_8508" s="T344">NEG.IMP</ta>
            <ta e="T346" id="Seg_8509" s="T345">leave-IMP.2PL</ta>
            <ta e="T347" id="Seg_8510" s="T346">two-ORD</ta>
            <ta e="T348" id="Seg_8511" s="T347">day-ILL</ta>
            <ta e="T349" id="Seg_8512" s="T348">here</ta>
            <ta e="T350" id="Seg_8513" s="T349">sun-TRL-CO.[3SG.S]</ta>
            <ta e="T351" id="Seg_8514" s="T350">this</ta>
            <ta e="T352" id="Seg_8515" s="T351">guy.[NOM]</ta>
            <ta e="T353" id="Seg_8516" s="T352">such-ADVZ</ta>
            <ta e="T354" id="Seg_8517" s="T353">become.[3SG.S]</ta>
            <ta e="T355" id="Seg_8518" s="T354">this</ta>
            <ta e="T356" id="Seg_8519" s="T355">grandfather-OBL.3SG-ALL</ta>
            <ta e="T357" id="Seg_8520" s="T356">this</ta>
            <ta e="T358" id="Seg_8521" s="T357">guest-EP-GEN</ta>
            <ta e="T359" id="Seg_8522" s="T358">grandfather-ILL</ta>
            <ta e="T360" id="Seg_8523" s="T359">as.if</ta>
            <ta e="T361" id="Seg_8524" s="T360">forward</ta>
            <ta e="T362" id="Seg_8525" s="T361">here</ta>
            <ta e="T363" id="Seg_8526" s="T362">something.[NOM]</ta>
            <ta e="T364" id="Seg_8527" s="T363">be-INFER.[3SG.S]</ta>
            <ta e="T365" id="Seg_8528" s="T364">as.if</ta>
            <ta e="T366" id="Seg_8529" s="T365">four</ta>
            <ta e="T367" id="Seg_8530" s="T366">hundred</ta>
            <ta e="T368" id="Seg_8531" s="T367">reindeer-GEN</ta>
            <ta e="T369" id="Seg_8532" s="T368">stand-HAB-TEMPN-COR</ta>
            <ta e="T370" id="Seg_8533" s="T369">as.if</ta>
            <ta e="T371" id="Seg_8534" s="T370">tundra.[NOM]</ta>
            <ta e="T372" id="Seg_8535" s="T371">be-INFER.[3SG.S]</ta>
            <ta e="T373" id="Seg_8536" s="T372">as.if</ta>
            <ta e="T374" id="Seg_8537" s="T373">there-ADV.ILL</ta>
            <ta e="T375" id="Seg_8538" s="T374">let.go-OPT-1PL</ta>
            <ta e="T376" id="Seg_8539" s="T375">as.if</ta>
            <ta e="T377" id="Seg_8540" s="T376">Nenets-PL.[NOM]</ta>
            <ta e="T378" id="Seg_8541" s="T377">come-FUT-3PL</ta>
            <ta e="T379" id="Seg_8542" s="T378">then</ta>
            <ta e="T380" id="Seg_8543" s="T379">reindeer-ACC</ta>
            <ta e="T381" id="Seg_8544" s="T380">shepherd-INF-be.going.to-FUT-3PL</ta>
            <ta e="T382" id="Seg_8545" s="T381">so</ta>
            <ta e="T383" id="Seg_8546" s="T382">as.if</ta>
            <ta e="T384" id="Seg_8547" s="T383">that</ta>
            <ta e="T385" id="Seg_8548" s="T384">reindeer-GEN</ta>
            <ta e="T386" id="Seg_8549" s="T385">pasture.[NOM]-3SG</ta>
            <ta e="T387" id="Seg_8550" s="T386">winter-%%</ta>
            <ta e="T388" id="Seg_8551" s="T387">winter-VBLZ-PTCP.PST</ta>
            <ta e="T389" id="Seg_8552" s="T388">as.if</ta>
            <ta e="T390" id="Seg_8553" s="T389">where</ta>
            <ta e="T391" id="Seg_8554" s="T390">make-FUT-1PL</ta>
            <ta e="T392" id="Seg_8555" s="T391">this</ta>
            <ta e="T393" id="Seg_8556" s="T392">guy.[NOM]</ta>
            <ta e="T394" id="Seg_8557" s="T393">so</ta>
            <ta e="T395" id="Seg_8558" s="T394">say-CO-3SG.O</ta>
            <ta e="T396" id="Seg_8559" s="T395">then</ta>
            <ta e="T397" id="Seg_8560" s="T396">so</ta>
            <ta e="T398" id="Seg_8561" s="T397">say-CO-3SG.O</ta>
            <ta e="T399" id="Seg_8562" s="T398">as.if</ta>
            <ta e="T401" id="Seg_8563" s="T400">I.ALL</ta>
            <ta e="T402" id="Seg_8564" s="T401">give-IMP.2SG.O</ta>
            <ta e="T403" id="Seg_8565" s="T402">new</ta>
            <ta e="T404" id="Seg_8566" s="T403">new</ta>
            <ta e="T405" id="Seg_8567" s="T404">house.[NOM]-2SG</ta>
            <ta e="T406" id="Seg_8568" s="T405">be-CO.[3SG.S]</ta>
            <ta e="T407" id="Seg_8569" s="T406">grandfather.[NOM]</ta>
            <ta e="T408" id="Seg_8570" s="T407">as.if</ta>
            <ta e="T409" id="Seg_8571" s="T408">what.[NOM]</ta>
            <ta e="T410" id="Seg_8572" s="T409">new</ta>
            <ta e="T411" id="Seg_8573" s="T410">house.[NOM]-2SG</ta>
            <ta e="T412" id="Seg_8574" s="T411">be-CO.[3SG.S]</ta>
            <ta e="T413" id="Seg_8575" s="T412">I.ALL</ta>
            <ta e="T414" id="Seg_8576" s="T413">give-IMP.2SG.O</ta>
            <ta e="T415" id="Seg_8577" s="T414">as.if</ta>
            <ta e="T416" id="Seg_8578" s="T415">and</ta>
            <ta e="T417" id="Seg_8579" s="T416">that</ta>
            <ta e="T418" id="Seg_8580" s="T417">that</ta>
            <ta e="T419" id="Seg_8581" s="T418">brother-GEN-3SG</ta>
            <ta e="T420" id="Seg_8582" s="T419">daughter.[NOM]</ta>
            <ta e="T421" id="Seg_8583" s="T420">grandfather.[NOM]-3SG</ta>
            <ta e="T422" id="Seg_8584" s="T421">this-ACC</ta>
            <ta e="T423" id="Seg_8585" s="T422">go-CAUS-DUR-INFER-3SG.O</ta>
            <ta e="T424" id="Seg_8586" s="T423">this</ta>
            <ta e="T425" id="Seg_8587" s="T424">guy-ALL</ta>
            <ta e="T426" id="Seg_8588" s="T425">I.NOM</ta>
            <ta e="T427" id="Seg_8589" s="T426">as.if</ta>
            <ta e="T428" id="Seg_8590" s="T427">just</ta>
            <ta e="T429" id="Seg_8591" s="T428">lift-%%-FUT-1SG.S</ta>
            <ta e="T430" id="Seg_8592" s="T429">tent.[NOM]</ta>
            <ta e="T431" id="Seg_8593" s="T430">forward-ADV.LOC</ta>
            <ta e="T432" id="Seg_8594" s="T431">%%</ta>
            <ta e="T433" id="Seg_8595" s="T432">forest-ADJZ</ta>
            <ta e="T434" id="Seg_8596" s="T433">isthmus.[NOM]</ta>
            <ta e="T435" id="Seg_8597" s="T434">be-FUT.[3SG.S]</ta>
            <ta e="T436" id="Seg_8598" s="T435">this</ta>
            <ta e="T437" id="Seg_8599" s="T436">forward-ADJZ</ta>
            <ta e="T438" id="Seg_8600" s="T437">side-ILL</ta>
            <ta e="T439" id="Seg_8601" s="T438">tent-EP-VBLZ-FUT-1SG.S</ta>
            <ta e="T440" id="Seg_8602" s="T439">new</ta>
            <ta e="T441" id="Seg_8603" s="T440">tent.[NOM]</ta>
            <ta e="T442" id="Seg_8604" s="T441">bring-CO.[3SG.S]</ta>
            <ta e="T443" id="Seg_8605" s="T442">here.it.is.[3SG.S]</ta>
            <ta e="T444" id="Seg_8606" s="T443">let.go-MOM-CO-3PL</ta>
            <ta e="T445" id="Seg_8607" s="T444">%%-2SG.O</ta>
            <ta e="T446" id="Seg_8608" s="T445">let.go-MOM-TR-3PL</ta>
            <ta e="T447" id="Seg_8609" s="T446">here</ta>
            <ta e="T448" id="Seg_8610" s="T447">supposedly</ta>
            <ta e="T449" id="Seg_8611" s="T448">get.tired-PST.[3SG.S]</ta>
            <ta e="T450" id="Seg_8612" s="T449">two-ORD</ta>
            <ta e="T451" id="Seg_8613" s="T450">three-ORD</ta>
            <ta e="T452" id="Seg_8614" s="T451">day.[NOM]</ta>
            <ta e="T453" id="Seg_8615" s="T452">come-FUT-3PL</ta>
            <ta e="T454" id="Seg_8616" s="T453">Poni.[NOM]</ta>
            <ta e="T455" id="Seg_8617" s="T454">old.man-ADJZ</ta>
            <ta e="T456" id="Seg_8618" s="T455">something.[NOM]</ta>
            <ta e="T457" id="Seg_8619" s="T456">(s)he-EP-PL-EP-ALL</ta>
            <ta e="T458" id="Seg_8620" s="T457">this</ta>
            <ta e="T459" id="Seg_8621" s="T458">Nenets.[NOM]</ta>
            <ta e="T460" id="Seg_8622" s="T459">old.man-ADJZ</ta>
            <ta e="T461" id="Seg_8623" s="T460">something-DU.[NOM]</ta>
            <ta e="T462" id="Seg_8624" s="T461">then</ta>
            <ta e="T463" id="Seg_8625" s="T462">gather-MOM-SUP.2/3SG</ta>
            <ta e="T464" id="Seg_8626" s="T463">brother-DYA-DU.[NOM]</ta>
            <ta e="T465" id="Seg_8627" s="T464">then</ta>
            <ta e="T466" id="Seg_8628" s="T465">this</ta>
            <ta e="T467" id="Seg_8629" s="T466">sun-LOC</ta>
            <ta e="T468" id="Seg_8630" s="T467">then</ta>
            <ta e="T469" id="Seg_8631" s="T468">then</ta>
            <ta e="T470" id="Seg_8632" s="T469">let.go-MOM-INFER-3PL</ta>
            <ta e="T471" id="Seg_8633" s="T470">this</ta>
            <ta e="T472" id="Seg_8634" s="T471">reindeer-ACC</ta>
            <ta e="T473" id="Seg_8635" s="T472">INFER</ta>
            <ta e="T474" id="Seg_8636" s="T473">shepherd-INFER-3PL</ta>
            <ta e="T475" id="Seg_8637" s="T474">supposedly</ta>
            <ta e="T476" id="Seg_8638" s="T475">(s)he.[NOM]</ta>
            <ta e="T477" id="Seg_8639" s="T476">also</ta>
            <ta e="T479" id="Seg_8640" s="T478">tent-INSTR</ta>
            <ta e="T480" id="Seg_8641" s="T479">INFER</ta>
            <ta e="T481" id="Seg_8642" s="T480">give-INFER-3PL</ta>
            <ta e="T482" id="Seg_8643" s="T481">tent.[NOM]</ta>
            <ta e="T483" id="Seg_8644" s="T482">come.in-CO.[3SG.S]</ta>
            <ta e="T484" id="Seg_8645" s="T483">this</ta>
            <ta e="T485" id="Seg_8646" s="T484">forest-GEN</ta>
            <ta e="T486" id="Seg_8647" s="T485">isthmus-GEN</ta>
            <ta e="T487" id="Seg_8648" s="T486">forward-ADJZ</ta>
            <ta e="T488" id="Seg_8649" s="T487">side-ADJZ</ta>
            <ta e="T489" id="Seg_8650" s="T488">tent.[NOM]</ta>
            <ta e="T490" id="Seg_8651" s="T489">come.in-CO.[3SG.S]</ta>
            <ta e="T491" id="Seg_8652" s="T490">then</ta>
            <ta e="T492" id="Seg_8653" s="T491">INFER</ta>
            <ta e="T493" id="Seg_8654" s="T492">tent-EP-VBLZ-INFER-3DU.S</ta>
            <ta e="T494" id="Seg_8655" s="T493">this</ta>
            <ta e="T495" id="Seg_8656" s="T494">wife-OBL.3SG-COM</ta>
            <ta e="T496" id="Seg_8657" s="T495">supposedly</ta>
            <ta e="T497" id="Seg_8658" s="T496">rich.man-ADJZ</ta>
            <ta e="T993" id="Seg_8659" s="T497">Nenets.[NOM]</ta>
            <ta e="T498" id="Seg_8660" s="T993">old.man.[NOM]</ta>
            <ta e="T499" id="Seg_8661" s="T498">what.[NOM]</ta>
            <ta e="T500" id="Seg_8662" s="T499">tent.[NOM]-3SG</ta>
            <ta e="T501" id="Seg_8663" s="T500">NEG.EX.[3SG.S]</ta>
            <ta e="T502" id="Seg_8664" s="T501">new</ta>
            <ta e="T503" id="Seg_8665" s="T502">tent.[NOM]-3SG</ta>
            <ta e="T504" id="Seg_8666" s="T503">skin-ADJZ</ta>
            <ta e="T505" id="Seg_8667" s="T504">tent.[NOM]-3SG</ta>
            <ta e="T506" id="Seg_8668" s="T505">that</ta>
            <ta e="T507" id="Seg_8669" s="T506">big-in.some.degree</ta>
            <ta e="T975" id="Seg_8670" s="T507">%%-CO-3SG.O</ta>
            <ta e="T976" id="Seg_8671" s="T509">say-DUR-IMP.2SG.S</ta>
            <ta e="T512" id="Seg_8672" s="T511">this</ta>
            <ta e="T513" id="Seg_8673" s="T512">down-in.some.degree</ta>
            <ta e="T514" id="Seg_8674" s="T513">front.part-TR-TR-IMP.2SG.S</ta>
            <ta e="T517" id="Seg_8675" s="T516">this</ta>
            <ta e="T518" id="Seg_8676" s="T517">apparently</ta>
            <ta e="T520" id="Seg_8677" s="T519">good-ADVZ</ta>
            <ta e="T521" id="Seg_8678" s="T520">INFER</ta>
            <ta e="T522" id="Seg_8679" s="T521">say-DUR-INFER.[3SG.S]</ta>
            <ta e="T523" id="Seg_8680" s="T522">INFER</ta>
            <ta e="T980" id="Seg_8681" s="T523">say-TR-DUR-INFER.[3SG.S]</ta>
            <ta e="T525" id="Seg_8682" s="T524">%%-2SG.O</ta>
            <ta e="T526" id="Seg_8683" s="T525">NEG.IMP</ta>
            <ta e="T981" id="Seg_8684" s="T526">make.noise.when.speaking-IMP.2SG.S</ta>
            <ta e="T528" id="Seg_8685" s="T527">supposedly</ta>
            <ta e="T529" id="Seg_8686" s="T528">head-CAR-ADJZ</ta>
            <ta e="T531" id="Seg_8687" s="T530">supposedly</ta>
            <ta e="T532" id="Seg_8688" s="T531">hear-TR-DUR-3SG.O</ta>
            <ta e="T533" id="Seg_8689" s="T532">maybe</ta>
            <ta e="T534" id="Seg_8690" s="T533">badly</ta>
            <ta e="T983" id="Seg_8691" s="T534">say-CO-3SG.O</ta>
            <ta e="T974" id="Seg_8692" s="T535">word-CAR</ta>
            <ta e="T537" id="Seg_8693" s="T536">then</ta>
            <ta e="T538" id="Seg_8694" s="T537">then</ta>
            <ta e="T539" id="Seg_8695" s="T538">spend.night-3DU.S</ta>
            <ta e="T540" id="Seg_8696" s="T539">this</ta>
            <ta e="T541" id="Seg_8697" s="T540">guy.[NOM]</ta>
            <ta e="T542" id="Seg_8698" s="T541">wife-OBL.3SG-COM</ta>
            <ta e="T543" id="Seg_8699" s="T542">spend.night-3PL</ta>
            <ta e="T544" id="Seg_8700" s="T543">edge-ADJZ</ta>
            <ta e="T545" id="Seg_8701" s="T544">morning.[NOM]</ta>
            <ta e="T546" id="Seg_8702" s="T545">such-ADJZ</ta>
            <ta e="T547" id="Seg_8703" s="T546">become.[3SG.S]</ta>
            <ta e="T548" id="Seg_8704" s="T547">that</ta>
            <ta e="T549" id="Seg_8705" s="T548">wife-OBL.3SG-%%</ta>
            <ta e="T550" id="Seg_8706" s="T549">I.NOM</ta>
            <ta e="T551" id="Seg_8707" s="T550">as.if</ta>
            <ta e="T552" id="Seg_8708" s="T551">upstream</ta>
            <ta e="T553" id="Seg_8709" s="T552">human.being-PL-ILL.1SG</ta>
            <ta e="T554" id="Seg_8710" s="T553">go.to-%%-1SG.S</ta>
            <ta e="T555" id="Seg_8711" s="T554">whether.[NOM]</ta>
            <ta e="T556" id="Seg_8712" s="T555">as.if</ta>
            <ta e="T557" id="Seg_8713" s="T556">tent-EP-VBLZ-PST.NAR-3PL</ta>
            <ta e="T558" id="Seg_8714" s="T557">either.or.[NOM]</ta>
            <ta e="T559" id="Seg_8715" s="T558">where.to.go-PST.NAR-3PL</ta>
            <ta e="T560" id="Seg_8716" s="T559">upstream</ta>
            <ta e="T561" id="Seg_8717" s="T560">start.for-PFV.[3SG.S]</ta>
            <ta e="T562" id="Seg_8718" s="T561">three</ta>
            <ta e="T563" id="Seg_8719" s="T562">bull-ACC</ta>
            <ta e="T564" id="Seg_8720" s="T563">harness-CVB</ta>
            <ta e="T565" id="Seg_8721" s="T564">supposedly</ta>
            <ta e="T566" id="Seg_8722" s="T565">three-ten-EP-ADJZ</ta>
            <ta e="T567" id="Seg_8723" s="T566">reindeer.[NOM]</ta>
            <ta e="T568" id="Seg_8724" s="T567">stay-TR-PST.NAR-INFER-3SG.O</ta>
            <ta e="T569" id="Seg_8725" s="T568">INFER</ta>
            <ta e="T570" id="Seg_8726" s="T569">guy.[NOM]</ta>
            <ta e="T571" id="Seg_8727" s="T570">upstream</ta>
            <ta e="T572" id="Seg_8728" s="T571">leave-PFV-PST.[3SG.S]</ta>
            <ta e="T573" id="Seg_8729" s="T572">wife.[NOM]-3SG</ta>
            <ta e="T574" id="Seg_8730" s="T573">oneself.3SG</ta>
            <ta e="T575" id="Seg_8731" s="T574">stay.[3SG.S]</ta>
            <ta e="T576" id="Seg_8732" s="T575">friend-CAR</ta>
            <ta e="T577" id="Seg_8733" s="T576">here</ta>
            <ta e="T578" id="Seg_8734" s="T577">sit-CO.[3SG.S]</ta>
            <ta e="T579" id="Seg_8735" s="T578">soon</ta>
            <ta e="T580" id="Seg_8736" s="T579">here</ta>
            <ta e="T582" id="Seg_8737" s="T581">one</ta>
            <ta e="T583" id="Seg_8738" s="T582">that-GEN</ta>
            <ta e="T584" id="Seg_8739" s="T583">middle-ADV.LOC</ta>
            <ta e="T585" id="Seg_8740" s="T584">sun.[NOM]-3SG</ta>
            <ta e="T586" id="Seg_8741" s="T585">evening-ILL</ta>
            <ta e="T587" id="Seg_8742" s="T586">here</ta>
            <ta e="T588" id="Seg_8743" s="T587">leave-INFER.[3SG.S]</ta>
            <ta e="T589" id="Seg_8744" s="T588">evening-OBL.3SG-LOC</ta>
            <ta e="T590" id="Seg_8745" s="T589">this</ta>
            <ta e="T591" id="Seg_8746" s="T590">guy.[NOM]</ta>
            <ta e="T592" id="Seg_8747" s="T591">quiet-ADVZ</ta>
            <ta e="T593" id="Seg_8748" s="T592">be-CO.[3SG.S]</ta>
            <ta e="T594" id="Seg_8749" s="T593">supposedly</ta>
            <ta e="T595" id="Seg_8750" s="T594">forward</ta>
            <ta e="T596" id="Seg_8751" s="T595">one</ta>
            <ta e="T597" id="Seg_8752" s="T596">that-GEN</ta>
            <ta e="T598" id="Seg_8753" s="T597">middle-ADV.LOC</ta>
            <ta e="T599" id="Seg_8754" s="T598">something.[NOM]</ta>
            <ta e="T600" id="Seg_8755" s="T599">human.being.[NOM]</ta>
            <ta e="T601" id="Seg_8756" s="T600">INFER</ta>
            <ta e="T602" id="Seg_8757" s="T601">RFL</ta>
            <ta e="T603" id="Seg_8758" s="T602">bring-INFER-3PL</ta>
            <ta e="T604" id="Seg_8759" s="T603">down.the.river-%%</ta>
            <ta e="T605" id="Seg_8760" s="T604">Pan.[NOM]</ta>
            <ta e="T606" id="Seg_8761" s="T605">old.man-ADJZ</ta>
            <ta e="T967" id="Seg_8762" s="T606">INFER</ta>
            <ta e="T607" id="Seg_8763" s="T967">RFL</ta>
            <ta e="T608" id="Seg_8764" s="T607">bring-INFER-3PL</ta>
            <ta e="T609" id="Seg_8765" s="T608">ten</ta>
            <ta e="T610" id="Seg_8766" s="T609">human.being-PL.[NOM]-3SG</ta>
            <ta e="T611" id="Seg_8767" s="T610">supposedly</ta>
            <ta e="T612" id="Seg_8768" s="T611">reindeer-ACC</ta>
            <ta e="T613" id="Seg_8769" s="T612">shepherd-PTCP.PST</ta>
            <ta e="T614" id="Seg_8770" s="T613">Pan.[NOM]</ta>
            <ta e="T615" id="Seg_8771" s="T614">old.man.[NOM]</ta>
            <ta e="T616" id="Seg_8772" s="T615">so.much</ta>
            <ta e="T617" id="Seg_8773" s="T616">see-PTCP.PRS</ta>
            <ta e="T618" id="Seg_8774" s="T617">be-CO.[3SG.S]</ta>
            <ta e="T619" id="Seg_8775" s="T618">so.much</ta>
            <ta e="T620" id="Seg_8776" s="T619">shaman.wisdom-COM</ta>
            <ta e="T621" id="Seg_8777" s="T620">be-CO.[3SG.S]</ta>
            <ta e="T622" id="Seg_8778" s="T621">then</ta>
            <ta e="T623" id="Seg_8779" s="T622">then</ta>
            <ta e="T624" id="Seg_8780" s="T623">%%-2SG.O</ta>
            <ta e="T625" id="Seg_8781" s="T624">this</ta>
            <ta e="T626" id="Seg_8782" s="T625">guy.[NOM]</ta>
            <ta e="T627" id="Seg_8783" s="T626">come-PFV-AUD.[3SG.S]</ta>
            <ta e="T628" id="Seg_8784" s="T627">outwards</ta>
            <ta e="T629" id="Seg_8785" s="T628">wife.[NOM]-3SG</ta>
            <ta e="T630" id="Seg_8786" s="T629">hear-IPFV-DUR.[3SG.S]</ta>
            <ta e="T631" id="Seg_8787" s="T630">come-PFV-AUD.[3SG.S]</ta>
            <ta e="T632" id="Seg_8788" s="T631">this</ta>
            <ta e="T633" id="Seg_8789" s="T632">reindeer-ACC-3SG</ta>
            <ta e="T634" id="Seg_8790" s="T633">away</ta>
            <ta e="T635" id="Seg_8791" s="T634">bind.[3SG.S]</ta>
            <ta e="T636" id="Seg_8792" s="T635">reindeer-ACC-3SG</ta>
            <ta e="T638" id="Seg_8793" s="T637">apparently</ta>
            <ta e="T639" id="Seg_8794" s="T638">hardly</ta>
            <ta e="T640" id="Seg_8795" s="T639">come-PFV.[3SG.S]</ta>
            <ta e="T641" id="Seg_8796" s="T640">apparently</ta>
            <ta e="T642" id="Seg_8797" s="T641">Poni.[NOM]</ta>
            <ta e="T643" id="Seg_8798" s="T642">old.man-ADJZ</ta>
            <ta e="T644" id="Seg_8799" s="T643">something-PL.[NOM]</ta>
            <ta e="T645" id="Seg_8800" s="T644">what.[NOM]</ta>
            <ta e="T646" id="Seg_8801" s="T645">apparently</ta>
            <ta e="T647" id="Seg_8802" s="T646">come-CO-3PL</ta>
            <ta e="T648" id="Seg_8803" s="T647">sledge-ADJZ</ta>
            <ta e="T649" id="Seg_8804" s="T648">something-PL.[3SG.S]-EP-3SG</ta>
            <ta e="T650" id="Seg_8805" s="T649">so</ta>
            <ta e="T651" id="Seg_8806" s="T650">stand-RFL-DUR-3PL</ta>
            <ta e="T652" id="Seg_8807" s="T651">that</ta>
            <ta e="T653" id="Seg_8808" s="T652">outside-ADV.LOC</ta>
            <ta e="T654" id="Seg_8809" s="T653">(s)he-EP-GEN</ta>
            <ta e="T655" id="Seg_8810" s="T654">outside-ADV.LOC</ta>
            <ta e="T656" id="Seg_8811" s="T655">come.in-CO.[3SG.S]</ta>
            <ta e="T657" id="Seg_8812" s="T656">tent.[NOM]</ta>
            <ta e="T658" id="Seg_8813" s="T657">come.in-CO.[3SG.S]</ta>
            <ta e="T659" id="Seg_8814" s="T658">door-GEN</ta>
            <ta e="T660" id="Seg_8815" s="T659">opening-ADV.LOC</ta>
            <ta e="T661" id="Seg_8816" s="T660">stand-HAB-CVB</ta>
            <ta e="T662" id="Seg_8817" s="T661">Nenets.[NOM]</ta>
            <ta e="T663" id="Seg_8818" s="T662">%%</ta>
            <ta e="T664" id="Seg_8819" s="T663">door-GEN</ta>
            <ta e="T665" id="Seg_8820" s="T664">opening-ADV.LOC</ta>
            <ta e="T666" id="Seg_8821" s="T665">%%</ta>
            <ta e="T667" id="Seg_8822" s="T666">leg-ACC-3SG</ta>
            <ta e="T668" id="Seg_8823" s="T667">shake-MOM-CO.[3SG.S]</ta>
            <ta e="T669" id="Seg_8824" s="T668">high.fur.boots-ACC-3SG</ta>
            <ta e="T670" id="Seg_8825" s="T669">here</ta>
            <ta e="T671" id="Seg_8826" s="T670">shake-MOM-3SG.O</ta>
            <ta e="T672" id="Seg_8827" s="T671">freeze-PTCP.PST</ta>
            <ta e="T673" id="Seg_8828" s="T672">side-ADJZ</ta>
            <ta e="T674" id="Seg_8829" s="T673">footwear.[NOM]-3SG</ta>
            <ta e="T675" id="Seg_8830" s="T674">shake-MOM-INFER-3SG.O</ta>
            <ta e="T676" id="Seg_8831" s="T675">so</ta>
            <ta e="T677" id="Seg_8832" s="T676">say-CO-3SG.O</ta>
            <ta e="T678" id="Seg_8833" s="T677">forward</ta>
            <ta e="T679" id="Seg_8834" s="T678">turn-MOM-CVB</ta>
            <ta e="T680" id="Seg_8835" s="T679">Poni.[NOM]</ta>
            <ta e="T681" id="Seg_8836" s="T680">old.man-%%</ta>
            <ta e="T682" id="Seg_8837" s="T681">as.if</ta>
            <ta e="T683" id="Seg_8838" s="T682">mouth.[NOM]-2SG</ta>
            <ta e="T684" id="Seg_8839" s="T683">as.if</ta>
            <ta e="T685" id="Seg_8840" s="T684">as.if</ta>
            <ta e="T686" id="Seg_8841" s="T685">jay-ADJZ</ta>
            <ta e="T687" id="Seg_8842" s="T686">as.if</ta>
            <ta e="T688" id="Seg_8843" s="T687">something.[NOM]</ta>
            <ta e="T689" id="Seg_8844" s="T688">%%-2SG.O</ta>
            <ta e="T690" id="Seg_8845" s="T689">INFER</ta>
            <ta e="T691" id="Seg_8846" s="T690">forward</ta>
            <ta e="T692" id="Seg_8847" s="T691">run-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T693" id="Seg_8848" s="T692">here</ta>
            <ta e="T694" id="Seg_8849" s="T693">fist-COM</ta>
            <ta e="T695" id="Seg_8850" s="T694">caress-MOM-PFV-PST.NAR-INFER-3SG.O</ta>
            <ta e="T696" id="Seg_8851" s="T695">half-ADJZ</ta>
            <ta e="T697" id="Seg_8852" s="T696">hand-GEN-3SG</ta>
            <ta e="T698" id="Seg_8853" s="T697">caress-MOM-PFV-PST.NAR-3SG.O</ta>
            <ta e="T699" id="Seg_8854" s="T698">Poni.[NOM]</ta>
            <ta e="T700" id="Seg_8855" s="T699">old.man-ADJZ</ta>
            <ta e="T701" id="Seg_8856" s="T700">something-PL-EP-ACC</ta>
            <ta e="T702" id="Seg_8857" s="T701">all</ta>
            <ta e="T703" id="Seg_8858" s="T702">flat</ta>
            <ta e="T704" id="Seg_8859" s="T703">hit-PST.NAR-3SG.O</ta>
            <ta e="T705" id="Seg_8860" s="T704">other.[NOM]-3SG</ta>
            <ta e="T706" id="Seg_8861" s="T705">die-PST.NAR.[3SG.S]</ta>
            <ta e="T707" id="Seg_8862" s="T706">Poni.[NOM]</ta>
            <ta e="T708" id="Seg_8863" s="T707">old.man.[NOM]</ta>
            <ta e="T709" id="Seg_8864" s="T708">live-CVB</ta>
            <ta e="T710" id="Seg_8865" s="T709">stay-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T711" id="Seg_8866" s="T710">two</ta>
            <ta e="T712" id="Seg_8867" s="T711">Nenets-DU.[NOM]</ta>
            <ta e="T713" id="Seg_8868" s="T712">live-CVB</ta>
            <ta e="T714" id="Seg_8869" s="T713">stay-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T715" id="Seg_8870" s="T714">outwards</ta>
            <ta e="T716" id="Seg_8871" s="T715">throw-MULO-PFV-INFER.[3SG.S]</ta>
            <ta e="T717" id="Seg_8872" s="T716">this</ta>
            <ta e="T718" id="Seg_8873" s="T717">guy.[NOM]</ta>
            <ta e="T719" id="Seg_8874" s="T718">one</ta>
            <ta e="T720" id="Seg_8875" s="T719">time.[NOM]</ta>
            <ta e="T721" id="Seg_8876" s="T720">fist-COM</ta>
            <ta e="T722" id="Seg_8877" s="T721">caress-MOM-PFV-PST.NAR-3SG.O</ta>
            <ta e="T723" id="Seg_8878" s="T722">all</ta>
            <ta e="T724" id="Seg_8879" s="T723">home-ADV.LOC</ta>
            <ta e="T725" id="Seg_8880" s="T724">clothing-GEN</ta>
            <ta e="T726" id="Seg_8881" s="T725">inside-ADV.ILL</ta>
            <ta e="T727" id="Seg_8882" s="T726">%%-DRV-PST.NAR-3PL</ta>
            <ta e="T728" id="Seg_8883" s="T727">this</ta>
            <ta e="T729" id="Seg_8884" s="T728">die-PTCP.PST</ta>
            <ta e="T730" id="Seg_8885" s="T729">something.[NOM]</ta>
            <ta e="T731" id="Seg_8886" s="T730">cunnus.[NOM]</ta>
            <ta e="T732" id="Seg_8887" s="T731">then</ta>
            <ta e="T733" id="Seg_8888" s="T732">then</ta>
            <ta e="T734" id="Seg_8889" s="T733">outwards</ta>
            <ta e="T735" id="Seg_8890" s="T734">throw-MULO-PFV-CO-3SG.O</ta>
            <ta e="T736" id="Seg_8891" s="T735">outside-ILL</ta>
            <ta e="T737" id="Seg_8892" s="T736">lately</ta>
            <ta e="T738" id="Seg_8893" s="T737">this</ta>
            <ta e="T739" id="Seg_8894" s="T738">sledge-GEN</ta>
            <ta e="T742" id="Seg_8895" s="T741">space.outside-LOC</ta>
            <ta e="T743" id="Seg_8896" s="T742">INFER</ta>
            <ta e="T744" id="Seg_8897" s="T743">bind-RFL-RES-INFER-3PL</ta>
            <ta e="T745" id="Seg_8898" s="T744">Poni.[NOM]</ta>
            <ta e="T746" id="Seg_8899" s="T745">old.man.[NOM]</ta>
            <ta e="T747" id="Seg_8900" s="T746">also</ta>
            <ta e="T748" id="Seg_8901" s="T747">outwards</ta>
            <ta e="T749" id="Seg_8902" s="T748">take-TR-PST.NAR-3SG.O</ta>
            <ta e="T750" id="Seg_8903" s="T749">what.[NOM]</ta>
            <ta e="T751" id="Seg_8904" s="T750">all</ta>
            <ta e="T752" id="Seg_8905" s="T751">outwards</ta>
            <ta e="T753" id="Seg_8906" s="T752">take-TR-PST.NAR-3SG.O</ta>
            <ta e="T754" id="Seg_8907" s="T753">down</ta>
            <ta e="T755" id="Seg_8908" s="T754">sit.down.[3SG.S]</ta>
            <ta e="T756" id="Seg_8909" s="T755">that</ta>
            <ta e="T757" id="Seg_8910" s="T756">overclothes.with.deer.fur.inside-ADJZ</ta>
            <ta e="T758" id="Seg_8911" s="T757">%%-3SG.O</ta>
            <ta e="T759" id="Seg_8912" s="T758">down</ta>
            <ta e="T760" id="Seg_8913" s="T759">eat-EP-FRQ-CVB</ta>
            <ta e="T762" id="Seg_8914" s="T761">down</ta>
            <ta e="T763" id="Seg_8915" s="T762">spend.night.[3SG.S]</ta>
            <ta e="T764" id="Seg_8916" s="T763">this</ta>
            <ta e="T765" id="Seg_8917" s="T764">human.being-PL.[NOM]-3SG</ta>
            <ta e="T766" id="Seg_8918" s="T765">space.outside-LOC</ta>
            <ta e="T767" id="Seg_8919" s="T766">INFER</ta>
            <ta e="T768" id="Seg_8920" s="T767">lie-MULS-RES-INFER-3PL</ta>
            <ta e="T769" id="Seg_8921" s="T768">then</ta>
            <ta e="T770" id="Seg_8922" s="T769">bind-RES-%%</ta>
            <ta e="T771" id="Seg_8923" s="T770">how</ta>
            <ta e="T772" id="Seg_8924" s="T771">bind-RFL-RES.[3SG.S]</ta>
            <ta e="T773" id="Seg_8925" s="T772">such-ADVZ</ta>
            <ta e="T774" id="Seg_8926" s="T773">spend.night.[3SG.S]</ta>
            <ta e="T775" id="Seg_8927" s="T774">edge-ADJZ</ta>
            <ta e="T776" id="Seg_8928" s="T775">morning.[NOM]</ta>
            <ta e="T777" id="Seg_8929" s="T776">up</ta>
            <ta e="T778" id="Seg_8930" s="T777">get.up.[3SG.S]</ta>
            <ta e="T779" id="Seg_8931" s="T778">Poni.[NOM]</ta>
            <ta e="T780" id="Seg_8932" s="T779">old.man.[NOM]</ta>
            <ta e="T781" id="Seg_8933" s="T780">such</ta>
            <ta e="T782" id="Seg_8934" s="T781">as.if</ta>
            <ta e="T783" id="Seg_8935" s="T782">face-ADJZ</ta>
            <ta e="T784" id="Seg_8936" s="T783">head.[NOM]-3SG</ta>
            <ta e="T785" id="Seg_8937" s="T784">whole</ta>
            <ta e="T786" id="Seg_8938" s="T785">swell.up-RFL.PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T787" id="Seg_8939" s="T786">that</ta>
            <ta e="T788" id="Seg_8940" s="T787">barely</ta>
            <ta e="T789" id="Seg_8941" s="T788">eye-DIM.[NOM]-3SG</ta>
            <ta e="T791" id="Seg_8942" s="T790">that</ta>
            <ta e="T792" id="Seg_8943" s="T791">Nenets-PL.[NOM]</ta>
            <ta e="T794" id="Seg_8944" s="T793">more</ta>
            <ta e="T795" id="Seg_8945" s="T794">good-in.some.degree</ta>
            <ta e="T796" id="Seg_8946" s="T795">be-PST.NAR.[3SG.S]</ta>
            <ta e="T797" id="Seg_8947" s="T796">such-ADJZ</ta>
            <ta e="T798" id="Seg_8948" s="T797">say-CO-3SG.O</ta>
            <ta e="T799" id="Seg_8949" s="T798">this</ta>
            <ta e="T800" id="Seg_8950" s="T799">guy.[NOM]</ta>
            <ta e="T801" id="Seg_8951" s="T800">as.if</ta>
            <ta e="T802" id="Seg_8952" s="T801">this</ta>
            <ta e="T803" id="Seg_8953" s="T802">Nenets-PL-EP-ALL-OBL.3SG</ta>
            <ta e="T804" id="Seg_8954" s="T803">human.being-PL-ACC-3SG</ta>
            <ta e="T805" id="Seg_8955" s="T804">home</ta>
            <ta e="T806" id="Seg_8956" s="T805">leave-TR-IMP.2SG.O</ta>
            <ta e="T807" id="Seg_8957" s="T806">as.if</ta>
            <ta e="T808" id="Seg_8958" s="T807">sledge-LOC.2SG</ta>
            <ta e="T809" id="Seg_8959" s="T808">load-MOM-CVB</ta>
            <ta e="T810" id="Seg_8960" s="T809">as.if</ta>
            <ta e="T811" id="Seg_8961" s="T810">wind.round-MOM-CVB</ta>
            <ta e="T812" id="Seg_8962" s="T811">oneself.3PL</ta>
            <ta e="T813" id="Seg_8963" s="T812">sledge-ILL-OBL.3PL</ta>
            <ta e="T814" id="Seg_8964" s="T813">then</ta>
            <ta e="T815" id="Seg_8965" s="T814">then</ta>
            <ta e="T816" id="Seg_8966" s="T815">there</ta>
            <ta e="T817" id="Seg_8967" s="T816">so</ta>
            <ta e="T818" id="Seg_8968" s="T817">outwards</ta>
            <ta e="T821" id="Seg_8969" s="T820">supposedly</ta>
            <ta e="T822" id="Seg_8970" s="T821">house.[NOM]</ta>
            <ta e="T823" id="Seg_8971" s="T822">come.in-RFL.PFV-US-INF</ta>
            <ta e="T824" id="Seg_8972" s="T823">and</ta>
            <ta e="T825" id="Seg_8973" s="T824">either.or</ta>
            <ta e="T826" id="Seg_8974" s="T825">eat-EP-FRQ-INF</ta>
            <ta e="T827" id="Seg_8975" s="T826">and</ta>
            <ta e="T828" id="Seg_8976" s="T827">either.or</ta>
            <ta e="T829" id="Seg_8977" s="T828">NEG</ta>
            <ta e="T830" id="Seg_8978" s="T829">eat-EP-FRQ-INF</ta>
            <ta e="T831" id="Seg_8979" s="T830">then</ta>
            <ta e="T832" id="Seg_8980" s="T831">this</ta>
            <ta e="T833" id="Seg_8981" s="T832">human.being.[NOM]</ta>
            <ta e="T834" id="Seg_8982" s="T833">that</ta>
            <ta e="T835" id="Seg_8983" s="T834">bind-MOM-PTCP.PST</ta>
            <ta e="T836" id="Seg_8984" s="T835">sledge-ILL-3SG</ta>
            <ta e="T837" id="Seg_8985" s="T836">that</ta>
            <ta e="T838" id="Seg_8986" s="T837">there</ta>
            <ta e="T839" id="Seg_8987" s="T838">home</ta>
            <ta e="T840" id="Seg_8988" s="T839">%%-CVB</ta>
            <ta e="T841" id="Seg_8989" s="T840">human.being-PL-ACC-3SG</ta>
            <ta e="T842" id="Seg_8990" s="T841">this</ta>
            <ta e="T843" id="Seg_8991" s="T842">die-PTCP.PST</ta>
            <ta e="T844" id="Seg_8992" s="T843">human.being-PL-ACC-3SG</ta>
            <ta e="T845" id="Seg_8993" s="T844">so</ta>
            <ta e="T846" id="Seg_8994" s="T845">go.away-TR-CO-3SG.O</ta>
            <ta e="T847" id="Seg_8995" s="T846">home</ta>
            <ta e="T848" id="Seg_8996" s="T847">down.the.river</ta>
            <ta e="T849" id="Seg_8997" s="T848">home</ta>
            <ta e="T850" id="Seg_8998" s="T849">Poni.[NOM]</ta>
            <ta e="T851" id="Seg_8999" s="T850">old.man-ACC</ta>
            <ta e="T852" id="Seg_9000" s="T851">%%</ta>
            <ta e="T853" id="Seg_9001" s="T852">%%-CVB</ta>
            <ta e="T854" id="Seg_9002" s="T853">go.away-TR-CO-3SG.O</ta>
            <ta e="T855" id="Seg_9003" s="T854">supposedly</ta>
            <ta e="T856" id="Seg_9004" s="T855">a.little.bit</ta>
            <ta e="T857" id="Seg_9005" s="T856">breath.[NOM]-3SG</ta>
            <ta e="T858" id="Seg_9006" s="T857">only</ta>
            <ta e="T859" id="Seg_9007" s="T858">%%</ta>
            <ta e="T860" id="Seg_9008" s="T859">go.away-CO-3PL</ta>
            <ta e="T861" id="Seg_9009" s="T860">that</ta>
            <ta e="T862" id="Seg_9010" s="T861">then</ta>
            <ta e="T863" id="Seg_9011" s="T862">good-ADVZ</ta>
            <ta e="T864" id="Seg_9012" s="T863">leave-CO-3PL</ta>
            <ta e="T865" id="Seg_9013" s="T864">there</ta>
            <ta e="T866" id="Seg_9014" s="T865">wife-OBL.3SG-%%</ta>
            <ta e="T867" id="Seg_9015" s="T866">so</ta>
            <ta e="T868" id="Seg_9016" s="T867">say-CO-3SG.O</ta>
            <ta e="T869" id="Seg_9017" s="T868">tent.[NOM]-2SG</ta>
            <ta e="T870" id="Seg_9018" s="T869">away</ta>
            <ta e="T871" id="Seg_9019" s="T870">remove-IMP.2SG.O</ta>
            <ta e="T872" id="Seg_9020" s="T871">we.PL.NOM</ta>
            <ta e="T873" id="Seg_9021" s="T872">forward</ta>
            <ta e="T874" id="Seg_9022" s="T873">leave-IPFV-DEB-1DU</ta>
            <ta e="T875" id="Seg_9023" s="T874">human.being-EP-PL-EP-ALL</ta>
            <ta e="T876" id="Seg_9024" s="T875">NEG</ta>
            <ta e="T877" id="Seg_9025" s="T876">know-PST.NAR-3PL</ta>
            <ta e="T878" id="Seg_9026" s="T877">supposedly</ta>
            <ta e="T879" id="Seg_9027" s="T878">oneself.3PL.[NOM]</ta>
            <ta e="T880" id="Seg_9028" s="T879">this</ta>
            <ta e="T881" id="Seg_9029" s="T880">grandfather-OBL.3SG-ADJZ</ta>
            <ta e="T882" id="Seg_9030" s="T881">something-PL.[NOM]</ta>
            <ta e="T883" id="Seg_9031" s="T882">INDEF3</ta>
            <ta e="T884" id="Seg_9032" s="T883">what.[NOM]</ta>
            <ta e="T885" id="Seg_9033" s="T884">forward</ta>
            <ta e="T886" id="Seg_9034" s="T885">go.away-CO.[3SG.S]</ta>
            <ta e="T887" id="Seg_9035" s="T886">that</ta>
            <ta e="T888" id="Seg_9036" s="T887">oneself.3SG</ta>
            <ta e="T889" id="Seg_9037" s="T888">NEG</ta>
            <ta e="T890" id="Seg_9038" s="T889">say-DUR.[3SG.S]</ta>
            <ta e="T892" id="Seg_9039" s="T891">that</ta>
            <ta e="T893" id="Seg_9040" s="T892">wife.[NOM]-3SG</ta>
            <ta e="T894" id="Seg_9041" s="T893">here</ta>
            <ta e="T895" id="Seg_9042" s="T894">say-FUT-3SG.O</ta>
            <ta e="T896" id="Seg_9043" s="T895">father-GEN-3SG</ta>
            <ta e="T897" id="Seg_9044" s="T896">grandfather-GEN-OBL.3SG</ta>
            <ta e="T898" id="Seg_9045" s="T897">something-DU-ALL</ta>
            <ta e="T899" id="Seg_9046" s="T898">Poni.[NOM]</ta>
            <ta e="T900" id="Seg_9047" s="T899">old.man-ADJZ</ta>
            <ta e="T901" id="Seg_9048" s="T900">something-PL.[NOM]</ta>
            <ta e="T902" id="Seg_9049" s="T901">that</ta>
            <ta e="T903" id="Seg_9050" s="T902">home</ta>
            <ta e="T904" id="Seg_9051" s="T903">go.to-PST-3PL</ta>
            <ta e="T905" id="Seg_9052" s="T904">it.is.said</ta>
            <ta e="T906" id="Seg_9053" s="T905">%%-2SG.O</ta>
            <ta e="T907" id="Seg_9054" s="T906">then</ta>
            <ta e="T908" id="Seg_9055" s="T907">then</ta>
            <ta e="T909" id="Seg_9056" s="T908">%%</ta>
            <ta e="T910" id="Seg_9057" s="T909">supposedly</ta>
            <ta e="T912" id="Seg_9058" s="T911">NEG</ta>
            <ta e="T985" id="Seg_9059" s="T912">know-3PL</ta>
            <ta e="T914" id="Seg_9060" s="T913">down</ta>
            <ta e="T986" id="Seg_9061" s="T914">say-DUR-IMP.2SG.S</ta>
            <ta e="T916" id="Seg_9062" s="T915">down</ta>
            <ta e="T987" id="Seg_9063" s="T916">tape.recorder-ILL.2SG</ta>
            <ta e="T918" id="Seg_9064" s="T917">down.the.river</ta>
            <ta e="T919" id="Seg_9065" s="T918">forward</ta>
            <ta e="T920" id="Seg_9066" s="T919">%%</ta>
            <ta e="T921" id="Seg_9067" s="T920">(s)he-EP-DIM-PL.[NOM]</ta>
            <ta e="T922" id="Seg_9068" s="T921">there</ta>
            <ta e="T923" id="Seg_9069" s="T922">grandfather-GEN-OBL.3SG</ta>
            <ta e="T924" id="Seg_9070" s="T923">house.[NOM]</ta>
            <ta e="T925" id="Seg_9071" s="T924">house.[NOM]</ta>
            <ta e="T926" id="Seg_9072" s="T925">come.in-CO.[3SG.S]</ta>
            <ta e="T927" id="Seg_9073" s="T926">sledge-PL.[NOM]</ta>
            <ta e="T928" id="Seg_9074" s="T927">that</ta>
            <ta e="T929" id="Seg_9075" s="T928">along</ta>
            <ta e="T930" id="Seg_9076" s="T929">wind.round-CVB</ta>
            <ta e="T931" id="Seg_9077" s="T930">so</ta>
            <ta e="T932" id="Seg_9078" s="T931">put-CO-3SG.O</ta>
            <ta e="T933" id="Seg_9079" s="T932">that</ta>
            <ta e="T934" id="Seg_9080" s="T933">leave-PST.NAR-INFER-3PL</ta>
            <ta e="T935" id="Seg_9081" s="T934">home</ta>
            <ta e="T936" id="Seg_9082" s="T935">go.away-PST.NAR-INFER-3PL</ta>
            <ta e="T937" id="Seg_9083" s="T936">Poni.[NOM]</ta>
            <ta e="T938" id="Seg_9084" s="T937">old.man.[NOM]</ta>
            <ta e="T939" id="Seg_9085" s="T938">barely</ta>
            <ta e="T940" id="Seg_9086" s="T939">down.the.river-ADV.LOC</ta>
            <ta e="T941" id="Seg_9087" s="T940">house-LOC</ta>
            <ta e="T942" id="Seg_9088" s="T941">come-US-PST.NAR.[3SG.S]</ta>
            <ta e="T943" id="Seg_9089" s="T942">down</ta>
            <ta e="T944" id="Seg_9090" s="T943">die-PST.NAR.[3SG.S]</ta>
            <ta e="T945" id="Seg_9091" s="T944">that</ta>
            <ta e="T946" id="Seg_9092" s="T945">two</ta>
            <ta e="T947" id="Seg_9093" s="T946">human.being-EP-DU.[NOM]</ta>
            <ta e="T948" id="Seg_9094" s="T947">now</ta>
            <ta e="T949" id="Seg_9095" s="T948">live-DUR-3DU.S</ta>
            <ta e="T950" id="Seg_9096" s="T949">Poni.[NOM]</ta>
            <ta e="T951" id="Seg_9097" s="T950">old.man.[NOM]</ta>
            <ta e="T952" id="Seg_9098" s="T951">Poni.[NOM]</ta>
            <ta e="T953" id="Seg_9099" s="T952">old.man-ADJZ</ta>
            <ta e="T954" id="Seg_9100" s="T953">something-PL.[NOM]</ta>
            <ta e="T955" id="Seg_9101" s="T954">there</ta>
            <ta e="T988" id="Seg_9102" s="T955">be-CO.[3SG.S]</ta>
            <ta e="T957" id="Seg_9103" s="T956">other</ta>
            <ta e="T958" id="Seg_9104" s="T957">something.[NOM]</ta>
            <ta e="T959" id="Seg_9105" s="T958">say-IMP.2SG.O</ta>
            <ta e="T960" id="Seg_9106" s="T959">other</ta>
            <ta e="T989" id="Seg_9107" s="T960">other</ta>
            <ta e="T962" id="Seg_9108" s="T961">other</ta>
            <ta e="T963" id="Seg_9109" s="T962">something.[NOM]</ta>
            <ta e="T964" id="Seg_9110" s="T963">say-IMP.2SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T3" id="Seg_9111" s="T2">один</ta>
            <ta e="T4" id="Seg_9112" s="T3">ребенок.[NOM]-3SG</ta>
            <ta e="T5" id="Seg_9113" s="T4">быть-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T7" id="Seg_9114" s="T6">этот</ta>
            <ta e="T8" id="Seg_9115" s="T7">INDEF3</ta>
            <ta e="T9" id="Seg_9116" s="T8">ведь</ta>
            <ta e="T11" id="Seg_9117" s="T10">ведь</ta>
            <ta e="T12" id="Seg_9118" s="T11">сказать-HAB-1SG.S</ta>
            <ta e="T13" id="Seg_9119" s="T12">это-ACC</ta>
            <ta e="T14" id="Seg_9120" s="T13">этот</ta>
            <ta e="T15" id="Seg_9121" s="T14">быль.[3SG.S]</ta>
            <ta e="T16" id="Seg_9122" s="T15">этот</ta>
            <ta e="T17" id="Seg_9123" s="T16">быль.[3SG.S]</ta>
            <ta e="T18" id="Seg_9124" s="T17">NEG</ta>
            <ta e="T19" id="Seg_9125" s="T18">сказка.[3SG.S]</ta>
            <ta e="T21" id="Seg_9126" s="T20">ты.GEN</ta>
            <ta e="T22" id="Seg_9127" s="T21">внизу</ta>
            <ta e="T24" id="Seg_9128" s="T23">селькуп-EP-PL.[NOM]</ta>
            <ta e="T25" id="Seg_9129" s="T24">туда</ta>
            <ta e="T27" id="Seg_9130" s="T26">этот</ta>
            <ta e="T28" id="Seg_9131" s="T27">вот</ta>
            <ta e="T29" id="Seg_9132" s="T28">ты.ACC</ta>
            <ta e="T30" id="Seg_9133" s="T29">увидеть-PFV-FUT.[3SG.S]</ta>
            <ta e="T32" id="Seg_9134" s="T31">ты.NOM</ta>
            <ta e="T33" id="Seg_9135" s="T32">речь.[NOM]-2SG</ta>
            <ta e="T971" id="Seg_9136" s="T33">остановиться-TR-PFV-INF-%%-IPFV-DUR-IMP.2SG.O</ta>
            <ta e="T972" id="Seg_9137" s="T34">таскать-DETR-TR-IMP.2SG.O</ta>
            <ta e="T36" id="Seg_9138" s="T35">середина-DIM-ADVZ</ta>
            <ta e="T973" id="Seg_9139" s="T36">сказать-IMP.2SG.O</ta>
            <ta e="T38" id="Seg_9140" s="T37">%%</ta>
            <ta e="T39" id="Seg_9141" s="T38">этот</ta>
            <ta e="T40" id="Seg_9142" s="T39">старик.[NOM]</ta>
            <ta e="T41" id="Seg_9143" s="T40">жить-PST.NAR-3SG.S</ta>
            <ta e="T42" id="Seg_9144" s="T41">болеть-INCH-PST.NAR.[3SG.S]</ta>
            <ta e="T43" id="Seg_9145" s="T42">это</ta>
            <ta e="T44" id="Seg_9146" s="T43">селькуп-EP-ADJZ</ta>
            <ta e="T45" id="Seg_9147" s="T44">старик.[3SG.S]</ta>
            <ta e="T46" id="Seg_9148" s="T45">посюсторонний</ta>
            <ta e="T47" id="Seg_9149" s="T46">сторона-ADJZ</ta>
            <ta e="T48" id="Seg_9150" s="T47">лабаз-3SG.O</ta>
            <ta e="T49" id="Seg_9151" s="T48">на.другую.сторону-ADJZ</ta>
            <ta e="T50" id="Seg_9152" s="T49">вниз.по.течению.реки-ADJZ</ta>
            <ta e="T51" id="Seg_9153" s="T50">большая.река.[NOM]</ta>
            <ta e="T52" id="Seg_9154" s="T51">сторона-LOC-ADJZ</ta>
            <ta e="T53" id="Seg_9155" s="T52">жить-PST.NAR.[3SG.S]</ta>
            <ta e="T54" id="Seg_9156" s="T53">туда-ADV.LOC-ADJZ</ta>
            <ta e="T55" id="Seg_9157" s="T54">он(а).[NOM]</ta>
            <ta e="T56" id="Seg_9158" s="T55">дерево-CAR-ADJZ</ta>
            <ta e="T57" id="Seg_9159" s="T56">земля-GEN</ta>
            <ta e="T992" id="Seg_9160" s="T58">что.[NOM]</ta>
            <ta e="T60" id="Seg_9161" s="T59">жить-PST.NAR.[3SG.S]</ta>
            <ta e="T61" id="Seg_9162" s="T60">тот</ta>
            <ta e="T62" id="Seg_9163" s="T61">болеть-INCH-PST.NAR.[3SG.S]</ta>
            <ta e="T63" id="Seg_9164" s="T62">сын.[NOM]-3SG</ta>
            <ta e="T64" id="Seg_9165" s="T63">один</ta>
            <ta e="T65" id="Seg_9166" s="T64">сын.[NOM]-3SG</ta>
            <ta e="T66" id="Seg_9167" s="T65">друг-CAR-ADJZ</ta>
            <ta e="T67" id="Seg_9168" s="T66">некто-CAR-ADJZ</ta>
            <ta e="T68" id="Seg_9169" s="T67">друг.[NOM]-3SG</ta>
            <ta e="T69" id="Seg_9170" s="T68">NEG</ta>
            <ta e="T70" id="Seg_9171" s="T69">дочь.[NOM]-3SG</ta>
            <ta e="T71" id="Seg_9172" s="T70">NEG.EX.[3SG.S]</ta>
            <ta e="T72" id="Seg_9173" s="T71">NEG</ta>
            <ta e="T73" id="Seg_9174" s="T72">что.[NOM]-3SG</ta>
            <ta e="T74" id="Seg_9175" s="T73">NEG.EX.[3SG.S]</ta>
            <ta e="T75" id="Seg_9176" s="T74">потом</ta>
            <ta e="T76" id="Seg_9177" s="T75">этот</ta>
            <ta e="T77" id="Seg_9178" s="T76">отец.[NOM]-3SG</ta>
            <ta e="T78" id="Seg_9179" s="T77">болеть-INCH-CO.[3SG.S]</ta>
            <ta e="T79" id="Seg_9180" s="T78">этот</ta>
            <ta e="T80" id="Seg_9181" s="T79">ребенок-DIM.[NOM]</ta>
            <ta e="T82" id="Seg_9182" s="T81">этот</ta>
            <ta e="T83" id="Seg_9183" s="T82">парень-GEN</ta>
            <ta e="T84" id="Seg_9184" s="T83">там</ta>
            <ta e="T85" id="Seg_9185" s="T84">жить.[3SG.S]</ta>
            <ta e="T86" id="Seg_9186" s="T85">вроде</ta>
            <ta e="T87" id="Seg_9187" s="T86">этот</ta>
            <ta e="T88" id="Seg_9188" s="T87">отец-GEN-3SG</ta>
            <ta e="T89" id="Seg_9189" s="T88">мать-GEN-3SG</ta>
            <ta e="T90" id="Seg_9190" s="T89">полог-LOC</ta>
            <ta e="T91" id="Seg_9191" s="T90">туда-ADV.ILL</ta>
            <ta e="T92" id="Seg_9192" s="T91">тот</ta>
            <ta e="T93" id="Seg_9193" s="T92">два</ta>
            <ta e="T94" id="Seg_9194" s="T93">ненец.[NOM]</ta>
            <ta e="T95" id="Seg_9195" s="T94">брат-DYA-DU.[NOM]</ta>
            <ta e="T96" id="Seg_9196" s="T95">быть-PST.NAR-INFER-3DU.S</ta>
            <ta e="T97" id="Seg_9197" s="T96">ненец.[NOM]</ta>
            <ta e="T98" id="Seg_9198" s="T97">старик.[NOM]</ta>
            <ta e="T99" id="Seg_9199" s="T98">куда</ta>
            <ta e="T100" id="Seg_9200" s="T99">быть-FUT.[3SG.S]</ta>
            <ta e="T102" id="Seg_9201" s="T101">что.ли.[NOM]</ta>
            <ta e="T103" id="Seg_9202" s="T102">жить.[3SG.S]</ta>
            <ta e="T104" id="Seg_9203" s="T103">куда</ta>
            <ta e="T105" id="Seg_9204" s="T104">%%</ta>
            <ta e="T106" id="Seg_9205" s="T105">тот</ta>
            <ta e="T107" id="Seg_9206" s="T106">этот</ta>
            <ta e="T108" id="Seg_9207" s="T107">старик.[NOM]</ta>
            <ta e="T109" id="Seg_9208" s="T108">вниз</ta>
            <ta e="T110" id="Seg_9209" s="T109">умереть-CO.[3SG.S]</ta>
            <ta e="T111" id="Seg_9210" s="T110">вниз</ta>
            <ta e="T112" id="Seg_9211" s="T111">умереть-CO.[3SG.S]</ta>
            <ta e="T113" id="Seg_9212" s="T112">тот</ta>
            <ta e="T114" id="Seg_9213" s="T113">потом</ta>
            <ta e="T115" id="Seg_9214" s="T114">потом</ta>
            <ta e="T116" id="Seg_9215" s="T115">тот</ta>
            <ta e="T117" id="Seg_9216" s="T116">парень.[NOM]</ta>
            <ta e="T118" id="Seg_9217" s="T117">ненец.[NOM]</ta>
            <ta e="T119" id="Seg_9218" s="T118">старик.[NOM]</ta>
            <ta e="T120" id="Seg_9219" s="T119">затем</ta>
            <ta e="T121" id="Seg_9220" s="T120">куда</ta>
            <ta e="T122" id="Seg_9221" s="T121">куда.деваться-FUT.[3SG.S]</ta>
            <ta e="T123" id="Seg_9222" s="T122">туда</ta>
            <ta e="T124" id="Seg_9223" s="T123">брат-OBL.3SG-%%</ta>
            <ta e="T125" id="Seg_9224" s="T124">прийти-US-CO.[3SG.S]</ta>
            <ta e="T126" id="Seg_9225" s="T125">старик.[NOM]</ta>
            <ta e="T127" id="Seg_9226" s="T126">умереть-PST.NAR.[3SG.S]</ta>
            <ta e="T128" id="Seg_9227" s="T127">вниз</ta>
            <ta e="T129" id="Seg_9228" s="T128">сделать-CO-3PL</ta>
            <ta e="T130" id="Seg_9229" s="T129">потом</ta>
            <ta e="T131" id="Seg_9230" s="T130">жена.[NOM]-3SG</ta>
            <ta e="T132" id="Seg_9231" s="T131">тот</ta>
            <ta e="T133" id="Seg_9232" s="T132">жена.[NOM]-3SG</ta>
            <ta e="T134" id="Seg_9233" s="T133">болеть-INCH-CO.[3SG.S]</ta>
            <ta e="T135" id="Seg_9234" s="T134">жена.[NOM]-3SG</ta>
            <ta e="T136" id="Seg_9235" s="T135">тоже</ta>
            <ta e="T137" id="Seg_9236" s="T136">вниз</ta>
            <ta e="T138" id="Seg_9237" s="T137">умереть-CO.[3SG.S]</ta>
            <ta e="T139" id="Seg_9238" s="T138">старик-ADJZ</ta>
            <ta e="T140" id="Seg_9239" s="T139">нечто-DU.[NOM]</ta>
            <ta e="T141" id="Seg_9240" s="T140">тот</ta>
            <ta e="T142" id="Seg_9241" s="T141">сын.[NOM]-3SG</ta>
            <ta e="T143" id="Seg_9242" s="T142">некто-CAR</ta>
            <ta e="T144" id="Seg_9243" s="T143">друг-CAR</ta>
            <ta e="T145" id="Seg_9244" s="T144">остаться.[3SG.S]</ta>
            <ta e="T146" id="Seg_9245" s="T145">куда</ta>
            <ta e="T147" id="Seg_9246" s="T146">куда.деваться-FUT.[3SG.S]</ta>
            <ta e="T148" id="Seg_9247" s="T147">этот</ta>
            <ta e="T149" id="Seg_9248" s="T148">ненец.[NOM]</ta>
            <ta e="T150" id="Seg_9249" s="T149">старик.[NOM]</ta>
            <ta e="T151" id="Seg_9250" s="T150">потом</ta>
            <ta e="T152" id="Seg_9251" s="T151">потом</ta>
            <ta e="T153" id="Seg_9252" s="T152">на.другую.сторону</ta>
            <ta e="T154" id="Seg_9253" s="T153">перейти-DRV-PST.NAR-3SG.O</ta>
            <ta e="T155" id="Seg_9254" s="T154">этот</ta>
            <ta e="T156" id="Seg_9255" s="T155">сирота-ADJZ</ta>
            <ta e="T157" id="Seg_9256" s="T156">парень.[NOM]</ta>
            <ta e="T158" id="Seg_9257" s="T157">большой</ta>
            <ta e="T159" id="Seg_9258" s="T158">сын.[NOM]-3SG</ta>
            <ta e="T167" id="Seg_9259" s="T166">какой.попало</ta>
            <ta e="T168" id="Seg_9260" s="T167">Гена-ADJZ</ta>
            <ta e="T169" id="Seg_9261" s="T168">нечто-GEN</ta>
            <ta e="T170" id="Seg_9262" s="T169">как</ta>
            <ta e="T171" id="Seg_9263" s="T170">такой-ADJZ</ta>
            <ta e="T172" id="Seg_9264" s="T171">сын.[NOM]-3SG-ADJZ</ta>
            <ta e="T173" id="Seg_9265" s="T172">быть-PST.NAR.[3SG.S]</ta>
            <ta e="T174" id="Seg_9266" s="T173">на.другую.сторону</ta>
            <ta e="T175" id="Seg_9267" s="T174">перейти-TR-CO-3SG.O</ta>
            <ta e="T176" id="Seg_9268" s="T175">олень.[NOM]-ILL-ADJZ</ta>
            <ta e="T177" id="Seg_9269" s="T176">NEG.EX.[3SG.S]</ta>
            <ta e="T178" id="Seg_9270" s="T177">олень-CAR</ta>
            <ta e="T179" id="Seg_9271" s="T178">нога-INSTR</ta>
            <ta e="T180" id="Seg_9272" s="T179">жить-PST.NAR-3PL</ta>
            <ta e="T181" id="Seg_9273" s="T180">этот</ta>
            <ta e="T182" id="Seg_9274" s="T181">отец-DYA-PL.[NOM]</ta>
            <ta e="T183" id="Seg_9275" s="T182">там</ta>
            <ta e="T184" id="Seg_9276" s="T183">жить-PST.NAR-3PL</ta>
            <ta e="T185" id="Seg_9277" s="T184">жить-PST.NAR-3PL</ta>
            <ta e="T186" id="Seg_9278" s="T185">потом</ta>
            <ta e="T187" id="Seg_9279" s="T186">на.другую.сторону</ta>
            <ta e="T188" id="Seg_9280" s="T187">перейти-RFL-PST-3PL</ta>
            <ta e="T189" id="Seg_9281" s="T188">на.другую.сторону</ta>
            <ta e="T190" id="Seg_9282" s="T189">перейти-CVB</ta>
            <ta e="T191" id="Seg_9283" s="T190">этот</ta>
            <ta e="T192" id="Seg_9284" s="T191">парень.[NOM]</ta>
            <ta e="T193" id="Seg_9285" s="T192">ненец.[NOM]</ta>
            <ta e="T194" id="Seg_9286" s="T193">старик.[NOM]</ta>
            <ta e="T195" id="Seg_9287" s="T194">чум-ILL</ta>
            <ta e="T196" id="Seg_9288" s="T195">ненец.[NOM]</ta>
            <ta e="T197" id="Seg_9289" s="T196">старик.[NOM]</ta>
            <ta e="T198" id="Seg_9290" s="T197">ребенок.[NOM]-3SG</ta>
            <ta e="T199" id="Seg_9291" s="T198">NEG.EX.[3SG.S]</ta>
            <ta e="T200" id="Seg_9292" s="T199">ребенок-CAR-ADJZ</ta>
            <ta e="T201" id="Seg_9293" s="T200">жена.[NOM]-3SG</ta>
            <ta e="T202" id="Seg_9294" s="T201">тот</ta>
            <ta e="T203" id="Seg_9295" s="T202">середина-ADJZ</ta>
            <ta e="T204" id="Seg_9296" s="T203">брат.[NOM]-3SG</ta>
            <ta e="T206" id="Seg_9297" s="T205">один</ta>
            <ta e="T207" id="Seg_9298" s="T206">дочь.[NOM]-3SG</ta>
            <ta e="T208" id="Seg_9299" s="T207">друг-CAR-ADJZ</ta>
            <ta e="T209" id="Seg_9300" s="T208">вниз.по.течению.реки-ADV.EL</ta>
            <ta e="T210" id="Seg_9301" s="T209">вперёд</ta>
            <ta e="T211" id="Seg_9302" s="T210">два</ta>
            <ta e="T212" id="Seg_9303" s="T211">ненец-DU.[NOM]</ta>
            <ta e="T213" id="Seg_9304" s="T212">он(а).[NOM]</ta>
            <ta e="T214" id="Seg_9305" s="T213">дерево-CAR-ADJZ</ta>
            <ta e="T215" id="Seg_9306" s="T214">земля-%%</ta>
            <ta e="T216" id="Seg_9307" s="T215">Пан.[NOM]</ta>
            <ta e="T217" id="Seg_9308" s="T216">старик.[NOM]</ta>
            <ta e="T218" id="Seg_9309" s="T217">Пони.[NOM]</ta>
            <ta e="T219" id="Seg_9310" s="T218">старик.[NOM]</ta>
            <ta e="T220" id="Seg_9311" s="T219">вот.он.есть.[3SG.S]</ta>
            <ta e="T221" id="Seg_9312" s="T220">где</ta>
            <ta e="T222" id="Seg_9313" s="T221">всё-ADJZ</ta>
            <ta e="T223" id="Seg_9314" s="T222">род.[NOM]-3SG</ta>
            <ta e="T224" id="Seg_9315" s="T223">олень-ACC</ta>
            <ta e="T225" id="Seg_9316" s="T224">всё</ta>
            <ta e="T226" id="Seg_9317" s="T225">собирать-US-PST.NAR-3SG.O</ta>
            <ta e="T227" id="Seg_9318" s="T226">%%-2SG.O</ta>
            <ta e="T228" id="Seg_9319" s="T227">такой-ADVZ</ta>
            <ta e="T229" id="Seg_9320" s="T228">прийти-CO.[3SG.S]</ta>
            <ta e="T230" id="Seg_9321" s="T229">этот</ta>
            <ta e="T231" id="Seg_9322" s="T230">брат-DYA-DU.[NOM]</ta>
            <ta e="T232" id="Seg_9323" s="T231">этот</ta>
            <ta e="T233" id="Seg_9324" s="T232">брат-DYA-DU.[NOM]</ta>
            <ta e="T234" id="Seg_9325" s="T233">жить-3DU.S</ta>
            <ta e="T235" id="Seg_9326" s="T234">этот</ta>
            <ta e="T236" id="Seg_9327" s="T235">старик-ADJZ</ta>
            <ta e="T237" id="Seg_9328" s="T236">брат-DYA-DU.[NOM]</ta>
            <ta e="T238" id="Seg_9329" s="T237">два</ta>
            <ta e="T239" id="Seg_9330" s="T238">четыре</ta>
            <ta e="T240" id="Seg_9331" s="T239">сто</ta>
            <ta e="T241" id="Seg_9332" s="T240">олень.[NOM]-3SG</ta>
            <ta e="T242" id="Seg_9333" s="T241">этот</ta>
            <ta e="T243" id="Seg_9334" s="T242">старик-GEN</ta>
            <ta e="T244" id="Seg_9335" s="T243">олень-ACC</ta>
            <ta e="T245" id="Seg_9336" s="T244">взять-SUP.2/3SG</ta>
            <ta e="T246" id="Seg_9337" s="T245">человек-PL-OBL.3SG-COM</ta>
            <ta e="T247" id="Seg_9338" s="T246">прийти-FUT.[3SG.S]</ta>
            <ta e="T248" id="Seg_9339" s="T247">Пони.[NOM]</ta>
            <ta e="T249" id="Seg_9340" s="T248">старик.[NOM]</ta>
            <ta e="T250" id="Seg_9341" s="T249">имя.[NOM]-3SG</ta>
            <ta e="T251" id="Seg_9342" s="T250">вот.он.есть.[3SG.S]</ta>
            <ta e="T252" id="Seg_9343" s="T251">потом</ta>
            <ta e="T253" id="Seg_9344" s="T252">%%-2SG.O</ta>
            <ta e="T254" id="Seg_9345" s="T253">прийти-CVB</ta>
            <ta e="T255" id="Seg_9346" s="T254">ночевать-3PL</ta>
            <ta e="T256" id="Seg_9347" s="T255">этот</ta>
            <ta e="T257" id="Seg_9348" s="T256">он(а)-PL-EP-GEN</ta>
            <ta e="T258" id="Seg_9349" s="T257">дом.[NOM]</ta>
            <ta e="T259" id="Seg_9350" s="T258">этот</ta>
            <ta e="T260" id="Seg_9351" s="T259">парень-ACC</ta>
            <ta e="T261" id="Seg_9352" s="T260">вроде</ta>
            <ta e="T262" id="Seg_9353" s="T261">NEG</ta>
            <ta e="T263" id="Seg_9354" s="T262">знать-3PL</ta>
            <ta e="T265" id="Seg_9355" s="T264">как</ta>
            <ta e="T266" id="Seg_9356" s="T265">сказать-DUR-FUT.[3SG.S]</ta>
            <ta e="T267" id="Seg_9357" s="T266">он(а).[NOM]</ta>
            <ta e="T268" id="Seg_9358" s="T267">что.[NOM]</ta>
            <ta e="T270" id="Seg_9359" s="T269">мудрёный-ADVZ</ta>
            <ta e="T272" id="Seg_9360" s="T271">или</ta>
            <ta e="T273" id="Seg_9361" s="T272">как</ta>
            <ta e="T274" id="Seg_9362" s="T273">жить.[3SG.S]</ta>
            <ta e="T275" id="Seg_9363" s="T274">сейчас</ta>
            <ta e="T276" id="Seg_9364" s="T275">как</ta>
            <ta e="T277" id="Seg_9365" s="T276">знать-DUR-FUT-2SG.O</ta>
            <ta e="T278" id="Seg_9366" s="T277">в.сторону-ADV.LOC</ta>
            <ta e="T279" id="Seg_9367" s="T278">жить-PTCP.PRS.[NOM]</ta>
            <ta e="T280" id="Seg_9368" s="T279">ночевать-3DU.S</ta>
            <ta e="T281" id="Seg_9369" s="T280">вот</ta>
            <ta e="T282" id="Seg_9370" s="T281">что.[NOM]</ta>
            <ta e="T283" id="Seg_9371" s="T282">мол</ta>
            <ta e="T284" id="Seg_9372" s="T283">три-ORD</ta>
            <ta e="T285" id="Seg_9373" s="T284">день.[NOM]</ta>
            <ta e="T286" id="Seg_9374" s="T285">%%</ta>
            <ta e="T287" id="Seg_9375" s="T286">мол</ta>
            <ta e="T288" id="Seg_9376" s="T287">вот</ta>
            <ta e="T289" id="Seg_9377" s="T288">прийти-CO.[3SG.S]</ta>
            <ta e="T290" id="Seg_9378" s="T289">мол</ta>
            <ta e="T291" id="Seg_9379" s="T290">Пони.[NOM]</ta>
            <ta e="T292" id="Seg_9380" s="T291">старик.[NOM]</ta>
            <ta e="T294" id="Seg_9381" s="T293">десять</ta>
            <ta e="T295" id="Seg_9382" s="T294">человек-COM</ta>
            <ta e="T296" id="Seg_9383" s="T295">прийти-PST.NAR.[3SG.S]</ta>
            <ta e="T297" id="Seg_9384" s="T296">вроде</ta>
            <ta e="T298" id="Seg_9385" s="T297">этот</ta>
            <ta e="T299" id="Seg_9386" s="T298">олень-PL.[NOM]</ta>
            <ta e="T300" id="Seg_9387" s="T299">погнать-SUP.2/3SG</ta>
            <ta e="T301" id="Seg_9388" s="T300">этот</ta>
            <ta e="T302" id="Seg_9389" s="T301">человек-EP-PL.[NOM]</ta>
            <ta e="T303" id="Seg_9390" s="T302">затем</ta>
            <ta e="T304" id="Seg_9391" s="T303">этот</ta>
            <ta e="T305" id="Seg_9392" s="T304">брат-DYA-DU-GEN</ta>
            <ta e="T306" id="Seg_9393" s="T305">этот</ta>
            <ta e="T307" id="Seg_9394" s="T306">олень-ACC</ta>
            <ta e="T308" id="Seg_9395" s="T307">погнать-DUR-3PL</ta>
            <ta e="T309" id="Seg_9396" s="T308">QUEST-GEN</ta>
            <ta e="T310" id="Seg_9397" s="T309">всё-ADJZ</ta>
            <ta e="T311" id="Seg_9398" s="T310">дом.[NOM]-3SG</ta>
            <ta e="T312" id="Seg_9399" s="T311">тот</ta>
            <ta e="T313" id="Seg_9400" s="T312">новый</ta>
            <ta e="T314" id="Seg_9401" s="T313">дом.[NOM]-3SG</ta>
            <ta e="T315" id="Seg_9402" s="T314">QUEST-GEN</ta>
            <ta e="T316" id="Seg_9403" s="T315">всё-ADJZ</ta>
            <ta e="T317" id="Seg_9404" s="T316">что.[NOM]-3SG</ta>
            <ta e="T318" id="Seg_9405" s="T317">всё</ta>
            <ta e="T319" id="Seg_9406" s="T318">собирать-MULO-IPFV-CO-3PL</ta>
            <ta e="T320" id="Seg_9407" s="T319">раньше</ta>
            <ta e="T321" id="Seg_9408" s="T320">еще</ta>
            <ta e="T322" id="Seg_9409" s="T321">на.север-ADJZ</ta>
            <ta e="T323" id="Seg_9410" s="T322">ненец-PL-GEN</ta>
            <ta e="T324" id="Seg_9411" s="T323">олень-ACC</ta>
            <ta e="T325" id="Seg_9412" s="T324">собирать-MULO-CVB</ta>
            <ta e="T326" id="Seg_9413" s="T325">еще</ta>
            <ta e="T327" id="Seg_9414" s="T326">всё</ta>
            <ta e="T328" id="Seg_9415" s="T327">взять-PST.NAR-3PL</ta>
            <ta e="T329" id="Seg_9416" s="T328">потом</ta>
            <ta e="T330" id="Seg_9417" s="T329">потом</ta>
            <ta e="T331" id="Seg_9418" s="T330">%%</ta>
            <ta e="T332" id="Seg_9419" s="T331">домой</ta>
            <ta e="T333" id="Seg_9420" s="T332">уйти-CO-3PL</ta>
            <ta e="T334" id="Seg_9421" s="T333">этот</ta>
            <ta e="T335" id="Seg_9422" s="T334">ненец-GEN</ta>
            <ta e="T336" id="Seg_9423" s="T335">стрела.[NOM]</ta>
            <ta e="T337" id="Seg_9424" s="T336">три-ORD</ta>
            <ta e="T338" id="Seg_9425" s="T337">день.[NOM]-3SG</ta>
            <ta e="T339" id="Seg_9426" s="T338">мол</ta>
            <ta e="T340" id="Seg_9427" s="T339">прийти-FUT-1PL</ta>
            <ta e="T341" id="Seg_9428" s="T340">будто</ta>
            <ta e="T342" id="Seg_9429" s="T341">вы.PL.NOM</ta>
            <ta e="T343" id="Seg_9430" s="T342">мол</ta>
            <ta e="T344" id="Seg_9431" s="T343">куда-%%</ta>
            <ta e="T345" id="Seg_9432" s="T344">NEG.IMP</ta>
            <ta e="T346" id="Seg_9433" s="T345">отправиться-IMP.2PL</ta>
            <ta e="T347" id="Seg_9434" s="T346">два-ORD</ta>
            <ta e="T348" id="Seg_9435" s="T347">день-ILL</ta>
            <ta e="T349" id="Seg_9436" s="T348">вот</ta>
            <ta e="T350" id="Seg_9437" s="T349">солнце-TRL-CO.[3SG.S]</ta>
            <ta e="T351" id="Seg_9438" s="T350">этот</ta>
            <ta e="T352" id="Seg_9439" s="T351">парень.[NOM]</ta>
            <ta e="T353" id="Seg_9440" s="T352">такой-ADVZ</ta>
            <ta e="T354" id="Seg_9441" s="T353">стать.[3SG.S]</ta>
            <ta e="T355" id="Seg_9442" s="T354">этот</ta>
            <ta e="T356" id="Seg_9443" s="T355">дедушка-OBL.3SG-ALL</ta>
            <ta e="T357" id="Seg_9444" s="T356">этот</ta>
            <ta e="T358" id="Seg_9445" s="T357">гость-EP-GEN</ta>
            <ta e="T359" id="Seg_9446" s="T358">дедушка-ILL</ta>
            <ta e="T360" id="Seg_9447" s="T359">будто</ta>
            <ta e="T361" id="Seg_9448" s="T360">вперёд</ta>
            <ta e="T362" id="Seg_9449" s="T361">здесь</ta>
            <ta e="T363" id="Seg_9450" s="T362">нечто.[NOM]</ta>
            <ta e="T364" id="Seg_9451" s="T363">быть-INFER.[3SG.S]</ta>
            <ta e="T365" id="Seg_9452" s="T364">будто</ta>
            <ta e="T366" id="Seg_9453" s="T365">четыре</ta>
            <ta e="T367" id="Seg_9454" s="T366">сто</ta>
            <ta e="T368" id="Seg_9455" s="T367">олень-GEN</ta>
            <ta e="T369" id="Seg_9456" s="T368">стоять-HAB-TEMPN-COR</ta>
            <ta e="T370" id="Seg_9457" s="T369">будто</ta>
            <ta e="T371" id="Seg_9458" s="T370">тундра.[NOM]</ta>
            <ta e="T372" id="Seg_9459" s="T371">быть-INFER.[3SG.S]</ta>
            <ta e="T373" id="Seg_9460" s="T372">будто</ta>
            <ta e="T374" id="Seg_9461" s="T373">туда-ADV.ILL</ta>
            <ta e="T375" id="Seg_9462" s="T374">пустить-OPT-1PL</ta>
            <ta e="T376" id="Seg_9463" s="T375">будто</ta>
            <ta e="T377" id="Seg_9464" s="T376">ненец-PL.[NOM]</ta>
            <ta e="T378" id="Seg_9465" s="T377">прийти-FUT-3PL</ta>
            <ta e="T379" id="Seg_9466" s="T378">тогда</ta>
            <ta e="T380" id="Seg_9467" s="T379">олень-ACC</ta>
            <ta e="T381" id="Seg_9468" s="T380">погнать-INF-собраться-FUT-3PL</ta>
            <ta e="T382" id="Seg_9469" s="T381">так</ta>
            <ta e="T383" id="Seg_9470" s="T382">будто</ta>
            <ta e="T384" id="Seg_9471" s="T383">тот</ta>
            <ta e="T385" id="Seg_9472" s="T384">олень-GEN</ta>
            <ta e="T386" id="Seg_9473" s="T385">пастбище.[NOM]-3SG</ta>
            <ta e="T387" id="Seg_9474" s="T386">зима-%%</ta>
            <ta e="T388" id="Seg_9475" s="T387">зима-VBLZ-PTCP.PST</ta>
            <ta e="T389" id="Seg_9476" s="T388">будто</ta>
            <ta e="T390" id="Seg_9477" s="T389">куда</ta>
            <ta e="T391" id="Seg_9478" s="T390">сделать-FUT-1PL</ta>
            <ta e="T392" id="Seg_9479" s="T391">этот</ta>
            <ta e="T393" id="Seg_9480" s="T392">парень.[NOM]</ta>
            <ta e="T394" id="Seg_9481" s="T393">так</ta>
            <ta e="T395" id="Seg_9482" s="T394">сказать-CO-3SG.O</ta>
            <ta e="T396" id="Seg_9483" s="T395">потом</ta>
            <ta e="T397" id="Seg_9484" s="T396">так</ta>
            <ta e="T398" id="Seg_9485" s="T397">сказать-CO-3SG.O</ta>
            <ta e="T399" id="Seg_9486" s="T398">будто</ta>
            <ta e="T401" id="Seg_9487" s="T400">я.ALL</ta>
            <ta e="T402" id="Seg_9488" s="T401">дать-IMP.2SG.O</ta>
            <ta e="T403" id="Seg_9489" s="T402">новый</ta>
            <ta e="T404" id="Seg_9490" s="T403">новый</ta>
            <ta e="T405" id="Seg_9491" s="T404">дом.[NOM]-2SG</ta>
            <ta e="T406" id="Seg_9492" s="T405">быть-CO.[3SG.S]</ta>
            <ta e="T407" id="Seg_9493" s="T406">дедушка.[NOM]</ta>
            <ta e="T408" id="Seg_9494" s="T407">будто</ta>
            <ta e="T409" id="Seg_9495" s="T408">что.[NOM]</ta>
            <ta e="T410" id="Seg_9496" s="T409">новый</ta>
            <ta e="T411" id="Seg_9497" s="T410">дом.[NOM]-2SG</ta>
            <ta e="T412" id="Seg_9498" s="T411">быть-CO.[3SG.S]</ta>
            <ta e="T413" id="Seg_9499" s="T412">я.ALL</ta>
            <ta e="T414" id="Seg_9500" s="T413">дать-IMP.2SG.O</ta>
            <ta e="T415" id="Seg_9501" s="T414">будто</ta>
            <ta e="T416" id="Seg_9502" s="T415">а</ta>
            <ta e="T417" id="Seg_9503" s="T416">тот</ta>
            <ta e="T418" id="Seg_9504" s="T417">тот</ta>
            <ta e="T419" id="Seg_9505" s="T418">брат-GEN-3SG</ta>
            <ta e="T420" id="Seg_9506" s="T419">дочь.[NOM]</ta>
            <ta e="T421" id="Seg_9507" s="T420">дедушка.[NOM]-3SG</ta>
            <ta e="T422" id="Seg_9508" s="T421">это-ACC</ta>
            <ta e="T423" id="Seg_9509" s="T422">идти-CAUS-DUR-INFER-3SG.O</ta>
            <ta e="T424" id="Seg_9510" s="T423">этот</ta>
            <ta e="T425" id="Seg_9511" s="T424">парень-ALL</ta>
            <ta e="T426" id="Seg_9512" s="T425">я.NOM</ta>
            <ta e="T427" id="Seg_9513" s="T426">будто</ta>
            <ta e="T428" id="Seg_9514" s="T427">просто</ta>
            <ta e="T429" id="Seg_9515" s="T428">поднять-%%-FUT-1SG.S</ta>
            <ta e="T430" id="Seg_9516" s="T429">чум.[NOM]</ta>
            <ta e="T431" id="Seg_9517" s="T430">вперёд-ADV.LOC</ta>
            <ta e="T432" id="Seg_9518" s="T431">%%</ta>
            <ta e="T433" id="Seg_9519" s="T432">лес-ADJZ</ta>
            <ta e="T434" id="Seg_9520" s="T433">перешеек.[NOM]</ta>
            <ta e="T435" id="Seg_9521" s="T434">быть-FUT.[3SG.S]</ta>
            <ta e="T436" id="Seg_9522" s="T435">этот</ta>
            <ta e="T437" id="Seg_9523" s="T436">вперёд-ADJZ</ta>
            <ta e="T438" id="Seg_9524" s="T437">сторона-ILL</ta>
            <ta e="T439" id="Seg_9525" s="T438">чум-EP-VBLZ-FUT-1SG.S</ta>
            <ta e="T440" id="Seg_9526" s="T439">новый</ta>
            <ta e="T441" id="Seg_9527" s="T440">чум.[NOM]</ta>
            <ta e="T442" id="Seg_9528" s="T441">принести-CO.[3SG.S]</ta>
            <ta e="T443" id="Seg_9529" s="T442">вот.он.есть.[3SG.S]</ta>
            <ta e="T444" id="Seg_9530" s="T443">пустить-MOM-CO-3PL</ta>
            <ta e="T445" id="Seg_9531" s="T444">%%-2SG.O</ta>
            <ta e="T446" id="Seg_9532" s="T445">пустить-MOM-TR-3PL</ta>
            <ta e="T447" id="Seg_9533" s="T446">вот</ta>
            <ta e="T448" id="Seg_9534" s="T447">вроде</ta>
            <ta e="T449" id="Seg_9535" s="T448">устать-PST.[3SG.S]</ta>
            <ta e="T450" id="Seg_9536" s="T449">два-ORD</ta>
            <ta e="T451" id="Seg_9537" s="T450">три-ORD</ta>
            <ta e="T452" id="Seg_9538" s="T451">день.[NOM]</ta>
            <ta e="T453" id="Seg_9539" s="T452">прийти-FUT-3PL</ta>
            <ta e="T454" id="Seg_9540" s="T453">Пони.[NOM]</ta>
            <ta e="T455" id="Seg_9541" s="T454">старик-ADJZ</ta>
            <ta e="T456" id="Seg_9542" s="T455">нечто.[NOM]</ta>
            <ta e="T457" id="Seg_9543" s="T456">он(а)-EP-PL-EP-ALL</ta>
            <ta e="T458" id="Seg_9544" s="T457">этот</ta>
            <ta e="T459" id="Seg_9545" s="T458">ненец.[NOM]</ta>
            <ta e="T460" id="Seg_9546" s="T459">старик-ADJZ</ta>
            <ta e="T461" id="Seg_9547" s="T460">нечто-DU.[NOM]</ta>
            <ta e="T462" id="Seg_9548" s="T461">затем</ta>
            <ta e="T463" id="Seg_9549" s="T462">собирать-MOM-SUP.2/3SG</ta>
            <ta e="T464" id="Seg_9550" s="T463">брат-DYA-DU.[NOM]</ta>
            <ta e="T465" id="Seg_9551" s="T464">затем</ta>
            <ta e="T466" id="Seg_9552" s="T465">этот</ta>
            <ta e="T467" id="Seg_9553" s="T466">солнце-LOC</ta>
            <ta e="T468" id="Seg_9554" s="T467">потом</ta>
            <ta e="T469" id="Seg_9555" s="T468">потом</ta>
            <ta e="T470" id="Seg_9556" s="T469">пустить-MOM-INFER-3PL</ta>
            <ta e="T471" id="Seg_9557" s="T470">этот</ta>
            <ta e="T472" id="Seg_9558" s="T471">олень-ACC</ta>
            <ta e="T473" id="Seg_9559" s="T472">INFER</ta>
            <ta e="T474" id="Seg_9560" s="T473">погнать-INFER-3PL</ta>
            <ta e="T475" id="Seg_9561" s="T474">вроде</ta>
            <ta e="T476" id="Seg_9562" s="T475">он(а).[NOM]</ta>
            <ta e="T477" id="Seg_9563" s="T476">тоже</ta>
            <ta e="T479" id="Seg_9564" s="T478">чум-INSTR</ta>
            <ta e="T480" id="Seg_9565" s="T479">INFER</ta>
            <ta e="T481" id="Seg_9566" s="T480">дать-INFER-3PL</ta>
            <ta e="T482" id="Seg_9567" s="T481">чум.[NOM]</ta>
            <ta e="T483" id="Seg_9568" s="T482">войти-CO.[3SG.S]</ta>
            <ta e="T484" id="Seg_9569" s="T483">этот</ta>
            <ta e="T485" id="Seg_9570" s="T484">лес-GEN</ta>
            <ta e="T486" id="Seg_9571" s="T485">перешеек-GEN</ta>
            <ta e="T487" id="Seg_9572" s="T486">вперёд-ADJZ</ta>
            <ta e="T488" id="Seg_9573" s="T487">сторона-ADJZ</ta>
            <ta e="T489" id="Seg_9574" s="T488">чум.[NOM]</ta>
            <ta e="T490" id="Seg_9575" s="T489">войти-CO.[3SG.S]</ta>
            <ta e="T491" id="Seg_9576" s="T490">потом</ta>
            <ta e="T492" id="Seg_9577" s="T491">INFER</ta>
            <ta e="T493" id="Seg_9578" s="T492">чум-EP-VBLZ-INFER-3DU.S</ta>
            <ta e="T494" id="Seg_9579" s="T493">этот</ta>
            <ta e="T495" id="Seg_9580" s="T494">жена-OBL.3SG-COM</ta>
            <ta e="T496" id="Seg_9581" s="T495">вроде</ta>
            <ta e="T497" id="Seg_9582" s="T496">богач-ADJZ</ta>
            <ta e="T993" id="Seg_9583" s="T497">ненец.[NOM]</ta>
            <ta e="T498" id="Seg_9584" s="T993">старик.[NOM]</ta>
            <ta e="T499" id="Seg_9585" s="T498">что.[NOM]</ta>
            <ta e="T500" id="Seg_9586" s="T499">чум.[NOM]-3SG</ta>
            <ta e="T501" id="Seg_9587" s="T500">NEG.EX.[3SG.S]</ta>
            <ta e="T502" id="Seg_9588" s="T501">новый</ta>
            <ta e="T503" id="Seg_9589" s="T502">чум.[NOM]-3SG</ta>
            <ta e="T504" id="Seg_9590" s="T503">шкура-ADJZ</ta>
            <ta e="T505" id="Seg_9591" s="T504">чум.[NOM]-3SG</ta>
            <ta e="T506" id="Seg_9592" s="T505">тот</ta>
            <ta e="T507" id="Seg_9593" s="T506">большой-в.некоторой.степени</ta>
            <ta e="T975" id="Seg_9594" s="T507">%%-CO-3SG.O</ta>
            <ta e="T976" id="Seg_9595" s="T509">сказать-DUR-IMP.2SG.S</ta>
            <ta e="T512" id="Seg_9596" s="T511">этот</ta>
            <ta e="T513" id="Seg_9597" s="T512">вниз-в.некоторой.степени</ta>
            <ta e="T514" id="Seg_9598" s="T513">перед-TR-TR-IMP.2SG.S</ta>
            <ta e="T517" id="Seg_9599" s="T516">этот</ta>
            <ta e="T518" id="Seg_9600" s="T517">видать</ta>
            <ta e="T520" id="Seg_9601" s="T519">хороший-ADVZ</ta>
            <ta e="T521" id="Seg_9602" s="T520">INFER</ta>
            <ta e="T522" id="Seg_9603" s="T521">сказать-DUR-INFER.[3SG.S]</ta>
            <ta e="T523" id="Seg_9604" s="T522">INFER</ta>
            <ta e="T980" id="Seg_9605" s="T523">сказать-TR-DUR-INFER.[3SG.S]</ta>
            <ta e="T525" id="Seg_9606" s="T524">%%-2SG.O</ta>
            <ta e="T526" id="Seg_9607" s="T525">NEG.IMP</ta>
            <ta e="T981" id="Seg_9608" s="T526">шуметь.при.разговоре-IMP.2SG.S</ta>
            <ta e="T528" id="Seg_9609" s="T527">вроде</ta>
            <ta e="T529" id="Seg_9610" s="T528">начальник-CAR-ADJZ</ta>
            <ta e="T531" id="Seg_9611" s="T530">вроде</ta>
            <ta e="T532" id="Seg_9612" s="T531">слушать-TR-DUR-3SG.O</ta>
            <ta e="T533" id="Seg_9613" s="T532">может.быть</ta>
            <ta e="T534" id="Seg_9614" s="T533">плохо</ta>
            <ta e="T983" id="Seg_9615" s="T534">сказать-CO-3SG.O</ta>
            <ta e="T974" id="Seg_9616" s="T535">слово-CAR</ta>
            <ta e="T537" id="Seg_9617" s="T536">потом</ta>
            <ta e="T538" id="Seg_9618" s="T537">потом</ta>
            <ta e="T539" id="Seg_9619" s="T538">ночевать-3DU.S</ta>
            <ta e="T540" id="Seg_9620" s="T539">этот</ta>
            <ta e="T541" id="Seg_9621" s="T540">парень.[NOM]</ta>
            <ta e="T542" id="Seg_9622" s="T541">жена-OBL.3SG-COM</ta>
            <ta e="T543" id="Seg_9623" s="T542">ночевать-3PL</ta>
            <ta e="T544" id="Seg_9624" s="T543">край-ADJZ</ta>
            <ta e="T545" id="Seg_9625" s="T544">утро.[NOM]</ta>
            <ta e="T546" id="Seg_9626" s="T545">такой-ADJZ</ta>
            <ta e="T547" id="Seg_9627" s="T546">стать.[3SG.S]</ta>
            <ta e="T548" id="Seg_9628" s="T547">тот</ta>
            <ta e="T549" id="Seg_9629" s="T548">жена-OBL.3SG-%%</ta>
            <ta e="T550" id="Seg_9630" s="T549">я.NOM</ta>
            <ta e="T551" id="Seg_9631" s="T550">будто</ta>
            <ta e="T552" id="Seg_9632" s="T551">вверх.по.течению</ta>
            <ta e="T553" id="Seg_9633" s="T552">человек-PL-ILL.1SG</ta>
            <ta e="T554" id="Seg_9634" s="T553">ходить.за.чем_либо-%%-1SG.S</ta>
            <ta e="T555" id="Seg_9635" s="T554">что.ли.[NOM]</ta>
            <ta e="T556" id="Seg_9636" s="T555">будто</ta>
            <ta e="T557" id="Seg_9637" s="T556">чум-EP-VBLZ-PST.NAR-3PL</ta>
            <ta e="T558" id="Seg_9638" s="T557">то.ли.[NOM]</ta>
            <ta e="T559" id="Seg_9639" s="T558">куда.деваться-PST.NAR-3PL</ta>
            <ta e="T560" id="Seg_9640" s="T559">вверх.по.течению</ta>
            <ta e="T561" id="Seg_9641" s="T560">тронуться-PFV.[3SG.S]</ta>
            <ta e="T562" id="Seg_9642" s="T561">три</ta>
            <ta e="T563" id="Seg_9643" s="T562">бык-ACC</ta>
            <ta e="T564" id="Seg_9644" s="T563">запрягать-CVB</ta>
            <ta e="T565" id="Seg_9645" s="T564">вроде</ta>
            <ta e="T566" id="Seg_9646" s="T565">три-десять-EP-ADJZ</ta>
            <ta e="T567" id="Seg_9647" s="T566">олень.[NOM]</ta>
            <ta e="T568" id="Seg_9648" s="T567">остаться-TR-PST.NAR-INFER-3SG.O</ta>
            <ta e="T569" id="Seg_9649" s="T568">INFER</ta>
            <ta e="T570" id="Seg_9650" s="T569">парень.[NOM]</ta>
            <ta e="T571" id="Seg_9651" s="T570">вверх.по.течению</ta>
            <ta e="T572" id="Seg_9652" s="T571">отправиться-PFV-PST.[3SG.S]</ta>
            <ta e="T573" id="Seg_9653" s="T572">жена.[NOM]-3SG</ta>
            <ta e="T574" id="Seg_9654" s="T573">сам.3SG</ta>
            <ta e="T575" id="Seg_9655" s="T574">остаться.[3SG.S]</ta>
            <ta e="T576" id="Seg_9656" s="T575">друг-CAR</ta>
            <ta e="T577" id="Seg_9657" s="T576">вот</ta>
            <ta e="T578" id="Seg_9658" s="T577">сидеть-CO.[3SG.S]</ta>
            <ta e="T579" id="Seg_9659" s="T578">скоро</ta>
            <ta e="T580" id="Seg_9660" s="T579">вот</ta>
            <ta e="T582" id="Seg_9661" s="T581">один</ta>
            <ta e="T583" id="Seg_9662" s="T582">тот-GEN</ta>
            <ta e="T584" id="Seg_9663" s="T583">середина-ADV.LOC</ta>
            <ta e="T585" id="Seg_9664" s="T584">солнце.[NOM]-3SG</ta>
            <ta e="T586" id="Seg_9665" s="T585">вечер-ILL</ta>
            <ta e="T587" id="Seg_9666" s="T586">вот</ta>
            <ta e="T588" id="Seg_9667" s="T587">отправиться-INFER.[3SG.S]</ta>
            <ta e="T589" id="Seg_9668" s="T588">вечер-OBL.3SG-LOC</ta>
            <ta e="T590" id="Seg_9669" s="T589">этот</ta>
            <ta e="T591" id="Seg_9670" s="T590">парень.[NOM]</ta>
            <ta e="T592" id="Seg_9671" s="T591">тихий-ADVZ</ta>
            <ta e="T593" id="Seg_9672" s="T592">быть-CO.[3SG.S]</ta>
            <ta e="T594" id="Seg_9673" s="T593">вроде</ta>
            <ta e="T595" id="Seg_9674" s="T594">вперёд</ta>
            <ta e="T596" id="Seg_9675" s="T595">один</ta>
            <ta e="T597" id="Seg_9676" s="T596">тот-GEN</ta>
            <ta e="T598" id="Seg_9677" s="T597">середина-ADV.LOC</ta>
            <ta e="T599" id="Seg_9678" s="T598">нечто.[NOM]</ta>
            <ta e="T600" id="Seg_9679" s="T599">человек.[NOM]</ta>
            <ta e="T601" id="Seg_9680" s="T600">INFER</ta>
            <ta e="T602" id="Seg_9681" s="T601">RFL</ta>
            <ta e="T603" id="Seg_9682" s="T602">привезти-INFER-3PL</ta>
            <ta e="T604" id="Seg_9683" s="T603">вниз.по.течению.реки-%%</ta>
            <ta e="T605" id="Seg_9684" s="T604">Пан.[NOM]</ta>
            <ta e="T606" id="Seg_9685" s="T605">старик-ADJZ</ta>
            <ta e="T967" id="Seg_9686" s="T606">INFER</ta>
            <ta e="T607" id="Seg_9687" s="T967">RFL</ta>
            <ta e="T608" id="Seg_9688" s="T607">привезти-INFER-3PL</ta>
            <ta e="T609" id="Seg_9689" s="T608">десять</ta>
            <ta e="T610" id="Seg_9690" s="T609">человек-PL.[NOM]-3SG</ta>
            <ta e="T611" id="Seg_9691" s="T610">вроде</ta>
            <ta e="T612" id="Seg_9692" s="T611">олень-ACC</ta>
            <ta e="T613" id="Seg_9693" s="T612">погнать-PTCP.PST</ta>
            <ta e="T614" id="Seg_9694" s="T613">Пан.[NOM]</ta>
            <ta e="T615" id="Seg_9695" s="T614">старик.[NOM]</ta>
            <ta e="T616" id="Seg_9696" s="T615">столько</ta>
            <ta e="T617" id="Seg_9697" s="T616">увидеть-PTCP.PRS</ta>
            <ta e="T618" id="Seg_9698" s="T617">быть-CO.[3SG.S]</ta>
            <ta e="T619" id="Seg_9699" s="T618">столько</ta>
            <ta e="T620" id="Seg_9700" s="T619">шаманская.мудрость-COM</ta>
            <ta e="T621" id="Seg_9701" s="T620">быть-CO.[3SG.S]</ta>
            <ta e="T622" id="Seg_9702" s="T621">потом</ta>
            <ta e="T623" id="Seg_9703" s="T622">потом</ta>
            <ta e="T624" id="Seg_9704" s="T623">%%-2SG.O</ta>
            <ta e="T625" id="Seg_9705" s="T624">этот</ta>
            <ta e="T626" id="Seg_9706" s="T625">парень.[NOM]</ta>
            <ta e="T627" id="Seg_9707" s="T626">прийти-PFV-AUD.[3SG.S]</ta>
            <ta e="T628" id="Seg_9708" s="T627">наружу</ta>
            <ta e="T629" id="Seg_9709" s="T628">жена.[NOM]-3SG</ta>
            <ta e="T630" id="Seg_9710" s="T629">слушать-IPFV-DUR.[3SG.S]</ta>
            <ta e="T631" id="Seg_9711" s="T630">прийти-PFV-AUD.[3SG.S]</ta>
            <ta e="T632" id="Seg_9712" s="T631">этот</ta>
            <ta e="T633" id="Seg_9713" s="T632">олень-ACC-3SG</ta>
            <ta e="T634" id="Seg_9714" s="T633">прочь</ta>
            <ta e="T635" id="Seg_9715" s="T634">привязать.[3SG.S]</ta>
            <ta e="T636" id="Seg_9716" s="T635">олень-ACC-3SG</ta>
            <ta e="T638" id="Seg_9717" s="T637">видать</ta>
            <ta e="T639" id="Seg_9718" s="T638">едва</ta>
            <ta e="T640" id="Seg_9719" s="T639">прийти-PFV.[3SG.S]</ta>
            <ta e="T641" id="Seg_9720" s="T640">видать</ta>
            <ta e="T642" id="Seg_9721" s="T641">Пони.[NOM]</ta>
            <ta e="T643" id="Seg_9722" s="T642">старик-ADJZ</ta>
            <ta e="T644" id="Seg_9723" s="T643">нечто-PL.[NOM]</ta>
            <ta e="T645" id="Seg_9724" s="T644">что.[NOM]</ta>
            <ta e="T646" id="Seg_9725" s="T645">видать</ta>
            <ta e="T647" id="Seg_9726" s="T646">прийти-CO-3PL</ta>
            <ta e="T648" id="Seg_9727" s="T647">нарты-ADJZ</ta>
            <ta e="T649" id="Seg_9728" s="T648">нечто-PL.[3SG.S]-EP-3SG</ta>
            <ta e="T650" id="Seg_9729" s="T649">так</ta>
            <ta e="T651" id="Seg_9730" s="T650">стоять-RFL-DUR-3PL</ta>
            <ta e="T652" id="Seg_9731" s="T651">тот</ta>
            <ta e="T653" id="Seg_9732" s="T652">улица-ADV.LOC</ta>
            <ta e="T654" id="Seg_9733" s="T653">он(а)-EP-GEN</ta>
            <ta e="T655" id="Seg_9734" s="T654">улица-ADV.LOC</ta>
            <ta e="T656" id="Seg_9735" s="T655">войти-CO.[3SG.S]</ta>
            <ta e="T657" id="Seg_9736" s="T656">чум.[NOM]</ta>
            <ta e="T658" id="Seg_9737" s="T657">войти-CO.[3SG.S]</ta>
            <ta e="T659" id="Seg_9738" s="T658">дверь-GEN</ta>
            <ta e="T660" id="Seg_9739" s="T659">отверстие-ADV.LOC</ta>
            <ta e="T661" id="Seg_9740" s="T660">стоять-HAB-CVB</ta>
            <ta e="T662" id="Seg_9741" s="T661">ненец.[NOM]</ta>
            <ta e="T663" id="Seg_9742" s="T662">%%</ta>
            <ta e="T664" id="Seg_9743" s="T663">дверь-GEN</ta>
            <ta e="T665" id="Seg_9744" s="T664">отверстие-ADV.LOC</ta>
            <ta e="T666" id="Seg_9745" s="T665">%%</ta>
            <ta e="T667" id="Seg_9746" s="T666">нога-ACC-3SG</ta>
            <ta e="T668" id="Seg_9747" s="T667">трясти-MOM-CO.[3SG.S]</ta>
            <ta e="T669" id="Seg_9748" s="T668">бокари-ACC-3SG</ta>
            <ta e="T670" id="Seg_9749" s="T669">вот</ta>
            <ta e="T671" id="Seg_9750" s="T670">трясти-MOM-3SG.O</ta>
            <ta e="T672" id="Seg_9751" s="T671">замерзнуть-PTCP.PST</ta>
            <ta e="T673" id="Seg_9752" s="T672">сторона-ADJZ</ta>
            <ta e="T674" id="Seg_9753" s="T673">обувь.[NOM]-3SG</ta>
            <ta e="T675" id="Seg_9754" s="T674">трясти-MOM-INFER-3SG.O</ta>
            <ta e="T676" id="Seg_9755" s="T675">так</ta>
            <ta e="T677" id="Seg_9756" s="T676">сказать-CO-3SG.O</ta>
            <ta e="T678" id="Seg_9757" s="T677">вперёд</ta>
            <ta e="T679" id="Seg_9758" s="T678">повернуть-MOM-CVB</ta>
            <ta e="T680" id="Seg_9759" s="T679">Пони.[NOM]</ta>
            <ta e="T681" id="Seg_9760" s="T680">старик-%%</ta>
            <ta e="T682" id="Seg_9761" s="T681">будто</ta>
            <ta e="T683" id="Seg_9762" s="T682">рот.[NOM]-2SG</ta>
            <ta e="T684" id="Seg_9763" s="T683">будто</ta>
            <ta e="T685" id="Seg_9764" s="T684">будто</ta>
            <ta e="T686" id="Seg_9765" s="T685">сойка-ADJZ</ta>
            <ta e="T687" id="Seg_9766" s="T686">будто</ta>
            <ta e="T688" id="Seg_9767" s="T687">нечто.[NOM]</ta>
            <ta e="T689" id="Seg_9768" s="T688">%%-2SG.O</ta>
            <ta e="T690" id="Seg_9769" s="T689">INFER</ta>
            <ta e="T691" id="Seg_9770" s="T690">вперёд</ta>
            <ta e="T692" id="Seg_9771" s="T691">побежать-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T693" id="Seg_9772" s="T692">вот</ta>
            <ta e="T694" id="Seg_9773" s="T693">кулак-COM</ta>
            <ta e="T695" id="Seg_9774" s="T694">гладить-MOM-PFV-PST.NAR-INFER-3SG.O</ta>
            <ta e="T696" id="Seg_9775" s="T695">половина-ADJZ</ta>
            <ta e="T697" id="Seg_9776" s="T696">рука-GEN-3SG</ta>
            <ta e="T698" id="Seg_9777" s="T697">гладить-MOM-PFV-PST.NAR-3SG.O</ta>
            <ta e="T699" id="Seg_9778" s="T698">Пони.[NOM]</ta>
            <ta e="T700" id="Seg_9779" s="T699">старик-ADJZ</ta>
            <ta e="T701" id="Seg_9780" s="T700">нечто-PL-EP-ACC</ta>
            <ta e="T702" id="Seg_9781" s="T701">всё</ta>
            <ta e="T703" id="Seg_9782" s="T702">плоско</ta>
            <ta e="T704" id="Seg_9783" s="T703">ударить-PST.NAR-3SG.O</ta>
            <ta e="T705" id="Seg_9784" s="T704">остальной.[NOM]-3SG</ta>
            <ta e="T706" id="Seg_9785" s="T705">умереть-PST.NAR.[3SG.S]</ta>
            <ta e="T707" id="Seg_9786" s="T706">Пони.[NOM]</ta>
            <ta e="T708" id="Seg_9787" s="T707">старик.[NOM]</ta>
            <ta e="T709" id="Seg_9788" s="T708">жить-CVB</ta>
            <ta e="T710" id="Seg_9789" s="T709">остаться-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T711" id="Seg_9790" s="T710">два</ta>
            <ta e="T712" id="Seg_9791" s="T711">ненец-DU.[NOM]</ta>
            <ta e="T713" id="Seg_9792" s="T712">жить-CVB</ta>
            <ta e="T714" id="Seg_9793" s="T713">остаться-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T715" id="Seg_9794" s="T714">наружу</ta>
            <ta e="T716" id="Seg_9795" s="T715">бросать-MULO-PFV-INFER.[3SG.S]</ta>
            <ta e="T717" id="Seg_9796" s="T716">этот</ta>
            <ta e="T718" id="Seg_9797" s="T717">парень.[NOM]</ta>
            <ta e="T719" id="Seg_9798" s="T718">один</ta>
            <ta e="T720" id="Seg_9799" s="T719">раз.[NOM]</ta>
            <ta e="T721" id="Seg_9800" s="T720">кулак-COM</ta>
            <ta e="T722" id="Seg_9801" s="T721">гладить-MOM-PFV-PST.NAR-3SG.O</ta>
            <ta e="T723" id="Seg_9802" s="T722">всё</ta>
            <ta e="T724" id="Seg_9803" s="T723">домой-ADV.LOC</ta>
            <ta e="T725" id="Seg_9804" s="T724">одежда-GEN</ta>
            <ta e="T726" id="Seg_9805" s="T725">внутри-ADV.ILL</ta>
            <ta e="T727" id="Seg_9806" s="T726">%%-DRV-PST.NAR-3PL</ta>
            <ta e="T728" id="Seg_9807" s="T727">этот</ta>
            <ta e="T729" id="Seg_9808" s="T728">умереть-PTCP.PST</ta>
            <ta e="T730" id="Seg_9809" s="T729">нечто.[NOM]</ta>
            <ta e="T731" id="Seg_9810" s="T730">cunnus.[NOM]</ta>
            <ta e="T732" id="Seg_9811" s="T731">потом</ta>
            <ta e="T733" id="Seg_9812" s="T732">потом</ta>
            <ta e="T734" id="Seg_9813" s="T733">наружу</ta>
            <ta e="T735" id="Seg_9814" s="T734">бросать-MULO-PFV-CO-3SG.O</ta>
            <ta e="T736" id="Seg_9815" s="T735">улица-ILL</ta>
            <ta e="T737" id="Seg_9816" s="T736">недавно</ta>
            <ta e="T738" id="Seg_9817" s="T737">этот</ta>
            <ta e="T739" id="Seg_9818" s="T738">нарты-GEN</ta>
            <ta e="T742" id="Seg_9819" s="T741">пространство.снаружи-LOC</ta>
            <ta e="T743" id="Seg_9820" s="T742">INFER</ta>
            <ta e="T744" id="Seg_9821" s="T743">привязать-RFL-RES-INFER-3PL</ta>
            <ta e="T745" id="Seg_9822" s="T744">Пони.[NOM]</ta>
            <ta e="T746" id="Seg_9823" s="T745">старик.[NOM]</ta>
            <ta e="T747" id="Seg_9824" s="T746">тоже</ta>
            <ta e="T748" id="Seg_9825" s="T747">наружу</ta>
            <ta e="T749" id="Seg_9826" s="T748">взять-TR-PST.NAR-3SG.O</ta>
            <ta e="T750" id="Seg_9827" s="T749">что.[NOM]</ta>
            <ta e="T751" id="Seg_9828" s="T750">всё</ta>
            <ta e="T752" id="Seg_9829" s="T751">наружу</ta>
            <ta e="T753" id="Seg_9830" s="T752">взять-TR-PST.NAR-3SG.O</ta>
            <ta e="T754" id="Seg_9831" s="T753">вниз</ta>
            <ta e="T755" id="Seg_9832" s="T754">сесть.[3SG.S]</ta>
            <ta e="T756" id="Seg_9833" s="T755">тот</ta>
            <ta e="T757" id="Seg_9834" s="T756">малица-ADJZ</ta>
            <ta e="T758" id="Seg_9835" s="T757">%%-3SG.O</ta>
            <ta e="T759" id="Seg_9836" s="T758">вниз</ta>
            <ta e="T760" id="Seg_9837" s="T759">съесть-EP-FRQ-CVB</ta>
            <ta e="T762" id="Seg_9838" s="T761">вниз</ta>
            <ta e="T763" id="Seg_9839" s="T762">ночевать.[3SG.S]</ta>
            <ta e="T764" id="Seg_9840" s="T763">этот</ta>
            <ta e="T765" id="Seg_9841" s="T764">человек-PL.[NOM]-3SG</ta>
            <ta e="T766" id="Seg_9842" s="T765">пространство.снаружи-LOC</ta>
            <ta e="T767" id="Seg_9843" s="T766">INFER</ta>
            <ta e="T768" id="Seg_9844" s="T767">лежать-MULS-RES-INFER-3PL</ta>
            <ta e="T769" id="Seg_9845" s="T768">затем</ta>
            <ta e="T770" id="Seg_9846" s="T769">привязать-RES-%%</ta>
            <ta e="T771" id="Seg_9847" s="T770">как</ta>
            <ta e="T772" id="Seg_9848" s="T771">привязать-RFL-RES.[3SG.S]</ta>
            <ta e="T773" id="Seg_9849" s="T772">такой-ADVZ</ta>
            <ta e="T774" id="Seg_9850" s="T773">ночевать.[3SG.S]</ta>
            <ta e="T775" id="Seg_9851" s="T774">край-ADJZ</ta>
            <ta e="T776" id="Seg_9852" s="T775">утро.[NOM]</ta>
            <ta e="T777" id="Seg_9853" s="T776">вверх</ta>
            <ta e="T778" id="Seg_9854" s="T777">встать.[3SG.S]</ta>
            <ta e="T779" id="Seg_9855" s="T778">Пони.[NOM]</ta>
            <ta e="T780" id="Seg_9856" s="T779">старик.[NOM]</ta>
            <ta e="T781" id="Seg_9857" s="T780">такой</ta>
            <ta e="T782" id="Seg_9858" s="T781">будто</ta>
            <ta e="T783" id="Seg_9859" s="T782">лицо-ADJZ</ta>
            <ta e="T784" id="Seg_9860" s="T783">голова.[NOM]-3SG</ta>
            <ta e="T785" id="Seg_9861" s="T784">целый</ta>
            <ta e="T786" id="Seg_9862" s="T785">распухнуть-RFL.PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T787" id="Seg_9863" s="T786">тот</ta>
            <ta e="T788" id="Seg_9864" s="T787">еле.еле</ta>
            <ta e="T789" id="Seg_9865" s="T788">глаз-DIM.[NOM]-3SG</ta>
            <ta e="T791" id="Seg_9866" s="T790">тот</ta>
            <ta e="T792" id="Seg_9867" s="T791">ненец-PL.[NOM]</ta>
            <ta e="T794" id="Seg_9868" s="T793">еще</ta>
            <ta e="T795" id="Seg_9869" s="T794">хороший-в.некоторой.степени</ta>
            <ta e="T796" id="Seg_9870" s="T795">быть-PST.NAR.[3SG.S]</ta>
            <ta e="T797" id="Seg_9871" s="T796">такой-ADJZ</ta>
            <ta e="T798" id="Seg_9872" s="T797">сказать-CO-3SG.O</ta>
            <ta e="T799" id="Seg_9873" s="T798">этот</ta>
            <ta e="T800" id="Seg_9874" s="T799">парень.[NOM]</ta>
            <ta e="T801" id="Seg_9875" s="T800">будто</ta>
            <ta e="T802" id="Seg_9876" s="T801">этот</ta>
            <ta e="T803" id="Seg_9877" s="T802">ненец-PL-EP-ALL-OBL.3SG</ta>
            <ta e="T804" id="Seg_9878" s="T803">человек-PL-ACC-3SG</ta>
            <ta e="T805" id="Seg_9879" s="T804">домой</ta>
            <ta e="T806" id="Seg_9880" s="T805">отправиться-TR-IMP.2SG.O</ta>
            <ta e="T807" id="Seg_9881" s="T806">будто</ta>
            <ta e="T808" id="Seg_9882" s="T807">нарты-LOC.2SG</ta>
            <ta e="T809" id="Seg_9883" s="T808">нагрузить-MOM-CVB</ta>
            <ta e="T810" id="Seg_9884" s="T809">будто</ta>
            <ta e="T811" id="Seg_9885" s="T810">замотать-MOM-CVB</ta>
            <ta e="T812" id="Seg_9886" s="T811">сам.3PL</ta>
            <ta e="T813" id="Seg_9887" s="T812">нарты-ILL-OBL.3PL</ta>
            <ta e="T814" id="Seg_9888" s="T813">потом</ta>
            <ta e="T815" id="Seg_9889" s="T814">потом</ta>
            <ta e="T816" id="Seg_9890" s="T815">туда</ta>
            <ta e="T817" id="Seg_9891" s="T816">так</ta>
            <ta e="T818" id="Seg_9892" s="T817">наружу</ta>
            <ta e="T821" id="Seg_9893" s="T820">вроде</ta>
            <ta e="T822" id="Seg_9894" s="T821">дом.[NOM]</ta>
            <ta e="T823" id="Seg_9895" s="T822">войти-RFL.PFV-US-INF</ta>
            <ta e="T824" id="Seg_9896" s="T823">и</ta>
            <ta e="T825" id="Seg_9897" s="T824">то.ли</ta>
            <ta e="T826" id="Seg_9898" s="T825">съесть-EP-FRQ-INF</ta>
            <ta e="T827" id="Seg_9899" s="T826">и</ta>
            <ta e="T828" id="Seg_9900" s="T827">то.ли</ta>
            <ta e="T829" id="Seg_9901" s="T828">NEG</ta>
            <ta e="T830" id="Seg_9902" s="T829">съесть-EP-FRQ-INF</ta>
            <ta e="T831" id="Seg_9903" s="T830">потом</ta>
            <ta e="T832" id="Seg_9904" s="T831">этот</ta>
            <ta e="T833" id="Seg_9905" s="T832">человек.[NOM]</ta>
            <ta e="T834" id="Seg_9906" s="T833">тот</ta>
            <ta e="T835" id="Seg_9907" s="T834">привязать-MOM-PTCP.PST</ta>
            <ta e="T836" id="Seg_9908" s="T835">нарты-ILL-3SG</ta>
            <ta e="T837" id="Seg_9909" s="T836">тот</ta>
            <ta e="T838" id="Seg_9910" s="T837">там</ta>
            <ta e="T839" id="Seg_9911" s="T838">домой</ta>
            <ta e="T840" id="Seg_9912" s="T839">%%-CVB</ta>
            <ta e="T841" id="Seg_9913" s="T840">человек-PL-ACC-3SG</ta>
            <ta e="T842" id="Seg_9914" s="T841">этот</ta>
            <ta e="T843" id="Seg_9915" s="T842">умереть-PTCP.PST</ta>
            <ta e="T844" id="Seg_9916" s="T843">человек-PL-ACC-3SG</ta>
            <ta e="T845" id="Seg_9917" s="T844">так</ta>
            <ta e="T846" id="Seg_9918" s="T845">уйти-TR-CO-3SG.O</ta>
            <ta e="T847" id="Seg_9919" s="T846">домой</ta>
            <ta e="T848" id="Seg_9920" s="T847">вниз.по.течению.реки</ta>
            <ta e="T849" id="Seg_9921" s="T848">домой</ta>
            <ta e="T850" id="Seg_9922" s="T849">Пони.[NOM]</ta>
            <ta e="T851" id="Seg_9923" s="T850">старик-ACC</ta>
            <ta e="T852" id="Seg_9924" s="T851">%%</ta>
            <ta e="T853" id="Seg_9925" s="T852">%%-CVB</ta>
            <ta e="T854" id="Seg_9926" s="T853">уйти-TR-CO-3SG.O</ta>
            <ta e="T855" id="Seg_9927" s="T854">вроде</ta>
            <ta e="T856" id="Seg_9928" s="T855">немного</ta>
            <ta e="T857" id="Seg_9929" s="T856">дыхание.[NOM]-3SG</ta>
            <ta e="T858" id="Seg_9930" s="T857">только</ta>
            <ta e="T859" id="Seg_9931" s="T858">%%</ta>
            <ta e="T860" id="Seg_9932" s="T859">уйти-CO-3PL</ta>
            <ta e="T861" id="Seg_9933" s="T860">тот</ta>
            <ta e="T862" id="Seg_9934" s="T861">потом</ta>
            <ta e="T863" id="Seg_9935" s="T862">хороший-ADVZ</ta>
            <ta e="T864" id="Seg_9936" s="T863">отправиться-CO-3PL</ta>
            <ta e="T865" id="Seg_9937" s="T864">там</ta>
            <ta e="T866" id="Seg_9938" s="T865">жена-OBL.3SG-%%</ta>
            <ta e="T867" id="Seg_9939" s="T866">так</ta>
            <ta e="T868" id="Seg_9940" s="T867">сказать-CO-3SG.O</ta>
            <ta e="T869" id="Seg_9941" s="T868">чум.[NOM]-2SG</ta>
            <ta e="T870" id="Seg_9942" s="T869">прочь</ta>
            <ta e="T871" id="Seg_9943" s="T870">снять-IMP.2SG.O</ta>
            <ta e="T872" id="Seg_9944" s="T871">мы.PL.NOM</ta>
            <ta e="T873" id="Seg_9945" s="T872">вперёд</ta>
            <ta e="T874" id="Seg_9946" s="T873">отправиться-IPFV-DEB-1DU</ta>
            <ta e="T875" id="Seg_9947" s="T874">человек-EP-PL-EP-ALL</ta>
            <ta e="T876" id="Seg_9948" s="T875">NEG</ta>
            <ta e="T877" id="Seg_9949" s="T876">знать-PST.NAR-3PL</ta>
            <ta e="T878" id="Seg_9950" s="T877">вроде</ta>
            <ta e="T879" id="Seg_9951" s="T878">сам.3PL.[NOM]</ta>
            <ta e="T880" id="Seg_9952" s="T879">этот</ta>
            <ta e="T881" id="Seg_9953" s="T880">дедушка-OBL.3SG-ADJZ</ta>
            <ta e="T882" id="Seg_9954" s="T881">нечто-PL.[NOM]</ta>
            <ta e="T883" id="Seg_9955" s="T882">INDEF3</ta>
            <ta e="T884" id="Seg_9956" s="T883">что.[NOM]</ta>
            <ta e="T885" id="Seg_9957" s="T884">вперёд</ta>
            <ta e="T886" id="Seg_9958" s="T885">уйти-CO.[3SG.S]</ta>
            <ta e="T887" id="Seg_9959" s="T886">тот</ta>
            <ta e="T888" id="Seg_9960" s="T887">сам.3SG</ta>
            <ta e="T889" id="Seg_9961" s="T888">NEG</ta>
            <ta e="T890" id="Seg_9962" s="T889">сказать-DUR.[3SG.S]</ta>
            <ta e="T892" id="Seg_9963" s="T891">тот</ta>
            <ta e="T893" id="Seg_9964" s="T892">жена.[NOM]-3SG</ta>
            <ta e="T894" id="Seg_9965" s="T893">вот</ta>
            <ta e="T895" id="Seg_9966" s="T894">сказать-FUT-3SG.O</ta>
            <ta e="T896" id="Seg_9967" s="T895">отец-GEN-3SG</ta>
            <ta e="T897" id="Seg_9968" s="T896">дедушка-GEN-OBL.3SG</ta>
            <ta e="T898" id="Seg_9969" s="T897">нечто-DU-ALL</ta>
            <ta e="T899" id="Seg_9970" s="T898">Пони.[NOM]</ta>
            <ta e="T900" id="Seg_9971" s="T899">старик-ADJZ</ta>
            <ta e="T901" id="Seg_9972" s="T900">нечто-PL.[NOM]</ta>
            <ta e="T902" id="Seg_9973" s="T901">тот</ta>
            <ta e="T903" id="Seg_9974" s="T902">домой</ta>
            <ta e="T904" id="Seg_9975" s="T903">ходить.за.чем_либо-PST-3PL</ta>
            <ta e="T905" id="Seg_9976" s="T904">мол</ta>
            <ta e="T906" id="Seg_9977" s="T905">%%-2SG.O</ta>
            <ta e="T907" id="Seg_9978" s="T906">потом</ta>
            <ta e="T908" id="Seg_9979" s="T907">потом</ta>
            <ta e="T909" id="Seg_9980" s="T908">%%</ta>
            <ta e="T910" id="Seg_9981" s="T909">вроде</ta>
            <ta e="T912" id="Seg_9982" s="T911">NEG</ta>
            <ta e="T985" id="Seg_9983" s="T912">знать-3PL</ta>
            <ta e="T914" id="Seg_9984" s="T913">вниз</ta>
            <ta e="T986" id="Seg_9985" s="T914">сказать-DUR-IMP.2SG.S</ta>
            <ta e="T916" id="Seg_9986" s="T915">вниз</ta>
            <ta e="T987" id="Seg_9987" s="T916">магнитофон-ILL.2SG</ta>
            <ta e="T918" id="Seg_9988" s="T917">вниз.по.течению.реки</ta>
            <ta e="T919" id="Seg_9989" s="T918">вперёд</ta>
            <ta e="T920" id="Seg_9990" s="T919">%%</ta>
            <ta e="T921" id="Seg_9991" s="T920">он(а)-EP-DIM-PL.[NOM]</ta>
            <ta e="T922" id="Seg_9992" s="T921">туда</ta>
            <ta e="T923" id="Seg_9993" s="T922">дедушка-GEN-OBL.3SG</ta>
            <ta e="T924" id="Seg_9994" s="T923">дом.[NOM]</ta>
            <ta e="T925" id="Seg_9995" s="T924">дом.[NOM]</ta>
            <ta e="T926" id="Seg_9996" s="T925">войти-CO.[3SG.S]</ta>
            <ta e="T927" id="Seg_9997" s="T926">нарты-PL.[NOM]</ta>
            <ta e="T928" id="Seg_9998" s="T927">тот</ta>
            <ta e="T929" id="Seg_9999" s="T928">по.этому.месту</ta>
            <ta e="T930" id="Seg_10000" s="T929">замотать-CVB</ta>
            <ta e="T931" id="Seg_10001" s="T930">так</ta>
            <ta e="T932" id="Seg_10002" s="T931">поставить-CO-3SG.O</ta>
            <ta e="T933" id="Seg_10003" s="T932">тот</ta>
            <ta e="T934" id="Seg_10004" s="T933">отправиться-PST.NAR-INFER-3PL</ta>
            <ta e="T935" id="Seg_10005" s="T934">домой</ta>
            <ta e="T936" id="Seg_10006" s="T935">уйти-PST.NAR-INFER-3PL</ta>
            <ta e="T937" id="Seg_10007" s="T936">Пони.[NOM]</ta>
            <ta e="T938" id="Seg_10008" s="T937">старик.[NOM]</ta>
            <ta e="T939" id="Seg_10009" s="T938">еле.еле</ta>
            <ta e="T940" id="Seg_10010" s="T939">вниз.по.течению.реки-ADV.LOC</ta>
            <ta e="T941" id="Seg_10011" s="T940">дом-LOC</ta>
            <ta e="T942" id="Seg_10012" s="T941">прийти-US-PST.NAR.[3SG.S]</ta>
            <ta e="T943" id="Seg_10013" s="T942">вниз</ta>
            <ta e="T944" id="Seg_10014" s="T943">умереть-PST.NAR.[3SG.S]</ta>
            <ta e="T945" id="Seg_10015" s="T944">тот</ta>
            <ta e="T946" id="Seg_10016" s="T945">два</ta>
            <ta e="T947" id="Seg_10017" s="T946">человек-EP-DU.[NOM]</ta>
            <ta e="T948" id="Seg_10018" s="T947">сейчас</ta>
            <ta e="T949" id="Seg_10019" s="T948">жить-DUR-3DU.S</ta>
            <ta e="T950" id="Seg_10020" s="T949">Пони.[NOM]</ta>
            <ta e="T951" id="Seg_10021" s="T950">старик.[NOM]</ta>
            <ta e="T952" id="Seg_10022" s="T951">Пони.[NOM]</ta>
            <ta e="T953" id="Seg_10023" s="T952">старик-ADJZ</ta>
            <ta e="T954" id="Seg_10024" s="T953">нечто-PL.[NOM]</ta>
            <ta e="T955" id="Seg_10025" s="T954">там</ta>
            <ta e="T988" id="Seg_10026" s="T955">быть-CO.[3SG.S]</ta>
            <ta e="T957" id="Seg_10027" s="T956">другой</ta>
            <ta e="T958" id="Seg_10028" s="T957">нечто.[NOM]</ta>
            <ta e="T959" id="Seg_10029" s="T958">сказать-IMP.2SG.O</ta>
            <ta e="T960" id="Seg_10030" s="T959">другой</ta>
            <ta e="T989" id="Seg_10031" s="T960">другой</ta>
            <ta e="T962" id="Seg_10032" s="T961">другой</ta>
            <ta e="T963" id="Seg_10033" s="T962">нечто.[NOM]</ta>
            <ta e="T964" id="Seg_10034" s="T963">сказать-IMP.2SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T3" id="Seg_10035" s="T2">num</ta>
            <ta e="T4" id="Seg_10036" s="T3">n-n:case-n:poss</ta>
            <ta e="T5" id="Seg_10037" s="T4">v-v:tense-v:mood-v:pn</ta>
            <ta e="T7" id="Seg_10038" s="T6">dem</ta>
            <ta e="T8" id="Seg_10039" s="T7">clit</ta>
            <ta e="T9" id="Seg_10040" s="T8">ptcl</ta>
            <ta e="T11" id="Seg_10041" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_10042" s="T11">v-v&gt;v-v:pn</ta>
            <ta e="T13" id="Seg_10043" s="T12">pro-n:case</ta>
            <ta e="T14" id="Seg_10044" s="T13">dem</ta>
            <ta e="T15" id="Seg_10045" s="T14">n-v:pn</ta>
            <ta e="T16" id="Seg_10046" s="T15">dem</ta>
            <ta e="T17" id="Seg_10047" s="T16">n-v:pn</ta>
            <ta e="T18" id="Seg_10048" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_10049" s="T18">n-v:pn</ta>
            <ta e="T21" id="Seg_10050" s="T20">pers</ta>
            <ta e="T22" id="Seg_10051" s="T21">adv</ta>
            <ta e="T24" id="Seg_10052" s="T23">n-n:ins-n:num-n:case</ta>
            <ta e="T25" id="Seg_10053" s="T24">adv</ta>
            <ta e="T27" id="Seg_10054" s="T26">dem</ta>
            <ta e="T28" id="Seg_10055" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_10056" s="T28">pers</ta>
            <ta e="T30" id="Seg_10057" s="T29">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_10058" s="T31">pers</ta>
            <ta e="T33" id="Seg_10059" s="T32">n-n:case-n:poss</ta>
            <ta e="T971" id="Seg_10060" s="T33">v-v&gt;v-v&gt;v-v:inf-%%-v&gt;v-v&gt;v-v:mood.pn</ta>
            <ta e="T972" id="Seg_10061" s="T34">v-v&gt;v-v&gt;v-v:mood.pn</ta>
            <ta e="T36" id="Seg_10062" s="T35">n-n&gt;n-n&gt;adv</ta>
            <ta e="T973" id="Seg_10063" s="T36">v-v:mood.pn</ta>
            <ta e="T38" id="Seg_10064" s="T37">%%</ta>
            <ta e="T39" id="Seg_10065" s="T38">dem</ta>
            <ta e="T40" id="Seg_10066" s="T39">n-n:case</ta>
            <ta e="T41" id="Seg_10067" s="T40">v-v:tense-v:pn</ta>
            <ta e="T42" id="Seg_10068" s="T41">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T43" id="Seg_10069" s="T42">pro</ta>
            <ta e="T44" id="Seg_10070" s="T43">n-n:ins-n&gt;adj</ta>
            <ta e="T45" id="Seg_10071" s="T44">n-v:pn</ta>
            <ta e="T46" id="Seg_10072" s="T45">adj</ta>
            <ta e="T47" id="Seg_10073" s="T46">n-n&gt;adj</ta>
            <ta e="T48" id="Seg_10074" s="T47">n-v:pn</ta>
            <ta e="T49" id="Seg_10075" s="T48">adv-adv&gt;adj</ta>
            <ta e="T50" id="Seg_10076" s="T49">adv-adv&gt;adj</ta>
            <ta e="T51" id="Seg_10077" s="T50">n-n:case</ta>
            <ta e="T52" id="Seg_10078" s="T51">n-n:case-n&gt;adj</ta>
            <ta e="T53" id="Seg_10079" s="T52">v-v:tense-v:pn</ta>
            <ta e="T54" id="Seg_10080" s="T53">adv-adv&gt;adv-n&gt;adj</ta>
            <ta e="T55" id="Seg_10081" s="T54">pers-n:case</ta>
            <ta e="T56" id="Seg_10082" s="T55">n-n&gt;n-n&gt;adj</ta>
            <ta e="T57" id="Seg_10083" s="T56">n-n:case</ta>
            <ta e="T992" id="Seg_10084" s="T58">interrog-n:case</ta>
            <ta e="T60" id="Seg_10085" s="T59">v-v:tense-v:pn</ta>
            <ta e="T61" id="Seg_10086" s="T60">dem</ta>
            <ta e="T62" id="Seg_10087" s="T61">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T63" id="Seg_10088" s="T62">n-n:case-n:poss</ta>
            <ta e="T64" id="Seg_10089" s="T63">num</ta>
            <ta e="T65" id="Seg_10090" s="T64">n-n:case-n:poss</ta>
            <ta e="T66" id="Seg_10091" s="T65">n-n&gt;n-n&gt;adj</ta>
            <ta e="T67" id="Seg_10092" s="T66">n-n&gt;n-n&gt;adj</ta>
            <ta e="T68" id="Seg_10093" s="T67">n-n:case-n:poss</ta>
            <ta e="T69" id="Seg_10094" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_10095" s="T69">n-n:case-n:poss</ta>
            <ta e="T71" id="Seg_10096" s="T70">v-v:pn</ta>
            <ta e="T72" id="Seg_10097" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_10098" s="T72">interrog-n:case-n:poss</ta>
            <ta e="T74" id="Seg_10099" s="T73">v-v:pn</ta>
            <ta e="T75" id="Seg_10100" s="T74">adv</ta>
            <ta e="T76" id="Seg_10101" s="T75">dem</ta>
            <ta e="T77" id="Seg_10102" s="T76">n-n:case-n:poss</ta>
            <ta e="T78" id="Seg_10103" s="T77">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T79" id="Seg_10104" s="T78">dem</ta>
            <ta e="T80" id="Seg_10105" s="T79">n-n&gt;n-n:case</ta>
            <ta e="T82" id="Seg_10106" s="T81">dem</ta>
            <ta e="T83" id="Seg_10107" s="T82">n-n:case</ta>
            <ta e="T84" id="Seg_10108" s="T83">adv</ta>
            <ta e="T85" id="Seg_10109" s="T84">v-v:pn</ta>
            <ta e="T86" id="Seg_10110" s="T85">ptcl</ta>
            <ta e="T87" id="Seg_10111" s="T86">dem</ta>
            <ta e="T88" id="Seg_10112" s="T87">n-n:case-n:poss</ta>
            <ta e="T89" id="Seg_10113" s="T88">n-n:case-n:poss</ta>
            <ta e="T90" id="Seg_10114" s="T89">n-n:case</ta>
            <ta e="T91" id="Seg_10115" s="T90">adv-adv:case</ta>
            <ta e="T92" id="Seg_10116" s="T91">dem</ta>
            <ta e="T93" id="Seg_10117" s="T92">num</ta>
            <ta e="T94" id="Seg_10118" s="T93">n-n:case</ta>
            <ta e="T95" id="Seg_10119" s="T94">n-n&gt;n-n:num-n:case</ta>
            <ta e="T96" id="Seg_10120" s="T95">v-v:tense-v:mood-v:pn</ta>
            <ta e="T97" id="Seg_10121" s="T96">n-n:case</ta>
            <ta e="T98" id="Seg_10122" s="T97">n-n:case</ta>
            <ta e="T99" id="Seg_10123" s="T98">interrog</ta>
            <ta e="T100" id="Seg_10124" s="T99">v-v:tense-v:pn</ta>
            <ta e="T102" id="Seg_10125" s="T101">ptcl-n:case</ta>
            <ta e="T103" id="Seg_10126" s="T102">v-v:pn</ta>
            <ta e="T104" id="Seg_10127" s="T103">interrog</ta>
            <ta e="T105" id="Seg_10128" s="T104">%%</ta>
            <ta e="T106" id="Seg_10129" s="T105">dem</ta>
            <ta e="T107" id="Seg_10130" s="T106">dem</ta>
            <ta e="T108" id="Seg_10131" s="T107">n-n:case</ta>
            <ta e="T109" id="Seg_10132" s="T108">preverb</ta>
            <ta e="T110" id="Seg_10133" s="T109">v-v:ins-v:pn</ta>
            <ta e="T111" id="Seg_10134" s="T110">preverb</ta>
            <ta e="T112" id="Seg_10135" s="T111">v-v:ins-v:pn</ta>
            <ta e="T113" id="Seg_10136" s="T112">dem</ta>
            <ta e="T114" id="Seg_10137" s="T113">adv</ta>
            <ta e="T115" id="Seg_10138" s="T114">adv</ta>
            <ta e="T116" id="Seg_10139" s="T115">dem</ta>
            <ta e="T117" id="Seg_10140" s="T116">n-n:case</ta>
            <ta e="T118" id="Seg_10141" s="T117">n-n:case</ta>
            <ta e="T119" id="Seg_10142" s="T118">n-n:case</ta>
            <ta e="T120" id="Seg_10143" s="T119">adv</ta>
            <ta e="T121" id="Seg_10144" s="T120">interrog</ta>
            <ta e="T122" id="Seg_10145" s="T121">qv-v:tense-v:pn</ta>
            <ta e="T123" id="Seg_10146" s="T122">adv</ta>
            <ta e="T124" id="Seg_10147" s="T123">n-n:obl.poss-n:case</ta>
            <ta e="T125" id="Seg_10148" s="T124">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T126" id="Seg_10149" s="T125">n-n:case</ta>
            <ta e="T127" id="Seg_10150" s="T126">v-v:tense-v:pn</ta>
            <ta e="T128" id="Seg_10151" s="T127">preverb</ta>
            <ta e="T129" id="Seg_10152" s="T128">v-v:ins-n:poss</ta>
            <ta e="T130" id="Seg_10153" s="T129">adv</ta>
            <ta e="T131" id="Seg_10154" s="T130">n-n:case-n:poss</ta>
            <ta e="T132" id="Seg_10155" s="T131">dem</ta>
            <ta e="T133" id="Seg_10156" s="T132">n-n:case-n:poss</ta>
            <ta e="T134" id="Seg_10157" s="T133">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T135" id="Seg_10158" s="T134">n-n:case-n:poss</ta>
            <ta e="T136" id="Seg_10159" s="T135">ptcl</ta>
            <ta e="T137" id="Seg_10160" s="T136">preverb</ta>
            <ta e="T138" id="Seg_10161" s="T137">v-v:ins-v:pn</ta>
            <ta e="T139" id="Seg_10162" s="T138">n-n&gt;adj</ta>
            <ta e="T140" id="Seg_10163" s="T139">n-n:num-n:case</ta>
            <ta e="T141" id="Seg_10164" s="T140">dem</ta>
            <ta e="T142" id="Seg_10165" s="T141">n-n:case-n:poss</ta>
            <ta e="T143" id="Seg_10166" s="T142">n-n&gt;adj</ta>
            <ta e="T144" id="Seg_10167" s="T143">n-n&gt;adj</ta>
            <ta e="T145" id="Seg_10168" s="T144">v-v:pn</ta>
            <ta e="T146" id="Seg_10169" s="T145">interrog</ta>
            <ta e="T147" id="Seg_10170" s="T146">qv-v:tense-v:pn</ta>
            <ta e="T148" id="Seg_10171" s="T147">dem</ta>
            <ta e="T149" id="Seg_10172" s="T148">n-n:case</ta>
            <ta e="T150" id="Seg_10173" s="T149">n-n:case</ta>
            <ta e="T151" id="Seg_10174" s="T150">adv</ta>
            <ta e="T152" id="Seg_10175" s="T151">adv</ta>
            <ta e="T153" id="Seg_10176" s="T152">adv</ta>
            <ta e="T154" id="Seg_10177" s="T153">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T155" id="Seg_10178" s="T154">dem</ta>
            <ta e="T156" id="Seg_10179" s="T155">n-n&gt;adj</ta>
            <ta e="T157" id="Seg_10180" s="T156">n-n:case</ta>
            <ta e="T158" id="Seg_10181" s="T157">adj</ta>
            <ta e="T159" id="Seg_10182" s="T158">n-n:case-n:poss</ta>
            <ta e="T167" id="Seg_10183" s="T166">adj</ta>
            <ta e="T168" id="Seg_10184" s="T167">nprop-n&gt;adj</ta>
            <ta e="T169" id="Seg_10185" s="T168">n-n:case</ta>
            <ta e="T170" id="Seg_10186" s="T169">pp</ta>
            <ta e="T171" id="Seg_10187" s="T170">dem-adj&gt;adj</ta>
            <ta e="T172" id="Seg_10188" s="T171">n-n:case-n:poss-n&gt;adj</ta>
            <ta e="T173" id="Seg_10189" s="T172">v-v:tense-v:pn</ta>
            <ta e="T174" id="Seg_10190" s="T173">adv</ta>
            <ta e="T175" id="Seg_10191" s="T174">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T176" id="Seg_10192" s="T175">n-n:case-n:case-n&gt;adj</ta>
            <ta e="T177" id="Seg_10193" s="T176">v-v:pn</ta>
            <ta e="T178" id="Seg_10194" s="T177">n-n&gt;adj</ta>
            <ta e="T179" id="Seg_10195" s="T178">n-n:case</ta>
            <ta e="T180" id="Seg_10196" s="T179">v-v:tense-n:poss</ta>
            <ta e="T181" id="Seg_10197" s="T180">dem</ta>
            <ta e="T182" id="Seg_10198" s="T181">n-n&gt;n-n:num-n:case</ta>
            <ta e="T183" id="Seg_10199" s="T182">adv</ta>
            <ta e="T184" id="Seg_10200" s="T183">v-v:tense-v:pn</ta>
            <ta e="T185" id="Seg_10201" s="T184">v-v:tense-v:pn</ta>
            <ta e="T186" id="Seg_10202" s="T185">adv</ta>
            <ta e="T187" id="Seg_10203" s="T186">adv</ta>
            <ta e="T188" id="Seg_10204" s="T187">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T189" id="Seg_10205" s="T188">adv</ta>
            <ta e="T190" id="Seg_10206" s="T189">v-v&gt;adv</ta>
            <ta e="T191" id="Seg_10207" s="T190">dem</ta>
            <ta e="T192" id="Seg_10208" s="T191">n-n:case</ta>
            <ta e="T193" id="Seg_10209" s="T192">n-n:case</ta>
            <ta e="T194" id="Seg_10210" s="T193">n-n:case</ta>
            <ta e="T195" id="Seg_10211" s="T194">n-n:case</ta>
            <ta e="T196" id="Seg_10212" s="T195">n-n:case</ta>
            <ta e="T197" id="Seg_10213" s="T196">n-n:case</ta>
            <ta e="T198" id="Seg_10214" s="T197">n-n:case-n:poss</ta>
            <ta e="T199" id="Seg_10215" s="T198">v-v:pn</ta>
            <ta e="T200" id="Seg_10216" s="T199">n-n&gt;n-n&gt;adj</ta>
            <ta e="T201" id="Seg_10217" s="T200">n-n:case-n:poss</ta>
            <ta e="T202" id="Seg_10218" s="T201">dem</ta>
            <ta e="T203" id="Seg_10219" s="T202">n-n&gt;adj</ta>
            <ta e="T204" id="Seg_10220" s="T203">n-n:case-n:poss</ta>
            <ta e="T206" id="Seg_10221" s="T205">num</ta>
            <ta e="T207" id="Seg_10222" s="T206">n-n:case-n:poss</ta>
            <ta e="T208" id="Seg_10223" s="T207">n-n&gt;n-n&gt;adj</ta>
            <ta e="T209" id="Seg_10224" s="T208">adv-adv:case</ta>
            <ta e="T210" id="Seg_10225" s="T209">adv</ta>
            <ta e="T211" id="Seg_10226" s="T210">num</ta>
            <ta e="T212" id="Seg_10227" s="T211">n-n:num-n:case</ta>
            <ta e="T213" id="Seg_10228" s="T212">pers-n:case</ta>
            <ta e="T214" id="Seg_10229" s="T213">n-n&gt;n-n&gt;adj</ta>
            <ta e="T215" id="Seg_10230" s="T214">n-%%</ta>
            <ta e="T216" id="Seg_10231" s="T215">nprop-n:case</ta>
            <ta e="T217" id="Seg_10232" s="T216">n-n:case</ta>
            <ta e="T218" id="Seg_10233" s="T217">nprop-n:case</ta>
            <ta e="T219" id="Seg_10234" s="T218">n-n:case</ta>
            <ta e="T220" id="Seg_10235" s="T219">v-v:pn</ta>
            <ta e="T221" id="Seg_10236" s="T220">interrog</ta>
            <ta e="T222" id="Seg_10237" s="T221">quant-n&gt;adj</ta>
            <ta e="T223" id="Seg_10238" s="T222">n-n:case-n:poss</ta>
            <ta e="T224" id="Seg_10239" s="T223">n-n:case</ta>
            <ta e="T225" id="Seg_10240" s="T224">quant</ta>
            <ta e="T226" id="Seg_10241" s="T225">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T227" id="Seg_10242" s="T226">%%-v:pn</ta>
            <ta e="T228" id="Seg_10243" s="T227">dem-adj&gt;adv</ta>
            <ta e="T229" id="Seg_10244" s="T228">v-v:ins-v:pn</ta>
            <ta e="T230" id="Seg_10245" s="T229">dem</ta>
            <ta e="T231" id="Seg_10246" s="T230">n-n&gt;n-n:num-n:case</ta>
            <ta e="T232" id="Seg_10247" s="T231">dem</ta>
            <ta e="T233" id="Seg_10248" s="T232">n-n&gt;n-n:num-n:case</ta>
            <ta e="T234" id="Seg_10249" s="T233">v-v:pn</ta>
            <ta e="T235" id="Seg_10250" s="T234">dem</ta>
            <ta e="T236" id="Seg_10251" s="T235">n-n&gt;adj</ta>
            <ta e="T237" id="Seg_10252" s="T236">n-n&gt;n-n:num-n:case</ta>
            <ta e="T238" id="Seg_10253" s="T237">num</ta>
            <ta e="T239" id="Seg_10254" s="T238">num</ta>
            <ta e="T240" id="Seg_10255" s="T239">num</ta>
            <ta e="T241" id="Seg_10256" s="T240">n-n:case-n:poss</ta>
            <ta e="T242" id="Seg_10257" s="T241">dem</ta>
            <ta e="T243" id="Seg_10258" s="T242">n-n:case</ta>
            <ta e="T244" id="Seg_10259" s="T243">n-n:case</ta>
            <ta e="T245" id="Seg_10260" s="T244">v-v:inf.poss</ta>
            <ta e="T246" id="Seg_10261" s="T245">n-n:num-n:obl.poss-n:case</ta>
            <ta e="T247" id="Seg_10262" s="T246">v-v:tense-v:pn</ta>
            <ta e="T248" id="Seg_10263" s="T247">nprop-n:case</ta>
            <ta e="T249" id="Seg_10264" s="T248">n-n:case</ta>
            <ta e="T250" id="Seg_10265" s="T249">n-n:case-n:poss</ta>
            <ta e="T251" id="Seg_10266" s="T250">v-v:pn</ta>
            <ta e="T252" id="Seg_10267" s="T251">adv</ta>
            <ta e="T253" id="Seg_10268" s="T252">%%-v:pn</ta>
            <ta e="T254" id="Seg_10269" s="T253">v-v&gt;adv</ta>
            <ta e="T255" id="Seg_10270" s="T254">v-v:pn</ta>
            <ta e="T256" id="Seg_10271" s="T255">dem</ta>
            <ta e="T257" id="Seg_10272" s="T256">pers-n:num-n:ins-n:case</ta>
            <ta e="T258" id="Seg_10273" s="T257">n-n:case</ta>
            <ta e="T259" id="Seg_10274" s="T258">dem</ta>
            <ta e="T260" id="Seg_10275" s="T259">n-n:case</ta>
            <ta e="T261" id="Seg_10276" s="T260">ptcl</ta>
            <ta e="T262" id="Seg_10277" s="T261">ptcl</ta>
            <ta e="T263" id="Seg_10278" s="T262">v-v:pn</ta>
            <ta e="T265" id="Seg_10279" s="T264">conj</ta>
            <ta e="T266" id="Seg_10280" s="T265">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T267" id="Seg_10281" s="T266">pers-n:case</ta>
            <ta e="T268" id="Seg_10282" s="T267">interrog-n:case</ta>
            <ta e="T270" id="Seg_10283" s="T269">adj-n&gt;adv</ta>
            <ta e="T272" id="Seg_10284" s="T271">conj</ta>
            <ta e="T273" id="Seg_10285" s="T272">conj</ta>
            <ta e="T274" id="Seg_10286" s="T273">v-v:pn</ta>
            <ta e="T275" id="Seg_10287" s="T274">adv</ta>
            <ta e="T276" id="Seg_10288" s="T275">conj</ta>
            <ta e="T277" id="Seg_10289" s="T276">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T278" id="Seg_10290" s="T277">adv-adv:case</ta>
            <ta e="T279" id="Seg_10291" s="T278">v-v&gt;ptcp-n:case</ta>
            <ta e="T280" id="Seg_10292" s="T279">v-v:pn</ta>
            <ta e="T281" id="Seg_10293" s="T280">ptcl</ta>
            <ta e="T282" id="Seg_10294" s="T281">interrog-n:case</ta>
            <ta e="T283" id="Seg_10295" s="T282">ptcl</ta>
            <ta e="T284" id="Seg_10296" s="T283">num-num&gt;adj</ta>
            <ta e="T285" id="Seg_10297" s="T284">n-n:case</ta>
            <ta e="T286" id="Seg_10298" s="T285">%%</ta>
            <ta e="T287" id="Seg_10299" s="T286">ptcl</ta>
            <ta e="T288" id="Seg_10300" s="T287">ptcl</ta>
            <ta e="T289" id="Seg_10301" s="T288">v-v:ins-v:pn</ta>
            <ta e="T290" id="Seg_10302" s="T289">ptcl</ta>
            <ta e="T291" id="Seg_10303" s="T290">nprop-n:case</ta>
            <ta e="T292" id="Seg_10304" s="T291">n-n:case</ta>
            <ta e="T294" id="Seg_10305" s="T293">num</ta>
            <ta e="T295" id="Seg_10306" s="T294">n-n:case</ta>
            <ta e="T296" id="Seg_10307" s="T295">v-v:tense-v:pn</ta>
            <ta e="T297" id="Seg_10308" s="T296">ptcl</ta>
            <ta e="T298" id="Seg_10309" s="T297">dem</ta>
            <ta e="T299" id="Seg_10310" s="T298">n-n:num-n:case</ta>
            <ta e="T300" id="Seg_10311" s="T299">v-v:inf.poss</ta>
            <ta e="T301" id="Seg_10312" s="T300">dem</ta>
            <ta e="T302" id="Seg_10313" s="T301">n-n:ins-n:num-n:case</ta>
            <ta e="T303" id="Seg_10314" s="T302">adv</ta>
            <ta e="T304" id="Seg_10315" s="T303">dem</ta>
            <ta e="T305" id="Seg_10316" s="T304">n-n&gt;n-n:num-n:case</ta>
            <ta e="T306" id="Seg_10317" s="T305">dem</ta>
            <ta e="T307" id="Seg_10318" s="T306">n-n:case</ta>
            <ta e="T308" id="Seg_10319" s="T307">v-v&gt;v-v:pn</ta>
            <ta e="T309" id="Seg_10320" s="T308">interrog-n:case</ta>
            <ta e="T310" id="Seg_10321" s="T309">quant-n&gt;adj</ta>
            <ta e="T311" id="Seg_10322" s="T310">n-n:case-n:poss</ta>
            <ta e="T312" id="Seg_10323" s="T311">dem</ta>
            <ta e="T313" id="Seg_10324" s="T312">adj</ta>
            <ta e="T314" id="Seg_10325" s="T313">n-n:case-n:poss</ta>
            <ta e="T315" id="Seg_10326" s="T314">interrog-n:case</ta>
            <ta e="T316" id="Seg_10327" s="T315">quant-n&gt;adj</ta>
            <ta e="T317" id="Seg_10328" s="T316">interrog-n:case-n:poss</ta>
            <ta e="T318" id="Seg_10329" s="T317">quant</ta>
            <ta e="T319" id="Seg_10330" s="T318">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T320" id="Seg_10331" s="T319">adv</ta>
            <ta e="T321" id="Seg_10332" s="T320">adv</ta>
            <ta e="T322" id="Seg_10333" s="T321">adv-n&gt;adj</ta>
            <ta e="T323" id="Seg_10334" s="T322">n-n:num-n:case</ta>
            <ta e="T324" id="Seg_10335" s="T323">n-n:case</ta>
            <ta e="T325" id="Seg_10336" s="T324">v-v&gt;v-v&gt;adv</ta>
            <ta e="T326" id="Seg_10337" s="T325">adv</ta>
            <ta e="T327" id="Seg_10338" s="T326">quant</ta>
            <ta e="T328" id="Seg_10339" s="T327">v-v:tense-v:pn</ta>
            <ta e="T329" id="Seg_10340" s="T328">adv</ta>
            <ta e="T330" id="Seg_10341" s="T329">adv</ta>
            <ta e="T331" id="Seg_10342" s="T330">%%</ta>
            <ta e="T332" id="Seg_10343" s="T331">adv</ta>
            <ta e="T333" id="Seg_10344" s="T332">v-v:ins-v:pn</ta>
            <ta e="T334" id="Seg_10345" s="T333">dem</ta>
            <ta e="T335" id="Seg_10346" s="T334">n-n:case</ta>
            <ta e="T336" id="Seg_10347" s="T335">n-n:case</ta>
            <ta e="T337" id="Seg_10348" s="T336">num-num&gt;adj</ta>
            <ta e="T338" id="Seg_10349" s="T337">n-n:case-n:poss</ta>
            <ta e="T339" id="Seg_10350" s="T338">ptcl</ta>
            <ta e="T340" id="Seg_10351" s="T339">v-v:tense-v:pn</ta>
            <ta e="T341" id="Seg_10352" s="T340">ptcl</ta>
            <ta e="T342" id="Seg_10353" s="T341">pers</ta>
            <ta e="T343" id="Seg_10354" s="T342">ptcl</ta>
            <ta e="T344" id="Seg_10355" s="T343">interrog-%%</ta>
            <ta e="T345" id="Seg_10356" s="T344">ptcl</ta>
            <ta e="T346" id="Seg_10357" s="T345">v-v:mood.pn</ta>
            <ta e="T347" id="Seg_10358" s="T346">num-num&gt;adj</ta>
            <ta e="T348" id="Seg_10359" s="T347">n-n:case</ta>
            <ta e="T349" id="Seg_10360" s="T348">ptcl</ta>
            <ta e="T350" id="Seg_10361" s="T349">n-n&gt;v-v:ins-v:pn</ta>
            <ta e="T351" id="Seg_10362" s="T350">dem</ta>
            <ta e="T352" id="Seg_10363" s="T351">n-n:case</ta>
            <ta e="T353" id="Seg_10364" s="T352">dem-adj&gt;adv</ta>
            <ta e="T354" id="Seg_10365" s="T353">v-v:pn</ta>
            <ta e="T355" id="Seg_10366" s="T354">dem</ta>
            <ta e="T356" id="Seg_10367" s="T355">n-n:obl.poss-n:case</ta>
            <ta e="T357" id="Seg_10368" s="T356">dem</ta>
            <ta e="T358" id="Seg_10369" s="T357">n-n:ins-n:case</ta>
            <ta e="T359" id="Seg_10370" s="T358">n-n:case</ta>
            <ta e="T360" id="Seg_10371" s="T359">ptcl</ta>
            <ta e="T361" id="Seg_10372" s="T360">adv</ta>
            <ta e="T362" id="Seg_10373" s="T361">adv</ta>
            <ta e="T363" id="Seg_10374" s="T362">n-n:case</ta>
            <ta e="T364" id="Seg_10375" s="T363">v-v:tense.mood-v:pn</ta>
            <ta e="T365" id="Seg_10376" s="T364">ptcl</ta>
            <ta e="T366" id="Seg_10377" s="T365">num</ta>
            <ta e="T367" id="Seg_10378" s="T366">num</ta>
            <ta e="T368" id="Seg_10379" s="T367">n-n:case</ta>
            <ta e="T369" id="Seg_10380" s="T368">v-v&gt;v-v&gt;n-n:case</ta>
            <ta e="T370" id="Seg_10381" s="T369">ptcl</ta>
            <ta e="T371" id="Seg_10382" s="T370">n-n:case</ta>
            <ta e="T372" id="Seg_10383" s="T371">v-v:tense.mood-v:pn</ta>
            <ta e="T373" id="Seg_10384" s="T372">ptcl</ta>
            <ta e="T374" id="Seg_10385" s="T373">adv-adv:case</ta>
            <ta e="T375" id="Seg_10386" s="T374">v-v:mood-v:pn</ta>
            <ta e="T376" id="Seg_10387" s="T375">ptcl</ta>
            <ta e="T377" id="Seg_10388" s="T376">n-n:num-n:case</ta>
            <ta e="T378" id="Seg_10389" s="T377">v-v:tense-v:pn</ta>
            <ta e="T379" id="Seg_10390" s="T378">adv</ta>
            <ta e="T380" id="Seg_10391" s="T379">n-n:case</ta>
            <ta e="T381" id="Seg_10392" s="T380">v-v:inf-v-v:tense-v:pn</ta>
            <ta e="T382" id="Seg_10393" s="T381">adv</ta>
            <ta e="T383" id="Seg_10394" s="T382">ptcl</ta>
            <ta e="T384" id="Seg_10395" s="T383">dem</ta>
            <ta e="T385" id="Seg_10396" s="T384">n-n:case</ta>
            <ta e="T386" id="Seg_10397" s="T385">n-n:case-n:poss</ta>
            <ta e="T387" id="Seg_10398" s="T386">n-%%</ta>
            <ta e="T388" id="Seg_10399" s="T387">n-n&gt;v-v&gt;ptcp</ta>
            <ta e="T389" id="Seg_10400" s="T388">ptcl</ta>
            <ta e="T390" id="Seg_10401" s="T389">interrog</ta>
            <ta e="T391" id="Seg_10402" s="T390">v-v:tense-v:pn</ta>
            <ta e="T392" id="Seg_10403" s="T391">dem</ta>
            <ta e="T393" id="Seg_10404" s="T392">n-n:case</ta>
            <ta e="T394" id="Seg_10405" s="T393">adv</ta>
            <ta e="T395" id="Seg_10406" s="T394">v-v:ins-v:pn</ta>
            <ta e="T396" id="Seg_10407" s="T395">adv</ta>
            <ta e="T397" id="Seg_10408" s="T396">adv</ta>
            <ta e="T398" id="Seg_10409" s="T397">v-v:ins-v:pn</ta>
            <ta e="T399" id="Seg_10410" s="T398">ptcl</ta>
            <ta e="T401" id="Seg_10411" s="T400">pers</ta>
            <ta e="T402" id="Seg_10412" s="T401">v-v:mood.pn</ta>
            <ta e="T403" id="Seg_10413" s="T402">adj</ta>
            <ta e="T404" id="Seg_10414" s="T403">adj</ta>
            <ta e="T405" id="Seg_10415" s="T404">n-n:case-n:poss</ta>
            <ta e="T406" id="Seg_10416" s="T405">v-v:ins-v:pn</ta>
            <ta e="T407" id="Seg_10417" s="T406">n-n:case</ta>
            <ta e="T408" id="Seg_10418" s="T407">ptcl</ta>
            <ta e="T409" id="Seg_10419" s="T408">interrog-n:case</ta>
            <ta e="T410" id="Seg_10420" s="T409">adj</ta>
            <ta e="T411" id="Seg_10421" s="T410">n-n:case-n:poss</ta>
            <ta e="T412" id="Seg_10422" s="T411">v-v:ins-v:pn</ta>
            <ta e="T413" id="Seg_10423" s="T412">pers</ta>
            <ta e="T414" id="Seg_10424" s="T413">v-v:mood.pn</ta>
            <ta e="T415" id="Seg_10425" s="T414">ptcl</ta>
            <ta e="T416" id="Seg_10426" s="T415">conj</ta>
            <ta e="T417" id="Seg_10427" s="T416">dem</ta>
            <ta e="T418" id="Seg_10428" s="T417">dem</ta>
            <ta e="T419" id="Seg_10429" s="T418">n-n:case-n:poss</ta>
            <ta e="T420" id="Seg_10430" s="T419">n-n:case</ta>
            <ta e="T421" id="Seg_10431" s="T420">n-n:case-n:poss</ta>
            <ta e="T422" id="Seg_10432" s="T421">pro-n:case</ta>
            <ta e="T423" id="Seg_10433" s="T422">v-v&gt;v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T424" id="Seg_10434" s="T423">dem</ta>
            <ta e="T425" id="Seg_10435" s="T424">n-n:case</ta>
            <ta e="T426" id="Seg_10436" s="T425">pers</ta>
            <ta e="T427" id="Seg_10437" s="T426">ptcl</ta>
            <ta e="T428" id="Seg_10438" s="T427">adv</ta>
            <ta e="T429" id="Seg_10439" s="T428">v-%%-v:tense-v:pn</ta>
            <ta e="T430" id="Seg_10440" s="T429">n-n:case</ta>
            <ta e="T431" id="Seg_10441" s="T430">adv-adv:case</ta>
            <ta e="T432" id="Seg_10442" s="T431">%%</ta>
            <ta e="T433" id="Seg_10443" s="T432">n-n&gt;adj</ta>
            <ta e="T434" id="Seg_10444" s="T433">n-n:case</ta>
            <ta e="T435" id="Seg_10445" s="T434">v-v:tense-v:pn</ta>
            <ta e="T436" id="Seg_10446" s="T435">dem</ta>
            <ta e="T437" id="Seg_10447" s="T436">adv-adv&gt;adj</ta>
            <ta e="T438" id="Seg_10448" s="T437">n-n:case</ta>
            <ta e="T439" id="Seg_10449" s="T438">n-n:ins-n&gt;v-v:tense-v:pn</ta>
            <ta e="T440" id="Seg_10450" s="T439">adj</ta>
            <ta e="T441" id="Seg_10451" s="T440">n-n:case</ta>
            <ta e="T442" id="Seg_10452" s="T441">v-v:ins-v:pn</ta>
            <ta e="T443" id="Seg_10453" s="T442">v-v:pn</ta>
            <ta e="T444" id="Seg_10454" s="T443">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T445" id="Seg_10455" s="T444">%%-v:pn</ta>
            <ta e="T446" id="Seg_10456" s="T445">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T447" id="Seg_10457" s="T446">ptcl</ta>
            <ta e="T448" id="Seg_10458" s="T447">ptcl</ta>
            <ta e="T449" id="Seg_10459" s="T448">v-v:tense-v:pn</ta>
            <ta e="T450" id="Seg_10460" s="T449">num-num&gt;adj</ta>
            <ta e="T451" id="Seg_10461" s="T450">num-num&gt;adj</ta>
            <ta e="T452" id="Seg_10462" s="T451">n-n:case</ta>
            <ta e="T453" id="Seg_10463" s="T452">v-v:tense-v:pn</ta>
            <ta e="T454" id="Seg_10464" s="T453">nprop-n:case</ta>
            <ta e="T455" id="Seg_10465" s="T454">n-n&gt;adj</ta>
            <ta e="T456" id="Seg_10466" s="T455">n-n:case</ta>
            <ta e="T457" id="Seg_10467" s="T456">pers-n:ins-n:num-n:ins-n:case</ta>
            <ta e="T458" id="Seg_10468" s="T457">dem</ta>
            <ta e="T459" id="Seg_10469" s="T458">n-n:case</ta>
            <ta e="T460" id="Seg_10470" s="T459">n-n&gt;adj</ta>
            <ta e="T461" id="Seg_10471" s="T460">n-n:num-n:case</ta>
            <ta e="T462" id="Seg_10472" s="T461">adv</ta>
            <ta e="T463" id="Seg_10473" s="T462">v-v&gt;v-v:inf.poss</ta>
            <ta e="T464" id="Seg_10474" s="T463">n-n&gt;n-n:num-n:case</ta>
            <ta e="T465" id="Seg_10475" s="T464">adv</ta>
            <ta e="T466" id="Seg_10476" s="T465">dem</ta>
            <ta e="T467" id="Seg_10477" s="T466">n-n:case</ta>
            <ta e="T468" id="Seg_10478" s="T467">adv</ta>
            <ta e="T469" id="Seg_10479" s="T468">adv</ta>
            <ta e="T470" id="Seg_10480" s="T469">v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T471" id="Seg_10481" s="T470">dem</ta>
            <ta e="T472" id="Seg_10482" s="T471">n-n:case</ta>
            <ta e="T473" id="Seg_10483" s="T472">ptcl</ta>
            <ta e="T474" id="Seg_10484" s="T473">v-v:tense.mood-v:pn</ta>
            <ta e="T475" id="Seg_10485" s="T474">ptcl</ta>
            <ta e="T476" id="Seg_10486" s="T475">pers-n:case</ta>
            <ta e="T477" id="Seg_10487" s="T476">ptcl</ta>
            <ta e="T479" id="Seg_10488" s="T478">n-n:case</ta>
            <ta e="T480" id="Seg_10489" s="T479">ptcl</ta>
            <ta e="T481" id="Seg_10490" s="T480">v-v:tense.mood-v:pn</ta>
            <ta e="T482" id="Seg_10491" s="T481">n-n:case</ta>
            <ta e="T483" id="Seg_10492" s="T482">v-v:ins-v:pn</ta>
            <ta e="T484" id="Seg_10493" s="T483">dem</ta>
            <ta e="T485" id="Seg_10494" s="T484">n-n:case</ta>
            <ta e="T486" id="Seg_10495" s="T485">n-n:case</ta>
            <ta e="T487" id="Seg_10496" s="T486">adv-adv&gt;adj</ta>
            <ta e="T488" id="Seg_10497" s="T487">n-n&gt;adj</ta>
            <ta e="T489" id="Seg_10498" s="T488">n-n:case</ta>
            <ta e="T490" id="Seg_10499" s="T489">v-v:ins-v:pn</ta>
            <ta e="T491" id="Seg_10500" s="T490">adv</ta>
            <ta e="T492" id="Seg_10501" s="T491">ptcl</ta>
            <ta e="T493" id="Seg_10502" s="T492">n-n:ins-n&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T494" id="Seg_10503" s="T493">dem</ta>
            <ta e="T495" id="Seg_10504" s="T494">n-n:obl.poss-n:case</ta>
            <ta e="T496" id="Seg_10505" s="T495">ptcl</ta>
            <ta e="T497" id="Seg_10506" s="T496">n-n&gt;adj</ta>
            <ta e="T993" id="Seg_10507" s="T497">n-n:case</ta>
            <ta e="T498" id="Seg_10508" s="T993">n-n:case</ta>
            <ta e="T499" id="Seg_10509" s="T498">interrog-n:case</ta>
            <ta e="T500" id="Seg_10510" s="T499">n-n:case-n:poss</ta>
            <ta e="T501" id="Seg_10511" s="T500">v-v:pn</ta>
            <ta e="T502" id="Seg_10512" s="T501">adj</ta>
            <ta e="T503" id="Seg_10513" s="T502">n-n:case-n:poss</ta>
            <ta e="T504" id="Seg_10514" s="T503">n-n&gt;adj</ta>
            <ta e="T505" id="Seg_10515" s="T504">n-n:case-n:poss</ta>
            <ta e="T506" id="Seg_10516" s="T505">dem</ta>
            <ta e="T507" id="Seg_10517" s="T506">adj-adj&gt;adj</ta>
            <ta e="T975" id="Seg_10518" s="T507">v-v:ins-v:pn</ta>
            <ta e="T976" id="Seg_10519" s="T509">v-v&gt;v-v:mood.pn</ta>
            <ta e="T512" id="Seg_10520" s="T511">dem</ta>
            <ta e="T513" id="Seg_10521" s="T512">adv-adv&gt;adv</ta>
            <ta e="T514" id="Seg_10522" s="T513">n-n&gt;v-v&gt;v-v:mood.pn</ta>
            <ta e="T517" id="Seg_10523" s="T516">dem</ta>
            <ta e="T518" id="Seg_10524" s="T517">ptcl</ta>
            <ta e="T520" id="Seg_10525" s="T519">adj-adj&gt;adv</ta>
            <ta e="T521" id="Seg_10526" s="T520">ptcl</ta>
            <ta e="T522" id="Seg_10527" s="T521">v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T523" id="Seg_10528" s="T522">ptcl</ta>
            <ta e="T980" id="Seg_10529" s="T523">v-v&gt;v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T525" id="Seg_10530" s="T524">%%-v:pn</ta>
            <ta e="T526" id="Seg_10531" s="T525">ptcl</ta>
            <ta e="T981" id="Seg_10532" s="T526">v-v:mood.pn</ta>
            <ta e="T528" id="Seg_10533" s="T527">ptcl</ta>
            <ta e="T529" id="Seg_10534" s="T528">n-n&gt;n-n&gt;adj</ta>
            <ta e="T531" id="Seg_10535" s="T530">ptcl</ta>
            <ta e="T532" id="Seg_10536" s="T531">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T533" id="Seg_10537" s="T532">ptcl</ta>
            <ta e="T534" id="Seg_10538" s="T533">adv</ta>
            <ta e="T983" id="Seg_10539" s="T534">v-v:ins-v:pn</ta>
            <ta e="T974" id="Seg_10540" s="T535">n-n&gt;adj</ta>
            <ta e="T537" id="Seg_10541" s="T536">adv</ta>
            <ta e="T538" id="Seg_10542" s="T537">adv</ta>
            <ta e="T539" id="Seg_10543" s="T538">v-v:pn</ta>
            <ta e="T540" id="Seg_10544" s="T539">dem</ta>
            <ta e="T541" id="Seg_10545" s="T540">n-n:case</ta>
            <ta e="T542" id="Seg_10546" s="T541">n-n:obl.poss-n:case</ta>
            <ta e="T543" id="Seg_10547" s="T542">v-v:pn</ta>
            <ta e="T544" id="Seg_10548" s="T543">n-n&gt;adj</ta>
            <ta e="T545" id="Seg_10549" s="T544">n-n:case</ta>
            <ta e="T546" id="Seg_10550" s="T545">dem-n&gt;adj</ta>
            <ta e="T547" id="Seg_10551" s="T546">v-v:pn</ta>
            <ta e="T548" id="Seg_10552" s="T547">dem</ta>
            <ta e="T549" id="Seg_10553" s="T548">n-n:obl.poss-n:case</ta>
            <ta e="T550" id="Seg_10554" s="T549">pers</ta>
            <ta e="T551" id="Seg_10555" s="T550">ptcl</ta>
            <ta e="T552" id="Seg_10556" s="T551">adv</ta>
            <ta e="T553" id="Seg_10557" s="T552">n-n:num-n:case.poss</ta>
            <ta e="T554" id="Seg_10558" s="T553">v-%%-v:pn</ta>
            <ta e="T555" id="Seg_10559" s="T554">ptcl-n:case</ta>
            <ta e="T556" id="Seg_10560" s="T555">ptcl</ta>
            <ta e="T557" id="Seg_10561" s="T556">n-n:ins-n&gt;v-v:tense-v:pn</ta>
            <ta e="T558" id="Seg_10562" s="T557">conj-n:case</ta>
            <ta e="T559" id="Seg_10563" s="T558">qv-v:tense-v:pn</ta>
            <ta e="T560" id="Seg_10564" s="T559">adv</ta>
            <ta e="T561" id="Seg_10565" s="T560">v-v&gt;v-v:pn</ta>
            <ta e="T562" id="Seg_10566" s="T561">num</ta>
            <ta e="T563" id="Seg_10567" s="T562">n-n:case</ta>
            <ta e="T564" id="Seg_10568" s="T563">v-v&gt;adv</ta>
            <ta e="T565" id="Seg_10569" s="T564">ptcl</ta>
            <ta e="T566" id="Seg_10570" s="T565">num-num&gt;num-n:ins-num&gt;adj</ta>
            <ta e="T567" id="Seg_10571" s="T566">n-n:case</ta>
            <ta e="T568" id="Seg_10572" s="T567">v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T569" id="Seg_10573" s="T568">ptcl</ta>
            <ta e="T570" id="Seg_10574" s="T569">n-n:case</ta>
            <ta e="T571" id="Seg_10575" s="T570">adv</ta>
            <ta e="T572" id="Seg_10576" s="T571">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T573" id="Seg_10577" s="T572">n-n:case-n:poss</ta>
            <ta e="T574" id="Seg_10578" s="T573">emphpro</ta>
            <ta e="T575" id="Seg_10579" s="T574">v-v:pn</ta>
            <ta e="T576" id="Seg_10580" s="T575">n-n&gt;adj</ta>
            <ta e="T577" id="Seg_10581" s="T576">ptcl</ta>
            <ta e="T578" id="Seg_10582" s="T577">v-v:ins-v:pn</ta>
            <ta e="T579" id="Seg_10583" s="T578">adv</ta>
            <ta e="T580" id="Seg_10584" s="T579">ptcl</ta>
            <ta e="T582" id="Seg_10585" s="T581">num</ta>
            <ta e="T583" id="Seg_10586" s="T582">dem-n:case</ta>
            <ta e="T584" id="Seg_10587" s="T583">n-n&gt;adv</ta>
            <ta e="T585" id="Seg_10588" s="T584">n-n:case-n:poss</ta>
            <ta e="T586" id="Seg_10589" s="T585">n-n:case</ta>
            <ta e="T587" id="Seg_10590" s="T586">ptcl</ta>
            <ta e="T588" id="Seg_10591" s="T587">v-v:tense.mood-v:pn</ta>
            <ta e="T589" id="Seg_10592" s="T588">n-n:obl.poss-n:case</ta>
            <ta e="T590" id="Seg_10593" s="T589">dem</ta>
            <ta e="T591" id="Seg_10594" s="T590">n-n:case</ta>
            <ta e="T592" id="Seg_10595" s="T591">adj-adj&gt;adv</ta>
            <ta e="T593" id="Seg_10596" s="T592">v-v:ins-v:pn</ta>
            <ta e="T594" id="Seg_10597" s="T593">ptcl</ta>
            <ta e="T595" id="Seg_10598" s="T594">adv</ta>
            <ta e="T596" id="Seg_10599" s="T595">num</ta>
            <ta e="T597" id="Seg_10600" s="T596">dem-n:case</ta>
            <ta e="T598" id="Seg_10601" s="T597">n-n&gt;adv</ta>
            <ta e="T599" id="Seg_10602" s="T598">n-n:case</ta>
            <ta e="T600" id="Seg_10603" s="T599">n-n:case</ta>
            <ta e="T601" id="Seg_10604" s="T600">ptcl</ta>
            <ta e="T602" id="Seg_10605" s="T601">preverb</ta>
            <ta e="T603" id="Seg_10606" s="T602">v-v:tense.mood-v:pn</ta>
            <ta e="T604" id="Seg_10607" s="T603">adv-%%</ta>
            <ta e="T605" id="Seg_10608" s="T604">nprop-n:case</ta>
            <ta e="T606" id="Seg_10609" s="T605">n-n&gt;adj</ta>
            <ta e="T967" id="Seg_10610" s="T606">ptcl</ta>
            <ta e="T607" id="Seg_10611" s="T967">preverb</ta>
            <ta e="T608" id="Seg_10612" s="T607">v-v:tense.mood-v:pn</ta>
            <ta e="T609" id="Seg_10613" s="T608">num</ta>
            <ta e="T610" id="Seg_10614" s="T609">n-n:num-n:case-n:poss</ta>
            <ta e="T611" id="Seg_10615" s="T610">ptcl</ta>
            <ta e="T612" id="Seg_10616" s="T611">n-n:case</ta>
            <ta e="T613" id="Seg_10617" s="T612">v-v&gt;ptcp</ta>
            <ta e="T614" id="Seg_10618" s="T613">nprop-n:case</ta>
            <ta e="T615" id="Seg_10619" s="T614">n-n:case</ta>
            <ta e="T616" id="Seg_10620" s="T615">quant</ta>
            <ta e="T617" id="Seg_10621" s="T616">v-v&gt;ptcp</ta>
            <ta e="T618" id="Seg_10622" s="T617">v-v:ins-v:pn</ta>
            <ta e="T619" id="Seg_10623" s="T618">quant</ta>
            <ta e="T620" id="Seg_10624" s="T619">n-n:case</ta>
            <ta e="T621" id="Seg_10625" s="T620">v-v:ins-v:pn</ta>
            <ta e="T622" id="Seg_10626" s="T621">adv</ta>
            <ta e="T623" id="Seg_10627" s="T622">adv</ta>
            <ta e="T624" id="Seg_10628" s="T623">%%-v:pn</ta>
            <ta e="T625" id="Seg_10629" s="T624">dem</ta>
            <ta e="T626" id="Seg_10630" s="T625">n-n:case</ta>
            <ta e="T627" id="Seg_10631" s="T626">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T628" id="Seg_10632" s="T627">adv</ta>
            <ta e="T629" id="Seg_10633" s="T628">n-n:case-n:poss</ta>
            <ta e="T630" id="Seg_10634" s="T629">v-v&gt;v-v:pn</ta>
            <ta e="T631" id="Seg_10635" s="T630">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T632" id="Seg_10636" s="T631">dem</ta>
            <ta e="T633" id="Seg_10637" s="T632">n-n:case-n:poss</ta>
            <ta e="T634" id="Seg_10638" s="T633">preverb</ta>
            <ta e="T635" id="Seg_10639" s="T634">v-v:pn</ta>
            <ta e="T636" id="Seg_10640" s="T635">n-n:case-n:poss</ta>
            <ta e="T638" id="Seg_10641" s="T637">ptcl</ta>
            <ta e="T639" id="Seg_10642" s="T638">ptcl</ta>
            <ta e="T640" id="Seg_10643" s="T639">v-v&gt;v-v:pn</ta>
            <ta e="T641" id="Seg_10644" s="T640">ptcl</ta>
            <ta e="T642" id="Seg_10645" s="T641">nprop-n:case</ta>
            <ta e="T643" id="Seg_10646" s="T642">n-n&gt;adj</ta>
            <ta e="T644" id="Seg_10647" s="T643">n-n:num-n:case</ta>
            <ta e="T645" id="Seg_10648" s="T644">interrog-n:case</ta>
            <ta e="T646" id="Seg_10649" s="T645">ptcl</ta>
            <ta e="T647" id="Seg_10650" s="T646">v-v:ins-v:pn</ta>
            <ta e="T648" id="Seg_10651" s="T647">n-n&gt;adj</ta>
            <ta e="T649" id="Seg_10652" s="T648">n-n:num-v:pn-n:ins-n:poss</ta>
            <ta e="T650" id="Seg_10653" s="T649">adv</ta>
            <ta e="T651" id="Seg_10654" s="T650">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T652" id="Seg_10655" s="T651">dem</ta>
            <ta e="T653" id="Seg_10656" s="T652">n-n&gt;adv</ta>
            <ta e="T654" id="Seg_10657" s="T653">pers-n:ins-n:case</ta>
            <ta e="T655" id="Seg_10658" s="T654">n-n&gt;adv</ta>
            <ta e="T656" id="Seg_10659" s="T655">v-v:ins-v:pn</ta>
            <ta e="T657" id="Seg_10660" s="T656">n-n:case</ta>
            <ta e="T658" id="Seg_10661" s="T657">v-v:ins-v:pn</ta>
            <ta e="T659" id="Seg_10662" s="T658">n-n:case</ta>
            <ta e="T660" id="Seg_10663" s="T659">n-n&gt;adv</ta>
            <ta e="T661" id="Seg_10664" s="T660">v-v&gt;v-v&gt;adv</ta>
            <ta e="T662" id="Seg_10665" s="T661">n-n:case</ta>
            <ta e="T663" id="Seg_10666" s="T662">%%</ta>
            <ta e="T664" id="Seg_10667" s="T663">n-n:case</ta>
            <ta e="T665" id="Seg_10668" s="T664">n-n&gt;adv</ta>
            <ta e="T666" id="Seg_10669" s="T665">%%</ta>
            <ta e="T667" id="Seg_10670" s="T666">n-n:case-n:poss</ta>
            <ta e="T668" id="Seg_10671" s="T667">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T669" id="Seg_10672" s="T668">n-n:case-n:poss</ta>
            <ta e="T670" id="Seg_10673" s="T669">ptcl</ta>
            <ta e="T671" id="Seg_10674" s="T670">v-v&gt;v-v:pn</ta>
            <ta e="T672" id="Seg_10675" s="T671">v-v&gt;ptcp</ta>
            <ta e="T673" id="Seg_10676" s="T672">n-n&gt;adj</ta>
            <ta e="T674" id="Seg_10677" s="T673">n-n:case-n:poss</ta>
            <ta e="T675" id="Seg_10678" s="T674">v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T676" id="Seg_10679" s="T675">adv</ta>
            <ta e="T677" id="Seg_10680" s="T676">v-v:ins-v:pn</ta>
            <ta e="T678" id="Seg_10681" s="T677">adv</ta>
            <ta e="T679" id="Seg_10682" s="T678">v-v&gt;v-v&gt;adv</ta>
            <ta e="T680" id="Seg_10683" s="T679">nprop-n:case</ta>
            <ta e="T681" id="Seg_10684" s="T680">n-n:case</ta>
            <ta e="T682" id="Seg_10685" s="T681">ptcl</ta>
            <ta e="T683" id="Seg_10686" s="T682">n-n:case-n:poss</ta>
            <ta e="T684" id="Seg_10687" s="T683">ptcl</ta>
            <ta e="T685" id="Seg_10688" s="T684">ptcl</ta>
            <ta e="T686" id="Seg_10689" s="T685">n-n&gt;adj</ta>
            <ta e="T687" id="Seg_10690" s="T686">ptcl</ta>
            <ta e="T688" id="Seg_10691" s="T687">n-n:case</ta>
            <ta e="T689" id="Seg_10692" s="T688">%%-v:pn</ta>
            <ta e="T690" id="Seg_10693" s="T689">ptcl</ta>
            <ta e="T691" id="Seg_10694" s="T690">adv</ta>
            <ta e="T692" id="Seg_10695" s="T691">v-v:tense-v:mood-v:pn</ta>
            <ta e="T693" id="Seg_10696" s="T692">ptcl</ta>
            <ta e="T694" id="Seg_10697" s="T693">n-n:case</ta>
            <ta e="T695" id="Seg_10698" s="T694">v-v&gt;v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T696" id="Seg_10699" s="T695">n-n&gt;adj</ta>
            <ta e="T697" id="Seg_10700" s="T696">n-n:case-n:poss</ta>
            <ta e="T698" id="Seg_10701" s="T697">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T699" id="Seg_10702" s="T698">nprop-n:case</ta>
            <ta e="T700" id="Seg_10703" s="T699">n-n&gt;adj</ta>
            <ta e="T701" id="Seg_10704" s="T700">n-n:num-n:ins-n:case</ta>
            <ta e="T702" id="Seg_10705" s="T701">quant</ta>
            <ta e="T703" id="Seg_10706" s="T702">adv</ta>
            <ta e="T704" id="Seg_10707" s="T703">v-v:tense-v:pn</ta>
            <ta e="T705" id="Seg_10708" s="T704">adj-n:case-n:poss</ta>
            <ta e="T706" id="Seg_10709" s="T705">v-v:tense-v:pn</ta>
            <ta e="T707" id="Seg_10710" s="T706">nprop-n:case</ta>
            <ta e="T708" id="Seg_10711" s="T707">n-n:case</ta>
            <ta e="T709" id="Seg_10712" s="T708">v-v&gt;adv</ta>
            <ta e="T710" id="Seg_10713" s="T709">v-v:tense-v:mood-v:pn</ta>
            <ta e="T711" id="Seg_10714" s="T710">num</ta>
            <ta e="T712" id="Seg_10715" s="T711">n-n:num-n:case</ta>
            <ta e="T713" id="Seg_10716" s="T712">v-v&gt;adv</ta>
            <ta e="T714" id="Seg_10717" s="T713">v-v:tense-v:mood-v:pn</ta>
            <ta e="T715" id="Seg_10718" s="T714">adv</ta>
            <ta e="T716" id="Seg_10719" s="T715">v-v&gt;v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T717" id="Seg_10720" s="T716">dem</ta>
            <ta e="T718" id="Seg_10721" s="T717">n-n:case</ta>
            <ta e="T719" id="Seg_10722" s="T718">num</ta>
            <ta e="T720" id="Seg_10723" s="T719">n-n:case</ta>
            <ta e="T721" id="Seg_10724" s="T720">n-n:case</ta>
            <ta e="T722" id="Seg_10725" s="T721">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T723" id="Seg_10726" s="T722">quant</ta>
            <ta e="T724" id="Seg_10727" s="T723">adv-adv:case</ta>
            <ta e="T725" id="Seg_10728" s="T724">n-n:case</ta>
            <ta e="T726" id="Seg_10729" s="T725">pp-adv:case</ta>
            <ta e="T727" id="Seg_10730" s="T726">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T728" id="Seg_10731" s="T727">dem</ta>
            <ta e="T729" id="Seg_10732" s="T728">v-v&gt;ptcp</ta>
            <ta e="T730" id="Seg_10733" s="T729">n-n:case</ta>
            <ta e="T731" id="Seg_10734" s="T730">n-n:case</ta>
            <ta e="T732" id="Seg_10735" s="T731">adv</ta>
            <ta e="T733" id="Seg_10736" s="T732">adv</ta>
            <ta e="T734" id="Seg_10737" s="T733">adv</ta>
            <ta e="T735" id="Seg_10738" s="T734">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T736" id="Seg_10739" s="T735">n-n:case</ta>
            <ta e="T737" id="Seg_10740" s="T736">adv</ta>
            <ta e="T738" id="Seg_10741" s="T737">dem</ta>
            <ta e="T739" id="Seg_10742" s="T738">n-n:case</ta>
            <ta e="T742" id="Seg_10743" s="T741">n-n:case</ta>
            <ta e="T743" id="Seg_10744" s="T742">ptcl</ta>
            <ta e="T744" id="Seg_10745" s="T743">v-v&gt;v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T745" id="Seg_10746" s="T744">nprop-n:case</ta>
            <ta e="T746" id="Seg_10747" s="T745">n-n:case</ta>
            <ta e="T747" id="Seg_10748" s="T746">ptcl</ta>
            <ta e="T748" id="Seg_10749" s="T747">adv</ta>
            <ta e="T749" id="Seg_10750" s="T748">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T750" id="Seg_10751" s="T749">interrog-n:case</ta>
            <ta e="T751" id="Seg_10752" s="T750">quant</ta>
            <ta e="T752" id="Seg_10753" s="T751">adv</ta>
            <ta e="T753" id="Seg_10754" s="T752">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T754" id="Seg_10755" s="T753">preverb</ta>
            <ta e="T755" id="Seg_10756" s="T754">v-v:pn</ta>
            <ta e="T756" id="Seg_10757" s="T755">dem</ta>
            <ta e="T757" id="Seg_10758" s="T756">n-n&gt;adj</ta>
            <ta e="T758" id="Seg_10759" s="T757">v-v:pn</ta>
            <ta e="T759" id="Seg_10760" s="T758">preverb</ta>
            <ta e="T760" id="Seg_10761" s="T759">v-v:ins-v&gt;v-v&gt;adv</ta>
            <ta e="T762" id="Seg_10762" s="T761">preverb</ta>
            <ta e="T763" id="Seg_10763" s="T762">v-v:pn</ta>
            <ta e="T764" id="Seg_10764" s="T763">dem</ta>
            <ta e="T765" id="Seg_10765" s="T764">n-n:num-n:case-n:poss</ta>
            <ta e="T766" id="Seg_10766" s="T765">n-n:case</ta>
            <ta e="T767" id="Seg_10767" s="T766">ptcl</ta>
            <ta e="T768" id="Seg_10768" s="T767">v-v&gt;v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T769" id="Seg_10769" s="T768">adv</ta>
            <ta e="T770" id="Seg_10770" s="T769">v-v&gt;v-%%</ta>
            <ta e="T771" id="Seg_10771" s="T770">conj</ta>
            <ta e="T772" id="Seg_10772" s="T771">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T773" id="Seg_10773" s="T772">dem-adj&gt;adv</ta>
            <ta e="T774" id="Seg_10774" s="T773">v-v:pn</ta>
            <ta e="T775" id="Seg_10775" s="T774">n-n&gt;adj</ta>
            <ta e="T776" id="Seg_10776" s="T775">n-n:case</ta>
            <ta e="T777" id="Seg_10777" s="T776">preverb</ta>
            <ta e="T778" id="Seg_10778" s="T777">v-v:pn</ta>
            <ta e="T779" id="Seg_10779" s="T778">nprop-n:case</ta>
            <ta e="T780" id="Seg_10780" s="T779">n-n:case</ta>
            <ta e="T781" id="Seg_10781" s="T780">dem</ta>
            <ta e="T782" id="Seg_10782" s="T781">ptcl</ta>
            <ta e="T783" id="Seg_10783" s="T782">n-n&gt;adj</ta>
            <ta e="T784" id="Seg_10784" s="T783">n-n:case-n:poss</ta>
            <ta e="T785" id="Seg_10785" s="T784">adj</ta>
            <ta e="T786" id="Seg_10786" s="T785">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T787" id="Seg_10787" s="T786">dem</ta>
            <ta e="T788" id="Seg_10788" s="T787">adv</ta>
            <ta e="T789" id="Seg_10789" s="T788">n-n&gt;n-n:case-n:poss</ta>
            <ta e="T791" id="Seg_10790" s="T790">dem</ta>
            <ta e="T792" id="Seg_10791" s="T791">n-n:num-n:case</ta>
            <ta e="T794" id="Seg_10792" s="T793">adv</ta>
            <ta e="T795" id="Seg_10793" s="T794">adj-adj&gt;adj</ta>
            <ta e="T796" id="Seg_10794" s="T795">v-v:tense-v:pn</ta>
            <ta e="T797" id="Seg_10795" s="T796">dem-n&gt;adj</ta>
            <ta e="T798" id="Seg_10796" s="T797">v-v:ins-v:pn</ta>
            <ta e="T799" id="Seg_10797" s="T798">dem</ta>
            <ta e="T800" id="Seg_10798" s="T799">n-n:case</ta>
            <ta e="T801" id="Seg_10799" s="T800">ptcl</ta>
            <ta e="T802" id="Seg_10800" s="T801">dem</ta>
            <ta e="T803" id="Seg_10801" s="T802">n-n:num-n:ins-n:case-n:obl.poss</ta>
            <ta e="T804" id="Seg_10802" s="T803">n-n:num-n:case-n:poss</ta>
            <ta e="T805" id="Seg_10803" s="T804">adv</ta>
            <ta e="T806" id="Seg_10804" s="T805">v-v&gt;v-v:mood.pn</ta>
            <ta e="T807" id="Seg_10805" s="T806">ptcl</ta>
            <ta e="T808" id="Seg_10806" s="T807">n-n:case.poss</ta>
            <ta e="T809" id="Seg_10807" s="T808">v-v&gt;v-v&gt;adv</ta>
            <ta e="T810" id="Seg_10808" s="T809">ptcl</ta>
            <ta e="T811" id="Seg_10809" s="T810">v-v&gt;v-v&gt;adv</ta>
            <ta e="T812" id="Seg_10810" s="T811">emphpro</ta>
            <ta e="T813" id="Seg_10811" s="T812">n-n:case-n:obl.poss</ta>
            <ta e="T814" id="Seg_10812" s="T813">adv</ta>
            <ta e="T815" id="Seg_10813" s="T814">adv</ta>
            <ta e="T816" id="Seg_10814" s="T815">adv</ta>
            <ta e="T817" id="Seg_10815" s="T816">adv</ta>
            <ta e="T818" id="Seg_10816" s="T817">adv</ta>
            <ta e="T821" id="Seg_10817" s="T820">ptcl</ta>
            <ta e="T822" id="Seg_10818" s="T821">n-n:case</ta>
            <ta e="T823" id="Seg_10819" s="T822">v-v&gt;v-v&gt;v-v:inf</ta>
            <ta e="T824" id="Seg_10820" s="T823">conj</ta>
            <ta e="T825" id="Seg_10821" s="T824">conj</ta>
            <ta e="T826" id="Seg_10822" s="T825">v-v:ins-v&gt;v-v:inf</ta>
            <ta e="T827" id="Seg_10823" s="T826">conj</ta>
            <ta e="T828" id="Seg_10824" s="T827">conj</ta>
            <ta e="T829" id="Seg_10825" s="T828">ptcl</ta>
            <ta e="T830" id="Seg_10826" s="T829">v-v:ins-v&gt;v-v:inf</ta>
            <ta e="T831" id="Seg_10827" s="T830">adv</ta>
            <ta e="T832" id="Seg_10828" s="T831">dem</ta>
            <ta e="T833" id="Seg_10829" s="T832">n-n:case</ta>
            <ta e="T834" id="Seg_10830" s="T833">dem</ta>
            <ta e="T835" id="Seg_10831" s="T834">v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T836" id="Seg_10832" s="T835">n-n:case-n:poss</ta>
            <ta e="T837" id="Seg_10833" s="T836">dem</ta>
            <ta e="T838" id="Seg_10834" s="T837">adv</ta>
            <ta e="T839" id="Seg_10835" s="T838">adv</ta>
            <ta e="T840" id="Seg_10836" s="T839">v-v&gt;adv</ta>
            <ta e="T841" id="Seg_10837" s="T840">n-n:num-n:case-n:poss</ta>
            <ta e="T842" id="Seg_10838" s="T841">dem</ta>
            <ta e="T843" id="Seg_10839" s="T842">v-v&gt;ptcp</ta>
            <ta e="T844" id="Seg_10840" s="T843">n-n:num-n:case-n:poss</ta>
            <ta e="T845" id="Seg_10841" s="T844">adv</ta>
            <ta e="T846" id="Seg_10842" s="T845">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T847" id="Seg_10843" s="T846">adv</ta>
            <ta e="T848" id="Seg_10844" s="T847">adv</ta>
            <ta e="T849" id="Seg_10845" s="T848">adv</ta>
            <ta e="T850" id="Seg_10846" s="T849">nprop-n:case</ta>
            <ta e="T851" id="Seg_10847" s="T850">n-n:case</ta>
            <ta e="T852" id="Seg_10848" s="T851">%%</ta>
            <ta e="T853" id="Seg_10849" s="T852">v-v&gt;adv</ta>
            <ta e="T854" id="Seg_10850" s="T853">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T855" id="Seg_10851" s="T854">ptcl</ta>
            <ta e="T856" id="Seg_10852" s="T855">adv</ta>
            <ta e="T857" id="Seg_10853" s="T856">n-n:case-n:poss</ta>
            <ta e="T858" id="Seg_10854" s="T857">ptcl</ta>
            <ta e="T859" id="Seg_10855" s="T858">%%</ta>
            <ta e="T860" id="Seg_10856" s="T859">v-v:ins-v:pn</ta>
            <ta e="T861" id="Seg_10857" s="T860">dem</ta>
            <ta e="T862" id="Seg_10858" s="T861">adv</ta>
            <ta e="T863" id="Seg_10859" s="T862">adj-adj&gt;adv</ta>
            <ta e="T864" id="Seg_10860" s="T863">v-v:ins-v:pn</ta>
            <ta e="T865" id="Seg_10861" s="T864">adv</ta>
            <ta e="T866" id="Seg_10862" s="T865">n-n:obl.poss-n:case</ta>
            <ta e="T867" id="Seg_10863" s="T866">adv</ta>
            <ta e="T868" id="Seg_10864" s="T867">v-v:ins-v:pn</ta>
            <ta e="T869" id="Seg_10865" s="T868">n-n:case-n:poss</ta>
            <ta e="T870" id="Seg_10866" s="T869">preverb</ta>
            <ta e="T871" id="Seg_10867" s="T870">v-v:mood.pn</ta>
            <ta e="T872" id="Seg_10868" s="T871">pers</ta>
            <ta e="T873" id="Seg_10869" s="T872">adv</ta>
            <ta e="T874" id="Seg_10870" s="T873">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T875" id="Seg_10871" s="T874">n-n:ins-n:num-n:ins-n:case</ta>
            <ta e="T876" id="Seg_10872" s="T875">ptcl</ta>
            <ta e="T877" id="Seg_10873" s="T876">v-v:tense-v:pn</ta>
            <ta e="T878" id="Seg_10874" s="T877">ptcl</ta>
            <ta e="T879" id="Seg_10875" s="T878">emphpro-n:case</ta>
            <ta e="T880" id="Seg_10876" s="T879">dem</ta>
            <ta e="T881" id="Seg_10877" s="T880">n-n:obl.poss-n&gt;adj</ta>
            <ta e="T882" id="Seg_10878" s="T881">n-n:num-n:case</ta>
            <ta e="T883" id="Seg_10879" s="T882">clit</ta>
            <ta e="T884" id="Seg_10880" s="T883">interrog-n:case</ta>
            <ta e="T885" id="Seg_10881" s="T884">adv</ta>
            <ta e="T886" id="Seg_10882" s="T885">v-v:ins-v:pn</ta>
            <ta e="T887" id="Seg_10883" s="T886">dem</ta>
            <ta e="T888" id="Seg_10884" s="T887">emphpro</ta>
            <ta e="T889" id="Seg_10885" s="T888">ptcl</ta>
            <ta e="T890" id="Seg_10886" s="T889">v-v&gt;v-v:pn</ta>
            <ta e="T892" id="Seg_10887" s="T891">dem</ta>
            <ta e="T893" id="Seg_10888" s="T892">n-n:case-n:poss</ta>
            <ta e="T894" id="Seg_10889" s="T893">ptcl</ta>
            <ta e="T895" id="Seg_10890" s="T894">v-v:tense-v:pn</ta>
            <ta e="T896" id="Seg_10891" s="T895">n-n:case-n:poss</ta>
            <ta e="T897" id="Seg_10892" s="T896">n-n:case-n:obl.poss</ta>
            <ta e="T898" id="Seg_10893" s="T897">n-n:num-n:case</ta>
            <ta e="T899" id="Seg_10894" s="T898">nprop-n:case</ta>
            <ta e="T900" id="Seg_10895" s="T899">n-n&gt;adj</ta>
            <ta e="T901" id="Seg_10896" s="T900">n-n:num-n:case</ta>
            <ta e="T902" id="Seg_10897" s="T901">dem</ta>
            <ta e="T903" id="Seg_10898" s="T902">adv</ta>
            <ta e="T904" id="Seg_10899" s="T903">v-v:tense-v:pn</ta>
            <ta e="T905" id="Seg_10900" s="T904">ptcl</ta>
            <ta e="T906" id="Seg_10901" s="T905">%%-v:pn</ta>
            <ta e="T907" id="Seg_10902" s="T906">adv</ta>
            <ta e="T908" id="Seg_10903" s="T907">adv</ta>
            <ta e="T909" id="Seg_10904" s="T908">%%</ta>
            <ta e="T910" id="Seg_10905" s="T909">ptcl</ta>
            <ta e="T912" id="Seg_10906" s="T911">ptcl</ta>
            <ta e="T985" id="Seg_10907" s="T912">v-v:pn</ta>
            <ta e="T914" id="Seg_10908" s="T913">adv</ta>
            <ta e="T986" id="Seg_10909" s="T914">v-v&gt;v-v:mood.pn</ta>
            <ta e="T916" id="Seg_10910" s="T915">adv</ta>
            <ta e="T987" id="Seg_10911" s="T916">n-n:case.poss</ta>
            <ta e="T918" id="Seg_10912" s="T917">adv</ta>
            <ta e="T919" id="Seg_10913" s="T918">adv</ta>
            <ta e="T920" id="Seg_10914" s="T919">v</ta>
            <ta e="T921" id="Seg_10915" s="T920">pers-n:ins-n&gt;n-n:num-n:case</ta>
            <ta e="T922" id="Seg_10916" s="T921">adv</ta>
            <ta e="T923" id="Seg_10917" s="T922">n-n:case-n:obl.poss</ta>
            <ta e="T924" id="Seg_10918" s="T923">n-n:case</ta>
            <ta e="T925" id="Seg_10919" s="T924">n-n:case</ta>
            <ta e="T926" id="Seg_10920" s="T925">v-v:ins-v:pn</ta>
            <ta e="T927" id="Seg_10921" s="T926">n-n:num-n:case</ta>
            <ta e="T928" id="Seg_10922" s="T927">dem</ta>
            <ta e="T929" id="Seg_10923" s="T928">adv</ta>
            <ta e="T930" id="Seg_10924" s="T929">v-v&gt;adv</ta>
            <ta e="T931" id="Seg_10925" s="T930">adv</ta>
            <ta e="T932" id="Seg_10926" s="T931">v-v:ins-v:pn</ta>
            <ta e="T933" id="Seg_10927" s="T932">dem</ta>
            <ta e="T934" id="Seg_10928" s="T933">v-v:tense-v:mood-v:pn</ta>
            <ta e="T935" id="Seg_10929" s="T934">adv</ta>
            <ta e="T936" id="Seg_10930" s="T935">v-v:tense-v:mood-v:pn</ta>
            <ta e="T937" id="Seg_10931" s="T936">nprop-n:case</ta>
            <ta e="T938" id="Seg_10932" s="T937">n-n:case</ta>
            <ta e="T939" id="Seg_10933" s="T938">adv</ta>
            <ta e="T940" id="Seg_10934" s="T939">adv-adv:case</ta>
            <ta e="T941" id="Seg_10935" s="T940">n-n:case</ta>
            <ta e="T942" id="Seg_10936" s="T941">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T943" id="Seg_10937" s="T942">preverb</ta>
            <ta e="T944" id="Seg_10938" s="T943">v-v:tense-v:pn</ta>
            <ta e="T945" id="Seg_10939" s="T944">dem</ta>
            <ta e="T946" id="Seg_10940" s="T945">num</ta>
            <ta e="T947" id="Seg_10941" s="T946">n-n:ins-n:num-n:case</ta>
            <ta e="T948" id="Seg_10942" s="T947">adv</ta>
            <ta e="T949" id="Seg_10943" s="T948">v-v&gt;v-v:pn</ta>
            <ta e="T950" id="Seg_10944" s="T949">nprop-n:case</ta>
            <ta e="T951" id="Seg_10945" s="T950">n-n:case</ta>
            <ta e="T952" id="Seg_10946" s="T951">nprop-n:case</ta>
            <ta e="T953" id="Seg_10947" s="T952">n-n&gt;adj</ta>
            <ta e="T954" id="Seg_10948" s="T953">n-n:num-n:case</ta>
            <ta e="T955" id="Seg_10949" s="T954">adv</ta>
            <ta e="T988" id="Seg_10950" s="T955">v-v:ins-v:pn</ta>
            <ta e="T957" id="Seg_10951" s="T956">adj</ta>
            <ta e="T958" id="Seg_10952" s="T957">n-n:case</ta>
            <ta e="T959" id="Seg_10953" s="T958">v-v:mood.pn</ta>
            <ta e="T960" id="Seg_10954" s="T959">adj</ta>
            <ta e="T989" id="Seg_10955" s="T960">adj</ta>
            <ta e="T962" id="Seg_10956" s="T961">adj</ta>
            <ta e="T963" id="Seg_10957" s="T962">n-n:case</ta>
            <ta e="T964" id="Seg_10958" s="T963">v-v:mood.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T3" id="Seg_10959" s="T2">num</ta>
            <ta e="T4" id="Seg_10960" s="T3">n</ta>
            <ta e="T5" id="Seg_10961" s="T4">v</ta>
            <ta e="T7" id="Seg_10962" s="T6">dem</ta>
            <ta e="T8" id="Seg_10963" s="T7">clit</ta>
            <ta e="T9" id="Seg_10964" s="T8">adv</ta>
            <ta e="T11" id="Seg_10965" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_10966" s="T11">v</ta>
            <ta e="T13" id="Seg_10967" s="T12">pro</ta>
            <ta e="T14" id="Seg_10968" s="T13">dem</ta>
            <ta e="T15" id="Seg_10969" s="T14">n</ta>
            <ta e="T16" id="Seg_10970" s="T15">dem</ta>
            <ta e="T17" id="Seg_10971" s="T16">n</ta>
            <ta e="T18" id="Seg_10972" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_10973" s="T18">n</ta>
            <ta e="T21" id="Seg_10974" s="T20">pers</ta>
            <ta e="T22" id="Seg_10975" s="T21">pp</ta>
            <ta e="T24" id="Seg_10976" s="T23">n</ta>
            <ta e="T25" id="Seg_10977" s="T24">adv</ta>
            <ta e="T27" id="Seg_10978" s="T26">dem</ta>
            <ta e="T28" id="Seg_10979" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_10980" s="T28">pers</ta>
            <ta e="T30" id="Seg_10981" s="T29">v</ta>
            <ta e="T32" id="Seg_10982" s="T31">pers</ta>
            <ta e="T33" id="Seg_10983" s="T32">v</ta>
            <ta e="T971" id="Seg_10984" s="T33">v</ta>
            <ta e="T972" id="Seg_10985" s="T34">v</ta>
            <ta e="T36" id="Seg_10986" s="T35">adv</ta>
            <ta e="T973" id="Seg_10987" s="T36">v</ta>
            <ta e="T39" id="Seg_10988" s="T38">dem</ta>
            <ta e="T40" id="Seg_10989" s="T39">n</ta>
            <ta e="T41" id="Seg_10990" s="T40">v</ta>
            <ta e="T42" id="Seg_10991" s="T41">v</ta>
            <ta e="T43" id="Seg_10992" s="T42">pro</ta>
            <ta e="T44" id="Seg_10993" s="T43">adj</ta>
            <ta e="T45" id="Seg_10994" s="T44">n</ta>
            <ta e="T46" id="Seg_10995" s="T45">adj</ta>
            <ta e="T47" id="Seg_10996" s="T46">adj</ta>
            <ta e="T48" id="Seg_10997" s="T47">n</ta>
            <ta e="T49" id="Seg_10998" s="T48">adj</ta>
            <ta e="T50" id="Seg_10999" s="T49">adj</ta>
            <ta e="T51" id="Seg_11000" s="T50">n</ta>
            <ta e="T52" id="Seg_11001" s="T51">adj</ta>
            <ta e="T53" id="Seg_11002" s="T52">v</ta>
            <ta e="T54" id="Seg_11003" s="T53">v</ta>
            <ta e="T55" id="Seg_11004" s="T54">pers</ta>
            <ta e="T56" id="Seg_11005" s="T55">n</ta>
            <ta e="T57" id="Seg_11006" s="T56">n</ta>
            <ta e="T992" id="Seg_11007" s="T58">interrog</ta>
            <ta e="T60" id="Seg_11008" s="T59">v</ta>
            <ta e="T61" id="Seg_11009" s="T60">dem</ta>
            <ta e="T62" id="Seg_11010" s="T61">v</ta>
            <ta e="T63" id="Seg_11011" s="T62">n</ta>
            <ta e="T64" id="Seg_11012" s="T63">num</ta>
            <ta e="T65" id="Seg_11013" s="T64">n</ta>
            <ta e="T66" id="Seg_11014" s="T65">adj</ta>
            <ta e="T67" id="Seg_11015" s="T66">adj</ta>
            <ta e="T68" id="Seg_11016" s="T67">n</ta>
            <ta e="T69" id="Seg_11017" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_11018" s="T69">n</ta>
            <ta e="T71" id="Seg_11019" s="T70">v</ta>
            <ta e="T72" id="Seg_11020" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_11021" s="T72">interrog</ta>
            <ta e="T74" id="Seg_11022" s="T73">v</ta>
            <ta e="T75" id="Seg_11023" s="T74">adv</ta>
            <ta e="T76" id="Seg_11024" s="T75">dem</ta>
            <ta e="T77" id="Seg_11025" s="T76">n</ta>
            <ta e="T78" id="Seg_11026" s="T77">v</ta>
            <ta e="T79" id="Seg_11027" s="T78">dem</ta>
            <ta e="T80" id="Seg_11028" s="T79">n</ta>
            <ta e="T82" id="Seg_11029" s="T81">dem</ta>
            <ta e="T83" id="Seg_11030" s="T82">n</ta>
            <ta e="T84" id="Seg_11031" s="T83">adv</ta>
            <ta e="T85" id="Seg_11032" s="T84">v</ta>
            <ta e="T86" id="Seg_11033" s="T85">ptcl</ta>
            <ta e="T87" id="Seg_11034" s="T86">dem</ta>
            <ta e="T88" id="Seg_11035" s="T87">n</ta>
            <ta e="T89" id="Seg_11036" s="T88">n</ta>
            <ta e="T90" id="Seg_11037" s="T89">n</ta>
            <ta e="T91" id="Seg_11038" s="T90">adv</ta>
            <ta e="T92" id="Seg_11039" s="T91">dem</ta>
            <ta e="T93" id="Seg_11040" s="T92">num</ta>
            <ta e="T94" id="Seg_11041" s="T93">n</ta>
            <ta e="T95" id="Seg_11042" s="T94">n</ta>
            <ta e="T96" id="Seg_11043" s="T95">v</ta>
            <ta e="T97" id="Seg_11044" s="T96">n</ta>
            <ta e="T98" id="Seg_11045" s="T97">n</ta>
            <ta e="T99" id="Seg_11046" s="T98">interrog</ta>
            <ta e="T100" id="Seg_11047" s="T99">v</ta>
            <ta e="T102" id="Seg_11048" s="T101">ptcl</ta>
            <ta e="T103" id="Seg_11049" s="T102">v</ta>
            <ta e="T104" id="Seg_11050" s="T103">interrog</ta>
            <ta e="T106" id="Seg_11051" s="T105">dem</ta>
            <ta e="T107" id="Seg_11052" s="T106">dem</ta>
            <ta e="T108" id="Seg_11053" s="T107">n</ta>
            <ta e="T109" id="Seg_11054" s="T108">preverb</ta>
            <ta e="T110" id="Seg_11055" s="T109">v</ta>
            <ta e="T111" id="Seg_11056" s="T110">adv</ta>
            <ta e="T112" id="Seg_11057" s="T111">v</ta>
            <ta e="T113" id="Seg_11058" s="T112">dem</ta>
            <ta e="T114" id="Seg_11059" s="T113">adv</ta>
            <ta e="T115" id="Seg_11060" s="T114">adv</ta>
            <ta e="T116" id="Seg_11061" s="T115">dem</ta>
            <ta e="T117" id="Seg_11062" s="T116">n</ta>
            <ta e="T118" id="Seg_11063" s="T117">n</ta>
            <ta e="T119" id="Seg_11064" s="T118">n</ta>
            <ta e="T120" id="Seg_11065" s="T119">adv</ta>
            <ta e="T121" id="Seg_11066" s="T120">interrog</ta>
            <ta e="T122" id="Seg_11067" s="T121">qv</ta>
            <ta e="T123" id="Seg_11068" s="T122">adv</ta>
            <ta e="T124" id="Seg_11069" s="T123">n</ta>
            <ta e="T125" id="Seg_11070" s="T124">v</ta>
            <ta e="T126" id="Seg_11071" s="T125">n</ta>
            <ta e="T127" id="Seg_11072" s="T126">v</ta>
            <ta e="T128" id="Seg_11073" s="T127">adv</ta>
            <ta e="T129" id="Seg_11074" s="T128">v</ta>
            <ta e="T130" id="Seg_11075" s="T129">adv</ta>
            <ta e="T131" id="Seg_11076" s="T130">n</ta>
            <ta e="T132" id="Seg_11077" s="T131">dem</ta>
            <ta e="T133" id="Seg_11078" s="T132">n</ta>
            <ta e="T134" id="Seg_11079" s="T133">v</ta>
            <ta e="T135" id="Seg_11080" s="T134">n</ta>
            <ta e="T136" id="Seg_11081" s="T135">ptcl</ta>
            <ta e="T137" id="Seg_11082" s="T136">preverb</ta>
            <ta e="T138" id="Seg_11083" s="T137">v</ta>
            <ta e="T139" id="Seg_11084" s="T138">adj</ta>
            <ta e="T140" id="Seg_11085" s="T139">n</ta>
            <ta e="T141" id="Seg_11086" s="T140">dem</ta>
            <ta e="T142" id="Seg_11087" s="T141">n</ta>
            <ta e="T143" id="Seg_11088" s="T142">adj</ta>
            <ta e="T144" id="Seg_11089" s="T143">adj</ta>
            <ta e="T145" id="Seg_11090" s="T144">v</ta>
            <ta e="T146" id="Seg_11091" s="T145">interrog</ta>
            <ta e="T147" id="Seg_11092" s="T146">qv</ta>
            <ta e="T148" id="Seg_11093" s="T147">dem</ta>
            <ta e="T149" id="Seg_11094" s="T148">n</ta>
            <ta e="T150" id="Seg_11095" s="T149">n</ta>
            <ta e="T151" id="Seg_11096" s="T150">adv</ta>
            <ta e="T152" id="Seg_11097" s="T151">adv</ta>
            <ta e="T153" id="Seg_11098" s="T152">dem</ta>
            <ta e="T154" id="Seg_11099" s="T153">v</ta>
            <ta e="T155" id="Seg_11100" s="T154">dem</ta>
            <ta e="T156" id="Seg_11101" s="T155">adj</ta>
            <ta e="T157" id="Seg_11102" s="T156">n</ta>
            <ta e="T158" id="Seg_11103" s="T157">adj</ta>
            <ta e="T159" id="Seg_11104" s="T158">n</ta>
            <ta e="T167" id="Seg_11105" s="T166">adj</ta>
            <ta e="T168" id="Seg_11106" s="T167">adj</ta>
            <ta e="T169" id="Seg_11107" s="T168">n</ta>
            <ta e="T170" id="Seg_11108" s="T169">pp</ta>
            <ta e="T171" id="Seg_11109" s="T170">adj</ta>
            <ta e="T172" id="Seg_11110" s="T171">adj</ta>
            <ta e="T173" id="Seg_11111" s="T172">v</ta>
            <ta e="T174" id="Seg_11112" s="T173">adv</ta>
            <ta e="T175" id="Seg_11113" s="T174">v</ta>
            <ta e="T176" id="Seg_11114" s="T175">adj</ta>
            <ta e="T177" id="Seg_11115" s="T176">v</ta>
            <ta e="T178" id="Seg_11116" s="T177">adj</ta>
            <ta e="T179" id="Seg_11117" s="T178">n</ta>
            <ta e="T180" id="Seg_11118" s="T179">v</ta>
            <ta e="T181" id="Seg_11119" s="T180">dem</ta>
            <ta e="T182" id="Seg_11120" s="T181">n</ta>
            <ta e="T183" id="Seg_11121" s="T182">adv</ta>
            <ta e="T184" id="Seg_11122" s="T183">v</ta>
            <ta e="T185" id="Seg_11123" s="T184">v</ta>
            <ta e="T186" id="Seg_11124" s="T185">adv</ta>
            <ta e="T187" id="Seg_11125" s="T186">adv</ta>
            <ta e="T188" id="Seg_11126" s="T187">v</ta>
            <ta e="T189" id="Seg_11127" s="T188">dem</ta>
            <ta e="T190" id="Seg_11128" s="T189">adv</ta>
            <ta e="T191" id="Seg_11129" s="T190">dem</ta>
            <ta e="T192" id="Seg_11130" s="T191">n</ta>
            <ta e="T193" id="Seg_11131" s="T192">n</ta>
            <ta e="T194" id="Seg_11132" s="T193">n</ta>
            <ta e="T195" id="Seg_11133" s="T194">n</ta>
            <ta e="T196" id="Seg_11134" s="T195">n</ta>
            <ta e="T197" id="Seg_11135" s="T196">n</ta>
            <ta e="T198" id="Seg_11136" s="T197">n</ta>
            <ta e="T199" id="Seg_11137" s="T198">v</ta>
            <ta e="T200" id="Seg_11138" s="T199">adj</ta>
            <ta e="T201" id="Seg_11139" s="T200">n</ta>
            <ta e="T202" id="Seg_11140" s="T201">dem</ta>
            <ta e="T203" id="Seg_11141" s="T202">adj</ta>
            <ta e="T204" id="Seg_11142" s="T203">n</ta>
            <ta e="T206" id="Seg_11143" s="T205">num</ta>
            <ta e="T207" id="Seg_11144" s="T206">n</ta>
            <ta e="T208" id="Seg_11145" s="T207">adj</ta>
            <ta e="T209" id="Seg_11146" s="T208">adv</ta>
            <ta e="T210" id="Seg_11147" s="T209">adv</ta>
            <ta e="T211" id="Seg_11148" s="T210">num</ta>
            <ta e="T212" id="Seg_11149" s="T211">n</ta>
            <ta e="T213" id="Seg_11150" s="T212">pers</ta>
            <ta e="T214" id="Seg_11151" s="T213">n</ta>
            <ta e="T215" id="Seg_11152" s="T214">n</ta>
            <ta e="T216" id="Seg_11153" s="T215">nprop</ta>
            <ta e="T217" id="Seg_11154" s="T216">n</ta>
            <ta e="T218" id="Seg_11155" s="T217">nprop</ta>
            <ta e="T219" id="Seg_11156" s="T218">n</ta>
            <ta e="T220" id="Seg_11157" s="T219">v</ta>
            <ta e="T221" id="Seg_11158" s="T220">interrog</ta>
            <ta e="T222" id="Seg_11159" s="T221">adj</ta>
            <ta e="T223" id="Seg_11160" s="T222">n</ta>
            <ta e="T224" id="Seg_11161" s="T223">n</ta>
            <ta e="T225" id="Seg_11162" s="T224">quant</ta>
            <ta e="T226" id="Seg_11163" s="T225">v</ta>
            <ta e="T227" id="Seg_11164" s="T226">v</ta>
            <ta e="T228" id="Seg_11165" s="T227">adv</ta>
            <ta e="T229" id="Seg_11166" s="T228">v</ta>
            <ta e="T230" id="Seg_11167" s="T229">dem</ta>
            <ta e="T231" id="Seg_11168" s="T230">n</ta>
            <ta e="T232" id="Seg_11169" s="T231">dem</ta>
            <ta e="T233" id="Seg_11170" s="T232">n</ta>
            <ta e="T234" id="Seg_11171" s="T233">v</ta>
            <ta e="T235" id="Seg_11172" s="T234">dem</ta>
            <ta e="T236" id="Seg_11173" s="T235">adj</ta>
            <ta e="T237" id="Seg_11174" s="T236">n</ta>
            <ta e="T238" id="Seg_11175" s="T237">num</ta>
            <ta e="T239" id="Seg_11176" s="T238">num</ta>
            <ta e="T240" id="Seg_11177" s="T239">n</ta>
            <ta e="T241" id="Seg_11178" s="T240">n</ta>
            <ta e="T242" id="Seg_11179" s="T241">dem</ta>
            <ta e="T243" id="Seg_11180" s="T242">n</ta>
            <ta e="T244" id="Seg_11181" s="T243">n</ta>
            <ta e="T245" id="Seg_11182" s="T244">v</ta>
            <ta e="T246" id="Seg_11183" s="T245">n</ta>
            <ta e="T247" id="Seg_11184" s="T246">v</ta>
            <ta e="T248" id="Seg_11185" s="T247">nprop</ta>
            <ta e="T249" id="Seg_11186" s="T248">n</ta>
            <ta e="T250" id="Seg_11187" s="T249">n</ta>
            <ta e="T251" id="Seg_11188" s="T250">v</ta>
            <ta e="T252" id="Seg_11189" s="T251">adv</ta>
            <ta e="T253" id="Seg_11190" s="T252">v</ta>
            <ta e="T254" id="Seg_11191" s="T253">adv</ta>
            <ta e="T255" id="Seg_11192" s="T254">v</ta>
            <ta e="T256" id="Seg_11193" s="T255">dem</ta>
            <ta e="T257" id="Seg_11194" s="T256">pers</ta>
            <ta e="T258" id="Seg_11195" s="T257">n</ta>
            <ta e="T259" id="Seg_11196" s="T258">dem</ta>
            <ta e="T260" id="Seg_11197" s="T259">n</ta>
            <ta e="T261" id="Seg_11198" s="T260">ptcl</ta>
            <ta e="T262" id="Seg_11199" s="T261">ptcl</ta>
            <ta e="T263" id="Seg_11200" s="T262">v</ta>
            <ta e="T265" id="Seg_11201" s="T264">conj</ta>
            <ta e="T266" id="Seg_11202" s="T265">v</ta>
            <ta e="T267" id="Seg_11203" s="T266">pers</ta>
            <ta e="T268" id="Seg_11204" s="T267">interrog</ta>
            <ta e="T270" id="Seg_11205" s="T269">adv</ta>
            <ta e="T272" id="Seg_11206" s="T271">conj</ta>
            <ta e="T273" id="Seg_11207" s="T272">conj</ta>
            <ta e="T274" id="Seg_11208" s="T273">v</ta>
            <ta e="T275" id="Seg_11209" s="T274">adv</ta>
            <ta e="T276" id="Seg_11210" s="T275">conj</ta>
            <ta e="T277" id="Seg_11211" s="T276">v</ta>
            <ta e="T278" id="Seg_11212" s="T277">adv</ta>
            <ta e="T279" id="Seg_11213" s="T278">ptcp</ta>
            <ta e="T280" id="Seg_11214" s="T279">v</ta>
            <ta e="T281" id="Seg_11215" s="T280">ptcl</ta>
            <ta e="T282" id="Seg_11216" s="T281">interrog</ta>
            <ta e="T283" id="Seg_11217" s="T282">ptcl</ta>
            <ta e="T284" id="Seg_11218" s="T283">adj</ta>
            <ta e="T285" id="Seg_11219" s="T284">n</ta>
            <ta e="T287" id="Seg_11220" s="T286">ptcl</ta>
            <ta e="T288" id="Seg_11221" s="T287">ptcl</ta>
            <ta e="T289" id="Seg_11222" s="T288">v</ta>
            <ta e="T290" id="Seg_11223" s="T289">ptcl</ta>
            <ta e="T291" id="Seg_11224" s="T290">nprop</ta>
            <ta e="T292" id="Seg_11225" s="T291">n</ta>
            <ta e="T294" id="Seg_11226" s="T293">num</ta>
            <ta e="T295" id="Seg_11227" s="T294">n</ta>
            <ta e="T296" id="Seg_11228" s="T295">v</ta>
            <ta e="T297" id="Seg_11229" s="T296">ptcl</ta>
            <ta e="T298" id="Seg_11230" s="T297">dem</ta>
            <ta e="T299" id="Seg_11231" s="T298">n</ta>
            <ta e="T300" id="Seg_11232" s="T299">v</ta>
            <ta e="T301" id="Seg_11233" s="T300">dem</ta>
            <ta e="T302" id="Seg_11234" s="T301">n</ta>
            <ta e="T303" id="Seg_11235" s="T302">adv</ta>
            <ta e="T304" id="Seg_11236" s="T303">dem</ta>
            <ta e="T305" id="Seg_11237" s="T304">n</ta>
            <ta e="T306" id="Seg_11238" s="T305">dem</ta>
            <ta e="T307" id="Seg_11239" s="T306">n</ta>
            <ta e="T308" id="Seg_11240" s="T307">v</ta>
            <ta e="T309" id="Seg_11241" s="T308">interrog</ta>
            <ta e="T310" id="Seg_11242" s="T309">adj</ta>
            <ta e="T311" id="Seg_11243" s="T310">n</ta>
            <ta e="T312" id="Seg_11244" s="T311">dem</ta>
            <ta e="T313" id="Seg_11245" s="T312">adj</ta>
            <ta e="T314" id="Seg_11246" s="T313">n</ta>
            <ta e="T315" id="Seg_11247" s="T314">interrog</ta>
            <ta e="T316" id="Seg_11248" s="T315">adj</ta>
            <ta e="T317" id="Seg_11249" s="T316">interrog</ta>
            <ta e="T318" id="Seg_11250" s="T317">quant</ta>
            <ta e="T319" id="Seg_11251" s="T318">v</ta>
            <ta e="T320" id="Seg_11252" s="T319">adv</ta>
            <ta e="T321" id="Seg_11253" s="T320">adv</ta>
            <ta e="T322" id="Seg_11254" s="T321">adj</ta>
            <ta e="T323" id="Seg_11255" s="T322">n</ta>
            <ta e="T324" id="Seg_11256" s="T323">n</ta>
            <ta e="T325" id="Seg_11257" s="T324">adv</ta>
            <ta e="T326" id="Seg_11258" s="T325">adv</ta>
            <ta e="T327" id="Seg_11259" s="T326">quant</ta>
            <ta e="T328" id="Seg_11260" s="T327">v</ta>
            <ta e="T329" id="Seg_11261" s="T328">adv</ta>
            <ta e="T330" id="Seg_11262" s="T329">adv</ta>
            <ta e="T332" id="Seg_11263" s="T331">adv</ta>
            <ta e="T333" id="Seg_11264" s="T332">v</ta>
            <ta e="T334" id="Seg_11265" s="T333">dem</ta>
            <ta e="T335" id="Seg_11266" s="T334">n</ta>
            <ta e="T336" id="Seg_11267" s="T335">n</ta>
            <ta e="T337" id="Seg_11268" s="T336">adj</ta>
            <ta e="T338" id="Seg_11269" s="T337">n</ta>
            <ta e="T339" id="Seg_11270" s="T338">ptcl</ta>
            <ta e="T340" id="Seg_11271" s="T339">v</ta>
            <ta e="T341" id="Seg_11272" s="T340">ptcl</ta>
            <ta e="T342" id="Seg_11273" s="T341">pers</ta>
            <ta e="T343" id="Seg_11274" s="T342">ptcl</ta>
            <ta e="T344" id="Seg_11275" s="T343">interrog</ta>
            <ta e="T345" id="Seg_11276" s="T344">ptcl</ta>
            <ta e="T346" id="Seg_11277" s="T345">v</ta>
            <ta e="T347" id="Seg_11278" s="T346">adj</ta>
            <ta e="T348" id="Seg_11279" s="T347">n</ta>
            <ta e="T349" id="Seg_11280" s="T348">ptcl</ta>
            <ta e="T350" id="Seg_11281" s="T349">v</ta>
            <ta e="T351" id="Seg_11282" s="T350">dem</ta>
            <ta e="T352" id="Seg_11283" s="T351">n</ta>
            <ta e="T353" id="Seg_11284" s="T352">adv</ta>
            <ta e="T354" id="Seg_11285" s="T353">v</ta>
            <ta e="T355" id="Seg_11286" s="T354">dem</ta>
            <ta e="T356" id="Seg_11287" s="T355">n</ta>
            <ta e="T357" id="Seg_11288" s="T356">dem</ta>
            <ta e="T358" id="Seg_11289" s="T357">n</ta>
            <ta e="T359" id="Seg_11290" s="T358">n</ta>
            <ta e="T360" id="Seg_11291" s="T359">ptcl</ta>
            <ta e="T361" id="Seg_11292" s="T360">adv</ta>
            <ta e="T362" id="Seg_11293" s="T361">adv</ta>
            <ta e="T363" id="Seg_11294" s="T362">n</ta>
            <ta e="T364" id="Seg_11295" s="T363">v</ta>
            <ta e="T365" id="Seg_11296" s="T364">ptcl</ta>
            <ta e="T366" id="Seg_11297" s="T365">num</ta>
            <ta e="T367" id="Seg_11298" s="T366">n</ta>
            <ta e="T368" id="Seg_11299" s="T367">n</ta>
            <ta e="T369" id="Seg_11300" s="T368">n</ta>
            <ta e="T370" id="Seg_11301" s="T369">ptcl</ta>
            <ta e="T371" id="Seg_11302" s="T370">n</ta>
            <ta e="T372" id="Seg_11303" s="T371">v</ta>
            <ta e="T373" id="Seg_11304" s="T372">ptcl</ta>
            <ta e="T374" id="Seg_11305" s="T373">adv</ta>
            <ta e="T375" id="Seg_11306" s="T374">v</ta>
            <ta e="T376" id="Seg_11307" s="T375">ptcl</ta>
            <ta e="T377" id="Seg_11308" s="T376">n</ta>
            <ta e="T378" id="Seg_11309" s="T377">v</ta>
            <ta e="T379" id="Seg_11310" s="T378">adv</ta>
            <ta e="T380" id="Seg_11311" s="T379">n</ta>
            <ta e="T381" id="Seg_11312" s="T380">v</ta>
            <ta e="T382" id="Seg_11313" s="T381">adv</ta>
            <ta e="T383" id="Seg_11314" s="T382">ptcl</ta>
            <ta e="T384" id="Seg_11315" s="T383">dem</ta>
            <ta e="T385" id="Seg_11316" s="T384">n</ta>
            <ta e="T386" id="Seg_11317" s="T385">n</ta>
            <ta e="T387" id="Seg_11318" s="T386">n</ta>
            <ta e="T388" id="Seg_11319" s="T387">ptcp</ta>
            <ta e="T389" id="Seg_11320" s="T388">ptcl</ta>
            <ta e="T390" id="Seg_11321" s="T389">interrog</ta>
            <ta e="T391" id="Seg_11322" s="T390">v</ta>
            <ta e="T392" id="Seg_11323" s="T391">dem</ta>
            <ta e="T393" id="Seg_11324" s="T392">n</ta>
            <ta e="T394" id="Seg_11325" s="T393">adv</ta>
            <ta e="T395" id="Seg_11326" s="T394">v</ta>
            <ta e="T396" id="Seg_11327" s="T395">adv</ta>
            <ta e="T397" id="Seg_11328" s="T396">adv</ta>
            <ta e="T398" id="Seg_11329" s="T397">v</ta>
            <ta e="T399" id="Seg_11330" s="T398">ptcl</ta>
            <ta e="T401" id="Seg_11331" s="T400">pers</ta>
            <ta e="T402" id="Seg_11332" s="T401">v</ta>
            <ta e="T403" id="Seg_11333" s="T402">adj</ta>
            <ta e="T404" id="Seg_11334" s="T403">adj</ta>
            <ta e="T405" id="Seg_11335" s="T404">v</ta>
            <ta e="T406" id="Seg_11336" s="T405">v</ta>
            <ta e="T407" id="Seg_11337" s="T406">n</ta>
            <ta e="T408" id="Seg_11338" s="T407">ptcl</ta>
            <ta e="T409" id="Seg_11339" s="T408">interrog</ta>
            <ta e="T410" id="Seg_11340" s="T409">adj</ta>
            <ta e="T411" id="Seg_11341" s="T410">v</ta>
            <ta e="T412" id="Seg_11342" s="T411">v</ta>
            <ta e="T413" id="Seg_11343" s="T412">pers</ta>
            <ta e="T414" id="Seg_11344" s="T413">v</ta>
            <ta e="T415" id="Seg_11345" s="T414">ptcl</ta>
            <ta e="T416" id="Seg_11346" s="T415">conj</ta>
            <ta e="T417" id="Seg_11347" s="T416">dem</ta>
            <ta e="T418" id="Seg_11348" s="T417">dem</ta>
            <ta e="T419" id="Seg_11349" s="T418">n</ta>
            <ta e="T420" id="Seg_11350" s="T419">n</ta>
            <ta e="T421" id="Seg_11351" s="T420">n</ta>
            <ta e="T422" id="Seg_11352" s="T421">pro</ta>
            <ta e="T423" id="Seg_11353" s="T422">v</ta>
            <ta e="T424" id="Seg_11354" s="T423">dem</ta>
            <ta e="T425" id="Seg_11355" s="T424">n</ta>
            <ta e="T426" id="Seg_11356" s="T425">pers</ta>
            <ta e="T427" id="Seg_11357" s="T426">ptcl</ta>
            <ta e="T428" id="Seg_11358" s="T427">adv</ta>
            <ta e="T429" id="Seg_11359" s="T428">v</ta>
            <ta e="T430" id="Seg_11360" s="T429">n</ta>
            <ta e="T431" id="Seg_11361" s="T430">adv</ta>
            <ta e="T433" id="Seg_11362" s="T432">adj</ta>
            <ta e="T434" id="Seg_11363" s="T433">n</ta>
            <ta e="T435" id="Seg_11364" s="T434">v</ta>
            <ta e="T436" id="Seg_11365" s="T435">dem</ta>
            <ta e="T437" id="Seg_11366" s="T436">adj</ta>
            <ta e="T438" id="Seg_11367" s="T437">n</ta>
            <ta e="T439" id="Seg_11368" s="T438">v</ta>
            <ta e="T440" id="Seg_11369" s="T439">adj</ta>
            <ta e="T441" id="Seg_11370" s="T440">n</ta>
            <ta e="T442" id="Seg_11371" s="T441">v</ta>
            <ta e="T443" id="Seg_11372" s="T442">v</ta>
            <ta e="T444" id="Seg_11373" s="T443">v</ta>
            <ta e="T445" id="Seg_11374" s="T444">v</ta>
            <ta e="T446" id="Seg_11375" s="T445">v</ta>
            <ta e="T447" id="Seg_11376" s="T446">ptcl</ta>
            <ta e="T448" id="Seg_11377" s="T447">ptcl</ta>
            <ta e="T449" id="Seg_11378" s="T448">v</ta>
            <ta e="T450" id="Seg_11379" s="T449">adj</ta>
            <ta e="T451" id="Seg_11380" s="T450">adj</ta>
            <ta e="T452" id="Seg_11381" s="T451">n</ta>
            <ta e="T453" id="Seg_11382" s="T452">v</ta>
            <ta e="T454" id="Seg_11383" s="T453">nprop</ta>
            <ta e="T455" id="Seg_11384" s="T454">adj</ta>
            <ta e="T456" id="Seg_11385" s="T455">n</ta>
            <ta e="T457" id="Seg_11386" s="T456">pers</ta>
            <ta e="T458" id="Seg_11387" s="T457">dem</ta>
            <ta e="T459" id="Seg_11388" s="T458">n</ta>
            <ta e="T460" id="Seg_11389" s="T459">adj</ta>
            <ta e="T461" id="Seg_11390" s="T460">n</ta>
            <ta e="T462" id="Seg_11391" s="T461">adv</ta>
            <ta e="T463" id="Seg_11392" s="T462">v</ta>
            <ta e="T464" id="Seg_11393" s="T463">n</ta>
            <ta e="T465" id="Seg_11394" s="T464">adv</ta>
            <ta e="T466" id="Seg_11395" s="T465">dem</ta>
            <ta e="T467" id="Seg_11396" s="T466">n</ta>
            <ta e="T468" id="Seg_11397" s="T467">adv</ta>
            <ta e="T469" id="Seg_11398" s="T468">adv</ta>
            <ta e="T470" id="Seg_11399" s="T469">v</ta>
            <ta e="T471" id="Seg_11400" s="T470">dem</ta>
            <ta e="T472" id="Seg_11401" s="T471">n</ta>
            <ta e="T473" id="Seg_11402" s="T472">ptcl</ta>
            <ta e="T474" id="Seg_11403" s="T473">v</ta>
            <ta e="T475" id="Seg_11404" s="T474">ptcl</ta>
            <ta e="T476" id="Seg_11405" s="T475">pers</ta>
            <ta e="T477" id="Seg_11406" s="T476">ptcl</ta>
            <ta e="T479" id="Seg_11407" s="T478">n</ta>
            <ta e="T480" id="Seg_11408" s="T479">ptcl</ta>
            <ta e="T481" id="Seg_11409" s="T480">v</ta>
            <ta e="T482" id="Seg_11410" s="T481">n</ta>
            <ta e="T483" id="Seg_11411" s="T482">v</ta>
            <ta e="T484" id="Seg_11412" s="T483">dem</ta>
            <ta e="T485" id="Seg_11413" s="T484">n</ta>
            <ta e="T486" id="Seg_11414" s="T485">n</ta>
            <ta e="T487" id="Seg_11415" s="T486">adj</ta>
            <ta e="T488" id="Seg_11416" s="T487">adj</ta>
            <ta e="T489" id="Seg_11417" s="T488">n</ta>
            <ta e="T490" id="Seg_11418" s="T489">v</ta>
            <ta e="T491" id="Seg_11419" s="T490">adv</ta>
            <ta e="T492" id="Seg_11420" s="T491">ptcl</ta>
            <ta e="T493" id="Seg_11421" s="T492">v</ta>
            <ta e="T494" id="Seg_11422" s="T493">dem</ta>
            <ta e="T495" id="Seg_11423" s="T494">n</ta>
            <ta e="T496" id="Seg_11424" s="T495">ptcl</ta>
            <ta e="T497" id="Seg_11425" s="T496">adj</ta>
            <ta e="T993" id="Seg_11426" s="T497">n</ta>
            <ta e="T498" id="Seg_11427" s="T993">n</ta>
            <ta e="T499" id="Seg_11428" s="T498">interrog</ta>
            <ta e="T500" id="Seg_11429" s="T499">n</ta>
            <ta e="T501" id="Seg_11430" s="T500">v</ta>
            <ta e="T502" id="Seg_11431" s="T501">adj</ta>
            <ta e="T503" id="Seg_11432" s="T502">n</ta>
            <ta e="T504" id="Seg_11433" s="T503">adj</ta>
            <ta e="T505" id="Seg_11434" s="T504">n</ta>
            <ta e="T506" id="Seg_11435" s="T505">dem</ta>
            <ta e="T507" id="Seg_11436" s="T506">adj</ta>
            <ta e="T975" id="Seg_11437" s="T507">v</ta>
            <ta e="T976" id="Seg_11438" s="T509">v</ta>
            <ta e="T512" id="Seg_11439" s="T511">dem</ta>
            <ta e="T513" id="Seg_11440" s="T512">adv</ta>
            <ta e="T514" id="Seg_11441" s="T513">v</ta>
            <ta e="T517" id="Seg_11442" s="T516">dem</ta>
            <ta e="T518" id="Seg_11443" s="T517">ptcl</ta>
            <ta e="T520" id="Seg_11444" s="T519">adv</ta>
            <ta e="T521" id="Seg_11445" s="T520">ptcl</ta>
            <ta e="T522" id="Seg_11446" s="T521">v</ta>
            <ta e="T523" id="Seg_11447" s="T522">ptcl</ta>
            <ta e="T980" id="Seg_11448" s="T523">v</ta>
            <ta e="T525" id="Seg_11449" s="T524">v</ta>
            <ta e="T526" id="Seg_11450" s="T525">ptcl</ta>
            <ta e="T981" id="Seg_11451" s="T526">v</ta>
            <ta e="T528" id="Seg_11452" s="T527">ptcl</ta>
            <ta e="T529" id="Seg_11453" s="T528">adj</ta>
            <ta e="T531" id="Seg_11454" s="T530">ptcl</ta>
            <ta e="T532" id="Seg_11455" s="T531">v</ta>
            <ta e="T533" id="Seg_11456" s="T532">ptcl</ta>
            <ta e="T534" id="Seg_11457" s="T533">adv</ta>
            <ta e="T983" id="Seg_11458" s="T534">v</ta>
            <ta e="T974" id="Seg_11459" s="T535">adj</ta>
            <ta e="T537" id="Seg_11460" s="T536">adv</ta>
            <ta e="T538" id="Seg_11461" s="T537">adv</ta>
            <ta e="T539" id="Seg_11462" s="T538">v</ta>
            <ta e="T540" id="Seg_11463" s="T539">dem</ta>
            <ta e="T541" id="Seg_11464" s="T540">n</ta>
            <ta e="T542" id="Seg_11465" s="T541">n</ta>
            <ta e="T543" id="Seg_11466" s="T542">v</ta>
            <ta e="T544" id="Seg_11467" s="T543">adj</ta>
            <ta e="T545" id="Seg_11468" s="T544">n</ta>
            <ta e="T546" id="Seg_11469" s="T545">adj</ta>
            <ta e="T547" id="Seg_11470" s="T546">v</ta>
            <ta e="T548" id="Seg_11471" s="T547">dem</ta>
            <ta e="T549" id="Seg_11472" s="T548">n</ta>
            <ta e="T550" id="Seg_11473" s="T549">pers</ta>
            <ta e="T551" id="Seg_11474" s="T550">ptcl</ta>
            <ta e="T552" id="Seg_11475" s="T551">adv</ta>
            <ta e="T553" id="Seg_11476" s="T552">n</ta>
            <ta e="T554" id="Seg_11477" s="T553">v</ta>
            <ta e="T555" id="Seg_11478" s="T554">ptcl</ta>
            <ta e="T556" id="Seg_11479" s="T555">ptcl</ta>
            <ta e="T557" id="Seg_11480" s="T556">v</ta>
            <ta e="T558" id="Seg_11481" s="T557">conj</ta>
            <ta e="T559" id="Seg_11482" s="T558">qv</ta>
            <ta e="T560" id="Seg_11483" s="T559">adv</ta>
            <ta e="T561" id="Seg_11484" s="T560">v</ta>
            <ta e="T562" id="Seg_11485" s="T561">num</ta>
            <ta e="T563" id="Seg_11486" s="T562">n</ta>
            <ta e="T564" id="Seg_11487" s="T563">adv</ta>
            <ta e="T565" id="Seg_11488" s="T564">ptcl</ta>
            <ta e="T566" id="Seg_11489" s="T565">adj</ta>
            <ta e="T567" id="Seg_11490" s="T566">n</ta>
            <ta e="T568" id="Seg_11491" s="T567">v</ta>
            <ta e="T569" id="Seg_11492" s="T568">ptcl</ta>
            <ta e="T570" id="Seg_11493" s="T569">n</ta>
            <ta e="T571" id="Seg_11494" s="T570">adv</ta>
            <ta e="T572" id="Seg_11495" s="T571">v</ta>
            <ta e="T573" id="Seg_11496" s="T572">n</ta>
            <ta e="T574" id="Seg_11497" s="T573">emphpro</ta>
            <ta e="T575" id="Seg_11498" s="T574">v</ta>
            <ta e="T576" id="Seg_11499" s="T575">adj</ta>
            <ta e="T577" id="Seg_11500" s="T576">ptcl</ta>
            <ta e="T578" id="Seg_11501" s="T577">v</ta>
            <ta e="T579" id="Seg_11502" s="T578">adv</ta>
            <ta e="T580" id="Seg_11503" s="T579">ptcl</ta>
            <ta e="T582" id="Seg_11504" s="T581">num</ta>
            <ta e="T583" id="Seg_11505" s="T582">dem</ta>
            <ta e="T584" id="Seg_11506" s="T583">pp</ta>
            <ta e="T585" id="Seg_11507" s="T584">n</ta>
            <ta e="T586" id="Seg_11508" s="T585">n</ta>
            <ta e="T587" id="Seg_11509" s="T586">ptcl</ta>
            <ta e="T588" id="Seg_11510" s="T587">n</ta>
            <ta e="T589" id="Seg_11511" s="T588">n</ta>
            <ta e="T590" id="Seg_11512" s="T589">dem</ta>
            <ta e="T591" id="Seg_11513" s="T590">n</ta>
            <ta e="T592" id="Seg_11514" s="T591">adv</ta>
            <ta e="T593" id="Seg_11515" s="T592">v</ta>
            <ta e="T594" id="Seg_11516" s="T593">ptcl</ta>
            <ta e="T595" id="Seg_11517" s="T594">adv</ta>
            <ta e="T596" id="Seg_11518" s="T595">num</ta>
            <ta e="T597" id="Seg_11519" s="T596">dem</ta>
            <ta e="T598" id="Seg_11520" s="T597">adv</ta>
            <ta e="T599" id="Seg_11521" s="T598">n</ta>
            <ta e="T600" id="Seg_11522" s="T599">n</ta>
            <ta e="T601" id="Seg_11523" s="T600">ptcl</ta>
            <ta e="T602" id="Seg_11524" s="T601">preverb</ta>
            <ta e="T603" id="Seg_11525" s="T602">v</ta>
            <ta e="T604" id="Seg_11526" s="T603">adv</ta>
            <ta e="T605" id="Seg_11527" s="T604">nprop</ta>
            <ta e="T606" id="Seg_11528" s="T605">adj</ta>
            <ta e="T967" id="Seg_11529" s="T606">ptcl</ta>
            <ta e="T607" id="Seg_11530" s="T967">preverb</ta>
            <ta e="T608" id="Seg_11531" s="T607">v</ta>
            <ta e="T609" id="Seg_11532" s="T608">num</ta>
            <ta e="T610" id="Seg_11533" s="T609">n</ta>
            <ta e="T611" id="Seg_11534" s="T610">ptcl</ta>
            <ta e="T612" id="Seg_11535" s="T611">n</ta>
            <ta e="T613" id="Seg_11536" s="T612">ptcp</ta>
            <ta e="T614" id="Seg_11537" s="T613">nprop</ta>
            <ta e="T615" id="Seg_11538" s="T614">n</ta>
            <ta e="T616" id="Seg_11539" s="T615">quant</ta>
            <ta e="T617" id="Seg_11540" s="T616">ptcp</ta>
            <ta e="T618" id="Seg_11541" s="T617">n</ta>
            <ta e="T619" id="Seg_11542" s="T618">quant</ta>
            <ta e="T620" id="Seg_11543" s="T619">n</ta>
            <ta e="T621" id="Seg_11544" s="T620">n</ta>
            <ta e="T622" id="Seg_11545" s="T621">adv</ta>
            <ta e="T623" id="Seg_11546" s="T622">adv</ta>
            <ta e="T624" id="Seg_11547" s="T623">v</ta>
            <ta e="T625" id="Seg_11548" s="T624">dem</ta>
            <ta e="T626" id="Seg_11549" s="T625">n</ta>
            <ta e="T627" id="Seg_11550" s="T626">v</ta>
            <ta e="T628" id="Seg_11551" s="T627">adv</ta>
            <ta e="T629" id="Seg_11552" s="T628">n</ta>
            <ta e="T630" id="Seg_11553" s="T629">v</ta>
            <ta e="T631" id="Seg_11554" s="T630">v</ta>
            <ta e="T632" id="Seg_11555" s="T631">dem</ta>
            <ta e="T633" id="Seg_11556" s="T632">n</ta>
            <ta e="T634" id="Seg_11557" s="T633">preverb</ta>
            <ta e="T635" id="Seg_11558" s="T634">v</ta>
            <ta e="T636" id="Seg_11559" s="T635">n</ta>
            <ta e="T638" id="Seg_11560" s="T637">ptcl</ta>
            <ta e="T639" id="Seg_11561" s="T638">conj</ta>
            <ta e="T640" id="Seg_11562" s="T639">v</ta>
            <ta e="T641" id="Seg_11563" s="T640">ptcl</ta>
            <ta e="T642" id="Seg_11564" s="T641">nprop</ta>
            <ta e="T643" id="Seg_11565" s="T642">adj</ta>
            <ta e="T644" id="Seg_11566" s="T643">ptcl</ta>
            <ta e="T645" id="Seg_11567" s="T644">ptcl</ta>
            <ta e="T646" id="Seg_11568" s="T645">ptcl</ta>
            <ta e="T647" id="Seg_11569" s="T646">v</ta>
            <ta e="T648" id="Seg_11570" s="T647">adj</ta>
            <ta e="T649" id="Seg_11571" s="T648">n</ta>
            <ta e="T650" id="Seg_11572" s="T649">adv</ta>
            <ta e="T651" id="Seg_11573" s="T650">v</ta>
            <ta e="T652" id="Seg_11574" s="T651">dem</ta>
            <ta e="T653" id="Seg_11575" s="T652">adv</ta>
            <ta e="T654" id="Seg_11576" s="T653">pers</ta>
            <ta e="T655" id="Seg_11577" s="T654">adv</ta>
            <ta e="T656" id="Seg_11578" s="T655">v</ta>
            <ta e="T657" id="Seg_11579" s="T656">n</ta>
            <ta e="T658" id="Seg_11580" s="T657">v</ta>
            <ta e="T659" id="Seg_11581" s="T658">n</ta>
            <ta e="T660" id="Seg_11582" s="T659">pp</ta>
            <ta e="T661" id="Seg_11583" s="T660">adv</ta>
            <ta e="T662" id="Seg_11584" s="T661">n</ta>
            <ta e="T664" id="Seg_11585" s="T663">n</ta>
            <ta e="T665" id="Seg_11586" s="T664">pp</ta>
            <ta e="T667" id="Seg_11587" s="T666">n</ta>
            <ta e="T668" id="Seg_11588" s="T667">v</ta>
            <ta e="T669" id="Seg_11589" s="T668">n</ta>
            <ta e="T670" id="Seg_11590" s="T669">ptcl</ta>
            <ta e="T671" id="Seg_11591" s="T670">v</ta>
            <ta e="T672" id="Seg_11592" s="T671">ptcp</ta>
            <ta e="T673" id="Seg_11593" s="T672">adj</ta>
            <ta e="T674" id="Seg_11594" s="T673">n</ta>
            <ta e="T675" id="Seg_11595" s="T674">v</ta>
            <ta e="T676" id="Seg_11596" s="T675">adv</ta>
            <ta e="T677" id="Seg_11597" s="T676">v</ta>
            <ta e="T678" id="Seg_11598" s="T677">adv</ta>
            <ta e="T679" id="Seg_11599" s="T678">adv</ta>
            <ta e="T680" id="Seg_11600" s="T679">nprop</ta>
            <ta e="T681" id="Seg_11601" s="T680">n</ta>
            <ta e="T682" id="Seg_11602" s="T681">ptcl</ta>
            <ta e="T683" id="Seg_11603" s="T682">v</ta>
            <ta e="T684" id="Seg_11604" s="T683">ptcl</ta>
            <ta e="T685" id="Seg_11605" s="T684">ptcl</ta>
            <ta e="T686" id="Seg_11606" s="T685">adj</ta>
            <ta e="T687" id="Seg_11607" s="T686">ptcl</ta>
            <ta e="T688" id="Seg_11608" s="T687">n</ta>
            <ta e="T689" id="Seg_11609" s="T688">v</ta>
            <ta e="T690" id="Seg_11610" s="T689">ptcl</ta>
            <ta e="T691" id="Seg_11611" s="T690">adv</ta>
            <ta e="T692" id="Seg_11612" s="T691">v</ta>
            <ta e="T693" id="Seg_11613" s="T692">ptcl</ta>
            <ta e="T694" id="Seg_11614" s="T693">n</ta>
            <ta e="T695" id="Seg_11615" s="T694">v</ta>
            <ta e="T696" id="Seg_11616" s="T695">adj</ta>
            <ta e="T697" id="Seg_11617" s="T696">n</ta>
            <ta e="T698" id="Seg_11618" s="T697">v</ta>
            <ta e="T699" id="Seg_11619" s="T698">nprop</ta>
            <ta e="T700" id="Seg_11620" s="T699">adj</ta>
            <ta e="T701" id="Seg_11621" s="T700">n</ta>
            <ta e="T702" id="Seg_11622" s="T701">quant</ta>
            <ta e="T703" id="Seg_11623" s="T702">adv</ta>
            <ta e="T704" id="Seg_11624" s="T703">v</ta>
            <ta e="T705" id="Seg_11625" s="T704">adj</ta>
            <ta e="T706" id="Seg_11626" s="T705">v</ta>
            <ta e="T707" id="Seg_11627" s="T706">nprop</ta>
            <ta e="T708" id="Seg_11628" s="T707">n</ta>
            <ta e="T709" id="Seg_11629" s="T708">adv</ta>
            <ta e="T710" id="Seg_11630" s="T709">v</ta>
            <ta e="T711" id="Seg_11631" s="T710">num</ta>
            <ta e="T712" id="Seg_11632" s="T711">n</ta>
            <ta e="T713" id="Seg_11633" s="T712">adv</ta>
            <ta e="T714" id="Seg_11634" s="T713">n</ta>
            <ta e="T715" id="Seg_11635" s="T714">adv</ta>
            <ta e="T716" id="Seg_11636" s="T715">v</ta>
            <ta e="T717" id="Seg_11637" s="T716">dem</ta>
            <ta e="T718" id="Seg_11638" s="T717">n</ta>
            <ta e="T719" id="Seg_11639" s="T718">num</ta>
            <ta e="T720" id="Seg_11640" s="T719">n</ta>
            <ta e="T721" id="Seg_11641" s="T720">n</ta>
            <ta e="T722" id="Seg_11642" s="T721">v</ta>
            <ta e="T723" id="Seg_11643" s="T722">quant</ta>
            <ta e="T724" id="Seg_11644" s="T723">adv</ta>
            <ta e="T725" id="Seg_11645" s="T724">n</ta>
            <ta e="T726" id="Seg_11646" s="T725">pp</ta>
            <ta e="T727" id="Seg_11647" s="T726">v</ta>
            <ta e="T728" id="Seg_11648" s="T727">dem</ta>
            <ta e="T729" id="Seg_11649" s="T728">ptcp</ta>
            <ta e="T730" id="Seg_11650" s="T729">n</ta>
            <ta e="T731" id="Seg_11651" s="T730">n</ta>
            <ta e="T732" id="Seg_11652" s="T731">adv</ta>
            <ta e="T733" id="Seg_11653" s="T732">adv</ta>
            <ta e="T734" id="Seg_11654" s="T733">adv</ta>
            <ta e="T735" id="Seg_11655" s="T734">v</ta>
            <ta e="T736" id="Seg_11656" s="T735">n</ta>
            <ta e="T737" id="Seg_11657" s="T736">adv</ta>
            <ta e="T738" id="Seg_11658" s="T737">dem</ta>
            <ta e="T739" id="Seg_11659" s="T738">n</ta>
            <ta e="T742" id="Seg_11660" s="T741">n</ta>
            <ta e="T743" id="Seg_11661" s="T742">ptcl</ta>
            <ta e="T744" id="Seg_11662" s="T743">v</ta>
            <ta e="T745" id="Seg_11663" s="T744">nprop</ta>
            <ta e="T746" id="Seg_11664" s="T745">n</ta>
            <ta e="T747" id="Seg_11665" s="T746">ptcl</ta>
            <ta e="T748" id="Seg_11666" s="T747">adv</ta>
            <ta e="T749" id="Seg_11667" s="T748">v</ta>
            <ta e="T750" id="Seg_11668" s="T749">interrog</ta>
            <ta e="T751" id="Seg_11669" s="T750">quant</ta>
            <ta e="T752" id="Seg_11670" s="T751">adv</ta>
            <ta e="T753" id="Seg_11671" s="T752">v</ta>
            <ta e="T754" id="Seg_11672" s="T753">preverb</ta>
            <ta e="T755" id="Seg_11673" s="T754">v</ta>
            <ta e="T756" id="Seg_11674" s="T755">dem</ta>
            <ta e="T757" id="Seg_11675" s="T756">adj</ta>
            <ta e="T758" id="Seg_11676" s="T757">v</ta>
            <ta e="T759" id="Seg_11677" s="T758">preverb</ta>
            <ta e="T760" id="Seg_11678" s="T759">v</ta>
            <ta e="T762" id="Seg_11679" s="T761">preverb</ta>
            <ta e="T763" id="Seg_11680" s="T762">v</ta>
            <ta e="T764" id="Seg_11681" s="T763">dem</ta>
            <ta e="T765" id="Seg_11682" s="T764">n</ta>
            <ta e="T766" id="Seg_11683" s="T765">n</ta>
            <ta e="T767" id="Seg_11684" s="T766">ptcl</ta>
            <ta e="T768" id="Seg_11685" s="T767">v</ta>
            <ta e="T769" id="Seg_11686" s="T768">adv</ta>
            <ta e="T770" id="Seg_11687" s="T769">v</ta>
            <ta e="T771" id="Seg_11688" s="T770">conj</ta>
            <ta e="T772" id="Seg_11689" s="T771">v</ta>
            <ta e="T773" id="Seg_11690" s="T772">adv</ta>
            <ta e="T774" id="Seg_11691" s="T773">v</ta>
            <ta e="T775" id="Seg_11692" s="T774">adj</ta>
            <ta e="T776" id="Seg_11693" s="T775">n</ta>
            <ta e="T777" id="Seg_11694" s="T776">preverb</ta>
            <ta e="T778" id="Seg_11695" s="T777">v</ta>
            <ta e="T779" id="Seg_11696" s="T778">nprop</ta>
            <ta e="T780" id="Seg_11697" s="T779">n</ta>
            <ta e="T781" id="Seg_11698" s="T780">dem</ta>
            <ta e="T782" id="Seg_11699" s="T781">ptcl</ta>
            <ta e="T783" id="Seg_11700" s="T782">adj</ta>
            <ta e="T784" id="Seg_11701" s="T783">n</ta>
            <ta e="T785" id="Seg_11702" s="T784">adj</ta>
            <ta e="T786" id="Seg_11703" s="T785">v</ta>
            <ta e="T787" id="Seg_11704" s="T786">dem</ta>
            <ta e="T788" id="Seg_11705" s="T787">adv</ta>
            <ta e="T789" id="Seg_11706" s="T788">n</ta>
            <ta e="T791" id="Seg_11707" s="T790">dem</ta>
            <ta e="T792" id="Seg_11708" s="T791">n</ta>
            <ta e="T794" id="Seg_11709" s="T793">adv</ta>
            <ta e="T795" id="Seg_11710" s="T794">adj</ta>
            <ta e="T796" id="Seg_11711" s="T795">v</ta>
            <ta e="T797" id="Seg_11712" s="T796">adj</ta>
            <ta e="T798" id="Seg_11713" s="T797">v</ta>
            <ta e="T799" id="Seg_11714" s="T798">dem</ta>
            <ta e="T800" id="Seg_11715" s="T799">n</ta>
            <ta e="T801" id="Seg_11716" s="T800">ptcl</ta>
            <ta e="T802" id="Seg_11717" s="T801">dem</ta>
            <ta e="T803" id="Seg_11718" s="T802">n</ta>
            <ta e="T804" id="Seg_11719" s="T803">n</ta>
            <ta e="T805" id="Seg_11720" s="T804">adv</ta>
            <ta e="T806" id="Seg_11721" s="T805">v</ta>
            <ta e="T807" id="Seg_11722" s="T806">ptcl</ta>
            <ta e="T808" id="Seg_11723" s="T807">n</ta>
            <ta e="T809" id="Seg_11724" s="T808">v</ta>
            <ta e="T810" id="Seg_11725" s="T809">ptcl</ta>
            <ta e="T811" id="Seg_11726" s="T810">adv</ta>
            <ta e="T812" id="Seg_11727" s="T811">emphpro</ta>
            <ta e="T813" id="Seg_11728" s="T812">n</ta>
            <ta e="T814" id="Seg_11729" s="T813">adv</ta>
            <ta e="T815" id="Seg_11730" s="T814">adv</ta>
            <ta e="T816" id="Seg_11731" s="T815">adv</ta>
            <ta e="T817" id="Seg_11732" s="T816">adv</ta>
            <ta e="T818" id="Seg_11733" s="T817">adv</ta>
            <ta e="T821" id="Seg_11734" s="T820">ptcl</ta>
            <ta e="T822" id="Seg_11735" s="T821">n</ta>
            <ta e="T823" id="Seg_11736" s="T822">v</ta>
            <ta e="T824" id="Seg_11737" s="T823">conj</ta>
            <ta e="T825" id="Seg_11738" s="T824">conj</ta>
            <ta e="T826" id="Seg_11739" s="T825">v</ta>
            <ta e="T827" id="Seg_11740" s="T826">conj</ta>
            <ta e="T828" id="Seg_11741" s="T827">conj</ta>
            <ta e="T829" id="Seg_11742" s="T828">ptcl</ta>
            <ta e="T830" id="Seg_11743" s="T829">v</ta>
            <ta e="T831" id="Seg_11744" s="T830">adv</ta>
            <ta e="T832" id="Seg_11745" s="T831">dem</ta>
            <ta e="T833" id="Seg_11746" s="T832">n</ta>
            <ta e="T834" id="Seg_11747" s="T833">dem</ta>
            <ta e="T835" id="Seg_11748" s="T834">v</ta>
            <ta e="T836" id="Seg_11749" s="T835">n</ta>
            <ta e="T837" id="Seg_11750" s="T836">dem</ta>
            <ta e="T838" id="Seg_11751" s="T837">adv</ta>
            <ta e="T839" id="Seg_11752" s="T838">adv</ta>
            <ta e="T840" id="Seg_11753" s="T839">adv</ta>
            <ta e="T841" id="Seg_11754" s="T840">n</ta>
            <ta e="T842" id="Seg_11755" s="T841">dem</ta>
            <ta e="T843" id="Seg_11756" s="T842">ptcp</ta>
            <ta e="T844" id="Seg_11757" s="T843">n</ta>
            <ta e="T845" id="Seg_11758" s="T844">adv</ta>
            <ta e="T846" id="Seg_11759" s="T845">v</ta>
            <ta e="T847" id="Seg_11760" s="T846">adv</ta>
            <ta e="T848" id="Seg_11761" s="T847">adv</ta>
            <ta e="T849" id="Seg_11762" s="T848">adv</ta>
            <ta e="T850" id="Seg_11763" s="T849">nprop</ta>
            <ta e="T851" id="Seg_11764" s="T850">n</ta>
            <ta e="T853" id="Seg_11765" s="T852">adv</ta>
            <ta e="T854" id="Seg_11766" s="T853">v</ta>
            <ta e="T855" id="Seg_11767" s="T854">ptcl</ta>
            <ta e="T856" id="Seg_11768" s="T855">adv</ta>
            <ta e="T857" id="Seg_11769" s="T856">n</ta>
            <ta e="T858" id="Seg_11770" s="T857">ptcl</ta>
            <ta e="T860" id="Seg_11771" s="T859">v</ta>
            <ta e="T861" id="Seg_11772" s="T860">dem</ta>
            <ta e="T862" id="Seg_11773" s="T861">adv</ta>
            <ta e="T863" id="Seg_11774" s="T862">adv</ta>
            <ta e="T864" id="Seg_11775" s="T863">v</ta>
            <ta e="T865" id="Seg_11776" s="T864">adv</ta>
            <ta e="T866" id="Seg_11777" s="T865">n</ta>
            <ta e="T867" id="Seg_11778" s="T866">adv</ta>
            <ta e="T868" id="Seg_11779" s="T867">v</ta>
            <ta e="T869" id="Seg_11780" s="T868">v</ta>
            <ta e="T870" id="Seg_11781" s="T869">preverb</ta>
            <ta e="T871" id="Seg_11782" s="T870">v</ta>
            <ta e="T872" id="Seg_11783" s="T871">pers</ta>
            <ta e="T873" id="Seg_11784" s="T872">adv</ta>
            <ta e="T874" id="Seg_11785" s="T873">v</ta>
            <ta e="T875" id="Seg_11786" s="T874">n</ta>
            <ta e="T876" id="Seg_11787" s="T875">ptcl</ta>
            <ta e="T877" id="Seg_11788" s="T876">v</ta>
            <ta e="T878" id="Seg_11789" s="T877">ptcl</ta>
            <ta e="T879" id="Seg_11790" s="T878">emphpro</ta>
            <ta e="T880" id="Seg_11791" s="T879">dem</ta>
            <ta e="T881" id="Seg_11792" s="T880">adj</ta>
            <ta e="T882" id="Seg_11793" s="T881">n</ta>
            <ta e="T883" id="Seg_11794" s="T882">clit</ta>
            <ta e="T884" id="Seg_11795" s="T883">interrog</ta>
            <ta e="T885" id="Seg_11796" s="T884">adv</ta>
            <ta e="T886" id="Seg_11797" s="T885">v</ta>
            <ta e="T887" id="Seg_11798" s="T886">dem</ta>
            <ta e="T888" id="Seg_11799" s="T887">emphpro</ta>
            <ta e="T889" id="Seg_11800" s="T888">ptcl</ta>
            <ta e="T890" id="Seg_11801" s="T889">v</ta>
            <ta e="T892" id="Seg_11802" s="T891">dem</ta>
            <ta e="T893" id="Seg_11803" s="T892">n</ta>
            <ta e="T894" id="Seg_11804" s="T893">ptcl</ta>
            <ta e="T895" id="Seg_11805" s="T894">v</ta>
            <ta e="T896" id="Seg_11806" s="T895">n</ta>
            <ta e="T897" id="Seg_11807" s="T896">n</ta>
            <ta e="T898" id="Seg_11808" s="T897">n</ta>
            <ta e="T899" id="Seg_11809" s="T898">nprop</ta>
            <ta e="T900" id="Seg_11810" s="T899">adj</ta>
            <ta e="T901" id="Seg_11811" s="T900">n</ta>
            <ta e="T902" id="Seg_11812" s="T901">dem</ta>
            <ta e="T903" id="Seg_11813" s="T902">adv</ta>
            <ta e="T904" id="Seg_11814" s="T903">v</ta>
            <ta e="T905" id="Seg_11815" s="T904">ptcl</ta>
            <ta e="T906" id="Seg_11816" s="T905">v</ta>
            <ta e="T907" id="Seg_11817" s="T906">adv</ta>
            <ta e="T908" id="Seg_11818" s="T907">adv</ta>
            <ta e="T910" id="Seg_11819" s="T909">ptcl</ta>
            <ta e="T912" id="Seg_11820" s="T911">ptcl</ta>
            <ta e="T985" id="Seg_11821" s="T912">v</ta>
            <ta e="T914" id="Seg_11822" s="T913">adv</ta>
            <ta e="T986" id="Seg_11823" s="T914">v</ta>
            <ta e="T916" id="Seg_11824" s="T915">adv</ta>
            <ta e="T987" id="Seg_11825" s="T916">n</ta>
            <ta e="T918" id="Seg_11826" s="T917">adv</ta>
            <ta e="T919" id="Seg_11827" s="T918">adv</ta>
            <ta e="T920" id="Seg_11828" s="T919">v</ta>
            <ta e="T921" id="Seg_11829" s="T920">n</ta>
            <ta e="T922" id="Seg_11830" s="T921">adv</ta>
            <ta e="T923" id="Seg_11831" s="T922">n</ta>
            <ta e="T924" id="Seg_11832" s="T923">n</ta>
            <ta e="T925" id="Seg_11833" s="T924">n</ta>
            <ta e="T926" id="Seg_11834" s="T925">v</ta>
            <ta e="T927" id="Seg_11835" s="T926">n</ta>
            <ta e="T928" id="Seg_11836" s="T927">dem</ta>
            <ta e="T929" id="Seg_11837" s="T928">adv</ta>
            <ta e="T930" id="Seg_11838" s="T929">adv</ta>
            <ta e="T931" id="Seg_11839" s="T930">adv</ta>
            <ta e="T932" id="Seg_11840" s="T931">v</ta>
            <ta e="T933" id="Seg_11841" s="T932">dem</ta>
            <ta e="T934" id="Seg_11842" s="T933">n</ta>
            <ta e="T935" id="Seg_11843" s="T934">adv</ta>
            <ta e="T936" id="Seg_11844" s="T935">v</ta>
            <ta e="T937" id="Seg_11845" s="T936">nprop</ta>
            <ta e="T938" id="Seg_11846" s="T937">n</ta>
            <ta e="T939" id="Seg_11847" s="T938">adv</ta>
            <ta e="T940" id="Seg_11848" s="T939">adv</ta>
            <ta e="T941" id="Seg_11849" s="T940">n</ta>
            <ta e="T942" id="Seg_11850" s="T941">v</ta>
            <ta e="T943" id="Seg_11851" s="T942">preverb</ta>
            <ta e="T944" id="Seg_11852" s="T943">v</ta>
            <ta e="T945" id="Seg_11853" s="T944">dem</ta>
            <ta e="T946" id="Seg_11854" s="T945">num</ta>
            <ta e="T947" id="Seg_11855" s="T946">n</ta>
            <ta e="T948" id="Seg_11856" s="T947">adv</ta>
            <ta e="T949" id="Seg_11857" s="T948">v</ta>
            <ta e="T950" id="Seg_11858" s="T949">nprop</ta>
            <ta e="T951" id="Seg_11859" s="T950">n</ta>
            <ta e="T952" id="Seg_11860" s="T951">nprop</ta>
            <ta e="T953" id="Seg_11861" s="T952">adj</ta>
            <ta e="T954" id="Seg_11862" s="T953">n</ta>
            <ta e="T955" id="Seg_11863" s="T954">adv</ta>
            <ta e="T988" id="Seg_11864" s="T955">n</ta>
            <ta e="T957" id="Seg_11865" s="T956">adj</ta>
            <ta e="T958" id="Seg_11866" s="T957">n</ta>
            <ta e="T959" id="Seg_11867" s="T958">v</ta>
            <ta e="T960" id="Seg_11868" s="T959">adj</ta>
            <ta e="T989" id="Seg_11869" s="T960">adj</ta>
            <ta e="T962" id="Seg_11870" s="T961">adj</ta>
            <ta e="T963" id="Seg_11871" s="T962">n</ta>
            <ta e="T964" id="Seg_11872" s="T963">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T4" id="Seg_11873" s="T3">np.h:Th</ta>
            <ta e="T12" id="Seg_11874" s="T11">0.1.h:A</ta>
            <ta e="T13" id="Seg_11875" s="T12">pro:Th</ta>
            <ta e="T14" id="Seg_11876" s="T13">pro:Th</ta>
            <ta e="T16" id="Seg_11877" s="T15">pro:Th</ta>
            <ta e="T19" id="Seg_11878" s="T18">0.3:Th</ta>
            <ta e="T29" id="Seg_11879" s="T28">pro.h:R</ta>
            <ta e="T30" id="Seg_11880" s="T29">0.3.h:A</ta>
            <ta e="T32" id="Seg_11881" s="T31">pro.h:A</ta>
            <ta e="T33" id="Seg_11882" s="T32">np:Th 0.2.h:Poss</ta>
            <ta e="T972" id="Seg_11883" s="T34">0.2.h:A 0.3:Th</ta>
            <ta e="T973" id="Seg_11884" s="T36">0.2.h:A</ta>
            <ta e="T40" id="Seg_11885" s="T39">np.h:Th</ta>
            <ta e="T42" id="Seg_11886" s="T41">0.3.h:P</ta>
            <ta e="T45" id="Seg_11887" s="T44">0.3.h:Th</ta>
            <ta e="T47" id="Seg_11888" s="T46">np:L</ta>
            <ta e="T48" id="Seg_11889" s="T47">np:Th</ta>
            <ta e="T52" id="Seg_11890" s="T51">np:L</ta>
            <ta e="T53" id="Seg_11891" s="T52">0.3.h:Th</ta>
            <ta e="T54" id="Seg_11892" s="T53">adv:L</ta>
            <ta e="T57" id="Seg_11893" s="T56">np:Poss</ta>
            <ta e="T60" id="Seg_11894" s="T59">0.3.h:Th</ta>
            <ta e="T62" id="Seg_11895" s="T61">0.3.h:P</ta>
            <ta e="T63" id="Seg_11896" s="T62">np.h:Th 0.3.h:Poss</ta>
            <ta e="T65" id="Seg_11897" s="T64">np.h:Th 0.3.h:Poss</ta>
            <ta e="T68" id="Seg_11898" s="T67">0.3.h:Poss</ta>
            <ta e="T70" id="Seg_11899" s="T69">np.h:Th 0.3.h:Poss</ta>
            <ta e="T73" id="Seg_11900" s="T72">pro.h:Th 0.3.h:Poss</ta>
            <ta e="T75" id="Seg_11901" s="T74">adv:Time</ta>
            <ta e="T77" id="Seg_11902" s="T76">np.h:P</ta>
            <ta e="T83" id="Seg_11903" s="T82">np.h:Poss</ta>
            <ta e="T84" id="Seg_11904" s="T83">adv:L</ta>
            <ta e="T85" id="Seg_11905" s="T84">0.3.h:Th</ta>
            <ta e="T88" id="Seg_11906" s="T87">np.h:Poss 0.3.h:Poss</ta>
            <ta e="T89" id="Seg_11907" s="T88">np.h:Poss 0.3.h:Poss</ta>
            <ta e="T90" id="Seg_11908" s="T89">np:L</ta>
            <ta e="T91" id="Seg_11909" s="T90">adv:L</ta>
            <ta e="T95" id="Seg_11910" s="T94">np.h:Th</ta>
            <ta e="T98" id="Seg_11911" s="T97">np.h:Th</ta>
            <ta e="T99" id="Seg_11912" s="T98">pro:L</ta>
            <ta e="T103" id="Seg_11913" s="T102">0.3.h:Th</ta>
            <ta e="T108" id="Seg_11914" s="T107">np.h:P</ta>
            <ta e="T112" id="Seg_11915" s="T111">0.3.h:P</ta>
            <ta e="T114" id="Seg_11916" s="T113">adv:Time</ta>
            <ta e="T119" id="Seg_11917" s="T118">np.h:A</ta>
            <ta e="T120" id="Seg_11918" s="T119">adv:Time</ta>
            <ta e="T121" id="Seg_11919" s="T120">pro:G</ta>
            <ta e="T123" id="Seg_11920" s="T122">adv:G</ta>
            <ta e="T124" id="Seg_11921" s="T123">np.h:G 0.3.h:Poss</ta>
            <ta e="T125" id="Seg_11922" s="T124">0.3.h:A</ta>
            <ta e="T126" id="Seg_11923" s="T125">np.h:P</ta>
            <ta e="T129" id="Seg_11924" s="T128">0.3.h:A 0.3.h:P</ta>
            <ta e="T130" id="Seg_11925" s="T129">adv:Time</ta>
            <ta e="T131" id="Seg_11926" s="T130">0.3.h:Poss</ta>
            <ta e="T133" id="Seg_11927" s="T132">np.h:P 0.3.h:Poss</ta>
            <ta e="T135" id="Seg_11928" s="T134">np.h:P 0.3.h:Poss</ta>
            <ta e="T142" id="Seg_11929" s="T141">np.h:Th 0.3.h:Poss</ta>
            <ta e="T146" id="Seg_11930" s="T145">pro:G</ta>
            <ta e="T147" id="Seg_11931" s="T146">0.3.h:A</ta>
            <ta e="T150" id="Seg_11932" s="T149">np.h:A</ta>
            <ta e="T151" id="Seg_11933" s="T150">adv:Time</ta>
            <ta e="T153" id="Seg_11934" s="T152">adv:G</ta>
            <ta e="T157" id="Seg_11935" s="T156">np.h:Th</ta>
            <ta e="T172" id="Seg_11936" s="T171">np.h:Th 0.3.h:Poss</ta>
            <ta e="T174" id="Seg_11937" s="T173">adv:G</ta>
            <ta e="T175" id="Seg_11938" s="T174">0.3.h:A 0.3.h:Th</ta>
            <ta e="T179" id="Seg_11939" s="T178">np:Ins</ta>
            <ta e="T182" id="Seg_11940" s="T181">np.h:Th</ta>
            <ta e="T183" id="Seg_11941" s="T182">adv:L</ta>
            <ta e="T184" id="Seg_11942" s="T183">0.3.h:Th</ta>
            <ta e="T185" id="Seg_11943" s="T184">0.3.h:Th</ta>
            <ta e="T186" id="Seg_11944" s="T185">adv:Time</ta>
            <ta e="T187" id="Seg_11945" s="T186">adv:G</ta>
            <ta e="T188" id="Seg_11946" s="T187">0.3.h:A</ta>
            <ta e="T189" id="Seg_11947" s="T188">adv:G</ta>
            <ta e="T190" id="Seg_11948" s="T189">0.3.h:A</ta>
            <ta e="T192" id="Seg_11949" s="T191">np.h:Th</ta>
            <ta e="T195" id="Seg_11950" s="T194">np:G</ta>
            <ta e="T197" id="Seg_11951" s="T196">np.h:Poss</ta>
            <ta e="T198" id="Seg_11952" s="T197">np.h:Th</ta>
            <ta e="T201" id="Seg_11953" s="T200">np.h:Th 0.3.h:Poss</ta>
            <ta e="T204" id="Seg_11954" s="T203">np.h:Poss 0.3.h:Poss</ta>
            <ta e="T207" id="Seg_11955" s="T206">np.h:Th</ta>
            <ta e="T209" id="Seg_11956" s="T208">adv:So</ta>
            <ta e="T210" id="Seg_11957" s="T209">adv:G</ta>
            <ta e="T212" id="Seg_11958" s="T211">np.h:A</ta>
            <ta e="T219" id="Seg_11959" s="T218">np.h:Th</ta>
            <ta e="T223" id="Seg_11960" s="T222">np.h:Poss</ta>
            <ta e="T224" id="Seg_11961" s="T223">np:Th</ta>
            <ta e="T226" id="Seg_11962" s="T225">0.3.h:A</ta>
            <ta e="T227" id="Seg_11963" s="T226">0.2.h:A</ta>
            <ta e="T231" id="Seg_11964" s="T230">np.h:A</ta>
            <ta e="T233" id="Seg_11965" s="T232">np.h:Th</ta>
            <ta e="T241" id="Seg_11966" s="T240">0.3.h:Poss np:Th</ta>
            <ta e="T243" id="Seg_11967" s="T242">np.h:Poss</ta>
            <ta e="T244" id="Seg_11968" s="T243">np:Th</ta>
            <ta e="T245" id="Seg_11969" s="T244">0.3.h:A</ta>
            <ta e="T246" id="Seg_11970" s="T245">np:Com 0.3.h:Poss</ta>
            <ta e="T249" id="Seg_11971" s="T248">np.h:A</ta>
            <ta e="T250" id="Seg_11972" s="T249">np:Th 0.3.h:Poss</ta>
            <ta e="T252" id="Seg_11973" s="T251">adv:Time</ta>
            <ta e="T253" id="Seg_11974" s="T252">0.2.h:A</ta>
            <ta e="T254" id="Seg_11975" s="T253">0.3.h:A</ta>
            <ta e="T255" id="Seg_11976" s="T254">0.3.h:Th</ta>
            <ta e="T257" id="Seg_11977" s="T256">pro.h:Poss</ta>
            <ta e="T258" id="Seg_11978" s="T257">np:L</ta>
            <ta e="T260" id="Seg_11979" s="T259">np.h:Th</ta>
            <ta e="T263" id="Seg_11980" s="T262">0.3.h:E</ta>
            <ta e="T266" id="Seg_11981" s="T265">0.3.h:A</ta>
            <ta e="T267" id="Seg_11982" s="T266">pro.h:Th</ta>
            <ta e="T275" id="Seg_11983" s="T274">adv:Time</ta>
            <ta e="T277" id="Seg_11984" s="T276">0.2.h:E 0.3:Th</ta>
            <ta e="T278" id="Seg_11985" s="T277">adv:L</ta>
            <ta e="T280" id="Seg_11986" s="T279">0.3.h:Th</ta>
            <ta e="T285" id="Seg_11987" s="T284">np:Time</ta>
            <ta e="T289" id="Seg_11988" s="T288">0.3.h:A</ta>
            <ta e="T292" id="Seg_11989" s="T291">np.h:A</ta>
            <ta e="T295" id="Seg_11990" s="T294">np:Com</ta>
            <ta e="T299" id="Seg_11991" s="T298">np:Th</ta>
            <ta e="T300" id="Seg_11992" s="T299">0.3.h:A</ta>
            <ta e="T302" id="Seg_11993" s="T301">np.h:A</ta>
            <ta e="T303" id="Seg_11994" s="T302">adv:Time</ta>
            <ta e="T305" id="Seg_11995" s="T304">np.h:Poss</ta>
            <ta e="T307" id="Seg_11996" s="T306">np:Th</ta>
            <ta e="T311" id="Seg_11997" s="T310">np:Th</ta>
            <ta e="T314" id="Seg_11998" s="T313">np:Th</ta>
            <ta e="T317" id="Seg_11999" s="T316">pro:Th</ta>
            <ta e="T318" id="Seg_12000" s="T317">pro:Th</ta>
            <ta e="T319" id="Seg_12001" s="T318">0.3.h:A</ta>
            <ta e="T320" id="Seg_12002" s="T319">adv:Time</ta>
            <ta e="T323" id="Seg_12003" s="T322">np.h:Poss</ta>
            <ta e="T324" id="Seg_12004" s="T323">np:Th</ta>
            <ta e="T325" id="Seg_12005" s="T324">0.3.h:A</ta>
            <ta e="T327" id="Seg_12006" s="T326">pro:Th</ta>
            <ta e="T328" id="Seg_12007" s="T327">0.3.h:A</ta>
            <ta e="T329" id="Seg_12008" s="T328">adv:Time</ta>
            <ta e="T330" id="Seg_12009" s="T329">adv:Time</ta>
            <ta e="T332" id="Seg_12010" s="T331">adv:G</ta>
            <ta e="T335" id="Seg_12011" s="T334">np.h:Poss</ta>
            <ta e="T336" id="Seg_12012" s="T335">np.h:A</ta>
            <ta e="T338" id="Seg_12013" s="T337">np:Time</ta>
            <ta e="T340" id="Seg_12014" s="T339">0.1.h:A</ta>
            <ta e="T342" id="Seg_12015" s="T341">pro.h:A</ta>
            <ta e="T344" id="Seg_12016" s="T343">pro:G</ta>
            <ta e="T348" id="Seg_12017" s="T347">np:Time</ta>
            <ta e="T350" id="Seg_12018" s="T349">0.3:Th</ta>
            <ta e="T352" id="Seg_12019" s="T351">np.h:A</ta>
            <ta e="T356" id="Seg_12020" s="T355">np.h:R 0.3.h:Poss</ta>
            <ta e="T358" id="Seg_12021" s="T357">np.h:Poss</ta>
            <ta e="T359" id="Seg_12022" s="T358">np.h:R</ta>
            <ta e="T361" id="Seg_12023" s="T360">adv:L</ta>
            <ta e="T363" id="Seg_12024" s="T362">np:Th</ta>
            <ta e="T368" id="Seg_12025" s="T367">np:Th</ta>
            <ta e="T371" id="Seg_12026" s="T370">np:Th</ta>
            <ta e="T374" id="Seg_12027" s="T373">adv:G</ta>
            <ta e="T375" id="Seg_12028" s="T374">0.1.h:A 0.3:Th</ta>
            <ta e="T377" id="Seg_12029" s="T376">np.h:A</ta>
            <ta e="T379" id="Seg_12030" s="T378">adv:Time</ta>
            <ta e="T380" id="Seg_12031" s="T379">np:Th</ta>
            <ta e="T381" id="Seg_12032" s="T380">0.3.h:A</ta>
            <ta e="T385" id="Seg_12033" s="T384">np:Poss</ta>
            <ta e="T391" id="Seg_12034" s="T390">0.1.h:A</ta>
            <ta e="T393" id="Seg_12035" s="T392">np.h:A</ta>
            <ta e="T396" id="Seg_12036" s="T395">adv:Time</ta>
            <ta e="T398" id="Seg_12037" s="T397">0.3.h:A</ta>
            <ta e="T401" id="Seg_12038" s="T400">pro.h:R</ta>
            <ta e="T402" id="Seg_12039" s="T401">0.2.h:A</ta>
            <ta e="T403" id="Seg_12040" s="T402">np:Th</ta>
            <ta e="T405" id="Seg_12041" s="T404">np:Th 0.2.h:Poss</ta>
            <ta e="T411" id="Seg_12042" s="T410">np:Th 0.2.h:Poss</ta>
            <ta e="T413" id="Seg_12043" s="T412">pro.h:R</ta>
            <ta e="T414" id="Seg_12044" s="T413">0.2.h:A 0.3:Th</ta>
            <ta e="T419" id="Seg_12045" s="T418">np.h:Poss 0.3.h:Poss</ta>
            <ta e="T421" id="Seg_12046" s="T420">np.h:A</ta>
            <ta e="T422" id="Seg_12047" s="T421">pro.h:Th</ta>
            <ta e="T425" id="Seg_12048" s="T424">np.h:R</ta>
            <ta e="T426" id="Seg_12049" s="T425">pro.h:A</ta>
            <ta e="T430" id="Seg_12050" s="T429">np:Th</ta>
            <ta e="T431" id="Seg_12051" s="T430">adv:L</ta>
            <ta e="T434" id="Seg_12052" s="T433">np:Th</ta>
            <ta e="T438" id="Seg_12053" s="T437">np:G</ta>
            <ta e="T439" id="Seg_12054" s="T438">0.1.h:A</ta>
            <ta e="T441" id="Seg_12055" s="T440">np:Th</ta>
            <ta e="T442" id="Seg_12056" s="T441">0.3.h:A</ta>
            <ta e="T443" id="Seg_12057" s="T442">0.3:Th</ta>
            <ta e="T444" id="Seg_12058" s="T443">0.3.h:A 0.3.h:Th</ta>
            <ta e="T445" id="Seg_12059" s="T444">0.2.h:A</ta>
            <ta e="T446" id="Seg_12060" s="T445">0.3.h:A 0.3.h:Th</ta>
            <ta e="T449" id="Seg_12061" s="T448">0.3.h:E</ta>
            <ta e="T452" id="Seg_12062" s="T451">np:Time</ta>
            <ta e="T454" id="Seg_12063" s="T453">np.h:A</ta>
            <ta e="T457" id="Seg_12064" s="T456">pro.h:G</ta>
            <ta e="T462" id="Seg_12065" s="T461">adv:Time</ta>
            <ta e="T463" id="Seg_12066" s="T462">0.3.h:A</ta>
            <ta e="T467" id="Seg_12067" s="T466">np:Time</ta>
            <ta e="T468" id="Seg_12068" s="T467">adv:Time</ta>
            <ta e="T470" id="Seg_12069" s="T469">0.3.h:A</ta>
            <ta e="T472" id="Seg_12070" s="T471">np:Th</ta>
            <ta e="T474" id="Seg_12071" s="T473">0.3.h:A</ta>
            <ta e="T479" id="Seg_12072" s="T478">np:Th</ta>
            <ta e="T481" id="Seg_12073" s="T480">0.3.h:A</ta>
            <ta e="T482" id="Seg_12074" s="T481">np:G</ta>
            <ta e="T483" id="Seg_12075" s="T482">0.3.h:A</ta>
            <ta e="T485" id="Seg_12076" s="T484">np:Poss</ta>
            <ta e="T486" id="Seg_12077" s="T485">np:Poss</ta>
            <ta e="T489" id="Seg_12078" s="T488">np:G</ta>
            <ta e="T490" id="Seg_12079" s="T489">0.3.h:A</ta>
            <ta e="T491" id="Seg_12080" s="T490">adv:Time</ta>
            <ta e="T493" id="Seg_12081" s="T492">0.3.h:A</ta>
            <ta e="T495" id="Seg_12082" s="T494">np:Com 0.3.h:Poss</ta>
            <ta e="T498" id="Seg_12083" s="T993">np.h:Poss</ta>
            <ta e="T500" id="Seg_12084" s="T499">np:Th</ta>
            <ta e="T503" id="Seg_12085" s="T502">0.3.h:Th</ta>
            <ta e="T505" id="Seg_12086" s="T504">0.3.h:Th</ta>
            <ta e="T975" id="Seg_12087" s="T507">0.3.h:A</ta>
            <ta e="T976" id="Seg_12088" s="T509">0.2.h:A</ta>
            <ta e="T514" id="Seg_12089" s="T513">0.2.h:A</ta>
            <ta e="T522" id="Seg_12090" s="T521">0.3.h:A</ta>
            <ta e="T980" id="Seg_12091" s="T523">0.3.h:A</ta>
            <ta e="T525" id="Seg_12092" s="T524">0.2.h:A</ta>
            <ta e="T981" id="Seg_12093" s="T526">0.2.h:A</ta>
            <ta e="T532" id="Seg_12094" s="T531">0.3.h:A</ta>
            <ta e="T983" id="Seg_12095" s="T534">0.3.h:A</ta>
            <ta e="T537" id="Seg_12096" s="T536">adv:Time</ta>
            <ta e="T541" id="Seg_12097" s="T540">np.h:Th</ta>
            <ta e="T542" id="Seg_12098" s="T541">np:Com 0.3.h:Poss</ta>
            <ta e="T543" id="Seg_12099" s="T542">0.3.h:Th</ta>
            <ta e="T545" id="Seg_12100" s="T544">np:Time</ta>
            <ta e="T547" id="Seg_12101" s="T546">0.3.h:A</ta>
            <ta e="T549" id="Seg_12102" s="T548">np.h:R 0.3.h:Poss</ta>
            <ta e="T550" id="Seg_12103" s="T549">pro.h:A</ta>
            <ta e="T552" id="Seg_12104" s="T551">adv:G</ta>
            <ta e="T553" id="Seg_12105" s="T552">np.h:G 0.1.h:Poss</ta>
            <ta e="T557" id="Seg_12106" s="T556">0.3.h:A</ta>
            <ta e="T559" id="Seg_12107" s="T558">0.3.h:A</ta>
            <ta e="T560" id="Seg_12108" s="T559">adv:G</ta>
            <ta e="T561" id="Seg_12109" s="T560">0.3.h:A</ta>
            <ta e="T563" id="Seg_12110" s="T562">np:Th</ta>
            <ta e="T564" id="Seg_12111" s="T563">0.3.h:A</ta>
            <ta e="T567" id="Seg_12112" s="T566">np:Th</ta>
            <ta e="T568" id="Seg_12113" s="T567">0.3.h:A</ta>
            <ta e="T570" id="Seg_12114" s="T569">np.h:A</ta>
            <ta e="T571" id="Seg_12115" s="T570">adv:G</ta>
            <ta e="T573" id="Seg_12116" s="T572">np.h:Th 0.3.h:Poss</ta>
            <ta e="T578" id="Seg_12117" s="T577">0.3.h:Th</ta>
            <ta e="T584" id="Seg_12118" s="T583">adv:Time</ta>
            <ta e="T585" id="Seg_12119" s="T584">np:Th</ta>
            <ta e="T586" id="Seg_12120" s="T585">np:G</ta>
            <ta e="T589" id="Seg_12121" s="T588">np:Time</ta>
            <ta e="T591" id="Seg_12122" s="T590">np.h:Th</ta>
            <ta e="T598" id="Seg_12123" s="T597">adv:Time</ta>
            <ta e="T600" id="Seg_12124" s="T599">np.h:A</ta>
            <ta e="T604" id="Seg_12125" s="T603">adv:So</ta>
            <ta e="T608" id="Seg_12126" s="T607">0.3.h:A</ta>
            <ta e="T610" id="Seg_12127" s="T609">np.h:Th</ta>
            <ta e="T615" id="Seg_12128" s="T614">np.h:Th</ta>
            <ta e="T621" id="Seg_12129" s="T620">0.3.h:Th</ta>
            <ta e="T622" id="Seg_12130" s="T621">adv:Time</ta>
            <ta e="T624" id="Seg_12131" s="T623">0.2.h:A</ta>
            <ta e="T626" id="Seg_12132" s="T625">np.h:A</ta>
            <ta e="T628" id="Seg_12133" s="T627">adv:L</ta>
            <ta e="T629" id="Seg_12134" s="T628">np.h:E 0.3.h:Poss</ta>
            <ta e="T631" id="Seg_12135" s="T630">0.3.h:A</ta>
            <ta e="T633" id="Seg_12136" s="T632">np:Th 0.3.h:Poss</ta>
            <ta e="T635" id="Seg_12137" s="T634">0.3.h:A</ta>
            <ta e="T636" id="Seg_12138" s="T635">np:Th 0.3.h:Poss</ta>
            <ta e="T640" id="Seg_12139" s="T639">0.3.h:A</ta>
            <ta e="T644" id="Seg_12140" s="T643">np.h:A</ta>
            <ta e="T649" id="Seg_12141" s="T648">np:Th 0.3.h:Poss</ta>
            <ta e="T653" id="Seg_12142" s="T652">adv:L</ta>
            <ta e="T654" id="Seg_12143" s="T653">pro.h:Poss</ta>
            <ta e="T655" id="Seg_12144" s="T654">adv:L</ta>
            <ta e="T656" id="Seg_12145" s="T655">0.3.h:A</ta>
            <ta e="T658" id="Seg_12146" s="T657">0.3.h:A</ta>
            <ta e="T659" id="Seg_12147" s="T658">pp:L</ta>
            <ta e="T661" id="Seg_12148" s="T660">0.3.h:Th</ta>
            <ta e="T664" id="Seg_12149" s="T663">pp:L</ta>
            <ta e="T667" id="Seg_12150" s="T666">np:Th 0.3.h:Poss</ta>
            <ta e="T668" id="Seg_12151" s="T667">0.3.h:A</ta>
            <ta e="T669" id="Seg_12152" s="T668">np:Th 0.3.h:Poss</ta>
            <ta e="T671" id="Seg_12153" s="T670">0.3.h:A</ta>
            <ta e="T674" id="Seg_12154" s="T673">np:Th 0.3.h:Poss</ta>
            <ta e="T675" id="Seg_12155" s="T674">0.3.h:A</ta>
            <ta e="T677" id="Seg_12156" s="T676">0.3.h:A</ta>
            <ta e="T679" id="Seg_12157" s="T678">0.3.h:A</ta>
            <ta e="T681" id="Seg_12158" s="T680">np.h:G</ta>
            <ta e="T683" id="Seg_12159" s="T682">np:Th 0.2.h:Poss</ta>
            <ta e="T689" id="Seg_12160" s="T688">0.2.h:A</ta>
            <ta e="T691" id="Seg_12161" s="T690">adv:G</ta>
            <ta e="T692" id="Seg_12162" s="T691">0.3.h:A</ta>
            <ta e="T694" id="Seg_12163" s="T693">np:Com</ta>
            <ta e="T695" id="Seg_12164" s="T694">0.3.h:A</ta>
            <ta e="T697" id="Seg_12165" s="T696">0.3.h:Poss</ta>
            <ta e="T698" id="Seg_12166" s="T697">0.3.h:A 0.3.h:P</ta>
            <ta e="T701" id="Seg_12167" s="T700">np.h:P</ta>
            <ta e="T704" id="Seg_12168" s="T703">0.3.h:A</ta>
            <ta e="T705" id="Seg_12169" s="T704">np.h:P</ta>
            <ta e="T708" id="Seg_12170" s="T707">np.h:Th</ta>
            <ta e="T712" id="Seg_12171" s="T711">np.h:Th</ta>
            <ta e="T715" id="Seg_12172" s="T714">adv:G</ta>
            <ta e="T716" id="Seg_12173" s="T715">0.3.h:P</ta>
            <ta e="T718" id="Seg_12174" s="T717">np.h:A</ta>
            <ta e="T720" id="Seg_12175" s="T719">np:Time</ta>
            <ta e="T721" id="Seg_12176" s="T720">np:Ins</ta>
            <ta e="T722" id="Seg_12177" s="T721">0.3.h:A 0.3.h:P</ta>
            <ta e="T723" id="Seg_12178" s="T722">pro.h:A</ta>
            <ta e="T724" id="Seg_12179" s="T723">adv:L</ta>
            <ta e="T725" id="Seg_12180" s="T724">pp:G</ta>
            <ta e="T732" id="Seg_12181" s="T731">adv:Time</ta>
            <ta e="T734" id="Seg_12182" s="T733">adv:G</ta>
            <ta e="T735" id="Seg_12183" s="T734">0.3.h:A 0.3.h:Th</ta>
            <ta e="T736" id="Seg_12184" s="T735">np:G</ta>
            <ta e="T739" id="Seg_12185" s="T738">np:Poss</ta>
            <ta e="T742" id="Seg_12186" s="T741">np:L</ta>
            <ta e="T744" id="Seg_12187" s="T743">0.3.h:A</ta>
            <ta e="T746" id="Seg_12188" s="T745">np.h:Th</ta>
            <ta e="T748" id="Seg_12189" s="T747">adv:G</ta>
            <ta e="T749" id="Seg_12190" s="T748">0.3.h:A</ta>
            <ta e="T751" id="Seg_12191" s="T750">pro.h:Th</ta>
            <ta e="T752" id="Seg_12192" s="T751">adv:G</ta>
            <ta e="T753" id="Seg_12193" s="T752">0.3.h:A</ta>
            <ta e="T755" id="Seg_12194" s="T754">0.3.h:A</ta>
            <ta e="T757" id="Seg_12195" s="T756">np:Th</ta>
            <ta e="T758" id="Seg_12196" s="T757">0.3.h:A</ta>
            <ta e="T760" id="Seg_12197" s="T759">0.3.h:A</ta>
            <ta e="T763" id="Seg_12198" s="T762">0.3.h:Th</ta>
            <ta e="T765" id="Seg_12199" s="T764">np.h:Th 0.3.h:Poss</ta>
            <ta e="T766" id="Seg_12200" s="T765">np:L</ta>
            <ta e="T769" id="Seg_12201" s="T768">adv:Time</ta>
            <ta e="T772" id="Seg_12202" s="T771">0.3.h:Th</ta>
            <ta e="T774" id="Seg_12203" s="T773">0.3.h:Th</ta>
            <ta e="T776" id="Seg_12204" s="T775">np:Time</ta>
            <ta e="T778" id="Seg_12205" s="T777">0.3.h:A</ta>
            <ta e="T784" id="Seg_12206" s="T783">np:P 0.3.h:Poss</ta>
            <ta e="T789" id="Seg_12207" s="T788">np:Th 0.3.h:Poss</ta>
            <ta e="T792" id="Seg_12208" s="T791">np.h:Th</ta>
            <ta e="T800" id="Seg_12209" s="T799">np.h:A</ta>
            <ta e="T803" id="Seg_12210" s="T802">np.h:R 0.3.h:Poss</ta>
            <ta e="T804" id="Seg_12211" s="T803">np.h:Th 0.3.h:Poss</ta>
            <ta e="T805" id="Seg_12212" s="T804">adv:G</ta>
            <ta e="T806" id="Seg_12213" s="T805">0.2.h:A</ta>
            <ta e="T808" id="Seg_12214" s="T807">np:L 0.2.h:Poss</ta>
            <ta e="T809" id="Seg_12215" s="T808">0.2.h:A</ta>
            <ta e="T811" id="Seg_12216" s="T810">0.2.h:A</ta>
            <ta e="T812" id="Seg_12217" s="T811">pro.h:Poss</ta>
            <ta e="T813" id="Seg_12218" s="T812">np:G</ta>
            <ta e="T814" id="Seg_12219" s="T813">adv:Time</ta>
            <ta e="T816" id="Seg_12220" s="T815">adv:L</ta>
            <ta e="T818" id="Seg_12221" s="T817">adv:G</ta>
            <ta e="T822" id="Seg_12222" s="T821">np:G</ta>
            <ta e="T823" id="Seg_12223" s="T822">0.3.h:A</ta>
            <ta e="T831" id="Seg_12224" s="T830">adv:Time</ta>
            <ta e="T833" id="Seg_12225" s="T832">np.h:Th</ta>
            <ta e="T836" id="Seg_12226" s="T835">np:G</ta>
            <ta e="T841" id="Seg_12227" s="T840">np.h:Th</ta>
            <ta e="T844" id="Seg_12228" s="T843">np.h:Th</ta>
            <ta e="T846" id="Seg_12229" s="T845">0.3.h:A</ta>
            <ta e="T847" id="Seg_12230" s="T846">adv:G</ta>
            <ta e="T849" id="Seg_12231" s="T848">adv:G</ta>
            <ta e="T851" id="Seg_12232" s="T850">np.h:Th</ta>
            <ta e="T854" id="Seg_12233" s="T853">0.3.h:A</ta>
            <ta e="T857" id="Seg_12234" s="T856">np:Th 0.3.h:Poss</ta>
            <ta e="T860" id="Seg_12235" s="T859">0.3.h:A</ta>
            <ta e="T862" id="Seg_12236" s="T861">adv:Time</ta>
            <ta e="T864" id="Seg_12237" s="T863">0.3.h:A</ta>
            <ta e="T865" id="Seg_12238" s="T864">adv:L</ta>
            <ta e="T866" id="Seg_12239" s="T865">np.h:R 0.3.h:Poss</ta>
            <ta e="T868" id="Seg_12240" s="T867">0.3.h:A</ta>
            <ta e="T869" id="Seg_12241" s="T868">np:P 0.2.h:Poss</ta>
            <ta e="T871" id="Seg_12242" s="T870">0.2.h:A</ta>
            <ta e="T872" id="Seg_12243" s="T871">pro.h:A</ta>
            <ta e="T873" id="Seg_12244" s="T872">adv:G</ta>
            <ta e="T875" id="Seg_12245" s="T874">np.h:G</ta>
            <ta e="T881" id="Seg_12246" s="T880">0.3.h:Poss</ta>
            <ta e="T882" id="Seg_12247" s="T881">np.h:E</ta>
            <ta e="T884" id="Seg_12248" s="T883">pro:Th</ta>
            <ta e="T885" id="Seg_12249" s="T884">adv:G</ta>
            <ta e="T886" id="Seg_12250" s="T885">0.3.h:A</ta>
            <ta e="T890" id="Seg_12251" s="T889">0.3.h:A</ta>
            <ta e="T893" id="Seg_12252" s="T892">np.h:A 0.3.h:Poss</ta>
            <ta e="T896" id="Seg_12253" s="T895">0.3.h:Poss</ta>
            <ta e="T897" id="Seg_12254" s="T896">0.3.h:Poss</ta>
            <ta e="T898" id="Seg_12255" s="T897">np.h:R</ta>
            <ta e="T901" id="Seg_12256" s="T900">np.h:A</ta>
            <ta e="T903" id="Seg_12257" s="T902">adv:G</ta>
            <ta e="T906" id="Seg_12258" s="T905">0.2.h:A</ta>
            <ta e="T985" id="Seg_12259" s="T912">0.3.h:E</ta>
            <ta e="T914" id="Seg_12260" s="T913">adv:G</ta>
            <ta e="T986" id="Seg_12261" s="T914">0.2.h:A</ta>
            <ta e="T987" id="Seg_12262" s="T916">np:G </ta>
            <ta e="T918" id="Seg_12263" s="T917">adv:G</ta>
            <ta e="T921" id="Seg_12264" s="T920">pro.h:A</ta>
            <ta e="T923" id="Seg_12265" s="T922">np.h:Poss 0.3.h:Poss</ta>
            <ta e="T924" id="Seg_12266" s="T923">np:G</ta>
            <ta e="T927" id="Seg_12267" s="T926">np:Th</ta>
            <ta e="T930" id="Seg_12268" s="T929">0.3.h:A</ta>
            <ta e="T932" id="Seg_12269" s="T931">0.3.h:A</ta>
            <ta e="T934" id="Seg_12270" s="T933">0.3.h:A</ta>
            <ta e="T935" id="Seg_12271" s="T934">adv:G</ta>
            <ta e="T936" id="Seg_12272" s="T935">0.3.h:A</ta>
            <ta e="T938" id="Seg_12273" s="T937">np.h:A</ta>
            <ta e="T940" id="Seg_12274" s="T939">adv:L</ta>
            <ta e="T941" id="Seg_12275" s="T940">np:G</ta>
            <ta e="T944" id="Seg_12276" s="T943">0.3.h:P</ta>
            <ta e="T947" id="Seg_12277" s="T946">np.h:Th</ta>
            <ta e="T951" id="Seg_12278" s="T950">np.h:Th</ta>
            <ta e="T954" id="Seg_12279" s="T953">np.h:Th</ta>
            <ta e="T955" id="Seg_12280" s="T954">adv:L</ta>
            <ta e="T958" id="Seg_12281" s="T957">np:Th</ta>
            <ta e="T959" id="Seg_12282" s="T958">0.2.h:A</ta>
            <ta e="T963" id="Seg_12283" s="T962">np:Th</ta>
            <ta e="T964" id="Seg_12284" s="T963">0.2.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_12285" s="T3">np.h:S</ta>
            <ta e="T5" id="Seg_12286" s="T4">v:pred</ta>
            <ta e="T12" id="Seg_12287" s="T11">0.1.h:S v:pred</ta>
            <ta e="T13" id="Seg_12288" s="T12">pro:O</ta>
            <ta e="T14" id="Seg_12289" s="T13">pro:S</ta>
            <ta e="T15" id="Seg_12290" s="T14">n:pred</ta>
            <ta e="T16" id="Seg_12291" s="T15">pro:S</ta>
            <ta e="T17" id="Seg_12292" s="T16">n:pred</ta>
            <ta e="T19" id="Seg_12293" s="T18">0.3:S n:pred</ta>
            <ta e="T30" id="Seg_12294" s="T29">0.3.h:S v:pred</ta>
            <ta e="T32" id="Seg_12295" s="T31">pro.h:S</ta>
            <ta e="T33" id="Seg_12296" s="T32">np:O</ta>
            <ta e="T971" id="Seg_12297" s="T33">v:pred</ta>
            <ta e="T972" id="Seg_12298" s="T34">0.2.h:S v:pred 0.3:O</ta>
            <ta e="T973" id="Seg_12299" s="T36">0.2.h:S v:pred</ta>
            <ta e="T40" id="Seg_12300" s="T39">np.h:S</ta>
            <ta e="T41" id="Seg_12301" s="T40">v:pred</ta>
            <ta e="T42" id="Seg_12302" s="T41">0.3.h:S v:pred</ta>
            <ta e="T45" id="Seg_12303" s="T44">0.3.h:S n:pred</ta>
            <ta e="T48" id="Seg_12304" s="T47">np:S</ta>
            <ta e="T53" id="Seg_12305" s="T52">0.3.h:S v:pred</ta>
            <ta e="T60" id="Seg_12306" s="T59">0.3.h:S v:pred</ta>
            <ta e="T62" id="Seg_12307" s="T61">0.3.h:S v:pred</ta>
            <ta e="T63" id="Seg_12308" s="T62">np:S</ta>
            <ta e="T65" id="Seg_12309" s="T64">np:S</ta>
            <ta e="T70" id="Seg_12310" s="T69">np.h:S</ta>
            <ta e="T71" id="Seg_12311" s="T70">v:pred</ta>
            <ta e="T73" id="Seg_12312" s="T72">pro.h:S</ta>
            <ta e="T74" id="Seg_12313" s="T73">v:pred</ta>
            <ta e="T77" id="Seg_12314" s="T76">np.h:S</ta>
            <ta e="T78" id="Seg_12315" s="T77">v:pred</ta>
            <ta e="T85" id="Seg_12316" s="T84">0.3.h:S v:pred</ta>
            <ta e="T95" id="Seg_12317" s="T94">np.h:S</ta>
            <ta e="T96" id="Seg_12318" s="T95">v:pred</ta>
            <ta e="T98" id="Seg_12319" s="T97">np.h:S</ta>
            <ta e="T100" id="Seg_12320" s="T99">v:pred</ta>
            <ta e="T103" id="Seg_12321" s="T102">0.3.h:S v:pred</ta>
            <ta e="T108" id="Seg_12322" s="T107">np.h:S</ta>
            <ta e="T110" id="Seg_12323" s="T109">v:pred</ta>
            <ta e="T112" id="Seg_12324" s="T111">0.3.h:S v:pred</ta>
            <ta e="T119" id="Seg_12325" s="T118">np.h:S</ta>
            <ta e="T122" id="Seg_12326" s="T121">v:pred</ta>
            <ta e="T125" id="Seg_12327" s="T124">0.3.h:S v:pred</ta>
            <ta e="T126" id="Seg_12328" s="T125">np.h:S</ta>
            <ta e="T127" id="Seg_12329" s="T126">v:pred</ta>
            <ta e="T129" id="Seg_12330" s="T128">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T133" id="Seg_12331" s="T132">np.h:S</ta>
            <ta e="T134" id="Seg_12332" s="T133">v:pred</ta>
            <ta e="T135" id="Seg_12333" s="T134">np.h:S</ta>
            <ta e="T138" id="Seg_12334" s="T137">v:pred</ta>
            <ta e="T142" id="Seg_12335" s="T141">np.h:S</ta>
            <ta e="T145" id="Seg_12336" s="T144">v:pred</ta>
            <ta e="T147" id="Seg_12337" s="T146">0.3.h:S v:pred</ta>
            <ta e="T150" id="Seg_12338" s="T149">np.h:S</ta>
            <ta e="T154" id="Seg_12339" s="T153">v:pred</ta>
            <ta e="T157" id="Seg_12340" s="T156">np.h:O</ta>
            <ta e="T171" id="Seg_12341" s="T170">adj:pred</ta>
            <ta e="T172" id="Seg_12342" s="T171">np.h:S</ta>
            <ta e="T173" id="Seg_12343" s="T172">cop</ta>
            <ta e="T175" id="Seg_12344" s="T174">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T177" id="Seg_12345" s="T176">v:pred</ta>
            <ta e="T180" id="Seg_12346" s="T179">v:pred</ta>
            <ta e="T182" id="Seg_12347" s="T181">np.h:S</ta>
            <ta e="T184" id="Seg_12348" s="T183">0.3.h:S v:pred</ta>
            <ta e="T185" id="Seg_12349" s="T184">0.3.h:S v:pred</ta>
            <ta e="T188" id="Seg_12350" s="T187">0.3.h:S v:pred</ta>
            <ta e="T190" id="Seg_12351" s="T188">s:temp</ta>
            <ta e="T192" id="Seg_12352" s="T191">np.h:S</ta>
            <ta e="T198" id="Seg_12353" s="T197">np.h:S</ta>
            <ta e="T199" id="Seg_12354" s="T198">v:pred</ta>
            <ta e="T200" id="Seg_12355" s="T199">adj:pred</ta>
            <ta e="T201" id="Seg_12356" s="T200">np.h:S</ta>
            <ta e="T207" id="Seg_12357" s="T206">np.h:S</ta>
            <ta e="T212" id="Seg_12358" s="T211">np.h:S</ta>
            <ta e="T219" id="Seg_12359" s="T218">np.h:S</ta>
            <ta e="T220" id="Seg_12360" s="T219">v:pred</ta>
            <ta e="T224" id="Seg_12361" s="T223">np:O</ta>
            <ta e="T226" id="Seg_12362" s="T225">0.3.h:S v:pred</ta>
            <ta e="T227" id="Seg_12363" s="T226">0.2.h:S v:pred</ta>
            <ta e="T229" id="Seg_12364" s="T228">v:pred</ta>
            <ta e="T231" id="Seg_12365" s="T230">np.h:S</ta>
            <ta e="T233" id="Seg_12366" s="T232">np.h:S</ta>
            <ta e="T234" id="Seg_12367" s="T233">v:pred</ta>
            <ta e="T241" id="Seg_12368" s="T240">np:S</ta>
            <ta e="T245" id="Seg_12369" s="T241">s:purp</ta>
            <ta e="T247" id="Seg_12370" s="T246">v:pred</ta>
            <ta e="T249" id="Seg_12371" s="T248">np.h:S</ta>
            <ta e="T250" id="Seg_12372" s="T249">np:S</ta>
            <ta e="T251" id="Seg_12373" s="T250">v:pred</ta>
            <ta e="T253" id="Seg_12374" s="T252">0.2.h:S v:pred</ta>
            <ta e="T254" id="Seg_12375" s="T253">s:temp</ta>
            <ta e="T255" id="Seg_12376" s="T254">0.3.h:S v:pred</ta>
            <ta e="T260" id="Seg_12377" s="T259">np.h:O</ta>
            <ta e="T263" id="Seg_12378" s="T262">0.3.h:S v:pred</ta>
            <ta e="T266" id="Seg_12379" s="T265">0.3.h:S v:pred</ta>
            <ta e="T267" id="Seg_12380" s="T266">pro.h:S</ta>
            <ta e="T274" id="Seg_12381" s="T273">v:pred</ta>
            <ta e="T277" id="Seg_12382" s="T276">0.2.h:S v:pred 0.3:O</ta>
            <ta e="T280" id="Seg_12383" s="T279">0.3.h:S v:pred</ta>
            <ta e="T289" id="Seg_12384" s="T288">0.3.h:S v:pred</ta>
            <ta e="T292" id="Seg_12385" s="T291">np.h:S</ta>
            <ta e="T296" id="Seg_12386" s="T295">v:pred</ta>
            <ta e="T300" id="Seg_12387" s="T296">s:purp</ta>
            <ta e="T302" id="Seg_12388" s="T301">np.h:S</ta>
            <ta e="T307" id="Seg_12389" s="T306">np:O</ta>
            <ta e="T308" id="Seg_12390" s="T307">v:pred</ta>
            <ta e="T311" id="Seg_12391" s="T310">np:S</ta>
            <ta e="T314" id="Seg_12392" s="T313">np:S</ta>
            <ta e="T317" id="Seg_12393" s="T316">pro:S</ta>
            <ta e="T318" id="Seg_12394" s="T317">pro:O</ta>
            <ta e="T319" id="Seg_12395" s="T318">0.3.h:S v:pred</ta>
            <ta e="T325" id="Seg_12396" s="T319">s:temp</ta>
            <ta e="T327" id="Seg_12397" s="T326">pro:O</ta>
            <ta e="T328" id="Seg_12398" s="T327">0.3.h:S v:pred</ta>
            <ta e="T333" id="Seg_12399" s="T332">v:pred</ta>
            <ta e="T336" id="Seg_12400" s="T335">np.h:S</ta>
            <ta e="T340" id="Seg_12401" s="T339">0.1.h:S v:pred</ta>
            <ta e="T342" id="Seg_12402" s="T341">pro.h:S</ta>
            <ta e="T346" id="Seg_12403" s="T345">v:pred</ta>
            <ta e="T350" id="Seg_12404" s="T349">0.3:S v:pred</ta>
            <ta e="T352" id="Seg_12405" s="T351">np.h:S</ta>
            <ta e="T354" id="Seg_12406" s="T353">v:pred</ta>
            <ta e="T363" id="Seg_12407" s="T362">np:S</ta>
            <ta e="T364" id="Seg_12408" s="T363">v:pred</ta>
            <ta e="T369" id="Seg_12409" s="T365">s:rel</ta>
            <ta e="T371" id="Seg_12410" s="T370">np:S</ta>
            <ta e="T372" id="Seg_12411" s="T371">v:pred</ta>
            <ta e="T375" id="Seg_12412" s="T374">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T377" id="Seg_12413" s="T376">np.h:S</ta>
            <ta e="T378" id="Seg_12414" s="T377">v:pred</ta>
            <ta e="T380" id="Seg_12415" s="T379">np:O</ta>
            <ta e="T381" id="Seg_12416" s="T380">0.3.h:S v:pred</ta>
            <ta e="T391" id="Seg_12417" s="T390">0.1.h:S v:pred</ta>
            <ta e="T393" id="Seg_12418" s="T392">np.h:S</ta>
            <ta e="T395" id="Seg_12419" s="T394">v:pred</ta>
            <ta e="T398" id="Seg_12420" s="T397">0.3.h:S v:pred</ta>
            <ta e="T402" id="Seg_12421" s="T401">0.2.h:S v:pred</ta>
            <ta e="T403" id="Seg_12422" s="T402">np:O</ta>
            <ta e="T405" id="Seg_12423" s="T404">np:S</ta>
            <ta e="T406" id="Seg_12424" s="T405">v:pred</ta>
            <ta e="T411" id="Seg_12425" s="T410">np:S</ta>
            <ta e="T412" id="Seg_12426" s="T411">v:pred</ta>
            <ta e="T414" id="Seg_12427" s="T413">0.2.h:S v:pred 0.3:O</ta>
            <ta e="T421" id="Seg_12428" s="T420">np.h:S</ta>
            <ta e="T422" id="Seg_12429" s="T421">pro.h:O</ta>
            <ta e="T423" id="Seg_12430" s="T422">v:pred</ta>
            <ta e="T426" id="Seg_12431" s="T425">pro.h:S</ta>
            <ta e="T429" id="Seg_12432" s="T428">v:pred</ta>
            <ta e="T430" id="Seg_12433" s="T429">np:O</ta>
            <ta e="T434" id="Seg_12434" s="T433">np:S</ta>
            <ta e="T435" id="Seg_12435" s="T434">v:pred</ta>
            <ta e="T439" id="Seg_12436" s="T438">0.1.h:S v:pred</ta>
            <ta e="T441" id="Seg_12437" s="T440">np:O</ta>
            <ta e="T442" id="Seg_12438" s="T441">0.3.h:S v:pred</ta>
            <ta e="T443" id="Seg_12439" s="T442">0.3:S v:pred</ta>
            <ta e="T444" id="Seg_12440" s="T443">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T445" id="Seg_12441" s="T444">0.2.h:S v:pred</ta>
            <ta e="T446" id="Seg_12442" s="T445">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T449" id="Seg_12443" s="T448">0.3.h:S v:pred</ta>
            <ta e="T453" id="Seg_12444" s="T452">v:pred</ta>
            <ta e="T454" id="Seg_12445" s="T453">np.h:S</ta>
            <ta e="T461" id="Seg_12446" s="T460">np.h:S</ta>
            <ta e="T463" id="Seg_12447" s="T461">s:purp</ta>
            <ta e="T464" id="Seg_12448" s="T463">np.h:S</ta>
            <ta e="T470" id="Seg_12449" s="T469">0.3.h:S v:pred</ta>
            <ta e="T472" id="Seg_12450" s="T471">np:O</ta>
            <ta e="T474" id="Seg_12451" s="T473">0.3.h:S v:pred</ta>
            <ta e="T481" id="Seg_12452" s="T480">0.3.h:S v:pred</ta>
            <ta e="T483" id="Seg_12453" s="T482">0.3.h:S v:pred</ta>
            <ta e="T490" id="Seg_12454" s="T489">0.3.h:S v:pred</ta>
            <ta e="T493" id="Seg_12455" s="T492">0.3.h:S v:pred</ta>
            <ta e="T500" id="Seg_12456" s="T499">np:S</ta>
            <ta e="T501" id="Seg_12457" s="T500">v:pred</ta>
            <ta e="T503" id="Seg_12458" s="T502">0.3.h:S n:pred</ta>
            <ta e="T505" id="Seg_12459" s="T504">0.3.h:S n:pred</ta>
            <ta e="T975" id="Seg_12460" s="T507">0.3.h:S v:pred</ta>
            <ta e="T976" id="Seg_12461" s="T509">0.2.h:S v:pred</ta>
            <ta e="T514" id="Seg_12462" s="T513">0.2.h:S v:pred</ta>
            <ta e="T522" id="Seg_12463" s="T521">0.3.h:S v:pred</ta>
            <ta e="T980" id="Seg_12464" s="T523">0.3.h:S v:pred</ta>
            <ta e="T525" id="Seg_12465" s="T524">0.2.h:S v:pred</ta>
            <ta e="T981" id="Seg_12466" s="T526">0.2.h:S v:pred</ta>
            <ta e="T532" id="Seg_12467" s="T531">0.3.h:S v:pred</ta>
            <ta e="T983" id="Seg_12468" s="T534">0.3.h:S v:pred</ta>
            <ta e="T539" id="Seg_12469" s="T538">v:pred</ta>
            <ta e="T541" id="Seg_12470" s="T540">np.h:S</ta>
            <ta e="T543" id="Seg_12471" s="T542">0.3.h:S v:pred</ta>
            <ta e="T547" id="Seg_12472" s="T546">0.3.h:S v:pred</ta>
            <ta e="T550" id="Seg_12473" s="T549">pro.h:S</ta>
            <ta e="T554" id="Seg_12474" s="T553">v:pred</ta>
            <ta e="T557" id="Seg_12475" s="T556">0.3.h:S v:pred</ta>
            <ta e="T559" id="Seg_12476" s="T558">0.3.h:S v:pred</ta>
            <ta e="T561" id="Seg_12477" s="T560">0.3.h:S v:pred</ta>
            <ta e="T564" id="Seg_12478" s="T561">s:temp</ta>
            <ta e="T567" id="Seg_12479" s="T566">np:O</ta>
            <ta e="T568" id="Seg_12480" s="T567">0.3.h:S v:pred</ta>
            <ta e="T570" id="Seg_12481" s="T569">np.h:S</ta>
            <ta e="T572" id="Seg_12482" s="T571">v:pred</ta>
            <ta e="T573" id="Seg_12483" s="T572">np.h:S</ta>
            <ta e="T575" id="Seg_12484" s="T574">v:pred</ta>
            <ta e="T578" id="Seg_12485" s="T577">0.3.h:S v:pred</ta>
            <ta e="T585" id="Seg_12486" s="T584">np:S</ta>
            <ta e="T588" id="Seg_12487" s="T587">v:pred</ta>
            <ta e="T591" id="Seg_12488" s="T590">np.h:S</ta>
            <ta e="T593" id="Seg_12489" s="T592">v:pred</ta>
            <ta e="T600" id="Seg_12490" s="T599">np.h:S</ta>
            <ta e="T603" id="Seg_12491" s="T602">v:pred</ta>
            <ta e="T608" id="Seg_12492" s="T607">0.3.h:S v:pred</ta>
            <ta e="T610" id="Seg_12493" s="T609">np.h:S</ta>
            <ta e="T613" id="Seg_12494" s="T611">s:rel</ta>
            <ta e="T615" id="Seg_12495" s="T614">np.h:S</ta>
            <ta e="T617" id="Seg_12496" s="T616">adj:pred</ta>
            <ta e="T618" id="Seg_12497" s="T617">cop</ta>
            <ta e="T620" id="Seg_12498" s="T619">n:pred</ta>
            <ta e="T621" id="Seg_12499" s="T620">0.3.h:S cop</ta>
            <ta e="T624" id="Seg_12500" s="T623">0.2.h:S v:pred</ta>
            <ta e="T626" id="Seg_12501" s="T625">np.h:S</ta>
            <ta e="T627" id="Seg_12502" s="T626">v:pred</ta>
            <ta e="T629" id="Seg_12503" s="T628">np.h:S</ta>
            <ta e="T630" id="Seg_12504" s="T629">v:pred</ta>
            <ta e="T631" id="Seg_12505" s="T630">0.3.h:S v:pred</ta>
            <ta e="T633" id="Seg_12506" s="T632">np:O</ta>
            <ta e="T635" id="Seg_12507" s="T634">0.3.h:S v:pred</ta>
            <ta e="T636" id="Seg_12508" s="T635">np:O</ta>
            <ta e="T640" id="Seg_12509" s="T639">0.3.h:S v:pred</ta>
            <ta e="T644" id="Seg_12510" s="T643">np.h:S</ta>
            <ta e="T647" id="Seg_12511" s="T646">v:pred</ta>
            <ta e="T649" id="Seg_12512" s="T648">np:S</ta>
            <ta e="T651" id="Seg_12513" s="T650">v:pred</ta>
            <ta e="T656" id="Seg_12514" s="T655">0.3.h:S v:pred</ta>
            <ta e="T658" id="Seg_12515" s="T657">0.3.h:S v:pred</ta>
            <ta e="T661" id="Seg_12516" s="T658">s:temp</ta>
            <ta e="T667" id="Seg_12517" s="T666">np:O</ta>
            <ta e="T668" id="Seg_12518" s="T667">0.3.h:S v:pred</ta>
            <ta e="T669" id="Seg_12519" s="T668">np:O</ta>
            <ta e="T671" id="Seg_12520" s="T670">0.3.h:S v:pred</ta>
            <ta e="T674" id="Seg_12521" s="T673">np:O</ta>
            <ta e="T675" id="Seg_12522" s="T674">0.3.h:S v:pred</ta>
            <ta e="T677" id="Seg_12523" s="T676">0.3.h:S v:pred</ta>
            <ta e="T681" id="Seg_12524" s="T677">s:temp</ta>
            <ta e="T683" id="Seg_12525" s="T682">np:S</ta>
            <ta e="T688" id="Seg_12526" s="T687">n:pred</ta>
            <ta e="T689" id="Seg_12527" s="T688">0.2.h:S v:pred</ta>
            <ta e="T692" id="Seg_12528" s="T691">0.3.h:S v:pred</ta>
            <ta e="T695" id="Seg_12529" s="T694">0.3.h:S v:pred</ta>
            <ta e="T698" id="Seg_12530" s="T697">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T701" id="Seg_12531" s="T700">np.h:O</ta>
            <ta e="T704" id="Seg_12532" s="T703">0.3.h:S v:pred</ta>
            <ta e="T705" id="Seg_12533" s="T704">np.h:S</ta>
            <ta e="T706" id="Seg_12534" s="T705">v:pred</ta>
            <ta e="T708" id="Seg_12535" s="T707">np.h:S</ta>
            <ta e="T709" id="Seg_12536" s="T708">s:adv</ta>
            <ta e="T710" id="Seg_12537" s="T709">v:pred</ta>
            <ta e="T712" id="Seg_12538" s="T711">np.h:S</ta>
            <ta e="T713" id="Seg_12539" s="T712">s:adv</ta>
            <ta e="T714" id="Seg_12540" s="T713">v:pred</ta>
            <ta e="T716" id="Seg_12541" s="T715">v:pred 0.3.h:O</ta>
            <ta e="T718" id="Seg_12542" s="T717">np.h:S</ta>
            <ta e="T722" id="Seg_12543" s="T721">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T723" id="Seg_12544" s="T722">pro.h:S</ta>
            <ta e="T727" id="Seg_12545" s="T726">v:pred</ta>
            <ta e="T735" id="Seg_12546" s="T734">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T744" id="Seg_12547" s="T743">0.3.h:S v:pred</ta>
            <ta e="T746" id="Seg_12548" s="T745">np.h:O</ta>
            <ta e="T749" id="Seg_12549" s="T748">0.3.h:S v:pred</ta>
            <ta e="T751" id="Seg_12550" s="T750">pro.h:O</ta>
            <ta e="T753" id="Seg_12551" s="T752">0.3.h:S v:pred</ta>
            <ta e="T755" id="Seg_12552" s="T754">0.3.h:S v:pred</ta>
            <ta e="T757" id="Seg_12553" s="T756">np:O</ta>
            <ta e="T758" id="Seg_12554" s="T757">0.3.h:S v:pred</ta>
            <ta e="T760" id="Seg_12555" s="T758">s:temp</ta>
            <ta e="T763" id="Seg_12556" s="T762">0.3.h:S v:pred</ta>
            <ta e="T765" id="Seg_12557" s="T764">np.h:S</ta>
            <ta e="T768" id="Seg_12558" s="T767">v:pred</ta>
            <ta e="T772" id="Seg_12559" s="T771">0.3.h:S v:pred</ta>
            <ta e="T774" id="Seg_12560" s="T773">0.3.h:S v:pred</ta>
            <ta e="T778" id="Seg_12561" s="T777">0.3.h:S v:pred</ta>
            <ta e="T784" id="Seg_12562" s="T783">np:S</ta>
            <ta e="T786" id="Seg_12563" s="T785">v:pred</ta>
            <ta e="T789" id="Seg_12564" s="T788">np:S</ta>
            <ta e="T792" id="Seg_12565" s="T791">np.h:S</ta>
            <ta e="T795" id="Seg_12566" s="T794">adj:pred</ta>
            <ta e="T796" id="Seg_12567" s="T795">cop</ta>
            <ta e="T798" id="Seg_12568" s="T797">v:pred</ta>
            <ta e="T800" id="Seg_12569" s="T799">np.h:S</ta>
            <ta e="T804" id="Seg_12570" s="T803">np.h:O</ta>
            <ta e="T806" id="Seg_12571" s="T805">0.2.h:S v:pred</ta>
            <ta e="T809" id="Seg_12572" s="T807">s:temp</ta>
            <ta e="T811" id="Seg_12573" s="T810">s:temp</ta>
            <ta e="T823" id="Seg_12574" s="T822">0.3.h:S v:pred</ta>
            <ta e="T833" id="Seg_12575" s="T832">np.h:S</ta>
            <ta e="T836" id="Seg_12576" s="T833">s:rel</ta>
            <ta e="T840" id="Seg_12577" s="T839">s:temp</ta>
            <ta e="T841" id="Seg_12578" s="T840">np.h:O</ta>
            <ta e="T844" id="Seg_12579" s="T843">np.h:O</ta>
            <ta e="T846" id="Seg_12580" s="T845">0.3.h:S v:pred</ta>
            <ta e="T851" id="Seg_12581" s="T850">np.h:O</ta>
            <ta e="T853" id="Seg_12582" s="T852">s:temp</ta>
            <ta e="T854" id="Seg_12583" s="T853">0.3.h:S v:pred</ta>
            <ta e="T857" id="Seg_12584" s="T856">np:S</ta>
            <ta e="T860" id="Seg_12585" s="T859">0.3.h:S v:pred</ta>
            <ta e="T864" id="Seg_12586" s="T863">0.3.h:S v:pred</ta>
            <ta e="T868" id="Seg_12587" s="T867">0.3.h:S v:pred</ta>
            <ta e="T869" id="Seg_12588" s="T868">np:O</ta>
            <ta e="T871" id="Seg_12589" s="T870">0.2.h:S v:pred</ta>
            <ta e="T872" id="Seg_12590" s="T871">pro.h:S</ta>
            <ta e="T874" id="Seg_12591" s="T873">v:pred</ta>
            <ta e="T877" id="Seg_12592" s="T876">v:pred</ta>
            <ta e="T882" id="Seg_12593" s="T881">np.h:S</ta>
            <ta e="T884" id="Seg_12594" s="T883">pro:O</ta>
            <ta e="T886" id="Seg_12595" s="T885">0.3.h:S v:pred</ta>
            <ta e="T890" id="Seg_12596" s="T889">0.3.h:S v:pred</ta>
            <ta e="T893" id="Seg_12597" s="T892">np.h:S</ta>
            <ta e="T895" id="Seg_12598" s="T894">v:pred</ta>
            <ta e="T901" id="Seg_12599" s="T900">np.h:S</ta>
            <ta e="T904" id="Seg_12600" s="T903">v:pred</ta>
            <ta e="T906" id="Seg_12601" s="T905">0.2.h:S v:pred</ta>
            <ta e="T985" id="Seg_12602" s="T912">0.3.h:S v:pred</ta>
            <ta e="T986" id="Seg_12603" s="T914">0.2.h:S v:pred</ta>
            <ta e="T921" id="Seg_12604" s="T920">pro.h:S</ta>
            <ta e="T926" id="Seg_12605" s="T925">v:pred</ta>
            <ta e="T930" id="Seg_12606" s="T928">s:temp</ta>
            <ta e="T932" id="Seg_12607" s="T931">0.3.h:S v:pred</ta>
            <ta e="T934" id="Seg_12608" s="T933">0.3.h:S v:pred</ta>
            <ta e="T936" id="Seg_12609" s="T935">0.3.h:S v:pred</ta>
            <ta e="T938" id="Seg_12610" s="T937">np.h:S</ta>
            <ta e="T942" id="Seg_12611" s="T941">v:pred</ta>
            <ta e="T944" id="Seg_12612" s="T943">0.3.h:S v:pred</ta>
            <ta e="T947" id="Seg_12613" s="T946">np.h:S</ta>
            <ta e="T949" id="Seg_12614" s="T948">v:pred</ta>
            <ta e="T951" id="Seg_12615" s="T950">np.h:S</ta>
            <ta e="T954" id="Seg_12616" s="T953">np.h:S</ta>
            <ta e="T988" id="Seg_12617" s="T955">v:pred</ta>
            <ta e="T958" id="Seg_12618" s="T957">np:O</ta>
            <ta e="T959" id="Seg_12619" s="T958">0.2.h:S v:pred</ta>
            <ta e="T963" id="Seg_12620" s="T962">np:O</ta>
            <ta e="T964" id="Seg_12621" s="T963">0.2.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T168" id="Seg_12622" s="T167">RUS:cult</ta>
            <ta e="T270" id="Seg_12623" s="T269">RUS:cult</ta>
            <ta e="T272" id="Seg_12624" s="T271">RUS:gram</ta>
            <ta e="T358" id="Seg_12625" s="T357">RUS:cult</ta>
            <ta e="T416" id="Seg_12626" s="T415">RUS:gram</ta>
            <ta e="T428" id="Seg_12627" s="T427">RUS:cult</ta>
            <ta e="T533" id="Seg_12628" s="T532">RUS:mod</ta>
            <ta e="T694" id="Seg_12629" s="T693">RUS:core?</ta>
            <ta e="T721" id="Seg_12630" s="T720">RUS:core?</ta>
            <ta e="T731" id="Seg_12631" s="T730">RUS:cult</ta>
            <ta e="T794" id="Seg_12632" s="T793">RUS:gram</ta>
            <ta e="T987" id="Seg_12633" s="T916">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T270" id="Seg_12634" s="T269">finCdel Vsub</ta>
            <ta e="T272" id="Seg_12635" s="T271">finVdel</ta>
            <ta e="T533" id="Seg_12636" s="T532">Csub</ta>
            <ta e="T694" id="Seg_12637" s="T693">Csub</ta>
            <ta e="T721" id="Seg_12638" s="T720">Csub</ta>
            <ta e="T731" id="Seg_12639" s="T730">Csub</ta>
            <ta e="T794" id="Seg_12640" s="T793">Csub Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T168" id="Seg_12641" s="T167">dir:infl</ta>
            <ta e="T270" id="Seg_12642" s="T269">indir:bare</ta>
            <ta e="T272" id="Seg_12643" s="T271">dir:bare</ta>
            <ta e="T358" id="Seg_12644" s="T357">dir:infl</ta>
            <ta e="T428" id="Seg_12645" s="T427">dir:bare</ta>
            <ta e="T533" id="Seg_12646" s="T532">dir:bare</ta>
            <ta e="T694" id="Seg_12647" s="T693">dir:infl</ta>
            <ta e="T721" id="Seg_12648" s="T720">dir:infl</ta>
            <ta e="T987" id="Seg_12649" s="T916">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS">
            <ta e="T968" id="Seg_12650" s="T0">RUS:ext</ta>
            <ta e="T969" id="Seg_12651" s="T19">RUS:ext</ta>
            <ta e="T515" id="Seg_12652" s="T514">RUS:ext</ta>
            <ta e="T978" id="Seg_12653" s="T515">RUS:ext</ta>
            <ta e="T979" id="Seg_12654" s="T518">RUS:int.ins</ta>
            <ta e="T965" id="Seg_12655" s="T964">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T968" id="Seg_12656" s="T0">[KuAI:] Лентяю всегда праздник.</ta>
            <ta e="T6" id="Seg_12657" s="T968">[NEP:] Был один ребёнок.</ta>
            <ta e="T13" id="Seg_12658" s="T6">Это хоть как-то (?), ведь я это всегда рассказываю. </ta>
            <ta e="T19" id="Seg_12659" s="T13">Это предание, предание, не сказка.</ta>
            <ta e="T969" id="Seg_12660" s="T19">[KuAI:] Подождите, пусть она подождёт, я попробую, посмотрю, проверю.</ta>
            <ta e="T970" id="Seg_12661" s="T969">[NN:] Под тобой… селькупы туда…</ta>
            <ta e="T971" id="Seg_12662" s="T970">[NN:] Это вот тебя укажет/(тебя увидит), ты разговор приостанавливай.</ta>
            <ta e="T972" id="Seg_12663" s="T971">[NN:] Сними [какой-то предмет одежды].</ta>
            <ta e="T973" id="Seg_12664" s="T972">[NN:] Медленно говори.</ta>
            <ta e="T41" id="Seg_12665" s="T973">[NEP:] Этот старик жил.</ta>
            <ta e="T42" id="Seg_12666" s="T41">Заболел.</ta>
            <ta e="T45" id="Seg_12667" s="T42">Этот старик – селькуп.</ta>
            <ta e="T53" id="Seg_12668" s="T45">На этой стороне [реки] лабаз, на другой стороне реки вниз по течению (/к северу?) жил. </ta>
            <ta e="T992" id="Seg_12669" s="T53">(Как будто?) он на безлесой земле (?).</ta>
            <ta e="T59" id="Seg_12670" s="T992">[?]</ta>
            <ta e="T61" id="Seg_12671" s="T59">Жил тот.</ta>
            <ta e="T62" id="Seg_12672" s="T61">Заболел.</ta>
            <ta e="T68" id="Seg_12673" s="T62">Сын, один сын, без товарища, без никого [в семье], друг (/половина?). </ta>
            <ta e="T74" id="Seg_12674" s="T68">Ни дочки у него нет, никого у него нет.</ta>
            <ta e="T83" id="Seg_12675" s="T74">Потом этот отец заболел у этого парня, этого парня. </ta>
            <ta e="T90" id="Seg_12676" s="T83">Там живёт, вроде, у своего отца, у своей матери в пологе.</ta>
            <ta e="T96" id="Seg_12677" s="T90">С той стороны (/оттуда?) те два брата-ненца были.</ta>
            <ta e="T101" id="Seg_12678" s="T96">Ненец-старик куда денется?</ta>
            <ta e="T110" id="Seg_12679" s="T101">Или жил, (куда же?), этот старик умер. </ta>
            <ta e="T122" id="Seg_12680" s="T110">Умер он; потом этот парень… ненец-старик куда денется?</ta>
            <ta e="T125" id="Seg_12681" s="T122">Туда к брату пришел. </ta>
            <ta e="T127" id="Seg_12682" s="T125">Старик умер.</ta>
            <ta e="T129" id="Seg_12683" s="T127">Похоронили.</ta>
            <ta e="T138" id="Seg_12684" s="T129">Потом его жена, эта его жена заболела, и жена тоже умерла.</ta>
            <ta e="T145" id="Seg_12685" s="T138">Этот сын старика со старухой без родни, без никого остался.</ta>
            <ta e="T147" id="Seg_12686" s="T145">Куда он денется?</ta>
            <ta e="T157" id="Seg_12687" s="T147">Этот ненец-старик потом на другую сторону переправил этого парня-сироту.</ta>
            <ta e="T170" id="Seg_12688" s="T157">Большой сын… всякий… как Гена с кем-то. [?]</ta>
            <ta e="T173" id="Seg_12689" s="T170">Такой сын был.</ta>
            <ta e="T177" id="Seg_12690" s="T173">На другую сторону его переправил, оленей у него нету.</ta>
            <ta e="T182" id="Seg_12691" s="T177">Без оленей ногами (пешком) жили эти отец с сыном.</ta>
            <ta e="T188" id="Seg_12692" s="T182">Там жили, жили, потом они на другую сторону переправились.</ta>
            <ta e="T199" id="Seg_12693" s="T188">На другую сторону переправившись, этот парень [пришёл] в дом старика-ненца, у старика-ненца детей нет.</ta>
            <ta e="T201" id="Seg_12694" s="T199">Жена у него бездетеная.</ta>
            <ta e="T208" id="Seg_12695" s="T201">У того его среднего брата одна дочь единственная.</ta>
            <ta e="T212" id="Seg_12696" s="T208">С севера/снизу по течению реки сюда два ненца [пришли].</ta>
            <ta e="T215" id="Seg_12697" s="T212">Он/они(?) с(?) земли без деревьев.</ta>
            <ta e="T226" id="Seg_12698" s="T215">Пони-старик вот есть, оленей всего племени всех перегнали.</ta>
            <ta e="T227" id="Seg_12699" s="T226">Подожди(?).</ta>
            <ta e="T238" id="Seg_12700" s="T227">Так пришли эти два брата, эти два брата живут, эти два старика-брата двое.</ta>
            <ta e="T241" id="Seg_12701" s="T238">Четыреста оленей у него.</ta>
            <ta e="T245" id="Seg_12702" s="T241">Этого старика оленей чтобы забрать.</ta>
            <ta e="T251" id="Seg_12703" s="T245">Со своими людьми придёт этот Пони-старик, это его имя.</ta>
            <ta e="T253" id="Seg_12704" s="T251">Теперь подожди.</ta>
            <ta e="T258" id="Seg_12705" s="T253">Придя, переночевали в этом их доме.</ta>
            <ta e="T266" id="Seg_12706" s="T258">Этого парня ведь не знают, как [он] будет разговаривать?</ta>
            <ta e="T274" id="Seg_12707" s="T266">Он правильно или как живёт.</ta>
            <ta e="T277" id="Seg_12708" s="T274">Сейчас как будешь знать?</ta>
            <ta e="T279" id="Seg_12709" s="T277">В стороне живя.</ta>
            <ta e="T282" id="Seg_12710" s="T279">Переночевали двое, вот что.</ta>
            <ta e="T289" id="Seg_12711" s="T282">Мол, на третий (с половиной?) день пришел.</ta>
            <ta e="T296" id="Seg_12712" s="T289">Пони-старик с десятью людьми пришёл.</ta>
            <ta e="T300" id="Seg_12713" s="T296">Вроде чтобы этих оленей пригнать.</ta>
            <ta e="T308" id="Seg_12714" s="T300">Эти люди этих двух братьев этих оленей гонят.</ta>
            <ta e="T319" id="Seg_12715" s="T308">Сколько всех домов, тех новых домов, сколько всего [их скарба] собрали.</ta>
            <ta e="T328" id="Seg_12716" s="T319">Раньше еще северных ненцев оленей собрав, еще всех взяли. </ta>
            <ta e="T331" id="Seg_12717" s="T328">Потом (так сделали)?.</ta>
            <ta e="T336" id="Seg_12718" s="T331">Домой пошёл этот ненецкий строй [в виде стрелы].</ta>
            <ta e="T341" id="Seg_12719" s="T336">На третий день, мол, мы придём.</ta>
            <ta e="T346" id="Seg_12720" s="T341">Вы, мол, никуда не ходите.</ta>
            <ta e="T356" id="Seg_12721" s="T346">На второй день рассвело, этот парень так стал (обращается) к этому своему деду.</ta>
            <ta e="T359" id="Seg_12722" s="T356">К этому гостя деду. [?]</ta>
            <ta e="T365" id="Seg_12723" s="T359">Вроде впереди(/вверх по течению) здесь что-то есть вроде.</ta>
            <ta e="T376" id="Seg_12724" s="T365">Четыреста оленей чтоб стояли, мол, там тундра есть, туда, мол, давай отпустим.</ta>
            <ta e="T384" id="Seg_12725" s="T376">Ненцы придут, потом оленей начнут загонять, так, мол, тот [парень говорит].</ta>
            <ta e="T391" id="Seg_12726" s="T384">Оленье пастбище всю зиму перезимовать, мол, куда денем?</ta>
            <ta e="T395" id="Seg_12727" s="T391">Этот парень так говорит.</ta>
            <ta e="T400" id="Seg_12728" s="T395">Потом так говорит, мол…</ta>
            <ta e="T406" id="Seg_12729" s="T400">"Мне дай новый, новый дом у тебя есть?</ta>
            <ta e="T412" id="Seg_12730" s="T406">Дед, мол, что, новый дом у тебя есть? </ta>
            <ta e="T415" id="Seg_12731" s="T412">Мне отдай," мол. </ta>
            <ta e="T425" id="Seg_12732" s="T415">А вот та того брата дочь, дед эту [девушку] заставляет идти к этому парню [=отдаёт замуж].</ta>
            <ta e="T439" id="Seg_12733" s="T425">"Я, мол, просто приподниму чум, впереди/вверх по течению лесной перешеек будет, на эту переднюю сторону поставлю чум".</ta>
            <ta e="T443" id="Seg_12734" s="T439">Новый чум принёс, вот он.</ta>
            <ta e="T444" id="Seg_12735" s="T443">Отпустили [они его].</ta>
            <ta e="T445" id="Seg_12736" s="T444">Подожди.</ta>
            <ta e="T447" id="Seg_12737" s="T445">Отпустили вот.</ta>
            <ta e="T457" id="Seg_12738" s="T447">Вроде устал, на второй, на третий день придет старик Пони со своими людьми к ним.</ta>
            <ta e="T467" id="Seg_12739" s="T457">Эти два ненца, [их оленей] чтобы угнать, два брата (сегодня)?.</ta>
            <ta e="T478" id="Seg_12740" s="T467">Потом они распустили этих оленей, загнали, вроде он тоже,…</ta>
            <ta e="T481" id="Seg_12741" s="T478">Чум [ему] дали.</ta>
            <ta e="T490" id="Seg_12742" s="T481">В чум зашёл, на этом лесном перешейке в передней стороне в чум зашёл. </ta>
            <ta e="T495" id="Seg_12743" s="T490">Вот и поставили они чум с женой.</ta>
            <ta e="T501" id="Seg_12744" s="T495">Разве у богатого ненца чума не будет?</ta>
            <ta e="T506" id="Seg_12745" s="T501">С новым нюком чум этот. </ta>
            <ta e="T975" id="Seg_12746" s="T506">Побольше (соорудили?).</ta>
            <ta e="T976" id="Seg_12747" s="T975">[NN:] (…) разговаривай.</ta>
            <ta e="T990" id="Seg_12748" s="T976">[NN:] Бабушка? </ta>
            <ta e="T977" id="Seg_12749" s="T990">[NEP:] А?</ta>
            <ta e="T514" id="Seg_12750" s="T977">[NN:] Пониже наклони [голову].</ta>
            <ta e="T515" id="Seg_12751" s="T514">[KuAI:] Сейчас попробуем.</ta>
            <ta e="T978" id="Seg_12752" s="T515">[NN:] … а на радио…</ta>
            <ta e="T979" id="Seg_12753" s="T978">[NN:] Видно, спрашивает.</ta>
            <ta e="T980" id="Seg_12754" s="T979">[NN:] Хорошо разговаривает, рассказывает.</ta>
            <ta e="T981" id="Seg_12755" s="T980">[NN:] Подожди, не шуми.</ta>
            <ta e="T982" id="Seg_12756" s="T981">[NN:] Вроде, глухой (?).</ta>
            <ta e="T983" id="Seg_12757" s="T982">[NN:] Вроде слушает, может быть, плохо говорит.</ta>
            <ta e="T974" id="Seg_12758" s="T983">[NN:] Тихо [=без слов].</ta>
            <ta e="T542" id="Seg_12759" s="T974">[NEP:] В общем, переночевали этот парень с женой.</ta>
            <ta e="T549" id="Seg_12760" s="T542">Переночевали, на следующее утро так стал [=обращается] к жене.</ta>
            <ta e="T556" id="Seg_12761" s="T549">Я, мол, вперёд/вверх по реке к тем своим людям схожу.</ta>
            <ta e="T559" id="Seg_12762" s="T556">Чум поставили [=обустроились], или куда делись.</ta>
            <ta e="T564" id="Seg_12763" s="T559">Вперёд/вверх по течению поехал, три оленя запряг.</ta>
            <ta e="T569" id="Seg_12764" s="T564">Вроде тридцать оленей оставил себе.</ta>
            <ta e="T576" id="Seg_12765" s="T569">Парень вперёд уехал, жена одна осталась сама.</ta>
            <ta e="T578" id="Seg_12766" s="T576">Вот сидит.</ta>
            <ta e="T581" id="Seg_12767" s="T578">Скоро…</ta>
            <ta e="T588" id="Seg_12768" s="T581">В одно время солнце к вечеру пошло.</ta>
            <ta e="T595" id="Seg_12769" s="T588">Вечером, (от парня тишина впереди?).</ta>
            <ta e="T604" id="Seg_12770" s="T595">В одно время люди (пришли?) с севера/снизу по реке.</ta>
            <ta e="T608" id="Seg_12771" s="T604">[Люди] Пони-старика пришли.</ta>
            <ta e="T613" id="Seg_12772" s="T608">Десять человек, которые оленей перегоняли.</ta>
            <ta e="T621" id="Seg_12773" s="T613">Пони-старик такой видящий, такой умный. </ta>
            <ta e="T630" id="Seg_12774" s="T621">Потом подожди, этот парень приехал, слышно, – с улицы слышит жена.</ta>
            <ta e="T636" id="Seg_12775" s="T630">Приехав, этих оленей привязал, оленей.</ta>
            <ta e="T984" id="Seg_12776" s="T636">[NN:] (…).</ta>
            <ta e="T641" id="Seg_12777" s="T984">[NEP:] Видать, только приехал.</ta>
            <ta e="T651" id="Seg_12778" s="T641">Люди Пони-старика что, видать, приехали, нарты его так стоят.</ta>
            <ta e="T655" id="Seg_12779" s="T651">На той территории, на его территории.</ta>
            <ta e="T662" id="Seg_12780" s="T655">Зашёл, в чум зашёл, у дверного проёма стоя, ненец…</ta>
            <ta e="T668" id="Seg_12781" s="T662">У дверного проёма ноги отряхнул.</ta>
            <ta e="T671" id="Seg_12782" s="T668">Бокари свои отряхнул.</ta>
            <ta e="T675" id="Seg_12783" s="T671">С замерзшей стороны свои бокари отряхнул.</ta>
            <ta e="T682" id="Seg_12784" s="T675">Так говорит, вперёд повернувшись к Пони-старику, мол.</ta>
            <ta e="T688" id="Seg_12785" s="T682">Рот твой, как будто сойки рот. </ta>
            <ta e="T689" id="Seg_12786" s="T688">Подожди.</ta>
            <ta e="T694" id="Seg_12787" s="T689">Вперёд побежал с кулаком.</ta>
            <ta e="T698" id="Seg_12788" s="T694">Прошёлся [=погладил] одной рукой [=половиной из 2 рук], прошёлся [=погладил].</ta>
            <ta e="T704" id="Seg_12789" s="T698">Людей Пони-старика всех набок уложил [=ударил]. </ta>
            <ta e="T706" id="Seg_12790" s="T704">Некоторые умерли.</ta>
            <ta e="T718" id="Seg_12791" s="T706">Пони-ира жив остался, два ненца живы остались, на улицу [их] выбросил, этот парень.</ta>
            <ta e="T722" id="Seg_12792" s="T718">Один раз кулаком прошёлся [=погладил].</ta>
            <ta e="T730" id="Seg_12793" s="T722">Все [еще] дома в штаны [=внутри одежды] (наделали?), эти умершие.</ta>
            <ta e="T731" id="Seg_12794" s="T730">[матерится]</ta>
            <ta e="T744" id="Seg_12795" s="T731">Потом на улицу побросал, на территорию вокруг чума, а (караван)? нарт на улице завязан. </ta>
            <ta e="T753" id="Seg_12796" s="T744">Пони-старика тоже на улицу выбросил, что там, всех на улицу выбросил.</ta>
            <ta e="T763" id="Seg_12797" s="T753">Сел, малицу (снял?), поев, спать лёг.</ta>
            <ta e="T770" id="Seg_12798" s="T763">Те его люди на улице лежат связанные.</ta>
            <ta e="T773" id="Seg_12799" s="T770">Как завязаны, так.</ta>
            <ta e="T778" id="Seg_12800" s="T773">Переночевал, на следующее утро встал.</ta>
            <ta e="T790" id="Seg_12801" s="T778">Пони-старик так, лицо, голова целиком опухли, кое-как глаза (светятся?).</ta>
            <ta e="T796" id="Seg_12802" s="T790">Те ненцы ещё лучше [=хуже] были.</ta>
            <ta e="T803" id="Seg_12803" s="T796">Так сказал этот парень, мол, этим своим ненцам.</ta>
            <ta e="T811" id="Seg_12804" s="T803">Своих людей, мол, домой увези, на свои сани загрузив, мол, примотав.</ta>
            <ta e="T813" id="Seg_12805" s="T811">На свои сани.</ta>
            <ta e="T830" id="Seg_12806" s="T813">Потом те двое на улице (на последнем дыхании?) домой зайдут, или кушают или нет. </ta>
            <ta e="T836" id="Seg_12807" s="T830">Эти люди, которые привязаны к его нартам.</ta>
            <ta e="T847" id="Seg_12808" s="T836">Тот там домой, (соединив [нарты]?), людей, этих умерших людей так увёз домой. </ta>
            <ta e="T849" id="Seg_12809" s="T847">На север/вниз по реке домой.</ta>
            <ta e="T854" id="Seg_12810" s="T849">Пони-старика (тоже привязав [нарты]?) увёз.</ta>
            <ta e="T858" id="Seg_12811" s="T854">Вроде немного только дыхания у него.</ta>
            <ta e="T861" id="Seg_12812" s="T858">Уехали вот.</ta>
            <ta e="T865" id="Seg_12813" s="T861">Потом хорошо поехали там.</ta>
            <ta e="T875" id="Seg_12814" s="T865">Жене так говорит: “Чум сними, чтобы мы вперед поехали к людям”.</ta>
            <ta e="T884" id="Seg_12815" s="T875">Не знали сами дед с бабкой, что-то.</ta>
            <ta e="T891" id="Seg_12816" s="T884">Вперёд пошёл, сам не разговаривает.</ta>
            <ta e="T898" id="Seg_12817" s="T891">Эта жена скажет отцу и дедушкам-бабушкам.</ta>
            <ta e="T905" id="Seg_12818" s="T898">Пони-старика люди, мол, домой поехали.</ta>
            <ta e="T906" id="Seg_12819" s="T905">Подожди.</ta>
            <ta e="T911" id="Seg_12820" s="T906">Потом вроде…</ta>
            <ta e="T985" id="Seg_12821" s="T911">Они не знают.</ta>
            <ta e="T986" id="Seg_12822" s="T985">[NN:] Вниз разговаривай.</ta>
            <ta e="T987" id="Seg_12823" s="T986">[NN:] Вниз к своему радио.</ta>
            <ta e="T932" id="Seg_12824" s="T987">[NEP:] Вперёд в северную сторону/вниз по реке зашли, они туда в деда чум зашли, нарты обмотав, там же и поставил.</ta>
            <ta e="T936" id="Seg_12825" s="T932">Они уехали, домой уехали.</ta>
            <ta e="T944" id="Seg_12826" s="T936">Пони-старик только домой на север/вниз по реке добрался, умер.</ta>
            <ta e="T949" id="Seg_12827" s="T944">Те двое его остались живы.</ta>
            <ta e="T988" id="Seg_12828" s="T949">Пони-старик, Пони-старик с людьми там есть. </ta>
            <ta e="T989" id="Seg_12829" s="T988">[NN:] Другое что-нибудь скажи, другое, другое. </ta>
            <ta e="T964" id="Seg_12830" s="T989">[NN:] Другое что-нибудь скажи.</ta>
            <ta e="T965" id="Seg_12831" s="T964">[NN:] Тут рассказанное всё и есть.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T968" id="Seg_12832" s="T0">[KuAI:] A lazy person always has a holiday.</ta>
            <ta e="T6" id="Seg_12833" s="T968">[NEP:] There was a child.</ta>
            <ta e="T13" id="Seg_12834" s="T6">This is somehow (?), after all I always tell this.</ta>
            <ta e="T19" id="Seg_12835" s="T13">It's a true story, a true story, not a fairy tale.</ta>
            <ta e="T969" id="Seg_12836" s="T19">[KuAI:] Wait, tell her to wait, I'll try, I'll have a look, I'll check.</ta>
            <ta e="T970" id="Seg_12837" s="T969">[NN:] Beneath you… the Selkups there…</ta>
            <ta e="T971" id="Seg_12838" s="T970">[NN:] This one will show you, pause your conversation.</ta>
            <ta e="T972" id="Seg_12839" s="T971">[NN:] Take it off.</ta>
            <ta e="T973" id="Seg_12840" s="T972">[NN:] Speak slowly.</ta>
            <ta e="T41" id="Seg_12841" s="T973">[NEP:] There lived this old man.</ta>
            <ta e="T42" id="Seg_12842" s="T41">He got ill.</ta>
            <ta e="T45" id="Seg_12843" s="T42">This old man was Selkup.</ta>
            <ta e="T53" id="Seg_12844" s="T45">On this side of the river there is a storage, on the other side of the river downstream [this old man] lived.</ta>
            <ta e="T992" id="Seg_12845" s="T53">(As if?) on a treeless territory (?).</ta>
            <ta e="T59" id="Seg_12846" s="T992">[?]</ta>
            <ta e="T61" id="Seg_12847" s="T59">That [old man] lived.</ta>
            <ta e="T62" id="Seg_12848" s="T61">He got ill.</ta>
            <ta e="T68" id="Seg_12849" s="T62">A son, one son, without a friend, without anybody.</ta>
            <ta e="T74" id="Seg_12850" s="T68">He has not got a daughter, he has not got anybody.</ta>
            <ta e="T83" id="Seg_12851" s="T74">Then the father of this guy got ill, this guy's [father].</ta>
            <ta e="T90" id="Seg_12852" s="T83">He lives there, by his father and his mother in a tent.</ta>
            <ta e="T96" id="Seg_12853" s="T90">On the other side there were two Nenets brothers.</ta>
            <ta e="T101" id="Seg_12854" s="T96">Where will this Nenets old man [go]?</ta>
            <ta e="T110" id="Seg_12855" s="T101">Whether he lived, (where?), that old man died.</ta>
            <ta e="T122" id="Seg_12856" s="T110">He died, then that guy… the Nenets old man, where will he go?</ta>
            <ta e="T125" id="Seg_12857" s="T122">He went there to his brother.</ta>
            <ta e="T127" id="Seg_12858" s="T125">The old man died.</ta>
            <ta e="T129" id="Seg_12859" s="T127">[They] buried [him].</ta>
            <ta e="T138" id="Seg_12860" s="T129">Then his wife, his wife got ill, and his wife died as well.</ta>
            <ta e="T145" id="Seg_12861" s="T138">This son of the old man and the old woman was left alone, without anybody.</ta>
            <ta e="T147" id="Seg_12862" s="T145">Where will he go?</ta>
            <ta e="T157" id="Seg_12863" s="T147">Then this Nenets oldman took the guy to the other side of the river.</ta>
            <ta e="T170" id="Seg_12864" s="T157">A big guy… whichever… like Gena with someone. [?]</ta>
            <ta e="T173" id="Seg_12865" s="T170">The son was like that.</ta>
            <ta e="T177" id="Seg_12866" s="T173">He took him to the other side, he has no reindeer.</ta>
            <ta e="T182" id="Seg_12867" s="T177">The father with his son lived without reindeer walking.</ta>
            <ta e="T188" id="Seg_12868" s="T182">They lived there, they lived [there], then they crossed [the river].</ta>
            <ta e="T199" id="Seg_12869" s="T188">Having crossed [the river], this guy [started to live] by the Nenets oldman, the Nenets oldman has no children.</ta>
            <ta e="T201" id="Seg_12870" s="T199">His wife was childless.</ta>
            <ta e="T208" id="Seg_12871" s="T201">His middle brother had one single daughter.</ta>
            <ta e="T212" id="Seg_12872" s="T208">The two Nenets [came] here from the North/from down the river.</ta>
            <ta e="T215" id="Seg_12873" s="T212">They [came] from the treeless land.</ta>
            <ta e="T226" id="Seg_12874" s="T215">There is Poni oldman, he took away all the reindeer of the family.</ta>
            <ta e="T227" id="Seg_12875" s="T226">Wait(?).</ta>
            <ta e="T238" id="Seg_12876" s="T227">So these two brothers came, they are living [here], these two old men brothers.</ta>
            <ta e="T241" id="Seg_12877" s="T238">He has got four hundred reindeer.</ta>
            <ta e="T245" id="Seg_12878" s="T241">In order to take away this old man's reindeer.</ta>
            <ta e="T251" id="Seg_12879" s="T245">Poni oldman is coming with his people, that is his name.</ta>
            <ta e="T253" id="Seg_12880" s="T251">Now wait.</ta>
            <ta e="T258" id="Seg_12881" s="T253">Having come, they spent a night in their house.</ta>
            <ta e="T266" id="Seg_12882" s="T258">They do not know this guy, how will he speak?</ta>
            <ta e="T274" id="Seg_12883" s="T266">Whether he lives wise or how [he lives].</ta>
            <ta e="T277" id="Seg_12884" s="T274">Now how do you know?</ta>
            <ta e="T279" id="Seg_12885" s="T277">He is living aside.</ta>
            <ta e="T282" id="Seg_12886" s="T279">The two of them spent the night [there], now what.</ta>
            <ta e="T289" id="Seg_12887" s="T282">They say, he came on the third day (/in three and a half days).</ta>
            <ta e="T296" id="Seg_12888" s="T289">The old man Poni came with ten people.</ta>
            <ta e="T300" id="Seg_12889" s="T296">Supposedely, to herd away the reindeer.</ta>
            <ta e="T308" id="Seg_12890" s="T300">These men drive away the reindeer of these two brothers.</ta>
            <ta e="T319" id="Seg_12891" s="T308">How many houses there are, those new houses, however many things there are, they collected everything.</ta>
            <ta e="T328" id="Seg_12892" s="T319">Earlier they had taken away the reindeer of the northern Nenets, they took all.</ta>
            <ta e="T331" id="Seg_12893" s="T328">Then (they made like this)?.</ta>
            <ta e="T336" id="Seg_12894" s="T331">Then these Nenets in arrow-like formation went home.</ta>
            <ta e="T341" id="Seg_12895" s="T336">They said, we would come on the third day.</ta>
            <ta e="T346" id="Seg_12896" s="T341">Don't you go anywhere, they said.</ta>
            <ta e="T356" id="Seg_12897" s="T346">On the second day it dawned, this boy said this to his [adopted] grandfather.</ta>
            <ta e="T359" id="Seg_12898" s="T356">To this guest's grandfather. [?]</ta>
            <ta e="T365" id="Seg_12899" s="T359">It seems there is something up the river.</ta>
            <ta e="T376" id="Seg_12900" s="T365">There is a [place in] tundra where four hundred reindeer could stay, he says, let's herd them there, he says.</ta>
            <ta e="T384" id="Seg_12901" s="T376">The Nenets men will come and start taking away the reindeer, so this [guy says].</ta>
            <ta e="T391" id="Seg_12902" s="T384">Having wintered at the reindeer herding place, he says, what [else] will we do.</ta>
            <ta e="T395" id="Seg_12903" s="T391">This guy says so.</ta>
            <ta e="T400" id="Seg_12904" s="T395">Then he says so…</ta>
            <ta e="T406" id="Seg_12905" s="T400">"Give me a new [house], have you got a new house?</ta>
            <ta e="T412" id="Seg_12906" s="T406">Grandfather, he says, have you got a new house?</ta>
            <ta e="T415" id="Seg_12907" s="T412">Give it to me," he says.</ta>
            <ta e="T425" id="Seg_12908" s="T415">And this that brother's daughter, the grandfather makes her go to this guy [to marry him].</ta>
            <ta e="T439" id="Seg_12909" s="T425">"I'll just move the tent, he says, forward/up the river there is a neck of land with a forest, I'll put the tent at the front edge [of it]."</ta>
            <ta e="T443" id="Seg_12910" s="T439">He brought a new tent, here it is.</ta>
            <ta e="T444" id="Seg_12911" s="T443">They let him go.</ta>
            <ta e="T445" id="Seg_12912" s="T444">Wait.</ta>
            <ta e="T447" id="Seg_12913" s="T445">Here they let him go.</ta>
            <ta e="T457" id="Seg_12914" s="T447">Supposedely he got tired, on the second, on the third day the old man Poni will come to them with his men.</ta>
            <ta e="T467" id="Seg_12915" s="T457">These two Nenets man, to collect [their reindeer], the two brothers (today)?.</ta>
            <ta e="T478" id="Seg_12916" s="T467">Then they let these reindeer go, they herded them, and he too…</ta>
            <ta e="T481" id="Seg_12917" s="T478">They gave him a tent.</ta>
            <ta e="T490" id="Seg_12918" s="T481">He entered the tent, at the front edge of this forested neck of land he entered the tent.</ta>
            <ta e="T495" id="Seg_12919" s="T490">They set up the tent with his wife.</ta>
            <ta e="T501" id="Seg_12920" s="T495">Of course, a rich Nenets man would [hardly] have no tent.</ta>
            <ta e="T506" id="Seg_12921" s="T501">That [was] a tent with a new tent skin.</ta>
            <ta e="T975" id="Seg_12922" s="T506">(They made?) a bigger one.</ta>
            <ta e="T976" id="Seg_12923" s="T975">[NN:] (…) speak.</ta>
            <ta e="T990" id="Seg_12924" s="T976">[NN:] Grandmother? </ta>
            <ta e="T977" id="Seg_12925" s="T990">[NEP:] Yeh?</ta>
            <ta e="T514" id="Seg_12926" s="T977">[NN:] Bow [your head].</ta>
            <ta e="T515" id="Seg_12927" s="T514">[KuAI:] Let's try now.</ta>
            <ta e="T978" id="Seg_12928" s="T515">[NN:] … and on the radio…</ta>
            <ta e="T979" id="Seg_12929" s="T978">[NN:] Apparently, she is asking.</ta>
            <ta e="T980" id="Seg_12930" s="T979">[NN:] She speaks well, she tells [the story well].</ta>
            <ta e="T981" id="Seg_12931" s="T980">[NN:] Wait, don't make noise.</ta>
            <ta e="T982" id="Seg_12932" s="T981">[NN:] Is (s)he deaf (?)…</ta>
            <ta e="T983" id="Seg_12933" s="T982">[NN:] (S)he seems to listen, maybe she speaks badly.</ta>
            <ta e="T974" id="Seg_12934" s="T983">[NN:] Silently [= ‎‎without words].</ta>
            <ta e="T542" id="Seg_12935" s="T974">[NEP:] Then this guy spent a night with his wife.</ta>
            <ta e="T549" id="Seg_12936" s="T542">They spent a night, then he says like this to his wife.</ta>
            <ta e="T556" id="Seg_12937" s="T549">I'll go forward/ up the river to those people of mine.</ta>
            <ta e="T559" id="Seg_12938" s="T556">They put the tent or what did they do.</ta>
            <ta e="T564" id="Seg_12939" s="T559">Forward/up the river he went, he harnessed three oxen.</ta>
            <ta e="T569" id="Seg_12940" s="T564">He should have left thirty reindeer to himself.</ta>
            <ta e="T576" id="Seg_12941" s="T569">The guy went up the river, his wife stayed alone.</ta>
            <ta e="T578" id="Seg_12942" s="T576">She is sitting.</ta>
            <ta e="T581" id="Seg_12943" s="T578">Soon…</ta>
            <ta e="T588" id="Seg_12944" s="T581">Once the sun was going to the evening.</ta>
            <ta e="T595" id="Seg_12945" s="T588">In the evening (this guy was quiet, forward?).</ta>
            <ta e="T604" id="Seg_12946" s="T595">Once the people (came?) from the north/from down the river.</ta>
            <ta e="T608" id="Seg_12947" s="T604">[The men] of the old man Poni came.</ta>
            <ta e="T613" id="Seg_12948" s="T608">Ten people, who were driving the reindeer.</ta>
            <ta e="T621" id="Seg_12949" s="T613">The old man Poni is so well-seeing, so wise.</ta>
            <ta e="T630" id="Seg_12950" s="T621">Then, wait, this guy came, one hears, – his wife hears from outside.</ta>
            <ta e="T636" id="Seg_12951" s="T630">Having come (one hears), he bound his reindeer.</ta>
            <ta e="T984" id="Seg_12952" s="T636">[NN:] (…).</ta>
            <ta e="T641" id="Seg_12953" s="T984">[NEP:] It seems he has just arrived.</ta>
            <ta e="T651" id="Seg_12954" s="T641">It seems, that the men of the old man Poni have arrived, his sledges stay like this.</ta>
            <ta e="T655" id="Seg_12955" s="T651">On the outside land, on his land.</ta>
            <ta e="T662" id="Seg_12956" s="T655">He entered, he entered the tent, the Nenets man, standing at the door…</ta>
            <ta e="T668" id="Seg_12957" s="T662">At the door he shaked off his leg.</ta>
            <ta e="T671" id="Seg_12958" s="T668">He shaked off his fur boots.</ta>
            <ta e="T675" id="Seg_12959" s="T671">He shaked off his fur boots from the frozen side.</ta>
            <ta e="T682" id="Seg_12960" s="T675">So he says, turning forward to the old man Pan, he says.</ta>
            <ta e="T688" id="Seg_12961" s="T682">Your mouth, he says, is like a jay's mouth.</ta>
            <ta e="T689" id="Seg_12962" s="T688">Wait.</ta>
            <ta e="T694" id="Seg_12963" s="T689">He ran forward with his fist.</ta>
            <ta e="T698" id="Seg_12964" s="T694">He stroke [=caressed] with one hand (with a half of his hands), he stroke [=caressed] [him].</ta>
            <ta e="T704" id="Seg_12965" s="T698">He lay down [=to the side] all the men of the old man Pan. </ta>
            <ta e="T706" id="Seg_12966" s="T704">Some of them died.</ta>
            <ta e="T718" id="Seg_12967" s="T706">The old man Pan remained alive, two Nenets man remained alive, he threw [them] outdoors, this guy.</ta>
            <ta e="T722" id="Seg_12968" s="T718">He stroke [=caressed] [them] once.</ta>
            <ta e="T730" id="Seg_12969" s="T722">They all [being] in the tent (shat?) their pants [=clothes], those who died.</ta>
            <ta e="T731" id="Seg_12970" s="T730">[cursing]</ta>
            <ta e="T744" id="Seg_12971" s="T731">Then he threw them outside, onto the territory near the house, and those sledges (train of sledges) are tied up outside.</ta>
            <ta e="T753" id="Seg_12972" s="T744">He also threw the old man Poni outside, well, he threw them all outside.</ta>
            <ta e="T763" id="Seg_12973" s="T753">He sat down, he (took off?) his fur overclothes, he ate and went asleep.</ta>
            <ta e="T770" id="Seg_12974" s="T763">Those people [of his] are lying outdoors, tied up.</ta>
            <ta e="T773" id="Seg_12975" s="T770">How they were tied up, so.</ta>
            <ta e="T778" id="Seg_12976" s="T773">He spent the night, the next morning he got up.</ta>
            <ta e="T790" id="Seg_12977" s="T778">The old man Poni, his face and his head got so swollen, his eyes (are glowing?).</ta>
            <ta e="T796" id="Seg_12978" s="T790">Those Nenets men were better [=worse].</ta>
            <ta e="T803" id="Seg_12979" s="T796">That’s what this boy said, he says, to these Nenets men of his. </ta>
            <ta e="T811" id="Seg_12980" s="T803">Take his/your men home on your own sledges, he says, having tied them up.</ta>
            <ta e="T813" id="Seg_12981" s="T811">Onto your own sledges.</ta>
            <ta e="T830" id="Seg_12982" s="T813">Then there [those two] entered the house (on their last breath?), whether to eat or not.</ta>
            <ta e="T836" id="Seg_12983" s="T830">Those people, who had been tied up to the sledges.</ta>
            <ta e="T847" id="Seg_12984" s="T836">Then, (having bound [the sledges]?) he took his people, these dead people back home.</ta>
            <ta e="T849" id="Seg_12985" s="T847">Home to the North/down the river.</ta>
            <ta e="T854" id="Seg_12986" s="T849">He took the old man Poni (having bound [the sledges]?) away.</ta>
            <ta e="T858" id="Seg_12987" s="T854">He seems to have just a little breath.</ta>
            <ta e="T861" id="Seg_12988" s="T858">So they went away.</ta>
            <ta e="T865" id="Seg_12989" s="T861">Then they left fine.</ta>
            <ta e="T875" id="Seg_12990" s="T865">He says so to his wife: “Take the tent down, so that we go forward/upstream to the people.”</ta>
            <ta e="T884" id="Seg_12991" s="T875">His (grand)parent didn't know anything.</ta>
            <ta e="T891" id="Seg_12992" s="T884">He went forward/upstream, he himself doesn't speak.</ta>
            <ta e="T898" id="Seg_12993" s="T891">His wife will tell [the story] to the (grand)parents.</ta>
            <ta e="T905" id="Seg_12994" s="T898">The old man Poni's men left home, she says.</ta>
            <ta e="T906" id="Seg_12995" s="T905">Wait.</ta>
            <ta e="T911" id="Seg_12996" s="T906">Then supposedly…</ta>
            <ta e="T985" id="Seg_12997" s="T911">They do not know.</ta>
            <ta e="T986" id="Seg_12998" s="T985">[NN:] Speak downward.</ta>
            <ta e="T987" id="Seg_12999" s="T986">[NN:] Downward to the tape recorder.</ta>
            <ta e="T932" id="Seg_13000" s="T987">[NEP:] They [went] forward to the North/down and entered the grandfather’s tent, having tied the sledges up, he put them there.</ta>
            <ta e="T936" id="Seg_13001" s="T932">They left, they went home.</ta>
            <ta e="T944" id="Seg_13002" s="T936">The old man Poni hardly reached his home to the North/down the river, [then] he died.</ta>
            <ta e="T949" id="Seg_13003" s="T944">Those two people lived.</ta>
            <ta e="T988" id="Seg_13004" s="T949">The old man Poni, there is such an old man Poni with his people.</ta>
            <ta e="T989" id="Seg_13005" s="T988">[NN:] Tell something else, the other one.</ta>
            <ta e="T964" id="Seg_13006" s="T989">[NN:] Tell the other one.</ta>
            <ta e="T965" id="Seg_13007" s="T964">[NN:] That's all story.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T968" id="Seg_13008" s="T0">[KuAI:] Für einen Faulen ist immer Feiertag.</ta>
            <ta e="T6" id="Seg_13009" s="T968">[NEP:] Es war [einmal] ein Kind.</ta>
            <ta e="T13" id="Seg_13010" s="T6">Das ist irgendwie (?), dennoch erzähle ich das.</ta>
            <ta e="T19" id="Seg_13011" s="T13">Es ist eine wahre Geschichte, eine wahre Geschichte, kein Märchen.</ta>
            <ta e="T969" id="Seg_13012" s="T19">[KuAI:] Wartet, sie soll warten, ich versuche, ich schaue, ich gucke nach.</ta>
            <ta e="T970" id="Seg_13013" s="T969">[NN:] Unter dir… die Selkupen hier…</ta>
            <ta e="T971" id="Seg_13014" s="T970">[NN:] Dieser wird dir zeigen, unterbrich dein Gespräch.</ta>
            <ta e="T972" id="Seg_13015" s="T971">[NN:] Nimm es ab.</ta>
            <ta e="T973" id="Seg_13016" s="T972">[NN:] Sprich langsam.</ta>
            <ta e="T41" id="Seg_13017" s="T973">[NEP:] Es lebte dieser alte Mann.</ta>
            <ta e="T42" id="Seg_13018" s="T41">Er wurde krank.</ta>
            <ta e="T45" id="Seg_13019" s="T42">Das war ein selkupischer alter Mann.</ta>
            <ta e="T53" id="Seg_13020" s="T45">Auf dieser Seite [des Flusses] ist ein Speicher, auf der anderen Seite des Flusses flussabwärts lebte er.</ta>
            <ta e="T992" id="Seg_13021" s="T53">(Als ob?) auf baumlosem Land (?).</ta>
            <ta e="T59" id="Seg_13022" s="T992">[?]</ta>
            <ta e="T61" id="Seg_13023" s="T59">Jener lebte.</ta>
            <ta e="T62" id="Seg_13024" s="T61">Er wurde krank.</ta>
            <ta e="T68" id="Seg_13025" s="T62">Ein Sohn, ein Sohn, ohne einen Freund, ohne irgendjemanden.</ta>
            <ta e="T74" id="Seg_13026" s="T68">Er hat keine Tochter, er hat niemanden.</ta>
            <ta e="T83" id="Seg_13027" s="T74">Dann wurde der Vater dieses Jungen krank, [der Vater] dieses Jungen.</ta>
            <ta e="T90" id="Seg_13028" s="T83">Er lebt dort, bei seinem Vater und seiner Mutter in einem Zelt.</ta>
            <ta e="T96" id="Seg_13029" s="T90">Auf der anderen Seite gab es zwei nenzische Brüder.</ta>
            <ta e="T101" id="Seg_13030" s="T96">Wo wird dieser alte Nenze hin[gehen]?</ta>
            <ta e="T110" id="Seg_13031" s="T101">Ob er lebte, (wohin?), dieser alte Mann starb.</ta>
            <ta e="T122" id="Seg_13032" s="T110">Er starb, dann jener Junge… wo geht der alte Nenze hin?</ta>
            <ta e="T125" id="Seg_13033" s="T122">Er ging dorthin zu seinem Bruder.</ta>
            <ta e="T127" id="Seg_13034" s="T125">Der alte Mann starb.</ta>
            <ta e="T129" id="Seg_13035" s="T127">Sie beerdigten [ihn].</ta>
            <ta e="T138" id="Seg_13036" s="T129">Dann seine Frau, seine Frau wurde krank, und seine Frau starb auch.</ta>
            <ta e="T145" id="Seg_13037" s="T138">Dieser Sohn des alten Mannes und der alten Frau blieb alleine, ohne irgendjemanden.</ta>
            <ta e="T147" id="Seg_13038" s="T145">Wo geht er hin?</ta>
            <ta e="T157" id="Seg_13039" s="T147">Dann brachte dieser alte Nenze den Jungen auf die andere Seite des Flusses.</ta>
            <ta e="T170" id="Seg_13040" s="T157">Ein großer Sohn… was immer… wie Gena mit jemandem. [?]</ta>
            <ta e="T173" id="Seg_13041" s="T170">So war der Sohn.</ta>
            <ta e="T177" id="Seg_13042" s="T173">Er brachte ihn auf die andere Seite, er hat keine Rentiere.</ta>
            <ta e="T182" id="Seg_13043" s="T177">Ohne Rentierbeine (zu Fuß) lebte der Vater mit seinem Sohn.</ta>
            <ta e="T188" id="Seg_13044" s="T182">Sie lebten dort, sie lebten, dann fuhren sie auf die andere Seite.</ta>
            <ta e="T199" id="Seg_13045" s="T188">Auf die andere Seite gekommen ist dieser Junge bei dem alten Nenzen, der alter Nenze hat keine Kinder.</ta>
            <ta e="T201" id="Seg_13046" s="T199">Seine Frau war kinderlos.</ta>
            <ta e="T208" id="Seg_13047" s="T201">Sein mittlerer Bruder hatte eine einzige Tochter.</ta>
            <ta e="T212" id="Seg_13048" s="T208">Von Norden/von weiter flussabwärts [kamen] die zwei Nenzen.</ta>
            <ta e="T215" id="Seg_13049" s="T212">Sie(?) sind aus(?) einem baumlosen Land.</ta>
            <ta e="T226" id="Seg_13050" s="T215">Da ist der alte Mann Poni, er hat alle Rentiere des Clans weggenommen.</ta>
            <ta e="T227" id="Seg_13051" s="T226">Warte(?).</ta>
            <ta e="T238" id="Seg_13052" s="T227">So kamen diese beiden Brüder, die beiden Brüder leben, diese beiden alten Brüder.</ta>
            <ta e="T241" id="Seg_13053" s="T238">Er hat vierhundert Rentiere.</ta>
            <ta e="T245" id="Seg_13054" s="T241">Um die Rentiere des alten Mannes wegzunehmen.</ta>
            <ta e="T251" id="Seg_13055" s="T245">Mit seinen Leuten wird dieser alte Poni kommen, das ist sein Name.</ta>
            <ta e="T253" id="Seg_13056" s="T251">Nun warte.</ta>
            <ta e="T258" id="Seg_13057" s="T253">Angekommen, übernachteten sie in ihrem Haus.</ta>
            <ta e="T266" id="Seg_13058" s="T258">Sie kennen diesen Jungen nicht, wie wird er sprechen?</ta>
            <ta e="T274" id="Seg_13059" s="T266">Ob er richtig oder wie lebt.</ta>
            <ta e="T277" id="Seg_13060" s="T274">Wie kannst du es wissen?</ta>
            <ta e="T279" id="Seg_13061" s="T277">Er lebt an der Seite.</ta>
            <ta e="T282" id="Seg_13062" s="T279">Die beiden übernachteten also.</ta>
            <ta e="T289" id="Seg_13063" s="T282">Man sagt, er kam am dritten Tag (/nach dreieinhalb Tagen).</ta>
            <ta e="T296" id="Seg_13064" s="T289">Der alte Poni kam mit zehn Leuten.</ta>
            <ta e="T300" id="Seg_13065" s="T296">Offenbar um die Rentiere wegzutreiben.</ta>
            <ta e="T308" id="Seg_13066" s="T300">Diese Leute treiben die Rentiere dieser beiden Brüder weg.</ta>
            <ta e="T319" id="Seg_13067" s="T308">Wie viele Häuser, jene neuen Häuser, wie viele Sachen dort sind, sie sammelten alles ein.</ta>
            <ta e="T328" id="Seg_13068" s="T319">Früher hatten sie die Rentiere der nördlichen Nenzen weggenommen, sie nahmen alle.</ta>
            <ta e="T331" id="Seg_13069" s="T328">Dann (machten sie so)?.</ta>
            <ta e="T336" id="Seg_13070" s="T331">Dann ging diese nenzische Pfeil [=Nenzen in Pfeilformation].</ta>
            <ta e="T341" id="Seg_13071" s="T336">Am dritten Tag, sagen sie, würden sie kommen.</ta>
            <ta e="T346" id="Seg_13072" s="T341">"Geht nirgendwohin", sagten sie.</ta>
            <ta e="T356" id="Seg_13073" s="T346">Am zweiten Tag dämmerte es, dieser Junge sagte zu seinem Großvater.</ta>
            <ta e="T359" id="Seg_13074" s="T356">Zum Großvater dieses Gastes. [?]</ta>
            <ta e="T365" id="Seg_13075" s="T359">"Flussaufwärts scheint irgendetwas zu sein.</ta>
            <ta e="T376" id="Seg_13076" s="T365">Es gibt dort Tundra, wo vierhundert Rentiere stehen können, sagt man, lass uns sie dorthin lassen.</ta>
            <ta e="T384" id="Seg_13077" s="T376">Die Nenzen werden kommen, dann werden sie anfangen die Rentiere zu treiben.</ta>
            <ta e="T391" id="Seg_13078" s="T384">Wenn wir an dem Ort der Rentierweiden überwintern, was machen wir dann wohl?"</ta>
            <ta e="T395" id="Seg_13079" s="T391">Dieser Junge sagt so.</ta>
            <ta e="T400" id="Seg_13080" s="T395">Dann sagt er so…</ta>
            <ta e="T406" id="Seg_13081" s="T400">"Gib mir ein neues [Haus], hast du ein neues Haus?</ta>
            <ta e="T412" id="Seg_13082" s="T406">Großvater", sagt er, "hast du ein neues Haus?</ta>
            <ta e="T415" id="Seg_13083" s="T412">Gib es mir", sagt er.</ta>
            <ta e="T425" id="Seg_13084" s="T415">Und diese Tochter jenes Bruders, der Großvater lässt sie zu diesem Jungen gehen [ihn heiraten].</ta>
            <ta e="T439" id="Seg_13085" s="T425">"Ich bewege nur das Zelt", sagt er, "weiter flussaufwärts ist eine bewaldete Landzunge, ich werde das Zelt auf die Vorderseite stellen."</ta>
            <ta e="T443" id="Seg_13086" s="T439">Er brachte ein neues Zelt, hier ist es.</ta>
            <ta e="T444" id="Seg_13087" s="T443">Sie ließen ihn gehen.</ta>
            <ta e="T445" id="Seg_13088" s="T444">Warte.</ta>
            <ta e="T447" id="Seg_13089" s="T445">Nun, sie ließen sie gehen.</ta>
            <ta e="T457" id="Seg_13090" s="T447">Er wurde wohl müde, am zweiten, am dritten Tag wird der alte Poni mit seinen Männern zu ihnen kommen.</ta>
            <ta e="T467" id="Seg_13091" s="T457">Diese beiden Nenzen, um [ihre Rentiere] wegzutreiben, die beiden Brüder (heute)?.</ta>
            <ta e="T478" id="Seg_13092" s="T467">Dann ließen sie diese Rentiere gehen, sie trieben sie, und er auch…</ta>
            <ta e="T481" id="Seg_13093" s="T478">Sie gaben ihm ein Zelt.</ta>
            <ta e="T490" id="Seg_13094" s="T481">Er ging in das Zelt hinein, auf der Vorderseite der bewaldeten Landzunge ging er ins Zelt hinein.</ta>
            <ta e="T495" id="Seg_13095" s="T490">Er stellte das Zelt mit seiner Frau auf.</ta>
            <ta e="T501" id="Seg_13096" s="T495">Natürlich würde ein reicher Nenze kaum kein Zelt haben.</ta>
            <ta e="T506" id="Seg_13097" s="T501">Das [war] ein Zelt mit einer neuen Zeltabdeckung.</ta>
            <ta e="T975" id="Seg_13098" s="T506">(Sie machten?) es größer.</ta>
            <ta e="T976" id="Seg_13099" s="T975">[NN:] (…) sprich.</ta>
            <ta e="T990" id="Seg_13100" s="T976">[NN:] Oma? </ta>
            <ta e="T977" id="Seg_13101" s="T990">[NEP:] Ja?</ta>
            <ta e="T514" id="Seg_13102" s="T977">[NN:] Zieh [den Kopf] ein.</ta>
            <ta e="T515" id="Seg_13103" s="T514">[KuAI:] Lass es uns jetzt probieren.</ta>
            <ta e="T978" id="Seg_13104" s="T515">[NN:] … und im Radio…</ta>
            <ta e="T979" id="Seg_13105" s="T978">[NN:] Offenbar fragt sie.</ta>
            <ta e="T980" id="Seg_13106" s="T979">[NN:] Sie spricht gut, sie erzählt [die Geschichte gut].</ta>
            <ta e="T981" id="Seg_13107" s="T980">[NN:] Warte, mach keinen Krach.</ta>
            <ta e="T982" id="Seg_13108" s="T981">[NN:] Als ob sie taub ist (?).</ta>
            <ta e="T983" id="Seg_13109" s="T982">[NN:] Er/sie scheint zuzuhören, vielleicht spricht sie schlecht.</ta>
            <ta e="T974" id="Seg_13110" s="T983">[NN:] Leise [= ohne Wörter].</ta>
            <ta e="T542" id="Seg_13111" s="T974">[NEP:] Dann übernachtete dieser Junge mit seiner Frau.</ta>
            <ta e="T549" id="Seg_13112" s="T542">Sie übernachteten, dann sagte er so zu seiner Frau:</ta>
            <ta e="T556" id="Seg_13113" s="T549">"Ich gehe weiter flussaufwärts zu diesen meinen Leuten."</ta>
            <ta e="T559" id="Seg_13114" s="T556">Sie bauten das Zelt auf [=machten es fertig], oder was machten sie.</ta>
            <ta e="T564" id="Seg_13115" s="T559">Er ging weiter flussaufwärts, er spannte drei Bullen an.</ta>
            <ta e="T569" id="Seg_13116" s="T564">Ungefähr dreißig Rentiere ließ er bei sich.</ta>
            <ta e="T576" id="Seg_13117" s="T569">Der Junge ging flussaufwärts, die Frau blieb alleine.</ta>
            <ta e="T578" id="Seg_13118" s="T576">Sie sitzt.</ta>
            <ta e="T581" id="Seg_13119" s="T578">Bald…</ta>
            <ta e="T588" id="Seg_13120" s="T581">Einmal ging die Sonne zum Abend [=wurde es Abend].</ta>
            <ta e="T595" id="Seg_13121" s="T588">Am Abend (war es ruhig um den Jungen?).</ta>
            <ta e="T604" id="Seg_13122" s="T595">Einmal (kamen?) die Leute von weiter flussabwärts.</ta>
            <ta e="T608" id="Seg_13123" s="T604">[Die Leute] des alten Poni kamen.</ta>
            <ta e="T613" id="Seg_13124" s="T608">Zehn Leute, die die Rentiere trieben.</ta>
            <ta e="T621" id="Seg_13125" s="T613">Der alte Poni ist so sehend, so weise.</ta>
            <ta e="T630" id="Seg_13126" s="T621">Dann, warte, ist zu hören, dass dieser Junge kommt – seine Frau hört es von draußen.</ta>
            <ta e="T636" id="Seg_13127" s="T630">Man hört, dass er kam, und er band seine Rentiere fest.</ta>
            <ta e="T984" id="Seg_13128" s="T636">[NN:] (…).</ta>
            <ta e="T641" id="Seg_13129" s="T984">[NEP:] Er scheint gerade angekommen zu sein.</ta>
            <ta e="T651" id="Seg_13130" s="T641">Die Leute des alten Poni scheinen gerade angekommen zu sein, seine Schlitten stehen so da.</ta>
            <ta e="T655" id="Seg_13131" s="T651">Da draußen, auf seinem Land.</ta>
            <ta e="T662" id="Seg_13132" s="T655">Er ging hinein, er ging ins Zelt hinein, der Nenze, an der Tür stehend…</ta>
            <ta e="T668" id="Seg_13133" s="T662">An der Tür schüttelte er seine Beine ab.</ta>
            <ta e="T671" id="Seg_13134" s="T668">Er schüttelte seine Stiefel ab.</ta>
            <ta e="T675" id="Seg_13135" s="T671">Er schüttelte seine Stiefel auf der gefrorenen Seite ab.</ta>
            <ta e="T682" id="Seg_13136" s="T675">So sagt er, sich zum alten Poni wendend sagt er:</ta>
            <ta e="T688" id="Seg_13137" s="T682">"Dein Mund ist wie der Mund eines Eichelhähers."</ta>
            <ta e="T689" id="Seg_13138" s="T688">Warte.</ta>
            <ta e="T694" id="Seg_13139" s="T689">Er stürzte mit seiner Faust nach vorne.</ta>
            <ta e="T698" id="Seg_13140" s="T694">Er traf [=liebkoste] mit einer Hand [=mit der Hälfte seiner Hände], er traf ihn.</ta>
            <ta e="T704" id="Seg_13141" s="T698">Alle Leute des alten Poni streckte er nieder [=auf die Seite].</ta>
            <ta e="T706" id="Seg_13142" s="T704">Einige starben.</ta>
            <ta e="T718" id="Seg_13143" s="T706">Der alte Poni blieb am Leben, zwei Nenzen blieben am Leben, er warf [sie] nach draußen, dieser Junge.</ta>
            <ta e="T722" id="Seg_13144" s="T718">Er traf [=liebkoste] [sie] einmal.</ta>
            <ta e="T730" id="Seg_13145" s="T722">Alle im Zelt (schissen?) sich in ihre Hosen [=Kleidung], diese Gestorbenen.</ta>
            <ta e="T731" id="Seg_13146" s="T730">[Schimpfwort]</ta>
            <ta e="T744" id="Seg_13147" s="T731">Dann warf er sie hinaus, auf die Erde um das Zelt herum, und jener Schlitten(zug)? ist draußen festgebunden.</ta>
            <ta e="T753" id="Seg_13148" s="T744">Er warf auch den alten Poni hinaus, nun, er warf alle hinaus.</ta>
            <ta e="T763" id="Seg_13149" s="T753">Er setzte sich, er (nahm?) seine Pelzoberkleidung (ab?), er aß und ging schlafen.</ta>
            <ta e="T770" id="Seg_13150" s="T763">Diese seine Leute liegen draußen, festgebunden.</ta>
            <ta e="T773" id="Seg_13151" s="T770">Wie sie festgebunden waren, so.</ta>
            <ta e="T778" id="Seg_13152" s="T773">Er übernachtete, am nächsten Morgen stand er auf.</ta>
            <ta e="T790" id="Seg_13153" s="T778">Der alte Poni, sein Gesicht und sein Kopf sind so angeschwollen, seine Augen (glühen?).</ta>
            <ta e="T796" id="Seg_13154" s="T790">Diese Nenzen waren besser [=schlechter].</ta>
            <ta e="T803" id="Seg_13155" s="T796">So sagte dieser Junge zu diesen seinen Nenzen:</ta>
            <ta e="T811" id="Seg_13156" s="T803">"Lade deine Leute auf deine Schlitten, binde sie fest und fahr nach Hause."</ta>
            <ta e="T813" id="Seg_13157" s="T811">Auf ihre eigenen Schlitten.</ta>
            <ta e="T830" id="Seg_13158" s="T813">Dann gehen die beiden auf der Straße (mit dem letzten Atemzug?) nach Hause, vielleicht essen sie oder nicht.</ta>
            <ta e="T836" id="Seg_13159" s="T830">Diese Leute, die an die Schlitten gebunden waren.</ta>
            <ta e="T847" id="Seg_13160" s="T836">Dann, (die [Schlitten] verbunden habend?), brachte er diese Leute, diese toten Leute nach Hause.</ta>
            <ta e="T849" id="Seg_13161" s="T847">Nach Norden/flussabwärts nach Hause.</ta>
            <ta e="T854" id="Seg_13162" s="T849">Den alten Poni ([die Schlitten] festgebunden haben?) brachte er weg.</ta>
            <ta e="T858" id="Seg_13163" s="T854">Er scheint nur ein bisschen Atem zu haben.</ta>
            <ta e="T861" id="Seg_13164" s="T858">So fuhren sie davon.</ta>
            <ta e="T865" id="Seg_13165" s="T861">Dann fuhren sie gut dort weg.</ta>
            <ta e="T875" id="Seg_13166" s="T865">Er sagt zu seiner Frau: "Bau das Zelt ab, damit wir weiter zu den Leuten fahren."</ta>
            <ta e="T884" id="Seg_13167" s="T875">Seine Großeltern wussten nichts.</ta>
            <ta e="T891" id="Seg_13168" s="T884">Er ging weiter, er selbst spricht nicht.</ta>
            <ta e="T898" id="Seg_13169" s="T891">Seine Frau wird es den Großeltern erzählen.</ta>
            <ta e="T905" id="Seg_13170" s="T898">Die Leute des alten Poni sind nach Hause gefahren, sagt sie.</ta>
            <ta e="T906" id="Seg_13171" s="T905">Warte.</ta>
            <ta e="T911" id="Seg_13172" s="T906">Dann irgendwie…</ta>
            <ta e="T985" id="Seg_13173" s="T911">Sie wissen nicht.</ta>
            <ta e="T986" id="Seg_13174" s="T985">[NN:] Sprich nach unten.</ta>
            <ta e="T987" id="Seg_13175" s="T986">[NN:] Nach unten zum Aufnahmegerät.</ta>
            <ta e="T932" id="Seg_13176" s="T987">[NEP:] Sie [gingen] weiter nach Norden/flussabwärts, gingen in das Zelt des Großvaters, nachdem sie die Schlitten angebunden hatten, er stellte sie dorthin.</ta>
            <ta e="T936" id="Seg_13177" s="T932">Sie fuhren weg, sie fuhren nach Hause.</ta>
            <ta e="T944" id="Seg_13178" s="T936">Der alte Poni erreichte kaum sein Zuhause im Norden/flussabwärts, er starb.</ta>
            <ta e="T949" id="Seg_13179" s="T944">Diese beiden Leute blieben am Leben.</ta>
            <ta e="T988" id="Seg_13180" s="T949">Der alte Poni, es gibt so einen alten Poni mit seinen Leuten.</ta>
            <ta e="T989" id="Seg_13181" s="T988">[NN:] Erzähl etwas anderes, etwas anderes.</ta>
            <ta e="T964" id="Seg_13182" s="T989">[NN:] Erzähl etwas anderes.</ta>
            <ta e="T965" id="Seg_13183" s="T964">[NN:] Das ist die ganze Geschichte.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T968" id="Seg_13184" s="T0">Лентяю всегда праздник.</ta>
            <ta e="T6" id="Seg_13185" s="T968">Был один ребёнок.</ta>
            <ta e="T13" id="Seg_13186" s="T6">Это хоть как-то, ну я об этом [всегда] рассказываю. </ta>
            <ta e="T19" id="Seg_13187" s="T13">Это предание, предание, не сказка.</ta>
            <ta e="T969" id="Seg_13188" s="T19">Подождите, подождите, пусть она подождёт, я проверю.</ta>
            <ta e="T970" id="Seg_13189" s="T969">Под тобой … селькупы туда</ta>
            <ta e="T971" id="Seg_13190" s="T970">Это тебя укажет, ты разговор свой приостанавливай.</ta>
            <ta e="T972" id="Seg_13191" s="T971">сними</ta>
            <ta e="T973" id="Seg_13192" s="T972">Медленно говори.</ta>
            <ta e="T41" id="Seg_13193" s="T973">[связка] Этот старик жил.</ta>
            <ta e="T42" id="Seg_13194" s="T41">Заболел.</ta>
            <ta e="T45" id="Seg_13195" s="T42">Этот старик селькуп.</ta>
            <ta e="T53" id="Seg_13196" s="T45">На этой половине [реки] лабаз, на другой половине к северу к реке жил. </ta>
            <ta e="T992" id="Seg_13197" s="T53">(Как будто?) без деревьев земле (?) </ta>
            <ta e="T61" id="Seg_13198" s="T59">Жил.</ta>
            <ta e="T62" id="Seg_13199" s="T61">Заболел.</ta>
            <ta e="T68" id="Seg_13200" s="T62">Сын, один сын, без никого [в семье], один без никого, половины. </ta>
            <ta e="T74" id="Seg_13201" s="T68">Ни дочки нет, никого нет.</ta>
            <ta e="T83" id="Seg_13202" s="T74">Потом отец заболел этого мальчика, этого мальчика. </ta>
            <ta e="T90" id="Seg_13203" s="T83">Там живёт в отца, матери пологе.</ta>
            <ta e="T96" id="Seg_13204" s="T90">С той стороны два ненца-брата были.</ta>
            <ta e="T101" id="Seg_13205" s="T96">Ненец-старик куда денется?</ta>
            <ta e="T110" id="Seg_13206" s="T101">Или жил, куда же (?), этот старик умер. </ta>
            <ta e="T122" id="Seg_13207" s="T110">Умер. Тот потом тот парень…. ненец-старик куда денется?</ta>
            <ta e="T125" id="Seg_13208" s="T122">На ту сторону к брату добрался. </ta>
            <ta e="T127" id="Seg_13209" s="T125">Старик умер.</ta>
            <ta e="T129" id="Seg_13210" s="T127">Похоронили.</ta>
            <ta e="T138" id="Seg_13211" s="T129">Потом бабушка, эта бабушка заболела, бабушка умерла.</ta>
            <ta e="T145" id="Seg_13212" s="T138">Мужчины с женщиной сын без родни, один остался.</ta>
            <ta e="T147" id="Seg_13213" s="T145">Куда он денется?</ta>
            <ta e="T157" id="Seg_13214" s="T147">Этого ненца старика потом на другую сторону переправил этот сирота.</ta>
            <ta e="T170" id="Seg_13215" s="T157">Большой сын… всякий… Как Гена с кем-то.</ta>
            <ta e="T173" id="Seg_13216" s="T170">Такой сын был.</ta>
            <ta e="T177" id="Seg_13217" s="T173">На другую сторону переправил, оленей у него нету.</ta>
            <ta e="T182" id="Seg_13218" s="T177">Без оленей ногами жили отец с детьми.</ta>
            <ta e="T188" id="Seg_13219" s="T182">Жили, жили, потом на другую сторону переправились.</ta>
            <ta e="T199" id="Seg_13220" s="T188">На другую сторону переправился этот мальчик в дом ненца старика, у ненца детей нет.</ta>
            <ta e="T201" id="Seg_13221" s="T199">Жена без детей.</ta>
            <ta e="T208" id="Seg_13222" s="T201">У среднего брата одна дочь одинокая.</ta>
            <ta e="T212" id="Seg_13223" s="T208">С севера сюда два ненца [пришли].</ta>
            <ta e="T215" id="Seg_13224" s="T212">Они с земли без деревьев.</ta>
            <ta e="T226" id="Seg_13225" s="T215">Пони-старик оленей всего племени всех перегнали.</ta>
            <ta e="T227" id="Seg_13226" s="T226">Подожди.</ta>
            <ta e="T238" id="Seg_13227" s="T227">Так пришли эти два брата, эти два брата живут, эти два старика-брата вдвоём.</ta>
            <ta e="T241" id="Seg_13228" s="T238">Четыреста оленей.</ta>
            <ta e="T245" id="Seg_13229" s="T241">Этого старика оленей забрать.</ta>
            <ta e="T251" id="Seg_13230" s="T245">Со своими людьми придёт этот Пони-ира, это его имя.</ta>
            <ta e="T253" id="Seg_13231" s="T251">Подожди.</ta>
            <ta e="T258" id="Seg_13232" s="T253">Придя, переночевали в их доме.</ta>
            <ta e="T266" id="Seg_13233" s="T258">Этого парня не знают, как он будет разговаривать?</ta>
            <ta e="T274" id="Seg_13234" s="T266">Он правильно или как живёт.</ta>
            <ta e="T277" id="Seg_13235" s="T274">Сейчас как будешь знать?</ta>
            <ta e="T279" id="Seg_13236" s="T277">В стороне живущий.</ta>
            <ta e="T282" id="Seg_13237" s="T279">Переночевали.</ta>
            <ta e="T289" id="Seg_13238" s="T282">Три с половиной дня шли.</ta>
            <ta e="T296" id="Seg_13239" s="T289">Пони-ира с десятью людьми пришёл.</ta>
            <ta e="T300" id="Seg_13240" s="T296">Чтоб этих оленей пригнать.</ta>
            <ta e="T308" id="Seg_13241" s="T300">Эти люди этих братьев оленей гнали.</ta>
            <ta e="T319" id="Seg_13242" s="T308">Сколько домов новых домов сколько всего [их скраба] собрали.</ta>
            <ta e="T328" id="Seg_13243" s="T319">Раньше северных ненцев оленей собрали, всех взяли. </ta>
            <ta e="T331" id="Seg_13244" s="T328">Так сделали.</ta>
            <ta e="T336" id="Seg_13245" s="T331">Домой пошёл этот ненецкий строй [в виде стрелы].</ta>
            <ta e="T341" id="Seg_13246" s="T336">На третий день придём.</ta>
            <ta e="T346" id="Seg_13247" s="T341">Вы никуда не ходите.</ta>
            <ta e="T356" id="Seg_13248" s="T346">На второй день рассвело, этот парень так к деду обращается.</ta>
            <ta e="T359" id="Seg_13249" s="T356">К этому гостю деду.</ta>
            <ta e="T365" id="Seg_13250" s="T359">Впереди там что-то есть.</ta>
            <ta e="T376" id="Seg_13251" s="T365">Четыреста оленей чтоб стояли, там тундра есть, туда отпустим.</ta>
            <ta e="T384" id="Seg_13252" s="T376">Ненцы придут оленей начнут загонять.</ta>
            <ta e="T391" id="Seg_13253" s="T384">Оленье пастбище всю зиму перезимовать куда денем?</ta>
            <ta e="T395" id="Seg_13254" s="T391">Этот парень так говорит:</ta>
            <ta e="T400" id="Seg_13255" s="T395">так говорит</ta>
            <ta e="T406" id="Seg_13256" s="T400">Мне новый, новый дом есть? </ta>
            <ta e="T412" id="Seg_13257" s="T406">Дед, новый дом есть?</ta>
            <ta e="T415" id="Seg_13258" s="T412">Мне отдай. </ta>
            <ta e="T425" id="Seg_13259" s="T415">А вот та брата дочь, дед эту [девушку] заставляет (=отдаёт замуж) этому парню.</ta>
            <ta e="T439" id="Seg_13260" s="T425">Я (просто?) приподниму чум, впереди лесной перешеек будет, в переднюю сторону поставлю чум.</ta>
            <ta e="T443" id="Seg_13261" s="T439">Новый чум принёс.</ta>
            <ta e="T444" id="Seg_13262" s="T443">Отпустили.</ta>
            <ta e="T445" id="Seg_13263" s="T444">Подожди.</ta>
            <ta e="T447" id="Seg_13264" s="T445">Отпустили.</ta>
            <ta e="T457" id="Seg_13265" s="T447">Устал, на второй, на третий день Пони-ира [со своими людьми] придёт к ним.</ta>
            <ta e="T467" id="Seg_13266" s="T457">Эти два ненца [хотят] остаться, два брата сегодня вроде.</ta>
            <ta e="T478" id="Seg_13267" s="T467">Потом распустили этих оленей загнали, и он тоже, что я рассказываю. </ta>
            <ta e="T481" id="Seg_13268" s="T478">Чум дали.</ta>
            <ta e="T490" id="Seg_13269" s="T481">В чум зашёл, на этом лесном перешейке в передней стороне в чум зашёл. </ta>
            <ta e="T495" id="Seg_13270" s="T490">Вот и поставили чум (=обосновались) с этой женой.</ta>
            <ta e="T501" id="Seg_13271" s="T495">У богатого ненца разве чума нет?</ta>
            <ta e="T506" id="Seg_13272" s="T501">Из нового нюка чум был. </ta>
            <ta e="T975" id="Seg_13273" s="T506">Побольше соорудили.</ta>
            <ta e="T976" id="Seg_13274" s="T975">(…) разговаривай.</ta>
            <ta e="T514" id="Seg_13275" s="T977">Пониже наклони [голову].</ta>
            <ta e="T979" id="Seg_13276" s="T978">спрашивает</ta>
            <ta e="T980" id="Seg_13277" s="T979">и так хорошо разговаривает, рассказывает.</ta>
            <ta e="T981" id="Seg_13278" s="T980">Подожди, не шуми.</ta>
            <ta e="T982" id="Seg_13279" s="T981">глухой</ta>
            <ta e="T983" id="Seg_13280" s="T982">Слушает, может быть плохо говорит.</ta>
            <ta e="T974" id="Seg_13281" s="T983">без слов (=тихо)</ta>
            <ta e="T542" id="Seg_13282" s="T974">В общем переночевали этот парень с женой.</ta>
            <ta e="T549" id="Seg_13283" s="T542">Переночевали, на следующее утро так обращается к жене.</ta>
            <ta e="T556" id="Seg_13284" s="T549">Я вперёд к тем людям схожу.</ta>
            <ta e="T559" id="Seg_13285" s="T556">Обустроились или куда делись.</ta>
            <ta e="T564" id="Seg_13286" s="T559">Вперёд поехал, три оленя запряг.</ta>
            <ta e="T569" id="Seg_13287" s="T564">Тридцать оленей оставил себе.</ta>
            <ta e="T576" id="Seg_13288" s="T569">Парень вперёд уехал, жена одна осталась.</ta>
            <ta e="T578" id="Seg_13289" s="T576">Сидит.</ta>
            <ta e="T581" id="Seg_13290" s="T578">Скоро…</ta>
            <ta e="T588" id="Seg_13291" s="T581">В одно время солнце к вечеру пошло.</ta>
            <ta e="T595" id="Seg_13292" s="T588">Вечереет, от парня тишина впереди.</ta>
            <ta e="T604" id="Seg_13293" s="T595">В одно время люди появились с севера.</ta>
            <ta e="T608" id="Seg_13294" s="T604">Дочку Пони-иры привезли.</ta>
            <ta e="T613" id="Seg_13295" s="T608">Десять человек - которые оленей перегоняли.</ta>
            <ta e="T621" id="Seg_13296" s="T613">Пони-ира такой (вперёд слышащий?), такой умный был. </ta>
            <ta e="T630" id="Seg_13297" s="T621">Потом подожди, этот парень приехал - с улицы слышит жена.</ta>
            <ta e="T636" id="Seg_13298" s="T630">Приехав, оленей привязал, оленей.</ta>
            <ta e="T984" id="Seg_13299" s="T636">Что-то течёт.</ta>
            <ta e="T641" id="Seg_13300" s="T984">Приехал.</ta>
            <ta e="T651" id="Seg_13301" s="T641">Пони-ира приехал(и), нарты так стоят.</ta>
            <ta e="T655" id="Seg_13302" s="T651">На его территории.</ta>
            <ta e="T662" id="Seg_13303" s="T655">Зашёл, в чум зашёл, у дверного проёма встал ненец.</ta>
            <ta e="T668" id="Seg_13304" s="T662">У дверного проёма ноги отряхнул.</ta>
            <ta e="T671" id="Seg_13305" s="T668">Бокари отряхнул.</ta>
            <ta e="T675" id="Seg_13306" s="T671">В замёрзшую сторону (=где замёрзло) бокари отряхнул.</ta>
            <ta e="T682" id="Seg_13307" s="T675">Так говорит вперёд повернувшись к Пони-ира.</ta>
            <ta e="T688" id="Seg_13308" s="T682">Рот твой как будто сойки рот. </ta>
            <ta e="T689" id="Seg_13309" s="T688">Подожди.</ta>
            <ta e="T694" id="Seg_13310" s="T689">Вперёд побежал с кулаком.</ta>
            <ta e="T698" id="Seg_13311" s="T694">Прошёлся (=погладил) одной рукой (=половиной из 2 рук) прошёлся (=погладил).</ta>
            <ta e="T704" id="Seg_13312" s="T698">Пони-иру и всех набок ударил. </ta>
            <ta e="T706" id="Seg_13313" s="T704">Некоторые умерли.</ta>
            <ta e="T718" id="Seg_13314" s="T706">Пони-ира жив остался, два ненца живы остались, на улицу выбросил этот парень.</ta>
            <ta e="T722" id="Seg_13315" s="T718">Один раз кулаком прошёлся (=погладил).</ta>
            <ta e="T730" id="Seg_13316" s="T722">Ещё дома внутри пальто какнули эти умершие.</ta>
            <ta e="T731" id="Seg_13317" s="T730">[матерится]</ta>
            <ta e="T744" id="Seg_13318" s="T731">Потом на улицу выкинул на территорию, а караван нарт на улице завязан. </ta>
            <ta e="T753" id="Seg_13319" s="T744">Пони-иру тоже на улицу выбросил, всех на улицу выбросил.</ta>
            <ta e="T763" id="Seg_13320" s="T753">Сел, малицу снял, поел, спать лёг.</ta>
            <ta e="T770" id="Seg_13321" s="T763">Те люди на улице лежат связанные.</ta>
            <ta e="T773" id="Seg_13322" s="T770">Как завязаны, так.</ta>
            <ta e="T778" id="Seg_13323" s="T773">Переночевал, на следующее утро встал.</ta>
            <ta e="T790" id="Seg_13324" s="T778">Пони-ира так лицо голова все опухли, кое-как глаза видно (=светятся).</ta>
            <ta e="T796" id="Seg_13325" s="T790">Те ненцы ещё получше были.</ta>
            <ta e="T803" id="Seg_13326" s="T796">Так сказал этот парень этим ненцам.</ta>
            <ta e="T811" id="Seg_13327" s="T803">Своих людей домой увези на сани загрузив, привязав.</ta>
            <ta e="T813" id="Seg_13328" s="T811">На свои сани.</ta>
            <ta e="T830" id="Seg_13329" s="T813">Потом те двое на улице на последнем дыхании (?) домой зайдут, или кушают или нет. </ta>
            <ta e="T836" id="Seg_13330" s="T830">Эти люди которые привязанные на нартах.</ta>
            <ta e="T847" id="Seg_13331" s="T836">Там домой соединив [нарты], людей умерших людей так увёз домой. </ta>
            <ta e="T849" id="Seg_13332" s="T847">На север домой.</ta>
            <ta e="T854" id="Seg_13333" s="T849">Пони-иру тоже привязав [нарты] увёз.</ta>
            <ta e="T858" id="Seg_13334" s="T854">Ну немного души.</ta>
            <ta e="T861" id="Seg_13335" s="T858">Уехали.</ta>
            <ta e="T865" id="Seg_13336" s="T861">Потом хорошо поехали.</ta>
            <ta e="T875" id="Seg_13337" s="T865">Жене так говорит: Чум выкопай вперёд, чтобы мы увезли людям.</ta>
            <ta e="T884" id="Seg_13338" s="T875">Не знали они его дед с бабкой.</ta>
            <ta e="T891" id="Seg_13339" s="T884">Вперёд пошёл, сам не разговаривает.</ta>
            <ta e="T898" id="Seg_13340" s="T891">Эта жена скажет отцу и дедам.</ta>
            <ta e="T905" id="Seg_13341" s="T898">Пони-ира с кем-то домой съездили будто.</ta>
            <ta e="T906" id="Seg_13342" s="T905">Подожди.</ta>
            <ta e="T911" id="Seg_13343" s="T906">Потом (…)</ta>
            <ta e="T985" id="Seg_13344" s="T911">(?) они не знают.</ta>
            <ta e="T986" id="Seg_13345" s="T985">Вниз разговаривай.</ta>
            <ta e="T987" id="Seg_13346" s="T986">Вниз к радио.</ta>
            <ta e="T932" id="Seg_13347" s="T987">Вперёд в северную сторону) зашли, они туда в деда чум зашли, нарты обмотав, там же и поставили [нарты].</ta>
            <ta e="T936" id="Seg_13348" s="T932">Они уехали, домой уехали.</ta>
            <ta e="T944" id="Seg_13349" s="T936">Пони-ира только домой на севере добрался, умер.</ta>
            <ta e="T949" id="Seg_13350" s="T944">Те двое людей жили [=остались живы].</ta>
            <ta e="T988" id="Seg_13351" s="T949">Пони-ира, Пони-ира с людьми там есть. </ta>
            <ta e="T989" id="Seg_13352" s="T988">Другое скажи, другое, другое. </ta>
            <ta e="T964" id="Seg_13353" s="T989">Другое скажи.</ta>
            <ta e="T965" id="Seg_13354" s="T964">Тут рассказанное всё и есть.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T13" id="Seg_13355" s="T6">[BrM:] Tentative analysis of ket, qət.</ta>
            <ta e="T970" id="Seg_13356" s="T969">[BrM:] Said by someone else.</ta>
            <ta e="T971" id="Seg_13357" s="T970">[BrM:] Said by someone else to the storyteller.</ta>
            <ta e="T972" id="Seg_13358" s="T971">‎[BrM:] A very rough analysis, one more check with native speaker needed.</ta>
            <ta e="T41" id="Seg_13359" s="T973">[IES:] Pija - some kind of linkage.</ta>
            <ta e="T992" id="Seg_13360" s="T53">[BrM:] Tentative analysis of 'toːqɨlʼ'.</ta>
            <ta e="T157" id="Seg_13361" s="T147">[IES:] puːčʼɨmpatɨ = puː-tɨ-mpa-ti 'cross-TR-PST.NAR-3SG.O'</ta>
            <ta e="T212" id="Seg_13362" s="T208">[BrM:] Alternative interpretation: from down the river upstream they came.</ta>
            <ta e="T245" id="Seg_13363" s="T241">[KJ:] Probably part of the next sentence</ta>
            <ta e="T251" id="Seg_13364" s="T245">[BrM:] Or he has already come (INFER instead of FUT)?</ta>
            <ta e="T258" id="Seg_13365" s="T253">[BrM:] Nominative of 'mɔːt'?</ta>
            <ta e="T300" id="Seg_13366" s="T296">[KJ:] Probably part of the previous sentence</ta>
            <ta e="T439" id="Seg_13367" s="T425">[IES:] the front side = opposite to the North</ta>
            <ta e="T976" id="Seg_13368" s="T975">[BrM:] 091-101: converstaion between several people, hard to analyse, so the transcription is very approximate.</ta>
            <ta e="T514" id="Seg_13369" s="T977">[BrM:] Rough transcription and tentative analysis of 'uːkaltäš'.</ta>
            <ta e="T542" id="Seg_13370" s="T974">[Br:] The story goes on.</ta>
            <ta e="T595" id="Seg_13371" s="T588">[BrM:] Tentative transcription.</ta>
            <ta e="T604" id="Seg_13372" s="T595">[BrM:] Tentative transcription and analysis.</ta>
            <ta e="T608" id="Seg_13373" s="T604">[BrM:] This daughter is never mentioned afterwards. Cf. 113: maybe 'nälʼap' = 'na jap', then the translation would be '(The men of) the old man Poni came', which seems to fit the story better.</ta>
            <ta e="T984" id="Seg_13374" s="T636">[BrM:] Said by someone else.</ta>
            <ta e="T668" id="Seg_13375" s="T662">[IES:] qät – 'usually'?</ta>
            <ta e="T698" id="Seg_13376" s="T694">[BrM:] Unsure analysis of 'pɛlaj', doesn't fit the syntax.</ta>
            <ta e="T744" id="Seg_13377" s="T731">[BrM:] Tentative analysis of 'sɔːralɨmmɨntɔːtɨt'.</ta>
            <ta e="T830" id="Seg_13378" s="T813">[BrM:] Tentative transcription, analysis, translation. Usage of infinitives?</ta>
            <ta e="T861" id="Seg_13379" s="T858">[BrM:] Təmol = təp mol 'he, he says'?</ta>
            <ta e="T985" id="Seg_13380" s="T911">[BrM:] Other people are speaking as well.</ta>
            <ta e="T986" id="Seg_13381" s="T985">‎</ta>
            <ta e="T932" id="Seg_13382" s="T987">[IES:] ɨpalnoj = ɨpqəlɛjnɔːt (entered). [BrM:] Tentative analysis of 'təpɨjat'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T968" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T969" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T970" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T971" />
            <conversion-tli id="T34" />
            <conversion-tli id="T972" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T973" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T992" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T993" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T975" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T976" />
            <conversion-tli id="T991" />
            <conversion-tli id="T990" />
            <conversion-tli id="T510" />
            <conversion-tli id="T977" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T978" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T979" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T980" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T981" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T982" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T983" />
            <conversion-tli id="T535" />
            <conversion-tli id="T974" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T967" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T984" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T985" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T986" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T987" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T925" />
            <conversion-tli id="T926" />
            <conversion-tli id="T927" />
            <conversion-tli id="T928" />
            <conversion-tli id="T929" />
            <conversion-tli id="T930" />
            <conversion-tli id="T931" />
            <conversion-tli id="T932" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T935" />
            <conversion-tli id="T936" />
            <conversion-tli id="T937" />
            <conversion-tli id="T938" />
            <conversion-tli id="T939" />
            <conversion-tli id="T940" />
            <conversion-tli id="T941" />
            <conversion-tli id="T942" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T945" />
            <conversion-tli id="T946" />
            <conversion-tli id="T947" />
            <conversion-tli id="T948" />
            <conversion-tli id="T949" />
            <conversion-tli id="T950" />
            <conversion-tli id="T951" />
            <conversion-tli id="T952" />
            <conversion-tli id="T953" />
            <conversion-tli id="T954" />
            <conversion-tli id="T955" />
            <conversion-tli id="T988" />
            <conversion-tli id="T956" />
            <conversion-tli id="T957" />
            <conversion-tli id="T958" />
            <conversion-tli id="T959" />
            <conversion-tli id="T960" />
            <conversion-tli id="T989" />
            <conversion-tli id="T961" />
            <conversion-tli id="T962" />
            <conversion-tli id="T963" />
            <conversion-tli id="T964" />
            <conversion-tli id="T965" />
            <conversion-tli id="T966" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
