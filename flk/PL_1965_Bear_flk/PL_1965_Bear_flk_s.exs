<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PL_1965_Bear_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PL_1965_Bear_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">76</ud-information>
            <ud-information attribute-name="# HIAT:w">58</ud-information>
            <ud-information attribute-name="# e">58</ud-information>
            <ud-information attribute-name="# HIAT:u">16</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PL">
            <abbreviation>PL</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PL"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T58" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Čʼäptä</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">loːs-ira</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">čʼɔːtɨ</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T6" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">İːjalʼan</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">sɔːntɨrpɔːtɨn</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">poːqɨn</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_26" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">Ukkɨr</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">čʼanton</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">tüŋɨntɨ</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">losɨ-ira</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_41" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">İjalʼatɨm</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">saqalpatɨ</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">meːšak</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">šunʼäntɨ</ts>
                  <nts id="Seg_53" n="HIAT:ip">.</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_56" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_58" n="HIAT:w" s="T14">Moqɨna</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_61" n="HIAT:w" s="T15">qännɨmpatɨ</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">imaqotaqɨntɨ</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17">kätɨmpat</ts>
                  <nts id="Seg_68" n="HIAT:ip">.</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_71" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_73" n="HIAT:w" s="T18">İjalʼatɨm</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_76" n="HIAT:w" s="T19">mušerät</ts>
                  <nts id="Seg_77" n="HIAT:ip">.</nts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_80" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_82" n="HIAT:w" s="T20">Mʼaːšaktɨ</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_85" n="HIAT:w" s="T21">poːqɨt</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_88" n="HIAT:w" s="T22">totɨmpa</ts>
                  <nts id="Seg_89" n="HIAT:ip">.</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_92" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_94" n="HIAT:w" s="T23">İːjalʼan</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_97" n="HIAT:w" s="T24">ınna</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_100" n="HIAT:w" s="T25">paktɨmpɔːtɨn</ts>
                  <nts id="Seg_101" n="HIAT:ip">.</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_104" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_106" n="HIAT:w" s="T26">Nätɨlʼa</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_109" n="HIAT:w" s="T27">ukkɨr</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_112" n="HIAT:w" s="T28">qalɨmmɨt</ts>
                  <nts id="Seg_113" n="HIAT:ip">.</nts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_116" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_118" n="HIAT:w" s="T29">Los-ira</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_121" n="HIAT:w" s="T30">meːšaktɨ</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_124" n="HIAT:w" s="T31">mɔːttɨ</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_127" n="HIAT:w" s="T32">tultɨmpatɨ</ts>
                  <nts id="Seg_128" n="HIAT:ip">.</nts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_131" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_133" n="HIAT:w" s="T33">İmaqotɨqɨntɨ</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_136" n="HIAT:w" s="T34">kətɨmpatɨ</ts>
                  <nts id="Seg_137" n="HIAT:ip">:</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_140" n="HIAT:w" s="T35">iːjalʼaiːlɨ</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_143" n="HIAT:w" s="T36">mušɨrätɨ</ts>
                  <nts id="Seg_144" n="HIAT:ip">.</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_147" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_149" n="HIAT:w" s="T37">İmaqota</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_152" n="HIAT:w" s="T38">qamtəıːmpatɨ</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_155" n="HIAT:w" s="T39">meːšaktɨ</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_158" n="HIAT:w" s="T40">čʼi</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_161" n="HIAT:w" s="T41">šünʼnʼantɨ</ts>
                  <nts id="Seg_162" n="HIAT:ip">.</nts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_165" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_167" n="HIAT:w" s="T42">Ukkɨr</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_170" n="HIAT:w" s="T43">nätalʼa</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_173" n="HIAT:w" s="T44">alʼčʼɨmmuntə</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_176" n="HIAT:w" s="T45">čʼi</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_179" n="HIAT:w" s="T46">šünʼnʼantɨ</ts>
                  <nts id="Seg_180" n="HIAT:ip">.</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_183" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_185" n="HIAT:w" s="T47">İmaqota</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_188" n="HIAT:w" s="T48">nʼenʼɨmɔːtpa</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_191" n="HIAT:w" s="T49">iraqɨntɨ</ts>
                  <nts id="Seg_192" n="HIAT:ip">.</nts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_195" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_197" n="HIAT:w" s="T50">Kätɨmpat</ts>
                  <nts id="Seg_198" n="HIAT:ip">:</nts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_201" n="HIAT:w" s="T51">Moqɨna</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_204" n="HIAT:w" s="T52">qäntätɨ</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_207" n="HIAT:w" s="T53">nätalʼa</ts>
                  <nts id="Seg_208" n="HIAT:ip">.</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_211" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_213" n="HIAT:w" s="T54">Loːs-ira</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_216" n="HIAT:w" s="T55">qännɨmpatɨ</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_219" n="HIAT:w" s="T56">nätalʼa</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_222" n="HIAT:w" s="T57">moqonɨ</ts>
                  <nts id="Seg_223" n="HIAT:ip">.</nts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T58" id="Seg_225" n="sc" s="T0">
               <ts e="T1" id="Seg_227" n="e" s="T0">Čʼäptä </ts>
               <ts e="T2" id="Seg_229" n="e" s="T1">loːs-ira </ts>
               <ts e="T3" id="Seg_231" n="e" s="T2">čʼɔːtɨ. </ts>
               <ts e="T4" id="Seg_233" n="e" s="T3">İːjalʼan </ts>
               <ts e="T5" id="Seg_235" n="e" s="T4">sɔːntɨrpɔːtɨn </ts>
               <ts e="T6" id="Seg_237" n="e" s="T5">poːqɨn. </ts>
               <ts e="T7" id="Seg_239" n="e" s="T6">Ukkɨr </ts>
               <ts e="T8" id="Seg_241" n="e" s="T7">čʼanton </ts>
               <ts e="T9" id="Seg_243" n="e" s="T8">tüŋɨntɨ </ts>
               <ts e="T10" id="Seg_245" n="e" s="T9">losɨ-ira. </ts>
               <ts e="T11" id="Seg_247" n="e" s="T10">İjalʼatɨm </ts>
               <ts e="T12" id="Seg_249" n="e" s="T11">saqalpatɨ </ts>
               <ts e="T13" id="Seg_251" n="e" s="T12">meːšak </ts>
               <ts e="T14" id="Seg_253" n="e" s="T13">šunʼäntɨ. </ts>
               <ts e="T15" id="Seg_255" n="e" s="T14">Moqɨna </ts>
               <ts e="T16" id="Seg_257" n="e" s="T15">qännɨmpatɨ </ts>
               <ts e="T17" id="Seg_259" n="e" s="T16">imaqotaqɨntɨ </ts>
               <ts e="T18" id="Seg_261" n="e" s="T17">kätɨmpat. </ts>
               <ts e="T19" id="Seg_263" n="e" s="T18">İjalʼatɨm </ts>
               <ts e="T20" id="Seg_265" n="e" s="T19">mušerät. </ts>
               <ts e="T21" id="Seg_267" n="e" s="T20">Mʼaːšaktɨ </ts>
               <ts e="T22" id="Seg_269" n="e" s="T21">poːqɨt </ts>
               <ts e="T23" id="Seg_271" n="e" s="T22">totɨmpa. </ts>
               <ts e="T24" id="Seg_273" n="e" s="T23">İːjalʼan </ts>
               <ts e="T25" id="Seg_275" n="e" s="T24">ınna </ts>
               <ts e="T26" id="Seg_277" n="e" s="T25">paktɨmpɔːtɨn. </ts>
               <ts e="T27" id="Seg_279" n="e" s="T26">Nätɨlʼa </ts>
               <ts e="T28" id="Seg_281" n="e" s="T27">ukkɨr </ts>
               <ts e="T29" id="Seg_283" n="e" s="T28">qalɨmmɨt. </ts>
               <ts e="T30" id="Seg_285" n="e" s="T29">Los-ira </ts>
               <ts e="T31" id="Seg_287" n="e" s="T30">meːšaktɨ </ts>
               <ts e="T32" id="Seg_289" n="e" s="T31">mɔːttɨ </ts>
               <ts e="T33" id="Seg_291" n="e" s="T32">tultɨmpatɨ. </ts>
               <ts e="T34" id="Seg_293" n="e" s="T33">İmaqotɨqɨntɨ </ts>
               <ts e="T35" id="Seg_295" n="e" s="T34">kətɨmpatɨ: </ts>
               <ts e="T36" id="Seg_297" n="e" s="T35">iːjalʼaiːlɨ </ts>
               <ts e="T37" id="Seg_299" n="e" s="T36">mušɨrätɨ. </ts>
               <ts e="T38" id="Seg_301" n="e" s="T37">İmaqota </ts>
               <ts e="T39" id="Seg_303" n="e" s="T38">qamtəıːmpatɨ </ts>
               <ts e="T40" id="Seg_305" n="e" s="T39">meːšaktɨ </ts>
               <ts e="T41" id="Seg_307" n="e" s="T40">čʼi </ts>
               <ts e="T42" id="Seg_309" n="e" s="T41">šünʼnʼantɨ. </ts>
               <ts e="T43" id="Seg_311" n="e" s="T42">Ukkɨr </ts>
               <ts e="T44" id="Seg_313" n="e" s="T43">nätalʼa </ts>
               <ts e="T45" id="Seg_315" n="e" s="T44">alʼčʼɨmmuntə </ts>
               <ts e="T46" id="Seg_317" n="e" s="T45">čʼi </ts>
               <ts e="T47" id="Seg_319" n="e" s="T46">šünʼnʼantɨ. </ts>
               <ts e="T48" id="Seg_321" n="e" s="T47">İmaqota </ts>
               <ts e="T49" id="Seg_323" n="e" s="T48">nʼenʼɨmɔːtpa </ts>
               <ts e="T50" id="Seg_325" n="e" s="T49">iraqɨntɨ. </ts>
               <ts e="T51" id="Seg_327" n="e" s="T50">Kätɨmpat: </ts>
               <ts e="T52" id="Seg_329" n="e" s="T51">Moqɨna </ts>
               <ts e="T53" id="Seg_331" n="e" s="T52">qäntätɨ </ts>
               <ts e="T54" id="Seg_333" n="e" s="T53">nätalʼa. </ts>
               <ts e="T55" id="Seg_335" n="e" s="T54">Loːs-ira </ts>
               <ts e="T56" id="Seg_337" n="e" s="T55">qännɨmpatɨ </ts>
               <ts e="T57" id="Seg_339" n="e" s="T56">nätalʼa </ts>
               <ts e="T58" id="Seg_341" n="e" s="T57">moqonɨ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_342" s="T0">PL_1965_Bear_flk.001 (001.001)</ta>
            <ta e="T6" id="Seg_343" s="T3">PL_1965_Bear_flk.002 (001.002)</ta>
            <ta e="T10" id="Seg_344" s="T6">PL_1965_Bear_flk.003 (001.003)</ta>
            <ta e="T14" id="Seg_345" s="T10">PL_1965_Bear_flk.004 (001.004)</ta>
            <ta e="T18" id="Seg_346" s="T14">PL_1965_Bear_flk.005 (001.005)</ta>
            <ta e="T20" id="Seg_347" s="T18">PL_1965_Bear_flk.006 (001.006)</ta>
            <ta e="T23" id="Seg_348" s="T20">PL_1965_Bear_flk.007 (001.007)</ta>
            <ta e="T26" id="Seg_349" s="T23">PL_1965_Bear_flk.008 (001.008)</ta>
            <ta e="T29" id="Seg_350" s="T26">PL_1965_Bear_flk.009 (001.009)</ta>
            <ta e="T33" id="Seg_351" s="T29">PL_1965_Bear_flk.010 (001.010)</ta>
            <ta e="T37" id="Seg_352" s="T33">PL_1965_Bear_flk.011 (001.011)</ta>
            <ta e="T42" id="Seg_353" s="T37">PL_1965_Bear_flk.012 (001.012)</ta>
            <ta e="T47" id="Seg_354" s="T42">PL_1965_Bear_flk.013 (001.013)</ta>
            <ta e="T50" id="Seg_355" s="T47">PL_1965_Bear_flk.014 (001.014)</ta>
            <ta e="T54" id="Seg_356" s="T50">PL_1965_Bear_flk.015 (001.015)</ta>
            <ta e="T58" id="Seg_357" s="T54">PL_1965_Bear_flk.016 (001.016)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_358" s="T0">′тʼӓптӓ лоз и′ра ′чоты.</ta>
            <ta e="T6" id="Seg_359" s="T3">′иjаlан ′сондырподын ′поɣын.</ta>
            <ta e="T10" id="Seg_360" s="T6">′уккырчандон тӱңынты ′лозы ′ира.</ta>
            <ta e="T14" id="Seg_361" s="T10">′иjаlа тым са′kалбаты ′мʼӓшʼак шʼу′нʼӓнды.</ta>
            <ta e="T18" id="Seg_362" s="T14">′моkына ′kӓнымпаты ′има′kотаɣ[k]ынты ′kӓтымпат.</ta>
            <ta e="T20" id="Seg_363" s="T18">и′jаlат ′тым ′мушʼе‵рʼӓт.</ta>
            <ta e="T23" id="Seg_364" s="T20">′мʼашʼакты ′поkыт ′тотымпа.</ta>
            <ta e="T26" id="Seg_365" s="T23">′иjаlам ′ина ′паkтым‵потын.</ta>
            <ta e="T29" id="Seg_366" s="T26">′нӓтылʼа ′уккыр ′kалымыт.</ta>
            <ta e="T33" id="Seg_367" s="T29">лос ′ира ′мʼӓшʼакты ′мо̄тъ ′тулдым‵паты.</ta>
            <ta e="T37" id="Seg_368" s="T33">′имаkоты kын′ты ′kъ̊тымпаты и′jаlа ′ӣлы ′мӯшʼи′рʼӓты.</ta>
            <ta e="T42" id="Seg_369" s="T37">′има‵kота ′kамтъ ӣмпаты ′мʼешʼакты чи шʼу′нʼанты.</ta>
            <ta e="T47" id="Seg_370" s="T42">′уккыр ′нӓталʼа ′аlчи‵мунтъ ′чишʼу‵нʼанты.</ta>
            <ta e="T50" id="Seg_371" s="T47">′има‵kота нʼӓнʼи′мотпа и′раkынты.</ta>
            <ta e="T54" id="Seg_372" s="T50">′kӓтымпат ′моkына ′kӓнтӓты ′нӓталʼа.</ta>
            <ta e="T58" id="Seg_373" s="T54">′ло̄с ′ӣра ′kӓнымпаты ′нӓталʼа ′моkоны.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T3" id="Seg_374" s="T0">čʼäptä loz ira čʼotɨ.</ta>
            <ta e="T6" id="Seg_375" s="T3">ijalʼan sondɨrpodɨn poqɨn.</ta>
            <ta e="T10" id="Seg_376" s="T6">ukkɨrčʼandon tüŋɨntɨ lozɨ ira.</ta>
            <ta e="T14" id="Seg_377" s="T10">ijalʼa tɨm saqalpatɨ mʼäšʼak šʼunʼändɨ.</ta>
            <ta e="T18" id="Seg_378" s="T14">moqɨna qänɨmpatɨ imaqotaqɨntɨ qätɨmpat.</ta>
            <ta e="T20" id="Seg_379" s="T18">ijalʼat tɨm mušʼerʼät.</ta>
            <ta e="T23" id="Seg_380" s="T20">mʼašʼaktɨ poqɨt totɨmpa.</ta>
            <ta e="T26" id="Seg_381" s="T23">ijalʼam ina paqtɨmpotɨn.</ta>
            <ta e="T29" id="Seg_382" s="T26">nätɨlʼa ukkɨr qalɨmɨt.</ta>
            <ta e="T33" id="Seg_383" s="T29">los ira mʼäšʼaktɨ moːtə tuldɨmpatɨ.</ta>
            <ta e="T37" id="Seg_384" s="T33">imaqotɨ qɨntɨ qətɨmpatɨ ijalʼa iːlɨ muːšʼirʼätɨ.</ta>
            <ta e="T42" id="Seg_385" s="T37">imaqota qamtə iːmpatɨ mʼešaktɨ čʼi šunʼantɨ.</ta>
            <ta e="T47" id="Seg_386" s="T42">ukkɨr nätalʼa alʼčʼimuntə čʼišʼunʼantɨ.</ta>
            <ta e="T50" id="Seg_387" s="T47">imaqota nʼänʼimotpa.</ta>
            <ta e="T54" id="Seg_388" s="T50">qätɨmpat moqɨna qäntätɨ nätalʼa.</ta>
            <ta e="T58" id="Seg_389" s="T54">loːs iːra qänɨmpatɨ nätalʼa moqonɨ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_390" s="T0">Čʼäptä loːs-ira čʼɔːtɨ. </ta>
            <ta e="T6" id="Seg_391" s="T3">İːjalʼan sɔːntɨrpɔːtɨn poːqɨn. </ta>
            <ta e="T10" id="Seg_392" s="T6">Ukkɨr čʼanton tüŋɨntɨ losɨ-ira. </ta>
            <ta e="T14" id="Seg_393" s="T10">İjalʼatɨm saqalpatɨ meːšak šunʼäntɨ. </ta>
            <ta e="T18" id="Seg_394" s="T14">Moqɨna qännɨmpatɨ imaqotaqɨntɨ kätɨmpat. </ta>
            <ta e="T20" id="Seg_395" s="T18">İjalʼatɨm mušerät. </ta>
            <ta e="T23" id="Seg_396" s="T20">Mʼaːšaktɨ poːqɨt totɨmpa. </ta>
            <ta e="T26" id="Seg_397" s="T23">İːjalʼan ınna paktɨmpɔːtɨn. </ta>
            <ta e="T29" id="Seg_398" s="T26">Nätɨlʼa ukkɨr qalɨmmɨt. </ta>
            <ta e="T33" id="Seg_399" s="T29">Los-ira meːšaktɨ mɔːttɨ tultɨmpatɨ. </ta>
            <ta e="T37" id="Seg_400" s="T33">İmaqotɨqɨntɨ kətɨmpatɨ: iːjalʼaiːlɨ mušɨrätɨ. </ta>
            <ta e="T42" id="Seg_401" s="T37">İmaqota qamtəıːmpatɨ meːšaktɨ čʼi šünʼnʼantɨ. </ta>
            <ta e="T47" id="Seg_402" s="T42">Ukkɨr nätalʼa alʼčʼɨmmuntə čʼi šünʼnʼantɨ. </ta>
            <ta e="T50" id="Seg_403" s="T47">İmaqota nʼenʼɨmɔːtpa iraqɨntɨ. </ta>
            <ta e="T54" id="Seg_404" s="T50">Kätɨmpat: Moqɨna qäntätɨ nätalʼa. </ta>
            <ta e="T58" id="Seg_405" s="T54">Loːs-ira qännɨmpatɨ nätalʼa moqonɨ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_406" s="T0">čʼäptä</ta>
            <ta e="T2" id="Seg_407" s="T1">loːs_ira</ta>
            <ta e="T3" id="Seg_408" s="T2">čʼɔːtɨ</ta>
            <ta e="T4" id="Seg_409" s="T3">iːja-lʼa-n</ta>
            <ta e="T5" id="Seg_410" s="T4">sɔːntɨr-pɔː-tɨn</ta>
            <ta e="T6" id="Seg_411" s="T5">poː-qɨn</ta>
            <ta e="T7" id="Seg_412" s="T6">ukkɨr</ta>
            <ta e="T8" id="Seg_413" s="T7">čʼanto-n</ta>
            <ta e="T9" id="Seg_414" s="T8">tü-ŋɨ-ntɨ</ta>
            <ta e="T10" id="Seg_415" s="T9">losɨ_ira</ta>
            <ta e="T11" id="Seg_416" s="T10">i̇ja-lʼa-t-ɨ-m</ta>
            <ta e="T12" id="Seg_417" s="T11">saq-al-pa-tɨ</ta>
            <ta e="T13" id="Seg_418" s="T12">meːšak</ta>
            <ta e="T14" id="Seg_419" s="T13">šunʼä-ntɨ</ta>
            <ta e="T15" id="Seg_420" s="T14">moqɨna</ta>
            <ta e="T16" id="Seg_421" s="T15">qän-nɨ-mpa-tɨ</ta>
            <ta e="T17" id="Seg_422" s="T16">imaqota-qɨn-tɨ</ta>
            <ta e="T18" id="Seg_423" s="T17">kätɨ-mpa-t</ta>
            <ta e="T19" id="Seg_424" s="T18">İja-lʼa-t-ɨ-m</ta>
            <ta e="T20" id="Seg_425" s="T19">muše-r-ät</ta>
            <ta e="T21" id="Seg_426" s="T20">mʼaːšak-tɨ</ta>
            <ta e="T22" id="Seg_427" s="T21">poː-qɨt</ta>
            <ta e="T23" id="Seg_428" s="T22">totɨ-mpa</ta>
            <ta e="T24" id="Seg_429" s="T23">iːja-lʼa-n</ta>
            <ta e="T25" id="Seg_430" s="T24">ınna</ta>
            <ta e="T26" id="Seg_431" s="T25">paktɨ-mpɔː-tɨn</ta>
            <ta e="T27" id="Seg_432" s="T26">nätɨ-lʼa</ta>
            <ta e="T28" id="Seg_433" s="T27">ukkɨr</ta>
            <ta e="T29" id="Seg_434" s="T28">qalɨ-mmɨ-t</ta>
            <ta e="T30" id="Seg_435" s="T29">los_ira</ta>
            <ta e="T31" id="Seg_436" s="T30">meːšak-tɨ</ta>
            <ta e="T32" id="Seg_437" s="T31">mɔːt-tɨ</ta>
            <ta e="T33" id="Seg_438" s="T32">tul-tɨ-mpa-tɨ</ta>
            <ta e="T34" id="Seg_439" s="T33">i̇maqotɨ-qɨn-tɨ</ta>
            <ta e="T35" id="Seg_440" s="T34">kətɨ-mpa-tɨ</ta>
            <ta e="T36" id="Seg_441" s="T35">iːja-lʼa-iː-lɨ</ta>
            <ta e="T37" id="Seg_442" s="T36">mušɨ-r-ätɨ</ta>
            <ta e="T38" id="Seg_443" s="T37">imaqota</ta>
            <ta e="T39" id="Seg_444" s="T38">qamtə-ıː-mpa-tɨ</ta>
            <ta e="T40" id="Seg_445" s="T39">meːšak-tɨ</ta>
            <ta e="T41" id="Seg_446" s="T40">čʼi</ta>
            <ta e="T42" id="Seg_447" s="T41">šünʼnʼa-ntɨ</ta>
            <ta e="T43" id="Seg_448" s="T42">ukkɨr</ta>
            <ta e="T44" id="Seg_449" s="T43">näta-lʼa</ta>
            <ta e="T45" id="Seg_450" s="T44">alʼčʼɨ-mmu-ntə</ta>
            <ta e="T46" id="Seg_451" s="T45">čʼi</ta>
            <ta e="T47" id="Seg_452" s="T46">šünʼnʼa-ntɨ</ta>
            <ta e="T48" id="Seg_453" s="T47">imaqota</ta>
            <ta e="T49" id="Seg_454" s="T48">nʼenʼɨ-mɔːt-pa</ta>
            <ta e="T50" id="Seg_455" s="T49">ira-qɨn-tɨ</ta>
            <ta e="T51" id="Seg_456" s="T50">kätɨ-mpa-t</ta>
            <ta e="T52" id="Seg_457" s="T51">moqɨna</ta>
            <ta e="T53" id="Seg_458" s="T52">qän-t-ätɨ</ta>
            <ta e="T54" id="Seg_459" s="T53">näta-lʼa</ta>
            <ta e="T55" id="Seg_460" s="T54">loːs_ira</ta>
            <ta e="T56" id="Seg_461" s="T55">qän-nɨ-mpa-tɨ</ta>
            <ta e="T57" id="Seg_462" s="T56">näta-lʼa</ta>
            <ta e="T58" id="Seg_463" s="T57">moqonɨ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_464" s="T0">čʼaptä</ta>
            <ta e="T2" id="Seg_465" s="T1">loːs_ira</ta>
            <ta e="T3" id="Seg_466" s="T2">čʼɔːtɨ</ta>
            <ta e="T4" id="Seg_467" s="T3">iːja-lʼa-t</ta>
            <ta e="T5" id="Seg_468" s="T4">sɔːntɨr-mpɨ-tɨt</ta>
            <ta e="T6" id="Seg_469" s="T5">poː-qɨn</ta>
            <ta e="T7" id="Seg_470" s="T6">ukkɨr</ta>
            <ta e="T8" id="Seg_471" s="T7">čʼontɨ-n</ta>
            <ta e="T9" id="Seg_472" s="T8">tü-ŋɨ-ntɨ</ta>
            <ta e="T10" id="Seg_473" s="T9">loːs_ira</ta>
            <ta e="T11" id="Seg_474" s="T10">iːja-lʼa-t-ɨ-m</ta>
            <ta e="T12" id="Seg_475" s="T11">saqɨ-ätɔːl-mpɨ-tɨ</ta>
            <ta e="T13" id="Seg_476" s="T12">meːšak</ta>
            <ta e="T14" id="Seg_477" s="T13">šünʼčʼɨ-ntɨ</ta>
            <ta e="T15" id="Seg_478" s="T14">moqɨnä</ta>
            <ta e="T16" id="Seg_479" s="T15">qən-tɨ-mpɨ-tɨ</ta>
            <ta e="T17" id="Seg_480" s="T16">imaqota-qɨn-ntɨ</ta>
            <ta e="T18" id="Seg_481" s="T17">kətɨ-mpɨ-tɨ</ta>
            <ta e="T19" id="Seg_482" s="T18">iːja-lʼa-t-ɨ-m</ta>
            <ta e="T20" id="Seg_483" s="T19">mušɨ-rɨ-ätɨ</ta>
            <ta e="T21" id="Seg_484" s="T20">meːšak-tɨ</ta>
            <ta e="T22" id="Seg_485" s="T21">poː-qɨn</ta>
            <ta e="T23" id="Seg_486" s="T22">tottɨ-mpɨ</ta>
            <ta e="T24" id="Seg_487" s="T23">iːja-lʼa-t</ta>
            <ta e="T25" id="Seg_488" s="T24">ınnä</ta>
            <ta e="T26" id="Seg_489" s="T25">paktɨ-mpɨ-tɨt</ta>
            <ta e="T27" id="Seg_490" s="T26">nätäk-lʼa</ta>
            <ta e="T28" id="Seg_491" s="T27">ukkɨr</ta>
            <ta e="T29" id="Seg_492" s="T28">qalɨ-mpɨ-ntɨ</ta>
            <ta e="T30" id="Seg_493" s="T29">loːs_ira</ta>
            <ta e="T31" id="Seg_494" s="T30">meːšak-tɨ</ta>
            <ta e="T32" id="Seg_495" s="T31">mɔːt-ntɨ</ta>
            <ta e="T33" id="Seg_496" s="T32">tul-tɨ-mpɨ-tɨ</ta>
            <ta e="T34" id="Seg_497" s="T33">imaqota-qɨn-ntɨ</ta>
            <ta e="T35" id="Seg_498" s="T34">kətɨ-mpɨ-tɨ</ta>
            <ta e="T36" id="Seg_499" s="T35">iːja-lʼa-iː-lɨ</ta>
            <ta e="T37" id="Seg_500" s="T36">mušɨ-rɨ-ätɨ</ta>
            <ta e="T38" id="Seg_501" s="T37">imaqota</ta>
            <ta e="T39" id="Seg_502" s="T38">qamtɨ-ıː-mpɨ-tɨ</ta>
            <ta e="T40" id="Seg_503" s="T39">meːšak-tɨ</ta>
            <ta e="T41" id="Seg_504" s="T40">čʼi</ta>
            <ta e="T42" id="Seg_505" s="T41">šünʼčʼɨ-ntɨ</ta>
            <ta e="T43" id="Seg_506" s="T42">ukkɨr</ta>
            <ta e="T44" id="Seg_507" s="T43">nätäk-lʼa</ta>
            <ta e="T45" id="Seg_508" s="T44">alʼčʼɨ-mpɨ-ntɨ</ta>
            <ta e="T46" id="Seg_509" s="T45">čʼi</ta>
            <ta e="T47" id="Seg_510" s="T46">šünʼčʼɨ-ntɨ</ta>
            <ta e="T48" id="Seg_511" s="T47">imaqota</ta>
            <ta e="T49" id="Seg_512" s="T48">nʼenʼnʼɨ-mɔːt-mpɨ</ta>
            <ta e="T50" id="Seg_513" s="T49">ira-qɨn-ntɨ</ta>
            <ta e="T51" id="Seg_514" s="T50">kətɨ-mpɨ-tɨ</ta>
            <ta e="T52" id="Seg_515" s="T51">moqɨnä</ta>
            <ta e="T53" id="Seg_516" s="T52">qən-tɨ-ätɨ</ta>
            <ta e="T54" id="Seg_517" s="T53">nätäk-lʼa</ta>
            <ta e="T55" id="Seg_518" s="T54">loːs_ira</ta>
            <ta e="T56" id="Seg_519" s="T55">qən-tɨ-mpɨ-tɨ</ta>
            <ta e="T57" id="Seg_520" s="T56">nätäk-lʼa</ta>
            <ta e="T58" id="Seg_521" s="T57">moqɨnä</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_522" s="T0">tale.[NOM]</ta>
            <ta e="T2" id="Seg_523" s="T1">bear.[NOM]</ta>
            <ta e="T3" id="Seg_524" s="T2">about</ta>
            <ta e="T4" id="Seg_525" s="T3">child-DIM-PL</ta>
            <ta e="T5" id="Seg_526" s="T4">play-PST.NAR-3PL</ta>
            <ta e="T6" id="Seg_527" s="T5">space.outside-LOC</ta>
            <ta e="T7" id="Seg_528" s="T6">one</ta>
            <ta e="T8" id="Seg_529" s="T7">middle-ADV.LOC</ta>
            <ta e="T9" id="Seg_530" s="T8">come-%%-INFER.[3SG.S]</ta>
            <ta e="T10" id="Seg_531" s="T9">bear.[NOM]</ta>
            <ta e="T11" id="Seg_532" s="T10">child-DIM-PL-EP-ACC</ta>
            <ta e="T12" id="Seg_533" s="T11">put.into-MOM-PST.NAR-3SG.O</ta>
            <ta e="T13" id="Seg_534" s="T12">sack.[NOM]</ta>
            <ta e="T14" id="Seg_535" s="T13">inside-ILL</ta>
            <ta e="T15" id="Seg_536" s="T14">home</ta>
            <ta e="T16" id="Seg_537" s="T15">leave-TR-PST.NAR-3SG.O</ta>
            <ta e="T17" id="Seg_538" s="T16">wife-ILL-OBL.3SG</ta>
            <ta e="T18" id="Seg_539" s="T17">say-PST.NAR-3SG.O</ta>
            <ta e="T19" id="Seg_540" s="T18">child-DIM-PL-EP-ACC</ta>
            <ta e="T20" id="Seg_541" s="T19">be.cooking-TR-IMP.2SG.O</ta>
            <ta e="T21" id="Seg_542" s="T20">sack.[NOM]-3SG</ta>
            <ta e="T22" id="Seg_543" s="T21">space.outside-LOC</ta>
            <ta e="T23" id="Seg_544" s="T22">stand-PST.NAR.[3SG.S]</ta>
            <ta e="T24" id="Seg_545" s="T23">child-DIM-PL</ta>
            <ta e="T25" id="Seg_546" s="T24">up</ta>
            <ta e="T26" id="Seg_547" s="T25">run-PST.NAR-3PL</ta>
            <ta e="T27" id="Seg_548" s="T26">girl-DIM.[NOM]</ta>
            <ta e="T28" id="Seg_549" s="T27">one</ta>
            <ta e="T29" id="Seg_550" s="T28">stay-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T30" id="Seg_551" s="T29">bear.[NOM]</ta>
            <ta e="T31" id="Seg_552" s="T30">sack.[NOM]-3SG</ta>
            <ta e="T32" id="Seg_553" s="T31">house-ILL</ta>
            <ta e="T33" id="Seg_554" s="T32">bring-TR-PST.NAR-3SG.O</ta>
            <ta e="T34" id="Seg_555" s="T33">wife-ILL-OBL.3SG</ta>
            <ta e="T35" id="Seg_556" s="T34">say-PST.NAR-3SG.O</ta>
            <ta e="T36" id="Seg_557" s="T35">child-DIM-PL.[NOM]-2SG</ta>
            <ta e="T37" id="Seg_558" s="T36">be.cooking-TR-IMP.2SG.O</ta>
            <ta e="T38" id="Seg_559" s="T37">wife.[NOM]</ta>
            <ta e="T39" id="Seg_560" s="T38">pour.out-RFL.PFV-PST.NAR-3SG.O</ta>
            <ta e="T40" id="Seg_561" s="T39">sack.[NOM]-3SG</ta>
            <ta e="T41" id="Seg_562" s="T40">cauldron.[NOM]</ta>
            <ta e="T42" id="Seg_563" s="T41">inside-ILL</ta>
            <ta e="T43" id="Seg_564" s="T42">one</ta>
            <ta e="T44" id="Seg_565" s="T43">girl-DIM.[NOM]</ta>
            <ta e="T45" id="Seg_566" s="T44">get.into-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T46" id="Seg_567" s="T45">cauldron.[NOM]</ta>
            <ta e="T47" id="Seg_568" s="T46">inside-ILL</ta>
            <ta e="T48" id="Seg_569" s="T47">wife.[NOM]</ta>
            <ta e="T49" id="Seg_570" s="T48">get.angry-DRV-PST.NAR.[3SG.S]</ta>
            <ta e="T50" id="Seg_571" s="T49">husband-ILL-OBL.3SG</ta>
            <ta e="T51" id="Seg_572" s="T50">say-PST.NAR-3SG.O</ta>
            <ta e="T52" id="Seg_573" s="T51">home</ta>
            <ta e="T53" id="Seg_574" s="T52">go.away-TR-IMP.2SG.O</ta>
            <ta e="T54" id="Seg_575" s="T53">girl-DIM.[NOM]</ta>
            <ta e="T55" id="Seg_576" s="T54">bear.[NOM]</ta>
            <ta e="T56" id="Seg_577" s="T55">leave-TR-PST.NAR-3SG.O</ta>
            <ta e="T57" id="Seg_578" s="T56">girl-DIM.[NOM]</ta>
            <ta e="T58" id="Seg_579" s="T57">home</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_580" s="T0">сказка.[NOM]</ta>
            <ta e="T2" id="Seg_581" s="T1">медведь.[NOM]</ta>
            <ta e="T3" id="Seg_582" s="T2">о</ta>
            <ta e="T4" id="Seg_583" s="T3">ребенок-DIM-PL</ta>
            <ta e="T5" id="Seg_584" s="T4">играть-PST.NAR-3PL</ta>
            <ta e="T6" id="Seg_585" s="T5">пространство.снаружи-LOC</ta>
            <ta e="T7" id="Seg_586" s="T6">один</ta>
            <ta e="T8" id="Seg_587" s="T7">середина-ADV.LOC</ta>
            <ta e="T9" id="Seg_588" s="T8">прийти-%%-INFER.[3SG.S]</ta>
            <ta e="T10" id="Seg_589" s="T9">медведь.[NOM]</ta>
            <ta e="T11" id="Seg_590" s="T10">ребенок-DIM-PL-EP-ACC</ta>
            <ta e="T12" id="Seg_591" s="T11">засунуть-MOM-PST.NAR-3SG.O</ta>
            <ta e="T13" id="Seg_592" s="T12">мешок.[NOM]</ta>
            <ta e="T14" id="Seg_593" s="T13">внутри-ILL</ta>
            <ta e="T15" id="Seg_594" s="T14">домой</ta>
            <ta e="T16" id="Seg_595" s="T15">отправиться-TR-PST.NAR-3SG.O</ta>
            <ta e="T17" id="Seg_596" s="T16">жена-ILL-OBL.3SG</ta>
            <ta e="T18" id="Seg_597" s="T17">сказать-PST.NAR-3SG.O</ta>
            <ta e="T19" id="Seg_598" s="T18">ребенок-DIM-PL-EP-ACC</ta>
            <ta e="T20" id="Seg_599" s="T19">свариться-TR-IMP.2SG.O</ta>
            <ta e="T21" id="Seg_600" s="T20">мешок.[NOM]-3SG</ta>
            <ta e="T22" id="Seg_601" s="T21">пространство.снаружи-LOC</ta>
            <ta e="T23" id="Seg_602" s="T22">стоять-PST.NAR.[3SG.S]</ta>
            <ta e="T24" id="Seg_603" s="T23">ребенок-DIM-PL</ta>
            <ta e="T25" id="Seg_604" s="T24">вверх</ta>
            <ta e="T26" id="Seg_605" s="T25">побежать-PST.NAR-3PL</ta>
            <ta e="T27" id="Seg_606" s="T26">девушка-DIM.[NOM]</ta>
            <ta e="T28" id="Seg_607" s="T27">один</ta>
            <ta e="T29" id="Seg_608" s="T28">остаться-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T30" id="Seg_609" s="T29">медведь.[NOM]</ta>
            <ta e="T31" id="Seg_610" s="T30">мешок.[NOM]-3SG</ta>
            <ta e="T32" id="Seg_611" s="T31">дом-ILL</ta>
            <ta e="T33" id="Seg_612" s="T32">занести-TR-PST.NAR-3SG.O</ta>
            <ta e="T34" id="Seg_613" s="T33">жена-ILL-OBL.3SG</ta>
            <ta e="T35" id="Seg_614" s="T34">сказать-PST.NAR-3SG.O</ta>
            <ta e="T36" id="Seg_615" s="T35">ребенок-DIM-PL.[NOM]-2SG</ta>
            <ta e="T37" id="Seg_616" s="T36">свариться-TR-IMP.2SG.O</ta>
            <ta e="T38" id="Seg_617" s="T37">жена.[NOM]</ta>
            <ta e="T39" id="Seg_618" s="T38">высыпать-RFL.PFV-PST.NAR-3SG.O</ta>
            <ta e="T40" id="Seg_619" s="T39">мешок.[NOM]-3SG</ta>
            <ta e="T41" id="Seg_620" s="T40">котёл.[NOM]</ta>
            <ta e="T42" id="Seg_621" s="T41">внутри-ILL</ta>
            <ta e="T43" id="Seg_622" s="T42">один</ta>
            <ta e="T44" id="Seg_623" s="T43">девушка-DIM.[NOM]</ta>
            <ta e="T45" id="Seg_624" s="T44">попасть-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T46" id="Seg_625" s="T45">котёл.[NOM]</ta>
            <ta e="T47" id="Seg_626" s="T46">внутри-ILL</ta>
            <ta e="T48" id="Seg_627" s="T47">жена.[NOM]</ta>
            <ta e="T49" id="Seg_628" s="T48">сердиться-DRV-PST.NAR.[3SG.S]</ta>
            <ta e="T50" id="Seg_629" s="T49">муж-ILL-OBL.3SG</ta>
            <ta e="T51" id="Seg_630" s="T50">сказать-PST.NAR-3SG.O</ta>
            <ta e="T52" id="Seg_631" s="T51">домой</ta>
            <ta e="T53" id="Seg_632" s="T52">уйти-TR-IMP.2SG.O</ta>
            <ta e="T54" id="Seg_633" s="T53">девушка-DIM.[NOM]</ta>
            <ta e="T55" id="Seg_634" s="T54">медведь.[NOM]</ta>
            <ta e="T56" id="Seg_635" s="T55">отправиться-TR-PST.NAR-3SG.O</ta>
            <ta e="T57" id="Seg_636" s="T56">девушка-DIM.[NOM]</ta>
            <ta e="T58" id="Seg_637" s="T57">домой</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_638" s="T0">n-n:case</ta>
            <ta e="T2" id="Seg_639" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_640" s="T2">pp</ta>
            <ta e="T4" id="Seg_641" s="T3">n-n&gt;n-n:num</ta>
            <ta e="T5" id="Seg_642" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_643" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_644" s="T6">num</ta>
            <ta e="T8" id="Seg_645" s="T7">n-n&gt;adv</ta>
            <ta e="T9" id="Seg_646" s="T8">v-%%-v:tense.mood-v:pn</ta>
            <ta e="T10" id="Seg_647" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_648" s="T10">n-n&gt;n-n:num-n:ins-n:case</ta>
            <ta e="T12" id="Seg_649" s="T11">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_650" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_651" s="T13">pp-n:case</ta>
            <ta e="T15" id="Seg_652" s="T14">adv</ta>
            <ta e="T16" id="Seg_653" s="T15">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_654" s="T16">n-n:case-n:obl.poss</ta>
            <ta e="T18" id="Seg_655" s="T17">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_656" s="T18">n-n&gt;n-n:num-n:ins-n:case</ta>
            <ta e="T20" id="Seg_657" s="T19">v-v&gt;v-v:mood.pn</ta>
            <ta e="T21" id="Seg_658" s="T20">n-n:case-n:poss</ta>
            <ta e="T22" id="Seg_659" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_660" s="T22">v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_661" s="T23">n-n&gt;n-n:num</ta>
            <ta e="T25" id="Seg_662" s="T24">preverb</ta>
            <ta e="T26" id="Seg_663" s="T25">v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_664" s="T26">n-n&gt;n-n:case</ta>
            <ta e="T28" id="Seg_665" s="T27">num</ta>
            <ta e="T29" id="Seg_666" s="T28">v-v:tense-v:mood-v:pn</ta>
            <ta e="T30" id="Seg_667" s="T29">n-n:case</ta>
            <ta e="T31" id="Seg_668" s="T30">n-n:case-n:poss</ta>
            <ta e="T32" id="Seg_669" s="T31">n-n:case</ta>
            <ta e="T33" id="Seg_670" s="T32">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T34" id="Seg_671" s="T33">n-n:case-n:obl.poss</ta>
            <ta e="T35" id="Seg_672" s="T34">v-v:tense-v:pn</ta>
            <ta e="T36" id="Seg_673" s="T35">n-n&gt;n-n:num-n:case-n:poss</ta>
            <ta e="T37" id="Seg_674" s="T36">v-v&gt;v-v:mood.pn</ta>
            <ta e="T38" id="Seg_675" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_676" s="T38">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T40" id="Seg_677" s="T39">n-n:case-n:poss</ta>
            <ta e="T41" id="Seg_678" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_679" s="T41">pp-n:case</ta>
            <ta e="T43" id="Seg_680" s="T42">num</ta>
            <ta e="T44" id="Seg_681" s="T43">n-n&gt;n-n:case</ta>
            <ta e="T45" id="Seg_682" s="T44">v-v:tense-v:mood-v:pn</ta>
            <ta e="T46" id="Seg_683" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_684" s="T46">pp-n:case</ta>
            <ta e="T48" id="Seg_685" s="T47">n-n:case</ta>
            <ta e="T49" id="Seg_686" s="T48">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T50" id="Seg_687" s="T49">n-n:case-n:obl.poss</ta>
            <ta e="T51" id="Seg_688" s="T50">v-v:tense-v:pn</ta>
            <ta e="T52" id="Seg_689" s="T51">adv</ta>
            <ta e="T53" id="Seg_690" s="T52">v-v&gt;v-v:mood.pn</ta>
            <ta e="T54" id="Seg_691" s="T53">n-n&gt;n-n:case</ta>
            <ta e="T55" id="Seg_692" s="T54">n-n:case</ta>
            <ta e="T56" id="Seg_693" s="T55">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T57" id="Seg_694" s="T56">n-n&gt;n-n:case</ta>
            <ta e="T58" id="Seg_695" s="T57">adv</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_696" s="T0">n</ta>
            <ta e="T2" id="Seg_697" s="T1">n</ta>
            <ta e="T3" id="Seg_698" s="T2">pp</ta>
            <ta e="T4" id="Seg_699" s="T3">n</ta>
            <ta e="T5" id="Seg_700" s="T4">v</ta>
            <ta e="T6" id="Seg_701" s="T5">n</ta>
            <ta e="T7" id="Seg_702" s="T6">num</ta>
            <ta e="T8" id="Seg_703" s="T7">adv</ta>
            <ta e="T9" id="Seg_704" s="T8">v</ta>
            <ta e="T10" id="Seg_705" s="T9">n</ta>
            <ta e="T11" id="Seg_706" s="T10">n</ta>
            <ta e="T12" id="Seg_707" s="T11">v</ta>
            <ta e="T13" id="Seg_708" s="T12">n</ta>
            <ta e="T14" id="Seg_709" s="T13">n</ta>
            <ta e="T15" id="Seg_710" s="T14">adv</ta>
            <ta e="T16" id="Seg_711" s="T15">v</ta>
            <ta e="T17" id="Seg_712" s="T16">n</ta>
            <ta e="T18" id="Seg_713" s="T17">v</ta>
            <ta e="T19" id="Seg_714" s="T18">n</ta>
            <ta e="T20" id="Seg_715" s="T19">v</ta>
            <ta e="T21" id="Seg_716" s="T20">n</ta>
            <ta e="T22" id="Seg_717" s="T21">n</ta>
            <ta e="T23" id="Seg_718" s="T22">v</ta>
            <ta e="T24" id="Seg_719" s="T23">n</ta>
            <ta e="T25" id="Seg_720" s="T24">preverb</ta>
            <ta e="T26" id="Seg_721" s="T25">v</ta>
            <ta e="T27" id="Seg_722" s="T26">n</ta>
            <ta e="T28" id="Seg_723" s="T27">num</ta>
            <ta e="T29" id="Seg_724" s="T28">v</ta>
            <ta e="T30" id="Seg_725" s="T29">n</ta>
            <ta e="T31" id="Seg_726" s="T30">n</ta>
            <ta e="T32" id="Seg_727" s="T31">n</ta>
            <ta e="T33" id="Seg_728" s="T32">v</ta>
            <ta e="T34" id="Seg_729" s="T33">n</ta>
            <ta e="T35" id="Seg_730" s="T34">v</ta>
            <ta e="T36" id="Seg_731" s="T35">n</ta>
            <ta e="T37" id="Seg_732" s="T36">v</ta>
            <ta e="T38" id="Seg_733" s="T37">n</ta>
            <ta e="T39" id="Seg_734" s="T38">v</ta>
            <ta e="T40" id="Seg_735" s="T39">n</ta>
            <ta e="T41" id="Seg_736" s="T40">n</ta>
            <ta e="T42" id="Seg_737" s="T41">pp</ta>
            <ta e="T43" id="Seg_738" s="T42">num</ta>
            <ta e="T44" id="Seg_739" s="T43">n</ta>
            <ta e="T45" id="Seg_740" s="T44">v</ta>
            <ta e="T46" id="Seg_741" s="T45">n</ta>
            <ta e="T47" id="Seg_742" s="T46">pp</ta>
            <ta e="T48" id="Seg_743" s="T47">n</ta>
            <ta e="T49" id="Seg_744" s="T48">v</ta>
            <ta e="T50" id="Seg_745" s="T49">n</ta>
            <ta e="T51" id="Seg_746" s="T50">v</ta>
            <ta e="T52" id="Seg_747" s="T51">adv</ta>
            <ta e="T53" id="Seg_748" s="T52">v</ta>
            <ta e="T54" id="Seg_749" s="T53">n</ta>
            <ta e="T55" id="Seg_750" s="T54">n</ta>
            <ta e="T56" id="Seg_751" s="T55">v</ta>
            <ta e="T57" id="Seg_752" s="T56">n</ta>
            <ta e="T58" id="Seg_753" s="T57">adv</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T4" id="Seg_754" s="T3">np.h:A</ta>
            <ta e="T6" id="Seg_755" s="T5">np:L</ta>
            <ta e="T8" id="Seg_756" s="T7">adv:Time</ta>
            <ta e="T10" id="Seg_757" s="T9">np.h:A</ta>
            <ta e="T11" id="Seg_758" s="T10">np.h:Th</ta>
            <ta e="T12" id="Seg_759" s="T11">0.3.h:A</ta>
            <ta e="T14" id="Seg_760" s="T13">np:G</ta>
            <ta e="T15" id="Seg_761" s="T14">adv:G</ta>
            <ta e="T16" id="Seg_762" s="T15">0.3.h:A</ta>
            <ta e="T17" id="Seg_763" s="T16">np.h:R 0.3.h:Poss</ta>
            <ta e="T18" id="Seg_764" s="T17">0.3.h:A</ta>
            <ta e="T19" id="Seg_765" s="T18">np.h:P</ta>
            <ta e="T20" id="Seg_766" s="T19">0.2.h:A</ta>
            <ta e="T21" id="Seg_767" s="T20">np:Th</ta>
            <ta e="T22" id="Seg_768" s="T21">np:L</ta>
            <ta e="T24" id="Seg_769" s="T23">np.h:A</ta>
            <ta e="T27" id="Seg_770" s="T26">np.h:Th</ta>
            <ta e="T30" id="Seg_771" s="T29">np.h:A</ta>
            <ta e="T31" id="Seg_772" s="T30">np:Th</ta>
            <ta e="T32" id="Seg_773" s="T31">np:G</ta>
            <ta e="T34" id="Seg_774" s="T33">np.h:R 0.3.h:Poss</ta>
            <ta e="T35" id="Seg_775" s="T34">0.3.h:A</ta>
            <ta e="T36" id="Seg_776" s="T35">np.h:P</ta>
            <ta e="T37" id="Seg_777" s="T36">0.2.h:A</ta>
            <ta e="T38" id="Seg_778" s="T37">np.h:A</ta>
            <ta e="T40" id="Seg_779" s="T39">np:Th</ta>
            <ta e="T41" id="Seg_780" s="T40">pp:G</ta>
            <ta e="T44" id="Seg_781" s="T43">np.h:P</ta>
            <ta e="T46" id="Seg_782" s="T45">pp:G</ta>
            <ta e="T48" id="Seg_783" s="T47">np.h:E</ta>
            <ta e="T50" id="Seg_784" s="T49">np.h:Th</ta>
            <ta e="T51" id="Seg_785" s="T50">0.3.h:A</ta>
            <ta e="T52" id="Seg_786" s="T51">adv:G</ta>
            <ta e="T53" id="Seg_787" s="T52">0.2.h:A</ta>
            <ta e="T54" id="Seg_788" s="T53">np.h:Th</ta>
            <ta e="T55" id="Seg_789" s="T54">np.h:A</ta>
            <ta e="T57" id="Seg_790" s="T56">np.h:Th</ta>
            <ta e="T58" id="Seg_791" s="T57">adv:G</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_792" s="T3">np.h:S</ta>
            <ta e="T5" id="Seg_793" s="T4">v:pred</ta>
            <ta e="T9" id="Seg_794" s="T8">v:pred</ta>
            <ta e="T10" id="Seg_795" s="T9">np.h:S</ta>
            <ta e="T11" id="Seg_796" s="T10">np.h:O</ta>
            <ta e="T12" id="Seg_797" s="T11">0.3.h:S v:pred</ta>
            <ta e="T16" id="Seg_798" s="T15">0.3.h:S v:pred</ta>
            <ta e="T18" id="Seg_799" s="T17">0.3.h:S v:pred</ta>
            <ta e="T19" id="Seg_800" s="T18">np.h:O</ta>
            <ta e="T20" id="Seg_801" s="T19">0.2.h:S v:pred</ta>
            <ta e="T21" id="Seg_802" s="T20">np:S</ta>
            <ta e="T23" id="Seg_803" s="T22">v:pred</ta>
            <ta e="T24" id="Seg_804" s="T23">np.h:S</ta>
            <ta e="T26" id="Seg_805" s="T25">v:pred</ta>
            <ta e="T27" id="Seg_806" s="T26">np.h:S</ta>
            <ta e="T29" id="Seg_807" s="T28">v:pred</ta>
            <ta e="T30" id="Seg_808" s="T29">np.h:S</ta>
            <ta e="T31" id="Seg_809" s="T30">np:O</ta>
            <ta e="T33" id="Seg_810" s="T32">v:pred</ta>
            <ta e="T35" id="Seg_811" s="T34">0.3.h:S v:pred</ta>
            <ta e="T36" id="Seg_812" s="T35">np.h:S</ta>
            <ta e="T37" id="Seg_813" s="T36">0.2.h:S v:pred</ta>
            <ta e="T38" id="Seg_814" s="T37">np.h:S</ta>
            <ta e="T39" id="Seg_815" s="T38">v:pred</ta>
            <ta e="T40" id="Seg_816" s="T39">np:O</ta>
            <ta e="T44" id="Seg_817" s="T43">np.h:S</ta>
            <ta e="T45" id="Seg_818" s="T44">v:pred</ta>
            <ta e="T48" id="Seg_819" s="T47">np.h:S</ta>
            <ta e="T49" id="Seg_820" s="T48">v:pred</ta>
            <ta e="T51" id="Seg_821" s="T50">0.3.h:S v:pred</ta>
            <ta e="T53" id="Seg_822" s="T52">0.2.h:S v:pred</ta>
            <ta e="T54" id="Seg_823" s="T53">np.h:O</ta>
            <ta e="T55" id="Seg_824" s="T54">np.h:S</ta>
            <ta e="T56" id="Seg_825" s="T55">v:pred</ta>
            <ta e="T57" id="Seg_826" s="T56">np.h:O</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T1" id="Seg_827" s="T0">accs-gen</ta>
            <ta e="T2" id="Seg_828" s="T1">new</ta>
            <ta e="T4" id="Seg_829" s="T3">new</ta>
            <ta e="T10" id="Seg_830" s="T9">new</ta>
            <ta e="T11" id="Seg_831" s="T10">giv-inactive</ta>
            <ta e="T12" id="Seg_832" s="T11">0.giv-active</ta>
            <ta e="T13" id="Seg_833" s="T12">new</ta>
            <ta e="T16" id="Seg_834" s="T15">0.giv-active</ta>
            <ta e="T17" id="Seg_835" s="T16">new</ta>
            <ta e="T18" id="Seg_836" s="T17">quot-sp</ta>
            <ta e="T19" id="Seg_837" s="T18">giv-inactive-Q</ta>
            <ta e="T20" id="Seg_838" s="T19">0.giv-active-Q</ta>
            <ta e="T21" id="Seg_839" s="T20">giv-inactive</ta>
            <ta e="T24" id="Seg_840" s="T23">giv-inactive</ta>
            <ta e="T27" id="Seg_841" s="T26">accs-sit</ta>
            <ta e="T30" id="Seg_842" s="T29">giv-inactive</ta>
            <ta e="T31" id="Seg_843" s="T30">giv-inactive</ta>
            <ta e="T32" id="Seg_844" s="T31">giv-inactive</ta>
            <ta e="T34" id="Seg_845" s="T33">giv-inactive</ta>
            <ta e="T35" id="Seg_846" s="T34">0.giv-active/0.quot-sp</ta>
            <ta e="T36" id="Seg_847" s="T35">giv-inactive-Q</ta>
            <ta e="T37" id="Seg_848" s="T36">0.giv-active-Q</ta>
            <ta e="T38" id="Seg_849" s="T37">giv-active</ta>
            <ta e="T40" id="Seg_850" s="T39">giv-inactive</ta>
            <ta e="T41" id="Seg_851" s="T40">accs-sit</ta>
            <ta e="T44" id="Seg_852" s="T43">giv-inactive</ta>
            <ta e="T46" id="Seg_853" s="T45">giv-inactive</ta>
            <ta e="T48" id="Seg_854" s="T47">giv-inactive</ta>
            <ta e="T50" id="Seg_855" s="T49">giv-inactive</ta>
            <ta e="T51" id="Seg_856" s="T50">0.giv-active/0.quot-sp</ta>
            <ta e="T53" id="Seg_857" s="T52">0.giv-active-Q</ta>
            <ta e="T54" id="Seg_858" s="T53">giv-inactive-Q</ta>
            <ta e="T55" id="Seg_859" s="T54">giv-active</ta>
            <ta e="T57" id="Seg_860" s="T56">giv-active</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_861" s="T0">Сказка про медведя.</ta>
            <ta e="T6" id="Seg_862" s="T3">Мальчики играли на улице.</ta>
            <ta e="T10" id="Seg_863" s="T6">Однажды пришел медведь.</ta>
            <ta e="T14" id="Seg_864" s="T10">Мальчиков положил в мешок.</ta>
            <ta e="T18" id="Seg_865" s="T14">Домой унёс, жене сказал.</ta>
            <ta e="T20" id="Seg_866" s="T18">"Свари мальчиков".</ta>
            <ta e="T23" id="Seg_867" s="T20">Мешок на улице стоял.</ta>
            <ta e="T26" id="Seg_868" s="T23">Мальчики убежали.</ta>
            <ta e="T29" id="Seg_869" s="T26">Одна девочка осталась.</ta>
            <ta e="T33" id="Seg_870" s="T29">Медведь мешок занес в дом.</ta>
            <ta e="T37" id="Seg_871" s="T33">Жене сказал: "Мальчиков свари".</ta>
            <ta e="T42" id="Seg_872" s="T37">Жена вывалила мешок в котел.</ta>
            <ta e="T47" id="Seg_873" s="T42">[Только] одна девочка упала в котел.</ta>
            <ta e="T50" id="Seg_874" s="T47">Жена рассердилась на мужа.</ta>
            <ta e="T54" id="Seg_875" s="T50">Сказала: "Неси домой девочку".</ta>
            <ta e="T58" id="Seg_876" s="T54">Медведь унес девочку домой.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_877" s="T0">A tale about a bear.</ta>
            <ta e="T6" id="Seg_878" s="T3">Boys were playing outside.</ta>
            <ta e="T10" id="Seg_879" s="T6">Once a bear came.</ta>
            <ta e="T14" id="Seg_880" s="T10">He put the boys into a sack.</ta>
            <ta e="T18" id="Seg_881" s="T14">He brought it home and said to his wife.</ta>
            <ta e="T20" id="Seg_882" s="T18">"Cook the boys!"</ta>
            <ta e="T23" id="Seg_883" s="T20">The sack stood outdoors.</ta>
            <ta e="T26" id="Seg_884" s="T23">The boys ran away.</ta>
            <ta e="T29" id="Seg_885" s="T26">One girl stayed.</ta>
            <ta e="T33" id="Seg_886" s="T29">The bear brought the sack into the house.</ta>
            <ta e="T37" id="Seg_887" s="T33">He said to his wife: "Cook the boys".</ta>
            <ta e="T42" id="Seg_888" s="T37">The wife shook out the sack into the cauldron.</ta>
            <ta e="T47" id="Seg_889" s="T42">Only one girl fell into the cauldron.</ta>
            <ta e="T50" id="Seg_890" s="T47">The wife got angry with her husband.</ta>
            <ta e="T54" id="Seg_891" s="T50">She said: "Bring the girl home".</ta>
            <ta e="T58" id="Seg_892" s="T54">The bear brought the girl home.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_893" s="T0">Ein Märchen über einen Bär.</ta>
            <ta e="T6" id="Seg_894" s="T3">Draußen spielten Jungen.</ta>
            <ta e="T10" id="Seg_895" s="T6">Einmal kam ein Bär.</ta>
            <ta e="T14" id="Seg_896" s="T10">Er steckte die Jungen in einen Sack.</ta>
            <ta e="T18" id="Seg_897" s="T14">Er brachte ihn nach Hause und sagte zu seiner Frau:</ta>
            <ta e="T20" id="Seg_898" s="T18">"Koch die Jungen!"</ta>
            <ta e="T23" id="Seg_899" s="T20">Der Sack stand draußen.</ta>
            <ta e="T26" id="Seg_900" s="T23">Die Jungen sind weggelaufen.</ta>
            <ta e="T29" id="Seg_901" s="T26">Ein Mädchen ist geblieben.</ta>
            <ta e="T33" id="Seg_902" s="T29">Der Bär brachte den Sack ins Haus.</ta>
            <ta e="T37" id="Seg_903" s="T33">Er sagte zu seiner Frau: "Koch die Jungen."</ta>
            <ta e="T42" id="Seg_904" s="T37">Die Frau leerte den Sack in den Kessel aus.</ta>
            <ta e="T47" id="Seg_905" s="T42">Nur ein Mädchen fiel in den Kessel.</ta>
            <ta e="T50" id="Seg_906" s="T47">Die Frau wurde böse auf ihren Mann.</ta>
            <ta e="T54" id="Seg_907" s="T50">Sie sagte: "Bring das Mädchen nach Hause."</ta>
            <ta e="T58" id="Seg_908" s="T54">Der Bär brachte das Mädchen nach Hause.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_909" s="T0">сказка про медведя.</ta>
            <ta e="T6" id="Seg_910" s="T3">мальчики играли на улице.</ta>
            <ta e="T10" id="Seg_911" s="T6">однажды [вдруг] пришел медведь.</ta>
            <ta e="T14" id="Seg_912" s="T10">мальчиков положил в мешок во внутрь.</ta>
            <ta e="T18" id="Seg_913" s="T14">домой понес жене (унес) понес.</ta>
            <ta e="T20" id="Seg_914" s="T18">мальчиков вари.</ta>
            <ta e="T23" id="Seg_915" s="T20">мешок на улице стоял</ta>
            <ta e="T26" id="Seg_916" s="T23">мальчики убежали.</ta>
            <ta e="T29" id="Seg_917" s="T26">одна девочка осталась.</ta>
            <ta e="T33" id="Seg_918" s="T29">медведь мешок занес в дом.</ta>
            <ta e="T37" id="Seg_919" s="T33">жене сказал мальчиков свари.</ta>
            <ta e="T42" id="Seg_920" s="T37">жена вылила [вывалила] мешок в котел.</ta>
            <ta e="T47" id="Seg_921" s="T42">одна девочка упала в котел.</ta>
            <ta e="T50" id="Seg_922" s="T47">жена рассердилась на мужа.</ta>
            <ta e="T54" id="Seg_923" s="T50">сказала неси домой девочку.</ta>
            <ta e="T58" id="Seg_924" s="T54">медведь унес девочку домой.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T10" id="Seg_925" s="T6">[OSV:] unclear verbal form "tüŋɨntɨ".</ta>
            <ta e="T14" id="Seg_926" s="T10">‎‎[OSV:] The form "ijalʼattɨm" has been corrected into "ijalʼatɨm".</ta>
            <ta e="T29" id="Seg_927" s="T26">[OSV:] unclear verbal form "qalɨmɨt" (stay-PST.NAR-INFER?-3SG.S?).</ta>
            <ta e="T37" id="Seg_928" s="T33">[OSV:] another possible interpretation: "İjalʼa ıllɨ mušɨrätɨ": (child-DIM-NOM down be.cooking-TR-IMP2SG.O)</ta>
            <ta e="T42" id="Seg_929" s="T37">[OSV:] unclear function of the affix "-ıː" in the verbal form "qamtəıːmpatɨ".</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
