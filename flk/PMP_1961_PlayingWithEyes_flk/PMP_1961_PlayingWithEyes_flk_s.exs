<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PMP_1961_PlayingWithEyes_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PMP_1961_PlayingWithEyes_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">831</ud-information>
            <ud-information attribute-name="# HIAT:w">600</ud-information>
            <ud-information attribute-name="# e">600</ud-information>
            <ud-information attribute-name="# HIAT:u">116</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PMP">
            <abbreviation>PMP</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
         <tli id="T204" />
         <tli id="T205" />
         <tli id="T206" />
         <tli id="T207" />
         <tli id="T208" />
         <tli id="T209" />
         <tli id="T210" />
         <tli id="T211" />
         <tli id="T212" />
         <tli id="T213" />
         <tli id="T214" />
         <tli id="T215" />
         <tli id="T216" />
         <tli id="T217" />
         <tli id="T218" />
         <tli id="T219" />
         <tli id="T220" />
         <tli id="T221" />
         <tli id="T222" />
         <tli id="T223" />
         <tli id="T224" />
         <tli id="T225" />
         <tli id="T226" />
         <tli id="T0" />
         <tli id="T227" />
         <tli id="T228" />
         <tli id="T229" />
         <tli id="T230" />
         <tli id="T231" />
         <tli id="T232" />
         <tli id="T233" />
         <tli id="T234" />
         <tli id="T235" />
         <tli id="T236" />
         <tli id="T237" />
         <tli id="T238" />
         <tli id="T239" />
         <tli id="T240" />
         <tli id="T241" />
         <tli id="T242" />
         <tli id="T243" />
         <tli id="T244" />
         <tli id="T245" />
         <tli id="T246" />
         <tli id="T247" />
         <tli id="T248" />
         <tli id="T249" />
         <tli id="T250" />
         <tli id="T251" />
         <tli id="T252" />
         <tli id="T253" />
         <tli id="T254" />
         <tli id="T255" />
         <tli id="T256" />
         <tli id="T257" />
         <tli id="T258" />
         <tli id="T259" />
         <tli id="T260" />
         <tli id="T261" />
         <tli id="T262" />
         <tli id="T263" />
         <tli id="T264" />
         <tli id="T265" />
         <tli id="T266" />
         <tli id="T267" />
         <tli id="T268" />
         <tli id="T269" />
         <tli id="T270" />
         <tli id="T271" />
         <tli id="T272" />
         <tli id="T273" />
         <tli id="T274" />
         <tli id="T275" />
         <tli id="T276" />
         <tli id="T277" />
         <tli id="T278" />
         <tli id="T279" />
         <tli id="T280" />
         <tli id="T281" />
         <tli id="T282" />
         <tli id="T283" />
         <tli id="T284" />
         <tli id="T285" />
         <tli id="T286" />
         <tli id="T287" />
         <tli id="T288" />
         <tli id="T289" />
         <tli id="T290" />
         <tli id="T291" />
         <tli id="T292" />
         <tli id="T293" />
         <tli id="T294" />
         <tli id="T295" />
         <tli id="T296" />
         <tli id="T297" />
         <tli id="T298" />
         <tli id="T299" />
         <tli id="T300" />
         <tli id="T301" />
         <tli id="T302" />
         <tli id="T303" />
         <tli id="T304" />
         <tli id="T305" />
         <tli id="T306" />
         <tli id="T307" />
         <tli id="T308" />
         <tli id="T309" />
         <tli id="T310" />
         <tli id="T311" />
         <tli id="T312" />
         <tli id="T313" />
         <tli id="T314" />
         <tli id="T315" />
         <tli id="T316" />
         <tli id="T317" />
         <tli id="T318" />
         <tli id="T319" />
         <tli id="T320" />
         <tli id="T321" />
         <tli id="T322" />
         <tli id="T323" />
         <tli id="T324" />
         <tli id="T325" />
         <tli id="T326" />
         <tli id="T327" />
         <tli id="T328" />
         <tli id="T329" />
         <tli id="T330" />
         <tli id="T331" />
         <tli id="T332" />
         <tli id="T333" />
         <tli id="T334" />
         <tli id="T335" />
         <tli id="T336" />
         <tli id="T337" />
         <tli id="T338" />
         <tli id="T339" />
         <tli id="T340" />
         <tli id="T341" />
         <tli id="T342" />
         <tli id="T343" />
         <tli id="T344" />
         <tli id="T345" />
         <tli id="T346" />
         <tli id="T347" />
         <tli id="T348" />
         <tli id="T349" />
         <tli id="T350" />
         <tli id="T351" />
         <tli id="T352" />
         <tli id="T353" />
         <tli id="T354" />
         <tli id="T355" />
         <tli id="T356" />
         <tli id="T357" />
         <tli id="T358" />
         <tli id="T359" />
         <tli id="T360" />
         <tli id="T361" />
         <tli id="T362" />
         <tli id="T363" />
         <tli id="T364" />
         <tli id="T365" />
         <tli id="T366" />
         <tli id="T367" />
         <tli id="T368" />
         <tli id="T369" />
         <tli id="T370" />
         <tli id="T371" />
         <tli id="T372" />
         <tli id="T373" />
         <tli id="T374" />
         <tli id="T375" />
         <tli id="T376" />
         <tli id="T377" />
         <tli id="T378" />
         <tli id="T379" />
         <tli id="T380" />
         <tli id="T381" />
         <tli id="T382" />
         <tli id="T383" />
         <tli id="T384" />
         <tli id="T385" />
         <tli id="T386" />
         <tli id="T387" />
         <tli id="T388" />
         <tli id="T389" />
         <tli id="T390" />
         <tli id="T391" />
         <tli id="T392" />
         <tli id="T393" />
         <tli id="T394" />
         <tli id="T395" />
         <tli id="T396" />
         <tli id="T397" />
         <tli id="T398" />
         <tli id="T399" />
         <tli id="T400" />
         <tli id="T401" />
         <tli id="T402" />
         <tli id="T403" />
         <tli id="T404" />
         <tli id="T405" />
         <tli id="T406" />
         <tli id="T407" />
         <tli id="T408" />
         <tli id="T409" />
         <tli id="T410" />
         <tli id="T411" />
         <tli id="T412" />
         <tli id="T413" />
         <tli id="T414" />
         <tli id="T415" />
         <tli id="T416" />
         <tli id="T417" />
         <tli id="T418" />
         <tli id="T419" />
         <tli id="T420" />
         <tli id="T421" />
         <tli id="T422" />
         <tli id="T423" />
         <tli id="T424" />
         <tli id="T425" />
         <tli id="T426" />
         <tli id="T427" />
         <tli id="T428" />
         <tli id="T429" />
         <tli id="T430" />
         <tli id="T431" />
         <tli id="T432" />
         <tli id="T433" />
         <tli id="T434" />
         <tli id="T435" />
         <tli id="T436" />
         <tli id="T437" />
         <tli id="T438" />
         <tli id="T439" />
         <tli id="T440" />
         <tli id="T441" />
         <tli id="T442" />
         <tli id="T443" />
         <tli id="T444" />
         <tli id="T445" />
         <tli id="T446" />
         <tli id="T447" />
         <tli id="T448" />
         <tli id="T449" />
         <tli id="T450" />
         <tli id="T451" />
         <tli id="T452" />
         <tli id="T453" />
         <tli id="T454" />
         <tli id="T455" />
         <tli id="T456" />
         <tli id="T457" />
         <tli id="T458" />
         <tli id="T459" />
         <tli id="T460" />
         <tli id="T461" />
         <tli id="T462" />
         <tli id="T463" />
         <tli id="T464" />
         <tli id="T465" />
         <tli id="T466" />
         <tli id="T467" />
         <tli id="T468" />
         <tli id="T469" />
         <tli id="T470" />
         <tli id="T471" />
         <tli id="T472" />
         <tli id="T473" />
         <tli id="T474" />
         <tli id="T475" />
         <tli id="T476" />
         <tli id="T477" />
         <tli id="T478" />
         <tli id="T479" />
         <tli id="T480" />
         <tli id="T481" />
         <tli id="T482" />
         <tli id="T483" />
         <tli id="T484" />
         <tli id="T485" />
         <tli id="T486" />
         <tli id="T487" />
         <tli id="T488" />
         <tli id="T489" />
         <tli id="T490" />
         <tli id="T491" />
         <tli id="T492" />
         <tli id="T493" />
         <tli id="T494" />
         <tli id="T495" />
         <tli id="T496" />
         <tli id="T497" />
         <tli id="T498" />
         <tli id="T499" />
         <tli id="T500" />
         <tli id="T501" />
         <tli id="T502" />
         <tli id="T503" />
         <tli id="T504" />
         <tli id="T505" />
         <tli id="T506" />
         <tli id="T507" />
         <tli id="T508" />
         <tli id="T509" />
         <tli id="T510" />
         <tli id="T511" />
         <tli id="T512" />
         <tli id="T513" />
         <tli id="T514" />
         <tli id="T515" />
         <tli id="T516" />
         <tli id="T517" />
         <tli id="T518" />
         <tli id="T519" />
         <tli id="T520" />
         <tli id="T521" />
         <tli id="T522" />
         <tli id="T523" />
         <tli id="T524" />
         <tli id="T525" />
         <tli id="T526" />
         <tli id="T527" />
         <tli id="T528" />
         <tli id="T529" />
         <tli id="T530" />
         <tli id="T531" />
         <tli id="T532" />
         <tli id="T533" />
         <tli id="T534" />
         <tli id="T535" />
         <tli id="T536" />
         <tli id="T537" />
         <tli id="T538" />
         <tli id="T539" />
         <tli id="T540" />
         <tli id="T541" />
         <tli id="T542" />
         <tli id="T543" />
         <tli id="T544" />
         <tli id="T545" />
         <tli id="T546" />
         <tli id="T547" />
         <tli id="T548" />
         <tli id="T549" />
         <tli id="T550" />
         <tli id="T551" />
         <tli id="T552" />
         <tli id="T553" />
         <tli id="T554" />
         <tli id="T555" />
         <tli id="T556" />
         <tli id="T557" />
         <tli id="T558" />
         <tli id="T559" />
         <tli id="T560" />
         <tli id="T561" />
         <tli id="T562" />
         <tli id="T563" />
         <tli id="T564" />
         <tli id="T565" />
         <tli id="T566" />
         <tli id="T567" />
         <tli id="T568" />
         <tli id="T569" />
         <tli id="T570" />
         <tli id="T571" />
         <tli id="T572" />
         <tli id="T573" />
         <tli id="T574" />
         <tli id="T575" />
         <tli id="T576" />
         <tli id="T577" />
         <tli id="T578" />
         <tli id="T579" />
         <tli id="T580" />
         <tli id="T581" />
         <tli id="T582" />
         <tli id="T583" />
         <tli id="T584" />
         <tli id="T585" />
         <tli id="T586" />
         <tli id="T587" />
         <tli id="T588" />
         <tli id="T589" />
         <tli id="T590" />
         <tli id="T591" />
         <tli id="T592" />
         <tli id="T593" />
         <tli id="T594" />
         <tli id="T595" />
         <tli id="T596" />
         <tli id="T597" />
         <tli id="T598" />
         <tli id="T599" />
         <tli id="T600" />
         <tli id="T601" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PMP"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T601" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Ilɨzaq</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">warkɨzaq</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">era</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">pajasak</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">Erat</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">palʼdʼukuŋ</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">suːrɨlʼe</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_29" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">Pajat</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">maːtqɨn</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">jewan</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_41" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">Erat</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">palʼdʼikuŋ</ts>
                  <nts id="Seg_47" n="HIAT:ip">,</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">palʼdʼikuŋ</ts>
                  <nts id="Seg_51" n="HIAT:ip">,</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_54" n="HIAT:w" s="T14">tʼeːlʼdʼömp</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_57" n="HIAT:w" s="T15">palʼdʼikuŋ</ts>
                  <nts id="Seg_58" n="HIAT:ip">.</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_61" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_63" n="HIAT:w" s="T16">Qaimnäj</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_66" n="HIAT:w" s="T17">ass</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_69" n="HIAT:w" s="T18">tatkut</ts>
                  <nts id="Seg_70" n="HIAT:ip">.</nts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_73" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_75" n="HIAT:w" s="T19">Kɨdaŋ</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_78" n="HIAT:w" s="T20">niŋ</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_81" n="HIAT:w" s="T21">palʼdʼikuŋ</ts>
                  <nts id="Seg_82" n="HIAT:ip">,</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_85" n="HIAT:w" s="T22">qaimne</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">as</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">tatkut</ts>
                  <nts id="Seg_92" n="HIAT:ip">.</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_95" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_97" n="HIAT:w" s="T25">Okkɨr</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_100" n="HIAT:w" s="T26">dʼel</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_103" n="HIAT:w" s="T27">pajat</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_106" n="HIAT:w" s="T28">teːrban</ts>
                  <nts id="Seg_107" n="HIAT:ip">:</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_110" n="HIAT:w" s="T29">Qajno</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_113" n="HIAT:w" s="T30">ʒə</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_116" n="HIAT:w" s="T31">tap</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_119" n="HIAT:w" s="T32">qaimnäj</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_122" n="HIAT:w" s="T33">as</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_125" n="HIAT:w" s="T34">tatkut</ts>
                  <nts id="Seg_126" n="HIAT:ip">?</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_129" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_131" n="HIAT:w" s="T35">Man</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_134" n="HIAT:w" s="T36">tabɨm</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_137" n="HIAT:w" s="T37">saralǯlʼew</ts>
                  <nts id="Seg_138" n="HIAT:ip">.</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_141" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_143" n="HIAT:w" s="T38">Qajno</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_146" n="HIAT:w" s="T39">tap</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_149" n="HIAT:w" s="T40">sündebɨŋ</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_152" n="HIAT:w" s="T41">tükkuŋ</ts>
                  <nts id="Seg_153" n="HIAT:ip">?</nts>
                  <nts id="Seg_154" n="HIAT:ip">”</nts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_157" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_159" n="HIAT:w" s="T42">Nu</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_162" n="HIAT:w" s="T43">i</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_165" n="HIAT:w" s="T44">nilʼdʼiŋ</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_168" n="HIAT:w" s="T45">meːwɨt</ts>
                  <nts id="Seg_169" n="HIAT:ip">.</nts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_172" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_174" n="HIAT:w" s="T46">Erat</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_177" n="HIAT:w" s="T47">üːbɨcʼlʼe</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_180" n="HIAT:w" s="T48">üːbɨraŋ</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_183" n="HIAT:w" s="T49">suːrulʼe</ts>
                  <nts id="Seg_184" n="HIAT:ip">,</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_187" n="HIAT:w" s="T50">qwannɨŋ</ts>
                  <nts id="Seg_188" n="HIAT:ip">.</nts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_191" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_193" n="HIAT:w" s="T51">Pajat</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_196" n="HIAT:w" s="T52">tʼäːno</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_199" n="HIAT:w" s="T53">tolʼdʼimt</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_202" n="HIAT:w" s="T54">sernat</ts>
                  <nts id="Seg_203" n="HIAT:ip">.</nts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_206" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_208" n="HIAT:w" s="T55">Erand</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_211" n="HIAT:w" s="T56">moqɨn</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_214" n="HIAT:w" s="T57">tʼäno</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_217" n="HIAT:w" s="T58">üːban</ts>
                  <nts id="Seg_218" n="HIAT:ip">.</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_221" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_223" n="HIAT:w" s="T59">Erandɨne</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_226" n="HIAT:w" s="T60">az</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_229" n="HIAT:w" s="T61">aːdolǯikuŋ</ts>
                  <nts id="Seg_230" n="HIAT:ip">.</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_233" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_235" n="HIAT:w" s="T62">Nano</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_238" n="HIAT:w" s="T63">az</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_241" n="HIAT:w" s="T64">aːdolǯikuŋ</ts>
                  <nts id="Seg_242" n="HIAT:ip">,</nts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_245" n="HIAT:w" s="T65">tabɨm</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_248" n="HIAT:w" s="T66">erat</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_251" n="HIAT:w" s="T67">ik</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_254" n="HIAT:w" s="T68">qonǯirnent</ts>
                  <nts id="Seg_255" n="HIAT:ip">.</nts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_258" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_260" n="HIAT:w" s="T69">Erat</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_263" n="HIAT:w" s="T70">medɨŋ</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_266" n="HIAT:w" s="T71">Qɨn</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_269" n="HIAT:w" s="T72">qɨnbart</ts>
                  <nts id="Seg_270" n="HIAT:ip">.</nts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_273" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_275" n="HIAT:w" s="T73">Tam</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_278" n="HIAT:w" s="T74">plʼekaj</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_281" n="HIAT:w" s="T75">qän</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_284" n="HIAT:w" s="T76">paːrɣɨn</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_287" n="HIAT:w" s="T77">ont</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_290" n="HIAT:w" s="T78">sʼäimd</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_293" n="HIAT:w" s="T79">paqqɨlgut</ts>
                  <nts id="Seg_294" n="HIAT:ip">.</nts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_297" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_299" n="HIAT:w" s="T80">Sʼäilamd</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_302" n="HIAT:w" s="T81">ädɨlʼe</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_305" n="HIAT:w" s="T82">qwäčikut</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_308" n="HIAT:w" s="T83">pon</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_311" n="HIAT:w" s="T84">molaɣɨn</ts>
                  <nts id="Seg_312" n="HIAT:ip">.</nts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_315" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_317" n="HIAT:w" s="T85">Taj</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_320" n="HIAT:w" s="T86">plʼekaj</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_323" n="HIAT:w" s="T87">qänt</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_326" n="HIAT:w" s="T88">nʼäzolguŋ</ts>
                  <nts id="Seg_327" n="HIAT:ip">.</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_330" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_332" n="HIAT:w" s="T89">Ondə</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_335" n="HIAT:w" s="T90">tʼärkuŋ</ts>
                  <nts id="Seg_336" n="HIAT:ip">:</nts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_338" n="HIAT:ip">“</nts>
                  <ts e="T92" id="Seg_340" n="HIAT:w" s="T91">Sʼäj</ts>
                  <nts id="Seg_341" n="HIAT:ip">,</nts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_344" n="HIAT:w" s="T92">ropeːs</ts>
                  <nts id="Seg_345" n="HIAT:ip">.</nts>
                  <nts id="Seg_346" n="HIAT:ip">”</nts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_349" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_351" n="HIAT:w" s="T93">Sʼäjla</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_354" n="HIAT:w" s="T94">tabnä</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_357" n="HIAT:w" s="T95">tʼüqwatt</ts>
                  <nts id="Seg_358" n="HIAT:ip">,</nts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_361" n="HIAT:w" s="T96">paːrɨčiqwattə</ts>
                  <nts id="Seg_362" n="HIAT:ip">.</nts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_365" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_367" n="HIAT:w" s="T97">Aj</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_370" n="HIAT:w" s="T98">natʼej</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_373" n="HIAT:w" s="T99">qɨn</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_376" n="HIAT:w" s="T100">paːroɣɨn</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_379" n="HIAT:w" s="T101">seint</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_382" n="HIAT:w" s="T102">paqqɨlʼe</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_385" n="HIAT:w" s="T103">qwecʼikut</ts>
                  <nts id="Seg_386" n="HIAT:ip">,</nts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_389" n="HIAT:w" s="T104">pon</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_392" n="HIAT:w" s="T105">moːland</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_395" n="HIAT:w" s="T106">ɨtkut</ts>
                  <nts id="Seg_396" n="HIAT:ip">.</nts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_399" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_401" n="HIAT:w" s="T107">Tʼärkuŋ</ts>
                  <nts id="Seg_402" n="HIAT:ip">:</nts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_405" n="HIAT:w" s="T108">sʼäjla</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_408" n="HIAT:w" s="T109">roːpeːss</ts>
                  <nts id="Seg_409" n="HIAT:ip">!</nts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T114" id="Seg_412" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_414" n="HIAT:w" s="T110">Sʼäjla</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_417" n="HIAT:w" s="T111">tabne</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_420" n="HIAT:w" s="T112">tʼülʼe</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_423" n="HIAT:w" s="T113">rokecʼqwatt</ts>
                  <nts id="Seg_424" n="HIAT:ip">.</nts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_427" n="HIAT:u" s="T114">
                  <ts e="T116" id="Seg_429" n="HIAT:w" s="T114">Nilʼdʼäredaŋ</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_432" n="HIAT:w" s="T116">selɨj</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_435" n="HIAT:w" s="T117">tʼeːldʼʒom</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_438" n="HIAT:w" s="T118">pünnɨŋ</ts>
                  <nts id="Seg_439" n="HIAT:ip">.</nts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_442" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_444" n="HIAT:w" s="T119">Pajat</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_447" n="HIAT:w" s="T120">mannɨmpɨs</ts>
                  <nts id="Seg_448" n="HIAT:ip">,</nts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_451" n="HIAT:w" s="T121">mannɨmpɨs</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_454" n="HIAT:w" s="T122">toːlakaŋ</ts>
                  <nts id="Seg_455" n="HIAT:ip">.</nts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_458" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_460" n="HIAT:w" s="T123">Erat</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_463" n="HIAT:w" s="T124">seːimd</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_466" n="HIAT:w" s="T125">paqqənnɨt</ts>
                  <nts id="Seg_467" n="HIAT:ip">.</nts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_470" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_472" n="HIAT:w" s="T126">Tam</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_475" n="HIAT:w" s="T127">plʼekaj</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_478" n="HIAT:w" s="T128">qän</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_481" n="HIAT:w" s="T129">känbart</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_484" n="HIAT:w" s="T130">qwädʼät</ts>
                  <nts id="Seg_485" n="HIAT:ip">.</nts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T138" id="Seg_488" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_490" n="HIAT:w" s="T131">A</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_493" n="HIAT:w" s="T132">pajat</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_496" n="HIAT:w" s="T133">tʼäːno</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_499" n="HIAT:w" s="T134">tabɨn</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_502" n="HIAT:w" s="T135">sejlamd</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_505" n="HIAT:w" s="T136">iːot</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_508" n="HIAT:w" s="T137">toːlakaŋ</ts>
                  <nts id="Seg_509" n="HIAT:ip">.</nts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_512" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_514" n="HIAT:w" s="T138">Ontə</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_517" n="HIAT:w" s="T139">teːrpan</ts>
                  <nts id="Seg_518" n="HIAT:ip">:</nts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_520" n="HIAT:ip">“</nts>
                  <ts e="T141" id="Seg_522" n="HIAT:w" s="T140">A</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_525" n="HIAT:w" s="T141">tan</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_528" n="HIAT:w" s="T142">nano</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_531" n="HIAT:w" s="T143">maːttə</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_534" n="HIAT:w" s="T144">qaimnej</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_537" n="HIAT:w" s="T145">as</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_540" n="HIAT:w" s="T146">tatqwal</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_543" n="HIAT:w" s="T147">meŋga</ts>
                  <nts id="Seg_544" n="HIAT:ip">.</nts>
                  <nts id="Seg_545" n="HIAT:ip">”</nts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T155" id="Seg_548" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_550" n="HIAT:w" s="T148">Nilʼdʼiŋ</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_553" n="HIAT:w" s="T149">pajat</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_556" n="HIAT:w" s="T150">qwannɨŋ</ts>
                  <nts id="Seg_557" n="HIAT:ip">,</nts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_560" n="HIAT:w" s="T151">qwannɨŋ</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_563" n="HIAT:w" s="T152">i</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_566" n="HIAT:w" s="T153">tʼönǯikaŋ</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_569" n="HIAT:w" s="T154">čaːǯiŋ</ts>
                  <nts id="Seg_570" n="HIAT:ip">.</nts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T159" id="Seg_573" n="HIAT:u" s="T155">
                  <ts e="T156" id="Seg_575" n="HIAT:w" s="T155">Erantɨnan</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_578" n="HIAT:w" s="T156">seiːt</ts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_581" n="HIAT:w" s="T157">tidam</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_584" n="HIAT:w" s="T158">tʼäŋwan</ts>
                  <nts id="Seg_585" n="HIAT:ip">.</nts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T163" id="Seg_588" n="HIAT:u" s="T159">
                  <ts e="T160" id="Seg_590" n="HIAT:w" s="T159">Qwäcʼku</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_593" n="HIAT:w" s="T160">tabɨm</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_596" n="HIAT:w" s="T161">tab</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_599" n="HIAT:w" s="T162">ürgenǯiŋ</ts>
                  <nts id="Seg_600" n="HIAT:ip">.</nts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_603" n="HIAT:u" s="T163">
                  <ts e="T164" id="Seg_605" n="HIAT:w" s="T163">Nano</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_608" n="HIAT:w" s="T164">i</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_611" n="HIAT:w" s="T165">tʼönǯikaŋ</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_614" n="HIAT:w" s="T166">čaːǯiŋ</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_617" n="HIAT:w" s="T167">pajat</ts>
                  <nts id="Seg_618" n="HIAT:ip">.</nts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T177" id="Seg_621" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_623" n="HIAT:w" s="T168">Kušaŋ</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_626" n="HIAT:w" s="T169">čaǯiŋ</ts>
                  <nts id="Seg_627" n="HIAT:ip">,</nts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_630" n="HIAT:w" s="T170">aj</ts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_633" n="HIAT:w" s="T171">moɣunä</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_636" n="HIAT:w" s="T172">manǯedʼikuŋ</ts>
                  <nts id="Seg_637" n="HIAT:ip">,</nts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_640" n="HIAT:w" s="T173">erat</ts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_643" n="HIAT:w" s="T174">čaːǯiŋ</ts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_646" n="HIAT:w" s="T175">alʼi</ts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_649" n="HIAT:w" s="T176">ass</ts>
                  <nts id="Seg_650" n="HIAT:ip">.</nts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_653" n="HIAT:u" s="T177">
                  <ts e="T178" id="Seg_655" n="HIAT:w" s="T177">Pajant</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_658" n="HIAT:w" s="T178">qwalʼewlʼe</ts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_661" n="HIAT:w" s="T179">erat</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_664" n="HIAT:w" s="T180">kušaŋ</ts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_667" n="HIAT:w" s="T181">laŋgɨniŋ</ts>
                  <nts id="Seg_668" n="HIAT:ip">:</nts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_670" n="HIAT:ip">“</nts>
                  <ts e="T183" id="Seg_672" n="HIAT:w" s="T182">Sʼäjla</ts>
                  <nts id="Seg_673" n="HIAT:ip">,</nts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_676" n="HIAT:w" s="T183">ropeːss</ts>
                  <nts id="Seg_677" n="HIAT:ip">!</nts>
                  <nts id="Seg_678" n="HIAT:ip">”</nts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T192" id="Seg_681" n="HIAT:u" s="T184">
                  <ts e="T185" id="Seg_683" n="HIAT:w" s="T184">Sʼäjla</ts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_686" n="HIAT:w" s="T185">tʼäŋgwattə</ts>
                  <nts id="Seg_687" n="HIAT:ip">,</nts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_690" n="HIAT:w" s="T186">as</ts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_693" n="HIAT:w" s="T187">tükwatte</ts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_696" n="HIAT:w" s="T188">tabnä</ts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_699" n="HIAT:w" s="T189">i</ts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_702" n="HIAT:w" s="T190">ass</ts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_705" n="HIAT:w" s="T191">parɨdʼiqwattə</ts>
                  <nts id="Seg_706" n="HIAT:ip">.</nts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T202" id="Seg_709" n="HIAT:u" s="T192">
                  <ts e="T193" id="Seg_711" n="HIAT:w" s="T192">Kušaŋ</ts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_714" n="HIAT:w" s="T193">sʼäjlamt</ts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_717" n="HIAT:w" s="T194">qwärɨstə</ts>
                  <nts id="Seg_718" n="HIAT:ip">,</nts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_721" n="HIAT:w" s="T195">qwärɨst</ts>
                  <nts id="Seg_722" n="HIAT:ip">,</nts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_725" n="HIAT:w" s="T196">sʼäila</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_728" n="HIAT:w" s="T197">kundar</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_731" n="HIAT:w" s="T198">tʼäŋgwattə</ts>
                  <nts id="Seg_732" n="HIAT:ip">,</nts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_735" n="HIAT:w" s="T199">niŋ</ts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_738" n="HIAT:w" s="T200">i</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_741" n="HIAT:w" s="T201">tʼäŋgwatt</ts>
                  <nts id="Seg_742" n="HIAT:ip">.</nts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T208" id="Seg_745" n="HIAT:u" s="T202">
                  <ts e="T203" id="Seg_747" n="HIAT:w" s="T202">Tidam</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_750" n="HIAT:w" s="T203">sejgalɨk</ts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_753" n="HIAT:w" s="T204">erat</ts>
                  <nts id="Seg_754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_756" n="HIAT:w" s="T205">na</ts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_759" n="HIAT:w" s="T206">üːbedʼän</ts>
                  <nts id="Seg_760" n="HIAT:ip">,</nts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_763" n="HIAT:w" s="T207">sʼäjgalɨk</ts>
                  <nts id="Seg_764" n="HIAT:ip">.</nts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T213" id="Seg_767" n="HIAT:u" s="T208">
                  <ts e="T209" id="Seg_769" n="HIAT:w" s="T208">Pajat</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_772" n="HIAT:w" s="T209">aːdelǯiŋ</ts>
                  <nts id="Seg_773" n="HIAT:ip">,</nts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_776" n="HIAT:w" s="T210">erandɨne</ts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_779" n="HIAT:w" s="T211">az</ts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_782" n="HIAT:w" s="T212">aːdelǯikuŋ</ts>
                  <nts id="Seg_783" n="HIAT:ip">.</nts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T217" id="Seg_786" n="HIAT:u" s="T213">
                  <ts e="T214" id="Seg_788" n="HIAT:w" s="T213">Erat</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_791" n="HIAT:w" s="T214">sejgalɨk</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_794" n="HIAT:w" s="T215">ürgɨle</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_797" n="HIAT:w" s="T216">qunǯiŋ</ts>
                  <nts id="Seg_798" n="HIAT:ip">.</nts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T221" id="Seg_801" n="HIAT:u" s="T217">
                  <ts e="T218" id="Seg_803" n="HIAT:w" s="T217">Madɨn</ts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_806" n="HIAT:w" s="T218">köndə</ts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_809" n="HIAT:w" s="T219">miglʼe</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_812" n="HIAT:w" s="T220">üːbəraq</ts>
                  <nts id="Seg_813" n="HIAT:ip">.</nts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T223" id="Seg_816" n="HIAT:u" s="T221">
                  <ts e="T222" id="Seg_818" n="HIAT:w" s="T221">Pajat</ts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_821" n="HIAT:w" s="T222">tɨdəmban</ts>
                  <nts id="Seg_822" n="HIAT:ip">.</nts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T228" id="Seg_825" n="HIAT:u" s="T223">
                  <ts e="T224" id="Seg_827" n="HIAT:w" s="T223">Tʼäːno</ts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_830" n="HIAT:w" s="T224">medɨdʼeŋ</ts>
                  <nts id="Seg_831" n="HIAT:ip">,</nts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_834" n="HIAT:w" s="T225">tolʼdʼilamd</ts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0" id="Seg_837" n="HIAT:w" s="T226">niŋ</ts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_840" n="HIAT:w" s="T0">el</ts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_843" n="HIAT:w" s="T227">čaɣannɨt</ts>
                  <nts id="Seg_844" n="HIAT:ip">.</nts>
                  <nts id="Seg_845" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T231" id="Seg_847" n="HIAT:u" s="T228">
                  <ts e="T229" id="Seg_849" n="HIAT:w" s="T228">Mattə</ts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_852" n="HIAT:w" s="T229">seːrnɨŋ</ts>
                  <nts id="Seg_853" n="HIAT:ip">,</nts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_856" n="HIAT:w" s="T230">nʼäɣɨniŋ</ts>
                  <nts id="Seg_857" n="HIAT:ip">.</nts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T238" id="Seg_860" n="HIAT:u" s="T231">
                  <ts e="T232" id="Seg_862" n="HIAT:w" s="T231">Erant</ts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_865" n="HIAT:w" s="T232">sejlam</ts>
                  <nts id="Seg_866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_868" n="HIAT:w" s="T233">tɨsont</ts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_871" n="HIAT:w" s="T234">pennɨt</ts>
                  <nts id="Seg_872" n="HIAT:ip">,</nts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_875" n="HIAT:w" s="T235">pʼötʼen</ts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_878" n="HIAT:w" s="T236">moqtə</ts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_881" n="HIAT:w" s="T237">oːcʼiŋnɨt</ts>
                  <nts id="Seg_882" n="HIAT:ip">.</nts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T243" id="Seg_885" n="HIAT:u" s="T238">
                  <ts e="T239" id="Seg_887" n="HIAT:w" s="T238">Erat</ts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_890" n="HIAT:w" s="T239">tʼönǯikaŋ</ts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_893" n="HIAT:w" s="T240">tüan</ts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_896" n="HIAT:w" s="T241">maːttə</ts>
                  <nts id="Seg_897" n="HIAT:ip">,</nts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_900" n="HIAT:w" s="T242">sernaŋ</ts>
                  <nts id="Seg_901" n="HIAT:ip">.</nts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T251" id="Seg_904" n="HIAT:u" s="T243">
                  <ts e="T244" id="Seg_906" n="HIAT:w" s="T243">Pajat</ts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_909" n="HIAT:w" s="T244">tʼärɨŋ</ts>
                  <nts id="Seg_910" n="HIAT:ip">:</nts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_912" n="HIAT:ip">“</nts>
                  <ts e="T246" id="Seg_914" n="HIAT:w" s="T245">A</ts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_917" n="HIAT:w" s="T246">tan</ts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_920" n="HIAT:w" s="T247">qajno</ts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_923" n="HIAT:w" s="T248">tamdʼel</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_926" n="HIAT:w" s="T249">kundɨŋ</ts>
                  <nts id="Seg_927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_929" n="HIAT:w" s="T250">jewant</ts>
                  <nts id="Seg_930" n="HIAT:ip">?</nts>
                  <nts id="Seg_931" n="HIAT:ip">”</nts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T260" id="Seg_934" n="HIAT:u" s="T251">
                  <ts e="T252" id="Seg_936" n="HIAT:w" s="T251">Erat</ts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_939" n="HIAT:w" s="T252">tʼärɨŋ</ts>
                  <nts id="Seg_940" n="HIAT:ip">:</nts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_942" n="HIAT:ip">“</nts>
                  <ts e="T254" id="Seg_944" n="HIAT:w" s="T253">Man</ts>
                  <nts id="Seg_945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_947" n="HIAT:w" s="T254">tamdʼel</ts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_950" n="HIAT:w" s="T255">sejowɨm</ts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_953" n="HIAT:w" s="T256">aːtsaŋ</ts>
                  <nts id="Seg_954" n="HIAT:ip">,</nts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_957" n="HIAT:w" s="T257">nano</ts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_960" n="HIAT:w" s="T258">kundɨŋ</ts>
                  <nts id="Seg_961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_963" n="HIAT:w" s="T259">jewaŋ</ts>
                  <nts id="Seg_964" n="HIAT:ip">.</nts>
                  <nts id="Seg_965" n="HIAT:ip">”</nts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T272" id="Seg_968" n="HIAT:u" s="T260">
                  <ts e="T261" id="Seg_970" n="HIAT:w" s="T260">Pajat</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_973" n="HIAT:w" s="T261">teːrban</ts>
                  <nts id="Seg_974" n="HIAT:ip">:</nts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_976" n="HIAT:ip">“</nts>
                  <ts e="T263" id="Seg_978" n="HIAT:w" s="T262">A</ts>
                  <nts id="Seg_979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_981" n="HIAT:w" s="T263">tan</ts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_984" n="HIAT:w" s="T264">siːrant</ts>
                  <nts id="Seg_985" n="HIAT:ip">,</nts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_988" n="HIAT:w" s="T265">man</ts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_991" n="HIAT:w" s="T266">tidam</ts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_994" n="HIAT:w" s="T267">tastɨ</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_997" n="HIAT:w" s="T268">tunuaŋ</ts>
                  <nts id="Seg_998" n="HIAT:ip">,</nts>
                  <nts id="Seg_999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1001" n="HIAT:w" s="T269">kundar</ts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1004" n="HIAT:w" s="T270">tan</ts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1007" n="HIAT:w" s="T271">suːrunʼänt</ts>
                  <nts id="Seg_1008" n="HIAT:ip">.</nts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T276" id="Seg_1011" n="HIAT:u" s="T272">
                  <ts e="T273" id="Seg_1013" n="HIAT:w" s="T272">Selɨj</ts>
                  <nts id="Seg_1014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1016" n="HIAT:w" s="T273">tʼelʼdʼzʼomp</ts>
                  <nts id="Seg_1017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1019" n="HIAT:w" s="T274">sejlandɨse</ts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1022" n="HIAT:w" s="T275">warkand</ts>
                  <nts id="Seg_1023" n="HIAT:ip">.</nts>
                  <nts id="Seg_1024" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T286" id="Seg_1026" n="HIAT:u" s="T276">
                  <ts e="T277" id="Seg_1028" n="HIAT:w" s="T276">Aw</ts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1031" n="HIAT:w" s="T277">qɨn</ts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1034" n="HIAT:w" s="T278">baːrt</ts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1037" n="HIAT:w" s="T279">ɨtqwäl</ts>
                  <nts id="Seg_1038" n="HIAT:ip">,</nts>
                  <nts id="Seg_1039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1041" n="HIAT:w" s="T280">a</ts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1044" n="HIAT:w" s="T281">tam</ts>
                  <nts id="Seg_1045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1047" n="HIAT:w" s="T282">plʼekaj</ts>
                  <nts id="Seg_1048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1050" n="HIAT:w" s="T283">kɨn</ts>
                  <nts id="Seg_1051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1053" n="HIAT:w" s="T284">baːrt</ts>
                  <nts id="Seg_1054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1056" n="HIAT:w" s="T285">neːzolgwant</ts>
                  <nts id="Seg_1057" n="HIAT:ip">.</nts>
                  <nts id="Seg_1058" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T290" id="Seg_1060" n="HIAT:u" s="T286">
                  <ts e="T287" id="Seg_1062" n="HIAT:w" s="T286">Sejlal</ts>
                  <nts id="Seg_1063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1065" n="HIAT:w" s="T287">tʼeŋga</ts>
                  <nts id="Seg_1066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1068" n="HIAT:w" s="T288">tʼülʼe</ts>
                  <nts id="Seg_1069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1071" n="HIAT:w" s="T289">parɨcʼikwattə</ts>
                  <nts id="Seg_1072" n="HIAT:ip">.</nts>
                  <nts id="Seg_1073" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T295" id="Seg_1075" n="HIAT:u" s="T290">
                  <ts e="T291" id="Seg_1077" n="HIAT:w" s="T290">Nilʼdʼiŋ</ts>
                  <nts id="Seg_1078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1080" n="HIAT:w" s="T291">tan</ts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1083" n="HIAT:w" s="T292">i</ts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1086" n="HIAT:w" s="T293">sündebɨŋ</ts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1089" n="HIAT:w" s="T294">tükkuzant</ts>
                  <nts id="Seg_1090" n="HIAT:ip">.</nts>
                  <nts id="Seg_1091" n="HIAT:ip">”</nts>
                  <nts id="Seg_1092" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T299" id="Seg_1094" n="HIAT:u" s="T295">
                  <ts e="T296" id="Seg_1096" n="HIAT:w" s="T295">Nu</ts>
                  <nts id="Seg_1097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1099" n="HIAT:w" s="T296">i</ts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1102" n="HIAT:w" s="T297">warkɨlʼe</ts>
                  <nts id="Seg_1103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1105" n="HIAT:w" s="T298">üːbəraq</ts>
                  <nts id="Seg_1106" n="HIAT:ip">.</nts>
                  <nts id="Seg_1107" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T303" id="Seg_1109" n="HIAT:u" s="T299">
                  <ts e="T300" id="Seg_1111" n="HIAT:w" s="T299">Kušaj</ts>
                  <nts id="Seg_1112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1114" n="HIAT:w" s="T300">da</ts>
                  <nts id="Seg_1115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1117" n="HIAT:w" s="T301">dʼeːlɨt</ts>
                  <nts id="Seg_1118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1120" n="HIAT:w" s="T302">mendɨŋ</ts>
                  <nts id="Seg_1121" n="HIAT:ip">.</nts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T309" id="Seg_1124" n="HIAT:u" s="T303">
                  <ts e="T304" id="Seg_1126" n="HIAT:w" s="T303">Pajat</ts>
                  <nts id="Seg_1127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1129" n="HIAT:w" s="T304">manǯediŋ</ts>
                  <nts id="Seg_1130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1132" n="HIAT:w" s="T305">südernaun</ts>
                  <nts id="Seg_1133" n="HIAT:ip">,</nts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1136" n="HIAT:w" s="T306">manǯediŋ</ts>
                  <nts id="Seg_1137" n="HIAT:ip">,</nts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1140" n="HIAT:w" s="T307">nɨŋan</ts>
                  <nts id="Seg_1141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1143" n="HIAT:w" s="T308">peŋk</ts>
                  <nts id="Seg_1144" n="HIAT:ip">.</nts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T314" id="Seg_1147" n="HIAT:u" s="T309">
                  <ts e="T310" id="Seg_1149" n="HIAT:w" s="T309">Pajat</ts>
                  <nts id="Seg_1150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1152" n="HIAT:w" s="T310">erandɨne</ts>
                  <nts id="Seg_1153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1155" n="HIAT:w" s="T311">tʼärɨŋ</ts>
                  <nts id="Seg_1156" n="HIAT:ip">:</nts>
                  <nts id="Seg_1157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1158" n="HIAT:ip">“</nts>
                  <ts e="T313" id="Seg_1160" n="HIAT:w" s="T312">Peŋkə</ts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1163" n="HIAT:w" s="T313">nɨŋgɨnt</ts>
                  <nts id="Seg_1164" n="HIAT:ip">.</nts>
                  <nts id="Seg_1165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T319" id="Seg_1167" n="HIAT:u" s="T314">
                  <ts e="T315" id="Seg_1169" n="HIAT:w" s="T314">Tan</ts>
                  <nts id="Seg_1170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1172" n="HIAT:w" s="T315">wazlʼent</ts>
                  <nts id="Seg_1173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1175" n="HIAT:w" s="T316">kundarem</ts>
                  <nts id="Seg_1176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1178" n="HIAT:w" s="T317">tʼäʒlʼel</ts>
                  <nts id="Seg_1179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1181" n="HIAT:w" s="T318">peŋkəm</ts>
                  <nts id="Seg_1182" n="HIAT:ip">.</nts>
                  <nts id="Seg_1183" n="HIAT:ip">”</nts>
                  <nts id="Seg_1184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T325" id="Seg_1186" n="HIAT:u" s="T319">
                  <ts e="T320" id="Seg_1188" n="HIAT:w" s="T319">A</ts>
                  <nts id="Seg_1189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1191" n="HIAT:w" s="T320">erat</ts>
                  <nts id="Seg_1192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1194" n="HIAT:w" s="T321">tʼärɨŋ</ts>
                  <nts id="Seg_1195" n="HIAT:ip">:</nts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1197" n="HIAT:ip">“</nts>
                  <ts e="T323" id="Seg_1199" n="HIAT:w" s="T322">Man</ts>
                  <nts id="Seg_1200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1202" n="HIAT:w" s="T323">kundar</ts>
                  <nts id="Seg_1203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1205" n="HIAT:w" s="T324">tʼäǯenǯau</ts>
                  <nts id="Seg_1206" n="HIAT:ip">?</nts>
                  <nts id="Seg_1207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T328" id="Seg_1209" n="HIAT:u" s="T325">
                  <ts e="T326" id="Seg_1211" n="HIAT:w" s="T325">Sejew</ts>
                  <nts id="Seg_1212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1214" n="HIAT:w" s="T326">az</ts>
                  <nts id="Seg_1215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1217" n="HIAT:w" s="T327">aːduŋ</ts>
                  <nts id="Seg_1218" n="HIAT:ip">.</nts>
                  <nts id="Seg_1219" n="HIAT:ip">”</nts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T336" id="Seg_1222" n="HIAT:u" s="T328">
                  <ts e="T329" id="Seg_1224" n="HIAT:w" s="T328">Pajat</ts>
                  <nts id="Seg_1225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1227" n="HIAT:w" s="T329">tʼärɨŋ</ts>
                  <nts id="Seg_1228" n="HIAT:ip">:</nts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1230" n="HIAT:ip">“</nts>
                  <ts e="T331" id="Seg_1232" n="HIAT:w" s="T330">Tüqɨ</ts>
                  <nts id="Seg_1233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1235" n="HIAT:w" s="T331">meŋga</ts>
                  <nts id="Seg_1236" n="HIAT:ip">,</nts>
                  <nts id="Seg_1237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1239" n="HIAT:w" s="T332">man</ts>
                  <nts id="Seg_1240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1242" n="HIAT:w" s="T333">tulsem</ts>
                  <nts id="Seg_1243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1245" n="HIAT:w" s="T334">teŋga</ts>
                  <nts id="Seg_1246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1248" n="HIAT:w" s="T335">oralǯolǯilʼews</ts>
                  <nts id="Seg_1249" n="HIAT:ip">.</nts>
                  <nts id="Seg_1250" n="HIAT:ip">”</nts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T340" id="Seg_1253" n="HIAT:u" s="T336">
                  <ts e="T337" id="Seg_1255" n="HIAT:w" s="T336">I</ts>
                  <nts id="Seg_1256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1258" n="HIAT:w" s="T337">erat</ts>
                  <nts id="Seg_1259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1261" n="HIAT:w" s="T338">tʼüan</ts>
                  <nts id="Seg_1262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1264" n="HIAT:w" s="T339">pajandɨne</ts>
                  <nts id="Seg_1265" n="HIAT:ip">.</nts>
                  <nts id="Seg_1266" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T346" id="Seg_1268" n="HIAT:u" s="T340">
                  <ts e="T341" id="Seg_1270" n="HIAT:w" s="T340">Pajat</ts>
                  <nts id="Seg_1271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1273" n="HIAT:w" s="T341">erandɨne</ts>
                  <nts id="Seg_1274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1276" n="HIAT:w" s="T342">oralǯolǯit</ts>
                  <nts id="Seg_1277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1279" n="HIAT:w" s="T343">udoɣɨnt</ts>
                  <nts id="Seg_1280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1282" n="HIAT:w" s="T344">apstɨmbɨdi</ts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1285" n="HIAT:w" s="T345">tʼülsem</ts>
                  <nts id="Seg_1286" n="HIAT:ip">.</nts>
                  <nts id="Seg_1287" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T352" id="Seg_1289" n="HIAT:u" s="T346">
                  <ts e="T347" id="Seg_1291" n="HIAT:w" s="T346">Ond</ts>
                  <nts id="Seg_1292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1294" n="HIAT:w" s="T347">tabne</ts>
                  <nts id="Seg_1295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1297" n="HIAT:w" s="T348">pɨmʒɨrɨt</ts>
                  <nts id="Seg_1298" n="HIAT:ip">,</nts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1301" n="HIAT:w" s="T349">kundar</ts>
                  <nts id="Seg_1302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1304" n="HIAT:w" s="T350">tʼätku</ts>
                  <nts id="Seg_1305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1307" n="HIAT:w" s="T351">peŋgɨm</ts>
                  <nts id="Seg_1308" n="HIAT:ip">.</nts>
                  <nts id="Seg_1309" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T356" id="Seg_1311" n="HIAT:u" s="T352">
                  <ts e="T353" id="Seg_1313" n="HIAT:w" s="T352">Erat</ts>
                  <nts id="Seg_1314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1316" n="HIAT:w" s="T353">tʼäʒit</ts>
                  <nts id="Seg_1317" n="HIAT:ip">,</nts>
                  <nts id="Seg_1318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1320" n="HIAT:w" s="T354">qwannɨt</ts>
                  <nts id="Seg_1321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1323" n="HIAT:w" s="T355">peŋgɨm</ts>
                  <nts id="Seg_1324" n="HIAT:ip">.</nts>
                  <nts id="Seg_1325" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T364" id="Seg_1327" n="HIAT:u" s="T356">
                  <ts e="T357" id="Seg_1329" n="HIAT:w" s="T356">Erand</ts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1332" n="HIAT:w" s="T357">sej</ts>
                  <nts id="Seg_1333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1335" n="HIAT:w" s="T358">az</ts>
                  <nts id="Seg_1336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1338" n="HIAT:w" s="T359">aːduŋ</ts>
                  <nts id="Seg_1339" n="HIAT:ip">,</nts>
                  <nts id="Seg_1340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1342" n="HIAT:w" s="T360">peŋgɨm</ts>
                  <nts id="Seg_1343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1345" n="HIAT:w" s="T361">tabne</ts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1348" n="HIAT:w" s="T362">as</ts>
                  <nts id="Seg_1349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1351" n="HIAT:w" s="T363">kɨrgu</ts>
                  <nts id="Seg_1352" n="HIAT:ip">.</nts>
                  <nts id="Seg_1353" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T369" id="Seg_1355" n="HIAT:u" s="T364">
                  <ts e="T365" id="Seg_1357" n="HIAT:w" s="T364">Pajat</ts>
                  <nts id="Seg_1358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1360" n="HIAT:w" s="T365">ondə</ts>
                  <nts id="Seg_1361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1363" n="HIAT:w" s="T366">kɨrlʼe</ts>
                  <nts id="Seg_1364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1366" n="HIAT:w" s="T367">üːbərɨt</ts>
                  <nts id="Seg_1367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1369" n="HIAT:w" s="T368">peŋgɨm</ts>
                  <nts id="Seg_1370" n="HIAT:ip">.</nts>
                  <nts id="Seg_1371" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T373" id="Seg_1373" n="HIAT:u" s="T369">
                  <ts e="T370" id="Seg_1375" n="HIAT:w" s="T369">Nu</ts>
                  <nts id="Seg_1376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1378" n="HIAT:w" s="T370">i</ts>
                  <nts id="Seg_1379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1381" n="HIAT:w" s="T371">kɨrɨt</ts>
                  <nts id="Seg_1382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1384" n="HIAT:w" s="T372">peŋgɨm</ts>
                  <nts id="Seg_1385" n="HIAT:ip">.</nts>
                  <nts id="Seg_1386" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T376" id="Seg_1388" n="HIAT:u" s="T373">
                  <ts e="T374" id="Seg_1390" n="HIAT:w" s="T373">Wadʼilam</ts>
                  <nts id="Seg_1391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1393" n="HIAT:w" s="T374">nʼörbannɨt</ts>
                  <nts id="Seg_1394" n="HIAT:ip">,</nts>
                  <nts id="Seg_1395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1397" n="HIAT:w" s="T375">ɨːdɨt</ts>
                  <nts id="Seg_1398" n="HIAT:ip">.</nts>
                  <nts id="Seg_1399" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T381" id="Seg_1401" n="HIAT:u" s="T376">
                  <ts e="T377" id="Seg_1403" n="HIAT:w" s="T376">Okkɨr</ts>
                  <nts id="Seg_1404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1406" n="HIAT:w" s="T377">dʼel</ts>
                  <nts id="Seg_1407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1409" n="HIAT:w" s="T378">wadʼilam</ts>
                  <nts id="Seg_1410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1412" n="HIAT:w" s="T379">kocʼin</ts>
                  <nts id="Seg_1413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1415" n="HIAT:w" s="T380">ponnɨt</ts>
                  <nts id="Seg_1416" n="HIAT:ip">.</nts>
                  <nts id="Seg_1417" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T390" id="Seg_1419" n="HIAT:u" s="T381">
                  <ts e="T382" id="Seg_1421" n="HIAT:w" s="T381">Erandɨne</ts>
                  <nts id="Seg_1422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1424" n="HIAT:w" s="T382">tʼärɨŋ</ts>
                  <nts id="Seg_1425" n="HIAT:ip">:</nts>
                  <nts id="Seg_1426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1427" n="HIAT:ip">“</nts>
                  <ts e="T384" id="Seg_1429" n="HIAT:w" s="T383">Tamdʼel</ts>
                  <nts id="Seg_1430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1432" n="HIAT:w" s="T384">qula</ts>
                  <nts id="Seg_1433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1435" n="HIAT:w" s="T385">tʼarattə</ts>
                  <nts id="Seg_1436" n="HIAT:ip">,</nts>
                  <nts id="Seg_1437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1439" n="HIAT:w" s="T386">tamdʼelʼe</ts>
                  <nts id="Seg_1440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1442" n="HIAT:w" s="T387">dʼel</ts>
                  <nts id="Seg_1443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1445" n="HIAT:w" s="T388">iːbkaj</ts>
                  <nts id="Seg_1446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1448" n="HIAT:w" s="T389">nuːlʼdʼel</ts>
                  <nts id="Seg_1449" n="HIAT:ip">.</nts>
                  <nts id="Seg_1450" n="HIAT:ip">”</nts>
                  <nts id="Seg_1451" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T393" id="Seg_1453" n="HIAT:u" s="T390">
                  <ts e="T391" id="Seg_1455" n="HIAT:w" s="T390">Erat</ts>
                  <nts id="Seg_1456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1458" n="HIAT:w" s="T391">tʼärɨŋ</ts>
                  <nts id="Seg_1459" n="HIAT:ip">:</nts>
                  <nts id="Seg_1460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1461" n="HIAT:ip">“</nts>
                  <ts e="T393" id="Seg_1463" n="HIAT:w" s="T392">Nuːlʼdʼelǯik</ts>
                  <nts id="Seg_1464" n="HIAT:ip">.</nts>
                  <nts id="Seg_1465" n="HIAT:ip">”</nts>
                  <nts id="Seg_1466" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T397" id="Seg_1468" n="HIAT:u" s="T393">
                  <ts e="T394" id="Seg_1470" n="HIAT:w" s="T393">Nu</ts>
                  <nts id="Seg_1471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1473" n="HIAT:w" s="T394">i</ts>
                  <nts id="Seg_1474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1476" n="HIAT:w" s="T395">nuːlʼdʼeːlčlʼe</ts>
                  <nts id="Seg_1477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1479" n="HIAT:w" s="T396">üːbərak</ts>
                  <nts id="Seg_1480" n="HIAT:ip">.</nts>
                  <nts id="Seg_1481" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T402" id="Seg_1483" n="HIAT:u" s="T397">
                  <ts e="T398" id="Seg_1485" n="HIAT:w" s="T397">Pajat</ts>
                  <nts id="Seg_1486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1488" n="HIAT:w" s="T398">ponen</ts>
                  <nts id="Seg_1489" n="HIAT:ip">,</nts>
                  <nts id="Seg_1490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1492" n="HIAT:w" s="T399">erat</ts>
                  <nts id="Seg_1493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1495" n="HIAT:w" s="T400">maːtqɨn</ts>
                  <nts id="Seg_1496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1498" n="HIAT:w" s="T401">jewan</ts>
                  <nts id="Seg_1499" n="HIAT:ip">.</nts>
                  <nts id="Seg_1500" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T406" id="Seg_1502" n="HIAT:u" s="T402">
                  <ts e="T403" id="Seg_1504" n="HIAT:w" s="T402">Pajat</ts>
                  <nts id="Seg_1505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1507" n="HIAT:w" s="T403">ponen</ts>
                  <nts id="Seg_1508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1510" n="HIAT:w" s="T404">äːdɨt</ts>
                  <nts id="Seg_1511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1513" n="HIAT:w" s="T405">kuːgɨrsanɨm</ts>
                  <nts id="Seg_1514" n="HIAT:ip">.</nts>
                  <nts id="Seg_1515" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T408" id="Seg_1517" n="HIAT:u" s="T406">
                  <ts e="T407" id="Seg_1519" n="HIAT:w" s="T406">Kuːgɨrkučilʼe</ts>
                  <nts id="Seg_1520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1522" n="HIAT:w" s="T407">üːbəraŋ</ts>
                  <nts id="Seg_1523" n="HIAT:ip">.</nts>
                  <nts id="Seg_1524" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T411" id="Seg_1526" n="HIAT:u" s="T408">
                  <ts e="T409" id="Seg_1528" n="HIAT:w" s="T408">Erat</ts>
                  <nts id="Seg_1529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1531" n="HIAT:w" s="T409">maːtqɨn</ts>
                  <nts id="Seg_1532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1534" n="HIAT:w" s="T410">matškaŋ</ts>
                  <nts id="Seg_1535" n="HIAT:ip">.</nts>
                  <nts id="Seg_1536" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T417" id="Seg_1538" n="HIAT:u" s="T411">
                  <ts e="T412" id="Seg_1540" n="HIAT:w" s="T411">A</ts>
                  <nts id="Seg_1541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1543" n="HIAT:w" s="T412">paja</ts>
                  <nts id="Seg_1544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1546" n="HIAT:w" s="T413">poːnen</ts>
                  <nts id="Seg_1547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1549" n="HIAT:w" s="T414">kugorčiŋ</ts>
                  <nts id="Seg_1550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1552" n="HIAT:w" s="T415">i</ts>
                  <nts id="Seg_1553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1555" n="HIAT:w" s="T416">kugorčiŋ</ts>
                  <nts id="Seg_1556" n="HIAT:ip">.</nts>
                  <nts id="Seg_1557" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T423" id="Seg_1559" n="HIAT:u" s="T417">
                  <ts e="T418" id="Seg_1561" n="HIAT:w" s="T417">A</ts>
                  <nts id="Seg_1562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1564" n="HIAT:w" s="T418">ont</ts>
                  <nts id="Seg_1565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1567" n="HIAT:w" s="T419">čagəmbedi</ts>
                  <nts id="Seg_1568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1570" n="HIAT:w" s="T420">peŋqɨn</ts>
                  <nts id="Seg_1571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1573" n="HIAT:w" s="T421">wadʼi</ts>
                  <nts id="Seg_1574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1576" n="HIAT:w" s="T422">aurnɨŋ</ts>
                  <nts id="Seg_1577" n="HIAT:ip">.</nts>
                  <nts id="Seg_1578" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T428" id="Seg_1580" n="HIAT:u" s="T423">
                  <ts e="T424" id="Seg_1582" n="HIAT:w" s="T423">Ont</ts>
                  <nts id="Seg_1583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1585" n="HIAT:w" s="T424">nuːn</ts>
                  <nts id="Seg_1586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1588" n="HIAT:w" s="T425">qojmɨn</ts>
                  <nts id="Seg_1589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1591" n="HIAT:w" s="T426">kelʼlʼe</ts>
                  <nts id="Seg_1592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1594" n="HIAT:w" s="T427">taːdərɨt</ts>
                  <nts id="Seg_1595" n="HIAT:ip">.</nts>
                  <nts id="Seg_1596" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T431" id="Seg_1598" n="HIAT:u" s="T428">
                  <ts e="T429" id="Seg_1600" n="HIAT:w" s="T428">Qampaj</ts>
                  <nts id="Seg_1601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1603" n="HIAT:w" s="T429">tʼeːlat</ts>
                  <nts id="Seg_1604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1606" n="HIAT:w" s="T430">tʼümbocʼka</ts>
                  <nts id="Seg_1607" n="HIAT:ip">.</nts>
                  <nts id="Seg_1608" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T436" id="Seg_1610" n="HIAT:u" s="T431">
                  <ts e="T432" id="Seg_1612" n="HIAT:w" s="T431">Erat</ts>
                  <nts id="Seg_1613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1615" n="HIAT:w" s="T432">maːtqɨn</ts>
                  <nts id="Seg_1616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1618" n="HIAT:w" s="T433">aːmdɨs</ts>
                  <nts id="Seg_1619" n="HIAT:ip">,</nts>
                  <nts id="Seg_1620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1622" n="HIAT:w" s="T434">aːmdɨs</ts>
                  <nts id="Seg_1623" n="HIAT:ip">,</nts>
                  <nts id="Seg_1624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1626" n="HIAT:w" s="T435">kertɨmnɨŋ</ts>
                  <nts id="Seg_1627" n="HIAT:ip">.</nts>
                  <nts id="Seg_1628" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T441" id="Seg_1630" n="HIAT:u" s="T436">
                  <ts e="T437" id="Seg_1632" n="HIAT:w" s="T436">I</ts>
                  <nts id="Seg_1633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1635" n="HIAT:w" s="T437">teːrban</ts>
                  <nts id="Seg_1636" n="HIAT:ip">:</nts>
                  <nts id="Seg_1637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1638" n="HIAT:ip">“</nts>
                  <ts e="T439" id="Seg_1640" n="HIAT:w" s="T438">Man</ts>
                  <nts id="Seg_1641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1643" n="HIAT:w" s="T439">sejlawɨm</ts>
                  <nts id="Seg_1644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1646" n="HIAT:w" s="T440">qwärlʼeu</ts>
                  <nts id="Seg_1647" n="HIAT:ip">.</nts>
                  <nts id="Seg_1648" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T446" id="Seg_1650" n="HIAT:u" s="T441">
                  <ts e="T442" id="Seg_1652" n="HIAT:w" s="T441">Sejla</ts>
                  <nts id="Seg_1653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1655" n="HIAT:w" s="T442">moʒetbɨtʼ</ts>
                  <nts id="Seg_1656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1658" n="HIAT:w" s="T443">meŋga</ts>
                  <nts id="Seg_1659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1661" n="HIAT:w" s="T444">tʼülʼezeq</ts>
                  <nts id="Seg_1662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1663" n="HIAT:ip">(</nts>
                  <ts e="T446" id="Seg_1665" n="HIAT:w" s="T445">tüːlʼezettɨ</ts>
                  <nts id="Seg_1666" n="HIAT:ip">)</nts>
                  <nts id="Seg_1667" n="HIAT:ip">.</nts>
                  <nts id="Seg_1668" n="HIAT:ip">”</nts>
                  <nts id="Seg_1669" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T453" id="Seg_1671" n="HIAT:u" s="T446">
                  <ts e="T447" id="Seg_1673" n="HIAT:w" s="T446">Nu</ts>
                  <nts id="Seg_1674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1676" n="HIAT:w" s="T447">nilʼdʼiŋ</ts>
                  <nts id="Seg_1677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1679" n="HIAT:w" s="T448">i</ts>
                  <nts id="Seg_1680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1682" n="HIAT:w" s="T449">meot</ts>
                  <nts id="Seg_1683" n="HIAT:ip">,</nts>
                  <nts id="Seg_1684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1686" n="HIAT:w" s="T450">sʼäjlamd</ts>
                  <nts id="Seg_1687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1689" n="HIAT:w" s="T451">qwärlʼe</ts>
                  <nts id="Seg_1690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1692" n="HIAT:w" s="T452">üːbərat</ts>
                  <nts id="Seg_1693" n="HIAT:ip">.</nts>
                  <nts id="Seg_1694" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T457" id="Seg_1696" n="HIAT:u" s="T453">
                  <ts e="T454" id="Seg_1698" n="HIAT:w" s="T453">Qajda</ts>
                  <nts id="Seg_1699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1701" n="HIAT:w" s="T454">pʼöcʼin</ts>
                  <nts id="Seg_1702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1704" n="HIAT:w" s="T455">moqɨn</ts>
                  <nts id="Seg_1705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1707" n="HIAT:w" s="T456">raxsɨmban</ts>
                  <nts id="Seg_1708" n="HIAT:ip">.</nts>
                  <nts id="Seg_1709" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T460" id="Seg_1711" n="HIAT:u" s="T457">
                  <ts e="T458" id="Seg_1713" n="HIAT:w" s="T457">Tap</ts>
                  <nts id="Seg_1714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1716" n="HIAT:w" s="T458">soːcʼkaŋ</ts>
                  <nts id="Seg_1717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1719" n="HIAT:w" s="T459">üŋgelǯiŋ</ts>
                  <nts id="Seg_1720" n="HIAT:ip">.</nts>
                  <nts id="Seg_1721" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T465" id="Seg_1723" n="HIAT:u" s="T460">
                  <ts e="T461" id="Seg_1725" n="HIAT:w" s="T460">Ondə</ts>
                  <nts id="Seg_1726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1728" n="HIAT:w" s="T461">tʼeːrban</ts>
                  <nts id="Seg_1729" n="HIAT:ip">:</nts>
                  <nts id="Seg_1730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1731" n="HIAT:ip">“</nts>
                  <ts e="T463" id="Seg_1733" n="HIAT:w" s="T462">Qaj</ts>
                  <nts id="Seg_1734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1736" n="HIAT:w" s="T463">natʼen</ts>
                  <nts id="Seg_1737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1739" n="HIAT:w" s="T464">raqsɨmɨnt</ts>
                  <nts id="Seg_1740" n="HIAT:ip">?</nts>
                  <nts id="Seg_1741" n="HIAT:ip">”</nts>
                  <nts id="Seg_1742" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T471" id="Seg_1744" n="HIAT:u" s="T465">
                  <ts e="T466" id="Seg_1746" n="HIAT:w" s="T465">Tap</ts>
                  <nts id="Seg_1747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1749" n="HIAT:w" s="T466">aj</ts>
                  <nts id="Seg_1750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1752" n="HIAT:w" s="T467">sədɨmǯel</ts>
                  <nts id="Seg_1753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1755" n="HIAT:w" s="T468">tʼärɨŋ</ts>
                  <nts id="Seg_1756" n="HIAT:ip">:</nts>
                  <nts id="Seg_1757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1758" n="HIAT:ip">“</nts>
                  <ts e="T470" id="Seg_1760" n="HIAT:w" s="T469">Sejla</ts>
                  <nts id="Seg_1761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1763" n="HIAT:w" s="T470">roːpeːss</ts>
                  <nts id="Seg_1764" n="HIAT:ip">!</nts>
                  <nts id="Seg_1765" n="HIAT:ip">”</nts>
                  <nts id="Seg_1766" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T477" id="Seg_1768" n="HIAT:u" s="T471">
                  <ts e="T472" id="Seg_1770" n="HIAT:w" s="T471">A</ts>
                  <nts id="Seg_1771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1773" n="HIAT:w" s="T472">pʼöcʼin</ts>
                  <nts id="Seg_1774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1776" n="HIAT:w" s="T473">moqɨn</ts>
                  <nts id="Seg_1777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1779" n="HIAT:w" s="T474">warɣɨŋ</ts>
                  <nts id="Seg_1780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1782" n="HIAT:w" s="T475">raqsonneŋ</ts>
                  <nts id="Seg_1783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1785" n="HIAT:w" s="T476">qajda</ts>
                  <nts id="Seg_1786" n="HIAT:ip">.</nts>
                  <nts id="Seg_1787" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T485" id="Seg_1789" n="HIAT:u" s="T477">
                  <ts e="T478" id="Seg_1791" n="HIAT:w" s="T477">Erat</ts>
                  <nts id="Seg_1792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1794" n="HIAT:w" s="T478">natʼet</ts>
                  <nts id="Seg_1795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1797" n="HIAT:w" s="T479">tʼüːrɨse</ts>
                  <nts id="Seg_1798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1800" n="HIAT:w" s="T480">tʼönǯikaŋ</ts>
                  <nts id="Seg_1801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1803" n="HIAT:w" s="T481">qwannɨŋ</ts>
                  <nts id="Seg_1804" n="HIAT:ip">,</nts>
                  <nts id="Seg_1805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1807" n="HIAT:w" s="T482">pöcʼim</ts>
                  <nts id="Seg_1808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1810" n="HIAT:w" s="T483">moqɨŋ</ts>
                  <nts id="Seg_1811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1813" n="HIAT:w" s="T484">pündarɨmpan</ts>
                  <nts id="Seg_1814" n="HIAT:ip">.</nts>
                  <nts id="Seg_1815" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T487" id="Seg_1817" n="HIAT:u" s="T485">
                  <ts e="T486" id="Seg_1819" n="HIAT:w" s="T485">Qoɨt</ts>
                  <nts id="Seg_1820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1822" n="HIAT:w" s="T486">tɨssam</ts>
                  <nts id="Seg_1823" n="HIAT:ip">.</nts>
                  <nts id="Seg_1824" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T493" id="Seg_1826" n="HIAT:u" s="T487">
                  <ts e="T488" id="Seg_1828" n="HIAT:w" s="T487">Oːmdɨŋ</ts>
                  <nts id="Seg_1829" n="HIAT:ip">,</nts>
                  <nts id="Seg_1830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1832" n="HIAT:w" s="T488">na</ts>
                  <nts id="Seg_1833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1835" n="HIAT:w" s="T489">tɨssɨm</ts>
                  <nts id="Seg_1836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1838" n="HIAT:w" s="T490">tʼiːqɨt</ts>
                  <nts id="Seg_1839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1841" n="HIAT:w" s="T491">i</ts>
                  <nts id="Seg_1842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1844" n="HIAT:w" s="T492">püːɣɨlǯimbat</ts>
                  <nts id="Seg_1845" n="HIAT:ip">.</nts>
                  <nts id="Seg_1846" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T496" id="Seg_1848" n="HIAT:u" s="T493">
                  <ts e="T494" id="Seg_1850" n="HIAT:w" s="T493">Qajda</ts>
                  <nts id="Seg_1851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1853" n="HIAT:w" s="T494">sət</ts>
                  <nts id="Seg_1854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1856" n="HIAT:w" s="T495">püːrlaška</ts>
                  <nts id="Seg_1857" n="HIAT:ip">.</nts>
                  <nts id="Seg_1858" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T500" id="Seg_1860" n="HIAT:u" s="T496">
                  <nts id="Seg_1861" n="HIAT:ip">“</nts>
                  <ts e="T497" id="Seg_1863" n="HIAT:w" s="T496">Tau</ts>
                  <nts id="Seg_1864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1866" n="HIAT:w" s="T497">odnako</ts>
                  <nts id="Seg_1867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1869" n="HIAT:w" s="T498">manani</ts>
                  <nts id="Seg_1870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1872" n="HIAT:w" s="T499">sejla</ts>
                  <nts id="Seg_1873" n="HIAT:ip">.</nts>
                  <nts id="Seg_1874" n="HIAT:ip">”</nts>
                  <nts id="Seg_1875" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T504" id="Seg_1877" n="HIAT:u" s="T500">
                  <ts e="T501" id="Seg_1879" n="HIAT:w" s="T500">Pöti</ts>
                  <nts id="Seg_1880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1882" n="HIAT:w" s="T501">üːdɨm</ts>
                  <nts id="Seg_1883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1885" n="HIAT:w" s="T502">qoɣɨt</ts>
                  <nts id="Seg_1886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1888" n="HIAT:w" s="T503">erat</ts>
                  <nts id="Seg_1889" n="HIAT:ip">.</nts>
                  <nts id="Seg_1890" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T506" id="Seg_1892" n="HIAT:u" s="T504">
                  <ts e="T505" id="Seg_1894" n="HIAT:w" s="T504">Üːttɨ</ts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1897" n="HIAT:w" s="T505">tʼäptäptɨt</ts>
                  <nts id="Seg_1898" n="HIAT:ip">.</nts>
                  <nts id="Seg_1899" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T512" id="Seg_1901" n="HIAT:u" s="T506">
                  <ts e="T507" id="Seg_1903" n="HIAT:w" s="T506">Sejlat</ts>
                  <nts id="Seg_1904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1905" n="HIAT:ip">(</nts>
                  <ts e="T508" id="Seg_1907" n="HIAT:w" s="T507">sət</ts>
                  <nts id="Seg_1908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1910" n="HIAT:w" s="T508">sej</ts>
                  <nts id="Seg_1911" n="HIAT:ip">)</nts>
                  <nts id="Seg_1912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1914" n="HIAT:w" s="T509">kušaŋ</ts>
                  <nts id="Seg_1915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1917" n="HIAT:w" s="T510">üːtqɨn</ts>
                  <nts id="Seg_1918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1920" n="HIAT:w" s="T511">tʼäptädimbɨzattə</ts>
                  <nts id="Seg_1921" n="HIAT:ip">.</nts>
                  <nts id="Seg_1922" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T514" id="Seg_1924" n="HIAT:u" s="T512">
                  <ts e="T513" id="Seg_1926" n="HIAT:w" s="T512">Saʒaŋ</ts>
                  <nts id="Seg_1927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1929" n="HIAT:w" s="T513">müzulǯit</ts>
                  <nts id="Seg_1930" n="HIAT:ip">.</nts>
                  <nts id="Seg_1931" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T518" id="Seg_1933" n="HIAT:u" s="T514">
                  <ts e="T515" id="Seg_1935" n="HIAT:w" s="T514">Sʼäjlaɣɨnd</ts>
                  <nts id="Seg_1936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1938" n="HIAT:w" s="T515">tʼärɨŋ</ts>
                  <nts id="Seg_1939" n="HIAT:ip">:</nts>
                  <nts id="Seg_1940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1941" n="HIAT:ip">“</nts>
                  <ts e="T517" id="Seg_1943" n="HIAT:w" s="T516">Sʼäjla</ts>
                  <nts id="Seg_1944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1946" n="HIAT:w" s="T517">ropeːss</ts>
                  <nts id="Seg_1947" n="HIAT:ip">!</nts>
                  <nts id="Seg_1948" n="HIAT:ip">”</nts>
                  <nts id="Seg_1949" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T521" id="Seg_1951" n="HIAT:u" s="T518">
                  <ts e="T519" id="Seg_1953" n="HIAT:w" s="T518">Sejlat</ts>
                  <nts id="Seg_1954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1956" n="HIAT:w" s="T519">tabɨnä</ts>
                  <nts id="Seg_1957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1959" n="HIAT:w" s="T520">parɨdʼättə</ts>
                  <nts id="Seg_1960" n="HIAT:ip">.</nts>
                  <nts id="Seg_1961" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T527" id="Seg_1963" n="HIAT:u" s="T521">
                  <ts e="T522" id="Seg_1965" n="HIAT:w" s="T521">Manǯedʼiŋ</ts>
                  <nts id="Seg_1966" n="HIAT:ip">,</nts>
                  <nts id="Seg_1967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_1969" n="HIAT:w" s="T522">a</ts>
                  <nts id="Seg_1970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1972" n="HIAT:w" s="T523">sejlat</ts>
                  <nts id="Seg_1973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1975" n="HIAT:w" s="T524">soːčʼkaŋ</ts>
                  <nts id="Seg_1976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1978" n="HIAT:w" s="T525">az</ts>
                  <nts id="Seg_1979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1981" n="HIAT:w" s="T526">aːdattə</ts>
                  <nts id="Seg_1982" n="HIAT:ip">.</nts>
                  <nts id="Seg_1983" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T532" id="Seg_1985" n="HIAT:u" s="T527">
                  <ts e="T528" id="Seg_1987" n="HIAT:w" s="T527">Sejlamd</ts>
                  <nts id="Seg_1988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1990" n="HIAT:w" s="T528">paqqɨnɨt</ts>
                  <nts id="Seg_1991" n="HIAT:ip">,</nts>
                  <nts id="Seg_1992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_1994" n="HIAT:w" s="T529">aj</ts>
                  <nts id="Seg_1995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_1997" n="HIAT:w" s="T530">tʼäptäptɨt</ts>
                  <nts id="Seg_1998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_2000" n="HIAT:w" s="T531">soːcʼkaŋ</ts>
                  <nts id="Seg_2001" n="HIAT:ip">.</nts>
                  <nts id="Seg_2002" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T536" id="Seg_2004" n="HIAT:u" s="T532">
                  <ts e="T533" id="Seg_2006" n="HIAT:w" s="T532">Sʼäjlamt</ts>
                  <nts id="Seg_2007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_2009" n="HIAT:w" s="T533">aj</ts>
                  <nts id="Seg_2010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_2012" n="HIAT:w" s="T534">soːcʼkaŋ</ts>
                  <nts id="Seg_2013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_2015" n="HIAT:w" s="T535">müsennɨt</ts>
                  <nts id="Seg_2016" n="HIAT:ip">.</nts>
                  <nts id="Seg_2017" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T539" id="Seg_2019" n="HIAT:u" s="T536">
                  <ts e="T537" id="Seg_2021" n="HIAT:w" s="T536">Tʼärɨŋ</ts>
                  <nts id="Seg_2022" n="HIAT:ip">:</nts>
                  <nts id="Seg_2023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2024" n="HIAT:ip">“</nts>
                  <ts e="T538" id="Seg_2026" n="HIAT:w" s="T537">Sejla</ts>
                  <nts id="Seg_2027" n="HIAT:ip">,</nts>
                  <nts id="Seg_2028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_2030" n="HIAT:w" s="T538">roːpeːss</ts>
                  <nts id="Seg_2031" n="HIAT:ip">!</nts>
                  <nts id="Seg_2032" n="HIAT:ip">”</nts>
                  <nts id="Seg_2033" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T542" id="Seg_2035" n="HIAT:u" s="T539">
                  <ts e="T540" id="Seg_2037" n="HIAT:w" s="T539">Sejlat</ts>
                  <nts id="Seg_2038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_2040" n="HIAT:w" s="T540">parɨdʼättə</ts>
                  <nts id="Seg_2041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_2043" n="HIAT:w" s="T541">kwesse</ts>
                  <nts id="Seg_2044" n="HIAT:ip">.</nts>
                  <nts id="Seg_2045" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T548" id="Seg_2047" n="HIAT:u" s="T542">
                  <ts e="T543" id="Seg_2049" n="HIAT:w" s="T542">Tibequm</ts>
                  <nts id="Seg_2050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_2052" n="HIAT:w" s="T543">aːndanɨŋ</ts>
                  <nts id="Seg_2053" n="HIAT:ip">:</nts>
                  <nts id="Seg_2054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2055" n="HIAT:ip">“</nts>
                  <ts e="T545" id="Seg_2057" n="HIAT:w" s="T544">Tidam</ts>
                  <nts id="Seg_2058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_2060" n="HIAT:w" s="T545">sejlau</ts>
                  <nts id="Seg_2061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_2063" n="HIAT:w" s="T546">soːcʼkaŋ</ts>
                  <nts id="Seg_2064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_2066" n="HIAT:w" s="T547">konǯernattə</ts>
                  <nts id="Seg_2067" n="HIAT:ip">.</nts>
                  <nts id="Seg_2068" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T552" id="Seg_2070" n="HIAT:u" s="T548">
                  <ts e="T549" id="Seg_2072" n="HIAT:w" s="T548">Tɨdam</ts>
                  <nts id="Seg_2073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_2075" n="HIAT:w" s="T549">man</ts>
                  <nts id="Seg_2076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_2078" n="HIAT:w" s="T550">sejze</ts>
                  <nts id="Seg_2079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2081" n="HIAT:w" s="T551">ezaŋ</ts>
                  <nts id="Seg_2082" n="HIAT:ip">.</nts>
                  <nts id="Seg_2083" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T558" id="Seg_2085" n="HIAT:u" s="T552">
                  <ts e="T553" id="Seg_2087" n="HIAT:w" s="T552">Uʒo</ts>
                  <nts id="Seg_2088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2090" n="HIAT:w" s="T553">man</ts>
                  <nts id="Seg_2091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_2093" n="HIAT:w" s="T554">tast</ts>
                  <nts id="Seg_2094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2096" n="HIAT:w" s="T555">sejlaɣɨn</ts>
                  <nts id="Seg_2097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_2099" n="HIAT:w" s="T556">tʼät</ts>
                  <nts id="Seg_2100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_2102" n="HIAT:w" s="T557">qoːbalaks</ts>
                  <nts id="Seg_2103" n="HIAT:ip">.</nts>
                  <nts id="Seg_2104" n="HIAT:ip">”</nts>
                  <nts id="Seg_2105" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T560" id="Seg_2107" n="HIAT:u" s="T558">
                  <ts e="T559" id="Seg_2109" n="HIAT:w" s="T558">Pone</ts>
                  <nts id="Seg_2110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_2112" n="HIAT:w" s="T559">čaːnǯiŋ</ts>
                  <nts id="Seg_2113" n="HIAT:ip">.</nts>
                  <nts id="Seg_2114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T566" id="Seg_2116" n="HIAT:u" s="T560">
                  <ts e="T561" id="Seg_2118" n="HIAT:w" s="T560">A</ts>
                  <nts id="Seg_2119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2121" n="HIAT:w" s="T561">pajat</ts>
                  <nts id="Seg_2122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_2124" n="HIAT:w" s="T562">kugurgutčiŋ</ts>
                  <nts id="Seg_2125" n="HIAT:ip">,</nts>
                  <nts id="Seg_2126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2128" n="HIAT:w" s="T563">nu</ts>
                  <nts id="Seg_2129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2131" n="HIAT:w" s="T564">kojmɨm</ts>
                  <nts id="Seg_2132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_2134" n="HIAT:w" s="T565">taːdərɨt</ts>
                  <nts id="Seg_2135" n="HIAT:ip">.</nts>
                  <nts id="Seg_2136" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T574" id="Seg_2138" n="HIAT:u" s="T566">
                  <ts e="T567" id="Seg_2140" n="HIAT:w" s="T566">Erat</ts>
                  <nts id="Seg_2141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_2143" n="HIAT:w" s="T567">tʼärɨŋ</ts>
                  <nts id="Seg_2144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_2146" n="HIAT:w" s="T568">pajandɨne</ts>
                  <nts id="Seg_2147" n="HIAT:ip">:</nts>
                  <nts id="Seg_2148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2149" n="HIAT:ip">“</nts>
                  <ts e="T570" id="Seg_2151" n="HIAT:w" s="T569">A</ts>
                  <nts id="Seg_2152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2154" n="HIAT:w" s="T570">tan</ts>
                  <nts id="Seg_2155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2157" n="HIAT:w" s="T571">mazɨm</ts>
                  <nts id="Seg_2158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_2160" n="HIAT:w" s="T572">qajno</ts>
                  <nts id="Seg_2161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2163" n="HIAT:w" s="T573">sejgɨlǯant</ts>
                  <nts id="Seg_2164" n="HIAT:ip">?</nts>
                  <nts id="Seg_2165" n="HIAT:ip">”</nts>
                  <nts id="Seg_2166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T588" id="Seg_2168" n="HIAT:u" s="T574">
                  <ts e="T575" id="Seg_2170" n="HIAT:w" s="T574">A</ts>
                  <nts id="Seg_2171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2173" n="HIAT:w" s="T575">pajat</ts>
                  <nts id="Seg_2174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2176" n="HIAT:w" s="T576">tʼärɨŋ</ts>
                  <nts id="Seg_2177" n="HIAT:ip">:</nts>
                  <nts id="Seg_2178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2179" n="HIAT:ip">“</nts>
                  <ts e="T578" id="Seg_2181" n="HIAT:w" s="T577">A</ts>
                  <nts id="Seg_2182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2184" n="HIAT:w" s="T578">tan</ts>
                  <nts id="Seg_2185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2187" n="HIAT:w" s="T579">qajno</ts>
                  <nts id="Seg_2188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2190" n="HIAT:w" s="T580">nilʼdʼiŋ</ts>
                  <nts id="Seg_2191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2193" n="HIAT:w" s="T581">palʼdʼikuzant</ts>
                  <nts id="Seg_2194" n="HIAT:ip">,</nts>
                  <nts id="Seg_2195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2197" n="HIAT:w" s="T582">köcʼkuzandə</ts>
                  <nts id="Seg_2198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2200" n="HIAT:w" s="T583">suːrusku</ts>
                  <nts id="Seg_2201" n="HIAT:ip">,</nts>
                  <nts id="Seg_2202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2204" n="HIAT:w" s="T584">a</ts>
                  <nts id="Seg_2205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2207" n="HIAT:w" s="T585">onent</ts>
                  <nts id="Seg_2208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2210" n="HIAT:w" s="T586">sejlandɨse</ts>
                  <nts id="Seg_2211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2213" n="HIAT:w" s="T587">sanǯirsant</ts>
                  <nts id="Seg_2214" n="HIAT:ip">?</nts>
                  <nts id="Seg_2215" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T593" id="Seg_2217" n="HIAT:u" s="T588">
                  <ts e="T589" id="Seg_2219" n="HIAT:w" s="T588">A</ts>
                  <nts id="Seg_2220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2222" n="HIAT:w" s="T589">maːtt</ts>
                  <nts id="Seg_2223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2225" n="HIAT:w" s="T590">qaimnej</ts>
                  <nts id="Seg_2226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2228" n="HIAT:w" s="T591">as</ts>
                  <nts id="Seg_2229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2231" n="HIAT:w" s="T592">taːtkuzal</ts>
                  <nts id="Seg_2232" n="HIAT:ip">.</nts>
                  <nts id="Seg_2233" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T601" id="Seg_2235" n="HIAT:u" s="T593">
                  <ts e="T594" id="Seg_2237" n="HIAT:w" s="T593">Wot</ts>
                  <nts id="Seg_2238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2240" n="HIAT:w" s="T594">man</ts>
                  <nts id="Seg_2241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2243" n="HIAT:w" s="T595">tannani</ts>
                  <nts id="Seg_2244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2246" n="HIAT:w" s="T596">sejlamt</ts>
                  <nts id="Seg_2247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2249" n="HIAT:w" s="T597">nano</ts>
                  <nts id="Seg_2250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2252" n="HIAT:w" s="T598">i</ts>
                  <nts id="Seg_2253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2254" n="HIAT:ip">(</nts>
                  <ts e="T600" id="Seg_2256" n="HIAT:w" s="T599">paqqɨnnau</ts>
                  <nts id="Seg_2257" n="HIAT:ip">)</nts>
                  <nts id="Seg_2258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2260" n="HIAT:w" s="T600">tannau</ts>
                  <nts id="Seg_2261" n="HIAT:ip">.</nts>
                  <nts id="Seg_2262" n="HIAT:ip">”</nts>
                  <nts id="Seg_2263" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T601" id="Seg_2264" n="sc" s="T1">
               <ts e="T2" id="Seg_2266" n="e" s="T1">Ilɨzaq </ts>
               <ts e="T3" id="Seg_2268" n="e" s="T2">warkɨzaq </ts>
               <ts e="T4" id="Seg_2270" n="e" s="T3">era </ts>
               <ts e="T5" id="Seg_2272" n="e" s="T4">pajasak. </ts>
               <ts e="T6" id="Seg_2274" n="e" s="T5">Erat </ts>
               <ts e="T7" id="Seg_2276" n="e" s="T6">palʼdʼukuŋ </ts>
               <ts e="T8" id="Seg_2278" n="e" s="T7">suːrɨlʼe. </ts>
               <ts e="T9" id="Seg_2280" n="e" s="T8">Pajat </ts>
               <ts e="T10" id="Seg_2282" n="e" s="T9">maːtqɨn </ts>
               <ts e="T11" id="Seg_2284" n="e" s="T10">jewan. </ts>
               <ts e="T12" id="Seg_2286" n="e" s="T11">Erat </ts>
               <ts e="T13" id="Seg_2288" n="e" s="T12">palʼdʼikuŋ, </ts>
               <ts e="T14" id="Seg_2290" n="e" s="T13">palʼdʼikuŋ, </ts>
               <ts e="T15" id="Seg_2292" n="e" s="T14">tʼeːlʼdʼömp </ts>
               <ts e="T16" id="Seg_2294" n="e" s="T15">palʼdʼikuŋ. </ts>
               <ts e="T17" id="Seg_2296" n="e" s="T16">Qaimnäj </ts>
               <ts e="T18" id="Seg_2298" n="e" s="T17">ass </ts>
               <ts e="T19" id="Seg_2300" n="e" s="T18">tatkut. </ts>
               <ts e="T20" id="Seg_2302" n="e" s="T19">Kɨdaŋ </ts>
               <ts e="T21" id="Seg_2304" n="e" s="T20">niŋ </ts>
               <ts e="T22" id="Seg_2306" n="e" s="T21">palʼdʼikuŋ, </ts>
               <ts e="T23" id="Seg_2308" n="e" s="T22">qaimne </ts>
               <ts e="T24" id="Seg_2310" n="e" s="T23">as </ts>
               <ts e="T25" id="Seg_2312" n="e" s="T24">tatkut. </ts>
               <ts e="T26" id="Seg_2314" n="e" s="T25">Okkɨr </ts>
               <ts e="T27" id="Seg_2316" n="e" s="T26">dʼel </ts>
               <ts e="T28" id="Seg_2318" n="e" s="T27">pajat </ts>
               <ts e="T29" id="Seg_2320" n="e" s="T28">teːrban: </ts>
               <ts e="T30" id="Seg_2322" n="e" s="T29">Qajno </ts>
               <ts e="T31" id="Seg_2324" n="e" s="T30">ʒə </ts>
               <ts e="T32" id="Seg_2326" n="e" s="T31">tap </ts>
               <ts e="T33" id="Seg_2328" n="e" s="T32">qaimnäj </ts>
               <ts e="T34" id="Seg_2330" n="e" s="T33">as </ts>
               <ts e="T35" id="Seg_2332" n="e" s="T34">tatkut? </ts>
               <ts e="T36" id="Seg_2334" n="e" s="T35">Man </ts>
               <ts e="T37" id="Seg_2336" n="e" s="T36">tabɨm </ts>
               <ts e="T38" id="Seg_2338" n="e" s="T37">saralǯlʼew. </ts>
               <ts e="T39" id="Seg_2340" n="e" s="T38">Qajno </ts>
               <ts e="T40" id="Seg_2342" n="e" s="T39">tap </ts>
               <ts e="T41" id="Seg_2344" n="e" s="T40">sündebɨŋ </ts>
               <ts e="T42" id="Seg_2346" n="e" s="T41">tükkuŋ?” </ts>
               <ts e="T43" id="Seg_2348" n="e" s="T42">Nu </ts>
               <ts e="T44" id="Seg_2350" n="e" s="T43">i </ts>
               <ts e="T45" id="Seg_2352" n="e" s="T44">nilʼdʼiŋ </ts>
               <ts e="T46" id="Seg_2354" n="e" s="T45">meːwɨt. </ts>
               <ts e="T47" id="Seg_2356" n="e" s="T46">Erat </ts>
               <ts e="T48" id="Seg_2358" n="e" s="T47">üːbɨcʼlʼe </ts>
               <ts e="T49" id="Seg_2360" n="e" s="T48">üːbɨraŋ </ts>
               <ts e="T50" id="Seg_2362" n="e" s="T49">suːrulʼe, </ts>
               <ts e="T51" id="Seg_2364" n="e" s="T50">qwannɨŋ. </ts>
               <ts e="T52" id="Seg_2366" n="e" s="T51">Pajat </ts>
               <ts e="T53" id="Seg_2368" n="e" s="T52">tʼäːno </ts>
               <ts e="T54" id="Seg_2370" n="e" s="T53">tolʼdʼimt </ts>
               <ts e="T55" id="Seg_2372" n="e" s="T54">sernat. </ts>
               <ts e="T56" id="Seg_2374" n="e" s="T55">Erand </ts>
               <ts e="T57" id="Seg_2376" n="e" s="T56">moqɨn </ts>
               <ts e="T58" id="Seg_2378" n="e" s="T57">tʼäno </ts>
               <ts e="T59" id="Seg_2380" n="e" s="T58">üːban. </ts>
               <ts e="T60" id="Seg_2382" n="e" s="T59">Erandɨne </ts>
               <ts e="T61" id="Seg_2384" n="e" s="T60">az </ts>
               <ts e="T62" id="Seg_2386" n="e" s="T61">aːdolǯikuŋ. </ts>
               <ts e="T63" id="Seg_2388" n="e" s="T62">Nano </ts>
               <ts e="T64" id="Seg_2390" n="e" s="T63">az </ts>
               <ts e="T65" id="Seg_2392" n="e" s="T64">aːdolǯikuŋ, </ts>
               <ts e="T66" id="Seg_2394" n="e" s="T65">tabɨm </ts>
               <ts e="T67" id="Seg_2396" n="e" s="T66">erat </ts>
               <ts e="T68" id="Seg_2398" n="e" s="T67">ik </ts>
               <ts e="T69" id="Seg_2400" n="e" s="T68">qonǯirnent. </ts>
               <ts e="T70" id="Seg_2402" n="e" s="T69">Erat </ts>
               <ts e="T71" id="Seg_2404" n="e" s="T70">medɨŋ </ts>
               <ts e="T72" id="Seg_2406" n="e" s="T71">Qɨn </ts>
               <ts e="T73" id="Seg_2408" n="e" s="T72">qɨnbart. </ts>
               <ts e="T74" id="Seg_2410" n="e" s="T73">Tam </ts>
               <ts e="T75" id="Seg_2412" n="e" s="T74">plʼekaj </ts>
               <ts e="T76" id="Seg_2414" n="e" s="T75">qän </ts>
               <ts e="T77" id="Seg_2416" n="e" s="T76">paːrɣɨn </ts>
               <ts e="T78" id="Seg_2418" n="e" s="T77">ont </ts>
               <ts e="T79" id="Seg_2420" n="e" s="T78">sʼäimd </ts>
               <ts e="T80" id="Seg_2422" n="e" s="T79">paqqɨlgut. </ts>
               <ts e="T81" id="Seg_2424" n="e" s="T80">Sʼäilamd </ts>
               <ts e="T82" id="Seg_2426" n="e" s="T81">ädɨlʼe </ts>
               <ts e="T83" id="Seg_2428" n="e" s="T82">qwäčikut </ts>
               <ts e="T84" id="Seg_2430" n="e" s="T83">pon </ts>
               <ts e="T85" id="Seg_2432" n="e" s="T84">molaɣɨn. </ts>
               <ts e="T86" id="Seg_2434" n="e" s="T85">Taj </ts>
               <ts e="T87" id="Seg_2436" n="e" s="T86">plʼekaj </ts>
               <ts e="T88" id="Seg_2438" n="e" s="T87">qänt </ts>
               <ts e="T89" id="Seg_2440" n="e" s="T88">nʼäzolguŋ. </ts>
               <ts e="T90" id="Seg_2442" n="e" s="T89">Ondə </ts>
               <ts e="T91" id="Seg_2444" n="e" s="T90">tʼärkuŋ: </ts>
               <ts e="T92" id="Seg_2446" n="e" s="T91">“Sʼäj, </ts>
               <ts e="T93" id="Seg_2448" n="e" s="T92">ropeːs.” </ts>
               <ts e="T94" id="Seg_2450" n="e" s="T93">Sʼäjla </ts>
               <ts e="T95" id="Seg_2452" n="e" s="T94">tabnä </ts>
               <ts e="T96" id="Seg_2454" n="e" s="T95">tʼüqwatt, </ts>
               <ts e="T97" id="Seg_2456" n="e" s="T96">paːrɨčiqwattə. </ts>
               <ts e="T98" id="Seg_2458" n="e" s="T97">Aj </ts>
               <ts e="T99" id="Seg_2460" n="e" s="T98">natʼej </ts>
               <ts e="T100" id="Seg_2462" n="e" s="T99">qɨn </ts>
               <ts e="T101" id="Seg_2464" n="e" s="T100">paːroɣɨn </ts>
               <ts e="T102" id="Seg_2466" n="e" s="T101">seint </ts>
               <ts e="T103" id="Seg_2468" n="e" s="T102">paqqɨlʼe </ts>
               <ts e="T104" id="Seg_2470" n="e" s="T103">qwecʼikut, </ts>
               <ts e="T105" id="Seg_2472" n="e" s="T104">pon </ts>
               <ts e="T106" id="Seg_2474" n="e" s="T105">moːland </ts>
               <ts e="T107" id="Seg_2476" n="e" s="T106">ɨtkut. </ts>
               <ts e="T108" id="Seg_2478" n="e" s="T107">Tʼärkuŋ: </ts>
               <ts e="T109" id="Seg_2480" n="e" s="T108">sʼäjla </ts>
               <ts e="T110" id="Seg_2482" n="e" s="T109">roːpeːss! </ts>
               <ts e="T111" id="Seg_2484" n="e" s="T110">Sʼäjla </ts>
               <ts e="T112" id="Seg_2486" n="e" s="T111">tabne </ts>
               <ts e="T113" id="Seg_2488" n="e" s="T112">tʼülʼe </ts>
               <ts e="T114" id="Seg_2490" n="e" s="T113">rokecʼqwatt. </ts>
               <ts e="T116" id="Seg_2492" n="e" s="T114">Nilʼdʼäredaŋ </ts>
               <ts e="T117" id="Seg_2494" n="e" s="T116">selɨj </ts>
               <ts e="T118" id="Seg_2496" n="e" s="T117">tʼeːldʼʒom </ts>
               <ts e="T119" id="Seg_2498" n="e" s="T118">pünnɨŋ. </ts>
               <ts e="T120" id="Seg_2500" n="e" s="T119">Pajat </ts>
               <ts e="T121" id="Seg_2502" n="e" s="T120">mannɨmpɨs, </ts>
               <ts e="T122" id="Seg_2504" n="e" s="T121">mannɨmpɨs </ts>
               <ts e="T123" id="Seg_2506" n="e" s="T122">toːlakaŋ. </ts>
               <ts e="T124" id="Seg_2508" n="e" s="T123">Erat </ts>
               <ts e="T125" id="Seg_2510" n="e" s="T124">seːimd </ts>
               <ts e="T126" id="Seg_2512" n="e" s="T125">paqqənnɨt. </ts>
               <ts e="T127" id="Seg_2514" n="e" s="T126">Tam </ts>
               <ts e="T128" id="Seg_2516" n="e" s="T127">plʼekaj </ts>
               <ts e="T129" id="Seg_2518" n="e" s="T128">qän </ts>
               <ts e="T130" id="Seg_2520" n="e" s="T129">känbart </ts>
               <ts e="T131" id="Seg_2522" n="e" s="T130">qwädʼät. </ts>
               <ts e="T132" id="Seg_2524" n="e" s="T131">A </ts>
               <ts e="T133" id="Seg_2526" n="e" s="T132">pajat </ts>
               <ts e="T134" id="Seg_2528" n="e" s="T133">tʼäːno </ts>
               <ts e="T135" id="Seg_2530" n="e" s="T134">tabɨn </ts>
               <ts e="T136" id="Seg_2532" n="e" s="T135">sejlamd </ts>
               <ts e="T137" id="Seg_2534" n="e" s="T136">iːot </ts>
               <ts e="T138" id="Seg_2536" n="e" s="T137">toːlakaŋ. </ts>
               <ts e="T139" id="Seg_2538" n="e" s="T138">Ontə </ts>
               <ts e="T140" id="Seg_2540" n="e" s="T139">teːrpan: </ts>
               <ts e="T141" id="Seg_2542" n="e" s="T140">“A </ts>
               <ts e="T142" id="Seg_2544" n="e" s="T141">tan </ts>
               <ts e="T143" id="Seg_2546" n="e" s="T142">nano </ts>
               <ts e="T144" id="Seg_2548" n="e" s="T143">maːttə </ts>
               <ts e="T145" id="Seg_2550" n="e" s="T144">qaimnej </ts>
               <ts e="T146" id="Seg_2552" n="e" s="T145">as </ts>
               <ts e="T147" id="Seg_2554" n="e" s="T146">tatqwal </ts>
               <ts e="T148" id="Seg_2556" n="e" s="T147">meŋga.” </ts>
               <ts e="T149" id="Seg_2558" n="e" s="T148">Nilʼdʼiŋ </ts>
               <ts e="T150" id="Seg_2560" n="e" s="T149">pajat </ts>
               <ts e="T151" id="Seg_2562" n="e" s="T150">qwannɨŋ, </ts>
               <ts e="T152" id="Seg_2564" n="e" s="T151">qwannɨŋ </ts>
               <ts e="T153" id="Seg_2566" n="e" s="T152">i </ts>
               <ts e="T154" id="Seg_2568" n="e" s="T153">tʼönǯikaŋ </ts>
               <ts e="T155" id="Seg_2570" n="e" s="T154">čaːǯiŋ. </ts>
               <ts e="T156" id="Seg_2572" n="e" s="T155">Erantɨnan </ts>
               <ts e="T157" id="Seg_2574" n="e" s="T156">seiːt </ts>
               <ts e="T158" id="Seg_2576" n="e" s="T157">tidam </ts>
               <ts e="T159" id="Seg_2578" n="e" s="T158">tʼäŋwan. </ts>
               <ts e="T160" id="Seg_2580" n="e" s="T159">Qwäcʼku </ts>
               <ts e="T161" id="Seg_2582" n="e" s="T160">tabɨm </ts>
               <ts e="T162" id="Seg_2584" n="e" s="T161">tab </ts>
               <ts e="T163" id="Seg_2586" n="e" s="T162">ürgenǯiŋ. </ts>
               <ts e="T164" id="Seg_2588" n="e" s="T163">Nano </ts>
               <ts e="T165" id="Seg_2590" n="e" s="T164">i </ts>
               <ts e="T166" id="Seg_2592" n="e" s="T165">tʼönǯikaŋ </ts>
               <ts e="T167" id="Seg_2594" n="e" s="T166">čaːǯiŋ </ts>
               <ts e="T168" id="Seg_2596" n="e" s="T167">pajat. </ts>
               <ts e="T169" id="Seg_2598" n="e" s="T168">Kušaŋ </ts>
               <ts e="T170" id="Seg_2600" n="e" s="T169">čaǯiŋ, </ts>
               <ts e="T171" id="Seg_2602" n="e" s="T170">aj </ts>
               <ts e="T172" id="Seg_2604" n="e" s="T171">moɣunä </ts>
               <ts e="T173" id="Seg_2606" n="e" s="T172">manǯedʼikuŋ, </ts>
               <ts e="T174" id="Seg_2608" n="e" s="T173">erat </ts>
               <ts e="T175" id="Seg_2610" n="e" s="T174">čaːǯiŋ </ts>
               <ts e="T176" id="Seg_2612" n="e" s="T175">alʼi </ts>
               <ts e="T177" id="Seg_2614" n="e" s="T176">ass. </ts>
               <ts e="T178" id="Seg_2616" n="e" s="T177">Pajant </ts>
               <ts e="T179" id="Seg_2618" n="e" s="T178">qwalʼewlʼe </ts>
               <ts e="T180" id="Seg_2620" n="e" s="T179">erat </ts>
               <ts e="T181" id="Seg_2622" n="e" s="T180">kušaŋ </ts>
               <ts e="T182" id="Seg_2624" n="e" s="T181">laŋgɨniŋ: </ts>
               <ts e="T183" id="Seg_2626" n="e" s="T182">“Sʼäjla, </ts>
               <ts e="T184" id="Seg_2628" n="e" s="T183">ropeːss!” </ts>
               <ts e="T185" id="Seg_2630" n="e" s="T184">Sʼäjla </ts>
               <ts e="T186" id="Seg_2632" n="e" s="T185">tʼäŋgwattə, </ts>
               <ts e="T187" id="Seg_2634" n="e" s="T186">as </ts>
               <ts e="T188" id="Seg_2636" n="e" s="T187">tükwatte </ts>
               <ts e="T189" id="Seg_2638" n="e" s="T188">tabnä </ts>
               <ts e="T190" id="Seg_2640" n="e" s="T189">i </ts>
               <ts e="T191" id="Seg_2642" n="e" s="T190">ass </ts>
               <ts e="T192" id="Seg_2644" n="e" s="T191">parɨdʼiqwattə. </ts>
               <ts e="T193" id="Seg_2646" n="e" s="T192">Kušaŋ </ts>
               <ts e="T194" id="Seg_2648" n="e" s="T193">sʼäjlamt </ts>
               <ts e="T195" id="Seg_2650" n="e" s="T194">qwärɨstə, </ts>
               <ts e="T196" id="Seg_2652" n="e" s="T195">qwärɨst, </ts>
               <ts e="T197" id="Seg_2654" n="e" s="T196">sʼäila </ts>
               <ts e="T198" id="Seg_2656" n="e" s="T197">kundar </ts>
               <ts e="T199" id="Seg_2658" n="e" s="T198">tʼäŋgwattə, </ts>
               <ts e="T200" id="Seg_2660" n="e" s="T199">niŋ </ts>
               <ts e="T201" id="Seg_2662" n="e" s="T200">i </ts>
               <ts e="T202" id="Seg_2664" n="e" s="T201">tʼäŋgwatt. </ts>
               <ts e="T203" id="Seg_2666" n="e" s="T202">Tidam </ts>
               <ts e="T204" id="Seg_2668" n="e" s="T203">sejgalɨk </ts>
               <ts e="T205" id="Seg_2670" n="e" s="T204">erat </ts>
               <ts e="T206" id="Seg_2672" n="e" s="T205">na </ts>
               <ts e="T207" id="Seg_2674" n="e" s="T206">üːbedʼän, </ts>
               <ts e="T208" id="Seg_2676" n="e" s="T207">sʼäjgalɨk. </ts>
               <ts e="T209" id="Seg_2678" n="e" s="T208">Pajat </ts>
               <ts e="T210" id="Seg_2680" n="e" s="T209">aːdelǯiŋ, </ts>
               <ts e="T211" id="Seg_2682" n="e" s="T210">erandɨne </ts>
               <ts e="T212" id="Seg_2684" n="e" s="T211">az </ts>
               <ts e="T213" id="Seg_2686" n="e" s="T212">aːdelǯikuŋ. </ts>
               <ts e="T214" id="Seg_2688" n="e" s="T213">Erat </ts>
               <ts e="T215" id="Seg_2690" n="e" s="T214">sejgalɨk </ts>
               <ts e="T216" id="Seg_2692" n="e" s="T215">ürgɨle </ts>
               <ts e="T217" id="Seg_2694" n="e" s="T216">qunǯiŋ. </ts>
               <ts e="T218" id="Seg_2696" n="e" s="T217">Madɨn </ts>
               <ts e="T219" id="Seg_2698" n="e" s="T218">köndə </ts>
               <ts e="T220" id="Seg_2700" n="e" s="T219">miglʼe </ts>
               <ts e="T221" id="Seg_2702" n="e" s="T220">üːbəraq. </ts>
               <ts e="T222" id="Seg_2704" n="e" s="T221">Pajat </ts>
               <ts e="T223" id="Seg_2706" n="e" s="T222">tɨdəmban. </ts>
               <ts e="T224" id="Seg_2708" n="e" s="T223">Tʼäːno </ts>
               <ts e="T225" id="Seg_2710" n="e" s="T224">medɨdʼeŋ, </ts>
               <ts e="T226" id="Seg_2712" n="e" s="T225">tolʼdʼilamd </ts>
               <ts e="T0" id="Seg_2714" n="e" s="T226">niŋ </ts>
               <ts e="T227" id="Seg_2716" n="e" s="T0">el </ts>
               <ts e="T228" id="Seg_2718" n="e" s="T227">čaɣannɨt. </ts>
               <ts e="T229" id="Seg_2720" n="e" s="T228">Mattə </ts>
               <ts e="T230" id="Seg_2722" n="e" s="T229">seːrnɨŋ, </ts>
               <ts e="T231" id="Seg_2724" n="e" s="T230">nʼäɣɨniŋ. </ts>
               <ts e="T232" id="Seg_2726" n="e" s="T231">Erant </ts>
               <ts e="T233" id="Seg_2728" n="e" s="T232">sejlam </ts>
               <ts e="T234" id="Seg_2730" n="e" s="T233">tɨsont </ts>
               <ts e="T235" id="Seg_2732" n="e" s="T234">pennɨt, </ts>
               <ts e="T236" id="Seg_2734" n="e" s="T235">pʼötʼen </ts>
               <ts e="T237" id="Seg_2736" n="e" s="T236">moqtə </ts>
               <ts e="T238" id="Seg_2738" n="e" s="T237">oːcʼiŋnɨt. </ts>
               <ts e="T239" id="Seg_2740" n="e" s="T238">Erat </ts>
               <ts e="T240" id="Seg_2742" n="e" s="T239">tʼönǯikaŋ </ts>
               <ts e="T241" id="Seg_2744" n="e" s="T240">tüan </ts>
               <ts e="T242" id="Seg_2746" n="e" s="T241">maːttə, </ts>
               <ts e="T243" id="Seg_2748" n="e" s="T242">sernaŋ. </ts>
               <ts e="T244" id="Seg_2750" n="e" s="T243">Pajat </ts>
               <ts e="T245" id="Seg_2752" n="e" s="T244">tʼärɨŋ: </ts>
               <ts e="T246" id="Seg_2754" n="e" s="T245">“A </ts>
               <ts e="T247" id="Seg_2756" n="e" s="T246">tan </ts>
               <ts e="T248" id="Seg_2758" n="e" s="T247">qajno </ts>
               <ts e="T249" id="Seg_2760" n="e" s="T248">tamdʼel </ts>
               <ts e="T250" id="Seg_2762" n="e" s="T249">kundɨŋ </ts>
               <ts e="T251" id="Seg_2764" n="e" s="T250">jewant?” </ts>
               <ts e="T252" id="Seg_2766" n="e" s="T251">Erat </ts>
               <ts e="T253" id="Seg_2768" n="e" s="T252">tʼärɨŋ: </ts>
               <ts e="T254" id="Seg_2770" n="e" s="T253">“Man </ts>
               <ts e="T255" id="Seg_2772" n="e" s="T254">tamdʼel </ts>
               <ts e="T256" id="Seg_2774" n="e" s="T255">sejowɨm </ts>
               <ts e="T257" id="Seg_2776" n="e" s="T256">aːtsaŋ, </ts>
               <ts e="T258" id="Seg_2778" n="e" s="T257">nano </ts>
               <ts e="T259" id="Seg_2780" n="e" s="T258">kundɨŋ </ts>
               <ts e="T260" id="Seg_2782" n="e" s="T259">jewaŋ.” </ts>
               <ts e="T261" id="Seg_2784" n="e" s="T260">Pajat </ts>
               <ts e="T262" id="Seg_2786" n="e" s="T261">teːrban: </ts>
               <ts e="T263" id="Seg_2788" n="e" s="T262">“A </ts>
               <ts e="T264" id="Seg_2790" n="e" s="T263">tan </ts>
               <ts e="T265" id="Seg_2792" n="e" s="T264">siːrant, </ts>
               <ts e="T266" id="Seg_2794" n="e" s="T265">man </ts>
               <ts e="T267" id="Seg_2796" n="e" s="T266">tidam </ts>
               <ts e="T268" id="Seg_2798" n="e" s="T267">tastɨ </ts>
               <ts e="T269" id="Seg_2800" n="e" s="T268">tunuaŋ, </ts>
               <ts e="T270" id="Seg_2802" n="e" s="T269">kundar </ts>
               <ts e="T271" id="Seg_2804" n="e" s="T270">tan </ts>
               <ts e="T272" id="Seg_2806" n="e" s="T271">suːrunʼänt. </ts>
               <ts e="T273" id="Seg_2808" n="e" s="T272">Selɨj </ts>
               <ts e="T274" id="Seg_2810" n="e" s="T273">tʼelʼdʼzʼomp </ts>
               <ts e="T275" id="Seg_2812" n="e" s="T274">sejlandɨse </ts>
               <ts e="T276" id="Seg_2814" n="e" s="T275">warkand. </ts>
               <ts e="T277" id="Seg_2816" n="e" s="T276">Aw </ts>
               <ts e="T278" id="Seg_2818" n="e" s="T277">qɨn </ts>
               <ts e="T279" id="Seg_2820" n="e" s="T278">baːrt </ts>
               <ts e="T280" id="Seg_2822" n="e" s="T279">ɨtqwäl, </ts>
               <ts e="T281" id="Seg_2824" n="e" s="T280">a </ts>
               <ts e="T282" id="Seg_2826" n="e" s="T281">tam </ts>
               <ts e="T283" id="Seg_2828" n="e" s="T282">plʼekaj </ts>
               <ts e="T284" id="Seg_2830" n="e" s="T283">kɨn </ts>
               <ts e="T285" id="Seg_2832" n="e" s="T284">baːrt </ts>
               <ts e="T286" id="Seg_2834" n="e" s="T285">neːzolgwant. </ts>
               <ts e="T287" id="Seg_2836" n="e" s="T286">Sejlal </ts>
               <ts e="T288" id="Seg_2838" n="e" s="T287">tʼeŋga </ts>
               <ts e="T289" id="Seg_2840" n="e" s="T288">tʼülʼe </ts>
               <ts e="T290" id="Seg_2842" n="e" s="T289">parɨcʼikwattə. </ts>
               <ts e="T291" id="Seg_2844" n="e" s="T290">Nilʼdʼiŋ </ts>
               <ts e="T292" id="Seg_2846" n="e" s="T291">tan </ts>
               <ts e="T293" id="Seg_2848" n="e" s="T292">i </ts>
               <ts e="T294" id="Seg_2850" n="e" s="T293">sündebɨŋ </ts>
               <ts e="T295" id="Seg_2852" n="e" s="T294">tükkuzant.” </ts>
               <ts e="T296" id="Seg_2854" n="e" s="T295">Nu </ts>
               <ts e="T297" id="Seg_2856" n="e" s="T296">i </ts>
               <ts e="T298" id="Seg_2858" n="e" s="T297">warkɨlʼe </ts>
               <ts e="T299" id="Seg_2860" n="e" s="T298">üːbəraq. </ts>
               <ts e="T300" id="Seg_2862" n="e" s="T299">Kušaj </ts>
               <ts e="T301" id="Seg_2864" n="e" s="T300">da </ts>
               <ts e="T302" id="Seg_2866" n="e" s="T301">dʼeːlɨt </ts>
               <ts e="T303" id="Seg_2868" n="e" s="T302">mendɨŋ. </ts>
               <ts e="T304" id="Seg_2870" n="e" s="T303">Pajat </ts>
               <ts e="T305" id="Seg_2872" n="e" s="T304">manǯediŋ </ts>
               <ts e="T306" id="Seg_2874" n="e" s="T305">südernaun, </ts>
               <ts e="T307" id="Seg_2876" n="e" s="T306">manǯediŋ, </ts>
               <ts e="T308" id="Seg_2878" n="e" s="T307">nɨŋan </ts>
               <ts e="T309" id="Seg_2880" n="e" s="T308">peŋk. </ts>
               <ts e="T310" id="Seg_2882" n="e" s="T309">Pajat </ts>
               <ts e="T311" id="Seg_2884" n="e" s="T310">erandɨne </ts>
               <ts e="T312" id="Seg_2886" n="e" s="T311">tʼärɨŋ: </ts>
               <ts e="T313" id="Seg_2888" n="e" s="T312">“Peŋkə </ts>
               <ts e="T314" id="Seg_2890" n="e" s="T313">nɨŋgɨnt. </ts>
               <ts e="T315" id="Seg_2892" n="e" s="T314">Tan </ts>
               <ts e="T316" id="Seg_2894" n="e" s="T315">wazlʼent </ts>
               <ts e="T317" id="Seg_2896" n="e" s="T316">kundarem </ts>
               <ts e="T318" id="Seg_2898" n="e" s="T317">tʼäʒlʼel </ts>
               <ts e="T319" id="Seg_2900" n="e" s="T318">peŋkəm.” </ts>
               <ts e="T320" id="Seg_2902" n="e" s="T319">A </ts>
               <ts e="T321" id="Seg_2904" n="e" s="T320">erat </ts>
               <ts e="T322" id="Seg_2906" n="e" s="T321">tʼärɨŋ: </ts>
               <ts e="T323" id="Seg_2908" n="e" s="T322">“Man </ts>
               <ts e="T324" id="Seg_2910" n="e" s="T323">kundar </ts>
               <ts e="T325" id="Seg_2912" n="e" s="T324">tʼäǯenǯau? </ts>
               <ts e="T326" id="Seg_2914" n="e" s="T325">Sejew </ts>
               <ts e="T327" id="Seg_2916" n="e" s="T326">az </ts>
               <ts e="T328" id="Seg_2918" n="e" s="T327">aːduŋ.” </ts>
               <ts e="T329" id="Seg_2920" n="e" s="T328">Pajat </ts>
               <ts e="T330" id="Seg_2922" n="e" s="T329">tʼärɨŋ: </ts>
               <ts e="T331" id="Seg_2924" n="e" s="T330">“Tüqɨ </ts>
               <ts e="T332" id="Seg_2926" n="e" s="T331">meŋga, </ts>
               <ts e="T333" id="Seg_2928" n="e" s="T332">man </ts>
               <ts e="T334" id="Seg_2930" n="e" s="T333">tulsem </ts>
               <ts e="T335" id="Seg_2932" n="e" s="T334">teŋga </ts>
               <ts e="T336" id="Seg_2934" n="e" s="T335">oralǯolǯilʼews.” </ts>
               <ts e="T337" id="Seg_2936" n="e" s="T336">I </ts>
               <ts e="T338" id="Seg_2938" n="e" s="T337">erat </ts>
               <ts e="T339" id="Seg_2940" n="e" s="T338">tʼüan </ts>
               <ts e="T340" id="Seg_2942" n="e" s="T339">pajandɨne. </ts>
               <ts e="T341" id="Seg_2944" n="e" s="T340">Pajat </ts>
               <ts e="T342" id="Seg_2946" n="e" s="T341">erandɨne </ts>
               <ts e="T343" id="Seg_2948" n="e" s="T342">oralǯolǯit </ts>
               <ts e="T344" id="Seg_2950" n="e" s="T343">udoɣɨnt </ts>
               <ts e="T345" id="Seg_2952" n="e" s="T344">apstɨmbɨdi </ts>
               <ts e="T346" id="Seg_2954" n="e" s="T345">tʼülsem. </ts>
               <ts e="T347" id="Seg_2956" n="e" s="T346">Ond </ts>
               <ts e="T348" id="Seg_2958" n="e" s="T347">tabne </ts>
               <ts e="T349" id="Seg_2960" n="e" s="T348">pɨmʒɨrɨt, </ts>
               <ts e="T350" id="Seg_2962" n="e" s="T349">kundar </ts>
               <ts e="T351" id="Seg_2964" n="e" s="T350">tʼätku </ts>
               <ts e="T352" id="Seg_2966" n="e" s="T351">peŋgɨm. </ts>
               <ts e="T353" id="Seg_2968" n="e" s="T352">Erat </ts>
               <ts e="T354" id="Seg_2970" n="e" s="T353">tʼäʒit, </ts>
               <ts e="T355" id="Seg_2972" n="e" s="T354">qwannɨt </ts>
               <ts e="T356" id="Seg_2974" n="e" s="T355">peŋgɨm. </ts>
               <ts e="T357" id="Seg_2976" n="e" s="T356">Erand </ts>
               <ts e="T358" id="Seg_2978" n="e" s="T357">sej </ts>
               <ts e="T359" id="Seg_2980" n="e" s="T358">az </ts>
               <ts e="T360" id="Seg_2982" n="e" s="T359">aːduŋ, </ts>
               <ts e="T361" id="Seg_2984" n="e" s="T360">peŋgɨm </ts>
               <ts e="T362" id="Seg_2986" n="e" s="T361">tabne </ts>
               <ts e="T363" id="Seg_2988" n="e" s="T362">as </ts>
               <ts e="T364" id="Seg_2990" n="e" s="T363">kɨrgu. </ts>
               <ts e="T365" id="Seg_2992" n="e" s="T364">Pajat </ts>
               <ts e="T366" id="Seg_2994" n="e" s="T365">ondə </ts>
               <ts e="T367" id="Seg_2996" n="e" s="T366">kɨrlʼe </ts>
               <ts e="T368" id="Seg_2998" n="e" s="T367">üːbərɨt </ts>
               <ts e="T369" id="Seg_3000" n="e" s="T368">peŋgɨm. </ts>
               <ts e="T370" id="Seg_3002" n="e" s="T369">Nu </ts>
               <ts e="T371" id="Seg_3004" n="e" s="T370">i </ts>
               <ts e="T372" id="Seg_3006" n="e" s="T371">kɨrɨt </ts>
               <ts e="T373" id="Seg_3008" n="e" s="T372">peŋgɨm. </ts>
               <ts e="T374" id="Seg_3010" n="e" s="T373">Wadʼilam </ts>
               <ts e="T375" id="Seg_3012" n="e" s="T374">nʼörbannɨt, </ts>
               <ts e="T376" id="Seg_3014" n="e" s="T375">ɨːdɨt. </ts>
               <ts e="T377" id="Seg_3016" n="e" s="T376">Okkɨr </ts>
               <ts e="T378" id="Seg_3018" n="e" s="T377">dʼel </ts>
               <ts e="T379" id="Seg_3020" n="e" s="T378">wadʼilam </ts>
               <ts e="T380" id="Seg_3022" n="e" s="T379">kocʼin </ts>
               <ts e="T381" id="Seg_3024" n="e" s="T380">ponnɨt. </ts>
               <ts e="T382" id="Seg_3026" n="e" s="T381">Erandɨne </ts>
               <ts e="T383" id="Seg_3028" n="e" s="T382">tʼärɨŋ: </ts>
               <ts e="T384" id="Seg_3030" n="e" s="T383">“Tamdʼel </ts>
               <ts e="T385" id="Seg_3032" n="e" s="T384">qula </ts>
               <ts e="T386" id="Seg_3034" n="e" s="T385">tʼarattə, </ts>
               <ts e="T387" id="Seg_3036" n="e" s="T386">tamdʼelʼe </ts>
               <ts e="T388" id="Seg_3038" n="e" s="T387">dʼel </ts>
               <ts e="T389" id="Seg_3040" n="e" s="T388">iːbkaj </ts>
               <ts e="T390" id="Seg_3042" n="e" s="T389">nuːlʼdʼel.” </ts>
               <ts e="T391" id="Seg_3044" n="e" s="T390">Erat </ts>
               <ts e="T392" id="Seg_3046" n="e" s="T391">tʼärɨŋ: </ts>
               <ts e="T393" id="Seg_3048" n="e" s="T392">“Nuːlʼdʼelǯik.” </ts>
               <ts e="T394" id="Seg_3050" n="e" s="T393">Nu </ts>
               <ts e="T395" id="Seg_3052" n="e" s="T394">i </ts>
               <ts e="T396" id="Seg_3054" n="e" s="T395">nuːlʼdʼeːlčlʼe </ts>
               <ts e="T397" id="Seg_3056" n="e" s="T396">üːbərak. </ts>
               <ts e="T398" id="Seg_3058" n="e" s="T397">Pajat </ts>
               <ts e="T399" id="Seg_3060" n="e" s="T398">ponen, </ts>
               <ts e="T400" id="Seg_3062" n="e" s="T399">erat </ts>
               <ts e="T401" id="Seg_3064" n="e" s="T400">maːtqɨn </ts>
               <ts e="T402" id="Seg_3066" n="e" s="T401">jewan. </ts>
               <ts e="T403" id="Seg_3068" n="e" s="T402">Pajat </ts>
               <ts e="T404" id="Seg_3070" n="e" s="T403">ponen </ts>
               <ts e="T405" id="Seg_3072" n="e" s="T404">äːdɨt </ts>
               <ts e="T406" id="Seg_3074" n="e" s="T405">kuːgɨrsanɨm. </ts>
               <ts e="T407" id="Seg_3076" n="e" s="T406">Kuːgɨrkučilʼe </ts>
               <ts e="T408" id="Seg_3078" n="e" s="T407">üːbəraŋ. </ts>
               <ts e="T409" id="Seg_3080" n="e" s="T408">Erat </ts>
               <ts e="T410" id="Seg_3082" n="e" s="T409">maːtqɨn </ts>
               <ts e="T411" id="Seg_3084" n="e" s="T410">matškaŋ. </ts>
               <ts e="T412" id="Seg_3086" n="e" s="T411">A </ts>
               <ts e="T413" id="Seg_3088" n="e" s="T412">paja </ts>
               <ts e="T414" id="Seg_3090" n="e" s="T413">poːnen </ts>
               <ts e="T415" id="Seg_3092" n="e" s="T414">kugorčiŋ </ts>
               <ts e="T416" id="Seg_3094" n="e" s="T415">i </ts>
               <ts e="T417" id="Seg_3096" n="e" s="T416">kugorčiŋ. </ts>
               <ts e="T418" id="Seg_3098" n="e" s="T417">A </ts>
               <ts e="T419" id="Seg_3100" n="e" s="T418">ont </ts>
               <ts e="T420" id="Seg_3102" n="e" s="T419">čagəmbedi </ts>
               <ts e="T421" id="Seg_3104" n="e" s="T420">peŋqɨn </ts>
               <ts e="T422" id="Seg_3106" n="e" s="T421">wadʼi </ts>
               <ts e="T423" id="Seg_3108" n="e" s="T422">aurnɨŋ. </ts>
               <ts e="T424" id="Seg_3110" n="e" s="T423">Ont </ts>
               <ts e="T425" id="Seg_3112" n="e" s="T424">nuːn </ts>
               <ts e="T426" id="Seg_3114" n="e" s="T425">qojmɨn </ts>
               <ts e="T427" id="Seg_3116" n="e" s="T426">kelʼlʼe </ts>
               <ts e="T428" id="Seg_3118" n="e" s="T427">taːdərɨt. </ts>
               <ts e="T429" id="Seg_3120" n="e" s="T428">Qampaj </ts>
               <ts e="T430" id="Seg_3122" n="e" s="T429">tʼeːlat </ts>
               <ts e="T431" id="Seg_3124" n="e" s="T430">tʼümbocʼka. </ts>
               <ts e="T432" id="Seg_3126" n="e" s="T431">Erat </ts>
               <ts e="T433" id="Seg_3128" n="e" s="T432">maːtqɨn </ts>
               <ts e="T434" id="Seg_3130" n="e" s="T433">aːmdɨs, </ts>
               <ts e="T435" id="Seg_3132" n="e" s="T434">aːmdɨs, </ts>
               <ts e="T436" id="Seg_3134" n="e" s="T435">kertɨmnɨŋ. </ts>
               <ts e="T437" id="Seg_3136" n="e" s="T436">I </ts>
               <ts e="T438" id="Seg_3138" n="e" s="T437">teːrban: </ts>
               <ts e="T439" id="Seg_3140" n="e" s="T438">“Man </ts>
               <ts e="T440" id="Seg_3142" n="e" s="T439">sejlawɨm </ts>
               <ts e="T441" id="Seg_3144" n="e" s="T440">qwärlʼeu. </ts>
               <ts e="T442" id="Seg_3146" n="e" s="T441">Sejla </ts>
               <ts e="T443" id="Seg_3148" n="e" s="T442">moʒetbɨtʼ </ts>
               <ts e="T444" id="Seg_3150" n="e" s="T443">meŋga </ts>
               <ts e="T445" id="Seg_3152" n="e" s="T444">tʼülʼezeq </ts>
               <ts e="T446" id="Seg_3154" n="e" s="T445">(tüːlʼezettɨ).” </ts>
               <ts e="T447" id="Seg_3156" n="e" s="T446">Nu </ts>
               <ts e="T448" id="Seg_3158" n="e" s="T447">nilʼdʼiŋ </ts>
               <ts e="T449" id="Seg_3160" n="e" s="T448">i </ts>
               <ts e="T450" id="Seg_3162" n="e" s="T449">meot, </ts>
               <ts e="T451" id="Seg_3164" n="e" s="T450">sʼäjlamd </ts>
               <ts e="T452" id="Seg_3166" n="e" s="T451">qwärlʼe </ts>
               <ts e="T453" id="Seg_3168" n="e" s="T452">üːbərat. </ts>
               <ts e="T454" id="Seg_3170" n="e" s="T453">Qajda </ts>
               <ts e="T455" id="Seg_3172" n="e" s="T454">pʼöcʼin </ts>
               <ts e="T456" id="Seg_3174" n="e" s="T455">moqɨn </ts>
               <ts e="T457" id="Seg_3176" n="e" s="T456">raxsɨmban. </ts>
               <ts e="T458" id="Seg_3178" n="e" s="T457">Tap </ts>
               <ts e="T459" id="Seg_3180" n="e" s="T458">soːcʼkaŋ </ts>
               <ts e="T460" id="Seg_3182" n="e" s="T459">üŋgelǯiŋ. </ts>
               <ts e="T461" id="Seg_3184" n="e" s="T460">Ondə </ts>
               <ts e="T462" id="Seg_3186" n="e" s="T461">tʼeːrban: </ts>
               <ts e="T463" id="Seg_3188" n="e" s="T462">“Qaj </ts>
               <ts e="T464" id="Seg_3190" n="e" s="T463">natʼen </ts>
               <ts e="T465" id="Seg_3192" n="e" s="T464">raqsɨmɨnt?” </ts>
               <ts e="T466" id="Seg_3194" n="e" s="T465">Tap </ts>
               <ts e="T467" id="Seg_3196" n="e" s="T466">aj </ts>
               <ts e="T468" id="Seg_3198" n="e" s="T467">sədɨmǯel </ts>
               <ts e="T469" id="Seg_3200" n="e" s="T468">tʼärɨŋ: </ts>
               <ts e="T470" id="Seg_3202" n="e" s="T469">“Sejla </ts>
               <ts e="T471" id="Seg_3204" n="e" s="T470">roːpeːss!” </ts>
               <ts e="T472" id="Seg_3206" n="e" s="T471">A </ts>
               <ts e="T473" id="Seg_3208" n="e" s="T472">pʼöcʼin </ts>
               <ts e="T474" id="Seg_3210" n="e" s="T473">moqɨn </ts>
               <ts e="T475" id="Seg_3212" n="e" s="T474">warɣɨŋ </ts>
               <ts e="T476" id="Seg_3214" n="e" s="T475">raqsonneŋ </ts>
               <ts e="T477" id="Seg_3216" n="e" s="T476">qajda. </ts>
               <ts e="T478" id="Seg_3218" n="e" s="T477">Erat </ts>
               <ts e="T479" id="Seg_3220" n="e" s="T478">natʼet </ts>
               <ts e="T480" id="Seg_3222" n="e" s="T479">tʼüːrɨse </ts>
               <ts e="T481" id="Seg_3224" n="e" s="T480">tʼönǯikaŋ </ts>
               <ts e="T482" id="Seg_3226" n="e" s="T481">qwannɨŋ, </ts>
               <ts e="T483" id="Seg_3228" n="e" s="T482">pöcʼim </ts>
               <ts e="T484" id="Seg_3230" n="e" s="T483">moqɨŋ </ts>
               <ts e="T485" id="Seg_3232" n="e" s="T484">pündarɨmpan. </ts>
               <ts e="T486" id="Seg_3234" n="e" s="T485">Qoɨt </ts>
               <ts e="T487" id="Seg_3236" n="e" s="T486">tɨssam. </ts>
               <ts e="T488" id="Seg_3238" n="e" s="T487">Oːmdɨŋ, </ts>
               <ts e="T489" id="Seg_3240" n="e" s="T488">na </ts>
               <ts e="T490" id="Seg_3242" n="e" s="T489">tɨssɨm </ts>
               <ts e="T491" id="Seg_3244" n="e" s="T490">tʼiːqɨt </ts>
               <ts e="T492" id="Seg_3246" n="e" s="T491">i </ts>
               <ts e="T493" id="Seg_3248" n="e" s="T492">püːɣɨlǯimbat. </ts>
               <ts e="T494" id="Seg_3250" n="e" s="T493">Qajda </ts>
               <ts e="T495" id="Seg_3252" n="e" s="T494">sət </ts>
               <ts e="T496" id="Seg_3254" n="e" s="T495">püːrlaška. </ts>
               <ts e="T497" id="Seg_3256" n="e" s="T496">“Tau </ts>
               <ts e="T498" id="Seg_3258" n="e" s="T497">odnako </ts>
               <ts e="T499" id="Seg_3260" n="e" s="T498">manani </ts>
               <ts e="T500" id="Seg_3262" n="e" s="T499">sejla.” </ts>
               <ts e="T501" id="Seg_3264" n="e" s="T500">Pöti </ts>
               <ts e="T502" id="Seg_3266" n="e" s="T501">üːdɨm </ts>
               <ts e="T503" id="Seg_3268" n="e" s="T502">qoɣɨt </ts>
               <ts e="T504" id="Seg_3270" n="e" s="T503">erat. </ts>
               <ts e="T505" id="Seg_3272" n="e" s="T504">Üːttɨ </ts>
               <ts e="T506" id="Seg_3274" n="e" s="T505">tʼäptäptɨt. </ts>
               <ts e="T507" id="Seg_3276" n="e" s="T506">Sejlat </ts>
               <ts e="T508" id="Seg_3278" n="e" s="T507">(sət </ts>
               <ts e="T509" id="Seg_3280" n="e" s="T508">sej) </ts>
               <ts e="T510" id="Seg_3282" n="e" s="T509">kušaŋ </ts>
               <ts e="T511" id="Seg_3284" n="e" s="T510">üːtqɨn </ts>
               <ts e="T512" id="Seg_3286" n="e" s="T511">tʼäptädimbɨzattə. </ts>
               <ts e="T513" id="Seg_3288" n="e" s="T512">Saʒaŋ </ts>
               <ts e="T514" id="Seg_3290" n="e" s="T513">müzulǯit. </ts>
               <ts e="T515" id="Seg_3292" n="e" s="T514">Sʼäjlaɣɨnd </ts>
               <ts e="T516" id="Seg_3294" n="e" s="T515">tʼärɨŋ: </ts>
               <ts e="T517" id="Seg_3296" n="e" s="T516">“Sʼäjla </ts>
               <ts e="T518" id="Seg_3298" n="e" s="T517">ropeːss!” </ts>
               <ts e="T519" id="Seg_3300" n="e" s="T518">Sejlat </ts>
               <ts e="T520" id="Seg_3302" n="e" s="T519">tabɨnä </ts>
               <ts e="T521" id="Seg_3304" n="e" s="T520">parɨdʼättə. </ts>
               <ts e="T522" id="Seg_3306" n="e" s="T521">Manǯedʼiŋ, </ts>
               <ts e="T523" id="Seg_3308" n="e" s="T522">a </ts>
               <ts e="T524" id="Seg_3310" n="e" s="T523">sejlat </ts>
               <ts e="T525" id="Seg_3312" n="e" s="T524">soːčʼkaŋ </ts>
               <ts e="T526" id="Seg_3314" n="e" s="T525">az </ts>
               <ts e="T527" id="Seg_3316" n="e" s="T526">aːdattə. </ts>
               <ts e="T528" id="Seg_3318" n="e" s="T527">Sejlamd </ts>
               <ts e="T529" id="Seg_3320" n="e" s="T528">paqqɨnɨt, </ts>
               <ts e="T530" id="Seg_3322" n="e" s="T529">aj </ts>
               <ts e="T531" id="Seg_3324" n="e" s="T530">tʼäptäptɨt </ts>
               <ts e="T532" id="Seg_3326" n="e" s="T531">soːcʼkaŋ. </ts>
               <ts e="T533" id="Seg_3328" n="e" s="T532">Sʼäjlamt </ts>
               <ts e="T534" id="Seg_3330" n="e" s="T533">aj </ts>
               <ts e="T535" id="Seg_3332" n="e" s="T534">soːcʼkaŋ </ts>
               <ts e="T536" id="Seg_3334" n="e" s="T535">müsennɨt. </ts>
               <ts e="T537" id="Seg_3336" n="e" s="T536">Tʼärɨŋ: </ts>
               <ts e="T538" id="Seg_3338" n="e" s="T537">“Sejla, </ts>
               <ts e="T539" id="Seg_3340" n="e" s="T538">roːpeːss!” </ts>
               <ts e="T540" id="Seg_3342" n="e" s="T539">Sejlat </ts>
               <ts e="T541" id="Seg_3344" n="e" s="T540">parɨdʼättə </ts>
               <ts e="T542" id="Seg_3346" n="e" s="T541">kwesse. </ts>
               <ts e="T543" id="Seg_3348" n="e" s="T542">Tibequm </ts>
               <ts e="T544" id="Seg_3350" n="e" s="T543">aːndanɨŋ: </ts>
               <ts e="T545" id="Seg_3352" n="e" s="T544">“Tidam </ts>
               <ts e="T546" id="Seg_3354" n="e" s="T545">sejlau </ts>
               <ts e="T547" id="Seg_3356" n="e" s="T546">soːcʼkaŋ </ts>
               <ts e="T548" id="Seg_3358" n="e" s="T547">konǯernattə. </ts>
               <ts e="T549" id="Seg_3360" n="e" s="T548">Tɨdam </ts>
               <ts e="T550" id="Seg_3362" n="e" s="T549">man </ts>
               <ts e="T551" id="Seg_3364" n="e" s="T550">sejze </ts>
               <ts e="T552" id="Seg_3366" n="e" s="T551">ezaŋ. </ts>
               <ts e="T553" id="Seg_3368" n="e" s="T552">Uʒo </ts>
               <ts e="T554" id="Seg_3370" n="e" s="T553">man </ts>
               <ts e="T555" id="Seg_3372" n="e" s="T554">tast </ts>
               <ts e="T556" id="Seg_3374" n="e" s="T555">sejlaɣɨn </ts>
               <ts e="T557" id="Seg_3376" n="e" s="T556">tʼät </ts>
               <ts e="T558" id="Seg_3378" n="e" s="T557">qoːbalaks.” </ts>
               <ts e="T559" id="Seg_3380" n="e" s="T558">Pone </ts>
               <ts e="T560" id="Seg_3382" n="e" s="T559">čaːnǯiŋ. </ts>
               <ts e="T561" id="Seg_3384" n="e" s="T560">A </ts>
               <ts e="T562" id="Seg_3386" n="e" s="T561">pajat </ts>
               <ts e="T563" id="Seg_3388" n="e" s="T562">kugurgutčiŋ, </ts>
               <ts e="T564" id="Seg_3390" n="e" s="T563">nu </ts>
               <ts e="T565" id="Seg_3392" n="e" s="T564">kojmɨm </ts>
               <ts e="T566" id="Seg_3394" n="e" s="T565">taːdərɨt. </ts>
               <ts e="T567" id="Seg_3396" n="e" s="T566">Erat </ts>
               <ts e="T568" id="Seg_3398" n="e" s="T567">tʼärɨŋ </ts>
               <ts e="T569" id="Seg_3400" n="e" s="T568">pajandɨne: </ts>
               <ts e="T570" id="Seg_3402" n="e" s="T569">“A </ts>
               <ts e="T571" id="Seg_3404" n="e" s="T570">tan </ts>
               <ts e="T572" id="Seg_3406" n="e" s="T571">mazɨm </ts>
               <ts e="T573" id="Seg_3408" n="e" s="T572">qajno </ts>
               <ts e="T574" id="Seg_3410" n="e" s="T573">sejgɨlǯant?” </ts>
               <ts e="T575" id="Seg_3412" n="e" s="T574">A </ts>
               <ts e="T576" id="Seg_3414" n="e" s="T575">pajat </ts>
               <ts e="T577" id="Seg_3416" n="e" s="T576">tʼärɨŋ: </ts>
               <ts e="T578" id="Seg_3418" n="e" s="T577">“A </ts>
               <ts e="T579" id="Seg_3420" n="e" s="T578">tan </ts>
               <ts e="T580" id="Seg_3422" n="e" s="T579">qajno </ts>
               <ts e="T581" id="Seg_3424" n="e" s="T580">nilʼdʼiŋ </ts>
               <ts e="T582" id="Seg_3426" n="e" s="T581">palʼdʼikuzant, </ts>
               <ts e="T583" id="Seg_3428" n="e" s="T582">köcʼkuzandə </ts>
               <ts e="T584" id="Seg_3430" n="e" s="T583">suːrusku, </ts>
               <ts e="T585" id="Seg_3432" n="e" s="T584">a </ts>
               <ts e="T586" id="Seg_3434" n="e" s="T585">onent </ts>
               <ts e="T587" id="Seg_3436" n="e" s="T586">sejlandɨse </ts>
               <ts e="T588" id="Seg_3438" n="e" s="T587">sanǯirsant? </ts>
               <ts e="T589" id="Seg_3440" n="e" s="T588">A </ts>
               <ts e="T590" id="Seg_3442" n="e" s="T589">maːtt </ts>
               <ts e="T591" id="Seg_3444" n="e" s="T590">qaimnej </ts>
               <ts e="T592" id="Seg_3446" n="e" s="T591">as </ts>
               <ts e="T593" id="Seg_3448" n="e" s="T592">taːtkuzal. </ts>
               <ts e="T594" id="Seg_3450" n="e" s="T593">Wot </ts>
               <ts e="T595" id="Seg_3452" n="e" s="T594">man </ts>
               <ts e="T596" id="Seg_3454" n="e" s="T595">tannani </ts>
               <ts e="T597" id="Seg_3456" n="e" s="T596">sejlamt </ts>
               <ts e="T598" id="Seg_3458" n="e" s="T597">nano </ts>
               <ts e="T599" id="Seg_3460" n="e" s="T598">i </ts>
               <ts e="T600" id="Seg_3462" n="e" s="T599">(paqqɨnnau) </ts>
               <ts e="T601" id="Seg_3464" n="e" s="T600">tannau.” </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_3465" s="T1">PMP_1961_PlayingWithEyes_flk.001 (001.001)</ta>
            <ta e="T8" id="Seg_3466" s="T5">PMP_1961_PlayingWithEyes_flk.002 (001.002)</ta>
            <ta e="T11" id="Seg_3467" s="T8">PMP_1961_PlayingWithEyes_flk.003 (001.003)</ta>
            <ta e="T16" id="Seg_3468" s="T11">PMP_1961_PlayingWithEyes_flk.004 (001.004)</ta>
            <ta e="T19" id="Seg_3469" s="T16">PMP_1961_PlayingWithEyes_flk.005 (001.005)</ta>
            <ta e="T25" id="Seg_3470" s="T19">PMP_1961_PlayingWithEyes_flk.006 (001.006)</ta>
            <ta e="T35" id="Seg_3471" s="T25">PMP_1961_PlayingWithEyes_flk.007 (001.007)</ta>
            <ta e="T38" id="Seg_3472" s="T35">PMP_1961_PlayingWithEyes_flk.008 (001.008)</ta>
            <ta e="T42" id="Seg_3473" s="T38">PMP_1961_PlayingWithEyes_flk.009 (001.009)</ta>
            <ta e="T46" id="Seg_3474" s="T42">PMP_1961_PlayingWithEyes_flk.010 (001.010)</ta>
            <ta e="T51" id="Seg_3475" s="T46">PMP_1961_PlayingWithEyes_flk.011 (001.011)</ta>
            <ta e="T55" id="Seg_3476" s="T51">PMP_1961_PlayingWithEyes_flk.012 (001.012)</ta>
            <ta e="T59" id="Seg_3477" s="T55">PMP_1961_PlayingWithEyes_flk.013 (001.013)</ta>
            <ta e="T62" id="Seg_3478" s="T59">PMP_1961_PlayingWithEyes_flk.014 (001.014)</ta>
            <ta e="T69" id="Seg_3479" s="T62">PMP_1961_PlayingWithEyes_flk.015 (001.015)</ta>
            <ta e="T73" id="Seg_3480" s="T69">PMP_1961_PlayingWithEyes_flk.016 (001.016)</ta>
            <ta e="T80" id="Seg_3481" s="T73">PMP_1961_PlayingWithEyes_flk.017 (001.017)</ta>
            <ta e="T85" id="Seg_3482" s="T80">PMP_1961_PlayingWithEyes_flk.018 (001.018)</ta>
            <ta e="T89" id="Seg_3483" s="T85">PMP_1961_PlayingWithEyes_flk.019 (001.019)</ta>
            <ta e="T93" id="Seg_3484" s="T89">PMP_1961_PlayingWithEyes_flk.020 (001.020)</ta>
            <ta e="T97" id="Seg_3485" s="T93">PMP_1961_PlayingWithEyes_flk.021 (001.021)</ta>
            <ta e="T107" id="Seg_3486" s="T97">PMP_1961_PlayingWithEyes_flk.022 (001.022)</ta>
            <ta e="T110" id="Seg_3487" s="T107">PMP_1961_PlayingWithEyes_flk.023 (001.023)</ta>
            <ta e="T114" id="Seg_3488" s="T110">PMP_1961_PlayingWithEyes_flk.024 (001.024)</ta>
            <ta e="T119" id="Seg_3489" s="T114">PMP_1961_PlayingWithEyes_flk.025 (001.025)</ta>
            <ta e="T123" id="Seg_3490" s="T119">PMP_1961_PlayingWithEyes_flk.026 (001.026)</ta>
            <ta e="T126" id="Seg_3491" s="T123">PMP_1961_PlayingWithEyes_flk.027 (001.027)</ta>
            <ta e="T131" id="Seg_3492" s="T126">PMP_1961_PlayingWithEyes_flk.028 (001.028)</ta>
            <ta e="T138" id="Seg_3493" s="T131">PMP_1961_PlayingWithEyes_flk.029 (001.029)</ta>
            <ta e="T148" id="Seg_3494" s="T138">PMP_1961_PlayingWithEyes_flk.030 (001.030)</ta>
            <ta e="T155" id="Seg_3495" s="T148">PMP_1961_PlayingWithEyes_flk.031 (001.031)</ta>
            <ta e="T159" id="Seg_3496" s="T155">PMP_1961_PlayingWithEyes_flk.032 (001.032)</ta>
            <ta e="T163" id="Seg_3497" s="T159">PMP_1961_PlayingWithEyes_flk.033 (001.033)</ta>
            <ta e="T168" id="Seg_3498" s="T163">PMP_1961_PlayingWithEyes_flk.034 (001.034)</ta>
            <ta e="T177" id="Seg_3499" s="T168">PMP_1961_PlayingWithEyes_flk.035 (001.035)</ta>
            <ta e="T184" id="Seg_3500" s="T177">PMP_1961_PlayingWithEyes_flk.036 (001.036)</ta>
            <ta e="T192" id="Seg_3501" s="T184">PMP_1961_PlayingWithEyes_flk.037 (001.037)</ta>
            <ta e="T202" id="Seg_3502" s="T192">PMP_1961_PlayingWithEyes_flk.038 (001.038)</ta>
            <ta e="T208" id="Seg_3503" s="T202">PMP_1961_PlayingWithEyes_flk.039 (001.039)</ta>
            <ta e="T213" id="Seg_3504" s="T208">PMP_1961_PlayingWithEyes_flk.040 (001.040)</ta>
            <ta e="T217" id="Seg_3505" s="T213">PMP_1961_PlayingWithEyes_flk.041 (001.041)</ta>
            <ta e="T221" id="Seg_3506" s="T217">PMP_1961_PlayingWithEyes_flk.042 (001.042)</ta>
            <ta e="T223" id="Seg_3507" s="T221">PMP_1961_PlayingWithEyes_flk.043 (001.043)</ta>
            <ta e="T228" id="Seg_3508" s="T223">PMP_1961_PlayingWithEyes_flk.044 (001.044)</ta>
            <ta e="T231" id="Seg_3509" s="T228">PMP_1961_PlayingWithEyes_flk.045 (001.045)</ta>
            <ta e="T238" id="Seg_3510" s="T231">PMP_1961_PlayingWithEyes_flk.046 (001.046)</ta>
            <ta e="T243" id="Seg_3511" s="T238">PMP_1961_PlayingWithEyes_flk.047 (001.047)</ta>
            <ta e="T251" id="Seg_3512" s="T243">PMP_1961_PlayingWithEyes_flk.048 (001.048)</ta>
            <ta e="T260" id="Seg_3513" s="T251">PMP_1961_PlayingWithEyes_flk.049 (001.049)</ta>
            <ta e="T272" id="Seg_3514" s="T260">PMP_1961_PlayingWithEyes_flk.050 (001.050)</ta>
            <ta e="T276" id="Seg_3515" s="T272">PMP_1961_PlayingWithEyes_flk.051 (001.051)</ta>
            <ta e="T286" id="Seg_3516" s="T276">PMP_1961_PlayingWithEyes_flk.052 (001.052)</ta>
            <ta e="T290" id="Seg_3517" s="T286">PMP_1961_PlayingWithEyes_flk.053 (001.053)</ta>
            <ta e="T295" id="Seg_3518" s="T290">PMP_1961_PlayingWithEyes_flk.054 (001.054)</ta>
            <ta e="T299" id="Seg_3519" s="T295">PMP_1961_PlayingWithEyes_flk.055 (001.055)</ta>
            <ta e="T303" id="Seg_3520" s="T299">PMP_1961_PlayingWithEyes_flk.056 (001.056)</ta>
            <ta e="T309" id="Seg_3521" s="T303">PMP_1961_PlayingWithEyes_flk.057 (001.057)</ta>
            <ta e="T314" id="Seg_3522" s="T309">PMP_1961_PlayingWithEyes_flk.058 (001.058)</ta>
            <ta e="T319" id="Seg_3523" s="T314">PMP_1961_PlayingWithEyes_flk.059 (001.059)</ta>
            <ta e="T325" id="Seg_3524" s="T319">PMP_1961_PlayingWithEyes_flk.060 (001.060)</ta>
            <ta e="T328" id="Seg_3525" s="T325">PMP_1961_PlayingWithEyes_flk.061 (001.061)</ta>
            <ta e="T336" id="Seg_3526" s="T328">PMP_1961_PlayingWithEyes_flk.062 (001.062)</ta>
            <ta e="T340" id="Seg_3527" s="T336">PMP_1961_PlayingWithEyes_flk.063 (001.063)</ta>
            <ta e="T346" id="Seg_3528" s="T340">PMP_1961_PlayingWithEyes_flk.064 (001.064)</ta>
            <ta e="T352" id="Seg_3529" s="T346">PMP_1961_PlayingWithEyes_flk.065 (001.065)</ta>
            <ta e="T356" id="Seg_3530" s="T352">PMP_1961_PlayingWithEyes_flk.066 (001.066)</ta>
            <ta e="T364" id="Seg_3531" s="T356">PMP_1961_PlayingWithEyes_flk.067 (001.067)</ta>
            <ta e="T369" id="Seg_3532" s="T364">PMP_1961_PlayingWithEyes_flk.068 (001.068)</ta>
            <ta e="T373" id="Seg_3533" s="T369">PMP_1961_PlayingWithEyes_flk.069 (001.069)</ta>
            <ta e="T376" id="Seg_3534" s="T373">PMP_1961_PlayingWithEyes_flk.070 (001.070)</ta>
            <ta e="T381" id="Seg_3535" s="T376">PMP_1961_PlayingWithEyes_flk.071 (001.071)</ta>
            <ta e="T390" id="Seg_3536" s="T381">PMP_1961_PlayingWithEyes_flk.072 (001.072)</ta>
            <ta e="T393" id="Seg_3537" s="T390">PMP_1961_PlayingWithEyes_flk.073 (001.073)</ta>
            <ta e="T397" id="Seg_3538" s="T393">PMP_1961_PlayingWithEyes_flk.074 (001.074)</ta>
            <ta e="T402" id="Seg_3539" s="T397">PMP_1961_PlayingWithEyes_flk.075 (001.075)</ta>
            <ta e="T406" id="Seg_3540" s="T402">PMP_1961_PlayingWithEyes_flk.076 (001.076)</ta>
            <ta e="T408" id="Seg_3541" s="T406">PMP_1961_PlayingWithEyes_flk.077 (001.077)</ta>
            <ta e="T411" id="Seg_3542" s="T408">PMP_1961_PlayingWithEyes_flk.078 (001.078)</ta>
            <ta e="T417" id="Seg_3543" s="T411">PMP_1961_PlayingWithEyes_flk.079 (001.079)</ta>
            <ta e="T423" id="Seg_3544" s="T417">PMP_1961_PlayingWithEyes_flk.080 (001.080)</ta>
            <ta e="T428" id="Seg_3545" s="T423">PMP_1961_PlayingWithEyes_flk.081 (001.081)</ta>
            <ta e="T431" id="Seg_3546" s="T428">PMP_1961_PlayingWithEyes_flk.082 (001.082)</ta>
            <ta e="T436" id="Seg_3547" s="T431">PMP_1961_PlayingWithEyes_flk.083 (001.083)</ta>
            <ta e="T441" id="Seg_3548" s="T436">PMP_1961_PlayingWithEyes_flk.084 (001.084)</ta>
            <ta e="T446" id="Seg_3549" s="T441">PMP_1961_PlayingWithEyes_flk.085 (001.085)</ta>
            <ta e="T453" id="Seg_3550" s="T446">PMP_1961_PlayingWithEyes_flk.086 (001.086)</ta>
            <ta e="T457" id="Seg_3551" s="T453">PMP_1961_PlayingWithEyes_flk.087 (001.087)</ta>
            <ta e="T460" id="Seg_3552" s="T457">PMP_1961_PlayingWithEyes_flk.088 (001.088)</ta>
            <ta e="T465" id="Seg_3553" s="T460">PMP_1961_PlayingWithEyes_flk.089 (001.089)</ta>
            <ta e="T471" id="Seg_3554" s="T465">PMP_1961_PlayingWithEyes_flk.090 (001.090)</ta>
            <ta e="T477" id="Seg_3555" s="T471">PMP_1961_PlayingWithEyes_flk.091 (001.091)</ta>
            <ta e="T485" id="Seg_3556" s="T477">PMP_1961_PlayingWithEyes_flk.092 (001.092)</ta>
            <ta e="T487" id="Seg_3557" s="T485">PMP_1961_PlayingWithEyes_flk.093 (001.093)</ta>
            <ta e="T493" id="Seg_3558" s="T487">PMP_1961_PlayingWithEyes_flk.094 (001.094)</ta>
            <ta e="T496" id="Seg_3559" s="T493">PMP_1961_PlayingWithEyes_flk.095 (001.095)</ta>
            <ta e="T500" id="Seg_3560" s="T496">PMP_1961_PlayingWithEyes_flk.096 (001.096)</ta>
            <ta e="T504" id="Seg_3561" s="T500">PMP_1961_PlayingWithEyes_flk.097 (001.097)</ta>
            <ta e="T506" id="Seg_3562" s="T504">PMP_1961_PlayingWithEyes_flk.098 (001.098)</ta>
            <ta e="T512" id="Seg_3563" s="T506">PMP_1961_PlayingWithEyes_flk.099 (001.099)</ta>
            <ta e="T514" id="Seg_3564" s="T512">PMP_1961_PlayingWithEyes_flk.100 (001.100)</ta>
            <ta e="T518" id="Seg_3565" s="T514">PMP_1961_PlayingWithEyes_flk.101 (001.101)</ta>
            <ta e="T521" id="Seg_3566" s="T518">PMP_1961_PlayingWithEyes_flk.102 (001.102)</ta>
            <ta e="T527" id="Seg_3567" s="T521">PMP_1961_PlayingWithEyes_flk.103 (001.103)</ta>
            <ta e="T532" id="Seg_3568" s="T527">PMP_1961_PlayingWithEyes_flk.104 (001.104)</ta>
            <ta e="T536" id="Seg_3569" s="T532">PMP_1961_PlayingWithEyes_flk.105 (001.105)</ta>
            <ta e="T539" id="Seg_3570" s="T536">PMP_1961_PlayingWithEyes_flk.106 (001.106)</ta>
            <ta e="T542" id="Seg_3571" s="T539">PMP_1961_PlayingWithEyes_flk.107 (001.107)</ta>
            <ta e="T548" id="Seg_3572" s="T542">PMP_1961_PlayingWithEyes_flk.108 (001.108)</ta>
            <ta e="T552" id="Seg_3573" s="T548">PMP_1961_PlayingWithEyes_flk.109 (001.109)</ta>
            <ta e="T558" id="Seg_3574" s="T552">PMP_1961_PlayingWithEyes_flk.110 (001.110)</ta>
            <ta e="T560" id="Seg_3575" s="T558">PMP_1961_PlayingWithEyes_flk.111 (001.111)</ta>
            <ta e="T566" id="Seg_3576" s="T560">PMP_1961_PlayingWithEyes_flk.112 (001.112)</ta>
            <ta e="T574" id="Seg_3577" s="T566">PMP_1961_PlayingWithEyes_flk.113 (001.113)</ta>
            <ta e="T588" id="Seg_3578" s="T574">PMP_1961_PlayingWithEyes_flk.114 (001.114)</ta>
            <ta e="T593" id="Seg_3579" s="T588">PMP_1961_PlayingWithEyes_flk.115 (001.115)</ta>
            <ta e="T601" id="Seg_3580" s="T593">PMP_1961_PlayingWithEyes_flk.116 (001.116)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_3581" s="T1">илы′заk варкы′заk е′ра па′jасак.</ta>
            <ta e="T8" id="Seg_3582" s="T5">е′рат палʼдʼу′куң ′сӯрылʼе.</ta>
            <ta e="T11" id="Seg_3583" s="T8">па′jат ′ма̄тkын ′jеван.</ta>
            <ta e="T16" id="Seg_3584" s="T11">е′рат палʼдʼи′куң, палʼдʼи′куң, ‵тʼе̄лʼ′дʼӧмп палʼдʼи(е)′куң.</ta>
            <ta e="T19" id="Seg_3585" s="T16">kаи′мнӓй асс тат′кут.</ta>
            <ta e="T25" id="Seg_3586" s="T19">′кыдаң ′ниң палʼдʼи′куң, kаим′не̨ ас тат′кут.</ta>
            <ta e="T35" id="Seg_3587" s="T25">′оккыр ′д̂ʼел па′jат ′те̄рбан: kай′но жъ тап kаим′нӓй ас тат′кут?</ta>
            <ta e="T38" id="Seg_3588" s="T35">ман та′бым са′ралджлʼеw.</ta>
            <ta e="T42" id="Seg_3589" s="T38">kай′но тап ‵сӱнде′бың ′тӱккуң?</ta>
            <ta e="T46" id="Seg_3590" s="T42">ну и нилʼ′дʼ(зʼ)иң ′ме̄выт.</ta>
            <ta e="T51" id="Seg_3591" s="T46">е′рат ′ӱ̄быцʼлʼе ′ӱ̄быраң ′сӯрулʼе, kwа′нның.</ta>
            <ta e="T55" id="Seg_3592" s="T51">па′jат ′тʼӓ̄но толʼ′дʼ(зʼ)имт(дъ) ′сернат.</ta>
            <ta e="T59" id="Seg_3593" s="T55">е′ранд мо′kын ′тʼӓно ′ӱ̄бан.</ta>
            <ta e="T62" id="Seg_3594" s="T59">е′рандыне аз ′а̄долджикуң.</ta>
            <ta e="T69" id="Seg_3595" s="T62">на′но аз ′а̄долджи′куң, та′бым е′рат ик kонджир′нент.</ta>
            <ta e="T73" id="Seg_3596" s="T69">е′рат ме′дың kын ′kынбарт.</ta>
            <ta e="T80" id="Seg_3597" s="T73">′там плʼе′кай kӓ(ы)н ′па̄р(о)ɣын онт сʼӓ′имд паkkыл′гут.</ta>
            <ta e="T85" id="Seg_3598" s="T80">сʼӓи′ламд ӓды′лʼе ′kwӓтш(цʼ)икут пон ′молаɣы(о)н.</ta>
            <ta e="T89" id="Seg_3599" s="T85">тай плʼе′кай kӓнт нʼӓ′золгуң.</ta>
            <ta e="T93" id="Seg_3600" s="T89">′ондъ тʼӓр′куң: ′сʼӓй, ро′пе̄с.</ta>
            <ta e="T97" id="Seg_3601" s="T93">сʼӓйла таб′нӓ ′тʼӱkwатт, ‵па̄рытши′kwаттъ.</ta>
            <ta e="T107" id="Seg_3602" s="T97">ай на′тʼей kын ′па̄′роɣын се′инт паkkы′лʼе ′kwецʼи‵кут, пон ′мо̄ланд ы(ӓ)т′кут.</ta>
            <ta e="T110" id="Seg_3603" s="T107">тʼӓр′куң: сʼӓй′ла ро̄′пе̄сс!</ta>
            <ta e="T114" id="Seg_3604" s="T110">сʼӓй′ла таб′не ′тʼӱлʼе ро′кецʼkwатт.</ta>
            <ta e="T119" id="Seg_3605" s="T114">нилʼдʼӓ ′реда(ы)ң ′селый ′тʼе̄лдʼжом пӱ′нның.</ta>
            <ta e="T123" id="Seg_3606" s="T119">па′jат ‵манным′пыс, ‵манным′пыс ′то̄лакаң.</ta>
            <ta e="T126" id="Seg_3607" s="T123">е′рат се̄′имд паkkъ′нныт.</ta>
            <ta e="T131" id="Seg_3608" s="T126">там плʼе′кай kӓн ′кӓнбарт ′kwӓдʼӓ(и)т.</ta>
            <ta e="T138" id="Seg_3609" s="T131">а па′jат ′тʼӓ̄но та′бын сей′ламд ӣот ′то̄лакаң.</ta>
            <ta e="T148" id="Seg_3610" s="T138">онтъ ′те̄рп(б̂)ан: а тан на′но ′ма̄ттъ kаим′ней ас тат′kwал ′меңга.</ta>
            <ta e="T155" id="Seg_3611" s="T148">нилʼ′дʼиң па′jат kwа′нның, kwа′нның и тʼӧнджи′каң ′тша̄джиң.</ta>
            <ta e="T159" id="Seg_3612" s="T155">е′рантынан се′ӣт ти′дам ′тʼӓңван.</ta>
            <ta e="T163" id="Seg_3613" s="T159">′kwӓцʼку та′бым таб ӱр′генджиң.</ta>
            <ta e="T168" id="Seg_3614" s="T163">на′но и тʼӧнджи′каң ′тша̄джиң па′jат.</ta>
            <ta e="T177" id="Seg_3615" s="T168">ку′шаң ′тшаджиң, ай моɣу′нӓ ман′джедʼикуң, е′рат ′тша̄джиң ′алʼи ′асс.</ta>
            <ta e="T184" id="Seg_3616" s="T177">па′jант kwа′лʼевлʼе е′рат ку′шаң ‵лаңгы′ниң: сʼӓй′ла, ро′пе̄сс!</ta>
            <ta e="T192" id="Seg_3617" s="T184">сʼӓй′ла ′тʼӓңгваттъ, ас ′тӱкватте таб′нӓ и асс ‵парыдʼ(цʼ)и′kwаттъ.</ta>
            <ta e="T202" id="Seg_3618" s="T192">ку′шаң сʼӓй′ламт ′kwӓрыстъ, ′kwӓрыст, сʼӓи′ла кун′дар тʼӓңгваттъ, ′ниңи ′тʼӓңгватт.</ta>
            <ta e="T208" id="Seg_3619" s="T202">ти′дам сей′галык е′рат на ′ӱ̄бедʼӓн, сʼӓй′галык.</ta>
            <ta e="T213" id="Seg_3620" s="T208">па′jат а̄дел′джиң, е′рандыне аз а̄делджи′куң.</ta>
            <ta e="T217" id="Seg_3621" s="T213">е′рат сей′галык ӱргы′ле ′kунджиң.</ta>
            <ta e="T221" id="Seg_3622" s="T217">′мадын ′кӧндъ миг′лʼе ′ӱ̄бъраk.</ta>
            <ta e="T223" id="Seg_3623" s="T221">па′jат ′тыдъмб̂ан.</ta>
            <ta e="T228" id="Seg_3624" s="T223">тʼӓ̄но меды′дʼең, толʼдʼи′ламд ни′ңел ′тшаɣанны(а)т.</ta>
            <ta e="T231" id="Seg_3625" s="T228">′маттъ ′се̄рның, ‵нʼӓɣы′ниң.</ta>
            <ta e="T238" id="Seg_3626" s="T231">е′рант сей′лам ты′с(с)онт пе′нныт, ‵пʼӧтʼ(цʼ)ен ′моkтъ о̄′цʼиңныт.</ta>
            <ta e="T243" id="Seg_3627" s="T238">е′рат тʼӧнджи′каң ′тӱан ′ма̄ттъ, ′сернаң.</ta>
            <ta e="T251" id="Seg_3628" s="T243">па′jат тʼӓ′рың: а тан kай′но там′дʼел ′кундың ′jевант?</ta>
            <ta e="T260" id="Seg_3629" s="T251">е′рат тʼӓ′рың: ман там′дʼел сей′овым а̄т′саң, на′но ку′ндың ′jеваң.</ta>
            <ta e="T272" id="Seg_3630" s="T260">па′jат ′те̨̄рбан: а тан ′сӣрант, ман ти′дам тас′ты туну′аң, кун′дар тан ′сӯрунʼӓнт.</ta>
            <ta e="T276" id="Seg_3631" s="T272">′селый тʼелʼдʼзʼомп сей′ландысе вар′канд.</ta>
            <ta e="T286" id="Seg_3632" s="T276">аw ′kынб̂а̄рт ыт′kwӓл, а там плʼе′кай кынб̂а̄рт не̄′золгвант.</ta>
            <ta e="T290" id="Seg_3633" s="T286">сей′лал ′тʼеңга ′тʼӱлʼе ‵парыцʼи′кваттъ.</ta>
            <ta e="T295" id="Seg_3634" s="T290">нилʼ′дʼиң тан и ‵сӱнде′бың ′тӱкку′зант.</ta>
            <ta e="T299" id="Seg_3635" s="T295">ну и варкы′лʼе ′ӱ̄бъраk.</ta>
            <ta e="T303" id="Seg_3636" s="T299">ку′шай да ′д̂ʼе̄лыт мен′дың.</ta>
            <ta e="T309" id="Seg_3637" s="T303">па′jат ман′джедиң ‵сӱдер′наун, ман′джедиң, ны′ңан ′пең(н̃)к.</ta>
            <ta e="T314" id="Seg_3638" s="T309">па′jат е′рандыне тʼӓ′рың: ′пеңкъ ның′гынт.</ta>
            <ta e="T319" id="Seg_3639" s="T314">тан ваз′лʼент кун‵да′рем тʼӓж(и)′лʼел ′пеңкъм.</ta>
            <ta e="T325" id="Seg_3640" s="T319">а е′рат тʼӓ′рың: ман кун′дар тʼӓ′дженджау?</ta>
            <ta e="T328" id="Seg_3641" s="T325">се′йеw аз а̄′дуң.</ta>
            <ta e="T336" id="Seg_3642" s="T328">па′jат тʼӓ′рың: тӱ′kы ′меңга, ман туl′сем ′теңга орал′джолджи′лʼевс.</ta>
            <ta e="T340" id="Seg_3643" s="T336">и е′рат тʼӱан па′jандыне.</ta>
            <ta e="T346" id="Seg_3644" s="T340">па′jат е′рандыне орал′джолджит у′доɣынт ‵апстымбы′ди тʼӱl′сем.</ta>
            <ta e="T352" id="Seg_3645" s="T346">онд таб′не̨ пымжы′рыт, кун′дар тʼӓтку ′пеңгым.</ta>
            <ta e="T356" id="Seg_3646" s="T352">е′рат тʼӓ′жит, kwа′нныт ′пеңгым.</ta>
            <ta e="T364" id="Seg_3647" s="T356">е′ранд ′сей аз а̄′дуң, пеңгым таб′не ас кыр′гу.</ta>
            <ta e="T369" id="Seg_3648" s="T364">па′jат ′ондъ кыр′лʼе ′ӱ̄бърыт ′пеңгым.</ta>
            <ta e="T373" id="Seg_3649" s="T369">ну и кы′рыт ′пеңгым.</ta>
            <ta e="T376" id="Seg_3650" s="T373">вадʼи′лам нʼӧр′банныт, ы̄(ӓ̄)′дыт.</ta>
            <ta e="T381" id="Seg_3651" s="T376">оккыр ′дʼел вадʼи′лам ′коцʼин ′понныт.</ta>
            <ta e="T390" id="Seg_3652" s="T381">е′рандыне тʼӓрың: там′дʼел kу′ла тʼа′раттъ, там′дʼелʼе ′дʼел ӣбкай ′нӯлʼ′д̂ʼ(тʼ)ел.</ta>
            <ta e="T393" id="Seg_3653" s="T390">е′рат тʼӓ′рың: ′нӯлʼ′дʼелджик.</ta>
            <ta e="T397" id="Seg_3654" s="T393">ну и нӯлʼ′дʼе̄лтшлʼе ′ӱ̄бърак.</ta>
            <ta e="T402" id="Seg_3655" s="T397">па′jат ′понен, е′рат ′ма̄тkын ′jеван.</ta>
            <ta e="T406" id="Seg_3656" s="T402">па′jат ′понен ӓ̄′дыт ‵кӯгыр′саным.</ta>
            <ta e="T408" id="Seg_3657" s="T406">′кӯгыр кутши′лʼе ′ӱ̄бъраң.</ta>
            <ta e="T411" id="Seg_3658" s="T408">е′рат ′ма̄тkын матш′каң.</ta>
            <ta e="T417" id="Seg_3659" s="T411">а па′jа ′по̄нен кугор′тшиң и кугор′тшиң.</ta>
            <ta e="T423" id="Seg_3660" s="T417">а онт ‵тшагъмбе′ди ′пең(н̃)kын ва′дʼи аур′ның.</ta>
            <ta e="T428" id="Seg_3661" s="T423">′онт ′нӯн kой′мын ке′лʼлʼе ′та̄дърыт.</ta>
            <ta e="T431" id="Seg_3662" s="T428">′kамп(б)ай ′тʼе̄лат ′тʼӱмбоцʼка.</ta>
            <ta e="T436" id="Seg_3663" s="T431">е′рат ′ма̄тkын ′а̄мдыс, ′а̄мдыс, ′кертым‵ның.</ta>
            <ta e="T441" id="Seg_3664" s="T436">и ′те̄рбан: ман сей′лавым ′kwӓрлʼеу(w).</ta>
            <ta e="T446" id="Seg_3665" s="T441">сей′ла ′может быть ′меңга ′тʼӱлʼезеk (‵тӱ̄лʼе′зетты).</ta>
            <ta e="T453" id="Seg_3666" s="T446">ну нилʼ′дʼиң и ′меот, сʼӓй′ламд ′kвӓрлʼе ′ӱ̄бърат.</ta>
            <ta e="T457" id="Seg_3667" s="T453">′kайда ′пʼӧцʼин мо′kын ‵рахсым′бан.</ta>
            <ta e="T460" id="Seg_3668" s="T457">тап ′со̄цʼкаң ‵ӱңгел′джиң.</ta>
            <ta e="T465" id="Seg_3669" s="T460">′ондъ ′тʼе̄рбан: kай на′тʼен ‵раkсы′мынт.</ta>
            <ta e="T471" id="Seg_3670" s="T465">тап ай ′съдым′дже̨л тʼӓ′рың: сей′ла ро̄′пе̄сс!</ta>
            <ta e="T477" id="Seg_3671" s="T471">а ′пʼӧцʼин мо′kын вар′ɣың раk′соннең kай′да.</ta>
            <ta e="T485" id="Seg_3672" s="T477">е′рат на′тʼет тʼӱ̄ры′се ‵тʼӧнджи′каң kwа′нның, ′пӧцʼим мо′kың ′пӱндарым′пан.</ta>
            <ta e="T487" id="Seg_3673" s="T485">kо′ыт ты′ссам.</ta>
            <ta e="T493" id="Seg_3674" s="T487">о̄м′дың, на ты′ссым тʼӣ′kыт и ′пӱ̄ɣы(ӓ)лджим′бат.</ta>
            <ta e="T496" id="Seg_3675" s="T493">′kайда сът пӱ̄р′лашка.</ta>
            <ta e="T500" id="Seg_3676" s="T496">′тау однако ма′нани сей′ла.</ta>
            <ta e="T504" id="Seg_3677" s="T500">′пӧти ′ӱ̄дым kо′ɣыт е′рат.</ta>
            <ta e="T506" id="Seg_3678" s="T504">ӱ̄т′ты тʼӓп′тӓптыт.</ta>
            <ta e="T512" id="Seg_3679" s="T506">сейлат (сът сей) ку′шаң ′ӱ̄т′kын тʼӓп′тӓт(д)имбы′заттъ.</ta>
            <ta e="T514" id="Seg_3680" s="T512">са′жаң мӱзул′джит.</ta>
            <ta e="T518" id="Seg_3681" s="T514">сʼӓй′лаɣынд тʼӓ′рың: сʼӓй′ла ро′пе̄сс!</ta>
            <ta e="T521" id="Seg_3682" s="T518">сей′лат табы′нӓ пары′дʼӓттъ.</ta>
            <ta e="T527" id="Seg_3683" s="T521">ман′джедʼиң, а сей′лат ′со̄чкаң аз а̄′даттъ.</ta>
            <ta e="T532" id="Seg_3684" s="T527">сей′ламд паkkы′ныт, ай тʼӓп′тӓптыт ′со̄цʼкаң.</ta>
            <ta e="T536" id="Seg_3685" s="T532">сʼӓй′ламт ай ′со̄цʼкаң мӱ′сенныт.</ta>
            <ta e="T539" id="Seg_3686" s="T536">тʼӓ′рың: сей′ла, ро̄′пе̄сс!</ta>
            <ta e="T542" id="Seg_3687" s="T539">сей′лат пары′дʼӓттъ ′квессе.</ta>
            <ta e="T548" id="Seg_3688" s="T542">тибе′kум ′а̄нданың: ти′дам сей′лау(w) ′со̄цʼкаң конджер′наттъ.</ta>
            <ta e="T552" id="Seg_3689" s="T548">ты′дам ман сей′зе е̨′заң.</ta>
            <ta e="T558" id="Seg_3690" s="T552">у′жо ман ′таст сей′лаɣын ′тʼӓт kо̄′балакс.</ta>
            <ta e="T560" id="Seg_3691" s="T558">′поне̨ тша̄н′джиң.</ta>
            <ta e="T566" id="Seg_3692" s="T560">а па′jат ‵кугургут′тшиң, ну кой′мым ′та̄дърыт.</ta>
            <ta e="T574" id="Seg_3693" s="T566">е′рат тʼӓ′рың па′jандыне: а тан ма′зым kай′но ‵сейгыл′джант?</ta>
            <ta e="T588" id="Seg_3694" s="T574">а па′jат тʼӓ′рың: а тан kай′но нилʼ′дʼиң ‵палʼдʼику′зант, кӧцʼку′зандъ ′сӯруску, а о′нент сей′ландысе ′санджир‵(т)сант?</ta>
            <ta e="T593" id="Seg_3695" s="T588">а ′ма̄тт kаим′не̨й ас та̄тку′зал.</ta>
            <ta e="T601" id="Seg_3696" s="T593">вот ман та′ннани сей′ламт на′но и (‵паkkы′ннау) та′ннау.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_3697" s="T1">ilɨzaq warkɨzaq era pajasak.</ta>
            <ta e="T8" id="Seg_3698" s="T5">erat palʼdʼukuŋ suːrɨlʼe.</ta>
            <ta e="T11" id="Seg_3699" s="T8">pajat maːtqɨn jewan.</ta>
            <ta e="T16" id="Seg_3700" s="T11">erat palʼdʼikuŋ, palʼdʼikuŋ, tʼeːlʼdʼömp palʼdʼi(e)kuŋ.</ta>
            <ta e="T19" id="Seg_3701" s="T16">qaimnäj ass tatkut.</ta>
            <ta e="T25" id="Seg_3702" s="T19">kɨdaŋ niŋ palʼdʼikuŋ, qaimne as tatkut.</ta>
            <ta e="T35" id="Seg_3703" s="T25">okkɨr d̂ʼel pajat teːrban: qajno ʒə tap qaimnäj as tatkut?</ta>
            <ta e="T38" id="Seg_3704" s="T35">man tabɨm saralǯlʼew.</ta>
            <ta e="T42" id="Seg_3705" s="T38">qajno tap sündebɨŋ tükkuŋ?</ta>
            <ta e="T46" id="Seg_3706" s="T42">nu i nilʼdʼ(zʼ)iŋ meːwɨt.</ta>
            <ta e="T51" id="Seg_3707" s="T46">erat üːbɨcʼlʼe üːbɨraŋ suːrulʼe, qwannɨŋ.</ta>
            <ta e="T55" id="Seg_3708" s="T51">pajat tʼäːno tolʼdʼ(zʼ)imt(də) sernat.</ta>
            <ta e="T59" id="Seg_3709" s="T55">erand moqɨn tʼäno üːban.</ta>
            <ta e="T62" id="Seg_3710" s="T59">erandɨne az aːdolǯikuŋ.</ta>
            <ta e="T69" id="Seg_3711" s="T62">nano az aːdolǯikuŋ, tabɨm erat ik qonǯirnent.</ta>
            <ta e="T73" id="Seg_3712" s="T69">erat medɨŋ qɨn qɨnbart.</ta>
            <ta e="T80" id="Seg_3713" s="T73">tam plʼekaj qä(ɨ)n paːr(o)ɣɨn ont sʼäimd paqqɨlgut.</ta>
            <ta e="T85" id="Seg_3714" s="T80">sʼäilamd ädɨlʼe qwäč(cʼ)ikut pon molaɣɨ(o)n.</ta>
            <ta e="T89" id="Seg_3715" s="T85">taj plʼekaj qänt nʼäzolguŋ.</ta>
            <ta e="T93" id="Seg_3716" s="T89">ondə tʼärkuŋ: sʼäj, ropeːs.</ta>
            <ta e="T97" id="Seg_3717" s="T93">sʼäjla tabnä tʼüqwatt, paːrɨčiqwattə.</ta>
            <ta e="T107" id="Seg_3718" s="T97">aj natʼej qɨn paːroɣɨn seint paqqɨlʼe qwecʼikut, pon moːland ɨ(ä)tkut.</ta>
            <ta e="T110" id="Seg_3719" s="T107">tʼärkuŋ: sʼäjla roːpeːss!</ta>
            <ta e="T114" id="Seg_3720" s="T110">sʼäjla tabne tʼülʼe rokecʼqwatt.</ta>
            <ta e="T119" id="Seg_3721" s="T114">nilʼdʼä reda(ɨ)ŋ selɨj tʼeːldʼʒom pünnɨŋ.</ta>
            <ta e="T123" id="Seg_3722" s="T119">pajat mannɨmpɨs, mannɨmpɨs toːlakaŋ.</ta>
            <ta e="T126" id="Seg_3723" s="T123">erat seːimd paqqənnɨt.</ta>
            <ta e="T131" id="Seg_3724" s="T126">tam plʼekaj qän känbart qwädʼä(i)t.</ta>
            <ta e="T138" id="Seg_3725" s="T131">a pajat tʼäːno tabɨn sejlamd iːot toːlakaŋ.</ta>
            <ta e="T148" id="Seg_3726" s="T138">ontə teːrp(b̂)an: a tan nano maːttə qaimnej as tatqwal meŋga.</ta>
            <ta e="T155" id="Seg_3727" s="T148">nilʼdʼiŋ pajat qwannɨŋ, qwannɨŋ i tʼönǯikaŋ čaːǯiŋ.</ta>
            <ta e="T159" id="Seg_3728" s="T155">erantɨnan seiːt tidam tʼäŋwan.</ta>
            <ta e="T163" id="Seg_3729" s="T159">qwäcʼku tabɨm tab ürgenǯiŋ.</ta>
            <ta e="T168" id="Seg_3730" s="T163">nano i tʼönǯikaŋ čaːǯiŋ pajat.</ta>
            <ta e="T177" id="Seg_3731" s="T168">kušaŋ čaǯiŋ, aj moɣunä manǯedʼikuŋ, erat čaːǯiŋ alʼi ass.</ta>
            <ta e="T184" id="Seg_3732" s="T177">pajant qwalʼewlʼe erat kušaŋ laŋgɨniŋ: sʼäjla, ropeːss!</ta>
            <ta e="T192" id="Seg_3733" s="T184">sʼäjla tʼäŋgwattə, as tükwatte tabnä i ass parɨdʼ(cʼ)iqwattə.</ta>
            <ta e="T202" id="Seg_3734" s="T192">kušaŋ sʼäjlamt qwärɨstə, qwärɨst, sʼäila kundar tʼäŋgwattə, niŋi tʼäŋgwatt.</ta>
            <ta e="T208" id="Seg_3735" s="T202">tidam sejgalɨk erat na üːbedʼän, sʼäjgalɨk.</ta>
            <ta e="T213" id="Seg_3736" s="T208">pajat aːdelǯiŋ, erandɨne az aːdelǯikuŋ.</ta>
            <ta e="T217" id="Seg_3737" s="T213">erat sejgalɨk ürgɨle qunǯiŋ.</ta>
            <ta e="T221" id="Seg_3738" s="T217">madɨn köndə miglʼe üːbəraq.</ta>
            <ta e="T223" id="Seg_3739" s="T221">pajat tɨdəmb̂an.</ta>
            <ta e="T228" id="Seg_3740" s="T223">tʼäːno medɨdʼeŋ, tolʼdʼilamd niŋel čaɣannɨ(a)t.</ta>
            <ta e="T231" id="Seg_3741" s="T228">mattə seːrnɨŋ, nʼäɣɨniŋ.</ta>
            <ta e="T238" id="Seg_3742" s="T231">erant sejlam tɨs(s)ont pennɨt, pʼötʼ(cʼ)en moqtə oːcʼiŋnɨt.</ta>
            <ta e="T243" id="Seg_3743" s="T238">erat tʼönǯikaŋ tüan maːttə, sernaŋ.</ta>
            <ta e="T251" id="Seg_3744" s="T243">pajat tʼärɨŋ: a tan qajno tamdʼel kundɨŋ jewant?</ta>
            <ta e="T260" id="Seg_3745" s="T251">erat tʼärɨŋ: man tamdʼel sejowɨm aːtsaŋ, nano kundɨŋ jewaŋ.</ta>
            <ta e="T272" id="Seg_3746" s="T260">pajat teːrban: a tan siːrant, man tidam tastɨ tunuaŋ, kundar tan suːrunʼänt.</ta>
            <ta e="T276" id="Seg_3747" s="T272">selɨj tʼelʼdʼzʼomp sejlandɨse warkand.</ta>
            <ta e="T286" id="Seg_3748" s="T276">aw qɨnb̂aːrt ɨtqwäl, a tam plʼekaj kɨnb̂aːrt neːzolgwant.</ta>
            <ta e="T290" id="Seg_3749" s="T286">sejlal tʼeŋga tʼülʼe parɨcʼikwattə.</ta>
            <ta e="T295" id="Seg_3750" s="T290">nilʼdʼiŋ tan i sündebɨŋ tükkuzant.</ta>
            <ta e="T299" id="Seg_3751" s="T295">nu i warkɨlʼe üːbəraq.</ta>
            <ta e="T303" id="Seg_3752" s="T299">kušaj da d̂ʼeːlɨt mendɨŋ.</ta>
            <ta e="T309" id="Seg_3753" s="T303">pajat manǯediŋ südernaun, manǯediŋ, nɨŋan peŋ(n)k.</ta>
            <ta e="T314" id="Seg_3754" s="T309">pajat erandɨne tʼärɨŋ: peŋkə nɨŋgɨnt.</ta>
            <ta e="T319" id="Seg_3755" s="T314">tan wazlʼent kundarem tʼäʒ(i)lʼel peŋkəm.</ta>
            <ta e="T325" id="Seg_3756" s="T319">a erat tʼärɨŋ: man kundar tʼäǯenǯau?</ta>
            <ta e="T328" id="Seg_3757" s="T325">sejew az aːduŋ.</ta>
            <ta e="T336" id="Seg_3758" s="T328">pajat tʼärɨŋ: tüqɨ meŋga, man tulsem teŋga oralǯolǯilʼews.</ta>
            <ta e="T340" id="Seg_3759" s="T336">i erat tʼüan pajandɨne.</ta>
            <ta e="T346" id="Seg_3760" s="T340">pajat erandɨne oralǯolǯit udoɣɨnt apstɨmbɨdi tʼülsem.</ta>
            <ta e="T352" id="Seg_3761" s="T346">ond tabne pɨmʒɨrɨt, kundar tʼätku peŋgɨm.</ta>
            <ta e="T356" id="Seg_3762" s="T352">erat tʼäʒit, qwannɨt peŋgɨm.</ta>
            <ta e="T364" id="Seg_3763" s="T356">erand sej az aːduŋ, peŋgɨm tabne as kɨrgu.</ta>
            <ta e="T369" id="Seg_3764" s="T364">pajat ondə kɨrlʼe üːbərɨt peŋgɨm.</ta>
            <ta e="T373" id="Seg_3765" s="T369">nu i kɨrɨt peŋgɨm.</ta>
            <ta e="T376" id="Seg_3766" s="T373">wadʼilam nʼörbannɨt, ɨː(äː)dɨt.</ta>
            <ta e="T381" id="Seg_3767" s="T376">okkɨr dʼel wadʼilam kocʼin ponnɨt.</ta>
            <ta e="T390" id="Seg_3768" s="T381">erandɨne tʼärɨŋ: tamdʼel qula tʼarattə, tamdʼelʼe dʼel iːbkaj nuːlʼd̂ʼ(tʼ)el.</ta>
            <ta e="T393" id="Seg_3769" s="T390">erat tʼärɨŋ: nuːlʼdʼelǯik.</ta>
            <ta e="T397" id="Seg_3770" s="T393">nu i nuːlʼdʼeːlčlʼe üːbərak.</ta>
            <ta e="T402" id="Seg_3771" s="T397">pajat ponen, erat maːtqɨn jewan.</ta>
            <ta e="T406" id="Seg_3772" s="T402">pajat ponen äːdɨt kuːgɨrsanɨm.</ta>
            <ta e="T408" id="Seg_3773" s="T406">kuːgɨr kučilʼe üːbəraŋ.</ta>
            <ta e="T411" id="Seg_3774" s="T408">erat maːtqɨn matškaŋ.</ta>
            <ta e="T417" id="Seg_3775" s="T411">a paja poːnen kugorčiŋ i kugorčiŋ.</ta>
            <ta e="T423" id="Seg_3776" s="T417">a ont čagəmbedi peŋ(n)qɨn wadʼi aurnɨŋ.</ta>
            <ta e="T428" id="Seg_3777" s="T423">ont nuːn qojmɨn kelʼlʼe taːdərɨt.</ta>
            <ta e="T431" id="Seg_3778" s="T428">qamp(b)aj tʼeːlat tʼümbocʼka.</ta>
            <ta e="T436" id="Seg_3779" s="T431">erat maːtqɨn aːmdɨs, aːmdɨs, kertɨmnɨŋ.</ta>
            <ta e="T441" id="Seg_3780" s="T436">i teːrban: man sejlawɨm qwärlʼeu(w).</ta>
            <ta e="T446" id="Seg_3781" s="T441">sejla moʒet bɨtʼ meŋga tʼülʼezeq (tüːlʼezettɨ).</ta>
            <ta e="T453" id="Seg_3782" s="T446">nu nilʼdʼiŋ i meot, sʼäjlamd qwärlʼe üːbərat.</ta>
            <ta e="T457" id="Seg_3783" s="T453">qajda pʼöcʼin moqɨn raxsɨmban.</ta>
            <ta e="T460" id="Seg_3784" s="T457">tap soːcʼkaŋ üŋgelǯiŋ.</ta>
            <ta e="T465" id="Seg_3785" s="T460">ondə tʼeːrban: qaj natʼen raqsɨmɨnt.</ta>
            <ta e="T471" id="Seg_3786" s="T465">tap aj sədɨmǯel tʼärɨŋ: sejla roːpeːss!</ta>
            <ta e="T477" id="Seg_3787" s="T471">a pʼöcʼin moqɨn warɣɨŋ raqsonneŋ qajda.</ta>
            <ta e="T485" id="Seg_3788" s="T477">erat natʼet tʼüːrɨse tʼönǯikaŋ qwannɨŋ, pöcʼim moqɨŋ pündarɨmpan.</ta>
            <ta e="T487" id="Seg_3789" s="T485">qoɨt tɨssam.</ta>
            <ta e="T493" id="Seg_3790" s="T487">oːmdɨŋ, na tɨssɨm tʼiːqɨt i püːɣɨ(ä)lǯimbat.</ta>
            <ta e="T496" id="Seg_3791" s="T493">qajda sət püːrlaška.</ta>
            <ta e="T500" id="Seg_3792" s="T496">tau odnako manani sejla.</ta>
            <ta e="T504" id="Seg_3793" s="T500">pöti üːdɨm qoɣɨt erat.</ta>
            <ta e="T506" id="Seg_3794" s="T504">üːttɨ tʼäptäptɨt.</ta>
            <ta e="T512" id="Seg_3795" s="T506">sejlat (sət sej) kušaŋ üːtqɨn tʼäptät(d)imbɨzattə.</ta>
            <ta e="T514" id="Seg_3796" s="T512">saʒaŋ müzulǯit.</ta>
            <ta e="T518" id="Seg_3797" s="T514">sʼäjlaɣɨnd tʼärɨŋ: sʼäjla ropeːss!</ta>
            <ta e="T521" id="Seg_3798" s="T518">sejlat tabɨnä parɨdʼättə.</ta>
            <ta e="T527" id="Seg_3799" s="T521">manǯedʼiŋ, a sejlat soːčʼkaŋ az aːdattə.</ta>
            <ta e="T532" id="Seg_3800" s="T527">sejlamd paqqɨnɨt, aj tʼäptäptɨt soːcʼkaŋ.</ta>
            <ta e="T536" id="Seg_3801" s="T532">sʼäjlamt aj soːcʼkaŋ müsennɨt.</ta>
            <ta e="T539" id="Seg_3802" s="T536">tʼärɨŋ: sejla, roːpeːss!</ta>
            <ta e="T542" id="Seg_3803" s="T539">sejlat parɨdʼättə kwesse.</ta>
            <ta e="T548" id="Seg_3804" s="T542">tibequm aːndanɨŋ: tidam sejlau(w) soːcʼkaŋ konǯernattə.</ta>
            <ta e="T552" id="Seg_3805" s="T548">tɨdam man sejze ezaŋ.</ta>
            <ta e="T558" id="Seg_3806" s="T552">uʒo man tast sejlaɣɨn tʼät qoːbalaks.</ta>
            <ta e="T560" id="Seg_3807" s="T558">pone čaːnǯiŋ.</ta>
            <ta e="T566" id="Seg_3808" s="T560">a pajat kugurgutčiŋ, nu kojmɨm taːdərɨt.</ta>
            <ta e="T574" id="Seg_3809" s="T566">erat tʼärɨŋ pajandɨne: a tan mazɨm qajno sejgɨlǯant?</ta>
            <ta e="T588" id="Seg_3810" s="T574">a pajat tʼärɨŋ: a tan qajno nilʼdʼiŋ palʼdʼikuzant, köcʼkuzandə suːrusku, a onent sejlandɨse sanǯir(t)sant?</ta>
            <ta e="T593" id="Seg_3811" s="T588">a maːtt qaimnej as taːtkuzal.</ta>
            <ta e="T601" id="Seg_3812" s="T593">wot man tannani sejlamt nano i (paqqɨnnau) tannau.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_3813" s="T1">Ilɨzaq warkɨzaq era pajasak. </ta>
            <ta e="T8" id="Seg_3814" s="T5">Erat palʼdʼukuŋ suːrɨlʼe. </ta>
            <ta e="T11" id="Seg_3815" s="T8">Pajat maːtqɨn jewan. </ta>
            <ta e="T16" id="Seg_3816" s="T11">Erat palʼdʼikuŋ, palʼdʼikuŋ, tʼeːlʼdʼömp palʼdʼikuŋ. </ta>
            <ta e="T19" id="Seg_3817" s="T16">Qaimnäj ass tatkut. </ta>
            <ta e="T25" id="Seg_3818" s="T19">Kɨdaŋ niŋ palʼdʼikuŋ, qaimne as tatkut. </ta>
            <ta e="T35" id="Seg_3819" s="T25">Okkɨr dʼel pajat teːrban: Qajno ʒə tap qaimnäj as tatkut? </ta>
            <ta e="T38" id="Seg_3820" s="T35">Man tabɨm saralǯlʼew. </ta>
            <ta e="T42" id="Seg_3821" s="T38">Qajno tap sündebɨŋ tükkuŋ?” </ta>
            <ta e="T46" id="Seg_3822" s="T42">Nu i nilʼdʼiŋ meːwɨt. </ta>
            <ta e="T51" id="Seg_3823" s="T46">Erat üːbɨcʼlʼe üːbɨraŋ suːrulʼe, qwannɨŋ. </ta>
            <ta e="T55" id="Seg_3824" s="T51">Pajat tʼäːno tolʼdʼimt sernat. </ta>
            <ta e="T59" id="Seg_3825" s="T55">Erand moqɨn tʼäno üːban. </ta>
            <ta e="T62" id="Seg_3826" s="T59">Erandɨne az aːdolǯikuŋ. </ta>
            <ta e="T69" id="Seg_3827" s="T62">Nano az aːdolǯikuŋ, tabɨm erat ik qonǯirnent. </ta>
            <ta e="T73" id="Seg_3828" s="T69">Erat medɨŋ Qɨn qɨnbart. </ta>
            <ta e="T80" id="Seg_3829" s="T73">Tam plʼekaj qän paːrɣɨn ont sʼäimd paqqɨlgut. </ta>
            <ta e="T85" id="Seg_3830" s="T80">Sʼäilamd ädɨlʼe qwäčikut pon molaɣɨn. </ta>
            <ta e="T89" id="Seg_3831" s="T85">Taj plʼekaj qänt nʼäzolguŋ. </ta>
            <ta e="T93" id="Seg_3832" s="T89">Ondə tʼärkuŋ: “Sʼäj, ropeːs.” </ta>
            <ta e="T97" id="Seg_3833" s="T93">Sʼäjla tabnä tʼüqwatt, paːrɨčiqwattə. </ta>
            <ta e="T107" id="Seg_3834" s="T97">Aj natʼej qɨn paːroɣɨn seint paqqɨlʼe qwecʼikut, pon moːland ɨtkut. </ta>
            <ta e="T110" id="Seg_3835" s="T107">Tʼärkuŋ: sʼäjla roːpeːss! </ta>
            <ta e="T114" id="Seg_3836" s="T110">Sʼäjla tabne tʼülʼe rokecʼqwatt. </ta>
            <ta e="T119" id="Seg_3837" s="T114">Nilʼdʼä redaŋ selɨj tʼeːldʼʒom pünnɨŋ. </ta>
            <ta e="T123" id="Seg_3838" s="T119">Pajat mannɨmpɨs, mannɨmpɨs toːlakaŋ. </ta>
            <ta e="T126" id="Seg_3839" s="T123">Erat seːimd paqqənnɨt. </ta>
            <ta e="T131" id="Seg_3840" s="T126">Tam plʼekaj qän känbart qwädʼät. </ta>
            <ta e="T138" id="Seg_3841" s="T131">A pajat tʼäːno tabɨn sejlamd iːot toːlakaŋ. </ta>
            <ta e="T148" id="Seg_3842" s="T138">Ontə teːrpan: “A tan nano maːttə qaimnej as tatqwal meŋga.” </ta>
            <ta e="T155" id="Seg_3843" s="T148">Nilʼdʼiŋ pajat qwannɨŋ, qwannɨŋ i tʼönǯikaŋ čaːǯiŋ. </ta>
            <ta e="T159" id="Seg_3844" s="T155">Erantɨnan seiːt tidam tʼäŋwan. </ta>
            <ta e="T163" id="Seg_3845" s="T159">Qwäcʼku tabɨm tab ürgenǯiŋ. </ta>
            <ta e="T168" id="Seg_3846" s="T163">Nano i tʼönǯikaŋ čaːǯiŋ pajat. </ta>
            <ta e="T177" id="Seg_3847" s="T168">Kušaŋ čaǯiŋ, aj moɣunä manǯedʼikuŋ, erat čaːǯiŋ alʼi ass. </ta>
            <ta e="T184" id="Seg_3848" s="T177">Pajant qwalʼewlʼe erat kušaŋ laŋgɨniŋ: “Sʼäjla, ropeːss!” </ta>
            <ta e="T192" id="Seg_3849" s="T184">Sʼäjla tʼäŋgwattə, as tükwatte tabnä i ass parɨdʼiqwattə. </ta>
            <ta e="T202" id="Seg_3850" s="T192">Kušaŋ sʼäjlamt qwärɨstə, qwärɨst, sʼäila kundar tʼäŋgwattə, niŋ i tʼäŋgwatt. </ta>
            <ta e="T208" id="Seg_3851" s="T202">Tidam sejgalɨk erat na üːbedʼän, sʼäjgalɨk. </ta>
            <ta e="T213" id="Seg_3852" s="T208">Pajat aːdelǯiŋ, erandɨne az aːdelǯikuŋ. </ta>
            <ta e="T217" id="Seg_3853" s="T213">Erat sejgalɨk ürgɨle qunǯiŋ. </ta>
            <ta e="T221" id="Seg_3854" s="T217">Madɨn köndə miglʼe üːbəraq. </ta>
            <ta e="T223" id="Seg_3855" s="T221">Pajat tɨdəmban. </ta>
            <ta e="T228" id="Seg_3856" s="T223">Tʼäːno medɨdʼeŋ, tolʼdʼilamd niŋel čaɣannɨt. </ta>
            <ta e="T231" id="Seg_3857" s="T228">Mattə seːrnɨŋ, nʼäɣɨniŋ. </ta>
            <ta e="T238" id="Seg_3858" s="T231">Erant sejlam tɨsont pennɨt, pʼötʼen moqtə oːcʼiŋnɨt. </ta>
            <ta e="T243" id="Seg_3859" s="T238">Erat tʼönǯikaŋ tüan maːttə, sernaŋ. </ta>
            <ta e="T251" id="Seg_3860" s="T243">Pajat tʼärɨŋ: “A tan qajno tamdʼel kundɨŋ jewant?” </ta>
            <ta e="T260" id="Seg_3861" s="T251">Erat tʼärɨŋ: “Man tamdʼel sejowɨm aːtsaŋ, nano kundɨŋ jewaŋ.” </ta>
            <ta e="T272" id="Seg_3862" s="T260">Pajat teːrban: “A tan siːrant, man tidam tastɨ tunuaŋ, kundar tan suːrunʼänt. </ta>
            <ta e="T276" id="Seg_3863" s="T272">Selɨj tʼelʼdʼzʼomp sejlandɨse warkand. </ta>
            <ta e="T286" id="Seg_3864" s="T276">Aw qɨn baːrt ɨtqwäl, a tam plʼekaj kɨn baːrt neːzolgwant. </ta>
            <ta e="T290" id="Seg_3865" s="T286">Sejlal tʼeŋga tʼülʼe parɨcʼikwattə. </ta>
            <ta e="T295" id="Seg_3866" s="T290">Nilʼdʼiŋ tan i sündebɨŋ tükkuzant.” </ta>
            <ta e="T299" id="Seg_3867" s="T295">Nu i warkɨlʼe üːbəraq. </ta>
            <ta e="T303" id="Seg_3868" s="T299">Kušaj da dʼeːlɨt mendɨŋ. </ta>
            <ta e="T309" id="Seg_3869" s="T303">Pajat manǯediŋ südernaun, manǯediŋ, nɨŋan peŋk. </ta>
            <ta e="T314" id="Seg_3870" s="T309">Pajat erandɨne tʼärɨŋ: “Peŋkə nɨŋgɨnt. </ta>
            <ta e="T319" id="Seg_3871" s="T314">Tan wazlʼent kundarem tʼäʒlʼel peŋkəm.” </ta>
            <ta e="T325" id="Seg_3872" s="T319">A erat tʼärɨŋ: “Man kundar tʼäǯenǯau? </ta>
            <ta e="T328" id="Seg_3873" s="T325">Sejew az aːduŋ.” </ta>
            <ta e="T336" id="Seg_3874" s="T328">Pajat tʼärɨŋ: “Tüqɨ meŋga, man tulsem teŋga oralǯolǯilʼews.” </ta>
            <ta e="T340" id="Seg_3875" s="T336">I erat tʼüan pajandɨne. </ta>
            <ta e="T346" id="Seg_3876" s="T340">Pajat erandɨne oralǯolǯit udoɣɨnt apstɨmbɨdi tʼülsem. </ta>
            <ta e="T352" id="Seg_3877" s="T346">Ond tabne pɨmʒɨrɨt, kundar tʼätku peŋgɨm. </ta>
            <ta e="T356" id="Seg_3878" s="T352">Erat tʼäʒit, qwannɨt peŋgɨm. </ta>
            <ta e="T364" id="Seg_3879" s="T356">Erand sej az aːduŋ, peŋgɨm tabne as kɨrgu. </ta>
            <ta e="T369" id="Seg_3880" s="T364">Pajat ondə kɨrlʼe üːbərɨt peŋgɨm. </ta>
            <ta e="T373" id="Seg_3881" s="T369">Nu i kɨrɨt peŋgɨm. </ta>
            <ta e="T376" id="Seg_3882" s="T373">Wadʼilam nʼörbannɨt, ɨːdɨt. </ta>
            <ta e="T381" id="Seg_3883" s="T376">Okkɨr dʼel wadʼilam kocʼin ponnɨt. </ta>
            <ta e="T390" id="Seg_3884" s="T381">Erandɨne tʼärɨŋ: “Tamdʼel qula tʼarattə, tamdʼelʼe dʼel iːbkaj nuːlʼdʼel.” </ta>
            <ta e="T393" id="Seg_3885" s="T390">Erat tʼärɨŋ: “Nuːlʼdʼelǯik.” </ta>
            <ta e="T397" id="Seg_3886" s="T393">Nu i nuːlʼdʼeːlčlʼe üːbərak. </ta>
            <ta e="T402" id="Seg_3887" s="T397">Pajat ponen, erat maːtqɨn jewan. </ta>
            <ta e="T406" id="Seg_3888" s="T402">Pajat ponen äːdɨt kuːgɨrsanɨm. </ta>
            <ta e="T408" id="Seg_3889" s="T406">Kuːgɨrkučilʼe üːbəraŋ. </ta>
            <ta e="T411" id="Seg_3890" s="T408">Erat maːtqɨn matškaŋ. </ta>
            <ta e="T417" id="Seg_3891" s="T411">A paja poːnen kugorčiŋ i kugorčiŋ. </ta>
            <ta e="T423" id="Seg_3892" s="T417">A ont čagəmbedi peŋqɨn wadʼi aurnɨŋ. </ta>
            <ta e="T428" id="Seg_3893" s="T423">Ont nuːn qojmɨn kelʼlʼe taːdərɨt. </ta>
            <ta e="T431" id="Seg_3894" s="T428">Qampaj tʼeːlat tʼümbocʼka. </ta>
            <ta e="T436" id="Seg_3895" s="T431">Erat maːtqɨn aːmdɨs, aːmdɨs, kertɨmnɨŋ. </ta>
            <ta e="T441" id="Seg_3896" s="T436">I teːrban: “Man sejlawɨm qwärlʼeu. </ta>
            <ta e="T446" id="Seg_3897" s="T441">Sejla moʒetbɨtʼ meŋga tʼülʼezeq (tüːlʼezettɨ).” </ta>
            <ta e="T453" id="Seg_3898" s="T446">Nu nilʼdʼiŋ i meot, sʼäjlamd qwärlʼe üːbərat. </ta>
            <ta e="T457" id="Seg_3899" s="T453">Qajda pʼöcʼin moqɨn raxsɨmban. </ta>
            <ta e="T460" id="Seg_3900" s="T457">Tap soːcʼkaŋ üŋgelǯiŋ. </ta>
            <ta e="T465" id="Seg_3901" s="T460">Ondə tʼeːrban: “Qaj natʼen raqsɨmɨnt?” </ta>
            <ta e="T471" id="Seg_3902" s="T465">Tap aj sədɨmǯel tʼärɨŋ: “Sejla roːpeːss!” </ta>
            <ta e="T477" id="Seg_3903" s="T471">A pʼöcʼin moqɨn warɣɨŋ raqsonneŋ qajda. </ta>
            <ta e="T485" id="Seg_3904" s="T477">Erat natʼet tʼüːrɨse tʼönǯikaŋ qwannɨŋ, pöcʼim moqɨŋ pündarɨmpan. </ta>
            <ta e="T487" id="Seg_3905" s="T485">Qoɨt tɨssam. </ta>
            <ta e="T493" id="Seg_3906" s="T487">Oːmdɨŋ, na tɨssɨm tʼiːqɨt i püːɣɨlǯimbat. </ta>
            <ta e="T496" id="Seg_3907" s="T493">Qajda sət püːrlaška. </ta>
            <ta e="T500" id="Seg_3908" s="T496">“Tau odnako manani sejla.” </ta>
            <ta e="T504" id="Seg_3909" s="T500">Pöti üːdɨm qoɣɨt erat. </ta>
            <ta e="T506" id="Seg_3910" s="T504">Üːttɨ tʼäptäptɨt. </ta>
            <ta e="T512" id="Seg_3911" s="T506">Sejlat (sət sej) kušaŋ üːtqɨn tʼäptädimbɨzattə. </ta>
            <ta e="T514" id="Seg_3912" s="T512">Saʒaŋ müzulǯit. </ta>
            <ta e="T518" id="Seg_3913" s="T514">Sʼäjlaɣɨnd tʼärɨŋ: “Sʼäjla ropeːss!” </ta>
            <ta e="T521" id="Seg_3914" s="T518">Sejlat tabɨnä parɨdʼättə. </ta>
            <ta e="T527" id="Seg_3915" s="T521">Manǯedʼiŋ, a sejlat soːčʼkaŋ az aːdattə. </ta>
            <ta e="T532" id="Seg_3916" s="T527">Sejlamd paqqɨnɨt, aj tʼäptäptɨt soːcʼkaŋ. </ta>
            <ta e="T536" id="Seg_3917" s="T532">Sʼäjlamt aj soːcʼkaŋ müsennɨt. </ta>
            <ta e="T539" id="Seg_3918" s="T536">Tʼärɨŋ: “Sejla, roːpeːss!” </ta>
            <ta e="T542" id="Seg_3919" s="T539">Sejlat parɨdʼättə kwesse. </ta>
            <ta e="T548" id="Seg_3920" s="T542">Tibequm aːndanɨŋ: “Tidam sejlau soːcʼkaŋ konǯernattə. </ta>
            <ta e="T552" id="Seg_3921" s="T548">Tɨdam man sejze ezaŋ. </ta>
            <ta e="T558" id="Seg_3922" s="T552">Uʒo man tast sejlaɣɨn tʼät qoːbalaks.” </ta>
            <ta e="T560" id="Seg_3923" s="T558">Pone čaːnǯiŋ. </ta>
            <ta e="T566" id="Seg_3924" s="T560">A pajat kugurgutčiŋ, nu kojmɨm taːdərɨt. </ta>
            <ta e="T574" id="Seg_3925" s="T566">Erat tʼärɨŋ pajandɨne: “A tan mazɨm qajno sejgɨlǯant?” </ta>
            <ta e="T588" id="Seg_3926" s="T574">A pajat tʼärɨŋ: “A tan qajno nilʼdʼiŋ palʼdʼikuzant, köcʼkuzandə suːrusku, a onent sejlandɨse sanǯirsant? </ta>
            <ta e="T593" id="Seg_3927" s="T588">A maːtt qaimnej as taːtkuzal. </ta>
            <ta e="T601" id="Seg_3928" s="T593">Wot man tannani sejlamt nano i (paqqɨnnau) tannau.” </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_3929" s="T1">ilɨ-za-q</ta>
            <ta e="T3" id="Seg_3930" s="T2">warkɨ-za-q</ta>
            <ta e="T4" id="Seg_3931" s="T3">era</ta>
            <ta e="T5" id="Seg_3932" s="T4">paja-sa-k</ta>
            <ta e="T6" id="Seg_3933" s="T5">era-t</ta>
            <ta e="T7" id="Seg_3934" s="T6">palʼdʼu-ku-ŋ</ta>
            <ta e="T8" id="Seg_3935" s="T7">suːrɨ-lʼe</ta>
            <ta e="T9" id="Seg_3936" s="T8">paja-t</ta>
            <ta e="T10" id="Seg_3937" s="T9">maːt-qɨn</ta>
            <ta e="T11" id="Seg_3938" s="T10">je-wa-n</ta>
            <ta e="T12" id="Seg_3939" s="T11">era-t</ta>
            <ta e="T13" id="Seg_3940" s="T12">palʼdʼi-ku-ŋ</ta>
            <ta e="T14" id="Seg_3941" s="T13">palʼdʼi-ku-ŋ</ta>
            <ta e="T15" id="Seg_3942" s="T14">tʼeːlʼ-dʼömp</ta>
            <ta e="T16" id="Seg_3943" s="T15">palʼdʼi-ku-ŋ</ta>
            <ta e="T17" id="Seg_3944" s="T16">qai-m-näj</ta>
            <ta e="T18" id="Seg_3945" s="T17">ass</ta>
            <ta e="T19" id="Seg_3946" s="T18">tat-ku-t</ta>
            <ta e="T20" id="Seg_3947" s="T19">kɨdaŋ</ta>
            <ta e="T21" id="Seg_3948" s="T20">niŋ</ta>
            <ta e="T22" id="Seg_3949" s="T21">palʼdʼi-ku-ŋ</ta>
            <ta e="T23" id="Seg_3950" s="T22">qai-m-ne</ta>
            <ta e="T24" id="Seg_3951" s="T23">as</ta>
            <ta e="T25" id="Seg_3952" s="T24">tat-ku-t</ta>
            <ta e="T26" id="Seg_3953" s="T25">okkɨr</ta>
            <ta e="T27" id="Seg_3954" s="T26">dʼel</ta>
            <ta e="T28" id="Seg_3955" s="T27">paja-t</ta>
            <ta e="T29" id="Seg_3956" s="T28">teːrba-n</ta>
            <ta e="T30" id="Seg_3957" s="T29">qaj-no</ta>
            <ta e="T31" id="Seg_3958" s="T30">ʒə</ta>
            <ta e="T32" id="Seg_3959" s="T31">tap</ta>
            <ta e="T33" id="Seg_3960" s="T32">qai-m-näj</ta>
            <ta e="T34" id="Seg_3961" s="T33">as</ta>
            <ta e="T35" id="Seg_3962" s="T34">tat-ku-t</ta>
            <ta e="T36" id="Seg_3963" s="T35">man</ta>
            <ta e="T37" id="Seg_3964" s="T36">tab-ǝ-m</ta>
            <ta e="T38" id="Seg_3965" s="T37">saralǯ-lʼe-w</ta>
            <ta e="T39" id="Seg_3966" s="T38">qaj-no</ta>
            <ta e="T40" id="Seg_3967" s="T39">tap</ta>
            <ta e="T41" id="Seg_3968" s="T40">sünde-bɨ-ŋ</ta>
            <ta e="T42" id="Seg_3969" s="T41">tü-kku-ŋ</ta>
            <ta e="T43" id="Seg_3970" s="T42">nu</ta>
            <ta e="T44" id="Seg_3971" s="T43">i</ta>
            <ta e="T45" id="Seg_3972" s="T44">nilʼdʼi-ŋ</ta>
            <ta e="T46" id="Seg_3973" s="T45">meː-wɨ-t</ta>
            <ta e="T47" id="Seg_3974" s="T46">era-t</ta>
            <ta e="T48" id="Seg_3975" s="T47">üːbɨcʼ-lʼe</ta>
            <ta e="T49" id="Seg_3976" s="T48">üːbɨ-r-a-ŋ</ta>
            <ta e="T50" id="Seg_3977" s="T49">suːru-lʼe</ta>
            <ta e="T51" id="Seg_3978" s="T50">qwan-nɨ-ŋ</ta>
            <ta e="T52" id="Seg_3979" s="T51">paja-t</ta>
            <ta e="T53" id="Seg_3980" s="T52">tʼäːno</ta>
            <ta e="T54" id="Seg_3981" s="T53">tolʼdʼ-i-m-t</ta>
            <ta e="T55" id="Seg_3982" s="T54">ser-na-t</ta>
            <ta e="T56" id="Seg_3983" s="T55">era-n-d</ta>
            <ta e="T57" id="Seg_3984" s="T56">moqɨn</ta>
            <ta e="T58" id="Seg_3985" s="T57">tʼäno</ta>
            <ta e="T59" id="Seg_3986" s="T58">üːba-n</ta>
            <ta e="T60" id="Seg_3987" s="T59">era-ndɨ-ne</ta>
            <ta e="T61" id="Seg_3988" s="T60">az</ta>
            <ta e="T62" id="Seg_3989" s="T61">aːdo-lǯi-ku-ŋ</ta>
            <ta e="T63" id="Seg_3990" s="T62">nano</ta>
            <ta e="T64" id="Seg_3991" s="T63">az</ta>
            <ta e="T65" id="Seg_3992" s="T64">aːdo-lǯi-ku-ŋ</ta>
            <ta e="T66" id="Seg_3993" s="T65">tab-ǝ-m</ta>
            <ta e="T67" id="Seg_3994" s="T66">era-t</ta>
            <ta e="T68" id="Seg_3995" s="T67">ik</ta>
            <ta e="T69" id="Seg_3996" s="T68">qo-nǯir-ne-nt</ta>
            <ta e="T70" id="Seg_3997" s="T69">era-t</ta>
            <ta e="T71" id="Seg_3998" s="T70">medɨ-ŋ</ta>
            <ta e="T72" id="Seg_3999" s="T71">Qɨ-n</ta>
            <ta e="T73" id="Seg_4000" s="T72">qɨ-n-bar-t</ta>
            <ta e="T74" id="Seg_4001" s="T73">tam</ta>
            <ta e="T75" id="Seg_4002" s="T74">plʼeka-j</ta>
            <ta e="T76" id="Seg_4003" s="T75">qä-n</ta>
            <ta e="T77" id="Seg_4004" s="T76">paːr-ɣɨn</ta>
            <ta e="T78" id="Seg_4005" s="T77">ont</ta>
            <ta e="T79" id="Seg_4006" s="T78">sʼäi-m-d</ta>
            <ta e="T80" id="Seg_4007" s="T79">paq-qɨl-gu-t</ta>
            <ta e="T81" id="Seg_4008" s="T80">sʼäi-la-m-d</ta>
            <ta e="T82" id="Seg_4009" s="T81">ädɨ-lʼe</ta>
            <ta e="T83" id="Seg_4010" s="T82">qwäči-ku-t</ta>
            <ta e="T84" id="Seg_4011" s="T83">po-n</ta>
            <ta e="T85" id="Seg_4012" s="T84">mo-la-ɣɨn</ta>
            <ta e="T86" id="Seg_4013" s="T85">taj</ta>
            <ta e="T87" id="Seg_4014" s="T86">plʼeka-j</ta>
            <ta e="T88" id="Seg_4015" s="T87">qä-nt</ta>
            <ta e="T89" id="Seg_4016" s="T88">nʼäz-ol-gu-ŋ</ta>
            <ta e="T90" id="Seg_4017" s="T89">ondə</ta>
            <ta e="T91" id="Seg_4018" s="T90">tʼär-ku-ŋ</ta>
            <ta e="T92" id="Seg_4019" s="T91">sʼäj</ta>
            <ta e="T93" id="Seg_4020" s="T92">ro-peː-s</ta>
            <ta e="T94" id="Seg_4021" s="T93">sʼäj-la</ta>
            <ta e="T95" id="Seg_4022" s="T94">tab-nä</ta>
            <ta e="T96" id="Seg_4023" s="T95">tʼü-q-wa-tt</ta>
            <ta e="T97" id="Seg_4024" s="T96">paːrɨči-q-wa-ttə</ta>
            <ta e="T98" id="Seg_4025" s="T97">aj</ta>
            <ta e="T99" id="Seg_4026" s="T98">natʼe-j</ta>
            <ta e="T100" id="Seg_4027" s="T99">qɨ-n</ta>
            <ta e="T101" id="Seg_4028" s="T100">paːr-o-ɣɨn</ta>
            <ta e="T102" id="Seg_4029" s="T101">sei-n-t</ta>
            <ta e="T103" id="Seg_4030" s="T102">paq-qɨ-lʼe</ta>
            <ta e="T104" id="Seg_4031" s="T103">qwecʼi-ku-t</ta>
            <ta e="T105" id="Seg_4032" s="T104">po-n</ta>
            <ta e="T106" id="Seg_4033" s="T105">moː-la-nd</ta>
            <ta e="T107" id="Seg_4034" s="T106">ɨt-ku-t</ta>
            <ta e="T108" id="Seg_4035" s="T107">tʼär-ku-ŋ</ta>
            <ta e="T109" id="Seg_4036" s="T108">sʼäj-la</ta>
            <ta e="T110" id="Seg_4037" s="T109">roː-peː-ss</ta>
            <ta e="T111" id="Seg_4038" s="T110">sʼäj-la</ta>
            <ta e="T112" id="Seg_4039" s="T111">tab-ne</ta>
            <ta e="T113" id="Seg_4040" s="T112">tʼü-lʼe</ta>
            <ta e="T114" id="Seg_4041" s="T113">ro-ke-cʼ-q-wa-tt</ta>
            <ta e="T116" id="Seg_4042" s="T114">nilʼdʼäredaŋ</ta>
            <ta e="T117" id="Seg_4043" s="T116">selɨj</ta>
            <ta e="T118" id="Seg_4044" s="T117">tʼeːl-dʼʒom</ta>
            <ta e="T119" id="Seg_4045" s="T118">pünnɨ-ŋ</ta>
            <ta e="T120" id="Seg_4046" s="T119">paja-t</ta>
            <ta e="T121" id="Seg_4047" s="T120">mannɨ-mpɨ-s</ta>
            <ta e="T122" id="Seg_4048" s="T121">mannɨ-mpɨ-s</ta>
            <ta e="T123" id="Seg_4049" s="T122">toːla-ka-ŋ</ta>
            <ta e="T124" id="Seg_4050" s="T123">era-t</ta>
            <ta e="T125" id="Seg_4051" s="T124">seːi-m-d</ta>
            <ta e="T126" id="Seg_4052" s="T125">paqqə-nnɨ-t</ta>
            <ta e="T127" id="Seg_4053" s="T126">tam</ta>
            <ta e="T128" id="Seg_4054" s="T127">plʼeka-j</ta>
            <ta e="T129" id="Seg_4055" s="T128">qä-n</ta>
            <ta e="T130" id="Seg_4056" s="T129">kä-n-bar-t</ta>
            <ta e="T131" id="Seg_4057" s="T130">qwädʼä-t</ta>
            <ta e="T132" id="Seg_4058" s="T131">a</ta>
            <ta e="T133" id="Seg_4059" s="T132">paja-t</ta>
            <ta e="T134" id="Seg_4060" s="T133">tʼäːno</ta>
            <ta e="T135" id="Seg_4061" s="T134">tap-ǝ-n</ta>
            <ta e="T136" id="Seg_4062" s="T135">sej-la-m-d</ta>
            <ta e="T137" id="Seg_4063" s="T136">iː-o-t</ta>
            <ta e="T138" id="Seg_4064" s="T137">toːlakaŋ</ta>
            <ta e="T139" id="Seg_4065" s="T138">ontə</ta>
            <ta e="T140" id="Seg_4066" s="T139">teːrpa-n</ta>
            <ta e="T141" id="Seg_4067" s="T140">a</ta>
            <ta e="T142" id="Seg_4068" s="T141">tat</ta>
            <ta e="T143" id="Seg_4069" s="T142">nano</ta>
            <ta e="T144" id="Seg_4070" s="T143">maːt-tə</ta>
            <ta e="T145" id="Seg_4071" s="T144">qai-m-nej</ta>
            <ta e="T146" id="Seg_4072" s="T145">as</ta>
            <ta e="T147" id="Seg_4073" s="T146">tat-q-wa-l</ta>
            <ta e="T148" id="Seg_4074" s="T147">meŋga</ta>
            <ta e="T149" id="Seg_4075" s="T148">nilʼdʼi-ŋ</ta>
            <ta e="T150" id="Seg_4076" s="T149">paja-t</ta>
            <ta e="T151" id="Seg_4077" s="T150">qwan-nɨ-ŋ</ta>
            <ta e="T152" id="Seg_4078" s="T151">qwan-nɨ-ŋ</ta>
            <ta e="T153" id="Seg_4079" s="T152">i</ta>
            <ta e="T154" id="Seg_4080" s="T153">tʼönǯikaŋ</ta>
            <ta e="T155" id="Seg_4081" s="T154">čaːǯi-ŋ</ta>
            <ta e="T156" id="Seg_4082" s="T155">era-ntɨ-nan</ta>
            <ta e="T157" id="Seg_4083" s="T156">seiː-t</ta>
            <ta e="T158" id="Seg_4084" s="T157">tidam</ta>
            <ta e="T159" id="Seg_4085" s="T158">tʼäŋ-wa-n</ta>
            <ta e="T160" id="Seg_4086" s="T159">qwäcʼ-ku</ta>
            <ta e="T161" id="Seg_4087" s="T160">tab-ǝ-m</ta>
            <ta e="T162" id="Seg_4088" s="T161">tab</ta>
            <ta e="T163" id="Seg_4089" s="T162">ür-g-enǯi-ŋ</ta>
            <ta e="T164" id="Seg_4090" s="T163">nano</ta>
            <ta e="T165" id="Seg_4091" s="T164">i</ta>
            <ta e="T166" id="Seg_4092" s="T165">tʼönǯikaŋ</ta>
            <ta e="T167" id="Seg_4093" s="T166">čaːǯi-ŋ</ta>
            <ta e="T168" id="Seg_4094" s="T167">paja-t</ta>
            <ta e="T169" id="Seg_4095" s="T168">kušaŋ</ta>
            <ta e="T170" id="Seg_4096" s="T169">čaǯi-ŋ</ta>
            <ta e="T171" id="Seg_4097" s="T170">aj</ta>
            <ta e="T172" id="Seg_4098" s="T171">moɣunä</ta>
            <ta e="T173" id="Seg_4099" s="T172">manǯe-dʼi-ku-ŋ</ta>
            <ta e="T174" id="Seg_4100" s="T173">era-t</ta>
            <ta e="T175" id="Seg_4101" s="T174">čaːǯi-ŋ</ta>
            <ta e="T176" id="Seg_4102" s="T175">alʼi</ta>
            <ta e="T177" id="Seg_4103" s="T176">ass</ta>
            <ta e="T178" id="Seg_4104" s="T177">paja-n-t</ta>
            <ta e="T179" id="Seg_4105" s="T178">qwa-lʼewlʼe</ta>
            <ta e="T180" id="Seg_4106" s="T179">era-t</ta>
            <ta e="T181" id="Seg_4107" s="T180">kušaŋ</ta>
            <ta e="T182" id="Seg_4108" s="T181">laŋgɨ-ni-ŋ</ta>
            <ta e="T183" id="Seg_4109" s="T182">sʼäj-la</ta>
            <ta e="T184" id="Seg_4110" s="T183">ro-peː-ss</ta>
            <ta e="T185" id="Seg_4111" s="T184">sʼäj-la</ta>
            <ta e="T186" id="Seg_4112" s="T185">tʼäŋ-g-wa-ttə</ta>
            <ta e="T187" id="Seg_4113" s="T186">as</ta>
            <ta e="T188" id="Seg_4114" s="T187">tü-k-wa-tte</ta>
            <ta e="T189" id="Seg_4115" s="T188">tab-nä</ta>
            <ta e="T190" id="Seg_4116" s="T189">i</ta>
            <ta e="T191" id="Seg_4117" s="T190">ass</ta>
            <ta e="T192" id="Seg_4118" s="T191">parɨdʼi-q-wa-ttə</ta>
            <ta e="T193" id="Seg_4119" s="T192">kušaŋ</ta>
            <ta e="T194" id="Seg_4120" s="T193">sʼäj-la-m-t</ta>
            <ta e="T195" id="Seg_4121" s="T194">qwärɨ-s-tə</ta>
            <ta e="T196" id="Seg_4122" s="T195">qwärɨ-s-t</ta>
            <ta e="T197" id="Seg_4123" s="T196">sʼäi-la</ta>
            <ta e="T198" id="Seg_4124" s="T197">kundar</ta>
            <ta e="T199" id="Seg_4125" s="T198">tʼäŋ-g-wa-ttə</ta>
            <ta e="T200" id="Seg_4126" s="T199">niŋ</ta>
            <ta e="T201" id="Seg_4127" s="T200">i</ta>
            <ta e="T202" id="Seg_4128" s="T201">tʼäŋ-g-wa-tt</ta>
            <ta e="T203" id="Seg_4129" s="T202">tidam</ta>
            <ta e="T204" id="Seg_4130" s="T203">sej-galɨ-k</ta>
            <ta e="T205" id="Seg_4131" s="T204">era-t</ta>
            <ta e="T206" id="Seg_4132" s="T205">na</ta>
            <ta e="T207" id="Seg_4133" s="T206">üːbedʼä-n</ta>
            <ta e="T208" id="Seg_4134" s="T207">sʼäj-galɨ-k</ta>
            <ta e="T209" id="Seg_4135" s="T208">paja-t</ta>
            <ta e="T210" id="Seg_4136" s="T209">aːde-lǯi-ŋ</ta>
            <ta e="T211" id="Seg_4137" s="T210">era-ndɨ-ne</ta>
            <ta e="T212" id="Seg_4138" s="T211">az</ta>
            <ta e="T213" id="Seg_4139" s="T212">aːde-lǯi-ku-ŋ</ta>
            <ta e="T214" id="Seg_4140" s="T213">era-t</ta>
            <ta e="T215" id="Seg_4141" s="T214">sej-galɨ-k</ta>
            <ta e="T216" id="Seg_4142" s="T215">ür-gɨ-le</ta>
            <ta e="T217" id="Seg_4143" s="T216">qu-nǯi-ŋ</ta>
            <ta e="T218" id="Seg_4144" s="T217">mad-ɨ-n</ta>
            <ta e="T219" id="Seg_4145" s="T218">kö-ndə</ta>
            <ta e="T220" id="Seg_4146" s="T219">mig-lʼe</ta>
            <ta e="T221" id="Seg_4147" s="T220">üːbə-r-a-q</ta>
            <ta e="T222" id="Seg_4148" s="T221">paja-t</ta>
            <ta e="T223" id="Seg_4149" s="T222">tɨdəmba-n</ta>
            <ta e="T224" id="Seg_4150" s="T223">tʼäːno</ta>
            <ta e="T225" id="Seg_4151" s="T224">medɨ-dʼe-ŋ</ta>
            <ta e="T226" id="Seg_4152" s="T225">tolʼdʼi-la-m-d</ta>
            <ta e="T0" id="Seg_4153" s="T226">niŋ</ta>
            <ta e="T227" id="Seg_4154" s="T0">el</ta>
            <ta e="T228" id="Seg_4155" s="T227">čaɣan-nɨ-t</ta>
            <ta e="T229" id="Seg_4156" s="T228">mat-tə</ta>
            <ta e="T230" id="Seg_4157" s="T229">seːr-nɨ-ŋ</ta>
            <ta e="T231" id="Seg_4158" s="T230">nʼäɣɨ-ni-ŋ</ta>
            <ta e="T232" id="Seg_4159" s="T231">era-n-t</ta>
            <ta e="T233" id="Seg_4160" s="T232">sej-la-m</ta>
            <ta e="T234" id="Seg_4161" s="T233">tɨso-nt</ta>
            <ta e="T235" id="Seg_4162" s="T234">pen-nɨ-t</ta>
            <ta e="T236" id="Seg_4163" s="T235">pʼötʼe-n</ta>
            <ta e="T237" id="Seg_4164" s="T236">moq-tə</ta>
            <ta e="T238" id="Seg_4165" s="T237">oːcʼiŋ-nɨ-t</ta>
            <ta e="T239" id="Seg_4166" s="T238">era-t</ta>
            <ta e="T240" id="Seg_4167" s="T239">tʼönǯikaŋ</ta>
            <ta e="T241" id="Seg_4168" s="T240">tü-a-n</ta>
            <ta e="T242" id="Seg_4169" s="T241">maːt-tə</ta>
            <ta e="T243" id="Seg_4170" s="T242">ser-na-ŋ</ta>
            <ta e="T244" id="Seg_4171" s="T243">paja-t</ta>
            <ta e="T245" id="Seg_4172" s="T244">tʼärɨ-ŋ</ta>
            <ta e="T246" id="Seg_4173" s="T245">a</ta>
            <ta e="T247" id="Seg_4174" s="T246">tat</ta>
            <ta e="T248" id="Seg_4175" s="T247">qaj-no</ta>
            <ta e="T249" id="Seg_4176" s="T248">tam-dʼel</ta>
            <ta e="T250" id="Seg_4177" s="T249">kundɨ-ŋ</ta>
            <ta e="T251" id="Seg_4178" s="T250">je-wa-nt</ta>
            <ta e="T252" id="Seg_4179" s="T251">era-t</ta>
            <ta e="T253" id="Seg_4180" s="T252">tʼärɨ-ŋ</ta>
            <ta e="T254" id="Seg_4181" s="T253">Man</ta>
            <ta e="T255" id="Seg_4182" s="T254">tam-dʼel</ta>
            <ta e="T256" id="Seg_4183" s="T255">sej-o-w-ɨ-m</ta>
            <ta e="T257" id="Seg_4184" s="T256">aːtsa-ŋ</ta>
            <ta e="T258" id="Seg_4185" s="T257">nano</ta>
            <ta e="T259" id="Seg_4186" s="T258">kundɨ-ŋ</ta>
            <ta e="T260" id="Seg_4187" s="T259">je-wa-ŋ</ta>
            <ta e="T261" id="Seg_4188" s="T260">paja-t</ta>
            <ta e="T262" id="Seg_4189" s="T261">teːrba-n</ta>
            <ta e="T263" id="Seg_4190" s="T262">a</ta>
            <ta e="T264" id="Seg_4191" s="T263">tat</ta>
            <ta e="T265" id="Seg_4192" s="T264">siːra-nt</ta>
            <ta e="T266" id="Seg_4193" s="T265">man</ta>
            <ta e="T267" id="Seg_4194" s="T266">tidam</ta>
            <ta e="T268" id="Seg_4195" s="T267">tastɨ</ta>
            <ta e="T269" id="Seg_4196" s="T268">tunu-a-ŋ</ta>
            <ta e="T270" id="Seg_4197" s="T269">kundar</ta>
            <ta e="T271" id="Seg_4198" s="T270">tat</ta>
            <ta e="T272" id="Seg_4199" s="T271">suːru-nʼä-nt</ta>
            <ta e="T273" id="Seg_4200" s="T272">selɨj</ta>
            <ta e="T274" id="Seg_4201" s="T273">tʼelʼ-dʼzʼomp</ta>
            <ta e="T275" id="Seg_4202" s="T274">sej-la-ndɨ-se</ta>
            <ta e="T276" id="Seg_4203" s="T275">warka-nd</ta>
            <ta e="T277" id="Seg_4204" s="T276">aw</ta>
            <ta e="T278" id="Seg_4205" s="T277">qɨ-n</ta>
            <ta e="T279" id="Seg_4206" s="T278">baːr-t</ta>
            <ta e="T280" id="Seg_4207" s="T279">ɨt-q-wä-l</ta>
            <ta e="T281" id="Seg_4208" s="T280">a</ta>
            <ta e="T282" id="Seg_4209" s="T281">tam</ta>
            <ta e="T283" id="Seg_4210" s="T282">plʼeka-j</ta>
            <ta e="T284" id="Seg_4211" s="T283">kɨ-n</ta>
            <ta e="T285" id="Seg_4212" s="T284">baːr-t</ta>
            <ta e="T286" id="Seg_4213" s="T285">neːz-ol-g-wa-nt</ta>
            <ta e="T287" id="Seg_4214" s="T286">sej-la-l</ta>
            <ta e="T288" id="Seg_4215" s="T287">tʼeŋga</ta>
            <ta e="T289" id="Seg_4216" s="T288">tʼü-lʼe</ta>
            <ta e="T290" id="Seg_4217" s="T289">parɨcʼi-k-wa-ttə</ta>
            <ta e="T291" id="Seg_4218" s="T290">nilʼdʼi-ŋ</ta>
            <ta e="T292" id="Seg_4219" s="T291">tat</ta>
            <ta e="T293" id="Seg_4220" s="T292">i</ta>
            <ta e="T294" id="Seg_4221" s="T293">sünde-bɨ-ŋ</ta>
            <ta e="T295" id="Seg_4222" s="T294">tü-kku-za-nt</ta>
            <ta e="T296" id="Seg_4223" s="T295">nu</ta>
            <ta e="T297" id="Seg_4224" s="T296">i</ta>
            <ta e="T298" id="Seg_4225" s="T297">warkɨ-lʼe</ta>
            <ta e="T299" id="Seg_4226" s="T298">üːbə-r-a-q</ta>
            <ta e="T300" id="Seg_4227" s="T299">kušaj</ta>
            <ta e="T301" id="Seg_4228" s="T300">da</ta>
            <ta e="T302" id="Seg_4229" s="T301">dʼeːl-ɨ-t</ta>
            <ta e="T303" id="Seg_4230" s="T302">mendɨ-ŋ</ta>
            <ta e="T304" id="Seg_4231" s="T303">paja-t</ta>
            <ta e="T305" id="Seg_4232" s="T304">manǯe-di-ŋ</ta>
            <ta e="T306" id="Seg_4233" s="T305">süderna-un</ta>
            <ta e="T307" id="Seg_4234" s="T306">manǯe-di-ŋ</ta>
            <ta e="T308" id="Seg_4235" s="T307">nɨ-ŋa-n</ta>
            <ta e="T309" id="Seg_4236" s="T308">peŋk</ta>
            <ta e="T310" id="Seg_4237" s="T309">paja-t</ta>
            <ta e="T311" id="Seg_4238" s="T310">era-ndɨ-ne</ta>
            <ta e="T312" id="Seg_4239" s="T311">tʼärɨ-ŋ</ta>
            <ta e="T313" id="Seg_4240" s="T312">peŋkə</ta>
            <ta e="T314" id="Seg_4241" s="T313">nɨ-ŋgɨ-nt</ta>
            <ta e="T315" id="Seg_4242" s="T314">tat</ta>
            <ta e="T316" id="Seg_4243" s="T315">waz-lʼe-nt</ta>
            <ta e="T317" id="Seg_4244" s="T316">kundar-em</ta>
            <ta e="T318" id="Seg_4245" s="T317">tʼäʒ-lʼe-l</ta>
            <ta e="T319" id="Seg_4246" s="T318">peŋk-ə-m</ta>
            <ta e="T320" id="Seg_4247" s="T319">a</ta>
            <ta e="T321" id="Seg_4248" s="T320">era-t</ta>
            <ta e="T322" id="Seg_4249" s="T321">tʼärɨ-ŋ</ta>
            <ta e="T323" id="Seg_4250" s="T322">Man</ta>
            <ta e="T324" id="Seg_4251" s="T323">kundar</ta>
            <ta e="T325" id="Seg_4252" s="T324">tʼäǯ-enǯa-u</ta>
            <ta e="T326" id="Seg_4253" s="T325">sej-e-w</ta>
            <ta e="T327" id="Seg_4254" s="T326">az</ta>
            <ta e="T328" id="Seg_4255" s="T327">aːdu-ŋ</ta>
            <ta e="T329" id="Seg_4256" s="T328">paja-t</ta>
            <ta e="T330" id="Seg_4257" s="T329">tʼärɨ-ŋ</ta>
            <ta e="T331" id="Seg_4258" s="T330">tü-qɨ</ta>
            <ta e="T332" id="Seg_4259" s="T331">meŋga</ta>
            <ta e="T333" id="Seg_4260" s="T332">man</ta>
            <ta e="T334" id="Seg_4261" s="T333">tulse-m</ta>
            <ta e="T335" id="Seg_4262" s="T334">teŋga</ta>
            <ta e="T336" id="Seg_4263" s="T335">oral-ǯo-lǯi-lʼe-w-s</ta>
            <ta e="T337" id="Seg_4264" s="T336">i</ta>
            <ta e="T338" id="Seg_4265" s="T337">era-t</ta>
            <ta e="T339" id="Seg_4266" s="T338">tʼü-a-n</ta>
            <ta e="T340" id="Seg_4267" s="T339">paja-ndɨ-ne</ta>
            <ta e="T341" id="Seg_4268" s="T340">paja-t</ta>
            <ta e="T342" id="Seg_4269" s="T341">era-ndɨ-ne</ta>
            <ta e="T343" id="Seg_4270" s="T342">oral-ǯo-lǯi-t</ta>
            <ta e="T344" id="Seg_4271" s="T343">udo-ɣɨnt</ta>
            <ta e="T345" id="Seg_4272" s="T344">aps-tɨ-mbɨdi</ta>
            <ta e="T346" id="Seg_4273" s="T345">tʼülse-m</ta>
            <ta e="T347" id="Seg_4274" s="T346">ond</ta>
            <ta e="T348" id="Seg_4275" s="T347">tab-ne</ta>
            <ta e="T349" id="Seg_4276" s="T348">pɨmʒɨr-ɨ-t</ta>
            <ta e="T350" id="Seg_4277" s="T349">kundar</ta>
            <ta e="T351" id="Seg_4278" s="T350">tʼät-ku</ta>
            <ta e="T352" id="Seg_4279" s="T351">peŋg-ɨ-m</ta>
            <ta e="T353" id="Seg_4280" s="T352">era-t</ta>
            <ta e="T354" id="Seg_4281" s="T353">tʼäʒi-t</ta>
            <ta e="T355" id="Seg_4282" s="T354">qwan-nɨ-t</ta>
            <ta e="T356" id="Seg_4283" s="T355">peŋg-ɨ-m</ta>
            <ta e="T357" id="Seg_4284" s="T356">era-n-d</ta>
            <ta e="T358" id="Seg_4285" s="T357">sej</ta>
            <ta e="T359" id="Seg_4286" s="T358">az</ta>
            <ta e="T360" id="Seg_4287" s="T359">aːdu-ŋ</ta>
            <ta e="T361" id="Seg_4288" s="T360">peŋg-ɨ-m</ta>
            <ta e="T362" id="Seg_4289" s="T361">tab-ne</ta>
            <ta e="T363" id="Seg_4290" s="T362">as</ta>
            <ta e="T364" id="Seg_4291" s="T363">kɨr-gu</ta>
            <ta e="T365" id="Seg_4292" s="T364">paja-t</ta>
            <ta e="T366" id="Seg_4293" s="T365">ondə</ta>
            <ta e="T367" id="Seg_4294" s="T366">kɨr-lʼe</ta>
            <ta e="T368" id="Seg_4295" s="T367">üːbə-r-ɨ-t</ta>
            <ta e="T369" id="Seg_4296" s="T368">peŋg-ɨ-m</ta>
            <ta e="T370" id="Seg_4297" s="T369">nu</ta>
            <ta e="T371" id="Seg_4298" s="T370">i</ta>
            <ta e="T372" id="Seg_4299" s="T371">kɨr-ɨ-t</ta>
            <ta e="T373" id="Seg_4300" s="T372">peŋg-ɨ-m</ta>
            <ta e="T374" id="Seg_4301" s="T373">wadʼi-la-m</ta>
            <ta e="T375" id="Seg_4302" s="T374">nʼörban-nɨ-t</ta>
            <ta e="T376" id="Seg_4303" s="T375">ɨːd-ɨ-t</ta>
            <ta e="T377" id="Seg_4304" s="T376">okkɨr</ta>
            <ta e="T378" id="Seg_4305" s="T377">dʼel</ta>
            <ta e="T379" id="Seg_4306" s="T378">wadʼi-la-m</ta>
            <ta e="T380" id="Seg_4307" s="T379">kocʼi-n</ta>
            <ta e="T381" id="Seg_4308" s="T380">pon-nɨ-t</ta>
            <ta e="T382" id="Seg_4309" s="T381">era-ndɨ-ne</ta>
            <ta e="T383" id="Seg_4310" s="T382">tʼärɨ-ŋ</ta>
            <ta e="T384" id="Seg_4311" s="T383">tam-dʼel</ta>
            <ta e="T385" id="Seg_4312" s="T384">qu-la</ta>
            <ta e="T386" id="Seg_4313" s="T385">tʼara-ttə</ta>
            <ta e="T387" id="Seg_4314" s="T386">tam-dʼe-lʼe</ta>
            <ta e="T388" id="Seg_4315" s="T387">dʼel</ta>
            <ta e="T389" id="Seg_4316" s="T388">iːbkaj</ta>
            <ta e="T390" id="Seg_4317" s="T389">nuː-lʼ-dʼel</ta>
            <ta e="T391" id="Seg_4318" s="T390">era-t</ta>
            <ta e="T392" id="Seg_4319" s="T391">tʼärɨ-ŋ</ta>
            <ta e="T393" id="Seg_4320" s="T392">nuː-lʼ-dʼel-ǯi-k</ta>
            <ta e="T394" id="Seg_4321" s="T393">nu</ta>
            <ta e="T395" id="Seg_4322" s="T394">i</ta>
            <ta e="T396" id="Seg_4323" s="T395">nuː-lʼ-dʼeːl-č-lʼe</ta>
            <ta e="T397" id="Seg_4324" s="T396">üːbə-r-a-k</ta>
            <ta e="T398" id="Seg_4325" s="T397">paja-t</ta>
            <ta e="T399" id="Seg_4326" s="T398">pone-n</ta>
            <ta e="T400" id="Seg_4327" s="T399">era-t</ta>
            <ta e="T401" id="Seg_4328" s="T400">maːt-qɨn</ta>
            <ta e="T402" id="Seg_4329" s="T401">je-wa-n</ta>
            <ta e="T403" id="Seg_4330" s="T402">paja-t</ta>
            <ta e="T404" id="Seg_4331" s="T403">pone-n</ta>
            <ta e="T405" id="Seg_4332" s="T404">äːd-ɨ-t</ta>
            <ta e="T406" id="Seg_4333" s="T405">kuːgɨr-san-ɨ-m</ta>
            <ta e="T407" id="Seg_4334" s="T406">kuːgɨr-ku-či-lʼe</ta>
            <ta e="T408" id="Seg_4335" s="T407">üːbə-r-a-ŋ</ta>
            <ta e="T409" id="Seg_4336" s="T408">era-t</ta>
            <ta e="T410" id="Seg_4337" s="T409">maːt-qɨn</ta>
            <ta e="T411" id="Seg_4338" s="T410">mat-š-ka-ŋ</ta>
            <ta e="T412" id="Seg_4339" s="T411">a</ta>
            <ta e="T413" id="Seg_4340" s="T412">paja</ta>
            <ta e="T414" id="Seg_4341" s="T413">poːne-n</ta>
            <ta e="T415" id="Seg_4342" s="T414">kugor-či-ŋ</ta>
            <ta e="T416" id="Seg_4343" s="T415">i</ta>
            <ta e="T417" id="Seg_4344" s="T416">kugor-či-ŋ</ta>
            <ta e="T418" id="Seg_4345" s="T417">a</ta>
            <ta e="T419" id="Seg_4346" s="T418">ont</ta>
            <ta e="T420" id="Seg_4347" s="T419">čagə-mbedi</ta>
            <ta e="T421" id="Seg_4348" s="T420">peŋq-ɨ-n</ta>
            <ta e="T422" id="Seg_4349" s="T421">wadʼi</ta>
            <ta e="T423" id="Seg_4350" s="T422">au-r-nɨ-ŋ</ta>
            <ta e="T424" id="Seg_4351" s="T423">ont</ta>
            <ta e="T425" id="Seg_4352" s="T424">nuː-n</ta>
            <ta e="T426" id="Seg_4353" s="T425">qojmɨ-n</ta>
            <ta e="T427" id="Seg_4354" s="T426">kelʼ-lʼe</ta>
            <ta e="T428" id="Seg_4355" s="T427">taːd-ə-r-ɨ-t</ta>
            <ta e="T429" id="Seg_4356" s="T428">qampa-j</ta>
            <ta e="T430" id="Seg_4357" s="T429">tʼeːl-a-t</ta>
            <ta e="T431" id="Seg_4358" s="T430">tʼümbo-cʼ-ka</ta>
            <ta e="T432" id="Seg_4359" s="T431">era-t</ta>
            <ta e="T433" id="Seg_4360" s="T432">maːt-qɨn</ta>
            <ta e="T434" id="Seg_4361" s="T433">aːmdɨ-s</ta>
            <ta e="T435" id="Seg_4362" s="T434">aːmdɨ-s</ta>
            <ta e="T436" id="Seg_4363" s="T435">kertɨmnɨ-ŋ</ta>
            <ta e="T437" id="Seg_4364" s="T436">i</ta>
            <ta e="T438" id="Seg_4365" s="T437">teːrba-n</ta>
            <ta e="T439" id="Seg_4366" s="T438">Man</ta>
            <ta e="T440" id="Seg_4367" s="T439">sej-la-w-ɨ-m</ta>
            <ta e="T441" id="Seg_4368" s="T440">qwär-lʼe-u</ta>
            <ta e="T442" id="Seg_4369" s="T441">sej-la</ta>
            <ta e="T443" id="Seg_4370" s="T442">moʒetbɨtʼ</ta>
            <ta e="T444" id="Seg_4371" s="T443">meŋga</ta>
            <ta e="T445" id="Seg_4372" s="T444">tʼü-lʼeze-q</ta>
            <ta e="T446" id="Seg_4373" s="T445">tüː-lʼeze-ttɨ</ta>
            <ta e="T447" id="Seg_4374" s="T446">nu</ta>
            <ta e="T448" id="Seg_4375" s="T447">nilʼdʼi-ŋ</ta>
            <ta e="T449" id="Seg_4376" s="T448">i</ta>
            <ta e="T450" id="Seg_4377" s="T449">me-o-t</ta>
            <ta e="T451" id="Seg_4378" s="T450">sʼäj-la-m-d</ta>
            <ta e="T452" id="Seg_4379" s="T451">qwär-lʼe</ta>
            <ta e="T453" id="Seg_4380" s="T452">üːbə-r-a-t</ta>
            <ta e="T454" id="Seg_4381" s="T453">qaj-da</ta>
            <ta e="T455" id="Seg_4382" s="T454">pʼöcʼi-n</ta>
            <ta e="T456" id="Seg_4383" s="T455">moqɨ-n</ta>
            <ta e="T457" id="Seg_4384" s="T456">raxsɨmba-n</ta>
            <ta e="T458" id="Seg_4385" s="T457">tap</ta>
            <ta e="T459" id="Seg_4386" s="T458">soːcʼka-ŋ</ta>
            <ta e="T460" id="Seg_4387" s="T459">üŋg-elǯi-ŋ</ta>
            <ta e="T461" id="Seg_4388" s="T460">ondə</ta>
            <ta e="T462" id="Seg_4389" s="T461">tʼeːrba-n</ta>
            <ta e="T463" id="Seg_4390" s="T462">Qaj</ta>
            <ta e="T464" id="Seg_4391" s="T463">natʼe-n</ta>
            <ta e="T465" id="Seg_4392" s="T464">raqsɨmɨ-nt</ta>
            <ta e="T466" id="Seg_4393" s="T465">tap</ta>
            <ta e="T467" id="Seg_4394" s="T466">aj</ta>
            <ta e="T468" id="Seg_4395" s="T467">sədɨ-mǯel</ta>
            <ta e="T469" id="Seg_4396" s="T468">tʼärɨ-ŋ</ta>
            <ta e="T470" id="Seg_4397" s="T469">sej-la</ta>
            <ta e="T471" id="Seg_4398" s="T470">roː-peː-ss</ta>
            <ta e="T472" id="Seg_4399" s="T471">a</ta>
            <ta e="T473" id="Seg_4400" s="T472">pʼöcʼi-n</ta>
            <ta e="T474" id="Seg_4401" s="T473">moqɨ-n</ta>
            <ta e="T475" id="Seg_4402" s="T474">warɣɨ-ŋ</ta>
            <ta e="T476" id="Seg_4403" s="T475">raqson-ne-ŋ</ta>
            <ta e="T477" id="Seg_4404" s="T476">qaj-da</ta>
            <ta e="T478" id="Seg_4405" s="T477">era-t</ta>
            <ta e="T479" id="Seg_4406" s="T478">natʼe-t</ta>
            <ta e="T480" id="Seg_4407" s="T479">tʼüːr-ɨ-se</ta>
            <ta e="T481" id="Seg_4408" s="T480">tʼönǯikaŋ</ta>
            <ta e="T482" id="Seg_4409" s="T481">qwan-nɨ-ŋ</ta>
            <ta e="T483" id="Seg_4410" s="T482">pöcʼi-m</ta>
            <ta e="T484" id="Seg_4411" s="T483">moqɨ-ŋ</ta>
            <ta e="T485" id="Seg_4412" s="T484">pündar-ɨ-mpa-n</ta>
            <ta e="T486" id="Seg_4413" s="T485">qo-ɨ-t</ta>
            <ta e="T487" id="Seg_4414" s="T486">tɨssa-m</ta>
            <ta e="T488" id="Seg_4415" s="T487">oːmdɨ-ŋ</ta>
            <ta e="T489" id="Seg_4416" s="T488">na</ta>
            <ta e="T490" id="Seg_4417" s="T489">tɨssɨ-m</ta>
            <ta e="T491" id="Seg_4418" s="T490">tʼiːqɨ-t</ta>
            <ta e="T492" id="Seg_4419" s="T491">i</ta>
            <ta e="T493" id="Seg_4420" s="T492">püːɣɨlǯi-mba-t</ta>
            <ta e="T494" id="Seg_4421" s="T493">qaj-da</ta>
            <ta e="T495" id="Seg_4422" s="T494">sət</ta>
            <ta e="T496" id="Seg_4423" s="T495">püːr-la-ška</ta>
            <ta e="T497" id="Seg_4424" s="T496">tau</ta>
            <ta e="T498" id="Seg_4425" s="T497">odnako</ta>
            <ta e="T499" id="Seg_4426" s="T498">ma-nan-i</ta>
            <ta e="T500" id="Seg_4427" s="T499">sej-la</ta>
            <ta e="T501" id="Seg_4428" s="T500">pöti</ta>
            <ta e="T502" id="Seg_4429" s="T501">üːd-ɨ-m</ta>
            <ta e="T503" id="Seg_4430" s="T502">qo-ɣɨ-t</ta>
            <ta e="T504" id="Seg_4431" s="T503">era-t</ta>
            <ta e="T505" id="Seg_4432" s="T504">üːt-tɨ</ta>
            <ta e="T506" id="Seg_4433" s="T505">tʼäptä-ptɨ-t</ta>
            <ta e="T507" id="Seg_4434" s="T506">sej-la-t</ta>
            <ta e="T508" id="Seg_4435" s="T507">sət</ta>
            <ta e="T509" id="Seg_4436" s="T508">sej</ta>
            <ta e="T510" id="Seg_4437" s="T509">kušaŋ</ta>
            <ta e="T511" id="Seg_4438" s="T510">üːt-qɨn</ta>
            <ta e="T512" id="Seg_4439" s="T511">tʼäptä-di-mbɨ-za-ttə</ta>
            <ta e="T513" id="Seg_4440" s="T512">saʒaŋ</ta>
            <ta e="T514" id="Seg_4441" s="T513">müzulǯi-t</ta>
            <ta e="T515" id="Seg_4442" s="T514">sʼäj-la-ɣɨnd</ta>
            <ta e="T516" id="Seg_4443" s="T515">tʼärɨ-ŋ</ta>
            <ta e="T517" id="Seg_4444" s="T516">sʼäj-la</ta>
            <ta e="T518" id="Seg_4445" s="T517">ro-peː-ss</ta>
            <ta e="T519" id="Seg_4446" s="T518">sej-la-t</ta>
            <ta e="T520" id="Seg_4447" s="T519">tab-ɨ-nä</ta>
            <ta e="T521" id="Seg_4448" s="T520">parɨdʼä-ttə</ta>
            <ta e="T522" id="Seg_4449" s="T521">manǯe-dʼi-ŋ</ta>
            <ta e="T523" id="Seg_4450" s="T522">a</ta>
            <ta e="T524" id="Seg_4451" s="T523">sej-la-t</ta>
            <ta e="T525" id="Seg_4452" s="T524">soːčʼka-ŋ</ta>
            <ta e="T526" id="Seg_4453" s="T525">az</ta>
            <ta e="T527" id="Seg_4454" s="T526">aːda-ttə</ta>
            <ta e="T528" id="Seg_4455" s="T527">sej-la-m-d</ta>
            <ta e="T529" id="Seg_4456" s="T528">paqqɨ-nɨ-t</ta>
            <ta e="T530" id="Seg_4457" s="T529">aj</ta>
            <ta e="T531" id="Seg_4458" s="T530">tʼäptä-ptɨ-t</ta>
            <ta e="T532" id="Seg_4459" s="T531">soːcʼka-ŋ</ta>
            <ta e="T533" id="Seg_4460" s="T532">sʼäj-la-m-t</ta>
            <ta e="T534" id="Seg_4461" s="T533">aj</ta>
            <ta e="T535" id="Seg_4462" s="T534">soːcʼka-ŋ</ta>
            <ta e="T536" id="Seg_4463" s="T535">müsen-nɨ-t</ta>
            <ta e="T537" id="Seg_4464" s="T536">tʼärɨ-ŋ</ta>
            <ta e="T538" id="Seg_4465" s="T537">sej-la</ta>
            <ta e="T539" id="Seg_4466" s="T538">roː-peː-ss</ta>
            <ta e="T540" id="Seg_4467" s="T539">sej-la-t</ta>
            <ta e="T541" id="Seg_4468" s="T540">parɨdʼä-ttə</ta>
            <ta e="T542" id="Seg_4469" s="T541">kwesse</ta>
            <ta e="T543" id="Seg_4470" s="T542">tibe-qum</ta>
            <ta e="T544" id="Seg_4471" s="T543">aːnda-nɨ-ŋ</ta>
            <ta e="T545" id="Seg_4472" s="T544">tidam</ta>
            <ta e="T546" id="Seg_4473" s="T545">sej-la-u</ta>
            <ta e="T547" id="Seg_4474" s="T546">soːcʼka-ŋ</ta>
            <ta e="T548" id="Seg_4475" s="T547">ko-nǯer-na-ttə</ta>
            <ta e="T549" id="Seg_4476" s="T548">tɨdam</ta>
            <ta e="T550" id="Seg_4477" s="T549">man</ta>
            <ta e="T551" id="Seg_4478" s="T550">sej-ze</ta>
            <ta e="T552" id="Seg_4479" s="T551">eza-ŋ</ta>
            <ta e="T553" id="Seg_4480" s="T552">uʒo</ta>
            <ta e="T554" id="Seg_4481" s="T553">man</ta>
            <ta e="T555" id="Seg_4482" s="T554">tast</ta>
            <ta e="T556" id="Seg_4483" s="T555">sej-la-ɣɨn</ta>
            <ta e="T557" id="Seg_4484" s="T556">tʼät</ta>
            <ta e="T558" id="Seg_4485" s="T557">qoːba-la-k-s</ta>
            <ta e="T559" id="Seg_4486" s="T558">pone</ta>
            <ta e="T560" id="Seg_4487" s="T559">čaːnǯi-ŋ</ta>
            <ta e="T561" id="Seg_4488" s="T560">a</ta>
            <ta e="T562" id="Seg_4489" s="T561">paja-t</ta>
            <ta e="T563" id="Seg_4490" s="T562">kugur-gu-tči-ŋ</ta>
            <ta e="T564" id="Seg_4491" s="T563">nu</ta>
            <ta e="T565" id="Seg_4492" s="T564">kojmɨ-m</ta>
            <ta e="T566" id="Seg_4493" s="T565">taːd-ə-r-ɨ-t</ta>
            <ta e="T567" id="Seg_4494" s="T566">era-t</ta>
            <ta e="T568" id="Seg_4495" s="T567">tʼärɨ-ŋ</ta>
            <ta e="T569" id="Seg_4496" s="T568">paja-ndɨ-ne</ta>
            <ta e="T570" id="Seg_4497" s="T569">a</ta>
            <ta e="T571" id="Seg_4498" s="T570">tat</ta>
            <ta e="T572" id="Seg_4499" s="T571">mazɨm</ta>
            <ta e="T573" id="Seg_4500" s="T572">qaj-no</ta>
            <ta e="T574" id="Seg_4501" s="T573">sejgɨlǯa-nt</ta>
            <ta e="T575" id="Seg_4502" s="T574">a</ta>
            <ta e="T576" id="Seg_4503" s="T575">paja-t</ta>
            <ta e="T577" id="Seg_4504" s="T576">tʼärɨ-ŋ</ta>
            <ta e="T578" id="Seg_4505" s="T577">a</ta>
            <ta e="T579" id="Seg_4506" s="T578">tat</ta>
            <ta e="T580" id="Seg_4507" s="T579">qaj-no</ta>
            <ta e="T581" id="Seg_4508" s="T580">nilʼdʼi-ŋ</ta>
            <ta e="T582" id="Seg_4509" s="T581">palʼdʼi-ku-za-nt</ta>
            <ta e="T583" id="Seg_4510" s="T582">köcʼ-ku-za-ndə</ta>
            <ta e="T584" id="Seg_4511" s="T583">suːru-s-ku</ta>
            <ta e="T585" id="Seg_4512" s="T584">a</ta>
            <ta e="T586" id="Seg_4513" s="T585">onent</ta>
            <ta e="T587" id="Seg_4514" s="T586">sej-la-ndɨ-se</ta>
            <ta e="T588" id="Seg_4515" s="T587">sanǯir-sa-nt</ta>
            <ta e="T589" id="Seg_4516" s="T588">a</ta>
            <ta e="T590" id="Seg_4517" s="T589">maːt-t</ta>
            <ta e="T591" id="Seg_4518" s="T590">qai-m-nej</ta>
            <ta e="T592" id="Seg_4519" s="T591">as</ta>
            <ta e="T593" id="Seg_4520" s="T592">taːt-ku-za-l</ta>
            <ta e="T594" id="Seg_4521" s="T593">wot</ta>
            <ta e="T595" id="Seg_4522" s="T594">man</ta>
            <ta e="T596" id="Seg_4523" s="T595">tan-nan-i</ta>
            <ta e="T597" id="Seg_4524" s="T596">sej-la-m-t</ta>
            <ta e="T598" id="Seg_4525" s="T597">nano</ta>
            <ta e="T599" id="Seg_4526" s="T598">i</ta>
            <ta e="T600" id="Seg_4527" s="T599">paq-qɨn-na-u</ta>
            <ta e="T601" id="Seg_4528" s="T600">tan-na-u</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_4529" s="T1">elɨ-sɨ-qij</ta>
            <ta e="T3" id="Seg_4530" s="T2">warkɨ-sɨ-qij</ta>
            <ta e="T4" id="Seg_4531" s="T3">era</ta>
            <ta e="T5" id="Seg_4532" s="T4">paja-sɨ-qi</ta>
            <ta e="T6" id="Seg_4533" s="T5">era-tə</ta>
            <ta e="T7" id="Seg_4534" s="T6">palʼdʼi-ku-n</ta>
            <ta e="T8" id="Seg_4535" s="T7">suːrum-le</ta>
            <ta e="T9" id="Seg_4536" s="T8">paja-tə</ta>
            <ta e="T10" id="Seg_4537" s="T9">maːt-qɨn</ta>
            <ta e="T11" id="Seg_4538" s="T10">eː-nɨ-n</ta>
            <ta e="T12" id="Seg_4539" s="T11">era-tə</ta>
            <ta e="T13" id="Seg_4540" s="T12">palʼdʼi-ku-n</ta>
            <ta e="T14" id="Seg_4541" s="T13">palʼdʼi-ku-n</ta>
            <ta e="T15" id="Seg_4542" s="T14">dʼel-tʼömb</ta>
            <ta e="T16" id="Seg_4543" s="T15">palʼdʼi-ku-n</ta>
            <ta e="T17" id="Seg_4544" s="T16">qaj-m-näj</ta>
            <ta e="T18" id="Seg_4545" s="T17">asa</ta>
            <ta e="T19" id="Seg_4546" s="T18">tat-ku-t</ta>
            <ta e="T20" id="Seg_4547" s="T19">qɨdan</ta>
            <ta e="T21" id="Seg_4548" s="T20">niŋ</ta>
            <ta e="T22" id="Seg_4549" s="T21">palʼdʼi-ku-n</ta>
            <ta e="T23" id="Seg_4550" s="T22">qaj-m-näj</ta>
            <ta e="T24" id="Seg_4551" s="T23">asa</ta>
            <ta e="T25" id="Seg_4552" s="T24">tat-ku-t</ta>
            <ta e="T26" id="Seg_4553" s="T25">okkɨr</ta>
            <ta e="T27" id="Seg_4554" s="T26">dʼel</ta>
            <ta e="T28" id="Seg_4555" s="T27">paja-tə</ta>
            <ta e="T29" id="Seg_4556" s="T28">tärba-n</ta>
            <ta e="T30" id="Seg_4557" s="T29">qaj-no</ta>
            <ta e="T31" id="Seg_4558" s="T30">ʒe</ta>
            <ta e="T32" id="Seg_4559" s="T31">täp</ta>
            <ta e="T33" id="Seg_4560" s="T32">qaj-m-näj</ta>
            <ta e="T34" id="Seg_4561" s="T33">asa</ta>
            <ta e="T35" id="Seg_4562" s="T34">tat-ku-t</ta>
            <ta e="T36" id="Seg_4563" s="T35">man</ta>
            <ta e="T37" id="Seg_4564" s="T36">täp-ɨ-m</ta>
            <ta e="T38" id="Seg_4565" s="T37">saralǯə-lä-w</ta>
            <ta e="T39" id="Seg_4566" s="T38">qaj-no</ta>
            <ta e="T40" id="Seg_4567" s="T39">täp</ta>
            <ta e="T41" id="Seg_4568" s="T40">sʼütdʼe-bɨ-ŋ</ta>
            <ta e="T42" id="Seg_4569" s="T41">tüː-ku-n</ta>
            <ta e="T43" id="Seg_4570" s="T42">nu</ta>
            <ta e="T44" id="Seg_4571" s="T43">i</ta>
            <ta e="T45" id="Seg_4572" s="T44">nʼilʼdʼi-ŋ</ta>
            <ta e="T46" id="Seg_4573" s="T45">meː-nɨ-t</ta>
            <ta e="T47" id="Seg_4574" s="T46">era-tə</ta>
            <ta e="T48" id="Seg_4575" s="T47">übɨdʼi-le</ta>
            <ta e="T49" id="Seg_4576" s="T48">übɨ-r-nɨ-n</ta>
            <ta e="T50" id="Seg_4577" s="T49">suːrum-le</ta>
            <ta e="T51" id="Seg_4578" s="T50">qwan-nɨ-n</ta>
            <ta e="T52" id="Seg_4579" s="T51">paja-tə</ta>
            <ta e="T53" id="Seg_4580" s="T52">tʼaŋ</ta>
            <ta e="T54" id="Seg_4581" s="T53">tolʼdʼ-ɨ-m-tə</ta>
            <ta e="T55" id="Seg_4582" s="T54">ser-nɨ-t</ta>
            <ta e="T56" id="Seg_4583" s="T55">era-n-tə</ta>
            <ta e="T57" id="Seg_4584" s="T56">moqɨn</ta>
            <ta e="T58" id="Seg_4585" s="T57">tʼaŋ</ta>
            <ta e="T59" id="Seg_4586" s="T58">übə-n</ta>
            <ta e="T60" id="Seg_4587" s="T59">era-ntɨ-nä</ta>
            <ta e="T61" id="Seg_4588" s="T60">asa</ta>
            <ta e="T62" id="Seg_4589" s="T61">aːdu-lǯi-kɨ-n</ta>
            <ta e="T63" id="Seg_4590" s="T62">nanoː</ta>
            <ta e="T64" id="Seg_4591" s="T63">asa</ta>
            <ta e="T65" id="Seg_4592" s="T64">aːdu-lǯi-kɨ-n</ta>
            <ta e="T66" id="Seg_4593" s="T65">täp-ɨ-m</ta>
            <ta e="T67" id="Seg_4594" s="T66">era-tə</ta>
            <ta e="T68" id="Seg_4595" s="T67">igə</ta>
            <ta e="T69" id="Seg_4596" s="T68">qo-nǯir-ne-t</ta>
            <ta e="T70" id="Seg_4597" s="T69">era-tə</ta>
            <ta e="T71" id="Seg_4598" s="T70">medə-n</ta>
            <ta e="T72" id="Seg_4599" s="T71">Qɨ-n</ta>
            <ta e="T73" id="Seg_4600" s="T72">qä-n-par-ntə</ta>
            <ta e="T74" id="Seg_4601" s="T73">taw</ta>
            <ta e="T75" id="Seg_4602" s="T74">plʼäka-lʼ</ta>
            <ta e="T76" id="Seg_4603" s="T75">qä-n</ta>
            <ta e="T77" id="Seg_4604" s="T76">par-qɨn</ta>
            <ta e="T78" id="Seg_4605" s="T77">ondə</ta>
            <ta e="T79" id="Seg_4606" s="T78">sej-m-tə</ta>
            <ta e="T80" id="Seg_4607" s="T79">paqə-qɨl-ku-t</ta>
            <ta e="T81" id="Seg_4608" s="T80">sej-la-m-tə</ta>
            <ta e="T82" id="Seg_4609" s="T81">ödə-le</ta>
            <ta e="T83" id="Seg_4610" s="T82">qwɛdʼi-ku-t</ta>
            <ta e="T84" id="Seg_4611" s="T83">po-n</ta>
            <ta e="T85" id="Seg_4612" s="T84">moː-la-qɨn</ta>
            <ta e="T86" id="Seg_4613" s="T85">to</ta>
            <ta e="T87" id="Seg_4614" s="T86">plʼäka-lʼ</ta>
            <ta e="T88" id="Seg_4615" s="T87">qä-ntə</ta>
            <ta e="T89" id="Seg_4616" s="T88">*nʼäz-ol-ku-n</ta>
            <ta e="T90" id="Seg_4617" s="T89">ondə</ta>
            <ta e="T91" id="Seg_4618" s="T90">tʼärɨ-ku-n</ta>
            <ta e="T92" id="Seg_4619" s="T91">sej</ta>
            <ta e="T93" id="Seg_4620" s="T92">*ro-mbɨ-s</ta>
            <ta e="T94" id="Seg_4621" s="T93">sej-la</ta>
            <ta e="T95" id="Seg_4622" s="T94">täp-nä</ta>
            <ta e="T96" id="Seg_4623" s="T95">tüː-ku-nɨ-tɨn</ta>
            <ta e="T97" id="Seg_4624" s="T96">parɨdi-ku-nɨ-tɨn</ta>
            <ta e="T98" id="Seg_4625" s="T97">aj</ta>
            <ta e="T99" id="Seg_4626" s="T98">natʼe-lʼ</ta>
            <ta e="T100" id="Seg_4627" s="T99">qä-n</ta>
            <ta e="T101" id="Seg_4628" s="T100">par-ɨ-qɨn</ta>
            <ta e="T102" id="Seg_4629" s="T101">sej-n-tə</ta>
            <ta e="T103" id="Seg_4630" s="T102">paqə-ku-le</ta>
            <ta e="T104" id="Seg_4631" s="T103">qwɛdʼi-ku-t</ta>
            <ta e="T105" id="Seg_4632" s="T104">po-n</ta>
            <ta e="T106" id="Seg_4633" s="T105">moː-la-ntə</ta>
            <ta e="T107" id="Seg_4634" s="T106">ɨt-ku-t</ta>
            <ta e="T108" id="Seg_4635" s="T107">tʼärɨ-ku-n</ta>
            <ta e="T109" id="Seg_4636" s="T108">sej-la</ta>
            <ta e="T110" id="Seg_4637" s="T109">*ro-mbɨ-s</ta>
            <ta e="T111" id="Seg_4638" s="T110">sej-la</ta>
            <ta e="T112" id="Seg_4639" s="T111">täp-nä</ta>
            <ta e="T113" id="Seg_4640" s="T112">tüː-le</ta>
            <ta e="T114" id="Seg_4641" s="T113">*ro-ku-cʼ-ku-nɨ-tɨn</ta>
            <ta e="T116" id="Seg_4642" s="T114">nilʼdʼzʼaredɨn</ta>
            <ta e="T117" id="Seg_4643" s="T116">selɨj</ta>
            <ta e="T118" id="Seg_4644" s="T117">dʼel-tʼömb</ta>
            <ta e="T119" id="Seg_4645" s="T118">pünnɨ-n</ta>
            <ta e="T120" id="Seg_4646" s="T119">paja-tə</ta>
            <ta e="T121" id="Seg_4647" s="T120">*mantɨ-mbɨ-sɨ</ta>
            <ta e="T122" id="Seg_4648" s="T121">*mantɨ-mbɨ-sɨ</ta>
            <ta e="T123" id="Seg_4649" s="T122">tolɨ-ka-n</ta>
            <ta e="T124" id="Seg_4650" s="T123">era-tə</ta>
            <ta e="T125" id="Seg_4651" s="T124">sej-m-tə</ta>
            <ta e="T126" id="Seg_4652" s="T125">paqə-ntɨ-t</ta>
            <ta e="T127" id="Seg_4653" s="T126">taw</ta>
            <ta e="T128" id="Seg_4654" s="T127">plʼäka-lʼ</ta>
            <ta e="T129" id="Seg_4655" s="T128">qä-n</ta>
            <ta e="T130" id="Seg_4656" s="T129">qä-n-par-ntə</ta>
            <ta e="T131" id="Seg_4657" s="T130">qwɛdʼi-t</ta>
            <ta e="T132" id="Seg_4658" s="T131">a</ta>
            <ta e="T133" id="Seg_4659" s="T132">paja-tə</ta>
            <ta e="T134" id="Seg_4660" s="T133">tʼaŋ</ta>
            <ta e="T135" id="Seg_4661" s="T134">täp-ɨ-n</ta>
            <ta e="T136" id="Seg_4662" s="T135">sej-la-m-tə</ta>
            <ta e="T137" id="Seg_4663" s="T136">iː-ɨ-t</ta>
            <ta e="T139" id="Seg_4664" s="T138">ondə</ta>
            <ta e="T140" id="Seg_4665" s="T139">tärba-n</ta>
            <ta e="T141" id="Seg_4666" s="T140">a</ta>
            <ta e="T142" id="Seg_4667" s="T141">tan</ta>
            <ta e="T143" id="Seg_4668" s="T142">nanoː</ta>
            <ta e="T144" id="Seg_4669" s="T143">maːt-ntə</ta>
            <ta e="T145" id="Seg_4670" s="T144">qaj-m-näj</ta>
            <ta e="T146" id="Seg_4671" s="T145">asa</ta>
            <ta e="T147" id="Seg_4672" s="T146">tat-ku-nɨ-l</ta>
            <ta e="T148" id="Seg_4673" s="T147">mekka</ta>
            <ta e="T149" id="Seg_4674" s="T148">nʼilʼdʼi-ŋ</ta>
            <ta e="T150" id="Seg_4675" s="T149">paja-tə</ta>
            <ta e="T151" id="Seg_4676" s="T150">qwan-nɨ-n</ta>
            <ta e="T152" id="Seg_4677" s="T151">qwan-nɨ-n</ta>
            <ta e="T153" id="Seg_4678" s="T152">i</ta>
            <ta e="T154" id="Seg_4679" s="T153">tʼötʒɨkaŋ</ta>
            <ta e="T155" id="Seg_4680" s="T154">čaǯɨ-n</ta>
            <ta e="T156" id="Seg_4681" s="T155">era-ntɨ-nan</ta>
            <ta e="T157" id="Seg_4682" s="T156">sej-tə</ta>
            <ta e="T158" id="Seg_4683" s="T157">tidam</ta>
            <ta e="T159" id="Seg_4684" s="T158">tʼäkku-nɨ-n</ta>
            <ta e="T160" id="Seg_4685" s="T159">qwɛdʼi-gu</ta>
            <ta e="T161" id="Seg_4686" s="T160">täp-ɨ-m</ta>
            <ta e="T162" id="Seg_4687" s="T161">täp</ta>
            <ta e="T163" id="Seg_4688" s="T162">ör-ku-enǯɨ-n</ta>
            <ta e="T164" id="Seg_4689" s="T163">nanoː</ta>
            <ta e="T165" id="Seg_4690" s="T164">i</ta>
            <ta e="T166" id="Seg_4691" s="T165">tʼötʒɨkaŋ</ta>
            <ta e="T167" id="Seg_4692" s="T166">čaǯɨ-n</ta>
            <ta e="T168" id="Seg_4693" s="T167">paja-tə</ta>
            <ta e="T169" id="Seg_4694" s="T168">qussän</ta>
            <ta e="T170" id="Seg_4695" s="T169">čaǯɨ-n</ta>
            <ta e="T171" id="Seg_4696" s="T170">aj</ta>
            <ta e="T172" id="Seg_4697" s="T171">moqɨnä</ta>
            <ta e="T173" id="Seg_4698" s="T172">manǯu-dʼi-ku-n</ta>
            <ta e="T174" id="Seg_4699" s="T173">era-tə</ta>
            <ta e="T175" id="Seg_4700" s="T174">čaǯɨ-n</ta>
            <ta e="T176" id="Seg_4701" s="T175">alʼi</ta>
            <ta e="T177" id="Seg_4702" s="T176">asa</ta>
            <ta e="T178" id="Seg_4703" s="T177">paja-n-tə</ta>
            <ta e="T179" id="Seg_4704" s="T178">qwan-lʼewlʼe</ta>
            <ta e="T180" id="Seg_4705" s="T179">era-tə</ta>
            <ta e="T181" id="Seg_4706" s="T180">qussän</ta>
            <ta e="T182" id="Seg_4707" s="T181">laŋgoj-nɨ-n</ta>
            <ta e="T183" id="Seg_4708" s="T182">sej-la</ta>
            <ta e="T184" id="Seg_4709" s="T183">*ro-mbɨ-s</ta>
            <ta e="T185" id="Seg_4710" s="T184">sej-la</ta>
            <ta e="T186" id="Seg_4711" s="T185">tʼäkku-ku-nɨ-tɨn</ta>
            <ta e="T187" id="Seg_4712" s="T186">asa</ta>
            <ta e="T188" id="Seg_4713" s="T187">tüː-ku-nɨ-tɨn</ta>
            <ta e="T189" id="Seg_4714" s="T188">täp-nä</ta>
            <ta e="T190" id="Seg_4715" s="T189">i</ta>
            <ta e="T191" id="Seg_4716" s="T190">asa</ta>
            <ta e="T192" id="Seg_4717" s="T191">parɨdi-ku-nɨ-tɨn</ta>
            <ta e="T193" id="Seg_4718" s="T192">qussän</ta>
            <ta e="T194" id="Seg_4719" s="T193">sej-la-m-tə</ta>
            <ta e="T195" id="Seg_4720" s="T194">qwɨrɨ-sɨ-t</ta>
            <ta e="T196" id="Seg_4721" s="T195">qwɨrɨ-sɨ-t</ta>
            <ta e="T197" id="Seg_4722" s="T196">sej-la</ta>
            <ta e="T198" id="Seg_4723" s="T197">qundar</ta>
            <ta e="T199" id="Seg_4724" s="T198">tʼäkku-ku-nɨ-tɨn</ta>
            <ta e="T200" id="Seg_4725" s="T199">niŋ</ta>
            <ta e="T201" id="Seg_4726" s="T200">i</ta>
            <ta e="T202" id="Seg_4727" s="T201">tʼäkku-ku-nɨ-tɨn</ta>
            <ta e="T203" id="Seg_4728" s="T202">tidam</ta>
            <ta e="T204" id="Seg_4729" s="T203">sej-galɨ-ŋ</ta>
            <ta e="T205" id="Seg_4730" s="T204">era-tə</ta>
            <ta e="T206" id="Seg_4731" s="T205">na</ta>
            <ta e="T207" id="Seg_4732" s="T206">übɨdʼi-n</ta>
            <ta e="T208" id="Seg_4733" s="T207">sej-galɨ-ŋ</ta>
            <ta e="T209" id="Seg_4734" s="T208">paja-tə</ta>
            <ta e="T210" id="Seg_4735" s="T209">adɨ-lǯi-n</ta>
            <ta e="T211" id="Seg_4736" s="T210">era-ntɨ-nä</ta>
            <ta e="T212" id="Seg_4737" s="T211">asa</ta>
            <ta e="T213" id="Seg_4738" s="T212">aːdu-lǯi-kɨ-n</ta>
            <ta e="T214" id="Seg_4739" s="T213">era-tə</ta>
            <ta e="T215" id="Seg_4740" s="T214">sej-galɨ-ŋ</ta>
            <ta e="T216" id="Seg_4741" s="T215">ör-ku-le</ta>
            <ta e="T217" id="Seg_4742" s="T216">quː-enǯɨ-n</ta>
            <ta e="T218" id="Seg_4743" s="T217">maːt-ɨ-n</ta>
            <ta e="T219" id="Seg_4744" s="T218">kö-ntə</ta>
            <ta e="T220" id="Seg_4745" s="T219">midɨ-le</ta>
            <ta e="T221" id="Seg_4746" s="T220">übɨ-r-nɨ-qij</ta>
            <ta e="T222" id="Seg_4747" s="T221">paja-tə</ta>
            <ta e="T223" id="Seg_4748" s="T222">tɨdɨmbɨ-n</ta>
            <ta e="T224" id="Seg_4749" s="T223">tʼaŋ</ta>
            <ta e="T225" id="Seg_4750" s="T224">medə-dʼi-n</ta>
            <ta e="T226" id="Seg_4751" s="T225">tolʼdʼ-la-m-tə</ta>
            <ta e="T0" id="Seg_4752" s="T226">niŋ</ta>
            <ta e="T227" id="Seg_4753" s="T0">elle</ta>
            <ta e="T228" id="Seg_4754" s="T227">čaɣaǯu-nɨ-t</ta>
            <ta e="T229" id="Seg_4755" s="T228">maːt-ntə</ta>
            <ta e="T230" id="Seg_4756" s="T229">ser-nɨ-n</ta>
            <ta e="T231" id="Seg_4757" s="T230">nʼagɨ-nɨ-n</ta>
            <ta e="T232" id="Seg_4758" s="T231">era-n-tə</ta>
            <ta e="T233" id="Seg_4759" s="T232">sej-la-m</ta>
            <ta e="T234" id="Seg_4760" s="T233">tɨssɨ-ntə</ta>
            <ta e="T235" id="Seg_4761" s="T234">pen-nɨ-t</ta>
            <ta e="T236" id="Seg_4762" s="T235">pötte-n</ta>
            <ta e="T237" id="Seg_4763" s="T236">moqə-ntə</ta>
            <ta e="T238" id="Seg_4764" s="T237">odʼiŋ-nɨ-t</ta>
            <ta e="T239" id="Seg_4765" s="T238">era-tə</ta>
            <ta e="T240" id="Seg_4766" s="T239">tʼötʒɨkaŋ</ta>
            <ta e="T241" id="Seg_4767" s="T240">tüː-nɨ-n</ta>
            <ta e="T242" id="Seg_4768" s="T241">maːt-ntə</ta>
            <ta e="T243" id="Seg_4769" s="T242">ser-nɨ-n</ta>
            <ta e="T244" id="Seg_4770" s="T243">paja-tə</ta>
            <ta e="T245" id="Seg_4771" s="T244">tʼärɨ-n</ta>
            <ta e="T246" id="Seg_4772" s="T245">a</ta>
            <ta e="T247" id="Seg_4773" s="T246">tan</ta>
            <ta e="T248" id="Seg_4774" s="T247">qaj-no</ta>
            <ta e="T249" id="Seg_4775" s="T248">taw-dʼel</ta>
            <ta e="T250" id="Seg_4776" s="T249">kundɨ-ŋ</ta>
            <ta e="T251" id="Seg_4777" s="T250">eː-nɨ-ntə</ta>
            <ta e="T252" id="Seg_4778" s="T251">era-tə</ta>
            <ta e="T253" id="Seg_4779" s="T252">tʼärɨ-n</ta>
            <ta e="T254" id="Seg_4780" s="T253">man</ta>
            <ta e="T255" id="Seg_4781" s="T254">taw-dʼel</ta>
            <ta e="T256" id="Seg_4782" s="T255">sej-ɨ-w-ɨ-m</ta>
            <ta e="T257" id="Seg_4783" s="T256">aːtsa-ŋ</ta>
            <ta e="T258" id="Seg_4784" s="T257">nanoː</ta>
            <ta e="T259" id="Seg_4785" s="T258">kundɨ-ŋ</ta>
            <ta e="T260" id="Seg_4786" s="T259">eː-nɨ-ŋ</ta>
            <ta e="T261" id="Seg_4787" s="T260">paja-tə</ta>
            <ta e="T262" id="Seg_4788" s="T261">tärba-n</ta>
            <ta e="T263" id="Seg_4789" s="T262">a</ta>
            <ta e="T264" id="Seg_4790" s="T263">tan</ta>
            <ta e="T265" id="Seg_4791" s="T264">siːra-ntə</ta>
            <ta e="T266" id="Seg_4792" s="T265">man</ta>
            <ta e="T267" id="Seg_4793" s="T266">tidam</ta>
            <ta e="T268" id="Seg_4794" s="T267">tastɨ</ta>
            <ta e="T269" id="Seg_4795" s="T268">tonu-nɨ-ŋ</ta>
            <ta e="T270" id="Seg_4796" s="T269">qundar</ta>
            <ta e="T271" id="Seg_4797" s="T270">tan</ta>
            <ta e="T272" id="Seg_4798" s="T271">suːrɨ-nɨ-ntə</ta>
            <ta e="T273" id="Seg_4799" s="T272">selɨj</ta>
            <ta e="T274" id="Seg_4800" s="T273">dʼel-tʼömb</ta>
            <ta e="T275" id="Seg_4801" s="T274">sej-la-ntɨ-se</ta>
            <ta e="T276" id="Seg_4802" s="T275">warkɨ-ntə</ta>
            <ta e="T277" id="Seg_4803" s="T276">au</ta>
            <ta e="T278" id="Seg_4804" s="T277">qä-n</ta>
            <ta e="T279" id="Seg_4805" s="T278">par-ntə</ta>
            <ta e="T280" id="Seg_4806" s="T279">ɨt-ku-nɨ-l</ta>
            <ta e="T281" id="Seg_4807" s="T280">a</ta>
            <ta e="T282" id="Seg_4808" s="T281">taw</ta>
            <ta e="T283" id="Seg_4809" s="T282">plʼäka-lʼ</ta>
            <ta e="T284" id="Seg_4810" s="T283">kɨ-n</ta>
            <ta e="T285" id="Seg_4811" s="T284">par-ntə</ta>
            <ta e="T286" id="Seg_4812" s="T285">*nʼäz-ol-ku-nɨ-ntə</ta>
            <ta e="T287" id="Seg_4813" s="T286">sej-la-l</ta>
            <ta e="T288" id="Seg_4814" s="T287">tekka</ta>
            <ta e="T289" id="Seg_4815" s="T288">tüː-le</ta>
            <ta e="T290" id="Seg_4816" s="T289">parɨdi-ku-nɨ-tɨn</ta>
            <ta e="T291" id="Seg_4817" s="T290">nʼilʼdʼi-ŋ</ta>
            <ta e="T292" id="Seg_4818" s="T291">tan</ta>
            <ta e="T293" id="Seg_4819" s="T292">i</ta>
            <ta e="T294" id="Seg_4820" s="T293">sʼütdʼe-bɨ-ŋ</ta>
            <ta e="T295" id="Seg_4821" s="T294">tüː-ku-sɨ-ntə</ta>
            <ta e="T296" id="Seg_4822" s="T295">nu</ta>
            <ta e="T297" id="Seg_4823" s="T296">i</ta>
            <ta e="T298" id="Seg_4824" s="T297">warkɨ-le</ta>
            <ta e="T299" id="Seg_4825" s="T298">übɨ-r-nɨ-qij</ta>
            <ta e="T300" id="Seg_4826" s="T299">qusaj</ta>
            <ta e="T301" id="Seg_4827" s="T300">də</ta>
            <ta e="T302" id="Seg_4828" s="T301">dʼel-ɨ-tə</ta>
            <ta e="T303" id="Seg_4829" s="T302">mendɨ-n</ta>
            <ta e="T304" id="Seg_4830" s="T303">paja-tə</ta>
            <ta e="T305" id="Seg_4831" s="T304">manǯu-dʼi-n</ta>
            <ta e="T306" id="Seg_4832" s="T305">süderna-un</ta>
            <ta e="T307" id="Seg_4833" s="T306">manǯu-dʼi-n</ta>
            <ta e="T308" id="Seg_4834" s="T307">nɨ-nɨ-n</ta>
            <ta e="T309" id="Seg_4835" s="T308">peq</ta>
            <ta e="T310" id="Seg_4836" s="T309">paja-tə</ta>
            <ta e="T311" id="Seg_4837" s="T310">era-ntɨ-nä</ta>
            <ta e="T312" id="Seg_4838" s="T311">tʼärɨ-n</ta>
            <ta e="T313" id="Seg_4839" s="T312">peq</ta>
            <ta e="T314" id="Seg_4840" s="T313">nɨ-ku-ntɨ</ta>
            <ta e="T315" id="Seg_4841" s="T314">tan</ta>
            <ta e="T316" id="Seg_4842" s="T315">waᴣǝ-lä-ntə</ta>
            <ta e="T317" id="Seg_4843" s="T316">qundar-amə</ta>
            <ta e="T318" id="Seg_4844" s="T317">tʼäʒə-lä-l</ta>
            <ta e="T319" id="Seg_4845" s="T318">peq-ɨ-m</ta>
            <ta e="T320" id="Seg_4846" s="T319">a</ta>
            <ta e="T321" id="Seg_4847" s="T320">era-tə</ta>
            <ta e="T322" id="Seg_4848" s="T321">tʼärɨ-n</ta>
            <ta e="T323" id="Seg_4849" s="T322">man</ta>
            <ta e="T324" id="Seg_4850" s="T323">qundar</ta>
            <ta e="T325" id="Seg_4851" s="T324">tʼäʒə-enǯɨ-w</ta>
            <ta e="T326" id="Seg_4852" s="T325">sej-ɨ-w</ta>
            <ta e="T327" id="Seg_4853" s="T326">asa</ta>
            <ta e="T328" id="Seg_4854" s="T327">aːdu-n</ta>
            <ta e="T329" id="Seg_4855" s="T328">paja-tə</ta>
            <ta e="T330" id="Seg_4856" s="T329">tʼärɨ-n</ta>
            <ta e="T331" id="Seg_4857" s="T330">tüː-kɨ</ta>
            <ta e="T332" id="Seg_4858" s="T331">mekka</ta>
            <ta e="T333" id="Seg_4859" s="T332">man</ta>
            <ta e="T334" id="Seg_4860" s="T333">tülse-m</ta>
            <ta e="T335" id="Seg_4861" s="T334">tekka</ta>
            <ta e="T336" id="Seg_4862" s="T335">oral-ǯə-lǯi-lä-w-s</ta>
            <ta e="T337" id="Seg_4863" s="T336">i</ta>
            <ta e="T338" id="Seg_4864" s="T337">era-tə</ta>
            <ta e="T339" id="Seg_4865" s="T338">tüː-ɨ-n</ta>
            <ta e="T340" id="Seg_4866" s="T339">paja-ntɨ-nä</ta>
            <ta e="T341" id="Seg_4867" s="T340">paja-tə</ta>
            <ta e="T342" id="Seg_4868" s="T341">era-ntɨ-nä</ta>
            <ta e="T343" id="Seg_4869" s="T342">oral-ǯə-lǯi-t</ta>
            <ta e="T344" id="Seg_4870" s="T343">ut-qɨntɨ</ta>
            <ta e="T345" id="Seg_4871" s="T344">apsǝ-tɨ-mbɨdi</ta>
            <ta e="T346" id="Seg_4872" s="T345">tülse-m</ta>
            <ta e="T347" id="Seg_4873" s="T346">ondə</ta>
            <ta e="T348" id="Seg_4874" s="T347">täp-nä</ta>
            <ta e="T349" id="Seg_4875" s="T348">pɨnǯɨr-ɨ-t</ta>
            <ta e="T350" id="Seg_4876" s="T349">qundar</ta>
            <ta e="T351" id="Seg_4877" s="T350">tʼäʒə-gu</ta>
            <ta e="T352" id="Seg_4878" s="T351">peq-ɨ-m</ta>
            <ta e="T353" id="Seg_4879" s="T352">era-tə</ta>
            <ta e="T354" id="Seg_4880" s="T353">tʼäʒə-t</ta>
            <ta e="T355" id="Seg_4881" s="T354">qwat-nɨ-t</ta>
            <ta e="T356" id="Seg_4882" s="T355">peq-ɨ-m</ta>
            <ta e="T357" id="Seg_4883" s="T356">era-n-tə</ta>
            <ta e="T358" id="Seg_4884" s="T357">sej</ta>
            <ta e="T359" id="Seg_4885" s="T358">asa</ta>
            <ta e="T360" id="Seg_4886" s="T359">aːdu-n</ta>
            <ta e="T361" id="Seg_4887" s="T360">peq-ɨ-m</ta>
            <ta e="T362" id="Seg_4888" s="T361">täp-nä</ta>
            <ta e="T363" id="Seg_4889" s="T362">asa</ta>
            <ta e="T364" id="Seg_4890" s="T363">kɨr-gu</ta>
            <ta e="T365" id="Seg_4891" s="T364">paja-tə</ta>
            <ta e="T366" id="Seg_4892" s="T365">ondə</ta>
            <ta e="T367" id="Seg_4893" s="T366">kɨr-le</ta>
            <ta e="T368" id="Seg_4894" s="T367">übɨ-r-ɨ-t</ta>
            <ta e="T369" id="Seg_4895" s="T368">peq-ɨ-m</ta>
            <ta e="T370" id="Seg_4896" s="T369">nu</ta>
            <ta e="T371" id="Seg_4897" s="T370">i</ta>
            <ta e="T372" id="Seg_4898" s="T371">kɨr-ɨ-t</ta>
            <ta e="T373" id="Seg_4899" s="T372">peq-ɨ-m</ta>
            <ta e="T374" id="Seg_4900" s="T373">wadʼi-la-m</ta>
            <ta e="T375" id="Seg_4901" s="T374">nʼörbal-nɨ-t</ta>
            <ta e="T376" id="Seg_4902" s="T375">ɨt-ɨ-t</ta>
            <ta e="T377" id="Seg_4903" s="T376">okkɨr</ta>
            <ta e="T378" id="Seg_4904" s="T377">dʼel</ta>
            <ta e="T379" id="Seg_4905" s="T378">wadʼi-la-m</ta>
            <ta e="T380" id="Seg_4906" s="T379">koːci-ŋ</ta>
            <ta e="T381" id="Seg_4907" s="T380">poːt-nɨ-t</ta>
            <ta e="T382" id="Seg_4908" s="T381">era-ntɨ-nä</ta>
            <ta e="T383" id="Seg_4909" s="T382">tʼärɨ-n</ta>
            <ta e="T384" id="Seg_4910" s="T383">taw-dʼel</ta>
            <ta e="T385" id="Seg_4911" s="T384">qum-la</ta>
            <ta e="T386" id="Seg_4912" s="T385">tʼärɨ-tɨn</ta>
            <ta e="T387" id="Seg_4913" s="T386">taw-dʼel-lʼ</ta>
            <ta e="T388" id="Seg_4914" s="T387">dʼel</ta>
            <ta e="T389" id="Seg_4915" s="T388">iːbəgaj</ta>
            <ta e="T390" id="Seg_4916" s="T389">nom-lʼ-dʼel</ta>
            <ta e="T391" id="Seg_4917" s="T390">era-tə</ta>
            <ta e="T392" id="Seg_4918" s="T391">tʼärɨ-n</ta>
            <ta e="T393" id="Seg_4919" s="T392">nom-lʼ-dʼel-ču-kɨ</ta>
            <ta e="T394" id="Seg_4920" s="T393">nu</ta>
            <ta e="T395" id="Seg_4921" s="T394">i</ta>
            <ta e="T396" id="Seg_4922" s="T395">nom-lʼ-dʼel-ču-le</ta>
            <ta e="T397" id="Seg_4923" s="T396">übɨ-r-nɨ-qij</ta>
            <ta e="T398" id="Seg_4924" s="T397">paja-tə</ta>
            <ta e="T399" id="Seg_4925" s="T398">poːne-n</ta>
            <ta e="T400" id="Seg_4926" s="T399">era-tə</ta>
            <ta e="T401" id="Seg_4927" s="T400">maːt-qɨn</ta>
            <ta e="T402" id="Seg_4928" s="T401">eː-nɨ-n</ta>
            <ta e="T403" id="Seg_4929" s="T402">paja-tə</ta>
            <ta e="T404" id="Seg_4930" s="T403">poːne-n</ta>
            <ta e="T405" id="Seg_4931" s="T404">ät-ɨ-t</ta>
            <ta e="T406" id="Seg_4932" s="T405">qugur-san-ɨ-m</ta>
            <ta e="T407" id="Seg_4933" s="T406">qugur-ku-či-le</ta>
            <ta e="T408" id="Seg_4934" s="T407">übɨ-r-nɨ-n</ta>
            <ta e="T409" id="Seg_4935" s="T408">era-tə</ta>
            <ta e="T410" id="Seg_4936" s="T409">maːt-qɨn</ta>
            <ta e="T411" id="Seg_4937" s="T410">maːt-s-ku-n</ta>
            <ta e="T412" id="Seg_4938" s="T411">a</ta>
            <ta e="T413" id="Seg_4939" s="T412">paja</ta>
            <ta e="T414" id="Seg_4940" s="T413">poːne-n</ta>
            <ta e="T415" id="Seg_4941" s="T414">qugur-či-n</ta>
            <ta e="T416" id="Seg_4942" s="T415">i</ta>
            <ta e="T417" id="Seg_4943" s="T416">qugur-či-n</ta>
            <ta e="T418" id="Seg_4944" s="T417">a</ta>
            <ta e="T419" id="Seg_4945" s="T418">ondə</ta>
            <ta e="T420" id="Seg_4946" s="T419">čagɨ-mbɨdi</ta>
            <ta e="T421" id="Seg_4947" s="T420">peq-ɨ-n</ta>
            <ta e="T422" id="Seg_4948" s="T421">wadʼi</ta>
            <ta e="T423" id="Seg_4949" s="T422">am-r-nɨ-n</ta>
            <ta e="T424" id="Seg_4950" s="T423">ondə</ta>
            <ta e="T425" id="Seg_4951" s="T424">nom-n</ta>
            <ta e="T426" id="Seg_4952" s="T425">qojmɨ-n</ta>
            <ta e="T427" id="Seg_4953" s="T426">ket-le</ta>
            <ta e="T428" id="Seg_4954" s="T427">tat-ɨ-r-ɨ-t</ta>
            <ta e="T429" id="Seg_4955" s="T428">qamba-lʼ</ta>
            <ta e="T430" id="Seg_4956" s="T429">dʼel-ɨ-tə</ta>
            <ta e="T431" id="Seg_4957" s="T430">tʼümbɨ-cʼ-ku</ta>
            <ta e="T432" id="Seg_4958" s="T431">era-tə</ta>
            <ta e="T433" id="Seg_4959" s="T432">maːt-qɨn</ta>
            <ta e="T434" id="Seg_4960" s="T433">amdɨ-sɨ</ta>
            <ta e="T435" id="Seg_4961" s="T434">amdɨ-sɨ</ta>
            <ta e="T436" id="Seg_4962" s="T435">kertɨmnɨ-n</ta>
            <ta e="T437" id="Seg_4963" s="T436">i</ta>
            <ta e="T438" id="Seg_4964" s="T437">tärba-n</ta>
            <ta e="T439" id="Seg_4965" s="T438">man</ta>
            <ta e="T440" id="Seg_4966" s="T439">sej-la-w-ɨ-m</ta>
            <ta e="T441" id="Seg_4967" s="T440">qwɨrɨ-lä-w</ta>
            <ta e="T442" id="Seg_4968" s="T441">sej-la</ta>
            <ta e="T443" id="Seg_4969" s="T442">moʒət_bɨtʼ</ta>
            <ta e="T444" id="Seg_4970" s="T443">mekka</ta>
            <ta e="T445" id="Seg_4971" s="T444">tüː-lʼeze-qij</ta>
            <ta e="T446" id="Seg_4972" s="T445">tüː-lʼeze-tɨn</ta>
            <ta e="T447" id="Seg_4973" s="T446">nu</ta>
            <ta e="T448" id="Seg_4974" s="T447">nʼilʼdʼi-ŋ</ta>
            <ta e="T449" id="Seg_4975" s="T448">i</ta>
            <ta e="T450" id="Seg_4976" s="T449">meː-nɨ-t</ta>
            <ta e="T451" id="Seg_4977" s="T450">sej-la-m-tə</ta>
            <ta e="T452" id="Seg_4978" s="T451">qwɨrɨ-le</ta>
            <ta e="T453" id="Seg_4979" s="T452">übɨ-r-nɨ-t</ta>
            <ta e="T454" id="Seg_4980" s="T453">qaj-ta</ta>
            <ta e="T455" id="Seg_4981" s="T454">pötte-n</ta>
            <ta e="T456" id="Seg_4982" s="T455">moqə-n</ta>
            <ta e="T457" id="Seg_4983" s="T456">raksəmbə-n</ta>
            <ta e="T458" id="Seg_4984" s="T457">täp</ta>
            <ta e="T459" id="Seg_4985" s="T458">soːdʼiga-ŋ</ta>
            <ta e="T460" id="Seg_4986" s="T459">üngu-lǯi-n</ta>
            <ta e="T461" id="Seg_4987" s="T460">ondə</ta>
            <ta e="T462" id="Seg_4988" s="T461">tärba-n</ta>
            <ta e="T463" id="Seg_4989" s="T462">qaj</ta>
            <ta e="T464" id="Seg_4990" s="T463">natʼe-n</ta>
            <ta e="T465" id="Seg_4991" s="T464">raksəmbə-ntɨ</ta>
            <ta e="T466" id="Seg_4992" s="T465">täp</ta>
            <ta e="T467" id="Seg_4993" s="T466">aj</ta>
            <ta e="T468" id="Seg_4994" s="T467">sədə-mǯʼeːli</ta>
            <ta e="T469" id="Seg_4995" s="T468">tʼärɨ-n</ta>
            <ta e="T470" id="Seg_4996" s="T469">sej-la</ta>
            <ta e="T471" id="Seg_4997" s="T470">*ro-mbɨ-s</ta>
            <ta e="T472" id="Seg_4998" s="T471">a</ta>
            <ta e="T473" id="Seg_4999" s="T472">pötte-n</ta>
            <ta e="T474" id="Seg_5000" s="T473">moqə-n</ta>
            <ta e="T475" id="Seg_5001" s="T474">wargɨ-ŋ</ta>
            <ta e="T476" id="Seg_5002" s="T475">raksəmbə-nɨ-n</ta>
            <ta e="T477" id="Seg_5003" s="T476">qaj-ta</ta>
            <ta e="T478" id="Seg_5004" s="T477">era-tə</ta>
            <ta e="T479" id="Seg_5005" s="T478">natʼe-n</ta>
            <ta e="T480" id="Seg_5006" s="T479">tʼür-ɨ-se</ta>
            <ta e="T481" id="Seg_5007" s="T480">tʼötʒɨkaŋ</ta>
            <ta e="T482" id="Seg_5008" s="T481">qwan-nɨ-n</ta>
            <ta e="T483" id="Seg_5009" s="T482">pötte-n</ta>
            <ta e="T484" id="Seg_5010" s="T483">moqə-n</ta>
            <ta e="T485" id="Seg_5011" s="T484">pündər-ɨ-mbɨ-n</ta>
            <ta e="T486" id="Seg_5012" s="T485">qo-ɨ-t</ta>
            <ta e="T487" id="Seg_5013" s="T486">tɨssɨ-m</ta>
            <ta e="T488" id="Seg_5014" s="T487">omdɨ-n</ta>
            <ta e="T489" id="Seg_5015" s="T488">na</ta>
            <ta e="T490" id="Seg_5016" s="T489">tɨssɨ-m</ta>
            <ta e="T491" id="Seg_5017" s="T490">tʼekkə-t</ta>
            <ta e="T492" id="Seg_5018" s="T491">i</ta>
            <ta e="T493" id="Seg_5019" s="T492">pügolǯu-mbɨ-t</ta>
            <ta e="T494" id="Seg_5020" s="T493">qaj-ta</ta>
            <ta e="T495" id="Seg_5021" s="T494">sədə</ta>
            <ta e="T496" id="Seg_5022" s="T495">pör-la-čka</ta>
            <ta e="T497" id="Seg_5023" s="T496">taw</ta>
            <ta e="T498" id="Seg_5024" s="T497">odnako</ta>
            <ta e="T499" id="Seg_5025" s="T498">man-nan-lʼ</ta>
            <ta e="T500" id="Seg_5026" s="T499">sej-la</ta>
            <ta e="T501" id="Seg_5027" s="T500">pʼötta</ta>
            <ta e="T502" id="Seg_5028" s="T501">üt-ɨ-m</ta>
            <ta e="T503" id="Seg_5029" s="T502">qo-nɨ-t</ta>
            <ta e="T504" id="Seg_5030" s="T503">era-tə</ta>
            <ta e="T505" id="Seg_5031" s="T504">üt-ntə</ta>
            <ta e="T506" id="Seg_5032" s="T505">tʼäptə-ptɨ-t</ta>
            <ta e="T507" id="Seg_5033" s="T506">sej-la-tə</ta>
            <ta e="T508" id="Seg_5034" s="T507">sədə</ta>
            <ta e="T509" id="Seg_5035" s="T508">sej</ta>
            <ta e="T510" id="Seg_5036" s="T509">qussän</ta>
            <ta e="T511" id="Seg_5037" s="T510">üt-qɨn</ta>
            <ta e="T512" id="Seg_5038" s="T511">tʼäptə-dʼi-mbɨ-sɨ-tɨn</ta>
            <ta e="T513" id="Seg_5039" s="T512">saʒaŋ</ta>
            <ta e="T514" id="Seg_5040" s="T513">müzulǯu-t</ta>
            <ta e="T515" id="Seg_5041" s="T514">sej-la-qɨntɨ</ta>
            <ta e="T516" id="Seg_5042" s="T515">tʼärɨ-n</ta>
            <ta e="T517" id="Seg_5043" s="T516">sej-la</ta>
            <ta e="T518" id="Seg_5044" s="T517">*ro-mbɨ-s</ta>
            <ta e="T519" id="Seg_5045" s="T518">sej-la-tə</ta>
            <ta e="T520" id="Seg_5046" s="T519">täp-ɨ-nä</ta>
            <ta e="T521" id="Seg_5047" s="T520">parɨdi-tɨn</ta>
            <ta e="T522" id="Seg_5048" s="T521">manǯu-dʼi-n</ta>
            <ta e="T523" id="Seg_5049" s="T522">a</ta>
            <ta e="T524" id="Seg_5050" s="T523">sej-la-tə</ta>
            <ta e="T525" id="Seg_5051" s="T524">soːdʼiga-ŋ</ta>
            <ta e="T526" id="Seg_5052" s="T525">asa</ta>
            <ta e="T527" id="Seg_5053" s="T526">aːdu-tɨn</ta>
            <ta e="T528" id="Seg_5054" s="T527">sej-la-m-tə</ta>
            <ta e="T529" id="Seg_5055" s="T528">paqə-nɨ-t</ta>
            <ta e="T530" id="Seg_5056" s="T529">aj</ta>
            <ta e="T531" id="Seg_5057" s="T530">tʼäptə-ptɨ-t</ta>
            <ta e="T532" id="Seg_5058" s="T531">soːdʼiga-ŋ</ta>
            <ta e="T533" id="Seg_5059" s="T532">sej-la-m-tə</ta>
            <ta e="T534" id="Seg_5060" s="T533">aj</ta>
            <ta e="T535" id="Seg_5061" s="T534">soːdʼiga-ŋ</ta>
            <ta e="T536" id="Seg_5062" s="T535">müzel-nɨ-t</ta>
            <ta e="T537" id="Seg_5063" s="T536">tʼärɨ-n</ta>
            <ta e="T538" id="Seg_5064" s="T537">sej-la</ta>
            <ta e="T539" id="Seg_5065" s="T538">*ro-mbɨ-s</ta>
            <ta e="T540" id="Seg_5066" s="T539">sej-la-tə</ta>
            <ta e="T541" id="Seg_5067" s="T540">parɨdi-tɨn</ta>
            <ta e="T542" id="Seg_5068" s="T541">kössə</ta>
            <ta e="T543" id="Seg_5069" s="T542">täbe-qum</ta>
            <ta e="T544" id="Seg_5070" s="T543">aːndal-nɨ-n</ta>
            <ta e="T545" id="Seg_5071" s="T544">tidam</ta>
            <ta e="T546" id="Seg_5072" s="T545">sej-la-w</ta>
            <ta e="T547" id="Seg_5073" s="T546">soːdʼiga-ŋ</ta>
            <ta e="T548" id="Seg_5074" s="T547">qo-nǯir-nɨ-tɨn</ta>
            <ta e="T549" id="Seg_5075" s="T548">tidam</ta>
            <ta e="T550" id="Seg_5076" s="T549">man</ta>
            <ta e="T551" id="Seg_5077" s="T550">sej-se</ta>
            <ta e="T552" id="Seg_5078" s="T551">azu-ŋ</ta>
            <ta e="T553" id="Seg_5079" s="T552">uʒ</ta>
            <ta e="T554" id="Seg_5080" s="T553">man</ta>
            <ta e="T555" id="Seg_5081" s="T554">tastɨ</ta>
            <ta e="T556" id="Seg_5082" s="T555">sej-la-qɨn</ta>
            <ta e="T557" id="Seg_5083" s="T556">tʼаːt</ta>
            <ta e="T558" id="Seg_5084" s="T557">qoːba-lä-ŋ-s</ta>
            <ta e="T559" id="Seg_5085" s="T558">poːne</ta>
            <ta e="T560" id="Seg_5086" s="T559">čanǯu-n</ta>
            <ta e="T561" id="Seg_5087" s="T560">a</ta>
            <ta e="T562" id="Seg_5088" s="T561">paja-tə</ta>
            <ta e="T563" id="Seg_5089" s="T562">qugur-ku-či-n</ta>
            <ta e="T564" id="Seg_5090" s="T563">nom</ta>
            <ta e="T565" id="Seg_5091" s="T564">qojmɨ-m</ta>
            <ta e="T566" id="Seg_5092" s="T565">tat-ɨ-r-ɨ-t</ta>
            <ta e="T567" id="Seg_5093" s="T566">era-tə</ta>
            <ta e="T568" id="Seg_5094" s="T567">tʼärɨ-n</ta>
            <ta e="T569" id="Seg_5095" s="T568">paja-ntɨ-nä</ta>
            <ta e="T570" id="Seg_5096" s="T569">a</ta>
            <ta e="T571" id="Seg_5097" s="T570">tan</ta>
            <ta e="T572" id="Seg_5098" s="T571">mazɨm</ta>
            <ta e="T573" id="Seg_5099" s="T572">qaj-no</ta>
            <ta e="T574" id="Seg_5100" s="T573">sejgulǯu-ntə</ta>
            <ta e="T575" id="Seg_5101" s="T574">a</ta>
            <ta e="T576" id="Seg_5102" s="T575">paja-tə</ta>
            <ta e="T577" id="Seg_5103" s="T576">tʼärɨ-n</ta>
            <ta e="T578" id="Seg_5104" s="T577">a</ta>
            <ta e="T579" id="Seg_5105" s="T578">tan</ta>
            <ta e="T580" id="Seg_5106" s="T579">qaj-no</ta>
            <ta e="T581" id="Seg_5107" s="T580">nʼilʼdʼi-ŋ</ta>
            <ta e="T582" id="Seg_5108" s="T581">palʼdʼi-ku-sɨ-ntə</ta>
            <ta e="T583" id="Seg_5109" s="T582">*qöːš-ku-sɨ-ntə</ta>
            <ta e="T584" id="Seg_5110" s="T583">suːrum-s-gu</ta>
            <ta e="T585" id="Seg_5111" s="T584">a</ta>
            <ta e="T586" id="Seg_5112" s="T585">onendǝ</ta>
            <ta e="T587" id="Seg_5113" s="T586">sej-la-ntɨ-se</ta>
            <ta e="T588" id="Seg_5114" s="T587">saːnǯər-sɨ-ntə</ta>
            <ta e="T589" id="Seg_5115" s="T588">a</ta>
            <ta e="T590" id="Seg_5116" s="T589">maːt-ntə</ta>
            <ta e="T591" id="Seg_5117" s="T590">qaj-m-näj</ta>
            <ta e="T592" id="Seg_5118" s="T591">asa</ta>
            <ta e="T593" id="Seg_5119" s="T592">tat-ku-sɨ-l</ta>
            <ta e="T594" id="Seg_5120" s="T593">ot</ta>
            <ta e="T595" id="Seg_5121" s="T594">man</ta>
            <ta e="T596" id="Seg_5122" s="T595">tan-nan-lʼ</ta>
            <ta e="T597" id="Seg_5123" s="T596">sej-la-m-tə</ta>
            <ta e="T598" id="Seg_5124" s="T597">nanoː</ta>
            <ta e="T599" id="Seg_5125" s="T598">i</ta>
            <ta e="T600" id="Seg_5126" s="T599">paqə-qɨl-nɨ-w</ta>
            <ta e="T601" id="Seg_5127" s="T600">tat-nɨ-w</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_5128" s="T1">live-PST-3DU.S</ta>
            <ta e="T3" id="Seg_5129" s="T2">live-PST-3DU.S</ta>
            <ta e="T4" id="Seg_5130" s="T3">husband.[NOM]</ta>
            <ta e="T5" id="Seg_5131" s="T4">old.woman-DYA-DU.[NOM]</ta>
            <ta e="T6" id="Seg_5132" s="T5">husband.[NOM]-3SG</ta>
            <ta e="T7" id="Seg_5133" s="T6">walk-HAB-3SG.S</ta>
            <ta e="T8" id="Seg_5134" s="T7">hunt-CVB</ta>
            <ta e="T9" id="Seg_5135" s="T8">wife.[NOM]-3SG</ta>
            <ta e="T10" id="Seg_5136" s="T9">house-LOC</ta>
            <ta e="T11" id="Seg_5137" s="T10">be-CO-3SG.S</ta>
            <ta e="T12" id="Seg_5138" s="T11">husband.[NOM]-3SG</ta>
            <ta e="T13" id="Seg_5139" s="T12">walk-HAB-3SG.S</ta>
            <ta e="T14" id="Seg_5140" s="T13">walk-HAB-3SG.S</ta>
            <ta e="T15" id="Seg_5141" s="T14">day-during</ta>
            <ta e="T16" id="Seg_5142" s="T15">walk-HAB-3SG.S</ta>
            <ta e="T17" id="Seg_5143" s="T16">what-ACC-EMPH</ta>
            <ta e="T18" id="Seg_5144" s="T17">NEG</ta>
            <ta e="T19" id="Seg_5145" s="T18">bring-HAB-3SG.O</ta>
            <ta e="T20" id="Seg_5146" s="T19">all.the.time</ta>
            <ta e="T21" id="Seg_5147" s="T20">so</ta>
            <ta e="T22" id="Seg_5148" s="T21">walk-HAB-3SG.S</ta>
            <ta e="T23" id="Seg_5149" s="T22">what-ACC-EMPH</ta>
            <ta e="T24" id="Seg_5150" s="T23">NEG</ta>
            <ta e="T25" id="Seg_5151" s="T24">bring-HAB-3SG.O</ta>
            <ta e="T26" id="Seg_5152" s="T25">one</ta>
            <ta e="T27" id="Seg_5153" s="T26">day.[NOM]</ta>
            <ta e="T28" id="Seg_5154" s="T27">wife.[NOM]-3SG</ta>
            <ta e="T29" id="Seg_5155" s="T28">think-3SG.S</ta>
            <ta e="T30" id="Seg_5156" s="T29">what-TRL</ta>
            <ta e="T31" id="Seg_5157" s="T30">EMPH</ta>
            <ta e="T32" id="Seg_5158" s="T31">(s)he.[NOM]</ta>
            <ta e="T33" id="Seg_5159" s="T32">what-ACC-EMPH</ta>
            <ta e="T34" id="Seg_5160" s="T33">NEG</ta>
            <ta e="T35" id="Seg_5161" s="T34">bring-HAB-3SG.O</ta>
            <ta e="T36" id="Seg_5162" s="T35">I.NOM</ta>
            <ta e="T37" id="Seg_5163" s="T36">(s)he-EP-ACC</ta>
            <ta e="T38" id="Seg_5164" s="T37">follow-FUT-1SG.O</ta>
            <ta e="T39" id="Seg_5165" s="T38">what-TRL</ta>
            <ta e="T40" id="Seg_5166" s="T39">(s)he.[NOM]</ta>
            <ta e="T41" id="Seg_5167" s="T40">become.empty-PTCP.PST-ADVZ</ta>
            <ta e="T42" id="Seg_5168" s="T41">come-HAB-3SG.S</ta>
            <ta e="T43" id="Seg_5169" s="T42">now</ta>
            <ta e="T44" id="Seg_5170" s="T43">and</ta>
            <ta e="T45" id="Seg_5171" s="T44">so-ADVZ</ta>
            <ta e="T46" id="Seg_5172" s="T45">do-CO-3SG.O</ta>
            <ta e="T47" id="Seg_5173" s="T46">husband.[NOM]-3SG</ta>
            <ta e="T48" id="Seg_5174" s="T47">set.off-CVB</ta>
            <ta e="T49" id="Seg_5175" s="T48">begin-DRV-CO-3SG.S</ta>
            <ta e="T50" id="Seg_5176" s="T49">hunt-CVB</ta>
            <ta e="T51" id="Seg_5177" s="T50">leave-CO-3SG.S</ta>
            <ta e="T52" id="Seg_5178" s="T51">wife.[NOM]-3SG</ta>
            <ta e="T53" id="Seg_5179" s="T52">fast</ta>
            <ta e="T54" id="Seg_5180" s="T53">ski-EP-ACC-3SG</ta>
            <ta e="T55" id="Seg_5181" s="T54">put.on-CO-3SG.O</ta>
            <ta e="T56" id="Seg_5182" s="T55">husband-GEN-3SG</ta>
            <ta e="T57" id="Seg_5183" s="T56">behind</ta>
            <ta e="T58" id="Seg_5184" s="T57">fast</ta>
            <ta e="T59" id="Seg_5185" s="T58">set.off-3SG.S</ta>
            <ta e="T60" id="Seg_5186" s="T59">husband-OBL.3SG-ALL</ta>
            <ta e="T61" id="Seg_5187" s="T60">NEG</ta>
            <ta e="T62" id="Seg_5188" s="T61">be.visible-TR-RFL-3SG.S</ta>
            <ta e="T63" id="Seg_5189" s="T62">that.is.why</ta>
            <ta e="T64" id="Seg_5190" s="T63">NEG</ta>
            <ta e="T65" id="Seg_5191" s="T64">be.visible-TR-RFL-3SG.S</ta>
            <ta e="T66" id="Seg_5192" s="T65">(s)he-EP-ACC</ta>
            <ta e="T67" id="Seg_5193" s="T66">husband.[NOM]-3SG</ta>
            <ta e="T68" id="Seg_5194" s="T67">NEG.IMP</ta>
            <ta e="T69" id="Seg_5195" s="T68">see-DRV-CONJ-3SG.O</ta>
            <ta e="T70" id="Seg_5196" s="T69">husband.[NOM]-3SG</ta>
            <ta e="T71" id="Seg_5197" s="T70">achieve-3SG.S</ta>
            <ta e="T72" id="Seg_5198" s="T71">Chaya-GEN</ta>
            <ta e="T73" id="Seg_5199" s="T72">steep.bank-GEN-top-ILL</ta>
            <ta e="T74" id="Seg_5200" s="T73">this</ta>
            <ta e="T75" id="Seg_5201" s="T74">side-ADJZ</ta>
            <ta e="T76" id="Seg_5202" s="T75">steep.bank-GEN</ta>
            <ta e="T77" id="Seg_5203" s="T76">top-LOC</ta>
            <ta e="T78" id="Seg_5204" s="T77">own.3SG</ta>
            <ta e="T79" id="Seg_5205" s="T78">eye-ACC-3SG</ta>
            <ta e="T80" id="Seg_5206" s="T79">dig.out-DRV-HAB-3SG.O</ta>
            <ta e="T81" id="Seg_5207" s="T80">eye-PL-ACC-3SG</ta>
            <ta e="T82" id="Seg_5208" s="T81">hang-CVB</ta>
            <ta e="T83" id="Seg_5209" s="T82">leave-HAB-3SG.O</ta>
            <ta e="T84" id="Seg_5210" s="T83">tree-GEN</ta>
            <ta e="T85" id="Seg_5211" s="T84">bough-PL-LOC</ta>
            <ta e="T86" id="Seg_5212" s="T85">that</ta>
            <ta e="T87" id="Seg_5213" s="T86">side-ADJZ</ta>
            <ta e="T88" id="Seg_5214" s="T87">steep.bank-ILL</ta>
            <ta e="T89" id="Seg_5215" s="T88">roll-MOM-HAB-3SG.S</ta>
            <ta e="T90" id="Seg_5216" s="T89">oneself.3SG</ta>
            <ta e="T91" id="Seg_5217" s="T90">say-HAB-3SG.S</ta>
            <ta e="T92" id="Seg_5218" s="T91">eye.[NOM]</ta>
            <ta e="T93" id="Seg_5219" s="T92">stick.to-DUR-OPT.[3SG.S]</ta>
            <ta e="T94" id="Seg_5220" s="T93">eye-PL.[NOM]</ta>
            <ta e="T95" id="Seg_5221" s="T94">(s)he-ALL</ta>
            <ta e="T96" id="Seg_5222" s="T95">come-HAB-CO-3PL</ta>
            <ta e="T97" id="Seg_5223" s="T96">stick-HAB-CO-3PL</ta>
            <ta e="T98" id="Seg_5224" s="T97">again</ta>
            <ta e="T99" id="Seg_5225" s="T98">there-ADJZ</ta>
            <ta e="T100" id="Seg_5226" s="T99">steep.bank-GEN</ta>
            <ta e="T101" id="Seg_5227" s="T100">top-EP-LOC</ta>
            <ta e="T102" id="Seg_5228" s="T101">eye-GEN-3SG</ta>
            <ta e="T103" id="Seg_5229" s="T102">dig.out-HAB-CVB</ta>
            <ta e="T104" id="Seg_5230" s="T103">leave-HAB-3SG.O</ta>
            <ta e="T105" id="Seg_5231" s="T104">tree-GEN</ta>
            <ta e="T106" id="Seg_5232" s="T105">bough-PL-ILL</ta>
            <ta e="T107" id="Seg_5233" s="T106">hang-HAB-3SG.O</ta>
            <ta e="T108" id="Seg_5234" s="T107">say-HAB-3SG.S</ta>
            <ta e="T109" id="Seg_5235" s="T108">eye-PL.[NOM]</ta>
            <ta e="T110" id="Seg_5236" s="T109">stick.to-DUR-OPT.[3SG.S]</ta>
            <ta e="T111" id="Seg_5237" s="T110">eye-PL.[NOM]</ta>
            <ta e="T112" id="Seg_5238" s="T111">(s)he-ALL</ta>
            <ta e="T113" id="Seg_5239" s="T112">come-CVB</ta>
            <ta e="T114" id="Seg_5240" s="T113">stick.to-HAB-DRV-HAB-CO-3PL</ta>
            <ta e="T116" id="Seg_5241" s="T114">%так</ta>
            <ta e="T117" id="Seg_5242" s="T116">whole</ta>
            <ta e="T118" id="Seg_5243" s="T117">day-during</ta>
            <ta e="T119" id="Seg_5244" s="T118">%%-3SG.S</ta>
            <ta e="T120" id="Seg_5245" s="T119">wife.[NOM]-3SG</ta>
            <ta e="T121" id="Seg_5246" s="T120">look-DUR-PST.[3SG.S]</ta>
            <ta e="T122" id="Seg_5247" s="T121">look-DUR-PST.[3SG.S]</ta>
            <ta e="T123" id="Seg_5248" s="T122">%steal-DIM-3SG.S</ta>
            <ta e="T124" id="Seg_5249" s="T123">husband.[NOM]-3SG</ta>
            <ta e="T125" id="Seg_5250" s="T124">eye-ACC-3SG</ta>
            <ta e="T126" id="Seg_5251" s="T125">dig.out-INFER-3SG.O</ta>
            <ta e="T127" id="Seg_5252" s="T126">this</ta>
            <ta e="T128" id="Seg_5253" s="T127">side-ADJZ</ta>
            <ta e="T129" id="Seg_5254" s="T128">steep.bank-GEN</ta>
            <ta e="T130" id="Seg_5255" s="T129">steep.bank-GEN-top-ILL</ta>
            <ta e="T131" id="Seg_5256" s="T130">leave-3SG.O</ta>
            <ta e="T132" id="Seg_5257" s="T131">and</ta>
            <ta e="T133" id="Seg_5258" s="T132">wife.[NOM]-3SG</ta>
            <ta e="T134" id="Seg_5259" s="T133">fast</ta>
            <ta e="T135" id="Seg_5260" s="T134">(s)he-EP-GEN</ta>
            <ta e="T136" id="Seg_5261" s="T135">eye-PL-ACC-3SG</ta>
            <ta e="T137" id="Seg_5262" s="T136">take-EP-3SG.O</ta>
            <ta e="T139" id="Seg_5263" s="T138">oneself.3SG</ta>
            <ta e="T140" id="Seg_5264" s="T139">think-3SG.O</ta>
            <ta e="T141" id="Seg_5265" s="T140">so</ta>
            <ta e="T142" id="Seg_5266" s="T141">you.SG.NOM</ta>
            <ta e="T143" id="Seg_5267" s="T142">that.is.why</ta>
            <ta e="T144" id="Seg_5268" s="T143">house-ILL</ta>
            <ta e="T145" id="Seg_5269" s="T144">what-ACC-EMPH</ta>
            <ta e="T146" id="Seg_5270" s="T145">NEG</ta>
            <ta e="T147" id="Seg_5271" s="T146">bring-HAB-CO-2SG.O</ta>
            <ta e="T148" id="Seg_5272" s="T147">I.ALL</ta>
            <ta e="T149" id="Seg_5273" s="T148">so-ADVZ</ta>
            <ta e="T150" id="Seg_5274" s="T149">wife.[NOM]-3SG</ta>
            <ta e="T151" id="Seg_5275" s="T150">leave-CO-3SG.S</ta>
            <ta e="T152" id="Seg_5276" s="T151">leave-CO-3SG.S</ta>
            <ta e="T153" id="Seg_5277" s="T152">and</ta>
            <ta e="T154" id="Seg_5278" s="T153">slowly</ta>
            <ta e="T155" id="Seg_5279" s="T154">go-3SG.S</ta>
            <ta e="T156" id="Seg_5280" s="T155">husband-OBL.3SG-ADES</ta>
            <ta e="T157" id="Seg_5281" s="T156">eye.[NOM]-3SG</ta>
            <ta e="T158" id="Seg_5282" s="T157">now</ta>
            <ta e="T159" id="Seg_5283" s="T158">NEG.EX-CO-3SG.S</ta>
            <ta e="T160" id="Seg_5284" s="T159">leave-INF</ta>
            <ta e="T161" id="Seg_5285" s="T160">(s)he-EP-ACC</ta>
            <ta e="T162" id="Seg_5286" s="T161">(s)he.[NOM]</ta>
            <ta e="T163" id="Seg_5287" s="T162">get.lost-HAB-FUT-3SG.S</ta>
            <ta e="T164" id="Seg_5288" s="T163">that.is.why</ta>
            <ta e="T165" id="Seg_5289" s="T164">and</ta>
            <ta e="T166" id="Seg_5290" s="T165">slowly</ta>
            <ta e="T167" id="Seg_5291" s="T166">go-3SG.S</ta>
            <ta e="T168" id="Seg_5292" s="T167">wife.[NOM]-3SG</ta>
            <ta e="T169" id="Seg_5293" s="T168">how.many</ta>
            <ta e="T170" id="Seg_5294" s="T169">go-3SG.S</ta>
            <ta e="T171" id="Seg_5295" s="T170">again</ta>
            <ta e="T172" id="Seg_5296" s="T171">back</ta>
            <ta e="T173" id="Seg_5297" s="T172">look.at-DRV-HAB-3SG.S</ta>
            <ta e="T174" id="Seg_5298" s="T173">husband.[NOM]-3SG</ta>
            <ta e="T175" id="Seg_5299" s="T174">go-3SG.S</ta>
            <ta e="T176" id="Seg_5300" s="T175">or</ta>
            <ta e="T177" id="Seg_5301" s="T176">NEG</ta>
            <ta e="T178" id="Seg_5302" s="T177">wife-GEN-3SG</ta>
            <ta e="T179" id="Seg_5303" s="T178">leave-CVB2</ta>
            <ta e="T180" id="Seg_5304" s="T179">husband.[NOM]-3SG</ta>
            <ta e="T181" id="Seg_5305" s="T180">so.many</ta>
            <ta e="T182" id="Seg_5306" s="T181">begin.to.cry-CO-3SG.S</ta>
            <ta e="T183" id="Seg_5307" s="T182">eye-PL.[NOM]</ta>
            <ta e="T184" id="Seg_5308" s="T183">stick.to-DUR-OPT.[3SG.S]</ta>
            <ta e="T185" id="Seg_5309" s="T184">eye-PL.[NOM]</ta>
            <ta e="T186" id="Seg_5310" s="T185">NEG.EX-HAB-CO-3PL</ta>
            <ta e="T187" id="Seg_5311" s="T186">NEG</ta>
            <ta e="T188" id="Seg_5312" s="T187">come-HAB-CO-3PL</ta>
            <ta e="T189" id="Seg_5313" s="T188">(s)he-ALL</ta>
            <ta e="T190" id="Seg_5314" s="T189">and</ta>
            <ta e="T191" id="Seg_5315" s="T190">NEG</ta>
            <ta e="T192" id="Seg_5316" s="T191">stick-HAB-CO-3PL</ta>
            <ta e="T193" id="Seg_5317" s="T192">how.many</ta>
            <ta e="T194" id="Seg_5318" s="T193">eye-PL-ACC-3SG</ta>
            <ta e="T195" id="Seg_5319" s="T194">call-PST-3SG.O</ta>
            <ta e="T196" id="Seg_5320" s="T195">call-PST-3SG.O</ta>
            <ta e="T197" id="Seg_5321" s="T196">eye-PL.[NOM]</ta>
            <ta e="T198" id="Seg_5322" s="T197">how</ta>
            <ta e="T199" id="Seg_5323" s="T198">NEG.EX-HAB-CO-3PL</ta>
            <ta e="T200" id="Seg_5324" s="T199">so</ta>
            <ta e="T201" id="Seg_5325" s="T200">and</ta>
            <ta e="T202" id="Seg_5326" s="T201">NEG.EX-HAB-CO-3PL</ta>
            <ta e="T203" id="Seg_5327" s="T202">now</ta>
            <ta e="T204" id="Seg_5328" s="T203">eye-CAR-ADVZ</ta>
            <ta e="T205" id="Seg_5329" s="T204">husband.[NOM]-3SG</ta>
            <ta e="T206" id="Seg_5330" s="T205">here</ta>
            <ta e="T207" id="Seg_5331" s="T206">set.off-3SG.S</ta>
            <ta e="T208" id="Seg_5332" s="T207">eye-CAR-ADVZ</ta>
            <ta e="T209" id="Seg_5333" s="T208">wife.[NOM]-3SG</ta>
            <ta e="T210" id="Seg_5334" s="T209">wait-TR-3SG.S</ta>
            <ta e="T211" id="Seg_5335" s="T210">husband-OBL.3SG-ALL</ta>
            <ta e="T212" id="Seg_5336" s="T211">NEG</ta>
            <ta e="T213" id="Seg_5337" s="T212">be.visible-TR-RFL-3SG.S</ta>
            <ta e="T214" id="Seg_5338" s="T213">husband.[NOM]-3SG</ta>
            <ta e="T215" id="Seg_5339" s="T214">eye-CAR-ADVZ</ta>
            <ta e="T216" id="Seg_5340" s="T215">get.lost-HAB-CVB</ta>
            <ta e="T217" id="Seg_5341" s="T216">die-FUT-3SG.S</ta>
            <ta e="T218" id="Seg_5342" s="T217">house-EP-GEN</ta>
            <ta e="T219" id="Seg_5343" s="T218">side-ILL</ta>
            <ta e="T220" id="Seg_5344" s="T219">approach-CVB</ta>
            <ta e="T221" id="Seg_5345" s="T220">begin-DRV-CO-3DU.S</ta>
            <ta e="T222" id="Seg_5346" s="T221">wife.[NOM]-3SG</ta>
            <ta e="T223" id="Seg_5347" s="T222">hurry-3SG.S</ta>
            <ta e="T224" id="Seg_5348" s="T223">fast</ta>
            <ta e="T225" id="Seg_5349" s="T224">achieve-DRV-3SG.S</ta>
            <ta e="T226" id="Seg_5350" s="T225">ski-PL-ACC-3SG</ta>
            <ta e="T0" id="Seg_5351" s="T226">so</ta>
            <ta e="T227" id="Seg_5352" s="T0">down</ta>
            <ta e="T228" id="Seg_5353" s="T227">drive.into-CO-3SG.O</ta>
            <ta e="T229" id="Seg_5354" s="T228">house-ILL</ta>
            <ta e="T230" id="Seg_5355" s="T229">come.in-CO-3SG.S</ta>
            <ta e="T231" id="Seg_5356" s="T230">undress-CO-3SG.S</ta>
            <ta e="T232" id="Seg_5357" s="T231">husband-GEN-3SG</ta>
            <ta e="T233" id="Seg_5358" s="T232">eye-PL-ACC</ta>
            <ta e="T234" id="Seg_5359" s="T233">vessel-ILL</ta>
            <ta e="T235" id="Seg_5360" s="T234">put-CO-3SG.O</ta>
            <ta e="T236" id="Seg_5361" s="T235">stove-GEN</ta>
            <ta e="T237" id="Seg_5362" s="T236">space.behind-ILL</ta>
            <ta e="T238" id="Seg_5363" s="T237">hide-CO-3SG.O</ta>
            <ta e="T239" id="Seg_5364" s="T238">husband.[NOM]-3SG</ta>
            <ta e="T240" id="Seg_5365" s="T239">slowly</ta>
            <ta e="T241" id="Seg_5366" s="T240">come-CO-3SG.S</ta>
            <ta e="T242" id="Seg_5367" s="T241">house-ILL</ta>
            <ta e="T243" id="Seg_5368" s="T242">come.in-CO-3SG.S</ta>
            <ta e="T244" id="Seg_5369" s="T243">wife.[NOM]-3SG</ta>
            <ta e="T245" id="Seg_5370" s="T244">say-3SG.S</ta>
            <ta e="T246" id="Seg_5371" s="T245">and</ta>
            <ta e="T247" id="Seg_5372" s="T246">you.SG.NOM</ta>
            <ta e="T248" id="Seg_5373" s="T247">what-TRL</ta>
            <ta e="T249" id="Seg_5374" s="T248">this-day.[NOM]</ta>
            <ta e="T250" id="Seg_5375" s="T249">long-ADVZ</ta>
            <ta e="T251" id="Seg_5376" s="T250">be-CO-2SG.S</ta>
            <ta e="T252" id="Seg_5377" s="T251">husband.[NOM]-3SG</ta>
            <ta e="T253" id="Seg_5378" s="T252">say-3SG.S</ta>
            <ta e="T254" id="Seg_5379" s="T253">I.NOM</ta>
            <ta e="T255" id="Seg_5380" s="T254">this-day.[NOM]</ta>
            <ta e="T256" id="Seg_5381" s="T255">eye-EP-1SG-EP-ACC</ta>
            <ta e="T257" id="Seg_5382" s="T256">%%-1SG.S</ta>
            <ta e="T258" id="Seg_5383" s="T257">that.is.why</ta>
            <ta e="T259" id="Seg_5384" s="T258">long-ADVZ</ta>
            <ta e="T260" id="Seg_5385" s="T259">be-CO-1SG.S</ta>
            <ta e="T261" id="Seg_5386" s="T260">wife.[NOM]-3SG</ta>
            <ta e="T262" id="Seg_5387" s="T261">think-3SG.S</ta>
            <ta e="T263" id="Seg_5388" s="T262">and</ta>
            <ta e="T264" id="Seg_5389" s="T263">you.SG.NOM</ta>
            <ta e="T265" id="Seg_5390" s="T264">%%-2SG.S</ta>
            <ta e="T266" id="Seg_5391" s="T265">I.NOM</ta>
            <ta e="T267" id="Seg_5392" s="T266">now</ta>
            <ta e="T268" id="Seg_5393" s="T267">you.SG.ACC</ta>
            <ta e="T269" id="Seg_5394" s="T268">know-CO-1SG.S</ta>
            <ta e="T270" id="Seg_5395" s="T269">how</ta>
            <ta e="T271" id="Seg_5396" s="T270">you.SG.NOM</ta>
            <ta e="T272" id="Seg_5397" s="T271">hunt-CO-2SG.S</ta>
            <ta e="T273" id="Seg_5398" s="T272">whole</ta>
            <ta e="T274" id="Seg_5399" s="T273">sun-during</ta>
            <ta e="T275" id="Seg_5400" s="T274">eye-PL-OBL.2SG-COM</ta>
            <ta e="T276" id="Seg_5401" s="T275">%play-2SG.S</ta>
            <ta e="T277" id="Seg_5402" s="T276">other</ta>
            <ta e="T278" id="Seg_5403" s="T277">steep.bank-GEN</ta>
            <ta e="T279" id="Seg_5404" s="T278">top-ILL</ta>
            <ta e="T280" id="Seg_5405" s="T279">hang-HAB-CO-2SG.O</ta>
            <ta e="T281" id="Seg_5406" s="T280">and</ta>
            <ta e="T282" id="Seg_5407" s="T281">this</ta>
            <ta e="T283" id="Seg_5408" s="T282">side-ADJZ</ta>
            <ta e="T284" id="Seg_5409" s="T283">river-ADV.LOC</ta>
            <ta e="T285" id="Seg_5410" s="T284">top-ILL</ta>
            <ta e="T286" id="Seg_5411" s="T285">roll-MOM-HAB-CO-2SG.S</ta>
            <ta e="T287" id="Seg_5412" s="T286">eye-PL.[NOM]-2SG</ta>
            <ta e="T288" id="Seg_5413" s="T287">you.ALL</ta>
            <ta e="T289" id="Seg_5414" s="T288">come-CVB</ta>
            <ta e="T290" id="Seg_5415" s="T289">stick-HAB-CO-3PL</ta>
            <ta e="T291" id="Seg_5416" s="T290">so-ADVZ</ta>
            <ta e="T292" id="Seg_5417" s="T291">you.SG.NOM</ta>
            <ta e="T293" id="Seg_5418" s="T292">and</ta>
            <ta e="T294" id="Seg_5419" s="T293">become.empty-PTCP.PST-ADVZ</ta>
            <ta e="T295" id="Seg_5420" s="T294">come-HAB-PST-2SG.S</ta>
            <ta e="T296" id="Seg_5421" s="T295">now</ta>
            <ta e="T297" id="Seg_5422" s="T296">and</ta>
            <ta e="T298" id="Seg_5423" s="T297">live-CVB</ta>
            <ta e="T299" id="Seg_5424" s="T298">begin-DRV-CO-3DU.S</ta>
            <ta e="T300" id="Seg_5425" s="T299">how.many</ta>
            <ta e="T301" id="Seg_5426" s="T300">EMPH</ta>
            <ta e="T302" id="Seg_5427" s="T301">sun.[NOM]-EP-3SG</ta>
            <ta e="T303" id="Seg_5428" s="T302">pass-3SG.S</ta>
            <ta e="T304" id="Seg_5429" s="T303">wife.[NOM]-3SG</ta>
            <ta e="T305" id="Seg_5430" s="T304">look.at-DRV-3SG.S</ta>
            <ta e="T306" id="Seg_5431" s="T305">window-PROL</ta>
            <ta e="T307" id="Seg_5432" s="T306">look.at-DRV-3SG.S</ta>
            <ta e="T308" id="Seg_5433" s="T307">stand-CO-3SG.S</ta>
            <ta e="T309" id="Seg_5434" s="T308">elk.[NOM]</ta>
            <ta e="T310" id="Seg_5435" s="T309">wife.[NOM]-3SG</ta>
            <ta e="T311" id="Seg_5436" s="T310">husband-OBL.3SG-ALL</ta>
            <ta e="T312" id="Seg_5437" s="T311">say-3SG.S</ta>
            <ta e="T313" id="Seg_5438" s="T312">elk.[NOM]</ta>
            <ta e="T314" id="Seg_5439" s="T313">stand-HAB-INFER.[3SG.S]</ta>
            <ta e="T315" id="Seg_5440" s="T314">you.SG.NOM</ta>
            <ta e="T316" id="Seg_5441" s="T315">get.up-OPT-2SG.S</ta>
            <ta e="T317" id="Seg_5442" s="T316">how-INDEF2</ta>
            <ta e="T318" id="Seg_5443" s="T317">shoot-OPT-2SG.O</ta>
            <ta e="T319" id="Seg_5444" s="T318">elk-EP-ACC</ta>
            <ta e="T320" id="Seg_5445" s="T319">and</ta>
            <ta e="T321" id="Seg_5446" s="T320">husband.[NOM]-3SG</ta>
            <ta e="T322" id="Seg_5447" s="T321">say-3SG.S</ta>
            <ta e="T323" id="Seg_5448" s="T322">I.NOM</ta>
            <ta e="T324" id="Seg_5449" s="T323">how</ta>
            <ta e="T325" id="Seg_5450" s="T324">shoot-FUT-1SG.O</ta>
            <ta e="T326" id="Seg_5451" s="T325">eye.[NOM]-EP-1SG</ta>
            <ta e="T327" id="Seg_5452" s="T326">NEG</ta>
            <ta e="T328" id="Seg_5453" s="T327">see-3SG.S</ta>
            <ta e="T329" id="Seg_5454" s="T328">wife.[NOM]-3SG</ta>
            <ta e="T330" id="Seg_5455" s="T329">say-3SG.S</ta>
            <ta e="T331" id="Seg_5456" s="T330">come-IMP.2SG.S</ta>
            <ta e="T332" id="Seg_5457" s="T331">I.ALL</ta>
            <ta e="T333" id="Seg_5458" s="T332">I.NOM</ta>
            <ta e="T334" id="Seg_5459" s="T333">rifle-ACC</ta>
            <ta e="T335" id="Seg_5460" s="T334">you.ALL</ta>
            <ta e="T336" id="Seg_5461" s="T335">take-DRV-TR-FUT-1SG.O-FUT</ta>
            <ta e="T337" id="Seg_5462" s="T336">and</ta>
            <ta e="T338" id="Seg_5463" s="T337">husband.[NOM]-3SG</ta>
            <ta e="T339" id="Seg_5464" s="T338">come-EP-3SG.S</ta>
            <ta e="T340" id="Seg_5465" s="T339">old.woman-OBL.3SG-ALL</ta>
            <ta e="T341" id="Seg_5466" s="T340">wife.[NOM]-3SG</ta>
            <ta e="T342" id="Seg_5467" s="T341">husband-OBL.3SG-ALL</ta>
            <ta e="T343" id="Seg_5468" s="T342">take-DRV-TR-3SG.O</ta>
            <ta e="T344" id="Seg_5469" s="T343">hand-ILL.3SG</ta>
            <ta e="T345" id="Seg_5470" s="T344">food-VBLZ-PTCP.PST</ta>
            <ta e="T346" id="Seg_5471" s="T345">rifle-ACC</ta>
            <ta e="T347" id="Seg_5472" s="T346">oneself.3SG</ta>
            <ta e="T348" id="Seg_5473" s="T347">(s)he-ALL</ta>
            <ta e="T349" id="Seg_5474" s="T348">show-EP-3SG.O</ta>
            <ta e="T350" id="Seg_5475" s="T349">how</ta>
            <ta e="T351" id="Seg_5476" s="T350">shoot-INF</ta>
            <ta e="T352" id="Seg_5477" s="T351">elk-EP-ACC</ta>
            <ta e="T353" id="Seg_5478" s="T352">husband.[NOM]-3SG</ta>
            <ta e="T354" id="Seg_5479" s="T353">shoot.at-3SG.O</ta>
            <ta e="T355" id="Seg_5480" s="T354">kill-CO-3SG.O</ta>
            <ta e="T356" id="Seg_5481" s="T355">elk-EP-ACC</ta>
            <ta e="T357" id="Seg_5482" s="T356">husband-GEN-3SG</ta>
            <ta e="T358" id="Seg_5483" s="T357">eye.[NOM]</ta>
            <ta e="T359" id="Seg_5484" s="T358">NEG</ta>
            <ta e="T360" id="Seg_5485" s="T359">see-3SG.S</ta>
            <ta e="T361" id="Seg_5486" s="T360">elk-EP-ACC</ta>
            <ta e="T362" id="Seg_5487" s="T361">(s)he-ALL</ta>
            <ta e="T363" id="Seg_5488" s="T362">NEG</ta>
            <ta e="T364" id="Seg_5489" s="T363">skin-INF</ta>
            <ta e="T365" id="Seg_5490" s="T364">wife.[NOM]-3SG</ta>
            <ta e="T366" id="Seg_5491" s="T365">oneself.3SG</ta>
            <ta e="T367" id="Seg_5492" s="T366">skin-CVB</ta>
            <ta e="T368" id="Seg_5493" s="T367">begin-DRV-EP-3SG.O</ta>
            <ta e="T369" id="Seg_5494" s="T368">elk-EP-ACC</ta>
            <ta e="T370" id="Seg_5495" s="T369">now</ta>
            <ta e="T371" id="Seg_5496" s="T370">and</ta>
            <ta e="T372" id="Seg_5497" s="T371">skin-EP-3SG.O</ta>
            <ta e="T373" id="Seg_5498" s="T372">elk-EP-ACC</ta>
            <ta e="T374" id="Seg_5499" s="T373">meat-PL-ACC</ta>
            <ta e="T375" id="Seg_5500" s="T374">scour-CO-3SG.O</ta>
            <ta e="T376" id="Seg_5501" s="T375">hang-EP-3SG.O</ta>
            <ta e="T377" id="Seg_5502" s="T376">one</ta>
            <ta e="T378" id="Seg_5503" s="T377">day.[NOM]</ta>
            <ta e="T379" id="Seg_5504" s="T378">meat-PL-ACC</ta>
            <ta e="T380" id="Seg_5505" s="T379">much-ADVZ</ta>
            <ta e="T381" id="Seg_5506" s="T380">cook-CO-3SG.O</ta>
            <ta e="T382" id="Seg_5507" s="T381">husband-OBL.3SG-ALL</ta>
            <ta e="T383" id="Seg_5508" s="T382">say-3SG.S</ta>
            <ta e="T384" id="Seg_5509" s="T383">this-day.[NOM]</ta>
            <ta e="T385" id="Seg_5510" s="T384">human.being-PL.[NOM]</ta>
            <ta e="T386" id="Seg_5511" s="T385">say-3PL</ta>
            <ta e="T387" id="Seg_5512" s="T386">this-day-ADJZ</ta>
            <ta e="T388" id="Seg_5513" s="T387">day.[NOM]</ta>
            <ta e="T389" id="Seg_5514" s="T388">big</ta>
            <ta e="T390" id="Seg_5515" s="T389">god-ADJZ-day.[NOM]</ta>
            <ta e="T391" id="Seg_5516" s="T390">husband.[NOM]-3SG</ta>
            <ta e="T392" id="Seg_5517" s="T391">say-3SG.S</ta>
            <ta e="T393" id="Seg_5518" s="T392">god-ADJZ-day-TR-IMP.2SG.S</ta>
            <ta e="T394" id="Seg_5519" s="T393">now</ta>
            <ta e="T395" id="Seg_5520" s="T394">and</ta>
            <ta e="T396" id="Seg_5521" s="T395">god-ADJZ-day-VBLZ-CVB</ta>
            <ta e="T397" id="Seg_5522" s="T396">begin-DRV-CO-3DU.S</ta>
            <ta e="T398" id="Seg_5523" s="T397">wife.[NOM]-3SG</ta>
            <ta e="T399" id="Seg_5524" s="T398">outward(s)-ADV.LOC</ta>
            <ta e="T400" id="Seg_5525" s="T399">husband.[NOM]-3SG</ta>
            <ta e="T401" id="Seg_5526" s="T400">house-LOC</ta>
            <ta e="T402" id="Seg_5527" s="T401">be-CO-3SG.S</ta>
            <ta e="T403" id="Seg_5528" s="T402">wife.[NOM]-3SG</ta>
            <ta e="T404" id="Seg_5529" s="T403">outward(s)-ADV.LOC</ta>
            <ta e="T405" id="Seg_5530" s="T404">hang-EP-3SG.O</ta>
            <ta e="T406" id="Seg_5531" s="T405">swing-INSTRN-EP-ACC</ta>
            <ta e="T407" id="Seg_5532" s="T406">swing-HAB-RFL-CVB</ta>
            <ta e="T408" id="Seg_5533" s="T407">begin-DRV-CO-3SG.S</ta>
            <ta e="T409" id="Seg_5534" s="T408">husband.[NOM]-3SG</ta>
            <ta e="T410" id="Seg_5535" s="T409">house-LOC</ta>
            <ta e="T411" id="Seg_5536" s="T410">tent-CAP-HAB-3SG.S</ta>
            <ta e="T412" id="Seg_5537" s="T411">and</ta>
            <ta e="T413" id="Seg_5538" s="T412">wife.[NOM]</ta>
            <ta e="T414" id="Seg_5539" s="T413">outward(s)-ADV.LOC</ta>
            <ta e="T415" id="Seg_5540" s="T414">swing-RFL-3SG.S</ta>
            <ta e="T416" id="Seg_5541" s="T415">and</ta>
            <ta e="T417" id="Seg_5542" s="T416">swing-RFL-3SG.S</ta>
            <ta e="T418" id="Seg_5543" s="T417">and</ta>
            <ta e="T419" id="Seg_5544" s="T418">oneself.3SG</ta>
            <ta e="T420" id="Seg_5545" s="T419">dry-PTCP.PST</ta>
            <ta e="T421" id="Seg_5546" s="T420">elk-EP-GEN</ta>
            <ta e="T422" id="Seg_5547" s="T421">meat.[NOM]</ta>
            <ta e="T423" id="Seg_5548" s="T422">eat-FRQ-CO-3SG.S</ta>
            <ta e="T424" id="Seg_5549" s="T423">oneself.3SG</ta>
            <ta e="T425" id="Seg_5550" s="T424">sky-GEN</ta>
            <ta e="T426" id="Seg_5551" s="T425">song-GEN</ta>
            <ta e="T427" id="Seg_5552" s="T426">say-CVB</ta>
            <ta e="T428" id="Seg_5553" s="T427">bring-EP-FRQ-EP-3SG.O</ta>
            <ta e="T429" id="Seg_5554" s="T428">spring-ADJZ</ta>
            <ta e="T430" id="Seg_5555" s="T429">sun.[NOM]-EP-3SG</ta>
            <ta e="T431" id="Seg_5556" s="T430">long-VBLZ-HAB.[3SG.S]</ta>
            <ta e="T432" id="Seg_5557" s="T431">husband.[NOM]-3SG</ta>
            <ta e="T433" id="Seg_5558" s="T432">house-LOC</ta>
            <ta e="T434" id="Seg_5559" s="T433">sit-PST.[3SG.S]</ta>
            <ta e="T435" id="Seg_5560" s="T434">sit-PST.[3SG.S]</ta>
            <ta e="T436" id="Seg_5561" s="T435">%get.tired-3SG.S</ta>
            <ta e="T437" id="Seg_5562" s="T436">and</ta>
            <ta e="T438" id="Seg_5563" s="T437">think-3SG.S</ta>
            <ta e="T439" id="Seg_5564" s="T438">I.NOM</ta>
            <ta e="T440" id="Seg_5565" s="T439">eye-PL-1SG-EP-ACC</ta>
            <ta e="T441" id="Seg_5566" s="T440">call-OPT-1SG.O</ta>
            <ta e="T442" id="Seg_5567" s="T441">eye-PL.[NOM]</ta>
            <ta e="T443" id="Seg_5568" s="T442">maybe</ta>
            <ta e="T444" id="Seg_5569" s="T443">I.ALL</ta>
            <ta e="T445" id="Seg_5570" s="T444">come-OPT-3DU.S</ta>
            <ta e="T446" id="Seg_5571" s="T445">come-OPT-3PL</ta>
            <ta e="T447" id="Seg_5572" s="T446">now</ta>
            <ta e="T448" id="Seg_5573" s="T447">so-ADVZ</ta>
            <ta e="T449" id="Seg_5574" s="T448">and</ta>
            <ta e="T450" id="Seg_5575" s="T449">do-CO-3SG.O</ta>
            <ta e="T451" id="Seg_5576" s="T450">eye-PL-ACC-3SG</ta>
            <ta e="T452" id="Seg_5577" s="T451">call-CVB</ta>
            <ta e="T453" id="Seg_5578" s="T452">begin-DRV-CO-3SG.O</ta>
            <ta e="T454" id="Seg_5579" s="T453">what-INDEF.[NOM]</ta>
            <ta e="T455" id="Seg_5580" s="T454">stove-GEN</ta>
            <ta e="T456" id="Seg_5581" s="T455">space.behind-ADV.LOC</ta>
            <ta e="T457" id="Seg_5582" s="T456">rattle-3SG.S</ta>
            <ta e="T458" id="Seg_5583" s="T457">(s)he.[NOM]</ta>
            <ta e="T459" id="Seg_5584" s="T458">good-ADVZ</ta>
            <ta e="T460" id="Seg_5585" s="T459">listen-PFV-3SG.S</ta>
            <ta e="T461" id="Seg_5586" s="T460">oneself.3SG</ta>
            <ta e="T462" id="Seg_5587" s="T461">think-3SG.S</ta>
            <ta e="T463" id="Seg_5588" s="T462">what.[NOM]</ta>
            <ta e="T464" id="Seg_5589" s="T463">there-ADV.LOC</ta>
            <ta e="T465" id="Seg_5590" s="T464">rattle-INFER.[3SG.S]</ta>
            <ta e="T466" id="Seg_5591" s="T465">(s)he.[NOM]</ta>
            <ta e="T467" id="Seg_5592" s="T466">again</ta>
            <ta e="T468" id="Seg_5593" s="T467">two-ORD</ta>
            <ta e="T469" id="Seg_5594" s="T468">say-3SG.S</ta>
            <ta e="T470" id="Seg_5595" s="T469">eye-PL.[NOM]</ta>
            <ta e="T471" id="Seg_5596" s="T470">stick.to-DUR-OPT.[3SG.S]</ta>
            <ta e="T472" id="Seg_5597" s="T471">and</ta>
            <ta e="T473" id="Seg_5598" s="T472">stove-GEN</ta>
            <ta e="T474" id="Seg_5599" s="T473">space.behind-ADV.LOC</ta>
            <ta e="T475" id="Seg_5600" s="T474">big-ADVZ</ta>
            <ta e="T476" id="Seg_5601" s="T475">rattle-CO-3SG.S</ta>
            <ta e="T477" id="Seg_5602" s="T476">what-INDEF.[NOM]</ta>
            <ta e="T478" id="Seg_5603" s="T477">husband.[NOM]-3SG</ta>
            <ta e="T479" id="Seg_5604" s="T478">there-ADV.LOC</ta>
            <ta e="T480" id="Seg_5605" s="T479">stick-EP-COM</ta>
            <ta e="T481" id="Seg_5606" s="T480">slowly</ta>
            <ta e="T482" id="Seg_5607" s="T481">leave-CO-3SG.S</ta>
            <ta e="T483" id="Seg_5608" s="T482">stove-GEN</ta>
            <ta e="T484" id="Seg_5609" s="T483">space.behind-ADV.LOC</ta>
            <ta e="T485" id="Seg_5610" s="T484">touch-EP-DUR-3SG.S</ta>
            <ta e="T486" id="Seg_5611" s="T485">find-EP-3SG.O</ta>
            <ta e="T487" id="Seg_5612" s="T486">vessel-ACC</ta>
            <ta e="T488" id="Seg_5613" s="T487">sit.down-3SG.S</ta>
            <ta e="T489" id="Seg_5614" s="T488">this</ta>
            <ta e="T490" id="Seg_5615" s="T489">vessel-ACC</ta>
            <ta e="T491" id="Seg_5616" s="T490">untie-3SG.O</ta>
            <ta e="T492" id="Seg_5617" s="T491">and</ta>
            <ta e="T493" id="Seg_5618" s="T492">touch-DUR-3SG.O</ta>
            <ta e="T494" id="Seg_5619" s="T493">what-INDEF.[NOM]</ta>
            <ta e="T495" id="Seg_5620" s="T494">two</ta>
            <ta e="T496" id="Seg_5621" s="T495">circle-PL-ATTEN.[NOM]</ta>
            <ta e="T497" id="Seg_5622" s="T496">this</ta>
            <ta e="T498" id="Seg_5623" s="T497">however</ta>
            <ta e="T499" id="Seg_5624" s="T498">I-ADES-ADJZ</ta>
            <ta e="T500" id="Seg_5625" s="T499">eye-PL.[NOM]</ta>
            <ta e="T501" id="Seg_5626" s="T500">hot</ta>
            <ta e="T502" id="Seg_5627" s="T501">water-EP-ACC</ta>
            <ta e="T503" id="Seg_5628" s="T502">find-CO-3SG.O</ta>
            <ta e="T504" id="Seg_5629" s="T503">husband.[NOM]-3SG</ta>
            <ta e="T505" id="Seg_5630" s="T504">water-ILL</ta>
            <ta e="T506" id="Seg_5631" s="T505">get.wet-CAUS-3SG.O</ta>
            <ta e="T507" id="Seg_5632" s="T506">eye-PL.[NOM]-3SG</ta>
            <ta e="T508" id="Seg_5633" s="T507">two</ta>
            <ta e="T509" id="Seg_5634" s="T508">eye.[NOM]</ta>
            <ta e="T510" id="Seg_5635" s="T509">so.many</ta>
            <ta e="T511" id="Seg_5636" s="T510">water-LOC</ta>
            <ta e="T512" id="Seg_5637" s="T511">get.wet-DRV-DUR-PST-3PL</ta>
            <ta e="T513" id="Seg_5638" s="T512">strongly</ta>
            <ta e="T514" id="Seg_5639" s="T513">wash-3SG.O</ta>
            <ta e="T515" id="Seg_5640" s="T514">eye-PL-ILL.3SG</ta>
            <ta e="T516" id="Seg_5641" s="T515">say-3SG.S</ta>
            <ta e="T517" id="Seg_5642" s="T516">eye-PL.[NOM]</ta>
            <ta e="T518" id="Seg_5643" s="T517">stick.to-DUR-OPT.[3SG.S]</ta>
            <ta e="T519" id="Seg_5644" s="T518">eye-PL.[NOM]-3SG</ta>
            <ta e="T520" id="Seg_5645" s="T519">(s)he-EP-ALL</ta>
            <ta e="T521" id="Seg_5646" s="T520">stick-3PL</ta>
            <ta e="T522" id="Seg_5647" s="T521">look.at-DRV-3SG.S</ta>
            <ta e="T523" id="Seg_5648" s="T522">and</ta>
            <ta e="T524" id="Seg_5649" s="T523">eye-PL.[NOM]-3SG</ta>
            <ta e="T525" id="Seg_5650" s="T524">good-ADVZ</ta>
            <ta e="T526" id="Seg_5651" s="T525">NEG</ta>
            <ta e="T527" id="Seg_5652" s="T526">see-3PL</ta>
            <ta e="T528" id="Seg_5653" s="T527">eye-PL-ACC-3SG</ta>
            <ta e="T529" id="Seg_5654" s="T528">dig.out-CO-3SG.O</ta>
            <ta e="T530" id="Seg_5655" s="T529">again</ta>
            <ta e="T531" id="Seg_5656" s="T530">get.wet-CAUS-3SG.O</ta>
            <ta e="T532" id="Seg_5657" s="T531">good-ADVZ</ta>
            <ta e="T533" id="Seg_5658" s="T532">eye-PL-ACC-3SG</ta>
            <ta e="T534" id="Seg_5659" s="T533">again</ta>
            <ta e="T535" id="Seg_5660" s="T534">good-ADVZ</ta>
            <ta e="T536" id="Seg_5661" s="T535">wipe-CO-3SG.O</ta>
            <ta e="T537" id="Seg_5662" s="T536">say-3SG.S</ta>
            <ta e="T538" id="Seg_5663" s="T537">eye-PL.[NOM]</ta>
            <ta e="T539" id="Seg_5664" s="T538">stick.to-DUR-OPT.[3SG.S]</ta>
            <ta e="T540" id="Seg_5665" s="T539">eye-PL.[NOM]-3SG</ta>
            <ta e="T541" id="Seg_5666" s="T540">stick-3PL</ta>
            <ta e="T542" id="Seg_5667" s="T541">backward</ta>
            <ta e="T543" id="Seg_5668" s="T542">man-human.being.[NOM]</ta>
            <ta e="T544" id="Seg_5669" s="T543">become.glad-CO-3SG.S</ta>
            <ta e="T545" id="Seg_5670" s="T544">now</ta>
            <ta e="T546" id="Seg_5671" s="T545">eye-PL.[NOM]-1SG</ta>
            <ta e="T547" id="Seg_5672" s="T546">good-ADVZ</ta>
            <ta e="T548" id="Seg_5673" s="T547">see-DRV-CO-3PL</ta>
            <ta e="T549" id="Seg_5674" s="T548">now</ta>
            <ta e="T550" id="Seg_5675" s="T549">I.NOM</ta>
            <ta e="T551" id="Seg_5676" s="T550">eye-COM</ta>
            <ta e="T552" id="Seg_5677" s="T551">become-1SG.S</ta>
            <ta e="T553" id="Seg_5678" s="T552">already</ta>
            <ta e="T554" id="Seg_5679" s="T553">I.NOM</ta>
            <ta e="T555" id="Seg_5680" s="T554">you.SG.ACC</ta>
            <ta e="T556" id="Seg_5681" s="T555">eye-PL-LOC</ta>
            <ta e="T557" id="Seg_5682" s="T556">because.of</ta>
            <ta e="T558" id="Seg_5683" s="T557">%%-FUT-1SG.S-FUT</ta>
            <ta e="T559" id="Seg_5684" s="T558">outward(s)</ta>
            <ta e="T560" id="Seg_5685" s="T559">go.out-3SG.S</ta>
            <ta e="T561" id="Seg_5686" s="T560">and</ta>
            <ta e="T562" id="Seg_5687" s="T561">wife.[NOM]-3SG</ta>
            <ta e="T563" id="Seg_5688" s="T562">swing-HAB-RFL-3SG.S</ta>
            <ta e="T564" id="Seg_5689" s="T563">god.[NOM]</ta>
            <ta e="T565" id="Seg_5690" s="T564">song-ACC</ta>
            <ta e="T566" id="Seg_5691" s="T565">bring-EP-FRQ-EP-3SG.O</ta>
            <ta e="T567" id="Seg_5692" s="T566">husband.[NOM]-3SG</ta>
            <ta e="T568" id="Seg_5693" s="T567">say-3SG.S</ta>
            <ta e="T569" id="Seg_5694" s="T568">wife-OBL.3SG-ALL</ta>
            <ta e="T570" id="Seg_5695" s="T569">and</ta>
            <ta e="T571" id="Seg_5696" s="T570">you.SG.NOM</ta>
            <ta e="T572" id="Seg_5697" s="T571">I.ACC</ta>
            <ta e="T573" id="Seg_5698" s="T572">what-TRL</ta>
            <ta e="T574" id="Seg_5699" s="T573">blind-2SG.S</ta>
            <ta e="T575" id="Seg_5700" s="T574">and</ta>
            <ta e="T576" id="Seg_5701" s="T575">wife.[NOM]-3SG</ta>
            <ta e="T577" id="Seg_5702" s="T576">say-3SG.S</ta>
            <ta e="T578" id="Seg_5703" s="T577">and</ta>
            <ta e="T579" id="Seg_5704" s="T578">you.SG.NOM</ta>
            <ta e="T580" id="Seg_5705" s="T579">what-TRL</ta>
            <ta e="T581" id="Seg_5706" s="T580">so-ADVZ</ta>
            <ta e="T582" id="Seg_5707" s="T581">walk-HAB-PST-2SG.S</ta>
            <ta e="T583" id="Seg_5708" s="T582">go-HAB-PST-2SG.S</ta>
            <ta e="T584" id="Seg_5709" s="T583">wild.animal-CAP-INF</ta>
            <ta e="T585" id="Seg_5710" s="T584">and</ta>
            <ta e="T586" id="Seg_5711" s="T585">oneself.2SG</ta>
            <ta e="T587" id="Seg_5712" s="T586">eye-PL-OBL.2SG-COM</ta>
            <ta e="T588" id="Seg_5713" s="T587">play-PST-2SG.S</ta>
            <ta e="T589" id="Seg_5714" s="T588">and</ta>
            <ta e="T590" id="Seg_5715" s="T589">house-ILL</ta>
            <ta e="T591" id="Seg_5716" s="T590">what-ACC-EMPH</ta>
            <ta e="T592" id="Seg_5717" s="T591">NEG</ta>
            <ta e="T593" id="Seg_5718" s="T592">bring-HAB-PST-2SG.O</ta>
            <ta e="T594" id="Seg_5719" s="T593">look.here</ta>
            <ta e="T595" id="Seg_5720" s="T594">I.NOM</ta>
            <ta e="T596" id="Seg_5721" s="T595">you.SG-ADES-ADJZ</ta>
            <ta e="T597" id="Seg_5722" s="T596">eye-PL-ACC-3SG</ta>
            <ta e="T598" id="Seg_5723" s="T597">that.is.why</ta>
            <ta e="T599" id="Seg_5724" s="T598">and</ta>
            <ta e="T600" id="Seg_5725" s="T599">dig.out-DRV-CO-1SG.O</ta>
            <ta e="T601" id="Seg_5726" s="T600">bring-CO-1SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_5727" s="T1">жить-PST-3DU.S</ta>
            <ta e="T3" id="Seg_5728" s="T2">жить-PST-3DU.S</ta>
            <ta e="T4" id="Seg_5729" s="T3">муж.[NOM]</ta>
            <ta e="T5" id="Seg_5730" s="T4">старуха-DYA-DU.[NOM]</ta>
            <ta e="T6" id="Seg_5731" s="T5">муж.[NOM]-3SG</ta>
            <ta e="T7" id="Seg_5732" s="T6">ходить-HAB-3SG.S</ta>
            <ta e="T8" id="Seg_5733" s="T7">охотиться-CVB</ta>
            <ta e="T9" id="Seg_5734" s="T8">жена.[NOM]-3SG</ta>
            <ta e="T10" id="Seg_5735" s="T9">дом-LOC</ta>
            <ta e="T11" id="Seg_5736" s="T10">быть-CO-3SG.S</ta>
            <ta e="T12" id="Seg_5737" s="T11">муж.[NOM]-3SG</ta>
            <ta e="T13" id="Seg_5738" s="T12">ходить-HAB-3SG.S</ta>
            <ta e="T14" id="Seg_5739" s="T13">ходить-HAB-3SG.S</ta>
            <ta e="T15" id="Seg_5740" s="T14">день-в.течение</ta>
            <ta e="T16" id="Seg_5741" s="T15">ходить-HAB-3SG.S</ta>
            <ta e="T17" id="Seg_5742" s="T16">что-ACC-EMPH</ta>
            <ta e="T18" id="Seg_5743" s="T17">NEG</ta>
            <ta e="T19" id="Seg_5744" s="T18">принести-HAB-3SG.O</ta>
            <ta e="T20" id="Seg_5745" s="T19">все.время</ta>
            <ta e="T21" id="Seg_5746" s="T20">так</ta>
            <ta e="T22" id="Seg_5747" s="T21">ходить-HAB-3SG.S</ta>
            <ta e="T23" id="Seg_5748" s="T22">что-ACC-EMPH</ta>
            <ta e="T24" id="Seg_5749" s="T23">NEG</ta>
            <ta e="T25" id="Seg_5750" s="T24">принести-HAB-3SG.O</ta>
            <ta e="T26" id="Seg_5751" s="T25">один</ta>
            <ta e="T27" id="Seg_5752" s="T26">день.[NOM]</ta>
            <ta e="T28" id="Seg_5753" s="T27">жена.[NOM]-3SG</ta>
            <ta e="T29" id="Seg_5754" s="T28">думать-3SG.S</ta>
            <ta e="T30" id="Seg_5755" s="T29">что-TRL</ta>
            <ta e="T31" id="Seg_5756" s="T30">же</ta>
            <ta e="T32" id="Seg_5757" s="T31">он(а).[NOM]</ta>
            <ta e="T33" id="Seg_5758" s="T32">что-ACC-EMPH</ta>
            <ta e="T34" id="Seg_5759" s="T33">NEG</ta>
            <ta e="T35" id="Seg_5760" s="T34">принести-HAB-3SG.O</ta>
            <ta e="T36" id="Seg_5761" s="T35">я.NOM</ta>
            <ta e="T37" id="Seg_5762" s="T36">он(а)-EP-ACC</ta>
            <ta e="T38" id="Seg_5763" s="T37">проследить-FUT-1SG.O</ta>
            <ta e="T39" id="Seg_5764" s="T38">что-TRL</ta>
            <ta e="T40" id="Seg_5765" s="T39">он(а).[NOM]</ta>
            <ta e="T41" id="Seg_5766" s="T40">опустеть-PTCP.PST-ADVZ</ta>
            <ta e="T42" id="Seg_5767" s="T41">прийти-HAB-3SG.S</ta>
            <ta e="T43" id="Seg_5768" s="T42">ну</ta>
            <ta e="T44" id="Seg_5769" s="T43">и</ta>
            <ta e="T45" id="Seg_5770" s="T44">так-ADVZ</ta>
            <ta e="T46" id="Seg_5771" s="T45">сделать-CO-3SG.O</ta>
            <ta e="T47" id="Seg_5772" s="T46">муж.[NOM]-3SG</ta>
            <ta e="T48" id="Seg_5773" s="T47">отправиться-CVB</ta>
            <ta e="T49" id="Seg_5774" s="T48">начать-DRV-CO-3SG.S</ta>
            <ta e="T50" id="Seg_5775" s="T49">охотиться-CVB</ta>
            <ta e="T51" id="Seg_5776" s="T50">отправиться-CO-3SG.S</ta>
            <ta e="T52" id="Seg_5777" s="T51">жена.[NOM]-3SG</ta>
            <ta e="T53" id="Seg_5778" s="T52">быстро</ta>
            <ta e="T54" id="Seg_5779" s="T53">лыжи-EP-ACC-3SG</ta>
            <ta e="T55" id="Seg_5780" s="T54">надеть-CO-3SG.O</ta>
            <ta e="T56" id="Seg_5781" s="T55">муж-GEN-3SG</ta>
            <ta e="T57" id="Seg_5782" s="T56">за</ta>
            <ta e="T58" id="Seg_5783" s="T57">быстро</ta>
            <ta e="T59" id="Seg_5784" s="T58">отправиться-3SG.S</ta>
            <ta e="T60" id="Seg_5785" s="T59">муж-OBL.3SG-ALL</ta>
            <ta e="T61" id="Seg_5786" s="T60">NEG</ta>
            <ta e="T62" id="Seg_5787" s="T61">виднеться-TR-RFL-3SG.S</ta>
            <ta e="T63" id="Seg_5788" s="T62">поэтому</ta>
            <ta e="T64" id="Seg_5789" s="T63">NEG</ta>
            <ta e="T65" id="Seg_5790" s="T64">виднеться-TR-RFL-3SG.S</ta>
            <ta e="T66" id="Seg_5791" s="T65">он(а)-EP-ACC</ta>
            <ta e="T67" id="Seg_5792" s="T66">муж.[NOM]-3SG</ta>
            <ta e="T68" id="Seg_5793" s="T67">NEG.IMP</ta>
            <ta e="T69" id="Seg_5794" s="T68">увидеть-DRV-CONJ-3SG.O</ta>
            <ta e="T70" id="Seg_5795" s="T69">муж.[NOM]-3SG</ta>
            <ta e="T71" id="Seg_5796" s="T70">достичь-3SG.S</ta>
            <ta e="T72" id="Seg_5797" s="T71">Чая-GEN</ta>
            <ta e="T73" id="Seg_5798" s="T72">крутой.берег-GEN-верхняя.часть-ILL</ta>
            <ta e="T74" id="Seg_5799" s="T73">этот</ta>
            <ta e="T75" id="Seg_5800" s="T74">сторона-ADJZ</ta>
            <ta e="T76" id="Seg_5801" s="T75">крутой.берег-GEN</ta>
            <ta e="T77" id="Seg_5802" s="T76">верхняя.часть-LOC</ta>
            <ta e="T78" id="Seg_5803" s="T77">свой.3SG</ta>
            <ta e="T79" id="Seg_5804" s="T78">глаз-ACC-3SG</ta>
            <ta e="T80" id="Seg_5805" s="T79">выкопать-DRV-HAB-3SG.O</ta>
            <ta e="T81" id="Seg_5806" s="T80">глаз-PL-ACC-3SG</ta>
            <ta e="T82" id="Seg_5807" s="T81">повесить-CVB</ta>
            <ta e="T83" id="Seg_5808" s="T82">оставить-HAB-3SG.O</ta>
            <ta e="T84" id="Seg_5809" s="T83">дерево-GEN</ta>
            <ta e="T85" id="Seg_5810" s="T84">сук-PL-LOC</ta>
            <ta e="T86" id="Seg_5811" s="T85">тот</ta>
            <ta e="T87" id="Seg_5812" s="T86">сторона-ADJZ</ta>
            <ta e="T88" id="Seg_5813" s="T87">крутой.берег-ILL</ta>
            <ta e="T89" id="Seg_5814" s="T88">покатиться-MOM-HAB-3SG.S</ta>
            <ta e="T90" id="Seg_5815" s="T89">сам.3SG</ta>
            <ta e="T91" id="Seg_5816" s="T90">сказать-HAB-3SG.S</ta>
            <ta e="T92" id="Seg_5817" s="T91">глаз.[NOM]</ta>
            <ta e="T93" id="Seg_5818" s="T92">приклеиться-DUR-OPT.[3SG.S]</ta>
            <ta e="T94" id="Seg_5819" s="T93">глаз-PL.[NOM]</ta>
            <ta e="T95" id="Seg_5820" s="T94">он(а)-ALL</ta>
            <ta e="T96" id="Seg_5821" s="T95">прийти-HAB-CO-3PL</ta>
            <ta e="T97" id="Seg_5822" s="T96">прилипнуть-HAB-CO-3PL</ta>
            <ta e="T98" id="Seg_5823" s="T97">опять</ta>
            <ta e="T99" id="Seg_5824" s="T98">туда-ADJZ</ta>
            <ta e="T100" id="Seg_5825" s="T99">крутой.берег-GEN</ta>
            <ta e="T101" id="Seg_5826" s="T100">верхняя.часть-EP-LOC</ta>
            <ta e="T102" id="Seg_5827" s="T101">глаз-GEN-3SG</ta>
            <ta e="T103" id="Seg_5828" s="T102">выкопать-HAB-CVB</ta>
            <ta e="T104" id="Seg_5829" s="T103">оставить-HAB-3SG.O</ta>
            <ta e="T105" id="Seg_5830" s="T104">дерево-GEN</ta>
            <ta e="T106" id="Seg_5831" s="T105">сук-PL-ILL</ta>
            <ta e="T107" id="Seg_5832" s="T106">повесить-HAB-3SG.O</ta>
            <ta e="T108" id="Seg_5833" s="T107">сказать-HAB-3SG.S</ta>
            <ta e="T109" id="Seg_5834" s="T108">глаз-PL.[NOM]</ta>
            <ta e="T110" id="Seg_5835" s="T109">приклеиться-DUR-OPT.[3SG.S]</ta>
            <ta e="T111" id="Seg_5836" s="T110">глаз-PL.[NOM]</ta>
            <ta e="T112" id="Seg_5837" s="T111">он(а)-ALL</ta>
            <ta e="T113" id="Seg_5838" s="T112">прийти-CVB</ta>
            <ta e="T114" id="Seg_5839" s="T113">приклеиться-HAB-DRV-HAB-CO-3PL</ta>
            <ta e="T116" id="Seg_5840" s="T114">%so</ta>
            <ta e="T117" id="Seg_5841" s="T116">целый</ta>
            <ta e="T118" id="Seg_5842" s="T117">день-в.течение</ta>
            <ta e="T119" id="Seg_5843" s="T118">%%-3SG.S</ta>
            <ta e="T120" id="Seg_5844" s="T119">жена.[NOM]-3SG</ta>
            <ta e="T121" id="Seg_5845" s="T120">посмотреть-DUR-PST.[3SG.S]</ta>
            <ta e="T122" id="Seg_5846" s="T121">посмотреть-DUR-PST.[3SG.S]</ta>
            <ta e="T123" id="Seg_5847" s="T122">%украсть-DIM-3SG.S</ta>
            <ta e="T124" id="Seg_5848" s="T123">муж.[NOM]-3SG</ta>
            <ta e="T125" id="Seg_5849" s="T124">глаз-ACC-3SG</ta>
            <ta e="T126" id="Seg_5850" s="T125">выкопать-INFER-3SG.O</ta>
            <ta e="T127" id="Seg_5851" s="T126">этот</ta>
            <ta e="T128" id="Seg_5852" s="T127">сторона-ADJZ</ta>
            <ta e="T129" id="Seg_5853" s="T128">крутой.берег-GEN</ta>
            <ta e="T130" id="Seg_5854" s="T129">крутой.берег-GEN-верхняя.часть-ILL</ta>
            <ta e="T131" id="Seg_5855" s="T130">оставить-3SG.O</ta>
            <ta e="T132" id="Seg_5856" s="T131">а</ta>
            <ta e="T133" id="Seg_5857" s="T132">жена.[NOM]-3SG</ta>
            <ta e="T134" id="Seg_5858" s="T133">быстро</ta>
            <ta e="T135" id="Seg_5859" s="T134">он(а)-EP-GEN</ta>
            <ta e="T136" id="Seg_5860" s="T135">глаз-PL-ACC-3SG</ta>
            <ta e="T137" id="Seg_5861" s="T136">взять-EP-3SG.O</ta>
            <ta e="T139" id="Seg_5862" s="T138">сам.3SG</ta>
            <ta e="T140" id="Seg_5863" s="T139">думать-3SG.O</ta>
            <ta e="T141" id="Seg_5864" s="T140">а</ta>
            <ta e="T142" id="Seg_5865" s="T141">ты.NOM</ta>
            <ta e="T143" id="Seg_5866" s="T142">поэтому</ta>
            <ta e="T144" id="Seg_5867" s="T143">дом-ILL</ta>
            <ta e="T145" id="Seg_5868" s="T144">что-ACC-EMPH</ta>
            <ta e="T146" id="Seg_5869" s="T145">NEG</ta>
            <ta e="T147" id="Seg_5870" s="T146">принести-HAB-CO-2SG.O</ta>
            <ta e="T148" id="Seg_5871" s="T147">я.ALL</ta>
            <ta e="T149" id="Seg_5872" s="T148">так-ADVZ</ta>
            <ta e="T150" id="Seg_5873" s="T149">жена.[NOM]-3SG</ta>
            <ta e="T151" id="Seg_5874" s="T150">отправиться-CO-3SG.S</ta>
            <ta e="T152" id="Seg_5875" s="T151">отправиться-CO-3SG.S</ta>
            <ta e="T153" id="Seg_5876" s="T152">и</ta>
            <ta e="T154" id="Seg_5877" s="T153">медленно</ta>
            <ta e="T155" id="Seg_5878" s="T154">ходить-3SG.S</ta>
            <ta e="T156" id="Seg_5879" s="T155">муж-OBL.3SG-ADES</ta>
            <ta e="T157" id="Seg_5880" s="T156">глаз.[NOM]-3SG</ta>
            <ta e="T158" id="Seg_5881" s="T157">теперь</ta>
            <ta e="T159" id="Seg_5882" s="T158">NEG.EX-CO-3SG.S</ta>
            <ta e="T160" id="Seg_5883" s="T159">оставить-INF</ta>
            <ta e="T161" id="Seg_5884" s="T160">он(а)-EP-ACC</ta>
            <ta e="T162" id="Seg_5885" s="T161">он(а).[NOM]</ta>
            <ta e="T163" id="Seg_5886" s="T162">потеряться-HAB-FUT-3SG.S</ta>
            <ta e="T164" id="Seg_5887" s="T163">поэтому</ta>
            <ta e="T165" id="Seg_5888" s="T164">и</ta>
            <ta e="T166" id="Seg_5889" s="T165">медленно</ta>
            <ta e="T167" id="Seg_5890" s="T166">ходить-3SG.S</ta>
            <ta e="T168" id="Seg_5891" s="T167">жена.[NOM]-3SG</ta>
            <ta e="T169" id="Seg_5892" s="T168">сколько</ta>
            <ta e="T170" id="Seg_5893" s="T169">ходить-3SG.S</ta>
            <ta e="T171" id="Seg_5894" s="T170">опять</ta>
            <ta e="T172" id="Seg_5895" s="T171">назад</ta>
            <ta e="T173" id="Seg_5896" s="T172">посмотреть-DRV-HAB-3SG.S</ta>
            <ta e="T174" id="Seg_5897" s="T173">муж.[NOM]-3SG</ta>
            <ta e="T175" id="Seg_5898" s="T174">ходить-3SG.S</ta>
            <ta e="T176" id="Seg_5899" s="T175">али</ta>
            <ta e="T177" id="Seg_5900" s="T176">NEG</ta>
            <ta e="T178" id="Seg_5901" s="T177">жена-GEN-3SG</ta>
            <ta e="T179" id="Seg_5902" s="T178">отправиться-CVB2</ta>
            <ta e="T180" id="Seg_5903" s="T179">муж.[NOM]-3SG</ta>
            <ta e="T181" id="Seg_5904" s="T180">столько</ta>
            <ta e="T182" id="Seg_5905" s="T181">закричать-CO-3SG.S</ta>
            <ta e="T183" id="Seg_5906" s="T182">глаз-PL.[NOM]</ta>
            <ta e="T184" id="Seg_5907" s="T183">приклеиться-DUR-OPT.[3SG.S]</ta>
            <ta e="T185" id="Seg_5908" s="T184">глаз-PL.[NOM]</ta>
            <ta e="T186" id="Seg_5909" s="T185">NEG.EX-HAB-CO-3PL</ta>
            <ta e="T187" id="Seg_5910" s="T186">NEG</ta>
            <ta e="T188" id="Seg_5911" s="T187">прийти-HAB-CO-3PL</ta>
            <ta e="T189" id="Seg_5912" s="T188">он(а)-ALL</ta>
            <ta e="T190" id="Seg_5913" s="T189">и</ta>
            <ta e="T191" id="Seg_5914" s="T190">NEG</ta>
            <ta e="T192" id="Seg_5915" s="T191">прилипнуть-HAB-CO-3PL</ta>
            <ta e="T193" id="Seg_5916" s="T192">сколько</ta>
            <ta e="T194" id="Seg_5917" s="T193">глаз-PL-ACC-3SG</ta>
            <ta e="T195" id="Seg_5918" s="T194">позвать-PST-3SG.O</ta>
            <ta e="T196" id="Seg_5919" s="T195">позвать-PST-3SG.O</ta>
            <ta e="T197" id="Seg_5920" s="T196">глаз-PL.[NOM]</ta>
            <ta e="T198" id="Seg_5921" s="T197">как</ta>
            <ta e="T199" id="Seg_5922" s="T198">NEG.EX-HAB-CO-3PL</ta>
            <ta e="T200" id="Seg_5923" s="T199">так</ta>
            <ta e="T201" id="Seg_5924" s="T200">и</ta>
            <ta e="T202" id="Seg_5925" s="T201">NEG.EX-HAB-CO-3PL</ta>
            <ta e="T203" id="Seg_5926" s="T202">теперь</ta>
            <ta e="T204" id="Seg_5927" s="T203">глаз-CAR-ADVZ</ta>
            <ta e="T205" id="Seg_5928" s="T204">муж.[NOM]-3SG</ta>
            <ta e="T206" id="Seg_5929" s="T205">ну</ta>
            <ta e="T207" id="Seg_5930" s="T206">отправиться-3SG.S</ta>
            <ta e="T208" id="Seg_5931" s="T207">глаз-CAR-ADVZ</ta>
            <ta e="T209" id="Seg_5932" s="T208">жена.[NOM]-3SG</ta>
            <ta e="T210" id="Seg_5933" s="T209">ждать-TR-3SG.S</ta>
            <ta e="T211" id="Seg_5934" s="T210">муж-OBL.3SG-ALL</ta>
            <ta e="T212" id="Seg_5935" s="T211">NEG</ta>
            <ta e="T213" id="Seg_5936" s="T212">виднеться-TR-RFL-3SG.S</ta>
            <ta e="T214" id="Seg_5937" s="T213">муж.[NOM]-3SG</ta>
            <ta e="T215" id="Seg_5938" s="T214">глаз-CAR-ADVZ</ta>
            <ta e="T216" id="Seg_5939" s="T215">потеряться-HAB-CVB</ta>
            <ta e="T217" id="Seg_5940" s="T216">умереть-FUT-3SG.S</ta>
            <ta e="T218" id="Seg_5941" s="T217">дом-EP-GEN</ta>
            <ta e="T219" id="Seg_5942" s="T218">бок-ILL</ta>
            <ta e="T220" id="Seg_5943" s="T219">подойти-CVB</ta>
            <ta e="T221" id="Seg_5944" s="T220">начать-DRV-CO-3DU.S</ta>
            <ta e="T222" id="Seg_5945" s="T221">жена.[NOM]-3SG</ta>
            <ta e="T223" id="Seg_5946" s="T222">торопиться-3SG.S</ta>
            <ta e="T224" id="Seg_5947" s="T223">быстро</ta>
            <ta e="T225" id="Seg_5948" s="T224">достичь-DRV-3SG.S</ta>
            <ta e="T226" id="Seg_5949" s="T225">лыжи-PL-ACC-3SG</ta>
            <ta e="T0" id="Seg_5950" s="T226">так</ta>
            <ta e="T227" id="Seg_5951" s="T0">вниз</ta>
            <ta e="T228" id="Seg_5952" s="T227">загнать-CO-3SG.O</ta>
            <ta e="T229" id="Seg_5953" s="T228">дом-ILL</ta>
            <ta e="T230" id="Seg_5954" s="T229">зайти-CO-3SG.S</ta>
            <ta e="T231" id="Seg_5955" s="T230">раздеться-CO-3SG.S</ta>
            <ta e="T232" id="Seg_5956" s="T231">муж-GEN-3SG</ta>
            <ta e="T233" id="Seg_5957" s="T232">глаз-PL-ACC</ta>
            <ta e="T234" id="Seg_5958" s="T233">посудина-ILL</ta>
            <ta e="T235" id="Seg_5959" s="T234">положить-CO-3SG.O</ta>
            <ta e="T236" id="Seg_5960" s="T235">печь-GEN</ta>
            <ta e="T237" id="Seg_5961" s="T236">пространство.за-ILL</ta>
            <ta e="T238" id="Seg_5962" s="T237">спрятать-CO-3SG.O</ta>
            <ta e="T239" id="Seg_5963" s="T238">муж.[NOM]-3SG</ta>
            <ta e="T240" id="Seg_5964" s="T239">медленно</ta>
            <ta e="T241" id="Seg_5965" s="T240">прийти-CO-3SG.S</ta>
            <ta e="T242" id="Seg_5966" s="T241">дом-ILL</ta>
            <ta e="T243" id="Seg_5967" s="T242">зайти-CO-3SG.S</ta>
            <ta e="T244" id="Seg_5968" s="T243">жена.[NOM]-3SG</ta>
            <ta e="T245" id="Seg_5969" s="T244">сказать-3SG.S</ta>
            <ta e="T246" id="Seg_5970" s="T245">а</ta>
            <ta e="T247" id="Seg_5971" s="T246">ты.NOM</ta>
            <ta e="T248" id="Seg_5972" s="T247">что-TRL</ta>
            <ta e="T249" id="Seg_5973" s="T248">этот-день.[NOM]</ta>
            <ta e="T250" id="Seg_5974" s="T249">долго-ADVZ</ta>
            <ta e="T251" id="Seg_5975" s="T250">быть-CO-2SG.S</ta>
            <ta e="T252" id="Seg_5976" s="T251">муж.[NOM]-3SG</ta>
            <ta e="T253" id="Seg_5977" s="T252">сказать-3SG.S</ta>
            <ta e="T254" id="Seg_5978" s="T253">я.NOM</ta>
            <ta e="T255" id="Seg_5979" s="T254">этот-день.[NOM]</ta>
            <ta e="T256" id="Seg_5980" s="T255">глаз-EP-1SG-EP-ACC</ta>
            <ta e="T257" id="Seg_5981" s="T256">%%-1SG.S</ta>
            <ta e="T258" id="Seg_5982" s="T257">поэтому</ta>
            <ta e="T259" id="Seg_5983" s="T258">долго-ADVZ</ta>
            <ta e="T260" id="Seg_5984" s="T259">быть-CO-1SG.S</ta>
            <ta e="T261" id="Seg_5985" s="T260">жена.[NOM]-3SG</ta>
            <ta e="T262" id="Seg_5986" s="T261">думать-3SG.S</ta>
            <ta e="T263" id="Seg_5987" s="T262">а</ta>
            <ta e="T264" id="Seg_5988" s="T263">ты.NOM</ta>
            <ta e="T265" id="Seg_5989" s="T264">%%-2SG.S</ta>
            <ta e="T266" id="Seg_5990" s="T265">я.NOM</ta>
            <ta e="T267" id="Seg_5991" s="T266">теперь</ta>
            <ta e="T268" id="Seg_5992" s="T267">ты.ACC</ta>
            <ta e="T269" id="Seg_5993" s="T268">знать-CO-1SG.S</ta>
            <ta e="T270" id="Seg_5994" s="T269">как</ta>
            <ta e="T271" id="Seg_5995" s="T270">ты.NOM</ta>
            <ta e="T272" id="Seg_5996" s="T271">охотиться-CO-2SG.S</ta>
            <ta e="T273" id="Seg_5997" s="T272">целый</ta>
            <ta e="T274" id="Seg_5998" s="T273">солнце-в.течение</ta>
            <ta e="T275" id="Seg_5999" s="T274">глаз-PL-OBL.2SG-COM</ta>
            <ta e="T276" id="Seg_6000" s="T275">%играть-2SG.S</ta>
            <ta e="T277" id="Seg_6001" s="T276">другой</ta>
            <ta e="T278" id="Seg_6002" s="T277">крутой.берег-GEN</ta>
            <ta e="T279" id="Seg_6003" s="T278">верхняя.часть-ILL</ta>
            <ta e="T280" id="Seg_6004" s="T279">повесить-HAB-CO-2SG.O</ta>
            <ta e="T281" id="Seg_6005" s="T280">а</ta>
            <ta e="T282" id="Seg_6006" s="T281">этот</ta>
            <ta e="T283" id="Seg_6007" s="T282">сторона-ADJZ</ta>
            <ta e="T284" id="Seg_6008" s="T283">река-ADV.LOC</ta>
            <ta e="T285" id="Seg_6009" s="T284">верхняя.часть-ILL</ta>
            <ta e="T286" id="Seg_6010" s="T285">покатиться-MOM-HAB-CO-2SG.S</ta>
            <ta e="T287" id="Seg_6011" s="T286">глаз-PL.[NOM]-2SG</ta>
            <ta e="T288" id="Seg_6012" s="T287">ты.ALL</ta>
            <ta e="T289" id="Seg_6013" s="T288">прийти-CVB</ta>
            <ta e="T290" id="Seg_6014" s="T289">прилипнуть-HAB-CO-3PL</ta>
            <ta e="T291" id="Seg_6015" s="T290">так-ADVZ</ta>
            <ta e="T292" id="Seg_6016" s="T291">ты.NOM</ta>
            <ta e="T293" id="Seg_6017" s="T292">и</ta>
            <ta e="T294" id="Seg_6018" s="T293">опустеть-PTCP.PST-ADVZ</ta>
            <ta e="T295" id="Seg_6019" s="T294">прийти-HAB-PST-2SG.S</ta>
            <ta e="T296" id="Seg_6020" s="T295">ну</ta>
            <ta e="T297" id="Seg_6021" s="T296">и</ta>
            <ta e="T298" id="Seg_6022" s="T297">жить-CVB</ta>
            <ta e="T299" id="Seg_6023" s="T298">начать-DRV-CO-3DU.S</ta>
            <ta e="T300" id="Seg_6024" s="T299">сколько</ta>
            <ta e="T301" id="Seg_6025" s="T300">то</ta>
            <ta e="T302" id="Seg_6026" s="T301">солнце.[NOM]-EP-3SG</ta>
            <ta e="T303" id="Seg_6027" s="T302">пройти-3SG.S</ta>
            <ta e="T304" id="Seg_6028" s="T303">жена.[NOM]-3SG</ta>
            <ta e="T305" id="Seg_6029" s="T304">посмотреть-DRV-3SG.S</ta>
            <ta e="T306" id="Seg_6030" s="T305">окно-PROL</ta>
            <ta e="T307" id="Seg_6031" s="T306">посмотреть-DRV-3SG.S</ta>
            <ta e="T308" id="Seg_6032" s="T307">стоять-CO-3SG.S</ta>
            <ta e="T309" id="Seg_6033" s="T308">лось.[NOM]</ta>
            <ta e="T310" id="Seg_6034" s="T309">жена.[NOM]-3SG</ta>
            <ta e="T311" id="Seg_6035" s="T310">муж-OBL.3SG-ALL</ta>
            <ta e="T312" id="Seg_6036" s="T311">сказать-3SG.S</ta>
            <ta e="T313" id="Seg_6037" s="T312">лось.[NOM]</ta>
            <ta e="T314" id="Seg_6038" s="T313">стоять-HAB-INFER.[3SG.S]</ta>
            <ta e="T315" id="Seg_6039" s="T314">ты.NOM</ta>
            <ta e="T316" id="Seg_6040" s="T315">встать-OPT-2SG.S</ta>
            <ta e="T317" id="Seg_6041" s="T316">как-INDEF2</ta>
            <ta e="T318" id="Seg_6042" s="T317">застрелить-OPT-2SG.O</ta>
            <ta e="T319" id="Seg_6043" s="T318">лось-EP-ACC</ta>
            <ta e="T320" id="Seg_6044" s="T319">а</ta>
            <ta e="T321" id="Seg_6045" s="T320">муж.[NOM]-3SG</ta>
            <ta e="T322" id="Seg_6046" s="T321">сказать-3SG.S</ta>
            <ta e="T323" id="Seg_6047" s="T322">я.NOM</ta>
            <ta e="T324" id="Seg_6048" s="T323">как</ta>
            <ta e="T325" id="Seg_6049" s="T324">застрелить-FUT-1SG.O</ta>
            <ta e="T326" id="Seg_6050" s="T325">глаз.[NOM]-EP-1SG</ta>
            <ta e="T327" id="Seg_6051" s="T326">NEG</ta>
            <ta e="T328" id="Seg_6052" s="T327">видеть-3SG.S</ta>
            <ta e="T329" id="Seg_6053" s="T328">жена.[NOM]-3SG</ta>
            <ta e="T330" id="Seg_6054" s="T329">сказать-3SG.S</ta>
            <ta e="T331" id="Seg_6055" s="T330">прийти-IMP.2SG.S</ta>
            <ta e="T332" id="Seg_6056" s="T331">я.ALL</ta>
            <ta e="T333" id="Seg_6057" s="T332">я.NOM</ta>
            <ta e="T334" id="Seg_6058" s="T333">ружье-ACC</ta>
            <ta e="T335" id="Seg_6059" s="T334">ты.ALL</ta>
            <ta e="T336" id="Seg_6060" s="T335">взять-DRV-TR-FUT-1SG.O-FUT</ta>
            <ta e="T337" id="Seg_6061" s="T336">и</ta>
            <ta e="T338" id="Seg_6062" s="T337">муж.[NOM]-3SG</ta>
            <ta e="T339" id="Seg_6063" s="T338">прийти-EP-3SG.S</ta>
            <ta e="T340" id="Seg_6064" s="T339">старуха-OBL.3SG-ALL</ta>
            <ta e="T341" id="Seg_6065" s="T340">жена.[NOM]-3SG</ta>
            <ta e="T342" id="Seg_6066" s="T341">муж-OBL.3SG-ALL</ta>
            <ta e="T343" id="Seg_6067" s="T342">взять-DRV-TR-3SG.O</ta>
            <ta e="T344" id="Seg_6068" s="T343">рука-ILL.3SG</ta>
            <ta e="T345" id="Seg_6069" s="T344">еда-VBLZ-PTCP.PST</ta>
            <ta e="T346" id="Seg_6070" s="T345">ружье-ACC</ta>
            <ta e="T347" id="Seg_6071" s="T346">сам.3SG</ta>
            <ta e="T348" id="Seg_6072" s="T347">он(а)-ALL</ta>
            <ta e="T349" id="Seg_6073" s="T348">показать-EP-3SG.O</ta>
            <ta e="T350" id="Seg_6074" s="T349">как</ta>
            <ta e="T351" id="Seg_6075" s="T350">застрелить-INF</ta>
            <ta e="T352" id="Seg_6076" s="T351">лось-EP-ACC</ta>
            <ta e="T353" id="Seg_6077" s="T352">муж.[NOM]-3SG</ta>
            <ta e="T354" id="Seg_6078" s="T353">выстрелить-3SG.O</ta>
            <ta e="T355" id="Seg_6079" s="T354">убить-CO-3SG.O</ta>
            <ta e="T356" id="Seg_6080" s="T355">лось-EP-ACC</ta>
            <ta e="T357" id="Seg_6081" s="T356">муж-GEN-3SG</ta>
            <ta e="T358" id="Seg_6082" s="T357">глаз.[NOM]</ta>
            <ta e="T359" id="Seg_6083" s="T358">NEG</ta>
            <ta e="T360" id="Seg_6084" s="T359">видеть-3SG.S</ta>
            <ta e="T361" id="Seg_6085" s="T360">лось-EP-ACC</ta>
            <ta e="T362" id="Seg_6086" s="T361">он(а)-ALL</ta>
            <ta e="T363" id="Seg_6087" s="T362">NEG</ta>
            <ta e="T364" id="Seg_6088" s="T363">ободрать-INF</ta>
            <ta e="T365" id="Seg_6089" s="T364">жена.[NOM]-3SG</ta>
            <ta e="T366" id="Seg_6090" s="T365">сам.3SG</ta>
            <ta e="T367" id="Seg_6091" s="T366">ободрать-CVB</ta>
            <ta e="T368" id="Seg_6092" s="T367">начать-DRV-EP-3SG.O</ta>
            <ta e="T369" id="Seg_6093" s="T368">лось-EP-ACC</ta>
            <ta e="T370" id="Seg_6094" s="T369">ну</ta>
            <ta e="T371" id="Seg_6095" s="T370">и</ta>
            <ta e="T372" id="Seg_6096" s="T371">ободрать-EP-3SG.O</ta>
            <ta e="T373" id="Seg_6097" s="T372">лось-EP-ACC</ta>
            <ta e="T374" id="Seg_6098" s="T373">мясо-PL-ACC</ta>
            <ta e="T375" id="Seg_6099" s="T374">почистить-CO-3SG.O</ta>
            <ta e="T376" id="Seg_6100" s="T375">повесить-EP-3SG.O</ta>
            <ta e="T377" id="Seg_6101" s="T376">один</ta>
            <ta e="T378" id="Seg_6102" s="T377">день.[NOM]</ta>
            <ta e="T379" id="Seg_6103" s="T378">мясо-PL-ACC</ta>
            <ta e="T380" id="Seg_6104" s="T379">много-ADVZ</ta>
            <ta e="T381" id="Seg_6105" s="T380">сварить-CO-3SG.O</ta>
            <ta e="T382" id="Seg_6106" s="T381">муж-OBL.3SG-ALL</ta>
            <ta e="T383" id="Seg_6107" s="T382">сказать-3SG.S</ta>
            <ta e="T384" id="Seg_6108" s="T383">этот-день.[NOM]</ta>
            <ta e="T385" id="Seg_6109" s="T384">человек-PL.[NOM]</ta>
            <ta e="T386" id="Seg_6110" s="T385">сказать-3PL</ta>
            <ta e="T387" id="Seg_6111" s="T386">этот-день-ADJZ</ta>
            <ta e="T388" id="Seg_6112" s="T387">день.[NOM]</ta>
            <ta e="T389" id="Seg_6113" s="T388">большой</ta>
            <ta e="T390" id="Seg_6114" s="T389">бог-ADJZ-день.[NOM]</ta>
            <ta e="T391" id="Seg_6115" s="T390">муж.[NOM]-3SG</ta>
            <ta e="T392" id="Seg_6116" s="T391">сказать-3SG.S</ta>
            <ta e="T393" id="Seg_6117" s="T392">бог-ADJZ-день-TR-IMP.2SG.S</ta>
            <ta e="T394" id="Seg_6118" s="T393">ну</ta>
            <ta e="T395" id="Seg_6119" s="T394">и</ta>
            <ta e="T396" id="Seg_6120" s="T395">бог-ADJZ-день-VBLZ-CVB</ta>
            <ta e="T397" id="Seg_6121" s="T396">начать-DRV-CO-3DU.S</ta>
            <ta e="T398" id="Seg_6122" s="T397">жена.[NOM]-3SG</ta>
            <ta e="T399" id="Seg_6123" s="T398">наружу-ADV.LOC</ta>
            <ta e="T400" id="Seg_6124" s="T399">муж.[NOM]-3SG</ta>
            <ta e="T401" id="Seg_6125" s="T400">дом-LOC</ta>
            <ta e="T402" id="Seg_6126" s="T401">быть-CO-3SG.S</ta>
            <ta e="T403" id="Seg_6127" s="T402">жена.[NOM]-3SG</ta>
            <ta e="T404" id="Seg_6128" s="T403">наружу-ADV.LOC</ta>
            <ta e="T405" id="Seg_6129" s="T404">повесить-EP-3SG.O</ta>
            <ta e="T406" id="Seg_6130" s="T405">качать-INSTRN-EP-ACC</ta>
            <ta e="T407" id="Seg_6131" s="T406">качать-HAB-RFL-CVB</ta>
            <ta e="T408" id="Seg_6132" s="T407">начать-DRV-CO-3SG.S</ta>
            <ta e="T409" id="Seg_6133" s="T408">муж.[NOM]-3SG</ta>
            <ta e="T410" id="Seg_6134" s="T409">дом-LOC</ta>
            <ta e="T411" id="Seg_6135" s="T410">чум-CAP-HAB-3SG.S</ta>
            <ta e="T412" id="Seg_6136" s="T411">а</ta>
            <ta e="T413" id="Seg_6137" s="T412">жена.[NOM]</ta>
            <ta e="T414" id="Seg_6138" s="T413">наружу-ADV.LOC</ta>
            <ta e="T415" id="Seg_6139" s="T414">качать-RFL-3SG.S</ta>
            <ta e="T416" id="Seg_6140" s="T415">и</ta>
            <ta e="T417" id="Seg_6141" s="T416">качать-RFL-3SG.S</ta>
            <ta e="T418" id="Seg_6142" s="T417">а</ta>
            <ta e="T419" id="Seg_6143" s="T418">сам.3SG</ta>
            <ta e="T420" id="Seg_6144" s="T419">высохнуть-PTCP.PST</ta>
            <ta e="T421" id="Seg_6145" s="T420">лось-EP-GEN</ta>
            <ta e="T422" id="Seg_6146" s="T421">мясо.[NOM]</ta>
            <ta e="T423" id="Seg_6147" s="T422">съесть-FRQ-CO-3SG.S</ta>
            <ta e="T424" id="Seg_6148" s="T423">сам.3SG</ta>
            <ta e="T425" id="Seg_6149" s="T424">небо-GEN</ta>
            <ta e="T426" id="Seg_6150" s="T425">песня-GEN</ta>
            <ta e="T427" id="Seg_6151" s="T426">сказать-CVB</ta>
            <ta e="T428" id="Seg_6152" s="T427">принести-EP-FRQ-EP-3SG.O</ta>
            <ta e="T429" id="Seg_6153" s="T428">весна-ADJZ</ta>
            <ta e="T430" id="Seg_6154" s="T429">солнце.[NOM]-EP-3SG</ta>
            <ta e="T431" id="Seg_6155" s="T430">длинный-VBLZ-HAB.[3SG.S]</ta>
            <ta e="T432" id="Seg_6156" s="T431">муж.[NOM]-3SG</ta>
            <ta e="T433" id="Seg_6157" s="T432">дом-LOC</ta>
            <ta e="T434" id="Seg_6158" s="T433">сидеть-PST.[3SG.S]</ta>
            <ta e="T435" id="Seg_6159" s="T434">сидеть-PST.[3SG.S]</ta>
            <ta e="T436" id="Seg_6160" s="T435">%устать-3SG.S</ta>
            <ta e="T437" id="Seg_6161" s="T436">и</ta>
            <ta e="T438" id="Seg_6162" s="T437">думать-3SG.S</ta>
            <ta e="T439" id="Seg_6163" s="T438">я.NOM</ta>
            <ta e="T440" id="Seg_6164" s="T439">глаз-PL-1SG-EP-ACC</ta>
            <ta e="T441" id="Seg_6165" s="T440">позвать-OPT-1SG.O</ta>
            <ta e="T442" id="Seg_6166" s="T441">глаз-PL.[NOM]</ta>
            <ta e="T443" id="Seg_6167" s="T442">может.быть</ta>
            <ta e="T444" id="Seg_6168" s="T443">я.ALL</ta>
            <ta e="T445" id="Seg_6169" s="T444">прийти-OPT-3DU.S</ta>
            <ta e="T446" id="Seg_6170" s="T445">прийти-OPT-3PL</ta>
            <ta e="T447" id="Seg_6171" s="T446">ну</ta>
            <ta e="T448" id="Seg_6172" s="T447">так-ADVZ</ta>
            <ta e="T449" id="Seg_6173" s="T448">и</ta>
            <ta e="T450" id="Seg_6174" s="T449">сделать-CO-3SG.O</ta>
            <ta e="T451" id="Seg_6175" s="T450">глаз-PL-ACC-3SG</ta>
            <ta e="T452" id="Seg_6176" s="T451">позвать-CVB</ta>
            <ta e="T453" id="Seg_6177" s="T452">начать-DRV-CO-3SG.O</ta>
            <ta e="T454" id="Seg_6178" s="T453">что-INDEF.[NOM]</ta>
            <ta e="T455" id="Seg_6179" s="T454">печь-GEN</ta>
            <ta e="T456" id="Seg_6180" s="T455">пространство.за-ADV.LOC</ta>
            <ta e="T457" id="Seg_6181" s="T456">прогреметь-3SG.S</ta>
            <ta e="T458" id="Seg_6182" s="T457">он(а).[NOM]</ta>
            <ta e="T459" id="Seg_6183" s="T458">хороший-ADVZ</ta>
            <ta e="T460" id="Seg_6184" s="T459">cлушать-PFV-3SG.S</ta>
            <ta e="T461" id="Seg_6185" s="T460">сам.3SG</ta>
            <ta e="T462" id="Seg_6186" s="T461">думать-3SG.S</ta>
            <ta e="T463" id="Seg_6187" s="T462">что.[NOM]</ta>
            <ta e="T464" id="Seg_6188" s="T463">туда-ADV.LOC</ta>
            <ta e="T465" id="Seg_6189" s="T464">прогреметь-INFER.[3SG.S]</ta>
            <ta e="T466" id="Seg_6190" s="T465">он(а).[NOM]</ta>
            <ta e="T467" id="Seg_6191" s="T466">опять</ta>
            <ta e="T468" id="Seg_6192" s="T467">два-ORD</ta>
            <ta e="T469" id="Seg_6193" s="T468">сказать-3SG.S</ta>
            <ta e="T470" id="Seg_6194" s="T469">глаз-PL.[NOM]</ta>
            <ta e="T471" id="Seg_6195" s="T470">приклеиться-DUR-OPT.[3SG.S]</ta>
            <ta e="T472" id="Seg_6196" s="T471">а</ta>
            <ta e="T473" id="Seg_6197" s="T472">печь-GEN</ta>
            <ta e="T474" id="Seg_6198" s="T473">пространство.за-ADV.LOC</ta>
            <ta e="T475" id="Seg_6199" s="T474">большой-ADVZ</ta>
            <ta e="T476" id="Seg_6200" s="T475">прогреметь-CO-3SG.S</ta>
            <ta e="T477" id="Seg_6201" s="T476">что-INDEF.[NOM]</ta>
            <ta e="T478" id="Seg_6202" s="T477">муж.[NOM]-3SG</ta>
            <ta e="T479" id="Seg_6203" s="T478">туда-ADV.LOC</ta>
            <ta e="T480" id="Seg_6204" s="T479">посох-EP-COM</ta>
            <ta e="T481" id="Seg_6205" s="T480">медленно</ta>
            <ta e="T482" id="Seg_6206" s="T481">отправиться-CO-3SG.S</ta>
            <ta e="T483" id="Seg_6207" s="T482">печь-GEN</ta>
            <ta e="T484" id="Seg_6208" s="T483">пространство.за-ADV.LOC</ta>
            <ta e="T485" id="Seg_6209" s="T484">ощупать-EP-DUR-3SG.S</ta>
            <ta e="T486" id="Seg_6210" s="T485">найти-EP-3SG.O</ta>
            <ta e="T487" id="Seg_6211" s="T486">посудина-ACC</ta>
            <ta e="T488" id="Seg_6212" s="T487">сесть-3SG.S</ta>
            <ta e="T489" id="Seg_6213" s="T488">этот</ta>
            <ta e="T490" id="Seg_6214" s="T489">посудина-ACC</ta>
            <ta e="T491" id="Seg_6215" s="T490">отвязать-3SG.O</ta>
            <ta e="T492" id="Seg_6216" s="T491">и</ta>
            <ta e="T493" id="Seg_6217" s="T492">ощупать-DUR-3SG.O</ta>
            <ta e="T494" id="Seg_6218" s="T493">что-INDEF.[NOM]</ta>
            <ta e="T495" id="Seg_6219" s="T494">два</ta>
            <ta e="T496" id="Seg_6220" s="T495">круг-PL-ATTEN.[NOM]</ta>
            <ta e="T497" id="Seg_6221" s="T496">этот</ta>
            <ta e="T498" id="Seg_6222" s="T497">однако</ta>
            <ta e="T499" id="Seg_6223" s="T498">я-ADES-ADJZ</ta>
            <ta e="T500" id="Seg_6224" s="T499">глаз-PL.[NOM]</ta>
            <ta e="T501" id="Seg_6225" s="T500">горячий</ta>
            <ta e="T502" id="Seg_6226" s="T501">вода-EP-ACC</ta>
            <ta e="T503" id="Seg_6227" s="T502">найти-CO-3SG.O</ta>
            <ta e="T504" id="Seg_6228" s="T503">муж.[NOM]-3SG</ta>
            <ta e="T505" id="Seg_6229" s="T504">вода-ILL</ta>
            <ta e="T506" id="Seg_6230" s="T505">намокнуть-CAUS-3SG.O</ta>
            <ta e="T507" id="Seg_6231" s="T506">глаз-PL.[NOM]-3SG</ta>
            <ta e="T508" id="Seg_6232" s="T507">два</ta>
            <ta e="T509" id="Seg_6233" s="T508">глаз.[NOM]</ta>
            <ta e="T510" id="Seg_6234" s="T509">столько</ta>
            <ta e="T511" id="Seg_6235" s="T510">вода-LOC</ta>
            <ta e="T512" id="Seg_6236" s="T511">намокнуть-DRV-DUR-PST-3PL</ta>
            <ta e="T513" id="Seg_6237" s="T512">сильно</ta>
            <ta e="T514" id="Seg_6238" s="T513">вымыть-3SG.O</ta>
            <ta e="T515" id="Seg_6239" s="T514">глаз-PL-ILL.3SG</ta>
            <ta e="T516" id="Seg_6240" s="T515">сказать-3SG.S</ta>
            <ta e="T517" id="Seg_6241" s="T516">глаз-PL.[NOM]</ta>
            <ta e="T518" id="Seg_6242" s="T517">приклеиться-DUR-OPT.[3SG.S]</ta>
            <ta e="T519" id="Seg_6243" s="T518">глаз-PL.[NOM]-3SG</ta>
            <ta e="T520" id="Seg_6244" s="T519">он(а)-EP-ALL</ta>
            <ta e="T521" id="Seg_6245" s="T520">прилипнуть-3PL</ta>
            <ta e="T522" id="Seg_6246" s="T521">посмотреть-DRV-3SG.S</ta>
            <ta e="T523" id="Seg_6247" s="T522">а</ta>
            <ta e="T524" id="Seg_6248" s="T523">глаз-PL.[NOM]-3SG</ta>
            <ta e="T525" id="Seg_6249" s="T524">хороший-ADVZ</ta>
            <ta e="T526" id="Seg_6250" s="T525">NEG</ta>
            <ta e="T527" id="Seg_6251" s="T526">видеть-3PL</ta>
            <ta e="T528" id="Seg_6252" s="T527">глаз-PL-ACC-3SG</ta>
            <ta e="T529" id="Seg_6253" s="T528">выкопать-CO-3SG.O</ta>
            <ta e="T530" id="Seg_6254" s="T529">опять</ta>
            <ta e="T531" id="Seg_6255" s="T530">намокнуть-CAUS-3SG.O</ta>
            <ta e="T532" id="Seg_6256" s="T531">хороший-ADVZ</ta>
            <ta e="T533" id="Seg_6257" s="T532">глаз-PL-ACC-3SG</ta>
            <ta e="T534" id="Seg_6258" s="T533">опять</ta>
            <ta e="T535" id="Seg_6259" s="T534">хороший-ADVZ</ta>
            <ta e="T536" id="Seg_6260" s="T535">вытереть-CO-3SG.O</ta>
            <ta e="T537" id="Seg_6261" s="T536">сказать-3SG.S</ta>
            <ta e="T538" id="Seg_6262" s="T537">глаз-PL.[NOM]</ta>
            <ta e="T539" id="Seg_6263" s="T538">приклеиться-DUR-OPT.[3SG.S]</ta>
            <ta e="T540" id="Seg_6264" s="T539">глаз-PL.[NOM]-3SG</ta>
            <ta e="T541" id="Seg_6265" s="T540">прилипнуть-3PL</ta>
            <ta e="T542" id="Seg_6266" s="T541">назад</ta>
            <ta e="T543" id="Seg_6267" s="T542">мужчина-человек.[NOM]</ta>
            <ta e="T544" id="Seg_6268" s="T543">обрадоваться-CO-3SG.S</ta>
            <ta e="T545" id="Seg_6269" s="T544">теперь</ta>
            <ta e="T546" id="Seg_6270" s="T545">глаз-PL.[NOM]-1SG</ta>
            <ta e="T547" id="Seg_6271" s="T546">хороший-ADVZ</ta>
            <ta e="T548" id="Seg_6272" s="T547">увидеть-DRV-CO-3PL</ta>
            <ta e="T549" id="Seg_6273" s="T548">теперь</ta>
            <ta e="T550" id="Seg_6274" s="T549">я.NOM</ta>
            <ta e="T551" id="Seg_6275" s="T550">глаз-COM</ta>
            <ta e="T552" id="Seg_6276" s="T551">стать-1SG.S</ta>
            <ta e="T553" id="Seg_6277" s="T552">уже</ta>
            <ta e="T554" id="Seg_6278" s="T553">я.NOM</ta>
            <ta e="T555" id="Seg_6279" s="T554">ты.ACC</ta>
            <ta e="T556" id="Seg_6280" s="T555">глаз-PL-LOC</ta>
            <ta e="T557" id="Seg_6281" s="T556">из_за</ta>
            <ta e="T558" id="Seg_6282" s="T557">%%-FUT-1SG.S-FUT</ta>
            <ta e="T559" id="Seg_6283" s="T558">наружу</ta>
            <ta e="T560" id="Seg_6284" s="T559">выйти-3SG.S</ta>
            <ta e="T561" id="Seg_6285" s="T560">а</ta>
            <ta e="T562" id="Seg_6286" s="T561">жена.[NOM]-3SG</ta>
            <ta e="T563" id="Seg_6287" s="T562">качать-HAB-RFL-3SG.S</ta>
            <ta e="T564" id="Seg_6288" s="T563">бог.[NOM]</ta>
            <ta e="T565" id="Seg_6289" s="T564">песня-ACC</ta>
            <ta e="T566" id="Seg_6290" s="T565">принести-EP-FRQ-EP-3SG.O</ta>
            <ta e="T567" id="Seg_6291" s="T566">муж.[NOM]-3SG</ta>
            <ta e="T568" id="Seg_6292" s="T567">сказать-3SG.S</ta>
            <ta e="T569" id="Seg_6293" s="T568">жена-OBL.3SG-ALL</ta>
            <ta e="T570" id="Seg_6294" s="T569">а</ta>
            <ta e="T571" id="Seg_6295" s="T570">ты.NOM</ta>
            <ta e="T572" id="Seg_6296" s="T571">я.ACC</ta>
            <ta e="T573" id="Seg_6297" s="T572">что-TRL</ta>
            <ta e="T574" id="Seg_6298" s="T573">ослепить-2SG.S</ta>
            <ta e="T575" id="Seg_6299" s="T574">а</ta>
            <ta e="T576" id="Seg_6300" s="T575">жена.[NOM]-3SG</ta>
            <ta e="T577" id="Seg_6301" s="T576">сказать-3SG.S</ta>
            <ta e="T578" id="Seg_6302" s="T577">а</ta>
            <ta e="T579" id="Seg_6303" s="T578">ты.NOM</ta>
            <ta e="T580" id="Seg_6304" s="T579">что-TRL</ta>
            <ta e="T581" id="Seg_6305" s="T580">так-ADVZ</ta>
            <ta e="T582" id="Seg_6306" s="T581">ходить-HAB-PST-2SG.S</ta>
            <ta e="T583" id="Seg_6307" s="T582">сходить-HAB-PST-2SG.S</ta>
            <ta e="T584" id="Seg_6308" s="T583">зверь-CAP-INF</ta>
            <ta e="T585" id="Seg_6309" s="T584">а</ta>
            <ta e="T586" id="Seg_6310" s="T585">сам.2SG</ta>
            <ta e="T587" id="Seg_6311" s="T586">глаз-PL-OBL.2SG-COM</ta>
            <ta e="T588" id="Seg_6312" s="T587">играть-PST-2SG.S</ta>
            <ta e="T589" id="Seg_6313" s="T588">а</ta>
            <ta e="T590" id="Seg_6314" s="T589">дом-ILL</ta>
            <ta e="T591" id="Seg_6315" s="T590">что-ACC-EMPH</ta>
            <ta e="T592" id="Seg_6316" s="T591">NEG</ta>
            <ta e="T593" id="Seg_6317" s="T592">принести-HAB-PST-2SG.O</ta>
            <ta e="T594" id="Seg_6318" s="T593">вот</ta>
            <ta e="T595" id="Seg_6319" s="T594">я.NOM</ta>
            <ta e="T596" id="Seg_6320" s="T595">ты-ADES-ADJZ</ta>
            <ta e="T597" id="Seg_6321" s="T596">глаз-PL-ACC-3SG</ta>
            <ta e="T598" id="Seg_6322" s="T597">поэтому</ta>
            <ta e="T599" id="Seg_6323" s="T598">и</ta>
            <ta e="T600" id="Seg_6324" s="T599">выкопать-DRV-CO-1SG.O</ta>
            <ta e="T601" id="Seg_6325" s="T600">принести-CO-1SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_6326" s="T1">v-v:tense-v:pn</ta>
            <ta e="T3" id="Seg_6327" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_6328" s="T3">n.[n:case]</ta>
            <ta e="T5" id="Seg_6329" s="T4">n-n&gt;n-n:num.[n:case]</ta>
            <ta e="T6" id="Seg_6330" s="T5">n.[n:case]-n:poss</ta>
            <ta e="T7" id="Seg_6331" s="T6">v-v&gt;v-v:pn</ta>
            <ta e="T8" id="Seg_6332" s="T7">v-v&gt;adv</ta>
            <ta e="T9" id="Seg_6333" s="T8">n.[n:case]-n:poss</ta>
            <ta e="T10" id="Seg_6334" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_6335" s="T10">v-v:ins-v:pn</ta>
            <ta e="T12" id="Seg_6336" s="T11">n.[n:case]-n:poss</ta>
            <ta e="T13" id="Seg_6337" s="T12">v-v&gt;v-v:pn</ta>
            <ta e="T14" id="Seg_6338" s="T13">v-v&gt;v-v:pn</ta>
            <ta e="T15" id="Seg_6339" s="T14">n-pp</ta>
            <ta e="T16" id="Seg_6340" s="T15">v-v&gt;v-v:pn</ta>
            <ta e="T17" id="Seg_6341" s="T16">interrog-n:case-clit</ta>
            <ta e="T18" id="Seg_6342" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_6343" s="T18">v-v&gt;v-v:pn</ta>
            <ta e="T20" id="Seg_6344" s="T19">adv</ta>
            <ta e="T21" id="Seg_6345" s="T20">adv</ta>
            <ta e="T22" id="Seg_6346" s="T21">v-v&gt;v-v:pn</ta>
            <ta e="T23" id="Seg_6347" s="T22">interrog-n:case-clit</ta>
            <ta e="T24" id="Seg_6348" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_6349" s="T24">v-v&gt;v-v:pn</ta>
            <ta e="T26" id="Seg_6350" s="T25">num</ta>
            <ta e="T27" id="Seg_6351" s="T26">n.[n:case]</ta>
            <ta e="T28" id="Seg_6352" s="T27">n.[n:case]-n:poss</ta>
            <ta e="T29" id="Seg_6353" s="T28">v-v:pn</ta>
            <ta e="T30" id="Seg_6354" s="T29">interrog-n:case</ta>
            <ta e="T31" id="Seg_6355" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_6356" s="T31">pers.[n:case]</ta>
            <ta e="T33" id="Seg_6357" s="T32">interrog-n:case-clit</ta>
            <ta e="T34" id="Seg_6358" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_6359" s="T34">v-v&gt;v-v:pn</ta>
            <ta e="T36" id="Seg_6360" s="T35">pers</ta>
            <ta e="T37" id="Seg_6361" s="T36">pers-n:ins-n:case</ta>
            <ta e="T38" id="Seg_6362" s="T37">v-v:tense-v:pn</ta>
            <ta e="T39" id="Seg_6363" s="T38">interrog-n:case</ta>
            <ta e="T40" id="Seg_6364" s="T39">pers.[n:case]</ta>
            <ta e="T41" id="Seg_6365" s="T40">v-v&gt;ptcp-adj&gt;adv</ta>
            <ta e="T42" id="Seg_6366" s="T41">v-v&gt;v-v:pn</ta>
            <ta e="T43" id="Seg_6367" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_6368" s="T43">conj</ta>
            <ta e="T45" id="Seg_6369" s="T44">adv-adj&gt;adv</ta>
            <ta e="T46" id="Seg_6370" s="T45">v-v:ins-v:pn</ta>
            <ta e="T47" id="Seg_6371" s="T46">n.[n:case]-n:poss</ta>
            <ta e="T48" id="Seg_6372" s="T47">v-v&gt;adv</ta>
            <ta e="T49" id="Seg_6373" s="T48">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T50" id="Seg_6374" s="T49">v-v&gt;adv</ta>
            <ta e="T51" id="Seg_6375" s="T50">v-v:ins-v:pn</ta>
            <ta e="T52" id="Seg_6376" s="T51">n.[n:case]-n:poss</ta>
            <ta e="T53" id="Seg_6377" s="T52">adv</ta>
            <ta e="T54" id="Seg_6378" s="T53">n-n:ins-n:case-n:poss</ta>
            <ta e="T55" id="Seg_6379" s="T54">v-v:ins-v:pn</ta>
            <ta e="T56" id="Seg_6380" s="T55">n-n:case-n:poss</ta>
            <ta e="T57" id="Seg_6381" s="T56">pp</ta>
            <ta e="T58" id="Seg_6382" s="T57">adv</ta>
            <ta e="T59" id="Seg_6383" s="T58">v-v:pn</ta>
            <ta e="T60" id="Seg_6384" s="T59">n-n:obl.poss-n:case</ta>
            <ta e="T61" id="Seg_6385" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_6386" s="T61">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T63" id="Seg_6387" s="T62">conj</ta>
            <ta e="T64" id="Seg_6388" s="T63">ptcl</ta>
            <ta e="T65" id="Seg_6389" s="T64">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T66" id="Seg_6390" s="T65">pers-n:ins-n:case</ta>
            <ta e="T67" id="Seg_6391" s="T66">n.[n:case]-n:poss</ta>
            <ta e="T68" id="Seg_6392" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_6393" s="T68">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T70" id="Seg_6394" s="T69">n.[n:case]-n:poss</ta>
            <ta e="T71" id="Seg_6395" s="T70">v-v:pn</ta>
            <ta e="T72" id="Seg_6396" s="T71">nprop-n:case</ta>
            <ta e="T73" id="Seg_6397" s="T72">n-n:case-n-n:case</ta>
            <ta e="T74" id="Seg_6398" s="T73">dem</ta>
            <ta e="T75" id="Seg_6399" s="T74">n-n&gt;adj</ta>
            <ta e="T76" id="Seg_6400" s="T75">n-n:case</ta>
            <ta e="T77" id="Seg_6401" s="T76">n-n:case</ta>
            <ta e="T78" id="Seg_6402" s="T77">emphpro</ta>
            <ta e="T79" id="Seg_6403" s="T78">n-n:case-n:poss</ta>
            <ta e="T80" id="Seg_6404" s="T79">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T81" id="Seg_6405" s="T80">n-n:num-n:case-n:poss</ta>
            <ta e="T82" id="Seg_6406" s="T81">v-v&gt;adv</ta>
            <ta e="T83" id="Seg_6407" s="T82">v-v&gt;v-v:pn</ta>
            <ta e="T84" id="Seg_6408" s="T83">n-n:case</ta>
            <ta e="T85" id="Seg_6409" s="T84">n-n:num-n:case</ta>
            <ta e="T86" id="Seg_6410" s="T85">dem</ta>
            <ta e="T87" id="Seg_6411" s="T86">n-n&gt;adj</ta>
            <ta e="T88" id="Seg_6412" s="T87">n-n:case</ta>
            <ta e="T89" id="Seg_6413" s="T88">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T90" id="Seg_6414" s="T89">emphpro</ta>
            <ta e="T91" id="Seg_6415" s="T90">v-v&gt;v-v:pn</ta>
            <ta e="T92" id="Seg_6416" s="T91">n.[n:case]</ta>
            <ta e="T93" id="Seg_6417" s="T92">v-v&gt;v-v:mood.[v:pn]</ta>
            <ta e="T94" id="Seg_6418" s="T93">n-n:num.[n:case]</ta>
            <ta e="T95" id="Seg_6419" s="T94">pers-n:case</ta>
            <ta e="T96" id="Seg_6420" s="T95">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T97" id="Seg_6421" s="T96">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T98" id="Seg_6422" s="T97">adv</ta>
            <ta e="T99" id="Seg_6423" s="T98">adv-adv&gt;adj</ta>
            <ta e="T100" id="Seg_6424" s="T99">n-n:case</ta>
            <ta e="T101" id="Seg_6425" s="T100">n-n:ins-n:case</ta>
            <ta e="T102" id="Seg_6426" s="T101">n-n:case-n:poss</ta>
            <ta e="T103" id="Seg_6427" s="T102">v-v&gt;v-v&gt;adv</ta>
            <ta e="T104" id="Seg_6428" s="T103">v-v&gt;v-v:pn</ta>
            <ta e="T105" id="Seg_6429" s="T104">n-n:case</ta>
            <ta e="T106" id="Seg_6430" s="T105">n-n:num-n:case</ta>
            <ta e="T107" id="Seg_6431" s="T106">v-v&gt;v-v:pn</ta>
            <ta e="T108" id="Seg_6432" s="T107">v-v&gt;v-v:pn</ta>
            <ta e="T109" id="Seg_6433" s="T108">n-n:num.[n:case]</ta>
            <ta e="T110" id="Seg_6434" s="T109">v-v&gt;v-v:mood.[v:pn]</ta>
            <ta e="T111" id="Seg_6435" s="T110">n-n:num.[n:case]</ta>
            <ta e="T112" id="Seg_6436" s="T111">pers-n:case</ta>
            <ta e="T113" id="Seg_6437" s="T112">v-v&gt;adv</ta>
            <ta e="T114" id="Seg_6438" s="T113">v-v&gt;v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T116" id="Seg_6439" s="T114">adv</ta>
            <ta e="T117" id="Seg_6440" s="T116">adj</ta>
            <ta e="T118" id="Seg_6441" s="T117">n-pp</ta>
            <ta e="T119" id="Seg_6442" s="T118">v:pn</ta>
            <ta e="T120" id="Seg_6443" s="T119">n.[n:case]-n:poss</ta>
            <ta e="T121" id="Seg_6444" s="T120">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T122" id="Seg_6445" s="T121">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T123" id="Seg_6446" s="T122">v-n&gt;n-v:pn</ta>
            <ta e="T124" id="Seg_6447" s="T123">n.[n:case]-n:poss</ta>
            <ta e="T125" id="Seg_6448" s="T124">n-n:case-n:poss</ta>
            <ta e="T126" id="Seg_6449" s="T125">v-v:mood-v:pn</ta>
            <ta e="T127" id="Seg_6450" s="T126">dem</ta>
            <ta e="T128" id="Seg_6451" s="T127">n-n&gt;adj</ta>
            <ta e="T129" id="Seg_6452" s="T128">n-n:case</ta>
            <ta e="T130" id="Seg_6453" s="T129">n-n:case-n-n:case</ta>
            <ta e="T131" id="Seg_6454" s="T130">v-v:pn</ta>
            <ta e="T132" id="Seg_6455" s="T131">conj</ta>
            <ta e="T133" id="Seg_6456" s="T132">n.[n:case]-n:poss</ta>
            <ta e="T134" id="Seg_6457" s="T133">adv</ta>
            <ta e="T135" id="Seg_6458" s="T134">pers-n:ins-n:case</ta>
            <ta e="T136" id="Seg_6459" s="T135">n-n:num-n:case-n:poss</ta>
            <ta e="T137" id="Seg_6460" s="T136">v-n:ins-v:pn</ta>
            <ta e="T139" id="Seg_6461" s="T138">emphpro</ta>
            <ta e="T140" id="Seg_6462" s="T139">v-v:pn</ta>
            <ta e="T141" id="Seg_6463" s="T140">interj</ta>
            <ta e="T142" id="Seg_6464" s="T141">pers</ta>
            <ta e="T143" id="Seg_6465" s="T142">conj</ta>
            <ta e="T144" id="Seg_6466" s="T143">n-n:case</ta>
            <ta e="T145" id="Seg_6467" s="T144">interrog-n:case-clit</ta>
            <ta e="T146" id="Seg_6468" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_6469" s="T146">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T148" id="Seg_6470" s="T147">pers</ta>
            <ta e="T149" id="Seg_6471" s="T148">adv-adj&gt;adv</ta>
            <ta e="T150" id="Seg_6472" s="T149">n.[n:case]-n:poss</ta>
            <ta e="T151" id="Seg_6473" s="T150">v-v:ins-v:pn</ta>
            <ta e="T152" id="Seg_6474" s="T151">v-v:ins-v:pn</ta>
            <ta e="T153" id="Seg_6475" s="T152">conj</ta>
            <ta e="T154" id="Seg_6476" s="T153">adv</ta>
            <ta e="T155" id="Seg_6477" s="T154">v-v:pn</ta>
            <ta e="T156" id="Seg_6478" s="T155">n-n:obl.poss-n:case</ta>
            <ta e="T157" id="Seg_6479" s="T156">n.[n:case]-n:poss</ta>
            <ta e="T158" id="Seg_6480" s="T157">adv</ta>
            <ta e="T159" id="Seg_6481" s="T158">v-v:ins.[v:pn]</ta>
            <ta e="T160" id="Seg_6482" s="T159">v-v:inf</ta>
            <ta e="T161" id="Seg_6483" s="T160">pers-n:ins-n:case</ta>
            <ta e="T162" id="Seg_6484" s="T161">pers.[n:case]</ta>
            <ta e="T163" id="Seg_6485" s="T162">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T164" id="Seg_6486" s="T163">conj</ta>
            <ta e="T165" id="Seg_6487" s="T164">conj</ta>
            <ta e="T166" id="Seg_6488" s="T165">adv</ta>
            <ta e="T167" id="Seg_6489" s="T166">v-v:pn</ta>
            <ta e="T168" id="Seg_6490" s="T167">n.[n:case]-n:poss</ta>
            <ta e="T169" id="Seg_6491" s="T168">interrog</ta>
            <ta e="T170" id="Seg_6492" s="T169">v-v:pn</ta>
            <ta e="T171" id="Seg_6493" s="T170">adv</ta>
            <ta e="T172" id="Seg_6494" s="T171">adv</ta>
            <ta e="T173" id="Seg_6495" s="T172">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T174" id="Seg_6496" s="T173">n.[n:case]-n:poss</ta>
            <ta e="T175" id="Seg_6497" s="T174">v-v:pn</ta>
            <ta e="T176" id="Seg_6498" s="T175">conj</ta>
            <ta e="T177" id="Seg_6499" s="T176">ptcl</ta>
            <ta e="T178" id="Seg_6500" s="T177">n-n:case-n:poss</ta>
            <ta e="T179" id="Seg_6501" s="T178">v-v&gt;adv</ta>
            <ta e="T180" id="Seg_6502" s="T179">n.[n:case]-n:poss</ta>
            <ta e="T181" id="Seg_6503" s="T180">adv</ta>
            <ta e="T182" id="Seg_6504" s="T181">v-v:ins-v:pn</ta>
            <ta e="T183" id="Seg_6505" s="T182">n-n:num.[n:case]</ta>
            <ta e="T184" id="Seg_6506" s="T183">v-v&gt;v-v:mood.[v:pn]</ta>
            <ta e="T185" id="Seg_6507" s="T184">n-n:num.[n:case]</ta>
            <ta e="T186" id="Seg_6508" s="T185">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T187" id="Seg_6509" s="T186">ptcl</ta>
            <ta e="T188" id="Seg_6510" s="T187">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T189" id="Seg_6511" s="T188">pers-n:case</ta>
            <ta e="T190" id="Seg_6512" s="T189">conj</ta>
            <ta e="T191" id="Seg_6513" s="T190">ptcl</ta>
            <ta e="T192" id="Seg_6514" s="T191">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T193" id="Seg_6515" s="T192">interrog</ta>
            <ta e="T194" id="Seg_6516" s="T193">n-n:num-n:case-n:poss</ta>
            <ta e="T195" id="Seg_6517" s="T194">v-v:tense-v:pn</ta>
            <ta e="T196" id="Seg_6518" s="T195">v-v:tense-v:pn</ta>
            <ta e="T197" id="Seg_6519" s="T196">n-n:num.[n:case]</ta>
            <ta e="T198" id="Seg_6520" s="T197">interrog</ta>
            <ta e="T199" id="Seg_6521" s="T198">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T200" id="Seg_6522" s="T199">adv</ta>
            <ta e="T201" id="Seg_6523" s="T200">conj</ta>
            <ta e="T202" id="Seg_6524" s="T201">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T203" id="Seg_6525" s="T202">adv</ta>
            <ta e="T204" id="Seg_6526" s="T203">n-n&gt;adj-adj&gt;adv</ta>
            <ta e="T205" id="Seg_6527" s="T204">n.[n:case]-n:poss</ta>
            <ta e="T206" id="Seg_6528" s="T205">ptcl</ta>
            <ta e="T207" id="Seg_6529" s="T206">v-v:pn</ta>
            <ta e="T208" id="Seg_6530" s="T207">n-n&gt;adj-adj&gt;adv</ta>
            <ta e="T209" id="Seg_6531" s="T208">n.[n:case]-n:poss</ta>
            <ta e="T210" id="Seg_6532" s="T209">v-v&gt;v-v:pn</ta>
            <ta e="T211" id="Seg_6533" s="T210">n-n:obl.poss-n:case</ta>
            <ta e="T212" id="Seg_6534" s="T211">ptcl</ta>
            <ta e="T213" id="Seg_6535" s="T212">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T214" id="Seg_6536" s="T213">n.[n:case]-n:poss</ta>
            <ta e="T215" id="Seg_6537" s="T214">n-n&gt;adj-adj&gt;adv</ta>
            <ta e="T216" id="Seg_6538" s="T215">v-v&gt;v-v&gt;adv</ta>
            <ta e="T217" id="Seg_6539" s="T216">v-v:tense-v:pn</ta>
            <ta e="T218" id="Seg_6540" s="T217">n-n:ins-n:case</ta>
            <ta e="T219" id="Seg_6541" s="T218">n-n:case</ta>
            <ta e="T220" id="Seg_6542" s="T219">v-v&gt;adv</ta>
            <ta e="T221" id="Seg_6543" s="T220">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T222" id="Seg_6544" s="T221">n.[n:case]-n:poss</ta>
            <ta e="T223" id="Seg_6545" s="T222">v-v:pn</ta>
            <ta e="T224" id="Seg_6546" s="T223">adv</ta>
            <ta e="T225" id="Seg_6547" s="T224">v-v&gt;v-v:pn</ta>
            <ta e="T226" id="Seg_6548" s="T225">n-n:num-n:case-n:poss</ta>
            <ta e="T0" id="Seg_6549" s="T226">adv</ta>
            <ta e="T227" id="Seg_6550" s="T0">preverb</ta>
            <ta e="T228" id="Seg_6551" s="T227">v-v:ins-v:pn</ta>
            <ta e="T229" id="Seg_6552" s="T228">n-n:case</ta>
            <ta e="T230" id="Seg_6553" s="T229">v-v:ins-v:pn</ta>
            <ta e="T231" id="Seg_6554" s="T230">v-v:ins-v:pn</ta>
            <ta e="T232" id="Seg_6555" s="T231">n-n:case-n:poss</ta>
            <ta e="T233" id="Seg_6556" s="T232">n-n:num-n:case</ta>
            <ta e="T234" id="Seg_6557" s="T233">n-n:case</ta>
            <ta e="T235" id="Seg_6558" s="T234">v-v:ins-v:pn</ta>
            <ta e="T236" id="Seg_6559" s="T235">n-n:case</ta>
            <ta e="T237" id="Seg_6560" s="T236">n-n:case</ta>
            <ta e="T238" id="Seg_6561" s="T237">v-v:ins-v:pn</ta>
            <ta e="T239" id="Seg_6562" s="T238">n.[n:case]-n:poss</ta>
            <ta e="T240" id="Seg_6563" s="T239">adv</ta>
            <ta e="T241" id="Seg_6564" s="T240">v-v:ins-v:pn</ta>
            <ta e="T242" id="Seg_6565" s="T241">n-n:case</ta>
            <ta e="T243" id="Seg_6566" s="T242">v-v:ins-v:pn</ta>
            <ta e="T244" id="Seg_6567" s="T243">n.[n:case]-n:poss</ta>
            <ta e="T245" id="Seg_6568" s="T244">v-v:pn</ta>
            <ta e="T246" id="Seg_6569" s="T245">conj</ta>
            <ta e="T247" id="Seg_6570" s="T246">pers</ta>
            <ta e="T248" id="Seg_6571" s="T247">interrog-n:case</ta>
            <ta e="T249" id="Seg_6572" s="T248">dem-n.[n:case]</ta>
            <ta e="T250" id="Seg_6573" s="T249">adv-adj&gt;adv</ta>
            <ta e="T251" id="Seg_6574" s="T250">v-v:ins-v:pn</ta>
            <ta e="T252" id="Seg_6575" s="T251">n.[n:case]-n:poss</ta>
            <ta e="T253" id="Seg_6576" s="T252">v-v:pn</ta>
            <ta e="T254" id="Seg_6577" s="T253">pers</ta>
            <ta e="T255" id="Seg_6578" s="T254">dem-n.[n:case]</ta>
            <ta e="T256" id="Seg_6579" s="T255">n-n:ins-n:poss-n:ins-n:case</ta>
            <ta e="T257" id="Seg_6580" s="T256">v-v:pn</ta>
            <ta e="T258" id="Seg_6581" s="T257">conj</ta>
            <ta e="T259" id="Seg_6582" s="T258">adv-adj&gt;adv</ta>
            <ta e="T260" id="Seg_6583" s="T259">v-v:ins-v:pn</ta>
            <ta e="T261" id="Seg_6584" s="T260">n.[n:case]-n:poss</ta>
            <ta e="T262" id="Seg_6585" s="T261">v-v:pn</ta>
            <ta e="T263" id="Seg_6586" s="T262">conj</ta>
            <ta e="T264" id="Seg_6587" s="T263">pers</ta>
            <ta e="T265" id="Seg_6588" s="T264">v-v:pn</ta>
            <ta e="T266" id="Seg_6589" s="T265">pers</ta>
            <ta e="T267" id="Seg_6590" s="T266">adv</ta>
            <ta e="T268" id="Seg_6591" s="T267">pers</ta>
            <ta e="T269" id="Seg_6592" s="T268">v-v:ins-v:pn</ta>
            <ta e="T270" id="Seg_6593" s="T269">conj</ta>
            <ta e="T271" id="Seg_6594" s="T270">pers</ta>
            <ta e="T272" id="Seg_6595" s="T271">v-v:ins-v:pn</ta>
            <ta e="T273" id="Seg_6596" s="T272">adj</ta>
            <ta e="T274" id="Seg_6597" s="T273">n-pp</ta>
            <ta e="T275" id="Seg_6598" s="T274">n-n:num-n:obl.poss-n:case</ta>
            <ta e="T276" id="Seg_6599" s="T275">v-v:pn</ta>
            <ta e="T277" id="Seg_6600" s="T276">adj</ta>
            <ta e="T278" id="Seg_6601" s="T277">n-n:case</ta>
            <ta e="T279" id="Seg_6602" s="T278">n-n:case</ta>
            <ta e="T280" id="Seg_6603" s="T279">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T281" id="Seg_6604" s="T280">conj</ta>
            <ta e="T282" id="Seg_6605" s="T281">dem</ta>
            <ta e="T283" id="Seg_6606" s="T282">n-n&gt;adj</ta>
            <ta e="T284" id="Seg_6607" s="T283">n-adv:case</ta>
            <ta e="T285" id="Seg_6608" s="T284">n-n:case</ta>
            <ta e="T286" id="Seg_6609" s="T285">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T287" id="Seg_6610" s="T286">n-n:num.[n:case]-n:poss</ta>
            <ta e="T288" id="Seg_6611" s="T287">pers</ta>
            <ta e="T289" id="Seg_6612" s="T288">v-v&gt;adv</ta>
            <ta e="T290" id="Seg_6613" s="T289">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T291" id="Seg_6614" s="T290">adv-adj&gt;adv</ta>
            <ta e="T292" id="Seg_6615" s="T291">pers</ta>
            <ta e="T293" id="Seg_6616" s="T292">conj</ta>
            <ta e="T294" id="Seg_6617" s="T293">v-v&gt;ptcp-adj&gt;adv</ta>
            <ta e="T295" id="Seg_6618" s="T294">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T296" id="Seg_6619" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_6620" s="T296">conj</ta>
            <ta e="T298" id="Seg_6621" s="T297">v-v&gt;adv</ta>
            <ta e="T299" id="Seg_6622" s="T298">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T300" id="Seg_6623" s="T299">interrog</ta>
            <ta e="T301" id="Seg_6624" s="T300">ptcl</ta>
            <ta e="T302" id="Seg_6625" s="T301">n.[n:case]-n:ins-n:poss</ta>
            <ta e="T303" id="Seg_6626" s="T302">v-v:pn</ta>
            <ta e="T304" id="Seg_6627" s="T303">n.[n:case]-n:poss</ta>
            <ta e="T305" id="Seg_6628" s="T304">v-v&gt;v-v:pn</ta>
            <ta e="T306" id="Seg_6629" s="T305">n-n:case</ta>
            <ta e="T307" id="Seg_6630" s="T306">v-v&gt;v-v:pn</ta>
            <ta e="T308" id="Seg_6631" s="T307">v-v:ins-v:pn</ta>
            <ta e="T309" id="Seg_6632" s="T308">n.[n:case]</ta>
            <ta e="T310" id="Seg_6633" s="T309">n.[n:case]-n:poss</ta>
            <ta e="T311" id="Seg_6634" s="T310">n-n:obl.poss-n:case</ta>
            <ta e="T312" id="Seg_6635" s="T311">v-v:pn</ta>
            <ta e="T313" id="Seg_6636" s="T312">n.[n:case]</ta>
            <ta e="T314" id="Seg_6637" s="T313">v-v&gt;v-v:mood.[v:pn]</ta>
            <ta e="T315" id="Seg_6638" s="T314">pers</ta>
            <ta e="T316" id="Seg_6639" s="T315">v-v:mood-v:pn</ta>
            <ta e="T317" id="Seg_6640" s="T316">interrog-clit</ta>
            <ta e="T318" id="Seg_6641" s="T317">v-v:mood-v:pn</ta>
            <ta e="T319" id="Seg_6642" s="T318">n-n:ins-n:case</ta>
            <ta e="T320" id="Seg_6643" s="T319">conj</ta>
            <ta e="T321" id="Seg_6644" s="T320">n.[n:case]-n:poss</ta>
            <ta e="T322" id="Seg_6645" s="T321">v-v:pn</ta>
            <ta e="T323" id="Seg_6646" s="T322">pers</ta>
            <ta e="T324" id="Seg_6647" s="T323">interrog</ta>
            <ta e="T325" id="Seg_6648" s="T324">v-v:tense-v:pn</ta>
            <ta e="T326" id="Seg_6649" s="T325">n.[n:case]-n:ins-n:poss</ta>
            <ta e="T327" id="Seg_6650" s="T326">ptcl</ta>
            <ta e="T328" id="Seg_6651" s="T327">v-v:pn</ta>
            <ta e="T329" id="Seg_6652" s="T328">n.[n:case]-n:poss</ta>
            <ta e="T330" id="Seg_6653" s="T329">v-v:pn</ta>
            <ta e="T331" id="Seg_6654" s="T330">v-v:mood.pn</ta>
            <ta e="T332" id="Seg_6655" s="T331">pers</ta>
            <ta e="T333" id="Seg_6656" s="T332">pers</ta>
            <ta e="T334" id="Seg_6657" s="T333">n-n:case</ta>
            <ta e="T335" id="Seg_6658" s="T334">pers</ta>
            <ta e="T336" id="Seg_6659" s="T335">v-v&gt;v-v&gt;v-v:tense-v:pn-v:tense</ta>
            <ta e="T337" id="Seg_6660" s="T336">conj</ta>
            <ta e="T338" id="Seg_6661" s="T337">n.[n:case]-n:poss</ta>
            <ta e="T339" id="Seg_6662" s="T338">v-n:ins-v:pn</ta>
            <ta e="T340" id="Seg_6663" s="T339">n-n:obl.poss-n:case</ta>
            <ta e="T341" id="Seg_6664" s="T340">n.[n:case]-n:poss</ta>
            <ta e="T342" id="Seg_6665" s="T341">n-n:obl.poss-n:case</ta>
            <ta e="T343" id="Seg_6666" s="T342">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T344" id="Seg_6667" s="T343">n-n:case.poss</ta>
            <ta e="T345" id="Seg_6668" s="T344">n-n&gt;v-v&gt;ptcp</ta>
            <ta e="T346" id="Seg_6669" s="T345">n-n:case</ta>
            <ta e="T347" id="Seg_6670" s="T346">emphpro</ta>
            <ta e="T348" id="Seg_6671" s="T347">pers-n:case</ta>
            <ta e="T349" id="Seg_6672" s="T348">v-v:ins-v:pn</ta>
            <ta e="T350" id="Seg_6673" s="T349">conj</ta>
            <ta e="T351" id="Seg_6674" s="T350">v-v:inf</ta>
            <ta e="T352" id="Seg_6675" s="T351">n-n:ins-n:case</ta>
            <ta e="T353" id="Seg_6676" s="T352">n.[n:case]-n:poss</ta>
            <ta e="T354" id="Seg_6677" s="T353">v-v:pn</ta>
            <ta e="T355" id="Seg_6678" s="T354">v-v:ins-v:pn</ta>
            <ta e="T356" id="Seg_6679" s="T355">n-n:ins-n:case</ta>
            <ta e="T357" id="Seg_6680" s="T356">n-n:case-n:poss</ta>
            <ta e="T358" id="Seg_6681" s="T357">n.[n:case]</ta>
            <ta e="T359" id="Seg_6682" s="T358">ptcl</ta>
            <ta e="T360" id="Seg_6683" s="T359">v-v:pn</ta>
            <ta e="T361" id="Seg_6684" s="T360">n-n:ins-n:case</ta>
            <ta e="T362" id="Seg_6685" s="T361">pers-n:case</ta>
            <ta e="T363" id="Seg_6686" s="T362">ptcl</ta>
            <ta e="T364" id="Seg_6687" s="T363">v-v:inf</ta>
            <ta e="T365" id="Seg_6688" s="T364">n.[n:case]-n:poss</ta>
            <ta e="T366" id="Seg_6689" s="T365">emphpro</ta>
            <ta e="T367" id="Seg_6690" s="T366">v-v&gt;adv</ta>
            <ta e="T368" id="Seg_6691" s="T367">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T369" id="Seg_6692" s="T368">n-n:ins-n:case</ta>
            <ta e="T370" id="Seg_6693" s="T369">ptcl</ta>
            <ta e="T371" id="Seg_6694" s="T370">conj</ta>
            <ta e="T372" id="Seg_6695" s="T371">v-v:ins-v:pn</ta>
            <ta e="T373" id="Seg_6696" s="T372">n-n:ins-n:case</ta>
            <ta e="T374" id="Seg_6697" s="T373">n-n:num-n:case</ta>
            <ta e="T375" id="Seg_6698" s="T374">v-v:ins-v:pn</ta>
            <ta e="T376" id="Seg_6699" s="T375">v-v:ins-v:pn</ta>
            <ta e="T377" id="Seg_6700" s="T376">num</ta>
            <ta e="T378" id="Seg_6701" s="T377">n.[n:case]</ta>
            <ta e="T379" id="Seg_6702" s="T378">n-n:num-n:case</ta>
            <ta e="T380" id="Seg_6703" s="T379">quant-adj&gt;adv</ta>
            <ta e="T381" id="Seg_6704" s="T380">v-v:ins-v:pn</ta>
            <ta e="T382" id="Seg_6705" s="T381">n-n:obl.poss-n:case</ta>
            <ta e="T383" id="Seg_6706" s="T382">v-v:pn</ta>
            <ta e="T384" id="Seg_6707" s="T383">dem-n.[n:case]</ta>
            <ta e="T385" id="Seg_6708" s="T384">n-n:num.[n:case]</ta>
            <ta e="T386" id="Seg_6709" s="T385">v-v:pn</ta>
            <ta e="T387" id="Seg_6710" s="T386">dem-n-n&gt;adj</ta>
            <ta e="T388" id="Seg_6711" s="T387">n.[n:case]</ta>
            <ta e="T389" id="Seg_6712" s="T388">adj</ta>
            <ta e="T390" id="Seg_6713" s="T389">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T391" id="Seg_6714" s="T390">n.[n:case]-n:poss</ta>
            <ta e="T392" id="Seg_6715" s="T391">v-v:pn</ta>
            <ta e="T393" id="Seg_6716" s="T392">n-n&gt;adj-n-v&gt;v-v:mood.pn</ta>
            <ta e="T394" id="Seg_6717" s="T393">ptcl</ta>
            <ta e="T395" id="Seg_6718" s="T394">conj</ta>
            <ta e="T396" id="Seg_6719" s="T395">n-n&gt;adj-n-n&gt;v-v&gt;adv</ta>
            <ta e="T397" id="Seg_6720" s="T396">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T398" id="Seg_6721" s="T397">n.[n:case]-n:poss</ta>
            <ta e="T399" id="Seg_6722" s="T398">adv-adv:case</ta>
            <ta e="T400" id="Seg_6723" s="T399">n.[n:case]-n:poss</ta>
            <ta e="T401" id="Seg_6724" s="T400">n-n:case</ta>
            <ta e="T402" id="Seg_6725" s="T401">v-v:ins-v:pn</ta>
            <ta e="T403" id="Seg_6726" s="T402">n.[n:case]-n:poss</ta>
            <ta e="T404" id="Seg_6727" s="T403">adv-adv:case</ta>
            <ta e="T405" id="Seg_6728" s="T404">v-n:ins-v:pn</ta>
            <ta e="T406" id="Seg_6729" s="T405">v-v&gt;n-n:ins-n:case</ta>
            <ta e="T407" id="Seg_6730" s="T406">v-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T408" id="Seg_6731" s="T407">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T409" id="Seg_6732" s="T408">n.[n:case]-n:poss</ta>
            <ta e="T410" id="Seg_6733" s="T409">n-n:case</ta>
            <ta e="T411" id="Seg_6734" s="T410">n-n&gt;v-v&gt;v-v:pn</ta>
            <ta e="T412" id="Seg_6735" s="T411">conj</ta>
            <ta e="T413" id="Seg_6736" s="T412">n.[n:case]</ta>
            <ta e="T414" id="Seg_6737" s="T413">adv-adv:case</ta>
            <ta e="T415" id="Seg_6738" s="T414">v-v&gt;v-v:pn</ta>
            <ta e="T416" id="Seg_6739" s="T415">conj</ta>
            <ta e="T417" id="Seg_6740" s="T416">v-v&gt;v-v:pn</ta>
            <ta e="T418" id="Seg_6741" s="T417">conj</ta>
            <ta e="T419" id="Seg_6742" s="T418">emphpro</ta>
            <ta e="T420" id="Seg_6743" s="T419">v-v&gt;ptcp</ta>
            <ta e="T421" id="Seg_6744" s="T420">n-n:ins-n:case</ta>
            <ta e="T422" id="Seg_6745" s="T421">n.[n:case]</ta>
            <ta e="T423" id="Seg_6746" s="T422">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T424" id="Seg_6747" s="T423">emphpro</ta>
            <ta e="T425" id="Seg_6748" s="T424">n-n:case</ta>
            <ta e="T426" id="Seg_6749" s="T425">n-n:case</ta>
            <ta e="T427" id="Seg_6750" s="T426">v-v&gt;adv</ta>
            <ta e="T428" id="Seg_6751" s="T427">v-v:ins-v&gt;v-v:ins-v:pn</ta>
            <ta e="T429" id="Seg_6752" s="T428">n-n&gt;adj</ta>
            <ta e="T430" id="Seg_6753" s="T429">n.[n:case]-n:ins-n:poss</ta>
            <ta e="T431" id="Seg_6754" s="T430">adj-adj&gt;v-v&gt;v.[v:pn]</ta>
            <ta e="T432" id="Seg_6755" s="T431">n.[n:case]-n:poss</ta>
            <ta e="T433" id="Seg_6756" s="T432">n-n:case</ta>
            <ta e="T434" id="Seg_6757" s="T433">v-v:tense.[v:pn]</ta>
            <ta e="T435" id="Seg_6758" s="T434">v-v:tense.[v:pn]</ta>
            <ta e="T436" id="Seg_6759" s="T435">v-v:pn</ta>
            <ta e="T437" id="Seg_6760" s="T436">conj</ta>
            <ta e="T438" id="Seg_6761" s="T437">v-v:pn</ta>
            <ta e="T439" id="Seg_6762" s="T438">pers</ta>
            <ta e="T440" id="Seg_6763" s="T439">n-n:num-n:poss-n:ins-n:case</ta>
            <ta e="T441" id="Seg_6764" s="T440">v-v:mood-v:pn</ta>
            <ta e="T442" id="Seg_6765" s="T441">n-n:num.[n:case]</ta>
            <ta e="T443" id="Seg_6766" s="T442">ptcl</ta>
            <ta e="T444" id="Seg_6767" s="T443">pers</ta>
            <ta e="T445" id="Seg_6768" s="T444">v-v:mood-v:pn</ta>
            <ta e="T446" id="Seg_6769" s="T445">v-v:mood-v:pn</ta>
            <ta e="T447" id="Seg_6770" s="T446">ptcl</ta>
            <ta e="T448" id="Seg_6771" s="T447">adv-adj&gt;adv</ta>
            <ta e="T449" id="Seg_6772" s="T448">conj</ta>
            <ta e="T450" id="Seg_6773" s="T449">v-v:ins-v:pn</ta>
            <ta e="T451" id="Seg_6774" s="T450">n-n:num-n:case-n:poss</ta>
            <ta e="T452" id="Seg_6775" s="T451">v-v&gt;adv</ta>
            <ta e="T453" id="Seg_6776" s="T452">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T454" id="Seg_6777" s="T453">interrog-clit.[n:case]</ta>
            <ta e="T455" id="Seg_6778" s="T454">n-n:case</ta>
            <ta e="T456" id="Seg_6779" s="T455">n-adv:case</ta>
            <ta e="T457" id="Seg_6780" s="T456">v-v:pn</ta>
            <ta e="T458" id="Seg_6781" s="T457">pers.[n:case]</ta>
            <ta e="T459" id="Seg_6782" s="T458">adj-adj&gt;adv</ta>
            <ta e="T460" id="Seg_6783" s="T459">v-v&gt;v-v:pn</ta>
            <ta e="T461" id="Seg_6784" s="T460">emphpro</ta>
            <ta e="T462" id="Seg_6785" s="T461">v-v:pn</ta>
            <ta e="T463" id="Seg_6786" s="T462">interrog.[n:case]</ta>
            <ta e="T464" id="Seg_6787" s="T463">adv-adv:case</ta>
            <ta e="T465" id="Seg_6788" s="T464">v-v:mood.[v:pn]</ta>
            <ta e="T466" id="Seg_6789" s="T465">pers.[n:case]</ta>
            <ta e="T467" id="Seg_6790" s="T466">adv</ta>
            <ta e="T468" id="Seg_6791" s="T467">num-num&gt;adj</ta>
            <ta e="T469" id="Seg_6792" s="T468">v-v:pn</ta>
            <ta e="T470" id="Seg_6793" s="T469">n-n:num.[n:case]</ta>
            <ta e="T471" id="Seg_6794" s="T470">v-v&gt;v-v:mood.[v:pn]</ta>
            <ta e="T472" id="Seg_6795" s="T471">conj</ta>
            <ta e="T473" id="Seg_6796" s="T472">n-n:case</ta>
            <ta e="T474" id="Seg_6797" s="T473">n-adv:case</ta>
            <ta e="T475" id="Seg_6798" s="T474">adj-adj&gt;adv</ta>
            <ta e="T476" id="Seg_6799" s="T475">v-v:ins-v:pn</ta>
            <ta e="T477" id="Seg_6800" s="T476">interrog-clit.[n:case]</ta>
            <ta e="T478" id="Seg_6801" s="T477">n.[n:case]-n:poss</ta>
            <ta e="T479" id="Seg_6802" s="T478">adv-adv:case</ta>
            <ta e="T480" id="Seg_6803" s="T479">n-n:ins-n:case</ta>
            <ta e="T481" id="Seg_6804" s="T480">adv</ta>
            <ta e="T482" id="Seg_6805" s="T481">v-v:ins-v:pn</ta>
            <ta e="T483" id="Seg_6806" s="T482">n-n:case</ta>
            <ta e="T484" id="Seg_6807" s="T483">n-adv:case</ta>
            <ta e="T485" id="Seg_6808" s="T484">v-v:ins-v&gt;v-v:pn</ta>
            <ta e="T486" id="Seg_6809" s="T485">v-v:ins-v:pn</ta>
            <ta e="T487" id="Seg_6810" s="T486">n-n:case</ta>
            <ta e="T488" id="Seg_6811" s="T487">v-v:pn</ta>
            <ta e="T489" id="Seg_6812" s="T488">dem</ta>
            <ta e="T490" id="Seg_6813" s="T489">n-n:case</ta>
            <ta e="T491" id="Seg_6814" s="T490">v-v:pn</ta>
            <ta e="T492" id="Seg_6815" s="T491">conj</ta>
            <ta e="T493" id="Seg_6816" s="T492">v-v&gt;v-v:pn</ta>
            <ta e="T494" id="Seg_6817" s="T493">interrog-clit.[n:case]</ta>
            <ta e="T495" id="Seg_6818" s="T494">num</ta>
            <ta e="T496" id="Seg_6819" s="T495">n-n:num-n&gt;n.[n:case]</ta>
            <ta e="T497" id="Seg_6820" s="T496">dem</ta>
            <ta e="T498" id="Seg_6821" s="T497">interj</ta>
            <ta e="T499" id="Seg_6822" s="T498">pers-n:case-n&gt;adj</ta>
            <ta e="T500" id="Seg_6823" s="T499">n-n:num.[n:case]</ta>
            <ta e="T501" id="Seg_6824" s="T500">adj</ta>
            <ta e="T502" id="Seg_6825" s="T501">n-n:ins-n:case</ta>
            <ta e="T503" id="Seg_6826" s="T502">v-v:ins-v:pn</ta>
            <ta e="T504" id="Seg_6827" s="T503">n.[n:case]-n:poss</ta>
            <ta e="T505" id="Seg_6828" s="T504">n-n:case</ta>
            <ta e="T506" id="Seg_6829" s="T505">v-v&gt;v-v:pn</ta>
            <ta e="T507" id="Seg_6830" s="T506">n-n:num.[n:case]-n:poss</ta>
            <ta e="T508" id="Seg_6831" s="T507">num</ta>
            <ta e="T509" id="Seg_6832" s="T508">n.[n:case]</ta>
            <ta e="T510" id="Seg_6833" s="T509">adv</ta>
            <ta e="T511" id="Seg_6834" s="T510">n-n:case</ta>
            <ta e="T512" id="Seg_6835" s="T511">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T513" id="Seg_6836" s="T512">adv</ta>
            <ta e="T514" id="Seg_6837" s="T513">v-v:pn</ta>
            <ta e="T515" id="Seg_6838" s="T514">n-n:num-n:case.poss</ta>
            <ta e="T516" id="Seg_6839" s="T515">v-v:pn</ta>
            <ta e="T517" id="Seg_6840" s="T516">n-n:num.[n:case]</ta>
            <ta e="T518" id="Seg_6841" s="T517">v-v&gt;v-v:mood.[v:pn]</ta>
            <ta e="T519" id="Seg_6842" s="T518">n-n:num.[n:case]-n:poss</ta>
            <ta e="T520" id="Seg_6843" s="T519">pers-n:ins-n:case</ta>
            <ta e="T521" id="Seg_6844" s="T520">v-v:pn</ta>
            <ta e="T522" id="Seg_6845" s="T521">v-v&gt;v-v:pn</ta>
            <ta e="T523" id="Seg_6846" s="T522">conj</ta>
            <ta e="T524" id="Seg_6847" s="T523">n-n:num.[n:case]-n:poss</ta>
            <ta e="T525" id="Seg_6848" s="T524">adj-adj&gt;adv</ta>
            <ta e="T526" id="Seg_6849" s="T525">ptcl</ta>
            <ta e="T527" id="Seg_6850" s="T526">v-v:pn</ta>
            <ta e="T528" id="Seg_6851" s="T527">n-n:num-n:case-n:poss</ta>
            <ta e="T529" id="Seg_6852" s="T528">v-v:ins-v:pn</ta>
            <ta e="T530" id="Seg_6853" s="T529">adv</ta>
            <ta e="T531" id="Seg_6854" s="T530">v-v&gt;v-v:pn</ta>
            <ta e="T532" id="Seg_6855" s="T531">adj-adj&gt;adv</ta>
            <ta e="T533" id="Seg_6856" s="T532">n-n:num-n:case-n:poss</ta>
            <ta e="T534" id="Seg_6857" s="T533">adv</ta>
            <ta e="T535" id="Seg_6858" s="T534">adj-adj&gt;adv</ta>
            <ta e="T536" id="Seg_6859" s="T535">v-v:ins-v:pn</ta>
            <ta e="T537" id="Seg_6860" s="T536">v-v:pn</ta>
            <ta e="T538" id="Seg_6861" s="T537">n-n:num.[n:case]</ta>
            <ta e="T539" id="Seg_6862" s="T538">v-v&gt;v-v:mood.[v:pn]</ta>
            <ta e="T540" id="Seg_6863" s="T539">n-n:num.[n:case]-n:poss</ta>
            <ta e="T541" id="Seg_6864" s="T540">v-v:pn</ta>
            <ta e="T542" id="Seg_6865" s="T541">adv</ta>
            <ta e="T543" id="Seg_6866" s="T542">n-n.[n:case]</ta>
            <ta e="T544" id="Seg_6867" s="T543">v-v:ins-v:pn</ta>
            <ta e="T545" id="Seg_6868" s="T544">adv</ta>
            <ta e="T546" id="Seg_6869" s="T545">n-n:num.[n:case]-n:poss</ta>
            <ta e="T547" id="Seg_6870" s="T546">adj-adj&gt;adv</ta>
            <ta e="T548" id="Seg_6871" s="T547">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T549" id="Seg_6872" s="T548">adv</ta>
            <ta e="T550" id="Seg_6873" s="T549">pers</ta>
            <ta e="T551" id="Seg_6874" s="T550">n-n:case</ta>
            <ta e="T552" id="Seg_6875" s="T551">v-v:pn</ta>
            <ta e="T553" id="Seg_6876" s="T552">adv</ta>
            <ta e="T554" id="Seg_6877" s="T553">pers</ta>
            <ta e="T555" id="Seg_6878" s="T554">pers</ta>
            <ta e="T556" id="Seg_6879" s="T555">n-n:num-n:case</ta>
            <ta e="T557" id="Seg_6880" s="T556">pp</ta>
            <ta e="T558" id="Seg_6881" s="T557">v-v:tense-v:pn-v:tense</ta>
            <ta e="T559" id="Seg_6882" s="T558">adv</ta>
            <ta e="T560" id="Seg_6883" s="T559">v-v:pn</ta>
            <ta e="T561" id="Seg_6884" s="T560">conj</ta>
            <ta e="T562" id="Seg_6885" s="T561">n.[n:case]-n:poss</ta>
            <ta e="T563" id="Seg_6886" s="T562">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T564" id="Seg_6887" s="T563">n.[n:case]</ta>
            <ta e="T565" id="Seg_6888" s="T564">n-n:case</ta>
            <ta e="T566" id="Seg_6889" s="T565">v-v:ins-v&gt;v-v:ins-v:pn</ta>
            <ta e="T567" id="Seg_6890" s="T566">n.[n:case]-n:poss</ta>
            <ta e="T568" id="Seg_6891" s="T567">v-v:pn</ta>
            <ta e="T569" id="Seg_6892" s="T568">n-n:obl.poss-n:case</ta>
            <ta e="T570" id="Seg_6893" s="T569">conj</ta>
            <ta e="T571" id="Seg_6894" s="T570">pers</ta>
            <ta e="T572" id="Seg_6895" s="T571">pers</ta>
            <ta e="T573" id="Seg_6896" s="T572">interrog-n:case</ta>
            <ta e="T574" id="Seg_6897" s="T573">v-v:pn</ta>
            <ta e="T575" id="Seg_6898" s="T574">conj</ta>
            <ta e="T576" id="Seg_6899" s="T575">n.[n:case]-n:poss</ta>
            <ta e="T577" id="Seg_6900" s="T576">v-v:pn</ta>
            <ta e="T578" id="Seg_6901" s="T577">conj</ta>
            <ta e="T579" id="Seg_6902" s="T578">pers</ta>
            <ta e="T580" id="Seg_6903" s="T579">interrog-n:case</ta>
            <ta e="T581" id="Seg_6904" s="T580">adv-adj&gt;adv</ta>
            <ta e="T582" id="Seg_6905" s="T581">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T583" id="Seg_6906" s="T582">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T584" id="Seg_6907" s="T583">n-n&gt;v-v:inf</ta>
            <ta e="T585" id="Seg_6908" s="T584">conj</ta>
            <ta e="T586" id="Seg_6909" s="T585">emphpro</ta>
            <ta e="T587" id="Seg_6910" s="T586">n-n:num-n:obl.poss-n:case</ta>
            <ta e="T588" id="Seg_6911" s="T587">v-v:tense-v:pn</ta>
            <ta e="T589" id="Seg_6912" s="T588">conj</ta>
            <ta e="T590" id="Seg_6913" s="T589">n-n:case</ta>
            <ta e="T591" id="Seg_6914" s="T590">interrog-n:case-clit</ta>
            <ta e="T592" id="Seg_6915" s="T591">ptcl</ta>
            <ta e="T593" id="Seg_6916" s="T592">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T594" id="Seg_6917" s="T593">interj</ta>
            <ta e="T595" id="Seg_6918" s="T594">pers</ta>
            <ta e="T596" id="Seg_6919" s="T595">pers-n:case-n&gt;adj</ta>
            <ta e="T597" id="Seg_6920" s="T596">n-n:num-n:case-n:poss</ta>
            <ta e="T598" id="Seg_6921" s="T597">conj</ta>
            <ta e="T599" id="Seg_6922" s="T598">conj</ta>
            <ta e="T600" id="Seg_6923" s="T599">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T601" id="Seg_6924" s="T600">v-v:ins-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_6925" s="T1">v</ta>
            <ta e="T3" id="Seg_6926" s="T2">v</ta>
            <ta e="T4" id="Seg_6927" s="T3">n</ta>
            <ta e="T5" id="Seg_6928" s="T4">n</ta>
            <ta e="T6" id="Seg_6929" s="T5">n</ta>
            <ta e="T7" id="Seg_6930" s="T6">v</ta>
            <ta e="T8" id="Seg_6931" s="T7">adv</ta>
            <ta e="T9" id="Seg_6932" s="T8">n</ta>
            <ta e="T10" id="Seg_6933" s="T9">n</ta>
            <ta e="T11" id="Seg_6934" s="T10">v</ta>
            <ta e="T12" id="Seg_6935" s="T11">n</ta>
            <ta e="T13" id="Seg_6936" s="T12">v</ta>
            <ta e="T14" id="Seg_6937" s="T13">v</ta>
            <ta e="T15" id="Seg_6938" s="T14">n</ta>
            <ta e="T16" id="Seg_6939" s="T15">v</ta>
            <ta e="T17" id="Seg_6940" s="T16">pro</ta>
            <ta e="T18" id="Seg_6941" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_6942" s="T18">v</ta>
            <ta e="T20" id="Seg_6943" s="T19">adv</ta>
            <ta e="T21" id="Seg_6944" s="T20">adv</ta>
            <ta e="T22" id="Seg_6945" s="T21">v</ta>
            <ta e="T23" id="Seg_6946" s="T22">pro</ta>
            <ta e="T24" id="Seg_6947" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_6948" s="T24">v</ta>
            <ta e="T26" id="Seg_6949" s="T25">num</ta>
            <ta e="T27" id="Seg_6950" s="T26">n</ta>
            <ta e="T28" id="Seg_6951" s="T27">n</ta>
            <ta e="T29" id="Seg_6952" s="T28">v</ta>
            <ta e="T30" id="Seg_6953" s="T29">interrog</ta>
            <ta e="T31" id="Seg_6954" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_6955" s="T31">pers</ta>
            <ta e="T33" id="Seg_6956" s="T32">pro</ta>
            <ta e="T34" id="Seg_6957" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_6958" s="T34">v</ta>
            <ta e="T36" id="Seg_6959" s="T35">pers</ta>
            <ta e="T37" id="Seg_6960" s="T36">pers</ta>
            <ta e="T38" id="Seg_6961" s="T37">v</ta>
            <ta e="T39" id="Seg_6962" s="T38">interrog</ta>
            <ta e="T40" id="Seg_6963" s="T39">pers</ta>
            <ta e="T41" id="Seg_6964" s="T40">v</ta>
            <ta e="T42" id="Seg_6965" s="T41">v</ta>
            <ta e="T43" id="Seg_6966" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_6967" s="T43">conj</ta>
            <ta e="T45" id="Seg_6968" s="T44">adv</ta>
            <ta e="T46" id="Seg_6969" s="T45">v</ta>
            <ta e="T47" id="Seg_6970" s="T46">n</ta>
            <ta e="T48" id="Seg_6971" s="T47">adv</ta>
            <ta e="T49" id="Seg_6972" s="T48">v</ta>
            <ta e="T50" id="Seg_6973" s="T49">adv</ta>
            <ta e="T51" id="Seg_6974" s="T50">v</ta>
            <ta e="T52" id="Seg_6975" s="T51">n</ta>
            <ta e="T53" id="Seg_6976" s="T52">adv</ta>
            <ta e="T54" id="Seg_6977" s="T53">n</ta>
            <ta e="T55" id="Seg_6978" s="T54">v</ta>
            <ta e="T56" id="Seg_6979" s="T55">n</ta>
            <ta e="T57" id="Seg_6980" s="T56">pp</ta>
            <ta e="T58" id="Seg_6981" s="T57">adv</ta>
            <ta e="T59" id="Seg_6982" s="T58">v</ta>
            <ta e="T60" id="Seg_6983" s="T59">n</ta>
            <ta e="T61" id="Seg_6984" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_6985" s="T61">v</ta>
            <ta e="T63" id="Seg_6986" s="T62">conj</ta>
            <ta e="T64" id="Seg_6987" s="T63">ptcl</ta>
            <ta e="T65" id="Seg_6988" s="T64">v</ta>
            <ta e="T66" id="Seg_6989" s="T65">pers</ta>
            <ta e="T67" id="Seg_6990" s="T66">n</ta>
            <ta e="T68" id="Seg_6991" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_6992" s="T68">v</ta>
            <ta e="T70" id="Seg_6993" s="T69">n</ta>
            <ta e="T71" id="Seg_6994" s="T70">v</ta>
            <ta e="T72" id="Seg_6995" s="T71">nprop</ta>
            <ta e="T73" id="Seg_6996" s="T72">n</ta>
            <ta e="T74" id="Seg_6997" s="T73">dem</ta>
            <ta e="T75" id="Seg_6998" s="T74">adj</ta>
            <ta e="T76" id="Seg_6999" s="T75">n</ta>
            <ta e="T77" id="Seg_7000" s="T76">n</ta>
            <ta e="T78" id="Seg_7001" s="T77">emphpro</ta>
            <ta e="T79" id="Seg_7002" s="T78">n</ta>
            <ta e="T80" id="Seg_7003" s="T79">v</ta>
            <ta e="T81" id="Seg_7004" s="T80">n</ta>
            <ta e="T82" id="Seg_7005" s="T81">adv</ta>
            <ta e="T83" id="Seg_7006" s="T82">v</ta>
            <ta e="T84" id="Seg_7007" s="T83">n</ta>
            <ta e="T85" id="Seg_7008" s="T84">n</ta>
            <ta e="T86" id="Seg_7009" s="T85">dem</ta>
            <ta e="T87" id="Seg_7010" s="T86">adj</ta>
            <ta e="T88" id="Seg_7011" s="T87">n</ta>
            <ta e="T89" id="Seg_7012" s="T88">v</ta>
            <ta e="T90" id="Seg_7013" s="T89">emphpro</ta>
            <ta e="T91" id="Seg_7014" s="T90">v</ta>
            <ta e="T92" id="Seg_7015" s="T91">n</ta>
            <ta e="T93" id="Seg_7016" s="T92">v</ta>
            <ta e="T94" id="Seg_7017" s="T93">n</ta>
            <ta e="T95" id="Seg_7018" s="T94">pers</ta>
            <ta e="T96" id="Seg_7019" s="T95">v</ta>
            <ta e="T97" id="Seg_7020" s="T96">v</ta>
            <ta e="T98" id="Seg_7021" s="T97">adv</ta>
            <ta e="T99" id="Seg_7022" s="T98">adj</ta>
            <ta e="T100" id="Seg_7023" s="T99">n</ta>
            <ta e="T101" id="Seg_7024" s="T100">n</ta>
            <ta e="T102" id="Seg_7025" s="T101">n</ta>
            <ta e="T103" id="Seg_7026" s="T102">adv</ta>
            <ta e="T104" id="Seg_7027" s="T103">v</ta>
            <ta e="T105" id="Seg_7028" s="T104">n</ta>
            <ta e="T106" id="Seg_7029" s="T105">n</ta>
            <ta e="T107" id="Seg_7030" s="T106">v</ta>
            <ta e="T108" id="Seg_7031" s="T107">v</ta>
            <ta e="T109" id="Seg_7032" s="T108">n</ta>
            <ta e="T110" id="Seg_7033" s="T109">v</ta>
            <ta e="T111" id="Seg_7034" s="T110">n</ta>
            <ta e="T112" id="Seg_7035" s="T111">pers</ta>
            <ta e="T113" id="Seg_7036" s="T112">adv</ta>
            <ta e="T114" id="Seg_7037" s="T113">v</ta>
            <ta e="T116" id="Seg_7038" s="T114">adv</ta>
            <ta e="T117" id="Seg_7039" s="T116">adj</ta>
            <ta e="T118" id="Seg_7040" s="T117">n</ta>
            <ta e="T120" id="Seg_7041" s="T119">n</ta>
            <ta e="T121" id="Seg_7042" s="T120">v</ta>
            <ta e="T122" id="Seg_7043" s="T121">v</ta>
            <ta e="T123" id="Seg_7044" s="T122">v</ta>
            <ta e="T124" id="Seg_7045" s="T123">n</ta>
            <ta e="T125" id="Seg_7046" s="T124">n</ta>
            <ta e="T126" id="Seg_7047" s="T125">v</ta>
            <ta e="T127" id="Seg_7048" s="T126">dem</ta>
            <ta e="T128" id="Seg_7049" s="T127">adj</ta>
            <ta e="T129" id="Seg_7050" s="T128">n</ta>
            <ta e="T130" id="Seg_7051" s="T129">n</ta>
            <ta e="T131" id="Seg_7052" s="T130">v</ta>
            <ta e="T132" id="Seg_7053" s="T131">conj</ta>
            <ta e="T133" id="Seg_7054" s="T132">n</ta>
            <ta e="T134" id="Seg_7055" s="T133">adv</ta>
            <ta e="T135" id="Seg_7056" s="T134">pers</ta>
            <ta e="T136" id="Seg_7057" s="T135">n</ta>
            <ta e="T137" id="Seg_7058" s="T136">v</ta>
            <ta e="T138" id="Seg_7059" s="T137">v</ta>
            <ta e="T139" id="Seg_7060" s="T138">emphpro</ta>
            <ta e="T140" id="Seg_7061" s="T139">v</ta>
            <ta e="T141" id="Seg_7062" s="T140">interj</ta>
            <ta e="T142" id="Seg_7063" s="T141">pers</ta>
            <ta e="T143" id="Seg_7064" s="T142">conj</ta>
            <ta e="T144" id="Seg_7065" s="T143">n</ta>
            <ta e="T145" id="Seg_7066" s="T144">pro</ta>
            <ta e="T146" id="Seg_7067" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_7068" s="T146">v</ta>
            <ta e="T148" id="Seg_7069" s="T147">pers</ta>
            <ta e="T149" id="Seg_7070" s="T148">adv</ta>
            <ta e="T150" id="Seg_7071" s="T149">n</ta>
            <ta e="T151" id="Seg_7072" s="T150">v</ta>
            <ta e="T152" id="Seg_7073" s="T151">v</ta>
            <ta e="T153" id="Seg_7074" s="T152">conj</ta>
            <ta e="T154" id="Seg_7075" s="T153">adv</ta>
            <ta e="T155" id="Seg_7076" s="T154">v</ta>
            <ta e="T156" id="Seg_7077" s="T155">n</ta>
            <ta e="T157" id="Seg_7078" s="T156">n</ta>
            <ta e="T158" id="Seg_7079" s="T157">adv</ta>
            <ta e="T159" id="Seg_7080" s="T158">v</ta>
            <ta e="T160" id="Seg_7081" s="T159">v</ta>
            <ta e="T161" id="Seg_7082" s="T160">pers</ta>
            <ta e="T162" id="Seg_7083" s="T161">pers</ta>
            <ta e="T163" id="Seg_7084" s="T162">v</ta>
            <ta e="T164" id="Seg_7085" s="T163">conj</ta>
            <ta e="T165" id="Seg_7086" s="T164">conj</ta>
            <ta e="T166" id="Seg_7087" s="T165">adv</ta>
            <ta e="T167" id="Seg_7088" s="T166">v</ta>
            <ta e="T168" id="Seg_7089" s="T167">n</ta>
            <ta e="T169" id="Seg_7090" s="T168">interrog</ta>
            <ta e="T170" id="Seg_7091" s="T169">v</ta>
            <ta e="T171" id="Seg_7092" s="T170">adv</ta>
            <ta e="T172" id="Seg_7093" s="T171">adv</ta>
            <ta e="T173" id="Seg_7094" s="T172">v</ta>
            <ta e="T174" id="Seg_7095" s="T173">n</ta>
            <ta e="T175" id="Seg_7096" s="T174">v</ta>
            <ta e="T176" id="Seg_7097" s="T175">conj</ta>
            <ta e="T177" id="Seg_7098" s="T176">ptcl</ta>
            <ta e="T178" id="Seg_7099" s="T177">n</ta>
            <ta e="T179" id="Seg_7100" s="T178">adv</ta>
            <ta e="T180" id="Seg_7101" s="T179">n</ta>
            <ta e="T181" id="Seg_7102" s="T180">adv</ta>
            <ta e="T182" id="Seg_7103" s="T181">v</ta>
            <ta e="T183" id="Seg_7104" s="T182">n</ta>
            <ta e="T184" id="Seg_7105" s="T183">v</ta>
            <ta e="T185" id="Seg_7106" s="T184">n</ta>
            <ta e="T186" id="Seg_7107" s="T185">v</ta>
            <ta e="T187" id="Seg_7108" s="T186">ptcl</ta>
            <ta e="T188" id="Seg_7109" s="T187">v</ta>
            <ta e="T189" id="Seg_7110" s="T188">pers</ta>
            <ta e="T190" id="Seg_7111" s="T189">conj</ta>
            <ta e="T191" id="Seg_7112" s="T190">ptcl</ta>
            <ta e="T192" id="Seg_7113" s="T191">v</ta>
            <ta e="T193" id="Seg_7114" s="T192">interrog</ta>
            <ta e="T194" id="Seg_7115" s="T193">n</ta>
            <ta e="T195" id="Seg_7116" s="T194">v</ta>
            <ta e="T196" id="Seg_7117" s="T195">v</ta>
            <ta e="T197" id="Seg_7118" s="T196">n</ta>
            <ta e="T198" id="Seg_7119" s="T197">interrog</ta>
            <ta e="T199" id="Seg_7120" s="T198">v</ta>
            <ta e="T200" id="Seg_7121" s="T199">adv</ta>
            <ta e="T201" id="Seg_7122" s="T200">conj</ta>
            <ta e="T202" id="Seg_7123" s="T201">v</ta>
            <ta e="T203" id="Seg_7124" s="T202">adv</ta>
            <ta e="T204" id="Seg_7125" s="T203">adv</ta>
            <ta e="T205" id="Seg_7126" s="T204">n</ta>
            <ta e="T206" id="Seg_7127" s="T205">ptcl</ta>
            <ta e="T207" id="Seg_7128" s="T206">v</ta>
            <ta e="T208" id="Seg_7129" s="T207">adv</ta>
            <ta e="T209" id="Seg_7130" s="T208">n</ta>
            <ta e="T210" id="Seg_7131" s="T209">v</ta>
            <ta e="T211" id="Seg_7132" s="T210">n</ta>
            <ta e="T212" id="Seg_7133" s="T211">ptcl</ta>
            <ta e="T213" id="Seg_7134" s="T212">v</ta>
            <ta e="T214" id="Seg_7135" s="T213">n</ta>
            <ta e="T215" id="Seg_7136" s="T214">adv</ta>
            <ta e="T216" id="Seg_7137" s="T215">adv</ta>
            <ta e="T217" id="Seg_7138" s="T216">v</ta>
            <ta e="T218" id="Seg_7139" s="T217">n</ta>
            <ta e="T219" id="Seg_7140" s="T218">n</ta>
            <ta e="T220" id="Seg_7141" s="T219">adv</ta>
            <ta e="T221" id="Seg_7142" s="T220">v</ta>
            <ta e="T222" id="Seg_7143" s="T221">n</ta>
            <ta e="T223" id="Seg_7144" s="T222">v</ta>
            <ta e="T224" id="Seg_7145" s="T223">adv</ta>
            <ta e="T225" id="Seg_7146" s="T224">v</ta>
            <ta e="T226" id="Seg_7147" s="T225">n</ta>
            <ta e="T0" id="Seg_7148" s="T226">adv</ta>
            <ta e="T227" id="Seg_7149" s="T0">preverb</ta>
            <ta e="T228" id="Seg_7150" s="T227">v</ta>
            <ta e="T229" id="Seg_7151" s="T228">n</ta>
            <ta e="T230" id="Seg_7152" s="T229">v</ta>
            <ta e="T231" id="Seg_7153" s="T230">v</ta>
            <ta e="T232" id="Seg_7154" s="T231">n</ta>
            <ta e="T233" id="Seg_7155" s="T232">n</ta>
            <ta e="T234" id="Seg_7156" s="T233">n</ta>
            <ta e="T235" id="Seg_7157" s="T234">v</ta>
            <ta e="T236" id="Seg_7158" s="T235">n</ta>
            <ta e="T237" id="Seg_7159" s="T236">n</ta>
            <ta e="T238" id="Seg_7160" s="T237">v</ta>
            <ta e="T239" id="Seg_7161" s="T238">n</ta>
            <ta e="T240" id="Seg_7162" s="T239">adv</ta>
            <ta e="T241" id="Seg_7163" s="T240">v</ta>
            <ta e="T242" id="Seg_7164" s="T241">n</ta>
            <ta e="T243" id="Seg_7165" s="T242">v</ta>
            <ta e="T244" id="Seg_7166" s="T243">n</ta>
            <ta e="T245" id="Seg_7167" s="T244">v</ta>
            <ta e="T246" id="Seg_7168" s="T245">conj</ta>
            <ta e="T247" id="Seg_7169" s="T246">pers</ta>
            <ta e="T248" id="Seg_7170" s="T247">interrog</ta>
            <ta e="T249" id="Seg_7171" s="T248">n</ta>
            <ta e="T250" id="Seg_7172" s="T249">adv</ta>
            <ta e="T251" id="Seg_7173" s="T250">v</ta>
            <ta e="T252" id="Seg_7174" s="T251">n</ta>
            <ta e="T253" id="Seg_7175" s="T252">v</ta>
            <ta e="T254" id="Seg_7176" s="T253">pers</ta>
            <ta e="T255" id="Seg_7177" s="T254">n</ta>
            <ta e="T256" id="Seg_7178" s="T255">n</ta>
            <ta e="T257" id="Seg_7179" s="T256">v</ta>
            <ta e="T258" id="Seg_7180" s="T257">conj</ta>
            <ta e="T259" id="Seg_7181" s="T258">adv</ta>
            <ta e="T260" id="Seg_7182" s="T259">v</ta>
            <ta e="T261" id="Seg_7183" s="T260">n</ta>
            <ta e="T262" id="Seg_7184" s="T261">v</ta>
            <ta e="T263" id="Seg_7185" s="T262">conj</ta>
            <ta e="T264" id="Seg_7186" s="T263">pers</ta>
            <ta e="T265" id="Seg_7187" s="T264">v</ta>
            <ta e="T266" id="Seg_7188" s="T265">pers</ta>
            <ta e="T267" id="Seg_7189" s="T266">adv</ta>
            <ta e="T268" id="Seg_7190" s="T267">pers</ta>
            <ta e="T269" id="Seg_7191" s="T268">v</ta>
            <ta e="T270" id="Seg_7192" s="T269">conj</ta>
            <ta e="T271" id="Seg_7193" s="T270">pers</ta>
            <ta e="T272" id="Seg_7194" s="T271">v</ta>
            <ta e="T273" id="Seg_7195" s="T272">adj</ta>
            <ta e="T274" id="Seg_7196" s="T273">n</ta>
            <ta e="T275" id="Seg_7197" s="T274">n</ta>
            <ta e="T276" id="Seg_7198" s="T275">v</ta>
            <ta e="T277" id="Seg_7199" s="T276">adj</ta>
            <ta e="T278" id="Seg_7200" s="T277">n</ta>
            <ta e="T279" id="Seg_7201" s="T278">n</ta>
            <ta e="T280" id="Seg_7202" s="T279">v</ta>
            <ta e="T281" id="Seg_7203" s="T280">conj</ta>
            <ta e="T282" id="Seg_7204" s="T281">dem</ta>
            <ta e="T283" id="Seg_7205" s="T282">adj</ta>
            <ta e="T284" id="Seg_7206" s="T283">n</ta>
            <ta e="T285" id="Seg_7207" s="T284">n</ta>
            <ta e="T286" id="Seg_7208" s="T285">v</ta>
            <ta e="T287" id="Seg_7209" s="T286">n</ta>
            <ta e="T288" id="Seg_7210" s="T287">pers</ta>
            <ta e="T289" id="Seg_7211" s="T288">adv</ta>
            <ta e="T290" id="Seg_7212" s="T289">v</ta>
            <ta e="T291" id="Seg_7213" s="T290">adv</ta>
            <ta e="T292" id="Seg_7214" s="T291">pers</ta>
            <ta e="T293" id="Seg_7215" s="T292">conj</ta>
            <ta e="T294" id="Seg_7216" s="T293">v</ta>
            <ta e="T295" id="Seg_7217" s="T294">v</ta>
            <ta e="T296" id="Seg_7218" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_7219" s="T296">conj</ta>
            <ta e="T298" id="Seg_7220" s="T297">adv</ta>
            <ta e="T299" id="Seg_7221" s="T298">v</ta>
            <ta e="T300" id="Seg_7222" s="T299">interrog</ta>
            <ta e="T301" id="Seg_7223" s="T300">ptcl</ta>
            <ta e="T302" id="Seg_7224" s="T301">n</ta>
            <ta e="T303" id="Seg_7225" s="T302">v</ta>
            <ta e="T304" id="Seg_7226" s="T303">n</ta>
            <ta e="T305" id="Seg_7227" s="T304">v</ta>
            <ta e="T306" id="Seg_7228" s="T305">n</ta>
            <ta e="T307" id="Seg_7229" s="T306">v</ta>
            <ta e="T308" id="Seg_7230" s="T307">v</ta>
            <ta e="T309" id="Seg_7231" s="T308">n</ta>
            <ta e="T310" id="Seg_7232" s="T309">n</ta>
            <ta e="T311" id="Seg_7233" s="T310">n</ta>
            <ta e="T312" id="Seg_7234" s="T311">v</ta>
            <ta e="T313" id="Seg_7235" s="T312">n</ta>
            <ta e="T314" id="Seg_7236" s="T313">v</ta>
            <ta e="T315" id="Seg_7237" s="T314">pers</ta>
            <ta e="T316" id="Seg_7238" s="T315">v</ta>
            <ta e="T317" id="Seg_7239" s="T316">pro</ta>
            <ta e="T318" id="Seg_7240" s="T317">v</ta>
            <ta e="T319" id="Seg_7241" s="T318">n</ta>
            <ta e="T320" id="Seg_7242" s="T319">conj</ta>
            <ta e="T321" id="Seg_7243" s="T320">n</ta>
            <ta e="T322" id="Seg_7244" s="T321">v</ta>
            <ta e="T323" id="Seg_7245" s="T322">pers</ta>
            <ta e="T324" id="Seg_7246" s="T323">interrog</ta>
            <ta e="T325" id="Seg_7247" s="T324">v</ta>
            <ta e="T326" id="Seg_7248" s="T325">n</ta>
            <ta e="T327" id="Seg_7249" s="T326">ptcl</ta>
            <ta e="T328" id="Seg_7250" s="T327">v</ta>
            <ta e="T329" id="Seg_7251" s="T328">n</ta>
            <ta e="T330" id="Seg_7252" s="T329">v</ta>
            <ta e="T331" id="Seg_7253" s="T330">v</ta>
            <ta e="T332" id="Seg_7254" s="T331">pers</ta>
            <ta e="T333" id="Seg_7255" s="T332">pers</ta>
            <ta e="T334" id="Seg_7256" s="T333">n</ta>
            <ta e="T335" id="Seg_7257" s="T334">pers</ta>
            <ta e="T336" id="Seg_7258" s="T335">v</ta>
            <ta e="T337" id="Seg_7259" s="T336">conj</ta>
            <ta e="T338" id="Seg_7260" s="T337">n</ta>
            <ta e="T339" id="Seg_7261" s="T338">v</ta>
            <ta e="T340" id="Seg_7262" s="T339">n</ta>
            <ta e="T341" id="Seg_7263" s="T340">n</ta>
            <ta e="T342" id="Seg_7264" s="T341">n</ta>
            <ta e="T343" id="Seg_7265" s="T342">v</ta>
            <ta e="T344" id="Seg_7266" s="T343">n</ta>
            <ta e="T345" id="Seg_7267" s="T344">ptcp</ta>
            <ta e="T346" id="Seg_7268" s="T345">n</ta>
            <ta e="T347" id="Seg_7269" s="T346">emphpro</ta>
            <ta e="T348" id="Seg_7270" s="T347">pers</ta>
            <ta e="T349" id="Seg_7271" s="T348">v</ta>
            <ta e="T350" id="Seg_7272" s="T349">conj</ta>
            <ta e="T351" id="Seg_7273" s="T350">v</ta>
            <ta e="T352" id="Seg_7274" s="T351">n</ta>
            <ta e="T353" id="Seg_7275" s="T352">n</ta>
            <ta e="T354" id="Seg_7276" s="T353">v</ta>
            <ta e="T355" id="Seg_7277" s="T354">v</ta>
            <ta e="T356" id="Seg_7278" s="T355">n</ta>
            <ta e="T357" id="Seg_7279" s="T356">n</ta>
            <ta e="T358" id="Seg_7280" s="T357">n</ta>
            <ta e="T359" id="Seg_7281" s="T358">ptcl</ta>
            <ta e="T360" id="Seg_7282" s="T359">v</ta>
            <ta e="T361" id="Seg_7283" s="T360">n</ta>
            <ta e="T362" id="Seg_7284" s="T361">pers</ta>
            <ta e="T363" id="Seg_7285" s="T362">ptcl</ta>
            <ta e="T364" id="Seg_7286" s="T363">v</ta>
            <ta e="T365" id="Seg_7287" s="T364">n</ta>
            <ta e="T366" id="Seg_7288" s="T365">emphpro</ta>
            <ta e="T367" id="Seg_7289" s="T366">adv</ta>
            <ta e="T368" id="Seg_7290" s="T367">v</ta>
            <ta e="T369" id="Seg_7291" s="T368">n</ta>
            <ta e="T370" id="Seg_7292" s="T369">ptcl</ta>
            <ta e="T371" id="Seg_7293" s="T370">conj</ta>
            <ta e="T372" id="Seg_7294" s="T371">v</ta>
            <ta e="T373" id="Seg_7295" s="T372">n</ta>
            <ta e="T374" id="Seg_7296" s="T373">n</ta>
            <ta e="T375" id="Seg_7297" s="T374">v</ta>
            <ta e="T376" id="Seg_7298" s="T375">v</ta>
            <ta e="T377" id="Seg_7299" s="T376">num</ta>
            <ta e="T378" id="Seg_7300" s="T377">n</ta>
            <ta e="T379" id="Seg_7301" s="T378">n</ta>
            <ta e="T380" id="Seg_7302" s="T379">quant</ta>
            <ta e="T381" id="Seg_7303" s="T380">v</ta>
            <ta e="T382" id="Seg_7304" s="T381">n</ta>
            <ta e="T383" id="Seg_7305" s="T382">v</ta>
            <ta e="T384" id="Seg_7306" s="T383">n</ta>
            <ta e="T385" id="Seg_7307" s="T384">n</ta>
            <ta e="T386" id="Seg_7308" s="T385">v</ta>
            <ta e="T387" id="Seg_7309" s="T386">adj</ta>
            <ta e="T388" id="Seg_7310" s="T387">n</ta>
            <ta e="T389" id="Seg_7311" s="T388">adj</ta>
            <ta e="T390" id="Seg_7312" s="T389">n</ta>
            <ta e="T391" id="Seg_7313" s="T390">n</ta>
            <ta e="T392" id="Seg_7314" s="T391">v</ta>
            <ta e="T393" id="Seg_7315" s="T392">v</ta>
            <ta e="T394" id="Seg_7316" s="T393">ptcl</ta>
            <ta e="T395" id="Seg_7317" s="T394">conj</ta>
            <ta e="T396" id="Seg_7318" s="T395">adv</ta>
            <ta e="T397" id="Seg_7319" s="T396">v</ta>
            <ta e="T398" id="Seg_7320" s="T397">n</ta>
            <ta e="T399" id="Seg_7321" s="T398">n</ta>
            <ta e="T400" id="Seg_7322" s="T399">n</ta>
            <ta e="T401" id="Seg_7323" s="T400">n</ta>
            <ta e="T402" id="Seg_7324" s="T401">v</ta>
            <ta e="T403" id="Seg_7325" s="T402">n</ta>
            <ta e="T404" id="Seg_7326" s="T403">n</ta>
            <ta e="T405" id="Seg_7327" s="T404">v</ta>
            <ta e="T406" id="Seg_7328" s="T405">n</ta>
            <ta e="T407" id="Seg_7329" s="T406">adv</ta>
            <ta e="T408" id="Seg_7330" s="T407">v</ta>
            <ta e="T409" id="Seg_7331" s="T408">n</ta>
            <ta e="T410" id="Seg_7332" s="T409">n</ta>
            <ta e="T411" id="Seg_7333" s="T410">v</ta>
            <ta e="T412" id="Seg_7334" s="T411">conj</ta>
            <ta e="T413" id="Seg_7335" s="T412">n</ta>
            <ta e="T414" id="Seg_7336" s="T413">adv</ta>
            <ta e="T415" id="Seg_7337" s="T414">v</ta>
            <ta e="T416" id="Seg_7338" s="T415">conj</ta>
            <ta e="T417" id="Seg_7339" s="T416">v</ta>
            <ta e="T418" id="Seg_7340" s="T417">conj</ta>
            <ta e="T419" id="Seg_7341" s="T418">emphpro</ta>
            <ta e="T420" id="Seg_7342" s="T419">ptcp</ta>
            <ta e="T421" id="Seg_7343" s="T420">n</ta>
            <ta e="T422" id="Seg_7344" s="T421">n</ta>
            <ta e="T423" id="Seg_7345" s="T422">v</ta>
            <ta e="T424" id="Seg_7346" s="T423">emphpro</ta>
            <ta e="T425" id="Seg_7347" s="T424">n</ta>
            <ta e="T426" id="Seg_7348" s="T425">n</ta>
            <ta e="T427" id="Seg_7349" s="T426">adv</ta>
            <ta e="T428" id="Seg_7350" s="T427">v</ta>
            <ta e="T429" id="Seg_7351" s="T428">adj</ta>
            <ta e="T430" id="Seg_7352" s="T429">n</ta>
            <ta e="T431" id="Seg_7353" s="T430">v</ta>
            <ta e="T432" id="Seg_7354" s="T431">n</ta>
            <ta e="T433" id="Seg_7355" s="T432">n</ta>
            <ta e="T434" id="Seg_7356" s="T433">v</ta>
            <ta e="T435" id="Seg_7357" s="T434">v</ta>
            <ta e="T436" id="Seg_7358" s="T435">v</ta>
            <ta e="T437" id="Seg_7359" s="T436">conj</ta>
            <ta e="T438" id="Seg_7360" s="T437">v</ta>
            <ta e="T439" id="Seg_7361" s="T438">pers</ta>
            <ta e="T440" id="Seg_7362" s="T439">n</ta>
            <ta e="T441" id="Seg_7363" s="T440">v</ta>
            <ta e="T442" id="Seg_7364" s="T441">n</ta>
            <ta e="T443" id="Seg_7365" s="T442">ptcl</ta>
            <ta e="T444" id="Seg_7366" s="T443">pers</ta>
            <ta e="T445" id="Seg_7367" s="T444">v</ta>
            <ta e="T446" id="Seg_7368" s="T445">v</ta>
            <ta e="T447" id="Seg_7369" s="T446">ptcl</ta>
            <ta e="T448" id="Seg_7370" s="T447">adv</ta>
            <ta e="T449" id="Seg_7371" s="T448">conj</ta>
            <ta e="T450" id="Seg_7372" s="T449">v</ta>
            <ta e="T451" id="Seg_7373" s="T450">n</ta>
            <ta e="T452" id="Seg_7374" s="T451">adv</ta>
            <ta e="T453" id="Seg_7375" s="T452">v</ta>
            <ta e="T454" id="Seg_7376" s="T453">pro</ta>
            <ta e="T455" id="Seg_7377" s="T454">n</ta>
            <ta e="T456" id="Seg_7378" s="T455">pp</ta>
            <ta e="T457" id="Seg_7379" s="T456">v</ta>
            <ta e="T458" id="Seg_7380" s="T457">pers</ta>
            <ta e="T459" id="Seg_7381" s="T458">adv</ta>
            <ta e="T460" id="Seg_7382" s="T459">v</ta>
            <ta e="T461" id="Seg_7383" s="T460">emphpro</ta>
            <ta e="T462" id="Seg_7384" s="T461">v</ta>
            <ta e="T463" id="Seg_7385" s="T462">interrog</ta>
            <ta e="T464" id="Seg_7386" s="T463">adv</ta>
            <ta e="T465" id="Seg_7387" s="T464">v</ta>
            <ta e="T466" id="Seg_7388" s="T465">pers</ta>
            <ta e="T467" id="Seg_7389" s="T466">adv</ta>
            <ta e="T468" id="Seg_7390" s="T467">adj</ta>
            <ta e="T469" id="Seg_7391" s="T468">v</ta>
            <ta e="T470" id="Seg_7392" s="T469">n</ta>
            <ta e="T471" id="Seg_7393" s="T470">v</ta>
            <ta e="T472" id="Seg_7394" s="T471">conj</ta>
            <ta e="T473" id="Seg_7395" s="T472">n</ta>
            <ta e="T474" id="Seg_7396" s="T473">pp</ta>
            <ta e="T475" id="Seg_7397" s="T474">adv</ta>
            <ta e="T476" id="Seg_7398" s="T475">v</ta>
            <ta e="T477" id="Seg_7399" s="T476">pro</ta>
            <ta e="T478" id="Seg_7400" s="T477">n</ta>
            <ta e="T479" id="Seg_7401" s="T478">adv</ta>
            <ta e="T480" id="Seg_7402" s="T479">n</ta>
            <ta e="T481" id="Seg_7403" s="T480">adv</ta>
            <ta e="T482" id="Seg_7404" s="T481">v</ta>
            <ta e="T483" id="Seg_7405" s="T482">n</ta>
            <ta e="T484" id="Seg_7406" s="T483">n</ta>
            <ta e="T485" id="Seg_7407" s="T484">v</ta>
            <ta e="T486" id="Seg_7408" s="T485">v</ta>
            <ta e="T487" id="Seg_7409" s="T486">n</ta>
            <ta e="T488" id="Seg_7410" s="T487">v</ta>
            <ta e="T489" id="Seg_7411" s="T488">dem</ta>
            <ta e="T490" id="Seg_7412" s="T489">n</ta>
            <ta e="T491" id="Seg_7413" s="T490">v</ta>
            <ta e="T492" id="Seg_7414" s="T491">conj</ta>
            <ta e="T493" id="Seg_7415" s="T492">v</ta>
            <ta e="T494" id="Seg_7416" s="T493">pro</ta>
            <ta e="T495" id="Seg_7417" s="T494">num</ta>
            <ta e="T496" id="Seg_7418" s="T495">n</ta>
            <ta e="T497" id="Seg_7419" s="T496">dem</ta>
            <ta e="T498" id="Seg_7420" s="T497">interj</ta>
            <ta e="T499" id="Seg_7421" s="T498">adj</ta>
            <ta e="T500" id="Seg_7422" s="T499">n</ta>
            <ta e="T501" id="Seg_7423" s="T500">adj</ta>
            <ta e="T502" id="Seg_7424" s="T501">n</ta>
            <ta e="T503" id="Seg_7425" s="T502">v</ta>
            <ta e="T504" id="Seg_7426" s="T503">n</ta>
            <ta e="T505" id="Seg_7427" s="T504">n</ta>
            <ta e="T506" id="Seg_7428" s="T505">v</ta>
            <ta e="T507" id="Seg_7429" s="T506">n</ta>
            <ta e="T508" id="Seg_7430" s="T507">num</ta>
            <ta e="T509" id="Seg_7431" s="T508">n</ta>
            <ta e="T510" id="Seg_7432" s="T509">adv</ta>
            <ta e="T511" id="Seg_7433" s="T510">n</ta>
            <ta e="T512" id="Seg_7434" s="T511">v</ta>
            <ta e="T513" id="Seg_7435" s="T512">adv</ta>
            <ta e="T514" id="Seg_7436" s="T513">v</ta>
            <ta e="T515" id="Seg_7437" s="T514">n</ta>
            <ta e="T516" id="Seg_7438" s="T515">v</ta>
            <ta e="T517" id="Seg_7439" s="T516">n</ta>
            <ta e="T518" id="Seg_7440" s="T517">v</ta>
            <ta e="T519" id="Seg_7441" s="T518">n</ta>
            <ta e="T520" id="Seg_7442" s="T519">pro</ta>
            <ta e="T521" id="Seg_7443" s="T520">v</ta>
            <ta e="T522" id="Seg_7444" s="T521">v</ta>
            <ta e="T523" id="Seg_7445" s="T522">conj</ta>
            <ta e="T524" id="Seg_7446" s="T523">n</ta>
            <ta e="T525" id="Seg_7447" s="T524">adv</ta>
            <ta e="T526" id="Seg_7448" s="T525">ptcl</ta>
            <ta e="T527" id="Seg_7449" s="T526">v</ta>
            <ta e="T528" id="Seg_7450" s="T527">n</ta>
            <ta e="T529" id="Seg_7451" s="T528">v</ta>
            <ta e="T530" id="Seg_7452" s="T529">adv</ta>
            <ta e="T531" id="Seg_7453" s="T530">v</ta>
            <ta e="T532" id="Seg_7454" s="T531">adv</ta>
            <ta e="T533" id="Seg_7455" s="T532">n</ta>
            <ta e="T534" id="Seg_7456" s="T533">adv</ta>
            <ta e="T535" id="Seg_7457" s="T534">adv</ta>
            <ta e="T536" id="Seg_7458" s="T535">v</ta>
            <ta e="T537" id="Seg_7459" s="T536">v</ta>
            <ta e="T538" id="Seg_7460" s="T537">n</ta>
            <ta e="T539" id="Seg_7461" s="T538">v</ta>
            <ta e="T540" id="Seg_7462" s="T539">n</ta>
            <ta e="T541" id="Seg_7463" s="T540">v</ta>
            <ta e="T542" id="Seg_7464" s="T541">adv</ta>
            <ta e="T543" id="Seg_7465" s="T542">n</ta>
            <ta e="T544" id="Seg_7466" s="T543">v</ta>
            <ta e="T545" id="Seg_7467" s="T544">adv</ta>
            <ta e="T546" id="Seg_7468" s="T545">n</ta>
            <ta e="T547" id="Seg_7469" s="T546">adv</ta>
            <ta e="T548" id="Seg_7470" s="T547">v</ta>
            <ta e="T549" id="Seg_7471" s="T548">adv</ta>
            <ta e="T550" id="Seg_7472" s="T549">pers</ta>
            <ta e="T551" id="Seg_7473" s="T550">n</ta>
            <ta e="T552" id="Seg_7474" s="T551">v</ta>
            <ta e="T553" id="Seg_7475" s="T552">adv</ta>
            <ta e="T554" id="Seg_7476" s="T553">pers</ta>
            <ta e="T555" id="Seg_7477" s="T554">pers</ta>
            <ta e="T556" id="Seg_7478" s="T555">n</ta>
            <ta e="T557" id="Seg_7479" s="T556">pp</ta>
            <ta e="T558" id="Seg_7480" s="T557">v</ta>
            <ta e="T559" id="Seg_7481" s="T558">adv</ta>
            <ta e="T560" id="Seg_7482" s="T559">v</ta>
            <ta e="T561" id="Seg_7483" s="T560">conj</ta>
            <ta e="T562" id="Seg_7484" s="T561">n</ta>
            <ta e="T563" id="Seg_7485" s="T562">v</ta>
            <ta e="T564" id="Seg_7486" s="T563">n</ta>
            <ta e="T565" id="Seg_7487" s="T564">n</ta>
            <ta e="T566" id="Seg_7488" s="T565">v</ta>
            <ta e="T567" id="Seg_7489" s="T566">n</ta>
            <ta e="T568" id="Seg_7490" s="T567">v</ta>
            <ta e="T569" id="Seg_7491" s="T568">n</ta>
            <ta e="T570" id="Seg_7492" s="T569">conj</ta>
            <ta e="T571" id="Seg_7493" s="T570">pers</ta>
            <ta e="T572" id="Seg_7494" s="T571">pers</ta>
            <ta e="T573" id="Seg_7495" s="T572">interrog</ta>
            <ta e="T574" id="Seg_7496" s="T573">v</ta>
            <ta e="T575" id="Seg_7497" s="T574">conj</ta>
            <ta e="T576" id="Seg_7498" s="T575">n</ta>
            <ta e="T577" id="Seg_7499" s="T576">v</ta>
            <ta e="T578" id="Seg_7500" s="T577">conj</ta>
            <ta e="T579" id="Seg_7501" s="T578">pers</ta>
            <ta e="T580" id="Seg_7502" s="T579">interrog</ta>
            <ta e="T581" id="Seg_7503" s="T580">adv</ta>
            <ta e="T582" id="Seg_7504" s="T581">v</ta>
            <ta e="T583" id="Seg_7505" s="T582">v</ta>
            <ta e="T584" id="Seg_7506" s="T583">v</ta>
            <ta e="T585" id="Seg_7507" s="T584">conj</ta>
            <ta e="T586" id="Seg_7508" s="T585">emphpro</ta>
            <ta e="T587" id="Seg_7509" s="T586">n</ta>
            <ta e="T588" id="Seg_7510" s="T587">v</ta>
            <ta e="T589" id="Seg_7511" s="T588">conj</ta>
            <ta e="T590" id="Seg_7512" s="T589">n</ta>
            <ta e="T591" id="Seg_7513" s="T590">pro</ta>
            <ta e="T592" id="Seg_7514" s="T591">ptcl</ta>
            <ta e="T593" id="Seg_7515" s="T592">v</ta>
            <ta e="T594" id="Seg_7516" s="T593">interj</ta>
            <ta e="T595" id="Seg_7517" s="T594">pers</ta>
            <ta e="T596" id="Seg_7518" s="T595">adj</ta>
            <ta e="T597" id="Seg_7519" s="T596">n</ta>
            <ta e="T598" id="Seg_7520" s="T597">conj</ta>
            <ta e="T599" id="Seg_7521" s="T598">conj</ta>
            <ta e="T600" id="Seg_7522" s="T599">v</ta>
            <ta e="T601" id="Seg_7523" s="T600">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T31" id="Seg_7524" s="T30">RUS:disc</ta>
            <ta e="T43" id="Seg_7525" s="T42">RUS:disc</ta>
            <ta e="T44" id="Seg_7526" s="T43">RUS:gram</ta>
            <ta e="T117" id="Seg_7527" s="T116">RUS:cult</ta>
            <ta e="T132" id="Seg_7528" s="T131">RUS:gram</ta>
            <ta e="T141" id="Seg_7529" s="T140">RUS:disc</ta>
            <ta e="T153" id="Seg_7530" s="T152">RUS:gram</ta>
            <ta e="T165" id="Seg_7531" s="T164">RUS:gram</ta>
            <ta e="T176" id="Seg_7532" s="T175">RUS:gram</ta>
            <ta e="T190" id="Seg_7533" s="T189">RUS:gram</ta>
            <ta e="T201" id="Seg_7534" s="T200">RUS:gram</ta>
            <ta e="T246" id="Seg_7535" s="T245">RUS:gram</ta>
            <ta e="T263" id="Seg_7536" s="T262">RUS:gram</ta>
            <ta e="T273" id="Seg_7537" s="T272">RUS:cult</ta>
            <ta e="T281" id="Seg_7538" s="T280">RUS:gram</ta>
            <ta e="T293" id="Seg_7539" s="T292">RUS:gram</ta>
            <ta e="T296" id="Seg_7540" s="T295">RUS:disc</ta>
            <ta e="T297" id="Seg_7541" s="T296">RUS:gram</ta>
            <ta e="T301" id="Seg_7542" s="T300">RUS:disc</ta>
            <ta e="T320" id="Seg_7543" s="T319">RUS:gram</ta>
            <ta e="T337" id="Seg_7544" s="T336">RUS:gram</ta>
            <ta e="T370" id="Seg_7545" s="T369">RUS:disc</ta>
            <ta e="T371" id="Seg_7546" s="T370">RUS:gram</ta>
            <ta e="T394" id="Seg_7547" s="T393">RUS:disc</ta>
            <ta e="T395" id="Seg_7548" s="T394">RUS:gram</ta>
            <ta e="T412" id="Seg_7549" s="T411">RUS:gram</ta>
            <ta e="T416" id="Seg_7550" s="T415">RUS:gram</ta>
            <ta e="T418" id="Seg_7551" s="T417">RUS:gram</ta>
            <ta e="T437" id="Seg_7552" s="T436">RUS:gram</ta>
            <ta e="T443" id="Seg_7553" s="T442">RUS:mod</ta>
            <ta e="T447" id="Seg_7554" s="T446">RUS:disc</ta>
            <ta e="T449" id="Seg_7555" s="T448">RUS:gram</ta>
            <ta e="T454" id="Seg_7556" s="T453">RUS:gram</ta>
            <ta e="T472" id="Seg_7557" s="T471">RUS:gram</ta>
            <ta e="T477" id="Seg_7558" s="T476">RUS:gram</ta>
            <ta e="T492" id="Seg_7559" s="T491">RUS:gram</ta>
            <ta e="T494" id="Seg_7560" s="T493">RUS:gram</ta>
            <ta e="T498" id="Seg_7561" s="T497">RUS:disc</ta>
            <ta e="T523" id="Seg_7562" s="T522">RUS:gram</ta>
            <ta e="T553" id="Seg_7563" s="T552">RUS:disc</ta>
            <ta e="T561" id="Seg_7564" s="T560">RUS:gram</ta>
            <ta e="T570" id="Seg_7565" s="T569">RUS:gram</ta>
            <ta e="T575" id="Seg_7566" s="T574">RUS:gram</ta>
            <ta e="T578" id="Seg_7567" s="T577">RUS:gram</ta>
            <ta e="T585" id="Seg_7568" s="T584">RUS:gram</ta>
            <ta e="T589" id="Seg_7569" s="T588">RUS:gram</ta>
            <ta e="T594" id="Seg_7570" s="T593">RUS:disc</ta>
            <ta e="T599" id="Seg_7571" s="T598">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_7572" s="T1">There lived a husband and a wife.</ta>
            <ta e="T8" id="Seg_7573" s="T5">The husband went hunting.</ta>
            <ta e="T11" id="Seg_7574" s="T8">The wife was staying at home.</ta>
            <ta e="T16" id="Seg_7575" s="T11">The husband wandered for days.</ta>
            <ta e="T19" id="Seg_7576" s="T16">He didn't bring anything.</ta>
            <ta e="T25" id="Seg_7577" s="T19">So he went and didn’t bring anything.</ta>
            <ta e="T35" id="Seg_7578" s="T25">One day the wife thought: “Why doesn’t he bring anything?</ta>
            <ta e="T38" id="Seg_7579" s="T35">I shall follow him.</ta>
            <ta e="T42" id="Seg_7580" s="T38">Why does he come back empty?”</ta>
            <ta e="T46" id="Seg_7581" s="T42">And so she did.</ta>
            <ta e="T51" id="Seg_7582" s="T46">The husband prepared to go hunting and he left.</ta>
            <ta e="T55" id="Seg_7583" s="T51">The wife quickly took the skies.</ta>
            <ta e="T59" id="Seg_7584" s="T55">She went quickly after her husband.</ta>
            <ta e="T62" id="Seg_7585" s="T59">She didn’t show herself up to her husband.</ta>
            <ta e="T69" id="Seg_7586" s="T62">She didn’t show herself up, so that her husband couldn’t see her.</ta>
            <ta e="T73" id="Seg_7587" s="T69">The husband reached the bank of the Chaya river.</ta>
            <ta e="T80" id="Seg_7588" s="T73">He dug out his eyes at this bank.</ta>
            <ta e="T85" id="Seg_7589" s="T80">He hanged his eyes onto twigs.</ta>
            <ta e="T89" id="Seg_7590" s="T85">They rolled to the other bank of the river.</ta>
            <ta e="T93" id="Seg_7591" s="T89">He said: “Eye, attach.”</ta>
            <ta e="T97" id="Seg_7592" s="T93">The eyes came to him and attached to him.</ta>
            <ta e="T107" id="Seg_7593" s="T97">He dug out his eyes again on the other side and left them hanging on twigs. </ta>
            <ta e="T110" id="Seg_7594" s="T107">He said: “Eyes, attach!”</ta>
            <ta e="T114" id="Seg_7595" s="T110">The eyes attached to him.</ta>
            <ta e="T119" id="Seg_7596" s="T114">So he was playing(?) the whole day long.</ta>
            <ta e="T123" id="Seg_7597" s="T119">The wife was watching and watching [him] (stealthily?).</ta>
            <ta e="T126" id="Seg_7598" s="T123">The husband dug the eyes out.</ta>
            <ta e="T131" id="Seg_7599" s="T126">He left them on this side of the bank.</ta>
            <ta e="T138" id="Seg_7600" s="T131">And his wife quickly took his eyes (stealthily?).</ta>
            <ta e="T148" id="Seg_7601" s="T138">She thought: “Ah, that’s why you don’t bring anything home to me.”</ta>
            <ta e="T155" id="Seg_7602" s="T148">So the wife went, she was walking slowly.</ta>
            <ta e="T159" id="Seg_7603" s="T155">Her husband didn’t have eyes any more.</ta>
            <ta e="T163" id="Seg_7604" s="T159">If one left he alone, he would have got lost.</ta>
            <ta e="T168" id="Seg_7605" s="T163">That’s why she was walking slowly.</ta>
            <ta e="T177" id="Seg_7606" s="T168">While she was walking, she looked back, whether her husband was following or not. </ta>
            <ta e="T184" id="Seg_7607" s="T177">After the wife had left, the husband cried: “Eyes, attach!”</ta>
            <ta e="T192" id="Seg_7608" s="T184">No eyes, they didn’t come to him and didn’t attach to him.</ta>
            <ta e="T202" id="Seg_7609" s="T192">He called and called his eyes, but the eyes were not to be detected anywhere.</ta>
            <ta e="T208" id="Seg_7610" s="T202">So the blind husband went forward, he was blind.</ta>
            <ta e="T213" id="Seg_7611" s="T208">The wife was waiting for him, but she didn’t show herself up.</ta>
            <ta e="T217" id="Seg_7612" s="T213">The husband was blind, if he had got lost, he would have died.</ta>
            <ta e="T221" id="Seg_7613" s="T217">They were approaching their house .</ta>
            <ta e="T223" id="Seg_7614" s="T221">The wife was in a hurry.</ta>
            <ta e="T228" id="Seg_7615" s="T223">She quickly reached the house and put off the skies.</ta>
            <ta e="T231" id="Seg_7616" s="T228">She enterd the house and put off her clothes.</ta>
            <ta e="T238" id="Seg_7617" s="T231">She put her husband’s eyes into a vessel and hid it behind the stove.</ta>
            <ta e="T243" id="Seg_7618" s="T238">The husband slowly came home, he came in.</ta>
            <ta e="T251" id="Seg_7619" s="T243">The wife said: “And why were you absent for so long today?”</ta>
            <ta e="T260" id="Seg_7620" s="T251">Her husband said: “I cut(?) my eyes today, that’s why I was absent for so long.”</ta>
            <ta e="T272" id="Seg_7621" s="T260">The wife thought: “What are you saying(?), Now I know you, how you go hunting .</ta>
            <ta e="T276" id="Seg_7622" s="T272">You play with your eyes the whole day long.</ta>
            <ta e="T286" id="Seg_7623" s="T276">You hang them on one side of the bank, and then come skiing to the other side.</ta>
            <ta e="T290" id="Seg_7624" s="T286">Your eyes come to you and attach to you.</ta>
            <ta e="T295" id="Seg_7625" s="T290">That’s why you always came back empty.”</ta>
            <ta e="T299" id="Seg_7626" s="T295">So they lived further.</ta>
            <ta e="T303" id="Seg_7627" s="T299">Several days passed.</ta>
            <ta e="T309" id="Seg_7628" s="T303">The wife looked, she looked through the window: there stood an elk.</ta>
            <ta e="T314" id="Seg_7629" s="T309">The wife said to her husband: “There stands an elk.</ta>
            <ta e="T319" id="Seg_7630" s="T314">Stand up and shoot the elk somehow.”</ta>
            <ta e="T325" id="Seg_7631" s="T319">And the husband said: “How do I shoot it?</ta>
            <ta e="T328" id="Seg_7632" s="T325">My eyes don’t see.”</ta>
            <ta e="T336" id="Seg_7633" s="T328">The wife said: “Come to me, I’ll give you a gun.”</ta>
            <ta e="T340" id="Seg_7634" s="T336">So the husband came to his wife.</ta>
            <ta e="T346" id="Seg_7635" s="T340">The wife gave a loaded gun to her husband.</ta>
            <ta e="T352" id="Seg_7636" s="T346">She showed him how to shoot the elk.</ta>
            <ta e="T356" id="Seg_7637" s="T352">The husband shot and killed the elk.</ta>
            <ta e="T364" id="Seg_7638" s="T356">The husband’s eyes don’t see, he can’t skin the elk.</ta>
            <ta e="T369" id="Seg_7639" s="T364">The wife began to skin the elk herself.</ta>
            <ta e="T373" id="Seg_7640" s="T369">So she skinned the elk.</ta>
            <ta e="T376" id="Seg_7641" s="T373">She cleaned all the meat and hung it.</ta>
            <ta e="T381" id="Seg_7642" s="T376">One day she cooked lots of meat.</ta>
            <ta e="T390" id="Seg_7643" s="T381">She said to her husband: “People say, that today is a big holiday.”</ta>
            <ta e="T393" id="Seg_7644" s="T390">The husband said: “Celebrate”.</ta>
            <ta e="T397" id="Seg_7645" s="T393">So they started celebrating.</ta>
            <ta e="T402" id="Seg_7646" s="T397">The wife outside, the husband inside.</ta>
            <ta e="T406" id="Seg_7647" s="T402">The wife hung a swing.</ta>
            <ta e="T408" id="Seg_7648" s="T406">She started swinging.</ta>
            <ta e="T411" id="Seg_7649" s="T408">The husband stayed at home.</ta>
            <ta e="T417" id="Seg_7650" s="T411">And the wife swung and swung outside.</ta>
            <ta e="T423" id="Seg_7651" s="T417">And she was eating elk meat.</ta>
            <ta e="T428" id="Seg_7652" s="T423">And she was singing a divine song(?) (Christ Has Risen?).</ta>
            <ta e="T431" id="Seg_7653" s="T428">The spring day was long.</ta>
            <ta e="T436" id="Seg_7654" s="T431">The husband became tired of sitting inside.</ta>
            <ta e="T441" id="Seg_7655" s="T436">And he thought: “Why don’t I call my eyes.</ta>
            <ta e="T446" id="Seg_7656" s="T441">Maybe my eyes come to me”.</ta>
            <ta e="T453" id="Seg_7657" s="T446">And so he did, he began to call his eyes.</ta>
            <ta e="T457" id="Seg_7658" s="T453">Something started clanking behind the stove.</ta>
            <ta e="T460" id="Seg_7659" s="T457">He listened to it very carefully.</ta>
            <ta e="T465" id="Seg_7660" s="T460">He thought to himself: “What is clanking over there?”</ta>
            <ta e="T471" id="Seg_7661" s="T465">He said for the second time: “Eyes, attach!”</ta>
            <ta e="T477" id="Seg_7662" s="T471"> Something started clanking behind the stove.</ta>
            <ta e="T485" id="Seg_7663" s="T477">The husband went there slowly with his stick and started scrambling about behind the stove.</ta>
            <ta e="T487" id="Seg_7664" s="T485">He found a vessel.</ta>
            <ta e="T493" id="Seg_7665" s="T487">He sat down, untied the vessel and investigated it.</ta>
            <ta e="T496" id="Seg_7666" s="T493">Some two round things.</ta>
            <ta e="T500" id="Seg_7667" s="T496">“Oh, that are my eyes.”</ta>
            <ta e="T504" id="Seg_7668" s="T500">The husband found some warm water.</ta>
            <ta e="T506" id="Seg_7669" s="T504">He moistened them.</ta>
            <ta e="T512" id="Seg_7670" s="T506">The eyes were soaking in the water for some time.</ta>
            <ta e="T514" id="Seg_7671" s="T512">He washed them well.</ta>
            <ta e="T518" id="Seg_7672" s="T514">He said to the eyes: “Eyes, attach!”</ta>
            <ta e="T521" id="Seg_7673" s="T518">The eyes attached to him.</ta>
            <ta e="T527" id="Seg_7674" s="T521">He looked around, his eyes were not seeing well.</ta>
            <ta e="T532" id="Seg_7675" s="T527">He dug out the eyes and moistened them well again.</ta>
            <ta e="T536" id="Seg_7676" s="T532">Then he wiped the eyes carefully.</ta>
            <ta e="T539" id="Seg_7677" s="T536">He said: “Eyes, attach!”</ta>
            <ta e="T542" id="Seg_7678" s="T539"> The eyes attached to him.</ta>
            <ta e="T548" id="Seg_7679" s="T542">The man was glad: “Now my eyes see well.</ta>
            <ta e="T552" id="Seg_7680" s="T548">I am seeing again.</ta>
            <ta e="T558" id="Seg_7681" s="T552">Now I’ll beat you because of the eyes.”</ta>
            <ta e="T560" id="Seg_7682" s="T558">He went outside.</ta>
            <ta e="T566" id="Seg_7683" s="T560">His wife was swinging singing “Christ Has Risen”.</ta>
            <ta e="T574" id="Seg_7684" s="T566">The husband said to his wife: “Why did you make me blind?”</ta>
            <ta e="T588" id="Seg_7685" s="T574">And the wife said: “And why did you leave, went hunting, and [instead of hunting] were you playing with your eyes?</ta>
            <ta e="T593" id="Seg_7686" s="T588">You didn’t bring anything home.</ta>
            <ta e="T601" id="Seg_7687" s="T593">That‘s why I brought (dug out) your eyes.”</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_7688" s="T1">Es lebten ein Mann und eine Frau.</ta>
            <ta e="T8" id="Seg_7689" s="T5">Der Mann ging jagen.</ta>
            <ta e="T11" id="Seg_7690" s="T8">Die Frau blieb zu Hause.</ta>
            <ta e="T16" id="Seg_7691" s="T11">Der Mann wanderte tagelang.</ta>
            <ta e="T19" id="Seg_7692" s="T16">Er brachte nichts.</ta>
            <ta e="T25" id="Seg_7693" s="T19">So ging er und brachte nichts.</ta>
            <ta e="T35" id="Seg_7694" s="T25">Eines Tages dachte die Frau: "Warum bring er nichts?</ta>
            <ta e="T38" id="Seg_7695" s="T35">Ich werde ihm folgen.</ta>
            <ta e="T42" id="Seg_7696" s="T38">Warum kommt er mit leeren Händen zurück?"</ta>
            <ta e="T46" id="Seg_7697" s="T42">Und so machte sie es.</ta>
            <ta e="T51" id="Seg_7698" s="T46">Der Mann machte sich fertig für die Jagd und ging los.</ta>
            <ta e="T55" id="Seg_7699" s="T51">Die Frau nahm schnell die Skier.</ta>
            <ta e="T59" id="Seg_7700" s="T55">Sie lief ihrem Mann rasch hinterher.</ta>
            <ta e="T62" id="Seg_7701" s="T59">Sie zeigte sich nicht ihrem Mann.</ta>
            <ta e="T69" id="Seg_7702" s="T62">Sie zeigte sich nicht, sodass ihr Mann sie nicht sehen konnte.</ta>
            <ta e="T73" id="Seg_7703" s="T69">Der Mann erreichte das Ufer des Chaya-Flusses.</ta>
            <ta e="T80" id="Seg_7704" s="T73">An diesem Ufer zog er seine Augen heraus.</ta>
            <ta e="T85" id="Seg_7705" s="T80">Er hängte die Augen an Zweige.</ta>
            <ta e="T89" id="Seg_7706" s="T85">Sie rollten ans andere Flussufer.</ta>
            <ta e="T93" id="Seg_7707" s="T89">Er sagte: "Augen, befestigt euch."</ta>
            <ta e="T97" id="Seg_7708" s="T93">Die Augen kamen zu ihm und befestigten sich an ihm.</ta>
            <ta e="T107" id="Seg_7709" s="T97">Er zog seine Augen wieder auf der anderen Seite heraus und ließ sie an Zweigen hängen.</ta>
            <ta e="T110" id="Seg_7710" s="T107">Er sagte: "Augen, befestigt euch!"</ta>
            <ta e="T114" id="Seg_7711" s="T110">Die Augen befestigten sich an ihm.</ta>
            <ta e="T119" id="Seg_7712" s="T114">So spielte(?) er den ganzen Tag.</ta>
            <ta e="T123" id="Seg_7713" s="T119">Die Frau beobachtete und beobachtete [ihn] (heimlich?).</ta>
            <ta e="T126" id="Seg_7714" s="T123">Der Mann zog seine Augen heraus.</ta>
            <ta e="T131" id="Seg_7715" s="T126">Er ließ sie an diesem Ufer zurück.</ta>
            <ta e="T138" id="Seg_7716" s="T131">Und seine Frau nahm seine Augen (heimlich?).</ta>
            <ta e="T148" id="Seg_7717" s="T138">Sie dachte: "Aha, deswegen bringst du mir nichts nach Hause."</ta>
            <ta e="T155" id="Seg_7718" s="T148">So ging die Frau, sie ging langsam.</ta>
            <ta e="T159" id="Seg_7719" s="T155">Ihr Mann hatte keine Augen mehr.</ta>
            <ta e="T163" id="Seg_7720" s="T159">Wenn man ihn alleine ließ, würde er sich verlaufen.</ta>
            <ta e="T168" id="Seg_7721" s="T163">Darum ging sie langsam.</ta>
            <ta e="T177" id="Seg_7722" s="T168">Während sie ging, schaute sie zurück, ob ihr Mann ihr folgte oder nicht.</ta>
            <ta e="T184" id="Seg_7723" s="T177">Nachdem die Frau gegangen war, rief der Mann: "Augen, befestigt euch!"</ta>
            <ta e="T192" id="Seg_7724" s="T184">Keine Augen, sie kamen nicht zu ihm und sie befestigten sich nicht an ihm.</ta>
            <ta e="T202" id="Seg_7725" s="T192">Er rief und rief seine Augen, doch seine Augen waren nirgends zu finden.</ta>
            <ta e="T208" id="Seg_7726" s="T202">So ging der blinde Mann los, er war blind.</ta>
            <ta e="T213" id="Seg_7727" s="T208">Die Frau wartete auf ihn, aber sie zeigte sich nicht.</ta>
            <ta e="T217" id="Seg_7728" s="T213">Der Mann war blind, wenn er sich verlief, würde er sterben.</ta>
            <ta e="T221" id="Seg_7729" s="T217">Sie erreichten ihr Haus.</ta>
            <ta e="T223" id="Seg_7730" s="T221">Die Frau beeilte sich.</ta>
            <ta e="T228" id="Seg_7731" s="T223">Rasch erreichte sie das Haus und zog die Skier aus.</ta>
            <ta e="T231" id="Seg_7732" s="T228">Sie kam ins Haus und zog ihre Kleider aus.</ta>
            <ta e="T238" id="Seg_7733" s="T231">Sie legte die Augen ihres Mannes in ein Gefäß und versteckte es hinter dem Ofen.</ta>
            <ta e="T243" id="Seg_7734" s="T238">Der Mann kam langsam nach Hause, er ging hinein.</ta>
            <ta e="T251" id="Seg_7735" s="T243">Die Frau sagte: "Warum bist du heute so lange fort gewesen?"</ta>
            <ta e="T260" id="Seg_7736" s="T251">Der Mann sagte: "Ich schnitt(?) meine Augen heute, darum war ich so lange weg."</ta>
            <ta e="T272" id="Seg_7737" s="T260">Die Frau dachte: "Was sagst(?) du da, ich kenne dich jetzt, wie du jagst.</ta>
            <ta e="T276" id="Seg_7738" s="T272">Du spielst den ganzen Tag mit deinen Augen.</ta>
            <ta e="T286" id="Seg_7739" s="T276">Du hängst sie an einem Ufer auf und dann fährst du mit den Skiern auf die andere Seite.</ta>
            <ta e="T290" id="Seg_7740" s="T286">Deine Augen kommen zu dir und befestigen sich an dir.</ta>
            <ta e="T295" id="Seg_7741" s="T290">Darum kommst du immer mit leeren Händen zurück."</ta>
            <ta e="T299" id="Seg_7742" s="T295">So lebten sie weiter.</ta>
            <ta e="T303" id="Seg_7743" s="T299">Mehrere Tage vergingen.</ta>
            <ta e="T309" id="Seg_7744" s="T303">Die Frau schaute, sie schaute durchs Fenster: dort stand ein Elch.</ta>
            <ta e="T314" id="Seg_7745" s="T309">Die Frau sagte ihrem Mann: "Dort steht ein Elch.</ta>
            <ta e="T319" id="Seg_7746" s="T314">Steh auf und erschieß den Elch irgendwie."</ta>
            <ta e="T325" id="Seg_7747" s="T319">Und der Mann sagte: "Wie soll ich ihn erschießen?</ta>
            <ta e="T328" id="Seg_7748" s="T325">Meine Augen sehen nicht."</ta>
            <ta e="T336" id="Seg_7749" s="T328">Die Frau sagte: "Komm zu mir, ich gebe dir ein Gewehr."</ta>
            <ta e="T340" id="Seg_7750" s="T336">So ging der Mann zu seiner Frau.</ta>
            <ta e="T346" id="Seg_7751" s="T340">Die Frau gab ihrem Mann ein geladenes Gewehr.</ta>
            <ta e="T352" id="Seg_7752" s="T346">Sie zeigte ihm, wie er den Elch abschießen sollte.</ta>
            <ta e="T356" id="Seg_7753" s="T352">Der Mann schoß und tötete den Elch.</ta>
            <ta e="T364" id="Seg_7754" s="T356">Die Augen des Mannes sehen nicht, er kann den Elch nicht häuten.</ta>
            <ta e="T369" id="Seg_7755" s="T364">Die Frau begann den Elch selbst zu häuten.</ta>
            <ta e="T373" id="Seg_7756" s="T369">So häutete sie den Elch.</ta>
            <ta e="T376" id="Seg_7757" s="T373">Sie säuberte das ganze Fleisch und hing es auf.</ta>
            <ta e="T381" id="Seg_7758" s="T376">Eines Tages kochte sie viel Fleisch.</ta>
            <ta e="T390" id="Seg_7759" s="T381">Sie sagte ihrem Mann: "Die Leute sagen, heute ist ein großer Feiertag."</ta>
            <ta e="T393" id="Seg_7760" s="T390">Der Mann sagte: "Feier (ihn)."</ta>
            <ta e="T397" id="Seg_7761" s="T393">So begannen sie zu feiern.</ta>
            <ta e="T402" id="Seg_7762" s="T397">Die Frau draußen, der Mann drinnen.</ta>
            <ta e="T406" id="Seg_7763" s="T402">Die Frau hing eine Schaukel auf.</ta>
            <ta e="T408" id="Seg_7764" s="T406">Sie begann zu schaukeln.</ta>
            <ta e="T411" id="Seg_7765" s="T408">Der Mann blieb zu Hause.</ta>
            <ta e="T417" id="Seg_7766" s="T411">Und die Frau schaukelte und schaukelte draußen.</ta>
            <ta e="T423" id="Seg_7767" s="T417">Und sie aß Elchfleisch.</ta>
            <ta e="T428" id="Seg_7768" s="T423">Und sie sang ein heiliges Lied(?) (Christ ist auferstanden?).</ta>
            <ta e="T431" id="Seg_7769" s="T428">Der Frühlingstag war lang.</ta>
            <ta e="T436" id="Seg_7770" s="T431">Dem Mann wurde es langweilig, drinnen zu sitzen.</ta>
            <ta e="T441" id="Seg_7771" s="T436">Und er dachte: "Warum rufe ich nicht meine Augen.</ta>
            <ta e="T446" id="Seg_7772" s="T441">Vielleicht kommen meine Augen zu mir."</ta>
            <ta e="T453" id="Seg_7773" s="T446">Und so machte er es, er begann seine Augen zu rufen.</ta>
            <ta e="T457" id="Seg_7774" s="T453">Etwas begann hinter dem Ofen zu klappern.</ta>
            <ta e="T460" id="Seg_7775" s="T457">Er horchte sehr aufmerksam darauf.</ta>
            <ta e="T465" id="Seg_7776" s="T460">Er dachte sich: "Was klappert dort?"</ta>
            <ta e="T471" id="Seg_7777" s="T465">Er sagte ein zweites Mal: "Augen, befestigt euch!"</ta>
            <ta e="T477" id="Seg_7778" s="T471">Etwas begann hinter dem Ofen zu klappern.</ta>
            <ta e="T485" id="Seg_7779" s="T477">Der Mann ging langsam mit seinem Stock dorthin und begann hinter dem Ofen zu kramen.</ta>
            <ta e="T487" id="Seg_7780" s="T485">Er fand ein Gefäß.</ta>
            <ta e="T493" id="Seg_7781" s="T487">Er setzte sich, band das Gefäß auf und untersuchte es.</ta>
            <ta e="T496" id="Seg_7782" s="T493">Zwei runde Dinge.</ta>
            <ta e="T500" id="Seg_7783" s="T496">"Oh, das sind meine Augen."</ta>
            <ta e="T504" id="Seg_7784" s="T500">Der Mann fand ein wenig warmes Wasser.</ta>
            <ta e="T506" id="Seg_7785" s="T504">Er befeuchtete sie.</ta>
            <ta e="T512" id="Seg_7786" s="T506">Die Augen weichten im Wasser einige Zeit ein.</ta>
            <ta e="T514" id="Seg_7787" s="T512">Er wusch sie gründlich.</ta>
            <ta e="T518" id="Seg_7788" s="T514">Er sagte zu den Augen: "Augen, befestigt euch!"</ta>
            <ta e="T521" id="Seg_7789" s="T518">Die Augen befestigten sich an ihm.</ta>
            <ta e="T527" id="Seg_7790" s="T521">Er sah sich um, seine Augen sahen nicht gut.</ta>
            <ta e="T532" id="Seg_7791" s="T527">Er zog seine Augen heraus und befeuchtete sie wieder gründlich.</ta>
            <ta e="T536" id="Seg_7792" s="T532">Dann trocknete er die Augen sorgfältig ab.</ta>
            <ta e="T539" id="Seg_7793" s="T536">Er sagte: "Augen, befestigt euch!"</ta>
            <ta e="T542" id="Seg_7794" s="T539">Die Augen befestigten sich an ihm.</ta>
            <ta e="T548" id="Seg_7795" s="T542">Der Mann war froh: "Jetzt sehen meine Augen gut.</ta>
            <ta e="T552" id="Seg_7796" s="T548">Ich sehe wieder.</ta>
            <ta e="T558" id="Seg_7797" s="T552">Jetzt werde ich dich wegen der Augen schlagen."</ta>
            <ta e="T560" id="Seg_7798" s="T558">Er ging nach draußen.</ta>
            <ta e="T566" id="Seg_7799" s="T560">Seine Frau schaukelte "Christ ist auferstanden" singend.</ta>
            <ta e="T574" id="Seg_7800" s="T566">Der Mann sagte zu seiner Frau: "Warum hast du mich erblinden lassen?"</ta>
            <ta e="T588" id="Seg_7801" s="T574">Und die Frau sagte: "Und warum bist du gegangen, jagen gegangen und hast [statt zu jagen] mit deinen Augen gespielt?</ta>
            <ta e="T593" id="Seg_7802" s="T588">Du hast nichts nach Hause gebracht.</ta>
            <ta e="T601" id="Seg_7803" s="T593">Darum habe ich deine Augen mitgenommen (rausgenommen)."</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_7804" s="T1">Жили-были муж с женой.</ta>
            <ta e="T8" id="Seg_7805" s="T5">Муж ходит охотиться.</ta>
            <ta e="T11" id="Seg_7806" s="T8">Жена дома бывает.</ta>
            <ta e="T16" id="Seg_7807" s="T11">Муж ходит, ходит, целый день ходит.</ta>
            <ta e="T19" id="Seg_7808" s="T16">Ничего не приносит.</ta>
            <ta e="T25" id="Seg_7809" s="T19">Всегда так ходит, ничего не приносит.</ta>
            <ta e="T35" id="Seg_7810" s="T25">В один день жена думает: “Почему же он ничего не приносит?</ta>
            <ta e="T38" id="Seg_7811" s="T35">Я за ним прослежу.</ta>
            <ta e="T42" id="Seg_7812" s="T38">Почему он пустой приходит?”</ta>
            <ta e="T46" id="Seg_7813" s="T42">Ну и так сделала.</ta>
            <ta e="T51" id="Seg_7814" s="T46">Муж собираться начал на охоту, отправился.</ta>
            <ta e="T55" id="Seg_7815" s="T51">Жена быстро лыжи одела.</ta>
            <ta e="T59" id="Seg_7816" s="T55">За мужом быстро отправилась.</ta>
            <ta e="T62" id="Seg_7817" s="T59">Мужу не показывается.</ta>
            <ta e="T69" id="Seg_7818" s="T62">Потому не показывается, чтобы муж ее не видел.</ta>
            <ta e="T73" id="Seg_7819" s="T69">Муж пришел к берегу Чаи.</ta>
            <ta e="T80" id="Seg_7820" s="T73">На этом берегу свои глаза выкопал.</ta>
            <ta e="T85" id="Seg_7821" s="T80">Глаза подвесил на сучки.</ta>
            <ta e="T89" id="Seg_7822" s="T85">На ту сторону реки укатываются.</ta>
            <ta e="T93" id="Seg_7823" s="T89">Сам говорит: “Глаз, приклейся”.</ta>
            <ta e="T97" id="Seg_7824" s="T93">Глаза к нему приходят, прилепляются.</ta>
            <ta e="T107" id="Seg_7825" s="T97">Опять на другой стороне берега глаза выкапывает, оставляет, на суки вешает.</ta>
            <ta e="T110" id="Seg_7826" s="T107">Говорит: “Глаза, приклейтесь!”</ta>
            <ta e="T114" id="Seg_7827" s="T110">Глаза к нему прилепляются.</ta>
            <ta e="T119" id="Seg_7828" s="T114">Так целый день балует(?).</ta>
            <ta e="T123" id="Seg_7829" s="T119">Жена смотрела, смотрела, (украдкой?).</ta>
            <ta e="T126" id="Seg_7830" s="T123">Муж глаза выкопал.</ta>
            <ta e="T131" id="Seg_7831" s="T126">На этой стороне берега оставил.</ta>
            <ta e="T138" id="Seg_7832" s="T131">А жена быстро его глаза взяла (украдкой?).</ta>
            <ta e="T148" id="Seg_7833" s="T138">Сама думает: “А, ты поэтому домой ничего не приносишь мне”.</ta>
            <ta e="T155" id="Seg_7834" s="T148">Так жена пошла, пошла и потихоньку идет.</ta>
            <ta e="T159" id="Seg_7835" s="T155">У ее мужа теперь глаз нет.</ta>
            <ta e="T163" id="Seg_7836" s="T159">Оставить его – он заблудится.</ta>
            <ta e="T168" id="Seg_7837" s="T163">Поэтому и тихонько идет жена.</ta>
            <ta e="T177" id="Seg_7838" s="T168">Сколько идет, все время назад смотрит: муж идет или нет.</ta>
            <ta e="T184" id="Seg_7839" s="T177">После ухода жены муж сколько кричал: “Глаза, прильните!”</ta>
            <ta e="T192" id="Seg_7840" s="T184">Глаз нет, не приходят к нему и не прилепляются.</ta>
            <ta e="T202" id="Seg_7841" s="T192">Сколько он свои глаза звал, звал, глаз как нигде нет, так и нет.</ta>
            <ta e="T208" id="Seg_7842" s="T202">Теперь слепой муж отправился, слепой.</ta>
            <ta e="T213" id="Seg_7843" s="T208">Жена поджидает, мужу не показывается.</ta>
            <ta e="T217" id="Seg_7844" s="T213">Муж слепой, заблудившись, умрет.</ta>
            <ta e="T221" id="Seg_7845" s="T217">К дому подходить стали.</ta>
            <ta e="T223" id="Seg_7846" s="T221">Жена торопится.</ta>
            <ta e="T228" id="Seg_7847" s="T223">Быстро доехала, лыжи скинула.</ta>
            <ta e="T231" id="Seg_7848" s="T228">В избу зашла, разделась.</ta>
            <ta e="T238" id="Seg_7849" s="T231">Глаза мужа в посуду положила, за печку спрятала.</ta>
            <ta e="T243" id="Seg_7850" s="T238">Муж потихоньку пришел домой, зашел.</ta>
            <ta e="T251" id="Seg_7851" s="T243">Жена сказала: “А ты почему сегодня долго был?”</ta>
            <ta e="T260" id="Seg_7852" s="T251">Муж ее сказал: “Я сегодня глаз напорол(?), поэтому долго был”.</ta>
            <ta e="T272" id="Seg_7853" s="T260">Жена думает: “А ты болтаешь(?), я теперь тебя знаю, как ты промышляешь.</ta>
            <ta e="T276" id="Seg_7854" s="T272">Ты целый день с глазами играешь.</ta>
            <ta e="T286" id="Seg_7855" s="T276">На ту сторону берега вешаешь, а на эту сторону прикатываешься (на лыжах).</ta>
            <ta e="T290" id="Seg_7856" s="T286">Глаза, к тебе приходя, прилипают.</ta>
            <ta e="T295" id="Seg_7857" s="T290">Так ты пустой и приходил”.</ta>
            <ta e="T299" id="Seg_7858" s="T295">Ну и жить стали.</ta>
            <ta e="T303" id="Seg_7859" s="T299">Сколько-то дней прошло.</ta>
            <ta e="T309" id="Seg_7860" s="T303">Жена взглянула, в окошко взглянула: стоит лось.</ta>
            <ta e="T314" id="Seg_7861" s="T309">Жена мужу сказала: “Лось стоит.</ta>
            <ta e="T319" id="Seg_7862" s="T314">Ты встань, как-нибудь застрели лося”.</ta>
            <ta e="T325" id="Seg_7863" s="T319">А муж сказал: “Я как застрелю?</ta>
            <ta e="T328" id="Seg_7864" s="T325">Глаза мои не видят”.</ta>
            <ta e="T336" id="Seg_7865" s="T328">Жена сказала: “Иди ко мне, я тебе ружье дам”.</ta>
            <ta e="T340" id="Seg_7866" s="T336">И муж пришел к жене.</ta>
            <ta e="T346" id="Seg_7867" s="T340">Жена мужу дала в руки заряженное ружье.</ta>
            <ta e="T352" id="Seg_7868" s="T346">Сама ему указала, как застрелить лося.</ta>
            <ta e="T356" id="Seg_7869" s="T352">Муж выстрелил, убил лося.</ta>
            <ta e="T364" id="Seg_7870" s="T356">Глаза мужа не видят, лося ему не ободрать.</ta>
            <ta e="T369" id="Seg_7871" s="T364">Жена сама обдирать стала лося.</ta>
            <ta e="T373" id="Seg_7872" s="T369">Ну и ободрала лося.</ta>
            <ta e="T376" id="Seg_7873" s="T373">Мясо все очистила, повесила.</ta>
            <ta e="T381" id="Seg_7874" s="T376">В один день мяса много сварила.</ta>
            <ta e="T390" id="Seg_7875" s="T381">Мужу говорит: “Сегодня, люди говорят, сегодняшний день большой праздник”.</ta>
            <ta e="T393" id="Seg_7876" s="T390">Муж сказал: “Празднуй”.</ta>
            <ta e="T397" id="Seg_7877" s="T393">Ну и праздновать начали.</ta>
            <ta e="T402" id="Seg_7878" s="T397">Жена на улице, муж в избе.</ta>
            <ta e="T406" id="Seg_7879" s="T402">Жена на улице повесила качели.</ta>
            <ta e="T408" id="Seg_7880" s="T406">Качаться начала.</ta>
            <ta e="T411" id="Seg_7881" s="T408">Муж дома торчит.</ta>
            <ta e="T417" id="Seg_7882" s="T411">А жена на улице качается и качается.</ta>
            <ta e="T423" id="Seg_7883" s="T417">А сама сушеное мясо лосиное ест.</ta>
            <ta e="T428" id="Seg_7884" s="T423">Сама божественную песню(?) (Христос воскресе?) поет.</ta>
            <ta e="T431" id="Seg_7885" s="T428">Весенний день долгий.</ta>
            <ta e="T436" id="Seg_7886" s="T431">Муж в избе сидел, сидел, устал.</ta>
            <ta e="T441" id="Seg_7887" s="T436">И думает: “Позвову-ка я глаза свои.</ta>
            <ta e="T446" id="Seg_7888" s="T441">Глаза, может быть, ко мне придут”.</ta>
            <ta e="T453" id="Seg_7889" s="T446">Ну так и сделал, глаза звать начал.</ta>
            <ta e="T457" id="Seg_7890" s="T453">Что-то за печкой гремит.</ta>
            <ta e="T460" id="Seg_7891" s="T457">Он хорошенько послушал.</ta>
            <ta e="T465" id="Seg_7892" s="T460">Сам думает: “Что там гремит?”</ta>
            <ta e="T471" id="Seg_7893" s="T465">Он опять во второй раз сказал: “Глаза, прильните!”</ta>
            <ta e="T477" id="Seg_7894" s="T471">А за печкой что-то сильно загремело.</ta>
            <ta e="T485" id="Seg_7895" s="T477">Муж туда с посохом помаленьку ушел, за печкой щупает.</ta>
            <ta e="T487" id="Seg_7896" s="T485">Нашел миску.</ta>
            <ta e="T493" id="Seg_7897" s="T487">Сел, эту миску развязал и щупает.</ta>
            <ta e="T496" id="Seg_7898" s="T493">Какие-то два кругляшка.</ta>
            <ta e="T500" id="Seg_7899" s="T496">“Это, однако, мои глаза”.</ta>
            <ta e="T504" id="Seg_7900" s="T500">Теплую воду нашел муж.</ta>
            <ta e="T506" id="Seg_7901" s="T504">В воде [их] намочил.</ta>
            <ta e="T512" id="Seg_7902" s="T506">Глаза сколько в воде намокали.</ta>
            <ta e="T514" id="Seg_7903" s="T512">Хорошенько вымыл.</ta>
            <ta e="T518" id="Seg_7904" s="T514">Глазам сказал: “Глаза, прильните!”</ta>
            <ta e="T521" id="Seg_7905" s="T518">Глаза к нему прильнули.</ta>
            <ta e="T527" id="Seg_7906" s="T521">Посмотрел, глаза хорошо не видят.</ta>
            <ta e="T532" id="Seg_7907" s="T527">Глаза выкопал, опять намочил хорошенько.</ta>
            <ta e="T536" id="Seg_7908" s="T532">Глаза опять хорошенько протер.</ta>
            <ta e="T539" id="Seg_7909" s="T536">Сказал: “Глаза, прилипните!”</ta>
            <ta e="T542" id="Seg_7910" s="T539">Глаза прильнули обратно.</ta>
            <ta e="T548" id="Seg_7911" s="T542">Мужик обрадовался: “Теперь глаза мои хорошо видят.</ta>
            <ta e="T552" id="Seg_7912" s="T548">Теперь я с глазами стал.</ta>
            <ta e="T558" id="Seg_7913" s="T552">Уж я тебя из-за глаз побью”.</ta>
            <ta e="T560" id="Seg_7914" s="T558">На улицу вышел.</ta>
            <ta e="T566" id="Seg_7915" s="T560">А жена качается, “Христос воскрес” поет.</ta>
            <ta e="T574" id="Seg_7916" s="T566">Муж сказал жене: “А ты меня зачем ослепила?”</ta>
            <ta e="T588" id="Seg_7917" s="T574">А жена сказала: “А ты почему так ходил, уходил охотиться, а сам с глазами играл?</ta>
            <ta e="T593" id="Seg_7918" s="T588">А домой ничего не приносил.</ta>
            <ta e="T601" id="Seg_7919" s="T593">Вот я твои глаза поэтому и (выкопала) притащила”.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_7920" s="T1">жили были муж с женой</ta>
            <ta e="T8" id="Seg_7921" s="T5">муж ходит охотиться</ta>
            <ta e="T11" id="Seg_7922" s="T8">жена дома</ta>
            <ta e="T16" id="Seg_7923" s="T11">муж ходит ходит целый день ходит</ta>
            <ta e="T19" id="Seg_7924" s="T16">никого не приносит</ta>
            <ta e="T25" id="Seg_7925" s="T19">всегда так ходит никого не приносит</ta>
            <ta e="T35" id="Seg_7926" s="T25">один день жена думает почему же он ничего не приносит</ta>
            <ta e="T38" id="Seg_7927" s="T35">я его попримечу (при(вы)слежу, выкараулю)</ta>
            <ta e="T42" id="Seg_7928" s="T38">почему он пустой приходит</ta>
            <ta e="T46" id="Seg_7929" s="T42">ну и так сделала</ta>
            <ta e="T51" id="Seg_7930" s="T46">мужик собираться начал на охоту пошел</ta>
            <ta e="T55" id="Seg_7931" s="T51">жена скоренько лыжи одела</ta>
            <ta e="T59" id="Seg_7932" s="T55">за мужиком скоренько пошагала</ta>
            <ta e="T62" id="Seg_7933" s="T59">мужику не показывается</ta>
            <ta e="T69" id="Seg_7934" s="T62">зато не показывается чтоб мужик ее не видел</ta>
            <ta e="T73" id="Seg_7935" s="T69">муж пришел к берегу Чаи</ta>
            <ta e="T80" id="Seg_7936" s="T73">на берегу себе глаза выкопал</ta>
            <ta e="T85" id="Seg_7937" s="T80">глаза подвесил на сучки</ta>
            <ta e="T89" id="Seg_7938" s="T85">на ту сторону реки (Чаи) укатятся</ta>
            <ta e="T93" id="Seg_7939" s="T89">сам скажет глаза прильните</ta>
            <ta e="T97" id="Seg_7940" s="T93">глаза к нему прикатываются (приходят) прилепляются</ta>
            <ta e="T107" id="Seg_7941" s="T97">опять на другой стороне берега глаза выкапывает оставляет на суки вешает</ta>
            <ta e="T110" id="Seg_7942" s="T107">говорит глаза прильните</ta>
            <ta e="T114" id="Seg_7943" s="T110">глаза к нему прилепляются</ta>
            <ta e="T119" id="Seg_7944" s="T114">и так целый день балует</ta>
            <ta e="T123" id="Seg_7945" s="T119">жена смотрела смотрела крадучи</ta>
            <ta e="T126" id="Seg_7946" s="T123">муж глаза выкопал</ta>
            <ta e="T131" id="Seg_7947" s="T126">на эту сторону берега оставил</ta>
            <ta e="T138" id="Seg_7948" s="T131">а жена скоренько его глаза взяла крадучи</ta>
            <ta e="T148" id="Seg_7949" s="T138">сама думает а ты поэтому домой ничего не приносишь мне</ta>
            <ta e="T155" id="Seg_7950" s="T148">так жена пошла пошла и помаленьку катится</ta>
            <ta e="T159" id="Seg_7951" s="T155">у мужа теперь глаз нет</ta>
            <ta e="T163" id="Seg_7952" s="T159">оставить его он заблудится</ta>
            <ta e="T168" id="Seg_7953" s="T163">поэтому и тихонько идет жен(а)</ta>
            <ta e="T177" id="Seg_7954" s="T168">сколько идет опять назад смотрит муж идет или нет</ta>
            <ta e="T184" id="Seg_7955" s="T177">после жены мужик сколько кричал глаза прильните</ta>
            <ta e="T192" id="Seg_7956" s="T184">глаз нет не приходят к нему и не прилепляются</ta>
            <ta e="T202" id="Seg_7957" s="T192">сколько он их звал глаз как нигде нет так нет</ta>
            <ta e="T208" id="Seg_7958" s="T202">теперь слепой муж отправился слепой</ta>
            <ta e="T213" id="Seg_7959" s="T208">жена поджидает мужу не показывается</ta>
            <ta e="T217" id="Seg_7960" s="T213">муж слепой заблудится помрет</ta>
            <ta e="T221" id="Seg_7961" s="T217">к дому подходить стали</ta>
            <ta e="T223" id="Seg_7962" s="T221">жена торопится</ta>
            <ta e="T228" id="Seg_7963" s="T223">скоренько прикатилась лыжи скинула</ta>
            <ta e="T231" id="Seg_7964" s="T228">в избу зашла разделась</ta>
            <ta e="T238" id="Seg_7965" s="T231">мужиковы глаза в посуду положила за печку спрятала</ta>
            <ta e="T243" id="Seg_7966" s="T238">муж потихоньку пришел домой</ta>
            <ta e="T251" id="Seg_7967" s="T243">жена сказала а ты почему сегодня долго ходил</ta>
            <ta e="T260" id="Seg_7968" s="T251">муж сказал я сегодня глаз напорол поэтому (зато) долго был</ta>
            <ta e="T272" id="Seg_7969" s="T260">жена думает ты болтаешь я теперь тебя знаю как ты промышляешь</ta>
            <ta e="T276" id="Seg_7970" s="T272">целый день с глазами играешь</ta>
            <ta e="T286" id="Seg_7971" s="T276">на ту сторону берега повесишь а на эту сторону прикатываешься (на лыжах)</ta>
            <ta e="T290" id="Seg_7972" s="T286">глаза к тебе прилетывают прилипают</ta>
            <ta e="T295" id="Seg_7973" s="T290">так ты пустой и приходил</ta>
            <ta e="T299" id="Seg_7974" s="T295">ну жить стали</ta>
            <ta e="T303" id="Seg_7975" s="T299">сколько-то дней прошло</ta>
            <ta e="T309" id="Seg_7976" s="T303">жена смотрит в окошко взглянула стоит лось</ta>
            <ta e="T314" id="Seg_7977" s="T309">жена мужу сказала лось стоит</ta>
            <ta e="T319" id="Seg_7978" s="T314">ты встань как-нибудь застрели лося</ta>
            <ta e="T325" id="Seg_7979" s="T319">а муж сказал я как застрелю</ta>
            <ta e="T328" id="Seg_7980" s="T325">глаза не видят</ta>
            <ta e="T336" id="Seg_7981" s="T328">жена сказала иди ко мне я тебе ружье дам</ta>
            <ta e="T340" id="Seg_7982" s="T336">и муж пришел к жене</ta>
            <ta e="T346" id="Seg_7983" s="T340">жена мужу дала в руки заряженное ружье</ta>
            <ta e="T352" id="Seg_7984" s="T346">сама ему указала как застрелить лося</ta>
            <ta e="T356" id="Seg_7985" s="T352">муж выстрелил убил лося</ta>
            <ta e="T364" id="Seg_7986" s="T356">глаза мужика не видят лося ему не ободрать</ta>
            <ta e="T369" id="Seg_7987" s="T364">жена сама обдирать стала лося</ta>
            <ta e="T373" id="Seg_7988" s="T369">ну ободрала лося</ta>
            <ta e="T376" id="Seg_7989" s="T373">мясо все очистила повесила</ta>
            <ta e="T381" id="Seg_7990" s="T376">один день мяса много сварила</ta>
            <ta e="T390" id="Seg_7991" s="T381">мужу говорит сегодня люди говорят сегодняшний день большой праздник</ta>
            <ta e="T393" id="Seg_7992" s="T390">муж сказал ну и празднуй</ta>
            <ta e="T397" id="Seg_7993" s="T393">ну праздновать начали</ta>
            <ta e="T402" id="Seg_7994" s="T397">жена на улице муж в избе</ta>
            <ta e="T406" id="Seg_7995" s="T402">жена на улице повесила качельку</ta>
            <ta e="T408" id="Seg_7996" s="T406">качаться начала</ta>
            <ta e="T411" id="Seg_7997" s="T408">муж дома торчит</ta>
            <ta e="T417" id="Seg_7998" s="T411">а жена на улице качается</ta>
            <ta e="T423" id="Seg_7999" s="T417">а сама сушеное мясо лосиное ест</ta>
            <ta e="T428" id="Seg_8000" s="T423">сама Христос воскрес поет</ta>
            <ta e="T431" id="Seg_8001" s="T428">весенний день долгий</ta>
            <ta e="T436" id="Seg_8002" s="T431">муж в избе сидел сидел весь устал</ta>
            <ta e="T441" id="Seg_8003" s="T436">и думает я глаза позову</ta>
            <ta e="T446" id="Seg_8004" s="T441">глаз может быть ко мне придут</ta>
            <ta e="T453" id="Seg_8005" s="T446">так и сделал глаза звать начал</ta>
            <ta e="T457" id="Seg_8006" s="T453">что-то за печкой гремит</ta>
            <ta e="T460" id="Seg_8007" s="T457">он хорошенько послушал</ta>
            <ta e="T465" id="Seg_8008" s="T460">сам думает чего там гремит</ta>
            <ta e="T471" id="Seg_8009" s="T465">он опять второй раз сказал глаза прильните</ta>
            <ta e="T477" id="Seg_8010" s="T471">а за печкой что-то шибко загремело</ta>
            <ta e="T485" id="Seg_8011" s="T477">муж туда с посохом помаленьку ушел за печкой шарится</ta>
            <ta e="T487" id="Seg_8012" s="T485">нашел посуду</ta>
            <ta e="T493" id="Seg_8013" s="T487">сел ту посуду развязал и щупает</ta>
            <ta e="T496" id="Seg_8014" s="T493">какие-то два кругляшка</ta>
            <ta e="T500" id="Seg_8015" s="T496">это однако мои глаза</ta>
            <ta e="T504" id="Seg_8016" s="T500">теплую воду нашел муж</ta>
            <ta e="T506" id="Seg_8017" s="T504">в воде намочил</ta>
            <ta e="T512" id="Seg_8018" s="T506">глаза сколько в воде намокали</ta>
            <ta e="T514" id="Seg_8019" s="T512">крепко вымыл</ta>
            <ta e="T518" id="Seg_8020" s="T514">глазам глаза прильните</ta>
            <ta e="T521" id="Seg_8021" s="T518">глаза к нему прильнули</ta>
            <ta e="T527" id="Seg_8022" s="T521">посмотрел глаза хорошо (мастерку) не видят</ta>
            <ta e="T532" id="Seg_8023" s="T527">глаза выкопал опять намочил хорошенько</ta>
            <ta e="T536" id="Seg_8024" s="T532">глаза опять хорошенько протер</ta>
            <ta e="T539" id="Seg_8025" s="T536">сказал глаза прилипните</ta>
            <ta e="T542" id="Seg_8026" s="T539">глаза прильнули обратно</ta>
            <ta e="T548" id="Seg_8027" s="T542">мужик обрадовался теперь глаза мои хорошо видят</ta>
            <ta e="T552" id="Seg_8028" s="T548">теперь я с глазами стал</ta>
            <ta e="T558" id="Seg_8029" s="T552">ужо я тебя из-за глаз изобью</ta>
            <ta e="T560" id="Seg_8030" s="T558">на улицу вышел</ta>
            <ta e="T566" id="Seg_8031" s="T560">а жена качается Христос воскрес поет</ta>
            <ta e="T574" id="Seg_8032" s="T566">муж сказал жене а ты меня почто ослепила</ta>
            <ta e="T588" id="Seg_8033" s="T574">а жена сказала а ты почему так ходил уходил охотиться а сам с глазами играл</ta>
            <ta e="T593" id="Seg_8034" s="T588">а домой никого не приносил</ta>
            <ta e="T601" id="Seg_8035" s="T593">вот я твои глаза зато и (выкопала) притащила</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T16" id="Seg_8036" s="T11">[KuAI:] Variant: 'palʼdʼekuŋ'.</ta>
            <ta e="T46" id="Seg_8037" s="T42">[KuAI:] Variant: 'nilʼdʼzʼiŋ'.</ta>
            <ta e="T55" id="Seg_8038" s="T51">[KuAI:] Variant: 'tolʼdʼzʼimdə'.</ta>
            <ta e="T80" id="Seg_8039" s="T73">[KuAI:] Variants: 'qɨn', 'paːroɣɨn'. [BrM:] Tentative analysis of 'paqqɨlgut'.</ta>
            <ta e="T85" id="Seg_8040" s="T80">[KuAI:] Variants: 'qwäcʼikut', 'molaɣon'.</ta>
            <ta e="T107" id="Seg_8041" s="T97">[KuAI:] Variant: 'ätkut'. [BrM:] GEN in 'seint'?</ta>
            <ta e="T114" id="Seg_8042" s="T110">[BrM:] Tentative analysis of 'rokecʼqwatt'.</ta>
            <ta e="T119" id="Seg_8043" s="T114">[KuAI:] Variant: 'redɨŋ'. [BrM:] 'nilʼdʼä redaŋ' changed to 'nilʼdʼäredaŋ'.</ta>
            <ta e="T131" id="Seg_8044" s="T126">[KuAI:] Variant: 'qwädʼit'.</ta>
            <ta e="T148" id="Seg_8045" s="T138">[KuAI:] Variant: 'teːrban'.</ta>
            <ta e="T192" id="Seg_8046" s="T184">[KuAI:] Variant: 'parɨcʼiqwattə'.</ta>
            <ta e="T202" id="Seg_8047" s="T192">[BrM:] 'niŋi' changed to 'niŋ i'.</ta>
            <ta e="T208" id="Seg_8048" s="T202">[BrM:] INFER?</ta>
            <ta e="T221" id="Seg_8049" s="T217">[BrM:] Tentative analysis of 'miglʼe'.</ta>
            <ta e="T228" id="Seg_8050" s="T223">[KuAI:] Variant: 'čaɣannat'. [BrM:] 'niŋel' changed to 'niŋ el'. [BrM:] Tentative analysis of 'niŋ el čaɣannɨt'.</ta>
            <ta e="T238" id="Seg_8051" s="T231">[KuAI:] Variants: 'tɨssont', 'pʼöcʼen'.</ta>
            <ta e="T272" id="Seg_8052" s="T260">[BrM:] Tentative analysis of 'siːrant', 'suːrunʼänt'.</ta>
            <ta e="T286" id="Seg_8053" s="T276">[BrM:] 'qɨnbaːrt' changed to 'qɨn baːrt', 'kɨnbaːrt' changed to 'kɨn baːrt'.</ta>
            <ta e="T309" id="Seg_8054" s="T303">[KuAI:] Variant: 'penk'.</ta>
            <ta e="T319" id="Seg_8055" s="T314">[KuAI:] Variant: 'tʼäʒilʼel'.</ta>
            <ta e="T336" id="Seg_8056" s="T328">[BrM:] Tentative analysis of 'oralǯolǯilʼews'.</ta>
            <ta e="T376" id="Seg_8057" s="T373">[KuAI:] Variant: 'äːdɨt'.</ta>
            <ta e="T390" id="Seg_8058" s="T381">[KuAI:] Variant: 'nuːlʼtʼel'.</ta>
            <ta e="T393" id="Seg_8059" s="T390">[BrM:] TR ču? cf 74</ta>
            <ta e="T408" id="Seg_8060" s="T406">[BrM:] 'kuːgɨr kučilʼe' changed to 'kuːgɨrkučilʼe'.</ta>
            <ta e="T411" id="Seg_8061" s="T408">[BrM:] Tentative analysis of 'matškaŋ'.</ta>
            <ta e="T423" id="Seg_8062" s="T417">[KuAI:] Variant: 'penqɨn'.</ta>
            <ta e="T428" id="Seg_8063" s="T423">[BrM:] GEN?</ta>
            <ta e="T431" id="Seg_8064" s="T428">[KuAI:] Variant: 'qambaj'. [BrM:] Tentative analysis of 'tʼümbocʼka'.</ta>
            <ta e="T441" id="Seg_8065" s="T436">[KuAI:] Variant: 'qwärlʼew'.</ta>
            <ta e="T446" id="Seg_8066" s="T441">[BrM:] 'moʒet bɨtʼ' changed to 'moʒetbɨtʼ'. [BrM:] Tentative analysis of 'tʼülʼezeq (tüːlʼezettɨ)'.</ta>
            <ta e="T477" id="Seg_8067" s="T471">[BrM:] Tentative analysis of 'raqsonneŋ'.</ta>
            <ta e="T493" id="Seg_8068" s="T487">[KuAI:] Variant: 'püːɣälǯimbat'.</ta>
            <ta e="T496" id="Seg_8069" s="T493">[BrM:] Tentative analysis of 'püːrlaška'.</ta>
            <ta e="T512" id="Seg_8070" s="T506">[KuAI:] Variant: 'tʼäptätimbɨzattə'.</ta>
            <ta e="T518" id="Seg_8071" s="T514">[BrM:] Tentative analysis of 'ropeːss'.</ta>
            <ta e="T539" id="Seg_8072" s="T536">[BrM:] Tentative analysis of 'ropeːss'.</ta>
            <ta e="T548" id="Seg_8073" s="T542">[KuAI:] Variant: 'sejlaw'.</ta>
            <ta e="T558" id="Seg_8074" s="T552">[BrM:] 'sejlaɣɨn tʼät' – LOC?</ta>
            <ta e="T566" id="Seg_8075" s="T560">[BrM:] Tentative analysis, unclear syntax: 'nu kojmɨm taːdərɨt'.</ta>
            <ta e="T588" id="Seg_8076" s="T574">[KuAI:] Variant: 'sanǯirtsant'.</ta>
            <ta e="T601" id="Seg_8077" s="T593">[BrM:] ADJZ? 3SG? [BrM:] Tentative analysis of 'paqqɨnnau'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T73" id="Seg_8078" s="T69">kы - Чая</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T0" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
