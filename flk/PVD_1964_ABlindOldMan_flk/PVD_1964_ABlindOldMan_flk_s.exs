<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_ABlindOldMan_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_ABlindOldMan_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">135</ud-information>
            <ud-information attribute-name="# HIAT:w">93</ud-information>
            <ud-information attribute-name="# e">93</ud-information>
            <ud-information attribute-name="# HIAT:u">25</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T94" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Näjɣum</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">kɨgɨstɨ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">äramdə</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">atrawiku</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">Ugon</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">jes</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_26" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">Täbeɣum</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">sejgattə</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">aːzus</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_38" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">Pajat</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">täbɨm</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">atrawiku</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">kɨgɨstɨ</ts>
                  <nts id="Seg_50" n="HIAT:ip">.</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_53" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">Süː</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">qwattɨt</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">i</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">čʼugunse</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">parʼinät</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">paskajdʼel</ts>
                  <nts id="Seg_71" n="HIAT:ip">.</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_74" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">Otdə</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">aurnɨn</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_82" n="HIAT:w" s="T22">peqɨn</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_85" n="HIAT:w" s="T23">wadʼi</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_88" n="HIAT:w" s="T24">i</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_91" n="HIAT:w" s="T25">tʼärɨn</ts>
                  <nts id="Seg_92" n="HIAT:ip">:</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_94" n="HIAT:ip">“</nts>
                  <ts e="T27" id="Seg_96" n="HIAT:w" s="T26">Aurčennaš</ts>
                  <nts id="Seg_97" n="HIAT:ip">?</nts>
                  <nts id="Seg_98" n="HIAT:ip">”</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_101" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_103" n="HIAT:w" s="T27">A</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_106" n="HIAT:w" s="T28">täp</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_109" n="HIAT:w" s="T29">tʼärɨn</ts>
                  <nts id="Seg_110" n="HIAT:ip">:</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_112" n="HIAT:ip">“</nts>
                  <ts e="T31" id="Seg_114" n="HIAT:w" s="T30">Aurčeǯan</ts>
                  <nts id="Seg_115" n="HIAT:ip">.</nts>
                  <nts id="Seg_116" n="HIAT:ip">”</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_119" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_121" n="HIAT:w" s="T31">Täp</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_124" n="HIAT:w" s="T32">čʼugunkam</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_127" n="HIAT:w" s="T33">stol</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_130" n="HIAT:w" s="T34">barotdə</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_133" n="HIAT:w" s="T35">čečɨt</ts>
                  <nts id="Seg_134" n="HIAT:ip">.</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_137" n="HIAT:u" s="T36">
                  <nts id="Seg_138" n="HIAT:ip">“</nts>
                  <ts e="T37" id="Seg_140" n="HIAT:w" s="T36">Aurgɨ</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_143" n="HIAT:w" s="T37">čʼugunqundo</ts>
                  <nts id="Seg_144" n="HIAT:ip">.</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_147" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_149" n="HIAT:w" s="T38">Man</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_152" n="HIAT:w" s="T39">qwaǯan</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_155" n="HIAT:w" s="T40">qugurkuʒugu</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_157" n="HIAT:ip">(</nts>
                  <ts e="T42" id="Seg_159" n="HIAT:w" s="T41">qugursan</ts>
                  <nts id="Seg_160" n="HIAT:ip">)</nts>
                  <nts id="Seg_161" n="HIAT:ip">.</nts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_164" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_166" n="HIAT:w" s="T42">Tamdʼel</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_169" n="HIAT:w" s="T43">paska</ts>
                  <nts id="Seg_170" n="HIAT:ip">.</nts>
                  <nts id="Seg_171" n="HIAT:ip">”</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_174" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_176" n="HIAT:w" s="T44">Täbeɣum</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_179" n="HIAT:w" s="T45">omdɨn</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_182" n="HIAT:w" s="T46">aurgu</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_185" n="HIAT:w" s="T47">päːrčin</ts>
                  <nts id="Seg_186" n="HIAT:ip">.</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_189" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_191" n="HIAT:w" s="T48">İ</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_194" n="HIAT:w" s="T49">krɨškam</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_197" n="HIAT:w" s="T50">nʼüːt</ts>
                  <nts id="Seg_198" n="HIAT:ip">,</nts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_201" n="HIAT:w" s="T51">natʼet</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_204" n="HIAT:w" s="T52">mannɨpa</ts>
                  <nts id="Seg_205" n="HIAT:ip">.</nts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_208" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_210" n="HIAT:w" s="T53">İ</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_213" n="HIAT:w" s="T54">sejlat</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_216" n="HIAT:w" s="T55">nügɨdʼättə</ts>
                  <nts id="Seg_217" n="HIAT:ip">.</nts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_220" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_222" n="HIAT:w" s="T56">Mannɨpa</ts>
                  <nts id="Seg_223" n="HIAT:ip">,</nts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_226" n="HIAT:w" s="T57">natʼen</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_229" n="HIAT:w" s="T58">sü</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_232" n="HIAT:w" s="T59">ippɨtda</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_235" n="HIAT:w" s="T60">čʼugunkaɣɨn</ts>
                  <nts id="Seg_236" n="HIAT:ip">.</nts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_239" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_241" n="HIAT:w" s="T61">Täp</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_244" n="HIAT:w" s="T62">ponä</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_247" n="HIAT:w" s="T63">čaːǯɨn</ts>
                  <nts id="Seg_248" n="HIAT:ip">.</nts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_251" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_253" n="HIAT:w" s="T64">Pajat</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_256" n="HIAT:w" s="T65">koːgurkuʒun</ts>
                  <nts id="Seg_257" n="HIAT:ip">.</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_260" n="HIAT:u" s="T66">
                  <nts id="Seg_261" n="HIAT:ip">“</nts>
                  <ts e="T67" id="Seg_263" n="HIAT:w" s="T66">Man</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_266" n="HIAT:w" s="T67">tʼäran</ts>
                  <nts id="Seg_267" n="HIAT:ip">,</nts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_270" n="HIAT:w" s="T68">qaj</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_273" n="HIAT:w" s="T69">kogurkuʒatt</ts>
                  <nts id="Seg_274" n="HIAT:ip">?</nts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_277" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_279" n="HIAT:w" s="T70">A</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_282" n="HIAT:w" s="T71">man</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_285" n="HIAT:w" s="T72">tastɨ</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_288" n="HIAT:w" s="T73">qoǯirnan</ts>
                  <nts id="Seg_289" n="HIAT:ip">.</nts>
                  <nts id="Seg_290" n="HIAT:ip">”</nts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_293" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_295" n="HIAT:w" s="T74">A</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_298" n="HIAT:w" s="T75">pajat</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_301" n="HIAT:w" s="T76">kɨcʼiwannɨn</ts>
                  <nts id="Seg_302" n="HIAT:ip">.</nts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_305" n="HIAT:u" s="T77">
                  <nts id="Seg_306" n="HIAT:ip">“</nts>
                  <ts e="T78" id="Seg_308" n="HIAT:w" s="T77">İgə</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_311" n="HIAT:w" s="T78">larɨpbaq</ts>
                  <nts id="Seg_312" n="HIAT:ip">.</nts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_315" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_317" n="HIAT:w" s="T79">Man</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_320" n="HIAT:w" s="T80">tastɨ</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_323" n="HIAT:w" s="T81">ass</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_326" n="HIAT:w" s="T82">qwatšan</ts>
                  <nts id="Seg_327" n="HIAT:ip">.</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_330" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_332" n="HIAT:w" s="T83">Täper</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_335" n="HIAT:w" s="T84">ilʼeǯaj</ts>
                  <nts id="Seg_336" n="HIAT:ip">.</nts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_339" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_341" n="HIAT:w" s="T85">Soːn</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_344" n="HIAT:w" s="T86">što</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_347" n="HIAT:w" s="T87">mazɨm</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_350" n="HIAT:w" s="T88">sejzʼe</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_353" n="HIAT:w" s="T89">mewat</ts>
                  <nts id="Seg_354" n="HIAT:ip">.</nts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_357" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_359" n="HIAT:w" s="T90">Teper</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_362" n="HIAT:w" s="T91">man</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_365" n="HIAT:w" s="T92">sejlau</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_368" n="HIAT:w" s="T93">aːduatt</ts>
                  <nts id="Seg_369" n="HIAT:ip">.</nts>
                  <nts id="Seg_370" n="HIAT:ip">”</nts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T94" id="Seg_372" n="sc" s="T1">
               <ts e="T2" id="Seg_374" n="e" s="T1">Näjɣum </ts>
               <ts e="T3" id="Seg_376" n="e" s="T2">kɨgɨstɨ </ts>
               <ts e="T4" id="Seg_378" n="e" s="T3">äramdə </ts>
               <ts e="T5" id="Seg_380" n="e" s="T4">atrawiku. </ts>
               <ts e="T6" id="Seg_382" n="e" s="T5">Ugon </ts>
               <ts e="T7" id="Seg_384" n="e" s="T6">jes. </ts>
               <ts e="T8" id="Seg_386" n="e" s="T7">Täbeɣum </ts>
               <ts e="T9" id="Seg_388" n="e" s="T8">sejgattə </ts>
               <ts e="T10" id="Seg_390" n="e" s="T9">aːzus. </ts>
               <ts e="T11" id="Seg_392" n="e" s="T10">Pajat </ts>
               <ts e="T12" id="Seg_394" n="e" s="T11">täbɨm </ts>
               <ts e="T13" id="Seg_396" n="e" s="T12">atrawiku </ts>
               <ts e="T14" id="Seg_398" n="e" s="T13">kɨgɨstɨ. </ts>
               <ts e="T15" id="Seg_400" n="e" s="T14">Süː </ts>
               <ts e="T16" id="Seg_402" n="e" s="T15">qwattɨt </ts>
               <ts e="T17" id="Seg_404" n="e" s="T16">i </ts>
               <ts e="T18" id="Seg_406" n="e" s="T17">čʼugunse </ts>
               <ts e="T19" id="Seg_408" n="e" s="T18">parʼinät </ts>
               <ts e="T20" id="Seg_410" n="e" s="T19">paskajdʼel. </ts>
               <ts e="T21" id="Seg_412" n="e" s="T20">Otdə </ts>
               <ts e="T22" id="Seg_414" n="e" s="T21">aurnɨn </ts>
               <ts e="T23" id="Seg_416" n="e" s="T22">peqɨn </ts>
               <ts e="T24" id="Seg_418" n="e" s="T23">wadʼi </ts>
               <ts e="T25" id="Seg_420" n="e" s="T24">i </ts>
               <ts e="T26" id="Seg_422" n="e" s="T25">tʼärɨn: </ts>
               <ts e="T27" id="Seg_424" n="e" s="T26">“Aurčennaš?” </ts>
               <ts e="T28" id="Seg_426" n="e" s="T27">A </ts>
               <ts e="T29" id="Seg_428" n="e" s="T28">täp </ts>
               <ts e="T30" id="Seg_430" n="e" s="T29">tʼärɨn: </ts>
               <ts e="T31" id="Seg_432" n="e" s="T30">“Aurčeǯan.” </ts>
               <ts e="T32" id="Seg_434" n="e" s="T31">Täp </ts>
               <ts e="T33" id="Seg_436" n="e" s="T32">čʼugunkam </ts>
               <ts e="T34" id="Seg_438" n="e" s="T33">stol </ts>
               <ts e="T35" id="Seg_440" n="e" s="T34">barotdə </ts>
               <ts e="T36" id="Seg_442" n="e" s="T35">čečɨt. </ts>
               <ts e="T37" id="Seg_444" n="e" s="T36">“Aurgɨ </ts>
               <ts e="T38" id="Seg_446" n="e" s="T37">čʼugunqundo. </ts>
               <ts e="T39" id="Seg_448" n="e" s="T38">Man </ts>
               <ts e="T40" id="Seg_450" n="e" s="T39">qwaǯan </ts>
               <ts e="T41" id="Seg_452" n="e" s="T40">qugurkuʒugu </ts>
               <ts e="T42" id="Seg_454" n="e" s="T41">(qugursan). </ts>
               <ts e="T43" id="Seg_456" n="e" s="T42">Tamdʼel </ts>
               <ts e="T44" id="Seg_458" n="e" s="T43">paska.” </ts>
               <ts e="T45" id="Seg_460" n="e" s="T44">Täbeɣum </ts>
               <ts e="T46" id="Seg_462" n="e" s="T45">omdɨn </ts>
               <ts e="T47" id="Seg_464" n="e" s="T46">aurgu </ts>
               <ts e="T48" id="Seg_466" n="e" s="T47">päːrčin. </ts>
               <ts e="T49" id="Seg_468" n="e" s="T48">İ </ts>
               <ts e="T50" id="Seg_470" n="e" s="T49">krɨškam </ts>
               <ts e="T51" id="Seg_472" n="e" s="T50">nʼüːt, </ts>
               <ts e="T52" id="Seg_474" n="e" s="T51">natʼet </ts>
               <ts e="T53" id="Seg_476" n="e" s="T52">mannɨpa. </ts>
               <ts e="T54" id="Seg_478" n="e" s="T53">İ </ts>
               <ts e="T55" id="Seg_480" n="e" s="T54">sejlat </ts>
               <ts e="T56" id="Seg_482" n="e" s="T55">nügɨdʼättə. </ts>
               <ts e="T57" id="Seg_484" n="e" s="T56">Mannɨpa, </ts>
               <ts e="T58" id="Seg_486" n="e" s="T57">natʼen </ts>
               <ts e="T59" id="Seg_488" n="e" s="T58">sü </ts>
               <ts e="T60" id="Seg_490" n="e" s="T59">ippɨtda </ts>
               <ts e="T61" id="Seg_492" n="e" s="T60">čʼugunkaɣɨn. </ts>
               <ts e="T62" id="Seg_494" n="e" s="T61">Täp </ts>
               <ts e="T63" id="Seg_496" n="e" s="T62">ponä </ts>
               <ts e="T64" id="Seg_498" n="e" s="T63">čaːǯɨn. </ts>
               <ts e="T65" id="Seg_500" n="e" s="T64">Pajat </ts>
               <ts e="T66" id="Seg_502" n="e" s="T65">koːgurkuʒun. </ts>
               <ts e="T67" id="Seg_504" n="e" s="T66">“Man </ts>
               <ts e="T68" id="Seg_506" n="e" s="T67">tʼäran, </ts>
               <ts e="T69" id="Seg_508" n="e" s="T68">qaj </ts>
               <ts e="T70" id="Seg_510" n="e" s="T69">kogurkuʒatt? </ts>
               <ts e="T71" id="Seg_512" n="e" s="T70">A </ts>
               <ts e="T72" id="Seg_514" n="e" s="T71">man </ts>
               <ts e="T73" id="Seg_516" n="e" s="T72">tastɨ </ts>
               <ts e="T74" id="Seg_518" n="e" s="T73">qoǯirnan.” </ts>
               <ts e="T75" id="Seg_520" n="e" s="T74">A </ts>
               <ts e="T76" id="Seg_522" n="e" s="T75">pajat </ts>
               <ts e="T77" id="Seg_524" n="e" s="T76">kɨcʼiwannɨn. </ts>
               <ts e="T78" id="Seg_526" n="e" s="T77">“İgə </ts>
               <ts e="T79" id="Seg_528" n="e" s="T78">larɨpbaq. </ts>
               <ts e="T80" id="Seg_530" n="e" s="T79">Man </ts>
               <ts e="T81" id="Seg_532" n="e" s="T80">tastɨ </ts>
               <ts e="T82" id="Seg_534" n="e" s="T81">ass </ts>
               <ts e="T83" id="Seg_536" n="e" s="T82">qwatšan. </ts>
               <ts e="T84" id="Seg_538" n="e" s="T83">Täper </ts>
               <ts e="T85" id="Seg_540" n="e" s="T84">ilʼeǯaj. </ts>
               <ts e="T86" id="Seg_542" n="e" s="T85">Soːn </ts>
               <ts e="T87" id="Seg_544" n="e" s="T86">što </ts>
               <ts e="T88" id="Seg_546" n="e" s="T87">mazɨm </ts>
               <ts e="T89" id="Seg_548" n="e" s="T88">sejzʼe </ts>
               <ts e="T90" id="Seg_550" n="e" s="T89">mewat. </ts>
               <ts e="T91" id="Seg_552" n="e" s="T90">Teper </ts>
               <ts e="T92" id="Seg_554" n="e" s="T91">man </ts>
               <ts e="T93" id="Seg_556" n="e" s="T92">sejlau </ts>
               <ts e="T94" id="Seg_558" n="e" s="T93">aːduatt.” </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_559" s="T1">PVD_1964_ABlindOldMan_flk.001 (001.001)</ta>
            <ta e="T7" id="Seg_560" s="T5">PVD_1964_ABlindOldMan_flk.002 (001.002)</ta>
            <ta e="T10" id="Seg_561" s="T7">PVD_1964_ABlindOldMan_flk.003 (001.003)</ta>
            <ta e="T14" id="Seg_562" s="T10">PVD_1964_ABlindOldMan_flk.004 (001.004)</ta>
            <ta e="T20" id="Seg_563" s="T14">PVD_1964_ABlindOldMan_flk.005 (001.005)</ta>
            <ta e="T27" id="Seg_564" s="T20">PVD_1964_ABlindOldMan_flk.006 (001.006)</ta>
            <ta e="T31" id="Seg_565" s="T27">PVD_1964_ABlindOldMan_flk.007 (001.007)</ta>
            <ta e="T36" id="Seg_566" s="T31">PVD_1964_ABlindOldMan_flk.008 (001.008)</ta>
            <ta e="T38" id="Seg_567" s="T36">PVD_1964_ABlindOldMan_flk.009 (001.009)</ta>
            <ta e="T42" id="Seg_568" s="T38">PVD_1964_ABlindOldMan_flk.010 (001.010)</ta>
            <ta e="T44" id="Seg_569" s="T42">PVD_1964_ABlindOldMan_flk.011 (001.011)</ta>
            <ta e="T48" id="Seg_570" s="T44">PVD_1964_ABlindOldMan_flk.012 (001.012)</ta>
            <ta e="T53" id="Seg_571" s="T48">PVD_1964_ABlindOldMan_flk.013 (001.013)</ta>
            <ta e="T56" id="Seg_572" s="T53">PVD_1964_ABlindOldMan_flk.014 (001.014)</ta>
            <ta e="T61" id="Seg_573" s="T56">PVD_1964_ABlindOldMan_flk.015 (001.015)</ta>
            <ta e="T64" id="Seg_574" s="T61">PVD_1964_ABlindOldMan_flk.016 (001.016)</ta>
            <ta e="T66" id="Seg_575" s="T64">PVD_1964_ABlindOldMan_flk.017 (001.017)</ta>
            <ta e="T70" id="Seg_576" s="T66">PVD_1964_ABlindOldMan_flk.018 (001.018)</ta>
            <ta e="T74" id="Seg_577" s="T70">PVD_1964_ABlindOldMan_flk.019 (001.019)</ta>
            <ta e="T77" id="Seg_578" s="T74">PVD_1964_ABlindOldMan_flk.020 (001.020)</ta>
            <ta e="T79" id="Seg_579" s="T77">PVD_1964_ABlindOldMan_flk.021 (001.021)</ta>
            <ta e="T83" id="Seg_580" s="T79">PVD_1964_ABlindOldMan_flk.022 (001.022)</ta>
            <ta e="T85" id="Seg_581" s="T83">PVD_1964_ABlindOldMan_flk.023 (001.023)</ta>
            <ta e="T90" id="Seg_582" s="T85">PVD_1964_ABlindOldMan_flk.024 (001.024)</ta>
            <ta e="T94" id="Seg_583" s="T90">PVD_1964_ABlindOldMan_flk.025 (001.025)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_584" s="T1">′нӓйɣум ′кыгысты ӓ′рамдъ а′травику.</ta>
            <ta e="T7" id="Seg_585" s="T5">у′гон jес.</ta>
            <ta e="T10" id="Seg_586" s="T7">тӓ′беɣ(k,Г)ум сей′гаттъ ′а̄зус.</ta>
            <ta e="T14" id="Seg_587" s="T10">па′jат тӓ′бым атра′вику ′кыгысты.</ta>
            <ta e="T20" id="Seg_588" s="T14">сӱ̄ ′kwаттыт и чу′гунсе ′парʼинӓт ′паскайдʼел.</ta>
            <ta e="T27" id="Seg_589" s="T20">отдъ ′аурнын ′пеkын ′вадʼи и тʼӓ′рын: аур′тшеннаш?</ta>
            <ta e="T31" id="Seg_590" s="T27">а тӓп тʼӓ′рын: аур′тшеджан.</ta>
            <ta e="T36" id="Seg_591" s="T31">тӓп чу′гункам ′стол ба‵ротдъ ′тше̨тшыт.</ta>
            <ta e="T38" id="Seg_592" s="T36">аур′гы чу′гунkунд̂о.</ta>
            <ta e="T42" id="Seg_593" s="T38">ман kwа′джан ′kугур‵кужугу (kугур′сан).</ta>
            <ta e="T44" id="Seg_594" s="T42">там′дʼел ′паска.</ta>
            <ta e="T48" id="Seg_595" s="T44">тӓ′беɣум ′омдын ′аургу ′пӓ̄ртшин.</ta>
            <ta e="T53" id="Seg_596" s="T48">и крышкам нʼӱ̄т, на′тʼет ′манныпа.</ta>
            <ta e="T56" id="Seg_597" s="T53">и ′сейлат ′нӱгыдʼӓттъ.</ta>
            <ta e="T61" id="Seg_598" s="T56">манны′п(б̂)а, на′тʼен сӱ ′иппытда чу′гункаɣын.</ta>
            <ta e="T64" id="Seg_599" s="T61">тӓп ′понӓ ′тша̄джын.</ta>
            <ta e="T66" id="Seg_600" s="T64">па′jат ′ко̄гурку‵жун.</ta>
            <ta e="T70" id="Seg_601" s="T66">ман тʼӓ′ран: kай ′когуркужатт?</ta>
            <ta e="T74" id="Seg_602" s="T70">а ман тас′ты ′kоджирнан.</ta>
            <ta e="T77" id="Seg_603" s="T74">а па′jат кыцʼи′ваннын.</ta>
            <ta e="T79" id="Seg_604" s="T77">′игъ ′ларып‵баk.</ta>
            <ta e="T83" id="Seg_605" s="T79">ман тас′ты асс kwа′тшан.</ta>
            <ta e="T85" id="Seg_606" s="T83">тӓ′пер и′лʼеджай.</ta>
            <ta e="T90" id="Seg_607" s="T85">со̄н што ма′зым сей′зʼе ′меват.</ta>
            <ta e="T94" id="Seg_608" s="T90">те′пер ман сей′лау а̄ду′атт.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_609" s="T1">näjɣum kɨgɨstɨ äramdə atrawiku.</ta>
            <ta e="T7" id="Seg_610" s="T5">ugon jes.</ta>
            <ta e="T10" id="Seg_611" s="T7">täbeɣ(q,Г)um sejgattə aːzus.</ta>
            <ta e="T14" id="Seg_612" s="T10">pajat täbɨm atrawiku kɨgɨstɨ.</ta>
            <ta e="T20" id="Seg_613" s="T14">süː qwattɨt i čugunse parʼinät paskajdʼel.</ta>
            <ta e="T27" id="Seg_614" s="T20">otdə aurnɨn peqɨn wadʼi i tʼärɨn: aurtšennaš?</ta>
            <ta e="T31" id="Seg_615" s="T27">a täp tʼärɨn: aurtšeǯan.</ta>
            <ta e="T36" id="Seg_616" s="T31">täp čugunkam stol barotdə tšetšɨt.</ta>
            <ta e="T38" id="Seg_617" s="T36">aurgɨ čugunqund̂o.</ta>
            <ta e="T42" id="Seg_618" s="T38">man qwaǯan qugurkuʒugu (qugursan).</ta>
            <ta e="T44" id="Seg_619" s="T42">tamdʼel paska.</ta>
            <ta e="T48" id="Seg_620" s="T44">täbeɣum omdɨn aurgu päːrtšin.</ta>
            <ta e="T53" id="Seg_621" s="T48">i krɨškam nʼüːt, natʼet mannɨpa.</ta>
            <ta e="T56" id="Seg_622" s="T53">i sejlat nügɨdʼättə.</ta>
            <ta e="T61" id="Seg_623" s="T56">mannɨp(b̂)a, natʼen sü ippɨtda čugunkaɣɨn.</ta>
            <ta e="T64" id="Seg_624" s="T61">täp ponä tšaːǯɨn.</ta>
            <ta e="T66" id="Seg_625" s="T64">pajat koːgurkuʒun.</ta>
            <ta e="T70" id="Seg_626" s="T66">man tʼäran: qaj kogurkuʒatt?</ta>
            <ta e="T74" id="Seg_627" s="T70">a man tastɨ qoǯirnan.</ta>
            <ta e="T77" id="Seg_628" s="T74">a pajat kɨcʼiwannɨn.</ta>
            <ta e="T79" id="Seg_629" s="T77">igə larɨpbaq.</ta>
            <ta e="T83" id="Seg_630" s="T79">man tastɨ ass qwatšan.</ta>
            <ta e="T85" id="Seg_631" s="T83">täper ilʼeǯaj.</ta>
            <ta e="T90" id="Seg_632" s="T85">soːn što mazɨm sejzʼe mewat.</ta>
            <ta e="T94" id="Seg_633" s="T90">teper man sejlau aːduatt.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_634" s="T1">Näjɣum kɨgɨstɨ äramdə atrawiku. </ta>
            <ta e="T7" id="Seg_635" s="T5">Ugon jes. </ta>
            <ta e="T10" id="Seg_636" s="T7">Täbeɣum sejgattə aːzus. </ta>
            <ta e="T14" id="Seg_637" s="T10">Pajat täbɨm atrawiku kɨgɨstɨ. </ta>
            <ta e="T20" id="Seg_638" s="T14">Süː qwattɨt i čʼugunse parʼinät paskajdʼel. </ta>
            <ta e="T27" id="Seg_639" s="T20">Otdə aurnɨn peqɨn wadʼi i tʼärɨn: “Aurčennaš?” </ta>
            <ta e="T31" id="Seg_640" s="T27">A täp tʼärɨn: “Aurčeǯan.” </ta>
            <ta e="T36" id="Seg_641" s="T31">Täp čʼugunkam stol barotdə čečɨt. </ta>
            <ta e="T38" id="Seg_642" s="T36">“Aurgɨ čʼugunqundo. </ta>
            <ta e="T42" id="Seg_643" s="T38">Man qwaǯan qugurkuʒugu (qugursan). </ta>
            <ta e="T44" id="Seg_644" s="T42">Tamdʼel paska.” </ta>
            <ta e="T48" id="Seg_645" s="T44">Täbeɣum omdɨn aurgu päːrčin. </ta>
            <ta e="T53" id="Seg_646" s="T48">İ krɨškam nʼüːt, natʼet mannɨpa. </ta>
            <ta e="T56" id="Seg_647" s="T53">İ sejlat nügɨdʼättə. </ta>
            <ta e="T61" id="Seg_648" s="T56">Mannɨpa, natʼen sü ippɨtda čʼugunkaɣɨn. </ta>
            <ta e="T64" id="Seg_649" s="T61">Täp ponä čaːǯɨn. </ta>
            <ta e="T66" id="Seg_650" s="T64">Pajat koːgurkuʒun. </ta>
            <ta e="T70" id="Seg_651" s="T66">“Man tʼäran, qaj kogurkuʒatt? </ta>
            <ta e="T74" id="Seg_652" s="T70">A man tastɨ qoǯirnan.” </ta>
            <ta e="T77" id="Seg_653" s="T74">A pajat kɨcʼiwannɨn. </ta>
            <ta e="T79" id="Seg_654" s="T77">“İgə larɨpbaq. </ta>
            <ta e="T83" id="Seg_655" s="T79">Man tastɨ ass qwatšan. </ta>
            <ta e="T85" id="Seg_656" s="T83">Täper ilʼeǯaj. </ta>
            <ta e="T90" id="Seg_657" s="T85">Soːn što mazɨm sejzʼe mewat. </ta>
            <ta e="T94" id="Seg_658" s="T90">Teper man sejlau aːduatt.” </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_659" s="T1">nä-j-ɣum</ta>
            <ta e="T3" id="Seg_660" s="T2">kɨgɨ-s-tɨ</ta>
            <ta e="T4" id="Seg_661" s="T3">ära-m-də</ta>
            <ta e="T5" id="Seg_662" s="T4">atrawi-ku</ta>
            <ta e="T6" id="Seg_663" s="T5">ugon</ta>
            <ta e="T7" id="Seg_664" s="T6">je-s</ta>
            <ta e="T8" id="Seg_665" s="T7">täbe-ɣum</ta>
            <ta e="T9" id="Seg_666" s="T8">sej-gattə</ta>
            <ta e="T10" id="Seg_667" s="T9">aːzu-s</ta>
            <ta e="T11" id="Seg_668" s="T10">paja-t</ta>
            <ta e="T12" id="Seg_669" s="T11">täb-ɨ-m</ta>
            <ta e="T13" id="Seg_670" s="T12">atrawi-ku</ta>
            <ta e="T14" id="Seg_671" s="T13">kɨgɨ-s-tɨ</ta>
            <ta e="T15" id="Seg_672" s="T14">süː</ta>
            <ta e="T16" id="Seg_673" s="T15">qwat-tɨ-t</ta>
            <ta e="T17" id="Seg_674" s="T16">i</ta>
            <ta e="T18" id="Seg_675" s="T17">čʼugun-se</ta>
            <ta e="T19" id="Seg_676" s="T18">parʼin-ä-t</ta>
            <ta e="T20" id="Seg_677" s="T19">paska-j-dʼel</ta>
            <ta e="T21" id="Seg_678" s="T20">otdə</ta>
            <ta e="T22" id="Seg_679" s="T21">au-r-nɨ-n</ta>
            <ta e="T23" id="Seg_680" s="T22">peq-ɨ-n</ta>
            <ta e="T24" id="Seg_681" s="T23">wadʼi</ta>
            <ta e="T25" id="Seg_682" s="T24">i</ta>
            <ta e="T26" id="Seg_683" s="T25">tʼärɨ-n</ta>
            <ta e="T27" id="Seg_684" s="T26">au-r-če-nnaš</ta>
            <ta e="T28" id="Seg_685" s="T27">a</ta>
            <ta e="T29" id="Seg_686" s="T28">täp</ta>
            <ta e="T30" id="Seg_687" s="T29">tʼärɨ-n</ta>
            <ta e="T31" id="Seg_688" s="T30">au-r-č-eǯa-n</ta>
            <ta e="T32" id="Seg_689" s="T31">täp</ta>
            <ta e="T33" id="Seg_690" s="T32">čʼugun-ka-m</ta>
            <ta e="T34" id="Seg_691" s="T33">stol</ta>
            <ta e="T35" id="Seg_692" s="T34">bar-o-tdə</ta>
            <ta e="T36" id="Seg_693" s="T35">čečɨ-t</ta>
            <ta e="T37" id="Seg_694" s="T36">au-r-gɨ</ta>
            <ta e="T38" id="Seg_695" s="T37">čʼugun-qundo</ta>
            <ta e="T39" id="Seg_696" s="T38">man</ta>
            <ta e="T40" id="Seg_697" s="T39">qwa-ǯa-n</ta>
            <ta e="T41" id="Seg_698" s="T40">qugur-ku-ʒu-gu</ta>
            <ta e="T42" id="Seg_699" s="T41">qugur-san</ta>
            <ta e="T43" id="Seg_700" s="T42">tam-dʼel</ta>
            <ta e="T44" id="Seg_701" s="T43">paska</ta>
            <ta e="T45" id="Seg_702" s="T44">täbe-ɣum</ta>
            <ta e="T46" id="Seg_703" s="T45">omdɨ-n</ta>
            <ta e="T47" id="Seg_704" s="T46">au-r-gu</ta>
            <ta e="T48" id="Seg_705" s="T47">päːrčin</ta>
            <ta e="T49" id="Seg_706" s="T48">i</ta>
            <ta e="T50" id="Seg_707" s="T49">krɨška-m</ta>
            <ta e="T51" id="Seg_708" s="T50">nʼüː-t</ta>
            <ta e="T52" id="Seg_709" s="T51">natʼe-t</ta>
            <ta e="T53" id="Seg_710" s="T52">mannɨ-pa</ta>
            <ta e="T54" id="Seg_711" s="T53">i</ta>
            <ta e="T55" id="Seg_712" s="T54">sej-la-t</ta>
            <ta e="T56" id="Seg_713" s="T55">nü-gɨ-dʼä-ttə</ta>
            <ta e="T57" id="Seg_714" s="T56">mannɨ-pa</ta>
            <ta e="T58" id="Seg_715" s="T57">natʼe-n</ta>
            <ta e="T59" id="Seg_716" s="T58">sü</ta>
            <ta e="T60" id="Seg_717" s="T59">ippɨ-tda</ta>
            <ta e="T61" id="Seg_718" s="T60">čʼugun-ka-ɣɨn</ta>
            <ta e="T62" id="Seg_719" s="T61">täp</ta>
            <ta e="T63" id="Seg_720" s="T62">ponä</ta>
            <ta e="T64" id="Seg_721" s="T63">čaːǯɨ-n</ta>
            <ta e="T65" id="Seg_722" s="T64">paja-t</ta>
            <ta e="T66" id="Seg_723" s="T65">koːgur-ku-ʒu-n</ta>
            <ta e="T67" id="Seg_724" s="T66">man</ta>
            <ta e="T68" id="Seg_725" s="T67">tʼära-n</ta>
            <ta e="T69" id="Seg_726" s="T68">qaj</ta>
            <ta e="T70" id="Seg_727" s="T69">kogur-ku-ʒa-tt</ta>
            <ta e="T71" id="Seg_728" s="T70">a</ta>
            <ta e="T72" id="Seg_729" s="T71">man</ta>
            <ta e="T73" id="Seg_730" s="T72">tastɨ</ta>
            <ta e="T74" id="Seg_731" s="T73">qo-ǯir-na-n</ta>
            <ta e="T75" id="Seg_732" s="T74">a</ta>
            <ta e="T76" id="Seg_733" s="T75">paja-t</ta>
            <ta e="T77" id="Seg_734" s="T76">kɨcʼiwan-nɨ-n</ta>
            <ta e="T78" id="Seg_735" s="T77">igə</ta>
            <ta e="T79" id="Seg_736" s="T78">larɨ-pba-q</ta>
            <ta e="T80" id="Seg_737" s="T79">man</ta>
            <ta e="T81" id="Seg_738" s="T80">tastɨ</ta>
            <ta e="T82" id="Seg_739" s="T81">ass</ta>
            <ta e="T83" id="Seg_740" s="T82">qwat-ša-n</ta>
            <ta e="T84" id="Seg_741" s="T83">täper</ta>
            <ta e="T85" id="Seg_742" s="T84">ilʼ-eǯa-j</ta>
            <ta e="T86" id="Seg_743" s="T85">soːn</ta>
            <ta e="T87" id="Seg_744" s="T86">što</ta>
            <ta e="T88" id="Seg_745" s="T87">mazɨm</ta>
            <ta e="T89" id="Seg_746" s="T88">sej-zʼe</ta>
            <ta e="T90" id="Seg_747" s="T89">me-wa-t</ta>
            <ta e="T91" id="Seg_748" s="T90">teper</ta>
            <ta e="T92" id="Seg_749" s="T91">man</ta>
            <ta e="T93" id="Seg_750" s="T92">sej-la-u</ta>
            <ta e="T94" id="Seg_751" s="T93">aːdu-a-tt</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_752" s="T1">ne-lʼ-qum</ta>
            <ta e="T3" id="Seg_753" s="T2">kɨgɨ-sɨ-t</ta>
            <ta e="T4" id="Seg_754" s="T3">era-m-tə</ta>
            <ta e="T5" id="Seg_755" s="T4">atrawi-gu</ta>
            <ta e="T6" id="Seg_756" s="T5">ugon</ta>
            <ta e="T7" id="Seg_757" s="T6">eː-sɨ</ta>
            <ta e="T8" id="Seg_758" s="T7">täbe-qum</ta>
            <ta e="T9" id="Seg_759" s="T8">sej-kattə</ta>
            <ta e="T10" id="Seg_760" s="T9">azu-sɨ</ta>
            <ta e="T11" id="Seg_761" s="T10">paja-tə</ta>
            <ta e="T12" id="Seg_762" s="T11">täp-ɨ-m</ta>
            <ta e="T13" id="Seg_763" s="T12">atrawi-gu</ta>
            <ta e="T14" id="Seg_764" s="T13">kɨgɨ-sɨ-t</ta>
            <ta e="T15" id="Seg_765" s="T14">süː</ta>
            <ta e="T16" id="Seg_766" s="T15">qwat-tɨ-t</ta>
            <ta e="T17" id="Seg_767" s="T16">i</ta>
            <ta e="T18" id="Seg_768" s="T17">čʼugun-se</ta>
            <ta e="T19" id="Seg_769" s="T18">parəŋ-nɨ-t</ta>
            <ta e="T20" id="Seg_770" s="T19">paska-lʼ-dʼel</ta>
            <ta e="T21" id="Seg_771" s="T20">ondə</ta>
            <ta e="T22" id="Seg_772" s="T21">am-r-nɨ-n</ta>
            <ta e="T23" id="Seg_773" s="T22">peq-ɨ-n</ta>
            <ta e="T24" id="Seg_774" s="T23">wadʼi</ta>
            <ta e="T25" id="Seg_775" s="T24">i</ta>
            <ta e="T26" id="Seg_776" s="T25">tʼärɨ-n</ta>
            <ta e="T27" id="Seg_777" s="T26">am-r-če-naš</ta>
            <ta e="T28" id="Seg_778" s="T27">a</ta>
            <ta e="T29" id="Seg_779" s="T28">täp</ta>
            <ta e="T30" id="Seg_780" s="T29">tʼärɨ-n</ta>
            <ta e="T31" id="Seg_781" s="T30">am-r-ču-enǯɨ-ŋ</ta>
            <ta e="T32" id="Seg_782" s="T31">täp</ta>
            <ta e="T33" id="Seg_783" s="T32">čʼugun-ka-m</ta>
            <ta e="T34" id="Seg_784" s="T33">istol</ta>
            <ta e="T35" id="Seg_785" s="T34">par-ɨ-ntə</ta>
            <ta e="T36" id="Seg_786" s="T35">čeččɨ-t</ta>
            <ta e="T37" id="Seg_787" s="T36">am-r-kɨ</ta>
            <ta e="T38" id="Seg_788" s="T37">čʼugun-qɨntɨ</ta>
            <ta e="T39" id="Seg_789" s="T38">man</ta>
            <ta e="T40" id="Seg_790" s="T39">qwan-enǯɨ-ŋ</ta>
            <ta e="T41" id="Seg_791" s="T40">qugur-kɨ-ʒu-gu</ta>
            <ta e="T42" id="Seg_792" s="T41">qugur-san</ta>
            <ta e="T43" id="Seg_793" s="T42">taw-dʼel</ta>
            <ta e="T44" id="Seg_794" s="T43">paska</ta>
            <ta e="T45" id="Seg_795" s="T44">täbe-qum</ta>
            <ta e="T46" id="Seg_796" s="T45">omdɨ-n</ta>
            <ta e="T47" id="Seg_797" s="T46">am-r-gu</ta>
            <ta e="T48" id="Seg_798" s="T47">pečan</ta>
            <ta e="T49" id="Seg_799" s="T48">i</ta>
            <ta e="T50" id="Seg_800" s="T49">krɨška-m</ta>
            <ta e="T51" id="Seg_801" s="T50">nʼuː-t</ta>
            <ta e="T52" id="Seg_802" s="T51">*natʼe-n</ta>
            <ta e="T53" id="Seg_803" s="T52">*mantɨ-mbɨ</ta>
            <ta e="T54" id="Seg_804" s="T53">i</ta>
            <ta e="T55" id="Seg_805" s="T54">sej-la-tə</ta>
            <ta e="T56" id="Seg_806" s="T55">nʼuː-kɨ-dʼi-tɨn</ta>
            <ta e="T57" id="Seg_807" s="T56">*mantɨ-mbɨ</ta>
            <ta e="T58" id="Seg_808" s="T57">*natʼe-n</ta>
            <ta e="T59" id="Seg_809" s="T58">süː</ta>
            <ta e="T60" id="Seg_810" s="T59">ippɨ-ntɨ</ta>
            <ta e="T61" id="Seg_811" s="T60">čʼugun-ka-qɨn</ta>
            <ta e="T62" id="Seg_812" s="T61">täp</ta>
            <ta e="T63" id="Seg_813" s="T62">poːne</ta>
            <ta e="T64" id="Seg_814" s="T63">čaǯɨ-n</ta>
            <ta e="T65" id="Seg_815" s="T64">paja-tə</ta>
            <ta e="T66" id="Seg_816" s="T65">qugur-kɨ-ʒu-n</ta>
            <ta e="T67" id="Seg_817" s="T66">man</ta>
            <ta e="T68" id="Seg_818" s="T67">tʼärɨ-ŋ</ta>
            <ta e="T69" id="Seg_819" s="T68">qaj</ta>
            <ta e="T70" id="Seg_820" s="T69">qugur-kɨ-ʒu-ntə</ta>
            <ta e="T71" id="Seg_821" s="T70">a</ta>
            <ta e="T72" id="Seg_822" s="T71">man</ta>
            <ta e="T73" id="Seg_823" s="T72">tastɨ</ta>
            <ta e="T74" id="Seg_824" s="T73">qo-nǯir-nɨ-ŋ</ta>
            <ta e="T75" id="Seg_825" s="T74">a</ta>
            <ta e="T76" id="Seg_826" s="T75">paja-tə</ta>
            <ta e="T77" id="Seg_827" s="T76">kɨcʼwat-nɨ-n</ta>
            <ta e="T78" id="Seg_828" s="T77">igə</ta>
            <ta e="T79" id="Seg_829" s="T78">*larɨ-mbɨ-kɨ</ta>
            <ta e="T80" id="Seg_830" s="T79">man</ta>
            <ta e="T81" id="Seg_831" s="T80">tastɨ</ta>
            <ta e="T82" id="Seg_832" s="T81">asa</ta>
            <ta e="T83" id="Seg_833" s="T82">qwat-enǯɨ-ŋ</ta>
            <ta e="T84" id="Seg_834" s="T83">teper</ta>
            <ta e="T85" id="Seg_835" s="T84">elɨ-enǯɨ-j</ta>
            <ta e="T86" id="Seg_836" s="T85">soŋ</ta>
            <ta e="T87" id="Seg_837" s="T86">što</ta>
            <ta e="T88" id="Seg_838" s="T87">mazɨm</ta>
            <ta e="T89" id="Seg_839" s="T88">sej-se</ta>
            <ta e="T90" id="Seg_840" s="T89">meː-nɨ-t</ta>
            <ta e="T91" id="Seg_841" s="T90">teper</ta>
            <ta e="T92" id="Seg_842" s="T91">man</ta>
            <ta e="T93" id="Seg_843" s="T92">sej-la-w</ta>
            <ta e="T94" id="Seg_844" s="T93">aːdu-nɨ-tɨn</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_845" s="T1">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T3" id="Seg_846" s="T2">want-PST-3SG.O</ta>
            <ta e="T4" id="Seg_847" s="T3">husband-ACC-3SG</ta>
            <ta e="T5" id="Seg_848" s="T4">poison-INF</ta>
            <ta e="T6" id="Seg_849" s="T5">earlier</ta>
            <ta e="T7" id="Seg_850" s="T6">be-PST.[3SG.S]</ta>
            <ta e="T8" id="Seg_851" s="T7">man-human.being.[NOM]</ta>
            <ta e="T9" id="Seg_852" s="T8">eye-CAR</ta>
            <ta e="T10" id="Seg_853" s="T9">become-PST.[3SG.S]</ta>
            <ta e="T11" id="Seg_854" s="T10">wife.[NOM]-3SG</ta>
            <ta e="T12" id="Seg_855" s="T11">(s)he-EP-ACC</ta>
            <ta e="T13" id="Seg_856" s="T12">poison-INF</ta>
            <ta e="T14" id="Seg_857" s="T13">want-PST-3SG.O</ta>
            <ta e="T15" id="Seg_858" s="T14">snake.[NOM]</ta>
            <ta e="T16" id="Seg_859" s="T15">kill-TR-3SG.O</ta>
            <ta e="T17" id="Seg_860" s="T16">and</ta>
            <ta e="T18" id="Seg_861" s="T17">cauldron-INSTR</ta>
            <ta e="T19" id="Seg_862" s="T18">cook-CO-3SG.O</ta>
            <ta e="T20" id="Seg_863" s="T19">Easter-ADJZ-day.[NOM]</ta>
            <ta e="T21" id="Seg_864" s="T20">oneself.3SG</ta>
            <ta e="T22" id="Seg_865" s="T21">eat-FRQ-CO-3SG.S</ta>
            <ta e="T23" id="Seg_866" s="T22">elk-EP-GEN</ta>
            <ta e="T24" id="Seg_867" s="T23">meat.[NOM]</ta>
            <ta e="T25" id="Seg_868" s="T24">and</ta>
            <ta e="T26" id="Seg_869" s="T25">say-3SG.S</ta>
            <ta e="T27" id="Seg_870" s="T26">eat-FRQ-DRV-POT.FUT.2SG</ta>
            <ta e="T28" id="Seg_871" s="T27">and</ta>
            <ta e="T29" id="Seg_872" s="T28">(s)he.[NOM]</ta>
            <ta e="T30" id="Seg_873" s="T29">say-3SG.S</ta>
            <ta e="T31" id="Seg_874" s="T30">eat-FRQ-TR-FUT-1SG.S</ta>
            <ta e="T32" id="Seg_875" s="T31">(s)he.[NOM]</ta>
            <ta e="T33" id="Seg_876" s="T32">cauldron-DIM-ACC</ta>
            <ta e="T34" id="Seg_877" s="T33">table.[NOM]</ta>
            <ta e="T35" id="Seg_878" s="T34">top-EP-ILL</ta>
            <ta e="T36" id="Seg_879" s="T35">put-3SG.O</ta>
            <ta e="T37" id="Seg_880" s="T36">eat-FRQ-IMP.2SG.S</ta>
            <ta e="T38" id="Seg_881" s="T37">cauldron-EL.3SG</ta>
            <ta e="T39" id="Seg_882" s="T38">I.NOM</ta>
            <ta e="T40" id="Seg_883" s="T39">leave-FUT-1SG.S</ta>
            <ta e="T41" id="Seg_884" s="T40">swing-RFL-DRV-INF</ta>
            <ta e="T42" id="Seg_885" s="T41">swing-INSTRN.[NOM]</ta>
            <ta e="T43" id="Seg_886" s="T42">this-day.[NOM]</ta>
            <ta e="T44" id="Seg_887" s="T43">Easter.[NOM]</ta>
            <ta e="T45" id="Seg_888" s="T44">man-human.being.[NOM]</ta>
            <ta e="T46" id="Seg_889" s="T45">sit.down-3SG.S</ta>
            <ta e="T47" id="Seg_890" s="T46">eat-FRQ-INF</ta>
            <ta e="T48" id="Seg_891" s="T47">hot</ta>
            <ta e="T49" id="Seg_892" s="T48">and</ta>
            <ta e="T50" id="Seg_893" s="T49">lid-ACC</ta>
            <ta e="T51" id="Seg_894" s="T50">open-3SG.O</ta>
            <ta e="T52" id="Seg_895" s="T51">there-ADV.LOC</ta>
            <ta e="T53" id="Seg_896" s="T52">look-DUR.[3SG.S]</ta>
            <ta e="T54" id="Seg_897" s="T53">and</ta>
            <ta e="T55" id="Seg_898" s="T54">eye-PL.[NOM]-3SG</ta>
            <ta e="T56" id="Seg_899" s="T55">open-RFL-DRV-3PL</ta>
            <ta e="T57" id="Seg_900" s="T56">look-DUR.[3SG.S]</ta>
            <ta e="T58" id="Seg_901" s="T57">there-ADV.LOC</ta>
            <ta e="T59" id="Seg_902" s="T58">snake.[NOM]</ta>
            <ta e="T60" id="Seg_903" s="T59">lie-INFER.[3SG.S]</ta>
            <ta e="T61" id="Seg_904" s="T60">cauldron-DIM-LOC</ta>
            <ta e="T62" id="Seg_905" s="T61">(s)he.[NOM]</ta>
            <ta e="T63" id="Seg_906" s="T62">outward(s)</ta>
            <ta e="T64" id="Seg_907" s="T63">go-3SG.S</ta>
            <ta e="T65" id="Seg_908" s="T64">wife.[NOM]-3SG</ta>
            <ta e="T66" id="Seg_909" s="T65">swing-RFL-DRV-3SG.S</ta>
            <ta e="T67" id="Seg_910" s="T66">I.NOM</ta>
            <ta e="T68" id="Seg_911" s="T67">say-1SG.S</ta>
            <ta e="T69" id="Seg_912" s="T68">either</ta>
            <ta e="T70" id="Seg_913" s="T69">swing-RFL-DRV-2SG.S</ta>
            <ta e="T71" id="Seg_914" s="T70">and</ta>
            <ta e="T72" id="Seg_915" s="T71">I.NOM</ta>
            <ta e="T73" id="Seg_916" s="T72">you.SG.ACC</ta>
            <ta e="T74" id="Seg_917" s="T73">see-DRV-CO-1SG.S</ta>
            <ta e="T75" id="Seg_918" s="T74">and</ta>
            <ta e="T76" id="Seg_919" s="T75">wife.[NOM]-3SG</ta>
            <ta e="T77" id="Seg_920" s="T76">get.scared-CO-3SG.S</ta>
            <ta e="T78" id="Seg_921" s="T77">NEG.IMP</ta>
            <ta e="T79" id="Seg_922" s="T78">get.afraid-DUR-IMP.2SG.S</ta>
            <ta e="T80" id="Seg_923" s="T79">I.NOM</ta>
            <ta e="T81" id="Seg_924" s="T80">you.SG.ACC</ta>
            <ta e="T82" id="Seg_925" s="T81">NEG</ta>
            <ta e="T83" id="Seg_926" s="T82">kill-FUT-1SG.S</ta>
            <ta e="T84" id="Seg_927" s="T83">now</ta>
            <ta e="T85" id="Seg_928" s="T84">live-FUT-1DU</ta>
            <ta e="T86" id="Seg_929" s="T85">good</ta>
            <ta e="T87" id="Seg_930" s="T86">that</ta>
            <ta e="T88" id="Seg_931" s="T87">I.ACC</ta>
            <ta e="T89" id="Seg_932" s="T88">eye-COM</ta>
            <ta e="T90" id="Seg_933" s="T89">do-CO-3SG.O</ta>
            <ta e="T91" id="Seg_934" s="T90">now</ta>
            <ta e="T92" id="Seg_935" s="T91">I.GEN</ta>
            <ta e="T93" id="Seg_936" s="T92">eye-PL.[NOM]-1SG</ta>
            <ta e="T94" id="Seg_937" s="T93">see-CO-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_938" s="T1">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T3" id="Seg_939" s="T2">хотеть-PST-3SG.O</ta>
            <ta e="T4" id="Seg_940" s="T3">муж-ACC-3SG</ta>
            <ta e="T5" id="Seg_941" s="T4">отравить-INF</ta>
            <ta e="T6" id="Seg_942" s="T5">раньше</ta>
            <ta e="T7" id="Seg_943" s="T6">быть-PST.[3SG.S]</ta>
            <ta e="T8" id="Seg_944" s="T7">мужчина-человек.[NOM]</ta>
            <ta e="T9" id="Seg_945" s="T8">глаз-CAR</ta>
            <ta e="T10" id="Seg_946" s="T9">стать-PST.[3SG.S]</ta>
            <ta e="T11" id="Seg_947" s="T10">жена.[NOM]-3SG</ta>
            <ta e="T12" id="Seg_948" s="T11">он(а)-EP-ACC</ta>
            <ta e="T13" id="Seg_949" s="T12">отравить-INF</ta>
            <ta e="T14" id="Seg_950" s="T13">хотеть-PST-3SG.O</ta>
            <ta e="T15" id="Seg_951" s="T14">змея.[NOM]</ta>
            <ta e="T16" id="Seg_952" s="T15">убить-TR-3SG.O</ta>
            <ta e="T17" id="Seg_953" s="T16">и</ta>
            <ta e="T18" id="Seg_954" s="T17">чугунок-INSTR</ta>
            <ta e="T19" id="Seg_955" s="T18">парить-CO-3SG.O</ta>
            <ta e="T20" id="Seg_956" s="T19">Пасха-ADJZ-день.[NOM]</ta>
            <ta e="T21" id="Seg_957" s="T20">сам.3SG</ta>
            <ta e="T22" id="Seg_958" s="T21">съесть-FRQ-CO-3SG.S</ta>
            <ta e="T23" id="Seg_959" s="T22">лось-EP-GEN</ta>
            <ta e="T24" id="Seg_960" s="T23">мясо.[NOM]</ta>
            <ta e="T25" id="Seg_961" s="T24">и</ta>
            <ta e="T26" id="Seg_962" s="T25">сказать-3SG.S</ta>
            <ta e="T27" id="Seg_963" s="T26">съесть-FRQ-DRV-POT.FUT.2SG</ta>
            <ta e="T28" id="Seg_964" s="T27">а</ta>
            <ta e="T29" id="Seg_965" s="T28">он(а).[NOM]</ta>
            <ta e="T30" id="Seg_966" s="T29">сказать-3SG.S</ta>
            <ta e="T31" id="Seg_967" s="T30">съесть-FRQ-TR-FUT-1SG.S</ta>
            <ta e="T32" id="Seg_968" s="T31">он(а).[NOM]</ta>
            <ta e="T33" id="Seg_969" s="T32">чугунок-DIM-ACC</ta>
            <ta e="T34" id="Seg_970" s="T33">стол.[NOM]</ta>
            <ta e="T35" id="Seg_971" s="T34">верхняя.часть-EP-ILL</ta>
            <ta e="T36" id="Seg_972" s="T35">поставить-3SG.O</ta>
            <ta e="T37" id="Seg_973" s="T36">съесть-FRQ-IMP.2SG.S</ta>
            <ta e="T38" id="Seg_974" s="T37">чугунок-EL.3SG</ta>
            <ta e="T39" id="Seg_975" s="T38">я.NOM</ta>
            <ta e="T40" id="Seg_976" s="T39">отправиться-FUT-1SG.S</ta>
            <ta e="T41" id="Seg_977" s="T40">качать-RFL-DRV-INF</ta>
            <ta e="T42" id="Seg_978" s="T41">качать-INSTRN.[NOM]</ta>
            <ta e="T43" id="Seg_979" s="T42">этот-день.[NOM]</ta>
            <ta e="T44" id="Seg_980" s="T43">Пасха.[NOM]</ta>
            <ta e="T45" id="Seg_981" s="T44">мужчина-человек.[NOM]</ta>
            <ta e="T46" id="Seg_982" s="T45">сесть-3SG.S</ta>
            <ta e="T47" id="Seg_983" s="T46">съесть-FRQ-INF</ta>
            <ta e="T48" id="Seg_984" s="T47">горячий</ta>
            <ta e="T49" id="Seg_985" s="T48">и</ta>
            <ta e="T50" id="Seg_986" s="T49">крышка-ACC</ta>
            <ta e="T51" id="Seg_987" s="T50">открыть-3SG.O</ta>
            <ta e="T52" id="Seg_988" s="T51">туда-ADV.LOC</ta>
            <ta e="T53" id="Seg_989" s="T52">посмотреть-DUR.[3SG.S]</ta>
            <ta e="T54" id="Seg_990" s="T53">и</ta>
            <ta e="T55" id="Seg_991" s="T54">глаз-PL.[NOM]-3SG</ta>
            <ta e="T56" id="Seg_992" s="T55">открыть-RFL-DRV-3PL</ta>
            <ta e="T57" id="Seg_993" s="T56">посмотреть-DUR.[3SG.S]</ta>
            <ta e="T58" id="Seg_994" s="T57">туда-ADV.LOC</ta>
            <ta e="T59" id="Seg_995" s="T58">змея.[NOM]</ta>
            <ta e="T60" id="Seg_996" s="T59">лежать-INFER.[3SG.S]</ta>
            <ta e="T61" id="Seg_997" s="T60">чугунок-DIM-LOC</ta>
            <ta e="T62" id="Seg_998" s="T61">он(а).[NOM]</ta>
            <ta e="T63" id="Seg_999" s="T62">наружу</ta>
            <ta e="T64" id="Seg_1000" s="T63">ходить-3SG.S</ta>
            <ta e="T65" id="Seg_1001" s="T64">жена.[NOM]-3SG</ta>
            <ta e="T66" id="Seg_1002" s="T65">качать-RFL-DRV-3SG.S</ta>
            <ta e="T67" id="Seg_1003" s="T66">я.NOM</ta>
            <ta e="T68" id="Seg_1004" s="T67">сказать-1SG.S</ta>
            <ta e="T69" id="Seg_1005" s="T68">ли</ta>
            <ta e="T70" id="Seg_1006" s="T69">качать-RFL-DRV-2SG.S</ta>
            <ta e="T71" id="Seg_1007" s="T70">а</ta>
            <ta e="T72" id="Seg_1008" s="T71">я.NOM</ta>
            <ta e="T73" id="Seg_1009" s="T72">ты.ACC</ta>
            <ta e="T74" id="Seg_1010" s="T73">увидеть-DRV-CO-1SG.S</ta>
            <ta e="T75" id="Seg_1011" s="T74">а</ta>
            <ta e="T76" id="Seg_1012" s="T75">жена.[NOM]-3SG</ta>
            <ta e="T77" id="Seg_1013" s="T76">испугаться-CO-3SG.S</ta>
            <ta e="T78" id="Seg_1014" s="T77">NEG.IMP</ta>
            <ta e="T79" id="Seg_1015" s="T78">испугаться-DUR-IMP.2SG.S</ta>
            <ta e="T80" id="Seg_1016" s="T79">я.NOM</ta>
            <ta e="T81" id="Seg_1017" s="T80">ты.ACC</ta>
            <ta e="T82" id="Seg_1018" s="T81">NEG</ta>
            <ta e="T83" id="Seg_1019" s="T82">убить-FUT-1SG.S</ta>
            <ta e="T84" id="Seg_1020" s="T83">теперь</ta>
            <ta e="T85" id="Seg_1021" s="T84">жить-FUT-1DU</ta>
            <ta e="T86" id="Seg_1022" s="T85">хорошо</ta>
            <ta e="T87" id="Seg_1023" s="T86">что</ta>
            <ta e="T88" id="Seg_1024" s="T87">я.ACC</ta>
            <ta e="T89" id="Seg_1025" s="T88">глаз-COM</ta>
            <ta e="T90" id="Seg_1026" s="T89">сделать-CO-3SG.O</ta>
            <ta e="T91" id="Seg_1027" s="T90">теперь</ta>
            <ta e="T92" id="Seg_1028" s="T91">я.GEN</ta>
            <ta e="T93" id="Seg_1029" s="T92">глаз-PL.[NOM]-1SG</ta>
            <ta e="T94" id="Seg_1030" s="T93">видеть-CO-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1031" s="T1">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T3" id="Seg_1032" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_1033" s="T3">n-n:case-n:poss</ta>
            <ta e="T5" id="Seg_1034" s="T4">v-v:inf</ta>
            <ta e="T6" id="Seg_1035" s="T5">adv</ta>
            <ta e="T7" id="Seg_1036" s="T6">v-v:tense.[v:pn]</ta>
            <ta e="T8" id="Seg_1037" s="T7">n-n.[n:case]</ta>
            <ta e="T9" id="Seg_1038" s="T8">n-n&gt;n</ta>
            <ta e="T10" id="Seg_1039" s="T9">v-v:tense.[v:pn]</ta>
            <ta e="T11" id="Seg_1040" s="T10">n.[n:case]-n:poss</ta>
            <ta e="T12" id="Seg_1041" s="T11">pers-n:ins-n:case</ta>
            <ta e="T13" id="Seg_1042" s="T12">v-v:inf</ta>
            <ta e="T14" id="Seg_1043" s="T13">v-v:tense-v:pn</ta>
            <ta e="T15" id="Seg_1044" s="T14">n.[n:case]</ta>
            <ta e="T16" id="Seg_1045" s="T15">v-v&gt;v-v:pn</ta>
            <ta e="T17" id="Seg_1046" s="T16">conj</ta>
            <ta e="T18" id="Seg_1047" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_1048" s="T18">v-v:ins-v:pn</ta>
            <ta e="T20" id="Seg_1049" s="T19">nprop-n&gt;adj-n.[n:case]</ta>
            <ta e="T21" id="Seg_1050" s="T20">emphpro</ta>
            <ta e="T22" id="Seg_1051" s="T21">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T23" id="Seg_1052" s="T22">n-n:ins-n:case</ta>
            <ta e="T24" id="Seg_1053" s="T23">n.[n:case]</ta>
            <ta e="T25" id="Seg_1054" s="T24">conj</ta>
            <ta e="T26" id="Seg_1055" s="T25">v-v:pn</ta>
            <ta e="T27" id="Seg_1056" s="T26">v-v&gt;v-v&gt;v-v:tense</ta>
            <ta e="T28" id="Seg_1057" s="T27">conj</ta>
            <ta e="T29" id="Seg_1058" s="T28">pers.[n:case]</ta>
            <ta e="T30" id="Seg_1059" s="T29">v-v:pn</ta>
            <ta e="T31" id="Seg_1060" s="T30">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_1061" s="T31">pers.[n:case]</ta>
            <ta e="T33" id="Seg_1062" s="T32">n-n&gt;n-n:case</ta>
            <ta e="T34" id="Seg_1063" s="T33">n.[n:case]</ta>
            <ta e="T35" id="Seg_1064" s="T34">n-n:ins-n:case</ta>
            <ta e="T36" id="Seg_1065" s="T35">v-v:pn</ta>
            <ta e="T37" id="Seg_1066" s="T36">v-v&gt;v-v:mood.pn</ta>
            <ta e="T38" id="Seg_1067" s="T37">n-n:case.poss</ta>
            <ta e="T39" id="Seg_1068" s="T38">pers</ta>
            <ta e="T40" id="Seg_1069" s="T39">v-v:tense-v:pn</ta>
            <ta e="T41" id="Seg_1070" s="T40">v-v&gt;v-v&gt;v-v:inf</ta>
            <ta e="T42" id="Seg_1071" s="T41">v-v&gt;n.[n:case]</ta>
            <ta e="T43" id="Seg_1072" s="T42">dem-n.[n:case]</ta>
            <ta e="T44" id="Seg_1073" s="T43">nprop.[n:case]</ta>
            <ta e="T45" id="Seg_1074" s="T44">n-n.[n:case]</ta>
            <ta e="T46" id="Seg_1075" s="T45">v-v:pn</ta>
            <ta e="T47" id="Seg_1076" s="T46">v-v&gt;v-v:inf</ta>
            <ta e="T48" id="Seg_1077" s="T47">adj</ta>
            <ta e="T49" id="Seg_1078" s="T48">conj</ta>
            <ta e="T50" id="Seg_1079" s="T49">n-n:case</ta>
            <ta e="T51" id="Seg_1080" s="T50">v-v:pn</ta>
            <ta e="T52" id="Seg_1081" s="T51">adv-adv:case</ta>
            <ta e="T53" id="Seg_1082" s="T52">v-v&gt;v.[v:pn]</ta>
            <ta e="T54" id="Seg_1083" s="T53">conj</ta>
            <ta e="T55" id="Seg_1084" s="T54">n-n:num.[n:case]-n:poss</ta>
            <ta e="T56" id="Seg_1085" s="T55">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T57" id="Seg_1086" s="T56">v-v&gt;v.[v:pn]</ta>
            <ta e="T58" id="Seg_1087" s="T57">adv-adv:case</ta>
            <ta e="T59" id="Seg_1088" s="T58">n.[n:case]</ta>
            <ta e="T60" id="Seg_1089" s="T59">v-v:mood.[v:pn]</ta>
            <ta e="T61" id="Seg_1090" s="T60">n-n&gt;n-n:case</ta>
            <ta e="T62" id="Seg_1091" s="T61">pers.[n:case]</ta>
            <ta e="T63" id="Seg_1092" s="T62">adv</ta>
            <ta e="T64" id="Seg_1093" s="T63">v-v:pn</ta>
            <ta e="T65" id="Seg_1094" s="T64">n.[n:case]-n:poss</ta>
            <ta e="T66" id="Seg_1095" s="T65">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T67" id="Seg_1096" s="T66">pers</ta>
            <ta e="T68" id="Seg_1097" s="T67">v-v:pn</ta>
            <ta e="T69" id="Seg_1098" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_1099" s="T69">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T71" id="Seg_1100" s="T70">conj</ta>
            <ta e="T72" id="Seg_1101" s="T71">pers</ta>
            <ta e="T73" id="Seg_1102" s="T72">pers</ta>
            <ta e="T74" id="Seg_1103" s="T73">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T75" id="Seg_1104" s="T74">conj</ta>
            <ta e="T76" id="Seg_1105" s="T75">n.[n:case]-n:poss</ta>
            <ta e="T77" id="Seg_1106" s="T76">v-v:ins-v:pn</ta>
            <ta e="T78" id="Seg_1107" s="T77">ptcl</ta>
            <ta e="T79" id="Seg_1108" s="T78">v-v&gt;v-v:mood.pn</ta>
            <ta e="T80" id="Seg_1109" s="T79">pers</ta>
            <ta e="T81" id="Seg_1110" s="T80">pers</ta>
            <ta e="T82" id="Seg_1111" s="T81">ptcl</ta>
            <ta e="T83" id="Seg_1112" s="T82">v-v:tense-v:pn</ta>
            <ta e="T84" id="Seg_1113" s="T83">adv</ta>
            <ta e="T85" id="Seg_1114" s="T84">v-v:tense-v:pn</ta>
            <ta e="T86" id="Seg_1115" s="T85">adv</ta>
            <ta e="T87" id="Seg_1116" s="T86">conj</ta>
            <ta e="T88" id="Seg_1117" s="T87">pers</ta>
            <ta e="T89" id="Seg_1118" s="T88">n-n:case</ta>
            <ta e="T90" id="Seg_1119" s="T89">v-v:ins-v:pn</ta>
            <ta e="T91" id="Seg_1120" s="T90">adv</ta>
            <ta e="T92" id="Seg_1121" s="T91">pers</ta>
            <ta e="T93" id="Seg_1122" s="T92">n-n:num.[n:case]-n:poss</ta>
            <ta e="T94" id="Seg_1123" s="T93">v-v:ins-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1124" s="T1">n</ta>
            <ta e="T3" id="Seg_1125" s="T2">v</ta>
            <ta e="T4" id="Seg_1126" s="T3">n</ta>
            <ta e="T5" id="Seg_1127" s="T4">v</ta>
            <ta e="T6" id="Seg_1128" s="T5">adv</ta>
            <ta e="T7" id="Seg_1129" s="T6">v</ta>
            <ta e="T8" id="Seg_1130" s="T7">n</ta>
            <ta e="T9" id="Seg_1131" s="T8">n</ta>
            <ta e="T10" id="Seg_1132" s="T9">v</ta>
            <ta e="T11" id="Seg_1133" s="T10">n</ta>
            <ta e="T12" id="Seg_1134" s="T11">pers</ta>
            <ta e="T13" id="Seg_1135" s="T12">v</ta>
            <ta e="T14" id="Seg_1136" s="T13">v</ta>
            <ta e="T15" id="Seg_1137" s="T14">n</ta>
            <ta e="T16" id="Seg_1138" s="T15">v</ta>
            <ta e="T17" id="Seg_1139" s="T16">conj</ta>
            <ta e="T18" id="Seg_1140" s="T17">n</ta>
            <ta e="T19" id="Seg_1141" s="T18">v</ta>
            <ta e="T20" id="Seg_1142" s="T19">n</ta>
            <ta e="T21" id="Seg_1143" s="T20">emphpro</ta>
            <ta e="T22" id="Seg_1144" s="T21">v</ta>
            <ta e="T23" id="Seg_1145" s="T22">n</ta>
            <ta e="T24" id="Seg_1146" s="T23">n</ta>
            <ta e="T25" id="Seg_1147" s="T24">conj</ta>
            <ta e="T26" id="Seg_1148" s="T25">v</ta>
            <ta e="T27" id="Seg_1149" s="T26">v</ta>
            <ta e="T28" id="Seg_1150" s="T27">conj</ta>
            <ta e="T29" id="Seg_1151" s="T28">pers</ta>
            <ta e="T30" id="Seg_1152" s="T29">v</ta>
            <ta e="T31" id="Seg_1153" s="T30">v</ta>
            <ta e="T32" id="Seg_1154" s="T31">pers</ta>
            <ta e="T33" id="Seg_1155" s="T32">n</ta>
            <ta e="T34" id="Seg_1156" s="T33">n</ta>
            <ta e="T35" id="Seg_1157" s="T34">n</ta>
            <ta e="T36" id="Seg_1158" s="T35">v</ta>
            <ta e="T37" id="Seg_1159" s="T36">v</ta>
            <ta e="T38" id="Seg_1160" s="T37">n</ta>
            <ta e="T39" id="Seg_1161" s="T38">pers</ta>
            <ta e="T40" id="Seg_1162" s="T39">v</ta>
            <ta e="T41" id="Seg_1163" s="T40">v</ta>
            <ta e="T42" id="Seg_1164" s="T41">n</ta>
            <ta e="T43" id="Seg_1165" s="T42">adv</ta>
            <ta e="T44" id="Seg_1166" s="T43">nprop</ta>
            <ta e="T45" id="Seg_1167" s="T44">n</ta>
            <ta e="T46" id="Seg_1168" s="T45">v</ta>
            <ta e="T47" id="Seg_1169" s="T46">v</ta>
            <ta e="T48" id="Seg_1170" s="T47">adj</ta>
            <ta e="T49" id="Seg_1171" s="T48">conj</ta>
            <ta e="T50" id="Seg_1172" s="T49">n</ta>
            <ta e="T51" id="Seg_1173" s="T50">v</ta>
            <ta e="T52" id="Seg_1174" s="T51">adv</ta>
            <ta e="T53" id="Seg_1175" s="T52">v</ta>
            <ta e="T54" id="Seg_1176" s="T53">conj</ta>
            <ta e="T55" id="Seg_1177" s="T54">n</ta>
            <ta e="T56" id="Seg_1178" s="T55">v</ta>
            <ta e="T57" id="Seg_1179" s="T56">v</ta>
            <ta e="T58" id="Seg_1180" s="T57">adv</ta>
            <ta e="T59" id="Seg_1181" s="T58">n</ta>
            <ta e="T60" id="Seg_1182" s="T59">v</ta>
            <ta e="T61" id="Seg_1183" s="T60">n</ta>
            <ta e="T62" id="Seg_1184" s="T61">pers</ta>
            <ta e="T63" id="Seg_1185" s="T62">adv</ta>
            <ta e="T64" id="Seg_1186" s="T63">v</ta>
            <ta e="T65" id="Seg_1187" s="T64">n</ta>
            <ta e="T66" id="Seg_1188" s="T65">v</ta>
            <ta e="T67" id="Seg_1189" s="T66">pers</ta>
            <ta e="T68" id="Seg_1190" s="T67">v</ta>
            <ta e="T69" id="Seg_1191" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_1192" s="T69">v</ta>
            <ta e="T71" id="Seg_1193" s="T70">conj</ta>
            <ta e="T72" id="Seg_1194" s="T71">pers</ta>
            <ta e="T73" id="Seg_1195" s="T72">pers</ta>
            <ta e="T74" id="Seg_1196" s="T73">v</ta>
            <ta e="T75" id="Seg_1197" s="T74">conj</ta>
            <ta e="T76" id="Seg_1198" s="T75">n</ta>
            <ta e="T77" id="Seg_1199" s="T76">v</ta>
            <ta e="T78" id="Seg_1200" s="T77">ptcl</ta>
            <ta e="T79" id="Seg_1201" s="T78">v</ta>
            <ta e="T80" id="Seg_1202" s="T79">pers</ta>
            <ta e="T81" id="Seg_1203" s="T80">pers</ta>
            <ta e="T82" id="Seg_1204" s="T81">ptcl</ta>
            <ta e="T83" id="Seg_1205" s="T82">v</ta>
            <ta e="T84" id="Seg_1206" s="T83">adv</ta>
            <ta e="T85" id="Seg_1207" s="T84">v</ta>
            <ta e="T86" id="Seg_1208" s="T85">adv</ta>
            <ta e="T87" id="Seg_1209" s="T86">conj</ta>
            <ta e="T88" id="Seg_1210" s="T87">pers</ta>
            <ta e="T89" id="Seg_1211" s="T88">n</ta>
            <ta e="T90" id="Seg_1212" s="T89">v</ta>
            <ta e="T91" id="Seg_1213" s="T90">adv</ta>
            <ta e="T92" id="Seg_1214" s="T91">pers</ta>
            <ta e="T93" id="Seg_1215" s="T92">n</ta>
            <ta e="T94" id="Seg_1216" s="T93">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_1217" s="T1">np.h:E</ta>
            <ta e="T4" id="Seg_1218" s="T3">np.h:P 0.3.h:Poss</ta>
            <ta e="T5" id="Seg_1219" s="T4">v:Th</ta>
            <ta e="T6" id="Seg_1220" s="T5">adv:Time</ta>
            <ta e="T7" id="Seg_1221" s="T6">0.3:Th</ta>
            <ta e="T8" id="Seg_1222" s="T7">np.h:Th</ta>
            <ta e="T11" id="Seg_1223" s="T10">np.h:E 0.3.h:Poss</ta>
            <ta e="T12" id="Seg_1224" s="T11">pro.h:P</ta>
            <ta e="T13" id="Seg_1225" s="T12">v:Th</ta>
            <ta e="T15" id="Seg_1226" s="T14">np:P</ta>
            <ta e="T16" id="Seg_1227" s="T15">0.3.h:A</ta>
            <ta e="T18" id="Seg_1228" s="T17">np:L</ta>
            <ta e="T19" id="Seg_1229" s="T18">0.3.h:A 0.3:P</ta>
            <ta e="T20" id="Seg_1230" s="T19">np:Time</ta>
            <ta e="T22" id="Seg_1231" s="T21">0.3.h:A</ta>
            <ta e="T23" id="Seg_1232" s="T22">np:Poss</ta>
            <ta e="T24" id="Seg_1233" s="T23">np:P</ta>
            <ta e="T26" id="Seg_1234" s="T25">0.3.h:A</ta>
            <ta e="T27" id="Seg_1235" s="T26">0.2.h:A</ta>
            <ta e="T29" id="Seg_1236" s="T28">pro.h:A</ta>
            <ta e="T31" id="Seg_1237" s="T30">0.1.h:A</ta>
            <ta e="T32" id="Seg_1238" s="T31">pro.h:A</ta>
            <ta e="T33" id="Seg_1239" s="T32">np:Th</ta>
            <ta e="T35" id="Seg_1240" s="T34">np:G</ta>
            <ta e="T37" id="Seg_1241" s="T36">0.2.h:A</ta>
            <ta e="T38" id="Seg_1242" s="T37">np:So</ta>
            <ta e="T39" id="Seg_1243" s="T38">pro.h:A</ta>
            <ta e="T43" id="Seg_1244" s="T42">np:Time</ta>
            <ta e="T45" id="Seg_1245" s="T44">np.h:A</ta>
            <ta e="T50" id="Seg_1246" s="T49">np:Th</ta>
            <ta e="T51" id="Seg_1247" s="T50">0.3.h:A</ta>
            <ta e="T52" id="Seg_1248" s="T51">adv:G</ta>
            <ta e="T53" id="Seg_1249" s="T52">0.3.h:A</ta>
            <ta e="T55" id="Seg_1250" s="T54">np:P 0.3.h:Poss</ta>
            <ta e="T57" id="Seg_1251" s="T56">0.3.h:A</ta>
            <ta e="T59" id="Seg_1252" s="T58">np:Th</ta>
            <ta e="T61" id="Seg_1253" s="T60">np:L</ta>
            <ta e="T62" id="Seg_1254" s="T61">pro.h:A</ta>
            <ta e="T63" id="Seg_1255" s="T62">adv:G</ta>
            <ta e="T65" id="Seg_1256" s="T64">np.h:A 0.3.h:Poss</ta>
            <ta e="T67" id="Seg_1257" s="T66">pro.h:A</ta>
            <ta e="T70" id="Seg_1258" s="T69">0.2.h:A</ta>
            <ta e="T72" id="Seg_1259" s="T71">pro.h:E</ta>
            <ta e="T73" id="Seg_1260" s="T72">pro.h:Th</ta>
            <ta e="T76" id="Seg_1261" s="T75">np.h:E 0.3.h:Poss</ta>
            <ta e="T79" id="Seg_1262" s="T78">0.2.h:E</ta>
            <ta e="T80" id="Seg_1263" s="T79">pro.h:A</ta>
            <ta e="T81" id="Seg_1264" s="T80">pro.h:P</ta>
            <ta e="T84" id="Seg_1265" s="T83">adv:Time</ta>
            <ta e="T85" id="Seg_1266" s="T84">0.1.h:Th</ta>
            <ta e="T88" id="Seg_1267" s="T87">pro.h:P</ta>
            <ta e="T89" id="Seg_1268" s="T88">np:Com</ta>
            <ta e="T90" id="Seg_1269" s="T89">0.3:A</ta>
            <ta e="T91" id="Seg_1270" s="T90">adv:Time</ta>
            <ta e="T92" id="Seg_1271" s="T91">pro.h:Poss</ta>
            <ta e="T93" id="Seg_1272" s="T92">np:E</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_1273" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_1274" s="T2">v:pred</ta>
            <ta e="T5" id="Seg_1275" s="T4">v:O</ta>
            <ta e="T7" id="Seg_1276" s="T6">0.3:S v:pred</ta>
            <ta e="T8" id="Seg_1277" s="T7">np.h:S</ta>
            <ta e="T10" id="Seg_1278" s="T9">v:pred</ta>
            <ta e="T11" id="Seg_1279" s="T10">np.h:S</ta>
            <ta e="T13" id="Seg_1280" s="T12">v:O</ta>
            <ta e="T14" id="Seg_1281" s="T13">v:pred</ta>
            <ta e="T15" id="Seg_1282" s="T14">np:O</ta>
            <ta e="T16" id="Seg_1283" s="T15">0.3.h:S v:pred</ta>
            <ta e="T19" id="Seg_1284" s="T18">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T22" id="Seg_1285" s="T21">0.3.h:S v:pred</ta>
            <ta e="T24" id="Seg_1286" s="T23">np:O</ta>
            <ta e="T26" id="Seg_1287" s="T25">0.3.h:S v:pred</ta>
            <ta e="T27" id="Seg_1288" s="T26">0.2.h:S v:pred</ta>
            <ta e="T29" id="Seg_1289" s="T28">pro.h:S</ta>
            <ta e="T30" id="Seg_1290" s="T29">v:pred</ta>
            <ta e="T31" id="Seg_1291" s="T30">0.1.h:S v:pred</ta>
            <ta e="T32" id="Seg_1292" s="T31">pro.h:S</ta>
            <ta e="T33" id="Seg_1293" s="T32">np:O</ta>
            <ta e="T36" id="Seg_1294" s="T35">v:pred</ta>
            <ta e="T37" id="Seg_1295" s="T36">0.2.h:S v:pred</ta>
            <ta e="T39" id="Seg_1296" s="T38">pro.h:S</ta>
            <ta e="T40" id="Seg_1297" s="T39">v:pred</ta>
            <ta e="T41" id="Seg_1298" s="T40">s:purp</ta>
            <ta e="T45" id="Seg_1299" s="T44">np.h:S</ta>
            <ta e="T46" id="Seg_1300" s="T45">v:pred</ta>
            <ta e="T48" id="Seg_1301" s="T46">s:purp</ta>
            <ta e="T50" id="Seg_1302" s="T49">np:O</ta>
            <ta e="T51" id="Seg_1303" s="T50">0.3.h:S v:pred</ta>
            <ta e="T53" id="Seg_1304" s="T52">0.3.h:S v:pred</ta>
            <ta e="T55" id="Seg_1305" s="T54">np:S</ta>
            <ta e="T56" id="Seg_1306" s="T55">v:pred</ta>
            <ta e="T57" id="Seg_1307" s="T56">0.3.h:S v:pred</ta>
            <ta e="T59" id="Seg_1308" s="T58">np:S</ta>
            <ta e="T60" id="Seg_1309" s="T59">v:pred</ta>
            <ta e="T62" id="Seg_1310" s="T61">pro.h:S</ta>
            <ta e="T64" id="Seg_1311" s="T63">v:pred</ta>
            <ta e="T65" id="Seg_1312" s="T64">np.h:S</ta>
            <ta e="T66" id="Seg_1313" s="T65">v:pred</ta>
            <ta e="T67" id="Seg_1314" s="T66">pro.h:S</ta>
            <ta e="T68" id="Seg_1315" s="T67">v:pred</ta>
            <ta e="T70" id="Seg_1316" s="T69">0.2.h:S v:pred</ta>
            <ta e="T72" id="Seg_1317" s="T71">pro.h:S</ta>
            <ta e="T73" id="Seg_1318" s="T72">pro.h:O</ta>
            <ta e="T74" id="Seg_1319" s="T73">v:pred</ta>
            <ta e="T76" id="Seg_1320" s="T75">np.h:S</ta>
            <ta e="T77" id="Seg_1321" s="T76">v:pred</ta>
            <ta e="T79" id="Seg_1322" s="T78">0.2.h:S v:pred</ta>
            <ta e="T80" id="Seg_1323" s="T79">pro.h:S</ta>
            <ta e="T81" id="Seg_1324" s="T80">pro.h:O</ta>
            <ta e="T83" id="Seg_1325" s="T82">v:pred</ta>
            <ta e="T85" id="Seg_1326" s="T84">0.1.h:S v:pred</ta>
            <ta e="T90" id="Seg_1327" s="T86">s:compl</ta>
            <ta e="T93" id="Seg_1328" s="T92">np:S</ta>
            <ta e="T94" id="Seg_1329" s="T93">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T5" id="Seg_1330" s="T4">RUS:cult</ta>
            <ta e="T13" id="Seg_1331" s="T12">RUS:cult</ta>
            <ta e="T17" id="Seg_1332" s="T16">RUS:gram</ta>
            <ta e="T18" id="Seg_1333" s="T17">RUS:cult</ta>
            <ta e="T19" id="Seg_1334" s="T18">RUS:cult</ta>
            <ta e="T20" id="Seg_1335" s="T19">RUS:cult</ta>
            <ta e="T25" id="Seg_1336" s="T24">RUS:gram</ta>
            <ta e="T28" id="Seg_1337" s="T27">RUS:gram</ta>
            <ta e="T33" id="Seg_1338" s="T32">RUS:cult</ta>
            <ta e="T34" id="Seg_1339" s="T33">RUS:cult</ta>
            <ta e="T35" id="Seg_1340" s="T34">WNB Noun or pp</ta>
            <ta e="T38" id="Seg_1341" s="T37">RUS:cult</ta>
            <ta e="T44" id="Seg_1342" s="T43">RUS:cult</ta>
            <ta e="T49" id="Seg_1343" s="T48">RUS:gram</ta>
            <ta e="T50" id="Seg_1344" s="T49">RUS:cult</ta>
            <ta e="T54" id="Seg_1345" s="T53">RUS:gram</ta>
            <ta e="T61" id="Seg_1346" s="T60">RUS:cult</ta>
            <ta e="T71" id="Seg_1347" s="T70">RUS:gram</ta>
            <ta e="T75" id="Seg_1348" s="T74">RUS:gram</ta>
            <ta e="T84" id="Seg_1349" s="T83">RUS:core</ta>
            <ta e="T87" id="Seg_1350" s="T86">RUS:gram</ta>
            <ta e="T91" id="Seg_1351" s="T90">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_1352" s="T1">One woman wanted to poison her husband.</ta>
            <ta e="T7" id="Seg_1353" s="T5">It was long ago.</ta>
            <ta e="T10" id="Seg_1354" s="T7">The man became blind.</ta>
            <ta e="T14" id="Seg_1355" s="T10">His wife wanted to poison him.</ta>
            <ta e="T20" id="Seg_1356" s="T14">She killed a snake and cooked it in a cauldron on Easter.</ta>
            <ta e="T27" id="Seg_1357" s="T20">She ate plenty of elk's meat and said: “Are you going to eat?”</ta>
            <ta e="T31" id="Seg_1358" s="T27">And he said: “I am going to eat.”</ta>
            <ta e="T36" id="Seg_1359" s="T31">She put the cauldron on the table.</ta>
            <ta e="T38" id="Seg_1360" s="T36">“Eat from the cauldron.</ta>
            <ta e="T42" id="Seg_1361" s="T38">I'll go and swing.</ta>
            <ta e="T44" id="Seg_1362" s="T42">Today is Easter.</ta>
            <ta e="T48" id="Seg_1363" s="T44">The man sat down to eat hot [meat].</ta>
            <ta e="T53" id="Seg_1364" s="T48">He opened the lid and looked there.</ta>
            <ta e="T56" id="Seg_1365" s="T53">And his eyes opened.</ta>
            <ta e="T61" id="Seg_1366" s="T56">He looked: a snake lay in the cauldron.</ta>
            <ta e="T64" id="Seg_1367" s="T61">He came out.</ta>
            <ta e="T66" id="Seg_1368" s="T64">His wife was swinging.</ta>
            <ta e="T70" id="Seg_1369" s="T66">“I say: are you swinging?</ta>
            <ta e="T74" id="Seg_1370" s="T70">And I see you.”</ta>
            <ta e="T77" id="Seg_1371" s="T74">And his wife got frightened.</ta>
            <ta e="T79" id="Seg_1372" s="T77">“Don't be afraid.</ta>
            <ta e="T83" id="Seg_1373" s="T79">I won't kill you.</ta>
            <ta e="T85" id="Seg_1374" s="T83">Now we'll live [together].</ta>
            <ta e="T90" id="Seg_1375" s="T85">It's good, that you made me [see] with my eyes.</ta>
            <ta e="T94" id="Seg_1376" s="T90">Now my eyes [can] see.”</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_1377" s="T1">Eine Frau wollte ihren Mann vergiften.</ta>
            <ta e="T7" id="Seg_1378" s="T5">Das war lange her.</ta>
            <ta e="T10" id="Seg_1379" s="T7">Der Mann wurde blind.</ta>
            <ta e="T14" id="Seg_1380" s="T10">Seine Frau wollte ihn vergiften.</ta>
            <ta e="T20" id="Seg_1381" s="T14">Sie tötete eine Schlange und kochte sie in einem Kessel an Ostern.</ta>
            <ta e="T27" id="Seg_1382" s="T20">Sie aß viel Elchfleisch und sagte: "Willst du [etwas] essen?"</ta>
            <ta e="T31" id="Seg_1383" s="T27">Und er sagte: "Ich will essen."</ta>
            <ta e="T36" id="Seg_1384" s="T31">Sie stellte den Kessel auf den Tisch.</ta>
            <ta e="T38" id="Seg_1385" s="T36">"Iss aus dem Kessel.</ta>
            <ta e="T42" id="Seg_1386" s="T38">Ich gehe um zu schaukeln.</ta>
            <ta e="T44" id="Seg_1387" s="T42">Heute ist Ostern.</ta>
            <ta e="T48" id="Seg_1388" s="T44">Der Mann setzte sich um das heiße [Fleisch] zu essen.</ta>
            <ta e="T53" id="Seg_1389" s="T48">Er öffnete den Deckel und sah hinein.</ta>
            <ta e="T56" id="Seg_1390" s="T53">Und seine Augen öffneten sich.</ta>
            <ta e="T61" id="Seg_1391" s="T56">Er schaute: Eine Schlange lag im Kessel.</ta>
            <ta e="T64" id="Seg_1392" s="T61">Er ging nach draußen.</ta>
            <ta e="T66" id="Seg_1393" s="T64">Seine Frau schaukelte.</ta>
            <ta e="T70" id="Seg_1394" s="T66">"Ich sage: Schaukelst du?</ta>
            <ta e="T74" id="Seg_1395" s="T70">Und ich sehe dich."</ta>
            <ta e="T77" id="Seg_1396" s="T74">Und seine Frau bekam Angst.</ta>
            <ta e="T79" id="Seg_1397" s="T77">"Hab keine Angst.</ta>
            <ta e="T83" id="Seg_1398" s="T79">Ich werde dich nicht töten.</ta>
            <ta e="T85" id="Seg_1399" s="T83">Jetzt leben wir [zusammen].</ta>
            <ta e="T90" id="Seg_1400" s="T85">Es ist gut, dass du mich mit meinen Augen [sehen] ließt.</ta>
            <ta e="T94" id="Seg_1401" s="T90">Jetzt sehen meine Augen [wieder]."</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_1402" s="T1">Женщина хотела своего мужа отравить.</ta>
            <ta e="T7" id="Seg_1403" s="T5">Давно это было.</ta>
            <ta e="T10" id="Seg_1404" s="T7">Мужик слепой стал.</ta>
            <ta e="T14" id="Seg_1405" s="T10">Его жена его хотела отравить.</ta>
            <ta e="T20" id="Seg_1406" s="T14">Она убила змею и в чугуне напарила ее на Пасху.</ta>
            <ta e="T27" id="Seg_1407" s="T20">Сама наелась мяса лося и говорит: “Есть будешь?”</ta>
            <ta e="T31" id="Seg_1408" s="T27">А он говорит: “Буду есть”.</ta>
            <ta e="T36" id="Seg_1409" s="T31">Она котелок на стол поставила.</ta>
            <ta e="T38" id="Seg_1410" s="T36">“Ешь (прямо) из котла.</ta>
            <ta e="T42" id="Seg_1411" s="T38">Я пойду качаться (качели).</ta>
            <ta e="T44" id="Seg_1412" s="T42">Сегодня Пасха”.</ta>
            <ta e="T48" id="Seg_1413" s="T44">Мужик сел есть горячий.</ta>
            <ta e="T53" id="Seg_1414" s="T48">И крышку открыл, потом (/туда?) глядит.</ta>
            <ta e="T56" id="Seg_1415" s="T53">И глаза открылись.</ta>
            <ta e="T61" id="Seg_1416" s="T56">Смотрит, там змея лежит в чугуне.</ta>
            <ta e="T64" id="Seg_1417" s="T61">Он на улицу вышел.</ta>
            <ta e="T66" id="Seg_1418" s="T64">Жена его качается.</ta>
            <ta e="T70" id="Seg_1419" s="T66">“Я говорю: что, качаешься?</ta>
            <ta e="T74" id="Seg_1420" s="T70">А я тебя вижу”.</ta>
            <ta e="T77" id="Seg_1421" s="T74">А жена его испугалась.</ta>
            <ta e="T79" id="Seg_1422" s="T77">“Ты не бойся.</ta>
            <ta e="T83" id="Seg_1423" s="T79">Я тебя не убью.</ta>
            <ta e="T85" id="Seg_1424" s="T83">Теперь мы (двое) жить будем.</ta>
            <ta e="T90" id="Seg_1425" s="T85">Хорошо, что меня с глазами сделала.</ta>
            <ta e="T94" id="Seg_1426" s="T90">Теперь мои глаза видят”. || теперь мои глаза видят</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_1427" s="T1">женщина хотела мужика отравить</ta>
            <ta e="T7" id="Seg_1428" s="T5">раньше было</ta>
            <ta e="T10" id="Seg_1429" s="T7">мужик слепой стал</ta>
            <ta e="T14" id="Seg_1430" s="T10">его жена его хотела отравить</ta>
            <ta e="T20" id="Seg_1431" s="T14">змею убила в чугуне напарила (в паску) на паске</ta>
            <ta e="T27" id="Seg_1432" s="T20">сама наелась лосева мяса и говорит есть будешь</ta>
            <ta e="T31" id="Seg_1433" s="T27">говорит есть буду</ta>
            <ta e="T36" id="Seg_1434" s="T31">она чугун на стол поставила</ta>
            <ta e="T38" id="Seg_1435" s="T36">ешь из чугуна (прямо)</ta>
            <ta e="T42" id="Seg_1436" s="T38">а я пойду качаться (качеля, на качелях)</ta>
            <ta e="T44" id="Seg_1437" s="T42">сегодня пасха</ta>
            <ta e="T48" id="Seg_1438" s="T44">мужик сел есть горячий</ta>
            <ta e="T53" id="Seg_1439" s="T48">крышку открыл потом глядит</ta>
            <ta e="T56" id="Seg_1440" s="T53">и глаза открылись</ta>
            <ta e="T61" id="Seg_1441" s="T56">смотрит там змея лежит в чугуне</ta>
            <ta e="T64" id="Seg_1442" s="T61">он на улицу вышел</ta>
            <ta e="T66" id="Seg_1443" s="T64">жена качается</ta>
            <ta e="T70" id="Seg_1444" s="T66">что качаешься</ta>
            <ta e="T74" id="Seg_1445" s="T70">а я тебя вижу</ta>
            <ta e="T77" id="Seg_1446" s="T74">а жена испугалась</ta>
            <ta e="T79" id="Seg_1447" s="T77">не бойся</ta>
            <ta e="T83" id="Seg_1448" s="T79">я тебя не убью</ta>
            <ta e="T85" id="Seg_1449" s="T83">теперь жить будем</ta>
            <ta e="T90" id="Seg_1450" s="T85">хорошо что меня с глазами сделала</ta>
            <ta e="T94" id="Seg_1451" s="T90">теперь мои глаза видят</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T20" id="Seg_1452" s="T14">[BrM:] Unexpected usage of Instrumental case. Tentative analysis of 'parʼinät'.</ta>
            <ta e="T27" id="Seg_1453" s="T20">[BrM:] Tentative analysis of 'aurčennaš'.</ta>
            <ta e="T42" id="Seg_1454" s="T38">[KuAI:] qugur′salla - swings (Pl.)</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
