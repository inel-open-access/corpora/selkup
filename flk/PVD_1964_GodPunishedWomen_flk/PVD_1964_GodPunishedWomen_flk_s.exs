<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_GodPunishedWomen_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_GodPunishedWomen_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">216</ud-information>
            <ud-information attribute-name="# HIAT:w">129</ud-information>
            <ud-information attribute-name="# e">129</ud-information>
            <ud-information attribute-name="# HIAT:u">29</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T130" id="Seg_0" n="sc" s="T1">
               <ts e="T13" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Qussaqɨn</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">swet</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">azuppaː</ts>
                  <nts id="Seg_11" n="HIAT:ip">,</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_14" n="HIAT:w" s="T4">nom</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_17" n="HIAT:w" s="T5">wes</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_20" n="HIAT:w" s="T6">čotčot</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_23" n="HIAT:w" s="T7">üdəpbat</ts>
                  <nts id="Seg_24" n="HIAT:ip">:</nts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_27" n="HIAT:w" s="T8">qulam</ts>
                  <nts id="Seg_28" n="HIAT:ip">,</nts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">sɨrlam</ts>
                  <nts id="Seg_32" n="HIAT:ip">,</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_35" n="HIAT:w" s="T10">kütdəlam</ts>
                  <nts id="Seg_36" n="HIAT:ip">,</nts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_39" n="HIAT:w" s="T11">qonärlam</ts>
                  <nts id="Seg_40" n="HIAT:ip">,</nts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">čuškalam</ts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_47" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">Soɣədʼzʼelʼe</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">übərɨppa</ts>
                  <nts id="Seg_53" n="HIAT:ip">.</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_56" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">Qwɛrɨpbat</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">sɨram</ts>
                  <nts id="Seg_62" n="HIAT:ip">:</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_64" n="HIAT:ip">“</nts>
                  <ts e="T18" id="Seg_66" n="HIAT:w" s="T17">Tʼekga</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_69" n="HIAT:w" s="T18">okkɨrɨn</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_72" n="HIAT:w" s="T19">seːp</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_75" n="HIAT:w" s="T20">jeǯɨn</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_78" n="HIAT:w" s="T21">poːɣotdə</ts>
                  <nts id="Seg_79" n="HIAT:ip">?</nts>
                  <nts id="Seg_80" n="HIAT:ip">”</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_83" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_85" n="HIAT:w" s="T22">A</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">sɨr</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">tʼärɨn</ts>
                  <nts id="Seg_92" n="HIAT:ip">:</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_94" n="HIAT:ip">“</nts>
                  <ts e="T26" id="Seg_96" n="HIAT:w" s="T25">Seːp</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_99" n="HIAT:w" s="T26">jeǯɨn</ts>
                  <nts id="Seg_100" n="HIAT:ip">.</nts>
                  <nts id="Seg_101" n="HIAT:ip">”</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_104" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_106" n="HIAT:w" s="T27">Täpär</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_109" n="HIAT:w" s="T28">kütdə</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_112" n="HIAT:w" s="T29">qwɛrɨtdɨt</ts>
                  <nts id="Seg_113" n="HIAT:ip">.</nts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_116" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_118" n="HIAT:w" s="T30">Soɣɨdʼzʼilʼe</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_121" n="HIAT:w" s="T31">übɨran</ts>
                  <nts id="Seg_122" n="HIAT:ip">:</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_124" n="HIAT:ip">“</nts>
                  <ts e="T33" id="Seg_126" n="HIAT:w" s="T32">Tekga</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_129" n="HIAT:w" s="T33">okkɨrɨn</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_132" n="HIAT:w" s="T34">seːp</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_135" n="HIAT:w" s="T35">jeǯɨn</ts>
                  <nts id="Seg_136" n="HIAT:ip">?</nts>
                  <nts id="Seg_137" n="HIAT:ip">”</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_140" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_142" n="HIAT:w" s="T36">Täp</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_145" n="HIAT:w" s="T37">tʼarɨn</ts>
                  <nts id="Seg_146" n="HIAT:ip">:</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_148" n="HIAT:ip">“</nts>
                  <ts e="T39" id="Seg_150" n="HIAT:w" s="T38">Seːp</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_153" n="HIAT:w" s="T39">jeǯɨn</ts>
                  <nts id="Seg_154" n="HIAT:ip">.</nts>
                  <nts id="Seg_155" n="HIAT:ip">”</nts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_158" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_160" n="HIAT:w" s="T40">Qwɛrɨt</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_163" n="HIAT:w" s="T41">qonärɨm</ts>
                  <nts id="Seg_164" n="HIAT:ip">.</nts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_167" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_169" n="HIAT:w" s="T42">Qonärnä</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_172" n="HIAT:w" s="T43">soɣudʼzʼin</ts>
                  <nts id="Seg_173" n="HIAT:ip">:</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_175" n="HIAT:ip">“</nts>
                  <ts e="T45" id="Seg_177" n="HIAT:w" s="T44">Tekga</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_180" n="HIAT:w" s="T45">okkɨrɨn</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_183" n="HIAT:w" s="T46">seːp</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_186" n="HIAT:w" s="T47">jeǯɨn</ts>
                  <nts id="Seg_187" n="HIAT:ip">?</nts>
                  <nts id="Seg_188" n="HIAT:ip">”</nts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_191" n="HIAT:u" s="T48">
                  <nts id="Seg_192" n="HIAT:ip">“</nts>
                  <ts e="T49" id="Seg_194" n="HIAT:w" s="T48">Seːp</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_197" n="HIAT:w" s="T49">jeǯɨn</ts>
                  <nts id="Seg_198" n="HIAT:ip">.</nts>
                  <nts id="Seg_199" n="HIAT:ip">”</nts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_202" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_204" n="HIAT:w" s="T50">Qwɛrɨt</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_207" n="HIAT:w" s="T51">čuškam</ts>
                  <nts id="Seg_208" n="HIAT:ip">.</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_211" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_213" n="HIAT:w" s="T52">Soɣudʼzʼin</ts>
                  <nts id="Seg_214" n="HIAT:ip">:</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_216" n="HIAT:ip">“</nts>
                  <ts e="T54" id="Seg_218" n="HIAT:w" s="T53">Tekga</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_221" n="HIAT:w" s="T54">okkɨrɨn</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_224" n="HIAT:w" s="T55">seːp</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_227" n="HIAT:w" s="T56">jeǯɨn</ts>
                  <nts id="Seg_228" n="HIAT:ip">?</nts>
                  <nts id="Seg_229" n="HIAT:ip">”</nts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_232" n="HIAT:u" s="T57">
                  <nts id="Seg_233" n="HIAT:ip">“</nts>
                  <ts e="T58" id="Seg_235" n="HIAT:w" s="T57">Ass</ts>
                  <nts id="Seg_236" n="HIAT:ip">!</nts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_239" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_241" n="HIAT:w" s="T58">Kʼettə</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_244" n="HIAT:w" s="T59">mekga</ts>
                  <nts id="Seg_245" n="HIAT:ip">:</nts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_248" n="HIAT:w" s="T60">Sədə</ts>
                  <nts id="Seg_249" n="HIAT:ip">.</nts>
                  <nts id="Seg_250" n="HIAT:ip">”</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_253" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_255" n="HIAT:w" s="T61">Nom</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_258" n="HIAT:w" s="T62">tʼärɨn</ts>
                  <nts id="Seg_259" n="HIAT:ip">:</nts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_261" n="HIAT:ip">“</nts>
                  <ts e="T64" id="Seg_263" n="HIAT:w" s="T63">Nu</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_266" n="HIAT:w" s="T64">puskaj</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_269" n="HIAT:w" s="T65">sədɨn</ts>
                  <nts id="Seg_270" n="HIAT:ip">.</nts>
                  <nts id="Seg_271" n="HIAT:ip">”</nts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_274" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_276" n="HIAT:w" s="T66">Qwɛrɨt</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_279" n="HIAT:w" s="T67">näjɣumɨm</ts>
                  <nts id="Seg_280" n="HIAT:ip">.</nts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_283" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_285" n="HIAT:w" s="T68">Näjɣumnä</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_288" n="HIAT:w" s="T69">soɣudʼzʼilʼe</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_291" n="HIAT:w" s="T70">übɨrɨn</ts>
                  <nts id="Seg_292" n="HIAT:ip">:</nts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_294" n="HIAT:ip">“</nts>
                  <ts e="T72" id="Seg_296" n="HIAT:w" s="T71">Tekga</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_299" n="HIAT:w" s="T72">okkɨrɨn</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_302" n="HIAT:w" s="T73">seːbʼ</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_305" n="HIAT:w" s="T74">jeǯɨn</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_308" n="HIAT:w" s="T75">poːɣotdə</ts>
                  <nts id="Seg_309" n="HIAT:ip">?</nts>
                  <nts id="Seg_310" n="HIAT:ip">”</nts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_313" n="HIAT:u" s="T76">
                  <nts id="Seg_314" n="HIAT:ip">“</nts>
                  <ts e="T77" id="Seg_316" n="HIAT:w" s="T76">Aːss</ts>
                  <nts id="Seg_317" n="HIAT:ip">,</nts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_320" n="HIAT:w" s="T77">sʼeːp</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_323" n="HIAT:w" s="T78">ass</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_326" n="HIAT:w" s="T79">jeǯɨn</ts>
                  <nts id="Seg_327" n="HIAT:ip">.</nts>
                  <nts id="Seg_328" n="HIAT:ip">”</nts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_331" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_333" n="HIAT:w" s="T80">Nom</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_336" n="HIAT:w" s="T81">tʼärɨn</ts>
                  <nts id="Seg_337" n="HIAT:ip">:</nts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_339" n="HIAT:ip">“</nts>
                  <ts e="T83" id="Seg_341" n="HIAT:w" s="T82">Sədən</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_344" n="HIAT:w" s="T83">sʼeːpʼ</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_347" n="HIAT:w" s="T84">jeǯɨn</ts>
                  <nts id="Seg_348" n="HIAT:ip">?</nts>
                  <nts id="Seg_349" n="HIAT:ip">”</nts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_352" n="HIAT:u" s="T85">
                  <nts id="Seg_353" n="HIAT:ip">“</nts>
                  <ts e="T86" id="Seg_355" n="HIAT:w" s="T85">Ass</ts>
                  <nts id="Seg_356" n="HIAT:ip">,</nts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_359" n="HIAT:w" s="T86">sədan</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_362" n="HIAT:w" s="T87">ass</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_365" n="HIAT:w" s="T88">kɨgan</ts>
                  <nts id="Seg_366" n="HIAT:ip">.</nts>
                  <nts id="Seg_367" n="HIAT:ip">”</nts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_370" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_372" n="HIAT:w" s="T89">Nom</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_375" n="HIAT:w" s="T90">tʼüːɨn</ts>
                  <nts id="Seg_376" n="HIAT:ip">,</nts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_379" n="HIAT:w" s="T91">tʼärɨn</ts>
                  <nts id="Seg_380" n="HIAT:ip">:</nts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_382" n="HIAT:ip">“</nts>
                  <ts e="T93" id="Seg_384" n="HIAT:w" s="T92">Nagurən</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_387" n="HIAT:w" s="T93">sʼeːb</ts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_390" n="HIAT:w" s="T94">jeǯɨn</ts>
                  <nts id="Seg_391" n="HIAT:ip">?</nts>
                  <nts id="Seg_392" n="HIAT:ip">”</nts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_395" n="HIAT:u" s="T95">
                  <nts id="Seg_396" n="HIAT:ip">“</nts>
                  <ts e="T96" id="Seg_398" n="HIAT:w" s="T95">Ass</ts>
                  <nts id="Seg_399" n="HIAT:ip">!</nts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T99" id="Seg_402" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_404" n="HIAT:w" s="T96">Nagurɨn</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_407" n="HIAT:w" s="T97">ass</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_410" n="HIAT:w" s="T98">kɨgan</ts>
                  <nts id="Seg_411" n="HIAT:ip">.</nts>
                  <nts id="Seg_412" n="HIAT:ip">”</nts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_415" n="HIAT:u" s="T99">
                  <ts e="T100" id="Seg_417" n="HIAT:w" s="T99">Nom</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_420" n="HIAT:w" s="T100">tʼüon</ts>
                  <nts id="Seg_421" n="HIAT:ip">:</nts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_423" n="HIAT:ip">“</nts>
                  <ts e="T102" id="Seg_425" n="HIAT:w" s="T101">Nu</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_428" n="HIAT:w" s="T102">i</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_431" n="HIAT:w" s="T103">wek</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_434" n="HIAT:w" s="T104">mantɨkok</ts>
                  <nts id="Seg_435" n="HIAT:ip">!</nts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_438" n="HIAT:u" s="T105">
                  <ts e="T106" id="Seg_440" n="HIAT:w" s="T105">Puskaj</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_443" n="HIAT:w" s="T106">äral</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_446" n="HIAT:w" s="T107">stä</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_449" n="HIAT:w" s="T108">maibukʼkʼi</ts>
                  <nts id="Seg_450" n="HIAT:ip">.</nts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_453" n="HIAT:u" s="T109">
                  <nts id="Seg_454" n="HIAT:ip">(</nts>
                  <ts e="T110" id="Seg_456" n="HIAT:w" s="T109">Puskaj</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_459" n="HIAT:w" s="T110">äral</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_462" n="HIAT:w" s="T111">majbukʼkʼistə</ts>
                  <nts id="Seg_463" n="HIAT:ip">)</nts>
                  <nts id="Seg_464" n="HIAT:ip">.</nts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_467" n="HIAT:u" s="T112">
                  <nts id="Seg_468" n="HIAT:ip">(</nts>
                  <ts e="T113" id="Seg_470" n="HIAT:w" s="T112">Aral</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_473" n="HIAT:w" s="T113">puskaj</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_476" n="HIAT:w" s="T114">stä</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_479" n="HIAT:w" s="T115">majbukʼi</ts>
                  <nts id="Seg_480" n="HIAT:ip">)</nts>
                  <nts id="Seg_481" n="HIAT:ip">.</nts>
                  <nts id="Seg_482" n="HIAT:ip">”</nts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T121" id="Seg_485" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_487" n="HIAT:w" s="T116">Täper</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_490" n="HIAT:w" s="T117">näjɣula</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_493" n="HIAT:w" s="T118">nilʼdʼzʼin</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_496" n="HIAT:w" s="T119">i</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_499" n="HIAT:w" s="T120">jewattə</ts>
                  <nts id="Seg_500" n="HIAT:ip">.</nts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_503" n="HIAT:u" s="T121">
                  <ts e="T122" id="Seg_505" n="HIAT:w" s="T121">Täblam</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_508" n="HIAT:w" s="T122">xotʼ</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_511" n="HIAT:w" s="T123">kaʒnaj</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_514" n="HIAT:w" s="T124">pin</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_517" n="HIAT:w" s="T125">malʼlʼe</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_520" n="HIAT:w" s="T126">taːdərət</ts>
                  <nts id="Seg_521" n="HIAT:ip">,</nts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_524" n="HIAT:w" s="T127">täbnä</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_527" n="HIAT:w" s="T128">malan</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_530" n="HIAT:w" s="T129">jeǯɨn</ts>
                  <nts id="Seg_531" n="HIAT:ip">.</nts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T130" id="Seg_533" n="sc" s="T1">
               <ts e="T2" id="Seg_535" n="e" s="T1">Qussaqɨn </ts>
               <ts e="T3" id="Seg_537" n="e" s="T2">swet </ts>
               <ts e="T4" id="Seg_539" n="e" s="T3">azuppaː, </ts>
               <ts e="T5" id="Seg_541" n="e" s="T4">nom </ts>
               <ts e="T6" id="Seg_543" n="e" s="T5">wes </ts>
               <ts e="T7" id="Seg_545" n="e" s="T6">čotčot </ts>
               <ts e="T8" id="Seg_547" n="e" s="T7">üdəpbat: </ts>
               <ts e="T9" id="Seg_549" n="e" s="T8">qulam, </ts>
               <ts e="T10" id="Seg_551" n="e" s="T9">sɨrlam, </ts>
               <ts e="T11" id="Seg_553" n="e" s="T10">kütdəlam, </ts>
               <ts e="T12" id="Seg_555" n="e" s="T11">qonärlam, </ts>
               <ts e="T13" id="Seg_557" n="e" s="T12">čuškalam. </ts>
               <ts e="T14" id="Seg_559" n="e" s="T13">Soɣədʼzʼelʼe </ts>
               <ts e="T15" id="Seg_561" n="e" s="T14">übərɨppa. </ts>
               <ts e="T16" id="Seg_563" n="e" s="T15">Qwɛrɨpbat </ts>
               <ts e="T17" id="Seg_565" n="e" s="T16">sɨram: </ts>
               <ts e="T18" id="Seg_567" n="e" s="T17">“Tʼekga </ts>
               <ts e="T19" id="Seg_569" n="e" s="T18">okkɨrɨn </ts>
               <ts e="T20" id="Seg_571" n="e" s="T19">seːp </ts>
               <ts e="T21" id="Seg_573" n="e" s="T20">jeǯɨn </ts>
               <ts e="T22" id="Seg_575" n="e" s="T21">poːɣotdə?” </ts>
               <ts e="T23" id="Seg_577" n="e" s="T22">A </ts>
               <ts e="T24" id="Seg_579" n="e" s="T23">sɨr </ts>
               <ts e="T25" id="Seg_581" n="e" s="T24">tʼärɨn: </ts>
               <ts e="T26" id="Seg_583" n="e" s="T25">“Seːp </ts>
               <ts e="T27" id="Seg_585" n="e" s="T26">jeǯɨn.” </ts>
               <ts e="T28" id="Seg_587" n="e" s="T27">Täpär </ts>
               <ts e="T29" id="Seg_589" n="e" s="T28">kütdə </ts>
               <ts e="T30" id="Seg_591" n="e" s="T29">qwɛrɨtdɨt. </ts>
               <ts e="T31" id="Seg_593" n="e" s="T30">Soɣɨdʼzʼilʼe </ts>
               <ts e="T32" id="Seg_595" n="e" s="T31">übɨran: </ts>
               <ts e="T33" id="Seg_597" n="e" s="T32">“Tekga </ts>
               <ts e="T34" id="Seg_599" n="e" s="T33">okkɨrɨn </ts>
               <ts e="T35" id="Seg_601" n="e" s="T34">seːp </ts>
               <ts e="T36" id="Seg_603" n="e" s="T35">jeǯɨn?” </ts>
               <ts e="T37" id="Seg_605" n="e" s="T36">Täp </ts>
               <ts e="T38" id="Seg_607" n="e" s="T37">tʼarɨn: </ts>
               <ts e="T39" id="Seg_609" n="e" s="T38">“Seːp </ts>
               <ts e="T40" id="Seg_611" n="e" s="T39">jeǯɨn.” </ts>
               <ts e="T41" id="Seg_613" n="e" s="T40">Qwɛrɨt </ts>
               <ts e="T42" id="Seg_615" n="e" s="T41">qonärɨm. </ts>
               <ts e="T43" id="Seg_617" n="e" s="T42">Qonärnä </ts>
               <ts e="T44" id="Seg_619" n="e" s="T43">soɣudʼzʼin: </ts>
               <ts e="T45" id="Seg_621" n="e" s="T44">“Tekga </ts>
               <ts e="T46" id="Seg_623" n="e" s="T45">okkɨrɨn </ts>
               <ts e="T47" id="Seg_625" n="e" s="T46">seːp </ts>
               <ts e="T48" id="Seg_627" n="e" s="T47">jeǯɨn?” </ts>
               <ts e="T49" id="Seg_629" n="e" s="T48">“Seːp </ts>
               <ts e="T50" id="Seg_631" n="e" s="T49">jeǯɨn.” </ts>
               <ts e="T51" id="Seg_633" n="e" s="T50">Qwɛrɨt </ts>
               <ts e="T52" id="Seg_635" n="e" s="T51">čuškam. </ts>
               <ts e="T53" id="Seg_637" n="e" s="T52">Soɣudʼzʼin: </ts>
               <ts e="T54" id="Seg_639" n="e" s="T53">“Tekga </ts>
               <ts e="T55" id="Seg_641" n="e" s="T54">okkɨrɨn </ts>
               <ts e="T56" id="Seg_643" n="e" s="T55">seːp </ts>
               <ts e="T57" id="Seg_645" n="e" s="T56">jeǯɨn?” </ts>
               <ts e="T58" id="Seg_647" n="e" s="T57">“Ass! </ts>
               <ts e="T59" id="Seg_649" n="e" s="T58">Kʼettə </ts>
               <ts e="T60" id="Seg_651" n="e" s="T59">mekga: </ts>
               <ts e="T61" id="Seg_653" n="e" s="T60">Sədə.” </ts>
               <ts e="T62" id="Seg_655" n="e" s="T61">Nom </ts>
               <ts e="T63" id="Seg_657" n="e" s="T62">tʼärɨn: </ts>
               <ts e="T64" id="Seg_659" n="e" s="T63">“Nu </ts>
               <ts e="T65" id="Seg_661" n="e" s="T64">puskaj </ts>
               <ts e="T66" id="Seg_663" n="e" s="T65">sədɨn.” </ts>
               <ts e="T67" id="Seg_665" n="e" s="T66">Qwɛrɨt </ts>
               <ts e="T68" id="Seg_667" n="e" s="T67">näjɣumɨm. </ts>
               <ts e="T69" id="Seg_669" n="e" s="T68">Näjɣumnä </ts>
               <ts e="T70" id="Seg_671" n="e" s="T69">soɣudʼzʼilʼe </ts>
               <ts e="T71" id="Seg_673" n="e" s="T70">übɨrɨn: </ts>
               <ts e="T72" id="Seg_675" n="e" s="T71">“Tekga </ts>
               <ts e="T73" id="Seg_677" n="e" s="T72">okkɨrɨn </ts>
               <ts e="T74" id="Seg_679" n="e" s="T73">seːbʼ </ts>
               <ts e="T75" id="Seg_681" n="e" s="T74">jeǯɨn </ts>
               <ts e="T76" id="Seg_683" n="e" s="T75">poːɣotdə?” </ts>
               <ts e="T77" id="Seg_685" n="e" s="T76">“Aːss, </ts>
               <ts e="T78" id="Seg_687" n="e" s="T77">sʼeːp </ts>
               <ts e="T79" id="Seg_689" n="e" s="T78">ass </ts>
               <ts e="T80" id="Seg_691" n="e" s="T79">jeǯɨn.” </ts>
               <ts e="T81" id="Seg_693" n="e" s="T80">Nom </ts>
               <ts e="T82" id="Seg_695" n="e" s="T81">tʼärɨn: </ts>
               <ts e="T83" id="Seg_697" n="e" s="T82">“Sədən </ts>
               <ts e="T84" id="Seg_699" n="e" s="T83">sʼeːpʼ </ts>
               <ts e="T85" id="Seg_701" n="e" s="T84">jeǯɨn?” </ts>
               <ts e="T86" id="Seg_703" n="e" s="T85">“Ass, </ts>
               <ts e="T87" id="Seg_705" n="e" s="T86">sədan </ts>
               <ts e="T88" id="Seg_707" n="e" s="T87">ass </ts>
               <ts e="T89" id="Seg_709" n="e" s="T88">kɨgan.” </ts>
               <ts e="T90" id="Seg_711" n="e" s="T89">Nom </ts>
               <ts e="T91" id="Seg_713" n="e" s="T90">tʼüːɨn, </ts>
               <ts e="T92" id="Seg_715" n="e" s="T91">tʼärɨn: </ts>
               <ts e="T93" id="Seg_717" n="e" s="T92">“Nagurən </ts>
               <ts e="T94" id="Seg_719" n="e" s="T93">sʼeːb </ts>
               <ts e="T95" id="Seg_721" n="e" s="T94">jeǯɨn?” </ts>
               <ts e="T96" id="Seg_723" n="e" s="T95">“Ass! </ts>
               <ts e="T97" id="Seg_725" n="e" s="T96">Nagurɨn </ts>
               <ts e="T98" id="Seg_727" n="e" s="T97">ass </ts>
               <ts e="T99" id="Seg_729" n="e" s="T98">kɨgan.” </ts>
               <ts e="T100" id="Seg_731" n="e" s="T99">Nom </ts>
               <ts e="T101" id="Seg_733" n="e" s="T100">tʼüon: </ts>
               <ts e="T102" id="Seg_735" n="e" s="T101">“Nu </ts>
               <ts e="T103" id="Seg_737" n="e" s="T102">i </ts>
               <ts e="T104" id="Seg_739" n="e" s="T103">wek </ts>
               <ts e="T105" id="Seg_741" n="e" s="T104">mantɨkok! </ts>
               <ts e="T106" id="Seg_743" n="e" s="T105">Puskaj </ts>
               <ts e="T107" id="Seg_745" n="e" s="T106">äral </ts>
               <ts e="T108" id="Seg_747" n="e" s="T107">stä </ts>
               <ts e="T109" id="Seg_749" n="e" s="T108">maibukʼkʼi. </ts>
               <ts e="T110" id="Seg_751" n="e" s="T109">(Puskaj </ts>
               <ts e="T111" id="Seg_753" n="e" s="T110">äral </ts>
               <ts e="T112" id="Seg_755" n="e" s="T111">majbukʼkʼistə). </ts>
               <ts e="T113" id="Seg_757" n="e" s="T112">(Aral </ts>
               <ts e="T114" id="Seg_759" n="e" s="T113">puskaj </ts>
               <ts e="T115" id="Seg_761" n="e" s="T114">stä </ts>
               <ts e="T116" id="Seg_763" n="e" s="T115">majbukʼi).” </ts>
               <ts e="T117" id="Seg_765" n="e" s="T116">Täper </ts>
               <ts e="T118" id="Seg_767" n="e" s="T117">näjɣula </ts>
               <ts e="T119" id="Seg_769" n="e" s="T118">nilʼdʼzʼin </ts>
               <ts e="T120" id="Seg_771" n="e" s="T119">i </ts>
               <ts e="T121" id="Seg_773" n="e" s="T120">jewattə. </ts>
               <ts e="T122" id="Seg_775" n="e" s="T121">Täblam </ts>
               <ts e="T123" id="Seg_777" n="e" s="T122">xotʼ </ts>
               <ts e="T124" id="Seg_779" n="e" s="T123">kaʒnaj </ts>
               <ts e="T125" id="Seg_781" n="e" s="T124">pin </ts>
               <ts e="T126" id="Seg_783" n="e" s="T125">malʼlʼe </ts>
               <ts e="T127" id="Seg_785" n="e" s="T126">taːdərət, </ts>
               <ts e="T128" id="Seg_787" n="e" s="T127">täbnä </ts>
               <ts e="T129" id="Seg_789" n="e" s="T128">malan </ts>
               <ts e="T130" id="Seg_791" n="e" s="T129">jeǯɨn. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T13" id="Seg_792" s="T1">PVD_1964_GodPunishedWomen_flk.001 (001.001)</ta>
            <ta e="T15" id="Seg_793" s="T13">PVD_1964_GodPunishedWomen_flk.002 (001.002)</ta>
            <ta e="T22" id="Seg_794" s="T15">PVD_1964_GodPunishedWomen_flk.003 (001.003)</ta>
            <ta e="T27" id="Seg_795" s="T22">PVD_1964_GodPunishedWomen_flk.004 (001.004)</ta>
            <ta e="T30" id="Seg_796" s="T27">PVD_1964_GodPunishedWomen_flk.005 (001.005)</ta>
            <ta e="T36" id="Seg_797" s="T30">PVD_1964_GodPunishedWomen_flk.006 (001.006)</ta>
            <ta e="T40" id="Seg_798" s="T36">PVD_1964_GodPunishedWomen_flk.007 (001.007)</ta>
            <ta e="T42" id="Seg_799" s="T40">PVD_1964_GodPunishedWomen_flk.008 (001.008)</ta>
            <ta e="T48" id="Seg_800" s="T42">PVD_1964_GodPunishedWomen_flk.009 (001.009)</ta>
            <ta e="T50" id="Seg_801" s="T48">PVD_1964_GodPunishedWomen_flk.010 (001.010)</ta>
            <ta e="T52" id="Seg_802" s="T50">PVD_1964_GodPunishedWomen_flk.011 (001.011)</ta>
            <ta e="T57" id="Seg_803" s="T52">PVD_1964_GodPunishedWomen_flk.012 (001.012)</ta>
            <ta e="T58" id="Seg_804" s="T57">PVD_1964_GodPunishedWomen_flk.013 (001.013)</ta>
            <ta e="T61" id="Seg_805" s="T58">PVD_1964_GodPunishedWomen_flk.014 (001.014)</ta>
            <ta e="T66" id="Seg_806" s="T61">PVD_1964_GodPunishedWomen_flk.015 (001.015)</ta>
            <ta e="T68" id="Seg_807" s="T66">PVD_1964_GodPunishedWomen_flk.016 (001.016)</ta>
            <ta e="T76" id="Seg_808" s="T68">PVD_1964_GodPunishedWomen_flk.017 (001.017)</ta>
            <ta e="T80" id="Seg_809" s="T76">PVD_1964_GodPunishedWomen_flk.018 (001.018)</ta>
            <ta e="T85" id="Seg_810" s="T80">PVD_1964_GodPunishedWomen_flk.019 (001.019)</ta>
            <ta e="T89" id="Seg_811" s="T85">PVD_1964_GodPunishedWomen_flk.020 (001.020)</ta>
            <ta e="T95" id="Seg_812" s="T89">PVD_1964_GodPunishedWomen_flk.021 (001.021)</ta>
            <ta e="T96" id="Seg_813" s="T95">PVD_1964_GodPunishedWomen_flk.022 (001.022)</ta>
            <ta e="T99" id="Seg_814" s="T96">PVD_1964_GodPunishedWomen_flk.023 (001.023)</ta>
            <ta e="T105" id="Seg_815" s="T99">PVD_1964_GodPunishedWomen_flk.024 (001.024)</ta>
            <ta e="T109" id="Seg_816" s="T105">PVD_1964_GodPunishedWomen_flk.025 (001.025)</ta>
            <ta e="T112" id="Seg_817" s="T109">PVD_1964_GodPunishedWomen_flk.026 (001.026)</ta>
            <ta e="T116" id="Seg_818" s="T112">PVD_1964_GodPunishedWomen_flk.027 (001.027)</ta>
            <ta e="T121" id="Seg_819" s="T116">PVD_1964_GodPunishedWomen_flk.028 (001.028)</ta>
            <ta e="T130" id="Seg_820" s="T121">PVD_1964_GodPunishedWomen_flk.029 (001.029)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T13" id="Seg_821" s="T1">kу′ссаkын свет азу′ппа̄, ном вес ′тшоттшот ′ӱдъпб̂ат: kу′lам, ′сырлам, кӱтдъ′лам, kонӓрлам, ′чушкалам.</ta>
            <ta e="T15" id="Seg_822" s="T13">′соɣъдʼзʼелʼе ′ӱбърыппа.</ta>
            <ta e="T22" id="Seg_823" s="T15">′kwɛрыпбат ′сырам: ′тʼекга ′оккырын ′се̄п ′jеджын ′по̄ɣотдъ?</ta>
            <ta e="T27" id="Seg_824" s="T22">а сыр тʼӓ′рын: се̄п jеджын.</ta>
            <ta e="T30" id="Seg_825" s="T27">тӓ′пӓр ′кӱтдъ ′kwɛрытдыт.</ta>
            <ta e="T36" id="Seg_826" s="T30">′соɣыдʼзʼилʼе ′ӱбыран: ′текга ок′кырын се̄п ′jеджын?</ta>
            <ta e="T40" id="Seg_827" s="T36">тӓп тʼа′рын: ′се̄п jеджын.</ta>
            <ta e="T42" id="Seg_828" s="T40">′kwɛрыт kо′нӓрым.</ta>
            <ta e="T48" id="Seg_829" s="T42">kо′нӓрнӓ соɣу′дʼзʼин: ′текга ′оккырын ′се̄п jеджын?</ta>
            <ta e="T50" id="Seg_830" s="T48">′се̄п jеджын.</ta>
            <ta e="T52" id="Seg_831" s="T50">′kwɛрыт ′чушкам.</ta>
            <ta e="T57" id="Seg_832" s="T52">соɣу′дʼзʼин: ′текга ′оккырын се̄п jеджын?</ta>
            <ta e="T58" id="Seg_833" s="T57">асс!</ta>
            <ta e="T61" id="Seg_834" s="T58">′кʼеттъ ′мекга: ′съдъ(ы).</ta>
            <ta e="T66" id="Seg_835" s="T61">ном тʼӓ′рын: ну пус′кай съ′дын.</ta>
            <ta e="T68" id="Seg_836" s="T66">′kwɛрыт ′нӓй′ɣумым.</ta>
            <ta e="T76" id="Seg_837" s="T68">нӓй′ɣумнӓ ′соɣудʼзʼилʼе ′ӱбырын: ′текга ′оккырын ′се̄бʼ ′jеджын ′по̄ɣотдъ?</ta>
            <ta e="T80" id="Seg_838" s="T76">а̄сс, ′сʼе̄п асс ′jеджын.</ta>
            <ta e="T85" id="Seg_839" s="T80">ном тʼӓ′рын: съ′дън ′сʼе̄пʼ ′jеджын?</ta>
            <ta e="T89" id="Seg_840" s="T85">асс, ′съдан асс ′кыган.</ta>
            <ta e="T95" id="Seg_841" s="T89">ном ′тʼӱ̄ын, тʼӓ′рын: нагурън ′сʼе̄б jеджын?</ta>
            <ta e="T96" id="Seg_842" s="T95">асс!</ta>
            <ta e="T99" id="Seg_843" s="T96">′нагурын асс кы′ган.</ta>
            <ta e="T105" id="Seg_844" s="T99">ном ′тʼӱон: ну и век манты′кок!</ta>
            <ta e="T109" id="Seg_845" s="T105">пускай ӓ′рал стӓ ′маиб̂укʼкʼи.</ta>
            <ta e="T112" id="Seg_846" s="T109">(пускай ӓ′рал ′майбукʼкʼистъ.)</ta>
            <ta e="T116" id="Seg_847" s="T112">(а′рал пускай стӓ(ы) ′майбукʼи.)</ta>
            <ta e="T121" id="Seg_848" s="T116">тӓ′пер нӓйɣула нилʼ′дʼзʼин и ′jеwаттъ.</ta>
            <ta e="T130" id="Seg_849" s="T121">тӓб′лам хотʼ кажнай ′пин ′малʼлʼе ′та̄дърът, тӓбнӓ ′маlан ′jеджын.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T13" id="Seg_850" s="T1">qussaqɨn swet azuppaː, nom wes tšottšot üdəpb̂at: qulam, sɨrlam, kütdəlam, qonärlam, čuškalam.</ta>
            <ta e="T15" id="Seg_851" s="T13">soɣədʼzʼelʼe übərɨppa.</ta>
            <ta e="T22" id="Seg_852" s="T15">qwɛrɨpbat sɨram: tʼekga okkɨrɨn seːp jeǯɨn poːɣotdə?</ta>
            <ta e="T27" id="Seg_853" s="T22">a sɨr tʼärɨn: seːp jeǯɨn.</ta>
            <ta e="T30" id="Seg_854" s="T27">täpär kütdə qwɛrɨtdɨt.</ta>
            <ta e="T36" id="Seg_855" s="T30">soɣɨdʼzʼilʼe übɨran: tekga okkɨrɨn seːp jeǯɨn?</ta>
            <ta e="T40" id="Seg_856" s="T36">täp tʼarɨn: seːp jeǯɨn.</ta>
            <ta e="T42" id="Seg_857" s="T40">qwɛrɨt qonärɨm.</ta>
            <ta e="T48" id="Seg_858" s="T42">qonärnä soɣudʼzʼin: tekga okkɨrɨn seːp jeǯɨn?</ta>
            <ta e="T50" id="Seg_859" s="T48">seːp jeǯɨn.</ta>
            <ta e="T52" id="Seg_860" s="T50">qwɛrɨt čuškam.</ta>
            <ta e="T57" id="Seg_861" s="T52">soɣudʼzʼin: tekga okkɨrɨn seːp jeǯɨn?</ta>
            <ta e="T58" id="Seg_862" s="T57">ass!</ta>
            <ta e="T61" id="Seg_863" s="T58">kʼettə mekga: sədə(ɨ).</ta>
            <ta e="T66" id="Seg_864" s="T61">nom tʼärɨn: nu puskaj sədɨn.</ta>
            <ta e="T68" id="Seg_865" s="T66">qwɛrɨt näjɣumɨm.</ta>
            <ta e="T76" id="Seg_866" s="T68">näjɣumnä soɣudʼzʼilʼe übɨrɨn: tekga okkɨrɨn seːbʼ jeǯɨn poːɣotdə?</ta>
            <ta e="T80" id="Seg_867" s="T76">aːss, sʼeːp ass jeǯɨn.</ta>
            <ta e="T85" id="Seg_868" s="T80">nom tʼärɨn: sədən sʼeːpʼ jeǯɨn?</ta>
            <ta e="T89" id="Seg_869" s="T85">ass, sədan ass kɨgan.</ta>
            <ta e="T95" id="Seg_870" s="T89">nom tʼüːɨn, tʼärɨn: nagurən sʼeːb jeǯɨn?</ta>
            <ta e="T96" id="Seg_871" s="T95">ass!</ta>
            <ta e="T99" id="Seg_872" s="T96">nagurɨn ass kɨgan.</ta>
            <ta e="T105" id="Seg_873" s="T99">nom tʼüon: nu i wek mantɨkok!</ta>
            <ta e="T109" id="Seg_874" s="T105">puskaj äral stä maib̂ukʼkʼi.</ta>
            <ta e="T112" id="Seg_875" s="T109">(puskaj äral majbukʼkʼistə.)</ta>
            <ta e="T116" id="Seg_876" s="T112">(aral puskaj stä(ɨ) majbukʼi.)</ta>
            <ta e="T121" id="Seg_877" s="T116">täper näjɣula nilʼdʼzʼin i jewattə.</ta>
            <ta e="T130" id="Seg_878" s="T121">täblam xotʼ kaʒnaj pin malʼlʼe taːdərət, täbnä malan jeǯɨn.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T13" id="Seg_879" s="T1">Qussaqɨn swet azuppaː, nom wes čotčot üdəpbat: qulam, sɨrlam, kütdəlam, qonärlam, čuškalam. </ta>
            <ta e="T15" id="Seg_880" s="T13">Soɣədʼzʼelʼe übərɨppa. </ta>
            <ta e="T22" id="Seg_881" s="T15">Qwɛrɨpbat sɨram: “Tʼekga okkɨrɨn seːp jeǯɨn poːɣotdə?” </ta>
            <ta e="T27" id="Seg_882" s="T22">A sɨr tʼärɨn: “Seːp jeǯɨn.” </ta>
            <ta e="T30" id="Seg_883" s="T27">Täpär kütdə qwɛrɨtdɨt. </ta>
            <ta e="T36" id="Seg_884" s="T30">Soɣɨdʼzʼilʼe übɨran: “Tekga okkɨrɨn seːp jeǯɨn?” </ta>
            <ta e="T40" id="Seg_885" s="T36">Täp tʼarɨn: “Seːp jeǯɨn.” </ta>
            <ta e="T42" id="Seg_886" s="T40">Qwɛrɨt qonärɨm. </ta>
            <ta e="T48" id="Seg_887" s="T42">Qonärnä soɣudʼzʼin: “Tekga okkɨrɨn seːp jeǯɨn?” </ta>
            <ta e="T50" id="Seg_888" s="T48">“Seːp jeǯɨn.” </ta>
            <ta e="T52" id="Seg_889" s="T50">Qwɛrɨt čuškam. </ta>
            <ta e="T57" id="Seg_890" s="T52">Soɣudʼzʼin: “Tekga okkɨrɨn seːp jeǯɨn?” </ta>
            <ta e="T58" id="Seg_891" s="T57">“Ass! </ta>
            <ta e="T61" id="Seg_892" s="T58">Kʼettə mekga: Sədə.” </ta>
            <ta e="T66" id="Seg_893" s="T61">Nom tʼärɨn: “Nu puskaj sədɨn.” </ta>
            <ta e="T68" id="Seg_894" s="T66">Qwɛrɨt näjɣumɨm. </ta>
            <ta e="T76" id="Seg_895" s="T68">Näjɣumnä soɣudʼzʼilʼe übɨrɨn: “Tekga okkɨrɨn seːbʼ jeǯɨn poːɣotdə?” </ta>
            <ta e="T80" id="Seg_896" s="T76">“Aːss, sʼeːp ass jeǯɨn.” </ta>
            <ta e="T85" id="Seg_897" s="T80">Nom tʼärɨn: “Sədən sʼeːpʼ jeǯɨn?” </ta>
            <ta e="T89" id="Seg_898" s="T85">“Ass, sədan ass kɨgan.” </ta>
            <ta e="T95" id="Seg_899" s="T89">Nom tʼüːɨn, tʼärɨn: “Nagurən sʼeːb jeǯɨn?” </ta>
            <ta e="T96" id="Seg_900" s="T95">“Ass! </ta>
            <ta e="T99" id="Seg_901" s="T96">Nagurɨn ass kɨgan.” </ta>
            <ta e="T105" id="Seg_902" s="T99">Nom tʼüon: “Nu i wek mantɨkok! </ta>
            <ta e="T109" id="Seg_903" s="T105">Puskaj äral stä maibukʼkʼi. </ta>
            <ta e="T112" id="Seg_904" s="T109">(Puskaj äral majbukʼkʼistə). </ta>
            <ta e="T116" id="Seg_905" s="T112">(Aral puskaj stä majbukʼi).” </ta>
            <ta e="T121" id="Seg_906" s="T116">Täper näjɣula nilʼdʼzʼin i jewattə. </ta>
            <ta e="T130" id="Seg_907" s="T121">Täblam xotʼ kaʒnaj pin malʼlʼe taːdərət, täbnä malan jeǯɨn. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_908" s="T1">qussaqɨn</ta>
            <ta e="T3" id="Seg_909" s="T2">swet</ta>
            <ta e="T4" id="Seg_910" s="T3">azu-ppaː</ta>
            <ta e="T5" id="Seg_911" s="T4">nom</ta>
            <ta e="T6" id="Seg_912" s="T5">wes</ta>
            <ta e="T7" id="Seg_913" s="T6">čotčo-t</ta>
            <ta e="T8" id="Seg_914" s="T7">üdə-pba-t</ta>
            <ta e="T9" id="Seg_915" s="T8">qu-la-m</ta>
            <ta e="T10" id="Seg_916" s="T9">sɨr-la-m</ta>
            <ta e="T11" id="Seg_917" s="T10">kütdə-la-m</ta>
            <ta e="T12" id="Seg_918" s="T11">qonär-la-m</ta>
            <ta e="T13" id="Seg_919" s="T12">čuška-la-m</ta>
            <ta e="T14" id="Seg_920" s="T13">soɣədʼzʼe-lʼe</ta>
            <ta e="T15" id="Seg_921" s="T14">übə-r-ɨ-ppa</ta>
            <ta e="T16" id="Seg_922" s="T15">qwɛrɨ-pba-t</ta>
            <ta e="T17" id="Seg_923" s="T16">sɨra-m</ta>
            <ta e="T18" id="Seg_924" s="T17">tʼekga</ta>
            <ta e="T19" id="Seg_925" s="T18">okkɨr-ɨ-n</ta>
            <ta e="T20" id="Seg_926" s="T19">seːp</ta>
            <ta e="T21" id="Seg_927" s="T20">je-ǯɨ-n</ta>
            <ta e="T22" id="Seg_928" s="T21">poː-ɣotdə</ta>
            <ta e="T23" id="Seg_929" s="T22">a</ta>
            <ta e="T24" id="Seg_930" s="T23">sɨr</ta>
            <ta e="T25" id="Seg_931" s="T24">tʼärɨ-n</ta>
            <ta e="T26" id="Seg_932" s="T25">seːp</ta>
            <ta e="T27" id="Seg_933" s="T26">je-ǯɨ-n</ta>
            <ta e="T28" id="Seg_934" s="T27">täpär</ta>
            <ta e="T29" id="Seg_935" s="T28">kütdə</ta>
            <ta e="T30" id="Seg_936" s="T29">qwɛrɨ-tdɨ-t</ta>
            <ta e="T31" id="Seg_937" s="T30">soɣɨdʼzʼi-lʼe</ta>
            <ta e="T32" id="Seg_938" s="T31">übɨ-r-a-n</ta>
            <ta e="T33" id="Seg_939" s="T32">tekga</ta>
            <ta e="T34" id="Seg_940" s="T33">okkɨr-ɨ-n</ta>
            <ta e="T35" id="Seg_941" s="T34">seːp</ta>
            <ta e="T36" id="Seg_942" s="T35">je-ǯɨ-n</ta>
            <ta e="T37" id="Seg_943" s="T36">täp</ta>
            <ta e="T38" id="Seg_944" s="T37">tʼarɨ-n</ta>
            <ta e="T39" id="Seg_945" s="T38">seːp</ta>
            <ta e="T40" id="Seg_946" s="T39">je-ǯɨ-n</ta>
            <ta e="T41" id="Seg_947" s="T40">qwɛrɨ-t</ta>
            <ta e="T42" id="Seg_948" s="T41">qonär-ɨ-m</ta>
            <ta e="T43" id="Seg_949" s="T42">qonär-nä</ta>
            <ta e="T44" id="Seg_950" s="T43">soɣudʼzʼi-n</ta>
            <ta e="T45" id="Seg_951" s="T44">tekga</ta>
            <ta e="T46" id="Seg_952" s="T45">okkɨr-ɨ-n</ta>
            <ta e="T47" id="Seg_953" s="T46">seːp</ta>
            <ta e="T48" id="Seg_954" s="T47">je-ǯɨ-n</ta>
            <ta e="T49" id="Seg_955" s="T48">seːp</ta>
            <ta e="T50" id="Seg_956" s="T49">je-ǯɨ-n</ta>
            <ta e="T51" id="Seg_957" s="T50">qwɛrɨ-t</ta>
            <ta e="T52" id="Seg_958" s="T51">čuška-m</ta>
            <ta e="T53" id="Seg_959" s="T52">soɣudʼzʼi-n</ta>
            <ta e="T54" id="Seg_960" s="T53">tekga</ta>
            <ta e="T55" id="Seg_961" s="T54">okkɨr-ɨ-n</ta>
            <ta e="T56" id="Seg_962" s="T55">seːp</ta>
            <ta e="T57" id="Seg_963" s="T56">je-ǯɨ-n</ta>
            <ta e="T58" id="Seg_964" s="T57">ass</ta>
            <ta e="T59" id="Seg_965" s="T58">kʼet-tə</ta>
            <ta e="T60" id="Seg_966" s="T59">mekga</ta>
            <ta e="T61" id="Seg_967" s="T60">sədə</ta>
            <ta e="T62" id="Seg_968" s="T61">nom</ta>
            <ta e="T63" id="Seg_969" s="T62">tʼärɨ-n</ta>
            <ta e="T64" id="Seg_970" s="T63">nu</ta>
            <ta e="T65" id="Seg_971" s="T64">puskaj</ta>
            <ta e="T66" id="Seg_972" s="T65">sədɨ-n</ta>
            <ta e="T67" id="Seg_973" s="T66">qwɛrɨ-t</ta>
            <ta e="T68" id="Seg_974" s="T67">nä-j-ɣum-ɨ-m</ta>
            <ta e="T69" id="Seg_975" s="T68">nä-j-ɣum-nä</ta>
            <ta e="T70" id="Seg_976" s="T69">soɣudʼzʼi-lʼe</ta>
            <ta e="T71" id="Seg_977" s="T70">übɨ-r-ɨ-n</ta>
            <ta e="T72" id="Seg_978" s="T71">tekga</ta>
            <ta e="T73" id="Seg_979" s="T72">okkɨr-ɨ-n</ta>
            <ta e="T74" id="Seg_980" s="T73">seːbʼ</ta>
            <ta e="T75" id="Seg_981" s="T74">je-ǯɨ-n</ta>
            <ta e="T76" id="Seg_982" s="T75">poː-ɣotdə</ta>
            <ta e="T77" id="Seg_983" s="T76">aːss</ta>
            <ta e="T78" id="Seg_984" s="T77">sʼeːp</ta>
            <ta e="T79" id="Seg_985" s="T78">ass</ta>
            <ta e="T80" id="Seg_986" s="T79">je-ǯɨ-n</ta>
            <ta e="T81" id="Seg_987" s="T80">nom</ta>
            <ta e="T82" id="Seg_988" s="T81">tʼärɨ-n</ta>
            <ta e="T83" id="Seg_989" s="T82">sədə-n</ta>
            <ta e="T84" id="Seg_990" s="T83">sʼeːpʼ</ta>
            <ta e="T85" id="Seg_991" s="T84">je-ǯɨ-n</ta>
            <ta e="T86" id="Seg_992" s="T85">ass</ta>
            <ta e="T87" id="Seg_993" s="T86">səda-n</ta>
            <ta e="T88" id="Seg_994" s="T87">ass</ta>
            <ta e="T89" id="Seg_995" s="T88">kɨga-n</ta>
            <ta e="T90" id="Seg_996" s="T89">nom</ta>
            <ta e="T91" id="Seg_997" s="T90">tʼüːɨ-n</ta>
            <ta e="T92" id="Seg_998" s="T91">tʼärɨ-n</ta>
            <ta e="T93" id="Seg_999" s="T92">nagur-ə-n</ta>
            <ta e="T94" id="Seg_1000" s="T93">sʼeːb</ta>
            <ta e="T95" id="Seg_1001" s="T94">je-ǯɨ-n</ta>
            <ta e="T96" id="Seg_1002" s="T95">ass</ta>
            <ta e="T97" id="Seg_1003" s="T96">nagur-ɨ-n</ta>
            <ta e="T98" id="Seg_1004" s="T97">ass</ta>
            <ta e="T99" id="Seg_1005" s="T98">kɨga-n</ta>
            <ta e="T100" id="Seg_1006" s="T99">nom</ta>
            <ta e="T101" id="Seg_1007" s="T100">tʼüo-n</ta>
            <ta e="T102" id="Seg_1008" s="T101">nu</ta>
            <ta e="T103" id="Seg_1009" s="T102">i</ta>
            <ta e="T104" id="Seg_1010" s="T103">wek</ta>
            <ta e="T105" id="Seg_1011" s="T104">man-tɨ-ko-k</ta>
            <ta e="T106" id="Seg_1012" s="T105">puskaj</ta>
            <ta e="T107" id="Seg_1013" s="T106">ära-l</ta>
            <ta e="T108" id="Seg_1014" s="T107">stä</ta>
            <ta e="T109" id="Seg_1015" s="T108">mai-bu-kʼkʼi</ta>
            <ta e="T110" id="Seg_1016" s="T109">puskaj</ta>
            <ta e="T111" id="Seg_1017" s="T110">ära-l</ta>
            <ta e="T112" id="Seg_1018" s="T111">maj-bu-kʼkʼi-s-tə</ta>
            <ta e="T113" id="Seg_1019" s="T112">ara-l</ta>
            <ta e="T114" id="Seg_1020" s="T113">puskaj</ta>
            <ta e="T115" id="Seg_1021" s="T114">stä</ta>
            <ta e="T116" id="Seg_1022" s="T115">maj-bu-kʼi</ta>
            <ta e="T117" id="Seg_1023" s="T116">täper</ta>
            <ta e="T118" id="Seg_1024" s="T117">nä-j-ɣu-la</ta>
            <ta e="T119" id="Seg_1025" s="T118">nilʼdʼzʼi-n</ta>
            <ta e="T120" id="Seg_1026" s="T119">i</ta>
            <ta e="T121" id="Seg_1027" s="T120">je-wa-ttə</ta>
            <ta e="T122" id="Seg_1028" s="T121">täb-la-m</ta>
            <ta e="T123" id="Seg_1029" s="T122">xotʼ</ta>
            <ta e="T124" id="Seg_1030" s="T123">kaʒnaj</ta>
            <ta e="T125" id="Seg_1031" s="T124">pi-n</ta>
            <ta e="T126" id="Seg_1032" s="T125">malʼ-lʼe</ta>
            <ta e="T127" id="Seg_1033" s="T126">taːd-ə-r-ət</ta>
            <ta e="T128" id="Seg_1034" s="T127">täb-nä</ta>
            <ta e="T129" id="Seg_1035" s="T128">mala-n</ta>
            <ta e="T130" id="Seg_1036" s="T129">je-ǯɨ-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_1037" s="T1">qussaqɨn</ta>
            <ta e="T3" id="Seg_1038" s="T2">swet</ta>
            <ta e="T4" id="Seg_1039" s="T3">azu-mbɨ</ta>
            <ta e="T5" id="Seg_1040" s="T4">nom</ta>
            <ta e="T6" id="Seg_1041" s="T5">wesʼ</ta>
            <ta e="T7" id="Seg_1042" s="T6">čoččə-ntə</ta>
            <ta e="T8" id="Seg_1043" s="T7">üdə-mbɨ-t</ta>
            <ta e="T9" id="Seg_1044" s="T8">qum-la-m</ta>
            <ta e="T10" id="Seg_1045" s="T9">sɨr-la-m</ta>
            <ta e="T11" id="Seg_1046" s="T10">kütdə-la-m</ta>
            <ta e="T12" id="Seg_1047" s="T11">qoner-la-m</ta>
            <ta e="T13" id="Seg_1048" s="T12">čuška-la-m</ta>
            <ta e="T14" id="Seg_1049" s="T13">sogandʼe-le</ta>
            <ta e="T15" id="Seg_1050" s="T14">übɨ-r-ɨ-mbɨ</ta>
            <ta e="T16" id="Seg_1051" s="T15">qwɨrɨ-mbɨ-t</ta>
            <ta e="T17" id="Seg_1052" s="T16">sɨr-m</ta>
            <ta e="T18" id="Seg_1053" s="T17">tekka</ta>
            <ta e="T19" id="Seg_1054" s="T18">okkɨr-ɨ-n</ta>
            <ta e="T20" id="Seg_1055" s="T19">seːp</ta>
            <ta e="T21" id="Seg_1056" s="T20">eː-enǯɨ-n</ta>
            <ta e="T22" id="Seg_1057" s="T21">po-qɨntɨ</ta>
            <ta e="T23" id="Seg_1058" s="T22">a</ta>
            <ta e="T24" id="Seg_1059" s="T23">sɨr</ta>
            <ta e="T25" id="Seg_1060" s="T24">tʼärɨ-n</ta>
            <ta e="T26" id="Seg_1061" s="T25">seːp</ta>
            <ta e="T27" id="Seg_1062" s="T26">eː-enǯɨ-n</ta>
            <ta e="T28" id="Seg_1063" s="T27">teper</ta>
            <ta e="T29" id="Seg_1064" s="T28">kütdə</ta>
            <ta e="T30" id="Seg_1065" s="T29">qwɨrɨ-ntɨ-t</ta>
            <ta e="T31" id="Seg_1066" s="T30">sogandʼe-le</ta>
            <ta e="T32" id="Seg_1067" s="T31">übɨ-r-nɨ-n</ta>
            <ta e="T33" id="Seg_1068" s="T32">tekka</ta>
            <ta e="T34" id="Seg_1069" s="T33">okkɨr-ɨ-n</ta>
            <ta e="T35" id="Seg_1070" s="T34">seːp</ta>
            <ta e="T36" id="Seg_1071" s="T35">eː-enǯɨ-n</ta>
            <ta e="T37" id="Seg_1072" s="T36">täp</ta>
            <ta e="T38" id="Seg_1073" s="T37">tʼärɨ-n</ta>
            <ta e="T39" id="Seg_1074" s="T38">seːp</ta>
            <ta e="T40" id="Seg_1075" s="T39">eː-enǯɨ-n</ta>
            <ta e="T41" id="Seg_1076" s="T40">qwɨrɨ-t</ta>
            <ta e="T42" id="Seg_1077" s="T41">qoner-ɨ-m</ta>
            <ta e="T43" id="Seg_1078" s="T42">qoner-nä</ta>
            <ta e="T44" id="Seg_1079" s="T43">sogandʼe-n</ta>
            <ta e="T45" id="Seg_1080" s="T44">tekka</ta>
            <ta e="T46" id="Seg_1081" s="T45">okkɨr-ɨ-n</ta>
            <ta e="T47" id="Seg_1082" s="T46">seːp</ta>
            <ta e="T48" id="Seg_1083" s="T47">eː-enǯɨ-n</ta>
            <ta e="T49" id="Seg_1084" s="T48">seːp</ta>
            <ta e="T50" id="Seg_1085" s="T49">eː-enǯɨ-n</ta>
            <ta e="T51" id="Seg_1086" s="T50">qwɨrɨ-t</ta>
            <ta e="T52" id="Seg_1087" s="T51">čuška-m</ta>
            <ta e="T53" id="Seg_1088" s="T52">sogandʼe-n</ta>
            <ta e="T54" id="Seg_1089" s="T53">tekka</ta>
            <ta e="T55" id="Seg_1090" s="T54">okkɨr-ɨ-n</ta>
            <ta e="T56" id="Seg_1091" s="T55">seːp</ta>
            <ta e="T57" id="Seg_1092" s="T56">eː-enǯɨ-n</ta>
            <ta e="T58" id="Seg_1093" s="T57">asa</ta>
            <ta e="T59" id="Seg_1094" s="T58">ket-etɨ</ta>
            <ta e="T60" id="Seg_1095" s="T59">mekka</ta>
            <ta e="T61" id="Seg_1096" s="T60">sədə</ta>
            <ta e="T62" id="Seg_1097" s="T61">nom</ta>
            <ta e="T63" id="Seg_1098" s="T62">tʼärɨ-n</ta>
            <ta e="T64" id="Seg_1099" s="T63">nu</ta>
            <ta e="T65" id="Seg_1100" s="T64">puskaj</ta>
            <ta e="T66" id="Seg_1101" s="T65">sədə-n</ta>
            <ta e="T67" id="Seg_1102" s="T66">qwɨrɨ-t</ta>
            <ta e="T68" id="Seg_1103" s="T67">ne-lʼ-qum-ɨ-m</ta>
            <ta e="T69" id="Seg_1104" s="T68">ne-lʼ-qum-nä</ta>
            <ta e="T70" id="Seg_1105" s="T69">sogandʼe-le</ta>
            <ta e="T71" id="Seg_1106" s="T70">übɨ-r-ɨ-n</ta>
            <ta e="T72" id="Seg_1107" s="T71">tekka</ta>
            <ta e="T73" id="Seg_1108" s="T72">okkɨr-ɨ-n</ta>
            <ta e="T74" id="Seg_1109" s="T73">seːp</ta>
            <ta e="T75" id="Seg_1110" s="T74">eː-enǯɨ-n</ta>
            <ta e="T76" id="Seg_1111" s="T75">po-qɨntɨ</ta>
            <ta e="T77" id="Seg_1112" s="T76">asa</ta>
            <ta e="T78" id="Seg_1113" s="T77">seːp</ta>
            <ta e="T79" id="Seg_1114" s="T78">asa</ta>
            <ta e="T80" id="Seg_1115" s="T79">eː-enǯɨ-n</ta>
            <ta e="T81" id="Seg_1116" s="T80">nom</ta>
            <ta e="T82" id="Seg_1117" s="T81">tʼärɨ-n</ta>
            <ta e="T83" id="Seg_1118" s="T82">sədə-n</ta>
            <ta e="T84" id="Seg_1119" s="T83">seːp</ta>
            <ta e="T85" id="Seg_1120" s="T84">eː-enǯɨ-n</ta>
            <ta e="T86" id="Seg_1121" s="T85">asa</ta>
            <ta e="T87" id="Seg_1122" s="T86">sədə-n</ta>
            <ta e="T88" id="Seg_1123" s="T87">asa</ta>
            <ta e="T89" id="Seg_1124" s="T88">kɨgɨ-ŋ</ta>
            <ta e="T90" id="Seg_1125" s="T89">nom</ta>
            <ta e="T91" id="Seg_1126" s="T90">tüu-n</ta>
            <ta e="T92" id="Seg_1127" s="T91">tʼärɨ-n</ta>
            <ta e="T93" id="Seg_1128" s="T92">nagur-ɨ-n</ta>
            <ta e="T94" id="Seg_1129" s="T93">seːp</ta>
            <ta e="T95" id="Seg_1130" s="T94">eː-enǯɨ-n</ta>
            <ta e="T96" id="Seg_1131" s="T95">asa</ta>
            <ta e="T97" id="Seg_1132" s="T96">nagur-ɨ-n</ta>
            <ta e="T98" id="Seg_1133" s="T97">asa</ta>
            <ta e="T99" id="Seg_1134" s="T98">kɨgɨ-ŋ</ta>
            <ta e="T100" id="Seg_1135" s="T99">nom</ta>
            <ta e="T101" id="Seg_1136" s="T100">tüu-n</ta>
            <ta e="T102" id="Seg_1137" s="T101">nu</ta>
            <ta e="T103" id="Seg_1138" s="T102">i</ta>
            <ta e="T104" id="Seg_1139" s="T103">wek</ta>
            <ta e="T105" id="Seg_1140" s="T104">mat-ntɨ-ku-kɨ</ta>
            <ta e="T106" id="Seg_1141" s="T105">puskaj</ta>
            <ta e="T107" id="Seg_1142" s="T106">era-l</ta>
            <ta e="T108" id="Seg_1143" s="T107">tastɨ</ta>
            <ta e="T109" id="Seg_1144" s="T108">*mai-mbɨ-ku</ta>
            <ta e="T110" id="Seg_1145" s="T109">puskaj</ta>
            <ta e="T111" id="Seg_1146" s="T110">era-l</ta>
            <ta e="T112" id="Seg_1147" s="T111">*mai-mbɨ-ku-s-t</ta>
            <ta e="T113" id="Seg_1148" s="T112">era-l</ta>
            <ta e="T114" id="Seg_1149" s="T113">puskaj</ta>
            <ta e="T115" id="Seg_1150" s="T114">tastɨ</ta>
            <ta e="T116" id="Seg_1151" s="T115">*mai-mbɨ-ku</ta>
            <ta e="T117" id="Seg_1152" s="T116">teper</ta>
            <ta e="T118" id="Seg_1153" s="T117">ne-lʼ-qum-la</ta>
            <ta e="T119" id="Seg_1154" s="T118">nʼilʼdʼi-ŋ</ta>
            <ta e="T120" id="Seg_1155" s="T119">i</ta>
            <ta e="T121" id="Seg_1156" s="T120">eː-nɨ-tɨn</ta>
            <ta e="T122" id="Seg_1157" s="T121">täp-la-m</ta>
            <ta e="T123" id="Seg_1158" s="T122">xotʼ</ta>
            <ta e="T124" id="Seg_1159" s="T123">kaʒnaj</ta>
            <ta e="T125" id="Seg_1160" s="T124">pi-n</ta>
            <ta e="T126" id="Seg_1161" s="T125">mat-le</ta>
            <ta e="T127" id="Seg_1162" s="T126">tat-ɨ-r-etɨ</ta>
            <ta e="T128" id="Seg_1163" s="T127">täp-nä</ta>
            <ta e="T129" id="Seg_1164" s="T128">mala-ŋ</ta>
            <ta e="T130" id="Seg_1165" s="T129">eː-enǯɨ-n</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_1166" s="T1">when</ta>
            <ta e="T3" id="Seg_1167" s="T2">light.[NOM]</ta>
            <ta e="T4" id="Seg_1168" s="T3">become-PST.NAR.[3SG.S]</ta>
            <ta e="T5" id="Seg_1169" s="T4">god.[NOM]</ta>
            <ta e="T6" id="Seg_1170" s="T5">all</ta>
            <ta e="T7" id="Seg_1171" s="T6">earth-ILL</ta>
            <ta e="T8" id="Seg_1172" s="T7">send-PST.NAR-3SG.O</ta>
            <ta e="T9" id="Seg_1173" s="T8">human.being-PL-ACC</ta>
            <ta e="T10" id="Seg_1174" s="T9">cow-PL-ACC</ta>
            <ta e="T11" id="Seg_1175" s="T10">horse-PL-ACC</ta>
            <ta e="T12" id="Seg_1176" s="T11">sheep-PL-ACC</ta>
            <ta e="T13" id="Seg_1177" s="T12">pig-PL-ACC</ta>
            <ta e="T14" id="Seg_1178" s="T13">ask-CVB</ta>
            <ta e="T15" id="Seg_1179" s="T14">begin-DRV-EP-PST.NAR.[3SG.S]</ta>
            <ta e="T16" id="Seg_1180" s="T15">call-PST.NAR-3SG.O</ta>
            <ta e="T17" id="Seg_1181" s="T16">cow-ACC</ta>
            <ta e="T18" id="Seg_1182" s="T17">you.ALL</ta>
            <ta e="T19" id="Seg_1183" s="T18">one-EP-ADV.LOC</ta>
            <ta e="T20" id="Seg_1184" s="T19">enough</ta>
            <ta e="T21" id="Seg_1185" s="T20">be-FUT-3SG.S</ta>
            <ta e="T22" id="Seg_1186" s="T21">year-LOC.3SG</ta>
            <ta e="T23" id="Seg_1187" s="T22">and</ta>
            <ta e="T24" id="Seg_1188" s="T23">cow.[NOM]</ta>
            <ta e="T25" id="Seg_1189" s="T24">say-3SG.S</ta>
            <ta e="T26" id="Seg_1190" s="T25">enough</ta>
            <ta e="T27" id="Seg_1191" s="T26">be-FUT-3SG.S</ta>
            <ta e="T28" id="Seg_1192" s="T27">now</ta>
            <ta e="T29" id="Seg_1193" s="T28">horse.[NOM]</ta>
            <ta e="T30" id="Seg_1194" s="T29">call-INFER-3SG.O</ta>
            <ta e="T31" id="Seg_1195" s="T30">ask-CVB</ta>
            <ta e="T32" id="Seg_1196" s="T31">begin-DRV-CO-3SG.S</ta>
            <ta e="T33" id="Seg_1197" s="T32">you.ALL</ta>
            <ta e="T34" id="Seg_1198" s="T33">one-EP-ADV.LOC</ta>
            <ta e="T35" id="Seg_1199" s="T34">enough</ta>
            <ta e="T36" id="Seg_1200" s="T35">be-FUT-3SG.S</ta>
            <ta e="T37" id="Seg_1201" s="T36">(s)he.[NOM]</ta>
            <ta e="T38" id="Seg_1202" s="T37">say-3SG.S</ta>
            <ta e="T39" id="Seg_1203" s="T38">enough</ta>
            <ta e="T40" id="Seg_1204" s="T39">be-FUT-3SG.S</ta>
            <ta e="T41" id="Seg_1205" s="T40">call-3SG.O</ta>
            <ta e="T42" id="Seg_1206" s="T41">sheep-EP-ACC</ta>
            <ta e="T43" id="Seg_1207" s="T42">sheep-ALL</ta>
            <ta e="T44" id="Seg_1208" s="T43">ask-3SG.S</ta>
            <ta e="T45" id="Seg_1209" s="T44">you.ALL</ta>
            <ta e="T46" id="Seg_1210" s="T45">one-EP-ADV.LOC</ta>
            <ta e="T47" id="Seg_1211" s="T46">enough</ta>
            <ta e="T48" id="Seg_1212" s="T47">be-FUT-3SG.S</ta>
            <ta e="T49" id="Seg_1213" s="T48">enough</ta>
            <ta e="T50" id="Seg_1214" s="T49">be-FUT-3SG.S</ta>
            <ta e="T51" id="Seg_1215" s="T50">call-3SG.O</ta>
            <ta e="T52" id="Seg_1216" s="T51">pig-ACC</ta>
            <ta e="T53" id="Seg_1217" s="T52">ask-3SG.S</ta>
            <ta e="T54" id="Seg_1218" s="T53">you.ALL</ta>
            <ta e="T55" id="Seg_1219" s="T54">one-EP-ADV.LOC</ta>
            <ta e="T56" id="Seg_1220" s="T55">enough</ta>
            <ta e="T57" id="Seg_1221" s="T56">be-FUT-3SG.S</ta>
            <ta e="T58" id="Seg_1222" s="T57">NEG</ta>
            <ta e="T59" id="Seg_1223" s="T58">say-IMP.2SG.O</ta>
            <ta e="T60" id="Seg_1224" s="T59">I.ALL</ta>
            <ta e="T61" id="Seg_1225" s="T60">two</ta>
            <ta e="T62" id="Seg_1226" s="T61">god.[NOM]</ta>
            <ta e="T63" id="Seg_1227" s="T62">say-3SG.S</ta>
            <ta e="T64" id="Seg_1228" s="T63">now</ta>
            <ta e="T65" id="Seg_1229" s="T64">JUSS</ta>
            <ta e="T66" id="Seg_1230" s="T65">two-ADV.LOC</ta>
            <ta e="T67" id="Seg_1231" s="T66">call-3SG.O</ta>
            <ta e="T68" id="Seg_1232" s="T67">woman-ADJZ-human.being-EP-ACC</ta>
            <ta e="T69" id="Seg_1233" s="T68">woman-ADJZ-human.being-ALL</ta>
            <ta e="T70" id="Seg_1234" s="T69">ask-CVB</ta>
            <ta e="T71" id="Seg_1235" s="T70">begin-DRV-EP-3SG.S</ta>
            <ta e="T72" id="Seg_1236" s="T71">you.ALL</ta>
            <ta e="T73" id="Seg_1237" s="T72">one-EP-ADV.LOC</ta>
            <ta e="T74" id="Seg_1238" s="T73">enough</ta>
            <ta e="T75" id="Seg_1239" s="T74">be-FUT-3SG.S</ta>
            <ta e="T76" id="Seg_1240" s="T75">year-LOC.3SG</ta>
            <ta e="T77" id="Seg_1241" s="T76">NEG</ta>
            <ta e="T78" id="Seg_1242" s="T77">enough</ta>
            <ta e="T79" id="Seg_1243" s="T78">NEG</ta>
            <ta e="T80" id="Seg_1244" s="T79">be-FUT-3SG.S</ta>
            <ta e="T81" id="Seg_1245" s="T80">god.[NOM]</ta>
            <ta e="T82" id="Seg_1246" s="T81">say-3SG.S</ta>
            <ta e="T83" id="Seg_1247" s="T82">two-ADV.LOC</ta>
            <ta e="T84" id="Seg_1248" s="T83">enough</ta>
            <ta e="T85" id="Seg_1249" s="T84">be-FUT-3SG.S</ta>
            <ta e="T86" id="Seg_1250" s="T85">NEG</ta>
            <ta e="T87" id="Seg_1251" s="T86">two-ADV.LOC</ta>
            <ta e="T88" id="Seg_1252" s="T87">NEG</ta>
            <ta e="T89" id="Seg_1253" s="T88">want-1SG.S</ta>
            <ta e="T90" id="Seg_1254" s="T89">god.[NOM]</ta>
            <ta e="T91" id="Seg_1255" s="T90">get.angry-3SG.S</ta>
            <ta e="T92" id="Seg_1256" s="T91">say-3SG.S</ta>
            <ta e="T93" id="Seg_1257" s="T92">three-EP-ADV.LOC</ta>
            <ta e="T94" id="Seg_1258" s="T93">enough</ta>
            <ta e="T95" id="Seg_1259" s="T94">be-FUT-3SG.S</ta>
            <ta e="T96" id="Seg_1260" s="T95">NEG</ta>
            <ta e="T97" id="Seg_1261" s="T96">three-EP-ADV.LOC</ta>
            <ta e="T98" id="Seg_1262" s="T97">NEG</ta>
            <ta e="T99" id="Seg_1263" s="T98">want-1SG.S</ta>
            <ta e="T100" id="Seg_1264" s="T99">god.[NOM]</ta>
            <ta e="T101" id="Seg_1265" s="T100">get.angry-3SG.S</ta>
            <ta e="T102" id="Seg_1266" s="T101">now</ta>
            <ta e="T103" id="Seg_1267" s="T102">and</ta>
            <ta e="T104" id="Seg_1268" s="T103">all.the.time</ta>
            <ta e="T105" id="Seg_1269" s="T104">fuck-IPFV-HAB-IMP.2SG.S</ta>
            <ta e="T106" id="Seg_1270" s="T105">JUSS</ta>
            <ta e="T107" id="Seg_1271" s="T106">husband.[NOM]-2SG</ta>
            <ta e="T108" id="Seg_1272" s="T107">you.SG.ACC</ta>
            <ta e="T109" id="Seg_1273" s="T108">torment-DUR-HAB.[3SG.S]</ta>
            <ta e="T110" id="Seg_1274" s="T109">JUSS</ta>
            <ta e="T111" id="Seg_1275" s="T110">husband.[NOM]-2SG</ta>
            <ta e="T112" id="Seg_1276" s="T111">torment-DUR-HAB-OPT-3SG.O</ta>
            <ta e="T113" id="Seg_1277" s="T112">husband.[NOM]-2SG</ta>
            <ta e="T114" id="Seg_1278" s="T113">JUSS</ta>
            <ta e="T115" id="Seg_1279" s="T114">you.SG.ACC</ta>
            <ta e="T116" id="Seg_1280" s="T115">torment-DUR-HAB.[3SG.S]</ta>
            <ta e="T117" id="Seg_1281" s="T116">now</ta>
            <ta e="T118" id="Seg_1282" s="T117">woman-ADJZ-human.being-PL.[NOM]</ta>
            <ta e="T119" id="Seg_1283" s="T118">such-ADVZ</ta>
            <ta e="T120" id="Seg_1284" s="T119">and</ta>
            <ta e="T121" id="Seg_1285" s="T120">be-CO-3PL</ta>
            <ta e="T122" id="Seg_1286" s="T121">(s)he-PL-ACC</ta>
            <ta e="T123" id="Seg_1287" s="T122">for.example</ta>
            <ta e="T124" id="Seg_1288" s="T123">every</ta>
            <ta e="T125" id="Seg_1289" s="T124">night-ADV.LOC</ta>
            <ta e="T126" id="Seg_1290" s="T125">fuck-CVB</ta>
            <ta e="T127" id="Seg_1291" s="T126">bring-EP-FRQ-IMP.2SG.O</ta>
            <ta e="T128" id="Seg_1292" s="T127">(s)he-ALL</ta>
            <ta e="T129" id="Seg_1293" s="T128">little-ADVZ</ta>
            <ta e="T130" id="Seg_1294" s="T129">be-FUT-3SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_1295" s="T1">когда</ta>
            <ta e="T3" id="Seg_1296" s="T2">свет.[NOM]</ta>
            <ta e="T4" id="Seg_1297" s="T3">стать-PST.NAR.[3SG.S]</ta>
            <ta e="T5" id="Seg_1298" s="T4">бог.[NOM]</ta>
            <ta e="T6" id="Seg_1299" s="T5">весь</ta>
            <ta e="T7" id="Seg_1300" s="T6">земля-ILL</ta>
            <ta e="T8" id="Seg_1301" s="T7">посылать-PST.NAR-3SG.O</ta>
            <ta e="T9" id="Seg_1302" s="T8">человек-PL-ACC</ta>
            <ta e="T10" id="Seg_1303" s="T9">корова-PL-ACC</ta>
            <ta e="T11" id="Seg_1304" s="T10">лошадь-PL-ACC</ta>
            <ta e="T12" id="Seg_1305" s="T11">овца-PL-ACC</ta>
            <ta e="T13" id="Seg_1306" s="T12">свинья-PL-ACC</ta>
            <ta e="T14" id="Seg_1307" s="T13">спросить-CVB</ta>
            <ta e="T15" id="Seg_1308" s="T14">начать-DRV-EP-PST.NAR.[3SG.S]</ta>
            <ta e="T16" id="Seg_1309" s="T15">позвать-PST.NAR-3SG.O</ta>
            <ta e="T17" id="Seg_1310" s="T16">корова-ACC</ta>
            <ta e="T18" id="Seg_1311" s="T17">ты.ALL</ta>
            <ta e="T19" id="Seg_1312" s="T18">один-EP-ADV.LOC</ta>
            <ta e="T20" id="Seg_1313" s="T19">достаточно</ta>
            <ta e="T21" id="Seg_1314" s="T20">быть-FUT-3SG.S</ta>
            <ta e="T22" id="Seg_1315" s="T21">год-LOC.3SG</ta>
            <ta e="T23" id="Seg_1316" s="T22">а</ta>
            <ta e="T24" id="Seg_1317" s="T23">корова.[NOM]</ta>
            <ta e="T25" id="Seg_1318" s="T24">сказать-3SG.S</ta>
            <ta e="T26" id="Seg_1319" s="T25">достаточно</ta>
            <ta e="T27" id="Seg_1320" s="T26">быть-FUT-3SG.S</ta>
            <ta e="T28" id="Seg_1321" s="T27">теперь</ta>
            <ta e="T29" id="Seg_1322" s="T28">лошадь.[NOM]</ta>
            <ta e="T30" id="Seg_1323" s="T29">позвать-INFER-3SG.O</ta>
            <ta e="T31" id="Seg_1324" s="T30">спросить-CVB</ta>
            <ta e="T32" id="Seg_1325" s="T31">начать-DRV-CO-3SG.S</ta>
            <ta e="T33" id="Seg_1326" s="T32">ты.ALL</ta>
            <ta e="T34" id="Seg_1327" s="T33">один-EP-ADV.LOC</ta>
            <ta e="T35" id="Seg_1328" s="T34">достаточно</ta>
            <ta e="T36" id="Seg_1329" s="T35">быть-FUT-3SG.S</ta>
            <ta e="T37" id="Seg_1330" s="T36">он(а).[NOM]</ta>
            <ta e="T38" id="Seg_1331" s="T37">сказать-3SG.S</ta>
            <ta e="T39" id="Seg_1332" s="T38">достаточно</ta>
            <ta e="T40" id="Seg_1333" s="T39">быть-FUT-3SG.S</ta>
            <ta e="T41" id="Seg_1334" s="T40">позвать-3SG.O</ta>
            <ta e="T42" id="Seg_1335" s="T41">овца-EP-ACC</ta>
            <ta e="T43" id="Seg_1336" s="T42">овца-ALL</ta>
            <ta e="T44" id="Seg_1337" s="T43">спросить-3SG.S</ta>
            <ta e="T45" id="Seg_1338" s="T44">ты.ALL</ta>
            <ta e="T46" id="Seg_1339" s="T45">один-EP-ADV.LOC</ta>
            <ta e="T47" id="Seg_1340" s="T46">достаточно</ta>
            <ta e="T48" id="Seg_1341" s="T47">быть-FUT-3SG.S</ta>
            <ta e="T49" id="Seg_1342" s="T48">достаточно</ta>
            <ta e="T50" id="Seg_1343" s="T49">быть-FUT-3SG.S</ta>
            <ta e="T51" id="Seg_1344" s="T50">позвать-3SG.O</ta>
            <ta e="T52" id="Seg_1345" s="T51">свинья-ACC</ta>
            <ta e="T53" id="Seg_1346" s="T52">спросить-3SG.S</ta>
            <ta e="T54" id="Seg_1347" s="T53">ты.ALL</ta>
            <ta e="T55" id="Seg_1348" s="T54">один-EP-ADV.LOC</ta>
            <ta e="T56" id="Seg_1349" s="T55">достаточно</ta>
            <ta e="T57" id="Seg_1350" s="T56">быть-FUT-3SG.S</ta>
            <ta e="T58" id="Seg_1351" s="T57">NEG</ta>
            <ta e="T59" id="Seg_1352" s="T58">сказать-IMP.2SG.O</ta>
            <ta e="T60" id="Seg_1353" s="T59">я.ALL</ta>
            <ta e="T61" id="Seg_1354" s="T60">два</ta>
            <ta e="T62" id="Seg_1355" s="T61">бог.[NOM]</ta>
            <ta e="T63" id="Seg_1356" s="T62">сказать-3SG.S</ta>
            <ta e="T64" id="Seg_1357" s="T63">ну</ta>
            <ta e="T65" id="Seg_1358" s="T64">JUSS</ta>
            <ta e="T66" id="Seg_1359" s="T65">два-ADV.LOC</ta>
            <ta e="T67" id="Seg_1360" s="T66">позвать-3SG.O</ta>
            <ta e="T68" id="Seg_1361" s="T67">женщина-ADJZ-человек-EP-ACC</ta>
            <ta e="T69" id="Seg_1362" s="T68">женщина-ADJZ-человек-ALL</ta>
            <ta e="T70" id="Seg_1363" s="T69">спросить-CVB</ta>
            <ta e="T71" id="Seg_1364" s="T70">начать-DRV-EP-3SG.S</ta>
            <ta e="T72" id="Seg_1365" s="T71">ты.ALL</ta>
            <ta e="T73" id="Seg_1366" s="T72">один-EP-ADV.LOC</ta>
            <ta e="T74" id="Seg_1367" s="T73">достаточно</ta>
            <ta e="T75" id="Seg_1368" s="T74">быть-FUT-3SG.S</ta>
            <ta e="T76" id="Seg_1369" s="T75">год-LOC.3SG</ta>
            <ta e="T77" id="Seg_1370" s="T76">NEG</ta>
            <ta e="T78" id="Seg_1371" s="T77">достаточно</ta>
            <ta e="T79" id="Seg_1372" s="T78">NEG</ta>
            <ta e="T80" id="Seg_1373" s="T79">быть-FUT-3SG.S</ta>
            <ta e="T81" id="Seg_1374" s="T80">бог.[NOM]</ta>
            <ta e="T82" id="Seg_1375" s="T81">сказать-3SG.S</ta>
            <ta e="T83" id="Seg_1376" s="T82">два-ADV.LOC</ta>
            <ta e="T84" id="Seg_1377" s="T83">достаточно</ta>
            <ta e="T85" id="Seg_1378" s="T84">быть-FUT-3SG.S</ta>
            <ta e="T86" id="Seg_1379" s="T85">NEG</ta>
            <ta e="T87" id="Seg_1380" s="T86">два-ADV.LOC</ta>
            <ta e="T88" id="Seg_1381" s="T87">NEG</ta>
            <ta e="T89" id="Seg_1382" s="T88">хотеть-1SG.S</ta>
            <ta e="T90" id="Seg_1383" s="T89">бог.[NOM]</ta>
            <ta e="T91" id="Seg_1384" s="T90">рассердиться-3SG.S</ta>
            <ta e="T92" id="Seg_1385" s="T91">сказать-3SG.S</ta>
            <ta e="T93" id="Seg_1386" s="T92">три-EP-ADV.LOC</ta>
            <ta e="T94" id="Seg_1387" s="T93">достаточно</ta>
            <ta e="T95" id="Seg_1388" s="T94">быть-FUT-3SG.S</ta>
            <ta e="T96" id="Seg_1389" s="T95">NEG</ta>
            <ta e="T97" id="Seg_1390" s="T96">три-EP-ADV.LOC</ta>
            <ta e="T98" id="Seg_1391" s="T97">NEG</ta>
            <ta e="T99" id="Seg_1392" s="T98">хотеть-1SG.S</ta>
            <ta e="T100" id="Seg_1393" s="T99">бог.[NOM]</ta>
            <ta e="T101" id="Seg_1394" s="T100">рассердиться-3SG.S</ta>
            <ta e="T102" id="Seg_1395" s="T101">ну</ta>
            <ta e="T103" id="Seg_1396" s="T102">и</ta>
            <ta e="T104" id="Seg_1397" s="T103">век</ta>
            <ta e="T105" id="Seg_1398" s="T104">соединяться.с.женщиной-IPFV-HAB-IMP.2SG.S</ta>
            <ta e="T106" id="Seg_1399" s="T105">JUSS</ta>
            <ta e="T107" id="Seg_1400" s="T106">муж.[NOM]-2SG</ta>
            <ta e="T108" id="Seg_1401" s="T107">ты.ACC</ta>
            <ta e="T109" id="Seg_1402" s="T108">мучить-DUR-HAB.[3SG.S]</ta>
            <ta e="T110" id="Seg_1403" s="T109">JUSS</ta>
            <ta e="T111" id="Seg_1404" s="T110">муж.[NOM]-2SG</ta>
            <ta e="T112" id="Seg_1405" s="T111">мучить-DUR-HAB-OPT-3SG.O</ta>
            <ta e="T113" id="Seg_1406" s="T112">муж.[NOM]-2SG</ta>
            <ta e="T114" id="Seg_1407" s="T113">JUSS</ta>
            <ta e="T115" id="Seg_1408" s="T114">ты.ACC</ta>
            <ta e="T116" id="Seg_1409" s="T115">мучить-DUR-HAB.[3SG.S]</ta>
            <ta e="T117" id="Seg_1410" s="T116">теперь</ta>
            <ta e="T118" id="Seg_1411" s="T117">женщина-ADJZ-человек-PL.[NOM]</ta>
            <ta e="T119" id="Seg_1412" s="T118">такой-ADVZ</ta>
            <ta e="T120" id="Seg_1413" s="T119">и</ta>
            <ta e="T121" id="Seg_1414" s="T120">быть-CO-3PL</ta>
            <ta e="T122" id="Seg_1415" s="T121">он(а)-PL-ACC</ta>
            <ta e="T123" id="Seg_1416" s="T122">хоть</ta>
            <ta e="T124" id="Seg_1417" s="T123">каждый</ta>
            <ta e="T125" id="Seg_1418" s="T124">ночь-ADV.LOC</ta>
            <ta e="T126" id="Seg_1419" s="T125">соединяться.с.женщиной-CVB</ta>
            <ta e="T127" id="Seg_1420" s="T126">принести-EP-FRQ-IMP.2SG.O</ta>
            <ta e="T128" id="Seg_1421" s="T127">он(а)-ALL</ta>
            <ta e="T129" id="Seg_1422" s="T128">мало-ADVZ</ta>
            <ta e="T130" id="Seg_1423" s="T129">быть-FUT-3SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1424" s="T1">conj</ta>
            <ta e="T3" id="Seg_1425" s="T2">n.[n:case]</ta>
            <ta e="T4" id="Seg_1426" s="T3">v-v:tense.[v:pn]</ta>
            <ta e="T5" id="Seg_1427" s="T4">n.[n:case]</ta>
            <ta e="T6" id="Seg_1428" s="T5">quant</ta>
            <ta e="T7" id="Seg_1429" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_1430" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_1431" s="T8">n-n:num-n:case</ta>
            <ta e="T10" id="Seg_1432" s="T9">n-n:num-n:case</ta>
            <ta e="T11" id="Seg_1433" s="T10">n-n:num-n:case</ta>
            <ta e="T12" id="Seg_1434" s="T11">n-n:num-n:case</ta>
            <ta e="T13" id="Seg_1435" s="T12">n-n:num-n:case</ta>
            <ta e="T14" id="Seg_1436" s="T13">v-v&gt;adv</ta>
            <ta e="T15" id="Seg_1437" s="T14">v-v&gt;v-v:ins-v:tense.[v:pn]</ta>
            <ta e="T16" id="Seg_1438" s="T15">v-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_1439" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_1440" s="T17">pers</ta>
            <ta e="T19" id="Seg_1441" s="T18">num-n:ins-n&gt;adv</ta>
            <ta e="T20" id="Seg_1442" s="T19">adv</ta>
            <ta e="T21" id="Seg_1443" s="T20">v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_1444" s="T21">n-n:case.poss</ta>
            <ta e="T23" id="Seg_1445" s="T22">conj</ta>
            <ta e="T24" id="Seg_1446" s="T23">n.[n:case]</ta>
            <ta e="T25" id="Seg_1447" s="T24">v-v:pn</ta>
            <ta e="T26" id="Seg_1448" s="T25">adv</ta>
            <ta e="T27" id="Seg_1449" s="T26">v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_1450" s="T27">adv</ta>
            <ta e="T29" id="Seg_1451" s="T28">n.[n:case]</ta>
            <ta e="T30" id="Seg_1452" s="T29">v-v:mood-v:pn</ta>
            <ta e="T31" id="Seg_1453" s="T30">v-v&gt;adv</ta>
            <ta e="T32" id="Seg_1454" s="T31">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T33" id="Seg_1455" s="T32">pers</ta>
            <ta e="T34" id="Seg_1456" s="T33">num-n:ins-n&gt;adv</ta>
            <ta e="T35" id="Seg_1457" s="T34">adv</ta>
            <ta e="T36" id="Seg_1458" s="T35">v-v:tense-v:pn</ta>
            <ta e="T37" id="Seg_1459" s="T36">pers.[n:case]</ta>
            <ta e="T38" id="Seg_1460" s="T37">v-v:pn</ta>
            <ta e="T39" id="Seg_1461" s="T38">adv</ta>
            <ta e="T40" id="Seg_1462" s="T39">v-v:tense-v:pn</ta>
            <ta e="T41" id="Seg_1463" s="T40">v-v:pn</ta>
            <ta e="T42" id="Seg_1464" s="T41">n-n:ins-n:case</ta>
            <ta e="T43" id="Seg_1465" s="T42">n-n:case</ta>
            <ta e="T44" id="Seg_1466" s="T43">v-v:pn</ta>
            <ta e="T45" id="Seg_1467" s="T44">pers</ta>
            <ta e="T46" id="Seg_1468" s="T45">num-n:ins-n&gt;adv</ta>
            <ta e="T47" id="Seg_1469" s="T46">adv</ta>
            <ta e="T48" id="Seg_1470" s="T47">v-v:tense-v:pn</ta>
            <ta e="T49" id="Seg_1471" s="T48">adv</ta>
            <ta e="T50" id="Seg_1472" s="T49">v-v:tense-v:pn</ta>
            <ta e="T51" id="Seg_1473" s="T50">v-v:pn</ta>
            <ta e="T52" id="Seg_1474" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_1475" s="T52">v-v:pn</ta>
            <ta e="T54" id="Seg_1476" s="T53">pers</ta>
            <ta e="T55" id="Seg_1477" s="T54">num-n:ins-n&gt;adv</ta>
            <ta e="T56" id="Seg_1478" s="T55">adv</ta>
            <ta e="T57" id="Seg_1479" s="T56">v-v:tense-v:pn</ta>
            <ta e="T58" id="Seg_1480" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_1481" s="T58">v-v:mood.pn</ta>
            <ta e="T60" id="Seg_1482" s="T59">pers</ta>
            <ta e="T61" id="Seg_1483" s="T60">num</ta>
            <ta e="T62" id="Seg_1484" s="T61">n.[n:case]</ta>
            <ta e="T63" id="Seg_1485" s="T62">v-v:pn</ta>
            <ta e="T64" id="Seg_1486" s="T63">ptcl</ta>
            <ta e="T65" id="Seg_1487" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_1488" s="T65">num-n&gt;adv</ta>
            <ta e="T67" id="Seg_1489" s="T66">v-v:pn</ta>
            <ta e="T68" id="Seg_1490" s="T67">n-n&gt;adj-n-n:ins-n:case</ta>
            <ta e="T69" id="Seg_1491" s="T68">n-n&gt;adj-n-n:case</ta>
            <ta e="T70" id="Seg_1492" s="T69">v-v&gt;adv</ta>
            <ta e="T71" id="Seg_1493" s="T70">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T72" id="Seg_1494" s="T71">pers</ta>
            <ta e="T73" id="Seg_1495" s="T72">num-n:ins-n&gt;adv</ta>
            <ta e="T74" id="Seg_1496" s="T73">adv</ta>
            <ta e="T75" id="Seg_1497" s="T74">v-v:tense-v:pn</ta>
            <ta e="T76" id="Seg_1498" s="T75">n-n:case.poss</ta>
            <ta e="T77" id="Seg_1499" s="T76">ptcl</ta>
            <ta e="T78" id="Seg_1500" s="T77">adv</ta>
            <ta e="T79" id="Seg_1501" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_1502" s="T79">v-v:tense-v:pn</ta>
            <ta e="T81" id="Seg_1503" s="T80">n.[n:case]</ta>
            <ta e="T82" id="Seg_1504" s="T81">v-v:pn</ta>
            <ta e="T83" id="Seg_1505" s="T82">num-n&gt;adv</ta>
            <ta e="T84" id="Seg_1506" s="T83">adv</ta>
            <ta e="T85" id="Seg_1507" s="T84">v-v:tense-v:pn</ta>
            <ta e="T86" id="Seg_1508" s="T85">ptcl</ta>
            <ta e="T87" id="Seg_1509" s="T86">num-n&gt;adv</ta>
            <ta e="T88" id="Seg_1510" s="T87">ptcl</ta>
            <ta e="T89" id="Seg_1511" s="T88">v-v:pn</ta>
            <ta e="T90" id="Seg_1512" s="T89">n.[n:case]</ta>
            <ta e="T91" id="Seg_1513" s="T90">v-v:pn</ta>
            <ta e="T92" id="Seg_1514" s="T91">v-v:pn</ta>
            <ta e="T93" id="Seg_1515" s="T92">num-n:ins-n&gt;adv</ta>
            <ta e="T94" id="Seg_1516" s="T93">adv</ta>
            <ta e="T95" id="Seg_1517" s="T94">v-v:tense-v:pn</ta>
            <ta e="T96" id="Seg_1518" s="T95">ptcl</ta>
            <ta e="T97" id="Seg_1519" s="T96">num-n:ins-n&gt;adv</ta>
            <ta e="T98" id="Seg_1520" s="T97">ptcl</ta>
            <ta e="T99" id="Seg_1521" s="T98">v-v:pn</ta>
            <ta e="T100" id="Seg_1522" s="T99">n.[n:case]</ta>
            <ta e="T101" id="Seg_1523" s="T100">v-v:pn</ta>
            <ta e="T102" id="Seg_1524" s="T101">ptcl</ta>
            <ta e="T103" id="Seg_1525" s="T102">conj</ta>
            <ta e="T104" id="Seg_1526" s="T103">adv</ta>
            <ta e="T105" id="Seg_1527" s="T104">v-v&gt;v-v&gt;v-v:mood.pn</ta>
            <ta e="T106" id="Seg_1528" s="T105">ptcl</ta>
            <ta e="T107" id="Seg_1529" s="T106">n.[n:case]-n:poss</ta>
            <ta e="T108" id="Seg_1530" s="T107">pers</ta>
            <ta e="T109" id="Seg_1531" s="T108">v-v&gt;v-v&gt;v.[v:pn]</ta>
            <ta e="T110" id="Seg_1532" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_1533" s="T110">n.[n:case]-n:poss</ta>
            <ta e="T112" id="Seg_1534" s="T111">v-v&gt;v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T113" id="Seg_1535" s="T112">n.[n:case]-n:poss</ta>
            <ta e="T114" id="Seg_1536" s="T113">ptcl</ta>
            <ta e="T115" id="Seg_1537" s="T114">pers</ta>
            <ta e="T116" id="Seg_1538" s="T115">v-v&gt;v-v&gt;v.[v:pn]</ta>
            <ta e="T117" id="Seg_1539" s="T116">adv</ta>
            <ta e="T118" id="Seg_1540" s="T117">n-n&gt;adj-n-n:num.[n:case]</ta>
            <ta e="T119" id="Seg_1541" s="T118">adj-adj&gt;adv</ta>
            <ta e="T120" id="Seg_1542" s="T119">conj</ta>
            <ta e="T121" id="Seg_1543" s="T120">v-v:ins-v:pn</ta>
            <ta e="T122" id="Seg_1544" s="T121">pers-n:num-n:case</ta>
            <ta e="T123" id="Seg_1545" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_1546" s="T123">adj</ta>
            <ta e="T125" id="Seg_1547" s="T124">n-adv:case</ta>
            <ta e="T126" id="Seg_1548" s="T125">v-v&gt;adv</ta>
            <ta e="T127" id="Seg_1549" s="T126">v-v:ins-v&gt;v-v:mood.pn</ta>
            <ta e="T128" id="Seg_1550" s="T127">pers-n:case</ta>
            <ta e="T129" id="Seg_1551" s="T128">adv-adj&gt;adv</ta>
            <ta e="T130" id="Seg_1552" s="T129">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1553" s="T1">conj</ta>
            <ta e="T3" id="Seg_1554" s="T2">n</ta>
            <ta e="T4" id="Seg_1555" s="T3">v</ta>
            <ta e="T5" id="Seg_1556" s="T4">n</ta>
            <ta e="T6" id="Seg_1557" s="T5">quant</ta>
            <ta e="T7" id="Seg_1558" s="T6">n</ta>
            <ta e="T8" id="Seg_1559" s="T7">v</ta>
            <ta e="T9" id="Seg_1560" s="T8">n</ta>
            <ta e="T10" id="Seg_1561" s="T9">n</ta>
            <ta e="T11" id="Seg_1562" s="T10">n</ta>
            <ta e="T12" id="Seg_1563" s="T11">n</ta>
            <ta e="T13" id="Seg_1564" s="T12">n</ta>
            <ta e="T14" id="Seg_1565" s="T13">adv</ta>
            <ta e="T15" id="Seg_1566" s="T14">v</ta>
            <ta e="T16" id="Seg_1567" s="T15">v</ta>
            <ta e="T17" id="Seg_1568" s="T16">n</ta>
            <ta e="T18" id="Seg_1569" s="T17">pers</ta>
            <ta e="T19" id="Seg_1570" s="T18">adv</ta>
            <ta e="T20" id="Seg_1571" s="T19">adv</ta>
            <ta e="T21" id="Seg_1572" s="T20">v</ta>
            <ta e="T22" id="Seg_1573" s="T21">n</ta>
            <ta e="T23" id="Seg_1574" s="T22">conj</ta>
            <ta e="T24" id="Seg_1575" s="T23">n</ta>
            <ta e="T25" id="Seg_1576" s="T24">v</ta>
            <ta e="T26" id="Seg_1577" s="T25">adv</ta>
            <ta e="T27" id="Seg_1578" s="T26">v</ta>
            <ta e="T28" id="Seg_1579" s="T27">adv</ta>
            <ta e="T29" id="Seg_1580" s="T28">n</ta>
            <ta e="T30" id="Seg_1581" s="T29">v</ta>
            <ta e="T31" id="Seg_1582" s="T30">adv</ta>
            <ta e="T32" id="Seg_1583" s="T31">v</ta>
            <ta e="T33" id="Seg_1584" s="T32">pers</ta>
            <ta e="T34" id="Seg_1585" s="T33">adv</ta>
            <ta e="T35" id="Seg_1586" s="T34">adv</ta>
            <ta e="T36" id="Seg_1587" s="T35">v</ta>
            <ta e="T37" id="Seg_1588" s="T36">pers</ta>
            <ta e="T38" id="Seg_1589" s="T37">v</ta>
            <ta e="T39" id="Seg_1590" s="T38">adv</ta>
            <ta e="T40" id="Seg_1591" s="T39">v</ta>
            <ta e="T41" id="Seg_1592" s="T40">v</ta>
            <ta e="T42" id="Seg_1593" s="T41">n</ta>
            <ta e="T43" id="Seg_1594" s="T42">n</ta>
            <ta e="T44" id="Seg_1595" s="T43">v</ta>
            <ta e="T45" id="Seg_1596" s="T44">pers</ta>
            <ta e="T46" id="Seg_1597" s="T45">adv</ta>
            <ta e="T47" id="Seg_1598" s="T46">adv</ta>
            <ta e="T48" id="Seg_1599" s="T47">v</ta>
            <ta e="T49" id="Seg_1600" s="T48">adv</ta>
            <ta e="T50" id="Seg_1601" s="T49">v</ta>
            <ta e="T51" id="Seg_1602" s="T50">v</ta>
            <ta e="T52" id="Seg_1603" s="T51">n</ta>
            <ta e="T53" id="Seg_1604" s="T52">v</ta>
            <ta e="T54" id="Seg_1605" s="T53">pers</ta>
            <ta e="T55" id="Seg_1606" s="T54">adv</ta>
            <ta e="T56" id="Seg_1607" s="T55">adv</ta>
            <ta e="T57" id="Seg_1608" s="T56">v</ta>
            <ta e="T58" id="Seg_1609" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_1610" s="T58">v</ta>
            <ta e="T60" id="Seg_1611" s="T59">pers</ta>
            <ta e="T61" id="Seg_1612" s="T60">num</ta>
            <ta e="T62" id="Seg_1613" s="T61">n</ta>
            <ta e="T63" id="Seg_1614" s="T62">v</ta>
            <ta e="T64" id="Seg_1615" s="T63">ptcl</ta>
            <ta e="T65" id="Seg_1616" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_1617" s="T65">num</ta>
            <ta e="T67" id="Seg_1618" s="T66">v</ta>
            <ta e="T68" id="Seg_1619" s="T67">n</ta>
            <ta e="T69" id="Seg_1620" s="T68">adj</ta>
            <ta e="T70" id="Seg_1621" s="T69">adv</ta>
            <ta e="T71" id="Seg_1622" s="T70">v</ta>
            <ta e="T72" id="Seg_1623" s="T71">pers</ta>
            <ta e="T73" id="Seg_1624" s="T72">adv</ta>
            <ta e="T74" id="Seg_1625" s="T73">adv</ta>
            <ta e="T75" id="Seg_1626" s="T74">v</ta>
            <ta e="T76" id="Seg_1627" s="T75">n</ta>
            <ta e="T77" id="Seg_1628" s="T76">ptcl</ta>
            <ta e="T78" id="Seg_1629" s="T77">adv</ta>
            <ta e="T79" id="Seg_1630" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_1631" s="T79">v</ta>
            <ta e="T81" id="Seg_1632" s="T80">n</ta>
            <ta e="T82" id="Seg_1633" s="T81">v</ta>
            <ta e="T83" id="Seg_1634" s="T82">adv</ta>
            <ta e="T84" id="Seg_1635" s="T83">adv</ta>
            <ta e="T85" id="Seg_1636" s="T84">v</ta>
            <ta e="T86" id="Seg_1637" s="T85">ptcl</ta>
            <ta e="T87" id="Seg_1638" s="T86">num</ta>
            <ta e="T88" id="Seg_1639" s="T87">ptcl</ta>
            <ta e="T89" id="Seg_1640" s="T88">v</ta>
            <ta e="T90" id="Seg_1641" s="T89">n</ta>
            <ta e="T91" id="Seg_1642" s="T90">v</ta>
            <ta e="T92" id="Seg_1643" s="T91">v</ta>
            <ta e="T93" id="Seg_1644" s="T92">num</ta>
            <ta e="T94" id="Seg_1645" s="T93">adv</ta>
            <ta e="T95" id="Seg_1646" s="T94">v</ta>
            <ta e="T96" id="Seg_1647" s="T95">ptcl</ta>
            <ta e="T97" id="Seg_1648" s="T96">adv</ta>
            <ta e="T98" id="Seg_1649" s="T97">ptcl</ta>
            <ta e="T99" id="Seg_1650" s="T98">v</ta>
            <ta e="T100" id="Seg_1651" s="T99">n</ta>
            <ta e="T101" id="Seg_1652" s="T100">v</ta>
            <ta e="T102" id="Seg_1653" s="T101">ptcl</ta>
            <ta e="T103" id="Seg_1654" s="T102">conj</ta>
            <ta e="T104" id="Seg_1655" s="T103">adv</ta>
            <ta e="T105" id="Seg_1656" s="T104">v</ta>
            <ta e="T106" id="Seg_1657" s="T105">ptcl</ta>
            <ta e="T107" id="Seg_1658" s="T106">n</ta>
            <ta e="T108" id="Seg_1659" s="T107">pers</ta>
            <ta e="T109" id="Seg_1660" s="T108">v</ta>
            <ta e="T110" id="Seg_1661" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_1662" s="T110">n</ta>
            <ta e="T112" id="Seg_1663" s="T111">v</ta>
            <ta e="T113" id="Seg_1664" s="T112">n</ta>
            <ta e="T114" id="Seg_1665" s="T113">ptcl</ta>
            <ta e="T115" id="Seg_1666" s="T114">pers</ta>
            <ta e="T116" id="Seg_1667" s="T115">v</ta>
            <ta e="T117" id="Seg_1668" s="T116">adv</ta>
            <ta e="T118" id="Seg_1669" s="T117">n</ta>
            <ta e="T119" id="Seg_1670" s="T118">adv</ta>
            <ta e="T120" id="Seg_1671" s="T119">conj</ta>
            <ta e="T121" id="Seg_1672" s="T120">v</ta>
            <ta e="T122" id="Seg_1673" s="T121">pers</ta>
            <ta e="T123" id="Seg_1674" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_1675" s="T123">adj</ta>
            <ta e="T125" id="Seg_1676" s="T124">n</ta>
            <ta e="T126" id="Seg_1677" s="T125">adv</ta>
            <ta e="T127" id="Seg_1678" s="T126">v</ta>
            <ta e="T128" id="Seg_1679" s="T127">pers</ta>
            <ta e="T129" id="Seg_1680" s="T128">adv</ta>
            <ta e="T130" id="Seg_1681" s="T129">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_1682" s="T2">np:Th</ta>
            <ta e="T5" id="Seg_1683" s="T4">np.h:A</ta>
            <ta e="T6" id="Seg_1684" s="T5">pro.h:Th</ta>
            <ta e="T7" id="Seg_1685" s="T6">np:G</ta>
            <ta e="T15" id="Seg_1686" s="T14">0.3.h:A</ta>
            <ta e="T16" id="Seg_1687" s="T15">0.3.h:A</ta>
            <ta e="T17" id="Seg_1688" s="T16">np.h:Th</ta>
            <ta e="T19" id="Seg_1689" s="T18">adv:Time</ta>
            <ta e="T21" id="Seg_1690" s="T20">0.3:Th</ta>
            <ta e="T22" id="Seg_1691" s="T21">np:Time</ta>
            <ta e="T24" id="Seg_1692" s="T23">np.h:A</ta>
            <ta e="T27" id="Seg_1693" s="T26">0.3:Th</ta>
            <ta e="T28" id="Seg_1694" s="T27">adv:Time</ta>
            <ta e="T29" id="Seg_1695" s="T28">np.h:Th</ta>
            <ta e="T30" id="Seg_1696" s="T29">0.3.h:A</ta>
            <ta e="T32" id="Seg_1697" s="T31">0.3.h:A</ta>
            <ta e="T34" id="Seg_1698" s="T33">adv:Time</ta>
            <ta e="T36" id="Seg_1699" s="T35">0.3:Th</ta>
            <ta e="T37" id="Seg_1700" s="T36">pro.h:A</ta>
            <ta e="T40" id="Seg_1701" s="T39">0.3:Th</ta>
            <ta e="T41" id="Seg_1702" s="T40">0.3.h:A</ta>
            <ta e="T42" id="Seg_1703" s="T41">np.h:Th</ta>
            <ta e="T43" id="Seg_1704" s="T42">np.h:R</ta>
            <ta e="T44" id="Seg_1705" s="T43">0.3.h:A</ta>
            <ta e="T46" id="Seg_1706" s="T45">adv:Time</ta>
            <ta e="T48" id="Seg_1707" s="T47">0.3:Th</ta>
            <ta e="T50" id="Seg_1708" s="T49">0.3:Th</ta>
            <ta e="T51" id="Seg_1709" s="T50">0.3.h:A</ta>
            <ta e="T52" id="Seg_1710" s="T51">np.h:Th</ta>
            <ta e="T53" id="Seg_1711" s="T52">0.3.h:A</ta>
            <ta e="T55" id="Seg_1712" s="T54">adv:Time</ta>
            <ta e="T57" id="Seg_1713" s="T56">0.3:Th</ta>
            <ta e="T59" id="Seg_1714" s="T58">0.2.h:A</ta>
            <ta e="T60" id="Seg_1715" s="T59">pro.h:R</ta>
            <ta e="T62" id="Seg_1716" s="T61">np.h:A</ta>
            <ta e="T66" id="Seg_1717" s="T65">np:Time</ta>
            <ta e="T67" id="Seg_1718" s="T66">0.3.h:A</ta>
            <ta e="T68" id="Seg_1719" s="T67">np.h:Th</ta>
            <ta e="T69" id="Seg_1720" s="T68">np.h:R</ta>
            <ta e="T71" id="Seg_1721" s="T70">0.3.h:A</ta>
            <ta e="T73" id="Seg_1722" s="T72">adv:Time</ta>
            <ta e="T75" id="Seg_1723" s="T74">0.3:Th</ta>
            <ta e="T76" id="Seg_1724" s="T75">np:Time</ta>
            <ta e="T80" id="Seg_1725" s="T79">0.3:Th</ta>
            <ta e="T81" id="Seg_1726" s="T80">np.h:A</ta>
            <ta e="T83" id="Seg_1727" s="T82">adv:Time</ta>
            <ta e="T85" id="Seg_1728" s="T84">0.3:Th</ta>
            <ta e="T87" id="Seg_1729" s="T86">adv:Time</ta>
            <ta e="T89" id="Seg_1730" s="T88">0.1.h:E</ta>
            <ta e="T90" id="Seg_1731" s="T89">np.h:E</ta>
            <ta e="T92" id="Seg_1732" s="T91">0.3.h:A</ta>
            <ta e="T93" id="Seg_1733" s="T92">adv:Time</ta>
            <ta e="T95" id="Seg_1734" s="T94">0.3:Th</ta>
            <ta e="T97" id="Seg_1735" s="T96">adv:Time</ta>
            <ta e="T99" id="Seg_1736" s="T98">0.1.h:E</ta>
            <ta e="T100" id="Seg_1737" s="T99">np.h:E</ta>
            <ta e="T104" id="Seg_1738" s="T103">adv:Time</ta>
            <ta e="T105" id="Seg_1739" s="T104">0.2.h:A</ta>
            <ta e="T107" id="Seg_1740" s="T106">np.h:A 0.2.h:Poss</ta>
            <ta e="T108" id="Seg_1741" s="T107">pro.h:P</ta>
            <ta e="T111" id="Seg_1742" s="T110">np.h:A 0.2.h:Poss</ta>
            <ta e="T112" id="Seg_1743" s="T111">0.2.h:P</ta>
            <ta e="T113" id="Seg_1744" s="T112">np.h:A 0.2.h:Poss</ta>
            <ta e="T115" id="Seg_1745" s="T114">pro.h:P</ta>
            <ta e="T117" id="Seg_1746" s="T116">adv:Time</ta>
            <ta e="T118" id="Seg_1747" s="T117">np.h:Th</ta>
            <ta e="T122" id="Seg_1748" s="T121">pro.h:Th</ta>
            <ta e="T125" id="Seg_1749" s="T124">adv:Time</ta>
            <ta e="T127" id="Seg_1750" s="T126">0.2.h:A</ta>
            <ta e="T130" id="Seg_1751" s="T129">0.3:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_1752" s="T1">s:temp</ta>
            <ta e="T5" id="Seg_1753" s="T4">np.h:S</ta>
            <ta e="T6" id="Seg_1754" s="T5">pro.h:O</ta>
            <ta e="T8" id="Seg_1755" s="T7">v:pred</ta>
            <ta e="T15" id="Seg_1756" s="T14">0.3.h:S v:pred</ta>
            <ta e="T16" id="Seg_1757" s="T15">0.3.h:S v:pred</ta>
            <ta e="T17" id="Seg_1758" s="T16">np.h:O</ta>
            <ta e="T21" id="Seg_1759" s="T20">0.3:S v:pred</ta>
            <ta e="T24" id="Seg_1760" s="T23">np.h:S</ta>
            <ta e="T25" id="Seg_1761" s="T24">v:pred</ta>
            <ta e="T27" id="Seg_1762" s="T26">0.3:S v:pred</ta>
            <ta e="T29" id="Seg_1763" s="T28">np.h:O</ta>
            <ta e="T30" id="Seg_1764" s="T29">0.3.h:S v:pred</ta>
            <ta e="T32" id="Seg_1765" s="T31">0.3.h:S v:pred</ta>
            <ta e="T36" id="Seg_1766" s="T35">0.3:S v:pred</ta>
            <ta e="T37" id="Seg_1767" s="T36">pro.h:S</ta>
            <ta e="T38" id="Seg_1768" s="T37">v:pred</ta>
            <ta e="T40" id="Seg_1769" s="T39">0.3:S v:pred</ta>
            <ta e="T41" id="Seg_1770" s="T40">0.3.h:S v:pred</ta>
            <ta e="T42" id="Seg_1771" s="T41">np.h:O</ta>
            <ta e="T44" id="Seg_1772" s="T43">0.3.h:S v:pred</ta>
            <ta e="T48" id="Seg_1773" s="T47">0.3:S v:pred</ta>
            <ta e="T50" id="Seg_1774" s="T49">0.3:S v:pted</ta>
            <ta e="T51" id="Seg_1775" s="T50">0.3.h:S v:pred</ta>
            <ta e="T52" id="Seg_1776" s="T51">np.h:O</ta>
            <ta e="T53" id="Seg_1777" s="T52">0.3.h:S v:pred</ta>
            <ta e="T57" id="Seg_1778" s="T56">0.3:S v:pred</ta>
            <ta e="T59" id="Seg_1779" s="T58">0.2.h:S v:pred</ta>
            <ta e="T62" id="Seg_1780" s="T61">np.h:S</ta>
            <ta e="T63" id="Seg_1781" s="T62">v:pred</ta>
            <ta e="T67" id="Seg_1782" s="T66">0.3.h:S v:pred</ta>
            <ta e="T68" id="Seg_1783" s="T67">np.h:O</ta>
            <ta e="T71" id="Seg_1784" s="T70">0.3.h:S v:pred</ta>
            <ta e="T75" id="Seg_1785" s="T74">0.3:S v:pred</ta>
            <ta e="T80" id="Seg_1786" s="T79">0.3:S v:pred</ta>
            <ta e="T81" id="Seg_1787" s="T80">np.h:S</ta>
            <ta e="T82" id="Seg_1788" s="T81">v:pred</ta>
            <ta e="T85" id="Seg_1789" s="T84">0.3:S v:pred</ta>
            <ta e="T89" id="Seg_1790" s="T88">0.1.h:S v:pred</ta>
            <ta e="T90" id="Seg_1791" s="T89">np.h:S</ta>
            <ta e="T91" id="Seg_1792" s="T90">v:pred</ta>
            <ta e="T92" id="Seg_1793" s="T91">0.3.h:S v:pred</ta>
            <ta e="T95" id="Seg_1794" s="T94">0.3:S v:pred</ta>
            <ta e="T99" id="Seg_1795" s="T98">0.1.h:S v:pred</ta>
            <ta e="T100" id="Seg_1796" s="T99">np.h:S</ta>
            <ta e="T101" id="Seg_1797" s="T100">v:pred</ta>
            <ta e="T105" id="Seg_1798" s="T104">0.2.h:S v:pred</ta>
            <ta e="T107" id="Seg_1799" s="T106">np.h:S</ta>
            <ta e="T108" id="Seg_1800" s="T107">pro.h:O</ta>
            <ta e="T109" id="Seg_1801" s="T108">v:pred</ta>
            <ta e="T111" id="Seg_1802" s="T110">np.h:S</ta>
            <ta e="T112" id="Seg_1803" s="T111">v:pred 0.2.h:O</ta>
            <ta e="T113" id="Seg_1804" s="T112">np.h:S</ta>
            <ta e="T115" id="Seg_1805" s="T114">pro.h:O</ta>
            <ta e="T116" id="Seg_1806" s="T115">v:pred</ta>
            <ta e="T118" id="Seg_1807" s="T117">np.h:S</ta>
            <ta e="T121" id="Seg_1808" s="T120">v:pred</ta>
            <ta e="T122" id="Seg_1809" s="T121">pro.h:O</ta>
            <ta e="T127" id="Seg_1810" s="T126">0.2.h:S v:pred</ta>
            <ta e="T130" id="Seg_1811" s="T129">0.3:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T3" id="Seg_1812" s="T2">RUS:core</ta>
            <ta e="T6" id="Seg_1813" s="T5">RUS:core</ta>
            <ta e="T13" id="Seg_1814" s="T12">RUS:cult</ta>
            <ta e="T23" id="Seg_1815" s="T22">RUS:gram</ta>
            <ta e="T28" id="Seg_1816" s="T27">RUS:core</ta>
            <ta e="T52" id="Seg_1817" s="T51">RUS:cult</ta>
            <ta e="T64" id="Seg_1818" s="T63">RUS:disc</ta>
            <ta e="T65" id="Seg_1819" s="T64">RUS:gram</ta>
            <ta e="T102" id="Seg_1820" s="T101">RUS:disc</ta>
            <ta e="T103" id="Seg_1821" s="T102">RUS:gram</ta>
            <ta e="T104" id="Seg_1822" s="T103">RUS:core</ta>
            <ta e="T106" id="Seg_1823" s="T105">RUS:gram</ta>
            <ta e="T110" id="Seg_1824" s="T109">RUS:gram</ta>
            <ta e="T114" id="Seg_1825" s="T113">RUS:gram</ta>
            <ta e="T117" id="Seg_1826" s="T116">RUS:core</ta>
            <ta e="T120" id="Seg_1827" s="T119">RUS:gram</ta>
            <ta e="T123" id="Seg_1828" s="T122">RUS:disc</ta>
            <ta e="T124" id="Seg_1829" s="T123">RUS:core</ta>
            <ta e="T129" id="Seg_1830" s="T128">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T13" id="Seg_1831" s="T1">When light had appeared, God sent everyone to the Earth: people, horses, sheep, pigs.</ta>
            <ta e="T15" id="Seg_1832" s="T13">He began asking them.</ta>
            <ta e="T22" id="Seg_1833" s="T15">He called a cow: “Will one time per year be enough for you?”</ta>
            <ta e="T27" id="Seg_1834" s="T22">And the cow said: “It'll be enough.”</ta>
            <ta e="T30" id="Seg_1835" s="T27">Then he called a horse.</ta>
            <ta e="T36" id="Seg_1836" s="T30">He began asking it: “Will one time be enough for you?”</ta>
            <ta e="T40" id="Seg_1837" s="T36">It said: “It'll be enough.”</ta>
            <ta e="T42" id="Seg_1838" s="T40">He called a sheep.</ta>
            <ta e="T48" id="Seg_1839" s="T42">He asked the sheep: “Will one time be enough for you?”</ta>
            <ta e="T50" id="Seg_1840" s="T48">“It'll be enough.”</ta>
            <ta e="T52" id="Seg_1841" s="T50">He called a pig.</ta>
            <ta e="T57" id="Seg_1842" s="T52">He asked: “Will one time be enough for you?”</ta>
            <ta e="T58" id="Seg_1843" s="T57">“No!</ta>
            <ta e="T61" id="Seg_1844" s="T58">Tell me: two times.”</ta>
            <ta e="T66" id="Seg_1845" s="T61">God said: “Let it be two times.”</ta>
            <ta e="T68" id="Seg_1846" s="T66">He called a woman.</ta>
            <ta e="T76" id="Seg_1847" s="T68">He began asking the woman: “Will one time be enough for you?”</ta>
            <ta e="T80" id="Seg_1848" s="T76">“No. it won't be enough.”</ta>
            <ta e="T85" id="Seg_1849" s="T80">God said: “Will two times be enough?”</ta>
            <ta e="T89" id="Seg_1850" s="T85">“No, I don't want two times.”</ta>
            <ta e="T95" id="Seg_1851" s="T89">God got angry and said: “Will three times be enough?”</ta>
            <ta e="T96" id="Seg_1852" s="T95">“No!</ta>
            <ta e="T99" id="Seg_1853" s="T96">I don't want three times.”</ta>
            <ta e="T105" id="Seg_1854" s="T99">God got angry: “Let you have sex all the time!</ta>
            <ta e="T109" id="Seg_1855" s="T105">Let your husband torment you [all the time].</ta>
            <ta e="T112" id="Seg_1856" s="T109">(Let yout husband torment you [all the time].)</ta>
            <ta e="T116" id="Seg_1857" s="T112">(Let yout husband torment you [all the time].)</ta>
            <ta e="T121" id="Seg_1858" s="T116">Now all women are like that.</ta>
            <ta e="T130" id="Seg_1859" s="T121">One can have sex with them every night, and it won't be enough for them.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T13" id="Seg_1860" s="T1">Als es Licht wurde, schickte Gott alle auf die Erde: Menschen, Kühe, Pferde, Schafe, Schweine.</ta>
            <ta e="T15" id="Seg_1861" s="T13">Er begann zu fragen.</ta>
            <ta e="T22" id="Seg_1862" s="T15">Er rief eine Kuh: "Wird dir einmal pro Jahr reichen?"</ta>
            <ta e="T27" id="Seg_1863" s="T22">Und die Kuh sagte: "Es wird reichen."</ta>
            <ta e="T30" id="Seg_1864" s="T27">Jetzt rief er ein Pferd.</ta>
            <ta e="T36" id="Seg_1865" s="T30">Er begann es zu fragen: "Wird dir ein Mal reichen?"</ta>
            <ta e="T40" id="Seg_1866" s="T36">Es sagte: "Es wird reichen."</ta>
            <ta e="T42" id="Seg_1867" s="T40">Er rief ein Schaf.</ta>
            <ta e="T48" id="Seg_1868" s="T42">Er fragte das Schaf: "Wird dir ein Mal reichen?"</ta>
            <ta e="T50" id="Seg_1869" s="T48">"Es wird reichen."</ta>
            <ta e="T52" id="Seg_1870" s="T50">Er rief ein Schwein.</ta>
            <ta e="T57" id="Seg_1871" s="T52">Er fragte: "Wird dir ein Mal reichen?"</ta>
            <ta e="T58" id="Seg_1872" s="T57">"Nein!</ta>
            <ta e="T61" id="Seg_1873" s="T58">Sag mir: Zwei Male."</ta>
            <ta e="T66" id="Seg_1874" s="T61">Gott sagte: "Lass es zwei Male sein."</ta>
            <ta e="T68" id="Seg_1875" s="T66">Er rief eine Frau.</ta>
            <ta e="T76" id="Seg_1876" s="T68">Er begann die Frau zu fragen: "Wird dir ein Mal reichen?"</ta>
            <ta e="T80" id="Seg_1877" s="T76">"Nein, es wird nicht reichen."</ta>
            <ta e="T85" id="Seg_1878" s="T80">Gott sagte: "Werden zwei Male reichen?"</ta>
            <ta e="T89" id="Seg_1879" s="T85">"Nein, ich will nicht zweimal."</ta>
            <ta e="T95" id="Seg_1880" s="T89">Gott wurde wütend und sagte: "Werden drei Male reichen?"</ta>
            <ta e="T96" id="Seg_1881" s="T95">"Nein!</ta>
            <ta e="T99" id="Seg_1882" s="T96">Ich will keine drei Male."</ta>
            <ta e="T105" id="Seg_1883" s="T99">Gott wurde wütend: "Du sollst jetzt und immer Sex haben!</ta>
            <ta e="T109" id="Seg_1884" s="T105">Dein Ehemann soll dich [die ganze Zeit] quälen.</ta>
            <ta e="T112" id="Seg_1885" s="T109">(Dein Ehemann solll dich [die ganze Zeit] quälen.)</ta>
            <ta e="T116" id="Seg_1886" s="T112">(Dein Ehemann soll dich [die ganze Zeit] quälen.)</ta>
            <ta e="T121" id="Seg_1887" s="T116">Jetzt sind alle Frauen so.</ta>
            <ta e="T130" id="Seg_1888" s="T121">Man kann jede Nacht mit ihnen schlafen und es wird ihnen nicht genug sein.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T13" id="Seg_1889" s="T1">Когда свет настал, Бог всех на землю послал: людей, коров, лошадей, овец, свиней.</ta>
            <ta e="T15" id="Seg_1890" s="T13">Спрашивать начал.</ta>
            <ta e="T22" id="Seg_1891" s="T15">Позвал корову: “Тебе одного раза достаточно будет в год?”</ta>
            <ta e="T27" id="Seg_1892" s="T22">А корова говорит: “Достаточно будет”.</ta>
            <ta e="T30" id="Seg_1893" s="T27">Теперь лошадь позвал.</ta>
            <ta e="T36" id="Seg_1894" s="T30">Спрашивать начал: “Тебе одного достаточно будет?”</ta>
            <ta e="T40" id="Seg_1895" s="T36">Она сказала: “Достаточно будет”.</ta>
            <ta e="T42" id="Seg_1896" s="T40">Позвал овечку.</ta>
            <ta e="T48" id="Seg_1897" s="T42">Овечку спрашивает: “Тебе одного раза хватит?”</ta>
            <ta e="T50" id="Seg_1898" s="T48">“Хватит”.</ta>
            <ta e="T52" id="Seg_1899" s="T50">Позвал свинью.</ta>
            <ta e="T57" id="Seg_1900" s="T52">Спрашивает: “Тебе одного раза хватит?”</ta>
            <ta e="T58" id="Seg_1901" s="T57">“Нет!</ta>
            <ta e="T61" id="Seg_1902" s="T58">Скажи (вели) мне два раза”.</ta>
            <ta e="T66" id="Seg_1903" s="T61">Бог сказал: “Ну пускай два”.</ta>
            <ta e="T68" id="Seg_1904" s="T66">Позвал женщину.</ta>
            <ta e="T76" id="Seg_1905" s="T68">Женщину спрашивать начал: “Тебе один раз в год хватит?”</ta>
            <ta e="T80" id="Seg_1906" s="T76">“Нет, не хватит”.</ta>
            <ta e="T85" id="Seg_1907" s="T80">Бог сказал: “Два раза хватит?”</ta>
            <ta e="T89" id="Seg_1908" s="T85">“Нет, два раза не хочу”.</ta>
            <ta e="T95" id="Seg_1909" s="T89">Бог рассердился, говорит: “Три раза хватит?”</ta>
            <ta e="T96" id="Seg_1910" s="T95">“Нет!</ta>
            <ta e="T99" id="Seg_1911" s="T96">Три раза не хочу”.</ta>
            <ta e="T105" id="Seg_1912" s="T99">Бог рассердился: “Ну и совокупляйся всю жизнь!</ta>
            <ta e="T109" id="Seg_1913" s="T105">Пускай твой муж тебя [век] мучает.</ta>
            <ta e="T112" id="Seg_1914" s="T109">(Пускай твой муж [век] мучает).</ta>
            <ta e="T116" id="Seg_1915" s="T112">Мужик пуская тебя [век] мучает”.</ta>
            <ta e="T121" id="Seg_1916" s="T116">Теперь женщины такие и есть.</ta>
            <ta e="T130" id="Seg_1917" s="T121">С ними совокупляйся хоть каждую ночь, ей все мало будет.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T13" id="Seg_1918" s="T1">когда свет стал (наступил) бог всех на землю спустил людей коров лошадей овечек поросят</ta>
            <ta e="T15" id="Seg_1919" s="T13">спрашивать начал</ta>
            <ta e="T22" id="Seg_1920" s="T15">созвал (зазвал) корову тебе раз хватит в год</ta>
            <ta e="T27" id="Seg_1921" s="T22">а корова говорит хватит</ta>
            <ta e="T30" id="Seg_1922" s="T27">теперь лошадь зазвал</ta>
            <ta e="T36" id="Seg_1923" s="T30">спрашивать начал тебе одного достаточно</ta>
            <ta e="T40" id="Seg_1924" s="T36">она сказала достаточно</ta>
            <ta e="T42" id="Seg_1925" s="T40">зазвал овечку</ta>
            <ta e="T48" id="Seg_1926" s="T42">овечку спрашивает тебе одного раза хватит</ta>
            <ta e="T50" id="Seg_1927" s="T48">хватит</ta>
            <ta e="T52" id="Seg_1928" s="T50">зазвал свинью</ta>
            <ta e="T57" id="Seg_1929" s="T52">спрашивает тебе одного раза хватит</ta>
            <ta e="T58" id="Seg_1930" s="T57">нет</ta>
            <ta e="T61" id="Seg_1931" s="T58">скажи (вели) мне два раза</ta>
            <ta e="T66" id="Seg_1932" s="T61">бог сказал ну пусть два</ta>
            <ta e="T68" id="Seg_1933" s="T66">зазвал женщину</ta>
            <ta e="T76" id="Seg_1934" s="T68">женщину спрашивать начал тебе один раз в год хватит</ta>
            <ta e="T80" id="Seg_1935" s="T76">нет не хватит</ta>
            <ta e="T85" id="Seg_1936" s="T80">бог сказал два раза хватит</ta>
            <ta e="T89" id="Seg_1937" s="T85">нет два раза не хочу</ta>
            <ta e="T95" id="Seg_1938" s="T89">бог рассердился говорит три раза хватит</ta>
            <ta e="T96" id="Seg_1939" s="T95">нет</ta>
            <ta e="T99" id="Seg_1940" s="T96">три раза не хочу</ta>
            <ta e="T105" id="Seg_1941" s="T99">бог рассердился ну и ебись и ебись всю жизнь</ta>
            <ta e="T109" id="Seg_1942" s="T105">пускай мужик тебя век мучает</ta>
            <ta e="T116" id="Seg_1943" s="T112">тебя</ta>
            <ta e="T121" id="Seg_1944" s="T116">теперь женщины такие и есть</ta>
            <ta e="T130" id="Seg_1945" s="T121">их еби хоть каждую ночь ей все мало</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T61" id="Seg_1946" s="T58">[KuAI:] Variant: 'Sədɨ'.</ta>
            <ta e="T116" id="Seg_1947" s="T112">[KuAI:] Variant 'stɨ'</ta>
            <ta e="T130" id="Seg_1948" s="T121">[BrM:] Imperative construction loaned from Russian.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
