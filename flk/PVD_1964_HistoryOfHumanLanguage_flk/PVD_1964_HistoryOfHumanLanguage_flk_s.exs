<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_HistoryOfHumanLanguage_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_HistoryOfHumanLanguage_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">113</ud-information>
            <ud-information attribute-name="# HIAT:w">84</ud-information>
            <ud-information attribute-name="# e">84</ud-information>
            <ud-information attribute-name="# HIAT:u">19</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T85" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Ranʼše</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">oqqɨr</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">seː</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">jes</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">Wes</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">qula</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">oqqɨr</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">sezʼe</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">kulɨpbɨzattə</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_35" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">Tebla</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">wes</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">tunuzattə</ts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_47" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">Otdɨtɨ</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">seːm</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">wes</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">tunuzattə</ts>
                  <nts id="Seg_59" n="HIAT:ip">.</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_62" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">Tebla</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">nomom</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">qostuɣu</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_73" n="HIAT:w" s="T20">qɨgɨzattə</ts>
                  <nts id="Seg_74" n="HIAT:ip">.</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_77" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">Tʼärattə</ts>
                  <nts id="Seg_80" n="HIAT:ip">:</nts>
                  <nts id="Seg_81" n="HIAT:ip">“</nts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_83" n="HIAT:ip">“</nts>
                  <ts e="T23" id="Seg_85" n="HIAT:w" s="T22">Nadə</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">inne</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">sɨɣɨlgu</ts>
                  <nts id="Seg_92" n="HIAT:ip">.</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_95" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_97" n="HIAT:w" s="T25">Nom</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_100" n="HIAT:w" s="T26">jen</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_103" n="HIAT:w" s="T27">alʼi</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_106" n="HIAT:w" s="T28">tʼäkku</ts>
                  <nts id="Seg_107" n="HIAT:ip">.</nts>
                  <nts id="Seg_108" n="HIAT:ip">”</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_111" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_113" n="HIAT:w" s="T29">Tebla</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_116" n="HIAT:w" s="T30">qɨrsam</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_119" n="HIAT:w" s="T31">melʼe</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_122" n="HIAT:w" s="T32">jübərattə</ts>
                  <nts id="Seg_123" n="HIAT:ip">.</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_126" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_128" n="HIAT:w" s="T33">Kirpišnaja</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_131" n="HIAT:w" s="T34">kɨrsam</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_134" n="HIAT:w" s="T35">pɨːrgɨn</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_137" n="HIAT:w" s="T36">mewattə</ts>
                  <nts id="Seg_138" n="HIAT:ip">.</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_141" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_143" n="HIAT:w" s="T37">A</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_146" n="HIAT:w" s="T38">nom</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_149" n="HIAT:w" s="T39">tʼärɨn</ts>
                  <nts id="Seg_150" n="HIAT:ip">:</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_152" n="HIAT:ip">“</nts>
                  <ts e="T41" id="Seg_154" n="HIAT:w" s="T40">Skoro</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_157" n="HIAT:w" s="T41">meqga</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_160" n="HIAT:w" s="T42">sɨːɣɨǯeǯattə</ts>
                  <nts id="Seg_161" n="HIAT:ip">.</nts>
                  <nts id="Seg_162" n="HIAT:ip">”</nts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_165" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_167" n="HIAT:w" s="T43">Tep</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_170" n="HIAT:w" s="T44">teblan</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_173" n="HIAT:w" s="T45">sem</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_176" n="HIAT:w" s="T46">puːtaintɨt</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_178" n="HIAT:ip">(</nts>
                  <ts e="T48" id="Seg_180" n="HIAT:w" s="T47">puːtainɨt</ts>
                  <nts id="Seg_181" n="HIAT:ip">)</nts>
                  <nts id="Seg_182" n="HIAT:ip">.</nts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_185" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_187" n="HIAT:w" s="T48">Qut</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_190" n="HIAT:w" s="T49">kutʼet</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_193" n="HIAT:w" s="T50">papal</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_196" n="HIAT:w" s="T51">kuluppulʼe</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_199" n="HIAT:w" s="T52">jubərattə</ts>
                  <nts id="Seg_200" n="HIAT:ip">.</nts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_203" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_205" n="HIAT:w" s="T53">Nʼämdəttə</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_208" n="HIAT:w" s="T54">jas</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_211" n="HIAT:w" s="T55">kostolǯuqwattə</ts>
                  <nts id="Seg_212" n="HIAT:ip">.</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_215" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_217" n="HIAT:w" s="T56">I</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_220" n="HIAT:w" s="T57">prosinattə</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_223" n="HIAT:w" s="T58">qɨrsaj</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_226" n="HIAT:w" s="T59">meʒərlʼe</ts>
                  <nts id="Seg_227" n="HIAT:ip">.</nts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_230" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_232" n="HIAT:w" s="T60">Nano</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_235" n="HIAT:w" s="T61">kocʼi</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_238" n="HIAT:w" s="T62">taper</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_241" n="HIAT:w" s="T63">seː</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_244" n="HIAT:w" s="T64">jen</ts>
                  <nts id="Seg_245" n="HIAT:ip">.</nts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_248" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_250" n="HIAT:w" s="T65">Tau</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_253" n="HIAT:w" s="T66">tʼäptän</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_256" n="HIAT:w" s="T67">dʼäja</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_259" n="HIAT:w" s="T68">Аndrʼej</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_262" n="HIAT:w" s="T69">mekka</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_265" n="HIAT:w" s="T70">kessɨt</ts>
                  <nts id="Seg_266" n="HIAT:ip">.</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_269" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_271" n="HIAT:w" s="T71">Tep</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_274" n="HIAT:w" s="T72">kocʼi</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_277" n="HIAT:w" s="T73">tʼäptäm</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_280" n="HIAT:w" s="T74">tonustə</ts>
                  <nts id="Seg_281" n="HIAT:ip">.</nts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_284" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_286" n="HIAT:w" s="T75">Man</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_289" n="HIAT:w" s="T76">oqqɨr</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_292" n="HIAT:w" s="T77">tʼäptämnej</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_295" n="HIAT:w" s="T78">ass</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_298" n="HIAT:w" s="T79">tonou</ts>
                  <nts id="Seg_299" n="HIAT:ip">.</nts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_302" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_304" n="HIAT:w" s="T80">Oqqɨrɨm</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_307" n="HIAT:w" s="T81">tunuzau</ts>
                  <nts id="Seg_308" n="HIAT:ip">,</nts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_311" n="HIAT:w" s="T82">Аndrʼej</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_314" n="HIAT:w" s="T83">Petrowičenä</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_317" n="HIAT:w" s="T84">kessau</ts>
                  <nts id="Seg_318" n="HIAT:ip">.</nts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T85" id="Seg_320" n="sc" s="T1">
               <ts e="T2" id="Seg_322" n="e" s="T1">Ranʼše </ts>
               <ts e="T3" id="Seg_324" n="e" s="T2">oqqɨr </ts>
               <ts e="T4" id="Seg_326" n="e" s="T3">seː </ts>
               <ts e="T5" id="Seg_328" n="e" s="T4">jes. </ts>
               <ts e="T6" id="Seg_330" n="e" s="T5">Wes </ts>
               <ts e="T7" id="Seg_332" n="e" s="T6">qula </ts>
               <ts e="T8" id="Seg_334" n="e" s="T7">oqqɨr </ts>
               <ts e="T9" id="Seg_336" n="e" s="T8">sezʼe </ts>
               <ts e="T10" id="Seg_338" n="e" s="T9">kulɨpbɨzattə. </ts>
               <ts e="T11" id="Seg_340" n="e" s="T10">Tebla </ts>
               <ts e="T12" id="Seg_342" n="e" s="T11">wes </ts>
               <ts e="T13" id="Seg_344" n="e" s="T12">tunuzattə. </ts>
               <ts e="T14" id="Seg_346" n="e" s="T13">Otdɨtɨ </ts>
               <ts e="T15" id="Seg_348" n="e" s="T14">seːm </ts>
               <ts e="T16" id="Seg_350" n="e" s="T15">wes </ts>
               <ts e="T17" id="Seg_352" n="e" s="T16">tunuzattə. </ts>
               <ts e="T18" id="Seg_354" n="e" s="T17">Tebla </ts>
               <ts e="T19" id="Seg_356" n="e" s="T18">nomom </ts>
               <ts e="T20" id="Seg_358" n="e" s="T19">qostuɣu </ts>
               <ts e="T21" id="Seg_360" n="e" s="T20">qɨgɨzattə. </ts>
               <ts e="T22" id="Seg_362" n="e" s="T21">Tʼärattə:“ </ts>
               <ts e="T23" id="Seg_364" n="e" s="T22">“Nadə </ts>
               <ts e="T24" id="Seg_366" n="e" s="T23">inne </ts>
               <ts e="T25" id="Seg_368" n="e" s="T24">sɨɣɨlgu. </ts>
               <ts e="T26" id="Seg_370" n="e" s="T25">Nom </ts>
               <ts e="T27" id="Seg_372" n="e" s="T26">jen </ts>
               <ts e="T28" id="Seg_374" n="e" s="T27">alʼi </ts>
               <ts e="T29" id="Seg_376" n="e" s="T28">tʼäkku.” </ts>
               <ts e="T30" id="Seg_378" n="e" s="T29">Tebla </ts>
               <ts e="T31" id="Seg_380" n="e" s="T30">qɨrsam </ts>
               <ts e="T32" id="Seg_382" n="e" s="T31">melʼe </ts>
               <ts e="T33" id="Seg_384" n="e" s="T32">jübərattə. </ts>
               <ts e="T34" id="Seg_386" n="e" s="T33">Kirpišnaja </ts>
               <ts e="T35" id="Seg_388" n="e" s="T34">kɨrsam </ts>
               <ts e="T36" id="Seg_390" n="e" s="T35">pɨːrgɨn </ts>
               <ts e="T37" id="Seg_392" n="e" s="T36">mewattə. </ts>
               <ts e="T38" id="Seg_394" n="e" s="T37">A </ts>
               <ts e="T39" id="Seg_396" n="e" s="T38">nom </ts>
               <ts e="T40" id="Seg_398" n="e" s="T39">tʼärɨn: </ts>
               <ts e="T41" id="Seg_400" n="e" s="T40">“Skoro </ts>
               <ts e="T42" id="Seg_402" n="e" s="T41">meqga </ts>
               <ts e="T43" id="Seg_404" n="e" s="T42">sɨːɣɨǯeǯattə.” </ts>
               <ts e="T44" id="Seg_406" n="e" s="T43">Tep </ts>
               <ts e="T45" id="Seg_408" n="e" s="T44">teblan </ts>
               <ts e="T46" id="Seg_410" n="e" s="T45">sem </ts>
               <ts e="T47" id="Seg_412" n="e" s="T46">puːtaintɨt </ts>
               <ts e="T48" id="Seg_414" n="e" s="T47">(puːtainɨt). </ts>
               <ts e="T49" id="Seg_416" n="e" s="T48">Qut </ts>
               <ts e="T50" id="Seg_418" n="e" s="T49">kutʼet </ts>
               <ts e="T51" id="Seg_420" n="e" s="T50">papal </ts>
               <ts e="T52" id="Seg_422" n="e" s="T51">kuluppulʼe </ts>
               <ts e="T53" id="Seg_424" n="e" s="T52">jubərattə. </ts>
               <ts e="T54" id="Seg_426" n="e" s="T53">Nʼämdəttə </ts>
               <ts e="T55" id="Seg_428" n="e" s="T54">jas </ts>
               <ts e="T56" id="Seg_430" n="e" s="T55">kostolǯuqwattə. </ts>
               <ts e="T57" id="Seg_432" n="e" s="T56">I </ts>
               <ts e="T58" id="Seg_434" n="e" s="T57">prosinattə </ts>
               <ts e="T59" id="Seg_436" n="e" s="T58">qɨrsaj </ts>
               <ts e="T60" id="Seg_438" n="e" s="T59">meʒərlʼe. </ts>
               <ts e="T61" id="Seg_440" n="e" s="T60">Nano </ts>
               <ts e="T62" id="Seg_442" n="e" s="T61">kocʼi </ts>
               <ts e="T63" id="Seg_444" n="e" s="T62">taper </ts>
               <ts e="T64" id="Seg_446" n="e" s="T63">seː </ts>
               <ts e="T65" id="Seg_448" n="e" s="T64">jen. </ts>
               <ts e="T66" id="Seg_450" n="e" s="T65">Tau </ts>
               <ts e="T67" id="Seg_452" n="e" s="T66">tʼäptän </ts>
               <ts e="T68" id="Seg_454" n="e" s="T67">dʼäja </ts>
               <ts e="T69" id="Seg_456" n="e" s="T68">Аndrʼej </ts>
               <ts e="T70" id="Seg_458" n="e" s="T69">mekka </ts>
               <ts e="T71" id="Seg_460" n="e" s="T70">kessɨt. </ts>
               <ts e="T72" id="Seg_462" n="e" s="T71">Tep </ts>
               <ts e="T73" id="Seg_464" n="e" s="T72">kocʼi </ts>
               <ts e="T74" id="Seg_466" n="e" s="T73">tʼäptäm </ts>
               <ts e="T75" id="Seg_468" n="e" s="T74">tonustə. </ts>
               <ts e="T76" id="Seg_470" n="e" s="T75">Man </ts>
               <ts e="T77" id="Seg_472" n="e" s="T76">oqqɨr </ts>
               <ts e="T78" id="Seg_474" n="e" s="T77">tʼäptämnej </ts>
               <ts e="T79" id="Seg_476" n="e" s="T78">ass </ts>
               <ts e="T80" id="Seg_478" n="e" s="T79">tonou. </ts>
               <ts e="T81" id="Seg_480" n="e" s="T80">Oqqɨrɨm </ts>
               <ts e="T82" id="Seg_482" n="e" s="T81">tunuzau, </ts>
               <ts e="T83" id="Seg_484" n="e" s="T82">Аndrʼej </ts>
               <ts e="T84" id="Seg_486" n="e" s="T83">Petrowičenä </ts>
               <ts e="T85" id="Seg_488" n="e" s="T84">kessau. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_489" s="T1">PVD_1964_HistoryOfHumanLanguage_flk.001 (001.001)</ta>
            <ta e="T10" id="Seg_490" s="T5">PVD_1964_HistoryOfHumanLanguage_flk.002 (001.002)</ta>
            <ta e="T13" id="Seg_491" s="T10">PVD_1964_HistoryOfHumanLanguage_flk.003 (001.003)</ta>
            <ta e="T17" id="Seg_492" s="T13">PVD_1964_HistoryOfHumanLanguage_flk.004 (001.004)</ta>
            <ta e="T21" id="Seg_493" s="T17">PVD_1964_HistoryOfHumanLanguage_flk.005 (001.005)</ta>
            <ta e="T25" id="Seg_494" s="T21">PVD_1964_HistoryOfHumanLanguage_flk.006 (001.006)</ta>
            <ta e="T29" id="Seg_495" s="T25">PVD_1964_HistoryOfHumanLanguage_flk.007 (001.007)</ta>
            <ta e="T33" id="Seg_496" s="T29">PVD_1964_HistoryOfHumanLanguage_flk.008 (001.008)</ta>
            <ta e="T37" id="Seg_497" s="T33">PVD_1964_HistoryOfHumanLanguage_flk.009 (001.009)</ta>
            <ta e="T43" id="Seg_498" s="T37">PVD_1964_HistoryOfHumanLanguage_flk.010 (001.010)</ta>
            <ta e="T48" id="Seg_499" s="T43">PVD_1964_HistoryOfHumanLanguage_flk.011 (001.011)</ta>
            <ta e="T53" id="Seg_500" s="T48">PVD_1964_HistoryOfHumanLanguage_flk.012 (001.012)</ta>
            <ta e="T56" id="Seg_501" s="T53">PVD_1964_HistoryOfHumanLanguage_flk.013 (001.013)</ta>
            <ta e="T60" id="Seg_502" s="T56">PVD_1964_HistoryOfHumanLanguage_flk.014 (001.014)</ta>
            <ta e="T65" id="Seg_503" s="T60">PVD_1964_HistoryOfHumanLanguage_flk.015 (001.015)</ta>
            <ta e="T71" id="Seg_504" s="T65">PVD_1964_HistoryOfHumanLanguage_flk.016 (001.016)</ta>
            <ta e="T75" id="Seg_505" s="T71">PVD_1964_HistoryOfHumanLanguage_flk.017 (001.017)</ta>
            <ta e="T80" id="Seg_506" s="T75">PVD_1964_HistoryOfHumanLanguage_flk.018 (001.018)</ta>
            <ta e="T85" id="Seg_507" s="T80">PVD_1964_HistoryOfHumanLanguage_flk.019 (001.019)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_508" s="T1">ранʼше ′оkkыр се̄ jес.</ta>
            <ta e="T10" id="Seg_509" s="T5">ве̨с kу′lа оkkыр ′сезʼе ′кулыпб̂ы′заттъ.</ta>
            <ta e="T13" id="Seg_510" s="T10">теб′ла вес тунузаттъ.</ta>
            <ta e="T17" id="Seg_511" s="T13">′отдыты се̄м вес ′тунузаттъ.</ta>
            <ta e="T21" id="Seg_512" s="T17">тебла ′номом ′kостуɣу ′kыгызаттъ.</ta>
            <ta e="T25" id="Seg_513" s="T21">тʼӓраттъ: надъ ин′не̨сыɣылг(ɣ)у.</ta>
            <ta e="T29" id="Seg_514" s="T25">ном jен алʼи тʼӓкку.</ta>
            <ta e="T33" id="Seg_515" s="T29">теб′ла kыр′сам ′мелʼе‵jӱбъраттъ.</ta>
            <ta e="T37" id="Seg_516" s="T33">кир′пишнаjа кыр′сам ′пы̄ргын ′меw(в)аттъ.</ta>
            <ta e="T43" id="Seg_517" s="T37">а ном тʼӓрын: скоро ′меkг̂а ′сы̄ɣыдже′джаттъ.</ta>
            <ta e="T48" id="Seg_518" s="T43">те̨п теблан ′сем ′пӯтаинтыт (′пӯтаиныт).</ta>
            <ta e="T53" id="Seg_519" s="T48">kут ку′тʼе̨т па′пал ку′луп(б̂)пулʼе ′jубъраттъ.</ta>
            <ta e="T56" id="Seg_520" s="T53">нʼӓмдъттъ jас кос′тоlджу′kwаттъ.</ta>
            <ta e="T60" id="Seg_521" s="T56">и ′просинаттъ kыр′сай ′межърлʼе.</ta>
            <ta e="T65" id="Seg_522" s="T60">на′но коцʼи та′пе̨р се̄ jен.</ta>
            <ta e="T71" id="Seg_523" s="T65">′тау тʼӓп′тӓн дʼӓjа Андрʼей ′мекка ′кессыт.</ta>
            <ta e="T75" id="Seg_524" s="T71">(те̨п коцʼи тʼӓп′тӓм ′тонустъ.</ta>
            <ta e="T80" id="Seg_525" s="T75">ман ′оkkыр тʼӓп′тӓм не̨й асс то′ноу.</ta>
            <ta e="T85" id="Seg_526" s="T80">′оkkырым туну′зау̹(w), Андрʼей Петровиченӓ ке′ссау.)</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_527" s="T1">ranʼše oqqɨr seː jes.</ta>
            <ta e="T10" id="Seg_528" s="T5">wes qula oqqɨr sezʼe kulɨpb̂ɨzattə.</ta>
            <ta e="T13" id="Seg_529" s="T10">tebla wes tunuzattə.</ta>
            <ta e="T17" id="Seg_530" s="T13">otdɨtɨ seːm wes tunuzattə.</ta>
            <ta e="T21" id="Seg_531" s="T17">tebla nomom qostuɣu qɨgɨzattə.</ta>
            <ta e="T25" id="Seg_532" s="T21">tʼärattə: nadə innesɨɣɨlg(ɣ)u.</ta>
            <ta e="T29" id="Seg_533" s="T25">nom jen alʼi tʼäkku.</ta>
            <ta e="T33" id="Seg_534" s="T29">tebla qɨrsam melʼejübərattə.</ta>
            <ta e="T37" id="Seg_535" s="T33">kirpišnaja kɨrsam pɨːrgɨn mew(w)attə.</ta>
            <ta e="T43" id="Seg_536" s="T37">a nom tʼärɨn: skoro meqĝa sɨːɣɨǯeǯattə.</ta>
            <ta e="T48" id="Seg_537" s="T43">tep teblan sem puːtaintɨt (puːtainɨt).</ta>
            <ta e="T53" id="Seg_538" s="T48">qut kutʼet papal kulup(b̂)pulʼe jubərattə.</ta>
            <ta e="T56" id="Seg_539" s="T53">nʼämdəttə jas kostolǯuqwattə.</ta>
            <ta e="T60" id="Seg_540" s="T56">i prosinattə qɨrsaj meʒərlʼe.</ta>
            <ta e="T65" id="Seg_541" s="T60">nano kocʼi taper seː jen.</ta>
            <ta e="T71" id="Seg_542" s="T65">tau tʼäptän dʼäja Аndrʼej mekka kessɨt.</ta>
            <ta e="T75" id="Seg_543" s="T71">(tep kocʼi tʼäptäm tonustə.</ta>
            <ta e="T80" id="Seg_544" s="T75">man oqqɨr tʼäptäm nej ass tonou.</ta>
            <ta e="T85" id="Seg_545" s="T80">oqqɨrɨm tunuzau̹(w), Аndrʼej Пetrowičenä kessau.)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_546" s="T1">Ranʼše oqqɨr seː jes. </ta>
            <ta e="T10" id="Seg_547" s="T5">Wes qula oqqɨr sezʼe kulɨpbɨzattə. </ta>
            <ta e="T13" id="Seg_548" s="T10">Tebla wes tunuzattə. </ta>
            <ta e="T17" id="Seg_549" s="T13">Otdɨtɨ seːm wes tunuzattə. </ta>
            <ta e="T21" id="Seg_550" s="T17">Tebla nomom qostuɣu qɨgɨzattə. </ta>
            <ta e="T25" id="Seg_551" s="T21">Tʼärattə:“ “Nadə inne sɨɣɨlgu. </ta>
            <ta e="T29" id="Seg_552" s="T25">Nom jen alʼi tʼäkku.” </ta>
            <ta e="T33" id="Seg_553" s="T29">Tebla qɨrsam melʼe jübərattə. </ta>
            <ta e="T37" id="Seg_554" s="T33">Kirpišnaja kɨrsam pɨːrgɨn mewattə. </ta>
            <ta e="T43" id="Seg_555" s="T37">A nom tʼärɨn: “Skoro meqga sɨːɣɨǯeǯattə.” </ta>
            <ta e="T48" id="Seg_556" s="T43">Tep teblan sem puːtaintɨt (puːtainɨt). </ta>
            <ta e="T53" id="Seg_557" s="T48">Qut kutʼet papal kuluppulʼe jubərattə. </ta>
            <ta e="T56" id="Seg_558" s="T53">Nʼämdəttə jas kostolǯuqwattə. </ta>
            <ta e="T60" id="Seg_559" s="T56">I prosinattə qɨrsaj meʒərlʼe. </ta>
            <ta e="T65" id="Seg_560" s="T60">Nano kocʼi taper seː jen. </ta>
            <ta e="T71" id="Seg_561" s="T65">Tau tʼäptän dʼäja Аndrʼej mekka kessɨt. </ta>
            <ta e="T75" id="Seg_562" s="T71">Tep kocʼi tʼäptäm tonustə. </ta>
            <ta e="T80" id="Seg_563" s="T75">Man oqqɨr tʼäptämnej ass tonou. </ta>
            <ta e="T85" id="Seg_564" s="T80">Oqqɨrɨm tunuzau, Аndrʼej Petrowičenä kessau. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_565" s="T1">ranʼše</ta>
            <ta e="T3" id="Seg_566" s="T2">oqqɨr</ta>
            <ta e="T4" id="Seg_567" s="T3">seː</ta>
            <ta e="T5" id="Seg_568" s="T4">je-s</ta>
            <ta e="T6" id="Seg_569" s="T5">wes</ta>
            <ta e="T7" id="Seg_570" s="T6">qu-la</ta>
            <ta e="T8" id="Seg_571" s="T7">oqqɨr</ta>
            <ta e="T9" id="Seg_572" s="T8">se-zʼe</ta>
            <ta e="T10" id="Seg_573" s="T9">kulɨpbɨ-za-ttə</ta>
            <ta e="T11" id="Seg_574" s="T10">teb-la</ta>
            <ta e="T12" id="Seg_575" s="T11">wes</ta>
            <ta e="T13" id="Seg_576" s="T12">tunu-za-ttə</ta>
            <ta e="T14" id="Seg_577" s="T13">otdɨtɨ</ta>
            <ta e="T15" id="Seg_578" s="T14">seː-m</ta>
            <ta e="T16" id="Seg_579" s="T15">wes</ta>
            <ta e="T17" id="Seg_580" s="T16">tunu-za-ttə</ta>
            <ta e="T18" id="Seg_581" s="T17">teb-la</ta>
            <ta e="T19" id="Seg_582" s="T18">nom-o-m</ta>
            <ta e="T20" id="Seg_583" s="T19">qostu-ɣu</ta>
            <ta e="T21" id="Seg_584" s="T20">qɨgɨ-za-ttə</ta>
            <ta e="T22" id="Seg_585" s="T21">tʼära-ttə</ta>
            <ta e="T23" id="Seg_586" s="T22">nadə</ta>
            <ta e="T24" id="Seg_587" s="T23">inne</ta>
            <ta e="T25" id="Seg_588" s="T24">sɨɣɨ-l-gu</ta>
            <ta e="T26" id="Seg_589" s="T25">nom</ta>
            <ta e="T27" id="Seg_590" s="T26">je-n</ta>
            <ta e="T28" id="Seg_591" s="T27">alʼi</ta>
            <ta e="T29" id="Seg_592" s="T28">tʼäkku</ta>
            <ta e="T30" id="Seg_593" s="T29">teb-la</ta>
            <ta e="T31" id="Seg_594" s="T30">qɨr-sa-m</ta>
            <ta e="T32" id="Seg_595" s="T31">me-lʼe</ta>
            <ta e="T33" id="Seg_596" s="T32">jübə-ra-ttə</ta>
            <ta e="T35" id="Seg_597" s="T34">kɨr-sa-m</ta>
            <ta e="T36" id="Seg_598" s="T35">pɨːrgɨ-n</ta>
            <ta e="T37" id="Seg_599" s="T36">me-wa-ttə</ta>
            <ta e="T38" id="Seg_600" s="T37">a</ta>
            <ta e="T39" id="Seg_601" s="T38">nom</ta>
            <ta e="T40" id="Seg_602" s="T39">tʼärɨ-n</ta>
            <ta e="T41" id="Seg_603" s="T40">skoro</ta>
            <ta e="T42" id="Seg_604" s="T41">meqga</ta>
            <ta e="T43" id="Seg_605" s="T42">sɨːɣɨ-ǯ-eǯa-ttə</ta>
            <ta e="T44" id="Seg_606" s="T43">tep</ta>
            <ta e="T45" id="Seg_607" s="T44">teb-la-n</ta>
            <ta e="T46" id="Seg_608" s="T45">se-m</ta>
            <ta e="T47" id="Seg_609" s="T46">puːtai-ntɨ-t</ta>
            <ta e="T48" id="Seg_610" s="T47">puːtai-nɨ-t</ta>
            <ta e="T49" id="Seg_611" s="T48">qut</ta>
            <ta e="T50" id="Seg_612" s="T49">kutʼet</ta>
            <ta e="T51" id="Seg_613" s="T50">papal</ta>
            <ta e="T52" id="Seg_614" s="T51">kuluppu-lʼe</ta>
            <ta e="T53" id="Seg_615" s="T52">jubə-ra-ttə</ta>
            <ta e="T54" id="Seg_616" s="T53">nʼämdə-ttə</ta>
            <ta e="T55" id="Seg_617" s="T54">jas</ta>
            <ta e="T56" id="Seg_618" s="T55">kost-ol-ǯu-qwa-ttə</ta>
            <ta e="T57" id="Seg_619" s="T56">i</ta>
            <ta e="T58" id="Seg_620" s="T57">prosi-na-ttə</ta>
            <ta e="T59" id="Seg_621" s="T58">qɨr-sa-j</ta>
            <ta e="T60" id="Seg_622" s="T59">me-ʒər-lʼe</ta>
            <ta e="T61" id="Seg_623" s="T60">nano</ta>
            <ta e="T62" id="Seg_624" s="T61">kocʼi</ta>
            <ta e="T63" id="Seg_625" s="T62">taper</ta>
            <ta e="T64" id="Seg_626" s="T63">seː</ta>
            <ta e="T65" id="Seg_627" s="T64">je-n</ta>
            <ta e="T66" id="Seg_628" s="T65">tau</ta>
            <ta e="T67" id="Seg_629" s="T66">tʼäptä-n</ta>
            <ta e="T68" id="Seg_630" s="T67">dʼäja</ta>
            <ta e="T69" id="Seg_631" s="T68">Аndrʼej</ta>
            <ta e="T70" id="Seg_632" s="T69">mekka</ta>
            <ta e="T71" id="Seg_633" s="T70">kes-sɨ-t</ta>
            <ta e="T72" id="Seg_634" s="T71">tep</ta>
            <ta e="T73" id="Seg_635" s="T72">kocʼi</ta>
            <ta e="T74" id="Seg_636" s="T73">tʼäptä-m</ta>
            <ta e="T75" id="Seg_637" s="T74">tonu-s-tə</ta>
            <ta e="T76" id="Seg_638" s="T75">man</ta>
            <ta e="T77" id="Seg_639" s="T76">oqqɨr</ta>
            <ta e="T78" id="Seg_640" s="T77">tʼäptä-m-nej</ta>
            <ta e="T79" id="Seg_641" s="T78">ass</ta>
            <ta e="T80" id="Seg_642" s="T79">tono-u</ta>
            <ta e="T81" id="Seg_643" s="T80">oqqɨr-ɨ-m</ta>
            <ta e="T82" id="Seg_644" s="T81">tunu-za-u</ta>
            <ta e="T83" id="Seg_645" s="T82">Аndrʼej</ta>
            <ta e="T84" id="Seg_646" s="T83">Petrowič-e-nä</ta>
            <ta e="T85" id="Seg_647" s="T84">kes-sa-u</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_648" s="T1">ranʼše</ta>
            <ta e="T3" id="Seg_649" s="T2">okkɨr</ta>
            <ta e="T4" id="Seg_650" s="T3">se</ta>
            <ta e="T5" id="Seg_651" s="T4">eː-sɨ</ta>
            <ta e="T6" id="Seg_652" s="T5">wesʼ</ta>
            <ta e="T7" id="Seg_653" s="T6">qum-la</ta>
            <ta e="T8" id="Seg_654" s="T7">okkɨr</ta>
            <ta e="T9" id="Seg_655" s="T8">se-se</ta>
            <ta e="T10" id="Seg_656" s="T9">kulubu-sɨ-tɨn</ta>
            <ta e="T11" id="Seg_657" s="T10">täp-la</ta>
            <ta e="T12" id="Seg_658" s="T11">wesʼ</ta>
            <ta e="T13" id="Seg_659" s="T12">tonu-sɨ-tɨn</ta>
            <ta e="T14" id="Seg_660" s="T13">ondɨt</ta>
            <ta e="T15" id="Seg_661" s="T14">se-m</ta>
            <ta e="T16" id="Seg_662" s="T15">wesʼ</ta>
            <ta e="T17" id="Seg_663" s="T16">tonu-sɨ-tɨn</ta>
            <ta e="T18" id="Seg_664" s="T17">täp-la</ta>
            <ta e="T19" id="Seg_665" s="T18">nom-ɨ-m</ta>
            <ta e="T20" id="Seg_666" s="T19">kostɨ-gu</ta>
            <ta e="T21" id="Seg_667" s="T20">kɨgɨ-sɨ-tɨn</ta>
            <ta e="T22" id="Seg_668" s="T21">tʼärɨ-tɨn</ta>
            <ta e="T23" id="Seg_669" s="T22">nadə</ta>
            <ta e="T24" id="Seg_670" s="T23">innä</ta>
            <ta e="T25" id="Seg_671" s="T24">sɨɣə-l-gu</ta>
            <ta e="T26" id="Seg_672" s="T25">nom</ta>
            <ta e="T27" id="Seg_673" s="T26">eː-n</ta>
            <ta e="T28" id="Seg_674" s="T27">alʼi</ta>
            <ta e="T29" id="Seg_675" s="T28">tʼäkku</ta>
            <ta e="T30" id="Seg_676" s="T29">täp-la</ta>
            <ta e="T31" id="Seg_677" s="T30">qɨr-se-m</ta>
            <ta e="T32" id="Seg_678" s="T31">meː-le</ta>
            <ta e="T33" id="Seg_679" s="T32">übɨ-rɨ-tɨn</ta>
            <ta e="T35" id="Seg_680" s="T34">qɨr-se-m</ta>
            <ta e="T36" id="Seg_681" s="T35">pirgə-ŋ</ta>
            <ta e="T37" id="Seg_682" s="T36">meː-nɨ-tɨn</ta>
            <ta e="T38" id="Seg_683" s="T37">a</ta>
            <ta e="T39" id="Seg_684" s="T38">nom</ta>
            <ta e="T40" id="Seg_685" s="T39">tʼärɨ-n</ta>
            <ta e="T41" id="Seg_686" s="T40">skoro</ta>
            <ta e="T42" id="Seg_687" s="T41">mekka</ta>
            <ta e="T43" id="Seg_688" s="T42">sɨɣə-ǯə-enǯɨ-tɨn</ta>
            <ta e="T44" id="Seg_689" s="T43">täp</ta>
            <ta e="T45" id="Seg_690" s="T44">täp-la-n</ta>
            <ta e="T46" id="Seg_691" s="T45">se-m</ta>
            <ta e="T47" id="Seg_692" s="T46">puːtai-ntɨ-t</ta>
            <ta e="T48" id="Seg_693" s="T47">puːtai-nɨ-t</ta>
            <ta e="T49" id="Seg_694" s="T48">kud</ta>
            <ta e="T50" id="Seg_695" s="T49">qutʼet</ta>
            <ta e="T51" id="Seg_696" s="T50">papal</ta>
            <ta e="T52" id="Seg_697" s="T51">kulubu-le</ta>
            <ta e="T53" id="Seg_698" s="T52">übɨ-rɨ-tɨn</ta>
            <ta e="T54" id="Seg_699" s="T53">nʼemdə-tɨn</ta>
            <ta e="T55" id="Seg_700" s="T54">asa</ta>
            <ta e="T56" id="Seg_701" s="T55">kostɨ-ol-ču-ku-tɨn</ta>
            <ta e="T57" id="Seg_702" s="T56">i</ta>
            <ta e="T58" id="Seg_703" s="T57">prosi-nɨ-tɨn</ta>
            <ta e="T59" id="Seg_704" s="T58">qɨr-se-lʼ</ta>
            <ta e="T60" id="Seg_705" s="T59">meː-nǯir-le</ta>
            <ta e="T61" id="Seg_706" s="T60">nanoː</ta>
            <ta e="T62" id="Seg_707" s="T61">koːci</ta>
            <ta e="T63" id="Seg_708" s="T62">teper</ta>
            <ta e="T64" id="Seg_709" s="T63">se</ta>
            <ta e="T65" id="Seg_710" s="T64">eː-n</ta>
            <ta e="T66" id="Seg_711" s="T65">taw</ta>
            <ta e="T67" id="Seg_712" s="T66">tʼäptä-n</ta>
            <ta e="T68" id="Seg_713" s="T67">dʼaja</ta>
            <ta e="T69" id="Seg_714" s="T68">Andrej</ta>
            <ta e="T70" id="Seg_715" s="T69">mekka</ta>
            <ta e="T71" id="Seg_716" s="T70">ket-sɨ-t</ta>
            <ta e="T72" id="Seg_717" s="T71">täp</ta>
            <ta e="T73" id="Seg_718" s="T72">koːci</ta>
            <ta e="T74" id="Seg_719" s="T73">tʼäptä-m</ta>
            <ta e="T75" id="Seg_720" s="T74">tonu-sɨ-t</ta>
            <ta e="T76" id="Seg_721" s="T75">man</ta>
            <ta e="T77" id="Seg_722" s="T76">okkɨr</ta>
            <ta e="T78" id="Seg_723" s="T77">tʼäptä-m-näj</ta>
            <ta e="T79" id="Seg_724" s="T78">asa</ta>
            <ta e="T80" id="Seg_725" s="T79">tonu-w</ta>
            <ta e="T81" id="Seg_726" s="T80">okkɨr-ɨ-m</ta>
            <ta e="T82" id="Seg_727" s="T81">tonu-sɨ-w</ta>
            <ta e="T83" id="Seg_728" s="T82">Andrej</ta>
            <ta e="T84" id="Seg_729" s="T83">Petrowičʼ-ɨ-nä</ta>
            <ta e="T85" id="Seg_730" s="T84">ket-sɨ-w</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_731" s="T1">earlier</ta>
            <ta e="T3" id="Seg_732" s="T2">one</ta>
            <ta e="T4" id="Seg_733" s="T3">language.[NOM]</ta>
            <ta e="T5" id="Seg_734" s="T4">be-PST.[3SG.S]</ta>
            <ta e="T6" id="Seg_735" s="T5">all</ta>
            <ta e="T7" id="Seg_736" s="T6">human.being-PL.[NOM]</ta>
            <ta e="T8" id="Seg_737" s="T7">one</ta>
            <ta e="T9" id="Seg_738" s="T8">language-INSTR</ta>
            <ta e="T10" id="Seg_739" s="T9">speak-PST-3PL</ta>
            <ta e="T11" id="Seg_740" s="T10">(s)he-PL.[NOM]</ta>
            <ta e="T12" id="Seg_741" s="T11">all</ta>
            <ta e="T13" id="Seg_742" s="T12">know-PST-3PL</ta>
            <ta e="T14" id="Seg_743" s="T13">own.3PL</ta>
            <ta e="T15" id="Seg_744" s="T14">language-ACC</ta>
            <ta e="T16" id="Seg_745" s="T15">all</ta>
            <ta e="T17" id="Seg_746" s="T16">know-PST-3PL</ta>
            <ta e="T18" id="Seg_747" s="T17">(s)he-PL.[NOM]</ta>
            <ta e="T19" id="Seg_748" s="T18">god-EP-ACC</ta>
            <ta e="T20" id="Seg_749" s="T19">get.to.know-INF</ta>
            <ta e="T21" id="Seg_750" s="T20">want-PST-3PL</ta>
            <ta e="T22" id="Seg_751" s="T21">say-3PL</ta>
            <ta e="T23" id="Seg_752" s="T22">one.should</ta>
            <ta e="T24" id="Seg_753" s="T23">up</ta>
            <ta e="T25" id="Seg_754" s="T24">climb-INCH-INF</ta>
            <ta e="T26" id="Seg_755" s="T25">god.[NOM]</ta>
            <ta e="T27" id="Seg_756" s="T26">be-3SG.S</ta>
            <ta e="T28" id="Seg_757" s="T27">or</ta>
            <ta e="T29" id="Seg_758" s="T28">NEG.EX.[3SG.S]</ta>
            <ta e="T30" id="Seg_759" s="T29">(s)he-PL.[NOM]</ta>
            <ta e="T31" id="Seg_760" s="T30">hole-COM-ACC</ta>
            <ta e="T32" id="Seg_761" s="T31">do-CVB</ta>
            <ta e="T33" id="Seg_762" s="T32">begin-DRV-3PL</ta>
            <ta e="T35" id="Seg_763" s="T34">hole-COM-ACC</ta>
            <ta e="T36" id="Seg_764" s="T35">tall-ADVZ</ta>
            <ta e="T37" id="Seg_765" s="T36">do-CO-3PL</ta>
            <ta e="T38" id="Seg_766" s="T37">and</ta>
            <ta e="T39" id="Seg_767" s="T38">god.[NOM]</ta>
            <ta e="T40" id="Seg_768" s="T39">say-3SG.S</ta>
            <ta e="T41" id="Seg_769" s="T40">soon</ta>
            <ta e="T42" id="Seg_770" s="T41">I.ALL</ta>
            <ta e="T43" id="Seg_771" s="T42">climb-DRV-FUT-3PL</ta>
            <ta e="T44" id="Seg_772" s="T43">(s)he.[NOM]</ta>
            <ta e="T45" id="Seg_773" s="T44">(s)he-PL-GEN</ta>
            <ta e="T46" id="Seg_774" s="T45">language-ACC</ta>
            <ta e="T47" id="Seg_775" s="T46">mix.up-INFER-3SG.O</ta>
            <ta e="T48" id="Seg_776" s="T47">mix.up-CO-3SG.O</ta>
            <ta e="T49" id="Seg_777" s="T48">who.[NOM]</ta>
            <ta e="T50" id="Seg_778" s="T49">where</ta>
            <ta e="T51" id="Seg_779" s="T50">how</ta>
            <ta e="T52" id="Seg_780" s="T51">speak-CVB</ta>
            <ta e="T53" id="Seg_781" s="T52">begin-DRV-3PL</ta>
            <ta e="T54" id="Seg_782" s="T53">each.other-3PL</ta>
            <ta e="T55" id="Seg_783" s="T54">NEG</ta>
            <ta e="T56" id="Seg_784" s="T55">get.to.know-MOM-TR-HAB-3PL</ta>
            <ta e="T57" id="Seg_785" s="T56">and</ta>
            <ta e="T58" id="Seg_786" s="T57">cease-CO-3PL</ta>
            <ta e="T59" id="Seg_787" s="T58">hole-COM-ADJZ</ta>
            <ta e="T60" id="Seg_788" s="T59">do-DRV-CVB</ta>
            <ta e="T61" id="Seg_789" s="T60">that.is.why</ta>
            <ta e="T62" id="Seg_790" s="T61">much</ta>
            <ta e="T63" id="Seg_791" s="T62">now</ta>
            <ta e="T64" id="Seg_792" s="T63">language.[NOM]</ta>
            <ta e="T65" id="Seg_793" s="T64">be-3SG.S</ta>
            <ta e="T66" id="Seg_794" s="T65">this</ta>
            <ta e="T67" id="Seg_795" s="T66">tale-GEN</ta>
            <ta e="T68" id="Seg_796" s="T67">uncle.[NOM]</ta>
            <ta e="T69" id="Seg_797" s="T68">Andrey.[NOM]</ta>
            <ta e="T70" id="Seg_798" s="T69">I.ALL</ta>
            <ta e="T71" id="Seg_799" s="T70">say-PST-3SG.O</ta>
            <ta e="T72" id="Seg_800" s="T71">(s)he.[NOM]</ta>
            <ta e="T73" id="Seg_801" s="T72">much</ta>
            <ta e="T74" id="Seg_802" s="T73">tale-ACC</ta>
            <ta e="T75" id="Seg_803" s="T74">know-PST-3SG.O</ta>
            <ta e="T76" id="Seg_804" s="T75">I.NOM</ta>
            <ta e="T77" id="Seg_805" s="T76">one</ta>
            <ta e="T78" id="Seg_806" s="T77">tale-ACC-EMPH</ta>
            <ta e="T79" id="Seg_807" s="T78">NEG</ta>
            <ta e="T80" id="Seg_808" s="T79">know-1SG.O</ta>
            <ta e="T81" id="Seg_809" s="T80">one-EP-ACC</ta>
            <ta e="T82" id="Seg_810" s="T81">know-PST-1SG.O</ta>
            <ta e="T83" id="Seg_811" s="T82">Andrey.[NOM]</ta>
            <ta e="T84" id="Seg_812" s="T83">Petrovich-EP-ALL</ta>
            <ta e="T85" id="Seg_813" s="T84">say-PST-1SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_814" s="T1">раньше</ta>
            <ta e="T3" id="Seg_815" s="T2">один</ta>
            <ta e="T4" id="Seg_816" s="T3">язык.[NOM]</ta>
            <ta e="T5" id="Seg_817" s="T4">быть-PST.[3SG.S]</ta>
            <ta e="T6" id="Seg_818" s="T5">весь</ta>
            <ta e="T7" id="Seg_819" s="T6">человек-PL.[NOM]</ta>
            <ta e="T8" id="Seg_820" s="T7">один</ta>
            <ta e="T9" id="Seg_821" s="T8">язык-INSTR</ta>
            <ta e="T10" id="Seg_822" s="T9">говорить-PST-3PL</ta>
            <ta e="T11" id="Seg_823" s="T10">он(а)-PL.[NOM]</ta>
            <ta e="T12" id="Seg_824" s="T11">весь</ta>
            <ta e="T13" id="Seg_825" s="T12">знать-PST-3PL</ta>
            <ta e="T14" id="Seg_826" s="T13">свой.3PL</ta>
            <ta e="T15" id="Seg_827" s="T14">язык-ACC</ta>
            <ta e="T16" id="Seg_828" s="T15">весь</ta>
            <ta e="T17" id="Seg_829" s="T16">знать-PST-3PL</ta>
            <ta e="T18" id="Seg_830" s="T17">он(а)-PL.[NOM]</ta>
            <ta e="T19" id="Seg_831" s="T18">бог-EP-ACC</ta>
            <ta e="T20" id="Seg_832" s="T19">узнать-INF</ta>
            <ta e="T21" id="Seg_833" s="T20">хотеть-PST-3PL</ta>
            <ta e="T22" id="Seg_834" s="T21">сказать-3PL</ta>
            <ta e="T23" id="Seg_835" s="T22">надо</ta>
            <ta e="T24" id="Seg_836" s="T23">наверх</ta>
            <ta e="T25" id="Seg_837" s="T24">залезть-INCH-INF</ta>
            <ta e="T26" id="Seg_838" s="T25">бог.[NOM]</ta>
            <ta e="T27" id="Seg_839" s="T26">быть-3SG.S</ta>
            <ta e="T28" id="Seg_840" s="T27">али</ta>
            <ta e="T29" id="Seg_841" s="T28">NEG.EX.[3SG.S]</ta>
            <ta e="T30" id="Seg_842" s="T29">он(а)-PL.[NOM]</ta>
            <ta e="T31" id="Seg_843" s="T30">дыра-COM-ACC</ta>
            <ta e="T32" id="Seg_844" s="T31">сделать-CVB</ta>
            <ta e="T33" id="Seg_845" s="T32">начать-DRV-3PL</ta>
            <ta e="T35" id="Seg_846" s="T34">дыра-COM-ACC</ta>
            <ta e="T36" id="Seg_847" s="T35">высокий-ADVZ</ta>
            <ta e="T37" id="Seg_848" s="T36">сделать-CO-3PL</ta>
            <ta e="T38" id="Seg_849" s="T37">а</ta>
            <ta e="T39" id="Seg_850" s="T38">бог.[NOM]</ta>
            <ta e="T40" id="Seg_851" s="T39">сказать-3SG.S</ta>
            <ta e="T41" id="Seg_852" s="T40">скоро</ta>
            <ta e="T42" id="Seg_853" s="T41">я.ALL</ta>
            <ta e="T43" id="Seg_854" s="T42">залезть-DRV-FUT-3PL</ta>
            <ta e="T44" id="Seg_855" s="T43">он(а).[NOM]</ta>
            <ta e="T45" id="Seg_856" s="T44">он(а)-PL-GEN</ta>
            <ta e="T46" id="Seg_857" s="T45">язык-ACC</ta>
            <ta e="T47" id="Seg_858" s="T46">запутать-INFER-3SG.O</ta>
            <ta e="T48" id="Seg_859" s="T47">запутать-CO-3SG.O</ta>
            <ta e="T49" id="Seg_860" s="T48">кто.[NOM]</ta>
            <ta e="T50" id="Seg_861" s="T49">куда</ta>
            <ta e="T51" id="Seg_862" s="T50">попало</ta>
            <ta e="T52" id="Seg_863" s="T51">говорить-CVB</ta>
            <ta e="T53" id="Seg_864" s="T52">начать-DRV-3PL</ta>
            <ta e="T54" id="Seg_865" s="T53">друг.друга-3PL</ta>
            <ta e="T55" id="Seg_866" s="T54">NEG</ta>
            <ta e="T56" id="Seg_867" s="T55">узнать-MOM-TR-HAB-3PL</ta>
            <ta e="T57" id="Seg_868" s="T56">и</ta>
            <ta e="T58" id="Seg_869" s="T57">бросить-CO-3PL</ta>
            <ta e="T59" id="Seg_870" s="T58">дыра-COM-ADJZ</ta>
            <ta e="T60" id="Seg_871" s="T59">сделать-DRV-CVB</ta>
            <ta e="T61" id="Seg_872" s="T60">поэтому</ta>
            <ta e="T62" id="Seg_873" s="T61">много</ta>
            <ta e="T63" id="Seg_874" s="T62">теперь</ta>
            <ta e="T64" id="Seg_875" s="T63">язык.[NOM]</ta>
            <ta e="T65" id="Seg_876" s="T64">быть-3SG.S</ta>
            <ta e="T66" id="Seg_877" s="T65">этот</ta>
            <ta e="T67" id="Seg_878" s="T66">сказка-GEN</ta>
            <ta e="T68" id="Seg_879" s="T67">дядя.[NOM]</ta>
            <ta e="T69" id="Seg_880" s="T68">Андрей.[NOM]</ta>
            <ta e="T70" id="Seg_881" s="T69">я.ALL</ta>
            <ta e="T71" id="Seg_882" s="T70">сказать-PST-3SG.O</ta>
            <ta e="T72" id="Seg_883" s="T71">он(а).[NOM]</ta>
            <ta e="T73" id="Seg_884" s="T72">много</ta>
            <ta e="T74" id="Seg_885" s="T73">сказка-ACC</ta>
            <ta e="T75" id="Seg_886" s="T74">знать-PST-3SG.O</ta>
            <ta e="T76" id="Seg_887" s="T75">я.NOM</ta>
            <ta e="T77" id="Seg_888" s="T76">один</ta>
            <ta e="T78" id="Seg_889" s="T77">сказка-ACC-EMPH</ta>
            <ta e="T79" id="Seg_890" s="T78">NEG</ta>
            <ta e="T80" id="Seg_891" s="T79">знать-1SG.O</ta>
            <ta e="T81" id="Seg_892" s="T80">один-EP-ACC</ta>
            <ta e="T82" id="Seg_893" s="T81">знать-PST-1SG.O</ta>
            <ta e="T83" id="Seg_894" s="T82">Андрей.[NOM]</ta>
            <ta e="T84" id="Seg_895" s="T83">Петрович-EP-ALL</ta>
            <ta e="T85" id="Seg_896" s="T84">сказать-PST-1SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_897" s="T1">adv</ta>
            <ta e="T3" id="Seg_898" s="T2">num</ta>
            <ta e="T4" id="Seg_899" s="T3">n.[n:case]</ta>
            <ta e="T5" id="Seg_900" s="T4">v-v:tense.[v:pn]</ta>
            <ta e="T6" id="Seg_901" s="T5">quant</ta>
            <ta e="T7" id="Seg_902" s="T6">n-n:num.[n:case]</ta>
            <ta e="T8" id="Seg_903" s="T7">num</ta>
            <ta e="T9" id="Seg_904" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_905" s="T9">v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_906" s="T10">pers-n:num.[n:case]</ta>
            <ta e="T12" id="Seg_907" s="T11">quant</ta>
            <ta e="T13" id="Seg_908" s="T12">v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_909" s="T13">emphpro</ta>
            <ta e="T15" id="Seg_910" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_911" s="T15">quant</ta>
            <ta e="T17" id="Seg_912" s="T16">v-v:tense-v:pn</ta>
            <ta e="T18" id="Seg_913" s="T17">pers-n:num.[n:case]</ta>
            <ta e="T19" id="Seg_914" s="T18">n-n:ins-n:case</ta>
            <ta e="T20" id="Seg_915" s="T19">v-v:inf</ta>
            <ta e="T21" id="Seg_916" s="T20">v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_917" s="T21">v-v:pn</ta>
            <ta e="T23" id="Seg_918" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_919" s="T23">adv</ta>
            <ta e="T25" id="Seg_920" s="T24">v-v&gt;v-v:inf</ta>
            <ta e="T26" id="Seg_921" s="T25">n.[n:case]</ta>
            <ta e="T27" id="Seg_922" s="T26">v-v:pn</ta>
            <ta e="T28" id="Seg_923" s="T27">conj</ta>
            <ta e="T29" id="Seg_924" s="T28">v.[v:pn]</ta>
            <ta e="T30" id="Seg_925" s="T29">pers-n:num.[n:case]</ta>
            <ta e="T31" id="Seg_926" s="T30">n-n:case-n:case</ta>
            <ta e="T32" id="Seg_927" s="T31">v-v&gt;adv</ta>
            <ta e="T33" id="Seg_928" s="T32">v-v&gt;v-v:pn</ta>
            <ta e="T35" id="Seg_929" s="T34">n-n:case-n:case</ta>
            <ta e="T36" id="Seg_930" s="T35">adj-adj&gt;adv</ta>
            <ta e="T37" id="Seg_931" s="T36">v-v:ins-v:pn</ta>
            <ta e="T38" id="Seg_932" s="T37">conj</ta>
            <ta e="T39" id="Seg_933" s="T38">n.[n:case]</ta>
            <ta e="T40" id="Seg_934" s="T39">v-v:pn</ta>
            <ta e="T41" id="Seg_935" s="T40">adv</ta>
            <ta e="T42" id="Seg_936" s="T41">pers</ta>
            <ta e="T43" id="Seg_937" s="T42">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T44" id="Seg_938" s="T43">pers.[n:case]</ta>
            <ta e="T45" id="Seg_939" s="T44">pers-n:num-n:case</ta>
            <ta e="T46" id="Seg_940" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_941" s="T46">v-v:mood-v:pn</ta>
            <ta e="T48" id="Seg_942" s="T47">v-v:ins-v:pn</ta>
            <ta e="T49" id="Seg_943" s="T48">interrog.[n:case]</ta>
            <ta e="T50" id="Seg_944" s="T49">interrog</ta>
            <ta e="T51" id="Seg_945" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_946" s="T51">v-v&gt;adv</ta>
            <ta e="T53" id="Seg_947" s="T52">v-v&gt;v-v:pn</ta>
            <ta e="T54" id="Seg_948" s="T53">pro-n:poss</ta>
            <ta e="T55" id="Seg_949" s="T54">ptcl</ta>
            <ta e="T56" id="Seg_950" s="T55">v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T57" id="Seg_951" s="T56">conj</ta>
            <ta e="T58" id="Seg_952" s="T57">v-v:ins-v:pn</ta>
            <ta e="T59" id="Seg_953" s="T58">n-n:case-n&gt;adj</ta>
            <ta e="T60" id="Seg_954" s="T59">v-v&gt;v-v&gt;adv</ta>
            <ta e="T61" id="Seg_955" s="T60">conj</ta>
            <ta e="T62" id="Seg_956" s="T61">quant</ta>
            <ta e="T63" id="Seg_957" s="T62">adv</ta>
            <ta e="T64" id="Seg_958" s="T63">n.[n:case]</ta>
            <ta e="T65" id="Seg_959" s="T64">v-v:pn</ta>
            <ta e="T66" id="Seg_960" s="T65">dem</ta>
            <ta e="T67" id="Seg_961" s="T66">n-n:case</ta>
            <ta e="T68" id="Seg_962" s="T67">n.[n:case]</ta>
            <ta e="T69" id="Seg_963" s="T68">nprop.[n:case]</ta>
            <ta e="T70" id="Seg_964" s="T69">pers</ta>
            <ta e="T71" id="Seg_965" s="T70">v-v:tense-v:pn</ta>
            <ta e="T72" id="Seg_966" s="T71">pers.[n:case]</ta>
            <ta e="T73" id="Seg_967" s="T72">quant</ta>
            <ta e="T74" id="Seg_968" s="T73">n-n:case</ta>
            <ta e="T75" id="Seg_969" s="T74">v-v:tense-v:pn</ta>
            <ta e="T76" id="Seg_970" s="T75">pers</ta>
            <ta e="T77" id="Seg_971" s="T76">num</ta>
            <ta e="T78" id="Seg_972" s="T77">n-n:case-clit</ta>
            <ta e="T79" id="Seg_973" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_974" s="T79">v-v:pn</ta>
            <ta e="T81" id="Seg_975" s="T80">num-n:ins-n:case</ta>
            <ta e="T82" id="Seg_976" s="T81">v-v:tense-v:pn</ta>
            <ta e="T83" id="Seg_977" s="T82">nprop.[n:case]</ta>
            <ta e="T84" id="Seg_978" s="T83">nprop-n:ins-n:case</ta>
            <ta e="T85" id="Seg_979" s="T84">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_980" s="T1">adv</ta>
            <ta e="T3" id="Seg_981" s="T2">num</ta>
            <ta e="T4" id="Seg_982" s="T3">n</ta>
            <ta e="T5" id="Seg_983" s="T4">v</ta>
            <ta e="T6" id="Seg_984" s="T5">quant</ta>
            <ta e="T7" id="Seg_985" s="T6">n</ta>
            <ta e="T8" id="Seg_986" s="T7">num</ta>
            <ta e="T9" id="Seg_987" s="T8">n</ta>
            <ta e="T10" id="Seg_988" s="T9">v</ta>
            <ta e="T11" id="Seg_989" s="T10">pers</ta>
            <ta e="T12" id="Seg_990" s="T11">quant</ta>
            <ta e="T13" id="Seg_991" s="T12">v</ta>
            <ta e="T14" id="Seg_992" s="T13">emphpro</ta>
            <ta e="T15" id="Seg_993" s="T14">n</ta>
            <ta e="T16" id="Seg_994" s="T15">quant</ta>
            <ta e="T17" id="Seg_995" s="T16">v</ta>
            <ta e="T18" id="Seg_996" s="T17">pers</ta>
            <ta e="T19" id="Seg_997" s="T18">n</ta>
            <ta e="T20" id="Seg_998" s="T19">v</ta>
            <ta e="T21" id="Seg_999" s="T20">v</ta>
            <ta e="T22" id="Seg_1000" s="T21">v</ta>
            <ta e="T23" id="Seg_1001" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_1002" s="T23">adv</ta>
            <ta e="T25" id="Seg_1003" s="T24">v</ta>
            <ta e="T26" id="Seg_1004" s="T25">n</ta>
            <ta e="T27" id="Seg_1005" s="T26">v</ta>
            <ta e="T28" id="Seg_1006" s="T27">conj</ta>
            <ta e="T29" id="Seg_1007" s="T28">v</ta>
            <ta e="T30" id="Seg_1008" s="T29">pers</ta>
            <ta e="T31" id="Seg_1009" s="T30">n</ta>
            <ta e="T32" id="Seg_1010" s="T31">adv</ta>
            <ta e="T33" id="Seg_1011" s="T32">v</ta>
            <ta e="T35" id="Seg_1012" s="T34">n</ta>
            <ta e="T36" id="Seg_1013" s="T35">adj</ta>
            <ta e="T37" id="Seg_1014" s="T36">v</ta>
            <ta e="T38" id="Seg_1015" s="T37">conj</ta>
            <ta e="T39" id="Seg_1016" s="T38">n</ta>
            <ta e="T40" id="Seg_1017" s="T39">v</ta>
            <ta e="T41" id="Seg_1018" s="T40">adv</ta>
            <ta e="T42" id="Seg_1019" s="T41">pers</ta>
            <ta e="T43" id="Seg_1020" s="T42">v</ta>
            <ta e="T44" id="Seg_1021" s="T43">pers</ta>
            <ta e="T45" id="Seg_1022" s="T44">pers</ta>
            <ta e="T46" id="Seg_1023" s="T45">n</ta>
            <ta e="T47" id="Seg_1024" s="T46">v</ta>
            <ta e="T48" id="Seg_1025" s="T47">v</ta>
            <ta e="T49" id="Seg_1026" s="T48">n</ta>
            <ta e="T50" id="Seg_1027" s="T49">interrog</ta>
            <ta e="T51" id="Seg_1028" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_1029" s="T51">adv</ta>
            <ta e="T53" id="Seg_1030" s="T52">v</ta>
            <ta e="T54" id="Seg_1031" s="T53">pro</ta>
            <ta e="T55" id="Seg_1032" s="T54">ptcl</ta>
            <ta e="T56" id="Seg_1033" s="T55">v</ta>
            <ta e="T57" id="Seg_1034" s="T56">conj</ta>
            <ta e="T58" id="Seg_1035" s="T57">v</ta>
            <ta e="T59" id="Seg_1036" s="T58">adj</ta>
            <ta e="T60" id="Seg_1037" s="T59">adv</ta>
            <ta e="T61" id="Seg_1038" s="T60">conj</ta>
            <ta e="T62" id="Seg_1039" s="T61">quant</ta>
            <ta e="T63" id="Seg_1040" s="T62">adv</ta>
            <ta e="T64" id="Seg_1041" s="T63">n</ta>
            <ta e="T65" id="Seg_1042" s="T64">v</ta>
            <ta e="T66" id="Seg_1043" s="T65">dem</ta>
            <ta e="T67" id="Seg_1044" s="T66">n</ta>
            <ta e="T68" id="Seg_1045" s="T67">n</ta>
            <ta e="T69" id="Seg_1046" s="T68">nprop</ta>
            <ta e="T70" id="Seg_1047" s="T69">pers</ta>
            <ta e="T71" id="Seg_1048" s="T70">v</ta>
            <ta e="T72" id="Seg_1049" s="T71">pers</ta>
            <ta e="T73" id="Seg_1050" s="T72">quant</ta>
            <ta e="T74" id="Seg_1051" s="T73">n</ta>
            <ta e="T75" id="Seg_1052" s="T74">v</ta>
            <ta e="T76" id="Seg_1053" s="T75">pers</ta>
            <ta e="T77" id="Seg_1054" s="T76">num</ta>
            <ta e="T78" id="Seg_1055" s="T77">n</ta>
            <ta e="T79" id="Seg_1056" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_1057" s="T79">v</ta>
            <ta e="T81" id="Seg_1058" s="T80">num</ta>
            <ta e="T82" id="Seg_1059" s="T81">v</ta>
            <ta e="T83" id="Seg_1060" s="T82">nprop</ta>
            <ta e="T84" id="Seg_1061" s="T83">nprop</ta>
            <ta e="T85" id="Seg_1062" s="T84">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_1063" s="T1">adv:Time</ta>
            <ta e="T4" id="Seg_1064" s="T3">np:Th</ta>
            <ta e="T7" id="Seg_1065" s="T6">np.h:A</ta>
            <ta e="T9" id="Seg_1066" s="T8">np:Th</ta>
            <ta e="T11" id="Seg_1067" s="T10">pro.h:E</ta>
            <ta e="T14" id="Seg_1068" s="T13">pro.h:Poss</ta>
            <ta e="T15" id="Seg_1069" s="T14">np:Th</ta>
            <ta e="T16" id="Seg_1070" s="T15">np.h:E</ta>
            <ta e="T18" id="Seg_1071" s="T17">pro.h:E</ta>
            <ta e="T19" id="Seg_1072" s="T18">np.h:Th</ta>
            <ta e="T20" id="Seg_1073" s="T19">v:Th</ta>
            <ta e="T22" id="Seg_1074" s="T21">0.3.h:A</ta>
            <ta e="T24" id="Seg_1075" s="T23">adv:G</ta>
            <ta e="T25" id="Seg_1076" s="T24">v:Th</ta>
            <ta e="T26" id="Seg_1077" s="T25">np.h:Th</ta>
            <ta e="T29" id="Seg_1078" s="T28">0.3.h:Th</ta>
            <ta e="T30" id="Seg_1079" s="T29">pro.h:A</ta>
            <ta e="T31" id="Seg_1080" s="T30">np:P</ta>
            <ta e="T35" id="Seg_1081" s="T34">np:P</ta>
            <ta e="T37" id="Seg_1082" s="T36">0.3.h:A</ta>
            <ta e="T39" id="Seg_1083" s="T38">np.h:A</ta>
            <ta e="T41" id="Seg_1084" s="T40">adv:Time</ta>
            <ta e="T42" id="Seg_1085" s="T41">pro.h:G</ta>
            <ta e="T43" id="Seg_1086" s="T42">0.3.h:A</ta>
            <ta e="T44" id="Seg_1087" s="T43">pro.h:A</ta>
            <ta e="T45" id="Seg_1088" s="T44">pro.h:Poss</ta>
            <ta e="T46" id="Seg_1089" s="T45">np:P</ta>
            <ta e="T49" id="Seg_1090" s="T48">pro.h:A</ta>
            <ta e="T54" id="Seg_1091" s="T53">pro.h:Th</ta>
            <ta e="T56" id="Seg_1092" s="T55">0.3.h:E</ta>
            <ta e="T58" id="Seg_1093" s="T57">0.3.h:A</ta>
            <ta e="T59" id="Seg_1094" s="T58">np:P</ta>
            <ta e="T63" id="Seg_1095" s="T62">adv:Time</ta>
            <ta e="T64" id="Seg_1096" s="T63">np:Th</ta>
            <ta e="T67" id="Seg_1097" s="T66">np:Th</ta>
            <ta e="T69" id="Seg_1098" s="T68">np.h:A</ta>
            <ta e="T70" id="Seg_1099" s="T69">pro.h:R</ta>
            <ta e="T72" id="Seg_1100" s="T71">pro.h:E</ta>
            <ta e="T74" id="Seg_1101" s="T73">np:Th</ta>
            <ta e="T76" id="Seg_1102" s="T75">pro.h:E</ta>
            <ta e="T78" id="Seg_1103" s="T77">np:Th</ta>
            <ta e="T81" id="Seg_1104" s="T80">pro:Th</ta>
            <ta e="T82" id="Seg_1105" s="T81">0.1.h:E</ta>
            <ta e="T84" id="Seg_1106" s="T83">np.h:R</ta>
            <ta e="T85" id="Seg_1107" s="T84">0.1.h:A 0.3:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_1108" s="T3">np:S</ta>
            <ta e="T5" id="Seg_1109" s="T4">v:pred</ta>
            <ta e="T7" id="Seg_1110" s="T6">np.h:S</ta>
            <ta e="T9" id="Seg_1111" s="T8">np:O</ta>
            <ta e="T10" id="Seg_1112" s="T9">v:pred</ta>
            <ta e="T11" id="Seg_1113" s="T10">pro.h:S</ta>
            <ta e="T13" id="Seg_1114" s="T12">v:pred</ta>
            <ta e="T15" id="Seg_1115" s="T14">np:O</ta>
            <ta e="T16" id="Seg_1116" s="T15">np.h:S</ta>
            <ta e="T17" id="Seg_1117" s="T16">v:pred</ta>
            <ta e="T18" id="Seg_1118" s="T17">pro.h:S</ta>
            <ta e="T20" id="Seg_1119" s="T19">v:O</ta>
            <ta e="T21" id="Seg_1120" s="T20">v:pred</ta>
            <ta e="T22" id="Seg_1121" s="T21">0.3.h:S v:pred</ta>
            <ta e="T23" id="Seg_1122" s="T22">ptcl:pred</ta>
            <ta e="T25" id="Seg_1123" s="T24">v:O</ta>
            <ta e="T26" id="Seg_1124" s="T25">np.h:S</ta>
            <ta e="T27" id="Seg_1125" s="T26">v:pred</ta>
            <ta e="T29" id="Seg_1126" s="T28">0.3.h:S v:pred</ta>
            <ta e="T30" id="Seg_1127" s="T29">pro.h:S</ta>
            <ta e="T33" id="Seg_1128" s="T32">v:pred</ta>
            <ta e="T35" id="Seg_1129" s="T34">np:O</ta>
            <ta e="T37" id="Seg_1130" s="T36">0.3.h:S v:pred</ta>
            <ta e="T39" id="Seg_1131" s="T38">np.h:S</ta>
            <ta e="T40" id="Seg_1132" s="T39">v:pred</ta>
            <ta e="T43" id="Seg_1133" s="T42">0.3.h:S v:pred</ta>
            <ta e="T44" id="Seg_1134" s="T43">pro.h:S</ta>
            <ta e="T46" id="Seg_1135" s="T45">np:O</ta>
            <ta e="T47" id="Seg_1136" s="T46">v:pred</ta>
            <ta e="T49" id="Seg_1137" s="T48">pro.h:S</ta>
            <ta e="T53" id="Seg_1138" s="T52">v:pred</ta>
            <ta e="T54" id="Seg_1139" s="T53">pro.h:O</ta>
            <ta e="T56" id="Seg_1140" s="T55">0.3.h:S v:pred</ta>
            <ta e="T58" id="Seg_1141" s="T57">0.3.h:S v:pred</ta>
            <ta e="T64" id="Seg_1142" s="T63">np:S</ta>
            <ta e="T65" id="Seg_1143" s="T64">v:pred</ta>
            <ta e="T69" id="Seg_1144" s="T68">np.h:S</ta>
            <ta e="T71" id="Seg_1145" s="T70">v:pred</ta>
            <ta e="T72" id="Seg_1146" s="T71">pro.h:S</ta>
            <ta e="T74" id="Seg_1147" s="T73">np:O</ta>
            <ta e="T75" id="Seg_1148" s="T74">v:pred</ta>
            <ta e="T76" id="Seg_1149" s="T75">pro.h:S</ta>
            <ta e="T78" id="Seg_1150" s="T77">np:O</ta>
            <ta e="T80" id="Seg_1151" s="T79">v:pred</ta>
            <ta e="T81" id="Seg_1152" s="T80">pro:O</ta>
            <ta e="T82" id="Seg_1153" s="T81">0.1.h:S v:pred</ta>
            <ta e="T85" id="Seg_1154" s="T84">0.1.h:S v:pred 0.3:O</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_1155" s="T1">RUS:core</ta>
            <ta e="T6" id="Seg_1156" s="T5">RUS:core</ta>
            <ta e="T12" id="Seg_1157" s="T11">RUS:core</ta>
            <ta e="T16" id="Seg_1158" s="T15">RUS:core</ta>
            <ta e="T23" id="Seg_1159" s="T22">RUS:mod</ta>
            <ta e="T28" id="Seg_1160" s="T27">RUS:gram</ta>
            <ta e="T38" id="Seg_1161" s="T37">RUS:gram</ta>
            <ta e="T41" id="Seg_1162" s="T40">RUS:core</ta>
            <ta e="T47" id="Seg_1163" s="T46">RUS:core</ta>
            <ta e="T48" id="Seg_1164" s="T47">RUS:core</ta>
            <ta e="T51" id="Seg_1165" s="T50">RUS:gram</ta>
            <ta e="T57" id="Seg_1166" s="T56">RUS:gram</ta>
            <ta e="T58" id="Seg_1167" s="T57">RUS:gram</ta>
            <ta e="T63" id="Seg_1168" s="T62">RUS:core</ta>
            <ta e="T68" id="Seg_1169" s="T67">RUS:cult</ta>
            <ta e="T69" id="Seg_1170" s="T68">RUS:cult</ta>
            <ta e="T83" id="Seg_1171" s="T82">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_1172" s="T1">Earlier there was one language.</ta>
            <ta e="T10" id="Seg_1173" s="T5">All people spoke one language.</ta>
            <ta e="T13" id="Seg_1174" s="T10">All of them knew it.</ta>
            <ta e="T17" id="Seg_1175" s="T13">All of them knew their language.</ta>
            <ta e="T21" id="Seg_1176" s="T17">They wanted to get to know God.</ta>
            <ta e="T25" id="Seg_1177" s="T21">They said: “[We] should climb upwards.</ta>
            <ta e="T29" id="Seg_1178" s="T25">Whether God exists or not.”</ta>
            <ta e="T33" id="Seg_1179" s="T29">They started building stairs.</ta>
            <ta e="T37" id="Seg_1180" s="T33">They built high brick stairs.</ta>
            <ta e="T43" id="Seg_1181" s="T37">And God said: “Soon they'll climb up to me.”</ta>
            <ta e="T48" id="Seg_1182" s="T43">He mixed up their language.</ta>
            <ta e="T53" id="Seg_1183" s="T48">They started speaking differently.</ta>
            <ta e="T56" id="Seg_1184" s="T53">They don't understand each other.</ta>
            <ta e="T60" id="Seg_1185" s="T56">And they gave up building stairs.</ta>
            <ta e="T65" id="Seg_1186" s="T60">That's why there are many languages now.</ta>
            <ta e="T71" id="Seg_1187" s="T65">Uncle Andrej told me this tale.</ta>
            <ta e="T75" id="Seg_1188" s="T71">He knew many tales.</ta>
            <ta e="T80" id="Seg_1189" s="T75">I don't know any tales.</ta>
            <ta e="T85" id="Seg_1190" s="T80">I knew one, I told it to Andrej Petrovich.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_1191" s="T1">Früher gab es [nur] eine Sprache.</ta>
            <ta e="T10" id="Seg_1192" s="T5">Alle Menschen sprachen eine Sprache.</ta>
            <ta e="T13" id="Seg_1193" s="T10">Alle kannten sie.</ta>
            <ta e="T17" id="Seg_1194" s="T13">Alle kannten ihre Sprache.</ta>
            <ta e="T21" id="Seg_1195" s="T17">Sie wollten Gott kennenlernen.</ta>
            <ta e="T25" id="Seg_1196" s="T21">Sie sagten: "[Wir] sollten nach oben klettern.</ta>
            <ta e="T29" id="Seg_1197" s="T25">Ob Gott nun existiert oder nicht."</ta>
            <ta e="T33" id="Seg_1198" s="T29">Sie begannen Treppen zu bauen.</ta>
            <ta e="T37" id="Seg_1199" s="T33">Sie bauten hohe Treppen aus Ziegeln.</ta>
            <ta e="T43" id="Seg_1200" s="T37">Und Gott sagte: "Bald werden sie zu mir klettern."</ta>
            <ta e="T48" id="Seg_1201" s="T43">Er brachte ihre Sprache durcheinander.</ta>
            <ta e="T53" id="Seg_1202" s="T48">Sie begannen unterschiedlich zu sprechen.</ta>
            <ta e="T56" id="Seg_1203" s="T53">Sie verstehen einander nicht.</ta>
            <ta e="T60" id="Seg_1204" s="T56">Und sie gaben es auf, die Treppen zu bauen.</ta>
            <ta e="T65" id="Seg_1205" s="T60">Deswegen gibt es jetzt so viele Sprachen.</ta>
            <ta e="T71" id="Seg_1206" s="T65">Onkel Andrej hat mir diese Geschichte erzählt.</ta>
            <ta e="T75" id="Seg_1207" s="T71">Er kannte viele Geschichten.</ta>
            <ta e="T80" id="Seg_1208" s="T75">Ich kenne nicht eine Geschichte.</ta>
            <ta e="T85" id="Seg_1209" s="T80">Ich kannte eine, ich habe sie Andrej Petrovich erzählt.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_1210" s="T1">Раньше был один язык.</ta>
            <ta e="T10" id="Seg_1211" s="T5">Все люди на одном языке разговаривали.</ta>
            <ta e="T13" id="Seg_1212" s="T10">Они все знали.</ta>
            <ta e="T17" id="Seg_1213" s="T13">Свой язык все знали.</ta>
            <ta e="T21" id="Seg_1214" s="T17">Они Бога хотели узнать.</ta>
            <ta e="T25" id="Seg_1215" s="T21">Сказали: “Надо наверх залезть.</ta>
            <ta e="T29" id="Seg_1216" s="T25">Бог есть или нет”.</ta>
            <ta e="T33" id="Seg_1217" s="T29">Они лестницу стали делать.</ta>
            <ta e="T37" id="Seg_1218" s="T33">Кирпичную лестницу (= стену) высокую сделали.</ta>
            <ta e="T43" id="Seg_1219" s="T37">А Бог говорит: “Скоро ко мне залезут.”</ta>
            <ta e="T48" id="Seg_1220" s="T43">Он их язык спутал.</ta>
            <ta e="T53" id="Seg_1221" s="T48">Кто куда стал(и) разговаривать стали.</ta>
            <ta e="T56" id="Seg_1222" s="T53">Друг друга не понимают.</ta>
            <ta e="T60" id="Seg_1223" s="T56">И бросили лестницу (= стену) делать.</ta>
            <ta e="T65" id="Seg_1224" s="T60">Поэтому много теперь языков.</ta>
            <ta e="T71" id="Seg_1225" s="T65">Эту сказку мне рассказал дядя Андрей.</ta>
            <ta e="T75" id="Seg_1226" s="T71">Он много сказок знал.</ta>
            <ta e="T80" id="Seg_1227" s="T75">Я ни одной сказки не знаю.</ta>
            <ta e="T85" id="Seg_1228" s="T80">Одну знала, Андрею Петровичу рассказала.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_1229" s="T1">раньше был один язык</ta>
            <ta e="T10" id="Seg_1230" s="T5">все люди на одном языке разговаривали</ta>
            <ta e="T13" id="Seg_1231" s="T10">они все знали</ta>
            <ta e="T17" id="Seg_1232" s="T13">свой язык все знали</ta>
            <ta e="T21" id="Seg_1233" s="T17">они бога хотели узнать</ta>
            <ta e="T25" id="Seg_1234" s="T21">сказали надо наверх залезть</ta>
            <ta e="T29" id="Seg_1235" s="T25">бог есть или нет</ta>
            <ta e="T33" id="Seg_1236" s="T29">они лестницу сделали</ta>
            <ta e="T37" id="Seg_1237" s="T33">кирпичную стену высокую сделали</ta>
            <ta e="T43" id="Seg_1238" s="T37">бог говорит скоро ко мне залезут</ta>
            <ta e="T48" id="Seg_1239" s="T43">он их язык спутал</ta>
            <ta e="T53" id="Seg_1240" s="T48">кто куда стал(и) разговаривать</ta>
            <ta e="T56" id="Seg_1241" s="T53">друг друга не понимают</ta>
            <ta e="T60" id="Seg_1242" s="T56">бросили стену (лестницу) делать</ta>
            <ta e="T65" id="Seg_1243" s="T60">поэтому много теперь языков</ta>
            <ta e="T71" id="Seg_1244" s="T65">эту сказку мне рассказал дядя Андрей</ta>
            <ta e="T75" id="Seg_1245" s="T71">он много сказок знает</ta>
            <ta e="T80" id="Seg_1246" s="T75">я ни одной сказки не знаю</ta>
            <ta e="T85" id="Seg_1247" s="T80">одну знала Андрею Петровичу рассказала</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T25" id="Seg_1248" s="T21">[KuAI:] Variant: 'innesɨɣɨlɣu'. [BrM:] 'innesɨɣɨlgu' changed to 'inne sɨɣɨlgu'.</ta>
            <ta e="T33" id="Seg_1249" s="T29">[BrM:] 'melʼejübərattə' changed to 'melʼe jübərattə'.</ta>
            <ta e="T37" id="Seg_1250" s="T33">[KuAI:] Variant: 'mevattə'.</ta>
            <ta e="T53" id="Seg_1251" s="T48">[KuAI:] Variant: 'kulubpulʼe'.</ta>
            <ta e="T56" id="Seg_1252" s="T53">[BrM:] TR -lǯɨ?</ta>
            <ta e="T71" id="Seg_1253" s="T65">[BrM:] GEN instead of ACC?</ta>
            <ta e="T80" id="Seg_1254" s="T75">[BrM:] 'tʼäptäm nej' changed to 'tʼäptämnej'.</ta>
            <ta e="T85" id="Seg_1255" s="T80">[KuAI:] Variant: 'tunuzaw'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
