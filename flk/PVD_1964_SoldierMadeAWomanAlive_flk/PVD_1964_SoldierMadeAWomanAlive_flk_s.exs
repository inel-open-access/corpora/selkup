<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_SoldierMadeAWomanAlive_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_SoldierMadeAWomanAlive_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">329</ud-information>
            <ud-information attribute-name="# HIAT:w">234</ud-information>
            <ud-information attribute-name="# e">234</ud-information>
            <ud-information attribute-name="# HIAT:u">50</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
         <tli id="T204" />
         <tli id="T205" />
         <tli id="T206" />
         <tli id="T207" />
         <tli id="T208" />
         <tli id="T209" />
         <tli id="T210" />
         <tli id="T211" />
         <tli id="T212" />
         <tli id="T213" />
         <tli id="T214" />
         <tli id="T215" />
         <tli id="T216" />
         <tli id="T217" />
         <tli id="T218" />
         <tli id="T219" />
         <tli id="T220" />
         <tli id="T221" />
         <tli id="T222" />
         <tli id="T223" />
         <tli id="T224" />
         <tli id="T225" />
         <tli id="T226" />
         <tli id="T227" />
         <tli id="T228" />
         <tli id="T229" />
         <tli id="T230" />
         <tli id="T231" />
         <tli id="T232" />
         <tli id="T233" />
         <tli id="T234" />
         <tli id="T235" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T235" id="Seg_0" n="sc" s="T1">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Ilɨs</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">soldat</ts>
                  <nts id="Seg_8" n="HIAT:ip">.</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_11" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">Täp</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">sɨsarʼe</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">pon</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">armʼiɣan</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">sluʒil</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_29" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">I</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">jedotdə</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">üdatdə</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_41" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">Täp</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">čaʒɨn</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">jedoɣot</ts>
                  <nts id="Seg_50" n="HIAT:ip">.</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_53" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">Täb</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">jedotdə</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">jübɨn</ts>
                  <nts id="Seg_62" n="HIAT:ip">,</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_65" n="HIAT:w" s="T17">uːdə</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_68" n="HIAT:w" s="T18">čaːʒɨn</ts>
                  <nts id="Seg_69" n="HIAT:ip">.</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_72" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_74" n="HIAT:w" s="T19">Qula</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_77" n="HIAT:w" s="T20">absodʼi</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_80" n="HIAT:w" s="T21">matčattə</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_83" n="HIAT:w" s="T22">i</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_86" n="HIAT:w" s="T23">nüːtčattə</ts>
                  <nts id="Seg_87" n="HIAT:ip">.</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_90" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_92" n="HIAT:w" s="T24">Täp</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_95" n="HIAT:w" s="T25">okkɨr</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">jedotda</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">tülʼe</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">medɨn</ts>
                  <nts id="Seg_105" n="HIAT:ip">.</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_108" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_110" n="HIAT:w" s="T29">Jet</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_113" n="HIAT:w" s="T30">iːbɨgaj</ts>
                  <nts id="Seg_114" n="HIAT:ip">.</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_117" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_119" n="HIAT:w" s="T31">Nɨtdɨn</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_122" n="HIAT:w" s="T32">ilɨn</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_125" n="HIAT:w" s="T33">kupes</ts>
                  <nts id="Seg_126" n="HIAT:ip">.</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_129" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_131" n="HIAT:w" s="T34">Tebnan</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_134" n="HIAT:w" s="T35">okkɨr</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_137" n="HIAT:w" s="T36">nät</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_140" n="HIAT:w" s="T37">qupba</ts>
                  <nts id="Seg_141" n="HIAT:ip">.</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_144" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_146" n="HIAT:w" s="T38">Qula</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_149" n="HIAT:w" s="T39">wes</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_152" n="HIAT:w" s="T40">nɨgata</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_155" n="HIAT:w" s="T41">qulupbattə</ts>
                  <nts id="Seg_156" n="HIAT:ip">.</nts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_159" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_161" n="HIAT:w" s="T42">Täp</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_164" n="HIAT:w" s="T43">nɨlʼezen</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_167" n="HIAT:w" s="T44">i</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_170" n="HIAT:w" s="T45">soːkutdʼen</ts>
                  <nts id="Seg_171" n="HIAT:ip">:</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_173" n="HIAT:ip">“</nts>
                  <ts e="T47" id="Seg_175" n="HIAT:w" s="T46">Qajno</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_178" n="HIAT:w" s="T47">taqkulupbaltə</ts>
                  <nts id="Seg_179" n="HIAT:ip">?</nts>
                  <nts id="Seg_180" n="HIAT:ip">”</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_183" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_185" n="HIAT:w" s="T48">A</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_188" n="HIAT:w" s="T49">täbla</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_191" n="HIAT:w" s="T50">tʼärattə</ts>
                  <nts id="Seg_192" n="HIAT:ip">:</nts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_194" n="HIAT:ip">“</nts>
                  <ts e="T52" id="Seg_196" n="HIAT:w" s="T51">Kupeznan</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_199" n="HIAT:w" s="T52">nät</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_202" n="HIAT:w" s="T53">qumɨt</ts>
                  <nts id="Seg_203" n="HIAT:ip">.</nts>
                  <nts id="Seg_204" n="HIAT:ip">”</nts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_207" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_209" n="HIAT:w" s="T54">Tebla</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_212" n="HIAT:w" s="T55">tʼärattə</ts>
                  <nts id="Seg_213" n="HIAT:ip">:</nts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_215" n="HIAT:ip">“</nts>
                  <ts e="T57" id="Seg_217" n="HIAT:w" s="T56">Tap</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_220" n="HIAT:w" s="T57">tʼärɨs</ts>
                  <nts id="Seg_221" n="HIAT:ip">:</nts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_224" n="HIAT:w" s="T58">Qut</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_227" n="HIAT:w" s="T59">neu</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_230" n="HIAT:w" s="T60">ilʼeptätǯɨt</ts>
                  <nts id="Seg_231" n="HIAT:ip">,</nts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_234" n="HIAT:w" s="T61">näu</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_237" n="HIAT:w" s="T62">täbnä</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_240" n="HIAT:w" s="T63">meǯau</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_243" n="HIAT:w" s="T64">tabetdə</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_246" n="HIAT:w" s="T65">i</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_249" n="HIAT:w" s="T66">palawina</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_252" n="HIAT:w" s="T67">mučistwa</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_255" n="HIAT:w" s="T68">mennäǯan</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_257" n="HIAT:ip">(</nts>
                  <ts e="T70" id="Seg_259" n="HIAT:w" s="T69">meǯau</ts>
                  <nts id="Seg_260" n="HIAT:ip">)</nts>
                  <nts id="Seg_261" n="HIAT:ip">.</nts>
                  <nts id="Seg_262" n="HIAT:ip">”</nts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_265" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_267" n="HIAT:w" s="T70">Saldat</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_270" n="HIAT:w" s="T71">tʼärɨn</ts>
                  <nts id="Seg_271" n="HIAT:ip">:</nts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_274" n="HIAT:w" s="T72">Serkulan</ts>
                  <nts id="Seg_275" n="HIAT:ip">,</nts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_277" n="HIAT:ip">–</nts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_280" n="HIAT:w" s="T73">i</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_283" n="HIAT:w" s="T74">sernɨn</ts>
                  <nts id="Seg_284" n="HIAT:ip">.</nts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_287" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_289" n="HIAT:w" s="T75">Täbɨm</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_292" n="HIAT:w" s="T76">strečajnat</ts>
                  <nts id="Seg_293" n="HIAT:ip">.</nts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_296" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_298" n="HIAT:w" s="T77">Täp</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_301" n="HIAT:w" s="T78">sernan</ts>
                  <nts id="Seg_302" n="HIAT:ip">,</nts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_305" n="HIAT:w" s="T79">mannɨbat</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_307" n="HIAT:ip">(</nts>
                  <ts e="T81" id="Seg_309" n="HIAT:w" s="T80">näjɣum</ts>
                  <nts id="Seg_310" n="HIAT:ip">)</nts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_313" n="HIAT:w" s="T81">dʼepka</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_316" n="HIAT:w" s="T82">säɣɨn</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_319" n="HIAT:w" s="T83">ippa</ts>
                  <nts id="Seg_320" n="HIAT:ip">.</nts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_323" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_325" n="HIAT:w" s="T84">Saldat</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_328" n="HIAT:w" s="T85">tʼärɨn</ts>
                  <nts id="Seg_329" n="HIAT:ip">:</nts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_331" n="HIAT:ip">“</nts>
                  <ts e="T87" id="Seg_333" n="HIAT:w" s="T86">Wes</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_336" n="HIAT:w" s="T87">ponä</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_339" n="HIAT:w" s="T88">qwannaltə</ts>
                  <nts id="Seg_340" n="HIAT:ip">.</nts>
                  <nts id="Seg_341" n="HIAT:ip">”</nts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_344" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_346" n="HIAT:w" s="T89">Täbla</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_349" n="HIAT:w" s="T90">wes</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_352" n="HIAT:w" s="T91">ponä</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_355" n="HIAT:w" s="T92">qwannattə</ts>
                  <nts id="Seg_356" n="HIAT:ip">.</nts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_359" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_361" n="HIAT:w" s="T93">Täp</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_364" n="HIAT:w" s="T94">akoškalam</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_367" n="HIAT:w" s="T95">zanawesinat</ts>
                  <nts id="Seg_368" n="HIAT:ip">.</nts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_371" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_373" n="HIAT:w" s="T96">A</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_376" n="HIAT:w" s="T97">okkɨr</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_379" n="HIAT:w" s="T98">akoška</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_382" n="HIAT:w" s="T99">nʼümmupa</ts>
                  <nts id="Seg_383" n="HIAT:ip">.</nts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_386" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_388" n="HIAT:w" s="T100">Täp</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_391" n="HIAT:w" s="T101">tʼepkam</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_394" n="HIAT:w" s="T102">säɣɨndə</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_397" n="HIAT:w" s="T103">saltšɨbɨt</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_400" n="HIAT:w" s="T104">pennɨt</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_403" n="HIAT:w" s="T105">i</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_406" n="HIAT:w" s="T106">malʼne</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_409" n="HIAT:w" s="T107">übɨrat</ts>
                  <nts id="Seg_410" n="HIAT:ip">.</nts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_413" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_415" n="HIAT:w" s="T108">Täp</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_418" n="HIAT:w" s="T109">ilʼedʼin</ts>
                  <nts id="Seg_419" n="HIAT:ip">.</nts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T114" id="Seg_422" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_424" n="HIAT:w" s="T110">Okkɨr</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_427" n="HIAT:w" s="T111">täbeɣum</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_430" n="HIAT:w" s="T112">akoškau</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_433" n="HIAT:w" s="T113">mannɨpba</ts>
                  <nts id="Seg_434" n="HIAT:ip">.</nts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_437" n="HIAT:u" s="T114">
                  <ts e="T115" id="Seg_439" n="HIAT:w" s="T114">Saldat</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_442" n="HIAT:w" s="T115">ponä</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_445" n="HIAT:w" s="T116">čaǯɨn</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_448" n="HIAT:w" s="T117">tʼärɨn</ts>
                  <nts id="Seg_449" n="HIAT:ip">:</nts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_451" n="HIAT:ip">“</nts>
                  <ts e="T119" id="Seg_453" n="HIAT:w" s="T118">Sernaltə</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_456" n="HIAT:w" s="T119">maːttə</ts>
                  <nts id="Seg_457" n="HIAT:ip">,</nts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_460" n="HIAT:w" s="T120">dʼepka</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_463" n="HIAT:w" s="T121">ilʼedʼin</ts>
                  <nts id="Seg_464" n="HIAT:ip">.</nts>
                  <nts id="Seg_465" n="HIAT:ip">”</nts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_468" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_470" n="HIAT:w" s="T122">Ästɨ</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_473" n="HIAT:w" s="T123">tʼärɨn</ts>
                  <nts id="Seg_474" n="HIAT:ip">:</nts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_476" n="HIAT:ip">“</nts>
                  <ts e="T125" id="Seg_478" n="HIAT:w" s="T124">Täpär</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_481" n="HIAT:w" s="T125">täbɨm</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_484" n="HIAT:w" s="T126">iːttə</ts>
                  <nts id="Seg_485" n="HIAT:ip">.</nts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_488" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_490" n="HIAT:w" s="T127">Man</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_493" n="HIAT:w" s="T128">täɣɨtdɨlʼi</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_496" n="HIAT:w" s="T129">wes</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_499" n="HIAT:w" s="T130">meǯau</ts>
                  <nts id="Seg_500" n="HIAT:ip">.</nts>
                  <nts id="Seg_501" n="HIAT:ip">”</nts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T138" id="Seg_504" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_506" n="HIAT:w" s="T131">A</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_509" n="HIAT:w" s="T132">saldat</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_512" n="HIAT:w" s="T133">tʼärɨn</ts>
                  <nts id="Seg_513" n="HIAT:ip">:</nts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_515" n="HIAT:ip">“</nts>
                  <ts e="T135" id="Seg_517" n="HIAT:w" s="T134">Mekga</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_520" n="HIAT:w" s="T135">qaimnä</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_523" n="HIAT:w" s="T136">ne</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_526" n="HIAT:w" s="T137">nadə</ts>
                  <nts id="Seg_527" n="HIAT:ip">.</nts>
                  <nts id="Seg_528" n="HIAT:ip">”</nts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_531" n="HIAT:u" s="T138">
                  <nts id="Seg_532" n="HIAT:ip">“</nts>
                  <ts e="T139" id="Seg_534" n="HIAT:w" s="T138">Nu</ts>
                  <nts id="Seg_535" n="HIAT:ip">,</nts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_538" n="HIAT:w" s="T139">man</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_541" n="HIAT:w" s="T140">tekga</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_544" n="HIAT:w" s="T141">qomdä</ts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_547" n="HIAT:w" s="T142">menɨǯan</ts>
                  <nts id="Seg_548" n="HIAT:ip">.</nts>
                  <nts id="Seg_549" n="HIAT:ip">”</nts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T146" id="Seg_552" n="HIAT:u" s="T143">
                  <ts e="T144" id="Seg_554" n="HIAT:w" s="T143">Täp</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_557" n="HIAT:w" s="T144">qomdem</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_560" n="HIAT:w" s="T145">meot</ts>
                  <nts id="Seg_561" n="HIAT:ip">.</nts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T150" id="Seg_564" n="HIAT:u" s="T146">
                  <ts e="T147" id="Seg_566" n="HIAT:w" s="T146">I</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_569" n="HIAT:w" s="T147">saldat</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_572" n="HIAT:w" s="T148">na</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_575" n="HIAT:w" s="T149">qwatda</ts>
                  <nts id="Seg_576" n="HIAT:ip">.</nts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_579" n="HIAT:u" s="T150">
                  <ts e="T151" id="Seg_581" n="HIAT:w" s="T150">Jedoɣot</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_584" n="HIAT:w" s="T151">tülʼe</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_587" n="HIAT:w" s="T152">medɨn</ts>
                  <nts id="Seg_588" n="HIAT:ip">.</nts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T158" id="Seg_591" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_593" n="HIAT:w" s="T153">Ästä</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_596" n="HIAT:w" s="T154">i</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_599" n="HIAT:w" s="T155">audä</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_602" n="HIAT:w" s="T156">erapbaɣə</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_605" n="HIAT:w" s="T157">pajäpbaɣə</ts>
                  <nts id="Seg_606" n="HIAT:ip">.</nts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T163" id="Seg_609" n="HIAT:u" s="T158">
                  <ts e="T159" id="Seg_611" n="HIAT:w" s="T158">Datao</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_614" n="HIAT:w" s="T159">aːtdalbak</ts>
                  <nts id="Seg_615" n="HIAT:ip">,</nts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_618" n="HIAT:w" s="T160">što</ts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_621" n="HIAT:w" s="T161">iːt</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_624" n="HIAT:w" s="T162">tütda</ts>
                  <nts id="Seg_625" n="HIAT:ip">.</nts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T167" id="Seg_628" n="HIAT:u" s="T163">
                  <ts e="T164" id="Seg_630" n="HIAT:w" s="T163">Iːt</ts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_633" n="HIAT:w" s="T164">soːdʼiga</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_636" n="HIAT:w" s="T165">maːt</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_639" n="HIAT:w" s="T166">taotdɨt</ts>
                  <nts id="Seg_640" n="HIAT:ip">.</nts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T171" id="Seg_643" n="HIAT:u" s="T167">
                  <ts e="T168" id="Seg_645" n="HIAT:w" s="T167">Täbɨnan</ts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_648" n="HIAT:w" s="T168">qomdät</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_651" n="HIAT:w" s="T169">qoːcʼin</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_654" n="HIAT:w" s="T170">jen</ts>
                  <nts id="Seg_655" n="HIAT:ip">.</nts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T173" id="Seg_658" n="HIAT:u" s="T171">
                  <ts e="T172" id="Seg_660" n="HIAT:w" s="T171">I</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_663" n="HIAT:w" s="T172">nädɨn</ts>
                  <nts id="Seg_664" n="HIAT:ip">.</nts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T178" id="Seg_667" n="HIAT:u" s="T173">
                  <ts e="T174" id="Seg_669" n="HIAT:w" s="T173">I</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_672" n="HIAT:w" s="T174">qoupbɨle</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_675" n="HIAT:w" s="T175">warkɨlʼe</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_678" n="HIAT:w" s="T176">na</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_681" n="HIAT:w" s="T177">übɨrɨtdattə</ts>
                  <nts id="Seg_682" n="HIAT:ip">.</nts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T181" id="Seg_685" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_687" n="HIAT:w" s="T178">Täpär</ts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_690" n="HIAT:w" s="T179">soːn</ts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_693" n="HIAT:w" s="T180">warkatdə</ts>
                  <nts id="Seg_694" n="HIAT:ip">.</nts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T186" id="Seg_697" n="HIAT:u" s="T181">
                  <ts e="T182" id="Seg_699" n="HIAT:w" s="T181">Saldat</ts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_702" n="HIAT:w" s="T182">täper</ts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_705" n="HIAT:w" s="T183">qwannɨn</ts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_708" n="HIAT:w" s="T184">kütdɨm</ts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_711" n="HIAT:w" s="T185">daugu</ts>
                  <nts id="Seg_712" n="HIAT:ip">.</nts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T188" id="Seg_715" n="HIAT:u" s="T186">
                  <ts e="T187" id="Seg_717" n="HIAT:w" s="T186">Čaːʒɨn</ts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_720" n="HIAT:w" s="T187">taːɣɨn</ts>
                  <nts id="Seg_721" n="HIAT:ip">.</nts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T192" id="Seg_724" n="HIAT:u" s="T188">
                  <ts e="T189" id="Seg_726" n="HIAT:w" s="T188">Qula</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_729" n="HIAT:w" s="T189">wes</ts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_732" n="HIAT:w" s="T190">pakosqɨn</ts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_735" n="HIAT:w" s="T191">jewattə</ts>
                  <nts id="Seg_736" n="HIAT:ip">.</nts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T199" id="Seg_739" n="HIAT:u" s="T192">
                  <ts e="T193" id="Seg_741" n="HIAT:w" s="T192">Iːbɨɣaj</ts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_744" n="HIAT:w" s="T193">edotdə</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_747" n="HIAT:w" s="T194">tülʼe</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_750" n="HIAT:w" s="T195">medɨn</ts>
                  <nts id="Seg_751" n="HIAT:ip">,</nts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_754" n="HIAT:w" s="T196">qutʼen</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_757" n="HIAT:w" s="T197">täp</ts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_760" n="HIAT:w" s="T198">jes</ts>
                  <nts id="Seg_761" n="HIAT:ip">.</nts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T205" id="Seg_764" n="HIAT:u" s="T199">
                  <ts e="T200" id="Seg_766" n="HIAT:w" s="T199">Krajnaj</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_769" n="HIAT:w" s="T200">qɨba</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_772" n="HIAT:w" s="T201">matqɨn</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_775" n="HIAT:w" s="T202">datao</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_778" n="HIAT:w" s="T203">qula</ts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_781" n="HIAT:w" s="T204">takqulupbattə</ts>
                  <nts id="Seg_782" n="HIAT:ip">.</nts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T210" id="Seg_785" n="HIAT:u" s="T205">
                  <ts e="T206" id="Seg_787" n="HIAT:w" s="T205">Täp</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_790" n="HIAT:w" s="T206">soɣudʼen</ts>
                  <nts id="Seg_791" n="HIAT:ip">:</nts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_793" n="HIAT:ip">“</nts>
                  <ts e="T208" id="Seg_795" n="HIAT:w" s="T207">Te</ts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_798" n="HIAT:w" s="T208">qajno</ts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_801" n="HIAT:w" s="T209">taqkulupbaltə</ts>
                  <nts id="Seg_802" n="HIAT:ip">?</nts>
                  <nts id="Seg_803" n="HIAT:ip">”</nts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T214" id="Seg_806" n="HIAT:u" s="T210">
                  <nts id="Seg_807" n="HIAT:ip">“</nts>
                  <ts e="T211" id="Seg_809" n="HIAT:w" s="T210">A</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_812" n="HIAT:w" s="T211">menan</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_815" n="HIAT:w" s="T212">pajäga</ts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_818" n="HIAT:w" s="T213">qummɨtda</ts>
                  <nts id="Seg_819" n="HIAT:ip">.</nts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T224" id="Seg_822" n="HIAT:u" s="T214">
                  <ts e="T215" id="Seg_824" n="HIAT:w" s="T214">Me</ts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_827" n="HIAT:w" s="T215">täbɨm</ts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_830" n="HIAT:w" s="T216">nagurumdättə</ts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_833" n="HIAT:w" s="T217">nädälǯə</ts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_836" n="HIAT:w" s="T218">malʼlʼe</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_839" n="HIAT:w" s="T219">taːdərautə</ts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_842" n="HIAT:w" s="T220">i</ts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_845" n="HIAT:w" s="T221">ilʼeptəgu</ts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_848" n="HIAT:w" s="T222">ne</ts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_851" n="HIAT:w" s="T223">moʒem</ts>
                  <nts id="Seg_852" n="HIAT:ip">.</nts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T226" id="Seg_855" n="HIAT:u" s="T224">
                  <ts e="T225" id="Seg_857" n="HIAT:w" s="T224">Jass</ts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_860" n="HIAT:w" s="T225">ilʼeːdʼeǯin</ts>
                  <nts id="Seg_861" n="HIAT:ip">.</nts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T233" id="Seg_864" n="HIAT:u" s="T226">
                  <ts e="T227" id="Seg_866" n="HIAT:w" s="T226">A</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_869" n="HIAT:w" s="T227">kupezɨn</ts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_872" n="HIAT:w" s="T228">nät</ts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_875" n="HIAT:w" s="T229">qukus</ts>
                  <nts id="Seg_876" n="HIAT:ip">,</nts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_879" n="HIAT:w" s="T230">saldat</ts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_882" n="HIAT:w" s="T231">pelgattə</ts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_885" n="HIAT:w" s="T232">massɨt</ts>
                  <nts id="Seg_886" n="HIAT:ip">.</nts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T235" id="Seg_889" n="HIAT:u" s="T233">
                  <ts e="T234" id="Seg_891" n="HIAT:w" s="T233">Täp</ts>
                  <nts id="Seg_892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_894" n="HIAT:w" s="T234">ilʼezis</ts>
                  <nts id="Seg_895" n="HIAT:ip">.</nts>
                  <nts id="Seg_896" n="HIAT:ip">”</nts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T235" id="Seg_898" n="sc" s="T1">
               <ts e="T2" id="Seg_900" n="e" s="T1">Ilɨs </ts>
               <ts e="T3" id="Seg_902" n="e" s="T2">soldat. </ts>
               <ts e="T4" id="Seg_904" n="e" s="T3">Täp </ts>
               <ts e="T5" id="Seg_906" n="e" s="T4">sɨsarʼe </ts>
               <ts e="T6" id="Seg_908" n="e" s="T5">pon </ts>
               <ts e="T7" id="Seg_910" n="e" s="T6">armʼiɣan </ts>
               <ts e="T8" id="Seg_912" n="e" s="T7">sluʒil. </ts>
               <ts e="T9" id="Seg_914" n="e" s="T8">I </ts>
               <ts e="T10" id="Seg_916" n="e" s="T9">jedotdə </ts>
               <ts e="T11" id="Seg_918" n="e" s="T10">üdatdə. </ts>
               <ts e="T12" id="Seg_920" n="e" s="T11">Täp </ts>
               <ts e="T13" id="Seg_922" n="e" s="T12">čaʒɨn </ts>
               <ts e="T14" id="Seg_924" n="e" s="T13">jedoɣot. </ts>
               <ts e="T15" id="Seg_926" n="e" s="T14">Täb </ts>
               <ts e="T16" id="Seg_928" n="e" s="T15">jedotdə </ts>
               <ts e="T17" id="Seg_930" n="e" s="T16">jübɨn, </ts>
               <ts e="T18" id="Seg_932" n="e" s="T17">uːdə </ts>
               <ts e="T19" id="Seg_934" n="e" s="T18">čaːʒɨn. </ts>
               <ts e="T20" id="Seg_936" n="e" s="T19">Qula </ts>
               <ts e="T21" id="Seg_938" n="e" s="T20">absodʼi </ts>
               <ts e="T22" id="Seg_940" n="e" s="T21">matčattə </ts>
               <ts e="T23" id="Seg_942" n="e" s="T22">i </ts>
               <ts e="T24" id="Seg_944" n="e" s="T23">nüːtčattə. </ts>
               <ts e="T25" id="Seg_946" n="e" s="T24">Täp </ts>
               <ts e="T26" id="Seg_948" n="e" s="T25">okkɨr </ts>
               <ts e="T27" id="Seg_950" n="e" s="T26">jedotda </ts>
               <ts e="T28" id="Seg_952" n="e" s="T27">tülʼe </ts>
               <ts e="T29" id="Seg_954" n="e" s="T28">medɨn. </ts>
               <ts e="T30" id="Seg_956" n="e" s="T29">Jet </ts>
               <ts e="T31" id="Seg_958" n="e" s="T30">iːbɨgaj. </ts>
               <ts e="T32" id="Seg_960" n="e" s="T31">Nɨtdɨn </ts>
               <ts e="T33" id="Seg_962" n="e" s="T32">ilɨn </ts>
               <ts e="T34" id="Seg_964" n="e" s="T33">kupes. </ts>
               <ts e="T35" id="Seg_966" n="e" s="T34">Tebnan </ts>
               <ts e="T36" id="Seg_968" n="e" s="T35">okkɨr </ts>
               <ts e="T37" id="Seg_970" n="e" s="T36">nät </ts>
               <ts e="T38" id="Seg_972" n="e" s="T37">qupba. </ts>
               <ts e="T39" id="Seg_974" n="e" s="T38">Qula </ts>
               <ts e="T40" id="Seg_976" n="e" s="T39">wes </ts>
               <ts e="T41" id="Seg_978" n="e" s="T40">nɨgata </ts>
               <ts e="T42" id="Seg_980" n="e" s="T41">qulupbattə. </ts>
               <ts e="T43" id="Seg_982" n="e" s="T42">Täp </ts>
               <ts e="T44" id="Seg_984" n="e" s="T43">nɨlʼezen </ts>
               <ts e="T45" id="Seg_986" n="e" s="T44">i </ts>
               <ts e="T46" id="Seg_988" n="e" s="T45">soːkutdʼen: </ts>
               <ts e="T47" id="Seg_990" n="e" s="T46">“Qajno </ts>
               <ts e="T48" id="Seg_992" n="e" s="T47">taqkulupbaltə?” </ts>
               <ts e="T49" id="Seg_994" n="e" s="T48">A </ts>
               <ts e="T50" id="Seg_996" n="e" s="T49">täbla </ts>
               <ts e="T51" id="Seg_998" n="e" s="T50">tʼärattə: </ts>
               <ts e="T52" id="Seg_1000" n="e" s="T51">“Kupeznan </ts>
               <ts e="T53" id="Seg_1002" n="e" s="T52">nät </ts>
               <ts e="T54" id="Seg_1004" n="e" s="T53">qumɨt.” </ts>
               <ts e="T55" id="Seg_1006" n="e" s="T54">Tebla </ts>
               <ts e="T56" id="Seg_1008" n="e" s="T55">tʼärattə: </ts>
               <ts e="T57" id="Seg_1010" n="e" s="T56">“Tap </ts>
               <ts e="T58" id="Seg_1012" n="e" s="T57">tʼärɨs: </ts>
               <ts e="T59" id="Seg_1014" n="e" s="T58">Qut </ts>
               <ts e="T60" id="Seg_1016" n="e" s="T59">neu </ts>
               <ts e="T61" id="Seg_1018" n="e" s="T60">ilʼeptätǯɨt, </ts>
               <ts e="T62" id="Seg_1020" n="e" s="T61">näu </ts>
               <ts e="T63" id="Seg_1022" n="e" s="T62">täbnä </ts>
               <ts e="T64" id="Seg_1024" n="e" s="T63">meǯau </ts>
               <ts e="T65" id="Seg_1026" n="e" s="T64">tabetdə </ts>
               <ts e="T66" id="Seg_1028" n="e" s="T65">i </ts>
               <ts e="T67" id="Seg_1030" n="e" s="T66">palawina </ts>
               <ts e="T68" id="Seg_1032" n="e" s="T67">mučistwa </ts>
               <ts e="T69" id="Seg_1034" n="e" s="T68">mennäǯan </ts>
               <ts e="T70" id="Seg_1036" n="e" s="T69">(meǯau).” </ts>
               <ts e="T71" id="Seg_1038" n="e" s="T70">Saldat </ts>
               <ts e="T72" id="Seg_1040" n="e" s="T71">tʼärɨn: </ts>
               <ts e="T73" id="Seg_1042" n="e" s="T72">Serkulan, – </ts>
               <ts e="T74" id="Seg_1044" n="e" s="T73">i </ts>
               <ts e="T75" id="Seg_1046" n="e" s="T74">sernɨn. </ts>
               <ts e="T76" id="Seg_1048" n="e" s="T75">Täbɨm </ts>
               <ts e="T77" id="Seg_1050" n="e" s="T76">strečajnat. </ts>
               <ts e="T78" id="Seg_1052" n="e" s="T77">Täp </ts>
               <ts e="T79" id="Seg_1054" n="e" s="T78">sernan, </ts>
               <ts e="T80" id="Seg_1056" n="e" s="T79">mannɨbat </ts>
               <ts e="T81" id="Seg_1058" n="e" s="T80">(näjɣum) </ts>
               <ts e="T82" id="Seg_1060" n="e" s="T81">dʼepka </ts>
               <ts e="T83" id="Seg_1062" n="e" s="T82">säɣɨn </ts>
               <ts e="T84" id="Seg_1064" n="e" s="T83">ippa. </ts>
               <ts e="T85" id="Seg_1066" n="e" s="T84">Saldat </ts>
               <ts e="T86" id="Seg_1068" n="e" s="T85">tʼärɨn: </ts>
               <ts e="T87" id="Seg_1070" n="e" s="T86">“Wes </ts>
               <ts e="T88" id="Seg_1072" n="e" s="T87">ponä </ts>
               <ts e="T89" id="Seg_1074" n="e" s="T88">qwannaltə.” </ts>
               <ts e="T90" id="Seg_1076" n="e" s="T89">Täbla </ts>
               <ts e="T91" id="Seg_1078" n="e" s="T90">wes </ts>
               <ts e="T92" id="Seg_1080" n="e" s="T91">ponä </ts>
               <ts e="T93" id="Seg_1082" n="e" s="T92">qwannattə. </ts>
               <ts e="T94" id="Seg_1084" n="e" s="T93">Täp </ts>
               <ts e="T95" id="Seg_1086" n="e" s="T94">akoškalam </ts>
               <ts e="T96" id="Seg_1088" n="e" s="T95">zanawesinat. </ts>
               <ts e="T97" id="Seg_1090" n="e" s="T96">A </ts>
               <ts e="T98" id="Seg_1092" n="e" s="T97">okkɨr </ts>
               <ts e="T99" id="Seg_1094" n="e" s="T98">akoška </ts>
               <ts e="T100" id="Seg_1096" n="e" s="T99">nʼümmupa. </ts>
               <ts e="T101" id="Seg_1098" n="e" s="T100">Täp </ts>
               <ts e="T102" id="Seg_1100" n="e" s="T101">tʼepkam </ts>
               <ts e="T103" id="Seg_1102" n="e" s="T102">säɣɨndə </ts>
               <ts e="T104" id="Seg_1104" n="e" s="T103">saltšɨbɨt </ts>
               <ts e="T105" id="Seg_1106" n="e" s="T104">pennɨt </ts>
               <ts e="T106" id="Seg_1108" n="e" s="T105">i </ts>
               <ts e="T107" id="Seg_1110" n="e" s="T106">malʼne </ts>
               <ts e="T108" id="Seg_1112" n="e" s="T107">übɨrat. </ts>
               <ts e="T109" id="Seg_1114" n="e" s="T108">Täp </ts>
               <ts e="T110" id="Seg_1116" n="e" s="T109">ilʼedʼin. </ts>
               <ts e="T111" id="Seg_1118" n="e" s="T110">Okkɨr </ts>
               <ts e="T112" id="Seg_1120" n="e" s="T111">täbeɣum </ts>
               <ts e="T113" id="Seg_1122" n="e" s="T112">akoškau </ts>
               <ts e="T114" id="Seg_1124" n="e" s="T113">mannɨpba. </ts>
               <ts e="T115" id="Seg_1126" n="e" s="T114">Saldat </ts>
               <ts e="T116" id="Seg_1128" n="e" s="T115">ponä </ts>
               <ts e="T117" id="Seg_1130" n="e" s="T116">čaǯɨn </ts>
               <ts e="T118" id="Seg_1132" n="e" s="T117">tʼärɨn: </ts>
               <ts e="T119" id="Seg_1134" n="e" s="T118">“Sernaltə </ts>
               <ts e="T120" id="Seg_1136" n="e" s="T119">maːttə, </ts>
               <ts e="T121" id="Seg_1138" n="e" s="T120">dʼepka </ts>
               <ts e="T122" id="Seg_1140" n="e" s="T121">ilʼedʼin.” </ts>
               <ts e="T123" id="Seg_1142" n="e" s="T122">Ästɨ </ts>
               <ts e="T124" id="Seg_1144" n="e" s="T123">tʼärɨn: </ts>
               <ts e="T125" id="Seg_1146" n="e" s="T124">“Täpär </ts>
               <ts e="T126" id="Seg_1148" n="e" s="T125">täbɨm </ts>
               <ts e="T127" id="Seg_1150" n="e" s="T126">iːttə. </ts>
               <ts e="T128" id="Seg_1152" n="e" s="T127">Man </ts>
               <ts e="T129" id="Seg_1154" n="e" s="T128">täɣɨtdɨlʼi </ts>
               <ts e="T130" id="Seg_1156" n="e" s="T129">wes </ts>
               <ts e="T131" id="Seg_1158" n="e" s="T130">meǯau.” </ts>
               <ts e="T132" id="Seg_1160" n="e" s="T131">A </ts>
               <ts e="T133" id="Seg_1162" n="e" s="T132">saldat </ts>
               <ts e="T134" id="Seg_1164" n="e" s="T133">tʼärɨn: </ts>
               <ts e="T135" id="Seg_1166" n="e" s="T134">“Mekga </ts>
               <ts e="T136" id="Seg_1168" n="e" s="T135">qaimnä </ts>
               <ts e="T137" id="Seg_1170" n="e" s="T136">ne </ts>
               <ts e="T138" id="Seg_1172" n="e" s="T137">nadə.” </ts>
               <ts e="T139" id="Seg_1174" n="e" s="T138">“Nu, </ts>
               <ts e="T140" id="Seg_1176" n="e" s="T139">man </ts>
               <ts e="T141" id="Seg_1178" n="e" s="T140">tekga </ts>
               <ts e="T142" id="Seg_1180" n="e" s="T141">qomdä </ts>
               <ts e="T143" id="Seg_1182" n="e" s="T142">menɨǯan.” </ts>
               <ts e="T144" id="Seg_1184" n="e" s="T143">Täp </ts>
               <ts e="T145" id="Seg_1186" n="e" s="T144">qomdem </ts>
               <ts e="T146" id="Seg_1188" n="e" s="T145">meot. </ts>
               <ts e="T147" id="Seg_1190" n="e" s="T146">I </ts>
               <ts e="T148" id="Seg_1192" n="e" s="T147">saldat </ts>
               <ts e="T149" id="Seg_1194" n="e" s="T148">na </ts>
               <ts e="T150" id="Seg_1196" n="e" s="T149">qwatda. </ts>
               <ts e="T151" id="Seg_1198" n="e" s="T150">Jedoɣot </ts>
               <ts e="T152" id="Seg_1200" n="e" s="T151">tülʼe </ts>
               <ts e="T153" id="Seg_1202" n="e" s="T152">medɨn. </ts>
               <ts e="T154" id="Seg_1204" n="e" s="T153">Ästä </ts>
               <ts e="T155" id="Seg_1206" n="e" s="T154">i </ts>
               <ts e="T156" id="Seg_1208" n="e" s="T155">audä </ts>
               <ts e="T157" id="Seg_1210" n="e" s="T156">erapbaɣə </ts>
               <ts e="T158" id="Seg_1212" n="e" s="T157">pajäpbaɣə. </ts>
               <ts e="T159" id="Seg_1214" n="e" s="T158">Datao </ts>
               <ts e="T160" id="Seg_1216" n="e" s="T159">aːtdalbak, </ts>
               <ts e="T161" id="Seg_1218" n="e" s="T160">što </ts>
               <ts e="T162" id="Seg_1220" n="e" s="T161">iːt </ts>
               <ts e="T163" id="Seg_1222" n="e" s="T162">tütda. </ts>
               <ts e="T164" id="Seg_1224" n="e" s="T163">Iːt </ts>
               <ts e="T165" id="Seg_1226" n="e" s="T164">soːdʼiga </ts>
               <ts e="T166" id="Seg_1228" n="e" s="T165">maːt </ts>
               <ts e="T167" id="Seg_1230" n="e" s="T166">taotdɨt. </ts>
               <ts e="T168" id="Seg_1232" n="e" s="T167">Täbɨnan </ts>
               <ts e="T169" id="Seg_1234" n="e" s="T168">qomdät </ts>
               <ts e="T170" id="Seg_1236" n="e" s="T169">qoːcʼin </ts>
               <ts e="T171" id="Seg_1238" n="e" s="T170">jen. </ts>
               <ts e="T172" id="Seg_1240" n="e" s="T171">I </ts>
               <ts e="T173" id="Seg_1242" n="e" s="T172">nädɨn. </ts>
               <ts e="T174" id="Seg_1244" n="e" s="T173">I </ts>
               <ts e="T175" id="Seg_1246" n="e" s="T174">qoupbɨle </ts>
               <ts e="T176" id="Seg_1248" n="e" s="T175">warkɨlʼe </ts>
               <ts e="T177" id="Seg_1250" n="e" s="T176">na </ts>
               <ts e="T178" id="Seg_1252" n="e" s="T177">übɨrɨtdattə. </ts>
               <ts e="T179" id="Seg_1254" n="e" s="T178">Täpär </ts>
               <ts e="T180" id="Seg_1256" n="e" s="T179">soːn </ts>
               <ts e="T181" id="Seg_1258" n="e" s="T180">warkatdə. </ts>
               <ts e="T182" id="Seg_1260" n="e" s="T181">Saldat </ts>
               <ts e="T183" id="Seg_1262" n="e" s="T182">täper </ts>
               <ts e="T184" id="Seg_1264" n="e" s="T183">qwannɨn </ts>
               <ts e="T185" id="Seg_1266" n="e" s="T184">kütdɨm </ts>
               <ts e="T186" id="Seg_1268" n="e" s="T185">daugu. </ts>
               <ts e="T187" id="Seg_1270" n="e" s="T186">Čaːʒɨn </ts>
               <ts e="T188" id="Seg_1272" n="e" s="T187">taːɣɨn. </ts>
               <ts e="T189" id="Seg_1274" n="e" s="T188">Qula </ts>
               <ts e="T190" id="Seg_1276" n="e" s="T189">wes </ts>
               <ts e="T191" id="Seg_1278" n="e" s="T190">pakosqɨn </ts>
               <ts e="T192" id="Seg_1280" n="e" s="T191">jewattə. </ts>
               <ts e="T193" id="Seg_1282" n="e" s="T192">Iːbɨɣaj </ts>
               <ts e="T194" id="Seg_1284" n="e" s="T193">edotdə </ts>
               <ts e="T195" id="Seg_1286" n="e" s="T194">tülʼe </ts>
               <ts e="T196" id="Seg_1288" n="e" s="T195">medɨn, </ts>
               <ts e="T197" id="Seg_1290" n="e" s="T196">qutʼen </ts>
               <ts e="T198" id="Seg_1292" n="e" s="T197">täp </ts>
               <ts e="T199" id="Seg_1294" n="e" s="T198">jes. </ts>
               <ts e="T200" id="Seg_1296" n="e" s="T199">Krajnaj </ts>
               <ts e="T201" id="Seg_1298" n="e" s="T200">qɨba </ts>
               <ts e="T202" id="Seg_1300" n="e" s="T201">matqɨn </ts>
               <ts e="T203" id="Seg_1302" n="e" s="T202">datao </ts>
               <ts e="T204" id="Seg_1304" n="e" s="T203">qula </ts>
               <ts e="T205" id="Seg_1306" n="e" s="T204">takqulupbattə. </ts>
               <ts e="T206" id="Seg_1308" n="e" s="T205">Täp </ts>
               <ts e="T207" id="Seg_1310" n="e" s="T206">soɣudʼen: </ts>
               <ts e="T208" id="Seg_1312" n="e" s="T207">“Te </ts>
               <ts e="T209" id="Seg_1314" n="e" s="T208">qajno </ts>
               <ts e="T210" id="Seg_1316" n="e" s="T209">taqkulupbaltə?” </ts>
               <ts e="T211" id="Seg_1318" n="e" s="T210">“A </ts>
               <ts e="T212" id="Seg_1320" n="e" s="T211">menan </ts>
               <ts e="T213" id="Seg_1322" n="e" s="T212">pajäga </ts>
               <ts e="T214" id="Seg_1324" n="e" s="T213">qummɨtda. </ts>
               <ts e="T215" id="Seg_1326" n="e" s="T214">Me </ts>
               <ts e="T216" id="Seg_1328" n="e" s="T215">täbɨm </ts>
               <ts e="T217" id="Seg_1330" n="e" s="T216">nagurumdättə </ts>
               <ts e="T218" id="Seg_1332" n="e" s="T217">nädälǯə </ts>
               <ts e="T219" id="Seg_1334" n="e" s="T218">malʼlʼe </ts>
               <ts e="T220" id="Seg_1336" n="e" s="T219">taːdərautə </ts>
               <ts e="T221" id="Seg_1338" n="e" s="T220">i </ts>
               <ts e="T222" id="Seg_1340" n="e" s="T221">ilʼeptəgu </ts>
               <ts e="T223" id="Seg_1342" n="e" s="T222">ne </ts>
               <ts e="T224" id="Seg_1344" n="e" s="T223">moʒem. </ts>
               <ts e="T225" id="Seg_1346" n="e" s="T224">Jass </ts>
               <ts e="T226" id="Seg_1348" n="e" s="T225">ilʼeːdʼeǯin. </ts>
               <ts e="T227" id="Seg_1350" n="e" s="T226">A </ts>
               <ts e="T228" id="Seg_1352" n="e" s="T227">kupezɨn </ts>
               <ts e="T229" id="Seg_1354" n="e" s="T228">nät </ts>
               <ts e="T230" id="Seg_1356" n="e" s="T229">qukus, </ts>
               <ts e="T231" id="Seg_1358" n="e" s="T230">saldat </ts>
               <ts e="T232" id="Seg_1360" n="e" s="T231">pelgattə </ts>
               <ts e="T233" id="Seg_1362" n="e" s="T232">massɨt. </ts>
               <ts e="T234" id="Seg_1364" n="e" s="T233">Täp </ts>
               <ts e="T235" id="Seg_1366" n="e" s="T234">ilʼezis.” </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_1367" s="T1">PVD_1964_SoldierMadeAWomanAlive_flk.001 (001.001)</ta>
            <ta e="T8" id="Seg_1368" s="T3">PVD_1964_SoldierMadeAWomanAlive_flk.002 (001.002)</ta>
            <ta e="T11" id="Seg_1369" s="T8">PVD_1964_SoldierMadeAWomanAlive_flk.003 (001.003)</ta>
            <ta e="T14" id="Seg_1370" s="T11">PVD_1964_SoldierMadeAWomanAlive_flk.004 (001.004)</ta>
            <ta e="T19" id="Seg_1371" s="T14">PVD_1964_SoldierMadeAWomanAlive_flk.005 (001.005)</ta>
            <ta e="T24" id="Seg_1372" s="T19">PVD_1964_SoldierMadeAWomanAlive_flk.006 (001.006)</ta>
            <ta e="T29" id="Seg_1373" s="T24">PVD_1964_SoldierMadeAWomanAlive_flk.007 (001.007)</ta>
            <ta e="T31" id="Seg_1374" s="T29">PVD_1964_SoldierMadeAWomanAlive_flk.008 (001.008)</ta>
            <ta e="T34" id="Seg_1375" s="T31">PVD_1964_SoldierMadeAWomanAlive_flk.009 (001.009)</ta>
            <ta e="T38" id="Seg_1376" s="T34">PVD_1964_SoldierMadeAWomanAlive_flk.010 (001.010)</ta>
            <ta e="T42" id="Seg_1377" s="T38">PVD_1964_SoldierMadeAWomanAlive_flk.011 (001.011)</ta>
            <ta e="T48" id="Seg_1378" s="T42">PVD_1964_SoldierMadeAWomanAlive_flk.012 (001.012)</ta>
            <ta e="T54" id="Seg_1379" s="T48">PVD_1964_SoldierMadeAWomanAlive_flk.013 (001.013)</ta>
            <ta e="T70" id="Seg_1380" s="T54">PVD_1964_SoldierMadeAWomanAlive_flk.014 (001.014)</ta>
            <ta e="T75" id="Seg_1381" s="T70">PVD_1964_SoldierMadeAWomanAlive_flk.015 (001.015)</ta>
            <ta e="T77" id="Seg_1382" s="T75">PVD_1964_SoldierMadeAWomanAlive_flk.016 (001.016)</ta>
            <ta e="T84" id="Seg_1383" s="T77">PVD_1964_SoldierMadeAWomanAlive_flk.017 (001.017)</ta>
            <ta e="T89" id="Seg_1384" s="T84">PVD_1964_SoldierMadeAWomanAlive_flk.018 (001.018)</ta>
            <ta e="T93" id="Seg_1385" s="T89">PVD_1964_SoldierMadeAWomanAlive_flk.019 (001.019)</ta>
            <ta e="T96" id="Seg_1386" s="T93">PVD_1964_SoldierMadeAWomanAlive_flk.020 (001.020)</ta>
            <ta e="T100" id="Seg_1387" s="T96">PVD_1964_SoldierMadeAWomanAlive_flk.021 (001.021)</ta>
            <ta e="T108" id="Seg_1388" s="T100">PVD_1964_SoldierMadeAWomanAlive_flk.022 (001.022)</ta>
            <ta e="T110" id="Seg_1389" s="T108">PVD_1964_SoldierMadeAWomanAlive_flk.023 (001.023)</ta>
            <ta e="T114" id="Seg_1390" s="T110">PVD_1964_SoldierMadeAWomanAlive_flk.024 (001.024)</ta>
            <ta e="T122" id="Seg_1391" s="T114">PVD_1964_SoldierMadeAWomanAlive_flk.025 (001.025)</ta>
            <ta e="T127" id="Seg_1392" s="T122">PVD_1964_SoldierMadeAWomanAlive_flk.026 (001.026)</ta>
            <ta e="T131" id="Seg_1393" s="T127">PVD_1964_SoldierMadeAWomanAlive_flk.027 (001.027)</ta>
            <ta e="T138" id="Seg_1394" s="T131">PVD_1964_SoldierMadeAWomanAlive_flk.028 (001.028)</ta>
            <ta e="T143" id="Seg_1395" s="T138">PVD_1964_SoldierMadeAWomanAlive_flk.029 (001.029)</ta>
            <ta e="T146" id="Seg_1396" s="T143">PVD_1964_SoldierMadeAWomanAlive_flk.030 (001.030)</ta>
            <ta e="T150" id="Seg_1397" s="T146">PVD_1964_SoldierMadeAWomanAlive_flk.031 (001.031)</ta>
            <ta e="T153" id="Seg_1398" s="T150">PVD_1964_SoldierMadeAWomanAlive_flk.032 (001.032)</ta>
            <ta e="T158" id="Seg_1399" s="T153">PVD_1964_SoldierMadeAWomanAlive_flk.033 (001.033)</ta>
            <ta e="T163" id="Seg_1400" s="T158">PVD_1964_SoldierMadeAWomanAlive_flk.034 (001.034)</ta>
            <ta e="T167" id="Seg_1401" s="T163">PVD_1964_SoldierMadeAWomanAlive_flk.035 (001.035)</ta>
            <ta e="T171" id="Seg_1402" s="T167">PVD_1964_SoldierMadeAWomanAlive_flk.036 (001.036)</ta>
            <ta e="T173" id="Seg_1403" s="T171">PVD_1964_SoldierMadeAWomanAlive_flk.037 (001.037)</ta>
            <ta e="T178" id="Seg_1404" s="T173">PVD_1964_SoldierMadeAWomanAlive_flk.038 (001.038)</ta>
            <ta e="T181" id="Seg_1405" s="T178">PVD_1964_SoldierMadeAWomanAlive_flk.039 (001.039)</ta>
            <ta e="T186" id="Seg_1406" s="T181">PVD_1964_SoldierMadeAWomanAlive_flk.040 (001.040)</ta>
            <ta e="T188" id="Seg_1407" s="T186">PVD_1964_SoldierMadeAWomanAlive_flk.041 (001.041)</ta>
            <ta e="T192" id="Seg_1408" s="T188">PVD_1964_SoldierMadeAWomanAlive_flk.042 (001.042)</ta>
            <ta e="T199" id="Seg_1409" s="T192">PVD_1964_SoldierMadeAWomanAlive_flk.043 (001.043)</ta>
            <ta e="T205" id="Seg_1410" s="T199">PVD_1964_SoldierMadeAWomanAlive_flk.044 (001.044)</ta>
            <ta e="T210" id="Seg_1411" s="T205">PVD_1964_SoldierMadeAWomanAlive_flk.045 (001.045)</ta>
            <ta e="T214" id="Seg_1412" s="T210">PVD_1964_SoldierMadeAWomanAlive_flk.046 (001.046)</ta>
            <ta e="T224" id="Seg_1413" s="T214">PVD_1964_SoldierMadeAWomanAlive_flk.047 (001.047)</ta>
            <ta e="T226" id="Seg_1414" s="T224">PVD_1964_SoldierMadeAWomanAlive_flk.048 (001.048)</ta>
            <ta e="T233" id="Seg_1415" s="T226">PVD_1964_SoldierMadeAWomanAlive_flk.049 (001.049)</ta>
            <ta e="T235" id="Seg_1416" s="T233">PVD_1964_SoldierMadeAWomanAlive_flk.050 (001.050)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_1417" s="T1">и′лыс сол′дат.</ta>
            <ta e="T8" id="Seg_1418" s="T3">тӓп сы′сарʼе пон ′армʼиɣан служил.</ta>
            <ta e="T11" id="Seg_1419" s="T8">и ′jедотдъ ′ӱдатдъ.</ta>
            <ta e="T14" id="Seg_1420" s="T11">тӓп тшажын ′jедоɣот.</ta>
            <ta e="T19" id="Seg_1421" s="T14">тӓб ′jедотдъ ′jӱбын, ′ӯдъ ′тша̄жын..</ta>
            <ta e="T24" id="Seg_1422" s="T19">kу′lа аб′содʼи мат′тшаттъ и ′нӱ̄тшаттъ.</ta>
            <ta e="T29" id="Seg_1423" s="T24">тӓп о′ккыр ′jедотда тӱ′лʼе ме′дын.</ta>
            <ta e="T31" id="Seg_1424" s="T29">jет ′ӣбыгай.</ta>
            <ta e="T34" id="Seg_1425" s="T31">ныт′дын и′лын ку′пес.</ta>
            <ta e="T38" id="Seg_1426" s="T34">теб′нан о′ккыр нӓт ′kупба.</ta>
            <ta e="T42" id="Seg_1427" s="T38">kу′lа вес ны′га ‵таkуlуп′б̂аттъ.</ta>
            <ta e="T48" id="Seg_1428" s="T42">тӓп ′нылʼезен и ′со̄кутдʼен: kай′но ‵таkкуlуп′балтъ?</ta>
            <ta e="T54" id="Seg_1429" s="T48">а тӓб′ла тʼӓ′раттъ: ку′пезнан нӓт ′kум(м)ыт.</ta>
            <ta e="T70" id="Seg_1430" s="T54">тебла тʼӓ′раттъ: тӓп тʼӓ′рыс: kут ′не(ӓ)у̹ и′лʼептӓтджыт, ′нӓу тӓбнӓ ме′джау̹ та(ъ)′бетдъ и пала′вина ′мучиства ′меннӓ‵джан (меджау).</ta>
            <ta e="T75" id="Seg_1431" s="T70">сал′дат тʼӓ′рын: ‵серку′лан, и ′сернын.</ta>
            <ta e="T77" id="Seg_1432" s="T75">тӓбым стре′чайнат.</ta>
            <ta e="T84" id="Seg_1433" s="T77">тӓп ′сернан, манныб̂ат (нӓйɣум) ′дʼепка ′сӓɣын ип′па.</ta>
            <ta e="T89" id="Seg_1434" s="T84">сал′дат тʼӓрын: вес ′понӓ kwа′нналтъ.</ta>
            <ta e="T93" id="Seg_1435" s="T89">тӓб′ла вес ′понӓ kwа′ннаттъ.</ta>
            <ta e="T96" id="Seg_1436" s="T93">тӓп а′кошкалам з̂ана′весинат.</ta>
            <ta e="T100" id="Seg_1437" s="T96">а ′оккыр а′кошка ′нʼӱммупа.</ta>
            <ta e="T108" id="Seg_1438" s="T100">тӓп ′тʼепкам ′сӓɣындъ ′саlтшыбыт ′пенныт и ′малʼне ′ӱбырат.</ta>
            <ta e="T110" id="Seg_1439" s="T108">тӓп ′илʼедʼин.</ta>
            <ta e="T114" id="Seg_1440" s="T110">о′ккыр ′тӓ′беɣум а′кошкау ′манныпба.</ta>
            <ta e="T122" id="Seg_1441" s="T114">сал′дат ′понӓ ′тшаджын тʼӓ′рын: ′се(ӓ)рналтъ ′ма̄ттъ, дʼепка и′лʼедʼин.</ta>
            <ta e="T127" id="Seg_1442" s="T122">ӓ′сты тʼӓ′рын: тӓ′пӓр ′тӓбым ′ӣттъ.</ta>
            <ta e="T131" id="Seg_1443" s="T127">ман ′тӓɣытдылʼи вес ме′джау̹.</ta>
            <ta e="T138" id="Seg_1444" s="T131">а сал′дат тʼӓ′рын: ′мекга kаим′нӓ не надъ.</ta>
            <ta e="T143" id="Seg_1445" s="T138">ну, ман ′текга kом′дӓ ′меныджан.</ta>
            <ta e="T146" id="Seg_1446" s="T143">тӓп ′kомдем ме′от.</ta>
            <ta e="T150" id="Seg_1447" s="T146">и сал′дат на kwат′да.</ta>
            <ta e="T153" id="Seg_1448" s="T150">′jедоɣот ′тӱлʼе ме′дын.</ta>
            <ta e="T158" id="Seg_1449" s="T153">ӓс′тӓ и ау′дӓ е′рапб̂аɣъ па′jӓпбаɣъ.</ta>
            <ta e="T163" id="Seg_1450" s="T158">да ′тао ′а̄тдалбак, што ӣт ′тӱтда.</ta>
            <ta e="T167" id="Seg_1451" s="T163">′ӣт ′со̄дʼига ма̄т та′отдыт.</ta>
            <ta e="T171" id="Seg_1452" s="T167">тӓбы′нан kом′дӓт ′kо̄цʼин jен.</ta>
            <ta e="T173" id="Seg_1453" s="T171">и нӓ′дын.</ta>
            <ta e="T178" id="Seg_1454" s="T173">и ′kоу(п)б̂ыл̂е ′варкылʼе на ′ӱбырытдаттъ.</ta>
            <ta e="T181" id="Seg_1455" s="T178">тӓ′пӓр со̄н вар′катдъ.</ta>
            <ta e="T186" id="Seg_1456" s="T181">сал′дат тӓ′пер kwа′ннын кӱт′дым ′даугу.</ta>
            <ta e="T188" id="Seg_1457" s="T186">′тша̄жын та̄′ɣын.</ta>
            <ta e="T192" id="Seg_1458" s="T188">kу′ла вес па′косkын ′jеwаттъ.</ta>
            <ta e="T199" id="Seg_1459" s="T192">′ӣбыɣай едотдъ ′тӱлʼе ме′дын, kу′тʼен тӓп jес.</ta>
            <ta e="T205" id="Seg_1460" s="T199">′крайнай ′kыба ′матkын д̂а та′о ′kуlа такkуlуп′баттъ.</ta>
            <ta e="T210" id="Seg_1461" s="T205">тӓп соɣу′дʼен: те kай′но ‵таkкуlуп′балтъ?</ta>
            <ta e="T214" id="Seg_1462" s="T210">а ме′нан па′jӓга ′kуммытда.</ta>
            <ta e="T224" id="Seg_1463" s="T214">ме тӓ′бым ′нагурум′дӓттъ нӓ′дӓlджъ ′малʼлʼе ′та̄дъраутъ и и′лʼептъгу не можем.</ta>
            <ta e="T226" id="Seg_1464" s="T224">jасс и′лʼе̄′дʼеджин.</ta>
            <ta e="T233" id="Seg_1465" s="T226">а ку′пезын нӓт ′kукус, сал′дат пел′гаттъ ′массыт.</ta>
            <ta e="T235" id="Seg_1466" s="T233">тӓп и′лʼезис.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T3" id="Seg_1467" s="T1">ilɨs soldat.</ta>
            <ta e="T8" id="Seg_1468" s="T3">täp sɨsarʼe pon armʼiɣan sluʒil.</ta>
            <ta e="T11" id="Seg_1469" s="T8">i jedotdə üdatdə.</ta>
            <ta e="T14" id="Seg_1470" s="T11">täp tšaʒɨn jedoɣot.</ta>
            <ta e="T19" id="Seg_1471" s="T14">täb jedotdə jübɨn, uːdə tšaːʒɨn..</ta>
            <ta e="T24" id="Seg_1472" s="T19">qula absodʼi mattšattə i nüːtšattə.</ta>
            <ta e="T29" id="Seg_1473" s="T24">täp okkɨr jedotda tülʼe medɨn.</ta>
            <ta e="T31" id="Seg_1474" s="T29">jet iːbɨgaj.</ta>
            <ta e="T34" id="Seg_1475" s="T31">nɨtdɨn ilɨn kupes.</ta>
            <ta e="T38" id="Seg_1476" s="T34">tebnan okkɨr nät qupba.</ta>
            <ta e="T42" id="Seg_1477" s="T38">qula wes nɨga taqulupb̂attə.</ta>
            <ta e="T48" id="Seg_1478" s="T42">täp nɨlʼezen i soːkutdʼen: qajno taqkulupbaltə?</ta>
            <ta e="T54" id="Seg_1479" s="T48">a täbla tʼärattə: kupeznan nät qum(m)ɨt.</ta>
            <ta e="T70" id="Seg_1480" s="T54">tebla tʼärattə: täp tʼärɨs: qut ne(ä)u̹ ilʼeptätǯɨt, näu täbnä meǯau̹ ta(ə)betdə i palawina mučistwa mennäǯan (meǯau).</ta>
            <ta e="T75" id="Seg_1481" s="T70">saldat tʼärɨn: serkulan, i sernɨn.</ta>
            <ta e="T77" id="Seg_1482" s="T75">täbɨm strečajnat.</ta>
            <ta e="T84" id="Seg_1483" s="T77">täp sernan, mannɨb̂at (näjɣum) dʼepka säɣɨn ippa.</ta>
            <ta e="T89" id="Seg_1484" s="T84">saldat tʼärɨn: wes ponä qwannaltə.</ta>
            <ta e="T93" id="Seg_1485" s="T89">täbla wes ponä qwannattə.</ta>
            <ta e="T96" id="Seg_1486" s="T93">täp akoškalam ẑanawesinat.</ta>
            <ta e="T100" id="Seg_1487" s="T96">a okkɨr akoška nʼümmupa.</ta>
            <ta e="T108" id="Seg_1488" s="T100">täp tʼepkam säɣɨndə saltšɨbɨt pennɨt i malʼne übɨrat.</ta>
            <ta e="T110" id="Seg_1489" s="T108">täp ilʼedʼin.</ta>
            <ta e="T114" id="Seg_1490" s="T110">okkɨr täbeɣum akoškau mannɨpba.</ta>
            <ta e="T122" id="Seg_1491" s="T114">saldat ponä tšaǯɨn tʼärɨn: se(ä)rnaltə maːttə, dʼepka ilʼedʼin.</ta>
            <ta e="T127" id="Seg_1492" s="T122">ästɨ tʼärɨn: täpär täbɨm iːttə.</ta>
            <ta e="T131" id="Seg_1493" s="T127">man täɣɨtdɨlʼi wes meǯau̹.</ta>
            <ta e="T138" id="Seg_1494" s="T131">a saldat tʼärɨn: mekga qaimnä ne nadə.</ta>
            <ta e="T143" id="Seg_1495" s="T138">nu, man tekga qomdä menɨǯan.</ta>
            <ta e="T146" id="Seg_1496" s="T143">täp qomdem meot.</ta>
            <ta e="T150" id="Seg_1497" s="T146">i saldat na qwatda.</ta>
            <ta e="T153" id="Seg_1498" s="T150">jedoɣot tülʼe medɨn.</ta>
            <ta e="T158" id="Seg_1499" s="T153">ästä i audä erapb̂aɣə pajäpbaɣə.</ta>
            <ta e="T163" id="Seg_1500" s="T158">da tao aːtdalbak, što iːt tütda.</ta>
            <ta e="T167" id="Seg_1501" s="T163">iːt soːdʼiga maːt taotdɨt.</ta>
            <ta e="T171" id="Seg_1502" s="T167">täbɨnan qomdät qoːcʼin jen.</ta>
            <ta e="T173" id="Seg_1503" s="T171">i nädɨn.</ta>
            <ta e="T178" id="Seg_1504" s="T173">i qou(p)b̂ɨl̂e warkɨlʼe na übɨrɨtdattə.</ta>
            <ta e="T181" id="Seg_1505" s="T178">täpär soːn warkatdə.</ta>
            <ta e="T186" id="Seg_1506" s="T181">saldat täper qwannɨn kütdɨm daugu.</ta>
            <ta e="T188" id="Seg_1507" s="T186">tšaːʒɨn taːɣɨn.</ta>
            <ta e="T192" id="Seg_1508" s="T188">qula wes pakosqɨn jewattə.</ta>
            <ta e="T199" id="Seg_1509" s="T192">iːbɨɣaj edotdə tülʼe medɨn, qutʼen täp jes.</ta>
            <ta e="T205" id="Seg_1510" s="T199">krajnaj qɨba matqɨn d̂a tao qula takqulupbattə.</ta>
            <ta e="T210" id="Seg_1511" s="T205">täp soɣudʼen: te qajno taqkulupbaltə?</ta>
            <ta e="T214" id="Seg_1512" s="T210">a menan pajäga qummɨtda.</ta>
            <ta e="T224" id="Seg_1513" s="T214">me täbɨm nagurumdättə nädälǯə malʼlʼe taːdərautə i ilʼeptəgu ne moʒem.</ta>
            <ta e="T226" id="Seg_1514" s="T224">jass ilʼeːdʼeǯin.</ta>
            <ta e="T233" id="Seg_1515" s="T226">a kupezɨn nät qukus, saldat pelgattə massɨt.</ta>
            <ta e="T235" id="Seg_1516" s="T233">täp ilʼezis.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_1517" s="T1">Ilɨs soldat. </ta>
            <ta e="T8" id="Seg_1518" s="T3">Täp sɨsarʼe pon armʼiɣan sluʒil. </ta>
            <ta e="T11" id="Seg_1519" s="T8">I jedotdə üdatdə. </ta>
            <ta e="T14" id="Seg_1520" s="T11">Täp čaʒɨn jedoɣot. </ta>
            <ta e="T19" id="Seg_1521" s="T14">Täb jedotdə jübɨn, uːdə čaːʒɨn. </ta>
            <ta e="T24" id="Seg_1522" s="T19">Qula absodʼi matčattə i nüːtčattə. </ta>
            <ta e="T29" id="Seg_1523" s="T24">Täp okkɨr jedotda tülʼe medɨn. </ta>
            <ta e="T31" id="Seg_1524" s="T29">Jet iːbɨgaj. </ta>
            <ta e="T34" id="Seg_1525" s="T31">Nɨtdɨn ilɨn kupes. </ta>
            <ta e="T38" id="Seg_1526" s="T34">Tebnan okkɨr nät qupba. </ta>
            <ta e="T42" id="Seg_1527" s="T38">Qula wes nɨgata qulupbattə. </ta>
            <ta e="T48" id="Seg_1528" s="T42">Täp nɨlʼezen i soːkutdʼen: “Qajno taqkulupbaltə?” </ta>
            <ta e="T54" id="Seg_1529" s="T48">A täbla tʼärattə: “Kupeznan nät qumɨt.” </ta>
            <ta e="T70" id="Seg_1530" s="T54">Tebla tʼärattə: “Tap tʼärɨs: Qut neu ilʼeptätǯɨt, näu täbnä meǯau tabetdə i palawina mučistwa mennäǯan (meǯau).” </ta>
            <ta e="T75" id="Seg_1531" s="T70">Saldat tʼärɨn: Serkulan, – i sernɨn. </ta>
            <ta e="T77" id="Seg_1532" s="T75">Täbɨm strečajnat. </ta>
            <ta e="T84" id="Seg_1533" s="T77">Täp sernan, mannɨbat (näjɣum) dʼepka säɣɨn ippa. </ta>
            <ta e="T89" id="Seg_1534" s="T84">Saldat tʼärɨn: “Wes ponä qwannaltə.” </ta>
            <ta e="T93" id="Seg_1535" s="T89">Täbla wes ponä qwannattə. </ta>
            <ta e="T96" id="Seg_1536" s="T93">Täp akoškalam zanawesinat. </ta>
            <ta e="T100" id="Seg_1537" s="T96">A okkɨr akoška nʼümmupa. </ta>
            <ta e="T108" id="Seg_1538" s="T100">Täp tʼepkam säɣɨndə saltšɨbɨt pennɨt i malʼne übɨrat. </ta>
            <ta e="T110" id="Seg_1539" s="T108">Täp ilʼedʼin. </ta>
            <ta e="T114" id="Seg_1540" s="T110">Okkɨr täbeɣum akoškau mannɨpba. </ta>
            <ta e="T122" id="Seg_1541" s="T114">Saldat ponä čaǯɨn tʼärɨn: “Sernaltə maːttə, dʼepka ilʼedʼin.” </ta>
            <ta e="T127" id="Seg_1542" s="T122">Ästɨ tʼärɨn: “Täpär täbɨm iːttə. </ta>
            <ta e="T131" id="Seg_1543" s="T127">Man täɣɨtdɨlʼi wes meǯau.” </ta>
            <ta e="T138" id="Seg_1544" s="T131">A saldat tʼärɨn: “Mekga qaimnä ne nadə.” </ta>
            <ta e="T143" id="Seg_1545" s="T138">“Nu, man tekga qomdä menɨǯan.” </ta>
            <ta e="T146" id="Seg_1546" s="T143">Täp qomdem meot. </ta>
            <ta e="T150" id="Seg_1547" s="T146">I saldat na qwatda. </ta>
            <ta e="T153" id="Seg_1548" s="T150">Jedoɣot tülʼe medɨn. </ta>
            <ta e="T158" id="Seg_1549" s="T153">Ästä i audä erapbaɣə pajäpbaɣə. </ta>
            <ta e="T163" id="Seg_1550" s="T158">Datao aːtdalbak, što iːt tütda. </ta>
            <ta e="T167" id="Seg_1551" s="T163">Iːt soːdʼiga maːt taotdɨt. </ta>
            <ta e="T171" id="Seg_1552" s="T167">Täbɨnan qomdät qoːcʼin jen. </ta>
            <ta e="T173" id="Seg_1553" s="T171">I nädɨn. </ta>
            <ta e="T178" id="Seg_1554" s="T173">I qoupbɨle warkɨlʼe na übɨrɨtdattə. </ta>
            <ta e="T181" id="Seg_1555" s="T178">Täpär soːn warkatdə. </ta>
            <ta e="T186" id="Seg_1556" s="T181">Saldat täper qwannɨn kütdɨm daugu. </ta>
            <ta e="T188" id="Seg_1557" s="T186">Čaːʒɨn taːɣɨn. </ta>
            <ta e="T192" id="Seg_1558" s="T188">Qula wes pakosqɨn jewattə. </ta>
            <ta e="T199" id="Seg_1559" s="T192">Iːbɨɣaj edotdə tülʼe medɨn, qutʼen täp jes. </ta>
            <ta e="T205" id="Seg_1560" s="T199">Krajnaj qɨba matqɨn datao qula takqulupbattə. </ta>
            <ta e="T210" id="Seg_1561" s="T205">Täp soɣudʼen: “Te qajno taqkulupbaltə?” </ta>
            <ta e="T214" id="Seg_1562" s="T210">“A menan pajäga qummɨtda. </ta>
            <ta e="T224" id="Seg_1563" s="T214">Me täbɨm nagurumdättə nädälǯə malʼlʼe taːdərautə i ilʼeptəgu ne moʒem. </ta>
            <ta e="T226" id="Seg_1564" s="T224">Jass ilʼeːdʼeǯin. </ta>
            <ta e="T233" id="Seg_1565" s="T226">A kupezɨn nät qukus, saldat pelgattə massɨt. </ta>
            <ta e="T235" id="Seg_1566" s="T233">Täp ilʼezis.” </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_1567" s="T1">elɨ-s</ta>
            <ta e="T3" id="Seg_1568" s="T2">soldat</ta>
            <ta e="T4" id="Seg_1569" s="T3">täp</ta>
            <ta e="T5" id="Seg_1570" s="T4">sɨsarʼe</ta>
            <ta e="T6" id="Seg_1571" s="T5">po-n</ta>
            <ta e="T7" id="Seg_1572" s="T6">armʼi-ɣan</ta>
            <ta e="T9" id="Seg_1573" s="T8">i</ta>
            <ta e="T10" id="Seg_1574" s="T9">jedo-tdə</ta>
            <ta e="T11" id="Seg_1575" s="T10">üda-tdə</ta>
            <ta e="T12" id="Seg_1576" s="T11">täp</ta>
            <ta e="T13" id="Seg_1577" s="T12">čaʒɨ-n</ta>
            <ta e="T14" id="Seg_1578" s="T13">jedo-ɣot</ta>
            <ta e="T15" id="Seg_1579" s="T14">täb</ta>
            <ta e="T16" id="Seg_1580" s="T15">jedo-tdə</ta>
            <ta e="T17" id="Seg_1581" s="T16">jübɨ-n</ta>
            <ta e="T18" id="Seg_1582" s="T17">uːdə</ta>
            <ta e="T19" id="Seg_1583" s="T18">čaːʒɨ-n</ta>
            <ta e="T20" id="Seg_1584" s="T19">qu-la</ta>
            <ta e="T21" id="Seg_1585" s="T20">absodʼi</ta>
            <ta e="T22" id="Seg_1586" s="T21">matča-ttə</ta>
            <ta e="T23" id="Seg_1587" s="T22">i</ta>
            <ta e="T24" id="Seg_1588" s="T23">nüːt-ča-ttə</ta>
            <ta e="T25" id="Seg_1589" s="T24">täp</ta>
            <ta e="T26" id="Seg_1590" s="T25">okkɨr</ta>
            <ta e="T27" id="Seg_1591" s="T26">jedo-tda</ta>
            <ta e="T28" id="Seg_1592" s="T27">tü-lʼe</ta>
            <ta e="T29" id="Seg_1593" s="T28">medɨ-n</ta>
            <ta e="T30" id="Seg_1594" s="T29">jet</ta>
            <ta e="T31" id="Seg_1595" s="T30">iːbɨgaj</ta>
            <ta e="T32" id="Seg_1596" s="T31">nɨtdɨ-n</ta>
            <ta e="T33" id="Seg_1597" s="T32">ilɨ-n</ta>
            <ta e="T34" id="Seg_1598" s="T33">kupes</ta>
            <ta e="T35" id="Seg_1599" s="T34">teb-nan</ta>
            <ta e="T36" id="Seg_1600" s="T35">okkɨr</ta>
            <ta e="T37" id="Seg_1601" s="T36">nä-t</ta>
            <ta e="T38" id="Seg_1602" s="T37">qu-pba</ta>
            <ta e="T39" id="Seg_1603" s="T38">qu-la</ta>
            <ta e="T40" id="Seg_1604" s="T39">wes</ta>
            <ta e="T41" id="Seg_1605" s="T40">nɨg-a-ta</ta>
            <ta e="T42" id="Seg_1606" s="T41">qulupba-ttə</ta>
            <ta e="T43" id="Seg_1607" s="T42">täp</ta>
            <ta e="T44" id="Seg_1608" s="T43">nɨlʼe-ze-n</ta>
            <ta e="T45" id="Seg_1609" s="T44">i</ta>
            <ta e="T46" id="Seg_1610" s="T45">soːkutdʼe-n</ta>
            <ta e="T47" id="Seg_1611" s="T46">qaj-no</ta>
            <ta e="T48" id="Seg_1612" s="T47">taqku-lu-pba-ltə</ta>
            <ta e="T49" id="Seg_1613" s="T48">a</ta>
            <ta e="T50" id="Seg_1614" s="T49">täb-la</ta>
            <ta e="T51" id="Seg_1615" s="T50">tʼära-ttə</ta>
            <ta e="T52" id="Seg_1616" s="T51">kupez-nan</ta>
            <ta e="T53" id="Seg_1617" s="T52">nä-t</ta>
            <ta e="T54" id="Seg_1618" s="T53">qu-mɨ-t</ta>
            <ta e="T55" id="Seg_1619" s="T54">teb-la</ta>
            <ta e="T56" id="Seg_1620" s="T55">tʼära-ttə</ta>
            <ta e="T57" id="Seg_1621" s="T56">tap</ta>
            <ta e="T58" id="Seg_1622" s="T57">tʼärɨ-s</ta>
            <ta e="T59" id="Seg_1623" s="T58">qut</ta>
            <ta e="T60" id="Seg_1624" s="T59">ne-u</ta>
            <ta e="T61" id="Seg_1625" s="T60">ilʼe-pt-ätǯɨ-t</ta>
            <ta e="T62" id="Seg_1626" s="T61">nä-u</ta>
            <ta e="T63" id="Seg_1627" s="T62">täb-nä</ta>
            <ta e="T64" id="Seg_1628" s="T63">me-ǯa-u</ta>
            <ta e="T65" id="Seg_1629" s="T64">tabetdə</ta>
            <ta e="T66" id="Seg_1630" s="T65">i</ta>
            <ta e="T69" id="Seg_1631" s="T68">me-nnä-ǯa-n</ta>
            <ta e="T70" id="Seg_1632" s="T69">me-ǯa-u</ta>
            <ta e="T71" id="Seg_1633" s="T70">saldat</ta>
            <ta e="T72" id="Seg_1634" s="T71">tʼärɨ-n</ta>
            <ta e="T73" id="Seg_1635" s="T72">ser-ku-la-n</ta>
            <ta e="T74" id="Seg_1636" s="T73">i</ta>
            <ta e="T75" id="Seg_1637" s="T74">ser-nɨ-n</ta>
            <ta e="T76" id="Seg_1638" s="T75">täb-ɨ-m</ta>
            <ta e="T77" id="Seg_1639" s="T76">strečaj-na-t</ta>
            <ta e="T78" id="Seg_1640" s="T77">täp</ta>
            <ta e="T79" id="Seg_1641" s="T78">ser-na-n</ta>
            <ta e="T80" id="Seg_1642" s="T79">mannɨ-ba-t</ta>
            <ta e="T81" id="Seg_1643" s="T80">nä-j-ɣum</ta>
            <ta e="T82" id="Seg_1644" s="T81">dʼepka</ta>
            <ta e="T83" id="Seg_1645" s="T82">sä-ɣɨn</ta>
            <ta e="T84" id="Seg_1646" s="T83">ippa</ta>
            <ta e="T85" id="Seg_1647" s="T84">saldat</ta>
            <ta e="T86" id="Seg_1648" s="T85">tʼärɨ-n</ta>
            <ta e="T87" id="Seg_1649" s="T86">wes</ta>
            <ta e="T88" id="Seg_1650" s="T87">ponä</ta>
            <ta e="T89" id="Seg_1651" s="T88">qwan-naltə</ta>
            <ta e="T90" id="Seg_1652" s="T89">täb-la</ta>
            <ta e="T91" id="Seg_1653" s="T90">wes</ta>
            <ta e="T92" id="Seg_1654" s="T91">ponä</ta>
            <ta e="T93" id="Seg_1655" s="T92">qwan-na-ttə</ta>
            <ta e="T94" id="Seg_1656" s="T93">täp</ta>
            <ta e="T95" id="Seg_1657" s="T94">akoška-la-m</ta>
            <ta e="T96" id="Seg_1658" s="T95">zanawesi-na-t</ta>
            <ta e="T97" id="Seg_1659" s="T96">a</ta>
            <ta e="T98" id="Seg_1660" s="T97">okkɨr</ta>
            <ta e="T99" id="Seg_1661" s="T98">akoška</ta>
            <ta e="T100" id="Seg_1662" s="T99">nʼü-mmu-pa</ta>
            <ta e="T101" id="Seg_1663" s="T100">täp</ta>
            <ta e="T102" id="Seg_1664" s="T101">tʼepka-m</ta>
            <ta e="T103" id="Seg_1665" s="T102">sä-ɣɨndə</ta>
            <ta e="T104" id="Seg_1666" s="T103">saltšɨbɨ-t</ta>
            <ta e="T105" id="Seg_1667" s="T104">pen-nɨ-t</ta>
            <ta e="T106" id="Seg_1668" s="T105">i</ta>
            <ta e="T107" id="Seg_1669" s="T106">ma-lʼ-ne</ta>
            <ta e="T108" id="Seg_1670" s="T107">übɨ-r-a-t</ta>
            <ta e="T109" id="Seg_1671" s="T108">täp</ta>
            <ta e="T110" id="Seg_1672" s="T109">ilʼe-dʼi-n</ta>
            <ta e="T111" id="Seg_1673" s="T110">okkɨr</ta>
            <ta e="T112" id="Seg_1674" s="T111">täbe-ɣum</ta>
            <ta e="T113" id="Seg_1675" s="T112">akoška-u</ta>
            <ta e="T114" id="Seg_1676" s="T113">mannɨ-pba</ta>
            <ta e="T115" id="Seg_1677" s="T114">saldat</ta>
            <ta e="T116" id="Seg_1678" s="T115">ponä</ta>
            <ta e="T117" id="Seg_1679" s="T116">čaǯɨ-n</ta>
            <ta e="T118" id="Seg_1680" s="T117">tʼärɨ-n</ta>
            <ta e="T119" id="Seg_1681" s="T118">ser-naltə</ta>
            <ta e="T120" id="Seg_1682" s="T119">maːt-tə</ta>
            <ta e="T121" id="Seg_1683" s="T120">dʼepka</ta>
            <ta e="T122" id="Seg_1684" s="T121">ilʼe-dʼi-n</ta>
            <ta e="T123" id="Seg_1685" s="T122">äs-tɨ</ta>
            <ta e="T124" id="Seg_1686" s="T123">tʼärɨ-n</ta>
            <ta e="T125" id="Seg_1687" s="T124">täpär</ta>
            <ta e="T126" id="Seg_1688" s="T125">täb-ɨ-m</ta>
            <ta e="T127" id="Seg_1689" s="T126">iː-ttə</ta>
            <ta e="T128" id="Seg_1690" s="T127">man</ta>
            <ta e="T129" id="Seg_1691" s="T128">täɣɨtdɨlʼi</ta>
            <ta e="T130" id="Seg_1692" s="T129">wes</ta>
            <ta e="T131" id="Seg_1693" s="T130">me-ǯa-u</ta>
            <ta e="T132" id="Seg_1694" s="T131">a</ta>
            <ta e="T133" id="Seg_1695" s="T132">saldat</ta>
            <ta e="T134" id="Seg_1696" s="T133">tʼärɨ-n</ta>
            <ta e="T135" id="Seg_1697" s="T134">mekga</ta>
            <ta e="T136" id="Seg_1698" s="T135">qai-m-nä</ta>
            <ta e="T137" id="Seg_1699" s="T136">ne</ta>
            <ta e="T138" id="Seg_1700" s="T137">nadə</ta>
            <ta e="T139" id="Seg_1701" s="T138">nu</ta>
            <ta e="T140" id="Seg_1702" s="T139">man</ta>
            <ta e="T141" id="Seg_1703" s="T140">tekga</ta>
            <ta e="T142" id="Seg_1704" s="T141">qomdä</ta>
            <ta e="T143" id="Seg_1705" s="T142">me-nɨ-ǯa-n</ta>
            <ta e="T144" id="Seg_1706" s="T143">täp</ta>
            <ta e="T145" id="Seg_1707" s="T144">qomde-m</ta>
            <ta e="T146" id="Seg_1708" s="T145">me-o-t</ta>
            <ta e="T147" id="Seg_1709" s="T146">i</ta>
            <ta e="T148" id="Seg_1710" s="T147">saldat</ta>
            <ta e="T149" id="Seg_1711" s="T148">na</ta>
            <ta e="T150" id="Seg_1712" s="T149">qwat-da</ta>
            <ta e="T151" id="Seg_1713" s="T150">jedo-ɣot</ta>
            <ta e="T152" id="Seg_1714" s="T151">tü-lʼe</ta>
            <ta e="T153" id="Seg_1715" s="T152">medɨ-n</ta>
            <ta e="T154" id="Seg_1716" s="T153">äs-tä</ta>
            <ta e="T155" id="Seg_1717" s="T154">i</ta>
            <ta e="T156" id="Seg_1718" s="T155">au-dä</ta>
            <ta e="T157" id="Seg_1719" s="T156">era-p-ba-ɣə</ta>
            <ta e="T158" id="Seg_1720" s="T157">pajä-p-ba-ɣə</ta>
            <ta e="T159" id="Seg_1721" s="T158">datao</ta>
            <ta e="T160" id="Seg_1722" s="T159">aːtdal-ba-k</ta>
            <ta e="T161" id="Seg_1723" s="T160">što</ta>
            <ta e="T162" id="Seg_1724" s="T161">iː-t</ta>
            <ta e="T163" id="Seg_1725" s="T162">tü-tda</ta>
            <ta e="T164" id="Seg_1726" s="T163">iː-t</ta>
            <ta e="T165" id="Seg_1727" s="T164">soːdʼiga</ta>
            <ta e="T166" id="Seg_1728" s="T165">maːt</ta>
            <ta e="T167" id="Seg_1729" s="T166">tao-tdɨ-t</ta>
            <ta e="T168" id="Seg_1730" s="T167">täb-ɨ-nan</ta>
            <ta e="T169" id="Seg_1731" s="T168">qomdä-t</ta>
            <ta e="T170" id="Seg_1732" s="T169">qoːcʼi-n</ta>
            <ta e="T171" id="Seg_1733" s="T170">je-n</ta>
            <ta e="T172" id="Seg_1734" s="T171">i</ta>
            <ta e="T173" id="Seg_1735" s="T172">nädɨ-n</ta>
            <ta e="T174" id="Seg_1736" s="T173">i</ta>
            <ta e="T175" id="Seg_1737" s="T174">qou-p-bɨ-le</ta>
            <ta e="T176" id="Seg_1738" s="T175">warkɨ-lʼe</ta>
            <ta e="T177" id="Seg_1739" s="T176">na</ta>
            <ta e="T178" id="Seg_1740" s="T177">übɨ-r-ɨ-tda-ttə</ta>
            <ta e="T179" id="Seg_1741" s="T178">täpär</ta>
            <ta e="T180" id="Seg_1742" s="T179">soːn</ta>
            <ta e="T181" id="Seg_1743" s="T180">warka-tdə</ta>
            <ta e="T182" id="Seg_1744" s="T181">saldat</ta>
            <ta e="T183" id="Seg_1745" s="T182">täper</ta>
            <ta e="T184" id="Seg_1746" s="T183">qwan-nɨ-n</ta>
            <ta e="T185" id="Seg_1747" s="T184">kütdɨ-m</ta>
            <ta e="T186" id="Seg_1748" s="T185">dau-gu</ta>
            <ta e="T187" id="Seg_1749" s="T186">čaːʒɨ-n</ta>
            <ta e="T188" id="Seg_1750" s="T187">taːɣ-ɨ-n</ta>
            <ta e="T189" id="Seg_1751" s="T188">qu-la</ta>
            <ta e="T190" id="Seg_1752" s="T189">wes</ta>
            <ta e="T191" id="Seg_1753" s="T190">pakos-qɨn</ta>
            <ta e="T192" id="Seg_1754" s="T191">je-wa-ttə</ta>
            <ta e="T193" id="Seg_1755" s="T192">iːbɨɣaj</ta>
            <ta e="T194" id="Seg_1756" s="T193">edo-tdə</ta>
            <ta e="T195" id="Seg_1757" s="T194">tü-lʼe</ta>
            <ta e="T196" id="Seg_1758" s="T195">medɨ-n</ta>
            <ta e="T197" id="Seg_1759" s="T196">qutʼen</ta>
            <ta e="T198" id="Seg_1760" s="T197">täp</ta>
            <ta e="T199" id="Seg_1761" s="T198">je-s</ta>
            <ta e="T200" id="Seg_1762" s="T199">krajnaj</ta>
            <ta e="T201" id="Seg_1763" s="T200">qɨba</ta>
            <ta e="T202" id="Seg_1764" s="T201">mat-qɨn</ta>
            <ta e="T203" id="Seg_1765" s="T202">datao</ta>
            <ta e="T204" id="Seg_1766" s="T203">qu-la</ta>
            <ta e="T205" id="Seg_1767" s="T204">takqu-lu-pba-ttə</ta>
            <ta e="T206" id="Seg_1768" s="T205">täp</ta>
            <ta e="T207" id="Seg_1769" s="T206">soɣudʼe-n</ta>
            <ta e="T208" id="Seg_1770" s="T207">te</ta>
            <ta e="T209" id="Seg_1771" s="T208">qaj-no</ta>
            <ta e="T210" id="Seg_1772" s="T209">taqku-lu-pba-ltə</ta>
            <ta e="T211" id="Seg_1773" s="T210">a</ta>
            <ta e="T212" id="Seg_1774" s="T211">me-nan</ta>
            <ta e="T213" id="Seg_1775" s="T212">pajä-ga</ta>
            <ta e="T214" id="Seg_1776" s="T213">qu-mmɨ-tda</ta>
            <ta e="T215" id="Seg_1777" s="T214">me</ta>
            <ta e="T216" id="Seg_1778" s="T215">täb-ɨ-m</ta>
            <ta e="T217" id="Seg_1779" s="T216">nagur-u-mdättə</ta>
            <ta e="T218" id="Seg_1780" s="T217">nädäl-ǯə</ta>
            <ta e="T219" id="Seg_1781" s="T218">malʼ-lʼe</ta>
            <ta e="T220" id="Seg_1782" s="T219">taːd-ə-r-a-utə</ta>
            <ta e="T221" id="Seg_1783" s="T220">i</ta>
            <ta e="T222" id="Seg_1784" s="T221">ilʼe-ptə-gu</ta>
            <ta e="T225" id="Seg_1785" s="T224">jass</ta>
            <ta e="T226" id="Seg_1786" s="T225">ilʼeː-dʼe-ǯi-n</ta>
            <ta e="T227" id="Seg_1787" s="T226">a</ta>
            <ta e="T228" id="Seg_1788" s="T227">kupez-ɨ-n</ta>
            <ta e="T229" id="Seg_1789" s="T228">nä-t</ta>
            <ta e="T230" id="Seg_1790" s="T229">qu-ku-s</ta>
            <ta e="T231" id="Seg_1791" s="T230">saldat</ta>
            <ta e="T232" id="Seg_1792" s="T231">pel-gattə</ta>
            <ta e="T233" id="Seg_1793" s="T232">mas-sɨ-t</ta>
            <ta e="T234" id="Seg_1794" s="T233">täp</ta>
            <ta e="T235" id="Seg_1795" s="T234">ilʼe-zi-s</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_1796" s="T1">elɨ-sɨ</ta>
            <ta e="T3" id="Seg_1797" s="T2">soldat</ta>
            <ta e="T4" id="Seg_1798" s="T3">täp</ta>
            <ta e="T5" id="Seg_1799" s="T4">sɨsari</ta>
            <ta e="T6" id="Seg_1800" s="T5">po-n</ta>
            <ta e="T7" id="Seg_1801" s="T6">armi-qɨn</ta>
            <ta e="T9" id="Seg_1802" s="T8">i</ta>
            <ta e="T10" id="Seg_1803" s="T9">eːde-ntə</ta>
            <ta e="T11" id="Seg_1804" s="T10">üdə-tɨn</ta>
            <ta e="T12" id="Seg_1805" s="T11">täp</ta>
            <ta e="T13" id="Seg_1806" s="T12">čaǯɨ-n</ta>
            <ta e="T14" id="Seg_1807" s="T13">eːde-qɨn</ta>
            <ta e="T15" id="Seg_1808" s="T14">täp</ta>
            <ta e="T16" id="Seg_1809" s="T15">eːde-ntə</ta>
            <ta e="T17" id="Seg_1810" s="T16">übɨ-n</ta>
            <ta e="T18" id="Seg_1811" s="T17">uːdə</ta>
            <ta e="T19" id="Seg_1812" s="T18">čaǯɨ-n</ta>
            <ta e="T20" id="Seg_1813" s="T19">qum-la</ta>
            <ta e="T21" id="Seg_1814" s="T20">apsod</ta>
            <ta e="T22" id="Seg_1815" s="T21">maǯə-tɨn</ta>
            <ta e="T23" id="Seg_1816" s="T22">i</ta>
            <ta e="T24" id="Seg_1817" s="T23">nʼüʒə-ču-tɨn</ta>
            <ta e="T25" id="Seg_1818" s="T24">täp</ta>
            <ta e="T26" id="Seg_1819" s="T25">okkɨr</ta>
            <ta e="T27" id="Seg_1820" s="T26">eːde-ntə</ta>
            <ta e="T28" id="Seg_1821" s="T27">tüː-le</ta>
            <ta e="T29" id="Seg_1822" s="T28">medə-n</ta>
            <ta e="T30" id="Seg_1823" s="T29">eːde</ta>
            <ta e="T31" id="Seg_1824" s="T30">iːbəgaj</ta>
            <ta e="T32" id="Seg_1825" s="T31">*natʼe-n</ta>
            <ta e="T33" id="Seg_1826" s="T32">elɨ-n</ta>
            <ta e="T34" id="Seg_1827" s="T33">kupes</ta>
            <ta e="T35" id="Seg_1828" s="T34">täp-nan</ta>
            <ta e="T36" id="Seg_1829" s="T35">okkɨr</ta>
            <ta e="T37" id="Seg_1830" s="T36">ne-tə</ta>
            <ta e="T38" id="Seg_1831" s="T37">quː-mbɨ</ta>
            <ta e="T39" id="Seg_1832" s="T38">qum-la</ta>
            <ta e="T40" id="Seg_1833" s="T39">wesʼ</ta>
            <ta e="T41" id="Seg_1834" s="T40">nɨŋgɨ-nɨ-tɨn</ta>
            <ta e="T42" id="Seg_1835" s="T41">kulubu-tɨn</ta>
            <ta e="T43" id="Seg_1836" s="T42">täp</ta>
            <ta e="T44" id="Seg_1837" s="T43">nil-zi-n</ta>
            <ta e="T45" id="Seg_1838" s="T44">i</ta>
            <ta e="T46" id="Seg_1839" s="T45">sogandʼe-n</ta>
            <ta e="T47" id="Seg_1840" s="T46">qaj-no</ta>
            <ta e="T48" id="Seg_1841" s="T47">taqqə-lɨ-mbɨ-lɨn</ta>
            <ta e="T49" id="Seg_1842" s="T48">a</ta>
            <ta e="T50" id="Seg_1843" s="T49">täp-la</ta>
            <ta e="T51" id="Seg_1844" s="T50">tʼärɨ-tɨn</ta>
            <ta e="T52" id="Seg_1845" s="T51">kupes-nan</ta>
            <ta e="T53" id="Seg_1846" s="T52">ne-tə</ta>
            <ta e="T54" id="Seg_1847" s="T53">quː-mbɨ-ntɨ</ta>
            <ta e="T55" id="Seg_1848" s="T54">täp-la</ta>
            <ta e="T56" id="Seg_1849" s="T55">tʼärɨ-tɨn</ta>
            <ta e="T57" id="Seg_1850" s="T56">täp</ta>
            <ta e="T58" id="Seg_1851" s="T57">tʼärɨ-sɨ</ta>
            <ta e="T59" id="Seg_1852" s="T58">kud</ta>
            <ta e="T60" id="Seg_1853" s="T59">ne-w</ta>
            <ta e="T61" id="Seg_1854" s="T60">elɨ-ptɨ-enǯɨ-t</ta>
            <ta e="T62" id="Seg_1855" s="T61">ne-w</ta>
            <ta e="T63" id="Seg_1856" s="T62">täp-nä</ta>
            <ta e="T64" id="Seg_1857" s="T63">me-enǯɨ-w</ta>
            <ta e="T65" id="Seg_1858" s="T64">tɨbetdə</ta>
            <ta e="T66" id="Seg_1859" s="T65">i</ta>
            <ta e="T69" id="Seg_1860" s="T68">me-ne-enǯɨ-ŋ</ta>
            <ta e="T70" id="Seg_1861" s="T69">meː-enǯɨ-w</ta>
            <ta e="T71" id="Seg_1862" s="T70">soldat</ta>
            <ta e="T72" id="Seg_1863" s="T71">tʼärɨ-n</ta>
            <ta e="T73" id="Seg_1864" s="T72">ser-ku-lä-nɨ</ta>
            <ta e="T74" id="Seg_1865" s="T73">i</ta>
            <ta e="T75" id="Seg_1866" s="T74">ser-nɨ-n</ta>
            <ta e="T76" id="Seg_1867" s="T75">täp-ɨ-m</ta>
            <ta e="T77" id="Seg_1868" s="T76">strečaj-nɨ-tɨn</ta>
            <ta e="T78" id="Seg_1869" s="T77">täp</ta>
            <ta e="T79" id="Seg_1870" s="T78">ser-nɨ-ŋ</ta>
            <ta e="T80" id="Seg_1871" s="T79">*mantɨ-mbɨ-t</ta>
            <ta e="T81" id="Seg_1872" s="T80">ne-lʼ-qum</ta>
            <ta e="T82" id="Seg_1873" s="T81">dʼepka</ta>
            <ta e="T83" id="Seg_1874" s="T82">sä-qɨn</ta>
            <ta e="T84" id="Seg_1875" s="T83">ippɨ</ta>
            <ta e="T85" id="Seg_1876" s="T84">soldat</ta>
            <ta e="T86" id="Seg_1877" s="T85">tʼärɨ-n</ta>
            <ta e="T87" id="Seg_1878" s="T86">wesʼ</ta>
            <ta e="T88" id="Seg_1879" s="T87">poːne</ta>
            <ta e="T89" id="Seg_1880" s="T88">qwan-naltə</ta>
            <ta e="T90" id="Seg_1881" s="T89">täp-la</ta>
            <ta e="T91" id="Seg_1882" s="T90">wesʼ</ta>
            <ta e="T92" id="Seg_1883" s="T91">poːne</ta>
            <ta e="T93" id="Seg_1884" s="T92">qwan-nɨ-tɨn</ta>
            <ta e="T94" id="Seg_1885" s="T93">täp</ta>
            <ta e="T95" id="Seg_1886" s="T94">akoška-la-m</ta>
            <ta e="T96" id="Seg_1887" s="T95">zanawesi-nɨ-t</ta>
            <ta e="T97" id="Seg_1888" s="T96">a</ta>
            <ta e="T98" id="Seg_1889" s="T97">okkɨr</ta>
            <ta e="T99" id="Seg_1890" s="T98">akoška</ta>
            <ta e="T100" id="Seg_1891" s="T99">nʼuː-mbɨ-mbɨ</ta>
            <ta e="T101" id="Seg_1892" s="T100">täp</ta>
            <ta e="T102" id="Seg_1893" s="T101">dʼepka-m</ta>
            <ta e="T103" id="Seg_1894" s="T102">sä-qɨntɨ</ta>
            <ta e="T104" id="Seg_1895" s="T103">saltšibo-ntə</ta>
            <ta e="T105" id="Seg_1896" s="T104">pen-nɨ-t</ta>
            <ta e="T106" id="Seg_1897" s="T105">i</ta>
            <ta e="T107" id="Seg_1898" s="T106">mat-le-näj</ta>
            <ta e="T108" id="Seg_1899" s="T107">übɨ-r-ɨ-t</ta>
            <ta e="T109" id="Seg_1900" s="T108">täp</ta>
            <ta e="T110" id="Seg_1901" s="T109">elɨ-dʼi-n</ta>
            <ta e="T111" id="Seg_1902" s="T110">okkɨr</ta>
            <ta e="T112" id="Seg_1903" s="T111">täbe-qum</ta>
            <ta e="T113" id="Seg_1904" s="T112">akoška-un</ta>
            <ta e="T114" id="Seg_1905" s="T113">*mantɨ-mbɨ</ta>
            <ta e="T115" id="Seg_1906" s="T114">soldat</ta>
            <ta e="T116" id="Seg_1907" s="T115">poːne</ta>
            <ta e="T117" id="Seg_1908" s="T116">čaǯɨ-n</ta>
            <ta e="T118" id="Seg_1909" s="T117">tʼärɨ-n</ta>
            <ta e="T119" id="Seg_1910" s="T118">ser-naltə</ta>
            <ta e="T120" id="Seg_1911" s="T119">maːt-ntə</ta>
            <ta e="T121" id="Seg_1912" s="T120">dʼepka</ta>
            <ta e="T122" id="Seg_1913" s="T121">elɨ-dʼi-n</ta>
            <ta e="T123" id="Seg_1914" s="T122">aze-tə</ta>
            <ta e="T124" id="Seg_1915" s="T123">tʼärɨ-n</ta>
            <ta e="T125" id="Seg_1916" s="T124">teper</ta>
            <ta e="T126" id="Seg_1917" s="T125">täp-ɨ-m</ta>
            <ta e="T127" id="Seg_1918" s="T126">iː-etɨ</ta>
            <ta e="T128" id="Seg_1919" s="T127">man</ta>
            <ta e="T129" id="Seg_1920" s="T128">täɣɨtdɨltə</ta>
            <ta e="T130" id="Seg_1921" s="T129">wesʼ</ta>
            <ta e="T131" id="Seg_1922" s="T130">meː-enǯɨ-w</ta>
            <ta e="T132" id="Seg_1923" s="T131">a</ta>
            <ta e="T133" id="Seg_1924" s="T132">soldat</ta>
            <ta e="T134" id="Seg_1925" s="T133">tʼärɨ-n</ta>
            <ta e="T135" id="Seg_1926" s="T134">mekka</ta>
            <ta e="T136" id="Seg_1927" s="T135">qaj-m-näj</ta>
            <ta e="T137" id="Seg_1928" s="T136">ne</ta>
            <ta e="T138" id="Seg_1929" s="T137">nadə</ta>
            <ta e="T139" id="Seg_1930" s="T138">nu</ta>
            <ta e="T140" id="Seg_1931" s="T139">man</ta>
            <ta e="T141" id="Seg_1932" s="T140">tekka</ta>
            <ta e="T142" id="Seg_1933" s="T141">qomdɛ</ta>
            <ta e="T143" id="Seg_1934" s="T142">meː-ntɨ-enǯɨ-ŋ</ta>
            <ta e="T144" id="Seg_1935" s="T143">täp</ta>
            <ta e="T145" id="Seg_1936" s="T144">qomdɛ-m</ta>
            <ta e="T146" id="Seg_1937" s="T145">meː-nɨ-t</ta>
            <ta e="T147" id="Seg_1938" s="T146">i</ta>
            <ta e="T148" id="Seg_1939" s="T147">soldat</ta>
            <ta e="T149" id="Seg_1940" s="T148">na</ta>
            <ta e="T150" id="Seg_1941" s="T149">qwan-ntɨ</ta>
            <ta e="T151" id="Seg_1942" s="T150">eːde-qɨn</ta>
            <ta e="T152" id="Seg_1943" s="T151">tüː-le</ta>
            <ta e="T153" id="Seg_1944" s="T152">medə-n</ta>
            <ta e="T154" id="Seg_1945" s="T153">aze-tə</ta>
            <ta e="T155" id="Seg_1946" s="T154">i</ta>
            <ta e="T156" id="Seg_1947" s="T155">awa-tə</ta>
            <ta e="T157" id="Seg_1948" s="T156">era-m-mbɨ-qij</ta>
            <ta e="T158" id="Seg_1949" s="T157">paja-m-mbɨ-qij</ta>
            <ta e="T159" id="Seg_1950" s="T158">tatawa</ta>
            <ta e="T160" id="Seg_1951" s="T159">aːndal-mbɨ-qij</ta>
            <ta e="T161" id="Seg_1952" s="T160">što</ta>
            <ta e="T162" id="Seg_1953" s="T161">iː-tə</ta>
            <ta e="T163" id="Seg_1954" s="T162">tüː-ntɨ</ta>
            <ta e="T164" id="Seg_1955" s="T163">iː-tə</ta>
            <ta e="T165" id="Seg_1956" s="T164">soːdʼiga</ta>
            <ta e="T166" id="Seg_1957" s="T165">maːt</ta>
            <ta e="T167" id="Seg_1958" s="T166">taw-ntɨ-t</ta>
            <ta e="T168" id="Seg_1959" s="T167">täp-ɨ-nan</ta>
            <ta e="T169" id="Seg_1960" s="T168">qomdɛ-tə</ta>
            <ta e="T170" id="Seg_1961" s="T169">koːci-ŋ</ta>
            <ta e="T171" id="Seg_1962" s="T170">eː-n</ta>
            <ta e="T172" id="Seg_1963" s="T171">i</ta>
            <ta e="T173" id="Seg_1964" s="T172">nedɨ-n</ta>
            <ta e="T174" id="Seg_1965" s="T173">i</ta>
            <ta e="T175" id="Seg_1966" s="T174">qoːwi-m-mbɨ-le</ta>
            <ta e="T176" id="Seg_1967" s="T175">warkɨ-le</ta>
            <ta e="T177" id="Seg_1968" s="T176">na</ta>
            <ta e="T178" id="Seg_1969" s="T177">übɨ-r-ɨ-ntɨ-tɨn</ta>
            <ta e="T179" id="Seg_1970" s="T178">teper</ta>
            <ta e="T180" id="Seg_1971" s="T179">soŋ</ta>
            <ta e="T181" id="Seg_1972" s="T180">warkɨ-tɨn</ta>
            <ta e="T182" id="Seg_1973" s="T181">soldat</ta>
            <ta e="T183" id="Seg_1974" s="T182">teper</ta>
            <ta e="T184" id="Seg_1975" s="T183">qwan-nɨ-n</ta>
            <ta e="T185" id="Seg_1976" s="T184">kütdə-m</ta>
            <ta e="T186" id="Seg_1977" s="T185">taw-gu</ta>
            <ta e="T187" id="Seg_1978" s="T186">čaǯɨ-n</ta>
            <ta e="T188" id="Seg_1979" s="T187">taɣ-ɨ-ŋ</ta>
            <ta e="T189" id="Seg_1980" s="T188">qum-la</ta>
            <ta e="T190" id="Seg_1981" s="T189">wesʼ</ta>
            <ta e="T191" id="Seg_1982" s="T190">pakos-qɨn</ta>
            <ta e="T192" id="Seg_1983" s="T191">eː-nɨ-tɨn</ta>
            <ta e="T193" id="Seg_1984" s="T192">iːbəgaj</ta>
            <ta e="T194" id="Seg_1985" s="T193">eːde-ntə</ta>
            <ta e="T195" id="Seg_1986" s="T194">tüː-le</ta>
            <ta e="T196" id="Seg_1987" s="T195">medə-n</ta>
            <ta e="T197" id="Seg_1988" s="T196">qutʼet</ta>
            <ta e="T198" id="Seg_1989" s="T197">täp</ta>
            <ta e="T199" id="Seg_1990" s="T198">eː-sɨ</ta>
            <ta e="T200" id="Seg_1991" s="T199">krajnaj</ta>
            <ta e="T201" id="Seg_1992" s="T200">qɨba</ta>
            <ta e="T202" id="Seg_1993" s="T201">maːt-qɨn</ta>
            <ta e="T203" id="Seg_1994" s="T202">tatawa</ta>
            <ta e="T204" id="Seg_1995" s="T203">qum-la</ta>
            <ta e="T205" id="Seg_1996" s="T204">taqqə-lɨ-mbɨ-tɨn</ta>
            <ta e="T206" id="Seg_1997" s="T205">täp</ta>
            <ta e="T207" id="Seg_1998" s="T206">sogandʼe-n</ta>
            <ta e="T208" id="Seg_1999" s="T207">te</ta>
            <ta e="T209" id="Seg_2000" s="T208">qaj-no</ta>
            <ta e="T210" id="Seg_2001" s="T209">taqqə-lɨ-mbɨ-lɨn</ta>
            <ta e="T211" id="Seg_2002" s="T210">a</ta>
            <ta e="T212" id="Seg_2003" s="T211">me-nan</ta>
            <ta e="T213" id="Seg_2004" s="T212">paja-ka</ta>
            <ta e="T214" id="Seg_2005" s="T213">qum-mbɨ-ntɨ</ta>
            <ta e="T215" id="Seg_2006" s="T214">me</ta>
            <ta e="T216" id="Seg_2007" s="T215">täp-ɨ-m</ta>
            <ta e="T217" id="Seg_2008" s="T216">nagur-ɨ-mtätte</ta>
            <ta e="T218" id="Seg_2009" s="T217">nädäl-ǯen</ta>
            <ta e="T219" id="Seg_2010" s="T218">mat-le</ta>
            <ta e="T220" id="Seg_2011" s="T219">tat-ɨ-r-nɨ-un</ta>
            <ta e="T221" id="Seg_2012" s="T220">i</ta>
            <ta e="T222" id="Seg_2013" s="T221">elɨ-ptɨ-gu</ta>
            <ta e="T225" id="Seg_2014" s="T224">asa</ta>
            <ta e="T226" id="Seg_2015" s="T225">elɨ-dʼi-ǯə-n</ta>
            <ta e="T227" id="Seg_2016" s="T226">a</ta>
            <ta e="T228" id="Seg_2017" s="T227">kupes-ɨ-n</ta>
            <ta e="T229" id="Seg_2018" s="T228">ne-tə</ta>
            <ta e="T230" id="Seg_2019" s="T229">quː-ku-sɨ</ta>
            <ta e="T231" id="Seg_2020" s="T230">soldat</ta>
            <ta e="T232" id="Seg_2021" s="T231">*pel-kattə</ta>
            <ta e="T233" id="Seg_2022" s="T232">mat-sɨ-t</ta>
            <ta e="T234" id="Seg_2023" s="T233">täp</ta>
            <ta e="T235" id="Seg_2024" s="T234">elɨ-zi-sɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_2025" s="T1">live-PST.[3SG.S]</ta>
            <ta e="T3" id="Seg_2026" s="T2">soldier.[NOM]</ta>
            <ta e="T4" id="Seg_2027" s="T3">(s)he.[NOM]</ta>
            <ta e="T5" id="Seg_2028" s="T4">twenty</ta>
            <ta e="T6" id="Seg_2029" s="T5">year-ADV.LOC</ta>
            <ta e="T7" id="Seg_2030" s="T6">army-LOC</ta>
            <ta e="T9" id="Seg_2031" s="T8">and</ta>
            <ta e="T10" id="Seg_2032" s="T9">village-ILL</ta>
            <ta e="T11" id="Seg_2033" s="T10">let.go-3PL</ta>
            <ta e="T12" id="Seg_2034" s="T11">(s)he.[NOM]</ta>
            <ta e="T13" id="Seg_2035" s="T12">go-3SG.S</ta>
            <ta e="T14" id="Seg_2036" s="T13">village-LOC</ta>
            <ta e="T15" id="Seg_2037" s="T14">(s)he.[NOM]</ta>
            <ta e="T16" id="Seg_2038" s="T15">village-ILL</ta>
            <ta e="T17" id="Seg_2039" s="T16">set.off-3SG.S</ta>
            <ta e="T18" id="Seg_2040" s="T17">on.foot</ta>
            <ta e="T19" id="Seg_2041" s="T18">go-3SG.S</ta>
            <ta e="T20" id="Seg_2042" s="T19">human.being-PL.[NOM]</ta>
            <ta e="T21" id="Seg_2043" s="T20">food.[NOM]</ta>
            <ta e="T22" id="Seg_2044" s="T21">cut-3PL</ta>
            <ta e="T23" id="Seg_2045" s="T22">and</ta>
            <ta e="T24" id="Seg_2046" s="T23">grass-VBLZ-3PL</ta>
            <ta e="T25" id="Seg_2047" s="T24">(s)he.[NOM]</ta>
            <ta e="T26" id="Seg_2048" s="T25">one</ta>
            <ta e="T27" id="Seg_2049" s="T26">village-ILL</ta>
            <ta e="T28" id="Seg_2050" s="T27">come-CVB</ta>
            <ta e="T29" id="Seg_2051" s="T28">achieve-3SG.S</ta>
            <ta e="T30" id="Seg_2052" s="T29">village.[NOM]</ta>
            <ta e="T31" id="Seg_2053" s="T30">big</ta>
            <ta e="T32" id="Seg_2054" s="T31">there-ADV.LOC</ta>
            <ta e="T33" id="Seg_2055" s="T32">live-3SG.S</ta>
            <ta e="T34" id="Seg_2056" s="T33">merchant.[NOM]</ta>
            <ta e="T35" id="Seg_2057" s="T34">(s)he-ADES</ta>
            <ta e="T36" id="Seg_2058" s="T35">one</ta>
            <ta e="T37" id="Seg_2059" s="T36">daughter.[NOM]-3SG</ta>
            <ta e="T38" id="Seg_2060" s="T37">die-PST.NAR.[3SG.S]</ta>
            <ta e="T39" id="Seg_2061" s="T38">human.being-PL.[NOM]</ta>
            <ta e="T40" id="Seg_2062" s="T39">all</ta>
            <ta e="T41" id="Seg_2063" s="T40">stand-CO-3PL</ta>
            <ta e="T42" id="Seg_2064" s="T41">speak-3PL</ta>
            <ta e="T43" id="Seg_2065" s="T42">(s)he.[NOM]</ta>
            <ta e="T44" id="Seg_2066" s="T43">stop-DRV-3SG.S</ta>
            <ta e="T45" id="Seg_2067" s="T44">and</ta>
            <ta e="T46" id="Seg_2068" s="T45">ask-3SG.S</ta>
            <ta e="T47" id="Seg_2069" s="T46">what-TRL</ta>
            <ta e="T48" id="Seg_2070" s="T47">gather-RES-PST.NAR-2PL</ta>
            <ta e="T49" id="Seg_2071" s="T48">and</ta>
            <ta e="T50" id="Seg_2072" s="T49">(s)he-PL.[NOM]</ta>
            <ta e="T51" id="Seg_2073" s="T50">say-3PL</ta>
            <ta e="T52" id="Seg_2074" s="T51">merchant-ADES</ta>
            <ta e="T53" id="Seg_2075" s="T52">daughter.[NOM]-3SG</ta>
            <ta e="T54" id="Seg_2076" s="T53">die-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T55" id="Seg_2077" s="T54">(s)he-PL.[NOM]</ta>
            <ta e="T56" id="Seg_2078" s="T55">say-3PL</ta>
            <ta e="T57" id="Seg_2079" s="T56">(s)he.[NOM]</ta>
            <ta e="T58" id="Seg_2080" s="T57">say-PST.[3SG.S]</ta>
            <ta e="T59" id="Seg_2081" s="T58">who.[NOM]</ta>
            <ta e="T60" id="Seg_2082" s="T59">daughter.[NOM]-1SG</ta>
            <ta e="T61" id="Seg_2083" s="T60">live-CAUS-FUT-3SG.O</ta>
            <ta e="T62" id="Seg_2084" s="T61">daughter.[NOM]-1SG</ta>
            <ta e="T63" id="Seg_2085" s="T62">(s)he-ALL</ta>
            <ta e="T64" id="Seg_2086" s="T63">give-FUT-1SG.O</ta>
            <ta e="T65" id="Seg_2087" s="T64">becoming.married</ta>
            <ta e="T66" id="Seg_2088" s="T65">and</ta>
            <ta e="T69" id="Seg_2089" s="T68">give-CONJ-FUT-1SG.S</ta>
            <ta e="T70" id="Seg_2090" s="T69">do-FUT-1SG.O</ta>
            <ta e="T71" id="Seg_2091" s="T70">soldier.[NOM]</ta>
            <ta e="T72" id="Seg_2092" s="T71">say-3SG.S</ta>
            <ta e="T73" id="Seg_2093" s="T72">come.in-HAB-OPT-1SG</ta>
            <ta e="T74" id="Seg_2094" s="T73">and</ta>
            <ta e="T75" id="Seg_2095" s="T74">come.in-CO-3SG.S</ta>
            <ta e="T76" id="Seg_2096" s="T75">(s)he-EP-ACC</ta>
            <ta e="T77" id="Seg_2097" s="T76">welcome-CO-3PL</ta>
            <ta e="T78" id="Seg_2098" s="T77">(s)he.[NOM]</ta>
            <ta e="T79" id="Seg_2099" s="T78">come.in-CO-1SG.S</ta>
            <ta e="T80" id="Seg_2100" s="T79">look-DUR-3SG.O</ta>
            <ta e="T81" id="Seg_2101" s="T80">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T82" id="Seg_2102" s="T81">girl.[NOM]</ta>
            <ta e="T83" id="Seg_2103" s="T82">coffin-LOC</ta>
            <ta e="T84" id="Seg_2104" s="T83">lie.[3SG.S]</ta>
            <ta e="T85" id="Seg_2105" s="T84">soldier.[NOM]</ta>
            <ta e="T86" id="Seg_2106" s="T85">say-3SG.S</ta>
            <ta e="T87" id="Seg_2107" s="T86">all</ta>
            <ta e="T88" id="Seg_2108" s="T87">outward(s)</ta>
            <ta e="T89" id="Seg_2109" s="T88">leave-IMP.2PL.S/O</ta>
            <ta e="T90" id="Seg_2110" s="T89">(s)he-PL.[NOM]</ta>
            <ta e="T91" id="Seg_2111" s="T90">all</ta>
            <ta e="T92" id="Seg_2112" s="T91">outward(s)</ta>
            <ta e="T93" id="Seg_2113" s="T92">leave-CO-3PL</ta>
            <ta e="T94" id="Seg_2114" s="T93">(s)he.[NOM]</ta>
            <ta e="T95" id="Seg_2115" s="T94">window-PL-ACC</ta>
            <ta e="T96" id="Seg_2116" s="T95">curtain-CO-3SG.O</ta>
            <ta e="T97" id="Seg_2117" s="T96">and</ta>
            <ta e="T98" id="Seg_2118" s="T97">one</ta>
            <ta e="T99" id="Seg_2119" s="T98">window.[NOM]</ta>
            <ta e="T100" id="Seg_2120" s="T99">open-RES-PST.NAR.[3SG.S]</ta>
            <ta e="T101" id="Seg_2121" s="T100">(s)he.[NOM]</ta>
            <ta e="T102" id="Seg_2122" s="T101">girl-ACC</ta>
            <ta e="T103" id="Seg_2123" s="T102">coffin-EL.3SG</ta>
            <ta e="T104" id="Seg_2124" s="T103">floor-ILL</ta>
            <ta e="T105" id="Seg_2125" s="T104">put-CO-3SG.O</ta>
            <ta e="T106" id="Seg_2126" s="T105">and</ta>
            <ta e="T107" id="Seg_2127" s="T106">fuck-CVB-EMPH</ta>
            <ta e="T108" id="Seg_2128" s="T107">begin-DRV-EP-3SG.O</ta>
            <ta e="T109" id="Seg_2129" s="T108">(s)he.[NOM]</ta>
            <ta e="T110" id="Seg_2130" s="T109">live-DRV-3SG.S</ta>
            <ta e="T111" id="Seg_2131" s="T110">one</ta>
            <ta e="T112" id="Seg_2132" s="T111">man-human.being.[NOM]</ta>
            <ta e="T113" id="Seg_2133" s="T112">window-PROL</ta>
            <ta e="T114" id="Seg_2134" s="T113">look-PST.NAR.[3SG.S]</ta>
            <ta e="T115" id="Seg_2135" s="T114">soldier.[NOM]</ta>
            <ta e="T116" id="Seg_2136" s="T115">outward(s)</ta>
            <ta e="T117" id="Seg_2137" s="T116">go-3SG.S</ta>
            <ta e="T118" id="Seg_2138" s="T117">say-3SG.S</ta>
            <ta e="T119" id="Seg_2139" s="T118">come.in-IMP.2PL.S/O</ta>
            <ta e="T120" id="Seg_2140" s="T119">house-ILL</ta>
            <ta e="T121" id="Seg_2141" s="T120">girl.[NOM]</ta>
            <ta e="T122" id="Seg_2142" s="T121">live-DRV-3SG.S</ta>
            <ta e="T123" id="Seg_2143" s="T122">father.[NOM]-3SG</ta>
            <ta e="T124" id="Seg_2144" s="T123">say-3SG.S</ta>
            <ta e="T125" id="Seg_2145" s="T124">now</ta>
            <ta e="T126" id="Seg_2146" s="T125">(s)he-EP-ACC</ta>
            <ta e="T127" id="Seg_2147" s="T126">take-IMP.2SG.O</ta>
            <ta e="T128" id="Seg_2148" s="T127">I.NOM</ta>
            <ta e="T129" id="Seg_2149" s="T128">you.PL.ALL</ta>
            <ta e="T130" id="Seg_2150" s="T129">all</ta>
            <ta e="T131" id="Seg_2151" s="T130">do-FUT-1SG.O</ta>
            <ta e="T132" id="Seg_2152" s="T131">and</ta>
            <ta e="T133" id="Seg_2153" s="T132">soldier.[NOM]</ta>
            <ta e="T134" id="Seg_2154" s="T133">say-3SG.S</ta>
            <ta e="T135" id="Seg_2155" s="T134">I.ALL</ta>
            <ta e="T136" id="Seg_2156" s="T135">what-ACC-EMPH</ta>
            <ta e="T137" id="Seg_2157" s="T136">NEG</ta>
            <ta e="T138" id="Seg_2158" s="T137">one.should</ta>
            <ta e="T139" id="Seg_2159" s="T138">now</ta>
            <ta e="T140" id="Seg_2160" s="T139">I.NOM</ta>
            <ta e="T141" id="Seg_2161" s="T140">you.ALL</ta>
            <ta e="T142" id="Seg_2162" s="T141">money.[NOM]</ta>
            <ta e="T143" id="Seg_2163" s="T142">do-INFER-FUT-1SG.S</ta>
            <ta e="T144" id="Seg_2164" s="T143">(s)he.[NOM]</ta>
            <ta e="T145" id="Seg_2165" s="T144">money-ACC</ta>
            <ta e="T146" id="Seg_2166" s="T145">do-CO-3SG.O</ta>
            <ta e="T147" id="Seg_2167" s="T146">and</ta>
            <ta e="T148" id="Seg_2168" s="T147">soldier.[NOM]</ta>
            <ta e="T149" id="Seg_2169" s="T148">here</ta>
            <ta e="T150" id="Seg_2170" s="T149">leave-INFER.[3SG.S]</ta>
            <ta e="T151" id="Seg_2171" s="T150">village-LOC</ta>
            <ta e="T152" id="Seg_2172" s="T151">come-CVB</ta>
            <ta e="T153" id="Seg_2173" s="T152">achieve-3SG.S</ta>
            <ta e="T154" id="Seg_2174" s="T153">father.[NOM]-3SG</ta>
            <ta e="T155" id="Seg_2175" s="T154">and</ta>
            <ta e="T156" id="Seg_2176" s="T155">mother.[NOM]-3SG</ta>
            <ta e="T157" id="Seg_2177" s="T156">old.man-TRL-PST.NAR-3DU.S</ta>
            <ta e="T158" id="Seg_2178" s="T157">old.woman-TRL-PST.NAR-3DU.S</ta>
            <ta e="T159" id="Seg_2179" s="T158">to.such.extent</ta>
            <ta e="T160" id="Seg_2180" s="T159">become.glad-PST.NAR-3DU.S</ta>
            <ta e="T161" id="Seg_2181" s="T160">that</ta>
            <ta e="T162" id="Seg_2182" s="T161">son.[NOM]-3SG</ta>
            <ta e="T163" id="Seg_2183" s="T162">come-INFER.[3SG.S]</ta>
            <ta e="T164" id="Seg_2184" s="T163">son.[NOM]-3SG</ta>
            <ta e="T165" id="Seg_2185" s="T164">good</ta>
            <ta e="T166" id="Seg_2186" s="T165">house.[NOM]</ta>
            <ta e="T167" id="Seg_2187" s="T166">buy-INFER-3SG.O</ta>
            <ta e="T168" id="Seg_2188" s="T167">(s)he-EP-ADES</ta>
            <ta e="T169" id="Seg_2189" s="T168">money.[NOM]-3SG</ta>
            <ta e="T170" id="Seg_2190" s="T169">much-ADVZ</ta>
            <ta e="T171" id="Seg_2191" s="T170">be-3SG.S</ta>
            <ta e="T172" id="Seg_2192" s="T171">and</ta>
            <ta e="T173" id="Seg_2193" s="T172">get.married-3SG.S</ta>
            <ta e="T174" id="Seg_2194" s="T173">and</ta>
            <ta e="T175" id="Seg_2195" s="T174">rich-TRL-DUR-CVB</ta>
            <ta e="T176" id="Seg_2196" s="T175">live-CVB</ta>
            <ta e="T177" id="Seg_2197" s="T176">here</ta>
            <ta e="T178" id="Seg_2198" s="T177">begin-DRV-EP-INFER-3PL</ta>
            <ta e="T179" id="Seg_2199" s="T178">now</ta>
            <ta e="T180" id="Seg_2200" s="T179">good</ta>
            <ta e="T181" id="Seg_2201" s="T180">live-3PL</ta>
            <ta e="T182" id="Seg_2202" s="T181">soldier.[NOM]</ta>
            <ta e="T183" id="Seg_2203" s="T182">now</ta>
            <ta e="T184" id="Seg_2204" s="T183">leave-CO-3SG.S</ta>
            <ta e="T185" id="Seg_2205" s="T184">horse-ACC</ta>
            <ta e="T186" id="Seg_2206" s="T185">buy-INF</ta>
            <ta e="T187" id="Seg_2207" s="T186">go-3SG.S</ta>
            <ta e="T188" id="Seg_2208" s="T187">summer-EP-ADVZ</ta>
            <ta e="T189" id="Seg_2209" s="T188">human.being-PL.[NOM]</ta>
            <ta e="T190" id="Seg_2210" s="T189">all</ta>
            <ta e="T191" id="Seg_2211" s="T190">haymaking-LOC</ta>
            <ta e="T192" id="Seg_2212" s="T191">be-CO-3PL</ta>
            <ta e="T193" id="Seg_2213" s="T192">big</ta>
            <ta e="T194" id="Seg_2214" s="T193">village-ILL</ta>
            <ta e="T195" id="Seg_2215" s="T194">come-CVB</ta>
            <ta e="T196" id="Seg_2216" s="T195">achieve-3SG.S</ta>
            <ta e="T197" id="Seg_2217" s="T196">where</ta>
            <ta e="T198" id="Seg_2218" s="T197">(s)he.[NOM]</ta>
            <ta e="T199" id="Seg_2219" s="T198">be-PST.[3SG.S]</ta>
            <ta e="T200" id="Seg_2220" s="T199">last</ta>
            <ta e="T201" id="Seg_2221" s="T200">small</ta>
            <ta e="T202" id="Seg_2222" s="T201">house-LOC</ta>
            <ta e="T203" id="Seg_2223" s="T202">to.such.extent</ta>
            <ta e="T204" id="Seg_2224" s="T203">human.being-PL.[NOM]</ta>
            <ta e="T205" id="Seg_2225" s="T204">gather-RES-PST.NAR-3PL</ta>
            <ta e="T206" id="Seg_2226" s="T205">(s)he.[NOM]</ta>
            <ta e="T207" id="Seg_2227" s="T206">ask-3SG.S</ta>
            <ta e="T208" id="Seg_2228" s="T207">you.PL.NOM</ta>
            <ta e="T209" id="Seg_2229" s="T208">what-TRL</ta>
            <ta e="T210" id="Seg_2230" s="T209">gather-RES-PST.NAR-2PL</ta>
            <ta e="T211" id="Seg_2231" s="T210">and</ta>
            <ta e="T212" id="Seg_2232" s="T211">we-ADES</ta>
            <ta e="T213" id="Seg_2233" s="T212">old.woman-DIM.[NOM]</ta>
            <ta e="T214" id="Seg_2234" s="T213">human.being-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T215" id="Seg_2235" s="T214">we.[NOM]</ta>
            <ta e="T216" id="Seg_2236" s="T215">(s)he-EP-ACC</ta>
            <ta e="T217" id="Seg_2237" s="T216">three-EP-ORD</ta>
            <ta e="T218" id="Seg_2238" s="T217">week-%%</ta>
            <ta e="T219" id="Seg_2239" s="T218">fuck-CVB</ta>
            <ta e="T220" id="Seg_2240" s="T219">bring-EP-FRQ-CO-1PL</ta>
            <ta e="T221" id="Seg_2241" s="T220">and</ta>
            <ta e="T222" id="Seg_2242" s="T221">live-CAUS-INF</ta>
            <ta e="T225" id="Seg_2243" s="T224">NEG</ta>
            <ta e="T226" id="Seg_2244" s="T225">live-DRV-DRV-3SG.S</ta>
            <ta e="T227" id="Seg_2245" s="T226">and</ta>
            <ta e="T228" id="Seg_2246" s="T227">merchant-EP-GEN</ta>
            <ta e="T229" id="Seg_2247" s="T228">daughter.[NOM]-3SG</ta>
            <ta e="T230" id="Seg_2248" s="T229">die-HAB-PST.[3SG.S]</ta>
            <ta e="T231" id="Seg_2249" s="T230">soldier.[NOM]</ta>
            <ta e="T232" id="Seg_2250" s="T231">friend-CAR</ta>
            <ta e="T233" id="Seg_2251" s="T232">fuck-PST-3SG.O</ta>
            <ta e="T234" id="Seg_2252" s="T233">(s)he.[NOM]</ta>
            <ta e="T235" id="Seg_2253" s="T234">live-DRV-PST.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_2254" s="T1">жить-PST.[3SG.S]</ta>
            <ta e="T3" id="Seg_2255" s="T2">солдат.[NOM]</ta>
            <ta e="T4" id="Seg_2256" s="T3">он(а).[NOM]</ta>
            <ta e="T5" id="Seg_2257" s="T4">двадцать</ta>
            <ta e="T6" id="Seg_2258" s="T5">год-ADV.LOC</ta>
            <ta e="T7" id="Seg_2259" s="T6">армия-LOC</ta>
            <ta e="T9" id="Seg_2260" s="T8">и</ta>
            <ta e="T10" id="Seg_2261" s="T9">деревня-ILL</ta>
            <ta e="T11" id="Seg_2262" s="T10">пустить-3PL</ta>
            <ta e="T12" id="Seg_2263" s="T11">он(а).[NOM]</ta>
            <ta e="T13" id="Seg_2264" s="T12">ходить-3SG.S</ta>
            <ta e="T14" id="Seg_2265" s="T13">деревня-LOC</ta>
            <ta e="T15" id="Seg_2266" s="T14">он(а).[NOM]</ta>
            <ta e="T16" id="Seg_2267" s="T15">деревня-ILL</ta>
            <ta e="T17" id="Seg_2268" s="T16">отправиться-3SG.S</ta>
            <ta e="T18" id="Seg_2269" s="T17">пешком</ta>
            <ta e="T19" id="Seg_2270" s="T18">ходить-3SG.S</ta>
            <ta e="T20" id="Seg_2271" s="T19">человек-PL.[NOM]</ta>
            <ta e="T21" id="Seg_2272" s="T20">еда.[NOM]</ta>
            <ta e="T22" id="Seg_2273" s="T21">отрезать-3PL</ta>
            <ta e="T23" id="Seg_2274" s="T22">и</ta>
            <ta e="T24" id="Seg_2275" s="T23">трава-VBLZ-3PL</ta>
            <ta e="T25" id="Seg_2276" s="T24">он(а).[NOM]</ta>
            <ta e="T26" id="Seg_2277" s="T25">один</ta>
            <ta e="T27" id="Seg_2278" s="T26">деревня-ILL</ta>
            <ta e="T28" id="Seg_2279" s="T27">прийти-CVB</ta>
            <ta e="T29" id="Seg_2280" s="T28">достичь-3SG.S</ta>
            <ta e="T30" id="Seg_2281" s="T29">деревня.[NOM]</ta>
            <ta e="T31" id="Seg_2282" s="T30">большой</ta>
            <ta e="T32" id="Seg_2283" s="T31">туда-ADV.LOC</ta>
            <ta e="T33" id="Seg_2284" s="T32">жить-3SG.S</ta>
            <ta e="T34" id="Seg_2285" s="T33">купец.[NOM]</ta>
            <ta e="T35" id="Seg_2286" s="T34">он(а)-ADES</ta>
            <ta e="T36" id="Seg_2287" s="T35">один</ta>
            <ta e="T37" id="Seg_2288" s="T36">дочь.[NOM]-3SG</ta>
            <ta e="T38" id="Seg_2289" s="T37">умереть-PST.NAR.[3SG.S]</ta>
            <ta e="T39" id="Seg_2290" s="T38">человек-PL.[NOM]</ta>
            <ta e="T40" id="Seg_2291" s="T39">весь</ta>
            <ta e="T41" id="Seg_2292" s="T40">стоять-CO-3PL</ta>
            <ta e="T42" id="Seg_2293" s="T41">говорить-3PL</ta>
            <ta e="T43" id="Seg_2294" s="T42">он(а).[NOM]</ta>
            <ta e="T44" id="Seg_2295" s="T43">остановиться-DRV-3SG.S</ta>
            <ta e="T45" id="Seg_2296" s="T44">и</ta>
            <ta e="T46" id="Seg_2297" s="T45">спросить-3SG.S</ta>
            <ta e="T47" id="Seg_2298" s="T46">что-TRL</ta>
            <ta e="T48" id="Seg_2299" s="T47">собрать-RES-PST.NAR-2PL</ta>
            <ta e="T49" id="Seg_2300" s="T48">а</ta>
            <ta e="T50" id="Seg_2301" s="T49">он(а)-PL.[NOM]</ta>
            <ta e="T51" id="Seg_2302" s="T50">сказать-3PL</ta>
            <ta e="T52" id="Seg_2303" s="T51">купец-ADES</ta>
            <ta e="T53" id="Seg_2304" s="T52">дочь.[NOM]-3SG</ta>
            <ta e="T54" id="Seg_2305" s="T53">умереть-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T55" id="Seg_2306" s="T54">он(а)-PL.[NOM]</ta>
            <ta e="T56" id="Seg_2307" s="T55">сказать-3PL</ta>
            <ta e="T57" id="Seg_2308" s="T56">он(а).[NOM]</ta>
            <ta e="T58" id="Seg_2309" s="T57">сказать-PST.[3SG.S]</ta>
            <ta e="T59" id="Seg_2310" s="T58">кто.[NOM]</ta>
            <ta e="T60" id="Seg_2311" s="T59">дочь.[NOM]-1SG</ta>
            <ta e="T61" id="Seg_2312" s="T60">жить-CAUS-FUT-3SG.O</ta>
            <ta e="T62" id="Seg_2313" s="T61">дочь.[NOM]-1SG</ta>
            <ta e="T63" id="Seg_2314" s="T62">он(а)-ALL</ta>
            <ta e="T64" id="Seg_2315" s="T63">дать-FUT-1SG.O</ta>
            <ta e="T65" id="Seg_2316" s="T64">замуж</ta>
            <ta e="T66" id="Seg_2317" s="T65">и</ta>
            <ta e="T69" id="Seg_2318" s="T68">дать-CONJ-FUT-1SG.S</ta>
            <ta e="T70" id="Seg_2319" s="T69">сделать-FUT-1SG.O</ta>
            <ta e="T71" id="Seg_2320" s="T70">солдат.[NOM]</ta>
            <ta e="T72" id="Seg_2321" s="T71">сказать-3SG.S</ta>
            <ta e="T73" id="Seg_2322" s="T72">зайти-HAB-OPT-1SG</ta>
            <ta e="T74" id="Seg_2323" s="T73">и</ta>
            <ta e="T75" id="Seg_2324" s="T74">зайти-CO-3SG.S</ta>
            <ta e="T76" id="Seg_2325" s="T75">он(а)-EP-ACC</ta>
            <ta e="T77" id="Seg_2326" s="T76">встречать-CO-3PL</ta>
            <ta e="T78" id="Seg_2327" s="T77">он(а).[NOM]</ta>
            <ta e="T79" id="Seg_2328" s="T78">зайти-CO-1SG.S</ta>
            <ta e="T80" id="Seg_2329" s="T79">посмотреть-DUR-3SG.O</ta>
            <ta e="T81" id="Seg_2330" s="T80">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T82" id="Seg_2331" s="T81">девка.[NOM]</ta>
            <ta e="T83" id="Seg_2332" s="T82">гроб-LOC</ta>
            <ta e="T84" id="Seg_2333" s="T83">лежать.[3SG.S]</ta>
            <ta e="T85" id="Seg_2334" s="T84">солдат.[NOM]</ta>
            <ta e="T86" id="Seg_2335" s="T85">сказать-3SG.S</ta>
            <ta e="T87" id="Seg_2336" s="T86">все</ta>
            <ta e="T88" id="Seg_2337" s="T87">наружу</ta>
            <ta e="T89" id="Seg_2338" s="T88">отправиться-IMP.2PL.S/O</ta>
            <ta e="T90" id="Seg_2339" s="T89">он(а)-PL.[NOM]</ta>
            <ta e="T91" id="Seg_2340" s="T90">все</ta>
            <ta e="T92" id="Seg_2341" s="T91">наружу</ta>
            <ta e="T93" id="Seg_2342" s="T92">отправиться-CO-3PL</ta>
            <ta e="T94" id="Seg_2343" s="T93">он(а).[NOM]</ta>
            <ta e="T95" id="Seg_2344" s="T94">окно-PL-ACC</ta>
            <ta e="T96" id="Seg_2345" s="T95">занавесить-CO-3SG.O</ta>
            <ta e="T97" id="Seg_2346" s="T96">а</ta>
            <ta e="T98" id="Seg_2347" s="T97">один</ta>
            <ta e="T99" id="Seg_2348" s="T98">окно.[NOM]</ta>
            <ta e="T100" id="Seg_2349" s="T99">открыть-RES-PST.NAR.[3SG.S]</ta>
            <ta e="T101" id="Seg_2350" s="T100">он(а).[NOM]</ta>
            <ta e="T102" id="Seg_2351" s="T101">девка-ACC</ta>
            <ta e="T103" id="Seg_2352" s="T102">гроб-EL.3SG</ta>
            <ta e="T104" id="Seg_2353" s="T103">пол-ILL</ta>
            <ta e="T105" id="Seg_2354" s="T104">положить-CO-3SG.O</ta>
            <ta e="T106" id="Seg_2355" s="T105">и</ta>
            <ta e="T107" id="Seg_2356" s="T106">соединяться.с.женщиной-CVB-EMPH</ta>
            <ta e="T108" id="Seg_2357" s="T107">начать-DRV-EP-3SG.O</ta>
            <ta e="T109" id="Seg_2358" s="T108">он(а).[NOM]</ta>
            <ta e="T110" id="Seg_2359" s="T109">жить-DRV-3SG.S</ta>
            <ta e="T111" id="Seg_2360" s="T110">один</ta>
            <ta e="T112" id="Seg_2361" s="T111">мужчина-человек.[NOM]</ta>
            <ta e="T113" id="Seg_2362" s="T112">окно-PROL</ta>
            <ta e="T114" id="Seg_2363" s="T113">посмотреть-PST.NAR.[3SG.S]</ta>
            <ta e="T115" id="Seg_2364" s="T114">солдат.[NOM]</ta>
            <ta e="T116" id="Seg_2365" s="T115">наружу</ta>
            <ta e="T117" id="Seg_2366" s="T116">ходить-3SG.S</ta>
            <ta e="T118" id="Seg_2367" s="T117">сказать-3SG.S</ta>
            <ta e="T119" id="Seg_2368" s="T118">зайти-IMP.2PL.S/O</ta>
            <ta e="T120" id="Seg_2369" s="T119">дом-ILL</ta>
            <ta e="T121" id="Seg_2370" s="T120">девка.[NOM]</ta>
            <ta e="T122" id="Seg_2371" s="T121">жить-DRV-3SG.S</ta>
            <ta e="T123" id="Seg_2372" s="T122">отец.[NOM]-3SG</ta>
            <ta e="T124" id="Seg_2373" s="T123">сказать-3SG.S</ta>
            <ta e="T125" id="Seg_2374" s="T124">теперь</ta>
            <ta e="T126" id="Seg_2375" s="T125">он(а)-EP-ACC</ta>
            <ta e="T127" id="Seg_2376" s="T126">взять-IMP.2SG.O</ta>
            <ta e="T128" id="Seg_2377" s="T127">я.NOM</ta>
            <ta e="T129" id="Seg_2378" s="T128">вы.PL.ALL</ta>
            <ta e="T130" id="Seg_2379" s="T129">весь</ta>
            <ta e="T131" id="Seg_2380" s="T130">сделать-FUT-1SG.O</ta>
            <ta e="T132" id="Seg_2381" s="T131">а</ta>
            <ta e="T133" id="Seg_2382" s="T132">солдат.[NOM]</ta>
            <ta e="T134" id="Seg_2383" s="T133">сказать-3SG.S</ta>
            <ta e="T135" id="Seg_2384" s="T134">я.ALL</ta>
            <ta e="T136" id="Seg_2385" s="T135">что-ACC-EMPH</ta>
            <ta e="T137" id="Seg_2386" s="T136">NEG</ta>
            <ta e="T138" id="Seg_2387" s="T137">надо</ta>
            <ta e="T139" id="Seg_2388" s="T138">ну</ta>
            <ta e="T140" id="Seg_2389" s="T139">я.NOM</ta>
            <ta e="T141" id="Seg_2390" s="T140">ты.ALL</ta>
            <ta e="T142" id="Seg_2391" s="T141">деньги.[NOM]</ta>
            <ta e="T143" id="Seg_2392" s="T142">сделать-INFER-FUT-1SG.S</ta>
            <ta e="T144" id="Seg_2393" s="T143">он(а).[NOM]</ta>
            <ta e="T145" id="Seg_2394" s="T144">деньги-ACC</ta>
            <ta e="T146" id="Seg_2395" s="T145">сделать-CO-3SG.O</ta>
            <ta e="T147" id="Seg_2396" s="T146">и</ta>
            <ta e="T148" id="Seg_2397" s="T147">солдат.[NOM]</ta>
            <ta e="T149" id="Seg_2398" s="T148">ну</ta>
            <ta e="T150" id="Seg_2399" s="T149">отправиться-INFER.[3SG.S]</ta>
            <ta e="T151" id="Seg_2400" s="T150">деревня-LOC</ta>
            <ta e="T152" id="Seg_2401" s="T151">прийти-CVB</ta>
            <ta e="T153" id="Seg_2402" s="T152">достичь-3SG.S</ta>
            <ta e="T154" id="Seg_2403" s="T153">отец.[NOM]-3SG</ta>
            <ta e="T155" id="Seg_2404" s="T154">и</ta>
            <ta e="T156" id="Seg_2405" s="T155">мать.[NOM]-3SG</ta>
            <ta e="T157" id="Seg_2406" s="T156">старик-TRL-PST.NAR-3DU.S</ta>
            <ta e="T158" id="Seg_2407" s="T157">старуха-TRL-PST.NAR-3DU.S</ta>
            <ta e="T159" id="Seg_2408" s="T158">до.того</ta>
            <ta e="T160" id="Seg_2409" s="T159">обрадоваться-PST.NAR-3DU.S</ta>
            <ta e="T161" id="Seg_2410" s="T160">что</ta>
            <ta e="T162" id="Seg_2411" s="T161">сын.[NOM]-3SG</ta>
            <ta e="T163" id="Seg_2412" s="T162">прийти-INFER.[3SG.S]</ta>
            <ta e="T164" id="Seg_2413" s="T163">сын.[NOM]-3SG</ta>
            <ta e="T165" id="Seg_2414" s="T164">хороший</ta>
            <ta e="T166" id="Seg_2415" s="T165">дом.[NOM]</ta>
            <ta e="T167" id="Seg_2416" s="T166">купить-INFER-3SG.O</ta>
            <ta e="T168" id="Seg_2417" s="T167">он(а)-EP-ADES</ta>
            <ta e="T169" id="Seg_2418" s="T168">деньги.[NOM]-3SG</ta>
            <ta e="T170" id="Seg_2419" s="T169">много-ADVZ</ta>
            <ta e="T171" id="Seg_2420" s="T170">быть-3SG.S</ta>
            <ta e="T172" id="Seg_2421" s="T171">и</ta>
            <ta e="T173" id="Seg_2422" s="T172">жениться-3SG.S</ta>
            <ta e="T174" id="Seg_2423" s="T173">и</ta>
            <ta e="T175" id="Seg_2424" s="T174">богатый-TRL-DUR-CVB</ta>
            <ta e="T176" id="Seg_2425" s="T175">жить-CVB</ta>
            <ta e="T177" id="Seg_2426" s="T176">ну</ta>
            <ta e="T178" id="Seg_2427" s="T177">начать-DRV-EP-INFER-3PL</ta>
            <ta e="T179" id="Seg_2428" s="T178">теперь</ta>
            <ta e="T180" id="Seg_2429" s="T179">хорошо</ta>
            <ta e="T181" id="Seg_2430" s="T180">жить-3PL</ta>
            <ta e="T182" id="Seg_2431" s="T181">солдат.[NOM]</ta>
            <ta e="T183" id="Seg_2432" s="T182">теперь</ta>
            <ta e="T184" id="Seg_2433" s="T183">отправиться-CO-3SG.S</ta>
            <ta e="T185" id="Seg_2434" s="T184">лошадь-ACC</ta>
            <ta e="T186" id="Seg_2435" s="T185">купить-INF</ta>
            <ta e="T187" id="Seg_2436" s="T186">ходить-3SG.S</ta>
            <ta e="T188" id="Seg_2437" s="T187">лето-EP-ADVZ</ta>
            <ta e="T189" id="Seg_2438" s="T188">человек-PL.[NOM]</ta>
            <ta e="T190" id="Seg_2439" s="T189">все</ta>
            <ta e="T191" id="Seg_2440" s="T190">покос-LOC</ta>
            <ta e="T192" id="Seg_2441" s="T191">быть-CO-3PL</ta>
            <ta e="T193" id="Seg_2442" s="T192">большой</ta>
            <ta e="T194" id="Seg_2443" s="T193">деревня-ILL</ta>
            <ta e="T195" id="Seg_2444" s="T194">прийти-CVB</ta>
            <ta e="T196" id="Seg_2445" s="T195">достичь-3SG.S</ta>
            <ta e="T197" id="Seg_2446" s="T196">где</ta>
            <ta e="T198" id="Seg_2447" s="T197">он(а).[NOM]</ta>
            <ta e="T199" id="Seg_2448" s="T198">быть-PST.[3SG.S]</ta>
            <ta e="T200" id="Seg_2449" s="T199">крайний</ta>
            <ta e="T201" id="Seg_2450" s="T200">маленький</ta>
            <ta e="T202" id="Seg_2451" s="T201">дом-LOC</ta>
            <ta e="T203" id="Seg_2452" s="T202">до.того</ta>
            <ta e="T204" id="Seg_2453" s="T203">человек-PL.[NOM]</ta>
            <ta e="T205" id="Seg_2454" s="T204">собрать-RES-PST.NAR-3PL</ta>
            <ta e="T206" id="Seg_2455" s="T205">он(а).[NOM]</ta>
            <ta e="T207" id="Seg_2456" s="T206">спросить-3SG.S</ta>
            <ta e="T208" id="Seg_2457" s="T207">вы.PL.NOM</ta>
            <ta e="T209" id="Seg_2458" s="T208">что-TRL</ta>
            <ta e="T210" id="Seg_2459" s="T209">собрать-RES-PST.NAR-2PL</ta>
            <ta e="T211" id="Seg_2460" s="T210">а</ta>
            <ta e="T212" id="Seg_2461" s="T211">мы-ADES</ta>
            <ta e="T213" id="Seg_2462" s="T212">старуха-DIM.[NOM]</ta>
            <ta e="T214" id="Seg_2463" s="T213">человек-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T215" id="Seg_2464" s="T214">мы.[NOM]</ta>
            <ta e="T216" id="Seg_2465" s="T215">он(а)-EP-ACC</ta>
            <ta e="T217" id="Seg_2466" s="T216">три-EP-ORD</ta>
            <ta e="T218" id="Seg_2467" s="T217">неделя-%%</ta>
            <ta e="T219" id="Seg_2468" s="T218">соединяться.с.женщиной-CVB</ta>
            <ta e="T220" id="Seg_2469" s="T219">принести-EP-FRQ-CO-1PL</ta>
            <ta e="T221" id="Seg_2470" s="T220">и</ta>
            <ta e="T222" id="Seg_2471" s="T221">жить-CAUS-INF</ta>
            <ta e="T225" id="Seg_2472" s="T224">NEG</ta>
            <ta e="T226" id="Seg_2473" s="T225">жить-DRV-DRV-3SG.S</ta>
            <ta e="T227" id="Seg_2474" s="T226">а</ta>
            <ta e="T228" id="Seg_2475" s="T227">купец-EP-GEN</ta>
            <ta e="T229" id="Seg_2476" s="T228">дочь.[NOM]-3SG</ta>
            <ta e="T230" id="Seg_2477" s="T229">умереть-HAB-PST.[3SG.S]</ta>
            <ta e="T231" id="Seg_2478" s="T230">солдат.[NOM]</ta>
            <ta e="T232" id="Seg_2479" s="T231">друг-CAR</ta>
            <ta e="T233" id="Seg_2480" s="T232">соединяться.с.женщиной-PST-3SG.O</ta>
            <ta e="T234" id="Seg_2481" s="T233">он(а).[NOM]</ta>
            <ta e="T235" id="Seg_2482" s="T234">жить-DRV-PST.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_2483" s="T1">v-v:tense.[v:pn]</ta>
            <ta e="T3" id="Seg_2484" s="T2">n.[n:case]</ta>
            <ta e="T4" id="Seg_2485" s="T3">pers.[n:case]</ta>
            <ta e="T5" id="Seg_2486" s="T4">num</ta>
            <ta e="T6" id="Seg_2487" s="T5">n-adv:case</ta>
            <ta e="T7" id="Seg_2488" s="T6">n-n:case</ta>
            <ta e="T9" id="Seg_2489" s="T8">conj</ta>
            <ta e="T10" id="Seg_2490" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_2491" s="T10">v-v:pn</ta>
            <ta e="T12" id="Seg_2492" s="T11">pers.[n:case]</ta>
            <ta e="T13" id="Seg_2493" s="T12">v-v:pn</ta>
            <ta e="T14" id="Seg_2494" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_2495" s="T14">pers.[n:case]</ta>
            <ta e="T16" id="Seg_2496" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_2497" s="T16">v-v:pn</ta>
            <ta e="T18" id="Seg_2498" s="T17">adv</ta>
            <ta e="T19" id="Seg_2499" s="T18">v-v:pn</ta>
            <ta e="T20" id="Seg_2500" s="T19">n-n:num.[n:case]</ta>
            <ta e="T21" id="Seg_2501" s="T20">n.[n:case]</ta>
            <ta e="T22" id="Seg_2502" s="T21">v-v:pn</ta>
            <ta e="T23" id="Seg_2503" s="T22">conj</ta>
            <ta e="T24" id="Seg_2504" s="T23">n-n&gt;v-v:pn</ta>
            <ta e="T25" id="Seg_2505" s="T24">pers.[n:case]</ta>
            <ta e="T26" id="Seg_2506" s="T25">num</ta>
            <ta e="T27" id="Seg_2507" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_2508" s="T27">v-v&gt;adv</ta>
            <ta e="T29" id="Seg_2509" s="T28">v-v:pn</ta>
            <ta e="T30" id="Seg_2510" s="T29">n.[n:case]</ta>
            <ta e="T31" id="Seg_2511" s="T30">adj</ta>
            <ta e="T32" id="Seg_2512" s="T31">adv-adv:case</ta>
            <ta e="T33" id="Seg_2513" s="T32">v-v:pn</ta>
            <ta e="T34" id="Seg_2514" s="T33">n.[n:case]</ta>
            <ta e="T35" id="Seg_2515" s="T34">pers-n:case</ta>
            <ta e="T36" id="Seg_2516" s="T35">num</ta>
            <ta e="T37" id="Seg_2517" s="T36">n.[n:case]-n:poss</ta>
            <ta e="T38" id="Seg_2518" s="T37">v-v:tense.[v:pn]</ta>
            <ta e="T39" id="Seg_2519" s="T38">n-n:num.[n:case]</ta>
            <ta e="T40" id="Seg_2520" s="T39">quant</ta>
            <ta e="T41" id="Seg_2521" s="T40">v-v:ins-v:pn</ta>
            <ta e="T42" id="Seg_2522" s="T41">v-v:pn</ta>
            <ta e="T43" id="Seg_2523" s="T42">pers.[n:case]</ta>
            <ta e="T44" id="Seg_2524" s="T43">v-v&gt;v-v:pn</ta>
            <ta e="T45" id="Seg_2525" s="T44">conj</ta>
            <ta e="T46" id="Seg_2526" s="T45">v-v:pn</ta>
            <ta e="T47" id="Seg_2527" s="T46">interrog-n:case</ta>
            <ta e="T48" id="Seg_2528" s="T47">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T49" id="Seg_2529" s="T48">conj</ta>
            <ta e="T50" id="Seg_2530" s="T49">pers-n:num.[n:case]</ta>
            <ta e="T51" id="Seg_2531" s="T50">v-v:pn</ta>
            <ta e="T52" id="Seg_2532" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_2533" s="T52">n.[n:case]-n:poss</ta>
            <ta e="T54" id="Seg_2534" s="T53">v-v:tense-v:mood.[v:pn]</ta>
            <ta e="T55" id="Seg_2535" s="T54">pers-n:num.[n:case]</ta>
            <ta e="T56" id="Seg_2536" s="T55">v-v:pn</ta>
            <ta e="T57" id="Seg_2537" s="T56">pers.[n:case]</ta>
            <ta e="T58" id="Seg_2538" s="T57">v-v:tense.[v:pn]</ta>
            <ta e="T59" id="Seg_2539" s="T58">interrog.[n:case]</ta>
            <ta e="T60" id="Seg_2540" s="T59">n.[n:case]-n:poss</ta>
            <ta e="T61" id="Seg_2541" s="T60">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T62" id="Seg_2542" s="T61">n.[n:case]-n:poss</ta>
            <ta e="T63" id="Seg_2543" s="T62">pers-n:case</ta>
            <ta e="T64" id="Seg_2544" s="T63">v-v:tense-v:pn</ta>
            <ta e="T65" id="Seg_2545" s="T64">adv</ta>
            <ta e="T66" id="Seg_2546" s="T65">conj</ta>
            <ta e="T69" id="Seg_2547" s="T68">v-v:mood-v:tense-v:pn</ta>
            <ta e="T70" id="Seg_2548" s="T69">v-v:tense-v:pn</ta>
            <ta e="T71" id="Seg_2549" s="T70">n.[n:case]</ta>
            <ta e="T72" id="Seg_2550" s="T71">v-v:pn</ta>
            <ta e="T73" id="Seg_2551" s="T72">v-v&gt;v-v:mood-n:poss</ta>
            <ta e="T74" id="Seg_2552" s="T73">conj</ta>
            <ta e="T75" id="Seg_2553" s="T74">v-v:ins-v:pn</ta>
            <ta e="T76" id="Seg_2554" s="T75">pers-n:ins-n:case</ta>
            <ta e="T77" id="Seg_2555" s="T76">v-v:ins-v:pn</ta>
            <ta e="T78" id="Seg_2556" s="T77">pers.[n:case]</ta>
            <ta e="T79" id="Seg_2557" s="T78">v-v:ins-v:pn</ta>
            <ta e="T80" id="Seg_2558" s="T79">v-v&gt;v-v:pn</ta>
            <ta e="T81" id="Seg_2559" s="T80">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T82" id="Seg_2560" s="T81">n.[n:case]</ta>
            <ta e="T83" id="Seg_2561" s="T82">n-n:case</ta>
            <ta e="T84" id="Seg_2562" s="T83">v.[v:pn]</ta>
            <ta e="T85" id="Seg_2563" s="T84">n.[n:case]</ta>
            <ta e="T86" id="Seg_2564" s="T85">v-v:pn</ta>
            <ta e="T87" id="Seg_2565" s="T86">quant</ta>
            <ta e="T88" id="Seg_2566" s="T87">adv</ta>
            <ta e="T89" id="Seg_2567" s="T88">v-v:mood.pn</ta>
            <ta e="T90" id="Seg_2568" s="T89">pers-n:num.[n:case]</ta>
            <ta e="T91" id="Seg_2569" s="T90">quant</ta>
            <ta e="T92" id="Seg_2570" s="T91">adv</ta>
            <ta e="T93" id="Seg_2571" s="T92">v-v:ins-v:pn</ta>
            <ta e="T94" id="Seg_2572" s="T93">pers.[n:case]</ta>
            <ta e="T95" id="Seg_2573" s="T94">n-n:num-n:case</ta>
            <ta e="T96" id="Seg_2574" s="T95">v-v:ins-v:pn</ta>
            <ta e="T97" id="Seg_2575" s="T96">conj</ta>
            <ta e="T98" id="Seg_2576" s="T97">num</ta>
            <ta e="T99" id="Seg_2577" s="T98">n.[n:case]</ta>
            <ta e="T100" id="Seg_2578" s="T99">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T101" id="Seg_2579" s="T100">pers.[n:case]</ta>
            <ta e="T102" id="Seg_2580" s="T101">n-n:case</ta>
            <ta e="T103" id="Seg_2581" s="T102">n-n:case.poss</ta>
            <ta e="T104" id="Seg_2582" s="T103">n-n:case</ta>
            <ta e="T105" id="Seg_2583" s="T104">v-v:ins-v:pn</ta>
            <ta e="T106" id="Seg_2584" s="T105">conj</ta>
            <ta e="T107" id="Seg_2585" s="T106">v-v&gt;adv-clit</ta>
            <ta e="T108" id="Seg_2586" s="T107">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T109" id="Seg_2587" s="T108">pers.[n:case]</ta>
            <ta e="T110" id="Seg_2588" s="T109">v-v&gt;v-v:pn</ta>
            <ta e="T111" id="Seg_2589" s="T110">num</ta>
            <ta e="T112" id="Seg_2590" s="T111">n-n.[n:case]</ta>
            <ta e="T113" id="Seg_2591" s="T112">n-n:case</ta>
            <ta e="T114" id="Seg_2592" s="T113">v-v:tense.[v:pn]</ta>
            <ta e="T115" id="Seg_2593" s="T114">n.[n:case]</ta>
            <ta e="T116" id="Seg_2594" s="T115">adv</ta>
            <ta e="T117" id="Seg_2595" s="T116">v-v:pn</ta>
            <ta e="T118" id="Seg_2596" s="T117">v-v:pn</ta>
            <ta e="T119" id="Seg_2597" s="T118">v-v:mood.pn</ta>
            <ta e="T120" id="Seg_2598" s="T119">n-n:case</ta>
            <ta e="T121" id="Seg_2599" s="T120">n.[n:case]</ta>
            <ta e="T122" id="Seg_2600" s="T121">v-v&gt;v-v:pn</ta>
            <ta e="T123" id="Seg_2601" s="T122">n.[n:case]-n:poss</ta>
            <ta e="T124" id="Seg_2602" s="T123">v-v:pn</ta>
            <ta e="T125" id="Seg_2603" s="T124">adv</ta>
            <ta e="T126" id="Seg_2604" s="T125">pers-n:ins-n:case</ta>
            <ta e="T127" id="Seg_2605" s="T126">v-v:mood.pn</ta>
            <ta e="T128" id="Seg_2606" s="T127">pers</ta>
            <ta e="T129" id="Seg_2607" s="T128">pers</ta>
            <ta e="T130" id="Seg_2608" s="T129">quant</ta>
            <ta e="T131" id="Seg_2609" s="T130">v-v:tense-v:pn</ta>
            <ta e="T132" id="Seg_2610" s="T131">conj</ta>
            <ta e="T133" id="Seg_2611" s="T132">n.[n:case]</ta>
            <ta e="T134" id="Seg_2612" s="T133">v-v:pn</ta>
            <ta e="T135" id="Seg_2613" s="T134">pers</ta>
            <ta e="T136" id="Seg_2614" s="T135">interrog-n:case-clit</ta>
            <ta e="T137" id="Seg_2615" s="T136">ptcl</ta>
            <ta e="T138" id="Seg_2616" s="T137">ptcl</ta>
            <ta e="T139" id="Seg_2617" s="T138">ptcl</ta>
            <ta e="T140" id="Seg_2618" s="T139">pers</ta>
            <ta e="T141" id="Seg_2619" s="T140">pers</ta>
            <ta e="T142" id="Seg_2620" s="T141">n.[n:case]</ta>
            <ta e="T143" id="Seg_2621" s="T142">v-v:mood-v:tense-v:pn</ta>
            <ta e="T144" id="Seg_2622" s="T143">pers.[n:case]</ta>
            <ta e="T145" id="Seg_2623" s="T144">n-n:case</ta>
            <ta e="T146" id="Seg_2624" s="T145">v-v:ins-v:pn</ta>
            <ta e="T147" id="Seg_2625" s="T146">conj</ta>
            <ta e="T148" id="Seg_2626" s="T147">n.[n:case]</ta>
            <ta e="T149" id="Seg_2627" s="T148">ptcl</ta>
            <ta e="T150" id="Seg_2628" s="T149">v-v:mood.[v:pn]</ta>
            <ta e="T151" id="Seg_2629" s="T150">n-n:case</ta>
            <ta e="T152" id="Seg_2630" s="T151">v-v&gt;adv</ta>
            <ta e="T153" id="Seg_2631" s="T152">v-v:pn</ta>
            <ta e="T154" id="Seg_2632" s="T153">n.[n:case]-n:poss</ta>
            <ta e="T155" id="Seg_2633" s="T154">conj</ta>
            <ta e="T156" id="Seg_2634" s="T155">n.[n:case]-n:poss</ta>
            <ta e="T157" id="Seg_2635" s="T156">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T158" id="Seg_2636" s="T157">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T159" id="Seg_2637" s="T158">adv</ta>
            <ta e="T160" id="Seg_2638" s="T159">v-v:tense-v:pn</ta>
            <ta e="T161" id="Seg_2639" s="T160">conj</ta>
            <ta e="T162" id="Seg_2640" s="T161">n.[n:case]-n:poss</ta>
            <ta e="T163" id="Seg_2641" s="T162">v-v:mood.[v:pn]</ta>
            <ta e="T164" id="Seg_2642" s="T163">n.[n:case]-n:poss</ta>
            <ta e="T165" id="Seg_2643" s="T164">adj</ta>
            <ta e="T166" id="Seg_2644" s="T165">n.[n:case]</ta>
            <ta e="T167" id="Seg_2645" s="T166">v-v:mood-v:pn</ta>
            <ta e="T168" id="Seg_2646" s="T167">pers-n:ins-n:case</ta>
            <ta e="T169" id="Seg_2647" s="T168">n.[n:case]-n:poss</ta>
            <ta e="T170" id="Seg_2648" s="T169">quant-quant&gt;adv</ta>
            <ta e="T171" id="Seg_2649" s="T170">v-v:pn</ta>
            <ta e="T172" id="Seg_2650" s="T171">conj</ta>
            <ta e="T173" id="Seg_2651" s="T172">v-v:pn</ta>
            <ta e="T174" id="Seg_2652" s="T173">conj</ta>
            <ta e="T175" id="Seg_2653" s="T174">adj-n&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T176" id="Seg_2654" s="T175">v-v&gt;adv</ta>
            <ta e="T177" id="Seg_2655" s="T176">ptcl</ta>
            <ta e="T178" id="Seg_2656" s="T177">v-v&gt;v-v:ins-v:mood-v:pn</ta>
            <ta e="T179" id="Seg_2657" s="T178">adv</ta>
            <ta e="T180" id="Seg_2658" s="T179">adv</ta>
            <ta e="T181" id="Seg_2659" s="T180">v-v:pn</ta>
            <ta e="T182" id="Seg_2660" s="T181">n.[n:case]</ta>
            <ta e="T183" id="Seg_2661" s="T182">adv</ta>
            <ta e="T184" id="Seg_2662" s="T183">v-v:ins-v:pn</ta>
            <ta e="T185" id="Seg_2663" s="T184">n-n:case</ta>
            <ta e="T186" id="Seg_2664" s="T185">v-v:inf</ta>
            <ta e="T187" id="Seg_2665" s="T186">v-v:pn</ta>
            <ta e="T188" id="Seg_2666" s="T187">n-n:ins-n&gt;adv</ta>
            <ta e="T189" id="Seg_2667" s="T188">n-n:num.[n:case]</ta>
            <ta e="T190" id="Seg_2668" s="T189">quant</ta>
            <ta e="T191" id="Seg_2669" s="T190">n-n:case</ta>
            <ta e="T192" id="Seg_2670" s="T191">v-v:ins-v:pn</ta>
            <ta e="T193" id="Seg_2671" s="T192">adj</ta>
            <ta e="T194" id="Seg_2672" s="T193">n-n:case</ta>
            <ta e="T195" id="Seg_2673" s="T194">v-v&gt;adv</ta>
            <ta e="T196" id="Seg_2674" s="T195">v-v:pn</ta>
            <ta e="T197" id="Seg_2675" s="T196">conj</ta>
            <ta e="T198" id="Seg_2676" s="T197">pers.[n:case]</ta>
            <ta e="T199" id="Seg_2677" s="T198">v-v:tense.[v:pn]</ta>
            <ta e="T200" id="Seg_2678" s="T199">adj</ta>
            <ta e="T201" id="Seg_2679" s="T200">adj</ta>
            <ta e="T202" id="Seg_2680" s="T201">n-n:case</ta>
            <ta e="T203" id="Seg_2681" s="T202">adv</ta>
            <ta e="T204" id="Seg_2682" s="T203">n-n:num.[n:case]</ta>
            <ta e="T205" id="Seg_2683" s="T204">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T206" id="Seg_2684" s="T205">pers.[n:case]</ta>
            <ta e="T207" id="Seg_2685" s="T206">v-v:pn</ta>
            <ta e="T208" id="Seg_2686" s="T207">pers</ta>
            <ta e="T209" id="Seg_2687" s="T208">interrog-n:case</ta>
            <ta e="T210" id="Seg_2688" s="T209">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T211" id="Seg_2689" s="T210">conj</ta>
            <ta e="T212" id="Seg_2690" s="T211">pers-n:case</ta>
            <ta e="T213" id="Seg_2691" s="T212">n-n&gt;n.[n:case]</ta>
            <ta e="T214" id="Seg_2692" s="T213">n-v:tense-v:mood.[v:pn]</ta>
            <ta e="T215" id="Seg_2693" s="T214">pers.[n:case]</ta>
            <ta e="T216" id="Seg_2694" s="T215">pers-n:ins-n:case</ta>
            <ta e="T217" id="Seg_2695" s="T216">num-n:ins-num&gt;adj</ta>
            <ta e="T218" id="Seg_2696" s="T217">n-%%</ta>
            <ta e="T219" id="Seg_2697" s="T218">v-v&gt;adv</ta>
            <ta e="T220" id="Seg_2698" s="T219">v-v:ins-v&gt;v-v:ins-v:pn</ta>
            <ta e="T221" id="Seg_2699" s="T220">conj</ta>
            <ta e="T222" id="Seg_2700" s="T221">v-v&gt;v-v:inf</ta>
            <ta e="T225" id="Seg_2701" s="T224">ptcl</ta>
            <ta e="T226" id="Seg_2702" s="T225">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T227" id="Seg_2703" s="T226">conj</ta>
            <ta e="T228" id="Seg_2704" s="T227">n-n:ins-n:case</ta>
            <ta e="T229" id="Seg_2705" s="T228">n.[n:case]-n:poss</ta>
            <ta e="T230" id="Seg_2706" s="T229">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T231" id="Seg_2707" s="T230">n.[n:case]</ta>
            <ta e="T232" id="Seg_2708" s="T231">n-n&gt;n</ta>
            <ta e="T233" id="Seg_2709" s="T232">v-v:tense-v:pn</ta>
            <ta e="T234" id="Seg_2710" s="T233">pers.[n:case]</ta>
            <ta e="T235" id="Seg_2711" s="T234">v-v&gt;v-v:tense.[v:pn]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_2712" s="T1">v</ta>
            <ta e="T3" id="Seg_2713" s="T2">n</ta>
            <ta e="T4" id="Seg_2714" s="T3">pers</ta>
            <ta e="T5" id="Seg_2715" s="T4">num</ta>
            <ta e="T6" id="Seg_2716" s="T5">n</ta>
            <ta e="T7" id="Seg_2717" s="T6">n</ta>
            <ta e="T9" id="Seg_2718" s="T8">conj</ta>
            <ta e="T10" id="Seg_2719" s="T9">n</ta>
            <ta e="T11" id="Seg_2720" s="T10">v</ta>
            <ta e="T12" id="Seg_2721" s="T11">pers</ta>
            <ta e="T13" id="Seg_2722" s="T12">v</ta>
            <ta e="T14" id="Seg_2723" s="T13">n</ta>
            <ta e="T15" id="Seg_2724" s="T14">pers</ta>
            <ta e="T16" id="Seg_2725" s="T15">n</ta>
            <ta e="T17" id="Seg_2726" s="T16">v</ta>
            <ta e="T18" id="Seg_2727" s="T17">adv</ta>
            <ta e="T19" id="Seg_2728" s="T18">v</ta>
            <ta e="T20" id="Seg_2729" s="T19">n</ta>
            <ta e="T21" id="Seg_2730" s="T20">n</ta>
            <ta e="T22" id="Seg_2731" s="T21">v</ta>
            <ta e="T23" id="Seg_2732" s="T22">conj</ta>
            <ta e="T24" id="Seg_2733" s="T23">n</ta>
            <ta e="T25" id="Seg_2734" s="T24">pers</ta>
            <ta e="T26" id="Seg_2735" s="T25">num</ta>
            <ta e="T27" id="Seg_2736" s="T26">n</ta>
            <ta e="T28" id="Seg_2737" s="T27">adv</ta>
            <ta e="T29" id="Seg_2738" s="T28">v</ta>
            <ta e="T30" id="Seg_2739" s="T29">n</ta>
            <ta e="T31" id="Seg_2740" s="T30">adj</ta>
            <ta e="T32" id="Seg_2741" s="T31">adv</ta>
            <ta e="T33" id="Seg_2742" s="T32">v</ta>
            <ta e="T34" id="Seg_2743" s="T33">n</ta>
            <ta e="T35" id="Seg_2744" s="T34">pers</ta>
            <ta e="T36" id="Seg_2745" s="T35">num</ta>
            <ta e="T37" id="Seg_2746" s="T36">n</ta>
            <ta e="T38" id="Seg_2747" s="T37">v</ta>
            <ta e="T39" id="Seg_2748" s="T38">n</ta>
            <ta e="T40" id="Seg_2749" s="T39">quant</ta>
            <ta e="T41" id="Seg_2750" s="T40">v</ta>
            <ta e="T42" id="Seg_2751" s="T41">v</ta>
            <ta e="T43" id="Seg_2752" s="T42">pers</ta>
            <ta e="T44" id="Seg_2753" s="T43">v</ta>
            <ta e="T45" id="Seg_2754" s="T44">conj</ta>
            <ta e="T46" id="Seg_2755" s="T45">v</ta>
            <ta e="T47" id="Seg_2756" s="T46">interrog</ta>
            <ta e="T48" id="Seg_2757" s="T47">v</ta>
            <ta e="T49" id="Seg_2758" s="T48">conj</ta>
            <ta e="T50" id="Seg_2759" s="T49">pers</ta>
            <ta e="T51" id="Seg_2760" s="T50">v</ta>
            <ta e="T52" id="Seg_2761" s="T51">n</ta>
            <ta e="T53" id="Seg_2762" s="T52">n</ta>
            <ta e="T54" id="Seg_2763" s="T53">n</ta>
            <ta e="T55" id="Seg_2764" s="T54">pers</ta>
            <ta e="T56" id="Seg_2765" s="T55">v</ta>
            <ta e="T57" id="Seg_2766" s="T56">pers</ta>
            <ta e="T58" id="Seg_2767" s="T57">v</ta>
            <ta e="T59" id="Seg_2768" s="T58">interrog</ta>
            <ta e="T60" id="Seg_2769" s="T59">n</ta>
            <ta e="T61" id="Seg_2770" s="T60">v</ta>
            <ta e="T62" id="Seg_2771" s="T61">n</ta>
            <ta e="T63" id="Seg_2772" s="T62">pers</ta>
            <ta e="T64" id="Seg_2773" s="T63">v</ta>
            <ta e="T65" id="Seg_2774" s="T64">adv</ta>
            <ta e="T66" id="Seg_2775" s="T65">conj</ta>
            <ta e="T69" id="Seg_2776" s="T68">v</ta>
            <ta e="T70" id="Seg_2777" s="T69">v</ta>
            <ta e="T71" id="Seg_2778" s="T70">n</ta>
            <ta e="T72" id="Seg_2779" s="T71">v</ta>
            <ta e="T73" id="Seg_2780" s="T72">v</ta>
            <ta e="T74" id="Seg_2781" s="T73">conj</ta>
            <ta e="T75" id="Seg_2782" s="T74">v</ta>
            <ta e="T76" id="Seg_2783" s="T75">pers</ta>
            <ta e="T77" id="Seg_2784" s="T76">v</ta>
            <ta e="T78" id="Seg_2785" s="T77">pers</ta>
            <ta e="T79" id="Seg_2786" s="T78">v</ta>
            <ta e="T80" id="Seg_2787" s="T79">v</ta>
            <ta e="T81" id="Seg_2788" s="T80">n</ta>
            <ta e="T82" id="Seg_2789" s="T81">n</ta>
            <ta e="T83" id="Seg_2790" s="T82">n</ta>
            <ta e="T84" id="Seg_2791" s="T83">v</ta>
            <ta e="T85" id="Seg_2792" s="T84">n</ta>
            <ta e="T86" id="Seg_2793" s="T85">v</ta>
            <ta e="T87" id="Seg_2794" s="T86">quant</ta>
            <ta e="T88" id="Seg_2795" s="T87">adv</ta>
            <ta e="T89" id="Seg_2796" s="T88">v</ta>
            <ta e="T90" id="Seg_2797" s="T89">pers</ta>
            <ta e="T91" id="Seg_2798" s="T90">quant</ta>
            <ta e="T92" id="Seg_2799" s="T91">adv</ta>
            <ta e="T93" id="Seg_2800" s="T92">v</ta>
            <ta e="T94" id="Seg_2801" s="T93">pers</ta>
            <ta e="T95" id="Seg_2802" s="T94">n</ta>
            <ta e="T96" id="Seg_2803" s="T95">v</ta>
            <ta e="T97" id="Seg_2804" s="T96">conj</ta>
            <ta e="T98" id="Seg_2805" s="T97">num</ta>
            <ta e="T99" id="Seg_2806" s="T98">n</ta>
            <ta e="T100" id="Seg_2807" s="T99">v</ta>
            <ta e="T101" id="Seg_2808" s="T100">pers</ta>
            <ta e="T102" id="Seg_2809" s="T101">n</ta>
            <ta e="T103" id="Seg_2810" s="T102">n</ta>
            <ta e="T104" id="Seg_2811" s="T103">n</ta>
            <ta e="T105" id="Seg_2812" s="T104">v</ta>
            <ta e="T106" id="Seg_2813" s="T105">conj</ta>
            <ta e="T107" id="Seg_2814" s="T106">adv</ta>
            <ta e="T108" id="Seg_2815" s="T107">v</ta>
            <ta e="T109" id="Seg_2816" s="T108">pers</ta>
            <ta e="T110" id="Seg_2817" s="T109">v</ta>
            <ta e="T111" id="Seg_2818" s="T110">num</ta>
            <ta e="T112" id="Seg_2819" s="T111">n</ta>
            <ta e="T113" id="Seg_2820" s="T112">n</ta>
            <ta e="T114" id="Seg_2821" s="T113">v</ta>
            <ta e="T115" id="Seg_2822" s="T114">n</ta>
            <ta e="T116" id="Seg_2823" s="T115">adv</ta>
            <ta e="T117" id="Seg_2824" s="T116">v</ta>
            <ta e="T118" id="Seg_2825" s="T117">v</ta>
            <ta e="T119" id="Seg_2826" s="T118">v</ta>
            <ta e="T120" id="Seg_2827" s="T119">n</ta>
            <ta e="T121" id="Seg_2828" s="T120">n</ta>
            <ta e="T122" id="Seg_2829" s="T121">v</ta>
            <ta e="T123" id="Seg_2830" s="T122">n</ta>
            <ta e="T124" id="Seg_2831" s="T123">v</ta>
            <ta e="T125" id="Seg_2832" s="T124">adv</ta>
            <ta e="T126" id="Seg_2833" s="T125">pers</ta>
            <ta e="T127" id="Seg_2834" s="T126">v</ta>
            <ta e="T128" id="Seg_2835" s="T127">pers</ta>
            <ta e="T129" id="Seg_2836" s="T128">pers</ta>
            <ta e="T130" id="Seg_2837" s="T129">quant</ta>
            <ta e="T131" id="Seg_2838" s="T130">v</ta>
            <ta e="T132" id="Seg_2839" s="T131">conj</ta>
            <ta e="T133" id="Seg_2840" s="T132">n</ta>
            <ta e="T134" id="Seg_2841" s="T133">v</ta>
            <ta e="T135" id="Seg_2842" s="T134">pers</ta>
            <ta e="T136" id="Seg_2843" s="T135">pro</ta>
            <ta e="T137" id="Seg_2844" s="T136">ptcl</ta>
            <ta e="T138" id="Seg_2845" s="T137">ptcl</ta>
            <ta e="T139" id="Seg_2846" s="T138">ptcl</ta>
            <ta e="T140" id="Seg_2847" s="T139">pers</ta>
            <ta e="T141" id="Seg_2848" s="T140">pers</ta>
            <ta e="T142" id="Seg_2849" s="T141">n</ta>
            <ta e="T143" id="Seg_2850" s="T142">v</ta>
            <ta e="T144" id="Seg_2851" s="T143">pers</ta>
            <ta e="T145" id="Seg_2852" s="T144">n</ta>
            <ta e="T146" id="Seg_2853" s="T145">v</ta>
            <ta e="T147" id="Seg_2854" s="T146">conj</ta>
            <ta e="T148" id="Seg_2855" s="T147">n</ta>
            <ta e="T149" id="Seg_2856" s="T148">ptcl</ta>
            <ta e="T150" id="Seg_2857" s="T149">pro</ta>
            <ta e="T151" id="Seg_2858" s="T150">n</ta>
            <ta e="T152" id="Seg_2859" s="T151">adv</ta>
            <ta e="T153" id="Seg_2860" s="T152">v</ta>
            <ta e="T154" id="Seg_2861" s="T153">n</ta>
            <ta e="T155" id="Seg_2862" s="T154">conj</ta>
            <ta e="T156" id="Seg_2863" s="T155">n</ta>
            <ta e="T157" id="Seg_2864" s="T156">v</ta>
            <ta e="T158" id="Seg_2865" s="T157">v</ta>
            <ta e="T159" id="Seg_2866" s="T158">adv</ta>
            <ta e="T160" id="Seg_2867" s="T159">v</ta>
            <ta e="T161" id="Seg_2868" s="T160">conj</ta>
            <ta e="T162" id="Seg_2869" s="T161">n</ta>
            <ta e="T163" id="Seg_2870" s="T162">v</ta>
            <ta e="T164" id="Seg_2871" s="T163">n</ta>
            <ta e="T165" id="Seg_2872" s="T164">adj</ta>
            <ta e="T166" id="Seg_2873" s="T165">n</ta>
            <ta e="T167" id="Seg_2874" s="T166">v</ta>
            <ta e="T168" id="Seg_2875" s="T167">pers</ta>
            <ta e="T169" id="Seg_2876" s="T168">n</ta>
            <ta e="T170" id="Seg_2877" s="T169">quant</ta>
            <ta e="T171" id="Seg_2878" s="T170">v</ta>
            <ta e="T172" id="Seg_2879" s="T171">conj</ta>
            <ta e="T173" id="Seg_2880" s="T172">n</ta>
            <ta e="T174" id="Seg_2881" s="T173">conj</ta>
            <ta e="T175" id="Seg_2882" s="T174">adv</ta>
            <ta e="T176" id="Seg_2883" s="T175">adv</ta>
            <ta e="T177" id="Seg_2884" s="T176">ptcl</ta>
            <ta e="T178" id="Seg_2885" s="T177">v</ta>
            <ta e="T179" id="Seg_2886" s="T178">adv</ta>
            <ta e="T180" id="Seg_2887" s="T179">adv</ta>
            <ta e="T181" id="Seg_2888" s="T180">v</ta>
            <ta e="T182" id="Seg_2889" s="T181">n</ta>
            <ta e="T183" id="Seg_2890" s="T182">adv</ta>
            <ta e="T184" id="Seg_2891" s="T183">v</ta>
            <ta e="T185" id="Seg_2892" s="T184">n</ta>
            <ta e="T186" id="Seg_2893" s="T185">v</ta>
            <ta e="T187" id="Seg_2894" s="T186">v</ta>
            <ta e="T188" id="Seg_2895" s="T187">adv</ta>
            <ta e="T189" id="Seg_2896" s="T188">n</ta>
            <ta e="T190" id="Seg_2897" s="T189">quant</ta>
            <ta e="T191" id="Seg_2898" s="T190">n</ta>
            <ta e="T192" id="Seg_2899" s="T191">v</ta>
            <ta e="T193" id="Seg_2900" s="T192">adj</ta>
            <ta e="T194" id="Seg_2901" s="T193">n</ta>
            <ta e="T195" id="Seg_2902" s="T194">adv</ta>
            <ta e="T196" id="Seg_2903" s="T195">v</ta>
            <ta e="T197" id="Seg_2904" s="T196">conj</ta>
            <ta e="T198" id="Seg_2905" s="T197">pers</ta>
            <ta e="T199" id="Seg_2906" s="T198">v</ta>
            <ta e="T200" id="Seg_2907" s="T199">adj</ta>
            <ta e="T201" id="Seg_2908" s="T200">adj</ta>
            <ta e="T202" id="Seg_2909" s="T201">n</ta>
            <ta e="T203" id="Seg_2910" s="T202">adv</ta>
            <ta e="T204" id="Seg_2911" s="T203">n</ta>
            <ta e="T205" id="Seg_2912" s="T204">v</ta>
            <ta e="T206" id="Seg_2913" s="T205">pers</ta>
            <ta e="T207" id="Seg_2914" s="T206">v</ta>
            <ta e="T208" id="Seg_2915" s="T207">pers</ta>
            <ta e="T209" id="Seg_2916" s="T208">interrog</ta>
            <ta e="T210" id="Seg_2917" s="T209">v</ta>
            <ta e="T211" id="Seg_2918" s="T210">conj</ta>
            <ta e="T212" id="Seg_2919" s="T211">pers</ta>
            <ta e="T213" id="Seg_2920" s="T212">n</ta>
            <ta e="T214" id="Seg_2921" s="T213">n</ta>
            <ta e="T215" id="Seg_2922" s="T214">pers</ta>
            <ta e="T216" id="Seg_2923" s="T215">pers</ta>
            <ta e="T217" id="Seg_2924" s="T216">adj</ta>
            <ta e="T218" id="Seg_2925" s="T217">n</ta>
            <ta e="T219" id="Seg_2926" s="T218">adv</ta>
            <ta e="T220" id="Seg_2927" s="T219">v</ta>
            <ta e="T221" id="Seg_2928" s="T220">conj</ta>
            <ta e="T222" id="Seg_2929" s="T221">v</ta>
            <ta e="T225" id="Seg_2930" s="T224">ptcl</ta>
            <ta e="T226" id="Seg_2931" s="T225">v</ta>
            <ta e="T227" id="Seg_2932" s="T226">conj</ta>
            <ta e="T228" id="Seg_2933" s="T227">n</ta>
            <ta e="T229" id="Seg_2934" s="T228">n</ta>
            <ta e="T230" id="Seg_2935" s="T229">v</ta>
            <ta e="T231" id="Seg_2936" s="T230">n</ta>
            <ta e="T232" id="Seg_2937" s="T231">n</ta>
            <ta e="T233" id="Seg_2938" s="T232">v</ta>
            <ta e="T234" id="Seg_2939" s="T233">pers</ta>
            <ta e="T235" id="Seg_2940" s="T234">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_2941" s="T2">np.h:Th</ta>
            <ta e="T4" id="Seg_2942" s="T3">pro.h:Th</ta>
            <ta e="T6" id="Seg_2943" s="T5">adv:Time</ta>
            <ta e="T10" id="Seg_2944" s="T9">np:G</ta>
            <ta e="T11" id="Seg_2945" s="T10">0.3.h:A 0.3.h:Th</ta>
            <ta e="T12" id="Seg_2946" s="T11">pro.h:A</ta>
            <ta e="T14" id="Seg_2947" s="T13">np:G</ta>
            <ta e="T15" id="Seg_2948" s="T14">pro.h:A</ta>
            <ta e="T16" id="Seg_2949" s="T15">np:G</ta>
            <ta e="T19" id="Seg_2950" s="T18">0.3.h:A</ta>
            <ta e="T20" id="Seg_2951" s="T19">np.h:A</ta>
            <ta e="T21" id="Seg_2952" s="T20">np:P</ta>
            <ta e="T24" id="Seg_2953" s="T23">0.3.h:A</ta>
            <ta e="T25" id="Seg_2954" s="T24">pro.h:A</ta>
            <ta e="T27" id="Seg_2955" s="T26">np:G</ta>
            <ta e="T30" id="Seg_2956" s="T29">np:Th</ta>
            <ta e="T32" id="Seg_2957" s="T31">adv:L</ta>
            <ta e="T34" id="Seg_2958" s="T33">np.h:Th</ta>
            <ta e="T35" id="Seg_2959" s="T34">pro.h:Poss</ta>
            <ta e="T37" id="Seg_2960" s="T36">np.h:P</ta>
            <ta e="T39" id="Seg_2961" s="T38">np.h:Th</ta>
            <ta e="T42" id="Seg_2962" s="T41">0.3.h:A</ta>
            <ta e="T43" id="Seg_2963" s="T42">pro.h:A</ta>
            <ta e="T46" id="Seg_2964" s="T45">0.3.h:A</ta>
            <ta e="T48" id="Seg_2965" s="T47">0.2.h:A</ta>
            <ta e="T50" id="Seg_2966" s="T49">pro.h:A</ta>
            <ta e="T52" id="Seg_2967" s="T51">np.h:Poss</ta>
            <ta e="T53" id="Seg_2968" s="T52">np.h:P</ta>
            <ta e="T55" id="Seg_2969" s="T54">pro.h:A</ta>
            <ta e="T57" id="Seg_2970" s="T56">pro.h:A</ta>
            <ta e="T59" id="Seg_2971" s="T58">pro.h:A</ta>
            <ta e="T60" id="Seg_2972" s="T59">np.h:P 0.1.h:Poss</ta>
            <ta e="T62" id="Seg_2973" s="T61">np.h:Th 0.1.h:Poss</ta>
            <ta e="T63" id="Seg_2974" s="T62">pro.h:R</ta>
            <ta e="T64" id="Seg_2975" s="T63">0.1.h:A</ta>
            <ta e="T69" id="Seg_2976" s="T68">0.1.h:A</ta>
            <ta e="T71" id="Seg_2977" s="T70">np.h:A</ta>
            <ta e="T73" id="Seg_2978" s="T72">0.1.h:A</ta>
            <ta e="T75" id="Seg_2979" s="T74">0.3.h:A</ta>
            <ta e="T76" id="Seg_2980" s="T75">pro.h:Th</ta>
            <ta e="T77" id="Seg_2981" s="T76">0.3.h:A</ta>
            <ta e="T78" id="Seg_2982" s="T77">pro.h:A</ta>
            <ta e="T80" id="Seg_2983" s="T79">0.3.h:A</ta>
            <ta e="T82" id="Seg_2984" s="T81">np.h:Th</ta>
            <ta e="T83" id="Seg_2985" s="T82">np:L</ta>
            <ta e="T85" id="Seg_2986" s="T84">np.h:A</ta>
            <ta e="T88" id="Seg_2987" s="T87">adv:G</ta>
            <ta e="T89" id="Seg_2988" s="T88">0.2.h:A</ta>
            <ta e="T90" id="Seg_2989" s="T89">pro.h:A</ta>
            <ta e="T92" id="Seg_2990" s="T91">adv:G</ta>
            <ta e="T94" id="Seg_2991" s="T93">pro.h:A</ta>
            <ta e="T95" id="Seg_2992" s="T94">np:Th</ta>
            <ta e="T99" id="Seg_2993" s="T98">np:Th</ta>
            <ta e="T101" id="Seg_2994" s="T100">pro.h:A</ta>
            <ta e="T102" id="Seg_2995" s="T101">np.h:Th</ta>
            <ta e="T103" id="Seg_2996" s="T102">np:So</ta>
            <ta e="T104" id="Seg_2997" s="T103">np:G</ta>
            <ta e="T108" id="Seg_2998" s="T107">0.3.h:A 0.3:Th</ta>
            <ta e="T109" id="Seg_2999" s="T108">pro.h:P</ta>
            <ta e="T112" id="Seg_3000" s="T111">np.h:A</ta>
            <ta e="T113" id="Seg_3001" s="T112">np:Path</ta>
            <ta e="T115" id="Seg_3002" s="T114">np.h:A</ta>
            <ta e="T116" id="Seg_3003" s="T115">adv:G</ta>
            <ta e="T118" id="Seg_3004" s="T117">0.3.h:A</ta>
            <ta e="T119" id="Seg_3005" s="T118">0.2.h:A</ta>
            <ta e="T120" id="Seg_3006" s="T119">np:G</ta>
            <ta e="T121" id="Seg_3007" s="T120">np.h:Th</ta>
            <ta e="T123" id="Seg_3008" s="T122">np.h:A 0.3.h:Poss</ta>
            <ta e="T125" id="Seg_3009" s="T124">adv:Time</ta>
            <ta e="T126" id="Seg_3010" s="T125">pro.h:Th</ta>
            <ta e="T127" id="Seg_3011" s="T126">0.2.h:A</ta>
            <ta e="T128" id="Seg_3012" s="T127">pro.h:A</ta>
            <ta e="T129" id="Seg_3013" s="T128">pro.h:R</ta>
            <ta e="T130" id="Seg_3014" s="T129">pro:Th</ta>
            <ta e="T133" id="Seg_3015" s="T132">np.h:A</ta>
            <ta e="T136" id="Seg_3016" s="T135">pro:Th</ta>
            <ta e="T140" id="Seg_3017" s="T139">pro.h:A</ta>
            <ta e="T141" id="Seg_3018" s="T140">pro.h:R</ta>
            <ta e="T142" id="Seg_3019" s="T141">np:Th</ta>
            <ta e="T144" id="Seg_3020" s="T143">pro.h:A</ta>
            <ta e="T145" id="Seg_3021" s="T144">np:Th</ta>
            <ta e="T148" id="Seg_3022" s="T147">np.h:A</ta>
            <ta e="T151" id="Seg_3023" s="T150">np:G</ta>
            <ta e="T153" id="Seg_3024" s="T152">0.3.h:A</ta>
            <ta e="T154" id="Seg_3025" s="T153">np.h:P 0.3.h:Poss</ta>
            <ta e="T156" id="Seg_3026" s="T155">np.h:P 0.3.h:Poss</ta>
            <ta e="T160" id="Seg_3027" s="T159">0.3.h:E</ta>
            <ta e="T162" id="Seg_3028" s="T161">np.h:A 0.3.h:Poss</ta>
            <ta e="T164" id="Seg_3029" s="T163">np.h:A 0.3.h:Poss</ta>
            <ta e="T166" id="Seg_3030" s="T165">np:Th</ta>
            <ta e="T168" id="Seg_3031" s="T167">pro.h:Poss</ta>
            <ta e="T169" id="Seg_3032" s="T168">np:Th</ta>
            <ta e="T173" id="Seg_3033" s="T172">0.3.h:A</ta>
            <ta e="T178" id="Seg_3034" s="T177">0.3.h:A</ta>
            <ta e="T179" id="Seg_3035" s="T178">adv:Time</ta>
            <ta e="T181" id="Seg_3036" s="T180">0.3.h:Th</ta>
            <ta e="T182" id="Seg_3037" s="T181">np.h:A</ta>
            <ta e="T183" id="Seg_3038" s="T182">adv:Time</ta>
            <ta e="T185" id="Seg_3039" s="T184">np:Th</ta>
            <ta e="T187" id="Seg_3040" s="T186">0.3.h:A</ta>
            <ta e="T188" id="Seg_3041" s="T187">adv:Time</ta>
            <ta e="T189" id="Seg_3042" s="T188">np.h:Th</ta>
            <ta e="T194" id="Seg_3043" s="T193">np:G</ta>
            <ta e="T196" id="Seg_3044" s="T195">0.3.h:A</ta>
            <ta e="T198" id="Seg_3045" s="T197">pro.h:Th</ta>
            <ta e="T202" id="Seg_3046" s="T201">np:L</ta>
            <ta e="T204" id="Seg_3047" s="T203">np.h:A</ta>
            <ta e="T206" id="Seg_3048" s="T205">pro.h:A</ta>
            <ta e="T208" id="Seg_3049" s="T207">pro.h:A</ta>
            <ta e="T212" id="Seg_3050" s="T211">pro.h:Poss</ta>
            <ta e="T215" id="Seg_3051" s="T214">pro.h:A</ta>
            <ta e="T216" id="Seg_3052" s="T215">pro.h:Th</ta>
            <ta e="T218" id="Seg_3053" s="T217">np:Time</ta>
            <ta e="T226" id="Seg_3054" s="T225">0.3.h:P</ta>
            <ta e="T228" id="Seg_3055" s="T227">np.h:Poss</ta>
            <ta e="T229" id="Seg_3056" s="T228">np.h:P</ta>
            <ta e="T231" id="Seg_3057" s="T230">np.h:A</ta>
            <ta e="T233" id="Seg_3058" s="T232">0.3.h:Th</ta>
            <ta e="T234" id="Seg_3059" s="T233">pro.h:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_3060" s="T1">v:pred</ta>
            <ta e="T3" id="Seg_3061" s="T2">np.h:S</ta>
            <ta e="T4" id="Seg_3062" s="T3">pro.h:S</ta>
            <ta e="T11" id="Seg_3063" s="T10">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T12" id="Seg_3064" s="T11">pro.h:S</ta>
            <ta e="T13" id="Seg_3065" s="T12">v:pred</ta>
            <ta e="T15" id="Seg_3066" s="T14">pro.h:S</ta>
            <ta e="T17" id="Seg_3067" s="T16">v:pred</ta>
            <ta e="T19" id="Seg_3068" s="T18">0.3.h:S v:pred</ta>
            <ta e="T20" id="Seg_3069" s="T19">np.h:S</ta>
            <ta e="T21" id="Seg_3070" s="T20">np:O</ta>
            <ta e="T22" id="Seg_3071" s="T21">v:pred</ta>
            <ta e="T24" id="Seg_3072" s="T23">0.3.h:S v:pred</ta>
            <ta e="T25" id="Seg_3073" s="T24">pro.h:S</ta>
            <ta e="T28" id="Seg_3074" s="T27">s:temp</ta>
            <ta e="T29" id="Seg_3075" s="T28">v:pred</ta>
            <ta e="T30" id="Seg_3076" s="T29">np:S</ta>
            <ta e="T31" id="Seg_3077" s="T30">adj:pred</ta>
            <ta e="T33" id="Seg_3078" s="T32">v:pred</ta>
            <ta e="T34" id="Seg_3079" s="T33">np.h:S</ta>
            <ta e="T37" id="Seg_3080" s="T36">np.h:S</ta>
            <ta e="T38" id="Seg_3081" s="T37">v:pred</ta>
            <ta e="T39" id="Seg_3082" s="T38">np.h:S</ta>
            <ta e="T41" id="Seg_3083" s="T40">v:pred</ta>
            <ta e="T42" id="Seg_3084" s="T41">0.3.h:S v:pred</ta>
            <ta e="T43" id="Seg_3085" s="T42">pro.h:S</ta>
            <ta e="T44" id="Seg_3086" s="T43">v:pred</ta>
            <ta e="T46" id="Seg_3087" s="T45">0.3.h:S v:pred</ta>
            <ta e="T48" id="Seg_3088" s="T47">0.2.h:S v:pred</ta>
            <ta e="T50" id="Seg_3089" s="T49">pro.h:S</ta>
            <ta e="T51" id="Seg_3090" s="T50">v:pred</ta>
            <ta e="T53" id="Seg_3091" s="T52">np.h:S</ta>
            <ta e="T54" id="Seg_3092" s="T53">v:pred</ta>
            <ta e="T55" id="Seg_3093" s="T54">pro.h:S</ta>
            <ta e="T56" id="Seg_3094" s="T55">v:pred</ta>
            <ta e="T57" id="Seg_3095" s="T56">pro.h:S</ta>
            <ta e="T58" id="Seg_3096" s="T57">v:pred</ta>
            <ta e="T59" id="Seg_3097" s="T58">pro.h:S</ta>
            <ta e="T60" id="Seg_3098" s="T59">np.h:O</ta>
            <ta e="T61" id="Seg_3099" s="T60">v:pred</ta>
            <ta e="T62" id="Seg_3100" s="T61">np.h:O</ta>
            <ta e="T64" id="Seg_3101" s="T63">0.1.h:S v:pred</ta>
            <ta e="T69" id="Seg_3102" s="T68">0.1.h:S v:pred</ta>
            <ta e="T71" id="Seg_3103" s="T70">np.h:S</ta>
            <ta e="T72" id="Seg_3104" s="T71">v:pred</ta>
            <ta e="T73" id="Seg_3105" s="T72">0.1.h:S v:pred</ta>
            <ta e="T75" id="Seg_3106" s="T74">0.3.h:S v:pred</ta>
            <ta e="T76" id="Seg_3107" s="T75">pro.h:O</ta>
            <ta e="T77" id="Seg_3108" s="T76">0.3.h:S v:pred</ta>
            <ta e="T78" id="Seg_3109" s="T77">pro.h:S</ta>
            <ta e="T79" id="Seg_3110" s="T78">n:pred</ta>
            <ta e="T80" id="Seg_3111" s="T79">0.3.h:S v:pred</ta>
            <ta e="T82" id="Seg_3112" s="T81">np.h:S</ta>
            <ta e="T84" id="Seg_3113" s="T83">v:pred</ta>
            <ta e="T85" id="Seg_3114" s="T84">np.h:S</ta>
            <ta e="T86" id="Seg_3115" s="T85">v:pred</ta>
            <ta e="T89" id="Seg_3116" s="T88">0.2.h:S v:pred</ta>
            <ta e="T90" id="Seg_3117" s="T89">pro.h:S</ta>
            <ta e="T93" id="Seg_3118" s="T92">v:pred</ta>
            <ta e="T94" id="Seg_3119" s="T93">pro.h:S</ta>
            <ta e="T95" id="Seg_3120" s="T94">np:O</ta>
            <ta e="T96" id="Seg_3121" s="T95">v:pred</ta>
            <ta e="T99" id="Seg_3122" s="T98">np:S</ta>
            <ta e="T100" id="Seg_3123" s="T99">v:pred</ta>
            <ta e="T101" id="Seg_3124" s="T100">pro.h:S</ta>
            <ta e="T102" id="Seg_3125" s="T101">np.h:O</ta>
            <ta e="T105" id="Seg_3126" s="T104">v:pred</ta>
            <ta e="T108" id="Seg_3127" s="T107">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T109" id="Seg_3128" s="T108">pro.h:S</ta>
            <ta e="T110" id="Seg_3129" s="T109">v:pred</ta>
            <ta e="T112" id="Seg_3130" s="T111">np.h:S</ta>
            <ta e="T114" id="Seg_3131" s="T113">v:pred</ta>
            <ta e="T115" id="Seg_3132" s="T114">np.h:S</ta>
            <ta e="T117" id="Seg_3133" s="T116">v:pred</ta>
            <ta e="T118" id="Seg_3134" s="T117">0.3.h:S v:pred</ta>
            <ta e="T119" id="Seg_3135" s="T118">0.2.h:S v:pred</ta>
            <ta e="T121" id="Seg_3136" s="T120">np.h:S</ta>
            <ta e="T122" id="Seg_3137" s="T121">v:pred</ta>
            <ta e="T123" id="Seg_3138" s="T122">np.h:S</ta>
            <ta e="T124" id="Seg_3139" s="T123">v:pred</ta>
            <ta e="T126" id="Seg_3140" s="T125">pro.h:O</ta>
            <ta e="T127" id="Seg_3141" s="T126">0.2.h:S v:pred</ta>
            <ta e="T128" id="Seg_3142" s="T127">pro.h:S</ta>
            <ta e="T130" id="Seg_3143" s="T129">pro:O</ta>
            <ta e="T131" id="Seg_3144" s="T130">v:pred</ta>
            <ta e="T133" id="Seg_3145" s="T132">np.h:S</ta>
            <ta e="T134" id="Seg_3146" s="T133">v:pred</ta>
            <ta e="T136" id="Seg_3147" s="T135">pro:O</ta>
            <ta e="T138" id="Seg_3148" s="T137">ptcl:pred</ta>
            <ta e="T140" id="Seg_3149" s="T139">pro.h:S</ta>
            <ta e="T142" id="Seg_3150" s="T141">np:O</ta>
            <ta e="T143" id="Seg_3151" s="T142">v:pred</ta>
            <ta e="T144" id="Seg_3152" s="T143">pro.h:S</ta>
            <ta e="T145" id="Seg_3153" s="T144">np:O</ta>
            <ta e="T146" id="Seg_3154" s="T145">v:pred</ta>
            <ta e="T148" id="Seg_3155" s="T147">np.h:S</ta>
            <ta e="T150" id="Seg_3156" s="T149">v:pred</ta>
            <ta e="T152" id="Seg_3157" s="T151">s:temp</ta>
            <ta e="T153" id="Seg_3158" s="T152">0.3.h:S v:pred</ta>
            <ta e="T154" id="Seg_3159" s="T153">np.h:S</ta>
            <ta e="T156" id="Seg_3160" s="T155">np.h:S</ta>
            <ta e="T157" id="Seg_3161" s="T156">v:pred</ta>
            <ta e="T158" id="Seg_3162" s="T157">v:pred</ta>
            <ta e="T160" id="Seg_3163" s="T159">0.3.h:S v:pred</ta>
            <ta e="T163" id="Seg_3164" s="T160">s:compl</ta>
            <ta e="T164" id="Seg_3165" s="T163">np.h:S</ta>
            <ta e="T166" id="Seg_3166" s="T165">np:O</ta>
            <ta e="T167" id="Seg_3167" s="T166">v:pred</ta>
            <ta e="T169" id="Seg_3168" s="T168">np:S</ta>
            <ta e="T171" id="Seg_3169" s="T170">v:pred</ta>
            <ta e="T173" id="Seg_3170" s="T172">0.3.h:S v:pred</ta>
            <ta e="T175" id="Seg_3171" s="T174">s:adv</ta>
            <ta e="T178" id="Seg_3172" s="T177">0.3.h:S v:pred</ta>
            <ta e="T181" id="Seg_3173" s="T180">0.3.h:S v:pred</ta>
            <ta e="T182" id="Seg_3174" s="T181">np.h:S</ta>
            <ta e="T184" id="Seg_3175" s="T183">v:pred</ta>
            <ta e="T186" id="Seg_3176" s="T184">s:purp</ta>
            <ta e="T187" id="Seg_3177" s="T186">0.3.h:S v:pred</ta>
            <ta e="T189" id="Seg_3178" s="T188">np.h:S</ta>
            <ta e="T192" id="Seg_3179" s="T191">v:pred</ta>
            <ta e="T195" id="Seg_3180" s="T194">s:temp</ta>
            <ta e="T196" id="Seg_3181" s="T195">0.3.h:S v:pred</ta>
            <ta e="T199" id="Seg_3182" s="T196">s:rel</ta>
            <ta e="T204" id="Seg_3183" s="T203">np.h:S</ta>
            <ta e="T205" id="Seg_3184" s="T204">v:pred</ta>
            <ta e="T206" id="Seg_3185" s="T205">pro.h:S</ta>
            <ta e="T207" id="Seg_3186" s="T206">v:pred</ta>
            <ta e="T208" id="Seg_3187" s="T207">pro.h:S</ta>
            <ta e="T210" id="Seg_3188" s="T209">v:pred</ta>
            <ta e="T215" id="Seg_3189" s="T214">pro.h:S</ta>
            <ta e="T216" id="Seg_3190" s="T215">pro.h:O</ta>
            <ta e="T219" id="Seg_3191" s="T218">s:temp</ta>
            <ta e="T220" id="Seg_3192" s="T219">v:pred</ta>
            <ta e="T226" id="Seg_3193" s="T225">0.3.h:S v:pred</ta>
            <ta e="T229" id="Seg_3194" s="T228">np.h:S</ta>
            <ta e="T230" id="Seg_3195" s="T229">v:pred</ta>
            <ta e="T231" id="Seg_3196" s="T230">np.h:S</ta>
            <ta e="T233" id="Seg_3197" s="T232">v:pred 0.3.h:O</ta>
            <ta e="T234" id="Seg_3198" s="T233">pro.h:S</ta>
            <ta e="T235" id="Seg_3199" s="T234">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T3" id="Seg_3200" s="T2">RUS:cult</ta>
            <ta e="T7" id="Seg_3201" s="T6">RUS:cult</ta>
            <ta e="T9" id="Seg_3202" s="T8">RUS:gram</ta>
            <ta e="T23" id="Seg_3203" s="T22">RUS:gram</ta>
            <ta e="T34" id="Seg_3204" s="T33">RUS:cult</ta>
            <ta e="T40" id="Seg_3205" s="T39">RUS:core</ta>
            <ta e="T45" id="Seg_3206" s="T44">RUS:gram</ta>
            <ta e="T49" id="Seg_3207" s="T48">RUS:gram</ta>
            <ta e="T52" id="Seg_3208" s="T51">RUS:cult</ta>
            <ta e="T66" id="Seg_3209" s="T65">RUS:gram</ta>
            <ta e="T71" id="Seg_3210" s="T70">RUS:cult</ta>
            <ta e="T74" id="Seg_3211" s="T73">RUS:gram</ta>
            <ta e="T77" id="Seg_3212" s="T76">RUS:core</ta>
            <ta e="T82" id="Seg_3213" s="T81">RUS:core</ta>
            <ta e="T85" id="Seg_3214" s="T84">RUS:cult</ta>
            <ta e="T87" id="Seg_3215" s="T86">RUS:core</ta>
            <ta e="T91" id="Seg_3216" s="T90">RUS:core</ta>
            <ta e="T95" id="Seg_3217" s="T94">RUS:cult</ta>
            <ta e="T96" id="Seg_3218" s="T95">RUS:cult</ta>
            <ta e="T97" id="Seg_3219" s="T96">RUS:gram</ta>
            <ta e="T99" id="Seg_3220" s="T98">RUS:cult</ta>
            <ta e="T102" id="Seg_3221" s="T101">RUS:core</ta>
            <ta e="T106" id="Seg_3222" s="T105">RUS:gram</ta>
            <ta e="T113" id="Seg_3223" s="T112">RUS:cult</ta>
            <ta e="T115" id="Seg_3224" s="T114">RUS:cult</ta>
            <ta e="T121" id="Seg_3225" s="T120">RUS:core</ta>
            <ta e="T125" id="Seg_3226" s="T124">RUS:core</ta>
            <ta e="T130" id="Seg_3227" s="T129">RUS:core</ta>
            <ta e="T132" id="Seg_3228" s="T131">RUS:gram</ta>
            <ta e="T133" id="Seg_3229" s="T132">RUS:cult</ta>
            <ta e="T137" id="Seg_3230" s="T136">RUS:gram</ta>
            <ta e="T138" id="Seg_3231" s="T137">RUS:mod</ta>
            <ta e="T139" id="Seg_3232" s="T138">RUS:disc</ta>
            <ta e="T147" id="Seg_3233" s="T146">RUS:gram</ta>
            <ta e="T148" id="Seg_3234" s="T147">RUS:cult</ta>
            <ta e="T155" id="Seg_3235" s="T154">RUS:gram</ta>
            <ta e="T159" id="Seg_3236" s="T158">RUS:core</ta>
            <ta e="T161" id="Seg_3237" s="T160">RUS:gram</ta>
            <ta e="T172" id="Seg_3238" s="T171">RUS:gram</ta>
            <ta e="T174" id="Seg_3239" s="T173">RUS:gram</ta>
            <ta e="T179" id="Seg_3240" s="T178">RUS:core</ta>
            <ta e="T182" id="Seg_3241" s="T181">RUS:cult</ta>
            <ta e="T183" id="Seg_3242" s="T182">RUS:core</ta>
            <ta e="T190" id="Seg_3243" s="T189">RUS:core</ta>
            <ta e="T191" id="Seg_3244" s="T190">RUS:cult</ta>
            <ta e="T200" id="Seg_3245" s="T199">RUS:core</ta>
            <ta e="T203" id="Seg_3246" s="T202">RUS:core</ta>
            <ta e="T211" id="Seg_3247" s="T210">RUS:gram</ta>
            <ta e="T218" id="Seg_3248" s="T217">RUS:cult</ta>
            <ta e="T221" id="Seg_3249" s="T220">RUS:gram</ta>
            <ta e="T227" id="Seg_3250" s="T226">RUS:gram</ta>
            <ta e="T228" id="Seg_3251" s="T227">RUS:cult</ta>
            <ta e="T231" id="Seg_3252" s="T230">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_3253" s="T1">Once there lived a soldier.</ta>
            <ta e="T8" id="Seg_3254" s="T3">He served in the army for twenty years.</ta>
            <ta e="T11" id="Seg_3255" s="T8">And then he was sent home.</ta>
            <ta e="T14" id="Seg_3256" s="T11">He went home.</ta>
            <ta e="T19" id="Seg_3257" s="T14">He went home on foot.</ta>
            <ta e="T24" id="Seg_3258" s="T19">People were mowing and haymaking.</ta>
            <ta e="T29" id="Seg_3259" s="T24">He came to a village.</ta>
            <ta e="T31" id="Seg_3260" s="T29">The village was big.</ta>
            <ta e="T34" id="Seg_3261" s="T31">There lived a merchant.</ta>
            <ta e="T38" id="Seg_3262" s="T34">One of his daughters had died.</ta>
            <ta e="T42" id="Seg_3263" s="T38">People were standing and talking.</ta>
            <ta e="T48" id="Seg_3264" s="T42">He stopped and asked: “Why did you gather together?”</ta>
            <ta e="T54" id="Seg_3265" s="T48">And they said: “The merchant's daughter had died.”</ta>
            <ta e="T70" id="Seg_3266" s="T54">They said: “He said: I'll give half of my belongings and my daughter to the one, who will bring her back to life.”</ta>
            <ta e="T75" id="Seg_3267" s="T70">The soldier said: “Let me come in,” – and he came in.</ta>
            <ta e="T77" id="Seg_3268" s="T75">He was met.</ta>
            <ta e="T84" id="Seg_3269" s="T77">He came in and saw a girl lying in a coffin.</ta>
            <ta e="T89" id="Seg_3270" s="T84">The soldier said: “All of you should go outside.”</ta>
            <ta e="T93" id="Seg_3271" s="T89">All of them came out.</ta>
            <ta e="T96" id="Seg_3272" s="T93">He curtained the windows.</ta>
            <ta e="T100" id="Seg_3273" s="T96">And one window was open.</ta>
            <ta e="T108" id="Seg_3274" s="T100">He put he girl out of the coffin to the floor and began having sex with her.</ta>
            <ta e="T110" id="Seg_3275" s="T108">She came to life.</ta>
            <ta e="T114" id="Seg_3276" s="T110">One man was looking in the window.</ta>
            <ta e="T122" id="Seg_3277" s="T114">The soldier came out and said: “Come in, the girl returned to life.”</ta>
            <ta e="T127" id="Seg_3278" s="T122">The father said: “Now take her.</ta>
            <ta e="T131" id="Seg_3279" s="T127">I'll give you everything.”</ta>
            <ta e="T138" id="Seg_3280" s="T131">And the soldier said: “I don't need anything.”</ta>
            <ta e="T143" id="Seg_3281" s="T138">“Well, I'll give you money.”</ta>
            <ta e="T146" id="Seg_3282" s="T143">He gave him money.</ta>
            <ta e="T150" id="Seg_3283" s="T146">And the soldier left.</ta>
            <ta e="T153" id="Seg_3284" s="T150">He came home.</ta>
            <ta e="T158" id="Seg_3285" s="T153">His father and his mother grew old.</ta>
            <ta e="T163" id="Seg_3286" s="T158">They were so happy, that their son had come back.</ta>
            <ta e="T167" id="Seg_3287" s="T163">Their son bought a good house.</ta>
            <ta e="T171" id="Seg_3288" s="T167">He had much money.</ta>
            <ta e="T173" id="Seg_3289" s="T171">And he got married.</ta>
            <ta e="T178" id="Seg_3290" s="T173">And they lived being rich.</ta>
            <ta e="T181" id="Seg_3291" s="T178">Now they live well.</ta>
            <ta e="T186" id="Seg_3292" s="T181">Then the soldier went to buy a horse.</ta>
            <ta e="T188" id="Seg_3293" s="T186">He went in summer.</ta>
            <ta e="T192" id="Seg_3294" s="T188">All the people were mowing.</ta>
            <ta e="T199" id="Seg_3295" s="T192">He came to a big village, where he had been once.</ta>
            <ta e="T205" id="Seg_3296" s="T199">Many people gathered in one last small house.</ta>
            <ta e="T210" id="Seg_3297" s="T205">He asked: “Why did you gather here?”</ta>
            <ta e="T214" id="Seg_3298" s="T210">“One of our old women had died.</ta>
            <ta e="T224" id="Seg_3299" s="T214">We are having sex with her for three weeks already and we can't bring her back to life.</ta>
            <ta e="T226" id="Seg_3300" s="T224">She is not coming back to life.</ta>
            <ta e="T233" id="Seg_3301" s="T226">And [when] a daughter of the merchant was dying, one soldier had sex with her.</ta>
            <ta e="T235" id="Seg_3302" s="T233">And she returned to life.”</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_3303" s="T1">Es lebte ein Soldat.</ta>
            <ta e="T8" id="Seg_3304" s="T3">Er diente in der Armee zwanzig Jahre lang.</ta>
            <ta e="T11" id="Seg_3305" s="T8">Und dann wurde er heimgeschickt.</ta>
            <ta e="T14" id="Seg_3306" s="T11">Er ging nach Hause.</ta>
            <ta e="T19" id="Seg_3307" s="T14">Er ging zu Fuß nach Hause.</ta>
            <ta e="T24" id="Seg_3308" s="T19">Die Leute mähten Getreide und machten Heu.</ta>
            <ta e="T29" id="Seg_3309" s="T24">Er kam in ein Dorf.</ta>
            <ta e="T31" id="Seg_3310" s="T29">Das Dorf war groß.</ta>
            <ta e="T34" id="Seg_3311" s="T31">Dort lebte ein Händler.</ta>
            <ta e="T38" id="Seg_3312" s="T34">Eine seiner Töchter war gestorben.</ta>
            <ta e="T42" id="Seg_3313" s="T38">Menschen standen [herum] und redeten.</ta>
            <ta e="T48" id="Seg_3314" s="T42">Er hielt an und fragte: "Warum habt ihr euch versammelt?"</ta>
            <ta e="T54" id="Seg_3315" s="T48">Und sie sagten: "Die Tochter des Händlers ist gestorben."</ta>
            <ta e="T70" id="Seg_3316" s="T54">Sie sagten: "Er hat gesagt: Wer meine Tochter wiederbelebt, dem gebe die Hälfte meines Besitzes und meine Tochter."</ta>
            <ta e="T75" id="Seg_3317" s="T70">Der Soldat sagte: "Lasst mich hineingehen", -- und er ging hinein.</ta>
            <ta e="T77" id="Seg_3318" s="T75">Er wurde empfangen.</ta>
            <ta e="T84" id="Seg_3319" s="T77">Er ging hinein und sah ein Mädchen in einem Sarg liegen.</ta>
            <ta e="T89" id="Seg_3320" s="T84">Der Soldat sagte: "Ihr alle geht nach draußen."</ta>
            <ta e="T93" id="Seg_3321" s="T89">Alle gingen hinaus.</ta>
            <ta e="T96" id="Seg_3322" s="T93">Er zog die Vorhänge vor die Fenster.</ta>
            <ta e="T100" id="Seg_3323" s="T96">Und ein Fenster war offen.</ta>
            <ta e="T108" id="Seg_3324" s="T100">Er legte das Mädchen aus dem Sarg auf den Boden und begann mit ihr zu schlafen.</ta>
            <ta e="T110" id="Seg_3325" s="T108">Sie erwachte zu neuem Leben.</ta>
            <ta e="T114" id="Seg_3326" s="T110">Ein Mann sah durchs Fenster.</ta>
            <ta e="T122" id="Seg_3327" s="T114">Der Soldat kam hinaus und sagte: "Kommt herein, das Mädchen lebt wieder."</ta>
            <ta e="T127" id="Seg_3328" s="T122">Der Vater sagte: "Jetzt nimm sie.</ta>
            <ta e="T131" id="Seg_3329" s="T127">Ich gebe dir alles."</ta>
            <ta e="T138" id="Seg_3330" s="T131">Und der Soldat sagte: "Ich brauche nichts."</ta>
            <ta e="T143" id="Seg_3331" s="T138">"Nun, ich gebe dir Geld."</ta>
            <ta e="T146" id="Seg_3332" s="T143">Er gab ihm Geld.</ta>
            <ta e="T150" id="Seg_3333" s="T146">Und der Soldat ging.</ta>
            <ta e="T153" id="Seg_3334" s="T150">Er kam nach Hause.</ta>
            <ta e="T158" id="Seg_3335" s="T153">Sein Vater und seine Mutter wurden alt.</ta>
            <ta e="T163" id="Seg_3336" s="T158">Sie waren so glücklich, dass ihr Sohn zurückgekommen war.</ta>
            <ta e="T167" id="Seg_3337" s="T163">Ihr Sohn kaufte ein gutes Haus.</ta>
            <ta e="T171" id="Seg_3338" s="T167">Er hatte viel Geld.</ta>
            <ta e="T173" id="Seg_3339" s="T171">Und er heiratete.</ta>
            <ta e="T178" id="Seg_3340" s="T173">Und sie lebten als reiche Menschen.</ta>
            <ta e="T181" id="Seg_3341" s="T178">Jetzt leben sie gut.</ta>
            <ta e="T186" id="Seg_3342" s="T181">Dann ging der Soldat, um ein Pferd zu kaufen.</ta>
            <ta e="T188" id="Seg_3343" s="T186">Er ging im Sommer.</ta>
            <ta e="T192" id="Seg_3344" s="T188">Die Leute mähten das Heu.</ta>
            <ta e="T199" id="Seg_3345" s="T192">Er kam in das große Dorf, wo er gewesen war.</ta>
            <ta e="T205" id="Seg_3346" s="T199">Viele Menschen versammelten sich in einem letzten kleinen Haus.</ta>
            <ta e="T210" id="Seg_3347" s="T205">Er fragte: "Warum habt ihr euch hier versammelt?"</ta>
            <ta e="T214" id="Seg_3348" s="T210">"Eine unserer alten Frauen ist gestorben.</ta>
            <ta e="T224" id="Seg_3349" s="T214">Wir haben drei Wochen lang mit ihr geschlafen aber wir können sie nicht wiederbeleben.</ta>
            <ta e="T226" id="Seg_3350" s="T224">Sie erwacht nicht zu neuem Leben.</ta>
            <ta e="T233" id="Seg_3351" s="T226">Und [als] die Tochter des Händlers gestorben war, hat ein Soldat mit ihr geschlafen.</ta>
            <ta e="T235" id="Seg_3352" s="T233">Und sie ist zu neuem Leben erwacht."</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_3353" s="T1">Жил солдат.</ta>
            <ta e="T8" id="Seg_3354" s="T3">Он двадцать лет в армии служил.</ta>
            <ta e="T11" id="Seg_3355" s="T8">И в деревню (домой) его отпустили.</ta>
            <ta e="T14" id="Seg_3356" s="T11">Он идет домой.</ta>
            <ta e="T19" id="Seg_3357" s="T14">Он домой отправился, пешком идет.</ta>
            <ta e="T24" id="Seg_3358" s="T19">Люди хлеб режут (серпом), сено убирают.</ta>
            <ta e="T29" id="Seg_3359" s="T24">Он в одну деревню пришел.</ta>
            <ta e="T31" id="Seg_3360" s="T29">Деревня большая.</ta>
            <ta e="T34" id="Seg_3361" s="T31">Там живет купец.</ta>
            <ta e="T38" id="Seg_3362" s="T34">У него одна дочь умерла.</ta>
            <ta e="T42" id="Seg_3363" s="T38">Люди все стоят разговаривают.</ta>
            <ta e="T48" id="Seg_3364" s="T42">Он остановился и спрашивает: “Чего собрались?”</ta>
            <ta e="T54" id="Seg_3365" s="T48">А они говорят: “Дочь купца умерла”.</ta>
            <ta e="T70" id="Seg_3366" s="T54">Они говорят: “Он сказал, кто дочь оживит, дочь тому отдам замуж и половину имущества отдаю (отдам)”.</ta>
            <ta e="T75" id="Seg_3367" s="T70">Солдат говорит: “Ну-ка зайду-ка”, – и зашел.</ta>
            <ta e="T77" id="Seg_3368" s="T75">Его встретили.</ta>
            <ta e="T84" id="Seg_3369" s="T77">Он вошел, смотрит: девка в гробу лежит.</ta>
            <ta e="T89" id="Seg_3370" s="T84">Солдат говорит: “Все на улицу уходите”.</ta>
            <ta e="T93" id="Seg_3371" s="T89">Они все на улицу ушли.</ta>
            <ta e="T96" id="Seg_3372" s="T93">Он окошки занавесил.</ta>
            <ta e="T100" id="Seg_3373" s="T96">А одно окошко открыто было.</ta>
            <ta e="T108" id="Seg_3374" s="T100">Он девку из гроба на пол положил и совокупляться с ней начал.</ta>
            <ta e="T110" id="Seg_3375" s="T108">Она ожила.</ta>
            <ta e="T114" id="Seg_3376" s="T110">Один мужик в окошко глядел.</ta>
            <ta e="T122" id="Seg_3377" s="T114">Солдат на улицу вышел, говорит: “Заходите в дом, девка ожила”.</ta>
            <ta e="T127" id="Seg_3378" s="T122">Отец говорит: “Теперь ее бери.</ta>
            <ta e="T131" id="Seg_3379" s="T127">Я вам все дам”.</ta>
            <ta e="T138" id="Seg_3380" s="T131">А солдат говорит: “Мне ничего не надо”.</ta>
            <ta e="T143" id="Seg_3381" s="T138">“Ну, я тебе деньги дам”.</ta>
            <ta e="T146" id="Seg_3382" s="T143">Он деньги дал.</ta>
            <ta e="T150" id="Seg_3383" s="T146">И солдат пошел.</ta>
            <ta e="T153" id="Seg_3384" s="T150">Домой пришел.</ta>
            <ta e="T158" id="Seg_3385" s="T153">Отец и мать стариком и старухой стали.</ta>
            <ta e="T163" id="Seg_3386" s="T158">До того обрадовались, что сын пришел.</ta>
            <ta e="T167" id="Seg_3387" s="T163">Сын хороший дом купил.</ta>
            <ta e="T171" id="Seg_3388" s="T167">У него денег много.</ta>
            <ta e="T173" id="Seg_3389" s="T171">И женился.</ta>
            <ta e="T178" id="Seg_3390" s="T173">И богато жить стали.</ta>
            <ta e="T181" id="Seg_3391" s="T178">Теперь хорошо живут.</ta>
            <ta e="T186" id="Seg_3392" s="T181">Солдат теперь ушел лошадь покупать.</ta>
            <ta e="T188" id="Seg_3393" s="T186">Ходит летом.</ta>
            <ta e="T192" id="Seg_3394" s="T188">Все люди на покосе были.</ta>
            <ta e="T199" id="Seg_3395" s="T192">В большую деревню пришел, в которой он был.</ta>
            <ta e="T205" id="Seg_3396" s="T199">В крайней маленькой избе так много людей собралось.</ta>
            <ta e="T210" id="Seg_3397" s="T205">Он спрашивает: “Вы зачем собрались?”</ta>
            <ta e="T214" id="Seg_3398" s="T210">“А у нас старуха умерла.</ta>
            <ta e="T224" id="Seg_3399" s="T214">Мы с ней третью неделю совокупляемся и оживить не можем.</ta>
            <ta e="T226" id="Seg_3400" s="T224">Не оживает.</ta>
            <ta e="T233" id="Seg_3401" s="T226">А у купца дочь помирала, солдат один совокупился с ней.</ta>
            <ta e="T235" id="Seg_3402" s="T233">Она ожила”.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_3403" s="T1">жил солдат</ta>
            <ta e="T8" id="Seg_3404" s="T3">он двадцать лет в армии служил</ta>
            <ta e="T11" id="Seg_3405" s="T8">в деревню (домой) его пустили</ta>
            <ta e="T14" id="Seg_3406" s="T11">он идет домой</ta>
            <ta e="T19" id="Seg_3407" s="T14">он домой отправился пешком идет</ta>
            <ta e="T24" id="Seg_3408" s="T19">люди хлеб режут (серпом) (убирают) сено убирают</ta>
            <ta e="T29" id="Seg_3409" s="T24">он в одну деревню пришел</ta>
            <ta e="T31" id="Seg_3410" s="T29">деревня большая</ta>
            <ta e="T34" id="Seg_3411" s="T31">тут живет купец</ta>
            <ta e="T38" id="Seg_3412" s="T34">у него одна дочь</ta>
            <ta e="T42" id="Seg_3413" s="T38">люди все стоят разговаривают</ta>
            <ta e="T48" id="Seg_3414" s="T42">он стал и спрашивает чего собрались</ta>
            <ta e="T54" id="Seg_3415" s="T48">они говорят дочь купца умерла</ta>
            <ta e="T70" id="Seg_3416" s="T54">он говорит кто дочь оживит дочь тому отдам замуж и половина имущества отдаю (отдам)</ta>
            <ta e="T75" id="Seg_3417" s="T70">солдат говорит ну-ка зауду-ка и зашел</ta>
            <ta e="T77" id="Seg_3418" s="T75">его встретили</ta>
            <ta e="T84" id="Seg_3419" s="T77">он вошел смотрит девка в гробу лежала</ta>
            <ta e="T89" id="Seg_3420" s="T84">солдат говорит все на улицу уходите</ta>
            <ta e="T93" id="Seg_3421" s="T89">они все на улицу ушли</ta>
            <ta e="T96" id="Seg_3422" s="T93">он окошки занавесил</ta>
            <ta e="T100" id="Seg_3423" s="T96">одно окошко открыто было</ta>
            <ta e="T108" id="Seg_3424" s="T100">он девку из гроба на пол положил и vӧgeln начал</ta>
            <ta e="T110" id="Seg_3425" s="T108">она ожила</ta>
            <ta e="T114" id="Seg_3426" s="T110">один мужик в окошко глядел</ta>
            <ta e="T122" id="Seg_3427" s="T114">солдат на улицу вышел говорит заходите в дом девка ожила</ta>
            <ta e="T127" id="Seg_3428" s="T122">отец говорит теперь ее бери</ta>
            <ta e="T131" id="Seg_3429" s="T127">я вам все дам</ta>
            <ta e="T138" id="Seg_3430" s="T131">солдат говорит мне ничего не надо</ta>
            <ta e="T143" id="Seg_3431" s="T138">я тебе деньги дам</ta>
            <ta e="T146" id="Seg_3432" s="T143">он деньги дал</ta>
            <ta e="T150" id="Seg_3433" s="T146">солдат пошел</ta>
            <ta e="T153" id="Seg_3434" s="T150">домой пришел</ta>
            <ta e="T158" id="Seg_3435" s="T153">отец и мать старые стали</ta>
            <ta e="T163" id="Seg_3436" s="T158">до того обрадовались что сын пришел</ta>
            <ta e="T167" id="Seg_3437" s="T163">сын хороший дом купил</ta>
            <ta e="T171" id="Seg_3438" s="T167">у него денег много</ta>
            <ta e="T173" id="Seg_3439" s="T171">и женился</ta>
            <ta e="T178" id="Seg_3440" s="T173">богато жить стали</ta>
            <ta e="T181" id="Seg_3441" s="T178">теперь хорошо живут</ta>
            <ta e="T186" id="Seg_3442" s="T181">солдат ушел лошадь покупать</ta>
            <ta e="T188" id="Seg_3443" s="T186">ходит летом</ta>
            <ta e="T192" id="Seg_3444" s="T188">все люди на покосе были</ta>
            <ta e="T199" id="Seg_3445" s="T192">в большую деревню пришел когда (в которой) он был</ta>
            <ta e="T205" id="Seg_3446" s="T199">в крайней маленькой избе так много людей собралось</ta>
            <ta e="T210" id="Seg_3447" s="T205">он спрашивает вы зачем собрались</ta>
            <ta e="T214" id="Seg_3448" s="T210">а у нас старуха умерла</ta>
            <ta e="T224" id="Seg_3449" s="T214">мы ее третью неделю ебем и оживить не можем</ta>
            <ta e="T226" id="Seg_3450" s="T224">не оживает</ta>
            <ta e="T233" id="Seg_3451" s="T226">купцова дочь помирала солдат один еб</ta>
            <ta e="T235" id="Seg_3452" s="T233">она ожила</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T42" id="Seg_3453" s="T38">[BrM:] 'nɨga taqulupbattə' changed to 'nɨgata qulupbattə'.</ta>
            <ta e="T48" id="Seg_3454" s="T42"> ‎‎[BrM:] Tentative analysis of 'taqkulupbaltə'.</ta>
            <ta e="T54" id="Seg_3455" s="T48">[KuAI:] Variant: 'qummɨt'. [BrM:] INFER?</ta>
            <ta e="T70" id="Seg_3456" s="T54">[KuAI:] Variants: 'näu', 'təbetdə'.</ta>
            <ta e="T108" id="Seg_3457" s="T100">[BrM:] Tentative analysis of 'malʼne'.</ta>
            <ta e="T114" id="Seg_3458" s="T110">[BrM:] PROL?</ta>
            <ta e="T122" id="Seg_3459" s="T114">[KuAI:] Variant: 'Särnaltə'.</ta>
            <ta e="T143" id="Seg_3460" s="T138">[BrM:] INFER?</ta>
            <ta e="T163" id="Seg_3461" s="T158">[BrM:] 'Da tao' changed to 'Datao'.</ta>
            <ta e="T178" id="Seg_3462" s="T173">[KuAI:] Variant: 'qoubɨle'.</ta>
            <ta e="T205" id="Seg_3463" s="T199">[BrM:] 'da tao' changed to 'datao'. [BrM:] Tentative analysis of 'takqulupbattə'.</ta>
            <ta e="T210" id="Seg_3464" s="T205">[BrM:] Tentative analysis of 'taqkulupbaltə'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T24" id="Seg_3465" s="T19">нӱги - сено</ta>
            <ta e="T108" id="Seg_3466" s="T100">зачеркнуто «ебать»</ta>
            <ta e="T199" id="Seg_3467" s="T192">когда из армии шел</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
