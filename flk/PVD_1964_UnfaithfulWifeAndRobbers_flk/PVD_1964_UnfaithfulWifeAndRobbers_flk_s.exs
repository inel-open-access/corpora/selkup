<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_UnfaithfulWifeAndRobbers_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_UnfaithfulWifeAndRobbers_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1519</ud-information>
            <ud-information attribute-name="# HIAT:w">1050</ud-information>
            <ud-information attribute-name="# e">1050</ud-information>
            <ud-information attribute-name="# HIAT:u">224</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
         <tli id="T204" />
         <tli id="T205" />
         <tli id="T206" />
         <tli id="T207" />
         <tli id="T208" />
         <tli id="T209" />
         <tli id="T210" />
         <tli id="T211" />
         <tli id="T212" />
         <tli id="T213" />
         <tli id="T214" />
         <tli id="T215" />
         <tli id="T216" />
         <tli id="T217" />
         <tli id="T218" />
         <tli id="T219" />
         <tli id="T220" />
         <tli id="T221" />
         <tli id="T222" />
         <tli id="T223" />
         <tli id="T224" />
         <tli id="T225" />
         <tli id="T226" />
         <tli id="T227" />
         <tli id="T228" />
         <tli id="T229" />
         <tli id="T230" />
         <tli id="T231" />
         <tli id="T232" />
         <tli id="T233" />
         <tli id="T234" />
         <tli id="T235" />
         <tli id="T236" />
         <tli id="T237" />
         <tli id="T238" />
         <tli id="T239" />
         <tli id="T240" />
         <tli id="T241" />
         <tli id="T242" />
         <tli id="T243" />
         <tli id="T244" />
         <tli id="T245" />
         <tli id="T246" />
         <tli id="T247" />
         <tli id="T248" />
         <tli id="T249" />
         <tli id="T250" />
         <tli id="T251" />
         <tli id="T252" />
         <tli id="T253" />
         <tli id="T254" />
         <tli id="T255" />
         <tli id="T256" />
         <tli id="T257" />
         <tli id="T258" />
         <tli id="T259" />
         <tli id="T260" />
         <tli id="T261" />
         <tli id="T262" />
         <tli id="T263" />
         <tli id="T264" />
         <tli id="T265" />
         <tli id="T266" />
         <tli id="T267" />
         <tli id="T268" />
         <tli id="T269" />
         <tli id="T270" />
         <tli id="T271" />
         <tli id="T272" />
         <tli id="T273" />
         <tli id="T274" />
         <tli id="T275" />
         <tli id="T276" />
         <tli id="T277" />
         <tli id="T278" />
         <tli id="T279" />
         <tli id="T280" />
         <tli id="T281" />
         <tli id="T282" />
         <tli id="T283" />
         <tli id="T284" />
         <tli id="T285" />
         <tli id="T286" />
         <tli id="T287" />
         <tli id="T288" />
         <tli id="T289" />
         <tli id="T290" />
         <tli id="T291" />
         <tli id="T292" />
         <tli id="T293" />
         <tli id="T294" />
         <tli id="T295" />
         <tli id="T296" />
         <tli id="T297" />
         <tli id="T298" />
         <tli id="T299" />
         <tli id="T300" />
         <tli id="T301" />
         <tli id="T302" />
         <tli id="T303" />
         <tli id="T304" />
         <tli id="T305" />
         <tli id="T306" />
         <tli id="T307" />
         <tli id="T308" />
         <tli id="T309" />
         <tli id="T310" />
         <tli id="T311" />
         <tli id="T312" />
         <tli id="T313" />
         <tli id="T314" />
         <tli id="T315" />
         <tli id="T316" />
         <tli id="T317" />
         <tli id="T318" />
         <tli id="T319" />
         <tli id="T320" />
         <tli id="T321" />
         <tli id="T322" />
         <tli id="T323" />
         <tli id="T324" />
         <tli id="T325" />
         <tli id="T326" />
         <tli id="T327" />
         <tli id="T328" />
         <tli id="T329" />
         <tli id="T330" />
         <tli id="T331" />
         <tli id="T332" />
         <tli id="T333" />
         <tli id="T334" />
         <tli id="T335" />
         <tli id="T336" />
         <tli id="T337" />
         <tli id="T338" />
         <tli id="T339" />
         <tli id="T340" />
         <tli id="T341" />
         <tli id="T342" />
         <tli id="T343" />
         <tli id="T344" />
         <tli id="T345" />
         <tli id="T346" />
         <tli id="T347" />
         <tli id="T348" />
         <tli id="T349" />
         <tli id="T350" />
         <tli id="T351" />
         <tli id="T352" />
         <tli id="T353" />
         <tli id="T354" />
         <tli id="T355" />
         <tli id="T356" />
         <tli id="T357" />
         <tli id="T358" />
         <tli id="T359" />
         <tli id="T360" />
         <tli id="T361" />
         <tli id="T362" />
         <tli id="T363" />
         <tli id="T364" />
         <tli id="T365" />
         <tli id="T366" />
         <tli id="T367" />
         <tli id="T368" />
         <tli id="T369" />
         <tli id="T370" />
         <tli id="T371" />
         <tli id="T372" />
         <tli id="T373" />
         <tli id="T374" />
         <tli id="T375" />
         <tli id="T376" />
         <tli id="T377" />
         <tli id="T378" />
         <tli id="T379" />
         <tli id="T380" />
         <tli id="T381" />
         <tli id="T382" />
         <tli id="T383" />
         <tli id="T384" />
         <tli id="T385" />
         <tli id="T386" />
         <tli id="T387" />
         <tli id="T388" />
         <tli id="T389" />
         <tli id="T390" />
         <tli id="T391" />
         <tli id="T392" />
         <tli id="T393" />
         <tli id="T394" />
         <tli id="T395" />
         <tli id="T396" />
         <tli id="T397" />
         <tli id="T398" />
         <tli id="T399" />
         <tli id="T400" />
         <tli id="T401" />
         <tli id="T402" />
         <tli id="T403" />
         <tli id="T404" />
         <tli id="T405" />
         <tli id="T406" />
         <tli id="T407" />
         <tli id="T408" />
         <tli id="T409" />
         <tli id="T410" />
         <tli id="T411" />
         <tli id="T412" />
         <tli id="T413" />
         <tli id="T414" />
         <tli id="T415" />
         <tli id="T416" />
         <tli id="T417" />
         <tli id="T418" />
         <tli id="T419" />
         <tli id="T420" />
         <tli id="T421" />
         <tli id="T422" />
         <tli id="T423" />
         <tli id="T424" />
         <tli id="T425" />
         <tli id="T426" />
         <tli id="T427" />
         <tli id="T428" />
         <tli id="T429" />
         <tli id="T430" />
         <tli id="T431" />
         <tli id="T432" />
         <tli id="T433" />
         <tli id="T434" />
         <tli id="T435" />
         <tli id="T436" />
         <tli id="T437" />
         <tli id="T438" />
         <tli id="T439" />
         <tli id="T440" />
         <tli id="T441" />
         <tli id="T442" />
         <tli id="T443" />
         <tli id="T444" />
         <tli id="T445" />
         <tli id="T446" />
         <tli id="T447" />
         <tli id="T448" />
         <tli id="T449" />
         <tli id="T450" />
         <tli id="T451" />
         <tli id="T452" />
         <tli id="T453" />
         <tli id="T454" />
         <tli id="T455" />
         <tli id="T456" />
         <tli id="T457" />
         <tli id="T458" />
         <tli id="T459" />
         <tli id="T460" />
         <tli id="T461" />
         <tli id="T462" />
         <tli id="T463" />
         <tli id="T464" />
         <tli id="T465" />
         <tli id="T466" />
         <tli id="T467" />
         <tli id="T468" />
         <tli id="T469" />
         <tli id="T470" />
         <tli id="T471" />
         <tli id="T472" />
         <tli id="T473" />
         <tli id="T474" />
         <tli id="T475" />
         <tli id="T476" />
         <tli id="T477" />
         <tli id="T478" />
         <tli id="T479" />
         <tli id="T480" />
         <tli id="T481" />
         <tli id="T482" />
         <tli id="T483" />
         <tli id="T484" />
         <tli id="T485" />
         <tli id="T486" />
         <tli id="T487" />
         <tli id="T488" />
         <tli id="T489" />
         <tli id="T490" />
         <tli id="T491" />
         <tli id="T492" />
         <tli id="T493" />
         <tli id="T494" />
         <tli id="T495" />
         <tli id="T496" />
         <tli id="T497" />
         <tli id="T498" />
         <tli id="T499" />
         <tli id="T500" />
         <tli id="T501" />
         <tli id="T502" />
         <tli id="T503" />
         <tli id="T504" />
         <tli id="T505" />
         <tli id="T506" />
         <tli id="T507" />
         <tli id="T508" />
         <tli id="T509" />
         <tli id="T510" />
         <tli id="T511" />
         <tli id="T512" />
         <tli id="T513" />
         <tli id="T514" />
         <tli id="T515" />
         <tli id="T516" />
         <tli id="T517" />
         <tli id="T518" />
         <tli id="T519" />
         <tli id="T520" />
         <tli id="T521" />
         <tli id="T522" />
         <tli id="T523" />
         <tli id="T524" />
         <tli id="T525" />
         <tli id="T526" />
         <tli id="T527" />
         <tli id="T528" />
         <tli id="T529" />
         <tli id="T530" />
         <tli id="T531" />
         <tli id="T532" />
         <tli id="T533" />
         <tli id="T534" />
         <tli id="T535" />
         <tli id="T536" />
         <tli id="T537" />
         <tli id="T538" />
         <tli id="T539" />
         <tli id="T540" />
         <tli id="T541" />
         <tli id="T542" />
         <tli id="T543" />
         <tli id="T544" />
         <tli id="T545" />
         <tli id="T546" />
         <tli id="T547" />
         <tli id="T548" />
         <tli id="T549" />
         <tli id="T550" />
         <tli id="T551" />
         <tli id="T552" />
         <tli id="T553" />
         <tli id="T554" />
         <tli id="T555" />
         <tli id="T556" />
         <tli id="T557" />
         <tli id="T558" />
         <tli id="T559" />
         <tli id="T560" />
         <tli id="T561" />
         <tli id="T562" />
         <tli id="T563" />
         <tli id="T564" />
         <tli id="T565" />
         <tli id="T566" />
         <tli id="T567" />
         <tli id="T568" />
         <tli id="T569" />
         <tli id="T570" />
         <tli id="T571" />
         <tli id="T572" />
         <tli id="T573" />
         <tli id="T574" />
         <tli id="T575" />
         <tli id="T576" />
         <tli id="T577" />
         <tli id="T578" />
         <tli id="T579" />
         <tli id="T580" />
         <tli id="T581" />
         <tli id="T582" />
         <tli id="T583" />
         <tli id="T584" />
         <tli id="T585" />
         <tli id="T586" />
         <tli id="T587" />
         <tli id="T588" />
         <tli id="T589" />
         <tli id="T590" />
         <tli id="T591" />
         <tli id="T592" />
         <tli id="T593" />
         <tli id="T594" />
         <tli id="T595" />
         <tli id="T596" />
         <tli id="T597" />
         <tli id="T598" />
         <tli id="T599" />
         <tli id="T600" />
         <tli id="T601" />
         <tli id="T602" />
         <tli id="T603" />
         <tli id="T604" />
         <tli id="T605" />
         <tli id="T606" />
         <tli id="T607" />
         <tli id="T608" />
         <tli id="T609" />
         <tli id="T610" />
         <tli id="T611" />
         <tli id="T612" />
         <tli id="T613" />
         <tli id="T614" />
         <tli id="T615" />
         <tli id="T616" />
         <tli id="T617" />
         <tli id="T618" />
         <tli id="T619" />
         <tli id="T620" />
         <tli id="T621" />
         <tli id="T622" />
         <tli id="T623" />
         <tli id="T624" />
         <tli id="T625" />
         <tli id="T626" />
         <tli id="T627" />
         <tli id="T628" />
         <tli id="T629" />
         <tli id="T630" />
         <tli id="T631" />
         <tli id="T632" />
         <tli id="T633" />
         <tli id="T634" />
         <tli id="T635" />
         <tli id="T636" />
         <tli id="T637" />
         <tli id="T638" />
         <tli id="T639" />
         <tli id="T640" />
         <tli id="T641" />
         <tli id="T642" />
         <tli id="T643" />
         <tli id="T644" />
         <tli id="T645" />
         <tli id="T646" />
         <tli id="T647" />
         <tli id="T648" />
         <tli id="T649" />
         <tli id="T650" />
         <tli id="T651" />
         <tli id="T652" />
         <tli id="T653" />
         <tli id="T654" />
         <tli id="T655" />
         <tli id="T656" />
         <tli id="T657" />
         <tli id="T658" />
         <tli id="T659" />
         <tli id="T660" />
         <tli id="T661" />
         <tli id="T662" />
         <tli id="T663" />
         <tli id="T664" />
         <tli id="T665" />
         <tli id="T666" />
         <tli id="T667" />
         <tli id="T668" />
         <tli id="T669" />
         <tli id="T670" />
         <tli id="T671" />
         <tli id="T672" />
         <tli id="T673" />
         <tli id="T674" />
         <tli id="T675" />
         <tli id="T676" />
         <tli id="T677" />
         <tli id="T678" />
         <tli id="T679" />
         <tli id="T680" />
         <tli id="T681" />
         <tli id="T682" />
         <tli id="T683" />
         <tli id="T684" />
         <tli id="T685" />
         <tli id="T686" />
         <tli id="T687" />
         <tli id="T688" />
         <tli id="T689" />
         <tli id="T690" />
         <tli id="T691" />
         <tli id="T692" />
         <tli id="T693" />
         <tli id="T694" />
         <tli id="T695" />
         <tli id="T696" />
         <tli id="T697" />
         <tli id="T698" />
         <tli id="T699" />
         <tli id="T700" />
         <tli id="T701" />
         <tli id="T702" />
         <tli id="T703" />
         <tli id="T704" />
         <tli id="T705" />
         <tli id="T706" />
         <tli id="T707" />
         <tli id="T708" />
         <tli id="T709" />
         <tli id="T710" />
         <tli id="T711" />
         <tli id="T712" />
         <tli id="T713" />
         <tli id="T714" />
         <tli id="T715" />
         <tli id="T716" />
         <tli id="T717" />
         <tli id="T718" />
         <tli id="T719" />
         <tli id="T720" />
         <tli id="T721" />
         <tli id="T722" />
         <tli id="T723" />
         <tli id="T724" />
         <tli id="T725" />
         <tli id="T726" />
         <tli id="T727" />
         <tli id="T728" />
         <tli id="T729" />
         <tli id="T730" />
         <tli id="T731" />
         <tli id="T732" />
         <tli id="T733" />
         <tli id="T734" />
         <tli id="T735" />
         <tli id="T736" />
         <tli id="T737" />
         <tli id="T738" />
         <tli id="T739" />
         <tli id="T740" />
         <tli id="T741" />
         <tli id="T742" />
         <tli id="T743" />
         <tli id="T744" />
         <tli id="T745" />
         <tli id="T746" />
         <tli id="T747" />
         <tli id="T748" />
         <tli id="T749" />
         <tli id="T750" />
         <tli id="T751" />
         <tli id="T752" />
         <tli id="T753" />
         <tli id="T754" />
         <tli id="T755" />
         <tli id="T756" />
         <tli id="T757" />
         <tli id="T758" />
         <tli id="T759" />
         <tli id="T760" />
         <tli id="T761" />
         <tli id="T762" />
         <tli id="T763" />
         <tli id="T764" />
         <tli id="T765" />
         <tli id="T766" />
         <tli id="T767" />
         <tli id="T768" />
         <tli id="T769" />
         <tli id="T770" />
         <tli id="T771" />
         <tli id="T772" />
         <tli id="T773" />
         <tli id="T774" />
         <tli id="T775" />
         <tli id="T776" />
         <tli id="T777" />
         <tli id="T778" />
         <tli id="T779" />
         <tli id="T780" />
         <tli id="T781" />
         <tli id="T782" />
         <tli id="T783" />
         <tli id="T784" />
         <tli id="T785" />
         <tli id="T786" />
         <tli id="T787" />
         <tli id="T788" />
         <tli id="T789" />
         <tli id="T790" />
         <tli id="T791" />
         <tli id="T792" />
         <tli id="T793" />
         <tli id="T794" />
         <tli id="T795" />
         <tli id="T796" />
         <tli id="T797" />
         <tli id="T798" />
         <tli id="T799" />
         <tli id="T800" />
         <tli id="T801" />
         <tli id="T802" />
         <tli id="T803" />
         <tli id="T804" />
         <tli id="T805" />
         <tli id="T806" />
         <tli id="T807" />
         <tli id="T808" />
         <tli id="T809" />
         <tli id="T810" />
         <tli id="T811" />
         <tli id="T812" />
         <tli id="T813" />
         <tli id="T814" />
         <tli id="T815" />
         <tli id="T816" />
         <tli id="T817" />
         <tli id="T818" />
         <tli id="T819" />
         <tli id="T820" />
         <tli id="T821" />
         <tli id="T822" />
         <tli id="T823" />
         <tli id="T824" />
         <tli id="T825" />
         <tli id="T826" />
         <tli id="T827" />
         <tli id="T828" />
         <tli id="T829" />
         <tli id="T830" />
         <tli id="T831" />
         <tli id="T832" />
         <tli id="T833" />
         <tli id="T834" />
         <tli id="T835" />
         <tli id="T836" />
         <tli id="T837" />
         <tli id="T838" />
         <tli id="T839" />
         <tli id="T840" />
         <tli id="T841" />
         <tli id="T842" />
         <tli id="T843" />
         <tli id="T844" />
         <tli id="T845" />
         <tli id="T846" />
         <tli id="T847" />
         <tli id="T848" />
         <tli id="T849" />
         <tli id="T850" />
         <tli id="T851" />
         <tli id="T852" />
         <tli id="T853" />
         <tli id="T854" />
         <tli id="T855" />
         <tli id="T856" />
         <tli id="T857" />
         <tli id="T858" />
         <tli id="T859" />
         <tli id="T860" />
         <tli id="T861" />
         <tli id="T862" />
         <tli id="T863" />
         <tli id="T864" />
         <tli id="T865" />
         <tli id="T866" />
         <tli id="T867" />
         <tli id="T868" />
         <tli id="T869" />
         <tli id="T870" />
         <tli id="T871" />
         <tli id="T872" />
         <tli id="T873" />
         <tli id="T874" />
         <tli id="T875" />
         <tli id="T876" />
         <tli id="T877" />
         <tli id="T878" />
         <tli id="T879" />
         <tli id="T880" />
         <tli id="T881" />
         <tli id="T882" />
         <tli id="T883" />
         <tli id="T884" />
         <tli id="T885" />
         <tli id="T886" />
         <tli id="T887" />
         <tli id="T888" />
         <tli id="T889" />
         <tli id="T890" />
         <tli id="T891" />
         <tli id="T892" />
         <tli id="T893" />
         <tli id="T894" />
         <tli id="T895" />
         <tli id="T896" />
         <tli id="T897" />
         <tli id="T898" />
         <tli id="T899" />
         <tli id="T900" />
         <tli id="T901" />
         <tli id="T902" />
         <tli id="T903" />
         <tli id="T904" />
         <tli id="T905" />
         <tli id="T906" />
         <tli id="T907" />
         <tli id="T908" />
         <tli id="T909" />
         <tli id="T910" />
         <tli id="T911" />
         <tli id="T912" />
         <tli id="T913" />
         <tli id="T914" />
         <tli id="T915" />
         <tli id="T916" />
         <tli id="T917" />
         <tli id="T918" />
         <tli id="T919" />
         <tli id="T920" />
         <tli id="T921" />
         <tli id="T922" />
         <tli id="T923" />
         <tli id="T924" />
         <tli id="T925" />
         <tli id="T926" />
         <tli id="T927" />
         <tli id="T928" />
         <tli id="T929" />
         <tli id="T930" />
         <tli id="T931" />
         <tli id="T932" />
         <tli id="T933" />
         <tli id="T934" />
         <tli id="T935" />
         <tli id="T936" />
         <tli id="T937" />
         <tli id="T938" />
         <tli id="T939" />
         <tli id="T940" />
         <tli id="T941" />
         <tli id="T942" />
         <tli id="T943" />
         <tli id="T944" />
         <tli id="T945" />
         <tli id="T946" />
         <tli id="T947" />
         <tli id="T948" />
         <tli id="T949" />
         <tli id="T950" />
         <tli id="T951" />
         <tli id="T952" />
         <tli id="T953" />
         <tli id="T954" />
         <tli id="T955" />
         <tli id="T956" />
         <tli id="T957" />
         <tli id="T958" />
         <tli id="T959" />
         <tli id="T960" />
         <tli id="T961" />
         <tli id="T962" />
         <tli id="T963" />
         <tli id="T964" />
         <tli id="T965" />
         <tli id="T966" />
         <tli id="T967" />
         <tli id="T968" />
         <tli id="T969" />
         <tli id="T970" />
         <tli id="T971" />
         <tli id="T972" />
         <tli id="T973" />
         <tli id="T974" />
         <tli id="T975" />
         <tli id="T976" />
         <tli id="T977" />
         <tli id="T978" />
         <tli id="T979" />
         <tli id="T980" />
         <tli id="T981" />
         <tli id="T982" />
         <tli id="T983" />
         <tli id="T984" />
         <tli id="T985" />
         <tli id="T986" />
         <tli id="T987" />
         <tli id="T988" />
         <tli id="T989" />
         <tli id="T990" />
         <tli id="T991" />
         <tli id="T992" />
         <tli id="T993" />
         <tli id="T994" />
         <tli id="T995" />
         <tli id="T996" />
         <tli id="T997" />
         <tli id="T998" />
         <tli id="T999" />
         <tli id="T1000" />
         <tli id="T1001" />
         <tli id="T1002" />
         <tli id="T1003" />
         <tli id="T1004" />
         <tli id="T1005" />
         <tli id="T1006" />
         <tli id="T1007" />
         <tli id="T1008" />
         <tli id="T1009" />
         <tli id="T1010" />
         <tli id="T1011" />
         <tli id="T1012" />
         <tli id="T1013" />
         <tli id="T1014" />
         <tli id="T1015" />
         <tli id="T1016" />
         <tli id="T1017" />
         <tli id="T1018" />
         <tli id="T1019" />
         <tli id="T1020" />
         <tli id="T1021" />
         <tli id="T1022" />
         <tli id="T1023" />
         <tli id="T1024" />
         <tli id="T1025" />
         <tli id="T1026" />
         <tli id="T1027" />
         <tli id="T1028" />
         <tli id="T1029" />
         <tli id="T1030" />
         <tli id="T1031" />
         <tli id="T1032" />
         <tli id="T1033" />
         <tli id="T1034" />
         <tli id="T1035" />
         <tli id="T1036" />
         <tli id="T1037" />
         <tli id="T1038" />
         <tli id="T1039" />
         <tli id="T1040" />
         <tli id="T1041" />
         <tli id="T1042" />
         <tli id="T1043" />
         <tli id="T1044" />
         <tli id="T1045" />
         <tli id="T1046" />
         <tli id="T1047" />
         <tli id="T1048" />
         <tli id="T1049" />
         <tli id="T1050" />
         <tli id="T1051" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T1051" id="Seg_0" n="sc" s="T1">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Xrestjan</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">era</ts>
                  <nts id="Seg_8" n="HIAT:ip">.</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T6" id="Seg_11" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">Tʼelɨmnɨn</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">Xrʼestʼän</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">era</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_23" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">Täbnan</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">naːgur</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">iːt</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_35" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">Iːlat</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">wes</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">nädälattə</ts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_47" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">Okkɨr</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">iːt</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">qɨdan</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">surulle</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">palʼdükun</ts>
                  <nts id="Seg_62" n="HIAT:ip">,</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_65" n="HIAT:w" s="T17">warɣə</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_68" n="HIAT:w" s="T18">iːt</ts>
                  <nts id="Seg_69" n="HIAT:ip">.</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_72" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_74" n="HIAT:w" s="T19">A</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_77" n="HIAT:w" s="T20">sət</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_79" n="HIAT:ip">(</nts>
                  <ts e="T22" id="Seg_81" n="HIAT:w" s="T21">sədə</ts>
                  <nts id="Seg_82" n="HIAT:ip">)</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_85" n="HIAT:w" s="T22">iːt</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">skatʼinalaze</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">palʼdüqwaɣə</ts>
                  <nts id="Seg_92" n="HIAT:ip">,</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_95" n="HIAT:w" s="T25">sɨrla</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">warədattə</ts>
                  <nts id="Seg_99" n="HIAT:ip">,</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_102" n="HIAT:w" s="T27">kütdɨla</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_105" n="HIAT:w" s="T28">i</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_108" n="HIAT:w" s="T29">konɛrla</ts>
                  <nts id="Seg_109" n="HIAT:ip">.</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_112" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_114" n="HIAT:w" s="T30">I</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_117" n="HIAT:w" s="T31">pašnä</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_120" n="HIAT:w" s="T32">wardattə</ts>
                  <nts id="Seg_121" n="HIAT:ip">,</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_124" n="HIAT:w" s="T33">apsot</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_127" n="HIAT:w" s="T34">sejakudattə</ts>
                  <nts id="Seg_128" n="HIAT:ip">.</nts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_131" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_133" n="HIAT:w" s="T35">A</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_136" n="HIAT:w" s="T36">warɣɨ</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_139" n="HIAT:w" s="T37">iːt</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_142" n="HIAT:w" s="T38">kɨːdan</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_145" n="HIAT:w" s="T39">madʼöt</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_148" n="HIAT:w" s="T40">paldʼän</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_150" n="HIAT:ip">(</nts>
                  <ts e="T42" id="Seg_152" n="HIAT:w" s="T41">paldʼikun</ts>
                  <nts id="Seg_153" n="HIAT:ip">)</nts>
                  <nts id="Seg_154" n="HIAT:ip">.</nts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_157" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_159" n="HIAT:w" s="T42">Tebnan</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_162" n="HIAT:w" s="T43">neːt</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_165" n="HIAT:w" s="T44">tʼelɨmda</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_168" n="HIAT:w" s="T45">i</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_171" n="HIAT:w" s="T46">iːga</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_174" n="HIAT:w" s="T47">tʼelɨmda</ts>
                  <nts id="Seg_175" n="HIAT:ip">.</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_178" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_180" n="HIAT:w" s="T48">Tebɨstaɣə</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_183" n="HIAT:w" s="T49">warɣɨn</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_186" n="HIAT:w" s="T50">aːzuaɣə</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_188" n="HIAT:ip">(</nts>
                  <ts e="T52" id="Seg_190" n="HIAT:w" s="T51">aːzuaq</ts>
                  <nts id="Seg_191" n="HIAT:ip">)</nts>
                  <nts id="Seg_192" n="HIAT:ip">.</nts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_195" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_197" n="HIAT:w" s="T52">A</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_200" n="HIAT:w" s="T53">täp</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_203" n="HIAT:w" s="T54">natʼen</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_206" n="HIAT:w" s="T55">madʼöɣɨn</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_209" n="HIAT:w" s="T56">iːbɨɣaj</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_212" n="HIAT:w" s="T57">maːt</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_215" n="HIAT:w" s="T58">omdɨlǯɨmɨtdɨt</ts>
                  <nts id="Seg_216" n="HIAT:ip">,</nts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_219" n="HIAT:w" s="T59">iːbɨɣaj</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_222" n="HIAT:w" s="T60">maːt</ts>
                  <nts id="Seg_223" n="HIAT:ip">.</nts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_226" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_228" n="HIAT:w" s="T61">Äotdänä</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_231" n="HIAT:w" s="T62">äzätdänä</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_234" n="HIAT:w" s="T63">tʼärɨn</ts>
                  <nts id="Seg_235" n="HIAT:ip">:</nts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_237" n="HIAT:ip">“</nts>
                  <ts e="T65" id="Seg_239" n="HIAT:w" s="T64">Nomse</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_242" n="HIAT:w" s="T65">mazɨm</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_245" n="HIAT:w" s="T66">paslawikə</ts>
                  <nts id="Seg_246" n="HIAT:ip">.</nts>
                  <nts id="Seg_247" n="HIAT:ip">”</nts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_250" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_252" n="HIAT:w" s="T67">Äutdə</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_255" n="HIAT:w" s="T68">nomɨm</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_258" n="HIAT:w" s="T69">iːut</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_261" n="HIAT:w" s="T70">i</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_264" n="HIAT:w" s="T71">paslawinɨt</ts>
                  <nts id="Seg_265" n="HIAT:ip">.</nts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_268" n="HIAT:u" s="T72">
                  <nts id="Seg_269" n="HIAT:ip">“</nts>
                  <ts e="T73" id="Seg_271" n="HIAT:w" s="T72">Man</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_274" n="HIAT:w" s="T73">qwatǯan</ts>
                  <nts id="Seg_275" n="HIAT:ip">.</nts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_278" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_280" n="HIAT:w" s="T74">Naːgur</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_283" n="HIAT:w" s="T75">agazlaftə</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_286" n="HIAT:w" s="T76">okkɨr</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_289" n="HIAT:w" s="T77">maːtqɨn</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_292" n="HIAT:w" s="T78">ass</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_295" n="HIAT:w" s="T79">elʼetǯutu</ts>
                  <nts id="Seg_296" n="HIAT:ip">.</nts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_299" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_301" n="HIAT:w" s="T80">Manan</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_304" n="HIAT:w" s="T81">täper</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_307" n="HIAT:w" s="T82">sʼämjau</ts>
                  <nts id="Seg_308" n="HIAT:ip">.</nts>
                  <nts id="Seg_309" n="HIAT:ip">”</nts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_312" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_314" n="HIAT:w" s="T83">Saozniktə</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_317" n="HIAT:w" s="T84">wes</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_320" n="HIAT:w" s="T85">tädomɨmdə</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_323" n="HIAT:w" s="T86">tuɣunnɨt</ts>
                  <nts id="Seg_324" n="HIAT:ip">.</nts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_327" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_329" n="HIAT:w" s="T87">I</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_332" n="HIAT:w" s="T88">na</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_335" n="HIAT:w" s="T89">üːbɨtdattə</ts>
                  <nts id="Seg_336" n="HIAT:ip">.</nts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_339" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_341" n="HIAT:w" s="T90">Ästɨ</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_344" n="HIAT:w" s="T91">äudɨ</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_347" n="HIAT:w" s="T92">i</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_350" n="HIAT:w" s="T93">türlʼe</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_353" n="HIAT:w" s="T94">na</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_356" n="HIAT:w" s="T95">ɣalɨtdattə</ts>
                  <nts id="Seg_357" n="HIAT:ip">.</nts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_360" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_362" n="HIAT:w" s="T96">I</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_365" n="HIAT:w" s="T97">täbla</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_368" n="HIAT:w" s="T98">i</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_371" n="HIAT:w" s="T99">na</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_374" n="HIAT:w" s="T100">qwatdattə</ts>
                  <nts id="Seg_375" n="HIAT:ip">.</nts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_378" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_380" n="HIAT:w" s="T101">No</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_383" n="HIAT:w" s="T102">i</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_386" n="HIAT:w" s="T103">tüle</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_389" n="HIAT:w" s="T104">na</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_392" n="HIAT:w" s="T105">medɨtdattə</ts>
                  <nts id="Seg_393" n="HIAT:ip">.</nts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_396" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_398" n="HIAT:w" s="T106">A</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_401" n="HIAT:w" s="T107">tebnan</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_404" n="HIAT:w" s="T108">natʼen</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_407" n="HIAT:w" s="T109">iːbɨɣaj</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_410" n="HIAT:w" s="T110">maːt</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_413" n="HIAT:w" s="T111">omdəlǯəmɨdɨt</ts>
                  <nts id="Seg_414" n="HIAT:ip">.</nts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_417" n="HIAT:u" s="T112">
                  <ts e="T113" id="Seg_419" n="HIAT:w" s="T112">Wes</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_422" n="HIAT:w" s="T113">tuɣunnattə</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_425" n="HIAT:w" s="T114">natʼet</ts>
                  <nts id="Seg_426" n="HIAT:ip">.</nts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_429" n="HIAT:u" s="T115">
                  <ts e="T116" id="Seg_431" n="HIAT:w" s="T115">No</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_434" n="HIAT:w" s="T116">i</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_437" n="HIAT:w" s="T117">ärat</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_440" n="HIAT:w" s="T118">wes</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_443" n="HIAT:w" s="T119">melʼe</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_446" n="HIAT:w" s="T120">taːdərat</ts>
                  <nts id="Seg_447" n="HIAT:ip">,</nts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_450" n="HIAT:w" s="T121">qoptə</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_453" n="HIAT:w" s="T122">metdɨt</ts>
                  <nts id="Seg_454" n="HIAT:ip">.</nts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_457" n="HIAT:u" s="T123">
                  <nts id="Seg_458" n="HIAT:ip">“</nts>
                  <ts e="T124" id="Seg_460" n="HIAT:w" s="T123">Qardʼen</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_463" n="HIAT:w" s="T124">madʼöt</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_466" n="HIAT:w" s="T125">qwatǯan</ts>
                  <nts id="Seg_467" n="HIAT:ip">.</nts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_470" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_472" n="HIAT:w" s="T126">A</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_475" n="HIAT:w" s="T127">tan</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_478" n="HIAT:w" s="T128">üdno</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_481" n="HIAT:w" s="T129">na</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_484" n="HIAT:w" s="T130">qwannaš</ts>
                  <nts id="Seg_485" n="HIAT:ip">.</nts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T137" id="Seg_488" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_490" n="HIAT:w" s="T131">Toboɣät</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_493" n="HIAT:w" s="T132">qajam</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_496" n="HIAT:w" s="T133">struška</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_499" n="HIAT:w" s="T134">papalan</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_502" n="HIAT:w" s="T135">ig</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_505" n="HIAT:w" s="T136">azü</ts>
                  <nts id="Seg_506" n="HIAT:ip">.</nts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T140" id="Seg_509" n="HIAT:u" s="T137">
                  <ts e="T138" id="Seg_511" n="HIAT:w" s="T137">Ato</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_514" n="HIAT:w" s="T138">aːwan</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_517" n="HIAT:w" s="T139">jetǯan</ts>
                  <nts id="Seg_518" n="HIAT:ip">.</nts>
                  <nts id="Seg_519" n="HIAT:ip">”</nts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T142" id="Seg_522" n="HIAT:u" s="T140">
                  <ts e="T141" id="Seg_524" n="HIAT:w" s="T140">No</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_527" n="HIAT:w" s="T141">sekgattə</ts>
                  <nts id="Seg_528" n="HIAT:ip">.</nts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_531" n="HIAT:u" s="T142">
                  <ts e="T143" id="Seg_533" n="HIAT:w" s="T142">Qarʼemɨɣɨn</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_536" n="HIAT:w" s="T143">wazɨn</ts>
                  <nts id="Seg_537" n="HIAT:ip">,</nts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_540" n="HIAT:w" s="T144">aurnɨn</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_543" n="HIAT:w" s="T145">i</ts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_546" n="HIAT:w" s="T146">na</ts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_549" n="HIAT:w" s="T147">qwatda</ts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_552" n="HIAT:w" s="T148">madʼötdə</ts>
                  <nts id="Seg_553" n="HIAT:ip">.</nts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T162" id="Seg_556" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_558" n="HIAT:w" s="T149">A</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_561" n="HIAT:w" s="T150">näjɣum</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_564" n="HIAT:w" s="T151">tʼärɨn</ts>
                  <nts id="Seg_565" n="HIAT:ip">:</nts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_567" n="HIAT:ip">“</nts>
                  <ts e="T153" id="Seg_569" n="HIAT:w" s="T152">Qajno</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_572" n="HIAT:w" s="T153">mekga</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_575" n="HIAT:w" s="T154">ärau</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_578" n="HIAT:w" s="T155">tʼärɨn</ts>
                  <nts id="Seg_579" n="HIAT:ip">,</nts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_582" n="HIAT:w" s="T156">što</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_585" n="HIAT:w" s="T157">struška</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_588" n="HIAT:w" s="T158">üttə</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_591" n="HIAT:w" s="T159">papalan</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_594" n="HIAT:w" s="T160">ik</ts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_597" n="HIAT:w" s="T161">azə</ts>
                  <nts id="Seg_598" n="HIAT:ip">.</nts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T172" id="Seg_601" n="HIAT:u" s="T162">
                  <ts e="T163" id="Seg_603" n="HIAT:w" s="T162">Man</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_606" n="HIAT:w" s="T163">sičas</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_609" n="HIAT:w" s="T164">sət</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_612" n="HIAT:w" s="T165">wedram</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_615" n="HIAT:w" s="T166">struškalaze</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_618" n="HIAT:w" s="T167">nabineǯau</ts>
                  <nts id="Seg_619" n="HIAT:ip">,</nts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_622" n="HIAT:w" s="T168">nabinäǯau</ts>
                  <nts id="Seg_623" n="HIAT:ip">,</nts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_626" n="HIAT:w" s="T169">qwalʼlʼe</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_629" n="HIAT:w" s="T170">üttə</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_632" n="HIAT:w" s="T171">qamnätǯau</ts>
                  <nts id="Seg_633" n="HIAT:ip">.</nts>
                  <nts id="Seg_634" n="HIAT:ip">”</nts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T177" id="Seg_637" n="HIAT:u" s="T172">
                  <ts e="T173" id="Seg_639" n="HIAT:w" s="T172">I</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_642" n="HIAT:w" s="T173">na</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_645" n="HIAT:w" s="T174">qwannɨdɨt</ts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_648" n="HIAT:w" s="T175">i</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_651" n="HIAT:w" s="T176">qamǯɨt</ts>
                  <nts id="Seg_652" n="HIAT:ip">.</nts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T180" id="Seg_655" n="HIAT:u" s="T177">
                  <ts e="T178" id="Seg_657" n="HIAT:w" s="T177">Nɨkga</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_660" n="HIAT:w" s="T178">i</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_663" n="HIAT:w" s="T179">mannɨpaːt</ts>
                  <nts id="Seg_664" n="HIAT:ip">.</nts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T183" id="Seg_667" n="HIAT:u" s="T180">
                  <ts e="T181" id="Seg_669" n="HIAT:w" s="T180">A</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_672" n="HIAT:w" s="T181">natʼen</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_675" n="HIAT:w" s="T182">köunɨn</ts>
                  <nts id="Seg_676" n="HIAT:ip">.</nts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T189" id="Seg_679" n="HIAT:u" s="T183">
                  <ts e="T184" id="Seg_681" n="HIAT:w" s="T183">Täp</ts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_684" n="HIAT:w" s="T184">üdɨm</ts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_687" n="HIAT:w" s="T185">soɣunnɨt</ts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_690" n="HIAT:w" s="T186">i</ts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_693" n="HIAT:w" s="T187">maːttɨ</ts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_696" n="HIAT:w" s="T188">tüa</ts>
                  <nts id="Seg_697" n="HIAT:ip">.</nts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T194" id="Seg_700" n="HIAT:u" s="T189">
                  <ts e="T190" id="Seg_702" n="HIAT:w" s="T189">A</ts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_705" n="HIAT:w" s="T190">täp</ts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_708" n="HIAT:w" s="T191">krasiwa</ts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_711" n="HIAT:w" s="T192">näjɣum</ts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_714" n="HIAT:w" s="T193">jes</ts>
                  <nts id="Seg_715" n="HIAT:ip">.</nts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T201" id="Seg_718" n="HIAT:u" s="T194">
                  <ts e="T195" id="Seg_720" n="HIAT:w" s="T194">Qwalʼlʼe</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_723" n="HIAT:w" s="T195">sütʼdinäj</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_726" n="HIAT:w" s="T196">maːttə</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_729" n="HIAT:w" s="T197">omdɨn</ts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_732" n="HIAT:w" s="T198">i</ts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_735" n="HIAT:w" s="T199">südɨrlʼe</ts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_738" n="HIAT:w" s="T200">übɨran</ts>
                  <nts id="Seg_739" n="HIAT:ip">.</nts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T205" id="Seg_742" n="HIAT:u" s="T201">
                  <ts e="T202" id="Seg_744" n="HIAT:w" s="T201">A</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_747" n="HIAT:w" s="T202">sət</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_750" n="HIAT:w" s="T203">qɨbanʼaʒa</ts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_753" n="HIAT:w" s="T204">warkaɣə</ts>
                  <nts id="Seg_754" n="HIAT:ip">.</nts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T215" id="Seg_757" n="HIAT:u" s="T205">
                  <ts e="T206" id="Seg_759" n="HIAT:w" s="T205">Razbojnigla</ts>
                  <nts id="Seg_760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_762" n="HIAT:w" s="T206">čaʒatdə</ts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_765" n="HIAT:w" s="T207">atdɨzʼe</ts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_768" n="HIAT:w" s="T208">i</ts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_771" n="HIAT:w" s="T209">tʼärattə</ts>
                  <nts id="Seg_772" n="HIAT:ip">:</nts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_774" n="HIAT:ip">“</nts>
                  <ts e="T211" id="Seg_776" n="HIAT:w" s="T210">Qaj</ts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_779" n="HIAT:w" s="T211">struškala</ts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_781" n="HIAT:ip">(</nts>
                  <ts e="T213" id="Seg_783" n="HIAT:w" s="T212">köudattə</ts>
                  <nts id="Seg_784" n="HIAT:ip">)</nts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_787" n="HIAT:w" s="T213">küuze</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_790" n="HIAT:w" s="T214">čaːʒɨdattə</ts>
                  <nts id="Seg_791" n="HIAT:ip">.</nts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T218" id="Seg_794" n="HIAT:u" s="T215">
                  <ts e="T216" id="Seg_796" n="HIAT:w" s="T215">Qən</ts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_799" n="HIAT:w" s="T216">barɨnʼä</ts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_802" n="HIAT:w" s="T217">praibet</ts>
                  <nts id="Seg_803" n="HIAT:ip">!</nts>
                  <nts id="Seg_804" n="HIAT:ip">”</nts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T222" id="Seg_807" n="HIAT:u" s="T218">
                  <ts e="T219" id="Seg_809" n="HIAT:w" s="T218">Tebla</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_812" n="HIAT:w" s="T219">mannɨpattə</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_815" n="HIAT:w" s="T220">qajda</ts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_818" n="HIAT:w" s="T221">wattɨ</ts>
                  <nts id="Seg_819" n="HIAT:ip">.</nts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T225" id="Seg_822" n="HIAT:u" s="T222">
                  <ts e="T223" id="Seg_824" n="HIAT:w" s="T222">Täbla</ts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_827" n="HIAT:w" s="T223">wattɨga</ts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_830" n="HIAT:w" s="T224">kotdattə</ts>
                  <nts id="Seg_831" n="HIAT:ip">.</nts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T231" id="Seg_834" n="HIAT:u" s="T225">
                  <nts id="Seg_835" n="HIAT:ip">“</nts>
                  <ts e="T226" id="Seg_837" n="HIAT:w" s="T225">Ugon</ts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_840" n="HIAT:w" s="T226">me</ts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_843" n="HIAT:w" s="T227">tau</ts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_846" n="HIAT:w" s="T228">wattɨm</ts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_849" n="HIAT:w" s="T229">jass</ts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_852" n="HIAT:w" s="T230">qoǯɨrguzautə</ts>
                  <nts id="Seg_853" n="HIAT:ip">.</nts>
                  <nts id="Seg_854" n="HIAT:ip">”</nts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T239" id="Seg_857" n="HIAT:u" s="T231">
                  <ts e="T232" id="Seg_859" n="HIAT:w" s="T231">Na</ts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_862" n="HIAT:w" s="T232">wattogatdə</ts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_864" n="HIAT:ip">(</nts>
                  <ts e="T234" id="Seg_866" n="HIAT:w" s="T233">na</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_869" n="HIAT:w" s="T234">watotdə</ts>
                  <nts id="Seg_870" n="HIAT:ip">,</nts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_873" n="HIAT:w" s="T235">na</ts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_876" n="HIAT:w" s="T236">watotdə</ts>
                  <nts id="Seg_877" n="HIAT:ip">)</nts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_880" n="HIAT:w" s="T237">i</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_883" n="HIAT:w" s="T238">uːdurattə</ts>
                  <nts id="Seg_884" n="HIAT:ip">.</nts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T251" id="Seg_887" n="HIAT:u" s="T239">
                  <ts e="T240" id="Seg_889" n="HIAT:w" s="T239">Attaman</ts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_892" n="HIAT:w" s="T240">tʼärɨn</ts>
                  <nts id="Seg_893" n="HIAT:ip">:</nts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_895" n="HIAT:ip">“</nts>
                  <ts e="T242" id="Seg_897" n="HIAT:w" s="T241">Tä</ts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_900" n="HIAT:w" s="T242">aːmdaltə</ts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_903" n="HIAT:w" s="T243">tɨtdɨn</ts>
                  <nts id="Seg_904" n="HIAT:ip">,</nts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_907" n="HIAT:w" s="T244">a</ts>
                  <nts id="Seg_908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_910" n="HIAT:w" s="T245">man</ts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_913" n="HIAT:w" s="T246">qurollan</ts>
                  <nts id="Seg_914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_916" n="HIAT:w" s="T247">mannɨbɨlʼeu</ts>
                  <nts id="Seg_917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_919" n="HIAT:w" s="T248">qaj</ts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_922" n="HIAT:w" s="T249">ilatda</ts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_925" n="HIAT:w" s="T250">tɨtdɨn</ts>
                  <nts id="Seg_926" n="HIAT:ip">.</nts>
                  <nts id="Seg_927" n="HIAT:ip">”</nts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T253" id="Seg_930" n="HIAT:u" s="T251">
                  <ts e="T252" id="Seg_932" n="HIAT:w" s="T251">Na</ts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_935" n="HIAT:w" s="T252">qurolǯa</ts>
                  <nts id="Seg_936" n="HIAT:ip">.</nts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T256" id="Seg_939" n="HIAT:u" s="T253">
                  <ts e="T254" id="Seg_941" n="HIAT:w" s="T253">Tüa</ts>
                  <nts id="Seg_942" n="HIAT:ip">,</nts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_945" n="HIAT:w" s="T254">sernɨn</ts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_948" n="HIAT:w" s="T255">maːttə</ts>
                  <nts id="Seg_949" n="HIAT:ip">.</nts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T257" id="Seg_952" n="HIAT:u" s="T256">
                  <nts id="Seg_953" n="HIAT:ip">“</nts>
                  <ts e="T257" id="Seg_955" n="HIAT:w" s="T256">Tʼolom</ts>
                  <nts id="Seg_956" n="HIAT:ip">.</nts>
                  <nts id="Seg_957" n="HIAT:ip">”</nts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T261" id="Seg_960" n="HIAT:u" s="T257">
                  <ts e="T258" id="Seg_962" n="HIAT:w" s="T257">Näjɣum</ts>
                  <nts id="Seg_963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_965" n="HIAT:w" s="T258">tʼärɨn</ts>
                  <nts id="Seg_966" n="HIAT:ip">:</nts>
                  <nts id="Seg_967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_968" n="HIAT:ip">“</nts>
                  <ts e="T260" id="Seg_970" n="HIAT:w" s="T259">Tʼolom</ts>
                  <nts id="Seg_971" n="HIAT:ip">,</nts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_974" n="HIAT:w" s="T260">tʼolom</ts>
                  <nts id="Seg_975" n="HIAT:ip">.</nts>
                  <nts id="Seg_976" n="HIAT:ip">”</nts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T264" id="Seg_979" n="HIAT:u" s="T261">
                  <ts e="T262" id="Seg_981" n="HIAT:w" s="T261">Stul</ts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_984" n="HIAT:w" s="T262">čečelʼe</ts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_987" n="HIAT:w" s="T263">meʒalǯɨt</ts>
                  <nts id="Seg_988" n="HIAT:ip">.</nts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T267" id="Seg_991" n="HIAT:u" s="T264">
                  <ts e="T265" id="Seg_993" n="HIAT:w" s="T264">Tʼärɨn</ts>
                  <nts id="Seg_994" n="HIAT:ip">:</nts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_996" n="HIAT:ip">“</nts>
                  <ts e="T266" id="Seg_998" n="HIAT:w" s="T265">Omdak</ts>
                  <nts id="Seg_999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1001" n="HIAT:w" s="T266">tɨkga</ts>
                  <nts id="Seg_1002" n="HIAT:ip">.</nts>
                  <nts id="Seg_1003" n="HIAT:ip">”</nts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T270" id="Seg_1006" n="HIAT:u" s="T267">
                  <ts e="T268" id="Seg_1008" n="HIAT:w" s="T267">A</ts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1011" n="HIAT:w" s="T268">täp</ts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1014" n="HIAT:w" s="T269">omdɨn</ts>
                  <nts id="Seg_1015" n="HIAT:ip">.</nts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T273" id="Seg_1018" n="HIAT:u" s="T270">
                  <ts e="T271" id="Seg_1020" n="HIAT:w" s="T270">Kulupbɨlʼe</ts>
                  <nts id="Seg_1021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1023" n="HIAT:w" s="T271">na</ts>
                  <nts id="Seg_1024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1026" n="HIAT:w" s="T272">übɨrɨtdaɣ</ts>
                  <nts id="Seg_1027" n="HIAT:ip">.</nts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T278" id="Seg_1030" n="HIAT:u" s="T273">
                  <ts e="T274" id="Seg_1032" n="HIAT:w" s="T273">Ataman</ts>
                  <nts id="Seg_1033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1035" n="HIAT:w" s="T274">tʼärɨn</ts>
                  <nts id="Seg_1036" n="HIAT:ip">:</nts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1038" n="HIAT:ip">“</nts>
                  <ts e="T276" id="Seg_1040" n="HIAT:w" s="T275">Tan</ts>
                  <nts id="Seg_1041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1043" n="HIAT:w" s="T276">mekga</ts>
                  <nts id="Seg_1044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1046" n="HIAT:w" s="T277">tɨbɨtdɨlʼet</ts>
                  <nts id="Seg_1047" n="HIAT:ip">.</nts>
                  <nts id="Seg_1048" n="HIAT:ip">”</nts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T282" id="Seg_1051" n="HIAT:u" s="T278">
                  <nts id="Seg_1052" n="HIAT:ip">“</nts>
                  <ts e="T279" id="Seg_1054" n="HIAT:w" s="T278">No</ts>
                  <nts id="Seg_1055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1057" n="HIAT:w" s="T279">mannan</ts>
                  <nts id="Seg_1058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1060" n="HIAT:w" s="T280">ärau</ts>
                  <nts id="Seg_1061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1063" n="HIAT:w" s="T281">orse</ts>
                  <nts id="Seg_1064" n="HIAT:ip">.</nts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T288" id="Seg_1067" n="HIAT:u" s="T282">
                  <ts e="T283" id="Seg_1069" n="HIAT:w" s="T282">Täp</ts>
                  <nts id="Seg_1070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1072" n="HIAT:w" s="T283">tastɨ</ts>
                  <nts id="Seg_1073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1075" n="HIAT:w" s="T284">qwačɨt</ts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1078" n="HIAT:w" s="T285">i</ts>
                  <nts id="Seg_1079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1081" n="HIAT:w" s="T286">mazɨm</ts>
                  <nts id="Seg_1082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1084" n="HIAT:w" s="T287">qwačɨt</ts>
                  <nts id="Seg_1085" n="HIAT:ip">.</nts>
                  <nts id="Seg_1086" n="HIAT:ip">”</nts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T293" id="Seg_1089" n="HIAT:u" s="T288">
                  <nts id="Seg_1090" n="HIAT:ip">“</nts>
                  <ts e="T289" id="Seg_1092" n="HIAT:w" s="T288">A</ts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1095" n="HIAT:w" s="T289">man</ts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1098" n="HIAT:w" s="T290">tekga</ts>
                  <nts id="Seg_1099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1101" n="HIAT:w" s="T291">qudugo</ts>
                  <nts id="Seg_1102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1104" n="HIAT:w" s="T292">mennɨǯan</ts>
                  <nts id="Seg_1105" n="HIAT:ip">.</nts>
                  <nts id="Seg_1106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T303" id="Seg_1108" n="HIAT:u" s="T293">
                  <nts id="Seg_1109" n="HIAT:ip">(</nts>
                  <ts e="T294" id="Seg_1111" n="HIAT:w" s="T293">Optə</ts>
                  <nts id="Seg_1112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1114" n="HIAT:w" s="T294">jennɨš</ts>
                  <nts id="Seg_1115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1117" n="HIAT:w" s="T295">i</ts>
                  <nts id="Seg_1118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1120" n="HIAT:w" s="T296">sɨnkowa</ts>
                  <nts id="Seg_1121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1123" n="HIAT:w" s="T297">prowoloka</ts>
                  <nts id="Seg_1124" n="HIAT:ip">,</nts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1127" n="HIAT:w" s="T298">täp</ts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1130" n="HIAT:w" s="T299">tau</ts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1133" n="HIAT:w" s="T300">kudɨgom</ts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1136" n="HIAT:w" s="T301">as</ts>
                  <nts id="Seg_1137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1139" n="HIAT:w" s="T302">lakčeǯit</ts>
                  <nts id="Seg_1140" n="HIAT:ip">)</nts>
                  <nts id="Seg_1141" n="HIAT:ip">.</nts>
                  <nts id="Seg_1142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T307" id="Seg_1144" n="HIAT:u" s="T303">
                  <ts e="T304" id="Seg_1146" n="HIAT:w" s="T303">Täp</ts>
                  <nts id="Seg_1147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1149" n="HIAT:w" s="T304">na</ts>
                  <nts id="Seg_1150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1152" n="HIAT:w" s="T305">tünnɨš</ts>
                  <nts id="Seg_1153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1155" n="HIAT:w" s="T306">üdomɨɣɨn</ts>
                  <nts id="Seg_1156" n="HIAT:ip">.</nts>
                  <nts id="Seg_1157" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T310" id="Seg_1159" n="HIAT:u" s="T307">
                  <ts e="T308" id="Seg_1161" n="HIAT:w" s="T307">Tan</ts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1164" n="HIAT:w" s="T308">tʼärak</ts>
                  <nts id="Seg_1165" n="HIAT:ip">:</nts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1167" n="HIAT:ip">“</nts>
                  <ts e="T310" id="Seg_1169" n="HIAT:w" s="T309">Qozɨrčɨlaj</ts>
                  <nts id="Seg_1170" n="HIAT:ip">.</nts>
                  <nts id="Seg_1171" n="HIAT:ip">”</nts>
                  <nts id="Seg_1172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T318" id="Seg_1174" n="HIAT:u" s="T310">
                  <ts e="T311" id="Seg_1176" n="HIAT:w" s="T310">Tabnä</ts>
                  <nts id="Seg_1177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1179" n="HIAT:w" s="T311">tʼärak</ts>
                  <nts id="Seg_1180" n="HIAT:ip">:</nts>
                  <nts id="Seg_1181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1182" n="HIAT:ip">“</nts>
                  <ts e="T313" id="Seg_1184" n="HIAT:w" s="T312">Qut</ts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1187" n="HIAT:w" s="T313">turakɨn</ts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1190" n="HIAT:w" s="T314">qalʼeǯɨn</ts>
                  <nts id="Seg_1191" n="HIAT:ip">,</nts>
                  <nts id="Seg_1192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1194" n="HIAT:w" s="T315">udlamdə</ts>
                  <nts id="Seg_1195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1197" n="HIAT:w" s="T316">moɣunä</ts>
                  <nts id="Seg_1198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1200" n="HIAT:w" s="T317">saruku</ts>
                  <nts id="Seg_1201" n="HIAT:ip">.</nts>
                  <nts id="Seg_1202" n="HIAT:ip">”</nts>
                  <nts id="Seg_1203" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T326" id="Seg_1205" n="HIAT:u" s="T318">
                  <ts e="T319" id="Seg_1207" n="HIAT:w" s="T318">A</ts>
                  <nts id="Seg_1208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1210" n="HIAT:w" s="T319">tan</ts>
                  <nts id="Seg_1211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1213" n="HIAT:w" s="T320">täbɨm</ts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1216" n="HIAT:w" s="T321">turakɨn</ts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1219" n="HIAT:w" s="T322">qwäzʼet</ts>
                  <nts id="Seg_1220" n="HIAT:ip">,</nts>
                  <nts id="Seg_1221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1223" n="HIAT:w" s="T323">udlamdə</ts>
                  <nts id="Seg_1224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1226" n="HIAT:w" s="T324">moɣunä</ts>
                  <nts id="Seg_1227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1229" n="HIAT:w" s="T325">saːrʼet</ts>
                  <nts id="Seg_1230" n="HIAT:ip">.</nts>
                  <nts id="Seg_1231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T331" id="Seg_1233" n="HIAT:u" s="T326">
                  <ts e="T327" id="Seg_1235" n="HIAT:w" s="T326">Täp</ts>
                  <nts id="Seg_1236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1238" n="HIAT:w" s="T327">tau</ts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1241" n="HIAT:w" s="T328">qudugom</ts>
                  <nts id="Seg_1242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1244" n="HIAT:w" s="T329">as</ts>
                  <nts id="Seg_1245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1247" n="HIAT:w" s="T330">lakčeǯin</ts>
                  <nts id="Seg_1248" n="HIAT:ip">.</nts>
                  <nts id="Seg_1249" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T341" id="Seg_1251" n="HIAT:u" s="T331">
                  <ts e="T332" id="Seg_1253" n="HIAT:w" s="T331">Täp</ts>
                  <nts id="Seg_1254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1256" n="HIAT:w" s="T332">jeʒlʼi</ts>
                  <nts id="Seg_1257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1259" n="HIAT:w" s="T333">ass</ts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1262" n="HIAT:w" s="T334">laqčɨt</ts>
                  <nts id="Seg_1263" n="HIAT:ip">,</nts>
                  <nts id="Seg_1264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1266" n="HIAT:w" s="T335">a</ts>
                  <nts id="Seg_1267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1269" n="HIAT:w" s="T336">tan</ts>
                  <nts id="Seg_1270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1272" n="HIAT:w" s="T337">ponä</ts>
                  <nts id="Seg_1273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1275" n="HIAT:w" s="T338">čatǯak</ts>
                  <nts id="Seg_1276" n="HIAT:ip">,</nts>
                  <nts id="Seg_1277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1279" n="HIAT:w" s="T339">mazɨm</ts>
                  <nts id="Seg_1280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1282" n="HIAT:w" s="T340">lakgolʼgə</ts>
                  <nts id="Seg_1283" n="HIAT:ip">.</nts>
                  <nts id="Seg_1284" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T346" id="Seg_1286" n="HIAT:u" s="T341">
                  <ts e="T342" id="Seg_1288" n="HIAT:w" s="T341">Man</ts>
                  <nts id="Seg_1289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1291" n="HIAT:w" s="T342">tütǯan</ts>
                  <nts id="Seg_1292" n="HIAT:ip">,</nts>
                  <nts id="Seg_1293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1295" n="HIAT:w" s="T343">täbɨm</ts>
                  <nts id="Seg_1296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1298" n="HIAT:w" s="T344">i</ts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1301" n="HIAT:w" s="T345">qwatčau</ts>
                  <nts id="Seg_1302" n="HIAT:ip">.</nts>
                  <nts id="Seg_1303" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T348" id="Seg_1305" n="HIAT:u" s="T346">
                  <ts e="T347" id="Seg_1307" n="HIAT:w" s="T346">Taze</ts>
                  <nts id="Seg_1308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1310" n="HIAT:w" s="T347">ilʼetǯaj</ts>
                  <nts id="Seg_1311" n="HIAT:ip">.</nts>
                  <nts id="Seg_1312" n="HIAT:ip">”</nts>
                  <nts id="Seg_1313" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T350" id="Seg_1315" n="HIAT:u" s="T348">
                  <ts e="T349" id="Seg_1317" n="HIAT:w" s="T348">Ärat</ts>
                  <nts id="Seg_1318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1320" n="HIAT:w" s="T349">tüa</ts>
                  <nts id="Seg_1321" n="HIAT:ip">.</nts>
                  <nts id="Seg_1322" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T355" id="Seg_1324" n="HIAT:u" s="T350">
                  <ts e="T351" id="Seg_1326" n="HIAT:w" s="T350">Täp</ts>
                  <nts id="Seg_1327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1329" n="HIAT:w" s="T351">täbɨm</ts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1332" n="HIAT:w" s="T352">apstɨt</ts>
                  <nts id="Seg_1333" n="HIAT:ip">,</nts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1336" n="HIAT:w" s="T353">tʼärɨn</ts>
                  <nts id="Seg_1337" n="HIAT:ip">:</nts>
                  <nts id="Seg_1338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1339" n="HIAT:ip">“</nts>
                  <ts e="T355" id="Seg_1341" n="HIAT:w" s="T354">Qozɨrčɨlaj</ts>
                  <nts id="Seg_1342" n="HIAT:ip">.</nts>
                  <nts id="Seg_1343" n="HIAT:ip">”</nts>
                  <nts id="Seg_1344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T361" id="Seg_1346" n="HIAT:u" s="T355">
                  <ts e="T356" id="Seg_1348" n="HIAT:w" s="T355">Ärat</ts>
                  <nts id="Seg_1349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1351" n="HIAT:w" s="T356">tʼärɨn</ts>
                  <nts id="Seg_1352" n="HIAT:ip">:</nts>
                  <nts id="Seg_1353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1354" n="HIAT:ip">“</nts>
                  <ts e="T358" id="Seg_1356" n="HIAT:w" s="T357">Man</ts>
                  <nts id="Seg_1357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1359" n="HIAT:w" s="T358">nunɨtʼipban</ts>
                  <nts id="Seg_1360" n="HIAT:ip">,</nts>
                  <nts id="Seg_1361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1363" n="HIAT:w" s="T359">qotdugu</ts>
                  <nts id="Seg_1364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1366" n="HIAT:w" s="T360">nadə</ts>
                  <nts id="Seg_1367" n="HIAT:ip">.</nts>
                  <nts id="Seg_1368" n="HIAT:ip">”</nts>
                  <nts id="Seg_1369" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T366" id="Seg_1371" n="HIAT:u" s="T361">
                  <nts id="Seg_1372" n="HIAT:ip">“</nts>
                  <ts e="T362" id="Seg_1374" n="HIAT:w" s="T361">Nu</ts>
                  <nts id="Seg_1375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1377" n="HIAT:w" s="T362">dawaj</ts>
                  <nts id="Seg_1378" n="HIAT:ip">,</nts>
                  <nts id="Seg_1379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1381" n="HIAT:w" s="T363">qozɨrčɨlaj</ts>
                  <nts id="Seg_1382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1384" n="HIAT:w" s="T364">ass</ts>
                  <nts id="Seg_1385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1387" n="HIAT:w" s="T365">qutdɨn</ts>
                  <nts id="Seg_1388" n="HIAT:ip">.</nts>
                  <nts id="Seg_1389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T373" id="Seg_1391" n="HIAT:u" s="T366">
                  <ts e="T367" id="Seg_1393" n="HIAT:w" s="T366">Ato</ts>
                  <nts id="Seg_1394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1396" n="HIAT:w" s="T367">onän</ts>
                  <nts id="Seg_1397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1399" n="HIAT:w" s="T368">amdan</ts>
                  <nts id="Seg_1400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1402" n="HIAT:w" s="T369">i</ts>
                  <nts id="Seg_1403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1405" n="HIAT:w" s="T370">skušnan</ts>
                  <nts id="Seg_1406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1408" n="HIAT:w" s="T371">mekga</ts>
                  <nts id="Seg_1409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1411" n="HIAT:w" s="T372">jen</ts>
                  <nts id="Seg_1412" n="HIAT:ip">.</nts>
                  <nts id="Seg_1413" n="HIAT:ip">”</nts>
                  <nts id="Seg_1414" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T377" id="Seg_1416" n="HIAT:u" s="T373">
                  <ts e="T374" id="Seg_1418" n="HIAT:w" s="T373">Nu</ts>
                  <nts id="Seg_1419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1421" n="HIAT:w" s="T374">i</ts>
                  <nts id="Seg_1422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1424" n="HIAT:w" s="T375">omdaɣə</ts>
                  <nts id="Seg_1425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1427" n="HIAT:w" s="T376">qozɨrčɨlʼe</ts>
                  <nts id="Seg_1428" n="HIAT:ip">.</nts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T383" id="Seg_1431" n="HIAT:u" s="T377">
                  <ts e="T378" id="Seg_1433" n="HIAT:w" s="T377">Qut</ts>
                  <nts id="Seg_1434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1436" n="HIAT:w" s="T378">turaqɨn</ts>
                  <nts id="Seg_1437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1439" n="HIAT:w" s="T379">qalʼeǯɨn</ts>
                  <nts id="Seg_1440" n="HIAT:ip">,</nts>
                  <nts id="Seg_1441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1443" n="HIAT:w" s="T380">udlamdə</ts>
                  <nts id="Seg_1444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1446" n="HIAT:w" s="T381">sarelǯugu</ts>
                  <nts id="Seg_1447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1449" n="HIAT:w" s="T382">moɣunä</ts>
                  <nts id="Seg_1450" n="HIAT:ip">.</nts>
                  <nts id="Seg_1451" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T386" id="Seg_1453" n="HIAT:u" s="T383">
                  <ts e="T384" id="Seg_1455" n="HIAT:w" s="T383">Ärat</ts>
                  <nts id="Seg_1456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1458" n="HIAT:w" s="T384">turaqɨn</ts>
                  <nts id="Seg_1459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1461" n="HIAT:w" s="T385">qalɨn</ts>
                  <nts id="Seg_1462" n="HIAT:ip">.</nts>
                  <nts id="Seg_1463" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T393" id="Seg_1465" n="HIAT:u" s="T386">
                  <nts id="Seg_1466" n="HIAT:ip">“</nts>
                  <ts e="T387" id="Seg_1468" n="HIAT:w" s="T386">Udlamdə</ts>
                  <nts id="Seg_1469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1471" n="HIAT:w" s="T387">nadə</ts>
                  <nts id="Seg_1472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1474" n="HIAT:w" s="T388">sarelǯugu</ts>
                  <nts id="Seg_1475" n="HIAT:ip">,</nts>
                  <nts id="Seg_1476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1478" n="HIAT:w" s="T389">udomdə</ts>
                  <nts id="Seg_1479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1481" n="HIAT:w" s="T390">nadə</ts>
                  <nts id="Seg_1482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1484" n="HIAT:w" s="T391">sarugu</ts>
                  <nts id="Seg_1485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1487" n="HIAT:w" s="T392">moɣunä</ts>
                  <nts id="Seg_1488" n="HIAT:ip">.</nts>
                  <nts id="Seg_1489" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T403" id="Seg_1491" n="HIAT:u" s="T393">
                  <ts e="T394" id="Seg_1493" n="HIAT:w" s="T393">A</ts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1496" n="HIAT:w" s="T394">jeʒlʼe</ts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1499" n="HIAT:w" s="T395">man</ts>
                  <nts id="Seg_1500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1502" n="HIAT:w" s="T396">qalɨnän</ts>
                  <nts id="Seg_1503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1505" n="HIAT:w" s="T397">turaqɨn</ts>
                  <nts id="Seg_1506" n="HIAT:ip">,</nts>
                  <nts id="Seg_1507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1509" n="HIAT:w" s="T398">tan</ts>
                  <nts id="Seg_1510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1512" n="HIAT:w" s="T399">bə</ts>
                  <nts id="Seg_1513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1515" n="HIAT:w" s="T400">mekga</ts>
                  <nts id="Seg_1516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1518" n="HIAT:w" s="T401">udlau</ts>
                  <nts id="Seg_1519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1521" n="HIAT:w" s="T402">zarelǯɨnäl</ts>
                  <nts id="Seg_1522" n="HIAT:ip">.</nts>
                  <nts id="Seg_1523" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T409" id="Seg_1525" n="HIAT:u" s="T403">
                  <ts e="T404" id="Seg_1527" n="HIAT:w" s="T403">A</ts>
                  <nts id="Seg_1528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1530" n="HIAT:w" s="T404">täper</ts>
                  <nts id="Seg_1531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1533" n="HIAT:w" s="T405">man</ts>
                  <nts id="Seg_1534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1536" n="HIAT:w" s="T406">tʼekga</ts>
                  <nts id="Seg_1537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1539" n="HIAT:w" s="T407">udomdə</ts>
                  <nts id="Seg_1540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1542" n="HIAT:w" s="T408">saretǯau</ts>
                  <nts id="Seg_1543" n="HIAT:ip">.</nts>
                  <nts id="Seg_1544" n="HIAT:ip">”</nts>
                  <nts id="Seg_1545" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T412" id="Seg_1547" n="HIAT:u" s="T409">
                  <ts e="T410" id="Seg_1549" n="HIAT:w" s="T409">Ärat</ts>
                  <nts id="Seg_1550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1552" n="HIAT:w" s="T410">tʼärɨn</ts>
                  <nts id="Seg_1553" n="HIAT:ip">:</nts>
                  <nts id="Seg_1554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1555" n="HIAT:ip">“</nts>
                  <ts e="T412" id="Seg_1557" n="HIAT:w" s="T411">Sarät</ts>
                  <nts id="Seg_1558" n="HIAT:ip">”</nts>
                  <nts id="Seg_1559" n="HIAT:ip">.</nts>
                  <nts id="Seg_1560" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T414" id="Seg_1562" n="HIAT:u" s="T412">
                  <ts e="T413" id="Seg_1564" n="HIAT:w" s="T412">Täp</ts>
                  <nts id="Seg_1565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1567" n="HIAT:w" s="T413">saːrät</ts>
                  <nts id="Seg_1568" n="HIAT:ip">.</nts>
                  <nts id="Seg_1569" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T420" id="Seg_1571" n="HIAT:u" s="T414">
                  <ts e="T415" id="Seg_1573" n="HIAT:w" s="T414">Ärat</ts>
                  <nts id="Seg_1574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1576" n="HIAT:w" s="T415">kak</ts>
                  <nts id="Seg_1577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1579" n="HIAT:w" s="T416">turugulǯɨt</ts>
                  <nts id="Seg_1580" n="HIAT:ip">,</nts>
                  <nts id="Seg_1581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1583" n="HIAT:w" s="T417">qudɨgola</ts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1586" n="HIAT:w" s="T418">wes</ts>
                  <nts id="Seg_1587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1589" n="HIAT:w" s="T419">laqčelattə</ts>
                  <nts id="Seg_1590" n="HIAT:ip">.</nts>
                  <nts id="Seg_1591" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T424" id="Seg_1593" n="HIAT:u" s="T420">
                  <ts e="T421" id="Seg_1595" n="HIAT:w" s="T420">No</ts>
                  <nts id="Seg_1596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1598" n="HIAT:w" s="T421">i</ts>
                  <nts id="Seg_1599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1601" n="HIAT:w" s="T422">qotdɨgu</ts>
                  <nts id="Seg_1602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1604" n="HIAT:w" s="T423">quʒannaɣɨ</ts>
                  <nts id="Seg_1605" n="HIAT:ip">.</nts>
                  <nts id="Seg_1606" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T426" id="Seg_1608" n="HIAT:u" s="T424">
                  <ts e="T425" id="Seg_1610" n="HIAT:w" s="T424">Qarʼemɨɣɨn</ts>
                  <nts id="Seg_1611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1613" n="HIAT:w" s="T425">wazɨn</ts>
                  <nts id="Seg_1614" n="HIAT:ip">.</nts>
                  <nts id="Seg_1615" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T433" id="Seg_1617" n="HIAT:u" s="T426">
                  <ts e="T427" id="Seg_1619" n="HIAT:w" s="T426">Näjɣum</ts>
                  <nts id="Seg_1620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1622" n="HIAT:w" s="T427">wes</ts>
                  <nts id="Seg_1623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1625" n="HIAT:w" s="T428">potqɨnɨt</ts>
                  <nts id="Seg_1626" n="HIAT:ip">,</nts>
                  <nts id="Seg_1627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1629" n="HIAT:w" s="T429">wadʼilam</ts>
                  <nts id="Seg_1630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1632" n="HIAT:w" s="T430">qajlam</ts>
                  <nts id="Seg_1633" n="HIAT:ip">,</nts>
                  <nts id="Seg_1634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1636" n="HIAT:w" s="T431">äramdə</ts>
                  <nts id="Seg_1637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1639" n="HIAT:w" s="T432">apstɨt</ts>
                  <nts id="Seg_1640" n="HIAT:ip">.</nts>
                  <nts id="Seg_1641" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T436" id="Seg_1643" n="HIAT:u" s="T433">
                  <ts e="T434" id="Seg_1645" n="HIAT:w" s="T433">Ärat</ts>
                  <nts id="Seg_1646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1648" n="HIAT:w" s="T434">madʼot</ts>
                  <nts id="Seg_1649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1651" n="HIAT:w" s="T435">qwannɨn</ts>
                  <nts id="Seg_1652" n="HIAT:ip">.</nts>
                  <nts id="Seg_1653" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T443" id="Seg_1655" n="HIAT:u" s="T436">
                  <ts e="T437" id="Seg_1657" n="HIAT:w" s="T436">Näjɣum</ts>
                  <nts id="Seg_1658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1660" n="HIAT:w" s="T437">ponä</ts>
                  <nts id="Seg_1661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1663" n="HIAT:w" s="T438">sapɨsin</ts>
                  <nts id="Seg_1664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1666" n="HIAT:w" s="T439">i</ts>
                  <nts id="Seg_1667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1669" n="HIAT:w" s="T440">na</ts>
                  <nts id="Seg_1670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1672" n="HIAT:w" s="T441">qaːrolʼdʼä</ts>
                  <nts id="Seg_1673" n="HIAT:ip">:</nts>
                  <nts id="Seg_1674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1675" n="HIAT:ip">“</nts>
                  <ts e="T443" id="Seg_1677" n="HIAT:w" s="T442">Tʼükkɨ</ts>
                  <nts id="Seg_1678" n="HIAT:ip">!</nts>
                  <nts id="Seg_1679" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T445" id="Seg_1681" n="HIAT:u" s="T443">
                  <ts e="T444" id="Seg_1683" n="HIAT:w" s="T443">Ärau</ts>
                  <nts id="Seg_1684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1686" n="HIAT:w" s="T444">qwanba</ts>
                  <nts id="Seg_1687" n="HIAT:ip">!</nts>
                  <nts id="Seg_1688" n="HIAT:ip">”</nts>
                  <nts id="Seg_1689" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T448" id="Seg_1691" n="HIAT:u" s="T445">
                  <ts e="T446" id="Seg_1693" n="HIAT:w" s="T445">Ataman</ts>
                  <nts id="Seg_1694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1696" n="HIAT:w" s="T446">na</ts>
                  <nts id="Seg_1697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1699" n="HIAT:w" s="T447">tütda</ts>
                  <nts id="Seg_1700" n="HIAT:ip">.</nts>
                  <nts id="Seg_1701" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T451" id="Seg_1703" n="HIAT:u" s="T448">
                  <nts id="Seg_1704" n="HIAT:ip">“</nts>
                  <ts e="T449" id="Seg_1706" n="HIAT:w" s="T448">No</ts>
                  <nts id="Seg_1707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1709" n="HIAT:w" s="T449">qaj</ts>
                  <nts id="Seg_1710" n="HIAT:ip">,</nts>
                  <nts id="Seg_1711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1713" n="HIAT:w" s="T450">qozɨrčezalʼe</ts>
                  <nts id="Seg_1714" n="HIAT:ip">?</nts>
                  <nts id="Seg_1715" n="HIAT:ip">”</nts>
                  <nts id="Seg_1716" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T452" id="Seg_1718" n="HIAT:u" s="T451">
                  <nts id="Seg_1719" n="HIAT:ip">“</nts>
                  <ts e="T452" id="Seg_1721" n="HIAT:w" s="T451">Qozɨrčezaj</ts>
                  <nts id="Seg_1722" n="HIAT:ip">.</nts>
                  <nts id="Seg_1723" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T462" id="Seg_1725" n="HIAT:u" s="T452">
                  <ts e="T453" id="Seg_1727" n="HIAT:w" s="T452">Man</ts>
                  <nts id="Seg_1728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1730" n="HIAT:w" s="T453">tʼekga</ts>
                  <nts id="Seg_1731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1733" n="HIAT:w" s="T454">tʼärɨzan</ts>
                  <nts id="Seg_1734" n="HIAT:ip">,</nts>
                  <nts id="Seg_1735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1737" n="HIAT:w" s="T455">täp</ts>
                  <nts id="Seg_1738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1740" n="HIAT:w" s="T456">orse</ts>
                  <nts id="Seg_1741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1743" n="HIAT:w" s="T457">jen</ts>
                  <nts id="Seg_1744" n="HIAT:ip">,</nts>
                  <nts id="Seg_1745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1747" n="HIAT:w" s="T458">na</ts>
                  <nts id="Seg_1748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1750" n="HIAT:w" s="T459">qudɨgolam</ts>
                  <nts id="Seg_1751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1753" n="HIAT:w" s="T460">wes</ts>
                  <nts id="Seg_1754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1756" n="HIAT:w" s="T461">lakčelǯɨt</ts>
                  <nts id="Seg_1757" n="HIAT:ip">.</nts>
                  <nts id="Seg_1758" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T470" id="Seg_1760" n="HIAT:u" s="T462">
                  <nts id="Seg_1761" n="HIAT:ip">“</nts>
                  <ts e="T463" id="Seg_1763" n="HIAT:w" s="T462">A</ts>
                  <nts id="Seg_1764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1766" n="HIAT:w" s="T463">tamdʼel</ts>
                  <nts id="Seg_1767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1769" n="HIAT:w" s="T464">man</ts>
                  <nts id="Seg_1770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1772" n="HIAT:w" s="T465">tʼekga</ts>
                  <nts id="Seg_1773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1775" n="HIAT:w" s="T466">meǯau</ts>
                  <nts id="Seg_1776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1778" n="HIAT:w" s="T467">optɨ</ts>
                  <nts id="Seg_1779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1781" n="HIAT:w" s="T468">qudɨgo</ts>
                  <nts id="Seg_1782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1784" n="HIAT:w" s="T469">mennɨǯan</ts>
                  <nts id="Seg_1785" n="HIAT:ip">.</nts>
                  <nts id="Seg_1786" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T476" id="Seg_1788" n="HIAT:u" s="T470">
                  <ts e="T471" id="Seg_1790" n="HIAT:w" s="T470">Üdəmɨɣɨn</ts>
                  <nts id="Seg_1791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1793" n="HIAT:w" s="T471">na</ts>
                  <nts id="Seg_1794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1796" n="HIAT:w" s="T472">tünnɨš</ts>
                  <nts id="Seg_1797" n="HIAT:ip">,</nts>
                  <nts id="Seg_1798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1800" n="HIAT:w" s="T473">aj</ts>
                  <nts id="Seg_1801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1803" n="HIAT:w" s="T474">tʼärak</ts>
                  <nts id="Seg_1804" n="HIAT:ip">:</nts>
                  <nts id="Seg_1805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1807" n="HIAT:w" s="T475">Qozɨrčɨlaj</ts>
                  <nts id="Seg_1808" n="HIAT:ip">.</nts>
                  <nts id="Seg_1809" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T482" id="Seg_1811" n="HIAT:u" s="T476">
                  <ts e="T477" id="Seg_1813" n="HIAT:w" s="T476">Qut</ts>
                  <nts id="Seg_1814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1816" n="HIAT:w" s="T477">turakɨn</ts>
                  <nts id="Seg_1817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1819" n="HIAT:w" s="T478">qalʼeǯɨn</ts>
                  <nts id="Seg_1820" n="HIAT:ip">,</nts>
                  <nts id="Seg_1821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1823" n="HIAT:w" s="T479">udlamdə</ts>
                  <nts id="Seg_1824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1826" n="HIAT:w" s="T480">moɣunä</ts>
                  <nts id="Seg_1827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1829" n="HIAT:w" s="T481">saːruku</ts>
                  <nts id="Seg_1830" n="HIAT:ip">.</nts>
                  <nts id="Seg_1831" n="HIAT:ip">”</nts>
                  <nts id="Seg_1832" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T487" id="Seg_1834" n="HIAT:u" s="T482">
                  <ts e="T483" id="Seg_1836" n="HIAT:w" s="T482">Näjɣum</ts>
                  <nts id="Seg_1837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1839" n="HIAT:w" s="T483">täbɨm</ts>
                  <nts id="Seg_1840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1842" n="HIAT:w" s="T484">apstɨpbaːt</ts>
                  <nts id="Seg_1843" n="HIAT:ip">,</nts>
                  <nts id="Seg_1844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1846" n="HIAT:w" s="T485">arakazʼe</ts>
                  <nts id="Seg_1847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1849" n="HIAT:w" s="T486">ärčibat</ts>
                  <nts id="Seg_1850" n="HIAT:ip">.</nts>
                  <nts id="Seg_1851" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T489" id="Seg_1853" n="HIAT:u" s="T487">
                  <ts e="T488" id="Seg_1855" n="HIAT:w" s="T487">Araqɨla</ts>
                  <nts id="Seg_1856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1858" n="HIAT:w" s="T488">üttɨdɨ</ts>
                  <nts id="Seg_1859" n="HIAT:ip">.</nts>
                  <nts id="Seg_1860" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T491" id="Seg_1862" n="HIAT:u" s="T489">
                  <ts e="T490" id="Seg_1864" n="HIAT:w" s="T489">Täbeɣum</ts>
                  <nts id="Seg_1865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1867" n="HIAT:w" s="T490">qwannɨn</ts>
                  <nts id="Seg_1868" n="HIAT:ip">.</nts>
                  <nts id="Seg_1869" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T493" id="Seg_1871" n="HIAT:u" s="T491">
                  <ts e="T492" id="Seg_1873" n="HIAT:w" s="T491">Ärat</ts>
                  <nts id="Seg_1874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1876" n="HIAT:w" s="T492">tüa</ts>
                  <nts id="Seg_1877" n="HIAT:ip">.</nts>
                  <nts id="Seg_1878" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T499" id="Seg_1880" n="HIAT:u" s="T493">
                  <ts e="T494" id="Seg_1882" n="HIAT:w" s="T493">Surumlam</ts>
                  <nts id="Seg_1883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1885" n="HIAT:w" s="T494">koːcʼen</ts>
                  <nts id="Seg_1886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1888" n="HIAT:w" s="T495">qwatpat</ts>
                  <nts id="Seg_1889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1891" n="HIAT:w" s="T496">i</ts>
                  <nts id="Seg_1892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1894" n="HIAT:w" s="T497">pekqɨ</ts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1897" n="HIAT:w" s="T498">qwatpadɨt</ts>
                  <nts id="Seg_1898" n="HIAT:ip">.</nts>
                  <nts id="Seg_1899" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T502" id="Seg_1901" n="HIAT:u" s="T499">
                  <ts e="T500" id="Seg_1903" n="HIAT:w" s="T499">Täp</ts>
                  <nts id="Seg_1904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1906" n="HIAT:w" s="T500">täbɨm</ts>
                  <nts id="Seg_1907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1909" n="HIAT:w" s="T501">apstɨt</ts>
                  <nts id="Seg_1910" n="HIAT:ip">.</nts>
                  <nts id="Seg_1911" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T504" id="Seg_1913" n="HIAT:u" s="T502">
                  <ts e="T503" id="Seg_1915" n="HIAT:w" s="T502">Tʼärɨn</ts>
                  <nts id="Seg_1916" n="HIAT:ip">:</nts>
                  <nts id="Seg_1917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1918" n="HIAT:ip">“</nts>
                  <ts e="T504" id="Seg_1920" n="HIAT:w" s="T503">Qozɨrčɨlaj</ts>
                  <nts id="Seg_1921" n="HIAT:ip">.</nts>
                  <nts id="Seg_1922" n="HIAT:ip">”</nts>
                  <nts id="Seg_1923" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T508" id="Seg_1925" n="HIAT:u" s="T504">
                  <ts e="T505" id="Seg_1927" n="HIAT:w" s="T504">Ärat</ts>
                  <nts id="Seg_1928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1930" n="HIAT:w" s="T505">tʼärɨn</ts>
                  <nts id="Seg_1931" n="HIAT:ip">:</nts>
                  <nts id="Seg_1932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1933" n="HIAT:ip">“</nts>
                  <ts e="T507" id="Seg_1935" n="HIAT:w" s="T506">Man</ts>
                  <nts id="Seg_1936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1938" n="HIAT:w" s="T507">nuːnɨdʼipbaːn</ts>
                  <nts id="Seg_1939" n="HIAT:ip">.</nts>
                  <nts id="Seg_1940" n="HIAT:ip">”</nts>
                  <nts id="Seg_1941" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T520" id="Seg_1943" n="HIAT:u" s="T508">
                  <ts e="T509" id="Seg_1945" n="HIAT:w" s="T508">Pajat</ts>
                  <nts id="Seg_1946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1948" n="HIAT:w" s="T509">tʼärɨn</ts>
                  <nts id="Seg_1949" n="HIAT:ip">:</nts>
                  <nts id="Seg_1950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1951" n="HIAT:ip">“</nts>
                  <ts e="T511" id="Seg_1953" n="HIAT:w" s="T510">Qozɨrčɨlaj</ts>
                  <nts id="Seg_1954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1956" n="HIAT:w" s="T511">tʼem</ts>
                  <nts id="Seg_1957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1959" n="HIAT:w" s="T512">ugoworom</ts>
                  <nts id="Seg_1960" n="HIAT:ip">,</nts>
                  <nts id="Seg_1961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1963" n="HIAT:w" s="T513">qudə</ts>
                  <nts id="Seg_1964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1966" n="HIAT:w" s="T514">qut</ts>
                  <nts id="Seg_1967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1969" n="HIAT:w" s="T515">turakɨn</ts>
                  <nts id="Seg_1970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1972" n="HIAT:w" s="T516">qalʼeǯɨn</ts>
                  <nts id="Seg_1973" n="HIAT:ip">,</nts>
                  <nts id="Seg_1974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1976" n="HIAT:w" s="T517">udɨmdə</ts>
                  <nts id="Seg_1977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1979" n="HIAT:w" s="T518">moɣunä</ts>
                  <nts id="Seg_1980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1982" n="HIAT:w" s="T519">saːrugu</ts>
                  <nts id="Seg_1983" n="HIAT:ip">.</nts>
                  <nts id="Seg_1984" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T528" id="Seg_1986" n="HIAT:u" s="T520">
                  <ts e="T521" id="Seg_1988" n="HIAT:w" s="T520">Ješlʼi</ts>
                  <nts id="Seg_1989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1991" n="HIAT:w" s="T521">man</ts>
                  <nts id="Seg_1992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_1994" n="HIAT:w" s="T522">qalan</ts>
                  <nts id="Seg_1995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1997" n="HIAT:w" s="T523">duraqɨn</ts>
                  <nts id="Seg_1998" n="HIAT:ip">,</nts>
                  <nts id="Seg_1999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_2001" n="HIAT:w" s="T524">man</ts>
                  <nts id="Seg_2002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_2004" n="HIAT:w" s="T525">udou</ts>
                  <nts id="Seg_2005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_2007" n="HIAT:w" s="T526">saːraq</ts>
                  <nts id="Seg_2008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2009" n="HIAT:ip">(</nts>
                  <ts e="T528" id="Seg_2011" n="HIAT:w" s="T527">sareǯal</ts>
                  <nts id="Seg_2012" n="HIAT:ip">)</nts>
                  <nts id="Seg_2013" n="HIAT:ip">.</nts>
                  <nts id="Seg_2014" n="HIAT:ip">”</nts>
                  <nts id="Seg_2015" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T531" id="Seg_2017" n="HIAT:u" s="T528">
                  <ts e="T529" id="Seg_2019" n="HIAT:w" s="T528">Täbɨstaɣə</ts>
                  <nts id="Seg_2020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_2022" n="HIAT:w" s="T529">omdaɣə</ts>
                  <nts id="Seg_2023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_2025" n="HIAT:w" s="T530">qozɨrčilʼe</ts>
                  <nts id="Seg_2026" n="HIAT:ip">.</nts>
                  <nts id="Seg_2027" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T534" id="Seg_2029" n="HIAT:u" s="T531">
                  <ts e="T532" id="Seg_2031" n="HIAT:w" s="T531">Ärat</ts>
                  <nts id="Seg_2032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_2034" n="HIAT:w" s="T532">turaqɨn</ts>
                  <nts id="Seg_2035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_2037" n="HIAT:w" s="T533">qalɨn</ts>
                  <nts id="Seg_2038" n="HIAT:ip">.</nts>
                  <nts id="Seg_2039" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T538" id="Seg_2041" n="HIAT:u" s="T534">
                  <nts id="Seg_2042" n="HIAT:ip">“</nts>
                  <ts e="T535" id="Seg_2044" n="HIAT:w" s="T534">Nu</ts>
                  <nts id="Seg_2045" n="HIAT:ip">,</nts>
                  <nts id="Seg_2046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_2048" n="HIAT:w" s="T535">täper</ts>
                  <nts id="Seg_2049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_2051" n="HIAT:w" s="T536">udomdə</ts>
                  <nts id="Seg_2052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_2054" n="HIAT:w" s="T537">saːreǯau</ts>
                  <nts id="Seg_2055" n="HIAT:ip">.</nts>
                  <nts id="Seg_2056" n="HIAT:ip">”</nts>
                  <nts id="Seg_2057" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T541" id="Seg_2059" n="HIAT:u" s="T538">
                  <ts e="T539" id="Seg_2061" n="HIAT:w" s="T538">Täp</ts>
                  <nts id="Seg_2062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_2064" n="HIAT:w" s="T539">saːrɨt</ts>
                  <nts id="Seg_2065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_2067" n="HIAT:w" s="T540">udɨmdə</ts>
                  <nts id="Seg_2068" n="HIAT:ip">.</nts>
                  <nts id="Seg_2069" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T546" id="Seg_2071" n="HIAT:u" s="T541">
                  <ts e="T542" id="Seg_2073" n="HIAT:w" s="T541">Täp</ts>
                  <nts id="Seg_2074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_2076" n="HIAT:w" s="T542">kak</ts>
                  <nts id="Seg_2077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_2079" n="HIAT:w" s="T543">turgulǯɨtda</ts>
                  <nts id="Seg_2080" n="HIAT:ip">,</nts>
                  <nts id="Seg_2081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_2083" n="HIAT:w" s="T544">qudɨgo</ts>
                  <nts id="Seg_2084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_2086" n="HIAT:w" s="T545">laqčɨdin</ts>
                  <nts id="Seg_2087" n="HIAT:ip">.</nts>
                  <nts id="Seg_2088" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T548" id="Seg_2090" n="HIAT:u" s="T546">
                  <ts e="T547" id="Seg_2092" n="HIAT:w" s="T546">Quʒannaɣə</ts>
                  <nts id="Seg_2093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_2095" n="HIAT:w" s="T547">qotduɣɨ</ts>
                  <nts id="Seg_2096" n="HIAT:ip">.</nts>
                  <nts id="Seg_2097" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T550" id="Seg_2099" n="HIAT:u" s="T548">
                  <ts e="T549" id="Seg_2101" n="HIAT:w" s="T548">Qarʼemɨɣɨn</ts>
                  <nts id="Seg_2102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_2104" n="HIAT:w" s="T549">wazaɣə</ts>
                  <nts id="Seg_2105" n="HIAT:ip">.</nts>
                  <nts id="Seg_2106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T553" id="Seg_2108" n="HIAT:u" s="T550">
                  <ts e="T551" id="Seg_2110" n="HIAT:w" s="T550">Näjɣum</ts>
                  <nts id="Seg_2111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2113" n="HIAT:w" s="T551">äramdə</ts>
                  <nts id="Seg_2114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_2116" n="HIAT:w" s="T552">apstɨt</ts>
                  <nts id="Seg_2117" n="HIAT:ip">.</nts>
                  <nts id="Seg_2118" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T559" id="Seg_2120" n="HIAT:u" s="T553">
                  <ts e="T554" id="Seg_2122" n="HIAT:w" s="T553">Ärat</ts>
                  <nts id="Seg_2123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_2125" n="HIAT:w" s="T554">aurnɨn</ts>
                  <nts id="Seg_2126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2128" n="HIAT:w" s="T555">i</ts>
                  <nts id="Seg_2129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_2131" n="HIAT:w" s="T556">madʼot</ts>
                  <nts id="Seg_2132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_2134" n="HIAT:w" s="T557">na</ts>
                  <nts id="Seg_2135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2137" n="HIAT:w" s="T558">qwatda</ts>
                  <nts id="Seg_2138" n="HIAT:ip">.</nts>
                  <nts id="Seg_2139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T566" id="Seg_2141" n="HIAT:u" s="T559">
                  <ts e="T560" id="Seg_2143" n="HIAT:w" s="T559">Näiɣum</ts>
                  <nts id="Seg_2144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2146" n="HIAT:w" s="T560">ponä</ts>
                  <nts id="Seg_2147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2149" n="HIAT:w" s="T561">čaːǯɨn</ts>
                  <nts id="Seg_2150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_2152" n="HIAT:w" s="T562">i</ts>
                  <nts id="Seg_2153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2155" n="HIAT:w" s="T563">na</ts>
                  <nts id="Seg_2156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2158" n="HIAT:w" s="T564">qaːronʼen</ts>
                  <nts id="Seg_2159" n="HIAT:ip">:</nts>
                  <nts id="Seg_2160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2161" n="HIAT:ip">“</nts>
                  <ts e="T566" id="Seg_2163" n="HIAT:w" s="T565">Tükkɨ</ts>
                  <nts id="Seg_2164" n="HIAT:ip">!</nts>
                  <nts id="Seg_2165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T568" id="Seg_2167" n="HIAT:u" s="T566">
                  <ts e="T567" id="Seg_2169" n="HIAT:w" s="T566">Ärau</ts>
                  <nts id="Seg_2170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_2172" n="HIAT:w" s="T567">qwanba</ts>
                  <nts id="Seg_2173" n="HIAT:ip">!</nts>
                  <nts id="Seg_2174" n="HIAT:ip">”</nts>
                  <nts id="Seg_2175" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T570" id="Seg_2177" n="HIAT:u" s="T568">
                  <ts e="T569" id="Seg_2179" n="HIAT:w" s="T568">Razbojnik</ts>
                  <nts id="Seg_2180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2182" n="HIAT:w" s="T569">tüwa</ts>
                  <nts id="Seg_2183" n="HIAT:ip">.</nts>
                  <nts id="Seg_2184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T576" id="Seg_2186" n="HIAT:u" s="T570">
                  <ts e="T571" id="Seg_2188" n="HIAT:w" s="T570">Näiɣum</ts>
                  <nts id="Seg_2189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2191" n="HIAT:w" s="T571">tʼärɨn</ts>
                  <nts id="Seg_2192" n="HIAT:ip">:</nts>
                  <nts id="Seg_2193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2194" n="HIAT:ip">“</nts>
                  <ts e="T573" id="Seg_2196" n="HIAT:w" s="T572">Qudɨgolam</ts>
                  <nts id="Seg_2197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2199" n="HIAT:w" s="T573">ärau</ts>
                  <nts id="Seg_2200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_2202" n="HIAT:w" s="T574">wes</ts>
                  <nts id="Seg_2203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2205" n="HIAT:w" s="T575">lakčelǯɨt</ts>
                  <nts id="Seg_2206" n="HIAT:ip">.</nts>
                  <nts id="Seg_2207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T579" id="Seg_2209" n="HIAT:u" s="T576">
                  <ts e="T577" id="Seg_2211" n="HIAT:w" s="T576">Täp</ts>
                  <nts id="Seg_2212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2214" n="HIAT:w" s="T577">orse</ts>
                  <nts id="Seg_2215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2217" n="HIAT:w" s="T578">jen</ts>
                  <nts id="Seg_2218" n="HIAT:ip">.</nts>
                  <nts id="Seg_2219" n="HIAT:ip">”</nts>
                  <nts id="Seg_2220" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T591" id="Seg_2222" n="HIAT:u" s="T579">
                  <nts id="Seg_2223" n="HIAT:ip">“</nts>
                  <ts e="T580" id="Seg_2225" n="HIAT:w" s="T579">A</ts>
                  <nts id="Seg_2226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2228" n="HIAT:w" s="T580">man</ts>
                  <nts id="Seg_2229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2231" n="HIAT:w" s="T581">tamdʼel</ts>
                  <nts id="Seg_2232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2234" n="HIAT:w" s="T582">tekga</ts>
                  <nts id="Seg_2235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2237" n="HIAT:w" s="T583">meǯau</ts>
                  <nts id="Seg_2238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2240" n="HIAT:w" s="T584">kudɨgo</ts>
                  <nts id="Seg_2241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2243" n="HIAT:w" s="T585">optɨ</ts>
                  <nts id="Seg_2244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2246" n="HIAT:w" s="T586">i</ts>
                  <nts id="Seg_2247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2249" n="HIAT:w" s="T587">sɨnkowaja</ts>
                  <nts id="Seg_2250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2252" n="HIAT:w" s="T588">prowoloka</ts>
                  <nts id="Seg_2253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2255" n="HIAT:w" s="T589">okkɨrmɨt</ts>
                  <nts id="Seg_2256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2258" n="HIAT:w" s="T590">tamkɨlba</ts>
                  <nts id="Seg_2259" n="HIAT:ip">.</nts>
                  <nts id="Seg_2260" n="HIAT:ip">”</nts>
                  <nts id="Seg_2261" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T599" id="Seg_2263" n="HIAT:u" s="T591">
                  <ts e="T592" id="Seg_2265" n="HIAT:w" s="T591">Üːdɨmɨɣɨn</ts>
                  <nts id="Seg_2266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2268" n="HIAT:w" s="T592">na</ts>
                  <nts id="Seg_2269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2271" n="HIAT:w" s="T593">tünnɨš</ts>
                  <nts id="Seg_2272" n="HIAT:ip">,</nts>
                  <nts id="Seg_2273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2275" n="HIAT:w" s="T594">tan</ts>
                  <nts id="Seg_2276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2278" n="HIAT:w" s="T595">aj</ts>
                  <nts id="Seg_2279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2281" n="HIAT:w" s="T596">tʼärak</ts>
                  <nts id="Seg_2282" n="HIAT:ip">:</nts>
                  <nts id="Seg_2283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2285" n="HIAT:w" s="T597">Qozɨrčuɣu</ts>
                  <nts id="Seg_2286" n="HIAT:ip">,</nts>
                  <nts id="Seg_2287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2289" n="HIAT:w" s="T598">qozɨrčilaj</ts>
                  <nts id="Seg_2290" n="HIAT:ip">.</nts>
                  <nts id="Seg_2291" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T606" id="Seg_2293" n="HIAT:u" s="T599">
                  <ts e="T600" id="Seg_2295" n="HIAT:w" s="T599">Qut</ts>
                  <nts id="Seg_2296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2298" n="HIAT:w" s="T600">turakɨn</ts>
                  <nts id="Seg_2299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2301" n="HIAT:w" s="T601">kalʼeǯɨn</ts>
                  <nts id="Seg_2302" n="HIAT:ip">,</nts>
                  <nts id="Seg_2303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2305" n="HIAT:w" s="T602">to</ts>
                  <nts id="Seg_2306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2308" n="HIAT:w" s="T603">udɨmdə</ts>
                  <nts id="Seg_2309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2311" n="HIAT:w" s="T604">moɣunä</ts>
                  <nts id="Seg_2312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2314" n="HIAT:w" s="T605">saːrugu</ts>
                  <nts id="Seg_2315" n="HIAT:ip">.</nts>
                  <nts id="Seg_2316" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T608" id="Seg_2318" n="HIAT:u" s="T606">
                  <ts e="T607" id="Seg_2320" n="HIAT:w" s="T606">Ära</ts>
                  <nts id="Seg_2321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2323" n="HIAT:w" s="T607">tüa</ts>
                  <nts id="Seg_2324" n="HIAT:ip">.</nts>
                  <nts id="Seg_2325" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T613" id="Seg_2327" n="HIAT:u" s="T608">
                  <ts e="T609" id="Seg_2329" n="HIAT:w" s="T608">Täp</ts>
                  <nts id="Seg_2330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2332" n="HIAT:w" s="T609">täbɨm</ts>
                  <nts id="Seg_2333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2335" n="HIAT:w" s="T610">apstɨt</ts>
                  <nts id="Seg_2336" n="HIAT:ip">,</nts>
                  <nts id="Seg_2337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2339" n="HIAT:w" s="T611">tʼärɨn</ts>
                  <nts id="Seg_2340" n="HIAT:ip">:</nts>
                  <nts id="Seg_2341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2342" n="HIAT:ip">“</nts>
                  <ts e="T613" id="Seg_2344" n="HIAT:w" s="T612">Qozɨrčɨlaj</ts>
                  <nts id="Seg_2345" n="HIAT:ip">.</nts>
                  <nts id="Seg_2346" n="HIAT:ip">”</nts>
                  <nts id="Seg_2347" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T619" id="Seg_2349" n="HIAT:u" s="T613">
                  <ts e="T614" id="Seg_2351" n="HIAT:w" s="T613">Ärat</ts>
                  <nts id="Seg_2352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2354" n="HIAT:w" s="T614">tʼärɨn</ts>
                  <nts id="Seg_2355" n="HIAT:ip">:</nts>
                  <nts id="Seg_2356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2357" n="HIAT:ip">“</nts>
                  <ts e="T616" id="Seg_2359" n="HIAT:w" s="T615">Man</ts>
                  <nts id="Seg_2360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2362" n="HIAT:w" s="T616">nunɨdʼzʼipan</ts>
                  <nts id="Seg_2363" n="HIAT:ip">,</nts>
                  <nts id="Seg_2364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2366" n="HIAT:w" s="T617">nadə</ts>
                  <nts id="Seg_2367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2369" n="HIAT:w" s="T618">qotdɨɣu</ts>
                  <nts id="Seg_2370" n="HIAT:ip">.</nts>
                  <nts id="Seg_2371" n="HIAT:ip">”</nts>
                  <nts id="Seg_2372" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T622" id="Seg_2374" n="HIAT:u" s="T619">
                  <nts id="Seg_2375" n="HIAT:ip">“</nts>
                  <ts e="T620" id="Seg_2377" n="HIAT:w" s="T619">No</ts>
                  <nts id="Seg_2378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2380" n="HIAT:w" s="T620">qozɨrčɨlaj</ts>
                  <nts id="Seg_2381" n="HIAT:ip">,</nts>
                  <nts id="Seg_2382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2384" n="HIAT:w" s="T621">qozɨrčɨlaj</ts>
                  <nts id="Seg_2385" n="HIAT:ip">.</nts>
                  <nts id="Seg_2386" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T630" id="Seg_2388" n="HIAT:u" s="T622">
                  <ts e="T623" id="Seg_2390" n="HIAT:w" s="T622">Xotʼ</ts>
                  <nts id="Seg_2391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2393" n="HIAT:w" s="T623">okkɨrɨn</ts>
                  <nts id="Seg_2394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2396" n="HIAT:w" s="T624">qut</ts>
                  <nts id="Seg_2397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2399" n="HIAT:w" s="T625">turakɨn</ts>
                  <nts id="Seg_2400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2402" n="HIAT:w" s="T626">kalʼeǯɨn</ts>
                  <nts id="Seg_2403" n="HIAT:ip">,</nts>
                  <nts id="Seg_2404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2406" n="HIAT:w" s="T627">udɨmdə</ts>
                  <nts id="Seg_2407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2409" n="HIAT:w" s="T628">moɣunä</ts>
                  <nts id="Seg_2410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2412" n="HIAT:w" s="T629">saaruku</ts>
                  <nts id="Seg_2413" n="HIAT:ip">.</nts>
                  <nts id="Seg_2414" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T637" id="Seg_2416" n="HIAT:u" s="T630">
                  <ts e="T631" id="Seg_2418" n="HIAT:w" s="T630">Jeʒlʼi</ts>
                  <nts id="Seg_2419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2421" n="HIAT:w" s="T631">man</ts>
                  <nts id="Seg_2422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2424" n="HIAT:w" s="T632">kalʼeǯan</ts>
                  <nts id="Seg_2425" n="HIAT:ip">,</nts>
                  <nts id="Seg_2426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2428" n="HIAT:w" s="T633">man</ts>
                  <nts id="Seg_2429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2431" n="HIAT:w" s="T634">udou</ts>
                  <nts id="Seg_2432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2434" n="HIAT:w" s="T635">saːrʼet</ts>
                  <nts id="Seg_2435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2437" n="HIAT:w" s="T636">moɣunä</ts>
                  <nts id="Seg_2438" n="HIAT:ip">.</nts>
                  <nts id="Seg_2439" n="HIAT:ip">”</nts>
                  <nts id="Seg_2440" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T640" id="Seg_2442" n="HIAT:u" s="T637">
                  <ts e="T638" id="Seg_2444" n="HIAT:w" s="T637">I</ts>
                  <nts id="Seg_2445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2447" n="HIAT:w" s="T638">oːmdaɣə</ts>
                  <nts id="Seg_2448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2450" n="HIAT:w" s="T639">qozɨrčɨlʼe</ts>
                  <nts id="Seg_2451" n="HIAT:ip">.</nts>
                  <nts id="Seg_2452" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T646" id="Seg_2454" n="HIAT:u" s="T640">
                  <ts e="T641" id="Seg_2456" n="HIAT:w" s="T640">Äramdə</ts>
                  <nts id="Seg_2457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2459" n="HIAT:w" s="T641">turagɨwlʼe</ts>
                  <nts id="Seg_2460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2462" n="HIAT:w" s="T642">qwɛdʼit</ts>
                  <nts id="Seg_2463" n="HIAT:ip">,</nts>
                  <nts id="Seg_2464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2466" n="HIAT:w" s="T643">udɨmdə</ts>
                  <nts id="Seg_2467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2469" n="HIAT:w" s="T644">moɣunä</ts>
                  <nts id="Seg_2470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2472" n="HIAT:w" s="T645">saːrɨt</ts>
                  <nts id="Seg_2473" n="HIAT:ip">.</nts>
                  <nts id="Seg_2474" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T652" id="Seg_2476" n="HIAT:u" s="T646">
                  <ts e="T647" id="Seg_2478" n="HIAT:w" s="T646">Täp</ts>
                  <nts id="Seg_2479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2481" n="HIAT:w" s="T647">kak</ts>
                  <nts id="Seg_2482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2484" n="HIAT:w" s="T648">turgulǯɨtda</ts>
                  <nts id="Seg_2485" n="HIAT:ip">,</nts>
                  <nts id="Seg_2486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2488" n="HIAT:w" s="T649">qudəkolam</ts>
                  <nts id="Seg_2489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_2491" n="HIAT:w" s="T650">jass</ts>
                  <nts id="Seg_2492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2494" n="HIAT:w" s="T651">laqčɨt</ts>
                  <nts id="Seg_2495" n="HIAT:ip">.</nts>
                  <nts id="Seg_2496" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T656" id="Seg_2498" n="HIAT:u" s="T652">
                  <ts e="T653" id="Seg_2500" n="HIAT:w" s="T652">Pajätdänä</ts>
                  <nts id="Seg_2501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2503" n="HIAT:w" s="T653">tʼärɨn</ts>
                  <nts id="Seg_2504" n="HIAT:ip">:</nts>
                  <nts id="Seg_2505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2506" n="HIAT:ip">“</nts>
                  <ts e="T655" id="Seg_2508" n="HIAT:w" s="T654">Tʼiket</ts>
                  <nts id="Seg_2509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2511" n="HIAT:w" s="T655">udou</ts>
                  <nts id="Seg_2512" n="HIAT:ip">.</nts>
                  <nts id="Seg_2513" n="HIAT:ip">”</nts>
                  <nts id="Seg_2514" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T659" id="Seg_2516" n="HIAT:u" s="T656">
                  <ts e="T657" id="Seg_2518" n="HIAT:w" s="T656">Pajät</ts>
                  <nts id="Seg_2519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2521" n="HIAT:w" s="T657">tʼärɨn</ts>
                  <nts id="Seg_2522" n="HIAT:ip">:</nts>
                  <nts id="Seg_2523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2524" n="HIAT:ip">“</nts>
                  <ts e="T659" id="Seg_2526" n="HIAT:w" s="T658">Lakčet</ts>
                  <nts id="Seg_2527" n="HIAT:ip">.</nts>
                  <nts id="Seg_2528" n="HIAT:ip">”</nts>
                  <nts id="Seg_2529" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T664" id="Seg_2531" n="HIAT:u" s="T659">
                  <ts e="T660" id="Seg_2533" n="HIAT:w" s="T659">Täp</ts>
                  <nts id="Seg_2534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2536" n="HIAT:w" s="T660">aj</ts>
                  <nts id="Seg_2537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2539" n="HIAT:w" s="T661">turgulǯɨn</ts>
                  <nts id="Seg_2540" n="HIAT:ip">,</nts>
                  <nts id="Seg_2541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2543" n="HIAT:w" s="T662">jass</ts>
                  <nts id="Seg_2544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2546" n="HIAT:w" s="T663">lakčɨt</ts>
                  <nts id="Seg_2547" n="HIAT:ip">.</nts>
                  <nts id="Seg_2548" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T666" id="Seg_2550" n="HIAT:u" s="T664">
                  <ts e="T665" id="Seg_2552" n="HIAT:w" s="T664">Tʼärɨn</ts>
                  <nts id="Seg_2553" n="HIAT:ip">:</nts>
                  <nts id="Seg_2554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2555" n="HIAT:ip">“</nts>
                  <ts e="T666" id="Seg_2557" n="HIAT:w" s="T665">Tʼiket</ts>
                  <nts id="Seg_2558" n="HIAT:ip">.</nts>
                  <nts id="Seg_2559" n="HIAT:ip">”</nts>
                  <nts id="Seg_2560" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T668" id="Seg_2562" n="HIAT:u" s="T666">
                  <nts id="Seg_2563" n="HIAT:ip">“</nts>
                  <ts e="T667" id="Seg_2565" n="HIAT:w" s="T666">Ass</ts>
                  <nts id="Seg_2566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2568" n="HIAT:w" s="T667">tʼikeǯau</ts>
                  <nts id="Seg_2569" n="HIAT:ip">.</nts>
                  <nts id="Seg_2570" n="HIAT:ip">”</nts>
                  <nts id="Seg_2571" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T675" id="Seg_2573" n="HIAT:u" s="T668">
                  <ts e="T669" id="Seg_2575" n="HIAT:w" s="T668">Ponä</ts>
                  <nts id="Seg_2576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2578" n="HIAT:w" s="T669">sappɨselʼe</ts>
                  <nts id="Seg_2579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2581" n="HIAT:w" s="T670">qwannɨn</ts>
                  <nts id="Seg_2582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2584" n="HIAT:w" s="T671">i</ts>
                  <nts id="Seg_2585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2587" n="HIAT:w" s="T672">na</ts>
                  <nts id="Seg_2588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2590" n="HIAT:w" s="T673">lakgolʼdʼä</ts>
                  <nts id="Seg_2591" n="HIAT:ip">:</nts>
                  <nts id="Seg_2592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2593" n="HIAT:ip">“</nts>
                  <ts e="T675" id="Seg_2595" n="HIAT:w" s="T674">Tükkɨ</ts>
                  <nts id="Seg_2596" n="HIAT:ip">!</nts>
                  <nts id="Seg_2597" n="HIAT:ip">”</nts>
                  <nts id="Seg_2598" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T680" id="Seg_2600" n="HIAT:u" s="T675">
                  <ts e="T676" id="Seg_2602" n="HIAT:w" s="T675">I</ts>
                  <nts id="Seg_2603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2605" n="HIAT:w" s="T676">na</ts>
                  <nts id="Seg_2606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2608" n="HIAT:w" s="T677">razbojnik</ts>
                  <nts id="Seg_2609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2611" n="HIAT:w" s="T678">na</ts>
                  <nts id="Seg_2612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2614" n="HIAT:w" s="T679">tütda</ts>
                  <nts id="Seg_2615" n="HIAT:ip">.</nts>
                  <nts id="Seg_2616" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T685" id="Seg_2618" n="HIAT:u" s="T680">
                  <ts e="T681" id="Seg_2620" n="HIAT:w" s="T680">Tʼärɨn</ts>
                  <nts id="Seg_2621" n="HIAT:ip">:</nts>
                  <nts id="Seg_2622" n="HIAT:ip">“</nts>
                  <nts id="Seg_2623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2624" n="HIAT:ip">“</nts>
                  <ts e="T682" id="Seg_2626" n="HIAT:w" s="T681">Ärau</ts>
                  <nts id="Seg_2627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2629" n="HIAT:w" s="T682">qudogom</ts>
                  <nts id="Seg_2630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2632" n="HIAT:w" s="T683">ass</ts>
                  <nts id="Seg_2633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2635" n="HIAT:w" s="T684">lakčɨt</ts>
                  <nts id="Seg_2636" n="HIAT:ip">.</nts>
                  <nts id="Seg_2637" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T687" id="Seg_2639" n="HIAT:u" s="T685">
                  <ts e="T686" id="Seg_2641" n="HIAT:w" s="T685">Serlʼe</ts>
                  <nts id="Seg_2642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2644" n="HIAT:w" s="T686">qwallaj</ts>
                  <nts id="Seg_2645" n="HIAT:ip">.</nts>
                  <nts id="Seg_2646" n="HIAT:ip">”</nts>
                  <nts id="Seg_2647" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T690" id="Seg_2649" n="HIAT:u" s="T687">
                  <ts e="T688" id="Seg_2651" n="HIAT:w" s="T687">I</ts>
                  <nts id="Seg_2652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2654" n="HIAT:w" s="T688">mattə</ts>
                  <nts id="Seg_2655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2657" n="HIAT:w" s="T689">sernaɣə</ts>
                  <nts id="Seg_2658" n="HIAT:ip">.</nts>
                  <nts id="Seg_2659" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T695" id="Seg_2661" n="HIAT:u" s="T690">
                  <ts e="T691" id="Seg_2663" n="HIAT:w" s="T690">Razbojnik</ts>
                  <nts id="Seg_2664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2666" n="HIAT:w" s="T691">tʼärɨn</ts>
                  <nts id="Seg_2667" n="HIAT:ip">:</nts>
                  <nts id="Seg_2668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2669" n="HIAT:ip">“</nts>
                  <ts e="T693" id="Seg_2671" n="HIAT:w" s="T692">Qardʼen</ts>
                  <nts id="Seg_2672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2674" n="HIAT:w" s="T693">qarʼemɨɣɨn</ts>
                  <nts id="Seg_2675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2677" n="HIAT:w" s="T694">qwatčaj</ts>
                  <nts id="Seg_2678" n="HIAT:ip">.</nts>
                  <nts id="Seg_2679" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T699" id="Seg_2681" n="HIAT:u" s="T695">
                  <ts e="T696" id="Seg_2683" n="HIAT:w" s="T695">Tulǯät</ts>
                  <nts id="Seg_2684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2686" n="HIAT:w" s="T696">wʼädran</ts>
                  <nts id="Seg_2687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2689" n="HIAT:w" s="T697">tʼiːr</ts>
                  <nts id="Seg_2690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2692" n="HIAT:w" s="T698">saːq</ts>
                  <nts id="Seg_2693" n="HIAT:ip">.</nts>
                  <nts id="Seg_2694" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T705" id="Seg_2696" n="HIAT:u" s="T699">
                  <ts e="T700" id="Seg_2698" n="HIAT:w" s="T699">Täbɨm</ts>
                  <nts id="Seg_2699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2701" n="HIAT:w" s="T700">saːɣɨn</ts>
                  <nts id="Seg_2702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2704" n="HIAT:w" s="T701">parotdə</ts>
                  <nts id="Seg_2705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2707" n="HIAT:w" s="T702">puləsejlatdɨze</ts>
                  <nts id="Seg_2708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2709" n="HIAT:ip">(</nts>
                  <ts e="T704" id="Seg_2711" n="HIAT:w" s="T703">saːɣot</ts>
                  <nts id="Seg_2712" n="HIAT:ip">)</nts>
                  <nts id="Seg_2713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2715" n="HIAT:w" s="T704">omdɨlǯeǯaj</ts>
                  <nts id="Seg_2716" n="HIAT:ip">.</nts>
                  <nts id="Seg_2717" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T707" id="Seg_2719" n="HIAT:u" s="T705">
                  <ts e="T706" id="Seg_2721" n="HIAT:w" s="T705">Qarʼeməttə</ts>
                  <nts id="Seg_2722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2724" n="HIAT:w" s="T706">aːmdə</ts>
                  <nts id="Seg_2725" n="HIAT:ip">.</nts>
                  <nts id="Seg_2726" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T709" id="Seg_2728" n="HIAT:u" s="T707">
                  <ts e="T708" id="Seg_2730" n="HIAT:w" s="T707">Qarʼemɨɣɨn</ts>
                  <nts id="Seg_2731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_2733" n="HIAT:w" s="T708">qwatčaj</ts>
                  <nts id="Seg_2734" n="HIAT:ip">.</nts>
                  <nts id="Seg_2735" n="HIAT:ip">”</nts>
                  <nts id="Seg_2736" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T714" id="Seg_2738" n="HIAT:u" s="T709">
                  <ts e="T710" id="Seg_2740" n="HIAT:w" s="T709">Näjɣum</ts>
                  <nts id="Seg_2741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_2743" n="HIAT:w" s="T710">watdoɣɨt</ts>
                  <nts id="Seg_2744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2746" n="HIAT:w" s="T711">tülʼe</ts>
                  <nts id="Seg_2747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2749" n="HIAT:w" s="T712">na</ts>
                  <nts id="Seg_2750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2752" n="HIAT:w" s="T713">söːzɨtdət</ts>
                  <nts id="Seg_2753" n="HIAT:ip">.</nts>
                  <nts id="Seg_2754" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T720" id="Seg_2756" n="HIAT:u" s="T714">
                  <nts id="Seg_2757" n="HIAT:ip">“</nts>
                  <ts e="T715" id="Seg_2759" n="HIAT:w" s="T714">Tan</ts>
                  <nts id="Seg_2760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_2762" n="HIAT:w" s="T715">tʼärɨkuzat</ts>
                  <nts id="Seg_2763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2765" n="HIAT:w" s="T716">što</ts>
                  <nts id="Seg_2766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2768" n="HIAT:w" s="T717">man</ts>
                  <nts id="Seg_2769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2771" n="HIAT:w" s="T718">orse</ts>
                  <nts id="Seg_2772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2774" n="HIAT:w" s="T719">jewan</ts>
                  <nts id="Seg_2775" n="HIAT:ip">.</nts>
                  <nts id="Seg_2776" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T725" id="Seg_2778" n="HIAT:u" s="T720">
                  <ts e="T721" id="Seg_2780" n="HIAT:w" s="T720">A</ts>
                  <nts id="Seg_2781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_2783" n="HIAT:w" s="T721">qajno</ts>
                  <nts id="Seg_2784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_2786" n="HIAT:w" s="T722">qudɨgom</ts>
                  <nts id="Seg_2787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_2789" n="HIAT:w" s="T723">ass</ts>
                  <nts id="Seg_2790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_2792" n="HIAT:w" s="T724">laqčeǯal</ts>
                  <nts id="Seg_2793" n="HIAT:ip">?</nts>
                  <nts id="Seg_2794" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T729" id="Seg_2796" n="HIAT:u" s="T725">
                  <ts e="T726" id="Seg_2798" n="HIAT:w" s="T725">Tapär</ts>
                  <nts id="Seg_2799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2801" n="HIAT:w" s="T726">qarʼemɨɣɨn</ts>
                  <nts id="Seg_2802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_2804" n="HIAT:w" s="T727">tastɨ</ts>
                  <nts id="Seg_2805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2807" n="HIAT:w" s="T728">qwatčaj</ts>
                  <nts id="Seg_2808" n="HIAT:ip">.</nts>
                  <nts id="Seg_2809" n="HIAT:ip">”</nts>
                  <nts id="Seg_2810" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T738" id="Seg_2812" n="HIAT:u" s="T729">
                  <ts e="T730" id="Seg_2814" n="HIAT:w" s="T729">Näjɣum</ts>
                  <nts id="Seg_2815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2817" n="HIAT:w" s="T730">quronnɨn</ts>
                  <nts id="Seg_2818" n="HIAT:ip">,</nts>
                  <nts id="Seg_2819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_2821" n="HIAT:w" s="T731">saːq</ts>
                  <nts id="Seg_2822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2824" n="HIAT:w" s="T732">tulǯɨtdɨt</ts>
                  <nts id="Seg_2825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2827" n="HIAT:w" s="T733">wädran</ts>
                  <nts id="Seg_2828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_2830" n="HIAT:w" s="T734">tiːr</ts>
                  <nts id="Seg_2831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_2833" n="HIAT:w" s="T735">i</ts>
                  <nts id="Seg_2834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_2836" n="HIAT:w" s="T736">salǯɨbot</ts>
                  <nts id="Seg_2837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_2839" n="HIAT:w" s="T737">qamǯɨt</ts>
                  <nts id="Seg_2840" n="HIAT:ip">.</nts>
                  <nts id="Seg_2841" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T742" id="Seg_2843" n="HIAT:u" s="T738">
                  <ts e="T739" id="Seg_2845" n="HIAT:w" s="T738">I</ts>
                  <nts id="Seg_2846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_2848" n="HIAT:w" s="T739">täbɨm</ts>
                  <nts id="Seg_2849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_2851" n="HIAT:w" s="T740">puluzein</ts>
                  <nts id="Seg_2852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_2854" n="HIAT:w" s="T741">omdɨlǯɨt</ts>
                  <nts id="Seg_2855" n="HIAT:ip">.</nts>
                  <nts id="Seg_2856" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T745" id="Seg_2858" n="HIAT:u" s="T742">
                  <ts e="T743" id="Seg_2860" n="HIAT:w" s="T742">Tʼärɨn</ts>
                  <nts id="Seg_2861" n="HIAT:ip">:</nts>
                  <nts id="Seg_2862" n="HIAT:ip">“</nts>
                  <nts id="Seg_2863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2864" n="HIAT:ip">“</nts>
                  <ts e="T744" id="Seg_2866" n="HIAT:w" s="T743">Aːmdaq</ts>
                  <nts id="Seg_2867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_2869" n="HIAT:w" s="T744">qarʼemɨtdə</ts>
                  <nts id="Seg_2870" n="HIAT:ip">!</nts>
                  <nts id="Seg_2871" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T748" id="Seg_2873" n="HIAT:u" s="T745">
                  <ts e="T746" id="Seg_2875" n="HIAT:w" s="T745">Qarʼemɨɣɨn</ts>
                  <nts id="Seg_2876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_2878" n="HIAT:w" s="T746">tastɨ</ts>
                  <nts id="Seg_2879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_2881" n="HIAT:w" s="T747">qwatčaj</ts>
                  <nts id="Seg_2882" n="HIAT:ip">.</nts>
                  <nts id="Seg_2883" n="HIAT:ip">”</nts>
                  <nts id="Seg_2884" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T754" id="Seg_2886" n="HIAT:u" s="T748">
                  <ts e="T749" id="Seg_2888" n="HIAT:w" s="T748">Omdaɣə</ts>
                  <nts id="Seg_2889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_2891" n="HIAT:w" s="T749">aurlʼe</ts>
                  <nts id="Seg_2892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_2894" n="HIAT:w" s="T750">razbojnikse</ts>
                  <nts id="Seg_2895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_2897" n="HIAT:w" s="T751">arakaj</ts>
                  <nts id="Seg_2898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_2900" n="HIAT:w" s="T752">ärlʼe</ts>
                  <nts id="Seg_2901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_2903" n="HIAT:w" s="T753">jübɨraq</ts>
                  <nts id="Seg_2904" n="HIAT:ip">.</nts>
                  <nts id="Seg_2905" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T762" id="Seg_2907" n="HIAT:u" s="T754">
                  <ts e="T755" id="Seg_2909" n="HIAT:w" s="T754">Näjɣum</ts>
                  <nts id="Seg_2910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_2912" n="HIAT:w" s="T755">tüuwa</ts>
                  <nts id="Seg_2913" n="HIAT:ip">,</nts>
                  <nts id="Seg_2914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_2916" n="HIAT:w" s="T756">äratdə</ts>
                  <nts id="Seg_2917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_2919" n="HIAT:w" s="T757">kötdə</ts>
                  <nts id="Seg_2920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_2922" n="HIAT:w" s="T758">äramdə</ts>
                  <nts id="Seg_2923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_2925" n="HIAT:w" s="T759">watdoot</ts>
                  <nts id="Seg_2926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_2928" n="HIAT:w" s="T760">na</ts>
                  <nts id="Seg_2929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_2931" n="HIAT:w" s="T761">sözɨtdət</ts>
                  <nts id="Seg_2932" n="HIAT:ip">.</nts>
                  <nts id="Seg_2933" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T766" id="Seg_2935" n="HIAT:u" s="T762">
                  <nts id="Seg_2936" n="HIAT:ip">“</nts>
                  <ts e="T763" id="Seg_2938" n="HIAT:w" s="T762">Qarʼemɨɣɨn</ts>
                  <nts id="Seg_2939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_2941" n="HIAT:w" s="T763">me</ts>
                  <nts id="Seg_2942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_2944" n="HIAT:w" s="T764">tastɨ</ts>
                  <nts id="Seg_2945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_2947" n="HIAT:w" s="T765">qwatčaj</ts>
                  <nts id="Seg_2948" n="HIAT:ip">.</nts>
                  <nts id="Seg_2949" n="HIAT:ip">”</nts>
                  <nts id="Seg_2950" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T773" id="Seg_2952" n="HIAT:u" s="T766">
                  <ts e="T767" id="Seg_2954" n="HIAT:w" s="T766">Otdə</ts>
                  <nts id="Seg_2955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T768" id="Seg_2957" n="HIAT:w" s="T767">na</ts>
                  <nts id="Seg_2958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_2960" n="HIAT:w" s="T768">qwatda</ts>
                  <nts id="Seg_2961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_2963" n="HIAT:w" s="T769">razbojnikɨm</ts>
                  <nts id="Seg_2964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T771" id="Seg_2966" n="HIAT:w" s="T770">kaːwalgut</ts>
                  <nts id="Seg_2967" n="HIAT:ip">,</nts>
                  <nts id="Seg_2968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_2970" n="HIAT:w" s="T771">niːdellʼe</ts>
                  <nts id="Seg_2971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_2973" n="HIAT:w" s="T772">taːdɨrɨt</ts>
                  <nts id="Seg_2974" n="HIAT:ip">.</nts>
                  <nts id="Seg_2975" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T779" id="Seg_2977" n="HIAT:u" s="T773">
                  <ts e="T774" id="Seg_2979" n="HIAT:w" s="T773">I</ts>
                  <nts id="Seg_2980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_2982" n="HIAT:w" s="T774">qotduɣu</ts>
                  <nts id="Seg_2983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_2985" n="HIAT:w" s="T775">quʒannax</ts>
                  <nts id="Seg_2986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_2988" n="HIAT:w" s="T776">i</ts>
                  <nts id="Seg_2989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_2991" n="HIAT:w" s="T777">na</ts>
                  <nts id="Seg_2992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_2994" n="HIAT:w" s="T778">qotdolǯaɣɨ</ts>
                  <nts id="Seg_2995" n="HIAT:ip">.</nts>
                  <nts id="Seg_2996" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T784" id="Seg_2998" n="HIAT:u" s="T779">
                  <nts id="Seg_2999" n="HIAT:ip">(</nts>
                  <ts e="T780" id="Seg_3001" n="HIAT:w" s="T779">Ärat</ts>
                  <nts id="Seg_3002" n="HIAT:ip">)</nts>
                  <nts id="Seg_3003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_3005" n="HIAT:w" s="T780">täp</ts>
                  <nts id="Seg_3006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_3008" n="HIAT:w" s="T781">wazɨn</ts>
                  <nts id="Seg_3009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_3011" n="HIAT:w" s="T782">innä</ts>
                  <nts id="Seg_3012" n="HIAT:ip">,</nts>
                  <nts id="Seg_3013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_3015" n="HIAT:w" s="T783">paldʼukun</ts>
                  <nts id="Seg_3016" n="HIAT:ip">.</nts>
                  <nts id="Seg_3017" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T792" id="Seg_3019" n="HIAT:u" s="T784">
                  <ts e="T785" id="Seg_3021" n="HIAT:w" s="T784">A</ts>
                  <nts id="Seg_3022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T786" id="Seg_3024" n="HIAT:w" s="T785">kɨːban</ts>
                  <nts id="Seg_3025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_3027" n="HIAT:w" s="T786">nägat</ts>
                  <nts id="Seg_3028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_3030" n="HIAT:w" s="T787">iːɣat</ts>
                  <nts id="Seg_3031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_3033" n="HIAT:w" s="T788">täbɨstaːɣə</ts>
                  <nts id="Seg_3034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_3036" n="HIAT:w" s="T789">qotdɨzaɣə</ts>
                  <nts id="Seg_3037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_3039" n="HIAT:w" s="T790">palatin</ts>
                  <nts id="Seg_3040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_3042" n="HIAT:w" s="T791">paroɣɨn</ts>
                  <nts id="Seg_3043" n="HIAT:ip">.</nts>
                  <nts id="Seg_3044" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T796" id="Seg_3046" n="HIAT:u" s="T792">
                  <ts e="T793" id="Seg_3048" n="HIAT:w" s="T792">Nägat</ts>
                  <nts id="Seg_3049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_3051" n="HIAT:w" s="T793">küzɨgu</ts>
                  <nts id="Seg_3052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_3054" n="HIAT:w" s="T794">ilʼlʼe</ts>
                  <nts id="Seg_3055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_3057" n="HIAT:w" s="T795">tʼütʼöun</ts>
                  <nts id="Seg_3058" n="HIAT:ip">.</nts>
                  <nts id="Seg_3059" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T802" id="Seg_3061" n="HIAT:u" s="T796">
                  <ts e="T797" id="Seg_3063" n="HIAT:w" s="T796">Täp</ts>
                  <nts id="Seg_3064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_3066" n="HIAT:w" s="T797">nägatdanä</ts>
                  <nts id="Seg_3067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_3069" n="HIAT:w" s="T798">tʼärɨn</ts>
                  <nts id="Seg_3070" n="HIAT:ip">:</nts>
                  <nts id="Seg_3071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3072" n="HIAT:ip">“</nts>
                  <ts e="T800" id="Seg_3074" n="HIAT:w" s="T799">Man</ts>
                  <nts id="Seg_3075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_3077" n="HIAT:w" s="T800">udlau</ts>
                  <nts id="Seg_3078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_3080" n="HIAT:w" s="T801">tʼikɨlʼel</ts>
                  <nts id="Seg_3081" n="HIAT:ip">.</nts>
                  <nts id="Seg_3082" n="HIAT:ip">”</nts>
                  <nts id="Seg_3083" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T810" id="Seg_3085" n="HIAT:u" s="T802">
                  <ts e="T803" id="Seg_3087" n="HIAT:w" s="T802">A</ts>
                  <nts id="Seg_3088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_3090" n="HIAT:w" s="T803">nägat</ts>
                  <nts id="Seg_3091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_3093" n="HIAT:w" s="T804">tʼärɨn</ts>
                  <nts id="Seg_3094" n="HIAT:ip">:</nts>
                  <nts id="Seg_3095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3096" n="HIAT:ip">“</nts>
                  <ts e="T806" id="Seg_3098" n="HIAT:w" s="T805">Tan</ts>
                  <nts id="Seg_3099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_3101" n="HIAT:w" s="T806">tʼärukuzat</ts>
                  <nts id="Seg_3102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_3104" n="HIAT:w" s="T807">što</ts>
                  <nts id="Seg_3105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_3107" n="HIAT:w" s="T808">orse</ts>
                  <nts id="Seg_3108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_3110" n="HIAT:w" s="T809">jewan</ts>
                  <nts id="Seg_3111" n="HIAT:ip">.</nts>
                  <nts id="Seg_3112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T812" id="Seg_3114" n="HIAT:u" s="T810">
                  <ts e="T811" id="Seg_3116" n="HIAT:w" s="T810">Lakčelǯet</ts>
                  <nts id="Seg_3117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_3119" n="HIAT:w" s="T811">qudɨgom</ts>
                  <nts id="Seg_3120" n="HIAT:ip">.</nts>
                  <nts id="Seg_3121" n="HIAT:ip">”</nts>
                  <nts id="Seg_3122" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T817" id="Seg_3124" n="HIAT:u" s="T812">
                  <ts e="T813" id="Seg_3126" n="HIAT:w" s="T812">Palatin</ts>
                  <nts id="Seg_3127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T814" id="Seg_3129" n="HIAT:w" s="T813">barot</ts>
                  <nts id="Seg_3130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_3132" n="HIAT:w" s="T814">čaǯɨn</ts>
                  <nts id="Seg_3133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_3135" n="HIAT:w" s="T815">i</ts>
                  <nts id="Seg_3136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_3138" n="HIAT:w" s="T816">qotda</ts>
                  <nts id="Seg_3139" n="HIAT:ip">.</nts>
                  <nts id="Seg_3140" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T822" id="Seg_3142" n="HIAT:u" s="T817">
                  <ts e="T818" id="Seg_3144" n="HIAT:w" s="T817">Iːgat</ts>
                  <nts id="Seg_3145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_3147" n="HIAT:w" s="T818">palatiɣəndo</ts>
                  <nts id="Seg_3148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T820" id="Seg_3150" n="HIAT:w" s="T819">ilʼlʼe</ts>
                  <nts id="Seg_3151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T821" id="Seg_3153" n="HIAT:w" s="T820">tʼütʼöun</ts>
                  <nts id="Seg_3154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_3156" n="HIAT:w" s="T821">küzɨgu</ts>
                  <nts id="Seg_3157" n="HIAT:ip">.</nts>
                  <nts id="Seg_3158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T828" id="Seg_3160" n="HIAT:u" s="T822">
                  <ts e="T823" id="Seg_3162" n="HIAT:w" s="T822">Täp</ts>
                  <nts id="Seg_3163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T824" id="Seg_3165" n="HIAT:w" s="T823">iːgatdänä</ts>
                  <nts id="Seg_3166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_3168" n="HIAT:w" s="T824">tʼärɨn</ts>
                  <nts id="Seg_3169" n="HIAT:ip">:</nts>
                  <nts id="Seg_3170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3171" n="HIAT:ip">“</nts>
                  <ts e="T826" id="Seg_3173" n="HIAT:w" s="T825">Man</ts>
                  <nts id="Seg_3174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_3176" n="HIAT:w" s="T826">udou</ts>
                  <nts id="Seg_3177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T828" id="Seg_3179" n="HIAT:w" s="T827">tʼiːkelʼel</ts>
                  <nts id="Seg_3180" n="HIAT:ip">.</nts>
                  <nts id="Seg_3181" n="HIAT:ip">”</nts>
                  <nts id="Seg_3182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T834" id="Seg_3184" n="HIAT:u" s="T828">
                  <ts e="T829" id="Seg_3186" n="HIAT:w" s="T828">Täp</ts>
                  <nts id="Seg_3187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_3189" n="HIAT:w" s="T829">tüwa</ts>
                  <nts id="Seg_3190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T831" id="Seg_3192" n="HIAT:w" s="T830">äzɨtdänä</ts>
                  <nts id="Seg_3193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_3195" n="HIAT:w" s="T831">i</ts>
                  <nts id="Seg_3196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_3198" n="HIAT:w" s="T832">tʼikəlʼe</ts>
                  <nts id="Seg_3199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_3201" n="HIAT:w" s="T833">jübərat</ts>
                  <nts id="Seg_3202" n="HIAT:ip">.</nts>
                  <nts id="Seg_3203" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T836" id="Seg_3205" n="HIAT:u" s="T834">
                  <ts e="T835" id="Seg_3207" n="HIAT:w" s="T834">Jass</ts>
                  <nts id="Seg_3208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T836" id="Seg_3210" n="HIAT:w" s="T835">tʼikɨt</ts>
                  <nts id="Seg_3211" n="HIAT:ip">.</nts>
                  <nts id="Seg_3212" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T839" id="Seg_3214" n="HIAT:u" s="T836">
                  <nts id="Seg_3215" n="HIAT:ip">“</nts>
                  <ts e="T837" id="Seg_3217" n="HIAT:w" s="T836">Mekga</ts>
                  <nts id="Seg_3218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_3220" n="HIAT:w" s="T837">jass</ts>
                  <nts id="Seg_3221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T839" id="Seg_3223" n="HIAT:w" s="T838">tʼikɨɣuː</ts>
                  <nts id="Seg_3224" n="HIAT:ip">.</nts>
                  <nts id="Seg_3225" n="HIAT:ip">”</nts>
                  <nts id="Seg_3226" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T843" id="Seg_3228" n="HIAT:u" s="T839">
                  <ts e="T840" id="Seg_3230" n="HIAT:w" s="T839">Ästɨ</ts>
                  <nts id="Seg_3231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T841" id="Seg_3233" n="HIAT:w" s="T840">tʼärɨn</ts>
                  <nts id="Seg_3234" n="HIAT:ip">:</nts>
                  <nts id="Seg_3235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3236" n="HIAT:ip">“</nts>
                  <ts e="T842" id="Seg_3238" n="HIAT:w" s="T841">Patpʼilka</ts>
                  <nts id="Seg_3239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_3241" n="HIAT:w" s="T842">datkə</ts>
                  <nts id="Seg_3242" n="HIAT:ip">.</nts>
                  <nts id="Seg_3243" n="HIAT:ip">”</nts>
                  <nts id="Seg_3244" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T850" id="Seg_3246" n="HIAT:u" s="T843">
                  <ts e="T844" id="Seg_3248" n="HIAT:w" s="T843">Täp</ts>
                  <nts id="Seg_3249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T845" id="Seg_3251" n="HIAT:w" s="T844">tannɨt</ts>
                  <nts id="Seg_3252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_3254" n="HIAT:w" s="T845">i</ts>
                  <nts id="Seg_3255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_3257" n="HIAT:w" s="T846">kudɨgolam</ts>
                  <nts id="Seg_3258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T848" id="Seg_3260" n="HIAT:w" s="T847">sɨlʼelʼlʼe</ts>
                  <nts id="Seg_3261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T849" id="Seg_3263" n="HIAT:w" s="T848">na</ts>
                  <nts id="Seg_3264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T850" id="Seg_3266" n="HIAT:w" s="T849">übərətdɨt</ts>
                  <nts id="Seg_3267" n="HIAT:ip">.</nts>
                  <nts id="Seg_3268" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T853" id="Seg_3270" n="HIAT:u" s="T850">
                  <ts e="T851" id="Seg_3272" n="HIAT:w" s="T850">Sɨlɨt</ts>
                  <nts id="Seg_3273" n="HIAT:ip">,</nts>
                  <nts id="Seg_3274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_3276" n="HIAT:w" s="T851">qudəgola</ts>
                  <nts id="Seg_3277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T853" id="Seg_3279" n="HIAT:w" s="T852">warsɨwannat</ts>
                  <nts id="Seg_3280" n="HIAT:ip">.</nts>
                  <nts id="Seg_3281" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T858" id="Seg_3283" n="HIAT:u" s="T853">
                  <ts e="T854" id="Seg_3285" n="HIAT:w" s="T853">Täp</ts>
                  <nts id="Seg_3286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T855" id="Seg_3288" n="HIAT:w" s="T854">iːgatdänä</ts>
                  <nts id="Seg_3289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856" id="Seg_3291" n="HIAT:w" s="T855">tʼarɨn</ts>
                  <nts id="Seg_3292" n="HIAT:ip">:</nts>
                  <nts id="Seg_3293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3294" n="HIAT:ip">“</nts>
                  <ts e="T857" id="Seg_3296" n="HIAT:w" s="T856">Qwalʼlʼe</ts>
                  <nts id="Seg_3297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T858" id="Seg_3299" n="HIAT:w" s="T857">qotdaq</ts>
                  <nts id="Seg_3300" n="HIAT:ip">.</nts>
                  <nts id="Seg_3301" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T861" id="Seg_3303" n="HIAT:u" s="T858">
                  <ts e="T859" id="Seg_3305" n="HIAT:w" s="T858">Qarʼemɨɣɨn</ts>
                  <nts id="Seg_3306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T860" id="Seg_3308" n="HIAT:w" s="T859">iːgə</ts>
                  <nts id="Seg_3309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T861" id="Seg_3311" n="HIAT:w" s="T860">larɨpbaːk</ts>
                  <nts id="Seg_3312" n="HIAT:ip">.</nts>
                  <nts id="Seg_3313" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T865" id="Seg_3315" n="HIAT:u" s="T861">
                  <ts e="T862" id="Seg_3317" n="HIAT:w" s="T861">Me</ts>
                  <nts id="Seg_3318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T863" id="Seg_3320" n="HIAT:w" s="T862">tazʼe</ts>
                  <nts id="Seg_3321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864" id="Seg_3323" n="HIAT:w" s="T863">onäj</ts>
                  <nts id="Seg_3324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T865" id="Seg_3326" n="HIAT:w" s="T864">ilʼetǯaj</ts>
                  <nts id="Seg_3327" n="HIAT:ip">.</nts>
                  <nts id="Seg_3328" n="HIAT:ip">”</nts>
                  <nts id="Seg_3329" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T868" id="Seg_3331" n="HIAT:u" s="T865">
                  <ts e="T866" id="Seg_3333" n="HIAT:w" s="T865">Nom</ts>
                  <nts id="Seg_3334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T867" id="Seg_3336" n="HIAT:w" s="T866">tʼelɨmlʼe</ts>
                  <nts id="Seg_3337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T868" id="Seg_3339" n="HIAT:w" s="T867">jübɨrɨn</ts>
                  <nts id="Seg_3340" n="HIAT:ip">.</nts>
                  <nts id="Seg_3341" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T879" id="Seg_3343" n="HIAT:u" s="T868">
                  <ts e="T869" id="Seg_3345" n="HIAT:w" s="T868">Täp</ts>
                  <nts id="Seg_3346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T870" id="Seg_3348" n="HIAT:w" s="T869">kudɨgolam</ts>
                  <nts id="Seg_3349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T871" id="Seg_3351" n="HIAT:w" s="T870">udoɣətdə</ts>
                  <nts id="Seg_3352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T872" id="Seg_3354" n="HIAT:w" s="T871">pennɨt</ts>
                  <nts id="Seg_3355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3356" n="HIAT:ip">(</nts>
                  <ts e="T873" id="Seg_3358" n="HIAT:w" s="T872">kak</ts>
                  <nts id="Seg_3359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T874" id="Seg_3361" n="HIAT:w" s="T873">budto</ts>
                  <nts id="Seg_3362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T875" id="Seg_3364" n="HIAT:w" s="T874">saːrɨpbaːt</ts>
                  <nts id="Seg_3365" n="HIAT:ip">)</nts>
                  <nts id="Seg_3366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_3368" n="HIAT:w" s="T875">i</ts>
                  <nts id="Seg_3369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T877" id="Seg_3371" n="HIAT:w" s="T876">omdɨn</ts>
                  <nts id="Seg_3372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T878" id="Seg_3374" n="HIAT:w" s="T877">puluzein</ts>
                  <nts id="Seg_3375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T879" id="Seg_3377" n="HIAT:w" s="T878">parot</ts>
                  <nts id="Seg_3378" n="HIAT:ip">.</nts>
                  <nts id="Seg_3379" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T884" id="Seg_3381" n="HIAT:u" s="T879">
                  <ts e="T880" id="Seg_3383" n="HIAT:w" s="T879">Pajät</ts>
                  <nts id="Seg_3384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T881" id="Seg_3386" n="HIAT:w" s="T880">wazɨn</ts>
                  <nts id="Seg_3387" n="HIAT:ip">,</nts>
                  <nts id="Seg_3388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T882" id="Seg_3390" n="HIAT:w" s="T881">tülʼe</ts>
                  <nts id="Seg_3391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T883" id="Seg_3393" n="HIAT:w" s="T882">watdoɨn</ts>
                  <nts id="Seg_3394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T884" id="Seg_3396" n="HIAT:w" s="T883">söːzɨt</ts>
                  <nts id="Seg_3397" n="HIAT:ip">.</nts>
                  <nts id="Seg_3398" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T888" id="Seg_3400" n="HIAT:u" s="T884">
                  <ts e="T885" id="Seg_3402" n="HIAT:w" s="T884">Razbojnignä</ts>
                  <nts id="Seg_3403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T886" id="Seg_3405" n="HIAT:w" s="T885">tʼärɨn</ts>
                  <nts id="Seg_3406" n="HIAT:ip">:</nts>
                  <nts id="Seg_3407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3408" n="HIAT:ip">“</nts>
                  <ts e="T887" id="Seg_3410" n="HIAT:w" s="T886">Nu</ts>
                  <nts id="Seg_3411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T888" id="Seg_3413" n="HIAT:w" s="T887">qwallaj</ts>
                  <nts id="Seg_3414" n="HIAT:ip">.</nts>
                  <nts id="Seg_3415" n="HIAT:ip">”</nts>
                  <nts id="Seg_3416" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T893" id="Seg_3418" n="HIAT:u" s="T888">
                  <ts e="T889" id="Seg_3420" n="HIAT:w" s="T888">A</ts>
                  <nts id="Seg_3421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T890" id="Seg_3423" n="HIAT:w" s="T889">razbojnik</ts>
                  <nts id="Seg_3424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T891" id="Seg_3426" n="HIAT:w" s="T890">tʼärɨn</ts>
                  <nts id="Seg_3427" n="HIAT:ip">:</nts>
                  <nts id="Seg_3428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3429" n="HIAT:ip">“</nts>
                  <ts e="T892" id="Seg_3431" n="HIAT:w" s="T891">Aurlʼe</ts>
                  <nts id="Seg_3432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T893" id="Seg_3434" n="HIAT:w" s="T892">qwallajze</ts>
                  <nts id="Seg_3435" n="HIAT:ip">.</nts>
                  <nts id="Seg_3436" n="HIAT:ip">”</nts>
                  <nts id="Seg_3437" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T896" id="Seg_3439" n="HIAT:u" s="T893">
                  <ts e="T894" id="Seg_3441" n="HIAT:w" s="T893">Tep</ts>
                  <nts id="Seg_3442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T895" id="Seg_3444" n="HIAT:w" s="T894">wadʼilam</ts>
                  <nts id="Seg_3445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T896" id="Seg_3447" n="HIAT:w" s="T895">müzurɣɨnnät</ts>
                  <nts id="Seg_3448" n="HIAT:ip">.</nts>
                  <nts id="Seg_3449" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T897" id="Seg_3451" n="HIAT:u" s="T896">
                  <ts e="T897" id="Seg_3453" n="HIAT:w" s="T896">Aurnaɣə</ts>
                  <nts id="Seg_3454" n="HIAT:ip">.</nts>
                  <nts id="Seg_3455" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T900" id="Seg_3457" n="HIAT:u" s="T897">
                  <nts id="Seg_3458" n="HIAT:ip">“</nts>
                  <ts e="T898" id="Seg_3460" n="HIAT:w" s="T897">Nu</ts>
                  <nts id="Seg_3461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T899" id="Seg_3463" n="HIAT:w" s="T898">täpär</ts>
                  <nts id="Seg_3464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T900" id="Seg_3466" n="HIAT:w" s="T899">qwallaj</ts>
                  <nts id="Seg_3467" n="HIAT:ip">.</nts>
                  <nts id="Seg_3468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T904" id="Seg_3470" n="HIAT:u" s="T900">
                  <ts e="T901" id="Seg_3472" n="HIAT:w" s="T900">Maːtqɨn</ts>
                  <nts id="Seg_3473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T902" id="Seg_3475" n="HIAT:w" s="T901">qwatčaj</ts>
                  <nts id="Seg_3476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T903" id="Seg_3478" n="HIAT:w" s="T902">alʼi</ts>
                  <nts id="Seg_3479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T904" id="Seg_3481" n="HIAT:w" s="T903">ponän</ts>
                  <nts id="Seg_3482" n="HIAT:ip">?</nts>
                  <nts id="Seg_3483" n="HIAT:ip">”</nts>
                  <nts id="Seg_3484" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T908" id="Seg_3486" n="HIAT:u" s="T904">
                  <ts e="T905" id="Seg_3488" n="HIAT:w" s="T904">Razbojnik</ts>
                  <nts id="Seg_3489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T906" id="Seg_3491" n="HIAT:w" s="T905">tʼärɨn</ts>
                  <nts id="Seg_3492" n="HIAT:ip">:</nts>
                  <nts id="Seg_3493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3494" n="HIAT:ip">“</nts>
                  <ts e="T907" id="Seg_3496" n="HIAT:w" s="T906">Ponän</ts>
                  <nts id="Seg_3497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T908" id="Seg_3499" n="HIAT:w" s="T907">qwatčaj</ts>
                  <nts id="Seg_3500" n="HIAT:ip">.</nts>
                  <nts id="Seg_3501" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T912" id="Seg_3503" n="HIAT:u" s="T908">
                  <ts e="T909" id="Seg_3505" n="HIAT:w" s="T908">Nu</ts>
                  <nts id="Seg_3506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T910" id="Seg_3508" n="HIAT:w" s="T909">wazak</ts>
                  <nts id="Seg_3509" n="HIAT:ip">,</nts>
                  <nts id="Seg_3510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T911" id="Seg_3512" n="HIAT:w" s="T910">čaːʒɨk</ts>
                  <nts id="Seg_3513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T912" id="Seg_3515" n="HIAT:w" s="T911">ponä</ts>
                  <nts id="Seg_3516" n="HIAT:ip">.</nts>
                  <nts id="Seg_3517" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T914" id="Seg_3519" n="HIAT:u" s="T912">
                  <ts e="T913" id="Seg_3521" n="HIAT:w" s="T912">Tastɨ</ts>
                  <nts id="Seg_3522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T914" id="Seg_3524" n="HIAT:w" s="T913">qwatčaj</ts>
                  <nts id="Seg_3525" n="HIAT:ip">.</nts>
                  <nts id="Seg_3526" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T918" id="Seg_3528" n="HIAT:u" s="T914">
                  <ts e="T915" id="Seg_3530" n="HIAT:w" s="T914">Qaj</ts>
                  <nts id="Seg_3531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T916" id="Seg_3533" n="HIAT:w" s="T915">kudɨgolam</ts>
                  <nts id="Seg_3534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T917" id="Seg_3536" n="HIAT:w" s="T916">jass</ts>
                  <nts id="Seg_3537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T918" id="Seg_3539" n="HIAT:w" s="T917">lakčelǯal</ts>
                  <nts id="Seg_3540" n="HIAT:ip">.</nts>
                  <nts id="Seg_3541" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T922" id="Seg_3543" n="HIAT:u" s="T918">
                  <ts e="T919" id="Seg_3545" n="HIAT:w" s="T918">Tan</ts>
                  <nts id="Seg_3546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T920" id="Seg_3548" n="HIAT:w" s="T919">tʼärukuzattə</ts>
                  <nts id="Seg_3549" n="HIAT:ip">,</nts>
                  <nts id="Seg_3550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T921" id="Seg_3552" n="HIAT:w" s="T920">orse</ts>
                  <nts id="Seg_3553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T922" id="Seg_3555" n="HIAT:w" s="T921">jewan</ts>
                  <nts id="Seg_3556" n="HIAT:ip">.</nts>
                  <nts id="Seg_3557" n="HIAT:ip">”</nts>
                  <nts id="Seg_3558" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T926" id="Seg_3560" n="HIAT:u" s="T922">
                  <ts e="T923" id="Seg_3562" n="HIAT:w" s="T922">Näuɣum</ts>
                  <nts id="Seg_3563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T924" id="Seg_3565" n="HIAT:w" s="T923">kak</ts>
                  <nts id="Seg_3566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T925" id="Seg_3568" n="HIAT:w" s="T924">watdoutdə</ts>
                  <nts id="Seg_3569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T926" id="Seg_3571" n="HIAT:w" s="T925">sözɨtdɨt</ts>
                  <nts id="Seg_3572" n="HIAT:ip">.</nts>
                  <nts id="Seg_3573" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T933" id="Seg_3575" n="HIAT:u" s="T926">
                  <ts e="T927" id="Seg_3577" n="HIAT:w" s="T926">Täp</ts>
                  <nts id="Seg_3578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T928" id="Seg_3580" n="HIAT:w" s="T927">kak</ts>
                  <nts id="Seg_3581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T929" id="Seg_3583" n="HIAT:w" s="T928">innä</ts>
                  <nts id="Seg_3584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T930" id="Seg_3586" n="HIAT:w" s="T929">wazʼezitda</ts>
                  <nts id="Seg_3587" n="HIAT:ip">,</nts>
                  <nts id="Seg_3588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T931" id="Seg_3590" n="HIAT:w" s="T930">kak</ts>
                  <nts id="Seg_3591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T932" id="Seg_3593" n="HIAT:w" s="T931">razbojnikam</ts>
                  <nts id="Seg_3594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T933" id="Seg_3596" n="HIAT:w" s="T932">qättɨdɨt</ts>
                  <nts id="Seg_3597" n="HIAT:ip">.</nts>
                  <nts id="Seg_3598" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T937" id="Seg_3600" n="HIAT:u" s="T933">
                  <ts e="T934" id="Seg_3602" n="HIAT:w" s="T933">Razbojnik</ts>
                  <nts id="Seg_3603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T935" id="Seg_3605" n="HIAT:w" s="T934">qotä</ts>
                  <nts id="Seg_3606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T936" id="Seg_3608" n="HIAT:w" s="T935">čäčädʼzʼen</ts>
                  <nts id="Seg_3609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3610" n="HIAT:ip">(</nts>
                  <ts e="T937" id="Seg_3612" n="HIAT:w" s="T936">čäčädin</ts>
                  <nts id="Seg_3613" n="HIAT:ip">)</nts>
                  <nts id="Seg_3614" n="HIAT:ip">.</nts>
                  <nts id="Seg_3615" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T939" id="Seg_3617" n="HIAT:u" s="T937">
                  <ts e="T938" id="Seg_3619" n="HIAT:w" s="T937">I</ts>
                  <nts id="Seg_3620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T939" id="Seg_3622" n="HIAT:w" s="T938">qwannɨt</ts>
                  <nts id="Seg_3623" n="HIAT:ip">.</nts>
                  <nts id="Seg_3624" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T943" id="Seg_3626" n="HIAT:u" s="T939">
                  <ts e="T940" id="Seg_3628" n="HIAT:w" s="T939">A</ts>
                  <nts id="Seg_3629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T941" id="Seg_3631" n="HIAT:w" s="T940">pajät</ts>
                  <nts id="Seg_3632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T942" id="Seg_3634" n="HIAT:w" s="T941">sojoɣotdə</ts>
                  <nts id="Seg_3635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T943" id="Seg_3637" n="HIAT:w" s="T942">ɨdədʼen</ts>
                  <nts id="Seg_3638" n="HIAT:ip">.</nts>
                  <nts id="Seg_3639" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T948" id="Seg_3641" n="HIAT:u" s="T943">
                  <ts e="T944" id="Seg_3643" n="HIAT:w" s="T943">Tʼärɨn</ts>
                  <nts id="Seg_3644" n="HIAT:ip">:</nts>
                  <nts id="Seg_3645" n="HIAT:ip">“</nts>
                  <nts id="Seg_3646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3647" n="HIAT:ip">“</nts>
                  <ts e="T945" id="Seg_3649" n="HIAT:w" s="T944">Man</ts>
                  <nts id="Seg_3650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T946" id="Seg_3652" n="HIAT:w" s="T945">nɨlʼdʼin</ts>
                  <nts id="Seg_3653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T947" id="Seg_3655" n="HIAT:w" s="T946">ass</ts>
                  <nts id="Seg_3656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T948" id="Seg_3658" n="HIAT:w" s="T947">meːqweǯau</ts>
                  <nts id="Seg_3659" n="HIAT:ip">.</nts>
                  <nts id="Seg_3660" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T950" id="Seg_3662" n="HIAT:u" s="T948">
                  <ts e="T949" id="Seg_3664" n="HIAT:w" s="T948">Toblamdə</ts>
                  <nts id="Seg_3665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T950" id="Seg_3667" n="HIAT:w" s="T949">müzulǯukeǯau</ts>
                  <nts id="Seg_3668" n="HIAT:ip">.</nts>
                  <nts id="Seg_3669" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T953" id="Seg_3671" n="HIAT:u" s="T950">
                  <ts e="T951" id="Seg_3673" n="HIAT:w" s="T950">Na</ts>
                  <nts id="Seg_3674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T952" id="Seg_3676" n="HIAT:w" s="T951">ödɨm</ts>
                  <nts id="Seg_3677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T953" id="Seg_3679" n="HIAT:w" s="T952">ütkeǯau</ts>
                  <nts id="Seg_3680" n="HIAT:ip">.</nts>
                  <nts id="Seg_3681" n="HIAT:ip">”</nts>
                  <nts id="Seg_3682" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T956" id="Seg_3684" n="HIAT:u" s="T953">
                  <ts e="T954" id="Seg_3686" n="HIAT:w" s="T953">Erat</ts>
                  <nts id="Seg_3687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T955" id="Seg_3689" n="HIAT:w" s="T954">kak</ts>
                  <nts id="Seg_3690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T956" id="Seg_3692" n="HIAT:w" s="T955">qäːtäǯɨt</ts>
                  <nts id="Seg_3693" n="HIAT:ip">.</nts>
                  <nts id="Seg_3694" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T958" id="Seg_3696" n="HIAT:u" s="T956">
                  <ts e="T957" id="Seg_3698" n="HIAT:w" s="T956">I</ts>
                  <nts id="Seg_3699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T958" id="Seg_3701" n="HIAT:w" s="T957">qwannɨt</ts>
                  <nts id="Seg_3702" n="HIAT:ip">.</nts>
                  <nts id="Seg_3703" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T961" id="Seg_3705" n="HIAT:u" s="T958">
                  <ts e="T959" id="Seg_3707" n="HIAT:w" s="T958">Qɨbanʼaʒala</ts>
                  <nts id="Seg_3708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T960" id="Seg_3710" n="HIAT:w" s="T959">türlʼe</ts>
                  <nts id="Seg_3711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T961" id="Seg_3713" n="HIAT:w" s="T960">übərat</ts>
                  <nts id="Seg_3714" n="HIAT:ip">.</nts>
                  <nts id="Seg_3715" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T966" id="Seg_3717" n="HIAT:u" s="T961">
                  <ts e="T962" id="Seg_3719" n="HIAT:w" s="T961">Nägamdə</ts>
                  <nts id="Seg_3720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T963" id="Seg_3722" n="HIAT:w" s="T962">kak</ts>
                  <nts id="Seg_3723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T964" id="Seg_3725" n="HIAT:w" s="T963">qätɨtdɨt</ts>
                  <nts id="Seg_3726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T965" id="Seg_3728" n="HIAT:w" s="T964">i</ts>
                  <nts id="Seg_3729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T966" id="Seg_3731" n="HIAT:w" s="T965">qwannɨt</ts>
                  <nts id="Seg_3732" n="HIAT:ip">.</nts>
                  <nts id="Seg_3733" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T972" id="Seg_3735" n="HIAT:u" s="T966">
                  <ts e="T967" id="Seg_3737" n="HIAT:w" s="T966">I</ts>
                  <nts id="Seg_3738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T968" id="Seg_3740" n="HIAT:w" s="T967">iːgatdänä</ts>
                  <nts id="Seg_3741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T969" id="Seg_3743" n="HIAT:w" s="T968">tʼärɨn</ts>
                  <nts id="Seg_3744" n="HIAT:ip">:</nts>
                  <nts id="Seg_3745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3746" n="HIAT:ip">“</nts>
                  <ts e="T970" id="Seg_3748" n="HIAT:w" s="T969">Me</ts>
                  <nts id="Seg_3749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T971" id="Seg_3751" n="HIAT:w" s="T970">taze</ts>
                  <nts id="Seg_3752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T972" id="Seg_3754" n="HIAT:w" s="T971">ilʼetǯaj</ts>
                  <nts id="Seg_3755" n="HIAT:ip">.</nts>
                  <nts id="Seg_3756" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T974" id="Seg_3758" n="HIAT:u" s="T972">
                  <ts e="T973" id="Seg_3760" n="HIAT:w" s="T972">Igə</ts>
                  <nts id="Seg_3761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T974" id="Seg_3763" n="HIAT:w" s="T973">türak</ts>
                  <nts id="Seg_3764" n="HIAT:ip">.</nts>
                  <nts id="Seg_3765" n="HIAT:ip">”</nts>
                  <nts id="Seg_3766" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T977" id="Seg_3768" n="HIAT:u" s="T974">
                  <ts e="T975" id="Seg_3770" n="HIAT:w" s="T974">Qwannɨn</ts>
                  <nts id="Seg_3771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T976" id="Seg_3773" n="HIAT:w" s="T975">tʼüj</ts>
                  <nts id="Seg_3774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T977" id="Seg_3776" n="HIAT:w" s="T976">baɣattɨlʼe</ts>
                  <nts id="Seg_3777" n="HIAT:ip">.</nts>
                  <nts id="Seg_3778" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T980" id="Seg_3780" n="HIAT:u" s="T977">
                  <ts e="T978" id="Seg_3782" n="HIAT:w" s="T977">Tüj</ts>
                  <nts id="Seg_3783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T979" id="Seg_3785" n="HIAT:w" s="T978">qɨlɨm</ts>
                  <nts id="Seg_3786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T980" id="Seg_3788" n="HIAT:w" s="T979">paqkɨnɨt</ts>
                  <nts id="Seg_3789" n="HIAT:ip">.</nts>
                  <nts id="Seg_3790" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T987" id="Seg_3792" n="HIAT:u" s="T980">
                  <ts e="T981" id="Seg_3794" n="HIAT:w" s="T980">Täblam</ts>
                  <nts id="Seg_3795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T982" id="Seg_3797" n="HIAT:w" s="T981">natʼet</ts>
                  <nts id="Seg_3798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T983" id="Seg_3800" n="HIAT:w" s="T982">qäːlɣɨnɨt</ts>
                  <nts id="Seg_3801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3802" n="HIAT:ip">(</nts>
                  <ts e="T984" id="Seg_3804" n="HIAT:w" s="T983">qännɨt</ts>
                  <nts id="Seg_3805" n="HIAT:ip">)</nts>
                  <nts id="Seg_3806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T985" id="Seg_3808" n="HIAT:w" s="T984">i</ts>
                  <nts id="Seg_3809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T986" id="Seg_3811" n="HIAT:w" s="T985">tʼüze</ts>
                  <nts id="Seg_3812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T987" id="Seg_3814" n="HIAT:w" s="T986">taɣɨnɨt</ts>
                  <nts id="Seg_3815" n="HIAT:ip">.</nts>
                  <nts id="Seg_3816" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T991" id="Seg_3818" n="HIAT:u" s="T987">
                  <ts e="T988" id="Seg_3820" n="HIAT:w" s="T987">Saoznikamdə</ts>
                  <nts id="Seg_3821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T989" id="Seg_3823" n="HIAT:w" s="T988">qarʼe</ts>
                  <nts id="Seg_3824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T990" id="Seg_3826" n="HIAT:w" s="T989">ükkulʼe</ts>
                  <nts id="Seg_3827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T991" id="Seg_3829" n="HIAT:w" s="T990">qwatdɨt</ts>
                  <nts id="Seg_3830" n="HIAT:ip">.</nts>
                  <nts id="Seg_3831" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T994" id="Seg_3833" n="HIAT:u" s="T991">
                  <ts e="T992" id="Seg_3835" n="HIAT:w" s="T991">Tädomɨlamdə</ts>
                  <nts id="Seg_3836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T993" id="Seg_3838" n="HIAT:w" s="T992">wes</ts>
                  <nts id="Seg_3839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T994" id="Seg_3841" n="HIAT:w" s="T993">tuɣunnɨt</ts>
                  <nts id="Seg_3842" n="HIAT:ip">.</nts>
                  <nts id="Seg_3843" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T999" id="Seg_3845" n="HIAT:u" s="T994">
                  <ts e="T995" id="Seg_3847" n="HIAT:w" s="T994">Iːgatdɨze</ts>
                  <nts id="Seg_3848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T996" id="Seg_3850" n="HIAT:w" s="T995">omdaɣə</ts>
                  <nts id="Seg_3851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T997" id="Seg_3853" n="HIAT:w" s="T996">i</ts>
                  <nts id="Seg_3854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T998" id="Seg_3856" n="HIAT:w" s="T997">na</ts>
                  <nts id="Seg_3857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T999" id="Seg_3859" n="HIAT:w" s="T998">qwatdaːq</ts>
                  <nts id="Seg_3860" n="HIAT:ip">.</nts>
                  <nts id="Seg_3861" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1003" id="Seg_3863" n="HIAT:u" s="T999">
                  <ts e="T1000" id="Seg_3865" n="HIAT:w" s="T999">Tʼülʼe</ts>
                  <nts id="Seg_3866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1001" id="Seg_3868" n="HIAT:w" s="T1000">meːdaɣə</ts>
                  <nts id="Seg_3869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1002" id="Seg_3871" n="HIAT:w" s="T1001">otdə</ts>
                  <nts id="Seg_3872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1003" id="Seg_3874" n="HIAT:w" s="T1002">jedoɣotdə</ts>
                  <nts id="Seg_3875" n="HIAT:ip">.</nts>
                  <nts id="Seg_3876" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1005" id="Seg_3878" n="HIAT:u" s="T1003">
                  <ts e="T1004" id="Seg_3880" n="HIAT:w" s="T1003">Qulat</ts>
                  <nts id="Seg_3881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1005" id="Seg_3883" n="HIAT:w" s="T1004">aːtdalbat</ts>
                  <nts id="Seg_3884" n="HIAT:ip">.</nts>
                  <nts id="Seg_3885" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1009" id="Seg_3887" n="HIAT:u" s="T1005">
                  <nts id="Seg_3888" n="HIAT:ip">“</nts>
                  <ts e="T1006" id="Seg_3890" n="HIAT:w" s="T1005">A</ts>
                  <nts id="Seg_3891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1007" id="Seg_3893" n="HIAT:w" s="T1006">pajal</ts>
                  <nts id="Seg_3894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1008" id="Seg_3896" n="HIAT:w" s="T1007">kuttʼen</ts>
                  <nts id="Seg_3897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1009" id="Seg_3899" n="HIAT:w" s="T1008">jen</ts>
                  <nts id="Seg_3900" n="HIAT:ip">?</nts>
                  <nts id="Seg_3901" n="HIAT:ip">”</nts>
                  <nts id="Seg_3902" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1011" id="Seg_3904" n="HIAT:u" s="T1009">
                  <nts id="Seg_3905" n="HIAT:ip">“</nts>
                  <ts e="T1010" id="Seg_3907" n="HIAT:w" s="T1009">Pajau</ts>
                  <nts id="Seg_3908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1011" id="Seg_3910" n="HIAT:w" s="T1010">qupba</ts>
                  <nts id="Seg_3911" n="HIAT:ip">.</nts>
                  <nts id="Seg_3912" n="HIAT:ip">”</nts>
                  <nts id="Seg_3913" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1014" id="Seg_3915" n="HIAT:u" s="T1011">
                  <ts e="T1012" id="Seg_3917" n="HIAT:w" s="T1011">Wes</ts>
                  <nts id="Seg_3918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1013" id="Seg_3920" n="HIAT:w" s="T1012">tugunattə</ts>
                  <nts id="Seg_3921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1014" id="Seg_3923" n="HIAT:w" s="T1013">tädomɨm</ts>
                  <nts id="Seg_3924" n="HIAT:ip">.</nts>
                  <nts id="Seg_3925" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1018" id="Seg_3927" n="HIAT:u" s="T1014">
                  <ts e="T1015" id="Seg_3929" n="HIAT:w" s="T1014">Täp</ts>
                  <nts id="Seg_3930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1016" id="Seg_3932" n="HIAT:w" s="T1015">pirʼäɣɨtdə</ts>
                  <nts id="Seg_3933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1017" id="Seg_3935" n="HIAT:w" s="T1016">maːt</ts>
                  <nts id="Seg_3936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1018" id="Seg_3938" n="HIAT:w" s="T1017">tautdɨt</ts>
                  <nts id="Seg_3939" n="HIAT:ip">.</nts>
                  <nts id="Seg_3940" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1024" id="Seg_3942" n="HIAT:u" s="T1018">
                  <ts e="T1019" id="Seg_3944" n="HIAT:w" s="T1018">I</ts>
                  <nts id="Seg_3945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1020" id="Seg_3947" n="HIAT:w" s="T1019">nädɨn</ts>
                  <nts id="Seg_3948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1021" id="Seg_3950" n="HIAT:w" s="T1020">i</ts>
                  <nts id="Seg_3951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1022" id="Seg_3953" n="HIAT:w" s="T1021">warkɨlʼe</ts>
                  <nts id="Seg_3954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1023" id="Seg_3956" n="HIAT:w" s="T1022">na</ts>
                  <nts id="Seg_3957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1024" id="Seg_3959" n="HIAT:w" s="T1023">übɨrɨtda</ts>
                  <nts id="Seg_3960" n="HIAT:ip">.</nts>
                  <nts id="Seg_3961" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1028" id="Seg_3963" n="HIAT:u" s="T1024">
                  <ts e="T1025" id="Seg_3965" n="HIAT:w" s="T1024">Man</ts>
                  <nts id="Seg_3966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1026" id="Seg_3968" n="HIAT:w" s="T1025">potdɨbon</ts>
                  <nts id="Seg_3969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1027" id="Seg_3971" n="HIAT:w" s="T1026">tebnan</ts>
                  <nts id="Seg_3972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1028" id="Seg_3974" n="HIAT:w" s="T1027">jezan</ts>
                  <nts id="Seg_3975" n="HIAT:ip">.</nts>
                  <nts id="Seg_3976" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1031" id="Seg_3978" n="HIAT:u" s="T1028">
                  <ts e="T1029" id="Seg_3980" n="HIAT:w" s="T1028">Datau</ts>
                  <nts id="Seg_3981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1030" id="Seg_3983" n="HIAT:w" s="T1029">sodʼigan</ts>
                  <nts id="Seg_3984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1031" id="Seg_3986" n="HIAT:w" s="T1030">warkaː</ts>
                  <nts id="Seg_3987" n="HIAT:ip">.</nts>
                  <nts id="Seg_3988" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1037" id="Seg_3990" n="HIAT:u" s="T1031">
                  <ts e="T1032" id="Seg_3992" n="HIAT:w" s="T1031">Pajat</ts>
                  <nts id="Seg_3993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1033" id="Seg_3995" n="HIAT:w" s="T1032">sodʼiga</ts>
                  <nts id="Seg_3996" n="HIAT:ip">,</nts>
                  <nts id="Seg_3997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1034" id="Seg_3999" n="HIAT:w" s="T1033">mazɨm</ts>
                  <nts id="Seg_4000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1035" id="Seg_4002" n="HIAT:w" s="T1034">čajlaze</ts>
                  <nts id="Seg_4003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1036" id="Seg_4005" n="HIAT:w" s="T1035">zɨm</ts>
                  <nts id="Seg_4006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1037" id="Seg_4008" n="HIAT:w" s="T1036">ärčɨs</ts>
                  <nts id="Seg_4009" n="HIAT:ip">.</nts>
                  <nts id="Seg_4010" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1041" id="Seg_4012" n="HIAT:u" s="T1037">
                  <ts e="T1038" id="Seg_4014" n="HIAT:w" s="T1037">Mekga</ts>
                  <nts id="Seg_4015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1039" id="Seg_4017" n="HIAT:w" s="T1038">tʼärɨn</ts>
                  <nts id="Seg_4018" n="HIAT:ip">:</nts>
                  <nts id="Seg_4019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4020" n="HIAT:ip">“</nts>
                  <ts e="T1040" id="Seg_4022" n="HIAT:w" s="T1039">Mäguntu</ts>
                  <nts id="Seg_4023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1041" id="Seg_4025" n="HIAT:w" s="T1040">paldʼikoq</ts>
                  <nts id="Seg_4026" n="HIAT:ip">.</nts>
                  <nts id="Seg_4027" n="HIAT:ip">”</nts>
                  <nts id="Seg_4028" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1045" id="Seg_4030" n="HIAT:u" s="T1041">
                  <ts e="T1042" id="Seg_4032" n="HIAT:w" s="T1041">Man</ts>
                  <nts id="Seg_4033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1043" id="Seg_4035" n="HIAT:w" s="T1042">teblanä</ts>
                  <nts id="Seg_4036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1044" id="Seg_4038" n="HIAT:w" s="T1043">qɨdan</ts>
                  <nts id="Seg_4039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1045" id="Seg_4041" n="HIAT:w" s="T1044">uduruqwan</ts>
                  <nts id="Seg_4042" n="HIAT:ip">.</nts>
                  <nts id="Seg_4043" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1048" id="Seg_4045" n="HIAT:u" s="T1045">
                  <ts e="T1046" id="Seg_4047" n="HIAT:w" s="T1045">Tabon</ts>
                  <nts id="Seg_4048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1047" id="Seg_4050" n="HIAT:w" s="T1046">jedot</ts>
                  <nts id="Seg_4051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1048" id="Seg_4053" n="HIAT:w" s="T1047">qwaǯan</ts>
                  <nts id="Seg_4054" n="HIAT:ip">.</nts>
                  <nts id="Seg_4055" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1051" id="Seg_4057" n="HIAT:u" s="T1048">
                  <ts e="T1049" id="Seg_4059" n="HIAT:w" s="T1048">Teblanä</ts>
                  <nts id="Seg_4060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1050" id="Seg_4062" n="HIAT:w" s="T1049">aj</ts>
                  <nts id="Seg_4063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1051" id="Seg_4065" n="HIAT:w" s="T1050">serkeǯan</ts>
                  <nts id="Seg_4066" n="HIAT:ip">.</nts>
                  <nts id="Seg_4067" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T1051" id="Seg_4068" n="sc" s="T1">
               <ts e="T2" id="Seg_4070" n="e" s="T1">Xrestjan </ts>
               <ts e="T3" id="Seg_4072" n="e" s="T2">era. </ts>
               <ts e="T4" id="Seg_4074" n="e" s="T3">Tʼelɨmnɨn </ts>
               <ts e="T5" id="Seg_4076" n="e" s="T4">Xrʼestʼän </ts>
               <ts e="T6" id="Seg_4078" n="e" s="T5">era. </ts>
               <ts e="T7" id="Seg_4080" n="e" s="T6">Täbnan </ts>
               <ts e="T8" id="Seg_4082" n="e" s="T7">naːgur </ts>
               <ts e="T9" id="Seg_4084" n="e" s="T8">iːt. </ts>
               <ts e="T10" id="Seg_4086" n="e" s="T9">Iːlat </ts>
               <ts e="T11" id="Seg_4088" n="e" s="T10">wes </ts>
               <ts e="T12" id="Seg_4090" n="e" s="T11">nädälattə. </ts>
               <ts e="T13" id="Seg_4092" n="e" s="T12">Okkɨr </ts>
               <ts e="T14" id="Seg_4094" n="e" s="T13">iːt </ts>
               <ts e="T15" id="Seg_4096" n="e" s="T14">qɨdan </ts>
               <ts e="T16" id="Seg_4098" n="e" s="T15">surulle </ts>
               <ts e="T17" id="Seg_4100" n="e" s="T16">palʼdükun, </ts>
               <ts e="T18" id="Seg_4102" n="e" s="T17">warɣə </ts>
               <ts e="T19" id="Seg_4104" n="e" s="T18">iːt. </ts>
               <ts e="T20" id="Seg_4106" n="e" s="T19">A </ts>
               <ts e="T21" id="Seg_4108" n="e" s="T20">sət </ts>
               <ts e="T22" id="Seg_4110" n="e" s="T21">(sədə) </ts>
               <ts e="T23" id="Seg_4112" n="e" s="T22">iːt </ts>
               <ts e="T24" id="Seg_4114" n="e" s="T23">skatʼinalaze </ts>
               <ts e="T25" id="Seg_4116" n="e" s="T24">palʼdüqwaɣə, </ts>
               <ts e="T26" id="Seg_4118" n="e" s="T25">sɨrla </ts>
               <ts e="T27" id="Seg_4120" n="e" s="T26">warədattə, </ts>
               <ts e="T28" id="Seg_4122" n="e" s="T27">kütdɨla </ts>
               <ts e="T29" id="Seg_4124" n="e" s="T28">i </ts>
               <ts e="T30" id="Seg_4126" n="e" s="T29">konɛrla. </ts>
               <ts e="T31" id="Seg_4128" n="e" s="T30">I </ts>
               <ts e="T32" id="Seg_4130" n="e" s="T31">pašnä </ts>
               <ts e="T33" id="Seg_4132" n="e" s="T32">wardattə, </ts>
               <ts e="T34" id="Seg_4134" n="e" s="T33">apsot </ts>
               <ts e="T35" id="Seg_4136" n="e" s="T34">sejakudattə. </ts>
               <ts e="T36" id="Seg_4138" n="e" s="T35">A </ts>
               <ts e="T37" id="Seg_4140" n="e" s="T36">warɣɨ </ts>
               <ts e="T38" id="Seg_4142" n="e" s="T37">iːt </ts>
               <ts e="T39" id="Seg_4144" n="e" s="T38">kɨːdan </ts>
               <ts e="T40" id="Seg_4146" n="e" s="T39">madʼöt </ts>
               <ts e="T41" id="Seg_4148" n="e" s="T40">paldʼän </ts>
               <ts e="T42" id="Seg_4150" n="e" s="T41">(paldʼikun). </ts>
               <ts e="T43" id="Seg_4152" n="e" s="T42">Tebnan </ts>
               <ts e="T44" id="Seg_4154" n="e" s="T43">neːt </ts>
               <ts e="T45" id="Seg_4156" n="e" s="T44">tʼelɨmda </ts>
               <ts e="T46" id="Seg_4158" n="e" s="T45">i </ts>
               <ts e="T47" id="Seg_4160" n="e" s="T46">iːga </ts>
               <ts e="T48" id="Seg_4162" n="e" s="T47">tʼelɨmda. </ts>
               <ts e="T49" id="Seg_4164" n="e" s="T48">Tebɨstaɣə </ts>
               <ts e="T50" id="Seg_4166" n="e" s="T49">warɣɨn </ts>
               <ts e="T51" id="Seg_4168" n="e" s="T50">aːzuaɣə </ts>
               <ts e="T52" id="Seg_4170" n="e" s="T51">(aːzuaq). </ts>
               <ts e="T53" id="Seg_4172" n="e" s="T52">A </ts>
               <ts e="T54" id="Seg_4174" n="e" s="T53">täp </ts>
               <ts e="T55" id="Seg_4176" n="e" s="T54">natʼen </ts>
               <ts e="T56" id="Seg_4178" n="e" s="T55">madʼöɣɨn </ts>
               <ts e="T57" id="Seg_4180" n="e" s="T56">iːbɨɣaj </ts>
               <ts e="T58" id="Seg_4182" n="e" s="T57">maːt </ts>
               <ts e="T59" id="Seg_4184" n="e" s="T58">omdɨlǯɨmɨtdɨt, </ts>
               <ts e="T60" id="Seg_4186" n="e" s="T59">iːbɨɣaj </ts>
               <ts e="T61" id="Seg_4188" n="e" s="T60">maːt. </ts>
               <ts e="T62" id="Seg_4190" n="e" s="T61">Äotdänä </ts>
               <ts e="T63" id="Seg_4192" n="e" s="T62">äzätdänä </ts>
               <ts e="T64" id="Seg_4194" n="e" s="T63">tʼärɨn: </ts>
               <ts e="T65" id="Seg_4196" n="e" s="T64">“Nomse </ts>
               <ts e="T66" id="Seg_4198" n="e" s="T65">mazɨm </ts>
               <ts e="T67" id="Seg_4200" n="e" s="T66">paslawikə.” </ts>
               <ts e="T68" id="Seg_4202" n="e" s="T67">Äutdə </ts>
               <ts e="T69" id="Seg_4204" n="e" s="T68">nomɨm </ts>
               <ts e="T70" id="Seg_4206" n="e" s="T69">iːut </ts>
               <ts e="T71" id="Seg_4208" n="e" s="T70">i </ts>
               <ts e="T72" id="Seg_4210" n="e" s="T71">paslawinɨt. </ts>
               <ts e="T73" id="Seg_4212" n="e" s="T72">“Man </ts>
               <ts e="T74" id="Seg_4214" n="e" s="T73">qwatǯan. </ts>
               <ts e="T75" id="Seg_4216" n="e" s="T74">Naːgur </ts>
               <ts e="T76" id="Seg_4218" n="e" s="T75">agazlaftə </ts>
               <ts e="T77" id="Seg_4220" n="e" s="T76">okkɨr </ts>
               <ts e="T78" id="Seg_4222" n="e" s="T77">maːtqɨn </ts>
               <ts e="T79" id="Seg_4224" n="e" s="T78">ass </ts>
               <ts e="T80" id="Seg_4226" n="e" s="T79">elʼetǯutu. </ts>
               <ts e="T81" id="Seg_4228" n="e" s="T80">Manan </ts>
               <ts e="T82" id="Seg_4230" n="e" s="T81">täper </ts>
               <ts e="T83" id="Seg_4232" n="e" s="T82">sʼämjau.” </ts>
               <ts e="T84" id="Seg_4234" n="e" s="T83">Saozniktə </ts>
               <ts e="T85" id="Seg_4236" n="e" s="T84">wes </ts>
               <ts e="T86" id="Seg_4238" n="e" s="T85">tädomɨmdə </ts>
               <ts e="T87" id="Seg_4240" n="e" s="T86">tuɣunnɨt. </ts>
               <ts e="T88" id="Seg_4242" n="e" s="T87">I </ts>
               <ts e="T89" id="Seg_4244" n="e" s="T88">na </ts>
               <ts e="T90" id="Seg_4246" n="e" s="T89">üːbɨtdattə. </ts>
               <ts e="T91" id="Seg_4248" n="e" s="T90">Ästɨ </ts>
               <ts e="T92" id="Seg_4250" n="e" s="T91">äudɨ </ts>
               <ts e="T93" id="Seg_4252" n="e" s="T92">i </ts>
               <ts e="T94" id="Seg_4254" n="e" s="T93">türlʼe </ts>
               <ts e="T95" id="Seg_4256" n="e" s="T94">na </ts>
               <ts e="T96" id="Seg_4258" n="e" s="T95">ɣalɨtdattə. </ts>
               <ts e="T97" id="Seg_4260" n="e" s="T96">I </ts>
               <ts e="T98" id="Seg_4262" n="e" s="T97">täbla </ts>
               <ts e="T99" id="Seg_4264" n="e" s="T98">i </ts>
               <ts e="T100" id="Seg_4266" n="e" s="T99">na </ts>
               <ts e="T101" id="Seg_4268" n="e" s="T100">qwatdattə. </ts>
               <ts e="T102" id="Seg_4270" n="e" s="T101">No </ts>
               <ts e="T103" id="Seg_4272" n="e" s="T102">i </ts>
               <ts e="T104" id="Seg_4274" n="e" s="T103">tüle </ts>
               <ts e="T105" id="Seg_4276" n="e" s="T104">na </ts>
               <ts e="T106" id="Seg_4278" n="e" s="T105">medɨtdattə. </ts>
               <ts e="T107" id="Seg_4280" n="e" s="T106">A </ts>
               <ts e="T108" id="Seg_4282" n="e" s="T107">tebnan </ts>
               <ts e="T109" id="Seg_4284" n="e" s="T108">natʼen </ts>
               <ts e="T110" id="Seg_4286" n="e" s="T109">iːbɨɣaj </ts>
               <ts e="T111" id="Seg_4288" n="e" s="T110">maːt </ts>
               <ts e="T112" id="Seg_4290" n="e" s="T111">omdəlǯəmɨdɨt. </ts>
               <ts e="T113" id="Seg_4292" n="e" s="T112">Wes </ts>
               <ts e="T114" id="Seg_4294" n="e" s="T113">tuɣunnattə </ts>
               <ts e="T115" id="Seg_4296" n="e" s="T114">natʼet. </ts>
               <ts e="T116" id="Seg_4298" n="e" s="T115">No </ts>
               <ts e="T117" id="Seg_4300" n="e" s="T116">i </ts>
               <ts e="T118" id="Seg_4302" n="e" s="T117">ärat </ts>
               <ts e="T119" id="Seg_4304" n="e" s="T118">wes </ts>
               <ts e="T120" id="Seg_4306" n="e" s="T119">melʼe </ts>
               <ts e="T121" id="Seg_4308" n="e" s="T120">taːdərat, </ts>
               <ts e="T122" id="Seg_4310" n="e" s="T121">qoptə </ts>
               <ts e="T123" id="Seg_4312" n="e" s="T122">metdɨt. </ts>
               <ts e="T124" id="Seg_4314" n="e" s="T123">“Qardʼen </ts>
               <ts e="T125" id="Seg_4316" n="e" s="T124">madʼöt </ts>
               <ts e="T126" id="Seg_4318" n="e" s="T125">qwatǯan. </ts>
               <ts e="T127" id="Seg_4320" n="e" s="T126">A </ts>
               <ts e="T128" id="Seg_4322" n="e" s="T127">tan </ts>
               <ts e="T129" id="Seg_4324" n="e" s="T128">üdno </ts>
               <ts e="T130" id="Seg_4326" n="e" s="T129">na </ts>
               <ts e="T131" id="Seg_4328" n="e" s="T130">qwannaš. </ts>
               <ts e="T132" id="Seg_4330" n="e" s="T131">Toboɣät </ts>
               <ts e="T133" id="Seg_4332" n="e" s="T132">qajam </ts>
               <ts e="T134" id="Seg_4334" n="e" s="T133">struška </ts>
               <ts e="T135" id="Seg_4336" n="e" s="T134">papalan </ts>
               <ts e="T136" id="Seg_4338" n="e" s="T135">ig </ts>
               <ts e="T137" id="Seg_4340" n="e" s="T136">azü. </ts>
               <ts e="T138" id="Seg_4342" n="e" s="T137">Ato </ts>
               <ts e="T139" id="Seg_4344" n="e" s="T138">aːwan </ts>
               <ts e="T140" id="Seg_4346" n="e" s="T139">jetǯan.” </ts>
               <ts e="T141" id="Seg_4348" n="e" s="T140">No </ts>
               <ts e="T142" id="Seg_4350" n="e" s="T141">sekgattə. </ts>
               <ts e="T143" id="Seg_4352" n="e" s="T142">Qarʼemɨɣɨn </ts>
               <ts e="T144" id="Seg_4354" n="e" s="T143">wazɨn, </ts>
               <ts e="T145" id="Seg_4356" n="e" s="T144">aurnɨn </ts>
               <ts e="T146" id="Seg_4358" n="e" s="T145">i </ts>
               <ts e="T147" id="Seg_4360" n="e" s="T146">na </ts>
               <ts e="T148" id="Seg_4362" n="e" s="T147">qwatda </ts>
               <ts e="T149" id="Seg_4364" n="e" s="T148">madʼötdə. </ts>
               <ts e="T150" id="Seg_4366" n="e" s="T149">A </ts>
               <ts e="T151" id="Seg_4368" n="e" s="T150">näjɣum </ts>
               <ts e="T152" id="Seg_4370" n="e" s="T151">tʼärɨn: </ts>
               <ts e="T153" id="Seg_4372" n="e" s="T152">“Qajno </ts>
               <ts e="T154" id="Seg_4374" n="e" s="T153">mekga </ts>
               <ts e="T155" id="Seg_4376" n="e" s="T154">ärau </ts>
               <ts e="T156" id="Seg_4378" n="e" s="T155">tʼärɨn, </ts>
               <ts e="T157" id="Seg_4380" n="e" s="T156">što </ts>
               <ts e="T158" id="Seg_4382" n="e" s="T157">struška </ts>
               <ts e="T159" id="Seg_4384" n="e" s="T158">üttə </ts>
               <ts e="T160" id="Seg_4386" n="e" s="T159">papalan </ts>
               <ts e="T161" id="Seg_4388" n="e" s="T160">ik </ts>
               <ts e="T162" id="Seg_4390" n="e" s="T161">azə. </ts>
               <ts e="T163" id="Seg_4392" n="e" s="T162">Man </ts>
               <ts e="T164" id="Seg_4394" n="e" s="T163">sičas </ts>
               <ts e="T165" id="Seg_4396" n="e" s="T164">sət </ts>
               <ts e="T166" id="Seg_4398" n="e" s="T165">wedram </ts>
               <ts e="T167" id="Seg_4400" n="e" s="T166">struškalaze </ts>
               <ts e="T168" id="Seg_4402" n="e" s="T167">nabineǯau, </ts>
               <ts e="T169" id="Seg_4404" n="e" s="T168">nabinäǯau, </ts>
               <ts e="T170" id="Seg_4406" n="e" s="T169">qwalʼlʼe </ts>
               <ts e="T171" id="Seg_4408" n="e" s="T170">üttə </ts>
               <ts e="T172" id="Seg_4410" n="e" s="T171">qamnätǯau.” </ts>
               <ts e="T173" id="Seg_4412" n="e" s="T172">I </ts>
               <ts e="T174" id="Seg_4414" n="e" s="T173">na </ts>
               <ts e="T175" id="Seg_4416" n="e" s="T174">qwannɨdɨt </ts>
               <ts e="T176" id="Seg_4418" n="e" s="T175">i </ts>
               <ts e="T177" id="Seg_4420" n="e" s="T176">qamǯɨt. </ts>
               <ts e="T178" id="Seg_4422" n="e" s="T177">Nɨkga </ts>
               <ts e="T179" id="Seg_4424" n="e" s="T178">i </ts>
               <ts e="T180" id="Seg_4426" n="e" s="T179">mannɨpaːt. </ts>
               <ts e="T181" id="Seg_4428" n="e" s="T180">A </ts>
               <ts e="T182" id="Seg_4430" n="e" s="T181">natʼen </ts>
               <ts e="T183" id="Seg_4432" n="e" s="T182">köunɨn. </ts>
               <ts e="T184" id="Seg_4434" n="e" s="T183">Täp </ts>
               <ts e="T185" id="Seg_4436" n="e" s="T184">üdɨm </ts>
               <ts e="T186" id="Seg_4438" n="e" s="T185">soɣunnɨt </ts>
               <ts e="T187" id="Seg_4440" n="e" s="T186">i </ts>
               <ts e="T188" id="Seg_4442" n="e" s="T187">maːttɨ </ts>
               <ts e="T189" id="Seg_4444" n="e" s="T188">tüa. </ts>
               <ts e="T190" id="Seg_4446" n="e" s="T189">A </ts>
               <ts e="T191" id="Seg_4448" n="e" s="T190">täp </ts>
               <ts e="T192" id="Seg_4450" n="e" s="T191">krasiwa </ts>
               <ts e="T193" id="Seg_4452" n="e" s="T192">näjɣum </ts>
               <ts e="T194" id="Seg_4454" n="e" s="T193">jes. </ts>
               <ts e="T195" id="Seg_4456" n="e" s="T194">Qwalʼlʼe </ts>
               <ts e="T196" id="Seg_4458" n="e" s="T195">sütʼdinäj </ts>
               <ts e="T197" id="Seg_4460" n="e" s="T196">maːttə </ts>
               <ts e="T198" id="Seg_4462" n="e" s="T197">omdɨn </ts>
               <ts e="T199" id="Seg_4464" n="e" s="T198">i </ts>
               <ts e="T200" id="Seg_4466" n="e" s="T199">südɨrlʼe </ts>
               <ts e="T201" id="Seg_4468" n="e" s="T200">übɨran. </ts>
               <ts e="T202" id="Seg_4470" n="e" s="T201">A </ts>
               <ts e="T203" id="Seg_4472" n="e" s="T202">sət </ts>
               <ts e="T204" id="Seg_4474" n="e" s="T203">qɨbanʼaʒa </ts>
               <ts e="T205" id="Seg_4476" n="e" s="T204">warkaɣə. </ts>
               <ts e="T206" id="Seg_4478" n="e" s="T205">Razbojnigla </ts>
               <ts e="T207" id="Seg_4480" n="e" s="T206">čaʒatdə </ts>
               <ts e="T208" id="Seg_4482" n="e" s="T207">atdɨzʼe </ts>
               <ts e="T209" id="Seg_4484" n="e" s="T208">i </ts>
               <ts e="T210" id="Seg_4486" n="e" s="T209">tʼärattə: </ts>
               <ts e="T211" id="Seg_4488" n="e" s="T210">“Qaj </ts>
               <ts e="T212" id="Seg_4490" n="e" s="T211">struškala </ts>
               <ts e="T213" id="Seg_4492" n="e" s="T212">(köudattə) </ts>
               <ts e="T214" id="Seg_4494" n="e" s="T213">küuze </ts>
               <ts e="T215" id="Seg_4496" n="e" s="T214">čaːʒɨdattə. </ts>
               <ts e="T216" id="Seg_4498" n="e" s="T215">Qən </ts>
               <ts e="T217" id="Seg_4500" n="e" s="T216">barɨnʼä </ts>
               <ts e="T218" id="Seg_4502" n="e" s="T217">praibet!” </ts>
               <ts e="T219" id="Seg_4504" n="e" s="T218">Tebla </ts>
               <ts e="T220" id="Seg_4506" n="e" s="T219">mannɨpattə </ts>
               <ts e="T221" id="Seg_4508" n="e" s="T220">qajda </ts>
               <ts e="T222" id="Seg_4510" n="e" s="T221">wattɨ. </ts>
               <ts e="T223" id="Seg_4512" n="e" s="T222">Täbla </ts>
               <ts e="T224" id="Seg_4514" n="e" s="T223">wattɨga </ts>
               <ts e="T225" id="Seg_4516" n="e" s="T224">kotdattə. </ts>
               <ts e="T226" id="Seg_4518" n="e" s="T225">“Ugon </ts>
               <ts e="T227" id="Seg_4520" n="e" s="T226">me </ts>
               <ts e="T228" id="Seg_4522" n="e" s="T227">tau </ts>
               <ts e="T229" id="Seg_4524" n="e" s="T228">wattɨm </ts>
               <ts e="T230" id="Seg_4526" n="e" s="T229">jass </ts>
               <ts e="T231" id="Seg_4528" n="e" s="T230">qoǯɨrguzautə.” </ts>
               <ts e="T232" id="Seg_4530" n="e" s="T231">Na </ts>
               <ts e="T233" id="Seg_4532" n="e" s="T232">wattogatdə </ts>
               <ts e="T234" id="Seg_4534" n="e" s="T233">(na </ts>
               <ts e="T235" id="Seg_4536" n="e" s="T234">watotdə, </ts>
               <ts e="T236" id="Seg_4538" n="e" s="T235">na </ts>
               <ts e="T237" id="Seg_4540" n="e" s="T236">watotdə) </ts>
               <ts e="T238" id="Seg_4542" n="e" s="T237">i </ts>
               <ts e="T239" id="Seg_4544" n="e" s="T238">uːdurattə. </ts>
               <ts e="T240" id="Seg_4546" n="e" s="T239">Attaman </ts>
               <ts e="T241" id="Seg_4548" n="e" s="T240">tʼärɨn: </ts>
               <ts e="T242" id="Seg_4550" n="e" s="T241">“Tä </ts>
               <ts e="T243" id="Seg_4552" n="e" s="T242">aːmdaltə </ts>
               <ts e="T244" id="Seg_4554" n="e" s="T243">tɨtdɨn, </ts>
               <ts e="T245" id="Seg_4556" n="e" s="T244">a </ts>
               <ts e="T246" id="Seg_4558" n="e" s="T245">man </ts>
               <ts e="T247" id="Seg_4560" n="e" s="T246">qurollan </ts>
               <ts e="T248" id="Seg_4562" n="e" s="T247">mannɨbɨlʼeu </ts>
               <ts e="T249" id="Seg_4564" n="e" s="T248">qaj </ts>
               <ts e="T250" id="Seg_4566" n="e" s="T249">ilatda </ts>
               <ts e="T251" id="Seg_4568" n="e" s="T250">tɨtdɨn.” </ts>
               <ts e="T252" id="Seg_4570" n="e" s="T251">Na </ts>
               <ts e="T253" id="Seg_4572" n="e" s="T252">qurolǯa. </ts>
               <ts e="T254" id="Seg_4574" n="e" s="T253">Tüa, </ts>
               <ts e="T255" id="Seg_4576" n="e" s="T254">sernɨn </ts>
               <ts e="T256" id="Seg_4578" n="e" s="T255">maːttə. </ts>
               <ts e="T257" id="Seg_4580" n="e" s="T256">“Tʼolom.” </ts>
               <ts e="T258" id="Seg_4582" n="e" s="T257">Näjɣum </ts>
               <ts e="T259" id="Seg_4584" n="e" s="T258">tʼärɨn: </ts>
               <ts e="T260" id="Seg_4586" n="e" s="T259">“Tʼolom, </ts>
               <ts e="T261" id="Seg_4588" n="e" s="T260">tʼolom.” </ts>
               <ts e="T262" id="Seg_4590" n="e" s="T261">Stul </ts>
               <ts e="T263" id="Seg_4592" n="e" s="T262">čečelʼe </ts>
               <ts e="T264" id="Seg_4594" n="e" s="T263">meʒalǯɨt. </ts>
               <ts e="T265" id="Seg_4596" n="e" s="T264">Tʼärɨn: </ts>
               <ts e="T266" id="Seg_4598" n="e" s="T265">“Omdak </ts>
               <ts e="T267" id="Seg_4600" n="e" s="T266">tɨkga.” </ts>
               <ts e="T268" id="Seg_4602" n="e" s="T267">A </ts>
               <ts e="T269" id="Seg_4604" n="e" s="T268">täp </ts>
               <ts e="T270" id="Seg_4606" n="e" s="T269">omdɨn. </ts>
               <ts e="T271" id="Seg_4608" n="e" s="T270">Kulupbɨlʼe </ts>
               <ts e="T272" id="Seg_4610" n="e" s="T271">na </ts>
               <ts e="T273" id="Seg_4612" n="e" s="T272">übɨrɨtdaɣ. </ts>
               <ts e="T274" id="Seg_4614" n="e" s="T273">Ataman </ts>
               <ts e="T275" id="Seg_4616" n="e" s="T274">tʼärɨn: </ts>
               <ts e="T276" id="Seg_4618" n="e" s="T275">“Tan </ts>
               <ts e="T277" id="Seg_4620" n="e" s="T276">mekga </ts>
               <ts e="T278" id="Seg_4622" n="e" s="T277">tɨbɨtdɨlʼet.” </ts>
               <ts e="T279" id="Seg_4624" n="e" s="T278">“No </ts>
               <ts e="T280" id="Seg_4626" n="e" s="T279">mannan </ts>
               <ts e="T281" id="Seg_4628" n="e" s="T280">ärau </ts>
               <ts e="T282" id="Seg_4630" n="e" s="T281">orse. </ts>
               <ts e="T283" id="Seg_4632" n="e" s="T282">Täp </ts>
               <ts e="T284" id="Seg_4634" n="e" s="T283">tastɨ </ts>
               <ts e="T285" id="Seg_4636" n="e" s="T284">qwačɨt </ts>
               <ts e="T286" id="Seg_4638" n="e" s="T285">i </ts>
               <ts e="T287" id="Seg_4640" n="e" s="T286">mazɨm </ts>
               <ts e="T288" id="Seg_4642" n="e" s="T287">qwačɨt.” </ts>
               <ts e="T289" id="Seg_4644" n="e" s="T288">“A </ts>
               <ts e="T290" id="Seg_4646" n="e" s="T289">man </ts>
               <ts e="T291" id="Seg_4648" n="e" s="T290">tekga </ts>
               <ts e="T292" id="Seg_4650" n="e" s="T291">qudugo </ts>
               <ts e="T293" id="Seg_4652" n="e" s="T292">mennɨǯan. </ts>
               <ts e="T294" id="Seg_4654" n="e" s="T293">(Optə </ts>
               <ts e="T295" id="Seg_4656" n="e" s="T294">jennɨš </ts>
               <ts e="T296" id="Seg_4658" n="e" s="T295">i </ts>
               <ts e="T297" id="Seg_4660" n="e" s="T296">sɨnkowa </ts>
               <ts e="T298" id="Seg_4662" n="e" s="T297">prowoloka, </ts>
               <ts e="T299" id="Seg_4664" n="e" s="T298">täp </ts>
               <ts e="T300" id="Seg_4666" n="e" s="T299">tau </ts>
               <ts e="T301" id="Seg_4668" n="e" s="T300">kudɨgom </ts>
               <ts e="T302" id="Seg_4670" n="e" s="T301">as </ts>
               <ts e="T303" id="Seg_4672" n="e" s="T302">lakčeǯit). </ts>
               <ts e="T304" id="Seg_4674" n="e" s="T303">Täp </ts>
               <ts e="T305" id="Seg_4676" n="e" s="T304">na </ts>
               <ts e="T306" id="Seg_4678" n="e" s="T305">tünnɨš </ts>
               <ts e="T307" id="Seg_4680" n="e" s="T306">üdomɨɣɨn. </ts>
               <ts e="T308" id="Seg_4682" n="e" s="T307">Tan </ts>
               <ts e="T309" id="Seg_4684" n="e" s="T308">tʼärak: </ts>
               <ts e="T310" id="Seg_4686" n="e" s="T309">“Qozɨrčɨlaj.” </ts>
               <ts e="T311" id="Seg_4688" n="e" s="T310">Tabnä </ts>
               <ts e="T312" id="Seg_4690" n="e" s="T311">tʼärak: </ts>
               <ts e="T313" id="Seg_4692" n="e" s="T312">“Qut </ts>
               <ts e="T314" id="Seg_4694" n="e" s="T313">turakɨn </ts>
               <ts e="T315" id="Seg_4696" n="e" s="T314">qalʼeǯɨn, </ts>
               <ts e="T316" id="Seg_4698" n="e" s="T315">udlamdə </ts>
               <ts e="T317" id="Seg_4700" n="e" s="T316">moɣunä </ts>
               <ts e="T318" id="Seg_4702" n="e" s="T317">saruku.” </ts>
               <ts e="T319" id="Seg_4704" n="e" s="T318">A </ts>
               <ts e="T320" id="Seg_4706" n="e" s="T319">tan </ts>
               <ts e="T321" id="Seg_4708" n="e" s="T320">täbɨm </ts>
               <ts e="T322" id="Seg_4710" n="e" s="T321">turakɨn </ts>
               <ts e="T323" id="Seg_4712" n="e" s="T322">qwäzʼet, </ts>
               <ts e="T324" id="Seg_4714" n="e" s="T323">udlamdə </ts>
               <ts e="T325" id="Seg_4716" n="e" s="T324">moɣunä </ts>
               <ts e="T326" id="Seg_4718" n="e" s="T325">saːrʼet. </ts>
               <ts e="T327" id="Seg_4720" n="e" s="T326">Täp </ts>
               <ts e="T328" id="Seg_4722" n="e" s="T327">tau </ts>
               <ts e="T329" id="Seg_4724" n="e" s="T328">qudugom </ts>
               <ts e="T330" id="Seg_4726" n="e" s="T329">as </ts>
               <ts e="T331" id="Seg_4728" n="e" s="T330">lakčeǯin. </ts>
               <ts e="T332" id="Seg_4730" n="e" s="T331">Täp </ts>
               <ts e="T333" id="Seg_4732" n="e" s="T332">jeʒlʼi </ts>
               <ts e="T334" id="Seg_4734" n="e" s="T333">ass </ts>
               <ts e="T335" id="Seg_4736" n="e" s="T334">laqčɨt, </ts>
               <ts e="T336" id="Seg_4738" n="e" s="T335">a </ts>
               <ts e="T337" id="Seg_4740" n="e" s="T336">tan </ts>
               <ts e="T338" id="Seg_4742" n="e" s="T337">ponä </ts>
               <ts e="T339" id="Seg_4744" n="e" s="T338">čatǯak, </ts>
               <ts e="T340" id="Seg_4746" n="e" s="T339">mazɨm </ts>
               <ts e="T341" id="Seg_4748" n="e" s="T340">lakgolʼgə. </ts>
               <ts e="T342" id="Seg_4750" n="e" s="T341">Man </ts>
               <ts e="T343" id="Seg_4752" n="e" s="T342">tütǯan, </ts>
               <ts e="T344" id="Seg_4754" n="e" s="T343">täbɨm </ts>
               <ts e="T345" id="Seg_4756" n="e" s="T344">i </ts>
               <ts e="T346" id="Seg_4758" n="e" s="T345">qwatčau. </ts>
               <ts e="T347" id="Seg_4760" n="e" s="T346">Taze </ts>
               <ts e="T348" id="Seg_4762" n="e" s="T347">ilʼetǯaj.” </ts>
               <ts e="T349" id="Seg_4764" n="e" s="T348">Ärat </ts>
               <ts e="T350" id="Seg_4766" n="e" s="T349">tüa. </ts>
               <ts e="T351" id="Seg_4768" n="e" s="T350">Täp </ts>
               <ts e="T352" id="Seg_4770" n="e" s="T351">täbɨm </ts>
               <ts e="T353" id="Seg_4772" n="e" s="T352">apstɨt, </ts>
               <ts e="T354" id="Seg_4774" n="e" s="T353">tʼärɨn: </ts>
               <ts e="T355" id="Seg_4776" n="e" s="T354">“Qozɨrčɨlaj.” </ts>
               <ts e="T356" id="Seg_4778" n="e" s="T355">Ärat </ts>
               <ts e="T357" id="Seg_4780" n="e" s="T356">tʼärɨn: </ts>
               <ts e="T358" id="Seg_4782" n="e" s="T357">“Man </ts>
               <ts e="T359" id="Seg_4784" n="e" s="T358">nunɨtʼipban, </ts>
               <ts e="T360" id="Seg_4786" n="e" s="T359">qotdugu </ts>
               <ts e="T361" id="Seg_4788" n="e" s="T360">nadə.” </ts>
               <ts e="T362" id="Seg_4790" n="e" s="T361">“Nu </ts>
               <ts e="T363" id="Seg_4792" n="e" s="T362">dawaj, </ts>
               <ts e="T364" id="Seg_4794" n="e" s="T363">qozɨrčɨlaj </ts>
               <ts e="T365" id="Seg_4796" n="e" s="T364">ass </ts>
               <ts e="T366" id="Seg_4798" n="e" s="T365">qutdɨn. </ts>
               <ts e="T367" id="Seg_4800" n="e" s="T366">Ato </ts>
               <ts e="T368" id="Seg_4802" n="e" s="T367">onän </ts>
               <ts e="T369" id="Seg_4804" n="e" s="T368">amdan </ts>
               <ts e="T370" id="Seg_4806" n="e" s="T369">i </ts>
               <ts e="T371" id="Seg_4808" n="e" s="T370">skušnan </ts>
               <ts e="T372" id="Seg_4810" n="e" s="T371">mekga </ts>
               <ts e="T373" id="Seg_4812" n="e" s="T372">jen.” </ts>
               <ts e="T374" id="Seg_4814" n="e" s="T373">Nu </ts>
               <ts e="T375" id="Seg_4816" n="e" s="T374">i </ts>
               <ts e="T376" id="Seg_4818" n="e" s="T375">omdaɣə </ts>
               <ts e="T377" id="Seg_4820" n="e" s="T376">qozɨrčɨlʼe. </ts>
               <ts e="T378" id="Seg_4822" n="e" s="T377">Qut </ts>
               <ts e="T379" id="Seg_4824" n="e" s="T378">turaqɨn </ts>
               <ts e="T380" id="Seg_4826" n="e" s="T379">qalʼeǯɨn, </ts>
               <ts e="T381" id="Seg_4828" n="e" s="T380">udlamdə </ts>
               <ts e="T382" id="Seg_4830" n="e" s="T381">sarelǯugu </ts>
               <ts e="T383" id="Seg_4832" n="e" s="T382">moɣunä. </ts>
               <ts e="T384" id="Seg_4834" n="e" s="T383">Ärat </ts>
               <ts e="T385" id="Seg_4836" n="e" s="T384">turaqɨn </ts>
               <ts e="T386" id="Seg_4838" n="e" s="T385">qalɨn. </ts>
               <ts e="T387" id="Seg_4840" n="e" s="T386">“Udlamdə </ts>
               <ts e="T388" id="Seg_4842" n="e" s="T387">nadə </ts>
               <ts e="T389" id="Seg_4844" n="e" s="T388">sarelǯugu, </ts>
               <ts e="T390" id="Seg_4846" n="e" s="T389">udomdə </ts>
               <ts e="T391" id="Seg_4848" n="e" s="T390">nadə </ts>
               <ts e="T392" id="Seg_4850" n="e" s="T391">sarugu </ts>
               <ts e="T393" id="Seg_4852" n="e" s="T392">moɣunä. </ts>
               <ts e="T394" id="Seg_4854" n="e" s="T393">A </ts>
               <ts e="T395" id="Seg_4856" n="e" s="T394">jeʒlʼe </ts>
               <ts e="T396" id="Seg_4858" n="e" s="T395">man </ts>
               <ts e="T397" id="Seg_4860" n="e" s="T396">qalɨnän </ts>
               <ts e="T398" id="Seg_4862" n="e" s="T397">turaqɨn, </ts>
               <ts e="T399" id="Seg_4864" n="e" s="T398">tan </ts>
               <ts e="T400" id="Seg_4866" n="e" s="T399">bə </ts>
               <ts e="T401" id="Seg_4868" n="e" s="T400">mekga </ts>
               <ts e="T402" id="Seg_4870" n="e" s="T401">udlau </ts>
               <ts e="T403" id="Seg_4872" n="e" s="T402">zarelǯɨnäl. </ts>
               <ts e="T404" id="Seg_4874" n="e" s="T403">A </ts>
               <ts e="T405" id="Seg_4876" n="e" s="T404">täper </ts>
               <ts e="T406" id="Seg_4878" n="e" s="T405">man </ts>
               <ts e="T407" id="Seg_4880" n="e" s="T406">tʼekga </ts>
               <ts e="T408" id="Seg_4882" n="e" s="T407">udomdə </ts>
               <ts e="T409" id="Seg_4884" n="e" s="T408">saretǯau.” </ts>
               <ts e="T410" id="Seg_4886" n="e" s="T409">Ärat </ts>
               <ts e="T411" id="Seg_4888" n="e" s="T410">tʼärɨn: </ts>
               <ts e="T412" id="Seg_4890" n="e" s="T411">“Sarät”. </ts>
               <ts e="T413" id="Seg_4892" n="e" s="T412">Täp </ts>
               <ts e="T414" id="Seg_4894" n="e" s="T413">saːrät. </ts>
               <ts e="T415" id="Seg_4896" n="e" s="T414">Ärat </ts>
               <ts e="T416" id="Seg_4898" n="e" s="T415">kak </ts>
               <ts e="T417" id="Seg_4900" n="e" s="T416">turugulǯɨt, </ts>
               <ts e="T418" id="Seg_4902" n="e" s="T417">qudɨgola </ts>
               <ts e="T419" id="Seg_4904" n="e" s="T418">wes </ts>
               <ts e="T420" id="Seg_4906" n="e" s="T419">laqčelattə. </ts>
               <ts e="T421" id="Seg_4908" n="e" s="T420">No </ts>
               <ts e="T422" id="Seg_4910" n="e" s="T421">i </ts>
               <ts e="T423" id="Seg_4912" n="e" s="T422">qotdɨgu </ts>
               <ts e="T424" id="Seg_4914" n="e" s="T423">quʒannaɣɨ. </ts>
               <ts e="T425" id="Seg_4916" n="e" s="T424">Qarʼemɨɣɨn </ts>
               <ts e="T426" id="Seg_4918" n="e" s="T425">wazɨn. </ts>
               <ts e="T427" id="Seg_4920" n="e" s="T426">Näjɣum </ts>
               <ts e="T428" id="Seg_4922" n="e" s="T427">wes </ts>
               <ts e="T429" id="Seg_4924" n="e" s="T428">potqɨnɨt, </ts>
               <ts e="T430" id="Seg_4926" n="e" s="T429">wadʼilam </ts>
               <ts e="T431" id="Seg_4928" n="e" s="T430">qajlam, </ts>
               <ts e="T432" id="Seg_4930" n="e" s="T431">äramdə </ts>
               <ts e="T433" id="Seg_4932" n="e" s="T432">apstɨt. </ts>
               <ts e="T434" id="Seg_4934" n="e" s="T433">Ärat </ts>
               <ts e="T435" id="Seg_4936" n="e" s="T434">madʼot </ts>
               <ts e="T436" id="Seg_4938" n="e" s="T435">qwannɨn. </ts>
               <ts e="T437" id="Seg_4940" n="e" s="T436">Näjɣum </ts>
               <ts e="T438" id="Seg_4942" n="e" s="T437">ponä </ts>
               <ts e="T439" id="Seg_4944" n="e" s="T438">sapɨsin </ts>
               <ts e="T440" id="Seg_4946" n="e" s="T439">i </ts>
               <ts e="T441" id="Seg_4948" n="e" s="T440">na </ts>
               <ts e="T442" id="Seg_4950" n="e" s="T441">qaːrolʼdʼä: </ts>
               <ts e="T443" id="Seg_4952" n="e" s="T442">“Tʼükkɨ! </ts>
               <ts e="T444" id="Seg_4954" n="e" s="T443">Ärau </ts>
               <ts e="T445" id="Seg_4956" n="e" s="T444">qwanba!” </ts>
               <ts e="T446" id="Seg_4958" n="e" s="T445">Ataman </ts>
               <ts e="T447" id="Seg_4960" n="e" s="T446">na </ts>
               <ts e="T448" id="Seg_4962" n="e" s="T447">tütda. </ts>
               <ts e="T449" id="Seg_4964" n="e" s="T448">“No </ts>
               <ts e="T450" id="Seg_4966" n="e" s="T449">qaj, </ts>
               <ts e="T451" id="Seg_4968" n="e" s="T450">qozɨrčezalʼe?” </ts>
               <ts e="T452" id="Seg_4970" n="e" s="T451">“Qozɨrčezaj. </ts>
               <ts e="T453" id="Seg_4972" n="e" s="T452">Man </ts>
               <ts e="T454" id="Seg_4974" n="e" s="T453">tʼekga </ts>
               <ts e="T455" id="Seg_4976" n="e" s="T454">tʼärɨzan, </ts>
               <ts e="T456" id="Seg_4978" n="e" s="T455">täp </ts>
               <ts e="T457" id="Seg_4980" n="e" s="T456">orse </ts>
               <ts e="T458" id="Seg_4982" n="e" s="T457">jen, </ts>
               <ts e="T459" id="Seg_4984" n="e" s="T458">na </ts>
               <ts e="T460" id="Seg_4986" n="e" s="T459">qudɨgolam </ts>
               <ts e="T461" id="Seg_4988" n="e" s="T460">wes </ts>
               <ts e="T462" id="Seg_4990" n="e" s="T461">lakčelǯɨt. </ts>
               <ts e="T463" id="Seg_4992" n="e" s="T462">“A </ts>
               <ts e="T464" id="Seg_4994" n="e" s="T463">tamdʼel </ts>
               <ts e="T465" id="Seg_4996" n="e" s="T464">man </ts>
               <ts e="T466" id="Seg_4998" n="e" s="T465">tʼekga </ts>
               <ts e="T467" id="Seg_5000" n="e" s="T466">meǯau </ts>
               <ts e="T468" id="Seg_5002" n="e" s="T467">optɨ </ts>
               <ts e="T469" id="Seg_5004" n="e" s="T468">qudɨgo </ts>
               <ts e="T470" id="Seg_5006" n="e" s="T469">mennɨǯan. </ts>
               <ts e="T471" id="Seg_5008" n="e" s="T470">Üdəmɨɣɨn </ts>
               <ts e="T472" id="Seg_5010" n="e" s="T471">na </ts>
               <ts e="T473" id="Seg_5012" n="e" s="T472">tünnɨš, </ts>
               <ts e="T474" id="Seg_5014" n="e" s="T473">aj </ts>
               <ts e="T475" id="Seg_5016" n="e" s="T474">tʼärak: </ts>
               <ts e="T476" id="Seg_5018" n="e" s="T475">Qozɨrčɨlaj. </ts>
               <ts e="T477" id="Seg_5020" n="e" s="T476">Qut </ts>
               <ts e="T478" id="Seg_5022" n="e" s="T477">turakɨn </ts>
               <ts e="T479" id="Seg_5024" n="e" s="T478">qalʼeǯɨn, </ts>
               <ts e="T480" id="Seg_5026" n="e" s="T479">udlamdə </ts>
               <ts e="T481" id="Seg_5028" n="e" s="T480">moɣunä </ts>
               <ts e="T482" id="Seg_5030" n="e" s="T481">saːruku.” </ts>
               <ts e="T483" id="Seg_5032" n="e" s="T482">Näjɣum </ts>
               <ts e="T484" id="Seg_5034" n="e" s="T483">täbɨm </ts>
               <ts e="T485" id="Seg_5036" n="e" s="T484">apstɨpbaːt, </ts>
               <ts e="T486" id="Seg_5038" n="e" s="T485">arakazʼe </ts>
               <ts e="T487" id="Seg_5040" n="e" s="T486">ärčibat. </ts>
               <ts e="T488" id="Seg_5042" n="e" s="T487">Araqɨla </ts>
               <ts e="T489" id="Seg_5044" n="e" s="T488">üttɨdɨ. </ts>
               <ts e="T490" id="Seg_5046" n="e" s="T489">Täbeɣum </ts>
               <ts e="T491" id="Seg_5048" n="e" s="T490">qwannɨn. </ts>
               <ts e="T492" id="Seg_5050" n="e" s="T491">Ärat </ts>
               <ts e="T493" id="Seg_5052" n="e" s="T492">tüa. </ts>
               <ts e="T494" id="Seg_5054" n="e" s="T493">Surumlam </ts>
               <ts e="T495" id="Seg_5056" n="e" s="T494">koːcʼen </ts>
               <ts e="T496" id="Seg_5058" n="e" s="T495">qwatpat </ts>
               <ts e="T497" id="Seg_5060" n="e" s="T496">i </ts>
               <ts e="T498" id="Seg_5062" n="e" s="T497">pekqɨ </ts>
               <ts e="T499" id="Seg_5064" n="e" s="T498">qwatpadɨt. </ts>
               <ts e="T500" id="Seg_5066" n="e" s="T499">Täp </ts>
               <ts e="T501" id="Seg_5068" n="e" s="T500">täbɨm </ts>
               <ts e="T502" id="Seg_5070" n="e" s="T501">apstɨt. </ts>
               <ts e="T503" id="Seg_5072" n="e" s="T502">Tʼärɨn: </ts>
               <ts e="T504" id="Seg_5074" n="e" s="T503">“Qozɨrčɨlaj.” </ts>
               <ts e="T505" id="Seg_5076" n="e" s="T504">Ärat </ts>
               <ts e="T506" id="Seg_5078" n="e" s="T505">tʼärɨn: </ts>
               <ts e="T507" id="Seg_5080" n="e" s="T506">“Man </ts>
               <ts e="T508" id="Seg_5082" n="e" s="T507">nuːnɨdʼipbaːn.” </ts>
               <ts e="T509" id="Seg_5084" n="e" s="T508">Pajat </ts>
               <ts e="T510" id="Seg_5086" n="e" s="T509">tʼärɨn: </ts>
               <ts e="T511" id="Seg_5088" n="e" s="T510">“Qozɨrčɨlaj </ts>
               <ts e="T512" id="Seg_5090" n="e" s="T511">tʼem </ts>
               <ts e="T513" id="Seg_5092" n="e" s="T512">ugoworom, </ts>
               <ts e="T514" id="Seg_5094" n="e" s="T513">qudə </ts>
               <ts e="T515" id="Seg_5096" n="e" s="T514">qut </ts>
               <ts e="T516" id="Seg_5098" n="e" s="T515">turakɨn </ts>
               <ts e="T517" id="Seg_5100" n="e" s="T516">qalʼeǯɨn, </ts>
               <ts e="T518" id="Seg_5102" n="e" s="T517">udɨmdə </ts>
               <ts e="T519" id="Seg_5104" n="e" s="T518">moɣunä </ts>
               <ts e="T520" id="Seg_5106" n="e" s="T519">saːrugu. </ts>
               <ts e="T521" id="Seg_5108" n="e" s="T520">Ješlʼi </ts>
               <ts e="T522" id="Seg_5110" n="e" s="T521">man </ts>
               <ts e="T523" id="Seg_5112" n="e" s="T522">qalan </ts>
               <ts e="T524" id="Seg_5114" n="e" s="T523">duraqɨn, </ts>
               <ts e="T525" id="Seg_5116" n="e" s="T524">man </ts>
               <ts e="T526" id="Seg_5118" n="e" s="T525">udou </ts>
               <ts e="T527" id="Seg_5120" n="e" s="T526">saːraq </ts>
               <ts e="T528" id="Seg_5122" n="e" s="T527">(sareǯal).” </ts>
               <ts e="T529" id="Seg_5124" n="e" s="T528">Täbɨstaɣə </ts>
               <ts e="T530" id="Seg_5126" n="e" s="T529">omdaɣə </ts>
               <ts e="T531" id="Seg_5128" n="e" s="T530">qozɨrčilʼe. </ts>
               <ts e="T532" id="Seg_5130" n="e" s="T531">Ärat </ts>
               <ts e="T533" id="Seg_5132" n="e" s="T532">turaqɨn </ts>
               <ts e="T534" id="Seg_5134" n="e" s="T533">qalɨn. </ts>
               <ts e="T535" id="Seg_5136" n="e" s="T534">“Nu, </ts>
               <ts e="T536" id="Seg_5138" n="e" s="T535">täper </ts>
               <ts e="T537" id="Seg_5140" n="e" s="T536">udomdə </ts>
               <ts e="T538" id="Seg_5142" n="e" s="T537">saːreǯau.” </ts>
               <ts e="T539" id="Seg_5144" n="e" s="T538">Täp </ts>
               <ts e="T540" id="Seg_5146" n="e" s="T539">saːrɨt </ts>
               <ts e="T541" id="Seg_5148" n="e" s="T540">udɨmdə. </ts>
               <ts e="T542" id="Seg_5150" n="e" s="T541">Täp </ts>
               <ts e="T543" id="Seg_5152" n="e" s="T542">kak </ts>
               <ts e="T544" id="Seg_5154" n="e" s="T543">turgulǯɨtda, </ts>
               <ts e="T545" id="Seg_5156" n="e" s="T544">qudɨgo </ts>
               <ts e="T546" id="Seg_5158" n="e" s="T545">laqčɨdin. </ts>
               <ts e="T547" id="Seg_5160" n="e" s="T546">Quʒannaɣə </ts>
               <ts e="T548" id="Seg_5162" n="e" s="T547">qotduɣɨ. </ts>
               <ts e="T549" id="Seg_5164" n="e" s="T548">Qarʼemɨɣɨn </ts>
               <ts e="T550" id="Seg_5166" n="e" s="T549">wazaɣə. </ts>
               <ts e="T551" id="Seg_5168" n="e" s="T550">Näjɣum </ts>
               <ts e="T552" id="Seg_5170" n="e" s="T551">äramdə </ts>
               <ts e="T553" id="Seg_5172" n="e" s="T552">apstɨt. </ts>
               <ts e="T554" id="Seg_5174" n="e" s="T553">Ärat </ts>
               <ts e="T555" id="Seg_5176" n="e" s="T554">aurnɨn </ts>
               <ts e="T556" id="Seg_5178" n="e" s="T555">i </ts>
               <ts e="T557" id="Seg_5180" n="e" s="T556">madʼot </ts>
               <ts e="T558" id="Seg_5182" n="e" s="T557">na </ts>
               <ts e="T559" id="Seg_5184" n="e" s="T558">qwatda. </ts>
               <ts e="T560" id="Seg_5186" n="e" s="T559">Näiɣum </ts>
               <ts e="T561" id="Seg_5188" n="e" s="T560">ponä </ts>
               <ts e="T562" id="Seg_5190" n="e" s="T561">čaːǯɨn </ts>
               <ts e="T563" id="Seg_5192" n="e" s="T562">i </ts>
               <ts e="T564" id="Seg_5194" n="e" s="T563">na </ts>
               <ts e="T565" id="Seg_5196" n="e" s="T564">qaːronʼen: </ts>
               <ts e="T566" id="Seg_5198" n="e" s="T565">“Tükkɨ! </ts>
               <ts e="T567" id="Seg_5200" n="e" s="T566">Ärau </ts>
               <ts e="T568" id="Seg_5202" n="e" s="T567">qwanba!” </ts>
               <ts e="T569" id="Seg_5204" n="e" s="T568">Razbojnik </ts>
               <ts e="T570" id="Seg_5206" n="e" s="T569">tüwa. </ts>
               <ts e="T571" id="Seg_5208" n="e" s="T570">Näiɣum </ts>
               <ts e="T572" id="Seg_5210" n="e" s="T571">tʼärɨn: </ts>
               <ts e="T573" id="Seg_5212" n="e" s="T572">“Qudɨgolam </ts>
               <ts e="T574" id="Seg_5214" n="e" s="T573">ärau </ts>
               <ts e="T575" id="Seg_5216" n="e" s="T574">wes </ts>
               <ts e="T576" id="Seg_5218" n="e" s="T575">lakčelǯɨt. </ts>
               <ts e="T577" id="Seg_5220" n="e" s="T576">Täp </ts>
               <ts e="T578" id="Seg_5222" n="e" s="T577">orse </ts>
               <ts e="T579" id="Seg_5224" n="e" s="T578">jen.” </ts>
               <ts e="T580" id="Seg_5226" n="e" s="T579">“A </ts>
               <ts e="T581" id="Seg_5228" n="e" s="T580">man </ts>
               <ts e="T582" id="Seg_5230" n="e" s="T581">tamdʼel </ts>
               <ts e="T583" id="Seg_5232" n="e" s="T582">tekga </ts>
               <ts e="T584" id="Seg_5234" n="e" s="T583">meǯau </ts>
               <ts e="T585" id="Seg_5236" n="e" s="T584">kudɨgo </ts>
               <ts e="T586" id="Seg_5238" n="e" s="T585">optɨ </ts>
               <ts e="T587" id="Seg_5240" n="e" s="T586">i </ts>
               <ts e="T588" id="Seg_5242" n="e" s="T587">sɨnkowaja </ts>
               <ts e="T589" id="Seg_5244" n="e" s="T588">prowoloka </ts>
               <ts e="T590" id="Seg_5246" n="e" s="T589">okkɨrmɨt </ts>
               <ts e="T591" id="Seg_5248" n="e" s="T590">tamkɨlba.” </ts>
               <ts e="T592" id="Seg_5250" n="e" s="T591">Üːdɨmɨɣɨn </ts>
               <ts e="T593" id="Seg_5252" n="e" s="T592">na </ts>
               <ts e="T594" id="Seg_5254" n="e" s="T593">tünnɨš, </ts>
               <ts e="T595" id="Seg_5256" n="e" s="T594">tan </ts>
               <ts e="T596" id="Seg_5258" n="e" s="T595">aj </ts>
               <ts e="T597" id="Seg_5260" n="e" s="T596">tʼärak: </ts>
               <ts e="T598" id="Seg_5262" n="e" s="T597">Qozɨrčuɣu, </ts>
               <ts e="T599" id="Seg_5264" n="e" s="T598">qozɨrčilaj. </ts>
               <ts e="T600" id="Seg_5266" n="e" s="T599">Qut </ts>
               <ts e="T601" id="Seg_5268" n="e" s="T600">turakɨn </ts>
               <ts e="T602" id="Seg_5270" n="e" s="T601">kalʼeǯɨn, </ts>
               <ts e="T603" id="Seg_5272" n="e" s="T602">to </ts>
               <ts e="T604" id="Seg_5274" n="e" s="T603">udɨmdə </ts>
               <ts e="T605" id="Seg_5276" n="e" s="T604">moɣunä </ts>
               <ts e="T606" id="Seg_5278" n="e" s="T605">saːrugu. </ts>
               <ts e="T607" id="Seg_5280" n="e" s="T606">Ära </ts>
               <ts e="T608" id="Seg_5282" n="e" s="T607">tüa. </ts>
               <ts e="T609" id="Seg_5284" n="e" s="T608">Täp </ts>
               <ts e="T610" id="Seg_5286" n="e" s="T609">täbɨm </ts>
               <ts e="T611" id="Seg_5288" n="e" s="T610">apstɨt, </ts>
               <ts e="T612" id="Seg_5290" n="e" s="T611">tʼärɨn: </ts>
               <ts e="T613" id="Seg_5292" n="e" s="T612">“Qozɨrčɨlaj.” </ts>
               <ts e="T614" id="Seg_5294" n="e" s="T613">Ärat </ts>
               <ts e="T615" id="Seg_5296" n="e" s="T614">tʼärɨn: </ts>
               <ts e="T616" id="Seg_5298" n="e" s="T615">“Man </ts>
               <ts e="T617" id="Seg_5300" n="e" s="T616">nunɨdʼzʼipan, </ts>
               <ts e="T618" id="Seg_5302" n="e" s="T617">nadə </ts>
               <ts e="T619" id="Seg_5304" n="e" s="T618">qotdɨɣu.” </ts>
               <ts e="T620" id="Seg_5306" n="e" s="T619">“No </ts>
               <ts e="T621" id="Seg_5308" n="e" s="T620">qozɨrčɨlaj, </ts>
               <ts e="T622" id="Seg_5310" n="e" s="T621">qozɨrčɨlaj. </ts>
               <ts e="T623" id="Seg_5312" n="e" s="T622">Xotʼ </ts>
               <ts e="T624" id="Seg_5314" n="e" s="T623">okkɨrɨn </ts>
               <ts e="T625" id="Seg_5316" n="e" s="T624">qut </ts>
               <ts e="T626" id="Seg_5318" n="e" s="T625">turakɨn </ts>
               <ts e="T627" id="Seg_5320" n="e" s="T626">kalʼeǯɨn, </ts>
               <ts e="T628" id="Seg_5322" n="e" s="T627">udɨmdə </ts>
               <ts e="T629" id="Seg_5324" n="e" s="T628">moɣunä </ts>
               <ts e="T630" id="Seg_5326" n="e" s="T629">saaruku. </ts>
               <ts e="T631" id="Seg_5328" n="e" s="T630">Jeʒlʼi </ts>
               <ts e="T632" id="Seg_5330" n="e" s="T631">man </ts>
               <ts e="T633" id="Seg_5332" n="e" s="T632">kalʼeǯan, </ts>
               <ts e="T634" id="Seg_5334" n="e" s="T633">man </ts>
               <ts e="T635" id="Seg_5336" n="e" s="T634">udou </ts>
               <ts e="T636" id="Seg_5338" n="e" s="T635">saːrʼet </ts>
               <ts e="T637" id="Seg_5340" n="e" s="T636">moɣunä.” </ts>
               <ts e="T638" id="Seg_5342" n="e" s="T637">I </ts>
               <ts e="T639" id="Seg_5344" n="e" s="T638">oːmdaɣə </ts>
               <ts e="T640" id="Seg_5346" n="e" s="T639">qozɨrčɨlʼe. </ts>
               <ts e="T641" id="Seg_5348" n="e" s="T640">Äramdə </ts>
               <ts e="T642" id="Seg_5350" n="e" s="T641">turagɨwlʼe </ts>
               <ts e="T643" id="Seg_5352" n="e" s="T642">qwɛdʼit, </ts>
               <ts e="T644" id="Seg_5354" n="e" s="T643">udɨmdə </ts>
               <ts e="T645" id="Seg_5356" n="e" s="T644">moɣunä </ts>
               <ts e="T646" id="Seg_5358" n="e" s="T645">saːrɨt. </ts>
               <ts e="T647" id="Seg_5360" n="e" s="T646">Täp </ts>
               <ts e="T648" id="Seg_5362" n="e" s="T647">kak </ts>
               <ts e="T649" id="Seg_5364" n="e" s="T648">turgulǯɨtda, </ts>
               <ts e="T650" id="Seg_5366" n="e" s="T649">qudəkolam </ts>
               <ts e="T651" id="Seg_5368" n="e" s="T650">jass </ts>
               <ts e="T652" id="Seg_5370" n="e" s="T651">laqčɨt. </ts>
               <ts e="T653" id="Seg_5372" n="e" s="T652">Pajätdänä </ts>
               <ts e="T654" id="Seg_5374" n="e" s="T653">tʼärɨn: </ts>
               <ts e="T655" id="Seg_5376" n="e" s="T654">“Tʼiket </ts>
               <ts e="T656" id="Seg_5378" n="e" s="T655">udou.” </ts>
               <ts e="T657" id="Seg_5380" n="e" s="T656">Pajät </ts>
               <ts e="T658" id="Seg_5382" n="e" s="T657">tʼärɨn: </ts>
               <ts e="T659" id="Seg_5384" n="e" s="T658">“Lakčet.” </ts>
               <ts e="T660" id="Seg_5386" n="e" s="T659">Täp </ts>
               <ts e="T661" id="Seg_5388" n="e" s="T660">aj </ts>
               <ts e="T662" id="Seg_5390" n="e" s="T661">turgulǯɨn, </ts>
               <ts e="T663" id="Seg_5392" n="e" s="T662">jass </ts>
               <ts e="T664" id="Seg_5394" n="e" s="T663">lakčɨt. </ts>
               <ts e="T665" id="Seg_5396" n="e" s="T664">Tʼärɨn: </ts>
               <ts e="T666" id="Seg_5398" n="e" s="T665">“Tʼiket.” </ts>
               <ts e="T667" id="Seg_5400" n="e" s="T666">“Ass </ts>
               <ts e="T668" id="Seg_5402" n="e" s="T667">tʼikeǯau.” </ts>
               <ts e="T669" id="Seg_5404" n="e" s="T668">Ponä </ts>
               <ts e="T670" id="Seg_5406" n="e" s="T669">sappɨselʼe </ts>
               <ts e="T671" id="Seg_5408" n="e" s="T670">qwannɨn </ts>
               <ts e="T672" id="Seg_5410" n="e" s="T671">i </ts>
               <ts e="T673" id="Seg_5412" n="e" s="T672">na </ts>
               <ts e="T674" id="Seg_5414" n="e" s="T673">lakgolʼdʼä: </ts>
               <ts e="T675" id="Seg_5416" n="e" s="T674">“Tükkɨ!” </ts>
               <ts e="T676" id="Seg_5418" n="e" s="T675">I </ts>
               <ts e="T677" id="Seg_5420" n="e" s="T676">na </ts>
               <ts e="T678" id="Seg_5422" n="e" s="T677">razbojnik </ts>
               <ts e="T679" id="Seg_5424" n="e" s="T678">na </ts>
               <ts e="T680" id="Seg_5426" n="e" s="T679">tütda. </ts>
               <ts e="T681" id="Seg_5428" n="e" s="T680">Tʼärɨn:“ </ts>
               <ts e="T682" id="Seg_5430" n="e" s="T681">“Ärau </ts>
               <ts e="T683" id="Seg_5432" n="e" s="T682">qudogom </ts>
               <ts e="T684" id="Seg_5434" n="e" s="T683">ass </ts>
               <ts e="T685" id="Seg_5436" n="e" s="T684">lakčɨt. </ts>
               <ts e="T686" id="Seg_5438" n="e" s="T685">Serlʼe </ts>
               <ts e="T687" id="Seg_5440" n="e" s="T686">qwallaj.” </ts>
               <ts e="T688" id="Seg_5442" n="e" s="T687">I </ts>
               <ts e="T689" id="Seg_5444" n="e" s="T688">mattə </ts>
               <ts e="T690" id="Seg_5446" n="e" s="T689">sernaɣə. </ts>
               <ts e="T691" id="Seg_5448" n="e" s="T690">Razbojnik </ts>
               <ts e="T692" id="Seg_5450" n="e" s="T691">tʼärɨn: </ts>
               <ts e="T693" id="Seg_5452" n="e" s="T692">“Qardʼen </ts>
               <ts e="T694" id="Seg_5454" n="e" s="T693">qarʼemɨɣɨn </ts>
               <ts e="T695" id="Seg_5456" n="e" s="T694">qwatčaj. </ts>
               <ts e="T696" id="Seg_5458" n="e" s="T695">Tulǯät </ts>
               <ts e="T697" id="Seg_5460" n="e" s="T696">wʼädran </ts>
               <ts e="T698" id="Seg_5462" n="e" s="T697">tʼiːr </ts>
               <ts e="T699" id="Seg_5464" n="e" s="T698">saːq. </ts>
               <ts e="T700" id="Seg_5466" n="e" s="T699">Täbɨm </ts>
               <ts e="T701" id="Seg_5468" n="e" s="T700">saːɣɨn </ts>
               <ts e="T702" id="Seg_5470" n="e" s="T701">parotdə </ts>
               <ts e="T703" id="Seg_5472" n="e" s="T702">puləsejlatdɨze </ts>
               <ts e="T704" id="Seg_5474" n="e" s="T703">(saːɣot) </ts>
               <ts e="T705" id="Seg_5476" n="e" s="T704">omdɨlǯeǯaj. </ts>
               <ts e="T706" id="Seg_5478" n="e" s="T705">Qarʼeməttə </ts>
               <ts e="T707" id="Seg_5480" n="e" s="T706">aːmdə. </ts>
               <ts e="T708" id="Seg_5482" n="e" s="T707">Qarʼemɨɣɨn </ts>
               <ts e="T709" id="Seg_5484" n="e" s="T708">qwatčaj.” </ts>
               <ts e="T710" id="Seg_5486" n="e" s="T709">Näjɣum </ts>
               <ts e="T711" id="Seg_5488" n="e" s="T710">watdoɣɨt </ts>
               <ts e="T712" id="Seg_5490" n="e" s="T711">tülʼe </ts>
               <ts e="T713" id="Seg_5492" n="e" s="T712">na </ts>
               <ts e="T714" id="Seg_5494" n="e" s="T713">söːzɨtdət. </ts>
               <ts e="T715" id="Seg_5496" n="e" s="T714">“Tan </ts>
               <ts e="T716" id="Seg_5498" n="e" s="T715">tʼärɨkuzat </ts>
               <ts e="T717" id="Seg_5500" n="e" s="T716">što </ts>
               <ts e="T718" id="Seg_5502" n="e" s="T717">man </ts>
               <ts e="T719" id="Seg_5504" n="e" s="T718">orse </ts>
               <ts e="T720" id="Seg_5506" n="e" s="T719">jewan. </ts>
               <ts e="T721" id="Seg_5508" n="e" s="T720">A </ts>
               <ts e="T722" id="Seg_5510" n="e" s="T721">qajno </ts>
               <ts e="T723" id="Seg_5512" n="e" s="T722">qudɨgom </ts>
               <ts e="T724" id="Seg_5514" n="e" s="T723">ass </ts>
               <ts e="T725" id="Seg_5516" n="e" s="T724">laqčeǯal? </ts>
               <ts e="T726" id="Seg_5518" n="e" s="T725">Tapär </ts>
               <ts e="T727" id="Seg_5520" n="e" s="T726">qarʼemɨɣɨn </ts>
               <ts e="T728" id="Seg_5522" n="e" s="T727">tastɨ </ts>
               <ts e="T729" id="Seg_5524" n="e" s="T728">qwatčaj.” </ts>
               <ts e="T730" id="Seg_5526" n="e" s="T729">Näjɣum </ts>
               <ts e="T731" id="Seg_5528" n="e" s="T730">quronnɨn, </ts>
               <ts e="T732" id="Seg_5530" n="e" s="T731">saːq </ts>
               <ts e="T733" id="Seg_5532" n="e" s="T732">tulǯɨtdɨt </ts>
               <ts e="T734" id="Seg_5534" n="e" s="T733">wädran </ts>
               <ts e="T735" id="Seg_5536" n="e" s="T734">tiːr </ts>
               <ts e="T736" id="Seg_5538" n="e" s="T735">i </ts>
               <ts e="T737" id="Seg_5540" n="e" s="T736">salǯɨbot </ts>
               <ts e="T738" id="Seg_5542" n="e" s="T737">qamǯɨt. </ts>
               <ts e="T739" id="Seg_5544" n="e" s="T738">I </ts>
               <ts e="T740" id="Seg_5546" n="e" s="T739">täbɨm </ts>
               <ts e="T741" id="Seg_5548" n="e" s="T740">puluzein </ts>
               <ts e="T742" id="Seg_5550" n="e" s="T741">omdɨlǯɨt. </ts>
               <ts e="T743" id="Seg_5552" n="e" s="T742">Tʼärɨn:“ </ts>
               <ts e="T744" id="Seg_5554" n="e" s="T743">“Aːmdaq </ts>
               <ts e="T745" id="Seg_5556" n="e" s="T744">qarʼemɨtdə! </ts>
               <ts e="T746" id="Seg_5558" n="e" s="T745">Qarʼemɨɣɨn </ts>
               <ts e="T747" id="Seg_5560" n="e" s="T746">tastɨ </ts>
               <ts e="T748" id="Seg_5562" n="e" s="T747">qwatčaj.” </ts>
               <ts e="T749" id="Seg_5564" n="e" s="T748">Omdaɣə </ts>
               <ts e="T750" id="Seg_5566" n="e" s="T749">aurlʼe </ts>
               <ts e="T751" id="Seg_5568" n="e" s="T750">razbojnikse </ts>
               <ts e="T752" id="Seg_5570" n="e" s="T751">arakaj </ts>
               <ts e="T753" id="Seg_5572" n="e" s="T752">ärlʼe </ts>
               <ts e="T754" id="Seg_5574" n="e" s="T753">jübɨraq. </ts>
               <ts e="T755" id="Seg_5576" n="e" s="T754">Näjɣum </ts>
               <ts e="T756" id="Seg_5578" n="e" s="T755">tüuwa, </ts>
               <ts e="T757" id="Seg_5580" n="e" s="T756">äratdə </ts>
               <ts e="T758" id="Seg_5582" n="e" s="T757">kötdə </ts>
               <ts e="T759" id="Seg_5584" n="e" s="T758">äramdə </ts>
               <ts e="T760" id="Seg_5586" n="e" s="T759">watdoot </ts>
               <ts e="T761" id="Seg_5588" n="e" s="T760">na </ts>
               <ts e="T762" id="Seg_5590" n="e" s="T761">sözɨtdət. </ts>
               <ts e="T763" id="Seg_5592" n="e" s="T762">“Qarʼemɨɣɨn </ts>
               <ts e="T764" id="Seg_5594" n="e" s="T763">me </ts>
               <ts e="T765" id="Seg_5596" n="e" s="T764">tastɨ </ts>
               <ts e="T766" id="Seg_5598" n="e" s="T765">qwatčaj.” </ts>
               <ts e="T767" id="Seg_5600" n="e" s="T766">Otdə </ts>
               <ts e="T768" id="Seg_5602" n="e" s="T767">na </ts>
               <ts e="T769" id="Seg_5604" n="e" s="T768">qwatda </ts>
               <ts e="T770" id="Seg_5606" n="e" s="T769">razbojnikɨm </ts>
               <ts e="T771" id="Seg_5608" n="e" s="T770">kaːwalgut, </ts>
               <ts e="T772" id="Seg_5610" n="e" s="T771">niːdellʼe </ts>
               <ts e="T773" id="Seg_5612" n="e" s="T772">taːdɨrɨt. </ts>
               <ts e="T774" id="Seg_5614" n="e" s="T773">I </ts>
               <ts e="T775" id="Seg_5616" n="e" s="T774">qotduɣu </ts>
               <ts e="T776" id="Seg_5618" n="e" s="T775">quʒannax </ts>
               <ts e="T777" id="Seg_5620" n="e" s="T776">i </ts>
               <ts e="T778" id="Seg_5622" n="e" s="T777">na </ts>
               <ts e="T779" id="Seg_5624" n="e" s="T778">qotdolǯaɣɨ. </ts>
               <ts e="T780" id="Seg_5626" n="e" s="T779">(Ärat) </ts>
               <ts e="T781" id="Seg_5628" n="e" s="T780">täp </ts>
               <ts e="T782" id="Seg_5630" n="e" s="T781">wazɨn </ts>
               <ts e="T783" id="Seg_5632" n="e" s="T782">innä, </ts>
               <ts e="T784" id="Seg_5634" n="e" s="T783">paldʼukun. </ts>
               <ts e="T785" id="Seg_5636" n="e" s="T784">A </ts>
               <ts e="T786" id="Seg_5638" n="e" s="T785">kɨːban </ts>
               <ts e="T787" id="Seg_5640" n="e" s="T786">nägat </ts>
               <ts e="T788" id="Seg_5642" n="e" s="T787">iːɣat </ts>
               <ts e="T789" id="Seg_5644" n="e" s="T788">täbɨstaːɣə </ts>
               <ts e="T790" id="Seg_5646" n="e" s="T789">qotdɨzaɣə </ts>
               <ts e="T791" id="Seg_5648" n="e" s="T790">palatin </ts>
               <ts e="T792" id="Seg_5650" n="e" s="T791">paroɣɨn. </ts>
               <ts e="T793" id="Seg_5652" n="e" s="T792">Nägat </ts>
               <ts e="T794" id="Seg_5654" n="e" s="T793">küzɨgu </ts>
               <ts e="T795" id="Seg_5656" n="e" s="T794">ilʼlʼe </ts>
               <ts e="T796" id="Seg_5658" n="e" s="T795">tʼütʼöun. </ts>
               <ts e="T797" id="Seg_5660" n="e" s="T796">Täp </ts>
               <ts e="T798" id="Seg_5662" n="e" s="T797">nägatdanä </ts>
               <ts e="T799" id="Seg_5664" n="e" s="T798">tʼärɨn: </ts>
               <ts e="T800" id="Seg_5666" n="e" s="T799">“Man </ts>
               <ts e="T801" id="Seg_5668" n="e" s="T800">udlau </ts>
               <ts e="T802" id="Seg_5670" n="e" s="T801">tʼikɨlʼel.” </ts>
               <ts e="T803" id="Seg_5672" n="e" s="T802">A </ts>
               <ts e="T804" id="Seg_5674" n="e" s="T803">nägat </ts>
               <ts e="T805" id="Seg_5676" n="e" s="T804">tʼärɨn: </ts>
               <ts e="T806" id="Seg_5678" n="e" s="T805">“Tan </ts>
               <ts e="T807" id="Seg_5680" n="e" s="T806">tʼärukuzat </ts>
               <ts e="T808" id="Seg_5682" n="e" s="T807">što </ts>
               <ts e="T809" id="Seg_5684" n="e" s="T808">orse </ts>
               <ts e="T810" id="Seg_5686" n="e" s="T809">jewan. </ts>
               <ts e="T811" id="Seg_5688" n="e" s="T810">Lakčelǯet </ts>
               <ts e="T812" id="Seg_5690" n="e" s="T811">qudɨgom.” </ts>
               <ts e="T813" id="Seg_5692" n="e" s="T812">Palatin </ts>
               <ts e="T814" id="Seg_5694" n="e" s="T813">barot </ts>
               <ts e="T815" id="Seg_5696" n="e" s="T814">čaǯɨn </ts>
               <ts e="T816" id="Seg_5698" n="e" s="T815">i </ts>
               <ts e="T817" id="Seg_5700" n="e" s="T816">qotda. </ts>
               <ts e="T818" id="Seg_5702" n="e" s="T817">Iːgat </ts>
               <ts e="T819" id="Seg_5704" n="e" s="T818">palatiɣəndo </ts>
               <ts e="T820" id="Seg_5706" n="e" s="T819">ilʼlʼe </ts>
               <ts e="T821" id="Seg_5708" n="e" s="T820">tʼütʼöun </ts>
               <ts e="T822" id="Seg_5710" n="e" s="T821">küzɨgu. </ts>
               <ts e="T823" id="Seg_5712" n="e" s="T822">Täp </ts>
               <ts e="T824" id="Seg_5714" n="e" s="T823">iːgatdänä </ts>
               <ts e="T825" id="Seg_5716" n="e" s="T824">tʼärɨn: </ts>
               <ts e="T826" id="Seg_5718" n="e" s="T825">“Man </ts>
               <ts e="T827" id="Seg_5720" n="e" s="T826">udou </ts>
               <ts e="T828" id="Seg_5722" n="e" s="T827">tʼiːkelʼel.” </ts>
               <ts e="T829" id="Seg_5724" n="e" s="T828">Täp </ts>
               <ts e="T830" id="Seg_5726" n="e" s="T829">tüwa </ts>
               <ts e="T831" id="Seg_5728" n="e" s="T830">äzɨtdänä </ts>
               <ts e="T832" id="Seg_5730" n="e" s="T831">i </ts>
               <ts e="T833" id="Seg_5732" n="e" s="T832">tʼikəlʼe </ts>
               <ts e="T834" id="Seg_5734" n="e" s="T833">jübərat. </ts>
               <ts e="T835" id="Seg_5736" n="e" s="T834">Jass </ts>
               <ts e="T836" id="Seg_5738" n="e" s="T835">tʼikɨt. </ts>
               <ts e="T837" id="Seg_5740" n="e" s="T836">“Mekga </ts>
               <ts e="T838" id="Seg_5742" n="e" s="T837">jass </ts>
               <ts e="T839" id="Seg_5744" n="e" s="T838">tʼikɨɣuː.” </ts>
               <ts e="T840" id="Seg_5746" n="e" s="T839">Ästɨ </ts>
               <ts e="T841" id="Seg_5748" n="e" s="T840">tʼärɨn: </ts>
               <ts e="T842" id="Seg_5750" n="e" s="T841">“Patpʼilka </ts>
               <ts e="T843" id="Seg_5752" n="e" s="T842">datkə.” </ts>
               <ts e="T844" id="Seg_5754" n="e" s="T843">Täp </ts>
               <ts e="T845" id="Seg_5756" n="e" s="T844">tannɨt </ts>
               <ts e="T846" id="Seg_5758" n="e" s="T845">i </ts>
               <ts e="T847" id="Seg_5760" n="e" s="T846">kudɨgolam </ts>
               <ts e="T848" id="Seg_5762" n="e" s="T847">sɨlʼelʼlʼe </ts>
               <ts e="T849" id="Seg_5764" n="e" s="T848">na </ts>
               <ts e="T850" id="Seg_5766" n="e" s="T849">übərətdɨt. </ts>
               <ts e="T851" id="Seg_5768" n="e" s="T850">Sɨlɨt, </ts>
               <ts e="T852" id="Seg_5770" n="e" s="T851">qudəgola </ts>
               <ts e="T853" id="Seg_5772" n="e" s="T852">warsɨwannat. </ts>
               <ts e="T854" id="Seg_5774" n="e" s="T853">Täp </ts>
               <ts e="T855" id="Seg_5776" n="e" s="T854">iːgatdänä </ts>
               <ts e="T856" id="Seg_5778" n="e" s="T855">tʼarɨn: </ts>
               <ts e="T857" id="Seg_5780" n="e" s="T856">“Qwalʼlʼe </ts>
               <ts e="T858" id="Seg_5782" n="e" s="T857">qotdaq. </ts>
               <ts e="T859" id="Seg_5784" n="e" s="T858">Qarʼemɨɣɨn </ts>
               <ts e="T860" id="Seg_5786" n="e" s="T859">iːgə </ts>
               <ts e="T861" id="Seg_5788" n="e" s="T860">larɨpbaːk. </ts>
               <ts e="T862" id="Seg_5790" n="e" s="T861">Me </ts>
               <ts e="T863" id="Seg_5792" n="e" s="T862">tazʼe </ts>
               <ts e="T864" id="Seg_5794" n="e" s="T863">onäj </ts>
               <ts e="T865" id="Seg_5796" n="e" s="T864">ilʼetǯaj.” </ts>
               <ts e="T866" id="Seg_5798" n="e" s="T865">Nom </ts>
               <ts e="T867" id="Seg_5800" n="e" s="T866">tʼelɨmlʼe </ts>
               <ts e="T868" id="Seg_5802" n="e" s="T867">jübɨrɨn. </ts>
               <ts e="T869" id="Seg_5804" n="e" s="T868">Täp </ts>
               <ts e="T870" id="Seg_5806" n="e" s="T869">kudɨgolam </ts>
               <ts e="T871" id="Seg_5808" n="e" s="T870">udoɣətdə </ts>
               <ts e="T872" id="Seg_5810" n="e" s="T871">pennɨt </ts>
               <ts e="T873" id="Seg_5812" n="e" s="T872">(kak </ts>
               <ts e="T874" id="Seg_5814" n="e" s="T873">budto </ts>
               <ts e="T875" id="Seg_5816" n="e" s="T874">saːrɨpbaːt) </ts>
               <ts e="T876" id="Seg_5818" n="e" s="T875">i </ts>
               <ts e="T877" id="Seg_5820" n="e" s="T876">omdɨn </ts>
               <ts e="T878" id="Seg_5822" n="e" s="T877">puluzein </ts>
               <ts e="T879" id="Seg_5824" n="e" s="T878">parot. </ts>
               <ts e="T880" id="Seg_5826" n="e" s="T879">Pajät </ts>
               <ts e="T881" id="Seg_5828" n="e" s="T880">wazɨn, </ts>
               <ts e="T882" id="Seg_5830" n="e" s="T881">tülʼe </ts>
               <ts e="T883" id="Seg_5832" n="e" s="T882">watdoɨn </ts>
               <ts e="T884" id="Seg_5834" n="e" s="T883">söːzɨt. </ts>
               <ts e="T885" id="Seg_5836" n="e" s="T884">Razbojnignä </ts>
               <ts e="T886" id="Seg_5838" n="e" s="T885">tʼärɨn: </ts>
               <ts e="T887" id="Seg_5840" n="e" s="T886">“Nu </ts>
               <ts e="T888" id="Seg_5842" n="e" s="T887">qwallaj.” </ts>
               <ts e="T889" id="Seg_5844" n="e" s="T888">A </ts>
               <ts e="T890" id="Seg_5846" n="e" s="T889">razbojnik </ts>
               <ts e="T891" id="Seg_5848" n="e" s="T890">tʼärɨn: </ts>
               <ts e="T892" id="Seg_5850" n="e" s="T891">“Aurlʼe </ts>
               <ts e="T893" id="Seg_5852" n="e" s="T892">qwallajze.” </ts>
               <ts e="T894" id="Seg_5854" n="e" s="T893">Tep </ts>
               <ts e="T895" id="Seg_5856" n="e" s="T894">wadʼilam </ts>
               <ts e="T896" id="Seg_5858" n="e" s="T895">müzurɣɨnnät. </ts>
               <ts e="T897" id="Seg_5860" n="e" s="T896">Aurnaɣə. </ts>
               <ts e="T898" id="Seg_5862" n="e" s="T897">“Nu </ts>
               <ts e="T899" id="Seg_5864" n="e" s="T898">täpär </ts>
               <ts e="T900" id="Seg_5866" n="e" s="T899">qwallaj. </ts>
               <ts e="T901" id="Seg_5868" n="e" s="T900">Maːtqɨn </ts>
               <ts e="T902" id="Seg_5870" n="e" s="T901">qwatčaj </ts>
               <ts e="T903" id="Seg_5872" n="e" s="T902">alʼi </ts>
               <ts e="T904" id="Seg_5874" n="e" s="T903">ponän?” </ts>
               <ts e="T905" id="Seg_5876" n="e" s="T904">Razbojnik </ts>
               <ts e="T906" id="Seg_5878" n="e" s="T905">tʼärɨn: </ts>
               <ts e="T907" id="Seg_5880" n="e" s="T906">“Ponän </ts>
               <ts e="T908" id="Seg_5882" n="e" s="T907">qwatčaj. </ts>
               <ts e="T909" id="Seg_5884" n="e" s="T908">Nu </ts>
               <ts e="T910" id="Seg_5886" n="e" s="T909">wazak, </ts>
               <ts e="T911" id="Seg_5888" n="e" s="T910">čaːʒɨk </ts>
               <ts e="T912" id="Seg_5890" n="e" s="T911">ponä. </ts>
               <ts e="T913" id="Seg_5892" n="e" s="T912">Tastɨ </ts>
               <ts e="T914" id="Seg_5894" n="e" s="T913">qwatčaj. </ts>
               <ts e="T915" id="Seg_5896" n="e" s="T914">Qaj </ts>
               <ts e="T916" id="Seg_5898" n="e" s="T915">kudɨgolam </ts>
               <ts e="T917" id="Seg_5900" n="e" s="T916">jass </ts>
               <ts e="T918" id="Seg_5902" n="e" s="T917">lakčelǯal. </ts>
               <ts e="T919" id="Seg_5904" n="e" s="T918">Tan </ts>
               <ts e="T920" id="Seg_5906" n="e" s="T919">tʼärukuzattə, </ts>
               <ts e="T921" id="Seg_5908" n="e" s="T920">orse </ts>
               <ts e="T922" id="Seg_5910" n="e" s="T921">jewan.” </ts>
               <ts e="T923" id="Seg_5912" n="e" s="T922">Näuɣum </ts>
               <ts e="T924" id="Seg_5914" n="e" s="T923">kak </ts>
               <ts e="T925" id="Seg_5916" n="e" s="T924">watdoutdə </ts>
               <ts e="T926" id="Seg_5918" n="e" s="T925">sözɨtdɨt. </ts>
               <ts e="T927" id="Seg_5920" n="e" s="T926">Täp </ts>
               <ts e="T928" id="Seg_5922" n="e" s="T927">kak </ts>
               <ts e="T929" id="Seg_5924" n="e" s="T928">innä </ts>
               <ts e="T930" id="Seg_5926" n="e" s="T929">wazʼezitda, </ts>
               <ts e="T931" id="Seg_5928" n="e" s="T930">kak </ts>
               <ts e="T932" id="Seg_5930" n="e" s="T931">razbojnikam </ts>
               <ts e="T933" id="Seg_5932" n="e" s="T932">qättɨdɨt. </ts>
               <ts e="T934" id="Seg_5934" n="e" s="T933">Razbojnik </ts>
               <ts e="T935" id="Seg_5936" n="e" s="T934">qotä </ts>
               <ts e="T936" id="Seg_5938" n="e" s="T935">čäčädʼzʼen </ts>
               <ts e="T937" id="Seg_5940" n="e" s="T936">(čäčädin). </ts>
               <ts e="T938" id="Seg_5942" n="e" s="T937">I </ts>
               <ts e="T939" id="Seg_5944" n="e" s="T938">qwannɨt. </ts>
               <ts e="T940" id="Seg_5946" n="e" s="T939">A </ts>
               <ts e="T941" id="Seg_5948" n="e" s="T940">pajät </ts>
               <ts e="T942" id="Seg_5950" n="e" s="T941">sojoɣotdə </ts>
               <ts e="T943" id="Seg_5952" n="e" s="T942">ɨdədʼen. </ts>
               <ts e="T944" id="Seg_5954" n="e" s="T943">Tʼärɨn:“ </ts>
               <ts e="T945" id="Seg_5956" n="e" s="T944">“Man </ts>
               <ts e="T946" id="Seg_5958" n="e" s="T945">nɨlʼdʼin </ts>
               <ts e="T947" id="Seg_5960" n="e" s="T946">ass </ts>
               <ts e="T948" id="Seg_5962" n="e" s="T947">meːqweǯau. </ts>
               <ts e="T949" id="Seg_5964" n="e" s="T948">Toblamdə </ts>
               <ts e="T950" id="Seg_5966" n="e" s="T949">müzulǯukeǯau. </ts>
               <ts e="T951" id="Seg_5968" n="e" s="T950">Na </ts>
               <ts e="T952" id="Seg_5970" n="e" s="T951">ödɨm </ts>
               <ts e="T953" id="Seg_5972" n="e" s="T952">ütkeǯau.” </ts>
               <ts e="T954" id="Seg_5974" n="e" s="T953">Erat </ts>
               <ts e="T955" id="Seg_5976" n="e" s="T954">kak </ts>
               <ts e="T956" id="Seg_5978" n="e" s="T955">qäːtäǯɨt. </ts>
               <ts e="T957" id="Seg_5980" n="e" s="T956">I </ts>
               <ts e="T958" id="Seg_5982" n="e" s="T957">qwannɨt. </ts>
               <ts e="T959" id="Seg_5984" n="e" s="T958">Qɨbanʼaʒala </ts>
               <ts e="T960" id="Seg_5986" n="e" s="T959">türlʼe </ts>
               <ts e="T961" id="Seg_5988" n="e" s="T960">übərat. </ts>
               <ts e="T962" id="Seg_5990" n="e" s="T961">Nägamdə </ts>
               <ts e="T963" id="Seg_5992" n="e" s="T962">kak </ts>
               <ts e="T964" id="Seg_5994" n="e" s="T963">qätɨtdɨt </ts>
               <ts e="T965" id="Seg_5996" n="e" s="T964">i </ts>
               <ts e="T966" id="Seg_5998" n="e" s="T965">qwannɨt. </ts>
               <ts e="T967" id="Seg_6000" n="e" s="T966">I </ts>
               <ts e="T968" id="Seg_6002" n="e" s="T967">iːgatdänä </ts>
               <ts e="T969" id="Seg_6004" n="e" s="T968">tʼärɨn: </ts>
               <ts e="T970" id="Seg_6006" n="e" s="T969">“Me </ts>
               <ts e="T971" id="Seg_6008" n="e" s="T970">taze </ts>
               <ts e="T972" id="Seg_6010" n="e" s="T971">ilʼetǯaj. </ts>
               <ts e="T973" id="Seg_6012" n="e" s="T972">Igə </ts>
               <ts e="T974" id="Seg_6014" n="e" s="T973">türak.” </ts>
               <ts e="T975" id="Seg_6016" n="e" s="T974">Qwannɨn </ts>
               <ts e="T976" id="Seg_6018" n="e" s="T975">tʼüj </ts>
               <ts e="T977" id="Seg_6020" n="e" s="T976">baɣattɨlʼe. </ts>
               <ts e="T978" id="Seg_6022" n="e" s="T977">Tüj </ts>
               <ts e="T979" id="Seg_6024" n="e" s="T978">qɨlɨm </ts>
               <ts e="T980" id="Seg_6026" n="e" s="T979">paqkɨnɨt. </ts>
               <ts e="T981" id="Seg_6028" n="e" s="T980">Täblam </ts>
               <ts e="T982" id="Seg_6030" n="e" s="T981">natʼet </ts>
               <ts e="T983" id="Seg_6032" n="e" s="T982">qäːlɣɨnɨt </ts>
               <ts e="T984" id="Seg_6034" n="e" s="T983">(qännɨt) </ts>
               <ts e="T985" id="Seg_6036" n="e" s="T984">i </ts>
               <ts e="T986" id="Seg_6038" n="e" s="T985">tʼüze </ts>
               <ts e="T987" id="Seg_6040" n="e" s="T986">taɣɨnɨt. </ts>
               <ts e="T988" id="Seg_6042" n="e" s="T987">Saoznikamdə </ts>
               <ts e="T989" id="Seg_6044" n="e" s="T988">qarʼe </ts>
               <ts e="T990" id="Seg_6046" n="e" s="T989">ükkulʼe </ts>
               <ts e="T991" id="Seg_6048" n="e" s="T990">qwatdɨt. </ts>
               <ts e="T992" id="Seg_6050" n="e" s="T991">Tädomɨlamdə </ts>
               <ts e="T993" id="Seg_6052" n="e" s="T992">wes </ts>
               <ts e="T994" id="Seg_6054" n="e" s="T993">tuɣunnɨt. </ts>
               <ts e="T995" id="Seg_6056" n="e" s="T994">Iːgatdɨze </ts>
               <ts e="T996" id="Seg_6058" n="e" s="T995">omdaɣə </ts>
               <ts e="T997" id="Seg_6060" n="e" s="T996">i </ts>
               <ts e="T998" id="Seg_6062" n="e" s="T997">na </ts>
               <ts e="T999" id="Seg_6064" n="e" s="T998">qwatdaːq. </ts>
               <ts e="T1000" id="Seg_6066" n="e" s="T999">Tʼülʼe </ts>
               <ts e="T1001" id="Seg_6068" n="e" s="T1000">meːdaɣə </ts>
               <ts e="T1002" id="Seg_6070" n="e" s="T1001">otdə </ts>
               <ts e="T1003" id="Seg_6072" n="e" s="T1002">jedoɣotdə. </ts>
               <ts e="T1004" id="Seg_6074" n="e" s="T1003">Qulat </ts>
               <ts e="T1005" id="Seg_6076" n="e" s="T1004">aːtdalbat. </ts>
               <ts e="T1006" id="Seg_6078" n="e" s="T1005">“A </ts>
               <ts e="T1007" id="Seg_6080" n="e" s="T1006">pajal </ts>
               <ts e="T1008" id="Seg_6082" n="e" s="T1007">kuttʼen </ts>
               <ts e="T1009" id="Seg_6084" n="e" s="T1008">jen?” </ts>
               <ts e="T1010" id="Seg_6086" n="e" s="T1009">“Pajau </ts>
               <ts e="T1011" id="Seg_6088" n="e" s="T1010">qupba.” </ts>
               <ts e="T1012" id="Seg_6090" n="e" s="T1011">Wes </ts>
               <ts e="T1013" id="Seg_6092" n="e" s="T1012">tugunattə </ts>
               <ts e="T1014" id="Seg_6094" n="e" s="T1013">tädomɨm. </ts>
               <ts e="T1015" id="Seg_6096" n="e" s="T1014">Täp </ts>
               <ts e="T1016" id="Seg_6098" n="e" s="T1015">pirʼäɣɨtdə </ts>
               <ts e="T1017" id="Seg_6100" n="e" s="T1016">maːt </ts>
               <ts e="T1018" id="Seg_6102" n="e" s="T1017">tautdɨt. </ts>
               <ts e="T1019" id="Seg_6104" n="e" s="T1018">I </ts>
               <ts e="T1020" id="Seg_6106" n="e" s="T1019">nädɨn </ts>
               <ts e="T1021" id="Seg_6108" n="e" s="T1020">i </ts>
               <ts e="T1022" id="Seg_6110" n="e" s="T1021">warkɨlʼe </ts>
               <ts e="T1023" id="Seg_6112" n="e" s="T1022">na </ts>
               <ts e="T1024" id="Seg_6114" n="e" s="T1023">übɨrɨtda. </ts>
               <ts e="T1025" id="Seg_6116" n="e" s="T1024">Man </ts>
               <ts e="T1026" id="Seg_6118" n="e" s="T1025">potdɨbon </ts>
               <ts e="T1027" id="Seg_6120" n="e" s="T1026">tebnan </ts>
               <ts e="T1028" id="Seg_6122" n="e" s="T1027">jezan. </ts>
               <ts e="T1029" id="Seg_6124" n="e" s="T1028">Datau </ts>
               <ts e="T1030" id="Seg_6126" n="e" s="T1029">sodʼigan </ts>
               <ts e="T1031" id="Seg_6128" n="e" s="T1030">warkaː. </ts>
               <ts e="T1032" id="Seg_6130" n="e" s="T1031">Pajat </ts>
               <ts e="T1033" id="Seg_6132" n="e" s="T1032">sodʼiga, </ts>
               <ts e="T1034" id="Seg_6134" n="e" s="T1033">mazɨm </ts>
               <ts e="T1035" id="Seg_6136" n="e" s="T1034">čajlaze </ts>
               <ts e="T1036" id="Seg_6138" n="e" s="T1035">zɨm </ts>
               <ts e="T1037" id="Seg_6140" n="e" s="T1036">ärčɨs. </ts>
               <ts e="T1038" id="Seg_6142" n="e" s="T1037">Mekga </ts>
               <ts e="T1039" id="Seg_6144" n="e" s="T1038">tʼärɨn: </ts>
               <ts e="T1040" id="Seg_6146" n="e" s="T1039">“Mäguntu </ts>
               <ts e="T1041" id="Seg_6148" n="e" s="T1040">paldʼikoq.” </ts>
               <ts e="T1042" id="Seg_6150" n="e" s="T1041">Man </ts>
               <ts e="T1043" id="Seg_6152" n="e" s="T1042">teblanä </ts>
               <ts e="T1044" id="Seg_6154" n="e" s="T1043">qɨdan </ts>
               <ts e="T1045" id="Seg_6156" n="e" s="T1044">uduruqwan. </ts>
               <ts e="T1046" id="Seg_6158" n="e" s="T1045">Tabon </ts>
               <ts e="T1047" id="Seg_6160" n="e" s="T1046">jedot </ts>
               <ts e="T1048" id="Seg_6162" n="e" s="T1047">qwaǯan. </ts>
               <ts e="T1049" id="Seg_6164" n="e" s="T1048">Teblanä </ts>
               <ts e="T1050" id="Seg_6166" n="e" s="T1049">aj </ts>
               <ts e="T1051" id="Seg_6168" n="e" s="T1050">serkeǯan. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_6169" s="T1">PVD_1964_UnfaithfulWifeAndRobbers_flk.001 (001.001)</ta>
            <ta e="T6" id="Seg_6170" s="T3">PVD_1964_UnfaithfulWifeAndRobbers_flk.002 (001.002)</ta>
            <ta e="T9" id="Seg_6171" s="T6">PVD_1964_UnfaithfulWifeAndRobbers_flk.003 (001.003)</ta>
            <ta e="T12" id="Seg_6172" s="T9">PVD_1964_UnfaithfulWifeAndRobbers_flk.004 (001.004)</ta>
            <ta e="T19" id="Seg_6173" s="T12">PVD_1964_UnfaithfulWifeAndRobbers_flk.005 (001.005)</ta>
            <ta e="T30" id="Seg_6174" s="T19">PVD_1964_UnfaithfulWifeAndRobbers_flk.006 (001.006)</ta>
            <ta e="T35" id="Seg_6175" s="T30">PVD_1964_UnfaithfulWifeAndRobbers_flk.007 (001.007)</ta>
            <ta e="T42" id="Seg_6176" s="T35">PVD_1964_UnfaithfulWifeAndRobbers_flk.008 (001.008)</ta>
            <ta e="T48" id="Seg_6177" s="T42">PVD_1964_UnfaithfulWifeAndRobbers_flk.009 (001.009)</ta>
            <ta e="T52" id="Seg_6178" s="T48">PVD_1964_UnfaithfulWifeAndRobbers_flk.010 (001.010)</ta>
            <ta e="T61" id="Seg_6179" s="T52">PVD_1964_UnfaithfulWifeAndRobbers_flk.011 (001.011)</ta>
            <ta e="T67" id="Seg_6180" s="T61">PVD_1964_UnfaithfulWifeAndRobbers_flk.012 (001.012)</ta>
            <ta e="T72" id="Seg_6181" s="T67">PVD_1964_UnfaithfulWifeAndRobbers_flk.013 (001.013)</ta>
            <ta e="T74" id="Seg_6182" s="T72">PVD_1964_UnfaithfulWifeAndRobbers_flk.014 (001.014)</ta>
            <ta e="T80" id="Seg_6183" s="T74">PVD_1964_UnfaithfulWifeAndRobbers_flk.015 (001.015)</ta>
            <ta e="T83" id="Seg_6184" s="T80">PVD_1964_UnfaithfulWifeAndRobbers_flk.016 (001.016)</ta>
            <ta e="T87" id="Seg_6185" s="T83">PVD_1964_UnfaithfulWifeAndRobbers_flk.017 (001.017)</ta>
            <ta e="T90" id="Seg_6186" s="T87">PVD_1964_UnfaithfulWifeAndRobbers_flk.018 (001.018)</ta>
            <ta e="T96" id="Seg_6187" s="T90">PVD_1964_UnfaithfulWifeAndRobbers_flk.019 (001.019)</ta>
            <ta e="T101" id="Seg_6188" s="T96">PVD_1964_UnfaithfulWifeAndRobbers_flk.020 (001.020)</ta>
            <ta e="T106" id="Seg_6189" s="T101">PVD_1964_UnfaithfulWifeAndRobbers_flk.021 (001.021)</ta>
            <ta e="T112" id="Seg_6190" s="T106">PVD_1964_UnfaithfulWifeAndRobbers_flk.022 (001.022)</ta>
            <ta e="T115" id="Seg_6191" s="T112">PVD_1964_UnfaithfulWifeAndRobbers_flk.023 (001.023)</ta>
            <ta e="T123" id="Seg_6192" s="T115">PVD_1964_UnfaithfulWifeAndRobbers_flk.024 (001.024)</ta>
            <ta e="T126" id="Seg_6193" s="T123">PVD_1964_UnfaithfulWifeAndRobbers_flk.025 (001.025)</ta>
            <ta e="T131" id="Seg_6194" s="T126">PVD_1964_UnfaithfulWifeAndRobbers_flk.026 (001.026)</ta>
            <ta e="T137" id="Seg_6195" s="T131">PVD_1964_UnfaithfulWifeAndRobbers_flk.027 (001.027)</ta>
            <ta e="T140" id="Seg_6196" s="T137">PVD_1964_UnfaithfulWifeAndRobbers_flk.028 (001.028)</ta>
            <ta e="T142" id="Seg_6197" s="T140">PVD_1964_UnfaithfulWifeAndRobbers_flk.029 (001.029)</ta>
            <ta e="T149" id="Seg_6198" s="T142">PVD_1964_UnfaithfulWifeAndRobbers_flk.030 (001.030)</ta>
            <ta e="T162" id="Seg_6199" s="T149">PVD_1964_UnfaithfulWifeAndRobbers_flk.031 (001.031)</ta>
            <ta e="T172" id="Seg_6200" s="T162">PVD_1964_UnfaithfulWifeAndRobbers_flk.032 (001.032)</ta>
            <ta e="T177" id="Seg_6201" s="T172">PVD_1964_UnfaithfulWifeAndRobbers_flk.033 (001.033)</ta>
            <ta e="T180" id="Seg_6202" s="T177">PVD_1964_UnfaithfulWifeAndRobbers_flk.034 (001.034)</ta>
            <ta e="T183" id="Seg_6203" s="T180">PVD_1964_UnfaithfulWifeAndRobbers_flk.035 (001.035)</ta>
            <ta e="T189" id="Seg_6204" s="T183">PVD_1964_UnfaithfulWifeAndRobbers_flk.036 (001.036)</ta>
            <ta e="T194" id="Seg_6205" s="T189">PVD_1964_UnfaithfulWifeAndRobbers_flk.037 (001.037)</ta>
            <ta e="T201" id="Seg_6206" s="T194">PVD_1964_UnfaithfulWifeAndRobbers_flk.038 (001.038)</ta>
            <ta e="T205" id="Seg_6207" s="T201">PVD_1964_UnfaithfulWifeAndRobbers_flk.039 (001.039)</ta>
            <ta e="T215" id="Seg_6208" s="T205">PVD_1964_UnfaithfulWifeAndRobbers_flk.040 (001.040)</ta>
            <ta e="T218" id="Seg_6209" s="T215">PVD_1964_UnfaithfulWifeAndRobbers_flk.041 (001.041)</ta>
            <ta e="T222" id="Seg_6210" s="T218">PVD_1964_UnfaithfulWifeAndRobbers_flk.042 (001.042)</ta>
            <ta e="T225" id="Seg_6211" s="T222">PVD_1964_UnfaithfulWifeAndRobbers_flk.043 (001.043)</ta>
            <ta e="T231" id="Seg_6212" s="T225">PVD_1964_UnfaithfulWifeAndRobbers_flk.044 (001.044)</ta>
            <ta e="T239" id="Seg_6213" s="T231">PVD_1964_UnfaithfulWifeAndRobbers_flk.045 (001.045)</ta>
            <ta e="T251" id="Seg_6214" s="T239">PVD_1964_UnfaithfulWifeAndRobbers_flk.046 (001.046)</ta>
            <ta e="T253" id="Seg_6215" s="T251">PVD_1964_UnfaithfulWifeAndRobbers_flk.047 (001.047)</ta>
            <ta e="T256" id="Seg_6216" s="T253">PVD_1964_UnfaithfulWifeAndRobbers_flk.048 (001.048)</ta>
            <ta e="T257" id="Seg_6217" s="T256">PVD_1964_UnfaithfulWifeAndRobbers_flk.049 (001.049)</ta>
            <ta e="T261" id="Seg_6218" s="T257">PVD_1964_UnfaithfulWifeAndRobbers_flk.050 (001.050)</ta>
            <ta e="T264" id="Seg_6219" s="T261">PVD_1964_UnfaithfulWifeAndRobbers_flk.051 (001.051)</ta>
            <ta e="T267" id="Seg_6220" s="T264">PVD_1964_UnfaithfulWifeAndRobbers_flk.052 (001.052)</ta>
            <ta e="T270" id="Seg_6221" s="T267">PVD_1964_UnfaithfulWifeAndRobbers_flk.053 (001.053)</ta>
            <ta e="T273" id="Seg_6222" s="T270">PVD_1964_UnfaithfulWifeAndRobbers_flk.054 (001.054)</ta>
            <ta e="T278" id="Seg_6223" s="T273">PVD_1964_UnfaithfulWifeAndRobbers_flk.055 (001.055)</ta>
            <ta e="T282" id="Seg_6224" s="T278">PVD_1964_UnfaithfulWifeAndRobbers_flk.056 (001.056)</ta>
            <ta e="T288" id="Seg_6225" s="T282">PVD_1964_UnfaithfulWifeAndRobbers_flk.057 (001.057)</ta>
            <ta e="T293" id="Seg_6226" s="T288">PVD_1964_UnfaithfulWifeAndRobbers_flk.058 (001.058)</ta>
            <ta e="T303" id="Seg_6227" s="T293">PVD_1964_UnfaithfulWifeAndRobbers_flk.059 (001.059)</ta>
            <ta e="T307" id="Seg_6228" s="T303">PVD_1964_UnfaithfulWifeAndRobbers_flk.060 (001.060)</ta>
            <ta e="T310" id="Seg_6229" s="T307">PVD_1964_UnfaithfulWifeAndRobbers_flk.061 (001.061)</ta>
            <ta e="T318" id="Seg_6230" s="T310">PVD_1964_UnfaithfulWifeAndRobbers_flk.062 (001.062)</ta>
            <ta e="T326" id="Seg_6231" s="T318">PVD_1964_UnfaithfulWifeAndRobbers_flk.063 (001.063)</ta>
            <ta e="T331" id="Seg_6232" s="T326">PVD_1964_UnfaithfulWifeAndRobbers_flk.064 (001.064)</ta>
            <ta e="T341" id="Seg_6233" s="T331">PVD_1964_UnfaithfulWifeAndRobbers_flk.065 (001.065)</ta>
            <ta e="T346" id="Seg_6234" s="T341">PVD_1964_UnfaithfulWifeAndRobbers_flk.066 (001.066)</ta>
            <ta e="T348" id="Seg_6235" s="T346">PVD_1964_UnfaithfulWifeAndRobbers_flk.067 (001.067)</ta>
            <ta e="T350" id="Seg_6236" s="T348">PVD_1964_UnfaithfulWifeAndRobbers_flk.068 (001.068)</ta>
            <ta e="T355" id="Seg_6237" s="T350">PVD_1964_UnfaithfulWifeAndRobbers_flk.069 (001.069)</ta>
            <ta e="T361" id="Seg_6238" s="T355">PVD_1964_UnfaithfulWifeAndRobbers_flk.070 (001.070)</ta>
            <ta e="T366" id="Seg_6239" s="T361">PVD_1964_UnfaithfulWifeAndRobbers_flk.071 (001.071)</ta>
            <ta e="T373" id="Seg_6240" s="T366">PVD_1964_UnfaithfulWifeAndRobbers_flk.072 (001.072)</ta>
            <ta e="T377" id="Seg_6241" s="T373">PVD_1964_UnfaithfulWifeAndRobbers_flk.073 (001.073)</ta>
            <ta e="T383" id="Seg_6242" s="T377">PVD_1964_UnfaithfulWifeAndRobbers_flk.074 (001.074)</ta>
            <ta e="T386" id="Seg_6243" s="T383">PVD_1964_UnfaithfulWifeAndRobbers_flk.075 (001.075)</ta>
            <ta e="T393" id="Seg_6244" s="T386">PVD_1964_UnfaithfulWifeAndRobbers_flk.076 (001.076)</ta>
            <ta e="T403" id="Seg_6245" s="T393">PVD_1964_UnfaithfulWifeAndRobbers_flk.077 (001.077)</ta>
            <ta e="T409" id="Seg_6246" s="T403">PVD_1964_UnfaithfulWifeAndRobbers_flk.078 (001.078)</ta>
            <ta e="T412" id="Seg_6247" s="T409">PVD_1964_UnfaithfulWifeAndRobbers_flk.079 (001.079)</ta>
            <ta e="T414" id="Seg_6248" s="T412">PVD_1964_UnfaithfulWifeAndRobbers_flk.080 (001.080)</ta>
            <ta e="T420" id="Seg_6249" s="T414">PVD_1964_UnfaithfulWifeAndRobbers_flk.081 (001.081)</ta>
            <ta e="T424" id="Seg_6250" s="T420">PVD_1964_UnfaithfulWifeAndRobbers_flk.082 (001.082)</ta>
            <ta e="T426" id="Seg_6251" s="T424">PVD_1964_UnfaithfulWifeAndRobbers_flk.083 (001.083)</ta>
            <ta e="T433" id="Seg_6252" s="T426">PVD_1964_UnfaithfulWifeAndRobbers_flk.084 (001.084)</ta>
            <ta e="T436" id="Seg_6253" s="T433">PVD_1964_UnfaithfulWifeAndRobbers_flk.085 (001.085)</ta>
            <ta e="T443" id="Seg_6254" s="T436">PVD_1964_UnfaithfulWifeAndRobbers_flk.086 (001.086)</ta>
            <ta e="T445" id="Seg_6255" s="T443">PVD_1964_UnfaithfulWifeAndRobbers_flk.087 (001.087)</ta>
            <ta e="T448" id="Seg_6256" s="T445">PVD_1964_UnfaithfulWifeAndRobbers_flk.088 (001.088)</ta>
            <ta e="T451" id="Seg_6257" s="T448">PVD_1964_UnfaithfulWifeAndRobbers_flk.089 (001.089)</ta>
            <ta e="T452" id="Seg_6258" s="T451">PVD_1964_UnfaithfulWifeAndRobbers_flk.090 (001.090)</ta>
            <ta e="T462" id="Seg_6259" s="T452">PVD_1964_UnfaithfulWifeAndRobbers_flk.091 (001.091)</ta>
            <ta e="T470" id="Seg_6260" s="T462">PVD_1964_UnfaithfulWifeAndRobbers_flk.092 (001.092)</ta>
            <ta e="T476" id="Seg_6261" s="T470">PVD_1964_UnfaithfulWifeAndRobbers_flk.093 (001.093)</ta>
            <ta e="T482" id="Seg_6262" s="T476">PVD_1964_UnfaithfulWifeAndRobbers_flk.094 (001.094)</ta>
            <ta e="T487" id="Seg_6263" s="T482">PVD_1964_UnfaithfulWifeAndRobbers_flk.095 (001.095)</ta>
            <ta e="T489" id="Seg_6264" s="T487">PVD_1964_UnfaithfulWifeAndRobbers_flk.096 (001.096)</ta>
            <ta e="T491" id="Seg_6265" s="T489">PVD_1964_UnfaithfulWifeAndRobbers_flk.097 (001.097)</ta>
            <ta e="T493" id="Seg_6266" s="T491">PVD_1964_UnfaithfulWifeAndRobbers_flk.098 (001.098)</ta>
            <ta e="T499" id="Seg_6267" s="T493">PVD_1964_UnfaithfulWifeAndRobbers_flk.099 (001.099)</ta>
            <ta e="T502" id="Seg_6268" s="T499">PVD_1964_UnfaithfulWifeAndRobbers_flk.100 (001.100)</ta>
            <ta e="T504" id="Seg_6269" s="T502">PVD_1964_UnfaithfulWifeAndRobbers_flk.101 (001.101)</ta>
            <ta e="T508" id="Seg_6270" s="T504">PVD_1964_UnfaithfulWifeAndRobbers_flk.102 (001.102)</ta>
            <ta e="T520" id="Seg_6271" s="T508">PVD_1964_UnfaithfulWifeAndRobbers_flk.103 (001.103)</ta>
            <ta e="T528" id="Seg_6272" s="T520">PVD_1964_UnfaithfulWifeAndRobbers_flk.104 (001.104)</ta>
            <ta e="T531" id="Seg_6273" s="T528">PVD_1964_UnfaithfulWifeAndRobbers_flk.105 (001.105)</ta>
            <ta e="T534" id="Seg_6274" s="T531">PVD_1964_UnfaithfulWifeAndRobbers_flk.106 (001.106)</ta>
            <ta e="T538" id="Seg_6275" s="T534">PVD_1964_UnfaithfulWifeAndRobbers_flk.107 (001.107)</ta>
            <ta e="T541" id="Seg_6276" s="T538">PVD_1964_UnfaithfulWifeAndRobbers_flk.108 (001.108)</ta>
            <ta e="T546" id="Seg_6277" s="T541">PVD_1964_UnfaithfulWifeAndRobbers_flk.109 (001.109)</ta>
            <ta e="T548" id="Seg_6278" s="T546">PVD_1964_UnfaithfulWifeAndRobbers_flk.110 (001.110)</ta>
            <ta e="T550" id="Seg_6279" s="T548">PVD_1964_UnfaithfulWifeAndRobbers_flk.111 (001.111)</ta>
            <ta e="T553" id="Seg_6280" s="T550">PVD_1964_UnfaithfulWifeAndRobbers_flk.112 (001.112)</ta>
            <ta e="T559" id="Seg_6281" s="T553">PVD_1964_UnfaithfulWifeAndRobbers_flk.113 (001.113)</ta>
            <ta e="T566" id="Seg_6282" s="T559">PVD_1964_UnfaithfulWifeAndRobbers_flk.114 (001.114)</ta>
            <ta e="T568" id="Seg_6283" s="T566">PVD_1964_UnfaithfulWifeAndRobbers_flk.115 (001.115)</ta>
            <ta e="T570" id="Seg_6284" s="T568">PVD_1964_UnfaithfulWifeAndRobbers_flk.116 (001.116)</ta>
            <ta e="T576" id="Seg_6285" s="T570">PVD_1964_UnfaithfulWifeAndRobbers_flk.117 (001.117)</ta>
            <ta e="T579" id="Seg_6286" s="T576">PVD_1964_UnfaithfulWifeAndRobbers_flk.118 (001.118)</ta>
            <ta e="T591" id="Seg_6287" s="T579">PVD_1964_UnfaithfulWifeAndRobbers_flk.119 (001.119)</ta>
            <ta e="T599" id="Seg_6288" s="T591">PVD_1964_UnfaithfulWifeAndRobbers_flk.120 (001.120)</ta>
            <ta e="T606" id="Seg_6289" s="T599">PVD_1964_UnfaithfulWifeAndRobbers_flk.121 (001.121)</ta>
            <ta e="T608" id="Seg_6290" s="T606">PVD_1964_UnfaithfulWifeAndRobbers_flk.122 (001.122)</ta>
            <ta e="T613" id="Seg_6291" s="T608">PVD_1964_UnfaithfulWifeAndRobbers_flk.123 (001.123)</ta>
            <ta e="T619" id="Seg_6292" s="T613">PVD_1964_UnfaithfulWifeAndRobbers_flk.124 (001.124)</ta>
            <ta e="T622" id="Seg_6293" s="T619">PVD_1964_UnfaithfulWifeAndRobbers_flk.125 (001.125)</ta>
            <ta e="T630" id="Seg_6294" s="T622">PVD_1964_UnfaithfulWifeAndRobbers_flk.126 (001.126)</ta>
            <ta e="T637" id="Seg_6295" s="T630">PVD_1964_UnfaithfulWifeAndRobbers_flk.127 (001.127)</ta>
            <ta e="T640" id="Seg_6296" s="T637">PVD_1964_UnfaithfulWifeAndRobbers_flk.128 (001.128)</ta>
            <ta e="T646" id="Seg_6297" s="T640">PVD_1964_UnfaithfulWifeAndRobbers_flk.129 (001.129)</ta>
            <ta e="T652" id="Seg_6298" s="T646">PVD_1964_UnfaithfulWifeAndRobbers_flk.130 (001.130)</ta>
            <ta e="T656" id="Seg_6299" s="T652">PVD_1964_UnfaithfulWifeAndRobbers_flk.131 (001.131)</ta>
            <ta e="T659" id="Seg_6300" s="T656">PVD_1964_UnfaithfulWifeAndRobbers_flk.132 (001.132)</ta>
            <ta e="T664" id="Seg_6301" s="T659">PVD_1964_UnfaithfulWifeAndRobbers_flk.133 (001.133)</ta>
            <ta e="T666" id="Seg_6302" s="T664">PVD_1964_UnfaithfulWifeAndRobbers_flk.134 (001.134)</ta>
            <ta e="T668" id="Seg_6303" s="T666">PVD_1964_UnfaithfulWifeAndRobbers_flk.135 (001.135)</ta>
            <ta e="T675" id="Seg_6304" s="T668">PVD_1964_UnfaithfulWifeAndRobbers_flk.136 (001.136)</ta>
            <ta e="T680" id="Seg_6305" s="T675">PVD_1964_UnfaithfulWifeAndRobbers_flk.137 (001.137)</ta>
            <ta e="T685" id="Seg_6306" s="T680">PVD_1964_UnfaithfulWifeAndRobbers_flk.138 (001.138)</ta>
            <ta e="T687" id="Seg_6307" s="T685">PVD_1964_UnfaithfulWifeAndRobbers_flk.139 (001.139)</ta>
            <ta e="T690" id="Seg_6308" s="T687">PVD_1964_UnfaithfulWifeAndRobbers_flk.140 (001.140)</ta>
            <ta e="T695" id="Seg_6309" s="T690">PVD_1964_UnfaithfulWifeAndRobbers_flk.141 (001.141)</ta>
            <ta e="T699" id="Seg_6310" s="T695">PVD_1964_UnfaithfulWifeAndRobbers_flk.142 (001.142)</ta>
            <ta e="T705" id="Seg_6311" s="T699">PVD_1964_UnfaithfulWifeAndRobbers_flk.143 (001.143)</ta>
            <ta e="T707" id="Seg_6312" s="T705">PVD_1964_UnfaithfulWifeAndRobbers_flk.144 (001.144)</ta>
            <ta e="T709" id="Seg_6313" s="T707">PVD_1964_UnfaithfulWifeAndRobbers_flk.145 (001.145)</ta>
            <ta e="T714" id="Seg_6314" s="T709">PVD_1964_UnfaithfulWifeAndRobbers_flk.146 (001.146)</ta>
            <ta e="T720" id="Seg_6315" s="T714">PVD_1964_UnfaithfulWifeAndRobbers_flk.147 (001.147)</ta>
            <ta e="T725" id="Seg_6316" s="T720">PVD_1964_UnfaithfulWifeAndRobbers_flk.148 (001.148)</ta>
            <ta e="T729" id="Seg_6317" s="T725">PVD_1964_UnfaithfulWifeAndRobbers_flk.149 (001.149)</ta>
            <ta e="T738" id="Seg_6318" s="T729">PVD_1964_UnfaithfulWifeAndRobbers_flk.150 (001.150)</ta>
            <ta e="T742" id="Seg_6319" s="T738">PVD_1964_UnfaithfulWifeAndRobbers_flk.151 (001.151)</ta>
            <ta e="T745" id="Seg_6320" s="T742">PVD_1964_UnfaithfulWifeAndRobbers_flk.152 (001.152)</ta>
            <ta e="T748" id="Seg_6321" s="T745">PVD_1964_UnfaithfulWifeAndRobbers_flk.153 (001.153)</ta>
            <ta e="T754" id="Seg_6322" s="T748">PVD_1964_UnfaithfulWifeAndRobbers_flk.154 (001.154)</ta>
            <ta e="T762" id="Seg_6323" s="T754">PVD_1964_UnfaithfulWifeAndRobbers_flk.155 (001.155)</ta>
            <ta e="T766" id="Seg_6324" s="T762">PVD_1964_UnfaithfulWifeAndRobbers_flk.156 (001.156)</ta>
            <ta e="T773" id="Seg_6325" s="T766">PVD_1964_UnfaithfulWifeAndRobbers_flk.157 (001.157)</ta>
            <ta e="T779" id="Seg_6326" s="T773">PVD_1964_UnfaithfulWifeAndRobbers_flk.158 (001.158)</ta>
            <ta e="T784" id="Seg_6327" s="T779">PVD_1964_UnfaithfulWifeAndRobbers_flk.159 (001.159)</ta>
            <ta e="T792" id="Seg_6328" s="T784">PVD_1964_UnfaithfulWifeAndRobbers_flk.160 (001.160)</ta>
            <ta e="T796" id="Seg_6329" s="T792">PVD_1964_UnfaithfulWifeAndRobbers_flk.161 (001.161)</ta>
            <ta e="T802" id="Seg_6330" s="T796">PVD_1964_UnfaithfulWifeAndRobbers_flk.162 (001.162)</ta>
            <ta e="T810" id="Seg_6331" s="T802">PVD_1964_UnfaithfulWifeAndRobbers_flk.163 (001.163)</ta>
            <ta e="T812" id="Seg_6332" s="T810">PVD_1964_UnfaithfulWifeAndRobbers_flk.164 (001.164)</ta>
            <ta e="T817" id="Seg_6333" s="T812">PVD_1964_UnfaithfulWifeAndRobbers_flk.165 (001.165)</ta>
            <ta e="T822" id="Seg_6334" s="T817">PVD_1964_UnfaithfulWifeAndRobbers_flk.166 (001.166)</ta>
            <ta e="T828" id="Seg_6335" s="T822">PVD_1964_UnfaithfulWifeAndRobbers_flk.167 (001.167)</ta>
            <ta e="T834" id="Seg_6336" s="T828">PVD_1964_UnfaithfulWifeAndRobbers_flk.168 (001.168)</ta>
            <ta e="T836" id="Seg_6337" s="T834">PVD_1964_UnfaithfulWifeAndRobbers_flk.169 (001.169)</ta>
            <ta e="T839" id="Seg_6338" s="T836">PVD_1964_UnfaithfulWifeAndRobbers_flk.170 (001.170)</ta>
            <ta e="T843" id="Seg_6339" s="T839">PVD_1964_UnfaithfulWifeAndRobbers_flk.171 (001.171)</ta>
            <ta e="T850" id="Seg_6340" s="T843">PVD_1964_UnfaithfulWifeAndRobbers_flk.172 (001.172)</ta>
            <ta e="T853" id="Seg_6341" s="T850">PVD_1964_UnfaithfulWifeAndRobbers_flk.173 (001.173)</ta>
            <ta e="T858" id="Seg_6342" s="T853">PVD_1964_UnfaithfulWifeAndRobbers_flk.174 (001.174)</ta>
            <ta e="T861" id="Seg_6343" s="T858">PVD_1964_UnfaithfulWifeAndRobbers_flk.175 (001.175)</ta>
            <ta e="T865" id="Seg_6344" s="T861">PVD_1964_UnfaithfulWifeAndRobbers_flk.176 (001.176)</ta>
            <ta e="T868" id="Seg_6345" s="T865">PVD_1964_UnfaithfulWifeAndRobbers_flk.177 (001.177)</ta>
            <ta e="T879" id="Seg_6346" s="T868">PVD_1964_UnfaithfulWifeAndRobbers_flk.178 (001.178)</ta>
            <ta e="T884" id="Seg_6347" s="T879">PVD_1964_UnfaithfulWifeAndRobbers_flk.179 (001.179)</ta>
            <ta e="T888" id="Seg_6348" s="T884">PVD_1964_UnfaithfulWifeAndRobbers_flk.180 (001.180)</ta>
            <ta e="T893" id="Seg_6349" s="T888">PVD_1964_UnfaithfulWifeAndRobbers_flk.181 (001.181)</ta>
            <ta e="T896" id="Seg_6350" s="T893">PVD_1964_UnfaithfulWifeAndRobbers_flk.182 (001.182)</ta>
            <ta e="T897" id="Seg_6351" s="T896">PVD_1964_UnfaithfulWifeAndRobbers_flk.183 (001.183)</ta>
            <ta e="T900" id="Seg_6352" s="T897">PVD_1964_UnfaithfulWifeAndRobbers_flk.184 (001.184)</ta>
            <ta e="T904" id="Seg_6353" s="T900">PVD_1964_UnfaithfulWifeAndRobbers_flk.185 (001.185)</ta>
            <ta e="T908" id="Seg_6354" s="T904">PVD_1964_UnfaithfulWifeAndRobbers_flk.186 (001.186)</ta>
            <ta e="T912" id="Seg_6355" s="T908">PVD_1964_UnfaithfulWifeAndRobbers_flk.187 (001.187)</ta>
            <ta e="T914" id="Seg_6356" s="T912">PVD_1964_UnfaithfulWifeAndRobbers_flk.188 (001.188)</ta>
            <ta e="T918" id="Seg_6357" s="T914">PVD_1964_UnfaithfulWifeAndRobbers_flk.189 (001.189)</ta>
            <ta e="T922" id="Seg_6358" s="T918">PVD_1964_UnfaithfulWifeAndRobbers_flk.190 (001.190)</ta>
            <ta e="T926" id="Seg_6359" s="T922">PVD_1964_UnfaithfulWifeAndRobbers_flk.191 (001.191)</ta>
            <ta e="T933" id="Seg_6360" s="T926">PVD_1964_UnfaithfulWifeAndRobbers_flk.192 (001.192)</ta>
            <ta e="T937" id="Seg_6361" s="T933">PVD_1964_UnfaithfulWifeAndRobbers_flk.193 (001.193)</ta>
            <ta e="T939" id="Seg_6362" s="T937">PVD_1964_UnfaithfulWifeAndRobbers_flk.194 (001.194)</ta>
            <ta e="T943" id="Seg_6363" s="T939">PVD_1964_UnfaithfulWifeAndRobbers_flk.195 (001.195)</ta>
            <ta e="T948" id="Seg_6364" s="T943">PVD_1964_UnfaithfulWifeAndRobbers_flk.196 (001.196)</ta>
            <ta e="T950" id="Seg_6365" s="T948">PVD_1964_UnfaithfulWifeAndRobbers_flk.197 (001.197)</ta>
            <ta e="T953" id="Seg_6366" s="T950">PVD_1964_UnfaithfulWifeAndRobbers_flk.198 (001.198)</ta>
            <ta e="T956" id="Seg_6367" s="T953">PVD_1964_UnfaithfulWifeAndRobbers_flk.199 (001.199)</ta>
            <ta e="T958" id="Seg_6368" s="T956">PVD_1964_UnfaithfulWifeAndRobbers_flk.200 (001.200)</ta>
            <ta e="T961" id="Seg_6369" s="T958">PVD_1964_UnfaithfulWifeAndRobbers_flk.201 (001.201)</ta>
            <ta e="T966" id="Seg_6370" s="T961">PVD_1964_UnfaithfulWifeAndRobbers_flk.202 (001.202)</ta>
            <ta e="T972" id="Seg_6371" s="T966">PVD_1964_UnfaithfulWifeAndRobbers_flk.203 (001.203)</ta>
            <ta e="T974" id="Seg_6372" s="T972">PVD_1964_UnfaithfulWifeAndRobbers_flk.204 (001.204)</ta>
            <ta e="T977" id="Seg_6373" s="T974">PVD_1964_UnfaithfulWifeAndRobbers_flk.205 (001.205)</ta>
            <ta e="T980" id="Seg_6374" s="T977">PVD_1964_UnfaithfulWifeAndRobbers_flk.206 (001.206)</ta>
            <ta e="T987" id="Seg_6375" s="T980">PVD_1964_UnfaithfulWifeAndRobbers_flk.207 (001.207)</ta>
            <ta e="T991" id="Seg_6376" s="T987">PVD_1964_UnfaithfulWifeAndRobbers_flk.208 (001.208)</ta>
            <ta e="T994" id="Seg_6377" s="T991">PVD_1964_UnfaithfulWifeAndRobbers_flk.209 (001.209)</ta>
            <ta e="T999" id="Seg_6378" s="T994">PVD_1964_UnfaithfulWifeAndRobbers_flk.210 (001.210)</ta>
            <ta e="T1003" id="Seg_6379" s="T999">PVD_1964_UnfaithfulWifeAndRobbers_flk.211 (001.211)</ta>
            <ta e="T1005" id="Seg_6380" s="T1003">PVD_1964_UnfaithfulWifeAndRobbers_flk.212 (001.212)</ta>
            <ta e="T1009" id="Seg_6381" s="T1005">PVD_1964_UnfaithfulWifeAndRobbers_flk.213 (001.213)</ta>
            <ta e="T1011" id="Seg_6382" s="T1009">PVD_1964_UnfaithfulWifeAndRobbers_flk.214 (001.214)</ta>
            <ta e="T1014" id="Seg_6383" s="T1011">PVD_1964_UnfaithfulWifeAndRobbers_flk.215 (001.215)</ta>
            <ta e="T1018" id="Seg_6384" s="T1014">PVD_1964_UnfaithfulWifeAndRobbers_flk.216 (001.216)</ta>
            <ta e="T1024" id="Seg_6385" s="T1018">PVD_1964_UnfaithfulWifeAndRobbers_flk.217 (001.217)</ta>
            <ta e="T1028" id="Seg_6386" s="T1024">PVD_1964_UnfaithfulWifeAndRobbers_flk.218 (001.218)</ta>
            <ta e="T1031" id="Seg_6387" s="T1028">PVD_1964_UnfaithfulWifeAndRobbers_flk.219 (001.219)</ta>
            <ta e="T1037" id="Seg_6388" s="T1031">PVD_1964_UnfaithfulWifeAndRobbers_flk.220 (001.220)</ta>
            <ta e="T1041" id="Seg_6389" s="T1037">PVD_1964_UnfaithfulWifeAndRobbers_flk.221 (001.221)</ta>
            <ta e="T1045" id="Seg_6390" s="T1041">PVD_1964_UnfaithfulWifeAndRobbers_flk.222 (001.222)</ta>
            <ta e="T1048" id="Seg_6391" s="T1045">PVD_1964_UnfaithfulWifeAndRobbers_flk.223 (001.223)</ta>
            <ta e="T1051" id="Seg_6392" s="T1048">PVD_1964_UnfaithfulWifeAndRobbers_flk.224 (001.224)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_6393" s="T1">хрес′тjан е′ра.</ta>
            <ta e="T6" id="Seg_6394" s="T3">′тʼелымнын хрʼес′тʼӓн е(ӓ)′ра.</ta>
            <ta e="T9" id="Seg_6395" s="T6">тӓб′нан ′на̄гур ӣт.</ta>
            <ta e="T12" id="Seg_6396" s="T9">′ӣлат вес нӓ′дӓлаттъ.</ta>
            <ta e="T19" id="Seg_6397" s="T12">ок′кыр ӣт ′kыдан ′сурулле ′палʼдӱкун, ′wарɣъ ӣт.</ta>
            <ta e="T30" id="Seg_6398" s="T19">а сът (′съдъ) ӣт скатʼина′лазе палʼдӱkwаɣъ, ′сырла wаръ′даттъ, кӱтды′ла и ко′нɛрла.</ta>
            <ta e="T35" id="Seg_6399" s="T30">и ′пашнӓ ′wардаттъ, ап′сот ′сеjаку′даттъ.</ta>
            <ta e="T42" id="Seg_6400" s="T35">а wарɣы ӣт ′кы̄дан ма′дʼӧт паl′дʼӓн (паlдʼи′кун).</ta>
            <ta e="T48" id="Seg_6401" s="T42">теб′нан ′не̄(ӓ)т ′тʼе̨лымда и ӣга ′тʼелымда.</ta>
            <ta e="T52" id="Seg_6402" s="T48">те(ӓ)быс′таɣъ вар′ɣын а̄зу′аɣъ (а̄зу′аk).</ta>
            <ta e="T61" id="Seg_6403" s="T52">а тӓп на′тʼен ма′дʼӧɣын ′ӣбыɣай ма̄т ′омдыlджы‵мытдыт, ′ӣбыɣай ма̄т.</ta>
            <ta e="T67" id="Seg_6404" s="T61">′ӓотдӓнӓ ′ӓзӓтдӓнӓ тʼӓ′рын: ном′се ма′зым пас′лавикъ.</ta>
            <ta e="T72" id="Seg_6405" s="T67">′ӓу(w)тдъ ′номым ӣут и пас′лави‵ныт.</ta>
            <ta e="T74" id="Seg_6406" s="T72">ман kwат′джан.</ta>
            <ta e="T80" id="Seg_6407" s="T74">′на̄гур а′газлаф(в)тъ о′ккыр ′ма̄тkын асс е′лʼетджуту.</ta>
            <ta e="T83" id="Seg_6408" s="T80">ма′нан тӓ′пер сʼӓм′jау̹.</ta>
            <ta e="T87" id="Seg_6409" s="T83">са′озниктъ вес тӓ′домымдъ ′туɣунныт.</ta>
            <ta e="T90" id="Seg_6410" s="T87">и на ′ӱ̄быт‵даттъ.</ta>
            <ta e="T96" id="Seg_6411" s="T90">ӓс′ты ӓу′ды и ′тӱрлʼе на ′ɣалыт′даттъ.</ta>
            <ta e="T101" id="Seg_6412" s="T96">и тӓб′ла и ′на kwат′даттъ.</ta>
            <ta e="T106" id="Seg_6413" s="T101">но и ′тӱlе на ′медыт′даттъ.</ta>
            <ta e="T112" id="Seg_6414" s="T106">а теб′нан на′тʼен ′ӣбыɣай ма̄т ′омдъlджъ‵мыд̂ыт.</ta>
            <ta e="T115" id="Seg_6415" s="T112">вес ′туɣун‵наттъ на′тʼет.</ta>
            <ta e="T123" id="Seg_6416" s="T115">но и ӓ′рат вес ′мелʼета̄дърат, ′kоптъ ′метдыт.</ta>
            <ta e="T126" id="Seg_6417" s="T123">kар′дʼен ма′дʼӧт kwат′джан.</ta>
            <ta e="T131" id="Seg_6418" s="T126">а тан ӱд′но на kwа′ннаш.</ta>
            <ta e="T137" id="Seg_6419" s="T131">′тобоɣӓт kа′jам ′струшка па′палан иг(ик) а′зӱ.</ta>
            <ta e="T140" id="Seg_6420" s="T137">а ′то ′а̄wан ′jетджан.</ta>
            <ta e="T142" id="Seg_6421" s="T140">но ′секг̂аттъ.</ta>
            <ta e="T149" id="Seg_6422" s="T142">kа′рʼемыɣын ва′зын, аур′нын и на kwат′да ма′дʼӧтдъ.</ta>
            <ta e="T162" id="Seg_6423" s="T149">а нӓй′ɣум тʼӓ′рын: kай′но ′мекга ӓ′рау̹ тʼӓрын, што ′струшка ′ӱттъ папалан ик(г) а′зъ̊.</ta>
            <ta e="T172" id="Seg_6424" s="T162">ман си′час сът вед′рам ′струшкала′зе наби′неджау̹, наби′нӓджау̹, kwалʼ′лʼе ′ӱттъ kам′нӓтджау̹.</ta>
            <ta e="T177" id="Seg_6425" s="T172">и ′на ′kwанны′д̂ыт и ′kамджыт.</ta>
            <ta e="T180" id="Seg_6426" s="T177">нык′га и ′манны‵па̄т.</ta>
            <ta e="T183" id="Seg_6427" s="T180">а на′тʼен кӧу′нын.</ta>
            <ta e="T189" id="Seg_6428" s="T183">тӓп ′ӱдым ′соɣунныт и ма̄тты ′тӱа.</ta>
            <ta e="T194" id="Seg_6429" s="T189">а тӓп кра′сива ′нӓйɣум jес.</ta>
            <ta e="T201" id="Seg_6430" s="T194">kwалʼлʼе сӱтʼди′нӓй ма̄ттъ ′омдын и ′сӱдырлʼе ′ӱбыран.</ta>
            <ta e="T205" id="Seg_6431" s="T201">а сът kы′банʼажа вар′каɣъ.</ta>
            <ta e="T215" id="Seg_6432" s="T205">раз′бойнигла ′тшажатдъ ′атды(ъ)зʼе и тʼӓ′раттъ: kай ′струшкала (′кӧудаттъ) ′кӱузе ′тша̄жыдаттъ.</ta>
            <ta e="T218" id="Seg_6433" s="T215">′kън′б̂арынʼӓ ′праи′бет!</ta>
            <ta e="T222" id="Seg_6434" s="T218">теб′ла манны′паттъ kай′да ват′ты.</ta>
            <ta e="T225" id="Seg_6435" s="T222">тӓб′ла ′ваттыга ′котдаттъ.</ta>
            <ta e="T231" id="Seg_6436" s="T225">у′гон ме ′тау ′ваттым jасс ′kоджыргу′зау(w)тъ.</ta>
            <ta e="T239" id="Seg_6437" s="T231">на ватто′г̂атдъ (на ва′тотдъ, на ва′тотдъ) и ′ӯду′раттъ.</ta>
            <ta e="T251" id="Seg_6438" s="T239">атта′ман тʼӓ′рын: тӓ ′а̄мдалтъ тыт′дын, а ман kу′роллан манныбы′лʼеу(w) kай ′илатда тыт′дын.</ta>
            <ta e="T253" id="Seg_6439" s="T251">на kу′роlджа.</ta>
            <ta e="T256" id="Seg_6440" s="T253">′тӱа, ′сернын ма̄ттъ.</ta>
            <ta e="T257" id="Seg_6441" s="T256">тʼо′лом.</ta>
            <ta e="T261" id="Seg_6442" s="T257">нӓйɣум тʼӓ′рын: тʼо′лом, тʼо′лом.</ta>
            <ta e="T264" id="Seg_6443" s="T261">стул ′тшетшелʼе ме′жалджыт.</ta>
            <ta e="T267" id="Seg_6444" s="T264">тʼӓрын ом′дак тык′г̂а.</ta>
            <ta e="T270" id="Seg_6445" s="T267">а тӓп ом′дын.</ta>
            <ta e="T273" id="Seg_6446" s="T270">ку′лупбылʼе на ′ӱбырыт′даɣ.</ta>
            <ta e="T278" id="Seg_6447" s="T273">ата′ман тʼӓ′рын: тан ′мекга ты‵бытды′лʼет.</ta>
            <ta e="T282" id="Seg_6448" s="T278">но маннан ӓ′рау̹ ор′се.</ta>
            <ta e="T288" id="Seg_6449" s="T282">тӓп тас′ты kwа′тшыт и ′мазым kwа′тшыт.</ta>
            <ta e="T293" id="Seg_6450" s="T288">а ман ′текга ′kудуго ′менныджан.</ta>
            <ta e="T303" id="Seg_6451" s="T293">(оп′тъ jенныш и сынкова проволока, тӓп ′тау ′кудыгом ас лак′тшеджит).</ta>
            <ta e="T307" id="Seg_6452" s="T303">тӓп на тӱнныш ′ӱдомыɣын.</ta>
            <ta e="T310" id="Seg_6453" s="T307">тан тʼӓ′рак: kо′зыртшы′лай.</ta>
            <ta e="T318" id="Seg_6454" s="T310">таб′нӓ тʼӓ′рак: kут ту′ракын kа′лʼеджын, уд′ламдъ моɣу′нӓ ′саруку.</ta>
            <ta e="T326" id="Seg_6455" s="T318">а тан тӓ′бым ту′ракын ′kwӓзʼет, уд′лам дъ моɣунӓ ′са̄рʼет.</ta>
            <ta e="T331" id="Seg_6456" s="T326">тӓп ′тау ′kудугом ас лак′тшеджин.</ta>
            <ta e="T341" id="Seg_6457" s="T331">тӓп jежлʼи асс лаk′тшыт, а тан ′понӓ тшат′джак, ма′зым лак′голʼг(k)ъ.</ta>
            <ta e="T346" id="Seg_6458" s="T341">ман ′тӱтджан, ′тӓбым и kwат′тшау̹.</ta>
            <ta e="T348" id="Seg_6459" s="T346">та′зе и′лʼетджай.</ta>
            <ta e="T350" id="Seg_6460" s="T348">ӓ′рат ′тӱа.</ta>
            <ta e="T355" id="Seg_6461" s="T350">тӓп тӓ′бым апс′тыт, тʼӓ′рын: ′kозыртшылай.</ta>
            <ta e="T361" id="Seg_6462" s="T355">ӓ′рат тʼӓ′рын: ман ′нунытʼип‵бан, ′kотдугу ′надъ.</ta>
            <ta e="T366" id="Seg_6463" s="T361">ну да′вай, kозыртшы′лай асс ′kутдын.</ta>
            <ta e="T373" id="Seg_6464" s="T366">а то ′онӓн ′амдан и ′скушнан ′мекга jен.</ta>
            <ta e="T377" id="Seg_6465" s="T373">ну и ′омдаɣъ ′kозыртшылʼе.</ta>
            <ta e="T383" id="Seg_6466" s="T377">kут ту′раkын kа′лʼеджын, у′дламдъ са′реlджугу моɣунӓ.</ta>
            <ta e="T386" id="Seg_6467" s="T383">′ӓрат ту′раkын ′kалын.</ta>
            <ta e="T393" id="Seg_6468" s="T386">уд′ламдъ ′надъ са′реlджугу, у′домдъ надъ ′саругу моɣу′нӓ.</ta>
            <ta e="T403" id="Seg_6469" s="T393">а ′jежлʼе ман ′kалынӓн ту′раkын, тан бъ ′мекга уд′лау̹ з̂а′реlджынӓл.</ta>
            <ta e="T409" id="Seg_6470" s="T403">а тӓ′пер ман ′тʼекга у′домдъ са′ретджау.</ta>
            <ta e="T412" id="Seg_6471" s="T409">ӓ′рат тʼӓ′рын: са′рӓт.</ta>
            <ta e="T414" id="Seg_6472" s="T412">тӓп ′са̄рӓт.</ta>
            <ta e="T420" id="Seg_6473" s="T414">ӓ′рат как туру′гуlджыт, ′kуды‵гола вес ′лаkтше′латтъ.</ta>
            <ta e="T424" id="Seg_6474" s="T420">но и ′kотдыгу kу′жан′наɣы.</ta>
            <ta e="T426" id="Seg_6475" s="T424">kа′рʼемыɣын ва′зын.</ta>
            <ta e="T433" id="Seg_6476" s="T426">′нӓйɣум вес ′потkыныт, вадʼи′лам kай′лам, ӓ′рамдъ апс′тыт.</ta>
            <ta e="T436" id="Seg_6477" s="T433">ӓ′рат ма′дʼот kwа′ннын.</ta>
            <ta e="T443" id="Seg_6478" s="T436">′нӓйɣум ′понӓ сапы′син и на ′kа̄ролʼдʼӓ: тʼӱк′кы!</ta>
            <ta e="T445" id="Seg_6479" s="T443">ӓ′рау kwан′б̂а!</ta>
            <ta e="T448" id="Seg_6480" s="T445">ата′ман на ′тӱтда.</ta>
            <ta e="T451" id="Seg_6481" s="T448">но kай, kо′зыртше‵залʼе?</ta>
            <ta e="T452" id="Seg_6482" s="T451">kозыртше′зай.</ta>
            <ta e="T462" id="Seg_6483" s="T452">ман ′тʼекга тʼӓры′зан, тӓп ор′се jен, на ′kуды′голамвес лак′тшелджыт.</ta>
            <ta e="T470" id="Seg_6484" s="T462">а там′дʼел ман ′тʼекга ме′джау оп′ты ′kудыго ′менны‵джан.</ta>
            <ta e="T476" id="Seg_6485" s="T470">′ӱдъмы‵ɣын на ′тӱнныш, ай тʼӓ′рак: ‵kозыртшы′лай.</ta>
            <ta e="T482" id="Seg_6486" s="T476">kут ту′ракын kа′лʼеджын, уд′ламдъ моɣу′нӓ ′са̄руку.</ta>
            <ta e="T487" id="Seg_6487" s="T482">′нӓйɣум тӓ′бым ′апстыпба̄т, ара′казʼе ′ӓртшибат.</ta>
            <ta e="T489" id="Seg_6488" s="T487">а′раkыла ′ӱттыды.</ta>
            <ta e="T491" id="Seg_6489" s="T489">′тӓбеɣум kwаннын.</ta>
            <ta e="T493" id="Seg_6490" s="T491">′ӓрат ′тӱа.</ta>
            <ta e="T499" id="Seg_6491" s="T493">сурумлам ′ко̄цʼен kwат′пат и ′пекkы ′kwатпадыт.</ta>
            <ta e="T502" id="Seg_6492" s="T499">тӓп ′тӓбым апс′тыт.</ta>
            <ta e="T504" id="Seg_6493" s="T502">тʼӓ′рын: ′kозыртшы′лай.</ta>
            <ta e="T508" id="Seg_6494" s="T504">ӓ′рат тʼӓ′рын: ман ′нӯныдʼипб̂а̄н.</ta>
            <ta e="T520" id="Seg_6495" s="T508">па′jат тʼӓ′рын: kозыртшы′лай тʼем уго′вором, ′kудъ kут ту′ракын kа′лʼеджын, у′дымдъ моɣу′нӓ ′са̄ругу.</ta>
            <ta e="T528" id="Seg_6496" s="T520">′jешлʼи ман kа′лан д̂у′раkын, ман у′доу ′са̄раk (′сареджал).</ta>
            <ta e="T531" id="Seg_6497" s="T528">тӓбыс′таɣъ ом′даɣъ kозыртшилʼе.</ta>
            <ta e="T534" id="Seg_6498" s="T531">ӓ′рат ту′раk(к)ын ′kалын.</ta>
            <ta e="T538" id="Seg_6499" s="T534">ну, тӓ′пер у′домдъ ′са̄реджау.</ta>
            <ta e="T541" id="Seg_6500" s="T538">тӓп са̄рыт ′удымдъ.</ta>
            <ta e="T546" id="Seg_6501" s="T541">тӓп как тур′гуlджытда, ′kудыго ′лаkт′шыдин.</ta>
            <ta e="T548" id="Seg_6502" s="T546">kу′жаннаɣъ ′kотду(ы)ɣы.</ta>
            <ta e="T550" id="Seg_6503" s="T548">kа′рʼемыɣын ва′заɣъ.</ta>
            <ta e="T553" id="Seg_6504" s="T550">′нӓйɣум ӓ′рамдъ ′апстыт.</ta>
            <ta e="T559" id="Seg_6505" s="T553">ӓ′рат аур′нын и ма′дʼот на kwат′да.</ta>
            <ta e="T566" id="Seg_6506" s="T559">нӓи′ɣум ′понӓ ′тша̄джын и на ′kа̄ронʼен: тӱк′кы!</ta>
            <ta e="T568" id="Seg_6507" s="T566">ӓ′рау kwан′ба!</ta>
            <ta e="T570" id="Seg_6508" s="T568">раз′бойник ′тӱва.</ta>
            <ta e="T576" id="Seg_6509" s="T570">нӓиɣум тʼӓрын: ′kуды′голам ӓ′рау̹ вес лак′тше(ӓ)лджыт.</ta>
            <ta e="T579" id="Seg_6510" s="T576">тӓп ор′се jен.</ta>
            <ta e="T591" id="Seg_6511" s="T579">а ман там′дʼел ′текга мед′жау ′кудыго оп′ты и ′сынковаjа проволока о′ккыр′мыт ′тамкылба.</ta>
            <ta e="T599" id="Seg_6512" s="T591">′ӱ̄дымыɣын на ′тӱнныш, тан ай тʼӓ′рак ′kозыртшуɣу, kозыртшилай.</ta>
            <ta e="T606" id="Seg_6513" s="T599">kут ту′ракын ка′лʼеджын, то у′дымдъ моɣу′нӓ ′са̄ругу.</ta>
            <ta e="T608" id="Seg_6514" s="T606">ӓ′ра ′тӱа.</ta>
            <ta e="T613" id="Seg_6515" s="T608">′тӓп ′тӓбым апс′тыт, тʼӓ′рын: ′kозыртшылай.</ta>
            <ta e="T619" id="Seg_6516" s="T613">′ӓрат тʼӓ′рын: ман ′нуныдʼзʼипан, надъ ′kотды(у)ɣу.</ta>
            <ta e="T622" id="Seg_6517" s="T619">но ′kозыртшылай ′kозыртшылай.</ta>
            <ta e="T630" id="Seg_6518" s="T622">хотʼ ′оккырын kут ту′ракын ка′лʼеджын, у′дымдъ моɣу′нӓ ′сааруку.</ta>
            <ta e="T637" id="Seg_6519" s="T630">′jежлʼи ман ка′лʼеджан, ман у′доу ′са̄рʼет ′моɣунӓ.</ta>
            <ta e="T640" id="Seg_6520" s="T637">и о̄м′даɣъ ′kозыртшы′лʼе.</ta>
            <ta e="T646" id="Seg_6521" s="T640">ӓ′рамдъ ту′рагывлʼе ′kwɛдʼит, у′дымдъ моɣу′нӓ ′са̄рыт.</ta>
            <ta e="T652" id="Seg_6522" s="T646">тӓп как тур′гуlджытда, ′kудъко′lам ′jасс лаk′тшыт.</ta>
            <ta e="T656" id="Seg_6523" s="T652">па′jӓтдӓнӓ тʼӓ′рын: тʼи′кет у′доу̹.</ta>
            <ta e="T659" id="Seg_6524" s="T656">па′jӓт тʼӓ′рын: лак′тшет.</ta>
            <ta e="T664" id="Seg_6525" s="T659">тӓп ′ай тур′гуlджын, ′jасс лак′тшыт.</ta>
            <ta e="T666" id="Seg_6526" s="T664">тʼӓ′рын: тʼи′кет.</ta>
            <ta e="T668" id="Seg_6527" s="T666">асс тʼи′кеджау̹.</ta>
            <ta e="T675" id="Seg_6528" s="T668">понӓ ′саппыселʼе ′kwаннын и на лак′голʼдʼӓ: тӱ′ккы!</ta>
            <ta e="T680" id="Seg_6529" s="T675">и на разбойник на ′тӱтда.</ta>
            <ta e="T685" id="Seg_6530" s="T680">тʼӓ′рын: ӓ′рау̹ ′kудогом асс лак′тшыт.</ta>
            <ta e="T687" id="Seg_6531" s="T685">′серлʼе ′kwаллай.</ta>
            <ta e="T690" id="Seg_6532" s="T687">и ′маттъ ′сернаɣъ (′сернаk).</ta>
            <ta e="T695" id="Seg_6533" s="T690">разбойник тʼӓ′рын: kар′дʼен kа′рʼемы‵ɣын kwат′тшай.</ta>
            <ta e="T699" id="Seg_6534" s="T695">туl′джӓт вʼӓд′ран тʼӣр са̄k.</ta>
            <ta e="T705" id="Seg_6535" s="T699">тӓбым ′са̄ɣын ′паротдъ ′пулъ ′сейлатды′зе (′са̄ɣот) омдыл′джеджай.</ta>
            <ta e="T707" id="Seg_6536" s="T705">kа′рʼемъттъ ′а̄мдъ.</ta>
            <ta e="T709" id="Seg_6537" s="T707">kа′рʼемыɣын kwат′тшай.</ta>
            <ta e="T714" id="Seg_6538" s="T709">нӓй′ɣум ват′доɣы(у)т ′тӱлʼе на′сӧ̄зытдът.</ta>
            <ta e="T720" id="Seg_6539" s="T714">тан тʼӓрыкузат(тъ) што ман ор′се ′jеwан.</ta>
            <ta e="T725" id="Seg_6540" s="T720">а kай′но kу′дыгом асс лаk′тше̨джал?</ta>
            <ta e="T729" id="Seg_6541" s="T725">та′пӓр kа′рʼемыɣын тас′ты kwат′тшай.</ta>
            <ta e="T738" id="Seg_6542" s="T729">нӓй′ɣум kу′роннын, ′са̄k ′туlджытдыт вӓд′ран тӣр и ′саlджы‵бот ′kамджыт.</ta>
            <ta e="T742" id="Seg_6543" s="T738">и тӓбым ′пуlу′зеин ′омдыlджыт.</ta>
            <ta e="T745" id="Seg_6544" s="T742">тʼӓ′рын: ′а̄мдаk kа′рʼемытдъ!</ta>
            <ta e="T748" id="Seg_6545" s="T745">kа′рʼемыɣын та′сты kwат′тшай.</ta>
            <ta e="T754" id="Seg_6546" s="T748">ом′даɣъ аур′лʼе раз′бойниксе а′ракай ′ӓрлʼе ′jӱбыраk.</ta>
            <ta e="T762" id="Seg_6547" s="T754">нӓй′ɣум тӱуwа, ӓ′ратдъ ′кӧтдъ ӓ′рамдъ ват′доот на ′сӧзыт‵дът.</ta>
            <ta e="T766" id="Seg_6548" s="T762">kа′рʼемыɣын ме тас′ты kwат′тшай.</ta>
            <ta e="T773" id="Seg_6549" s="T766">′отдъ на kwат′да раз′бойникым ′ка̄wалгут, ′нӣделлʼе ′та̄дырыт.</ta>
            <ta e="T779" id="Seg_6550" s="T773">и ′kотдуɣу kу′жаннах(ɣ) и на kот′доlджаɣы.</ta>
            <ta e="T784" id="Seg_6551" s="T779">(ӓ′рат) тӓп ′вазын ′иннӓ, ′паlдʼукун.</ta>
            <ta e="T792" id="Seg_6552" s="T784">а кы̄бан ′нӓгат ′ӣɣ(Г)ат ′тӓбыс′та̄ɣъ kотды′заɣъ па′латин па′роɣын.</ta>
            <ta e="T796" id="Seg_6553" s="T792">′нӓгат ′кӱзыгу ′илʼлʼе тʼӱ′тʼӧун.</ta>
            <ta e="T802" id="Seg_6554" s="T796">тӓп ′нӓгатда′нӓ тʼӓ′рын: ман ′удлау ′тʼикылʼел.</ta>
            <ta e="T810" id="Seg_6555" s="T802">а ′нӓгат тʼӓ′рын: тан тʼӓ′руку‵зат што ор′се ′jеwан.</ta>
            <ta e="T812" id="Seg_6556" s="T810">лак′тшелджет ′kудыгом.</ta>
            <ta e="T817" id="Seg_6557" s="T812">па′латин ′барот тша′джын и ′kотда.</ta>
            <ta e="T822" id="Seg_6558" s="T817">′ӣгат па′латиɣъ(ы)н′до илʼ′лʼе тʼӱ′тʼӧун ′кӱзыгу.</ta>
            <ta e="T828" id="Seg_6559" s="T822">тӓп ′ӣгатдӓнӓ тʼӓ′рын: ман у′доу̹ ′тʼӣкелʼел.</ta>
            <ta e="T834" id="Seg_6560" s="T828">тӓп тӱwа ′ӓзытдӓнӓ и ′тʼикълʼе jӱбърат.</ta>
            <ta e="T836" id="Seg_6561" s="T834">jасс тʼи′кыт.</ta>
            <ta e="T839" id="Seg_6562" s="T836">′мекга jасс тʼикы′ɣӯ.</ta>
            <ta e="T843" id="Seg_6563" s="T839">ӓс′ты тʼӓ′рын: пат′пʼилка′д̂аткъ таткъ.</ta>
            <ta e="T850" id="Seg_6564" s="T843">′тӓп ′танныт и ′куды′голам сы′лʼелʼлʼе на ′ӱбърътдыт.</ta>
            <ta e="T853" id="Seg_6565" s="T850">сы′лыт, ′kудъ′гола ′варсы‵ваннат.</ta>
            <ta e="T858" id="Seg_6566" s="T853">тӓп ′ӣгатдӓнӓ тʼа′рын: kwалʼ′лʼе kот′даk.</ta>
            <ta e="T861" id="Seg_6567" s="T858">kа′рʼемыɣын ′ӣгъ ‵ларып′ба̄к.</ta>
            <ta e="T865" id="Seg_6568" s="T861">ме та′зʼе о′нӓй и′лʼетджай.</ta>
            <ta e="T868" id="Seg_6569" s="T865">ном ′тʼелымлʼе ′jӱбырын.</ta>
            <ta e="T879" id="Seg_6570" s="T868">тӓп куды′голам у′доɣътдъ пе′нныт (как ′будто ′са̄рыпба̄т) и ом′дын ′пуlу′зеин ′парот.</ta>
            <ta e="T884" id="Seg_6571" s="T879">па′jӓт ва′зын, ′тӱлʼе ват′доын ′сӧ̄зыт.</ta>
            <ta e="T888" id="Seg_6572" s="T884">раз′бойниг′нӓ тʼӓ′рын: ну kwа′ллай.</ta>
            <ta e="T893" id="Seg_6573" s="T888">а раз′бойник тʼӓ′рын: аур′лʼе kwа′ллайзе.</ta>
            <ta e="T896" id="Seg_6574" s="T893">теп вадʼи′лам мӱ′зурɣын′нӓт.</ta>
            <ta e="T897" id="Seg_6575" s="T896">аур′наɣъ.</ta>
            <ta e="T900" id="Seg_6576" s="T897">ну тӓ′пӓр kwаl′лай.</ta>
            <ta e="T904" id="Seg_6577" s="T900">′ма̄тkын kwат′тшай алʼи ′понӓн?</ta>
            <ta e="T908" id="Seg_6578" s="T904">разбойник тʼӓ′рын: ′понӓн kwат′тшай.</ta>
            <ta e="T912" id="Seg_6579" s="T908">ну ва′зак, тша̄жык ′понӓ.</ta>
            <ta e="T914" id="Seg_6580" s="T912">тас′ты kwат′тшай.</ta>
            <ta e="T918" id="Seg_6581" s="T914">kай куды′голам jасс лак′тше(ӓ)лджал.</ta>
            <ta e="T922" id="Seg_6582" s="T918">тан тʼӓруку′заттъ, ор′се ′jеwан.</ta>
            <ta e="T926" id="Seg_6583" s="T922">нӓу′ɣум как ват′доутдъ ′сӧзытдыт.</ta>
            <ta e="T933" id="Seg_6584" s="T926">тӓп как ин′нӓ ва′зʼезитда, как разбойник(г)ам ′kӓттыдыт.</ta>
            <ta e="T937" id="Seg_6585" s="T933">раз′бойник ′kотӓ тшӓ′тшӓдʼзʼен (тшӓ′тшӓдин).</ta>
            <ta e="T939" id="Seg_6586" s="T937">и ′kwанныт.</ta>
            <ta e="T943" id="Seg_6587" s="T939">а па′jӓт ′соjоɣотдъ ыдъ′дʼен.</ta>
            <ta e="T948" id="Seg_6588" s="T943">тʼӓ′рын: ман нылʼдʼин асс ′ме̄kwе′джау̹.</ta>
            <ta e="T950" id="Seg_6589" s="T948">тоб′ламдъ мӱ′зуlджу′кеджау.</ta>
            <ta e="T953" id="Seg_6590" s="T950">на ӧ′дым ӱт′кеджау.</ta>
            <ta e="T956" id="Seg_6591" s="T953">е′рат как ′kӓ̄тӓджыт.</ta>
            <ta e="T958" id="Seg_6592" s="T956">и ′kwанныт.</ta>
            <ta e="T961" id="Seg_6593" s="T958">kы′банʼажала ′тӱрлʼе ӱбърат.</ta>
            <ta e="T966" id="Seg_6594" s="T961">′нӓгамдъ как ′kӓтытдыт и ′kwанныт.</ta>
            <ta e="T972" id="Seg_6595" s="T966">и ′ӣгатдӓнӓ тʼӓ′рын: ме та′зе и′лʼетджай.</ta>
            <ta e="T974" id="Seg_6596" s="T972">′игъ ′тӱрак.</ta>
            <ta e="T977" id="Seg_6597" s="T974">kwа′ннын тʼӱй ба′ɣаттылʼе.</ta>
            <ta e="T980" id="Seg_6598" s="T977">тӱй kы′лым паkкы′ныт.</ta>
            <ta e="T987" id="Seg_6599" s="T980">тӓб′лам на′тʼет ′kӓ̄лɣыныт (kӓнныт) и ′тʼӱзе ′таɣыныт.</ta>
            <ta e="T991" id="Seg_6600" s="T987">са′озникамдъ kа′рʼе ′ӱккулʼе kwат′дыт.</ta>
            <ta e="T994" id="Seg_6601" s="T991">тӓ′домы‵ламдъ вес ′туɣунныт.</ta>
            <ta e="T999" id="Seg_6602" s="T994">′ӣгатдызе ом′даɣъ и на kwат′да̄k.</ta>
            <ta e="T1003" id="Seg_6603" s="T999">′тʼӱлʼе ме̄′даɣъ ′отдъ ′jедоɣотдъ.</ta>
            <ta e="T1005" id="Seg_6604" s="T1003">kуlат ′а̄тдалбат.</ta>
            <ta e="T1009" id="Seg_6605" s="T1005">а ′паjал кут′тʼен jен?</ta>
            <ta e="T1011" id="Seg_6606" s="T1009">па′jау̹ ′kупба.</ta>
            <ta e="T1014" id="Seg_6607" s="T1011">вес ′тугунаттъ тӓ′домым.</ta>
            <ta e="T1018" id="Seg_6608" s="T1014">тӓп ′пирʼӓɣытдъ ма̄т ′таутдыт.</ta>
            <ta e="T1024" id="Seg_6609" s="T1018">и нӓ′дын и варкы′лʼе на ′ӱбырытда.</ta>
            <ta e="T1028" id="Seg_6610" s="T1024">ман ′потдыбон теб′нан ′jезан.</ta>
            <ta e="T1031" id="Seg_6611" s="T1028">д̂а ′тау ′содʼиган вар′ка̄.</ta>
            <ta e="T1037" id="Seg_6612" s="T1031">па′jат ′содʼига, ма′зым ′чайлазе зым ′ӓртшыс.</ta>
            <ta e="T1041" id="Seg_6613" s="T1037">′мекг̂а тʼӓ′рын: мӓ′гунту паlдʼи′коk.</ta>
            <ta e="T1045" id="Seg_6614" s="T1041">ман теб′ланӓ ′kыдан ′удуруkw̹ан.</ta>
            <ta e="T1048" id="Seg_6615" s="T1045">та′бон ′jедот kwа′джан.</ta>
            <ta e="T1051" id="Seg_6616" s="T1048">теб′ланӓ ай ′серкеджан.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T3" id="Seg_6617" s="T1">xrestjan era.</ta>
            <ta e="T6" id="Seg_6618" s="T3">tʼelɨmnɨn xrʼestʼän e(ä)ra.</ta>
            <ta e="T9" id="Seg_6619" s="T6">täbnan naːgur iːt.</ta>
            <ta e="T12" id="Seg_6620" s="T9">iːlat wes nädälattə.</ta>
            <ta e="T19" id="Seg_6621" s="T12">okkɨr iːt qɨdan surulle palʼdükun, warɣə iːt.</ta>
            <ta e="T30" id="Seg_6622" s="T19">a sət (sədə) iːt skatʼinalaze palʼdüqwaɣə, sɨrla warədattə, kütdɨla i konɛrla.</ta>
            <ta e="T35" id="Seg_6623" s="T30">i pašnä wardattə, apsot sejakudattə.</ta>
            <ta e="T42" id="Seg_6624" s="T35">a warɣɨ iːt kɨːdan madʼöt paldʼän (paldʼikun).</ta>
            <ta e="T48" id="Seg_6625" s="T42">tebnan neː(ä)t tʼelɨmda i iːga tʼelɨmda.</ta>
            <ta e="T52" id="Seg_6626" s="T48">te(ä)bɨstaɣə warɣɨn aːzuaɣə (aːzuaq).</ta>
            <ta e="T61" id="Seg_6627" s="T52">a täp natʼen madʼöɣɨn iːbɨɣaj maːt omdɨlǯɨmɨtdɨt, iːbɨɣaj maːt.</ta>
            <ta e="T67" id="Seg_6628" s="T61">äotdänä äzätdänä tʼärɨn: nomse mazɨm paslawikə.</ta>
            <ta e="T72" id="Seg_6629" s="T67">äu(w)tdə nomɨm iːut i paslawinɨt.</ta>
            <ta e="T74" id="Seg_6630" s="T72">man qwatǯan.</ta>
            <ta e="T80" id="Seg_6631" s="T74">naːgur agazlaf(w)tə okkɨr maːtqɨn ass elʼetǯutu.</ta>
            <ta e="T83" id="Seg_6632" s="T80">manan täper sʼämjau̹.</ta>
            <ta e="T87" id="Seg_6633" s="T83">saozniktə wes tädomɨmdə tuɣunnɨt.</ta>
            <ta e="T90" id="Seg_6634" s="T87">i na üːbɨtdattə.</ta>
            <ta e="T96" id="Seg_6635" s="T90">ästɨ äudɨ i türlʼe na ɣalɨtdattə.</ta>
            <ta e="T101" id="Seg_6636" s="T96">i täbla i na qwatdattə.</ta>
            <ta e="T106" id="Seg_6637" s="T101">no i tüle na medɨtdattə.</ta>
            <ta e="T112" id="Seg_6638" s="T106">a tebnan natʼen iːbɨɣaj maːt omdəlǯəmɨd̂ɨt.</ta>
            <ta e="T115" id="Seg_6639" s="T112">wes tuɣunnattə natʼet.</ta>
            <ta e="T123" id="Seg_6640" s="T115">no i ärat wes melʼetaːdərat, qoptə metdɨt.</ta>
            <ta e="T126" id="Seg_6641" s="T123">qardʼen madʼöt qwatǯan.</ta>
            <ta e="T131" id="Seg_6642" s="T126">a tan üdno na qwannaš.</ta>
            <ta e="T137" id="Seg_6643" s="T131">toboɣät qajam struška papalan ig(ik) azü.</ta>
            <ta e="T140" id="Seg_6644" s="T137">a to aːwan jetǯan.</ta>
            <ta e="T142" id="Seg_6645" s="T140">no sekĝattə.</ta>
            <ta e="T149" id="Seg_6646" s="T142">qarʼemɨɣɨn wazɨn, aurnɨn i na qwatda madʼötdə.</ta>
            <ta e="T162" id="Seg_6647" s="T149">a näjɣum tʼärɨn: qajno mekga ärau̹ tʼärɨn, što struška üttə papalan ik(g) azə.</ta>
            <ta e="T172" id="Seg_6648" s="T162">man sičas sət wedram struškalaze nabineǯau̹, nabinäǯau̹, qwalʼlʼe üttə qamnätǯau̹.</ta>
            <ta e="T177" id="Seg_6649" s="T172">i na qwannɨd̂ɨt i qamǯɨt.</ta>
            <ta e="T180" id="Seg_6650" s="T177">nɨkga i mannɨpaːt.</ta>
            <ta e="T183" id="Seg_6651" s="T180">a natʼen köunɨn.</ta>
            <ta e="T189" id="Seg_6652" s="T183">täp üdɨm soɣunnɨt i maːttɨ tüa.</ta>
            <ta e="T194" id="Seg_6653" s="T189">a täp krasiwa näjɣum jes.</ta>
            <ta e="T201" id="Seg_6654" s="T194">qwalʼlʼe sütʼdinäj maːttə omdɨn i südɨrlʼe übɨran.</ta>
            <ta e="T205" id="Seg_6655" s="T201">a sət qɨbanʼaʒa warkaɣə.</ta>
            <ta e="T215" id="Seg_6656" s="T205">razbojnigla tšaʒatdə atdɨ(ə)zʼe i tʼärattə: qaj struškala (köudattə) küuze tšaːʒɨdattə.</ta>
            <ta e="T218" id="Seg_6657" s="T215">qənb̂arɨnʼä praibet!</ta>
            <ta e="T222" id="Seg_6658" s="T218">tebla mannɨpattə qajda wattɨ.</ta>
            <ta e="T225" id="Seg_6659" s="T222">täbla wattɨga kotdattə.</ta>
            <ta e="T231" id="Seg_6660" s="T225">ugon me tau wattɨm jass qoǯɨrguzau(w)tə.</ta>
            <ta e="T239" id="Seg_6661" s="T231">na wattoĝatdə (na watotdə, na watotdə) i uːdurattə.</ta>
            <ta e="T251" id="Seg_6662" s="T239">attaman tʼärɨn: tä aːmdaltə tɨtdɨn, a man qurollan mannɨbɨlʼeu(w) qaj ilatda tɨtdɨn.</ta>
            <ta e="T253" id="Seg_6663" s="T251">na qurolǯa.</ta>
            <ta e="T256" id="Seg_6664" s="T253">tüa, sernɨn maːttə.</ta>
            <ta e="T257" id="Seg_6665" s="T256">tʼolom.</ta>
            <ta e="T261" id="Seg_6666" s="T257">näjɣum tʼärɨn: tʼolom, tʼolom.</ta>
            <ta e="T264" id="Seg_6667" s="T261">stul tšetšelʼe meʒalǯɨt.</ta>
            <ta e="T267" id="Seg_6668" s="T264">tʼärɨn omdak tɨkĝa.</ta>
            <ta e="T270" id="Seg_6669" s="T267">a täp omdɨn.</ta>
            <ta e="T273" id="Seg_6670" s="T270">kulupbɨlʼe na übɨrɨtdaɣ.</ta>
            <ta e="T278" id="Seg_6671" s="T273">ataman tʼärɨn: tan mekga tɨbɨtdɨlʼet.</ta>
            <ta e="T282" id="Seg_6672" s="T278">no mannan ärau̹ orse.</ta>
            <ta e="T288" id="Seg_6673" s="T282">täp tastɨ qwatšɨt i mazɨm qwatšɨt.</ta>
            <ta e="T293" id="Seg_6674" s="T288">a man tekga qudugo mennɨǯan.</ta>
            <ta e="T303" id="Seg_6675" s="T293">(optə jennɨš i sɨnkowa prowoloka, täp tau kudɨgom as laktšeǯit).</ta>
            <ta e="T307" id="Seg_6676" s="T303">täp na tünnɨš üdomɨɣɨn.</ta>
            <ta e="T310" id="Seg_6677" s="T307">tan tʼärak: qozɨrtšɨlaj.</ta>
            <ta e="T318" id="Seg_6678" s="T310">tabnä tʼärak: qut turakɨn qalʼeǯɨn, udlamdə moɣunä saruku.</ta>
            <ta e="T326" id="Seg_6679" s="T318">a tan täbɨm turakɨn qwäzʼet, udlam də moɣunä saːrʼet.</ta>
            <ta e="T331" id="Seg_6680" s="T326">täp tau qudugom as laktšeǯin.</ta>
            <ta e="T341" id="Seg_6681" s="T331">täp jeʒlʼi ass laqtšɨt, a tan ponä tšatǯak, mazɨm lakgolʼg(q)ə.</ta>
            <ta e="T346" id="Seg_6682" s="T341">man tütǯan, täbɨm i qwattšau̹.</ta>
            <ta e="T348" id="Seg_6683" s="T346">taze ilʼetǯaj.</ta>
            <ta e="T350" id="Seg_6684" s="T348">ärat tüa.</ta>
            <ta e="T355" id="Seg_6685" s="T350">täp täbɨm apstɨt, tʼärɨn: qozɨrtšɨlaj.</ta>
            <ta e="T361" id="Seg_6686" s="T355">ärat tʼärɨn: man nunɨtʼipban, qotdugu nadə.</ta>
            <ta e="T366" id="Seg_6687" s="T361">nu dawaj, qozɨrtšɨlaj ass qutdɨn.</ta>
            <ta e="T373" id="Seg_6688" s="T366">a to onän amdan i skušnan mekga jen.</ta>
            <ta e="T377" id="Seg_6689" s="T373">nu i omdaɣə qozɨrtšɨlʼe.</ta>
            <ta e="T383" id="Seg_6690" s="T377">qut turaqɨn qalʼeǯɨn, udlamdə sarelǯugu moɣunä.</ta>
            <ta e="T386" id="Seg_6691" s="T383">ärat turaqɨn qalɨn.</ta>
            <ta e="T393" id="Seg_6692" s="T386">udlamdə nadə sarelǯugu, udomdə nadə sarugu moɣunä.</ta>
            <ta e="T403" id="Seg_6693" s="T393">a jeʒlʼe man qalɨnän turaqɨn, tan bə mekga udlau̹ ẑarelǯɨnäl.</ta>
            <ta e="T409" id="Seg_6694" s="T403">a täper man tʼekga udomdə saretǯau.</ta>
            <ta e="T412" id="Seg_6695" s="T409">ärat tʼärɨn: sarät.</ta>
            <ta e="T414" id="Seg_6696" s="T412">täp saːrät.</ta>
            <ta e="T420" id="Seg_6697" s="T414">ärat kak turugulǯɨt, qudɨgola wes laqtšelattə.</ta>
            <ta e="T424" id="Seg_6698" s="T420">no i qotdɨgu quʒannaɣɨ.</ta>
            <ta e="T426" id="Seg_6699" s="T424">qarʼemɨɣɨn wazɨn.</ta>
            <ta e="T433" id="Seg_6700" s="T426">näjɣum wes potqɨnɨt, wadʼilam qajlam, äramdə apstɨt.</ta>
            <ta e="T436" id="Seg_6701" s="T433">ärat madʼot qwannɨn.</ta>
            <ta e="T443" id="Seg_6702" s="T436">näjɣum ponä sapɨsin i na qaːrolʼdʼä: tʼükkɨ!</ta>
            <ta e="T445" id="Seg_6703" s="T443">ärau qwanb̂a!</ta>
            <ta e="T448" id="Seg_6704" s="T445">ataman na tütda.</ta>
            <ta e="T451" id="Seg_6705" s="T448">no qaj, qozɨrtšezalʼe?</ta>
            <ta e="T452" id="Seg_6706" s="T451">qozɨrtšezaj.</ta>
            <ta e="T462" id="Seg_6707" s="T452">man tʼekga tʼärɨzan, täp orse jen, na qudɨgolamwes laktšelǯɨt.</ta>
            <ta e="T470" id="Seg_6708" s="T462">a tamdʼel man tʼekga meǯau optɨ qudɨgo mennɨǯan.</ta>
            <ta e="T476" id="Seg_6709" s="T470">üdəmɨɣɨn na tünnɨš, aj tʼärak: qozɨrtšɨlaj.</ta>
            <ta e="T482" id="Seg_6710" s="T476">qut turakɨn qalʼeǯɨn, udlamdə moɣunä saːruku.</ta>
            <ta e="T487" id="Seg_6711" s="T482">näjɣum täbɨm apstɨpbaːt, arakazʼe ärtšibat.</ta>
            <ta e="T489" id="Seg_6712" s="T487">araqɨla üttɨdɨ.</ta>
            <ta e="T491" id="Seg_6713" s="T489">täbeɣum qwannɨn.</ta>
            <ta e="T493" id="Seg_6714" s="T491">ärat tüa.</ta>
            <ta e="T499" id="Seg_6715" s="T493">surumlam koːcʼen qwatpat i pekqɨ qwatpadɨt.</ta>
            <ta e="T502" id="Seg_6716" s="T499">täp täbɨm apstɨt.</ta>
            <ta e="T504" id="Seg_6717" s="T502">tʼärɨn: qozɨrtšɨlaj.</ta>
            <ta e="T508" id="Seg_6718" s="T504">ärat tʼärɨn: man nuːnɨdʼipb̂aːn.</ta>
            <ta e="T520" id="Seg_6719" s="T508">pajat tʼärɨn: qozɨrtšɨlaj tʼem ugoworom, qudə qut turakɨn qalʼeǯɨn, udɨmdə moɣunä saːrugu.</ta>
            <ta e="T528" id="Seg_6720" s="T520">ješlʼi man qalan d̂uraqɨn, man udou saːraq (sareǯal).</ta>
            <ta e="T531" id="Seg_6721" s="T528">täbɨstaɣə omdaɣə qozɨrtšilʼe.</ta>
            <ta e="T534" id="Seg_6722" s="T531">ärat turaq(k)ɨn qalɨn.</ta>
            <ta e="T538" id="Seg_6723" s="T534">nu, täper udomdə saːreǯau.</ta>
            <ta e="T541" id="Seg_6724" s="T538">täp saːrɨt udɨmdə.</ta>
            <ta e="T546" id="Seg_6725" s="T541">täp kak turgulǯɨtda, qudɨgo laqtšɨdin.</ta>
            <ta e="T548" id="Seg_6726" s="T546">quʒannaɣə qotdu(ɨ)ɣɨ.</ta>
            <ta e="T550" id="Seg_6727" s="T548">qarʼemɨɣɨn wazaɣə.</ta>
            <ta e="T553" id="Seg_6728" s="T550">näjɣum äramdə apstɨt.</ta>
            <ta e="T559" id="Seg_6729" s="T553">ärat aurnɨn i madʼot na qwatda.</ta>
            <ta e="T566" id="Seg_6730" s="T559">näiɣum ponä tšaːǯɨn i na qaːronʼen: tükkɨ!</ta>
            <ta e="T568" id="Seg_6731" s="T566">ärau qwanba!</ta>
            <ta e="T570" id="Seg_6732" s="T568">razbojnik tüwa.</ta>
            <ta e="T576" id="Seg_6733" s="T570">näiɣum tʼärɨn: qudɨgolam ärau̹ wes laktše(ä)lǯɨt.</ta>
            <ta e="T579" id="Seg_6734" s="T576">täp orse jen.</ta>
            <ta e="T591" id="Seg_6735" s="T579">a man tamdʼel tekga meǯau kudɨgo optɨ i sɨnkowaja prowoloka okkɨrmɨt tamkɨlba.</ta>
            <ta e="T599" id="Seg_6736" s="T591">üːdɨmɨɣɨn na tünnɨš, tan aj tʼärak qozɨrtšuɣu, qozɨrtšilaj.</ta>
            <ta e="T606" id="Seg_6737" s="T599">qut turakɨn kalʼeǯɨn, to udɨmdə moɣunä saːrugu.</ta>
            <ta e="T608" id="Seg_6738" s="T606">ära tüa.</ta>
            <ta e="T613" id="Seg_6739" s="T608">täp täbɨm apstɨt, tʼärɨn: qozɨrtšɨlaj.</ta>
            <ta e="T619" id="Seg_6740" s="T613">ärat tʼärɨn: man nunɨdʼzʼipan, nadə qotdɨ(u)ɣu.</ta>
            <ta e="T622" id="Seg_6741" s="T619">no qozɨrtšɨlaj qozɨrtšɨlaj.</ta>
            <ta e="T630" id="Seg_6742" s="T622">xotʼ okkɨrɨn qut turakɨn kalʼeǯɨn, udɨmdə moɣunä saaruku.</ta>
            <ta e="T637" id="Seg_6743" s="T630">jeʒlʼi man kalʼeǯan, man udou saːrʼet moɣunä.</ta>
            <ta e="T640" id="Seg_6744" s="T637">i oːmdaɣə qozɨrtšɨlʼe.</ta>
            <ta e="T646" id="Seg_6745" s="T640">äramdə turagɨwlʼe qwɛdʼit, udɨmdə moɣunä saːrɨt.</ta>
            <ta e="T652" id="Seg_6746" s="T646">täp kak turgulǯɨtda, qudəkolam jass laqtšɨt.</ta>
            <ta e="T656" id="Seg_6747" s="T652">pajätdänä tʼärɨn: tʼiket udou̹.</ta>
            <ta e="T659" id="Seg_6748" s="T656">pajät tʼärɨn: laktšet.</ta>
            <ta e="T664" id="Seg_6749" s="T659">täp aj turgulǯɨn, jass laktšɨt.</ta>
            <ta e="T666" id="Seg_6750" s="T664">tʼärɨn: tʼiket.</ta>
            <ta e="T668" id="Seg_6751" s="T666">ass tʼikeǯau̹.</ta>
            <ta e="T675" id="Seg_6752" s="T668">ponä sappɨselʼe qwannɨn i na lakgolʼdʼä: tükkɨ!</ta>
            <ta e="T680" id="Seg_6753" s="T675">i na razbojnik na tütda.</ta>
            <ta e="T685" id="Seg_6754" s="T680">tʼärɨn: ärau̹ qudogom ass laktšɨt.</ta>
            <ta e="T687" id="Seg_6755" s="T685">serlʼe qwallaj.</ta>
            <ta e="T690" id="Seg_6756" s="T687">i mattə sernaɣə (sernaq).</ta>
            <ta e="T695" id="Seg_6757" s="T690">razbojnik tʼärɨn: qardʼen qarʼemɨɣɨn qwattšaj.</ta>
            <ta e="T699" id="Seg_6758" s="T695">tulǯät wʼädran tʼiːr saːq.</ta>
            <ta e="T705" id="Seg_6759" s="T699">täbɨm saːɣɨn parotdə pulə sejlatdɨze (saːɣot) omdɨlǯeǯaj.</ta>
            <ta e="T707" id="Seg_6760" s="T705">qarʼeməttə aːmdə.</ta>
            <ta e="T709" id="Seg_6761" s="T707">qarʼemɨɣɨn qwattšaj.</ta>
            <ta e="T714" id="Seg_6762" s="T709">näjɣum watdoɣɨ(u)t tülʼe nasöːzɨtdət.</ta>
            <ta e="T720" id="Seg_6763" s="T714">tan tʼärɨkuzat(tə) što man orse jewan.</ta>
            <ta e="T725" id="Seg_6764" s="T720">a qajno qudɨgom ass laqtšeǯal?</ta>
            <ta e="T729" id="Seg_6765" s="T725">tapär qarʼemɨɣɨn tastɨ qwattšaj.</ta>
            <ta e="T738" id="Seg_6766" s="T729">näjɣum quronnɨn, saːq tulǯɨtdɨt wädran tiːr i salǯɨbot qamǯɨt.</ta>
            <ta e="T742" id="Seg_6767" s="T738">i täbɨm puluzein omdɨlǯɨt.</ta>
            <ta e="T745" id="Seg_6768" s="T742">tʼärɨn: aːmdaq qarʼemɨtdə!</ta>
            <ta e="T748" id="Seg_6769" s="T745">qarʼemɨɣɨn tastɨ qwattšaj.</ta>
            <ta e="T754" id="Seg_6770" s="T748">omdaɣə aurlʼe razbojnikse arakaj ärlʼe jübɨraq.</ta>
            <ta e="T762" id="Seg_6771" s="T754">näjɣum tüuwa, äratdə kötdə äramdə watdoot na sözɨtdət.</ta>
            <ta e="T766" id="Seg_6772" s="T762">qarʼemɨɣɨn me tastɨ qwattšaj.</ta>
            <ta e="T773" id="Seg_6773" s="T766">otdə na qwatda razbojnikɨm kaːwalgut, niːdellʼe taːdɨrɨt.</ta>
            <ta e="T779" id="Seg_6774" s="T773">i qotduɣu quʒannax(ɣ) i na qotdolǯaɣɨ.</ta>
            <ta e="T784" id="Seg_6775" s="T779">(ärat) täp wazɨn innä, paldʼukun.</ta>
            <ta e="T792" id="Seg_6776" s="T784">a kɨːban nägat iːɣ(Г)at täbɨstaːɣə qotdɨzaɣə palatin paroɣɨn.</ta>
            <ta e="T796" id="Seg_6777" s="T792">nägat küzɨgu ilʼlʼe tʼütʼöun.</ta>
            <ta e="T802" id="Seg_6778" s="T796">täp nägatdanä tʼärɨn: man udlau tʼikɨlʼel.</ta>
            <ta e="T810" id="Seg_6779" s="T802">a nägat tʼärɨn: tan tʼärukuzat što orse jewan.</ta>
            <ta e="T812" id="Seg_6780" s="T810">laktšelǯet qudɨgom.</ta>
            <ta e="T817" id="Seg_6781" s="T812">palatin barot tšaǯɨn i qotda.</ta>
            <ta e="T822" id="Seg_6782" s="T817">iːgat palatiɣə(ɨ)ndo ilʼlʼe tʼütʼöun küzɨgu.</ta>
            <ta e="T828" id="Seg_6783" s="T822">täp iːgatdänä tʼärɨn: man udou̹ tʼiːkelʼel.</ta>
            <ta e="T834" id="Seg_6784" s="T828">täp tüwa äzɨtdänä i tʼikəlʼe jübərat.</ta>
            <ta e="T836" id="Seg_6785" s="T834">jass tʼikɨt.</ta>
            <ta e="T839" id="Seg_6786" s="T836">mekga jass tʼikɨɣuː.</ta>
            <ta e="T843" id="Seg_6787" s="T839">ästɨ tʼärɨn: patpʼilkad̂atkə tatkə.</ta>
            <ta e="T850" id="Seg_6788" s="T843">täp tannɨt i kudɨgolam sɨlʼelʼlʼe na übərətdɨt.</ta>
            <ta e="T853" id="Seg_6789" s="T850">sɨlɨt, qudəgola warsɨwannat.</ta>
            <ta e="T858" id="Seg_6790" s="T853">täp iːgatdänä tʼarɨn: qwalʼlʼe qotdaq.</ta>
            <ta e="T861" id="Seg_6791" s="T858">qarʼemɨɣɨn iːgə larɨpbaːk.</ta>
            <ta e="T865" id="Seg_6792" s="T861">me tazʼe onäj ilʼetǯaj.</ta>
            <ta e="T868" id="Seg_6793" s="T865">nom tʼelɨmlʼe jübɨrɨn.</ta>
            <ta e="T879" id="Seg_6794" s="T868">täp kudɨgolam udoɣətdə pennɨt (kak budto saːrɨpbaːt) i omdɨn puluzein parot.</ta>
            <ta e="T884" id="Seg_6795" s="T879">pajät wazɨn, tülʼe watdoɨn söːzɨt.</ta>
            <ta e="T888" id="Seg_6796" s="T884">razbojnignä tʼärɨn: nu qwallaj.</ta>
            <ta e="T893" id="Seg_6797" s="T888">a razbojnik tʼärɨn: aurlʼe qwallajze.</ta>
            <ta e="T896" id="Seg_6798" s="T893">tep wadʼilam müzurɣɨnnät.</ta>
            <ta e="T897" id="Seg_6799" s="T896">aurnaɣə.</ta>
            <ta e="T900" id="Seg_6800" s="T897">nu täpär qwallaj.</ta>
            <ta e="T904" id="Seg_6801" s="T900">maːtqɨn qwattšaj alʼi ponän?</ta>
            <ta e="T908" id="Seg_6802" s="T904">razbojnik tʼärɨn: ponän qwattšaj.</ta>
            <ta e="T912" id="Seg_6803" s="T908">nu wazak, tšaːʒɨk ponä.</ta>
            <ta e="T914" id="Seg_6804" s="T912">tastɨ qwattšaj.</ta>
            <ta e="T918" id="Seg_6805" s="T914">qaj kudɨgolam jass laktše(ä)lǯal.</ta>
            <ta e="T922" id="Seg_6806" s="T918">tan tʼärukuzattə, orse jewan.</ta>
            <ta e="T926" id="Seg_6807" s="T922">näuɣum kak watdoutdə sözɨtdɨt.</ta>
            <ta e="T933" id="Seg_6808" s="T926">täp kak innä wazʼezitda, kak razbojnik(g)am qättɨdɨt.</ta>
            <ta e="T937" id="Seg_6809" s="T933">razbojnik qotä tšätšädʼzʼen (tšätšädin).</ta>
            <ta e="T939" id="Seg_6810" s="T937">i qwannɨt.</ta>
            <ta e="T943" id="Seg_6811" s="T939">a pajät sojoɣotdə ɨdədʼen.</ta>
            <ta e="T948" id="Seg_6812" s="T943">tʼärɨn: man nɨlʼdʼin ass meːqweǯau̹.</ta>
            <ta e="T950" id="Seg_6813" s="T948">toblamdə müzulǯukeǯau.</ta>
            <ta e="T953" id="Seg_6814" s="T950">na ödɨm ütkeǯau.</ta>
            <ta e="T956" id="Seg_6815" s="T953">erat kak qäːtäǯɨt.</ta>
            <ta e="T958" id="Seg_6816" s="T956">i qwannɨt.</ta>
            <ta e="T961" id="Seg_6817" s="T958">qɨbanʼaʒala türlʼe übərat.</ta>
            <ta e="T966" id="Seg_6818" s="T961">nägamdə kak qätɨtdɨt i qwannɨt.</ta>
            <ta e="T972" id="Seg_6819" s="T966">i iːgatdänä tʼärɨn: me taze ilʼetǯaj.</ta>
            <ta e="T974" id="Seg_6820" s="T972">igə türak.</ta>
            <ta e="T977" id="Seg_6821" s="T974">qwannɨn tʼüj baɣattɨlʼe.</ta>
            <ta e="T980" id="Seg_6822" s="T977">tüj qɨlɨm paqkɨnɨt.</ta>
            <ta e="T987" id="Seg_6823" s="T980">täblam natʼet qäːlɣɨnɨt (qännɨt) i tʼüze taɣɨnɨt.</ta>
            <ta e="T991" id="Seg_6824" s="T987">saoznikamdə qarʼe ükkulʼe qwatdɨt.</ta>
            <ta e="T994" id="Seg_6825" s="T991">tädomɨlamdə wes tuɣunnɨt.</ta>
            <ta e="T999" id="Seg_6826" s="T994">iːgatdɨze omdaɣə i na qwatdaːq.</ta>
            <ta e="T1003" id="Seg_6827" s="T999">tʼülʼe meːdaɣə otdə jedoɣotdə.</ta>
            <ta e="T1005" id="Seg_6828" s="T1003">qulat aːtdalbat.</ta>
            <ta e="T1009" id="Seg_6829" s="T1005">a pajal kuttʼen jen?</ta>
            <ta e="T1011" id="Seg_6830" s="T1009">pajau̹ qupba.</ta>
            <ta e="T1014" id="Seg_6831" s="T1011">wes tugunattə tädomɨm.</ta>
            <ta e="T1018" id="Seg_6832" s="T1014">täp pirʼäɣɨtdə maːt tautdɨt.</ta>
            <ta e="T1024" id="Seg_6833" s="T1018">i nädɨn i warkɨlʼe na übɨrɨtda.</ta>
            <ta e="T1028" id="Seg_6834" s="T1024">man potdɨbon tebnan jezan.</ta>
            <ta e="T1031" id="Seg_6835" s="T1028">d̂a tau sodʼigan warkaː.</ta>
            <ta e="T1037" id="Seg_6836" s="T1031">pajat sodʼiga, mazɨm čajlaze zɨm ärtšɨs.</ta>
            <ta e="T1041" id="Seg_6837" s="T1037">mekĝa tʼärɨn: mäguntu paldʼikoq.</ta>
            <ta e="T1045" id="Seg_6838" s="T1041">man teblanä qɨdan uduruqw̹an.</ta>
            <ta e="T1048" id="Seg_6839" s="T1045">tabon jedot qwaǯan.</ta>
            <ta e="T1051" id="Seg_6840" s="T1048">teblanä aj serkeǯan.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_6841" s="T1">Xrestjan era. </ta>
            <ta e="T6" id="Seg_6842" s="T3">Tʼelɨmnɨn Xrʼestʼän era. </ta>
            <ta e="T9" id="Seg_6843" s="T6">Täbnan naːgur iːt. </ta>
            <ta e="T12" id="Seg_6844" s="T9">Iːlat wes nädälattə. </ta>
            <ta e="T19" id="Seg_6845" s="T12">Okkɨr iːt qɨdan surulle palʼdükun, warɣə iːt. </ta>
            <ta e="T30" id="Seg_6846" s="T19">A sət (sədə) iːt skatʼinalaze palʼdüqwaɣə, sɨrla warədattə, kütdɨla i konɛrla. </ta>
            <ta e="T35" id="Seg_6847" s="T30">I pašnä wardattə, apsot sejakudattə. </ta>
            <ta e="T42" id="Seg_6848" s="T35">A warɣɨ iːt kɨːdan madʼöt paldʼän (paldʼikun). </ta>
            <ta e="T48" id="Seg_6849" s="T42">Tebnan neːt tʼelɨmda i iːga tʼelɨmda. </ta>
            <ta e="T52" id="Seg_6850" s="T48">Tebɨstaɣə warɣɨn aːzuaɣə (aːzuaq). </ta>
            <ta e="T61" id="Seg_6851" s="T52">A täp natʼen madʼöɣɨn iːbɨɣaj maːt omdɨlǯɨmɨtdɨt, iːbɨɣaj maːt. </ta>
            <ta e="T67" id="Seg_6852" s="T61">Äotdänä äzätdänä tʼärɨn: “Nomse mazɨm paslawikə.” </ta>
            <ta e="T72" id="Seg_6853" s="T67">Äutdə nomɨm iːut i paslawinɨt. </ta>
            <ta e="T74" id="Seg_6854" s="T72">“Man qwatǯan. </ta>
            <ta e="T80" id="Seg_6855" s="T74">Naːgur agazlaftə okkɨr maːtqɨn ass elʼetǯutu. </ta>
            <ta e="T83" id="Seg_6856" s="T80">Manan täper sʼämjau.” </ta>
            <ta e="T87" id="Seg_6857" s="T83">Saozniktə wes tädomɨmdə tuɣunnɨt. </ta>
            <ta e="T90" id="Seg_6858" s="T87">I na üːbɨtdattə. </ta>
            <ta e="T96" id="Seg_6859" s="T90">Ästɨ äudɨ i türlʼe na ɣalɨtdattə. </ta>
            <ta e="T101" id="Seg_6860" s="T96">I täbla i na qwatdattə. </ta>
            <ta e="T106" id="Seg_6861" s="T101">No i tüle na medɨtdattə. </ta>
            <ta e="T112" id="Seg_6862" s="T106">A tebnan natʼen iːbɨɣaj maːt omdəlǯəmɨdɨt. </ta>
            <ta e="T115" id="Seg_6863" s="T112">Wes tuɣunnattə natʼet. </ta>
            <ta e="T123" id="Seg_6864" s="T115">No i ärat wes melʼe taːdərat, qoptə metdɨt. </ta>
            <ta e="T126" id="Seg_6865" s="T123">“Qardʼen madʼöt qwatǯan. </ta>
            <ta e="T131" id="Seg_6866" s="T126">A tan üdno na qwannaš. </ta>
            <ta e="T137" id="Seg_6867" s="T131">Toboɣät qajam struška papalan ig azü. </ta>
            <ta e="T140" id="Seg_6868" s="T137">Ato aːwan jetǯan.” </ta>
            <ta e="T142" id="Seg_6869" s="T140">No sekgattə. </ta>
            <ta e="T149" id="Seg_6870" s="T142">Qarʼemɨɣɨn wazɨn, aurnɨn i na qwatda madʼötdə. </ta>
            <ta e="T162" id="Seg_6871" s="T149">A näjɣum tʼärɨn: “Qajno mekga ärau tʼärɨn, što struška üttə papalan ik azə. </ta>
            <ta e="T172" id="Seg_6872" s="T162">Man sičas sət wedram struškalaze nabineǯau, nabinäǯau, qwalʼlʼe üttə qamnätǯau.” </ta>
            <ta e="T177" id="Seg_6873" s="T172">I na qwannɨdɨt i qamǯɨt. </ta>
            <ta e="T180" id="Seg_6874" s="T177">Nɨkga i mannɨpaːt. </ta>
            <ta e="T183" id="Seg_6875" s="T180">A natʼen köunɨn. </ta>
            <ta e="T189" id="Seg_6876" s="T183">Täp üdɨm soɣunnɨt i maːttɨ tüa. </ta>
            <ta e="T194" id="Seg_6877" s="T189">A täp krasiwa näjɣum jes. </ta>
            <ta e="T201" id="Seg_6878" s="T194">Qwalʼlʼe sütʼdinäj maːttə omdɨn i südɨrlʼe übɨran. </ta>
            <ta e="T205" id="Seg_6879" s="T201">A sət qɨbanʼaʒa warkaɣə. </ta>
            <ta e="T215" id="Seg_6880" s="T205">Razbojnigla čaʒatdə atdɨzʼe i tʼärattə: “Qaj struškala (köudattə) küuze čaːʒɨdattə. </ta>
            <ta e="T218" id="Seg_6881" s="T215">Qən barɨnʼä praibet!” </ta>
            <ta e="T222" id="Seg_6882" s="T218">Tebla mannɨpattə qajda wattɨ. </ta>
            <ta e="T225" id="Seg_6883" s="T222">Täbla wattɨga kotdattə. </ta>
            <ta e="T231" id="Seg_6884" s="T225">“Ugon me tau wattɨm jass qoǯɨrguzautə.” </ta>
            <ta e="T239" id="Seg_6885" s="T231">Na wattogatdə (na watotdə, na watotdə) i uːdurattə. </ta>
            <ta e="T251" id="Seg_6886" s="T239">Attaman tʼärɨn: “Tä aːmdaltə tɨtdɨn, a man qurollan mannɨbɨlʼeu qaj ilatda tɨtdɨn.” </ta>
            <ta e="T253" id="Seg_6887" s="T251">Na qurolǯa. </ta>
            <ta e="T256" id="Seg_6888" s="T253">Tüa, sernɨn maːttə. </ta>
            <ta e="T257" id="Seg_6889" s="T256">“Tʼolom.” </ta>
            <ta e="T261" id="Seg_6890" s="T257">Näjɣum tʼärɨn: “Tʼolom, tʼolom.” </ta>
            <ta e="T264" id="Seg_6891" s="T261">Stul čečelʼe meʒalǯɨt. </ta>
            <ta e="T267" id="Seg_6892" s="T264">Tʼärɨn: “Omdak tɨkga.” </ta>
            <ta e="T270" id="Seg_6893" s="T267">A täp omdɨn. </ta>
            <ta e="T273" id="Seg_6894" s="T270">Kulupbɨlʼe na übɨrɨtdaɣ. </ta>
            <ta e="T278" id="Seg_6895" s="T273">Ataman tʼärɨn: “Tan mekga tɨbɨtdɨlʼet.” </ta>
            <ta e="T282" id="Seg_6896" s="T278">“No mannan ärau orse. </ta>
            <ta e="T288" id="Seg_6897" s="T282">Täp tastɨ qwačɨt i mazɨm qwačɨt.” </ta>
            <ta e="T293" id="Seg_6898" s="T288">“A man tekga qudugo mennɨǯan. </ta>
            <ta e="T303" id="Seg_6899" s="T293">(Optə jennɨš i sɨnkowa prowoloka, täp tau kudɨgom as lakčeǯit). </ta>
            <ta e="T307" id="Seg_6900" s="T303">Täp na tünnɨš üdomɨɣɨn. </ta>
            <ta e="T310" id="Seg_6901" s="T307">Tan tʼärak: “Qozɨrčɨlaj.” </ta>
            <ta e="T318" id="Seg_6902" s="T310">Tabnä tʼärak: “Qut turakɨn qalʼeǯɨn, udlamdə moɣunä saruku.” </ta>
            <ta e="T326" id="Seg_6903" s="T318">A tan täbɨm turakɨn qwäzʼet, udlamdə moɣunä saːrʼet. </ta>
            <ta e="T331" id="Seg_6904" s="T326">Täp tau qudugom as lakčeǯin. </ta>
            <ta e="T341" id="Seg_6905" s="T331">Täp jeʒlʼi ass laqčɨt, a tan ponä čatǯak, mazɨm lakgolʼgə. </ta>
            <ta e="T346" id="Seg_6906" s="T341">Man tütǯan, täbɨm i qwatčau. </ta>
            <ta e="T348" id="Seg_6907" s="T346">Taze ilʼetǯaj.” </ta>
            <ta e="T350" id="Seg_6908" s="T348">Ärat tüa. </ta>
            <ta e="T355" id="Seg_6909" s="T350">Täp täbɨm apstɨt, tʼärɨn: “Qozɨrčɨlaj.” </ta>
            <ta e="T361" id="Seg_6910" s="T355">Ärat tʼärɨn: “Man nunɨtʼipban, qotdugu nadə.” </ta>
            <ta e="T366" id="Seg_6911" s="T361">“Nu dawaj, qozɨrčɨlaj ass qutdɨn. </ta>
            <ta e="T373" id="Seg_6912" s="T366">Ato onän amdan i skušnan mekga jen.” </ta>
            <ta e="T377" id="Seg_6913" s="T373">Nu i omdaɣə qozɨrčɨlʼe. </ta>
            <ta e="T383" id="Seg_6914" s="T377">Qut turaqɨn qalʼeǯɨn, udlamdə sarelǯugu moɣunä. </ta>
            <ta e="T386" id="Seg_6915" s="T383">Ärat turaqɨn qalɨn. </ta>
            <ta e="T393" id="Seg_6916" s="T386">“Udlamdə nadə sarelǯugu, udomdə nadə sarugu moɣunä. </ta>
            <ta e="T403" id="Seg_6917" s="T393">A jeʒlʼe man qalɨnän turaqɨn, tan bə mekga udlau zarelǯɨnäl. </ta>
            <ta e="T409" id="Seg_6918" s="T403">A täper man tʼekga udomdə saretǯau.” </ta>
            <ta e="T412" id="Seg_6919" s="T409">Ärat tʼärɨn: “Sarät”. </ta>
            <ta e="T414" id="Seg_6920" s="T412">Täp saːrät. </ta>
            <ta e="T420" id="Seg_6921" s="T414">Ärat kak turugulǯɨt, qudɨgola wes laqčelattə. </ta>
            <ta e="T424" id="Seg_6922" s="T420">No i qotdɨgu quʒannaɣɨ. </ta>
            <ta e="T426" id="Seg_6923" s="T424">Qarʼemɨɣɨn wazɨn. </ta>
            <ta e="T433" id="Seg_6924" s="T426">Näjɣum wes potqɨnɨt, wadʼilam qajlam, äramdə apstɨt. </ta>
            <ta e="T436" id="Seg_6925" s="T433">Ärat madʼot qwannɨn. </ta>
            <ta e="T443" id="Seg_6926" s="T436">Näjɣum ponä sapɨsin i na qaːrolʼdʼä: “Tʼükkɨ! </ta>
            <ta e="T445" id="Seg_6927" s="T443">Ärau qwanba!” </ta>
            <ta e="T448" id="Seg_6928" s="T445">Ataman na tütda. </ta>
            <ta e="T451" id="Seg_6929" s="T448">“No qaj, qozɨrčezalʼe?” </ta>
            <ta e="T452" id="Seg_6930" s="T451">“Qozɨrčezaj. </ta>
            <ta e="T462" id="Seg_6931" s="T452">Man tʼekga tʼärɨzan, täp orse jen, na qudɨgolam wes lakčelǯɨt. </ta>
            <ta e="T470" id="Seg_6932" s="T462">“A tamdʼel man tʼekga meǯau optɨ qudɨgo mennɨǯan. </ta>
            <ta e="T476" id="Seg_6933" s="T470">Üdəmɨɣɨn na tünnɨš, aj tʼärak: Qozɨrčɨlaj. </ta>
            <ta e="T482" id="Seg_6934" s="T476">Qut turakɨn qalʼeǯɨn, udlamdə moɣunä saːruku.” </ta>
            <ta e="T487" id="Seg_6935" s="T482">Näjɣum täbɨm apstɨpbaːt, arakazʼe ärčibat. </ta>
            <ta e="T489" id="Seg_6936" s="T487">Araqɨla üttɨdɨ. </ta>
            <ta e="T491" id="Seg_6937" s="T489">Täbeɣum qwannɨn. </ta>
            <ta e="T493" id="Seg_6938" s="T491">Ärat tüa. </ta>
            <ta e="T499" id="Seg_6939" s="T493">Surumlam koːcʼen qwatpat i pekqɨ qwatpadɨt. </ta>
            <ta e="T502" id="Seg_6940" s="T499">Täp täbɨm apstɨt. </ta>
            <ta e="T504" id="Seg_6941" s="T502">Tʼärɨn: “Qozɨrčɨlaj.” </ta>
            <ta e="T508" id="Seg_6942" s="T504">Ärat tʼärɨn: “Man nuːnɨdʼipbaːn.” </ta>
            <ta e="T520" id="Seg_6943" s="T508">Pajat tʼärɨn: “Qozɨrčɨlaj tʼem ugoworom, qudə qut turakɨn qalʼeǯɨn, udɨmdə moɣunä saːrugu. </ta>
            <ta e="T528" id="Seg_6944" s="T520">Ješlʼi man qalan duraqɨn, man udou saːraq (sareǯal).” </ta>
            <ta e="T531" id="Seg_6945" s="T528">Täbɨstaɣə omdaɣə qozɨrčilʼe. </ta>
            <ta e="T534" id="Seg_6946" s="T531">Ärat turaqɨn qalɨn. </ta>
            <ta e="T538" id="Seg_6947" s="T534">“Nu, täper udomdə saːreǯau.” </ta>
            <ta e="T541" id="Seg_6948" s="T538">Täp saːrɨt udɨmdə. </ta>
            <ta e="T546" id="Seg_6949" s="T541">Täp kak turgulǯɨtda, qudɨgo laqčɨdin. </ta>
            <ta e="T548" id="Seg_6950" s="T546">Quʒannaɣə qotduɣɨ. </ta>
            <ta e="T550" id="Seg_6951" s="T548">Qarʼemɨɣɨn wazaɣə. </ta>
            <ta e="T553" id="Seg_6952" s="T550">Näjɣum äramdə apstɨt. </ta>
            <ta e="T559" id="Seg_6953" s="T553">Ärat aurnɨn i madʼot na qwatda. </ta>
            <ta e="T566" id="Seg_6954" s="T559">Näiɣum ponä čaːǯɨn i na qaːronʼen: “Tükkɨ! </ta>
            <ta e="T568" id="Seg_6955" s="T566">Ärau qwanba!” </ta>
            <ta e="T570" id="Seg_6956" s="T568">Razbojnik tüwa. </ta>
            <ta e="T576" id="Seg_6957" s="T570">Näiɣum tʼärɨn: “Qudɨgolam ärau wes lakčelǯɨt. </ta>
            <ta e="T579" id="Seg_6958" s="T576">Täp orse jen.” </ta>
            <ta e="T591" id="Seg_6959" s="T579">“A man tamdʼel tekga meǯau kudɨgo optɨ i sɨnkowaja prowoloka okkɨrmɨt tamkɨlba.” </ta>
            <ta e="T599" id="Seg_6960" s="T591">Üːdɨmɨɣɨn na tünnɨš, tan aj tʼärak: Qozɨrčuɣu, qozɨrčilaj. </ta>
            <ta e="T606" id="Seg_6961" s="T599">Qut turakɨn kalʼeǯɨn, to udɨmdə moɣunä saːrugu. </ta>
            <ta e="T608" id="Seg_6962" s="T606">Ära tüa. </ta>
            <ta e="T613" id="Seg_6963" s="T608">Täp täbɨm apstɨt, tʼärɨn: “Qozɨrčɨlaj.” </ta>
            <ta e="T619" id="Seg_6964" s="T613">Ärat tʼärɨn: “Man nunɨdʼzʼipan, nadə qotdɨɣu.” </ta>
            <ta e="T622" id="Seg_6965" s="T619">“No qozɨrčɨlaj, qozɨrčɨlaj. </ta>
            <ta e="T630" id="Seg_6966" s="T622">Xotʼ okkɨrɨn qut turakɨn kalʼeǯɨn, udɨmdə moɣunä saaruku. </ta>
            <ta e="T637" id="Seg_6967" s="T630">Jeʒlʼi man kalʼeǯan, man udou saːrʼet moɣunä.” </ta>
            <ta e="T640" id="Seg_6968" s="T637">I oːmdaɣə qozɨrčɨlʼe. </ta>
            <ta e="T646" id="Seg_6969" s="T640">Äramdə turagɨwlʼe qwɛdʼit, udɨmdə moɣunä saːrɨt. </ta>
            <ta e="T652" id="Seg_6970" s="T646">Täp kak turgulǯɨtda, qudəkolam jass laqčɨt. </ta>
            <ta e="T656" id="Seg_6971" s="T652">Pajätdänä tʼärɨn: “Tʼiket udou.” </ta>
            <ta e="T659" id="Seg_6972" s="T656">Pajät tʼärɨn: “Lakčet.” </ta>
            <ta e="T664" id="Seg_6973" s="T659">Täp aj turgulǯɨn, jass lakčɨt. </ta>
            <ta e="T666" id="Seg_6974" s="T664">Tʼärɨn: “Tʼiket.” </ta>
            <ta e="T668" id="Seg_6975" s="T666">“Ass tʼikeǯau.” </ta>
            <ta e="T675" id="Seg_6976" s="T668">Ponä sappɨselʼe qwannɨn i na lakgolʼdʼä: “Tükkɨ!” </ta>
            <ta e="T680" id="Seg_6977" s="T675">I na razbojnik na tütda. </ta>
            <ta e="T685" id="Seg_6978" s="T680">Tʼärɨn:“ “Ärau qudogom ass lakčɨt. </ta>
            <ta e="T687" id="Seg_6979" s="T685">Serlʼe qwallaj.” </ta>
            <ta e="T690" id="Seg_6980" s="T687">I mattə sernaɣə. </ta>
            <ta e="T695" id="Seg_6981" s="T690">Razbojnik tʼärɨn: “Qardʼen qarʼemɨɣɨn qwatčaj. </ta>
            <ta e="T699" id="Seg_6982" s="T695">Tulǯät wʼädran tʼiːr saːq. </ta>
            <ta e="T705" id="Seg_6983" s="T699">Täbɨm saːɣɨn parotdə puləsejlatdɨze (saːɣot) omdɨlǯeǯaj. </ta>
            <ta e="T707" id="Seg_6984" s="T705">Qarʼeməttə aːmdə. </ta>
            <ta e="T709" id="Seg_6985" s="T707">Qarʼemɨɣɨn qwatčaj.” </ta>
            <ta e="T714" id="Seg_6986" s="T709">Näjɣum watdoɣɨt tülʼe na söːzɨtdət. </ta>
            <ta e="T720" id="Seg_6987" s="T714">“Tan tʼärɨkuzat što man orse jewan. </ta>
            <ta e="T725" id="Seg_6988" s="T720">A qajno qudɨgom ass laqčeǯal? </ta>
            <ta e="T729" id="Seg_6989" s="T725">Tapär qarʼemɨɣɨn tastɨ qwatčaj.” </ta>
            <ta e="T738" id="Seg_6990" s="T729">Näjɣum quronnɨn, saːq tulǯɨtdɨt wädran tiːr i salǯɨbot qamǯɨt. </ta>
            <ta e="T742" id="Seg_6991" s="T738">I täbɨm puluzein omdɨlǯɨt. </ta>
            <ta e="T745" id="Seg_6992" s="T742">Tʼärɨn:“ “Aːmdaq qarʼemɨtdə! </ta>
            <ta e="T748" id="Seg_6993" s="T745">Qarʼemɨɣɨn tastɨ qwatčaj.” </ta>
            <ta e="T754" id="Seg_6994" s="T748">Omdaɣə aurlʼe razbojnikse arakaj ärlʼe jübɨraq. </ta>
            <ta e="T762" id="Seg_6995" s="T754">Näjɣum tüuwa, äratdə kötdə äramdə watdoot na sözɨtdət. </ta>
            <ta e="T766" id="Seg_6996" s="T762">“Qarʼemɨɣɨn me tastɨ qwatčaj.” </ta>
            <ta e="T773" id="Seg_6997" s="T766">Otdə na qwatda razbojnikɨm kaːwalgut, niːdellʼe taːdɨrɨt. </ta>
            <ta e="T779" id="Seg_6998" s="T773">I qotduɣu quʒannax i na qotdolǯaɣɨ. </ta>
            <ta e="T784" id="Seg_6999" s="T779">(Ärat) täp wazɨn innä, paldʼukun. </ta>
            <ta e="T792" id="Seg_7000" s="T784">A kɨːban nägat iːɣat täbɨstaːɣə qotdɨzaɣə palatin paroɣɨn. </ta>
            <ta e="T796" id="Seg_7001" s="T792">Nägat küzɨgu ilʼlʼe tʼütʼöun. </ta>
            <ta e="T802" id="Seg_7002" s="T796">Täp nägatdanä tʼärɨn: “Man udlau tʼikɨlʼel.” </ta>
            <ta e="T810" id="Seg_7003" s="T802">A nägat tʼärɨn: “Tan tʼärukuzat što orse jewan. </ta>
            <ta e="T812" id="Seg_7004" s="T810">Lakčelǯet qudɨgom.” </ta>
            <ta e="T817" id="Seg_7005" s="T812">Palatin barot čaǯɨn i qotda. </ta>
            <ta e="T822" id="Seg_7006" s="T817">Iːgat palatiɣəndo ilʼlʼe tʼütʼöun küzɨgu. </ta>
            <ta e="T828" id="Seg_7007" s="T822">Täp iːgatdänä tʼärɨn: “Man udou tʼiːkelʼel.” </ta>
            <ta e="T834" id="Seg_7008" s="T828">Täp tüwa äzɨtdänä i tʼikəlʼe jübərat. </ta>
            <ta e="T836" id="Seg_7009" s="T834">Jass tʼikɨt. </ta>
            <ta e="T839" id="Seg_7010" s="T836">“Mekga jass tʼikɨɣuː.” </ta>
            <ta e="T843" id="Seg_7011" s="T839">Ästɨ tʼärɨn: “Patpʼilka datkə.” </ta>
            <ta e="T850" id="Seg_7012" s="T843">Täp tannɨt i kudɨgolam sɨlʼelʼlʼe na übərətdɨt. </ta>
            <ta e="T853" id="Seg_7013" s="T850">Sɨlɨt, qudəgola warsɨwannat. </ta>
            <ta e="T858" id="Seg_7014" s="T853">Täp iːgatdänä tʼarɨn: “Qwalʼlʼe qotdaq. </ta>
            <ta e="T861" id="Seg_7015" s="T858">Qarʼemɨɣɨn iːgə larɨpbaːk. </ta>
            <ta e="T865" id="Seg_7016" s="T861">Me tazʼe onäj ilʼetǯaj.” </ta>
            <ta e="T868" id="Seg_7017" s="T865">Nom tʼelɨmlʼe jübɨrɨn. </ta>
            <ta e="T879" id="Seg_7018" s="T868">Täp kudɨgolam udoɣətdə pennɨt (kak budto saːrɨpbaːt) i omdɨn puluzein parot. </ta>
            <ta e="T884" id="Seg_7019" s="T879">Pajät wazɨn, tülʼe watdoɨn söːzɨt. </ta>
            <ta e="T888" id="Seg_7020" s="T884">Razbojnignä tʼärɨn: “Nu qwallaj.” </ta>
            <ta e="T893" id="Seg_7021" s="T888">A razbojnik tʼärɨn: “Aurlʼe qwallajze.” </ta>
            <ta e="T896" id="Seg_7022" s="T893">Tep wadʼilam müzurɣɨnnät. </ta>
            <ta e="T897" id="Seg_7023" s="T896">Aurnaɣə. </ta>
            <ta e="T900" id="Seg_7024" s="T897">“Nu täpär qwallaj. </ta>
            <ta e="T904" id="Seg_7025" s="T900">Maːtqɨn qwatčaj alʼi ponän?” </ta>
            <ta e="T908" id="Seg_7026" s="T904">Razbojnik tʼärɨn: “Ponän qwatčaj. </ta>
            <ta e="T912" id="Seg_7027" s="T908">Nu wazak, čaːʒɨk ponä. </ta>
            <ta e="T914" id="Seg_7028" s="T912">Tastɨ qwatčaj. </ta>
            <ta e="T918" id="Seg_7029" s="T914">Qaj kudɨgolam jass lakčelǯal. </ta>
            <ta e="T922" id="Seg_7030" s="T918">Tan tʼärukuzattə, orse jewan.” </ta>
            <ta e="T926" id="Seg_7031" s="T922">Näuɣum kak watdoutdə sözɨtdɨt. </ta>
            <ta e="T933" id="Seg_7032" s="T926">Täp kak innä wazʼezitda, kak razbojnikam qättɨdɨt. </ta>
            <ta e="T937" id="Seg_7033" s="T933">Razbojnik qotä čäčädʼzʼen (čäčädin). </ta>
            <ta e="T939" id="Seg_7034" s="T937">I qwannɨt. </ta>
            <ta e="T943" id="Seg_7035" s="T939">A pajät sojoɣotdə ɨdədʼen. </ta>
            <ta e="T948" id="Seg_7036" s="T943">Tʼärɨn:“ “Man nɨlʼdʼin ass meːqweǯau. </ta>
            <ta e="T950" id="Seg_7037" s="T948">Toblamdə müzulǯukeǯau. </ta>
            <ta e="T953" id="Seg_7038" s="T950">Na ödɨm ütkeǯau.” </ta>
            <ta e="T956" id="Seg_7039" s="T953">Erat kak qäːtäǯɨt. </ta>
            <ta e="T958" id="Seg_7040" s="T956">I qwannɨt. </ta>
            <ta e="T961" id="Seg_7041" s="T958">Qɨbanʼaʒala türlʼe übərat. </ta>
            <ta e="T966" id="Seg_7042" s="T961">Nägamdə kak qätɨtdɨt i qwannɨt. </ta>
            <ta e="T972" id="Seg_7043" s="T966">I iːgatdänä tʼärɨn: “Me taze ilʼetǯaj. </ta>
            <ta e="T974" id="Seg_7044" s="T972">Igə türak.” </ta>
            <ta e="T977" id="Seg_7045" s="T974">Qwannɨn tʼüj baɣattɨlʼe. </ta>
            <ta e="T980" id="Seg_7046" s="T977">Tüj qɨlɨm paqkɨnɨt. </ta>
            <ta e="T987" id="Seg_7047" s="T980">Täblam natʼet qäːlɣɨnɨt (qännɨt) i tʼüze taɣɨnɨt. </ta>
            <ta e="T991" id="Seg_7048" s="T987">Saoznikamdə qarʼe ükkulʼe qwatdɨt. </ta>
            <ta e="T994" id="Seg_7049" s="T991">Tädomɨlamdə wes tuɣunnɨt. </ta>
            <ta e="T999" id="Seg_7050" s="T994">Iːgatdɨze omdaɣə i na qwatdaːq. </ta>
            <ta e="T1003" id="Seg_7051" s="T999">Tʼülʼe meːdaɣə otdə jedoɣotdə. </ta>
            <ta e="T1005" id="Seg_7052" s="T1003">Qulat aːtdalbat. </ta>
            <ta e="T1009" id="Seg_7053" s="T1005">“A pajal kuttʼen jen?” </ta>
            <ta e="T1011" id="Seg_7054" s="T1009">“Pajau qupba.” </ta>
            <ta e="T1014" id="Seg_7055" s="T1011">Wes tugunattə tädomɨm. </ta>
            <ta e="T1018" id="Seg_7056" s="T1014">Täp pirʼäɣɨtdə maːt tautdɨt. </ta>
            <ta e="T1024" id="Seg_7057" s="T1018">I nädɨn i warkɨlʼe na übɨrɨtda. </ta>
            <ta e="T1028" id="Seg_7058" s="T1024">Man potdɨbon tebnan jezan. </ta>
            <ta e="T1031" id="Seg_7059" s="T1028">Datau sodʼigan warkaː. </ta>
            <ta e="T1037" id="Seg_7060" s="T1031">Pajat sodʼiga, mazɨm čajlaze zɨm ärčɨs. </ta>
            <ta e="T1041" id="Seg_7061" s="T1037">Mekga tʼärɨn: “Mäguntu paldʼikoq.” </ta>
            <ta e="T1045" id="Seg_7062" s="T1041">Man teblanä qɨdan uduruqwan. </ta>
            <ta e="T1048" id="Seg_7063" s="T1045">Tabon jedot qwaǯan. </ta>
            <ta e="T1051" id="Seg_7064" s="T1048">Teblanä aj serkeǯan. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_7065" s="T1">Xrestjan</ta>
            <ta e="T3" id="Seg_7066" s="T2">era</ta>
            <ta e="T4" id="Seg_7067" s="T3">tʼelɨm-nɨ-n</ta>
            <ta e="T5" id="Seg_7068" s="T4">Xrʼestʼän</ta>
            <ta e="T6" id="Seg_7069" s="T5">era</ta>
            <ta e="T7" id="Seg_7070" s="T6">täb-nan</ta>
            <ta e="T8" id="Seg_7071" s="T7">naːgur</ta>
            <ta e="T9" id="Seg_7072" s="T8">iː-t</ta>
            <ta e="T10" id="Seg_7073" s="T9">iː-la-t</ta>
            <ta e="T11" id="Seg_7074" s="T10">wes</ta>
            <ta e="T12" id="Seg_7075" s="T11">nädä-la-ttə</ta>
            <ta e="T13" id="Seg_7076" s="T12">okkɨr</ta>
            <ta e="T14" id="Seg_7077" s="T13">iː-t</ta>
            <ta e="T15" id="Seg_7078" s="T14">qɨdan</ta>
            <ta e="T16" id="Seg_7079" s="T15">suru-l-le</ta>
            <ta e="T17" id="Seg_7080" s="T16">palʼdü-ku-n</ta>
            <ta e="T18" id="Seg_7081" s="T17">warɣə</ta>
            <ta e="T19" id="Seg_7082" s="T18">iː-t</ta>
            <ta e="T20" id="Seg_7083" s="T19">a</ta>
            <ta e="T21" id="Seg_7084" s="T20">sət</ta>
            <ta e="T22" id="Seg_7085" s="T21">sədə</ta>
            <ta e="T23" id="Seg_7086" s="T22">iː-t</ta>
            <ta e="T24" id="Seg_7087" s="T23">skatʼina-la-ze</ta>
            <ta e="T25" id="Seg_7088" s="T24">palʼdü-q-wa-ɣə</ta>
            <ta e="T26" id="Seg_7089" s="T25">sɨr-la</ta>
            <ta e="T27" id="Seg_7090" s="T26">warə-da-ttə</ta>
            <ta e="T28" id="Seg_7091" s="T27">kütdɨ-la</ta>
            <ta e="T29" id="Seg_7092" s="T28">i</ta>
            <ta e="T30" id="Seg_7093" s="T29">konɛr-la</ta>
            <ta e="T31" id="Seg_7094" s="T30">i</ta>
            <ta e="T32" id="Seg_7095" s="T31">pašnä</ta>
            <ta e="T33" id="Seg_7096" s="T32">war-da-ttə</ta>
            <ta e="T34" id="Seg_7097" s="T33">apso-t</ta>
            <ta e="T35" id="Seg_7098" s="T34">seja-ku-da-ttə</ta>
            <ta e="T36" id="Seg_7099" s="T35">a</ta>
            <ta e="T37" id="Seg_7100" s="T36">warɣɨ</ta>
            <ta e="T38" id="Seg_7101" s="T37">iː-t</ta>
            <ta e="T39" id="Seg_7102" s="T38">kɨːdan</ta>
            <ta e="T40" id="Seg_7103" s="T39">madʼ-ö-t</ta>
            <ta e="T41" id="Seg_7104" s="T40">paldʼä-n</ta>
            <ta e="T42" id="Seg_7105" s="T41">paldʼi-ku-n</ta>
            <ta e="T43" id="Seg_7106" s="T42">teb-nan</ta>
            <ta e="T44" id="Seg_7107" s="T43">neː-t</ta>
            <ta e="T45" id="Seg_7108" s="T44">tʼelɨm-da</ta>
            <ta e="T46" id="Seg_7109" s="T45">i</ta>
            <ta e="T47" id="Seg_7110" s="T46">iː-ga</ta>
            <ta e="T48" id="Seg_7111" s="T47">tʼelɨm-da</ta>
            <ta e="T49" id="Seg_7112" s="T48">teb-ɨ-staɣə</ta>
            <ta e="T50" id="Seg_7113" s="T49">warɣɨ-n</ta>
            <ta e="T51" id="Seg_7114" s="T50">aːzu-a-ɣə</ta>
            <ta e="T52" id="Seg_7115" s="T51">aːzu-a-q</ta>
            <ta e="T53" id="Seg_7116" s="T52">a</ta>
            <ta e="T54" id="Seg_7117" s="T53">täp</ta>
            <ta e="T55" id="Seg_7118" s="T54">natʼe-n</ta>
            <ta e="T56" id="Seg_7119" s="T55">madʼ-ö-ɣɨn</ta>
            <ta e="T57" id="Seg_7120" s="T56">iːbɨɣaj</ta>
            <ta e="T58" id="Seg_7121" s="T57">maːt</ta>
            <ta e="T59" id="Seg_7122" s="T58">omdɨ-lǯɨ-mɨ-tdɨ-t</ta>
            <ta e="T60" id="Seg_7123" s="T59">iːbɨɣaj</ta>
            <ta e="T61" id="Seg_7124" s="T60">maːt</ta>
            <ta e="T62" id="Seg_7125" s="T61">äo-tdä-nä</ta>
            <ta e="T63" id="Seg_7126" s="T62">äzä-tdä-nä</ta>
            <ta e="T64" id="Seg_7127" s="T63">tʼärɨ-n</ta>
            <ta e="T65" id="Seg_7128" s="T64">nom-se</ta>
            <ta e="T66" id="Seg_7129" s="T65">mazɨm</ta>
            <ta e="T67" id="Seg_7130" s="T66">paslawi-kə</ta>
            <ta e="T68" id="Seg_7131" s="T67">äu-tdə</ta>
            <ta e="T69" id="Seg_7132" s="T68">nom-ɨ-m</ta>
            <ta e="T70" id="Seg_7133" s="T69">iː-u-t</ta>
            <ta e="T71" id="Seg_7134" s="T70">i</ta>
            <ta e="T72" id="Seg_7135" s="T71">paslawi-nɨ-t</ta>
            <ta e="T73" id="Seg_7136" s="T72">Man</ta>
            <ta e="T74" id="Seg_7137" s="T73">qwat-ǯa-n</ta>
            <ta e="T75" id="Seg_7138" s="T74">naːgur</ta>
            <ta e="T76" id="Seg_7139" s="T75">aga-z-la-ftə</ta>
            <ta e="T77" id="Seg_7140" s="T76">okkɨr</ta>
            <ta e="T78" id="Seg_7141" s="T77">maːt-qɨn</ta>
            <ta e="T79" id="Seg_7142" s="T78">ass</ta>
            <ta e="T80" id="Seg_7143" s="T79">elʼ-etǯu-tu</ta>
            <ta e="T81" id="Seg_7144" s="T80">ma-nan</ta>
            <ta e="T82" id="Seg_7145" s="T81">täper</ta>
            <ta e="T83" id="Seg_7146" s="T82">sʼämja-u</ta>
            <ta e="T84" id="Seg_7147" s="T83">saoznik-tə</ta>
            <ta e="T85" id="Seg_7148" s="T84">wes</ta>
            <ta e="T86" id="Seg_7149" s="T85">tädomɨ-m-də</ta>
            <ta e="T87" id="Seg_7150" s="T86">tuɣun-nɨ-t</ta>
            <ta e="T88" id="Seg_7151" s="T87">i</ta>
            <ta e="T89" id="Seg_7152" s="T88">na</ta>
            <ta e="T90" id="Seg_7153" s="T89">üːbɨ-tda-ttə</ta>
            <ta e="T91" id="Seg_7154" s="T90">äs-tɨ</ta>
            <ta e="T92" id="Seg_7155" s="T91">äu-dɨ</ta>
            <ta e="T93" id="Seg_7156" s="T92">i</ta>
            <ta e="T94" id="Seg_7157" s="T93">tür-lʼe</ta>
            <ta e="T95" id="Seg_7158" s="T94">na</ta>
            <ta e="T96" id="Seg_7159" s="T95">ɣalɨ-tda-ttə</ta>
            <ta e="T97" id="Seg_7160" s="T96">i</ta>
            <ta e="T98" id="Seg_7161" s="T97">täb-la</ta>
            <ta e="T99" id="Seg_7162" s="T98">i</ta>
            <ta e="T100" id="Seg_7163" s="T99">na</ta>
            <ta e="T101" id="Seg_7164" s="T100">qwa-tda-ttə</ta>
            <ta e="T102" id="Seg_7165" s="T101">no</ta>
            <ta e="T103" id="Seg_7166" s="T102">i</ta>
            <ta e="T104" id="Seg_7167" s="T103">tü-le</ta>
            <ta e="T105" id="Seg_7168" s="T104">na</ta>
            <ta e="T106" id="Seg_7169" s="T105">medɨ-tda-ttə</ta>
            <ta e="T107" id="Seg_7170" s="T106">a</ta>
            <ta e="T108" id="Seg_7171" s="T107">teb-nan</ta>
            <ta e="T109" id="Seg_7172" s="T108">natʼe-n</ta>
            <ta e="T110" id="Seg_7173" s="T109">iːbɨɣaj</ta>
            <ta e="T111" id="Seg_7174" s="T110">maːt</ta>
            <ta e="T112" id="Seg_7175" s="T111">omdə-lǯə-mɨ-dɨ-t</ta>
            <ta e="T113" id="Seg_7176" s="T112">wes</ta>
            <ta e="T114" id="Seg_7177" s="T113">tuɣun-na-ttə</ta>
            <ta e="T115" id="Seg_7178" s="T114">natʼe-t</ta>
            <ta e="T116" id="Seg_7179" s="T115">no</ta>
            <ta e="T117" id="Seg_7180" s="T116">i</ta>
            <ta e="T118" id="Seg_7181" s="T117">ära-t</ta>
            <ta e="T119" id="Seg_7182" s="T118">wes</ta>
            <ta e="T120" id="Seg_7183" s="T119">me-lʼe</ta>
            <ta e="T121" id="Seg_7184" s="T120">taːd-ə-r-a-t</ta>
            <ta e="T122" id="Seg_7185" s="T121">qoptə</ta>
            <ta e="T123" id="Seg_7186" s="T122">me-tdɨ-t</ta>
            <ta e="T124" id="Seg_7187" s="T123">qar-dʼe-n</ta>
            <ta e="T125" id="Seg_7188" s="T124">madʼ-ö-t</ta>
            <ta e="T126" id="Seg_7189" s="T125">qwat-ǯa-n</ta>
            <ta e="T127" id="Seg_7190" s="T126">a</ta>
            <ta e="T128" id="Seg_7191" s="T127">tat</ta>
            <ta e="T129" id="Seg_7192" s="T128">üd-no</ta>
            <ta e="T130" id="Seg_7193" s="T129">na</ta>
            <ta e="T131" id="Seg_7194" s="T130">qwan-naš</ta>
            <ta e="T132" id="Seg_7195" s="T131">tobo-ɣät</ta>
            <ta e="T133" id="Seg_7196" s="T132">qaj-am</ta>
            <ta e="T134" id="Seg_7197" s="T133">struška</ta>
            <ta e="T135" id="Seg_7198" s="T134">papala-n</ta>
            <ta e="T136" id="Seg_7199" s="T135">ig</ta>
            <ta e="T137" id="Seg_7200" s="T136">azü</ta>
            <ta e="T138" id="Seg_7201" s="T137">ato</ta>
            <ta e="T139" id="Seg_7202" s="T138">aːwa-n</ta>
            <ta e="T140" id="Seg_7203" s="T139">je-tǯa-n</ta>
            <ta e="T141" id="Seg_7204" s="T140">no</ta>
            <ta e="T142" id="Seg_7205" s="T141">sekga-ttə</ta>
            <ta e="T143" id="Seg_7206" s="T142">qarʼe-mɨ-ɣɨn</ta>
            <ta e="T144" id="Seg_7207" s="T143">wazɨ-n</ta>
            <ta e="T145" id="Seg_7208" s="T144">au-r-nɨ-n</ta>
            <ta e="T146" id="Seg_7209" s="T145">i</ta>
            <ta e="T147" id="Seg_7210" s="T146">na</ta>
            <ta e="T148" id="Seg_7211" s="T147">qwat-da</ta>
            <ta e="T149" id="Seg_7212" s="T148">madʼ-ö-tdə</ta>
            <ta e="T150" id="Seg_7213" s="T149">a</ta>
            <ta e="T151" id="Seg_7214" s="T150">nä-j-ɣum</ta>
            <ta e="T152" id="Seg_7215" s="T151">tʼärɨ-n</ta>
            <ta e="T153" id="Seg_7216" s="T152">qaj-no</ta>
            <ta e="T154" id="Seg_7217" s="T153">mekga</ta>
            <ta e="T155" id="Seg_7218" s="T154">ära-u</ta>
            <ta e="T156" id="Seg_7219" s="T155">tʼärɨ-n</ta>
            <ta e="T157" id="Seg_7220" s="T156">što</ta>
            <ta e="T158" id="Seg_7221" s="T157">struška</ta>
            <ta e="T159" id="Seg_7222" s="T158">üt-tə</ta>
            <ta e="T160" id="Seg_7223" s="T159">papala-n</ta>
            <ta e="T161" id="Seg_7224" s="T160">ik</ta>
            <ta e="T162" id="Seg_7225" s="T161">azə</ta>
            <ta e="T163" id="Seg_7226" s="T162">man</ta>
            <ta e="T164" id="Seg_7227" s="T163">sičas</ta>
            <ta e="T165" id="Seg_7228" s="T164">sət</ta>
            <ta e="T166" id="Seg_7229" s="T165">wedra-m</ta>
            <ta e="T167" id="Seg_7230" s="T166">struška-la-ze</ta>
            <ta e="T168" id="Seg_7231" s="T167">nabi-n-eǯa-u</ta>
            <ta e="T169" id="Seg_7232" s="T168">nabi-n-äǯa-u</ta>
            <ta e="T170" id="Seg_7233" s="T169">qwalʼ-lʼe</ta>
            <ta e="T171" id="Seg_7234" s="T170">üt-tə</ta>
            <ta e="T172" id="Seg_7235" s="T171">qamn-ätǯa-u</ta>
            <ta e="T173" id="Seg_7236" s="T172">i</ta>
            <ta e="T174" id="Seg_7237" s="T173">na</ta>
            <ta e="T175" id="Seg_7238" s="T174">qwan-nɨ-dɨ-t</ta>
            <ta e="T176" id="Seg_7239" s="T175">i</ta>
            <ta e="T177" id="Seg_7240" s="T176">qamǯɨ-t</ta>
            <ta e="T178" id="Seg_7241" s="T177">nɨ-kga</ta>
            <ta e="T179" id="Seg_7242" s="T178">i</ta>
            <ta e="T180" id="Seg_7243" s="T179">mannɨ-paː-t</ta>
            <ta e="T181" id="Seg_7244" s="T180">a</ta>
            <ta e="T182" id="Seg_7245" s="T181">natʼe-n</ta>
            <ta e="T183" id="Seg_7246" s="T182">köu-nɨ-n</ta>
            <ta e="T184" id="Seg_7247" s="T183">täp</ta>
            <ta e="T185" id="Seg_7248" s="T184">üd-ɨ-m</ta>
            <ta e="T186" id="Seg_7249" s="T185">soɣu-n-nɨ-t</ta>
            <ta e="T187" id="Seg_7250" s="T186">i</ta>
            <ta e="T188" id="Seg_7251" s="T187">maːt-tɨ</ta>
            <ta e="T189" id="Seg_7252" s="T188">tü-a</ta>
            <ta e="T190" id="Seg_7253" s="T189">a</ta>
            <ta e="T191" id="Seg_7254" s="T190">täp</ta>
            <ta e="T192" id="Seg_7255" s="T191">krasiwa</ta>
            <ta e="T193" id="Seg_7256" s="T192">nä-j-ɣum</ta>
            <ta e="T194" id="Seg_7257" s="T193">je-s</ta>
            <ta e="T195" id="Seg_7258" s="T194">qwalʼ-lʼe</ta>
            <ta e="T196" id="Seg_7259" s="T195">sütʼdi-näj</ta>
            <ta e="T197" id="Seg_7260" s="T196">maːt-tə</ta>
            <ta e="T198" id="Seg_7261" s="T197">omdɨ-n</ta>
            <ta e="T199" id="Seg_7262" s="T198">i</ta>
            <ta e="T200" id="Seg_7263" s="T199">süd-ɨ-r-lʼe</ta>
            <ta e="T201" id="Seg_7264" s="T200">übɨ-r-a-n</ta>
            <ta e="T202" id="Seg_7265" s="T201">a</ta>
            <ta e="T203" id="Seg_7266" s="T202">sət</ta>
            <ta e="T204" id="Seg_7267" s="T203">qɨbanʼaʒa</ta>
            <ta e="T205" id="Seg_7268" s="T204">warka-ɣə</ta>
            <ta e="T206" id="Seg_7269" s="T205">razbojnig-la</ta>
            <ta e="T207" id="Seg_7270" s="T206">čaʒa-tdə</ta>
            <ta e="T208" id="Seg_7271" s="T207">atdɨ-zʼe</ta>
            <ta e="T209" id="Seg_7272" s="T208">i</ta>
            <ta e="T210" id="Seg_7273" s="T209">tʼära-ttə</ta>
            <ta e="T211" id="Seg_7274" s="T210">Qaj</ta>
            <ta e="T212" id="Seg_7275" s="T211">struška-la</ta>
            <ta e="T213" id="Seg_7276" s="T212">köu-da-ttə</ta>
            <ta e="T214" id="Seg_7277" s="T213">küu-ze</ta>
            <ta e="T215" id="Seg_7278" s="T214">čaːʒɨ-da-ttə</ta>
            <ta e="T216" id="Seg_7279" s="T215">qə-n</ta>
            <ta e="T217" id="Seg_7280" s="T216">bar-ɨ-nʼä</ta>
            <ta e="T218" id="Seg_7281" s="T217">praib-et</ta>
            <ta e="T219" id="Seg_7282" s="T218">teb-la</ta>
            <ta e="T220" id="Seg_7283" s="T219">mannɨ-pa-ttə</ta>
            <ta e="T221" id="Seg_7284" s="T220">qaj-da</ta>
            <ta e="T222" id="Seg_7285" s="T221">wattɨ</ta>
            <ta e="T223" id="Seg_7286" s="T222">täb-la</ta>
            <ta e="T224" id="Seg_7287" s="T223">wattɨ-ga</ta>
            <ta e="T225" id="Seg_7288" s="T224">ko-tda-ttə</ta>
            <ta e="T226" id="Seg_7289" s="T225">ugon</ta>
            <ta e="T227" id="Seg_7290" s="T226">me</ta>
            <ta e="T228" id="Seg_7291" s="T227">tau</ta>
            <ta e="T229" id="Seg_7292" s="T228">wattɨ-m</ta>
            <ta e="T230" id="Seg_7293" s="T229">jass</ta>
            <ta e="T231" id="Seg_7294" s="T230">qo-ǯɨr-gu-za-utə</ta>
            <ta e="T232" id="Seg_7295" s="T231">na</ta>
            <ta e="T233" id="Seg_7296" s="T232">watt-o-ga-tdə</ta>
            <ta e="T234" id="Seg_7297" s="T233">na</ta>
            <ta e="T235" id="Seg_7298" s="T234">wat-o-tdə</ta>
            <ta e="T236" id="Seg_7299" s="T235">na</ta>
            <ta e="T237" id="Seg_7300" s="T236">wat-o-tdə</ta>
            <ta e="T238" id="Seg_7301" s="T237">i</ta>
            <ta e="T239" id="Seg_7302" s="T238">uːdur-a-ttə</ta>
            <ta e="T240" id="Seg_7303" s="T239">attaman</ta>
            <ta e="T241" id="Seg_7304" s="T240">tʼärɨ-n</ta>
            <ta e="T242" id="Seg_7305" s="T241">tä</ta>
            <ta e="T243" id="Seg_7306" s="T242">aːmda-ltə</ta>
            <ta e="T244" id="Seg_7307" s="T243">tɨtdɨ-n</ta>
            <ta e="T245" id="Seg_7308" s="T244">a</ta>
            <ta e="T246" id="Seg_7309" s="T245">man</ta>
            <ta e="T247" id="Seg_7310" s="T246">qur-ol-la-n</ta>
            <ta e="T248" id="Seg_7311" s="T247">mannɨ-bɨ-lʼe-u</ta>
            <ta e="T249" id="Seg_7312" s="T248">qaj</ta>
            <ta e="T250" id="Seg_7313" s="T249">ila-tda</ta>
            <ta e="T251" id="Seg_7314" s="T250">tɨtdɨ-n</ta>
            <ta e="T252" id="Seg_7315" s="T251">na</ta>
            <ta e="T253" id="Seg_7316" s="T252">qur-ol-ǯa</ta>
            <ta e="T254" id="Seg_7317" s="T253">tü-a</ta>
            <ta e="T255" id="Seg_7318" s="T254">ser-nɨ-n</ta>
            <ta e="T256" id="Seg_7319" s="T255">maːt-tə</ta>
            <ta e="T257" id="Seg_7320" s="T256">tʼolom</ta>
            <ta e="T258" id="Seg_7321" s="T257">nä-j-ɣum</ta>
            <ta e="T259" id="Seg_7322" s="T258">tʼärɨ-n</ta>
            <ta e="T260" id="Seg_7323" s="T259">tʼolom</ta>
            <ta e="T261" id="Seg_7324" s="T260">tʼolom</ta>
            <ta e="T262" id="Seg_7325" s="T261">stul</ta>
            <ta e="T263" id="Seg_7326" s="T262">čeče-lʼe</ta>
            <ta e="T264" id="Seg_7327" s="T263">me-ʒa-lǯɨ-t</ta>
            <ta e="T265" id="Seg_7328" s="T264">tʼärɨ-n</ta>
            <ta e="T266" id="Seg_7329" s="T265">omda-k</ta>
            <ta e="T267" id="Seg_7330" s="T266">tɨkga</ta>
            <ta e="T268" id="Seg_7331" s="T267">a</ta>
            <ta e="T269" id="Seg_7332" s="T268">täp</ta>
            <ta e="T270" id="Seg_7333" s="T269">omdɨ-n</ta>
            <ta e="T271" id="Seg_7334" s="T270">kulupbɨ-lʼe</ta>
            <ta e="T272" id="Seg_7335" s="T271">na</ta>
            <ta e="T273" id="Seg_7336" s="T272">übɨ-r-ɨ-tda-ɣ</ta>
            <ta e="T274" id="Seg_7337" s="T273">ataman</ta>
            <ta e="T275" id="Seg_7338" s="T274">tʼärɨ-n</ta>
            <ta e="T276" id="Seg_7339" s="T275">Tan</ta>
            <ta e="T277" id="Seg_7340" s="T276">mekga</ta>
            <ta e="T278" id="Seg_7341" s="T277">tɨbɨtdɨ-lʼe-t</ta>
            <ta e="T279" id="Seg_7342" s="T278">no</ta>
            <ta e="T280" id="Seg_7343" s="T279">man-nan</ta>
            <ta e="T281" id="Seg_7344" s="T280">ära-u</ta>
            <ta e="T282" id="Seg_7345" s="T281">or-se</ta>
            <ta e="T283" id="Seg_7346" s="T282">täp</ta>
            <ta e="T284" id="Seg_7347" s="T283">tastɨ</ta>
            <ta e="T285" id="Seg_7348" s="T284">qwa-čɨ-t</ta>
            <ta e="T286" id="Seg_7349" s="T285">i</ta>
            <ta e="T287" id="Seg_7350" s="T286">mazɨm</ta>
            <ta e="T288" id="Seg_7351" s="T287">qwa-čɨ-t</ta>
            <ta e="T289" id="Seg_7352" s="T288">a</ta>
            <ta e="T290" id="Seg_7353" s="T289">man</ta>
            <ta e="T291" id="Seg_7354" s="T290">tekga</ta>
            <ta e="T292" id="Seg_7355" s="T291">qudugo</ta>
            <ta e="T293" id="Seg_7356" s="T292">me-nnɨ-ǯa-n</ta>
            <ta e="T294" id="Seg_7357" s="T293">optə</ta>
            <ta e="T295" id="Seg_7358" s="T294">je-nnɨš</ta>
            <ta e="T296" id="Seg_7359" s="T295">i</ta>
            <ta e="T299" id="Seg_7360" s="T298">täp</ta>
            <ta e="T300" id="Seg_7361" s="T299">tau</ta>
            <ta e="T301" id="Seg_7362" s="T300">kudɨgo-m</ta>
            <ta e="T302" id="Seg_7363" s="T301">as</ta>
            <ta e="T303" id="Seg_7364" s="T302">lakč-eǯi-t</ta>
            <ta e="T304" id="Seg_7365" s="T303">täp</ta>
            <ta e="T305" id="Seg_7366" s="T304">na</ta>
            <ta e="T306" id="Seg_7367" s="T305">tü-nnɨš</ta>
            <ta e="T307" id="Seg_7368" s="T306">üdo-mɨ-ɣɨn</ta>
            <ta e="T308" id="Seg_7369" s="T307">tat</ta>
            <ta e="T309" id="Seg_7370" s="T308">tʼära-k</ta>
            <ta e="T310" id="Seg_7371" s="T309">qozɨr-čɨ-la-j</ta>
            <ta e="T311" id="Seg_7372" s="T310">tab-nä</ta>
            <ta e="T312" id="Seg_7373" s="T311">tʼära-k</ta>
            <ta e="T313" id="Seg_7374" s="T312">qut</ta>
            <ta e="T314" id="Seg_7375" s="T313">turak-ɨ-n</ta>
            <ta e="T315" id="Seg_7376" s="T314">qalʼ-eǯɨ-n</ta>
            <ta e="T316" id="Seg_7377" s="T315">ud-la-m-də</ta>
            <ta e="T317" id="Seg_7378" s="T316">moɣunä</ta>
            <ta e="T318" id="Seg_7379" s="T317">saru-ku</ta>
            <ta e="T319" id="Seg_7380" s="T318">a</ta>
            <ta e="T320" id="Seg_7381" s="T319">tat</ta>
            <ta e="T321" id="Seg_7382" s="T320">täb-ɨ-m</ta>
            <ta e="T322" id="Seg_7383" s="T321">turak-ɨ-n</ta>
            <ta e="T323" id="Seg_7384" s="T322">qwäzʼ-et</ta>
            <ta e="T324" id="Seg_7385" s="T323">ud-la-m-də</ta>
            <ta e="T325" id="Seg_7386" s="T324">moɣunä</ta>
            <ta e="T326" id="Seg_7387" s="T325">saːrʼ-et</ta>
            <ta e="T327" id="Seg_7388" s="T326">täp</ta>
            <ta e="T328" id="Seg_7389" s="T327">tau</ta>
            <ta e="T329" id="Seg_7390" s="T328">qudugo-m</ta>
            <ta e="T330" id="Seg_7391" s="T329">as</ta>
            <ta e="T331" id="Seg_7392" s="T330">lakč-eǯi-n</ta>
            <ta e="T332" id="Seg_7393" s="T331">täp</ta>
            <ta e="T333" id="Seg_7394" s="T332">jeʒlʼi</ta>
            <ta e="T334" id="Seg_7395" s="T333">ass</ta>
            <ta e="T335" id="Seg_7396" s="T334">laqčɨ-t</ta>
            <ta e="T336" id="Seg_7397" s="T335">a</ta>
            <ta e="T337" id="Seg_7398" s="T336">tat</ta>
            <ta e="T338" id="Seg_7399" s="T337">ponä</ta>
            <ta e="T339" id="Seg_7400" s="T338">čatǯa-k</ta>
            <ta e="T340" id="Seg_7401" s="T339">mazɨm</ta>
            <ta e="T341" id="Seg_7402" s="T340">lakgo-lʼ-gə</ta>
            <ta e="T342" id="Seg_7403" s="T341">man</ta>
            <ta e="T343" id="Seg_7404" s="T342">tü-tǯa-n</ta>
            <ta e="T344" id="Seg_7405" s="T343">täb-ɨ-m</ta>
            <ta e="T345" id="Seg_7406" s="T344">i</ta>
            <ta e="T346" id="Seg_7407" s="T345">qwat-ča-u</ta>
            <ta e="T347" id="Seg_7408" s="T346">ta-ze</ta>
            <ta e="T348" id="Seg_7409" s="T347">ilʼ-etǯa-j</ta>
            <ta e="T349" id="Seg_7410" s="T348">ära-t</ta>
            <ta e="T350" id="Seg_7411" s="T349">tü-a</ta>
            <ta e="T351" id="Seg_7412" s="T350">täp</ta>
            <ta e="T352" id="Seg_7413" s="T351">täb-ɨ-m</ta>
            <ta e="T353" id="Seg_7414" s="T352">aps-tɨ-t</ta>
            <ta e="T354" id="Seg_7415" s="T353">tʼärɨ-n</ta>
            <ta e="T355" id="Seg_7416" s="T354">qozɨr-čɨ-la-j</ta>
            <ta e="T356" id="Seg_7417" s="T355">ära-t</ta>
            <ta e="T357" id="Seg_7418" s="T356">tʼärɨ-n</ta>
            <ta e="T358" id="Seg_7419" s="T357">Man</ta>
            <ta e="T359" id="Seg_7420" s="T358">nunɨ-tʼi-pba-n</ta>
            <ta e="T360" id="Seg_7421" s="T359">qotdu-gu</ta>
            <ta e="T361" id="Seg_7422" s="T360">nadə</ta>
            <ta e="T362" id="Seg_7423" s="T361">nu</ta>
            <ta e="T363" id="Seg_7424" s="T362">dawaj</ta>
            <ta e="T364" id="Seg_7425" s="T363">qozɨr-čɨ-la-j</ta>
            <ta e="T365" id="Seg_7426" s="T364">ass</ta>
            <ta e="T366" id="Seg_7427" s="T365">qutdɨ-n</ta>
            <ta e="T367" id="Seg_7428" s="T366">ato</ta>
            <ta e="T368" id="Seg_7429" s="T367">onän</ta>
            <ta e="T369" id="Seg_7430" s="T368">amda-n</ta>
            <ta e="T370" id="Seg_7431" s="T369">i</ta>
            <ta e="T371" id="Seg_7432" s="T370">skušna-n</ta>
            <ta e="T372" id="Seg_7433" s="T371">mekga</ta>
            <ta e="T373" id="Seg_7434" s="T372">je-n</ta>
            <ta e="T374" id="Seg_7435" s="T373">nu</ta>
            <ta e="T375" id="Seg_7436" s="T374">i</ta>
            <ta e="T376" id="Seg_7437" s="T375">omda-ɣə</ta>
            <ta e="T377" id="Seg_7438" s="T376">qozɨr-čɨ-lʼe</ta>
            <ta e="T378" id="Seg_7439" s="T377">qut</ta>
            <ta e="T379" id="Seg_7440" s="T378">turaq-ɨ-n</ta>
            <ta e="T380" id="Seg_7441" s="T379">qalʼ-eǯɨ-n</ta>
            <ta e="T381" id="Seg_7442" s="T380">ud-la-m-də</ta>
            <ta e="T382" id="Seg_7443" s="T381">sare-lǯu-gu</ta>
            <ta e="T383" id="Seg_7444" s="T382">moɣunä</ta>
            <ta e="T384" id="Seg_7445" s="T383">ära-t</ta>
            <ta e="T385" id="Seg_7446" s="T384">turaq-ɨ-n</ta>
            <ta e="T386" id="Seg_7447" s="T385">qalɨ-n</ta>
            <ta e="T387" id="Seg_7448" s="T386">ud-la-m-də</ta>
            <ta e="T388" id="Seg_7449" s="T387">nadə</ta>
            <ta e="T389" id="Seg_7450" s="T388">sare-lǯu-gu</ta>
            <ta e="T390" id="Seg_7451" s="T389">udo-m-də</ta>
            <ta e="T391" id="Seg_7452" s="T390">nadə</ta>
            <ta e="T392" id="Seg_7453" s="T391">saru-gu</ta>
            <ta e="T393" id="Seg_7454" s="T392">moɣunä</ta>
            <ta e="T394" id="Seg_7455" s="T393">a</ta>
            <ta e="T395" id="Seg_7456" s="T394">jeʒlʼe</ta>
            <ta e="T396" id="Seg_7457" s="T395">man</ta>
            <ta e="T397" id="Seg_7458" s="T396">qalɨ-nä-n</ta>
            <ta e="T398" id="Seg_7459" s="T397">turaq-ɨ-n</ta>
            <ta e="T399" id="Seg_7460" s="T398">tat</ta>
            <ta e="T400" id="Seg_7461" s="T399">bə</ta>
            <ta e="T401" id="Seg_7462" s="T400">mekga</ta>
            <ta e="T402" id="Seg_7463" s="T401">ud-la-u</ta>
            <ta e="T403" id="Seg_7464" s="T402">zare-lǯɨ-nä-l</ta>
            <ta e="T404" id="Seg_7465" s="T403">a</ta>
            <ta e="T405" id="Seg_7466" s="T404">täper</ta>
            <ta e="T406" id="Seg_7467" s="T405">man</ta>
            <ta e="T407" id="Seg_7468" s="T406">tʼekga</ta>
            <ta e="T408" id="Seg_7469" s="T407">udo-m-də</ta>
            <ta e="T409" id="Seg_7470" s="T408">sar-etǯa-u</ta>
            <ta e="T410" id="Seg_7471" s="T409">ära-t</ta>
            <ta e="T411" id="Seg_7472" s="T410">tʼärɨ-n</ta>
            <ta e="T412" id="Seg_7473" s="T411">sar-ät</ta>
            <ta e="T413" id="Seg_7474" s="T412">täp</ta>
            <ta e="T414" id="Seg_7475" s="T413">saːrä-t</ta>
            <ta e="T415" id="Seg_7476" s="T414">ära-t</ta>
            <ta e="T416" id="Seg_7477" s="T415">kak</ta>
            <ta e="T417" id="Seg_7478" s="T416">turugu-lǯɨ-t</ta>
            <ta e="T418" id="Seg_7479" s="T417">qudɨgo-la</ta>
            <ta e="T419" id="Seg_7480" s="T418">wes</ta>
            <ta e="T420" id="Seg_7481" s="T419">laqče-la-ttə</ta>
            <ta e="T421" id="Seg_7482" s="T420">no</ta>
            <ta e="T422" id="Seg_7483" s="T421">i</ta>
            <ta e="T423" id="Seg_7484" s="T422">qotdɨ-gu</ta>
            <ta e="T424" id="Seg_7485" s="T423">quʒa-na-ɣɨ</ta>
            <ta e="T425" id="Seg_7486" s="T424">qarʼe-mɨ-ɣɨn</ta>
            <ta e="T426" id="Seg_7487" s="T425">wazɨ-n</ta>
            <ta e="T427" id="Seg_7488" s="T426">nä-j-ɣum</ta>
            <ta e="T428" id="Seg_7489" s="T427">wes</ta>
            <ta e="T429" id="Seg_7490" s="T428">pot-qɨ-nɨ-t</ta>
            <ta e="T430" id="Seg_7491" s="T429">wadʼi-la-m</ta>
            <ta e="T431" id="Seg_7492" s="T430">qaj-la-m</ta>
            <ta e="T432" id="Seg_7493" s="T431">ära-m-də</ta>
            <ta e="T433" id="Seg_7494" s="T432">aps-tɨ-t</ta>
            <ta e="T434" id="Seg_7495" s="T433">ära-t</ta>
            <ta e="T435" id="Seg_7496" s="T434">madʼ-o-t</ta>
            <ta e="T436" id="Seg_7497" s="T435">qwan-nɨ-n</ta>
            <ta e="T437" id="Seg_7498" s="T436">nä-j-ɣum</ta>
            <ta e="T438" id="Seg_7499" s="T437">ponä</ta>
            <ta e="T439" id="Seg_7500" s="T438">sapɨsi-n</ta>
            <ta e="T440" id="Seg_7501" s="T439">i</ta>
            <ta e="T441" id="Seg_7502" s="T440">na</ta>
            <ta e="T442" id="Seg_7503" s="T441">qaːr-olʼ-dʼä</ta>
            <ta e="T443" id="Seg_7504" s="T442">tʼü-kkɨ</ta>
            <ta e="T444" id="Seg_7505" s="T443">ära-u</ta>
            <ta e="T445" id="Seg_7506" s="T444">qwan-ba</ta>
            <ta e="T446" id="Seg_7507" s="T445">ataman</ta>
            <ta e="T447" id="Seg_7508" s="T446">na</ta>
            <ta e="T448" id="Seg_7509" s="T447">tü-tda</ta>
            <ta e="T449" id="Seg_7510" s="T448">no</ta>
            <ta e="T450" id="Seg_7511" s="T449">qaj</ta>
            <ta e="T451" id="Seg_7512" s="T450">qozɨr-če-za-lʼe</ta>
            <ta e="T452" id="Seg_7513" s="T451">qozɨr-če-za-j</ta>
            <ta e="T453" id="Seg_7514" s="T452">man</ta>
            <ta e="T454" id="Seg_7515" s="T453">tʼekga</ta>
            <ta e="T455" id="Seg_7516" s="T454">tʼärɨ-za-n</ta>
            <ta e="T456" id="Seg_7517" s="T455">täp</ta>
            <ta e="T457" id="Seg_7518" s="T456">or-se</ta>
            <ta e="T458" id="Seg_7519" s="T457">je-n</ta>
            <ta e="T459" id="Seg_7520" s="T458">na</ta>
            <ta e="T460" id="Seg_7521" s="T459">qudɨgo-la-m</ta>
            <ta e="T461" id="Seg_7522" s="T460">wes</ta>
            <ta e="T462" id="Seg_7523" s="T461">lakče-lǯɨ-t</ta>
            <ta e="T463" id="Seg_7524" s="T462">a</ta>
            <ta e="T464" id="Seg_7525" s="T463">tam-dʼel</ta>
            <ta e="T465" id="Seg_7526" s="T464">man</ta>
            <ta e="T466" id="Seg_7527" s="T465">tʼekga</ta>
            <ta e="T467" id="Seg_7528" s="T466">me-ǯa-u</ta>
            <ta e="T468" id="Seg_7529" s="T467">optɨ</ta>
            <ta e="T469" id="Seg_7530" s="T468">qudɨgo</ta>
            <ta e="T470" id="Seg_7531" s="T469">me-nnɨ-ǯa-n</ta>
            <ta e="T471" id="Seg_7532" s="T470">üdə-mɨ-ɣɨn</ta>
            <ta e="T472" id="Seg_7533" s="T471">na</ta>
            <ta e="T473" id="Seg_7534" s="T472">tü-nnɨš</ta>
            <ta e="T474" id="Seg_7535" s="T473">aj</ta>
            <ta e="T475" id="Seg_7536" s="T474">tʼära-k</ta>
            <ta e="T476" id="Seg_7537" s="T475">qozɨr-čɨ-la-j</ta>
            <ta e="T477" id="Seg_7538" s="T476">qut</ta>
            <ta e="T478" id="Seg_7539" s="T477">turak-ɨ-n</ta>
            <ta e="T479" id="Seg_7540" s="T478">qalʼ-eǯɨ-n</ta>
            <ta e="T480" id="Seg_7541" s="T479">ud-la-m-də</ta>
            <ta e="T481" id="Seg_7542" s="T480">moɣunä</ta>
            <ta e="T482" id="Seg_7543" s="T481">saːru-ku</ta>
            <ta e="T483" id="Seg_7544" s="T482">nä-j-ɣum</ta>
            <ta e="T484" id="Seg_7545" s="T483">täb-ɨ-m</ta>
            <ta e="T485" id="Seg_7546" s="T484">aps-tɨ-pbaː-t</ta>
            <ta e="T486" id="Seg_7547" s="T485">araka-zʼe</ta>
            <ta e="T487" id="Seg_7548" s="T486">är-či-ba-t</ta>
            <ta e="T488" id="Seg_7549" s="T487">araqɨ-la</ta>
            <ta e="T489" id="Seg_7550" s="T488">üt-tɨ-dɨ</ta>
            <ta e="T490" id="Seg_7551" s="T489">täbe-ɣum</ta>
            <ta e="T491" id="Seg_7552" s="T490">qwan-nɨ-n</ta>
            <ta e="T492" id="Seg_7553" s="T491">ära-t</ta>
            <ta e="T493" id="Seg_7554" s="T492">tü-a</ta>
            <ta e="T494" id="Seg_7555" s="T493">surum-la-m</ta>
            <ta e="T495" id="Seg_7556" s="T494">koːcʼe-n</ta>
            <ta e="T496" id="Seg_7557" s="T495">qwat-pa-t</ta>
            <ta e="T497" id="Seg_7558" s="T496">i</ta>
            <ta e="T498" id="Seg_7559" s="T497">pekqɨ</ta>
            <ta e="T499" id="Seg_7560" s="T498">qwat-pa-dɨ-t</ta>
            <ta e="T500" id="Seg_7561" s="T499">täp</ta>
            <ta e="T501" id="Seg_7562" s="T500">täb-ɨ-m</ta>
            <ta e="T502" id="Seg_7563" s="T501">aps-tɨ-t</ta>
            <ta e="T503" id="Seg_7564" s="T502">tʼärɨ-n</ta>
            <ta e="T504" id="Seg_7565" s="T503">qozɨr-čɨ-la-j</ta>
            <ta e="T505" id="Seg_7566" s="T504">ära-t</ta>
            <ta e="T506" id="Seg_7567" s="T505">tʼärɨ-n</ta>
            <ta e="T507" id="Seg_7568" s="T506">Man</ta>
            <ta e="T508" id="Seg_7569" s="T507">nuːnɨ-dʼi-pbaː-n</ta>
            <ta e="T509" id="Seg_7570" s="T508">paja-t</ta>
            <ta e="T510" id="Seg_7571" s="T509">tʼärɨ-n</ta>
            <ta e="T511" id="Seg_7572" s="T510">qozɨr-čɨ-la-j</ta>
            <ta e="T514" id="Seg_7573" s="T513">qudə</ta>
            <ta e="T515" id="Seg_7574" s="T514">qut</ta>
            <ta e="T516" id="Seg_7575" s="T515">turak-ɨ-n</ta>
            <ta e="T517" id="Seg_7576" s="T516">qalʼ-eǯɨ-n</ta>
            <ta e="T518" id="Seg_7577" s="T517">ud-ɨ-m-də</ta>
            <ta e="T519" id="Seg_7578" s="T518">moɣunä</ta>
            <ta e="T520" id="Seg_7579" s="T519">saːru-gu</ta>
            <ta e="T521" id="Seg_7580" s="T520">ješlʼi</ta>
            <ta e="T522" id="Seg_7581" s="T521">man</ta>
            <ta e="T523" id="Seg_7582" s="T522">qala-n</ta>
            <ta e="T524" id="Seg_7583" s="T523">duraq-ɨ-n</ta>
            <ta e="T525" id="Seg_7584" s="T524">man</ta>
            <ta e="T526" id="Seg_7585" s="T525">ud-o-u</ta>
            <ta e="T527" id="Seg_7586" s="T526">saːra-q</ta>
            <ta e="T528" id="Seg_7587" s="T527">sar-eǯa-l</ta>
            <ta e="T529" id="Seg_7588" s="T528">täb-ɨ-staɣə</ta>
            <ta e="T530" id="Seg_7589" s="T529">omda-ɣə</ta>
            <ta e="T531" id="Seg_7590" s="T530">qozɨr-či-lʼe</ta>
            <ta e="T532" id="Seg_7591" s="T531">ära-t</ta>
            <ta e="T533" id="Seg_7592" s="T532">turaq-ɨ-n</ta>
            <ta e="T534" id="Seg_7593" s="T533">qalɨ-n</ta>
            <ta e="T535" id="Seg_7594" s="T534">nu</ta>
            <ta e="T536" id="Seg_7595" s="T535">täper</ta>
            <ta e="T537" id="Seg_7596" s="T536">udo-m-də</ta>
            <ta e="T538" id="Seg_7597" s="T537">saːr-eǯa-u</ta>
            <ta e="T539" id="Seg_7598" s="T538">täp</ta>
            <ta e="T540" id="Seg_7599" s="T539">saːrɨ-t</ta>
            <ta e="T541" id="Seg_7600" s="T540">ud-ɨ-m-də</ta>
            <ta e="T542" id="Seg_7601" s="T541">täp</ta>
            <ta e="T543" id="Seg_7602" s="T542">kak</ta>
            <ta e="T544" id="Seg_7603" s="T543">turgu-lǯɨ-tda</ta>
            <ta e="T545" id="Seg_7604" s="T544">qudɨgo</ta>
            <ta e="T546" id="Seg_7605" s="T545">laqčɨ-di-n</ta>
            <ta e="T547" id="Seg_7606" s="T546">quʒan-na-ɣə</ta>
            <ta e="T548" id="Seg_7607" s="T547">qotdu-ɣɨ</ta>
            <ta e="T549" id="Seg_7608" s="T548">qarʼe-mɨ-ɣɨn</ta>
            <ta e="T550" id="Seg_7609" s="T549">waza-ɣə</ta>
            <ta e="T551" id="Seg_7610" s="T550">nä-j-ɣum</ta>
            <ta e="T552" id="Seg_7611" s="T551">ära-m-də</ta>
            <ta e="T553" id="Seg_7612" s="T552">aps-tɨ-t</ta>
            <ta e="T554" id="Seg_7613" s="T553">ära-t</ta>
            <ta e="T555" id="Seg_7614" s="T554">au-r-nɨ-n</ta>
            <ta e="T556" id="Seg_7615" s="T555">i</ta>
            <ta e="T557" id="Seg_7616" s="T556">madʼ-o-t</ta>
            <ta e="T558" id="Seg_7617" s="T557">na</ta>
            <ta e="T559" id="Seg_7618" s="T558">qwat-da</ta>
            <ta e="T560" id="Seg_7619" s="T559">nä-i-ɣum</ta>
            <ta e="T561" id="Seg_7620" s="T560">ponä</ta>
            <ta e="T562" id="Seg_7621" s="T561">čaːǯɨ-n</ta>
            <ta e="T563" id="Seg_7622" s="T562">i</ta>
            <ta e="T564" id="Seg_7623" s="T563">na</ta>
            <ta e="T565" id="Seg_7624" s="T564">qaːr-onʼ-e-n</ta>
            <ta e="T566" id="Seg_7625" s="T565">tü-kkɨ</ta>
            <ta e="T567" id="Seg_7626" s="T566">ära-u</ta>
            <ta e="T568" id="Seg_7627" s="T567">qwan-ba</ta>
            <ta e="T569" id="Seg_7628" s="T568">razbojnik</ta>
            <ta e="T570" id="Seg_7629" s="T569">tü-wa</ta>
            <ta e="T571" id="Seg_7630" s="T570">nä-i-ɣum</ta>
            <ta e="T572" id="Seg_7631" s="T571">tʼärɨ-n</ta>
            <ta e="T573" id="Seg_7632" s="T572">qudɨgo-la-m</ta>
            <ta e="T574" id="Seg_7633" s="T573">ära-u</ta>
            <ta e="T575" id="Seg_7634" s="T574">wes</ta>
            <ta e="T576" id="Seg_7635" s="T575">lakče-lǯɨ-t</ta>
            <ta e="T577" id="Seg_7636" s="T576">täp</ta>
            <ta e="T578" id="Seg_7637" s="T577">or-se</ta>
            <ta e="T579" id="Seg_7638" s="T578">je-n</ta>
            <ta e="T580" id="Seg_7639" s="T579">a</ta>
            <ta e="T581" id="Seg_7640" s="T580">man</ta>
            <ta e="T582" id="Seg_7641" s="T581">tam-dʼel</ta>
            <ta e="T583" id="Seg_7642" s="T582">tekga</ta>
            <ta e="T584" id="Seg_7643" s="T583">me-ǯa-u</ta>
            <ta e="T585" id="Seg_7644" s="T584">kudɨgo</ta>
            <ta e="T586" id="Seg_7645" s="T585">optɨ</ta>
            <ta e="T587" id="Seg_7646" s="T586">i</ta>
            <ta e="T590" id="Seg_7647" s="T589">okkɨr-mɨ-t</ta>
            <ta e="T591" id="Seg_7648" s="T590">tamkɨl-ba</ta>
            <ta e="T592" id="Seg_7649" s="T591">üːdɨ-mɨ-ɣɨn</ta>
            <ta e="T593" id="Seg_7650" s="T592">na</ta>
            <ta e="T594" id="Seg_7651" s="T593">tü-nnɨš</ta>
            <ta e="T595" id="Seg_7652" s="T594">tat</ta>
            <ta e="T596" id="Seg_7653" s="T595">aj</ta>
            <ta e="T597" id="Seg_7654" s="T596">tʼära-k</ta>
            <ta e="T598" id="Seg_7655" s="T597">qozɨr-ču-ɣu</ta>
            <ta e="T599" id="Seg_7656" s="T598">qozɨr-či-la-j</ta>
            <ta e="T600" id="Seg_7657" s="T599">qut</ta>
            <ta e="T601" id="Seg_7658" s="T600">turak-ɨ-n</ta>
            <ta e="T602" id="Seg_7659" s="T601">kalʼ-eǯɨ-n</ta>
            <ta e="T603" id="Seg_7660" s="T602">to</ta>
            <ta e="T604" id="Seg_7661" s="T603">ud-ɨ-m-də</ta>
            <ta e="T605" id="Seg_7662" s="T604">moɣunä</ta>
            <ta e="T606" id="Seg_7663" s="T605">saːru-gu</ta>
            <ta e="T607" id="Seg_7664" s="T606">ära</ta>
            <ta e="T608" id="Seg_7665" s="T607">tü-a</ta>
            <ta e="T609" id="Seg_7666" s="T608">täp</ta>
            <ta e="T610" id="Seg_7667" s="T609">täb-ɨ-m</ta>
            <ta e="T611" id="Seg_7668" s="T610">aps-tɨ-t</ta>
            <ta e="T612" id="Seg_7669" s="T611">tʼärɨ-n</ta>
            <ta e="T613" id="Seg_7670" s="T612">qozɨr-čɨ-la-j</ta>
            <ta e="T614" id="Seg_7671" s="T613">ära-t</ta>
            <ta e="T615" id="Seg_7672" s="T614">tʼärɨ-n</ta>
            <ta e="T616" id="Seg_7673" s="T615">Man</ta>
            <ta e="T617" id="Seg_7674" s="T616">nunɨ-dʼzʼi-pa-n</ta>
            <ta e="T618" id="Seg_7675" s="T617">nadə</ta>
            <ta e="T619" id="Seg_7676" s="T618">qotdɨ-ɣu</ta>
            <ta e="T620" id="Seg_7677" s="T619">no</ta>
            <ta e="T621" id="Seg_7678" s="T620">qozɨr-čɨ-la-j</ta>
            <ta e="T622" id="Seg_7679" s="T621">qozɨr-čɨ-la-j</ta>
            <ta e="T623" id="Seg_7680" s="T622">xotʼ</ta>
            <ta e="T624" id="Seg_7681" s="T623">okkɨr-ɨ-n</ta>
            <ta e="T625" id="Seg_7682" s="T624">qut</ta>
            <ta e="T626" id="Seg_7683" s="T625">turak-ɨ-n</ta>
            <ta e="T627" id="Seg_7684" s="T626">kalʼ-eǯɨ-n</ta>
            <ta e="T628" id="Seg_7685" s="T627">ud-ɨ-m-də</ta>
            <ta e="T629" id="Seg_7686" s="T628">moɣunä</ta>
            <ta e="T630" id="Seg_7687" s="T629">saaru-ku</ta>
            <ta e="T631" id="Seg_7688" s="T630">jeʒlʼi</ta>
            <ta e="T632" id="Seg_7689" s="T631">man</ta>
            <ta e="T633" id="Seg_7690" s="T632">kalʼ-eǯa-n</ta>
            <ta e="T634" id="Seg_7691" s="T633">man</ta>
            <ta e="T635" id="Seg_7692" s="T634">ud-o-u</ta>
            <ta e="T636" id="Seg_7693" s="T635">saːrʼ-et</ta>
            <ta e="T637" id="Seg_7694" s="T636">moɣunä</ta>
            <ta e="T638" id="Seg_7695" s="T637">i</ta>
            <ta e="T639" id="Seg_7696" s="T638">oːmda-ɣə</ta>
            <ta e="T640" id="Seg_7697" s="T639">qozɨr-čɨ-lʼe</ta>
            <ta e="T641" id="Seg_7698" s="T640">ära-m-də</ta>
            <ta e="T642" id="Seg_7699" s="T641">turag-ɨ-w-lʼe</ta>
            <ta e="T643" id="Seg_7700" s="T642">qwɛdʼi-t</ta>
            <ta e="T644" id="Seg_7701" s="T643">ud-ɨ-m-də</ta>
            <ta e="T645" id="Seg_7702" s="T644">moɣunä</ta>
            <ta e="T646" id="Seg_7703" s="T645">saːrɨ-t</ta>
            <ta e="T647" id="Seg_7704" s="T646">täp</ta>
            <ta e="T648" id="Seg_7705" s="T647">kak</ta>
            <ta e="T649" id="Seg_7706" s="T648">turgu-lǯɨ-tda</ta>
            <ta e="T650" id="Seg_7707" s="T649">qudəko-la-m</ta>
            <ta e="T651" id="Seg_7708" s="T650">jass</ta>
            <ta e="T652" id="Seg_7709" s="T651">laqčɨ-t</ta>
            <ta e="T653" id="Seg_7710" s="T652">pajä-tdä-nä</ta>
            <ta e="T654" id="Seg_7711" s="T653">tʼärɨ-n</ta>
            <ta e="T655" id="Seg_7712" s="T654">tʼik-et</ta>
            <ta e="T656" id="Seg_7713" s="T655">ud-o-u</ta>
            <ta e="T657" id="Seg_7714" s="T656">pajä-t</ta>
            <ta e="T658" id="Seg_7715" s="T657">tʼärɨ-n</ta>
            <ta e="T659" id="Seg_7716" s="T658">lakč-et</ta>
            <ta e="T660" id="Seg_7717" s="T659">täp</ta>
            <ta e="T661" id="Seg_7718" s="T660">aj</ta>
            <ta e="T662" id="Seg_7719" s="T661">turgu-lǯɨ-n</ta>
            <ta e="T663" id="Seg_7720" s="T662">jass</ta>
            <ta e="T664" id="Seg_7721" s="T663">lakčɨ-t</ta>
            <ta e="T665" id="Seg_7722" s="T664">tʼärɨ-n</ta>
            <ta e="T666" id="Seg_7723" s="T665">tʼik-et</ta>
            <ta e="T667" id="Seg_7724" s="T666">ass</ta>
            <ta e="T668" id="Seg_7725" s="T667">tʼik-eǯa-u</ta>
            <ta e="T669" id="Seg_7726" s="T668">ponä</ta>
            <ta e="T670" id="Seg_7727" s="T669">sappɨse-lʼe</ta>
            <ta e="T671" id="Seg_7728" s="T670">qwan-nɨ-n</ta>
            <ta e="T672" id="Seg_7729" s="T671">i</ta>
            <ta e="T673" id="Seg_7730" s="T672">na</ta>
            <ta e="T674" id="Seg_7731" s="T673">lakgolʼ-dʼä</ta>
            <ta e="T675" id="Seg_7732" s="T674">tü-kkɨ</ta>
            <ta e="T676" id="Seg_7733" s="T675">i</ta>
            <ta e="T677" id="Seg_7734" s="T676">na</ta>
            <ta e="T678" id="Seg_7735" s="T677">razbojnik</ta>
            <ta e="T679" id="Seg_7736" s="T678">na</ta>
            <ta e="T680" id="Seg_7737" s="T679">tü-tda</ta>
            <ta e="T681" id="Seg_7738" s="T680">tʼärɨ-n</ta>
            <ta e="T682" id="Seg_7739" s="T681">ära-u</ta>
            <ta e="T683" id="Seg_7740" s="T682">qudogo-m</ta>
            <ta e="T684" id="Seg_7741" s="T683">ass</ta>
            <ta e="T685" id="Seg_7742" s="T684">lakčɨ-t</ta>
            <ta e="T686" id="Seg_7743" s="T685">ser-lʼe</ta>
            <ta e="T687" id="Seg_7744" s="T686">qwal-la-j</ta>
            <ta e="T688" id="Seg_7745" s="T687">i</ta>
            <ta e="T689" id="Seg_7746" s="T688">mat-tə</ta>
            <ta e="T690" id="Seg_7747" s="T689">ser-na-ɣə</ta>
            <ta e="T691" id="Seg_7748" s="T690">razbojnik</ta>
            <ta e="T692" id="Seg_7749" s="T691">tʼärɨ-n</ta>
            <ta e="T693" id="Seg_7750" s="T692">qar-dʼe-n</ta>
            <ta e="T694" id="Seg_7751" s="T693">qarʼe-mɨ-ɣɨn</ta>
            <ta e="T695" id="Seg_7752" s="T694">qwat-ča-j</ta>
            <ta e="T696" id="Seg_7753" s="T695">tulǯ-ät</ta>
            <ta e="T697" id="Seg_7754" s="T696">wʼädra-n</ta>
            <ta e="T698" id="Seg_7755" s="T697">tʼiːr</ta>
            <ta e="T699" id="Seg_7756" s="T698">saːq</ta>
            <ta e="T700" id="Seg_7757" s="T699">täb-ɨ-m</ta>
            <ta e="T701" id="Seg_7758" s="T700">saːɣ-ɨ-n</ta>
            <ta e="T702" id="Seg_7759" s="T701">par-o-tdə</ta>
            <ta e="T703" id="Seg_7760" s="T702">puləsej-la-tdɨ-ze</ta>
            <ta e="T704" id="Seg_7761" s="T703">saːɣ-o-t</ta>
            <ta e="T705" id="Seg_7762" s="T704">omdɨ-lǯ-eǯa-j</ta>
            <ta e="T706" id="Seg_7763" s="T705">qarʼe-mə-ttə</ta>
            <ta e="T707" id="Seg_7764" s="T706">aːmdə</ta>
            <ta e="T708" id="Seg_7765" s="T707">qarʼe-mɨ-ɣɨn</ta>
            <ta e="T709" id="Seg_7766" s="T708">qwat-ča-j</ta>
            <ta e="T710" id="Seg_7767" s="T709">nä-j-ɣum</ta>
            <ta e="T711" id="Seg_7768" s="T710">watdo-ɣɨt</ta>
            <ta e="T712" id="Seg_7769" s="T711">tü-lʼe</ta>
            <ta e="T713" id="Seg_7770" s="T712">na</ta>
            <ta e="T714" id="Seg_7771" s="T713">söːzɨ-tdə-t</ta>
            <ta e="T715" id="Seg_7772" s="T714">Tan</ta>
            <ta e="T716" id="Seg_7773" s="T715">tʼärɨ-ku-za-t</ta>
            <ta e="T717" id="Seg_7774" s="T716">što</ta>
            <ta e="T718" id="Seg_7775" s="T717">man</ta>
            <ta e="T719" id="Seg_7776" s="T718">or-se</ta>
            <ta e="T720" id="Seg_7777" s="T719">je-wa-n</ta>
            <ta e="T721" id="Seg_7778" s="T720">a</ta>
            <ta e="T722" id="Seg_7779" s="T721">qaj-no</ta>
            <ta e="T723" id="Seg_7780" s="T722">qudɨgo-m</ta>
            <ta e="T724" id="Seg_7781" s="T723">ass</ta>
            <ta e="T725" id="Seg_7782" s="T724">laqč-eǯa-l</ta>
            <ta e="T726" id="Seg_7783" s="T725">tapär</ta>
            <ta e="T727" id="Seg_7784" s="T726">qarʼe-mɨ-ɣɨn</ta>
            <ta e="T728" id="Seg_7785" s="T727">tastɨ</ta>
            <ta e="T729" id="Seg_7786" s="T728">qwat-ča-j</ta>
            <ta e="T730" id="Seg_7787" s="T729">nä-j-ɣum</ta>
            <ta e="T731" id="Seg_7788" s="T730">qur-on-nɨ-n</ta>
            <ta e="T732" id="Seg_7789" s="T731">saːq</ta>
            <ta e="T733" id="Seg_7790" s="T732">tulǯɨ-tdɨ-t</ta>
            <ta e="T734" id="Seg_7791" s="T733">wädra-n</ta>
            <ta e="T735" id="Seg_7792" s="T734">tiːr</ta>
            <ta e="T736" id="Seg_7793" s="T735">i</ta>
            <ta e="T737" id="Seg_7794" s="T736">salǯɨbo-t</ta>
            <ta e="T738" id="Seg_7795" s="T737">qamǯɨ-t</ta>
            <ta e="T739" id="Seg_7796" s="T738">i</ta>
            <ta e="T740" id="Seg_7797" s="T739">täb-ɨ-m</ta>
            <ta e="T741" id="Seg_7798" s="T740">puluzei-n</ta>
            <ta e="T742" id="Seg_7799" s="T741">omdɨ-lǯɨ-t</ta>
            <ta e="T743" id="Seg_7800" s="T742">tʼärɨ-n</ta>
            <ta e="T744" id="Seg_7801" s="T743">aːmda-q</ta>
            <ta e="T745" id="Seg_7802" s="T744">qarʼe-mɨ-tdə</ta>
            <ta e="T746" id="Seg_7803" s="T745">qarʼe-mɨ-ɣɨn</ta>
            <ta e="T747" id="Seg_7804" s="T746">tastɨ</ta>
            <ta e="T748" id="Seg_7805" s="T747">qwat-ča-j</ta>
            <ta e="T749" id="Seg_7806" s="T748">omda-ɣə</ta>
            <ta e="T750" id="Seg_7807" s="T749">au-r-lʼe</ta>
            <ta e="T751" id="Seg_7808" s="T750">razbojnik-se</ta>
            <ta e="T752" id="Seg_7809" s="T751">araka-j</ta>
            <ta e="T753" id="Seg_7810" s="T752">är-lʼe</ta>
            <ta e="T754" id="Seg_7811" s="T753">jübɨ-r-a-q</ta>
            <ta e="T755" id="Seg_7812" s="T754">nä-j-ɣum</ta>
            <ta e="T756" id="Seg_7813" s="T755">tüu-wa</ta>
            <ta e="T757" id="Seg_7814" s="T756">ära-t-də</ta>
            <ta e="T758" id="Seg_7815" s="T757">kö-tdə</ta>
            <ta e="T759" id="Seg_7816" s="T758">ära-m-də</ta>
            <ta e="T760" id="Seg_7817" s="T759">watdo-o-t</ta>
            <ta e="T761" id="Seg_7818" s="T760">na</ta>
            <ta e="T762" id="Seg_7819" s="T761">sözɨ-tdə-t</ta>
            <ta e="T763" id="Seg_7820" s="T762">qarʼe-mɨ-ɣɨn</ta>
            <ta e="T764" id="Seg_7821" s="T763">me</ta>
            <ta e="T765" id="Seg_7822" s="T764">tastɨ</ta>
            <ta e="T766" id="Seg_7823" s="T765">qwat-ča-j</ta>
            <ta e="T767" id="Seg_7824" s="T766">otdə</ta>
            <ta e="T768" id="Seg_7825" s="T767">na</ta>
            <ta e="T769" id="Seg_7826" s="T768">qwat-da</ta>
            <ta e="T770" id="Seg_7827" s="T769">razbojnik-ɨ-m</ta>
            <ta e="T771" id="Seg_7828" s="T770">kaːwal-gu-t</ta>
            <ta e="T772" id="Seg_7829" s="T771">niːde-l-lʼe</ta>
            <ta e="T773" id="Seg_7830" s="T772">taːd-ɨ-r-ɨ-t</ta>
            <ta e="T774" id="Seg_7831" s="T773">i</ta>
            <ta e="T775" id="Seg_7832" s="T774">qotdu-ɣu</ta>
            <ta e="T776" id="Seg_7833" s="T775">quʒan-na-x</ta>
            <ta e="T777" id="Seg_7834" s="T776">i</ta>
            <ta e="T778" id="Seg_7835" s="T777">na</ta>
            <ta e="T779" id="Seg_7836" s="T778">qotdo-lǯa-ɣɨ</ta>
            <ta e="T780" id="Seg_7837" s="T779">ära-t</ta>
            <ta e="T781" id="Seg_7838" s="T780">täp</ta>
            <ta e="T782" id="Seg_7839" s="T781">wazɨ-n</ta>
            <ta e="T783" id="Seg_7840" s="T782">innä</ta>
            <ta e="T784" id="Seg_7841" s="T783">paldʼu-ku-n</ta>
            <ta e="T785" id="Seg_7842" s="T784">a</ta>
            <ta e="T786" id="Seg_7843" s="T785">kɨːba-n</ta>
            <ta e="T787" id="Seg_7844" s="T786">nä-ga-t</ta>
            <ta e="T788" id="Seg_7845" s="T787">iː-ɣa-t</ta>
            <ta e="T789" id="Seg_7846" s="T788">täb-ɨ-staːɣə</ta>
            <ta e="T790" id="Seg_7847" s="T789">qotdɨ-za-ɣə</ta>
            <ta e="T791" id="Seg_7848" s="T790">palati-n</ta>
            <ta e="T792" id="Seg_7849" s="T791">par-o-ɣɨn</ta>
            <ta e="T793" id="Seg_7850" s="T792">nä-ga-t</ta>
            <ta e="T794" id="Seg_7851" s="T793">küzɨ-gu</ta>
            <ta e="T795" id="Seg_7852" s="T794">ilʼlʼe</ta>
            <ta e="T796" id="Seg_7853" s="T795">tʼütʼöu-n</ta>
            <ta e="T797" id="Seg_7854" s="T796">täp</ta>
            <ta e="T798" id="Seg_7855" s="T797">nä-ga-tda-nä</ta>
            <ta e="T799" id="Seg_7856" s="T798">tʼärɨ-n</ta>
            <ta e="T800" id="Seg_7857" s="T799">Man</ta>
            <ta e="T801" id="Seg_7858" s="T800">ud-la-u</ta>
            <ta e="T802" id="Seg_7859" s="T801">tʼikɨ-lʼe-l</ta>
            <ta e="T803" id="Seg_7860" s="T802">a</ta>
            <ta e="T804" id="Seg_7861" s="T803">nä-ga-t</ta>
            <ta e="T805" id="Seg_7862" s="T804">tʼärɨ-n</ta>
            <ta e="T806" id="Seg_7863" s="T805">Tan</ta>
            <ta e="T807" id="Seg_7864" s="T806">tʼäru-ku-za-t</ta>
            <ta e="T808" id="Seg_7865" s="T807">što</ta>
            <ta e="T809" id="Seg_7866" s="T808">or-se</ta>
            <ta e="T810" id="Seg_7867" s="T809">je-wa-n</ta>
            <ta e="T811" id="Seg_7868" s="T810">lakče-lǯ-et</ta>
            <ta e="T812" id="Seg_7869" s="T811">qudɨgo-m</ta>
            <ta e="T813" id="Seg_7870" s="T812">palati-n</ta>
            <ta e="T814" id="Seg_7871" s="T813">bar-o-t</ta>
            <ta e="T815" id="Seg_7872" s="T814">čaǯɨ-n</ta>
            <ta e="T816" id="Seg_7873" s="T815">i</ta>
            <ta e="T817" id="Seg_7874" s="T816">qotda</ta>
            <ta e="T818" id="Seg_7875" s="T817">iː-ga-t</ta>
            <ta e="T819" id="Seg_7876" s="T818">palati-ɣəndo</ta>
            <ta e="T820" id="Seg_7877" s="T819">ilʼlʼe</ta>
            <ta e="T821" id="Seg_7878" s="T820">tʼütʼöu-n</ta>
            <ta e="T822" id="Seg_7879" s="T821">küzɨ-gu</ta>
            <ta e="T823" id="Seg_7880" s="T822">täp</ta>
            <ta e="T824" id="Seg_7881" s="T823">iː-ga-tdä-nä</ta>
            <ta e="T825" id="Seg_7882" s="T824">tʼärɨ-n</ta>
            <ta e="T826" id="Seg_7883" s="T825">man</ta>
            <ta e="T827" id="Seg_7884" s="T826">ud-o-u</ta>
            <ta e="T828" id="Seg_7885" s="T827">tʼiːke-lʼe-l</ta>
            <ta e="T829" id="Seg_7886" s="T828">täp</ta>
            <ta e="T830" id="Seg_7887" s="T829">tü-wa</ta>
            <ta e="T831" id="Seg_7888" s="T830">äzɨ-tdä-nä</ta>
            <ta e="T832" id="Seg_7889" s="T831">i</ta>
            <ta e="T833" id="Seg_7890" s="T832">tʼikə-lʼe</ta>
            <ta e="T834" id="Seg_7891" s="T833">jübə-r-a-t</ta>
            <ta e="T835" id="Seg_7892" s="T834">jass</ta>
            <ta e="T836" id="Seg_7893" s="T835">tʼikɨ-t</ta>
            <ta e="T837" id="Seg_7894" s="T836">mekga</ta>
            <ta e="T838" id="Seg_7895" s="T837">jass</ta>
            <ta e="T839" id="Seg_7896" s="T838">tʼikɨ-ɣuː</ta>
            <ta e="T840" id="Seg_7897" s="T839">äs-tɨ</ta>
            <ta e="T841" id="Seg_7898" s="T840">tʼärɨ-n</ta>
            <ta e="T842" id="Seg_7899" s="T841">patpʼilka</ta>
            <ta e="T843" id="Seg_7900" s="T842">dat-kə</ta>
            <ta e="T844" id="Seg_7901" s="T843">täp</ta>
            <ta e="T845" id="Seg_7902" s="T844">tan-nɨ-t</ta>
            <ta e="T846" id="Seg_7903" s="T845">i</ta>
            <ta e="T847" id="Seg_7904" s="T846">kudɨgo-la-m</ta>
            <ta e="T848" id="Seg_7905" s="T847">sɨlʼe-lʼ-lʼe</ta>
            <ta e="T849" id="Seg_7906" s="T848">na</ta>
            <ta e="T850" id="Seg_7907" s="T849">übə-r-ə-tdɨ-t</ta>
            <ta e="T851" id="Seg_7908" s="T850">sɨlɨ-t</ta>
            <ta e="T852" id="Seg_7909" s="T851">qudəgo-la</ta>
            <ta e="T853" id="Seg_7910" s="T852">warsɨ-wan-na-t</ta>
            <ta e="T854" id="Seg_7911" s="T853">täp</ta>
            <ta e="T855" id="Seg_7912" s="T854">iː-ga-tdä-nä</ta>
            <ta e="T856" id="Seg_7913" s="T855">tʼarɨ-n</ta>
            <ta e="T857" id="Seg_7914" s="T856">qwalʼ-lʼe</ta>
            <ta e="T858" id="Seg_7915" s="T857">qotda-q</ta>
            <ta e="T859" id="Seg_7916" s="T858">qarʼe-mɨ-ɣɨn</ta>
            <ta e="T860" id="Seg_7917" s="T859">iːgə</ta>
            <ta e="T861" id="Seg_7918" s="T860">larɨ-pbaː-k</ta>
            <ta e="T862" id="Seg_7919" s="T861">me</ta>
            <ta e="T863" id="Seg_7920" s="T862">ta-zʼe</ta>
            <ta e="T864" id="Seg_7921" s="T863">onäj</ta>
            <ta e="T865" id="Seg_7922" s="T864">ilʼ-etǯa-j</ta>
            <ta e="T866" id="Seg_7923" s="T865">nom</ta>
            <ta e="T867" id="Seg_7924" s="T866">tʼel-ɨ-m-lʼe</ta>
            <ta e="T868" id="Seg_7925" s="T867">jübɨ-r-ɨ-n</ta>
            <ta e="T869" id="Seg_7926" s="T868">täp</ta>
            <ta e="T870" id="Seg_7927" s="T869">kudɨgo-la-m</ta>
            <ta e="T871" id="Seg_7928" s="T870">udo-ɣətdə</ta>
            <ta e="T872" id="Seg_7929" s="T871">pen-nɨ-t</ta>
            <ta e="T875" id="Seg_7930" s="T874">saːrɨ-pbaː-t</ta>
            <ta e="T876" id="Seg_7931" s="T875">i</ta>
            <ta e="T877" id="Seg_7932" s="T876">omdɨ-n</ta>
            <ta e="T878" id="Seg_7933" s="T877">puluzei-n</ta>
            <ta e="T879" id="Seg_7934" s="T878">par-o-t</ta>
            <ta e="T880" id="Seg_7935" s="T879">pajä-t</ta>
            <ta e="T881" id="Seg_7936" s="T880">wazɨ-n</ta>
            <ta e="T882" id="Seg_7937" s="T881">tü-lʼe</ta>
            <ta e="T883" id="Seg_7938" s="T882">watdo-ɨ-n</ta>
            <ta e="T884" id="Seg_7939" s="T883">söːzɨ-t</ta>
            <ta e="T885" id="Seg_7940" s="T884">razbojnig-nä</ta>
            <ta e="T886" id="Seg_7941" s="T885">tʼärɨ-n</ta>
            <ta e="T887" id="Seg_7942" s="T886">nu</ta>
            <ta e="T888" id="Seg_7943" s="T887">qwal-la-j</ta>
            <ta e="T889" id="Seg_7944" s="T888">a</ta>
            <ta e="T890" id="Seg_7945" s="T889">razbojnik</ta>
            <ta e="T891" id="Seg_7946" s="T890">tʼärɨ-n</ta>
            <ta e="T892" id="Seg_7947" s="T891">au-r-lʼe</ta>
            <ta e="T893" id="Seg_7948" s="T892">qwal-la-j-ze</ta>
            <ta e="T894" id="Seg_7949" s="T893">tep</ta>
            <ta e="T895" id="Seg_7950" s="T894">wadʼi-la-m</ta>
            <ta e="T896" id="Seg_7951" s="T895">müzu-r-ɣɨn-nä-t</ta>
            <ta e="T897" id="Seg_7952" s="T896">au-r-na-ɣə</ta>
            <ta e="T898" id="Seg_7953" s="T897">nu</ta>
            <ta e="T899" id="Seg_7954" s="T898">täpär</ta>
            <ta e="T900" id="Seg_7955" s="T899">qwal-la-j</ta>
            <ta e="T901" id="Seg_7956" s="T900">maːt-qɨn</ta>
            <ta e="T902" id="Seg_7957" s="T901">qwat-ča-j</ta>
            <ta e="T903" id="Seg_7958" s="T902">alʼi</ta>
            <ta e="T904" id="Seg_7959" s="T903">ponä-n</ta>
            <ta e="T905" id="Seg_7960" s="T904">razbojnik</ta>
            <ta e="T906" id="Seg_7961" s="T905">tʼärɨ-n</ta>
            <ta e="T907" id="Seg_7962" s="T906">ponä-n</ta>
            <ta e="T908" id="Seg_7963" s="T907">qwat-ča-j</ta>
            <ta e="T909" id="Seg_7964" s="T908">nu</ta>
            <ta e="T910" id="Seg_7965" s="T909">waza-k</ta>
            <ta e="T911" id="Seg_7966" s="T910">čaːʒɨ-k</ta>
            <ta e="T912" id="Seg_7967" s="T911">ponä</ta>
            <ta e="T913" id="Seg_7968" s="T912">tastɨ</ta>
            <ta e="T914" id="Seg_7969" s="T913">qwat-ča-j</ta>
            <ta e="T915" id="Seg_7970" s="T914">qaj</ta>
            <ta e="T916" id="Seg_7971" s="T915">kudɨgo-la-m</ta>
            <ta e="T917" id="Seg_7972" s="T916">jass</ta>
            <ta e="T918" id="Seg_7973" s="T917">lakče-lǯa-l</ta>
            <ta e="T919" id="Seg_7974" s="T918">tat</ta>
            <ta e="T920" id="Seg_7975" s="T919">tʼäru-ku-za-ttə</ta>
            <ta e="T921" id="Seg_7976" s="T920">or-se</ta>
            <ta e="T922" id="Seg_7977" s="T921">je-wa-n</ta>
            <ta e="T923" id="Seg_7978" s="T922">nä-u-ɣum</ta>
            <ta e="T924" id="Seg_7979" s="T923">kak</ta>
            <ta e="T925" id="Seg_7980" s="T924">watdo-u-tdə</ta>
            <ta e="T926" id="Seg_7981" s="T925">sözɨ-tdɨ-t</ta>
            <ta e="T927" id="Seg_7982" s="T926">täp</ta>
            <ta e="T928" id="Seg_7983" s="T927">kak</ta>
            <ta e="T929" id="Seg_7984" s="T928">innä</ta>
            <ta e="T930" id="Seg_7985" s="T929">wazʼe-zi-tda</ta>
            <ta e="T931" id="Seg_7986" s="T930">kak</ta>
            <ta e="T932" id="Seg_7987" s="T931">razbojnik-a-m</ta>
            <ta e="T933" id="Seg_7988" s="T932">qättɨ-dɨ-t</ta>
            <ta e="T934" id="Seg_7989" s="T933">razbojnik</ta>
            <ta e="T935" id="Seg_7990" s="T934">qotä</ta>
            <ta e="T936" id="Seg_7991" s="T935">čäčä-dʼzʼe-n</ta>
            <ta e="T937" id="Seg_7992" s="T936">čäčä-di-n</ta>
            <ta e="T938" id="Seg_7993" s="T937">i</ta>
            <ta e="T939" id="Seg_7994" s="T938">qwan-nɨ-t</ta>
            <ta e="T940" id="Seg_7995" s="T939">a</ta>
            <ta e="T941" id="Seg_7996" s="T940">pajä-t</ta>
            <ta e="T942" id="Seg_7997" s="T941">soj-o-ɣotdə</ta>
            <ta e="T943" id="Seg_7998" s="T942">ɨd-ə-dʼe-n</ta>
            <ta e="T944" id="Seg_7999" s="T943">tʼärɨ-n</ta>
            <ta e="T945" id="Seg_8000" s="T944">Man</ta>
            <ta e="T946" id="Seg_8001" s="T945">nɨlʼdʼi-n</ta>
            <ta e="T947" id="Seg_8002" s="T946">ass</ta>
            <ta e="T948" id="Seg_8003" s="T947">meː-qw-eǯa-u</ta>
            <ta e="T949" id="Seg_8004" s="T948">tob-la-m-də</ta>
            <ta e="T950" id="Seg_8005" s="T949">müzulǯu-k-eǯa-u</ta>
            <ta e="T951" id="Seg_8006" s="T950">na</ta>
            <ta e="T952" id="Seg_8007" s="T951">öd-ɨ-m</ta>
            <ta e="T953" id="Seg_8008" s="T952">üt-k-eǯa-u</ta>
            <ta e="T954" id="Seg_8009" s="T953">era-t</ta>
            <ta e="T955" id="Seg_8010" s="T954">kak</ta>
            <ta e="T956" id="Seg_8011" s="T955">qäːtä-ǯɨ-t</ta>
            <ta e="T957" id="Seg_8012" s="T956">i</ta>
            <ta e="T958" id="Seg_8013" s="T957">qwan-nɨ-t</ta>
            <ta e="T959" id="Seg_8014" s="T958">qɨbanʼaʒa-la</ta>
            <ta e="T960" id="Seg_8015" s="T959">tür-lʼe</ta>
            <ta e="T961" id="Seg_8016" s="T960">übə-r-a-t</ta>
            <ta e="T962" id="Seg_8017" s="T961">nä-ga-m-də</ta>
            <ta e="T963" id="Seg_8018" s="T962">kak</ta>
            <ta e="T964" id="Seg_8019" s="T963">qätɨ-tdɨ-t</ta>
            <ta e="T965" id="Seg_8020" s="T964">i</ta>
            <ta e="T966" id="Seg_8021" s="T965">qwan-nɨ-t</ta>
            <ta e="T967" id="Seg_8022" s="T966">i</ta>
            <ta e="T968" id="Seg_8023" s="T967">iː-ga-tdä-nä</ta>
            <ta e="T969" id="Seg_8024" s="T968">tʼärɨ-n</ta>
            <ta e="T970" id="Seg_8025" s="T969">me</ta>
            <ta e="T971" id="Seg_8026" s="T970">ta-ze</ta>
            <ta e="T972" id="Seg_8027" s="T971">ilʼ-etǯa-j</ta>
            <ta e="T973" id="Seg_8028" s="T972">igə</ta>
            <ta e="T974" id="Seg_8029" s="T973">türa-k</ta>
            <ta e="T975" id="Seg_8030" s="T974">qwan-nɨ-n</ta>
            <ta e="T976" id="Seg_8031" s="T975">tʼü-j</ta>
            <ta e="T977" id="Seg_8032" s="T976">baɣa-ttɨ-lʼe</ta>
            <ta e="T978" id="Seg_8033" s="T977">tü-j</ta>
            <ta e="T979" id="Seg_8034" s="T978">qɨl-ɨ-m</ta>
            <ta e="T980" id="Seg_8035" s="T979">paqkɨ-nɨ-t</ta>
            <ta e="T981" id="Seg_8036" s="T980">täb-la-m</ta>
            <ta e="T982" id="Seg_8037" s="T981">natʼe-t</ta>
            <ta e="T983" id="Seg_8038" s="T982">qäːl-ɣɨn-ɨ-t</ta>
            <ta e="T984" id="Seg_8039" s="T983">qän-nɨ-t</ta>
            <ta e="T985" id="Seg_8040" s="T984">i</ta>
            <ta e="T986" id="Seg_8041" s="T985">tʼü-ze</ta>
            <ta e="T987" id="Seg_8042" s="T986">ta-ɣɨ-nɨ-t</ta>
            <ta e="T988" id="Seg_8043" s="T987">saoznik-a-m-də</ta>
            <ta e="T989" id="Seg_8044" s="T988">qarʼe</ta>
            <ta e="T990" id="Seg_8045" s="T989">ü-kku-lʼe</ta>
            <ta e="T991" id="Seg_8046" s="T990">qwat-dɨ-t</ta>
            <ta e="T992" id="Seg_8047" s="T991">tädomɨ-la-m-də</ta>
            <ta e="T993" id="Seg_8048" s="T992">wes</ta>
            <ta e="T994" id="Seg_8049" s="T993">tuɣun-nɨ-t</ta>
            <ta e="T995" id="Seg_8050" s="T994">iː-ga-tdɨ-ze</ta>
            <ta e="T996" id="Seg_8051" s="T995">omda-ɣə</ta>
            <ta e="T997" id="Seg_8052" s="T996">i</ta>
            <ta e="T998" id="Seg_8053" s="T997">na</ta>
            <ta e="T999" id="Seg_8054" s="T998">qwa-tdaː-q</ta>
            <ta e="T1000" id="Seg_8055" s="T999">tʼü-lʼe</ta>
            <ta e="T1001" id="Seg_8056" s="T1000">meːda-ɣə</ta>
            <ta e="T1002" id="Seg_8057" s="T1001">otdə</ta>
            <ta e="T1003" id="Seg_8058" s="T1002">jedo-ɣotdə</ta>
            <ta e="T1004" id="Seg_8059" s="T1003">qu-la-t</ta>
            <ta e="T1005" id="Seg_8060" s="T1004">aːtdal-ba-t</ta>
            <ta e="T1006" id="Seg_8061" s="T1005">a</ta>
            <ta e="T1007" id="Seg_8062" s="T1006">paja-l</ta>
            <ta e="T1008" id="Seg_8063" s="T1007">kuttʼen</ta>
            <ta e="T1009" id="Seg_8064" s="T1008">je-n</ta>
            <ta e="T1010" id="Seg_8065" s="T1009">paja-u</ta>
            <ta e="T1011" id="Seg_8066" s="T1010">qu-pba</ta>
            <ta e="T1012" id="Seg_8067" s="T1011">wes</ta>
            <ta e="T1013" id="Seg_8068" s="T1012">tugu-na-ttə</ta>
            <ta e="T1014" id="Seg_8069" s="T1013">tädomɨ-m</ta>
            <ta e="T1015" id="Seg_8070" s="T1014">täp</ta>
            <ta e="T1016" id="Seg_8071" s="T1015">pirʼä-ɣɨtdə</ta>
            <ta e="T1017" id="Seg_8072" s="T1016">maːt</ta>
            <ta e="T1018" id="Seg_8073" s="T1017">tau-tdɨ-t</ta>
            <ta e="T1019" id="Seg_8074" s="T1018">i</ta>
            <ta e="T1020" id="Seg_8075" s="T1019">nädɨ-n</ta>
            <ta e="T1021" id="Seg_8076" s="T1020">i</ta>
            <ta e="T1022" id="Seg_8077" s="T1021">warkɨ-lʼe</ta>
            <ta e="T1023" id="Seg_8078" s="T1022">na</ta>
            <ta e="T1024" id="Seg_8079" s="T1023">übɨ-r-ɨ-tda</ta>
            <ta e="T1025" id="Seg_8080" s="T1024">man</ta>
            <ta e="T1026" id="Seg_8081" s="T1025">potdɨbon</ta>
            <ta e="T1027" id="Seg_8082" s="T1026">teb-nan</ta>
            <ta e="T1028" id="Seg_8083" s="T1027">je-za-n</ta>
            <ta e="T1029" id="Seg_8084" s="T1028">datau</ta>
            <ta e="T1030" id="Seg_8085" s="T1029">sodʼiga-n</ta>
            <ta e="T1031" id="Seg_8086" s="T1030">warkaː</ta>
            <ta e="T1032" id="Seg_8087" s="T1031">paja-t</ta>
            <ta e="T1033" id="Seg_8088" s="T1032">sodʼiga</ta>
            <ta e="T1034" id="Seg_8089" s="T1033">mazɨm</ta>
            <ta e="T1035" id="Seg_8090" s="T1034">čaj-la-ze</ta>
            <ta e="T1036" id="Seg_8091" s="T1035">zɨm</ta>
            <ta e="T1037" id="Seg_8092" s="T1036">är-čɨ-s</ta>
            <ta e="T1038" id="Seg_8093" s="T1037">mekga</ta>
            <ta e="T1039" id="Seg_8094" s="T1038">tʼärɨ-n</ta>
            <ta e="T1040" id="Seg_8095" s="T1039">mä-guntu</ta>
            <ta e="T1041" id="Seg_8096" s="T1040">paldʼi-ko-q</ta>
            <ta e="T1042" id="Seg_8097" s="T1041">man</ta>
            <ta e="T1043" id="Seg_8098" s="T1042">teb-la-nä</ta>
            <ta e="T1044" id="Seg_8099" s="T1043">qɨdan</ta>
            <ta e="T1045" id="Seg_8100" s="T1044">udur-u-q-wa-n</ta>
            <ta e="T1046" id="Seg_8101" s="T1045">ta-bo-n</ta>
            <ta e="T1047" id="Seg_8102" s="T1046">jedo-t</ta>
            <ta e="T1048" id="Seg_8103" s="T1047">qwa-ǯa-n</ta>
            <ta e="T1049" id="Seg_8104" s="T1048">teb-la-nä</ta>
            <ta e="T1050" id="Seg_8105" s="T1049">aj</ta>
            <ta e="T1051" id="Seg_8106" s="T1050">ser-k-eǯa-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_8107" s="T1">Xrestjan</ta>
            <ta e="T3" id="Seg_8108" s="T2">era</ta>
            <ta e="T4" id="Seg_8109" s="T3">tʼelɨm-ntɨ-n</ta>
            <ta e="T5" id="Seg_8110" s="T4">Xrestjan</ta>
            <ta e="T6" id="Seg_8111" s="T5">era</ta>
            <ta e="T7" id="Seg_8112" s="T6">täp-nan</ta>
            <ta e="T8" id="Seg_8113" s="T7">nagur</ta>
            <ta e="T9" id="Seg_8114" s="T8">iː-tə</ta>
            <ta e="T10" id="Seg_8115" s="T9">iː-la-tə</ta>
            <ta e="T11" id="Seg_8116" s="T10">wesʼ</ta>
            <ta e="T12" id="Seg_8117" s="T11">nedɨ-lɨ-tɨn</ta>
            <ta e="T13" id="Seg_8118" s="T12">okkɨr</ta>
            <ta e="T14" id="Seg_8119" s="T13">iː-tə</ta>
            <ta e="T15" id="Seg_8120" s="T14">qɨdan</ta>
            <ta e="T16" id="Seg_8121" s="T15">suːrum-s-le</ta>
            <ta e="T17" id="Seg_8122" s="T16">palʼdʼi-ku-n</ta>
            <ta e="T18" id="Seg_8123" s="T17">wargɨ</ta>
            <ta e="T19" id="Seg_8124" s="T18">iː-tə</ta>
            <ta e="T20" id="Seg_8125" s="T19">a</ta>
            <ta e="T21" id="Seg_8126" s="T20">sədə</ta>
            <ta e="T22" id="Seg_8127" s="T21">sədə</ta>
            <ta e="T23" id="Seg_8128" s="T22">iː-tə</ta>
            <ta e="T24" id="Seg_8129" s="T23">skatʼina-la-se</ta>
            <ta e="T25" id="Seg_8130" s="T24">palʼdʼi-ku-nɨ-qij</ta>
            <ta e="T26" id="Seg_8131" s="T25">sɨr-la</ta>
            <ta e="T27" id="Seg_8132" s="T26">warɨ-ntɨ-tɨn</ta>
            <ta e="T28" id="Seg_8133" s="T27">kütdə-la</ta>
            <ta e="T29" id="Seg_8134" s="T28">i</ta>
            <ta e="T30" id="Seg_8135" s="T29">qoner-la</ta>
            <ta e="T31" id="Seg_8136" s="T30">i</ta>
            <ta e="T32" id="Seg_8137" s="T31">pašnä</ta>
            <ta e="T33" id="Seg_8138" s="T32">warɨ-ntɨ-tɨn</ta>
            <ta e="T34" id="Seg_8139" s="T33">apsǝ-tə</ta>
            <ta e="T35" id="Seg_8140" s="T34">seja-ku-ntɨ-tɨn</ta>
            <ta e="T36" id="Seg_8141" s="T35">a</ta>
            <ta e="T37" id="Seg_8142" s="T36">wargɨ</ta>
            <ta e="T38" id="Seg_8143" s="T37">iː-tə</ta>
            <ta e="T39" id="Seg_8144" s="T38">qɨdan</ta>
            <ta e="T40" id="Seg_8145" s="T39">madʼ-ɨ-ntə</ta>
            <ta e="T41" id="Seg_8146" s="T40">palʼdʼi-n</ta>
            <ta e="T42" id="Seg_8147" s="T41">palʼdʼi-ku-n</ta>
            <ta e="T43" id="Seg_8148" s="T42">täp-nan</ta>
            <ta e="T44" id="Seg_8149" s="T43">ne-tə</ta>
            <ta e="T45" id="Seg_8150" s="T44">tʼelɨm-ntɨ</ta>
            <ta e="T46" id="Seg_8151" s="T45">i</ta>
            <ta e="T47" id="Seg_8152" s="T46">iː-ka</ta>
            <ta e="T48" id="Seg_8153" s="T47">tʼelɨm-ntɨ</ta>
            <ta e="T49" id="Seg_8154" s="T48">täp-ɨ-staɣɨ</ta>
            <ta e="T50" id="Seg_8155" s="T49">wargɨ-ŋ</ta>
            <ta e="T51" id="Seg_8156" s="T50">azu-ɨ-qij</ta>
            <ta e="T52" id="Seg_8157" s="T51">azu-ɨ-qij</ta>
            <ta e="T53" id="Seg_8158" s="T52">a</ta>
            <ta e="T54" id="Seg_8159" s="T53">täp</ta>
            <ta e="T55" id="Seg_8160" s="T54">*natʼe-n</ta>
            <ta e="T56" id="Seg_8161" s="T55">madʼ-ɨ-qɨn</ta>
            <ta e="T57" id="Seg_8162" s="T56">iːbəgaj</ta>
            <ta e="T58" id="Seg_8163" s="T57">maːt</ta>
            <ta e="T59" id="Seg_8164" s="T58">omdɨ-lǯɨ-mbɨ-ntɨ-t</ta>
            <ta e="T60" id="Seg_8165" s="T59">iːbəgaj</ta>
            <ta e="T61" id="Seg_8166" s="T60">maːt</ta>
            <ta e="T62" id="Seg_8167" s="T61">awa-ntɨ-nä</ta>
            <ta e="T63" id="Seg_8168" s="T62">aze-ntɨ-nä</ta>
            <ta e="T64" id="Seg_8169" s="T63">tʼärɨ-n</ta>
            <ta e="T65" id="Seg_8170" s="T64">nom-se</ta>
            <ta e="T66" id="Seg_8171" s="T65">mazɨm</ta>
            <ta e="T67" id="Seg_8172" s="T66">paslawi-kɨ</ta>
            <ta e="T68" id="Seg_8173" s="T67">awa-tə</ta>
            <ta e="T69" id="Seg_8174" s="T68">nom-ɨ-m</ta>
            <ta e="T70" id="Seg_8175" s="T69">iː-nɨ-t</ta>
            <ta e="T71" id="Seg_8176" s="T70">i</ta>
            <ta e="T72" id="Seg_8177" s="T71">paslawi-nɨ-t</ta>
            <ta e="T73" id="Seg_8178" s="T72">man</ta>
            <ta e="T74" id="Seg_8179" s="T73">qwan-enǯɨ-ŋ</ta>
            <ta e="T75" id="Seg_8180" s="T74">nagur</ta>
            <ta e="T76" id="Seg_8181" s="T75">agaː-sɨ-la-un</ta>
            <ta e="T77" id="Seg_8182" s="T76">okkɨr</ta>
            <ta e="T78" id="Seg_8183" s="T77">maːt-qɨn</ta>
            <ta e="T79" id="Seg_8184" s="T78">asa</ta>
            <ta e="T80" id="Seg_8185" s="T79">elɨ-enǯɨ-un</ta>
            <ta e="T81" id="Seg_8186" s="T80">man-nan</ta>
            <ta e="T82" id="Seg_8187" s="T81">teper</ta>
            <ta e="T83" id="Seg_8188" s="T82">semja-w</ta>
            <ta e="T84" id="Seg_8189" s="T83">zawoznʼik-ntə</ta>
            <ta e="T85" id="Seg_8190" s="T84">wesʼ</ta>
            <ta e="T86" id="Seg_8191" s="T85">tädomɨ-m-tə</ta>
            <ta e="T87" id="Seg_8192" s="T86">tugul-nɨ-t</ta>
            <ta e="T88" id="Seg_8193" s="T87">i</ta>
            <ta e="T89" id="Seg_8194" s="T88">na</ta>
            <ta e="T90" id="Seg_8195" s="T89">übɨ-ntɨ-tɨn</ta>
            <ta e="T91" id="Seg_8196" s="T90">aze-tə</ta>
            <ta e="T92" id="Seg_8197" s="T91">awa-tə</ta>
            <ta e="T93" id="Seg_8198" s="T92">i</ta>
            <ta e="T94" id="Seg_8199" s="T93">tʼüru-le</ta>
            <ta e="T95" id="Seg_8200" s="T94">na</ta>
            <ta e="T96" id="Seg_8201" s="T95">qalɨ-ntɨ-tɨn</ta>
            <ta e="T97" id="Seg_8202" s="T96">i</ta>
            <ta e="T98" id="Seg_8203" s="T97">täp-la</ta>
            <ta e="T99" id="Seg_8204" s="T98">i</ta>
            <ta e="T100" id="Seg_8205" s="T99">na</ta>
            <ta e="T101" id="Seg_8206" s="T100">qwan-ntɨ-tɨn</ta>
            <ta e="T102" id="Seg_8207" s="T101">nu</ta>
            <ta e="T103" id="Seg_8208" s="T102">i</ta>
            <ta e="T104" id="Seg_8209" s="T103">tüː-le</ta>
            <ta e="T105" id="Seg_8210" s="T104">na</ta>
            <ta e="T106" id="Seg_8211" s="T105">medə-ntɨ-tɨn</ta>
            <ta e="T107" id="Seg_8212" s="T106">a</ta>
            <ta e="T108" id="Seg_8213" s="T107">täp-nan</ta>
            <ta e="T109" id="Seg_8214" s="T108">*natʼe-n</ta>
            <ta e="T110" id="Seg_8215" s="T109">iːbəgaj</ta>
            <ta e="T111" id="Seg_8216" s="T110">maːt</ta>
            <ta e="T112" id="Seg_8217" s="T111">omdɨ-lǯɨ-mbɨ-ntɨ-t</ta>
            <ta e="T113" id="Seg_8218" s="T112">wesʼ</ta>
            <ta e="T114" id="Seg_8219" s="T113">tugul-nɨ-tɨn</ta>
            <ta e="T115" id="Seg_8220" s="T114">*natʼe-ntə</ta>
            <ta e="T116" id="Seg_8221" s="T115">nu</ta>
            <ta e="T117" id="Seg_8222" s="T116">i</ta>
            <ta e="T118" id="Seg_8223" s="T117">era-tə</ta>
            <ta e="T119" id="Seg_8224" s="T118">wesʼ</ta>
            <ta e="T120" id="Seg_8225" s="T119">meː-le</ta>
            <ta e="T121" id="Seg_8226" s="T120">tat-ɨ-r-nɨ-t</ta>
            <ta e="T122" id="Seg_8227" s="T121">qoptə</ta>
            <ta e="T123" id="Seg_8228" s="T122">meː-ntɨ-t</ta>
            <ta e="T124" id="Seg_8229" s="T123">qare-dʼel-n</ta>
            <ta e="T125" id="Seg_8230" s="T124">madʼ-ɨ-ntə</ta>
            <ta e="T126" id="Seg_8231" s="T125">qwan-enǯɨ-ŋ</ta>
            <ta e="T127" id="Seg_8232" s="T126">a</ta>
            <ta e="T128" id="Seg_8233" s="T127">tan</ta>
            <ta e="T129" id="Seg_8234" s="T128">üt-no</ta>
            <ta e="T130" id="Seg_8235" s="T129">na</ta>
            <ta e="T131" id="Seg_8236" s="T130">qwan-naš</ta>
            <ta e="T132" id="Seg_8237" s="T131">tob-ɣät</ta>
            <ta e="T133" id="Seg_8238" s="T132">qaj-amə</ta>
            <ta e="T134" id="Seg_8239" s="T133">struška</ta>
            <ta e="T135" id="Seg_8240" s="T134">papal-ŋ</ta>
            <ta e="T136" id="Seg_8241" s="T135">igə</ta>
            <ta e="T137" id="Seg_8242" s="T136">azu</ta>
            <ta e="T138" id="Seg_8243" s="T137">ato</ta>
            <ta e="T139" id="Seg_8244" s="T138">awa-ŋ</ta>
            <ta e="T140" id="Seg_8245" s="T139">eː-enǯɨ-n</ta>
            <ta e="T141" id="Seg_8246" s="T140">nu</ta>
            <ta e="T142" id="Seg_8247" s="T141">seqqɨ-tɨn</ta>
            <ta e="T143" id="Seg_8248" s="T142">qare-mɨ-qɨn</ta>
            <ta e="T144" id="Seg_8249" s="T143">wazɨ-n</ta>
            <ta e="T145" id="Seg_8250" s="T144">am-r-nɨ-n</ta>
            <ta e="T146" id="Seg_8251" s="T145">i</ta>
            <ta e="T147" id="Seg_8252" s="T146">na</ta>
            <ta e="T148" id="Seg_8253" s="T147">qwan-ntɨ</ta>
            <ta e="T149" id="Seg_8254" s="T148">madʼ-ɨ-ntə</ta>
            <ta e="T150" id="Seg_8255" s="T149">a</ta>
            <ta e="T151" id="Seg_8256" s="T150">ne-lʼ-qum</ta>
            <ta e="T152" id="Seg_8257" s="T151">tʼärɨ-n</ta>
            <ta e="T153" id="Seg_8258" s="T152">qaj-no</ta>
            <ta e="T154" id="Seg_8259" s="T153">mekka</ta>
            <ta e="T155" id="Seg_8260" s="T154">era-w</ta>
            <ta e="T156" id="Seg_8261" s="T155">tʼärɨ-n</ta>
            <ta e="T157" id="Seg_8262" s="T156">što</ta>
            <ta e="T158" id="Seg_8263" s="T157">struška</ta>
            <ta e="T159" id="Seg_8264" s="T158">üt-ntə</ta>
            <ta e="T160" id="Seg_8265" s="T159">papal-ŋ</ta>
            <ta e="T161" id="Seg_8266" s="T160">igə</ta>
            <ta e="T162" id="Seg_8267" s="T161">azu</ta>
            <ta e="T163" id="Seg_8268" s="T162">man</ta>
            <ta e="T164" id="Seg_8269" s="T163">sičas</ta>
            <ta e="T165" id="Seg_8270" s="T164">sədə</ta>
            <ta e="T166" id="Seg_8271" s="T165">wedro-m</ta>
            <ta e="T167" id="Seg_8272" s="T166">struška-la-se</ta>
            <ta e="T168" id="Seg_8273" s="T167">nabi-n-enǯɨ-w</ta>
            <ta e="T169" id="Seg_8274" s="T168">nabi-n-enǯɨ-w</ta>
            <ta e="T170" id="Seg_8275" s="T169">qwan-le</ta>
            <ta e="T171" id="Seg_8276" s="T170">üt-ntə</ta>
            <ta e="T172" id="Seg_8277" s="T171">qamdə-enǯɨ-w</ta>
            <ta e="T173" id="Seg_8278" s="T172">i</ta>
            <ta e="T174" id="Seg_8279" s="T173">na</ta>
            <ta e="T175" id="Seg_8280" s="T174">qwan-tɨ-ntɨ-t</ta>
            <ta e="T176" id="Seg_8281" s="T175">i</ta>
            <ta e="T177" id="Seg_8282" s="T176">qamǯu-t</ta>
            <ta e="T178" id="Seg_8283" s="T177">nɨ-ku</ta>
            <ta e="T179" id="Seg_8284" s="T178">i</ta>
            <ta e="T180" id="Seg_8285" s="T179">*mantɨ-mbɨ-t</ta>
            <ta e="T181" id="Seg_8286" s="T180">a</ta>
            <ta e="T182" id="Seg_8287" s="T181">*natʼe-n</ta>
            <ta e="T183" id="Seg_8288" s="T182">köu-nɨ-n</ta>
            <ta e="T184" id="Seg_8289" s="T183">täp</ta>
            <ta e="T185" id="Seg_8290" s="T184">üt-ɨ-m</ta>
            <ta e="T186" id="Seg_8291" s="T185">*soɣu-l-nɨ-t</ta>
            <ta e="T187" id="Seg_8292" s="T186">i</ta>
            <ta e="T188" id="Seg_8293" s="T187">maːt-ntə</ta>
            <ta e="T189" id="Seg_8294" s="T188">tüː-nɨ</ta>
            <ta e="T190" id="Seg_8295" s="T189">a</ta>
            <ta e="T191" id="Seg_8296" s="T190">täp</ta>
            <ta e="T192" id="Seg_8297" s="T191">krasiwa</ta>
            <ta e="T193" id="Seg_8298" s="T192">ne-lʼ-qum</ta>
            <ta e="T194" id="Seg_8299" s="T193">eː-sɨ</ta>
            <ta e="T195" id="Seg_8300" s="T194">qwan-le</ta>
            <ta e="T196" id="Seg_8301" s="T195">sʼütdʼe-näj</ta>
            <ta e="T197" id="Seg_8302" s="T196">maːt-ntə</ta>
            <ta e="T198" id="Seg_8303" s="T197">omdɨ-n</ta>
            <ta e="T199" id="Seg_8304" s="T198">i</ta>
            <ta e="T200" id="Seg_8305" s="T199">süt-ɨ-r-le</ta>
            <ta e="T201" id="Seg_8306" s="T200">übɨ-r-nɨ-n</ta>
            <ta e="T202" id="Seg_8307" s="T201">a</ta>
            <ta e="T203" id="Seg_8308" s="T202">sədə</ta>
            <ta e="T204" id="Seg_8309" s="T203">qɨbanʼaǯa</ta>
            <ta e="T205" id="Seg_8310" s="T204">warkɨ-qij</ta>
            <ta e="T206" id="Seg_8311" s="T205">razbojnik-la</ta>
            <ta e="T207" id="Seg_8312" s="T206">čaǯɨ-tɨn</ta>
            <ta e="T208" id="Seg_8313" s="T207">andǝ-se</ta>
            <ta e="T209" id="Seg_8314" s="T208">i</ta>
            <ta e="T210" id="Seg_8315" s="T209">tʼärɨ-tɨn</ta>
            <ta e="T211" id="Seg_8316" s="T210">qaj</ta>
            <ta e="T212" id="Seg_8317" s="T211">struška-la</ta>
            <ta e="T213" id="Seg_8318" s="T212">köu-ntɨ-tɨn</ta>
            <ta e="T214" id="Seg_8319" s="T213">köu-se</ta>
            <ta e="T215" id="Seg_8320" s="T214">čaǯɨ-ntɨ-tɨn</ta>
            <ta e="T216" id="Seg_8321" s="T215">qä-n</ta>
            <ta e="T217" id="Seg_8322" s="T216">par-ɨ-nä</ta>
            <ta e="T218" id="Seg_8323" s="T217">prai-etɨ</ta>
            <ta e="T219" id="Seg_8324" s="T218">täp-la</ta>
            <ta e="T220" id="Seg_8325" s="T219">*mantɨ-mbɨ-tɨn</ta>
            <ta e="T221" id="Seg_8326" s="T220">qaj-ta</ta>
            <ta e="T222" id="Seg_8327" s="T221">watt</ta>
            <ta e="T223" id="Seg_8328" s="T222">täp-la</ta>
            <ta e="T224" id="Seg_8329" s="T223">watt-ka</ta>
            <ta e="T225" id="Seg_8330" s="T224">qo-ntɨ-tɨn</ta>
            <ta e="T226" id="Seg_8331" s="T225">ugon</ta>
            <ta e="T227" id="Seg_8332" s="T226">me</ta>
            <ta e="T228" id="Seg_8333" s="T227">taw</ta>
            <ta e="T229" id="Seg_8334" s="T228">watt-m</ta>
            <ta e="T230" id="Seg_8335" s="T229">asa</ta>
            <ta e="T231" id="Seg_8336" s="T230">qo-nǯir-ku-sɨ-un</ta>
            <ta e="T232" id="Seg_8337" s="T231">na</ta>
            <ta e="T233" id="Seg_8338" s="T232">watt-ɨ-ka-ntə</ta>
            <ta e="T234" id="Seg_8339" s="T233">na</ta>
            <ta e="T235" id="Seg_8340" s="T234">watt-ɨ-ntə</ta>
            <ta e="T236" id="Seg_8341" s="T235">na</ta>
            <ta e="T237" id="Seg_8342" s="T236">watt-ɨ-ntə</ta>
            <ta e="T238" id="Seg_8343" s="T237">i</ta>
            <ta e="T239" id="Seg_8344" s="T238">udɨr-ɨ-tɨn</ta>
            <ta e="T240" id="Seg_8345" s="T239">ataman</ta>
            <ta e="T241" id="Seg_8346" s="T240">tʼärɨ-n</ta>
            <ta e="T242" id="Seg_8347" s="T241">te</ta>
            <ta e="T243" id="Seg_8348" s="T242">amdɨ-naltə</ta>
            <ta e="T244" id="Seg_8349" s="T243">tɨtʼa-n</ta>
            <ta e="T245" id="Seg_8350" s="T244">a</ta>
            <ta e="T246" id="Seg_8351" s="T245">man</ta>
            <ta e="T247" id="Seg_8352" s="T246">kur-ol-lä-ŋ</ta>
            <ta e="T248" id="Seg_8353" s="T247">*mantɨ-mbɨ-lä-w</ta>
            <ta e="T249" id="Seg_8354" s="T248">qaj</ta>
            <ta e="T250" id="Seg_8355" s="T249">elɨ-ntɨ</ta>
            <ta e="T251" id="Seg_8356" s="T250">tɨtʼa-n</ta>
            <ta e="T252" id="Seg_8357" s="T251">na</ta>
            <ta e="T253" id="Seg_8358" s="T252">kur-ol-ntɨ</ta>
            <ta e="T254" id="Seg_8359" s="T253">tüː-nɨ</ta>
            <ta e="T255" id="Seg_8360" s="T254">ser-nɨ-n</ta>
            <ta e="T256" id="Seg_8361" s="T255">maːt-ntə</ta>
            <ta e="T257" id="Seg_8362" s="T256">tʼolom</ta>
            <ta e="T258" id="Seg_8363" s="T257">ne-lʼ-qum</ta>
            <ta e="T259" id="Seg_8364" s="T258">tʼärɨ-n</ta>
            <ta e="T260" id="Seg_8365" s="T259">tʼolom</ta>
            <ta e="T261" id="Seg_8366" s="T260">tʼolom</ta>
            <ta e="T262" id="Seg_8367" s="T261">stul</ta>
            <ta e="T263" id="Seg_8368" s="T262">čeččɨ-le</ta>
            <ta e="T264" id="Seg_8369" s="T263">meː-ʒu-lǯɨ-t</ta>
            <ta e="T265" id="Seg_8370" s="T264">tʼärɨ-n</ta>
            <ta e="T266" id="Seg_8371" s="T265">omdɨ-kɨ</ta>
            <ta e="T267" id="Seg_8372" s="T266">tɨŋga</ta>
            <ta e="T268" id="Seg_8373" s="T267">a</ta>
            <ta e="T269" id="Seg_8374" s="T268">täp</ta>
            <ta e="T270" id="Seg_8375" s="T269">omdɨ-n</ta>
            <ta e="T271" id="Seg_8376" s="T270">kulubu-le</ta>
            <ta e="T272" id="Seg_8377" s="T271">na</ta>
            <ta e="T273" id="Seg_8378" s="T272">übɨ-r-ɨ-ntɨ-qij</ta>
            <ta e="T274" id="Seg_8379" s="T273">ataman</ta>
            <ta e="T275" id="Seg_8380" s="T274">tʼärɨ-n</ta>
            <ta e="T276" id="Seg_8381" s="T275">tan</ta>
            <ta e="T277" id="Seg_8382" s="T276">mekka</ta>
            <ta e="T278" id="Seg_8383" s="T277">tɨbəndu-lä-ntə</ta>
            <ta e="T279" id="Seg_8384" s="T278">no</ta>
            <ta e="T280" id="Seg_8385" s="T279">man-nan</ta>
            <ta e="T281" id="Seg_8386" s="T280">era-w</ta>
            <ta e="T282" id="Seg_8387" s="T281">or-se</ta>
            <ta e="T283" id="Seg_8388" s="T282">täp</ta>
            <ta e="T284" id="Seg_8389" s="T283">tastɨ</ta>
            <ta e="T285" id="Seg_8390" s="T284">qwat-enǯɨ-t</ta>
            <ta e="T286" id="Seg_8391" s="T285">i</ta>
            <ta e="T287" id="Seg_8392" s="T286">mazɨm</ta>
            <ta e="T288" id="Seg_8393" s="T287">qwat-enǯɨ-t</ta>
            <ta e="T289" id="Seg_8394" s="T288">a</ta>
            <ta e="T290" id="Seg_8395" s="T289">man</ta>
            <ta e="T291" id="Seg_8396" s="T290">tekka</ta>
            <ta e="T292" id="Seg_8397" s="T291">quːdəgo</ta>
            <ta e="T293" id="Seg_8398" s="T292">meː-ntɨ-enǯɨ-ŋ</ta>
            <ta e="T294" id="Seg_8399" s="T293">optə</ta>
            <ta e="T295" id="Seg_8400" s="T294">eː-nɨš</ta>
            <ta e="T296" id="Seg_8401" s="T295">i</ta>
            <ta e="T299" id="Seg_8402" s="T298">täp</ta>
            <ta e="T300" id="Seg_8403" s="T299">taw</ta>
            <ta e="T301" id="Seg_8404" s="T300">quːdəgo-m</ta>
            <ta e="T302" id="Seg_8405" s="T301">asa</ta>
            <ta e="T303" id="Seg_8406" s="T302">lakču-enǯɨ-t</ta>
            <ta e="T304" id="Seg_8407" s="T303">täp</ta>
            <ta e="T305" id="Seg_8408" s="T304">na</ta>
            <ta e="T306" id="Seg_8409" s="T305">tüː-nɨš</ta>
            <ta e="T307" id="Seg_8410" s="T306">üdə-mɨ-qɨn</ta>
            <ta e="T308" id="Seg_8411" s="T307">tan</ta>
            <ta e="T309" id="Seg_8412" s="T308">tʼärɨ-kɨ</ta>
            <ta e="T310" id="Seg_8413" s="T309">kozɨr-ču-lä-j</ta>
            <ta e="T311" id="Seg_8414" s="T310">täp-nä</ta>
            <ta e="T312" id="Seg_8415" s="T311">tʼärɨ-kɨ</ta>
            <ta e="T313" id="Seg_8416" s="T312">kud</ta>
            <ta e="T314" id="Seg_8417" s="T313">turak-ɨ-ŋ</ta>
            <ta e="T315" id="Seg_8418" s="T314">qalɨ-enǯɨ-n</ta>
            <ta e="T316" id="Seg_8419" s="T315">ut-la-m-tə</ta>
            <ta e="T317" id="Seg_8420" s="T316">moqɨnä</ta>
            <ta e="T318" id="Seg_8421" s="T317">saːrə-gu</ta>
            <ta e="T319" id="Seg_8422" s="T318">a</ta>
            <ta e="T320" id="Seg_8423" s="T319">tan</ta>
            <ta e="T321" id="Seg_8424" s="T320">täp-ɨ-m</ta>
            <ta e="T322" id="Seg_8425" s="T321">turak-ɨ-ŋ</ta>
            <ta e="T323" id="Seg_8426" s="T322">qwɛdʼi-etɨ</ta>
            <ta e="T324" id="Seg_8427" s="T323">ut-la-m-tə</ta>
            <ta e="T325" id="Seg_8428" s="T324">moqɨnä</ta>
            <ta e="T326" id="Seg_8429" s="T325">saːrə-etɨ</ta>
            <ta e="T327" id="Seg_8430" s="T326">täp</ta>
            <ta e="T328" id="Seg_8431" s="T327">taw</ta>
            <ta e="T329" id="Seg_8432" s="T328">quːdəgo-m</ta>
            <ta e="T330" id="Seg_8433" s="T329">asa</ta>
            <ta e="T331" id="Seg_8434" s="T330">lakču-enǯɨ-n</ta>
            <ta e="T332" id="Seg_8435" s="T331">täp</ta>
            <ta e="T333" id="Seg_8436" s="T332">jeʒlʼe</ta>
            <ta e="T334" id="Seg_8437" s="T333">asa</ta>
            <ta e="T335" id="Seg_8438" s="T334">lakču-t</ta>
            <ta e="T336" id="Seg_8439" s="T335">a</ta>
            <ta e="T337" id="Seg_8440" s="T336">tan</ta>
            <ta e="T338" id="Seg_8441" s="T337">poːne</ta>
            <ta e="T339" id="Seg_8442" s="T338">čanǯu-kɨ</ta>
            <ta e="T340" id="Seg_8443" s="T339">mazɨm</ta>
            <ta e="T341" id="Seg_8444" s="T340">laŋgoj-l-kɨ</ta>
            <ta e="T342" id="Seg_8445" s="T341">man</ta>
            <ta e="T343" id="Seg_8446" s="T342">tüː-enǯɨ-ŋ</ta>
            <ta e="T344" id="Seg_8447" s="T343">täp-ɨ-m</ta>
            <ta e="T345" id="Seg_8448" s="T344">i</ta>
            <ta e="T346" id="Seg_8449" s="T345">qwat-enǯɨ-w</ta>
            <ta e="T347" id="Seg_8450" s="T346">tan-se</ta>
            <ta e="T348" id="Seg_8451" s="T347">elɨ-enǯɨ-j</ta>
            <ta e="T349" id="Seg_8452" s="T348">era-tə</ta>
            <ta e="T350" id="Seg_8453" s="T349">tüː-nɨ</ta>
            <ta e="T351" id="Seg_8454" s="T350">täp</ta>
            <ta e="T352" id="Seg_8455" s="T351">täp-ɨ-m</ta>
            <ta e="T353" id="Seg_8456" s="T352">apsǝ-tɨ-t</ta>
            <ta e="T354" id="Seg_8457" s="T353">tʼärɨ-n</ta>
            <ta e="T355" id="Seg_8458" s="T354">kozɨr-ču-lä-j</ta>
            <ta e="T356" id="Seg_8459" s="T355">era-tə</ta>
            <ta e="T357" id="Seg_8460" s="T356">tʼärɨ-n</ta>
            <ta e="T358" id="Seg_8461" s="T357">man</ta>
            <ta e="T359" id="Seg_8462" s="T358">nunɨ-dʼi-mbɨ-ŋ</ta>
            <ta e="T360" id="Seg_8463" s="T359">qondu-gu</ta>
            <ta e="T361" id="Seg_8464" s="T360">nadə</ta>
            <ta e="T362" id="Seg_8465" s="T361">nu</ta>
            <ta e="T363" id="Seg_8466" s="T362">dawaj</ta>
            <ta e="T364" id="Seg_8467" s="T363">kozɨr-ču-lä-j</ta>
            <ta e="T365" id="Seg_8468" s="T364">asa</ta>
            <ta e="T366" id="Seg_8469" s="T365">kundɨ-n</ta>
            <ta e="T367" id="Seg_8470" s="T366">ato</ta>
            <ta e="T368" id="Seg_8471" s="T367">oneŋ</ta>
            <ta e="T369" id="Seg_8472" s="T368">amdɨ-ŋ</ta>
            <ta e="T370" id="Seg_8473" s="T369">i</ta>
            <ta e="T371" id="Seg_8474" s="T370">skušna-ŋ</ta>
            <ta e="T372" id="Seg_8475" s="T371">mekka</ta>
            <ta e="T373" id="Seg_8476" s="T372">eː-n</ta>
            <ta e="T374" id="Seg_8477" s="T373">nu</ta>
            <ta e="T375" id="Seg_8478" s="T374">i</ta>
            <ta e="T376" id="Seg_8479" s="T375">omdɨ-qij</ta>
            <ta e="T377" id="Seg_8480" s="T376">kozɨr-ču-le</ta>
            <ta e="T378" id="Seg_8481" s="T377">kud</ta>
            <ta e="T379" id="Seg_8482" s="T378">turak-ɨ-ŋ</ta>
            <ta e="T380" id="Seg_8483" s="T379">qalɨ-enǯɨ-n</ta>
            <ta e="T381" id="Seg_8484" s="T380">ut-la-m-tə</ta>
            <ta e="T382" id="Seg_8485" s="T381">saːrə-lǯi-gu</ta>
            <ta e="T383" id="Seg_8486" s="T382">moqɨnä</ta>
            <ta e="T384" id="Seg_8487" s="T383">era-tə</ta>
            <ta e="T385" id="Seg_8488" s="T384">turak-ɨ-ŋ</ta>
            <ta e="T386" id="Seg_8489" s="T385">qalɨ-n</ta>
            <ta e="T387" id="Seg_8490" s="T386">ut-la-m-tə</ta>
            <ta e="T388" id="Seg_8491" s="T387">nadə</ta>
            <ta e="T389" id="Seg_8492" s="T388">saːrə-lǯi-gu</ta>
            <ta e="T390" id="Seg_8493" s="T389">ut-m-tə</ta>
            <ta e="T391" id="Seg_8494" s="T390">nadə</ta>
            <ta e="T392" id="Seg_8495" s="T391">saːrə-gu</ta>
            <ta e="T393" id="Seg_8496" s="T392">moqɨnä</ta>
            <ta e="T394" id="Seg_8497" s="T393">a</ta>
            <ta e="T395" id="Seg_8498" s="T394">jeʒlʼe</ta>
            <ta e="T396" id="Seg_8499" s="T395">man</ta>
            <ta e="T397" id="Seg_8500" s="T396">qalɨ-ne-ŋ</ta>
            <ta e="T398" id="Seg_8501" s="T397">turak-ɨ-ŋ</ta>
            <ta e="T399" id="Seg_8502" s="T398">tan</ta>
            <ta e="T400" id="Seg_8503" s="T399">bɨ</ta>
            <ta e="T401" id="Seg_8504" s="T400">mekka</ta>
            <ta e="T402" id="Seg_8505" s="T401">ut-la-w</ta>
            <ta e="T403" id="Seg_8506" s="T402">saːrə-lǯɨ-ne-l</ta>
            <ta e="T404" id="Seg_8507" s="T403">a</ta>
            <ta e="T405" id="Seg_8508" s="T404">teper</ta>
            <ta e="T406" id="Seg_8509" s="T405">man</ta>
            <ta e="T407" id="Seg_8510" s="T406">tekka</ta>
            <ta e="T408" id="Seg_8511" s="T407">ut-m-ntɨ</ta>
            <ta e="T409" id="Seg_8512" s="T408">saːrə-enǯɨ-w</ta>
            <ta e="T410" id="Seg_8513" s="T409">era-tə</ta>
            <ta e="T411" id="Seg_8514" s="T410">tʼärɨ-n</ta>
            <ta e="T412" id="Seg_8515" s="T411">saːrə-etɨ</ta>
            <ta e="T413" id="Seg_8516" s="T412">täp</ta>
            <ta e="T414" id="Seg_8517" s="T413">saːrə-t</ta>
            <ta e="T415" id="Seg_8518" s="T414">era-tə</ta>
            <ta e="T416" id="Seg_8519" s="T415">kak</ta>
            <ta e="T417" id="Seg_8520" s="T416">turugu-lǯɨ-t</ta>
            <ta e="T418" id="Seg_8521" s="T417">quːdəgo-la</ta>
            <ta e="T419" id="Seg_8522" s="T418">wesʼ</ta>
            <ta e="T420" id="Seg_8523" s="T419">lakču-lɨ-tɨn</ta>
            <ta e="T421" id="Seg_8524" s="T420">nu</ta>
            <ta e="T422" id="Seg_8525" s="T421">i</ta>
            <ta e="T423" id="Seg_8526" s="T422">qondu-gu</ta>
            <ta e="T424" id="Seg_8527" s="T423">quʒal-nɨ-qij</ta>
            <ta e="T425" id="Seg_8528" s="T424">qare-mɨ-qɨn</ta>
            <ta e="T426" id="Seg_8529" s="T425">wazɨ-n</ta>
            <ta e="T427" id="Seg_8530" s="T426">ne-lʼ-qum</ta>
            <ta e="T428" id="Seg_8531" s="T427">wesʼ</ta>
            <ta e="T429" id="Seg_8532" s="T428">poːt-ku-nɨ-t</ta>
            <ta e="T430" id="Seg_8533" s="T429">wadʼi-la-m</ta>
            <ta e="T431" id="Seg_8534" s="T430">qaj-la-m</ta>
            <ta e="T432" id="Seg_8535" s="T431">era-m-tə</ta>
            <ta e="T433" id="Seg_8536" s="T432">apsǝ-tɨ-t</ta>
            <ta e="T434" id="Seg_8537" s="T433">era-tə</ta>
            <ta e="T435" id="Seg_8538" s="T434">madʼ-ɨ-ntə</ta>
            <ta e="T436" id="Seg_8539" s="T435">qwan-nɨ-n</ta>
            <ta e="T437" id="Seg_8540" s="T436">ne-lʼ-qum</ta>
            <ta e="T438" id="Seg_8541" s="T437">poːne</ta>
            <ta e="T439" id="Seg_8542" s="T438">sapɨsi-n</ta>
            <ta e="T440" id="Seg_8543" s="T439">i</ta>
            <ta e="T441" id="Seg_8544" s="T440">na</ta>
            <ta e="T442" id="Seg_8545" s="T441">qarɨ-ol-ntɨ</ta>
            <ta e="T443" id="Seg_8546" s="T442">tüː-kɨ</ta>
            <ta e="T444" id="Seg_8547" s="T443">era-w</ta>
            <ta e="T445" id="Seg_8548" s="T444">qwan-mbɨ</ta>
            <ta e="T446" id="Seg_8549" s="T445">ataman</ta>
            <ta e="T447" id="Seg_8550" s="T446">na</ta>
            <ta e="T448" id="Seg_8551" s="T447">tüː-ntɨ</ta>
            <ta e="T449" id="Seg_8552" s="T448">nu</ta>
            <ta e="T450" id="Seg_8553" s="T449">qaj</ta>
            <ta e="T451" id="Seg_8554" s="T450">kozɨr-ču-sɨ-lɨn</ta>
            <ta e="T452" id="Seg_8555" s="T451">kozɨr-ču-sɨ-j</ta>
            <ta e="T453" id="Seg_8556" s="T452">man</ta>
            <ta e="T454" id="Seg_8557" s="T453">tekka</ta>
            <ta e="T455" id="Seg_8558" s="T454">tʼärɨ-sɨ-ŋ</ta>
            <ta e="T456" id="Seg_8559" s="T455">täp</ta>
            <ta e="T457" id="Seg_8560" s="T456">or-se</ta>
            <ta e="T458" id="Seg_8561" s="T457">eː-n</ta>
            <ta e="T459" id="Seg_8562" s="T458">na</ta>
            <ta e="T460" id="Seg_8563" s="T459">quːdəgo-la-m</ta>
            <ta e="T461" id="Seg_8564" s="T460">wesʼ</ta>
            <ta e="T462" id="Seg_8565" s="T461">lakču-lǯi-t</ta>
            <ta e="T463" id="Seg_8566" s="T462">a</ta>
            <ta e="T464" id="Seg_8567" s="T463">taw-dʼel</ta>
            <ta e="T465" id="Seg_8568" s="T464">man</ta>
            <ta e="T466" id="Seg_8569" s="T465">tekka</ta>
            <ta e="T467" id="Seg_8570" s="T466">me-enǯɨ-w</ta>
            <ta e="T468" id="Seg_8571" s="T467">optə</ta>
            <ta e="T469" id="Seg_8572" s="T468">quːdəgo</ta>
            <ta e="T470" id="Seg_8573" s="T469">me-ntɨ-enǯɨ-ŋ</ta>
            <ta e="T471" id="Seg_8574" s="T470">üdə-mɨ-qɨn</ta>
            <ta e="T472" id="Seg_8575" s="T471">na</ta>
            <ta e="T473" id="Seg_8576" s="T472">tüː-nɨš</ta>
            <ta e="T474" id="Seg_8577" s="T473">aj</ta>
            <ta e="T475" id="Seg_8578" s="T474">tʼärɨ-kɨ</ta>
            <ta e="T476" id="Seg_8579" s="T475">kozɨr-ču-lä-j</ta>
            <ta e="T477" id="Seg_8580" s="T476">kud</ta>
            <ta e="T478" id="Seg_8581" s="T477">turak-ɨ-ŋ</ta>
            <ta e="T479" id="Seg_8582" s="T478">qalɨ-enǯɨ-n</ta>
            <ta e="T480" id="Seg_8583" s="T479">ut-la-m-tə</ta>
            <ta e="T481" id="Seg_8584" s="T480">moqɨnä</ta>
            <ta e="T482" id="Seg_8585" s="T481">saːrə-gu</ta>
            <ta e="T483" id="Seg_8586" s="T482">ne-lʼ-qum</ta>
            <ta e="T484" id="Seg_8587" s="T483">täp-ɨ-m</ta>
            <ta e="T485" id="Seg_8588" s="T484">apsǝ-tɨ-mbɨ-t</ta>
            <ta e="T486" id="Seg_8589" s="T485">araŋka-se</ta>
            <ta e="T487" id="Seg_8590" s="T486">öru-ču-mbɨ-t</ta>
            <ta e="T488" id="Seg_8591" s="T487">araŋka-la</ta>
            <ta e="T489" id="Seg_8592" s="T488">üt-ntɨ-di</ta>
            <ta e="T490" id="Seg_8593" s="T489">täbe-qum</ta>
            <ta e="T491" id="Seg_8594" s="T490">qwan-nɨ-n</ta>
            <ta e="T492" id="Seg_8595" s="T491">era-tə</ta>
            <ta e="T493" id="Seg_8596" s="T492">tüː-nɨ</ta>
            <ta e="T494" id="Seg_8597" s="T493">suːrum-la-m</ta>
            <ta e="T495" id="Seg_8598" s="T494">koːci-ŋ</ta>
            <ta e="T496" id="Seg_8599" s="T495">qwat-mbɨ-t</ta>
            <ta e="T497" id="Seg_8600" s="T496">i</ta>
            <ta e="T498" id="Seg_8601" s="T497">peq</ta>
            <ta e="T499" id="Seg_8602" s="T498">qwat-mbɨ-ntɨ-t</ta>
            <ta e="T500" id="Seg_8603" s="T499">täp</ta>
            <ta e="T501" id="Seg_8604" s="T500">täp-ɨ-m</ta>
            <ta e="T502" id="Seg_8605" s="T501">apsǝ-tɨ-t</ta>
            <ta e="T503" id="Seg_8606" s="T502">tʼärɨ-n</ta>
            <ta e="T504" id="Seg_8607" s="T503">kozɨr-ču-lä-j</ta>
            <ta e="T505" id="Seg_8608" s="T504">era-tə</ta>
            <ta e="T506" id="Seg_8609" s="T505">tʼärɨ-n</ta>
            <ta e="T507" id="Seg_8610" s="T506">man</ta>
            <ta e="T508" id="Seg_8611" s="T507">nunɨ-dʼi-mbɨ-ŋ</ta>
            <ta e="T509" id="Seg_8612" s="T508">paja-tə</ta>
            <ta e="T510" id="Seg_8613" s="T509">tʼärɨ-n</ta>
            <ta e="T511" id="Seg_8614" s="T510">kozɨr-ču-lä-j</ta>
            <ta e="T514" id="Seg_8615" s="T513">kud</ta>
            <ta e="T515" id="Seg_8616" s="T514">kud</ta>
            <ta e="T516" id="Seg_8617" s="T515">turak-ɨ-ŋ</ta>
            <ta e="T517" id="Seg_8618" s="T516">qalɨ-enǯɨ-n</ta>
            <ta e="T518" id="Seg_8619" s="T517">ut-ɨ-m-tə</ta>
            <ta e="T519" id="Seg_8620" s="T518">moqɨnä</ta>
            <ta e="T520" id="Seg_8621" s="T519">saːrə-gu</ta>
            <ta e="T521" id="Seg_8622" s="T520">jesʼli</ta>
            <ta e="T522" id="Seg_8623" s="T521">man</ta>
            <ta e="T523" id="Seg_8624" s="T522">qalɨ-ŋ</ta>
            <ta e="T524" id="Seg_8625" s="T523">turak-ɨ-ŋ</ta>
            <ta e="T525" id="Seg_8626" s="T524">man</ta>
            <ta e="T526" id="Seg_8627" s="T525">ut-ɨ-w</ta>
            <ta e="T527" id="Seg_8628" s="T526">saːrə-kɨ</ta>
            <ta e="T528" id="Seg_8629" s="T527">saːrə-enǯɨ-l</ta>
            <ta e="T529" id="Seg_8630" s="T528">täp-ɨ-staɣɨ</ta>
            <ta e="T530" id="Seg_8631" s="T529">omdɨ-qij</ta>
            <ta e="T531" id="Seg_8632" s="T530">kozɨr-ču-le</ta>
            <ta e="T532" id="Seg_8633" s="T531">era-tə</ta>
            <ta e="T533" id="Seg_8634" s="T532">turak-ɨ-ŋ</ta>
            <ta e="T534" id="Seg_8635" s="T533">qalɨ-n</ta>
            <ta e="T535" id="Seg_8636" s="T534">nu</ta>
            <ta e="T536" id="Seg_8637" s="T535">teper</ta>
            <ta e="T537" id="Seg_8638" s="T536">ut-m-ntɨ</ta>
            <ta e="T538" id="Seg_8639" s="T537">saːrə-enǯɨ-w</ta>
            <ta e="T539" id="Seg_8640" s="T538">täp</ta>
            <ta e="T540" id="Seg_8641" s="T539">saːrə-t</ta>
            <ta e="T541" id="Seg_8642" s="T540">ut-ɨ-m-tə</ta>
            <ta e="T542" id="Seg_8643" s="T541">täp</ta>
            <ta e="T543" id="Seg_8644" s="T542">kak</ta>
            <ta e="T544" id="Seg_8645" s="T543">turugu-lǯi-ntɨ</ta>
            <ta e="T545" id="Seg_8646" s="T544">quːdəgo</ta>
            <ta e="T546" id="Seg_8647" s="T545">lakču-dʼi-n</ta>
            <ta e="T547" id="Seg_8648" s="T546">quʒal-nɨ-qij</ta>
            <ta e="T548" id="Seg_8649" s="T547">qondu-nɨ</ta>
            <ta e="T549" id="Seg_8650" s="T548">qare-mɨ-qɨn</ta>
            <ta e="T550" id="Seg_8651" s="T549">wazɨ-qij</ta>
            <ta e="T551" id="Seg_8652" s="T550">ne-lʼ-qum</ta>
            <ta e="T552" id="Seg_8653" s="T551">era-m-tə</ta>
            <ta e="T553" id="Seg_8654" s="T552">apsǝ-tɨ-t</ta>
            <ta e="T554" id="Seg_8655" s="T553">era-tə</ta>
            <ta e="T555" id="Seg_8656" s="T554">am-r-nɨ-n</ta>
            <ta e="T556" id="Seg_8657" s="T555">i</ta>
            <ta e="T557" id="Seg_8658" s="T556">madʼ-ɨ-ntə</ta>
            <ta e="T558" id="Seg_8659" s="T557">na</ta>
            <ta e="T559" id="Seg_8660" s="T558">qwan-ntɨ</ta>
            <ta e="T560" id="Seg_8661" s="T559">ne-lʼ-qum</ta>
            <ta e="T561" id="Seg_8662" s="T560">poːne</ta>
            <ta e="T562" id="Seg_8663" s="T561">čaǯɨ-n</ta>
            <ta e="T563" id="Seg_8664" s="T562">i</ta>
            <ta e="T564" id="Seg_8665" s="T563">na</ta>
            <ta e="T565" id="Seg_8666" s="T564">qarɨ-ol-ɨ-n</ta>
            <ta e="T566" id="Seg_8667" s="T565">tüː-kɨ</ta>
            <ta e="T567" id="Seg_8668" s="T566">era-w</ta>
            <ta e="T568" id="Seg_8669" s="T567">qwan-mbɨ</ta>
            <ta e="T569" id="Seg_8670" s="T568">razbojnik</ta>
            <ta e="T570" id="Seg_8671" s="T569">tüː-nɨ</ta>
            <ta e="T571" id="Seg_8672" s="T570">ne-lʼ-qum</ta>
            <ta e="T572" id="Seg_8673" s="T571">tʼärɨ-n</ta>
            <ta e="T573" id="Seg_8674" s="T572">quːdəgo-la-m</ta>
            <ta e="T574" id="Seg_8675" s="T573">era-w</ta>
            <ta e="T575" id="Seg_8676" s="T574">wesʼ</ta>
            <ta e="T576" id="Seg_8677" s="T575">lakču-lǯi-t</ta>
            <ta e="T577" id="Seg_8678" s="T576">täp</ta>
            <ta e="T578" id="Seg_8679" s="T577">or-se</ta>
            <ta e="T579" id="Seg_8680" s="T578">eː-n</ta>
            <ta e="T580" id="Seg_8681" s="T579">a</ta>
            <ta e="T581" id="Seg_8682" s="T580">man</ta>
            <ta e="T582" id="Seg_8683" s="T581">taw-dʼel</ta>
            <ta e="T583" id="Seg_8684" s="T582">tekka</ta>
            <ta e="T584" id="Seg_8685" s="T583">me-enǯɨ-w</ta>
            <ta e="T585" id="Seg_8686" s="T584">quːdəgo</ta>
            <ta e="T586" id="Seg_8687" s="T585">optə</ta>
            <ta e="T587" id="Seg_8688" s="T586">i</ta>
            <ta e="T590" id="Seg_8689" s="T589">okkɨr-mɨ-n</ta>
            <ta e="T591" id="Seg_8690" s="T590">tamgɨl-mbɨ</ta>
            <ta e="T592" id="Seg_8691" s="T591">üdə-mɨ-qɨn</ta>
            <ta e="T593" id="Seg_8692" s="T592">na</ta>
            <ta e="T594" id="Seg_8693" s="T593">tüː-nɨš</ta>
            <ta e="T595" id="Seg_8694" s="T594">tan</ta>
            <ta e="T596" id="Seg_8695" s="T595">aj</ta>
            <ta e="T597" id="Seg_8696" s="T596">tʼärɨ-kɨ</ta>
            <ta e="T598" id="Seg_8697" s="T597">kozɨr-ču-gu</ta>
            <ta e="T599" id="Seg_8698" s="T598">kozɨr-ču-lä-j</ta>
            <ta e="T600" id="Seg_8699" s="T599">kud</ta>
            <ta e="T601" id="Seg_8700" s="T600">turak-ɨ-ŋ</ta>
            <ta e="T602" id="Seg_8701" s="T601">qalɨ-enǯɨ-n</ta>
            <ta e="T603" id="Seg_8702" s="T602">to</ta>
            <ta e="T604" id="Seg_8703" s="T603">ut-ɨ-m-tə</ta>
            <ta e="T605" id="Seg_8704" s="T604">moqɨnä</ta>
            <ta e="T606" id="Seg_8705" s="T605">saːrə-gu</ta>
            <ta e="T607" id="Seg_8706" s="T606">era</ta>
            <ta e="T608" id="Seg_8707" s="T607">tüː-nɨ</ta>
            <ta e="T609" id="Seg_8708" s="T608">täp</ta>
            <ta e="T610" id="Seg_8709" s="T609">täp-ɨ-m</ta>
            <ta e="T611" id="Seg_8710" s="T610">apsǝ-tɨ-t</ta>
            <ta e="T612" id="Seg_8711" s="T611">tʼärɨ-n</ta>
            <ta e="T613" id="Seg_8712" s="T612">kozɨr-ču-lä-j</ta>
            <ta e="T614" id="Seg_8713" s="T613">era-tə</ta>
            <ta e="T615" id="Seg_8714" s="T614">tʼärɨ-n</ta>
            <ta e="T616" id="Seg_8715" s="T615">man</ta>
            <ta e="T617" id="Seg_8716" s="T616">nunɨ-či-mbɨ-ŋ</ta>
            <ta e="T618" id="Seg_8717" s="T617">nadə</ta>
            <ta e="T619" id="Seg_8718" s="T618">qondu-gu</ta>
            <ta e="T620" id="Seg_8719" s="T619">nu</ta>
            <ta e="T621" id="Seg_8720" s="T620">kozɨr-ču-lä-j</ta>
            <ta e="T622" id="Seg_8721" s="T621">kozɨr-ču-lä-j</ta>
            <ta e="T623" id="Seg_8722" s="T622">xotʼ</ta>
            <ta e="T624" id="Seg_8723" s="T623">okkɨr-ɨ-n</ta>
            <ta e="T625" id="Seg_8724" s="T624">kud</ta>
            <ta e="T626" id="Seg_8725" s="T625">turak-ɨ-ŋ</ta>
            <ta e="T627" id="Seg_8726" s="T626">qalɨ-enǯɨ-n</ta>
            <ta e="T628" id="Seg_8727" s="T627">ut-ɨ-m-tə</ta>
            <ta e="T629" id="Seg_8728" s="T628">moqɨnä</ta>
            <ta e="T630" id="Seg_8729" s="T629">saːrə-gu</ta>
            <ta e="T631" id="Seg_8730" s="T630">jeʒlʼe</ta>
            <ta e="T632" id="Seg_8731" s="T631">man</ta>
            <ta e="T633" id="Seg_8732" s="T632">qalɨ-enǯɨ-ŋ</ta>
            <ta e="T634" id="Seg_8733" s="T633">man</ta>
            <ta e="T635" id="Seg_8734" s="T634">ut-ɨ-w</ta>
            <ta e="T636" id="Seg_8735" s="T635">saːrə-etɨ</ta>
            <ta e="T637" id="Seg_8736" s="T636">moqɨnä</ta>
            <ta e="T638" id="Seg_8737" s="T637">i</ta>
            <ta e="T639" id="Seg_8738" s="T638">omdɨ-qij</ta>
            <ta e="T640" id="Seg_8739" s="T639">kozɨr-ču-le</ta>
            <ta e="T641" id="Seg_8740" s="T640">era-m-tə</ta>
            <ta e="T642" id="Seg_8741" s="T641">turak-ɨ-m-le</ta>
            <ta e="T643" id="Seg_8742" s="T642">qwɛdʼi-t</ta>
            <ta e="T644" id="Seg_8743" s="T643">ut-ɨ-m-tə</ta>
            <ta e="T645" id="Seg_8744" s="T644">moqɨnä</ta>
            <ta e="T646" id="Seg_8745" s="T645">saːrə-t</ta>
            <ta e="T647" id="Seg_8746" s="T646">täp</ta>
            <ta e="T648" id="Seg_8747" s="T647">kak</ta>
            <ta e="T649" id="Seg_8748" s="T648">turugu-lǯi-ntɨ</ta>
            <ta e="T650" id="Seg_8749" s="T649">quːdəgo-la-m</ta>
            <ta e="T651" id="Seg_8750" s="T650">asa</ta>
            <ta e="T652" id="Seg_8751" s="T651">lakču-t</ta>
            <ta e="T653" id="Seg_8752" s="T652">paja-tə-nä</ta>
            <ta e="T654" id="Seg_8753" s="T653">tʼärɨ-n</ta>
            <ta e="T655" id="Seg_8754" s="T654">tʼekkə-etɨ</ta>
            <ta e="T656" id="Seg_8755" s="T655">ut-ɨ-w</ta>
            <ta e="T657" id="Seg_8756" s="T656">paja-tə</ta>
            <ta e="T658" id="Seg_8757" s="T657">tʼärɨ-n</ta>
            <ta e="T659" id="Seg_8758" s="T658">lakču-etɨ</ta>
            <ta e="T660" id="Seg_8759" s="T659">täp</ta>
            <ta e="T661" id="Seg_8760" s="T660">aj</ta>
            <ta e="T662" id="Seg_8761" s="T661">turugu-lǯi-n</ta>
            <ta e="T663" id="Seg_8762" s="T662">asa</ta>
            <ta e="T664" id="Seg_8763" s="T663">lakču-t</ta>
            <ta e="T665" id="Seg_8764" s="T664">tʼärɨ-n</ta>
            <ta e="T666" id="Seg_8765" s="T665">tʼekkə-etɨ</ta>
            <ta e="T667" id="Seg_8766" s="T666">asa</ta>
            <ta e="T668" id="Seg_8767" s="T667">tʼekkə-enǯɨ-w</ta>
            <ta e="T669" id="Seg_8768" s="T668">poːne</ta>
            <ta e="T670" id="Seg_8769" s="T669">sapɨsi-le</ta>
            <ta e="T671" id="Seg_8770" s="T670">qwan-nɨ-n</ta>
            <ta e="T672" id="Seg_8771" s="T671">i</ta>
            <ta e="T673" id="Seg_8772" s="T672">na</ta>
            <ta e="T674" id="Seg_8773" s="T673">laŋgoj-ntɨ</ta>
            <ta e="T675" id="Seg_8774" s="T674">tüː-kɨ</ta>
            <ta e="T676" id="Seg_8775" s="T675">i</ta>
            <ta e="T677" id="Seg_8776" s="T676">na</ta>
            <ta e="T678" id="Seg_8777" s="T677">razbojnik</ta>
            <ta e="T679" id="Seg_8778" s="T678">na</ta>
            <ta e="T680" id="Seg_8779" s="T679">tüː-ntɨ</ta>
            <ta e="T681" id="Seg_8780" s="T680">tʼärɨ-n</ta>
            <ta e="T682" id="Seg_8781" s="T681">era-w</ta>
            <ta e="T683" id="Seg_8782" s="T682">quːdəgo-m</ta>
            <ta e="T684" id="Seg_8783" s="T683">asa</ta>
            <ta e="T685" id="Seg_8784" s="T684">lakču-t</ta>
            <ta e="T686" id="Seg_8785" s="T685">ser-le</ta>
            <ta e="T687" id="Seg_8786" s="T686">qwat-lä-j</ta>
            <ta e="T688" id="Seg_8787" s="T687">i</ta>
            <ta e="T689" id="Seg_8788" s="T688">maːt-ntə</ta>
            <ta e="T690" id="Seg_8789" s="T689">ser-nɨ-qij</ta>
            <ta e="T691" id="Seg_8790" s="T690">razbojnik</ta>
            <ta e="T692" id="Seg_8791" s="T691">tʼärɨ-n</ta>
            <ta e="T693" id="Seg_8792" s="T692">qare-dʼel-n</ta>
            <ta e="T694" id="Seg_8793" s="T693">qare-mɨ-qɨn</ta>
            <ta e="T695" id="Seg_8794" s="T694">qwat-enǯɨ-j</ta>
            <ta e="T696" id="Seg_8795" s="T695">tulǯu-etɨ</ta>
            <ta e="T697" id="Seg_8796" s="T696">wedro-n</ta>
            <ta e="T698" id="Seg_8797" s="T697">tʼir</ta>
            <ta e="T699" id="Seg_8798" s="T698">saq</ta>
            <ta e="T700" id="Seg_8799" s="T699">täp-ɨ-m</ta>
            <ta e="T701" id="Seg_8800" s="T700">saq-ɨ-n</ta>
            <ta e="T702" id="Seg_8801" s="T701">par-ɨ-ntə</ta>
            <ta e="T703" id="Seg_8802" s="T702">pulsaj-lä-ntɨ-se</ta>
            <ta e="T704" id="Seg_8803" s="T703">saq-ɨ-ntə</ta>
            <ta e="T705" id="Seg_8804" s="T704">omdɨ-lǯi-enǯɨ-j</ta>
            <ta e="T706" id="Seg_8805" s="T705">qare-miː-ntə</ta>
            <ta e="T707" id="Seg_8806" s="T706">amdɨ</ta>
            <ta e="T708" id="Seg_8807" s="T707">qare-mɨ-qɨn</ta>
            <ta e="T709" id="Seg_8808" s="T708">qwat-enǯɨ-j</ta>
            <ta e="T710" id="Seg_8809" s="T709">ne-lʼ-qum</ta>
            <ta e="T711" id="Seg_8810" s="T710">wandǝ-qɨn</ta>
            <ta e="T712" id="Seg_8811" s="T711">tüː-le</ta>
            <ta e="T713" id="Seg_8812" s="T712">na</ta>
            <ta e="T714" id="Seg_8813" s="T713">sösu-ntɨ-t</ta>
            <ta e="T715" id="Seg_8814" s="T714">tan</ta>
            <ta e="T716" id="Seg_8815" s="T715">tʼärɨ-ku-sɨ-ntə</ta>
            <ta e="T717" id="Seg_8816" s="T716">što</ta>
            <ta e="T718" id="Seg_8817" s="T717">man</ta>
            <ta e="T719" id="Seg_8818" s="T718">or-se</ta>
            <ta e="T720" id="Seg_8819" s="T719">eː-nɨ-ŋ</ta>
            <ta e="T721" id="Seg_8820" s="T720">a</ta>
            <ta e="T722" id="Seg_8821" s="T721">qaj-no</ta>
            <ta e="T723" id="Seg_8822" s="T722">quːdəgo-m</ta>
            <ta e="T724" id="Seg_8823" s="T723">asa</ta>
            <ta e="T725" id="Seg_8824" s="T724">lakču-enǯɨ-l</ta>
            <ta e="T726" id="Seg_8825" s="T725">teper</ta>
            <ta e="T727" id="Seg_8826" s="T726">qare-mɨ-qɨn</ta>
            <ta e="T728" id="Seg_8827" s="T727">tastɨ</ta>
            <ta e="T729" id="Seg_8828" s="T728">qwat-enǯɨ-j</ta>
            <ta e="T730" id="Seg_8829" s="T729">ne-lʼ-qum</ta>
            <ta e="T731" id="Seg_8830" s="T730">kur-ol-nɨ-n</ta>
            <ta e="T732" id="Seg_8831" s="T731">saq</ta>
            <ta e="T733" id="Seg_8832" s="T732">tulǯu-ntɨ-t</ta>
            <ta e="T734" id="Seg_8833" s="T733">wedro-n</ta>
            <ta e="T735" id="Seg_8834" s="T734">tʼir</ta>
            <ta e="T736" id="Seg_8835" s="T735">i</ta>
            <ta e="T737" id="Seg_8836" s="T736">saltšibo-ntə</ta>
            <ta e="T738" id="Seg_8837" s="T737">qamǯu-t</ta>
            <ta e="T739" id="Seg_8838" s="T738">i</ta>
            <ta e="T740" id="Seg_8839" s="T739">täp-ɨ-m</ta>
            <ta e="T741" id="Seg_8840" s="T740">pulsaj-n</ta>
            <ta e="T742" id="Seg_8841" s="T741">omdɨ-lǯɨ-t</ta>
            <ta e="T743" id="Seg_8842" s="T742">tʼärɨ-n</ta>
            <ta e="T744" id="Seg_8843" s="T743">amdɨ-kɨ</ta>
            <ta e="T745" id="Seg_8844" s="T744">qare-mɨ-ntə</ta>
            <ta e="T746" id="Seg_8845" s="T745">qare-mɨ-qɨn</ta>
            <ta e="T747" id="Seg_8846" s="T746">tastɨ</ta>
            <ta e="T748" id="Seg_8847" s="T747">qwat-enǯɨ-j</ta>
            <ta e="T749" id="Seg_8848" s="T748">omdɨ-qij</ta>
            <ta e="T750" id="Seg_8849" s="T749">am-r-le</ta>
            <ta e="T751" id="Seg_8850" s="T750">razbojnik-se</ta>
            <ta e="T752" id="Seg_8851" s="T751">araŋka-lʼ</ta>
            <ta e="T753" id="Seg_8852" s="T752">öru-le</ta>
            <ta e="T754" id="Seg_8853" s="T753">übɨ-r-ɨ-qij</ta>
            <ta e="T755" id="Seg_8854" s="T754">ne-lʼ-qum</ta>
            <ta e="T756" id="Seg_8855" s="T755">tüː-nɨ</ta>
            <ta e="T757" id="Seg_8856" s="T756">era-n-tə</ta>
            <ta e="T758" id="Seg_8857" s="T757">kö-ntə</ta>
            <ta e="T759" id="Seg_8858" s="T758">era-m-tə</ta>
            <ta e="T760" id="Seg_8859" s="T759">wandǝ-ɨ-ntə</ta>
            <ta e="T761" id="Seg_8860" s="T760">na</ta>
            <ta e="T762" id="Seg_8861" s="T761">sösu-ntɨ-t</ta>
            <ta e="T763" id="Seg_8862" s="T762">qare-mɨ-qɨn</ta>
            <ta e="T764" id="Seg_8863" s="T763">me</ta>
            <ta e="T765" id="Seg_8864" s="T764">tastɨ</ta>
            <ta e="T766" id="Seg_8865" s="T765">qwat-enǯɨ-j</ta>
            <ta e="T767" id="Seg_8866" s="T766">ondə</ta>
            <ta e="T768" id="Seg_8867" s="T767">na</ta>
            <ta e="T769" id="Seg_8868" s="T768">qwan-ntɨ</ta>
            <ta e="T770" id="Seg_8869" s="T769">razbojnik-ɨ-m</ta>
            <ta e="T771" id="Seg_8870" s="T770">kaːwal-ku-t</ta>
            <ta e="T772" id="Seg_8871" s="T771">nidə-l-le</ta>
            <ta e="T773" id="Seg_8872" s="T772">tat-ɨ-r-ɨ-t</ta>
            <ta e="T774" id="Seg_8873" s="T773">i</ta>
            <ta e="T775" id="Seg_8874" s="T774">qondu-gu</ta>
            <ta e="T776" id="Seg_8875" s="T775">quʒal-nɨ-qij</ta>
            <ta e="T777" id="Seg_8876" s="T776">i</ta>
            <ta e="T778" id="Seg_8877" s="T777">na</ta>
            <ta e="T779" id="Seg_8878" s="T778">qondu-lǯi-qij</ta>
            <ta e="T780" id="Seg_8879" s="T779">era-tə</ta>
            <ta e="T781" id="Seg_8880" s="T780">täp</ta>
            <ta e="T782" id="Seg_8881" s="T781">wazɨ-n</ta>
            <ta e="T783" id="Seg_8882" s="T782">innä</ta>
            <ta e="T784" id="Seg_8883" s="T783">palʼdʼi-ku-n</ta>
            <ta e="T785" id="Seg_8884" s="T784">a</ta>
            <ta e="T786" id="Seg_8885" s="T785">qɨba-ŋ</ta>
            <ta e="T787" id="Seg_8886" s="T786">ne-ka-tə</ta>
            <ta e="T788" id="Seg_8887" s="T787">iː-ka-tə</ta>
            <ta e="T789" id="Seg_8888" s="T788">täp-ɨ-staɣɨ</ta>
            <ta e="T790" id="Seg_8889" s="T789">qondu-sɨ-qij</ta>
            <ta e="T791" id="Seg_8890" s="T790">palati-n</ta>
            <ta e="T792" id="Seg_8891" s="T791">par-ɨ-qɨn</ta>
            <ta e="T793" id="Seg_8892" s="T792">ne-ka-tə</ta>
            <ta e="T794" id="Seg_8893" s="T793">qözu-gu</ta>
            <ta e="T795" id="Seg_8894" s="T794">ilʼlʼe</ta>
            <ta e="T796" id="Seg_8895" s="T795">tʼötʼöu-n</ta>
            <ta e="T797" id="Seg_8896" s="T796">täp</ta>
            <ta e="T798" id="Seg_8897" s="T797">ne-ka-ntɨ-nä</ta>
            <ta e="T799" id="Seg_8898" s="T798">tʼärɨ-n</ta>
            <ta e="T800" id="Seg_8899" s="T799">man</ta>
            <ta e="T801" id="Seg_8900" s="T800">ut-la-w</ta>
            <ta e="T802" id="Seg_8901" s="T801">tʼekkə-lä-l</ta>
            <ta e="T803" id="Seg_8902" s="T802">a</ta>
            <ta e="T804" id="Seg_8903" s="T803">ne-ka-tə</ta>
            <ta e="T805" id="Seg_8904" s="T804">tʼärɨ-n</ta>
            <ta e="T806" id="Seg_8905" s="T805">tan</ta>
            <ta e="T807" id="Seg_8906" s="T806">tʼärɨ-ku-sɨ-ntə</ta>
            <ta e="T808" id="Seg_8907" s="T807">što</ta>
            <ta e="T809" id="Seg_8908" s="T808">or-se</ta>
            <ta e="T810" id="Seg_8909" s="T809">eː-nɨ-ŋ</ta>
            <ta e="T811" id="Seg_8910" s="T810">lakču-lǯi-etɨ</ta>
            <ta e="T812" id="Seg_8911" s="T811">quːdəgo-m</ta>
            <ta e="T813" id="Seg_8912" s="T812">palati-n</ta>
            <ta e="T814" id="Seg_8913" s="T813">par-ɨ-ntə</ta>
            <ta e="T815" id="Seg_8914" s="T814">čaǯɨ-n</ta>
            <ta e="T816" id="Seg_8915" s="T815">i</ta>
            <ta e="T817" id="Seg_8916" s="T816">qondu</ta>
            <ta e="T818" id="Seg_8917" s="T817">iː-ka-tə</ta>
            <ta e="T819" id="Seg_8918" s="T818">palati-qɨntɨ</ta>
            <ta e="T820" id="Seg_8919" s="T819">ilʼlʼe</ta>
            <ta e="T821" id="Seg_8920" s="T820">tʼötʼöu-n</ta>
            <ta e="T822" id="Seg_8921" s="T821">qözu-gu</ta>
            <ta e="T823" id="Seg_8922" s="T822">täp</ta>
            <ta e="T824" id="Seg_8923" s="T823">iː-ka-tə-nä</ta>
            <ta e="T825" id="Seg_8924" s="T824">tʼärɨ-n</ta>
            <ta e="T826" id="Seg_8925" s="T825">man</ta>
            <ta e="T827" id="Seg_8926" s="T826">ut-ɨ-w</ta>
            <ta e="T828" id="Seg_8927" s="T827">tʼekkə-lä-l</ta>
            <ta e="T829" id="Seg_8928" s="T828">täp</ta>
            <ta e="T830" id="Seg_8929" s="T829">tüː-nɨ</ta>
            <ta e="T831" id="Seg_8930" s="T830">aze-tə-nä</ta>
            <ta e="T832" id="Seg_8931" s="T831">i</ta>
            <ta e="T833" id="Seg_8932" s="T832">tʼekkə-le</ta>
            <ta e="T834" id="Seg_8933" s="T833">übɨ-r-ɨ-t</ta>
            <ta e="T835" id="Seg_8934" s="T834">asa</ta>
            <ta e="T836" id="Seg_8935" s="T835">tʼekkə-t</ta>
            <ta e="T837" id="Seg_8936" s="T836">mekka</ta>
            <ta e="T838" id="Seg_8937" s="T837">asa</ta>
            <ta e="T839" id="Seg_8938" s="T838">tʼekkə-gu</ta>
            <ta e="T840" id="Seg_8939" s="T839">aze-tə</ta>
            <ta e="T841" id="Seg_8940" s="T840">tʼärɨ-n</ta>
            <ta e="T842" id="Seg_8941" s="T841">patpilka</ta>
            <ta e="T843" id="Seg_8942" s="T842">tat-kɨ</ta>
            <ta e="T844" id="Seg_8943" s="T843">täp</ta>
            <ta e="T845" id="Seg_8944" s="T844">tat-nɨ-t</ta>
            <ta e="T846" id="Seg_8945" s="T845">i</ta>
            <ta e="T847" id="Seg_8946" s="T846">quːdəgo-la-m</ta>
            <ta e="T848" id="Seg_8947" s="T847">sɨlɨ-l-le</ta>
            <ta e="T849" id="Seg_8948" s="T848">na</ta>
            <ta e="T850" id="Seg_8949" s="T849">übɨ-r-ɨ-ntɨ-t</ta>
            <ta e="T851" id="Seg_8950" s="T850">sɨlɨ-t</ta>
            <ta e="T852" id="Seg_8951" s="T851">quːdəgo-la</ta>
            <ta e="T853" id="Seg_8952" s="T852">warsɨ-wat-nɨ-tɨn</ta>
            <ta e="T854" id="Seg_8953" s="T853">täp</ta>
            <ta e="T855" id="Seg_8954" s="T854">iː-ka-tə-nä</ta>
            <ta e="T856" id="Seg_8955" s="T855">tʼärɨ-n</ta>
            <ta e="T857" id="Seg_8956" s="T856">qwan-le</ta>
            <ta e="T858" id="Seg_8957" s="T857">qondu-kɨ</ta>
            <ta e="T859" id="Seg_8958" s="T858">qare-mɨ-qɨn</ta>
            <ta e="T860" id="Seg_8959" s="T859">igə</ta>
            <ta e="T861" id="Seg_8960" s="T860">*larɨ-mbɨ-kɨ</ta>
            <ta e="T862" id="Seg_8961" s="T861">me</ta>
            <ta e="T863" id="Seg_8962" s="T862">tan-se</ta>
            <ta e="T864" id="Seg_8963" s="T863">onäj</ta>
            <ta e="T865" id="Seg_8964" s="T864">elɨ-enǯɨ-j</ta>
            <ta e="T866" id="Seg_8965" s="T865">nom</ta>
            <ta e="T867" id="Seg_8966" s="T866">dʼel-ɨ-m-le</ta>
            <ta e="T868" id="Seg_8967" s="T867">übɨ-r-ɨ-n</ta>
            <ta e="T869" id="Seg_8968" s="T868">täp</ta>
            <ta e="T870" id="Seg_8969" s="T869">quːdəgo-la-m</ta>
            <ta e="T871" id="Seg_8970" s="T870">ut-qɨntɨ</ta>
            <ta e="T872" id="Seg_8971" s="T871">pen-nɨ-t</ta>
            <ta e="T875" id="Seg_8972" s="T874">saːrə-mbɨ-ntɨ</ta>
            <ta e="T876" id="Seg_8973" s="T875">i</ta>
            <ta e="T877" id="Seg_8974" s="T876">omdɨ-n</ta>
            <ta e="T878" id="Seg_8975" s="T877">pulsaj-n</ta>
            <ta e="T879" id="Seg_8976" s="T878">par-ɨ-n</ta>
            <ta e="T880" id="Seg_8977" s="T879">paja-tə</ta>
            <ta e="T881" id="Seg_8978" s="T880">wazɨ-n</ta>
            <ta e="T882" id="Seg_8979" s="T881">tüː-le</ta>
            <ta e="T883" id="Seg_8980" s="T882">wandǝ-ɨ-n</ta>
            <ta e="T884" id="Seg_8981" s="T883">sösu-t</ta>
            <ta e="T885" id="Seg_8982" s="T884">razbojnik-nä</ta>
            <ta e="T886" id="Seg_8983" s="T885">tʼärɨ-n</ta>
            <ta e="T887" id="Seg_8984" s="T886">nu</ta>
            <ta e="T888" id="Seg_8985" s="T887">qwat-lä-j</ta>
            <ta e="T889" id="Seg_8986" s="T888">a</ta>
            <ta e="T890" id="Seg_8987" s="T889">razbojnik</ta>
            <ta e="T891" id="Seg_8988" s="T890">tʼärɨ-n</ta>
            <ta e="T892" id="Seg_8989" s="T891">am-r-le</ta>
            <ta e="T893" id="Seg_8990" s="T892">qwan-lä-j-s</ta>
            <ta e="T894" id="Seg_8991" s="T893">täp</ta>
            <ta e="T895" id="Seg_8992" s="T894">wadʼi-la-m</ta>
            <ta e="T896" id="Seg_8993" s="T895">mözu-r-qɨl-nɨ-t</ta>
            <ta e="T897" id="Seg_8994" s="T896">am-r-nɨ-qij</ta>
            <ta e="T898" id="Seg_8995" s="T897">nu</ta>
            <ta e="T899" id="Seg_8996" s="T898">teper</ta>
            <ta e="T900" id="Seg_8997" s="T899">qwat-lä-j</ta>
            <ta e="T901" id="Seg_8998" s="T900">maːt-qɨn</ta>
            <ta e="T902" id="Seg_8999" s="T901">qwat-enǯɨ-j</ta>
            <ta e="T903" id="Seg_9000" s="T902">alʼi</ta>
            <ta e="T904" id="Seg_9001" s="T903">poːne-n</ta>
            <ta e="T905" id="Seg_9002" s="T904">razbojnik</ta>
            <ta e="T906" id="Seg_9003" s="T905">tʼärɨ-n</ta>
            <ta e="T907" id="Seg_9004" s="T906">poːne-n</ta>
            <ta e="T908" id="Seg_9005" s="T907">qwat-enǯɨ-j</ta>
            <ta e="T909" id="Seg_9006" s="T908">nu</ta>
            <ta e="T910" id="Seg_9007" s="T909">wazɨ-kɨ</ta>
            <ta e="T911" id="Seg_9008" s="T910">čaǯɨ-kɨ</ta>
            <ta e="T912" id="Seg_9009" s="T911">poːne</ta>
            <ta e="T913" id="Seg_9010" s="T912">tastɨ</ta>
            <ta e="T914" id="Seg_9011" s="T913">qwat-enǯɨ-j</ta>
            <ta e="T915" id="Seg_9012" s="T914">qaj</ta>
            <ta e="T916" id="Seg_9013" s="T915">quːdəgo-la-m</ta>
            <ta e="T917" id="Seg_9014" s="T916">asa</ta>
            <ta e="T918" id="Seg_9015" s="T917">lakču-lǯi-l</ta>
            <ta e="T919" id="Seg_9016" s="T918">tan</ta>
            <ta e="T920" id="Seg_9017" s="T919">tʼärɨ-ku-sɨ-ntə</ta>
            <ta e="T921" id="Seg_9018" s="T920">or-se</ta>
            <ta e="T922" id="Seg_9019" s="T921">eː-nɨ-ŋ</ta>
            <ta e="T923" id="Seg_9020" s="T922">ne-lʼ-qum</ta>
            <ta e="T924" id="Seg_9021" s="T923">kak</ta>
            <ta e="T925" id="Seg_9022" s="T924">wandǝ-ɨ-ntə</ta>
            <ta e="T926" id="Seg_9023" s="T925">sösu-ntɨ-t</ta>
            <ta e="T927" id="Seg_9024" s="T926">täp</ta>
            <ta e="T928" id="Seg_9025" s="T927">kak</ta>
            <ta e="T929" id="Seg_9026" s="T928">innä</ta>
            <ta e="T930" id="Seg_9027" s="T929">wazʼe-zi-ntɨ</ta>
            <ta e="T931" id="Seg_9028" s="T930">kak</ta>
            <ta e="T932" id="Seg_9029" s="T931">razbojnik-ɨ-m</ta>
            <ta e="T933" id="Seg_9030" s="T932">qättɨ-ntɨ-t</ta>
            <ta e="T934" id="Seg_9031" s="T933">razbojnik</ta>
            <ta e="T935" id="Seg_9032" s="T934">qotə</ta>
            <ta e="T936" id="Seg_9033" s="T935">*čäčo-lǯi-n</ta>
            <ta e="T937" id="Seg_9034" s="T936">*čäčo-dʼi-n</ta>
            <ta e="T938" id="Seg_9035" s="T937">i</ta>
            <ta e="T939" id="Seg_9036" s="T938">qwat-nɨ-t</ta>
            <ta e="T940" id="Seg_9037" s="T939">a</ta>
            <ta e="T941" id="Seg_9038" s="T940">paja-tə</ta>
            <ta e="T942" id="Seg_9039" s="T941">sоj-ɨ-qɨntɨ</ta>
            <ta e="T943" id="Seg_9040" s="T942">ɨt-ɨ-dʼi-n</ta>
            <ta e="T944" id="Seg_9041" s="T943">tʼärɨ-n</ta>
            <ta e="T945" id="Seg_9042" s="T944">man</ta>
            <ta e="T946" id="Seg_9043" s="T945">nʼilʼdʼi-ŋ</ta>
            <ta e="T947" id="Seg_9044" s="T946">asa</ta>
            <ta e="T948" id="Seg_9045" s="T947">meː-ku-enǯɨ-w</ta>
            <ta e="T949" id="Seg_9046" s="T948">tob-la-m-ntɨ</ta>
            <ta e="T950" id="Seg_9047" s="T949">müzulǯu-ku-enǯɨ-w</ta>
            <ta e="T951" id="Seg_9048" s="T950">na</ta>
            <ta e="T952" id="Seg_9049" s="T951">üt-ɨ-m</ta>
            <ta e="T953" id="Seg_9050" s="T952">üt-ku-enǯɨ-w</ta>
            <ta e="T954" id="Seg_9051" s="T953">era-tə</ta>
            <ta e="T955" id="Seg_9052" s="T954">kak</ta>
            <ta e="T956" id="Seg_9053" s="T955">qättɨ-lǯi-t</ta>
            <ta e="T957" id="Seg_9054" s="T956">i</ta>
            <ta e="T958" id="Seg_9055" s="T957">qwat-nɨ-t</ta>
            <ta e="T959" id="Seg_9056" s="T958">qɨbanʼaǯa-la</ta>
            <ta e="T960" id="Seg_9057" s="T959">tʼüru-le</ta>
            <ta e="T961" id="Seg_9058" s="T960">übɨ-r-ɨ-tɨn</ta>
            <ta e="T962" id="Seg_9059" s="T961">ne-ka-m-tə</ta>
            <ta e="T963" id="Seg_9060" s="T962">kak</ta>
            <ta e="T964" id="Seg_9061" s="T963">qättɨ-ntɨ-t</ta>
            <ta e="T965" id="Seg_9062" s="T964">i</ta>
            <ta e="T966" id="Seg_9063" s="T965">qwat-nɨ-t</ta>
            <ta e="T967" id="Seg_9064" s="T966">i</ta>
            <ta e="T968" id="Seg_9065" s="T967">iː-ka-tə-nä</ta>
            <ta e="T969" id="Seg_9066" s="T968">tʼärɨ-n</ta>
            <ta e="T970" id="Seg_9067" s="T969">me</ta>
            <ta e="T971" id="Seg_9068" s="T970">tan-se</ta>
            <ta e="T972" id="Seg_9069" s="T971">elɨ-enǯɨ-j</ta>
            <ta e="T973" id="Seg_9070" s="T972">igə</ta>
            <ta e="T974" id="Seg_9071" s="T973">tʼüru-kɨ</ta>
            <ta e="T975" id="Seg_9072" s="T974">qwan-nɨ-n</ta>
            <ta e="T976" id="Seg_9073" s="T975">tʼü-lʼ</ta>
            <ta e="T977" id="Seg_9074" s="T976">paqə-ntɨ-le</ta>
            <ta e="T978" id="Seg_9075" s="T977">tʼü-lʼ</ta>
            <ta e="T979" id="Seg_9076" s="T978">qɨl-ɨ-m</ta>
            <ta e="T980" id="Seg_9077" s="T979">paqə-nɨ-t</ta>
            <ta e="T981" id="Seg_9078" s="T980">täp-la-m</ta>
            <ta e="T982" id="Seg_9079" s="T981">*natʼe-n</ta>
            <ta e="T983" id="Seg_9080" s="T982">qal-qɨl-ɨ-t</ta>
            <ta e="T984" id="Seg_9081" s="T983">qal-nɨ-t</ta>
            <ta e="T985" id="Seg_9082" s="T984">i</ta>
            <ta e="T986" id="Seg_9083" s="T985">tʼü-se</ta>
            <ta e="T987" id="Seg_9084" s="T986">taq-qɨl-nɨ-t</ta>
            <ta e="T988" id="Seg_9085" s="T987">zawoznʼik-ɨ-m-tə</ta>
            <ta e="T989" id="Seg_9086" s="T988">qare</ta>
            <ta e="T990" id="Seg_9087" s="T989">ü-qɨl-le</ta>
            <ta e="T991" id="Seg_9088" s="T990">qwan-tɨ-t</ta>
            <ta e="T992" id="Seg_9089" s="T991">tädomɨ-la-m-tə</ta>
            <ta e="T993" id="Seg_9090" s="T992">wesʼ</ta>
            <ta e="T994" id="Seg_9091" s="T993">tugul-nɨ-t</ta>
            <ta e="T995" id="Seg_9092" s="T994">iː-ka-ntɨ-se</ta>
            <ta e="T996" id="Seg_9093" s="T995">omdɨ-qij</ta>
            <ta e="T997" id="Seg_9094" s="T996">i</ta>
            <ta e="T998" id="Seg_9095" s="T997">na</ta>
            <ta e="T999" id="Seg_9096" s="T998">qwan-ntɨ-qij</ta>
            <ta e="T1000" id="Seg_9097" s="T999">tüː-le</ta>
            <ta e="T1001" id="Seg_9098" s="T1000">medə-qij</ta>
            <ta e="T1002" id="Seg_9099" s="T1001">ondə</ta>
            <ta e="T1003" id="Seg_9100" s="T1002">eːde-qɨntɨ</ta>
            <ta e="T1004" id="Seg_9101" s="T1003">qum-la-tə</ta>
            <ta e="T1005" id="Seg_9102" s="T1004">aːndal-mbɨ-tɨn</ta>
            <ta e="T1006" id="Seg_9103" s="T1005">a</ta>
            <ta e="T1007" id="Seg_9104" s="T1006">paja-l</ta>
            <ta e="T1008" id="Seg_9105" s="T1007">qutʼet</ta>
            <ta e="T1009" id="Seg_9106" s="T1008">eː-n</ta>
            <ta e="T1010" id="Seg_9107" s="T1009">paja-w</ta>
            <ta e="T1011" id="Seg_9108" s="T1010">quː-mbɨ</ta>
            <ta e="T1012" id="Seg_9109" s="T1011">wesʼ</ta>
            <ta e="T1013" id="Seg_9110" s="T1012">tugul-nɨ-tɨn</ta>
            <ta e="T1014" id="Seg_9111" s="T1013">tädomɨ-m</ta>
            <ta e="T1015" id="Seg_9112" s="T1014">täp</ta>
            <ta e="T1016" id="Seg_9113" s="T1015">pirä-qɨntɨ</ta>
            <ta e="T1017" id="Seg_9114" s="T1016">maːt</ta>
            <ta e="T1018" id="Seg_9115" s="T1017">taw-ntɨ-t</ta>
            <ta e="T1019" id="Seg_9116" s="T1018">i</ta>
            <ta e="T1020" id="Seg_9117" s="T1019">nedɨ-n</ta>
            <ta e="T1021" id="Seg_9118" s="T1020">i</ta>
            <ta e="T1022" id="Seg_9119" s="T1021">warkɨ-le</ta>
            <ta e="T1023" id="Seg_9120" s="T1022">na</ta>
            <ta e="T1024" id="Seg_9121" s="T1023">übɨ-r-ɨ-ntɨ</ta>
            <ta e="T1025" id="Seg_9122" s="T1024">man</ta>
            <ta e="T1026" id="Seg_9123" s="T1025">pondəbon</ta>
            <ta e="T1027" id="Seg_9124" s="T1026">täp-nan</ta>
            <ta e="T1028" id="Seg_9125" s="T1027">eː-sɨ-ŋ</ta>
            <ta e="T1029" id="Seg_9126" s="T1028">tatawa</ta>
            <ta e="T1030" id="Seg_9127" s="T1029">soːdʼiga-ŋ</ta>
            <ta e="T1031" id="Seg_9128" s="T1030">warkɨ</ta>
            <ta e="T1032" id="Seg_9129" s="T1031">paja-tə</ta>
            <ta e="T1033" id="Seg_9130" s="T1032">soːdʼiga</ta>
            <ta e="T1034" id="Seg_9131" s="T1033">mazɨm</ta>
            <ta e="T1035" id="Seg_9132" s="T1034">čʼai-la-se</ta>
            <ta e="T1036" id="Seg_9133" s="T1035">mazɨm</ta>
            <ta e="T1037" id="Seg_9134" s="T1036">öru-ču-sɨ</ta>
            <ta e="T1038" id="Seg_9135" s="T1037">mekka</ta>
            <ta e="T1039" id="Seg_9136" s="T1038">tʼärɨ-n</ta>
            <ta e="T1040" id="Seg_9137" s="T1039">me-quntu</ta>
            <ta e="T1041" id="Seg_9138" s="T1040">palʼdʼi-ku-kɨ</ta>
            <ta e="T1042" id="Seg_9139" s="T1041">man</ta>
            <ta e="T1043" id="Seg_9140" s="T1042">täp-la-nä</ta>
            <ta e="T1044" id="Seg_9141" s="T1043">qɨdan</ta>
            <ta e="T1045" id="Seg_9142" s="T1044">udɨr-ɨ-ku-nɨ-ŋ</ta>
            <ta e="T1046" id="Seg_9143" s="T1045">taw-po-n</ta>
            <ta e="T1047" id="Seg_9144" s="T1046">eːde-ntə</ta>
            <ta e="T1048" id="Seg_9145" s="T1047">qwan-enǯɨ-ŋ</ta>
            <ta e="T1049" id="Seg_9146" s="T1048">täp-la-nä</ta>
            <ta e="T1050" id="Seg_9147" s="T1049">aj</ta>
            <ta e="T1051" id="Seg_9148" s="T1050">ser-ku-enǯɨ-ŋ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_9149" s="T1">Xristjan.[NOM]</ta>
            <ta e="T3" id="Seg_9150" s="T2">old.man.[NOM]</ta>
            <ta e="T4" id="Seg_9151" s="T3">be.born-INFER-3SG.S</ta>
            <ta e="T5" id="Seg_9152" s="T4">Xristjan.[NOM]</ta>
            <ta e="T6" id="Seg_9153" s="T5">old.man.[NOM]</ta>
            <ta e="T7" id="Seg_9154" s="T6">(s)he-ADES</ta>
            <ta e="T8" id="Seg_9155" s="T7">three</ta>
            <ta e="T9" id="Seg_9156" s="T8">son.[NOM]-3SG</ta>
            <ta e="T10" id="Seg_9157" s="T9">son-PL.[NOM]-3SG</ta>
            <ta e="T11" id="Seg_9158" s="T10">all</ta>
            <ta e="T12" id="Seg_9159" s="T11">get.married-RES-3PL</ta>
            <ta e="T13" id="Seg_9160" s="T12">one</ta>
            <ta e="T14" id="Seg_9161" s="T13">son.[NOM]-3SG</ta>
            <ta e="T15" id="Seg_9162" s="T14">all.the.time</ta>
            <ta e="T16" id="Seg_9163" s="T15">wild.animal-CAP-CVB</ta>
            <ta e="T17" id="Seg_9164" s="T16">walk-HAB-3SG.S</ta>
            <ta e="T18" id="Seg_9165" s="T17">elder</ta>
            <ta e="T19" id="Seg_9166" s="T18">son.[NOM]-3SG</ta>
            <ta e="T20" id="Seg_9167" s="T19">and</ta>
            <ta e="T21" id="Seg_9168" s="T20">two</ta>
            <ta e="T22" id="Seg_9169" s="T21">two</ta>
            <ta e="T23" id="Seg_9170" s="T22">son.[NOM]-3SG</ta>
            <ta e="T24" id="Seg_9171" s="T23">cattle-PL-COM</ta>
            <ta e="T25" id="Seg_9172" s="T24">walk-HAB-CO-3DU.S</ta>
            <ta e="T26" id="Seg_9173" s="T25">cow-PL.[NOM]</ta>
            <ta e="T27" id="Seg_9174" s="T26">keep-INFER-3PL</ta>
            <ta e="T28" id="Seg_9175" s="T27">horse-PL.[NOM]</ta>
            <ta e="T29" id="Seg_9176" s="T28">and</ta>
            <ta e="T30" id="Seg_9177" s="T29">sheep-PL.[NOM]</ta>
            <ta e="T31" id="Seg_9178" s="T30">and</ta>
            <ta e="T32" id="Seg_9179" s="T31">ploughland.[NOM]</ta>
            <ta e="T33" id="Seg_9180" s="T32">keep-INFER-3PL</ta>
            <ta e="T34" id="Seg_9181" s="T33">food.[NOM]-3SG</ta>
            <ta e="T35" id="Seg_9182" s="T34">sow-HAB-INFER-3PL</ta>
            <ta e="T36" id="Seg_9183" s="T35">and</ta>
            <ta e="T37" id="Seg_9184" s="T36">elder</ta>
            <ta e="T38" id="Seg_9185" s="T37">son.[NOM]-3SG</ta>
            <ta e="T39" id="Seg_9186" s="T38">all.the.time</ta>
            <ta e="T40" id="Seg_9187" s="T39">taiga-EP-ILL</ta>
            <ta e="T41" id="Seg_9188" s="T40">walk-3SG.S</ta>
            <ta e="T42" id="Seg_9189" s="T41">walk-HAB-3SG.S</ta>
            <ta e="T43" id="Seg_9190" s="T42">(s)he-ADES</ta>
            <ta e="T44" id="Seg_9191" s="T43">daughter.[NOM]-3SG</ta>
            <ta e="T45" id="Seg_9192" s="T44">be.born-INFER.[3SG.S]</ta>
            <ta e="T46" id="Seg_9193" s="T45">and</ta>
            <ta e="T47" id="Seg_9194" s="T46">son-DIM.[NOM]</ta>
            <ta e="T48" id="Seg_9195" s="T47">be.born-INFER.[3SG.S]</ta>
            <ta e="T49" id="Seg_9196" s="T48">(s)he-EP-DU.[NOM]</ta>
            <ta e="T50" id="Seg_9197" s="T49">big-ADVZ</ta>
            <ta e="T51" id="Seg_9198" s="T50">become-EP-3DU.S</ta>
            <ta e="T52" id="Seg_9199" s="T51">become-EP-3DU.S</ta>
            <ta e="T53" id="Seg_9200" s="T52">and</ta>
            <ta e="T54" id="Seg_9201" s="T53">(s)he.[NOM]</ta>
            <ta e="T55" id="Seg_9202" s="T54">there-ADV.LOC</ta>
            <ta e="T56" id="Seg_9203" s="T55">taiga-EP-LOC</ta>
            <ta e="T57" id="Seg_9204" s="T56">big</ta>
            <ta e="T58" id="Seg_9205" s="T57">house.[NOM]</ta>
            <ta e="T59" id="Seg_9206" s="T58">sit.down-TR-PST.NAR-INFER-3SG.O</ta>
            <ta e="T60" id="Seg_9207" s="T59">big</ta>
            <ta e="T61" id="Seg_9208" s="T60">house.[NOM]</ta>
            <ta e="T62" id="Seg_9209" s="T61">mother-OBL.3SG-ALL</ta>
            <ta e="T63" id="Seg_9210" s="T62">father-OBL.3SG-ALL</ta>
            <ta e="T64" id="Seg_9211" s="T63">say-3SG.S</ta>
            <ta e="T65" id="Seg_9212" s="T64">icon-INSTR</ta>
            <ta e="T66" id="Seg_9213" s="T65">I.ACC</ta>
            <ta e="T67" id="Seg_9214" s="T66">bless-IMP.2SG.S</ta>
            <ta e="T68" id="Seg_9215" s="T67">mother.[NOM]-3SG</ta>
            <ta e="T69" id="Seg_9216" s="T68">icon-EP-ACC</ta>
            <ta e="T70" id="Seg_9217" s="T69">take-CO-3SG.O</ta>
            <ta e="T71" id="Seg_9218" s="T70">and</ta>
            <ta e="T72" id="Seg_9219" s="T71">bless-CO-3SG.O</ta>
            <ta e="T73" id="Seg_9220" s="T72">I.NOM</ta>
            <ta e="T74" id="Seg_9221" s="T73">leave-FUT-1SG.S</ta>
            <ta e="T75" id="Seg_9222" s="T74">three</ta>
            <ta e="T76" id="Seg_9223" s="T75">brother-DYA-PL.[NOM]-1PL</ta>
            <ta e="T77" id="Seg_9224" s="T76">one</ta>
            <ta e="T78" id="Seg_9225" s="T77">house-LOC</ta>
            <ta e="T79" id="Seg_9226" s="T78">NEG</ta>
            <ta e="T80" id="Seg_9227" s="T79">live-FUT-1PL</ta>
            <ta e="T81" id="Seg_9228" s="T80">I-ADES</ta>
            <ta e="T82" id="Seg_9229" s="T81">now</ta>
            <ta e="T83" id="Seg_9230" s="T82">family.[NOM]-1SG</ta>
            <ta e="T84" id="Seg_9231" s="T83">boat-ILL</ta>
            <ta e="T85" id="Seg_9232" s="T84">all</ta>
            <ta e="T86" id="Seg_9233" s="T85">thing-ACC-3SG</ta>
            <ta e="T87" id="Seg_9234" s="T86">pull.out-CO-3SG.O</ta>
            <ta e="T88" id="Seg_9235" s="T87">and</ta>
            <ta e="T89" id="Seg_9236" s="T88">here</ta>
            <ta e="T90" id="Seg_9237" s="T89">set.off-INFER-3PL</ta>
            <ta e="T91" id="Seg_9238" s="T90">father.[NOM]-3SG</ta>
            <ta e="T92" id="Seg_9239" s="T91">mother.[NOM]-3SG</ta>
            <ta e="T93" id="Seg_9240" s="T92">and</ta>
            <ta e="T94" id="Seg_9241" s="T93">cry-CVB</ta>
            <ta e="T95" id="Seg_9242" s="T94">here</ta>
            <ta e="T96" id="Seg_9243" s="T95">stay-INFER-3PL</ta>
            <ta e="T97" id="Seg_9244" s="T96">and</ta>
            <ta e="T98" id="Seg_9245" s="T97">(s)he-PL.[NOM]</ta>
            <ta e="T99" id="Seg_9246" s="T98">and</ta>
            <ta e="T100" id="Seg_9247" s="T99">here</ta>
            <ta e="T101" id="Seg_9248" s="T100">leave-INFER-3PL</ta>
            <ta e="T102" id="Seg_9249" s="T101">now</ta>
            <ta e="T103" id="Seg_9250" s="T102">and</ta>
            <ta e="T104" id="Seg_9251" s="T103">come-CVB</ta>
            <ta e="T105" id="Seg_9252" s="T104">here</ta>
            <ta e="T106" id="Seg_9253" s="T105">achieve-INFER-3PL</ta>
            <ta e="T107" id="Seg_9254" s="T106">and</ta>
            <ta e="T108" id="Seg_9255" s="T107">(s)he-ADES</ta>
            <ta e="T109" id="Seg_9256" s="T108">there-ADV.LOC</ta>
            <ta e="T110" id="Seg_9257" s="T109">big</ta>
            <ta e="T111" id="Seg_9258" s="T110">house.[NOM]</ta>
            <ta e="T112" id="Seg_9259" s="T111">sit.down-TR-PST.NAR-INFER-3SG.O</ta>
            <ta e="T113" id="Seg_9260" s="T112">all</ta>
            <ta e="T114" id="Seg_9261" s="T113">pull.out-CO-3PL</ta>
            <ta e="T115" id="Seg_9262" s="T114">there-ILL</ta>
            <ta e="T116" id="Seg_9263" s="T115">now</ta>
            <ta e="T117" id="Seg_9264" s="T116">and</ta>
            <ta e="T118" id="Seg_9265" s="T117">husband.[NOM]-3SG</ta>
            <ta e="T119" id="Seg_9266" s="T118">all</ta>
            <ta e="T120" id="Seg_9267" s="T119">do-CVB</ta>
            <ta e="T121" id="Seg_9268" s="T120">bring-EP-FRQ-CO-3SG.O</ta>
            <ta e="T122" id="Seg_9269" s="T121">bed.[NOM]</ta>
            <ta e="T123" id="Seg_9270" s="T122">do-INFER-3SG.O</ta>
            <ta e="T124" id="Seg_9271" s="T123">morning-day-ADV.LOC</ta>
            <ta e="T125" id="Seg_9272" s="T124">taiga-EP-ILL</ta>
            <ta e="T126" id="Seg_9273" s="T125">leave-FUT-1SG.S</ta>
            <ta e="T127" id="Seg_9274" s="T126">and</ta>
            <ta e="T128" id="Seg_9275" s="T127">you.SG.NOM</ta>
            <ta e="T129" id="Seg_9276" s="T128">water-TRL</ta>
            <ta e="T130" id="Seg_9277" s="T129">here</ta>
            <ta e="T131" id="Seg_9278" s="T130">leave-POT.FUT.2SG</ta>
            <ta e="T132" id="Seg_9279" s="T131">leg-ILL.2SG</ta>
            <ta e="T133" id="Seg_9280" s="T132">what-INDEF2</ta>
            <ta e="T134" id="Seg_9281" s="T133">scobs.[NOM]</ta>
            <ta e="T135" id="Seg_9282" s="T134">how-ADVZ</ta>
            <ta e="T136" id="Seg_9283" s="T135">NEG.IMP</ta>
            <ta e="T137" id="Seg_9284" s="T136">become.[3SG.S]</ta>
            <ta e="T138" id="Seg_9285" s="T137">otherwise</ta>
            <ta e="T139" id="Seg_9286" s="T138">bad-ADVZ</ta>
            <ta e="T140" id="Seg_9287" s="T139">be-FUT-3SG.S</ta>
            <ta e="T141" id="Seg_9288" s="T140">now</ta>
            <ta e="T142" id="Seg_9289" s="T141">overnight-3PL</ta>
            <ta e="T143" id="Seg_9290" s="T142">morning-something-LOC</ta>
            <ta e="T144" id="Seg_9291" s="T143">get.up-3SG.S</ta>
            <ta e="T145" id="Seg_9292" s="T144">eat-FRQ-CO-3SG.S</ta>
            <ta e="T146" id="Seg_9293" s="T145">and</ta>
            <ta e="T147" id="Seg_9294" s="T146">here</ta>
            <ta e="T148" id="Seg_9295" s="T147">leave-INFER.[3SG.S]</ta>
            <ta e="T149" id="Seg_9296" s="T148">taiga-EP-ILL</ta>
            <ta e="T150" id="Seg_9297" s="T149">and</ta>
            <ta e="T151" id="Seg_9298" s="T150">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T152" id="Seg_9299" s="T151">say-3SG.S</ta>
            <ta e="T153" id="Seg_9300" s="T152">what-TRL</ta>
            <ta e="T154" id="Seg_9301" s="T153">I.ALL</ta>
            <ta e="T155" id="Seg_9302" s="T154">husband.[NOM]-1SG</ta>
            <ta e="T156" id="Seg_9303" s="T155">say-3SG.S</ta>
            <ta e="T157" id="Seg_9304" s="T156">that</ta>
            <ta e="T158" id="Seg_9305" s="T157">scobs.[NOM]</ta>
            <ta e="T159" id="Seg_9306" s="T158">water-ILL</ta>
            <ta e="T160" id="Seg_9307" s="T159">how-ADVZ</ta>
            <ta e="T161" id="Seg_9308" s="T160">NEG.IMP</ta>
            <ta e="T162" id="Seg_9309" s="T161">become.[3SG.S]</ta>
            <ta e="T163" id="Seg_9310" s="T162">I.NOM</ta>
            <ta e="T164" id="Seg_9311" s="T163">now</ta>
            <ta e="T165" id="Seg_9312" s="T164">two</ta>
            <ta e="T166" id="Seg_9313" s="T165">bucket-ACC</ta>
            <ta e="T167" id="Seg_9314" s="T166">scobs-PL-INSTR</ta>
            <ta e="T168" id="Seg_9315" s="T167">fill-%%-FUT-1SG.O</ta>
            <ta e="T169" id="Seg_9316" s="T168">fill-%%-FUT-1SG.O</ta>
            <ta e="T170" id="Seg_9317" s="T169">leave-CVB</ta>
            <ta e="T171" id="Seg_9318" s="T170">water-ILL</ta>
            <ta e="T172" id="Seg_9319" s="T171">pour.out-FUT-1SG.O</ta>
            <ta e="T173" id="Seg_9320" s="T172">and</ta>
            <ta e="T174" id="Seg_9321" s="T173">here</ta>
            <ta e="T175" id="Seg_9322" s="T174">leave-TR-INFER-3SG.O</ta>
            <ta e="T176" id="Seg_9323" s="T175">and</ta>
            <ta e="T177" id="Seg_9324" s="T176">pour-3SG.O</ta>
            <ta e="T178" id="Seg_9325" s="T177">stand-HAB.[3SG.S]</ta>
            <ta e="T179" id="Seg_9326" s="T178">and</ta>
            <ta e="T180" id="Seg_9327" s="T179">look-DUR-3SG.O</ta>
            <ta e="T181" id="Seg_9328" s="T180">and</ta>
            <ta e="T182" id="Seg_9329" s="T181">there-ADV.LOC</ta>
            <ta e="T183" id="Seg_9330" s="T182">flow.quickly-CO-3SG.S</ta>
            <ta e="T184" id="Seg_9331" s="T183">(s)he.[NOM]</ta>
            <ta e="T185" id="Seg_9332" s="T184">water-EP-ACC</ta>
            <ta e="T186" id="Seg_9333" s="T185">ladle-INCH-CO-3SG.O</ta>
            <ta e="T187" id="Seg_9334" s="T186">and</ta>
            <ta e="T188" id="Seg_9335" s="T187">house-ILL</ta>
            <ta e="T189" id="Seg_9336" s="T188">come-CO.[3SG.S]</ta>
            <ta e="T190" id="Seg_9337" s="T189">and</ta>
            <ta e="T191" id="Seg_9338" s="T190">(s)he.[NOM]</ta>
            <ta e="T192" id="Seg_9339" s="T191">beautiful</ta>
            <ta e="T193" id="Seg_9340" s="T192">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T194" id="Seg_9341" s="T193">be-PST.[3SG.S]</ta>
            <ta e="T195" id="Seg_9342" s="T194">leave-CVB</ta>
            <ta e="T196" id="Seg_9343" s="T195">room-EMPH</ta>
            <ta e="T197" id="Seg_9344" s="T196">house-ILL</ta>
            <ta e="T198" id="Seg_9345" s="T197">sit.down-3SG.S</ta>
            <ta e="T199" id="Seg_9346" s="T198">and</ta>
            <ta e="T200" id="Seg_9347" s="T199">sew-EP-FRQ-CVB</ta>
            <ta e="T201" id="Seg_9348" s="T200">begin-DRV-CO-3SG.S</ta>
            <ta e="T202" id="Seg_9349" s="T201">and</ta>
            <ta e="T203" id="Seg_9350" s="T202">two</ta>
            <ta e="T204" id="Seg_9351" s="T203">child.[NOM]</ta>
            <ta e="T205" id="Seg_9352" s="T204">%play-3DU.S</ta>
            <ta e="T206" id="Seg_9353" s="T205">robber-PL.[NOM]</ta>
            <ta e="T207" id="Seg_9354" s="T206">go-3PL</ta>
            <ta e="T208" id="Seg_9355" s="T207">boat-INSTR</ta>
            <ta e="T209" id="Seg_9356" s="T208">and</ta>
            <ta e="T210" id="Seg_9357" s="T209">say-3PL</ta>
            <ta e="T211" id="Seg_9358" s="T210">what.[NOM]</ta>
            <ta e="T212" id="Seg_9359" s="T211">scobs-PL.[NOM]</ta>
            <ta e="T213" id="Seg_9360" s="T212">flow.quickly-INFER-3PL</ta>
            <ta e="T214" id="Seg_9361" s="T213">quick.stream-INSTR</ta>
            <ta e="T215" id="Seg_9362" s="T214">go-INFER-3PL</ta>
            <ta e="T216" id="Seg_9363" s="T215">steep.bank-GEN</ta>
            <ta e="T217" id="Seg_9364" s="T216">top-EP-ALL</ta>
            <ta e="T218" id="Seg_9365" s="T217">steer-IMP.2SG.O</ta>
            <ta e="T219" id="Seg_9366" s="T218">(s)he-PL.[NOM]</ta>
            <ta e="T220" id="Seg_9367" s="T219">look-DUR-3PL</ta>
            <ta e="T221" id="Seg_9368" s="T220">what-INDEF.[NOM]</ta>
            <ta e="T222" id="Seg_9369" s="T221">road.[NOM]</ta>
            <ta e="T223" id="Seg_9370" s="T222">(s)he-PL.[NOM]</ta>
            <ta e="T224" id="Seg_9371" s="T223">road-DIM.[NOM]</ta>
            <ta e="T225" id="Seg_9372" s="T224">find-INFER-3PL</ta>
            <ta e="T226" id="Seg_9373" s="T225">earlier</ta>
            <ta e="T227" id="Seg_9374" s="T226">we.[NOM]</ta>
            <ta e="T228" id="Seg_9375" s="T227">this</ta>
            <ta e="T229" id="Seg_9376" s="T228">road-ACC</ta>
            <ta e="T230" id="Seg_9377" s="T229">NEG</ta>
            <ta e="T231" id="Seg_9378" s="T230">see-DRV-HAB-PST-1PL</ta>
            <ta e="T232" id="Seg_9379" s="T231">this</ta>
            <ta e="T233" id="Seg_9380" s="T232">road-EP-DIM-ILL</ta>
            <ta e="T234" id="Seg_9381" s="T233">this</ta>
            <ta e="T235" id="Seg_9382" s="T234">road-EP-ILL</ta>
            <ta e="T236" id="Seg_9383" s="T235">this</ta>
            <ta e="T237" id="Seg_9384" s="T236">road-EP-ILL</ta>
            <ta e="T238" id="Seg_9385" s="T237">and</ta>
            <ta e="T239" id="Seg_9386" s="T238">stop-EP-3PL</ta>
            <ta e="T240" id="Seg_9387" s="T239">robber.chief.[NOM]</ta>
            <ta e="T241" id="Seg_9388" s="T240">say-3SG.S</ta>
            <ta e="T242" id="Seg_9389" s="T241">you.PL.NOM</ta>
            <ta e="T243" id="Seg_9390" s="T242">sit-IMP.2PL.S/O</ta>
            <ta e="T244" id="Seg_9391" s="T243">here-ADV.LOC</ta>
            <ta e="T245" id="Seg_9392" s="T244">and</ta>
            <ta e="T246" id="Seg_9393" s="T245">I.NOM</ta>
            <ta e="T247" id="Seg_9394" s="T246">run-MOM-OPT-1SG.S</ta>
            <ta e="T248" id="Seg_9395" s="T247">look-DUR-OPT-1SG.O</ta>
            <ta e="T249" id="Seg_9396" s="T248">what.[NOM]</ta>
            <ta e="T250" id="Seg_9397" s="T249">live-INFER.[3SG.S]</ta>
            <ta e="T251" id="Seg_9398" s="T250">here-ADV.LOC</ta>
            <ta e="T252" id="Seg_9399" s="T251">here</ta>
            <ta e="T253" id="Seg_9400" s="T252">run-MOM-INFER.[3SG.S]</ta>
            <ta e="T254" id="Seg_9401" s="T253">come-CO.[3SG.S]</ta>
            <ta e="T255" id="Seg_9402" s="T254">come.in-CO-3SG.S</ta>
            <ta e="T256" id="Seg_9403" s="T255">house-ILL</ta>
            <ta e="T257" id="Seg_9404" s="T256">hello</ta>
            <ta e="T258" id="Seg_9405" s="T257">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T259" id="Seg_9406" s="T258">say-3SG.S</ta>
            <ta e="T260" id="Seg_9407" s="T259">hello</ta>
            <ta e="T261" id="Seg_9408" s="T260">hello</ta>
            <ta e="T262" id="Seg_9409" s="T261">chair.[NOM]</ta>
            <ta e="T263" id="Seg_9410" s="T262">put-CVB</ta>
            <ta e="T264" id="Seg_9411" s="T263">do-DRV-TR-3SG.O</ta>
            <ta e="T265" id="Seg_9412" s="T264">say-3SG.S</ta>
            <ta e="T266" id="Seg_9413" s="T265">sit.down-IMP.2SG.S</ta>
            <ta e="T267" id="Seg_9414" s="T266">here</ta>
            <ta e="T268" id="Seg_9415" s="T267">and</ta>
            <ta e="T269" id="Seg_9416" s="T268">(s)he.[NOM]</ta>
            <ta e="T270" id="Seg_9417" s="T269">sit.down-3SG.S</ta>
            <ta e="T271" id="Seg_9418" s="T270">speak-CVB</ta>
            <ta e="T272" id="Seg_9419" s="T271">here</ta>
            <ta e="T273" id="Seg_9420" s="T272">begin-DRV-EP-INFER-3DU.S</ta>
            <ta e="T274" id="Seg_9421" s="T273">robber.chief.[NOM]</ta>
            <ta e="T275" id="Seg_9422" s="T274">say-3SG.S</ta>
            <ta e="T276" id="Seg_9423" s="T275">you.SG.NOM</ta>
            <ta e="T277" id="Seg_9424" s="T276">I.ALL</ta>
            <ta e="T278" id="Seg_9425" s="T277">marry-OPT-2SG.S</ta>
            <ta e="T279" id="Seg_9426" s="T278">but</ta>
            <ta e="T280" id="Seg_9427" s="T279">I-ADES</ta>
            <ta e="T281" id="Seg_9428" s="T280">husband.[NOM]-1SG</ta>
            <ta e="T282" id="Seg_9429" s="T281">force-COM</ta>
            <ta e="T283" id="Seg_9430" s="T282">(s)he.[NOM]</ta>
            <ta e="T284" id="Seg_9431" s="T283">you.SG.ACC</ta>
            <ta e="T285" id="Seg_9432" s="T284">kill-FUT-3SG.O</ta>
            <ta e="T286" id="Seg_9433" s="T285">and</ta>
            <ta e="T287" id="Seg_9434" s="T286">I.ACC</ta>
            <ta e="T288" id="Seg_9435" s="T287">kill-FUT-3SG.O</ta>
            <ta e="T289" id="Seg_9436" s="T288">and</ta>
            <ta e="T290" id="Seg_9437" s="T289">I.NOM</ta>
            <ta e="T291" id="Seg_9438" s="T290">you.ALL</ta>
            <ta e="T292" id="Seg_9439" s="T291">rope.[NOM]</ta>
            <ta e="T293" id="Seg_9440" s="T292">do-INFER-FUT-1SG.S</ta>
            <ta e="T294" id="Seg_9441" s="T293">hair.[NOM]</ta>
            <ta e="T295" id="Seg_9442" s="T294">be-POT.FUT.3SG</ta>
            <ta e="T296" id="Seg_9443" s="T295">and</ta>
            <ta e="T299" id="Seg_9444" s="T298">(s)he.[NOM]</ta>
            <ta e="T300" id="Seg_9445" s="T299">this</ta>
            <ta e="T301" id="Seg_9446" s="T300">rope-ACC</ta>
            <ta e="T302" id="Seg_9447" s="T301">NEG</ta>
            <ta e="T303" id="Seg_9448" s="T302">tear-FUT-3SG.O</ta>
            <ta e="T304" id="Seg_9449" s="T303">(s)he.[NOM]</ta>
            <ta e="T305" id="Seg_9450" s="T304">here</ta>
            <ta e="T306" id="Seg_9451" s="T305">come-POT.FUT.3SG</ta>
            <ta e="T307" id="Seg_9452" s="T306">evening-something-LOC</ta>
            <ta e="T308" id="Seg_9453" s="T307">you.SG.NOM</ta>
            <ta e="T309" id="Seg_9454" s="T308">say-IMP.2SG.S</ta>
            <ta e="T310" id="Seg_9455" s="T309">card-VBLZ-OPT-1DU</ta>
            <ta e="T311" id="Seg_9456" s="T310">(s)he-ALL</ta>
            <ta e="T312" id="Seg_9457" s="T311">say-IMP.2SG.S</ta>
            <ta e="T313" id="Seg_9458" s="T312">who.[NOM]</ta>
            <ta e="T314" id="Seg_9459" s="T313">fool-EP-ADVZ</ta>
            <ta e="T315" id="Seg_9460" s="T314">stay-FUT-3SG.S</ta>
            <ta e="T316" id="Seg_9461" s="T315">hand-PL-ACC-3SG</ta>
            <ta e="T317" id="Seg_9462" s="T316">behind</ta>
            <ta e="T318" id="Seg_9463" s="T317">bind-INF</ta>
            <ta e="T319" id="Seg_9464" s="T318">and</ta>
            <ta e="T320" id="Seg_9465" s="T319">you.SG.NOM</ta>
            <ta e="T321" id="Seg_9466" s="T320">(s)he-EP-ACC</ta>
            <ta e="T322" id="Seg_9467" s="T321">fool-EP-ADVZ</ta>
            <ta e="T323" id="Seg_9468" s="T322">leave-IMP.2SG.O</ta>
            <ta e="T324" id="Seg_9469" s="T323">hand-PL-ACC-3SG</ta>
            <ta e="T325" id="Seg_9470" s="T324">behind</ta>
            <ta e="T326" id="Seg_9471" s="T325">bind-IMP.2SG.O</ta>
            <ta e="T327" id="Seg_9472" s="T326">(s)he.[NOM]</ta>
            <ta e="T328" id="Seg_9473" s="T327">this</ta>
            <ta e="T329" id="Seg_9474" s="T328">rope-ACC</ta>
            <ta e="T330" id="Seg_9475" s="T329">NEG</ta>
            <ta e="T331" id="Seg_9476" s="T330">tear-FUT-3SG.S</ta>
            <ta e="T332" id="Seg_9477" s="T331">(s)he.[NOM]</ta>
            <ta e="T333" id="Seg_9478" s="T332">if</ta>
            <ta e="T334" id="Seg_9479" s="T333">NEG</ta>
            <ta e="T335" id="Seg_9480" s="T334">tear-3SG.O</ta>
            <ta e="T336" id="Seg_9481" s="T335">and</ta>
            <ta e="T337" id="Seg_9482" s="T336">you.SG.NOM</ta>
            <ta e="T338" id="Seg_9483" s="T337">outward(s)</ta>
            <ta e="T339" id="Seg_9484" s="T338">go.out-IMP.2SG.S</ta>
            <ta e="T340" id="Seg_9485" s="T339">I.ACC</ta>
            <ta e="T341" id="Seg_9486" s="T340">begin.to.cry-INCH-IMP.2SG.S</ta>
            <ta e="T342" id="Seg_9487" s="T341">I.NOM</ta>
            <ta e="T343" id="Seg_9488" s="T342">come-FUT-1SG.S</ta>
            <ta e="T344" id="Seg_9489" s="T343">(s)he-EP-ACC</ta>
            <ta e="T345" id="Seg_9490" s="T344">and</ta>
            <ta e="T346" id="Seg_9491" s="T345">kill-FUT-1SG.O</ta>
            <ta e="T347" id="Seg_9492" s="T346">you.SG-COM</ta>
            <ta e="T348" id="Seg_9493" s="T347">live-FUT-1DU</ta>
            <ta e="T349" id="Seg_9494" s="T348">husband.[NOM]-3SG</ta>
            <ta e="T350" id="Seg_9495" s="T349">come-CO.[3SG.S]</ta>
            <ta e="T351" id="Seg_9496" s="T350">(s)he.[NOM]</ta>
            <ta e="T352" id="Seg_9497" s="T351">(s)he-EP-ACC</ta>
            <ta e="T353" id="Seg_9498" s="T352">food-VBLZ-3SG.O</ta>
            <ta e="T354" id="Seg_9499" s="T353">say-3SG.S</ta>
            <ta e="T355" id="Seg_9500" s="T354">card-VBLZ-OPT-1DU</ta>
            <ta e="T356" id="Seg_9501" s="T355">husband.[NOM]-3SG</ta>
            <ta e="T357" id="Seg_9502" s="T356">say-3SG.S</ta>
            <ta e="T358" id="Seg_9503" s="T357">I.NOM</ta>
            <ta e="T359" id="Seg_9504" s="T358">get.tired-RFL-PST.NAR-1SG.S</ta>
            <ta e="T360" id="Seg_9505" s="T359">sleep-INF</ta>
            <ta e="T361" id="Seg_9506" s="T360">one.should</ta>
            <ta e="T362" id="Seg_9507" s="T361">now</ta>
            <ta e="T363" id="Seg_9508" s="T362">HORT</ta>
            <ta e="T364" id="Seg_9509" s="T363">card-VBLZ-OPT-1DU</ta>
            <ta e="T365" id="Seg_9510" s="T364">NEG</ta>
            <ta e="T366" id="Seg_9511" s="T365">long-ADV.LOC</ta>
            <ta e="T367" id="Seg_9512" s="T366">otherwise</ta>
            <ta e="T368" id="Seg_9513" s="T367">oneself.1SG</ta>
            <ta e="T369" id="Seg_9514" s="T368">sit-1SG.S</ta>
            <ta e="T370" id="Seg_9515" s="T369">and</ta>
            <ta e="T371" id="Seg_9516" s="T370">boring-ADVZ</ta>
            <ta e="T372" id="Seg_9517" s="T371">I.ALL</ta>
            <ta e="T373" id="Seg_9518" s="T372">be-3SG.S</ta>
            <ta e="T374" id="Seg_9519" s="T373">now</ta>
            <ta e="T375" id="Seg_9520" s="T374">and</ta>
            <ta e="T376" id="Seg_9521" s="T375">sit.down-3DU.S</ta>
            <ta e="T377" id="Seg_9522" s="T376">card-VBLZ-CVB</ta>
            <ta e="T378" id="Seg_9523" s="T377">who.[NOM]</ta>
            <ta e="T379" id="Seg_9524" s="T378">fool-EP-ADVZ</ta>
            <ta e="T380" id="Seg_9525" s="T379">stay-FUT-3SG.S</ta>
            <ta e="T381" id="Seg_9526" s="T380">hand-PL-ACC-3SG</ta>
            <ta e="T382" id="Seg_9527" s="T381">bind-TR-INF</ta>
            <ta e="T383" id="Seg_9528" s="T382">behind</ta>
            <ta e="T384" id="Seg_9529" s="T383">husband.[NOM]-3SG</ta>
            <ta e="T385" id="Seg_9530" s="T384">fool-EP-ADVZ</ta>
            <ta e="T386" id="Seg_9531" s="T385">stay-3SG.S</ta>
            <ta e="T387" id="Seg_9532" s="T386">hand-PL-ACC-3SG</ta>
            <ta e="T388" id="Seg_9533" s="T387">one.should</ta>
            <ta e="T389" id="Seg_9534" s="T388">bind-TR-INF</ta>
            <ta e="T390" id="Seg_9535" s="T389">hand-ACC-3SG</ta>
            <ta e="T391" id="Seg_9536" s="T390">one.should</ta>
            <ta e="T392" id="Seg_9537" s="T391">bind-INF</ta>
            <ta e="T393" id="Seg_9538" s="T392">behind</ta>
            <ta e="T394" id="Seg_9539" s="T393">and</ta>
            <ta e="T395" id="Seg_9540" s="T394">if</ta>
            <ta e="T396" id="Seg_9541" s="T395">I.NOM</ta>
            <ta e="T397" id="Seg_9542" s="T396">stay-CONJ-1SG.S</ta>
            <ta e="T398" id="Seg_9543" s="T397">fool-EP-ADVZ</ta>
            <ta e="T399" id="Seg_9544" s="T398">you.SG.NOM</ta>
            <ta e="T400" id="Seg_9545" s="T399">IRREAL</ta>
            <ta e="T401" id="Seg_9546" s="T400">I.ALL</ta>
            <ta e="T402" id="Seg_9547" s="T401">hand-PL.[NOM]-1SG</ta>
            <ta e="T403" id="Seg_9548" s="T402">bind-TR-CONJ-2SG.O</ta>
            <ta e="T404" id="Seg_9549" s="T403">and</ta>
            <ta e="T405" id="Seg_9550" s="T404">now</ta>
            <ta e="T406" id="Seg_9551" s="T405">I.NOM</ta>
            <ta e="T407" id="Seg_9552" s="T406">you.ALL</ta>
            <ta e="T408" id="Seg_9553" s="T407">hand-ACC-OBL.2SG</ta>
            <ta e="T409" id="Seg_9554" s="T408">bind-FUT-1SG.O</ta>
            <ta e="T410" id="Seg_9555" s="T409">husband.[NOM]-3SG</ta>
            <ta e="T411" id="Seg_9556" s="T410">say-3SG.S</ta>
            <ta e="T412" id="Seg_9557" s="T411">bind-IMP.2SG.O</ta>
            <ta e="T413" id="Seg_9558" s="T412">(s)he.[NOM]</ta>
            <ta e="T414" id="Seg_9559" s="T413">bind-3SG.O</ta>
            <ta e="T415" id="Seg_9560" s="T414">husband.[NOM]-3SG</ta>
            <ta e="T416" id="Seg_9561" s="T415">suddenly</ta>
            <ta e="T417" id="Seg_9562" s="T416">%%-TR-3SG.O</ta>
            <ta e="T418" id="Seg_9563" s="T417">rope-PL.[NOM]</ta>
            <ta e="T419" id="Seg_9564" s="T418">all</ta>
            <ta e="T420" id="Seg_9565" s="T419">tear-RES-3PL</ta>
            <ta e="T421" id="Seg_9566" s="T420">now</ta>
            <ta e="T422" id="Seg_9567" s="T421">and</ta>
            <ta e="T423" id="Seg_9568" s="T422">sleep-INF</ta>
            <ta e="T424" id="Seg_9569" s="T423">go.to.sleep-CO-3DU.S</ta>
            <ta e="T425" id="Seg_9570" s="T424">morning-something-LOC</ta>
            <ta e="T426" id="Seg_9571" s="T425">get.up-3SG.S</ta>
            <ta e="T427" id="Seg_9572" s="T426">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T428" id="Seg_9573" s="T427">all</ta>
            <ta e="T429" id="Seg_9574" s="T428">cook-HAB-CO-3SG.O</ta>
            <ta e="T430" id="Seg_9575" s="T429">meat-PL-ACC</ta>
            <ta e="T431" id="Seg_9576" s="T430">what-PL-ACC</ta>
            <ta e="T432" id="Seg_9577" s="T431">husband-ACC-3SG</ta>
            <ta e="T433" id="Seg_9578" s="T432">food-VBLZ-3SG.O</ta>
            <ta e="T434" id="Seg_9579" s="T433">husband.[NOM]-3SG</ta>
            <ta e="T435" id="Seg_9580" s="T434">taiga-EP-ILL</ta>
            <ta e="T436" id="Seg_9581" s="T435">leave-CO-3SG.S</ta>
            <ta e="T437" id="Seg_9582" s="T436">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T438" id="Seg_9583" s="T437">outward(s)</ta>
            <ta e="T439" id="Seg_9584" s="T438">jump.out-3SG.S</ta>
            <ta e="T440" id="Seg_9585" s="T439">and</ta>
            <ta e="T441" id="Seg_9586" s="T440">here</ta>
            <ta e="T442" id="Seg_9587" s="T441">shout-MOM-INFER.[3SG.S]</ta>
            <ta e="T443" id="Seg_9588" s="T442">come-IMP.2SG.S</ta>
            <ta e="T444" id="Seg_9589" s="T443">husband.[NOM]-1SG</ta>
            <ta e="T445" id="Seg_9590" s="T444">leave-PST.NAR.[3SG.S]</ta>
            <ta e="T446" id="Seg_9591" s="T445">robber.chief.[NOM]</ta>
            <ta e="T447" id="Seg_9592" s="T446">here</ta>
            <ta e="T448" id="Seg_9593" s="T447">come-INFER.[3SG.S]</ta>
            <ta e="T449" id="Seg_9594" s="T448">now</ta>
            <ta e="T450" id="Seg_9595" s="T449">what.[NOM]</ta>
            <ta e="T451" id="Seg_9596" s="T450">card-VBLZ-PST-2PL</ta>
            <ta e="T452" id="Seg_9597" s="T451">card-VBLZ-PST-1DU</ta>
            <ta e="T453" id="Seg_9598" s="T452">I.NOM</ta>
            <ta e="T454" id="Seg_9599" s="T453">you.ALL</ta>
            <ta e="T455" id="Seg_9600" s="T454">say-PST-1SG.S</ta>
            <ta e="T456" id="Seg_9601" s="T455">(s)he.[NOM]</ta>
            <ta e="T457" id="Seg_9602" s="T456">force-COM</ta>
            <ta e="T458" id="Seg_9603" s="T457">be-3SG.S</ta>
            <ta e="T459" id="Seg_9604" s="T458">this</ta>
            <ta e="T460" id="Seg_9605" s="T459">rope-PL-ACC</ta>
            <ta e="T461" id="Seg_9606" s="T460">all</ta>
            <ta e="T462" id="Seg_9607" s="T461">tear-PFV-3SG.O</ta>
            <ta e="T463" id="Seg_9608" s="T462">and</ta>
            <ta e="T464" id="Seg_9609" s="T463">this-day.[NOM]</ta>
            <ta e="T465" id="Seg_9610" s="T464">I.NOM</ta>
            <ta e="T466" id="Seg_9611" s="T465">you.ALL</ta>
            <ta e="T467" id="Seg_9612" s="T466">give-FUT-1SG.O</ta>
            <ta e="T468" id="Seg_9613" s="T467">hair.[NOM]</ta>
            <ta e="T469" id="Seg_9614" s="T468">rope.[NOM]</ta>
            <ta e="T470" id="Seg_9615" s="T469">give-INFER-FUT-1SG.S</ta>
            <ta e="T471" id="Seg_9616" s="T470">evening-something-LOC</ta>
            <ta e="T472" id="Seg_9617" s="T471">here</ta>
            <ta e="T473" id="Seg_9618" s="T472">come-POT.FUT.3SG</ta>
            <ta e="T474" id="Seg_9619" s="T473">again</ta>
            <ta e="T475" id="Seg_9620" s="T474">say-IMP.2SG.S</ta>
            <ta e="T476" id="Seg_9621" s="T475">card-VBLZ-OPT-1DU</ta>
            <ta e="T477" id="Seg_9622" s="T476">who.[NOM]</ta>
            <ta e="T478" id="Seg_9623" s="T477">fool-EP-ADVZ</ta>
            <ta e="T479" id="Seg_9624" s="T478">stay-FUT-3SG.S</ta>
            <ta e="T480" id="Seg_9625" s="T479">hand-PL-ACC-3SG</ta>
            <ta e="T481" id="Seg_9626" s="T480">behind</ta>
            <ta e="T482" id="Seg_9627" s="T481">bind-INF</ta>
            <ta e="T483" id="Seg_9628" s="T482">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T484" id="Seg_9629" s="T483">(s)he-EP-ACC</ta>
            <ta e="T485" id="Seg_9630" s="T484">food-VBLZ-PST.NAR-3SG.O</ta>
            <ta e="T486" id="Seg_9631" s="T485">wine-INSTR</ta>
            <ta e="T487" id="Seg_9632" s="T486">drink-TR-PST.NAR-3SG.O</ta>
            <ta e="T488" id="Seg_9633" s="T487">wine-PL.[NOM]</ta>
            <ta e="T489" id="Seg_9634" s="T488">drink-INFER-3DU.O</ta>
            <ta e="T490" id="Seg_9635" s="T489">man-human.being.[NOM]</ta>
            <ta e="T491" id="Seg_9636" s="T490">leave-CO-3SG.S</ta>
            <ta e="T492" id="Seg_9637" s="T491">husband.[NOM]-3SG</ta>
            <ta e="T493" id="Seg_9638" s="T492">come-CO.[3SG.S]</ta>
            <ta e="T494" id="Seg_9639" s="T493">wild.animal-PL-ACC</ta>
            <ta e="T495" id="Seg_9640" s="T494">much-ADVZ</ta>
            <ta e="T496" id="Seg_9641" s="T495">manage.to.get-PST.NAR-3SG.O</ta>
            <ta e="T497" id="Seg_9642" s="T496">and</ta>
            <ta e="T498" id="Seg_9643" s="T497">elk.[NOM]</ta>
            <ta e="T499" id="Seg_9644" s="T498">manage.to.get-PST.NAR-INFER-3SG.O</ta>
            <ta e="T500" id="Seg_9645" s="T499">(s)he.[NOM]</ta>
            <ta e="T501" id="Seg_9646" s="T500">(s)he-EP-ACC</ta>
            <ta e="T502" id="Seg_9647" s="T501">food-VBLZ-3SG.O</ta>
            <ta e="T503" id="Seg_9648" s="T502">say-3SG.S</ta>
            <ta e="T504" id="Seg_9649" s="T503">card-VBLZ-OPT-1DU</ta>
            <ta e="T505" id="Seg_9650" s="T504">husband.[NOM]-3SG</ta>
            <ta e="T506" id="Seg_9651" s="T505">say-3SG.S</ta>
            <ta e="T507" id="Seg_9652" s="T506">I.NOM</ta>
            <ta e="T508" id="Seg_9653" s="T507">get.tired-RFL-PST.NAR-1SG.S</ta>
            <ta e="T509" id="Seg_9654" s="T508">wife.[NOM]-3SG</ta>
            <ta e="T510" id="Seg_9655" s="T509">say-3SG.S</ta>
            <ta e="T511" id="Seg_9656" s="T510">card-VBLZ-OPT-1DU</ta>
            <ta e="T514" id="Seg_9657" s="T513">who.[NOM]</ta>
            <ta e="T515" id="Seg_9658" s="T514">who.[NOM]</ta>
            <ta e="T516" id="Seg_9659" s="T515">fool-EP-ADVZ</ta>
            <ta e="T517" id="Seg_9660" s="T516">stay-FUT-3SG.S</ta>
            <ta e="T518" id="Seg_9661" s="T517">hand-EP-ACC-3SG</ta>
            <ta e="T519" id="Seg_9662" s="T518">behind</ta>
            <ta e="T520" id="Seg_9663" s="T519">bind-INF</ta>
            <ta e="T521" id="Seg_9664" s="T520">if</ta>
            <ta e="T522" id="Seg_9665" s="T521">I.NOM</ta>
            <ta e="T523" id="Seg_9666" s="T522">stay-1SG.S</ta>
            <ta e="T524" id="Seg_9667" s="T523">fool-EP-ADVZ</ta>
            <ta e="T525" id="Seg_9668" s="T524">I.NOM</ta>
            <ta e="T526" id="Seg_9669" s="T525">hand.[NOM]-EP-1SG</ta>
            <ta e="T527" id="Seg_9670" s="T526">bind-IMP.2SG.S</ta>
            <ta e="T528" id="Seg_9671" s="T527">bind-FUT-2SG.O</ta>
            <ta e="T529" id="Seg_9672" s="T528">(s)he-EP-DU.[NOM]</ta>
            <ta e="T530" id="Seg_9673" s="T529">sit.down-3DU.S</ta>
            <ta e="T531" id="Seg_9674" s="T530">card-VBLZ-CVB</ta>
            <ta e="T532" id="Seg_9675" s="T531">husband.[NOM]-3SG</ta>
            <ta e="T533" id="Seg_9676" s="T532">fool-EP-ADVZ</ta>
            <ta e="T534" id="Seg_9677" s="T533">stay-3SG.S</ta>
            <ta e="T535" id="Seg_9678" s="T534">now</ta>
            <ta e="T536" id="Seg_9679" s="T535">now</ta>
            <ta e="T537" id="Seg_9680" s="T536">hand-ACC-OBL.2SG</ta>
            <ta e="T538" id="Seg_9681" s="T537">bind-FUT-1SG.O</ta>
            <ta e="T539" id="Seg_9682" s="T538">(s)he.[NOM]</ta>
            <ta e="T540" id="Seg_9683" s="T539">bind-3SG.O</ta>
            <ta e="T541" id="Seg_9684" s="T540">hand-EP-ACC-3SG</ta>
            <ta e="T542" id="Seg_9685" s="T541">(s)he.[NOM]</ta>
            <ta e="T543" id="Seg_9686" s="T542">suddenly</ta>
            <ta e="T544" id="Seg_9687" s="T543">%%-PFV-INFER.[3SG.S]</ta>
            <ta e="T545" id="Seg_9688" s="T544">rope.[NOM]</ta>
            <ta e="T546" id="Seg_9689" s="T545">tear-RFL-3SG.S</ta>
            <ta e="T547" id="Seg_9690" s="T546">go.to.sleep-CO-3DU.S</ta>
            <ta e="T548" id="Seg_9691" s="T547">sleep-CO</ta>
            <ta e="T549" id="Seg_9692" s="T548">morning-something-LOC</ta>
            <ta e="T550" id="Seg_9693" s="T549">get.up-3DU.S</ta>
            <ta e="T551" id="Seg_9694" s="T550">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T552" id="Seg_9695" s="T551">husband-ACC-3SG</ta>
            <ta e="T553" id="Seg_9696" s="T552">food-VBLZ-3SG.O</ta>
            <ta e="T554" id="Seg_9697" s="T553">husband.[NOM]-3SG</ta>
            <ta e="T555" id="Seg_9698" s="T554">eat-FRQ-CO-3SG.S</ta>
            <ta e="T556" id="Seg_9699" s="T555">and</ta>
            <ta e="T557" id="Seg_9700" s="T556">taiga-EP-ILL</ta>
            <ta e="T558" id="Seg_9701" s="T557">here</ta>
            <ta e="T559" id="Seg_9702" s="T558">leave-INFER.[3SG.S]</ta>
            <ta e="T560" id="Seg_9703" s="T559">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T561" id="Seg_9704" s="T560">outward(s)</ta>
            <ta e="T562" id="Seg_9705" s="T561">go-3SG.S</ta>
            <ta e="T563" id="Seg_9706" s="T562">and</ta>
            <ta e="T564" id="Seg_9707" s="T563">here</ta>
            <ta e="T565" id="Seg_9708" s="T564">shout-MOM-EP-GEN</ta>
            <ta e="T566" id="Seg_9709" s="T565">come-IMP.2SG.S</ta>
            <ta e="T567" id="Seg_9710" s="T566">husband.[NOM]-1SG</ta>
            <ta e="T568" id="Seg_9711" s="T567">leave-PST.NAR.[3SG.S]</ta>
            <ta e="T569" id="Seg_9712" s="T568">robber.[NOM]</ta>
            <ta e="T570" id="Seg_9713" s="T569">come-CO.[3SG.S]</ta>
            <ta e="T571" id="Seg_9714" s="T570">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T572" id="Seg_9715" s="T571">say-3SG.S</ta>
            <ta e="T573" id="Seg_9716" s="T572">rope-PL-ACC</ta>
            <ta e="T574" id="Seg_9717" s="T573">husband.[NOM]-1SG</ta>
            <ta e="T575" id="Seg_9718" s="T574">all</ta>
            <ta e="T576" id="Seg_9719" s="T575">tear-PFV-3SG.O</ta>
            <ta e="T577" id="Seg_9720" s="T576">(s)he.[NOM]</ta>
            <ta e="T578" id="Seg_9721" s="T577">force-COM</ta>
            <ta e="T579" id="Seg_9722" s="T578">be-3SG.S</ta>
            <ta e="T580" id="Seg_9723" s="T579">and</ta>
            <ta e="T581" id="Seg_9724" s="T580">I.NOM</ta>
            <ta e="T582" id="Seg_9725" s="T581">this-day.[NOM]</ta>
            <ta e="T583" id="Seg_9726" s="T582">you.ALL</ta>
            <ta e="T584" id="Seg_9727" s="T583">give-FUT-1SG.O</ta>
            <ta e="T585" id="Seg_9728" s="T584">rope.[NOM]</ta>
            <ta e="T586" id="Seg_9729" s="T585">hair.[NOM]</ta>
            <ta e="T587" id="Seg_9730" s="T586">and</ta>
            <ta e="T590" id="Seg_9731" s="T589">one-something-ADV.LOC</ta>
            <ta e="T591" id="Seg_9732" s="T590">twist-RES.[3SG.S]</ta>
            <ta e="T592" id="Seg_9733" s="T591">evening-something-LOC</ta>
            <ta e="T593" id="Seg_9734" s="T592">here</ta>
            <ta e="T594" id="Seg_9735" s="T593">come-POT.FUT.3SG</ta>
            <ta e="T595" id="Seg_9736" s="T594">you.SG.NOM</ta>
            <ta e="T596" id="Seg_9737" s="T595">again</ta>
            <ta e="T597" id="Seg_9738" s="T596">say-IMP.2SG.S</ta>
            <ta e="T598" id="Seg_9739" s="T597">card-VBLZ-INF</ta>
            <ta e="T599" id="Seg_9740" s="T598">card-VBLZ-OPT-1DU</ta>
            <ta e="T600" id="Seg_9741" s="T599">who.[NOM]</ta>
            <ta e="T601" id="Seg_9742" s="T600">fool-EP-ADVZ</ta>
            <ta e="T602" id="Seg_9743" s="T601">stay-FUT-3SG.S</ta>
            <ta e="T603" id="Seg_9744" s="T602">then</ta>
            <ta e="T604" id="Seg_9745" s="T603">hand-EP-ACC-3SG</ta>
            <ta e="T605" id="Seg_9746" s="T604">behind</ta>
            <ta e="T606" id="Seg_9747" s="T605">bind-INF</ta>
            <ta e="T607" id="Seg_9748" s="T606">old.man.[NOM]</ta>
            <ta e="T608" id="Seg_9749" s="T607">come-CO.[3SG.S]</ta>
            <ta e="T609" id="Seg_9750" s="T608">(s)he.[NOM]</ta>
            <ta e="T610" id="Seg_9751" s="T609">(s)he-EP-ACC</ta>
            <ta e="T611" id="Seg_9752" s="T610">food-VBLZ-3SG.O</ta>
            <ta e="T612" id="Seg_9753" s="T611">say-3SG.S</ta>
            <ta e="T613" id="Seg_9754" s="T612">card-VBLZ-OPT-1DU</ta>
            <ta e="T614" id="Seg_9755" s="T613">husband.[NOM]-3SG</ta>
            <ta e="T615" id="Seg_9756" s="T614">say-3SG.S</ta>
            <ta e="T616" id="Seg_9757" s="T615">I.NOM</ta>
            <ta e="T617" id="Seg_9758" s="T616">get.tired-RFL-PST.NAR-1SG.S</ta>
            <ta e="T618" id="Seg_9759" s="T617">one.should</ta>
            <ta e="T619" id="Seg_9760" s="T618">sleep-INF</ta>
            <ta e="T620" id="Seg_9761" s="T619">now</ta>
            <ta e="T621" id="Seg_9762" s="T620">card-VBLZ-OPT-1DU</ta>
            <ta e="T622" id="Seg_9763" s="T621">card-VBLZ-OPT-1DU</ta>
            <ta e="T623" id="Seg_9764" s="T622">for.example</ta>
            <ta e="T624" id="Seg_9765" s="T623">one-EP-ADV.LOC</ta>
            <ta e="T625" id="Seg_9766" s="T624">who.[NOM]</ta>
            <ta e="T626" id="Seg_9767" s="T625">fool-EP-ADVZ</ta>
            <ta e="T627" id="Seg_9768" s="T626">stay-FUT-3SG.S</ta>
            <ta e="T628" id="Seg_9769" s="T627">hand-EP-ACC-3SG</ta>
            <ta e="T629" id="Seg_9770" s="T628">behind</ta>
            <ta e="T630" id="Seg_9771" s="T629">bind-INF</ta>
            <ta e="T631" id="Seg_9772" s="T630">if</ta>
            <ta e="T632" id="Seg_9773" s="T631">I.NOM</ta>
            <ta e="T633" id="Seg_9774" s="T632">stay-FUT-1SG.S</ta>
            <ta e="T634" id="Seg_9775" s="T633">I.GEN</ta>
            <ta e="T635" id="Seg_9776" s="T634">hand.[NOM]-EP-1SG</ta>
            <ta e="T636" id="Seg_9777" s="T635">bind-IMP.2SG.O</ta>
            <ta e="T637" id="Seg_9778" s="T636">behind</ta>
            <ta e="T638" id="Seg_9779" s="T637">and</ta>
            <ta e="T639" id="Seg_9780" s="T638">sit.down-3DU.S</ta>
            <ta e="T640" id="Seg_9781" s="T639">card-VBLZ-CVB</ta>
            <ta e="T641" id="Seg_9782" s="T640">husband-ACC-3SG</ta>
            <ta e="T642" id="Seg_9783" s="T641">fool-EP-TRL-CVB</ta>
            <ta e="T643" id="Seg_9784" s="T642">leave-3SG.O</ta>
            <ta e="T644" id="Seg_9785" s="T643">hand-EP-ACC-3SG</ta>
            <ta e="T645" id="Seg_9786" s="T644">behind</ta>
            <ta e="T646" id="Seg_9787" s="T645">bind-3SG.O</ta>
            <ta e="T647" id="Seg_9788" s="T646">(s)he.[NOM]</ta>
            <ta e="T648" id="Seg_9789" s="T647">suddenly</ta>
            <ta e="T649" id="Seg_9790" s="T648">%%-PFV-INFER.[3SG.S]</ta>
            <ta e="T650" id="Seg_9791" s="T649">rope-PL-ACC</ta>
            <ta e="T651" id="Seg_9792" s="T650">NEG</ta>
            <ta e="T652" id="Seg_9793" s="T651">tear-3SG.O</ta>
            <ta e="T653" id="Seg_9794" s="T652">wife-3SG-ALL</ta>
            <ta e="T654" id="Seg_9795" s="T653">say-3SG.S</ta>
            <ta e="T655" id="Seg_9796" s="T654">untie-IMP.2SG.O</ta>
            <ta e="T656" id="Seg_9797" s="T655">hand.[NOM]-EP-1SG</ta>
            <ta e="T657" id="Seg_9798" s="T656">old.woman.[NOM]-3SG</ta>
            <ta e="T658" id="Seg_9799" s="T657">say-3SG.S</ta>
            <ta e="T659" id="Seg_9800" s="T658">tear-IMP.2SG.O</ta>
            <ta e="T660" id="Seg_9801" s="T659">(s)he.[NOM]</ta>
            <ta e="T661" id="Seg_9802" s="T660">again</ta>
            <ta e="T662" id="Seg_9803" s="T661">%%-PFV-3SG.S</ta>
            <ta e="T663" id="Seg_9804" s="T662">NEG</ta>
            <ta e="T664" id="Seg_9805" s="T663">tear-3SG.O</ta>
            <ta e="T665" id="Seg_9806" s="T664">say-3SG.S</ta>
            <ta e="T666" id="Seg_9807" s="T665">untie-IMP.2SG.O</ta>
            <ta e="T667" id="Seg_9808" s="T666">NEG</ta>
            <ta e="T668" id="Seg_9809" s="T667">untie-FUT-1SG.O</ta>
            <ta e="T669" id="Seg_9810" s="T668">outward(s)</ta>
            <ta e="T670" id="Seg_9811" s="T669">jump.out-CVB</ta>
            <ta e="T671" id="Seg_9812" s="T670">leave-CO-3SG.S</ta>
            <ta e="T672" id="Seg_9813" s="T671">and</ta>
            <ta e="T673" id="Seg_9814" s="T672">here</ta>
            <ta e="T674" id="Seg_9815" s="T673">begin.to.cry-INFER.[3SG.S]</ta>
            <ta e="T675" id="Seg_9816" s="T674">come-IMP.2SG.S</ta>
            <ta e="T676" id="Seg_9817" s="T675">and</ta>
            <ta e="T677" id="Seg_9818" s="T676">this</ta>
            <ta e="T678" id="Seg_9819" s="T677">robber.[NOM]</ta>
            <ta e="T679" id="Seg_9820" s="T678">here</ta>
            <ta e="T680" id="Seg_9821" s="T679">come-INFER.[3SG.S]</ta>
            <ta e="T681" id="Seg_9822" s="T680">say-3SG.S</ta>
            <ta e="T682" id="Seg_9823" s="T681">husband.[NOM]-1SG</ta>
            <ta e="T683" id="Seg_9824" s="T682">rope-ACC</ta>
            <ta e="T684" id="Seg_9825" s="T683">NEG</ta>
            <ta e="T685" id="Seg_9826" s="T684">tear-3SG.O</ta>
            <ta e="T686" id="Seg_9827" s="T685">come.in-CVB</ta>
            <ta e="T687" id="Seg_9828" s="T686">kill-OPT-1DU</ta>
            <ta e="T688" id="Seg_9829" s="T687">and</ta>
            <ta e="T689" id="Seg_9830" s="T688">house-ILL</ta>
            <ta e="T690" id="Seg_9831" s="T689">come.in-CO-3DU.S</ta>
            <ta e="T691" id="Seg_9832" s="T690">robber.[NOM]</ta>
            <ta e="T692" id="Seg_9833" s="T691">say-3SG.S</ta>
            <ta e="T693" id="Seg_9834" s="T692">morning-day-ADV.LOC</ta>
            <ta e="T694" id="Seg_9835" s="T693">morning-something-LOC</ta>
            <ta e="T695" id="Seg_9836" s="T694">kill-FUT-1DU</ta>
            <ta e="T696" id="Seg_9837" s="T695">bring.in-IMP.2SG.O</ta>
            <ta e="T697" id="Seg_9838" s="T696">bucket-GEN</ta>
            <ta e="T698" id="Seg_9839" s="T697">fullness.[NOM]</ta>
            <ta e="T699" id="Seg_9840" s="T698">salt.[NOM]</ta>
            <ta e="T700" id="Seg_9841" s="T699">(s)he-EP-ACC</ta>
            <ta e="T701" id="Seg_9842" s="T700">salt-EP-GEN</ta>
            <ta e="T702" id="Seg_9843" s="T701">top-EP-ILL</ta>
            <ta e="T703" id="Seg_9844" s="T702">knee-OPT-OBL.3SG-INSTR</ta>
            <ta e="T704" id="Seg_9845" s="T703">salt-EP-ILL</ta>
            <ta e="T705" id="Seg_9846" s="T704">sit.down-TR-FUT-1DU</ta>
            <ta e="T706" id="Seg_9847" s="T705">morning-we.DU-ILL</ta>
            <ta e="T707" id="Seg_9848" s="T706">sit.[3SG.S]</ta>
            <ta e="T708" id="Seg_9849" s="T707">morning-something-LOC</ta>
            <ta e="T709" id="Seg_9850" s="T708">kill-FUT-1DU</ta>
            <ta e="T710" id="Seg_9851" s="T709">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T711" id="Seg_9852" s="T710">face-LOC</ta>
            <ta e="T712" id="Seg_9853" s="T711">come-CVB</ta>
            <ta e="T713" id="Seg_9854" s="T712">here</ta>
            <ta e="T714" id="Seg_9855" s="T713">spit-INFER-3SG.O</ta>
            <ta e="T715" id="Seg_9856" s="T714">you.SG.NOM</ta>
            <ta e="T716" id="Seg_9857" s="T715">say-HAB-PST-2SG.S</ta>
            <ta e="T717" id="Seg_9858" s="T716">that</ta>
            <ta e="T718" id="Seg_9859" s="T717">I.NOM</ta>
            <ta e="T719" id="Seg_9860" s="T718">force-COM</ta>
            <ta e="T720" id="Seg_9861" s="T719">be-CO-1SG.S</ta>
            <ta e="T721" id="Seg_9862" s="T720">and</ta>
            <ta e="T722" id="Seg_9863" s="T721">what-TRL</ta>
            <ta e="T723" id="Seg_9864" s="T722">rope-ACC</ta>
            <ta e="T724" id="Seg_9865" s="T723">NEG</ta>
            <ta e="T725" id="Seg_9866" s="T724">tear-FUT-2SG.O</ta>
            <ta e="T726" id="Seg_9867" s="T725">now</ta>
            <ta e="T727" id="Seg_9868" s="T726">morning-something-LOC</ta>
            <ta e="T728" id="Seg_9869" s="T727">you.SG.ACC</ta>
            <ta e="T729" id="Seg_9870" s="T728">kill-FUT-1DU</ta>
            <ta e="T730" id="Seg_9871" s="T729">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T731" id="Seg_9872" s="T730">run-MOM-CO-3SG.S</ta>
            <ta e="T732" id="Seg_9873" s="T731">salt.[NOM]</ta>
            <ta e="T733" id="Seg_9874" s="T732">bring.in-INFER-3SG.O</ta>
            <ta e="T734" id="Seg_9875" s="T733">bucket-GEN</ta>
            <ta e="T735" id="Seg_9876" s="T734">fullness.[NOM]</ta>
            <ta e="T736" id="Seg_9877" s="T735">and</ta>
            <ta e="T737" id="Seg_9878" s="T736">floor-ILL</ta>
            <ta e="T738" id="Seg_9879" s="T737">pour-3SG.O</ta>
            <ta e="T739" id="Seg_9880" s="T738">and</ta>
            <ta e="T740" id="Seg_9881" s="T739">(s)he-EP-ACC</ta>
            <ta e="T741" id="Seg_9882" s="T740">knee-ADV.LOC</ta>
            <ta e="T742" id="Seg_9883" s="T741">sit.down-TR-3SG.O</ta>
            <ta e="T743" id="Seg_9884" s="T742">say-3SG.S</ta>
            <ta e="T744" id="Seg_9885" s="T743">sit-IMP.2SG.S</ta>
            <ta e="T745" id="Seg_9886" s="T744">morning-something-ILL</ta>
            <ta e="T746" id="Seg_9887" s="T745">morning-something-LOC</ta>
            <ta e="T747" id="Seg_9888" s="T746">you.SG.ACC</ta>
            <ta e="T748" id="Seg_9889" s="T747">kill-FUT-1DU</ta>
            <ta e="T749" id="Seg_9890" s="T748">sit.down-3DU.S</ta>
            <ta e="T750" id="Seg_9891" s="T749">eat-FRQ-CVB</ta>
            <ta e="T751" id="Seg_9892" s="T750">robber-COM</ta>
            <ta e="T752" id="Seg_9893" s="T751">wine-ADJZ</ta>
            <ta e="T753" id="Seg_9894" s="T752">drink-CVB</ta>
            <ta e="T754" id="Seg_9895" s="T753">begin-DRV-EP-3DU.S</ta>
            <ta e="T755" id="Seg_9896" s="T754">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T756" id="Seg_9897" s="T755">come-CO.[3SG.S]</ta>
            <ta e="T757" id="Seg_9898" s="T756">husband-GEN-3SG</ta>
            <ta e="T758" id="Seg_9899" s="T757">side-ILL</ta>
            <ta e="T759" id="Seg_9900" s="T758">husband-ACC-3SG</ta>
            <ta e="T760" id="Seg_9901" s="T759">face-EP-ILL</ta>
            <ta e="T761" id="Seg_9902" s="T760">here</ta>
            <ta e="T762" id="Seg_9903" s="T761">spit-INFER-3SG.O</ta>
            <ta e="T763" id="Seg_9904" s="T762">morning-something-LOC</ta>
            <ta e="T764" id="Seg_9905" s="T763">we.[NOM]</ta>
            <ta e="T765" id="Seg_9906" s="T764">you.SG.ACC</ta>
            <ta e="T766" id="Seg_9907" s="T765">kill-FUT-1DU</ta>
            <ta e="T767" id="Seg_9908" s="T766">oneself.3SG</ta>
            <ta e="T768" id="Seg_9909" s="T767">here</ta>
            <ta e="T769" id="Seg_9910" s="T768">leave-INFER.[3SG.S]</ta>
            <ta e="T770" id="Seg_9911" s="T769">robber-EP-ACC</ta>
            <ta e="T771" id="Seg_9912" s="T770">embrace-HAB-3SG.O</ta>
            <ta e="T772" id="Seg_9913" s="T771">kiss-INCH-CVB</ta>
            <ta e="T773" id="Seg_9914" s="T772">bring-EP-FRQ-EP-3SG.O</ta>
            <ta e="T774" id="Seg_9915" s="T773">and</ta>
            <ta e="T775" id="Seg_9916" s="T774">sleep-INF</ta>
            <ta e="T776" id="Seg_9917" s="T775">go.to.sleep-CO-3DU.S</ta>
            <ta e="T777" id="Seg_9918" s="T776">and</ta>
            <ta e="T778" id="Seg_9919" s="T777">here</ta>
            <ta e="T779" id="Seg_9920" s="T778">sleep-PFV-3DU.S</ta>
            <ta e="T780" id="Seg_9921" s="T779">husband.[NOM]-3SG</ta>
            <ta e="T781" id="Seg_9922" s="T780">(s)he.[NOM]</ta>
            <ta e="T782" id="Seg_9923" s="T781">get.up-3SG.S</ta>
            <ta e="T783" id="Seg_9924" s="T782">up</ta>
            <ta e="T784" id="Seg_9925" s="T783">walk-HAB-3SG.S</ta>
            <ta e="T785" id="Seg_9926" s="T784">and</ta>
            <ta e="T786" id="Seg_9927" s="T785">small-ADVZ</ta>
            <ta e="T787" id="Seg_9928" s="T786">daughter-DIM.[NOM]-3SG</ta>
            <ta e="T788" id="Seg_9929" s="T787">son-DIM.[NOM]-3SG</ta>
            <ta e="T789" id="Seg_9930" s="T788">(s)he-EP-DU.[NOM]</ta>
            <ta e="T790" id="Seg_9931" s="T789">sleep-PST-3DU.S</ta>
            <ta e="T791" id="Seg_9932" s="T790">high.bunk-GEN</ta>
            <ta e="T792" id="Seg_9933" s="T791">top-EP-LOC</ta>
            <ta e="T793" id="Seg_9934" s="T792">daughter-DIM.[NOM]-3SG</ta>
            <ta e="T794" id="Seg_9935" s="T793">piss-INF</ta>
            <ta e="T795" id="Seg_9936" s="T794">down</ta>
            <ta e="T796" id="Seg_9937" s="T795">climb.down-3SG.S</ta>
            <ta e="T797" id="Seg_9938" s="T796">(s)he.[NOM]</ta>
            <ta e="T798" id="Seg_9939" s="T797">daughter-DIM-OBL.3SG-ALL</ta>
            <ta e="T799" id="Seg_9940" s="T798">say-3SG.S</ta>
            <ta e="T800" id="Seg_9941" s="T799">I.GEN</ta>
            <ta e="T801" id="Seg_9942" s="T800">hand-PL.[NOM]-1SG</ta>
            <ta e="T802" id="Seg_9943" s="T801">untie-OPT-2SG.O</ta>
            <ta e="T803" id="Seg_9944" s="T802">and</ta>
            <ta e="T804" id="Seg_9945" s="T803">daughter-DIM.[NOM]-3SG</ta>
            <ta e="T805" id="Seg_9946" s="T804">say-3SG.S</ta>
            <ta e="T806" id="Seg_9947" s="T805">you.SG.NOM</ta>
            <ta e="T807" id="Seg_9948" s="T806">say-HAB-PST-2SG.S</ta>
            <ta e="T808" id="Seg_9949" s="T807">that</ta>
            <ta e="T809" id="Seg_9950" s="T808">force-COM</ta>
            <ta e="T810" id="Seg_9951" s="T809">be-CO-1SG.S</ta>
            <ta e="T811" id="Seg_9952" s="T810">tear-TR-IMP.2SG.O</ta>
            <ta e="T812" id="Seg_9953" s="T811">rope-ACC</ta>
            <ta e="T813" id="Seg_9954" s="T812">high.bunk-GEN</ta>
            <ta e="T814" id="Seg_9955" s="T813">top-EP-ILL</ta>
            <ta e="T815" id="Seg_9956" s="T814">go-3SG.S</ta>
            <ta e="T816" id="Seg_9957" s="T815">and</ta>
            <ta e="T817" id="Seg_9958" s="T816">sleep.[3SG.S]</ta>
            <ta e="T818" id="Seg_9959" s="T817">son-DIM.[NOM]-3SG</ta>
            <ta e="T819" id="Seg_9960" s="T818">high.bunk-EL.3SG</ta>
            <ta e="T820" id="Seg_9961" s="T819">down</ta>
            <ta e="T821" id="Seg_9962" s="T820">climb.down-3SG.S</ta>
            <ta e="T822" id="Seg_9963" s="T821">piss-INF</ta>
            <ta e="T823" id="Seg_9964" s="T822">(s)he.[NOM]</ta>
            <ta e="T824" id="Seg_9965" s="T823">son-DIM-3SG-ALL</ta>
            <ta e="T825" id="Seg_9966" s="T824">say-3SG.S</ta>
            <ta e="T826" id="Seg_9967" s="T825">I.GEN</ta>
            <ta e="T827" id="Seg_9968" s="T826">hand.[NOM]-EP-1SG</ta>
            <ta e="T828" id="Seg_9969" s="T827">untie-OPT-2SG.O</ta>
            <ta e="T829" id="Seg_9970" s="T828">(s)he.[NOM]</ta>
            <ta e="T830" id="Seg_9971" s="T829">come-CO.[3SG.S]</ta>
            <ta e="T831" id="Seg_9972" s="T830">father-3SG-ALL</ta>
            <ta e="T832" id="Seg_9973" s="T831">and</ta>
            <ta e="T833" id="Seg_9974" s="T832">untie-CVB</ta>
            <ta e="T834" id="Seg_9975" s="T833">begin-FRQ-EP-3SG.O</ta>
            <ta e="T835" id="Seg_9976" s="T834">NEG</ta>
            <ta e="T836" id="Seg_9977" s="T835">untie-3SG.O</ta>
            <ta e="T837" id="Seg_9978" s="T836">I.ALL</ta>
            <ta e="T838" id="Seg_9979" s="T837">NEG</ta>
            <ta e="T839" id="Seg_9980" s="T838">untie-INF</ta>
            <ta e="T840" id="Seg_9981" s="T839">father.[NOM]-3SG</ta>
            <ta e="T841" id="Seg_9982" s="T840">say-3SG.S</ta>
            <ta e="T842" id="Seg_9983" s="T841">rasp</ta>
            <ta e="T843" id="Seg_9984" s="T842">bring-IMP.2SG.S</ta>
            <ta e="T844" id="Seg_9985" s="T843">(s)he.[NOM]</ta>
            <ta e="T845" id="Seg_9986" s="T844">bring-CO-3SG.O</ta>
            <ta e="T846" id="Seg_9987" s="T845">and</ta>
            <ta e="T847" id="Seg_9988" s="T846">rope-PL-ACC</ta>
            <ta e="T848" id="Seg_9989" s="T847">sharpen-INCH-CVB</ta>
            <ta e="T849" id="Seg_9990" s="T848">here</ta>
            <ta e="T850" id="Seg_9991" s="T849">begin-FRQ-EP-INFER-3SG.O</ta>
            <ta e="T851" id="Seg_9992" s="T850">sharpen-3SG.O</ta>
            <ta e="T852" id="Seg_9993" s="T851">rope-PL.[NOM]</ta>
            <ta e="T853" id="Seg_9994" s="T852">%%-DETR-CO-3PL</ta>
            <ta e="T854" id="Seg_9995" s="T853">(s)he.[NOM]</ta>
            <ta e="T855" id="Seg_9996" s="T854">son-DIM-3SG-ALL</ta>
            <ta e="T856" id="Seg_9997" s="T855">say-3SG.S</ta>
            <ta e="T857" id="Seg_9998" s="T856">leave-CVB</ta>
            <ta e="T858" id="Seg_9999" s="T857">sleep-IMP.2SG.S</ta>
            <ta e="T859" id="Seg_10000" s="T858">morning-something-LOC</ta>
            <ta e="T860" id="Seg_10001" s="T859">NEG.IMP</ta>
            <ta e="T861" id="Seg_10002" s="T860">get.afraid-DUR-IMP.2SG.S</ta>
            <ta e="T862" id="Seg_10003" s="T861">we.[NOM]</ta>
            <ta e="T863" id="Seg_10004" s="T862">you.SG-COM</ta>
            <ta e="T864" id="Seg_10005" s="T863">oneself.1DU</ta>
            <ta e="T865" id="Seg_10006" s="T864">live-FUT-1DU</ta>
            <ta e="T866" id="Seg_10007" s="T865">sky.[NOM]</ta>
            <ta e="T867" id="Seg_10008" s="T866">day-EP-TRL-CVB</ta>
            <ta e="T868" id="Seg_10009" s="T867">begin-DRV-EP-3SG.S</ta>
            <ta e="T869" id="Seg_10010" s="T868">(s)he.[NOM]</ta>
            <ta e="T870" id="Seg_10011" s="T869">rope-PL-ACC</ta>
            <ta e="T871" id="Seg_10012" s="T870">hand-ILL.3SG</ta>
            <ta e="T872" id="Seg_10013" s="T871">put-CO-3SG.O</ta>
            <ta e="T875" id="Seg_10014" s="T874">bind-RES-INFER.[3SG.S]</ta>
            <ta e="T876" id="Seg_10015" s="T875">and</ta>
            <ta e="T877" id="Seg_10016" s="T876">sit.down-3SG.S</ta>
            <ta e="T878" id="Seg_10017" s="T877">knee-GEN</ta>
            <ta e="T879" id="Seg_10018" s="T878">top-EP-ADV.LOC</ta>
            <ta e="T880" id="Seg_10019" s="T879">wife.[NOM]-3SG</ta>
            <ta e="T881" id="Seg_10020" s="T880">get.up-3SG.S</ta>
            <ta e="T882" id="Seg_10021" s="T881">come-CVB</ta>
            <ta e="T883" id="Seg_10022" s="T882">face-EP-ADV.LOC</ta>
            <ta e="T884" id="Seg_10023" s="T883">spit-3SG.O</ta>
            <ta e="T885" id="Seg_10024" s="T884">robber-ALL</ta>
            <ta e="T886" id="Seg_10025" s="T885">say-3SG.S</ta>
            <ta e="T887" id="Seg_10026" s="T886">now</ta>
            <ta e="T888" id="Seg_10027" s="T887">kill-OPT-1DU</ta>
            <ta e="T889" id="Seg_10028" s="T888">and</ta>
            <ta e="T890" id="Seg_10029" s="T889">robber.[NOM]</ta>
            <ta e="T891" id="Seg_10030" s="T890">say-3SG.S</ta>
            <ta e="T892" id="Seg_10031" s="T891">eat-FRQ-CVB</ta>
            <ta e="T893" id="Seg_10032" s="T892">leave-OPT-1DU-OPT</ta>
            <ta e="T894" id="Seg_10033" s="T893">(s)he.[NOM]</ta>
            <ta e="T895" id="Seg_10034" s="T894">meat-PL-ACC</ta>
            <ta e="T896" id="Seg_10035" s="T895">cook-FRQ-DRV-CO-3SG.O</ta>
            <ta e="T897" id="Seg_10036" s="T896">eat-FRQ-CO-3DU.S</ta>
            <ta e="T898" id="Seg_10037" s="T897">now</ta>
            <ta e="T899" id="Seg_10038" s="T898">now</ta>
            <ta e="T900" id="Seg_10039" s="T899">kill-OPT-1DU</ta>
            <ta e="T901" id="Seg_10040" s="T900">house-LOC</ta>
            <ta e="T902" id="Seg_10041" s="T901">kill-FUT-1DU</ta>
            <ta e="T903" id="Seg_10042" s="T902">or</ta>
            <ta e="T904" id="Seg_10043" s="T903">outward(s)-ADV.LOC</ta>
            <ta e="T905" id="Seg_10044" s="T904">robber.[NOM]</ta>
            <ta e="T906" id="Seg_10045" s="T905">say-3SG.S</ta>
            <ta e="T907" id="Seg_10046" s="T906">outward(s)-ADV.LOC</ta>
            <ta e="T908" id="Seg_10047" s="T907">kill-FUT-1DU</ta>
            <ta e="T909" id="Seg_10048" s="T908">now</ta>
            <ta e="T910" id="Seg_10049" s="T909">get.up-IMP.2SG.S</ta>
            <ta e="T911" id="Seg_10050" s="T910">go-IMP.2SG.S</ta>
            <ta e="T912" id="Seg_10051" s="T911">outward(s)</ta>
            <ta e="T913" id="Seg_10052" s="T912">you.SG.ACC</ta>
            <ta e="T914" id="Seg_10053" s="T913">kill-FUT-1DU</ta>
            <ta e="T915" id="Seg_10054" s="T914">what.[NOM]</ta>
            <ta e="T916" id="Seg_10055" s="T915">rope-PL-ACC</ta>
            <ta e="T917" id="Seg_10056" s="T916">NEG</ta>
            <ta e="T918" id="Seg_10057" s="T917">tear-TR-2SG.O</ta>
            <ta e="T919" id="Seg_10058" s="T918">you.SG.NOM</ta>
            <ta e="T920" id="Seg_10059" s="T919">say-HAB-PST-2SG.S</ta>
            <ta e="T921" id="Seg_10060" s="T920">force-COM</ta>
            <ta e="T922" id="Seg_10061" s="T921">be-CO-1SG.S</ta>
            <ta e="T923" id="Seg_10062" s="T922">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T924" id="Seg_10063" s="T923">suddenly</ta>
            <ta e="T925" id="Seg_10064" s="T924">face-EP-ILL</ta>
            <ta e="T926" id="Seg_10065" s="T925">spit-INFER-3SG.O</ta>
            <ta e="T927" id="Seg_10066" s="T926">(s)he.[NOM]</ta>
            <ta e="T928" id="Seg_10067" s="T927">suddenly</ta>
            <ta e="T929" id="Seg_10068" s="T928">up</ta>
            <ta e="T930" id="Seg_10069" s="T929">stand.up-DRV-INFER.[3SG.S]</ta>
            <ta e="T931" id="Seg_10070" s="T930">suddenly</ta>
            <ta e="T932" id="Seg_10071" s="T931">robber-EP-ACC</ta>
            <ta e="T933" id="Seg_10072" s="T932">beat-INFER-3SG.O</ta>
            <ta e="T934" id="Seg_10073" s="T933">robber.[NOM]</ta>
            <ta e="T935" id="Seg_10074" s="T934">on.oneʼs.back</ta>
            <ta e="T936" id="Seg_10075" s="T935">fall-PFV-3SG.S</ta>
            <ta e="T937" id="Seg_10076" s="T936">fall-RFL-3SG.S</ta>
            <ta e="T938" id="Seg_10077" s="T937">and</ta>
            <ta e="T939" id="Seg_10078" s="T938">kill-CO-3SG.O</ta>
            <ta e="T940" id="Seg_10079" s="T939">and</ta>
            <ta e="T941" id="Seg_10080" s="T940">wife.[NOM]-3SG</ta>
            <ta e="T942" id="Seg_10081" s="T941">nape-EP-ILL.3SG</ta>
            <ta e="T943" id="Seg_10082" s="T942">hang-EP-RFL-3SG.S</ta>
            <ta e="T944" id="Seg_10083" s="T943">say-3SG.S</ta>
            <ta e="T945" id="Seg_10084" s="T944">I.NOM</ta>
            <ta e="T946" id="Seg_10085" s="T945">such-ADVZ</ta>
            <ta e="T947" id="Seg_10086" s="T946">NEG</ta>
            <ta e="T948" id="Seg_10087" s="T947">do-HAB-FUT-1SG.O</ta>
            <ta e="T949" id="Seg_10088" s="T948">leg-PL-ACC-OBL.2SG</ta>
            <ta e="T950" id="Seg_10089" s="T949">wash-HAB-FUT-1SG.O</ta>
            <ta e="T951" id="Seg_10090" s="T950">this</ta>
            <ta e="T952" id="Seg_10091" s="T951">water-EP-ACC</ta>
            <ta e="T953" id="Seg_10092" s="T952">drink-HAB-FUT-1SG.O</ta>
            <ta e="T954" id="Seg_10093" s="T953">husband.[NOM]-3SG</ta>
            <ta e="T955" id="Seg_10094" s="T954">suddenly</ta>
            <ta e="T956" id="Seg_10095" s="T955">beat-PFV-3SG.O</ta>
            <ta e="T957" id="Seg_10096" s="T956">and</ta>
            <ta e="T958" id="Seg_10097" s="T957">kill-CO-3SG.O</ta>
            <ta e="T959" id="Seg_10098" s="T958">child-PL.[NOM]</ta>
            <ta e="T960" id="Seg_10099" s="T959">cry-CVB</ta>
            <ta e="T961" id="Seg_10100" s="T960">begin-DRV-EP-3PL</ta>
            <ta e="T962" id="Seg_10101" s="T961">daughter-DIM-ACC-3SG</ta>
            <ta e="T963" id="Seg_10102" s="T962">suddenly</ta>
            <ta e="T964" id="Seg_10103" s="T963">beat-INFER-3SG.O</ta>
            <ta e="T965" id="Seg_10104" s="T964">and</ta>
            <ta e="T966" id="Seg_10105" s="T965">kill-CO-3SG.O</ta>
            <ta e="T967" id="Seg_10106" s="T966">and</ta>
            <ta e="T968" id="Seg_10107" s="T967">son-DIM-3SG-ALL</ta>
            <ta e="T969" id="Seg_10108" s="T968">say-3SG.S</ta>
            <ta e="T970" id="Seg_10109" s="T969">we.[NOM]</ta>
            <ta e="T971" id="Seg_10110" s="T970">you.SG-COM</ta>
            <ta e="T972" id="Seg_10111" s="T971">live-FUT-1DU</ta>
            <ta e="T973" id="Seg_10112" s="T972">NEG.IMP</ta>
            <ta e="T974" id="Seg_10113" s="T973">cry-IMP.2SG.S</ta>
            <ta e="T975" id="Seg_10114" s="T974">leave-CO-3SG.S</ta>
            <ta e="T976" id="Seg_10115" s="T975">ground-ADJZ</ta>
            <ta e="T977" id="Seg_10116" s="T976">dig.out-IPFV-CVB</ta>
            <ta e="T978" id="Seg_10117" s="T977">ground-ADJZ</ta>
            <ta e="T979" id="Seg_10118" s="T978">hole-EP-ACC</ta>
            <ta e="T980" id="Seg_10119" s="T979">dig.out-CO-3SG.O</ta>
            <ta e="T981" id="Seg_10120" s="T980">(s)he-PL-ACC</ta>
            <ta e="T982" id="Seg_10121" s="T981">there-ADV.LOC</ta>
            <ta e="T983" id="Seg_10122" s="T982">throw.out-DRV-EP-3SG.O</ta>
            <ta e="T984" id="Seg_10123" s="T983">throw.out-CO-3SG.O</ta>
            <ta e="T985" id="Seg_10124" s="T984">and</ta>
            <ta e="T986" id="Seg_10125" s="T985">ground-INSTR</ta>
            <ta e="T987" id="Seg_10126" s="T986">bury-DRV-CO-3SG.O</ta>
            <ta e="T988" id="Seg_10127" s="T987">boat-EP-ACC-3SG</ta>
            <ta e="T989" id="Seg_10128" s="T988">to.the.river</ta>
            <ta e="T990" id="Seg_10129" s="T989">pull-DRV-CVB</ta>
            <ta e="T991" id="Seg_10130" s="T990">leave-TR-3SG.O</ta>
            <ta e="T992" id="Seg_10131" s="T991">thing-PL-ACC-3SG</ta>
            <ta e="T993" id="Seg_10132" s="T992">all</ta>
            <ta e="T994" id="Seg_10133" s="T993">pull.out-CO-3SG.O</ta>
            <ta e="T995" id="Seg_10134" s="T994">son-DIM-OBL.3SG-COM</ta>
            <ta e="T996" id="Seg_10135" s="T995">sit.down-3DU.S</ta>
            <ta e="T997" id="Seg_10136" s="T996">and</ta>
            <ta e="T998" id="Seg_10137" s="T997">here</ta>
            <ta e="T999" id="Seg_10138" s="T998">leave-INFER-3DU.S</ta>
            <ta e="T1000" id="Seg_10139" s="T999">come-CVB</ta>
            <ta e="T1001" id="Seg_10140" s="T1000">achieve-3DU.S</ta>
            <ta e="T1002" id="Seg_10141" s="T1001">own.3SG</ta>
            <ta e="T1003" id="Seg_10142" s="T1002">village-ILL.3SG</ta>
            <ta e="T1004" id="Seg_10143" s="T1003">human.being-PL.[NOM]-3SG</ta>
            <ta e="T1005" id="Seg_10144" s="T1004">become.glad-DUR-3PL</ta>
            <ta e="T1006" id="Seg_10145" s="T1005">and</ta>
            <ta e="T1007" id="Seg_10146" s="T1006">wife.[NOM]-2SG</ta>
            <ta e="T1008" id="Seg_10147" s="T1007">where</ta>
            <ta e="T1009" id="Seg_10148" s="T1008">be-3SG.S</ta>
            <ta e="T1010" id="Seg_10149" s="T1009">wife.[NOM]-1SG</ta>
            <ta e="T1011" id="Seg_10150" s="T1010">die-PST.NAR.[3SG.S]</ta>
            <ta e="T1012" id="Seg_10151" s="T1011">all</ta>
            <ta e="T1013" id="Seg_10152" s="T1012">pull.out-CO-3PL</ta>
            <ta e="T1014" id="Seg_10153" s="T1013">thing-ACC</ta>
            <ta e="T1015" id="Seg_10154" s="T1014">(s)he.[NOM]</ta>
            <ta e="T1016" id="Seg_10155" s="T1015">self-ALL.3SG</ta>
            <ta e="T1017" id="Seg_10156" s="T1016">house.[NOM]</ta>
            <ta e="T1018" id="Seg_10157" s="T1017">buy-INFER-3SG.O</ta>
            <ta e="T1019" id="Seg_10158" s="T1018">and</ta>
            <ta e="T1020" id="Seg_10159" s="T1019">get.married-3SG.S</ta>
            <ta e="T1021" id="Seg_10160" s="T1020">and</ta>
            <ta e="T1022" id="Seg_10161" s="T1021">live-CVB</ta>
            <ta e="T1023" id="Seg_10162" s="T1022">here</ta>
            <ta e="T1024" id="Seg_10163" s="T1023">begin-DRV-EP-INFER.[3SG.S]</ta>
            <ta e="T1025" id="Seg_10164" s="T1024">I.NOM</ta>
            <ta e="T1026" id="Seg_10165" s="T1025">last.year</ta>
            <ta e="T1027" id="Seg_10166" s="T1026">(s)he-ADES</ta>
            <ta e="T1028" id="Seg_10167" s="T1027">be-PST-1SG.S</ta>
            <ta e="T1029" id="Seg_10168" s="T1028">to.such.extent</ta>
            <ta e="T1030" id="Seg_10169" s="T1029">good-ADVZ</ta>
            <ta e="T1031" id="Seg_10170" s="T1030">live.[3SG.S]</ta>
            <ta e="T1032" id="Seg_10171" s="T1031">wife.[NOM]-3SG</ta>
            <ta e="T1033" id="Seg_10172" s="T1032">good</ta>
            <ta e="T1034" id="Seg_10173" s="T1033">I.ACC</ta>
            <ta e="T1035" id="Seg_10174" s="T1034">tea-PL-INSTR</ta>
            <ta e="T1036" id="Seg_10175" s="T1035">I.ACC</ta>
            <ta e="T1037" id="Seg_10176" s="T1036">drink-TR-PST.[3SG.S]</ta>
            <ta e="T1038" id="Seg_10177" s="T1037">I.ALL</ta>
            <ta e="T1039" id="Seg_10178" s="T1038">say-3SG.S</ta>
            <ta e="T1040" id="Seg_10179" s="T1039">we-ALL.1PL</ta>
            <ta e="T1041" id="Seg_10180" s="T1040">walk-HAB-IMP.2SG.S</ta>
            <ta e="T1042" id="Seg_10181" s="T1041">I.NOM</ta>
            <ta e="T1043" id="Seg_10182" s="T1042">(s)he-PL-ALL</ta>
            <ta e="T1044" id="Seg_10183" s="T1043">all.the.time</ta>
            <ta e="T1045" id="Seg_10184" s="T1044">stop.in-EP-HAB-CO-1SG.S</ta>
            <ta e="T1046" id="Seg_10185" s="T1045">this-year-ADV.LOC</ta>
            <ta e="T1047" id="Seg_10186" s="T1046">village-ILL</ta>
            <ta e="T1048" id="Seg_10187" s="T1047">leave-FUT-1SG.S</ta>
            <ta e="T1049" id="Seg_10188" s="T1048">(s)he-PL-ALL</ta>
            <ta e="T1050" id="Seg_10189" s="T1049">again</ta>
            <ta e="T1051" id="Seg_10190" s="T1050">come.in-HAB-FUT-1SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_10191" s="T1">Христян.[NOM]</ta>
            <ta e="T3" id="Seg_10192" s="T2">старик.[NOM]</ta>
            <ta e="T4" id="Seg_10193" s="T3">родиться-INFER-3SG.S</ta>
            <ta e="T5" id="Seg_10194" s="T4">Христян.[NOM]</ta>
            <ta e="T6" id="Seg_10195" s="T5">старик.[NOM]</ta>
            <ta e="T7" id="Seg_10196" s="T6">он(а)-ADES</ta>
            <ta e="T8" id="Seg_10197" s="T7">три</ta>
            <ta e="T9" id="Seg_10198" s="T8">сын.[NOM]-3SG</ta>
            <ta e="T10" id="Seg_10199" s="T9">сын-PL.[NOM]-3SG</ta>
            <ta e="T11" id="Seg_10200" s="T10">все</ta>
            <ta e="T12" id="Seg_10201" s="T11">жениться-RES-3PL</ta>
            <ta e="T13" id="Seg_10202" s="T12">один</ta>
            <ta e="T14" id="Seg_10203" s="T13">сын.[NOM]-3SG</ta>
            <ta e="T15" id="Seg_10204" s="T14">все.время</ta>
            <ta e="T16" id="Seg_10205" s="T15">зверь-CAP-CVB</ta>
            <ta e="T17" id="Seg_10206" s="T16">ходить-HAB-3SG.S</ta>
            <ta e="T18" id="Seg_10207" s="T17">старший</ta>
            <ta e="T19" id="Seg_10208" s="T18">сын.[NOM]-3SG</ta>
            <ta e="T20" id="Seg_10209" s="T19">а</ta>
            <ta e="T21" id="Seg_10210" s="T20">два</ta>
            <ta e="T22" id="Seg_10211" s="T21">два</ta>
            <ta e="T23" id="Seg_10212" s="T22">сын.[NOM]-3SG</ta>
            <ta e="T24" id="Seg_10213" s="T23">скотина-PL-COM</ta>
            <ta e="T25" id="Seg_10214" s="T24">ходить-HAB-CO-3DU.S</ta>
            <ta e="T26" id="Seg_10215" s="T25">корова-PL.[NOM]</ta>
            <ta e="T27" id="Seg_10216" s="T26">держать-INFER-3PL</ta>
            <ta e="T28" id="Seg_10217" s="T27">лошадь-PL.[NOM]</ta>
            <ta e="T29" id="Seg_10218" s="T28">и</ta>
            <ta e="T30" id="Seg_10219" s="T29">овца-PL.[NOM]</ta>
            <ta e="T31" id="Seg_10220" s="T30">и</ta>
            <ta e="T32" id="Seg_10221" s="T31">пашня.[NOM]</ta>
            <ta e="T33" id="Seg_10222" s="T32">держать-INFER-3PL</ta>
            <ta e="T34" id="Seg_10223" s="T33">еда.[NOM]-3SG</ta>
            <ta e="T35" id="Seg_10224" s="T34">сеять-HAB-INFER-3PL</ta>
            <ta e="T36" id="Seg_10225" s="T35">а</ta>
            <ta e="T37" id="Seg_10226" s="T36">старший</ta>
            <ta e="T38" id="Seg_10227" s="T37">сын.[NOM]-3SG</ta>
            <ta e="T39" id="Seg_10228" s="T38">все.время</ta>
            <ta e="T40" id="Seg_10229" s="T39">тайга-EP-ILL</ta>
            <ta e="T41" id="Seg_10230" s="T40">ходить-3SG.S</ta>
            <ta e="T42" id="Seg_10231" s="T41">ходить-HAB-3SG.S</ta>
            <ta e="T43" id="Seg_10232" s="T42">он(а)-ADES</ta>
            <ta e="T44" id="Seg_10233" s="T43">дочь.[NOM]-3SG</ta>
            <ta e="T45" id="Seg_10234" s="T44">родиться-INFER.[3SG.S]</ta>
            <ta e="T46" id="Seg_10235" s="T45">и</ta>
            <ta e="T47" id="Seg_10236" s="T46">сын-DIM.[NOM]</ta>
            <ta e="T48" id="Seg_10237" s="T47">родиться-INFER.[3SG.S]</ta>
            <ta e="T49" id="Seg_10238" s="T48">он(а)-EP-DU.[NOM]</ta>
            <ta e="T50" id="Seg_10239" s="T49">большой-ADVZ</ta>
            <ta e="T51" id="Seg_10240" s="T50">стать-EP-3DU.S</ta>
            <ta e="T52" id="Seg_10241" s="T51">стать-EP-3DU.S</ta>
            <ta e="T53" id="Seg_10242" s="T52">а</ta>
            <ta e="T54" id="Seg_10243" s="T53">он(а).[NOM]</ta>
            <ta e="T55" id="Seg_10244" s="T54">туда-ADV.LOC</ta>
            <ta e="T56" id="Seg_10245" s="T55">тайга-EP-LOC</ta>
            <ta e="T57" id="Seg_10246" s="T56">большой</ta>
            <ta e="T58" id="Seg_10247" s="T57">дом.[NOM]</ta>
            <ta e="T59" id="Seg_10248" s="T58">сесть-TR-PST.NAR-INFER-3SG.O</ta>
            <ta e="T60" id="Seg_10249" s="T59">большой</ta>
            <ta e="T61" id="Seg_10250" s="T60">дом.[NOM]</ta>
            <ta e="T62" id="Seg_10251" s="T61">мать-OBL.3SG-ALL</ta>
            <ta e="T63" id="Seg_10252" s="T62">отец-OBL.3SG-ALL</ta>
            <ta e="T64" id="Seg_10253" s="T63">сказать-3SG.S</ta>
            <ta e="T65" id="Seg_10254" s="T64">икона-INSTR</ta>
            <ta e="T66" id="Seg_10255" s="T65">я.ACC</ta>
            <ta e="T67" id="Seg_10256" s="T66">благословить-IMP.2SG.S</ta>
            <ta e="T68" id="Seg_10257" s="T67">мать.[NOM]-3SG</ta>
            <ta e="T69" id="Seg_10258" s="T68">икона-EP-ACC</ta>
            <ta e="T70" id="Seg_10259" s="T69">взять-CO-3SG.O</ta>
            <ta e="T71" id="Seg_10260" s="T70">и</ta>
            <ta e="T72" id="Seg_10261" s="T71">благословить-CO-3SG.O</ta>
            <ta e="T73" id="Seg_10262" s="T72">я.NOM</ta>
            <ta e="T74" id="Seg_10263" s="T73">отправиться-FUT-1SG.S</ta>
            <ta e="T75" id="Seg_10264" s="T74">три</ta>
            <ta e="T76" id="Seg_10265" s="T75">брат-DYA-PL.[NOM]-1PL</ta>
            <ta e="T77" id="Seg_10266" s="T76">один</ta>
            <ta e="T78" id="Seg_10267" s="T77">дом-LOC</ta>
            <ta e="T79" id="Seg_10268" s="T78">NEG</ta>
            <ta e="T80" id="Seg_10269" s="T79">жить-FUT-1PL</ta>
            <ta e="T81" id="Seg_10270" s="T80">я-ADES</ta>
            <ta e="T82" id="Seg_10271" s="T81">теперь</ta>
            <ta e="T83" id="Seg_10272" s="T82">семья.[NOM]-1SG</ta>
            <ta e="T84" id="Seg_10273" s="T83">лодка-ILL</ta>
            <ta e="T85" id="Seg_10274" s="T84">весь</ta>
            <ta e="T86" id="Seg_10275" s="T85">вещь-ACC-3SG</ta>
            <ta e="T87" id="Seg_10276" s="T86">вытащить-CO-3SG.O</ta>
            <ta e="T88" id="Seg_10277" s="T87">и</ta>
            <ta e="T89" id="Seg_10278" s="T88">ну</ta>
            <ta e="T90" id="Seg_10279" s="T89">отправиться-INFER-3PL</ta>
            <ta e="T91" id="Seg_10280" s="T90">отец.[NOM]-3SG</ta>
            <ta e="T92" id="Seg_10281" s="T91">мать.[NOM]-3SG</ta>
            <ta e="T93" id="Seg_10282" s="T92">и</ta>
            <ta e="T94" id="Seg_10283" s="T93">плакать-CVB</ta>
            <ta e="T95" id="Seg_10284" s="T94">ну</ta>
            <ta e="T96" id="Seg_10285" s="T95">остаться-INFER-3PL</ta>
            <ta e="T97" id="Seg_10286" s="T96">и</ta>
            <ta e="T98" id="Seg_10287" s="T97">он(а)-PL.[NOM]</ta>
            <ta e="T99" id="Seg_10288" s="T98">и</ta>
            <ta e="T100" id="Seg_10289" s="T99">ну</ta>
            <ta e="T101" id="Seg_10290" s="T100">отправиться-INFER-3PL</ta>
            <ta e="T102" id="Seg_10291" s="T101">ну</ta>
            <ta e="T103" id="Seg_10292" s="T102">и</ta>
            <ta e="T104" id="Seg_10293" s="T103">прийти-CVB</ta>
            <ta e="T105" id="Seg_10294" s="T104">ну</ta>
            <ta e="T106" id="Seg_10295" s="T105">достичь-INFER-3PL</ta>
            <ta e="T107" id="Seg_10296" s="T106">а</ta>
            <ta e="T108" id="Seg_10297" s="T107">он(а)-ADES</ta>
            <ta e="T109" id="Seg_10298" s="T108">туда-ADV.LOC</ta>
            <ta e="T110" id="Seg_10299" s="T109">большой</ta>
            <ta e="T111" id="Seg_10300" s="T110">дом.[NOM]</ta>
            <ta e="T112" id="Seg_10301" s="T111">сесть-TR-PST.NAR-INFER-3SG.O</ta>
            <ta e="T113" id="Seg_10302" s="T112">весь</ta>
            <ta e="T114" id="Seg_10303" s="T113">вытащить-CO-3PL</ta>
            <ta e="T115" id="Seg_10304" s="T114">туда-ILL</ta>
            <ta e="T116" id="Seg_10305" s="T115">ну</ta>
            <ta e="T117" id="Seg_10306" s="T116">и</ta>
            <ta e="T118" id="Seg_10307" s="T117">муж.[NOM]-3SG</ta>
            <ta e="T119" id="Seg_10308" s="T118">весь</ta>
            <ta e="T120" id="Seg_10309" s="T119">сделать-CVB</ta>
            <ta e="T121" id="Seg_10310" s="T120">принести-EP-FRQ-CO-3SG.O</ta>
            <ta e="T122" id="Seg_10311" s="T121">кровать.[NOM]</ta>
            <ta e="T123" id="Seg_10312" s="T122">сделать-INFER-3SG.O</ta>
            <ta e="T124" id="Seg_10313" s="T123">утро-день-ADV.LOC</ta>
            <ta e="T125" id="Seg_10314" s="T124">тайга-EP-ILL</ta>
            <ta e="T126" id="Seg_10315" s="T125">отправиться-FUT-1SG.S</ta>
            <ta e="T127" id="Seg_10316" s="T126">а</ta>
            <ta e="T128" id="Seg_10317" s="T127">ты.NOM</ta>
            <ta e="T129" id="Seg_10318" s="T128">вода-TRL</ta>
            <ta e="T130" id="Seg_10319" s="T129">ну</ta>
            <ta e="T131" id="Seg_10320" s="T130">отправиться-POT.FUT.2SG</ta>
            <ta e="T132" id="Seg_10321" s="T131">нога-ILL.2SG</ta>
            <ta e="T133" id="Seg_10322" s="T132">что-INDEF2</ta>
            <ta e="T134" id="Seg_10323" s="T133">стружка.[NOM]</ta>
            <ta e="T135" id="Seg_10324" s="T134">попало-ADVZ</ta>
            <ta e="T136" id="Seg_10325" s="T135">NEG.IMP</ta>
            <ta e="T137" id="Seg_10326" s="T136">стать.[3SG.S]</ta>
            <ta e="T138" id="Seg_10327" s="T137">а.то</ta>
            <ta e="T139" id="Seg_10328" s="T138">плохой-ADVZ</ta>
            <ta e="T140" id="Seg_10329" s="T139">быть-FUT-3SG.S</ta>
            <ta e="T141" id="Seg_10330" s="T140">ну</ta>
            <ta e="T142" id="Seg_10331" s="T141">ночевать-3PL</ta>
            <ta e="T143" id="Seg_10332" s="T142">утро-нечто-LOC</ta>
            <ta e="T144" id="Seg_10333" s="T143">встать-3SG.S</ta>
            <ta e="T145" id="Seg_10334" s="T144">съесть-FRQ-CO-3SG.S</ta>
            <ta e="T146" id="Seg_10335" s="T145">и</ta>
            <ta e="T147" id="Seg_10336" s="T146">ну</ta>
            <ta e="T148" id="Seg_10337" s="T147">отправиться-INFER.[3SG.S]</ta>
            <ta e="T149" id="Seg_10338" s="T148">тайга-EP-ILL</ta>
            <ta e="T150" id="Seg_10339" s="T149">а</ta>
            <ta e="T151" id="Seg_10340" s="T150">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T152" id="Seg_10341" s="T151">сказать-3SG.S</ta>
            <ta e="T153" id="Seg_10342" s="T152">что-TRL</ta>
            <ta e="T154" id="Seg_10343" s="T153">я.ALL</ta>
            <ta e="T155" id="Seg_10344" s="T154">муж.[NOM]-1SG</ta>
            <ta e="T156" id="Seg_10345" s="T155">сказать-3SG.S</ta>
            <ta e="T157" id="Seg_10346" s="T156">что</ta>
            <ta e="T158" id="Seg_10347" s="T157">стружка.[NOM]</ta>
            <ta e="T159" id="Seg_10348" s="T158">вода-ILL</ta>
            <ta e="T160" id="Seg_10349" s="T159">попало-ADVZ</ta>
            <ta e="T161" id="Seg_10350" s="T160">NEG.IMP</ta>
            <ta e="T162" id="Seg_10351" s="T161">стать.[3SG.S]</ta>
            <ta e="T163" id="Seg_10352" s="T162">я.NOM</ta>
            <ta e="T164" id="Seg_10353" s="T163">сейчас</ta>
            <ta e="T165" id="Seg_10354" s="T164">два</ta>
            <ta e="T166" id="Seg_10355" s="T165">ведро-ACC</ta>
            <ta e="T167" id="Seg_10356" s="T166">стружка-PL-INSTR</ta>
            <ta e="T168" id="Seg_10357" s="T167">набить-%%-FUT-1SG.O</ta>
            <ta e="T169" id="Seg_10358" s="T168">набить-%%-FUT-1SG.O</ta>
            <ta e="T170" id="Seg_10359" s="T169">отправиться-CVB</ta>
            <ta e="T171" id="Seg_10360" s="T170">вода-ILL</ta>
            <ta e="T172" id="Seg_10361" s="T171">высыпать-FUT-1SG.O</ta>
            <ta e="T173" id="Seg_10362" s="T172">и</ta>
            <ta e="T174" id="Seg_10363" s="T173">ну</ta>
            <ta e="T175" id="Seg_10364" s="T174">отправиться-TR-INFER-3SG.O</ta>
            <ta e="T176" id="Seg_10365" s="T175">и</ta>
            <ta e="T177" id="Seg_10366" s="T176">налить-3SG.O</ta>
            <ta e="T178" id="Seg_10367" s="T177">стоять-HAB.[3SG.S]</ta>
            <ta e="T179" id="Seg_10368" s="T178">и</ta>
            <ta e="T180" id="Seg_10369" s="T179">посмотреть-DUR-3SG.O</ta>
            <ta e="T181" id="Seg_10370" s="T180">а</ta>
            <ta e="T182" id="Seg_10371" s="T181">туда-ADV.LOC</ta>
            <ta e="T183" id="Seg_10372" s="T182">течь.быстро-CO-3SG.S</ta>
            <ta e="T184" id="Seg_10373" s="T183">он(а).[NOM]</ta>
            <ta e="T185" id="Seg_10374" s="T184">вода-EP-ACC</ta>
            <ta e="T186" id="Seg_10375" s="T185">черпать-INCH-CO-3SG.O</ta>
            <ta e="T187" id="Seg_10376" s="T186">и</ta>
            <ta e="T188" id="Seg_10377" s="T187">дом-ILL</ta>
            <ta e="T189" id="Seg_10378" s="T188">прийти-CO.[3SG.S]</ta>
            <ta e="T190" id="Seg_10379" s="T189">а</ta>
            <ta e="T191" id="Seg_10380" s="T190">он(а).[NOM]</ta>
            <ta e="T192" id="Seg_10381" s="T191">красивый</ta>
            <ta e="T193" id="Seg_10382" s="T192">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T194" id="Seg_10383" s="T193">быть-PST.[3SG.S]</ta>
            <ta e="T195" id="Seg_10384" s="T194">отправиться-CVB</ta>
            <ta e="T196" id="Seg_10385" s="T195">комната-EMPH</ta>
            <ta e="T197" id="Seg_10386" s="T196">дом-ILL</ta>
            <ta e="T198" id="Seg_10387" s="T197">сесть-3SG.S</ta>
            <ta e="T199" id="Seg_10388" s="T198">и</ta>
            <ta e="T200" id="Seg_10389" s="T199">сшить-EP-FRQ-CVB</ta>
            <ta e="T201" id="Seg_10390" s="T200">начать-DRV-CO-3SG.S</ta>
            <ta e="T202" id="Seg_10391" s="T201">а</ta>
            <ta e="T203" id="Seg_10392" s="T202">два</ta>
            <ta e="T204" id="Seg_10393" s="T203">ребенок.[NOM]</ta>
            <ta e="T205" id="Seg_10394" s="T204">%играть-3DU.S</ta>
            <ta e="T206" id="Seg_10395" s="T205">разбойник-PL.[NOM]</ta>
            <ta e="T207" id="Seg_10396" s="T206">ходить-3PL</ta>
            <ta e="T208" id="Seg_10397" s="T207">обласок-INSTR</ta>
            <ta e="T209" id="Seg_10398" s="T208">и</ta>
            <ta e="T210" id="Seg_10399" s="T209">сказать-3PL</ta>
            <ta e="T211" id="Seg_10400" s="T210">что.[NOM]</ta>
            <ta e="T212" id="Seg_10401" s="T211">стружка-PL.[NOM]</ta>
            <ta e="T213" id="Seg_10402" s="T212">течь.быстро-INFER-3PL</ta>
            <ta e="T214" id="Seg_10403" s="T213">стреж-INSTR</ta>
            <ta e="T215" id="Seg_10404" s="T214">ходить-INFER-3PL</ta>
            <ta e="T216" id="Seg_10405" s="T215">крутой.берег-GEN</ta>
            <ta e="T217" id="Seg_10406" s="T216">верхняя.часть-EP-ALL</ta>
            <ta e="T218" id="Seg_10407" s="T217">направлять-IMP.2SG.O</ta>
            <ta e="T219" id="Seg_10408" s="T218">он(а)-PL.[NOM]</ta>
            <ta e="T220" id="Seg_10409" s="T219">посмотреть-DUR-3PL</ta>
            <ta e="T221" id="Seg_10410" s="T220">что-INDEF.[NOM]</ta>
            <ta e="T222" id="Seg_10411" s="T221">дорога.[NOM]</ta>
            <ta e="T223" id="Seg_10412" s="T222">он(а)-PL.[NOM]</ta>
            <ta e="T224" id="Seg_10413" s="T223">дорога-DIM.[NOM]</ta>
            <ta e="T225" id="Seg_10414" s="T224">найти-INFER-3PL</ta>
            <ta e="T226" id="Seg_10415" s="T225">раньше</ta>
            <ta e="T227" id="Seg_10416" s="T226">мы.[NOM]</ta>
            <ta e="T228" id="Seg_10417" s="T227">этот</ta>
            <ta e="T229" id="Seg_10418" s="T228">дорога-ACC</ta>
            <ta e="T230" id="Seg_10419" s="T229">NEG</ta>
            <ta e="T231" id="Seg_10420" s="T230">увидеть-DRV-HAB-PST-1PL</ta>
            <ta e="T232" id="Seg_10421" s="T231">этот</ta>
            <ta e="T233" id="Seg_10422" s="T232">дорога-EP-DIM-ILL</ta>
            <ta e="T234" id="Seg_10423" s="T233">этот</ta>
            <ta e="T235" id="Seg_10424" s="T234">дорога-EP-ILL</ta>
            <ta e="T236" id="Seg_10425" s="T235">этот</ta>
            <ta e="T237" id="Seg_10426" s="T236">дорога-EP-ILL</ta>
            <ta e="T238" id="Seg_10427" s="T237">и</ta>
            <ta e="T239" id="Seg_10428" s="T238">остановиться-EP-3PL</ta>
            <ta e="T240" id="Seg_10429" s="T239">атаман.[NOM]</ta>
            <ta e="T241" id="Seg_10430" s="T240">сказать-3SG.S</ta>
            <ta e="T242" id="Seg_10431" s="T241">вы.PL.NOM</ta>
            <ta e="T243" id="Seg_10432" s="T242">сидеть-IMP.2PL.S/O</ta>
            <ta e="T244" id="Seg_10433" s="T243">сюда-ADV.LOC</ta>
            <ta e="T245" id="Seg_10434" s="T244">а</ta>
            <ta e="T246" id="Seg_10435" s="T245">я.NOM</ta>
            <ta e="T247" id="Seg_10436" s="T246">бегать-MOM-OPT-1SG.S</ta>
            <ta e="T248" id="Seg_10437" s="T247">посмотреть-DUR-OPT-1SG.O</ta>
            <ta e="T249" id="Seg_10438" s="T248">что.[NOM]</ta>
            <ta e="T250" id="Seg_10439" s="T249">жить-INFER.[3SG.S]</ta>
            <ta e="T251" id="Seg_10440" s="T250">сюда-ADV.LOC</ta>
            <ta e="T252" id="Seg_10441" s="T251">ну</ta>
            <ta e="T253" id="Seg_10442" s="T252">бегать-MOM-INFER.[3SG.S]</ta>
            <ta e="T254" id="Seg_10443" s="T253">прийти-CO.[3SG.S]</ta>
            <ta e="T255" id="Seg_10444" s="T254">зайти-CO-3SG.S</ta>
            <ta e="T256" id="Seg_10445" s="T255">дом-ILL</ta>
            <ta e="T257" id="Seg_10446" s="T256">здравствуй</ta>
            <ta e="T258" id="Seg_10447" s="T257">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T259" id="Seg_10448" s="T258">сказать-3SG.S</ta>
            <ta e="T260" id="Seg_10449" s="T259">здравствуй</ta>
            <ta e="T261" id="Seg_10450" s="T260">здравствуй</ta>
            <ta e="T262" id="Seg_10451" s="T261">стул.[NOM]</ta>
            <ta e="T263" id="Seg_10452" s="T262">поставить-CVB</ta>
            <ta e="T264" id="Seg_10453" s="T263">сделать-DRV-TR-3SG.O</ta>
            <ta e="T265" id="Seg_10454" s="T264">сказать-3SG.S</ta>
            <ta e="T266" id="Seg_10455" s="T265">сесть-IMP.2SG.S</ta>
            <ta e="T267" id="Seg_10456" s="T266">сюда</ta>
            <ta e="T268" id="Seg_10457" s="T267">а</ta>
            <ta e="T269" id="Seg_10458" s="T268">он(а).[NOM]</ta>
            <ta e="T270" id="Seg_10459" s="T269">сесть-3SG.S</ta>
            <ta e="T271" id="Seg_10460" s="T270">говорить-CVB</ta>
            <ta e="T272" id="Seg_10461" s="T271">ну</ta>
            <ta e="T273" id="Seg_10462" s="T272">начать-DRV-EP-INFER-3DU.S</ta>
            <ta e="T274" id="Seg_10463" s="T273">атаман.[NOM]</ta>
            <ta e="T275" id="Seg_10464" s="T274">сказать-3SG.S</ta>
            <ta e="T276" id="Seg_10465" s="T275">ты.NOM</ta>
            <ta e="T277" id="Seg_10466" s="T276">я.ALL</ta>
            <ta e="T278" id="Seg_10467" s="T277">выйти.замуж-OPT-2SG.S</ta>
            <ta e="T279" id="Seg_10468" s="T278">но</ta>
            <ta e="T280" id="Seg_10469" s="T279">я-ADES</ta>
            <ta e="T281" id="Seg_10470" s="T280">муж.[NOM]-1SG</ta>
            <ta e="T282" id="Seg_10471" s="T281">сила-COM</ta>
            <ta e="T283" id="Seg_10472" s="T282">он(а).[NOM]</ta>
            <ta e="T284" id="Seg_10473" s="T283">ты.ACC</ta>
            <ta e="T285" id="Seg_10474" s="T284">убить-FUT-3SG.O</ta>
            <ta e="T286" id="Seg_10475" s="T285">и</ta>
            <ta e="T287" id="Seg_10476" s="T286">я.ACC</ta>
            <ta e="T288" id="Seg_10477" s="T287">убить-FUT-3SG.O</ta>
            <ta e="T289" id="Seg_10478" s="T288">а</ta>
            <ta e="T290" id="Seg_10479" s="T289">я.NOM</ta>
            <ta e="T291" id="Seg_10480" s="T290">ты.ALL</ta>
            <ta e="T292" id="Seg_10481" s="T291">веревка.[NOM]</ta>
            <ta e="T293" id="Seg_10482" s="T292">сделать-INFER-FUT-1SG.S</ta>
            <ta e="T294" id="Seg_10483" s="T293">волос.[NOM]</ta>
            <ta e="T295" id="Seg_10484" s="T294">быть-POT.FUT.3SG</ta>
            <ta e="T296" id="Seg_10485" s="T295">и</ta>
            <ta e="T299" id="Seg_10486" s="T298">он(а).[NOM]</ta>
            <ta e="T300" id="Seg_10487" s="T299">этот</ta>
            <ta e="T301" id="Seg_10488" s="T300">веревка-ACC</ta>
            <ta e="T302" id="Seg_10489" s="T301">NEG</ta>
            <ta e="T303" id="Seg_10490" s="T302">порвать-FUT-3SG.O</ta>
            <ta e="T304" id="Seg_10491" s="T303">он(а).[NOM]</ta>
            <ta e="T305" id="Seg_10492" s="T304">ну</ta>
            <ta e="T306" id="Seg_10493" s="T305">прийти-POT.FUT.3SG</ta>
            <ta e="T307" id="Seg_10494" s="T306">вечер-нечто-LOC</ta>
            <ta e="T308" id="Seg_10495" s="T307">ты.NOM</ta>
            <ta e="T309" id="Seg_10496" s="T308">сказать-IMP.2SG.S</ta>
            <ta e="T310" id="Seg_10497" s="T309">карта-VBLZ-OPT-1DU</ta>
            <ta e="T311" id="Seg_10498" s="T310">он(а)-ALL</ta>
            <ta e="T312" id="Seg_10499" s="T311">сказать-IMP.2SG.S</ta>
            <ta e="T313" id="Seg_10500" s="T312">кто.[NOM]</ta>
            <ta e="T314" id="Seg_10501" s="T313">дурак-EP-ADVZ</ta>
            <ta e="T315" id="Seg_10502" s="T314">остаться-FUT-3SG.S</ta>
            <ta e="T316" id="Seg_10503" s="T315">рука-PL-ACC-3SG</ta>
            <ta e="T317" id="Seg_10504" s="T316">назад</ta>
            <ta e="T318" id="Seg_10505" s="T317">привязать-INF</ta>
            <ta e="T319" id="Seg_10506" s="T318">а</ta>
            <ta e="T320" id="Seg_10507" s="T319">ты.NOM</ta>
            <ta e="T321" id="Seg_10508" s="T320">он(а)-EP-ACC</ta>
            <ta e="T322" id="Seg_10509" s="T321">дурак-EP-ADVZ</ta>
            <ta e="T323" id="Seg_10510" s="T322">оставить-IMP.2SG.O</ta>
            <ta e="T324" id="Seg_10511" s="T323">рука-PL-ACC-3SG</ta>
            <ta e="T325" id="Seg_10512" s="T324">назад</ta>
            <ta e="T326" id="Seg_10513" s="T325">привязать-IMP.2SG.O</ta>
            <ta e="T327" id="Seg_10514" s="T326">он(а).[NOM]</ta>
            <ta e="T328" id="Seg_10515" s="T327">этот</ta>
            <ta e="T329" id="Seg_10516" s="T328">веревка-ACC</ta>
            <ta e="T330" id="Seg_10517" s="T329">NEG</ta>
            <ta e="T331" id="Seg_10518" s="T330">порвать-FUT-3SG.S</ta>
            <ta e="T332" id="Seg_10519" s="T331">он(а).[NOM]</ta>
            <ta e="T333" id="Seg_10520" s="T332">ежели</ta>
            <ta e="T334" id="Seg_10521" s="T333">NEG</ta>
            <ta e="T335" id="Seg_10522" s="T334">порвать-3SG.O</ta>
            <ta e="T336" id="Seg_10523" s="T335">а</ta>
            <ta e="T337" id="Seg_10524" s="T336">ты.NOM</ta>
            <ta e="T338" id="Seg_10525" s="T337">наружу</ta>
            <ta e="T339" id="Seg_10526" s="T338">выходить-IMP.2SG.S</ta>
            <ta e="T340" id="Seg_10527" s="T339">я.ACC</ta>
            <ta e="T341" id="Seg_10528" s="T340">закричать-INCH-IMP.2SG.S</ta>
            <ta e="T342" id="Seg_10529" s="T341">я.NOM</ta>
            <ta e="T343" id="Seg_10530" s="T342">прийти-FUT-1SG.S</ta>
            <ta e="T344" id="Seg_10531" s="T343">он(а)-EP-ACC</ta>
            <ta e="T345" id="Seg_10532" s="T344">и</ta>
            <ta e="T346" id="Seg_10533" s="T345">убить-FUT-1SG.O</ta>
            <ta e="T347" id="Seg_10534" s="T346">ты-COM</ta>
            <ta e="T348" id="Seg_10535" s="T347">жить-FUT-1DU</ta>
            <ta e="T349" id="Seg_10536" s="T348">муж.[NOM]-3SG</ta>
            <ta e="T350" id="Seg_10537" s="T349">прийти-CO.[3SG.S]</ta>
            <ta e="T351" id="Seg_10538" s="T350">он(а).[NOM]</ta>
            <ta e="T352" id="Seg_10539" s="T351">он(а)-EP-ACC</ta>
            <ta e="T353" id="Seg_10540" s="T352">еда-VBLZ-3SG.O</ta>
            <ta e="T354" id="Seg_10541" s="T353">сказать-3SG.S</ta>
            <ta e="T355" id="Seg_10542" s="T354">карта-VBLZ-OPT-1DU</ta>
            <ta e="T356" id="Seg_10543" s="T355">муж.[NOM]-3SG</ta>
            <ta e="T357" id="Seg_10544" s="T356">сказать-3SG.S</ta>
            <ta e="T358" id="Seg_10545" s="T357">я.NOM</ta>
            <ta e="T359" id="Seg_10546" s="T358">устать-RFL-PST.NAR-1SG.S</ta>
            <ta e="T360" id="Seg_10547" s="T359">спать-INF</ta>
            <ta e="T361" id="Seg_10548" s="T360">надо</ta>
            <ta e="T362" id="Seg_10549" s="T361">ну</ta>
            <ta e="T363" id="Seg_10550" s="T362">HORT</ta>
            <ta e="T364" id="Seg_10551" s="T363">карта-VBLZ-OPT-1DU</ta>
            <ta e="T365" id="Seg_10552" s="T364">NEG</ta>
            <ta e="T366" id="Seg_10553" s="T365">долго-ADV.LOC</ta>
            <ta e="T367" id="Seg_10554" s="T366">а.то</ta>
            <ta e="T368" id="Seg_10555" s="T367">сам.1SG</ta>
            <ta e="T369" id="Seg_10556" s="T368">сидеть-1SG.S</ta>
            <ta e="T370" id="Seg_10557" s="T369">и</ta>
            <ta e="T371" id="Seg_10558" s="T370">скучный-ADVZ</ta>
            <ta e="T372" id="Seg_10559" s="T371">я.ALL</ta>
            <ta e="T373" id="Seg_10560" s="T372">быть-3SG.S</ta>
            <ta e="T374" id="Seg_10561" s="T373">ну</ta>
            <ta e="T375" id="Seg_10562" s="T374">и</ta>
            <ta e="T376" id="Seg_10563" s="T375">сесть-3DU.S</ta>
            <ta e="T377" id="Seg_10564" s="T376">карта-VBLZ-CVB</ta>
            <ta e="T378" id="Seg_10565" s="T377">кто.[NOM]</ta>
            <ta e="T379" id="Seg_10566" s="T378">дурак-EP-ADVZ</ta>
            <ta e="T380" id="Seg_10567" s="T379">остаться-FUT-3SG.S</ta>
            <ta e="T381" id="Seg_10568" s="T380">рука-PL-ACC-3SG</ta>
            <ta e="T382" id="Seg_10569" s="T381">привязать-TR-INF</ta>
            <ta e="T383" id="Seg_10570" s="T382">назад</ta>
            <ta e="T384" id="Seg_10571" s="T383">муж.[NOM]-3SG</ta>
            <ta e="T385" id="Seg_10572" s="T384">дурак-EP-ADVZ</ta>
            <ta e="T386" id="Seg_10573" s="T385">остаться-3SG.S</ta>
            <ta e="T387" id="Seg_10574" s="T386">рука-PL-ACC-3SG</ta>
            <ta e="T388" id="Seg_10575" s="T387">надо</ta>
            <ta e="T389" id="Seg_10576" s="T388">привязать-TR-INF</ta>
            <ta e="T390" id="Seg_10577" s="T389">рука-ACC-3SG</ta>
            <ta e="T391" id="Seg_10578" s="T390">надо</ta>
            <ta e="T392" id="Seg_10579" s="T391">привязать-INF</ta>
            <ta e="T393" id="Seg_10580" s="T392">назад</ta>
            <ta e="T394" id="Seg_10581" s="T393">а</ta>
            <ta e="T395" id="Seg_10582" s="T394">ежели</ta>
            <ta e="T396" id="Seg_10583" s="T395">я.NOM</ta>
            <ta e="T397" id="Seg_10584" s="T396">остаться-CONJ-1SG.S</ta>
            <ta e="T398" id="Seg_10585" s="T397">дурак-EP-ADVZ</ta>
            <ta e="T399" id="Seg_10586" s="T398">ты.NOM</ta>
            <ta e="T400" id="Seg_10587" s="T399">IRREAL</ta>
            <ta e="T401" id="Seg_10588" s="T400">я.ALL</ta>
            <ta e="T402" id="Seg_10589" s="T401">рука-PL.[NOM]-1SG</ta>
            <ta e="T403" id="Seg_10590" s="T402">привязать-TR-CONJ-2SG.O</ta>
            <ta e="T404" id="Seg_10591" s="T403">а</ta>
            <ta e="T405" id="Seg_10592" s="T404">теперь</ta>
            <ta e="T406" id="Seg_10593" s="T405">я.NOM</ta>
            <ta e="T407" id="Seg_10594" s="T406">ты.ALL</ta>
            <ta e="T408" id="Seg_10595" s="T407">рука-ACC-OBL.2SG</ta>
            <ta e="T409" id="Seg_10596" s="T408">привязать-FUT-1SG.O</ta>
            <ta e="T410" id="Seg_10597" s="T409">муж.[NOM]-3SG</ta>
            <ta e="T411" id="Seg_10598" s="T410">сказать-3SG.S</ta>
            <ta e="T412" id="Seg_10599" s="T411">привязать-IMP.2SG.O</ta>
            <ta e="T413" id="Seg_10600" s="T412">он(а).[NOM]</ta>
            <ta e="T414" id="Seg_10601" s="T413">привязать-3SG.O</ta>
            <ta e="T415" id="Seg_10602" s="T414">муж.[NOM]-3SG</ta>
            <ta e="T416" id="Seg_10603" s="T415">как</ta>
            <ta e="T417" id="Seg_10604" s="T416">%%-TR-3SG.O</ta>
            <ta e="T418" id="Seg_10605" s="T417">веревка-PL.[NOM]</ta>
            <ta e="T419" id="Seg_10606" s="T418">все</ta>
            <ta e="T420" id="Seg_10607" s="T419">порвать-RES-3PL</ta>
            <ta e="T421" id="Seg_10608" s="T420">ну</ta>
            <ta e="T422" id="Seg_10609" s="T421">и</ta>
            <ta e="T423" id="Seg_10610" s="T422">спать-INF</ta>
            <ta e="T424" id="Seg_10611" s="T423">лечь.спать-CO-3DU.S</ta>
            <ta e="T425" id="Seg_10612" s="T424">утро-нечто-LOC</ta>
            <ta e="T426" id="Seg_10613" s="T425">встать-3SG.S</ta>
            <ta e="T427" id="Seg_10614" s="T426">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T428" id="Seg_10615" s="T427">все</ta>
            <ta e="T429" id="Seg_10616" s="T428">сварить-HAB-CO-3SG.O</ta>
            <ta e="T430" id="Seg_10617" s="T429">мясо-PL-ACC</ta>
            <ta e="T431" id="Seg_10618" s="T430">что-PL-ACC</ta>
            <ta e="T432" id="Seg_10619" s="T431">муж-ACC-3SG</ta>
            <ta e="T433" id="Seg_10620" s="T432">еда-VBLZ-3SG.O</ta>
            <ta e="T434" id="Seg_10621" s="T433">муж.[NOM]-3SG</ta>
            <ta e="T435" id="Seg_10622" s="T434">тайга-EP-ILL</ta>
            <ta e="T436" id="Seg_10623" s="T435">отправиться-CO-3SG.S</ta>
            <ta e="T437" id="Seg_10624" s="T436">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T438" id="Seg_10625" s="T437">наружу</ta>
            <ta e="T439" id="Seg_10626" s="T438">выскочить-3SG.S</ta>
            <ta e="T440" id="Seg_10627" s="T439">и</ta>
            <ta e="T441" id="Seg_10628" s="T440">ну</ta>
            <ta e="T442" id="Seg_10629" s="T441">кричать-MOM-INFER.[3SG.S]</ta>
            <ta e="T443" id="Seg_10630" s="T442">прийти-IMP.2SG.S</ta>
            <ta e="T444" id="Seg_10631" s="T443">муж.[NOM]-1SG</ta>
            <ta e="T445" id="Seg_10632" s="T444">отправиться-PST.NAR.[3SG.S]</ta>
            <ta e="T446" id="Seg_10633" s="T445">атаман.[NOM]</ta>
            <ta e="T447" id="Seg_10634" s="T446">ну</ta>
            <ta e="T448" id="Seg_10635" s="T447">прийти-INFER.[3SG.S]</ta>
            <ta e="T449" id="Seg_10636" s="T448">ну</ta>
            <ta e="T450" id="Seg_10637" s="T449">что.[NOM]</ta>
            <ta e="T451" id="Seg_10638" s="T450">карта-VBLZ-PST-2PL</ta>
            <ta e="T452" id="Seg_10639" s="T451">карта-VBLZ-PST-1DU</ta>
            <ta e="T453" id="Seg_10640" s="T452">я.NOM</ta>
            <ta e="T454" id="Seg_10641" s="T453">ты.ALL</ta>
            <ta e="T455" id="Seg_10642" s="T454">сказать-PST-1SG.S</ta>
            <ta e="T456" id="Seg_10643" s="T455">он(а).[NOM]</ta>
            <ta e="T457" id="Seg_10644" s="T456">сила-COM</ta>
            <ta e="T458" id="Seg_10645" s="T457">быть-3SG.S</ta>
            <ta e="T459" id="Seg_10646" s="T458">этот</ta>
            <ta e="T460" id="Seg_10647" s="T459">веревка-PL-ACC</ta>
            <ta e="T461" id="Seg_10648" s="T460">весь</ta>
            <ta e="T462" id="Seg_10649" s="T461">порвать-PFV-3SG.O</ta>
            <ta e="T463" id="Seg_10650" s="T462">а</ta>
            <ta e="T464" id="Seg_10651" s="T463">этот-день.[NOM]</ta>
            <ta e="T465" id="Seg_10652" s="T464">я.NOM</ta>
            <ta e="T466" id="Seg_10653" s="T465">ты.ALL</ta>
            <ta e="T467" id="Seg_10654" s="T466">дать-FUT-1SG.O</ta>
            <ta e="T468" id="Seg_10655" s="T467">волос.[NOM]</ta>
            <ta e="T469" id="Seg_10656" s="T468">веревка.[NOM]</ta>
            <ta e="T470" id="Seg_10657" s="T469">дать-INFER-FUT-1SG.S</ta>
            <ta e="T471" id="Seg_10658" s="T470">вечер-нечто-LOC</ta>
            <ta e="T472" id="Seg_10659" s="T471">ну</ta>
            <ta e="T473" id="Seg_10660" s="T472">прийти-POT.FUT.3SG</ta>
            <ta e="T474" id="Seg_10661" s="T473">опять</ta>
            <ta e="T475" id="Seg_10662" s="T474">сказать-IMP.2SG.S</ta>
            <ta e="T476" id="Seg_10663" s="T475">карта-VBLZ-OPT-1DU</ta>
            <ta e="T477" id="Seg_10664" s="T476">кто.[NOM]</ta>
            <ta e="T478" id="Seg_10665" s="T477">дурак-EP-ADVZ</ta>
            <ta e="T479" id="Seg_10666" s="T478">остаться-FUT-3SG.S</ta>
            <ta e="T480" id="Seg_10667" s="T479">рука-PL-ACC-3SG</ta>
            <ta e="T481" id="Seg_10668" s="T480">назад</ta>
            <ta e="T482" id="Seg_10669" s="T481">привязать-INF</ta>
            <ta e="T483" id="Seg_10670" s="T482">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T484" id="Seg_10671" s="T483">он(а)-EP-ACC</ta>
            <ta e="T485" id="Seg_10672" s="T484">еда-VBLZ-PST.NAR-3SG.O</ta>
            <ta e="T486" id="Seg_10673" s="T485">вино-INSTR</ta>
            <ta e="T487" id="Seg_10674" s="T486">пить-TR-PST.NAR-3SG.O</ta>
            <ta e="T488" id="Seg_10675" s="T487">вино-PL.[NOM]</ta>
            <ta e="T489" id="Seg_10676" s="T488">пить-INFER-3DU.O</ta>
            <ta e="T490" id="Seg_10677" s="T489">мужчина-человек.[NOM]</ta>
            <ta e="T491" id="Seg_10678" s="T490">отправиться-CO-3SG.S</ta>
            <ta e="T492" id="Seg_10679" s="T491">муж.[NOM]-3SG</ta>
            <ta e="T493" id="Seg_10680" s="T492">прийти-CO.[3SG.S]</ta>
            <ta e="T494" id="Seg_10681" s="T493">зверь-PL-ACC</ta>
            <ta e="T495" id="Seg_10682" s="T494">много-ADVZ</ta>
            <ta e="T496" id="Seg_10683" s="T495">добыть-PST.NAR-3SG.O</ta>
            <ta e="T497" id="Seg_10684" s="T496">и</ta>
            <ta e="T498" id="Seg_10685" s="T497">лось.[NOM]</ta>
            <ta e="T499" id="Seg_10686" s="T498">добыть-PST.NAR-INFER-3SG.O</ta>
            <ta e="T500" id="Seg_10687" s="T499">он(а).[NOM]</ta>
            <ta e="T501" id="Seg_10688" s="T500">он(а)-EP-ACC</ta>
            <ta e="T502" id="Seg_10689" s="T501">еда-VBLZ-3SG.O</ta>
            <ta e="T503" id="Seg_10690" s="T502">сказать-3SG.S</ta>
            <ta e="T504" id="Seg_10691" s="T503">карта-VBLZ-OPT-1DU</ta>
            <ta e="T505" id="Seg_10692" s="T504">муж.[NOM]-3SG</ta>
            <ta e="T506" id="Seg_10693" s="T505">сказать-3SG.S</ta>
            <ta e="T507" id="Seg_10694" s="T506">я.NOM</ta>
            <ta e="T508" id="Seg_10695" s="T507">устать-RFL-PST.NAR-1SG.S</ta>
            <ta e="T509" id="Seg_10696" s="T508">жена.[NOM]-3SG</ta>
            <ta e="T510" id="Seg_10697" s="T509">сказать-3SG.S</ta>
            <ta e="T511" id="Seg_10698" s="T510">карта-VBLZ-OPT-1DU</ta>
            <ta e="T514" id="Seg_10699" s="T513">кто.[NOM]</ta>
            <ta e="T515" id="Seg_10700" s="T514">кто.[NOM]</ta>
            <ta e="T516" id="Seg_10701" s="T515">дурак-EP-ADVZ</ta>
            <ta e="T517" id="Seg_10702" s="T516">остаться-FUT-3SG.S</ta>
            <ta e="T518" id="Seg_10703" s="T517">рука-EP-ACC-3SG</ta>
            <ta e="T519" id="Seg_10704" s="T518">назад</ta>
            <ta e="T520" id="Seg_10705" s="T519">привязать-INF</ta>
            <ta e="T521" id="Seg_10706" s="T520">если</ta>
            <ta e="T522" id="Seg_10707" s="T521">я.NOM</ta>
            <ta e="T523" id="Seg_10708" s="T522">остаться-1SG.S</ta>
            <ta e="T524" id="Seg_10709" s="T523">дурак-EP-ADVZ</ta>
            <ta e="T525" id="Seg_10710" s="T524">я.NOM</ta>
            <ta e="T526" id="Seg_10711" s="T525">рука.[NOM]-EP-1SG</ta>
            <ta e="T527" id="Seg_10712" s="T526">привязать-IMP.2SG.S</ta>
            <ta e="T528" id="Seg_10713" s="T527">привязать-FUT-2SG.O</ta>
            <ta e="T529" id="Seg_10714" s="T528">он(а)-EP-DU.[NOM]</ta>
            <ta e="T530" id="Seg_10715" s="T529">сесть-3DU.S</ta>
            <ta e="T531" id="Seg_10716" s="T530">карта-VBLZ-CVB</ta>
            <ta e="T532" id="Seg_10717" s="T531">муж.[NOM]-3SG</ta>
            <ta e="T533" id="Seg_10718" s="T532">дурак-EP-ADVZ</ta>
            <ta e="T534" id="Seg_10719" s="T533">остаться-3SG.S</ta>
            <ta e="T535" id="Seg_10720" s="T534">ну</ta>
            <ta e="T536" id="Seg_10721" s="T535">теперь</ta>
            <ta e="T537" id="Seg_10722" s="T536">рука-ACC-OBL.2SG</ta>
            <ta e="T538" id="Seg_10723" s="T537">привязать-FUT-1SG.O</ta>
            <ta e="T539" id="Seg_10724" s="T538">он(а).[NOM]</ta>
            <ta e="T540" id="Seg_10725" s="T539">привязать-3SG.O</ta>
            <ta e="T541" id="Seg_10726" s="T540">рука-EP-ACC-3SG</ta>
            <ta e="T542" id="Seg_10727" s="T541">он(а).[NOM]</ta>
            <ta e="T543" id="Seg_10728" s="T542">как</ta>
            <ta e="T544" id="Seg_10729" s="T543">%%-PFV-INFER.[3SG.S]</ta>
            <ta e="T545" id="Seg_10730" s="T544">веревка.[NOM]</ta>
            <ta e="T546" id="Seg_10731" s="T545">порвать-RFL-3SG.S</ta>
            <ta e="T547" id="Seg_10732" s="T546">лечь.спать-CO-3DU.S</ta>
            <ta e="T548" id="Seg_10733" s="T547">спать-CO</ta>
            <ta e="T549" id="Seg_10734" s="T548">утро-нечто-LOC</ta>
            <ta e="T550" id="Seg_10735" s="T549">встать-3DU.S</ta>
            <ta e="T551" id="Seg_10736" s="T550">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T552" id="Seg_10737" s="T551">муж-ACC-3SG</ta>
            <ta e="T553" id="Seg_10738" s="T552">еда-VBLZ-3SG.O</ta>
            <ta e="T554" id="Seg_10739" s="T553">муж.[NOM]-3SG</ta>
            <ta e="T555" id="Seg_10740" s="T554">съесть-FRQ-CO-3SG.S</ta>
            <ta e="T556" id="Seg_10741" s="T555">и</ta>
            <ta e="T557" id="Seg_10742" s="T556">тайга-EP-ILL</ta>
            <ta e="T558" id="Seg_10743" s="T557">ну</ta>
            <ta e="T559" id="Seg_10744" s="T558">отправиться-INFER.[3SG.S]</ta>
            <ta e="T560" id="Seg_10745" s="T559">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T561" id="Seg_10746" s="T560">наружу</ta>
            <ta e="T562" id="Seg_10747" s="T561">ходить-3SG.S</ta>
            <ta e="T563" id="Seg_10748" s="T562">и</ta>
            <ta e="T564" id="Seg_10749" s="T563">ну</ta>
            <ta e="T565" id="Seg_10750" s="T564">кричать-MOM-EP-GEN</ta>
            <ta e="T566" id="Seg_10751" s="T565">прийти-IMP.2SG.S</ta>
            <ta e="T567" id="Seg_10752" s="T566">муж.[NOM]-1SG</ta>
            <ta e="T568" id="Seg_10753" s="T567">отправиться-PST.NAR.[3SG.S]</ta>
            <ta e="T569" id="Seg_10754" s="T568">разбойник.[NOM]</ta>
            <ta e="T570" id="Seg_10755" s="T569">прийти-CO.[3SG.S]</ta>
            <ta e="T571" id="Seg_10756" s="T570">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T572" id="Seg_10757" s="T571">сказать-3SG.S</ta>
            <ta e="T573" id="Seg_10758" s="T572">веревка-PL-ACC</ta>
            <ta e="T574" id="Seg_10759" s="T573">муж.[NOM]-1SG</ta>
            <ta e="T575" id="Seg_10760" s="T574">все</ta>
            <ta e="T576" id="Seg_10761" s="T575">порвать-PFV-3SG.O</ta>
            <ta e="T577" id="Seg_10762" s="T576">он(а).[NOM]</ta>
            <ta e="T578" id="Seg_10763" s="T577">сила-COM</ta>
            <ta e="T579" id="Seg_10764" s="T578">быть-3SG.S</ta>
            <ta e="T580" id="Seg_10765" s="T579">а</ta>
            <ta e="T581" id="Seg_10766" s="T580">я.NOM</ta>
            <ta e="T582" id="Seg_10767" s="T581">этот-день.[NOM]</ta>
            <ta e="T583" id="Seg_10768" s="T582">ты.ALL</ta>
            <ta e="T584" id="Seg_10769" s="T583">дать-FUT-1SG.O</ta>
            <ta e="T585" id="Seg_10770" s="T584">веревка.[NOM]</ta>
            <ta e="T586" id="Seg_10771" s="T585">волос.[NOM]</ta>
            <ta e="T587" id="Seg_10772" s="T586">и</ta>
            <ta e="T590" id="Seg_10773" s="T589">один-нечто-ADV.LOC</ta>
            <ta e="T591" id="Seg_10774" s="T590">свить-RES.[3SG.S]</ta>
            <ta e="T592" id="Seg_10775" s="T591">вечер-нечто-LOC</ta>
            <ta e="T593" id="Seg_10776" s="T592">ну</ta>
            <ta e="T594" id="Seg_10777" s="T593">прийти-POT.FUT.3SG</ta>
            <ta e="T595" id="Seg_10778" s="T594">ты.NOM</ta>
            <ta e="T596" id="Seg_10779" s="T595">опять</ta>
            <ta e="T597" id="Seg_10780" s="T596">сказать-IMP.2SG.S</ta>
            <ta e="T598" id="Seg_10781" s="T597">карта-VBLZ-INF</ta>
            <ta e="T599" id="Seg_10782" s="T598">карта-VBLZ-OPT-1DU</ta>
            <ta e="T600" id="Seg_10783" s="T599">кто.[NOM]</ta>
            <ta e="T601" id="Seg_10784" s="T600">дурак-EP-ADVZ</ta>
            <ta e="T602" id="Seg_10785" s="T601">остаться-FUT-3SG.S</ta>
            <ta e="T603" id="Seg_10786" s="T602">то</ta>
            <ta e="T604" id="Seg_10787" s="T603">рука-EP-ACC-3SG</ta>
            <ta e="T605" id="Seg_10788" s="T604">назад</ta>
            <ta e="T606" id="Seg_10789" s="T605">привязать-INF</ta>
            <ta e="T607" id="Seg_10790" s="T606">старик.[NOM]</ta>
            <ta e="T608" id="Seg_10791" s="T607">прийти-CO.[3SG.S]</ta>
            <ta e="T609" id="Seg_10792" s="T608">он(а).[NOM]</ta>
            <ta e="T610" id="Seg_10793" s="T609">он(а)-EP-ACC</ta>
            <ta e="T611" id="Seg_10794" s="T610">еда-VBLZ-3SG.O</ta>
            <ta e="T612" id="Seg_10795" s="T611">сказать-3SG.S</ta>
            <ta e="T613" id="Seg_10796" s="T612">карта-VBLZ-OPT-1DU</ta>
            <ta e="T614" id="Seg_10797" s="T613">муж.[NOM]-3SG</ta>
            <ta e="T615" id="Seg_10798" s="T614">сказать-3SG.S</ta>
            <ta e="T616" id="Seg_10799" s="T615">я.NOM</ta>
            <ta e="T617" id="Seg_10800" s="T616">устать-RFL-PST.NAR-1SG.S</ta>
            <ta e="T618" id="Seg_10801" s="T617">надо</ta>
            <ta e="T619" id="Seg_10802" s="T618">спать-INF</ta>
            <ta e="T620" id="Seg_10803" s="T619">ну</ta>
            <ta e="T621" id="Seg_10804" s="T620">карта-VBLZ-OPT-1DU</ta>
            <ta e="T622" id="Seg_10805" s="T621">карта-VBLZ-OPT-1DU</ta>
            <ta e="T623" id="Seg_10806" s="T622">хоть</ta>
            <ta e="T624" id="Seg_10807" s="T623">один-EP-ADV.LOC</ta>
            <ta e="T625" id="Seg_10808" s="T624">кто.[NOM]</ta>
            <ta e="T626" id="Seg_10809" s="T625">дурак-EP-ADVZ</ta>
            <ta e="T627" id="Seg_10810" s="T626">остаться-FUT-3SG.S</ta>
            <ta e="T628" id="Seg_10811" s="T627">рука-EP-ACC-3SG</ta>
            <ta e="T629" id="Seg_10812" s="T628">назад</ta>
            <ta e="T630" id="Seg_10813" s="T629">привязать-INF</ta>
            <ta e="T631" id="Seg_10814" s="T630">ежели</ta>
            <ta e="T632" id="Seg_10815" s="T631">я.NOM</ta>
            <ta e="T633" id="Seg_10816" s="T632">остаться-FUT-1SG.S</ta>
            <ta e="T634" id="Seg_10817" s="T633">я.GEN</ta>
            <ta e="T635" id="Seg_10818" s="T634">рука.[NOM]-EP-1SG</ta>
            <ta e="T636" id="Seg_10819" s="T635">привязать-IMP.2SG.O</ta>
            <ta e="T637" id="Seg_10820" s="T636">назад</ta>
            <ta e="T638" id="Seg_10821" s="T637">и</ta>
            <ta e="T639" id="Seg_10822" s="T638">сесть-3DU.S</ta>
            <ta e="T640" id="Seg_10823" s="T639">карта-VBLZ-CVB</ta>
            <ta e="T641" id="Seg_10824" s="T640">муж-ACC-3SG</ta>
            <ta e="T642" id="Seg_10825" s="T641">дурак-EP-TRL-CVB</ta>
            <ta e="T643" id="Seg_10826" s="T642">оставить-3SG.O</ta>
            <ta e="T644" id="Seg_10827" s="T643">рука-EP-ACC-3SG</ta>
            <ta e="T645" id="Seg_10828" s="T644">назад</ta>
            <ta e="T646" id="Seg_10829" s="T645">привязать-3SG.O</ta>
            <ta e="T647" id="Seg_10830" s="T646">он(а).[NOM]</ta>
            <ta e="T648" id="Seg_10831" s="T647">как</ta>
            <ta e="T649" id="Seg_10832" s="T648">%%-PFV-INFER.[3SG.S]</ta>
            <ta e="T650" id="Seg_10833" s="T649">веревка-PL-ACC</ta>
            <ta e="T651" id="Seg_10834" s="T650">NEG</ta>
            <ta e="T652" id="Seg_10835" s="T651">порвать-3SG.O</ta>
            <ta e="T653" id="Seg_10836" s="T652">жена-3SG-ALL</ta>
            <ta e="T654" id="Seg_10837" s="T653">сказать-3SG.S</ta>
            <ta e="T655" id="Seg_10838" s="T654">отвязать-IMP.2SG.O</ta>
            <ta e="T656" id="Seg_10839" s="T655">рука.[NOM]-EP-1SG</ta>
            <ta e="T657" id="Seg_10840" s="T656">старуха.[NOM]-3SG</ta>
            <ta e="T658" id="Seg_10841" s="T657">сказать-3SG.S</ta>
            <ta e="T659" id="Seg_10842" s="T658">порвать-IMP.2SG.O</ta>
            <ta e="T660" id="Seg_10843" s="T659">он(а).[NOM]</ta>
            <ta e="T661" id="Seg_10844" s="T660">опять</ta>
            <ta e="T662" id="Seg_10845" s="T661">%%-PFV-3SG.S</ta>
            <ta e="T663" id="Seg_10846" s="T662">NEG</ta>
            <ta e="T664" id="Seg_10847" s="T663">порвать-3SG.O</ta>
            <ta e="T665" id="Seg_10848" s="T664">сказать-3SG.S</ta>
            <ta e="T666" id="Seg_10849" s="T665">отвязать-IMP.2SG.O</ta>
            <ta e="T667" id="Seg_10850" s="T666">NEG</ta>
            <ta e="T668" id="Seg_10851" s="T667">отвязать-FUT-1SG.O</ta>
            <ta e="T669" id="Seg_10852" s="T668">наружу</ta>
            <ta e="T670" id="Seg_10853" s="T669">выскочить-CVB</ta>
            <ta e="T671" id="Seg_10854" s="T670">отправиться-CO-3SG.S</ta>
            <ta e="T672" id="Seg_10855" s="T671">и</ta>
            <ta e="T673" id="Seg_10856" s="T672">ну</ta>
            <ta e="T674" id="Seg_10857" s="T673">закричать-INFER.[3SG.S]</ta>
            <ta e="T675" id="Seg_10858" s="T674">прийти-IMP.2SG.S</ta>
            <ta e="T676" id="Seg_10859" s="T675">и</ta>
            <ta e="T677" id="Seg_10860" s="T676">этот</ta>
            <ta e="T678" id="Seg_10861" s="T677">разбойник.[NOM]</ta>
            <ta e="T679" id="Seg_10862" s="T678">ну</ta>
            <ta e="T680" id="Seg_10863" s="T679">прийти-INFER.[3SG.S]</ta>
            <ta e="T681" id="Seg_10864" s="T680">сказать-3SG.S</ta>
            <ta e="T682" id="Seg_10865" s="T681">муж.[NOM]-1SG</ta>
            <ta e="T683" id="Seg_10866" s="T682">веревка-ACC</ta>
            <ta e="T684" id="Seg_10867" s="T683">NEG</ta>
            <ta e="T685" id="Seg_10868" s="T684">порвать-3SG.O</ta>
            <ta e="T686" id="Seg_10869" s="T685">зайти-CVB</ta>
            <ta e="T687" id="Seg_10870" s="T686">убить-OPT-1DU</ta>
            <ta e="T688" id="Seg_10871" s="T687">и</ta>
            <ta e="T689" id="Seg_10872" s="T688">дом-ILL</ta>
            <ta e="T690" id="Seg_10873" s="T689">зайти-CO-3DU.S</ta>
            <ta e="T691" id="Seg_10874" s="T690">разбойник.[NOM]</ta>
            <ta e="T692" id="Seg_10875" s="T691">сказать-3SG.S</ta>
            <ta e="T693" id="Seg_10876" s="T692">утро-день-ADV.LOC</ta>
            <ta e="T694" id="Seg_10877" s="T693">утро-нечто-LOC</ta>
            <ta e="T695" id="Seg_10878" s="T694">убить-FUT-1DU</ta>
            <ta e="T696" id="Seg_10879" s="T695">внести-IMP.2SG.O</ta>
            <ta e="T697" id="Seg_10880" s="T696">ведро-GEN</ta>
            <ta e="T698" id="Seg_10881" s="T697">полнота.[NOM]</ta>
            <ta e="T699" id="Seg_10882" s="T698">соль.[NOM]</ta>
            <ta e="T700" id="Seg_10883" s="T699">он(а)-EP-ACC</ta>
            <ta e="T701" id="Seg_10884" s="T700">соль-EP-GEN</ta>
            <ta e="T702" id="Seg_10885" s="T701">верхняя.часть-EP-ILL</ta>
            <ta e="T703" id="Seg_10886" s="T702">колено-OPT-OBL.3SG-INSTR</ta>
            <ta e="T704" id="Seg_10887" s="T703">соль-EP-ILL</ta>
            <ta e="T705" id="Seg_10888" s="T704">сесть-TR-FUT-1DU</ta>
            <ta e="T706" id="Seg_10889" s="T705">утро-мы.DU-ILL</ta>
            <ta e="T707" id="Seg_10890" s="T706">сидеть.[3SG.S]</ta>
            <ta e="T708" id="Seg_10891" s="T707">утро-нечто-LOC</ta>
            <ta e="T709" id="Seg_10892" s="T708">убить-FUT-1DU</ta>
            <ta e="T710" id="Seg_10893" s="T709">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T711" id="Seg_10894" s="T710">лицо-LOC</ta>
            <ta e="T712" id="Seg_10895" s="T711">прийти-CVB</ta>
            <ta e="T713" id="Seg_10896" s="T712">ну</ta>
            <ta e="T714" id="Seg_10897" s="T713">плюнуть-INFER-3SG.O</ta>
            <ta e="T715" id="Seg_10898" s="T714">ты.NOM</ta>
            <ta e="T716" id="Seg_10899" s="T715">сказать-HAB-PST-2SG.S</ta>
            <ta e="T717" id="Seg_10900" s="T716">что</ta>
            <ta e="T718" id="Seg_10901" s="T717">я.NOM</ta>
            <ta e="T719" id="Seg_10902" s="T718">сила-COM</ta>
            <ta e="T720" id="Seg_10903" s="T719">быть-CO-1SG.S</ta>
            <ta e="T721" id="Seg_10904" s="T720">а</ta>
            <ta e="T722" id="Seg_10905" s="T721">что-TRL</ta>
            <ta e="T723" id="Seg_10906" s="T722">веревка-ACC</ta>
            <ta e="T724" id="Seg_10907" s="T723">NEG</ta>
            <ta e="T725" id="Seg_10908" s="T724">порвать-FUT-2SG.O</ta>
            <ta e="T726" id="Seg_10909" s="T725">теперь</ta>
            <ta e="T727" id="Seg_10910" s="T726">утро-нечто-LOC</ta>
            <ta e="T728" id="Seg_10911" s="T727">ты.ACC</ta>
            <ta e="T729" id="Seg_10912" s="T728">убить-FUT-1DU</ta>
            <ta e="T730" id="Seg_10913" s="T729">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T731" id="Seg_10914" s="T730">бегать-MOM-CO-3SG.S</ta>
            <ta e="T732" id="Seg_10915" s="T731">соль.[NOM]</ta>
            <ta e="T733" id="Seg_10916" s="T732">внести-INFER-3SG.O</ta>
            <ta e="T734" id="Seg_10917" s="T733">ведро-GEN</ta>
            <ta e="T735" id="Seg_10918" s="T734">полнота.[NOM]</ta>
            <ta e="T736" id="Seg_10919" s="T735">и</ta>
            <ta e="T737" id="Seg_10920" s="T736">пол-ILL</ta>
            <ta e="T738" id="Seg_10921" s="T737">насыпать-3SG.O</ta>
            <ta e="T739" id="Seg_10922" s="T738">и</ta>
            <ta e="T740" id="Seg_10923" s="T739">он(а)-EP-ACC</ta>
            <ta e="T741" id="Seg_10924" s="T740">колено-ADV.LOC</ta>
            <ta e="T742" id="Seg_10925" s="T741">сесть-TR-3SG.O</ta>
            <ta e="T743" id="Seg_10926" s="T742">сказать-3SG.S</ta>
            <ta e="T744" id="Seg_10927" s="T743">сидеть-IMP.2SG.S</ta>
            <ta e="T745" id="Seg_10928" s="T744">утро-нечто-ILL</ta>
            <ta e="T746" id="Seg_10929" s="T745">утро-нечто-LOC</ta>
            <ta e="T747" id="Seg_10930" s="T746">ты.ACC</ta>
            <ta e="T748" id="Seg_10931" s="T747">убить-FUT-1DU</ta>
            <ta e="T749" id="Seg_10932" s="T748">сесть-3DU.S</ta>
            <ta e="T750" id="Seg_10933" s="T749">съесть-FRQ-CVB</ta>
            <ta e="T751" id="Seg_10934" s="T750">разбойник-COM</ta>
            <ta e="T752" id="Seg_10935" s="T751">вино-ADJZ</ta>
            <ta e="T753" id="Seg_10936" s="T752">пить-CVB</ta>
            <ta e="T754" id="Seg_10937" s="T753">начать-DRV-EP-3DU.S</ta>
            <ta e="T755" id="Seg_10938" s="T754">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T756" id="Seg_10939" s="T755">прийти-CO.[3SG.S]</ta>
            <ta e="T757" id="Seg_10940" s="T756">муж-GEN-3SG</ta>
            <ta e="T758" id="Seg_10941" s="T757">бок-ILL</ta>
            <ta e="T759" id="Seg_10942" s="T758">муж-ACC-3SG</ta>
            <ta e="T760" id="Seg_10943" s="T759">лицо-EP-ILL</ta>
            <ta e="T761" id="Seg_10944" s="T760">ну</ta>
            <ta e="T762" id="Seg_10945" s="T761">плюнуть-INFER-3SG.O</ta>
            <ta e="T763" id="Seg_10946" s="T762">утро-нечто-LOC</ta>
            <ta e="T764" id="Seg_10947" s="T763">мы.[NOM]</ta>
            <ta e="T765" id="Seg_10948" s="T764">ты.ACC</ta>
            <ta e="T766" id="Seg_10949" s="T765">убить-FUT-1DU</ta>
            <ta e="T767" id="Seg_10950" s="T766">сам.3SG</ta>
            <ta e="T768" id="Seg_10951" s="T767">ну</ta>
            <ta e="T769" id="Seg_10952" s="T768">отправиться-INFER.[3SG.S]</ta>
            <ta e="T770" id="Seg_10953" s="T769">разбойник-EP-ACC</ta>
            <ta e="T771" id="Seg_10954" s="T770">обнять-HAB-3SG.O</ta>
            <ta e="T772" id="Seg_10955" s="T771">поцеловать-INCH-CVB</ta>
            <ta e="T773" id="Seg_10956" s="T772">принести-EP-FRQ-EP-3SG.O</ta>
            <ta e="T774" id="Seg_10957" s="T773">и</ta>
            <ta e="T775" id="Seg_10958" s="T774">спать-INF</ta>
            <ta e="T776" id="Seg_10959" s="T775">лечь.спать-CO-3DU.S</ta>
            <ta e="T777" id="Seg_10960" s="T776">и</ta>
            <ta e="T778" id="Seg_10961" s="T777">ну</ta>
            <ta e="T779" id="Seg_10962" s="T778">спать-PFV-3DU.S</ta>
            <ta e="T780" id="Seg_10963" s="T779">муж.[NOM]-3SG</ta>
            <ta e="T781" id="Seg_10964" s="T780">он(а).[NOM]</ta>
            <ta e="T782" id="Seg_10965" s="T781">встать-3SG.S</ta>
            <ta e="T783" id="Seg_10966" s="T782">наверх</ta>
            <ta e="T784" id="Seg_10967" s="T783">ходить-HAB-3SG.S</ta>
            <ta e="T785" id="Seg_10968" s="T784">а</ta>
            <ta e="T786" id="Seg_10969" s="T785">маленький-ADVZ</ta>
            <ta e="T787" id="Seg_10970" s="T786">дочь-DIM.[NOM]-3SG</ta>
            <ta e="T788" id="Seg_10971" s="T787">сын-DIM.[NOM]-3SG</ta>
            <ta e="T789" id="Seg_10972" s="T788">он(а)-EP-DU.[NOM]</ta>
            <ta e="T790" id="Seg_10973" s="T789">спать-PST-3DU.S</ta>
            <ta e="T791" id="Seg_10974" s="T790">полати-GEN</ta>
            <ta e="T792" id="Seg_10975" s="T791">верхняя.часть-EP-LOC</ta>
            <ta e="T793" id="Seg_10976" s="T792">дочь-DIM.[NOM]-3SG</ta>
            <ta e="T794" id="Seg_10977" s="T793">помочиться-INF</ta>
            <ta e="T795" id="Seg_10978" s="T794">вниз</ta>
            <ta e="T796" id="Seg_10979" s="T795">слезть-3SG.S</ta>
            <ta e="T797" id="Seg_10980" s="T796">он(а).[NOM]</ta>
            <ta e="T798" id="Seg_10981" s="T797">дочь-DIM-OBL.3SG-ALL</ta>
            <ta e="T799" id="Seg_10982" s="T798">сказать-3SG.S</ta>
            <ta e="T800" id="Seg_10983" s="T799">я.GEN</ta>
            <ta e="T801" id="Seg_10984" s="T800">рука-PL.[NOM]-1SG</ta>
            <ta e="T802" id="Seg_10985" s="T801">отвязать-OPT-2SG.O</ta>
            <ta e="T803" id="Seg_10986" s="T802">а</ta>
            <ta e="T804" id="Seg_10987" s="T803">дочь-DIM.[NOM]-3SG</ta>
            <ta e="T805" id="Seg_10988" s="T804">сказать-3SG.S</ta>
            <ta e="T806" id="Seg_10989" s="T805">ты.NOM</ta>
            <ta e="T807" id="Seg_10990" s="T806">сказать-HAB-PST-2SG.S</ta>
            <ta e="T808" id="Seg_10991" s="T807">что</ta>
            <ta e="T809" id="Seg_10992" s="T808">сила-COM</ta>
            <ta e="T810" id="Seg_10993" s="T809">быть-CO-1SG.S</ta>
            <ta e="T811" id="Seg_10994" s="T810">порвать-TR-IMP.2SG.O</ta>
            <ta e="T812" id="Seg_10995" s="T811">веревка-ACC</ta>
            <ta e="T813" id="Seg_10996" s="T812">полати-GEN</ta>
            <ta e="T814" id="Seg_10997" s="T813">верхняя.часть-EP-ILL</ta>
            <ta e="T815" id="Seg_10998" s="T814">ходить-3SG.S</ta>
            <ta e="T816" id="Seg_10999" s="T815">и</ta>
            <ta e="T817" id="Seg_11000" s="T816">спать.[3SG.S]</ta>
            <ta e="T818" id="Seg_11001" s="T817">сын-DIM.[NOM]-3SG</ta>
            <ta e="T819" id="Seg_11002" s="T818">полати-EL.3SG</ta>
            <ta e="T820" id="Seg_11003" s="T819">вниз</ta>
            <ta e="T821" id="Seg_11004" s="T820">слезть-3SG.S</ta>
            <ta e="T822" id="Seg_11005" s="T821">помочиться-INF</ta>
            <ta e="T823" id="Seg_11006" s="T822">он(а).[NOM]</ta>
            <ta e="T824" id="Seg_11007" s="T823">сын-DIM-3SG-ALL</ta>
            <ta e="T825" id="Seg_11008" s="T824">сказать-3SG.S</ta>
            <ta e="T826" id="Seg_11009" s="T825">я.GEN</ta>
            <ta e="T827" id="Seg_11010" s="T826">рука.[NOM]-EP-1SG</ta>
            <ta e="T828" id="Seg_11011" s="T827">отвязать-OPT-2SG.O</ta>
            <ta e="T829" id="Seg_11012" s="T828">он(а).[NOM]</ta>
            <ta e="T830" id="Seg_11013" s="T829">прийти-CO.[3SG.S]</ta>
            <ta e="T831" id="Seg_11014" s="T830">отец-3SG-ALL</ta>
            <ta e="T832" id="Seg_11015" s="T831">и</ta>
            <ta e="T833" id="Seg_11016" s="T832">отвязать-CVB</ta>
            <ta e="T834" id="Seg_11017" s="T833">начать-FRQ-EP-3SG.O</ta>
            <ta e="T835" id="Seg_11018" s="T834">NEG</ta>
            <ta e="T836" id="Seg_11019" s="T835">отвязать-3SG.O</ta>
            <ta e="T837" id="Seg_11020" s="T836">я.ALL</ta>
            <ta e="T838" id="Seg_11021" s="T837">NEG</ta>
            <ta e="T839" id="Seg_11022" s="T838">отвязать-INF</ta>
            <ta e="T840" id="Seg_11023" s="T839">отец.[NOM]-3SG</ta>
            <ta e="T841" id="Seg_11024" s="T840">сказать-3SG.S</ta>
            <ta e="T842" id="Seg_11025" s="T841">напильник</ta>
            <ta e="T843" id="Seg_11026" s="T842">принести-IMP.2SG.S</ta>
            <ta e="T844" id="Seg_11027" s="T843">он(а).[NOM]</ta>
            <ta e="T845" id="Seg_11028" s="T844">принести-CO-3SG.O</ta>
            <ta e="T846" id="Seg_11029" s="T845">и</ta>
            <ta e="T847" id="Seg_11030" s="T846">веревка-PL-ACC</ta>
            <ta e="T848" id="Seg_11031" s="T847">наточить-INCH-CVB</ta>
            <ta e="T849" id="Seg_11032" s="T848">ну</ta>
            <ta e="T850" id="Seg_11033" s="T849">начать-FRQ-EP-INFER-3SG.O</ta>
            <ta e="T851" id="Seg_11034" s="T850">наточить-3SG.O</ta>
            <ta e="T852" id="Seg_11035" s="T851">веревка-PL.[NOM]</ta>
            <ta e="T853" id="Seg_11036" s="T852">%%-DETR-CO-3PL</ta>
            <ta e="T854" id="Seg_11037" s="T853">он(а).[NOM]</ta>
            <ta e="T855" id="Seg_11038" s="T854">сын-DIM-3SG-ALL</ta>
            <ta e="T856" id="Seg_11039" s="T855">сказать-3SG.S</ta>
            <ta e="T857" id="Seg_11040" s="T856">отправиться-CVB</ta>
            <ta e="T858" id="Seg_11041" s="T857">спать-IMP.2SG.S</ta>
            <ta e="T859" id="Seg_11042" s="T858">утро-нечто-LOC</ta>
            <ta e="T860" id="Seg_11043" s="T859">NEG.IMP</ta>
            <ta e="T861" id="Seg_11044" s="T860">испугаться-DUR-IMP.2SG.S</ta>
            <ta e="T862" id="Seg_11045" s="T861">мы.[NOM]</ta>
            <ta e="T863" id="Seg_11046" s="T862">ты-COM</ta>
            <ta e="T864" id="Seg_11047" s="T863">сам.1DU</ta>
            <ta e="T865" id="Seg_11048" s="T864">жить-FUT-1DU</ta>
            <ta e="T866" id="Seg_11049" s="T865">небо.[NOM]</ta>
            <ta e="T867" id="Seg_11050" s="T866">день-EP-TRL-CVB</ta>
            <ta e="T868" id="Seg_11051" s="T867">начать-DRV-EP-3SG.S</ta>
            <ta e="T869" id="Seg_11052" s="T868">он(а).[NOM]</ta>
            <ta e="T870" id="Seg_11053" s="T869">веревка-PL-ACC</ta>
            <ta e="T871" id="Seg_11054" s="T870">рука-ILL.3SG</ta>
            <ta e="T872" id="Seg_11055" s="T871">положить-CO-3SG.O</ta>
            <ta e="T875" id="Seg_11056" s="T874">привязать-RES-INFER.[3SG.S]</ta>
            <ta e="T876" id="Seg_11057" s="T875">и</ta>
            <ta e="T877" id="Seg_11058" s="T876">сесть-3SG.S</ta>
            <ta e="T878" id="Seg_11059" s="T877">колено-GEN</ta>
            <ta e="T879" id="Seg_11060" s="T878">верхняя.часть-EP-ADV.LOC</ta>
            <ta e="T880" id="Seg_11061" s="T879">жена.[NOM]-3SG</ta>
            <ta e="T881" id="Seg_11062" s="T880">встать-3SG.S</ta>
            <ta e="T882" id="Seg_11063" s="T881">прийти-CVB</ta>
            <ta e="T883" id="Seg_11064" s="T882">лицо-EP-ADV.LOC</ta>
            <ta e="T884" id="Seg_11065" s="T883">плюнуть-3SG.O</ta>
            <ta e="T885" id="Seg_11066" s="T884">разбойник-ALL</ta>
            <ta e="T886" id="Seg_11067" s="T885">сказать-3SG.S</ta>
            <ta e="T887" id="Seg_11068" s="T886">ну</ta>
            <ta e="T888" id="Seg_11069" s="T887">убить-OPT-1DU</ta>
            <ta e="T889" id="Seg_11070" s="T888">а</ta>
            <ta e="T890" id="Seg_11071" s="T889">разбойник.[NOM]</ta>
            <ta e="T891" id="Seg_11072" s="T890">сказать-3SG.S</ta>
            <ta e="T892" id="Seg_11073" s="T891">съесть-FRQ-CVB</ta>
            <ta e="T893" id="Seg_11074" s="T892">отправиться-OPT-1DU-OPT</ta>
            <ta e="T894" id="Seg_11075" s="T893">он(а).[NOM]</ta>
            <ta e="T895" id="Seg_11076" s="T894">мясо-PL-ACC</ta>
            <ta e="T896" id="Seg_11077" s="T895">сварить-FRQ-DRV-CO-3SG.O</ta>
            <ta e="T897" id="Seg_11078" s="T896">съесть-FRQ-CO-3DU.S</ta>
            <ta e="T898" id="Seg_11079" s="T897">ну</ta>
            <ta e="T899" id="Seg_11080" s="T898">теперь</ta>
            <ta e="T900" id="Seg_11081" s="T899">убить-OPT-1DU</ta>
            <ta e="T901" id="Seg_11082" s="T900">дом-LOC</ta>
            <ta e="T902" id="Seg_11083" s="T901">убить-FUT-1DU</ta>
            <ta e="T903" id="Seg_11084" s="T902">али</ta>
            <ta e="T904" id="Seg_11085" s="T903">наружу-ADV.LOC</ta>
            <ta e="T905" id="Seg_11086" s="T904">разбойник.[NOM]</ta>
            <ta e="T906" id="Seg_11087" s="T905">сказать-3SG.S</ta>
            <ta e="T907" id="Seg_11088" s="T906">наружу-ADV.LOC</ta>
            <ta e="T908" id="Seg_11089" s="T907">убить-FUT-1DU</ta>
            <ta e="T909" id="Seg_11090" s="T908">ну</ta>
            <ta e="T910" id="Seg_11091" s="T909">встать-IMP.2SG.S</ta>
            <ta e="T911" id="Seg_11092" s="T910">ходить-IMP.2SG.S</ta>
            <ta e="T912" id="Seg_11093" s="T911">наружу</ta>
            <ta e="T913" id="Seg_11094" s="T912">ты.ACC</ta>
            <ta e="T914" id="Seg_11095" s="T913">убить-FUT-1DU</ta>
            <ta e="T915" id="Seg_11096" s="T914">что.[NOM]</ta>
            <ta e="T916" id="Seg_11097" s="T915">веревка-PL-ACC</ta>
            <ta e="T917" id="Seg_11098" s="T916">NEG</ta>
            <ta e="T918" id="Seg_11099" s="T917">порвать-TR-2SG.O</ta>
            <ta e="T919" id="Seg_11100" s="T918">ты.NOM</ta>
            <ta e="T920" id="Seg_11101" s="T919">сказать-HAB-PST-2SG.S</ta>
            <ta e="T921" id="Seg_11102" s="T920">сила-COM</ta>
            <ta e="T922" id="Seg_11103" s="T921">быть-CO-1SG.S</ta>
            <ta e="T923" id="Seg_11104" s="T922">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T924" id="Seg_11105" s="T923">как</ta>
            <ta e="T925" id="Seg_11106" s="T924">лицо-EP-ILL</ta>
            <ta e="T926" id="Seg_11107" s="T925">плюнуть-INFER-3SG.O</ta>
            <ta e="T927" id="Seg_11108" s="T926">он(а).[NOM]</ta>
            <ta e="T928" id="Seg_11109" s="T927">как</ta>
            <ta e="T929" id="Seg_11110" s="T928">наверх</ta>
            <ta e="T930" id="Seg_11111" s="T929">встать-DRV-INFER.[3SG.S]</ta>
            <ta e="T931" id="Seg_11112" s="T930">как</ta>
            <ta e="T932" id="Seg_11113" s="T931">разбойник-EP-ACC</ta>
            <ta e="T933" id="Seg_11114" s="T932">побить-INFER-3SG.O</ta>
            <ta e="T934" id="Seg_11115" s="T933">разбойник.[NOM]</ta>
            <ta e="T935" id="Seg_11116" s="T934">навзничь</ta>
            <ta e="T936" id="Seg_11117" s="T935">упасть-PFV-3SG.S</ta>
            <ta e="T937" id="Seg_11118" s="T936">упасть-RFL-3SG.S</ta>
            <ta e="T938" id="Seg_11119" s="T937">и</ta>
            <ta e="T939" id="Seg_11120" s="T938">убить-CO-3SG.O</ta>
            <ta e="T940" id="Seg_11121" s="T939">а</ta>
            <ta e="T941" id="Seg_11122" s="T940">жена.[NOM]-3SG</ta>
            <ta e="T942" id="Seg_11123" s="T941">шея-EP-ILL.3SG</ta>
            <ta e="T943" id="Seg_11124" s="T942">повесить-EP-RFL-3SG.S</ta>
            <ta e="T944" id="Seg_11125" s="T943">сказать-3SG.S</ta>
            <ta e="T945" id="Seg_11126" s="T944">я.NOM</ta>
            <ta e="T946" id="Seg_11127" s="T945">такой-ADVZ</ta>
            <ta e="T947" id="Seg_11128" s="T946">NEG</ta>
            <ta e="T948" id="Seg_11129" s="T947">сделать-HAB-FUT-1SG.O</ta>
            <ta e="T949" id="Seg_11130" s="T948">нога-PL-ACC-OBL.2SG</ta>
            <ta e="T950" id="Seg_11131" s="T949">вымыть-HAB-FUT-1SG.O</ta>
            <ta e="T951" id="Seg_11132" s="T950">этот</ta>
            <ta e="T952" id="Seg_11133" s="T951">вода-EP-ACC</ta>
            <ta e="T953" id="Seg_11134" s="T952">пить-HAB-FUT-1SG.O</ta>
            <ta e="T954" id="Seg_11135" s="T953">муж.[NOM]-3SG</ta>
            <ta e="T955" id="Seg_11136" s="T954">как</ta>
            <ta e="T956" id="Seg_11137" s="T955">побить-PFV-3SG.O</ta>
            <ta e="T957" id="Seg_11138" s="T956">и</ta>
            <ta e="T958" id="Seg_11139" s="T957">убить-CO-3SG.O</ta>
            <ta e="T959" id="Seg_11140" s="T958">ребенок-PL.[NOM]</ta>
            <ta e="T960" id="Seg_11141" s="T959">плакать-CVB</ta>
            <ta e="T961" id="Seg_11142" s="T960">начать-DRV-EP-3PL</ta>
            <ta e="T962" id="Seg_11143" s="T961">дочь-DIM-ACC-3SG</ta>
            <ta e="T963" id="Seg_11144" s="T962">как</ta>
            <ta e="T964" id="Seg_11145" s="T963">побить-INFER-3SG.O</ta>
            <ta e="T965" id="Seg_11146" s="T964">и</ta>
            <ta e="T966" id="Seg_11147" s="T965">убить-CO-3SG.O</ta>
            <ta e="T967" id="Seg_11148" s="T966">и</ta>
            <ta e="T968" id="Seg_11149" s="T967">сын-DIM-3SG-ALL</ta>
            <ta e="T969" id="Seg_11150" s="T968">сказать-3SG.S</ta>
            <ta e="T970" id="Seg_11151" s="T969">мы.[NOM]</ta>
            <ta e="T971" id="Seg_11152" s="T970">ты-COM</ta>
            <ta e="T972" id="Seg_11153" s="T971">жить-FUT-1DU</ta>
            <ta e="T973" id="Seg_11154" s="T972">NEG.IMP</ta>
            <ta e="T974" id="Seg_11155" s="T973">плакать-IMP.2SG.S</ta>
            <ta e="T975" id="Seg_11156" s="T974">отправиться-CO-3SG.S</ta>
            <ta e="T976" id="Seg_11157" s="T975">земля-ADJZ</ta>
            <ta e="T977" id="Seg_11158" s="T976">выкопать-IPFV-CVB</ta>
            <ta e="T978" id="Seg_11159" s="T977">земля-ADJZ</ta>
            <ta e="T979" id="Seg_11160" s="T978">яма-EP-ACC</ta>
            <ta e="T980" id="Seg_11161" s="T979">выкопать-CO-3SG.O</ta>
            <ta e="T981" id="Seg_11162" s="T980">он(а)-PL-ACC</ta>
            <ta e="T982" id="Seg_11163" s="T981">туда-ADV.LOC</ta>
            <ta e="T983" id="Seg_11164" s="T982">раскидать-DRV-EP-3SG.O</ta>
            <ta e="T984" id="Seg_11165" s="T983">раскидать-CO-3SG.O</ta>
            <ta e="T985" id="Seg_11166" s="T984">и</ta>
            <ta e="T986" id="Seg_11167" s="T985">земля-INSTR</ta>
            <ta e="T987" id="Seg_11168" s="T986">зарыть-DRV-CO-3SG.O</ta>
            <ta e="T988" id="Seg_11169" s="T987">лодка-EP-ACC-3SG</ta>
            <ta e="T989" id="Seg_11170" s="T988">к.воде</ta>
            <ta e="T990" id="Seg_11171" s="T989">тащить-DRV-CVB</ta>
            <ta e="T991" id="Seg_11172" s="T990">отправиться-TR-3SG.O</ta>
            <ta e="T992" id="Seg_11173" s="T991">вещь-PL-ACC-3SG</ta>
            <ta e="T993" id="Seg_11174" s="T992">все</ta>
            <ta e="T994" id="Seg_11175" s="T993">вытащить-CO-3SG.O</ta>
            <ta e="T995" id="Seg_11176" s="T994">сын-DIM-OBL.3SG-COM</ta>
            <ta e="T996" id="Seg_11177" s="T995">сесть-3DU.S</ta>
            <ta e="T997" id="Seg_11178" s="T996">и</ta>
            <ta e="T998" id="Seg_11179" s="T997">ну</ta>
            <ta e="T999" id="Seg_11180" s="T998">отправиться-INFER-3DU.S</ta>
            <ta e="T1000" id="Seg_11181" s="T999">прийти-CVB</ta>
            <ta e="T1001" id="Seg_11182" s="T1000">достичь-3DU.S</ta>
            <ta e="T1002" id="Seg_11183" s="T1001">свой.3SG</ta>
            <ta e="T1003" id="Seg_11184" s="T1002">деревня-ILL.3SG</ta>
            <ta e="T1004" id="Seg_11185" s="T1003">человек-PL.[NOM]-3SG</ta>
            <ta e="T1005" id="Seg_11186" s="T1004">обрадоваться-DUR-3PL</ta>
            <ta e="T1006" id="Seg_11187" s="T1005">а</ta>
            <ta e="T1007" id="Seg_11188" s="T1006">жена.[NOM]-2SG</ta>
            <ta e="T1008" id="Seg_11189" s="T1007">куда</ta>
            <ta e="T1009" id="Seg_11190" s="T1008">быть-3SG.S</ta>
            <ta e="T1010" id="Seg_11191" s="T1009">жена.[NOM]-1SG</ta>
            <ta e="T1011" id="Seg_11192" s="T1010">умереть-PST.NAR.[3SG.S]</ta>
            <ta e="T1012" id="Seg_11193" s="T1011">все</ta>
            <ta e="T1013" id="Seg_11194" s="T1012">вытащить-CO-3PL</ta>
            <ta e="T1014" id="Seg_11195" s="T1013">вещь-ACC</ta>
            <ta e="T1015" id="Seg_11196" s="T1014">он(а).[NOM]</ta>
            <ta e="T1016" id="Seg_11197" s="T1015">себя-ALL.3SG</ta>
            <ta e="T1017" id="Seg_11198" s="T1016">дом.[NOM]</ta>
            <ta e="T1018" id="Seg_11199" s="T1017">купить-INFER-3SG.O</ta>
            <ta e="T1019" id="Seg_11200" s="T1018">и</ta>
            <ta e="T1020" id="Seg_11201" s="T1019">жениться-3SG.S</ta>
            <ta e="T1021" id="Seg_11202" s="T1020">и</ta>
            <ta e="T1022" id="Seg_11203" s="T1021">жить-CVB</ta>
            <ta e="T1023" id="Seg_11204" s="T1022">ну</ta>
            <ta e="T1024" id="Seg_11205" s="T1023">начать-DRV-EP-INFER.[3SG.S]</ta>
            <ta e="T1025" id="Seg_11206" s="T1024">я.NOM</ta>
            <ta e="T1026" id="Seg_11207" s="T1025">в.прошлом.году</ta>
            <ta e="T1027" id="Seg_11208" s="T1026">он(а)-ADES</ta>
            <ta e="T1028" id="Seg_11209" s="T1027">быть-PST-1SG.S</ta>
            <ta e="T1029" id="Seg_11210" s="T1028">до.того</ta>
            <ta e="T1030" id="Seg_11211" s="T1029">хороший-ADVZ</ta>
            <ta e="T1031" id="Seg_11212" s="T1030">жить.[3SG.S]</ta>
            <ta e="T1032" id="Seg_11213" s="T1031">жена.[NOM]-3SG</ta>
            <ta e="T1033" id="Seg_11214" s="T1032">хороший</ta>
            <ta e="T1034" id="Seg_11215" s="T1033">я.ACC</ta>
            <ta e="T1035" id="Seg_11216" s="T1034">чай-PL-INSTR</ta>
            <ta e="T1036" id="Seg_11217" s="T1035">я.ACC</ta>
            <ta e="T1037" id="Seg_11218" s="T1036">пить-TR-PST.[3SG.S]</ta>
            <ta e="T1038" id="Seg_11219" s="T1037">я.ALL</ta>
            <ta e="T1039" id="Seg_11220" s="T1038">сказать-3SG.S</ta>
            <ta e="T1040" id="Seg_11221" s="T1039">мы-ALL.1PL</ta>
            <ta e="T1041" id="Seg_11222" s="T1040">ходить-HAB-IMP.2SG.S</ta>
            <ta e="T1042" id="Seg_11223" s="T1041">я.NOM</ta>
            <ta e="T1043" id="Seg_11224" s="T1042">он(а)-PL-ALL</ta>
            <ta e="T1044" id="Seg_11225" s="T1043">все.время</ta>
            <ta e="T1045" id="Seg_11226" s="T1044">заехать-EP-HAB-CO-1SG.S</ta>
            <ta e="T1046" id="Seg_11227" s="T1045">этот-год-ADV.LOC</ta>
            <ta e="T1047" id="Seg_11228" s="T1046">деревня-ILL</ta>
            <ta e="T1048" id="Seg_11229" s="T1047">отправиться-FUT-1SG.S</ta>
            <ta e="T1049" id="Seg_11230" s="T1048">он(а)-PL-ALL</ta>
            <ta e="T1050" id="Seg_11231" s="T1049">опять</ta>
            <ta e="T1051" id="Seg_11232" s="T1050">зайти-HAB-FUT-1SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_11233" s="T1">nprop.[n:case]</ta>
            <ta e="T3" id="Seg_11234" s="T2">n.[n:case]</ta>
            <ta e="T4" id="Seg_11235" s="T3">v-v:mood-v:pn</ta>
            <ta e="T5" id="Seg_11236" s="T4">nprop.[n:case]</ta>
            <ta e="T6" id="Seg_11237" s="T5">n.[n:case]</ta>
            <ta e="T7" id="Seg_11238" s="T6">pers-n:case</ta>
            <ta e="T8" id="Seg_11239" s="T7">num</ta>
            <ta e="T9" id="Seg_11240" s="T8">n.[n:case]-n:poss</ta>
            <ta e="T10" id="Seg_11241" s="T9">n-n:num.[n:case]-n:poss</ta>
            <ta e="T11" id="Seg_11242" s="T10">quant</ta>
            <ta e="T12" id="Seg_11243" s="T11">v-v&gt;v-v:pn</ta>
            <ta e="T13" id="Seg_11244" s="T12">num</ta>
            <ta e="T14" id="Seg_11245" s="T13">n.[n:case]-n:poss</ta>
            <ta e="T15" id="Seg_11246" s="T14">adv</ta>
            <ta e="T16" id="Seg_11247" s="T15">n-n&gt;v-v&gt;adv</ta>
            <ta e="T17" id="Seg_11248" s="T16">v-v&gt;v-v:pn</ta>
            <ta e="T18" id="Seg_11249" s="T17">adj</ta>
            <ta e="T19" id="Seg_11250" s="T18">n.[n:case]-n:poss</ta>
            <ta e="T20" id="Seg_11251" s="T19">conj</ta>
            <ta e="T21" id="Seg_11252" s="T20">num</ta>
            <ta e="T22" id="Seg_11253" s="T21">num</ta>
            <ta e="T23" id="Seg_11254" s="T22">n.[n:case]-n:poss</ta>
            <ta e="T24" id="Seg_11255" s="T23">n-n:num-n:case</ta>
            <ta e="T25" id="Seg_11256" s="T24">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T26" id="Seg_11257" s="T25">n-n:num.[n:case]</ta>
            <ta e="T27" id="Seg_11258" s="T26">v-v:mood-v:pn</ta>
            <ta e="T28" id="Seg_11259" s="T27">n-n:num.[n:case]</ta>
            <ta e="T29" id="Seg_11260" s="T28">conj</ta>
            <ta e="T30" id="Seg_11261" s="T29">n-n:num.[n:case]</ta>
            <ta e="T31" id="Seg_11262" s="T30">conj</ta>
            <ta e="T32" id="Seg_11263" s="T31">n.[n:case]</ta>
            <ta e="T33" id="Seg_11264" s="T32">v-v:mood-v:pn</ta>
            <ta e="T34" id="Seg_11265" s="T33">n.[n:case]-n:poss</ta>
            <ta e="T35" id="Seg_11266" s="T34">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T36" id="Seg_11267" s="T35">conj</ta>
            <ta e="T37" id="Seg_11268" s="T36">adj</ta>
            <ta e="T38" id="Seg_11269" s="T37">n.[n:case]-n:poss</ta>
            <ta e="T39" id="Seg_11270" s="T38">adv</ta>
            <ta e="T40" id="Seg_11271" s="T39">n-n:ins-n:case</ta>
            <ta e="T41" id="Seg_11272" s="T40">v-v:pn</ta>
            <ta e="T42" id="Seg_11273" s="T41">v-v&gt;v-v:pn</ta>
            <ta e="T43" id="Seg_11274" s="T42">pers-n:case</ta>
            <ta e="T44" id="Seg_11275" s="T43">n.[n:case]-n:poss</ta>
            <ta e="T45" id="Seg_11276" s="T44">v-v:mood.[v:pn]</ta>
            <ta e="T46" id="Seg_11277" s="T45">conj</ta>
            <ta e="T47" id="Seg_11278" s="T46">n-n&gt;n.[n:case]</ta>
            <ta e="T48" id="Seg_11279" s="T47">v-v:mood.[v:pn]</ta>
            <ta e="T49" id="Seg_11280" s="T48">pers-n:ins-n:num.[n:case]</ta>
            <ta e="T50" id="Seg_11281" s="T49">adj-adj&gt;adv</ta>
            <ta e="T51" id="Seg_11282" s="T50">v-v:ins-v:pn</ta>
            <ta e="T52" id="Seg_11283" s="T51">v-v:ins-v:pn</ta>
            <ta e="T53" id="Seg_11284" s="T52">conj</ta>
            <ta e="T54" id="Seg_11285" s="T53">pers.[n:case]</ta>
            <ta e="T55" id="Seg_11286" s="T54">adv-adv:case</ta>
            <ta e="T56" id="Seg_11287" s="T55">n-n:ins-n:case</ta>
            <ta e="T57" id="Seg_11288" s="T56">adj</ta>
            <ta e="T58" id="Seg_11289" s="T57">n.[n:case]</ta>
            <ta e="T59" id="Seg_11290" s="T58">v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T60" id="Seg_11291" s="T59">adj</ta>
            <ta e="T61" id="Seg_11292" s="T60">n.[n:case]</ta>
            <ta e="T62" id="Seg_11293" s="T61">n-n:obl.poss-n:case</ta>
            <ta e="T63" id="Seg_11294" s="T62">n-n:obl.poss-n:case</ta>
            <ta e="T64" id="Seg_11295" s="T63">v-v:pn</ta>
            <ta e="T65" id="Seg_11296" s="T64">n-n:case</ta>
            <ta e="T66" id="Seg_11297" s="T65">pers</ta>
            <ta e="T67" id="Seg_11298" s="T66">v-v:mood.pn</ta>
            <ta e="T68" id="Seg_11299" s="T67">n.[n:case]-n:poss</ta>
            <ta e="T69" id="Seg_11300" s="T68">n-n:ins-n:case</ta>
            <ta e="T70" id="Seg_11301" s="T69">v-v:ins-v:pn</ta>
            <ta e="T71" id="Seg_11302" s="T70">conj</ta>
            <ta e="T72" id="Seg_11303" s="T71">v-v:ins-v:pn</ta>
            <ta e="T73" id="Seg_11304" s="T72">pers</ta>
            <ta e="T74" id="Seg_11305" s="T73">v-v:tense-v:pn</ta>
            <ta e="T75" id="Seg_11306" s="T74">num</ta>
            <ta e="T76" id="Seg_11307" s="T75">n-n&gt;n-n:num.[n:case]-n:poss</ta>
            <ta e="T77" id="Seg_11308" s="T76">num</ta>
            <ta e="T78" id="Seg_11309" s="T77">n-n:case</ta>
            <ta e="T79" id="Seg_11310" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_11311" s="T79">v-v:tense-v:pn</ta>
            <ta e="T81" id="Seg_11312" s="T80">pers-n:case</ta>
            <ta e="T82" id="Seg_11313" s="T81">adv</ta>
            <ta e="T83" id="Seg_11314" s="T82">n.[n:case]-n:poss</ta>
            <ta e="T84" id="Seg_11315" s="T83">n-n:case</ta>
            <ta e="T85" id="Seg_11316" s="T84">quant</ta>
            <ta e="T86" id="Seg_11317" s="T85">n-n:case-n:poss</ta>
            <ta e="T87" id="Seg_11318" s="T86">v-v:ins-v:pn</ta>
            <ta e="T88" id="Seg_11319" s="T87">conj</ta>
            <ta e="T89" id="Seg_11320" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_11321" s="T89">v-v:mood-v:pn</ta>
            <ta e="T91" id="Seg_11322" s="T90">n.[n:case]-n:poss</ta>
            <ta e="T92" id="Seg_11323" s="T91">n.[n:case]-n:poss</ta>
            <ta e="T93" id="Seg_11324" s="T92">conj</ta>
            <ta e="T94" id="Seg_11325" s="T93">v-v&gt;adv</ta>
            <ta e="T95" id="Seg_11326" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_11327" s="T95">v-v:mood-v:pn</ta>
            <ta e="T97" id="Seg_11328" s="T96">conj</ta>
            <ta e="T98" id="Seg_11329" s="T97">pers-n:num.[n:case]</ta>
            <ta e="T99" id="Seg_11330" s="T98">conj</ta>
            <ta e="T100" id="Seg_11331" s="T99">ptcl</ta>
            <ta e="T101" id="Seg_11332" s="T100">v-v:mood-v:pn</ta>
            <ta e="T102" id="Seg_11333" s="T101">ptcl</ta>
            <ta e="T103" id="Seg_11334" s="T102">conj</ta>
            <ta e="T104" id="Seg_11335" s="T103">v-v&gt;adv</ta>
            <ta e="T105" id="Seg_11336" s="T104">ptcl</ta>
            <ta e="T106" id="Seg_11337" s="T105">v-v:mood-v:pn</ta>
            <ta e="T107" id="Seg_11338" s="T106">conj</ta>
            <ta e="T108" id="Seg_11339" s="T107">pers-n:case</ta>
            <ta e="T109" id="Seg_11340" s="T108">adv-adv:case</ta>
            <ta e="T110" id="Seg_11341" s="T109">adj</ta>
            <ta e="T111" id="Seg_11342" s="T110">n.[n:case]</ta>
            <ta e="T112" id="Seg_11343" s="T111">v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T113" id="Seg_11344" s="T112">quant</ta>
            <ta e="T114" id="Seg_11345" s="T113">v-v:ins-v:pn</ta>
            <ta e="T115" id="Seg_11346" s="T114">adv-n:case</ta>
            <ta e="T116" id="Seg_11347" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_11348" s="T116">conj</ta>
            <ta e="T118" id="Seg_11349" s="T117">n.[n:case]-n:poss</ta>
            <ta e="T119" id="Seg_11350" s="T118">quant</ta>
            <ta e="T120" id="Seg_11351" s="T119">v-v&gt;adv</ta>
            <ta e="T121" id="Seg_11352" s="T120">v-v:ins-v&gt;v-v:ins-v:pn</ta>
            <ta e="T122" id="Seg_11353" s="T121">n.[n:case]</ta>
            <ta e="T123" id="Seg_11354" s="T122">v-v:mood-v:pn</ta>
            <ta e="T124" id="Seg_11355" s="T123">n-n-adv:case</ta>
            <ta e="T125" id="Seg_11356" s="T124">n-n:ins-n:case</ta>
            <ta e="T126" id="Seg_11357" s="T125">v-v:tense-v:pn</ta>
            <ta e="T127" id="Seg_11358" s="T126">conj</ta>
            <ta e="T128" id="Seg_11359" s="T127">pers</ta>
            <ta e="T129" id="Seg_11360" s="T128">n-n:case</ta>
            <ta e="T130" id="Seg_11361" s="T129">ptcl</ta>
            <ta e="T131" id="Seg_11362" s="T130">v-v:tense</ta>
            <ta e="T132" id="Seg_11363" s="T131">n-n:case.poss</ta>
            <ta e="T133" id="Seg_11364" s="T132">interrog-clit</ta>
            <ta e="T134" id="Seg_11365" s="T133">n.[n:case]</ta>
            <ta e="T135" id="Seg_11366" s="T134">ptcl-adj&gt;adv</ta>
            <ta e="T136" id="Seg_11367" s="T135">ptcl</ta>
            <ta e="T137" id="Seg_11368" s="T136">v.[v:pn]</ta>
            <ta e="T138" id="Seg_11369" s="T137">conj</ta>
            <ta e="T139" id="Seg_11370" s="T138">adj-adj&gt;adv</ta>
            <ta e="T140" id="Seg_11371" s="T139">v-v:tense-v:pn</ta>
            <ta e="T141" id="Seg_11372" s="T140">ptcl</ta>
            <ta e="T142" id="Seg_11373" s="T141">v-v:pn</ta>
            <ta e="T143" id="Seg_11374" s="T142">n-n-n:case</ta>
            <ta e="T144" id="Seg_11375" s="T143">v-v:pn</ta>
            <ta e="T145" id="Seg_11376" s="T144">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T146" id="Seg_11377" s="T145">conj</ta>
            <ta e="T147" id="Seg_11378" s="T146">ptcl</ta>
            <ta e="T148" id="Seg_11379" s="T147">v-v:mood.[v:pn]</ta>
            <ta e="T149" id="Seg_11380" s="T148">n-n:ins-n:case</ta>
            <ta e="T150" id="Seg_11381" s="T149">conj</ta>
            <ta e="T151" id="Seg_11382" s="T150">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T152" id="Seg_11383" s="T151">v-v:pn</ta>
            <ta e="T153" id="Seg_11384" s="T152">interrog-n:case</ta>
            <ta e="T154" id="Seg_11385" s="T153">pers</ta>
            <ta e="T155" id="Seg_11386" s="T154">n.[n:case]-n:poss</ta>
            <ta e="T156" id="Seg_11387" s="T155">v-v:pn</ta>
            <ta e="T157" id="Seg_11388" s="T156">conj</ta>
            <ta e="T158" id="Seg_11389" s="T157">n.[n:case]</ta>
            <ta e="T159" id="Seg_11390" s="T158">n-n:case</ta>
            <ta e="T160" id="Seg_11391" s="T159">ptcl-adj&gt;adv</ta>
            <ta e="T161" id="Seg_11392" s="T160">ptcl</ta>
            <ta e="T162" id="Seg_11393" s="T161">v.[v:pn]</ta>
            <ta e="T163" id="Seg_11394" s="T162">pers</ta>
            <ta e="T164" id="Seg_11395" s="T163">adv</ta>
            <ta e="T165" id="Seg_11396" s="T164">num</ta>
            <ta e="T166" id="Seg_11397" s="T165">n-n:case</ta>
            <ta e="T167" id="Seg_11398" s="T166">n-n:num-n:case</ta>
            <ta e="T168" id="Seg_11399" s="T167">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T169" id="Seg_11400" s="T168">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T170" id="Seg_11401" s="T169">v-v&gt;adv</ta>
            <ta e="T171" id="Seg_11402" s="T170">n-n:case</ta>
            <ta e="T172" id="Seg_11403" s="T171">v-v:tense-v:pn</ta>
            <ta e="T173" id="Seg_11404" s="T172">conj</ta>
            <ta e="T174" id="Seg_11405" s="T173">ptcl</ta>
            <ta e="T175" id="Seg_11406" s="T174">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T176" id="Seg_11407" s="T175">conj</ta>
            <ta e="T177" id="Seg_11408" s="T176">v-v:pn</ta>
            <ta e="T178" id="Seg_11409" s="T177">v-v&gt;v.[v:pn]</ta>
            <ta e="T179" id="Seg_11410" s="T178">conj</ta>
            <ta e="T180" id="Seg_11411" s="T179">v-v&gt;v-v:pn</ta>
            <ta e="T181" id="Seg_11412" s="T180">conj</ta>
            <ta e="T182" id="Seg_11413" s="T181">adv-adv:case</ta>
            <ta e="T183" id="Seg_11414" s="T182">v-v:ins-v:pn</ta>
            <ta e="T184" id="Seg_11415" s="T183">pers.[n:case]</ta>
            <ta e="T185" id="Seg_11416" s="T184">n-n:ins-n:case</ta>
            <ta e="T186" id="Seg_11417" s="T185">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T187" id="Seg_11418" s="T186">conj</ta>
            <ta e="T188" id="Seg_11419" s="T187">n-n:case</ta>
            <ta e="T189" id="Seg_11420" s="T188">v-v:ins.[v:pn]</ta>
            <ta e="T190" id="Seg_11421" s="T189">conj</ta>
            <ta e="T191" id="Seg_11422" s="T190">pers.[n:case]</ta>
            <ta e="T192" id="Seg_11423" s="T191">adj</ta>
            <ta e="T193" id="Seg_11424" s="T192">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T194" id="Seg_11425" s="T193">v-v:tense.[v:pn]</ta>
            <ta e="T195" id="Seg_11426" s="T194">v-v&gt;adv</ta>
            <ta e="T196" id="Seg_11427" s="T195">n-clit</ta>
            <ta e="T197" id="Seg_11428" s="T196">n-n:case</ta>
            <ta e="T198" id="Seg_11429" s="T197">v-v:pn</ta>
            <ta e="T199" id="Seg_11430" s="T198">conj</ta>
            <ta e="T200" id="Seg_11431" s="T199">v-n:ins-v&gt;v-v&gt;adv</ta>
            <ta e="T201" id="Seg_11432" s="T200">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T202" id="Seg_11433" s="T201">conj</ta>
            <ta e="T203" id="Seg_11434" s="T202">num</ta>
            <ta e="T204" id="Seg_11435" s="T203">n.[n:case]</ta>
            <ta e="T205" id="Seg_11436" s="T204">v-v:pn</ta>
            <ta e="T206" id="Seg_11437" s="T205">n-n:num.[n:case]</ta>
            <ta e="T207" id="Seg_11438" s="T206">v-v:pn</ta>
            <ta e="T208" id="Seg_11439" s="T207">n-n:case</ta>
            <ta e="T209" id="Seg_11440" s="T208">conj</ta>
            <ta e="T210" id="Seg_11441" s="T209">v-v:pn</ta>
            <ta e="T211" id="Seg_11442" s="T210">interrog.[n:case]</ta>
            <ta e="T212" id="Seg_11443" s="T211">n-n:num.[n:case]</ta>
            <ta e="T213" id="Seg_11444" s="T212">v-v:mood-v:pn</ta>
            <ta e="T214" id="Seg_11445" s="T213">n-n:case</ta>
            <ta e="T215" id="Seg_11446" s="T214">v-v:mood-v:pn</ta>
            <ta e="T216" id="Seg_11447" s="T215">n-n:case</ta>
            <ta e="T217" id="Seg_11448" s="T216">n-n:ins-n:case</ta>
            <ta e="T218" id="Seg_11449" s="T217">v-v:mood.pn</ta>
            <ta e="T219" id="Seg_11450" s="T218">pers-n:num.[n:case]</ta>
            <ta e="T220" id="Seg_11451" s="T219">v-v&gt;v-v:pn</ta>
            <ta e="T221" id="Seg_11452" s="T220">interrog-clit.[n:case]</ta>
            <ta e="T222" id="Seg_11453" s="T221">n.[n:case]</ta>
            <ta e="T223" id="Seg_11454" s="T222">pers-n:num.[n:case]</ta>
            <ta e="T224" id="Seg_11455" s="T223">n-n&gt;n.[n:case]</ta>
            <ta e="T225" id="Seg_11456" s="T224">v-v:mood-v:pn</ta>
            <ta e="T226" id="Seg_11457" s="T225">adv</ta>
            <ta e="T227" id="Seg_11458" s="T226">pers.[n:case]</ta>
            <ta e="T228" id="Seg_11459" s="T227">dem</ta>
            <ta e="T229" id="Seg_11460" s="T228">n-n:case</ta>
            <ta e="T230" id="Seg_11461" s="T229">ptcl</ta>
            <ta e="T231" id="Seg_11462" s="T230">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T232" id="Seg_11463" s="T231">dem</ta>
            <ta e="T233" id="Seg_11464" s="T232">n-n:ins-n&gt;n-n:case</ta>
            <ta e="T234" id="Seg_11465" s="T233">dem</ta>
            <ta e="T235" id="Seg_11466" s="T234">n-n:ins-n:case</ta>
            <ta e="T236" id="Seg_11467" s="T235">dem</ta>
            <ta e="T237" id="Seg_11468" s="T236">n-n:ins-n:case</ta>
            <ta e="T238" id="Seg_11469" s="T237">conj</ta>
            <ta e="T239" id="Seg_11470" s="T238">v-v:ins-v:pn</ta>
            <ta e="T240" id="Seg_11471" s="T239">n.[n:case]</ta>
            <ta e="T241" id="Seg_11472" s="T240">v-v:pn</ta>
            <ta e="T242" id="Seg_11473" s="T241">pers</ta>
            <ta e="T243" id="Seg_11474" s="T242">v-v:mood.pn</ta>
            <ta e="T244" id="Seg_11475" s="T243">adv-adv:case</ta>
            <ta e="T245" id="Seg_11476" s="T244">conj</ta>
            <ta e="T246" id="Seg_11477" s="T245">pers</ta>
            <ta e="T247" id="Seg_11478" s="T246">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T248" id="Seg_11479" s="T247">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T249" id="Seg_11480" s="T248">interrog.[n:case]</ta>
            <ta e="T250" id="Seg_11481" s="T249">v-v:mood.[v:pn]</ta>
            <ta e="T251" id="Seg_11482" s="T250">adv-adv:case</ta>
            <ta e="T252" id="Seg_11483" s="T251">ptcl</ta>
            <ta e="T253" id="Seg_11484" s="T252">v-v&gt;v-v:mood.[v:pn]</ta>
            <ta e="T254" id="Seg_11485" s="T253">v-v:ins.[v:pn]</ta>
            <ta e="T255" id="Seg_11486" s="T254">v-v:ins-v:pn</ta>
            <ta e="T256" id="Seg_11487" s="T255">n-n:case</ta>
            <ta e="T257" id="Seg_11488" s="T256">interj</ta>
            <ta e="T258" id="Seg_11489" s="T257">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T259" id="Seg_11490" s="T258">v-v:pn</ta>
            <ta e="T260" id="Seg_11491" s="T259">interj</ta>
            <ta e="T261" id="Seg_11492" s="T260">interj</ta>
            <ta e="T262" id="Seg_11493" s="T261">n.[n:case]</ta>
            <ta e="T263" id="Seg_11494" s="T262">v-v&gt;adv</ta>
            <ta e="T264" id="Seg_11495" s="T263">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T265" id="Seg_11496" s="T264">v-v:pn</ta>
            <ta e="T266" id="Seg_11497" s="T265">v-v:mood.pn</ta>
            <ta e="T267" id="Seg_11498" s="T266">adv</ta>
            <ta e="T268" id="Seg_11499" s="T267">conj</ta>
            <ta e="T269" id="Seg_11500" s="T268">pers.[n:case]</ta>
            <ta e="T270" id="Seg_11501" s="T269">v-v:pn</ta>
            <ta e="T271" id="Seg_11502" s="T270">v-v&gt;adv</ta>
            <ta e="T272" id="Seg_11503" s="T271">ptcl</ta>
            <ta e="T273" id="Seg_11504" s="T272">v-v&gt;v-v:ins-v:mood-v:pn</ta>
            <ta e="T274" id="Seg_11505" s="T273">n.[n:case]</ta>
            <ta e="T275" id="Seg_11506" s="T274">v-v:pn</ta>
            <ta e="T276" id="Seg_11507" s="T275">pers</ta>
            <ta e="T277" id="Seg_11508" s="T276">pers</ta>
            <ta e="T278" id="Seg_11509" s="T277">v-v:mood-v:pn</ta>
            <ta e="T279" id="Seg_11510" s="T278">conj</ta>
            <ta e="T280" id="Seg_11511" s="T279">pers-n:case</ta>
            <ta e="T281" id="Seg_11512" s="T280">n.[n:case]-n:poss</ta>
            <ta e="T282" id="Seg_11513" s="T281">n-n:case</ta>
            <ta e="T283" id="Seg_11514" s="T282">pers.[n:case]</ta>
            <ta e="T284" id="Seg_11515" s="T283">pers</ta>
            <ta e="T285" id="Seg_11516" s="T284">v-v:tense-v:pn</ta>
            <ta e="T286" id="Seg_11517" s="T285">conj</ta>
            <ta e="T287" id="Seg_11518" s="T286">pers</ta>
            <ta e="T288" id="Seg_11519" s="T287">v-v:tense-v:pn</ta>
            <ta e="T289" id="Seg_11520" s="T288">conj</ta>
            <ta e="T290" id="Seg_11521" s="T289">pers</ta>
            <ta e="T291" id="Seg_11522" s="T290">pers</ta>
            <ta e="T292" id="Seg_11523" s="T291">n.[n:case]</ta>
            <ta e="T293" id="Seg_11524" s="T292">v-v:mood-v:tense-v:pn</ta>
            <ta e="T294" id="Seg_11525" s="T293">n.[n:case]</ta>
            <ta e="T295" id="Seg_11526" s="T294">v-v:tense</ta>
            <ta e="T296" id="Seg_11527" s="T295">conj</ta>
            <ta e="T299" id="Seg_11528" s="T298">pers.[n:case]</ta>
            <ta e="T300" id="Seg_11529" s="T299">dem</ta>
            <ta e="T301" id="Seg_11530" s="T300">n-n:case</ta>
            <ta e="T302" id="Seg_11531" s="T301">ptcl</ta>
            <ta e="T303" id="Seg_11532" s="T302">v-v:tense-v:pn</ta>
            <ta e="T304" id="Seg_11533" s="T303">pers.[n:case]</ta>
            <ta e="T305" id="Seg_11534" s="T304">ptcl</ta>
            <ta e="T306" id="Seg_11535" s="T305">v-v:tense</ta>
            <ta e="T307" id="Seg_11536" s="T306">n-n-n:case</ta>
            <ta e="T308" id="Seg_11537" s="T307">pers</ta>
            <ta e="T309" id="Seg_11538" s="T308">v-v:mood.pn</ta>
            <ta e="T310" id="Seg_11539" s="T309">n-n&gt;v-v:mood-v:pn</ta>
            <ta e="T311" id="Seg_11540" s="T310">pers-n:case</ta>
            <ta e="T312" id="Seg_11541" s="T311">v-v:pn</ta>
            <ta e="T313" id="Seg_11542" s="T312">interrog.[n:case]</ta>
            <ta e="T314" id="Seg_11543" s="T313">n-n:ins-n&gt;adv</ta>
            <ta e="T315" id="Seg_11544" s="T314">v-v:tense-v:pn</ta>
            <ta e="T316" id="Seg_11545" s="T315">n-n:num-n:case-n:poss</ta>
            <ta e="T317" id="Seg_11546" s="T316">adv</ta>
            <ta e="T318" id="Seg_11547" s="T317">v-v:inf</ta>
            <ta e="T319" id="Seg_11548" s="T318">conj</ta>
            <ta e="T320" id="Seg_11549" s="T319">pers</ta>
            <ta e="T321" id="Seg_11550" s="T320">pers-n:ins-n:case</ta>
            <ta e="T322" id="Seg_11551" s="T321">n-n:ins-n&gt;adv</ta>
            <ta e="T323" id="Seg_11552" s="T322">v-v:mood.pn</ta>
            <ta e="T324" id="Seg_11553" s="T323">n-n:num-n:case-n:poss</ta>
            <ta e="T325" id="Seg_11554" s="T324">adv</ta>
            <ta e="T326" id="Seg_11555" s="T325">v-v:mood.pn</ta>
            <ta e="T327" id="Seg_11556" s="T326">pers.[n:case]</ta>
            <ta e="T328" id="Seg_11557" s="T327">dem</ta>
            <ta e="T329" id="Seg_11558" s="T328">n-n:case</ta>
            <ta e="T330" id="Seg_11559" s="T329">ptcl</ta>
            <ta e="T331" id="Seg_11560" s="T330">v-v:tense-v:pn</ta>
            <ta e="T332" id="Seg_11561" s="T331">pers.[n:case]</ta>
            <ta e="T333" id="Seg_11562" s="T332">conj</ta>
            <ta e="T334" id="Seg_11563" s="T333">ptcl</ta>
            <ta e="T335" id="Seg_11564" s="T334">v-v:pn</ta>
            <ta e="T336" id="Seg_11565" s="T335">conj</ta>
            <ta e="T337" id="Seg_11566" s="T336">pers</ta>
            <ta e="T338" id="Seg_11567" s="T337">adv</ta>
            <ta e="T339" id="Seg_11568" s="T338">v-v:mood.pn</ta>
            <ta e="T340" id="Seg_11569" s="T339">pers</ta>
            <ta e="T341" id="Seg_11570" s="T340">v-v&gt;v-v:mood.pn</ta>
            <ta e="T342" id="Seg_11571" s="T341">pers</ta>
            <ta e="T343" id="Seg_11572" s="T342">v-v:tense-v:pn</ta>
            <ta e="T344" id="Seg_11573" s="T343">pers-n:ins-n:case</ta>
            <ta e="T345" id="Seg_11574" s="T344">conj</ta>
            <ta e="T346" id="Seg_11575" s="T345">v-v:tense-v:pn</ta>
            <ta e="T347" id="Seg_11576" s="T346">pers-n:case</ta>
            <ta e="T348" id="Seg_11577" s="T347">v-v:tense-v:pn</ta>
            <ta e="T349" id="Seg_11578" s="T348">n.[n:case]-n:poss</ta>
            <ta e="T350" id="Seg_11579" s="T349">v-v:ins.[v:pn]</ta>
            <ta e="T351" id="Seg_11580" s="T350">pers.[n:case]</ta>
            <ta e="T352" id="Seg_11581" s="T351">pers-n:ins-n:case</ta>
            <ta e="T353" id="Seg_11582" s="T352">n-n&gt;v-v:pn</ta>
            <ta e="T354" id="Seg_11583" s="T353">v-v:pn</ta>
            <ta e="T355" id="Seg_11584" s="T354">n-n&gt;v-v:mood-v:pn</ta>
            <ta e="T356" id="Seg_11585" s="T355">n.[n:case]-n:poss</ta>
            <ta e="T357" id="Seg_11586" s="T356">v-v:pn</ta>
            <ta e="T358" id="Seg_11587" s="T357">pers</ta>
            <ta e="T359" id="Seg_11588" s="T358">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T360" id="Seg_11589" s="T359">v-v:inf</ta>
            <ta e="T361" id="Seg_11590" s="T360">ptcl</ta>
            <ta e="T362" id="Seg_11591" s="T361">ptcl</ta>
            <ta e="T363" id="Seg_11592" s="T362">ptcl</ta>
            <ta e="T364" id="Seg_11593" s="T363">n-n&gt;v-v:mood-v:pn</ta>
            <ta e="T365" id="Seg_11594" s="T364">ptcl</ta>
            <ta e="T366" id="Seg_11595" s="T365">adv-adv:case</ta>
            <ta e="T367" id="Seg_11596" s="T366">conj</ta>
            <ta e="T368" id="Seg_11597" s="T367">emphpro</ta>
            <ta e="T369" id="Seg_11598" s="T368">v-v:pn</ta>
            <ta e="T370" id="Seg_11599" s="T369">conj</ta>
            <ta e="T371" id="Seg_11600" s="T370">adj-adj&gt;adv</ta>
            <ta e="T372" id="Seg_11601" s="T371">pers</ta>
            <ta e="T373" id="Seg_11602" s="T372">v-v:pn</ta>
            <ta e="T374" id="Seg_11603" s="T373">ptcl</ta>
            <ta e="T375" id="Seg_11604" s="T374">conj</ta>
            <ta e="T376" id="Seg_11605" s="T375">v-v:pn</ta>
            <ta e="T377" id="Seg_11606" s="T376">n-n&gt;v-v&gt;adv</ta>
            <ta e="T378" id="Seg_11607" s="T377">interrog.[n:case]</ta>
            <ta e="T379" id="Seg_11608" s="T378">n-n:ins-n&gt;adv</ta>
            <ta e="T380" id="Seg_11609" s="T379">v-v:tense-v:pn</ta>
            <ta e="T381" id="Seg_11610" s="T380">n-n:num-n:case-n:poss</ta>
            <ta e="T382" id="Seg_11611" s="T381">v-v&gt;v-v:inf</ta>
            <ta e="T383" id="Seg_11612" s="T382">adv</ta>
            <ta e="T384" id="Seg_11613" s="T383">n.[n:case]-n:poss</ta>
            <ta e="T385" id="Seg_11614" s="T384">n-n:ins-n&gt;adv</ta>
            <ta e="T386" id="Seg_11615" s="T385">v-v:pn</ta>
            <ta e="T387" id="Seg_11616" s="T386">n-n:num-n:case-n:poss</ta>
            <ta e="T388" id="Seg_11617" s="T387">ptcl</ta>
            <ta e="T389" id="Seg_11618" s="T388">v-v&gt;v-v:inf</ta>
            <ta e="T390" id="Seg_11619" s="T389">n-n:case-n:poss</ta>
            <ta e="T391" id="Seg_11620" s="T390">ptcl</ta>
            <ta e="T392" id="Seg_11621" s="T391">v-v:inf</ta>
            <ta e="T393" id="Seg_11622" s="T392">adv</ta>
            <ta e="T394" id="Seg_11623" s="T393">conj</ta>
            <ta e="T395" id="Seg_11624" s="T394">conj</ta>
            <ta e="T396" id="Seg_11625" s="T395">pers</ta>
            <ta e="T397" id="Seg_11626" s="T396">v-v:mood-v:pn</ta>
            <ta e="T398" id="Seg_11627" s="T397">n-n:ins-n&gt;adv</ta>
            <ta e="T399" id="Seg_11628" s="T398">pers</ta>
            <ta e="T400" id="Seg_11629" s="T399">ptcl</ta>
            <ta e="T401" id="Seg_11630" s="T400">pers</ta>
            <ta e="T402" id="Seg_11631" s="T401">n-n:num.[n:case]-n:poss</ta>
            <ta e="T403" id="Seg_11632" s="T402">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T404" id="Seg_11633" s="T403">conj</ta>
            <ta e="T405" id="Seg_11634" s="T404">adv</ta>
            <ta e="T406" id="Seg_11635" s="T405">pers</ta>
            <ta e="T407" id="Seg_11636" s="T406">pers</ta>
            <ta e="T408" id="Seg_11637" s="T407">n-n:case-n:obl.poss</ta>
            <ta e="T409" id="Seg_11638" s="T408">v-v:tense-v:pn</ta>
            <ta e="T410" id="Seg_11639" s="T409">n.[n:case]-n:poss</ta>
            <ta e="T411" id="Seg_11640" s="T410">v-v:pn</ta>
            <ta e="T412" id="Seg_11641" s="T411">v-v:mood.pn</ta>
            <ta e="T413" id="Seg_11642" s="T412">pers.[n:case]</ta>
            <ta e="T414" id="Seg_11643" s="T413">v-v:pn</ta>
            <ta e="T415" id="Seg_11644" s="T414">n.[n:case]-n:poss</ta>
            <ta e="T416" id="Seg_11645" s="T415">adv</ta>
            <ta e="T417" id="Seg_11646" s="T416">v-v&gt;v-v:pn</ta>
            <ta e="T418" id="Seg_11647" s="T417">n-n:num.[n:case]</ta>
            <ta e="T419" id="Seg_11648" s="T418">quant</ta>
            <ta e="T420" id="Seg_11649" s="T419">v-v&gt;v-v:pn</ta>
            <ta e="T421" id="Seg_11650" s="T420">ptcl</ta>
            <ta e="T422" id="Seg_11651" s="T421">conj</ta>
            <ta e="T423" id="Seg_11652" s="T422">v-v:inf</ta>
            <ta e="T424" id="Seg_11653" s="T423">v-v:ins-v:pn</ta>
            <ta e="T425" id="Seg_11654" s="T424">n-n-n:case</ta>
            <ta e="T426" id="Seg_11655" s="T425">v-v:pn</ta>
            <ta e="T427" id="Seg_11656" s="T426">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T428" id="Seg_11657" s="T427">quant</ta>
            <ta e="T429" id="Seg_11658" s="T428">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T430" id="Seg_11659" s="T429">n-n:num-n:case</ta>
            <ta e="T431" id="Seg_11660" s="T430">interrog-n:num-n:case</ta>
            <ta e="T432" id="Seg_11661" s="T431">n-n:case-n:poss</ta>
            <ta e="T433" id="Seg_11662" s="T432">n-n&gt;v-v:pn</ta>
            <ta e="T434" id="Seg_11663" s="T433">n.[n:case]-n:poss</ta>
            <ta e="T435" id="Seg_11664" s="T434">n-n:ins-n:case</ta>
            <ta e="T436" id="Seg_11665" s="T435">v-v:ins-v:pn</ta>
            <ta e="T437" id="Seg_11666" s="T436">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T438" id="Seg_11667" s="T437">adv</ta>
            <ta e="T439" id="Seg_11668" s="T438">v-v:pn</ta>
            <ta e="T440" id="Seg_11669" s="T439">conj</ta>
            <ta e="T441" id="Seg_11670" s="T440">ptcl</ta>
            <ta e="T442" id="Seg_11671" s="T441">v-v&gt;v-v:mood.[v:pn]</ta>
            <ta e="T443" id="Seg_11672" s="T442">v-v:mood.pn</ta>
            <ta e="T444" id="Seg_11673" s="T443">n.[n:case]-n:poss</ta>
            <ta e="T445" id="Seg_11674" s="T444">v-v:tense.[v:pn]</ta>
            <ta e="T446" id="Seg_11675" s="T445">n.[n:case]</ta>
            <ta e="T447" id="Seg_11676" s="T446">ptcl</ta>
            <ta e="T448" id="Seg_11677" s="T447">v-v:mood.[v:pn]</ta>
            <ta e="T449" id="Seg_11678" s="T448">ptcl</ta>
            <ta e="T450" id="Seg_11679" s="T449">interrog.[n:case]</ta>
            <ta e="T451" id="Seg_11680" s="T450">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T452" id="Seg_11681" s="T451">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T453" id="Seg_11682" s="T452">pers</ta>
            <ta e="T454" id="Seg_11683" s="T453">pers</ta>
            <ta e="T455" id="Seg_11684" s="T454">v-v:tense-v:pn</ta>
            <ta e="T456" id="Seg_11685" s="T455">pers.[n:case]</ta>
            <ta e="T457" id="Seg_11686" s="T456">n-n:case</ta>
            <ta e="T458" id="Seg_11687" s="T457">v-v:pn</ta>
            <ta e="T459" id="Seg_11688" s="T458">dem</ta>
            <ta e="T460" id="Seg_11689" s="T459">n-n:num-n:case</ta>
            <ta e="T461" id="Seg_11690" s="T460">quant</ta>
            <ta e="T462" id="Seg_11691" s="T461">v-v&gt;v-v:pn</ta>
            <ta e="T463" id="Seg_11692" s="T462">conj</ta>
            <ta e="T464" id="Seg_11693" s="T463">dem-n.[n:case]</ta>
            <ta e="T465" id="Seg_11694" s="T464">pers</ta>
            <ta e="T466" id="Seg_11695" s="T465">pers</ta>
            <ta e="T467" id="Seg_11696" s="T466">v-v:tense-v:pn</ta>
            <ta e="T468" id="Seg_11697" s="T467">n.[n:case]</ta>
            <ta e="T469" id="Seg_11698" s="T468">n.[n:case]</ta>
            <ta e="T470" id="Seg_11699" s="T469">v-v:mood-v:tense-v:pn</ta>
            <ta e="T471" id="Seg_11700" s="T470">n-n-n:case</ta>
            <ta e="T472" id="Seg_11701" s="T471">ptcl</ta>
            <ta e="T473" id="Seg_11702" s="T472">v-v:tense</ta>
            <ta e="T474" id="Seg_11703" s="T473">adv</ta>
            <ta e="T475" id="Seg_11704" s="T474">v-v:mood.pn</ta>
            <ta e="T476" id="Seg_11705" s="T475">n-n&gt;v-v:mood-v:pn</ta>
            <ta e="T477" id="Seg_11706" s="T476">interrog.[n:case]</ta>
            <ta e="T478" id="Seg_11707" s="T477">n-n:ins-n&gt;adv</ta>
            <ta e="T479" id="Seg_11708" s="T478">v-v:tense-v:pn</ta>
            <ta e="T480" id="Seg_11709" s="T479">n-n:num-n:case-n:poss</ta>
            <ta e="T481" id="Seg_11710" s="T480">adv</ta>
            <ta e="T482" id="Seg_11711" s="T481">v-v:inf</ta>
            <ta e="T483" id="Seg_11712" s="T482">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T484" id="Seg_11713" s="T483">pers-n:ins-n:case</ta>
            <ta e="T485" id="Seg_11714" s="T484">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T486" id="Seg_11715" s="T485">n-n:case</ta>
            <ta e="T487" id="Seg_11716" s="T486">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T488" id="Seg_11717" s="T487">n-n:num.[n:case]</ta>
            <ta e="T489" id="Seg_11718" s="T488">v-v:mood-v:pn</ta>
            <ta e="T490" id="Seg_11719" s="T489">n-n.[n:case]</ta>
            <ta e="T491" id="Seg_11720" s="T490">v-v:ins-v:pn</ta>
            <ta e="T492" id="Seg_11721" s="T491">n.[n:case]-n:poss</ta>
            <ta e="T493" id="Seg_11722" s="T492">v-v:ins.[v:pn]</ta>
            <ta e="T494" id="Seg_11723" s="T493">n-n:num-n:case</ta>
            <ta e="T495" id="Seg_11724" s="T494">quant-quant&gt;adv</ta>
            <ta e="T496" id="Seg_11725" s="T495">v-v:tense-v:pn</ta>
            <ta e="T497" id="Seg_11726" s="T496">conj</ta>
            <ta e="T498" id="Seg_11727" s="T497">n.[n:case]</ta>
            <ta e="T499" id="Seg_11728" s="T498">v-v:tense-v:mood-v:pn</ta>
            <ta e="T500" id="Seg_11729" s="T499">pers.[n:case]</ta>
            <ta e="T501" id="Seg_11730" s="T500">pers-n:ins-n:case</ta>
            <ta e="T502" id="Seg_11731" s="T501">n-n&gt;v-v:pn</ta>
            <ta e="T503" id="Seg_11732" s="T502">v-v:pn</ta>
            <ta e="T504" id="Seg_11733" s="T503">n-n&gt;v-v:mood-v:pn</ta>
            <ta e="T505" id="Seg_11734" s="T504">n.[n:case]-n:poss</ta>
            <ta e="T506" id="Seg_11735" s="T505">v-v:pn</ta>
            <ta e="T507" id="Seg_11736" s="T506">pers</ta>
            <ta e="T508" id="Seg_11737" s="T507">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T509" id="Seg_11738" s="T508">n.[n:case]-n:poss</ta>
            <ta e="T510" id="Seg_11739" s="T509">v-v:pn</ta>
            <ta e="T511" id="Seg_11740" s="T510">n-n&gt;v-v:mood-v:pn</ta>
            <ta e="T514" id="Seg_11741" s="T513">interrog.[n:case]</ta>
            <ta e="T515" id="Seg_11742" s="T514">interrog.[n:case]</ta>
            <ta e="T516" id="Seg_11743" s="T515">n-n:ins-n&gt;adv</ta>
            <ta e="T517" id="Seg_11744" s="T516">v-v:tense-v:pn</ta>
            <ta e="T518" id="Seg_11745" s="T517">n-n:ins-n:case-n:poss</ta>
            <ta e="T519" id="Seg_11746" s="T518">adv</ta>
            <ta e="T520" id="Seg_11747" s="T519">v-v:inf</ta>
            <ta e="T521" id="Seg_11748" s="T520">conj</ta>
            <ta e="T522" id="Seg_11749" s="T521">pers</ta>
            <ta e="T523" id="Seg_11750" s="T522">v-v:pn</ta>
            <ta e="T524" id="Seg_11751" s="T523">n-n:ins-n&gt;adv</ta>
            <ta e="T525" id="Seg_11752" s="T524">pers</ta>
            <ta e="T526" id="Seg_11753" s="T525">n.[n:case]-n:ins-n:poss</ta>
            <ta e="T527" id="Seg_11754" s="T526">v-v:mood.pn</ta>
            <ta e="T528" id="Seg_11755" s="T527">v-v:tense-v:pn</ta>
            <ta e="T529" id="Seg_11756" s="T528">pers-n:ins-n:num.[n:case]</ta>
            <ta e="T530" id="Seg_11757" s="T529">v-v:pn</ta>
            <ta e="T531" id="Seg_11758" s="T530">n-n&gt;v-v&gt;adv</ta>
            <ta e="T532" id="Seg_11759" s="T531">n.[n:case]-n:poss</ta>
            <ta e="T533" id="Seg_11760" s="T532">n-n:ins-n&gt;adv</ta>
            <ta e="T534" id="Seg_11761" s="T533">v-v:pn</ta>
            <ta e="T535" id="Seg_11762" s="T534">ptcl</ta>
            <ta e="T536" id="Seg_11763" s="T535">adv</ta>
            <ta e="T537" id="Seg_11764" s="T536">n-n:case-n:obl.poss</ta>
            <ta e="T538" id="Seg_11765" s="T537">v-v:tense-v:pn</ta>
            <ta e="T539" id="Seg_11766" s="T538">pers.[n:case]</ta>
            <ta e="T540" id="Seg_11767" s="T539">v-v:pn</ta>
            <ta e="T541" id="Seg_11768" s="T540">n-n:ins-n:case-n:poss</ta>
            <ta e="T542" id="Seg_11769" s="T541">pers.[n:case]</ta>
            <ta e="T543" id="Seg_11770" s="T542">adv</ta>
            <ta e="T544" id="Seg_11771" s="T543">v-v&gt;v-v:mood.[v:pn]</ta>
            <ta e="T545" id="Seg_11772" s="T544">n.[n:case]</ta>
            <ta e="T546" id="Seg_11773" s="T545">v-v&gt;v-v:pn</ta>
            <ta e="T547" id="Seg_11774" s="T546">v-v:ins-v:pn</ta>
            <ta e="T548" id="Seg_11775" s="T547">v-v:ins</ta>
            <ta e="T549" id="Seg_11776" s="T548">n-n-n:case</ta>
            <ta e="T550" id="Seg_11777" s="T549">v-v:pn</ta>
            <ta e="T551" id="Seg_11778" s="T550">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T552" id="Seg_11779" s="T551">n-n:case-n:poss</ta>
            <ta e="T553" id="Seg_11780" s="T552">n-n&gt;v-v:pn</ta>
            <ta e="T554" id="Seg_11781" s="T553">n.[n:case]-n:poss</ta>
            <ta e="T555" id="Seg_11782" s="T554">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T556" id="Seg_11783" s="T555">conj</ta>
            <ta e="T557" id="Seg_11784" s="T556">n-n:ins-n:case</ta>
            <ta e="T558" id="Seg_11785" s="T557">ptcl</ta>
            <ta e="T559" id="Seg_11786" s="T558">v-v:mood.[v:pn]</ta>
            <ta e="T560" id="Seg_11787" s="T559">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T561" id="Seg_11788" s="T560">adv</ta>
            <ta e="T562" id="Seg_11789" s="T561">v-v:pn</ta>
            <ta e="T563" id="Seg_11790" s="T562">conj</ta>
            <ta e="T564" id="Seg_11791" s="T563">ptcl</ta>
            <ta e="T565" id="Seg_11792" s="T564">v-v&gt;v-v:ins-n:case</ta>
            <ta e="T566" id="Seg_11793" s="T565">v-v:mood.pn</ta>
            <ta e="T567" id="Seg_11794" s="T566">n.[n:case]-n:poss</ta>
            <ta e="T568" id="Seg_11795" s="T567">v-v:tense.[v:pn]</ta>
            <ta e="T569" id="Seg_11796" s="T568">n.[n:case]</ta>
            <ta e="T570" id="Seg_11797" s="T569">v-v:ins.[v:pn]</ta>
            <ta e="T571" id="Seg_11798" s="T570">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T572" id="Seg_11799" s="T571">v-v:pn</ta>
            <ta e="T573" id="Seg_11800" s="T572">n-n:num-n:case</ta>
            <ta e="T574" id="Seg_11801" s="T573">n.[n:case]-n:poss</ta>
            <ta e="T575" id="Seg_11802" s="T574">quant</ta>
            <ta e="T576" id="Seg_11803" s="T575">v-v&gt;v-v:pn</ta>
            <ta e="T577" id="Seg_11804" s="T576">pers.[n:case]</ta>
            <ta e="T578" id="Seg_11805" s="T577">n-n:case</ta>
            <ta e="T579" id="Seg_11806" s="T578">v-v:pn</ta>
            <ta e="T580" id="Seg_11807" s="T579">conj</ta>
            <ta e="T581" id="Seg_11808" s="T580">pers</ta>
            <ta e="T582" id="Seg_11809" s="T581">dem-n.[n:case]</ta>
            <ta e="T583" id="Seg_11810" s="T582">pers</ta>
            <ta e="T584" id="Seg_11811" s="T583">v-v:tense-v:pn</ta>
            <ta e="T585" id="Seg_11812" s="T584">n.[n:case]</ta>
            <ta e="T586" id="Seg_11813" s="T585">n.[n:case]</ta>
            <ta e="T587" id="Seg_11814" s="T586">conj</ta>
            <ta e="T590" id="Seg_11815" s="T589">num-n-n&gt;adv</ta>
            <ta e="T591" id="Seg_11816" s="T590">v-v&gt;v.[v:pn]</ta>
            <ta e="T592" id="Seg_11817" s="T591">n-n-n:case</ta>
            <ta e="T593" id="Seg_11818" s="T592">ptcl</ta>
            <ta e="T594" id="Seg_11819" s="T593">v-v:tense</ta>
            <ta e="T595" id="Seg_11820" s="T594">pers</ta>
            <ta e="T596" id="Seg_11821" s="T595">adv</ta>
            <ta e="T597" id="Seg_11822" s="T596">v-v:mood.pn</ta>
            <ta e="T598" id="Seg_11823" s="T597">n-n&gt;v-v:inf</ta>
            <ta e="T599" id="Seg_11824" s="T598">n-n&gt;v-v:mood-v:pn</ta>
            <ta e="T600" id="Seg_11825" s="T599">interrog.[n:case]</ta>
            <ta e="T601" id="Seg_11826" s="T600">n-n:ins-n&gt;adv</ta>
            <ta e="T602" id="Seg_11827" s="T601">v-v:tense-v:pn</ta>
            <ta e="T603" id="Seg_11828" s="T602">conj</ta>
            <ta e="T604" id="Seg_11829" s="T603">n-n:ins-n:case-n:poss</ta>
            <ta e="T605" id="Seg_11830" s="T604">adv</ta>
            <ta e="T606" id="Seg_11831" s="T605">v-v:inf</ta>
            <ta e="T607" id="Seg_11832" s="T606">n.[n:case]</ta>
            <ta e="T608" id="Seg_11833" s="T607">v-v:ins.[v:pn]</ta>
            <ta e="T609" id="Seg_11834" s="T608">pers.[n:case]</ta>
            <ta e="T610" id="Seg_11835" s="T609">pers-n:ins-n:case</ta>
            <ta e="T611" id="Seg_11836" s="T610">n-n&gt;v-v:pn</ta>
            <ta e="T612" id="Seg_11837" s="T611">v-v:pn</ta>
            <ta e="T613" id="Seg_11838" s="T612">n-n&gt;v-v:mood-v:pn</ta>
            <ta e="T614" id="Seg_11839" s="T613">n.[n:case]-n:poss</ta>
            <ta e="T615" id="Seg_11840" s="T614">v-v:pn</ta>
            <ta e="T616" id="Seg_11841" s="T615">pers</ta>
            <ta e="T617" id="Seg_11842" s="T616">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T618" id="Seg_11843" s="T617">ptcl</ta>
            <ta e="T619" id="Seg_11844" s="T618">v-v:inf</ta>
            <ta e="T620" id="Seg_11845" s="T619">ptcl</ta>
            <ta e="T621" id="Seg_11846" s="T620">n-n&gt;v-v:mood-v:pn</ta>
            <ta e="T622" id="Seg_11847" s="T621">n-n&gt;v-v:mood-v:pn</ta>
            <ta e="T623" id="Seg_11848" s="T622">ptcl</ta>
            <ta e="T624" id="Seg_11849" s="T623">num-n:ins-n&gt;adv</ta>
            <ta e="T625" id="Seg_11850" s="T624">interrog.[n:case]</ta>
            <ta e="T626" id="Seg_11851" s="T625">n-n:ins-n&gt;adv</ta>
            <ta e="T627" id="Seg_11852" s="T626">v-v:tense-v:pn</ta>
            <ta e="T628" id="Seg_11853" s="T627">n-n:ins-n:case-n:poss</ta>
            <ta e="T629" id="Seg_11854" s="T628">adv</ta>
            <ta e="T630" id="Seg_11855" s="T629">v-v:inf</ta>
            <ta e="T631" id="Seg_11856" s="T630">conj</ta>
            <ta e="T632" id="Seg_11857" s="T631">pers</ta>
            <ta e="T633" id="Seg_11858" s="T632">v-v:tense-v:pn</ta>
            <ta e="T634" id="Seg_11859" s="T633">pers</ta>
            <ta e="T635" id="Seg_11860" s="T634">n.[n:case]-n:ins-n:poss</ta>
            <ta e="T636" id="Seg_11861" s="T635">v-v:mood.pn</ta>
            <ta e="T637" id="Seg_11862" s="T636">adv</ta>
            <ta e="T638" id="Seg_11863" s="T637">conj</ta>
            <ta e="T639" id="Seg_11864" s="T638">v-v:pn</ta>
            <ta e="T640" id="Seg_11865" s="T639">n-n&gt;v-v&gt;adv</ta>
            <ta e="T641" id="Seg_11866" s="T640">n-n:case-n:poss</ta>
            <ta e="T642" id="Seg_11867" s="T641">n-n:ins-n&gt;v-v&gt;adv</ta>
            <ta e="T643" id="Seg_11868" s="T642">v-v:pn</ta>
            <ta e="T644" id="Seg_11869" s="T643">n-n:ins-n:case-n:poss</ta>
            <ta e="T645" id="Seg_11870" s="T644">adv</ta>
            <ta e="T646" id="Seg_11871" s="T645">v-v:pn</ta>
            <ta e="T647" id="Seg_11872" s="T646">pers.[n:case]</ta>
            <ta e="T648" id="Seg_11873" s="T647">adv</ta>
            <ta e="T649" id="Seg_11874" s="T648">v-v&gt;v-v:mood.[v:pn]</ta>
            <ta e="T650" id="Seg_11875" s="T649">n-n:num-n:case</ta>
            <ta e="T651" id="Seg_11876" s="T650">ptcl</ta>
            <ta e="T652" id="Seg_11877" s="T651">v-v:pn</ta>
            <ta e="T653" id="Seg_11878" s="T652">n-n:poss-n:case</ta>
            <ta e="T654" id="Seg_11879" s="T653">v-v:pn</ta>
            <ta e="T655" id="Seg_11880" s="T654">v-v:mood.pn</ta>
            <ta e="T656" id="Seg_11881" s="T655">n.[n:case]-n:ins-n:poss</ta>
            <ta e="T657" id="Seg_11882" s="T656">n.[n:case]-n:poss</ta>
            <ta e="T658" id="Seg_11883" s="T657">v-v:pn</ta>
            <ta e="T659" id="Seg_11884" s="T658">v-v:mood.pn</ta>
            <ta e="T660" id="Seg_11885" s="T659">pers.[n:case]</ta>
            <ta e="T661" id="Seg_11886" s="T660">adv</ta>
            <ta e="T662" id="Seg_11887" s="T661">v-v&gt;v-v:pn</ta>
            <ta e="T663" id="Seg_11888" s="T662">ptcl</ta>
            <ta e="T664" id="Seg_11889" s="T663">v-v:pn</ta>
            <ta e="T665" id="Seg_11890" s="T664">v-v:pn</ta>
            <ta e="T666" id="Seg_11891" s="T665">v-v:mood.pn</ta>
            <ta e="T667" id="Seg_11892" s="T666">ptcl</ta>
            <ta e="T668" id="Seg_11893" s="T667">v-v:tense-v:pn</ta>
            <ta e="T669" id="Seg_11894" s="T668">adv</ta>
            <ta e="T670" id="Seg_11895" s="T669">v-v&gt;adv</ta>
            <ta e="T671" id="Seg_11896" s="T670">v-v:ins-v:pn</ta>
            <ta e="T672" id="Seg_11897" s="T671">conj</ta>
            <ta e="T673" id="Seg_11898" s="T672">ptcl</ta>
            <ta e="T674" id="Seg_11899" s="T673">v-v:mood.[v:pn]</ta>
            <ta e="T675" id="Seg_11900" s="T674">v-v:mood.pn</ta>
            <ta e="T676" id="Seg_11901" s="T675">conj</ta>
            <ta e="T677" id="Seg_11902" s="T676">dem</ta>
            <ta e="T678" id="Seg_11903" s="T677">n.[n:case]</ta>
            <ta e="T679" id="Seg_11904" s="T678">ptcl</ta>
            <ta e="T680" id="Seg_11905" s="T679">v-v:mood.[v:pn]</ta>
            <ta e="T681" id="Seg_11906" s="T680">v-v:pn</ta>
            <ta e="T682" id="Seg_11907" s="T681">n.[n:case]-n:poss</ta>
            <ta e="T683" id="Seg_11908" s="T682">n-n:case</ta>
            <ta e="T684" id="Seg_11909" s="T683">ptcl</ta>
            <ta e="T685" id="Seg_11910" s="T684">v-v:pn</ta>
            <ta e="T686" id="Seg_11911" s="T685">v-v&gt;adv</ta>
            <ta e="T687" id="Seg_11912" s="T686">v-v:mood-v:pn</ta>
            <ta e="T688" id="Seg_11913" s="T687">conj</ta>
            <ta e="T689" id="Seg_11914" s="T688">n-n:case</ta>
            <ta e="T690" id="Seg_11915" s="T689">v-v:ins-v:pn</ta>
            <ta e="T691" id="Seg_11916" s="T690">n.[n:case]</ta>
            <ta e="T692" id="Seg_11917" s="T691">v-v:pn</ta>
            <ta e="T693" id="Seg_11918" s="T692">n-n-n&gt;adv</ta>
            <ta e="T694" id="Seg_11919" s="T693">n-n-n:case</ta>
            <ta e="T695" id="Seg_11920" s="T694">v-v:tense-v:pn</ta>
            <ta e="T696" id="Seg_11921" s="T695">v-v:mood.pn</ta>
            <ta e="T697" id="Seg_11922" s="T696">n-n:case</ta>
            <ta e="T698" id="Seg_11923" s="T697">n.[n:case]</ta>
            <ta e="T699" id="Seg_11924" s="T698">n.[n:case]</ta>
            <ta e="T700" id="Seg_11925" s="T699">pers-n:ins-n:case</ta>
            <ta e="T701" id="Seg_11926" s="T700">n-n:ins-n:case</ta>
            <ta e="T702" id="Seg_11927" s="T701">n-n:ins-n:case</ta>
            <ta e="T703" id="Seg_11928" s="T702">n-v:mood-n:obl.poss-n:case</ta>
            <ta e="T704" id="Seg_11929" s="T703">n-n:ins-n:case</ta>
            <ta e="T705" id="Seg_11930" s="T704">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T706" id="Seg_11931" s="T705">n-pers-n:case</ta>
            <ta e="T707" id="Seg_11932" s="T706">v.[v:pn]</ta>
            <ta e="T708" id="Seg_11933" s="T707">n-n-n:case</ta>
            <ta e="T709" id="Seg_11934" s="T708">v-v:tense-v:pn</ta>
            <ta e="T710" id="Seg_11935" s="T709">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T711" id="Seg_11936" s="T710">n-n:case</ta>
            <ta e="T712" id="Seg_11937" s="T711">v-v&gt;adv</ta>
            <ta e="T713" id="Seg_11938" s="T712">ptcl</ta>
            <ta e="T714" id="Seg_11939" s="T713">v-v:mood-v:pn</ta>
            <ta e="T715" id="Seg_11940" s="T714">pers</ta>
            <ta e="T716" id="Seg_11941" s="T715">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T717" id="Seg_11942" s="T716">conj</ta>
            <ta e="T718" id="Seg_11943" s="T717">pers</ta>
            <ta e="T719" id="Seg_11944" s="T718">n-n:case</ta>
            <ta e="T720" id="Seg_11945" s="T719">v-v:ins-v:pn</ta>
            <ta e="T721" id="Seg_11946" s="T720">conj</ta>
            <ta e="T722" id="Seg_11947" s="T721">interrog-n:case</ta>
            <ta e="T723" id="Seg_11948" s="T722">n-n:case</ta>
            <ta e="T724" id="Seg_11949" s="T723">ptcl</ta>
            <ta e="T725" id="Seg_11950" s="T724">v-v:tense-v:pn</ta>
            <ta e="T726" id="Seg_11951" s="T725">adv</ta>
            <ta e="T727" id="Seg_11952" s="T726">n-n-n:case</ta>
            <ta e="T728" id="Seg_11953" s="T727">pers</ta>
            <ta e="T729" id="Seg_11954" s="T728">v-v:tense-v:pn</ta>
            <ta e="T730" id="Seg_11955" s="T729">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T731" id="Seg_11956" s="T730">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T732" id="Seg_11957" s="T731">n.[n:case]</ta>
            <ta e="T733" id="Seg_11958" s="T732">v-v:mood-v:pn</ta>
            <ta e="T734" id="Seg_11959" s="T733">n-n:case</ta>
            <ta e="T735" id="Seg_11960" s="T734">n.[n:case]</ta>
            <ta e="T736" id="Seg_11961" s="T735">conj</ta>
            <ta e="T737" id="Seg_11962" s="T736">n-n:case</ta>
            <ta e="T738" id="Seg_11963" s="T737">v-v:pn</ta>
            <ta e="T739" id="Seg_11964" s="T738">conj</ta>
            <ta e="T740" id="Seg_11965" s="T739">pers-n:ins-n:case</ta>
            <ta e="T741" id="Seg_11966" s="T740">n-n&gt;adv</ta>
            <ta e="T742" id="Seg_11967" s="T741">v-v&gt;v-v:pn</ta>
            <ta e="T743" id="Seg_11968" s="T742">v-v:pn</ta>
            <ta e="T744" id="Seg_11969" s="T743">v-v:mood.pn</ta>
            <ta e="T745" id="Seg_11970" s="T744">n-n-n:case</ta>
            <ta e="T746" id="Seg_11971" s="T745">n-n-n:case</ta>
            <ta e="T747" id="Seg_11972" s="T746">pers</ta>
            <ta e="T748" id="Seg_11973" s="T747">v-v:tense-v:pn</ta>
            <ta e="T749" id="Seg_11974" s="T748">v-v:pn</ta>
            <ta e="T750" id="Seg_11975" s="T749">v-v&gt;v-v&gt;adv</ta>
            <ta e="T751" id="Seg_11976" s="T750">n-n:case</ta>
            <ta e="T752" id="Seg_11977" s="T751">n-n&gt;adj</ta>
            <ta e="T753" id="Seg_11978" s="T752">v-v&gt;adv</ta>
            <ta e="T754" id="Seg_11979" s="T753">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T755" id="Seg_11980" s="T754">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T756" id="Seg_11981" s="T755">v-v:ins.[v:pn]</ta>
            <ta e="T757" id="Seg_11982" s="T756">n-n:case-n:poss</ta>
            <ta e="T758" id="Seg_11983" s="T757">n-n:case</ta>
            <ta e="T759" id="Seg_11984" s="T758">n-n:case-n:poss</ta>
            <ta e="T760" id="Seg_11985" s="T759">n-n:ins-n:case</ta>
            <ta e="T761" id="Seg_11986" s="T760">ptcl</ta>
            <ta e="T762" id="Seg_11987" s="T761">v-v:mood-v:pn</ta>
            <ta e="T763" id="Seg_11988" s="T762">n-n-n:case</ta>
            <ta e="T764" id="Seg_11989" s="T763">pers.[n:case]</ta>
            <ta e="T765" id="Seg_11990" s="T764">pers</ta>
            <ta e="T766" id="Seg_11991" s="T765">v-v:tense-v:pn</ta>
            <ta e="T767" id="Seg_11992" s="T766">emphpro</ta>
            <ta e="T768" id="Seg_11993" s="T767">ptcl</ta>
            <ta e="T769" id="Seg_11994" s="T768">v-v:mood.[v:pn]</ta>
            <ta e="T770" id="Seg_11995" s="T769">n-n:ins-n:case</ta>
            <ta e="T771" id="Seg_11996" s="T770">v-v&gt;v-v:pn</ta>
            <ta e="T772" id="Seg_11997" s="T771">v-v&gt;v-v&gt;adv</ta>
            <ta e="T773" id="Seg_11998" s="T772">v-v:ins-v&gt;v-v:ins-v:pn</ta>
            <ta e="T774" id="Seg_11999" s="T773">conj</ta>
            <ta e="T775" id="Seg_12000" s="T774">v-v:inf</ta>
            <ta e="T776" id="Seg_12001" s="T775">v-v:ins-v:pn</ta>
            <ta e="T777" id="Seg_12002" s="T776">conj</ta>
            <ta e="T778" id="Seg_12003" s="T777">ptcl</ta>
            <ta e="T779" id="Seg_12004" s="T778">v-v&gt;v-v:pn</ta>
            <ta e="T780" id="Seg_12005" s="T779">n.[n:case]-n:poss</ta>
            <ta e="T781" id="Seg_12006" s="T780">pers.[n:case]</ta>
            <ta e="T782" id="Seg_12007" s="T781">v-v:pn</ta>
            <ta e="T783" id="Seg_12008" s="T782">adv</ta>
            <ta e="T784" id="Seg_12009" s="T783">v-v&gt;v-v:pn</ta>
            <ta e="T785" id="Seg_12010" s="T784">conj</ta>
            <ta e="T786" id="Seg_12011" s="T785">adj-adj&gt;adv</ta>
            <ta e="T787" id="Seg_12012" s="T786">n-n&gt;n.[n:case]-n:poss</ta>
            <ta e="T788" id="Seg_12013" s="T787">n-n&gt;n.[n:case]-n:poss</ta>
            <ta e="T789" id="Seg_12014" s="T788">pers-n:ins-n:num.[n:case]</ta>
            <ta e="T790" id="Seg_12015" s="T789">v-v:tense-v:pn</ta>
            <ta e="T791" id="Seg_12016" s="T790">n-n:case</ta>
            <ta e="T792" id="Seg_12017" s="T791">n-n:ins-n:case</ta>
            <ta e="T793" id="Seg_12018" s="T792">n-n&gt;n.[n:case]-n:poss</ta>
            <ta e="T794" id="Seg_12019" s="T793">v-v:inf</ta>
            <ta e="T795" id="Seg_12020" s="T794">preverb</ta>
            <ta e="T796" id="Seg_12021" s="T795">v-v:pn</ta>
            <ta e="T797" id="Seg_12022" s="T796">pers.[n:case]</ta>
            <ta e="T798" id="Seg_12023" s="T797">n-n&gt;n-n:obl.poss-n:case</ta>
            <ta e="T799" id="Seg_12024" s="T798">v-v:pn</ta>
            <ta e="T800" id="Seg_12025" s="T799">pers</ta>
            <ta e="T801" id="Seg_12026" s="T800">n-n:num.[n:case]-n:poss</ta>
            <ta e="T802" id="Seg_12027" s="T801">v-v:mood-v:pn</ta>
            <ta e="T803" id="Seg_12028" s="T802">conj</ta>
            <ta e="T804" id="Seg_12029" s="T803">n-n&gt;n.[n:case]-n:poss</ta>
            <ta e="T805" id="Seg_12030" s="T804">v-v:pn</ta>
            <ta e="T806" id="Seg_12031" s="T805">pers</ta>
            <ta e="T807" id="Seg_12032" s="T806">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T808" id="Seg_12033" s="T807">conj</ta>
            <ta e="T809" id="Seg_12034" s="T808">n-n:case</ta>
            <ta e="T810" id="Seg_12035" s="T809">v-v:ins-v:pn</ta>
            <ta e="T811" id="Seg_12036" s="T810">v-v&gt;v-v:mood.pn</ta>
            <ta e="T812" id="Seg_12037" s="T811">n-n:case</ta>
            <ta e="T813" id="Seg_12038" s="T812">n-n:case</ta>
            <ta e="T814" id="Seg_12039" s="T813">n-n:ins-n:case</ta>
            <ta e="T815" id="Seg_12040" s="T814">v-v:pn</ta>
            <ta e="T816" id="Seg_12041" s="T815">conj</ta>
            <ta e="T817" id="Seg_12042" s="T816">v.[v:pn]</ta>
            <ta e="T818" id="Seg_12043" s="T817">n-n&gt;n.[n:case]-n:poss</ta>
            <ta e="T819" id="Seg_12044" s="T818">n-n:case.poss</ta>
            <ta e="T820" id="Seg_12045" s="T819">preverb</ta>
            <ta e="T821" id="Seg_12046" s="T820">v-v:pn</ta>
            <ta e="T822" id="Seg_12047" s="T821">v-v:inf</ta>
            <ta e="T823" id="Seg_12048" s="T822">pers.[n:case]</ta>
            <ta e="T824" id="Seg_12049" s="T823">n-n&gt;n-n:poss-n:case</ta>
            <ta e="T825" id="Seg_12050" s="T824">v-v:pn</ta>
            <ta e="T826" id="Seg_12051" s="T825">pers</ta>
            <ta e="T827" id="Seg_12052" s="T826">n.[n:case]-n:ins-n:poss</ta>
            <ta e="T828" id="Seg_12053" s="T827">v-v:mood-v:pn</ta>
            <ta e="T829" id="Seg_12054" s="T828">pers.[n:case]</ta>
            <ta e="T830" id="Seg_12055" s="T829">v-v:ins.[v:pn]</ta>
            <ta e="T831" id="Seg_12056" s="T830">n-n:poss-n:case</ta>
            <ta e="T832" id="Seg_12057" s="T831">conj</ta>
            <ta e="T833" id="Seg_12058" s="T832">v-v&gt;adv</ta>
            <ta e="T834" id="Seg_12059" s="T833">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T835" id="Seg_12060" s="T834">ptcl</ta>
            <ta e="T836" id="Seg_12061" s="T835">v-v:pn</ta>
            <ta e="T837" id="Seg_12062" s="T836">pers</ta>
            <ta e="T838" id="Seg_12063" s="T837">ptcl</ta>
            <ta e="T839" id="Seg_12064" s="T838">v-v:inf</ta>
            <ta e="T840" id="Seg_12065" s="T839">n.[n:case]-n:poss</ta>
            <ta e="T841" id="Seg_12066" s="T840">v-v:pn</ta>
            <ta e="T842" id="Seg_12067" s="T841">n</ta>
            <ta e="T843" id="Seg_12068" s="T842">v-v:mood.pn</ta>
            <ta e="T844" id="Seg_12069" s="T843">pers.[n:case]</ta>
            <ta e="T845" id="Seg_12070" s="T844">v-v:ins-v:pn</ta>
            <ta e="T846" id="Seg_12071" s="T845">conj</ta>
            <ta e="T847" id="Seg_12072" s="T846">n-n:num-n:case</ta>
            <ta e="T848" id="Seg_12073" s="T847">v-v&gt;v-v&gt;adv</ta>
            <ta e="T849" id="Seg_12074" s="T848">ptcl</ta>
            <ta e="T850" id="Seg_12075" s="T849">v-v&gt;v-v:ins-v:mood-v:pn</ta>
            <ta e="T851" id="Seg_12076" s="T850">v-v:pn</ta>
            <ta e="T852" id="Seg_12077" s="T851">n-n:num.[n:case]</ta>
            <ta e="T853" id="Seg_12078" s="T852">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T854" id="Seg_12079" s="T853">pers.[n:case]</ta>
            <ta e="T855" id="Seg_12080" s="T854">n-n&gt;n-n:poss-n:case</ta>
            <ta e="T856" id="Seg_12081" s="T855">v-v:pn</ta>
            <ta e="T857" id="Seg_12082" s="T856">v-v&gt;adv</ta>
            <ta e="T858" id="Seg_12083" s="T857">v-v:mood.pn</ta>
            <ta e="T859" id="Seg_12084" s="T858">n-n-n:case</ta>
            <ta e="T860" id="Seg_12085" s="T859">ptcl</ta>
            <ta e="T861" id="Seg_12086" s="T860">v-v&gt;v-v:mood.pn</ta>
            <ta e="T862" id="Seg_12087" s="T861">pers.[n:case]</ta>
            <ta e="T863" id="Seg_12088" s="T862">pers-n:case</ta>
            <ta e="T864" id="Seg_12089" s="T863">emphpro</ta>
            <ta e="T865" id="Seg_12090" s="T864">v-v:tense-v:pn</ta>
            <ta e="T866" id="Seg_12091" s="T865">n.[n:case]</ta>
            <ta e="T867" id="Seg_12092" s="T866">n-n:ins-n&gt;v-v&gt;adv</ta>
            <ta e="T868" id="Seg_12093" s="T867">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T869" id="Seg_12094" s="T868">pers.[n:case]</ta>
            <ta e="T870" id="Seg_12095" s="T869">n-n:num-n:case</ta>
            <ta e="T871" id="Seg_12096" s="T870">n-n:case.poss</ta>
            <ta e="T872" id="Seg_12097" s="T871">v-v:ins-v:pn</ta>
            <ta e="T875" id="Seg_12098" s="T874">v-v&gt;v-v:mood.[v:pn]</ta>
            <ta e="T876" id="Seg_12099" s="T875">conj</ta>
            <ta e="T877" id="Seg_12100" s="T876">v-v:pn</ta>
            <ta e="T878" id="Seg_12101" s="T877">n-n:case</ta>
            <ta e="T879" id="Seg_12102" s="T878">n-n:ins-n&gt;adv</ta>
            <ta e="T880" id="Seg_12103" s="T879">n.[n:case]-n:poss</ta>
            <ta e="T881" id="Seg_12104" s="T880">v-v:pn</ta>
            <ta e="T882" id="Seg_12105" s="T881">v-v&gt;adv</ta>
            <ta e="T883" id="Seg_12106" s="T882">n-n:ins-n&gt;adv</ta>
            <ta e="T884" id="Seg_12107" s="T883">v-v:pn</ta>
            <ta e="T885" id="Seg_12108" s="T884">n-n:case</ta>
            <ta e="T886" id="Seg_12109" s="T885">v-v:pn</ta>
            <ta e="T887" id="Seg_12110" s="T886">ptcl</ta>
            <ta e="T888" id="Seg_12111" s="T887">v-v:mood-v:pn</ta>
            <ta e="T889" id="Seg_12112" s="T888">conj</ta>
            <ta e="T890" id="Seg_12113" s="T889">n.[n:case]</ta>
            <ta e="T891" id="Seg_12114" s="T890">v-v:pn</ta>
            <ta e="T892" id="Seg_12115" s="T891">v-v&gt;v-v&gt;adv</ta>
            <ta e="T893" id="Seg_12116" s="T892">v-v:mood-v:pn-v:mood</ta>
            <ta e="T894" id="Seg_12117" s="T893">pers.[n:case]</ta>
            <ta e="T895" id="Seg_12118" s="T894">n-n:num-n:case</ta>
            <ta e="T896" id="Seg_12119" s="T895">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T897" id="Seg_12120" s="T896">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T898" id="Seg_12121" s="T897">ptcl</ta>
            <ta e="T899" id="Seg_12122" s="T898">adv</ta>
            <ta e="T900" id="Seg_12123" s="T899">v-v:mood-v:pn</ta>
            <ta e="T901" id="Seg_12124" s="T900">n-n:case</ta>
            <ta e="T902" id="Seg_12125" s="T901">v-v:tense-v:pn</ta>
            <ta e="T903" id="Seg_12126" s="T902">conj</ta>
            <ta e="T904" id="Seg_12127" s="T903">adv-adv:case</ta>
            <ta e="T905" id="Seg_12128" s="T904">n.[n:case]</ta>
            <ta e="T906" id="Seg_12129" s="T905">v-v:pn</ta>
            <ta e="T907" id="Seg_12130" s="T906">adv-adv:case</ta>
            <ta e="T908" id="Seg_12131" s="T907">v-v:tense-v:pn</ta>
            <ta e="T909" id="Seg_12132" s="T908">ptcl</ta>
            <ta e="T910" id="Seg_12133" s="T909">v-v:mood.pn</ta>
            <ta e="T911" id="Seg_12134" s="T910">v-v:mood.pn</ta>
            <ta e="T912" id="Seg_12135" s="T911">adv</ta>
            <ta e="T913" id="Seg_12136" s="T912">pers</ta>
            <ta e="T914" id="Seg_12137" s="T913">v-v:tense-v:pn</ta>
            <ta e="T915" id="Seg_12138" s="T914">interrog.[n:case]</ta>
            <ta e="T916" id="Seg_12139" s="T915">n-n:num-n:case</ta>
            <ta e="T917" id="Seg_12140" s="T916">ptcl</ta>
            <ta e="T918" id="Seg_12141" s="T917">v-v&gt;v-v:pn</ta>
            <ta e="T919" id="Seg_12142" s="T918">pers</ta>
            <ta e="T920" id="Seg_12143" s="T919">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T921" id="Seg_12144" s="T920">n-n:case</ta>
            <ta e="T922" id="Seg_12145" s="T921">v-v:ins-v:pn</ta>
            <ta e="T923" id="Seg_12146" s="T922">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T924" id="Seg_12147" s="T923">adv</ta>
            <ta e="T925" id="Seg_12148" s="T924">n-n:ins-n:case</ta>
            <ta e="T926" id="Seg_12149" s="T925">v-v:mood-v:pn</ta>
            <ta e="T927" id="Seg_12150" s="T926">pers.[n:case]</ta>
            <ta e="T928" id="Seg_12151" s="T927">adv</ta>
            <ta e="T929" id="Seg_12152" s="T928">adv</ta>
            <ta e="T930" id="Seg_12153" s="T929">v-v&gt;v-v:mood.[v:pn]</ta>
            <ta e="T931" id="Seg_12154" s="T930">adv</ta>
            <ta e="T932" id="Seg_12155" s="T931">n-n:ins-n:case</ta>
            <ta e="T933" id="Seg_12156" s="T932">v-v:mood-v:pn</ta>
            <ta e="T934" id="Seg_12157" s="T933">n.[n:case]</ta>
            <ta e="T935" id="Seg_12158" s="T934">adv</ta>
            <ta e="T936" id="Seg_12159" s="T935">v-v&gt;v-v:pn</ta>
            <ta e="T937" id="Seg_12160" s="T936">v-v&gt;v-v:pn</ta>
            <ta e="T938" id="Seg_12161" s="T937">conj</ta>
            <ta e="T939" id="Seg_12162" s="T938">v-v:ins-v:pn</ta>
            <ta e="T940" id="Seg_12163" s="T939">conj</ta>
            <ta e="T941" id="Seg_12164" s="T940">n.[n:case]-n:poss</ta>
            <ta e="T942" id="Seg_12165" s="T941">n-n:ins-n:case.poss</ta>
            <ta e="T943" id="Seg_12166" s="T942">v-v:ins-v&gt;v-v:pn</ta>
            <ta e="T944" id="Seg_12167" s="T943">v-v:pn</ta>
            <ta e="T945" id="Seg_12168" s="T944">pers</ta>
            <ta e="T946" id="Seg_12169" s="T945">adj-adj&gt;adv</ta>
            <ta e="T947" id="Seg_12170" s="T946">ptcl</ta>
            <ta e="T948" id="Seg_12171" s="T947">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T949" id="Seg_12172" s="T948">n-n:num-n:case-n:obl.poss</ta>
            <ta e="T950" id="Seg_12173" s="T949">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T951" id="Seg_12174" s="T950">dem</ta>
            <ta e="T952" id="Seg_12175" s="T951">n-n:ins-n:case</ta>
            <ta e="T953" id="Seg_12176" s="T952">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T954" id="Seg_12177" s="T953">n.[n:case]-n:poss</ta>
            <ta e="T955" id="Seg_12178" s="T954">adv</ta>
            <ta e="T956" id="Seg_12179" s="T955">v-v&gt;v-v:pn</ta>
            <ta e="T957" id="Seg_12180" s="T956">conj</ta>
            <ta e="T958" id="Seg_12181" s="T957">v-v:ins-v:pn</ta>
            <ta e="T959" id="Seg_12182" s="T958">n-n:num.[n:case]</ta>
            <ta e="T960" id="Seg_12183" s="T959">v-v&gt;adv</ta>
            <ta e="T961" id="Seg_12184" s="T960">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T962" id="Seg_12185" s="T961">n-n&gt;n-n:case-n:poss</ta>
            <ta e="T963" id="Seg_12186" s="T962">adv</ta>
            <ta e="T964" id="Seg_12187" s="T963">v-v:mood-v:pn</ta>
            <ta e="T965" id="Seg_12188" s="T964">conj</ta>
            <ta e="T966" id="Seg_12189" s="T965">v-v:ins-v:pn</ta>
            <ta e="T967" id="Seg_12190" s="T966">conj</ta>
            <ta e="T968" id="Seg_12191" s="T967">n-n&gt;n-n:poss-n:case</ta>
            <ta e="T969" id="Seg_12192" s="T968">v-v:pn</ta>
            <ta e="T970" id="Seg_12193" s="T969">pers.[n:case]</ta>
            <ta e="T971" id="Seg_12194" s="T970">pers-n:case</ta>
            <ta e="T972" id="Seg_12195" s="T971">v-v:tense-v:pn</ta>
            <ta e="T973" id="Seg_12196" s="T972">ptcl</ta>
            <ta e="T974" id="Seg_12197" s="T973">v-v:mood.pn</ta>
            <ta e="T975" id="Seg_12198" s="T974">v-v:ins-v:pn</ta>
            <ta e="T976" id="Seg_12199" s="T975">n-n&gt;adj</ta>
            <ta e="T977" id="Seg_12200" s="T976">v-v&gt;v-v&gt;adv</ta>
            <ta e="T978" id="Seg_12201" s="T977">n-n&gt;adj</ta>
            <ta e="T979" id="Seg_12202" s="T978">n-n:ins-n:case</ta>
            <ta e="T980" id="Seg_12203" s="T979">v-v:ins-v:pn</ta>
            <ta e="T981" id="Seg_12204" s="T980">pers-n:num-n:case</ta>
            <ta e="T982" id="Seg_12205" s="T981">adv-adv:case</ta>
            <ta e="T983" id="Seg_12206" s="T982">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T984" id="Seg_12207" s="T983">v-v:ins-v:pn</ta>
            <ta e="T985" id="Seg_12208" s="T984">conj</ta>
            <ta e="T986" id="Seg_12209" s="T985">n-n:case</ta>
            <ta e="T987" id="Seg_12210" s="T986">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T988" id="Seg_12211" s="T987">n-n:ins-n:case-n:poss</ta>
            <ta e="T989" id="Seg_12212" s="T988">adv</ta>
            <ta e="T990" id="Seg_12213" s="T989">v-v&gt;v-v&gt;adv</ta>
            <ta e="T991" id="Seg_12214" s="T990">v-v&gt;v-v:pn</ta>
            <ta e="T992" id="Seg_12215" s="T991">n-n:num-n:case-n:poss</ta>
            <ta e="T993" id="Seg_12216" s="T992">quant</ta>
            <ta e="T994" id="Seg_12217" s="T993">v-v:ins-v:pn</ta>
            <ta e="T995" id="Seg_12218" s="T994">n-n&gt;n-n:obl.poss-n:case</ta>
            <ta e="T996" id="Seg_12219" s="T995">v-v:pn</ta>
            <ta e="T997" id="Seg_12220" s="T996">conj</ta>
            <ta e="T998" id="Seg_12221" s="T997">ptcl</ta>
            <ta e="T999" id="Seg_12222" s="T998">v-v:mood-v:pn</ta>
            <ta e="T1000" id="Seg_12223" s="T999">v-v&gt;adv</ta>
            <ta e="T1001" id="Seg_12224" s="T1000">v-v:pn</ta>
            <ta e="T1002" id="Seg_12225" s="T1001">emphpro</ta>
            <ta e="T1003" id="Seg_12226" s="T1002">n-n:case.poss</ta>
            <ta e="T1004" id="Seg_12227" s="T1003">n-n:num.[n:case]-n:poss</ta>
            <ta e="T1005" id="Seg_12228" s="T1004">v-v&gt;v-v:pn</ta>
            <ta e="T1006" id="Seg_12229" s="T1005">conj</ta>
            <ta e="T1007" id="Seg_12230" s="T1006">n.[n:case]-n:poss</ta>
            <ta e="T1008" id="Seg_12231" s="T1007">interrog</ta>
            <ta e="T1009" id="Seg_12232" s="T1008">v-v:pn</ta>
            <ta e="T1010" id="Seg_12233" s="T1009">n.[n:case]-n:poss</ta>
            <ta e="T1011" id="Seg_12234" s="T1010">v-v:tense.[v:pn]</ta>
            <ta e="T1012" id="Seg_12235" s="T1011">quant</ta>
            <ta e="T1013" id="Seg_12236" s="T1012">v-v:ins-v:pn</ta>
            <ta e="T1014" id="Seg_12237" s="T1013">n-n:case</ta>
            <ta e="T1015" id="Seg_12238" s="T1014">pers.[n:case]</ta>
            <ta e="T1016" id="Seg_12239" s="T1015">pro-n:case.poss</ta>
            <ta e="T1017" id="Seg_12240" s="T1016">n.[n:case]</ta>
            <ta e="T1018" id="Seg_12241" s="T1017">v-v:mood-v:pn</ta>
            <ta e="T1019" id="Seg_12242" s="T1018">conj</ta>
            <ta e="T1020" id="Seg_12243" s="T1019">v-v:pn</ta>
            <ta e="T1021" id="Seg_12244" s="T1020">conj</ta>
            <ta e="T1022" id="Seg_12245" s="T1021">v-v&gt;adv</ta>
            <ta e="T1023" id="Seg_12246" s="T1022">ptcl</ta>
            <ta e="T1024" id="Seg_12247" s="T1023">v-v&gt;v-v:ins-v:mood.[v:pn]</ta>
            <ta e="T1025" id="Seg_12248" s="T1024">pers</ta>
            <ta e="T1026" id="Seg_12249" s="T1025">adv</ta>
            <ta e="T1027" id="Seg_12250" s="T1026">pers-n:case</ta>
            <ta e="T1028" id="Seg_12251" s="T1027">v-v:tense-v:pn</ta>
            <ta e="T1029" id="Seg_12252" s="T1028">adv</ta>
            <ta e="T1030" id="Seg_12253" s="T1029">adj-adj&gt;adv</ta>
            <ta e="T1031" id="Seg_12254" s="T1030">v.[v:pn]</ta>
            <ta e="T1032" id="Seg_12255" s="T1031">n.[n:case]-n:poss</ta>
            <ta e="T1033" id="Seg_12256" s="T1032">adj</ta>
            <ta e="T1034" id="Seg_12257" s="T1033">pers</ta>
            <ta e="T1035" id="Seg_12258" s="T1034">n-n:num-n:case</ta>
            <ta e="T1036" id="Seg_12259" s="T1035">pers</ta>
            <ta e="T1037" id="Seg_12260" s="T1036">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T1038" id="Seg_12261" s="T1037">pers</ta>
            <ta e="T1039" id="Seg_12262" s="T1038">v-v:pn</ta>
            <ta e="T1040" id="Seg_12263" s="T1039">pers-n:case.poss</ta>
            <ta e="T1041" id="Seg_12264" s="T1040">v-v&gt;v-v:mood.pn</ta>
            <ta e="T1042" id="Seg_12265" s="T1041">pers</ta>
            <ta e="T1043" id="Seg_12266" s="T1042">pers-n:num-n:case</ta>
            <ta e="T1044" id="Seg_12267" s="T1043">adv</ta>
            <ta e="T1045" id="Seg_12268" s="T1044">v-v:ins-v&gt;v-v:ins-v:pn</ta>
            <ta e="T1046" id="Seg_12269" s="T1045">dem-n-n&gt;adv</ta>
            <ta e="T1047" id="Seg_12270" s="T1046">n-n:case</ta>
            <ta e="T1048" id="Seg_12271" s="T1047">v-v:tense-v:pn</ta>
            <ta e="T1049" id="Seg_12272" s="T1048">pers-n:num-n:case</ta>
            <ta e="T1050" id="Seg_12273" s="T1049">adv</ta>
            <ta e="T1051" id="Seg_12274" s="T1050">v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_12275" s="T1">nprop</ta>
            <ta e="T3" id="Seg_12276" s="T2">n</ta>
            <ta e="T4" id="Seg_12277" s="T3">v</ta>
            <ta e="T5" id="Seg_12278" s="T4">nprop</ta>
            <ta e="T6" id="Seg_12279" s="T5">n</ta>
            <ta e="T7" id="Seg_12280" s="T6">pers</ta>
            <ta e="T8" id="Seg_12281" s="T7">num</ta>
            <ta e="T9" id="Seg_12282" s="T8">n</ta>
            <ta e="T10" id="Seg_12283" s="T9">n</ta>
            <ta e="T11" id="Seg_12284" s="T10">quant</ta>
            <ta e="T12" id="Seg_12285" s="T11">v</ta>
            <ta e="T13" id="Seg_12286" s="T12">num</ta>
            <ta e="T14" id="Seg_12287" s="T13">n</ta>
            <ta e="T15" id="Seg_12288" s="T14">adv</ta>
            <ta e="T16" id="Seg_12289" s="T15">adv</ta>
            <ta e="T17" id="Seg_12290" s="T16">v</ta>
            <ta e="T18" id="Seg_12291" s="T17">adj</ta>
            <ta e="T19" id="Seg_12292" s="T18">n</ta>
            <ta e="T20" id="Seg_12293" s="T19">conj</ta>
            <ta e="T21" id="Seg_12294" s="T20">num</ta>
            <ta e="T22" id="Seg_12295" s="T21">num</ta>
            <ta e="T23" id="Seg_12296" s="T22">n</ta>
            <ta e="T24" id="Seg_12297" s="T23">n</ta>
            <ta e="T25" id="Seg_12298" s="T24">v</ta>
            <ta e="T26" id="Seg_12299" s="T25">n</ta>
            <ta e="T27" id="Seg_12300" s="T26">v</ta>
            <ta e="T28" id="Seg_12301" s="T27">n</ta>
            <ta e="T29" id="Seg_12302" s="T28">conj</ta>
            <ta e="T30" id="Seg_12303" s="T29">n</ta>
            <ta e="T31" id="Seg_12304" s="T30">conj</ta>
            <ta e="T32" id="Seg_12305" s="T31">n</ta>
            <ta e="T33" id="Seg_12306" s="T32">v</ta>
            <ta e="T34" id="Seg_12307" s="T33">n</ta>
            <ta e="T35" id="Seg_12308" s="T34">v</ta>
            <ta e="T36" id="Seg_12309" s="T35">conj</ta>
            <ta e="T37" id="Seg_12310" s="T36">adj</ta>
            <ta e="T38" id="Seg_12311" s="T37">n</ta>
            <ta e="T39" id="Seg_12312" s="T38">adv</ta>
            <ta e="T40" id="Seg_12313" s="T39">n</ta>
            <ta e="T41" id="Seg_12314" s="T40">v</ta>
            <ta e="T42" id="Seg_12315" s="T41">v</ta>
            <ta e="T43" id="Seg_12316" s="T42">pers</ta>
            <ta e="T44" id="Seg_12317" s="T43">n</ta>
            <ta e="T45" id="Seg_12318" s="T44">v</ta>
            <ta e="T46" id="Seg_12319" s="T45">conj</ta>
            <ta e="T47" id="Seg_12320" s="T46">n</ta>
            <ta e="T48" id="Seg_12321" s="T47">v</ta>
            <ta e="T49" id="Seg_12322" s="T48">pers</ta>
            <ta e="T50" id="Seg_12323" s="T49">adv</ta>
            <ta e="T51" id="Seg_12324" s="T50">v</ta>
            <ta e="T52" id="Seg_12325" s="T51">v</ta>
            <ta e="T53" id="Seg_12326" s="T52">conj</ta>
            <ta e="T54" id="Seg_12327" s="T53">pers</ta>
            <ta e="T55" id="Seg_12328" s="T54">adv</ta>
            <ta e="T56" id="Seg_12329" s="T55">n</ta>
            <ta e="T57" id="Seg_12330" s="T56">adj</ta>
            <ta e="T58" id="Seg_12331" s="T57">n</ta>
            <ta e="T59" id="Seg_12332" s="T58">v</ta>
            <ta e="T60" id="Seg_12333" s="T59">adj</ta>
            <ta e="T61" id="Seg_12334" s="T60">n</ta>
            <ta e="T62" id="Seg_12335" s="T61">n</ta>
            <ta e="T63" id="Seg_12336" s="T62">n</ta>
            <ta e="T64" id="Seg_12337" s="T63">v</ta>
            <ta e="T65" id="Seg_12338" s="T64">n</ta>
            <ta e="T66" id="Seg_12339" s="T65">pers</ta>
            <ta e="T67" id="Seg_12340" s="T66">v</ta>
            <ta e="T68" id="Seg_12341" s="T67">n</ta>
            <ta e="T69" id="Seg_12342" s="T68">n</ta>
            <ta e="T70" id="Seg_12343" s="T69">v</ta>
            <ta e="T71" id="Seg_12344" s="T70">conj</ta>
            <ta e="T72" id="Seg_12345" s="T71">v</ta>
            <ta e="T73" id="Seg_12346" s="T72">pers</ta>
            <ta e="T74" id="Seg_12347" s="T73">v</ta>
            <ta e="T75" id="Seg_12348" s="T74">num</ta>
            <ta e="T76" id="Seg_12349" s="T75">n</ta>
            <ta e="T77" id="Seg_12350" s="T76">num</ta>
            <ta e="T78" id="Seg_12351" s="T77">n</ta>
            <ta e="T79" id="Seg_12352" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_12353" s="T79">v</ta>
            <ta e="T81" id="Seg_12354" s="T80">pers</ta>
            <ta e="T82" id="Seg_12355" s="T81">adv</ta>
            <ta e="T83" id="Seg_12356" s="T82">n</ta>
            <ta e="T84" id="Seg_12357" s="T83">n</ta>
            <ta e="T85" id="Seg_12358" s="T84">quant</ta>
            <ta e="T86" id="Seg_12359" s="T85">n</ta>
            <ta e="T87" id="Seg_12360" s="T86">v</ta>
            <ta e="T88" id="Seg_12361" s="T87">conj</ta>
            <ta e="T89" id="Seg_12362" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_12363" s="T89">v</ta>
            <ta e="T91" id="Seg_12364" s="T90">n</ta>
            <ta e="T92" id="Seg_12365" s="T91">n</ta>
            <ta e="T93" id="Seg_12366" s="T92">conj</ta>
            <ta e="T94" id="Seg_12367" s="T93">adv</ta>
            <ta e="T95" id="Seg_12368" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_12369" s="T95">v</ta>
            <ta e="T97" id="Seg_12370" s="T96">conj</ta>
            <ta e="T98" id="Seg_12371" s="T97">pers</ta>
            <ta e="T99" id="Seg_12372" s="T98">conj</ta>
            <ta e="T100" id="Seg_12373" s="T99">ptcl</ta>
            <ta e="T101" id="Seg_12374" s="T100">v</ta>
            <ta e="T102" id="Seg_12375" s="T101">ptcl</ta>
            <ta e="T103" id="Seg_12376" s="T102">conj</ta>
            <ta e="T104" id="Seg_12377" s="T103">adv</ta>
            <ta e="T105" id="Seg_12378" s="T104">ptcl</ta>
            <ta e="T106" id="Seg_12379" s="T105">v</ta>
            <ta e="T107" id="Seg_12380" s="T106">conj</ta>
            <ta e="T108" id="Seg_12381" s="T107">pers</ta>
            <ta e="T109" id="Seg_12382" s="T108">adv</ta>
            <ta e="T110" id="Seg_12383" s="T109">adj</ta>
            <ta e="T111" id="Seg_12384" s="T110">n</ta>
            <ta e="T112" id="Seg_12385" s="T111">v</ta>
            <ta e="T113" id="Seg_12386" s="T112">quant</ta>
            <ta e="T114" id="Seg_12387" s="T113">v</ta>
            <ta e="T115" id="Seg_12388" s="T114">adv</ta>
            <ta e="T116" id="Seg_12389" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_12390" s="T116">conj</ta>
            <ta e="T118" id="Seg_12391" s="T117">n</ta>
            <ta e="T119" id="Seg_12392" s="T118">quant</ta>
            <ta e="T120" id="Seg_12393" s="T119">adv</ta>
            <ta e="T121" id="Seg_12394" s="T120">v</ta>
            <ta e="T122" id="Seg_12395" s="T121">n</ta>
            <ta e="T123" id="Seg_12396" s="T122">v</ta>
            <ta e="T124" id="Seg_12397" s="T123">adv</ta>
            <ta e="T125" id="Seg_12398" s="T124">n</ta>
            <ta e="T126" id="Seg_12399" s="T125">v</ta>
            <ta e="T127" id="Seg_12400" s="T126">conj</ta>
            <ta e="T128" id="Seg_12401" s="T127">pers</ta>
            <ta e="T129" id="Seg_12402" s="T128">n</ta>
            <ta e="T130" id="Seg_12403" s="T129">ptcl</ta>
            <ta e="T131" id="Seg_12404" s="T130">v</ta>
            <ta e="T132" id="Seg_12405" s="T131">n</ta>
            <ta e="T133" id="Seg_12406" s="T132">pro</ta>
            <ta e="T134" id="Seg_12407" s="T133">n</ta>
            <ta e="T135" id="Seg_12408" s="T134">adv</ta>
            <ta e="T136" id="Seg_12409" s="T135">ptcl</ta>
            <ta e="T137" id="Seg_12410" s="T136">v</ta>
            <ta e="T138" id="Seg_12411" s="T137">conj</ta>
            <ta e="T139" id="Seg_12412" s="T138">adv</ta>
            <ta e="T140" id="Seg_12413" s="T139">v</ta>
            <ta e="T141" id="Seg_12414" s="T140">ptcl</ta>
            <ta e="T142" id="Seg_12415" s="T141">v</ta>
            <ta e="T143" id="Seg_12416" s="T142">n</ta>
            <ta e="T144" id="Seg_12417" s="T143">v</ta>
            <ta e="T145" id="Seg_12418" s="T144">v</ta>
            <ta e="T146" id="Seg_12419" s="T145">conj</ta>
            <ta e="T147" id="Seg_12420" s="T146">ptcl</ta>
            <ta e="T148" id="Seg_12421" s="T147">v</ta>
            <ta e="T149" id="Seg_12422" s="T148">n</ta>
            <ta e="T150" id="Seg_12423" s="T149">conj</ta>
            <ta e="T151" id="Seg_12424" s="T150">n</ta>
            <ta e="T152" id="Seg_12425" s="T151">v</ta>
            <ta e="T153" id="Seg_12426" s="T152">interrog</ta>
            <ta e="T154" id="Seg_12427" s="T153">pers</ta>
            <ta e="T155" id="Seg_12428" s="T154">n</ta>
            <ta e="T156" id="Seg_12429" s="T155">v</ta>
            <ta e="T157" id="Seg_12430" s="T156">conj</ta>
            <ta e="T158" id="Seg_12431" s="T157">n</ta>
            <ta e="T159" id="Seg_12432" s="T158">n</ta>
            <ta e="T160" id="Seg_12433" s="T159">adv</ta>
            <ta e="T161" id="Seg_12434" s="T160">ptcl</ta>
            <ta e="T162" id="Seg_12435" s="T161">v</ta>
            <ta e="T163" id="Seg_12436" s="T162">pers</ta>
            <ta e="T164" id="Seg_12437" s="T163">adv</ta>
            <ta e="T165" id="Seg_12438" s="T164">num</ta>
            <ta e="T166" id="Seg_12439" s="T165">n</ta>
            <ta e="T167" id="Seg_12440" s="T166">n</ta>
            <ta e="T168" id="Seg_12441" s="T167">v</ta>
            <ta e="T169" id="Seg_12442" s="T168">v</ta>
            <ta e="T170" id="Seg_12443" s="T169">adv</ta>
            <ta e="T171" id="Seg_12444" s="T170">n</ta>
            <ta e="T172" id="Seg_12445" s="T171">v</ta>
            <ta e="T173" id="Seg_12446" s="T172">conj</ta>
            <ta e="T174" id="Seg_12447" s="T173">ptcl</ta>
            <ta e="T175" id="Seg_12448" s="T174">v</ta>
            <ta e="T176" id="Seg_12449" s="T175">conj</ta>
            <ta e="T177" id="Seg_12450" s="T176">v</ta>
            <ta e="T178" id="Seg_12451" s="T177">v</ta>
            <ta e="T179" id="Seg_12452" s="T178">conj</ta>
            <ta e="T180" id="Seg_12453" s="T179">v</ta>
            <ta e="T181" id="Seg_12454" s="T180">conj</ta>
            <ta e="T182" id="Seg_12455" s="T181">adv</ta>
            <ta e="T183" id="Seg_12456" s="T182">v</ta>
            <ta e="T184" id="Seg_12457" s="T183">pers</ta>
            <ta e="T185" id="Seg_12458" s="T184">n</ta>
            <ta e="T186" id="Seg_12459" s="T185">v</ta>
            <ta e="T187" id="Seg_12460" s="T186">conj</ta>
            <ta e="T188" id="Seg_12461" s="T187">n</ta>
            <ta e="T189" id="Seg_12462" s="T188">v</ta>
            <ta e="T190" id="Seg_12463" s="T189">conj</ta>
            <ta e="T191" id="Seg_12464" s="T190">pers</ta>
            <ta e="T192" id="Seg_12465" s="T191">adj</ta>
            <ta e="T193" id="Seg_12466" s="T192">n</ta>
            <ta e="T194" id="Seg_12467" s="T193">v</ta>
            <ta e="T195" id="Seg_12468" s="T194">adv</ta>
            <ta e="T196" id="Seg_12469" s="T195">n</ta>
            <ta e="T197" id="Seg_12470" s="T196">n</ta>
            <ta e="T198" id="Seg_12471" s="T197">v</ta>
            <ta e="T199" id="Seg_12472" s="T198">conj</ta>
            <ta e="T200" id="Seg_12473" s="T199">adv</ta>
            <ta e="T201" id="Seg_12474" s="T200">v</ta>
            <ta e="T202" id="Seg_12475" s="T201">conj</ta>
            <ta e="T203" id="Seg_12476" s="T202">num</ta>
            <ta e="T204" id="Seg_12477" s="T203">n</ta>
            <ta e="T205" id="Seg_12478" s="T204">v</ta>
            <ta e="T206" id="Seg_12479" s="T205">n</ta>
            <ta e="T207" id="Seg_12480" s="T206">v</ta>
            <ta e="T208" id="Seg_12481" s="T207">n</ta>
            <ta e="T209" id="Seg_12482" s="T208">conj</ta>
            <ta e="T210" id="Seg_12483" s="T209">v</ta>
            <ta e="T211" id="Seg_12484" s="T210">interrog</ta>
            <ta e="T212" id="Seg_12485" s="T211">n</ta>
            <ta e="T213" id="Seg_12486" s="T212">v</ta>
            <ta e="T214" id="Seg_12487" s="T213">n</ta>
            <ta e="T215" id="Seg_12488" s="T214">v</ta>
            <ta e="T216" id="Seg_12489" s="T215">n</ta>
            <ta e="T217" id="Seg_12490" s="T216">n</ta>
            <ta e="T218" id="Seg_12491" s="T217">v</ta>
            <ta e="T219" id="Seg_12492" s="T218">pers</ta>
            <ta e="T220" id="Seg_12493" s="T219">v</ta>
            <ta e="T221" id="Seg_12494" s="T220">pro</ta>
            <ta e="T222" id="Seg_12495" s="T221">n</ta>
            <ta e="T223" id="Seg_12496" s="T222">pers</ta>
            <ta e="T224" id="Seg_12497" s="T223">n</ta>
            <ta e="T225" id="Seg_12498" s="T224">v</ta>
            <ta e="T226" id="Seg_12499" s="T225">adv</ta>
            <ta e="T227" id="Seg_12500" s="T226">pers</ta>
            <ta e="T228" id="Seg_12501" s="T227">dem</ta>
            <ta e="T229" id="Seg_12502" s="T228">n</ta>
            <ta e="T230" id="Seg_12503" s="T229">ptcl</ta>
            <ta e="T231" id="Seg_12504" s="T230">v</ta>
            <ta e="T232" id="Seg_12505" s="T231">dem</ta>
            <ta e="T233" id="Seg_12506" s="T232">n</ta>
            <ta e="T234" id="Seg_12507" s="T233">dem</ta>
            <ta e="T235" id="Seg_12508" s="T234">n</ta>
            <ta e="T236" id="Seg_12509" s="T235">dem</ta>
            <ta e="T237" id="Seg_12510" s="T236">n</ta>
            <ta e="T238" id="Seg_12511" s="T237">conj</ta>
            <ta e="T239" id="Seg_12512" s="T238">v</ta>
            <ta e="T240" id="Seg_12513" s="T239">n</ta>
            <ta e="T241" id="Seg_12514" s="T240">v</ta>
            <ta e="T242" id="Seg_12515" s="T241">pers</ta>
            <ta e="T243" id="Seg_12516" s="T242">v</ta>
            <ta e="T244" id="Seg_12517" s="T243">adv</ta>
            <ta e="T245" id="Seg_12518" s="T244">conj</ta>
            <ta e="T246" id="Seg_12519" s="T245">pers</ta>
            <ta e="T247" id="Seg_12520" s="T246">v</ta>
            <ta e="T248" id="Seg_12521" s="T247">v</ta>
            <ta e="T249" id="Seg_12522" s="T248">interrog</ta>
            <ta e="T250" id="Seg_12523" s="T249">v</ta>
            <ta e="T251" id="Seg_12524" s="T250">adv</ta>
            <ta e="T252" id="Seg_12525" s="T251">ptcl</ta>
            <ta e="T253" id="Seg_12526" s="T252">v</ta>
            <ta e="T254" id="Seg_12527" s="T253">v</ta>
            <ta e="T255" id="Seg_12528" s="T254">v</ta>
            <ta e="T256" id="Seg_12529" s="T255">n</ta>
            <ta e="T257" id="Seg_12530" s="T256">interj</ta>
            <ta e="T258" id="Seg_12531" s="T257">n</ta>
            <ta e="T259" id="Seg_12532" s="T258">v</ta>
            <ta e="T260" id="Seg_12533" s="T259">interj</ta>
            <ta e="T261" id="Seg_12534" s="T260">interj</ta>
            <ta e="T262" id="Seg_12535" s="T261">n</ta>
            <ta e="T263" id="Seg_12536" s="T262">adv</ta>
            <ta e="T264" id="Seg_12537" s="T263">v</ta>
            <ta e="T265" id="Seg_12538" s="T264">v</ta>
            <ta e="T266" id="Seg_12539" s="T265">v</ta>
            <ta e="T267" id="Seg_12540" s="T266">adv</ta>
            <ta e="T268" id="Seg_12541" s="T267">conj</ta>
            <ta e="T269" id="Seg_12542" s="T268">pers</ta>
            <ta e="T270" id="Seg_12543" s="T269">v</ta>
            <ta e="T271" id="Seg_12544" s="T270">adv</ta>
            <ta e="T272" id="Seg_12545" s="T271">ptcl</ta>
            <ta e="T273" id="Seg_12546" s="T272">v</ta>
            <ta e="T274" id="Seg_12547" s="T273">n</ta>
            <ta e="T275" id="Seg_12548" s="T274">v</ta>
            <ta e="T276" id="Seg_12549" s="T275">pers</ta>
            <ta e="T277" id="Seg_12550" s="T276">pers</ta>
            <ta e="T278" id="Seg_12551" s="T277">v</ta>
            <ta e="T279" id="Seg_12552" s="T278">conj</ta>
            <ta e="T280" id="Seg_12553" s="T279">pers</ta>
            <ta e="T281" id="Seg_12554" s="T280">n</ta>
            <ta e="T282" id="Seg_12555" s="T281">n</ta>
            <ta e="T283" id="Seg_12556" s="T282">pers</ta>
            <ta e="T284" id="Seg_12557" s="T283">pers</ta>
            <ta e="T285" id="Seg_12558" s="T284">v</ta>
            <ta e="T286" id="Seg_12559" s="T285">conj</ta>
            <ta e="T287" id="Seg_12560" s="T286">pers</ta>
            <ta e="T288" id="Seg_12561" s="T287">v</ta>
            <ta e="T289" id="Seg_12562" s="T288">conj</ta>
            <ta e="T290" id="Seg_12563" s="T289">pers</ta>
            <ta e="T291" id="Seg_12564" s="T290">pers</ta>
            <ta e="T292" id="Seg_12565" s="T291">n</ta>
            <ta e="T293" id="Seg_12566" s="T292">v</ta>
            <ta e="T294" id="Seg_12567" s="T293">n</ta>
            <ta e="T295" id="Seg_12568" s="T294">v</ta>
            <ta e="T296" id="Seg_12569" s="T295">conj</ta>
            <ta e="T299" id="Seg_12570" s="T298">pers</ta>
            <ta e="T300" id="Seg_12571" s="T299">dem</ta>
            <ta e="T301" id="Seg_12572" s="T300">n</ta>
            <ta e="T302" id="Seg_12573" s="T301">ptcl</ta>
            <ta e="T303" id="Seg_12574" s="T302">v</ta>
            <ta e="T304" id="Seg_12575" s="T303">pers</ta>
            <ta e="T305" id="Seg_12576" s="T304">ptcl</ta>
            <ta e="T306" id="Seg_12577" s="T305">v</ta>
            <ta e="T307" id="Seg_12578" s="T306">n</ta>
            <ta e="T308" id="Seg_12579" s="T307">pers</ta>
            <ta e="T309" id="Seg_12580" s="T308">v</ta>
            <ta e="T310" id="Seg_12581" s="T309">v</ta>
            <ta e="T311" id="Seg_12582" s="T310">pers</ta>
            <ta e="T312" id="Seg_12583" s="T311">v</ta>
            <ta e="T313" id="Seg_12584" s="T312">interrog</ta>
            <ta e="T314" id="Seg_12585" s="T313">adv</ta>
            <ta e="T315" id="Seg_12586" s="T314">v</ta>
            <ta e="T316" id="Seg_12587" s="T315">n</ta>
            <ta e="T317" id="Seg_12588" s="T316">adv</ta>
            <ta e="T318" id="Seg_12589" s="T317">v</ta>
            <ta e="T319" id="Seg_12590" s="T318">conj</ta>
            <ta e="T320" id="Seg_12591" s="T319">pers</ta>
            <ta e="T321" id="Seg_12592" s="T320">pers</ta>
            <ta e="T322" id="Seg_12593" s="T321">adv</ta>
            <ta e="T323" id="Seg_12594" s="T322">v</ta>
            <ta e="T324" id="Seg_12595" s="T323">n</ta>
            <ta e="T325" id="Seg_12596" s="T324">adv</ta>
            <ta e="T326" id="Seg_12597" s="T325">v</ta>
            <ta e="T327" id="Seg_12598" s="T326">pers</ta>
            <ta e="T328" id="Seg_12599" s="T327">dem</ta>
            <ta e="T329" id="Seg_12600" s="T328">n</ta>
            <ta e="T330" id="Seg_12601" s="T329">ptcl</ta>
            <ta e="T331" id="Seg_12602" s="T330">v</ta>
            <ta e="T332" id="Seg_12603" s="T331">pers</ta>
            <ta e="T333" id="Seg_12604" s="T332">conj</ta>
            <ta e="T334" id="Seg_12605" s="T333">ptcl</ta>
            <ta e="T335" id="Seg_12606" s="T334">v</ta>
            <ta e="T336" id="Seg_12607" s="T335">conj</ta>
            <ta e="T337" id="Seg_12608" s="T336">pers</ta>
            <ta e="T338" id="Seg_12609" s="T337">adv</ta>
            <ta e="T339" id="Seg_12610" s="T338">v</ta>
            <ta e="T340" id="Seg_12611" s="T339">pers</ta>
            <ta e="T341" id="Seg_12612" s="T340">v</ta>
            <ta e="T342" id="Seg_12613" s="T341">pers</ta>
            <ta e="T343" id="Seg_12614" s="T342">v</ta>
            <ta e="T344" id="Seg_12615" s="T343">pers</ta>
            <ta e="T345" id="Seg_12616" s="T344">conj</ta>
            <ta e="T346" id="Seg_12617" s="T345">v</ta>
            <ta e="T347" id="Seg_12618" s="T346">pers</ta>
            <ta e="T348" id="Seg_12619" s="T347">v</ta>
            <ta e="T349" id="Seg_12620" s="T348">n</ta>
            <ta e="T350" id="Seg_12621" s="T349">v</ta>
            <ta e="T351" id="Seg_12622" s="T350">pers</ta>
            <ta e="T352" id="Seg_12623" s="T351">pers</ta>
            <ta e="T353" id="Seg_12624" s="T352">v</ta>
            <ta e="T354" id="Seg_12625" s="T353">v</ta>
            <ta e="T355" id="Seg_12626" s="T354">v</ta>
            <ta e="T356" id="Seg_12627" s="T355">n</ta>
            <ta e="T357" id="Seg_12628" s="T356">v</ta>
            <ta e="T358" id="Seg_12629" s="T357">pers</ta>
            <ta e="T359" id="Seg_12630" s="T358">v</ta>
            <ta e="T360" id="Seg_12631" s="T359">v</ta>
            <ta e="T361" id="Seg_12632" s="T360">ptcl</ta>
            <ta e="T362" id="Seg_12633" s="T361">ptcl</ta>
            <ta e="T363" id="Seg_12634" s="T362">ptcl</ta>
            <ta e="T364" id="Seg_12635" s="T363">v</ta>
            <ta e="T365" id="Seg_12636" s="T364">ptcl</ta>
            <ta e="T366" id="Seg_12637" s="T365">adv</ta>
            <ta e="T367" id="Seg_12638" s="T366">conj</ta>
            <ta e="T368" id="Seg_12639" s="T367">emphpro</ta>
            <ta e="T369" id="Seg_12640" s="T368">v</ta>
            <ta e="T370" id="Seg_12641" s="T369">conj</ta>
            <ta e="T371" id="Seg_12642" s="T370">adj</ta>
            <ta e="T372" id="Seg_12643" s="T371">pers</ta>
            <ta e="T373" id="Seg_12644" s="T372">v</ta>
            <ta e="T374" id="Seg_12645" s="T373">ptcl</ta>
            <ta e="T375" id="Seg_12646" s="T374">conj</ta>
            <ta e="T376" id="Seg_12647" s="T375">v</ta>
            <ta e="T377" id="Seg_12648" s="T376">adv</ta>
            <ta e="T378" id="Seg_12649" s="T377">interrog</ta>
            <ta e="T379" id="Seg_12650" s="T378">adv</ta>
            <ta e="T380" id="Seg_12651" s="T379">v</ta>
            <ta e="T381" id="Seg_12652" s="T380">n</ta>
            <ta e="T382" id="Seg_12653" s="T381">v</ta>
            <ta e="T383" id="Seg_12654" s="T382">adv</ta>
            <ta e="T384" id="Seg_12655" s="T383">n</ta>
            <ta e="T385" id="Seg_12656" s="T384">adv</ta>
            <ta e="T386" id="Seg_12657" s="T385">v</ta>
            <ta e="T387" id="Seg_12658" s="T386">n</ta>
            <ta e="T388" id="Seg_12659" s="T387">ptcl</ta>
            <ta e="T389" id="Seg_12660" s="T388">v</ta>
            <ta e="T390" id="Seg_12661" s="T389">n</ta>
            <ta e="T391" id="Seg_12662" s="T390">ptcl</ta>
            <ta e="T392" id="Seg_12663" s="T391">v</ta>
            <ta e="T393" id="Seg_12664" s="T392">adv</ta>
            <ta e="T394" id="Seg_12665" s="T393">conj</ta>
            <ta e="T395" id="Seg_12666" s="T394">conj</ta>
            <ta e="T396" id="Seg_12667" s="T395">pers</ta>
            <ta e="T397" id="Seg_12668" s="T396">v</ta>
            <ta e="T398" id="Seg_12669" s="T397">adv</ta>
            <ta e="T399" id="Seg_12670" s="T398">pers</ta>
            <ta e="T400" id="Seg_12671" s="T399">ptcl</ta>
            <ta e="T401" id="Seg_12672" s="T400">pers</ta>
            <ta e="T402" id="Seg_12673" s="T401">n</ta>
            <ta e="T403" id="Seg_12674" s="T402">v</ta>
            <ta e="T404" id="Seg_12675" s="T403">conj</ta>
            <ta e="T405" id="Seg_12676" s="T404">adv</ta>
            <ta e="T406" id="Seg_12677" s="T405">pers</ta>
            <ta e="T407" id="Seg_12678" s="T406">pers</ta>
            <ta e="T408" id="Seg_12679" s="T407">n</ta>
            <ta e="T409" id="Seg_12680" s="T408">v</ta>
            <ta e="T410" id="Seg_12681" s="T409">n</ta>
            <ta e="T411" id="Seg_12682" s="T410">v</ta>
            <ta e="T412" id="Seg_12683" s="T411">v</ta>
            <ta e="T413" id="Seg_12684" s="T412">pers</ta>
            <ta e="T414" id="Seg_12685" s="T413">v</ta>
            <ta e="T415" id="Seg_12686" s="T414">n</ta>
            <ta e="T416" id="Seg_12687" s="T415">adv</ta>
            <ta e="T417" id="Seg_12688" s="T416">v</ta>
            <ta e="T418" id="Seg_12689" s="T417">n</ta>
            <ta e="T419" id="Seg_12690" s="T418">quant</ta>
            <ta e="T420" id="Seg_12691" s="T419">v</ta>
            <ta e="T421" id="Seg_12692" s="T420">ptcl</ta>
            <ta e="T422" id="Seg_12693" s="T421">conj</ta>
            <ta e="T423" id="Seg_12694" s="T422">v</ta>
            <ta e="T424" id="Seg_12695" s="T423">v</ta>
            <ta e="T425" id="Seg_12696" s="T424">n</ta>
            <ta e="T426" id="Seg_12697" s="T425">v</ta>
            <ta e="T427" id="Seg_12698" s="T426">n</ta>
            <ta e="T428" id="Seg_12699" s="T427">quant</ta>
            <ta e="T429" id="Seg_12700" s="T428">v</ta>
            <ta e="T430" id="Seg_12701" s="T429">n</ta>
            <ta e="T431" id="Seg_12702" s="T430">pro</ta>
            <ta e="T432" id="Seg_12703" s="T431">n</ta>
            <ta e="T433" id="Seg_12704" s="T432">v</ta>
            <ta e="T434" id="Seg_12705" s="T433">n</ta>
            <ta e="T435" id="Seg_12706" s="T434">n</ta>
            <ta e="T436" id="Seg_12707" s="T435">v</ta>
            <ta e="T437" id="Seg_12708" s="T436">n</ta>
            <ta e="T438" id="Seg_12709" s="T437">adv</ta>
            <ta e="T439" id="Seg_12710" s="T438">v</ta>
            <ta e="T440" id="Seg_12711" s="T439">conj</ta>
            <ta e="T441" id="Seg_12712" s="T440">ptcl</ta>
            <ta e="T442" id="Seg_12713" s="T441">v</ta>
            <ta e="T443" id="Seg_12714" s="T442">v</ta>
            <ta e="T444" id="Seg_12715" s="T443">n</ta>
            <ta e="T445" id="Seg_12716" s="T444">v</ta>
            <ta e="T446" id="Seg_12717" s="T445">n</ta>
            <ta e="T447" id="Seg_12718" s="T446">ptcl</ta>
            <ta e="T448" id="Seg_12719" s="T447">v</ta>
            <ta e="T449" id="Seg_12720" s="T448">ptcl</ta>
            <ta e="T450" id="Seg_12721" s="T449">interrog</ta>
            <ta e="T451" id="Seg_12722" s="T450">v</ta>
            <ta e="T452" id="Seg_12723" s="T451">v</ta>
            <ta e="T453" id="Seg_12724" s="T452">pers</ta>
            <ta e="T454" id="Seg_12725" s="T453">pers</ta>
            <ta e="T455" id="Seg_12726" s="T454">v</ta>
            <ta e="T456" id="Seg_12727" s="T455">pers</ta>
            <ta e="T457" id="Seg_12728" s="T456">n</ta>
            <ta e="T458" id="Seg_12729" s="T457">v</ta>
            <ta e="T459" id="Seg_12730" s="T458">dem</ta>
            <ta e="T460" id="Seg_12731" s="T459">n</ta>
            <ta e="T461" id="Seg_12732" s="T460">quant</ta>
            <ta e="T462" id="Seg_12733" s="T461">v</ta>
            <ta e="T463" id="Seg_12734" s="T462">conj</ta>
            <ta e="T464" id="Seg_12735" s="T463">adv</ta>
            <ta e="T465" id="Seg_12736" s="T464">pers</ta>
            <ta e="T466" id="Seg_12737" s="T465">pers</ta>
            <ta e="T467" id="Seg_12738" s="T466">v</ta>
            <ta e="T468" id="Seg_12739" s="T467">n</ta>
            <ta e="T469" id="Seg_12740" s="T468">n</ta>
            <ta e="T470" id="Seg_12741" s="T469">v</ta>
            <ta e="T471" id="Seg_12742" s="T470">n</ta>
            <ta e="T472" id="Seg_12743" s="T471">ptcl</ta>
            <ta e="T473" id="Seg_12744" s="T472">v</ta>
            <ta e="T474" id="Seg_12745" s="T473">adv</ta>
            <ta e="T475" id="Seg_12746" s="T474">v</ta>
            <ta e="T476" id="Seg_12747" s="T475">v</ta>
            <ta e="T477" id="Seg_12748" s="T476">interrog</ta>
            <ta e="T478" id="Seg_12749" s="T477">adv</ta>
            <ta e="T479" id="Seg_12750" s="T478">v</ta>
            <ta e="T480" id="Seg_12751" s="T479">n</ta>
            <ta e="T481" id="Seg_12752" s="T480">adv</ta>
            <ta e="T482" id="Seg_12753" s="T481">v</ta>
            <ta e="T483" id="Seg_12754" s="T482">n</ta>
            <ta e="T484" id="Seg_12755" s="T483">pers</ta>
            <ta e="T485" id="Seg_12756" s="T484">v</ta>
            <ta e="T486" id="Seg_12757" s="T485">n</ta>
            <ta e="T487" id="Seg_12758" s="T486">v</ta>
            <ta e="T488" id="Seg_12759" s="T487">n</ta>
            <ta e="T489" id="Seg_12760" s="T488">v</ta>
            <ta e="T490" id="Seg_12761" s="T489">n</ta>
            <ta e="T491" id="Seg_12762" s="T490">v</ta>
            <ta e="T492" id="Seg_12763" s="T491">n</ta>
            <ta e="T493" id="Seg_12764" s="T492">v</ta>
            <ta e="T494" id="Seg_12765" s="T493">n</ta>
            <ta e="T495" id="Seg_12766" s="T494">quant</ta>
            <ta e="T496" id="Seg_12767" s="T495">v</ta>
            <ta e="T497" id="Seg_12768" s="T496">conj</ta>
            <ta e="T498" id="Seg_12769" s="T497">n</ta>
            <ta e="T499" id="Seg_12770" s="T498">v</ta>
            <ta e="T500" id="Seg_12771" s="T499">pers</ta>
            <ta e="T501" id="Seg_12772" s="T500">pers</ta>
            <ta e="T502" id="Seg_12773" s="T501">v</ta>
            <ta e="T503" id="Seg_12774" s="T502">v</ta>
            <ta e="T504" id="Seg_12775" s="T503">v</ta>
            <ta e="T505" id="Seg_12776" s="T504">n</ta>
            <ta e="T506" id="Seg_12777" s="T505">v</ta>
            <ta e="T507" id="Seg_12778" s="T506">pers</ta>
            <ta e="T508" id="Seg_12779" s="T507">v</ta>
            <ta e="T509" id="Seg_12780" s="T508">n</ta>
            <ta e="T510" id="Seg_12781" s="T509">v</ta>
            <ta e="T511" id="Seg_12782" s="T510">v</ta>
            <ta e="T514" id="Seg_12783" s="T513">interrog</ta>
            <ta e="T515" id="Seg_12784" s="T514">interrog</ta>
            <ta e="T516" id="Seg_12785" s="T515">adv</ta>
            <ta e="T517" id="Seg_12786" s="T516">v</ta>
            <ta e="T518" id="Seg_12787" s="T517">n</ta>
            <ta e="T519" id="Seg_12788" s="T518">adv</ta>
            <ta e="T520" id="Seg_12789" s="T519">v</ta>
            <ta e="T521" id="Seg_12790" s="T520">conj</ta>
            <ta e="T522" id="Seg_12791" s="T521">pers</ta>
            <ta e="T523" id="Seg_12792" s="T522">v</ta>
            <ta e="T524" id="Seg_12793" s="T523">adv</ta>
            <ta e="T525" id="Seg_12794" s="T524">pers</ta>
            <ta e="T526" id="Seg_12795" s="T525">n</ta>
            <ta e="T527" id="Seg_12796" s="T526">v</ta>
            <ta e="T528" id="Seg_12797" s="T527">v</ta>
            <ta e="T529" id="Seg_12798" s="T528">pers</ta>
            <ta e="T530" id="Seg_12799" s="T529">v</ta>
            <ta e="T531" id="Seg_12800" s="T530">adv</ta>
            <ta e="T532" id="Seg_12801" s="T531">n</ta>
            <ta e="T533" id="Seg_12802" s="T532">adv</ta>
            <ta e="T534" id="Seg_12803" s="T533">v</ta>
            <ta e="T535" id="Seg_12804" s="T534">ptcl</ta>
            <ta e="T536" id="Seg_12805" s="T535">adv</ta>
            <ta e="T537" id="Seg_12806" s="T536">n</ta>
            <ta e="T538" id="Seg_12807" s="T537">v</ta>
            <ta e="T539" id="Seg_12808" s="T538">pers</ta>
            <ta e="T540" id="Seg_12809" s="T539">v</ta>
            <ta e="T541" id="Seg_12810" s="T540">n</ta>
            <ta e="T542" id="Seg_12811" s="T541">pers</ta>
            <ta e="T543" id="Seg_12812" s="T542">adv</ta>
            <ta e="T544" id="Seg_12813" s="T543">v</ta>
            <ta e="T545" id="Seg_12814" s="T544">n</ta>
            <ta e="T546" id="Seg_12815" s="T545">v</ta>
            <ta e="T547" id="Seg_12816" s="T546">v</ta>
            <ta e="T549" id="Seg_12817" s="T548">n</ta>
            <ta e="T550" id="Seg_12818" s="T549">v</ta>
            <ta e="T551" id="Seg_12819" s="T550">n</ta>
            <ta e="T552" id="Seg_12820" s="T551">n</ta>
            <ta e="T553" id="Seg_12821" s="T552">v</ta>
            <ta e="T554" id="Seg_12822" s="T553">n</ta>
            <ta e="T555" id="Seg_12823" s="T554">v</ta>
            <ta e="T556" id="Seg_12824" s="T555">conj</ta>
            <ta e="T557" id="Seg_12825" s="T556">n</ta>
            <ta e="T558" id="Seg_12826" s="T557">ptcl</ta>
            <ta e="T559" id="Seg_12827" s="T558">pro</ta>
            <ta e="T560" id="Seg_12828" s="T559">n</ta>
            <ta e="T561" id="Seg_12829" s="T560">adv</ta>
            <ta e="T562" id="Seg_12830" s="T561">v</ta>
            <ta e="T563" id="Seg_12831" s="T562">conj</ta>
            <ta e="T564" id="Seg_12832" s="T563">ptcl</ta>
            <ta e="T565" id="Seg_12833" s="T564">v</ta>
            <ta e="T566" id="Seg_12834" s="T565">v</ta>
            <ta e="T567" id="Seg_12835" s="T566">n</ta>
            <ta e="T568" id="Seg_12836" s="T567">v</ta>
            <ta e="T569" id="Seg_12837" s="T568">n</ta>
            <ta e="T570" id="Seg_12838" s="T569">v</ta>
            <ta e="T571" id="Seg_12839" s="T570">n</ta>
            <ta e="T572" id="Seg_12840" s="T571">v</ta>
            <ta e="T573" id="Seg_12841" s="T572">n</ta>
            <ta e="T574" id="Seg_12842" s="T573">n</ta>
            <ta e="T575" id="Seg_12843" s="T574">quant</ta>
            <ta e="T576" id="Seg_12844" s="T575">v</ta>
            <ta e="T577" id="Seg_12845" s="T576">pers</ta>
            <ta e="T578" id="Seg_12846" s="T577">n</ta>
            <ta e="T579" id="Seg_12847" s="T578">v</ta>
            <ta e="T580" id="Seg_12848" s="T579">conj</ta>
            <ta e="T581" id="Seg_12849" s="T580">pers</ta>
            <ta e="T582" id="Seg_12850" s="T581">adv</ta>
            <ta e="T583" id="Seg_12851" s="T582">pers</ta>
            <ta e="T584" id="Seg_12852" s="T583">v</ta>
            <ta e="T585" id="Seg_12853" s="T584">n</ta>
            <ta e="T586" id="Seg_12854" s="T585">n</ta>
            <ta e="T587" id="Seg_12855" s="T586">conj</ta>
            <ta e="T590" id="Seg_12856" s="T589">adv</ta>
            <ta e="T591" id="Seg_12857" s="T590">v</ta>
            <ta e="T592" id="Seg_12858" s="T591">adv</ta>
            <ta e="T593" id="Seg_12859" s="T592">ptcl</ta>
            <ta e="T594" id="Seg_12860" s="T593">v</ta>
            <ta e="T595" id="Seg_12861" s="T594">pers</ta>
            <ta e="T596" id="Seg_12862" s="T595">adv</ta>
            <ta e="T597" id="Seg_12863" s="T596">v</ta>
            <ta e="T598" id="Seg_12864" s="T597">v</ta>
            <ta e="T599" id="Seg_12865" s="T598">v</ta>
            <ta e="T600" id="Seg_12866" s="T599">interrog</ta>
            <ta e="T601" id="Seg_12867" s="T600">adv</ta>
            <ta e="T602" id="Seg_12868" s="T601">v</ta>
            <ta e="T603" id="Seg_12869" s="T602">conj</ta>
            <ta e="T604" id="Seg_12870" s="T603">n</ta>
            <ta e="T605" id="Seg_12871" s="T604">adv</ta>
            <ta e="T606" id="Seg_12872" s="T605">v</ta>
            <ta e="T607" id="Seg_12873" s="T606">n</ta>
            <ta e="T608" id="Seg_12874" s="T607">v</ta>
            <ta e="T609" id="Seg_12875" s="T608">pers</ta>
            <ta e="T610" id="Seg_12876" s="T609">pers</ta>
            <ta e="T611" id="Seg_12877" s="T610">v</ta>
            <ta e="T612" id="Seg_12878" s="T611">v</ta>
            <ta e="T613" id="Seg_12879" s="T612">v</ta>
            <ta e="T614" id="Seg_12880" s="T613">n</ta>
            <ta e="T615" id="Seg_12881" s="T614">v</ta>
            <ta e="T616" id="Seg_12882" s="T615">pers</ta>
            <ta e="T617" id="Seg_12883" s="T616">v</ta>
            <ta e="T618" id="Seg_12884" s="T617">ptcl</ta>
            <ta e="T619" id="Seg_12885" s="T618">v</ta>
            <ta e="T620" id="Seg_12886" s="T619">ptcl</ta>
            <ta e="T621" id="Seg_12887" s="T620">v</ta>
            <ta e="T622" id="Seg_12888" s="T621">v</ta>
            <ta e="T623" id="Seg_12889" s="T622">ptcl</ta>
            <ta e="T624" id="Seg_12890" s="T623">adv</ta>
            <ta e="T625" id="Seg_12891" s="T624">interrog</ta>
            <ta e="T626" id="Seg_12892" s="T625">adv</ta>
            <ta e="T627" id="Seg_12893" s="T626">v</ta>
            <ta e="T628" id="Seg_12894" s="T627">n</ta>
            <ta e="T629" id="Seg_12895" s="T628">adv</ta>
            <ta e="T630" id="Seg_12896" s="T629">v</ta>
            <ta e="T631" id="Seg_12897" s="T630">conj</ta>
            <ta e="T632" id="Seg_12898" s="T631">pers</ta>
            <ta e="T633" id="Seg_12899" s="T632">v</ta>
            <ta e="T634" id="Seg_12900" s="T633">pers</ta>
            <ta e="T635" id="Seg_12901" s="T634">n</ta>
            <ta e="T636" id="Seg_12902" s="T635">v</ta>
            <ta e="T637" id="Seg_12903" s="T636">adv</ta>
            <ta e="T638" id="Seg_12904" s="T637">conj</ta>
            <ta e="T639" id="Seg_12905" s="T638">v</ta>
            <ta e="T640" id="Seg_12906" s="T639">adv</ta>
            <ta e="T641" id="Seg_12907" s="T640">n</ta>
            <ta e="T642" id="Seg_12908" s="T641">adv</ta>
            <ta e="T643" id="Seg_12909" s="T642">v</ta>
            <ta e="T644" id="Seg_12910" s="T643">n</ta>
            <ta e="T645" id="Seg_12911" s="T644">adv</ta>
            <ta e="T646" id="Seg_12912" s="T645">v</ta>
            <ta e="T647" id="Seg_12913" s="T646">pers</ta>
            <ta e="T648" id="Seg_12914" s="T647">adv</ta>
            <ta e="T649" id="Seg_12915" s="T648">v</ta>
            <ta e="T650" id="Seg_12916" s="T649">n</ta>
            <ta e="T651" id="Seg_12917" s="T650">ptcl</ta>
            <ta e="T652" id="Seg_12918" s="T651">v</ta>
            <ta e="T653" id="Seg_12919" s="T652">n</ta>
            <ta e="T654" id="Seg_12920" s="T653">v</ta>
            <ta e="T655" id="Seg_12921" s="T654">v</ta>
            <ta e="T656" id="Seg_12922" s="T655">n</ta>
            <ta e="T657" id="Seg_12923" s="T656">n</ta>
            <ta e="T658" id="Seg_12924" s="T657">v</ta>
            <ta e="T659" id="Seg_12925" s="T658">v</ta>
            <ta e="T660" id="Seg_12926" s="T659">pers</ta>
            <ta e="T661" id="Seg_12927" s="T660">adv</ta>
            <ta e="T662" id="Seg_12928" s="T661">v</ta>
            <ta e="T663" id="Seg_12929" s="T662">ptcl</ta>
            <ta e="T664" id="Seg_12930" s="T663">v</ta>
            <ta e="T665" id="Seg_12931" s="T664">v</ta>
            <ta e="T666" id="Seg_12932" s="T665">v</ta>
            <ta e="T667" id="Seg_12933" s="T666">ptcl</ta>
            <ta e="T668" id="Seg_12934" s="T667">v</ta>
            <ta e="T669" id="Seg_12935" s="T668">adv</ta>
            <ta e="T670" id="Seg_12936" s="T669">adv</ta>
            <ta e="T671" id="Seg_12937" s="T670">v</ta>
            <ta e="T672" id="Seg_12938" s="T671">conj</ta>
            <ta e="T673" id="Seg_12939" s="T672">ptcl</ta>
            <ta e="T674" id="Seg_12940" s="T673">v</ta>
            <ta e="T675" id="Seg_12941" s="T674">v</ta>
            <ta e="T676" id="Seg_12942" s="T675">conj</ta>
            <ta e="T677" id="Seg_12943" s="T676">dem</ta>
            <ta e="T678" id="Seg_12944" s="T677">n</ta>
            <ta e="T679" id="Seg_12945" s="T678">ptcl</ta>
            <ta e="T680" id="Seg_12946" s="T679">v</ta>
            <ta e="T681" id="Seg_12947" s="T680">v</ta>
            <ta e="T682" id="Seg_12948" s="T681">n</ta>
            <ta e="T683" id="Seg_12949" s="T682">n</ta>
            <ta e="T684" id="Seg_12950" s="T683">ptcl</ta>
            <ta e="T685" id="Seg_12951" s="T684">v</ta>
            <ta e="T686" id="Seg_12952" s="T685">adv</ta>
            <ta e="T687" id="Seg_12953" s="T686">v</ta>
            <ta e="T688" id="Seg_12954" s="T687">conj</ta>
            <ta e="T689" id="Seg_12955" s="T688">n</ta>
            <ta e="T690" id="Seg_12956" s="T689">v</ta>
            <ta e="T691" id="Seg_12957" s="T690">n</ta>
            <ta e="T692" id="Seg_12958" s="T691">v</ta>
            <ta e="T693" id="Seg_12959" s="T692">adv</ta>
            <ta e="T694" id="Seg_12960" s="T693">n</ta>
            <ta e="T695" id="Seg_12961" s="T694">v</ta>
            <ta e="T696" id="Seg_12962" s="T695">v</ta>
            <ta e="T697" id="Seg_12963" s="T696">n</ta>
            <ta e="T698" id="Seg_12964" s="T697">n</ta>
            <ta e="T699" id="Seg_12965" s="T698">n</ta>
            <ta e="T700" id="Seg_12966" s="T699">pers</ta>
            <ta e="T701" id="Seg_12967" s="T700">n</ta>
            <ta e="T702" id="Seg_12968" s="T701">n</ta>
            <ta e="T703" id="Seg_12969" s="T702">n</ta>
            <ta e="T704" id="Seg_12970" s="T703">n</ta>
            <ta e="T705" id="Seg_12971" s="T704">v</ta>
            <ta e="T706" id="Seg_12972" s="T705">n</ta>
            <ta e="T707" id="Seg_12973" s="T706">v</ta>
            <ta e="T708" id="Seg_12974" s="T707">n</ta>
            <ta e="T709" id="Seg_12975" s="T708">v</ta>
            <ta e="T710" id="Seg_12976" s="T709">n</ta>
            <ta e="T711" id="Seg_12977" s="T710">n</ta>
            <ta e="T712" id="Seg_12978" s="T711">adv</ta>
            <ta e="T713" id="Seg_12979" s="T712">ptcl</ta>
            <ta e="T714" id="Seg_12980" s="T713">v</ta>
            <ta e="T715" id="Seg_12981" s="T714">pers</ta>
            <ta e="T716" id="Seg_12982" s="T715">v</ta>
            <ta e="T717" id="Seg_12983" s="T716">conj</ta>
            <ta e="T718" id="Seg_12984" s="T717">pers</ta>
            <ta e="T719" id="Seg_12985" s="T718">n</ta>
            <ta e="T720" id="Seg_12986" s="T719">v</ta>
            <ta e="T721" id="Seg_12987" s="T720">conj</ta>
            <ta e="T722" id="Seg_12988" s="T721">interrog</ta>
            <ta e="T723" id="Seg_12989" s="T722">n</ta>
            <ta e="T724" id="Seg_12990" s="T723">ptcl</ta>
            <ta e="T725" id="Seg_12991" s="T724">v</ta>
            <ta e="T726" id="Seg_12992" s="T725">adv</ta>
            <ta e="T727" id="Seg_12993" s="T726">n</ta>
            <ta e="T728" id="Seg_12994" s="T727">pers</ta>
            <ta e="T729" id="Seg_12995" s="T728">v</ta>
            <ta e="T730" id="Seg_12996" s="T729">n</ta>
            <ta e="T731" id="Seg_12997" s="T730">v</ta>
            <ta e="T732" id="Seg_12998" s="T731">n</ta>
            <ta e="T733" id="Seg_12999" s="T732">v</ta>
            <ta e="T734" id="Seg_13000" s="T733">n</ta>
            <ta e="T735" id="Seg_13001" s="T734">n</ta>
            <ta e="T736" id="Seg_13002" s="T735">conj</ta>
            <ta e="T737" id="Seg_13003" s="T736">n</ta>
            <ta e="T738" id="Seg_13004" s="T737">v</ta>
            <ta e="T739" id="Seg_13005" s="T738">conj</ta>
            <ta e="T740" id="Seg_13006" s="T739">pers</ta>
            <ta e="T741" id="Seg_13007" s="T740">n</ta>
            <ta e="T742" id="Seg_13008" s="T741">v</ta>
            <ta e="T743" id="Seg_13009" s="T742">v</ta>
            <ta e="T744" id="Seg_13010" s="T743">v</ta>
            <ta e="T745" id="Seg_13011" s="T744">n</ta>
            <ta e="T746" id="Seg_13012" s="T745">n</ta>
            <ta e="T747" id="Seg_13013" s="T746">pers</ta>
            <ta e="T748" id="Seg_13014" s="T747">v</ta>
            <ta e="T749" id="Seg_13015" s="T748">v</ta>
            <ta e="T750" id="Seg_13016" s="T749">adv</ta>
            <ta e="T751" id="Seg_13017" s="T750">n</ta>
            <ta e="T752" id="Seg_13018" s="T751">adj</ta>
            <ta e="T753" id="Seg_13019" s="T752">adv</ta>
            <ta e="T754" id="Seg_13020" s="T753">v</ta>
            <ta e="T755" id="Seg_13021" s="T754">n</ta>
            <ta e="T756" id="Seg_13022" s="T755">v</ta>
            <ta e="T757" id="Seg_13023" s="T756">n</ta>
            <ta e="T758" id="Seg_13024" s="T757">n</ta>
            <ta e="T759" id="Seg_13025" s="T758">n</ta>
            <ta e="T760" id="Seg_13026" s="T759">n</ta>
            <ta e="T761" id="Seg_13027" s="T760">ptcl</ta>
            <ta e="T762" id="Seg_13028" s="T761">v</ta>
            <ta e="T763" id="Seg_13029" s="T762">n</ta>
            <ta e="T764" id="Seg_13030" s="T763">pers</ta>
            <ta e="T765" id="Seg_13031" s="T764">pers</ta>
            <ta e="T766" id="Seg_13032" s="T765">v</ta>
            <ta e="T767" id="Seg_13033" s="T766">emphpro</ta>
            <ta e="T768" id="Seg_13034" s="T767">ptcl</ta>
            <ta e="T769" id="Seg_13035" s="T768">v</ta>
            <ta e="T770" id="Seg_13036" s="T769">n</ta>
            <ta e="T771" id="Seg_13037" s="T770">v</ta>
            <ta e="T772" id="Seg_13038" s="T771">v</ta>
            <ta e="T773" id="Seg_13039" s="T772">v</ta>
            <ta e="T774" id="Seg_13040" s="T773">conj</ta>
            <ta e="T775" id="Seg_13041" s="T774">v</ta>
            <ta e="T776" id="Seg_13042" s="T775">v</ta>
            <ta e="T777" id="Seg_13043" s="T776">conj</ta>
            <ta e="T778" id="Seg_13044" s="T777">ptcl</ta>
            <ta e="T779" id="Seg_13045" s="T778">v</ta>
            <ta e="T780" id="Seg_13046" s="T779">n</ta>
            <ta e="T781" id="Seg_13047" s="T780">pers</ta>
            <ta e="T782" id="Seg_13048" s="T781">v</ta>
            <ta e="T783" id="Seg_13049" s="T782">adv</ta>
            <ta e="T784" id="Seg_13050" s="T783">v</ta>
            <ta e="T785" id="Seg_13051" s="T784">conj</ta>
            <ta e="T786" id="Seg_13052" s="T785">adv</ta>
            <ta e="T787" id="Seg_13053" s="T786">n</ta>
            <ta e="T788" id="Seg_13054" s="T787">n</ta>
            <ta e="T789" id="Seg_13055" s="T788">pers</ta>
            <ta e="T790" id="Seg_13056" s="T789">v</ta>
            <ta e="T791" id="Seg_13057" s="T790">n</ta>
            <ta e="T792" id="Seg_13058" s="T791">n</ta>
            <ta e="T793" id="Seg_13059" s="T792">n</ta>
            <ta e="T794" id="Seg_13060" s="T793">v</ta>
            <ta e="T795" id="Seg_13061" s="T794">preverb</ta>
            <ta e="T796" id="Seg_13062" s="T795">v</ta>
            <ta e="T797" id="Seg_13063" s="T796">pers</ta>
            <ta e="T798" id="Seg_13064" s="T797">n</ta>
            <ta e="T799" id="Seg_13065" s="T798">v</ta>
            <ta e="T800" id="Seg_13066" s="T799">pers</ta>
            <ta e="T801" id="Seg_13067" s="T800">n</ta>
            <ta e="T802" id="Seg_13068" s="T801">v</ta>
            <ta e="T803" id="Seg_13069" s="T802">conj</ta>
            <ta e="T804" id="Seg_13070" s="T803">n</ta>
            <ta e="T805" id="Seg_13071" s="T804">v</ta>
            <ta e="T806" id="Seg_13072" s="T805">pers</ta>
            <ta e="T807" id="Seg_13073" s="T806">v</ta>
            <ta e="T808" id="Seg_13074" s="T807">conj</ta>
            <ta e="T809" id="Seg_13075" s="T808">n</ta>
            <ta e="T810" id="Seg_13076" s="T809">v</ta>
            <ta e="T811" id="Seg_13077" s="T810">v</ta>
            <ta e="T812" id="Seg_13078" s="T811">n</ta>
            <ta e="T813" id="Seg_13079" s="T812">n</ta>
            <ta e="T814" id="Seg_13080" s="T813">n</ta>
            <ta e="T815" id="Seg_13081" s="T814">v</ta>
            <ta e="T816" id="Seg_13082" s="T815">conj</ta>
            <ta e="T817" id="Seg_13083" s="T816">v</ta>
            <ta e="T818" id="Seg_13084" s="T817">n</ta>
            <ta e="T819" id="Seg_13085" s="T818">n</ta>
            <ta e="T820" id="Seg_13086" s="T819">preverb</ta>
            <ta e="T821" id="Seg_13087" s="T820">v</ta>
            <ta e="T822" id="Seg_13088" s="T821">v</ta>
            <ta e="T823" id="Seg_13089" s="T822">pers</ta>
            <ta e="T824" id="Seg_13090" s="T823">n</ta>
            <ta e="T825" id="Seg_13091" s="T824">v</ta>
            <ta e="T826" id="Seg_13092" s="T825">pers</ta>
            <ta e="T827" id="Seg_13093" s="T826">n</ta>
            <ta e="T828" id="Seg_13094" s="T827">v</ta>
            <ta e="T829" id="Seg_13095" s="T828">pers</ta>
            <ta e="T830" id="Seg_13096" s="T829">v</ta>
            <ta e="T831" id="Seg_13097" s="T830">n</ta>
            <ta e="T832" id="Seg_13098" s="T831">conj</ta>
            <ta e="T833" id="Seg_13099" s="T832">adv</ta>
            <ta e="T834" id="Seg_13100" s="T833">v</ta>
            <ta e="T835" id="Seg_13101" s="T834">ptcl</ta>
            <ta e="T836" id="Seg_13102" s="T835">v</ta>
            <ta e="T837" id="Seg_13103" s="T836">pers</ta>
            <ta e="T838" id="Seg_13104" s="T837">ptcl</ta>
            <ta e="T839" id="Seg_13105" s="T838">v</ta>
            <ta e="T840" id="Seg_13106" s="T839">n</ta>
            <ta e="T841" id="Seg_13107" s="T840">v</ta>
            <ta e="T842" id="Seg_13108" s="T841">n</ta>
            <ta e="T843" id="Seg_13109" s="T842">v</ta>
            <ta e="T844" id="Seg_13110" s="T843">pers</ta>
            <ta e="T845" id="Seg_13111" s="T844">v</ta>
            <ta e="T846" id="Seg_13112" s="T845">conj</ta>
            <ta e="T847" id="Seg_13113" s="T846">n</ta>
            <ta e="T848" id="Seg_13114" s="T847">adv</ta>
            <ta e="T849" id="Seg_13115" s="T848">ptcl</ta>
            <ta e="T850" id="Seg_13116" s="T849">v</ta>
            <ta e="T851" id="Seg_13117" s="T850">v</ta>
            <ta e="T852" id="Seg_13118" s="T851">n</ta>
            <ta e="T853" id="Seg_13119" s="T852">v</ta>
            <ta e="T854" id="Seg_13120" s="T853">pers</ta>
            <ta e="T855" id="Seg_13121" s="T854">n</ta>
            <ta e="T856" id="Seg_13122" s="T855">v</ta>
            <ta e="T857" id="Seg_13123" s="T856">adv</ta>
            <ta e="T858" id="Seg_13124" s="T857">v</ta>
            <ta e="T859" id="Seg_13125" s="T858">n</ta>
            <ta e="T860" id="Seg_13126" s="T859">ptcl</ta>
            <ta e="T861" id="Seg_13127" s="T860">v</ta>
            <ta e="T862" id="Seg_13128" s="T861">pers</ta>
            <ta e="T863" id="Seg_13129" s="T862">pers</ta>
            <ta e="T864" id="Seg_13130" s="T863">emphpro</ta>
            <ta e="T865" id="Seg_13131" s="T864">v</ta>
            <ta e="T866" id="Seg_13132" s="T865">n</ta>
            <ta e="T867" id="Seg_13133" s="T866">adv</ta>
            <ta e="T868" id="Seg_13134" s="T867">v</ta>
            <ta e="T869" id="Seg_13135" s="T868">pers</ta>
            <ta e="T870" id="Seg_13136" s="T869">n</ta>
            <ta e="T871" id="Seg_13137" s="T870">n</ta>
            <ta e="T872" id="Seg_13138" s="T871">v</ta>
            <ta e="T875" id="Seg_13139" s="T874">v</ta>
            <ta e="T876" id="Seg_13140" s="T875">conj</ta>
            <ta e="T877" id="Seg_13141" s="T876">v</ta>
            <ta e="T878" id="Seg_13142" s="T877">n</ta>
            <ta e="T879" id="Seg_13143" s="T878">adv</ta>
            <ta e="T880" id="Seg_13144" s="T879">n</ta>
            <ta e="T881" id="Seg_13145" s="T880">v</ta>
            <ta e="T882" id="Seg_13146" s="T881">adv</ta>
            <ta e="T883" id="Seg_13147" s="T882">adv</ta>
            <ta e="T884" id="Seg_13148" s="T883">v</ta>
            <ta e="T885" id="Seg_13149" s="T884">n</ta>
            <ta e="T886" id="Seg_13150" s="T885">v</ta>
            <ta e="T887" id="Seg_13151" s="T886">ptcl</ta>
            <ta e="T888" id="Seg_13152" s="T887">v</ta>
            <ta e="T889" id="Seg_13153" s="T888">conj</ta>
            <ta e="T890" id="Seg_13154" s="T889">n</ta>
            <ta e="T891" id="Seg_13155" s="T890">v</ta>
            <ta e="T892" id="Seg_13156" s="T891">adv</ta>
            <ta e="T893" id="Seg_13157" s="T892">v</ta>
            <ta e="T894" id="Seg_13158" s="T893">pers</ta>
            <ta e="T895" id="Seg_13159" s="T894">n</ta>
            <ta e="T896" id="Seg_13160" s="T895">v</ta>
            <ta e="T897" id="Seg_13161" s="T896">v</ta>
            <ta e="T898" id="Seg_13162" s="T897">ptcl</ta>
            <ta e="T899" id="Seg_13163" s="T898">adv</ta>
            <ta e="T900" id="Seg_13164" s="T899">v</ta>
            <ta e="T901" id="Seg_13165" s="T900">n</ta>
            <ta e="T902" id="Seg_13166" s="T901">v</ta>
            <ta e="T903" id="Seg_13167" s="T902">conj</ta>
            <ta e="T904" id="Seg_13168" s="T903">adv</ta>
            <ta e="T905" id="Seg_13169" s="T904">n</ta>
            <ta e="T906" id="Seg_13170" s="T905">v</ta>
            <ta e="T907" id="Seg_13171" s="T906">adv</ta>
            <ta e="T908" id="Seg_13172" s="T907">v</ta>
            <ta e="T909" id="Seg_13173" s="T908">ptcl</ta>
            <ta e="T910" id="Seg_13174" s="T909">v</ta>
            <ta e="T911" id="Seg_13175" s="T910">v</ta>
            <ta e="T912" id="Seg_13176" s="T911">adv</ta>
            <ta e="T913" id="Seg_13177" s="T912">pers</ta>
            <ta e="T914" id="Seg_13178" s="T913">v</ta>
            <ta e="T915" id="Seg_13179" s="T914">interrog</ta>
            <ta e="T916" id="Seg_13180" s="T915">n</ta>
            <ta e="T917" id="Seg_13181" s="T916">ptcl</ta>
            <ta e="T918" id="Seg_13182" s="T917">v</ta>
            <ta e="T919" id="Seg_13183" s="T918">pers</ta>
            <ta e="T920" id="Seg_13184" s="T919">v</ta>
            <ta e="T921" id="Seg_13185" s="T920">n</ta>
            <ta e="T922" id="Seg_13186" s="T921">v</ta>
            <ta e="T923" id="Seg_13187" s="T922">n</ta>
            <ta e="T924" id="Seg_13188" s="T923">adv</ta>
            <ta e="T925" id="Seg_13189" s="T924">n</ta>
            <ta e="T926" id="Seg_13190" s="T925">v</ta>
            <ta e="T927" id="Seg_13191" s="T926">pers</ta>
            <ta e="T928" id="Seg_13192" s="T927">adv</ta>
            <ta e="T929" id="Seg_13193" s="T928">adv</ta>
            <ta e="T930" id="Seg_13194" s="T929">v</ta>
            <ta e="T931" id="Seg_13195" s="T930">adv</ta>
            <ta e="T932" id="Seg_13196" s="T931">n</ta>
            <ta e="T933" id="Seg_13197" s="T932">v</ta>
            <ta e="T934" id="Seg_13198" s="T933">n</ta>
            <ta e="T935" id="Seg_13199" s="T934">adv</ta>
            <ta e="T936" id="Seg_13200" s="T935">v</ta>
            <ta e="T937" id="Seg_13201" s="T936">v</ta>
            <ta e="T938" id="Seg_13202" s="T937">conj</ta>
            <ta e="T939" id="Seg_13203" s="T938">v</ta>
            <ta e="T940" id="Seg_13204" s="T939">conj</ta>
            <ta e="T941" id="Seg_13205" s="T940">n</ta>
            <ta e="T942" id="Seg_13206" s="T941">n</ta>
            <ta e="T943" id="Seg_13207" s="T942">v</ta>
            <ta e="T944" id="Seg_13208" s="T943">v</ta>
            <ta e="T945" id="Seg_13209" s="T944">pers</ta>
            <ta e="T946" id="Seg_13210" s="T945">adj</ta>
            <ta e="T947" id="Seg_13211" s="T946">ptcl</ta>
            <ta e="T948" id="Seg_13212" s="T947">v</ta>
            <ta e="T949" id="Seg_13213" s="T948">n</ta>
            <ta e="T950" id="Seg_13214" s="T949">v</ta>
            <ta e="T951" id="Seg_13215" s="T950">dem</ta>
            <ta e="T952" id="Seg_13216" s="T951">n</ta>
            <ta e="T953" id="Seg_13217" s="T952">v</ta>
            <ta e="T954" id="Seg_13218" s="T953">n</ta>
            <ta e="T955" id="Seg_13219" s="T954">adv</ta>
            <ta e="T956" id="Seg_13220" s="T955">v</ta>
            <ta e="T957" id="Seg_13221" s="T956">conj</ta>
            <ta e="T958" id="Seg_13222" s="T957">v</ta>
            <ta e="T959" id="Seg_13223" s="T958">n</ta>
            <ta e="T960" id="Seg_13224" s="T959">adv</ta>
            <ta e="T961" id="Seg_13225" s="T960">v</ta>
            <ta e="T962" id="Seg_13226" s="T961">n</ta>
            <ta e="T963" id="Seg_13227" s="T962">adv</ta>
            <ta e="T964" id="Seg_13228" s="T963">v</ta>
            <ta e="T965" id="Seg_13229" s="T964">conj</ta>
            <ta e="T966" id="Seg_13230" s="T965">v</ta>
            <ta e="T967" id="Seg_13231" s="T966">conj</ta>
            <ta e="T968" id="Seg_13232" s="T967">n</ta>
            <ta e="T969" id="Seg_13233" s="T968">v</ta>
            <ta e="T970" id="Seg_13234" s="T969">pers</ta>
            <ta e="T971" id="Seg_13235" s="T970">pers</ta>
            <ta e="T972" id="Seg_13236" s="T971">v</ta>
            <ta e="T973" id="Seg_13237" s="T972">ptcl</ta>
            <ta e="T974" id="Seg_13238" s="T973">v</ta>
            <ta e="T975" id="Seg_13239" s="T974">v</ta>
            <ta e="T976" id="Seg_13240" s="T975">adj</ta>
            <ta e="T977" id="Seg_13241" s="T976">v</ta>
            <ta e="T978" id="Seg_13242" s="T977">adj</ta>
            <ta e="T979" id="Seg_13243" s="T978">n</ta>
            <ta e="T980" id="Seg_13244" s="T979">v</ta>
            <ta e="T981" id="Seg_13245" s="T980">pers</ta>
            <ta e="T982" id="Seg_13246" s="T981">adv</ta>
            <ta e="T983" id="Seg_13247" s="T982">v</ta>
            <ta e="T984" id="Seg_13248" s="T983">v</ta>
            <ta e="T985" id="Seg_13249" s="T984">conj</ta>
            <ta e="T986" id="Seg_13250" s="T985">n</ta>
            <ta e="T987" id="Seg_13251" s="T986">v</ta>
            <ta e="T988" id="Seg_13252" s="T987">n</ta>
            <ta e="T989" id="Seg_13253" s="T988">adv</ta>
            <ta e="T990" id="Seg_13254" s="T989">adv</ta>
            <ta e="T991" id="Seg_13255" s="T990">v</ta>
            <ta e="T992" id="Seg_13256" s="T991">n</ta>
            <ta e="T993" id="Seg_13257" s="T992">quant</ta>
            <ta e="T994" id="Seg_13258" s="T993">v</ta>
            <ta e="T995" id="Seg_13259" s="T994">n</ta>
            <ta e="T996" id="Seg_13260" s="T995">v</ta>
            <ta e="T997" id="Seg_13261" s="T996">conj</ta>
            <ta e="T998" id="Seg_13262" s="T997">ptcl</ta>
            <ta e="T999" id="Seg_13263" s="T998">v</ta>
            <ta e="T1000" id="Seg_13264" s="T999">adv</ta>
            <ta e="T1001" id="Seg_13265" s="T1000">v</ta>
            <ta e="T1002" id="Seg_13266" s="T1001">emphpro</ta>
            <ta e="T1003" id="Seg_13267" s="T1002">n</ta>
            <ta e="T1004" id="Seg_13268" s="T1003">n</ta>
            <ta e="T1005" id="Seg_13269" s="T1004">v</ta>
            <ta e="T1006" id="Seg_13270" s="T1005">conj</ta>
            <ta e="T1007" id="Seg_13271" s="T1006">n</ta>
            <ta e="T1008" id="Seg_13272" s="T1007">interrog</ta>
            <ta e="T1009" id="Seg_13273" s="T1008">v</ta>
            <ta e="T1010" id="Seg_13274" s="T1009">n</ta>
            <ta e="T1011" id="Seg_13275" s="T1010">v</ta>
            <ta e="T1012" id="Seg_13276" s="T1011">quant</ta>
            <ta e="T1013" id="Seg_13277" s="T1012">v</ta>
            <ta e="T1014" id="Seg_13278" s="T1013">n</ta>
            <ta e="T1015" id="Seg_13279" s="T1014">pers</ta>
            <ta e="T1016" id="Seg_13280" s="T1015">pro</ta>
            <ta e="T1017" id="Seg_13281" s="T1016">n</ta>
            <ta e="T1018" id="Seg_13282" s="T1017">v</ta>
            <ta e="T1019" id="Seg_13283" s="T1018">conj</ta>
            <ta e="T1020" id="Seg_13284" s="T1019">n</ta>
            <ta e="T1021" id="Seg_13285" s="T1020">conj</ta>
            <ta e="T1022" id="Seg_13286" s="T1021">adv</ta>
            <ta e="T1023" id="Seg_13287" s="T1022">ptcl</ta>
            <ta e="T1024" id="Seg_13288" s="T1023">v</ta>
            <ta e="T1025" id="Seg_13289" s="T1024">pers</ta>
            <ta e="T1026" id="Seg_13290" s="T1025">adv</ta>
            <ta e="T1027" id="Seg_13291" s="T1026">pers</ta>
            <ta e="T1028" id="Seg_13292" s="T1027">v</ta>
            <ta e="T1029" id="Seg_13293" s="T1028">adv</ta>
            <ta e="T1030" id="Seg_13294" s="T1029">adv</ta>
            <ta e="T1031" id="Seg_13295" s="T1030">v</ta>
            <ta e="T1032" id="Seg_13296" s="T1031">n</ta>
            <ta e="T1033" id="Seg_13297" s="T1032">adj</ta>
            <ta e="T1034" id="Seg_13298" s="T1033">pers</ta>
            <ta e="T1035" id="Seg_13299" s="T1034">n</ta>
            <ta e="T1036" id="Seg_13300" s="T1035">pers</ta>
            <ta e="T1037" id="Seg_13301" s="T1036">v</ta>
            <ta e="T1038" id="Seg_13302" s="T1037">pers</ta>
            <ta e="T1039" id="Seg_13303" s="T1038">v</ta>
            <ta e="T1040" id="Seg_13304" s="T1039">pers</ta>
            <ta e="T1041" id="Seg_13305" s="T1040">v</ta>
            <ta e="T1042" id="Seg_13306" s="T1041">pers</ta>
            <ta e="T1043" id="Seg_13307" s="T1042">pers</ta>
            <ta e="T1044" id="Seg_13308" s="T1043">adv</ta>
            <ta e="T1045" id="Seg_13309" s="T1044">v</ta>
            <ta e="T1046" id="Seg_13310" s="T1045">adv</ta>
            <ta e="T1047" id="Seg_13311" s="T1046">n</ta>
            <ta e="T1048" id="Seg_13312" s="T1047">v</ta>
            <ta e="T1049" id="Seg_13313" s="T1048">pers</ta>
            <ta e="T1050" id="Seg_13314" s="T1049">adv</ta>
            <ta e="T1051" id="Seg_13315" s="T1050">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T5" id="Seg_13316" s="T4">np.h:P</ta>
            <ta e="T7" id="Seg_13317" s="T6">pro.h:Poss</ta>
            <ta e="T9" id="Seg_13318" s="T8">np.h:Th</ta>
            <ta e="T10" id="Seg_13319" s="T9">np.h:A 0.3.h:Poss</ta>
            <ta e="T14" id="Seg_13320" s="T13">np.h:A 0.3.h:Poss</ta>
            <ta e="T15" id="Seg_13321" s="T14">adv:Time</ta>
            <ta e="T16" id="Seg_13322" s="T15">0.3.h:A</ta>
            <ta e="T23" id="Seg_13323" s="T22">np.h:A 0.3.h:Poss</ta>
            <ta e="T24" id="Seg_13324" s="T23">np:Com</ta>
            <ta e="T26" id="Seg_13325" s="T25">np:Th</ta>
            <ta e="T27" id="Seg_13326" s="T26">0.3.h:A</ta>
            <ta e="T28" id="Seg_13327" s="T27">np:Th</ta>
            <ta e="T30" id="Seg_13328" s="T29">np:Th</ta>
            <ta e="T32" id="Seg_13329" s="T31">np:Th</ta>
            <ta e="T33" id="Seg_13330" s="T32">0.3.h:A</ta>
            <ta e="T34" id="Seg_13331" s="T33">np:P </ta>
            <ta e="T35" id="Seg_13332" s="T34">0.3.h:A</ta>
            <ta e="T38" id="Seg_13333" s="T37">np.h:A 0.3.h:Poss</ta>
            <ta e="T39" id="Seg_13334" s="T38">adv:Time</ta>
            <ta e="T40" id="Seg_13335" s="T39">np:G</ta>
            <ta e="T43" id="Seg_13336" s="T42">pro.h:Poss</ta>
            <ta e="T44" id="Seg_13337" s="T43">np.h:P</ta>
            <ta e="T47" id="Seg_13338" s="T46">np.h:P</ta>
            <ta e="T49" id="Seg_13339" s="T48">pro.h:Th</ta>
            <ta e="T54" id="Seg_13340" s="T53">pro.h:A</ta>
            <ta e="T56" id="Seg_13341" s="T55">np:L</ta>
            <ta e="T58" id="Seg_13342" s="T57">np:P</ta>
            <ta e="T62" id="Seg_13343" s="T61">np.h:R 0.3.h:Poss</ta>
            <ta e="T63" id="Seg_13344" s="T62">np.h:R 0.3.h:Poss</ta>
            <ta e="T64" id="Seg_13345" s="T63">0.3.h:A</ta>
            <ta e="T65" id="Seg_13346" s="T64">np:Ins</ta>
            <ta e="T66" id="Seg_13347" s="T65">pro.h:Th</ta>
            <ta e="T67" id="Seg_13348" s="T66">0.2.h:A</ta>
            <ta e="T68" id="Seg_13349" s="T67">np.h:A 0.3.h:Poss</ta>
            <ta e="T69" id="Seg_13350" s="T68">np:Th</ta>
            <ta e="T72" id="Seg_13351" s="T71">0.3.h:A 0.3.h:Th</ta>
            <ta e="T73" id="Seg_13352" s="T72">pro.h:A</ta>
            <ta e="T76" id="Seg_13353" s="T75">np.h:Th</ta>
            <ta e="T78" id="Seg_13354" s="T77">np:L</ta>
            <ta e="T81" id="Seg_13355" s="T80">pro.h:Poss</ta>
            <ta e="T82" id="Seg_13356" s="T81">adv:Time</ta>
            <ta e="T83" id="Seg_13357" s="T82">np:Th</ta>
            <ta e="T84" id="Seg_13358" s="T83">np:G</ta>
            <ta e="T86" id="Seg_13359" s="T85">np:Th 0.3.h:Poss</ta>
            <ta e="T87" id="Seg_13360" s="T86">0.3.h:A</ta>
            <ta e="T90" id="Seg_13361" s="T89">0.3.h:A</ta>
            <ta e="T91" id="Seg_13362" s="T90">np.h:Th 0.3.h:Poss</ta>
            <ta e="T92" id="Seg_13363" s="T91">np.h:Th 0.3.h:Poss</ta>
            <ta e="T94" id="Seg_13364" s="T93">0.3.h:A</ta>
            <ta e="T98" id="Seg_13365" s="T97">pro.h:A</ta>
            <ta e="T106" id="Seg_13366" s="T105">0.3.h:A</ta>
            <ta e="T109" id="Seg_13367" s="T108">adv:L</ta>
            <ta e="T111" id="Seg_13368" s="T110">np:P</ta>
            <ta e="T112" id="Seg_13369" s="T111">0.3.h:A</ta>
            <ta e="T113" id="Seg_13370" s="T112">pro:Th</ta>
            <ta e="T114" id="Seg_13371" s="T113">0.3.h:A</ta>
            <ta e="T115" id="Seg_13372" s="T114">adv:G</ta>
            <ta e="T118" id="Seg_13373" s="T117">np.h:A</ta>
            <ta e="T119" id="Seg_13374" s="T118">np:P</ta>
            <ta e="T122" id="Seg_13375" s="T121">np:P</ta>
            <ta e="T123" id="Seg_13376" s="T122">0.3.h:A</ta>
            <ta e="T124" id="Seg_13377" s="T123">adv:Time</ta>
            <ta e="T125" id="Seg_13378" s="T124">np:G</ta>
            <ta e="T126" id="Seg_13379" s="T125">0.1.h:A</ta>
            <ta e="T128" id="Seg_13380" s="T127">pro.h:A</ta>
            <ta e="T132" id="Seg_13381" s="T131">np:G 0.2.h:Poss</ta>
            <ta e="T134" id="Seg_13382" s="T133">np:Th</ta>
            <ta e="T140" id="Seg_13383" s="T139">0.3:Th</ta>
            <ta e="T142" id="Seg_13384" s="T141">0.3.h:Th</ta>
            <ta e="T143" id="Seg_13385" s="T142">np:Time</ta>
            <ta e="T144" id="Seg_13386" s="T143">0.3.h:A</ta>
            <ta e="T145" id="Seg_13387" s="T144">0.3.h:A</ta>
            <ta e="T148" id="Seg_13388" s="T147">0.3.h:A</ta>
            <ta e="T149" id="Seg_13389" s="T148">np:G</ta>
            <ta e="T151" id="Seg_13390" s="T150">np.h:A</ta>
            <ta e="T154" id="Seg_13391" s="T153">pro.h:R</ta>
            <ta e="T155" id="Seg_13392" s="T154">np.h:A 0.1.h:Poss</ta>
            <ta e="T158" id="Seg_13393" s="T157">np:Th</ta>
            <ta e="T159" id="Seg_13394" s="T158">np:G</ta>
            <ta e="T163" id="Seg_13395" s="T162">pro.h:A</ta>
            <ta e="T164" id="Seg_13396" s="T163">adv:Time</ta>
            <ta e="T166" id="Seg_13397" s="T165">np:Th</ta>
            <ta e="T167" id="Seg_13398" s="T166">np:Ins</ta>
            <ta e="T169" id="Seg_13399" s="T168">0.1.h:A 0.3:Th</ta>
            <ta e="T170" id="Seg_13400" s="T169">0.1.h:A</ta>
            <ta e="T171" id="Seg_13401" s="T170">np:G</ta>
            <ta e="T172" id="Seg_13402" s="T171">0.1.h:A 0.3:Th</ta>
            <ta e="T175" id="Seg_13403" s="T174">0.3.h:A 0.3:Th</ta>
            <ta e="T177" id="Seg_13404" s="T176">0.3.h:A 0.3:Th</ta>
            <ta e="T178" id="Seg_13405" s="T177">0.3.h:Th</ta>
            <ta e="T180" id="Seg_13406" s="T179">0.3.h:A</ta>
            <ta e="T182" id="Seg_13407" s="T181">adv:L</ta>
            <ta e="T183" id="Seg_13408" s="T182">0.3:Th</ta>
            <ta e="T184" id="Seg_13409" s="T183">pro.h:A</ta>
            <ta e="T185" id="Seg_13410" s="T184">np:Th</ta>
            <ta e="T188" id="Seg_13411" s="T187">np:G</ta>
            <ta e="T189" id="Seg_13412" s="T188">0.3.h:A</ta>
            <ta e="T191" id="Seg_13413" s="T190">pro.h:Th</ta>
            <ta e="T195" id="Seg_13414" s="T194">0.3.h:A</ta>
            <ta e="T197" id="Seg_13415" s="T196">np:G</ta>
            <ta e="T198" id="Seg_13416" s="T197">0.3.h:A</ta>
            <ta e="T201" id="Seg_13417" s="T200">0.3.h:A</ta>
            <ta e="T204" id="Seg_13418" s="T203">np.h:A</ta>
            <ta e="T206" id="Seg_13419" s="T205">np.h:A</ta>
            <ta e="T208" id="Seg_13420" s="T207">np:Ins</ta>
            <ta e="T210" id="Seg_13421" s="T209">0.3.h:A</ta>
            <ta e="T212" id="Seg_13422" s="T211">np:Th</ta>
            <ta e="T216" id="Seg_13423" s="T215">np:Poss</ta>
            <ta e="T217" id="Seg_13424" s="T216">np:G</ta>
            <ta e="T218" id="Seg_13425" s="T217">0.2.h:A</ta>
            <ta e="T219" id="Seg_13426" s="T218">pro.h:A</ta>
            <ta e="T222" id="Seg_13427" s="T221">np:Th</ta>
            <ta e="T223" id="Seg_13428" s="T222">pro.h:B</ta>
            <ta e="T224" id="Seg_13429" s="T223">np:Th</ta>
            <ta e="T226" id="Seg_13430" s="T225">adv:Time</ta>
            <ta e="T227" id="Seg_13431" s="T226">pro.h:E</ta>
            <ta e="T229" id="Seg_13432" s="T228">np:Th</ta>
            <ta e="T233" id="Seg_13433" s="T232">np:G</ta>
            <ta e="T239" id="Seg_13434" s="T238">0.3.h:A</ta>
            <ta e="T240" id="Seg_13435" s="T239">np.h:A</ta>
            <ta e="T242" id="Seg_13436" s="T241">pro.h:Th</ta>
            <ta e="T244" id="Seg_13437" s="T243">adv:L</ta>
            <ta e="T246" id="Seg_13438" s="T245">pro.h:A</ta>
            <ta e="T248" id="Seg_13439" s="T247">0.1.h:A</ta>
            <ta e="T249" id="Seg_13440" s="T248">pro.h:Th</ta>
            <ta e="T251" id="Seg_13441" s="T250">adv:L</ta>
            <ta e="T253" id="Seg_13442" s="T252">0.3.h:A</ta>
            <ta e="T254" id="Seg_13443" s="T253">0.3.h:A</ta>
            <ta e="T255" id="Seg_13444" s="T254">0.3.h:A</ta>
            <ta e="T256" id="Seg_13445" s="T255">np:G</ta>
            <ta e="T258" id="Seg_13446" s="T257">np.h:A</ta>
            <ta e="T262" id="Seg_13447" s="T261">np:Th</ta>
            <ta e="T264" id="Seg_13448" s="T263">0.3.h:A</ta>
            <ta e="T265" id="Seg_13449" s="T264">0.3.h:A</ta>
            <ta e="T266" id="Seg_13450" s="T265">0.2.h:A</ta>
            <ta e="T267" id="Seg_13451" s="T266">adv:L</ta>
            <ta e="T269" id="Seg_13452" s="T268">pro.h:A</ta>
            <ta e="T273" id="Seg_13453" s="T272">0.3.h:A</ta>
            <ta e="T274" id="Seg_13454" s="T273">np.h:A</ta>
            <ta e="T276" id="Seg_13455" s="T275">pro.h:A</ta>
            <ta e="T277" id="Seg_13456" s="T276">pro.h:Th</ta>
            <ta e="T280" id="Seg_13457" s="T279">pro.h:Poss</ta>
            <ta e="T281" id="Seg_13458" s="T280">np.h:Th</ta>
            <ta e="T283" id="Seg_13459" s="T282">pro.h:A</ta>
            <ta e="T284" id="Seg_13460" s="T283">pro.h:P</ta>
            <ta e="T287" id="Seg_13461" s="T286">pro.h:P</ta>
            <ta e="T288" id="Seg_13462" s="T287">0.3.h:A</ta>
            <ta e="T290" id="Seg_13463" s="T289">pro.h:A</ta>
            <ta e="T291" id="Seg_13464" s="T290">pro.h:R</ta>
            <ta e="T292" id="Seg_13465" s="T291">np:Th</ta>
            <ta e="T294" id="Seg_13466" s="T293">np:Th</ta>
            <ta e="T299" id="Seg_13467" s="T298">pro.h:A</ta>
            <ta e="T301" id="Seg_13468" s="T300">np:P</ta>
            <ta e="T304" id="Seg_13469" s="T303">pro.h:A</ta>
            <ta e="T307" id="Seg_13470" s="T306">np:Time</ta>
            <ta e="T308" id="Seg_13471" s="T307">pro.h:A</ta>
            <ta e="T310" id="Seg_13472" s="T309">0.1.h:A</ta>
            <ta e="T311" id="Seg_13473" s="T310">pro.h:R</ta>
            <ta e="T312" id="Seg_13474" s="T311">0.2h:A</ta>
            <ta e="T313" id="Seg_13475" s="T312">pro.h:Th</ta>
            <ta e="T316" id="Seg_13476" s="T315">np:P 0.3.h:Poss</ta>
            <ta e="T317" id="Seg_13477" s="T316">adv:G</ta>
            <ta e="T320" id="Seg_13478" s="T319">pro.h:A</ta>
            <ta e="T321" id="Seg_13479" s="T320">pro.h:Th</ta>
            <ta e="T324" id="Seg_13480" s="T323">np:Th 0.3.h:Poss</ta>
            <ta e="T325" id="Seg_13481" s="T324">adv:G</ta>
            <ta e="T326" id="Seg_13482" s="T325">0.2.h:A</ta>
            <ta e="T327" id="Seg_13483" s="T326">pro.h:A</ta>
            <ta e="T329" id="Seg_13484" s="T328">np:P</ta>
            <ta e="T332" id="Seg_13485" s="T331">pro.h:A</ta>
            <ta e="T335" id="Seg_13486" s="T334">0.3:P</ta>
            <ta e="T337" id="Seg_13487" s="T336">pro.h:A</ta>
            <ta e="T338" id="Seg_13488" s="T337">adv:G</ta>
            <ta e="T340" id="Seg_13489" s="T339">pro.h:Th</ta>
            <ta e="T341" id="Seg_13490" s="T340">0.2.h:A</ta>
            <ta e="T342" id="Seg_13491" s="T341">pro.h:A</ta>
            <ta e="T344" id="Seg_13492" s="T343">pro.h:P</ta>
            <ta e="T346" id="Seg_13493" s="T345">0.1.h:A</ta>
            <ta e="T347" id="Seg_13494" s="T346">pro:Com</ta>
            <ta e="T348" id="Seg_13495" s="T347">0.1.h:Th</ta>
            <ta e="T349" id="Seg_13496" s="T348">np.h:A 0.3.h:Poss</ta>
            <ta e="T351" id="Seg_13497" s="T350">pro.h:A</ta>
            <ta e="T352" id="Seg_13498" s="T351">pro.h:B</ta>
            <ta e="T354" id="Seg_13499" s="T353">0.3.h:A</ta>
            <ta e="T355" id="Seg_13500" s="T354">0.1.h:A</ta>
            <ta e="T356" id="Seg_13501" s="T355">np.h:A 0.3.h:Poss</ta>
            <ta e="T358" id="Seg_13502" s="T357">pro.h:E</ta>
            <ta e="T360" id="Seg_13503" s="T359">v:Th</ta>
            <ta e="T364" id="Seg_13504" s="T363">0.1.h:A</ta>
            <ta e="T366" id="Seg_13505" s="T365">adv:Time</ta>
            <ta e="T369" id="Seg_13506" s="T368">0.1.h:Th</ta>
            <ta e="T372" id="Seg_13507" s="T371">pro.h:E</ta>
            <ta e="T373" id="Seg_13508" s="T372">0.3:Th</ta>
            <ta e="T376" id="Seg_13509" s="T375">0.3.h:A</ta>
            <ta e="T377" id="Seg_13510" s="T376">0.3.h:A</ta>
            <ta e="T378" id="Seg_13511" s="T377">pro.h:Th</ta>
            <ta e="T381" id="Seg_13512" s="T380">np:Th 0.3.h:Poss</ta>
            <ta e="T383" id="Seg_13513" s="T382">adv:G</ta>
            <ta e="T384" id="Seg_13514" s="T383">np.h:Th 0.3.h:Poss</ta>
            <ta e="T387" id="Seg_13515" s="T386">np:Th 0.3.h:Poss</ta>
            <ta e="T389" id="Seg_13516" s="T388">v:Th</ta>
            <ta e="T390" id="Seg_13517" s="T389">np:P 0.3.h:Poss</ta>
            <ta e="T392" id="Seg_13518" s="T391">v:Th</ta>
            <ta e="T393" id="Seg_13519" s="T392">adv:G</ta>
            <ta e="T396" id="Seg_13520" s="T395">pro.h:Th</ta>
            <ta e="T399" id="Seg_13521" s="T398">pro.h:A</ta>
            <ta e="T401" id="Seg_13522" s="T400">pro.h:B</ta>
            <ta e="T402" id="Seg_13523" s="T401">np:Th 0.1.h:Poss</ta>
            <ta e="T405" id="Seg_13524" s="T404">adv:Time</ta>
            <ta e="T406" id="Seg_13525" s="T405">pro.h:A</ta>
            <ta e="T407" id="Seg_13526" s="T406">pro.h:B</ta>
            <ta e="T408" id="Seg_13527" s="T407">np:Th 0.2.h:Poss</ta>
            <ta e="T410" id="Seg_13528" s="T409">np.h:A 0.3.h:Poss</ta>
            <ta e="T412" id="Seg_13529" s="T411">0.2.h:A 0.3:Th</ta>
            <ta e="T413" id="Seg_13530" s="T412">pro.h:A</ta>
            <ta e="T414" id="Seg_13531" s="T413">0.3:Th</ta>
            <ta e="T415" id="Seg_13532" s="T414">np.h:A 0.3.h:Poss</ta>
            <ta e="T418" id="Seg_13533" s="T417">np:P</ta>
            <ta e="T424" id="Seg_13534" s="T423">0.3.h:A</ta>
            <ta e="T425" id="Seg_13535" s="T424">np:Time</ta>
            <ta e="T426" id="Seg_13536" s="T425">0.3.h:A</ta>
            <ta e="T427" id="Seg_13537" s="T426">np.h:A</ta>
            <ta e="T428" id="Seg_13538" s="T427">pro:P</ta>
            <ta e="T430" id="Seg_13539" s="T429">np:P</ta>
            <ta e="T431" id="Seg_13540" s="T430">pro:P</ta>
            <ta e="T432" id="Seg_13541" s="T431">np.h:B 0.3.h:Poss</ta>
            <ta e="T433" id="Seg_13542" s="T432">0.3.h:A</ta>
            <ta e="T434" id="Seg_13543" s="T433">np.h:A 0.3.h:Poss</ta>
            <ta e="T435" id="Seg_13544" s="T434">np:G</ta>
            <ta e="T437" id="Seg_13545" s="T436">np.h:A</ta>
            <ta e="T438" id="Seg_13546" s="T437">adv:G</ta>
            <ta e="T442" id="Seg_13547" s="T441">0.3.h:A</ta>
            <ta e="T443" id="Seg_13548" s="T442">0.2.h:A</ta>
            <ta e="T444" id="Seg_13549" s="T443">np.h:A 0.1.h:Poss</ta>
            <ta e="T446" id="Seg_13550" s="T445">np.h:A</ta>
            <ta e="T451" id="Seg_13551" s="T450">0.2.h:A</ta>
            <ta e="T452" id="Seg_13552" s="T451">0.1.h:A</ta>
            <ta e="T453" id="Seg_13553" s="T452">pro.h:A</ta>
            <ta e="T454" id="Seg_13554" s="T453">pro.h:R</ta>
            <ta e="T456" id="Seg_13555" s="T455">pro.h:Th</ta>
            <ta e="T460" id="Seg_13556" s="T459">np:P</ta>
            <ta e="T462" id="Seg_13557" s="T461">0.3.h:A</ta>
            <ta e="T464" id="Seg_13558" s="T463">np:Time</ta>
            <ta e="T465" id="Seg_13559" s="T464">pro.h:A</ta>
            <ta e="T466" id="Seg_13560" s="T465">pro.h:R</ta>
            <ta e="T469" id="Seg_13561" s="T468">np:Th</ta>
            <ta e="T471" id="Seg_13562" s="T470">np:Time</ta>
            <ta e="T473" id="Seg_13563" s="T472">0.3.h:A</ta>
            <ta e="T475" id="Seg_13564" s="T474">0.2.h:A</ta>
            <ta e="T476" id="Seg_13565" s="T475">0.1.h:A</ta>
            <ta e="T477" id="Seg_13566" s="T476">pro.h:Th</ta>
            <ta e="T480" id="Seg_13567" s="T479">np:Th 0.3.h:Poss</ta>
            <ta e="T481" id="Seg_13568" s="T480">adv:G</ta>
            <ta e="T483" id="Seg_13569" s="T482">np.h:A</ta>
            <ta e="T484" id="Seg_13570" s="T483">pro.h:B</ta>
            <ta e="T486" id="Seg_13571" s="T485">np:Ins</ta>
            <ta e="T487" id="Seg_13572" s="T486">0.3.h:A 0.3.h:B</ta>
            <ta e="T488" id="Seg_13573" s="T487">np:P</ta>
            <ta e="T489" id="Seg_13574" s="T488">0.3.h:A</ta>
            <ta e="T490" id="Seg_13575" s="T489">np.h:A</ta>
            <ta e="T492" id="Seg_13576" s="T491">np.h:A 0.3.h:Poss</ta>
            <ta e="T494" id="Seg_13577" s="T493">np:P</ta>
            <ta e="T496" id="Seg_13578" s="T495">0.3.h:A</ta>
            <ta e="T498" id="Seg_13579" s="T497">np:P</ta>
            <ta e="T499" id="Seg_13580" s="T498">0.3.h:A</ta>
            <ta e="T500" id="Seg_13581" s="T499">pro.h:A</ta>
            <ta e="T501" id="Seg_13582" s="T500">pro.h:B</ta>
            <ta e="T503" id="Seg_13583" s="T502">0.3.h:A</ta>
            <ta e="T504" id="Seg_13584" s="T503">0.1.h:A</ta>
            <ta e="T505" id="Seg_13585" s="T504">np.h:A 0.3.h:Poss</ta>
            <ta e="T507" id="Seg_13586" s="T506">pro.h:E</ta>
            <ta e="T509" id="Seg_13587" s="T508">np.h:A 0.3.h:Poss</ta>
            <ta e="T511" id="Seg_13588" s="T510">0.1.h:A</ta>
            <ta e="T515" id="Seg_13589" s="T514">pro.h:Th</ta>
            <ta e="T518" id="Seg_13590" s="T517">np:Th 0.3.h:Poss</ta>
            <ta e="T519" id="Seg_13591" s="T518">adv:G</ta>
            <ta e="T522" id="Seg_13592" s="T521">pro.h:Th</ta>
            <ta e="T526" id="Seg_13593" s="T525">np:Th 0.1.h:Poss</ta>
            <ta e="T527" id="Seg_13594" s="T526">0.2.h:A</ta>
            <ta e="T529" id="Seg_13595" s="T528">pro.h:A</ta>
            <ta e="T531" id="Seg_13596" s="T530">0.3.h:A</ta>
            <ta e="T532" id="Seg_13597" s="T531">np.h:Th 0.3.h:Poss</ta>
            <ta e="T536" id="Seg_13598" s="T535">adv:Time</ta>
            <ta e="T537" id="Seg_13599" s="T536">np:Th 0.2.h:Poss</ta>
            <ta e="T538" id="Seg_13600" s="T537">0.1.h:A</ta>
            <ta e="T539" id="Seg_13601" s="T538">pro.h:A</ta>
            <ta e="T541" id="Seg_13602" s="T540">np:Th 0.3.h:Poss</ta>
            <ta e="T542" id="Seg_13603" s="T541">pro.h:A</ta>
            <ta e="T545" id="Seg_13604" s="T544">np:P</ta>
            <ta e="T547" id="Seg_13605" s="T546">0.3.h:A</ta>
            <ta e="T549" id="Seg_13606" s="T548">np:Time</ta>
            <ta e="T550" id="Seg_13607" s="T549">0.3.h:A</ta>
            <ta e="T551" id="Seg_13608" s="T550">np.h:A</ta>
            <ta e="T552" id="Seg_13609" s="T551">np.h:B 0.3.h:Poss</ta>
            <ta e="T554" id="Seg_13610" s="T553">np.h:A 0.3.h:Poss</ta>
            <ta e="T557" id="Seg_13611" s="T556">np:G</ta>
            <ta e="T559" id="Seg_13612" s="T558">0.3.h:A</ta>
            <ta e="T560" id="Seg_13613" s="T559">np.h:A</ta>
            <ta e="T561" id="Seg_13614" s="T560">adv:G</ta>
            <ta e="T566" id="Seg_13615" s="T565">0.2.h:A</ta>
            <ta e="T567" id="Seg_13616" s="T566">np.h:A 0.1.h:Poss</ta>
            <ta e="T569" id="Seg_13617" s="T568">np.h:A</ta>
            <ta e="T571" id="Seg_13618" s="T570">np.h:A</ta>
            <ta e="T573" id="Seg_13619" s="T572">np:P</ta>
            <ta e="T574" id="Seg_13620" s="T573">np.h:A 0.1.h:Poss</ta>
            <ta e="T577" id="Seg_13621" s="T576">pro.h:Th</ta>
            <ta e="T581" id="Seg_13622" s="T580">pro.h:A</ta>
            <ta e="T582" id="Seg_13623" s="T581">np:Time</ta>
            <ta e="T583" id="Seg_13624" s="T582">pro.h:R</ta>
            <ta e="T585" id="Seg_13625" s="T584">np:Th</ta>
            <ta e="T590" id="Seg_13626" s="T589">adv:L</ta>
            <ta e="T591" id="Seg_13627" s="T590">0.3:P</ta>
            <ta e="T592" id="Seg_13628" s="T591">np:Time</ta>
            <ta e="T594" id="Seg_13629" s="T593">0.3.h:A</ta>
            <ta e="T595" id="Seg_13630" s="T594">pro.h:A</ta>
            <ta e="T599" id="Seg_13631" s="T598">0.1.h:A</ta>
            <ta e="T600" id="Seg_13632" s="T599">pro.h:Th</ta>
            <ta e="T604" id="Seg_13633" s="T603">np:Th 0.3.h:Poss</ta>
            <ta e="T605" id="Seg_13634" s="T604">adv:G</ta>
            <ta e="T607" id="Seg_13635" s="T606">np.h:A</ta>
            <ta e="T609" id="Seg_13636" s="T608">pro.h:A</ta>
            <ta e="T610" id="Seg_13637" s="T609">pro.h:B</ta>
            <ta e="T612" id="Seg_13638" s="T611">0.3.h:A</ta>
            <ta e="T613" id="Seg_13639" s="T612">0.1.h:A</ta>
            <ta e="T614" id="Seg_13640" s="T613">np.h:A 0.3.h:Poss</ta>
            <ta e="T616" id="Seg_13641" s="T615">pro.h:E</ta>
            <ta e="T619" id="Seg_13642" s="T618">v:Th</ta>
            <ta e="T621" id="Seg_13643" s="T620">0.1.h:A</ta>
            <ta e="T622" id="Seg_13644" s="T621">0.1.h:A</ta>
            <ta e="T624" id="Seg_13645" s="T623">adv:Time</ta>
            <ta e="T625" id="Seg_13646" s="T624">pro.h:Th</ta>
            <ta e="T628" id="Seg_13647" s="T627">np:Th 0.3.h:Poss</ta>
            <ta e="T629" id="Seg_13648" s="T628">adv:G</ta>
            <ta e="T632" id="Seg_13649" s="T631">pro.h:Th</ta>
            <ta e="T634" id="Seg_13650" s="T633">pro.h:Poss</ta>
            <ta e="T635" id="Seg_13651" s="T634">np:Th</ta>
            <ta e="T636" id="Seg_13652" s="T635">0.2.h:A</ta>
            <ta e="T637" id="Seg_13653" s="T636">adv:G</ta>
            <ta e="T639" id="Seg_13654" s="T638">0.3.h:A</ta>
            <ta e="T640" id="Seg_13655" s="T639">0.3.h:A</ta>
            <ta e="T641" id="Seg_13656" s="T640">np.h:Th 0.3.h:Poss</ta>
            <ta e="T643" id="Seg_13657" s="T642">0.3.h:A</ta>
            <ta e="T644" id="Seg_13658" s="T643">np:Th 0.3.h:Poss</ta>
            <ta e="T645" id="Seg_13659" s="T644">adv:G</ta>
            <ta e="T646" id="Seg_13660" s="T645">0.3.h:A</ta>
            <ta e="T647" id="Seg_13661" s="T646">pro.h:A</ta>
            <ta e="T650" id="Seg_13662" s="T649">np:P</ta>
            <ta e="T652" id="Seg_13663" s="T651">0.3.h:A</ta>
            <ta e="T653" id="Seg_13664" s="T652">np.h:R 0.3.h:Poss</ta>
            <ta e="T654" id="Seg_13665" s="T653">0.3.h:A</ta>
            <ta e="T655" id="Seg_13666" s="T654">0.2.h:A</ta>
            <ta e="T656" id="Seg_13667" s="T655">np:Th 0.1.h:Poss</ta>
            <ta e="T657" id="Seg_13668" s="T656">np.h:A 0.3.h:Poss</ta>
            <ta e="T659" id="Seg_13669" s="T658">0.2.h:A 0.3:P</ta>
            <ta e="T660" id="Seg_13670" s="T659">pro.h:A</ta>
            <ta e="T664" id="Seg_13671" s="T663">0.3.h:A 0.3:P</ta>
            <ta e="T665" id="Seg_13672" s="T664">0.3.h:A</ta>
            <ta e="T666" id="Seg_13673" s="T665">0.2.h:A 0.3:Th</ta>
            <ta e="T668" id="Seg_13674" s="T667">0.1.h:A 0.3:Th</ta>
            <ta e="T669" id="Seg_13675" s="T668">adv:G</ta>
            <ta e="T670" id="Seg_13676" s="T669">0.3.h:A</ta>
            <ta e="T671" id="Seg_13677" s="T670">0.3.h:A</ta>
            <ta e="T674" id="Seg_13678" s="T673">0.3.h:A</ta>
            <ta e="T675" id="Seg_13679" s="T674">0.2.h:A</ta>
            <ta e="T678" id="Seg_13680" s="T677">np.h:A</ta>
            <ta e="T681" id="Seg_13681" s="T680">0.3.h:A</ta>
            <ta e="T682" id="Seg_13682" s="T681">np.h:A 0.1.h:Poss</ta>
            <ta e="T683" id="Seg_13683" s="T682">np:P</ta>
            <ta e="T687" id="Seg_13684" s="T686">0.1.h:A 0.3.h:P</ta>
            <ta e="T689" id="Seg_13685" s="T688">np:G</ta>
            <ta e="T690" id="Seg_13686" s="T689">0.3.h:A</ta>
            <ta e="T691" id="Seg_13687" s="T690">np.h:A</ta>
            <ta e="T694" id="Seg_13688" s="T693">np:Time</ta>
            <ta e="T695" id="Seg_13689" s="T694">0.1.h:A 0.3.h:P</ta>
            <ta e="T696" id="Seg_13690" s="T695">0.2.h:A</ta>
            <ta e="T699" id="Seg_13691" s="T698">np:Th</ta>
            <ta e="T700" id="Seg_13692" s="T699">pro.h:Th</ta>
            <ta e="T701" id="Seg_13693" s="T700">np:Poss</ta>
            <ta e="T702" id="Seg_13694" s="T701">np:G</ta>
            <ta e="T703" id="Seg_13695" s="T702">np:Ins</ta>
            <ta e="T705" id="Seg_13696" s="T704">0.1.h:A</ta>
            <ta e="T706" id="Seg_13697" s="T705">np:Time</ta>
            <ta e="T707" id="Seg_13698" s="T706">0.3.h:Th</ta>
            <ta e="T708" id="Seg_13699" s="T707">np:Time</ta>
            <ta e="T709" id="Seg_13700" s="T708">0.1.h:A 0.3.h:P</ta>
            <ta e="T710" id="Seg_13701" s="T709">np.h:A</ta>
            <ta e="T711" id="Seg_13702" s="T710">np:G</ta>
            <ta e="T712" id="Seg_13703" s="T711">0.3.h:A</ta>
            <ta e="T715" id="Seg_13704" s="T714">pro.h:A</ta>
            <ta e="T718" id="Seg_13705" s="T717">pro.h:Th</ta>
            <ta e="T723" id="Seg_13706" s="T722">np:P</ta>
            <ta e="T725" id="Seg_13707" s="T724">0.2.h:A</ta>
            <ta e="T726" id="Seg_13708" s="T725">adv:Time</ta>
            <ta e="T727" id="Seg_13709" s="T726">np:Time</ta>
            <ta e="T728" id="Seg_13710" s="T727">pro.h:P</ta>
            <ta e="T729" id="Seg_13711" s="T728">0.1.h:A</ta>
            <ta e="T730" id="Seg_13712" s="T729">np.h:A</ta>
            <ta e="T732" id="Seg_13713" s="T731">np:Th</ta>
            <ta e="T733" id="Seg_13714" s="T732">0.3.h:A</ta>
            <ta e="T737" id="Seg_13715" s="T736">np:G</ta>
            <ta e="T738" id="Seg_13716" s="T737">0.3.h:A 0.3:Th</ta>
            <ta e="T740" id="Seg_13717" s="T739">pro.h:Th</ta>
            <ta e="T741" id="Seg_13718" s="T740">adv:G</ta>
            <ta e="T742" id="Seg_13719" s="T741">0.3.h:A</ta>
            <ta e="T743" id="Seg_13720" s="T742">0.3.h:A</ta>
            <ta e="T744" id="Seg_13721" s="T743">0.2.h:Th</ta>
            <ta e="T745" id="Seg_13722" s="T744">np:Time</ta>
            <ta e="T746" id="Seg_13723" s="T745">adv:Time</ta>
            <ta e="T747" id="Seg_13724" s="T746">pro.h:P</ta>
            <ta e="T748" id="Seg_13725" s="T747">0.1.h:A</ta>
            <ta e="T749" id="Seg_13726" s="T748">0.3.h:A</ta>
            <ta e="T750" id="Seg_13727" s="T749">0.3.h:A</ta>
            <ta e="T751" id="Seg_13728" s="T750">np:Com</ta>
            <ta e="T752" id="Seg_13729" s="T751">np:P</ta>
            <ta e="T753" id="Seg_13730" s="T752">0.3.h:A</ta>
            <ta e="T754" id="Seg_13731" s="T753">0.3.h:A</ta>
            <ta e="T755" id="Seg_13732" s="T754">np.h:A</ta>
            <ta e="T757" id="Seg_13733" s="T756">np.h:Poss 0.3.h:Poss</ta>
            <ta e="T758" id="Seg_13734" s="T757">np:G</ta>
            <ta e="T759" id="Seg_13735" s="T758">np.h:P 0.3.h:Poss</ta>
            <ta e="T760" id="Seg_13736" s="T759">np:G</ta>
            <ta e="T762" id="Seg_13737" s="T761">0.3.h:A</ta>
            <ta e="T763" id="Seg_13738" s="T762">np:Time</ta>
            <ta e="T764" id="Seg_13739" s="T763">pro.h:A</ta>
            <ta e="T765" id="Seg_13740" s="T764">pro.h:P</ta>
            <ta e="T769" id="Seg_13741" s="T768">0.3.h:A</ta>
            <ta e="T770" id="Seg_13742" s="T769">np.h:Th</ta>
            <ta e="T771" id="Seg_13743" s="T770">0.3.h:A</ta>
            <ta e="T772" id="Seg_13744" s="T771">0.3.h:A</ta>
            <ta e="T773" id="Seg_13745" s="T772">0.3.h:A </ta>
            <ta e="T776" id="Seg_13746" s="T775">0.3.h:A</ta>
            <ta e="T779" id="Seg_13747" s="T778">0.3.h:P</ta>
            <ta e="T781" id="Seg_13748" s="T780">pro.h:A</ta>
            <ta e="T784" id="Seg_13749" s="T783">0.3.h:A</ta>
            <ta e="T787" id="Seg_13750" s="T786">np.h:Th 0.3.h:Poss</ta>
            <ta e="T788" id="Seg_13751" s="T787">np.h:Th 0.3.h:Poss</ta>
            <ta e="T791" id="Seg_13752" s="T790">np:Poss</ta>
            <ta e="T792" id="Seg_13753" s="T791">np:L</ta>
            <ta e="T793" id="Seg_13754" s="T792">np.h:A 0.3.h:Poss</ta>
            <ta e="T794" id="Seg_13755" s="T793">0.3.h:A</ta>
            <ta e="T797" id="Seg_13756" s="T796">pro.h:A</ta>
            <ta e="T798" id="Seg_13757" s="T797">np.h:R 0.3.h:Poss</ta>
            <ta e="T800" id="Seg_13758" s="T799">pro.h:Poss</ta>
            <ta e="T801" id="Seg_13759" s="T800">np:Th</ta>
            <ta e="T802" id="Seg_13760" s="T801">0.2.h:A</ta>
            <ta e="T804" id="Seg_13761" s="T803">np.h:A 0.3.h:Poss</ta>
            <ta e="T806" id="Seg_13762" s="T805">pro.h:A</ta>
            <ta e="T810" id="Seg_13763" s="T809">0.1.h:Th</ta>
            <ta e="T811" id="Seg_13764" s="T810">0.2.h:A</ta>
            <ta e="T812" id="Seg_13765" s="T811">np:P</ta>
            <ta e="T813" id="Seg_13766" s="T812">np:Poss</ta>
            <ta e="T814" id="Seg_13767" s="T813">np:G</ta>
            <ta e="T815" id="Seg_13768" s="T814">0.3.h:A</ta>
            <ta e="T817" id="Seg_13769" s="T816">0.3.h:P</ta>
            <ta e="T818" id="Seg_13770" s="T817">np.h:A 0.3.h:Poss</ta>
            <ta e="T819" id="Seg_13771" s="T818">np:So 0.3.h:Poss</ta>
            <ta e="T822" id="Seg_13772" s="T821">0.3.h:A</ta>
            <ta e="T823" id="Seg_13773" s="T822">pro.h:A</ta>
            <ta e="T824" id="Seg_13774" s="T823">np.h:R 0.3.h:Poss</ta>
            <ta e="T826" id="Seg_13775" s="T825">pro.h:Poss</ta>
            <ta e="T827" id="Seg_13776" s="T826">np:Th</ta>
            <ta e="T828" id="Seg_13777" s="T827">0.2.h:A</ta>
            <ta e="T829" id="Seg_13778" s="T828">pro.h:A</ta>
            <ta e="T831" id="Seg_13779" s="T830">np.h:G 0.3.h:Poss</ta>
            <ta e="T834" id="Seg_13780" s="T833">0.3.h:A 0.3.h:Th</ta>
            <ta e="T836" id="Seg_13781" s="T835">0.3.h:A 0.3.h:Th</ta>
            <ta e="T840" id="Seg_13782" s="T839">np.h:A 0.3.h:A</ta>
            <ta e="T842" id="Seg_13783" s="T841">np:Th</ta>
            <ta e="T843" id="Seg_13784" s="T842">0.2.h:A</ta>
            <ta e="T844" id="Seg_13785" s="T843">pro.h:A</ta>
            <ta e="T845" id="Seg_13786" s="T844">0.3:Th</ta>
            <ta e="T847" id="Seg_13787" s="T846">np:P</ta>
            <ta e="T850" id="Seg_13788" s="T849">0.3.h:A</ta>
            <ta e="T851" id="Seg_13789" s="T850">0.3.h:A 0.3:P</ta>
            <ta e="T852" id="Seg_13790" s="T851">np:Th</ta>
            <ta e="T854" id="Seg_13791" s="T853">pro.h:A</ta>
            <ta e="T855" id="Seg_13792" s="T854">np.h:R 0.3.h:Poss</ta>
            <ta e="T857" id="Seg_13793" s="T856">0.2.h:A</ta>
            <ta e="T858" id="Seg_13794" s="T857">0.2.h:Th</ta>
            <ta e="T859" id="Seg_13795" s="T858">np:Time</ta>
            <ta e="T861" id="Seg_13796" s="T860">0.2.h:E</ta>
            <ta e="T862" id="Seg_13797" s="T861">pro.h:Th</ta>
            <ta e="T863" id="Seg_13798" s="T862">pro:Com</ta>
            <ta e="T866" id="Seg_13799" s="T865">np:Th</ta>
            <ta e="T869" id="Seg_13800" s="T868">pro.h:A</ta>
            <ta e="T870" id="Seg_13801" s="T869">np:Th</ta>
            <ta e="T871" id="Seg_13802" s="T870">np:G 0.3.h:Poss</ta>
            <ta e="T875" id="Seg_13803" s="T874">0.3.h:P</ta>
            <ta e="T877" id="Seg_13804" s="T876">0.3.h:A</ta>
            <ta e="T878" id="Seg_13805" s="T877">np:Poss</ta>
            <ta e="T879" id="Seg_13806" s="T878">adv:G</ta>
            <ta e="T880" id="Seg_13807" s="T879">np.h:A</ta>
            <ta e="T883" id="Seg_13808" s="T882">adv:G</ta>
            <ta e="T884" id="Seg_13809" s="T883">0.3.h:A 0.3.h:P</ta>
            <ta e="T885" id="Seg_13810" s="T884">np.h:R</ta>
            <ta e="T886" id="Seg_13811" s="T885">0.3.h:A</ta>
            <ta e="T888" id="Seg_13812" s="T887">0.1.h:A 0.3.h:P</ta>
            <ta e="T890" id="Seg_13813" s="T889">np.h:A</ta>
            <ta e="T892" id="Seg_13814" s="T891">0.1.h:A</ta>
            <ta e="T893" id="Seg_13815" s="T892">0.1.h:A 0.3.h:P</ta>
            <ta e="T894" id="Seg_13816" s="T893">pro.h:A</ta>
            <ta e="T895" id="Seg_13817" s="T894">np:P</ta>
            <ta e="T897" id="Seg_13818" s="T896">0.3.h:A</ta>
            <ta e="T899" id="Seg_13819" s="T898">adv:Time</ta>
            <ta e="T900" id="Seg_13820" s="T899">0.1.h:A 0.3.h:P</ta>
            <ta e="T901" id="Seg_13821" s="T900">np:L</ta>
            <ta e="T902" id="Seg_13822" s="T901">0.1.h:A 0.3.h:P</ta>
            <ta e="T904" id="Seg_13823" s="T903">adv:L</ta>
            <ta e="T905" id="Seg_13824" s="T904">np.h:A</ta>
            <ta e="T907" id="Seg_13825" s="T906">adv:L</ta>
            <ta e="T908" id="Seg_13826" s="T907">0.1.h:A 0.3.h:P</ta>
            <ta e="T910" id="Seg_13827" s="T909">0.2.h:A</ta>
            <ta e="T911" id="Seg_13828" s="T910">0.2.h:A</ta>
            <ta e="T912" id="Seg_13829" s="T911">adv:G</ta>
            <ta e="T913" id="Seg_13830" s="T912">pro.h:P</ta>
            <ta e="T914" id="Seg_13831" s="T913">0.1.h:A</ta>
            <ta e="T916" id="Seg_13832" s="T915">np:P</ta>
            <ta e="T918" id="Seg_13833" s="T917">0.2.h:A</ta>
            <ta e="T919" id="Seg_13834" s="T918">pro.h:A</ta>
            <ta e="T922" id="Seg_13835" s="T921">0.1.h:Th</ta>
            <ta e="T923" id="Seg_13836" s="T922">np.h:A</ta>
            <ta e="T925" id="Seg_13837" s="T924">np:G</ta>
            <ta e="T926" id="Seg_13838" s="T925">0.3.h:P</ta>
            <ta e="T927" id="Seg_13839" s="T926">pro.h:A</ta>
            <ta e="T932" id="Seg_13840" s="T931">np.h:P</ta>
            <ta e="T933" id="Seg_13841" s="T932">0.3.h:A</ta>
            <ta e="T934" id="Seg_13842" s="T933">np.h:P</ta>
            <ta e="T935" id="Seg_13843" s="T934">adv:G</ta>
            <ta e="T939" id="Seg_13844" s="T938">0.3.h:A 0.3.h:P</ta>
            <ta e="T941" id="Seg_13845" s="T940">np.h:A 0.3.h:Poss</ta>
            <ta e="T942" id="Seg_13846" s="T941">np:G 0.3.h:Poss</ta>
            <ta e="T944" id="Seg_13847" s="T943">0.3.h:A</ta>
            <ta e="T945" id="Seg_13848" s="T944">pro.h:A</ta>
            <ta e="T948" id="Seg_13849" s="T947">0.3:Th</ta>
            <ta e="T949" id="Seg_13850" s="T948">np:Th 0.2.h:Poss</ta>
            <ta e="T950" id="Seg_13851" s="T949">0.1.h:A</ta>
            <ta e="T952" id="Seg_13852" s="T951">np:P</ta>
            <ta e="T953" id="Seg_13853" s="T952">0.1.h:A</ta>
            <ta e="T954" id="Seg_13854" s="T953">np.h:A</ta>
            <ta e="T956" id="Seg_13855" s="T955">0.3.h:P</ta>
            <ta e="T958" id="Seg_13856" s="T957">0.3.h:A 0.3.h:P</ta>
            <ta e="T959" id="Seg_13857" s="T958">np.h:A</ta>
            <ta e="T962" id="Seg_13858" s="T961">np.h:P 0.3.h:Poss</ta>
            <ta e="T964" id="Seg_13859" s="T963">0.3.h:A</ta>
            <ta e="T966" id="Seg_13860" s="T965">0.3.h:A 0.3.h:P</ta>
            <ta e="T968" id="Seg_13861" s="T967">np.h:R 0.3.h:Poss</ta>
            <ta e="T969" id="Seg_13862" s="T968">0.3.h:A</ta>
            <ta e="T970" id="Seg_13863" s="T969">pro.h:Th</ta>
            <ta e="T971" id="Seg_13864" s="T970">pro:Com</ta>
            <ta e="T974" id="Seg_13865" s="T973">0.2.h:A</ta>
            <ta e="T975" id="Seg_13866" s="T974">0.3.h:A</ta>
            <ta e="T977" id="Seg_13867" s="T976">0.3.h:A</ta>
            <ta e="T979" id="Seg_13868" s="T978">np:P</ta>
            <ta e="T980" id="Seg_13869" s="T979">0.3.h:A</ta>
            <ta e="T981" id="Seg_13870" s="T980">pro.h:Th</ta>
            <ta e="T982" id="Seg_13871" s="T981">adv:G</ta>
            <ta e="T983" id="Seg_13872" s="T982">0.3.h:A</ta>
            <ta e="T986" id="Seg_13873" s="T985">np:Ins</ta>
            <ta e="T987" id="Seg_13874" s="T986">0.3.h:A 0.3.h:P</ta>
            <ta e="T988" id="Seg_13875" s="T987">np:Th 0.3.h:Poss</ta>
            <ta e="T989" id="Seg_13876" s="T988">adv:G</ta>
            <ta e="T990" id="Seg_13877" s="T989">0.3.h:A</ta>
            <ta e="T991" id="Seg_13878" s="T990">0.3.h:A</ta>
            <ta e="T992" id="Seg_13879" s="T991">np:Th 0.3.h:Poss</ta>
            <ta e="T994" id="Seg_13880" s="T993">0.3.h:A</ta>
            <ta e="T995" id="Seg_13881" s="T994">np:Com 0.3.h:Poss</ta>
            <ta e="T996" id="Seg_13882" s="T995">0.3.h:A</ta>
            <ta e="T999" id="Seg_13883" s="T998">0.3.h:A</ta>
            <ta e="T1000" id="Seg_13884" s="T999">0.3.h:A</ta>
            <ta e="T1001" id="Seg_13885" s="T1000">0.3.h:A</ta>
            <ta e="T1003" id="Seg_13886" s="T1002">np:G 0.3.h:Poss</ta>
            <ta e="T1004" id="Seg_13887" s="T1003">np.h:E</ta>
            <ta e="T1007" id="Seg_13888" s="T1006">np.h:Th 0.2.h:Poss</ta>
            <ta e="T1010" id="Seg_13889" s="T1009">np.h:P 0.1.h:Poss</ta>
            <ta e="T1013" id="Seg_13890" s="T1012">0.3.h:A</ta>
            <ta e="T1014" id="Seg_13891" s="T1013">np:Th</ta>
            <ta e="T1015" id="Seg_13892" s="T1014">pro.h:A</ta>
            <ta e="T1016" id="Seg_13893" s="T1015">pro.h:B</ta>
            <ta e="T1017" id="Seg_13894" s="T1016">np:Th</ta>
            <ta e="T1020" id="Seg_13895" s="T1019">0.3.h:A</ta>
            <ta e="T1024" id="Seg_13896" s="T1023">0.3.h:Th</ta>
            <ta e="T1025" id="Seg_13897" s="T1024">pro.h:Th</ta>
            <ta e="T1026" id="Seg_13898" s="T1025">adv:Time</ta>
            <ta e="T1027" id="Seg_13899" s="T1026">pro.h:L</ta>
            <ta e="T1031" id="Seg_13900" s="T1030">0.3.h:Th</ta>
            <ta e="T1032" id="Seg_13901" s="T1031">np.h:Th 0.3.h:Poss</ta>
            <ta e="T1034" id="Seg_13902" s="T1033">pro.h:B</ta>
            <ta e="T1035" id="Seg_13903" s="T1034">np:Ins</ta>
            <ta e="T1037" id="Seg_13904" s="T1036">0.3.h:A</ta>
            <ta e="T1038" id="Seg_13905" s="T1037">pro.h:R</ta>
            <ta e="T1039" id="Seg_13906" s="T1038">0.3.h:A</ta>
            <ta e="T1040" id="Seg_13907" s="T1039">pro.h:G</ta>
            <ta e="T1041" id="Seg_13908" s="T1040">0.2.h:A</ta>
            <ta e="T1042" id="Seg_13909" s="T1041">pro.h:A</ta>
            <ta e="T1043" id="Seg_13910" s="T1042">pro.h:G</ta>
            <ta e="T1044" id="Seg_13911" s="T1043">adv:Time</ta>
            <ta e="T1046" id="Seg_13912" s="T1045">adv:Time</ta>
            <ta e="T1047" id="Seg_13913" s="T1046">np:G</ta>
            <ta e="T1048" id="Seg_13914" s="T1047">0.1.h:A</ta>
            <ta e="T1049" id="Seg_13915" s="T1048">pro.h:G</ta>
            <ta e="T1051" id="Seg_13916" s="T1050">0.1.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_13917" s="T3">v:pred</ta>
            <ta e="T5" id="Seg_13918" s="T4">np.h:S</ta>
            <ta e="T9" id="Seg_13919" s="T8">np.h:S</ta>
            <ta e="T10" id="Seg_13920" s="T9">np.h:S</ta>
            <ta e="T12" id="Seg_13921" s="T11">v:pred</ta>
            <ta e="T14" id="Seg_13922" s="T13">np.h:S</ta>
            <ta e="T16" id="Seg_13923" s="T15">s:purp</ta>
            <ta e="T17" id="Seg_13924" s="T16">v:pred</ta>
            <ta e="T23" id="Seg_13925" s="T22">np.h:S</ta>
            <ta e="T25" id="Seg_13926" s="T24">v:pred</ta>
            <ta e="T26" id="Seg_13927" s="T25">np:O</ta>
            <ta e="T27" id="Seg_13928" s="T26">0.3.h:S v:pred</ta>
            <ta e="T28" id="Seg_13929" s="T27">np:O</ta>
            <ta e="T30" id="Seg_13930" s="T29">np:O</ta>
            <ta e="T32" id="Seg_13931" s="T31">np:O</ta>
            <ta e="T33" id="Seg_13932" s="T32">0.3.h:S v:pred</ta>
            <ta e="T34" id="Seg_13933" s="T33">np:O</ta>
            <ta e="T35" id="Seg_13934" s="T34">0.3.h:S v:pred</ta>
            <ta e="T38" id="Seg_13935" s="T37">np.h:S</ta>
            <ta e="T41" id="Seg_13936" s="T40">v:pred</ta>
            <ta e="T44" id="Seg_13937" s="T43">np.h:S</ta>
            <ta e="T45" id="Seg_13938" s="T44">v:pred</ta>
            <ta e="T47" id="Seg_13939" s="T46">np.h:S</ta>
            <ta e="T48" id="Seg_13940" s="T47">v:pred</ta>
            <ta e="T49" id="Seg_13941" s="T48">pro.h:S</ta>
            <ta e="T51" id="Seg_13942" s="T50">v:pred</ta>
            <ta e="T54" id="Seg_13943" s="T53">pro.h:S</ta>
            <ta e="T58" id="Seg_13944" s="T57">np:O</ta>
            <ta e="T59" id="Seg_13945" s="T58">v:pred</ta>
            <ta e="T64" id="Seg_13946" s="T63">0.3.h:S v:pred</ta>
            <ta e="T66" id="Seg_13947" s="T65">pro.h:O</ta>
            <ta e="T67" id="Seg_13948" s="T66">0.2.h:S v:pred</ta>
            <ta e="T68" id="Seg_13949" s="T67">np.h:S</ta>
            <ta e="T69" id="Seg_13950" s="T68">np:O</ta>
            <ta e="T70" id="Seg_13951" s="T69">v:pred</ta>
            <ta e="T72" id="Seg_13952" s="T71">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T73" id="Seg_13953" s="T72">pro.h:S</ta>
            <ta e="T74" id="Seg_13954" s="T73">v:pred</ta>
            <ta e="T76" id="Seg_13955" s="T75">np.h:S</ta>
            <ta e="T80" id="Seg_13956" s="T79">v:pred</ta>
            <ta e="T83" id="Seg_13957" s="T82">np:S</ta>
            <ta e="T86" id="Seg_13958" s="T85">np:O</ta>
            <ta e="T87" id="Seg_13959" s="T86">0.3.h:S v:pred</ta>
            <ta e="T90" id="Seg_13960" s="T89">0.3.h:S v:pred</ta>
            <ta e="T91" id="Seg_13961" s="T90">np.h:S</ta>
            <ta e="T92" id="Seg_13962" s="T91">np.h:S</ta>
            <ta e="T94" id="Seg_13963" s="T93">s:temp</ta>
            <ta e="T96" id="Seg_13964" s="T95">v:pred</ta>
            <ta e="T98" id="Seg_13965" s="T97">pro.h:S</ta>
            <ta e="T101" id="Seg_13966" s="T100">v:pred</ta>
            <ta e="T104" id="Seg_13967" s="T103">s:temp</ta>
            <ta e="T106" id="Seg_13968" s="T105">0.3.h:S v:pred</ta>
            <ta e="T111" id="Seg_13969" s="T110">np:O</ta>
            <ta e="T112" id="Seg_13970" s="T111">0.3.h:S v:pred</ta>
            <ta e="T113" id="Seg_13971" s="T112">pro:O</ta>
            <ta e="T114" id="Seg_13972" s="T113">0.3.h:S v:pred</ta>
            <ta e="T118" id="Seg_13973" s="T117">np.h:S</ta>
            <ta e="T119" id="Seg_13974" s="T118">np:O</ta>
            <ta e="T120" id="Seg_13975" s="T119">s:temp</ta>
            <ta e="T121" id="Seg_13976" s="T120">v:pred</ta>
            <ta e="T122" id="Seg_13977" s="T121">np:O</ta>
            <ta e="T123" id="Seg_13978" s="T122">0.3.h:S v:pred</ta>
            <ta e="T126" id="Seg_13979" s="T125">0.1.h:S v:pred</ta>
            <ta e="T128" id="Seg_13980" s="T127">pro.h:S</ta>
            <ta e="T131" id="Seg_13981" s="T130">v:pred</ta>
            <ta e="T134" id="Seg_13982" s="T133">np:S</ta>
            <ta e="T137" id="Seg_13983" s="T136">v:pred</ta>
            <ta e="T140" id="Seg_13984" s="T139">0.3:S v:pred</ta>
            <ta e="T142" id="Seg_13985" s="T141">0.3.h:S v:pred</ta>
            <ta e="T144" id="Seg_13986" s="T143">0.3.h:S v:pred</ta>
            <ta e="T145" id="Seg_13987" s="T144">0.3.h:S v:pred</ta>
            <ta e="T148" id="Seg_13988" s="T147">0.3.h:S v:pred</ta>
            <ta e="T151" id="Seg_13989" s="T150">np.h:S</ta>
            <ta e="T152" id="Seg_13990" s="T151">v:pred</ta>
            <ta e="T155" id="Seg_13991" s="T154">np.h:S</ta>
            <ta e="T156" id="Seg_13992" s="T155">v:pred</ta>
            <ta e="T162" id="Seg_13993" s="T156">s:compl</ta>
            <ta e="T163" id="Seg_13994" s="T162">pro.h:S</ta>
            <ta e="T166" id="Seg_13995" s="T165">np:O</ta>
            <ta e="T168" id="Seg_13996" s="T167">v:pred</ta>
            <ta e="T169" id="Seg_13997" s="T168">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T170" id="Seg_13998" s="T169">s:temp</ta>
            <ta e="T172" id="Seg_13999" s="T171">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T175" id="Seg_14000" s="T174">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T177" id="Seg_14001" s="T176">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T178" id="Seg_14002" s="T177">0.3.h:S v:pred</ta>
            <ta e="T180" id="Seg_14003" s="T179">0.3.h:S v:pred</ta>
            <ta e="T183" id="Seg_14004" s="T182">0.3:S v:pred</ta>
            <ta e="T184" id="Seg_14005" s="T183">pro.h:S</ta>
            <ta e="T185" id="Seg_14006" s="T184">np:O</ta>
            <ta e="T186" id="Seg_14007" s="T185">v:pred</ta>
            <ta e="T189" id="Seg_14008" s="T188">0.3.h:S v:pred</ta>
            <ta e="T191" id="Seg_14009" s="T190">pro.h:S</ta>
            <ta e="T193" id="Seg_14010" s="T192">n:pred</ta>
            <ta e="T194" id="Seg_14011" s="T193">cop</ta>
            <ta e="T195" id="Seg_14012" s="T194">s:temp</ta>
            <ta e="T198" id="Seg_14013" s="T197">0.3.h:S v:pred</ta>
            <ta e="T201" id="Seg_14014" s="T200">0.3.h:S v:pred</ta>
            <ta e="T204" id="Seg_14015" s="T203">np.h:S</ta>
            <ta e="T205" id="Seg_14016" s="T204">v:pred</ta>
            <ta e="T206" id="Seg_14017" s="T205">np.h:S</ta>
            <ta e="T207" id="Seg_14018" s="T206">v:pred</ta>
            <ta e="T210" id="Seg_14019" s="T209">0.3.h:S v:pred</ta>
            <ta e="T212" id="Seg_14020" s="T211">np:S</ta>
            <ta e="T215" id="Seg_14021" s="T214">v:pred</ta>
            <ta e="T218" id="Seg_14022" s="T217">0.2.h:S v:pred</ta>
            <ta e="T219" id="Seg_14023" s="T218">pro.h:S</ta>
            <ta e="T220" id="Seg_14024" s="T219">v:pred</ta>
            <ta e="T222" id="Seg_14025" s="T221">np:O</ta>
            <ta e="T223" id="Seg_14026" s="T222">pro.h:S</ta>
            <ta e="T224" id="Seg_14027" s="T223">np:O</ta>
            <ta e="T225" id="Seg_14028" s="T224">v:pred</ta>
            <ta e="T227" id="Seg_14029" s="T226">pro.h:S</ta>
            <ta e="T229" id="Seg_14030" s="T228">np:O</ta>
            <ta e="T231" id="Seg_14031" s="T230">v:pred</ta>
            <ta e="T239" id="Seg_14032" s="T238">0.3.h:S v:pred</ta>
            <ta e="T240" id="Seg_14033" s="T239">np.h:S</ta>
            <ta e="T241" id="Seg_14034" s="T240">v:pred</ta>
            <ta e="T242" id="Seg_14035" s="T241">pro.h:S</ta>
            <ta e="T243" id="Seg_14036" s="T242">v:pred</ta>
            <ta e="T246" id="Seg_14037" s="T245">pro.h:S</ta>
            <ta e="T247" id="Seg_14038" s="T246">v:pred</ta>
            <ta e="T248" id="Seg_14039" s="T247">0.1.h:S v:pred</ta>
            <ta e="T251" id="Seg_14040" s="T248">s:compl</ta>
            <ta e="T253" id="Seg_14041" s="T252">0.3.h:S v:pred</ta>
            <ta e="T254" id="Seg_14042" s="T253">0.3.h:S v:pred</ta>
            <ta e="T255" id="Seg_14043" s="T254">0.3.h:S v:pred</ta>
            <ta e="T258" id="Seg_14044" s="T257">np.h:S</ta>
            <ta e="T259" id="Seg_14045" s="T258">v:pred</ta>
            <ta e="T262" id="Seg_14046" s="T261">np:O</ta>
            <ta e="T263" id="Seg_14047" s="T262">s:temp</ta>
            <ta e="T264" id="Seg_14048" s="T263">0.3.h:S v:pred</ta>
            <ta e="T265" id="Seg_14049" s="T264">0.3.h:S v:pred</ta>
            <ta e="T266" id="Seg_14050" s="T265">0.2.h:S v:pred</ta>
            <ta e="T269" id="Seg_14051" s="T268">pro.h:S</ta>
            <ta e="T270" id="Seg_14052" s="T269">v:pred</ta>
            <ta e="T273" id="Seg_14053" s="T272">0.3.h:S v:pred</ta>
            <ta e="T274" id="Seg_14054" s="T273">np.h:S</ta>
            <ta e="T275" id="Seg_14055" s="T274">v:pred</ta>
            <ta e="T276" id="Seg_14056" s="T275">pro.h:S</ta>
            <ta e="T278" id="Seg_14057" s="T277">v:pred</ta>
            <ta e="T281" id="Seg_14058" s="T280">np.h:S</ta>
            <ta e="T282" id="Seg_14059" s="T281">n:pred</ta>
            <ta e="T283" id="Seg_14060" s="T282">pro.h:S</ta>
            <ta e="T284" id="Seg_14061" s="T283">pro.h:O</ta>
            <ta e="T285" id="Seg_14062" s="T284">v:pred</ta>
            <ta e="T287" id="Seg_14063" s="T286">pro.h:O</ta>
            <ta e="T288" id="Seg_14064" s="T287">0.3.h:S v:pred</ta>
            <ta e="T290" id="Seg_14065" s="T289">pro.h:S</ta>
            <ta e="T292" id="Seg_14066" s="T291">np:O</ta>
            <ta e="T293" id="Seg_14067" s="T292">v:pred</ta>
            <ta e="T294" id="Seg_14068" s="T293">np:S</ta>
            <ta e="T295" id="Seg_14069" s="T294">v:pred</ta>
            <ta e="T299" id="Seg_14070" s="T298">pro.h:S</ta>
            <ta e="T301" id="Seg_14071" s="T300">np:O</ta>
            <ta e="T303" id="Seg_14072" s="T302">v:pred</ta>
            <ta e="T304" id="Seg_14073" s="T303">pro.h:S</ta>
            <ta e="T306" id="Seg_14074" s="T305">v:pred</ta>
            <ta e="T308" id="Seg_14075" s="T307">pro.h:S</ta>
            <ta e="T309" id="Seg_14076" s="T308">v:pred</ta>
            <ta e="T310" id="Seg_14077" s="T309">0.1.h:S v:pred</ta>
            <ta e="T312" id="Seg_14078" s="T311">0.2.h:S v:pred</ta>
            <ta e="T313" id="Seg_14079" s="T312">pro.h:S</ta>
            <ta e="T315" id="Seg_14080" s="T314">v:pred</ta>
            <ta e="T320" id="Seg_14081" s="T319">pro.h:S</ta>
            <ta e="T321" id="Seg_14082" s="T320">pro.h:O</ta>
            <ta e="T323" id="Seg_14083" s="T322">v:pred</ta>
            <ta e="T324" id="Seg_14084" s="T323">np:O</ta>
            <ta e="T326" id="Seg_14085" s="T325">0.2.h:S v:pred</ta>
            <ta e="T327" id="Seg_14086" s="T326">pro.h:S</ta>
            <ta e="T329" id="Seg_14087" s="T328">np:O</ta>
            <ta e="T331" id="Seg_14088" s="T330">v:pred</ta>
            <ta e="T335" id="Seg_14089" s="T331">s:cond</ta>
            <ta e="T337" id="Seg_14090" s="T336">pro.h:S</ta>
            <ta e="T339" id="Seg_14091" s="T338">v:pred</ta>
            <ta e="T340" id="Seg_14092" s="T339">pro.h:O</ta>
            <ta e="T341" id="Seg_14093" s="T340">0.2.h:S v:pred</ta>
            <ta e="T342" id="Seg_14094" s="T341">pro.h:S</ta>
            <ta e="T343" id="Seg_14095" s="T342">v:pred</ta>
            <ta e="T344" id="Seg_14096" s="T343">pro.h:O</ta>
            <ta e="T346" id="Seg_14097" s="T345">0.1.h:S v:pred</ta>
            <ta e="T348" id="Seg_14098" s="T347">0.1.h:S v:pred</ta>
            <ta e="T349" id="Seg_14099" s="T348">np.h:S</ta>
            <ta e="T350" id="Seg_14100" s="T349">v:pred</ta>
            <ta e="T351" id="Seg_14101" s="T350">pro.h:S</ta>
            <ta e="T352" id="Seg_14102" s="T351">pro.h:O</ta>
            <ta e="T353" id="Seg_14103" s="T352">v:pred</ta>
            <ta e="T354" id="Seg_14104" s="T353">0.3.h:S v:pred</ta>
            <ta e="T355" id="Seg_14105" s="T354">0.1.h:S v:pred</ta>
            <ta e="T356" id="Seg_14106" s="T355">np.h:S</ta>
            <ta e="T357" id="Seg_14107" s="T356">v:pred</ta>
            <ta e="T358" id="Seg_14108" s="T357">pro.h:S</ta>
            <ta e="T359" id="Seg_14109" s="T358">v:pred</ta>
            <ta e="T360" id="Seg_14110" s="T359">v:O</ta>
            <ta e="T361" id="Seg_14111" s="T360">ptcl:pred</ta>
            <ta e="T364" id="Seg_14112" s="T363">0.1.h:S v:pred</ta>
            <ta e="T369" id="Seg_14113" s="T368">0.1.h:S v:pred</ta>
            <ta e="T373" id="Seg_14114" s="T372">0.3:S v:pred</ta>
            <ta e="T376" id="Seg_14115" s="T375">0.3.h:S v:pred</ta>
            <ta e="T377" id="Seg_14116" s="T376">s:purp</ta>
            <ta e="T378" id="Seg_14117" s="T377">pro.h:S</ta>
            <ta e="T380" id="Seg_14118" s="T379">v:pred</ta>
            <ta e="T384" id="Seg_14119" s="T383">np.h:S</ta>
            <ta e="T386" id="Seg_14120" s="T385">v:pred</ta>
            <ta e="T388" id="Seg_14121" s="T387">ptcl:pred</ta>
            <ta e="T389" id="Seg_14122" s="T388">v:O</ta>
            <ta e="T391" id="Seg_14123" s="T390">ptcl:pred</ta>
            <ta e="T392" id="Seg_14124" s="T391">v:O</ta>
            <ta e="T398" id="Seg_14125" s="T393">s:cond</ta>
            <ta e="T399" id="Seg_14126" s="T398">pro.h:S</ta>
            <ta e="T402" id="Seg_14127" s="T401">np:O</ta>
            <ta e="T403" id="Seg_14128" s="T402">v:pred</ta>
            <ta e="T406" id="Seg_14129" s="T405">pro.h:S</ta>
            <ta e="T408" id="Seg_14130" s="T407">np:O</ta>
            <ta e="T409" id="Seg_14131" s="T408">v:pred</ta>
            <ta e="T410" id="Seg_14132" s="T409">np.h:S</ta>
            <ta e="T411" id="Seg_14133" s="T410">v:pred</ta>
            <ta e="T412" id="Seg_14134" s="T411">0.2.h:S v:pred 0.3:O</ta>
            <ta e="T413" id="Seg_14135" s="T412">pro.h:S</ta>
            <ta e="T414" id="Seg_14136" s="T413">v:pred 0.3:O</ta>
            <ta e="T415" id="Seg_14137" s="T414">np.h:S</ta>
            <ta e="T417" id="Seg_14138" s="T416">v:pred</ta>
            <ta e="T418" id="Seg_14139" s="T417">np:S</ta>
            <ta e="T420" id="Seg_14140" s="T419">v:pred</ta>
            <ta e="T423" id="Seg_14141" s="T422">s:purp</ta>
            <ta e="T424" id="Seg_14142" s="T423">0.3.h:S v:pred</ta>
            <ta e="T426" id="Seg_14143" s="T425">0.3.h:S v:pred</ta>
            <ta e="T427" id="Seg_14144" s="T426">np.h:S</ta>
            <ta e="T428" id="Seg_14145" s="T427">pro:O</ta>
            <ta e="T429" id="Seg_14146" s="T428">v:pred</ta>
            <ta e="T430" id="Seg_14147" s="T429">np:O</ta>
            <ta e="T431" id="Seg_14148" s="T430">pro:O</ta>
            <ta e="T432" id="Seg_14149" s="T431">np.h:O</ta>
            <ta e="T433" id="Seg_14150" s="T432">0.3.h:S v:pred</ta>
            <ta e="T434" id="Seg_14151" s="T433">np.h:S</ta>
            <ta e="T436" id="Seg_14152" s="T435">v:pred</ta>
            <ta e="T437" id="Seg_14153" s="T436">np.h:S</ta>
            <ta e="T439" id="Seg_14154" s="T438">v:pred</ta>
            <ta e="T442" id="Seg_14155" s="T441">0.3.h:S v:pred</ta>
            <ta e="T443" id="Seg_14156" s="T442">0.2.h:S v:pred</ta>
            <ta e="T444" id="Seg_14157" s="T443">np.h:S</ta>
            <ta e="T445" id="Seg_14158" s="T444">v:pred</ta>
            <ta e="T446" id="Seg_14159" s="T445">np.h:S</ta>
            <ta e="T448" id="Seg_14160" s="T447">v:pred</ta>
            <ta e="T451" id="Seg_14161" s="T450">0.2.h:S v:pred</ta>
            <ta e="T452" id="Seg_14162" s="T451">0.1.h:S v:pred</ta>
            <ta e="T453" id="Seg_14163" s="T452">pro.h:S</ta>
            <ta e="T455" id="Seg_14164" s="T454">v:pred</ta>
            <ta e="T456" id="Seg_14165" s="T455">pro.h:S</ta>
            <ta e="T457" id="Seg_14166" s="T456">n:pred</ta>
            <ta e="T458" id="Seg_14167" s="T457">cop</ta>
            <ta e="T460" id="Seg_14168" s="T459">np:O</ta>
            <ta e="T462" id="Seg_14169" s="T461">0.3.h:S v:pred</ta>
            <ta e="T465" id="Seg_14170" s="T464">pro.h:S</ta>
            <ta e="T467" id="Seg_14171" s="T466">v:pred</ta>
            <ta e="T469" id="Seg_14172" s="T468">np:O</ta>
            <ta e="T473" id="Seg_14173" s="T472">0.3.h:S v:pred</ta>
            <ta e="T475" id="Seg_14174" s="T474">0.2.h:S v:pred</ta>
            <ta e="T476" id="Seg_14175" s="T475">0.1.h:S v:pred</ta>
            <ta e="T477" id="Seg_14176" s="T476">pro.h:S</ta>
            <ta e="T479" id="Seg_14177" s="T478">v:pred</ta>
            <ta e="T483" id="Seg_14178" s="T482">np.h:S</ta>
            <ta e="T484" id="Seg_14179" s="T483">pro.h:O</ta>
            <ta e="T485" id="Seg_14180" s="T484">v:pred</ta>
            <ta e="T487" id="Seg_14181" s="T486">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T488" id="Seg_14182" s="T487">np:O</ta>
            <ta e="T489" id="Seg_14183" s="T488">0.3.h:S v:pred</ta>
            <ta e="T490" id="Seg_14184" s="T489">np.h:S</ta>
            <ta e="T491" id="Seg_14185" s="T490">v:pred</ta>
            <ta e="T492" id="Seg_14186" s="T491">np.h:S</ta>
            <ta e="T493" id="Seg_14187" s="T492">v:pred</ta>
            <ta e="T494" id="Seg_14188" s="T493">np:O</ta>
            <ta e="T496" id="Seg_14189" s="T495">0.3.h:S v:pred</ta>
            <ta e="T498" id="Seg_14190" s="T497">np:O</ta>
            <ta e="T499" id="Seg_14191" s="T498">0.3.h:S v:pred</ta>
            <ta e="T500" id="Seg_14192" s="T499">pro.h:S</ta>
            <ta e="T501" id="Seg_14193" s="T500">pro.h:O</ta>
            <ta e="T502" id="Seg_14194" s="T501">v:pred</ta>
            <ta e="T503" id="Seg_14195" s="T502">0.3.h:S v:pred</ta>
            <ta e="T504" id="Seg_14196" s="T503">0.1.h:S v:pred</ta>
            <ta e="T505" id="Seg_14197" s="T504">np.h:S</ta>
            <ta e="T506" id="Seg_14198" s="T505">v:pred</ta>
            <ta e="T507" id="Seg_14199" s="T506">pro.h:S</ta>
            <ta e="T508" id="Seg_14200" s="T507">v:pred</ta>
            <ta e="T509" id="Seg_14201" s="T508">np.h:S</ta>
            <ta e="T510" id="Seg_14202" s="T509">v:pred</ta>
            <ta e="T511" id="Seg_14203" s="T510">0.1.h:S v:pred</ta>
            <ta e="T515" id="Seg_14204" s="T514">pro.h:S</ta>
            <ta e="T517" id="Seg_14205" s="T516">v:pred</ta>
            <ta e="T524" id="Seg_14206" s="T520">s:cond</ta>
            <ta e="T526" id="Seg_14207" s="T525">np:O</ta>
            <ta e="T527" id="Seg_14208" s="T526">0.2.h:S v:pred</ta>
            <ta e="T529" id="Seg_14209" s="T528">pro.h:S</ta>
            <ta e="T530" id="Seg_14210" s="T529">v:pred</ta>
            <ta e="T531" id="Seg_14211" s="T530">s:purp</ta>
            <ta e="T532" id="Seg_14212" s="T531">np.h:S</ta>
            <ta e="T534" id="Seg_14213" s="T533">v:pred</ta>
            <ta e="T537" id="Seg_14214" s="T536">np:O</ta>
            <ta e="T538" id="Seg_14215" s="T537">0.1.h:S v:pred</ta>
            <ta e="T539" id="Seg_14216" s="T538">pro.h:S</ta>
            <ta e="T540" id="Seg_14217" s="T539">v:pred</ta>
            <ta e="T541" id="Seg_14218" s="T540">np:O</ta>
            <ta e="T542" id="Seg_14219" s="T541">pro.h:S</ta>
            <ta e="T544" id="Seg_14220" s="T543">v:pred</ta>
            <ta e="T545" id="Seg_14221" s="T544">np:S</ta>
            <ta e="T546" id="Seg_14222" s="T545">v:pred</ta>
            <ta e="T547" id="Seg_14223" s="T546">0.3.h:S v:pred</ta>
            <ta e="T550" id="Seg_14224" s="T549">0.3.h:S v:pred</ta>
            <ta e="T551" id="Seg_14225" s="T550">np.h:S</ta>
            <ta e="T552" id="Seg_14226" s="T551">np.h:O</ta>
            <ta e="T553" id="Seg_14227" s="T552">v:pred</ta>
            <ta e="T554" id="Seg_14228" s="T553">np.h:S</ta>
            <ta e="T555" id="Seg_14229" s="T554">v:pred</ta>
            <ta e="T559" id="Seg_14230" s="T558">0.3.h:S v:pred</ta>
            <ta e="T560" id="Seg_14231" s="T559">np.h:S</ta>
            <ta e="T562" id="Seg_14232" s="T561">v:pred</ta>
            <ta e="T566" id="Seg_14233" s="T565">0.2.h:S v:pred</ta>
            <ta e="T567" id="Seg_14234" s="T566">np.h:S</ta>
            <ta e="T568" id="Seg_14235" s="T567">v:pred</ta>
            <ta e="T569" id="Seg_14236" s="T568">np.h:S</ta>
            <ta e="T570" id="Seg_14237" s="T569">v:pred</ta>
            <ta e="T571" id="Seg_14238" s="T570">np.h:S</ta>
            <ta e="T572" id="Seg_14239" s="T571">v:pred</ta>
            <ta e="T573" id="Seg_14240" s="T572">np:O</ta>
            <ta e="T574" id="Seg_14241" s="T573">np.h:S</ta>
            <ta e="T576" id="Seg_14242" s="T575">v:pred</ta>
            <ta e="T577" id="Seg_14243" s="T576">pro.h:S</ta>
            <ta e="T578" id="Seg_14244" s="T577">n:pred</ta>
            <ta e="T579" id="Seg_14245" s="T578">cop</ta>
            <ta e="T581" id="Seg_14246" s="T580">pro.h:S</ta>
            <ta e="T584" id="Seg_14247" s="T583">v:pred</ta>
            <ta e="T585" id="Seg_14248" s="T584">np:O</ta>
            <ta e="T591" id="Seg_14249" s="T590">0.3:S v:pred</ta>
            <ta e="T594" id="Seg_14250" s="T593">0.3.h:S v:pred</ta>
            <ta e="T595" id="Seg_14251" s="T594">pro.h:S</ta>
            <ta e="T597" id="Seg_14252" s="T596">v:pred</ta>
            <ta e="T599" id="Seg_14253" s="T598">0.1.h:S v:pred</ta>
            <ta e="T600" id="Seg_14254" s="T599">pro.h:S</ta>
            <ta e="T602" id="Seg_14255" s="T601">v:pred</ta>
            <ta e="T607" id="Seg_14256" s="T606">np.h:S</ta>
            <ta e="T608" id="Seg_14257" s="T607">v:pred</ta>
            <ta e="T609" id="Seg_14258" s="T608">pro.h:S</ta>
            <ta e="T610" id="Seg_14259" s="T609">pro.h:O</ta>
            <ta e="T611" id="Seg_14260" s="T610">v:pred</ta>
            <ta e="T612" id="Seg_14261" s="T611">0.3.h:S v:pred</ta>
            <ta e="T613" id="Seg_14262" s="T612">0.1.h:S v:pred</ta>
            <ta e="T614" id="Seg_14263" s="T613">np.h:S</ta>
            <ta e="T615" id="Seg_14264" s="T614">v:pred</ta>
            <ta e="T616" id="Seg_14265" s="T615">pro.h:S</ta>
            <ta e="T617" id="Seg_14266" s="T616">v:pred</ta>
            <ta e="T618" id="Seg_14267" s="T617">ptcl:pred</ta>
            <ta e="T619" id="Seg_14268" s="T618">v:O</ta>
            <ta e="T621" id="Seg_14269" s="T620">0.1.h:S v:pred</ta>
            <ta e="T622" id="Seg_14270" s="T621">0.1.h:S v:pred</ta>
            <ta e="T625" id="Seg_14271" s="T624">pro.h:S</ta>
            <ta e="T627" id="Seg_14272" s="T626">v:pred</ta>
            <ta e="T633" id="Seg_14273" s="T630">s:cond</ta>
            <ta e="T635" id="Seg_14274" s="T634">np:O</ta>
            <ta e="T636" id="Seg_14275" s="T635">0.2.h:S v:pred</ta>
            <ta e="T639" id="Seg_14276" s="T638">0.3.h:S v:pred</ta>
            <ta e="T640" id="Seg_14277" s="T639">s:purp</ta>
            <ta e="T641" id="Seg_14278" s="T640">np.h:O</ta>
            <ta e="T642" id="Seg_14279" s="T641">s:temp</ta>
            <ta e="T643" id="Seg_14280" s="T642">0.3.h:S v:pred</ta>
            <ta e="T644" id="Seg_14281" s="T643">np:O</ta>
            <ta e="T646" id="Seg_14282" s="T645">0.3.h:S v:pred</ta>
            <ta e="T647" id="Seg_14283" s="T646">pro.h:S</ta>
            <ta e="T649" id="Seg_14284" s="T648">v:pred</ta>
            <ta e="T650" id="Seg_14285" s="T649">np:O</ta>
            <ta e="T652" id="Seg_14286" s="T651">0.3.h:S v:pred</ta>
            <ta e="T654" id="Seg_14287" s="T653">0.3.h:S v:pred</ta>
            <ta e="T655" id="Seg_14288" s="T654">0.2.h:S v:pred</ta>
            <ta e="T656" id="Seg_14289" s="T655">np:O</ta>
            <ta e="T657" id="Seg_14290" s="T656">np.h:S</ta>
            <ta e="T658" id="Seg_14291" s="T657">v:pred</ta>
            <ta e="T659" id="Seg_14292" s="T658">0.2.h:S v:pred 0.3:O</ta>
            <ta e="T660" id="Seg_14293" s="T659">pro.h:S</ta>
            <ta e="T662" id="Seg_14294" s="T661">v:pred</ta>
            <ta e="T664" id="Seg_14295" s="T663">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T665" id="Seg_14296" s="T664">0.3.h:S v:pred</ta>
            <ta e="T666" id="Seg_14297" s="T665">0.2.h:S v:pred 0.3:O</ta>
            <ta e="T668" id="Seg_14298" s="T667">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T670" id="Seg_14299" s="T668">s:temp</ta>
            <ta e="T671" id="Seg_14300" s="T670">0.3.h:S v:pred</ta>
            <ta e="T674" id="Seg_14301" s="T673">0.3.h:S v:pred</ta>
            <ta e="T675" id="Seg_14302" s="T674">0.2.h:S v:pred</ta>
            <ta e="T678" id="Seg_14303" s="T677">np.h:S</ta>
            <ta e="T680" id="Seg_14304" s="T679">v:pred</ta>
            <ta e="T681" id="Seg_14305" s="T680">0.3.h:S v:pred</ta>
            <ta e="T682" id="Seg_14306" s="T681">np.h:S</ta>
            <ta e="T683" id="Seg_14307" s="T682">np:O</ta>
            <ta e="T685" id="Seg_14308" s="T684">v:pred</ta>
            <ta e="T686" id="Seg_14309" s="T685">s:temp</ta>
            <ta e="T687" id="Seg_14310" s="T686">0.1.h:S v:pred 0.3.h:O</ta>
            <ta e="T690" id="Seg_14311" s="T689">0.3.h:S v:pred</ta>
            <ta e="T691" id="Seg_14312" s="T690">np.h:S</ta>
            <ta e="T692" id="Seg_14313" s="T691">v:pred</ta>
            <ta e="T695" id="Seg_14314" s="T694">0.1.h:S v:pred 0.3.h:O</ta>
            <ta e="T696" id="Seg_14315" s="T695">0.2.h:S v:pred</ta>
            <ta e="T699" id="Seg_14316" s="T698">np:O</ta>
            <ta e="T700" id="Seg_14317" s="T699">pro.h:O</ta>
            <ta e="T705" id="Seg_14318" s="T704">0.1.h:S v:pred</ta>
            <ta e="T707" id="Seg_14319" s="T706">0.3.h:S v:pred</ta>
            <ta e="T709" id="Seg_14320" s="T708">0.1.h:S v:pred 0.3.h:O</ta>
            <ta e="T710" id="Seg_14321" s="T709">np.h:S</ta>
            <ta e="T712" id="Seg_14322" s="T711">s:temp</ta>
            <ta e="T714" id="Seg_14323" s="T713">v:pred</ta>
            <ta e="T715" id="Seg_14324" s="T714">pro.h:S</ta>
            <ta e="T716" id="Seg_14325" s="T715">v:pred</ta>
            <ta e="T720" id="Seg_14326" s="T716">s:compl</ta>
            <ta e="T723" id="Seg_14327" s="T722">np:O</ta>
            <ta e="T725" id="Seg_14328" s="T724">0.2.h:S v:pred</ta>
            <ta e="T728" id="Seg_14329" s="T727">pro.h:O</ta>
            <ta e="T729" id="Seg_14330" s="T728">0.1.h:S v:pred</ta>
            <ta e="T730" id="Seg_14331" s="T729">np.h:S</ta>
            <ta e="T731" id="Seg_14332" s="T730">v:pred</ta>
            <ta e="T732" id="Seg_14333" s="T731">np:O</ta>
            <ta e="T733" id="Seg_14334" s="T732">0.3.h:S v:pred</ta>
            <ta e="T738" id="Seg_14335" s="T737">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T740" id="Seg_14336" s="T739">pro.h:O</ta>
            <ta e="T742" id="Seg_14337" s="T741">0.3.h:S v:pred</ta>
            <ta e="T743" id="Seg_14338" s="T742">0.3.h:S v:pred</ta>
            <ta e="T744" id="Seg_14339" s="T743">0.2.h:S v:pred</ta>
            <ta e="T747" id="Seg_14340" s="T746">pro.h:O</ta>
            <ta e="T748" id="Seg_14341" s="T747">0.1.h:S v:pred</ta>
            <ta e="T749" id="Seg_14342" s="T748">0.3.h:S v:pred</ta>
            <ta e="T750" id="Seg_14343" s="T749">s:purp</ta>
            <ta e="T752" id="Seg_14344" s="T751">np:O</ta>
            <ta e="T753" id="Seg_14345" s="T752">s:temp</ta>
            <ta e="T754" id="Seg_14346" s="T753">0.3.h:S v:pred</ta>
            <ta e="T755" id="Seg_14347" s="T754">np.h:S</ta>
            <ta e="T756" id="Seg_14348" s="T755">v:pred</ta>
            <ta e="T759" id="Seg_14349" s="T758">np.h:O</ta>
            <ta e="T762" id="Seg_14350" s="T761">0.3.h:S v:pred</ta>
            <ta e="T764" id="Seg_14351" s="T763">pro.h:S</ta>
            <ta e="T765" id="Seg_14352" s="T764">pro.h:O</ta>
            <ta e="T766" id="Seg_14353" s="T765">v:pred</ta>
            <ta e="T769" id="Seg_14354" s="T768">0.3.h:S v:pred</ta>
            <ta e="T770" id="Seg_14355" s="T769">np.h:O</ta>
            <ta e="T771" id="Seg_14356" s="T770">0.3.h:S v:pred</ta>
            <ta e="T772" id="Seg_14357" s="T771">s:temp</ta>
            <ta e="T773" id="Seg_14358" s="T772">0.3.h:S v:pred</ta>
            <ta e="T775" id="Seg_14359" s="T774">s:purp</ta>
            <ta e="T776" id="Seg_14360" s="T775">0.3.h:S v:pred</ta>
            <ta e="T779" id="Seg_14361" s="T778">0.3.h:S v:pred</ta>
            <ta e="T781" id="Seg_14362" s="T780">pro.h:S</ta>
            <ta e="T782" id="Seg_14363" s="T781">v:pred</ta>
            <ta e="T784" id="Seg_14364" s="T783">0.3.h:S v:pred</ta>
            <ta e="T787" id="Seg_14365" s="T786">np.h:S</ta>
            <ta e="T788" id="Seg_14366" s="T787">np.h:S</ta>
            <ta e="T790" id="Seg_14367" s="T789">v:pred</ta>
            <ta e="T793" id="Seg_14368" s="T792">np.h:S</ta>
            <ta e="T794" id="Seg_14369" s="T793">s:purp</ta>
            <ta e="T796" id="Seg_14370" s="T795">v:pred</ta>
            <ta e="T797" id="Seg_14371" s="T796">pro.h:S</ta>
            <ta e="T799" id="Seg_14372" s="T798">v:pred</ta>
            <ta e="T801" id="Seg_14373" s="T800">np:O</ta>
            <ta e="T802" id="Seg_14374" s="T801">0.2.h:S v:pred</ta>
            <ta e="T804" id="Seg_14375" s="T803">np.h:S</ta>
            <ta e="T805" id="Seg_14376" s="T804">v:pred</ta>
            <ta e="T806" id="Seg_14377" s="T805">pro.h:S</ta>
            <ta e="T807" id="Seg_14378" s="T806">v:pred</ta>
            <ta e="T810" id="Seg_14379" s="T807">s:compl</ta>
            <ta e="T811" id="Seg_14380" s="T810">0.2.h:S v:pred</ta>
            <ta e="T812" id="Seg_14381" s="T811">np:O</ta>
            <ta e="T815" id="Seg_14382" s="T814">0.3.h:S v:pred</ta>
            <ta e="T817" id="Seg_14383" s="T816">0.3.h:S v:pred</ta>
            <ta e="T818" id="Seg_14384" s="T817">np.h:S</ta>
            <ta e="T821" id="Seg_14385" s="T820">v:pred</ta>
            <ta e="T822" id="Seg_14386" s="T821">s:purp</ta>
            <ta e="T823" id="Seg_14387" s="T822">pro.h:S</ta>
            <ta e="T825" id="Seg_14388" s="T824">v:pred</ta>
            <ta e="T827" id="Seg_14389" s="T826">np:O</ta>
            <ta e="T828" id="Seg_14390" s="T827">0.2.h:S v:pred</ta>
            <ta e="T829" id="Seg_14391" s="T828">pro.h:S</ta>
            <ta e="T830" id="Seg_14392" s="T829">v:pred</ta>
            <ta e="T834" id="Seg_14393" s="T833">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T836" id="Seg_14394" s="T835">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T840" id="Seg_14395" s="T839">np.h:S</ta>
            <ta e="T841" id="Seg_14396" s="T840">v:pred</ta>
            <ta e="T842" id="Seg_14397" s="T841">np:O</ta>
            <ta e="T843" id="Seg_14398" s="T842">0.2.h:S v:pred</ta>
            <ta e="T844" id="Seg_14399" s="T843">pro.h:S</ta>
            <ta e="T845" id="Seg_14400" s="T844">v:pred 0.3:O</ta>
            <ta e="T847" id="Seg_14401" s="T846">np:O</ta>
            <ta e="T850" id="Seg_14402" s="T849">0.3.h:S v:pred</ta>
            <ta e="T851" id="Seg_14403" s="T850">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T852" id="Seg_14404" s="T851">np:S</ta>
            <ta e="T853" id="Seg_14405" s="T852">v:pred</ta>
            <ta e="T854" id="Seg_14406" s="T853">pro.h:S</ta>
            <ta e="T856" id="Seg_14407" s="T855">v:pred</ta>
            <ta e="T857" id="Seg_14408" s="T856">s:temp</ta>
            <ta e="T858" id="Seg_14409" s="T857">0.2.h:S v:pred</ta>
            <ta e="T861" id="Seg_14410" s="T860">0.2.h:S v:pred</ta>
            <ta e="T862" id="Seg_14411" s="T861">pro.h:S</ta>
            <ta e="T865" id="Seg_14412" s="T864">v:pred</ta>
            <ta e="T866" id="Seg_14413" s="T865">np:S</ta>
            <ta e="T868" id="Seg_14414" s="T867">v:pred</ta>
            <ta e="T869" id="Seg_14415" s="T868">pro.h:S</ta>
            <ta e="T870" id="Seg_14416" s="T869">np:O</ta>
            <ta e="T872" id="Seg_14417" s="T871">v:pred</ta>
            <ta e="T875" id="Seg_14418" s="T874">0.3.h:S v:pred</ta>
            <ta e="T877" id="Seg_14419" s="T876">0.3.h:S v:pred</ta>
            <ta e="T880" id="Seg_14420" s="T879">np.h:S</ta>
            <ta e="T881" id="Seg_14421" s="T880">v:pred</ta>
            <ta e="T882" id="Seg_14422" s="T881">s:temp</ta>
            <ta e="T884" id="Seg_14423" s="T883">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T886" id="Seg_14424" s="T885">0.3.h:S v:pred</ta>
            <ta e="T888" id="Seg_14425" s="T887">0.1.h:S v:pred 0.3.h:O</ta>
            <ta e="T890" id="Seg_14426" s="T889">np.h:S</ta>
            <ta e="T891" id="Seg_14427" s="T890">v:pred</ta>
            <ta e="T892" id="Seg_14428" s="T891">s:temp</ta>
            <ta e="T893" id="Seg_14429" s="T892">0.1.h:S v:pred 0.3.h:O</ta>
            <ta e="T894" id="Seg_14430" s="T893">pro.h:S</ta>
            <ta e="T895" id="Seg_14431" s="T894">np:O</ta>
            <ta e="T896" id="Seg_14432" s="T895">v:pred</ta>
            <ta e="T897" id="Seg_14433" s="T896">0.3.h:S v:pred</ta>
            <ta e="T900" id="Seg_14434" s="T899">0.1.h:S v:pred 0.3.h:O</ta>
            <ta e="T902" id="Seg_14435" s="T901">0.1.h:S v:pred 0.3.h:O</ta>
            <ta e="T905" id="Seg_14436" s="T904">np.h:S</ta>
            <ta e="T906" id="Seg_14437" s="T905">v:pred</ta>
            <ta e="T908" id="Seg_14438" s="T907">0.1.h:S v:pred 0.3.h:O</ta>
            <ta e="T910" id="Seg_14439" s="T909">0.2.h:S v:pred</ta>
            <ta e="T911" id="Seg_14440" s="T910">0.2.h:S v:pred</ta>
            <ta e="T913" id="Seg_14441" s="T912">pro.h:O</ta>
            <ta e="T914" id="Seg_14442" s="T913">0.1.h:S v:pred</ta>
            <ta e="T916" id="Seg_14443" s="T915">np:O</ta>
            <ta e="T918" id="Seg_14444" s="T917">0.2.h:S v:pred</ta>
            <ta e="T919" id="Seg_14445" s="T918">pro.h:S</ta>
            <ta e="T920" id="Seg_14446" s="T919">v:pred</ta>
            <ta e="T921" id="Seg_14447" s="T920">n:pred</ta>
            <ta e="T922" id="Seg_14448" s="T921">0.1.h:S cop</ta>
            <ta e="T923" id="Seg_14449" s="T922">np.h:S</ta>
            <ta e="T926" id="Seg_14450" s="T925">v:pred 0.3.h:O</ta>
            <ta e="T927" id="Seg_14451" s="T926">pro.h:S</ta>
            <ta e="T930" id="Seg_14452" s="T929">v:pred</ta>
            <ta e="T932" id="Seg_14453" s="T931">np.h:O</ta>
            <ta e="T933" id="Seg_14454" s="T932">0.3.h:S v:pred</ta>
            <ta e="T934" id="Seg_14455" s="T933">np.h:S</ta>
            <ta e="T936" id="Seg_14456" s="T935">v:pred</ta>
            <ta e="T939" id="Seg_14457" s="T938">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T941" id="Seg_14458" s="T940">np.h:S</ta>
            <ta e="T943" id="Seg_14459" s="T942">v:pred</ta>
            <ta e="T944" id="Seg_14460" s="T943">0.3.h:S v:pred</ta>
            <ta e="T945" id="Seg_14461" s="T944">pro.h:S</ta>
            <ta e="T948" id="Seg_14462" s="T947">v:pred 0.3:S</ta>
            <ta e="T949" id="Seg_14463" s="T948">np:O</ta>
            <ta e="T950" id="Seg_14464" s="T949">0.1.h:S v:pred</ta>
            <ta e="T952" id="Seg_14465" s="T951">np:O</ta>
            <ta e="T953" id="Seg_14466" s="T952">0.1.h:S v:pred</ta>
            <ta e="T954" id="Seg_14467" s="T953">np.h:S</ta>
            <ta e="T956" id="Seg_14468" s="T955">v:pred 0.3.h:O</ta>
            <ta e="T958" id="Seg_14469" s="T957">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T959" id="Seg_14470" s="T958">np.h:S</ta>
            <ta e="T961" id="Seg_14471" s="T960">v:pred</ta>
            <ta e="T962" id="Seg_14472" s="T961">np.h:O</ta>
            <ta e="T964" id="Seg_14473" s="T963">0.3.h:S v:pred</ta>
            <ta e="T966" id="Seg_14474" s="T965">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T969" id="Seg_14475" s="T968">0.3.h:S v:pred</ta>
            <ta e="T970" id="Seg_14476" s="T969">pro.h:S</ta>
            <ta e="T972" id="Seg_14477" s="T971">v:pred</ta>
            <ta e="T974" id="Seg_14478" s="T973">0.2.h:S v:pred</ta>
            <ta e="T975" id="Seg_14479" s="T974">0.3.h:S v:pred</ta>
            <ta e="T977" id="Seg_14480" s="T975">s:purp</ta>
            <ta e="T979" id="Seg_14481" s="T978">np:O</ta>
            <ta e="T980" id="Seg_14482" s="T979">0.3.h:S v:pred</ta>
            <ta e="T981" id="Seg_14483" s="T980">pro.h:O</ta>
            <ta e="T983" id="Seg_14484" s="T982">0.3.h:S v:pred</ta>
            <ta e="T987" id="Seg_14485" s="T986">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T990" id="Seg_14486" s="T987">s:temp</ta>
            <ta e="T991" id="Seg_14487" s="T990">0.3.h:S v:pred</ta>
            <ta e="T992" id="Seg_14488" s="T991">np:O</ta>
            <ta e="T994" id="Seg_14489" s="T993">0.3.h:S v:pred</ta>
            <ta e="T996" id="Seg_14490" s="T995">0.3.h:S v:pred</ta>
            <ta e="T999" id="Seg_14491" s="T998">0.3.h:S v:pred</ta>
            <ta e="T1000" id="Seg_14492" s="T999">s:temp</ta>
            <ta e="T1001" id="Seg_14493" s="T1000">0.3.h:S v:pred</ta>
            <ta e="T1004" id="Seg_14494" s="T1003">np.h:S</ta>
            <ta e="T1005" id="Seg_14495" s="T1004">v:pred</ta>
            <ta e="T1007" id="Seg_14496" s="T1006">np.h:S</ta>
            <ta e="T1009" id="Seg_14497" s="T1008">v:pred</ta>
            <ta e="T1010" id="Seg_14498" s="T1009">np.h:S</ta>
            <ta e="T1011" id="Seg_14499" s="T1010">v:pred</ta>
            <ta e="T1013" id="Seg_14500" s="T1012">0.3.h:S v:pred</ta>
            <ta e="T1014" id="Seg_14501" s="T1013">np:O</ta>
            <ta e="T1015" id="Seg_14502" s="T1014">pro.h:S</ta>
            <ta e="T1017" id="Seg_14503" s="T1016">np:O</ta>
            <ta e="T1018" id="Seg_14504" s="T1017">v:pred</ta>
            <ta e="T1020" id="Seg_14505" s="T1019">0.3.h:S v:pred</ta>
            <ta e="T1024" id="Seg_14506" s="T1023">0.3.h:S v:pred</ta>
            <ta e="T1025" id="Seg_14507" s="T1024">pro.h:S</ta>
            <ta e="T1028" id="Seg_14508" s="T1027">v:pred</ta>
            <ta e="T1031" id="Seg_14509" s="T1030">0.3.h:S v:pred</ta>
            <ta e="T1032" id="Seg_14510" s="T1031">np.h:S</ta>
            <ta e="T1033" id="Seg_14511" s="T1032">adj:pred</ta>
            <ta e="T1034" id="Seg_14512" s="T1033">pro.h:O</ta>
            <ta e="T1037" id="Seg_14513" s="T1036">0.3.h:S v:pred</ta>
            <ta e="T1039" id="Seg_14514" s="T1038">0.3.h:S v:pred</ta>
            <ta e="T1041" id="Seg_14515" s="T1040">0.2.h:S v:pred</ta>
            <ta e="T1042" id="Seg_14516" s="T1041">pro.h:S</ta>
            <ta e="T1045" id="Seg_14517" s="T1044">v:pred</ta>
            <ta e="T1048" id="Seg_14518" s="T1047">0.1.h:S v:pred</ta>
            <ta e="T1051" id="Seg_14519" s="T1050">0.1.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T11" id="Seg_14520" s="T10">RUS:core</ta>
            <ta e="T20" id="Seg_14521" s="T19">RUS:gram</ta>
            <ta e="T24" id="Seg_14522" s="T23">RUS:cult</ta>
            <ta e="T29" id="Seg_14523" s="T28">RUS:gram</ta>
            <ta e="T31" id="Seg_14524" s="T30">RUS:gram</ta>
            <ta e="T32" id="Seg_14525" s="T31">RUS:cult</ta>
            <ta e="T35" id="Seg_14526" s="T34">RUS:cult</ta>
            <ta e="T36" id="Seg_14527" s="T35">RUS:gram</ta>
            <ta e="T46" id="Seg_14528" s="T45">RUS:gram</ta>
            <ta e="T53" id="Seg_14529" s="T52">RUS:gram</ta>
            <ta e="T67" id="Seg_14530" s="T66">RUS:cult</ta>
            <ta e="T71" id="Seg_14531" s="T70">RUS:gram</ta>
            <ta e="T72" id="Seg_14532" s="T71">RUS:cult</ta>
            <ta e="T82" id="Seg_14533" s="T81">RUS:core</ta>
            <ta e="T83" id="Seg_14534" s="T82">RUS:cult</ta>
            <ta e="T84" id="Seg_14535" s="T83">RUS:cult</ta>
            <ta e="T85" id="Seg_14536" s="T84">RUS:core</ta>
            <ta e="T88" id="Seg_14537" s="T87">RUS:gram</ta>
            <ta e="T93" id="Seg_14538" s="T92">RUS:gram</ta>
            <ta e="T97" id="Seg_14539" s="T96">RUS:gram</ta>
            <ta e="T99" id="Seg_14540" s="T98">RUS:gram</ta>
            <ta e="T102" id="Seg_14541" s="T101">RUS:disc</ta>
            <ta e="T103" id="Seg_14542" s="T102">RUS:gram</ta>
            <ta e="T107" id="Seg_14543" s="T106">RUS:gram</ta>
            <ta e="T113" id="Seg_14544" s="T112">RUS:core</ta>
            <ta e="T116" id="Seg_14545" s="T115">RUS:disc</ta>
            <ta e="T117" id="Seg_14546" s="T116">RUS:gram</ta>
            <ta e="T119" id="Seg_14547" s="T118">RUS:core</ta>
            <ta e="T127" id="Seg_14548" s="T126">RUS:gram</ta>
            <ta e="T135" id="Seg_14549" s="T134">RUS:gram</ta>
            <ta e="T138" id="Seg_14550" s="T137">RUS:gram</ta>
            <ta e="T141" id="Seg_14551" s="T140">RUS:disc</ta>
            <ta e="T146" id="Seg_14552" s="T145">RUS:gram</ta>
            <ta e="T150" id="Seg_14553" s="T149">RUS:gram</ta>
            <ta e="T157" id="Seg_14554" s="T156">RUS:gram</ta>
            <ta e="T160" id="Seg_14555" s="T159">RUS:gram</ta>
            <ta e="T164" id="Seg_14556" s="T163">RUS:core</ta>
            <ta e="T166" id="Seg_14557" s="T165">RUS:cult</ta>
            <ta e="T168" id="Seg_14558" s="T167">RUS:cult</ta>
            <ta e="T169" id="Seg_14559" s="T168">RUS:cult</ta>
            <ta e="T173" id="Seg_14560" s="T172">RUS:gram</ta>
            <ta e="T176" id="Seg_14561" s="T175">RUS:gram</ta>
            <ta e="T179" id="Seg_14562" s="T178">RUS:gram</ta>
            <ta e="T181" id="Seg_14563" s="T180">RUS:gram</ta>
            <ta e="T187" id="Seg_14564" s="T186">RUS:gram</ta>
            <ta e="T190" id="Seg_14565" s="T189">RUS:gram</ta>
            <ta e="T192" id="Seg_14566" s="T191">RUS:core</ta>
            <ta e="T199" id="Seg_14567" s="T198">RUS:gram</ta>
            <ta e="T202" id="Seg_14568" s="T201">RUS:gram</ta>
            <ta e="T206" id="Seg_14569" s="T205">RUS:cult</ta>
            <ta e="T209" id="Seg_14570" s="T208">RUS:gram</ta>
            <ta e="T218" id="Seg_14571" s="T217">RUS:cult</ta>
            <ta e="T221" id="Seg_14572" s="T220">RUS:gram</ta>
            <ta e="T238" id="Seg_14573" s="T237">RUS:gram</ta>
            <ta e="T240" id="Seg_14574" s="T239">RUS:cult</ta>
            <ta e="T245" id="Seg_14575" s="T244">RUS:gram</ta>
            <ta e="T262" id="Seg_14576" s="T261">RUS:cult</ta>
            <ta e="T268" id="Seg_14577" s="T267">RUS:gram</ta>
            <ta e="T274" id="Seg_14578" s="T273">RUS:cult</ta>
            <ta e="T279" id="Seg_14579" s="T278">RUS:gram</ta>
            <ta e="T286" id="Seg_14580" s="T285">RUS:gram</ta>
            <ta e="T289" id="Seg_14581" s="T288">RUS:gram</ta>
            <ta e="T296" id="Seg_14582" s="T295">RUS:gram</ta>
            <ta e="T310" id="Seg_14583" s="T309">RUS:cult</ta>
            <ta e="T314" id="Seg_14584" s="T313">RUS:cult</ta>
            <ta e="T319" id="Seg_14585" s="T318">RUS:gram</ta>
            <ta e="T322" id="Seg_14586" s="T321">RUS:cult</ta>
            <ta e="T333" id="Seg_14587" s="T332">RUS:gram</ta>
            <ta e="T336" id="Seg_14588" s="T335">RUS:gram</ta>
            <ta e="T345" id="Seg_14589" s="T344">RUS:gram</ta>
            <ta e="T355" id="Seg_14590" s="T354">RUS:cult</ta>
            <ta e="T361" id="Seg_14591" s="T360">RUS:mod</ta>
            <ta e="T362" id="Seg_14592" s="T361">RUS:disc</ta>
            <ta e="T363" id="Seg_14593" s="T362">RUS:gram</ta>
            <ta e="T364" id="Seg_14594" s="T363">RUS:cult</ta>
            <ta e="T367" id="Seg_14595" s="T366">RUS:gram</ta>
            <ta e="T370" id="Seg_14596" s="T369">RUS:gram</ta>
            <ta e="T371" id="Seg_14597" s="T370">RUS:cult</ta>
            <ta e="T374" id="Seg_14598" s="T373">RUS:disc</ta>
            <ta e="T375" id="Seg_14599" s="T374">RUS:gram</ta>
            <ta e="T377" id="Seg_14600" s="T376">RUS:cult</ta>
            <ta e="T379" id="Seg_14601" s="T378">RUS:cult</ta>
            <ta e="T385" id="Seg_14602" s="T384">RUS:cult</ta>
            <ta e="T388" id="Seg_14603" s="T387">RUS:mod</ta>
            <ta e="T391" id="Seg_14604" s="T390">RUS:mod</ta>
            <ta e="T394" id="Seg_14605" s="T393">RUS:gram</ta>
            <ta e="T395" id="Seg_14606" s="T394">RUS:gram</ta>
            <ta e="T398" id="Seg_14607" s="T397">RUS:cult</ta>
            <ta e="T400" id="Seg_14608" s="T399">RUS:gram</ta>
            <ta e="T404" id="Seg_14609" s="T403">RUS:gram</ta>
            <ta e="T405" id="Seg_14610" s="T404">RUS:core</ta>
            <ta e="T416" id="Seg_14611" s="T415">RUS:disc</ta>
            <ta e="T419" id="Seg_14612" s="T418">RUS:core</ta>
            <ta e="T421" id="Seg_14613" s="T420">RUS:disc</ta>
            <ta e="T422" id="Seg_14614" s="T421">RUS:gram</ta>
            <ta e="T428" id="Seg_14615" s="T427">RUS:core</ta>
            <ta e="T440" id="Seg_14616" s="T439">RUS:gram</ta>
            <ta e="T446" id="Seg_14617" s="T445">RUS:cult</ta>
            <ta e="T449" id="Seg_14618" s="T448">RUS:disc</ta>
            <ta e="T451" id="Seg_14619" s="T450">RUS:cult</ta>
            <ta e="T452" id="Seg_14620" s="T451">RUS:cult</ta>
            <ta e="T461" id="Seg_14621" s="T460">RUS:core</ta>
            <ta e="T463" id="Seg_14622" s="T462">RUS:gram</ta>
            <ta e="T476" id="Seg_14623" s="T475">RUS:cult</ta>
            <ta e="T478" id="Seg_14624" s="T477">RUS:cult</ta>
            <ta e="T486" id="Seg_14625" s="T485">TURK:cult</ta>
            <ta e="T488" id="Seg_14626" s="T487">TURK:cult</ta>
            <ta e="T497" id="Seg_14627" s="T496">RUS:gram</ta>
            <ta e="T504" id="Seg_14628" s="T503">RUS:cult</ta>
            <ta e="T511" id="Seg_14629" s="T510">RUS:cult</ta>
            <ta e="T516" id="Seg_14630" s="T515">RUS:cult</ta>
            <ta e="T521" id="Seg_14631" s="T520">RUS:gram</ta>
            <ta e="T524" id="Seg_14632" s="T523">RUS:cult</ta>
            <ta e="T531" id="Seg_14633" s="T530">RUS:cult</ta>
            <ta e="T533" id="Seg_14634" s="T532">RUS:cult</ta>
            <ta e="T535" id="Seg_14635" s="T534">RUS:disc</ta>
            <ta e="T536" id="Seg_14636" s="T535">RUS:core</ta>
            <ta e="T543" id="Seg_14637" s="T542">RUS:disc</ta>
            <ta e="T556" id="Seg_14638" s="T555">RUS:gram</ta>
            <ta e="T563" id="Seg_14639" s="T562">RUS:gram</ta>
            <ta e="T569" id="Seg_14640" s="T568">RUS:cult</ta>
            <ta e="T575" id="Seg_14641" s="T574">RUS:core</ta>
            <ta e="T580" id="Seg_14642" s="T579">RUS:gram</ta>
            <ta e="T587" id="Seg_14643" s="T586">RUS:gram</ta>
            <ta e="T598" id="Seg_14644" s="T597">RUS:cult</ta>
            <ta e="T599" id="Seg_14645" s="T598">RUS:cult</ta>
            <ta e="T601" id="Seg_14646" s="T600">RUS:cult</ta>
            <ta e="T603" id="Seg_14647" s="T602">RUS:gram</ta>
            <ta e="T613" id="Seg_14648" s="T612">RUS:cult</ta>
            <ta e="T618" id="Seg_14649" s="T617">RUS:mod</ta>
            <ta e="T620" id="Seg_14650" s="T619">RUS:disc</ta>
            <ta e="T621" id="Seg_14651" s="T620">RUS:cult</ta>
            <ta e="T622" id="Seg_14652" s="T621">RUS:cult</ta>
            <ta e="T623" id="Seg_14653" s="T622">RUS:disc</ta>
            <ta e="T626" id="Seg_14654" s="T625">RUS:cult</ta>
            <ta e="T631" id="Seg_14655" s="T630">RUS:gram</ta>
            <ta e="T638" id="Seg_14656" s="T637">RUS:gram</ta>
            <ta e="T640" id="Seg_14657" s="T639">RUS:cult</ta>
            <ta e="T642" id="Seg_14658" s="T641">RUS:cult</ta>
            <ta e="T648" id="Seg_14659" s="T647">RUS:disc</ta>
            <ta e="T672" id="Seg_14660" s="T671">RUS:gram</ta>
            <ta e="T676" id="Seg_14661" s="T675">RUS:gram</ta>
            <ta e="T678" id="Seg_14662" s="T677">RUS:cult</ta>
            <ta e="T688" id="Seg_14663" s="T687">RUS:gram</ta>
            <ta e="T691" id="Seg_14664" s="T690">RUS:cult</ta>
            <ta e="T697" id="Seg_14665" s="T696">RUS:cult</ta>
            <ta e="T717" id="Seg_14666" s="T716">RUS:gram</ta>
            <ta e="T721" id="Seg_14667" s="T720">RUS:gram</ta>
            <ta e="T726" id="Seg_14668" s="T725">RUS:core</ta>
            <ta e="T734" id="Seg_14669" s="T733">RUS:cult</ta>
            <ta e="T736" id="Seg_14670" s="T735">RUS:gram</ta>
            <ta e="T739" id="Seg_14671" s="T738">RUS:gram</ta>
            <ta e="T751" id="Seg_14672" s="T750">RUS:cult</ta>
            <ta e="T752" id="Seg_14673" s="T751">TURK:cult</ta>
            <ta e="T770" id="Seg_14674" s="T769">RUS:cult</ta>
            <ta e="T774" id="Seg_14675" s="T773">RUS:gram</ta>
            <ta e="T777" id="Seg_14676" s="T776">RUS:gram</ta>
            <ta e="T785" id="Seg_14677" s="T784">RUS:gram</ta>
            <ta e="T791" id="Seg_14678" s="T790">RUS:cult</ta>
            <ta e="T803" id="Seg_14679" s="T802">RUS:gram</ta>
            <ta e="T808" id="Seg_14680" s="T807">RUS:gram</ta>
            <ta e="T813" id="Seg_14681" s="T812">RUS:cult</ta>
            <ta e="T816" id="Seg_14682" s="T815">RUS:gram</ta>
            <ta e="T819" id="Seg_14683" s="T818">RUS:cult</ta>
            <ta e="T832" id="Seg_14684" s="T831">RUS:gram</ta>
            <ta e="T842" id="Seg_14685" s="T841">RUS:cult</ta>
            <ta e="T846" id="Seg_14686" s="T845">RUS:gram</ta>
            <ta e="T876" id="Seg_14687" s="T875">RUS:gram</ta>
            <ta e="T885" id="Seg_14688" s="T884">RUS:cult</ta>
            <ta e="T887" id="Seg_14689" s="T886">RUS:disc</ta>
            <ta e="T889" id="Seg_14690" s="T888">RUS:gram</ta>
            <ta e="T890" id="Seg_14691" s="T889">RUS:cult</ta>
            <ta e="T898" id="Seg_14692" s="T897">RUS:disc</ta>
            <ta e="T899" id="Seg_14693" s="T898">RUS:core</ta>
            <ta e="T903" id="Seg_14694" s="T902">RUS:gram</ta>
            <ta e="T905" id="Seg_14695" s="T904">RUS:cult</ta>
            <ta e="T909" id="Seg_14696" s="T908">RUS:disc</ta>
            <ta e="T924" id="Seg_14697" s="T923">RUS:disc</ta>
            <ta e="T928" id="Seg_14698" s="T927">RUS:disc</ta>
            <ta e="T931" id="Seg_14699" s="T930">RUS:disc</ta>
            <ta e="T932" id="Seg_14700" s="T931">RUS:cult</ta>
            <ta e="T934" id="Seg_14701" s="T933">RUS:cult</ta>
            <ta e="T938" id="Seg_14702" s="T937">RUS:gram</ta>
            <ta e="T940" id="Seg_14703" s="T939">RUS:gram</ta>
            <ta e="T955" id="Seg_14704" s="T954">RUS:disc</ta>
            <ta e="T957" id="Seg_14705" s="T956">RUS:gram</ta>
            <ta e="T963" id="Seg_14706" s="T962">RUS:disc</ta>
            <ta e="T965" id="Seg_14707" s="T964">RUS:gram</ta>
            <ta e="T967" id="Seg_14708" s="T966">RUS:gram</ta>
            <ta e="T985" id="Seg_14709" s="T984">RUS:gram</ta>
            <ta e="T988" id="Seg_14710" s="T987">RUS:cult</ta>
            <ta e="T993" id="Seg_14711" s="T992">RUS:core</ta>
            <ta e="T997" id="Seg_14712" s="T996">RUS:gram</ta>
            <ta e="T1006" id="Seg_14713" s="T1005">RUS:gram</ta>
            <ta e="T1012" id="Seg_14714" s="T1011">RUS:core</ta>
            <ta e="T1019" id="Seg_14715" s="T1018">RUS:gram</ta>
            <ta e="T1021" id="Seg_14716" s="T1020">RUS:gram</ta>
            <ta e="T1029" id="Seg_14717" s="T1028">RUS:core</ta>
            <ta e="T1035" id="Seg_14718" s="T1034">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_14719" s="T1">Xristjan old man.</ta>
            <ta e="T6" id="Seg_14720" s="T3">Xristjan old man was born.</ta>
            <ta e="T9" id="Seg_14721" s="T6">He has got three sons.</ta>
            <ta e="T12" id="Seg_14722" s="T9">The sons got married.</ta>
            <ta e="T19" id="Seg_14723" s="T12">One son always goes hunting, the elder son.</ta>
            <ta e="T30" id="Seg_14724" s="T19">And two sons look after livestock, they have cows, horses and sheep.</ta>
            <ta e="T35" id="Seg_14725" s="T30">And they have ploughland, they sow corn.</ta>
            <ta e="T42" id="Seg_14726" s="T35">And the elder son goes to the taiga.</ta>
            <ta e="T48" id="Seg_14727" s="T42">He had a daughter born and a son born.</ta>
            <ta e="T52" id="Seg_14728" s="T48">They grew old already.</ta>
            <ta e="T61" id="Seg_14729" s="T52">And he had built a big house there in the taiga, a big house.</ta>
            <ta e="T67" id="Seg_14730" s="T61">He told to his mother, to his father: “Bless me with an icon.”</ta>
            <ta e="T72" id="Seg_14731" s="T67">His mother took an icon and blessed him.</ta>
            <ta e="T74" id="Seg_14732" s="T72">I will leave.</ta>
            <ta e="T80" id="Seg_14733" s="T74">Three brothers are not going to live in one house.</ta>
            <ta e="T83" id="Seg_14734" s="T80">Now I have a family.”</ta>
            <ta e="T87" id="Seg_14735" s="T83">He carried all his belongings into a boat.</ta>
            <ta e="T90" id="Seg_14736" s="T87">And they set off.</ta>
            <ta e="T96" id="Seg_14737" s="T90">His father and his mother stayed crying.</ta>
            <ta e="T101" id="Seg_14738" s="T96">And they left.</ta>
            <ta e="T106" id="Seg_14739" s="T101">Well, they arrived.</ta>
            <ta e="T112" id="Seg_14740" s="T106">And he had built there a big house.</ta>
            <ta e="T115" id="Seg_14741" s="T112">He brought there everything.</ta>
            <ta e="T123" id="Seg_14742" s="T115">And the husband made everything, he made beds.</ta>
            <ta e="T126" id="Seg_14743" s="T123">“Tomorrow I'll go to the taiga.</ta>
            <ta e="T131" id="Seg_14744" s="T126">And you'll go search for water.</ta>
            <ta e="T137" id="Seg_14745" s="T131">There should be no scobs on your feet.</ta>
            <ta e="T140" id="Seg_14746" s="T137">Otherwise it will get bad.”</ta>
            <ta e="T142" id="Seg_14747" s="T140">They spent one night.</ta>
            <ta e="T149" id="Seg_14748" s="T142">In the morning he got up, ate and went to the taiga.</ta>
            <ta e="T162" id="Seg_14749" s="T149">And the woman said: “Why did my husband say, that the scobs shouldn't fall into water?</ta>
            <ta e="T172" id="Seg_14750" s="T162">I'll go now, fill two buckets with scobs and pour them into water.”</ta>
            <ta e="T177" id="Seg_14751" s="T172">And she took [the buckets] and poured them [into water].</ta>
            <ta e="T180" id="Seg_14752" s="T177">She was staying and looking.</ta>
            <ta e="T183" id="Seg_14753" s="T180">And the current there was very swift.</ta>
            <ta e="T189" id="Seg_14754" s="T183">She ladled water and went home.</ta>
            <ta e="T194" id="Seg_14755" s="T189">And she was a beautiful woman.</ta>
            <ta e="T201" id="Seg_14756" s="T194">She went and sat down in the room and began to sew.</ta>
            <ta e="T205" id="Seg_14757" s="T201">And the two children were playing.</ta>
            <ta e="T215" id="Seg_14758" s="T205">There were robbers passing by in a boat, they said: “What for scobs are there in the current?</ta>
            <ta e="T218" id="Seg_14759" s="T215">Steer to the bank!”</ta>
            <ta e="T222" id="Seg_14760" s="T218">They looked, [there is] a path.</ta>
            <ta e="T225" id="Seg_14761" s="T222">They found a small path.</ta>
            <ta e="T231" id="Seg_14762" s="T225">“We hadn't seen this small path earlier.”</ta>
            <ta e="T239" id="Seg_14763" s="T231">They moved to this path.</ta>
            <ta e="T251" id="Seg_14764" s="T239">The robbers' chief said: “You sit here, and I'll run and have a look, who lives here.”</ta>
            <ta e="T253" id="Seg_14765" s="T251">He ran.</ta>
            <ta e="T256" id="Seg_14766" s="T253">He came to the house and entered it.</ta>
            <ta e="T257" id="Seg_14767" s="T256">“Hello.”</ta>
            <ta e="T261" id="Seg_14768" s="T257">The woman said: “Hello.”</ta>
            <ta e="T264" id="Seg_14769" s="T261">She put a chair.</ta>
            <ta e="T267" id="Seg_14770" s="T264">She said: “Sit down here.”</ta>
            <ta e="T270" id="Seg_14771" s="T267">And he sat down [there].</ta>
            <ta e="T273" id="Seg_14772" s="T270">They began to talk.</ta>
            <ta e="T278" id="Seg_14773" s="T273">The robbers' chief said: “Marry me.”</ta>
            <ta e="T282" id="Seg_14774" s="T278">“But my husband is very strong.</ta>
            <ta e="T288" id="Seg_14775" s="T282">He'll kill you and he'll kill me.”</ta>
            <ta e="T293" id="Seg_14776" s="T288">“And I will give you a rope.”</ta>
            <ta e="T303" id="Seg_14777" s="T293">(There will be a hair(?) rope and a zinc wire, he won't tear it.)</ta>
            <ta e="T307" id="Seg_14778" s="T303">He'll come in the evening.</ta>
            <ta e="T310" id="Seg_14779" s="T307">Say to him: “Let's play cards.”</ta>
            <ta e="T318" id="Seg_14780" s="T310">Say to him: “Let's tie behind the hands of the one, who will lose (who will be left 'fool').”</ta>
            <ta e="T326" id="Seg_14781" s="T318">And make him lose (leave him 'fool'), tie his hands behind.</ta>
            <ta e="T331" id="Seg_14782" s="T326">He won't tear this rope.</ta>
            <ta e="T341" id="Seg_14783" s="T331">If he doesn't tear it, go outside and call me.</ta>
            <ta e="T346" id="Seg_14784" s="T341">I'll come and kill him.</ta>
            <ta e="T348" id="Seg_14785" s="T346">We'll live with you together.”</ta>
            <ta e="T350" id="Seg_14786" s="T348">The husband came.</ta>
            <ta e="T355" id="Seg_14787" s="T350">She gave him to eat and said: “Let' play cards.”</ta>
            <ta e="T361" id="Seg_14788" s="T355">The husband said: “I'm tired, it's time to sleep.”</ta>
            <ta e="T366" id="Seg_14789" s="T361">“Come on, let's play cards a little.</ta>
            <ta e="T373" id="Seg_14790" s="T366">I'm sitting here alone, I'm bored.”</ta>
            <ta e="T377" id="Seg_14791" s="T373">So they sat down and began playing cards.</ta>
            <ta e="T383" id="Seg_14792" s="T377">“We'll tie behind the hand of the one, who will lose (= will be left 'fool').”</ta>
            <ta e="T386" id="Seg_14793" s="T383">The husband lost (=was left 'fool').</ta>
            <ta e="T393" id="Seg_14794" s="T386">“We should tie the hands, tie the hands behind.</ta>
            <ta e="T403" id="Seg_14795" s="T393">If I had lost (=had been left 'fool'), you would have tied my hands behind my back.</ta>
            <ta e="T409" id="Seg_14796" s="T403">And now I'll tie your hands.”</ta>
            <ta e="T412" id="Seg_14797" s="T409">The husband said: “Tie it up.”</ta>
            <ta e="T414" id="Seg_14798" s="T412">She tied [his hands].</ta>
            <ta e="T420" id="Seg_14799" s="T414">The husband strained(?), all the ropes got torn.</ta>
            <ta e="T424" id="Seg_14800" s="T420">They went to sleep.</ta>
            <ta e="T426" id="Seg_14801" s="T424">In the morning they got up.</ta>
            <ta e="T433" id="Seg_14802" s="T426">The woman cooked everything, meat and other [food], gave her husband to eat.</ta>
            <ta e="T436" id="Seg_14803" s="T433">The husband went to the taiga.</ta>
            <ta e="T443" id="Seg_14804" s="T436">The woman jumped outside and cried: “Come!</ta>
            <ta e="T445" id="Seg_14805" s="T443">My husband is gone!”</ta>
            <ta e="T448" id="Seg_14806" s="T445">The robbers' chief came.</ta>
            <ta e="T451" id="Seg_14807" s="T448">“Well, did you play cards?”</ta>
            <ta e="T452" id="Seg_14808" s="T451">“We did.</ta>
            <ta e="T462" id="Seg_14809" s="T452">I was telling you, he is strong, he tore the rope.”</ta>
            <ta e="T470" id="Seg_14810" s="T462">“Today I'll give you a hair rope.</ta>
            <ta e="T476" id="Seg_14811" s="T470">When he comes again in the evening, say to him: let's play cards.</ta>
            <ta e="T482" id="Seg_14812" s="T476">We will tie behind the hands of the one, who will lose (=will be left 'fool').”</ta>
            <ta e="T487" id="Seg_14813" s="T482">The woman gave him to eat and wine to drink.</ta>
            <ta e="T489" id="Seg_14814" s="T487">They drank wine.</ta>
            <ta e="T491" id="Seg_14815" s="T489">The man left.</ta>
            <ta e="T493" id="Seg_14816" s="T491">The husband came.</ta>
            <ta e="T499" id="Seg_14817" s="T493">He killed many animals, and he killed an elk.</ta>
            <ta e="T502" id="Seg_14818" s="T499">She gave him to eat.</ta>
            <ta e="T504" id="Seg_14819" s="T502">She said: “Let's play cards.”</ta>
            <ta e="T508" id="Seg_14820" s="T504">The husband said: “I'm tired.”</ta>
            <ta e="T520" id="Seg_14821" s="T508">The woman said: “Let's play cards on the condition, that we tie behind the hands of the one, who will lose (=will be left 'fool').</ta>
            <ta e="T528" id="Seg_14822" s="T520">If I loose, tie (you will tie) my hands.”</ta>
            <ta e="T531" id="Seg_14823" s="T528">They sat down and began playing cards.</ta>
            <ta e="T534" id="Seg_14824" s="T531">The husband lost (=was left 'fool').</ta>
            <ta e="T538" id="Seg_14825" s="T534">“Well, now I'll tie your hands.”</ta>
            <ta e="T541" id="Seg_14826" s="T538">She tied his hands.</ta>
            <ta e="T546" id="Seg_14827" s="T541">He strained, the rope broke.</ta>
            <ta e="T548" id="Seg_14828" s="T546">They went to sleep.</ta>
            <ta e="T550" id="Seg_14829" s="T548">In the morning they got up.</ta>
            <ta e="T553" id="Seg_14830" s="T550">The woman have her husband to eat.</ta>
            <ta e="T559" id="Seg_14831" s="T553">The husband ate and went to the taiga.</ta>
            <ta e="T566" id="Seg_14832" s="T559">The woman went outside and cried: “Come!</ta>
            <ta e="T568" id="Seg_14833" s="T566">My husband is gone!”</ta>
            <ta e="T570" id="Seg_14834" s="T568">The robber came.</ta>
            <ta e="T576" id="Seg_14835" s="T570">The woman said: “My husband has torn all the ropes.</ta>
            <ta e="T579" id="Seg_14836" s="T576">He is strong.”</ta>
            <ta e="T591" id="Seg_14837" s="T579">“Today I'll give you a hair rope and a zinc wire twisted together.</ta>
            <ta e="T599" id="Seg_14838" s="T591">He will come in the evening. say to him again: let's play cards, let's play cards.</ta>
            <ta e="T606" id="Seg_14839" s="T599">We will tie behind the hands of the one, who will lose (=will be left 'fool').”</ta>
            <ta e="T608" id="Seg_14840" s="T606">The husband came.</ta>
            <ta e="T613" id="Seg_14841" s="T608">She gave him to eat and said: “Let's play cards.”</ta>
            <ta e="T619" id="Seg_14842" s="T613">The husband said: “I'm tired, it's time to sleep.”</ta>
            <ta e="T622" id="Seg_14843" s="T619">“Come on, let's play cards, let's play cards.</ta>
            <ta e="T630" id="Seg_14844" s="T622">We tie behind the hands of the one, who will lose (=will be left 'fool') at least once.</ta>
            <ta e="T637" id="Seg_14845" s="T630">If I lose (=will be left 'fool'), tie my hands behind.”</ta>
            <ta e="T640" id="Seg_14846" s="T637">And they sat down to play.</ta>
            <ta e="T646" id="Seg_14847" s="T640">She made her husband 'fool' and tied his hands behind.</ta>
            <ta e="T652" id="Seg_14848" s="T646">He strained, [but] he didn't tear the ropes.</ta>
            <ta e="T656" id="Seg_14849" s="T652">He said to his wife: “Untie my hands.”</ta>
            <ta e="T659" id="Seg_14850" s="T656">The wife said: “Tear it.”</ta>
            <ta e="T664" id="Seg_14851" s="T659">He strained again, [but] didn't tear it.</ta>
            <ta e="T666" id="Seg_14852" s="T664">He said: “Untie.”</ta>
            <ta e="T668" id="Seg_14853" s="T666">“I won't untie it.”</ta>
            <ta e="T675" id="Seg_14854" s="T668">She jumped outside and cried: “Come!”</ta>
            <ta e="T680" id="Seg_14855" s="T675">And this robber came.</ta>
            <ta e="T685" id="Seg_14856" s="T680">She said: “My husband didn't tear the rope.</ta>
            <ta e="T687" id="Seg_14857" s="T685">Let's come in and kill him.”</ta>
            <ta e="T690" id="Seg_14858" s="T687">And they entered the house.</ta>
            <ta e="T695" id="Seg_14859" s="T690">The robber said: “We'll kill him tomorrow morning.</ta>
            <ta e="T699" id="Seg_14860" s="T695">Bring a bucket full of salt.</ta>
            <ta e="T705" id="Seg_14861" s="T699">We'll put him on the knees (onto the salt).</ta>
            <ta e="T707" id="Seg_14862" s="T705">He will sit till morning.</ta>
            <ta e="T709" id="Seg_14863" s="T707">We'll kill him in the morning.”</ta>
            <ta e="T714" id="Seg_14864" s="T709">The woman approached him and spat into his face.</ta>
            <ta e="T720" id="Seg_14865" s="T714">“You said that I am strong.</ta>
            <ta e="T725" id="Seg_14866" s="T720">And why don't you tear the rope?</ta>
            <ta e="T729" id="Seg_14867" s="T725">Now we'll kill you in the morning."</ta>
            <ta e="T738" id="Seg_14868" s="T729">The woman ran, brought a bucket full of salt and poured it onto the floor.</ta>
            <ta e="T742" id="Seg_14869" s="T738">And they put him on the knees.</ta>
            <ta e="T745" id="Seg_14870" s="T742">She said: “Sit [here] till next morning!</ta>
            <ta e="T748" id="Seg_14871" s="T745">In the morning we will kill you.”</ta>
            <ta e="T754" id="Seg_14872" s="T748">She sat down to eat and drink wine with the robber.</ta>
            <ta e="T762" id="Seg_14873" s="T754">The woman came and spat into her husband's face.</ta>
            <ta e="T766" id="Seg_14874" s="T762">“We'll kill you in the morning.”</ta>
            <ta e="T773" id="Seg_14875" s="T766">She went and began to embrace and to kiss the robber.</ta>
            <ta e="T779" id="Seg_14876" s="T773">They went to sleep, they fell asleep.</ta>
            <ta e="T784" id="Seg_14877" s="T779">(The husband) he got up and began to walk.</ta>
            <ta e="T792" id="Seg_14878" s="T784">His small daughter and son were sleeping together on a high bunk.</ta>
            <ta e="T796" id="Seg_14879" s="T792">The daughter climbed down to piss.</ta>
            <ta e="T802" id="Seg_14880" s="T796">He said to his daughter: “Untie my hands.”</ta>
            <ta e="T810" id="Seg_14881" s="T802">And the daughter said: “You used to say you were strong.”</ta>
            <ta e="T812" id="Seg_14882" s="T810">Tear the rope.”</ta>
            <ta e="T817" id="Seg_14883" s="T812">She climbed back to the high bunk and fell asleep.</ta>
            <ta e="T822" id="Seg_14884" s="T817">The son climbed down from the high bunk to piss.</ta>
            <ta e="T828" id="Seg_14885" s="T822">He said to his son: “Untie my hands.”</ta>
            <ta e="T834" id="Seg_14886" s="T828">He came to his father and began to untie him.</ta>
            <ta e="T836" id="Seg_14887" s="T834">He didn't untie him.</ta>
            <ta e="T839" id="Seg_14888" s="T836">“I can't untie you.”</ta>
            <ta e="T843" id="Seg_14889" s="T839">The father said: “Bring a rasp.”</ta>
            <ta e="T850" id="Seg_14890" s="T843">He brought it and began to grind the ropes.</ta>
            <ta e="T853" id="Seg_14891" s="T850">He ground it, the ropes flew aside(?).</ta>
            <ta e="T858" id="Seg_14892" s="T853">He said to his son: “Go and sleep.</ta>
            <ta e="T861" id="Seg_14893" s="T858">Don't be afraid in the morning.</ta>
            <ta e="T865" id="Seg_14894" s="T861">We will live with you together.”</ta>
            <ta e="T868" id="Seg_14895" s="T865">The day was breaking.</ta>
            <ta e="T879" id="Seg_14896" s="T868">He put the ropes onto his hands (as if he had been tied) and sat down onto his knees.</ta>
            <ta e="T884" id="Seg_14897" s="T879">His wife got up, she came and spat into his face.</ta>
            <ta e="T888" id="Seg_14898" s="T884">She said to the robber: “Well, let's kill him.”</ta>
            <ta e="T893" id="Seg_14899" s="T888">And the robber said: “Let's kill him after we have eaten.”</ta>
            <ta e="T896" id="Seg_14900" s="T893">She cooked meat.</ta>
            <ta e="T897" id="Seg_14901" s="T896">They ate.</ta>
            <ta e="T900" id="Seg_14902" s="T897">“Well, now let's kill him.</ta>
            <ta e="T904" id="Seg_14903" s="T900">Shall we kill him at home or outside?”</ta>
            <ta e="T908" id="Seg_14904" s="T904">The robber said: “We'll kill him outside.</ta>
            <ta e="T912" id="Seg_14905" s="T908">Come one, get up, go outside.</ta>
            <ta e="T914" id="Seg_14906" s="T912">We will kill you.</ta>
            <ta e="T918" id="Seg_14907" s="T914">You hadn't torn the ropes, had you?</ta>
            <ta e="T922" id="Seg_14908" s="T918">You were saying you were strong.”</ta>
            <ta e="T926" id="Seg_14909" s="T922">The woman spat into his face.</ta>
            <ta e="T933" id="Seg_14910" s="T926">Suddenly he jumped up and hit the robber.</ta>
            <ta e="T937" id="Seg_14911" s="T933">The robber fell on his back.</ta>
            <ta e="T939" id="Seg_14912" s="T937">He killed [the robber].</ta>
            <ta e="T943" id="Seg_14913" s="T939">And his wife clasped her arms round his neck.</ta>
            <ta e="T948" id="Seg_14914" s="T943">She said: “I won't do it any more.</ta>
            <ta e="T950" id="Seg_14915" s="T948">I will wash your feet.</ta>
            <ta e="T953" id="Seg_14916" s="T950">I will drink this water.”</ta>
            <ta e="T956" id="Seg_14917" s="T953">The husband hit her.</ta>
            <ta e="T958" id="Seg_14918" s="T956">And he killed her.</ta>
            <ta e="T961" id="Seg_14919" s="T958">The children began crying.</ta>
            <ta e="T966" id="Seg_14920" s="T961">He hit his daughter and killed her.</ta>
            <ta e="T972" id="Seg_14921" s="T966">He said to his son: “We'll live with you together.</ta>
            <ta e="T974" id="Seg_14922" s="T972">Don't cry.”</ta>
            <ta e="T977" id="Seg_14923" s="T974">He went to dig ground.</ta>
            <ta e="T980" id="Seg_14924" s="T977">He digged out a pit.</ta>
            <ta e="T987" id="Seg_14925" s="T980">He threw them there and buried them.</ta>
            <ta e="T991" id="Seg_14926" s="T987">He dragged his boat to the river.</ta>
            <ta e="T994" id="Seg_14927" s="T991">He carried all his belongings.</ta>
            <ta e="T999" id="Seg_14928" s="T994">He got into the boat with his son and they set off.</ta>
            <ta e="T1003" id="Seg_14929" s="T999">They came to their village.</ta>
            <ta e="T1005" id="Seg_14930" s="T1003">Their people were glad.</ta>
            <ta e="T1009" id="Seg_14931" s="T1005">“And where is your wife?”</ta>
            <ta e="T1011" id="Seg_14932" s="T1009">“My wife died.”</ta>
            <ta e="T1014" id="Seg_14933" s="T1011">They brought all their belongings.</ta>
            <ta e="T1018" id="Seg_14934" s="T1014">He bought a house.</ta>
            <ta e="T1024" id="Seg_14935" s="T1018">He got married and began to live.</ta>
            <ta e="T1028" id="Seg_14936" s="T1024">Last year I came to visit him.</ta>
            <ta e="T1031" id="Seg_14937" s="T1028">He is doing well.</ta>
            <ta e="T1037" id="Seg_14938" s="T1031">His wife is a good woman, she gave me tea.</ta>
            <ta e="T1041" id="Seg_14939" s="T1037">She said: “Come to visit us.”</ta>
            <ta e="T1045" id="Seg_14940" s="T1041">I come to visit them all the time.</ta>
            <ta e="T1048" id="Seg_14941" s="T1045">Now I'll go home (to my village).</ta>
            <ta e="T1051" id="Seg_14942" s="T1048">I'll come to visit them again.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_14943" s="T1">Christian, der alte Mann.</ta>
            <ta e="T6" id="Seg_14944" s="T3">Der alte Christian wurde geboren.</ta>
            <ta e="T9" id="Seg_14945" s="T6">Er hat drei Söhne.</ta>
            <ta e="T12" id="Seg_14946" s="T9">Die Söhne heirateten alle.</ta>
            <ta e="T19" id="Seg_14947" s="T12">Ein Sohn geht immer jagen, der älteste Sohn.</ta>
            <ta e="T30" id="Seg_14948" s="T19">Und zwei Söhne hüten das Vieh, sie haben Kühe, Pferde und Schafe.</ta>
            <ta e="T35" id="Seg_14949" s="T30">Und sie haben Ackerland, sie sähen Korn.</ta>
            <ta e="T42" id="Seg_14950" s="T35">Und der älteste Sohn geht in die Taiga.</ta>
            <ta e="T48" id="Seg_14951" s="T42">Er bekam eine Tochter und er bekam einen Sohn.</ta>
            <ta e="T52" id="Seg_14952" s="T48">Sie sind schon groß geworden.</ta>
            <ta e="T61" id="Seg_14953" s="T52">Und er hatte ein großes Haus dort in der Taiga gebaut, ein großes Haus.</ta>
            <ta e="T67" id="Seg_14954" s="T61">Er sagte seiner Mutter, seinem Vater: "Segnet mich mit einer Götze."</ta>
            <ta e="T72" id="Seg_14955" s="T67">Seine Mutter nahm eine Götze und segnete ihn.</ta>
            <ta e="T74" id="Seg_14956" s="T72">Ich werde gehen.</ta>
            <ta e="T80" id="Seg_14957" s="T74">Drei Brüder sollten nicht in einem Haus leben.</ta>
            <ta e="T83" id="Seg_14958" s="T80">Jetzt habe ich eine Familie."</ta>
            <ta e="T87" id="Seg_14959" s="T83">Er trug all seinen Besitz in ein Boot.</ta>
            <ta e="T90" id="Seg_14960" s="T87">Und sie brachen auf.</ta>
            <ta e="T96" id="Seg_14961" s="T90">Sein Vater und seine Mutter blieben weinend zurück.</ta>
            <ta e="T101" id="Seg_14962" s="T96">Und sie gingen.</ta>
            <ta e="T106" id="Seg_14963" s="T101">Nunja, sie kamen an.</ta>
            <ta e="T112" id="Seg_14964" s="T106">Und er hatte dort ein großes Haus gebaut.</ta>
            <ta e="T115" id="Seg_14965" s="T112">Er brachte alles dorthin.</ta>
            <ta e="T123" id="Seg_14966" s="T115">Und der Eheman machte alles, er baute Betten.</ta>
            <ta e="T126" id="Seg_14967" s="T123">"Morgen gehe ich in die Taiga.</ta>
            <ta e="T131" id="Seg_14968" s="T126">Und du gehst Wasser holen.</ta>
            <ta e="T137" id="Seg_14969" s="T131">Es sollten keine Späne auf deine Füße (ins Wasser?) kommen.</ta>
            <ta e="T140" id="Seg_14970" s="T137">Sonst wird es schlecht."</ta>
            <ta e="T142" id="Seg_14971" s="T140">Sie verbrachten eine Nacht.</ta>
            <ta e="T149" id="Seg_14972" s="T142">Am Morgen stand er auf, aß und ging in die Taiga.</ta>
            <ta e="T162" id="Seg_14973" s="T149">Und die Frau sagte: "Warum hat mein Mann gesagt, dass die Späne nicht ins Wasser fallen sollen?</ta>
            <ta e="T172" id="Seg_14974" s="T162">Ich werde jetzt gehen, fülle zwei Eimer mit Spänen und gieße sie ins Wasser."</ta>
            <ta e="T177" id="Seg_14975" s="T172">Und sie nahm [die Eimer] und kippte sie [ins Wasser].</ta>
            <ta e="T180" id="Seg_14976" s="T177">Sie blieb und schaute.</ta>
            <ta e="T183" id="Seg_14977" s="T180">Und die Strömung war sehr stark.</ta>
            <ta e="T189" id="Seg_14978" s="T183">Sie schöpfte Wasser und ging nach Hause.</ta>
            <ta e="T194" id="Seg_14979" s="T189">Und sie war eine schöne Frau.</ta>
            <ta e="T201" id="Seg_14980" s="T194">Sie ging und setzte sich in den Raum und begann zu nähen.</ta>
            <ta e="T205" id="Seg_14981" s="T201">Und die beiden Kinder spielten.</ta>
            <ta e="T215" id="Seg_14982" s="T205">Räuber kamen in einem Boot vorbei, sie sagten: "Was für Späne schwimmen da in der Strömung?</ta>
            <ta e="T218" id="Seg_14983" s="T215">Steuert zum Ufer!"</ta>
            <ta e="T222" id="Seg_14984" s="T218">Sie schauten, [da ist] ein Pfad.</ta>
            <ta e="T225" id="Seg_14985" s="T222">Sie fanden einen schmalen Pfad.</ta>
            <ta e="T231" id="Seg_14986" s="T225">"Früher haben wir diesen schmalen Pfad nicht gesehen."</ta>
            <ta e="T239" id="Seg_14987" s="T231">Sie gingen auf diesen Pfad.</ta>
            <ta e="T251" id="Seg_14988" s="T239">Der Räuberhauptmann sagte: "Ihr sitzt hier und ich laufe und schaue, wer hier lebt."</ta>
            <ta e="T253" id="Seg_14989" s="T251">Er lief.</ta>
            <ta e="T256" id="Seg_14990" s="T253">Er kam zum Haus und trat ein.</ta>
            <ta e="T257" id="Seg_14991" s="T256">"Hallo."</ta>
            <ta e="T261" id="Seg_14992" s="T257">Die Frau sagte: "Hallo."</ta>
            <ta e="T264" id="Seg_14993" s="T261">Sie stellte einen Stuhl hin.</ta>
            <ta e="T267" id="Seg_14994" s="T264">Sie sagte: "Setz dich hier hin."</ta>
            <ta e="T270" id="Seg_14995" s="T267">Und er setzte sich [dort] hin.</ta>
            <ta e="T273" id="Seg_14996" s="T270">Sie begannen zu reden.</ta>
            <ta e="T278" id="Seg_14997" s="T273">Der Räuberhauptmann sagte: "Heirate mich."</ta>
            <ta e="T282" id="Seg_14998" s="T278">"Aber mein Mann ist sehr stark.</ta>
            <ta e="T288" id="Seg_14999" s="T282">Er wird dich töten und er wird mich töten."</ta>
            <ta e="T293" id="Seg_15000" s="T288">"Und ich werde dir ein Seil geben."</ta>
            <ta e="T303" id="Seg_15001" s="T293">(Es wird ein Haarseil geben und ein Zinkdraht geben, er kann ihn nicht zerreißen.)</ta>
            <ta e="T307" id="Seg_15002" s="T303">Er wird am Abend kommen.</ta>
            <ta e="T310" id="Seg_15003" s="T307">Sag zu ihm: "Lass uns Karten spielen."</ta>
            <ta e="T318" id="Seg_15004" s="T310">Sag ihm: "Lass uns die Hände von dem, der verliert (der Narr bleibt), auf den Rücken binden."</ta>
            <ta e="T326" id="Seg_15005" s="T318">Und lass ihn verlieren (lass ihm den Narren), binde seine Hände nach hinten.</ta>
            <ta e="T331" id="Seg_15006" s="T326">Er wird das Seil nicht zerreißen.</ta>
            <ta e="T341" id="Seg_15007" s="T331">Wenn er es nicht zerreißt, geh nach draußen und ruf mich.</ta>
            <ta e="T346" id="Seg_15008" s="T341">Ich werde kommen und ihn töten.</ta>
            <ta e="T348" id="Seg_15009" s="T346">Wir werden zusammen leben."</ta>
            <ta e="T350" id="Seg_15010" s="T348">Der Ehemann kam.</ta>
            <ta e="T355" id="Seg_15011" s="T350">Sie gab ihm zu essen und sagte: "Lass uns Karten spielen."</ta>
            <ta e="T361" id="Seg_15012" s="T355">Ihr Mann sagte: "Ich bin müde, es ist Zeit zu schlafen."</ta>
            <ta e="T366" id="Seg_15013" s="T361">"Komm schon, lass uns ein wenig Karten spielen.</ta>
            <ta e="T373" id="Seg_15014" s="T366">Ich sitze hier allein und langweile mich."</ta>
            <ta e="T377" id="Seg_15015" s="T373">So setzten sie sich und begannen Karten zu spielen.</ta>
            <ta e="T383" id="Seg_15016" s="T377">"Wir binden dem, der verliert (= der Narr bleibt), die Hände nach hinten."</ta>
            <ta e="T386" id="Seg_15017" s="T383">Der Mann verlor (= blieb der Narr).</ta>
            <ta e="T393" id="Seg_15018" s="T386">"Wir müssen die Hände binden, die Hände auf den Rücken binden.</ta>
            <ta e="T403" id="Seg_15019" s="T393">Wenn ich verloren hätte (=Narr geblieben wär), müsstest du mir meine Hände nach hinten binden.</ta>
            <ta e="T409" id="Seg_15020" s="T403">Und jetzt fessel ich deine Hände."</ta>
            <ta e="T412" id="Seg_15021" s="T409">Der Mann sagte: "Fessel sie."</ta>
            <ta e="T414" id="Seg_15022" s="T412">Sie fesselte [seine Hände].</ta>
            <ta e="T420" id="Seg_15023" s="T414">Der Mann spannte sich an(?), alle Seile rissen.</ta>
            <ta e="T424" id="Seg_15024" s="T420">Sie gingen schlafen.</ta>
            <ta e="T426" id="Seg_15025" s="T424">Am Morgen standen sie auf.</ta>
            <ta e="T433" id="Seg_15026" s="T426">Die Frau kochte alles, Fleisch und anderes, gab ihrem Mann zu essen.</ta>
            <ta e="T436" id="Seg_15027" s="T433">Der Mann ging in die Taiga.</ta>
            <ta e="T443" id="Seg_15028" s="T436">Die Frau sprang nach draußen und rief: "Komm!</ta>
            <ta e="T445" id="Seg_15029" s="T443">Mein Mann ist gegangen!"</ta>
            <ta e="T448" id="Seg_15030" s="T445">Der Räuberhauptmann kam.</ta>
            <ta e="T451" id="Seg_15031" s="T448">"Also, habt ihr Karten gespielt?"</ta>
            <ta e="T452" id="Seg_15032" s="T451">"Wir haben Karten gespielt.</ta>
            <ta e="T462" id="Seg_15033" s="T452">Ich habe dir gesagt, er ist stark, er hat das Seil zerrissen."</ta>
            <ta e="T470" id="Seg_15034" s="T462">"Heute gebe ich dir ein Haarseil.</ta>
            <ta e="T476" id="Seg_15035" s="T470">Wenn er am Abend wiederkommt, sag ihm: Lass uns Karten spielen.</ta>
            <ta e="T482" id="Seg_15036" s="T476">Wir werden dem, der verliert (=Narr bleibt), die Hände nach hinten binden."</ta>
            <ta e="T487" id="Seg_15037" s="T482">Die Frau gab ihm zu essen und Wein zu trinken.</ta>
            <ta e="T489" id="Seg_15038" s="T487">Sie tranken Wein.</ta>
            <ta e="T491" id="Seg_15039" s="T489">Der Mann ging.</ta>
            <ta e="T493" id="Seg_15040" s="T491">Der Ehemann kam.</ta>
            <ta e="T499" id="Seg_15041" s="T493">Er erbeutete viele Tiere und er erbeutete einen Elch.</ta>
            <ta e="T502" id="Seg_15042" s="T499">Sie gab ihm zu essen.</ta>
            <ta e="T504" id="Seg_15043" s="T502">Sie sagte: "Lass uns Karten spielen."</ta>
            <ta e="T508" id="Seg_15044" s="T504">Ihr Mann sagte: "Ich bin müde."</ta>
            <ta e="T520" id="Seg_15045" s="T508">Die Frau sagte: "Lass uns Karten unter der Bedingung spielen, dass wir dem, der verliert (=Narr bleibt), die Hände nach hinten binden.</ta>
            <ta e="T528" id="Seg_15046" s="T520">Wenn ich verliere, binde (wirst du binden) meine Hände."</ta>
            <ta e="T531" id="Seg_15047" s="T528">Sie setzten sich und begannen Karten zu spielen.</ta>
            <ta e="T534" id="Seg_15048" s="T531">Der Mann verlor (=blieb Narr).</ta>
            <ta e="T538" id="Seg_15049" s="T534">"Also, ich binde jetzt deine Hände."</ta>
            <ta e="T541" id="Seg_15050" s="T538">Sie fesselte seine Hände.</ta>
            <ta e="T546" id="Seg_15051" s="T541">Er spannte sich an, das Seil riss.</ta>
            <ta e="T548" id="Seg_15052" s="T546">Sie gingen schlafen.</ta>
            <ta e="T550" id="Seg_15053" s="T548">Am Morgen standen sie auf.</ta>
            <ta e="T553" id="Seg_15054" s="T550">Die Frau gab ihrem Mann zu essen.</ta>
            <ta e="T559" id="Seg_15055" s="T553">Ihr Mann aß und ging in die Taiga.</ta>
            <ta e="T566" id="Seg_15056" s="T559">Die Frau ging nach draußen und rief: "Komm!</ta>
            <ta e="T568" id="Seg_15057" s="T566">Mein Mann ist gegangen!"</ta>
            <ta e="T570" id="Seg_15058" s="T568">Der Räuber kam.</ta>
            <ta e="T576" id="Seg_15059" s="T570">Die Frau sagte: "Mein Mann hat alle Seile zerrissen.</ta>
            <ta e="T579" id="Seg_15060" s="T576">Er ist stark."</ta>
            <ta e="T591" id="Seg_15061" s="T579">"Heute gebe ich dir ein Haarseil und einen Zinkdraht miteinander verflochten.</ta>
            <ta e="T599" id="Seg_15062" s="T591">Er wird am Abend kommen, sag ihm wieder: Lass uns Karten spielen, lass uns Karten spielen.</ta>
            <ta e="T606" id="Seg_15063" s="T599">Wir binden dem, der verliert (=Narr bleibt) die Hände nach hinten."</ta>
            <ta e="T608" id="Seg_15064" s="T606">Der Ehemann kam.</ta>
            <ta e="T613" id="Seg_15065" s="T608">Sie gab ihm zu essen und sagte: "Lass uns Karten spielen."</ta>
            <ta e="T619" id="Seg_15066" s="T613">Ihr Mann sagte: "Ich bin müde, es ist Zeit zu schlafen."</ta>
            <ta e="T622" id="Seg_15067" s="T619">"Komm schon, lass uns Karten spielen, lass uns Karten spielen.</ta>
            <ta e="T630" id="Seg_15068" s="T622">Wir binden dem, der mindestens einmal verliert (=Narr bleibt), die Hände nach hinten.</ta>
            <ta e="T637" id="Seg_15069" s="T630">Wenn ich verliere (=Narr bleibe), binde meine Hände nach hinten."</ta>
            <ta e="T640" id="Seg_15070" s="T637">Und sie setzten sich um zu spielen.</ta>
            <ta e="T646" id="Seg_15071" s="T640">Sie machte ihren Mann zum Narren und band seine Hände nach hinten.</ta>
            <ta e="T652" id="Seg_15072" s="T646">Er spannte sich an, [aber] er zerriss die Seile nicht.</ta>
            <ta e="T656" id="Seg_15073" s="T652">Er sagte zu seiner Frau: "Binde meine Hände los."</ta>
            <ta e="T659" id="Seg_15074" s="T656">Die Frau sagte: "Zerreiß es."</ta>
            <ta e="T664" id="Seg_15075" s="T659">Er spannte sich wieder an, [aber] zerriss es nicht.</ta>
            <ta e="T666" id="Seg_15076" s="T664">Er sagte: "Binde es los."</ta>
            <ta e="T668" id="Seg_15077" s="T666">"Ich werde es nicht losbinden."</ta>
            <ta e="T675" id="Seg_15078" s="T668">Sie sprang nach draußen und rief: "Komm!"</ta>
            <ta e="T680" id="Seg_15079" s="T675">Und dieser Räuber kam.</ta>
            <ta e="T685" id="Seg_15080" s="T680">Sie sagte: "Mein Mann hat das Seil nicht zerrissen.</ta>
            <ta e="T687" id="Seg_15081" s="T685">Lass uns hineingehen und ihn töten."</ta>
            <ta e="T690" id="Seg_15082" s="T687">Und sie gingen ins Haus.</ta>
            <ta e="T695" id="Seg_15083" s="T690">Der Räuber sagte: "Wir werden ihn morgen früh töten.</ta>
            <ta e="T699" id="Seg_15084" s="T695">Bring einen Eimer voller Salz.</ta>
            <ta e="T705" id="Seg_15085" s="T699">Wir setzen ihn mit den Knien (auf das Salz).</ta>
            <ta e="T707" id="Seg_15086" s="T705">Er sitzt bis zum Morgen.</ta>
            <ta e="T709" id="Seg_15087" s="T707">Wir werden ihn morgen töten."</ta>
            <ta e="T714" id="Seg_15088" s="T709">Sie näherte sich ihm und spuckte ihm ins Gesicht.</ta>
            <ta e="T720" id="Seg_15089" s="T714">"Du hast gesagt, dass ich stark bin.</ta>
            <ta e="T725" id="Seg_15090" s="T720">Und warum zerreißt du nicht das Seil?</ta>
            <ta e="T729" id="Seg_15091" s="T725">Jetzt werden wir dich am Morgen töten."</ta>
            <ta e="T738" id="Seg_15092" s="T729">Die Frau rannte, brachte einen Eimer voll mit Salz und kippte es auf den Boden.</ta>
            <ta e="T742" id="Seg_15093" s="T738">Und sie zwangen ihn auf die Knie.</ta>
            <ta e="T745" id="Seg_15094" s="T742">Sie sagte: "Sitz [hier] bis morgen früh!</ta>
            <ta e="T748" id="Seg_15095" s="T745">Morgen früh töten wir dich."</ta>
            <ta e="T754" id="Seg_15096" s="T748">Sie setzte sich um mit dem Räuber zu essen und Wein zu trinken.</ta>
            <ta e="T762" id="Seg_15097" s="T754">Die Frau kam und spuckte ihrem Mann ins Gesicht.</ta>
            <ta e="T766" id="Seg_15098" s="T762">"Wir werden dich morgen früh töten."</ta>
            <ta e="T773" id="Seg_15099" s="T766">Sie ging und begann den Räuber zu umarmen und zu küssen.</ta>
            <ta e="T779" id="Seg_15100" s="T773">Sie gingen schlafen, sie schliefen ein.</ta>
            <ta e="T784" id="Seg_15101" s="T779">(Der Ehemann) er stand auf und begann zu gehen.</ta>
            <ta e="T792" id="Seg_15102" s="T784">Seine kleine Tochter und sein Sohn schliefen gemeinsam im oberen Bett.</ta>
            <ta e="T796" id="Seg_15103" s="T792">Die Tochter kletterte herunter um zu pinkeln.</ta>
            <ta e="T802" id="Seg_15104" s="T796">Er sagte zu seiner Tochter: "Binde meine Hände los."</ta>
            <ta e="T810" id="Seg_15105" s="T802">Und die Tochter sagte: "Du hast immer gesagt, du wärst stark."</ta>
            <ta e="T812" id="Seg_15106" s="T810">Zerreiß das Seil."</ta>
            <ta e="T817" id="Seg_15107" s="T812">Sie kletterte zurück ins Hochbett und schlief ein.</ta>
            <ta e="T822" id="Seg_15108" s="T817">Der Sohn kletterte vom Hochbett herunter um zu pinkeln.</ta>
            <ta e="T828" id="Seg_15109" s="T822">Er sagte zu seinem Sohn: "Binde meine Hände los."</ta>
            <ta e="T834" id="Seg_15110" s="T828">Er kam zu seinem Vater und begann ihn loszubinden.</ta>
            <ta e="T836" id="Seg_15111" s="T834">Er band ihn nicht los.</ta>
            <ta e="T839" id="Seg_15112" s="T836">"Ich kann dich nicht losbinden."</ta>
            <ta e="T843" id="Seg_15113" s="T839">Der Vater sagte: "Bring eine Raspel."</ta>
            <ta e="T850" id="Seg_15114" s="T843">Er brachte sie und begann die Seile zu zerreiben.</ta>
            <ta e="T853" id="Seg_15115" s="T850">Er zerrieb sie, die Seile flogen zur Seite(?).</ta>
            <ta e="T858" id="Seg_15116" s="T853">Er sagte zu seinem Sohn: "Geh und schlaf.</ta>
            <ta e="T861" id="Seg_15117" s="T858">Hab morgen keine Angst.</ta>
            <ta e="T865" id="Seg_15118" s="T861">Wir werden mit dir gemeinsam leben."</ta>
            <ta e="T868" id="Seg_15119" s="T865">Der Tag brach an.</ta>
            <ta e="T879" id="Seg_15120" s="T868">Er legte die Seile auf seine Hände (als wäre er gefesselt) und setzte sich auf die Knie.</ta>
            <ta e="T884" id="Seg_15121" s="T879">Seine Frau stand auf, sie kam zu ihm und spuckte ihm ins Gesicht.</ta>
            <ta e="T888" id="Seg_15122" s="T884">Sie sagte zum Räuber: "Jetzt lass uns ihn töten."</ta>
            <ta e="T893" id="Seg_15123" s="T888">Und der Räuber sagte: "Lass uns ihn töten, nachdem wir gegessen haben."</ta>
            <ta e="T896" id="Seg_15124" s="T893">Sie kochte Fleisch.</ta>
            <ta e="T897" id="Seg_15125" s="T896">Sie aßen.</ta>
            <ta e="T900" id="Seg_15126" s="T897">"Gut, jetzt lass uns ihn töten.</ta>
            <ta e="T904" id="Seg_15127" s="T900">Sollen wir ihn im Haus oder draußen töten?"</ta>
            <ta e="T908" id="Seg_15128" s="T904">Der Räuber sagte: "Wir werden ihn draußen töten.</ta>
            <ta e="T912" id="Seg_15129" s="T908">Komm schon, steh auf, geh nach draußen.</ta>
            <ta e="T914" id="Seg_15130" s="T912">Wir werden dich töten.</ta>
            <ta e="T918" id="Seg_15131" s="T914">Du hast die Seile nicht zerrissen, oder?</ta>
            <ta e="T922" id="Seg_15132" s="T918">Du hast gesagt, du wärst stark."</ta>
            <ta e="T926" id="Seg_15133" s="T922">Die Frau spuckte ihm ins Gesicht.</ta>
            <ta e="T933" id="Seg_15134" s="T926">Plötzlich sprang er auf und schlug den Räuber.</ta>
            <ta e="T937" id="Seg_15135" s="T933">Der Räuber fiel auf den Rücken.</ta>
            <ta e="T939" id="Seg_15136" s="T937">Er tötete [den Räuber].</ta>
            <ta e="T943" id="Seg_15137" s="T939">Und seine Frau legte ihre Arme um seinen Hals.</ta>
            <ta e="T948" id="Seg_15138" s="T943">Sie sagte: "Ich werde es nie wieder tun.</ta>
            <ta e="T950" id="Seg_15139" s="T948">Ich werde deine Füße waschen.</ta>
            <ta e="T953" id="Seg_15140" s="T950">Ich werde dieses Wasser trinken."</ta>
            <ta e="T956" id="Seg_15141" s="T953">Ihr Ehemann schlug sie.</ta>
            <ta e="T958" id="Seg_15142" s="T956">Und er tötete sie.</ta>
            <ta e="T961" id="Seg_15143" s="T958">Die Kinder begannen zu weinen.</ta>
            <ta e="T966" id="Seg_15144" s="T961">Er schlug seine Tochter und tötete sie.</ta>
            <ta e="T972" id="Seg_15145" s="T966">Er sagte zu seinem Sohn: "Wir werden gemeinsam mit dir leben.</ta>
            <ta e="T974" id="Seg_15146" s="T972">Weine nicht."</ta>
            <ta e="T977" id="Seg_15147" s="T974">Er ging um im Boden zu graben.</ta>
            <ta e="T980" id="Seg_15148" s="T977">Er grub eine Grube.</ta>
            <ta e="T987" id="Seg_15149" s="T980">Er warf sie dort hinein und begrub sie.</ta>
            <ta e="T991" id="Seg_15150" s="T987">Er zog sein Boot zum Fluss.</ta>
            <ta e="T994" id="Seg_15151" s="T991">Er trug all sein Habe.</ta>
            <ta e="T999" id="Seg_15152" s="T994">Er stieg mit seinem Sohn ins Boot und sie brachen auf.</ta>
            <ta e="T1003" id="Seg_15153" s="T999">Sie kamen in ihr Dorf.</ta>
            <ta e="T1005" id="Seg_15154" s="T1003">Die Leute waren froh.</ta>
            <ta e="T1009" id="Seg_15155" s="T1005">"Und wo ist deine Frau?"</ta>
            <ta e="T1011" id="Seg_15156" s="T1009">"Meine Frau ist gestorben."</ta>
            <ta e="T1014" id="Seg_15157" s="T1011">Sie brachten all ihr Habe.</ta>
            <ta e="T1018" id="Seg_15158" s="T1014">Er kaufte ein Haus.</ta>
            <ta e="T1024" id="Seg_15159" s="T1018">Er heiratete und begann zu leben.</ta>
            <ta e="T1028" id="Seg_15160" s="T1024">Letztes Jahr habe ich ihn besucht.</ta>
            <ta e="T1031" id="Seg_15161" s="T1028">Ihm geht es gut.</ta>
            <ta e="T1037" id="Seg_15162" s="T1031">Seine Frau ist eine gute Frau, sie gab mir Tee.</ta>
            <ta e="T1041" id="Seg_15163" s="T1037">Sie sagte: "Komm uns besuchen."</ta>
            <ta e="T1045" id="Seg_15164" s="T1041">Ich kam bei ihnen immer wieder zu Besuch.</ta>
            <ta e="T1048" id="Seg_15165" s="T1045">Jetzt gehe ich nach Hause (in mein Dorf).</ta>
            <ta e="T1051" id="Seg_15166" s="T1048">Ich werde wieder bei ihnen vorbeikommen.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_15167" s="T1">Старик Христян.</ta>
            <ta e="T6" id="Seg_15168" s="T3">Родился Христян-старик.</ta>
            <ta e="T9" id="Seg_15169" s="T6">У него три сына.</ta>
            <ta e="T12" id="Seg_15170" s="T9">Сыновья все женились.</ta>
            <ta e="T19" id="Seg_15171" s="T12">Один сын все на охоту ходит, старший сын.</ta>
            <ta e="T30" id="Seg_15172" s="T19">А два сына за скотиной ухаживают, коров держат, лошадей и овечек.</ta>
            <ta e="T35" id="Seg_15173" s="T30">И пашню держат, хлеб сеют.</ta>
            <ta e="T42" id="Seg_15174" s="T35">А старший сын все в тайгу ходит.</ta>
            <ta e="T48" id="Seg_15175" s="T42">У него дочь родилась, и сын родился.</ta>
            <ta e="T52" id="Seg_15176" s="T48">Они уже большие стали.</ta>
            <ta e="T61" id="Seg_15177" s="T52">А он там в тайге большой дом поставил, большой дом.</ta>
            <ta e="T67" id="Seg_15178" s="T61">Матери, отцу сказал: “Иконой меня благослови”.</ta>
            <ta e="T72" id="Seg_15179" s="T67">Мать икону взяла и благословила.</ta>
            <ta e="T74" id="Seg_15180" s="T72">“Я пойду.</ta>
            <ta e="T80" id="Seg_15181" s="T74">Три брата в одном доме жить не будем.</ta>
            <ta e="T83" id="Seg_15182" s="T80">У меня теперь семья”.</ta>
            <ta e="T87" id="Seg_15183" s="T83">В лодку все свои вещи притащил.</ta>
            <ta e="T90" id="Seg_15184" s="T87">И они отправились.</ta>
            <ta e="T96" id="Seg_15185" s="T90">Отец, мать, заплакав, остались.</ta>
            <ta e="T101" id="Seg_15186" s="T96">И они уехали.</ta>
            <ta e="T106" id="Seg_15187" s="T101">Ну и приехали.</ta>
            <ta e="T112" id="Seg_15188" s="T106">А него там большой дом стоит (поставил).</ta>
            <ta e="T115" id="Seg_15189" s="T112">Всё притащили туда.</ta>
            <ta e="T123" id="Seg_15190" s="T115">Ну и муж все делает, спальное место сделал.</ta>
            <ta e="T126" id="Seg_15191" s="T123">“Завтра в тайгу пойду.</ta>
            <ta e="T131" id="Seg_15192" s="T126">А ты по воду пойдешь.</ta>
            <ta e="T137" id="Seg_15193" s="T131">На ноги какие-нибудь стружки чтобы не попали.</ta>
            <ta e="T140" id="Seg_15194" s="T137">А то плохо будет”.</ta>
            <ta e="T142" id="Seg_15195" s="T140">Ну, ночевали.</ta>
            <ta e="T149" id="Seg_15196" s="T142">Утром встал, поел и ушёл в тайгу.</ta>
            <ta e="T162" id="Seg_15197" s="T149">А женщина сказала: “Почему мне муж сказал, чтобы стружка в воду не попала?</ta>
            <ta e="T172" id="Seg_15198" s="T162">Я сейчас два ведра набью, набью, пойду в воду высыплю”.</ta>
            <ta e="T177" id="Seg_15199" s="T172">И унесла, и высыпала.</ta>
            <ta e="T180" id="Seg_15200" s="T177">Стоит и глядит.</ta>
            <ta e="T183" id="Seg_15201" s="T180">А там стреж был.</ta>
            <ta e="T189" id="Seg_15202" s="T183">Она воды зачерпнула, домой пришла.</ta>
            <ta e="T194" id="Seg_15203" s="T189">А она красивая женщина была.</ta>
            <ta e="T201" id="Seg_15204" s="T194">Пошла и в комнату села, и шить стала.</ta>
            <ta e="T205" id="Seg_15205" s="T201">А двое ребятишек играют.</ta>
            <ta e="T215" id="Seg_15206" s="T205">Разбойники ехали в лодке, говорят: “Что за стружки стрежнем идут?</ta>
            <ta e="T218" id="Seg_15207" s="T215">К берегу правь!”</ta>
            <ta e="T222" id="Seg_15208" s="T218">Они посмотрели: какая-то дорожка.</ta>
            <ta e="T225" id="Seg_15209" s="T222">Они тропинку нашли.</ta>
            <ta e="T231" id="Seg_15210" s="T225">“Раньше мы эту дорогу не видели”.</ta>
            <ta e="T239" id="Seg_15211" s="T231">На эту дорожку (тропинку) и заехали.</ta>
            <ta e="T251" id="Seg_15212" s="T239">Атаман сказал: “Вы тут сидите, а я побегу посмотрю, кто живет тут.”</ta>
            <ta e="T253" id="Seg_15213" s="T251">Он побежал.</ta>
            <ta e="T256" id="Seg_15214" s="T253">Пришел, зашел в избу.</ta>
            <ta e="T257" id="Seg_15215" s="T256">“Здравствуйте.”</ta>
            <ta e="T261" id="Seg_15216" s="T257">Женщина говорит: “Здравствуй.”</ta>
            <ta e="T264" id="Seg_15217" s="T261">Стул поставила.</ta>
            <ta e="T267" id="Seg_15218" s="T264">Говорит: “Садись сюда.”</ta>
            <ta e="T270" id="Seg_15219" s="T267">А он сел.</ta>
            <ta e="T273" id="Seg_15220" s="T270">Разговаривать стали.</ta>
            <ta e="T278" id="Seg_15221" s="T273">Атаман сказал: “Ты выходи за меня замуж”.</ta>
            <ta e="T282" id="Seg_15222" s="T278">“Но у меня муж сильный.</ta>
            <ta e="T288" id="Seg_15223" s="T282">Он тебя убьет и меня убьет”.</ta>
            <ta e="T293" id="Seg_15224" s="T288">“А я тебе веревку дам.</ta>
            <ta e="T303" id="Seg_15225" s="T293">(Волосяная(?) будет и цинковая проволока, эту веревку он не порвет.)</ta>
            <ta e="T307" id="Seg_15226" s="T303">Он вечером придет.</ta>
            <ta e="T310" id="Seg_15227" s="T307">Ты скажи: В карты давай играть будем.</ta>
            <ta e="T318" id="Seg_15228" s="T310">Ему скажи: “Кто дураком останется, руки [надо] назад завязать”.</ta>
            <ta e="T326" id="Seg_15229" s="T318">А ты его дураком оставь, руки назад завяжи.</ta>
            <ta e="T331" id="Seg_15230" s="T326">Он эту веревку не порвет.</ta>
            <ta e="T341" id="Seg_15231" s="T331">Если он не порвет, ты на улицу выйди, меня позови.</ta>
            <ta e="T346" id="Seg_15232" s="T341">Я приду, его и убью.</ta>
            <ta e="T348" id="Seg_15233" s="T346">С тобой жить будем”.</ta>
            <ta e="T350" id="Seg_15234" s="T348">Муж пришел.</ta>
            <ta e="T355" id="Seg_15235" s="T350">Она его накормила, говорит: “Давай в карты играть”.</ta>
            <ta e="T361" id="Seg_15236" s="T355">Муж говорит: “Я устал, надо спать”.</ta>
            <ta e="T366" id="Seg_15237" s="T361">“Ну давай в карты поиграем недолго.</ta>
            <ta e="T373" id="Seg_15238" s="T366">А то я одна сижу, скучно мне”.</ta>
            <ta e="T377" id="Seg_15239" s="T373">Ну и сели в карты играть.</ta>
            <ta e="T383" id="Seg_15240" s="T377">“Кто дураком останется, руки назад завязать [надо]”.</ta>
            <ta e="T386" id="Seg_15241" s="T383">Муж дураком остался.</ta>
            <ta e="T393" id="Seg_15242" s="T386">“Руки надо завязать, руки надо назад завязать.</ta>
            <ta e="T403" id="Seg_15243" s="T393">А если бы я осталась дураком, ты бы мне руки завязал.</ta>
            <ta e="T409" id="Seg_15244" s="T403">А теперь я тебе руки завяжу”.</ta>
            <ta e="T412" id="Seg_15245" s="T409">Мужик сказал: “Завязывай”.</ta>
            <ta e="T414" id="Seg_15246" s="T412">Она завязала.</ta>
            <ta e="T420" id="Seg_15247" s="T414">Муж как поднатужился(?), все веревки порвались.</ta>
            <ta e="T424" id="Seg_15248" s="T420">Ну и спать легли.</ta>
            <ta e="T426" id="Seg_15249" s="T424">Утром встали.</ta>
            <ta e="T433" id="Seg_15250" s="T426">Женщина всего наварила, мяса и всякую всячину, мужа накормила.</ta>
            <ta e="T436" id="Seg_15251" s="T433">Муж в тайгу пошел.</ta>
            <ta e="T443" id="Seg_15252" s="T436">Женщина на улицу выскочила и закричала “Приходи!</ta>
            <ta e="T445" id="Seg_15253" s="T443">Муж ушел!”</ta>
            <ta e="T448" id="Seg_15254" s="T445">Разбойник пришел.</ta>
            <ta e="T451" id="Seg_15255" s="T448">“Ну что, в карты играли?”</ta>
            <ta e="T452" id="Seg_15256" s="T451">“Играли.</ta>
            <ta e="T462" id="Seg_15257" s="T452">Я тебе говорила, он сильный, веревку всю порвал.”</ta>
            <ta e="T470" id="Seg_15258" s="T462">“А сегодня я тебе дам волосяную веревку дам.</ta>
            <ta e="T476" id="Seg_15259" s="T470">Вечером придет опять, говори: Давай в карты играть.</ta>
            <ta e="T482" id="Seg_15260" s="T476">Кто дураком останется, тому [надо] назад руки связать”.</ta>
            <ta e="T487" id="Seg_15261" s="T482">Женщина его накормила, вином напоила.</ta>
            <ta e="T489" id="Seg_15262" s="T487">Вино выпили.</ta>
            <ta e="T491" id="Seg_15263" s="T489">Мужчина ушел.</ta>
            <ta e="T493" id="Seg_15264" s="T491">Муж пришел.</ta>
            <ta e="T499" id="Seg_15265" s="T493">Зверей много добыл и лося добыл.</ta>
            <ta e="T502" id="Seg_15266" s="T499">Она его накормила.</ta>
            <ta e="T504" id="Seg_15267" s="T502">Говорит: “Давай в карты играть”.</ta>
            <ta e="T508" id="Seg_15268" s="T504">Муж говорит: “Я устал”.</ta>
            <ta e="T520" id="Seg_15269" s="T508">Жена говорит: “Давай в карты играть с тем уговором, что кто дураком останется, руки [тому надо] назад завязать.</ta>
            <ta e="T528" id="Seg_15270" s="T520">Если я останусь дураком, мои руки завяжи (завяжешь)”.</ta>
            <ta e="T531" id="Seg_15271" s="T528">Они сели в карты играть.</ta>
            <ta e="T534" id="Seg_15272" s="T531">Муж дураком остался.</ta>
            <ta e="T538" id="Seg_15273" s="T534">“Ну, теперь руки завяжу”.</ta>
            <ta e="T541" id="Seg_15274" s="T538">Она завязала руки.</ta>
            <ta e="T546" id="Seg_15275" s="T541">Он как поднатужился, веревка оборвалась.</ta>
            <ta e="T548" id="Seg_15276" s="T546">Легли спать.</ta>
            <ta e="T550" id="Seg_15277" s="T548">Утром встали.</ta>
            <ta e="T553" id="Seg_15278" s="T550">Женщина мужа накормила.</ta>
            <ta e="T559" id="Seg_15279" s="T553">Муж поел и в тайгу ушел.</ta>
            <ta e="T566" id="Seg_15280" s="T559">Женщина на улицу пошла и закричала: “Приходи!</ta>
            <ta e="T568" id="Seg_15281" s="T566">Мой муж ушел!”</ta>
            <ta e="T570" id="Seg_15282" s="T568">Разбойник пришел.</ta>
            <ta e="T576" id="Seg_15283" s="T570">Женщина говорит: “Веревки мой муж все порвал.</ta>
            <ta e="T579" id="Seg_15284" s="T576">Он сильный”.</ta>
            <ta e="T591" id="Seg_15285" s="T579">“А я сегодня тебе дам веревку волосяная и цинковая проволока все вместе накрученная.</ta>
            <ta e="T599" id="Seg_15286" s="T591">Вечером он придет, ты опять скажи: в карты играть, давай в карты сыграем.</ta>
            <ta e="T606" id="Seg_15287" s="T599">Кто дураком останется, то руки [надо] назад завязать.”</ta>
            <ta e="T608" id="Seg_15288" s="T606">Муж пришел.</ta>
            <ta e="T613" id="Seg_15289" s="T608">Она его накормила, говорит: “Давай в карты играть”.</ta>
            <ta e="T619" id="Seg_15290" s="T613">Муж сказал: “Я устал, надо спать”.</ta>
            <ta e="T622" id="Seg_15291" s="T619">“Ну давай играть в карты, давай играть.</ta>
            <ta e="T630" id="Seg_15292" s="T622">Хоть раз кто дураком останется, [тому надо] руки назад завязать.</ta>
            <ta e="T637" id="Seg_15293" s="T630">Если я останусь, мои руки завяжи назад”.</ta>
            <ta e="T640" id="Seg_15294" s="T637">И сели играть.</ta>
            <ta e="T646" id="Seg_15295" s="T640">Мужа дураком оставила, руки его назад завязала.</ta>
            <ta e="T652" id="Seg_15296" s="T646">Он как поднатужился, веревки не порвал.</ta>
            <ta e="T656" id="Seg_15297" s="T652">Жене сказал: “Развяжи мои руки”.</ta>
            <ta e="T659" id="Seg_15298" s="T656">Жена говорит: “Рви”.</ta>
            <ta e="T664" id="Seg_15299" s="T659">Он опять поднатужился, не порвал.</ta>
            <ta e="T666" id="Seg_15300" s="T664">Говорит: “Развяжи”.</ta>
            <ta e="T668" id="Seg_15301" s="T666">“Не развяжу”.</ta>
            <ta e="T675" id="Seg_15302" s="T668">На улицу выскочила и закричала: “Приходи!”</ta>
            <ta e="T680" id="Seg_15303" s="T675">И этот разбойник пришел.</ta>
            <ta e="T685" id="Seg_15304" s="T680">Говорит: “Мой муж веревку не порвал.</ta>
            <ta e="T687" id="Seg_15305" s="T685">Зайдем и убьем”.</ta>
            <ta e="T690" id="Seg_15306" s="T687">И в избу они двое зашли.</ta>
            <ta e="T695" id="Seg_15307" s="T690">Разбойник говорит: “Завтра утром убьем.</ta>
            <ta e="T699" id="Seg_15308" s="T695">Занеси полное ведро соли.</ta>
            <ta e="T705" id="Seg_15309" s="T699">Его на соль коленками (в соль) посадим.</ta>
            <ta e="T707" id="Seg_15310" s="T705">До утра (пусть) сидит.</ta>
            <ta e="T709" id="Seg_15311" s="T707">Утром убьем”.</ta>
            <ta e="T714" id="Seg_15312" s="T709">Женщина ему в лицо, подойдя, плюнула.</ta>
            <ta e="T720" id="Seg_15313" s="T714">“Ты же говорил, что я, мол, сильный.</ta>
            <ta e="T725" id="Seg_15314" s="T720">А почему веревку не порвешь?</ta>
            <ta e="T729" id="Seg_15315" s="T725">Теперь утром мы тебя убьем”.</ta>
            <ta e="T738" id="Seg_15316" s="T729">Женщина побежала, полное ведро соли занесла и на пол высыпала.</ta>
            <ta e="T742" id="Seg_15317" s="T738">И они его на коленки поставили.</ta>
            <ta e="T745" id="Seg_15318" s="T742">Говорит: “Сиди до утра!</ta>
            <ta e="T748" id="Seg_15319" s="T745">Утром тебя убьем”.</ta>
            <ta e="T754" id="Seg_15320" s="T748">Сели закусывать с разбойником и вино выпивать.</ta>
            <ta e="T762" id="Seg_15321" s="T754">Женщина пришла, мужу, мужу в лицо плюнула.</ta>
            <ta e="T766" id="Seg_15322" s="T762">“Утром мы тебя убьем”.</ta>
            <ta e="T773" id="Seg_15323" s="T766">Сама пошла, разбойника обнимает, целует.</ta>
            <ta e="T779" id="Seg_15324" s="T773">И спать легли, и уснули.</ta>
            <ta e="T784" id="Seg_15325" s="T779">(Муж) он встал, ходит.</ta>
            <ta e="T792" id="Seg_15326" s="T784">А маленькая дочь и сынишка вдвоем спали на полатях.</ta>
            <ta e="T796" id="Seg_15327" s="T792">Дочка пописать спустилась.</ta>
            <ta e="T802" id="Seg_15328" s="T796">Он дочке говорит: “Мои руки развяжи”.</ta>
            <ta e="T810" id="Seg_15329" s="T802">А дочка сказала: “Ты говорил, что я, мол, сильный.</ta>
            <ta e="T812" id="Seg_15330" s="T810">Разорви веревку”.</ta>
            <ta e="T817" id="Seg_15331" s="T812">На полати залезла и заснула.</ta>
            <ta e="T822" id="Seg_15332" s="T817">Сын с палатей слез пописать.</ta>
            <ta e="T828" id="Seg_15333" s="T822">Он сыну говорит: “Мои руки развяжи”.</ta>
            <ta e="T834" id="Seg_15334" s="T828">Он подошел к отцу и стал развязывать.</ta>
            <ta e="T836" id="Seg_15335" s="T834">Не развязал.</ta>
            <ta e="T839" id="Seg_15336" s="T836">“Мне не развязать”.</ta>
            <ta e="T843" id="Seg_15337" s="T839">Отец говорит: “Подпилок принеси”.</ta>
            <ta e="T850" id="Seg_15338" s="T843">Он принес и стал точить веревки.</ta>
            <ta e="T853" id="Seg_15339" s="T850">Обточил, веревки разлетелись(?).</ta>
            <ta e="T858" id="Seg_15340" s="T853">Он сыну говорит: “Иди спи.</ta>
            <ta e="T861" id="Seg_15341" s="T858">Утром не бойся.</ta>
            <ta e="T865" id="Seg_15342" s="T861">Мы с тобой вдвоем сами жить будем”.</ta>
            <ta e="T868" id="Seg_15343" s="T865">Светать начинает.</ta>
            <ta e="T879" id="Seg_15344" s="T868">Он веревки на руки положил (как будто он завязан) и сидит, на коленках сидит.</ta>
            <ta e="T884" id="Seg_15345" s="T879">Жена встала, пришла, в лицо плюнула.</ta>
            <ta e="T888" id="Seg_15346" s="T884">Разбойнику говорит: “Ну, убьем”.</ta>
            <ta e="T893" id="Seg_15347" s="T888">А разбойник говорит: “Поев, убьем”.</ta>
            <ta e="T896" id="Seg_15348" s="T893">Она мяса наварила.</ta>
            <ta e="T897" id="Seg_15349" s="T896">Они поели.</ta>
            <ta e="T900" id="Seg_15350" s="T897">“Ну, теперь давай его убьем.</ta>
            <ta e="T904" id="Seg_15351" s="T900">Дома убьем или на улице?”</ta>
            <ta e="T908" id="Seg_15352" s="T904">Разбойник говорит: “На улице убьем.</ta>
            <ta e="T912" id="Seg_15353" s="T908">Ну, вставай, иди на улицу.</ta>
            <ta e="T914" id="Seg_15354" s="T912">Мы тебя убьем.</ta>
            <ta e="T918" id="Seg_15355" s="T914">Что, веревки ты не оборвал.</ta>
            <ta e="T922" id="Seg_15356" s="T918">Ты говорил, что я, мол, сильный”.</ta>
            <ta e="T926" id="Seg_15357" s="T922">Женщина как в лицо плюнула.</ta>
            <ta e="T933" id="Seg_15358" s="T926">Он как вскочил наверх, как разбойника ударил.</ta>
            <ta e="T937" id="Seg_15359" s="T933">Разбойник навзничь упал.</ta>
            <ta e="T939" id="Seg_15360" s="T937">И убил.</ta>
            <ta e="T943" id="Seg_15361" s="T939">А жена на шее у него повисла.</ta>
            <ta e="T948" id="Seg_15362" s="T943">Говорит: “Я так делать не буду.</ta>
            <ta e="T950" id="Seg_15363" s="T948">Ноги твои мыть буду.</ta>
            <ta e="T953" id="Seg_15364" s="T950">Эту воду буду пить”.</ta>
            <ta e="T956" id="Seg_15365" s="T953">Муж ее как ударит.</ta>
            <ta e="T958" id="Seg_15366" s="T956">И убил.</ta>
            <ta e="T961" id="Seg_15367" s="T958">Дети плакать стали.</ta>
            <ta e="T966" id="Seg_15368" s="T961">Дочку как ударил, и убил.</ta>
            <ta e="T972" id="Seg_15369" s="T966">И сыну сказал: “Мы с тобой жить будем.</ta>
            <ta e="T974" id="Seg_15370" s="T972">Не плачь”.</ta>
            <ta e="T977" id="Seg_15371" s="T974">Он пошел землю копать.</ta>
            <ta e="T980" id="Seg_15372" s="T977">Яму выкопал.</ta>
            <ta e="T987" id="Seg_15373" s="T980">Их там раскидал и землей закопал.</ta>
            <ta e="T991" id="Seg_15374" s="T987">Лодку к реке потащил.</ta>
            <ta e="T994" id="Seg_15375" s="T991">Вещи все перетаскал.</ta>
            <ta e="T999" id="Seg_15376" s="T994">С сынишкой сели и поехали.</ta>
            <ta e="T1003" id="Seg_15377" s="T999">Приехали в свою деревню.</ta>
            <ta e="T1005" id="Seg_15378" s="T1003">Люди (свои) радуются.</ta>
            <ta e="T1009" id="Seg_15379" s="T1005">“А жена твоя где?”</ta>
            <ta e="T1011" id="Seg_15380" s="T1009">“Жена моя умерла”.</ta>
            <ta e="T1014" id="Seg_15381" s="T1011">Все вещи стаскали.</ta>
            <ta e="T1018" id="Seg_15382" s="T1014">Он себе дом купил.</ta>
            <ta e="T1024" id="Seg_15383" s="T1018">И женился, и стал жить.</ta>
            <ta e="T1028" id="Seg_15384" s="T1024">Я в прошлом году была у него.</ta>
            <ta e="T1031" id="Seg_15385" s="T1028">До того хорошо живет.</ta>
            <ta e="T1037" id="Seg_15386" s="T1031">Его жена хорошая, меня чаем меня напоила.</ta>
            <ta e="T1041" id="Seg_15387" s="T1037">Она мне говорила: “К нам ходи”.</ta>
            <ta e="T1045" id="Seg_15388" s="T1041">Я все время к ним заезжаю.</ta>
            <ta e="T1048" id="Seg_15389" s="T1045">Нонче домой (в деревню) поеду.</ta>
            <ta e="T1051" id="Seg_15390" s="T1048">К ним опять заходить буду.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_15391" s="T3">родился Христян старик</ta>
            <ta e="T9" id="Seg_15392" s="T6">у него три сына</ta>
            <ta e="T12" id="Seg_15393" s="T9">сыновья все женились</ta>
            <ta e="T19" id="Seg_15394" s="T12">один сын все на охоту ходит больший сын</ta>
            <ta e="T30" id="Seg_15395" s="T19">а два сына за скотиной ходят коров держат коней и овечек</ta>
            <ta e="T35" id="Seg_15396" s="T30">и пашню держат хлеб сеют</ta>
            <ta e="T42" id="Seg_15397" s="T35">а старший сын все в тайгу ходит</ta>
            <ta e="T48" id="Seg_15398" s="T42">у него дочь родилась и сын родился</ta>
            <ta e="T52" id="Seg_15399" s="T48">они уже большие стали</ta>
            <ta e="T61" id="Seg_15400" s="T52">он там в тайге большой дом себе поставил большой дом</ta>
            <ta e="T67" id="Seg_15401" s="T61">матери отцу сказал иконой меня благословите</ta>
            <ta e="T72" id="Seg_15402" s="T67">мать икону взяла и благословила</ta>
            <ta e="T74" id="Seg_15403" s="T72">я пойду</ta>
            <ta e="T80" id="Seg_15404" s="T74">три брата в одном дому жить не будем</ta>
            <ta e="T83" id="Seg_15405" s="T80">у меня теперь семья</ta>
            <ta e="T87" id="Seg_15406" s="T83">в лодку всю одежду (монатки) стаскал</ta>
            <ta e="T90" id="Seg_15407" s="T87">поехали</ta>
            <ta e="T96" id="Seg_15408" s="T90">отец мать заплакали остались</ta>
            <ta e="T101" id="Seg_15409" s="T96">они уехали</ta>
            <ta e="T106" id="Seg_15410" s="T101">ну и приехали</ta>
            <ta e="T112" id="Seg_15411" s="T106">у него там большой дом (поставил) стоит</ta>
            <ta e="T115" id="Seg_15412" s="T112">все стаскали туда</ta>
            <ta e="T123" id="Seg_15413" s="T115">ну и мужик все делает койку сделал</ta>
            <ta e="T126" id="Seg_15414" s="T123">завтра в тайгу пойду</ta>
            <ta e="T131" id="Seg_15415" s="T126">а ты по воду как пойдешь</ta>
            <ta e="T137" id="Seg_15416" s="T131">на ноги какие-нибудь струшки чтоб не попали стружки?</ta>
            <ta e="T140" id="Seg_15417" s="T137">а то худо будет</ta>
            <ta e="T142" id="Seg_15418" s="T140">ночевали</ta>
            <ta e="T149" id="Seg_15419" s="T142">утром встали наелся и ушёл в тайгу</ta>
            <ta e="T162" id="Seg_15420" s="T149">женщина сказала почто (почему) мне мужик сказал чтобы струшка в воду не попала</ta>
            <ta e="T172" id="Seg_15421" s="T162">я сейчас два ведра набью набью пойду в воду вывалю</ta>
            <ta e="T177" id="Seg_15422" s="T172">унесла и вывалила</ta>
            <ta e="T180" id="Seg_15423" s="T177">стоит и глядит</ta>
            <ta e="T183" id="Seg_15424" s="T180">а там стреж бы</ta>
            <ta e="T189" id="Seg_15425" s="T183">она воды черпнула домой пришла</ta>
            <ta e="T194" id="Seg_15426" s="T189">а она красива баба была</ta>
            <ta e="T201" id="Seg_15427" s="T194">пошла и в комнату села (сидит) и шить (шиться) стала</ta>
            <ta e="T205" id="Seg_15428" s="T201">ребятишки (двое) играют</ta>
            <ta e="T215" id="Seg_15429" s="T205">разбойники ехали в лодке говорят какие струшки стрежнем идут (напротив стрежня)</ta>
            <ta e="T218" id="Seg_15430" s="T215">к берегу правьте</ta>
            <ta e="T222" id="Seg_15431" s="T218">они глядели какая-то дорожка</ta>
            <ta e="T225" id="Seg_15432" s="T222">они тропинку нашли</ta>
            <ta e="T231" id="Seg_15433" s="T225">раньше мы эту дорогу не видели</ta>
            <ta e="T239" id="Seg_15434" s="T231">на эту дорожку (тропинку) и заехали</ta>
            <ta e="T251" id="Seg_15435" s="T239">атаман сказал вы тут сидите а я побегу посмотрю кто живет тут</ta>
            <ta e="T253" id="Seg_15436" s="T251">побежал</ta>
            <ta e="T256" id="Seg_15437" s="T253">пришел зашел в избу</ta>
            <ta e="T257" id="Seg_15438" s="T256">здравствуйте</ta>
            <ta e="T261" id="Seg_15439" s="T257">женщина говорит здравствуй</ta>
            <ta e="T264" id="Seg_15440" s="T261">стул поставила</ta>
            <ta e="T267" id="Seg_15441" s="T264">садись сюда</ta>
            <ta e="T270" id="Seg_15442" s="T267">а он сел</ta>
            <ta e="T273" id="Seg_15443" s="T270">разговаривать стали</ta>
            <ta e="T278" id="Seg_15444" s="T273">атаман сказал ты за меня иди замуж</ta>
            <ta e="T282" id="Seg_15445" s="T278">но у меня мужик сильный</ta>
            <ta e="T288" id="Seg_15446" s="T282">он тебя убьет и меня убьет</ta>
            <ta e="T293" id="Seg_15447" s="T288">а я тебе веревку дам</ta>
            <ta e="T303" id="Seg_15448" s="T293">эту веревку он не порвет</ta>
            <ta e="T307" id="Seg_15449" s="T303">он вечером придет</ta>
            <ta e="T310" id="Seg_15450" s="T307">ты скажи в карты давай играть будем</ta>
            <ta e="T318" id="Seg_15451" s="T310">ему скажи кто дураком останется руки назад завязать</ta>
            <ta e="T326" id="Seg_15452" s="T318">ты его дураком оставь руки назад завяжи</ta>
            <ta e="T331" id="Seg_15453" s="T326">он эту веревку не порвет</ta>
            <ta e="T341" id="Seg_15454" s="T331">ты на улицу выйди меня закричи</ta>
            <ta e="T346" id="Seg_15455" s="T341">я приду его убью</ta>
            <ta e="T348" id="Seg_15456" s="T346">с тобой жить буду</ta>
            <ta e="T350" id="Seg_15457" s="T348">мужик пришел</ta>
            <ta e="T355" id="Seg_15458" s="T350">она его накормила говорит давай в карты играть</ta>
            <ta e="T361" id="Seg_15459" s="T355">муж говорит я устал надо спать</ta>
            <ta e="T366" id="Seg_15460" s="T361">ну давай в карты играть маленько</ta>
            <ta e="T373" id="Seg_15461" s="T366">а то сама (одна) сижу скучно мне</ta>
            <ta e="T377" id="Seg_15462" s="T373">ну и сели в карты играть</ta>
            <ta e="T383" id="Seg_15463" s="T377">кто дураком останется руки назад завязать</ta>
            <ta e="T386" id="Seg_15464" s="T383">мужик дураком остался</ta>
            <ta e="T393" id="Seg_15465" s="T386">руки надо завязать руки надо назад завязать</ta>
            <ta e="T403" id="Seg_15466" s="T393">а если бы я осталась дураком ты бы мне руки завязал</ta>
            <ta e="T409" id="Seg_15467" s="T403">а теперь я тебе руки завяжу</ta>
            <ta e="T412" id="Seg_15468" s="T409">мужик сказал завязывай</ta>
            <ta e="T414" id="Seg_15469" s="T412">она завязала</ta>
            <ta e="T420" id="Seg_15470" s="T414">мужик как надулся (поднатужился) все веревки порвались</ta>
            <ta e="T424" id="Seg_15471" s="T420">ну и спать легли</ta>
            <ta e="T426" id="Seg_15472" s="T424">утром встали</ta>
            <ta e="T433" id="Seg_15473" s="T426">женщина все наварила (наготовила) мяса всё (всякую всячину) мужика накормила</ta>
            <ta e="T436" id="Seg_15474" s="T433">старик в тайгу пошел</ta>
            <ta e="T443" id="Seg_15475" s="T436">женщина на улицу и заревела приходи</ta>
            <ta e="T445" id="Seg_15476" s="T443">муж ушел</ta>
            <ta e="T448" id="Seg_15477" s="T445">пришел</ta>
            <ta e="T451" id="Seg_15478" s="T448">ну чего в карты играли</ta>
            <ta e="T452" id="Seg_15479" s="T451">играли</ta>
            <ta e="T462" id="Seg_15480" s="T452">я тебе говорила он сильный веревку всю порвал</ta>
            <ta e="T470" id="Seg_15481" s="T462">а сегодня я тебе дам волосяную веревку дам</ta>
            <ta e="T476" id="Seg_15482" s="T470">вечером придет опять говори давай в карты играть</ta>
            <ta e="T482" id="Seg_15483" s="T476">кто дураком останется тому назад руки связать</ta>
            <ta e="T487" id="Seg_15484" s="T482">она его накормила вином напоила</ta>
            <ta e="T489" id="Seg_15485" s="T487">вино выпили</ta>
            <ta e="T491" id="Seg_15486" s="T489">мужчина ушел</ta>
            <ta e="T493" id="Seg_15487" s="T491">муж пришел</ta>
            <ta e="T499" id="Seg_15488" s="T493">зверей много добыл и лося добыл</ta>
            <ta e="T502" id="Seg_15489" s="T499">она его накормила</ta>
            <ta e="T504" id="Seg_15490" s="T502">говорит давай в карты играть</ta>
            <ta e="T508" id="Seg_15491" s="T504">мужик говорит я устал</ta>
            <ta e="T520" id="Seg_15492" s="T508">жена говорит давай в карты играть кто дураком останется руки назад завязать</ta>
            <ta e="T528" id="Seg_15493" s="T520">если я останусь дураком мои руки (завяжешь) завяжи</ta>
            <ta e="T531" id="Seg_15494" s="T528">они сели в карты играть</ta>
            <ta e="T534" id="Seg_15495" s="T531">мужик дураком остался</ta>
            <ta e="T538" id="Seg_15496" s="T534">ну теперь руки завяжу</ta>
            <ta e="T541" id="Seg_15497" s="T538">она завязала руки</ta>
            <ta e="T546" id="Seg_15498" s="T541">он как поднатужился веревка оборвалась</ta>
            <ta e="T548" id="Seg_15499" s="T546">легли спать</ta>
            <ta e="T550" id="Seg_15500" s="T548">утром встали</ta>
            <ta e="T553" id="Seg_15501" s="T550">женщина мужа накормила</ta>
            <ta e="T559" id="Seg_15502" s="T553">мужик наелся и в тайгу ушел</ta>
            <ta e="T566" id="Seg_15503" s="T559">женщина на улицу ушла и заревела приходи</ta>
            <ta e="T568" id="Seg_15504" s="T566">мужик ушел</ta>
            <ta e="T570" id="Seg_15505" s="T568">пришел</ta>
            <ta e="T576" id="Seg_15506" s="T570">женщина говорит ему веревки старик все порвал</ta>
            <ta e="T579" id="Seg_15507" s="T576">он сильный</ta>
            <ta e="T591" id="Seg_15508" s="T579">я сегодня тебе дам веревку волосяная и цинковая проволока все вместе накрученная</ta>
            <ta e="T599" id="Seg_15509" s="T591">вечером придет ты опять скажи играть давай играть</ta>
            <ta e="T606" id="Seg_15510" s="T599">кто дураком останется то руки назад завязать</ta>
            <ta e="T608" id="Seg_15511" s="T606">муж пришел</ta>
            <ta e="T613" id="Seg_15512" s="T608">она его накормила говорит давай в карты играть</ta>
            <ta e="T619" id="Seg_15513" s="T613">мужик сказал я устал надо спать</ta>
            <ta e="T622" id="Seg_15514" s="T619">ну давай играть в карты</ta>
            <ta e="T630" id="Seg_15515" s="T622">хоть раз кто дураком останется руки назад завязать</ta>
            <ta e="T637" id="Seg_15516" s="T630">если я останусь мои руки завязать назад</ta>
            <ta e="T640" id="Seg_15517" s="T637">и сели играть</ta>
            <ta e="T646" id="Seg_15518" s="T640">мужика дураком оставила руки назад завязала</ta>
            <ta e="T652" id="Seg_15519" s="T646">он как поднатужился веревки не порвались</ta>
            <ta e="T656" id="Seg_15520" s="T652">жене сказал развяжи мои руки</ta>
            <ta e="T659" id="Seg_15521" s="T656">жена говорит рви</ta>
            <ta e="T664" id="Seg_15522" s="T659">он опять поднатужился не порвал</ta>
            <ta e="T666" id="Seg_15523" s="T664">говорит развяжи</ta>
            <ta e="T668" id="Seg_15524" s="T666">не развяжу</ta>
            <ta e="T675" id="Seg_15525" s="T668">на улицу (скоро) убежала и заревела приходи</ta>
            <ta e="T680" id="Seg_15526" s="T675">и разбойник пришел</ta>
            <ta e="T685" id="Seg_15527" s="T680">говорит мужик (мой) веревку не порвал</ta>
            <ta e="T687" id="Seg_15528" s="T685">зайдем и убьем</ta>
            <ta e="T690" id="Seg_15529" s="T687">и в избу зашли (двое)</ta>
            <ta e="T695" id="Seg_15530" s="T690">говорит завтра утром убьем</ta>
            <ta e="T699" id="Seg_15531" s="T695">принеси (занеси) целое ведро соль</ta>
            <ta e="T705" id="Seg_15532" s="T699">его на соль коленками поставим</ta>
            <ta e="T707" id="Seg_15533" s="T705">до утра пусть сидит</ta>
            <ta e="T709" id="Seg_15534" s="T707">утром убьем</ta>
            <ta e="T714" id="Seg_15535" s="T709">женщина в лицо пришла (подойдя) плюнула</ta>
            <ta e="T720" id="Seg_15536" s="T714">ты же говорил что я сильный</ta>
            <ta e="T725" id="Seg_15537" s="T720">а почто веревку не оборвешь</ta>
            <ta e="T729" id="Seg_15538" s="T725">теперь утром мы тебя убьем</ta>
            <ta e="T738" id="Seg_15539" s="T729">женщина побежала соль притащила ведро полное на пол высыпала</ta>
            <ta e="T742" id="Seg_15540" s="T738">его на коленки поставили</ta>
            <ta e="T745" id="Seg_15541" s="T742">говорит сиди</ta>
            <ta e="T748" id="Seg_15542" s="T745">утром тебя убьем</ta>
            <ta e="T754" id="Seg_15543" s="T748">сели закусывать с разбойником и вино выпивать</ta>
            <ta e="T762" id="Seg_15544" s="T754">женщина пришла мужику к мужику в лицо плюнула</ta>
            <ta e="T766" id="Seg_15545" s="T762">утром мы тебя убьем</ta>
            <ta e="T773" id="Seg_15546" s="T766">сама пошла разбойника обнимает (она) целует</ta>
            <ta e="T779" id="Seg_15547" s="T773">спать легли они уснули</ta>
            <ta e="T784" id="Seg_15548" s="T779">он встал ходит</ta>
            <ta e="T792" id="Seg_15549" s="T784">маленькая дочь и сынишка вдвоем спали на палатях</ta>
            <ta e="T796" id="Seg_15550" s="T792">девчонка пописать спустилась</ta>
            <ta e="T802" id="Seg_15551" s="T796">он девочке говорит мои руки развяжи</ta>
            <ta e="T810" id="Seg_15552" s="T802">девочка сказала ты говорил что ты сильный</ta>
            <ta e="T812" id="Seg_15553" s="T810">разорви веревку</ta>
            <ta e="T817" id="Seg_15554" s="T812">на палати залезла заснула</ta>
            <ta e="T822" id="Seg_15555" s="T817">сын с палатей слез писать</ta>
            <ta e="T828" id="Seg_15556" s="T822">он сыну говорит мои руки развяжи</ta>
            <ta e="T834" id="Seg_15557" s="T828">он подошел к отцу развязывать стал</ta>
            <ta e="T836" id="Seg_15558" s="T834">не развязал</ta>
            <ta e="T839" id="Seg_15559" s="T836">мне не развязать</ta>
            <ta e="T843" id="Seg_15560" s="T839">отец говорит подпилок принеси</ta>
            <ta e="T850" id="Seg_15561" s="T843">он принес веревку точить стал</ta>
            <ta e="T853" id="Seg_15562" s="T850">обточил веревки (развязались) разлетелись</ta>
            <ta e="T858" id="Seg_15563" s="T853">он сыну говорит иди спи</ta>
            <ta e="T861" id="Seg_15564" s="T858">утром не бойся</ta>
            <ta e="T865" id="Seg_15565" s="T861">мы с тобой двое жить будем</ta>
            <ta e="T868" id="Seg_15566" s="T865">светать начинает</ta>
            <ta e="T879" id="Seg_15567" s="T868">он веревки на руки (собрал) положил (как будто он завязан) и сидит на коленке сидит</ta>
            <ta e="T884" id="Seg_15568" s="T879">жена встала пришла в лицо плюнула</ta>
            <ta e="T888" id="Seg_15569" s="T884">разбойнику говорит ну убьем</ta>
            <ta e="T893" id="Seg_15570" s="T888">а разбойник говорит надо поесть потом убить</ta>
            <ta e="T896" id="Seg_15571" s="T893">она мяса наварила</ta>
            <ta e="T897" id="Seg_15572" s="T896">поели</ta>
            <ta e="T900" id="Seg_15573" s="T897">ну теперь убьем</ta>
            <ta e="T904" id="Seg_15574" s="T900">дома убьем или на улице</ta>
            <ta e="T908" id="Seg_15575" s="T904">говорит на улице убьем</ta>
            <ta e="T912" id="Seg_15576" s="T908">ну вставай иди на улицу</ta>
            <ta e="T914" id="Seg_15577" s="T912">тебя убьем</ta>
            <ta e="T918" id="Seg_15578" s="T914">веревки не оборвал</ta>
            <ta e="T922" id="Seg_15579" s="T918">ты говорил что я сильный</ta>
            <ta e="T926" id="Seg_15580" s="T922">женщина как в лицо плюнула</ta>
            <ta e="T933" id="Seg_15581" s="T926">он как соскочил наверх как разбойника ударил</ta>
            <ta e="T937" id="Seg_15582" s="T933">разбойник навзничь упал</ta>
            <ta e="T939" id="Seg_15583" s="T937">убил</ta>
            <ta e="T943" id="Seg_15584" s="T939">жена на шее как поймалась</ta>
            <ta e="T948" id="Seg_15585" s="T943">говорит я так делать не буду</ta>
            <ta e="T950" id="Seg_15586" s="T948">ноги твои мыть буду</ta>
            <ta e="T953" id="Seg_15587" s="T950">эту воду буду пить</ta>
            <ta e="T956" id="Seg_15588" s="T953">муж как ударит</ta>
            <ta e="T958" id="Seg_15589" s="T956">убил</ta>
            <ta e="T961" id="Seg_15590" s="T958">детки плакать стали</ta>
            <ta e="T966" id="Seg_15591" s="T961">девчонку как ударил убил</ta>
            <ta e="T972" id="Seg_15592" s="T966">сыну сказал мы с тобой жить будем</ta>
            <ta e="T974" id="Seg_15593" s="T972">не плачь</ta>
            <ta e="T977" id="Seg_15594" s="T974">пошел яму (землю копать)</ta>
            <ta e="T980" id="Seg_15595" s="T977">яму вскопал</ta>
            <ta e="T987" id="Seg_15596" s="T980">он их скидал землей закопал</ta>
            <ta e="T991" id="Seg_15597" s="T987">лодку наверх потащил</ta>
            <ta e="T994" id="Seg_15598" s="T991">монатки (вещи) все стаскал</ta>
            <ta e="T999" id="Seg_15599" s="T994">с сынишкой сели и поехали</ta>
            <ta e="T1003" id="Seg_15600" s="T999">приехали в свою деревню</ta>
            <ta e="T1005" id="Seg_15601" s="T1003">свои радуются</ta>
            <ta e="T1009" id="Seg_15602" s="T1005">а жена где</ta>
            <ta e="T1011" id="Seg_15603" s="T1009">жена померла</ta>
            <ta e="T1014" id="Seg_15604" s="T1011">все (вещи) стаскали</ta>
            <ta e="T1018" id="Seg_15605" s="T1014">он себе дом купил</ta>
            <ta e="T1024" id="Seg_15606" s="T1018">и женился стал жить</ta>
            <ta e="T1028" id="Seg_15607" s="T1024">я прошлый год была у них</ta>
            <ta e="T1031" id="Seg_15608" s="T1028">до того хорошо живут</ta>
            <ta e="T1037" id="Seg_15609" s="T1031">жена хорошая меня чаем меня напоила</ta>
            <ta e="T1041" id="Seg_15610" s="T1037">она мне говорила к нам ходи</ta>
            <ta e="T1045" id="Seg_15611" s="T1041">я все к ним заезжаю</ta>
            <ta e="T1048" id="Seg_15612" s="T1045">нонче домой (в деревню) поеду</ta>
            <ta e="T1051" id="Seg_15613" s="T1048">к ним опять зайду</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T6" id="Seg_15614" s="T3">[BrM:] INFER? [KuAI:] Variant: 'ära'.</ta>
            <ta e="T12" id="Seg_15615" s="T9">[BrM:] RES?</ta>
            <ta e="T30" id="Seg_15616" s="T19">[BrM:] INFER?</ta>
            <ta e="T48" id="Seg_15617" s="T42">[KuAI:] Variant: 'nät'. [BrM:] INFER?</ta>
            <ta e="T52" id="Seg_15618" s="T48">[KuAI:] Variant: 'täbɨstaɣə'.</ta>
            <ta e="T72" id="Seg_15619" s="T67">[KuAI:] Variant: 'äwtdə'.</ta>
            <ta e="T80" id="Seg_15620" s="T74">[KuAI:] Variant: 'agazlawtə'. [BrM:] DYA?</ta>
            <ta e="T123" id="Seg_15621" s="T115">[BrM:] 'melʼetaːdərat' changed to 'melʼe taːdərat'.</ta>
            <ta e="T137" id="Seg_15622" s="T131">[KuAI:] Variant: 'ik'. [BrM:] 'papalan' from Russian verb 'попасть' - 'get.into'? [BrM:] Futheron it's clear, that scobs shouldn't fall in water, not on feet.</ta>
            <ta e="T215" id="Seg_15623" s="T205">[KuAI:] Variant: 'atdəzʼe'.</ta>
            <ta e="T218" id="Seg_15624" s="T215">[BrM:] '' changed to ''.</ta>
            <ta e="T231" id="Seg_15625" s="T225">[KuAI:] Variant: 'qoǯɨrguzawtə'.</ta>
            <ta e="T251" id="Seg_15626" s="T239">[KuAI:] Variant: 'mannɨbɨlʼew'.</ta>
            <ta e="T253" id="Seg_15627" s="T251">[BrM:] INFER?</ta>
            <ta e="T326" id="Seg_15628" s="T318">[BrM:] Tentative analysis of 'qwäzʼet'. [BrM:] 'udlam də' changed to 'udlamdə'.</ta>
            <ta e="T341" id="Seg_15629" s="T331">[KuAI:] Variant: 'lakgolʼqə'.</ta>
            <ta e="T373" id="Seg_15630" s="T366">[BrM:] 'A to' changed to 'Ato'.</ta>
            <ta e="T420" id="Seg_15631" s="T414">[BrM:] Tentative analysis of 'turugulǯɨt', 'laqčelattə'.</ta>
            <ta e="T443" id="Seg_15632" s="T436">[BrM:] Tentative analysis of 'qaːrolʼdʼä'.</ta>
            <ta e="T462" id="Seg_15633" s="T452">[BrM:] 'qudɨgolamwes' changed to 'qudɨgolam wes'. [BrM:] INT.PFV or TR?</ta>
            <ta e="T470" id="Seg_15634" s="T462">[BrM:] INFER/IPFV?</ta>
            <ta e="T489" id="Seg_15635" s="T487">[BrM:] INFER?</ta>
            <ta e="T534" id="Seg_15636" s="T531">[KuAI:] Variant: 'turakɨn'.</ta>
            <ta e="T548" id="Seg_15637" s="T546">[KuAI:] Variant: 'qotdɨɣɨ'.</ta>
            <ta e="T566" id="Seg_15638" s="T559">[BrM:] Tentative analysis of 'qaːronʼen'.</ta>
            <ta e="T576" id="Seg_15639" s="T570">[KuAI:] Variant: 'lakčälǯɨt'. INT.PFV or TR?</ta>
            <ta e="T619" id="Seg_15640" s="T613">[KuAI:] Variant: 'qotduɣu'.</ta>
            <ta e="T675" id="Seg_15641" s="T668">[BrM:] INFER?</ta>
            <ta e="T690" id="Seg_15642" s="T687">[KuAI:] Variant: 'sernaq'.</ta>
            <ta e="T699" id="Seg_15643" s="T695">[BrM:] GEN? Syntax?</ta>
            <ta e="T705" id="Seg_15644" s="T699">[BrM:] 'pulə sejlatdɨze' changed to 'puləsejlatdɨze'.</ta>
            <ta e="T707" id="Seg_15645" s="T705">[BrM:] It's unclear, where optative interpretation comes from.</ta>
            <ta e="T714" id="Seg_15646" s="T709">[KuAI:] Variant: 'watdoɣut'. [BrM:] 'nasöːzɨtdət' changed to 'na söːzɨtdət'.</ta>
            <ta e="T720" id="Seg_15647" s="T714">[KuAI:] Variant: ''.</ta>
            <ta e="T762" id="Seg_15648" s="T754">[BrM:] NP syntax? GEN-ACC?</ta>
            <ta e="T773" id="Seg_15649" s="T766">[BrM:] INCH?</ta>
            <ta e="T779" id="Seg_15650" s="T773">[KuAI:] Variant: 'quʒannaɣ'.</ta>
            <ta e="T822" id="Seg_15651" s="T817">[KuAI:] Variant: 'palatiɣɨndo'.</ta>
            <ta e="T843" id="Seg_15652" s="T839">[KuAI:] Variant: 'tatkə'.</ta>
            <ta e="T850" id="Seg_15653" s="T843">[BrM:] INCH?</ta>
            <ta e="T879" id="Seg_15654" s="T868">[BrM:] INFER?</ta>
            <ta e="T918" id="Seg_15655" s="T914">[KuAI:] Variant: 'lakčälǯal'.</ta>
            <ta e="T933" id="Seg_15656" s="T926">[KuAI:] Variant: 'razbojnigam'.</ta>
            <ta e="T977" id="Seg_15657" s="T974">[BrM:] IPFV?</ta>
            <ta e="T1031" id="Seg_15658" s="T1028">[BrM:] 'Da tau' changed to 'Datau'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T215" id="Seg_15659" s="T205">не лодка, не обласок</ta>
            <ta e="T987" id="Seg_15660" s="T980">их много - трое; ′тʼӓжыт - только одного бросил</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T925" />
            <conversion-tli id="T926" />
            <conversion-tli id="T927" />
            <conversion-tli id="T928" />
            <conversion-tli id="T929" />
            <conversion-tli id="T930" />
            <conversion-tli id="T931" />
            <conversion-tli id="T932" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T935" />
            <conversion-tli id="T936" />
            <conversion-tli id="T937" />
            <conversion-tli id="T938" />
            <conversion-tli id="T939" />
            <conversion-tli id="T940" />
            <conversion-tli id="T941" />
            <conversion-tli id="T942" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T945" />
            <conversion-tli id="T946" />
            <conversion-tli id="T947" />
            <conversion-tli id="T948" />
            <conversion-tli id="T949" />
            <conversion-tli id="T950" />
            <conversion-tli id="T951" />
            <conversion-tli id="T952" />
            <conversion-tli id="T953" />
            <conversion-tli id="T954" />
            <conversion-tli id="T955" />
            <conversion-tli id="T956" />
            <conversion-tli id="T957" />
            <conversion-tli id="T958" />
            <conversion-tli id="T959" />
            <conversion-tli id="T960" />
            <conversion-tli id="T961" />
            <conversion-tli id="T962" />
            <conversion-tli id="T963" />
            <conversion-tli id="T964" />
            <conversion-tli id="T965" />
            <conversion-tli id="T966" />
            <conversion-tli id="T967" />
            <conversion-tli id="T968" />
            <conversion-tli id="T969" />
            <conversion-tli id="T970" />
            <conversion-tli id="T971" />
            <conversion-tli id="T972" />
            <conversion-tli id="T973" />
            <conversion-tli id="T974" />
            <conversion-tli id="T975" />
            <conversion-tli id="T976" />
            <conversion-tli id="T977" />
            <conversion-tli id="T978" />
            <conversion-tli id="T979" />
            <conversion-tli id="T980" />
            <conversion-tli id="T981" />
            <conversion-tli id="T982" />
            <conversion-tli id="T983" />
            <conversion-tli id="T984" />
            <conversion-tli id="T985" />
            <conversion-tli id="T986" />
            <conversion-tli id="T987" />
            <conversion-tli id="T988" />
            <conversion-tli id="T989" />
            <conversion-tli id="T990" />
            <conversion-tli id="T991" />
            <conversion-tli id="T992" />
            <conversion-tli id="T993" />
            <conversion-tli id="T994" />
            <conversion-tli id="T995" />
            <conversion-tli id="T996" />
            <conversion-tli id="T997" />
            <conversion-tli id="T998" />
            <conversion-tli id="T999" />
            <conversion-tli id="T1000" />
            <conversion-tli id="T1001" />
            <conversion-tli id="T1002" />
            <conversion-tli id="T1003" />
            <conversion-tli id="T1004" />
            <conversion-tli id="T1005" />
            <conversion-tli id="T1006" />
            <conversion-tli id="T1007" />
            <conversion-tli id="T1008" />
            <conversion-tli id="T1009" />
            <conversion-tli id="T1010" />
            <conversion-tli id="T1011" />
            <conversion-tli id="T1012" />
            <conversion-tli id="T1013" />
            <conversion-tli id="T1014" />
            <conversion-tli id="T1015" />
            <conversion-tli id="T1016" />
            <conversion-tli id="T1017" />
            <conversion-tli id="T1018" />
            <conversion-tli id="T1019" />
            <conversion-tli id="T1020" />
            <conversion-tli id="T1021" />
            <conversion-tli id="T1022" />
            <conversion-tli id="T1023" />
            <conversion-tli id="T1024" />
            <conversion-tli id="T1025" />
            <conversion-tli id="T1026" />
            <conversion-tli id="T1027" />
            <conversion-tli id="T1028" />
            <conversion-tli id="T1029" />
            <conversion-tli id="T1030" />
            <conversion-tli id="T1031" />
            <conversion-tli id="T1032" />
            <conversion-tli id="T1033" />
            <conversion-tli id="T1034" />
            <conversion-tli id="T1035" />
            <conversion-tli id="T1036" />
            <conversion-tli id="T1037" />
            <conversion-tli id="T1038" />
            <conversion-tli id="T1039" />
            <conversion-tli id="T1040" />
            <conversion-tli id="T1041" />
            <conversion-tli id="T1042" />
            <conversion-tli id="T1043" />
            <conversion-tli id="T1044" />
            <conversion-tli id="T1045" />
            <conversion-tli id="T1046" />
            <conversion-tli id="T1047" />
            <conversion-tli id="T1048" />
            <conversion-tli id="T1049" />
            <conversion-tli id="T1050" />
            <conversion-tli id="T1051" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
