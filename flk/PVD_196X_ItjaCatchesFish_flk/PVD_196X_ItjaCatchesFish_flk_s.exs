<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_196X_ItjaCatchesFish</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_196X_ItjaCatchesFish_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">138</ud-information>
            <ud-information attribute-name="# HIAT:w">87</ud-information>
            <ud-information attribute-name="# e">87</ud-information>
            <ud-information attribute-name="# HIAT:u">23</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T88" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Iːdʼä</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">agalatdɨze</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_9" n="HIAT:ip">(</nts>
                  <ts e="T4" id="Seg_11" n="HIAT:w" s="T3">tɨmnʼätdɨze</ts>
                  <nts id="Seg_12" n="HIAT:ip">)</nts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_15" n="HIAT:w" s="T4">ilɨkkus</ts>
                  <nts id="Seg_16" n="HIAT:ip">.</nts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_19" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_21" n="HIAT:w" s="T5">Iːdʼa</ts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_24" n="HIAT:w" s="T6">suːruttʼi</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_27" n="HIAT:w" s="T7">mɨɣɨndi</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_30" n="HIAT:w" s="T8">tüːa</ts>
                  <nts id="Seg_31" n="HIAT:ip">.</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_34" n="HIAT:u" s="T9">
                  <nts id="Seg_35" n="HIAT:ip">“</nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">Tan</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">ass</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">tunal</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">qajɣɨn</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">qwälɨm</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">qwatku</ts>
                  <nts id="Seg_53" n="HIAT:ip">,</nts>
                  <nts id="Seg_54" n="HIAT:ip">”</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_56" n="HIAT:ip">–</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">tɨmnʼät</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_62" n="HIAT:w" s="T16">tʼärɨn</ts>
                  <nts id="Seg_63" n="HIAT:ip">.</nts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_66" n="HIAT:u" s="T17">
                  <nts id="Seg_67" n="HIAT:ip">“</nts>
                  <ts e="T18" id="Seg_69" n="HIAT:w" s="T17">Madʼöɣɨn</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_72" n="HIAT:w" s="T18">toːnɨtdɨ</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_75" n="HIAT:w" s="T19">to</ts>
                  <nts id="Seg_76" n="HIAT:ip">,</nts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_79" n="HIAT:w" s="T20">madʼöɣə</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_82" n="HIAT:w" s="T21">toː</ts>
                  <nts id="Seg_83" n="HIAT:ip">.</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_86" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_88" n="HIAT:w" s="T22">Me</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_91" n="HIAT:w" s="T23">nɨtdɨn</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_94" n="HIAT:w" s="T24">ilɨzaj</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_97" n="HIAT:w" s="T25">qwälalʼlʼe</ts>
                  <nts id="Seg_98" n="HIAT:ip">.</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_101" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_103" n="HIAT:w" s="T26">Nɨtdɨn</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_106" n="HIAT:w" s="T27">ilɨn</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_109" n="HIAT:w" s="T28">qwäːlɨn</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_112" n="HIAT:w" s="T29">ästə</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_115" n="HIAT:w" s="T30">i</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_118" n="HIAT:w" s="T31">qwälɨn</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_121" n="HIAT:w" s="T32">äwdä</ts>
                  <nts id="Seg_122" n="HIAT:ip">.</nts>
                  <nts id="Seg_123" n="HIAT:ip">”</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_126" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_128" n="HIAT:w" s="T33">Iːdʼä</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_131" n="HIAT:w" s="T34">tʼärɨn</ts>
                  <nts id="Seg_132" n="HIAT:ip">:</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_134" n="HIAT:ip">“</nts>
                  <ts e="T36" id="Seg_136" n="HIAT:w" s="T35">Qardʼen</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_139" n="HIAT:w" s="T36">qwälɨlʼlʼe</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_142" n="HIAT:w" s="T37">qwaǯan</ts>
                  <nts id="Seg_143" n="HIAT:ip">.</nts>
                  <nts id="Seg_144" n="HIAT:ip">”</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_147" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_149" n="HIAT:w" s="T38">Iːdʼä</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_152" n="HIAT:w" s="T39">qarʼemɨɣɨn</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_155" n="HIAT:w" s="T40">qwannɨn</ts>
                  <nts id="Seg_156" n="HIAT:ip">.</nts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_159" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_161" n="HIAT:w" s="T41">Tʼüːwa</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_164" n="HIAT:w" s="T42">madʼöɣə</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_167" n="HIAT:w" s="T43">totdʼ</ts>
                  <nts id="Seg_168" n="HIAT:ip">.</nts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_171" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_173" n="HIAT:w" s="T44">Suxarlam</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_176" n="HIAT:w" s="T45">iːtdət</ts>
                  <nts id="Seg_177" n="HIAT:ip">.</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_180" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_182" n="HIAT:w" s="T46">Otdə</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_185" n="HIAT:w" s="T47">konnä</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_188" n="HIAT:w" s="T48">üːbən</ts>
                  <nts id="Seg_189" n="HIAT:ip">.</nts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_192" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_194" n="HIAT:w" s="T49">Konnä</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_197" n="HIAT:w" s="T50">matǯalba</ts>
                  <nts id="Seg_198" n="HIAT:ip">.</nts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_201" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_203" n="HIAT:w" s="T51">Ulɣom</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_206" n="HIAT:w" s="T52">wes</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_209" n="HIAT:w" s="T53">pitqalba</ts>
                  <nts id="Seg_210" n="HIAT:ip">.</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_213" n="HIAT:u" s="T54">
                  <nts id="Seg_214" n="HIAT:ip">“</nts>
                  <ts e="T55" id="Seg_216" n="HIAT:w" s="T54">Kutdär</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_219" n="HIAT:w" s="T55">qwälɨm</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_222" n="HIAT:w" s="T56">qwatku</ts>
                  <nts id="Seg_223" n="HIAT:ip">,</nts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_226" n="HIAT:w" s="T57">ass</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_229" n="HIAT:w" s="T58">tinnuwam</ts>
                  <nts id="Seg_230" n="HIAT:ip">,</nts>
                  <nts id="Seg_231" n="HIAT:ip">”</nts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_233" n="HIAT:ip">–</nts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_236" n="HIAT:w" s="T59">täp</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_239" n="HIAT:w" s="T60">tʼärɨppa</ts>
                  <nts id="Seg_240" n="HIAT:ip">.</nts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_243" n="HIAT:u" s="T61">
                  <nts id="Seg_244" n="HIAT:ip">“</nts>
                  <ts e="T62" id="Seg_246" n="HIAT:w" s="T61">Qwaǯan</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_249" n="HIAT:w" s="T62">ɛmnäɣən</ts>
                  <nts id="Seg_250" n="HIAT:ip">.</nts>
                  <nts id="Seg_251" n="HIAT:ip">”</nts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_254" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_256" n="HIAT:w" s="T63">Tüːuwan</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_259" n="HIAT:w" s="T64">ämnäɣən</ts>
                  <nts id="Seg_260" n="HIAT:ip">.</nts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_263" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_265" n="HIAT:w" s="T65">No</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_268" n="HIAT:w" s="T66">qwälam</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_271" n="HIAT:w" s="T67">nekɨgda</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_274" n="HIAT:w" s="T68">qwatku</ts>
                  <nts id="Seg_275" n="HIAT:ip">.</nts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_278" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_280" n="HIAT:w" s="T69">Te</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_283" n="HIAT:w" s="T70">ass</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_286" n="HIAT:w" s="T71">tunnalʼe</ts>
                  <nts id="Seg_287" n="HIAT:ip">.</nts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_290" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_292" n="HIAT:w" s="T72">Ämnät</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_295" n="HIAT:w" s="T73">tʼärɨn</ts>
                  <nts id="Seg_296" n="HIAT:ip">:</nts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_298" n="HIAT:ip">“</nts>
                  <ts e="T75" id="Seg_300" n="HIAT:w" s="T74">Qalana</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_303" n="HIAT:w" s="T75">ass</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_306" n="HIAT:w" s="T76">tunnou</ts>
                  <nts id="Seg_307" n="HIAT:ip">.</nts>
                  <nts id="Seg_308" n="HIAT:ip">”</nts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_311" n="HIAT:u" s="T77">
                  <nts id="Seg_312" n="HIAT:ip">“</nts>
                  <ts e="T78" id="Seg_314" n="HIAT:w" s="T77">Tunnuwalʼi</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_317" n="HIAT:w" s="T78">ass</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_320" n="HIAT:w" s="T79">ketkualʼe</ts>
                  <nts id="Seg_321" n="HIAT:ip">.</nts>
                  <nts id="Seg_322" n="HIAT:ip">”</nts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_325" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_327" n="HIAT:w" s="T80">Ämnämdə</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_330" n="HIAT:w" s="T81">tɨlʼe</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_333" n="HIAT:w" s="T82">taːdərɨt</ts>
                  <nts id="Seg_334" n="HIAT:ip">.</nts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_337" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_339" n="HIAT:w" s="T83">Konnä</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_342" n="HIAT:w" s="T84">mišalgut</ts>
                  <nts id="Seg_343" n="HIAT:ip">.</nts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_346" n="HIAT:u" s="T85">
                  <nts id="Seg_347" n="HIAT:ip">“</nts>
                  <ts e="T86" id="Seg_349" n="HIAT:w" s="T85">Tunal</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_352" n="HIAT:w" s="T86">də</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_355" n="HIAT:w" s="T87">kettɨ</ts>
                  <nts id="Seg_356" n="HIAT:ip">.</nts>
                  <nts id="Seg_357" n="HIAT:ip">”</nts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T88" id="Seg_359" n="sc" s="T1">
               <ts e="T2" id="Seg_361" n="e" s="T1">Iːdʼä </ts>
               <ts e="T3" id="Seg_363" n="e" s="T2">agalatdɨze </ts>
               <ts e="T4" id="Seg_365" n="e" s="T3">(tɨmnʼätdɨze) </ts>
               <ts e="T5" id="Seg_367" n="e" s="T4">ilɨkkus. </ts>
               <ts e="T6" id="Seg_369" n="e" s="T5">Iːdʼa </ts>
               <ts e="T7" id="Seg_371" n="e" s="T6">suːruttʼi </ts>
               <ts e="T8" id="Seg_373" n="e" s="T7">mɨɣɨndi </ts>
               <ts e="T9" id="Seg_375" n="e" s="T8">tüːa. </ts>
               <ts e="T10" id="Seg_377" n="e" s="T9">“Tan </ts>
               <ts e="T11" id="Seg_379" n="e" s="T10">ass </ts>
               <ts e="T12" id="Seg_381" n="e" s="T11">tunal </ts>
               <ts e="T13" id="Seg_383" n="e" s="T12">qajɣɨn </ts>
               <ts e="T14" id="Seg_385" n="e" s="T13">qwälɨm </ts>
               <ts e="T15" id="Seg_387" n="e" s="T14">qwatku,” – </ts>
               <ts e="T16" id="Seg_389" n="e" s="T15">tɨmnʼät </ts>
               <ts e="T17" id="Seg_391" n="e" s="T16">tʼärɨn. </ts>
               <ts e="T18" id="Seg_393" n="e" s="T17">“Madʼöɣɨn </ts>
               <ts e="T19" id="Seg_395" n="e" s="T18">toːnɨtdɨ </ts>
               <ts e="T20" id="Seg_397" n="e" s="T19">to, </ts>
               <ts e="T21" id="Seg_399" n="e" s="T20">madʼöɣə </ts>
               <ts e="T22" id="Seg_401" n="e" s="T21">toː. </ts>
               <ts e="T23" id="Seg_403" n="e" s="T22">Me </ts>
               <ts e="T24" id="Seg_405" n="e" s="T23">nɨtdɨn </ts>
               <ts e="T25" id="Seg_407" n="e" s="T24">ilɨzaj </ts>
               <ts e="T26" id="Seg_409" n="e" s="T25">qwälalʼlʼe. </ts>
               <ts e="T27" id="Seg_411" n="e" s="T26">Nɨtdɨn </ts>
               <ts e="T28" id="Seg_413" n="e" s="T27">ilɨn </ts>
               <ts e="T29" id="Seg_415" n="e" s="T28">qwäːlɨn </ts>
               <ts e="T30" id="Seg_417" n="e" s="T29">ästə </ts>
               <ts e="T31" id="Seg_419" n="e" s="T30">i </ts>
               <ts e="T32" id="Seg_421" n="e" s="T31">qwälɨn </ts>
               <ts e="T33" id="Seg_423" n="e" s="T32">äwdä.” </ts>
               <ts e="T34" id="Seg_425" n="e" s="T33">Iːdʼä </ts>
               <ts e="T35" id="Seg_427" n="e" s="T34">tʼärɨn: </ts>
               <ts e="T36" id="Seg_429" n="e" s="T35">“Qardʼen </ts>
               <ts e="T37" id="Seg_431" n="e" s="T36">qwälɨlʼlʼe </ts>
               <ts e="T38" id="Seg_433" n="e" s="T37">qwaǯan.” </ts>
               <ts e="T39" id="Seg_435" n="e" s="T38">Iːdʼä </ts>
               <ts e="T40" id="Seg_437" n="e" s="T39">qarʼemɨɣɨn </ts>
               <ts e="T41" id="Seg_439" n="e" s="T40">qwannɨn. </ts>
               <ts e="T42" id="Seg_441" n="e" s="T41">Tʼüːwa </ts>
               <ts e="T43" id="Seg_443" n="e" s="T42">madʼöɣə </ts>
               <ts e="T44" id="Seg_445" n="e" s="T43">totdʼ. </ts>
               <ts e="T45" id="Seg_447" n="e" s="T44">Suxarlam </ts>
               <ts e="T46" id="Seg_449" n="e" s="T45">iːtdət. </ts>
               <ts e="T47" id="Seg_451" n="e" s="T46">Otdə </ts>
               <ts e="T48" id="Seg_453" n="e" s="T47">konnä </ts>
               <ts e="T49" id="Seg_455" n="e" s="T48">üːbən. </ts>
               <ts e="T50" id="Seg_457" n="e" s="T49">Konnä </ts>
               <ts e="T51" id="Seg_459" n="e" s="T50">matǯalba. </ts>
               <ts e="T52" id="Seg_461" n="e" s="T51">Ulɣom </ts>
               <ts e="T53" id="Seg_463" n="e" s="T52">wes </ts>
               <ts e="T54" id="Seg_465" n="e" s="T53">pitqalba. </ts>
               <ts e="T55" id="Seg_467" n="e" s="T54">“Kutdär </ts>
               <ts e="T56" id="Seg_469" n="e" s="T55">qwälɨm </ts>
               <ts e="T57" id="Seg_471" n="e" s="T56">qwatku, </ts>
               <ts e="T58" id="Seg_473" n="e" s="T57">ass </ts>
               <ts e="T59" id="Seg_475" n="e" s="T58">tinnuwam,” – </ts>
               <ts e="T60" id="Seg_477" n="e" s="T59">täp </ts>
               <ts e="T61" id="Seg_479" n="e" s="T60">tʼärɨppa. </ts>
               <ts e="T62" id="Seg_481" n="e" s="T61">“Qwaǯan </ts>
               <ts e="T63" id="Seg_483" n="e" s="T62">ɛmnäɣən.” </ts>
               <ts e="T64" id="Seg_485" n="e" s="T63">Tüːuwan </ts>
               <ts e="T65" id="Seg_487" n="e" s="T64">ämnäɣən. </ts>
               <ts e="T66" id="Seg_489" n="e" s="T65">No </ts>
               <ts e="T67" id="Seg_491" n="e" s="T66">qwälam </ts>
               <ts e="T68" id="Seg_493" n="e" s="T67">nekɨgda </ts>
               <ts e="T69" id="Seg_495" n="e" s="T68">qwatku. </ts>
               <ts e="T70" id="Seg_497" n="e" s="T69">Te </ts>
               <ts e="T71" id="Seg_499" n="e" s="T70">ass </ts>
               <ts e="T72" id="Seg_501" n="e" s="T71">tunnalʼe. </ts>
               <ts e="T73" id="Seg_503" n="e" s="T72">Ämnät </ts>
               <ts e="T74" id="Seg_505" n="e" s="T73">tʼärɨn: </ts>
               <ts e="T75" id="Seg_507" n="e" s="T74">“Qalana </ts>
               <ts e="T76" id="Seg_509" n="e" s="T75">ass </ts>
               <ts e="T77" id="Seg_511" n="e" s="T76">tunnou.” </ts>
               <ts e="T78" id="Seg_513" n="e" s="T77">“Tunnuwalʼi </ts>
               <ts e="T79" id="Seg_515" n="e" s="T78">ass </ts>
               <ts e="T80" id="Seg_517" n="e" s="T79">ketkualʼe.” </ts>
               <ts e="T81" id="Seg_519" n="e" s="T80">Ämnämdə </ts>
               <ts e="T82" id="Seg_521" n="e" s="T81">tɨlʼe </ts>
               <ts e="T83" id="Seg_523" n="e" s="T82">taːdərɨt. </ts>
               <ts e="T84" id="Seg_525" n="e" s="T83">Konnä </ts>
               <ts e="T85" id="Seg_527" n="e" s="T84">mišalgut. </ts>
               <ts e="T86" id="Seg_529" n="e" s="T85">“Tunal </ts>
               <ts e="T87" id="Seg_531" n="e" s="T86">də </ts>
               <ts e="T88" id="Seg_533" n="e" s="T87">kettɨ.” </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_534" s="T1">PVD_196X_ItjaCatchesFish_flk.001 (001.001)</ta>
            <ta e="T9" id="Seg_535" s="T5">PVD_196X_ItjaCatchesFish_flk.002 (001.002)</ta>
            <ta e="T17" id="Seg_536" s="T9">PVD_196X_ItjaCatchesFish_flk.003 (001.003)</ta>
            <ta e="T22" id="Seg_537" s="T17">PVD_196X_ItjaCatchesFish_flk.004 (001.004)</ta>
            <ta e="T26" id="Seg_538" s="T22">PVD_196X_ItjaCatchesFish_flk.005 (001.005)</ta>
            <ta e="T33" id="Seg_539" s="T26">PVD_196X_ItjaCatchesFish_flk.006 (001.006)</ta>
            <ta e="T38" id="Seg_540" s="T33">PVD_196X_ItjaCatchesFish_flk.007 (001.007)</ta>
            <ta e="T41" id="Seg_541" s="T38">PVD_196X_ItjaCatchesFish_flk.008 (001.008)</ta>
            <ta e="T44" id="Seg_542" s="T41">PVD_196X_ItjaCatchesFish_flk.009 (001.009)</ta>
            <ta e="T46" id="Seg_543" s="T44">PVD_196X_ItjaCatchesFish_flk.010 (001.010)</ta>
            <ta e="T49" id="Seg_544" s="T46">PVD_196X_ItjaCatchesFish_flk.011 (001.011)</ta>
            <ta e="T51" id="Seg_545" s="T49">PVD_196X_ItjaCatchesFish_flk.012 (001.012)</ta>
            <ta e="T54" id="Seg_546" s="T51">PVD_196X_ItjaCatchesFish_flk.013 (001.013)</ta>
            <ta e="T61" id="Seg_547" s="T54">PVD_196X_ItjaCatchesFish_flk.014 (001.014)</ta>
            <ta e="T63" id="Seg_548" s="T61">PVD_196X_ItjaCatchesFish_flk.015 (001.015)</ta>
            <ta e="T65" id="Seg_549" s="T63">PVD_196X_ItjaCatchesFish_flk.016 (001.016)</ta>
            <ta e="T69" id="Seg_550" s="T65">PVD_196X_ItjaCatchesFish_flk.017 (001.017)</ta>
            <ta e="T72" id="Seg_551" s="T69">PVD_196X_ItjaCatchesFish_flk.018 (001.018)</ta>
            <ta e="T77" id="Seg_552" s="T72">PVD_196X_ItjaCatchesFish_flk.019 (001.019)</ta>
            <ta e="T80" id="Seg_553" s="T77">PVD_196X_ItjaCatchesFish_flk.020 (001.020)</ta>
            <ta e="T83" id="Seg_554" s="T80">PVD_196X_ItjaCatchesFish_flk.021 (001.021)</ta>
            <ta e="T85" id="Seg_555" s="T83">PVD_196X_ItjaCatchesFish_flk.022 (001.022)</ta>
            <ta e="T88" id="Seg_556" s="T85">PVD_196X_ItjaCatchesFish_flk.023 (001.023)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_557" s="T1">′Ӣдʼӓ ага′латдызе (тым′нʼӓтдызе) илык′кус.</ta>
            <ta e="T9" id="Seg_558" s="T5">′Ӣдʼа ′сӯрут′тʼи мы′ɣын′д̂и(ы) ′тӱ̄а.</ta>
            <ta e="T17" id="Seg_559" s="T9">тым′нʼӓт тʼӓ′рын. тан асс ту′нал kай′ɣын ′kwӓлым kwат′ку, тым′нʼӓт тʼӓ′рын.</ta>
            <ta e="T22" id="Seg_560" s="T17">ма′дʼӧɣын то̄ныт′ды то, ма′дʼӧɣъ то̄.</ta>
            <ta e="T26" id="Seg_561" s="T22">ме ныт′дын илы′зай ′kwӓлалʼлʼе.</ta>
            <ta e="T33" id="Seg_562" s="T26">ныт′дын и′лын ′kwӓ̄лын ӓс′тъ(ӓ) и kwӓлын ӓв′дӓ.</ta>
            <ta e="T38" id="Seg_563" s="T33">′Ӣдʼӓ тʼӓ′рын: kар′дʼен ′kwӓлылʼлʼе kwа(т)джан.</ta>
            <ta e="T41" id="Seg_564" s="T38">′Ӣдʼӓ kа′рʼемыɣын kwа′ннын.</ta>
            <ta e="T44" id="Seg_565" s="T41">′тʼӱ̄ва ма′дʼӧɣъ ′тотдь.</ta>
            <ta e="T46" id="Seg_566" s="T44">су′харлам ′ӣтдът.</ta>
            <ta e="T49" id="Seg_567" s="T46">отдъ ко′ннӓ ӱ̄бън.</ta>
            <ta e="T51" id="Seg_568" s="T49">ко′ннӓ ′матджалба.</ta>
            <ta e="T54" id="Seg_569" s="T51">ул′ɣом вес питkал′ба.</ta>
            <ta e="T61" id="Seg_570" s="T54">тӓп ‵тʼӓрып′па. кут′дӓр ′kwӓлым kwатку, асс ′тиннувам, тӓп ‵тʼӓрып′па.</ta>
            <ta e="T63" id="Seg_571" s="T61">kwа′джан эм′нӓɣън.</ta>
            <ta e="T65" id="Seg_572" s="T63">′тӱ̄уван ӓм′нӓɣън.</ta>
            <ta e="T69" id="Seg_573" s="T65">но ′kwӓлам ′некыгда kwат′ку.</ta>
            <ta e="T72" id="Seg_574" s="T69">те асс ту′нналʼе.</ta>
            <ta e="T77" id="Seg_575" s="T72">ӓм′нӓт тʼӓ′рын. kа′лана асс ту′нноу̹.</ta>
            <ta e="T80" id="Seg_576" s="T77">тунну′валʼи асс кетку′алʼе.</ta>
            <ta e="T83" id="Seg_577" s="T80">ӓмнӓмдъ ′тылʼе ′та̄дърыт.</ta>
            <ta e="T85" id="Seg_578" s="T83">ко′ннӓ ми′шалгут.</ta>
            <ta e="T88" id="Seg_579" s="T85">ту′нал дъ ке′тты.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_580" s="T1">Иːdʼä agalatdɨze (tɨmnʼätdɨze) ilɨkkus.</ta>
            <ta e="T9" id="Seg_581" s="T5">Иːdʼa suːruttʼi mɨɣɨnd̂i(ɨ) tüːa.</ta>
            <ta e="T17" id="Seg_582" s="T9">tan ass tunal qajɣɨn qwälɨm qwatku, tɨmnʼät tʼärɨn.</ta>
            <ta e="T22" id="Seg_583" s="T17">madʼöɣɨn toːnɨtdɨ to, madʼöɣə toː.</ta>
            <ta e="T26" id="Seg_584" s="T22">me nɨtdɨn ilɨzaj qwälalʼlʼe.</ta>
            <ta e="T33" id="Seg_585" s="T26">nɨtdɨn ilɨn qwäːlɨn ästə(ä) i qwälɨn äwdä.</ta>
            <ta e="T38" id="Seg_586" s="T33">Иːdʼä tʼärɨn: qardʼen qwälɨlʼlʼe qwa(t)ǯan.</ta>
            <ta e="T41" id="Seg_587" s="T38">Иːdʼä qarʼemɨɣɨn qwannɨn.</ta>
            <ta e="T44" id="Seg_588" s="T41">tʼüːwa madʼöɣə totdʼ.</ta>
            <ta e="T46" id="Seg_589" s="T44">suxarlam iːtdət.</ta>
            <ta e="T49" id="Seg_590" s="T46">otdə konnä üːbən.</ta>
            <ta e="T51" id="Seg_591" s="T49">konnä matǯalba.</ta>
            <ta e="T54" id="Seg_592" s="T51">ulɣom wes pitqalba.</ta>
            <ta e="T61" id="Seg_593" s="T54">kutdär qwälɨm qwatku, ass tinnuwam, täp tʼärɨppa.</ta>
            <ta e="T63" id="Seg_594" s="T61">qwaǯan ɛmnäɣən.</ta>
            <ta e="T65" id="Seg_595" s="T63">tüːuwan ämnäɣən.</ta>
            <ta e="T69" id="Seg_596" s="T65">no qwälam nekɨgda qwatku.</ta>
            <ta e="T72" id="Seg_597" s="T69">te ass tunnalʼe.</ta>
            <ta e="T77" id="Seg_598" s="T72">ämnät tʼärɨn. qalana ass tunnou̹.</ta>
            <ta e="T80" id="Seg_599" s="T77">tunnuwalʼi ass ketkualʼe.</ta>
            <ta e="T83" id="Seg_600" s="T80">ämnämdə tɨlʼe taːdərɨt.</ta>
            <ta e="T85" id="Seg_601" s="T83">konnä mišalgut.</ta>
            <ta e="T88" id="Seg_602" s="T85">tunal də kettɨ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_603" s="T1">Iːdʼä agalatdɨze (tɨmnʼätdɨze) ilɨkkus. </ta>
            <ta e="T9" id="Seg_604" s="T5">Iːdʼa suːruttʼi mɨɣɨndi tüːa. </ta>
            <ta e="T17" id="Seg_605" s="T9">“Tan ass tunal qajɣɨn qwälɨm qwatku,” – tɨmnʼät tʼärɨn. </ta>
            <ta e="T22" id="Seg_606" s="T17">“Madʼöɣɨn toːnɨtdɨ to, madʼöɣə toː. </ta>
            <ta e="T26" id="Seg_607" s="T22">Me nɨtdɨn ilɨzaj qwälalʼlʼe. </ta>
            <ta e="T33" id="Seg_608" s="T26">Nɨtdɨn ilɨn qwäːlɨn ästə i qwälɨn äwdä.” </ta>
            <ta e="T38" id="Seg_609" s="T33">Iːdʼä tʼärɨn: “Qardʼen qwälɨlʼlʼe qwaǯan.” </ta>
            <ta e="T41" id="Seg_610" s="T38">Iːdʼä qarʼemɨɣɨn qwannɨn. </ta>
            <ta e="T44" id="Seg_611" s="T41">Tʼüːwa madʼöɣə totdʼ. </ta>
            <ta e="T46" id="Seg_612" s="T44">Suxarlam iːtdət. </ta>
            <ta e="T49" id="Seg_613" s="T46">Otdə konnä üːbən. </ta>
            <ta e="T51" id="Seg_614" s="T49">Konnä matǯalba. </ta>
            <ta e="T54" id="Seg_615" s="T51">Ulɣom wes pitqalba. </ta>
            <ta e="T61" id="Seg_616" s="T54">“Kutdär qwälɨm qwatku, ass tinnuwam,” – täp tʼärɨppa. </ta>
            <ta e="T63" id="Seg_617" s="T61">“Qwaǯan ɛmnäɣən.” </ta>
            <ta e="T65" id="Seg_618" s="T63">Tüːuwan ämnäɣən. </ta>
            <ta e="T69" id="Seg_619" s="T65">No qwälam nekɨgda qwatku. </ta>
            <ta e="T72" id="Seg_620" s="T69">Te ass tunnalʼe. </ta>
            <ta e="T77" id="Seg_621" s="T72">Ämnät tʼärɨn: “Qalana ass tunnou.” </ta>
            <ta e="T80" id="Seg_622" s="T77">“Tunnuwalʼi ass ketkualʼe.” </ta>
            <ta e="T83" id="Seg_623" s="T80">Ämnämdə tɨlʼe taːdərɨt. </ta>
            <ta e="T85" id="Seg_624" s="T83">Konnä mišalgut. </ta>
            <ta e="T88" id="Seg_625" s="T85">“Tunal də kettɨ.” </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_626" s="T1">Iːdʼä</ta>
            <ta e="T3" id="Seg_627" s="T2">aga-la-tdɨ-ze</ta>
            <ta e="T4" id="Seg_628" s="T3">tɨmnʼä-tdɨ-ze</ta>
            <ta e="T5" id="Seg_629" s="T4">elɨ-kku-s</ta>
            <ta e="T6" id="Seg_630" s="T5">Iːdʼa</ta>
            <ta e="T7" id="Seg_631" s="T6">suːru-t-tʼi</ta>
            <ta e="T8" id="Seg_632" s="T7">mɨ-ɣɨndi</ta>
            <ta e="T9" id="Seg_633" s="T8">tüː-a</ta>
            <ta e="T10" id="Seg_634" s="T9">Tan</ta>
            <ta e="T11" id="Seg_635" s="T10">ass</ta>
            <ta e="T12" id="Seg_636" s="T11">tuna-l</ta>
            <ta e="T13" id="Seg_637" s="T12">qaj-ɣɨn</ta>
            <ta e="T14" id="Seg_638" s="T13">qwäl-ɨ-m</ta>
            <ta e="T15" id="Seg_639" s="T14">qwat-ku</ta>
            <ta e="T16" id="Seg_640" s="T15">tɨmnʼä-t</ta>
            <ta e="T17" id="Seg_641" s="T16">tʼärɨ-n</ta>
            <ta e="T18" id="Seg_642" s="T17">madʼ-ö-ɣɨn</ta>
            <ta e="T19" id="Seg_643" s="T18">toːnɨ-tdɨ</ta>
            <ta e="T20" id="Seg_644" s="T19">to</ta>
            <ta e="T21" id="Seg_645" s="T20">madʼ-ö-ɣə</ta>
            <ta e="T22" id="Seg_646" s="T21">toː</ta>
            <ta e="T23" id="Seg_647" s="T22">me</ta>
            <ta e="T24" id="Seg_648" s="T23">nɨtdɨ-n</ta>
            <ta e="T25" id="Seg_649" s="T24">ilɨ-za-j</ta>
            <ta e="T26" id="Seg_650" s="T25">qwäl-a-lʼ-lʼe</ta>
            <ta e="T27" id="Seg_651" s="T26">nɨtdɨ-n</ta>
            <ta e="T28" id="Seg_652" s="T27">ilɨ-n</ta>
            <ta e="T29" id="Seg_653" s="T28">qwäːl-ɨ-n</ta>
            <ta e="T30" id="Seg_654" s="T29">äs-tə</ta>
            <ta e="T31" id="Seg_655" s="T30">i</ta>
            <ta e="T32" id="Seg_656" s="T31">qwäl-ɨ-n</ta>
            <ta e="T33" id="Seg_657" s="T32">äw-dä</ta>
            <ta e="T34" id="Seg_658" s="T33">Iːdʼä</ta>
            <ta e="T35" id="Seg_659" s="T34">tʼärɨ-n</ta>
            <ta e="T36" id="Seg_660" s="T35">qar-dʼe-n</ta>
            <ta e="T37" id="Seg_661" s="T36">qwäl-ɨ-lʼ-lʼe</ta>
            <ta e="T38" id="Seg_662" s="T37">qwa-ǯa-n</ta>
            <ta e="T39" id="Seg_663" s="T38">Iːdʼä</ta>
            <ta e="T40" id="Seg_664" s="T39">qarʼe-mɨ-ɣɨn</ta>
            <ta e="T41" id="Seg_665" s="T40">qwan-nɨ-n</ta>
            <ta e="T42" id="Seg_666" s="T41">tʼüː-wa</ta>
            <ta e="T43" id="Seg_667" s="T42">madʼ-ö-ɣə</ta>
            <ta e="T44" id="Seg_668" s="T43">to-tdʼ</ta>
            <ta e="T45" id="Seg_669" s="T44">suxar-la-m</ta>
            <ta e="T46" id="Seg_670" s="T45">iː-tdə-t</ta>
            <ta e="T47" id="Seg_671" s="T46">otdə</ta>
            <ta e="T48" id="Seg_672" s="T47">konnä</ta>
            <ta e="T49" id="Seg_673" s="T48">üːbə-n</ta>
            <ta e="T50" id="Seg_674" s="T49">konnä</ta>
            <ta e="T51" id="Seg_675" s="T50">matǯa-l-ba</ta>
            <ta e="T52" id="Seg_676" s="T51">ulɣo-m</ta>
            <ta e="T53" id="Seg_677" s="T52">wes</ta>
            <ta e="T54" id="Seg_678" s="T53">pit-qal-ba</ta>
            <ta e="T55" id="Seg_679" s="T54">kutdär</ta>
            <ta e="T56" id="Seg_680" s="T55">qwäl-ɨ-m</ta>
            <ta e="T57" id="Seg_681" s="T56">qwat-ku</ta>
            <ta e="T58" id="Seg_682" s="T57">ass</ta>
            <ta e="T59" id="Seg_683" s="T58">tinnu-wa-m</ta>
            <ta e="T60" id="Seg_684" s="T59">täp</ta>
            <ta e="T61" id="Seg_685" s="T60">tʼärɨ-ppa</ta>
            <ta e="T62" id="Seg_686" s="T61">qwa-ǯa-n</ta>
            <ta e="T63" id="Seg_687" s="T62">ɛmnä-ɣən</ta>
            <ta e="T64" id="Seg_688" s="T63">tüːu-wa-n</ta>
            <ta e="T65" id="Seg_689" s="T64">ämnä-ɣən</ta>
            <ta e="T66" id="Seg_690" s="T65">no</ta>
            <ta e="T67" id="Seg_691" s="T66">qwäl-a-m</ta>
            <ta e="T69" id="Seg_692" s="T68">qwat-ku</ta>
            <ta e="T70" id="Seg_693" s="T69">te</ta>
            <ta e="T71" id="Seg_694" s="T70">ass</ta>
            <ta e="T72" id="Seg_695" s="T71">tunna-lʼe</ta>
            <ta e="T73" id="Seg_696" s="T72">ämnä-t</ta>
            <ta e="T74" id="Seg_697" s="T73">tʼärɨ-n</ta>
            <ta e="T75" id="Seg_698" s="T74">qala-na</ta>
            <ta e="T76" id="Seg_699" s="T75">ass</ta>
            <ta e="T77" id="Seg_700" s="T76">tunno-u</ta>
            <ta e="T78" id="Seg_701" s="T77">tunnu-wa-lʼi</ta>
            <ta e="T79" id="Seg_702" s="T78">ass</ta>
            <ta e="T80" id="Seg_703" s="T79">ket-ku-a-lʼe</ta>
            <ta e="T81" id="Seg_704" s="T80">ämnä-m-də</ta>
            <ta e="T82" id="Seg_705" s="T81">tɨ-lʼe</ta>
            <ta e="T83" id="Seg_706" s="T82">taːd-ə-r-ɨ-t</ta>
            <ta e="T84" id="Seg_707" s="T83">konnä</ta>
            <ta e="T85" id="Seg_708" s="T84">mišal-gu-t</ta>
            <ta e="T86" id="Seg_709" s="T85">tuna-l</ta>
            <ta e="T87" id="Seg_710" s="T86">də</ta>
            <ta e="T88" id="Seg_711" s="T87">ket-tɨ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_712" s="T1">Iːdʼä</ta>
            <ta e="T3" id="Seg_713" s="T2">agaː-la-ntɨ-se</ta>
            <ta e="T4" id="Seg_714" s="T3">tɨmnʼa-ntɨ-se</ta>
            <ta e="T5" id="Seg_715" s="T4">elɨ-ku-sɨ</ta>
            <ta e="T6" id="Seg_716" s="T5">Iːdʼä</ta>
            <ta e="T7" id="Seg_717" s="T6">suːrum-s-ptä</ta>
            <ta e="T8" id="Seg_718" s="T7">mɨ-qɨntɨ</ta>
            <ta e="T9" id="Seg_719" s="T8">tüː-nɨ</ta>
            <ta e="T10" id="Seg_720" s="T9">tan</ta>
            <ta e="T11" id="Seg_721" s="T10">asa</ta>
            <ta e="T12" id="Seg_722" s="T11">tonu-l</ta>
            <ta e="T13" id="Seg_723" s="T12">qaj-qɨn</ta>
            <ta e="T14" id="Seg_724" s="T13">qwɛl-ɨ-m</ta>
            <ta e="T15" id="Seg_725" s="T14">qwat-gu</ta>
            <ta e="T16" id="Seg_726" s="T15">tɨmnʼa-tə</ta>
            <ta e="T17" id="Seg_727" s="T16">tʼärɨ-n</ta>
            <ta e="T18" id="Seg_728" s="T17">madʼ-ɨ-qɨn</ta>
            <ta e="T19" id="Seg_729" s="T18">tonu-ntɨ</ta>
            <ta e="T20" id="Seg_730" s="T19">to</ta>
            <ta e="T21" id="Seg_731" s="T20">madʼ-ɨ-qij</ta>
            <ta e="T22" id="Seg_732" s="T21">to</ta>
            <ta e="T23" id="Seg_733" s="T22">me</ta>
            <ta e="T24" id="Seg_734" s="T23">*natʼe-n</ta>
            <ta e="T25" id="Seg_735" s="T24">elɨ-sɨ-j</ta>
            <ta e="T26" id="Seg_736" s="T25">qwɛl-ɨ-s-le</ta>
            <ta e="T27" id="Seg_737" s="T26">*natʼe-n</ta>
            <ta e="T28" id="Seg_738" s="T27">elɨ-n</ta>
            <ta e="T29" id="Seg_739" s="T28">qwɛl-ɨ-n</ta>
            <ta e="T30" id="Seg_740" s="T29">aze-tə</ta>
            <ta e="T31" id="Seg_741" s="T30">i</ta>
            <ta e="T32" id="Seg_742" s="T31">qwɛl-ɨ-n</ta>
            <ta e="T33" id="Seg_743" s="T32">awa-tə</ta>
            <ta e="T34" id="Seg_744" s="T33">Iːdʼä</ta>
            <ta e="T35" id="Seg_745" s="T34">tʼärɨ-n</ta>
            <ta e="T36" id="Seg_746" s="T35">qare-dʼel-n</ta>
            <ta e="T37" id="Seg_747" s="T36">qwɛl-ɨ-s-le</ta>
            <ta e="T38" id="Seg_748" s="T37">qwan-enǯɨ-ŋ</ta>
            <ta e="T39" id="Seg_749" s="T38">Iːdʼä</ta>
            <ta e="T40" id="Seg_750" s="T39">qare-mɨ-qɨn</ta>
            <ta e="T41" id="Seg_751" s="T40">qwan-nɨ-n</ta>
            <ta e="T42" id="Seg_752" s="T41">tüː-nɨ</ta>
            <ta e="T43" id="Seg_753" s="T42">madʼ-ɨ-qij</ta>
            <ta e="T44" id="Seg_754" s="T43">to-ntə</ta>
            <ta e="T45" id="Seg_755" s="T44">suhar-la-m</ta>
            <ta e="T46" id="Seg_756" s="T45">iː-ntɨ-t</ta>
            <ta e="T47" id="Seg_757" s="T46">ondə</ta>
            <ta e="T48" id="Seg_758" s="T47">qonnä</ta>
            <ta e="T49" id="Seg_759" s="T48">übɨ-n</ta>
            <ta e="T50" id="Seg_760" s="T49">qonnä</ta>
            <ta e="T51" id="Seg_761" s="T50">manǯu-l-mbɨ</ta>
            <ta e="T52" id="Seg_762" s="T51">ulgo-m</ta>
            <ta e="T53" id="Seg_763" s="T52">wesʼ</ta>
            <ta e="T54" id="Seg_764" s="T53">pit-qɨl-mbɨ</ta>
            <ta e="T55" id="Seg_765" s="T54">qundar</ta>
            <ta e="T56" id="Seg_766" s="T55">qwɛl-ɨ-m</ta>
            <ta e="T57" id="Seg_767" s="T56">qwat-gu</ta>
            <ta e="T58" id="Seg_768" s="T57">asa</ta>
            <ta e="T59" id="Seg_769" s="T58">tonu-nɨ-w</ta>
            <ta e="T60" id="Seg_770" s="T59">täp</ta>
            <ta e="T61" id="Seg_771" s="T60">tʼärɨ-mbɨ</ta>
            <ta e="T62" id="Seg_772" s="T61">qwan-enǯɨ-ŋ</ta>
            <ta e="T63" id="Seg_773" s="T62">ämnä-qɨn</ta>
            <ta e="T64" id="Seg_774" s="T63">tüː-nɨ-n</ta>
            <ta e="T65" id="Seg_775" s="T64">ämnä-qɨn</ta>
            <ta e="T66" id="Seg_776" s="T65">nu</ta>
            <ta e="T67" id="Seg_777" s="T66">qwɛl-ɨ-m</ta>
            <ta e="T69" id="Seg_778" s="T68">qwat-gu</ta>
            <ta e="T70" id="Seg_779" s="T69">te</ta>
            <ta e="T71" id="Seg_780" s="T70">asa</ta>
            <ta e="T72" id="Seg_781" s="T71">tonu-lɨn</ta>
            <ta e="T73" id="Seg_782" s="T72">ämnä-tə</ta>
            <ta e="T74" id="Seg_783" s="T73">tʼärɨ-n</ta>
            <ta e="T75" id="Seg_784" s="T74">qala-näj</ta>
            <ta e="T76" id="Seg_785" s="T75">asa</ta>
            <ta e="T77" id="Seg_786" s="T76">tonu-w</ta>
            <ta e="T78" id="Seg_787" s="T77">tonu-nɨ-lɨn</ta>
            <ta e="T79" id="Seg_788" s="T78">asa</ta>
            <ta e="T80" id="Seg_789" s="T79">ket-ku-nɨ-lɨn</ta>
            <ta e="T81" id="Seg_790" s="T80">ämnä-m-tə</ta>
            <ta e="T82" id="Seg_791" s="T81">tɨ-le</ta>
            <ta e="T83" id="Seg_792" s="T82">tat-ɨ-r-ɨ-t</ta>
            <ta e="T84" id="Seg_793" s="T83">qonnä</ta>
            <ta e="T85" id="Seg_794" s="T84">miššal-ku-t</ta>
            <ta e="T86" id="Seg_795" s="T85">tonu-l</ta>
            <ta e="T87" id="Seg_796" s="T86">də</ta>
            <ta e="T88" id="Seg_797" s="T87">ket-etɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_798" s="T1">Itja.[NOM]</ta>
            <ta e="T3" id="Seg_799" s="T2">brother-PL-OBL.3SG-COM</ta>
            <ta e="T4" id="Seg_800" s="T3">brother-OBL.3SG-COM</ta>
            <ta e="T5" id="Seg_801" s="T4">live-HAB-PST.[3SG.S]</ta>
            <ta e="T6" id="Seg_802" s="T5">Itja.[NOM]</ta>
            <ta e="T7" id="Seg_803" s="T6">wild.animal-CAP-ACTN.[NOM]</ta>
            <ta e="T8" id="Seg_804" s="T7">something-EL.3SG</ta>
            <ta e="T9" id="Seg_805" s="T8">come-CO.[3SG.S]</ta>
            <ta e="T10" id="Seg_806" s="T9">you.SG.NOM</ta>
            <ta e="T11" id="Seg_807" s="T10">NEG</ta>
            <ta e="T12" id="Seg_808" s="T11">know-2SG.O</ta>
            <ta e="T13" id="Seg_809" s="T12">what-LOC</ta>
            <ta e="T14" id="Seg_810" s="T13">fish-EP-ACC</ta>
            <ta e="T15" id="Seg_811" s="T14">manage.to.get-INF</ta>
            <ta e="T16" id="Seg_812" s="T15">brother.[NOM]-3SG</ta>
            <ta e="T17" id="Seg_813" s="T16">say-3SG.S</ta>
            <ta e="T18" id="Seg_814" s="T17">taiga-EP-LOC</ta>
            <ta e="T19" id="Seg_815" s="T18">know-PTCP.PRS.[NOM]</ta>
            <ta e="T20" id="Seg_816" s="T19">lake.[NOM]</ta>
            <ta e="T21" id="Seg_817" s="T20">taiga-EP-ADJZ</ta>
            <ta e="T22" id="Seg_818" s="T21">lake.[NOM]</ta>
            <ta e="T23" id="Seg_819" s="T22">we.[NOM]</ta>
            <ta e="T24" id="Seg_820" s="T23">there-ADV.LOC</ta>
            <ta e="T25" id="Seg_821" s="T24">live-PST-1DU</ta>
            <ta e="T26" id="Seg_822" s="T25">fish-EP-CAP-CVB</ta>
            <ta e="T27" id="Seg_823" s="T26">there-ADV.LOC</ta>
            <ta e="T28" id="Seg_824" s="T27">live-3SG.S</ta>
            <ta e="T29" id="Seg_825" s="T28">fish-EP-GEN</ta>
            <ta e="T30" id="Seg_826" s="T29">father.[NOM]-3SG</ta>
            <ta e="T31" id="Seg_827" s="T30">and</ta>
            <ta e="T32" id="Seg_828" s="T31">fish-EP-GEN</ta>
            <ta e="T33" id="Seg_829" s="T32">mother.[NOM]-3SG</ta>
            <ta e="T34" id="Seg_830" s="T33">Itja.[NOM]</ta>
            <ta e="T35" id="Seg_831" s="T34">say-3SG.S</ta>
            <ta e="T36" id="Seg_832" s="T35">morning-day-ADV.LOC</ta>
            <ta e="T37" id="Seg_833" s="T36">fish-EP-CAP-CVB</ta>
            <ta e="T38" id="Seg_834" s="T37">leave-FUT-1SG.S</ta>
            <ta e="T39" id="Seg_835" s="T38">Itja.[NOM]</ta>
            <ta e="T40" id="Seg_836" s="T39">morning-something-LOC</ta>
            <ta e="T41" id="Seg_837" s="T40">leave-CO-3SG.S</ta>
            <ta e="T42" id="Seg_838" s="T41">come-CO.[3SG.S]</ta>
            <ta e="T43" id="Seg_839" s="T42">taiga-EP-ADJZ</ta>
            <ta e="T44" id="Seg_840" s="T43">lake-ILL</ta>
            <ta e="T45" id="Seg_841" s="T44">rusk-PL-ACC</ta>
            <ta e="T46" id="Seg_842" s="T45">take-INFER-3SG.O</ta>
            <ta e="T47" id="Seg_843" s="T46">oneself.3SG.[NOM]</ta>
            <ta e="T48" id="Seg_844" s="T47">up.the.shore</ta>
            <ta e="T49" id="Seg_845" s="T48">set.off-3SG.O</ta>
            <ta e="T50" id="Seg_846" s="T49">up.the.shore</ta>
            <ta e="T51" id="Seg_847" s="T50">look.at-INCH-DUR.[3SG.S]</ta>
            <ta e="T52" id="Seg_848" s="T51">ice-ACC</ta>
            <ta e="T53" id="Seg_849" s="T52">all</ta>
            <ta e="T54" id="Seg_850" s="T53">crumple-DRV-PST.NAR.[3SG.S]</ta>
            <ta e="T55" id="Seg_851" s="T54">how</ta>
            <ta e="T56" id="Seg_852" s="T55">fish-EP-ACC</ta>
            <ta e="T57" id="Seg_853" s="T56">manage.to.get-INF</ta>
            <ta e="T58" id="Seg_854" s="T57">NEG</ta>
            <ta e="T59" id="Seg_855" s="T58">know-CO-1SG.O</ta>
            <ta e="T60" id="Seg_856" s="T59">(s)he.[NOM]</ta>
            <ta e="T61" id="Seg_857" s="T60">say-PST.NAR.[3SG.S]</ta>
            <ta e="T62" id="Seg_858" s="T61">leave-FUT-1SG.S</ta>
            <ta e="T63" id="Seg_859" s="T62">son_in_law-LOC</ta>
            <ta e="T64" id="Seg_860" s="T63">come-CO-3SG.S</ta>
            <ta e="T65" id="Seg_861" s="T64">son_in_law-LOC</ta>
            <ta e="T66" id="Seg_862" s="T65">now</ta>
            <ta e="T67" id="Seg_863" s="T66">fish-EP-ACC</ta>
            <ta e="T69" id="Seg_864" s="T68">manage.to.get-INF</ta>
            <ta e="T70" id="Seg_865" s="T69">you.PL.NOM</ta>
            <ta e="T71" id="Seg_866" s="T70">NEG</ta>
            <ta e="T72" id="Seg_867" s="T71">know-2PL</ta>
            <ta e="T73" id="Seg_868" s="T72">son_in_law.[NOM]-3SG</ta>
            <ta e="T74" id="Seg_869" s="T73">say-3SG.S</ta>
            <ta e="T75" id="Seg_870" s="T74">supposedly-EMPH</ta>
            <ta e="T76" id="Seg_871" s="T75">NEG</ta>
            <ta e="T77" id="Seg_872" s="T76">know-1SG.O</ta>
            <ta e="T78" id="Seg_873" s="T77">know-CO-2PL</ta>
            <ta e="T79" id="Seg_874" s="T78">NEG</ta>
            <ta e="T80" id="Seg_875" s="T79">say-HAB-CO-2PL</ta>
            <ta e="T81" id="Seg_876" s="T80">son_in_law-ACC-3SG</ta>
            <ta e="T82" id="Seg_877" s="T81">swear.at-CVB</ta>
            <ta e="T83" id="Seg_878" s="T82">bring-EP-FRQ-EP-3SG.O</ta>
            <ta e="T84" id="Seg_879" s="T83">up.the.shore</ta>
            <ta e="T85" id="Seg_880" s="T84">pull.out-HAB-3SG.O</ta>
            <ta e="T86" id="Seg_881" s="T85">know-2SG.O</ta>
            <ta e="T87" id="Seg_882" s="T86">then</ta>
            <ta e="T88" id="Seg_883" s="T87">say-IMP.2SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_884" s="T1">Итя.[NOM]</ta>
            <ta e="T3" id="Seg_885" s="T2">брат-PL-OBL.3SG-COM</ta>
            <ta e="T4" id="Seg_886" s="T3">брат-OBL.3SG-COM</ta>
            <ta e="T5" id="Seg_887" s="T4">жить-HAB-PST.[3SG.S]</ta>
            <ta e="T6" id="Seg_888" s="T5">Итя.[NOM]</ta>
            <ta e="T7" id="Seg_889" s="T6">зверь-CAP-ACTN.[NOM]</ta>
            <ta e="T8" id="Seg_890" s="T7">нечто-EL.3SG</ta>
            <ta e="T9" id="Seg_891" s="T8">прийти-CO.[3SG.S]</ta>
            <ta e="T10" id="Seg_892" s="T9">ты.NOM</ta>
            <ta e="T11" id="Seg_893" s="T10">NEG</ta>
            <ta e="T12" id="Seg_894" s="T11">знать-2SG.O</ta>
            <ta e="T13" id="Seg_895" s="T12">что-LOC</ta>
            <ta e="T14" id="Seg_896" s="T13">рыба-EP-ACC</ta>
            <ta e="T15" id="Seg_897" s="T14">добыть-INF</ta>
            <ta e="T16" id="Seg_898" s="T15">брат.[NOM]-3SG</ta>
            <ta e="T17" id="Seg_899" s="T16">сказать-3SG.S</ta>
            <ta e="T18" id="Seg_900" s="T17">тайга-EP-LOC</ta>
            <ta e="T19" id="Seg_901" s="T18">знать-PTCP.PRS.[NOM]</ta>
            <ta e="T20" id="Seg_902" s="T19">озеро.[NOM]</ta>
            <ta e="T21" id="Seg_903" s="T20">тайга-EP-ADJZ</ta>
            <ta e="T22" id="Seg_904" s="T21">озеро.[NOM]</ta>
            <ta e="T23" id="Seg_905" s="T22">мы.[NOM]</ta>
            <ta e="T24" id="Seg_906" s="T23">туда-ADV.LOC</ta>
            <ta e="T25" id="Seg_907" s="T24">жить-PST-1DU</ta>
            <ta e="T26" id="Seg_908" s="T25">рыба-EP-CAP-CVB</ta>
            <ta e="T27" id="Seg_909" s="T26">туда-ADV.LOC</ta>
            <ta e="T28" id="Seg_910" s="T27">жить-3SG.S</ta>
            <ta e="T29" id="Seg_911" s="T28">рыба-EP-GEN</ta>
            <ta e="T30" id="Seg_912" s="T29">отец.[NOM]-3SG</ta>
            <ta e="T31" id="Seg_913" s="T30">и</ta>
            <ta e="T32" id="Seg_914" s="T31">рыба-EP-GEN</ta>
            <ta e="T33" id="Seg_915" s="T32">мать.[NOM]-3SG</ta>
            <ta e="T34" id="Seg_916" s="T33">Итя.[NOM]</ta>
            <ta e="T35" id="Seg_917" s="T34">сказать-3SG.S</ta>
            <ta e="T36" id="Seg_918" s="T35">утро-день-ADV.LOC</ta>
            <ta e="T37" id="Seg_919" s="T36">рыба-EP-CAP-CVB</ta>
            <ta e="T38" id="Seg_920" s="T37">отправиться-FUT-1SG.S</ta>
            <ta e="T39" id="Seg_921" s="T38">Итя.[NOM]</ta>
            <ta e="T40" id="Seg_922" s="T39">утро-нечто-LOC</ta>
            <ta e="T41" id="Seg_923" s="T40">отправиться-CO-3SG.S</ta>
            <ta e="T42" id="Seg_924" s="T41">прийти-CO.[3SG.S]</ta>
            <ta e="T43" id="Seg_925" s="T42">тайга-EP-ADJZ</ta>
            <ta e="T44" id="Seg_926" s="T43">озеро-ILL</ta>
            <ta e="T45" id="Seg_927" s="T44">сухарь-PL-ACC</ta>
            <ta e="T46" id="Seg_928" s="T45">взять-INFER-3SG.O</ta>
            <ta e="T47" id="Seg_929" s="T46">сам.3SG.[NOM]</ta>
            <ta e="T48" id="Seg_930" s="T47">на.берег</ta>
            <ta e="T49" id="Seg_931" s="T48">отправиться-3SG.O</ta>
            <ta e="T50" id="Seg_932" s="T49">на.берег</ta>
            <ta e="T51" id="Seg_933" s="T50">посмотреть-INCH-DUR.[3SG.S]</ta>
            <ta e="T52" id="Seg_934" s="T51">лёд-ACC</ta>
            <ta e="T53" id="Seg_935" s="T52">весь</ta>
            <ta e="T54" id="Seg_936" s="T53">измять-DRV-PST.NAR.[3SG.S]</ta>
            <ta e="T55" id="Seg_937" s="T54">как</ta>
            <ta e="T56" id="Seg_938" s="T55">рыба-EP-ACC</ta>
            <ta e="T57" id="Seg_939" s="T56">добыть-INF</ta>
            <ta e="T58" id="Seg_940" s="T57">NEG</ta>
            <ta e="T59" id="Seg_941" s="T58">знать-CO-1SG.O</ta>
            <ta e="T60" id="Seg_942" s="T59">он(а).[NOM]</ta>
            <ta e="T61" id="Seg_943" s="T60">сказать-PST.NAR.[3SG.S]</ta>
            <ta e="T62" id="Seg_944" s="T61">отправиться-FUT-1SG.S</ta>
            <ta e="T63" id="Seg_945" s="T62">зять-LOC</ta>
            <ta e="T64" id="Seg_946" s="T63">прийти-CO-3SG.S</ta>
            <ta e="T65" id="Seg_947" s="T64">зять-LOC</ta>
            <ta e="T66" id="Seg_948" s="T65">ну</ta>
            <ta e="T67" id="Seg_949" s="T66">рыба-EP-ACC</ta>
            <ta e="T69" id="Seg_950" s="T68">добыть-INF</ta>
            <ta e="T70" id="Seg_951" s="T69">вы.PL.NOM</ta>
            <ta e="T71" id="Seg_952" s="T70">NEG</ta>
            <ta e="T72" id="Seg_953" s="T71">знать-2PL</ta>
            <ta e="T73" id="Seg_954" s="T72">зять.[NOM]-3SG</ta>
            <ta e="T74" id="Seg_955" s="T73">сказать-3SG.S</ta>
            <ta e="T75" id="Seg_956" s="T74">кажется-EMPH</ta>
            <ta e="T76" id="Seg_957" s="T75">NEG</ta>
            <ta e="T77" id="Seg_958" s="T76">знать-1SG.O</ta>
            <ta e="T78" id="Seg_959" s="T77">знать-CO-2PL</ta>
            <ta e="T79" id="Seg_960" s="T78">NEG</ta>
            <ta e="T80" id="Seg_961" s="T79">сказать-HAB-CO-2PL</ta>
            <ta e="T81" id="Seg_962" s="T80">зять-ACC-3SG</ta>
            <ta e="T82" id="Seg_963" s="T81">поругать-CVB</ta>
            <ta e="T83" id="Seg_964" s="T82">принести-EP-FRQ-EP-3SG.O</ta>
            <ta e="T84" id="Seg_965" s="T83">на.берег</ta>
            <ta e="T85" id="Seg_966" s="T84">вытащить-HAB-3SG.O</ta>
            <ta e="T86" id="Seg_967" s="T85">знать-2SG.O</ta>
            <ta e="T87" id="Seg_968" s="T86">то</ta>
            <ta e="T88" id="Seg_969" s="T87">сказать-IMP.2SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_970" s="T1">nprop.[n:case]</ta>
            <ta e="T3" id="Seg_971" s="T2">n-n:num-n:obl.poss-n:case</ta>
            <ta e="T4" id="Seg_972" s="T3">n-n:obl.poss-n:case</ta>
            <ta e="T5" id="Seg_973" s="T4">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T6" id="Seg_974" s="T5">nprop.[n:case]</ta>
            <ta e="T7" id="Seg_975" s="T6">n-n&gt;v-v&gt;n.[n:case]</ta>
            <ta e="T8" id="Seg_976" s="T7">n-n:case.poss</ta>
            <ta e="T9" id="Seg_977" s="T8">v-v:ins.[v:pn]</ta>
            <ta e="T10" id="Seg_978" s="T9">pers</ta>
            <ta e="T11" id="Seg_979" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_980" s="T11">v-v:pn</ta>
            <ta e="T13" id="Seg_981" s="T12">interrog-n:case</ta>
            <ta e="T14" id="Seg_982" s="T13">n-n:ins-n:case</ta>
            <ta e="T15" id="Seg_983" s="T14">v-v:inf</ta>
            <ta e="T16" id="Seg_984" s="T15">n.[n:case]-n:poss</ta>
            <ta e="T17" id="Seg_985" s="T16">v-v:pn</ta>
            <ta e="T18" id="Seg_986" s="T17">n-n:ins-n:case</ta>
            <ta e="T19" id="Seg_987" s="T18">v-v&gt;ptcp.[n:case]</ta>
            <ta e="T20" id="Seg_988" s="T19">n.[n:case]</ta>
            <ta e="T21" id="Seg_989" s="T20">n-n:ins-n&gt;adj</ta>
            <ta e="T22" id="Seg_990" s="T21">n.[n:case]</ta>
            <ta e="T23" id="Seg_991" s="T22">pers.[n:case]</ta>
            <ta e="T24" id="Seg_992" s="T23">adv-adv:case</ta>
            <ta e="T25" id="Seg_993" s="T24">v-v:tense-v:pn</ta>
            <ta e="T26" id="Seg_994" s="T25">n-n:ins-n&gt;v-v&gt;adv</ta>
            <ta e="T27" id="Seg_995" s="T26">adv-adv:case</ta>
            <ta e="T28" id="Seg_996" s="T27">v-v:pn</ta>
            <ta e="T29" id="Seg_997" s="T28">n-n:ins-n:case</ta>
            <ta e="T30" id="Seg_998" s="T29">n.[n:case]-n:poss</ta>
            <ta e="T31" id="Seg_999" s="T30">conj</ta>
            <ta e="T32" id="Seg_1000" s="T31">n-n:ins-n:case</ta>
            <ta e="T33" id="Seg_1001" s="T32">n.[n:case]-n:poss</ta>
            <ta e="T34" id="Seg_1002" s="T33">nprop.[n:case]</ta>
            <ta e="T35" id="Seg_1003" s="T34">v-v:pn</ta>
            <ta e="T36" id="Seg_1004" s="T35">n-n-adv:case</ta>
            <ta e="T37" id="Seg_1005" s="T36">n-n:ins-n&gt;v-v&gt;adv</ta>
            <ta e="T38" id="Seg_1006" s="T37">v-v:tense-v:pn</ta>
            <ta e="T39" id="Seg_1007" s="T38">nprop.[n:case]</ta>
            <ta e="T40" id="Seg_1008" s="T39">n-n-n:case</ta>
            <ta e="T41" id="Seg_1009" s="T40">v-v:ins-v:pn</ta>
            <ta e="T42" id="Seg_1010" s="T41">v-v:ins.[v:pn]</ta>
            <ta e="T43" id="Seg_1011" s="T42">n-n:ins-n&gt;adj</ta>
            <ta e="T44" id="Seg_1012" s="T43">n-n:case</ta>
            <ta e="T45" id="Seg_1013" s="T44">n-n:num-n:case</ta>
            <ta e="T46" id="Seg_1014" s="T45">v-v:mood-v:pn</ta>
            <ta e="T47" id="Seg_1015" s="T46">emphpro.[n:case]</ta>
            <ta e="T48" id="Seg_1016" s="T47">adv</ta>
            <ta e="T49" id="Seg_1017" s="T48">v-v:pn</ta>
            <ta e="T50" id="Seg_1018" s="T49">adv</ta>
            <ta e="T51" id="Seg_1019" s="T50">v-v&gt;v-v&gt;v.[v:pn]</ta>
            <ta e="T52" id="Seg_1020" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_1021" s="T52">quant</ta>
            <ta e="T54" id="Seg_1022" s="T53">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T55" id="Seg_1023" s="T54">conj</ta>
            <ta e="T56" id="Seg_1024" s="T55">n-n:ins-n:case</ta>
            <ta e="T57" id="Seg_1025" s="T56">v-v:inf</ta>
            <ta e="T58" id="Seg_1026" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_1027" s="T58">v-v:ins-v:pn</ta>
            <ta e="T60" id="Seg_1028" s="T59">pers.[n:case]</ta>
            <ta e="T61" id="Seg_1029" s="T60">v-v:tense.[v:pn]</ta>
            <ta e="T62" id="Seg_1030" s="T61">v-v:tense-v:pn</ta>
            <ta e="T63" id="Seg_1031" s="T62">n-n:case</ta>
            <ta e="T64" id="Seg_1032" s="T63">v-v:ins-v:pn</ta>
            <ta e="T65" id="Seg_1033" s="T64">n-n:case</ta>
            <ta e="T66" id="Seg_1034" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_1035" s="T66">n-n:ins-n:case</ta>
            <ta e="T69" id="Seg_1036" s="T68">v-v:inf</ta>
            <ta e="T70" id="Seg_1037" s="T69">pers</ta>
            <ta e="T71" id="Seg_1038" s="T70">ptcl</ta>
            <ta e="T72" id="Seg_1039" s="T71">v-v:pn</ta>
            <ta e="T73" id="Seg_1040" s="T72">n.[n:case]-n:poss</ta>
            <ta e="T74" id="Seg_1041" s="T73">v-v:pn</ta>
            <ta e="T75" id="Seg_1042" s="T74">ptcl-clit</ta>
            <ta e="T76" id="Seg_1043" s="T75">ptcl</ta>
            <ta e="T77" id="Seg_1044" s="T76">v-v:pn</ta>
            <ta e="T78" id="Seg_1045" s="T77">v-v:ins-v:pn</ta>
            <ta e="T79" id="Seg_1046" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_1047" s="T79">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T81" id="Seg_1048" s="T80">n-n:case-n:poss</ta>
            <ta e="T82" id="Seg_1049" s="T81">v-v&gt;adv</ta>
            <ta e="T83" id="Seg_1050" s="T82">v-v:ins-v&gt;v-n:ins-v:pn</ta>
            <ta e="T84" id="Seg_1051" s="T83">adv</ta>
            <ta e="T85" id="Seg_1052" s="T84">v-v&gt;v-v:pn</ta>
            <ta e="T86" id="Seg_1053" s="T85">v-v:pn</ta>
            <ta e="T87" id="Seg_1054" s="T86">conj</ta>
            <ta e="T88" id="Seg_1055" s="T87">v-v:mood.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1056" s="T1">nprop</ta>
            <ta e="T3" id="Seg_1057" s="T2">n</ta>
            <ta e="T4" id="Seg_1058" s="T3">n</ta>
            <ta e="T5" id="Seg_1059" s="T4">v</ta>
            <ta e="T6" id="Seg_1060" s="T5">nprop</ta>
            <ta e="T7" id="Seg_1061" s="T6">n</ta>
            <ta e="T8" id="Seg_1062" s="T7">n</ta>
            <ta e="T9" id="Seg_1063" s="T8">v</ta>
            <ta e="T10" id="Seg_1064" s="T9">pers</ta>
            <ta e="T11" id="Seg_1065" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_1066" s="T11">v</ta>
            <ta e="T13" id="Seg_1067" s="T12">interrog</ta>
            <ta e="T14" id="Seg_1068" s="T13">n</ta>
            <ta e="T15" id="Seg_1069" s="T14">v</ta>
            <ta e="T16" id="Seg_1070" s="T15">n</ta>
            <ta e="T17" id="Seg_1071" s="T16">v</ta>
            <ta e="T18" id="Seg_1072" s="T17">n</ta>
            <ta e="T19" id="Seg_1073" s="T18">v</ta>
            <ta e="T20" id="Seg_1074" s="T19">n</ta>
            <ta e="T21" id="Seg_1075" s="T20">adj</ta>
            <ta e="T22" id="Seg_1076" s="T21">n</ta>
            <ta e="T23" id="Seg_1077" s="T22">pers</ta>
            <ta e="T24" id="Seg_1078" s="T23">adv</ta>
            <ta e="T25" id="Seg_1079" s="T24">v</ta>
            <ta e="T26" id="Seg_1080" s="T25">adv</ta>
            <ta e="T27" id="Seg_1081" s="T26">adv</ta>
            <ta e="T28" id="Seg_1082" s="T27">v</ta>
            <ta e="T29" id="Seg_1083" s="T28">n</ta>
            <ta e="T30" id="Seg_1084" s="T29">n</ta>
            <ta e="T31" id="Seg_1085" s="T30">conj</ta>
            <ta e="T32" id="Seg_1086" s="T31">n</ta>
            <ta e="T33" id="Seg_1087" s="T32">n</ta>
            <ta e="T34" id="Seg_1088" s="T33">nprop</ta>
            <ta e="T35" id="Seg_1089" s="T34">v</ta>
            <ta e="T36" id="Seg_1090" s="T35">adv</ta>
            <ta e="T37" id="Seg_1091" s="T36">adv</ta>
            <ta e="T38" id="Seg_1092" s="T37">v</ta>
            <ta e="T39" id="Seg_1093" s="T38">nprop</ta>
            <ta e="T40" id="Seg_1094" s="T39">n</ta>
            <ta e="T41" id="Seg_1095" s="T40">v</ta>
            <ta e="T42" id="Seg_1096" s="T41">v</ta>
            <ta e="T43" id="Seg_1097" s="T42">adj</ta>
            <ta e="T44" id="Seg_1098" s="T43">n</ta>
            <ta e="T45" id="Seg_1099" s="T44">n</ta>
            <ta e="T46" id="Seg_1100" s="T45">v</ta>
            <ta e="T47" id="Seg_1101" s="T46">emphpro</ta>
            <ta e="T48" id="Seg_1102" s="T47">adv</ta>
            <ta e="T49" id="Seg_1103" s="T48">v</ta>
            <ta e="T50" id="Seg_1104" s="T49">adv</ta>
            <ta e="T51" id="Seg_1105" s="T50">v</ta>
            <ta e="T52" id="Seg_1106" s="T51">n</ta>
            <ta e="T53" id="Seg_1107" s="T52">quant</ta>
            <ta e="T54" id="Seg_1108" s="T53">v</ta>
            <ta e="T55" id="Seg_1109" s="T54">conj</ta>
            <ta e="T56" id="Seg_1110" s="T55">n</ta>
            <ta e="T57" id="Seg_1111" s="T56">v</ta>
            <ta e="T58" id="Seg_1112" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_1113" s="T58">v</ta>
            <ta e="T60" id="Seg_1114" s="T59">pers</ta>
            <ta e="T61" id="Seg_1115" s="T60">v</ta>
            <ta e="T62" id="Seg_1116" s="T61">v</ta>
            <ta e="T63" id="Seg_1117" s="T62">n</ta>
            <ta e="T64" id="Seg_1118" s="T63">v</ta>
            <ta e="T65" id="Seg_1119" s="T64">n</ta>
            <ta e="T66" id="Seg_1120" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_1121" s="T66">n</ta>
            <ta e="T69" id="Seg_1122" s="T68">v</ta>
            <ta e="T70" id="Seg_1123" s="T69">pers</ta>
            <ta e="T71" id="Seg_1124" s="T70">ptcl</ta>
            <ta e="T72" id="Seg_1125" s="T71">v</ta>
            <ta e="T73" id="Seg_1126" s="T72">n</ta>
            <ta e="T74" id="Seg_1127" s="T73">v</ta>
            <ta e="T75" id="Seg_1128" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_1129" s="T75">ptcl</ta>
            <ta e="T77" id="Seg_1130" s="T76">v</ta>
            <ta e="T78" id="Seg_1131" s="T77">v</ta>
            <ta e="T79" id="Seg_1132" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_1133" s="T79">v</ta>
            <ta e="T81" id="Seg_1134" s="T80">n</ta>
            <ta e="T82" id="Seg_1135" s="T81">adv</ta>
            <ta e="T83" id="Seg_1136" s="T82">v</ta>
            <ta e="T84" id="Seg_1137" s="T83">adv</ta>
            <ta e="T85" id="Seg_1138" s="T84">v</ta>
            <ta e="T86" id="Seg_1139" s="T85">v</ta>
            <ta e="T87" id="Seg_1140" s="T86">conj</ta>
            <ta e="T88" id="Seg_1141" s="T87">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_1142" s="T1">np.h:Th</ta>
            <ta e="T3" id="Seg_1143" s="T2">np:Com</ta>
            <ta e="T6" id="Seg_1144" s="T5">np.h:A</ta>
            <ta e="T8" id="Seg_1145" s="T7">np:So</ta>
            <ta e="T10" id="Seg_1146" s="T9">pro.h:E</ta>
            <ta e="T13" id="Seg_1147" s="T12">pro:L</ta>
            <ta e="T14" id="Seg_1148" s="T13">np:P</ta>
            <ta e="T16" id="Seg_1149" s="T15">np.h:A 0.3.h:Poss</ta>
            <ta e="T18" id="Seg_1150" s="T17">np:L</ta>
            <ta e="T20" id="Seg_1151" s="T19">np:Th</ta>
            <ta e="T22" id="Seg_1152" s="T21">np:Th</ta>
            <ta e="T23" id="Seg_1153" s="T22">pro.h:Th</ta>
            <ta e="T24" id="Seg_1154" s="T23">adv:L</ta>
            <ta e="T27" id="Seg_1155" s="T26">adv:L</ta>
            <ta e="T29" id="Seg_1156" s="T28">np:Poss</ta>
            <ta e="T30" id="Seg_1157" s="T29">np.h:Th</ta>
            <ta e="T32" id="Seg_1158" s="T31">np:Poss</ta>
            <ta e="T33" id="Seg_1159" s="T32">np.h:Th</ta>
            <ta e="T34" id="Seg_1160" s="T33">np.h:A</ta>
            <ta e="T36" id="Seg_1161" s="T35">adv:Time</ta>
            <ta e="T38" id="Seg_1162" s="T37">0.1.h:A</ta>
            <ta e="T39" id="Seg_1163" s="T38">np.h:A</ta>
            <ta e="T40" id="Seg_1164" s="T39">np:Time</ta>
            <ta e="T42" id="Seg_1165" s="T41">0.3.h:A</ta>
            <ta e="T44" id="Seg_1166" s="T43">np:G</ta>
            <ta e="T45" id="Seg_1167" s="T44">np:Th</ta>
            <ta e="T46" id="Seg_1168" s="T45">0.3.h:A</ta>
            <ta e="T48" id="Seg_1169" s="T47">adv:G</ta>
            <ta e="T49" id="Seg_1170" s="T48">0.3.h:A</ta>
            <ta e="T50" id="Seg_1171" s="T49">adv:G</ta>
            <ta e="T51" id="Seg_1172" s="T50">0.3.h:A</ta>
            <ta e="T52" id="Seg_1173" s="T51">np:P</ta>
            <ta e="T54" id="Seg_1174" s="T53">0.3:A</ta>
            <ta e="T56" id="Seg_1175" s="T55">np:P</ta>
            <ta e="T59" id="Seg_1176" s="T58">0.1.h:E</ta>
            <ta e="T60" id="Seg_1177" s="T59">pro.h:A</ta>
            <ta e="T62" id="Seg_1178" s="T61">0.1.h:A</ta>
            <ta e="T63" id="Seg_1179" s="T62">np.h:G</ta>
            <ta e="T64" id="Seg_1180" s="T63">0.3.h:A</ta>
            <ta e="T65" id="Seg_1181" s="T64">np.h:G</ta>
            <ta e="T70" id="Seg_1182" s="T69">pro.h:E</ta>
            <ta e="T73" id="Seg_1183" s="T72">np.h:A 0.3.h:Poss</ta>
            <ta e="T77" id="Seg_1184" s="T76">0.1.h:E</ta>
            <ta e="T78" id="Seg_1185" s="T77">0.2.h:E</ta>
            <ta e="T80" id="Seg_1186" s="T79">0.2.h:A</ta>
            <ta e="T81" id="Seg_1187" s="T80">np.h:R 0.3.h:Poss</ta>
            <ta e="T83" id="Seg_1188" s="T82">0.3.h:A</ta>
            <ta e="T84" id="Seg_1189" s="T83">adv:G</ta>
            <ta e="T85" id="Seg_1190" s="T84">0.3.h:A 0.3.h:Th</ta>
            <ta e="T86" id="Seg_1191" s="T85">0.2.h:E</ta>
            <ta e="T88" id="Seg_1192" s="T87">0.2.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_1193" s="T1">np.h:S</ta>
            <ta e="T5" id="Seg_1194" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_1195" s="T5">np.h:S</ta>
            <ta e="T9" id="Seg_1196" s="T8">v:pred</ta>
            <ta e="T10" id="Seg_1197" s="T9">pro.h:S</ta>
            <ta e="T12" id="Seg_1198" s="T11">v:pred</ta>
            <ta e="T15" id="Seg_1199" s="T12">s:compl</ta>
            <ta e="T16" id="Seg_1200" s="T15">np.h:S</ta>
            <ta e="T17" id="Seg_1201" s="T16">v:pred</ta>
            <ta e="T20" id="Seg_1202" s="T19">np:S</ta>
            <ta e="T22" id="Seg_1203" s="T21">np:S</ta>
            <ta e="T23" id="Seg_1204" s="T22">pro.h:S</ta>
            <ta e="T25" id="Seg_1205" s="T24">v:pred</ta>
            <ta e="T26" id="Seg_1206" s="T25">s:temp</ta>
            <ta e="T28" id="Seg_1207" s="T27">v:pred</ta>
            <ta e="T30" id="Seg_1208" s="T29">np.h:S</ta>
            <ta e="T33" id="Seg_1209" s="T32">np.h:S</ta>
            <ta e="T34" id="Seg_1210" s="T33">np.h:S</ta>
            <ta e="T35" id="Seg_1211" s="T34">v:pred</ta>
            <ta e="T37" id="Seg_1212" s="T36">s:purp</ta>
            <ta e="T38" id="Seg_1213" s="T37">0.1.h:S v:pred</ta>
            <ta e="T39" id="Seg_1214" s="T38">np.h:S</ta>
            <ta e="T41" id="Seg_1215" s="T40">v:pred</ta>
            <ta e="T42" id="Seg_1216" s="T41">0.3.h:S v:pred</ta>
            <ta e="T45" id="Seg_1217" s="T44">np:O</ta>
            <ta e="T46" id="Seg_1218" s="T45">0.3.h:S v:pred</ta>
            <ta e="T49" id="Seg_1219" s="T48">0.3.h:S v:pred</ta>
            <ta e="T51" id="Seg_1220" s="T50">0.3.h:S v:pred</ta>
            <ta e="T52" id="Seg_1221" s="T51">np:O</ta>
            <ta e="T54" id="Seg_1222" s="T53">0.3:S v:pred</ta>
            <ta e="T57" id="Seg_1223" s="T54">s:compl</ta>
            <ta e="T59" id="Seg_1224" s="T58">0.1.h:S v:pred</ta>
            <ta e="T60" id="Seg_1225" s="T59">pro.h:S</ta>
            <ta e="T61" id="Seg_1226" s="T60">v:pred</ta>
            <ta e="T62" id="Seg_1227" s="T61">0.1.h:S v:pred</ta>
            <ta e="T64" id="Seg_1228" s="T63">0.3.h:S v:pred</ta>
            <ta e="T70" id="Seg_1229" s="T69">pro.h:S</ta>
            <ta e="T72" id="Seg_1230" s="T71">v:pred</ta>
            <ta e="T73" id="Seg_1231" s="T72">np.h:S</ta>
            <ta e="T74" id="Seg_1232" s="T73">v:pred</ta>
            <ta e="T77" id="Seg_1233" s="T76">0.1.h:S v:pred</ta>
            <ta e="T78" id="Seg_1234" s="T77">0.2.h:S v:pred</ta>
            <ta e="T80" id="Seg_1235" s="T79">0.2.h:S v:pred</ta>
            <ta e="T82" id="Seg_1236" s="T80">s:temp</ta>
            <ta e="T83" id="Seg_1237" s="T82">0.3.h:S v:pred</ta>
            <ta e="T85" id="Seg_1238" s="T84">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T86" id="Seg_1239" s="T85">0.2.h:S v:pred</ta>
            <ta e="T88" id="Seg_1240" s="T87">0.2.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T31" id="Seg_1241" s="T30">RUS:gram</ta>
            <ta e="T45" id="Seg_1242" s="T44">RUS:cult</ta>
            <ta e="T53" id="Seg_1243" s="T52">RUS:core</ta>
            <ta e="T66" id="Seg_1244" s="T65">RUS:disc</ta>
            <ta e="T87" id="Seg_1245" s="T86">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_1246" s="T1">Idya lived with his elder brotheres (with his younger brothers).</ta>
            <ta e="T9" id="Seg_1247" s="T5">Idya returned from the hunt.</ta>
            <ta e="T17" id="Seg_1248" s="T9">“Do you know where one could catch fish?” – his younger brother said.</ta>
            <ta e="T22" id="Seg_1249" s="T17">“There is a well-known lake in the taiga, a taiga lake.</ta>
            <ta e="T26" id="Seg_1250" s="T22">We used to live and to fish there.</ta>
            <ta e="T33" id="Seg_1251" s="T26">There live the father of fish and the mother of fish.”</ta>
            <ta e="T38" id="Seg_1252" s="T33">Idya said: “Tomorrow I'll go fishing.”</ta>
            <ta e="T41" id="Seg_1253" s="T38">Idya left in the morning.</ta>
            <ta e="T44" id="Seg_1254" s="T41">He came to the taiga lake.</ta>
            <ta e="T46" id="Seg_1255" s="T44">He took rusks with him.</ta>
            <ta e="T49" id="Seg_1256" s="T46">He went to the shore.</ta>
            <ta e="T51" id="Seg_1257" s="T49">He looked at the shore.</ta>
            <ta e="T54" id="Seg_1258" s="T51">The ice is squeezed(?/covered with snow?).</ta>
            <ta e="T61" id="Seg_1259" s="T54">“I don't know how to get fish,” – he said.</ta>
            <ta e="T63" id="Seg_1260" s="T61">I'll go to my son-in-law.</ta>
            <ta e="T65" id="Seg_1261" s="T63">He came to his son-in-law.</ta>
            <ta e="T69" id="Seg_1262" s="T65">Well, it's not fishing time.</ta>
            <ta e="T72" id="Seg_1263" s="T69">You don't know.</ta>
            <ta e="T77" id="Seg_1264" s="T72">His son-in-law said: “As if I didn't know.”</ta>
            <ta e="T80" id="Seg_1265" s="T77">“You know, but you don't say.”</ta>
            <ta e="T83" id="Seg_1266" s="T80">He was scolding his son-in-law.</ta>
            <ta e="T85" id="Seg_1267" s="T83">He took him to the shore.</ta>
            <ta e="T88" id="Seg_1268" s="T85">“If you know, then tell me.”</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_1269" s="T1">Itja lebte mit seinen älteren Brüdern (mit seinen jüngeren Brüdern).</ta>
            <ta e="T9" id="Seg_1270" s="T5">Itja kehrte von der Jagd zurück.</ta>
            <ta e="T17" id="Seg_1271" s="T9">"Weißt du, wo man Fische fangen kann?" -- sagte sein kleiner Bruder.</ta>
            <ta e="T22" id="Seg_1272" s="T17">"Es gibt da einen bekannten See in der Taiga, einen Taiga-See.</ta>
            <ta e="T26" id="Seg_1273" s="T22">Wir haben dort gelebt und gefischt.</ta>
            <ta e="T33" id="Seg_1274" s="T26">Dort leben der Vater der Fische und die Mutter der Fische."</ta>
            <ta e="T38" id="Seg_1275" s="T33">Itja sagte: "Morgen gehe ich fischen."</ta>
            <ta e="T41" id="Seg_1276" s="T38">Itja brach am Morgen auf.</ta>
            <ta e="T44" id="Seg_1277" s="T41">Er kam zum Taiga-See.</ta>
            <ta e="T46" id="Seg_1278" s="T44">Er nahm Zwieback mit.</ta>
            <ta e="T49" id="Seg_1279" s="T46">Er ging ans Ufer.</ta>
            <ta e="T51" id="Seg_1280" s="T49">Er sah ans Ufer.</ta>
            <ta e="T54" id="Seg_1281" s="T51">Das Eis ist zusammengedrückt(?/mit Schnee bedeckt?).</ta>
            <ta e="T61" id="Seg_1282" s="T54">"Ich weiß nicht, wie man Fische kriegt", - sagte er.</ta>
            <ta e="T63" id="Seg_1283" s="T61">Ich gehe zu meinem Schwiegersohn.</ta>
            <ta e="T65" id="Seg_1284" s="T63">Er kam zu seinem Schwiegersohn.</ta>
            <ta e="T69" id="Seg_1285" s="T65">Nunja, es ist nicht die Zeit zum Fischen.</ta>
            <ta e="T72" id="Seg_1286" s="T69">Du weißt es nicht.</ta>
            <ta e="T77" id="Seg_1287" s="T72">Sein Schwiegersohn sagte: "Als ob ich es nicht wüsste."</ta>
            <ta e="T80" id="Seg_1288" s="T77">"Du weißt es, aber du sagst nichts."</ta>
            <ta e="T83" id="Seg_1289" s="T80">Er schimpfte mit seinem Schwiegersohn.</ta>
            <ta e="T85" id="Seg_1290" s="T83">Er brachte ihn zum Ufer.</ta>
            <ta e="T88" id="Seg_1291" s="T85">"Wenn du es weißt, dann sag es mir."</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_1292" s="T1">Идя жил со старшими братьями (с младшими братьями).</ta>
            <ta e="T9" id="Seg_1293" s="T5">Идя с охоты пришел.</ta>
            <ta e="T17" id="Seg_1294" s="T9">“Ты не знаешь, где рыбу добыть?” – младший брат говорит.</ta>
            <ta e="T22" id="Seg_1295" s="T17">“В тайге знакомое озеро, таежное озеро.</ta>
            <ta e="T26" id="Seg_1296" s="T22">Мы там жили, рыбачили.</ta>
            <ta e="T33" id="Seg_1297" s="T26">Здесь живут отец рыб и мать рыб”.</ta>
            <ta e="T38" id="Seg_1298" s="T33">Идя говорит: “Завтра рыбачить поеду”.</ta>
            <ta e="T41" id="Seg_1299" s="T38">Идя утром поехал.</ta>
            <ta e="T44" id="Seg_1300" s="T41">Пришел на таёжное озеро.</ta>
            <ta e="T46" id="Seg_1301" s="T44">Сухарей взял.</ta>
            <ta e="T49" id="Seg_1302" s="T46">Сам на берег(/гору) отправился.</ta>
            <ta e="T51" id="Seg_1303" s="T49">На берег смотрит.</ta>
            <ta e="T54" id="Seg_1304" s="T51">Лед весь замело(?).</ta>
            <ta e="T61" id="Seg_1305" s="T54">“Как рыбу добыть, не знаю”, – он сказал.</ta>
            <ta e="T63" id="Seg_1306" s="T61">“К зятю пойду”.</ta>
            <ta e="T65" id="Seg_1307" s="T63">Пришел к зятю.</ta>
            <ta e="T69" id="Seg_1308" s="T65">Ну, рыбу некогда добывать.</ta>
            <ta e="T72" id="Seg_1309" s="T69">Вы не знаете.</ta>
            <ta e="T77" id="Seg_1310" s="T72">Зять его сказал: “Как будто бы я не знаю”.</ta>
            <ta e="T80" id="Seg_1311" s="T77">“Вы знаете, да не говорите”.</ta>
            <ta e="T83" id="Seg_1312" s="T80">Зятя своего ругает.</ta>
            <ta e="T85" id="Seg_1313" s="T83">На берег тащит.</ta>
            <ta e="T88" id="Seg_1314" s="T85">“(Если) знаешь, то скажи”.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_1315" s="T1">Идя жил с братьями старшими (младшими братьями)</ta>
            <ta e="T9" id="Seg_1316" s="T5">Идя с охоты пришел</ta>
            <ta e="T17" id="Seg_1317" s="T9">ты не знаешь где рыбу добыть младший брат говорит</ta>
            <ta e="T22" id="Seg_1318" s="T17">таежное зна(ко)мое озеро таежное озеро</ta>
            <ta e="T26" id="Seg_1319" s="T22">мы тут и жили рыбачили</ta>
            <ta e="T33" id="Seg_1320" s="T26">здесь жил отец рыб и мать рыб</ta>
            <ta e="T38" id="Seg_1321" s="T33">Идя говорит завтра рыбачить поеду</ta>
            <ta e="T41" id="Seg_1322" s="T38">Идя утром поехал</ta>
            <ta e="T44" id="Seg_1323" s="T41">пришел на материчное озеро</ta>
            <ta e="T46" id="Seg_1324" s="T44">сухарей взял</ta>
            <ta e="T49" id="Seg_1325" s="T46">сам на гору пошел</ta>
            <ta e="T51" id="Seg_1326" s="T49">на берег смотрит</ta>
            <ta e="T54" id="Seg_1327" s="T51">лед весь замело</ta>
            <ta e="T61" id="Seg_1328" s="T54">как рыбу добыть не знаю он сказал</ta>
            <ta e="T63" id="Seg_1329" s="T61">к зятю пойду</ta>
            <ta e="T65" id="Seg_1330" s="T63">пришел к зятю</ta>
            <ta e="T69" id="Seg_1331" s="T65">рыбу некогда добывать</ta>
            <ta e="T72" id="Seg_1332" s="T69">вы не знаете</ta>
            <ta e="T77" id="Seg_1333" s="T72">зять сказал как будто бы я не знаю</ta>
            <ta e="T80" id="Seg_1334" s="T77">знаете да не сказываете</ta>
            <ta e="T83" id="Seg_1335" s="T80">зятя материт</ta>
            <ta e="T85" id="Seg_1336" s="T83">на берег тащит</ta>
            <ta e="T88" id="Seg_1337" s="T85">если знаешь то скажи</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T33" id="Seg_1338" s="T26">[KuAI:] Variant: 'ästä'.</ta>
            <ta e="T38" id="Seg_1339" s="T33">[KuAI:] Variant: 'qwatʒan'.</ta>
            <ta e="T54" id="Seg_1340" s="T51">[BrM:] Tentative analysis of 'pitqalba'.</ta>
            <ta e="T63" id="Seg_1341" s="T61">[BrM:] LOC?</ta>
            <ta e="T65" id="Seg_1342" s="T63">[BrM:] LOC?</ta>
            <ta e="T69" id="Seg_1343" s="T65">[BrM:] It's not clear, who says this.</ta>
            <ta e="T72" id="Seg_1344" s="T69">[BrM:] It's not clear, who says this. [BrM:] 2PL?</ta>
            <ta e="T80" id="Seg_1345" s="T77">[BrM:] 2PL?</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T83" id="Seg_1346" s="T80">′ты̄ɣу - материть</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
