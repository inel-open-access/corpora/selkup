<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_196X_TwoBrothers_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_196X_TwoBrothers_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">216</ud-information>
            <ud-information attribute-name="# HIAT:w">163</ud-information>
            <ud-information attribute-name="# e">163</ud-information>
            <ud-information attribute-name="# HIAT:u">29</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T164" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Səd</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">agazɨɣandʼät</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">tʼäptä</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_14" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">Menan</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">jes</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">səda</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">agazɨɣə</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_29" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">Okkɨrɨn</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">me</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">üːbaj</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">nʼürtɨ</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">poːno</ts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_47" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">Pom</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">padʼännaj</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">i</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">omdaj</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_61" n="HIAT:w" s="T17">qwasqulǯuɣu</ts>
                  <nts id="Seg_62" n="HIAT:ip">.</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_65" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">Man</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">agaw</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_73" n="HIAT:w" s="T20">tʼärɨn</ts>
                  <nts id="Seg_74" n="HIAT:ip">:</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_76" n="HIAT:ip">“</nts>
                  <ts e="T22" id="Seg_78" n="HIAT:w" s="T21">Tʼeːɣə</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_81" n="HIAT:w" s="T22">bə</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_84" n="HIAT:w" s="T23">nʼäim</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_87" n="HIAT:w" s="T24">mʼäɣunä</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_90" n="HIAT:w" s="T25">menettə</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_93" n="HIAT:w" s="T26">bə</ts>
                  <nts id="Seg_94" n="HIAT:ip">.</nts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_97" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_99" n="HIAT:w" s="T27">Me</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_102" n="HIAT:w" s="T28">bə</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_105" n="HIAT:w" s="T29">augədi</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_108" n="HIAT:w" s="T30">perget</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_111" n="HIAT:w" s="T31">wes</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_114" n="HIAT:w" s="T32">amnäj</ts>
                  <nts id="Seg_115" n="HIAT:ip">.</nts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_118" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_120" n="HIAT:w" s="T33">Tan</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_123" n="HIAT:w" s="T34">aga</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_126" n="HIAT:w" s="T35">meːgak</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_129" n="HIAT:w" s="T36">mannɨbɨnet</ts>
                  <nts id="Seg_130" n="HIAT:ip">.</nts>
                  <nts id="Seg_131" n="HIAT:ip">”</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_134" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_136" n="HIAT:w" s="T37">Täp</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_139" n="HIAT:w" s="T38">kennɨt</ts>
                  <nts id="Seg_140" n="HIAT:ip">,</nts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_143" n="HIAT:w" s="T39">i</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_146" n="HIAT:w" s="T40">tʼeɣa</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_149" n="HIAT:w" s="T41">nʼäj</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_152" n="HIAT:w" s="T42">čaːʒit</ts>
                  <nts id="Seg_153" n="HIAT:ip">,</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_156" n="HIAT:w" s="T43">kɨɣəndi</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_159" n="HIAT:w" s="T44">pükgɨjbɨlʼe</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_162" n="HIAT:w" s="T45">čaːǯit</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_165" n="HIAT:w" s="T46">i</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_168" n="HIAT:w" s="T47">meɣuni</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_171" n="HIAT:w" s="T48">tüːa</ts>
                  <nts id="Seg_172" n="HIAT:ip">.</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_175" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_177" n="HIAT:w" s="T49">Me</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_180" n="HIAT:w" s="T50">omdaj</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_183" n="HIAT:w" s="T51">i</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_186" n="HIAT:w" s="T52">amnaj</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_189" n="HIAT:w" s="T53">täbɨm</ts>
                  <nts id="Seg_190" n="HIAT:ip">.</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_193" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_195" n="HIAT:w" s="T54">Agaw</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_198" n="HIAT:w" s="T55">tʼärɨn</ts>
                  <nts id="Seg_199" n="HIAT:ip">:</nts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_201" n="HIAT:ip">“</nts>
                  <ts e="T57" id="Seg_203" n="HIAT:w" s="T56">Arakaj</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_206" n="HIAT:w" s="T57">čorgɨm</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_209" n="HIAT:w" s="T58">bə</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_212" n="HIAT:w" s="T59">täperʼ</ts>
                  <nts id="Seg_213" n="HIAT:ip">.</nts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_216" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_218" n="HIAT:w" s="T60">Me</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_221" n="HIAT:w" s="T61">bɨ</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_224" n="HIAT:w" s="T62">täbɨm</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_227" n="HIAT:w" s="T63">wes</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_230" n="HIAT:w" s="T64">bə</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_233" n="HIAT:w" s="T65">ünnäj</ts>
                  <nts id="Seg_234" n="HIAT:ip">.</nts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_237" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_239" n="HIAT:w" s="T66">Ot</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_242" n="HIAT:w" s="T67">tan</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_245" n="HIAT:w" s="T68">be</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_248" n="HIAT:w" s="T69">mekga</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_251" n="HIAT:w" s="T70">mannɨbɨnät</ts>
                  <nts id="Seg_252" n="HIAT:ip">,</nts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_255" n="HIAT:w" s="T71">mazɨm</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_258" n="HIAT:w" s="T72">kutdär</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_261" n="HIAT:w" s="T73">seːrba</ts>
                  <nts id="Seg_262" n="HIAT:ip">.</nts>
                  <nts id="Seg_263" n="HIAT:ip">”</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_266" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_268" n="HIAT:w" s="T74">Täp</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_271" n="HIAT:w" s="T75">kedädʼit</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_274" n="HIAT:w" s="T76">i</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_277" n="HIAT:w" s="T77">qäɣɨndɨ</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_280" n="HIAT:w" s="T78">čorgə</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_283" n="HIAT:w" s="T79">pükgɨjbɨlʼe</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_286" n="HIAT:w" s="T80">čaːǯin</ts>
                  <nts id="Seg_287" n="HIAT:ip">,</nts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_290" n="HIAT:w" s="T81">meɣuni</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_293" n="HIAT:w" s="T82">čaːʒɨn</ts>
                  <nts id="Seg_294" n="HIAT:ip">.</nts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_297" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_299" n="HIAT:w" s="T83">Me</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_302" n="HIAT:w" s="T84">nɨtdɨn</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_305" n="HIAT:w" s="T85">malčaj</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_308" n="HIAT:w" s="T86">čorɣokkam</ts>
                  <nts id="Seg_309" n="HIAT:ip">,</nts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_312" n="HIAT:w" s="T87">nʼüɣunnaj</ts>
                  <nts id="Seg_313" n="HIAT:ip">.</nts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_316" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_318" n="HIAT:w" s="T88">Agat</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_321" n="HIAT:w" s="T89">aj</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_324" n="HIAT:w" s="T90">tʼärɨn</ts>
                  <nts id="Seg_325" n="HIAT:ip">:</nts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_327" n="HIAT:ip">“</nts>
                  <ts e="T92" id="Seg_329" n="HIAT:w" s="T91">Ot</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_332" n="HIAT:w" s="T92">me</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_335" n="HIAT:w" s="T93">aurnaj</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_338" n="HIAT:w" s="T94">i</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_341" n="HIAT:w" s="T95">qabɨʒaj</ts>
                  <nts id="Seg_342" n="HIAT:ip">.</nts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_345" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_347" n="HIAT:w" s="T96">Sečasbə</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_350" n="HIAT:w" s="T97">surum</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_353" n="HIAT:w" s="T98">eraze</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_356" n="HIAT:w" s="T99">parosačugu</ts>
                  <nts id="Seg_357" n="HIAT:ip">,</nts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_360" n="HIAT:w" s="T100">lɨlaw</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_363" n="HIAT:w" s="T101">bə</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_366" n="HIAT:w" s="T102">petkɨgu</ts>
                  <nts id="Seg_367" n="HIAT:ip">.</nts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_370" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_372" n="HIAT:w" s="T103">Tan</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_375" n="HIAT:w" s="T104">bə</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_378" n="HIAT:w" s="T105">mazɨm</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_381" n="HIAT:w" s="T106">mannɨbɨneːt</ts>
                  <nts id="Seg_382" n="HIAT:ip">.</nts>
                  <nts id="Seg_383" n="HIAT:ip">”</nts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_386" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_388" n="HIAT:w" s="T107">Agat</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_391" n="HIAT:w" s="T108">tolʼko</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_394" n="HIAT:w" s="T109">tʼärɨn</ts>
                  <nts id="Seg_395" n="HIAT:ip">,</nts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_398" n="HIAT:w" s="T110">nʼürɣɨn</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_401" n="HIAT:w" s="T111">kɨʒiblʼe</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_404" n="HIAT:w" s="T112">üːbəran</ts>
                  <nts id="Seg_405" n="HIAT:ip">.</nts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_408" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_410" n="HIAT:w" s="T113">Mʼäɣune</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_413" n="HIAT:w" s="T114">čaːǯin</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_416" n="HIAT:w" s="T115">suːräm</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_419" n="HIAT:w" s="T116">era</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_422" n="HIAT:w" s="T117">čobɨrɨm</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_425" n="HIAT:w" s="T118">čaɣoptugu</ts>
                  <nts id="Seg_426" n="HIAT:ip">.</nts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_429" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_431" n="HIAT:w" s="T119">Mezuwi</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_434" n="HIAT:w" s="T120">petkulgu</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_437" n="HIAT:w" s="T121">kɨgɨn</ts>
                  <nts id="Seg_438" n="HIAT:ip">.</nts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_441" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_443" n="HIAT:w" s="T122">Manan</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_446" n="HIAT:w" s="T123">sɨt</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_449" n="HIAT:w" s="T124">sidʼeu</ts>
                  <nts id="Seg_450" n="HIAT:ip">.</nts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T129" id="Seg_453" n="HIAT:u" s="T125">
                  <ts e="T126" id="Seg_455" n="HIAT:w" s="T125">Okkɨr</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_458" n="HIAT:w" s="T126">sidel</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_461" n="HIAT:w" s="T127">qulupba</ts>
                  <nts id="Seg_462" n="HIAT:ip">:</nts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_464" n="HIAT:ip">“</nts>
                  <ts e="T129" id="Seg_466" n="HIAT:w" s="T128">Oralbet</ts>
                  <nts id="Seg_467" n="HIAT:ip">!</nts>
                  <nts id="Seg_468" n="HIAT:ip">”</nts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T134" id="Seg_471" n="HIAT:u" s="T129">
                  <ts e="T130" id="Seg_473" n="HIAT:w" s="T129">A</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_476" n="HIAT:w" s="T130">sədəmdettə</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_479" n="HIAT:w" s="T131">sidel</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_482" n="HIAT:w" s="T132">tʼarɨn</ts>
                  <nts id="Seg_483" n="HIAT:ip">:</nts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_485" n="HIAT:ip">“</nts>
                  <ts e="T134" id="Seg_487" n="HIAT:w" s="T133">Qurolgə</ts>
                  <nts id="Seg_488" n="HIAT:ip">.</nts>
                  <nts id="Seg_489" n="HIAT:ip">”</nts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T140" id="Seg_492" n="HIAT:u" s="T134">
                  <ts e="T135" id="Seg_494" n="HIAT:w" s="T134">Man</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_497" n="HIAT:w" s="T135">ügulǯan</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_500" n="HIAT:w" s="T136">na</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_503" n="HIAT:w" s="T137">sideɣɨn</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_506" n="HIAT:w" s="T138">što</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_509" n="HIAT:w" s="T139">qurolge</ts>
                  <nts id="Seg_510" n="HIAT:ip">.</nts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T146" id="Seg_513" n="HIAT:u" s="T140">
                  <ts e="T141" id="Seg_515" n="HIAT:w" s="T140">Kak</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_518" n="HIAT:w" s="T141">üːbəditdat</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_521" n="HIAT:w" s="T142">i</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_524" n="HIAT:w" s="T143">mazɨm</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_527" n="HIAT:w" s="T144">tolʼko</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_530" n="HIAT:w" s="T145">kolʼdättə</ts>
                  <nts id="Seg_531" n="HIAT:ip">.</nts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_534" n="HIAT:u" s="T146">
                  <ts e="T147" id="Seg_536" n="HIAT:w" s="T146">Man</ts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_539" n="HIAT:w" s="T147">sədəmdʼettə</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_542" n="HIAT:w" s="T148">dʼel</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_545" n="HIAT:w" s="T149">paːrannan</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_548" n="HIAT:w" s="T150">mannɨbɨgu</ts>
                  <nts id="Seg_549" n="HIAT:ip">,</nts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_552" n="HIAT:w" s="T151">kudə</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_555" n="HIAT:w" s="T152">laptäditda</ts>
                  <nts id="Seg_556" n="HIAT:ip">.</nts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T156" id="Seg_559" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_561" n="HIAT:w" s="T153">Man</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_564" n="HIAT:w" s="T154">tüan</ts>
                  <nts id="Seg_565" n="HIAT:ip">,</nts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_568" n="HIAT:w" s="T155">mannɨpou</ts>
                  <nts id="Seg_569" n="HIAT:ip">.</nts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_572" n="HIAT:u" s="T156">
                  <ts e="T157" id="Seg_574" n="HIAT:w" s="T156">Agaunan</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_577" n="HIAT:w" s="T157">kɨbelʼdʼika</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_580" n="HIAT:w" s="T158">lagaška</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_583" n="HIAT:w" s="T159">qalɨmmɨt</ts>
                  <nts id="Seg_584" n="HIAT:ip">.</nts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T164" id="Seg_587" n="HIAT:u" s="T160">
                  <ts e="T161" id="Seg_589" n="HIAT:w" s="T160">A</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_592" n="HIAT:w" s="T161">suːrumnan</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_595" n="HIAT:w" s="T162">qajnäs</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_598" n="HIAT:w" s="T163">qalɨpba</ts>
                  <nts id="Seg_599" n="HIAT:ip">.</nts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T164" id="Seg_601" n="sc" s="T1">
               <ts e="T2" id="Seg_603" n="e" s="T1">Səd </ts>
               <ts e="T3" id="Seg_605" n="e" s="T2">agazɨɣandʼät </ts>
               <ts e="T4" id="Seg_607" n="e" s="T3">tʼäptä. </ts>
               <ts e="T5" id="Seg_609" n="e" s="T4">Menan </ts>
               <ts e="T6" id="Seg_611" n="e" s="T5">jes </ts>
               <ts e="T7" id="Seg_613" n="e" s="T6">səda </ts>
               <ts e="T8" id="Seg_615" n="e" s="T7">agazɨɣə. </ts>
               <ts e="T9" id="Seg_617" n="e" s="T8">Okkɨrɨn </ts>
               <ts e="T10" id="Seg_619" n="e" s="T9">me </ts>
               <ts e="T11" id="Seg_621" n="e" s="T10">üːbaj </ts>
               <ts e="T12" id="Seg_623" n="e" s="T11">nʼürtɨ </ts>
               <ts e="T13" id="Seg_625" n="e" s="T12">poːno. </ts>
               <ts e="T14" id="Seg_627" n="e" s="T13">Pom </ts>
               <ts e="T15" id="Seg_629" n="e" s="T14">padʼännaj </ts>
               <ts e="T16" id="Seg_631" n="e" s="T15">i </ts>
               <ts e="T17" id="Seg_633" n="e" s="T16">omdaj </ts>
               <ts e="T18" id="Seg_635" n="e" s="T17">qwasqulǯuɣu. </ts>
               <ts e="T19" id="Seg_637" n="e" s="T18">Man </ts>
               <ts e="T20" id="Seg_639" n="e" s="T19">agaw </ts>
               <ts e="T21" id="Seg_641" n="e" s="T20">tʼärɨn: </ts>
               <ts e="T22" id="Seg_643" n="e" s="T21">“Tʼeːɣə </ts>
               <ts e="T23" id="Seg_645" n="e" s="T22">bə </ts>
               <ts e="T24" id="Seg_647" n="e" s="T23">nʼäim </ts>
               <ts e="T25" id="Seg_649" n="e" s="T24">mʼäɣunä </ts>
               <ts e="T26" id="Seg_651" n="e" s="T25">menettə </ts>
               <ts e="T27" id="Seg_653" n="e" s="T26">bə. </ts>
               <ts e="T28" id="Seg_655" n="e" s="T27">Me </ts>
               <ts e="T29" id="Seg_657" n="e" s="T28">bə </ts>
               <ts e="T30" id="Seg_659" n="e" s="T29">augədi </ts>
               <ts e="T31" id="Seg_661" n="e" s="T30">perget </ts>
               <ts e="T32" id="Seg_663" n="e" s="T31">wes </ts>
               <ts e="T33" id="Seg_665" n="e" s="T32">amnäj. </ts>
               <ts e="T34" id="Seg_667" n="e" s="T33">Tan </ts>
               <ts e="T35" id="Seg_669" n="e" s="T34">aga </ts>
               <ts e="T36" id="Seg_671" n="e" s="T35">meːgak </ts>
               <ts e="T37" id="Seg_673" n="e" s="T36">mannɨbɨnet.” </ts>
               <ts e="T38" id="Seg_675" n="e" s="T37">Täp </ts>
               <ts e="T39" id="Seg_677" n="e" s="T38">kennɨt, </ts>
               <ts e="T40" id="Seg_679" n="e" s="T39">i </ts>
               <ts e="T41" id="Seg_681" n="e" s="T40">tʼeɣa </ts>
               <ts e="T42" id="Seg_683" n="e" s="T41">nʼäj </ts>
               <ts e="T43" id="Seg_685" n="e" s="T42">čaːʒit, </ts>
               <ts e="T44" id="Seg_687" n="e" s="T43">kɨɣəndi </ts>
               <ts e="T45" id="Seg_689" n="e" s="T44">pükgɨjbɨlʼe </ts>
               <ts e="T46" id="Seg_691" n="e" s="T45">čaːǯit </ts>
               <ts e="T47" id="Seg_693" n="e" s="T46">i </ts>
               <ts e="T48" id="Seg_695" n="e" s="T47">meɣuni </ts>
               <ts e="T49" id="Seg_697" n="e" s="T48">tüːa. </ts>
               <ts e="T50" id="Seg_699" n="e" s="T49">Me </ts>
               <ts e="T51" id="Seg_701" n="e" s="T50">omdaj </ts>
               <ts e="T52" id="Seg_703" n="e" s="T51">i </ts>
               <ts e="T53" id="Seg_705" n="e" s="T52">amnaj </ts>
               <ts e="T54" id="Seg_707" n="e" s="T53">täbɨm. </ts>
               <ts e="T55" id="Seg_709" n="e" s="T54">Agaw </ts>
               <ts e="T56" id="Seg_711" n="e" s="T55">tʼärɨn: </ts>
               <ts e="T57" id="Seg_713" n="e" s="T56">“Arakaj </ts>
               <ts e="T58" id="Seg_715" n="e" s="T57">čorgɨm </ts>
               <ts e="T59" id="Seg_717" n="e" s="T58">bə </ts>
               <ts e="T60" id="Seg_719" n="e" s="T59">täperʼ. </ts>
               <ts e="T61" id="Seg_721" n="e" s="T60">Me </ts>
               <ts e="T62" id="Seg_723" n="e" s="T61">bɨ </ts>
               <ts e="T63" id="Seg_725" n="e" s="T62">täbɨm </ts>
               <ts e="T64" id="Seg_727" n="e" s="T63">wes </ts>
               <ts e="T65" id="Seg_729" n="e" s="T64">bə </ts>
               <ts e="T66" id="Seg_731" n="e" s="T65">ünnäj. </ts>
               <ts e="T67" id="Seg_733" n="e" s="T66">Ot </ts>
               <ts e="T68" id="Seg_735" n="e" s="T67">tan </ts>
               <ts e="T69" id="Seg_737" n="e" s="T68">be </ts>
               <ts e="T70" id="Seg_739" n="e" s="T69">mekga </ts>
               <ts e="T71" id="Seg_741" n="e" s="T70">mannɨbɨnät, </ts>
               <ts e="T72" id="Seg_743" n="e" s="T71">mazɨm </ts>
               <ts e="T73" id="Seg_745" n="e" s="T72">kutdär </ts>
               <ts e="T74" id="Seg_747" n="e" s="T73">seːrba.” </ts>
               <ts e="T75" id="Seg_749" n="e" s="T74">Täp </ts>
               <ts e="T76" id="Seg_751" n="e" s="T75">kedädʼit </ts>
               <ts e="T77" id="Seg_753" n="e" s="T76">i </ts>
               <ts e="T78" id="Seg_755" n="e" s="T77">qäɣɨndɨ </ts>
               <ts e="T79" id="Seg_757" n="e" s="T78">čorgə </ts>
               <ts e="T80" id="Seg_759" n="e" s="T79">pükgɨjbɨlʼe </ts>
               <ts e="T81" id="Seg_761" n="e" s="T80">čaːǯin, </ts>
               <ts e="T82" id="Seg_763" n="e" s="T81">meɣuni </ts>
               <ts e="T83" id="Seg_765" n="e" s="T82">čaːʒɨn. </ts>
               <ts e="T84" id="Seg_767" n="e" s="T83">Me </ts>
               <ts e="T85" id="Seg_769" n="e" s="T84">nɨtdɨn </ts>
               <ts e="T86" id="Seg_771" n="e" s="T85">malčaj </ts>
               <ts e="T87" id="Seg_773" n="e" s="T86">čorɣokkam, </ts>
               <ts e="T88" id="Seg_775" n="e" s="T87">nʼüɣunnaj. </ts>
               <ts e="T89" id="Seg_777" n="e" s="T88">Agat </ts>
               <ts e="T90" id="Seg_779" n="e" s="T89">aj </ts>
               <ts e="T91" id="Seg_781" n="e" s="T90">tʼärɨn: </ts>
               <ts e="T92" id="Seg_783" n="e" s="T91">“Ot </ts>
               <ts e="T93" id="Seg_785" n="e" s="T92">me </ts>
               <ts e="T94" id="Seg_787" n="e" s="T93">aurnaj </ts>
               <ts e="T95" id="Seg_789" n="e" s="T94">i </ts>
               <ts e="T96" id="Seg_791" n="e" s="T95">qabɨʒaj. </ts>
               <ts e="T97" id="Seg_793" n="e" s="T96">Sečasbə </ts>
               <ts e="T98" id="Seg_795" n="e" s="T97">surum </ts>
               <ts e="T99" id="Seg_797" n="e" s="T98">eraze </ts>
               <ts e="T100" id="Seg_799" n="e" s="T99">parosačugu, </ts>
               <ts e="T101" id="Seg_801" n="e" s="T100">lɨlaw </ts>
               <ts e="T102" id="Seg_803" n="e" s="T101">bə </ts>
               <ts e="T103" id="Seg_805" n="e" s="T102">petkɨgu. </ts>
               <ts e="T104" id="Seg_807" n="e" s="T103">Tan </ts>
               <ts e="T105" id="Seg_809" n="e" s="T104">bə </ts>
               <ts e="T106" id="Seg_811" n="e" s="T105">mazɨm </ts>
               <ts e="T107" id="Seg_813" n="e" s="T106">mannɨbɨneːt.” </ts>
               <ts e="T108" id="Seg_815" n="e" s="T107">Agat </ts>
               <ts e="T109" id="Seg_817" n="e" s="T108">tolʼko </ts>
               <ts e="T110" id="Seg_819" n="e" s="T109">tʼärɨn, </ts>
               <ts e="T111" id="Seg_821" n="e" s="T110">nʼürɣɨn </ts>
               <ts e="T112" id="Seg_823" n="e" s="T111">kɨʒiblʼe </ts>
               <ts e="T113" id="Seg_825" n="e" s="T112">üːbəran. </ts>
               <ts e="T114" id="Seg_827" n="e" s="T113">Mʼäɣune </ts>
               <ts e="T115" id="Seg_829" n="e" s="T114">čaːǯin </ts>
               <ts e="T116" id="Seg_831" n="e" s="T115">suːräm </ts>
               <ts e="T117" id="Seg_833" n="e" s="T116">era </ts>
               <ts e="T118" id="Seg_835" n="e" s="T117">čobɨrɨm </ts>
               <ts e="T119" id="Seg_837" n="e" s="T118">čaɣoptugu. </ts>
               <ts e="T120" id="Seg_839" n="e" s="T119">Mezuwi </ts>
               <ts e="T121" id="Seg_841" n="e" s="T120">petkulgu </ts>
               <ts e="T122" id="Seg_843" n="e" s="T121">kɨgɨn. </ts>
               <ts e="T123" id="Seg_845" n="e" s="T122">Manan </ts>
               <ts e="T124" id="Seg_847" n="e" s="T123">sɨt </ts>
               <ts e="T125" id="Seg_849" n="e" s="T124">sidʼeu. </ts>
               <ts e="T126" id="Seg_851" n="e" s="T125">Okkɨr </ts>
               <ts e="T127" id="Seg_853" n="e" s="T126">sidel </ts>
               <ts e="T128" id="Seg_855" n="e" s="T127">qulupba: </ts>
               <ts e="T129" id="Seg_857" n="e" s="T128">“Oralbet!” </ts>
               <ts e="T130" id="Seg_859" n="e" s="T129">A </ts>
               <ts e="T131" id="Seg_861" n="e" s="T130">sədəmdettə </ts>
               <ts e="T132" id="Seg_863" n="e" s="T131">sidel </ts>
               <ts e="T133" id="Seg_865" n="e" s="T132">tʼarɨn: </ts>
               <ts e="T134" id="Seg_867" n="e" s="T133">“Qurolgə.” </ts>
               <ts e="T135" id="Seg_869" n="e" s="T134">Man </ts>
               <ts e="T136" id="Seg_871" n="e" s="T135">ügulǯan </ts>
               <ts e="T137" id="Seg_873" n="e" s="T136">na </ts>
               <ts e="T138" id="Seg_875" n="e" s="T137">sideɣɨn </ts>
               <ts e="T139" id="Seg_877" n="e" s="T138">što </ts>
               <ts e="T140" id="Seg_879" n="e" s="T139">qurolge. </ts>
               <ts e="T141" id="Seg_881" n="e" s="T140">Kak </ts>
               <ts e="T142" id="Seg_883" n="e" s="T141">üːbəditdat </ts>
               <ts e="T143" id="Seg_885" n="e" s="T142">i </ts>
               <ts e="T144" id="Seg_887" n="e" s="T143">mazɨm </ts>
               <ts e="T145" id="Seg_889" n="e" s="T144">tolʼko </ts>
               <ts e="T146" id="Seg_891" n="e" s="T145">kolʼdättə. </ts>
               <ts e="T147" id="Seg_893" n="e" s="T146">Man </ts>
               <ts e="T148" id="Seg_895" n="e" s="T147">sədəmdʼettə </ts>
               <ts e="T149" id="Seg_897" n="e" s="T148">dʼel </ts>
               <ts e="T150" id="Seg_899" n="e" s="T149">paːrannan </ts>
               <ts e="T151" id="Seg_901" n="e" s="T150">mannɨbɨgu, </ts>
               <ts e="T152" id="Seg_903" n="e" s="T151">kudə </ts>
               <ts e="T153" id="Seg_905" n="e" s="T152">laptäditda. </ts>
               <ts e="T154" id="Seg_907" n="e" s="T153">Man </ts>
               <ts e="T155" id="Seg_909" n="e" s="T154">tüan, </ts>
               <ts e="T156" id="Seg_911" n="e" s="T155">mannɨpou. </ts>
               <ts e="T157" id="Seg_913" n="e" s="T156">Agaunan </ts>
               <ts e="T158" id="Seg_915" n="e" s="T157">kɨbelʼdʼika </ts>
               <ts e="T159" id="Seg_917" n="e" s="T158">lagaška </ts>
               <ts e="T160" id="Seg_919" n="e" s="T159">qalɨmmɨt. </ts>
               <ts e="T161" id="Seg_921" n="e" s="T160">A </ts>
               <ts e="T162" id="Seg_923" n="e" s="T161">suːrumnan </ts>
               <ts e="T163" id="Seg_925" n="e" s="T162">qajnäs </ts>
               <ts e="T164" id="Seg_927" n="e" s="T163">qalɨpba. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_928" s="T1">PVD_196X_TwoBrothers_flk.001 (001.001)</ta>
            <ta e="T8" id="Seg_929" s="T4">PVD_196X_TwoBrothers_flk.002 (001.002)</ta>
            <ta e="T13" id="Seg_930" s="T8">PVD_196X_TwoBrothers_flk.003 (001.003)</ta>
            <ta e="T18" id="Seg_931" s="T13">PVD_196X_TwoBrothers_flk.004 (001.004)</ta>
            <ta e="T27" id="Seg_932" s="T18">PVD_196X_TwoBrothers_flk.005 (001.005)</ta>
            <ta e="T33" id="Seg_933" s="T27">PVD_196X_TwoBrothers_flk.006 (001.006)</ta>
            <ta e="T37" id="Seg_934" s="T33">PVD_196X_TwoBrothers_flk.007 (001.007)</ta>
            <ta e="T49" id="Seg_935" s="T37">PVD_196X_TwoBrothers_flk.008 (001.008)</ta>
            <ta e="T54" id="Seg_936" s="T49">PVD_196X_TwoBrothers_flk.009 (001.009)</ta>
            <ta e="T60" id="Seg_937" s="T54">PVD_196X_TwoBrothers_flk.010 (001.010)</ta>
            <ta e="T66" id="Seg_938" s="T60">PVD_196X_TwoBrothers_flk.011 (001.011)</ta>
            <ta e="T74" id="Seg_939" s="T66">PVD_196X_TwoBrothers_flk.012 (001.012)</ta>
            <ta e="T83" id="Seg_940" s="T74">PVD_196X_TwoBrothers_flk.013 (001.013)</ta>
            <ta e="T88" id="Seg_941" s="T83">PVD_196X_TwoBrothers_flk.014 (001.014)</ta>
            <ta e="T96" id="Seg_942" s="T88">PVD_196X_TwoBrothers_flk.015 (001.015)</ta>
            <ta e="T103" id="Seg_943" s="T96">PVD_196X_TwoBrothers_flk.016 (001.016)</ta>
            <ta e="T107" id="Seg_944" s="T103">PVD_196X_TwoBrothers_flk.017 (001.017)</ta>
            <ta e="T113" id="Seg_945" s="T107">PVD_196X_TwoBrothers_flk.018 (001.018)</ta>
            <ta e="T119" id="Seg_946" s="T113">PVD_196X_TwoBrothers_flk.019 (001.019)</ta>
            <ta e="T122" id="Seg_947" s="T119">PVD_196X_TwoBrothers_flk.020 (001.020)</ta>
            <ta e="T125" id="Seg_948" s="T122">PVD_196X_TwoBrothers_flk.021 (001.021)</ta>
            <ta e="T129" id="Seg_949" s="T125">PVD_196X_TwoBrothers_flk.022 (001.022)</ta>
            <ta e="T134" id="Seg_950" s="T129">PVD_196X_TwoBrothers_flk.023 (001.023)</ta>
            <ta e="T140" id="Seg_951" s="T134">PVD_196X_TwoBrothers_flk.024 (001.024)</ta>
            <ta e="T146" id="Seg_952" s="T140">PVD_196X_TwoBrothers_flk.025 (001.025)</ta>
            <ta e="T153" id="Seg_953" s="T146">PVD_196X_TwoBrothers_flk.026 (001.026)</ta>
            <ta e="T156" id="Seg_954" s="T153">PVD_196X_TwoBrothers_flk.027 (001.027)</ta>
            <ta e="T160" id="Seg_955" s="T156">PVD_196X_TwoBrothers_flk.028 (001.028)</ta>
            <ta e="T164" id="Seg_956" s="T160">PVD_196X_TwoBrothers_flk.029 (001.029)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_957" s="T1">съ‵да′газыɣан′дʼӓт тʼӓ′птӓ.</ta>
            <ta e="T8" id="Seg_958" s="T4">ме′нан ′jес съ′да(ъ) а′газыɣъ(ӓ).</ta>
            <ta e="T13" id="Seg_959" s="T8">о′ккырын ме ′ӱ̄бай нʼӱр′ты ′по̄но.</ta>
            <ta e="T18" id="Seg_960" s="T13">пом па′дʼӓннай и ом′дай ‵kwасkуlджу′ɣу.</ta>
            <ta e="T27" id="Seg_961" s="T18">ман а′гаw тʼӓ′рын: ′тʼе̄ɣъ бъ ′нʼӓим мʼӓɣу′нӓ ме′не̨ттъ бъ.</ta>
            <ta e="T33" id="Seg_962" s="T27">ме бъ ′аугъди пер′гет вес ам′нӓй.</ta>
            <ta e="T37" id="Seg_963" s="T33">тан а′га ′ме̄гак ‵манныбы′не̨т.</ta>
            <ta e="T49" id="Seg_964" s="T37">тӓп ке′нныт, и ′тʼеɣа нʼӓй ′тша̄жит, ′кы(ӓ)ɣънди ′пӱкгыйбы′лʼе ′тша̄джит и меɣу′ни ′тӱ̄а.</ta>
            <ta e="T54" id="Seg_965" s="T49">ме ом′дай и ам′най ′тӓбым.</ta>
            <ta e="T60" id="Seg_966" s="T54">а′гаw тʼӓ′рын: а′ракай тшо′ргым бъ тӓперʼ.</ta>
            <ta e="T66" id="Seg_967" s="T60">ме бы тӓбым вес бъ ӱ′ннӓй.</ta>
            <ta e="T74" id="Seg_968" s="T66">от тан бе ′мекга ‵манныбы′нӓт, ′мазым кут′дӓр ′се̄рба.</ta>
            <ta e="T83" id="Seg_969" s="T74">тӓп ке′дӓдʼит и ′kӓɣынды ′тшоргъ ′пӱкгы(о)йбылʼе ′тша̄дж(ж)ин, ′меɣуни ′тша̄жын.</ta>
            <ta e="T88" id="Seg_970" s="T83">ме ныт′дын маl′тшай тшор′ɣоккам, ′нʼӱɣуннай.</ta>
            <ta e="T96" id="Seg_971" s="T88">а′гат ′ай тʼӓ′рын: от ме аур′най и kабы′жай.</ta>
            <ta e="T103" id="Seg_972" s="T96">се′часбъ ′сурум е′разе па′росатшугу, лы′лаw бъ ′петкыгу.</ta>
            <ta e="T107" id="Seg_973" s="T103">тан бъ ма′зым манныбы′не̨̄т.</ta>
            <ta e="T113" id="Seg_974" s="T107">а′гат толʼко тʼӓ′рын, нʼӱр′ɣын ′кыжиблʼе ′ӱ̄бъран.</ta>
            <ta e="T119" id="Seg_975" s="T113">мʼӓɣу′не ′тша̄джин ′сӯрӓм е′ра ′тшобырым тша′ɣоптугу.</ta>
            <ta e="T122" id="Seg_976" s="T119">мезу′ви петкул′гу кы′гын.</ta>
            <ta e="T125" id="Seg_977" s="T122">ма′нан сы(ъ)т си′дʼеу.</ta>
            <ta e="T129" id="Seg_978" s="T125">оккыр си′дел kулуп′ба: о′ралбет!</ta>
            <ta e="T134" id="Seg_979" s="T129">а съдъм′деттъ си′дел тʼа′рын: kу′ролгъ.</ta>
            <ta e="T140" id="Seg_980" s="T134">ман ӱгуl′джан на си′деɣын што kу′ролге.</ta>
            <ta e="T146" id="Seg_981" s="T140">как ′ӱ̄бъдитдат и ма′зым ′толʼко колʼ′дӓттъ.</ta>
            <ta e="T153" id="Seg_982" s="T146">ман съдъм′дʼеттъ дʼел ′па̄раннан манны′быгу, ′кудъ ′лаптӓдитда.</ta>
            <ta e="T156" id="Seg_983" s="T153">ман ′тӱан, ‵манны′поу.</ta>
            <ta e="T160" id="Seg_984" s="T156">а′гаунан ′кыбелʼдʼика ла′гашка ′kалыммыт.</ta>
            <ta e="T164" id="Seg_985" s="T160">а ′сӯрумнан kай′нӓс kалып′ба.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_986" s="T1">sədagazɨɣandʼät tʼäptä.</ta>
            <ta e="T8" id="Seg_987" s="T4">menan jes səda(ə) agazɨɣə(ä).</ta>
            <ta e="T13" id="Seg_988" s="T8">okkɨrɨn me üːbaj nʼürtɨ poːno.</ta>
            <ta e="T18" id="Seg_989" s="T13">pom padʼännaj i omdaj qwasqulǯuɣu.</ta>
            <ta e="T27" id="Seg_990" s="T18">man agaw tʼärɨn: tʼeːɣə bə nʼäim mʼäɣunä menettə bə.</ta>
            <ta e="T33" id="Seg_991" s="T27">me bə augədi perget wes amnäj.</ta>
            <ta e="T37" id="Seg_992" s="T33">tan aga meːgak mannɨbɨnet.</ta>
            <ta e="T49" id="Seg_993" s="T37">täp kennɨt, i tʼeɣa nʼäj tšaːʒit, kɨ(ä)ɣəndi pükgɨjbɨlʼe tšaːǯit i meɣuni tüːa.</ta>
            <ta e="T54" id="Seg_994" s="T49">me omdaj i amnaj täbɨm.</ta>
            <ta e="T60" id="Seg_995" s="T54">agaw tʼärɨn: arakaj tšorgɨm bə täperʼ.</ta>
            <ta e="T66" id="Seg_996" s="T60">me bɨ täbɨm wes bə ünnäj.</ta>
            <ta e="T74" id="Seg_997" s="T66">ot tan be mekga mannɨbɨnät, mazɨm kutdär seːrba.</ta>
            <ta e="T83" id="Seg_998" s="T74">täp kedädʼit i qäɣɨndɨ tšorgə pükgɨ(o)jbɨlʼe tšaːǯ(ʒ)in, meɣuni tšaːʒɨn.</ta>
            <ta e="T88" id="Seg_999" s="T83">me nɨtdɨn maltšaj tšorɣokkam, nʼüɣunnaj.</ta>
            <ta e="T96" id="Seg_1000" s="T88">agat aj tʼärɨn: ot me aurnaj i qabɨʒaj.</ta>
            <ta e="T103" id="Seg_1001" s="T96">sečasbə surum eraze parosatšugu, lɨlaw bə petkɨgu.</ta>
            <ta e="T107" id="Seg_1002" s="T103">tan bə mazɨm mannɨbɨneːt.</ta>
            <ta e="T113" id="Seg_1003" s="T107">agat tolʼko tʼärɨn, nʼürɣɨn kɨʒiblʼe üːbəran.</ta>
            <ta e="T119" id="Seg_1004" s="T113">mʼäɣune tšaːǯin suːräm era tšobɨrɨm tšaɣoptugu.</ta>
            <ta e="T122" id="Seg_1005" s="T119">mezuwi petkulgu kɨgɨn.</ta>
            <ta e="T125" id="Seg_1006" s="T122">manan sɨ(ə)t sidʼeu.</ta>
            <ta e="T129" id="Seg_1007" s="T125">okkɨr sidel qulupba: oralbet!</ta>
            <ta e="T134" id="Seg_1008" s="T129">a sədəmdettə sidel tʼarɨn: qurolgə.</ta>
            <ta e="T140" id="Seg_1009" s="T134">man ügulǯan na sideɣɨn što qurolge.</ta>
            <ta e="T146" id="Seg_1010" s="T140">kak üːbəditdat i mazɨm tolʼko kolʼdättə.</ta>
            <ta e="T153" id="Seg_1011" s="T146">man sədəmdʼettə dʼel paːrannan mannɨbɨgu, kudə laptäditda.</ta>
            <ta e="T156" id="Seg_1012" s="T153">man tüan, mannɨpou.</ta>
            <ta e="T160" id="Seg_1013" s="T156">agaunan kɨbelʼdʼika lagaška qalɨmmɨt.</ta>
            <ta e="T164" id="Seg_1014" s="T160">a suːrumnan qajnäs qalɨpba.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_1015" s="T1">Səd agazɨɣandʼät tʼäptä. </ta>
            <ta e="T8" id="Seg_1016" s="T4">Menan jes səda agazɨɣə. </ta>
            <ta e="T13" id="Seg_1017" s="T8">Okkɨrɨn me üːbaj nʼürtɨ poːno. </ta>
            <ta e="T18" id="Seg_1018" s="T13">Pom padʼännaj i omdaj qwasqulǯuɣu. </ta>
            <ta e="T27" id="Seg_1019" s="T18">Man agaw tʼärɨn: “Tʼeːɣə bə nʼäim mʼäɣunä menettə bə. </ta>
            <ta e="T33" id="Seg_1020" s="T27">Me bə augədi perget wes amnäj. </ta>
            <ta e="T37" id="Seg_1021" s="T33">Tan aga meːgak mannɨbɨnet.” </ta>
            <ta e="T49" id="Seg_1022" s="T37">Täp kennɨt, i tʼeɣa nʼäj čaːʒit, kɨɣəndi pükgɨjbɨlʼe čaːǯit i meɣuni tüːa. </ta>
            <ta e="T54" id="Seg_1023" s="T49">Me omdaj i amnaj täbɨm. </ta>
            <ta e="T60" id="Seg_1024" s="T54">Agaw tʼärɨn: “Arakaj čorgɨm bə täperʼ. </ta>
            <ta e="T66" id="Seg_1025" s="T60">Me bɨ täbɨm wes bə ünnäj. </ta>
            <ta e="T74" id="Seg_1026" s="T66">Ot tan be mekga mannɨbɨnät, mazɨm kutdär seːrba.” </ta>
            <ta e="T83" id="Seg_1027" s="T74">Täp kedädʼit i qäɣɨndɨ čorgə pükgɨjbɨlʼe čaːǯin, meɣuni čaːʒɨn. </ta>
            <ta e="T88" id="Seg_1028" s="T83">Me nɨtdɨn malčaj čorɣokkam, nʼüɣunnaj. </ta>
            <ta e="T96" id="Seg_1029" s="T88">Agat aj tʼärɨn: “Ot me aurnaj i qabɨʒaj. </ta>
            <ta e="T103" id="Seg_1030" s="T96">Sečasbə surum eraze parosačugu, lɨlaw bə petkɨgu. </ta>
            <ta e="T107" id="Seg_1031" s="T103">Tan bə mazɨm mannɨbɨneːt.” </ta>
            <ta e="T113" id="Seg_1032" s="T107">Agat tolʼko tʼärɨn, nʼürɣɨn kɨʒiblʼe üːbəran. </ta>
            <ta e="T119" id="Seg_1033" s="T113">Mʼäɣune čaːǯin suːräm era čobɨrɨm čaɣoptugu. </ta>
            <ta e="T122" id="Seg_1034" s="T119">Mezuwi petkulgu kɨgɨn. </ta>
            <ta e="T125" id="Seg_1035" s="T122">Manan sɨt sidʼeu. </ta>
            <ta e="T129" id="Seg_1036" s="T125">Okkɨr sidel qulupba: “Oralbet!” </ta>
            <ta e="T134" id="Seg_1037" s="T129">A sədəmdettə sidel tʼarɨn: “Qurolgə.” </ta>
            <ta e="T140" id="Seg_1038" s="T134">Man ügulǯan na sideɣɨn što qurolge. </ta>
            <ta e="T146" id="Seg_1039" s="T140">Kak üːbəditdat i mazɨm tolʼko kolʼdättə. </ta>
            <ta e="T153" id="Seg_1040" s="T146">Man sədəmdʼettə dʼel paːrannan mannɨbɨgu, kudə laptäditda. </ta>
            <ta e="T156" id="Seg_1041" s="T153">Man tüan, mannɨpou. </ta>
            <ta e="T160" id="Seg_1042" s="T156">Agaunan kɨbelʼdʼika lagaška qalɨmmɨt. </ta>
            <ta e="T164" id="Seg_1043" s="T160">A suːrumnan qajnäs qalɨpba. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_1044" s="T1">səd</ta>
            <ta e="T3" id="Seg_1045" s="T2">aga-zɨ-ɣandʼät</ta>
            <ta e="T4" id="Seg_1046" s="T3">tʼäptä</ta>
            <ta e="T5" id="Seg_1047" s="T4">me-nan</ta>
            <ta e="T6" id="Seg_1048" s="T5">je-s</ta>
            <ta e="T7" id="Seg_1049" s="T6">səda</ta>
            <ta e="T8" id="Seg_1050" s="T7">aga-zɨ-ɣə</ta>
            <ta e="T9" id="Seg_1051" s="T8">okkɨr-ɨ-n</ta>
            <ta e="T10" id="Seg_1052" s="T9">me</ta>
            <ta e="T11" id="Seg_1053" s="T10">üːba-j</ta>
            <ta e="T12" id="Seg_1054" s="T11">nʼür-tɨ</ta>
            <ta e="T13" id="Seg_1055" s="T12">poː-no</ta>
            <ta e="T14" id="Seg_1056" s="T13">po-m</ta>
            <ta e="T15" id="Seg_1057" s="T14">padʼän-na-j</ta>
            <ta e="T16" id="Seg_1058" s="T15">i</ta>
            <ta e="T17" id="Seg_1059" s="T16">omda-j</ta>
            <ta e="T18" id="Seg_1060" s="T17">qwasqulǯu-ɣu</ta>
            <ta e="T19" id="Seg_1061" s="T18">man</ta>
            <ta e="T20" id="Seg_1062" s="T19">aga-w</ta>
            <ta e="T21" id="Seg_1063" s="T20">tʼärɨ-n</ta>
            <ta e="T22" id="Seg_1064" s="T21">tʼeːɣə</ta>
            <ta e="T23" id="Seg_1065" s="T22">bə</ta>
            <ta e="T24" id="Seg_1066" s="T23">nʼäi-m</ta>
            <ta e="T25" id="Seg_1067" s="T24">mʼäɣunä</ta>
            <ta e="T26" id="Seg_1068" s="T25">me-ne-ttə</ta>
            <ta e="T27" id="Seg_1069" s="T26">bə</ta>
            <ta e="T28" id="Seg_1070" s="T27">me</ta>
            <ta e="T29" id="Seg_1071" s="T28">bə</ta>
            <ta e="T30" id="Seg_1072" s="T29">au-gədi</ta>
            <ta e="T31" id="Seg_1073" s="T30">perge-t</ta>
            <ta e="T32" id="Seg_1074" s="T31">wes</ta>
            <ta e="T33" id="Seg_1075" s="T32">am-nä-j</ta>
            <ta e="T34" id="Seg_1076" s="T33">tat</ta>
            <ta e="T35" id="Seg_1077" s="T34">aga</ta>
            <ta e="T36" id="Seg_1078" s="T35">meːga-k</ta>
            <ta e="T37" id="Seg_1079" s="T36">mannɨ-bɨ-ne-t</ta>
            <ta e="T38" id="Seg_1080" s="T37">täp</ta>
            <ta e="T39" id="Seg_1081" s="T38">ken-nɨ-t</ta>
            <ta e="T40" id="Seg_1082" s="T39">i</ta>
            <ta e="T41" id="Seg_1083" s="T40">tʼeɣa</ta>
            <ta e="T42" id="Seg_1084" s="T41">nʼäj</ta>
            <ta e="T43" id="Seg_1085" s="T42">čaːʒi-t</ta>
            <ta e="T44" id="Seg_1086" s="T43">kɨ-ɣəndi</ta>
            <ta e="T45" id="Seg_1087" s="T44">pükgɨj-bɨ-lʼe</ta>
            <ta e="T46" id="Seg_1088" s="T45">čaːǯi-t</ta>
            <ta e="T47" id="Seg_1089" s="T46">i</ta>
            <ta e="T48" id="Seg_1090" s="T47">meɣuni</ta>
            <ta e="T49" id="Seg_1091" s="T48">tüː-a</ta>
            <ta e="T50" id="Seg_1092" s="T49">me</ta>
            <ta e="T51" id="Seg_1093" s="T50">omda-j</ta>
            <ta e="T52" id="Seg_1094" s="T51">i</ta>
            <ta e="T53" id="Seg_1095" s="T52">am-na-j</ta>
            <ta e="T54" id="Seg_1096" s="T53">täb-ɨ-m</ta>
            <ta e="T55" id="Seg_1097" s="T54">aga-w</ta>
            <ta e="T56" id="Seg_1098" s="T55">tʼärɨ-n</ta>
            <ta e="T57" id="Seg_1099" s="T56">araka-j</ta>
            <ta e="T58" id="Seg_1100" s="T57">čorg-ɨ-m</ta>
            <ta e="T59" id="Seg_1101" s="T58">bə</ta>
            <ta e="T60" id="Seg_1102" s="T59">täperʼ</ta>
            <ta e="T61" id="Seg_1103" s="T60">me</ta>
            <ta e="T62" id="Seg_1104" s="T61">bɨ</ta>
            <ta e="T63" id="Seg_1105" s="T62">täb-ɨ-m</ta>
            <ta e="T64" id="Seg_1106" s="T63">wes</ta>
            <ta e="T65" id="Seg_1107" s="T64">bə</ta>
            <ta e="T66" id="Seg_1108" s="T65">ün-nä-j</ta>
            <ta e="T67" id="Seg_1109" s="T66">ot</ta>
            <ta e="T68" id="Seg_1110" s="T67">tat</ta>
            <ta e="T69" id="Seg_1111" s="T68">be</ta>
            <ta e="T70" id="Seg_1112" s="T69">mekga</ta>
            <ta e="T71" id="Seg_1113" s="T70">mannɨ-bɨ-nä-t</ta>
            <ta e="T72" id="Seg_1114" s="T71">mazɨm</ta>
            <ta e="T73" id="Seg_1115" s="T72">kutdär</ta>
            <ta e="T74" id="Seg_1116" s="T73">seːr-ba</ta>
            <ta e="T75" id="Seg_1117" s="T74">täp</ta>
            <ta e="T76" id="Seg_1118" s="T75">ked-ä-dʼi-t</ta>
            <ta e="T77" id="Seg_1119" s="T76">i</ta>
            <ta e="T78" id="Seg_1120" s="T77">qä-ɣɨndɨ</ta>
            <ta e="T79" id="Seg_1121" s="T78">čorgə</ta>
            <ta e="T80" id="Seg_1122" s="T79">pükgɨj-bɨ-lʼe</ta>
            <ta e="T81" id="Seg_1123" s="T80">čaːǯi-n</ta>
            <ta e="T82" id="Seg_1124" s="T81">meɣuni</ta>
            <ta e="T83" id="Seg_1125" s="T82">čaːʒɨ-n</ta>
            <ta e="T84" id="Seg_1126" s="T83">me</ta>
            <ta e="T85" id="Seg_1127" s="T84">nɨtdɨ-n</ta>
            <ta e="T86" id="Seg_1128" s="T85">malča-j</ta>
            <ta e="T87" id="Seg_1129" s="T86">čorɣ-o-kka-m</ta>
            <ta e="T88" id="Seg_1130" s="T87">nʼüɣun-na-j</ta>
            <ta e="T89" id="Seg_1131" s="T88">aga-t</ta>
            <ta e="T90" id="Seg_1132" s="T89">aj</ta>
            <ta e="T91" id="Seg_1133" s="T90">tʼärɨ-n</ta>
            <ta e="T92" id="Seg_1134" s="T91">ot</ta>
            <ta e="T93" id="Seg_1135" s="T92">me</ta>
            <ta e="T94" id="Seg_1136" s="T93">au-r-na-j</ta>
            <ta e="T95" id="Seg_1137" s="T94">i</ta>
            <ta e="T96" id="Seg_1138" s="T95">qabɨʒa-j</ta>
            <ta e="T97" id="Seg_1139" s="T96">sečas-bə</ta>
            <ta e="T98" id="Seg_1140" s="T97">surum</ta>
            <ta e="T99" id="Seg_1141" s="T98">era-ze</ta>
            <ta e="T100" id="Seg_1142" s="T99">parosa-ču-gu</ta>
            <ta e="T101" id="Seg_1143" s="T100">lɨ-la-w</ta>
            <ta e="T102" id="Seg_1144" s="T101">bə</ta>
            <ta e="T103" id="Seg_1145" s="T102">petkɨ-gu</ta>
            <ta e="T104" id="Seg_1146" s="T103">tat</ta>
            <ta e="T105" id="Seg_1147" s="T104">bə</ta>
            <ta e="T106" id="Seg_1148" s="T105">mazɨm</ta>
            <ta e="T107" id="Seg_1149" s="T106">mannɨ-bɨ-neː-t</ta>
            <ta e="T108" id="Seg_1150" s="T107">aga-t</ta>
            <ta e="T109" id="Seg_1151" s="T108">tolʼko</ta>
            <ta e="T110" id="Seg_1152" s="T109">tʼärɨ-n</ta>
            <ta e="T111" id="Seg_1153" s="T110">nʼür-ɣɨn</ta>
            <ta e="T112" id="Seg_1154" s="T111">kɨʒib-lʼe</ta>
            <ta e="T113" id="Seg_1155" s="T112">üːbə-r-a-n</ta>
            <ta e="T114" id="Seg_1156" s="T113">mʼäɣune</ta>
            <ta e="T115" id="Seg_1157" s="T114">čaːǯi-n</ta>
            <ta e="T116" id="Seg_1158" s="T115">suːräm</ta>
            <ta e="T117" id="Seg_1159" s="T116">era</ta>
            <ta e="T118" id="Seg_1160" s="T117">čobɨr-ɨ-m</ta>
            <ta e="T119" id="Seg_1161" s="T118">čaɣo-ptu-gu</ta>
            <ta e="T120" id="Seg_1162" s="T119">mezuwi</ta>
            <ta e="T121" id="Seg_1163" s="T120">petkul-gu</ta>
            <ta e="T122" id="Seg_1164" s="T121">kɨgɨ-n</ta>
            <ta e="T123" id="Seg_1165" s="T122">ma-nan</ta>
            <ta e="T124" id="Seg_1166" s="T123">sɨt</ta>
            <ta e="T125" id="Seg_1167" s="T124">sidʼe-u</ta>
            <ta e="T126" id="Seg_1168" s="T125">okkɨr</ta>
            <ta e="T127" id="Seg_1169" s="T126">side-l</ta>
            <ta e="T128" id="Seg_1170" s="T127">qulupba</ta>
            <ta e="T129" id="Seg_1171" s="T128">oral-b-et</ta>
            <ta e="T130" id="Seg_1172" s="T129">a</ta>
            <ta e="T131" id="Seg_1173" s="T130">sədə-mdettə</ta>
            <ta e="T132" id="Seg_1174" s="T131">side-l</ta>
            <ta e="T133" id="Seg_1175" s="T132">tʼarɨ-n</ta>
            <ta e="T134" id="Seg_1176" s="T133">qur-ol-gə</ta>
            <ta e="T135" id="Seg_1177" s="T134">man</ta>
            <ta e="T136" id="Seg_1178" s="T135">ügulǯa-n</ta>
            <ta e="T137" id="Seg_1179" s="T136">na</ta>
            <ta e="T138" id="Seg_1180" s="T137">side-ɣɨn</ta>
            <ta e="T139" id="Seg_1181" s="T138">što</ta>
            <ta e="T140" id="Seg_1182" s="T139">qur-ol-ge</ta>
            <ta e="T141" id="Seg_1183" s="T140">kak</ta>
            <ta e="T142" id="Seg_1184" s="T141">üːbə-di-tda-t</ta>
            <ta e="T143" id="Seg_1185" s="T142">i</ta>
            <ta e="T144" id="Seg_1186" s="T143">mazɨm</ta>
            <ta e="T145" id="Seg_1187" s="T144">tolʼko</ta>
            <ta e="T146" id="Seg_1188" s="T145">ko-lʼdä-ttə</ta>
            <ta e="T147" id="Seg_1189" s="T146">man</ta>
            <ta e="T148" id="Seg_1190" s="T147">sədə-mdʼettə</ta>
            <ta e="T149" id="Seg_1191" s="T148">dʼel</ta>
            <ta e="T150" id="Seg_1192" s="T149">paːran-na-n</ta>
            <ta e="T151" id="Seg_1193" s="T150">mannɨ-bɨ-gu</ta>
            <ta e="T152" id="Seg_1194" s="T151">kudə</ta>
            <ta e="T153" id="Seg_1195" s="T152">laptä-di-tda</ta>
            <ta e="T154" id="Seg_1196" s="T153">man</ta>
            <ta e="T155" id="Seg_1197" s="T154">tü-a-n</ta>
            <ta e="T156" id="Seg_1198" s="T155">mannɨ-po-u</ta>
            <ta e="T157" id="Seg_1199" s="T156">aga-u-nan</ta>
            <ta e="T158" id="Seg_1200" s="T157">kɨbelʼdʼika</ta>
            <ta e="T159" id="Seg_1201" s="T158">laga-š-ka</ta>
            <ta e="T160" id="Seg_1202" s="T159">qalɨ-mmɨ-t</ta>
            <ta e="T161" id="Seg_1203" s="T160">a</ta>
            <ta e="T162" id="Seg_1204" s="T161">suːrum-nan</ta>
            <ta e="T163" id="Seg_1205" s="T162">qaj-nä-s</ta>
            <ta e="T164" id="Seg_1206" s="T163">qalɨ-pba</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_1207" s="T1">sədə</ta>
            <ta e="T3" id="Seg_1208" s="T2">agaː-sɨ-qandɨt</ta>
            <ta e="T4" id="Seg_1209" s="T3">tʼäptä</ta>
            <ta e="T5" id="Seg_1210" s="T4">me-nan</ta>
            <ta e="T6" id="Seg_1211" s="T5">eː-sɨ</ta>
            <ta e="T7" id="Seg_1212" s="T6">sədə</ta>
            <ta e="T8" id="Seg_1213" s="T7">agaː-sɨ-qi</ta>
            <ta e="T9" id="Seg_1214" s="T8">okkɨr-ɨ-n</ta>
            <ta e="T10" id="Seg_1215" s="T9">me</ta>
            <ta e="T11" id="Seg_1216" s="T10">übɨ-j</ta>
            <ta e="T12" id="Seg_1217" s="T11">nür-ntə</ta>
            <ta e="T13" id="Seg_1218" s="T12">po-no</ta>
            <ta e="T14" id="Seg_1219" s="T13">po-m</ta>
            <ta e="T15" id="Seg_1220" s="T14">padʼal-nɨ-j</ta>
            <ta e="T16" id="Seg_1221" s="T15">i</ta>
            <ta e="T17" id="Seg_1222" s="T16">omdɨ-j</ta>
            <ta e="T18" id="Seg_1223" s="T17">qwasqɨlʼǯɨ-gu</ta>
            <ta e="T19" id="Seg_1224" s="T18">man</ta>
            <ta e="T20" id="Seg_1225" s="T19">agaː-w</ta>
            <ta e="T21" id="Seg_1226" s="T20">tʼärɨ-n</ta>
            <ta e="T22" id="Seg_1227" s="T21">tʼeɣə</ta>
            <ta e="T23" id="Seg_1228" s="T22">bɨ</ta>
            <ta e="T24" id="Seg_1229" s="T23">nʼäj-m</ta>
            <ta e="T25" id="Seg_1230" s="T24">meguni</ta>
            <ta e="T26" id="Seg_1231" s="T25">me-ne-tɨn</ta>
            <ta e="T27" id="Seg_1232" s="T26">bɨ</ta>
            <ta e="T28" id="Seg_1233" s="T27">me</ta>
            <ta e="T29" id="Seg_1234" s="T28">bɨ</ta>
            <ta e="T30" id="Seg_1235" s="T29">ab-gɨdi</ta>
            <ta e="T31" id="Seg_1236" s="T30">pärgə-tə</ta>
            <ta e="T32" id="Seg_1237" s="T31">wesʼ</ta>
            <ta e="T33" id="Seg_1238" s="T32">am-ne-j</ta>
            <ta e="T34" id="Seg_1239" s="T33">tan</ta>
            <ta e="T35" id="Seg_1240" s="T34">agaː</ta>
            <ta e="T36" id="Seg_1241" s="T35">mekka-k</ta>
            <ta e="T37" id="Seg_1242" s="T36">*mantɨ-mbɨ-ne-t</ta>
            <ta e="T38" id="Seg_1243" s="T37">täp</ta>
            <ta e="T39" id="Seg_1244" s="T38">ket-nɨ-t</ta>
            <ta e="T40" id="Seg_1245" s="T39">i</ta>
            <ta e="T41" id="Seg_1246" s="T40">tʼeɣə</ta>
            <ta e="T42" id="Seg_1247" s="T41">nʼäj</ta>
            <ta e="T43" id="Seg_1248" s="T42">čaǯɨ-ntɨ</ta>
            <ta e="T44" id="Seg_1249" s="T43">qä-qɨntɨ</ta>
            <ta e="T45" id="Seg_1250" s="T44">pükgɨj-mbɨ-le</ta>
            <ta e="T46" id="Seg_1251" s="T45">čaǯɨ-ntɨ</ta>
            <ta e="T47" id="Seg_1252" s="T46">i</ta>
            <ta e="T48" id="Seg_1253" s="T47">meguni</ta>
            <ta e="T49" id="Seg_1254" s="T48">tüː-nɨ</ta>
            <ta e="T50" id="Seg_1255" s="T49">me</ta>
            <ta e="T51" id="Seg_1256" s="T50">omdɨ-j</ta>
            <ta e="T52" id="Seg_1257" s="T51">i</ta>
            <ta e="T53" id="Seg_1258" s="T52">am-nɨ-j</ta>
            <ta e="T54" id="Seg_1259" s="T53">täp-ɨ-m</ta>
            <ta e="T55" id="Seg_1260" s="T54">agaː-w</ta>
            <ta e="T56" id="Seg_1261" s="T55">tʼärɨ-n</ta>
            <ta e="T57" id="Seg_1262" s="T56">araŋka-lʼ</ta>
            <ta e="T58" id="Seg_1263" s="T57">čorg-ɨ-m</ta>
            <ta e="T59" id="Seg_1264" s="T58">bɨ</ta>
            <ta e="T60" id="Seg_1265" s="T59">teper</ta>
            <ta e="T61" id="Seg_1266" s="T60">me</ta>
            <ta e="T62" id="Seg_1267" s="T61">bɨ</ta>
            <ta e="T63" id="Seg_1268" s="T62">täp-ɨ-m</ta>
            <ta e="T64" id="Seg_1269" s="T63">wesʼ</ta>
            <ta e="T65" id="Seg_1270" s="T64">bɨ</ta>
            <ta e="T66" id="Seg_1271" s="T65">üt-ne-j</ta>
            <ta e="T67" id="Seg_1272" s="T66">ot</ta>
            <ta e="T68" id="Seg_1273" s="T67">tan</ta>
            <ta e="T69" id="Seg_1274" s="T68">bɨ</ta>
            <ta e="T70" id="Seg_1275" s="T69">mekka</ta>
            <ta e="T71" id="Seg_1276" s="T70">*mantɨ-mbɨ-ne-ntə</ta>
            <ta e="T72" id="Seg_1277" s="T71">mazɨm</ta>
            <ta e="T73" id="Seg_1278" s="T72">qundar</ta>
            <ta e="T74" id="Seg_1279" s="T73">ser-mbɨ</ta>
            <ta e="T75" id="Seg_1280" s="T74">täp</ta>
            <ta e="T76" id="Seg_1281" s="T75">ket-ɨ-dʼi-t</ta>
            <ta e="T77" id="Seg_1282" s="T76">i</ta>
            <ta e="T78" id="Seg_1283" s="T77">qä-qɨntɨ</ta>
            <ta e="T79" id="Seg_1284" s="T78">čorg</ta>
            <ta e="T80" id="Seg_1285" s="T79">pükgɨj-mbɨ-le</ta>
            <ta e="T81" id="Seg_1286" s="T80">čaǯɨ-n</ta>
            <ta e="T82" id="Seg_1287" s="T81">meguni</ta>
            <ta e="T83" id="Seg_1288" s="T82">čaǯɨ-n</ta>
            <ta e="T84" id="Seg_1289" s="T83">me</ta>
            <ta e="T85" id="Seg_1290" s="T84">*natʼe-n</ta>
            <ta e="T86" id="Seg_1291" s="T85">malčə-j</ta>
            <ta e="T87" id="Seg_1292" s="T86">čorg-ɨ-ka-m</ta>
            <ta e="T88" id="Seg_1293" s="T87">nʼügul-nɨ-j</ta>
            <ta e="T89" id="Seg_1294" s="T88">agaː-tə</ta>
            <ta e="T90" id="Seg_1295" s="T89">aj</ta>
            <ta e="T91" id="Seg_1296" s="T90">tʼärɨ-n</ta>
            <ta e="T92" id="Seg_1297" s="T91">ot</ta>
            <ta e="T93" id="Seg_1298" s="T92">me</ta>
            <ta e="T94" id="Seg_1299" s="T93">ab-r-nɨ-j</ta>
            <ta e="T95" id="Seg_1300" s="T94">i</ta>
            <ta e="T96" id="Seg_1301" s="T95">qabuʒu-j</ta>
            <ta e="T97" id="Seg_1302" s="T96">sičas-bɨ</ta>
            <ta e="T98" id="Seg_1303" s="T97">suːrum</ta>
            <ta e="T99" id="Seg_1304" s="T98">era-se</ta>
            <ta e="T100" id="Seg_1305" s="T99">parosa-ču-gu</ta>
            <ta e="T101" id="Seg_1306" s="T100">lɨ-la-w</ta>
            <ta e="T102" id="Seg_1307" s="T101">bɨ</ta>
            <ta e="T103" id="Seg_1308" s="T102">petkɨl-gu</ta>
            <ta e="T104" id="Seg_1309" s="T103">tan</ta>
            <ta e="T105" id="Seg_1310" s="T104">bɨ</ta>
            <ta e="T106" id="Seg_1311" s="T105">mazɨm</ta>
            <ta e="T107" id="Seg_1312" s="T106">*mantɨ-mbɨ-ne-ntə</ta>
            <ta e="T108" id="Seg_1313" s="T107">agaː-tə</ta>
            <ta e="T109" id="Seg_1314" s="T108">tolʼko</ta>
            <ta e="T110" id="Seg_1315" s="T109">tʼärɨ-n</ta>
            <ta e="T111" id="Seg_1316" s="T110">nür-qɨn</ta>
            <ta e="T112" id="Seg_1317" s="T111">kɨʒɨbu-le</ta>
            <ta e="T113" id="Seg_1318" s="T112">übɨ-r-nɨ-n</ta>
            <ta e="T114" id="Seg_1319" s="T113">meguni</ta>
            <ta e="T115" id="Seg_1320" s="T114">čaǯɨ-n</ta>
            <ta e="T116" id="Seg_1321" s="T115">suːrum</ta>
            <ta e="T117" id="Seg_1322" s="T116">era</ta>
            <ta e="T118" id="Seg_1323" s="T117">čobər-ɨ-m</ta>
            <ta e="T119" id="Seg_1324" s="T118">čagɨ-ptɨ-gu</ta>
            <ta e="T120" id="Seg_1325" s="T119">mezuwi</ta>
            <ta e="T121" id="Seg_1326" s="T120">petkɨl-gu</ta>
            <ta e="T122" id="Seg_1327" s="T121">kɨgɨ-n</ta>
            <ta e="T123" id="Seg_1328" s="T122">man-nan</ta>
            <ta e="T124" id="Seg_1329" s="T123">sədə</ta>
            <ta e="T125" id="Seg_1330" s="T124">sidʼe-w</ta>
            <ta e="T126" id="Seg_1331" s="T125">okkɨr</ta>
            <ta e="T127" id="Seg_1332" s="T126">sidʼe-l</ta>
            <ta e="T128" id="Seg_1333" s="T127">kulubu</ta>
            <ta e="T129" id="Seg_1334" s="T128">oral-mbɨ-etɨ</ta>
            <ta e="T130" id="Seg_1335" s="T129">a</ta>
            <ta e="T131" id="Seg_1336" s="T130">sədə-mǯʼeːli</ta>
            <ta e="T132" id="Seg_1337" s="T131">sidʼe-l</ta>
            <ta e="T133" id="Seg_1338" s="T132">tʼärɨ-n</ta>
            <ta e="T134" id="Seg_1339" s="T133">kur-ol-kɨ</ta>
            <ta e="T135" id="Seg_1340" s="T134">man</ta>
            <ta e="T136" id="Seg_1341" s="T135">üŋgulǯu-ŋ</ta>
            <ta e="T137" id="Seg_1342" s="T136">na</ta>
            <ta e="T138" id="Seg_1343" s="T137">sidʼe-qɨn</ta>
            <ta e="T139" id="Seg_1344" s="T138">što</ta>
            <ta e="T140" id="Seg_1345" s="T139">kur-ol-kɨ</ta>
            <ta e="T141" id="Seg_1346" s="T140">kak</ta>
            <ta e="T142" id="Seg_1347" s="T141">übɨ-dʼi-ntɨ-t</ta>
            <ta e="T143" id="Seg_1348" s="T142">i</ta>
            <ta e="T144" id="Seg_1349" s="T143">mazɨm</ta>
            <ta e="T145" id="Seg_1350" s="T144">tolʼko</ta>
            <ta e="T146" id="Seg_1351" s="T145">qo-lǯi-tɨn</ta>
            <ta e="T147" id="Seg_1352" s="T146">man</ta>
            <ta e="T148" id="Seg_1353" s="T147">sədə-mtätte</ta>
            <ta e="T149" id="Seg_1354" s="T148">dʼel</ta>
            <ta e="T150" id="Seg_1355" s="T149">paral-nɨ-ŋ</ta>
            <ta e="T151" id="Seg_1356" s="T150">*mantɨ-mbɨ-gu</ta>
            <ta e="T152" id="Seg_1357" s="T151">kud</ta>
            <ta e="T153" id="Seg_1358" s="T152">laptə-dʼi-ntɨ</ta>
            <ta e="T154" id="Seg_1359" s="T153">man</ta>
            <ta e="T155" id="Seg_1360" s="T154">tüː-nɨ-n</ta>
            <ta e="T156" id="Seg_1361" s="T155">*mantɨ-mbɨ-w</ta>
            <ta e="T157" id="Seg_1362" s="T156">agaː-w-nan</ta>
            <ta e="T158" id="Seg_1363" s="T157">kɨbɨlʼdʼiga</ta>
            <ta e="T159" id="Seg_1364" s="T158">laga-š-ka</ta>
            <ta e="T160" id="Seg_1365" s="T159">qalɨ-mbɨ-ntɨ</ta>
            <ta e="T161" id="Seg_1366" s="T160">a</ta>
            <ta e="T162" id="Seg_1367" s="T161">suːrum-nan</ta>
            <ta e="T163" id="Seg_1368" s="T162">qaj-näj-s</ta>
            <ta e="T164" id="Seg_1369" s="T163">qalɨ-mbɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_1370" s="T1">two</ta>
            <ta e="T3" id="Seg_1371" s="T2">brother-DYA-ALL.3PL</ta>
            <ta e="T4" id="Seg_1372" s="T3">tale.[NOM]</ta>
            <ta e="T5" id="Seg_1373" s="T4">we-ADES</ta>
            <ta e="T6" id="Seg_1374" s="T5">be-PST.[3SG.S]</ta>
            <ta e="T7" id="Seg_1375" s="T6">two</ta>
            <ta e="T8" id="Seg_1376" s="T7">brother-DYA-DU.[NOM]</ta>
            <ta e="T9" id="Seg_1377" s="T8">one-EP-ADV.LOC</ta>
            <ta e="T10" id="Seg_1378" s="T9">we.[NOM]</ta>
            <ta e="T11" id="Seg_1379" s="T10">set.off-1DU</ta>
            <ta e="T12" id="Seg_1380" s="T11">field-ILL</ta>
            <ta e="T13" id="Seg_1381" s="T12">wood-TRL</ta>
            <ta e="T14" id="Seg_1382" s="T13">wood-ACC</ta>
            <ta e="T15" id="Seg_1383" s="T14">chop.off-CO-1DU</ta>
            <ta e="T16" id="Seg_1384" s="T15">and</ta>
            <ta e="T17" id="Seg_1385" s="T16">sit.down-1DU</ta>
            <ta e="T18" id="Seg_1386" s="T17">rest-INF</ta>
            <ta e="T19" id="Seg_1387" s="T18">I.GEN</ta>
            <ta e="T20" id="Seg_1388" s="T19">brother.[NOM]-1SG</ta>
            <ta e="T21" id="Seg_1389" s="T20">say-3SG.S</ta>
            <ta e="T22" id="Seg_1390" s="T21">white</ta>
            <ta e="T23" id="Seg_1391" s="T22">IRREAL</ta>
            <ta e="T24" id="Seg_1392" s="T23">bread-ACC</ta>
            <ta e="T25" id="Seg_1393" s="T24">we.DU.ALL</ta>
            <ta e="T26" id="Seg_1394" s="T25">give-CONJ-3PL</ta>
            <ta e="T27" id="Seg_1395" s="T26">IRREAL</ta>
            <ta e="T28" id="Seg_1396" s="T27">we.[NOM]</ta>
            <ta e="T29" id="Seg_1397" s="T28">IRREAL</ta>
            <ta e="T30" id="Seg_1398" s="T29">food-CAR</ta>
            <ta e="T31" id="Seg_1399" s="T30">stomach.[NOM]-3SG</ta>
            <ta e="T32" id="Seg_1400" s="T31">all</ta>
            <ta e="T33" id="Seg_1401" s="T32">eat-CONJ-1DU</ta>
            <ta e="T34" id="Seg_1402" s="T33">you.SG.NOM</ta>
            <ta e="T35" id="Seg_1403" s="T34">brother.[NOM]</ta>
            <ta e="T36" id="Seg_1404" s="T35">I.ALL-%%</ta>
            <ta e="T37" id="Seg_1405" s="T36">look-DUR-CONJ-3SG.O</ta>
            <ta e="T38" id="Seg_1406" s="T37">(s)he.[NOM]</ta>
            <ta e="T39" id="Seg_1407" s="T38">say-CO-3SG.O</ta>
            <ta e="T40" id="Seg_1408" s="T39">and</ta>
            <ta e="T41" id="Seg_1409" s="T40">white</ta>
            <ta e="T42" id="Seg_1410" s="T41">bread.[NOM]</ta>
            <ta e="T43" id="Seg_1411" s="T42">go-INFER.[NOM]</ta>
            <ta e="T44" id="Seg_1412" s="T43">hill-EL.3SG</ta>
            <ta e="T45" id="Seg_1413" s="T44">%roll-DUR-CVB</ta>
            <ta e="T46" id="Seg_1414" s="T45">go-INFER.[3SG.S]</ta>
            <ta e="T47" id="Seg_1415" s="T46">and</ta>
            <ta e="T48" id="Seg_1416" s="T47">we.DU.ALL</ta>
            <ta e="T49" id="Seg_1417" s="T48">come-CO.[3SG.S]</ta>
            <ta e="T50" id="Seg_1418" s="T49">we.[NOM]</ta>
            <ta e="T51" id="Seg_1419" s="T50">sit.down-1DU</ta>
            <ta e="T52" id="Seg_1420" s="T51">and</ta>
            <ta e="T53" id="Seg_1421" s="T52">eat-CO-1DU</ta>
            <ta e="T54" id="Seg_1422" s="T53">(s)he-EP-ACC</ta>
            <ta e="T55" id="Seg_1423" s="T54">brother.[NOM]-1SG</ta>
            <ta e="T56" id="Seg_1424" s="T55">say-3SG.S</ta>
            <ta e="T57" id="Seg_1425" s="T56">wine-ADJZ</ta>
            <ta e="T58" id="Seg_1426" s="T57">bottle-EP-ACC</ta>
            <ta e="T59" id="Seg_1427" s="T58">IRREAL</ta>
            <ta e="T60" id="Seg_1428" s="T59">now</ta>
            <ta e="T61" id="Seg_1429" s="T60">we.[NOM]</ta>
            <ta e="T62" id="Seg_1430" s="T61">IRREAL</ta>
            <ta e="T63" id="Seg_1431" s="T62">(s)he-EP-ACC</ta>
            <ta e="T64" id="Seg_1432" s="T63">all</ta>
            <ta e="T65" id="Seg_1433" s="T64">IRREAL</ta>
            <ta e="T66" id="Seg_1434" s="T65">drink-CONJ-1DU</ta>
            <ta e="T67" id="Seg_1435" s="T66">look.here</ta>
            <ta e="T68" id="Seg_1436" s="T67">you.SG.NOM</ta>
            <ta e="T69" id="Seg_1437" s="T68">IRREAL</ta>
            <ta e="T70" id="Seg_1438" s="T69">I.ALL</ta>
            <ta e="T71" id="Seg_1439" s="T70">look-DUR-CONJ-2SG.S</ta>
            <ta e="T72" id="Seg_1440" s="T71">I.ACC</ta>
            <ta e="T73" id="Seg_1441" s="T72">how</ta>
            <ta e="T74" id="Seg_1442" s="T73">put.on-RES.[3SG.S]</ta>
            <ta e="T75" id="Seg_1443" s="T74">(s)he.[NOM]</ta>
            <ta e="T76" id="Seg_1444" s="T75">say-EP-DRV-3SG.O</ta>
            <ta e="T77" id="Seg_1445" s="T76">and</ta>
            <ta e="T78" id="Seg_1446" s="T77">hill-EL.3SG</ta>
            <ta e="T79" id="Seg_1447" s="T78">bottle.[NOM]</ta>
            <ta e="T80" id="Seg_1448" s="T79">%roll-DUR-CVB</ta>
            <ta e="T81" id="Seg_1449" s="T80">go-3SG.S</ta>
            <ta e="T82" id="Seg_1450" s="T81">we.DU.ALL</ta>
            <ta e="T83" id="Seg_1451" s="T82">go-3SG.S</ta>
            <ta e="T84" id="Seg_1452" s="T83">we.[NOM]</ta>
            <ta e="T85" id="Seg_1453" s="T84">there-ADV.LOC</ta>
            <ta e="T86" id="Seg_1454" s="T85">stop-1DU</ta>
            <ta e="T87" id="Seg_1455" s="T86">bottle-EP-DIM-ACC</ta>
            <ta e="T88" id="Seg_1456" s="T87">lick.clean-CO-1DU</ta>
            <ta e="T89" id="Seg_1457" s="T88">brother.[NOM]-3SG</ta>
            <ta e="T90" id="Seg_1458" s="T89">again</ta>
            <ta e="T91" id="Seg_1459" s="T90">say-3SG.S</ta>
            <ta e="T92" id="Seg_1460" s="T91">look.here</ta>
            <ta e="T93" id="Seg_1461" s="T92">we.[NOM]</ta>
            <ta e="T94" id="Seg_1462" s="T93">food-VBLZ-CO-1DU</ta>
            <ta e="T95" id="Seg_1463" s="T94">and</ta>
            <ta e="T96" id="Seg_1464" s="T95">eat.one's.fill-1DU</ta>
            <ta e="T97" id="Seg_1465" s="T96">now-IRREAL</ta>
            <ta e="T98" id="Seg_1466" s="T97">wild.animal.[NOM]</ta>
            <ta e="T99" id="Seg_1467" s="T98">old.man-COM</ta>
            <ta e="T100" id="Seg_1468" s="T99">struggle-TR-INF</ta>
            <ta e="T101" id="Seg_1469" s="T100">bone-PL.[NOM]-1SG</ta>
            <ta e="T102" id="Seg_1470" s="T101">IRREAL</ta>
            <ta e="T103" id="Seg_1471" s="T102">press-INF</ta>
            <ta e="T104" id="Seg_1472" s="T103">you.SG.NOM</ta>
            <ta e="T105" id="Seg_1473" s="T104">IRREAL</ta>
            <ta e="T106" id="Seg_1474" s="T105">I.ACC</ta>
            <ta e="T107" id="Seg_1475" s="T106">look-DUR-CONJ-2SG.S</ta>
            <ta e="T108" id="Seg_1476" s="T107">brother.[NOM]-3SG</ta>
            <ta e="T109" id="Seg_1477" s="T108">only</ta>
            <ta e="T110" id="Seg_1478" s="T109">say-3SG.S</ta>
            <ta e="T111" id="Seg_1479" s="T110">field-LOC</ta>
            <ta e="T112" id="Seg_1480" s="T111">crack-CVB</ta>
            <ta e="T113" id="Seg_1481" s="T112">begin-FRQ-CO-3SG.S</ta>
            <ta e="T114" id="Seg_1482" s="T113">we.DU.ALL</ta>
            <ta e="T115" id="Seg_1483" s="T114">go-3SG.S</ta>
            <ta e="T116" id="Seg_1484" s="T115">wild.animal.[NOM]</ta>
            <ta e="T117" id="Seg_1485" s="T116">old.man.[NOM]</ta>
            <ta e="T118" id="Seg_1486" s="T117">berry-EP-ACC</ta>
            <ta e="T119" id="Seg_1487" s="T118">dry-CAUS-INF</ta>
            <ta e="T120" id="Seg_1488" s="T119">we.ACC</ta>
            <ta e="T121" id="Seg_1489" s="T120">press-INF</ta>
            <ta e="T122" id="Seg_1490" s="T121">want-3SG.S</ta>
            <ta e="T123" id="Seg_1491" s="T122">I-ADES</ta>
            <ta e="T124" id="Seg_1492" s="T123">two</ta>
            <ta e="T125" id="Seg_1493" s="T124">heart.[NOM]-1SG</ta>
            <ta e="T126" id="Seg_1494" s="T125">one</ta>
            <ta e="T127" id="Seg_1495" s="T126">heart.[NOM]-2SG</ta>
            <ta e="T128" id="Seg_1496" s="T127">speak</ta>
            <ta e="T129" id="Seg_1497" s="T128">take-DUR-IMP.2SG.O</ta>
            <ta e="T130" id="Seg_1498" s="T129">and</ta>
            <ta e="T131" id="Seg_1499" s="T130">two-ORD</ta>
            <ta e="T132" id="Seg_1500" s="T131">heart.[NOM]-2SG</ta>
            <ta e="T133" id="Seg_1501" s="T132">say-3SG.S</ta>
            <ta e="T134" id="Seg_1502" s="T133">run-MOM-IMP.2SG.S</ta>
            <ta e="T135" id="Seg_1503" s="T134">I.NOM</ta>
            <ta e="T136" id="Seg_1504" s="T135">listen.to-1SG.S</ta>
            <ta e="T137" id="Seg_1505" s="T136">this</ta>
            <ta e="T138" id="Seg_1506" s="T137">heart-LOC</ta>
            <ta e="T139" id="Seg_1507" s="T138">that</ta>
            <ta e="T140" id="Seg_1508" s="T139">run-MOM-IMP.2SG.S</ta>
            <ta e="T141" id="Seg_1509" s="T140">suddenly</ta>
            <ta e="T142" id="Seg_1510" s="T141">begin-DRV-INFER-%%</ta>
            <ta e="T143" id="Seg_1511" s="T142">and</ta>
            <ta e="T144" id="Seg_1512" s="T143">I.ACC</ta>
            <ta e="T145" id="Seg_1513" s="T144">only</ta>
            <ta e="T146" id="Seg_1514" s="T145">see-PFV-3PL</ta>
            <ta e="T147" id="Seg_1515" s="T146">I.NOM</ta>
            <ta e="T148" id="Seg_1516" s="T147">two-ORD</ta>
            <ta e="T149" id="Seg_1517" s="T148">day.[NOM]</ta>
            <ta e="T150" id="Seg_1518" s="T149">return-CO-1SG.S</ta>
            <ta e="T151" id="Seg_1519" s="T150">look-DUR-INF</ta>
            <ta e="T152" id="Seg_1520" s="T151">who.[NOM]</ta>
            <ta e="T153" id="Seg_1521" s="T152">beat-DRV-INFER.[3SG.S]</ta>
            <ta e="T154" id="Seg_1522" s="T153">I.NOM</ta>
            <ta e="T155" id="Seg_1523" s="T154">come-CO-3SG.S</ta>
            <ta e="T156" id="Seg_1524" s="T155">look-DUR-1SG.O</ta>
            <ta e="T157" id="Seg_1525" s="T156">brother-1SG-ADES</ta>
            <ta e="T158" id="Seg_1526" s="T157">small</ta>
            <ta e="T159" id="Seg_1527" s="T158">flock-%%-DIM.[NOM]</ta>
            <ta e="T160" id="Seg_1528" s="T159">stay-PST.NAR-INFER.[NOM]</ta>
            <ta e="T161" id="Seg_1529" s="T160">and</ta>
            <ta e="T162" id="Seg_1530" s="T161">wild.animal-ADES</ta>
            <ta e="T163" id="Seg_1531" s="T162">what.[NOM]-EMPH-NEG</ta>
            <ta e="T164" id="Seg_1532" s="T163">stay-PST.NAR.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_1533" s="T1">два</ta>
            <ta e="T3" id="Seg_1534" s="T2">брат-DYA-ALL.3PL</ta>
            <ta e="T4" id="Seg_1535" s="T3">сказка.[NOM]</ta>
            <ta e="T5" id="Seg_1536" s="T4">мы-ADES</ta>
            <ta e="T6" id="Seg_1537" s="T5">быть-PST.[3SG.S]</ta>
            <ta e="T7" id="Seg_1538" s="T6">два</ta>
            <ta e="T8" id="Seg_1539" s="T7">брат-DYA-DU.[NOM]</ta>
            <ta e="T9" id="Seg_1540" s="T8">один-EP-ADV.LOC</ta>
            <ta e="T10" id="Seg_1541" s="T9">мы.[NOM]</ta>
            <ta e="T11" id="Seg_1542" s="T10">отправиться-1DU</ta>
            <ta e="T12" id="Seg_1543" s="T11">поле-ILL</ta>
            <ta e="T13" id="Seg_1544" s="T12">дрова-TRL</ta>
            <ta e="T14" id="Seg_1545" s="T13">дрова-ACC</ta>
            <ta e="T15" id="Seg_1546" s="T14">срубить-CO-1DU</ta>
            <ta e="T16" id="Seg_1547" s="T15">и</ta>
            <ta e="T17" id="Seg_1548" s="T16">сесть-1DU</ta>
            <ta e="T18" id="Seg_1549" s="T17">отдохнуть-INF</ta>
            <ta e="T19" id="Seg_1550" s="T18">я.GEN</ta>
            <ta e="T20" id="Seg_1551" s="T19">брат.[NOM]-1SG</ta>
            <ta e="T21" id="Seg_1552" s="T20">сказать-3SG.S</ta>
            <ta e="T22" id="Seg_1553" s="T21">белый</ta>
            <ta e="T23" id="Seg_1554" s="T22">IRREAL</ta>
            <ta e="T24" id="Seg_1555" s="T23">хлеб-ACC</ta>
            <ta e="T25" id="Seg_1556" s="T24">мы.DU.ALL</ta>
            <ta e="T26" id="Seg_1557" s="T25">дать-CONJ-3PL</ta>
            <ta e="T27" id="Seg_1558" s="T26">IRREAL</ta>
            <ta e="T28" id="Seg_1559" s="T27">мы.[NOM]</ta>
            <ta e="T29" id="Seg_1560" s="T28">IRREAL</ta>
            <ta e="T30" id="Seg_1561" s="T29">еда-CAR</ta>
            <ta e="T31" id="Seg_1562" s="T30">желудок.[NOM]-3SG</ta>
            <ta e="T32" id="Seg_1563" s="T31">весь</ta>
            <ta e="T33" id="Seg_1564" s="T32">съесть-CONJ-1DU</ta>
            <ta e="T34" id="Seg_1565" s="T33">ты.NOM</ta>
            <ta e="T35" id="Seg_1566" s="T34">брат.[NOM]</ta>
            <ta e="T36" id="Seg_1567" s="T35">я.ALL-%%</ta>
            <ta e="T37" id="Seg_1568" s="T36">посмотреть-DUR-CONJ-3SG.O</ta>
            <ta e="T38" id="Seg_1569" s="T37">он(а).[NOM]</ta>
            <ta e="T39" id="Seg_1570" s="T38">сказать-CO-3SG.O</ta>
            <ta e="T40" id="Seg_1571" s="T39">и</ta>
            <ta e="T41" id="Seg_1572" s="T40">белый</ta>
            <ta e="T42" id="Seg_1573" s="T41">хлеб.[NOM]</ta>
            <ta e="T43" id="Seg_1574" s="T42">ходить-INFER.[NOM]</ta>
            <ta e="T44" id="Seg_1575" s="T43">гора-EL.3SG</ta>
            <ta e="T45" id="Seg_1576" s="T44">%катиться-DUR-CVB</ta>
            <ta e="T46" id="Seg_1577" s="T45">ходить-INFER.[3SG.S]</ta>
            <ta e="T47" id="Seg_1578" s="T46">и</ta>
            <ta e="T48" id="Seg_1579" s="T47">мы.DU.ALL</ta>
            <ta e="T49" id="Seg_1580" s="T48">прийти-CO.[3SG.S]</ta>
            <ta e="T50" id="Seg_1581" s="T49">мы.[NOM]</ta>
            <ta e="T51" id="Seg_1582" s="T50">сесть-1DU</ta>
            <ta e="T52" id="Seg_1583" s="T51">и</ta>
            <ta e="T53" id="Seg_1584" s="T52">съесть-CO-1DU</ta>
            <ta e="T54" id="Seg_1585" s="T53">он(а)-EP-ACC</ta>
            <ta e="T55" id="Seg_1586" s="T54">брат.[NOM]-1SG</ta>
            <ta e="T56" id="Seg_1587" s="T55">сказать-3SG.S</ta>
            <ta e="T57" id="Seg_1588" s="T56">вино-ADJZ</ta>
            <ta e="T58" id="Seg_1589" s="T57">бутылка-EP-ACC</ta>
            <ta e="T59" id="Seg_1590" s="T58">IRREAL</ta>
            <ta e="T60" id="Seg_1591" s="T59">теперь</ta>
            <ta e="T61" id="Seg_1592" s="T60">мы.[NOM]</ta>
            <ta e="T62" id="Seg_1593" s="T61">IRREAL</ta>
            <ta e="T63" id="Seg_1594" s="T62">он(а)-EP-ACC</ta>
            <ta e="T64" id="Seg_1595" s="T63">весь</ta>
            <ta e="T65" id="Seg_1596" s="T64">IRREAL</ta>
            <ta e="T66" id="Seg_1597" s="T65">пить-CONJ-1DU</ta>
            <ta e="T67" id="Seg_1598" s="T66">вот</ta>
            <ta e="T68" id="Seg_1599" s="T67">ты.NOM</ta>
            <ta e="T69" id="Seg_1600" s="T68">IRREAL</ta>
            <ta e="T70" id="Seg_1601" s="T69">я.ALL</ta>
            <ta e="T71" id="Seg_1602" s="T70">посмотреть-DUR-CONJ-2SG.S</ta>
            <ta e="T72" id="Seg_1603" s="T71">я.ACC</ta>
            <ta e="T73" id="Seg_1604" s="T72">как</ta>
            <ta e="T74" id="Seg_1605" s="T73">надеть-RES.[3SG.S]</ta>
            <ta e="T75" id="Seg_1606" s="T74">он(а).[NOM]</ta>
            <ta e="T76" id="Seg_1607" s="T75">сказать-EP-DRV-3SG.O</ta>
            <ta e="T77" id="Seg_1608" s="T76">и</ta>
            <ta e="T78" id="Seg_1609" s="T77">гора-EL.3SG</ta>
            <ta e="T79" id="Seg_1610" s="T78">бутылка.[NOM]</ta>
            <ta e="T80" id="Seg_1611" s="T79">%катиться-DUR-CVB</ta>
            <ta e="T81" id="Seg_1612" s="T80">ходить-3SG.S</ta>
            <ta e="T82" id="Seg_1613" s="T81">мы.DU.ALL</ta>
            <ta e="T83" id="Seg_1614" s="T82">ходить-3SG.S</ta>
            <ta e="T84" id="Seg_1615" s="T83">мы.[NOM]</ta>
            <ta e="T85" id="Seg_1616" s="T84">туда-ADV.LOC</ta>
            <ta e="T86" id="Seg_1617" s="T85">закончить-1DU</ta>
            <ta e="T87" id="Seg_1618" s="T86">бутылка-EP-DIM-ACC</ta>
            <ta e="T88" id="Seg_1619" s="T87">вылизать-CO-1DU</ta>
            <ta e="T89" id="Seg_1620" s="T88">брат.[NOM]-3SG</ta>
            <ta e="T90" id="Seg_1621" s="T89">опять</ta>
            <ta e="T91" id="Seg_1622" s="T90">сказать-3SG.S</ta>
            <ta e="T92" id="Seg_1623" s="T91">вот</ta>
            <ta e="T93" id="Seg_1624" s="T92">мы.[NOM]</ta>
            <ta e="T94" id="Seg_1625" s="T93">еда-VBLZ-CO-1DU</ta>
            <ta e="T95" id="Seg_1626" s="T94">и</ta>
            <ta e="T96" id="Seg_1627" s="T95">насытиться-1DU</ta>
            <ta e="T97" id="Seg_1628" s="T96">сейчас-IRREAL</ta>
            <ta e="T98" id="Seg_1629" s="T97">зверь.[NOM]</ta>
            <ta e="T99" id="Seg_1630" s="T98">старик-COM</ta>
            <ta e="T100" id="Seg_1631" s="T99">бороться-TR-INF</ta>
            <ta e="T101" id="Seg_1632" s="T100">кость-PL.[NOM]-1SG</ta>
            <ta e="T102" id="Seg_1633" s="T101">IRREAL</ta>
            <ta e="T103" id="Seg_1634" s="T102">помять-INF</ta>
            <ta e="T104" id="Seg_1635" s="T103">ты.NOM</ta>
            <ta e="T105" id="Seg_1636" s="T104">IRREAL</ta>
            <ta e="T106" id="Seg_1637" s="T105">я.ACC</ta>
            <ta e="T107" id="Seg_1638" s="T106">посмотреть-DUR-CONJ-2SG.S</ta>
            <ta e="T108" id="Seg_1639" s="T107">брат.[NOM]-3SG</ta>
            <ta e="T109" id="Seg_1640" s="T108">только</ta>
            <ta e="T110" id="Seg_1641" s="T109">сказать-3SG.S</ta>
            <ta e="T111" id="Seg_1642" s="T110">поле-LOC</ta>
            <ta e="T112" id="Seg_1643" s="T111">трещать-CVB</ta>
            <ta e="T113" id="Seg_1644" s="T112">начать-FRQ-CO-3SG.S</ta>
            <ta e="T114" id="Seg_1645" s="T113">мы.DU.ALL</ta>
            <ta e="T115" id="Seg_1646" s="T114">ходить-3SG.S</ta>
            <ta e="T116" id="Seg_1647" s="T115">зверь.[NOM]</ta>
            <ta e="T117" id="Seg_1648" s="T116">старик.[NOM]</ta>
            <ta e="T118" id="Seg_1649" s="T117">ягода-EP-ACC</ta>
            <ta e="T119" id="Seg_1650" s="T118">высохнуть-CAUS-INF</ta>
            <ta e="T120" id="Seg_1651" s="T119">мы.ACC</ta>
            <ta e="T121" id="Seg_1652" s="T120">помять-INF</ta>
            <ta e="T122" id="Seg_1653" s="T121">хотеть-3SG.S</ta>
            <ta e="T123" id="Seg_1654" s="T122">я-ADES</ta>
            <ta e="T124" id="Seg_1655" s="T123">два</ta>
            <ta e="T125" id="Seg_1656" s="T124">сердце.[NOM]-1SG</ta>
            <ta e="T126" id="Seg_1657" s="T125">один</ta>
            <ta e="T127" id="Seg_1658" s="T126">сердце.[NOM]-2SG</ta>
            <ta e="T128" id="Seg_1659" s="T127">говорить</ta>
            <ta e="T129" id="Seg_1660" s="T128">взять-DUR-IMP.2SG.O</ta>
            <ta e="T130" id="Seg_1661" s="T129">а</ta>
            <ta e="T131" id="Seg_1662" s="T130">два-ORD</ta>
            <ta e="T132" id="Seg_1663" s="T131">сердце.[NOM]-2SG</ta>
            <ta e="T133" id="Seg_1664" s="T132">сказать-3SG.S</ta>
            <ta e="T134" id="Seg_1665" s="T133">бегать-MOM-IMP.2SG.S</ta>
            <ta e="T135" id="Seg_1666" s="T134">я.NOM</ta>
            <ta e="T136" id="Seg_1667" s="T135">послушаться-1SG.S</ta>
            <ta e="T137" id="Seg_1668" s="T136">этот</ta>
            <ta e="T138" id="Seg_1669" s="T137">сердце-LOC</ta>
            <ta e="T139" id="Seg_1670" s="T138">что</ta>
            <ta e="T140" id="Seg_1671" s="T139">бегать-MOM-IMP.2SG.S</ta>
            <ta e="T141" id="Seg_1672" s="T140">как</ta>
            <ta e="T142" id="Seg_1673" s="T141">начать-DRV-INFER-%%</ta>
            <ta e="T143" id="Seg_1674" s="T142">и</ta>
            <ta e="T144" id="Seg_1675" s="T143">я.ACC</ta>
            <ta e="T145" id="Seg_1676" s="T144">только</ta>
            <ta e="T146" id="Seg_1677" s="T145">увидеть-PFV-3PL</ta>
            <ta e="T147" id="Seg_1678" s="T146">я.NOM</ta>
            <ta e="T148" id="Seg_1679" s="T147">два-ORD</ta>
            <ta e="T149" id="Seg_1680" s="T148">день.[NOM]</ta>
            <ta e="T150" id="Seg_1681" s="T149">вернуться-CO-1SG.S</ta>
            <ta e="T151" id="Seg_1682" s="T150">посмотреть-DUR-INF</ta>
            <ta e="T152" id="Seg_1683" s="T151">кто.[NOM]</ta>
            <ta e="T153" id="Seg_1684" s="T152">побороть-DRV-INFER.[3SG.S]</ta>
            <ta e="T154" id="Seg_1685" s="T153">я.NOM</ta>
            <ta e="T155" id="Seg_1686" s="T154">прийти-CO-3SG.S</ta>
            <ta e="T156" id="Seg_1687" s="T155">посмотреть-DUR-1SG.O</ta>
            <ta e="T157" id="Seg_1688" s="T156">брат-1SG-ADES</ta>
            <ta e="T158" id="Seg_1689" s="T157">маленький</ta>
            <ta e="T159" id="Seg_1690" s="T158">клок-%%-DIM.[NOM]</ta>
            <ta e="T160" id="Seg_1691" s="T159">остаться-PST.NAR-INFER.[NOM]</ta>
            <ta e="T161" id="Seg_1692" s="T160">а</ta>
            <ta e="T162" id="Seg_1693" s="T161">зверь-ADES</ta>
            <ta e="T163" id="Seg_1694" s="T162">что.[NOM]-EMPH-NEG</ta>
            <ta e="T164" id="Seg_1695" s="T163">остаться-PST.NAR.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1696" s="T1">num</ta>
            <ta e="T3" id="Seg_1697" s="T2">n-n&gt;n-n:case.poss</ta>
            <ta e="T4" id="Seg_1698" s="T3">n.[n:case]</ta>
            <ta e="T5" id="Seg_1699" s="T4">pers-n:case</ta>
            <ta e="T6" id="Seg_1700" s="T5">v-v:tense.[v:pn]</ta>
            <ta e="T7" id="Seg_1701" s="T6">num</ta>
            <ta e="T8" id="Seg_1702" s="T7">n-n&gt;n-n:num.[n:case]</ta>
            <ta e="T9" id="Seg_1703" s="T8">num-n:ins-n&gt;adv</ta>
            <ta e="T10" id="Seg_1704" s="T9">pers.[n:case]</ta>
            <ta e="T11" id="Seg_1705" s="T10">v-v:pn</ta>
            <ta e="T12" id="Seg_1706" s="T11">n-n:case</ta>
            <ta e="T13" id="Seg_1707" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_1708" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_1709" s="T14">v-v:ins-v:pn</ta>
            <ta e="T16" id="Seg_1710" s="T15">conj</ta>
            <ta e="T17" id="Seg_1711" s="T16">v-v:pn</ta>
            <ta e="T18" id="Seg_1712" s="T17">v-v:inf</ta>
            <ta e="T19" id="Seg_1713" s="T18">pers</ta>
            <ta e="T20" id="Seg_1714" s="T19">n.[n:case]-n:poss</ta>
            <ta e="T21" id="Seg_1715" s="T20">v-v:pn</ta>
            <ta e="T22" id="Seg_1716" s="T21">adj</ta>
            <ta e="T23" id="Seg_1717" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_1718" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_1719" s="T24">pers</ta>
            <ta e="T26" id="Seg_1720" s="T25">v-v:mood-v:pn</ta>
            <ta e="T27" id="Seg_1721" s="T26">ptcl</ta>
            <ta e="T28" id="Seg_1722" s="T27">pers.[n:case]</ta>
            <ta e="T29" id="Seg_1723" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_1724" s="T29">n-n&gt;adj</ta>
            <ta e="T31" id="Seg_1725" s="T30">n.[n:case]-n:poss</ta>
            <ta e="T32" id="Seg_1726" s="T31">quant</ta>
            <ta e="T33" id="Seg_1727" s="T32">v-v:mood-v:pn</ta>
            <ta e="T34" id="Seg_1728" s="T33">pers</ta>
            <ta e="T35" id="Seg_1729" s="T34">n.[n:case]</ta>
            <ta e="T36" id="Seg_1730" s="T35">pers-%%</ta>
            <ta e="T37" id="Seg_1731" s="T36">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T38" id="Seg_1732" s="T37">pers.[n:case]</ta>
            <ta e="T39" id="Seg_1733" s="T38">v-v:ins-v:pn</ta>
            <ta e="T40" id="Seg_1734" s="T39">conj</ta>
            <ta e="T41" id="Seg_1735" s="T40">adj</ta>
            <ta e="T42" id="Seg_1736" s="T41">n.[n:case]</ta>
            <ta e="T43" id="Seg_1737" s="T42">v-v:mood.[n:case]</ta>
            <ta e="T44" id="Seg_1738" s="T43">n-n:case.poss</ta>
            <ta e="T45" id="Seg_1739" s="T44">v-v&gt;v-v&gt;adv</ta>
            <ta e="T46" id="Seg_1740" s="T45">v-v:mood.[v:pn]</ta>
            <ta e="T47" id="Seg_1741" s="T46">conj</ta>
            <ta e="T48" id="Seg_1742" s="T47">pers</ta>
            <ta e="T49" id="Seg_1743" s="T48">v-v:ins.[v:pn]</ta>
            <ta e="T50" id="Seg_1744" s="T49">pers.[n:case]</ta>
            <ta e="T51" id="Seg_1745" s="T50">v-v:pn</ta>
            <ta e="T52" id="Seg_1746" s="T51">conj</ta>
            <ta e="T53" id="Seg_1747" s="T52">v-v:ins-v:pn</ta>
            <ta e="T54" id="Seg_1748" s="T53">pers-n:ins-n:case</ta>
            <ta e="T55" id="Seg_1749" s="T54">n.[n:case]-n:poss</ta>
            <ta e="T56" id="Seg_1750" s="T55">v-v:pn</ta>
            <ta e="T57" id="Seg_1751" s="T56">n-n&gt;adj</ta>
            <ta e="T58" id="Seg_1752" s="T57">n-n:ins-n:case</ta>
            <ta e="T59" id="Seg_1753" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_1754" s="T59">adv</ta>
            <ta e="T61" id="Seg_1755" s="T60">pers.[n:case]</ta>
            <ta e="T62" id="Seg_1756" s="T61">ptcl</ta>
            <ta e="T63" id="Seg_1757" s="T62">pers-n:ins-n:case</ta>
            <ta e="T64" id="Seg_1758" s="T63">quant</ta>
            <ta e="T65" id="Seg_1759" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_1760" s="T65">v-v:mood-v:pn</ta>
            <ta e="T67" id="Seg_1761" s="T66">interj</ta>
            <ta e="T68" id="Seg_1762" s="T67">pers</ta>
            <ta e="T69" id="Seg_1763" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_1764" s="T69">pers</ta>
            <ta e="T71" id="Seg_1765" s="T70">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T72" id="Seg_1766" s="T71">pers</ta>
            <ta e="T73" id="Seg_1767" s="T72">conj</ta>
            <ta e="T74" id="Seg_1768" s="T73">v-v&gt;v.[v:pn]</ta>
            <ta e="T75" id="Seg_1769" s="T74">pers.[n:case]</ta>
            <ta e="T76" id="Seg_1770" s="T75">v-v:ins-v&gt;v-v:pn</ta>
            <ta e="T77" id="Seg_1771" s="T76">conj</ta>
            <ta e="T78" id="Seg_1772" s="T77">n-n:case.poss</ta>
            <ta e="T79" id="Seg_1773" s="T78">n.[n:case]</ta>
            <ta e="T80" id="Seg_1774" s="T79">v-v&gt;v-v&gt;adv</ta>
            <ta e="T81" id="Seg_1775" s="T80">v-v:pn</ta>
            <ta e="T82" id="Seg_1776" s="T81">pers</ta>
            <ta e="T83" id="Seg_1777" s="T82">v-v:pn</ta>
            <ta e="T84" id="Seg_1778" s="T83">pers.[n:case]</ta>
            <ta e="T85" id="Seg_1779" s="T84">adv-adv:case</ta>
            <ta e="T86" id="Seg_1780" s="T85">v-v:pn</ta>
            <ta e="T87" id="Seg_1781" s="T86">n-n:ins-n&gt;n-n:case</ta>
            <ta e="T88" id="Seg_1782" s="T87">v-v:ins-v:pn</ta>
            <ta e="T89" id="Seg_1783" s="T88">n.[n:case]-n:poss</ta>
            <ta e="T90" id="Seg_1784" s="T89">adv</ta>
            <ta e="T91" id="Seg_1785" s="T90">v-v:pn</ta>
            <ta e="T92" id="Seg_1786" s="T91">interj</ta>
            <ta e="T93" id="Seg_1787" s="T92">pers.[n:case]</ta>
            <ta e="T94" id="Seg_1788" s="T93">n-n&gt;v-v:ins-v:pn</ta>
            <ta e="T95" id="Seg_1789" s="T94">conj</ta>
            <ta e="T96" id="Seg_1790" s="T95">v-v:pn</ta>
            <ta e="T97" id="Seg_1791" s="T96">adv-ptcl</ta>
            <ta e="T98" id="Seg_1792" s="T97">n.[n:case]</ta>
            <ta e="T99" id="Seg_1793" s="T98">n-n:case</ta>
            <ta e="T100" id="Seg_1794" s="T99">v-v&gt;v-v:inf</ta>
            <ta e="T101" id="Seg_1795" s="T100">n-n:num.[n:case]-n:poss</ta>
            <ta e="T102" id="Seg_1796" s="T101">ptcl</ta>
            <ta e="T103" id="Seg_1797" s="T102">v-v:inf</ta>
            <ta e="T104" id="Seg_1798" s="T103">pers</ta>
            <ta e="T105" id="Seg_1799" s="T104">ptcl</ta>
            <ta e="T106" id="Seg_1800" s="T105">pers</ta>
            <ta e="T107" id="Seg_1801" s="T106">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T108" id="Seg_1802" s="T107">n.[n:case]-n:poss</ta>
            <ta e="T109" id="Seg_1803" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_1804" s="T109">v-v:pn</ta>
            <ta e="T111" id="Seg_1805" s="T110">n-n:case</ta>
            <ta e="T112" id="Seg_1806" s="T111">v-v&gt;adv</ta>
            <ta e="T113" id="Seg_1807" s="T112">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T114" id="Seg_1808" s="T113">pers</ta>
            <ta e="T115" id="Seg_1809" s="T114">v-v:pn</ta>
            <ta e="T116" id="Seg_1810" s="T115">n.[n:case]</ta>
            <ta e="T117" id="Seg_1811" s="T116">n.[n:case]</ta>
            <ta e="T118" id="Seg_1812" s="T117">n-n:ins-n:case</ta>
            <ta e="T119" id="Seg_1813" s="T118">v-v&gt;v-v:inf</ta>
            <ta e="T120" id="Seg_1814" s="T119">pers</ta>
            <ta e="T121" id="Seg_1815" s="T120">v-v:inf</ta>
            <ta e="T122" id="Seg_1816" s="T121">v-v:pn</ta>
            <ta e="T123" id="Seg_1817" s="T122">pers-n:case</ta>
            <ta e="T124" id="Seg_1818" s="T123">num</ta>
            <ta e="T125" id="Seg_1819" s="T124">n.[n:case]-n:poss</ta>
            <ta e="T126" id="Seg_1820" s="T125">num</ta>
            <ta e="T127" id="Seg_1821" s="T126">n.[n:case]-n:poss</ta>
            <ta e="T128" id="Seg_1822" s="T127">v</ta>
            <ta e="T129" id="Seg_1823" s="T128">v-v&gt;v-v:mood.pn</ta>
            <ta e="T130" id="Seg_1824" s="T129">conj</ta>
            <ta e="T131" id="Seg_1825" s="T130">num-num&gt;adj</ta>
            <ta e="T132" id="Seg_1826" s="T131">n.[n:case]-n:poss</ta>
            <ta e="T133" id="Seg_1827" s="T132">v-v:pn</ta>
            <ta e="T134" id="Seg_1828" s="T133">v-v&gt;v-v:mood.pn</ta>
            <ta e="T135" id="Seg_1829" s="T134">pers</ta>
            <ta e="T136" id="Seg_1830" s="T135">v-v:pn</ta>
            <ta e="T137" id="Seg_1831" s="T136">dem</ta>
            <ta e="T138" id="Seg_1832" s="T137">n-n:case</ta>
            <ta e="T139" id="Seg_1833" s="T138">conj</ta>
            <ta e="T140" id="Seg_1834" s="T139">v-v&gt;v-v:mood.pn</ta>
            <ta e="T141" id="Seg_1835" s="T140">adv</ta>
            <ta e="T142" id="Seg_1836" s="T141">v-v&gt;v-v:mood-%%</ta>
            <ta e="T143" id="Seg_1837" s="T142">conj</ta>
            <ta e="T144" id="Seg_1838" s="T143">pers</ta>
            <ta e="T145" id="Seg_1839" s="T144">ptcl</ta>
            <ta e="T146" id="Seg_1840" s="T145">v-v&gt;v-v:pn</ta>
            <ta e="T147" id="Seg_1841" s="T146">pers</ta>
            <ta e="T148" id="Seg_1842" s="T147">num-num&gt;adj</ta>
            <ta e="T149" id="Seg_1843" s="T148">n.[n:case]</ta>
            <ta e="T150" id="Seg_1844" s="T149">v-v:ins-v:pn</ta>
            <ta e="T151" id="Seg_1845" s="T150">v-v&gt;v-v:inf</ta>
            <ta e="T152" id="Seg_1846" s="T151">interrog.[n:case]</ta>
            <ta e="T153" id="Seg_1847" s="T152">v-v&gt;v-v:mood.[v:pn]</ta>
            <ta e="T154" id="Seg_1848" s="T153">pers</ta>
            <ta e="T155" id="Seg_1849" s="T154">v-v:ins-v:pn</ta>
            <ta e="T156" id="Seg_1850" s="T155">v-v&gt;v-v:pn</ta>
            <ta e="T157" id="Seg_1851" s="T156">n-n:poss-n:case</ta>
            <ta e="T158" id="Seg_1852" s="T157">adj</ta>
            <ta e="T159" id="Seg_1853" s="T158">n-n&gt;n-n&gt;n.[n:case]</ta>
            <ta e="T160" id="Seg_1854" s="T159">v-v:tense-v:mood.[n:case]</ta>
            <ta e="T161" id="Seg_1855" s="T160">conj</ta>
            <ta e="T162" id="Seg_1856" s="T161">n-n:case</ta>
            <ta e="T163" id="Seg_1857" s="T162">interrog.[n:case]-clit-clit</ta>
            <ta e="T164" id="Seg_1858" s="T163">v-v:tense.[v:pn]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1859" s="T1">num</ta>
            <ta e="T3" id="Seg_1860" s="T2">n</ta>
            <ta e="T4" id="Seg_1861" s="T3">n</ta>
            <ta e="T5" id="Seg_1862" s="T4">pers</ta>
            <ta e="T6" id="Seg_1863" s="T5">v</ta>
            <ta e="T7" id="Seg_1864" s="T6">num</ta>
            <ta e="T8" id="Seg_1865" s="T7">n</ta>
            <ta e="T9" id="Seg_1866" s="T8">adv</ta>
            <ta e="T10" id="Seg_1867" s="T9">pers</ta>
            <ta e="T11" id="Seg_1868" s="T10">adj</ta>
            <ta e="T12" id="Seg_1869" s="T11">n</ta>
            <ta e="T13" id="Seg_1870" s="T12">n</ta>
            <ta e="T14" id="Seg_1871" s="T13">n</ta>
            <ta e="T15" id="Seg_1872" s="T14">v</ta>
            <ta e="T16" id="Seg_1873" s="T15">conj</ta>
            <ta e="T17" id="Seg_1874" s="T16">v</ta>
            <ta e="T18" id="Seg_1875" s="T17">v</ta>
            <ta e="T19" id="Seg_1876" s="T18">pers</ta>
            <ta e="T20" id="Seg_1877" s="T19">n</ta>
            <ta e="T21" id="Seg_1878" s="T20">v</ta>
            <ta e="T22" id="Seg_1879" s="T21">adj</ta>
            <ta e="T23" id="Seg_1880" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_1881" s="T23">n</ta>
            <ta e="T25" id="Seg_1882" s="T24">pers</ta>
            <ta e="T26" id="Seg_1883" s="T25">v</ta>
            <ta e="T27" id="Seg_1884" s="T26">ptcl</ta>
            <ta e="T28" id="Seg_1885" s="T27">pers</ta>
            <ta e="T29" id="Seg_1886" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_1887" s="T29">adj</ta>
            <ta e="T31" id="Seg_1888" s="T30">n</ta>
            <ta e="T32" id="Seg_1889" s="T31">quant</ta>
            <ta e="T33" id="Seg_1890" s="T32">v</ta>
            <ta e="T34" id="Seg_1891" s="T33">pers</ta>
            <ta e="T35" id="Seg_1892" s="T34">n</ta>
            <ta e="T36" id="Seg_1893" s="T35">pers</ta>
            <ta e="T37" id="Seg_1894" s="T36">v</ta>
            <ta e="T38" id="Seg_1895" s="T37">pers</ta>
            <ta e="T39" id="Seg_1896" s="T38">v</ta>
            <ta e="T40" id="Seg_1897" s="T39">conj</ta>
            <ta e="T41" id="Seg_1898" s="T40">adj</ta>
            <ta e="T42" id="Seg_1899" s="T41">n</ta>
            <ta e="T43" id="Seg_1900" s="T42">v</ta>
            <ta e="T44" id="Seg_1901" s="T43">n</ta>
            <ta e="T45" id="Seg_1902" s="T44">adv</ta>
            <ta e="T46" id="Seg_1903" s="T45">v</ta>
            <ta e="T47" id="Seg_1904" s="T46">conj</ta>
            <ta e="T48" id="Seg_1905" s="T47">pers</ta>
            <ta e="T49" id="Seg_1906" s="T48">v</ta>
            <ta e="T50" id="Seg_1907" s="T49">pers</ta>
            <ta e="T51" id="Seg_1908" s="T50">v</ta>
            <ta e="T52" id="Seg_1909" s="T51">conj</ta>
            <ta e="T53" id="Seg_1910" s="T52">v</ta>
            <ta e="T54" id="Seg_1911" s="T53">pers</ta>
            <ta e="T55" id="Seg_1912" s="T54">n</ta>
            <ta e="T56" id="Seg_1913" s="T55">v</ta>
            <ta e="T57" id="Seg_1914" s="T56">adj</ta>
            <ta e="T58" id="Seg_1915" s="T57">n</ta>
            <ta e="T59" id="Seg_1916" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_1917" s="T59">adv</ta>
            <ta e="T61" id="Seg_1918" s="T60">pers</ta>
            <ta e="T62" id="Seg_1919" s="T61">ptcl</ta>
            <ta e="T63" id="Seg_1920" s="T62">pers</ta>
            <ta e="T64" id="Seg_1921" s="T63">quant</ta>
            <ta e="T65" id="Seg_1922" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_1923" s="T65">adj</ta>
            <ta e="T67" id="Seg_1924" s="T66">interj</ta>
            <ta e="T68" id="Seg_1925" s="T67">pers</ta>
            <ta e="T69" id="Seg_1926" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_1927" s="T69">pers</ta>
            <ta e="T71" id="Seg_1928" s="T70">v</ta>
            <ta e="T72" id="Seg_1929" s="T71">pers</ta>
            <ta e="T73" id="Seg_1930" s="T72">conj</ta>
            <ta e="T74" id="Seg_1931" s="T73">v</ta>
            <ta e="T75" id="Seg_1932" s="T74">pers</ta>
            <ta e="T76" id="Seg_1933" s="T75">v</ta>
            <ta e="T77" id="Seg_1934" s="T76">conj</ta>
            <ta e="T78" id="Seg_1935" s="T77">n</ta>
            <ta e="T79" id="Seg_1936" s="T78">n</ta>
            <ta e="T80" id="Seg_1937" s="T79">adv</ta>
            <ta e="T81" id="Seg_1938" s="T80">v</ta>
            <ta e="T82" id="Seg_1939" s="T81">pers</ta>
            <ta e="T83" id="Seg_1940" s="T82">v</ta>
            <ta e="T84" id="Seg_1941" s="T83">pers</ta>
            <ta e="T85" id="Seg_1942" s="T84">adv</ta>
            <ta e="T86" id="Seg_1943" s="T85">v</ta>
            <ta e="T87" id="Seg_1944" s="T86">n</ta>
            <ta e="T88" id="Seg_1945" s="T87">v</ta>
            <ta e="T89" id="Seg_1946" s="T88">n</ta>
            <ta e="T90" id="Seg_1947" s="T89">adv</ta>
            <ta e="T91" id="Seg_1948" s="T90">v</ta>
            <ta e="T92" id="Seg_1949" s="T91">interj</ta>
            <ta e="T93" id="Seg_1950" s="T92">pers</ta>
            <ta e="T94" id="Seg_1951" s="T93">v</ta>
            <ta e="T95" id="Seg_1952" s="T94">conj</ta>
            <ta e="T96" id="Seg_1953" s="T95">v</ta>
            <ta e="T97" id="Seg_1954" s="T96">adv</ta>
            <ta e="T98" id="Seg_1955" s="T97">n</ta>
            <ta e="T99" id="Seg_1956" s="T98">n</ta>
            <ta e="T100" id="Seg_1957" s="T99">v</ta>
            <ta e="T101" id="Seg_1958" s="T100">n</ta>
            <ta e="T102" id="Seg_1959" s="T101">ptcl</ta>
            <ta e="T103" id="Seg_1960" s="T102">v</ta>
            <ta e="T104" id="Seg_1961" s="T103">pers</ta>
            <ta e="T105" id="Seg_1962" s="T104">ptcl</ta>
            <ta e="T106" id="Seg_1963" s="T105">pers</ta>
            <ta e="T107" id="Seg_1964" s="T106">v</ta>
            <ta e="T108" id="Seg_1965" s="T107">n</ta>
            <ta e="T109" id="Seg_1966" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_1967" s="T109">v</ta>
            <ta e="T111" id="Seg_1968" s="T110">n</ta>
            <ta e="T112" id="Seg_1969" s="T111">adv</ta>
            <ta e="T113" id="Seg_1970" s="T112">v</ta>
            <ta e="T114" id="Seg_1971" s="T113">pers</ta>
            <ta e="T115" id="Seg_1972" s="T114">v</ta>
            <ta e="T116" id="Seg_1973" s="T115">n</ta>
            <ta e="T117" id="Seg_1974" s="T116">n</ta>
            <ta e="T118" id="Seg_1975" s="T117">n</ta>
            <ta e="T119" id="Seg_1976" s="T118">v</ta>
            <ta e="T120" id="Seg_1977" s="T119">pers</ta>
            <ta e="T121" id="Seg_1978" s="T120">v</ta>
            <ta e="T122" id="Seg_1979" s="T121">v</ta>
            <ta e="T123" id="Seg_1980" s="T122">pers</ta>
            <ta e="T124" id="Seg_1981" s="T123">num</ta>
            <ta e="T125" id="Seg_1982" s="T124">n</ta>
            <ta e="T126" id="Seg_1983" s="T125">num</ta>
            <ta e="T127" id="Seg_1984" s="T126">n</ta>
            <ta e="T128" id="Seg_1985" s="T127">v</ta>
            <ta e="T129" id="Seg_1986" s="T128">v</ta>
            <ta e="T130" id="Seg_1987" s="T129">conj</ta>
            <ta e="T131" id="Seg_1988" s="T130">adj</ta>
            <ta e="T132" id="Seg_1989" s="T131">n</ta>
            <ta e="T133" id="Seg_1990" s="T132">v</ta>
            <ta e="T134" id="Seg_1991" s="T133">v</ta>
            <ta e="T135" id="Seg_1992" s="T134">pers</ta>
            <ta e="T136" id="Seg_1993" s="T135">v</ta>
            <ta e="T137" id="Seg_1994" s="T136">dem</ta>
            <ta e="T138" id="Seg_1995" s="T137">n</ta>
            <ta e="T139" id="Seg_1996" s="T138">conj</ta>
            <ta e="T140" id="Seg_1997" s="T139">v</ta>
            <ta e="T141" id="Seg_1998" s="T140">adv</ta>
            <ta e="T142" id="Seg_1999" s="T141">v</ta>
            <ta e="T143" id="Seg_2000" s="T142">conj</ta>
            <ta e="T144" id="Seg_2001" s="T143">pers</ta>
            <ta e="T145" id="Seg_2002" s="T144">ptcl</ta>
            <ta e="T146" id="Seg_2003" s="T145">v</ta>
            <ta e="T147" id="Seg_2004" s="T146">pers</ta>
            <ta e="T148" id="Seg_2005" s="T147">adj</ta>
            <ta e="T149" id="Seg_2006" s="T148">n</ta>
            <ta e="T150" id="Seg_2007" s="T149">v</ta>
            <ta e="T151" id="Seg_2008" s="T150">v</ta>
            <ta e="T152" id="Seg_2009" s="T151">interrog</ta>
            <ta e="T153" id="Seg_2010" s="T152">v</ta>
            <ta e="T154" id="Seg_2011" s="T153">pers</ta>
            <ta e="T155" id="Seg_2012" s="T154">v</ta>
            <ta e="T156" id="Seg_2013" s="T155">v</ta>
            <ta e="T157" id="Seg_2014" s="T156">n</ta>
            <ta e="T158" id="Seg_2015" s="T157">adj</ta>
            <ta e="T159" id="Seg_2016" s="T158">n</ta>
            <ta e="T160" id="Seg_2017" s="T159">v</ta>
            <ta e="T161" id="Seg_2018" s="T160">conj</ta>
            <ta e="T162" id="Seg_2019" s="T161">n</ta>
            <ta e="T163" id="Seg_2020" s="T162">interrog</ta>
            <ta e="T164" id="Seg_2021" s="T163">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T8" id="Seg_2022" s="T7">np.h:Th</ta>
            <ta e="T9" id="Seg_2023" s="T8">adv:Time</ta>
            <ta e="T10" id="Seg_2024" s="T9">pro.h:A</ta>
            <ta e="T12" id="Seg_2025" s="T11">np:G</ta>
            <ta e="T14" id="Seg_2026" s="T13">np:P</ta>
            <ta e="T15" id="Seg_2027" s="T14">0.1.h:A</ta>
            <ta e="T17" id="Seg_2028" s="T16">0.1.h:A</ta>
            <ta e="T19" id="Seg_2029" s="T18">pro.h:Poss</ta>
            <ta e="T20" id="Seg_2030" s="T19">np.h:A</ta>
            <ta e="T24" id="Seg_2031" s="T23">np:Th</ta>
            <ta e="T25" id="Seg_2032" s="T24">pro.h:R</ta>
            <ta e="T26" id="Seg_2033" s="T25">0.3.h:A</ta>
            <ta e="T28" id="Seg_2034" s="T27">pro.h:A</ta>
            <ta e="T34" id="Seg_2035" s="T33">pro.h:A</ta>
            <ta e="T36" id="Seg_2036" s="T35">pro.h:Th</ta>
            <ta e="T38" id="Seg_2037" s="T37">pro.h:A</ta>
            <ta e="T42" id="Seg_2038" s="T41">np:A</ta>
            <ta e="T44" id="Seg_2039" s="T43">np:So</ta>
            <ta e="T46" id="Seg_2040" s="T45">0.3:A</ta>
            <ta e="T48" id="Seg_2041" s="T47">pro.h:G</ta>
            <ta e="T49" id="Seg_2042" s="T48">0.3:A</ta>
            <ta e="T50" id="Seg_2043" s="T49">pro.h:A</ta>
            <ta e="T53" id="Seg_2044" s="T52">0.1.h:A</ta>
            <ta e="T54" id="Seg_2045" s="T53">pro:P</ta>
            <ta e="T55" id="Seg_2046" s="T54">np.h:A 0.1.h:Poss</ta>
            <ta e="T60" id="Seg_2047" s="T59">adv:Time</ta>
            <ta e="T61" id="Seg_2048" s="T60">pro.h:A</ta>
            <ta e="T63" id="Seg_2049" s="T62">pro:P</ta>
            <ta e="T68" id="Seg_2050" s="T67">pro.h:A</ta>
            <ta e="T70" id="Seg_2051" s="T69">pro.h:Th</ta>
            <ta e="T72" id="Seg_2052" s="T71">pro.h:P</ta>
            <ta e="T74" id="Seg_2053" s="T73">0.3:A</ta>
            <ta e="T75" id="Seg_2054" s="T74">pro.h:A</ta>
            <ta e="T78" id="Seg_2055" s="T77">np:So</ta>
            <ta e="T79" id="Seg_2056" s="T78">np:A</ta>
            <ta e="T82" id="Seg_2057" s="T81">pro.h:G</ta>
            <ta e="T83" id="Seg_2058" s="T82">0.3:A</ta>
            <ta e="T84" id="Seg_2059" s="T83">pro.h:A</ta>
            <ta e="T85" id="Seg_2060" s="T84">adv:L</ta>
            <ta e="T87" id="Seg_2061" s="T86">np:Th</ta>
            <ta e="T88" id="Seg_2062" s="T87">0.1.h:A 0.3:Th</ta>
            <ta e="T89" id="Seg_2063" s="T88">np.h:A</ta>
            <ta e="T93" id="Seg_2064" s="T92">pro.h:A</ta>
            <ta e="T96" id="Seg_2065" s="T95">0.1.h:A</ta>
            <ta e="T97" id="Seg_2066" s="T96">adv:Time</ta>
            <ta e="T104" id="Seg_2067" s="T103">pro.h:A</ta>
            <ta e="T106" id="Seg_2068" s="T105">pro.h:Th</ta>
            <ta e="T108" id="Seg_2069" s="T107">np.h:A</ta>
            <ta e="T111" id="Seg_2070" s="T110">np:L</ta>
            <ta e="T113" id="Seg_2071" s="T112">0.3:A</ta>
            <ta e="T114" id="Seg_2072" s="T113">pro.h:G</ta>
            <ta e="T117" id="Seg_2073" s="T116">np:A</ta>
            <ta e="T118" id="Seg_2074" s="T117">np:P</ta>
            <ta e="T120" id="Seg_2075" s="T119">pro.h:P</ta>
            <ta e="T121" id="Seg_2076" s="T120">v:Th</ta>
            <ta e="T122" id="Seg_2077" s="T121">0.3:E</ta>
            <ta e="T123" id="Seg_2078" s="T122">pro.h:Poss</ta>
            <ta e="T125" id="Seg_2079" s="T124">np:Th</ta>
            <ta e="T127" id="Seg_2080" s="T126">np:A</ta>
            <ta e="T129" id="Seg_2081" s="T128">0.2.h:A 0.3:Th</ta>
            <ta e="T132" id="Seg_2082" s="T131">np:A</ta>
            <ta e="T134" id="Seg_2083" s="T133">0.2.h:A</ta>
            <ta e="T135" id="Seg_2084" s="T134">pro.h:A</ta>
            <ta e="T138" id="Seg_2085" s="T137">np:Th</ta>
            <ta e="T140" id="Seg_2086" s="T139">0.2.h:A</ta>
            <ta e="T142" id="Seg_2087" s="T141">0.1.h:A</ta>
            <ta e="T144" id="Seg_2088" s="T143">pro.h:Th</ta>
            <ta e="T146" id="Seg_2089" s="T145">0.3.h:E</ta>
            <ta e="T147" id="Seg_2090" s="T146">pro.h:A</ta>
            <ta e="T149" id="Seg_2091" s="T148">np:Time</ta>
            <ta e="T152" id="Seg_2092" s="T151">pro.h:A</ta>
            <ta e="T154" id="Seg_2093" s="T153">pro.h:A</ta>
            <ta e="T156" id="Seg_2094" s="T155">0.1.h:A</ta>
            <ta e="T157" id="Seg_2095" s="T156">np.h:Poss 0.1.h:Poss</ta>
            <ta e="T159" id="Seg_2096" s="T158">np:Th</ta>
            <ta e="T162" id="Seg_2097" s="T161">np:Poss</ta>
            <ta e="T163" id="Seg_2098" s="T162">pro:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T6" id="Seg_2099" s="T5">v:pred</ta>
            <ta e="T8" id="Seg_2100" s="T7">np.h:S</ta>
            <ta e="T10" id="Seg_2101" s="T9">pro.h:S</ta>
            <ta e="T11" id="Seg_2102" s="T10">v:pred</ta>
            <ta e="T14" id="Seg_2103" s="T13">np:O</ta>
            <ta e="T15" id="Seg_2104" s="T14">0.1.h:S v:pred</ta>
            <ta e="T17" id="Seg_2105" s="T16">0.1.h:S v:pred</ta>
            <ta e="T18" id="Seg_2106" s="T17">s:purp</ta>
            <ta e="T20" id="Seg_2107" s="T19">np.h:S</ta>
            <ta e="T21" id="Seg_2108" s="T20">v:pred</ta>
            <ta e="T24" id="Seg_2109" s="T23">np:O</ta>
            <ta e="T26" id="Seg_2110" s="T25">0.3.h:S v:pred</ta>
            <ta e="T28" id="Seg_2111" s="T27">pro.h:S</ta>
            <ta e="T33" id="Seg_2112" s="T32">v:pred</ta>
            <ta e="T34" id="Seg_2113" s="T33">pro.h:S</ta>
            <ta e="T37" id="Seg_2114" s="T36">v:pred</ta>
            <ta e="T38" id="Seg_2115" s="T37">pro.h:S</ta>
            <ta e="T39" id="Seg_2116" s="T38">v:pred</ta>
            <ta e="T42" id="Seg_2117" s="T41">np:S</ta>
            <ta e="T43" id="Seg_2118" s="T42">v:pred</ta>
            <ta e="T45" id="Seg_2119" s="T44">s:adv</ta>
            <ta e="T46" id="Seg_2120" s="T45">0.3:S v:pred</ta>
            <ta e="T49" id="Seg_2121" s="T48">0.3.h:S v:pred</ta>
            <ta e="T50" id="Seg_2122" s="T49">pro.h:S</ta>
            <ta e="T51" id="Seg_2123" s="T50">v:pred</ta>
            <ta e="T53" id="Seg_2124" s="T52">0.1.h:S v:pred</ta>
            <ta e="T54" id="Seg_2125" s="T53">pro:O</ta>
            <ta e="T55" id="Seg_2126" s="T54">np.h:S</ta>
            <ta e="T56" id="Seg_2127" s="T55">v:pred</ta>
            <ta e="T61" id="Seg_2128" s="T60">pro.h:S</ta>
            <ta e="T63" id="Seg_2129" s="T62">pro:O</ta>
            <ta e="T66" id="Seg_2130" s="T65">v:pred</ta>
            <ta e="T68" id="Seg_2131" s="T67">pro.h:S</ta>
            <ta e="T71" id="Seg_2132" s="T70">v:pred</ta>
            <ta e="T74" id="Seg_2133" s="T71">s:compl</ta>
            <ta e="T75" id="Seg_2134" s="T74">pro.h:S</ta>
            <ta e="T76" id="Seg_2135" s="T75">v:pred</ta>
            <ta e="T79" id="Seg_2136" s="T78">np:S</ta>
            <ta e="T80" id="Seg_2137" s="T79">s:adv</ta>
            <ta e="T81" id="Seg_2138" s="T80">v:pred</ta>
            <ta e="T83" id="Seg_2139" s="T82">0.3:S v:pred</ta>
            <ta e="T84" id="Seg_2140" s="T83">pro.h:S</ta>
            <ta e="T86" id="Seg_2141" s="T85">v:pred</ta>
            <ta e="T87" id="Seg_2142" s="T86">np:O</ta>
            <ta e="T88" id="Seg_2143" s="T87">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T89" id="Seg_2144" s="T88">np.h:S</ta>
            <ta e="T91" id="Seg_2145" s="T90">v:pred</ta>
            <ta e="T93" id="Seg_2146" s="T92">pro.h:S</ta>
            <ta e="T94" id="Seg_2147" s="T93">v:pred</ta>
            <ta e="T96" id="Seg_2148" s="T95">0.1.h:S v:pred</ta>
            <ta e="T104" id="Seg_2149" s="T103">pro.h:S</ta>
            <ta e="T106" id="Seg_2150" s="T105">pro.h:O</ta>
            <ta e="T107" id="Seg_2151" s="T106">v:pred</ta>
            <ta e="T108" id="Seg_2152" s="T107">np.h:S</ta>
            <ta e="T110" id="Seg_2153" s="T109">v:pred</ta>
            <ta e="T113" id="Seg_2154" s="T112">0.3:S v:pred</ta>
            <ta e="T115" id="Seg_2155" s="T114">v:pred</ta>
            <ta e="T117" id="Seg_2156" s="T116">np:S</ta>
            <ta e="T119" id="Seg_2157" s="T117">s:purp</ta>
            <ta e="T121" id="Seg_2158" s="T120">v:O</ta>
            <ta e="T122" id="Seg_2159" s="T121">0.3:S v:pred</ta>
            <ta e="T125" id="Seg_2160" s="T124">np:S</ta>
            <ta e="T127" id="Seg_2161" s="T126">np:S</ta>
            <ta e="T128" id="Seg_2162" s="T127">v:pred</ta>
            <ta e="T129" id="Seg_2163" s="T128">0.2.h:S v:pred 0.3:O</ta>
            <ta e="T132" id="Seg_2164" s="T131">np:S</ta>
            <ta e="T133" id="Seg_2165" s="T132">v:pred</ta>
            <ta e="T134" id="Seg_2166" s="T133">0.2.h:S v:pred</ta>
            <ta e="T135" id="Seg_2167" s="T134">pro.h:S</ta>
            <ta e="T136" id="Seg_2168" s="T135">v:pred</ta>
            <ta e="T140" id="Seg_2169" s="T138">s:rel</ta>
            <ta e="T142" id="Seg_2170" s="T141">0.1.h:S v:pred</ta>
            <ta e="T144" id="Seg_2171" s="T143">pro.h:O</ta>
            <ta e="T146" id="Seg_2172" s="T145">0.3.h:S v:pred</ta>
            <ta e="T147" id="Seg_2173" s="T146">pro.h:S</ta>
            <ta e="T150" id="Seg_2174" s="T149">v:pred</ta>
            <ta e="T151" id="Seg_2175" s="T150">s:purp</ta>
            <ta e="T153" id="Seg_2176" s="T151">s:compl</ta>
            <ta e="T154" id="Seg_2177" s="T153">pro.h:S</ta>
            <ta e="T155" id="Seg_2178" s="T154">v:pred</ta>
            <ta e="T156" id="Seg_2179" s="T155">0.1.h:S v:pred</ta>
            <ta e="T159" id="Seg_2180" s="T158">np:S</ta>
            <ta e="T160" id="Seg_2181" s="T159">v:pred</ta>
            <ta e="T163" id="Seg_2182" s="T162">pro:S</ta>
            <ta e="T164" id="Seg_2183" s="T163">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T16" id="Seg_2184" s="T15">RUS:gram</ta>
            <ta e="T23" id="Seg_2185" s="T22">RUS:gram</ta>
            <ta e="T27" id="Seg_2186" s="T26">RUS:gram</ta>
            <ta e="T29" id="Seg_2187" s="T28">RUS:gram</ta>
            <ta e="T32" id="Seg_2188" s="T31">RUS:core</ta>
            <ta e="T40" id="Seg_2189" s="T39">RUS:gram</ta>
            <ta e="T47" id="Seg_2190" s="T46">RUS:gram</ta>
            <ta e="T52" id="Seg_2191" s="T51">RUS:gram</ta>
            <ta e="T57" id="Seg_2192" s="T56">TURK:cult</ta>
            <ta e="T59" id="Seg_2193" s="T58">RUS:gram</ta>
            <ta e="T60" id="Seg_2194" s="T59">RUS:core</ta>
            <ta e="T62" id="Seg_2195" s="T61">RUS:gram</ta>
            <ta e="T64" id="Seg_2196" s="T63">RUS:core</ta>
            <ta e="T65" id="Seg_2197" s="T64">RUS:gram</ta>
            <ta e="T67" id="Seg_2198" s="T66">RUS:disc</ta>
            <ta e="T69" id="Seg_2199" s="T68">RUS:gram</ta>
            <ta e="T77" id="Seg_2200" s="T76">RUS:gram</ta>
            <ta e="T92" id="Seg_2201" s="T91">RUS:disc</ta>
            <ta e="T95" id="Seg_2202" s="T94">RUS:gram</ta>
            <ta e="T97" id="Seg_2203" s="T96">RUS:coreRUS:gram</ta>
            <ta e="T100" id="Seg_2204" s="T99">RUS:core</ta>
            <ta e="T102" id="Seg_2205" s="T101">RUS:gram</ta>
            <ta e="T105" id="Seg_2206" s="T104">RUS:gram</ta>
            <ta e="T109" id="Seg_2207" s="T108">RUS:disc</ta>
            <ta e="T130" id="Seg_2208" s="T129">RUS:gram</ta>
            <ta e="T139" id="Seg_2209" s="T138">RUS:gram</ta>
            <ta e="T141" id="Seg_2210" s="T140">RUS:disc</ta>
            <ta e="T143" id="Seg_2211" s="T142">RUS:gram</ta>
            <ta e="T145" id="Seg_2212" s="T144">RUS:disc</ta>
            <ta e="T161" id="Seg_2213" s="T160">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_2214" s="T1">A tale about two brothers.</ta>
            <ta e="T8" id="Seg_2215" s="T4">We were two brothers.</ta>
            <ta e="T13" id="Seg_2216" s="T8">Once we went to the forest [to chop] firewood.</ta>
            <ta e="T18" id="Seg_2217" s="T13">We chopped firewood and sat down to have a rest.</ta>
            <ta e="T27" id="Seg_2218" s="T18">My brother said: “If only they gave us some white bread.</ta>
            <ta e="T33" id="Seg_2219" s="T27">We would eat it on an empty stomach.</ta>
            <ta e="T37" id="Seg_2220" s="T33">Then you, brother, would look at me!”</ta>
            <ta e="T49" id="Seg_2221" s="T37">He said so, and a white bread rolled downhill and came to us.</ta>
            <ta e="T54" id="Seg_2222" s="T49">We sat down and ate it up.</ta>
            <ta e="T60" id="Seg_2223" s="T54">My brother said: “Now a bottle of wine would [be good].</ta>
            <ta e="T66" id="Seg_2224" s="T60">We would drink it all.</ta>
            <ta e="T74" id="Seg_2225" s="T66">You would look at me, how I would be drunk.”</ta>
            <ta e="T83" id="Seg_2226" s="T74">He said so, and a bottle rolled downhill and came to us.</ta>
            <ta e="T88" id="Seg_2227" s="T83">We finished the bottle, and licked it clean.</ta>
            <ta e="T96" id="Seg_2228" s="T88">The brother said again: “So we ate good and drank good.</ta>
            <ta e="T103" id="Seg_2229" s="T96">Now it would be time to struggle with a bear, to warm up my bones.</ta>
            <ta e="T107" id="Seg_2230" s="T103">You would look at me.”</ta>
            <ta e="T113" id="Seg_2231" s="T107">As soon as the brother said so, it began to crackle in the forest(?).</ta>
            <ta e="T119" id="Seg_2232" s="T113">A bear was coming to us to dry(?) berries.</ta>
            <ta e="T122" id="Seg_2233" s="T119">He wanted to crush us.</ta>
            <ta e="T125" id="Seg_2234" s="T122">I have got two hearts.</ta>
            <ta e="T129" id="Seg_2235" s="T125">One heart was saying: “Keep it!”</ta>
            <ta e="T134" id="Seg_2236" s="T129">And another heart was saying: “Run.”</ta>
            <ta e="T140" id="Seg_2237" s="T134">I listened to the heart, which was saying to run.</ta>
            <ta e="T146" id="Seg_2238" s="T140">I set off running, they could hardly see me.</ta>
            <ta e="T153" id="Seg_2239" s="T146">The next day I came to look who had won.</ta>
            <ta e="T156" id="Seg_2240" s="T153">I came and looked around.</ta>
            <ta e="T160" id="Seg_2241" s="T156">There was a little piece left from my brother.</ta>
            <ta e="T164" id="Seg_2242" s="T160">And there was nothing left from the bear.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_2243" s="T1">Eine Geschichte über zwei Brüder.</ta>
            <ta e="T8" id="Seg_2244" s="T4">Wir waren zwei Brüder.</ta>
            <ta e="T13" id="Seg_2245" s="T8">Einmal gingen wir in den Wald für Feuerholz.</ta>
            <ta e="T18" id="Seg_2246" s="T13">Wir hackten Feuerholz und setzten uns hin um zu rasten.</ta>
            <ta e="T27" id="Seg_2247" s="T18">Mein Bruder sagte: "Wenn wir nur weißes Brot hätten.</ta>
            <ta e="T33" id="Seg_2248" s="T27">Wir würden es auf leeren Magen essen.</ta>
            <ta e="T37" id="Seg_2249" s="T33">Dann, Bruder, würdest du zu mir schauen."</ta>
            <ta e="T49" id="Seg_2250" s="T37">Er sagte es so und ein Weißbrot rollte den Hügel herab und kam zu uns.</ta>
            <ta e="T54" id="Seg_2251" s="T49">Wir setzten uns und aßen es auf.</ta>
            <ta e="T60" id="Seg_2252" s="T54">Mein Bruder sagte: "Jetzt eine Flasche Wein wäre [gut].</ta>
            <ta e="T66" id="Seg_2253" s="T60">Wir würden sie austrinken.</ta>
            <ta e="T74" id="Seg_2254" s="T66">Du würdest mich ansehen, wie betrunken ich wäre."</ta>
            <ta e="T83" id="Seg_2255" s="T74">Er sagte es so und eine Flasche rollte den Hügel herab und kam zu uns.</ta>
            <ta e="T88" id="Seg_2256" s="T83">Wir leerten die Flasche und leckten sie sauber.</ta>
            <ta e="T96" id="Seg_2257" s="T88">Der Bruder sagte wieder: "So, wir haben gut gegessen und getrunken.</ta>
            <ta e="T103" id="Seg_2258" s="T96">Jetzt wäre es Zeit, um mit einem Bären zu ringen um meine Knochen aufzuwärmen.</ta>
            <ta e="T107" id="Seg_2259" s="T103">Du würdest mir zusehen."</ta>
            <ta e="T113" id="Seg_2260" s="T107">Sobald der Bruder das sagte, begann es im Wald zu knacken.</ta>
            <ta e="T119" id="Seg_2261" s="T113">Ein Bär kam zu uns um Beeren zu trocknen(?).</ta>
            <ta e="T122" id="Seg_2262" s="T119">Er wollte uns zermalmen.</ta>
            <ta e="T125" id="Seg_2263" s="T122">Ich habe zwei Herzen.</ta>
            <ta e="T129" id="Seg_2264" s="T125">Ein Herz sagte: "Behalte es!"</ta>
            <ta e="T134" id="Seg_2265" s="T129">Und das andere Herz sagte: "Lauf."</ta>
            <ta e="T140" id="Seg_2266" s="T134">Ich hörte auf das Herz, das mir auftrug zu rennen.</ta>
            <ta e="T146" id="Seg_2267" s="T140">Ich rannte los, sie konnten mich kaum sehen.</ta>
            <ta e="T153" id="Seg_2268" s="T146">Am nächsten Tag kehrte ich zurück um zu sehen, wer gewonnen hatte.</ta>
            <ta e="T156" id="Seg_2269" s="T153">Ich kam und schaute mich um.</ta>
            <ta e="T160" id="Seg_2270" s="T156">Von meinem Bruder blieb nur ein kleines Stück übrig.</ta>
            <ta e="T164" id="Seg_2271" s="T160">Und vom Bären blieb nichts übrig.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_2272" s="T1">Сказка про двух братьев.</ta>
            <ta e="T8" id="Seg_2273" s="T4">Было нас двое братьев.</ta>
            <ta e="T13" id="Seg_2274" s="T8">Однажды мы отправились в лес(?/поле?) по дрова.</ta>
            <ta e="T18" id="Seg_2275" s="T13">Дров нарубили и сели отдохнуть.</ta>
            <ta e="T27" id="Seg_2276" s="T18">Мой брат говорит: “Белый бы хлеб нам дали бы.</ta>
            <ta e="T33" id="Seg_2277" s="T27">Мы бы голодный живот весь бы (=натощак) съели.</ta>
            <ta e="T37" id="Seg_2278" s="T33">Ты бы, брат, на меня бы тогда посмотрел”.</ta>
            <ta e="T49" id="Seg_2279" s="T37">Он сказал, и белый хлеб идет, с горки катится и к нам пришел.</ta>
            <ta e="T54" id="Seg_2280" s="T49">Мы сели и съели его.</ta>
            <ta e="T60" id="Seg_2281" s="T54">Брат говорит: “Теперь бы бутылку вина.</ta>
            <ta e="T66" id="Seg_2282" s="T60">Мы бы его всё бы выпили.</ta>
            <ta e="T74" id="Seg_2283" s="T66">Вот бы ты посмотрел на меня, как я пьяный был бы”.</ta>
            <ta e="T83" id="Seg_2284" s="T74">Он сказал, и с горки бутылка катится, к нам идет.</ta>
            <ta e="T88" id="Seg_2285" s="T83">Мы тут кончили бутылку, вылизали.</ta>
            <ta e="T96" id="Seg_2286" s="T88">Брат опять говорит: “Вот мы наелись и напились.</ta>
            <ta e="T103" id="Seg_2287" s="T96">Сейчас бы с медведем побороться, косточки бы помять.</ta>
            <ta e="T107" id="Seg_2288" s="T103">Ты бы на меня посмотрел бы”.</ta>
            <ta e="T113" id="Seg_2289" s="T107">Брат только сказал, в лесу затрещало.</ta>
            <ta e="T119" id="Seg_2290" s="T113">К нам идет медведь ягоду сушить.</ta>
            <ta e="T122" id="Seg_2291" s="T119">Нас подмять хочет.</ta>
            <ta e="T125" id="Seg_2292" s="T122">У меня два сердца.</ta>
            <ta e="T129" id="Seg_2293" s="T125">Одно твое сердце говорит: “Держи!”</ta>
            <ta e="T134" id="Seg_2294" s="T129">А второе твое сердце говорит: “Беги”.</ta>
            <ta e="T140" id="Seg_2295" s="T134">Послушался я того сердце, которое (говорило): беги.</ta>
            <ta e="T146" id="Seg_2296" s="T140">Как припущу, меня только и видели.</ta>
            <ta e="T153" id="Seg_2297" s="T146">На другой день я вернулся посмотреть, кто победил.</ta>
            <ta e="T156" id="Seg_2298" s="T153">Я пришел посмотрел.</ta>
            <ta e="T160" id="Seg_2299" s="T156">От брата малюсенький клочочек остался.</ta>
            <ta e="T164" id="Seg_2300" s="T160">А от медведя ничего не осталось.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_2301" s="T1">сказка про двух братьев</ta>
            <ta e="T8" id="Seg_2302" s="T4">было нас двое братьев</ta>
            <ta e="T13" id="Seg_2303" s="T8">однажды мы отправились в лес по дрова</ta>
            <ta e="T18" id="Seg_2304" s="T13">дров нарубили и сели отдохнуть</ta>
            <ta e="T27" id="Seg_2305" s="T18">мой брат говорит белый хлеб бы нам дали</ta>
            <ta e="T33" id="Seg_2306" s="T27">мы бы натощак (голодный живот) весь бы съели</ta>
            <ta e="T37" id="Seg_2307" s="T33">ты бы брат на меня бы тогда посмотрел</ta>
            <ta e="T49" id="Seg_2308" s="T37">он сказал и белый хлеб идет с горки катится и к нам прикатился</ta>
            <ta e="T54" id="Seg_2309" s="T49">мы сели и съели его</ta>
            <ta e="T60" id="Seg_2310" s="T54">брат говорит теперь бы бутылку вина</ta>
            <ta e="T66" id="Seg_2311" s="T60">мы бы его бы весь выпили</ta>
            <ta e="T74" id="Seg_2312" s="T66">вот бы ты посмотрел на меня как я пьяный был бы</ta>
            <ta e="T83" id="Seg_2313" s="T74">он сказал и с горки бутылка катится к нам идет</ta>
            <ta e="T88" id="Seg_2314" s="T83">мы тут кончили бутылку облизали (облизнули)</ta>
            <ta e="T96" id="Seg_2315" s="T88">брат опять говорит вот мы наелись и напились</ta>
            <ta e="T103" id="Seg_2316" s="T96">сейчас бы с медведем побороться косточки помять</ta>
            <ta e="T107" id="Seg_2317" s="T103">ты бы на меня посмотрел бы</ta>
            <ta e="T113" id="Seg_2318" s="T107">брат только сказал в лесу затрещало</ta>
            <ta e="T119" id="Seg_2319" s="T113">к нам идет медведь ягоду сушить</ta>
            <ta e="T122" id="Seg_2320" s="T119">нас подмять хочет</ta>
            <ta e="T125" id="Seg_2321" s="T122">у меня два сердца</ta>
            <ta e="T129" id="Seg_2322" s="T125">одно сердце говорит держи</ta>
            <ta e="T134" id="Seg_2323" s="T129">а второе сердце говорит беги</ta>
            <ta e="T140" id="Seg_2324" s="T134">послушался я сердце беги</ta>
            <ta e="T146" id="Seg_2325" s="T140">как припущу(сь) меня только и видели</ta>
            <ta e="T153" id="Seg_2326" s="T146">на другой день я вернулся посмотреть кто победил</ta>
            <ta e="T156" id="Seg_2327" s="T153">я пришел посмотрел</ta>
            <ta e="T160" id="Seg_2328" s="T156">от брата малюсенький клочочек (клубочек) остался</ta>
            <ta e="T164" id="Seg_2329" s="T160">а от медведя ничего не осталось</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T8" id="Seg_2330" s="T4">[KuAI:] Variants: 'sədə', 'agazɨɣä'.</ta>
            <ta e="T74" id="Seg_2331" s="T66">[BrM:] Tentative analysis of 'seːrba', unclear construction.</ta>
            <ta e="T125" id="Seg_2332" s="T122">[KuAI:] Variant: 'sət'.</ta>
            <ta e="T146" id="Seg_2333" s="T140">[BrM:] üːbəditdat? Tentative analysis of 'kolʼdättə'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
