<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>TFF_1967_KalobokkaPoenegessa_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">TFF_1967_KalabokkaPoenegessa1_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">386</ud-information>
            <ud-information attribute-name="# HIAT:w">310</ud-information>
            <ud-information attribute-name="# e">310</ud-information>
            <ud-information attribute-name="# HIAT:u">45</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="TFF">
            <abbreviation>TFF</abbreviation>
            <sex value="f" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
         <tli id="T204" />
         <tli id="T205" />
         <tli id="T206" />
         <tli id="T207" />
         <tli id="T208" />
         <tli id="T209" />
         <tli id="T210" />
         <tli id="T211" />
         <tli id="T212" />
         <tli id="T213" />
         <tli id="T214" />
         <tli id="T215" />
         <tli id="T216" />
         <tli id="T217" />
         <tli id="T218" />
         <tli id="T219" />
         <tli id="T220" />
         <tli id="T221" />
         <tli id="T222" />
         <tli id="T223" />
         <tli id="T224" />
         <tli id="T225" />
         <tli id="T226" />
         <tli id="T227" />
         <tli id="T228" />
         <tli id="T229" />
         <tli id="T230" />
         <tli id="T231" />
         <tli id="T232" />
         <tli id="T233" />
         <tli id="T234" />
         <tli id="T235" />
         <tli id="T236" />
         <tli id="T237" />
         <tli id="T238" />
         <tli id="T239" />
         <tli id="T240" />
         <tli id="T241" />
         <tli id="T242" />
         <tli id="T243" />
         <tli id="T244" />
         <tli id="T245" />
         <tli id="T246" />
         <tli id="T247" />
         <tli id="T248" />
         <tli id="T249" />
         <tli id="T250" />
         <tli id="T251" />
         <tli id="T252" />
         <tli id="T253" />
         <tli id="T254" />
         <tli id="T255" />
         <tli id="T256" />
         <tli id="T257" />
         <tli id="T258" />
         <tli id="T259" />
         <tli id="T260" />
         <tli id="T261" />
         <tli id="T262" />
         <tli id="T263" />
         <tli id="T264" />
         <tli id="T265" />
         <tli id="T266" />
         <tli id="T267" />
         <tli id="T268" />
         <tli id="T269" />
         <tli id="T270" />
         <tli id="T271" />
         <tli id="T272" />
         <tli id="T273" />
         <tli id="T274" />
         <tli id="T275" />
         <tli id="T276" />
         <tli id="T277" />
         <tli id="T278" />
         <tli id="T279" />
         <tli id="T280" />
         <tli id="T281" />
         <tli id="T282" />
         <tli id="T283" />
         <tli id="T284" />
         <tli id="T285" />
         <tli id="T286" />
         <tli id="T287" />
         <tli id="T288" />
         <tli id="T289" />
         <tli id="T290" />
         <tli id="T291" />
         <tli id="T292" />
         <tli id="T293" />
         <tli id="T294" />
         <tli id="T295" />
         <tli id="T296" />
         <tli id="T297" />
         <tli id="T298" />
         <tli id="T299" />
         <tli id="T300" />
         <tli id="T301" />
         <tli id="T302" />
         <tli id="T303" />
         <tli id="T304" />
         <tli id="T305" />
         <tli id="T306" />
         <tli id="T307" />
         <tli id="T308" />
         <tli id="T309" />
         <tli id="T310" />
         <tli id="T311" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="TFF"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T311" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Elɨkwaːt</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">warkɨkwaːt</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">ära</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">pajassəqi</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">Tabɨštjannaːn</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">eːkwa</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">iːdi</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_29" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">Nept</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">Kalabokka</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_38" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">əːtət</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">meːta</ts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_47" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">Äzawət</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">kwannatən</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">poːjergu</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">šəːtt</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">a</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">Kalabokkanä</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">tʼičaːtt</ts>
                  <nts id="Seg_68" n="HIAT:ip">.</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_71" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">Tan</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">maːtqən</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">küːnaŋ</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_82" n="HIAT:w" s="T22">warqɨk</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_85" n="HIAT:w" s="T23">maːtəm</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_88" n="HIAT:w" s="T24">əːk</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_91" n="HIAT:w" s="T25">tʼazəlǯimbet</ts>
                  <nts id="Seg_92" n="HIAT:ip">.</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_95" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_97" n="HIAT:w" s="T26">Tʼaːzəlǯimendǯal</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_100" n="HIAT:w" s="T27">Pöːnegessa</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_103" n="HIAT:w" s="T28">töːnǯa</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_106" n="HIAT:w" s="T29">i</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_109" n="HIAT:w" s="T30">aːmǯa</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_112" n="HIAT:w" s="T31">št</ts>
                  <nts id="Seg_113" n="HIAT:ip">.</nts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_116" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_118" n="HIAT:w" s="T32">Äzt</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_121" n="HIAT:w" s="T33">i</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_124" n="HIAT:w" s="T34">aːwət</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_127" n="HIAT:w" s="T35">qwannaqi</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_130" n="HIAT:w" s="T36">šött</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_133" n="HIAT:w" s="T37">poːjergu</ts>
                  <nts id="Seg_134" n="HIAT:ip">.</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_137" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_139" n="HIAT:w" s="T38">Kalabokka</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_142" n="HIAT:w" s="T39">qaːla</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_145" n="HIAT:w" s="T40">pʼalgaːlɨŋ</ts>
                  <nts id="Seg_146" n="HIAT:ip">.</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_149" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_151" n="HIAT:w" s="T41">Aːdʼa</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_153" n="HIAT:ip">(</nts>
                  <ts e="T43" id="Seg_155" n="HIAT:w" s="T42">aːdʼe</ts>
                  <nts id="Seg_156" n="HIAT:ip">)</nts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_159" n="HIAT:w" s="T43">i</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_162" n="HIAT:w" s="T44">aːwa</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_165" n="HIAT:w" s="T45">qaj</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_168" n="HIAT:w" s="T46">čeːnčaːqi</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_171" n="HIAT:w" s="T47">Pöːnegessa</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_174" n="HIAT:w" s="T48">töːnǯa</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_177" n="HIAT:w" s="T49">maššim</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_180" n="HIAT:w" s="T50">amgu</ts>
                  <nts id="Seg_181" n="HIAT:ip">.</nts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_184" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_186" n="HIAT:w" s="T51">A</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_189" n="HIAT:w" s="T52">matt</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_192" n="HIAT:w" s="T53">kəba</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_195" n="HIAT:w" s="T54">pauze</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_198" n="HIAT:w" s="T55">tap</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_201" n="HIAT:w" s="T56">Pöːnegessam</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_204" n="HIAT:w" s="T57">maǯenǯaw</ts>
                  <nts id="Seg_205" n="HIAT:ip">.</nts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_208" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_210" n="HIAT:w" s="T58">Kəba</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_213" n="HIAT:w" s="T59">pawəmt</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_216" n="HIAT:w" s="T60">sälʼešpət</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_219" n="HIAT:w" s="T61">a</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_222" n="HIAT:w" s="T62">onǯa</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_225" n="HIAT:w" s="T63">läːra</ts>
                  <nts id="Seg_226" n="HIAT:ip">.</nts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_229" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_231" n="HIAT:w" s="T64">Madəmt</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_234" n="HIAT:w" s="T65">tʼaːzəlǯimbət</ts>
                  <nts id="Seg_235" n="HIAT:ip">.</nts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_238" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_240" n="HIAT:w" s="T66">Qatʼe</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_243" n="HIAT:w" s="T67">mɨlä</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_246" n="HIAT:w" s="T68">öntʼit</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_249" n="HIAT:w" s="T69">qajta</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_252" n="HIAT:w" s="T70">čaːǯa</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_255" n="HIAT:w" s="T71">toblantse</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_258" n="HIAT:w" s="T72">muǯuwatple</ts>
                  <nts id="Seg_259" n="HIAT:ip">.</nts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_262" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_264" n="HIAT:w" s="T73">Kalabokka</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_267" n="HIAT:w" s="T74">kəba</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_270" n="HIAT:w" s="T75">paundse</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_273" n="HIAT:w" s="T76">koptən</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_276" n="HIAT:w" s="T77">ɨːlont</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_279" n="HIAT:w" s="T78">atteda</ts>
                  <nts id="Seg_280" n="HIAT:ip">.</nts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_283" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_285" n="HIAT:w" s="T79">Pöːnegessa</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_288" n="HIAT:w" s="T80">maːt</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_291" n="HIAT:w" s="T81">šeːrna</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_294" n="HIAT:w" s="T82">Kalabokkam</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_297" n="HIAT:w" s="T83">koptən</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_300" n="HIAT:w" s="T84">ɨːlokonto</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_303" n="HIAT:w" s="T85">čatčit</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_306" n="HIAT:w" s="T86">onǯe</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_309" n="HIAT:w" s="T87">piːreqɨnt</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_312" n="HIAT:w" s="T88">i</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_315" n="HIAT:w" s="T89">Kalabokkam</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_318" n="HIAT:w" s="T90">poːlledit</ts>
                  <nts id="Seg_319" n="HIAT:ip">.</nts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_322" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_324" n="HIAT:w" s="T91">Kalabokkannaːn</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_327" n="HIAT:w" s="T92">eːja</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_330" n="HIAT:w" s="T93">kəba</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_333" n="HIAT:w" s="T94">paw</ts>
                  <nts id="Seg_334" n="HIAT:ip">.</nts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T104" id="Seg_337" n="HIAT:u" s="T95">
                  <ts e="T96" id="Seg_339" n="HIAT:w" s="T95">Aːmta</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_342" n="HIAT:w" s="T96">Pöːnegessan</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_345" n="HIAT:w" s="T97">pʼargeqən</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_348" n="HIAT:w" s="T98">kəba</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_351" n="HIAT:w" s="T99">paontsä</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_354" n="HIAT:w" s="T100">pʼargeqɨnt</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_357" n="HIAT:w" s="T101">tä</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_360" n="HIAT:w" s="T102">toː</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_363" n="HIAT:w" s="T103">poǯalǯešpat</ts>
                  <nts id="Seg_364" n="HIAT:ip">.</nts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_367" n="HIAT:u" s="T104">
                  <ts e="T105" id="Seg_369" n="HIAT:w" s="T104">Pöːnegessa</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_372" n="HIAT:w" s="T105">čeːnča</ts>
                  <nts id="Seg_373" n="HIAT:ip">.</nts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_376" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_378" n="HIAT:w" s="T106">Kalabokka</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_381" n="HIAT:w" s="T107">Kalabokka</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_384" n="HIAT:w" s="T108">mannani</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_387" n="HIAT:w" s="T109">pʼargʼew</ts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_390" n="HIAT:w" s="T110">ək</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_393" n="HIAT:w" s="T111">poǯalǯešplel</ts>
                  <nts id="Seg_394" n="HIAT:ip">.</nts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_397" n="HIAT:u" s="T112">
                  <ts e="T113" id="Seg_399" n="HIAT:w" s="T112">A</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_402" n="HIAT:w" s="T113">Kalabokka</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_405" n="HIAT:w" s="T114">pʼargʼeqɨn</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_408" n="HIAT:w" s="T115">na</ts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_411" n="HIAT:w" s="T116">kɨbɨʒo</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_414" n="HIAT:w" s="T117">paokantsä</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_417" n="HIAT:w" s="T118">Pöːnegessan</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_420" n="HIAT:w" s="T119">šiːdʼem</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_423" n="HIAT:w" s="T120">tʼoqollä</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_426" n="HIAT:w" s="T121">qwattɨt</ts>
                  <nts id="Seg_427" n="HIAT:ip">.</nts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_430" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_432" n="HIAT:w" s="T122">A</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_435" n="HIAT:w" s="T123">Pöːnegessa</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_438" n="HIAT:w" s="T124">čeːnča</ts>
                  <nts id="Seg_439" n="HIAT:ip">:</nts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_442" n="HIAT:w" s="T125">Kalabokka</ts>
                  <nts id="Seg_443" n="HIAT:ip">,</nts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_446" n="HIAT:w" s="T126">Kalabokka</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_449" n="HIAT:w" s="T127">matšiːdʼem</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_452" n="HIAT:w" s="T128">ək</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_455" n="HIAT:w" s="T129">tʼoqolešplel</ts>
                  <nts id="Seg_456" n="HIAT:ip">.</nts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T134" id="Seg_459" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_461" n="HIAT:w" s="T130">Mat</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_464" n="HIAT:w" s="T131">tašt</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_467" n="HIAT:w" s="T132">töːdenǯak</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_470" n="HIAT:w" s="T133">št</ts>
                  <nts id="Seg_471" n="HIAT:ip">.</nts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_474" n="HIAT:u" s="T134">
                  <ts e="T135" id="Seg_476" n="HIAT:w" s="T134">Pöːnegessa</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_479" n="HIAT:w" s="T135">Kalabokkam</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_482" n="HIAT:w" s="T136">tötət</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_485" n="HIAT:w" s="T137">moqonä</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_488" n="HIAT:w" s="T138">eːle</ts>
                  <nts id="Seg_489" n="HIAT:ip">.</nts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T146" id="Seg_492" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_494" n="HIAT:w" s="T139">Kalabokkam</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_497" n="HIAT:w" s="T140">olɨt</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_500" n="HIAT:w" s="T141">tarɨt</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_503" n="HIAT:w" s="T142">Pöːnegessa</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_506" n="HIAT:w" s="T143">köːgalbat</ts>
                  <nts id="Seg_507" n="HIAT:ip">,</nts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_510" n="HIAT:w" s="T144">oltargaːlɨŋ</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_513" n="HIAT:w" s="T145">qaːlɨmba</ts>
                  <nts id="Seg_514" n="HIAT:ip">.</nts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T151" id="Seg_517" n="HIAT:u" s="T146">
                  <ts e="T147" id="Seg_519" n="HIAT:w" s="T146">Pöːnegessa</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_522" n="HIAT:w" s="T147">poːnä</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_525" n="HIAT:w" s="T148">čaːnǯa</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_528" n="HIAT:w" s="T149">i</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_531" n="HIAT:w" s="T150">qwann</ts>
                  <nts id="Seg_532" n="HIAT:ip">.</nts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T156" id="Seg_535" n="HIAT:u" s="T151">
                  <ts e="T152" id="Seg_537" n="HIAT:w" s="T151">Kalabokka</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_540" n="HIAT:w" s="T152">mešelačlä</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_543" n="HIAT:w" s="T153">qaːla</ts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_546" n="HIAT:w" s="T154">i</ts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_549" n="HIAT:w" s="T155">oːltargaːlɨŋ</ts>
                  <nts id="Seg_550" n="HIAT:ip">.</nts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T167" id="Seg_553" n="HIAT:u" s="T156">
                  <ts e="T157" id="Seg_555" n="HIAT:w" s="T156">Üːdəmɨn</ts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_558" n="HIAT:w" s="T157">töːwaq</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_561" n="HIAT:w" s="T158">aːdʼat</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_564" n="HIAT:w" s="T159">i</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_567" n="HIAT:w" s="T160">aːwot</ts>
                  <nts id="Seg_568" n="HIAT:ip">,</nts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_571" n="HIAT:w" s="T161">a</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_574" n="HIAT:w" s="T162">Kalabokka</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_577" n="HIAT:w" s="T163">mešelačlä</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_580" n="HIAT:w" s="T164">eppa</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_583" n="HIAT:w" s="T165">i</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_586" n="HIAT:w" s="T166">oltargaːlɨŋ</ts>
                  <nts id="Seg_587" n="HIAT:ip">.</nts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T176" id="Seg_590" n="HIAT:u" s="T167">
                  <ts e="T168" id="Seg_592" n="HIAT:w" s="T167">Ästi</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_595" n="HIAT:w" s="T168">aːwot</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_598" n="HIAT:w" s="T169">soːqɨndʼaqi</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_601" n="HIAT:w" s="T170">Kalabokkanä</ts>
                  <nts id="Seg_602" n="HIAT:ip">,</nts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_605" n="HIAT:w" s="T171">oltaromt</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_608" n="HIAT:w" s="T172">ku</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_611" n="HIAT:w" s="T173">qaːdəbumbal</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_614" n="HIAT:w" s="T174">i</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_617" n="HIAT:w" s="T175">meːšelačant</ts>
                  <nts id="Seg_618" n="HIAT:ip">?</nts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_621" n="HIAT:u" s="T176">
                  <ts e="T177" id="Seg_623" n="HIAT:w" s="T176">A</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_626" n="HIAT:w" s="T177">Kalabokka</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_629" n="HIAT:w" s="T178">kadäšpat</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_632" n="HIAT:w" s="T179">mašim</ts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_635" n="HIAT:w" s="T180">čiča</ts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_637" n="HIAT:ip">(</nts>
                  <ts e="T182" id="Seg_639" n="HIAT:w" s="T181">tšitša</ts>
                  <nts id="Seg_640" n="HIAT:ip">)</nts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_643" n="HIAT:w" s="T182">loːssɨm</ts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_646" n="HIAT:w" s="T183">poːldʒirsa</ts>
                  <nts id="Seg_647" n="HIAT:ip">.</nts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T190" id="Seg_650" n="HIAT:u" s="T184">
                  <ts e="T185" id="Seg_652" n="HIAT:w" s="T184">I</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_655" n="HIAT:w" s="T185">mimbin</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_657" n="HIAT:ip">(</nts>
                  <ts e="T187" id="Seg_659" n="HIAT:w" s="T186">tilʼdi</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_662" n="HIAT:w" s="T187">nilʼdi</ts>
                  <nts id="Seg_663" n="HIAT:ip">)</nts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_666" n="HIAT:w" s="T188">loːssɨm</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_669" n="HIAT:w" s="T189">poːlǯirsa</ts>
                  <nts id="Seg_670" n="HIAT:ip">.</nts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T202" id="Seg_673" n="HIAT:u" s="T190">
                  <ts e="T191" id="Seg_675" n="HIAT:w" s="T190">Taːpti</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_678" n="HIAT:w" s="T191">tʼeːl</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_681" n="HIAT:w" s="T192">äːzɨt</ts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_684" n="HIAT:w" s="T193">aːwot</ts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_687" n="HIAT:w" s="T194">kuːnaj</ts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_690" n="HIAT:w" s="T195">aza</ts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_693" n="HIAT:w" s="T196">qwannaquij</ts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_695" n="HIAT:ip">(</nts>
                  <ts e="T198" id="Seg_697" n="HIAT:w" s="T197">qwannaːq</ts>
                  <nts id="Seg_698" n="HIAT:ip">)</nts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_701" n="HIAT:w" s="T198">maːt</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_704" n="HIAT:w" s="T199">tʼeladəgu</ts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_706" n="HIAT:ip">(</nts>
                  <ts e="T201" id="Seg_708" n="HIAT:w" s="T200">tʼaːladəgu</ts>
                  <nts id="Seg_709" n="HIAT:ip">)</nts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_712" n="HIAT:w" s="T201">qaːlaqi</ts>
                  <nts id="Seg_713" n="HIAT:ip">.</nts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T219" id="Seg_716" n="HIAT:u" s="T202">
                  <ts e="T203" id="Seg_718" n="HIAT:w" s="T202">Äzɨt</ts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_721" n="HIAT:w" s="T203">i</ts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_724" n="HIAT:w" s="T204">aːwɨt</ts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_727" n="HIAT:w" s="T205">čannɛ</ts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_730" n="HIAT:w" s="T206">tiːri</ts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_733" n="HIAT:w" s="T207">qwäːlɨt</ts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_736" n="HIAT:w" s="T208">nɨːndepom</ts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_739" n="HIAT:w" s="T209">aːnət</ts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_742" n="HIAT:w" s="T210">tomn</ts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_744" n="HIAT:ip">(</nts>
                  <ts e="T212" id="Seg_746" n="HIAT:w" s="T211">topt</ts>
                  <nts id="Seg_747" n="HIAT:ip">)</nts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_750" n="HIAT:w" s="T212">qamǯaːqij</ts>
                  <nts id="Seg_751" n="HIAT:ip">,</nts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_754" n="HIAT:w" s="T213">a</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_757" n="HIAT:w" s="T214">onǯi</ts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_760" n="HIAT:w" s="T215">maːdat</ts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_763" n="HIAT:w" s="T216">medoj</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_766" n="HIAT:w" s="T217">pʼlʼekkant</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_769" n="HIAT:w" s="T218">nɨledʼaːqij</ts>
                  <nts id="Seg_770" n="HIAT:ip">.</nts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T226" id="Seg_773" n="HIAT:u" s="T219">
                  <ts e="T220" id="Seg_775" n="HIAT:w" s="T219">Äzɨt</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_778" n="HIAT:w" s="T220">petʼ</ts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_781" n="HIAT:w" s="T221">iːndat</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_784" n="HIAT:w" s="T222">petʼse</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_787" n="HIAT:w" s="T223">nɨŋga</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_790" n="HIAT:w" s="T224">petʼse</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_793" n="HIAT:w" s="T225">nɨledʼa</ts>
                  <nts id="Seg_794" n="HIAT:ip">.</nts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T232" id="Seg_797" n="HIAT:u" s="T226">
                  <ts e="T227" id="Seg_799" n="HIAT:w" s="T226">A</ts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_802" n="HIAT:w" s="T227">pajat</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_805" n="HIAT:w" s="T228">aːwot</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_808" n="HIAT:w" s="T229">sommaj</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_811" n="HIAT:w" s="T230">poː</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_814" n="HIAT:w" s="T231">iːndət</ts>
                  <nts id="Seg_815" n="HIAT:ip">.</nts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T240" id="Seg_818" n="HIAT:u" s="T232">
                  <ts e="T233" id="Seg_820" n="HIAT:w" s="T232">A</ts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_823" n="HIAT:w" s="T233">Kalabokkanä</ts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_826" n="HIAT:w" s="T234">čeːnčaːqij</ts>
                  <nts id="Seg_827" n="HIAT:ip">:</nts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_830" n="HIAT:w" s="T235">a</ts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_833" n="HIAT:w" s="T236">tat</ts>
                  <nts id="Seg_834" n="HIAT:ip">,</nts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_837" n="HIAT:w" s="T237">Kalabokka</ts>
                  <nts id="Seg_838" n="HIAT:ip">,</nts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_841" n="HIAT:w" s="T238">maːdɨm</ts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_844" n="HIAT:w" s="T239">tʼäːzalǯimblel</ts>
                  <nts id="Seg_845" n="HIAT:ip">.</nts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T250" id="Seg_848" n="HIAT:u" s="T240">
                  <ts e="T241" id="Seg_850" n="HIAT:w" s="T240">Kalabokka</ts>
                  <nts id="Seg_851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_853" n="HIAT:w" s="T241">läːra</ts>
                  <nts id="Seg_854" n="HIAT:ip">,</nts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_857" n="HIAT:w" s="T242">poːndsä</ts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_860" n="HIAT:w" s="T243">qatälɨšpat</ts>
                  <nts id="Seg_861" n="HIAT:ip">,</nts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_864" n="HIAT:w" s="T244">qaimta</ts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_866" n="HIAT:ip">(</nts>
                  <ts e="T246" id="Seg_868" n="HIAT:w" s="T245">öːntʼit</ts>
                  <nts id="Seg_869" n="HIAT:ip">)</nts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_871" n="HIAT:ip">(</nts>
                  <ts e="T247" id="Seg_873" n="HIAT:w" s="T246">öntʼäːqi</ts>
                  <nts id="Seg_874" n="HIAT:ip">)</nts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_877" n="HIAT:w" s="T247">öːntʼäːdin</ts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_880" n="HIAT:w" s="T248">Pöːnegessa</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_883" n="HIAT:w" s="T249">čaːǯa</ts>
                  <nts id="Seg_884" n="HIAT:ip">.</nts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T263" id="Seg_887" n="HIAT:u" s="T250">
                  <ts e="T251" id="Seg_889" n="HIAT:w" s="T250">Maːt</ts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_892" n="HIAT:w" s="T251">šeːrešpa</ts>
                  <nts id="Seg_893" n="HIAT:ip">,</nts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_896" n="HIAT:w" s="T252">Pöːnegessa</ts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_899" n="HIAT:w" s="T253">maːdat</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_902" n="HIAT:w" s="T254">tom</ts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_905" n="HIAT:w" s="T255">aːzannɨt</ts>
                  <nts id="Seg_906" n="HIAT:ip">,</nts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_909" n="HIAT:w" s="T256">qwälɨn</ts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_912" n="HIAT:w" s="T257">nɨːndepont</ts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_915" n="HIAT:w" s="T258">tʼiːɣelǯə</ts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_918" n="HIAT:w" s="T259">i</ts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_921" n="HIAT:w" s="T260">nʼuqqɨlwallä</ts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_924" n="HIAT:w" s="T261">eːlle</ts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_927" n="HIAT:w" s="T262">aːldʼe</ts>
                  <nts id="Seg_928" n="HIAT:ip">.</nts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T276" id="Seg_931" n="HIAT:u" s="T263">
                  <ts e="T264" id="Seg_933" n="HIAT:w" s="T263">I</ts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_936" n="HIAT:w" s="T264">našt</ts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_939" n="HIAT:w" s="T265">qaron</ts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_942" n="HIAT:w" s="T266">äzɨt</ts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_945" n="HIAT:w" s="T267">i</ts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_948" n="HIAT:w" s="T268">aːwot</ts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_951" n="HIAT:w" s="T269">äzɨt</ts>
                  <nts id="Seg_952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_954" n="HIAT:w" s="T270">petʼsä</ts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_957" n="HIAT:w" s="T271">Pöːnegessan</ts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_960" n="HIAT:w" s="T272">olom</ts>
                  <nts id="Seg_961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_963" n="HIAT:w" s="T273">padʼannɨt</ts>
                  <nts id="Seg_964" n="HIAT:ip">,</nts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_967" n="HIAT:w" s="T274">Pöːnegessam</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_970" n="HIAT:w" s="T275">qwannaːqi</ts>
                  <nts id="Seg_971" n="HIAT:ip">.</nts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T280" id="Seg_974" n="HIAT:u" s="T276">
                  <ts e="T277" id="Seg_976" n="HIAT:w" s="T276">Poːnä</ts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_979" n="HIAT:w" s="T277">warq</ts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_982" n="HIAT:w" s="T278">tüːʒomom</ts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_985" n="HIAT:w" s="T279">pannaːqi</ts>
                  <nts id="Seg_986" n="HIAT:ip">.</nts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T285" id="Seg_989" n="HIAT:u" s="T280">
                  <ts e="T281" id="Seg_991" n="HIAT:w" s="T280">Pöːnegessam</ts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_994" n="HIAT:w" s="T281">na</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_997" n="HIAT:w" s="T282">warq</ts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1000" n="HIAT:w" s="T283">tüːʒomnä</ts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1003" n="HIAT:w" s="T284">tʼädʒaːqi</ts>
                  <nts id="Seg_1004" n="HIAT:ip">.</nts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T288" id="Seg_1007" n="HIAT:u" s="T285">
                  <ts e="T286" id="Seg_1009" n="HIAT:w" s="T285">Pöːnegessam</ts>
                  <nts id="Seg_1010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1012" n="HIAT:w" s="T286">tü</ts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1015" n="HIAT:w" s="T287">awešpat</ts>
                  <nts id="Seg_1016" n="HIAT:ip">.</nts>
                  <nts id="Seg_1017" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T295" id="Seg_1019" n="HIAT:u" s="T288">
                  <ts e="T289" id="Seg_1021" n="HIAT:w" s="T288">Pöːnegessan</ts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1024" n="HIAT:w" s="T289">teulat</ts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1027" n="HIAT:w" s="T290">qolʼcʼiwatplä</ts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1030" n="HIAT:w" s="T291">tüːɣondok</ts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1033" n="HIAT:w" s="T292">toː</ts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1036" n="HIAT:w" s="T293">eːle</ts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1039" n="HIAT:w" s="T294">paqtɨrnaːtt</ts>
                  <nts id="Seg_1040" n="HIAT:ip">.</nts>
                  <nts id="Seg_1041" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T303" id="Seg_1043" n="HIAT:u" s="T295">
                  <ts e="T296" id="Seg_1045" n="HIAT:w" s="T295">Pöːnegessam</ts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1048" n="HIAT:w" s="T296">tüː</ts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1051" n="HIAT:w" s="T297">amnɨt</ts>
                  <nts id="Seg_1052" n="HIAT:ip">,</nts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1055" n="HIAT:w" s="T298">i</ts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1058" n="HIAT:w" s="T299">teperta</ts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1061" n="HIAT:w" s="T300">eːlaːtt</ts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1064" n="HIAT:w" s="T301">i</ts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1067" n="HIAT:w" s="T302">warkaːtt</ts>
                  <nts id="Seg_1068" n="HIAT:ip">.</nts>
                  <nts id="Seg_1069" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T311" id="Seg_1071" n="HIAT:u" s="T303">
                  <ts e="T304" id="Seg_1073" n="HIAT:w" s="T303">I</ts>
                  <nts id="Seg_1074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1076" n="HIAT:w" s="T304">Kalabokka</ts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1079" n="HIAT:w" s="T305">naj</ts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1082" n="HIAT:w" s="T306">orumba</ts>
                  <nts id="Seg_1083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1085" n="HIAT:w" s="T307">i</ts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1088" n="HIAT:w" s="T308">Pönegessala</ts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1091" n="HIAT:w" s="T309">naj</ts>
                  <nts id="Seg_1092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1094" n="HIAT:w" s="T310">tʼäŋgwaːtt</ts>
                  <nts id="Seg_1095" n="HIAT:ip">.</nts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T311" id="Seg_1097" n="sc" s="T1">
               <ts e="T2" id="Seg_1099" n="e" s="T1">Elɨkwaːt </ts>
               <ts e="T3" id="Seg_1101" n="e" s="T2">warkɨkwaːt </ts>
               <ts e="T4" id="Seg_1103" n="e" s="T3">ära </ts>
               <ts e="T5" id="Seg_1105" n="e" s="T4">pajassəqi. </ts>
               <ts e="T6" id="Seg_1107" n="e" s="T5">Tabɨštjannaːn </ts>
               <ts e="T7" id="Seg_1109" n="e" s="T6">eːkwa </ts>
               <ts e="T8" id="Seg_1111" n="e" s="T7">iːdi. </ts>
               <ts e="T9" id="Seg_1113" n="e" s="T8">Nept </ts>
               <ts e="T10" id="Seg_1115" n="e" s="T9">Kalabokka. </ts>
               <ts e="T11" id="Seg_1117" n="e" s="T10">əːtət </ts>
               <ts e="T12" id="Seg_1119" n="e" s="T11">meːta. </ts>
               <ts e="T13" id="Seg_1121" n="e" s="T12">Äzawət </ts>
               <ts e="T14" id="Seg_1123" n="e" s="T13">kwannatən </ts>
               <ts e="T15" id="Seg_1125" n="e" s="T14">poːjergu </ts>
               <ts e="T16" id="Seg_1127" n="e" s="T15">šəːtt </ts>
               <ts e="T17" id="Seg_1129" n="e" s="T16">a </ts>
               <ts e="T18" id="Seg_1131" n="e" s="T17">Kalabokkanä </ts>
               <ts e="T19" id="Seg_1133" n="e" s="T18">tʼičaːtt. </ts>
               <ts e="T20" id="Seg_1135" n="e" s="T19">Tan </ts>
               <ts e="T21" id="Seg_1137" n="e" s="T20">maːtqən </ts>
               <ts e="T22" id="Seg_1139" n="e" s="T21">küːnaŋ </ts>
               <ts e="T23" id="Seg_1141" n="e" s="T22">warqɨk </ts>
               <ts e="T24" id="Seg_1143" n="e" s="T23">maːtəm </ts>
               <ts e="T25" id="Seg_1145" n="e" s="T24">əːk </ts>
               <ts e="T26" id="Seg_1147" n="e" s="T25">tʼazəlǯimbet. </ts>
               <ts e="T27" id="Seg_1149" n="e" s="T26">Tʼaːzəlǯimendǯal </ts>
               <ts e="T28" id="Seg_1151" n="e" s="T27">Pöːnegessa </ts>
               <ts e="T29" id="Seg_1153" n="e" s="T28">töːnǯa </ts>
               <ts e="T30" id="Seg_1155" n="e" s="T29">i </ts>
               <ts e="T31" id="Seg_1157" n="e" s="T30">aːmǯa </ts>
               <ts e="T32" id="Seg_1159" n="e" s="T31">št. </ts>
               <ts e="T33" id="Seg_1161" n="e" s="T32">Äzt </ts>
               <ts e="T34" id="Seg_1163" n="e" s="T33">i </ts>
               <ts e="T35" id="Seg_1165" n="e" s="T34">aːwət </ts>
               <ts e="T36" id="Seg_1167" n="e" s="T35">qwannaqi </ts>
               <ts e="T37" id="Seg_1169" n="e" s="T36">šött </ts>
               <ts e="T38" id="Seg_1171" n="e" s="T37">poːjergu. </ts>
               <ts e="T39" id="Seg_1173" n="e" s="T38">Kalabokka </ts>
               <ts e="T40" id="Seg_1175" n="e" s="T39">qaːla </ts>
               <ts e="T41" id="Seg_1177" n="e" s="T40">pʼalgaːlɨŋ. </ts>
               <ts e="T42" id="Seg_1179" n="e" s="T41">Aːdʼa </ts>
               <ts e="T43" id="Seg_1181" n="e" s="T42">(aːdʼe) </ts>
               <ts e="T44" id="Seg_1183" n="e" s="T43">i </ts>
               <ts e="T45" id="Seg_1185" n="e" s="T44">aːwa </ts>
               <ts e="T46" id="Seg_1187" n="e" s="T45">qaj </ts>
               <ts e="T47" id="Seg_1189" n="e" s="T46">čeːnčaːqi </ts>
               <ts e="T48" id="Seg_1191" n="e" s="T47">Pöːnegessa </ts>
               <ts e="T49" id="Seg_1193" n="e" s="T48">töːnǯa </ts>
               <ts e="T50" id="Seg_1195" n="e" s="T49">maššim </ts>
               <ts e="T51" id="Seg_1197" n="e" s="T50">amgu. </ts>
               <ts e="T52" id="Seg_1199" n="e" s="T51">A </ts>
               <ts e="T53" id="Seg_1201" n="e" s="T52">matt </ts>
               <ts e="T54" id="Seg_1203" n="e" s="T53">kəba </ts>
               <ts e="T55" id="Seg_1205" n="e" s="T54">pauze </ts>
               <ts e="T56" id="Seg_1207" n="e" s="T55">tap </ts>
               <ts e="T57" id="Seg_1209" n="e" s="T56">Pöːnegessam </ts>
               <ts e="T58" id="Seg_1211" n="e" s="T57">maǯenǯaw. </ts>
               <ts e="T59" id="Seg_1213" n="e" s="T58">Kəba </ts>
               <ts e="T60" id="Seg_1215" n="e" s="T59">pawəmt </ts>
               <ts e="T61" id="Seg_1217" n="e" s="T60">sälʼešpət </ts>
               <ts e="T62" id="Seg_1219" n="e" s="T61">a </ts>
               <ts e="T63" id="Seg_1221" n="e" s="T62">onǯa </ts>
               <ts e="T64" id="Seg_1223" n="e" s="T63">läːra. </ts>
               <ts e="T65" id="Seg_1225" n="e" s="T64">Madəmt </ts>
               <ts e="T66" id="Seg_1227" n="e" s="T65">tʼaːzəlǯimbət. </ts>
               <ts e="T67" id="Seg_1229" n="e" s="T66">Qatʼe </ts>
               <ts e="T68" id="Seg_1231" n="e" s="T67">mɨlä </ts>
               <ts e="T69" id="Seg_1233" n="e" s="T68">öntʼit </ts>
               <ts e="T70" id="Seg_1235" n="e" s="T69">qajta </ts>
               <ts e="T71" id="Seg_1237" n="e" s="T70">čaːǯa </ts>
               <ts e="T72" id="Seg_1239" n="e" s="T71">toblantse </ts>
               <ts e="T73" id="Seg_1241" n="e" s="T72">muǯuwatple. </ts>
               <ts e="T74" id="Seg_1243" n="e" s="T73">Kalabokka </ts>
               <ts e="T75" id="Seg_1245" n="e" s="T74">kəba </ts>
               <ts e="T76" id="Seg_1247" n="e" s="T75">paundse </ts>
               <ts e="T77" id="Seg_1249" n="e" s="T76">koptən </ts>
               <ts e="T78" id="Seg_1251" n="e" s="T77">ɨːlont </ts>
               <ts e="T79" id="Seg_1253" n="e" s="T78">atteda. </ts>
               <ts e="T80" id="Seg_1255" n="e" s="T79">Pöːnegessa </ts>
               <ts e="T81" id="Seg_1257" n="e" s="T80">maːt </ts>
               <ts e="T82" id="Seg_1259" n="e" s="T81">šeːrna </ts>
               <ts e="T83" id="Seg_1261" n="e" s="T82">Kalabokkam </ts>
               <ts e="T84" id="Seg_1263" n="e" s="T83">koptən </ts>
               <ts e="T85" id="Seg_1265" n="e" s="T84">ɨːlokonto </ts>
               <ts e="T86" id="Seg_1267" n="e" s="T85">čatčit </ts>
               <ts e="T87" id="Seg_1269" n="e" s="T86">onǯe </ts>
               <ts e="T88" id="Seg_1271" n="e" s="T87">piːreqɨnt </ts>
               <ts e="T89" id="Seg_1273" n="e" s="T88">i </ts>
               <ts e="T90" id="Seg_1275" n="e" s="T89">Kalabokkam </ts>
               <ts e="T91" id="Seg_1277" n="e" s="T90">poːlledit. </ts>
               <ts e="T92" id="Seg_1279" n="e" s="T91">Kalabokkannaːn </ts>
               <ts e="T93" id="Seg_1281" n="e" s="T92">eːja </ts>
               <ts e="T94" id="Seg_1283" n="e" s="T93">kəba </ts>
               <ts e="T95" id="Seg_1285" n="e" s="T94">paw. </ts>
               <ts e="T96" id="Seg_1287" n="e" s="T95">Aːmta </ts>
               <ts e="T97" id="Seg_1289" n="e" s="T96">Pöːnegessan </ts>
               <ts e="T98" id="Seg_1291" n="e" s="T97">pʼargeqən </ts>
               <ts e="T99" id="Seg_1293" n="e" s="T98">kəba </ts>
               <ts e="T100" id="Seg_1295" n="e" s="T99">paontsä </ts>
               <ts e="T101" id="Seg_1297" n="e" s="T100">pʼargeqɨnt </ts>
               <ts e="T102" id="Seg_1299" n="e" s="T101">tä </ts>
               <ts e="T103" id="Seg_1301" n="e" s="T102">toː </ts>
               <ts e="T104" id="Seg_1303" n="e" s="T103">poǯalǯešpat. </ts>
               <ts e="T105" id="Seg_1305" n="e" s="T104">Pöːnegessa </ts>
               <ts e="T106" id="Seg_1307" n="e" s="T105">čeːnča. </ts>
               <ts e="T107" id="Seg_1309" n="e" s="T106">Kalabokka </ts>
               <ts e="T108" id="Seg_1311" n="e" s="T107">Kalabokka </ts>
               <ts e="T109" id="Seg_1313" n="e" s="T108">mannani </ts>
               <ts e="T110" id="Seg_1315" n="e" s="T109">pʼargʼew </ts>
               <ts e="T111" id="Seg_1317" n="e" s="T110">ək </ts>
               <ts e="T112" id="Seg_1319" n="e" s="T111">poǯalǯešplel. </ts>
               <ts e="T113" id="Seg_1321" n="e" s="T112">A </ts>
               <ts e="T114" id="Seg_1323" n="e" s="T113">Kalabokka </ts>
               <ts e="T115" id="Seg_1325" n="e" s="T114">pʼargʼeqɨn </ts>
               <ts e="T116" id="Seg_1327" n="e" s="T115">na </ts>
               <ts e="T117" id="Seg_1329" n="e" s="T116">kɨbɨʒo </ts>
               <ts e="T118" id="Seg_1331" n="e" s="T117">paokantsä </ts>
               <ts e="T119" id="Seg_1333" n="e" s="T118">Pöːnegessan </ts>
               <ts e="T120" id="Seg_1335" n="e" s="T119">šiːdʼem </ts>
               <ts e="T121" id="Seg_1337" n="e" s="T120">tʼoqollä </ts>
               <ts e="T122" id="Seg_1339" n="e" s="T121">qwattɨt. </ts>
               <ts e="T123" id="Seg_1341" n="e" s="T122">A </ts>
               <ts e="T124" id="Seg_1343" n="e" s="T123">Pöːnegessa </ts>
               <ts e="T125" id="Seg_1345" n="e" s="T124">čeːnča: </ts>
               <ts e="T126" id="Seg_1347" n="e" s="T125">Kalabokka, </ts>
               <ts e="T127" id="Seg_1349" n="e" s="T126">Kalabokka </ts>
               <ts e="T128" id="Seg_1351" n="e" s="T127">matšiːdʼem </ts>
               <ts e="T129" id="Seg_1353" n="e" s="T128">ək </ts>
               <ts e="T130" id="Seg_1355" n="e" s="T129">tʼoqolešplel. </ts>
               <ts e="T131" id="Seg_1357" n="e" s="T130">Mat </ts>
               <ts e="T132" id="Seg_1359" n="e" s="T131">tašt </ts>
               <ts e="T133" id="Seg_1361" n="e" s="T132">töːdenǯak </ts>
               <ts e="T134" id="Seg_1363" n="e" s="T133">št. </ts>
               <ts e="T135" id="Seg_1365" n="e" s="T134">Pöːnegessa </ts>
               <ts e="T136" id="Seg_1367" n="e" s="T135">Kalabokkam </ts>
               <ts e="T137" id="Seg_1369" n="e" s="T136">tötət </ts>
               <ts e="T138" id="Seg_1371" n="e" s="T137">moqonä </ts>
               <ts e="T139" id="Seg_1373" n="e" s="T138">eːle. </ts>
               <ts e="T140" id="Seg_1375" n="e" s="T139">Kalabokkam </ts>
               <ts e="T141" id="Seg_1377" n="e" s="T140">olɨt </ts>
               <ts e="T142" id="Seg_1379" n="e" s="T141">tarɨt </ts>
               <ts e="T143" id="Seg_1381" n="e" s="T142">Pöːnegessa </ts>
               <ts e="T144" id="Seg_1383" n="e" s="T143">köːgalbat, </ts>
               <ts e="T145" id="Seg_1385" n="e" s="T144">oltargaːlɨŋ </ts>
               <ts e="T146" id="Seg_1387" n="e" s="T145">qaːlɨmba. </ts>
               <ts e="T147" id="Seg_1389" n="e" s="T146">Pöːnegessa </ts>
               <ts e="T148" id="Seg_1391" n="e" s="T147">poːnä </ts>
               <ts e="T149" id="Seg_1393" n="e" s="T148">čaːnǯa </ts>
               <ts e="T150" id="Seg_1395" n="e" s="T149">i </ts>
               <ts e="T151" id="Seg_1397" n="e" s="T150">qwann. </ts>
               <ts e="T152" id="Seg_1399" n="e" s="T151">Kalabokka </ts>
               <ts e="T153" id="Seg_1401" n="e" s="T152">mešelačlä </ts>
               <ts e="T154" id="Seg_1403" n="e" s="T153">qaːla </ts>
               <ts e="T155" id="Seg_1405" n="e" s="T154">i </ts>
               <ts e="T156" id="Seg_1407" n="e" s="T155">oːltargaːlɨŋ. </ts>
               <ts e="T157" id="Seg_1409" n="e" s="T156">Üːdəmɨn </ts>
               <ts e="T158" id="Seg_1411" n="e" s="T157">töːwaq </ts>
               <ts e="T159" id="Seg_1413" n="e" s="T158">aːdʼat </ts>
               <ts e="T160" id="Seg_1415" n="e" s="T159">i </ts>
               <ts e="T161" id="Seg_1417" n="e" s="T160">aːwot, </ts>
               <ts e="T162" id="Seg_1419" n="e" s="T161">a </ts>
               <ts e="T163" id="Seg_1421" n="e" s="T162">Kalabokka </ts>
               <ts e="T164" id="Seg_1423" n="e" s="T163">mešelačlä </ts>
               <ts e="T165" id="Seg_1425" n="e" s="T164">eppa </ts>
               <ts e="T166" id="Seg_1427" n="e" s="T165">i </ts>
               <ts e="T167" id="Seg_1429" n="e" s="T166">oltargaːlɨŋ. </ts>
               <ts e="T168" id="Seg_1431" n="e" s="T167">Ästi </ts>
               <ts e="T169" id="Seg_1433" n="e" s="T168">aːwot </ts>
               <ts e="T170" id="Seg_1435" n="e" s="T169">soːqɨndʼaqi </ts>
               <ts e="T171" id="Seg_1437" n="e" s="T170">Kalabokkanä, </ts>
               <ts e="T172" id="Seg_1439" n="e" s="T171">oltaromt </ts>
               <ts e="T173" id="Seg_1441" n="e" s="T172">ku </ts>
               <ts e="T174" id="Seg_1443" n="e" s="T173">qaːdəbumbal </ts>
               <ts e="T175" id="Seg_1445" n="e" s="T174">i </ts>
               <ts e="T176" id="Seg_1447" n="e" s="T175">meːšelačant? </ts>
               <ts e="T177" id="Seg_1449" n="e" s="T176">A </ts>
               <ts e="T178" id="Seg_1451" n="e" s="T177">Kalabokka </ts>
               <ts e="T179" id="Seg_1453" n="e" s="T178">kadäšpat </ts>
               <ts e="T180" id="Seg_1455" n="e" s="T179">mašim </ts>
               <ts e="T181" id="Seg_1457" n="e" s="T180">čiča </ts>
               <ts e="T182" id="Seg_1459" n="e" s="T181">(tšitša) </ts>
               <ts e="T183" id="Seg_1461" n="e" s="T182">loːssɨm </ts>
               <ts e="T184" id="Seg_1463" n="e" s="T183">poːldʒirsa. </ts>
               <ts e="T185" id="Seg_1465" n="e" s="T184">I </ts>
               <ts e="T186" id="Seg_1467" n="e" s="T185">mimbin </ts>
               <ts e="T187" id="Seg_1469" n="e" s="T186">(tilʼdi </ts>
               <ts e="T188" id="Seg_1471" n="e" s="T187">nilʼdi) </ts>
               <ts e="T189" id="Seg_1473" n="e" s="T188">loːssɨm </ts>
               <ts e="T190" id="Seg_1475" n="e" s="T189">poːlǯirsa. </ts>
               <ts e="T191" id="Seg_1477" n="e" s="T190">Taːpti </ts>
               <ts e="T192" id="Seg_1479" n="e" s="T191">tʼeːl </ts>
               <ts e="T193" id="Seg_1481" n="e" s="T192">äːzɨt </ts>
               <ts e="T194" id="Seg_1483" n="e" s="T193">aːwot </ts>
               <ts e="T195" id="Seg_1485" n="e" s="T194">kuːnaj </ts>
               <ts e="T196" id="Seg_1487" n="e" s="T195">aza </ts>
               <ts e="T197" id="Seg_1489" n="e" s="T196">qwannaquij </ts>
               <ts e="T198" id="Seg_1491" n="e" s="T197">(qwannaːq) </ts>
               <ts e="T199" id="Seg_1493" n="e" s="T198">maːt </ts>
               <ts e="T200" id="Seg_1495" n="e" s="T199">tʼeladəgu </ts>
               <ts e="T201" id="Seg_1497" n="e" s="T200">(tʼaːladəgu) </ts>
               <ts e="T202" id="Seg_1499" n="e" s="T201">qaːlaqi. </ts>
               <ts e="T203" id="Seg_1501" n="e" s="T202">Äzɨt </ts>
               <ts e="T204" id="Seg_1503" n="e" s="T203">i </ts>
               <ts e="T205" id="Seg_1505" n="e" s="T204">aːwɨt </ts>
               <ts e="T206" id="Seg_1507" n="e" s="T205">čannɛ </ts>
               <ts e="T207" id="Seg_1509" n="e" s="T206">tiːri </ts>
               <ts e="T208" id="Seg_1511" n="e" s="T207">qwäːlɨt </ts>
               <ts e="T209" id="Seg_1513" n="e" s="T208">nɨːndepom </ts>
               <ts e="T210" id="Seg_1515" n="e" s="T209">aːnət </ts>
               <ts e="T211" id="Seg_1517" n="e" s="T210">tomn </ts>
               <ts e="T212" id="Seg_1519" n="e" s="T211">(topt) </ts>
               <ts e="T213" id="Seg_1521" n="e" s="T212">qamǯaːqij, </ts>
               <ts e="T214" id="Seg_1523" n="e" s="T213">a </ts>
               <ts e="T215" id="Seg_1525" n="e" s="T214">onǯi </ts>
               <ts e="T216" id="Seg_1527" n="e" s="T215">maːdat </ts>
               <ts e="T217" id="Seg_1529" n="e" s="T216">medoj </ts>
               <ts e="T218" id="Seg_1531" n="e" s="T217">pʼlʼekkant </ts>
               <ts e="T219" id="Seg_1533" n="e" s="T218">nɨledʼaːqij. </ts>
               <ts e="T220" id="Seg_1535" n="e" s="T219">Äzɨt </ts>
               <ts e="T221" id="Seg_1537" n="e" s="T220">petʼ </ts>
               <ts e="T222" id="Seg_1539" n="e" s="T221">iːndat </ts>
               <ts e="T223" id="Seg_1541" n="e" s="T222">petʼse </ts>
               <ts e="T224" id="Seg_1543" n="e" s="T223">nɨŋga </ts>
               <ts e="T225" id="Seg_1545" n="e" s="T224">petʼse </ts>
               <ts e="T226" id="Seg_1547" n="e" s="T225">nɨledʼa. </ts>
               <ts e="T227" id="Seg_1549" n="e" s="T226">A </ts>
               <ts e="T228" id="Seg_1551" n="e" s="T227">pajat </ts>
               <ts e="T229" id="Seg_1553" n="e" s="T228">aːwot </ts>
               <ts e="T230" id="Seg_1555" n="e" s="T229">sommaj </ts>
               <ts e="T231" id="Seg_1557" n="e" s="T230">poː </ts>
               <ts e="T232" id="Seg_1559" n="e" s="T231">iːndət. </ts>
               <ts e="T233" id="Seg_1561" n="e" s="T232">A </ts>
               <ts e="T234" id="Seg_1563" n="e" s="T233">Kalabokkanä </ts>
               <ts e="T235" id="Seg_1565" n="e" s="T234">čeːnčaːqij: </ts>
               <ts e="T236" id="Seg_1567" n="e" s="T235">a </ts>
               <ts e="T237" id="Seg_1569" n="e" s="T236">tat, </ts>
               <ts e="T238" id="Seg_1571" n="e" s="T237">Kalabokka, </ts>
               <ts e="T239" id="Seg_1573" n="e" s="T238">maːdɨm </ts>
               <ts e="T240" id="Seg_1575" n="e" s="T239">tʼäːzalǯimblel. </ts>
               <ts e="T241" id="Seg_1577" n="e" s="T240">Kalabokka </ts>
               <ts e="T242" id="Seg_1579" n="e" s="T241">läːra, </ts>
               <ts e="T243" id="Seg_1581" n="e" s="T242">poːndsä </ts>
               <ts e="T244" id="Seg_1583" n="e" s="T243">qatälɨšpat, </ts>
               <ts e="T245" id="Seg_1585" n="e" s="T244">qaimta </ts>
               <ts e="T246" id="Seg_1587" n="e" s="T245">(öːntʼit) </ts>
               <ts e="T247" id="Seg_1589" n="e" s="T246">(öntʼäːqi) </ts>
               <ts e="T248" id="Seg_1591" n="e" s="T247">öːntʼäːdin </ts>
               <ts e="T249" id="Seg_1593" n="e" s="T248">Pöːnegessa </ts>
               <ts e="T250" id="Seg_1595" n="e" s="T249">čaːǯa. </ts>
               <ts e="T251" id="Seg_1597" n="e" s="T250">Maːt </ts>
               <ts e="T252" id="Seg_1599" n="e" s="T251">šeːrešpa, </ts>
               <ts e="T253" id="Seg_1601" n="e" s="T252">Pöːnegessa </ts>
               <ts e="T254" id="Seg_1603" n="e" s="T253">maːdat </ts>
               <ts e="T255" id="Seg_1605" n="e" s="T254">tom </ts>
               <ts e="T256" id="Seg_1607" n="e" s="T255">aːzannɨt, </ts>
               <ts e="T257" id="Seg_1609" n="e" s="T256">qwälɨn </ts>
               <ts e="T258" id="Seg_1611" n="e" s="T257">nɨːndepont </ts>
               <ts e="T259" id="Seg_1613" n="e" s="T258">tʼiːɣelǯə </ts>
               <ts e="T260" id="Seg_1615" n="e" s="T259">i </ts>
               <ts e="T261" id="Seg_1617" n="e" s="T260">nʼuqqɨlwallä </ts>
               <ts e="T262" id="Seg_1619" n="e" s="T261">eːlle </ts>
               <ts e="T263" id="Seg_1621" n="e" s="T262">aːldʼe. </ts>
               <ts e="T264" id="Seg_1623" n="e" s="T263">I </ts>
               <ts e="T265" id="Seg_1625" n="e" s="T264">našt </ts>
               <ts e="T266" id="Seg_1627" n="e" s="T265">qaron </ts>
               <ts e="T267" id="Seg_1629" n="e" s="T266">äzɨt </ts>
               <ts e="T268" id="Seg_1631" n="e" s="T267">i </ts>
               <ts e="T269" id="Seg_1633" n="e" s="T268">aːwot </ts>
               <ts e="T270" id="Seg_1635" n="e" s="T269">äzɨt </ts>
               <ts e="T271" id="Seg_1637" n="e" s="T270">petʼsä </ts>
               <ts e="T272" id="Seg_1639" n="e" s="T271">Pöːnegessan </ts>
               <ts e="T273" id="Seg_1641" n="e" s="T272">olom </ts>
               <ts e="T274" id="Seg_1643" n="e" s="T273">padʼannɨt, </ts>
               <ts e="T275" id="Seg_1645" n="e" s="T274">Pöːnegessam </ts>
               <ts e="T276" id="Seg_1647" n="e" s="T275">qwannaːqi. </ts>
               <ts e="T277" id="Seg_1649" n="e" s="T276">Poːnä </ts>
               <ts e="T278" id="Seg_1651" n="e" s="T277">warq </ts>
               <ts e="T279" id="Seg_1653" n="e" s="T278">tüːʒomom </ts>
               <ts e="T280" id="Seg_1655" n="e" s="T279">pannaːqi. </ts>
               <ts e="T281" id="Seg_1657" n="e" s="T280">Pöːnegessam </ts>
               <ts e="T282" id="Seg_1659" n="e" s="T281">na </ts>
               <ts e="T283" id="Seg_1661" n="e" s="T282">warq </ts>
               <ts e="T284" id="Seg_1663" n="e" s="T283">tüːʒomnä </ts>
               <ts e="T285" id="Seg_1665" n="e" s="T284">tʼädʒaːqi. </ts>
               <ts e="T286" id="Seg_1667" n="e" s="T285">Pöːnegessam </ts>
               <ts e="T287" id="Seg_1669" n="e" s="T286">tü </ts>
               <ts e="T288" id="Seg_1671" n="e" s="T287">awešpat. </ts>
               <ts e="T289" id="Seg_1673" n="e" s="T288">Pöːnegessan </ts>
               <ts e="T290" id="Seg_1675" n="e" s="T289">teulat </ts>
               <ts e="T291" id="Seg_1677" n="e" s="T290">qolʼcʼiwatplä </ts>
               <ts e="T292" id="Seg_1679" n="e" s="T291">tüːɣondok </ts>
               <ts e="T293" id="Seg_1681" n="e" s="T292">toː </ts>
               <ts e="T294" id="Seg_1683" n="e" s="T293">eːle </ts>
               <ts e="T295" id="Seg_1685" n="e" s="T294">paqtɨrnaːtt. </ts>
               <ts e="T296" id="Seg_1687" n="e" s="T295">Pöːnegessam </ts>
               <ts e="T297" id="Seg_1689" n="e" s="T296">tüː </ts>
               <ts e="T298" id="Seg_1691" n="e" s="T297">amnɨt, </ts>
               <ts e="T299" id="Seg_1693" n="e" s="T298">i </ts>
               <ts e="T300" id="Seg_1695" n="e" s="T299">teperta </ts>
               <ts e="T301" id="Seg_1697" n="e" s="T300">eːlaːtt </ts>
               <ts e="T302" id="Seg_1699" n="e" s="T301">i </ts>
               <ts e="T303" id="Seg_1701" n="e" s="T302">warkaːtt. </ts>
               <ts e="T304" id="Seg_1703" n="e" s="T303">I </ts>
               <ts e="T305" id="Seg_1705" n="e" s="T304">Kalabokka </ts>
               <ts e="T306" id="Seg_1707" n="e" s="T305">naj </ts>
               <ts e="T307" id="Seg_1709" n="e" s="T306">orumba </ts>
               <ts e="T308" id="Seg_1711" n="e" s="T307">i </ts>
               <ts e="T309" id="Seg_1713" n="e" s="T308">Pönegessala </ts>
               <ts e="T310" id="Seg_1715" n="e" s="T309">naj </ts>
               <ts e="T311" id="Seg_1717" n="e" s="T310">tʼäŋgwaːtt. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_1718" s="T1">TFF_1967_KalabokkaPoenegessa1_flk.001 (001.001)</ta>
            <ta e="T8" id="Seg_1719" s="T5">TFF_1967_KalabokkaPoenegessa1_flk.002 (001.002)</ta>
            <ta e="T10" id="Seg_1720" s="T8">TFF_1967_KalabokkaPoenegessa1_flk.003 (001.003)</ta>
            <ta e="T12" id="Seg_1721" s="T10">TFF_1967_KalabokkaPoenegessa1_flk.004 (001.004)</ta>
            <ta e="T19" id="Seg_1722" s="T12">TFF_1967_KalabokkaPoenegessa1_flk.005 (001.005)</ta>
            <ta e="T26" id="Seg_1723" s="T19">TFF_1967_KalabokkaPoenegessa1_flk.006 (001.006)</ta>
            <ta e="T32" id="Seg_1724" s="T26">TFF_1967_KalabokkaPoenegessa1_flk.007 (001.007)</ta>
            <ta e="T38" id="Seg_1725" s="T32">TFF_1967_KalabokkaPoenegessa1_flk.008 (001.008)</ta>
            <ta e="T41" id="Seg_1726" s="T38">TFF_1967_KalabokkaPoenegessa1_flk.009 (001.009)</ta>
            <ta e="T51" id="Seg_1727" s="T41">TFF_1967_KalabokkaPoenegessa1_flk.010 (001.010)</ta>
            <ta e="T58" id="Seg_1728" s="T51">TFF_1967_KalabokkaPoenegessa1_flk.011 (001.011)</ta>
            <ta e="T64" id="Seg_1729" s="T58">TFF_1967_KalabokkaPoenegessa1_flk.012 (001.012)</ta>
            <ta e="T66" id="Seg_1730" s="T64">TFF_1967_KalabokkaPoenegessa1_flk.013 (001.013)</ta>
            <ta e="T73" id="Seg_1731" s="T66">TFF_1967_KalabokkaPoenegessa1_flk.014 (001.014)</ta>
            <ta e="T79" id="Seg_1732" s="T73">TFF_1967_KalabokkaPoenegessa1_flk.015 (001.015)</ta>
            <ta e="T91" id="Seg_1733" s="T79">TFF_1967_KalabokkaPoenegessa1_flk.016 (001.016)</ta>
            <ta e="T95" id="Seg_1734" s="T91">TFF_1967_KalabokkaPoenegessa1_flk.017 (001.017)</ta>
            <ta e="T104" id="Seg_1735" s="T95">TFF_1967_KalabokkaPoenegessa1_flk.018 (001.018)</ta>
            <ta e="T106" id="Seg_1736" s="T104">TFF_1967_KalabokkaPoenegessa1_flk.019 (001.019)</ta>
            <ta e="T112" id="Seg_1737" s="T106">TFF_1967_KalabokkaPoenegessa1_flk.020 (001.020)</ta>
            <ta e="T122" id="Seg_1738" s="T112">TFF_1967_KalabokkaPoenegessa1_flk.021 (001.021)</ta>
            <ta e="T130" id="Seg_1739" s="T122">TFF_1967_KalabokkaPoenegessa1_flk.022 (001.022)</ta>
            <ta e="T134" id="Seg_1740" s="T130">TFF_1967_KalabokkaPoenegessa1_flk.023 (001.023)</ta>
            <ta e="T139" id="Seg_1741" s="T134">TFF_1967_KalabokkaPoenegessa1_flk.024 (001.024)</ta>
            <ta e="T146" id="Seg_1742" s="T139">TFF_1967_KalabokkaPoenegessa1_flk.025 (001.025)</ta>
            <ta e="T151" id="Seg_1743" s="T146">TFF_1967_KalabokkaPoenegessa1_flk.026 (001.026)</ta>
            <ta e="T156" id="Seg_1744" s="T151">TFF_1967_KalabokkaPoenegessa1_flk.027 (001.027)</ta>
            <ta e="T167" id="Seg_1745" s="T156">TFF_1967_KalabokkaPoenegessa1_flk.028 (001.028)</ta>
            <ta e="T176" id="Seg_1746" s="T167">TFF_1967_KalabokkaPoenegessa1_flk.029 (001.029)</ta>
            <ta e="T184" id="Seg_1747" s="T176">TFF_1967_KalabokkaPoenegessa1_flk.030 (001.030)</ta>
            <ta e="T190" id="Seg_1748" s="T184">TFF_1967_KalabokkaPoenegessa1_flk.031 (001.031)</ta>
            <ta e="T202" id="Seg_1749" s="T190">TFF_1967_KalabokkaPoenegessa1_flk.032 (001.032)</ta>
            <ta e="T219" id="Seg_1750" s="T202">TFF_1967_KalabokkaPoenegessa1_flk.033 (001.033)</ta>
            <ta e="T226" id="Seg_1751" s="T219">TFF_1967_KalabokkaPoenegessa1_flk.034 (001.034)</ta>
            <ta e="T232" id="Seg_1752" s="T226">TFF_1967_KalabokkaPoenegessa1_flk.035 (001.035)</ta>
            <ta e="T240" id="Seg_1753" s="T232">TFF_1967_KalabokkaPoenegessa1_flk.036 (001.036)</ta>
            <ta e="T250" id="Seg_1754" s="T240">TFF_1967_KalabokkaPoenegessa1_flk.037 (001.037)</ta>
            <ta e="T263" id="Seg_1755" s="T250">TFF_1967_KalabokkaPoenegessa1_flk.038 (001.038)</ta>
            <ta e="T276" id="Seg_1756" s="T263">TFF_1967_KalabokkaPoenegessa1_flk.039 (001.039)</ta>
            <ta e="T280" id="Seg_1757" s="T276">TFF_1967_KalabokkaPoenegessa1_flk.040 (001.040)</ta>
            <ta e="T285" id="Seg_1758" s="T280">TFF_1967_KalabokkaPoenegessa1_flk.041 (001.041)</ta>
            <ta e="T288" id="Seg_1759" s="T285">TFF_1967_KalabokkaPoenegessa1_flk.042 (001.042)</ta>
            <ta e="T295" id="Seg_1760" s="T288">TFF_1967_KalabokkaPoenegessa1_flk.043 (001.043)</ta>
            <ta e="T303" id="Seg_1761" s="T295">TFF_1967_KalabokkaPoenegessa1_flk.044 (001.044)</ta>
            <ta e="T311" id="Seg_1762" s="T303">TFF_1967_KalabokkaPoenegessa1_flk.045 (001.045)</ta>
         </annotation>
         <annotation name="st" tierref="st" />
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_1763" s="T1">elɨkwaːt warkɨkwaːt ära pajassəqi</ta>
            <ta e="T8" id="Seg_1764" s="T5">tabɨštjannaːn eːkwa iːdi</ta>
            <ta e="T10" id="Seg_1765" s="T8">nept kalabokka</ta>
            <ta e="T12" id="Seg_1766" s="T10">əːtət meːt(d)a</ta>
            <ta e="T19" id="Seg_1767" s="T12">äzawət kwannat(d)ən poːjergu šəːtt a Kalabokkanɨ(ä) tʼičaːtt</ta>
            <ta e="T26" id="Seg_1768" s="T19">tan maːtqən küːnaŋ warqɨk maːt(d)əm əːk tʼazəlǯimbet</ta>
            <ta e="T32" id="Seg_1769" s="T26">tʼaːzəl imendǯal pöːnegessa töːnǯa i aːmǯa št</ta>
            <ta e="T38" id="Seg_1770" s="T32">äzt i aːwət qwannaqi šə(ö)tt poːjergu</ta>
            <ta e="T41" id="Seg_1771" s="T38">kalabokka qaːla pʼalgaːlɨŋ</ta>
            <ta e="T51" id="Seg_1772" s="T41">aːdʼa (aːdʼe) i aːwa qaj čeːnčaːqi pöːnegessa töːnǯa maššim amgu</ta>
            <ta e="T58" id="Seg_1773" s="T51">a matt kəba pauze tap pöːnegessam maǯenǯaw</ta>
            <ta e="T64" id="Seg_1774" s="T58">kəba pawəmt sälʼešpət a onǯa läːra. </ta>
            <ta e="T66" id="Seg_1775" s="T64">madəmt tʼaːzəlǯimbət.</ta>
            <ta e="T73" id="Seg_1776" s="T66">qatʼe mɨlä öntʼit qajt(d)a čaːǯa toblantse muǯuwatple. </ta>
            <ta e="T79" id="Seg_1777" s="T73">kalabokka kəba paundse koptən ɨːlont(d) attedʼa.</ta>
            <ta e="T91" id="Seg_1778" s="T79">pöːnegessa maːt šeːrna kalabokkam koptən ɨːlok(ə)onto čatči(ə)t onǯe piːreqɨ(ə)nt i kalabokkam poːlle(ä)(o)dit</ta>
            <ta e="T95" id="Seg_1779" s="T91">kalabokkannaːn eːja kəba paw.</ta>
            <ta e="T104" id="Seg_1780" s="T95">aːmt(d)a pöːnegessan pʼargʼeqən kəba paontsä pʼargʼeqɨnt tä toː poǯalǯešpa(ə)t.</ta>
            <ta e="T106" id="Seg_1781" s="T104">pöːnegʼessa čeːnča.</ta>
            <ta e="T112" id="Seg_1782" s="T106">kalabokka kalabokka mannani pʼargʼew ək poǯalǯešplel.</ta>
            <ta e="T122" id="Seg_1783" s="T112">a kalabokka pʼargʼeqɨn na kɨbɨʒo paokantsä pöːnegʼessan šiːdʼem tʼoqollä qwattɨt. </ta>
            <ta e="T130" id="Seg_1784" s="T122">a pöːnegʼessa čeːnča: kalabokka, kalabokka matšiːdʼem ək tʼoqolešplel. </ta>
            <ta e="T134" id="Seg_1785" s="T130">mat tašt töːdenǯak št</ta>
            <ta e="T139" id="Seg_1786" s="T134">pöːnegʼessa kalabokkam tötət moqonä eːle</ta>
            <ta e="T146" id="Seg_1787" s="T139">kalabokkan olɨt tarɨt pöːnegʼessa köːgalbat, oltargaːlɨŋ qaːlɨmba</ta>
            <ta e="T151" id="Seg_1788" s="T146">pöːnegʼessa poːnä čaːnǯa i qwann(a).</ta>
            <ta e="T156" id="Seg_1789" s="T151">kalabokka mešelačlä qaːla i oːltargaːlɨŋ. </ta>
            <ta e="T167" id="Seg_1790" s="T156">üːdəmɨn töːwaq aːdʼat i aːwot, a kalabokka mešelačlä eppa i oltargaːlɨŋ.</ta>
            <ta e="T176" id="Seg_1791" s="T167">ästi aːwot soːqɨndʼaqi kalabokkanä, oltaromt ku qaːdəbumbal i meːšelačant?</ta>
            <ta e="T184" id="Seg_1792" s="T176">a kalabokka kadäšpa(ə)t mašim čiča (tšitša) loːssɨm poːldʒirsa.</ta>
            <ta e="T190" id="Seg_1793" s="T184">i mimbin (tilʼdi nilʼdi) loːssɨ(i)m poːlǯirsa.</ta>
            <ta e="T202" id="Seg_1794" s="T190">taːpti tʼeːl äːzɨt aːwot kuːnaj aza qwannaːquj (qwannaːq) maːt tʼeladəgu (tʼaːladəgu) qaːlaqi </ta>
            <ta e="T219" id="Seg_1795" s="T202">äzɨt i aːwɨt čannɛ tiːri qwäːlɨt nɨːndepom aːnət tomn (topt) qamǯaːqij a onǯi maːdat medoj pʼlʼekkant nɨledʼaːqij.</ta>
            <ta e="T226" id="Seg_1796" s="T219">äzɨt petʼ iːnda(ə)t petʼse nɨŋga petʼse nɨledʼa</ta>
            <ta e="T232" id="Seg_1797" s="T226">a pajat aːwot sommaj poː iːndət</ta>
            <ta e="T240" id="Seg_1798" s="T232">a kalabokkanä čeːnčaːqij a tat , kalabokka, maːdɨm tʼäːzalǯimblel</ta>
            <ta e="T250" id="Seg_1799" s="T240">kalabokka läːra, poːndsä qatälɨšpat, qaimta (öːntʼit) (öntʼäʼqi) öntʼäʼdin pöːnegʼessa čaːǯa</ta>
            <ta e="T263" id="Seg_1800" s="T250">maːt šeːrešpa, pöːnegessa maːdat tom aːzannɨt, qwälɨn nɨːndepont tʼiːɣelǯə i nʼuqqɨlwallä eːlle aːldʼe.</ta>
            <ta e="T276" id="Seg_1801" s="T263">i našt qaron äzɨt i aːwot äzɨt petʼsä pöːnegessan olom padʼannɨt, pöːnegessam qwannaːqi.</ta>
            <ta e="T280" id="Seg_1802" s="T276">poːnä warq tüːʒomom pannaːqi.</ta>
            <ta e="T285" id="Seg_1803" s="T280">pöːnegʼessam na warq tüːʒomnä tʼädʒaːqi.</ta>
            <ta e="T288" id="Seg_1804" s="T285">pöːnegʼessam tüː awešpa(ə)t.</ta>
            <ta e="T295" id="Seg_1805" s="T288">pöːnegːessan teulat qolʼcʼiwatplä tüːɣondok toː eːle paqtɨrnaːtt. </ta>
            <ta e="T303" id="Seg_1806" s="T295">pöːnegʼessam tüː amnɨt, i teperta eːlaːtt i warkaːtt.</ta>
            <ta e="T311" id="Seg_1807" s="T303">i kalabokka naj orumba i pönegʼessala naj tʼäŋgwaːtt.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_1808" s="T1">Elɨkwaːt warkɨkwaːt ära pajassəqi. </ta>
            <ta e="T8" id="Seg_1809" s="T5">Tabɨštjannaːn eːkwa iːdi. </ta>
            <ta e="T10" id="Seg_1810" s="T8">Nept Kalabokka. </ta>
            <ta e="T12" id="Seg_1811" s="T10">əːtət meːta. </ta>
            <ta e="T19" id="Seg_1812" s="T12">Äzawət kwannatən poːjergu šəːtt a Kalabokkanä tʼičaːtt. </ta>
            <ta e="T26" id="Seg_1813" s="T19">Tan maːtqən küːnaŋ warqɨk maːtəm əːk tʼazəlǯimbet. </ta>
            <ta e="T32" id="Seg_1814" s="T26">Tʼaːzəlǯimendǯal Pöːnegessa töːnǯa i aːmǯa št. </ta>
            <ta e="T38" id="Seg_1815" s="T32">Äzt i aːwət qwannaqi šött poːjergu. </ta>
            <ta e="T41" id="Seg_1816" s="T38">Kalabokka qaːla pʼalgaːlɨŋ. </ta>
            <ta e="T51" id="Seg_1817" s="T41">Aːdʼa (aːdʼe) i aːwa qaj čeːnčaːqi Pöːnegessa töːnǯa maššim amgu. </ta>
            <ta e="T58" id="Seg_1818" s="T51">A matt kəba pauze tap Pöːnegessam maǯenǯaw. </ta>
            <ta e="T64" id="Seg_1819" s="T58">Kəba pawəmt sälʼešpət a onǯa läːra. </ta>
            <ta e="T66" id="Seg_1820" s="T64">Madəmt tʼaːzəlǯimbət. </ta>
            <ta e="T73" id="Seg_1821" s="T66">Qatʼe mɨlä öntʼit qajta čaːǯa toblantse muǯuwatple. </ta>
            <ta e="T79" id="Seg_1822" s="T73">Kalabokka kəba paundse koptən ɨːlont atteda. </ta>
            <ta e="T91" id="Seg_1823" s="T79">Pöːnegessa maːt šeːrna Kalabokkam koptən ɨːlokonto čatčit onǯe piːreqɨnt i Kalabokkam poːlledit. </ta>
            <ta e="T95" id="Seg_1824" s="T91">Kalabokkannaːn eːja kəba paw. </ta>
            <ta e="T104" id="Seg_1825" s="T95">Aːmta Pöːnegessan pʼargeqən kəba paontsä pʼargeqɨnt tä toː poǯalǯešpat. </ta>
            <ta e="T106" id="Seg_1826" s="T104">Pöːnegessa čeːnča. </ta>
            <ta e="T112" id="Seg_1827" s="T106">Kalabokka Kalabokka mannani pʼargʼew ək poǯalǯešplel. </ta>
            <ta e="T122" id="Seg_1828" s="T112">A Kalabokka pʼargʼeqɨn na kɨbɨʒo paokantsä Pöːnegessan šiːdʼem tʼoqollä qwattɨt. </ta>
            <ta e="T130" id="Seg_1829" s="T122">A Pöːnegessa čeːnča: Kalabokka, Kalabokka matšiːdʼem ək tʼoqolešplel. </ta>
            <ta e="T134" id="Seg_1830" s="T130">Mat tašt töːdenǯak št. </ta>
            <ta e="T139" id="Seg_1831" s="T134">Pöːnegessa Kalabokkam tötət moqonä eːle. </ta>
            <ta e="T146" id="Seg_1832" s="T139">Kalabokkam olɨt tarɨt Pöːnegessa köːgalbat, oltargaːlɨŋ qaːlɨmba. </ta>
            <ta e="T151" id="Seg_1833" s="T146">Pöːnegessa poːnä čaːnǯa i qwann. </ta>
            <ta e="T156" id="Seg_1834" s="T151">Kalabokka mešelačlä qaːla i oːltargaːlɨŋ. </ta>
            <ta e="T167" id="Seg_1835" s="T156">Üːdəmɨn töːwaq aːdʼat i aːwot, a Kalabokka mešelačlä eppa i oltargaːlɨŋ. </ta>
            <ta e="T176" id="Seg_1836" s="T167">Ästi aːwot soːqɨndʼaqi Kalabokkanä, oltaromt ku qaːdəbumbal i meːšelačant? </ta>
            <ta e="T184" id="Seg_1837" s="T176">A Kalabokka kadäšpat mašim čiča (tšitša) loːssɨm poːldʒirsa. </ta>
            <ta e="T190" id="Seg_1838" s="T184">I mimbin (tilʼdi nilʼdi) loːssɨm poːlǯirsa. </ta>
            <ta e="T202" id="Seg_1839" s="T190">Taːpti tʼeːl äːzɨt aːwot kuːnaj aza qwannaquij (qwannaːq) maːt tʼeladəgu (tʼaːladəgu) qaːlaqi. </ta>
            <ta e="T219" id="Seg_1840" s="T202">Äzɨt i aːwɨt čannɛ tiːri qwäːlɨt nɨːndepom aːnət tomn (topt) qamǯaːqij, a onǯi maːdat medoj pʼlʼekkant nɨledʼaːqij. </ta>
            <ta e="T226" id="Seg_1841" s="T219">Äzɨt petʼ iːndat petʼse nɨŋga petʼse nɨledʼa. </ta>
            <ta e="T232" id="Seg_1842" s="T226">A pajat aːwot sommaj poː iːndət. </ta>
            <ta e="T240" id="Seg_1843" s="T232">A Kalabokkanä čeːnčaːqij: a tat, Kalabokka, maːdɨm tʼäːzalǯimblel. </ta>
            <ta e="T250" id="Seg_1844" s="T240">Kalabokka läːra, poːndsä qatälɨšpat, qaimta (öːntʼit) (öntʼäːqi) öːntʼäːdin Pöːnegessa čaːǯa. </ta>
            <ta e="T263" id="Seg_1845" s="T250">Maːt šeːrešpa, Pöːnegessa maːdat tom aːzannɨt, qwälɨn nɨːndepont tʼiːɣelǯə i nʼuqqɨlwallä eːlle aːldʼe. </ta>
            <ta e="T276" id="Seg_1846" s="T263">I našt qaron äzɨt i aːwot äzɨt petʼsä Pöːnegessan olom padʼannɨt, Pöːnegessam qwannaːqi. </ta>
            <ta e="T280" id="Seg_1847" s="T276">Poːnä warq tüːʒomom pannaːqi. </ta>
            <ta e="T285" id="Seg_1848" s="T280">Pöːnegessam na warq tüːʒomnä tʼädʒaːqi. </ta>
            <ta e="T288" id="Seg_1849" s="T285">Pöːnegessam tü awešpat. </ta>
            <ta e="T295" id="Seg_1850" s="T288">Pöːnegessan teulat qolʼcʼiwatplä tüːɣondok toː eːle paqtɨrnaːtt. </ta>
            <ta e="T303" id="Seg_1851" s="T295">Pöːnegessam tüː amnɨt, i teperta eːlaːtt i warkaːtt. </ta>
            <ta e="T311" id="Seg_1852" s="T303">I Kalabokka naj orumba i Pönegessala naj tʼäŋgwaːtt. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_1853" s="T1">elɨ-k-waː-t</ta>
            <ta e="T3" id="Seg_1854" s="T2">warkɨ-k-waː-t</ta>
            <ta e="T4" id="Seg_1855" s="T3">ära</ta>
            <ta e="T5" id="Seg_1856" s="T4">paja-ssə-qi</ta>
            <ta e="T6" id="Seg_1857" s="T5">tab-ɨ-štja-n-naːn</ta>
            <ta e="T7" id="Seg_1858" s="T6">eː-k-wa</ta>
            <ta e="T8" id="Seg_1859" s="T7">iː-di</ta>
            <ta e="T9" id="Seg_1860" s="T8">nep-t</ta>
            <ta e="T10" id="Seg_1861" s="T9">Kalabok-ka</ta>
            <ta e="T11" id="Seg_1862" s="T10">əːtə-t</ta>
            <ta e="T12" id="Seg_1863" s="T11">meːta</ta>
            <ta e="T13" id="Seg_1864" s="T12">äza-wə-t</ta>
            <ta e="T14" id="Seg_1865" s="T13">kwa-na-tən</ta>
            <ta e="T15" id="Seg_1866" s="T14">poː-j-e-r-gu</ta>
            <ta e="T16" id="Seg_1867" s="T15">šəːt-t</ta>
            <ta e="T17" id="Seg_1868" s="T16">a</ta>
            <ta e="T18" id="Seg_1869" s="T17">Kalabok-ka-nä</ta>
            <ta e="T19" id="Seg_1870" s="T18">tʼičaː-tt</ta>
            <ta e="T20" id="Seg_1871" s="T19">tat</ta>
            <ta e="T21" id="Seg_1872" s="T20">maːt-qən</ta>
            <ta e="T22" id="Seg_1873" s="T21">küːna-ŋ</ta>
            <ta e="T23" id="Seg_1874" s="T22">warqɨ-k</ta>
            <ta e="T24" id="Seg_1875" s="T23">maːt-ə-m</ta>
            <ta e="T25" id="Seg_1876" s="T24">əːk</ta>
            <ta e="T26" id="Seg_1877" s="T25">tʼazə-lǯi-mb-et</ta>
            <ta e="T27" id="Seg_1878" s="T26">tʼaːzə-lǯi-me-ndǯa-l</ta>
            <ta e="T28" id="Seg_1879" s="T27">pöːnegessa</ta>
            <ta e="T29" id="Seg_1880" s="T28">töː-nǯa</ta>
            <ta e="T30" id="Seg_1881" s="T29">i</ta>
            <ta e="T31" id="Seg_1882" s="T30">aːm-ǯa</ta>
            <ta e="T32" id="Seg_1883" s="T31">št</ta>
            <ta e="T33" id="Seg_1884" s="T32">äz-t</ta>
            <ta e="T34" id="Seg_1885" s="T33">i</ta>
            <ta e="T35" id="Seg_1886" s="T34">aːwə-t</ta>
            <ta e="T36" id="Seg_1887" s="T35">qwan-na-qi</ta>
            <ta e="T37" id="Seg_1888" s="T36">šöt-t</ta>
            <ta e="T38" id="Seg_1889" s="T37">poː-j-e-r-gu</ta>
            <ta e="T39" id="Seg_1890" s="T38">Kalabok-ka</ta>
            <ta e="T40" id="Seg_1891" s="T39">qaːla</ta>
            <ta e="T41" id="Seg_1892" s="T40">pʼal-gaːlɨ-ŋ</ta>
            <ta e="T42" id="Seg_1893" s="T41">aːdʼa</ta>
            <ta e="T43" id="Seg_1894" s="T42">aːdʼe</ta>
            <ta e="T44" id="Seg_1895" s="T43">i</ta>
            <ta e="T45" id="Seg_1896" s="T44">aːwa</ta>
            <ta e="T46" id="Seg_1897" s="T45">qaj</ta>
            <ta e="T47" id="Seg_1898" s="T46">čeːnčaː-qi</ta>
            <ta e="T48" id="Seg_1899" s="T47">Pöːnegessa</ta>
            <ta e="T49" id="Seg_1900" s="T48">töː-nǯa</ta>
            <ta e="T50" id="Seg_1901" s="T49">maššim</ta>
            <ta e="T51" id="Seg_1902" s="T50">am-gu</ta>
            <ta e="T52" id="Seg_1903" s="T51">a</ta>
            <ta e="T53" id="Seg_1904" s="T52">matt</ta>
            <ta e="T54" id="Seg_1905" s="T53">kəba</ta>
            <ta e="T55" id="Seg_1906" s="T54">pau-ze</ta>
            <ta e="T56" id="Seg_1907" s="T55">tap</ta>
            <ta e="T57" id="Seg_1908" s="T56">Pöːnegessa-m</ta>
            <ta e="T58" id="Seg_1909" s="T57">maǯe-nǯa-w</ta>
            <ta e="T59" id="Seg_1910" s="T58">kəba</ta>
            <ta e="T60" id="Seg_1911" s="T59">pawə-m-t</ta>
            <ta e="T61" id="Seg_1912" s="T60">sälʼe-špə-t</ta>
            <ta e="T62" id="Seg_1913" s="T61">a</ta>
            <ta e="T63" id="Seg_1914" s="T62">onǯa</ta>
            <ta e="T64" id="Seg_1915" s="T63">läː-r-a</ta>
            <ta e="T65" id="Seg_1916" s="T64">mad-ə-m-t</ta>
            <ta e="T66" id="Seg_1917" s="T65">tʼaːzə-lǯi-mbə-t</ta>
            <ta e="T67" id="Seg_1918" s="T66">qatʼe</ta>
            <ta e="T68" id="Seg_1919" s="T67">mɨ-lä</ta>
            <ta e="T69" id="Seg_1920" s="T68">öntʼi-t</ta>
            <ta e="T70" id="Seg_1921" s="T69">qaj-da</ta>
            <ta e="T71" id="Seg_1922" s="T70">čaːǯa</ta>
            <ta e="T72" id="Seg_1923" s="T71">tob-la-nt-se</ta>
            <ta e="T73" id="Seg_1924" s="T72">muǯu-wat-ple</ta>
            <ta e="T74" id="Seg_1925" s="T73">Kalabok-ka</ta>
            <ta e="T75" id="Seg_1926" s="T74">kəba</ta>
            <ta e="T76" id="Seg_1927" s="T75">pau-nd-se</ta>
            <ta e="T77" id="Seg_1928" s="T76">koptə-n</ta>
            <ta e="T78" id="Seg_1929" s="T77">ɨːlo-nt</ta>
            <ta e="T79" id="Seg_1930" s="T78">atte-da</ta>
            <ta e="T80" id="Seg_1931" s="T79">Pöːnegessa</ta>
            <ta e="T81" id="Seg_1932" s="T80">maːt</ta>
            <ta e="T82" id="Seg_1933" s="T81">šeːr-na</ta>
            <ta e="T83" id="Seg_1934" s="T82">Kalabok-ka-m</ta>
            <ta e="T84" id="Seg_1935" s="T83">koptə-n</ta>
            <ta e="T85" id="Seg_1936" s="T84">ɨːlo-konto</ta>
            <ta e="T86" id="Seg_1937" s="T85">čatči-t</ta>
            <ta e="T87" id="Seg_1938" s="T86">onǯe</ta>
            <ta e="T88" id="Seg_1939" s="T87">piːre-qɨnt</ta>
            <ta e="T89" id="Seg_1940" s="T88">i</ta>
            <ta e="T90" id="Seg_1941" s="T89">Kalabok-ka-m</ta>
            <ta e="T91" id="Seg_1942" s="T90">poːlle-di-t</ta>
            <ta e="T92" id="Seg_1943" s="T91">Kalabok-ka-n-naːn</ta>
            <ta e="T93" id="Seg_1944" s="T92">eː-ja</ta>
            <ta e="T94" id="Seg_1945" s="T93">kəba</ta>
            <ta e="T95" id="Seg_1946" s="T94">paw</ta>
            <ta e="T96" id="Seg_1947" s="T95">aːmta</ta>
            <ta e="T97" id="Seg_1948" s="T96">Pöːnegessa-n</ta>
            <ta e="T98" id="Seg_1949" s="T97">pʼarge-qən</ta>
            <ta e="T99" id="Seg_1950" s="T98">kəba</ta>
            <ta e="T100" id="Seg_1951" s="T99">pao-nt-se</ta>
            <ta e="T101" id="Seg_1952" s="T100">pʼarge-qɨnt</ta>
            <ta e="T102" id="Seg_1953" s="T101">tä</ta>
            <ta e="T103" id="Seg_1954" s="T102">toː</ta>
            <ta e="T104" id="Seg_1955" s="T103">poǯa-lǯe-špa-t</ta>
            <ta e="T105" id="Seg_1956" s="T104">Pöːnegessa</ta>
            <ta e="T106" id="Seg_1957" s="T105">čeːnča</ta>
            <ta e="T107" id="Seg_1958" s="T106">Kalabok-ka</ta>
            <ta e="T108" id="Seg_1959" s="T107">Kalabok-ka</ta>
            <ta e="T109" id="Seg_1960" s="T108">man-nan-i</ta>
            <ta e="T110" id="Seg_1961" s="T109">pʼargʼe-w</ta>
            <ta e="T111" id="Seg_1962" s="T110">ək</ta>
            <ta e="T112" id="Seg_1963" s="T111">poǯa-lǯe-šp-le-l</ta>
            <ta e="T113" id="Seg_1964" s="T112">a</ta>
            <ta e="T114" id="Seg_1965" s="T113">Kalabok-ka</ta>
            <ta e="T115" id="Seg_1966" s="T114">pʼargʼe-qɨn</ta>
            <ta e="T116" id="Seg_1967" s="T115">na</ta>
            <ta e="T117" id="Seg_1968" s="T116">kɨbɨʒo</ta>
            <ta e="T118" id="Seg_1969" s="T117">pao-ka-nt-se</ta>
            <ta e="T119" id="Seg_1970" s="T118">Pöːnegessa-n</ta>
            <ta e="T120" id="Seg_1971" s="T119">šiːdʼe-m</ta>
            <ta e="T121" id="Seg_1972" s="T120">tʼoqo-l-lä</ta>
            <ta e="T122" id="Seg_1973" s="T121">qwattɨ-t</ta>
            <ta e="T123" id="Seg_1974" s="T122">a</ta>
            <ta e="T124" id="Seg_1975" s="T123">Pöːnegessa</ta>
            <ta e="T125" id="Seg_1976" s="T124">čeːnča</ta>
            <ta e="T126" id="Seg_1977" s="T125">Kalabok-ka</ta>
            <ta e="T127" id="Seg_1978" s="T126">Kalabok</ta>
            <ta e="T128" id="Seg_1979" s="T127">mat-šiːdʼe-m</ta>
            <ta e="T129" id="Seg_1980" s="T128">ək</ta>
            <ta e="T130" id="Seg_1981" s="T129">tʼoqo-le-šp-le-l</ta>
            <ta e="T131" id="Seg_1982" s="T130">mat</ta>
            <ta e="T132" id="Seg_1983" s="T131">tašt</ta>
            <ta e="T133" id="Seg_1984" s="T132">töːd-e-nǯa-k</ta>
            <ta e="T134" id="Seg_1985" s="T133">št</ta>
            <ta e="T135" id="Seg_1986" s="T134">Pöːnegessa</ta>
            <ta e="T136" id="Seg_1987" s="T135">Kalabok-ka-m</ta>
            <ta e="T137" id="Seg_1988" s="T136">töt-ə-t</ta>
            <ta e="T138" id="Seg_1989" s="T137">moqonä</ta>
            <ta e="T139" id="Seg_1990" s="T138">eːle</ta>
            <ta e="T140" id="Seg_1991" s="T139">Kalabok-ka-m</ta>
            <ta e="T141" id="Seg_1992" s="T140">olɨ-t</ta>
            <ta e="T142" id="Seg_1993" s="T141">tar-ɨ-t</ta>
            <ta e="T143" id="Seg_1994" s="T142">Pöːnegessa</ta>
            <ta e="T144" id="Seg_1995" s="T143">köːga-l-ba-t</ta>
            <ta e="T145" id="Seg_1996" s="T144">ol-tar-gaːlɨ-ŋ</ta>
            <ta e="T146" id="Seg_1997" s="T145">qaːlɨ-mba</ta>
            <ta e="T147" id="Seg_1998" s="T146">Pöːnegessa</ta>
            <ta e="T148" id="Seg_1999" s="T147">poːnä</ta>
            <ta e="T149" id="Seg_2000" s="T148">čaːnǯa</ta>
            <ta e="T150" id="Seg_2001" s="T149">i</ta>
            <ta e="T151" id="Seg_2002" s="T150">qwan-n</ta>
            <ta e="T152" id="Seg_2003" s="T151">Kalabok-ka</ta>
            <ta e="T153" id="Seg_2004" s="T152">mešela-č-lä</ta>
            <ta e="T154" id="Seg_2005" s="T153">qaːla</ta>
            <ta e="T155" id="Seg_2006" s="T154">i</ta>
            <ta e="T156" id="Seg_2007" s="T155">oːl-tar-gaːlɨ-ŋ</ta>
            <ta e="T157" id="Seg_2008" s="T156">üːdə-mɨn</ta>
            <ta e="T158" id="Seg_2009" s="T157">töː-wa-q</ta>
            <ta e="T159" id="Seg_2010" s="T158">aːdʼa-t</ta>
            <ta e="T160" id="Seg_2011" s="T159">i</ta>
            <ta e="T161" id="Seg_2012" s="T160">aːwo-t</ta>
            <ta e="T162" id="Seg_2013" s="T161">a</ta>
            <ta e="T163" id="Seg_2014" s="T162">Kalabok-ka</ta>
            <ta e="T164" id="Seg_2015" s="T163">mešela-č-lä</ta>
            <ta e="T165" id="Seg_2016" s="T164">eppa</ta>
            <ta e="T166" id="Seg_2017" s="T165">i</ta>
            <ta e="T167" id="Seg_2018" s="T166">ol-tar-gaːlɨ-ŋ</ta>
            <ta e="T168" id="Seg_2019" s="T167">äs-ti</ta>
            <ta e="T169" id="Seg_2020" s="T168">aːwo-t</ta>
            <ta e="T170" id="Seg_2021" s="T169">soːqɨ-ndʼa-qi</ta>
            <ta e="T171" id="Seg_2022" s="T170">Kalabok-ka-nä</ta>
            <ta e="T172" id="Seg_2023" s="T171">ol-tar-o-m-t</ta>
            <ta e="T173" id="Seg_2024" s="T172">ku</ta>
            <ta e="T174" id="Seg_2025" s="T173">qaːdə-bu-mba-l</ta>
            <ta e="T175" id="Seg_2026" s="T174">i</ta>
            <ta e="T176" id="Seg_2027" s="T175">meːšela-ča-nt</ta>
            <ta e="T177" id="Seg_2028" s="T176">a</ta>
            <ta e="T178" id="Seg_2029" s="T177">Kalabok-ka</ta>
            <ta e="T179" id="Seg_2030" s="T178">kadä-špa-t</ta>
            <ta e="T180" id="Seg_2031" s="T179">mašim</ta>
            <ta e="T181" id="Seg_2032" s="T180">čiča</ta>
            <ta e="T182" id="Seg_2033" s="T181">tšitša</ta>
            <ta e="T183" id="Seg_2034" s="T182">loːssɨ-m</ta>
            <ta e="T184" id="Seg_2035" s="T183">poːl-dʒi-r-sa</ta>
            <ta e="T185" id="Seg_2036" s="T184">i</ta>
            <ta e="T186" id="Seg_2037" s="T185">mimbin</ta>
            <ta e="T187" id="Seg_2038" s="T186">tilʼdi</ta>
            <ta e="T188" id="Seg_2039" s="T187">nilʼdi</ta>
            <ta e="T189" id="Seg_2040" s="T188">loːss-ɨ-m</ta>
            <ta e="T190" id="Seg_2041" s="T189">poːl-ǯi-r-sa</ta>
            <ta e="T191" id="Seg_2042" s="T190">taːpti</ta>
            <ta e="T192" id="Seg_2043" s="T191">tʼeːl</ta>
            <ta e="T193" id="Seg_2044" s="T192">äːzɨ-t</ta>
            <ta e="T194" id="Seg_2045" s="T193">aːwo-t</ta>
            <ta e="T195" id="Seg_2046" s="T194">kuː-naj</ta>
            <ta e="T196" id="Seg_2047" s="T195">aza</ta>
            <ta e="T197" id="Seg_2048" s="T196">qwa-na-quij</ta>
            <ta e="T198" id="Seg_2049" s="T197">qwan-naː-q</ta>
            <ta e="T199" id="Seg_2050" s="T198">maːt</ta>
            <ta e="T200" id="Seg_2051" s="T199">tʼela-də-gu</ta>
            <ta e="T201" id="Seg_2052" s="T200">tʼaːla-də-gu</ta>
            <ta e="T202" id="Seg_2053" s="T201">qaːl-a-qi</ta>
            <ta e="T203" id="Seg_2054" s="T202">äzɨ-t</ta>
            <ta e="T204" id="Seg_2055" s="T203">i</ta>
            <ta e="T205" id="Seg_2056" s="T204">aːwɨ-t</ta>
            <ta e="T206" id="Seg_2057" s="T205">čannɛ</ta>
            <ta e="T207" id="Seg_2058" s="T206">tiːri</ta>
            <ta e="T208" id="Seg_2059" s="T207">qwäːl-ɨ-t</ta>
            <ta e="T209" id="Seg_2060" s="T208">nɨːndepo-m</ta>
            <ta e="T210" id="Seg_2061" s="T209">aːn-ə-t</ta>
            <ta e="T211" id="Seg_2062" s="T210">tom-n</ta>
            <ta e="T212" id="Seg_2063" s="T211">top-t</ta>
            <ta e="T213" id="Seg_2064" s="T212">qamǯaː-qij</ta>
            <ta e="T214" id="Seg_2065" s="T213">a</ta>
            <ta e="T215" id="Seg_2066" s="T214">onǯi</ta>
            <ta e="T216" id="Seg_2067" s="T215">maːda-t</ta>
            <ta e="T217" id="Seg_2068" s="T216">medo-j</ta>
            <ta e="T218" id="Seg_2069" s="T217">pʼlʼekk-a-nt</ta>
            <ta e="T219" id="Seg_2070" s="T218">nɨ-le-dʼaː-qij</ta>
            <ta e="T220" id="Seg_2071" s="T219">äzɨ-t</ta>
            <ta e="T221" id="Seg_2072" s="T220">petʼ</ta>
            <ta e="T222" id="Seg_2073" s="T221">iː-nda-t</ta>
            <ta e="T223" id="Seg_2074" s="T222">petʼ-se</ta>
            <ta e="T224" id="Seg_2075" s="T223">nɨ-ŋga</ta>
            <ta e="T225" id="Seg_2076" s="T224">petʼ-se</ta>
            <ta e="T226" id="Seg_2077" s="T225">nɨ-le-dʼa</ta>
            <ta e="T227" id="Seg_2078" s="T226">a</ta>
            <ta e="T228" id="Seg_2079" s="T227">paja-t</ta>
            <ta e="T229" id="Seg_2080" s="T228">aːwo-t</ta>
            <ta e="T230" id="Seg_2081" s="T229">somma-j</ta>
            <ta e="T231" id="Seg_2082" s="T230">poː</ta>
            <ta e="T232" id="Seg_2083" s="T231">iː-ndə-t</ta>
            <ta e="T233" id="Seg_2084" s="T232">a</ta>
            <ta e="T234" id="Seg_2085" s="T233">Kalabok-ka-nä</ta>
            <ta e="T235" id="Seg_2086" s="T234">čeːnč-aː-qij</ta>
            <ta e="T236" id="Seg_2087" s="T235">a</ta>
            <ta e="T237" id="Seg_2088" s="T236">tan</ta>
            <ta e="T238" id="Seg_2089" s="T237">Kalabok-ka</ta>
            <ta e="T239" id="Seg_2090" s="T238">maːd-ɨ-m</ta>
            <ta e="T240" id="Seg_2091" s="T239">tʼäːza-lǯi-mb-le-l</ta>
            <ta e="T241" id="Seg_2092" s="T240">Kalabok-ka</ta>
            <ta e="T242" id="Seg_2093" s="T241">läː-r-a</ta>
            <ta e="T243" id="Seg_2094" s="T242">poː-nd-se</ta>
            <ta e="T244" id="Seg_2095" s="T243">qatä-lɨ-špa-t</ta>
            <ta e="T245" id="Seg_2096" s="T244">qai-m-ta</ta>
            <ta e="T246" id="Seg_2097" s="T245">öːntʼi-t</ta>
            <ta e="T247" id="Seg_2098" s="T246">öntʼäː-qi</ta>
            <ta e="T248" id="Seg_2099" s="T247">öːntʼäː-din</ta>
            <ta e="T249" id="Seg_2100" s="T248">Pöːnegessa</ta>
            <ta e="T250" id="Seg_2101" s="T249">čaːǯa</ta>
            <ta e="T251" id="Seg_2102" s="T250">maːt</ta>
            <ta e="T252" id="Seg_2103" s="T251">šeːr-e-špa</ta>
            <ta e="T253" id="Seg_2104" s="T252">Pöːnegessa</ta>
            <ta e="T254" id="Seg_2105" s="T253">maːda-t</ta>
            <ta e="T255" id="Seg_2106" s="T254">tom</ta>
            <ta e="T256" id="Seg_2107" s="T255">aːza-nnɨ-t</ta>
            <ta e="T257" id="Seg_2108" s="T256">qwäl-ɨ-n</ta>
            <ta e="T258" id="Seg_2109" s="T257">nɨːndepo-nt</ta>
            <ta e="T259" id="Seg_2110" s="T258">tʼiːɣe-lǯə</ta>
            <ta e="T260" id="Seg_2111" s="T259">i</ta>
            <ta e="T261" id="Seg_2112" s="T260">nʼuqqɨ-l-wal-lä</ta>
            <ta e="T262" id="Seg_2113" s="T261">eːlle</ta>
            <ta e="T263" id="Seg_2114" s="T262">aːldʼe</ta>
            <ta e="T264" id="Seg_2115" s="T263">i</ta>
            <ta e="T265" id="Seg_2116" s="T264">na-št</ta>
            <ta e="T266" id="Seg_2117" s="T265">qaro-n</ta>
            <ta e="T267" id="Seg_2118" s="T266">äzɨ-t</ta>
            <ta e="T268" id="Seg_2119" s="T267">i</ta>
            <ta e="T269" id="Seg_2120" s="T268">aːwo-t</ta>
            <ta e="T270" id="Seg_2121" s="T269">äzɨ-t</ta>
            <ta e="T271" id="Seg_2122" s="T270">petʼ-se</ta>
            <ta e="T272" id="Seg_2123" s="T271">Pöːnegessa-n</ta>
            <ta e="T273" id="Seg_2124" s="T272">olo-m</ta>
            <ta e="T274" id="Seg_2125" s="T273">padʼa-nnɨ-t</ta>
            <ta e="T275" id="Seg_2126" s="T274">Pöːnegessa-m</ta>
            <ta e="T276" id="Seg_2127" s="T275">qwan-naː-qi</ta>
            <ta e="T277" id="Seg_2128" s="T276">poːnä</ta>
            <ta e="T278" id="Seg_2129" s="T277">warq</ta>
            <ta e="T279" id="Seg_2130" s="T278">tüː-ʒom-o-m</ta>
            <ta e="T280" id="Seg_2131" s="T279">pan-naː-qi</ta>
            <ta e="T281" id="Seg_2132" s="T280">Pöːnegessa-m</ta>
            <ta e="T282" id="Seg_2133" s="T281">na</ta>
            <ta e="T283" id="Seg_2134" s="T282">warq</ta>
            <ta e="T284" id="Seg_2135" s="T283">tüː-ʒom-nä</ta>
            <ta e="T285" id="Seg_2136" s="T284">tʼädʒaː-qi</ta>
            <ta e="T286" id="Seg_2137" s="T285">Pöːnegessa-m</ta>
            <ta e="T287" id="Seg_2138" s="T286">tü</ta>
            <ta e="T288" id="Seg_2139" s="T287">aw-e-špa-t</ta>
            <ta e="T289" id="Seg_2140" s="T288">Pöːnegessa-n</ta>
            <ta e="T290" id="Seg_2141" s="T289">teu-la-t</ta>
            <ta e="T291" id="Seg_2142" s="T290">qo-lʼcʼi-wat-plä</ta>
            <ta e="T292" id="Seg_2143" s="T291">tüː-ɣondo-k</ta>
            <ta e="T293" id="Seg_2144" s="T292">toː</ta>
            <ta e="T294" id="Seg_2145" s="T293">eːle</ta>
            <ta e="T295" id="Seg_2146" s="T294">paqtɨ-r-naː-tt</ta>
            <ta e="T296" id="Seg_2147" s="T295">Pöːnegessa-m</ta>
            <ta e="T297" id="Seg_2148" s="T296">tüː</ta>
            <ta e="T298" id="Seg_2149" s="T297">am-nɨ-t</ta>
            <ta e="T299" id="Seg_2150" s="T298">i</ta>
            <ta e="T300" id="Seg_2151" s="T299">teper-ta</ta>
            <ta e="T301" id="Seg_2152" s="T300">eːlaː-tt</ta>
            <ta e="T302" id="Seg_2153" s="T301">i</ta>
            <ta e="T303" id="Seg_2154" s="T302">warkaː-tt</ta>
            <ta e="T304" id="Seg_2155" s="T303">i</ta>
            <ta e="T305" id="Seg_2156" s="T304">Kalabok-ka</ta>
            <ta e="T306" id="Seg_2157" s="T305">naj</ta>
            <ta e="T307" id="Seg_2158" s="T306">or-u-m-ba</ta>
            <ta e="T308" id="Seg_2159" s="T307">i</ta>
            <ta e="T309" id="Seg_2160" s="T308">Pönegessa-la</ta>
            <ta e="T310" id="Seg_2161" s="T309">naj</ta>
            <ta e="T311" id="Seg_2162" s="T310">tʼäŋg-waː-tt</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_2163" s="T1">elɨ-k-ŋɨ-tɨt</ta>
            <ta e="T3" id="Seg_2164" s="T2">warkə-k-ŋɨ-tɨt</ta>
            <ta e="T4" id="Seg_2165" s="T3">ara</ta>
            <ta e="T5" id="Seg_2166" s="T4">paja-se-qi</ta>
            <ta e="T6" id="Seg_2167" s="T5">tap-ɨ-štja-n-nan</ta>
            <ta e="T7" id="Seg_2168" s="T6">eː-ku-ŋɨ</ta>
            <ta e="T8" id="Seg_2169" s="T7">iː-tɨ</ta>
            <ta e="T9" id="Seg_2170" s="T8">nim-tɨ</ta>
            <ta e="T10" id="Seg_2171" s="T9">Kalabok-ka</ta>
            <ta e="T11" id="Seg_2172" s="T10">üːtǝ-tɨ</ta>
            <ta e="T12" id="Seg_2173" s="T11">medɨ</ta>
            <ta e="T13" id="Seg_2174" s="T12">aǯʼa-awa-tɨ</ta>
            <ta e="T14" id="Seg_2175" s="T13">qwən-ŋɨ-tɨt</ta>
            <ta e="T15" id="Seg_2176" s="T14">po-j-ɨ-r-gu</ta>
            <ta e="T16" id="Seg_2177" s="T15">šöːt-ntɨ</ta>
            <ta e="T17" id="Seg_2178" s="T16">a</ta>
            <ta e="T18" id="Seg_2179" s="T17">Kalabok-ka-nɨ</ta>
            <ta e="T19" id="Seg_2180" s="T18">tiːǯe-tɨt</ta>
            <ta e="T20" id="Seg_2181" s="T19">tan</ta>
            <ta e="T21" id="Seg_2182" s="T20">maːt-qɨn</ta>
            <ta e="T22" id="Seg_2183" s="T21">kuːna-k</ta>
            <ta e="T23" id="Seg_2184" s="T22">warkə-äšɨk</ta>
            <ta e="T24" id="Seg_2185" s="T23">maːt-ɨ-m</ta>
            <ta e="T25" id="Seg_2186" s="T24">ɨkɨ</ta>
            <ta e="T26" id="Seg_2187" s="T25">tʼaze-lʼčǝ-mbɨ-ätɨ</ta>
            <ta e="T27" id="Seg_2188" s="T26">tʼaze-lʼčǝ-me-nǯe-l</ta>
            <ta e="T28" id="Seg_2189" s="T27">Pöːnege</ta>
            <ta e="T29" id="Seg_2190" s="T28">töː-nǯe</ta>
            <ta e="T30" id="Seg_2191" s="T29">i</ta>
            <ta e="T31" id="Seg_2192" s="T30">amɨ-nǯe</ta>
            <ta e="T32" id="Seg_2193" s="T31">tašintɨ</ta>
            <ta e="T33" id="Seg_2194" s="T32">aǯʼa-tɨ</ta>
            <ta e="T34" id="Seg_2195" s="T33">i</ta>
            <ta e="T35" id="Seg_2196" s="T34">awa-tɨ</ta>
            <ta e="T36" id="Seg_2197" s="T35">qwən-ŋɨ-qiː</ta>
            <ta e="T37" id="Seg_2198" s="T36">šöːt-ntɨ</ta>
            <ta e="T38" id="Seg_2199" s="T37">po-j-ɨ-r-gu</ta>
            <ta e="T39" id="Seg_2200" s="T38">Kalabok-ka</ta>
            <ta e="T40" id="Seg_2201" s="T39">qalɨ</ta>
            <ta e="T41" id="Seg_2202" s="T40">pelǝ-gaːlɨ-k</ta>
            <ta e="T42" id="Seg_2203" s="T41">aǯʼa</ta>
            <ta e="T43" id="Seg_2204" s="T42">aǯʼa</ta>
            <ta e="T44" id="Seg_2205" s="T43">i</ta>
            <ta e="T45" id="Seg_2206" s="T44">awa</ta>
            <ta e="T46" id="Seg_2207" s="T45">qaj</ta>
            <ta e="T47" id="Seg_2208" s="T46">čenčɨ-qiː</ta>
            <ta e="T48" id="Seg_2209" s="T47">Pöːnege</ta>
            <ta e="T49" id="Seg_2210" s="T48">töː-nǯe</ta>
            <ta e="T50" id="Seg_2211" s="T49">mašim</ta>
            <ta e="T51" id="Seg_2212" s="T50">am-gu</ta>
            <ta e="T52" id="Seg_2213" s="T51">a</ta>
            <ta e="T53" id="Seg_2214" s="T52">man</ta>
            <ta e="T54" id="Seg_2215" s="T53">kɨba</ta>
            <ta e="T55" id="Seg_2216" s="T54">pagɨ-se</ta>
            <ta e="T56" id="Seg_2217" s="T55">tap</ta>
            <ta e="T57" id="Seg_2218" s="T56">Pöːnege-m</ta>
            <ta e="T58" id="Seg_2219" s="T57">mačə-nǯe-m</ta>
            <ta e="T59" id="Seg_2220" s="T58">kɨba</ta>
            <ta e="T60" id="Seg_2221" s="T59">pagɨ-m-tɨ</ta>
            <ta e="T61" id="Seg_2222" s="T60">sillə-špə-tɨ</ta>
            <ta e="T62" id="Seg_2223" s="T61">a</ta>
            <ta e="T63" id="Seg_2224" s="T62">onǯe</ta>
            <ta e="T64" id="Seg_2225" s="T63">lɨː-r-ɨ</ta>
            <ta e="T65" id="Seg_2226" s="T64">maːt-ɨ-m-tɨ</ta>
            <ta e="T66" id="Seg_2227" s="T65">tʼaze-lčɨ-mbɨ-tɨ</ta>
            <ta e="T67" id="Seg_2228" s="T66">kate</ta>
            <ta e="T68" id="Seg_2229" s="T67">meː-le</ta>
            <ta e="T69" id="Seg_2230" s="T68">unde-tɨ</ta>
            <ta e="T70" id="Seg_2231" s="T69">qaj-ta</ta>
            <ta e="T71" id="Seg_2232" s="T70">čaːǯɨ</ta>
            <ta e="T72" id="Seg_2233" s="T71">topǝ-la-ntɨ-se</ta>
            <ta e="T73" id="Seg_2234" s="T72">muːǯe-wat-lewlə</ta>
            <ta e="T74" id="Seg_2235" s="T73">Kalabok-ka</ta>
            <ta e="T75" id="Seg_2236" s="T74">kɨba</ta>
            <ta e="T76" id="Seg_2237" s="T75">pagɨ-ntɨ-se</ta>
            <ta e="T77" id="Seg_2238" s="T76">koːptǝ-n</ta>
            <ta e="T78" id="Seg_2239" s="T77">ɨlə-ntɨ</ta>
            <ta e="T79" id="Seg_2240" s="T78">atte-ntɨ</ta>
            <ta e="T80" id="Seg_2241" s="T79">Pöːnege</ta>
            <ta e="T81" id="Seg_2242" s="T80">maːt</ta>
            <ta e="T82" id="Seg_2243" s="T81">šeːr-ŋɨ</ta>
            <ta e="T83" id="Seg_2244" s="T82">Kalabok-ka-m</ta>
            <ta e="T84" id="Seg_2245" s="T83">koːptǝ-n</ta>
            <ta e="T85" id="Seg_2246" s="T84">ɨlə-qɨntɨ</ta>
            <ta e="T86" id="Seg_2247" s="T85">tʼatče-tɨ</ta>
            <ta e="T87" id="Seg_2248" s="T86">onǯe</ta>
            <ta e="T88" id="Seg_2249" s="T87">pirə-qɨntɨ</ta>
            <ta e="T89" id="Seg_2250" s="T88">i</ta>
            <ta e="T90" id="Seg_2251" s="T89">Kalabok-ka-m</ta>
            <ta e="T91" id="Seg_2252" s="T90">poːlɛ-ntɨ-tɨ</ta>
            <ta e="T92" id="Seg_2253" s="T91">Kalabok-ka-n-nan</ta>
            <ta e="T93" id="Seg_2254" s="T92">eː-ŋɨ</ta>
            <ta e="T94" id="Seg_2255" s="T93">kɨba</ta>
            <ta e="T95" id="Seg_2256" s="T94">pagɨ</ta>
            <ta e="T96" id="Seg_2257" s="T95">omtə</ta>
            <ta e="T97" id="Seg_2258" s="T96">Pöːnege-n</ta>
            <ta e="T98" id="Seg_2259" s="T97">perkə-qɨn</ta>
            <ta e="T99" id="Seg_2260" s="T98">kɨba</ta>
            <ta e="T100" id="Seg_2261" s="T99">pagɨ-ntɨ-se</ta>
            <ta e="T101" id="Seg_2262" s="T100">perkə-qɨntɨ</ta>
            <ta e="T102" id="Seg_2263" s="T101">tɛː</ta>
            <ta e="T103" id="Seg_2264" s="T102">to</ta>
            <ta e="T104" id="Seg_2265" s="T103">poǯa-lʼčǝ-špə-tɨ</ta>
            <ta e="T105" id="Seg_2266" s="T104">Pöːnege</ta>
            <ta e="T106" id="Seg_2267" s="T105">čenčɨ</ta>
            <ta e="T107" id="Seg_2268" s="T106">Kalabok-ka</ta>
            <ta e="T108" id="Seg_2269" s="T107">Kalabok-ka</ta>
            <ta e="T109" id="Seg_2270" s="T108">man-nan-lʼ</ta>
            <ta e="T110" id="Seg_2271" s="T109">perkə-m</ta>
            <ta e="T111" id="Seg_2272" s="T110">ɨkɨ</ta>
            <ta e="T112" id="Seg_2273" s="T111">poǯa-lʼčǝ-špə-lä-l</ta>
            <ta e="T113" id="Seg_2274" s="T112">a</ta>
            <ta e="T114" id="Seg_2275" s="T113">Kalabok-ka</ta>
            <ta e="T115" id="Seg_2276" s="T114">perkə-qɨn</ta>
            <ta e="T116" id="Seg_2277" s="T115">na</ta>
            <ta e="T117" id="Seg_2278" s="T116">kɨba</ta>
            <ta e="T118" id="Seg_2279" s="T117">pagɨ-ka-ntɨ-se</ta>
            <ta e="T119" id="Seg_2280" s="T118">Pöːnege-n</ta>
            <ta e="T120" id="Seg_2281" s="T119">šide-m</ta>
            <ta e="T121" id="Seg_2282" s="T120">tökko-l-le</ta>
            <ta e="T122" id="Seg_2283" s="T121">kwata-tɨ</ta>
            <ta e="T123" id="Seg_2284" s="T122">a</ta>
            <ta e="T124" id="Seg_2285" s="T123">Pöːnege</ta>
            <ta e="T125" id="Seg_2286" s="T124">čenčɨ</ta>
            <ta e="T126" id="Seg_2287" s="T125">Kalabok-ka</ta>
            <ta e="T127" id="Seg_2288" s="T126">Kalabok</ta>
            <ta e="T128" id="Seg_2289" s="T127">man-šide-m</ta>
            <ta e="T129" id="Seg_2290" s="T128">ɨkɨ</ta>
            <ta e="T130" id="Seg_2291" s="T129">tökko-lɨ-špə-lä-l</ta>
            <ta e="T131" id="Seg_2292" s="T130">man</ta>
            <ta e="T132" id="Seg_2293" s="T131">tašintɨ</ta>
            <ta e="T133" id="Seg_2294" s="T132">töt-ɨ-nǯe-k</ta>
            <ta e="T134" id="Seg_2295" s="T133">tašintɨ</ta>
            <ta e="T135" id="Seg_2296" s="T134">Pöːnege</ta>
            <ta e="T136" id="Seg_2297" s="T135">Kalabok-ka-m</ta>
            <ta e="T137" id="Seg_2298" s="T136">töt-ɨ-tɨ</ta>
            <ta e="T138" id="Seg_2299" s="T137">moqənä</ta>
            <ta e="T139" id="Seg_2300" s="T138">ɨlə</ta>
            <ta e="T140" id="Seg_2301" s="T139">Kalabok-ka-m</ta>
            <ta e="T141" id="Seg_2302" s="T140">olɨ-n</ta>
            <ta e="T142" id="Seg_2303" s="T141">tar-ɨ-t</ta>
            <ta e="T143" id="Seg_2304" s="T142">Pöːnege</ta>
            <ta e="T144" id="Seg_2305" s="T143">köːga-l-mbɨ-tɨ</ta>
            <ta e="T145" id="Seg_2306" s="T144">olɨ-tar-gaːlɨ-k</ta>
            <ta e="T146" id="Seg_2307" s="T145">qalɨ-mbɨ</ta>
            <ta e="T147" id="Seg_2308" s="T146">Pöːnege</ta>
            <ta e="T148" id="Seg_2309" s="T147">poːne</ta>
            <ta e="T149" id="Seg_2310" s="T148">čanǯe</ta>
            <ta e="T150" id="Seg_2311" s="T149">i</ta>
            <ta e="T151" id="Seg_2312" s="T150">qwən-ŋ</ta>
            <ta e="T152" id="Seg_2313" s="T151">Kalabok-ka</ta>
            <ta e="T153" id="Seg_2314" s="T152">mɛsala-nče-le</ta>
            <ta e="T154" id="Seg_2315" s="T153">qalɨ</ta>
            <ta e="T155" id="Seg_2316" s="T154">i</ta>
            <ta e="T156" id="Seg_2317" s="T155">olɨ-tar-gaːlɨ-k</ta>
            <ta e="T157" id="Seg_2318" s="T156">üːtǝ-mɨn</ta>
            <ta e="T158" id="Seg_2319" s="T157">töː-ŋɨ-qiː</ta>
            <ta e="T159" id="Seg_2320" s="T158">aǯʼa-tɨ</ta>
            <ta e="T160" id="Seg_2321" s="T159">i</ta>
            <ta e="T161" id="Seg_2322" s="T160">awa-tɨ</ta>
            <ta e="T162" id="Seg_2323" s="T161">a</ta>
            <ta e="T163" id="Seg_2324" s="T162">Kalabok-ka</ta>
            <ta e="T164" id="Seg_2325" s="T163">mɛsala-nče-le</ta>
            <ta e="T165" id="Seg_2326" s="T164">ippi</ta>
            <ta e="T166" id="Seg_2327" s="T165">i</ta>
            <ta e="T167" id="Seg_2328" s="T166">olɨ-tar-gaːlɨ-k</ta>
            <ta e="T168" id="Seg_2329" s="T167">aǯʼa-tɨ</ta>
            <ta e="T169" id="Seg_2330" s="T168">awa-tɨ</ta>
            <ta e="T170" id="Seg_2331" s="T169">soqə-ntɨ-qiː</ta>
            <ta e="T171" id="Seg_2332" s="T170">Kalabok-ka-nɨ</ta>
            <ta e="T172" id="Seg_2333" s="T171">olɨ-tar-ɨ-m-tɨ</ta>
            <ta e="T173" id="Seg_2334" s="T172">kuː</ta>
            <ta e="T174" id="Seg_2335" s="T173">qaːdə-mbɨ-mbɨ-l</ta>
            <ta e="T175" id="Seg_2336" s="T174">i</ta>
            <ta e="T176" id="Seg_2337" s="T175">mɛsala-nče-ntɨ</ta>
            <ta e="T177" id="Seg_2338" s="T176">a</ta>
            <ta e="T178" id="Seg_2339" s="T177">Kalabok-ka</ta>
            <ta e="T179" id="Seg_2340" s="T178">kadɨ-špə-tɨ</ta>
            <ta e="T180" id="Seg_2341" s="T179">mašim</ta>
            <ta e="T181" id="Seg_2342" s="T180">čiča</ta>
            <ta e="T182" id="Seg_2343" s="T181">čiča</ta>
            <ta e="T183" id="Seg_2344" s="T182">loːzi-mɨ</ta>
            <ta e="T184" id="Seg_2345" s="T183">poːlɛ-nče-r-sɨ</ta>
            <ta e="T185" id="Seg_2346" s="T184">i</ta>
            <ta e="T186" id="Seg_2347" s="T185">mašim</ta>
            <ta e="T187" id="Seg_2348" s="T186">tildiŋ</ta>
            <ta e="T188" id="Seg_2349" s="T187">nɨlʼǯi</ta>
            <ta e="T189" id="Seg_2350" s="T188">loːzi-ɨ-mɨ</ta>
            <ta e="T190" id="Seg_2351" s="T189">poːlɛ-nče-r-sɨ</ta>
            <ta e="T191" id="Seg_2352" s="T190">tɔːptɨlʼ</ta>
            <ta e="T192" id="Seg_2353" s="T191">tʼeːlə</ta>
            <ta e="T193" id="Seg_2354" s="T192">aǯʼa-tɨ</ta>
            <ta e="T194" id="Seg_2355" s="T193">awa-tɨ</ta>
            <ta e="T195" id="Seg_2356" s="T194">kuː-naj</ta>
            <ta e="T196" id="Seg_2357" s="T195">assa</ta>
            <ta e="T197" id="Seg_2358" s="T196">qwən-ŋɨ-qiː</ta>
            <ta e="T198" id="Seg_2359" s="T197">qwən-ŋɨ-qiː</ta>
            <ta e="T199" id="Seg_2360" s="T198">maːt</ta>
            <ta e="T200" id="Seg_2361" s="T199">tʼeːlə-tɨ-gu</ta>
            <ta e="T201" id="Seg_2362" s="T200">tʼeːlə-tɨ-gu</ta>
            <ta e="T202" id="Seg_2363" s="T201">qalɨ-ɨ-qiː</ta>
            <ta e="T203" id="Seg_2364" s="T202">aǯʼa-tɨ</ta>
            <ta e="T204" id="Seg_2365" s="T203">i</ta>
            <ta e="T205" id="Seg_2366" s="T204">awa-tɨ</ta>
            <ta e="T206" id="Seg_2367" s="T205">čannɛ</ta>
            <ta e="T207" id="Seg_2368" s="T206">tɨrɨ</ta>
            <ta e="T208" id="Seg_2369" s="T207">qwäːl-ɨ-n</ta>
            <ta e="T209" id="Seg_2370" s="T208">nɨːndepo-m</ta>
            <ta e="T210" id="Seg_2371" s="T209">aːŋ-ɨ-n</ta>
            <ta e="T211" id="Seg_2372" s="T210">toːp-n</ta>
            <ta e="T212" id="Seg_2373" s="T211">toːp-n</ta>
            <ta e="T213" id="Seg_2374" s="T212">qamǯə-qiː</ta>
            <ta e="T214" id="Seg_2375" s="T213">a</ta>
            <ta e="T215" id="Seg_2376" s="T214">onǯe</ta>
            <ta e="T216" id="Seg_2377" s="T215">maːta-n</ta>
            <ta e="T217" id="Seg_2378" s="T216">medo-lʼ</ta>
            <ta e="T218" id="Seg_2379" s="T217">pɛläk-ɨ-ntɨ</ta>
            <ta e="T219" id="Seg_2380" s="T218">nɨ-lɨ-dʼi-qiː</ta>
            <ta e="T220" id="Seg_2381" s="T219">aǯʼa-tɨ</ta>
            <ta e="T221" id="Seg_2382" s="T220">petʼ</ta>
            <ta e="T222" id="Seg_2383" s="T221">iː-ntɨ-tɨ</ta>
            <ta e="T223" id="Seg_2384" s="T222">petʼ-se</ta>
            <ta e="T224" id="Seg_2385" s="T223">nɨ-ŋɨ</ta>
            <ta e="T225" id="Seg_2386" s="T224">petʼ-se</ta>
            <ta e="T226" id="Seg_2387" s="T225">nɨ-lɨ-dʼi</ta>
            <ta e="T227" id="Seg_2388" s="T226">a</ta>
            <ta e="T228" id="Seg_2389" s="T227">paja-tɨ</ta>
            <ta e="T229" id="Seg_2390" s="T228">awa-tɨ</ta>
            <ta e="T230" id="Seg_2391" s="T229">soma-lʼ</ta>
            <ta e="T231" id="Seg_2392" s="T230">püː</ta>
            <ta e="T232" id="Seg_2393" s="T231">iː-ntɨ-tɨ</ta>
            <ta e="T233" id="Seg_2394" s="T232">a</ta>
            <ta e="T234" id="Seg_2395" s="T233">Kalabok-ka-nɨ</ta>
            <ta e="T235" id="Seg_2396" s="T234">čenčɨ-ɨ-qiː</ta>
            <ta e="T236" id="Seg_2397" s="T235">a</ta>
            <ta e="T237" id="Seg_2398" s="T236">tan</ta>
            <ta e="T238" id="Seg_2399" s="T237">Kalabok-ka</ta>
            <ta e="T239" id="Seg_2400" s="T238">maːt-ɨ-m</ta>
            <ta e="T240" id="Seg_2401" s="T239">tʼaze-lčɨ-mbɨ-lä-l</ta>
            <ta e="T241" id="Seg_2402" s="T240">Kalabok-ka</ta>
            <ta e="T242" id="Seg_2403" s="T241">lɨː-r-ŋɨ</ta>
            <ta e="T243" id="Seg_2404" s="T242">po-ntɨ-se</ta>
            <ta e="T244" id="Seg_2405" s="T243">qätə-lɨ-špə-tɨ</ta>
            <ta e="T245" id="Seg_2406" s="T244">qaj-m-ta</ta>
            <ta e="T246" id="Seg_2407" s="T245">unde-tɨ</ta>
            <ta e="T247" id="Seg_2408" s="T246">unde-qiː</ta>
            <ta e="T248" id="Seg_2409" s="T247">unde-tɨt</ta>
            <ta e="T249" id="Seg_2410" s="T248">Pöːnege</ta>
            <ta e="T250" id="Seg_2411" s="T249">čaːǯɨ</ta>
            <ta e="T251" id="Seg_2412" s="T250">maːt</ta>
            <ta e="T252" id="Seg_2413" s="T251">šeːr-ɨ-špə</ta>
            <ta e="T253" id="Seg_2414" s="T252">Pöːnege</ta>
            <ta e="T254" id="Seg_2415" s="T253">maːta-n</ta>
            <ta e="T255" id="Seg_2416" s="T254">toːp</ta>
            <ta e="T256" id="Seg_2417" s="T255">aʒɛ-ŋɨ-tɨ</ta>
            <ta e="T257" id="Seg_2418" s="T256">qwäːl-ɨ-n</ta>
            <ta e="T258" id="Seg_2419" s="T257">nɨːndepo-ntɨ</ta>
            <ta e="T259" id="Seg_2420" s="T258">tita-lʼčǝ</ta>
            <ta e="T260" id="Seg_2421" s="T259">i</ta>
            <ta e="T261" id="Seg_2422" s="T260">nʼuqqɨ-l-wat-le</ta>
            <ta e="T262" id="Seg_2423" s="T261">ɨlə</ta>
            <ta e="T263" id="Seg_2424" s="T262">alʼči</ta>
            <ta e="T264" id="Seg_2425" s="T263">i</ta>
            <ta e="T265" id="Seg_2426" s="T264">na-št</ta>
            <ta e="T266" id="Seg_2427" s="T265">qarɨ-n</ta>
            <ta e="T267" id="Seg_2428" s="T266">aǯʼa-tɨ</ta>
            <ta e="T268" id="Seg_2429" s="T267">i</ta>
            <ta e="T269" id="Seg_2430" s="T268">awa-tɨ</ta>
            <ta e="T270" id="Seg_2431" s="T269">aǯʼa-tɨ</ta>
            <ta e="T271" id="Seg_2432" s="T270">petʼ-se</ta>
            <ta e="T272" id="Seg_2433" s="T271">Pöːnege-n</ta>
            <ta e="T273" id="Seg_2434" s="T272">olɨ-m</ta>
            <ta e="T274" id="Seg_2435" s="T273">padʼa-ŋɨ-tɨ</ta>
            <ta e="T275" id="Seg_2436" s="T274">Pöːnege-m</ta>
            <ta e="T276" id="Seg_2437" s="T275">qwən-ŋɨ-qiː</ta>
            <ta e="T277" id="Seg_2438" s="T276">poːne</ta>
            <ta e="T278" id="Seg_2439" s="T277">wargi</ta>
            <ta e="T279" id="Seg_2440" s="T278">tüː-ʒom-ɨ-m</ta>
            <ta e="T280" id="Seg_2441" s="T279">pat-ŋɨ-qiː</ta>
            <ta e="T281" id="Seg_2442" s="T280">Pöːnege-m</ta>
            <ta e="T282" id="Seg_2443" s="T281">na</ta>
            <ta e="T283" id="Seg_2444" s="T282">wargi</ta>
            <ta e="T284" id="Seg_2445" s="T283">tüː-ʒom-nɨ</ta>
            <ta e="T285" id="Seg_2446" s="T284">čačɨ-qiː</ta>
            <ta e="T286" id="Seg_2447" s="T285">Pöːnege-m</ta>
            <ta e="T287" id="Seg_2448" s="T286">tüː</ta>
            <ta e="T288" id="Seg_2449" s="T287">am-ɨ-špə-tɨ</ta>
            <ta e="T289" id="Seg_2450" s="T288">Pöːnege-n</ta>
            <ta e="T290" id="Seg_2451" s="T289">teːwɛ-la-tɨ</ta>
            <ta e="T291" id="Seg_2452" s="T290">qo-lʼčǝ-wat-lewlə</ta>
            <ta e="T292" id="Seg_2453" s="T291">tüː-qɨntɨ-k</ta>
            <ta e="T293" id="Seg_2454" s="T292">teː</ta>
            <ta e="T294" id="Seg_2455" s="T293">ɨlə</ta>
            <ta e="T295" id="Seg_2456" s="T294">paktǝ-r-ŋɨ-tɨt</ta>
            <ta e="T296" id="Seg_2457" s="T295">Pöːnege-m</ta>
            <ta e="T297" id="Seg_2458" s="T296">tüː</ta>
            <ta e="T298" id="Seg_2459" s="T297">am-ŋɨ-tɨ</ta>
            <ta e="T299" id="Seg_2460" s="T298">i</ta>
            <ta e="T300" id="Seg_2461" s="T299">tʼipeːrʼ-ta</ta>
            <ta e="T301" id="Seg_2462" s="T300">elɨ-tɨt</ta>
            <ta e="T302" id="Seg_2463" s="T301">i</ta>
            <ta e="T303" id="Seg_2464" s="T302">warkə-tɨt</ta>
            <ta e="T304" id="Seg_2465" s="T303">i</ta>
            <ta e="T305" id="Seg_2466" s="T304">Kalabok-ka</ta>
            <ta e="T306" id="Seg_2467" s="T305">naj</ta>
            <ta e="T307" id="Seg_2468" s="T306">or-ɨ-m-mbɨ</ta>
            <ta e="T308" id="Seg_2469" s="T307">i</ta>
            <ta e="T309" id="Seg_2470" s="T308">Pöːnege-la</ta>
            <ta e="T310" id="Seg_2471" s="T309">naj</ta>
            <ta e="T311" id="Seg_2472" s="T310">čaŋkɨ-ŋɨ-tɨt</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_2473" s="T1">live-1SG.S-CO-3PL</ta>
            <ta e="T3" id="Seg_2474" s="T2">live-1SG.S-CO-3PL</ta>
            <ta e="T4" id="Seg_2475" s="T3">old.man.[NOM]</ta>
            <ta e="T5" id="Seg_2476" s="T4">old.woman-DYA-DU</ta>
            <ta e="T6" id="Seg_2477" s="T5">(s)he-EP-DU-GEN-ADES</ta>
            <ta e="T7" id="Seg_2478" s="T6">be-HAB-CO.[3SG.S]</ta>
            <ta e="T8" id="Seg_2479" s="T7">son.[NOM]-3SG</ta>
            <ta e="T9" id="Seg_2480" s="T8">name.[NOM]-3SG</ta>
            <ta e="T10" id="Seg_2481" s="T9">Kalabok-DIM.[NOM]</ta>
            <ta e="T11" id="Seg_2482" s="T10">spring-3SG</ta>
            <ta e="T12" id="Seg_2483" s="T11">achieve.[3SG.S]</ta>
            <ta e="T13" id="Seg_2484" s="T12">father-mother-3SG</ta>
            <ta e="T14" id="Seg_2485" s="T13">go.away-CO-3PL</ta>
            <ta e="T15" id="Seg_2486" s="T14">tree-CAP-EP-FRQ-INF</ta>
            <ta e="T16" id="Seg_2487" s="T15">forest-ILL</ta>
            <ta e="T17" id="Seg_2488" s="T16">but</ta>
            <ta e="T18" id="Seg_2489" s="T17">Kalabok-DIM-ALL</ta>
            <ta e="T19" id="Seg_2490" s="T18">punish-3PL</ta>
            <ta e="T20" id="Seg_2491" s="T19">you.SG.NOM</ta>
            <ta e="T21" id="Seg_2492" s="T20">house-LOC</ta>
            <ta e="T22" id="Seg_2493" s="T21">mute-ADVZ</ta>
            <ta e="T23" id="Seg_2494" s="T22">live-IMP.2SG.S</ta>
            <ta e="T24" id="Seg_2495" s="T23">house-EP-ACC</ta>
            <ta e="T25" id="Seg_2496" s="T24">NEG.IMP</ta>
            <ta e="T26" id="Seg_2497" s="T25">fool.around-PFV-DUR-IMP.2SG.O</ta>
            <ta e="T27" id="Seg_2498" s="T26">fool.around-PFV-COND-FUT-2SG.O</ta>
            <ta e="T28" id="Seg_2499" s="T27">Pönege.[NOM]</ta>
            <ta e="T29" id="Seg_2500" s="T28">come-FUT.[3SG.S]</ta>
            <ta e="T30" id="Seg_2501" s="T29">and</ta>
            <ta e="T31" id="Seg_2502" s="T30">burn-FUT.[3SG.S]</ta>
            <ta e="T32" id="Seg_2503" s="T31">you.SG.ACC</ta>
            <ta e="T33" id="Seg_2504" s="T32">father-3SG</ta>
            <ta e="T34" id="Seg_2505" s="T33">and</ta>
            <ta e="T35" id="Seg_2506" s="T34">mother.[NOM]-3SG</ta>
            <ta e="T36" id="Seg_2507" s="T35">go.away-CO-3DU.S</ta>
            <ta e="T37" id="Seg_2508" s="T36">forest-ILL</ta>
            <ta e="T38" id="Seg_2509" s="T37">tree-CAP-EP-FRQ-INF</ta>
            <ta e="T39" id="Seg_2510" s="T38">Kalabok-DIM.[NOM]</ta>
            <ta e="T40" id="Seg_2511" s="T39">stay.[3SG.S]</ta>
            <ta e="T41" id="Seg_2512" s="T40">friend-CAR-ADVZ</ta>
            <ta e="T42" id="Seg_2513" s="T41">father.[NOM]</ta>
            <ta e="T43" id="Seg_2514" s="T42">father.[NOM]</ta>
            <ta e="T44" id="Seg_2515" s="T43">and</ta>
            <ta e="T45" id="Seg_2516" s="T44">mother.[NOM]</ta>
            <ta e="T46" id="Seg_2517" s="T45">what</ta>
            <ta e="T47" id="Seg_2518" s="T46">say-3DU.S</ta>
            <ta e="T48" id="Seg_2519" s="T47">Pönege.[NOM]</ta>
            <ta e="T49" id="Seg_2520" s="T48">come-FUT.[3SG.S]</ta>
            <ta e="T50" id="Seg_2521" s="T49">I.ACC</ta>
            <ta e="T51" id="Seg_2522" s="T50">eat-INF</ta>
            <ta e="T52" id="Seg_2523" s="T51">but</ta>
            <ta e="T53" id="Seg_2524" s="T52">I.NOM</ta>
            <ta e="T54" id="Seg_2525" s="T53">small</ta>
            <ta e="T55" id="Seg_2526" s="T54">knife-INSTR</ta>
            <ta e="T56" id="Seg_2527" s="T55">still</ta>
            <ta e="T57" id="Seg_2528" s="T56">Pönege-ACC</ta>
            <ta e="T58" id="Seg_2529" s="T57">cut-FUT-1SG.O</ta>
            <ta e="T59" id="Seg_2530" s="T58">small</ta>
            <ta e="T60" id="Seg_2531" s="T59">knife-ACC-3SG</ta>
            <ta e="T61" id="Seg_2532" s="T60">sharpen-IPFV2-3SG.O</ta>
            <ta e="T62" id="Seg_2533" s="T61">but</ta>
            <ta e="T63" id="Seg_2534" s="T62">oneself.3SG</ta>
            <ta e="T64" id="Seg_2535" s="T63">sing-FRQ-EP.[3SG.S]</ta>
            <ta e="T65" id="Seg_2536" s="T64">house-EP-ACC-3SG</ta>
            <ta e="T66" id="Seg_2537" s="T65">fool.around-TR-DUR-3SG.O</ta>
            <ta e="T67" id="Seg_2538" s="T66">then</ta>
            <ta e="T68" id="Seg_2539" s="T67">do-CVB</ta>
            <ta e="T69" id="Seg_2540" s="T68">hear-3SG.O</ta>
            <ta e="T70" id="Seg_2541" s="T69">what-INDEF</ta>
            <ta e="T71" id="Seg_2542" s="T70">go.[3SG.S]</ta>
            <ta e="T72" id="Seg_2543" s="T71">leg-PL-OBL.3SG-INSTR</ta>
            <ta e="T73" id="Seg_2544" s="T72">sound-DETR-CVB2</ta>
            <ta e="T74" id="Seg_2545" s="T73">Kalabok-DIM.[NOM]</ta>
            <ta e="T75" id="Seg_2546" s="T74">small</ta>
            <ta e="T76" id="Seg_2547" s="T75">knife-OBL.3SG-INSTR</ta>
            <ta e="T77" id="Seg_2548" s="T76">bed-GEN</ta>
            <ta e="T78" id="Seg_2549" s="T77">bottom-ILL</ta>
            <ta e="T79" id="Seg_2550" s="T78">hide-IPFV2.[3SG.S]</ta>
            <ta e="T80" id="Seg_2551" s="T79">Pönege.[NOM]</ta>
            <ta e="T81" id="Seg_2552" s="T80">house.[NOM]</ta>
            <ta e="T82" id="Seg_2553" s="T81">go.in(to)-CO.[3SG.S]</ta>
            <ta e="T83" id="Seg_2554" s="T82">Kalabok-DIM-ACC</ta>
            <ta e="T84" id="Seg_2555" s="T83">bed-GEN</ta>
            <ta e="T85" id="Seg_2556" s="T84">down-EL.3SG</ta>
            <ta e="T86" id="Seg_2557" s="T85">pull.out-3SG.O</ta>
            <ta e="T87" id="Seg_2558" s="T86">oneself.3SG</ta>
            <ta e="T88" id="Seg_2559" s="T87">stature-ILL.3SG</ta>
            <ta e="T89" id="Seg_2560" s="T88">and</ta>
            <ta e="T90" id="Seg_2561" s="T89">Kalabok-DIM-ACC</ta>
            <ta e="T91" id="Seg_2562" s="T90">swallow-IPFV2-3SG.O</ta>
            <ta e="T92" id="Seg_2563" s="T91">Kalabok-DIM-GEN-ADES</ta>
            <ta e="T93" id="Seg_2564" s="T92">be-CO.[3SG.S]</ta>
            <ta e="T94" id="Seg_2565" s="T93">small</ta>
            <ta e="T95" id="Seg_2566" s="T94">knife.[NOM]</ta>
            <ta e="T96" id="Seg_2567" s="T95">sit.[3SG.S]</ta>
            <ta e="T97" id="Seg_2568" s="T96">Pönege-GEN</ta>
            <ta e="T98" id="Seg_2569" s="T97">stomach-LOC</ta>
            <ta e="T99" id="Seg_2570" s="T98">small</ta>
            <ta e="T100" id="Seg_2571" s="T99">knife-OBL.3SG-INSTR</ta>
            <ta e="T101" id="Seg_2572" s="T100">stomach-LOC.3SG</ta>
            <ta e="T102" id="Seg_2573" s="T101">here</ta>
            <ta e="T103" id="Seg_2574" s="T102">there</ta>
            <ta e="T104" id="Seg_2575" s="T103">knife-PFV-IPFV2-3SG.O</ta>
            <ta e="T105" id="Seg_2576" s="T104">Pönege.[NOM]</ta>
            <ta e="T106" id="Seg_2577" s="T105">say.[3SG.S]</ta>
            <ta e="T107" id="Seg_2578" s="T106">Kalabok-DIM.[NOM]</ta>
            <ta e="T108" id="Seg_2579" s="T107">Kalabok-DIM.[NOM]</ta>
            <ta e="T109" id="Seg_2580" s="T108">I.NOM-ADES-ADJZ</ta>
            <ta e="T110" id="Seg_2581" s="T109">stomach-ACC</ta>
            <ta e="T111" id="Seg_2582" s="T110">NEG.IMP</ta>
            <ta e="T112" id="Seg_2583" s="T111">knife-PFV-IPFV2-OPT-2SG.O</ta>
            <ta e="T113" id="Seg_2584" s="T112">but</ta>
            <ta e="T114" id="Seg_2585" s="T113">Kalabok-DIM.[NOM]</ta>
            <ta e="T115" id="Seg_2586" s="T114">stomach-LOC</ta>
            <ta e="T116" id="Seg_2587" s="T115">this</ta>
            <ta e="T117" id="Seg_2588" s="T116">small</ta>
            <ta e="T118" id="Seg_2589" s="T117">knife-DIM-OBL.3SG-INSTR</ta>
            <ta e="T119" id="Seg_2590" s="T118">Pönege-GEN</ta>
            <ta e="T120" id="Seg_2591" s="T119">heart-ACC</ta>
            <ta e="T121" id="Seg_2592" s="T120">prick-INCH-CVB</ta>
            <ta e="T122" id="Seg_2593" s="T121">begin-3SG.O</ta>
            <ta e="T123" id="Seg_2594" s="T122">but</ta>
            <ta e="T124" id="Seg_2595" s="T123">Pönege.[NOM]</ta>
            <ta e="T125" id="Seg_2596" s="T124">say.[3SG.S]</ta>
            <ta e="T126" id="Seg_2597" s="T125">Kalabok-DIM.[NOM]</ta>
            <ta e="T127" id="Seg_2598" s="T126">Kalabok.[NOM]</ta>
            <ta e="T128" id="Seg_2599" s="T127">I.NOM-heart-ACC</ta>
            <ta e="T129" id="Seg_2600" s="T128">NEG.IMP</ta>
            <ta e="T130" id="Seg_2601" s="T129">prick-RES-IPFV2-OPT-2SG.O</ta>
            <ta e="T131" id="Seg_2602" s="T130">I.NOM</ta>
            <ta e="T132" id="Seg_2603" s="T131">you.SG.ACC</ta>
            <ta e="T133" id="Seg_2604" s="T132">shit-EP-FUT-1SG.S</ta>
            <ta e="T134" id="Seg_2605" s="T133">you.SG.ACC</ta>
            <ta e="T135" id="Seg_2606" s="T134">Pönege.[NOM]</ta>
            <ta e="T136" id="Seg_2607" s="T135">Kalabok-DIM-ACC</ta>
            <ta e="T137" id="Seg_2608" s="T136">defecate-EP-3SG.O</ta>
            <ta e="T138" id="Seg_2609" s="T137">back</ta>
            <ta e="T139" id="Seg_2610" s="T138">down</ta>
            <ta e="T140" id="Seg_2611" s="T139">Kalabok-DIM-ACC</ta>
            <ta e="T141" id="Seg_2612" s="T140">head-GEN</ta>
            <ta e="T142" id="Seg_2613" s="T141">hair-EP-PL.[NOM]</ta>
            <ta e="T143" id="Seg_2614" s="T142">Pönege.[NOM]</ta>
            <ta e="T144" id="Seg_2615" s="T143">spit-INCH-PST.NAR-3SG.O</ta>
            <ta e="T145" id="Seg_2616" s="T144">head-hair-CAR-ADVZ</ta>
            <ta e="T146" id="Seg_2617" s="T145">stay-PST.NAR.[3SG.S]</ta>
            <ta e="T147" id="Seg_2618" s="T146">Pönege.[NOM]</ta>
            <ta e="T148" id="Seg_2619" s="T147">outward(s)</ta>
            <ta e="T149" id="Seg_2620" s="T148">go.out.[3SG.S]</ta>
            <ta e="T150" id="Seg_2621" s="T149">and</ta>
            <ta e="T151" id="Seg_2622" s="T150">go.away-3SG.S</ta>
            <ta e="T152" id="Seg_2623" s="T151">Kalabok-DIM.[NOM]</ta>
            <ta e="T153" id="Seg_2624" s="T152">be.ill-IPFV3-CVB</ta>
            <ta e="T154" id="Seg_2625" s="T153">stay.[3SG.S]</ta>
            <ta e="T155" id="Seg_2626" s="T154">and</ta>
            <ta e="T156" id="Seg_2627" s="T155">head-hair-CAR-ADVZ</ta>
            <ta e="T157" id="Seg_2628" s="T156">evening-PROL</ta>
            <ta e="T158" id="Seg_2629" s="T157">come-CO-3DU.S</ta>
            <ta e="T159" id="Seg_2630" s="T158">father.[NOM]-3SG</ta>
            <ta e="T160" id="Seg_2631" s="T159">and</ta>
            <ta e="T161" id="Seg_2632" s="T160">mother.[NOM]-3SG</ta>
            <ta e="T162" id="Seg_2633" s="T161">but</ta>
            <ta e="T163" id="Seg_2634" s="T162">Kalabok-DIM.[NOM]</ta>
            <ta e="T164" id="Seg_2635" s="T163">be.ill-IPFV3-CVB</ta>
            <ta e="T165" id="Seg_2636" s="T164">lie.[3SG.S]</ta>
            <ta e="T166" id="Seg_2637" s="T165">and</ta>
            <ta e="T167" id="Seg_2638" s="T166">head-hair-CAR-ADVZ</ta>
            <ta e="T168" id="Seg_2639" s="T167">father.[NOM]-3SG</ta>
            <ta e="T169" id="Seg_2640" s="T168">mother.[NOM]-3SG</ta>
            <ta e="T170" id="Seg_2641" s="T169">ask-IPFV2-3DU.S</ta>
            <ta e="T171" id="Seg_2642" s="T170">Kalabok-DIM-ALL</ta>
            <ta e="T172" id="Seg_2643" s="T171">head-hair-EP-ACC-3SG</ta>
            <ta e="T173" id="Seg_2644" s="T172">where</ta>
            <ta e="T174" id="Seg_2645" s="T173">scratch-DUR-PST.NAR-2SG.O</ta>
            <ta e="T175" id="Seg_2646" s="T174">and</ta>
            <ta e="T176" id="Seg_2647" s="T175">be.ill-IPFV3-2SG.S</ta>
            <ta e="T177" id="Seg_2648" s="T176">but</ta>
            <ta e="T178" id="Seg_2649" s="T177">Kalabok-DIM.[NOM]</ta>
            <ta e="T179" id="Seg_2650" s="T178">tell-IPFV2-3SG.O</ta>
            <ta e="T180" id="Seg_2651" s="T179">I.ACC</ta>
            <ta e="T181" id="Seg_2652" s="T180">grandfather.[NOM]</ta>
            <ta e="T182" id="Seg_2653" s="T181">grandfather.[NOM]</ta>
            <ta e="T183" id="Seg_2654" s="T182">devil-1SG</ta>
            <ta e="T184" id="Seg_2655" s="T183">swallow-IPFV3-FRQ-PST.[3SG.S]</ta>
            <ta e="T185" id="Seg_2656" s="T184">and</ta>
            <ta e="T186" id="Seg_2657" s="T185">I.ACC</ta>
            <ta e="T187" id="Seg_2658" s="T186">such</ta>
            <ta e="T188" id="Seg_2659" s="T187">such</ta>
            <ta e="T189" id="Seg_2660" s="T188">devil-EP-1SG</ta>
            <ta e="T190" id="Seg_2661" s="T189">swallow-IPFV3-FRQ-PST.[3SG.S]</ta>
            <ta e="T191" id="Seg_2662" s="T190">next</ta>
            <ta e="T192" id="Seg_2663" s="T191">sun.[NOM]</ta>
            <ta e="T193" id="Seg_2664" s="T192">father.[NOM]-3SG</ta>
            <ta e="T194" id="Seg_2665" s="T193">mother.[NOM]-3SG</ta>
            <ta e="T195" id="Seg_2666" s="T194">where-EMPH</ta>
            <ta e="T196" id="Seg_2667" s="T195">NEG</ta>
            <ta e="T197" id="Seg_2668" s="T196">go.away-CO-3DU.S</ta>
            <ta e="T198" id="Seg_2669" s="T197">go.away-CO-3DU.S</ta>
            <ta e="T199" id="Seg_2670" s="T198">at.home</ta>
            <ta e="T200" id="Seg_2671" s="T199">day-TR-INF</ta>
            <ta e="T201" id="Seg_2672" s="T200">day-TR-INF</ta>
            <ta e="T202" id="Seg_2673" s="T201">stay-EP-3DU.S</ta>
            <ta e="T203" id="Seg_2674" s="T202">father.[NOM]-3SG</ta>
            <ta e="T204" id="Seg_2675" s="T203">and</ta>
            <ta e="T205" id="Seg_2676" s="T204">mother.[NOM]-3SG</ta>
            <ta e="T206" id="Seg_2677" s="T205">trough.[NOM]</ta>
            <ta e="T207" id="Seg_2678" s="T206">full</ta>
            <ta e="T208" id="Seg_2679" s="T207">fish-EP-GEN</ta>
            <ta e="T209" id="Seg_2680" s="T208">slime-ACC</ta>
            <ta e="T210" id="Seg_2681" s="T209">mouth-EP-GEN</ta>
            <ta e="T211" id="Seg_2682" s="T210">border-ADV.LOC</ta>
            <ta e="T212" id="Seg_2683" s="T211">border-ADV.LOC</ta>
            <ta e="T213" id="Seg_2684" s="T212">pour-3DU.S</ta>
            <ta e="T214" id="Seg_2685" s="T213">but</ta>
            <ta e="T215" id="Seg_2686" s="T214">oneself.3SG</ta>
            <ta e="T216" id="Seg_2687" s="T215">door-GEN</ta>
            <ta e="T217" id="Seg_2688" s="T216">%%-ADJZ</ta>
            <ta e="T218" id="Seg_2689" s="T217">side-EP-ILL</ta>
            <ta e="T219" id="Seg_2690" s="T218">stand-RES-DRV-3DU.S</ta>
            <ta e="T220" id="Seg_2691" s="T219">father.[NOM]-3SG</ta>
            <ta e="T221" id="Seg_2692" s="T220">axe.[NOM]</ta>
            <ta e="T222" id="Seg_2693" s="T221">take-IPFV2-3SG.O</ta>
            <ta e="T223" id="Seg_2694" s="T222">axe-INSTR</ta>
            <ta e="T224" id="Seg_2695" s="T223">stand-CO.[3SG.S]</ta>
            <ta e="T225" id="Seg_2696" s="T224">axe-INSTR</ta>
            <ta e="T226" id="Seg_2697" s="T225">stand-RES-DRV.[3SG.S]</ta>
            <ta e="T227" id="Seg_2698" s="T226">but</ta>
            <ta e="T228" id="Seg_2699" s="T227">old.woman.[NOM]-3SG</ta>
            <ta e="T229" id="Seg_2700" s="T228">mother.[NOM]-3SG</ta>
            <ta e="T230" id="Seg_2701" s="T229">mortar-ADJZ</ta>
            <ta e="T231" id="Seg_2702" s="T230">stone.[NOM]</ta>
            <ta e="T232" id="Seg_2703" s="T231">take-INFER-3SG.O</ta>
            <ta e="T233" id="Seg_2704" s="T232">but</ta>
            <ta e="T234" id="Seg_2705" s="T233">Kalabok-DIM-ALL</ta>
            <ta e="T235" id="Seg_2706" s="T234">say-EP-3DU.S</ta>
            <ta e="T236" id="Seg_2707" s="T235">but</ta>
            <ta e="T237" id="Seg_2708" s="T236">you.SG.NOM</ta>
            <ta e="T238" id="Seg_2709" s="T237">Kalabok-DIM.[NOM]</ta>
            <ta e="T239" id="Seg_2710" s="T238">house-EP-ACC</ta>
            <ta e="T240" id="Seg_2711" s="T239">fool.around-TR-DUR-OPT-2SG.O</ta>
            <ta e="T241" id="Seg_2712" s="T240">Kalabok-DIM.[NOM]</ta>
            <ta e="T242" id="Seg_2713" s="T241">sing-FRQ-CO.[3SG.S]</ta>
            <ta e="T243" id="Seg_2714" s="T242">tree-OBL.3SG-INSTR</ta>
            <ta e="T244" id="Seg_2715" s="T243">beat-RES-IPFV2-3SG.O</ta>
            <ta e="T245" id="Seg_2716" s="T244">what-ACC-INDEF</ta>
            <ta e="T246" id="Seg_2717" s="T245">hear-3SG.O</ta>
            <ta e="T247" id="Seg_2718" s="T246">hear-3DU.S</ta>
            <ta e="T248" id="Seg_2719" s="T247">hear-3PL</ta>
            <ta e="T249" id="Seg_2720" s="T248">Pönege.[NOM]</ta>
            <ta e="T250" id="Seg_2721" s="T249">go.[3SG.S]</ta>
            <ta e="T251" id="Seg_2722" s="T250">house.[NOM]</ta>
            <ta e="T252" id="Seg_2723" s="T251">go.in(to)-EP-IPFV2.[3SG.S]</ta>
            <ta e="T253" id="Seg_2724" s="T252">Pönege.[NOM]</ta>
            <ta e="T254" id="Seg_2725" s="T253">door-GEN</ta>
            <ta e="T255" id="Seg_2726" s="T254">border.[NOM]</ta>
            <ta e="T256" id="Seg_2727" s="T255">stride-CO-3SG.O</ta>
            <ta e="T257" id="Seg_2728" s="T256">fish-EP-GEN</ta>
            <ta e="T258" id="Seg_2729" s="T257">slime-ILL</ta>
            <ta e="T259" id="Seg_2730" s="T258">walk-PFV.[3SG.S]</ta>
            <ta e="T260" id="Seg_2731" s="T259">and</ta>
            <ta e="T261" id="Seg_2732" s="T260">glide-INCH-DETR-CVB</ta>
            <ta e="T262" id="Seg_2733" s="T261">down</ta>
            <ta e="T263" id="Seg_2734" s="T262">fall.[3SG.S]</ta>
            <ta e="T264" id="Seg_2735" s="T263">and</ta>
            <ta e="T265" id="Seg_2736" s="T264">this-%%</ta>
            <ta e="T266" id="Seg_2737" s="T265">morning-ADV.LOC</ta>
            <ta e="T267" id="Seg_2738" s="T266">father.[NOM]-3SG</ta>
            <ta e="T268" id="Seg_2739" s="T267">and</ta>
            <ta e="T269" id="Seg_2740" s="T268">mother.[NOM]-3SG</ta>
            <ta e="T270" id="Seg_2741" s="T269">father.[NOM]-3SG</ta>
            <ta e="T271" id="Seg_2742" s="T270">axe-INSTR</ta>
            <ta e="T272" id="Seg_2743" s="T271">Pönege-GEN</ta>
            <ta e="T273" id="Seg_2744" s="T272">head-ACC</ta>
            <ta e="T274" id="Seg_2745" s="T273">chop.off-CO-3SG.O</ta>
            <ta e="T275" id="Seg_2746" s="T274">Pönege-ACC</ta>
            <ta e="T276" id="Seg_2747" s="T275">go.away-CO-3DU.S</ta>
            <ta e="T277" id="Seg_2748" s="T276">outward(s)</ta>
            <ta e="T278" id="Seg_2749" s="T277">big</ta>
            <ta e="T279" id="Seg_2750" s="T278">fire-AUGM2-EP-ACC</ta>
            <ta e="T280" id="Seg_2751" s="T279">put-CO-3DU.S</ta>
            <ta e="T281" id="Seg_2752" s="T280">Pönege-ACC</ta>
            <ta e="T282" id="Seg_2753" s="T281">this</ta>
            <ta e="T283" id="Seg_2754" s="T282">big</ta>
            <ta e="T284" id="Seg_2755" s="T283">fire-AUGM2-ALL</ta>
            <ta e="T285" id="Seg_2756" s="T284">throw-3DU.S</ta>
            <ta e="T286" id="Seg_2757" s="T285">Pönege-ACC</ta>
            <ta e="T287" id="Seg_2758" s="T286">fire</ta>
            <ta e="T288" id="Seg_2759" s="T287">eat-EP-IPFV2-3SG.O</ta>
            <ta e="T289" id="Seg_2760" s="T288">Pönege-GEN</ta>
            <ta e="T290" id="Seg_2761" s="T289">tooth-PL.[NOM]-3SG</ta>
            <ta e="T291" id="Seg_2762" s="T290">go-PFV-DETR-CVB2</ta>
            <ta e="T292" id="Seg_2763" s="T291">fire-EL.3SG-%%</ta>
            <ta e="T293" id="Seg_2764" s="T292">away</ta>
            <ta e="T294" id="Seg_2765" s="T293">down</ta>
            <ta e="T295" id="Seg_2766" s="T294">jump-FRQ-CO-3PL</ta>
            <ta e="T296" id="Seg_2767" s="T295">Pönege-ACC</ta>
            <ta e="T297" id="Seg_2768" s="T296">fire.[NOM]</ta>
            <ta e="T298" id="Seg_2769" s="T297">eat-CO-3SG.O</ta>
            <ta e="T299" id="Seg_2770" s="T298">and</ta>
            <ta e="T300" id="Seg_2771" s="T299">now-EMPH2</ta>
            <ta e="T301" id="Seg_2772" s="T300">live-3PL</ta>
            <ta e="T302" id="Seg_2773" s="T301">and</ta>
            <ta e="T303" id="Seg_2774" s="T302">live-3PL</ta>
            <ta e="T304" id="Seg_2775" s="T303">and</ta>
            <ta e="T305" id="Seg_2776" s="T304">Kalabok-DIM.[NOM]</ta>
            <ta e="T306" id="Seg_2777" s="T305">also</ta>
            <ta e="T307" id="Seg_2778" s="T306">force-EP-TRL-PST.NAR.[3SG.S]</ta>
            <ta e="T308" id="Seg_2779" s="T307">and</ta>
            <ta e="T309" id="Seg_2780" s="T308">Pönege-PL.[NOM]</ta>
            <ta e="T310" id="Seg_2781" s="T309">also</ta>
            <ta e="T311" id="Seg_2782" s="T310">NEG.EX-CO-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_2783" s="T1">жить-1SG.S-CO-3PL</ta>
            <ta e="T3" id="Seg_2784" s="T2">жить-1SG.S-CO-3PL</ta>
            <ta e="T4" id="Seg_2785" s="T3">старик.[NOM]</ta>
            <ta e="T5" id="Seg_2786" s="T4">старуха-DYA-DU</ta>
            <ta e="T6" id="Seg_2787" s="T5">он(а)-EP-DU-GEN-ADES</ta>
            <ta e="T7" id="Seg_2788" s="T6">быть-HAB-CO.[3SG.S]</ta>
            <ta e="T8" id="Seg_2789" s="T7">сын.[NOM]-3SG</ta>
            <ta e="T9" id="Seg_2790" s="T8">имя.[NOM]-3SG</ta>
            <ta e="T10" id="Seg_2791" s="T9">Калабок-DIM.[NOM]</ta>
            <ta e="T11" id="Seg_2792" s="T10">весна-3SG</ta>
            <ta e="T12" id="Seg_2793" s="T11">достичь.[3SG.S]</ta>
            <ta e="T13" id="Seg_2794" s="T12">отец-мать-3SG</ta>
            <ta e="T14" id="Seg_2795" s="T13">пойти-CO-3PL</ta>
            <ta e="T15" id="Seg_2796" s="T14">дерево-CAP-EP-FRQ-INF</ta>
            <ta e="T16" id="Seg_2797" s="T15">лес-ILL</ta>
            <ta e="T17" id="Seg_2798" s="T16">а</ta>
            <ta e="T18" id="Seg_2799" s="T17">Калабок-DIM-ALL</ta>
            <ta e="T19" id="Seg_2800" s="T18">наказывать-3PL</ta>
            <ta e="T20" id="Seg_2801" s="T19">ты.NOM</ta>
            <ta e="T21" id="Seg_2802" s="T20">дом-LOC</ta>
            <ta e="T22" id="Seg_2803" s="T21">молча-ADVZ</ta>
            <ta e="T23" id="Seg_2804" s="T22">жить-IMP.2SG.S</ta>
            <ta e="T24" id="Seg_2805" s="T23">дом-EP-ACC</ta>
            <ta e="T25" id="Seg_2806" s="T24">NEG.IMP</ta>
            <ta e="T26" id="Seg_2807" s="T25">баловаться-PFV-DUR-IMP.2SG.O</ta>
            <ta e="T27" id="Seg_2808" s="T26">баловаться-PFV-COND-FUT-2SG.O</ta>
            <ta e="T28" id="Seg_2809" s="T27">Пёнеге.[NOM]</ta>
            <ta e="T29" id="Seg_2810" s="T28">прийти-FUT.[3SG.S]</ta>
            <ta e="T30" id="Seg_2811" s="T29">и</ta>
            <ta e="T31" id="Seg_2812" s="T30">гореть-FUT.[3SG.S]</ta>
            <ta e="T32" id="Seg_2813" s="T31">ты.ACC</ta>
            <ta e="T33" id="Seg_2814" s="T32">отец-3SG</ta>
            <ta e="T34" id="Seg_2815" s="T33">и</ta>
            <ta e="T35" id="Seg_2816" s="T34">мать.[NOM]-3SG</ta>
            <ta e="T36" id="Seg_2817" s="T35">пойти-CO-3DU.S</ta>
            <ta e="T37" id="Seg_2818" s="T36">лес-ILL</ta>
            <ta e="T38" id="Seg_2819" s="T37">дерево-CAP-EP-FRQ-INF</ta>
            <ta e="T39" id="Seg_2820" s="T38">Калабок-DIM.[NOM]</ta>
            <ta e="T40" id="Seg_2821" s="T39">остаться.[3SG.S]</ta>
            <ta e="T41" id="Seg_2822" s="T40">друг-CAR-ADVZ</ta>
            <ta e="T42" id="Seg_2823" s="T41">отец.[NOM]</ta>
            <ta e="T43" id="Seg_2824" s="T42">отец.[NOM]</ta>
            <ta e="T44" id="Seg_2825" s="T43">и</ta>
            <ta e="T45" id="Seg_2826" s="T44">мать.[NOM]</ta>
            <ta e="T46" id="Seg_2827" s="T45">что</ta>
            <ta e="T47" id="Seg_2828" s="T46">сказать-3DU.S</ta>
            <ta e="T48" id="Seg_2829" s="T47">Пёнеге.[NOM]</ta>
            <ta e="T49" id="Seg_2830" s="T48">прийти-FUT.[3SG.S]</ta>
            <ta e="T50" id="Seg_2831" s="T49">я.ACC</ta>
            <ta e="T51" id="Seg_2832" s="T50">есть-INF</ta>
            <ta e="T52" id="Seg_2833" s="T51">а</ta>
            <ta e="T53" id="Seg_2834" s="T52">я.NOM</ta>
            <ta e="T54" id="Seg_2835" s="T53">маленький</ta>
            <ta e="T55" id="Seg_2836" s="T54">нож-INSTR</ta>
            <ta e="T56" id="Seg_2837" s="T55">ещё</ta>
            <ta e="T57" id="Seg_2838" s="T56">Пёнеге-ACC</ta>
            <ta e="T58" id="Seg_2839" s="T57">резать-FUT-1SG.O</ta>
            <ta e="T59" id="Seg_2840" s="T58">маленький</ta>
            <ta e="T60" id="Seg_2841" s="T59">нож-ACC-3SG</ta>
            <ta e="T61" id="Seg_2842" s="T60">наточить-IPFV2-3SG.O</ta>
            <ta e="T62" id="Seg_2843" s="T61">а</ta>
            <ta e="T63" id="Seg_2844" s="T62">сам.3SG</ta>
            <ta e="T64" id="Seg_2845" s="T63">петь-FRQ-EP.[3SG.S]</ta>
            <ta e="T65" id="Seg_2846" s="T64">дом-EP-ACC-3SG</ta>
            <ta e="T66" id="Seg_2847" s="T65">баловаться-TR-DUR-3SG.O</ta>
            <ta e="T67" id="Seg_2848" s="T66">потом</ta>
            <ta e="T68" id="Seg_2849" s="T67">делать-CVB</ta>
            <ta e="T69" id="Seg_2850" s="T68">слышать-3SG.O</ta>
            <ta e="T70" id="Seg_2851" s="T69">что-INDEF</ta>
            <ta e="T71" id="Seg_2852" s="T70">идти.[3SG.S]</ta>
            <ta e="T72" id="Seg_2853" s="T71">нога-PL-OBL.3SG-INSTR</ta>
            <ta e="T73" id="Seg_2854" s="T72">звонить-DETR-CVB2</ta>
            <ta e="T74" id="Seg_2855" s="T73">Калабок-DIM.[NOM]</ta>
            <ta e="T75" id="Seg_2856" s="T74">маленький</ta>
            <ta e="T76" id="Seg_2857" s="T75">нож-OBL.3SG-INSTR</ta>
            <ta e="T77" id="Seg_2858" s="T76">кровать-GEN</ta>
            <ta e="T78" id="Seg_2859" s="T77">дно-ILL</ta>
            <ta e="T79" id="Seg_2860" s="T78">спрягаться-IPFV2.[3SG.S]</ta>
            <ta e="T80" id="Seg_2861" s="T79">Пёнеге.[NOM]</ta>
            <ta e="T81" id="Seg_2862" s="T80">дом.[NOM]</ta>
            <ta e="T82" id="Seg_2863" s="T81">войти-CO.[3SG.S]</ta>
            <ta e="T83" id="Seg_2864" s="T82">Калабок-DIM-ACC</ta>
            <ta e="T84" id="Seg_2865" s="T83">кровать-GEN</ta>
            <ta e="T85" id="Seg_2866" s="T84">вниз-EL.3SG</ta>
            <ta e="T86" id="Seg_2867" s="T85">вытащить-3SG.O</ta>
            <ta e="T87" id="Seg_2868" s="T86">сам.3SG</ta>
            <ta e="T88" id="Seg_2869" s="T87">рост-ILL.3SG</ta>
            <ta e="T89" id="Seg_2870" s="T88">и</ta>
            <ta e="T90" id="Seg_2871" s="T89">Калабок-DIM-ACC</ta>
            <ta e="T91" id="Seg_2872" s="T90">глотать-IPFV2-3SG.O</ta>
            <ta e="T92" id="Seg_2873" s="T91">Калабок-DIM-GEN-ADES</ta>
            <ta e="T93" id="Seg_2874" s="T92">быть-CO.[3SG.S]</ta>
            <ta e="T94" id="Seg_2875" s="T93">маленький</ta>
            <ta e="T95" id="Seg_2876" s="T94">нож.[NOM]</ta>
            <ta e="T96" id="Seg_2877" s="T95">сидеть.[3SG.S]</ta>
            <ta e="T97" id="Seg_2878" s="T96">Пёнеге-GEN</ta>
            <ta e="T98" id="Seg_2879" s="T97">желудок-LOC</ta>
            <ta e="T99" id="Seg_2880" s="T98">маленький</ta>
            <ta e="T100" id="Seg_2881" s="T99">нож-OBL.3SG-INSTR</ta>
            <ta e="T101" id="Seg_2882" s="T100">желудок-LOC.3SG</ta>
            <ta e="T102" id="Seg_2883" s="T101">сюда</ta>
            <ta e="T103" id="Seg_2884" s="T102">туда</ta>
            <ta e="T104" id="Seg_2885" s="T103">исколоть-PFV-IPFV2-3SG.O</ta>
            <ta e="T105" id="Seg_2886" s="T104">Пёнеге.[NOM]</ta>
            <ta e="T106" id="Seg_2887" s="T105">сказать.[3SG.S]</ta>
            <ta e="T107" id="Seg_2888" s="T106">Калабок-DIM.[NOM]</ta>
            <ta e="T108" id="Seg_2889" s="T107">Калабок-DIM.[NOM]</ta>
            <ta e="T109" id="Seg_2890" s="T108">я.NOM-ADES-ADJZ</ta>
            <ta e="T110" id="Seg_2891" s="T109">желудок-ACC</ta>
            <ta e="T111" id="Seg_2892" s="T110">NEG.IMP</ta>
            <ta e="T112" id="Seg_2893" s="T111">исколоть-PFV-IPFV2-OPT-2SG.O</ta>
            <ta e="T113" id="Seg_2894" s="T112">а</ta>
            <ta e="T114" id="Seg_2895" s="T113">Калабок-DIM.[NOM]</ta>
            <ta e="T115" id="Seg_2896" s="T114">желудок-LOC</ta>
            <ta e="T116" id="Seg_2897" s="T115">этот</ta>
            <ta e="T117" id="Seg_2898" s="T116">маленький</ta>
            <ta e="T118" id="Seg_2899" s="T117">нож-DIM-OBL.3SG-INSTR</ta>
            <ta e="T119" id="Seg_2900" s="T118">Пёнеге-GEN</ta>
            <ta e="T120" id="Seg_2901" s="T119">сердце-ACC</ta>
            <ta e="T121" id="Seg_2902" s="T120">воткнуть-INCH-CVB</ta>
            <ta e="T122" id="Seg_2903" s="T121">начать-3SG.O</ta>
            <ta e="T123" id="Seg_2904" s="T122">а</ta>
            <ta e="T124" id="Seg_2905" s="T123">Пёнеге.[NOM]</ta>
            <ta e="T125" id="Seg_2906" s="T124">сказать.[3SG.S]</ta>
            <ta e="T126" id="Seg_2907" s="T125">Калабок-DIM.[NOM]</ta>
            <ta e="T127" id="Seg_2908" s="T126">Калабок.[NOM]</ta>
            <ta e="T128" id="Seg_2909" s="T127">я.NOM-сердце-ACC</ta>
            <ta e="T129" id="Seg_2910" s="T128">NEG.IMP</ta>
            <ta e="T130" id="Seg_2911" s="T129">воткнуть-RES-IPFV2-OPT-2SG.O</ta>
            <ta e="T131" id="Seg_2912" s="T130">я.NOM</ta>
            <ta e="T132" id="Seg_2913" s="T131">ты.ACC</ta>
            <ta e="T133" id="Seg_2914" s="T132">говно-EP-FUT-1SG.S</ta>
            <ta e="T134" id="Seg_2915" s="T133">ты.ACC</ta>
            <ta e="T135" id="Seg_2916" s="T134">Пёнеге.[NOM]</ta>
            <ta e="T136" id="Seg_2917" s="T135">Калабок-DIM-ACC</ta>
            <ta e="T137" id="Seg_2918" s="T136">испражниться-EP-3SG.O</ta>
            <ta e="T138" id="Seg_2919" s="T137">назад</ta>
            <ta e="T139" id="Seg_2920" s="T138">вниз</ta>
            <ta e="T140" id="Seg_2921" s="T139">Калабок-DIM-ACC</ta>
            <ta e="T141" id="Seg_2922" s="T140">голова-GEN</ta>
            <ta e="T142" id="Seg_2923" s="T141">волосы-EP-PL.[NOM]</ta>
            <ta e="T143" id="Seg_2924" s="T142">Пёнеге.[NOM]</ta>
            <ta e="T144" id="Seg_2925" s="T143">плевать-INCH-PST.NAR-3SG.O</ta>
            <ta e="T145" id="Seg_2926" s="T144">голова-волосы-CAR-ADVZ</ta>
            <ta e="T146" id="Seg_2927" s="T145">остаться-PST.NAR.[3SG.S]</ta>
            <ta e="T147" id="Seg_2928" s="T146">Пёнеге.[NOM]</ta>
            <ta e="T148" id="Seg_2929" s="T147">наружу</ta>
            <ta e="T149" id="Seg_2930" s="T148">выходить.[3SG.S]</ta>
            <ta e="T150" id="Seg_2931" s="T149">и</ta>
            <ta e="T151" id="Seg_2932" s="T150">пойти-3SG.S</ta>
            <ta e="T152" id="Seg_2933" s="T151">Калабок-DIM.[NOM]</ta>
            <ta e="T153" id="Seg_2934" s="T152">болеть-IPFV3-CVB</ta>
            <ta e="T154" id="Seg_2935" s="T153">остаться.[3SG.S]</ta>
            <ta e="T155" id="Seg_2936" s="T154">и</ta>
            <ta e="T156" id="Seg_2937" s="T155">голова-волосы-CAR-ADVZ</ta>
            <ta e="T157" id="Seg_2938" s="T156">вечер-PROL</ta>
            <ta e="T158" id="Seg_2939" s="T157">прийти-CO-3DU.S</ta>
            <ta e="T159" id="Seg_2940" s="T158">отец.[NOM]-3SG</ta>
            <ta e="T160" id="Seg_2941" s="T159">и</ta>
            <ta e="T161" id="Seg_2942" s="T160">мать.[NOM]-3SG</ta>
            <ta e="T162" id="Seg_2943" s="T161">а</ta>
            <ta e="T163" id="Seg_2944" s="T162">Калабок-DIM.[NOM]</ta>
            <ta e="T164" id="Seg_2945" s="T163">болеть-IPFV3-CVB</ta>
            <ta e="T165" id="Seg_2946" s="T164">лежать.[3SG.S]</ta>
            <ta e="T166" id="Seg_2947" s="T165">и</ta>
            <ta e="T167" id="Seg_2948" s="T166">голова-волосы-CAR-ADVZ</ta>
            <ta e="T168" id="Seg_2949" s="T167">отец.[NOM]-3SG</ta>
            <ta e="T169" id="Seg_2950" s="T168">мать.[NOM]-3SG</ta>
            <ta e="T170" id="Seg_2951" s="T169">спрашивать-IPFV2-3DU.S</ta>
            <ta e="T171" id="Seg_2952" s="T170">Калабок-DIM-ALL</ta>
            <ta e="T172" id="Seg_2953" s="T171">голова-волосы-EP-ACC-3SG</ta>
            <ta e="T173" id="Seg_2954" s="T172">куда</ta>
            <ta e="T174" id="Seg_2955" s="T173">почесать-DUR-PST.NAR-2SG.O</ta>
            <ta e="T175" id="Seg_2956" s="T174">и</ta>
            <ta e="T176" id="Seg_2957" s="T175">болеть-IPFV3-2SG.S</ta>
            <ta e="T177" id="Seg_2958" s="T176">а</ta>
            <ta e="T178" id="Seg_2959" s="T177">Калабок-DIM.[NOM]</ta>
            <ta e="T179" id="Seg_2960" s="T178">рассказывать-IPFV2-3SG.O</ta>
            <ta e="T180" id="Seg_2961" s="T179">я.ACC</ta>
            <ta e="T181" id="Seg_2962" s="T180">дядя.[NOM]</ta>
            <ta e="T182" id="Seg_2963" s="T181">дядя.[NOM]</ta>
            <ta e="T183" id="Seg_2964" s="T182">черт-1SG</ta>
            <ta e="T184" id="Seg_2965" s="T183">глотать-IPFV3-FRQ-PST.[3SG.S]</ta>
            <ta e="T185" id="Seg_2966" s="T184">и</ta>
            <ta e="T186" id="Seg_2967" s="T185">я.ACC</ta>
            <ta e="T187" id="Seg_2968" s="T186">такой</ta>
            <ta e="T188" id="Seg_2969" s="T187">такой</ta>
            <ta e="T189" id="Seg_2970" s="T188">черт-EP-1SG</ta>
            <ta e="T190" id="Seg_2971" s="T189">глотать-IPFV3-FRQ-PST.[3SG.S]</ta>
            <ta e="T191" id="Seg_2972" s="T190">следующий</ta>
            <ta e="T192" id="Seg_2973" s="T191">солнце.[NOM]</ta>
            <ta e="T193" id="Seg_2974" s="T192">отец.[NOM]-3SG</ta>
            <ta e="T194" id="Seg_2975" s="T193">мать.[NOM]-3SG</ta>
            <ta e="T195" id="Seg_2976" s="T194">куда-EMPH</ta>
            <ta e="T196" id="Seg_2977" s="T195">NEG</ta>
            <ta e="T197" id="Seg_2978" s="T196">пойти-CO-3DU.S</ta>
            <ta e="T198" id="Seg_2979" s="T197">пойти-CO-3DU.S</ta>
            <ta e="T199" id="Seg_2980" s="T198">дома</ta>
            <ta e="T200" id="Seg_2981" s="T199">день-TR-INF</ta>
            <ta e="T201" id="Seg_2982" s="T200">день-TR-INF</ta>
            <ta e="T202" id="Seg_2983" s="T201">остаться-EP-3DU.S</ta>
            <ta e="T203" id="Seg_2984" s="T202">отец.[NOM]-3SG</ta>
            <ta e="T204" id="Seg_2985" s="T203">и</ta>
            <ta e="T205" id="Seg_2986" s="T204">мать.[NOM]-3SG</ta>
            <ta e="T206" id="Seg_2987" s="T205">корыто.[NOM]</ta>
            <ta e="T207" id="Seg_2988" s="T206">полный</ta>
            <ta e="T208" id="Seg_2989" s="T207">рыба-EP-GEN</ta>
            <ta e="T209" id="Seg_2990" s="T208">слизь-ACC</ta>
            <ta e="T210" id="Seg_2991" s="T209">рот-EP-GEN</ta>
            <ta e="T211" id="Seg_2992" s="T210">край-ADV.LOC</ta>
            <ta e="T212" id="Seg_2993" s="T211">край-ADV.LOC</ta>
            <ta e="T213" id="Seg_2994" s="T212">лить-3DU.S</ta>
            <ta e="T214" id="Seg_2995" s="T213">а</ta>
            <ta e="T215" id="Seg_2996" s="T214">сам.3SG</ta>
            <ta e="T216" id="Seg_2997" s="T215">дверь-GEN</ta>
            <ta e="T217" id="Seg_2998" s="T216">%%-ADJZ</ta>
            <ta e="T218" id="Seg_2999" s="T217">сторона-EP-ILL</ta>
            <ta e="T219" id="Seg_3000" s="T218">стоять-RES-DRV-3DU.S</ta>
            <ta e="T220" id="Seg_3001" s="T219">отец.[NOM]-3SG</ta>
            <ta e="T221" id="Seg_3002" s="T220">топор.[NOM]</ta>
            <ta e="T222" id="Seg_3003" s="T221">взять-IPFV2-3SG.O</ta>
            <ta e="T223" id="Seg_3004" s="T222">топор-INSTR</ta>
            <ta e="T224" id="Seg_3005" s="T223">стоять-CO.[3SG.S]</ta>
            <ta e="T225" id="Seg_3006" s="T224">топор-INSTR</ta>
            <ta e="T226" id="Seg_3007" s="T225">стоять-RES-DRV.[3SG.S]</ta>
            <ta e="T227" id="Seg_3008" s="T226">а</ta>
            <ta e="T228" id="Seg_3009" s="T227">старуха.[NOM]-3SG</ta>
            <ta e="T229" id="Seg_3010" s="T228">мать.[NOM]-3SG</ta>
            <ta e="T230" id="Seg_3011" s="T229">ступа-ADJZ</ta>
            <ta e="T231" id="Seg_3012" s="T230">камень.[NOM]</ta>
            <ta e="T232" id="Seg_3013" s="T231">взять-INFER-3SG.O</ta>
            <ta e="T233" id="Seg_3014" s="T232">а</ta>
            <ta e="T234" id="Seg_3015" s="T233">Калабок-DIM-ALL</ta>
            <ta e="T235" id="Seg_3016" s="T234">сказать-EP-3DU.S</ta>
            <ta e="T236" id="Seg_3017" s="T235">а</ta>
            <ta e="T237" id="Seg_3018" s="T236">ты.NOM</ta>
            <ta e="T238" id="Seg_3019" s="T237">Калабок-DIM.[NOM]</ta>
            <ta e="T239" id="Seg_3020" s="T238">дом-EP-ACC</ta>
            <ta e="T240" id="Seg_3021" s="T239">баловаться-TR-DUR-OPT-2SG.O</ta>
            <ta e="T241" id="Seg_3022" s="T240">Калабок-DIM.[NOM]</ta>
            <ta e="T242" id="Seg_3023" s="T241">петь-FRQ-CO.[3SG.S]</ta>
            <ta e="T243" id="Seg_3024" s="T242">дерево-OBL.3SG-INSTR</ta>
            <ta e="T244" id="Seg_3025" s="T243">бить-RES-IPFV2-3SG.O</ta>
            <ta e="T245" id="Seg_3026" s="T244">что-ACC-INDEF</ta>
            <ta e="T246" id="Seg_3027" s="T245">слышать-3SG.O</ta>
            <ta e="T247" id="Seg_3028" s="T246">слышать-3DU.S</ta>
            <ta e="T248" id="Seg_3029" s="T247">слышать-3PL</ta>
            <ta e="T249" id="Seg_3030" s="T248">Пёнеге.[NOM]</ta>
            <ta e="T250" id="Seg_3031" s="T249">идти.[3SG.S]</ta>
            <ta e="T251" id="Seg_3032" s="T250">дом.[NOM]</ta>
            <ta e="T252" id="Seg_3033" s="T251">войти-EP-IPFV2.[3SG.S]</ta>
            <ta e="T253" id="Seg_3034" s="T252">Пёнеге.[NOM]</ta>
            <ta e="T254" id="Seg_3035" s="T253">дверь-GEN</ta>
            <ta e="T255" id="Seg_3036" s="T254">край.[NOM]</ta>
            <ta e="T256" id="Seg_3037" s="T255">шагнуть-CO-3SG.O</ta>
            <ta e="T257" id="Seg_3038" s="T256">рыба-EP-GEN</ta>
            <ta e="T258" id="Seg_3039" s="T257">слизь-ILL</ta>
            <ta e="T259" id="Seg_3040" s="T258">зашагать-PFV.[3SG.S]</ta>
            <ta e="T260" id="Seg_3041" s="T259">и</ta>
            <ta e="T261" id="Seg_3042" s="T260">скользнуть-INCH-DETR-CVB</ta>
            <ta e="T262" id="Seg_3043" s="T261">вниз</ta>
            <ta e="T263" id="Seg_3044" s="T262">упасть.[3SG.S]</ta>
            <ta e="T264" id="Seg_3045" s="T263">и</ta>
            <ta e="T265" id="Seg_3046" s="T264">этот-%%</ta>
            <ta e="T266" id="Seg_3047" s="T265">утро-ADV.LOC</ta>
            <ta e="T267" id="Seg_3048" s="T266">отец.[NOM]-3SG</ta>
            <ta e="T268" id="Seg_3049" s="T267">и</ta>
            <ta e="T269" id="Seg_3050" s="T268">мать.[NOM]-3SG</ta>
            <ta e="T270" id="Seg_3051" s="T269">отец.[NOM]-3SG</ta>
            <ta e="T271" id="Seg_3052" s="T270">топор-INSTR</ta>
            <ta e="T272" id="Seg_3053" s="T271">Пёнеге-GEN</ta>
            <ta e="T273" id="Seg_3054" s="T272">голова-ACC</ta>
            <ta e="T274" id="Seg_3055" s="T273">рубить-CO-3SG.O</ta>
            <ta e="T275" id="Seg_3056" s="T274">Пёнеге-ACC</ta>
            <ta e="T276" id="Seg_3057" s="T275">пойти-CO-3DU.S</ta>
            <ta e="T277" id="Seg_3058" s="T276">наружу</ta>
            <ta e="T278" id="Seg_3059" s="T277">большой</ta>
            <ta e="T279" id="Seg_3060" s="T278">огонь-AUGM2-EP-ACC</ta>
            <ta e="T280" id="Seg_3061" s="T279">положить-CO-3DU.S</ta>
            <ta e="T281" id="Seg_3062" s="T280">Пёнеге-ACC</ta>
            <ta e="T282" id="Seg_3063" s="T281">этот</ta>
            <ta e="T283" id="Seg_3064" s="T282">большой</ta>
            <ta e="T284" id="Seg_3065" s="T283">огонь-AUGM2-ALL</ta>
            <ta e="T285" id="Seg_3066" s="T284">бросить-3DU.S</ta>
            <ta e="T286" id="Seg_3067" s="T285">Пёнеге-ACC</ta>
            <ta e="T287" id="Seg_3068" s="T286">огонь</ta>
            <ta e="T288" id="Seg_3069" s="T287">есть-EP-IPFV2-3SG.O</ta>
            <ta e="T289" id="Seg_3070" s="T288">Пёнеге-GEN</ta>
            <ta e="T290" id="Seg_3071" s="T289">зуб-PL.[NOM]-3SG</ta>
            <ta e="T291" id="Seg_3072" s="T290">идти-PFV-DETR-CVB2</ta>
            <ta e="T292" id="Seg_3073" s="T291">огонь-EL.3SG-%%</ta>
            <ta e="T293" id="Seg_3074" s="T292">прочь</ta>
            <ta e="T294" id="Seg_3075" s="T293">вниз</ta>
            <ta e="T295" id="Seg_3076" s="T294">прыгнуть-FRQ-CO-3PL</ta>
            <ta e="T296" id="Seg_3077" s="T295">Пёнеге-ACC</ta>
            <ta e="T297" id="Seg_3078" s="T296">огонь.[NOM]</ta>
            <ta e="T298" id="Seg_3079" s="T297">есть-CO-3SG.O</ta>
            <ta e="T299" id="Seg_3080" s="T298">и</ta>
            <ta e="T300" id="Seg_3081" s="T299">теперь-EMPH2</ta>
            <ta e="T301" id="Seg_3082" s="T300">жить-3PL</ta>
            <ta e="T302" id="Seg_3083" s="T301">и</ta>
            <ta e="T303" id="Seg_3084" s="T302">жить-3PL</ta>
            <ta e="T304" id="Seg_3085" s="T303">и</ta>
            <ta e="T305" id="Seg_3086" s="T304">Калабок-DIM.[NOM]</ta>
            <ta e="T306" id="Seg_3087" s="T305">тоже</ta>
            <ta e="T307" id="Seg_3088" s="T306">сила-EP-TRL-PST.NAR.[3SG.S]</ta>
            <ta e="T308" id="Seg_3089" s="T307">и</ta>
            <ta e="T309" id="Seg_3090" s="T308">Пёнеге-PL.[NOM]</ta>
            <ta e="T310" id="Seg_3091" s="T309">тоже</ta>
            <ta e="T311" id="Seg_3092" s="T310">NEG.EX-CO-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_3093" s="T1">v-v:pn-v:ins-v:pn</ta>
            <ta e="T3" id="Seg_3094" s="T2">v-v:pn-v:ins-v:pn</ta>
            <ta e="T4" id="Seg_3095" s="T3">n.[n:case]</ta>
            <ta e="T5" id="Seg_3096" s="T4">n-n&gt;n-n:num</ta>
            <ta e="T6" id="Seg_3097" s="T5">pers-n:ins-n:num-n:case-n:case</ta>
            <ta e="T7" id="Seg_3098" s="T6">v-v&gt;v-v:ins.[v:pn]</ta>
            <ta e="T8" id="Seg_3099" s="T7">n.[n:case]-n:poss</ta>
            <ta e="T9" id="Seg_3100" s="T8">n.[n:case]-n:poss</ta>
            <ta e="T10" id="Seg_3101" s="T9">nprop-n&gt;n.[n:case]</ta>
            <ta e="T11" id="Seg_3102" s="T10">n-n:poss</ta>
            <ta e="T12" id="Seg_3103" s="T11">v.[v:pn]</ta>
            <ta e="T13" id="Seg_3104" s="T12">n-n-n:poss</ta>
            <ta e="T14" id="Seg_3105" s="T13">v-v:ins-v:pn</ta>
            <ta e="T15" id="Seg_3106" s="T14">n-n&gt;v-n:ins-v&gt;v-v:inf</ta>
            <ta e="T16" id="Seg_3107" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_3108" s="T16">conj</ta>
            <ta e="T18" id="Seg_3109" s="T17">nprop-n&gt;n-n:case</ta>
            <ta e="T19" id="Seg_3110" s="T18">v-v:pn</ta>
            <ta e="T20" id="Seg_3111" s="T19">pers</ta>
            <ta e="T21" id="Seg_3112" s="T20">n-n:case</ta>
            <ta e="T22" id="Seg_3113" s="T21">adv-adj&gt;adv</ta>
            <ta e="T23" id="Seg_3114" s="T22">v-v:mood.pn</ta>
            <ta e="T24" id="Seg_3115" s="T23">n-n:ins-n:case</ta>
            <ta e="T25" id="Seg_3116" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_3117" s="T25">v-v&gt;v-v&gt;v-v:mood.pn</ta>
            <ta e="T27" id="Seg_3118" s="T26">v-v&gt;v-v:mood-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_3119" s="T27">nprop.[n:case]</ta>
            <ta e="T29" id="Seg_3120" s="T28">v-v:tense.[v:pn]</ta>
            <ta e="T30" id="Seg_3121" s="T29">conj</ta>
            <ta e="T31" id="Seg_3122" s="T30">v-v:tense.[v:pn]</ta>
            <ta e="T32" id="Seg_3123" s="T31">pers</ta>
            <ta e="T33" id="Seg_3124" s="T32">n-n:poss</ta>
            <ta e="T34" id="Seg_3125" s="T33">conj</ta>
            <ta e="T35" id="Seg_3126" s="T34">n.[n:case]-n:poss</ta>
            <ta e="T36" id="Seg_3127" s="T35">v-v:ins-v:pn</ta>
            <ta e="T37" id="Seg_3128" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_3129" s="T37">n-n&gt;v-n:ins-v&gt;v-v:inf</ta>
            <ta e="T39" id="Seg_3130" s="T38">nprop-n&gt;n.[n:case]</ta>
            <ta e="T40" id="Seg_3131" s="T39">v.[v:pn]</ta>
            <ta e="T41" id="Seg_3132" s="T40">n-n&gt;adj-adj&gt;adv</ta>
            <ta e="T42" id="Seg_3133" s="T41">n.[n:case]</ta>
            <ta e="T43" id="Seg_3134" s="T42">n.[n:case]</ta>
            <ta e="T44" id="Seg_3135" s="T43">conj</ta>
            <ta e="T45" id="Seg_3136" s="T44">n.[n:case]</ta>
            <ta e="T46" id="Seg_3137" s="T45">interrog</ta>
            <ta e="T47" id="Seg_3138" s="T46">v-v:pn</ta>
            <ta e="T48" id="Seg_3139" s="T47">nprop.[n:case]</ta>
            <ta e="T49" id="Seg_3140" s="T48">v-v:tense.[v:pn]</ta>
            <ta e="T50" id="Seg_3141" s="T49">pers</ta>
            <ta e="T51" id="Seg_3142" s="T50">v-v:inf</ta>
            <ta e="T52" id="Seg_3143" s="T51">conj</ta>
            <ta e="T53" id="Seg_3144" s="T52">pers</ta>
            <ta e="T54" id="Seg_3145" s="T53">adj</ta>
            <ta e="T55" id="Seg_3146" s="T54">n-n:case</ta>
            <ta e="T56" id="Seg_3147" s="T55">adv</ta>
            <ta e="T57" id="Seg_3148" s="T56">nprop-n:case</ta>
            <ta e="T58" id="Seg_3149" s="T57">v-v:tense-v:pn</ta>
            <ta e="T59" id="Seg_3150" s="T58">adj</ta>
            <ta e="T60" id="Seg_3151" s="T59">n-n:case-n:poss</ta>
            <ta e="T61" id="Seg_3152" s="T60">v-v&gt;v-v:pn</ta>
            <ta e="T62" id="Seg_3153" s="T61">conj</ta>
            <ta e="T63" id="Seg_3154" s="T62">emphpro</ta>
            <ta e="T64" id="Seg_3155" s="T63">v-v&gt;v-n:ins.[v:pn]</ta>
            <ta e="T65" id="Seg_3156" s="T64">n-n:ins-n:case-n:poss</ta>
            <ta e="T66" id="Seg_3157" s="T65">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T67" id="Seg_3158" s="T66">adv</ta>
            <ta e="T68" id="Seg_3159" s="T67">v-v&gt;adv</ta>
            <ta e="T69" id="Seg_3160" s="T68">v-v:pn</ta>
            <ta e="T70" id="Seg_3161" s="T69">interrog-clit</ta>
            <ta e="T71" id="Seg_3162" s="T70">v.[v:pn]</ta>
            <ta e="T72" id="Seg_3163" s="T71">n-n:num-n:case.poss-n:case</ta>
            <ta e="T73" id="Seg_3164" s="T72">v-v&gt;v-v&gt;adv</ta>
            <ta e="T74" id="Seg_3165" s="T73">nprop-n&gt;n.[n:case]</ta>
            <ta e="T75" id="Seg_3166" s="T74">adj</ta>
            <ta e="T76" id="Seg_3167" s="T75">n-n:case.poss-n:case</ta>
            <ta e="T77" id="Seg_3168" s="T76">n-n:case</ta>
            <ta e="T78" id="Seg_3169" s="T77">n-n:case</ta>
            <ta e="T79" id="Seg_3170" s="T78">v-v&gt;v.[v:pn]</ta>
            <ta e="T80" id="Seg_3171" s="T79">nprop.[n:case]</ta>
            <ta e="T81" id="Seg_3172" s="T80">n.[n:case]</ta>
            <ta e="T82" id="Seg_3173" s="T81">v-v:ins.[v:pn]</ta>
            <ta e="T83" id="Seg_3174" s="T82">nprop-n&gt;n-n:case</ta>
            <ta e="T84" id="Seg_3175" s="T83">n-n:case</ta>
            <ta e="T85" id="Seg_3176" s="T84">adv-n:case.poss</ta>
            <ta e="T86" id="Seg_3177" s="T85">v-v:pn</ta>
            <ta e="T87" id="Seg_3178" s="T86">emphpro</ta>
            <ta e="T88" id="Seg_3179" s="T87">n-n:case.poss</ta>
            <ta e="T89" id="Seg_3180" s="T88">conj</ta>
            <ta e="T90" id="Seg_3181" s="T89">nprop-n&gt;n-n:case</ta>
            <ta e="T91" id="Seg_3182" s="T90">v-v&gt;v-v:pn</ta>
            <ta e="T92" id="Seg_3183" s="T91">nprop-n&gt;n-n:case-n:case</ta>
            <ta e="T93" id="Seg_3184" s="T92">v-v:ins.[v:pn]</ta>
            <ta e="T94" id="Seg_3185" s="T93">adj</ta>
            <ta e="T95" id="Seg_3186" s="T94">n.[n:case]</ta>
            <ta e="T96" id="Seg_3187" s="T95">v.[v:pn]</ta>
            <ta e="T97" id="Seg_3188" s="T96">nprop-n:case</ta>
            <ta e="T98" id="Seg_3189" s="T97">n-n:case</ta>
            <ta e="T99" id="Seg_3190" s="T98">adj</ta>
            <ta e="T100" id="Seg_3191" s="T99">n-n:case.poss-n:case</ta>
            <ta e="T101" id="Seg_3192" s="T100">n-n:case.poss</ta>
            <ta e="T102" id="Seg_3193" s="T101">adv</ta>
            <ta e="T103" id="Seg_3194" s="T102">adv</ta>
            <ta e="T104" id="Seg_3195" s="T103">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T105" id="Seg_3196" s="T104">nprop.[n:case]</ta>
            <ta e="T106" id="Seg_3197" s="T105">v.[v:pn]</ta>
            <ta e="T107" id="Seg_3198" s="T106">nprop-n&gt;n.[n:case]</ta>
            <ta e="T108" id="Seg_3199" s="T107">nprop-n&gt;n.[n:case]</ta>
            <ta e="T109" id="Seg_3200" s="T108">pers-n:case-n&gt;adj</ta>
            <ta e="T110" id="Seg_3201" s="T109">n-n:case</ta>
            <ta e="T111" id="Seg_3202" s="T110">ptcl</ta>
            <ta e="T112" id="Seg_3203" s="T111">v-v&gt;v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T113" id="Seg_3204" s="T112">conj</ta>
            <ta e="T114" id="Seg_3205" s="T113">nprop-n&gt;n.[n:case]</ta>
            <ta e="T115" id="Seg_3206" s="T114">n-n:case</ta>
            <ta e="T116" id="Seg_3207" s="T115">dem</ta>
            <ta e="T117" id="Seg_3208" s="T116">adj</ta>
            <ta e="T118" id="Seg_3209" s="T117">n-n&gt;n-n:case.poss-n:case</ta>
            <ta e="T119" id="Seg_3210" s="T118">nprop-n:case</ta>
            <ta e="T120" id="Seg_3211" s="T119">n-n:case</ta>
            <ta e="T121" id="Seg_3212" s="T120">v-v&gt;v-v&gt;adv</ta>
            <ta e="T122" id="Seg_3213" s="T121">v-v:pn</ta>
            <ta e="T123" id="Seg_3214" s="T122">conj</ta>
            <ta e="T124" id="Seg_3215" s="T123">nprop.[n:case]</ta>
            <ta e="T125" id="Seg_3216" s="T124">v.[v:pn]</ta>
            <ta e="T126" id="Seg_3217" s="T125">nprop-n&gt;n.[n:case]</ta>
            <ta e="T127" id="Seg_3218" s="T126">nprop.[n:case]</ta>
            <ta e="T128" id="Seg_3219" s="T127">pers-n-n:case</ta>
            <ta e="T129" id="Seg_3220" s="T128">ptcl</ta>
            <ta e="T130" id="Seg_3221" s="T129">v-v&gt;v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T131" id="Seg_3222" s="T130">pers</ta>
            <ta e="T132" id="Seg_3223" s="T131">pers</ta>
            <ta e="T133" id="Seg_3224" s="T132">n-n:ins-v:tense-v:pn</ta>
            <ta e="T134" id="Seg_3225" s="T133">pers</ta>
            <ta e="T135" id="Seg_3226" s="T134">nprop.[n:case]</ta>
            <ta e="T136" id="Seg_3227" s="T135">nprop-n&gt;n-n:case</ta>
            <ta e="T137" id="Seg_3228" s="T136">v-n:ins-v:pn</ta>
            <ta e="T138" id="Seg_3229" s="T137">adv</ta>
            <ta e="T139" id="Seg_3230" s="T138">adv</ta>
            <ta e="T140" id="Seg_3231" s="T139">nprop-n&gt;n-n:case</ta>
            <ta e="T141" id="Seg_3232" s="T140">n-n:case</ta>
            <ta e="T142" id="Seg_3233" s="T141">n-n:ins-n:num.[n:case]</ta>
            <ta e="T143" id="Seg_3234" s="T142">nprop.[n:case]</ta>
            <ta e="T144" id="Seg_3235" s="T143">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T145" id="Seg_3236" s="T144">n-n-n&gt;adj-adj&gt;adv</ta>
            <ta e="T146" id="Seg_3237" s="T145">v-v:tense.[v:pn]</ta>
            <ta e="T147" id="Seg_3238" s="T146">nprop.[n:case]</ta>
            <ta e="T148" id="Seg_3239" s="T147">adv</ta>
            <ta e="T149" id="Seg_3240" s="T148">v.[v:pn]</ta>
            <ta e="T150" id="Seg_3241" s="T149">conj</ta>
            <ta e="T151" id="Seg_3242" s="T150">v-v:pn</ta>
            <ta e="T152" id="Seg_3243" s="T151">nprop-n&gt;n.[n:case]</ta>
            <ta e="T153" id="Seg_3244" s="T152">v-v&gt;v-v&gt;adv</ta>
            <ta e="T154" id="Seg_3245" s="T153">v.[v:pn]</ta>
            <ta e="T155" id="Seg_3246" s="T154">conj</ta>
            <ta e="T156" id="Seg_3247" s="T155">n-n-n&gt;adj-adj&gt;adv</ta>
            <ta e="T157" id="Seg_3248" s="T156">n-n:case</ta>
            <ta e="T158" id="Seg_3249" s="T157">v-v:ins-v:pn</ta>
            <ta e="T159" id="Seg_3250" s="T158">n.[n:case]-n:poss</ta>
            <ta e="T160" id="Seg_3251" s="T159">conj</ta>
            <ta e="T161" id="Seg_3252" s="T160">n.[n:case]-n:poss</ta>
            <ta e="T162" id="Seg_3253" s="T161">conj</ta>
            <ta e="T163" id="Seg_3254" s="T162">nprop-n&gt;n.[n:case]</ta>
            <ta e="T164" id="Seg_3255" s="T163">v-v&gt;v-v&gt;adv</ta>
            <ta e="T165" id="Seg_3256" s="T164">v.[v:pn]</ta>
            <ta e="T166" id="Seg_3257" s="T165">conj</ta>
            <ta e="T167" id="Seg_3258" s="T166">n-n-n&gt;adj-adj&gt;adv</ta>
            <ta e="T168" id="Seg_3259" s="T167">n.[n:case]-n:poss</ta>
            <ta e="T169" id="Seg_3260" s="T168">n.[n:case]-n:poss</ta>
            <ta e="T170" id="Seg_3261" s="T169">v-v&gt;v-v:pn</ta>
            <ta e="T171" id="Seg_3262" s="T170">nprop-n&gt;n-n:case</ta>
            <ta e="T172" id="Seg_3263" s="T171">n-n-n:ins-n:case-n:poss</ta>
            <ta e="T173" id="Seg_3264" s="T172">interrog</ta>
            <ta e="T174" id="Seg_3265" s="T173">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T175" id="Seg_3266" s="T174">conj</ta>
            <ta e="T176" id="Seg_3267" s="T175">v-v&gt;v-v:pn</ta>
            <ta e="T177" id="Seg_3268" s="T176">conj</ta>
            <ta e="T178" id="Seg_3269" s="T177">nprop-n&gt;n.[n:case]</ta>
            <ta e="T179" id="Seg_3270" s="T178">v-v&gt;v-v:pn</ta>
            <ta e="T180" id="Seg_3271" s="T179">pers</ta>
            <ta e="T181" id="Seg_3272" s="T180">n.[n:case]</ta>
            <ta e="T182" id="Seg_3273" s="T181">n.[n:case]</ta>
            <ta e="T183" id="Seg_3274" s="T182">n-n:case.poss</ta>
            <ta e="T184" id="Seg_3275" s="T183">v-v&gt;v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T185" id="Seg_3276" s="T184">conj</ta>
            <ta e="T186" id="Seg_3277" s="T185">pers</ta>
            <ta e="T187" id="Seg_3278" s="T186">adj</ta>
            <ta e="T188" id="Seg_3279" s="T187">adj</ta>
            <ta e="T189" id="Seg_3280" s="T188">n-n:ins-n:case.poss</ta>
            <ta e="T190" id="Seg_3281" s="T189">v-v&gt;v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T191" id="Seg_3282" s="T190">adj</ta>
            <ta e="T192" id="Seg_3283" s="T191">n.[n:case]</ta>
            <ta e="T193" id="Seg_3284" s="T192">n.[n:case]-n:poss</ta>
            <ta e="T194" id="Seg_3285" s="T193">n.[n:case]-n:poss</ta>
            <ta e="T195" id="Seg_3286" s="T194">interrog-clit</ta>
            <ta e="T196" id="Seg_3287" s="T195">ptcl</ta>
            <ta e="T197" id="Seg_3288" s="T196">v-v:ins-v:pn</ta>
            <ta e="T198" id="Seg_3289" s="T197">v-v:ins-v:pn</ta>
            <ta e="T199" id="Seg_3290" s="T198">adv</ta>
            <ta e="T200" id="Seg_3291" s="T199">n-n&gt;v-v:inf</ta>
            <ta e="T201" id="Seg_3292" s="T200">n-n&gt;v-v:inf</ta>
            <ta e="T202" id="Seg_3293" s="T201">v-n:ins-v:pn</ta>
            <ta e="T203" id="Seg_3294" s="T202">n.[n:case]-n:poss</ta>
            <ta e="T204" id="Seg_3295" s="T203">conj</ta>
            <ta e="T205" id="Seg_3296" s="T204">n.[n:case]-n:poss</ta>
            <ta e="T206" id="Seg_3297" s="T205">n.[n:case]</ta>
            <ta e="T207" id="Seg_3298" s="T206">adj</ta>
            <ta e="T208" id="Seg_3299" s="T207">n-n:ins-n:case</ta>
            <ta e="T209" id="Seg_3300" s="T208">n-n:case</ta>
            <ta e="T210" id="Seg_3301" s="T209">n-n:ins-n:case</ta>
            <ta e="T211" id="Seg_3302" s="T210">n-adv:case</ta>
            <ta e="T212" id="Seg_3303" s="T211">n-adv:case</ta>
            <ta e="T213" id="Seg_3304" s="T212">v-v:pn</ta>
            <ta e="T214" id="Seg_3305" s="T213">conj</ta>
            <ta e="T215" id="Seg_3306" s="T214">emphpro</ta>
            <ta e="T216" id="Seg_3307" s="T215">n-n:case</ta>
            <ta e="T217" id="Seg_3308" s="T216">n-n&gt;adj</ta>
            <ta e="T218" id="Seg_3309" s="T217">n-n:ins-n:case</ta>
            <ta e="T219" id="Seg_3310" s="T218">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T220" id="Seg_3311" s="T219">n.[n:case]-n:poss</ta>
            <ta e="T221" id="Seg_3312" s="T220">n.[n:case]</ta>
            <ta e="T222" id="Seg_3313" s="T221">v-v&gt;v-v:pn</ta>
            <ta e="T223" id="Seg_3314" s="T222">n-n:case</ta>
            <ta e="T224" id="Seg_3315" s="T223">v-v:ins.[v:pn]</ta>
            <ta e="T225" id="Seg_3316" s="T224">n-n:case</ta>
            <ta e="T226" id="Seg_3317" s="T225">v-v&gt;v-v&gt;v.[v:pn]</ta>
            <ta e="T227" id="Seg_3318" s="T226">conj</ta>
            <ta e="T228" id="Seg_3319" s="T227">n.[n:case]-n:poss</ta>
            <ta e="T229" id="Seg_3320" s="T228">n.[n:case]-n:poss</ta>
            <ta e="T230" id="Seg_3321" s="T229">n-n&gt;adj</ta>
            <ta e="T231" id="Seg_3322" s="T230">n.[n:case]</ta>
            <ta e="T232" id="Seg_3323" s="T231">v-v:mood-v:pn</ta>
            <ta e="T233" id="Seg_3324" s="T232">conj</ta>
            <ta e="T234" id="Seg_3325" s="T233">nprop-n&gt;n-n:case</ta>
            <ta e="T235" id="Seg_3326" s="T234">v-n:ins-v:pn</ta>
            <ta e="T236" id="Seg_3327" s="T235">conj</ta>
            <ta e="T237" id="Seg_3328" s="T236">pers</ta>
            <ta e="T238" id="Seg_3329" s="T237">nprop-n&gt;n.[n:case]</ta>
            <ta e="T239" id="Seg_3330" s="T238">n-n:ins-n:case</ta>
            <ta e="T240" id="Seg_3331" s="T239">v-v&gt;v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T241" id="Seg_3332" s="T240">nprop-n&gt;n.[n:case]</ta>
            <ta e="T242" id="Seg_3333" s="T241">v-v&gt;v-v:ins.[v:pn]</ta>
            <ta e="T243" id="Seg_3334" s="T242">n-n:case.poss-n:case</ta>
            <ta e="T244" id="Seg_3335" s="T243">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T245" id="Seg_3336" s="T244">interrog-n:case-clit</ta>
            <ta e="T246" id="Seg_3337" s="T245">v-v:pn</ta>
            <ta e="T247" id="Seg_3338" s="T246">v-v:pn</ta>
            <ta e="T248" id="Seg_3339" s="T247">v-v:pn</ta>
            <ta e="T249" id="Seg_3340" s="T248">nprop.[n:case]</ta>
            <ta e="T250" id="Seg_3341" s="T249">v.[v:pn]</ta>
            <ta e="T251" id="Seg_3342" s="T250">n.[n:case]</ta>
            <ta e="T252" id="Seg_3343" s="T251">v-n:ins-v&gt;v.[v:pn]</ta>
            <ta e="T253" id="Seg_3344" s="T252">nprop.[n:case]</ta>
            <ta e="T254" id="Seg_3345" s="T253">n-n:case</ta>
            <ta e="T255" id="Seg_3346" s="T254">n.[n:case]</ta>
            <ta e="T256" id="Seg_3347" s="T255">v-v:ins-v:pn</ta>
            <ta e="T257" id="Seg_3348" s="T256">n-n:ins-n:case</ta>
            <ta e="T258" id="Seg_3349" s="T257">n-n:case</ta>
            <ta e="T259" id="Seg_3350" s="T258">v-v&gt;v.[v:pn]</ta>
            <ta e="T260" id="Seg_3351" s="T259">conj</ta>
            <ta e="T261" id="Seg_3352" s="T260">v-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T262" id="Seg_3353" s="T261">preverb</ta>
            <ta e="T263" id="Seg_3354" s="T262">v.[v:pn]</ta>
            <ta e="T264" id="Seg_3355" s="T263">conj</ta>
            <ta e="T265" id="Seg_3356" s="T264">dem-clit</ta>
            <ta e="T266" id="Seg_3357" s="T265">n-adv:case</ta>
            <ta e="T267" id="Seg_3358" s="T266">n.[n:case]-n:poss</ta>
            <ta e="T268" id="Seg_3359" s="T267">conj</ta>
            <ta e="T269" id="Seg_3360" s="T268">n.[n:case]-n:poss</ta>
            <ta e="T270" id="Seg_3361" s="T269">n.[n:case]-n:poss</ta>
            <ta e="T271" id="Seg_3362" s="T270">n-n:case</ta>
            <ta e="T272" id="Seg_3363" s="T271">nprop-n:case</ta>
            <ta e="T273" id="Seg_3364" s="T272">n-n:case</ta>
            <ta e="T274" id="Seg_3365" s="T273">v-v:ins-v:pn</ta>
            <ta e="T275" id="Seg_3366" s="T274">nprop-n:case</ta>
            <ta e="T276" id="Seg_3367" s="T275">v-v:ins-v:pn</ta>
            <ta e="T277" id="Seg_3368" s="T276">adv</ta>
            <ta e="T278" id="Seg_3369" s="T277">adj</ta>
            <ta e="T279" id="Seg_3370" s="T278">n-n&gt;n-n:ins-n:case</ta>
            <ta e="T280" id="Seg_3371" s="T279">v-v:ins-v:pn</ta>
            <ta e="T281" id="Seg_3372" s="T280">nprop-n:case</ta>
            <ta e="T282" id="Seg_3373" s="T281">dem</ta>
            <ta e="T283" id="Seg_3374" s="T282">adj</ta>
            <ta e="T284" id="Seg_3375" s="T283">n-n&gt;n-n:case</ta>
            <ta e="T285" id="Seg_3376" s="T284">v-v:pn</ta>
            <ta e="T286" id="Seg_3377" s="T285">nprop-n:case</ta>
            <ta e="T287" id="Seg_3378" s="T286">n</ta>
            <ta e="T288" id="Seg_3379" s="T287">v-n:ins-v&gt;v-v:pn</ta>
            <ta e="T289" id="Seg_3380" s="T288">nprop-n:case</ta>
            <ta e="T290" id="Seg_3381" s="T289">n-n:num.[n:case]-n:poss</ta>
            <ta e="T291" id="Seg_3382" s="T290">v-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T292" id="Seg_3383" s="T291">n-n:case.poss</ta>
            <ta e="T293" id="Seg_3384" s="T292">preverb</ta>
            <ta e="T294" id="Seg_3385" s="T293">preverb</ta>
            <ta e="T295" id="Seg_3386" s="T294">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T296" id="Seg_3387" s="T295">nprop-n:case</ta>
            <ta e="T297" id="Seg_3388" s="T296">n.[n:case]</ta>
            <ta e="T298" id="Seg_3389" s="T297">v-v:ins-v:pn</ta>
            <ta e="T299" id="Seg_3390" s="T298">conj</ta>
            <ta e="T300" id="Seg_3391" s="T299">adv-clit</ta>
            <ta e="T301" id="Seg_3392" s="T300">v-v:pn</ta>
            <ta e="T302" id="Seg_3393" s="T301">conj</ta>
            <ta e="T303" id="Seg_3394" s="T302">v-v:pn</ta>
            <ta e="T304" id="Seg_3395" s="T303">conj</ta>
            <ta e="T305" id="Seg_3396" s="T304">nprop-n&gt;n.[n:case]</ta>
            <ta e="T306" id="Seg_3397" s="T305">ptcl</ta>
            <ta e="T307" id="Seg_3398" s="T306">n-n:ins-n&gt;v-v:tense.[v:pn]</ta>
            <ta e="T308" id="Seg_3399" s="T307">conj</ta>
            <ta e="T309" id="Seg_3400" s="T308">nprop-n:num.[n:case]</ta>
            <ta e="T310" id="Seg_3401" s="T309">ptcl</ta>
            <ta e="T311" id="Seg_3402" s="T310">v-v:ins-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_3403" s="T1">v</ta>
            <ta e="T3" id="Seg_3404" s="T2">v</ta>
            <ta e="T4" id="Seg_3405" s="T3">n</ta>
            <ta e="T5" id="Seg_3406" s="T4">n</ta>
            <ta e="T6" id="Seg_3407" s="T5">pers</ta>
            <ta e="T7" id="Seg_3408" s="T6">v</ta>
            <ta e="T8" id="Seg_3409" s="T7">n</ta>
            <ta e="T9" id="Seg_3410" s="T8">n</ta>
            <ta e="T10" id="Seg_3411" s="T9">nprop</ta>
            <ta e="T11" id="Seg_3412" s="T10">n</ta>
            <ta e="T12" id="Seg_3413" s="T11">v</ta>
            <ta e="T13" id="Seg_3414" s="T12">n</ta>
            <ta e="T14" id="Seg_3415" s="T13">v</ta>
            <ta e="T15" id="Seg_3416" s="T14">n</ta>
            <ta e="T16" id="Seg_3417" s="T15">n</ta>
            <ta e="T17" id="Seg_3418" s="T16">conj</ta>
            <ta e="T18" id="Seg_3419" s="T17">nprop</ta>
            <ta e="T19" id="Seg_3420" s="T18">v</ta>
            <ta e="T20" id="Seg_3421" s="T19">pers</ta>
            <ta e="T21" id="Seg_3422" s="T20">n</ta>
            <ta e="T22" id="Seg_3423" s="T21">adv</ta>
            <ta e="T23" id="Seg_3424" s="T22">adv</ta>
            <ta e="T24" id="Seg_3425" s="T23">n</ta>
            <ta e="T25" id="Seg_3426" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_3427" s="T25">v</ta>
            <ta e="T27" id="Seg_3428" s="T26">v</ta>
            <ta e="T28" id="Seg_3429" s="T27">nprop</ta>
            <ta e="T29" id="Seg_3430" s="T28">v</ta>
            <ta e="T30" id="Seg_3431" s="T29">conj</ta>
            <ta e="T31" id="Seg_3432" s="T30">v</ta>
            <ta e="T32" id="Seg_3433" s="T31">pers</ta>
            <ta e="T33" id="Seg_3434" s="T32">n</ta>
            <ta e="T34" id="Seg_3435" s="T33">conj</ta>
            <ta e="T35" id="Seg_3436" s="T34">n</ta>
            <ta e="T36" id="Seg_3437" s="T35">v</ta>
            <ta e="T37" id="Seg_3438" s="T36">n</ta>
            <ta e="T38" id="Seg_3439" s="T37">n</ta>
            <ta e="T39" id="Seg_3440" s="T38">nprop</ta>
            <ta e="T40" id="Seg_3441" s="T39">v</ta>
            <ta e="T41" id="Seg_3442" s="T40">adj</ta>
            <ta e="T42" id="Seg_3443" s="T41">n</ta>
            <ta e="T43" id="Seg_3444" s="T42">n</ta>
            <ta e="T44" id="Seg_3445" s="T43">conj</ta>
            <ta e="T45" id="Seg_3446" s="T44">n</ta>
            <ta e="T46" id="Seg_3447" s="T45">interrog</ta>
            <ta e="T47" id="Seg_3448" s="T46">v</ta>
            <ta e="T48" id="Seg_3449" s="T47">nprop</ta>
            <ta e="T49" id="Seg_3450" s="T48">v</ta>
            <ta e="T50" id="Seg_3451" s="T49">pers</ta>
            <ta e="T51" id="Seg_3452" s="T50">v</ta>
            <ta e="T52" id="Seg_3453" s="T51">conj</ta>
            <ta e="T53" id="Seg_3454" s="T52">pers</ta>
            <ta e="T54" id="Seg_3455" s="T53">adj</ta>
            <ta e="T55" id="Seg_3456" s="T54">n</ta>
            <ta e="T56" id="Seg_3457" s="T55">pers</ta>
            <ta e="T57" id="Seg_3458" s="T56">nprop</ta>
            <ta e="T58" id="Seg_3459" s="T57">v</ta>
            <ta e="T59" id="Seg_3460" s="T58">adj</ta>
            <ta e="T60" id="Seg_3461" s="T59">n</ta>
            <ta e="T61" id="Seg_3462" s="T60">v</ta>
            <ta e="T62" id="Seg_3463" s="T61">conj</ta>
            <ta e="T63" id="Seg_3464" s="T62">emphpro</ta>
            <ta e="T64" id="Seg_3465" s="T63">v</ta>
            <ta e="T65" id="Seg_3466" s="T64">n</ta>
            <ta e="T66" id="Seg_3467" s="T65">v</ta>
            <ta e="T67" id="Seg_3468" s="T66">adv</ta>
            <ta e="T68" id="Seg_3469" s="T67">adv</ta>
            <ta e="T69" id="Seg_3470" s="T68">v</ta>
            <ta e="T70" id="Seg_3471" s="T69">pro</ta>
            <ta e="T71" id="Seg_3472" s="T70">v</ta>
            <ta e="T72" id="Seg_3473" s="T71">n</ta>
            <ta e="T73" id="Seg_3474" s="T72">v</ta>
            <ta e="T74" id="Seg_3475" s="T73">nprop</ta>
            <ta e="T75" id="Seg_3476" s="T74">adj</ta>
            <ta e="T76" id="Seg_3477" s="T75">n</ta>
            <ta e="T77" id="Seg_3478" s="T76">n</ta>
            <ta e="T78" id="Seg_3479" s="T77">adv</ta>
            <ta e="T79" id="Seg_3480" s="T78">v</ta>
            <ta e="T80" id="Seg_3481" s="T79">nprop</ta>
            <ta e="T81" id="Seg_3482" s="T80">n</ta>
            <ta e="T82" id="Seg_3483" s="T81">v</ta>
            <ta e="T83" id="Seg_3484" s="T82">nprop</ta>
            <ta e="T84" id="Seg_3485" s="T83">n</ta>
            <ta e="T85" id="Seg_3486" s="T84">adv</ta>
            <ta e="T86" id="Seg_3487" s="T85">v</ta>
            <ta e="T87" id="Seg_3488" s="T86">emphpro</ta>
            <ta e="T88" id="Seg_3489" s="T87">n</ta>
            <ta e="T89" id="Seg_3490" s="T88">conj</ta>
            <ta e="T90" id="Seg_3491" s="T89">n</ta>
            <ta e="T91" id="Seg_3492" s="T90">v</ta>
            <ta e="T92" id="Seg_3493" s="T91">nprop</ta>
            <ta e="T93" id="Seg_3494" s="T92">v</ta>
            <ta e="T94" id="Seg_3495" s="T93">adj</ta>
            <ta e="T95" id="Seg_3496" s="T94">n</ta>
            <ta e="T96" id="Seg_3497" s="T95">v</ta>
            <ta e="T97" id="Seg_3498" s="T96">nprop</ta>
            <ta e="T98" id="Seg_3499" s="T97">n</ta>
            <ta e="T99" id="Seg_3500" s="T98">adj</ta>
            <ta e="T100" id="Seg_3501" s="T99">n</ta>
            <ta e="T101" id="Seg_3502" s="T100">n</ta>
            <ta e="T102" id="Seg_3503" s="T101">pers</ta>
            <ta e="T103" id="Seg_3504" s="T102">adv</ta>
            <ta e="T104" id="Seg_3505" s="T103">v</ta>
            <ta e="T105" id="Seg_3506" s="T104">nprop</ta>
            <ta e="T106" id="Seg_3507" s="T105">v</ta>
            <ta e="T107" id="Seg_3508" s="T106">nprop</ta>
            <ta e="T108" id="Seg_3509" s="T107">nprop</ta>
            <ta e="T109" id="Seg_3510" s="T108">adj</ta>
            <ta e="T110" id="Seg_3511" s="T109">n</ta>
            <ta e="T111" id="Seg_3512" s="T110">ptcl</ta>
            <ta e="T112" id="Seg_3513" s="T111">v</ta>
            <ta e="T113" id="Seg_3514" s="T112">conj</ta>
            <ta e="T114" id="Seg_3515" s="T113">nprop</ta>
            <ta e="T115" id="Seg_3516" s="T114">n</ta>
            <ta e="T116" id="Seg_3517" s="T115">dem</ta>
            <ta e="T117" id="Seg_3518" s="T116">adj</ta>
            <ta e="T118" id="Seg_3519" s="T117">n</ta>
            <ta e="T119" id="Seg_3520" s="T118">nprop</ta>
            <ta e="T120" id="Seg_3521" s="T119">n</ta>
            <ta e="T121" id="Seg_3522" s="T120">adv</ta>
            <ta e="T122" id="Seg_3523" s="T121">v</ta>
            <ta e="T123" id="Seg_3524" s="T122">conj</ta>
            <ta e="T124" id="Seg_3525" s="T123">nprop</ta>
            <ta e="T125" id="Seg_3526" s="T124">v</ta>
            <ta e="T126" id="Seg_3527" s="T125">nprop</ta>
            <ta e="T127" id="Seg_3528" s="T126">nprop</ta>
            <ta e="T128" id="Seg_3529" s="T127">pers</ta>
            <ta e="T129" id="Seg_3530" s="T128">ptcl</ta>
            <ta e="T130" id="Seg_3531" s="T129">v</ta>
            <ta e="T131" id="Seg_3532" s="T130">pers</ta>
            <ta e="T132" id="Seg_3533" s="T131">pers</ta>
            <ta e="T133" id="Seg_3534" s="T132">n</ta>
            <ta e="T134" id="Seg_3535" s="T133">pers</ta>
            <ta e="T135" id="Seg_3536" s="T134">nprop</ta>
            <ta e="T136" id="Seg_3537" s="T135">nprop</ta>
            <ta e="T137" id="Seg_3538" s="T136">v</ta>
            <ta e="T138" id="Seg_3539" s="T137">adv</ta>
            <ta e="T139" id="Seg_3540" s="T138">adv</ta>
            <ta e="T140" id="Seg_3541" s="T139">nprop</ta>
            <ta e="T141" id="Seg_3542" s="T140">n</ta>
            <ta e="T142" id="Seg_3543" s="T141">n</ta>
            <ta e="T143" id="Seg_3544" s="T142">nprop</ta>
            <ta e="T144" id="Seg_3545" s="T143">v</ta>
            <ta e="T145" id="Seg_3546" s="T144">adv</ta>
            <ta e="T146" id="Seg_3547" s="T145">v</ta>
            <ta e="T147" id="Seg_3548" s="T146">nprop</ta>
            <ta e="T148" id="Seg_3549" s="T147">adv</ta>
            <ta e="T149" id="Seg_3550" s="T148">v</ta>
            <ta e="T150" id="Seg_3551" s="T149">conj</ta>
            <ta e="T151" id="Seg_3552" s="T150">v</ta>
            <ta e="T152" id="Seg_3553" s="T151">nprop</ta>
            <ta e="T153" id="Seg_3554" s="T152">adv</ta>
            <ta e="T154" id="Seg_3555" s="T153">v</ta>
            <ta e="T155" id="Seg_3556" s="T154">conj</ta>
            <ta e="T156" id="Seg_3557" s="T155">adv</ta>
            <ta e="T157" id="Seg_3558" s="T156">n</ta>
            <ta e="T158" id="Seg_3559" s="T157">v</ta>
            <ta e="T159" id="Seg_3560" s="T158">n</ta>
            <ta e="T160" id="Seg_3561" s="T159">conj</ta>
            <ta e="T161" id="Seg_3562" s="T160">n</ta>
            <ta e="T162" id="Seg_3563" s="T161">conj</ta>
            <ta e="T163" id="Seg_3564" s="T162">nprop</ta>
            <ta e="T164" id="Seg_3565" s="T163">adv</ta>
            <ta e="T165" id="Seg_3566" s="T164">n</ta>
            <ta e="T166" id="Seg_3567" s="T165">conj</ta>
            <ta e="T167" id="Seg_3568" s="T166">adv</ta>
            <ta e="T168" id="Seg_3569" s="T167">n</ta>
            <ta e="T169" id="Seg_3570" s="T168">n</ta>
            <ta e="T170" id="Seg_3571" s="T169">v</ta>
            <ta e="T171" id="Seg_3572" s="T170">nprop</ta>
            <ta e="T172" id="Seg_3573" s="T171">n</ta>
            <ta e="T173" id="Seg_3574" s="T172">interrog</ta>
            <ta e="T174" id="Seg_3575" s="T173">v</ta>
            <ta e="T175" id="Seg_3576" s="T174">conj</ta>
            <ta e="T176" id="Seg_3577" s="T175">v</ta>
            <ta e="T177" id="Seg_3578" s="T176">conj</ta>
            <ta e="T178" id="Seg_3579" s="T177">nprop</ta>
            <ta e="T179" id="Seg_3580" s="T178">v</ta>
            <ta e="T180" id="Seg_3581" s="T179">pers</ta>
            <ta e="T181" id="Seg_3582" s="T180">n</ta>
            <ta e="T182" id="Seg_3583" s="T181">n</ta>
            <ta e="T183" id="Seg_3584" s="T182">n</ta>
            <ta e="T184" id="Seg_3585" s="T183">v</ta>
            <ta e="T185" id="Seg_3586" s="T184">conj</ta>
            <ta e="T186" id="Seg_3587" s="T185">pers</ta>
            <ta e="T187" id="Seg_3588" s="T186">adj</ta>
            <ta e="T188" id="Seg_3589" s="T187">adj</ta>
            <ta e="T189" id="Seg_3590" s="T188">n</ta>
            <ta e="T190" id="Seg_3591" s="T189">v</ta>
            <ta e="T191" id="Seg_3592" s="T190">adj</ta>
            <ta e="T192" id="Seg_3593" s="T191">n</ta>
            <ta e="T193" id="Seg_3594" s="T192">n</ta>
            <ta e="T194" id="Seg_3595" s="T193">n</ta>
            <ta e="T195" id="Seg_3596" s="T194">interrog</ta>
            <ta e="T196" id="Seg_3597" s="T195">ptcl</ta>
            <ta e="T197" id="Seg_3598" s="T196">v</ta>
            <ta e="T198" id="Seg_3599" s="T197">v</ta>
            <ta e="T199" id="Seg_3600" s="T198">adv</ta>
            <ta e="T200" id="Seg_3601" s="T199">n</ta>
            <ta e="T201" id="Seg_3602" s="T200">n</ta>
            <ta e="T202" id="Seg_3603" s="T201">v</ta>
            <ta e="T203" id="Seg_3604" s="T202">n</ta>
            <ta e="T204" id="Seg_3605" s="T203">conj</ta>
            <ta e="T205" id="Seg_3606" s="T204">n</ta>
            <ta e="T206" id="Seg_3607" s="T205">n</ta>
            <ta e="T207" id="Seg_3608" s="T206">adj</ta>
            <ta e="T208" id="Seg_3609" s="T207">n</ta>
            <ta e="T209" id="Seg_3610" s="T208">n</ta>
            <ta e="T210" id="Seg_3611" s="T209">n</ta>
            <ta e="T211" id="Seg_3612" s="T210">n</ta>
            <ta e="T212" id="Seg_3613" s="T211">n</ta>
            <ta e="T213" id="Seg_3614" s="T212">v</ta>
            <ta e="T214" id="Seg_3615" s="T213">conj</ta>
            <ta e="T215" id="Seg_3616" s="T214">emphpro</ta>
            <ta e="T216" id="Seg_3617" s="T215">n</ta>
            <ta e="T217" id="Seg_3618" s="T216">adj</ta>
            <ta e="T218" id="Seg_3619" s="T217">n</ta>
            <ta e="T219" id="Seg_3620" s="T218">v</ta>
            <ta e="T220" id="Seg_3621" s="T219">n</ta>
            <ta e="T221" id="Seg_3622" s="T220">n</ta>
            <ta e="T222" id="Seg_3623" s="T221">v</ta>
            <ta e="T223" id="Seg_3624" s="T222">n</ta>
            <ta e="T224" id="Seg_3625" s="T223">v</ta>
            <ta e="T225" id="Seg_3626" s="T224">n</ta>
            <ta e="T226" id="Seg_3627" s="T225">v</ta>
            <ta e="T227" id="Seg_3628" s="T226">conj</ta>
            <ta e="T228" id="Seg_3629" s="T227">n</ta>
            <ta e="T229" id="Seg_3630" s="T228">n</ta>
            <ta e="T230" id="Seg_3631" s="T229">adj</ta>
            <ta e="T231" id="Seg_3632" s="T230">n</ta>
            <ta e="T232" id="Seg_3633" s="T231">v</ta>
            <ta e="T233" id="Seg_3634" s="T232">conj</ta>
            <ta e="T234" id="Seg_3635" s="T233">nprop</ta>
            <ta e="T235" id="Seg_3636" s="T234">v</ta>
            <ta e="T236" id="Seg_3637" s="T235">conj</ta>
            <ta e="T237" id="Seg_3638" s="T236">pers</ta>
            <ta e="T238" id="Seg_3639" s="T237">nprop</ta>
            <ta e="T239" id="Seg_3640" s="T238">n</ta>
            <ta e="T240" id="Seg_3641" s="T239">v</ta>
            <ta e="T241" id="Seg_3642" s="T240">nprop</ta>
            <ta e="T242" id="Seg_3643" s="T241">v</ta>
            <ta e="T243" id="Seg_3644" s="T242">n</ta>
            <ta e="T244" id="Seg_3645" s="T243">v</ta>
            <ta e="T245" id="Seg_3646" s="T244">pro</ta>
            <ta e="T246" id="Seg_3647" s="T245">v</ta>
            <ta e="T247" id="Seg_3648" s="T246">v</ta>
            <ta e="T248" id="Seg_3649" s="T247">v</ta>
            <ta e="T249" id="Seg_3650" s="T248">nprop</ta>
            <ta e="T250" id="Seg_3651" s="T249">v</ta>
            <ta e="T251" id="Seg_3652" s="T250">n</ta>
            <ta e="T252" id="Seg_3653" s="T251">v</ta>
            <ta e="T253" id="Seg_3654" s="T252">nprop</ta>
            <ta e="T254" id="Seg_3655" s="T253">n</ta>
            <ta e="T255" id="Seg_3656" s="T254">n</ta>
            <ta e="T256" id="Seg_3657" s="T255">v</ta>
            <ta e="T257" id="Seg_3658" s="T256">n</ta>
            <ta e="T258" id="Seg_3659" s="T257">n</ta>
            <ta e="T259" id="Seg_3660" s="T258">v</ta>
            <ta e="T260" id="Seg_3661" s="T259">conj</ta>
            <ta e="T261" id="Seg_3662" s="T260">v</ta>
            <ta e="T262" id="Seg_3663" s="T261">preverb</ta>
            <ta e="T263" id="Seg_3664" s="T262">v</ta>
            <ta e="T264" id="Seg_3665" s="T263">conj</ta>
            <ta e="T265" id="Seg_3666" s="T264">dem</ta>
            <ta e="T266" id="Seg_3667" s="T265">n</ta>
            <ta e="T267" id="Seg_3668" s="T266">n</ta>
            <ta e="T268" id="Seg_3669" s="T267">conj</ta>
            <ta e="T269" id="Seg_3670" s="T268">n</ta>
            <ta e="T270" id="Seg_3671" s="T269">n</ta>
            <ta e="T271" id="Seg_3672" s="T270">n</ta>
            <ta e="T272" id="Seg_3673" s="T271">nprop</ta>
            <ta e="T273" id="Seg_3674" s="T272">n</ta>
            <ta e="T274" id="Seg_3675" s="T273">v</ta>
            <ta e="T275" id="Seg_3676" s="T274">nprop</ta>
            <ta e="T276" id="Seg_3677" s="T275">v</ta>
            <ta e="T277" id="Seg_3678" s="T276">adv</ta>
            <ta e="T278" id="Seg_3679" s="T277">adj</ta>
            <ta e="T279" id="Seg_3680" s="T278">n</ta>
            <ta e="T280" id="Seg_3681" s="T279">v</ta>
            <ta e="T281" id="Seg_3682" s="T280">nprop</ta>
            <ta e="T282" id="Seg_3683" s="T281">dem</ta>
            <ta e="T283" id="Seg_3684" s="T282">adj</ta>
            <ta e="T284" id="Seg_3685" s="T283">v</ta>
            <ta e="T285" id="Seg_3686" s="T284">v</ta>
            <ta e="T286" id="Seg_3687" s="T285">nprop</ta>
            <ta e="T287" id="Seg_3688" s="T286">n</ta>
            <ta e="T288" id="Seg_3689" s="T287">v</ta>
            <ta e="T289" id="Seg_3690" s="T288">nprop</ta>
            <ta e="T290" id="Seg_3691" s="T289">n</ta>
            <ta e="T291" id="Seg_3692" s="T290">adv</ta>
            <ta e="T292" id="Seg_3693" s="T291">n</ta>
            <ta e="T293" id="Seg_3694" s="T292">preverb</ta>
            <ta e="T294" id="Seg_3695" s="T293">preverb</ta>
            <ta e="T295" id="Seg_3696" s="T294">v</ta>
            <ta e="T296" id="Seg_3697" s="T295">n</ta>
            <ta e="T297" id="Seg_3698" s="T296">n</ta>
            <ta e="T298" id="Seg_3699" s="T297">v</ta>
            <ta e="T299" id="Seg_3700" s="T298">conj</ta>
            <ta e="T300" id="Seg_3701" s="T299">pro</ta>
            <ta e="T301" id="Seg_3702" s="T300">v</ta>
            <ta e="T302" id="Seg_3703" s="T301">conj</ta>
            <ta e="T303" id="Seg_3704" s="T302">v</ta>
            <ta e="T304" id="Seg_3705" s="T303">conj</ta>
            <ta e="T305" id="Seg_3706" s="T304">nprop</ta>
            <ta e="T306" id="Seg_3707" s="T305">ptcl</ta>
            <ta e="T307" id="Seg_3708" s="T306">n</ta>
            <ta e="T308" id="Seg_3709" s="T307">conj</ta>
            <ta e="T309" id="Seg_3710" s="T308">nprop</ta>
            <ta e="T310" id="Seg_3711" s="T309">ptcl</ta>
            <ta e="T311" id="Seg_3712" s="T310">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T4" id="Seg_3713" s="T3">np.h:Th</ta>
            <ta e="T5" id="Seg_3714" s="T4">np.h:Th</ta>
            <ta e="T6" id="Seg_3715" s="T5">pro.h:Poss</ta>
            <ta e="T8" id="Seg_3716" s="T7">np.h:Th</ta>
            <ta e="T9" id="Seg_3717" s="T8">np:Th 0.3.h:Poss</ta>
            <ta e="T11" id="Seg_3718" s="T10">np:Th</ta>
            <ta e="T13" id="Seg_3719" s="T12">np.h:A 0.3.h:Poss</ta>
            <ta e="T15" id="Seg_3720" s="T14">0.3.h:A</ta>
            <ta e="T16" id="Seg_3721" s="T15">np:G</ta>
            <ta e="T18" id="Seg_3722" s="T17">np.h:R</ta>
            <ta e="T19" id="Seg_3723" s="T18">0.3.h:A</ta>
            <ta e="T20" id="Seg_3724" s="T19">pro.h:Th</ta>
            <ta e="T21" id="Seg_3725" s="T20">np:L</ta>
            <ta e="T26" id="Seg_3726" s="T25">0.2.h:A</ta>
            <ta e="T27" id="Seg_3727" s="T26">0.2.h:A</ta>
            <ta e="T28" id="Seg_3728" s="T27">np.h:A</ta>
            <ta e="T31" id="Seg_3729" s="T30">0.3.h:A</ta>
            <ta e="T32" id="Seg_3730" s="T31">pro.h:P</ta>
            <ta e="T33" id="Seg_3731" s="T32">np.h:A 0.3.h:Poss</ta>
            <ta e="T35" id="Seg_3732" s="T34">np.h:A 0.3.h:Poss</ta>
            <ta e="T37" id="Seg_3733" s="T36">np:G</ta>
            <ta e="T38" id="Seg_3734" s="T37">0.3.h:A</ta>
            <ta e="T39" id="Seg_3735" s="T38">np.h:Th</ta>
            <ta e="T42" id="Seg_3736" s="T41">np.h:A</ta>
            <ta e="T45" id="Seg_3737" s="T44">np.h:A</ta>
            <ta e="T48" id="Seg_3738" s="T47">np.h:A</ta>
            <ta e="T50" id="Seg_3739" s="T49">pro.h:P</ta>
            <ta e="T53" id="Seg_3740" s="T52">pro.h:A</ta>
            <ta e="T55" id="Seg_3741" s="T54">np:Ins</ta>
            <ta e="T57" id="Seg_3742" s="T56">np.h:P</ta>
            <ta e="T60" id="Seg_3743" s="T59">np:P 0.3.h:Poss</ta>
            <ta e="T61" id="Seg_3744" s="T60">0.3.h:A</ta>
            <ta e="T64" id="Seg_3745" s="T63">0.3.h:A</ta>
            <ta e="T66" id="Seg_3746" s="T65">0.3.h:A</ta>
            <ta e="T67" id="Seg_3747" s="T66">adv:Time</ta>
            <ta e="T69" id="Seg_3748" s="T68">0.3.h:E</ta>
            <ta e="T70" id="Seg_3749" s="T69">pro.h:A</ta>
            <ta e="T72" id="Seg_3750" s="T71">np:Ins 0.3.h:Poss</ta>
            <ta e="T74" id="Seg_3751" s="T73">np.h:A</ta>
            <ta e="T76" id="Seg_3752" s="T75">np:Ins 0.3.h:Poss</ta>
            <ta e="T77" id="Seg_3753" s="T76">np:Poss</ta>
            <ta e="T78" id="Seg_3754" s="T77">np:G</ta>
            <ta e="T80" id="Seg_3755" s="T79">np.h:A</ta>
            <ta e="T83" id="Seg_3756" s="T82">np.h:Th</ta>
            <ta e="T84" id="Seg_3757" s="T83">np:Poss</ta>
            <ta e="T85" id="Seg_3758" s="T84">adv:So</ta>
            <ta e="T86" id="Seg_3759" s="T85">0.3.h:A</ta>
            <ta e="T88" id="Seg_3760" s="T87">np:G 0.3.h:Poss</ta>
            <ta e="T90" id="Seg_3761" s="T89">np.h:P</ta>
            <ta e="T91" id="Seg_3762" s="T90">0.3.h:A</ta>
            <ta e="T92" id="Seg_3763" s="T91">np.h:Poss</ta>
            <ta e="T95" id="Seg_3764" s="T94">np:Th</ta>
            <ta e="T96" id="Seg_3765" s="T95">0.3.h:Th</ta>
            <ta e="T97" id="Seg_3766" s="T96">np.h:Poss</ta>
            <ta e="T98" id="Seg_3767" s="T97">np:L</ta>
            <ta e="T100" id="Seg_3768" s="T99">np:Ins 0.3.h:Poss</ta>
            <ta e="T101" id="Seg_3769" s="T100">np:L 0.3.h:Poss</ta>
            <ta e="T104" id="Seg_3770" s="T103">0.3.h:A 0.3.h:P</ta>
            <ta e="T105" id="Seg_3771" s="T104">np.h:A</ta>
            <ta e="T109" id="Seg_3772" s="T108">pro.h:Poss</ta>
            <ta e="T110" id="Seg_3773" s="T109">np:P</ta>
            <ta e="T112" id="Seg_3774" s="T111">0.2.h:A</ta>
            <ta e="T114" id="Seg_3775" s="T113">np.h:A</ta>
            <ta e="T115" id="Seg_3776" s="T114">np:L</ta>
            <ta e="T118" id="Seg_3777" s="T117">np:Ins 0.3.h:Poss</ta>
            <ta e="T119" id="Seg_3778" s="T118">np.h:Poss</ta>
            <ta e="T120" id="Seg_3779" s="T119">np:P</ta>
            <ta e="T124" id="Seg_3780" s="T123">np.h:A</ta>
            <ta e="T128" id="Seg_3781" s="T127">np:P</ta>
            <ta e="T130" id="Seg_3782" s="T129">0.2.h:A</ta>
            <ta e="T131" id="Seg_3783" s="T130">pro.h:A</ta>
            <ta e="T132" id="Seg_3784" s="T131">pro.h:P</ta>
            <ta e="T135" id="Seg_3785" s="T134">np.h:A</ta>
            <ta e="T136" id="Seg_3786" s="T135">np.h:P</ta>
            <ta e="T139" id="Seg_3787" s="T138">adv:G</ta>
            <ta e="T141" id="Seg_3788" s="T140">np:Poss</ta>
            <ta e="T142" id="Seg_3789" s="T141">np:P</ta>
            <ta e="T143" id="Seg_3790" s="T142">np.h:A</ta>
            <ta e="T146" id="Seg_3791" s="T145">0.3.h:Th</ta>
            <ta e="T147" id="Seg_3792" s="T146">np.h:A</ta>
            <ta e="T148" id="Seg_3793" s="T147">adv:G</ta>
            <ta e="T151" id="Seg_3794" s="T150">0.3.h:A</ta>
            <ta e="T152" id="Seg_3795" s="T151">np.h:Th</ta>
            <ta e="T153" id="Seg_3796" s="T152">0.3.h:Th</ta>
            <ta e="T157" id="Seg_3797" s="T156">np:Time</ta>
            <ta e="T159" id="Seg_3798" s="T158">np.h:A 0.3.h:Poss</ta>
            <ta e="T161" id="Seg_3799" s="T160">np.h:A 0.3.h:Poss</ta>
            <ta e="T163" id="Seg_3800" s="T162">np.h:Th</ta>
            <ta e="T164" id="Seg_3801" s="T163">0.3.h:Th</ta>
            <ta e="T168" id="Seg_3802" s="T167">np.h:A 0.3.h:Poss</ta>
            <ta e="T169" id="Seg_3803" s="T168">np.h:A 0.3.h:Poss</ta>
            <ta e="T171" id="Seg_3804" s="T170">np.h:R</ta>
            <ta e="T172" id="Seg_3805" s="T171">np:P</ta>
            <ta e="T174" id="Seg_3806" s="T173">0.2.h:A</ta>
            <ta e="T176" id="Seg_3807" s="T175">0.2.h:Th</ta>
            <ta e="T178" id="Seg_3808" s="T177">np.h:A</ta>
            <ta e="T180" id="Seg_3809" s="T179">pro.h:P</ta>
            <ta e="T183" id="Seg_3810" s="T182">np.h:A</ta>
            <ta e="T186" id="Seg_3811" s="T185">pro.h:P</ta>
            <ta e="T189" id="Seg_3812" s="T188">np.h:A</ta>
            <ta e="T192" id="Seg_3813" s="T191">np:Time</ta>
            <ta e="T193" id="Seg_3814" s="T192">np.h:A 0.3.h:Poss</ta>
            <ta e="T194" id="Seg_3815" s="T193">np.h:A 0.3.h:Poss</ta>
            <ta e="T195" id="Seg_3816" s="T194">pro:G</ta>
            <ta e="T199" id="Seg_3817" s="T198">adv:L</ta>
            <ta e="T202" id="Seg_3818" s="T201">0.3.h:Th</ta>
            <ta e="T203" id="Seg_3819" s="T202">np.h:A 0.3.h:Poss</ta>
            <ta e="T205" id="Seg_3820" s="T204">np.h:A 0.3.h:Poss</ta>
            <ta e="T208" id="Seg_3821" s="T207">np:Poss</ta>
            <ta e="T209" id="Seg_3822" s="T208">np:Th</ta>
            <ta e="T210" id="Seg_3823" s="T209">np:Poss</ta>
            <ta e="T211" id="Seg_3824" s="T210">adv:G</ta>
            <ta e="T216" id="Seg_3825" s="T215">np:Poss</ta>
            <ta e="T218" id="Seg_3826" s="T217">np:G</ta>
            <ta e="T219" id="Seg_3827" s="T218">0.3.h:A</ta>
            <ta e="T220" id="Seg_3828" s="T219">np.h:A 0.3.h:Poss</ta>
            <ta e="T221" id="Seg_3829" s="T220">np:Th</ta>
            <ta e="T223" id="Seg_3830" s="T222">np:Com</ta>
            <ta e="T224" id="Seg_3831" s="T223">0.3.h:Th</ta>
            <ta e="T225" id="Seg_3832" s="T224">np:Com</ta>
            <ta e="T226" id="Seg_3833" s="T225">0.3.h:A</ta>
            <ta e="T228" id="Seg_3834" s="T227">np.h:A 0.3.h:Poss</ta>
            <ta e="T231" id="Seg_3835" s="T230">np:Th</ta>
            <ta e="T234" id="Seg_3836" s="T233">np.h:R</ta>
            <ta e="T235" id="Seg_3837" s="T234">0.3.h:A</ta>
            <ta e="T237" id="Seg_3838" s="T236">pro.h:A</ta>
            <ta e="T241" id="Seg_3839" s="T240">np.h:A</ta>
            <ta e="T243" id="Seg_3840" s="T242">np:Ins 0.3.h:Poss</ta>
            <ta e="T244" id="Seg_3841" s="T243">0.3.h:A</ta>
            <ta e="T245" id="Seg_3842" s="T244">pro:Th</ta>
            <ta e="T248" id="Seg_3843" s="T247">0.3.h:E</ta>
            <ta e="T249" id="Seg_3844" s="T248">np.h:A</ta>
            <ta e="T251" id="Seg_3845" s="T250">np:G</ta>
            <ta e="T252" id="Seg_3846" s="T251">0.3.h:A</ta>
            <ta e="T253" id="Seg_3847" s="T252">np.h:A</ta>
            <ta e="T254" id="Seg_3848" s="T253">np:Poss</ta>
            <ta e="T255" id="Seg_3849" s="T254">np:Th</ta>
            <ta e="T257" id="Seg_3850" s="T256">np:Poss</ta>
            <ta e="T258" id="Seg_3851" s="T257">np:G</ta>
            <ta e="T259" id="Seg_3852" s="T258">0.3.h:A</ta>
            <ta e="T261" id="Seg_3853" s="T260">0.3.h:P</ta>
            <ta e="T263" id="Seg_3854" s="T262">0.3.h:P</ta>
            <ta e="T266" id="Seg_3855" s="T265">adv:Time</ta>
            <ta e="T270" id="Seg_3856" s="T269">np.h:A</ta>
            <ta e="T271" id="Seg_3857" s="T270">np:Ins</ta>
            <ta e="T272" id="Seg_3858" s="T271">np.h:Poss</ta>
            <ta e="T273" id="Seg_3859" s="T272">np:P</ta>
            <ta e="T275" id="Seg_3860" s="T274">np.h:P</ta>
            <ta e="T276" id="Seg_3861" s="T275">0.3.h:A</ta>
            <ta e="T277" id="Seg_3862" s="T276">adv:L</ta>
            <ta e="T279" id="Seg_3863" s="T278">np:P</ta>
            <ta e="T280" id="Seg_3864" s="T279">0.3.h:A</ta>
            <ta e="T281" id="Seg_3865" s="T280">np.h:Th</ta>
            <ta e="T284" id="Seg_3866" s="T283">np:G</ta>
            <ta e="T285" id="Seg_3867" s="T284">0.3.h:A</ta>
            <ta e="T286" id="Seg_3868" s="T285">np.h:P</ta>
            <ta e="T287" id="Seg_3869" s="T286">np:Cau</ta>
            <ta e="T289" id="Seg_3870" s="T288">np.h:Poss</ta>
            <ta e="T290" id="Seg_3871" s="T289">np:A</ta>
            <ta e="T291" id="Seg_3872" s="T290">0.3:A</ta>
            <ta e="T292" id="Seg_3873" s="T291">np:So</ta>
            <ta e="T296" id="Seg_3874" s="T295">np.h:P</ta>
            <ta e="T297" id="Seg_3875" s="T296">np:Cau</ta>
            <ta e="T301" id="Seg_3876" s="T300">0.3.h:Th</ta>
            <ta e="T303" id="Seg_3877" s="T302">0.3.h:Th</ta>
            <ta e="T305" id="Seg_3878" s="T304">np.h:P</ta>
            <ta e="T309" id="Seg_3879" s="T308">np.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_3880" s="T1">v:pred</ta>
            <ta e="T3" id="Seg_3881" s="T2">v:pred</ta>
            <ta e="T4" id="Seg_3882" s="T3">np.h:S</ta>
            <ta e="T5" id="Seg_3883" s="T4">np.h:S</ta>
            <ta e="T7" id="Seg_3884" s="T6">v:pred</ta>
            <ta e="T8" id="Seg_3885" s="T7">np.h:S</ta>
            <ta e="T9" id="Seg_3886" s="T8">np:S</ta>
            <ta e="T10" id="Seg_3887" s="T9">n:pred</ta>
            <ta e="T11" id="Seg_3888" s="T10">np:S</ta>
            <ta e="T12" id="Seg_3889" s="T11">v:pred</ta>
            <ta e="T13" id="Seg_3890" s="T12">np.h:S</ta>
            <ta e="T14" id="Seg_3891" s="T13">v:pred</ta>
            <ta e="T15" id="Seg_3892" s="T14">s:purp</ta>
            <ta e="T19" id="Seg_3893" s="T18">0.3.h:S v:pred</ta>
            <ta e="T20" id="Seg_3894" s="T19">pro.h:S</ta>
            <ta e="T23" id="Seg_3895" s="T22">v:pred</ta>
            <ta e="T24" id="Seg_3896" s="T23">np:O</ta>
            <ta e="T26" id="Seg_3897" s="T25">0.2.h:S v:pred</ta>
            <ta e="T27" id="Seg_3898" s="T26">s:cond</ta>
            <ta e="T28" id="Seg_3899" s="T27">np.h:S</ta>
            <ta e="T29" id="Seg_3900" s="T28">v:pred</ta>
            <ta e="T31" id="Seg_3901" s="T30">0.3.h:S v:pred</ta>
            <ta e="T32" id="Seg_3902" s="T31">pro.h:O</ta>
            <ta e="T33" id="Seg_3903" s="T32">np.h:S</ta>
            <ta e="T35" id="Seg_3904" s="T34">np.h:S</ta>
            <ta e="T36" id="Seg_3905" s="T35">v:pred</ta>
            <ta e="T38" id="Seg_3906" s="T37">s:purp</ta>
            <ta e="T39" id="Seg_3907" s="T38">np.h:S</ta>
            <ta e="T40" id="Seg_3908" s="T39">v:pred</ta>
            <ta e="T42" id="Seg_3909" s="T41">np.h:S</ta>
            <ta e="T45" id="Seg_3910" s="T44">np.h:S</ta>
            <ta e="T47" id="Seg_3911" s="T46">v:pred</ta>
            <ta e="T48" id="Seg_3912" s="T47">np.h:S</ta>
            <ta e="T49" id="Seg_3913" s="T48">v:pred</ta>
            <ta e="T51" id="Seg_3914" s="T49">s:purp</ta>
            <ta e="T53" id="Seg_3915" s="T52">pro.h:S</ta>
            <ta e="T57" id="Seg_3916" s="T56">np.h:O</ta>
            <ta e="T58" id="Seg_3917" s="T57">v:pred</ta>
            <ta e="T60" id="Seg_3918" s="T59">np:O</ta>
            <ta e="T61" id="Seg_3919" s="T60">0.3.h:S v:pred</ta>
            <ta e="T64" id="Seg_3920" s="T63">0.3.h:S v:pred</ta>
            <ta e="T65" id="Seg_3921" s="T64">np:O</ta>
            <ta e="T66" id="Seg_3922" s="T65">0.3.h:S v:pred</ta>
            <ta e="T69" id="Seg_3923" s="T68">0.3.h:S v:pred</ta>
            <ta e="T70" id="Seg_3924" s="T69">pro.h:S</ta>
            <ta e="T71" id="Seg_3925" s="T70">v:pred</ta>
            <ta e="T73" id="Seg_3926" s="T71">s:temp</ta>
            <ta e="T74" id="Seg_3927" s="T73">np.h:S</ta>
            <ta e="T79" id="Seg_3928" s="T78">v:pred</ta>
            <ta e="T80" id="Seg_3929" s="T79">np.h:S</ta>
            <ta e="T82" id="Seg_3930" s="T81">v:pred</ta>
            <ta e="T83" id="Seg_3931" s="T82">np.h:O</ta>
            <ta e="T86" id="Seg_3932" s="T85">0.3.h:S v:pred</ta>
            <ta e="T90" id="Seg_3933" s="T89">np.h:O</ta>
            <ta e="T91" id="Seg_3934" s="T90">0.3.h:S v:pred</ta>
            <ta e="T93" id="Seg_3935" s="T92">v:pred</ta>
            <ta e="T95" id="Seg_3936" s="T94">np:S</ta>
            <ta e="T96" id="Seg_3937" s="T95">0.3.h:S v:pred</ta>
            <ta e="T104" id="Seg_3938" s="T103">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T105" id="Seg_3939" s="T104">np.h:S</ta>
            <ta e="T106" id="Seg_3940" s="T105">v:pred</ta>
            <ta e="T110" id="Seg_3941" s="T109">np:O</ta>
            <ta e="T112" id="Seg_3942" s="T111">0.2.h:S v:pred</ta>
            <ta e="T114" id="Seg_3943" s="T113">np.h:S</ta>
            <ta e="T120" id="Seg_3944" s="T119">np:O</ta>
            <ta e="T122" id="Seg_3945" s="T121">v:pred</ta>
            <ta e="T124" id="Seg_3946" s="T123">np.h:S</ta>
            <ta e="T125" id="Seg_3947" s="T124">v:pred</ta>
            <ta e="T128" id="Seg_3948" s="T127">np:O</ta>
            <ta e="T130" id="Seg_3949" s="T129">0.2.h:S v:pred</ta>
            <ta e="T131" id="Seg_3950" s="T130">pro.h:S</ta>
            <ta e="T132" id="Seg_3951" s="T131">pro.h:O</ta>
            <ta e="T133" id="Seg_3952" s="T132">v:pred</ta>
            <ta e="T135" id="Seg_3953" s="T134">np.h:S</ta>
            <ta e="T136" id="Seg_3954" s="T135">np.h:O</ta>
            <ta e="T137" id="Seg_3955" s="T136">v:pred</ta>
            <ta e="T142" id="Seg_3956" s="T141">np:O</ta>
            <ta e="T143" id="Seg_3957" s="T142">np.h:S</ta>
            <ta e="T144" id="Seg_3958" s="T143">v:pred</ta>
            <ta e="T146" id="Seg_3959" s="T145">0.3.h:S v:pred</ta>
            <ta e="T147" id="Seg_3960" s="T146">np.h:S</ta>
            <ta e="T149" id="Seg_3961" s="T148">v:pred</ta>
            <ta e="T151" id="Seg_3962" s="T150">0.3.h:S v:pred</ta>
            <ta e="T152" id="Seg_3963" s="T151">np.h:S</ta>
            <ta e="T153" id="Seg_3964" s="T152">s:adv</ta>
            <ta e="T154" id="Seg_3965" s="T153">v:pred</ta>
            <ta e="T158" id="Seg_3966" s="T157">v:pred</ta>
            <ta e="T159" id="Seg_3967" s="T158">np.h:S</ta>
            <ta e="T161" id="Seg_3968" s="T160">np.h:S</ta>
            <ta e="T163" id="Seg_3969" s="T162">np.h:S</ta>
            <ta e="T164" id="Seg_3970" s="T163">s:adv</ta>
            <ta e="T165" id="Seg_3971" s="T164">v:pred</ta>
            <ta e="T168" id="Seg_3972" s="T167">np.h:S</ta>
            <ta e="T169" id="Seg_3973" s="T168">np.h:S</ta>
            <ta e="T170" id="Seg_3974" s="T169">v:pred</ta>
            <ta e="T172" id="Seg_3975" s="T171">np:O</ta>
            <ta e="T174" id="Seg_3976" s="T173">0.2.h:S v:pred</ta>
            <ta e="T176" id="Seg_3977" s="T175">0.2.h:S v:pred</ta>
            <ta e="T178" id="Seg_3978" s="T177">np.h:S</ta>
            <ta e="T179" id="Seg_3979" s="T178">v:pred</ta>
            <ta e="T180" id="Seg_3980" s="T179">pro.h:O</ta>
            <ta e="T183" id="Seg_3981" s="T182">np.h:S</ta>
            <ta e="T184" id="Seg_3982" s="T183">v:pred</ta>
            <ta e="T186" id="Seg_3983" s="T185">pro.h:O</ta>
            <ta e="T189" id="Seg_3984" s="T188">np.h:S</ta>
            <ta e="T190" id="Seg_3985" s="T189">v:pred</ta>
            <ta e="T193" id="Seg_3986" s="T192">np.h:S</ta>
            <ta e="T194" id="Seg_3987" s="T193">np.h:S</ta>
            <ta e="T197" id="Seg_3988" s="T196">v:pred</ta>
            <ta e="T200" id="Seg_3989" s="T199">s:purp</ta>
            <ta e="T202" id="Seg_3990" s="T201">0.3.h:S v:pred</ta>
            <ta e="T203" id="Seg_3991" s="T202">np.h:S</ta>
            <ta e="T205" id="Seg_3992" s="T204">np.h:S</ta>
            <ta e="T209" id="Seg_3993" s="T208">np:O</ta>
            <ta e="T213" id="Seg_3994" s="T212">v:pred</ta>
            <ta e="T219" id="Seg_3995" s="T218">0.3.h:S v:pred</ta>
            <ta e="T220" id="Seg_3996" s="T219">np.h:S</ta>
            <ta e="T221" id="Seg_3997" s="T220">np:O</ta>
            <ta e="T222" id="Seg_3998" s="T221">v:pred</ta>
            <ta e="T224" id="Seg_3999" s="T223">0.3.h:S v:pred</ta>
            <ta e="T226" id="Seg_4000" s="T225">0.3.h:S v:pred</ta>
            <ta e="T228" id="Seg_4001" s="T227">np.h:S</ta>
            <ta e="T231" id="Seg_4002" s="T230">np:O</ta>
            <ta e="T232" id="Seg_4003" s="T231">v:pred</ta>
            <ta e="T235" id="Seg_4004" s="T234">0.3.h:S v:pred</ta>
            <ta e="T237" id="Seg_4005" s="T236">pro.h:S</ta>
            <ta e="T239" id="Seg_4006" s="T238">np:O</ta>
            <ta e="T240" id="Seg_4007" s="T239">v:pred</ta>
            <ta e="T241" id="Seg_4008" s="T240">np.h:S</ta>
            <ta e="T242" id="Seg_4009" s="T241">v:pred</ta>
            <ta e="T244" id="Seg_4010" s="T243">0.3.h:S v:pred</ta>
            <ta e="T245" id="Seg_4011" s="T244">pro:O</ta>
            <ta e="T248" id="Seg_4012" s="T247">0.3.h:S v:pred</ta>
            <ta e="T249" id="Seg_4013" s="T248">np.h:S</ta>
            <ta e="T250" id="Seg_4014" s="T249">v:pred</ta>
            <ta e="T252" id="Seg_4015" s="T251">0.3.h:S v:pred</ta>
            <ta e="T253" id="Seg_4016" s="T252">np.h:S</ta>
            <ta e="T255" id="Seg_4017" s="T254">np:O</ta>
            <ta e="T256" id="Seg_4018" s="T255">v:pred</ta>
            <ta e="T259" id="Seg_4019" s="T258">0.3.h:S v:pred</ta>
            <ta e="T261" id="Seg_4020" s="T260">s:temp</ta>
            <ta e="T263" id="Seg_4021" s="T262">0.3.h:S v:pred</ta>
            <ta e="T270" id="Seg_4022" s="T269">np.h:S</ta>
            <ta e="T273" id="Seg_4023" s="T272">np:O</ta>
            <ta e="T274" id="Seg_4024" s="T273">v:pred</ta>
            <ta e="T275" id="Seg_4025" s="T274">np.h:O</ta>
            <ta e="T276" id="Seg_4026" s="T275">0.3.h:S v:pred</ta>
            <ta e="T279" id="Seg_4027" s="T278">np:O</ta>
            <ta e="T280" id="Seg_4028" s="T279">0.3.h:S v:pred</ta>
            <ta e="T281" id="Seg_4029" s="T280">np.h:O</ta>
            <ta e="T285" id="Seg_4030" s="T284">0.3.h:S v:pred</ta>
            <ta e="T286" id="Seg_4031" s="T285">np.h:O</ta>
            <ta e="T287" id="Seg_4032" s="T286">np:S</ta>
            <ta e="T288" id="Seg_4033" s="T287">v:pred</ta>
            <ta e="T290" id="Seg_4034" s="T289">np:S</ta>
            <ta e="T291" id="Seg_4035" s="T290">s:temp</ta>
            <ta e="T295" id="Seg_4036" s="T294">v:pred</ta>
            <ta e="T296" id="Seg_4037" s="T295">np.h:O</ta>
            <ta e="T297" id="Seg_4038" s="T296">np:S</ta>
            <ta e="T298" id="Seg_4039" s="T297">v:pred</ta>
            <ta e="T301" id="Seg_4040" s="T300">0.3.h:S v:pred</ta>
            <ta e="T303" id="Seg_4041" s="T302">0.3.h:S v:pred</ta>
            <ta e="T305" id="Seg_4042" s="T304">np.h:S</ta>
            <ta e="T307" id="Seg_4043" s="T306">v:pred</ta>
            <ta e="T309" id="Seg_4044" s="T308">np.h:S</ta>
            <ta e="T311" id="Seg_4045" s="T310">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T17" id="Seg_4046" s="T16">RUS:gram</ta>
            <ta e="T30" id="Seg_4047" s="T29">RUS:gram</ta>
            <ta e="T34" id="Seg_4048" s="T33">RUS:gram</ta>
            <ta e="T44" id="Seg_4049" s="T43">RUS:gram</ta>
            <ta e="T52" id="Seg_4050" s="T51">RUS:gram</ta>
            <ta e="T62" id="Seg_4051" s="T61">RUS:gram</ta>
            <ta e="T70" id="Seg_4052" s="T69">RUS:gram</ta>
            <ta e="T89" id="Seg_4053" s="T88">RUS:gram</ta>
            <ta e="T113" id="Seg_4054" s="T112">RUS:gram</ta>
            <ta e="T123" id="Seg_4055" s="T122">RUS:gram</ta>
            <ta e="T150" id="Seg_4056" s="T149">RUS:gram</ta>
            <ta e="T155" id="Seg_4057" s="T154">RUS:gram</ta>
            <ta e="T160" id="Seg_4058" s="T159">RUS:gram</ta>
            <ta e="T162" id="Seg_4059" s="T161">RUS:gram</ta>
            <ta e="T166" id="Seg_4060" s="T165">RUS:gram</ta>
            <ta e="T175" id="Seg_4061" s="T174">RUS:gram</ta>
            <ta e="T177" id="Seg_4062" s="T176">RUS:gram</ta>
            <ta e="T185" id="Seg_4063" s="T184">RUS:gram</ta>
            <ta e="T204" id="Seg_4064" s="T203">RUS:gram</ta>
            <ta e="T214" id="Seg_4065" s="T213">RUS:gram</ta>
            <ta e="T227" id="Seg_4066" s="T226">RUS:gram</ta>
            <ta e="T233" id="Seg_4067" s="T232">RUS:gram</ta>
            <ta e="T236" id="Seg_4068" s="T235">RUS:gram</ta>
            <ta e="T245" id="Seg_4069" s="T244">RUS:gram</ta>
            <ta e="T260" id="Seg_4070" s="T259">RUS:gram</ta>
            <ta e="T264" id="Seg_4071" s="T263">RUS:gram</ta>
            <ta e="T268" id="Seg_4072" s="T267">RUS:gram</ta>
            <ta e="T299" id="Seg_4073" s="T298">RUS:gram</ta>
            <ta e="T300" id="Seg_4074" s="T299">RUS:coreRUS:gram</ta>
            <ta e="T302" id="Seg_4075" s="T301">RUS:gram</ta>
            <ta e="T304" id="Seg_4076" s="T303">RUS:gram</ta>
            <ta e="T308" id="Seg_4077" s="T307">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_4078" s="T1">Once there live an old man and an old woman.</ta>
            <ta e="T8" id="Seg_4079" s="T5">They have a son.</ta>
            <ta e="T10" id="Seg_4080" s="T8">His name is little Kalabok.</ta>
            <ta e="T12" id="Seg_4081" s="T10">Spring comes.</ta>
            <ta e="T19" id="Seg_4082" s="T12">The parents go into the forest to get wood and say to Kalabok:</ta>
            <ta e="T26" id="Seg_4083" s="T19">You are very quiet at home, do not make noise in the house.</ta>
            <ta e="T32" id="Seg_4084" s="T26">If you will be too noisy, Pönege will come and he will eat you.</ta>
            <ta e="T38" id="Seg_4085" s="T32">The parents go into the forest to get wood.</ta>
            <ta e="T41" id="Seg_4086" s="T38">Little Kalabok stays alone.</ta>
            <ta e="T51" id="Seg_4087" s="T41">What the parent do tell me: Pönege will come to eat me.</ta>
            <ta e="T58" id="Seg_4088" s="T51">But I will slice Pönegessa with the little knife.</ta>
            <ta e="T64" id="Seg_4089" s="T58">He sharpens the knife, is singing songs.</ta>
            <ta e="T66" id="Seg_4090" s="T64">He makes noises in the house.</ta>
            <ta e="T73" id="Seg_4091" s="T66">Soon he hears someone coming, stamping.</ta>
            <ta e="T79" id="Seg_4092" s="T73">Little Kalabok hides himself with his small knife under the bed.</ta>
            <ta e="T91" id="Seg_4093" s="T79">Pönege comes into the house, pulls Little Kalabok from under the bed to him and swallows him.</ta>
            <ta e="T95" id="Seg_4094" s="T91">Little Kalabok has a small knife.</ta>
            <ta e="T104" id="Seg_4095" s="T95">He sits in Pönege's stomach, stabs here and there with the small knife.</ta>
            <ta e="T106" id="Seg_4096" s="T104">Pönege says:</ta>
            <ta e="T112" id="Seg_4097" s="T106">Little Kalabok, do not stab into my stomach.</ta>
            <ta e="T122" id="Seg_4098" s="T112">But Little Kalabok begins to stab Pönegessa in the heart with this little knife in his stomach. </ta>
            <ta e="T130" id="Seg_4099" s="T122">And Pönege says: Little Kalabok, Little Kalabokka, do not prick my heart.</ta>
            <ta e="T134" id="Seg_4100" s="T130">I will puke you out.</ta>
            <ta e="T139" id="Seg_4101" s="T134">Pönege spits Little Kalabok out.</ta>
            <ta e="T146" id="Seg_4102" s="T139">Pönege caused Little Kalabok's hair to fall out, he was without hair.</ta>
            <ta e="T151" id="Seg_4103" s="T146">Pönege goes outside and leaves.</ta>
            <ta e="T156" id="Seg_4104" s="T151">Little Kalabok remains sick and without hair.</ta>
            <ta e="T167" id="Seg_4105" s="T156">In the evening his father and mother come and Little Kalabok is lying around sick and without hair.</ta>
            <ta e="T176" id="Seg_4106" s="T167">The parents ask Little Kalabok: Where did your hair go and why are you lying here around sick?</ta>
            <ta e="T184" id="Seg_4107" s="T176">And Little Kalabok says: My evil devil swallowed me.</ta>
            <ta e="T190" id="Seg_4108" s="T184">Such a devil had swallowed me.</ta>
            <ta e="T202" id="Seg_4109" s="T190">On the next day father and mother don't go anywhere, they stay at home during the day.</ta>
            <ta e="T219" id="Seg_4110" s="T202">Father and mother fill a trough with fish slime on the edge of the door and they themselves stand next to both sides of the door.</ta>
            <ta e="T226" id="Seg_4111" s="T219">The father takes an axe, he stands with the axe, stands with the axe [next to the door].</ta>
            <ta e="T232" id="Seg_4112" s="T226">And the old woman, the mother takes the mortar.</ta>
            <ta e="T240" id="Seg_4113" s="T232">And they say to Little Kalabok : Any you, Little Kalabok, make noise in the house!</ta>
            <ta e="T250" id="Seg_4114" s="T240">Little Kalabok sings, beats with a stick, one hears something, Pönege is coming.</ta>
            <ta e="T263" id="Seg_4115" s="T250">He goes into the house, Pönege steps over the threshold, steps into the fish slime, slips, falls down.</ta>
            <ta e="T276" id="Seg_4116" s="T263">And on this morning the father and the mother, the father goes down on Pönege's head with the axe, they kill Pönege.</ta>
            <ta e="T280" id="Seg_4117" s="T276">Outside they make a big fire.</ta>
            <ta e="T285" id="Seg_4118" s="T280">They throw Pönege into the big fire.</ta>
            <ta e="T288" id="Seg_4119" s="T285">Pönege is burning in the fire.</ta>
            <ta e="T295" id="Seg_4120" s="T288">Pönege's teeth fly chattering out of the fire.</ta>
            <ta e="T303" id="Seg_4121" s="T295">The fire eats Pönegesse and now they are living.</ta>
            <ta e="T311" id="Seg_4122" s="T303">And Little Kalabok did grow up now, and there is no Pönege anymore.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_4123" s="T1">Es waren einmal ein alter Mann und eine alte Frau. </ta>
            <ta e="T8" id="Seg_4124" s="T5">Sie hatten einen Sohn. </ta>
            <ta e="T10" id="Seg_4125" s="T8">Sein Name war kleiner Kalabok. </ta>
            <ta e="T12" id="Seg_4126" s="T10">Der Frühling kam. </ta>
            <ta e="T19" id="Seg_4127" s="T12">Die Eltern gingen in den Wald, um Holz zu holen, und sagten zu Kalabokka: </ta>
            <ta e="T26" id="Seg_4128" s="T19">Du bist leise zu Hause, lärme nicht zu sehr im Haus. </ta>
            <ta e="T32" id="Seg_4129" s="T26">Wenn Du lärmen wirst, kommt Pönegessa und frisst dich.“ </ta>
            <ta e="T38" id="Seg_4130" s="T32">Die Eltern gingen in den Wald Holz holen. </ta>
            <ta e="T41" id="Seg_4131" s="T38">Kleiner Kalabok blieb allein. </ta>
            <ta e="T51" id="Seg_4132" s="T41">Was mir die Eltern gesagt haben: Pönegessa kommt mich fressen. </ta>
            <ta e="T58" id="Seg_4133" s="T51">Und ich schneide ihn mit dem kleinen Messer. </ta>
            <ta e="T64" id="Seg_4134" s="T58">Er schleift das Messer, singt Lieder.</ta>
            <ta e="T66" id="Seg_4135" s="T64">Er lärmt im Haus. </ta>
            <ta e="T73" id="Seg_4136" s="T66">Bald hört er jemanden kommen und mit den Füßen stampfen.</ta>
            <ta e="T79" id="Seg_4137" s="T73">Kleiner Kalabok versteckte sich mit seinem kleinen Messer unter dem Bett.</ta>
            <ta e="T91" id="Seg_4138" s="T79">Pönegessa trat ins Haus ein, zog Kleinen Kalabok unter dem Bett zu sich (in sich) und verschluckte ihn. </ta>
            <ta e="T95" id="Seg_4139" s="T91">Kleiner Kalabok hat ein kleines Messer. </ta>
            <ta e="T104" id="Seg_4140" s="T95">Er sitzt in Pönegessas Bauch, sticht hin und her mit dem kleinen Messer.</ta>
            <ta e="T106" id="Seg_4141" s="T104">Pönegessas sagt. </ta>
            <ta e="T112" id="Seg_4142" s="T106">„Kleiner Kalabok, stich mich nicht in meinem Bauch.“ </ta>
            <ta e="T122" id="Seg_4143" s="T112">Und Kalabokka fing an im Bauch mit diesem kleinen Messer Pönegessa ins Herz zu stechen. </ta>
            <ta e="T130" id="Seg_4144" s="T122">Pönegessas sagt: „Kleiner Kalabok, Kleiner Kalabok, stich mein Herz nicht. </ta>
            <ta e="T134" id="Seg_4145" s="T130">Ich kotze dich aus.“ </ta>
            <ta e="T139" id="Seg_4146" s="T134">Pönegessa kotzte Kalabokka aus. </ta>
            <ta e="T146" id="Seg_4147" s="T139">Pönege ließ Kleiner Kalaboks Kopfhaare ausfallen, er blieb ohne Haare.</ta>
            <ta e="T151" id="Seg_4148" s="T146">Pönegessa ging nach draußen und ging weg. </ta>
            <ta e="T156" id="Seg_4149" s="T151">Kleiner Kalabok blieb krank und ohne Haare. </ta>
            <ta e="T167" id="Seg_4150" s="T156">Am Abend kamen sein Vater und seine Mutter, und Kleiner Kalabok liegt krank und ohne Haare da. </ta>
            <ta e="T176" id="Seg_4151" s="T167">Die Eltern fragen Kleinen Kalabok: „Wo sind deine Haare geblieben und warum liegst du hier krank?“ </ta>
            <ta e="T184" id="Seg_4152" s="T176">Und Kleiner Kalabok erzählt: „Ein schrecklicher Teufel hat mich verschluckt.</ta>
            <ta e="T190" id="Seg_4153" s="T184">So ein Teufel hat mich verschluckt. </ta>
            <ta e="T202" id="Seg_4154" s="T190">Am nächsten Tag gingen der Vater und die Mutter nirgendwohin, sie blieben am Tag zu Hause.</ta>
            <ta e="T219" id="Seg_4155" s="T202">Vater und Mutter gossen einen vollen Trog Fischschleim an die Schwelle, und sich selbst stellten sie an die beiden Türseiten.</ta>
            <ta e="T226" id="Seg_4156" s="T219">Der Vater nahm eine Axt, stand mit der Axt, stellte sich mit der Axt [an die Türseite]. </ta>
            <ta e="T232" id="Seg_4157" s="T226">Und die Alte, die Mutter nahm den Stößel.</ta>
            <ta e="T240" id="Seg_4158" s="T232">Und zu Kleiner Kalabok sagen sie: Und du, Kleiner Kalabok, lärme im Haus!</ta>
            <ta e="T250" id="Seg_4159" s="T240">Kleiner Kalabok singt, klopft mit dem Stock, man hört etwas, Pönegessa kommt.</ta>
            <ta e="T263" id="Seg_4160" s="T250">Er ging ins Haus, Pönegessa überschritt die Schwelle, trat auf Fischschleim, rutschte aus, fiel. </ta>
            <ta e="T276" id="Seg_4161" s="T263">Und an diesem Morgen der Vater und die Mutter, der Vater hackte Pönegessas Kopf mit der Axt ab, sie töteten Pönegessa. </ta>
            <ta e="T280" id="Seg_4162" s="T276">Draußen machten sie ein großes Feuer. </ta>
            <ta e="T285" id="Seg_4163" s="T280">Sie warfen Pönegessa in dieses große Feuer. </ta>
            <ta e="T288" id="Seg_4164" s="T285">Pönegessa brennt im Feuer.</ta>
            <ta e="T295" id="Seg_4165" s="T288">Pönegessas Zähne fliegen klappernd aus dem Feuer weg. </ta>
            <ta e="T303" id="Seg_4166" s="T295">Pönegessa ist verbrannt, und jetzt leben sie wohl. </ta>
            <ta e="T311" id="Seg_4167" s="T303">Und Kleiner Kalabok ist jetzt aufgewachsen, und Pönegessas gibt es nicht mehr.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_4168" s="T1">Жили были старик со старухой. </ta>
            <ta e="T8" id="Seg_4169" s="T5">У них был сын. </ta>
            <ta e="T10" id="Seg_4170" s="T8">Его имя маленький Калабок. </ta>
            <ta e="T12" id="Seg_4171" s="T10">Весна пришла. </ta>
            <ta e="T19" id="Seg_4172" s="T12">Отец-мать пошли дрова заготавливать в лес, а Калабокку наказывают: </ta>
            <ta e="T26" id="Seg_4173" s="T19">Ты дома молча, сильно в избе не шуми. </ta>
            <ta e="T32" id="Seg_4174" s="T26">Если шуметь будешь, Пёнегесса придёт и съест тебя.» </ta>
            <ta e="T38" id="Seg_4175" s="T32">Отец и мать ушли в лес дрова готовить.</ta>
            <ta e="T41" id="Seg_4176" s="T38">Меленький Калабок остался один. </ta>
            <ta e="T51" id="Seg_4177" s="T41">Отец и мать что мне говорили: Пёнегесса придёт меня съесть.</ta>
            <ta e="T58" id="Seg_4178" s="T51">А я маленьким ножом Пёнегесса зарежу. </ta>
            <ta e="T64" id="Seg_4179" s="T58">Маленький ножик точит, а сам песни поёт.</ta>
            <ta e="T66" id="Seg_4180" s="T64">В доме шумит. </ta>
            <ta e="T73" id="Seg_4181" s="T66">Вскоре слышит, кто-то идёт, ногами стучит.</ta>
            <ta e="T79" id="Seg_4182" s="T73">Маленький Калабок с маленьким ножом под кровать спрятался.</ta>
            <ta e="T91" id="Seg_4183" s="T79">Пёнегесса в дом зашел, маленького Калабока из-под кровати вытащил к себе (в себя) и Маленького Калабока проглотил. </ta>
            <ta e="T95" id="Seg_4184" s="T91">У маленького Калабока есть маленький нож. </ta>
            <ta e="T104" id="Seg_4185" s="T95">Сидит у Пёнегесса в брюхе, маленьким ножом туда-сюда колет. </ta>
            <ta e="T106" id="Seg_4186" s="T104">Пёнегесса и говорит. </ta>
            <ta e="T112" id="Seg_4187" s="T106">«Маленький Калабок, меленький Калабок, в моём брюхе не тыкай меня». </ta>
            <ta e="T122" id="Seg_4188" s="T112">А Маленький Калабок в брюхе этим маленьким ножиком Пёнегесса по сердцу стал тыкать. </ta>
            <ta e="T130" id="Seg_4189" s="T122">Пёнегесса говорит: «Маленький Калабок, Маленький Калабок, моё сердце не тычь. </ta>
            <ta e="T134" id="Seg_4190" s="T130">Я тебя обратно выблюю».</ta>
            <ta e="T139" id="Seg_4191" s="T134">​Пёнегесса маленького Калабока выблевал обратно. </ta>
            <ta e="T146" id="Seg_4192" s="T139">У Маленького Калабока волосы облезли все, он без волос остался. </ta>
            <ta e="T151" id="Seg_4193" s="T146">Пёнегесса на улицу вышел и ушел. </ta>
            <ta e="T156" id="Seg_4194" s="T151">Маленький Калабок больным остался и без волос.</ta>
            <ta e="T167" id="Seg_4195" s="T156">Вечером пришли отец его и мать, а Маленький Калабок, болея, лежит и без волос.</ta>
            <ta e="T176" id="Seg_4196" s="T167">Отец и мать спрашивают Маленького Калабока: «Волосы свои куда девал и болеешь?» </ta>
            <ta e="T184" id="Seg_4197" s="T176">А Маленький Калабок рассказывает: «Меня страшный черт глотал.</ta>
            <ta e="T190" id="Seg_4198" s="T184">Вот такой черт меня глотал. </ta>
            <ta e="T202" id="Seg_4199" s="T190">на завтрашний день отец и мать никуда не пошли, дома дневать остались. </ta>
            <ta e="T219" id="Seg_4200" s="T202">Отец и мать полное корыто рыбьей слизи у порога налили, ‎‎а сами по обеим сторонам двери встали.</ta>
            <ta e="T226" id="Seg_4201" s="T219">Отец топор взял, с топором стоит, с топором встал. </ta>
            <ta e="T232" id="Seg_4202" s="T226">А мать пест от ступки взяла. </ta>
            <ta e="T240" id="Seg_4203" s="T232">Маленькому Калабоку говорят: Ты, Маленький Калабок, в доме шуми!. </ta>
            <ta e="T250" id="Seg_4204" s="T240">Маленький Калабок поёт, палкой колотит, что-то слышат, Пёнегесса идёт. </ta>
            <ta e="T263" id="Seg_4205" s="T250">В избу заходит, Пёнегесса через порог перешагнул, на рыбью слизь наступил, поскользнувшись, упал. </ta>
            <ta e="T276" id="Seg_4206" s="T263">И в это время отец и мать, топором отец отрубили Пёнегесса голову, Пёнегесса убили. </ta>
            <ta e="T280" id="Seg_4207" s="T276">На улице большое огнище развели. </ta>
            <ta e="T285" id="Seg_4208" s="T280">Пенегесса в это большое огнище бросили. </ta>
            <ta e="T288" id="Seg_4209" s="T285">Пёнегесса в огне горит.</ta>
            <ta e="T295" id="Seg_4210" s="T288">Зубы Пёнегесса со щелчком (цокая) от огня прочь летят.</ta>
            <ta e="T303" id="Seg_4211" s="T295">Пёнегесса сгорел, и теперь то живут и поживают. </ta>
            <ta e="T311" id="Seg_4212" s="T303">И Маленький Калабок теперь вырос, и Пёнегессов теперь нет.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_4213" s="T1">жили были старик со старухой</ta>
            <ta e="T8" id="Seg_4214" s="T5">у них был сын их</ta>
            <ta e="T10" id="Seg_4215" s="T8">имя его калобок </ta>
            <ta e="T12" id="Seg_4216" s="T10">есна пришла</ta>
            <ta e="T19" id="Seg_4217" s="T12">отец-мать пошли дрова заготавливать в лес калабоку наказывают</ta>
            <ta e="T26" id="Seg_4218" s="T19">‎‎ты дома молча, сильно в избе не шуми</ta>
            <ta e="T32" id="Seg_4219" s="T26"> ‎‎если шуметь будешь, колдун придёт и съест тебя</ta>
            <ta e="T38" id="Seg_4220" s="T32">отец и мать ушли в лес дрова готовить</ta>
            <ta e="T41" id="Seg_4221" s="T38">К. остался один.</ta>
            <ta e="T51" id="Seg_4222" s="T41">отец и мать (как) говорили колдун придет меня съесть</ta>
            <ta e="T58" id="Seg_4223" s="T51">я меленьким ножом калдуна зарежу</ta>
            <ta e="T64" id="Seg_4224" s="T58">маленький ножик точит сам песни поет</ta>
            <ta e="T66" id="Seg_4225" s="T64">в доме (в избе) шумит.</ta>
            <ta e="T73" id="Seg_4226" s="T66">вскоре и слышит кто-то идет ногами стучит</ta>
            <ta e="T79" id="Seg_4227" s="T73">к с маленьким ножом под кровать спрятался</ta>
            <ta e="T91" id="Seg_4228" s="T79">калдун-старик в дом зашел, к-ка из-под кровати вытащил к себе к-ка проглотил.</ta>
            <ta e="T95" id="Seg_4229" s="T91">у к-ка есть маленький нож.</ta>
            <ta e="T104" id="Seg_4230" s="T95">сидит у колдуна в брюхе маленьким ножом в брюхе туда-сюда колет.</ta>
            <ta e="T106" id="Seg_4231" s="T104">колдун и говорит.</ta>
            <ta e="T112" id="Seg_4232" s="T106">к к в моем брюхе не тыкай меня.</ta>
            <ta e="T122" id="Seg_4233" s="T112">а к в брюхе этим маленьким ножиком колдуна по сердцу стал тыкать.</ta>
            <ta e="T130" id="Seg_4234" s="T122">колдун говорит к мое сердце не тычь</ta>
            <ta e="T134" id="Seg_4235" s="T130">я тебя выблюю тебя обратно.</ta>
            <ta e="T139" id="Seg_4236" s="T134">колдун kалабока выблевал обратно.</ta>
            <ta e="T146" id="Seg_4237" s="T139">калабока волосы колдун плешувы и стал (обез вес) он без волос остался.</ta>
            <ta e="T151" id="Seg_4238" s="T146">колдун на улицу вышел и ушел.</ta>
            <ta e="T156" id="Seg_4239" s="T151">колобоко болея (больным) остался и без волос.</ta>
            <ta e="T167" id="Seg_4240" s="T156">ечером пришли отец его и мать, а к болея лежит и без волос.</ta>
            <ta e="T176" id="Seg_4241" s="T167">отец и мать спрашивают колобоки Волосы свои куда девал и болеешь</ta>
            <ta e="T184" id="Seg_4242" s="T176"> ‎‎а к рассказывает меня страшный черт глотал.</ta>
            <ta e="T190" id="Seg_4243" s="T184">вот такой черт меня глотал</ta>
            <ta e="T202" id="Seg_4244" s="T190"> ‎‎на завтрашний день отец и мать никуда не пошли, дома дневать (деновать) остались.</ta>
            <ta e="T219" id="Seg_4245" s="T202">отец мать полное корыто рыбьей слизи у порога налили сами по обеим сторонам двери встали.</ta>
            <ta e="T226" id="Seg_4246" s="T219">отец топор взял с топором стоит, с топором встал.</ta>
            <ta e="T232" id="Seg_4247" s="T226">а мать пест от ступки взяла.</ta>
            <ta e="T240" id="Seg_4248" s="T232">к-ку говорят ты, колобок шуми</ta>
            <ta e="T250" id="Seg_4249" s="T240">к поет, палкой колотит что-то слышат, колдун идёт.</ta>
            <ta e="T263" id="Seg_4250" s="T250"> ‎‎в избу заходит колдун через порог перешагнул, на рыбью слизь наступил поскользнувшись, упал</ta>
            <ta e="T276" id="Seg_4251" s="T263">в это время отец мать, отец топором колдуна голову отрубил, колдуна убили.</ta>
            <ta e="T280" id="Seg_4252" s="T276">колдуна в этот большое огнище бросили.</ta>
            <ta e="T285" id="Seg_4253" s="T280"> ‎‎колдуна в этот большое огнище бросили.</ta>
            <ta e="T288" id="Seg_4254" s="T285">колдун горит.</ta>
            <ta e="T295" id="Seg_4255" s="T288">зубы колдуна со щелчком (цокая) от огня прочь летят.</ta>
            <ta e="T303" id="Seg_4256" s="T295">Колдун сгорел и теперь живут и поживают.</ta>
            <ta e="T311" id="Seg_4257" s="T303"> ‎‎Колабок вырос колдунов теперь нет.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T19" id="Seg_4258" s="T12">[AAV:] "наказывать" 'order', not 'punish'</ta>
            <ta e="T51" id="Seg_4259" s="T41">[AAV:] Skipped sentence (№10, p. 675 in mscr.)</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T184" id="Seg_4260" s="T176"> ‎‎чича по-русски никак не переведует</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
