<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>TMR_1967_Poenege_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">TMR_1967_Poenege_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">158</ud-information>
            <ud-information attribute-name="# HIAT:w">114</ud-information>
            <ud-information attribute-name="# e">114</ud-information>
            <ud-information attribute-name="# HIAT:u">30</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="TMR">
            <abbreviation>TMR</abbreviation>
            <sex value="f" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="TMR"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T115" id="Seg_0" n="sc" s="T1">
               <ts e="T2" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Pönege</ts>
                  <nts id="Seg_5" n="HIAT:ip">.</nts>
                  <nts id="Seg_6" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T6" id="Seg_8" n="HIAT:u" s="T2">
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">Elakumbaq</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">warkəkumbaq</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">pajaga</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">aragase</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_23" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">Tabɨštjanan</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">eppa</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">kɨba</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">mɨla</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_38" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">Tabɨštja</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">köškumbaq</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">madʼont</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_48" n="HIAT:ip">(</nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">maːdʼečlʼe</ts>
                  <nts id="Seg_51" n="HIAT:ip">)</nts>
                  <nts id="Seg_52" n="HIAT:ip">.</nts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_55" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_57" n="HIAT:w" s="T14">Elmadladi</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_60" n="HIAT:w" s="T15">qaːlakumbadi</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_63" n="HIAT:w" s="T16">maːtqät</ts>
                  <nts id="Seg_64" n="HIAT:ip">.</nts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_67" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_69" n="HIAT:w" s="T17">Tiːǯekubaq</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_72" n="HIAT:w" s="T18">elmadlaɣandɨ</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_75" n="HIAT:w" s="T19">kunak</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_78" n="HIAT:w" s="T20">eːkat</ts>
                  <nts id="Seg_79" n="HIAT:ip">.</nts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_82" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_84" n="HIAT:w" s="T21">Mi</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_87" n="HIAT:w" s="T22">lɨbwalʼebe</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_90" n="HIAT:w" s="T23">tönǯaj</ts>
                  <nts id="Seg_91" n="HIAT:ip">.</nts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_94" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_96" n="HIAT:w" s="T24">Taptʼel</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_99" n="HIAT:w" s="T25">kundaŋɨn</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_102" n="HIAT:w" s="T26">qwanǯaj</ts>
                  <nts id="Seg_103" n="HIAT:ip">.</nts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_106" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_108" n="HIAT:w" s="T27">Qöškumbaq</ts>
                  <nts id="Seg_109" n="HIAT:ip">.</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_112" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_114" n="HIAT:w" s="T28">Elmadladi</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_117" n="HIAT:w" s="T29">qaːlakumbadi</ts>
                  <nts id="Seg_118" n="HIAT:ip">.</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_121" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_123" n="HIAT:w" s="T30">Tʼeːl</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_126" n="HIAT:w" s="T31">mor</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_129" n="HIAT:w" s="T32">qurtkumbaden</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_132" n="HIAT:w" s="T33">maːtqät</ts>
                  <nts id="Seg_133" n="HIAT:ip">.</nts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_136" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_138" n="HIAT:w" s="T34">Meʒalbəkumbadət</ts>
                  <nts id="Seg_139" n="HIAT:ip">,</nts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_142" n="HIAT:w" s="T35">lärakumbadɨt</ts>
                  <nts id="Seg_143" n="HIAT:ip">,</nts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_146" n="HIAT:w" s="T36">trakačkumbadet</ts>
                  <nts id="Seg_147" n="HIAT:ip">,</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_150" n="HIAT:w" s="T37">a</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_153" n="HIAT:w" s="T38">Pönege</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_156" n="HIAT:w" s="T39">aːmdakumba</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_159" n="HIAT:w" s="T40">onǯe</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_162" n="HIAT:w" s="T41">maːtqɨnt</ts>
                  <nts id="Seg_163" n="HIAT:ip">.</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_166" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_168" n="HIAT:w" s="T42">Aːmdlʼe</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_171" n="HIAT:w" s="T43">massəkumba</ts>
                  <nts id="Seg_172" n="HIAT:ip">:</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_174" n="HIAT:ip">“</nts>
                  <ts e="T45" id="Seg_176" n="HIAT:w" s="T44">Mat</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_179" n="HIAT:w" s="T45">taw</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_182" n="HIAT:w" s="T46">olom</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_185" n="HIAT:w" s="T47">naj</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_188" n="HIAT:w" s="T48">passeda</ts>
                  <nts id="Seg_189" n="HIAT:ip">”</nts>
                  <nts id="Seg_190" n="HIAT:ip">.</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_193" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_195" n="HIAT:w" s="T49">Kʼeːmdese</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_198" n="HIAT:w" s="T50">saːrlʼe</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_201" n="HIAT:w" s="T51">olom</ts>
                  <nts id="Seg_202" n="HIAT:ip">.</nts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_205" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_207" n="HIAT:w" s="T52">Kʼeːmdeze</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_210" n="HIAT:w" s="T53">saːrɨmbat</ts>
                  <nts id="Seg_211" n="HIAT:ip">.</nts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_214" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_216" n="HIAT:w" s="T54">Aːmnɨmba</ts>
                  <nts id="Seg_217" n="HIAT:ip">,</nts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_220" n="HIAT:w" s="T55">i</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_223" n="HIAT:w" s="T56">kʼeːmdet</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_226" n="HIAT:w" s="T57">naj</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_229" n="HIAT:w" s="T58">ologɨt</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_232" n="HIAT:w" s="T59">passedimba</ts>
                  <nts id="Seg_233" n="HIAT:ip">.</nts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_236" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_238" n="HIAT:w" s="T60">Qwädumba</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_241" n="HIAT:w" s="T61">Pönege</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_244" n="HIAT:w" s="T62">i</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_247" n="HIAT:w" s="T63">qwanba</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_250" n="HIAT:w" s="T64">kɨba</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_253" n="HIAT:w" s="T65">malam</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_256" n="HIAT:w" s="T66">amgu</ts>
                  <nts id="Seg_257" n="HIAT:ip">.</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_260" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_262" n="HIAT:w" s="T67">Tömba</ts>
                  <nts id="Seg_263" n="HIAT:ip">,</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_266" n="HIAT:w" s="T68">a</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_269" n="HIAT:w" s="T69">tabla</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_272" n="HIAT:w" s="T70">lakwatčembadet</ts>
                  <nts id="Seg_273" n="HIAT:ip">.</nts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_276" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_278" n="HIAT:w" s="T71">Moːrokondɨt</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_281" n="HIAT:w" s="T72">aza</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_284" n="HIAT:w" s="T73">medɨkumbadɨt</ts>
                  <nts id="Seg_285" n="HIAT:ip">.</nts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_288" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_290" n="HIAT:w" s="T74">Qatälbat</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_293" n="HIAT:w" s="T75">maːdap</ts>
                  <nts id="Seg_294" n="HIAT:ip">.</nts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_297" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_299" n="HIAT:w" s="T76">Wargə</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_302" n="HIAT:w" s="T77">nʼanʼadet</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_305" n="HIAT:w" s="T78">čannɨmba</ts>
                  <nts id="Seg_306" n="HIAT:ip">.</nts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_309" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_311" n="HIAT:w" s="T79">Kud</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_314" n="HIAT:w" s="T80">nɨnda</ts>
                  <nts id="Seg_315" n="HIAT:ip">?</nts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_318" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_320" n="HIAT:w" s="T81">Nop</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_323" n="HIAT:w" s="T82">tau</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_326" n="HIAT:w" s="T83">lɨpwatpa</ts>
                  <nts id="Seg_327" n="HIAT:ip">.</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_330" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_332" n="HIAT:w" s="T84">Awem</ts>
                  <nts id="Seg_333" n="HIAT:ip">,</nts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_336" n="HIAT:w" s="T85">tiː</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_339" n="HIAT:w" s="T86">töːndalʼi</ts>
                  <nts id="Seg_340" n="HIAT:ip">?</nts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_343" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_345" n="HIAT:w" s="T87">A</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_348" n="HIAT:w" s="T88">Pönege</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_351" n="HIAT:w" s="T89">äːǯalgumba</ts>
                  <nts id="Seg_352" n="HIAT:ip">:</nts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_355" n="HIAT:w" s="T90">Mat</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_358" n="HIAT:w" s="T91">töndak</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_361" n="HIAT:w" s="T92">Pönege</ts>
                  <nts id="Seg_362" n="HIAT:ip">.</nts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_365" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_367" n="HIAT:w" s="T93">Ma</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_370" n="HIAT:w" s="T94">štɨt</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_373" n="HIAT:w" s="T95">töːwak</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_376" n="HIAT:w" s="T96">amgu</ts>
                  <nts id="Seg_377" n="HIAT:ip">.</nts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_380" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_382" n="HIAT:w" s="T97">Man</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_385" n="HIAT:w" s="T98">olom</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_388" n="HIAT:w" s="T99">naj</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_391" n="HIAT:w" s="T100">korwan</ts>
                  <nts id="Seg_392" n="HIAT:ip">,</nts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_395" n="HIAT:w" s="T101">passedʼimba</ts>
                  <nts id="Seg_396" n="HIAT:ip">.</nts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_399" n="HIAT:u" s="T102">
                  <ts e="T103" id="Seg_401" n="HIAT:w" s="T102">Kʼeːmdɨze</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_404" n="HIAT:w" s="T103">saːrɨkkam</ts>
                  <nts id="Seg_405" n="HIAT:ip">,</nts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_408" n="HIAT:w" s="T104">kʼeːmde</ts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_411" n="HIAT:w" s="T105">naj</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_414" n="HIAT:w" s="T106">passedʼa</ts>
                  <nts id="Seg_415" n="HIAT:ip">.</nts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T111" id="Seg_418" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_420" n="HIAT:w" s="T107">I</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_423" n="HIAT:w" s="T108">töːwak</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_426" n="HIAT:w" s="T109">tiːštaj</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_429" n="HIAT:w" s="T110">amgu</ts>
                  <nts id="Seg_430" n="HIAT:ip">.</nts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_433" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_435" n="HIAT:w" s="T111">Tiː</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_438" n="HIAT:w" s="T112">wargɨk</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_441" n="HIAT:w" s="T113">maːdep</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_444" n="HIAT:w" s="T114">tʼazelǯimbalɨt</ts>
                  <nts id="Seg_445" n="HIAT:ip">.</nts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T115" id="Seg_447" n="sc" s="T1">
               <ts e="T2" id="Seg_449" n="e" s="T1">Pönege. </ts>
               <ts e="T3" id="Seg_451" n="e" s="T2">Elakumbaq </ts>
               <ts e="T4" id="Seg_453" n="e" s="T3">warkəkumbaq </ts>
               <ts e="T5" id="Seg_455" n="e" s="T4">pajaga </ts>
               <ts e="T6" id="Seg_457" n="e" s="T5">aragase. </ts>
               <ts e="T7" id="Seg_459" n="e" s="T6">Tabɨštjanan </ts>
               <ts e="T8" id="Seg_461" n="e" s="T7">eppa </ts>
               <ts e="T9" id="Seg_463" n="e" s="T8">kɨba </ts>
               <ts e="T10" id="Seg_465" n="e" s="T9">mɨla. </ts>
               <ts e="T11" id="Seg_467" n="e" s="T10">Tabɨštja </ts>
               <ts e="T12" id="Seg_469" n="e" s="T11">köškumbaq </ts>
               <ts e="T13" id="Seg_471" n="e" s="T12">madʼont </ts>
               <ts e="T14" id="Seg_473" n="e" s="T13">(maːdʼečlʼe). </ts>
               <ts e="T15" id="Seg_475" n="e" s="T14">Elmadladi </ts>
               <ts e="T16" id="Seg_477" n="e" s="T15">qaːlakumbadi </ts>
               <ts e="T17" id="Seg_479" n="e" s="T16">maːtqät. </ts>
               <ts e="T18" id="Seg_481" n="e" s="T17">Tiːǯekubaq </ts>
               <ts e="T19" id="Seg_483" n="e" s="T18">elmadlaɣandɨ </ts>
               <ts e="T20" id="Seg_485" n="e" s="T19">kunak </ts>
               <ts e="T21" id="Seg_487" n="e" s="T20">eːkat. </ts>
               <ts e="T22" id="Seg_489" n="e" s="T21">Mi </ts>
               <ts e="T23" id="Seg_491" n="e" s="T22">lɨbwalʼebe </ts>
               <ts e="T24" id="Seg_493" n="e" s="T23">tönǯaj. </ts>
               <ts e="T25" id="Seg_495" n="e" s="T24">Taptʼel </ts>
               <ts e="T26" id="Seg_497" n="e" s="T25">kundaŋɨn </ts>
               <ts e="T27" id="Seg_499" n="e" s="T26">qwanǯaj. </ts>
               <ts e="T28" id="Seg_501" n="e" s="T27">Qöškumbaq. </ts>
               <ts e="T29" id="Seg_503" n="e" s="T28">Elmadladi </ts>
               <ts e="T30" id="Seg_505" n="e" s="T29">qaːlakumbadi. </ts>
               <ts e="T31" id="Seg_507" n="e" s="T30">Tʼeːl </ts>
               <ts e="T32" id="Seg_509" n="e" s="T31">mor </ts>
               <ts e="T33" id="Seg_511" n="e" s="T32">qurtkumbaden </ts>
               <ts e="T34" id="Seg_513" n="e" s="T33">maːtqät. </ts>
               <ts e="T35" id="Seg_515" n="e" s="T34">Meʒalbəkumbadət, </ts>
               <ts e="T36" id="Seg_517" n="e" s="T35">lärakumbadɨt, </ts>
               <ts e="T37" id="Seg_519" n="e" s="T36">trakačkumbadet, </ts>
               <ts e="T38" id="Seg_521" n="e" s="T37">a </ts>
               <ts e="T39" id="Seg_523" n="e" s="T38">Pönege </ts>
               <ts e="T40" id="Seg_525" n="e" s="T39">aːmdakumba </ts>
               <ts e="T41" id="Seg_527" n="e" s="T40">onǯe </ts>
               <ts e="T42" id="Seg_529" n="e" s="T41">maːtqɨnt. </ts>
               <ts e="T43" id="Seg_531" n="e" s="T42">Aːmdlʼe </ts>
               <ts e="T44" id="Seg_533" n="e" s="T43">massəkumba: </ts>
               <ts e="T45" id="Seg_535" n="e" s="T44">“Mat </ts>
               <ts e="T46" id="Seg_537" n="e" s="T45">taw </ts>
               <ts e="T47" id="Seg_539" n="e" s="T46">olom </ts>
               <ts e="T48" id="Seg_541" n="e" s="T47">naj </ts>
               <ts e="T49" id="Seg_543" n="e" s="T48">passeda”. </ts>
               <ts e="T50" id="Seg_545" n="e" s="T49">Kʼeːmdese </ts>
               <ts e="T51" id="Seg_547" n="e" s="T50">saːrlʼe </ts>
               <ts e="T52" id="Seg_549" n="e" s="T51">olom. </ts>
               <ts e="T53" id="Seg_551" n="e" s="T52">Kʼeːmdeze </ts>
               <ts e="T54" id="Seg_553" n="e" s="T53">saːrɨmbat. </ts>
               <ts e="T55" id="Seg_555" n="e" s="T54">Aːmnɨmba, </ts>
               <ts e="T56" id="Seg_557" n="e" s="T55">i </ts>
               <ts e="T57" id="Seg_559" n="e" s="T56">kʼeːmdet </ts>
               <ts e="T58" id="Seg_561" n="e" s="T57">naj </ts>
               <ts e="T59" id="Seg_563" n="e" s="T58">ologɨt </ts>
               <ts e="T60" id="Seg_565" n="e" s="T59">passedimba. </ts>
               <ts e="T61" id="Seg_567" n="e" s="T60">Qwädumba </ts>
               <ts e="T62" id="Seg_569" n="e" s="T61">Pönege </ts>
               <ts e="T63" id="Seg_571" n="e" s="T62">i </ts>
               <ts e="T64" id="Seg_573" n="e" s="T63">qwanba </ts>
               <ts e="T65" id="Seg_575" n="e" s="T64">kɨba </ts>
               <ts e="T66" id="Seg_577" n="e" s="T65">malam </ts>
               <ts e="T67" id="Seg_579" n="e" s="T66">amgu. </ts>
               <ts e="T68" id="Seg_581" n="e" s="T67">Tömba, </ts>
               <ts e="T69" id="Seg_583" n="e" s="T68">a </ts>
               <ts e="T70" id="Seg_585" n="e" s="T69">tabla </ts>
               <ts e="T71" id="Seg_587" n="e" s="T70">lakwatčembadet. </ts>
               <ts e="T72" id="Seg_589" n="e" s="T71">Moːrokondɨt </ts>
               <ts e="T73" id="Seg_591" n="e" s="T72">aza </ts>
               <ts e="T74" id="Seg_593" n="e" s="T73">medɨkumbadɨt. </ts>
               <ts e="T75" id="Seg_595" n="e" s="T74">Qatälbat </ts>
               <ts e="T76" id="Seg_597" n="e" s="T75">maːdap. </ts>
               <ts e="T77" id="Seg_599" n="e" s="T76">Wargə </ts>
               <ts e="T78" id="Seg_601" n="e" s="T77">nʼanʼadet </ts>
               <ts e="T79" id="Seg_603" n="e" s="T78">čannɨmba. </ts>
               <ts e="T80" id="Seg_605" n="e" s="T79">Kud </ts>
               <ts e="T81" id="Seg_607" n="e" s="T80">nɨnda? </ts>
               <ts e="T82" id="Seg_609" n="e" s="T81">Nop </ts>
               <ts e="T83" id="Seg_611" n="e" s="T82">tau </ts>
               <ts e="T84" id="Seg_613" n="e" s="T83">lɨpwatpa. </ts>
               <ts e="T85" id="Seg_615" n="e" s="T84">Awem, </ts>
               <ts e="T86" id="Seg_617" n="e" s="T85">tiː </ts>
               <ts e="T87" id="Seg_619" n="e" s="T86">töːndalʼi? </ts>
               <ts e="T88" id="Seg_621" n="e" s="T87">A </ts>
               <ts e="T89" id="Seg_623" n="e" s="T88">Pönege </ts>
               <ts e="T90" id="Seg_625" n="e" s="T89">äːǯalgumba: </ts>
               <ts e="T91" id="Seg_627" n="e" s="T90">Mat </ts>
               <ts e="T92" id="Seg_629" n="e" s="T91">töndak </ts>
               <ts e="T93" id="Seg_631" n="e" s="T92">Pönege. </ts>
               <ts e="T94" id="Seg_633" n="e" s="T93">Ma </ts>
               <ts e="T95" id="Seg_635" n="e" s="T94">štɨt </ts>
               <ts e="T96" id="Seg_637" n="e" s="T95">töːwak </ts>
               <ts e="T97" id="Seg_639" n="e" s="T96">amgu. </ts>
               <ts e="T98" id="Seg_641" n="e" s="T97">Man </ts>
               <ts e="T99" id="Seg_643" n="e" s="T98">olom </ts>
               <ts e="T100" id="Seg_645" n="e" s="T99">naj </ts>
               <ts e="T101" id="Seg_647" n="e" s="T100">korwan, </ts>
               <ts e="T102" id="Seg_649" n="e" s="T101">passedʼimba. </ts>
               <ts e="T103" id="Seg_651" n="e" s="T102">Kʼeːmdɨze </ts>
               <ts e="T104" id="Seg_653" n="e" s="T103">saːrɨkkam, </ts>
               <ts e="T105" id="Seg_655" n="e" s="T104">kʼeːmde </ts>
               <ts e="T106" id="Seg_657" n="e" s="T105">naj </ts>
               <ts e="T107" id="Seg_659" n="e" s="T106">passedʼa. </ts>
               <ts e="T108" id="Seg_661" n="e" s="T107">I </ts>
               <ts e="T109" id="Seg_663" n="e" s="T108">töːwak </ts>
               <ts e="T110" id="Seg_665" n="e" s="T109">tiːštaj </ts>
               <ts e="T111" id="Seg_667" n="e" s="T110">amgu. </ts>
               <ts e="T112" id="Seg_669" n="e" s="T111">Tiː </ts>
               <ts e="T113" id="Seg_671" n="e" s="T112">wargɨk </ts>
               <ts e="T114" id="Seg_673" n="e" s="T113">maːdep </ts>
               <ts e="T115" id="Seg_675" n="e" s="T114">tʼazelǯimbalɨt. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T2" id="Seg_676" s="T1">TMR_1967_Poenege_flk.001 (001.001)</ta>
            <ta e="T6" id="Seg_677" s="T2">TMR_1967_Poenege_flk.002 (001.002)</ta>
            <ta e="T10" id="Seg_678" s="T6">TMR_1967_Poenege_flk.003 (001.003)</ta>
            <ta e="T14" id="Seg_679" s="T10">TMR_1967_Poenege_flk.004 (001.004)</ta>
            <ta e="T17" id="Seg_680" s="T14">TMR_1967_Poenege_flk.005 (001.005)</ta>
            <ta e="T21" id="Seg_681" s="T17">TMR_1967_Poenege_flk.006 (001.006)</ta>
            <ta e="T24" id="Seg_682" s="T21">TMR_1967_Poenege_flk.007 (001.007)</ta>
            <ta e="T27" id="Seg_683" s="T24">TMR_1967_Poenege_flk.008 (001.008)</ta>
            <ta e="T28" id="Seg_684" s="T27">TMR_1967_Poenege_flk.009 (001.009)</ta>
            <ta e="T30" id="Seg_685" s="T28">TMR_1967_Poenege_flk.010 (001.010)</ta>
            <ta e="T34" id="Seg_686" s="T30">TMR_1967_Poenege_flk.011 (001.011)</ta>
            <ta e="T42" id="Seg_687" s="T34">TMR_1967_Poenege_flk.012 (001.012)</ta>
            <ta e="T49" id="Seg_688" s="T42">TMR_1967_Poenege_flk.013 (001.013)</ta>
            <ta e="T52" id="Seg_689" s="T49">TMR_1967_Poenege_flk.014 (001.014)</ta>
            <ta e="T54" id="Seg_690" s="T52">TMR_1967_Poenege_flk.015 (001.015)</ta>
            <ta e="T60" id="Seg_691" s="T54">TMR_1967_Poenege_flk.016 (001.016)</ta>
            <ta e="T67" id="Seg_692" s="T60">TMR_1967_Poenege_flk.017 (001.017)</ta>
            <ta e="T71" id="Seg_693" s="T67">TMR_1967_Poenege_flk.018 (001.018)</ta>
            <ta e="T74" id="Seg_694" s="T71">TMR_1967_Poenege_flk.019 (001.019)</ta>
            <ta e="T76" id="Seg_695" s="T74">TMR_1967_Poenege_flk.020 (001.020)</ta>
            <ta e="T79" id="Seg_696" s="T76">TMR_1967_Poenege_flk.021 (001.021)</ta>
            <ta e="T81" id="Seg_697" s="T79">TMR_1967_Poenege_flk.022 (001.022)</ta>
            <ta e="T84" id="Seg_698" s="T81">TMR_1967_Poenege_flk.023 (001.023)</ta>
            <ta e="T87" id="Seg_699" s="T84">TMR_1967_Poenege_flk.024 (001.024)</ta>
            <ta e="T93" id="Seg_700" s="T87">TMR_1967_Poenege_flk.025 (001.025)</ta>
            <ta e="T97" id="Seg_701" s="T93">TMR_1967_Poenege_flk.026 (001.026)</ta>
            <ta e="T102" id="Seg_702" s="T97">TMR_1967_Poenege_flk.027 (001.027)</ta>
            <ta e="T107" id="Seg_703" s="T102">TMR_1967_Poenege_flk.028 (001.028)</ta>
            <ta e="T111" id="Seg_704" s="T107">TMR_1967_Poenege_flk.029 (001.029)</ta>
            <ta e="T115" id="Seg_705" s="T111">TMR_1967_Poenege_flk.030 (001.030)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_706" s="T2">′еlакумбаk ′варкъкумбаk па′jага а′рагасе.</ta>
            <ta e="T10" id="Seg_707" s="T6">табышт′jанан ′еппа кы′ба мы′ла.</ta>
            <ta e="T14" id="Seg_708" s="T10">табышт′jа ′кöшкумбаk ма′дʼонт (′мāдʼечлʼе).</ta>
            <ta e="T17" id="Seg_709" s="T14">еlмадлади kāлакумбади ′мāтkäт.</ta>
            <ta e="T21" id="Seg_710" s="T17">′тӣджекубаk еl′мадлаɣанды кунак ′ēкат.</ta>
            <ta e="T24" id="Seg_711" s="T21">ми лыб′валʼебе тöнджай.</ta>
            <ta e="T27" id="Seg_712" s="T24">тап′тʼел кун′даӈын kван′джай.</ta>
            <ta e="T28" id="Seg_713" s="T27">′köшкумбаk.</ta>
            <ta e="T30" id="Seg_714" s="T28">е′lмадлади kāла′кумбади.</ta>
            <ta e="T34" id="Seg_715" s="T30">тʼел мор ′kурткумбаден мāтkäт.</ta>
            <ta e="T42" id="Seg_716" s="T34">ме′жалбъкумбадът, ′лäракумбадыт, ′тракачкумбадет, а ′пöнеге ′āмдакумба ′ондже ′мāтkынт.</ta>
            <ta e="T49" id="Seg_717" s="T42">′āмдлʼе ′массъкумба: мат таw ′олом най пас′седʼа. </ta>
            <ta e="T52" id="Seg_718" s="T49">кʼēмдесе ′сāрлʼе о′лом. </ta>
            <ta e="T54" id="Seg_719" s="T52">′кʼēмдезе сāрымбат.</ta>
            <ta e="T60" id="Seg_720" s="T54">āмнымба, и кʼēмдет най о′логыт па′сседʼимба.</ta>
            <ta e="T67" id="Seg_721" s="T60">′kwäдумба пöнеге и kwäн′ба кы′ба ма′лам ам′гу.</ta>
            <ta e="T71" id="Seg_722" s="T67">тöмба, а таб′ла ла′кватчембадет</ta>
            <ta e="T74" id="Seg_723" s="T71">′мōрокондыт аза медыкум′бадыт.</ta>
            <ta e="T76" id="Seg_724" s="T74">kа′тäлбат ′мāдап.</ta>
            <ta e="T79" id="Seg_725" s="T76">варгъ нʼа′нʼадет ′чаннымба.</ta>
            <ta e="T81" id="Seg_726" s="T79">куд нын′да? </ta>
            <ta e="T84" id="Seg_727" s="T81">ноп ′тау лып′ватпа.</ta>
            <ta e="T87" id="Seg_728" s="T84">а′вем, тӣ ′тȫндалʼи? </ta>
            <ta e="T93" id="Seg_729" s="T87">а пöне′ге ǟ′джалгумба: мат ′тöндак пöне′ге.</ta>
            <ta e="T97" id="Seg_730" s="T93">′маштыт ′тȫвак амгу. </ta>
            <ta e="T102" id="Seg_731" s="T97">ман ′олом най кор′ван, па′сседʼ(e)имба.</ta>
            <ta e="T107" id="Seg_732" s="T102">′кʼēмдызе ′сāрыккам, ′кʼēмде най па′сседʼа.</ta>
            <ta e="T111" id="Seg_733" s="T107">и ′тȫвак тӣштай ам′гу. </ta>
            <ta e="T115" id="Seg_734" s="T111">тӣ ′варгык мāдеп ′тʼазелджимбалыт.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T2" id="Seg_735" s="T1">Pönege</ta>
            <ta e="T6" id="Seg_736" s="T2">elakumbaq warkəkumbaq pajaga aragase. </ta>
            <ta e="T10" id="Seg_737" s="T6">tabɨštjanan eppa kɨba mɨla. </ta>
            <ta e="T14" id="Seg_738" s="T10">tabɨštja köškumbaq madʼont (maːdʼečlʼe).</ta>
            <ta e="T17" id="Seg_739" s="T14">elmadladi qaːlakumbadi maːtqät. </ta>
            <ta e="T21" id="Seg_740" s="T17">tiːǯekubaq elmadlaɣandɨ kunak eːkat. </ta>
            <ta e="T24" id="Seg_741" s="T21">mi lɨbwalʼebe tönǯaj. </ta>
            <ta e="T27" id="Seg_742" s="T24">taptʼel kundaŋɨn qwanǯaj. </ta>
            <ta e="T28" id="Seg_743" s="T27">qöškumbaq.</ta>
            <ta e="T30" id="Seg_744" s="T28">elmadladi qaːlakumbadi. </ta>
            <ta e="T34" id="Seg_745" s="T30">tʼeːl mor qurtkumbaden maːtqät.</ta>
            <ta e="T42" id="Seg_746" s="T34">meʒalbəkumbadət, lärakumbadɨt, trakačkumbadet, a pönege aːmdakumba onǯe maːtqɨnt. </ta>
            <ta e="T49" id="Seg_747" s="T42">aːmdlʼe massəkumba: mat taw olom naj passeda.</ta>
            <ta e="T52" id="Seg_748" s="T49">kʼeːmdese saːrlʼe olom.</ta>
            <ta e="T54" id="Seg_749" s="T52">kʼeːmdeze saːrɨmbat.</ta>
            <ta e="T60" id="Seg_750" s="T54">aːmnɨmba, i keːmdet naj ologɨt passedimba. </ta>
            <ta e="T67" id="Seg_751" s="T60">qwädumba pönege i qwanba kɨba malam amgu. </ta>
            <ta e="T71" id="Seg_752" s="T67">tömba, a tabla lakwatčembadet.</ta>
            <ta e="T74" id="Seg_753" s="T71">moːrokondɨt aza medɨkumbadɨt. </ta>
            <ta e="T76" id="Seg_754" s="T74">qatälbat maːdap.</ta>
            <ta e="T79" id="Seg_755" s="T76">wargə nʼanʼadet čannɨmba. </ta>
            <ta e="T81" id="Seg_756" s="T79">kud nɨnda? </ta>
            <ta e="T84" id="Seg_757" s="T81">nop tau lɨpwatpa. </ta>
            <ta e="T87" id="Seg_758" s="T84">awem, tiː töːndalʼi?</ta>
            <ta e="T97" id="Seg_759" s="T93">ma štɨt töːwak amgu.</ta>
            <ta e="T102" id="Seg_760" s="T97">man olom naj korwan, passedʼimba.</ta>
            <ta e="T107" id="Seg_761" s="T102">kʼeːmdɨze saːrɨkkam, kʼeːmde naj passedʼa. </ta>
            <ta e="T111" id="Seg_762" s="T107">i töːwak tiːštaj amgu.</ta>
            <ta e="T115" id="Seg_763" s="T111">tiː wargɨk maːdep tʼazelǯimbalɨt.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T2" id="Seg_764" s="T1">Pönege. </ta>
            <ta e="T6" id="Seg_765" s="T2">Elakumbaq warkəkumbaq pajaga aragase. </ta>
            <ta e="T10" id="Seg_766" s="T6">Tabɨštjanan eppa kɨba mɨla. </ta>
            <ta e="T14" id="Seg_767" s="T10">Tabɨštja köškumbaq madʼont (maːdʼečlʼe). </ta>
            <ta e="T17" id="Seg_768" s="T14">Elmadladi qaːlakumbadi maːtqät. </ta>
            <ta e="T21" id="Seg_769" s="T17">Tiːǯekubaq elmadlaɣandɨ kunak eːkat. </ta>
            <ta e="T24" id="Seg_770" s="T21">Mi lɨbwalʼebe tönǯaj. </ta>
            <ta e="T27" id="Seg_771" s="T24">Taptʼel kundaŋɨn qwanǯaj. </ta>
            <ta e="T28" id="Seg_772" s="T27">Qöškumbaq. </ta>
            <ta e="T30" id="Seg_773" s="T28">Elmadladi qaːlakumbadi. </ta>
            <ta e="T34" id="Seg_774" s="T30">Tʼeːl mor qurtkumbaden maːtqät. </ta>
            <ta e="T42" id="Seg_775" s="T34">Meʒalbəkumbadət, lärakumbadɨt, trakačkumbadet, a Pönege aːmdakumba onǯe maːtqɨnt. </ta>
            <ta e="T49" id="Seg_776" s="T42">Aːmdlʼe massəkumba: “Mat taw olom naj passeda”. </ta>
            <ta e="T52" id="Seg_777" s="T49">Kʼeːmdese saːrlʼe olom. </ta>
            <ta e="T54" id="Seg_778" s="T52">Kʼeːmdeze saːrɨmbat. </ta>
            <ta e="T60" id="Seg_779" s="T54">Aːmnɨmba, i kʼeːmdet naj ologɨt passedimba. </ta>
            <ta e="T67" id="Seg_780" s="T60">Qwädumba Pönege i qwanba kɨba malam amgu. </ta>
            <ta e="T71" id="Seg_781" s="T67">Tömba, a tabla lakwatčembadet. </ta>
            <ta e="T74" id="Seg_782" s="T71">Moːrokondɨt aza medɨkumbadɨt. </ta>
            <ta e="T76" id="Seg_783" s="T74">Qatälbat maːdap. </ta>
            <ta e="T79" id="Seg_784" s="T76">Wargə nʼanʼadet čannɨmba. </ta>
            <ta e="T81" id="Seg_785" s="T79">Kud nɨnda? </ta>
            <ta e="T84" id="Seg_786" s="T81">Nop tau lɨpwatpa. </ta>
            <ta e="T87" id="Seg_787" s="T84">Awem, tiː töːndalʼi? </ta>
            <ta e="T93" id="Seg_788" s="T87">A Pönege äːǯalgumba: Mat töndak Pönege. </ta>
            <ta e="T97" id="Seg_789" s="T93">Ma štɨt töːwak amgu. </ta>
            <ta e="T102" id="Seg_790" s="T97">Man olom naj korwan, passedʼimba. </ta>
            <ta e="T107" id="Seg_791" s="T102">Kʼeːmdɨze saːrɨkkam, kʼeːmde naj passedʼa. </ta>
            <ta e="T111" id="Seg_792" s="T107">I töːwak tiːštaj amgu. </ta>
            <ta e="T115" id="Seg_793" s="T111">Tiː wargɨk maːdep tʼazelǯimbalɨt. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_794" s="T1">pönege</ta>
            <ta e="T3" id="Seg_795" s="T2">ela-ku-mba-q</ta>
            <ta e="T4" id="Seg_796" s="T3">warkə-ku-mba-q</ta>
            <ta e="T5" id="Seg_797" s="T4">paja-ga</ta>
            <ta e="T6" id="Seg_798" s="T5">ara-ga-se</ta>
            <ta e="T7" id="Seg_799" s="T6">tab-ɨ-štja-nan</ta>
            <ta e="T8" id="Seg_800" s="T7">e-ppa</ta>
            <ta e="T9" id="Seg_801" s="T8">kɨba</ta>
            <ta e="T10" id="Seg_802" s="T9">mɨ-la</ta>
            <ta e="T11" id="Seg_803" s="T10">tab-ɨ-štja</ta>
            <ta e="T12" id="Seg_804" s="T11">köšku-mba-q</ta>
            <ta e="T13" id="Seg_805" s="T12">madʼo-nt</ta>
            <ta e="T14" id="Seg_806" s="T13">maːdʼe-č-lʼe</ta>
            <ta e="T15" id="Seg_807" s="T14">elmad-la-di</ta>
            <ta e="T16" id="Seg_808" s="T15">qaːla-ku-mba-di</ta>
            <ta e="T17" id="Seg_809" s="T16">maːt-qät</ta>
            <ta e="T18" id="Seg_810" s="T17">tiːǯe-ku-ba-q</ta>
            <ta e="T19" id="Seg_811" s="T18">elmad-la-ɣandɨ</ta>
            <ta e="T20" id="Seg_812" s="T19">kunak</ta>
            <ta e="T21" id="Seg_813" s="T20">eː-ka-t</ta>
            <ta e="T22" id="Seg_814" s="T21">mi</ta>
            <ta e="T23" id="Seg_815" s="T22">lɨb-wa-lʼebe</ta>
            <ta e="T24" id="Seg_816" s="T23">tö-nǯa-j</ta>
            <ta e="T25" id="Seg_817" s="T24">tap-tʼel</ta>
            <ta e="T26" id="Seg_818" s="T25">kundaŋ-ɨ-n</ta>
            <ta e="T27" id="Seg_819" s="T26">qwan-ǯa-j</ta>
            <ta e="T28" id="Seg_820" s="T27">qöšku-mba-q</ta>
            <ta e="T29" id="Seg_821" s="T28">elmad-la-di</ta>
            <ta e="T30" id="Seg_822" s="T29">qaːla-ku-mba-di</ta>
            <ta e="T31" id="Seg_823" s="T30">tʼeːl</ta>
            <ta e="T32" id="Seg_824" s="T31">mor</ta>
            <ta e="T33" id="Seg_825" s="T32">qur-t-ku-mba-den</ta>
            <ta e="T34" id="Seg_826" s="T33">maːt-qät</ta>
            <ta e="T35" id="Seg_827" s="T34">meʒa-l-bə-ku-mba-dət</ta>
            <ta e="T36" id="Seg_828" s="T35">lä-r-a-ku-mba-dɨt</ta>
            <ta e="T37" id="Seg_829" s="T36">trakač-ku-mba-det</ta>
            <ta e="T38" id="Seg_830" s="T37">a</ta>
            <ta e="T39" id="Seg_831" s="T38">Pönege</ta>
            <ta e="T40" id="Seg_832" s="T39">aːmda-ku-mba</ta>
            <ta e="T41" id="Seg_833" s="T40">onǯe</ta>
            <ta e="T42" id="Seg_834" s="T41">maːt-qɨn-t</ta>
            <ta e="T43" id="Seg_835" s="T42">aːmd-lʼe</ta>
            <ta e="T44" id="Seg_836" s="T43">massə-ku-mba</ta>
            <ta e="T45" id="Seg_837" s="T44">mat</ta>
            <ta e="T46" id="Seg_838" s="T45">taw</ta>
            <ta e="T47" id="Seg_839" s="T46">olo-m</ta>
            <ta e="T48" id="Seg_840" s="T47">naj</ta>
            <ta e="T49" id="Seg_841" s="T48">passe-da</ta>
            <ta e="T50" id="Seg_842" s="T49">kʼeːmde-se</ta>
            <ta e="T51" id="Seg_843" s="T50">saːr-lʼe</ta>
            <ta e="T52" id="Seg_844" s="T51">olo-m</ta>
            <ta e="T53" id="Seg_845" s="T52">kʼeːmde-ze</ta>
            <ta e="T54" id="Seg_846" s="T53">saːrɨ-mba-t</ta>
            <ta e="T55" id="Seg_847" s="T54">aːmnɨ-mba</ta>
            <ta e="T56" id="Seg_848" s="T55">i</ta>
            <ta e="T57" id="Seg_849" s="T56">kʼeːmde-t</ta>
            <ta e="T58" id="Seg_850" s="T57">naj</ta>
            <ta e="T59" id="Seg_851" s="T58">olo-gɨt</ta>
            <ta e="T60" id="Seg_852" s="T59">passe-di-mba</ta>
            <ta e="T61" id="Seg_853" s="T60">qwädu-mba</ta>
            <ta e="T62" id="Seg_854" s="T61">Pönege</ta>
            <ta e="T63" id="Seg_855" s="T62">i</ta>
            <ta e="T64" id="Seg_856" s="T63">qwan-ba</ta>
            <ta e="T65" id="Seg_857" s="T64">kɨba</ta>
            <ta e="T66" id="Seg_858" s="T65">ma-la-m</ta>
            <ta e="T67" id="Seg_859" s="T66">am-gu</ta>
            <ta e="T68" id="Seg_860" s="T67">tö-mba</ta>
            <ta e="T69" id="Seg_861" s="T68">a</ta>
            <ta e="T70" id="Seg_862" s="T69">tab-la</ta>
            <ta e="T71" id="Seg_863" s="T70">lak-wa-tče-mba-det</ta>
            <ta e="T72" id="Seg_864" s="T71">moːro-kon-dɨt</ta>
            <ta e="T73" id="Seg_865" s="T72">aza</ta>
            <ta e="T74" id="Seg_866" s="T73">medɨ-ku-mba-dɨt</ta>
            <ta e="T75" id="Seg_867" s="T74">qatä-l-ba-t</ta>
            <ta e="T76" id="Seg_868" s="T75">maːda-p</ta>
            <ta e="T77" id="Seg_869" s="T76">wargə</ta>
            <ta e="T78" id="Seg_870" s="T77">nʼanʼa-det</ta>
            <ta e="T79" id="Seg_871" s="T78">čannɨ-mba</ta>
            <ta e="T80" id="Seg_872" s="T79">kud</ta>
            <ta e="T81" id="Seg_873" s="T80">nɨnda</ta>
            <ta e="T82" id="Seg_874" s="T81">nop</ta>
            <ta e="T83" id="Seg_875" s="T82">tau</ta>
            <ta e="T84" id="Seg_876" s="T83">lɨp-wat-pa</ta>
            <ta e="T85" id="Seg_877" s="T84">awe-m</ta>
            <ta e="T86" id="Seg_878" s="T85">tiː</ta>
            <ta e="T87" id="Seg_879" s="T86">töː-nda-lʼi</ta>
            <ta e="T88" id="Seg_880" s="T87">a</ta>
            <ta e="T89" id="Seg_881" s="T88">Pönege</ta>
            <ta e="T90" id="Seg_882" s="T89">äːǯa-l-gu-mba</ta>
            <ta e="T91" id="Seg_883" s="T90">mat</ta>
            <ta e="T92" id="Seg_884" s="T91">tö-nda-k</ta>
            <ta e="T93" id="Seg_885" s="T92">Pönege</ta>
            <ta e="T94" id="Seg_886" s="T93">ma</ta>
            <ta e="T95" id="Seg_887" s="T94">štɨt</ta>
            <ta e="T96" id="Seg_888" s="T95">töː-wa-k</ta>
            <ta e="T97" id="Seg_889" s="T96">am-gu</ta>
            <ta e="T98" id="Seg_890" s="T97">man</ta>
            <ta e="T99" id="Seg_891" s="T98">olo-m</ta>
            <ta e="T100" id="Seg_892" s="T99">naj</ta>
            <ta e="T101" id="Seg_893" s="T100">kor-wa-n</ta>
            <ta e="T102" id="Seg_894" s="T101">passe-dʼi-mba</ta>
            <ta e="T103" id="Seg_895" s="T102">kʼeːmdɨ-ze</ta>
            <ta e="T104" id="Seg_896" s="T103">saːrɨ-kka-m</ta>
            <ta e="T105" id="Seg_897" s="T104">kʼeːmde</ta>
            <ta e="T106" id="Seg_898" s="T105">naj</ta>
            <ta e="T107" id="Seg_899" s="T106">passe-dʼa</ta>
            <ta e="T108" id="Seg_900" s="T107">i</ta>
            <ta e="T109" id="Seg_901" s="T108">töː-wa-k</ta>
            <ta e="T110" id="Seg_902" s="T109">tiːštaj</ta>
            <ta e="T111" id="Seg_903" s="T110">am-gu</ta>
            <ta e="T112" id="Seg_904" s="T111">tiː</ta>
            <ta e="T113" id="Seg_905" s="T112">wargɨ-k</ta>
            <ta e="T114" id="Seg_906" s="T113">maːd-e-p</ta>
            <ta e="T115" id="Seg_907" s="T114">tʼaze-lǯi-mba-lɨt</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_908" s="T1">Pöːnege</ta>
            <ta e="T3" id="Seg_909" s="T2">elɨ-ku-mbɨ-qiː</ta>
            <ta e="T4" id="Seg_910" s="T3">warkə-ku-mbɨ-qiː</ta>
            <ta e="T5" id="Seg_911" s="T4">paja-ka</ta>
            <ta e="T6" id="Seg_912" s="T5">ara-ka-se</ta>
            <ta e="T7" id="Seg_913" s="T6">tap-ɨ-štja-nan</ta>
            <ta e="T8" id="Seg_914" s="T7">eː-mbɨ</ta>
            <ta e="T9" id="Seg_915" s="T8">kɨba</ta>
            <ta e="T10" id="Seg_916" s="T9">mɨ-la</ta>
            <ta e="T11" id="Seg_917" s="T10">tap-ɨ-štja</ta>
            <ta e="T12" id="Seg_918" s="T11">qöškɨ-mbɨ-qiː</ta>
            <ta e="T13" id="Seg_919" s="T12">maǯə-ntɨ</ta>
            <ta e="T14" id="Seg_920" s="T13">maǯə-č-le</ta>
            <ta e="T15" id="Seg_921" s="T14">ɛlʼmаːt-la-tiː</ta>
            <ta e="T16" id="Seg_922" s="T15">qalɨ-ku-mbɨ-di</ta>
            <ta e="T17" id="Seg_923" s="T16">maːt-qɨn</ta>
            <ta e="T18" id="Seg_924" s="T17">tiːǯe-ku-mbɨ-qiː</ta>
            <ta e="T19" id="Seg_925" s="T18">ɛlʼmаːt-la-qɨntɨ</ta>
            <ta e="T20" id="Seg_926" s="T19">kunak</ta>
            <ta e="T21" id="Seg_927" s="T20">eː-ku-tɨt</ta>
            <ta e="T22" id="Seg_928" s="T21">meː</ta>
            <ta e="T23" id="Seg_929" s="T22">lɨba-wa-lewlə</ta>
            <ta e="T24" id="Seg_930" s="T23">töː-nǯe-j</ta>
            <ta e="T25" id="Seg_931" s="T24">taw-tʼeːlə</ta>
            <ta e="T26" id="Seg_932" s="T25">kuntak-ɨ-k</ta>
            <ta e="T27" id="Seg_933" s="T26">qwən-nǯe-j</ta>
            <ta e="T28" id="Seg_934" s="T27">qöškɨ-mbɨ-qiː</ta>
            <ta e="T29" id="Seg_935" s="T28">ɛlʼmаːt-la-tiː</ta>
            <ta e="T30" id="Seg_936" s="T29">qalɨ-ku-mbɨ-di</ta>
            <ta e="T31" id="Seg_937" s="T30">tʼeːlə</ta>
            <ta e="T32" id="Seg_938" s="T31">mor</ta>
            <ta e="T33" id="Seg_939" s="T32">kur-tɨ-ku-mbɨ-tɨt</ta>
            <ta e="T34" id="Seg_940" s="T33">maːt-qɨn</ta>
            <ta e="T35" id="Seg_941" s="T34">meša-l-mbɨ-ku-mbɨ-tɨt</ta>
            <ta e="T36" id="Seg_942" s="T35">lɨː-r-ɨ-ku-mbɨ-tɨt</ta>
            <ta e="T37" id="Seg_943" s="T36">trakač-ku-mbɨ-tɨt</ta>
            <ta e="T38" id="Seg_944" s="T37">a</ta>
            <ta e="T39" id="Seg_945" s="T38">Pöːnege</ta>
            <ta e="T40" id="Seg_946" s="T39">omtə-ku-mbɨ</ta>
            <ta e="T41" id="Seg_947" s="T40">onǯe</ta>
            <ta e="T42" id="Seg_948" s="T41">maːt-qɨn-ntɨ</ta>
            <ta e="T43" id="Seg_949" s="T42">omtə-le</ta>
            <ta e="T44" id="Seg_950" s="T43">massɨ-ku-mbɨ</ta>
            <ta e="T45" id="Seg_951" s="T44">man</ta>
            <ta e="T46" id="Seg_952" s="T45">taw</ta>
            <ta e="T47" id="Seg_953" s="T46">olɨ-mɨ</ta>
            <ta e="T48" id="Seg_954" s="T47">naj</ta>
            <ta e="T49" id="Seg_955" s="T48">passɛː-ntɨ</ta>
            <ta e="T50" id="Seg_956" s="T49">kemdə-se</ta>
            <ta e="T51" id="Seg_957" s="T50">saːrə-lɨ</ta>
            <ta e="T52" id="Seg_958" s="T51">olɨ-m</ta>
            <ta e="T53" id="Seg_959" s="T52">kemdə-se</ta>
            <ta e="T54" id="Seg_960" s="T53">saːrə-mbɨ-tɨ</ta>
            <ta e="T55" id="Seg_961" s="T54">omtə-mbɨ</ta>
            <ta e="T56" id="Seg_962" s="T55">i</ta>
            <ta e="T57" id="Seg_963" s="T56">kemdə-tɨ</ta>
            <ta e="T58" id="Seg_964" s="T57">naj</ta>
            <ta e="T59" id="Seg_965" s="T58">olɨ-qɨn</ta>
            <ta e="T60" id="Seg_966" s="T59">passɛː-ntɨ-mbɨ</ta>
            <ta e="T61" id="Seg_967" s="T60">qwodɨ-mbɨ</ta>
            <ta e="T62" id="Seg_968" s="T61">Pöːnege</ta>
            <ta e="T63" id="Seg_969" s="T62">i</ta>
            <ta e="T64" id="Seg_970" s="T63">qwən-mbɨ</ta>
            <ta e="T65" id="Seg_971" s="T64">kɨba</ta>
            <ta e="T66" id="Seg_972" s="T65">mɨ-la-m</ta>
            <ta e="T67" id="Seg_973" s="T66">am-gu</ta>
            <ta e="T68" id="Seg_974" s="T67">töː-mbɨ</ta>
            <ta e="T69" id="Seg_975" s="T68">a</ta>
            <ta e="T70" id="Seg_976" s="T69">tap-la</ta>
            <ta e="T71" id="Seg_977" s="T70">laqǝ-wa-ču-mbɨ-tɨt</ta>
            <ta e="T72" id="Seg_978" s="T71">moːrɨ-qɨn-tɨt</ta>
            <ta e="T73" id="Seg_979" s="T72">assa</ta>
            <ta e="T74" id="Seg_980" s="T73">medɨ-ku-mbɨ-tɨt</ta>
            <ta e="T75" id="Seg_981" s="T74">qätə-l-mbɨ-tɨ</ta>
            <ta e="T76" id="Seg_982" s="T75">maːta-m</ta>
            <ta e="T77" id="Seg_983" s="T76">wargi</ta>
            <ta e="T78" id="Seg_984" s="T77">nennʼa-tɨt</ta>
            <ta e="T79" id="Seg_985" s="T78">čanǯe-mbɨ</ta>
            <ta e="T80" id="Seg_986" s="T79">kod</ta>
            <ta e="T81" id="Seg_987" s="T80">nɨmtɨ</ta>
            <ta e="T82" id="Seg_988" s="T81">num</ta>
            <ta e="T83" id="Seg_989" s="T82">taw</ta>
            <ta e="T84" id="Seg_990" s="T83">lɨba-wat-mbɨ</ta>
            <ta e="T85" id="Seg_991" s="T84">awa-mɨ</ta>
            <ta e="T86" id="Seg_992" s="T85">tɛː</ta>
            <ta e="T87" id="Seg_993" s="T86">töː-ntɨ-liː</ta>
            <ta e="T88" id="Seg_994" s="T87">a</ta>
            <ta e="T89" id="Seg_995" s="T88">Pöːnege</ta>
            <ta e="T90" id="Seg_996" s="T89">ɛːǯa-l-ku-mbɨ</ta>
            <ta e="T91" id="Seg_997" s="T90">man</ta>
            <ta e="T92" id="Seg_998" s="T91">töː-ntɨ-k</ta>
            <ta e="T93" id="Seg_999" s="T92">Pöːnege</ta>
            <ta e="T94" id="Seg_1000" s="T93">man</ta>
            <ta e="T95" id="Seg_1001" s="T94">sʼindat</ta>
            <ta e="T96" id="Seg_1002" s="T95">töː-ŋɨ-k</ta>
            <ta e="T97" id="Seg_1003" s="T96">am-gu</ta>
            <ta e="T98" id="Seg_1004" s="T97">man</ta>
            <ta e="T99" id="Seg_1005" s="T98">olɨ-mɨ</ta>
            <ta e="T100" id="Seg_1006" s="T99">naj</ta>
            <ta e="T101" id="Seg_1007" s="T100">kur-ŋɨ-ŋ</ta>
            <ta e="T102" id="Seg_1008" s="T101">passɛː-ntɨ-mbɨ</ta>
            <ta e="T103" id="Seg_1009" s="T102">kemdə-se</ta>
            <ta e="T104" id="Seg_1010" s="T103">saːrə-ku-m</ta>
            <ta e="T105" id="Seg_1011" s="T104">kemdə</ta>
            <ta e="T106" id="Seg_1012" s="T105">naj</ta>
            <ta e="T107" id="Seg_1013" s="T106">passɛː-ntɨ</ta>
            <ta e="T108" id="Seg_1014" s="T107">i</ta>
            <ta e="T109" id="Seg_1015" s="T108">töː-ŋɨ-k</ta>
            <ta e="T110" id="Seg_1016" s="T109">tiːšti</ta>
            <ta e="T111" id="Seg_1017" s="T110">am-gu</ta>
            <ta e="T112" id="Seg_1018" s="T111">tɛː</ta>
            <ta e="T113" id="Seg_1019" s="T112">wargi-k</ta>
            <ta e="T114" id="Seg_1020" s="T113">maːt-ɨ-m</ta>
            <ta e="T115" id="Seg_1021" s="T114">tʼaze-lʼčǝ-mbɨ-lɨn</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_1022" s="T1">Pönege.[NOM]</ta>
            <ta e="T3" id="Seg_1023" s="T2">live-HAB-PST.NAR-3DU.S</ta>
            <ta e="T4" id="Seg_1024" s="T3">live-HAB-PST.NAR-3DU.S</ta>
            <ta e="T5" id="Seg_1025" s="T4">old.woman-DIM.[NOM]</ta>
            <ta e="T6" id="Seg_1026" s="T5">old.man-DIM-COM</ta>
            <ta e="T7" id="Seg_1027" s="T6">(s)he-EP-DU-ADES</ta>
            <ta e="T8" id="Seg_1028" s="T7">be-DUR.[3SG.S]</ta>
            <ta e="T9" id="Seg_1029" s="T8">small</ta>
            <ta e="T10" id="Seg_1030" s="T9">something-PL.[NOM]</ta>
            <ta e="T11" id="Seg_1031" s="T10">(s)he-EP-DU</ta>
            <ta e="T12" id="Seg_1032" s="T11">go-PST.NAR-3DU.S</ta>
            <ta e="T13" id="Seg_1033" s="T12">taiga-ILL</ta>
            <ta e="T14" id="Seg_1034" s="T13">taiga-TR-CVB</ta>
            <ta e="T15" id="Seg_1035" s="T14">child-PL.[NOM]-3DU</ta>
            <ta e="T16" id="Seg_1036" s="T15">stay-HAB-PST.NAR-3DU.S</ta>
            <ta e="T17" id="Seg_1037" s="T16">tent-LOC</ta>
            <ta e="T18" id="Seg_1038" s="T17">punish-HAB-PST.NAR-3DU.S</ta>
            <ta e="T19" id="Seg_1039" s="T18">child-PL-ILL.3SG</ta>
            <ta e="T20" id="Seg_1040" s="T19">quiet</ta>
            <ta e="T21" id="Seg_1041" s="T20">be-HAB-3PL</ta>
            <ta e="T22" id="Seg_1042" s="T21">we.DU.[NOM]</ta>
            <ta e="T23" id="Seg_1043" s="T22">dark-DRV-CVB2</ta>
            <ta e="T24" id="Seg_1044" s="T23">come-FUT-1DU</ta>
            <ta e="T25" id="Seg_1045" s="T24">this-day.[NOM]</ta>
            <ta e="T26" id="Seg_1046" s="T25">far-EP-ADVZ</ta>
            <ta e="T27" id="Seg_1047" s="T26">go.away-FUT-1DU</ta>
            <ta e="T28" id="Seg_1048" s="T27">go-PST.NAR-3DU.S</ta>
            <ta e="T29" id="Seg_1049" s="T28">child-PL.[NOM]-3DU</ta>
            <ta e="T30" id="Seg_1050" s="T29">stay-HAB-PST.NAR-3DU.S</ta>
            <ta e="T31" id="Seg_1051" s="T30">day.[NOM]</ta>
            <ta e="T32" id="Seg_1052" s="T31">whole</ta>
            <ta e="T33" id="Seg_1053" s="T32">run-DRV-HAB-PST.NAR-3PL</ta>
            <ta e="T34" id="Seg_1054" s="T33">house-LOC</ta>
            <ta e="T35" id="Seg_1055" s="T34">dance-INCH-DUR-HAB-PST.NAR-3PL</ta>
            <ta e="T36" id="Seg_1056" s="T35">sing-FRQ-EP-HAB-PST.NAR-3PL</ta>
            <ta e="T37" id="Seg_1057" s="T36">%%-HAB-PST.NAR-3PL</ta>
            <ta e="T38" id="Seg_1058" s="T37">but</ta>
            <ta e="T39" id="Seg_1059" s="T38">Pönege.[NOM]</ta>
            <ta e="T40" id="Seg_1060" s="T39">sit-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T41" id="Seg_1061" s="T40">oneself.3SG</ta>
            <ta e="T42" id="Seg_1062" s="T41">house-LOC-OBL.3SG</ta>
            <ta e="T43" id="Seg_1063" s="T42">sit-CVB</ta>
            <ta e="T44" id="Seg_1064" s="T43">say-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T45" id="Seg_1065" s="T44">I.GEN</ta>
            <ta e="T46" id="Seg_1066" s="T45">this</ta>
            <ta e="T47" id="Seg_1067" s="T46">head.[NOM]-1SG</ta>
            <ta e="T48" id="Seg_1068" s="T47">also</ta>
            <ta e="T49" id="Seg_1069" s="T48">break-IPFV.[3SG.S]</ta>
            <ta e="T50" id="Seg_1070" s="T49">bird.cherry.twig-INSTR</ta>
            <ta e="T51" id="Seg_1071" s="T50">bind-RES.[3SG.S]</ta>
            <ta e="T52" id="Seg_1072" s="T51">head-ACC</ta>
            <ta e="T53" id="Seg_1073" s="T52">bird.cherry.twig-INSTR</ta>
            <ta e="T54" id="Seg_1074" s="T53">bind-PST.NAR-3SG.O</ta>
            <ta e="T55" id="Seg_1075" s="T54">sit-PST.NAR.[3SG.S]</ta>
            <ta e="T56" id="Seg_1076" s="T55">and</ta>
            <ta e="T57" id="Seg_1077" s="T56">bird.cherry.twig-3SG</ta>
            <ta e="T58" id="Seg_1078" s="T57">also</ta>
            <ta e="T59" id="Seg_1079" s="T58">head-LOC</ta>
            <ta e="T60" id="Seg_1080" s="T59">break-IPFV-PST.NAR.[3SG.S]</ta>
            <ta e="T61" id="Seg_1081" s="T60">scold-PST.NAR.[3SG.S]</ta>
            <ta e="T62" id="Seg_1082" s="T61">Pönege.[NOM]</ta>
            <ta e="T63" id="Seg_1083" s="T62">and</ta>
            <ta e="T64" id="Seg_1084" s="T63">go.away-PST.NAR.[3SG.S]</ta>
            <ta e="T65" id="Seg_1085" s="T64">small</ta>
            <ta e="T66" id="Seg_1086" s="T65">thing-PL-ACC</ta>
            <ta e="T67" id="Seg_1087" s="T66">eat-INF</ta>
            <ta e="T68" id="Seg_1088" s="T67">come-PST.NAR.[3SG.S]</ta>
            <ta e="T69" id="Seg_1089" s="T68">but</ta>
            <ta e="T70" id="Seg_1090" s="T69">(s)he-PL.[NOM]</ta>
            <ta e="T71" id="Seg_1091" s="T70">laugh-DRV-TR-PST.NAR-3PL</ta>
            <ta e="T72" id="Seg_1092" s="T71">end-LOC-3PL</ta>
            <ta e="T73" id="Seg_1093" s="T72">NEG</ta>
            <ta e="T74" id="Seg_1094" s="T73">become-HAB-PST.NAR-3PL</ta>
            <ta e="T75" id="Seg_1095" s="T74">knock-INCH-PST.NAR-3SG.O</ta>
            <ta e="T76" id="Seg_1096" s="T75">door-ACC</ta>
            <ta e="T77" id="Seg_1097" s="T76">big</ta>
            <ta e="T78" id="Seg_1098" s="T77">sister-3PL</ta>
            <ta e="T79" id="Seg_1099" s="T78">go.out-PST.NAR.[3SG.S]</ta>
            <ta e="T80" id="Seg_1100" s="T79">who</ta>
            <ta e="T81" id="Seg_1101" s="T80">here</ta>
            <ta e="T82" id="Seg_1102" s="T81">sky.[NOM]</ta>
            <ta e="T83" id="Seg_1103" s="T82">there</ta>
            <ta e="T84" id="Seg_1104" s="T83">dark-VBLZ-PST.NAR.[3SG.S]</ta>
            <ta e="T85" id="Seg_1105" s="T84">mother.[NOM]-1SG</ta>
            <ta e="T86" id="Seg_1106" s="T85">you.DU.NOM</ta>
            <ta e="T87" id="Seg_1107" s="T86">come-INFER-2DU</ta>
            <ta e="T88" id="Seg_1108" s="T87">but</ta>
            <ta e="T89" id="Seg_1109" s="T88">Pönege.[NOM]</ta>
            <ta e="T90" id="Seg_1110" s="T89">say-INCH-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T91" id="Seg_1111" s="T90">I.NOM</ta>
            <ta e="T92" id="Seg_1112" s="T91">come-INFER-1SG.S</ta>
            <ta e="T93" id="Seg_1113" s="T92">Pönege.[NOM]</ta>
            <ta e="T94" id="Seg_1114" s="T93">I.NOM</ta>
            <ta e="T95" id="Seg_1115" s="T94">you.PL.ACC</ta>
            <ta e="T96" id="Seg_1116" s="T95">come-CO-1SG.S</ta>
            <ta e="T97" id="Seg_1117" s="T96">eat-INF</ta>
            <ta e="T98" id="Seg_1118" s="T97">I.NOM</ta>
            <ta e="T99" id="Seg_1119" s="T98">head.[NOM]-1SG</ta>
            <ta e="T100" id="Seg_1120" s="T99">also</ta>
            <ta e="T101" id="Seg_1121" s="T100">run-CO-3SG.S</ta>
            <ta e="T102" id="Seg_1122" s="T101">break-IPFV-PST.NAR.[3SG.S]</ta>
            <ta e="T103" id="Seg_1123" s="T102">bird.cherry.twig-INSTR</ta>
            <ta e="T104" id="Seg_1124" s="T103">bind-HAB-1SG.O</ta>
            <ta e="T105" id="Seg_1125" s="T104">bird.cherry.twig.[NOM]</ta>
            <ta e="T106" id="Seg_1126" s="T105">also</ta>
            <ta e="T107" id="Seg_1127" s="T106">break-IPFV.[3SG.S]</ta>
            <ta e="T108" id="Seg_1128" s="T107">and</ta>
            <ta e="T109" id="Seg_1129" s="T108">come-CO-1SG.S</ta>
            <ta e="T110" id="Seg_1130" s="T109">you.PL.ACC</ta>
            <ta e="T111" id="Seg_1131" s="T110">eat-INF</ta>
            <ta e="T112" id="Seg_1132" s="T111">you.PL.NOM</ta>
            <ta e="T113" id="Seg_1133" s="T112">big-ADVZ</ta>
            <ta e="T114" id="Seg_1134" s="T113">house-EP-ACC</ta>
            <ta e="T115" id="Seg_1135" s="T114">fool.around-PFV-PST.NAR-2PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_1136" s="T1">Пёнеге.[NOM]</ta>
            <ta e="T3" id="Seg_1137" s="T2">жить-HAB-PST.NAR-3DU.S</ta>
            <ta e="T4" id="Seg_1138" s="T3">жить-HAB-PST.NAR-3DU.S</ta>
            <ta e="T5" id="Seg_1139" s="T4">старуха-DIM.[NOM]</ta>
            <ta e="T6" id="Seg_1140" s="T5">старик-DIM-COM</ta>
            <ta e="T7" id="Seg_1141" s="T6">он(а)-EP-DU-ADES</ta>
            <ta e="T8" id="Seg_1142" s="T7">быть-DUR.[3SG.S]</ta>
            <ta e="T9" id="Seg_1143" s="T8">маленький</ta>
            <ta e="T10" id="Seg_1144" s="T9">нечто-PL.[NOM]</ta>
            <ta e="T11" id="Seg_1145" s="T10">он(а)-EP-DU</ta>
            <ta e="T12" id="Seg_1146" s="T11">ходить-PST.NAR-3DU.S</ta>
            <ta e="T13" id="Seg_1147" s="T12">тайга-ILL</ta>
            <ta e="T14" id="Seg_1148" s="T13">тайга-TR-CVB</ta>
            <ta e="T15" id="Seg_1149" s="T14">ребёнок-PL.[NOM]-3DU</ta>
            <ta e="T16" id="Seg_1150" s="T15">остаться-HAB-PST.NAR-3DU.S</ta>
            <ta e="T17" id="Seg_1151" s="T16">чум-LOC</ta>
            <ta e="T18" id="Seg_1152" s="T17">наказывать-HAB-PST.NAR-3DU.S</ta>
            <ta e="T19" id="Seg_1153" s="T18">ребёнок-PL-ILL.3SG</ta>
            <ta e="T20" id="Seg_1154" s="T19">тихо</ta>
            <ta e="T21" id="Seg_1155" s="T20">быть-HAB-3PL</ta>
            <ta e="T22" id="Seg_1156" s="T21">мы.DU.[NOM]</ta>
            <ta e="T23" id="Seg_1157" s="T22">тёмный-DRV-CVB2</ta>
            <ta e="T24" id="Seg_1158" s="T23">прийти-FUT-1DU</ta>
            <ta e="T25" id="Seg_1159" s="T24">этот-день.[NOM]</ta>
            <ta e="T26" id="Seg_1160" s="T25">далеко-EP-ADVZ</ta>
            <ta e="T27" id="Seg_1161" s="T26">пойти-FUT-1DU</ta>
            <ta e="T28" id="Seg_1162" s="T27">ходить-PST.NAR-3DU.S</ta>
            <ta e="T29" id="Seg_1163" s="T28">ребёнок-PL.[NOM]-3DU</ta>
            <ta e="T30" id="Seg_1164" s="T29">остаться-HAB-PST.NAR-3DU.S</ta>
            <ta e="T31" id="Seg_1165" s="T30">день.[NOM]</ta>
            <ta e="T32" id="Seg_1166" s="T31">целый</ta>
            <ta e="T33" id="Seg_1167" s="T32">бегать-DRV-HAB-PST.NAR-3PL</ta>
            <ta e="T34" id="Seg_1168" s="T33">дом-LOC</ta>
            <ta e="T35" id="Seg_1169" s="T34">плясать-INCH-DUR-HAB-PST.NAR-3PL</ta>
            <ta e="T36" id="Seg_1170" s="T35">петь-FRQ-EP-HAB-PST.NAR-3PL</ta>
            <ta e="T37" id="Seg_1171" s="T36">%%-HAB-PST.NAR-3PL</ta>
            <ta e="T38" id="Seg_1172" s="T37">а</ta>
            <ta e="T39" id="Seg_1173" s="T38">Пёнеге.[NOM]</ta>
            <ta e="T40" id="Seg_1174" s="T39">сидеть-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T41" id="Seg_1175" s="T40">сам.3SG</ta>
            <ta e="T42" id="Seg_1176" s="T41">дом-LOC-OBL.3SG</ta>
            <ta e="T43" id="Seg_1177" s="T42">сидеть-CVB</ta>
            <ta e="T44" id="Seg_1178" s="T43">сказать-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T45" id="Seg_1179" s="T44">я.GEN</ta>
            <ta e="T46" id="Seg_1180" s="T45">этот</ta>
            <ta e="T47" id="Seg_1181" s="T46">голова.[NOM]-1SG</ta>
            <ta e="T48" id="Seg_1182" s="T47">тоже</ta>
            <ta e="T49" id="Seg_1183" s="T48">лопнуть-IPFV.[3SG.S]</ta>
            <ta e="T50" id="Seg_1184" s="T49">черёмуховый.прут-INSTR</ta>
            <ta e="T51" id="Seg_1185" s="T50">привязать-RES.[3SG.S]</ta>
            <ta e="T52" id="Seg_1186" s="T51">голова-ACC</ta>
            <ta e="T53" id="Seg_1187" s="T52">черёмуховый.прут-INSTR</ta>
            <ta e="T54" id="Seg_1188" s="T53">привязать-PST.NAR-3SG.O</ta>
            <ta e="T55" id="Seg_1189" s="T54">сидеть-PST.NAR.[3SG.S]</ta>
            <ta e="T56" id="Seg_1190" s="T55">и</ta>
            <ta e="T57" id="Seg_1191" s="T56">черёмуховый.прут-3SG</ta>
            <ta e="T58" id="Seg_1192" s="T57">тоже</ta>
            <ta e="T59" id="Seg_1193" s="T58">голова-LOC</ta>
            <ta e="T60" id="Seg_1194" s="T59">лопнуть-IPFV-PST.NAR.[3SG.S]</ta>
            <ta e="T61" id="Seg_1195" s="T60">выругать-PST.NAR.[3SG.S]</ta>
            <ta e="T62" id="Seg_1196" s="T61">Пёнеге.[NOM]</ta>
            <ta e="T63" id="Seg_1197" s="T62">и</ta>
            <ta e="T64" id="Seg_1198" s="T63">пойти-PST.NAR.[3SG.S]</ta>
            <ta e="T65" id="Seg_1199" s="T64">маленький</ta>
            <ta e="T66" id="Seg_1200" s="T65">вещь-PL-ACC</ta>
            <ta e="T67" id="Seg_1201" s="T66">есть-INF</ta>
            <ta e="T68" id="Seg_1202" s="T67">прийти-PST.NAR.[3SG.S]</ta>
            <ta e="T69" id="Seg_1203" s="T68">а</ta>
            <ta e="T70" id="Seg_1204" s="T69">он(а)-PL.[NOM]</ta>
            <ta e="T71" id="Seg_1205" s="T70">смеяться-DRV-TR-PST.NAR-3PL</ta>
            <ta e="T72" id="Seg_1206" s="T71">конец-LOC-3PL</ta>
            <ta e="T73" id="Seg_1207" s="T72">NEG</ta>
            <ta e="T74" id="Seg_1208" s="T73">становиться-HAB-PST.NAR-3PL</ta>
            <ta e="T75" id="Seg_1209" s="T74">стучать-INCH-PST.NAR-3SG.O</ta>
            <ta e="T76" id="Seg_1210" s="T75">дверь-ACC</ta>
            <ta e="T77" id="Seg_1211" s="T76">большой</ta>
            <ta e="T78" id="Seg_1212" s="T77">сестра-3PL</ta>
            <ta e="T79" id="Seg_1213" s="T78">выходить-PST.NAR.[3SG.S]</ta>
            <ta e="T80" id="Seg_1214" s="T79">кто</ta>
            <ta e="T81" id="Seg_1215" s="T80">здесь</ta>
            <ta e="T82" id="Seg_1216" s="T81">небо.[NOM]</ta>
            <ta e="T83" id="Seg_1217" s="T82">там</ta>
            <ta e="T84" id="Seg_1218" s="T83">тёмный-VBLZ-PST.NAR.[3SG.S]</ta>
            <ta e="T85" id="Seg_1219" s="T84">мать.[NOM]-1SG</ta>
            <ta e="T86" id="Seg_1220" s="T85">вы.DU.NOM</ta>
            <ta e="T87" id="Seg_1221" s="T86">прийти-INFER-2DU</ta>
            <ta e="T88" id="Seg_1222" s="T87">а</ta>
            <ta e="T89" id="Seg_1223" s="T88">Пёнеге.[NOM]</ta>
            <ta e="T90" id="Seg_1224" s="T89">сказать-INCH-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T91" id="Seg_1225" s="T90">я.NOM</ta>
            <ta e="T92" id="Seg_1226" s="T91">прийти-INFER-1SG.S</ta>
            <ta e="T93" id="Seg_1227" s="T92">Пёнеге.[NOM]</ta>
            <ta e="T94" id="Seg_1228" s="T93">я.NOM</ta>
            <ta e="T95" id="Seg_1229" s="T94">они.PL.ACC</ta>
            <ta e="T96" id="Seg_1230" s="T95">прийти-CO-1SG.S</ta>
            <ta e="T97" id="Seg_1231" s="T96">есть-INF</ta>
            <ta e="T98" id="Seg_1232" s="T97">я.NOM</ta>
            <ta e="T99" id="Seg_1233" s="T98">голова.[NOM]-1SG</ta>
            <ta e="T100" id="Seg_1234" s="T99">тоже</ta>
            <ta e="T101" id="Seg_1235" s="T100">бегать-CO-3SG.S</ta>
            <ta e="T102" id="Seg_1236" s="T101">лопнуть-IPFV-PST.NAR.[3SG.S]</ta>
            <ta e="T103" id="Seg_1237" s="T102">черёмуховый.прут-INSTR</ta>
            <ta e="T104" id="Seg_1238" s="T103">привязать-HAB-1SG.O</ta>
            <ta e="T105" id="Seg_1239" s="T104">черёмуховый.прут.[NOM]</ta>
            <ta e="T106" id="Seg_1240" s="T105">тоже</ta>
            <ta e="T107" id="Seg_1241" s="T106">лопнуть-IPFV.[3SG.S]</ta>
            <ta e="T108" id="Seg_1242" s="T107">и</ta>
            <ta e="T109" id="Seg_1243" s="T108">прийти-CO-1SG.S</ta>
            <ta e="T110" id="Seg_1244" s="T109">вы.PL.ACC</ta>
            <ta e="T111" id="Seg_1245" s="T110">есть-INF</ta>
            <ta e="T112" id="Seg_1246" s="T111">вы.PL.NOM</ta>
            <ta e="T113" id="Seg_1247" s="T112">большой-ADVZ</ta>
            <ta e="T114" id="Seg_1248" s="T113">дом-EP-ACC</ta>
            <ta e="T115" id="Seg_1249" s="T114">баловаться-PFV-PST.NAR-2PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1250" s="T1">nprop.[n:case]</ta>
            <ta e="T3" id="Seg_1251" s="T2">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_1252" s="T3">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_1253" s="T4">n-n&gt;n.[n:case]</ta>
            <ta e="T6" id="Seg_1254" s="T5">n-n&gt;n-n:case</ta>
            <ta e="T7" id="Seg_1255" s="T6">pers-n:ins-n:num-n:case</ta>
            <ta e="T8" id="Seg_1256" s="T7">v-v&gt;v.[v:pn]</ta>
            <ta e="T9" id="Seg_1257" s="T8">adj</ta>
            <ta e="T10" id="Seg_1258" s="T9">n-n:num.[n:case]</ta>
            <ta e="T11" id="Seg_1259" s="T10">pers-n:ins-n:num</ta>
            <ta e="T12" id="Seg_1260" s="T11">v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_1261" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_1262" s="T13">n-n&gt;v-adv</ta>
            <ta e="T15" id="Seg_1263" s="T14">n-n:num.[n:case]-n:poss</ta>
            <ta e="T16" id="Seg_1264" s="T15">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_1265" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_1266" s="T17">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_1267" s="T18">n-n:num-n:case.poss</ta>
            <ta e="T20" id="Seg_1268" s="T19">adv</ta>
            <ta e="T21" id="Seg_1269" s="T20">v-v&gt;v-v:pn</ta>
            <ta e="T22" id="Seg_1270" s="T21">pers.[n:case]</ta>
            <ta e="T23" id="Seg_1271" s="T22">adj-adj&gt;v-v&gt;adv</ta>
            <ta e="T24" id="Seg_1272" s="T23">v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_1273" s="T24">dem-n.[n:case]</ta>
            <ta e="T26" id="Seg_1274" s="T25">adv-n:ins-adj&gt;adv</ta>
            <ta e="T27" id="Seg_1275" s="T26">v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_1276" s="T27">v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_1277" s="T28">n-n:num.[n:case]-n:poss</ta>
            <ta e="T30" id="Seg_1278" s="T29">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_1279" s="T30">n.[n:case]</ta>
            <ta e="T32" id="Seg_1280" s="T31">adj</ta>
            <ta e="T33" id="Seg_1281" s="T32">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T34" id="Seg_1282" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_1283" s="T34">v-v&gt;v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T36" id="Seg_1284" s="T35">v-v&gt;v-n:ins-v&gt;v-v:tense-v:pn</ta>
            <ta e="T37" id="Seg_1285" s="T36">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T38" id="Seg_1286" s="T37">conj</ta>
            <ta e="T39" id="Seg_1287" s="T38">nprop.[n:case]</ta>
            <ta e="T40" id="Seg_1288" s="T39">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T41" id="Seg_1289" s="T40">emphpro</ta>
            <ta e="T42" id="Seg_1290" s="T41">n-n:case-n:case.poss</ta>
            <ta e="T43" id="Seg_1291" s="T42">v-v&gt;adv</ta>
            <ta e="T44" id="Seg_1292" s="T43">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T45" id="Seg_1293" s="T44">pers</ta>
            <ta e="T46" id="Seg_1294" s="T45">dem</ta>
            <ta e="T47" id="Seg_1295" s="T46">n.[n:case]-n:case.poss</ta>
            <ta e="T48" id="Seg_1296" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_1297" s="T48">v-v&gt;v.[v:pn]</ta>
            <ta e="T50" id="Seg_1298" s="T49">n-n:case</ta>
            <ta e="T51" id="Seg_1299" s="T50">v&gt;v.[v:pn]</ta>
            <ta e="T52" id="Seg_1300" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_1301" s="T52">n-n:case</ta>
            <ta e="T54" id="Seg_1302" s="T53">v-v:tense-v:pn</ta>
            <ta e="T55" id="Seg_1303" s="T54">v-v:tense.[v:pn]</ta>
            <ta e="T56" id="Seg_1304" s="T55">conj</ta>
            <ta e="T57" id="Seg_1305" s="T56">n-n:poss</ta>
            <ta e="T58" id="Seg_1306" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_1307" s="T58">n-n:case</ta>
            <ta e="T60" id="Seg_1308" s="T59">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T61" id="Seg_1309" s="T60">v-v:tense.[v:pn]</ta>
            <ta e="T62" id="Seg_1310" s="T61">nprop.[n:case]</ta>
            <ta e="T63" id="Seg_1311" s="T62">conj</ta>
            <ta e="T64" id="Seg_1312" s="T63">v-v:tense.[v:pn]</ta>
            <ta e="T65" id="Seg_1313" s="T64">adj</ta>
            <ta e="T66" id="Seg_1314" s="T65">n-n:num-n:case</ta>
            <ta e="T67" id="Seg_1315" s="T66">v-v:inf</ta>
            <ta e="T68" id="Seg_1316" s="T67">v-v:tense.[v:pn]</ta>
            <ta e="T69" id="Seg_1317" s="T68">conj</ta>
            <ta e="T70" id="Seg_1318" s="T69">pers-n:num.[n:case]</ta>
            <ta e="T71" id="Seg_1319" s="T70">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T72" id="Seg_1320" s="T71">n-n:case-n:poss</ta>
            <ta e="T73" id="Seg_1321" s="T72">ptcl</ta>
            <ta e="T74" id="Seg_1322" s="T73">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T75" id="Seg_1323" s="T74">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T76" id="Seg_1324" s="T75">n-n:case</ta>
            <ta e="T77" id="Seg_1325" s="T76">adj</ta>
            <ta e="T78" id="Seg_1326" s="T77">n-n:poss</ta>
            <ta e="T79" id="Seg_1327" s="T78">v-v:tense.[v:pn]</ta>
            <ta e="T80" id="Seg_1328" s="T79">interrog</ta>
            <ta e="T81" id="Seg_1329" s="T80">adv</ta>
            <ta e="T82" id="Seg_1330" s="T81">n.[n:case]</ta>
            <ta e="T83" id="Seg_1331" s="T82">adv</ta>
            <ta e="T84" id="Seg_1332" s="T83">adj-adj&gt;v-v:tense.[v:pn]</ta>
            <ta e="T85" id="Seg_1333" s="T84">n.[n:case]-n:case.poss</ta>
            <ta e="T86" id="Seg_1334" s="T85">pers</ta>
            <ta e="T87" id="Seg_1335" s="T86">v-v:mood-v:pn</ta>
            <ta e="T88" id="Seg_1336" s="T87">conj</ta>
            <ta e="T89" id="Seg_1337" s="T88">nprop.[n:case]</ta>
            <ta e="T90" id="Seg_1338" s="T89">v-v&gt;v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T91" id="Seg_1339" s="T90">pers</ta>
            <ta e="T92" id="Seg_1340" s="T91">v-v:mood-v:pn</ta>
            <ta e="T93" id="Seg_1341" s="T92">nprop.[n:case]</ta>
            <ta e="T94" id="Seg_1342" s="T93">pers</ta>
            <ta e="T95" id="Seg_1343" s="T94">pers</ta>
            <ta e="T96" id="Seg_1344" s="T95">v-v:ins-v:pn</ta>
            <ta e="T97" id="Seg_1345" s="T96">v-v:inf</ta>
            <ta e="T98" id="Seg_1346" s="T97">pers</ta>
            <ta e="T99" id="Seg_1347" s="T98">n.[n:case]-n:case.poss</ta>
            <ta e="T100" id="Seg_1348" s="T99">ptcl</ta>
            <ta e="T101" id="Seg_1349" s="T100">v-v:ins-v:pn</ta>
            <ta e="T102" id="Seg_1350" s="T101">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T103" id="Seg_1351" s="T102">n-n:case</ta>
            <ta e="T104" id="Seg_1352" s="T103">v-v&gt;v-v:pn</ta>
            <ta e="T105" id="Seg_1353" s="T104">n.[n:case]</ta>
            <ta e="T106" id="Seg_1354" s="T105">ptcl</ta>
            <ta e="T107" id="Seg_1355" s="T106">v-v&gt;v.[v:pn]</ta>
            <ta e="T108" id="Seg_1356" s="T107">conj</ta>
            <ta e="T109" id="Seg_1357" s="T108">v-v:ins-v:pn</ta>
            <ta e="T110" id="Seg_1358" s="T109">pers</ta>
            <ta e="T111" id="Seg_1359" s="T110">v-v:inf</ta>
            <ta e="T112" id="Seg_1360" s="T111">pers</ta>
            <ta e="T113" id="Seg_1361" s="T112">adj-adj&gt;adv</ta>
            <ta e="T114" id="Seg_1362" s="T113">n-n:ins-n:case</ta>
            <ta e="T115" id="Seg_1363" s="T114">v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1364" s="T1">nprop</ta>
            <ta e="T3" id="Seg_1365" s="T2">v</ta>
            <ta e="T4" id="Seg_1366" s="T3">v</ta>
            <ta e="T5" id="Seg_1367" s="T4">n</ta>
            <ta e="T6" id="Seg_1368" s="T5">n</ta>
            <ta e="T7" id="Seg_1369" s="T6">pers</ta>
            <ta e="T8" id="Seg_1370" s="T7">v</ta>
            <ta e="T9" id="Seg_1371" s="T8">n</ta>
            <ta e="T10" id="Seg_1372" s="T9">n</ta>
            <ta e="T11" id="Seg_1373" s="T10">pers</ta>
            <ta e="T12" id="Seg_1374" s="T11">v</ta>
            <ta e="T13" id="Seg_1375" s="T12">n</ta>
            <ta e="T14" id="Seg_1376" s="T13">adv</ta>
            <ta e="T15" id="Seg_1377" s="T14">n</ta>
            <ta e="T16" id="Seg_1378" s="T15">v</ta>
            <ta e="T17" id="Seg_1379" s="T16">n</ta>
            <ta e="T18" id="Seg_1380" s="T17">v</ta>
            <ta e="T19" id="Seg_1381" s="T18">n</ta>
            <ta e="T20" id="Seg_1382" s="T19">adv</ta>
            <ta e="T21" id="Seg_1383" s="T20">n</ta>
            <ta e="T22" id="Seg_1384" s="T21">pers</ta>
            <ta e="T23" id="Seg_1385" s="T22">adv</ta>
            <ta e="T24" id="Seg_1386" s="T23">v</ta>
            <ta e="T25" id="Seg_1387" s="T24">n</ta>
            <ta e="T26" id="Seg_1388" s="T25">adv</ta>
            <ta e="T27" id="Seg_1389" s="T26">adj</ta>
            <ta e="T28" id="Seg_1390" s="T27">v</ta>
            <ta e="T29" id="Seg_1391" s="T28">n</ta>
            <ta e="T30" id="Seg_1392" s="T29">v</ta>
            <ta e="T31" id="Seg_1393" s="T30">n</ta>
            <ta e="T32" id="Seg_1394" s="T31">pp</ta>
            <ta e="T33" id="Seg_1395" s="T32">v</ta>
            <ta e="T34" id="Seg_1396" s="T33">n</ta>
            <ta e="T35" id="Seg_1397" s="T34">v</ta>
            <ta e="T36" id="Seg_1398" s="T35">v</ta>
            <ta e="T37" id="Seg_1399" s="T36">v</ta>
            <ta e="T38" id="Seg_1400" s="T37">conj</ta>
            <ta e="T39" id="Seg_1401" s="T38">nprop</ta>
            <ta e="T40" id="Seg_1402" s="T39">v</ta>
            <ta e="T41" id="Seg_1403" s="T40">emphpro</ta>
            <ta e="T42" id="Seg_1404" s="T41">n</ta>
            <ta e="T43" id="Seg_1405" s="T42">adv</ta>
            <ta e="T44" id="Seg_1406" s="T43">v</ta>
            <ta e="T45" id="Seg_1407" s="T44">pers</ta>
            <ta e="T46" id="Seg_1408" s="T45">dem</ta>
            <ta e="T47" id="Seg_1409" s="T46">n</ta>
            <ta e="T48" id="Seg_1410" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_1411" s="T48">v</ta>
            <ta e="T50" id="Seg_1412" s="T49">nprop</ta>
            <ta e="T51" id="Seg_1413" s="T50">v</ta>
            <ta e="T52" id="Seg_1414" s="T51">n</ta>
            <ta e="T53" id="Seg_1415" s="T52">nprop</ta>
            <ta e="T54" id="Seg_1416" s="T53">v</ta>
            <ta e="T55" id="Seg_1417" s="T54">v</ta>
            <ta e="T56" id="Seg_1418" s="T55">conj</ta>
            <ta e="T57" id="Seg_1419" s="T56">n</ta>
            <ta e="T58" id="Seg_1420" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_1421" s="T58">n</ta>
            <ta e="T60" id="Seg_1422" s="T59">v</ta>
            <ta e="T61" id="Seg_1423" s="T60">v</ta>
            <ta e="T62" id="Seg_1424" s="T61">nprop</ta>
            <ta e="T63" id="Seg_1425" s="T62">conj</ta>
            <ta e="T64" id="Seg_1426" s="T63">v</ta>
            <ta e="T65" id="Seg_1427" s="T64">n</ta>
            <ta e="T66" id="Seg_1428" s="T65">n</ta>
            <ta e="T67" id="Seg_1429" s="T66">v</ta>
            <ta e="T68" id="Seg_1430" s="T67">v</ta>
            <ta e="T69" id="Seg_1431" s="T68">conj</ta>
            <ta e="T70" id="Seg_1432" s="T69">pro</ta>
            <ta e="T71" id="Seg_1433" s="T70">v</ta>
            <ta e="T72" id="Seg_1434" s="T71">n</ta>
            <ta e="T73" id="Seg_1435" s="T72">ptcl</ta>
            <ta e="T74" id="Seg_1436" s="T73">v</ta>
            <ta e="T75" id="Seg_1437" s="T74">v</ta>
            <ta e="T76" id="Seg_1438" s="T75">n</ta>
            <ta e="T77" id="Seg_1439" s="T76">adj</ta>
            <ta e="T78" id="Seg_1440" s="T77">n</ta>
            <ta e="T79" id="Seg_1441" s="T78">v</ta>
            <ta e="T80" id="Seg_1442" s="T79">interrog</ta>
            <ta e="T81" id="Seg_1443" s="T80">adv</ta>
            <ta e="T82" id="Seg_1444" s="T81">n</ta>
            <ta e="T83" id="Seg_1445" s="T82">adv</ta>
            <ta e="T84" id="Seg_1446" s="T83">v</ta>
            <ta e="T85" id="Seg_1447" s="T84">n</ta>
            <ta e="T86" id="Seg_1448" s="T85">pers</ta>
            <ta e="T87" id="Seg_1449" s="T86">v</ta>
            <ta e="T88" id="Seg_1450" s="T87">conj</ta>
            <ta e="T89" id="Seg_1451" s="T88">nprop</ta>
            <ta e="T90" id="Seg_1452" s="T89">v</ta>
            <ta e="T91" id="Seg_1453" s="T90">pers</ta>
            <ta e="T92" id="Seg_1454" s="T91">v</ta>
            <ta e="T93" id="Seg_1455" s="T92">nprop</ta>
            <ta e="T94" id="Seg_1456" s="T93">pers</ta>
            <ta e="T95" id="Seg_1457" s="T94">pers</ta>
            <ta e="T96" id="Seg_1458" s="T95">v</ta>
            <ta e="T97" id="Seg_1459" s="T96">v</ta>
            <ta e="T98" id="Seg_1460" s="T97">pers</ta>
            <ta e="T99" id="Seg_1461" s="T98">n</ta>
            <ta e="T100" id="Seg_1462" s="T99">ptcl</ta>
            <ta e="T101" id="Seg_1463" s="T100">v</ta>
            <ta e="T102" id="Seg_1464" s="T101">v</ta>
            <ta e="T103" id="Seg_1465" s="T102">n</ta>
            <ta e="T104" id="Seg_1466" s="T103">n</ta>
            <ta e="T105" id="Seg_1467" s="T104">n</ta>
            <ta e="T106" id="Seg_1468" s="T105">ptcl</ta>
            <ta e="T107" id="Seg_1469" s="T106">n</ta>
            <ta e="T108" id="Seg_1470" s="T107">conj</ta>
            <ta e="T109" id="Seg_1471" s="T108">v</ta>
            <ta e="T110" id="Seg_1472" s="T109">pers</ta>
            <ta e="T111" id="Seg_1473" s="T110">v</ta>
            <ta e="T112" id="Seg_1474" s="T111">pers</ta>
            <ta e="T113" id="Seg_1475" s="T112">adv</ta>
            <ta e="T114" id="Seg_1476" s="T113">n</ta>
            <ta e="T115" id="Seg_1477" s="T114">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_1478" s="T2">v:pred</ta>
            <ta e="T4" id="Seg_1479" s="T3">v:pred</ta>
            <ta e="T5" id="Seg_1480" s="T4">np.h:S</ta>
            <ta e="T8" id="Seg_1481" s="T7">v:pred</ta>
            <ta e="T10" id="Seg_1482" s="T9">np.h:S</ta>
            <ta e="T11" id="Seg_1483" s="T10">pro.h:S</ta>
            <ta e="T12" id="Seg_1484" s="T11">v:pred</ta>
            <ta e="T15" id="Seg_1485" s="T14">np.h:S</ta>
            <ta e="T16" id="Seg_1486" s="T15">v:pred</ta>
            <ta e="T18" id="Seg_1487" s="T17">0.3.h:S v:pred</ta>
            <ta e="T21" id="Seg_1488" s="T20">0.3.h:S v:pred</ta>
            <ta e="T22" id="Seg_1489" s="T21">pro.h:S</ta>
            <ta e="T23" id="Seg_1490" s="T22">s:temp</ta>
            <ta e="T24" id="Seg_1491" s="T23">v:pred</ta>
            <ta e="T27" id="Seg_1492" s="T26">0.1.h:S v:pred</ta>
            <ta e="T28" id="Seg_1493" s="T27">0.3.h:S v:pred</ta>
            <ta e="T29" id="Seg_1494" s="T28">np.h:S</ta>
            <ta e="T30" id="Seg_1495" s="T29">v:pred</ta>
            <ta e="T33" id="Seg_1496" s="T32">0.3.h:S v:pred</ta>
            <ta e="T35" id="Seg_1497" s="T34">0.3.h:S v:pred</ta>
            <ta e="T36" id="Seg_1498" s="T35">0.3.h:S v:pred</ta>
            <ta e="T37" id="Seg_1499" s="T36">0.3.h:S v:pred</ta>
            <ta e="T39" id="Seg_1500" s="T38">np.h:S</ta>
            <ta e="T40" id="Seg_1501" s="T39">v:pred</ta>
            <ta e="T43" id="Seg_1502" s="T42">s:adv</ta>
            <ta e="T44" id="Seg_1503" s="T43">0.3.h:S v:pred</ta>
            <ta e="T47" id="Seg_1504" s="T46">np:S</ta>
            <ta e="T49" id="Seg_1505" s="T48">v:pred</ta>
            <ta e="T51" id="Seg_1506" s="T50">0.3.h:S v:pred</ta>
            <ta e="T52" id="Seg_1507" s="T51">np:O</ta>
            <ta e="T54" id="Seg_1508" s="T53">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T55" id="Seg_1509" s="T54">0.3.h:S v:pred</ta>
            <ta e="T57" id="Seg_1510" s="T56">np:S</ta>
            <ta e="T60" id="Seg_1511" s="T59">v:pred</ta>
            <ta e="T61" id="Seg_1512" s="T60">v:pred</ta>
            <ta e="T62" id="Seg_1513" s="T61">np.h:S</ta>
            <ta e="T64" id="Seg_1514" s="T63">0.3.h:S v:pred</ta>
            <ta e="T67" id="Seg_1515" s="T64">s:purp</ta>
            <ta e="T68" id="Seg_1516" s="T67">0.3.h:S v:pred</ta>
            <ta e="T70" id="Seg_1517" s="T69">pro.h:S</ta>
            <ta e="T71" id="Seg_1518" s="T70">v:pred</ta>
            <ta e="T72" id="Seg_1519" s="T71">np:S</ta>
            <ta e="T74" id="Seg_1520" s="T73">v:pred</ta>
            <ta e="T75" id="Seg_1521" s="T74">0.3.h:S v:pred</ta>
            <ta e="T76" id="Seg_1522" s="T75">np:O</ta>
            <ta e="T78" id="Seg_1523" s="T77">np.h:S</ta>
            <ta e="T79" id="Seg_1524" s="T78">v:pred</ta>
            <ta e="T80" id="Seg_1525" s="T79">pro.h:S</ta>
            <ta e="T81" id="Seg_1526" s="T80">adv:pred</ta>
            <ta e="T82" id="Seg_1527" s="T81">np:S</ta>
            <ta e="T84" id="Seg_1528" s="T83">v:pred</ta>
            <ta e="T86" id="Seg_1529" s="T85">pro.h:S</ta>
            <ta e="T87" id="Seg_1530" s="T86">v:pred</ta>
            <ta e="T89" id="Seg_1531" s="T88">np.h:S</ta>
            <ta e="T90" id="Seg_1532" s="T89">v:pred</ta>
            <ta e="T91" id="Seg_1533" s="T90">pro.h:S</ta>
            <ta e="T92" id="Seg_1534" s="T91">v:pred</ta>
            <ta e="T94" id="Seg_1535" s="T93">pro.h:S</ta>
            <ta e="T95" id="Seg_1536" s="T94">s:purp</ta>
            <ta e="T96" id="Seg_1537" s="T95">v:pred</ta>
            <ta e="T97" id="Seg_1538" s="T96">s:purp</ta>
            <ta e="T99" id="Seg_1539" s="T98">np:S</ta>
            <ta e="T101" id="Seg_1540" s="T100">v:pred</ta>
            <ta e="T102" id="Seg_1541" s="T101">0.3:S v:pred</ta>
            <ta e="T104" id="Seg_1542" s="T103">0.1.h:S 0.3:O v:pred</ta>
            <ta e="T105" id="Seg_1543" s="T104">np:S</ta>
            <ta e="T107" id="Seg_1544" s="T106">v:pred</ta>
            <ta e="T109" id="Seg_1545" s="T108">0.1.h:S v:pred</ta>
            <ta e="T111" id="Seg_1546" s="T109">s:purp</ta>
            <ta e="T112" id="Seg_1547" s="T111">pro.h:S</ta>
            <ta e="T114" id="Seg_1548" s="T113">np:O</ta>
            <ta e="T115" id="Seg_1549" s="T114">v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T5" id="Seg_1550" s="T4">np.h:Th</ta>
            <ta e="T6" id="Seg_1551" s="T5">np.h:Com</ta>
            <ta e="T7" id="Seg_1552" s="T6">pro.h:Poss</ta>
            <ta e="T10" id="Seg_1553" s="T9">np.h:Th</ta>
            <ta e="T11" id="Seg_1554" s="T10">pro.h:A</ta>
            <ta e="T13" id="Seg_1555" s="T12">np:G</ta>
            <ta e="T15" id="Seg_1556" s="T14">np.h:Th 0.3.h:Poss</ta>
            <ta e="T17" id="Seg_1557" s="T16">np:L</ta>
            <ta e="T18" id="Seg_1558" s="T17">0.3.h:A</ta>
            <ta e="T21" id="Seg_1559" s="T20">0.3.h:Th</ta>
            <ta e="T22" id="Seg_1560" s="T21">pro.h:A</ta>
            <ta e="T25" id="Seg_1561" s="T24">np:Time</ta>
            <ta e="T27" id="Seg_1562" s="T26">0.1.h:A</ta>
            <ta e="T28" id="Seg_1563" s="T27">0.3.h:A</ta>
            <ta e="T29" id="Seg_1564" s="T28">np.h:Th</ta>
            <ta e="T31" id="Seg_1565" s="T30">np:Time</ta>
            <ta e="T33" id="Seg_1566" s="T32">0.3.h:A</ta>
            <ta e="T34" id="Seg_1567" s="T33">np:L</ta>
            <ta e="T35" id="Seg_1568" s="T34">0.3.h:A</ta>
            <ta e="T36" id="Seg_1569" s="T35">0.3.h:A</ta>
            <ta e="T37" id="Seg_1570" s="T36">0.3.h:A</ta>
            <ta e="T39" id="Seg_1571" s="T38">np.h:Th</ta>
            <ta e="T41" id="Seg_1572" s="T40">pro.h:Poss</ta>
            <ta e="T42" id="Seg_1573" s="T41">np:L</ta>
            <ta e="T44" id="Seg_1574" s="T43">0.3.h:A</ta>
            <ta e="T45" id="Seg_1575" s="T44">pro.h:Poss</ta>
            <ta e="T47" id="Seg_1576" s="T46">np:P</ta>
            <ta e="T50" id="Seg_1577" s="T49">np:Ins</ta>
            <ta e="T51" id="Seg_1578" s="T50">0.3.h:A</ta>
            <ta e="T52" id="Seg_1579" s="T51">np:Th</ta>
            <ta e="T53" id="Seg_1580" s="T52">np:Ins</ta>
            <ta e="T54" id="Seg_1581" s="T53">0.3.h:A 0.3:Th</ta>
            <ta e="T55" id="Seg_1582" s="T54">0.3.h:A</ta>
            <ta e="T57" id="Seg_1583" s="T56">np:P</ta>
            <ta e="T59" id="Seg_1584" s="T58">np:L</ta>
            <ta e="T62" id="Seg_1585" s="T61">np.h:E</ta>
            <ta e="T64" id="Seg_1586" s="T63">0.3.h:A</ta>
            <ta e="T66" id="Seg_1587" s="T65">np.h:P</ta>
            <ta e="T68" id="Seg_1588" s="T67">0.3.h:A</ta>
            <ta e="T70" id="Seg_1589" s="T69">pro.h:A</ta>
            <ta e="T72" id="Seg_1590" s="T71">np:Th</ta>
            <ta e="T75" id="Seg_1591" s="T74">0.3.h:A</ta>
            <ta e="T76" id="Seg_1592" s="T75">np:Th</ta>
            <ta e="T78" id="Seg_1593" s="T77">np.h:A</ta>
            <ta e="T80" id="Seg_1594" s="T79">pro.h:Th</ta>
            <ta e="T82" id="Seg_1595" s="T81">np:Th</ta>
            <ta e="T86" id="Seg_1596" s="T85">pro.h:A</ta>
            <ta e="T89" id="Seg_1597" s="T88">np.h:A</ta>
            <ta e="T91" id="Seg_1598" s="T90">pro.h:A</ta>
            <ta e="T94" id="Seg_1599" s="T93">pro.h:A</ta>
            <ta e="T95" id="Seg_1600" s="T94">pro.h:P</ta>
            <ta e="T98" id="Seg_1601" s="T97">pro.h:Poss</ta>
            <ta e="T99" id="Seg_1602" s="T98">np:Th</ta>
            <ta e="T102" id="Seg_1603" s="T101">0.3:P</ta>
            <ta e="T103" id="Seg_1604" s="T102">np:Ins</ta>
            <ta e="T104" id="Seg_1605" s="T103">0.1.h:A 0.3:Th</ta>
            <ta e="T105" id="Seg_1606" s="T104">np:P</ta>
            <ta e="T109" id="Seg_1607" s="T108">0.1.h:A</ta>
            <ta e="T110" id="Seg_1608" s="T109">pro.h:P</ta>
            <ta e="T112" id="Seg_1609" s="T111">pro.h:A</ta>
            <ta e="T114" id="Seg_1610" s="T113">np:Th</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T5" id="Seg_1611" s="T4">new</ta>
            <ta e="T6" id="Seg_1612" s="T5">new</ta>
            <ta e="T10" id="Seg_1613" s="T9">new</ta>
            <ta e="T11" id="Seg_1614" s="T10">accs-agg</ta>
            <ta e="T13" id="Seg_1615" s="T12">accs-gen</ta>
            <ta e="T15" id="Seg_1616" s="T14">giv-inactive</ta>
            <ta e="T17" id="Seg_1617" s="T16">accs-inf</ta>
            <ta e="T18" id="Seg_1618" s="T17">0.giv-inactive</ta>
            <ta e="T19" id="Seg_1619" s="T18">giv-active</ta>
            <ta e="T22" id="Seg_1620" s="T21">giv-active-Q</ta>
            <ta e="T25" id="Seg_1621" s="T24">accs-sit-Q</ta>
            <ta e="T27" id="Seg_1622" s="T26">0.giv-active-Q</ta>
            <ta e="T28" id="Seg_1623" s="T27">0.giv-active</ta>
            <ta e="T29" id="Seg_1624" s="T28">giv-inactive</ta>
            <ta e="T31" id="Seg_1625" s="T30">accs-sit</ta>
            <ta e="T33" id="Seg_1626" s="T32">0.giv-active</ta>
            <ta e="T34" id="Seg_1627" s="T33">giv-inactive</ta>
            <ta e="T35" id="Seg_1628" s="T34">0.giv-active</ta>
            <ta e="T36" id="Seg_1629" s="T35">0.giv-active</ta>
            <ta e="T37" id="Seg_1630" s="T36">0.giv-active</ta>
            <ta e="T39" id="Seg_1631" s="T38">accs-gen</ta>
            <ta e="T42" id="Seg_1632" s="T41">new</ta>
            <ta e="T44" id="Seg_1633" s="T43">0.giv-active</ta>
            <ta e="T47" id="Seg_1634" s="T46"> accs-inf-Q</ta>
            <ta e="T50" id="Seg_1635" s="T49">new</ta>
            <ta e="T51" id="Seg_1636" s="T50">0.giv-active</ta>
            <ta e="T52" id="Seg_1637" s="T51">giv-active</ta>
            <ta e="T53" id="Seg_1638" s="T52">giv-active</ta>
            <ta e="T54" id="Seg_1639" s="T53">0.giv-active 0.giv-active</ta>
            <ta e="T55" id="Seg_1640" s="T54">0.giv-active</ta>
            <ta e="T57" id="Seg_1641" s="T56">giv-active</ta>
            <ta e="T59" id="Seg_1642" s="T58">giv-active</ta>
            <ta e="T62" id="Seg_1643" s="T61">giv-active</ta>
            <ta e="T64" id="Seg_1644" s="T63">0.giv-active</ta>
            <ta e="T66" id="Seg_1645" s="T65">giv-inactive</ta>
            <ta e="T68" id="Seg_1646" s="T67">0.giv-active</ta>
            <ta e="T70" id="Seg_1647" s="T69">giv-inactive</ta>
            <ta e="T74" id="Seg_1648" s="T73">0.giv-active</ta>
            <ta e="T76" id="Seg_1649" s="T75">accs-sit</ta>
            <ta e="T78" id="Seg_1650" s="T77">accs-sit</ta>
            <ta e="T82" id="Seg_1651" s="T81">accs-gen</ta>
            <ta e="T86" id="Seg_1652" s="T85">giv-inactive-Q</ta>
            <ta e="T89" id="Seg_1653" s="T88">giv-inactive</ta>
            <ta e="T91" id="Seg_1654" s="T90">giv-active-Q</ta>
            <ta e="T96" id="Seg_1655" s="T95">0.giv-active-Q</ta>
            <ta e="T99" id="Seg_1656" s="T98">0.giv-inactive-Q</ta>
            <ta e="T102" id="Seg_1657" s="T101">0.giv-active-Q</ta>
            <ta e="T103" id="Seg_1658" s="T102">0.giv-inactive-Q</ta>
            <ta e="T104" id="Seg_1659" s="T103">0.giv-active-Q</ta>
            <ta e="T105" id="Seg_1660" s="T104">0.giv-active-Q</ta>
            <ta e="T109" id="Seg_1661" s="T108">0.giv-active-Q</ta>
            <ta e="T110" id="Seg_1662" s="T109">giv-inactive-Q</ta>
            <ta e="T112" id="Seg_1663" s="T111">giv-active-Q</ta>
            <ta e="T114" id="Seg_1664" s="T113">giv-inactive-Q</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T38" id="Seg_1665" s="T37">RUS:gram</ta>
            <ta e="T56" id="Seg_1666" s="T55">RUS:gram</ta>
            <ta e="T63" id="Seg_1667" s="T62">RUS:gram</ta>
            <ta e="T69" id="Seg_1668" s="T68">RUS:gram</ta>
            <ta e="T88" id="Seg_1669" s="T87">RUS:gram</ta>
            <ta e="T108" id="Seg_1670" s="T107">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T2" id="Seg_1671" s="T1">Pönege.</ta>
            <ta e="T6" id="Seg_1672" s="T2">There lived an old woman with her man. ‎‎</ta>
            <ta e="T10" id="Seg_1673" s="T6">They have children.</ta>
            <ta e="T14" id="Seg_1674" s="T10">They went into the Taiga.</ta>
            <ta e="T17" id="Seg_1675" s="T14">Their children stayed at home.</ta>
            <ta e="T21" id="Seg_1676" s="T17">They instructed the children to be quiet.</ta>
            <ta e="T24" id="Seg_1677" s="T21">We will come back very late.</ta>
            <ta e="T27" id="Seg_1678" s="T24">Today we will go far away.</ta>
            <ta e="T28" id="Seg_1679" s="T27">They went.</ta>
            <ta e="T30" id="Seg_1680" s="T28">The children stayed alone.</ta>
            <ta e="T34" id="Seg_1681" s="T30">The whole day they ran around in the house.</ta>
            <ta e="T42" id="Seg_1682" s="T34">They danced, they sang songs, they beat each othe, and Pönege sat in his house.</ta>
            <ta e="T49" id="Seg_1683" s="T42">He sat and said: My head explodes.</ta>
            <ta e="T52" id="Seg_1684" s="T49">He will bandage his head with black cherry.</ta>
            <ta e="T54" id="Seg_1685" s="T52">He bandaged it with black cherry.</ta>
            <ta e="T60" id="Seg_1686" s="T54">He sat around and the black cherry broke on his head.</ta>
            <ta e="T67" id="Seg_1687" s="T60">Pönege got angry and went away to eat the children.</ta>
            <ta e="T71" id="Seg_1688" s="T67">He came and they laughed.</ta>
            <ta e="T74" id="Seg_1689" s="T71">They couldn't stop.</ta>
            <ta e="T76" id="Seg_1690" s="T74">He knocked at the door.</ta>
            <ta e="T79" id="Seg_1691" s="T76">The older sister went outside.</ta>
            <ta e="T81" id="Seg_1692" s="T79">Who is there?</ta>
            <ta e="T84" id="Seg_1693" s="T81">It got dark.</ta>
            <ta e="T87" id="Seg_1694" s="T84">Mother, are you coming?</ta>
            <ta e="T93" id="Seg_1695" s="T87">And Pönege said: I have come, Pönege.</ta>
            <ta e="T97" id="Seg_1696" s="T93">I come to eat you.</ta>
            <ta e="T102" id="Seg_1697" s="T97">I am dizzy, my head exploded.</ta>
            <ta e="T107" id="Seg_1698" s="T102">I have bandaged it with black cherry, the black cherry also broke.</ta>
            <ta e="T111" id="Seg_1699" s="T107">I come to eat you.</ta>
            <ta e="T115" id="Seg_1700" s="T111">You were very noise in the house.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T2" id="Seg_1701" s="T1">Pönege.</ta>
            <ta e="T6" id="Seg_1702" s="T2">‎‎Es war einmal eine alte Frau mit ihrem Mann.</ta>
            <ta e="T10" id="Seg_1703" s="T6">Sie hatten Kinder. </ta>
            <ta e="T14" id="Seg_1704" s="T10">Sie gingen beide in die Taiga.</ta>
            <ta e="T17" id="Seg_1705" s="T14">Die Kinder blieben zu Hause. </ta>
            <ta e="T21" id="Seg_1706" s="T17">Sie belehrten die Kinder, leise zu sein.</ta>
            <ta e="T24" id="Seg_1707" s="T21">Wir kommen sehr spät zurück. </ta>
            <ta e="T27" id="Seg_1708" s="T24">Heute gehen wir weit weg. </ta>
            <ta e="T28" id="Seg_1709" s="T27">Sie gingen. </ta>
            <ta e="T30" id="Seg_1710" s="T28">Die Kinder blieben allein. </ta>
            <ta e="T34" id="Seg_1711" s="T30">Sie liefen den ganzen Tag im Haus herum.</ta>
            <ta e="T42" id="Seg_1712" s="T34">Sie tanzten, sangen Lieder, prügeln einander, und Pönege saß in seinem Haus.</ta>
            <ta e="T49" id="Seg_1713" s="T42">Er sitzt und sagt: „Mein Kopf zerbricht.“ </ta>
            <ta e="T52" id="Seg_1714" s="T49">Er band um seinen Kopf eine Rute der Traubenkirsche.</ta>
            <ta e="T54" id="Seg_1715" s="T52">Er umband ihn mit der Rute der Traubenkirsche.</ta>
            <ta e="T60" id="Seg_1716" s="T54">Er saß und von der Rute der Traubenkirsche zerbrach sein Kopf auch.</ta>
            <ta e="T67" id="Seg_1717" s="T60">Pönege wurde böse und ging kleine Dinge (Kinder) fressen. </ta>
            <ta e="T71" id="Seg_1718" s="T67">Er kam und sie lachten.</ta>
            <ta e="T74" id="Seg_1719" s="T71">Sie verloren den Verstand, hörten nicht auf zu lachen.</ta>
            <ta e="T76" id="Seg_1720" s="T74">Er klopfte an die Tür.</ta>
            <ta e="T79" id="Seg_1721" s="T76">Die ältere Schwester ging [zur Tür].</ta>
            <ta e="T81" id="Seg_1722" s="T79">Wer ist da? </ta>
            <ta e="T84" id="Seg_1723" s="T81">Es ist dunkel geworden.</ta>
            <ta e="T87" id="Seg_1724" s="T84">Mutter, vermutlich seid ihr es? </ta>
            <ta e="T93" id="Seg_1725" s="T87">Und Pönege sagte: „Ich bin gekommen, Pönege.</ta>
            <ta e="T97" id="Seg_1726" s="T93">Ich kam, um euch zu fressen.</ta>
            <ta e="T102" id="Seg_1727" s="T97">Ich habe Schwindel, mein Kopf zerbricht. </ta>
            <ta e="T107" id="Seg_1728" s="T102">Er wurde mit der Rute des Faulbaums umbunden, die Rute des Faulbaums ist auch gerissen. </ta>
            <ta e="T111" id="Seg_1729" s="T107">Ich bin gekommen, um euch zu fressen. </ta>
            <ta e="T115" id="Seg_1730" s="T111">Ihr lärmt sehr im Haus». </ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T2" id="Seg_1731" s="T1">Пёнеге.</ta>
            <ta e="T6" id="Seg_1732" s="T2">Жили были старуха со стариком.</ta>
            <ta e="T10" id="Seg_1733" s="T6">У них были дети.</ta>
            <ta e="T14" id="Seg_1734" s="T10">Они двое уходили в тайгу. </ta>
            <ta e="T17" id="Seg_1735" s="T14">Дети оставались дома. </ta>
            <ta e="T21" id="Seg_1736" s="T17">Наказывали детям, чтобы смирно были (не шумели в избе). </ta>
            <ta e="T24" id="Seg_1737" s="T21">Мы очень поздно придём. </ta>
            <ta e="T27" id="Seg_1738" s="T24">Сегодня далеко мы пойдём. </ta>
            <ta e="T28" id="Seg_1739" s="T27">Ушли они. </ta>
            <ta e="T30" id="Seg_1740" s="T28">Дети остались. </ta>
            <ta e="T34" id="Seg_1741" s="T30">Весь день бегали в избе.</ta>
            <ta e="T42" id="Seg_1742" s="T34">Плясали, пели песни, дрались, а Пёнеге сидел в своём дому.</ta>
            <ta e="T49" id="Seg_1743" s="T42">Сидит и говорит: «Голова-то моя раскололась». </ta>
            <ta e="T52" id="Seg_1744" s="T49">Черёмуховой верёвкой обвязал голову. </ta>
            <ta e="T54" id="Seg_1745" s="T52">Черемуховой верёвкой обвязал. </ta>
            <ta e="T60" id="Seg_1746" s="T54">Сидел, и от черёмуховой верёвки тоже голова его раскалывалась. </ta>
            <ta e="T67" id="Seg_1747" s="T60">Рассердился Пёнеге и ушел ребятишек съесть. </ta>
            <ta e="T71" id="Seg_1748" s="T67">Пришел, а они смеялись. </ta>
            <ta e="T74" id="Seg_1749" s="T71">До сознания не доходит, ‎ хохотали без остановки (не прекращали).</ta>
            <ta e="T76" id="Seg_1750" s="T74">Застучался в дверь. </ta>
            <ta e="T79" id="Seg_1751" s="T76">Большая старшая сестра вышла. </ta>
            <ta e="T81" id="Seg_1752" s="T79">Кто там? </ta>
            <ta e="T84" id="Seg_1753" s="T81">Уже стало темно. </ta>
            <ta e="T87" id="Seg_1754" s="T84">Мать, наверно, вы пришли? </ta>
            <ta e="T93" id="Seg_1755" s="T87">А Пёнеге сказал: «Это я пришел, Пёнеге. </ta>
            <ta e="T97" id="Seg_1756" s="T93">Я вас пришел есть. </ta>
            <ta e="T102" id="Seg_1757" s="T97">Моя голова кружится, раскалывается.</ta>
            <ta e="T107" id="Seg_1758" s="T102">черемошниковой веревкой завязана была, черемошник тоже раскололся.</ta>
            <ta e="T111" id="Seg_1759" s="T107">И пришел я вас съесть. </ta>
            <ta e="T115" id="Seg_1760" s="T111">Вы шибко в избе шумите». </ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_1761" s="T2">жили были старуха со стариком</ta>
            <ta e="T10" id="Seg_1762" s="T6"> ‎‎у них были дети.</ta>
            <ta e="T14" id="Seg_1763" s="T10">они (двое) уходили в тайгу.</ta>
            <ta e="T17" id="Seg_1764" s="T14">дети оставались дома.</ta>
            <ta e="T21" id="Seg_1765" s="T17">аказывали (они двое) детям смирно были (не шумели в избе) </ta>
            <ta e="T24" id="Seg_1766" s="T21">мы очень поздно (вечером) придем.</ta>
            <ta e="T27" id="Seg_1767" s="T24">сегодня далеко мы пойдём.</ta>
            <ta e="T28" id="Seg_1768" s="T27">уходили (они дв)</ta>
            <ta e="T30" id="Seg_1769" s="T28">ети оставались</ta>
            <ta e="T34" id="Seg_1770" s="T30">весь день бегали в избе.</ta>
            <ta e="T42" id="Seg_1771" s="T34">плясали пели песни дрались а Пöнеге сидел в своем дому.</ta>
            <ta e="T49" id="Seg_1772" s="T42">сидит и говорит голова-то моя раскололась.</ta>
            <ta e="T52" id="Seg_1773" s="T49">черемуховой верёвкой обвязал голову.</ta>
            <ta e="T54" id="Seg_1774" s="T52">черемуховой обвязал. </ta>
            <ta e="T60" id="Seg_1775" s="T54">сидел, и от черемуховой веревки тоже голова его раскалывалась. </ta>
            <ta e="T67" id="Seg_1776" s="T60">рассердился П. ушел ребятишек съесть. </ta>
            <ta e="T71" id="Seg_1777" s="T67"> ‎‎пришел они смеялись. </ta>
            <ta e="T74" id="Seg_1778" s="T71">до сознания не доходят, ‎ хохотали без остановки (не прекращали) останавливая.</ta>
            <ta e="T76" id="Seg_1779" s="T74">застучался в дверь.</ta>
            <ta e="T79" id="Seg_1780" s="T76">старшая сестра вышла</ta>
            <ta e="T81" id="Seg_1781" s="T79">кто там</ta>
            <ta e="T84" id="Seg_1782" s="T81"> ‎‎уже стало темно</ta>
            <ta e="T87" id="Seg_1783" s="T84">мать наверно вы пришли?</ta>
            <ta e="T93" id="Seg_1784" s="T87">П. сказал это я пришел Пöнеге</ta>
            <ta e="T97" id="Seg_1785" s="T93">я вас пришел есть</ta>
            <ta e="T102" id="Seg_1786" s="T97">моя голова кружится, раскалывается.</ta>
            <ta e="T107" id="Seg_1787" s="T102">Черемошниковой верёвкой завязана была, черемошник тоже раскололся.</ta>
            <ta e="T111" id="Seg_1788" s="T107"> ‎‎пришел я вас съесть.</ta>
            <ta e="T115" id="Seg_1789" s="T111">вы шибко в избе шумите.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
