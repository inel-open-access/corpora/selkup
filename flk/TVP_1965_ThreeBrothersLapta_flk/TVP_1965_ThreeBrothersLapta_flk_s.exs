<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>TVP_1965_ThreeBrothersLaptyla_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">TVP_1965_ThreeBrothersLapta_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1079</ud-information>
            <ud-information attribute-name="# HIAT:w">922</ud-information>
            <ud-information attribute-name="# e">923</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">1</ud-information>
            <ud-information attribute-name="# HIAT:u">126</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="TVP">
            <abbreviation>TVP</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
         <tli id="T204" />
         <tli id="T205" />
         <tli id="T206" />
         <tli id="T207" />
         <tli id="T208" />
         <tli id="T209" />
         <tli id="T210" />
         <tli id="T211" />
         <tli id="T212" />
         <tli id="T213" />
         <tli id="T214" />
         <tli id="T215" />
         <tli id="T216" />
         <tli id="T217" />
         <tli id="T218" />
         <tli id="T219" />
         <tli id="T220" />
         <tli id="T221" />
         <tli id="T222" />
         <tli id="T223" />
         <tli id="T224" />
         <tli id="T225" />
         <tli id="T226" />
         <tli id="T227" />
         <tli id="T228" />
         <tli id="T229" />
         <tli id="T230" />
         <tli id="T231" />
         <tli id="T232" />
         <tli id="T233" />
         <tli id="T234" />
         <tli id="T235" />
         <tli id="T236" />
         <tli id="T237" />
         <tli id="T238" />
         <tli id="T239" />
         <tli id="T240" />
         <tli id="T241" />
         <tli id="T242" />
         <tli id="T243" />
         <tli id="T244" />
         <tli id="T245" />
         <tli id="T246" />
         <tli id="T247" />
         <tli id="T248" />
         <tli id="T249" />
         <tli id="T250" />
         <tli id="T251" />
         <tli id="T252" />
         <tli id="T253" />
         <tli id="T254" />
         <tli id="T255" />
         <tli id="T256" />
         <tli id="T257" />
         <tli id="T258" />
         <tli id="T259" />
         <tli id="T260" />
         <tli id="T261" />
         <tli id="T262" />
         <tli id="T263" />
         <tli id="T264" />
         <tli id="T265" />
         <tli id="T266" />
         <tli id="T267" />
         <tli id="T268" />
         <tli id="T269" />
         <tli id="T270" />
         <tli id="T271" />
         <tli id="T272" />
         <tli id="T273" />
         <tli id="T274" />
         <tli id="T275" />
         <tli id="T276" />
         <tli id="T277" />
         <tli id="T278" />
         <tli id="T279" />
         <tli id="T280" />
         <tli id="T281" />
         <tli id="T282" />
         <tli id="T283" />
         <tli id="T284" />
         <tli id="T285" />
         <tli id="T286" />
         <tli id="T287" />
         <tli id="T288" />
         <tli id="T289" />
         <tli id="T290" />
         <tli id="T291" />
         <tli id="T292" />
         <tli id="T293" />
         <tli id="T294" />
         <tli id="T295" />
         <tli id="T296" />
         <tli id="T297" />
         <tli id="T298" />
         <tli id="T299" />
         <tli id="T300" />
         <tli id="T301" />
         <tli id="T302" />
         <tli id="T303" />
         <tli id="T304" />
         <tli id="T305" />
         <tli id="T306" />
         <tli id="T307" />
         <tli id="T308" />
         <tli id="T309" />
         <tli id="T310" />
         <tli id="T311" />
         <tli id="T312" />
         <tli id="T313" />
         <tli id="T314" />
         <tli id="T315" />
         <tli id="T316" />
         <tli id="T317" />
         <tli id="T318" />
         <tli id="T319" />
         <tli id="T320" />
         <tli id="T321" />
         <tli id="T322" />
         <tli id="T323" />
         <tli id="T324" />
         <tli id="T325" />
         <tli id="T326" />
         <tli id="T327" />
         <tli id="T328" />
         <tli id="T329" />
         <tli id="T330" />
         <tli id="T331" />
         <tli id="T332" />
         <tli id="T333" />
         <tli id="T334" />
         <tli id="T335" />
         <tli id="T336" />
         <tli id="T337" />
         <tli id="T338" />
         <tli id="T339" />
         <tli id="T340" />
         <tli id="T341" />
         <tli id="T342" />
         <tli id="T343" />
         <tli id="T344" />
         <tli id="T345" />
         <tli id="T346" />
         <tli id="T347" />
         <tli id="T348" />
         <tli id="T349" />
         <tli id="T350" />
         <tli id="T351" />
         <tli id="T352" />
         <tli id="T353" />
         <tli id="T354" />
         <tli id="T355" />
         <tli id="T356" />
         <tli id="T357" />
         <tli id="T358" />
         <tli id="T359" />
         <tli id="T360" />
         <tli id="T361" />
         <tli id="T362" />
         <tli id="T363" />
         <tli id="T364" />
         <tli id="T365" />
         <tli id="T366" />
         <tli id="T367" />
         <tli id="T368" />
         <tli id="T369" />
         <tli id="T370" />
         <tli id="T371" />
         <tli id="T372" />
         <tli id="T373" />
         <tli id="T374" />
         <tli id="T375" />
         <tli id="T376" />
         <tli id="T377" />
         <tli id="T378" />
         <tli id="T379" />
         <tli id="T380" />
         <tli id="T381" />
         <tli id="T382" />
         <tli id="T383" />
         <tli id="T384" />
         <tli id="T385" />
         <tli id="T386" />
         <tli id="T387" />
         <tli id="T388" />
         <tli id="T389" />
         <tli id="T390" />
         <tli id="T391" />
         <tli id="T392" />
         <tli id="T393" />
         <tli id="T394" />
         <tli id="T395" />
         <tli id="T396" />
         <tli id="T397" />
         <tli id="T398" />
         <tli id="T399" />
         <tli id="T400" />
         <tli id="T401" />
         <tli id="T402" />
         <tli id="T403" />
         <tli id="T404" />
         <tli id="T405" />
         <tli id="T406" />
         <tli id="T407" />
         <tli id="T408" />
         <tli id="T409" />
         <tli id="T410" />
         <tli id="T411" />
         <tli id="T412" />
         <tli id="T413" />
         <tli id="T414" />
         <tli id="T415" />
         <tli id="T416" />
         <tli id="T417" />
         <tli id="T418" />
         <tli id="T419" />
         <tli id="T420" />
         <tli id="T421" />
         <tli id="T422" />
         <tli id="T423" />
         <tli id="T424" />
         <tli id="T425" />
         <tli id="T426" />
         <tli id="T427" />
         <tli id="T428" />
         <tli id="T429" />
         <tli id="T430" />
         <tli id="T431" />
         <tli id="T432" />
         <tli id="T433" />
         <tli id="T434" />
         <tli id="T435" />
         <tli id="T436" />
         <tli id="T437" />
         <tli id="T438" />
         <tli id="T439" />
         <tli id="T440" />
         <tli id="T441" />
         <tli id="T442" />
         <tli id="T443" />
         <tli id="T444" />
         <tli id="T445" />
         <tli id="T446" />
         <tli id="T447" />
         <tli id="T448" />
         <tli id="T449" />
         <tli id="T450" />
         <tli id="T451" />
         <tli id="T452" />
         <tli id="T453" />
         <tli id="T454" />
         <tli id="T455" />
         <tli id="T456" />
         <tli id="T457" />
         <tli id="T458" />
         <tli id="T459" />
         <tli id="T460" />
         <tli id="T461" />
         <tli id="T462" />
         <tli id="T463" />
         <tli id="T464" />
         <tli id="T465" />
         <tli id="T466" />
         <tli id="T467" />
         <tli id="T468" />
         <tli id="T469" />
         <tli id="T470" />
         <tli id="T471" />
         <tli id="T472" />
         <tli id="T473" />
         <tli id="T474" />
         <tli id="T475" />
         <tli id="T476" />
         <tli id="T477" />
         <tli id="T478" />
         <tli id="T479" />
         <tli id="T480" />
         <tli id="T481" />
         <tli id="T482" />
         <tli id="T483" />
         <tli id="T484" />
         <tli id="T485" />
         <tli id="T486" />
         <tli id="T487" />
         <tli id="T488" />
         <tli id="T489" />
         <tli id="T490" />
         <tli id="T491" />
         <tli id="T492" />
         <tli id="T493" />
         <tli id="T494" />
         <tli id="T495" />
         <tli id="T496" />
         <tli id="T497" />
         <tli id="T498" />
         <tli id="T499" />
         <tli id="T500" />
         <tli id="T501" />
         <tli id="T502" />
         <tli id="T503" />
         <tli id="T504" />
         <tli id="T505" />
         <tli id="T506" />
         <tli id="T507" />
         <tli id="T508" />
         <tli id="T509" />
         <tli id="T510" />
         <tli id="T511" />
         <tli id="T512" />
         <tli id="T513" />
         <tli id="T514" />
         <tli id="T515" />
         <tli id="T516" />
         <tli id="T517" />
         <tli id="T518" />
         <tli id="T519" />
         <tli id="T520" />
         <tli id="T521" />
         <tli id="T522" />
         <tli id="T523" />
         <tli id="T524" />
         <tli id="T525" />
         <tli id="T526" />
         <tli id="T527" />
         <tli id="T528" />
         <tli id="T529" />
         <tli id="T530" />
         <tli id="T531" />
         <tli id="T532" />
         <tli id="T533" />
         <tli id="T534" />
         <tli id="T535" />
         <tli id="T536" />
         <tli id="T537" />
         <tli id="T538" />
         <tli id="T539" />
         <tli id="T540" />
         <tli id="T541" />
         <tli id="T542" />
         <tli id="T543" />
         <tli id="T544" />
         <tli id="T545" />
         <tli id="T546" />
         <tli id="T547" />
         <tli id="T548" />
         <tli id="T549" />
         <tli id="T550" />
         <tli id="T551" />
         <tli id="T552" />
         <tli id="T553" />
         <tli id="T554" />
         <tli id="T555" />
         <tli id="T556" />
         <tli id="T557" />
         <tli id="T558" />
         <tli id="T559" />
         <tli id="T560" />
         <tli id="T561" />
         <tli id="T562" />
         <tli id="T563" />
         <tli id="T564" />
         <tli id="T565" />
         <tli id="T566" />
         <tli id="T567" />
         <tli id="T568" />
         <tli id="T569" />
         <tli id="T570" />
         <tli id="T571" />
         <tli id="T572" />
         <tli id="T573" />
         <tli id="T574" />
         <tli id="T575" />
         <tli id="T576" />
         <tli id="T577" />
         <tli id="T578" />
         <tli id="T579" />
         <tli id="T580" />
         <tli id="T581" />
         <tli id="T582" />
         <tli id="T583" />
         <tli id="T584" />
         <tli id="T585" />
         <tli id="T586" />
         <tli id="T587" />
         <tli id="T588" />
         <tli id="T589" />
         <tli id="T590" />
         <tli id="T591" />
         <tli id="T592" />
         <tli id="T593" />
         <tli id="T594" />
         <tli id="T595" />
         <tli id="T596" />
         <tli id="T597" />
         <tli id="T598" />
         <tli id="T599" />
         <tli id="T600" />
         <tli id="T601" />
         <tli id="T602" />
         <tli id="T603" />
         <tli id="T604" />
         <tli id="T605" />
         <tli id="T606" />
         <tli id="T607" />
         <tli id="T608" />
         <tli id="T609" />
         <tli id="T610" />
         <tli id="T611" />
         <tli id="T612" />
         <tli id="T613" />
         <tli id="T614" />
         <tli id="T615" />
         <tli id="T616" />
         <tli id="T617" />
         <tli id="T618" />
         <tli id="T619" />
         <tli id="T620" />
         <tli id="T621" />
         <tli id="T622" />
         <tli id="T623" />
         <tli id="T624" />
         <tli id="T625" />
         <tli id="T626" />
         <tli id="T627" />
         <tli id="T628" />
         <tli id="T629" />
         <tli id="T630" />
         <tli id="T631" />
         <tli id="T632" />
         <tli id="T633" />
         <tli id="T634" />
         <tli id="T635" />
         <tli id="T636" />
         <tli id="T637" />
         <tli id="T638" />
         <tli id="T639" />
         <tli id="T640" />
         <tli id="T641" />
         <tli id="T642" />
         <tli id="T643" />
         <tli id="T644" />
         <tli id="T645" />
         <tli id="T646" />
         <tli id="T647" />
         <tli id="T648" />
         <tli id="T649" />
         <tli id="T650" />
         <tli id="T651" />
         <tli id="T652" />
         <tli id="T653" />
         <tli id="T654" />
         <tli id="T655" />
         <tli id="T656" />
         <tli id="T657" />
         <tli id="T658" />
         <tli id="T659" />
         <tli id="T660" />
         <tli id="T661" />
         <tli id="T662" />
         <tli id="T663" />
         <tli id="T664" />
         <tli id="T665" />
         <tli id="T666" />
         <tli id="T667" />
         <tli id="T668" />
         <tli id="T669" />
         <tli id="T670" />
         <tli id="T671" />
         <tli id="T672" />
         <tli id="T673" />
         <tli id="T674" />
         <tli id="T675" />
         <tli id="T676" />
         <tli id="T677" />
         <tli id="T678" />
         <tli id="T679" />
         <tli id="T680" />
         <tli id="T681" />
         <tli id="T682" />
         <tli id="T683" />
         <tli id="T684" />
         <tli id="T685" />
         <tli id="T686" />
         <tli id="T687" />
         <tli id="T688" />
         <tli id="T689" />
         <tli id="T690" />
         <tli id="T691" />
         <tli id="T692" />
         <tli id="T693" />
         <tli id="T694" />
         <tli id="T695" />
         <tli id="T696" />
         <tli id="T697" />
         <tli id="T698" />
         <tli id="T699" />
         <tli id="T700" />
         <tli id="T701" />
         <tli id="T702" />
         <tli id="T703" />
         <tli id="T704" />
         <tli id="T705" />
         <tli id="T706" />
         <tli id="T707" />
         <tli id="T708" />
         <tli id="T709" />
         <tli id="T710" />
         <tli id="T711" />
         <tli id="T712" />
         <tli id="T713" />
         <tli id="T714" />
         <tli id="T715" />
         <tli id="T716" />
         <tli id="T717" />
         <tli id="T718" />
         <tli id="T719" />
         <tli id="T720" />
         <tli id="T721" />
         <tli id="T722" />
         <tli id="T723" />
         <tli id="T724" />
         <tli id="T725" />
         <tli id="T726" />
         <tli id="T727" />
         <tli id="T728" />
         <tli id="T729" />
         <tli id="T730" />
         <tli id="T731" />
         <tli id="T732" />
         <tli id="T733" />
         <tli id="T734" />
         <tli id="T735" />
         <tli id="T736" />
         <tli id="T737" />
         <tli id="T738" />
         <tli id="T739" />
         <tli id="T740" />
         <tli id="T741" />
         <tli id="T742" />
         <tli id="T743" />
         <tli id="T744" />
         <tli id="T745" />
         <tli id="T746" />
         <tli id="T747" />
         <tli id="T748" />
         <tli id="T749" />
         <tli id="T750" />
         <tli id="T751" />
         <tli id="T752" />
         <tli id="T753" />
         <tli id="T754" />
         <tli id="T755" />
         <tli id="T756" />
         <tli id="T757" />
         <tli id="T758" />
         <tli id="T759" />
         <tli id="T760" />
         <tli id="T761" />
         <tli id="T762" />
         <tli id="T763" />
         <tli id="T764" />
         <tli id="T765" />
         <tli id="T766" />
         <tli id="T767" />
         <tli id="T768" />
         <tli id="T769" />
         <tli id="T770" />
         <tli id="T771" />
         <tli id="T772" />
         <tli id="T773" />
         <tli id="T774" />
         <tli id="T775" />
         <tli id="T776" />
         <tli id="T777" />
         <tli id="T778" />
         <tli id="T779" />
         <tli id="T780" />
         <tli id="T781" />
         <tli id="T782" />
         <tli id="T783" />
         <tli id="T784" />
         <tli id="T785" />
         <tli id="T786" />
         <tli id="T787" />
         <tli id="T788" />
         <tli id="T789" />
         <tli id="T790" />
         <tli id="T791" />
         <tli id="T792" />
         <tli id="T793" />
         <tli id="T794" />
         <tli id="T795" />
         <tli id="T796" />
         <tli id="T797" />
         <tli id="T798" />
         <tli id="T799" />
         <tli id="T800" />
         <tli id="T801" />
         <tli id="T802" />
         <tli id="T803" />
         <tli id="T804" />
         <tli id="T805" />
         <tli id="T806" />
         <tli id="T807" />
         <tli id="T808" />
         <tli id="T809" />
         <tli id="T810" />
         <tli id="T811" />
         <tli id="T812" />
         <tli id="T813" />
         <tli id="T814" />
         <tli id="T815" />
         <tli id="T816" />
         <tli id="T817" />
         <tli id="T818" />
         <tli id="T819" />
         <tli id="T820" />
         <tli id="T821" />
         <tli id="T822" />
         <tli id="T823" />
         <tli id="T824" />
         <tli id="T825" />
         <tli id="T826" />
         <tli id="T827" />
         <tli id="T828" />
         <tli id="T829" />
         <tli id="T830" />
         <tli id="T831" />
         <tli id="T832" />
         <tli id="T833" />
         <tli id="T834" />
         <tli id="T835" />
         <tli id="T836" />
         <tli id="T837" />
         <tli id="T838" />
         <tli id="T839" />
         <tli id="T840" />
         <tli id="T841" />
         <tli id="T842" />
         <tli id="T843" />
         <tli id="T844" />
         <tli id="T845" />
         <tli id="T846" />
         <tli id="T847" />
         <tli id="T848" />
         <tli id="T849" />
         <tli id="T850" />
         <tli id="T851" />
         <tli id="T852" />
         <tli id="T853" />
         <tli id="T854" />
         <tli id="T855" />
         <tli id="T856" />
         <tli id="T857" />
         <tli id="T858" />
         <tli id="T859" />
         <tli id="T860" />
         <tli id="T861" />
         <tli id="T862" />
         <tli id="T863" />
         <tli id="T864" />
         <tli id="T865" />
         <tli id="T866" />
         <tli id="T867" />
         <tli id="T868" />
         <tli id="T869" />
         <tli id="T870" />
         <tli id="T871" />
         <tli id="T872" />
         <tli id="T873" />
         <tli id="T874" />
         <tli id="T875" />
         <tli id="T876" />
         <tli id="T877" />
         <tli id="T878" />
         <tli id="T879" />
         <tli id="T880" />
         <tli id="T881" />
         <tli id="T882" />
         <tli id="T883" />
         <tli id="T884" />
         <tli id="T885" />
         <tli id="T886" />
         <tli id="T887" />
         <tli id="T888" />
         <tli id="T889" />
         <tli id="T890" />
         <tli id="T891" />
         <tli id="T892" />
         <tli id="T893" />
         <tli id="T894" />
         <tli id="T895" />
         <tli id="T896" />
         <tli id="T897" />
         <tli id="T898" />
         <tli id="T899" />
         <tli id="T900" />
         <tli id="T901" />
         <tli id="T902" />
         <tli id="T903" />
         <tli id="T904" />
         <tli id="T905" />
         <tli id="T906" />
         <tli id="T907" />
         <tli id="T908" />
         <tli id="T909" />
         <tli id="T910" />
         <tli id="T911" />
         <tli id="T912" />
         <tli id="T913" />
         <tli id="T914" />
         <tli id="T915" />
         <tli id="T916" />
         <tli id="T917" />
         <tli id="T918" />
         <tli id="T919" />
         <tli id="T920" />
         <tli id="T921" />
         <tli id="T922" />
         <tli id="T923" />
         <tli id="T924" />
         <tli id="T925" />
         <tli id="T926" />
         <tli id="T927" />
         <tli id="T928" />
         <tli id="T929" />
         <tli id="T930" />
         <tli id="T931" />
         <tli id="T932" />
         <tli id="T933" />
         <tli id="T934" />
         <tli id="T935" />
         <tli id="T936" />
         <tli id="T937" />
         <tli id="T938" />
         <tli id="T939" />
         <tli id="T940" />
         <tli id="T941" />
         <tli id="T942" />
         <tli id="T943" />
         <tli id="T944" />
         <tli id="T945" />
         <tli id="T946" />
         <tli id="T947" />
         <tli id="T948" />
         <tli id="T949" />
         <tli id="T950" />
         <tli id="T951" />
         <tli id="T952" />
         <tli id="T953" />
         <tli id="T954" />
         <tli id="T955" />
         <tli id="T956" />
         <tli id="T957" />
         <tli id="T958" />
         <tli id="T959" />
         <tli id="T960" />
         <tli id="T961" />
         <tli id="T962" />
         <tli id="T963" />
         <tli id="T964" />
         <tli id="T965" />
         <tli id="T966" />
         <tli id="T967" />
         <tli id="T968" />
         <tli id="T969" />
         <tli id="T970" />
         <tli id="T971" />
         <tli id="T972" />
         <tli id="T973" />
         <tli id="T974" />
         <tli id="T975" />
         <tli id="T976" />
         <tli id="T977" />
         <tli id="T978" />
         <tli id="T979" />
         <tli id="T980" />
         <tli id="T981" />
         <tli id="T982" />
         <tli id="T983" />
         <tli id="T984" />
         <tli id="T985" />
         <tli id="T986" />
         <tli id="T987" />
         <tli id="T988" />
         <tli id="T989" />
         <tli id="T990" />
         <tli id="T991" />
         <tli id="T992" />
         <tli id="T993" />
         <tli id="T994" />
         <tli id="T995" />
         <tli id="T996" />
         <tli id="T997" />
         <tli id="T998" />
         <tli id="T999" />
         <tli id="T1000" />
         <tli id="T1001" />
         <tli id="T1002" />
         <tli id="T1003" />
         <tli id="T1004" />
         <tli id="T1005" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="TVP"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T1005" id="Seg_0" n="sc" s="T82">
               <ts e="T85" id="Seg_2" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_4" n="HIAT:w" s="T82">Laptɨlʼ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_7" n="HIAT:w" s="T83">nɔːkur</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_10" n="HIAT:w" s="T84">timnʼäsɨtɨ</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_14" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_16" n="HIAT:w" s="T85">Šite</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_19" n="HIAT:w" s="T86">ämäsäqä</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_22" n="HIAT:w" s="T87">ilɨmmɨntɔː</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_26" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_28" n="HIAT:w" s="T88">Ijatɨ</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_31" n="HIAT:w" s="T89">nɔːtɨ</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_34" n="HIAT:w" s="T90">qumtɨšak</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_37" n="HIAT:w" s="T91">orɨmpa</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T99" id="Seg_41" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_43" n="HIAT:w" s="T92">Iːjatɨ</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_46" n="HIAT:w" s="T93">namɨm</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_49" n="HIAT:w" s="T94">aša</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_52" n="HIAT:w" s="T95">tɛnɨmɨmpat</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_55" n="HIAT:w" s="T96">äsɨtɨ</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_58" n="HIAT:w" s="T97">kušat</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_61" n="HIAT:w" s="T98">qumpa</ts>
                  <nts id="Seg_62" n="HIAT:ip">.</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_65" n="HIAT:u" s="T99">
                  <ts e="T100" id="Seg_67" n="HIAT:w" s="T99">Ukkur</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_70" n="HIAT:w" s="T100">tapčʼeːlʼi</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_73" n="HIAT:w" s="T101">iːja</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_76" n="HIAT:w" s="T102">əmɨntɨnɨk</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_79" n="HIAT:w" s="T103">nalʼat</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_82" n="HIAT:w" s="T104">ɛsa</ts>
                  <nts id="Seg_83" n="HIAT:ip">.</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_86" n="HIAT:u" s="T105">
                  <ts e="T106" id="Seg_88" n="HIAT:w" s="T105">Ama</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_91" n="HIAT:w" s="T106">man</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_94" n="HIAT:w" s="T107">äsanɨ</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_97" n="HIAT:w" s="T108">kurampɨlʼ</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_100" n="HIAT:w" s="T109">ɔːtatɨ</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_103" n="HIAT:w" s="T110">kuni</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_106" n="HIAT:w" s="T111">mäkkä</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_109" n="HIAT:w" s="T112">atɨltätɨ</ts>
                  <nts id="Seg_110" n="HIAT:ip">.</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_113" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_115" n="HIAT:w" s="T113">A</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_118" n="HIAT:w" s="T114">ɔːtatɨj</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_121" n="HIAT:w" s="T115">əmäsɨqa</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_124" n="HIAT:w" s="T116">nɔːnɨ</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_126" n="HIAT:ip">(</nts>
                  <ts e="T118" id="Seg_128" n="HIAT:w" s="T117">nanɨrɨk</ts>
                  <nts id="Seg_129" n="HIAT:ip">)</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_132" n="HIAT:w" s="T118">ɛppa</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_134" n="HIAT:ip">–</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_137" n="HIAT:w" s="T119">tətɨntɨsa</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_140" n="HIAT:w" s="T120">ampatɨ</ts>
                  <nts id="Seg_141" n="HIAT:ip">,</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_144" n="HIAT:w" s="T121">a</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_147" n="HIAT:w" s="T122">ɔːtantij</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_150" n="HIAT:w" s="T123">nılʼčʼilʼ</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_153" n="HIAT:w" s="T124">tɔːqaštij</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_156" n="HIAT:w" s="T125">ɛppɨntɨ</ts>
                  <nts id="Seg_157" n="HIAT:ip">.</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_160" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_162" n="HIAT:w" s="T126">Nʼuːqotɨ</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_165" n="HIAT:w" s="T127">qəːlɨt</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_168" n="HIAT:w" s="T128">qopɨtɨ</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_171" n="HIAT:w" s="T129">kuraktɨ</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_174" n="HIAT:w" s="T130">ɛppa</ts>
                  <nts id="Seg_175" n="HIAT:ip">.</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T137" id="Seg_178" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_180" n="HIAT:w" s="T131">Namɨmtɨ</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_183" n="HIAT:w" s="T132">karnʼaltila</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_186" n="HIAT:w" s="T133">ɔːtamtip</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_189" n="HIAT:w" s="T134">nılʼčʼik</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_192" n="HIAT:w" s="T135">moqona</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_195" n="HIAT:w" s="T136">tɔːqqɨqolʼimɨmpatij</ts>
                  <nts id="Seg_196" n="HIAT:ip">.</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T140" id="Seg_199" n="HIAT:u" s="T137">
                  <ts e="T138" id="Seg_201" n="HIAT:w" s="T137">Əmɨtɨ</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_204" n="HIAT:w" s="T138">naːlʼät</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_207" n="HIAT:w" s="T139">ɛsa</ts>
                  <nts id="Seg_208" n="HIAT:ip">.</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_211" n="HIAT:u" s="T140">
                  <ts e="T141" id="Seg_213" n="HIAT:w" s="T140">Tan</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_216" n="HIAT:w" s="T141">ɔːtasa</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_219" n="HIAT:w" s="T142">kučʼa</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_222" n="HIAT:w" s="T143">qənnantɨ</ts>
                  <nts id="Seg_223" n="HIAT:ip">?</nts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T147" id="Seg_226" n="HIAT:u" s="T144">
                  <ts e="T145" id="Seg_228" n="HIAT:w" s="T144">Ija</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_231" n="HIAT:w" s="T145">naːlʼat</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_234" n="HIAT:w" s="T146">ɛsa</ts>
                  <nts id="Seg_235" n="HIAT:ip">.</nts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_238" n="HIAT:u" s="T147">
                  <ts e="T148" id="Seg_240" n="HIAT:w" s="T147">Man</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_243" n="HIAT:w" s="T148">nʼi</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_246" n="HIAT:w" s="T149">kučʼatij</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_249" n="HIAT:w" s="T150">äša</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_252" n="HIAT:w" s="T151">qəntak</ts>
                  <nts id="Seg_253" n="HIAT:ip">.</nts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T155" id="Seg_256" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_258" n="HIAT:w" s="T152">Mat</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_261" n="HIAT:w" s="T153">tap</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_264" n="HIAT:w" s="T154">puršɨmɔːtänoːqa</ts>
                  <nts id="Seg_265" n="HIAT:ip">.</nts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T158" id="Seg_268" n="HIAT:u" s="T155">
                  <ts e="T156" id="Seg_270" n="HIAT:w" s="T155">Karrät</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_273" n="HIAT:w" s="T156">nʼärot</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_276" n="HIAT:w" s="T157">qoltɛntak</ts>
                  <nts id="Seg_277" n="HIAT:ip">.</nts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_280" n="HIAT:u" s="T158">
                  <ts e="T159" id="Seg_282" n="HIAT:w" s="T158">Əmɨtɨ</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_285" n="HIAT:w" s="T159">aša</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_288" n="HIAT:w" s="T160">qatɨ</ts>
                  <nts id="Seg_289" n="HIAT:ip">.</nts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T178" id="Seg_292" n="HIAT:u" s="T161">
                  <ts e="T162" id="Seg_294" n="HIAT:w" s="T161">Qaːlʼimɨt</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_297" n="HIAT:w" s="T162">puːtot</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_300" n="HIAT:w" s="T163">taršältɨlä</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_303" n="HIAT:w" s="T164">nʼamtɨ</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_306" n="HIAT:w" s="T165">čʼarɨlʼ</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_309" n="HIAT:w" s="T166">ɔːtalʼ</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_312" n="HIAT:w" s="T167">orasilʼ</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_315" n="HIAT:w" s="T168">tɛːmtɨ</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_318" n="HIAT:w" s="T169">ılla</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_321" n="HIAT:w" s="T170">iːntɨtɨ</ts>
                  <nts id="Seg_322" n="HIAT:ip">,</nts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_325" n="HIAT:w" s="T171">montɨ</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_328" n="HIAT:w" s="T172">mɨta</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_331" n="HIAT:w" s="T173">poːlʼ</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_334" n="HIAT:w" s="T174">kalat</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_337" n="HIAT:w" s="T175">paqqət</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_340" n="HIAT:w" s="T176">čʼarɨm</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_343" n="HIAT:w" s="T177">eŋa</ts>
                  <nts id="Seg_344" n="HIAT:ip">.</nts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T181" id="Seg_347" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_349" n="HIAT:w" s="T178">Iːjanɨk</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_352" n="HIAT:w" s="T179">nılʼčʼɨŋä</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_355" n="HIAT:w" s="T180">pɨŋtalʼtɨmmɨntɨ</ts>
                  <nts id="Seg_356" n="HIAT:ip">.</nts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T190" id="Seg_359" n="HIAT:u" s="T181">
                  <ts e="T182" id="Seg_361" n="HIAT:w" s="T181">Karrä</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_364" n="HIAT:w" s="T182">tap</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_367" n="HIAT:w" s="T183">ɔːtäntɨ</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_370" n="HIAT:w" s="T184">puːtoːqɨt</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_373" n="HIAT:w" s="T185">nɔːkɨr</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_376" n="HIAT:w" s="T186">pekɨralʼ</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_379" n="HIAT:w" s="T187">qoptɨ</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_382" n="HIAT:w" s="T188">na</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_385" n="HIAT:w" s="T189">ɛːntɨ</ts>
                  <nts id="Seg_386" n="HIAT:ip">.</nts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T198" id="Seg_389" n="HIAT:u" s="T190">
                  <ts e="T191" id="Seg_391" n="HIAT:w" s="T190">Ukkur</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_394" n="HIAT:w" s="T191">pekɨralʼ</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_397" n="HIAT:w" s="T192">qoptɨ</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_400" n="HIAT:w" s="T193">na</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_403" n="HIAT:w" s="T194">ɛːntɨ</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_406" n="HIAT:w" s="T195">wärqɨlɔːqɨlʼ</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_409" n="HIAT:w" s="T196">nam</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_412" n="HIAT:w" s="T197">orqoltɨ</ts>
                  <nts id="Seg_413" n="HIAT:ip">.</nts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T204" id="Seg_416" n="HIAT:u" s="T198">
                  <ts e="T199" id="Seg_418" n="HIAT:w" s="T198">Toːnna</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_421" n="HIAT:w" s="T199">mäːqıj</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_424" n="HIAT:w" s="T200">ontıːj</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_427" n="HIAT:w" s="T201">əːtɨla</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_430" n="HIAT:w" s="T202">na</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_433" n="HIAT:w" s="T203">tünɨntɔːqıj</ts>
                  <nts id="Seg_434" n="HIAT:ip">.</nts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T210" id="Seg_437" n="HIAT:u" s="T204">
                  <ts e="T205" id="Seg_439" n="HIAT:w" s="T204">Ija</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_442" n="HIAT:w" s="T205">tɛːmnɨmtɨ</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_445" n="HIAT:w" s="T206">kəš</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_448" n="HIAT:w" s="T207">mɨnto</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_451" n="HIAT:w" s="T208">na</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_454" n="HIAT:w" s="T209">totqɨlpɨntɨtɨ</ts>
                  <nts id="Seg_455" n="HIAT:ip">.</nts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T214" id="Seg_458" n="HIAT:u" s="T210">
                  <ts e="T211" id="Seg_460" n="HIAT:w" s="T210">Nılʼčʼɨk</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_463" n="HIAT:w" s="T211">karrä</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_466" n="HIAT:w" s="T212">nä</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_469" n="HIAT:w" s="T213">qənmmɨntɨ</ts>
                  <nts id="Seg_470" n="HIAT:ip">.</nts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T223" id="Seg_473" n="HIAT:u" s="T214">
                  <ts e="T215" id="Seg_475" n="HIAT:w" s="T214">Ɔːtantɨ</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_478" n="HIAT:w" s="T215">puːtot</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_481" n="HIAT:w" s="T216">peːkɨlla</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_484" n="HIAT:w" s="T217">tına</ts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_487" n="HIAT:w" s="T218">əmɨntɨ</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_490" n="HIAT:w" s="T219">tompɨlʼ</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_493" n="HIAT:w" s="T220">qoptɨp</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_496" n="HIAT:w" s="T221">na</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_499" n="HIAT:w" s="T222">orqɨlʼpantɨt</ts>
                  <nts id="Seg_500" n="HIAT:ip">.</nts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T226" id="Seg_503" n="HIAT:u" s="T223">
                  <ts e="T224" id="Seg_505" n="HIAT:w" s="T223">Konna</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_508" n="HIAT:w" s="T224">tatɨŋɨtɨ</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_511" n="HIAT:w" s="T225">sɔːralqə</ts>
                  <nts id="Seg_512" n="HIAT:ip">.</nts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T243" id="Seg_515" n="HIAT:u" s="T226">
                  <ts e="T227" id="Seg_517" n="HIAT:w" s="T226">Nat</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_520" n="HIAT:w" s="T227">kuntə</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_523" n="HIAT:w" s="T228">əmɨtɨ</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_526" n="HIAT:w" s="T229">munte</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_529" n="HIAT:w" s="T230">mɨta</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_532" n="HIAT:w" s="T231">qalɨtɨt</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_535" n="HIAT:w" s="T232">puːtoːqɨt</ts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_538" n="HIAT:w" s="T233">nılʼčʼi</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_541" n="HIAT:w" s="T234">qallɨ</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_544" n="HIAT:w" s="T235">ılla</ts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_547" n="HIAT:w" s="T236">iːmmɨntɨtɨ</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_550" n="HIAT:w" s="T237">mɨnta</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_553" n="HIAT:w" s="T238">mɨta</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_556" n="HIAT:w" s="T239">nɔssarɨlʼ</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_559" n="HIAT:w" s="T240">košar</ts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_562" n="HIAT:w" s="T241">nɔːmtɨlʼ</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_565" n="HIAT:w" s="T242">topɨtä</ts>
                  <nts id="Seg_566" n="HIAT:ip">.</nts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T247" id="Seg_569" n="HIAT:u" s="T243">
                  <ts e="T244" id="Seg_571" n="HIAT:w" s="T243">Ija</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_574" n="HIAT:w" s="T244">qaːlɨmtɨ</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_577" n="HIAT:w" s="T245">na</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_580" n="HIAT:w" s="T246">saralqulammɨntɨ</ts>
                  <nts id="Seg_581" n="HIAT:ip">.</nts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T261" id="Seg_584" n="HIAT:u" s="T247">
                  <ts e="T248" id="Seg_586" n="HIAT:w" s="T247">Munta</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_589" n="HIAT:w" s="T248">mɨta</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_592" n="HIAT:w" s="T249">tına</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_595" n="HIAT:w" s="T250">šittɨ</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_598" n="HIAT:w" s="T251">pekaralʼ</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_601" n="HIAT:w" s="T252">kɨpalɔːlʼ</ts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_604" n="HIAT:w" s="T253">qoptoːqıt</ts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_607" n="HIAT:w" s="T254">ontıː</ts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_610" n="HIAT:w" s="T255">əːtɨla</ts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_613" n="HIAT:w" s="T256">montɨ</ts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_616" n="HIAT:w" s="T257">mɨta</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_619" n="HIAT:w" s="T258">konna</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_622" n="HIAT:w" s="T259">na</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_625" n="HIAT:w" s="T260">tüːmmɨntɔː</ts>
                  <nts id="Seg_626" n="HIAT:ip">.</nts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T266" id="Seg_629" n="HIAT:u" s="T261">
                  <ts e="T262" id="Seg_631" n="HIAT:w" s="T261">Ija</ts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_634" n="HIAT:w" s="T262">qalɨmtɨ</ts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_637" n="HIAT:w" s="T263">sarala</ts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_640" n="HIAT:w" s="T264">na</ts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_643" n="HIAT:w" s="T265">tuːrtɨmmɨntɨt</ts>
                  <nts id="Seg_644" n="HIAT:ip">.</nts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T274" id="Seg_647" n="HIAT:u" s="T266">
                  <ts e="T267" id="Seg_649" n="HIAT:w" s="T266">Əmɨtɨ</ts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_652" n="HIAT:w" s="T267">seːlʼčʼi</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_655" n="HIAT:w" s="T268">kočʼenɨlʼ</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_658" n="HIAT:w" s="T269">košar</ts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_661" n="HIAT:w" s="T270">nɔːmtelʼ</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_664" n="HIAT:w" s="T271">naraposa</ts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_667" n="HIAT:w" s="T272">na</ts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_670" n="HIAT:w" s="T273">miːnmɨntɨtɨ</ts>
                  <nts id="Seg_671" n="HIAT:ip">.</nts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T280" id="Seg_674" n="HIAT:u" s="T274">
                  <ts e="T275" id="Seg_676" n="HIAT:w" s="T274">Pekɨralʼ</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_679" n="HIAT:w" s="T275">maːtɨrla</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_682" n="HIAT:w" s="T276">meːmpɨlʼ</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_685" n="HIAT:w" s="T277">soqqɨsa</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_688" n="HIAT:w" s="T278">na</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_691" n="HIAT:w" s="T279">tokalaltɨmmɨtɨt</ts>
                  <nts id="Seg_692" n="HIAT:ip">.</nts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T287" id="Seg_695" n="HIAT:u" s="T280">
                  <ts e="T281" id="Seg_697" n="HIAT:w" s="T280">Šentɨ</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_700" n="HIAT:w" s="T281">maːtɨrla</ts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_703" n="HIAT:w" s="T282">meːmpɨlʼ</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_706" n="HIAT:w" s="T283">qälilʼ</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_709" n="HIAT:w" s="T284">kulusa</ts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_712" n="HIAT:w" s="T285">na</ts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_715" n="HIAT:w" s="T286">tokalaltɨmmɨntɨt</ts>
                  <nts id="Seg_716" n="HIAT:ip">.</nts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T295" id="Seg_719" n="HIAT:u" s="T287">
                  <ts e="T288" id="Seg_721" n="HIAT:w" s="T287">Nɨːnɨ</ts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_724" n="HIAT:w" s="T288">əmɨtɨ</ts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_727" n="HIAT:w" s="T289">nılʼčʼiŋa</ts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_730" n="HIAT:w" s="T290">soqošpɨntɨtɨ</ts>
                  <nts id="Seg_731" n="HIAT:ip">:</nts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_734" n="HIAT:w" s="T291">ɔːtantɨ</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_737" n="HIAT:w" s="T292">olɨ</ts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_740" n="HIAT:w" s="T293">ɨkɨ</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_743" n="HIAT:w" s="T294">laqɨrätɨ</ts>
                  <nts id="Seg_744" n="HIAT:ip">.</nts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T303" id="Seg_747" n="HIAT:u" s="T295">
                  <ts e="T296" id="Seg_749" n="HIAT:w" s="T295">Ontɨt</ts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_752" n="HIAT:w" s="T296">našte</ts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_755" n="HIAT:w" s="T297">totalʼ</ts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_758" n="HIAT:w" s="T298">tukɛntɔːtɨt</ts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_761" n="HIAT:w" s="T299">ɛj</ts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_764" n="HIAT:w" s="T300">moqanä</ts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_767" n="HIAT:w" s="T301">našti</ts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_770" n="HIAT:w" s="T302">taːtɛntɔːtɨt</ts>
                  <nts id="Seg_771" n="HIAT:ip">.</nts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T313" id="Seg_774" n="HIAT:u" s="T303">
                  <ts e="T304" id="Seg_776" n="HIAT:w" s="T303">Ija</ts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_779" n="HIAT:w" s="T304">ɔːqaltɨ</ts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_782" n="HIAT:w" s="T305">tartaltɨlä</ts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_785" n="HIAT:w" s="T306">ınna</ts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_788" n="HIAT:w" s="T307">omtɨlʼa</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_791" n="HIAT:w" s="T308">ɔːtantɨ</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_794" n="HIAT:w" s="T309">olɨp</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_797" n="HIAT:w" s="T310">nʼena</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_800" n="HIAT:w" s="T311">na</ts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_803" n="HIAT:w" s="T312">üːtɨmmɨntɨ</ts>
                  <nts id="Seg_804" n="HIAT:ip">.</nts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T321" id="Seg_807" n="HIAT:u" s="T313">
                  <ts e="T314" id="Seg_809" n="HIAT:w" s="T313">Nɨːnɨ</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_812" n="HIAT:w" s="T314">nʼennɨ</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_815" n="HIAT:w" s="T315">laqaltiptäːqɨntɨ</ts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_818" n="HIAT:w" s="T316">qaj</ts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_821" n="HIAT:w" s="T317">čʼumpɨk</ts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_824" n="HIAT:w" s="T318">qänɨmpa</ts>
                  <nts id="Seg_825" n="HIAT:ip">,</nts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_828" n="HIAT:w" s="T319">qaj</ts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_831" n="HIAT:w" s="T320">qɔːmɨčʼäk</ts>
                  <nts id="Seg_832" n="HIAT:ip">.</nts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T328" id="Seg_835" n="HIAT:u" s="T321">
                  <ts e="T322" id="Seg_837" n="HIAT:w" s="T321">Ukkur</ts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_840" n="HIAT:w" s="T322">nʼarqɨlʼ</ts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_843" n="HIAT:w" s="T323">kotpas</ts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_846" n="HIAT:w" s="T324">pɔːrɨntɨ</ts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_849" n="HIAT:w" s="T325">na</ts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_852" n="HIAT:w" s="T326">omtij</ts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_855" n="HIAT:w" s="T327">ɛːmmɨntɨ</ts>
                  <nts id="Seg_856" n="HIAT:ip">.</nts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T341" id="Seg_859" n="HIAT:u" s="T328">
                  <ts e="T329" id="Seg_861" n="HIAT:w" s="T328">Kotpas</ts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_864" n="HIAT:w" s="T329">pɔːroːqot</ts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_867" n="HIAT:w" s="T330">ɔːmtɨla</ts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_870" n="HIAT:w" s="T331">nılʼčʼik</ts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_873" n="HIAT:w" s="T332">qoŋotɨ</ts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_876" n="HIAT:w" s="T333">montɨ</ts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_879" n="HIAT:w" s="T334">mɨta</ts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_882" n="HIAT:w" s="T335">karrät</ts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_885" n="HIAT:w" s="T336">toːt</ts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_888" n="HIAT:w" s="T337">qanaqqɨt</ts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_891" n="HIAT:w" s="T338">nɔːkɨr</ts>
                  <nts id="Seg_892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_894" n="HIAT:w" s="T339">mɔːt</ts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_897" n="HIAT:w" s="T340">ɔːmnontɨ</ts>
                  <nts id="Seg_898" n="HIAT:ip">.</nts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T346" id="Seg_901" n="HIAT:u" s="T341">
                  <ts e="T342" id="Seg_903" n="HIAT:w" s="T341">Na</ts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_906" n="HIAT:w" s="T342">mɔːtoqanaqɨt</ts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_909" n="HIAT:w" s="T343">qälʼi</ts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_912" n="HIAT:w" s="T344">ɔːtä</ts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_915" n="HIAT:w" s="T345">kuralɨmmɨntɔːtɨt</ts>
                  <nts id="Seg_916" n="HIAT:ip">.</nts>
                  <nts id="Seg_917" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T352" id="Seg_919" n="HIAT:u" s="T346">
                  <ts e="T347" id="Seg_921" n="HIAT:w" s="T346">Ija</ts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_924" n="HIAT:w" s="T347">aša</ts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_927" n="HIAT:w" s="T348">qata</ts>
                  <nts id="Seg_928" n="HIAT:ip">,</nts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_931" n="HIAT:w" s="T349">mɔːtta</ts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_934" n="HIAT:w" s="T350">karala</ts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_937" n="HIAT:w" s="T351">qaltɛlʼčʼe</ts>
                  <nts id="Seg_938" n="HIAT:ip">.</nts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T359" id="Seg_941" n="HIAT:u" s="T352">
                  <ts e="T353" id="Seg_943" n="HIAT:w" s="T352">Čʼontoːqɨntɨ</ts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_946" n="HIAT:w" s="T353">onti</ts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_949" n="HIAT:w" s="T354">mɨta</ts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_952" n="HIAT:w" s="T355">seːpɨlak</ts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_955" n="HIAT:w" s="T356">wärqa</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_958" n="HIAT:w" s="T357">mɔːt</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_961" n="HIAT:w" s="T358">ɔːmnɨntɨ</ts>
                  <nts id="Seg_962" n="HIAT:ip">.</nts>
                  <nts id="Seg_963" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T365" id="Seg_965" n="HIAT:u" s="T359">
                  <ts e="T360" id="Seg_967" n="HIAT:w" s="T359">Na</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_970" n="HIAT:w" s="T360">wärqɨ</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_973" n="HIAT:w" s="T361">mɔːtanoːt</ts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_976" n="HIAT:w" s="T362">čʼɔːtɨ</ts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_979" n="HIAT:w" s="T363">ılla</ts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_982" n="HIAT:w" s="T364">uterɛlʼčʼiŋɨtɨ</ts>
                  <nts id="Seg_983" n="HIAT:ip">.</nts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T370" id="Seg_986" n="HIAT:u" s="T365">
                  <ts e="T366" id="Seg_988" n="HIAT:w" s="T365">İlla</ts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_991" n="HIAT:w" s="T366">uterɛːmpɨla</ts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_994" n="HIAT:w" s="T367">qalintɨ</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_997" n="HIAT:w" s="T368">šünʼčʼoːqɨt</ts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1000" n="HIAT:w" s="T369">ɔːmttä</ts>
                  <nts id="Seg_1001" n="HIAT:ip">.</nts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T375" id="Seg_1004" n="HIAT:u" s="T370">
                  <ts e="T371" id="Seg_1006" n="HIAT:w" s="T370">Mɔːtqɨnɨ</ts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1009" n="HIAT:w" s="T371">ponä</ts>
                  <nts id="Seg_1010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1012" n="HIAT:w" s="T372">qumɨt</ts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1015" n="HIAT:w" s="T373">nʼeːja</ts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1018" n="HIAT:w" s="T374">tattɨmmɨntɔːtɨt</ts>
                  <nts id="Seg_1019" n="HIAT:ip">.</nts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T383" id="Seg_1022" n="HIAT:u" s="T375">
                  <ts e="T376" id="Seg_1024" n="HIAT:w" s="T375">Ukkur</ts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1027" n="HIAT:w" s="T376">ira</ts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1030" n="HIAT:w" s="T377">montɨ</ts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1033" n="HIAT:w" s="T378">mɨt</ts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1036" n="HIAT:w" s="T379">ijanɨk</ts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1039" n="HIAT:w" s="T380">tüːla</ts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1042" n="HIAT:w" s="T381">naːlʼät</ts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1045" n="HIAT:w" s="T382">ɛsɨmmɨntɨ</ts>
                  <nts id="Seg_1046" n="HIAT:ip">.</nts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T387" id="Seg_1049" n="HIAT:u" s="T383">
                  <ts e="T384" id="Seg_1051" n="HIAT:w" s="T383">Qaːlʼ</ts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1054" n="HIAT:w" s="T384">tɨtɨt</ts>
                  <nts id="Seg_1055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1057" n="HIAT:w" s="T385">qumantɨ</ts>
                  <nts id="Seg_1058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1060" n="HIAT:w" s="T386">ɛsantɨ</ts>
                  <nts id="Seg_1061" n="HIAT:ip">?</nts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T399" id="Seg_1064" n="HIAT:u" s="T387">
                  <ts e="T388" id="Seg_1066" n="HIAT:w" s="T387">Iːja</ts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1069" n="HIAT:w" s="T388">nälʼät</ts>
                  <nts id="Seg_1070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1072" n="HIAT:w" s="T389">ɛsɨmmɨntɨ</ts>
                  <nts id="Seg_1073" n="HIAT:ip">:</nts>
                  <nts id="Seg_1074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1076" n="HIAT:w" s="T390">Man</ts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1079" n="HIAT:w" s="T391">nʼe</ts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1082" n="HIAT:w" s="T392">qumɨm</ts>
                  <nts id="Seg_1083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1085" n="HIAT:w" s="T393">aša</ts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1088" n="HIAT:w" s="T394">tɛnʼimap</ts>
                  <nts id="Seg_1089" n="HIAT:ip">,</nts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1092" n="HIAT:w" s="T395">nʼe</ts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1095" n="HIAT:w" s="T396">qaim</ts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1098" n="HIAT:w" s="T397">aša</ts>
                  <nts id="Seg_1099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1101" n="HIAT:w" s="T398">tɛnimap</ts>
                  <nts id="Seg_1102" n="HIAT:ip">.</nts>
                  <nts id="Seg_1103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T403" id="Seg_1105" n="HIAT:u" s="T399">
                  <ts e="T400" id="Seg_1107" n="HIAT:w" s="T399">Pɛlʼikɔːl</ts>
                  <nts id="Seg_1108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1110" n="HIAT:w" s="T400">ilʼiptäːlʼ</ts>
                  <nts id="Seg_1111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1113" n="HIAT:w" s="T401">tätan</ts>
                  <nts id="Seg_1114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1116" n="HIAT:w" s="T402">nɔːnɨ</ts>
                  <nts id="Seg_1117" n="HIAT:ip">.</nts>
                  <nts id="Seg_1118" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T409" id="Seg_1120" n="HIAT:u" s="T403">
                  <ts e="T404" id="Seg_1122" n="HIAT:w" s="T403">A</ts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1125" n="HIAT:w" s="T404">tan</ts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1128" n="HIAT:w" s="T405">mɨta</ts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1131" n="HIAT:w" s="T406">qaj</ts>
                  <nts id="Seg_1132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1134" n="HIAT:w" s="T407">iraŋɨnta</ts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1137" n="HIAT:w" s="T408">ɛsantä</ts>
                  <nts id="Seg_1138" n="HIAT:ip">.</nts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T413" id="Seg_1141" n="HIAT:u" s="T409">
                  <ts e="T410" id="Seg_1143" n="HIAT:w" s="T409">Na</ts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1146" n="HIAT:w" s="T410">iːra</ts>
                  <nts id="Seg_1147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1149" n="HIAT:w" s="T411">nılʼčʼik</ts>
                  <nts id="Seg_1150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1152" n="HIAT:w" s="T412">kətiŋɨtɨ</ts>
                  <nts id="Seg_1153" n="HIAT:ip">.</nts>
                  <nts id="Seg_1154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T424" id="Seg_1156" n="HIAT:u" s="T413">
                  <ts e="T414" id="Seg_1158" n="HIAT:w" s="T413">Meː</ts>
                  <nts id="Seg_1159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1161" n="HIAT:w" s="T414">mɨta</ts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1164" n="HIAT:w" s="T415">nɔːkur</ts>
                  <nts id="Seg_1165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1167" n="HIAT:w" s="T416">sartätalʼ</ts>
                  <nts id="Seg_1168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1170" n="HIAT:w" s="T417">tɨmnʼäsɨmɨt</ts>
                  <nts id="Seg_1171" n="HIAT:ip">,</nts>
                  <nts id="Seg_1172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1174" n="HIAT:w" s="T418">poːsɨ</ts>
                  <nts id="Seg_1175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1177" n="HIAT:w" s="T419">warqɨ</ts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1180" n="HIAT:w" s="T420">sartətta</ts>
                  <nts id="Seg_1181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1183" n="HIAT:w" s="T421">nɨk</ts>
                  <nts id="Seg_1184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1186" n="HIAT:w" s="T422">mat</ts>
                  <nts id="Seg_1187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1189" n="HIAT:w" s="T423">ɛːmmintak</ts>
                  <nts id="Seg_1190" n="HIAT:ip">.</nts>
                  <nts id="Seg_1191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T432" id="Seg_1193" n="HIAT:u" s="T424">
                  <ts e="T425" id="Seg_1195" n="HIAT:w" s="T424">Tına</ts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1198" n="HIAT:w" s="T425">pona</ts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1201" n="HIAT:w" s="T426">tannɨmpɨlʼ</ts>
                  <nts id="Seg_1202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1204" n="HIAT:w" s="T427">qumɨt</ts>
                  <nts id="Seg_1205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1207" n="HIAT:w" s="T428">montɨ</ts>
                  <nts id="Seg_1208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1210" n="HIAT:w" s="T429">mɨta</ts>
                  <nts id="Seg_1211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1213" n="HIAT:w" s="T430">ɔːtalʼ</ts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1216" n="HIAT:w" s="T431">orattɔːtɨt</ts>
                  <nts id="Seg_1217" n="HIAT:ip">.</nts>
                  <nts id="Seg_1218" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T441" id="Seg_1220" n="HIAT:u" s="T432">
                  <ts e="T433" id="Seg_1222" n="HIAT:w" s="T432">Na</ts>
                  <nts id="Seg_1223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1225" n="HIAT:w" s="T433">ɔːtatɨt</ts>
                  <nts id="Seg_1226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1228" n="HIAT:w" s="T434">puːtot</ts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1231" n="HIAT:w" s="T435">ukkur</ts>
                  <nts id="Seg_1232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1234" n="HIAT:w" s="T436">ɔːmtɨkɨtɨlʼ</ts>
                  <nts id="Seg_1235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1237" n="HIAT:w" s="T437">qälʼilʼ</ts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1240" n="HIAT:w" s="T438">waqtalʼ</ts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1243" n="HIAT:w" s="T439">čʼaktɨ</ts>
                  <nts id="Seg_1244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1246" n="HIAT:w" s="T440">orantɔːtɨt</ts>
                  <nts id="Seg_1247" n="HIAT:ip">.</nts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T446" id="Seg_1250" n="HIAT:u" s="T441">
                  <ts e="T442" id="Seg_1252" n="HIAT:w" s="T441">Nılʼčʼik</ts>
                  <nts id="Seg_1253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1255" n="HIAT:w" s="T442">ija</ts>
                  <nts id="Seg_1256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1258" n="HIAT:w" s="T443">mannɨmpatɨ</ts>
                  <nts id="Seg_1259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1261" n="HIAT:w" s="T444">orqɨlqa</ts>
                  <nts id="Seg_1262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1264" n="HIAT:w" s="T445">tačʼalpɔːtɨt</ts>
                  <nts id="Seg_1265" n="HIAT:ip">.</nts>
                  <nts id="Seg_1266" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T459" id="Seg_1268" n="HIAT:u" s="T446">
                  <ts e="T447" id="Seg_1270" n="HIAT:w" s="T446">Ija</ts>
                  <nts id="Seg_1271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1273" n="HIAT:w" s="T447">qaːlɨntɨ</ts>
                  <nts id="Seg_1274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1276" n="HIAT:w" s="T448">šunʼčʼoːqɨt</ts>
                  <nts id="Seg_1277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1279" n="HIAT:w" s="T449">ɔːmtɨlʼä</ts>
                  <nts id="Seg_1280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1282" n="HIAT:w" s="T450">tɛːmnim</ts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1285" n="HIAT:w" s="T451">tɨnä</ts>
                  <nts id="Seg_1286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1288" n="HIAT:w" s="T452">totqɨlpɨntɨtäːnɨ</ts>
                  <nts id="Seg_1289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1291" n="HIAT:w" s="T453">čʼəktɨp</ts>
                  <nts id="Seg_1292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1294" n="HIAT:w" s="T454">ijat</ts>
                  <nts id="Seg_1295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1297" n="HIAT:w" s="T455">qanaŋmɨt</ts>
                  <nts id="Seg_1298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1300" n="HIAT:w" s="T456">na</ts>
                  <nts id="Seg_1301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1303" n="HIAT:w" s="T457">mintɨralʼ</ts>
                  <nts id="Seg_1304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1306" n="HIAT:w" s="T458">tämmintɔːtɨt</ts>
                  <nts id="Seg_1307" n="HIAT:ip">.</nts>
                  <nts id="Seg_1308" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T470" id="Seg_1310" n="HIAT:u" s="T459">
                  <ts e="T460" id="Seg_1312" n="HIAT:w" s="T459">Iːja</ts>
                  <nts id="Seg_1313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1315" n="HIAT:w" s="T460">čʼəktɨp</ts>
                  <nts id="Seg_1316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1318" n="HIAT:w" s="T461">tatɨraltɨlä</ts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1321" n="HIAT:w" s="T462">orqɨlpɨntɨlä</ts>
                  <nts id="Seg_1322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1324" n="HIAT:w" s="T463">tɛːmtɨ</ts>
                  <nts id="Seg_1325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1327" n="HIAT:w" s="T464">čʼam</ts>
                  <nts id="Seg_1328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1330" n="HIAT:w" s="T465">mišalpatɨ</ts>
                  <nts id="Seg_1331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1333" n="HIAT:w" s="T466">čʼəktɨt</ts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1336" n="HIAT:w" s="T467">püqa</ts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1339" n="HIAT:w" s="T468">toːnna</ts>
                  <nts id="Seg_1340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1342" n="HIAT:w" s="T469">seːpemmɨntɨ</ts>
                  <nts id="Seg_1343" n="HIAT:ip">.</nts>
                  <nts id="Seg_1344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T475" id="Seg_1346" n="HIAT:u" s="T470">
                  <ts e="T471" id="Seg_1348" n="HIAT:w" s="T470">Ija</ts>
                  <nts id="Seg_1349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1351" n="HIAT:w" s="T471">tɛːmnimtɨ</ts>
                  <nts id="Seg_1352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1354" n="HIAT:w" s="T472">ınna</ts>
                  <nts id="Seg_1355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1357" n="HIAT:w" s="T473">na</ts>
                  <nts id="Seg_1358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1360" n="HIAT:w" s="T474">totqɨlpɨntɨ</ts>
                  <nts id="Seg_1361" n="HIAT:ip">.</nts>
                  <nts id="Seg_1362" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T483" id="Seg_1364" n="HIAT:u" s="T475">
                  <ts e="T476" id="Seg_1366" n="HIAT:w" s="T475">Nalʼät</ts>
                  <nts id="Seg_1367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1369" n="HIAT:w" s="T476">ɛsɨmpa</ts>
                  <nts id="Seg_1370" n="HIAT:ip">:</nts>
                  <nts id="Seg_1371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1373" n="HIAT:w" s="T477">Tap</ts>
                  <nts id="Seg_1374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1376" n="HIAT:w" s="T478">tɛːmtilʼ</ts>
                  <nts id="Seg_1377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1379" n="HIAT:w" s="T479">tomɨ</ts>
                  <nts id="Seg_1380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1382" n="HIAT:w" s="T480">kämsa</ts>
                  <nts id="Seg_1383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1385" n="HIAT:w" s="T481">qajqə</ts>
                  <nts id="Seg_1386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1388" n="HIAT:w" s="T482">neklɔːltap</ts>
                  <nts id="Seg_1389" n="HIAT:ip">?</nts>
                  <nts id="Seg_1390" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T488" id="Seg_1392" n="HIAT:u" s="T483">
                  <ts e="T484" id="Seg_1394" n="HIAT:w" s="T483">Na</ts>
                  <nts id="Seg_1395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1397" n="HIAT:w" s="T484">qumɨt</ts>
                  <nts id="Seg_1398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1400" n="HIAT:w" s="T485">ɔːtam</ts>
                  <nts id="Seg_1401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1403" n="HIAT:w" s="T486">tɨnä</ts>
                  <nts id="Seg_1404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1406" n="HIAT:w" s="T487">kırammintɔːtɨt</ts>
                  <nts id="Seg_1407" n="HIAT:ip">.</nts>
                  <nts id="Seg_1408" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T492" id="Seg_1410" n="HIAT:u" s="T488">
                  <ts e="T489" id="Seg_1412" n="HIAT:w" s="T488">Qälilʼ</ts>
                  <nts id="Seg_1413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1415" n="HIAT:w" s="T489">qumɨt</ts>
                  <nts id="Seg_1416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1418" n="HIAT:w" s="T490">poqonnä</ts>
                  <nts id="Seg_1419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1421" n="HIAT:w" s="T491">tuštiqolammɨntɔːtɨt</ts>
                  <nts id="Seg_1422" n="HIAT:ip">.</nts>
                  <nts id="Seg_1423" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T500" id="Seg_1425" n="HIAT:u" s="T492">
                  <ts e="T493" id="Seg_1427" n="HIAT:w" s="T492">Ija</ts>
                  <nts id="Seg_1428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1430" n="HIAT:w" s="T493">nälʼät</ts>
                  <nts id="Seg_1431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1433" n="HIAT:w" s="T494">na</ts>
                  <nts id="Seg_1434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1436" n="HIAT:w" s="T495">kuraltimmɨntɔːtɨt</ts>
                  <nts id="Seg_1437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1439" n="HIAT:w" s="T496">tat</ts>
                  <nts id="Seg_1440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1442" n="HIAT:w" s="T497">lʼa</ts>
                  <nts id="Seg_1443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1445" n="HIAT:w" s="T498">meːsʼa</ts>
                  <nts id="Seg_1446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1448" n="HIAT:w" s="T499">tuštäšik</ts>
                  <nts id="Seg_1449" n="HIAT:ip">.</nts>
                  <nts id="Seg_1450" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T508" id="Seg_1452" n="HIAT:u" s="T500">
                  <ts e="T501" id="Seg_1454" n="HIAT:w" s="T500">Ija</ts>
                  <nts id="Seg_1455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1457" n="HIAT:w" s="T501">nalʼat</ts>
                  <nts id="Seg_1458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1460" n="HIAT:w" s="T502">na</ts>
                  <nts id="Seg_1461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1463" n="HIAT:w" s="T503">ɛsɨmmɨntɨ</ts>
                  <nts id="Seg_1464" n="HIAT:ip">:</nts>
                  <nts id="Seg_1465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1467" n="HIAT:w" s="T504">Mat</ts>
                  <nts id="Seg_1468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1470" n="HIAT:w" s="T505">tuštɨqa</ts>
                  <nts id="Seg_1471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1473" n="HIAT:w" s="T506">aša</ts>
                  <nts id="Seg_1474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1476" n="HIAT:w" s="T507">tɛnimak</ts>
                  <nts id="Seg_1477" n="HIAT:ip">.</nts>
                  <nts id="Seg_1478" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T515" id="Seg_1480" n="HIAT:u" s="T508">
                  <ts e="T509" id="Seg_1482" n="HIAT:w" s="T508">Tına</ts>
                  <nts id="Seg_1483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1485" n="HIAT:w" s="T509">sər</ts>
                  <nts id="Seg_1486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1488" n="HIAT:w" s="T510">təttalʼ</ts>
                  <nts id="Seg_1489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1491" n="HIAT:w" s="T511">irra</ts>
                  <nts id="Seg_1492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1494" n="HIAT:w" s="T512">nalʼät</ts>
                  <nts id="Seg_1495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1497" n="HIAT:w" s="T513">na</ts>
                  <nts id="Seg_1498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1500" n="HIAT:w" s="T514">ɛsɨmmɨntɨ</ts>
                  <nts id="Seg_1501" n="HIAT:ip">.</nts>
                  <nts id="Seg_1502" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T518" id="Seg_1504" n="HIAT:u" s="T515">
                  <ts e="T516" id="Seg_1506" n="HIAT:w" s="T515">Tɛː</ts>
                  <nts id="Seg_1507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1509" n="HIAT:w" s="T516">qajqa</ts>
                  <nts id="Seg_1510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1512" n="HIAT:w" s="T517">ɔːmtoːlet</ts>
                  <nts id="Seg_1513" n="HIAT:ip">.</nts>
                  <nts id="Seg_1514" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T522" id="Seg_1516" n="HIAT:u" s="T518">
                  <ts e="T519" id="Seg_1518" n="HIAT:w" s="T518">Wäčʼit</ts>
                  <nts id="Seg_1519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1521" n="HIAT:w" s="T519">mɔːttə</ts>
                  <nts id="Seg_1522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1524" n="HIAT:w" s="T520">tuːltila</ts>
                  <nts id="Seg_1525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1527" n="HIAT:w" s="T521">mušeriŋɨlɨt</ts>
                  <nts id="Seg_1528" n="HIAT:ip">.</nts>
                  <nts id="Seg_1529" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T532" id="Seg_1531" n="HIAT:u" s="T522">
                  <ts e="T523" id="Seg_1533" n="HIAT:w" s="T522">Ija</ts>
                  <nts id="Seg_1534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1536" n="HIAT:w" s="T523">mannɨmpatɨ</ts>
                  <nts id="Seg_1537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1539" n="HIAT:w" s="T524">monte</ts>
                  <nts id="Seg_1540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1542" n="HIAT:w" s="T525">mɨtä</ts>
                  <nts id="Seg_1543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1545" n="HIAT:w" s="T526">mɔːtqonä</ts>
                  <nts id="Seg_1546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_1548" n="HIAT:w" s="T527">ponä</ts>
                  <nts id="Seg_1549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1551" n="HIAT:w" s="T528">ukkur</ts>
                  <nts id="Seg_1552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_1554" n="HIAT:w" s="T529">nätak</ts>
                  <nts id="Seg_1555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_1557" n="HIAT:w" s="T530">na</ts>
                  <nts id="Seg_1558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_1560" n="HIAT:w" s="T531">tannɨmmɨntɨ</ts>
                  <nts id="Seg_1561" n="HIAT:ip">.</nts>
                  <nts id="Seg_1562" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T539" id="Seg_1564" n="HIAT:u" s="T532">
                  <ts e="T533" id="Seg_1566" n="HIAT:w" s="T532">Nätak</ts>
                  <nts id="Seg_1567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_1569" n="HIAT:w" s="T533">tantɨlä</ts>
                  <nts id="Seg_1570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_1572" n="HIAT:w" s="T534">wäčʼip</ts>
                  <nts id="Seg_1573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1575" n="HIAT:w" s="T535">maːtalä</ts>
                  <nts id="Seg_1576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_1578" n="HIAT:w" s="T536">mɔːtto</ts>
                  <nts id="Seg_1579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_1581" n="HIAT:w" s="T537">nä</ts>
                  <nts id="Seg_1582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_1584" n="HIAT:w" s="T538">tultimmɨntɨ</ts>
                  <nts id="Seg_1585" n="HIAT:ip">.</nts>
                  <nts id="Seg_1586" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T549" id="Seg_1588" n="HIAT:u" s="T539">
                  <ts e="T540" id="Seg_1590" n="HIAT:w" s="T539">Aša</ts>
                  <nts id="Seg_1591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_1593" n="HIAT:w" s="T540">kuntɨ</ts>
                  <nts id="Seg_1594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_1596" n="HIAT:w" s="T541">ɔːmtäla</ts>
                  <nts id="Seg_1597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_1599" n="HIAT:w" s="T542">puːla</ts>
                  <nts id="Seg_1600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_1602" n="HIAT:w" s="T543">ira</ts>
                  <nts id="Seg_1603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_1605" n="HIAT:w" s="T544">ɛj</ts>
                  <nts id="Seg_1606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_1608" n="HIAT:w" s="T545">iːja</ts>
                  <nts id="Seg_1609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_1611" n="HIAT:w" s="T546">mɔːtta</ts>
                  <nts id="Seg_1612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_1614" n="HIAT:w" s="T547">nä</ts>
                  <nts id="Seg_1615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_1617" n="HIAT:w" s="T548">šeːrpintɔːqıj</ts>
                  <nts id="Seg_1618" n="HIAT:ip">.</nts>
                  <nts id="Seg_1619" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T556" id="Seg_1621" n="HIAT:u" s="T549">
                  <ts e="T550" id="Seg_1623" n="HIAT:w" s="T549">Ija</ts>
                  <nts id="Seg_1624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_1626" n="HIAT:w" s="T550">mɔːt</ts>
                  <nts id="Seg_1627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_1629" n="HIAT:w" s="T551">šeːrlʼa</ts>
                  <nts id="Seg_1630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_1632" n="HIAT:w" s="T552">mɔːtan</ts>
                  <nts id="Seg_1633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_1635" n="HIAT:w" s="T553">ɔːktɨ</ts>
                  <nts id="Seg_1636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_1638" n="HIAT:w" s="T554">čʼam</ts>
                  <nts id="Seg_1639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_1641" n="HIAT:w" s="T555">nɨllɛːjoqolampa</ts>
                  <nts id="Seg_1642" n="HIAT:ip">.</nts>
                  <nts id="Seg_1643" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T565" id="Seg_1645" n="HIAT:u" s="T556">
                  <ts e="T557" id="Seg_1647" n="HIAT:w" s="T556">Ira</ts>
                  <nts id="Seg_1648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_1650" n="HIAT:w" s="T557">nalʼät</ts>
                  <nts id="Seg_1651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_1653" n="HIAT:w" s="T558">na</ts>
                  <nts id="Seg_1654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_1656" n="HIAT:w" s="T559">ɛsɨmmɨntɨ</ts>
                  <nts id="Seg_1657" n="HIAT:ip">:</nts>
                  <nts id="Seg_1658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_1660" n="HIAT:w" s="T560">tɛː</ts>
                  <nts id="Seg_1661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_1663" n="HIAT:w" s="T561">qaj</ts>
                  <nts id="Seg_1664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_1666" n="HIAT:w" s="T562">qumɨp</ts>
                  <nts id="Seg_1667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_1669" n="HIAT:w" s="T563">aša</ts>
                  <nts id="Seg_1670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_1672" n="HIAT:w" s="T564">qontɔːlɨt</ts>
                  <nts id="Seg_1673" n="HIAT:ip">.</nts>
                  <nts id="Seg_1674" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T571" id="Seg_1676" n="HIAT:u" s="T565">
                  <ts e="T566" id="Seg_1678" n="HIAT:w" s="T565">Nʼenna</ts>
                  <nts id="Seg_1679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_1681" n="HIAT:w" s="T566">üːtoqɨntɨtqa</ts>
                  <nts id="Seg_1682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_1684" n="HIAT:w" s="T567">qajqa</ts>
                  <nts id="Seg_1685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_1687" n="HIAT:w" s="T568">mɔːtan</ts>
                  <nts id="Seg_1688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_1690" n="HIAT:w" s="T569">ɔːktɨ</ts>
                  <nts id="Seg_1691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_1693" n="HIAT:w" s="T570">nɨlʼtimpɔːlɨt</ts>
                  <nts id="Seg_1694" n="HIAT:ip">.</nts>
                  <nts id="Seg_1695" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T577" id="Seg_1697" n="HIAT:u" s="T571">
                  <ts e="T572" id="Seg_1699" n="HIAT:w" s="T571">Ijap</ts>
                  <nts id="Seg_1700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_1702" n="HIAT:w" s="T572">nʼenna</ts>
                  <nts id="Seg_1703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_1705" n="HIAT:w" s="T573">iːrat</ts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_1708" n="HIAT:w" s="T574">qönʼte</ts>
                  <nts id="Seg_1709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_1711" n="HIAT:w" s="T575">na</ts>
                  <nts id="Seg_1712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_1714" n="HIAT:w" s="T576">omtɨltämmɨntɔːtɨt</ts>
                  <nts id="Seg_1715" n="HIAT:ip">.</nts>
                  <nts id="Seg_1716" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T584" id="Seg_1718" n="HIAT:u" s="T577">
                  <ts e="T578" id="Seg_1720" n="HIAT:w" s="T577">Wäčʼitsä</ts>
                  <nts id="Seg_1721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_1723" n="HIAT:w" s="T578">na</ts>
                  <nts id="Seg_1724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_1726" n="HIAT:w" s="T579">tıːtalʼpintɔːtɨt</ts>
                  <nts id="Seg_1727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_1729" n="HIAT:w" s="T580">selʼčʼuqɨlʼ</ts>
                  <nts id="Seg_1730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_1732" n="HIAT:w" s="T581">apsɨlʼ</ts>
                  <nts id="Seg_1733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_1735" n="HIAT:w" s="T582">na</ts>
                  <nts id="Seg_1736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_1738" n="HIAT:w" s="T583">tottɨmmɨntɔːtɨt</ts>
                  <nts id="Seg_1739" n="HIAT:ip">.</nts>
                  <nts id="Seg_1740" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T592" id="Seg_1742" n="HIAT:u" s="T584">
                  <ts e="T585" id="Seg_1744" n="HIAT:w" s="T584">Šitte</ts>
                  <nts id="Seg_1745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_1747" n="HIAT:w" s="T585">qumoːqıj</ts>
                  <nts id="Seg_1748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_1750" n="HIAT:w" s="T586">pona</ts>
                  <nts id="Seg_1751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_1753" n="HIAT:w" s="T587">tantɨla</ts>
                  <nts id="Seg_1754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_1756" n="HIAT:w" s="T588">ütɨp</ts>
                  <nts id="Seg_1757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_1759" n="HIAT:w" s="T589">mɔːttɨ</ts>
                  <nts id="Seg_1760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_1762" n="HIAT:w" s="T590">nä</ts>
                  <nts id="Seg_1763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_1765" n="HIAT:w" s="T591">tültimmontətij</ts>
                  <nts id="Seg_1766" n="HIAT:ip">.</nts>
                  <nts id="Seg_1767" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T597" id="Seg_1769" n="HIAT:u" s="T592">
                  <ts e="T593" id="Seg_1771" n="HIAT:w" s="T592">Na</ts>
                  <nts id="Seg_1772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_1774" n="HIAT:w" s="T593">ütim</ts>
                  <nts id="Seg_1775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_1777" n="HIAT:w" s="T594">peralintɔːtɨt</ts>
                  <nts id="Seg_1778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_1780" n="HIAT:w" s="T595">ɛj</ts>
                  <nts id="Seg_1781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_1783" n="HIAT:w" s="T596">amɨrqulʼammɨntɔːtɨt</ts>
                  <nts id="Seg_1784" n="HIAT:ip">.</nts>
                  <nts id="Seg_1785" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T602" id="Seg_1787" n="HIAT:u" s="T597">
                  <ts e="T598" id="Seg_1789" n="HIAT:w" s="T597">Iːja</ts>
                  <nts id="Seg_1790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_1792" n="HIAT:w" s="T598">amɨrä</ts>
                  <nts id="Seg_1793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_1795" n="HIAT:w" s="T599">čʼuntoːqɨt</ts>
                  <nts id="Seg_1796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_1798" n="HIAT:w" s="T600">nılʼčʼiŋä</ts>
                  <nts id="Seg_1799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_1801" n="HIAT:w" s="T601">üntɨšpɨntäna</ts>
                  <nts id="Seg_1802" n="HIAT:ip">.</nts>
                  <nts id="Seg_1803" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T612" id="Seg_1805" n="HIAT:u" s="T602">
                  <ts e="T603" id="Seg_1807" n="HIAT:w" s="T602">Montä</ts>
                  <nts id="Seg_1808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_1810" n="HIAT:w" s="T603">mɨta</ts>
                  <nts id="Seg_1811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_1813" n="HIAT:w" s="T604">Laptelʼ</ts>
                  <nts id="Seg_1814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_1816" n="HIAT:w" s="T605">nɔːkur</ts>
                  <nts id="Seg_1817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_1819" n="HIAT:w" s="T606">timnʼäsɨt</ts>
                  <nts id="Seg_1820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_1822" n="HIAT:w" s="T607">imalʼlʼä</ts>
                  <nts id="Seg_1823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_1825" n="HIAT:w" s="T608">täp</ts>
                  <nts id="Seg_1826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_1828" n="HIAT:w" s="T609">taːtɨmpɔːtɨt</ts>
                  <nts id="Seg_1829" n="HIAT:ip">,</nts>
                  <nts id="Seg_1830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_1832" n="HIAT:w" s="T610">sərtəttalʼ</ts>
                  <nts id="Seg_1833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_1835" n="HIAT:w" s="T611">timnʼäsɨtkinte</ts>
                  <nts id="Seg_1836" n="HIAT:ip">.</nts>
                  <nts id="Seg_1837" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T617" id="Seg_1839" n="HIAT:u" s="T612">
                  <ts e="T613" id="Seg_1841" n="HIAT:w" s="T612">Sərtəttalʼ</ts>
                  <nts id="Seg_1842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_1844" n="HIAT:w" s="T613">timnʼäsɨt</ts>
                  <nts id="Seg_1845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_1847" n="HIAT:w" s="T614">ukkur</ts>
                  <nts id="Seg_1848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_1850" n="HIAT:w" s="T615">nʼenʼätɨt</ts>
                  <nts id="Seg_1851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_1853" n="HIAT:w" s="T616">ɛːppɨntä</ts>
                  <nts id="Seg_1854" n="HIAT:ip">.</nts>
                  <nts id="Seg_1855" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T620" id="Seg_1857" n="HIAT:u" s="T617">
                  <ts e="T618" id="Seg_1859" n="HIAT:w" s="T617">Namɨnɨk</ts>
                  <nts id="Seg_1860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_1862" n="HIAT:w" s="T618">imalʼlʼi</ts>
                  <nts id="Seg_1863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_1865" n="HIAT:w" s="T619">tümpɔːtɨt</ts>
                  <nts id="Seg_1866" n="HIAT:ip">.</nts>
                  <nts id="Seg_1867" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T627" id="Seg_1869" n="HIAT:u" s="T620">
                  <ts e="T621" id="Seg_1871" n="HIAT:w" s="T620">Amɨrlʼä</ts>
                  <nts id="Seg_1872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_1874" n="HIAT:w" s="T621">somak</ts>
                  <nts id="Seg_1875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_1877" n="HIAT:w" s="T622">tortɨntɔːtɨk</ts>
                  <nts id="Seg_1878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_1880" n="HIAT:w" s="T623">nɨːnɨ</ts>
                  <nts id="Seg_1881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_1883" n="HIAT:w" s="T624">šitɨ</ts>
                  <nts id="Seg_1884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_1886" n="HIAT:w" s="T625">na</ts>
                  <nts id="Seg_1887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_1889" n="HIAT:w" s="T626">qənqolammɨntɔːtɨt</ts>
                  <nts id="Seg_1890" n="HIAT:ip">.</nts>
                  <nts id="Seg_1891" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T634" id="Seg_1893" n="HIAT:u" s="T627">
                  <ts e="T628" id="Seg_1895" n="HIAT:w" s="T627">Əːtɨt</ts>
                  <nts id="Seg_1896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_1898" n="HIAT:w" s="T628">montɨ</ts>
                  <nts id="Seg_1899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_1901" n="HIAT:w" s="T629">mɨta</ts>
                  <nts id="Seg_1902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_1904" n="HIAT:w" s="T630">nilʼčʼik</ts>
                  <nts id="Seg_1905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_1907" n="HIAT:w" s="T631">nʼentɨ</ts>
                  <nts id="Seg_1908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_1910" n="HIAT:w" s="T632">nɨː</ts>
                  <nts id="Seg_1911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_1913" n="HIAT:w" s="T633">pintɔːtɨt</ts>
                  <nts id="Seg_1914" n="HIAT:ip">.</nts>
                  <nts id="Seg_1915" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T640" id="Seg_1917" n="HIAT:u" s="T634">
                  <ts e="T635" id="Seg_1919" n="HIAT:w" s="T634">Täːlʼi</ts>
                  <nts id="Seg_1920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_1922" n="HIAT:w" s="T635">nɔːtɨ</ts>
                  <nts id="Seg_1923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_1925" n="HIAT:w" s="T636">qumaqumtij</ts>
                  <nts id="Seg_1926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_1928" n="HIAT:w" s="T637">nʼentɨ</ts>
                  <nts id="Seg_1929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_1931" n="HIAT:w" s="T638">na</ts>
                  <nts id="Seg_1932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_1934" n="HIAT:w" s="T639">omtɨltɛntɔːtɨt</ts>
                  <nts id="Seg_1935" n="HIAT:ip">.</nts>
                  <nts id="Seg_1936" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T647" id="Seg_1938" n="HIAT:u" s="T640">
                  <ts e="T641" id="Seg_1940" n="HIAT:w" s="T640">Qumɨtɨt</ts>
                  <nts id="Seg_1941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_1943" n="HIAT:w" s="T641">qəntaːtqo</ts>
                  <nts id="Seg_1944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_1946" n="HIAT:w" s="T642">ija</ts>
                  <nts id="Seg_1947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_1949" n="HIAT:w" s="T643">ɛj</ts>
                  <nts id="Seg_1950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_1952" n="HIAT:w" s="T644">moqona</ts>
                  <nts id="Seg_1953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_1955" n="HIAT:w" s="T645">na</ts>
                  <nts id="Seg_1956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_1958" n="HIAT:w" s="T646">qənqolamnɨtɨnɨ</ts>
                  <nts id="Seg_1959" n="HIAT:ip">.</nts>
                  <nts id="Seg_1960" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T660" id="Seg_1962" n="HIAT:u" s="T647">
                  <ts e="T648" id="Seg_1964" n="HIAT:w" s="T647">Pona</ts>
                  <nts id="Seg_1965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_1967" n="HIAT:w" s="T648">tantɨptäːqɨntɨt</ts>
                  <nts id="Seg_1968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_1970" n="HIAT:w" s="T649">ija</ts>
                  <nts id="Seg_1971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_1973" n="HIAT:w" s="T650">soqomtä</ts>
                  <nts id="Seg_1974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_1976" n="HIAT:w" s="T651">soqaltiptäːqɨt</ts>
                  <nts id="Seg_1977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_1979" n="HIAT:w" s="T652">Laptɨlʼ</ts>
                  <nts id="Seg_1980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_1982" n="HIAT:w" s="T653">poːsa</ts>
                  <nts id="Seg_1983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_1985" n="HIAT:w" s="T654">warqɨ</ts>
                  <nts id="Seg_1986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_1988" n="HIAT:w" s="T655">timnʼätɨt</ts>
                  <nts id="Seg_1989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_1991" n="HIAT:w" s="T656">nälʼat</ts>
                  <nts id="Seg_1992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_1994" n="HIAT:w" s="T657">na</ts>
                  <nts id="Seg_1995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_1997" n="HIAT:w" s="T658">ɛsɨmmɨntɨ</ts>
                  <nts id="Seg_1998" n="HIAT:ip">,</nts>
                  <nts id="Seg_1999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2001" n="HIAT:w" s="T659">ijanɨk</ts>
                  <nts id="Seg_2002" n="HIAT:ip">.</nts>
                  <nts id="Seg_2003" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T673" id="Seg_2005" n="HIAT:u" s="T660">
                  <ts e="T661" id="Seg_2007" n="HIAT:w" s="T660">Soqqante</ts>
                  <nts id="Seg_2008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2010" n="HIAT:w" s="T661">tasɨt</ts>
                  <nts id="Seg_2011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2013" n="HIAT:w" s="T662">mitə</ts>
                  <nts id="Seg_2014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2016" n="HIAT:w" s="T663">tına</ts>
                  <nts id="Seg_2017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2019" n="HIAT:w" s="T664">opčʼintas</ts>
                  <nts id="Seg_2020" n="HIAT:ip">,</nts>
                  <nts id="Seg_2021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2023" n="HIAT:w" s="T665">a</ts>
                  <nts id="Seg_2024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2026" n="HIAT:w" s="T666">soqqant</ts>
                  <nts id="Seg_2027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2029" n="HIAT:w" s="T667">olä</ts>
                  <nts id="Seg_2030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2032" n="HIAT:w" s="T668">mitɨ</ts>
                  <nts id="Seg_2033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2035" n="HIAT:w" s="T669">tına</ts>
                  <nts id="Seg_2036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2038" n="HIAT:w" s="T670">poːlʼ</ts>
                  <nts id="Seg_2039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2041" n="HIAT:w" s="T671">kalan</ts>
                  <nts id="Seg_2042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2044" n="HIAT:w" s="T672">olɨ</ts>
                  <nts id="Seg_2045" n="HIAT:ip">.</nts>
                  <nts id="Seg_2046" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T679" id="Seg_2048" n="HIAT:u" s="T673">
                  <ts e="T674" id="Seg_2050" n="HIAT:w" s="T673">Ija</ts>
                  <nts id="Seg_2051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2053" n="HIAT:w" s="T674">ne</ts>
                  <nts id="Seg_2054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2056" n="HIAT:w" s="T675">qajimi</ts>
                  <nts id="Seg_2057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2059" n="HIAT:w" s="T676">äčʼa</ts>
                  <nts id="Seg_2060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2062" n="HIAT:w" s="T677">na</ts>
                  <nts id="Seg_2063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2065" n="HIAT:w" s="T678">kətɨmnɨtɨ</ts>
                  <nts id="Seg_2066" n="HIAT:ip">.</nts>
                  <nts id="Seg_2067" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T681" id="Seg_2069" n="HIAT:u" s="T679">
                  <ts e="T680" id="Seg_2071" n="HIAT:w" s="T679">Nɨːnɨ</ts>
                  <nts id="Seg_2072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2074" n="HIAT:w" s="T680">laqaltɛːmnɨt</ts>
                  <nts id="Seg_2075" n="HIAT:ip">.</nts>
                  <nts id="Seg_2076" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T684" id="Seg_2078" n="HIAT:u" s="T681">
                  <ts e="T682" id="Seg_2080" n="HIAT:w" s="T681">Mɔːtt</ts>
                  <nts id="Seg_2081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2083" n="HIAT:w" s="T682">čʼap</ts>
                  <nts id="Seg_2084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2086" n="HIAT:w" s="T683">tülʼčʼa</ts>
                  <nts id="Seg_2087" n="HIAT:ip">.</nts>
                  <nts id="Seg_2088" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T692" id="Seg_2090" n="HIAT:u" s="T684">
                  <ts e="T685" id="Seg_2092" n="HIAT:w" s="T684">Əmɨtɨ</ts>
                  <nts id="Seg_2093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2095" n="HIAT:w" s="T685">mɔːtan</ts>
                  <nts id="Seg_2096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2098" n="HIAT:w" s="T686">ɔːqɨt</ts>
                  <nts id="Seg_2099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2101" n="HIAT:w" s="T687">ətɨlʼ</ts>
                  <nts id="Seg_2102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2104" n="HIAT:w" s="T688">tükulä</ts>
                  <nts id="Seg_2105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2107" n="HIAT:w" s="T689">nɨŋka</ts>
                  <nts id="Seg_2108" n="HIAT:ip">,</nts>
                  <nts id="Seg_2109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2111" n="HIAT:w" s="T690">ijamtɨ</ts>
                  <nts id="Seg_2112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2114" n="HIAT:w" s="T691">ɛtɨla</ts>
                  <nts id="Seg_2115" n="HIAT:ip">.</nts>
                  <nts id="Seg_2116" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T704" id="Seg_2118" n="HIAT:u" s="T692">
                  <ts e="T693" id="Seg_2120" n="HIAT:w" s="T692">Ija</ts>
                  <nts id="Seg_2121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2123" n="HIAT:w" s="T693">tat</ts>
                  <nts id="Seg_2124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2126" n="HIAT:w" s="T694">kučʼa</ts>
                  <nts id="Seg_2127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2129" n="HIAT:w" s="T695">namɨšak</ts>
                  <nts id="Seg_2130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2132" n="HIAT:w" s="T696">qonɨnʼantɨ</ts>
                  <nts id="Seg_2133" n="HIAT:ip">,</nts>
                  <nts id="Seg_2134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2136" n="HIAT:w" s="T697">mat</ts>
                  <nts id="Seg_2137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2139" n="HIAT:w" s="T698">puršimɔːtʼanoːqa</ts>
                  <nts id="Seg_2140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2142" n="HIAT:w" s="T699">karrʼat</ts>
                  <nts id="Seg_2143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2145" n="HIAT:w" s="T700">tap</ts>
                  <nts id="Seg_2146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2148" n="HIAT:w" s="T701">kotpas</ts>
                  <nts id="Seg_2149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2151" n="HIAT:w" s="T702">pɔːrɨqɨt</ts>
                  <nts id="Seg_2152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_2154" n="HIAT:w" s="T703">ɔːmtɨkolʼimmɨntak</ts>
                  <nts id="Seg_2155" n="HIAT:ip">.</nts>
                  <nts id="Seg_2156" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T709" id="Seg_2158" n="HIAT:u" s="T704">
                  <ts e="T705" id="Seg_2160" n="HIAT:w" s="T704">Na</ts>
                  <nts id="Seg_2161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2163" n="HIAT:w" s="T705">pit</ts>
                  <nts id="Seg_2164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2166" n="HIAT:w" s="T706">ija</ts>
                  <nts id="Seg_2167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_2169" n="HIAT:w" s="T707">na</ts>
                  <nts id="Seg_2170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_2172" n="HIAT:w" s="T708">šäqɨmmɨtɨ</ts>
                  <nts id="Seg_2173" n="HIAT:ip">.</nts>
                  <nts id="Seg_2174" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T723" id="Seg_2176" n="HIAT:u" s="T709">
                  <ts e="T710" id="Seg_2178" n="HIAT:w" s="T709">Tɔːptɨlʼ</ts>
                  <nts id="Seg_2179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_2181" n="HIAT:w" s="T710">čʼeːlɨ</ts>
                  <nts id="Seg_2182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2184" n="HIAT:w" s="T711">ɔːtamtɨ</ts>
                  <nts id="Seg_2185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2187" n="HIAT:w" s="T712">moqɨna</ts>
                  <nts id="Seg_2188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2190" n="HIAT:w" s="T713">tɔːqɨla</ts>
                  <nts id="Seg_2191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2193" n="HIAT:w" s="T714">ɔːtantɨ</ts>
                  <nts id="Seg_2194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_2196" n="HIAT:w" s="T715">puːtoːqɨt</ts>
                  <nts id="Seg_2197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2199" n="HIAT:w" s="T716">peːkɨla</ts>
                  <nts id="Seg_2200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2202" n="HIAT:w" s="T717">šitɨ</ts>
                  <nts id="Seg_2203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2205" n="HIAT:w" s="T718">koškat</ts>
                  <nts id="Seg_2206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2208" n="HIAT:w" s="T719">tarɨlʼ</ts>
                  <nts id="Seg_2209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_2211" n="HIAT:w" s="T720">qoptoːqın</ts>
                  <nts id="Seg_2212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_2214" n="HIAT:w" s="T721">na</ts>
                  <nts id="Seg_2215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_2217" n="HIAT:w" s="T722">orqɨlpɨntɨtɨ</ts>
                  <nts id="Seg_2218" n="HIAT:ip">.</nts>
                  <nts id="Seg_2219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T727" id="Seg_2221" n="HIAT:u" s="T723">
                  <ts e="T724" id="Seg_2223" n="HIAT:w" s="T723">Moːtɨkɔːlʼ</ts>
                  <nts id="Seg_2224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_2226" n="HIAT:w" s="T724">qalɨmtɨ</ts>
                  <nts id="Seg_2227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_2229" n="HIAT:w" s="T725">na</ts>
                  <nts id="Seg_2230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2232" n="HIAT:w" s="T726">sɔːrɔːlpɨntɨtɨnta</ts>
                  <nts id="Seg_2233" n="HIAT:ip">.</nts>
                  <nts id="Seg_2234" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T733" id="Seg_2236" n="HIAT:u" s="T727">
                  <ts e="T728" id="Seg_2238" n="HIAT:w" s="T727">Kotälʼ</ts>
                  <nts id="Seg_2239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2241" n="HIAT:w" s="T728">malʼčʼamtɨ</ts>
                  <nts id="Seg_2242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2244" n="HIAT:w" s="T729">ɛj</ts>
                  <nts id="Seg_2245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2247" n="HIAT:w" s="T730">soqqɨmtɨ</ts>
                  <nts id="Seg_2248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_2250" n="HIAT:w" s="T731">nä</ts>
                  <nts id="Seg_2251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2253" n="HIAT:w" s="T732">tokaltɨmmɨntɨtäːnɨ</ts>
                  <nts id="Seg_2254" n="HIAT:ip">.</nts>
                  <nts id="Seg_2255" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T741" id="Seg_2257" n="HIAT:u" s="T733">
                  <ts e="T734" id="Seg_2259" n="HIAT:w" s="T733">Narapomtä</ts>
                  <nts id="Seg_2260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_2262" n="HIAT:w" s="T734">iːmpatä</ts>
                  <nts id="Seg_2263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_2265" n="HIAT:w" s="T735">qaj</ts>
                  <nts id="Seg_2266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_2268" n="HIAT:w" s="T736">aša</ts>
                  <nts id="Seg_2269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2270" n="HIAT:ip">(</nts>
                  <nts id="Seg_2271" n="HIAT:ip">(</nts>
                  <ats e="T738" id="Seg_2272" n="HIAT:non-pho" s="T737">…</ats>
                  <nts id="Seg_2273" n="HIAT:ip">)</nts>
                  <nts id="Seg_2274" n="HIAT:ip">)</nts>
                  <nts id="Seg_2275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_2277" n="HIAT:w" s="T738">nılʼčʼik</ts>
                  <nts id="Seg_2278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_2280" n="HIAT:w" s="T739">na</ts>
                  <nts id="Seg_2281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_2283" n="HIAT:w" s="T740">laqalʼtɛːmmintɨ</ts>
                  <nts id="Seg_2284" n="HIAT:ip">.</nts>
                  <nts id="Seg_2285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T753" id="Seg_2287" n="HIAT:u" s="T741">
                  <ts e="T742" id="Seg_2289" n="HIAT:w" s="T741">Tolʼ</ts>
                  <nts id="Seg_2290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_2292" n="HIAT:w" s="T742">qumɨtɨn</ts>
                  <nts id="Seg_2293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_2295" n="HIAT:w" s="T743">mɔːttə</ts>
                  <nts id="Seg_2296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_2298" n="HIAT:w" s="T744">čʼap</ts>
                  <nts id="Seg_2299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_2301" n="HIAT:w" s="T745">tüːlʼčʼe</ts>
                  <nts id="Seg_2302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_2304" n="HIAT:w" s="T746">monte</ts>
                  <nts id="Seg_2305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_2307" n="HIAT:w" s="T747">mɨta</ts>
                  <nts id="Seg_2308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_2310" n="HIAT:w" s="T748">Laptälʼ</ts>
                  <nts id="Seg_2311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_2313" n="HIAT:w" s="T749">tɨmnʼäsɨt</ts>
                  <nts id="Seg_2314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_2316" n="HIAT:w" s="T750">okoːt</ts>
                  <nts id="Seg_2317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_2319" n="HIAT:w" s="T751">qaj</ts>
                  <nts id="Seg_2320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_2322" n="HIAT:w" s="T752">tüːŋɔːtät</ts>
                  <nts id="Seg_2323" n="HIAT:ip">.</nts>
                  <nts id="Seg_2324" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T760" id="Seg_2326" n="HIAT:u" s="T753">
                  <ts e="T754" id="Seg_2328" n="HIAT:w" s="T753">Ija</ts>
                  <nts id="Seg_2329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_2331" n="HIAT:w" s="T754">ɔːqalʼtɨ</ts>
                  <nts id="Seg_2332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_2334" n="HIAT:w" s="T755">ılla</ts>
                  <nts id="Seg_2335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_2337" n="HIAT:w" s="T756">sɔːrɨlʼä</ts>
                  <nts id="Seg_2338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_2340" n="HIAT:w" s="T757">mɔːttɨ</ts>
                  <nts id="Seg_2341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_2343" n="HIAT:w" s="T758">na</ts>
                  <nts id="Seg_2344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_2346" n="HIAT:w" s="T759">šeːrpɨtɨ</ts>
                  <nts id="Seg_2347" n="HIAT:ip">.</nts>
                  <nts id="Seg_2348" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T773" id="Seg_2350" n="HIAT:u" s="T760">
                  <ts e="T761" id="Seg_2352" n="HIAT:w" s="T760">Mɔːttɨ</ts>
                  <nts id="Seg_2353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_2355" n="HIAT:w" s="T761">šeːrlʼa</ts>
                  <nts id="Seg_2356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_2358" n="HIAT:w" s="T762">mɔːtan</ts>
                  <nts id="Seg_2359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_2361" n="HIAT:w" s="T763">ɔːktə</ts>
                  <nts id="Seg_2362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_2364" n="HIAT:w" s="T764">čʼap</ts>
                  <nts id="Seg_2365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_2367" n="HIAT:w" s="T765">nɨllɛːje</ts>
                  <nts id="Seg_2368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_2370" n="HIAT:w" s="T766">ira</ts>
                  <nts id="Seg_2371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T768" id="Seg_2373" n="HIAT:w" s="T767">tälʼ</ts>
                  <nts id="Seg_2374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_2376" n="HIAT:w" s="T768">čʼeːlɨ</ts>
                  <nts id="Seg_2377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_2379" n="HIAT:w" s="T769">montɨ</ts>
                  <nts id="Seg_2380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T771" id="Seg_2382" n="HIAT:w" s="T770">mɨta</ts>
                  <nts id="Seg_2383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_2385" n="HIAT:w" s="T771">nalʼät</ts>
                  <nts id="Seg_2386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_2388" n="HIAT:w" s="T772">ɛsa</ts>
                  <nts id="Seg_2389" n="HIAT:ip">.</nts>
                  <nts id="Seg_2390" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T779" id="Seg_2392" n="HIAT:u" s="T773">
                  <ts e="T774" id="Seg_2394" n="HIAT:w" s="T773">Tɛː</ts>
                  <nts id="Seg_2395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_2397" n="HIAT:w" s="T774">qaj</ts>
                  <nts id="Seg_2398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_2400" n="HIAT:w" s="T775">qoštɨlʼ</ts>
                  <nts id="Seg_2401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_2403" n="HIAT:w" s="T776">qumɨp</ts>
                  <nts id="Seg_2404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_2406" n="HIAT:w" s="T777">aša</ts>
                  <nts id="Seg_2407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_2409" n="HIAT:w" s="T778">qontɔːlɨt</ts>
                  <nts id="Seg_2410" n="HIAT:ip">.</nts>
                  <nts id="Seg_2411" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T781" id="Seg_2413" n="HIAT:u" s="T779">
                  <ts e="T780" id="Seg_2415" n="HIAT:w" s="T779">Nänna</ts>
                  <nts id="Seg_2416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_2418" n="HIAT:w" s="T780">üːtočʼɨntɨtqa</ts>
                  <nts id="Seg_2419" n="HIAT:ip">.</nts>
                  <nts id="Seg_2420" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T789" id="Seg_2422" n="HIAT:u" s="T781">
                  <ts e="T782" id="Seg_2424" n="HIAT:w" s="T781">Ija</ts>
                  <nts id="Seg_2425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_2427" n="HIAT:w" s="T782">mannɨmpatɨ</ts>
                  <nts id="Seg_2428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_2430" n="HIAT:w" s="T783">irat</ts>
                  <nts id="Seg_2431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_2433" n="HIAT:w" s="T784">qöntɨ</ts>
                  <nts id="Seg_2434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T786" id="Seg_2436" n="HIAT:w" s="T785">koptɨp</ts>
                  <nts id="Seg_2437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_2439" n="HIAT:w" s="T786">šitä</ts>
                  <nts id="Seg_2440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_2442" n="HIAT:w" s="T787">na</ts>
                  <nts id="Seg_2443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_2445" n="HIAT:w" s="T788">meːntɔːtɨt</ts>
                  <nts id="Seg_2446" n="HIAT:ip">.</nts>
                  <nts id="Seg_2447" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T793" id="Seg_2449" n="HIAT:u" s="T789">
                  <ts e="T790" id="Seg_2451" n="HIAT:w" s="T789">Ija</ts>
                  <nts id="Seg_2452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_2454" n="HIAT:w" s="T790">nʼenna</ts>
                  <nts id="Seg_2455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_2457" n="HIAT:w" s="T791">ılla</ts>
                  <nts id="Seg_2458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T793" id="Seg_2460" n="HIAT:w" s="T792">omta</ts>
                  <nts id="Seg_2461" n="HIAT:ip">.</nts>
                  <nts id="Seg_2462" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T795" id="Seg_2464" n="HIAT:u" s="T793">
                  <ts e="T794" id="Seg_2466" n="HIAT:w" s="T793">Nılʼčʼik</ts>
                  <nts id="Seg_2467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_2469" n="HIAT:w" s="T794">mannɨmpatɨ</ts>
                  <nts id="Seg_2470" n="HIAT:ip">.</nts>
                  <nts id="Seg_2471" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T802" id="Seg_2473" n="HIAT:u" s="T795">
                  <ts e="T796" id="Seg_2475" n="HIAT:w" s="T795">Montɨ</ts>
                  <nts id="Seg_2476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T797" id="Seg_2478" n="HIAT:w" s="T796">mɨta</ts>
                  <nts id="Seg_2479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_2481" n="HIAT:w" s="T797">qumaqumtij</ts>
                  <nts id="Seg_2482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_2484" n="HIAT:w" s="T798">nʼentɨ</ts>
                  <nts id="Seg_2485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_2487" n="HIAT:w" s="T799">omtalʼ</ts>
                  <nts id="Seg_2488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_2490" n="HIAT:w" s="T800">tümpɨla</ts>
                  <nts id="Seg_2491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_2493" n="HIAT:w" s="T801">ütɨmpɔːtɨt</ts>
                  <nts id="Seg_2494" n="HIAT:ip">.</nts>
                  <nts id="Seg_2495" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T810" id="Seg_2497" n="HIAT:u" s="T802">
                  <ts e="T803" id="Seg_2499" n="HIAT:w" s="T802">Ija</ts>
                  <nts id="Seg_2500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_2502" n="HIAT:w" s="T803">ɛj</ts>
                  <nts id="Seg_2503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_2505" n="HIAT:w" s="T804">na</ts>
                  <nts id="Seg_2506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_2508" n="HIAT:w" s="T805">qomintisa</ts>
                  <nts id="Seg_2509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_2511" n="HIAT:w" s="T806">təm</ts>
                  <nts id="Seg_2512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_2514" n="HIAT:w" s="T807">ɛj</ts>
                  <nts id="Seg_2515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_2517" n="HIAT:w" s="T808">na</ts>
                  <nts id="Seg_2518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_2520" n="HIAT:w" s="T809">ütɨmperalʼmɨtɨ</ts>
                  <nts id="Seg_2521" n="HIAT:ip">.</nts>
                  <nts id="Seg_2522" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T816" id="Seg_2524" n="HIAT:u" s="T810">
                  <ts e="T811" id="Seg_2526" n="HIAT:w" s="T810">Montɨ</ts>
                  <nts id="Seg_2527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_2529" n="HIAT:w" s="T811">mɨta</ts>
                  <nts id="Seg_2530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T813" id="Seg_2532" n="HIAT:w" s="T812">ijap</ts>
                  <nts id="Seg_2533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T814" id="Seg_2535" n="HIAT:w" s="T813">ün</ts>
                  <nts id="Seg_2536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_2538" n="HIAT:w" s="T814">na</ts>
                  <nts id="Seg_2539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_2541" n="HIAT:w" s="T815">šeːrtätä</ts>
                  <nts id="Seg_2542" n="HIAT:ip">.</nts>
                  <nts id="Seg_2543" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T820" id="Seg_2545" n="HIAT:u" s="T816">
                  <ts e="T817" id="Seg_2547" n="HIAT:w" s="T816">Ü</ts>
                  <nts id="Seg_2548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_2550" n="HIAT:w" s="T817">šeːrla</ts>
                  <nts id="Seg_2551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_2553" n="HIAT:w" s="T818">ija</ts>
                  <nts id="Seg_2554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T820" id="Seg_2556" n="HIAT:w" s="T819">alʼčʼa</ts>
                  <nts id="Seg_2557" n="HIAT:ip">.</nts>
                  <nts id="Seg_2558" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T830" id="Seg_2560" n="HIAT:u" s="T820">
                  <ts e="T821" id="Seg_2562" n="HIAT:w" s="T820">Ü</ts>
                  <nts id="Seg_2563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_2565" n="HIAT:w" s="T821">sʼeːrpɨlʼ</ts>
                  <nts id="Seg_2566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_2568" n="HIAT:w" s="T822">qup</ts>
                  <nts id="Seg_2569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T824" id="Seg_2571" n="HIAT:w" s="T823">qaj</ts>
                  <nts id="Seg_2572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_2574" n="HIAT:w" s="T824">qaj</ts>
                  <nts id="Seg_2575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_2577" n="HIAT:w" s="T825">tɛnimɨntä</ts>
                  <nts id="Seg_2578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_2580" n="HIAT:w" s="T826">kušaŋ</ts>
                  <nts id="Seg_2581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T828" id="Seg_2583" n="HIAT:w" s="T827">aša</ts>
                  <nts id="Seg_2584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T829" id="Seg_2586" n="HIAT:w" s="T828">na</ts>
                  <nts id="Seg_2587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_2589" n="HIAT:w" s="T829">ippɨnmɨntɨ</ts>
                  <nts id="Seg_2590" n="HIAT:ip">.</nts>
                  <nts id="Seg_2591" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T843" id="Seg_2593" n="HIAT:u" s="T830">
                  <ts e="T831" id="Seg_2595" n="HIAT:w" s="T830">Ukkur</ts>
                  <nts id="Seg_2596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_2598" n="HIAT:w" s="T831">tət</ts>
                  <nts id="Seg_2599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_2601" n="HIAT:w" s="T832">čʼontoːqɨt</ts>
                  <nts id="Seg_2602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_2604" n="HIAT:w" s="T833">iːja</ts>
                  <nts id="Seg_2605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_2607" n="HIAT:w" s="T834">ınna</ts>
                  <nts id="Seg_2608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T836" id="Seg_2610" n="HIAT:w" s="T835">čʼap</ts>
                  <nts id="Seg_2611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T837" id="Seg_2613" n="HIAT:w" s="T836">qəŋa</ts>
                  <nts id="Seg_2614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_2616" n="HIAT:w" s="T837">montɨ</ts>
                  <nts id="Seg_2617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T839" id="Seg_2619" n="HIAT:w" s="T838">mɨta</ts>
                  <nts id="Seg_2620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_2622" n="HIAT:w" s="T839">qup</ts>
                  <nts id="Seg_2623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T841" id="Seg_2625" n="HIAT:w" s="T840">šıp</ts>
                  <nts id="Seg_2626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_2628" n="HIAT:w" s="T841">qəttɨrorrij</ts>
                  <nts id="Seg_2629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_2631" n="HIAT:w" s="T842">nʼoːtɨš</ts>
                  <nts id="Seg_2632" n="HIAT:ip">.</nts>
                  <nts id="Seg_2633" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T852" id="Seg_2635" n="HIAT:u" s="T843">
                  <ts e="T844" id="Seg_2637" n="HIAT:w" s="T843">Nılʼčʼik</ts>
                  <nts id="Seg_2638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T845" id="Seg_2640" n="HIAT:w" s="T844">ippɨlʼa</ts>
                  <nts id="Seg_2641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_2643" n="HIAT:w" s="T845">üŋkultimpatɨ</ts>
                  <nts id="Seg_2644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_2646" n="HIAT:w" s="T846">qäːlʼ</ts>
                  <nts id="Seg_2647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T848" id="Seg_2649" n="HIAT:w" s="T847">täːttoːqɨt</ts>
                  <nts id="Seg_2650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T849" id="Seg_2652" n="HIAT:w" s="T848">tına</ts>
                  <nts id="Seg_2653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T850" id="Seg_2655" n="HIAT:w" s="T849">sərtətattɨt</ts>
                  <nts id="Seg_2656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T851" id="Seg_2658" n="HIAT:w" s="T850">nʼenʼnʼa</ts>
                  <nts id="Seg_2659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_2661" n="HIAT:w" s="T851">laŋkuškuna</ts>
                  <nts id="Seg_2662" n="HIAT:ip">.</nts>
                  <nts id="Seg_2663" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T858" id="Seg_2665" n="HIAT:u" s="T852">
                  <ts e="T853" id="Seg_2667" n="HIAT:w" s="T852">Tɛː</ts>
                  <nts id="Seg_2668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_2670" n="HIAT:w" s="T853">lʼa</ts>
                  <nts id="Seg_2671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T855" id="Seg_2673" n="HIAT:w" s="T854">mompa</ts>
                  <nts id="Seg_2674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856" id="Seg_2676" n="HIAT:w" s="T855">mɨta</ts>
                  <nts id="Seg_2677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T857" id="Seg_2679" n="HIAT:w" s="T856">qaj</ts>
                  <nts id="Seg_2680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T858" id="Seg_2682" n="HIAT:w" s="T857">manpɨmmɨntalit</ts>
                  <nts id="Seg_2683" n="HIAT:ip">.</nts>
                  <nts id="Seg_2684" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T866" id="Seg_2686" n="HIAT:u" s="T858">
                  <ts e="T859" id="Seg_2688" n="HIAT:w" s="T858">Tına</ts>
                  <nts id="Seg_2689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T860" id="Seg_2691" n="HIAT:w" s="T859">šentɨk</ts>
                  <nts id="Seg_2692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T861" id="Seg_2694" n="HIAT:w" s="T860">tümpɨlʼ</ts>
                  <nts id="Seg_2695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T862" id="Seg_2697" n="HIAT:w" s="T861">iːjap</ts>
                  <nts id="Seg_2698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T863" id="Seg_2700" n="HIAT:w" s="T862">na</ts>
                  <nts id="Seg_2701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864" id="Seg_2703" n="HIAT:w" s="T863">qättɔːtɨt</ts>
                  <nts id="Seg_2704" n="HIAT:ip">,</nts>
                  <nts id="Seg_2705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T865" id="Seg_2707" n="HIAT:w" s="T864">Loptɨlʼ</ts>
                  <nts id="Seg_2708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T866" id="Seg_2710" n="HIAT:w" s="T865">tɨmnʼäsɨt</ts>
                  <nts id="Seg_2711" n="HIAT:ip">.</nts>
                  <nts id="Seg_2712" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T869" id="Seg_2714" n="HIAT:u" s="T866">
                  <ts e="T867" id="Seg_2716" n="HIAT:w" s="T866">Ija</ts>
                  <nts id="Seg_2717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T868" id="Seg_2719" n="HIAT:w" s="T867">asʼa</ts>
                  <nts id="Seg_2720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T869" id="Seg_2722" n="HIAT:w" s="T868">qatta</ts>
                  <nts id="Seg_2723" n="HIAT:ip">.</nts>
                  <nts id="Seg_2724" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T879" id="Seg_2726" n="HIAT:u" s="T869">
                  <ts e="T870" id="Seg_2728" n="HIAT:w" s="T869">Ippɨptäː</ts>
                  <nts id="Seg_2729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T871" id="Seg_2731" n="HIAT:w" s="T870">säqɨt</ts>
                  <nts id="Seg_2732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T872" id="Seg_2734" n="HIAT:w" s="T871">iːqɨntä</ts>
                  <nts id="Seg_2735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T873" id="Seg_2737" n="HIAT:w" s="T872">qəttɨla</ts>
                  <nts id="Seg_2738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T874" id="Seg_2740" n="HIAT:w" s="T873">ɔːlʼčʼimpɨlʼ</ts>
                  <nts id="Seg_2741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T875" id="Seg_2743" n="HIAT:w" s="T874">qumimtɨ</ts>
                  <nts id="Seg_2744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_2746" n="HIAT:w" s="T875">səntätɨsä</ts>
                  <nts id="Seg_2747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T877" id="Seg_2749" n="HIAT:w" s="T876">šitä</ts>
                  <nts id="Seg_2750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T878" id="Seg_2752" n="HIAT:w" s="T877">na</ts>
                  <nts id="Seg_2753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T879" id="Seg_2755" n="HIAT:w" s="T878">qättɨmmɨntä</ts>
                  <nts id="Seg_2756" n="HIAT:ip">.</nts>
                  <nts id="Seg_2757" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T894" id="Seg_2759" n="HIAT:u" s="T879">
                  <ts e="T880" id="Seg_2761" n="HIAT:w" s="T879">Nɨːnä</ts>
                  <nts id="Seg_2762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T881" id="Seg_2764" n="HIAT:w" s="T880">šitɨmtalʼi</ts>
                  <nts id="Seg_2765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T882" id="Seg_2767" n="HIAT:w" s="T881">šitɨ</ts>
                  <nts id="Seg_2768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T883" id="Seg_2770" n="HIAT:w" s="T882">čʼammantə</ts>
                  <nts id="Seg_2771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T884" id="Seg_2773" n="HIAT:w" s="T883">montɨ</ts>
                  <nts id="Seg_2774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T885" id="Seg_2776" n="HIAT:w" s="T884">mɨta</ts>
                  <nts id="Seg_2777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T886" id="Seg_2779" n="HIAT:w" s="T885">qälilʼ</ts>
                  <nts id="Seg_2780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T887" id="Seg_2782" n="HIAT:w" s="T886">tɨmnʼäsɨt</ts>
                  <nts id="Seg_2783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T888" id="Seg_2785" n="HIAT:w" s="T887">Loptɨlʼ</ts>
                  <nts id="Seg_2786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T889" id="Seg_2788" n="HIAT:w" s="T888">nılʼčʼik</ts>
                  <nts id="Seg_2789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T890" id="Seg_2791" n="HIAT:w" s="T889">qumpɔːtɨt</ts>
                  <nts id="Seg_2792" n="HIAT:ip">,</nts>
                  <nts id="Seg_2793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T891" id="Seg_2795" n="HIAT:w" s="T890">ɔːmɨnʼeːtɨt</ts>
                  <nts id="Seg_2796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T892" id="Seg_2798" n="HIAT:w" s="T891">čʼontoːmɨt</ts>
                  <nts id="Seg_2799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T893" id="Seg_2801" n="HIAT:w" s="T892">šitə</ts>
                  <nts id="Seg_2802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T894" id="Seg_2804" n="HIAT:w" s="T893">säpäıːmpa</ts>
                  <nts id="Seg_2805" n="HIAT:ip">.</nts>
                  <nts id="Seg_2806" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T916" id="Seg_2808" n="HIAT:u" s="T894">
                  <ts e="T895" id="Seg_2810" n="HIAT:w" s="T894">Ɔːmɨnʼeːtɨt</ts>
                  <nts id="Seg_2811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T896" id="Seg_2813" n="HIAT:w" s="T895">kɨm</ts>
                  <nts id="Seg_2814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T897" id="Seg_2816" n="HIAT:w" s="T896">šitə</ts>
                  <nts id="Seg_2817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898" id="Seg_2819" n="HIAT:w" s="T897">pasəıːmpa</ts>
                  <nts id="Seg_2820" n="HIAT:ip">,</nts>
                  <nts id="Seg_2821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T899" id="Seg_2823" n="HIAT:w" s="T898">a</ts>
                  <nts id="Seg_2824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T900" id="Seg_2826" n="HIAT:w" s="T899">poːsɨ</ts>
                  <nts id="Seg_2827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T901" id="Seg_2829" n="HIAT:w" s="T900">kɨpa</ts>
                  <nts id="Seg_2830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T902" id="Seg_2832" n="HIAT:w" s="T901">qəːtsɨmɨlʼ</ts>
                  <nts id="Seg_2833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T903" id="Seg_2835" n="HIAT:w" s="T902">timnʼatɨt</ts>
                  <nts id="Seg_2836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T904" id="Seg_2838" n="HIAT:w" s="T903">muntɨ</ts>
                  <nts id="Seg_2839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T905" id="Seg_2841" n="HIAT:w" s="T904">mɨta</ts>
                  <nts id="Seg_2842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T906" id="Seg_2844" n="HIAT:w" s="T905">olotɨ</ts>
                  <nts id="Seg_2845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T907" id="Seg_2847" n="HIAT:w" s="T906">meːl</ts>
                  <nts id="Seg_2848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T908" id="Seg_2850" n="HIAT:w" s="T907">čʼäː</ts>
                  <nts id="Seg_2851" n="HIAT:ip">,</nts>
                  <nts id="Seg_2852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909" id="Seg_2854" n="HIAT:w" s="T908">a</ts>
                  <nts id="Seg_2855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T910" id="Seg_2857" n="HIAT:w" s="T909">na</ts>
                  <nts id="Seg_2858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T911" id="Seg_2860" n="HIAT:w" s="T910">moːtɨkɔːl</ts>
                  <nts id="Seg_2861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T912" id="Seg_2863" n="HIAT:w" s="T911">mɔːt</ts>
                  <nts id="Seg_2864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T913" id="Seg_2866" n="HIAT:w" s="T912">čʼaŋalʼ</ts>
                  <nts id="Seg_2867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T914" id="Seg_2869" n="HIAT:w" s="T913">moːtɨkɔːl</ts>
                  <nts id="Seg_2870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T915" id="Seg_2872" n="HIAT:w" s="T914">tɔːpɨ</ts>
                  <nts id="Seg_2873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T916" id="Seg_2875" n="HIAT:w" s="T915">qalimmɨnta</ts>
                  <nts id="Seg_2876" n="HIAT:ip">.</nts>
                  <nts id="Seg_2877" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T924" id="Seg_2879" n="HIAT:u" s="T916">
                  <ts e="T917" id="Seg_2881" n="HIAT:w" s="T916">Mɔːtɨkota</ts>
                  <nts id="Seg_2882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T918" id="Seg_2884" n="HIAT:w" s="T917">ɛj</ts>
                  <nts id="Seg_2885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T919" id="Seg_2887" n="HIAT:w" s="T918">mɔːtɨqop</ts>
                  <nts id="Seg_2888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T920" id="Seg_2890" n="HIAT:w" s="T919">täːlʼšɨlʼ</ts>
                  <nts id="Seg_2891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T921" id="Seg_2893" n="HIAT:w" s="T920">tät</ts>
                  <nts id="Seg_2894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T922" id="Seg_2896" n="HIAT:w" s="T921">toːt</ts>
                  <nts id="Seg_2897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T923" id="Seg_2899" n="HIAT:w" s="T922">älpät</ts>
                  <nts id="Seg_2900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T924" id="Seg_2902" n="HIAT:w" s="T923">ippɔːtɨt</ts>
                  <nts id="Seg_2903" n="HIAT:ip">.</nts>
                  <nts id="Seg_2904" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T933" id="Seg_2906" n="HIAT:u" s="T924">
                  <ts e="T925" id="Seg_2908" n="HIAT:w" s="T924">İnnä</ts>
                  <nts id="Seg_2909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T926" id="Seg_2911" n="HIAT:w" s="T925">wešila</ts>
                  <nts id="Seg_2912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T927" id="Seg_2914" n="HIAT:w" s="T926">nılʼčʼik</ts>
                  <nts id="Seg_2915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T928" id="Seg_2917" n="HIAT:w" s="T927">šitɨ</ts>
                  <nts id="Seg_2918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T929" id="Seg_2920" n="HIAT:w" s="T928">mannɨmpatɨ</ts>
                  <nts id="Seg_2921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T930" id="Seg_2923" n="HIAT:w" s="T929">qumɨt</ts>
                  <nts id="Seg_2924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T931" id="Seg_2926" n="HIAT:w" s="T930">taqqempɨla</ts>
                  <nts id="Seg_2927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T932" id="Seg_2929" n="HIAT:w" s="T931">täpɨp</ts>
                  <nts id="Seg_2930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T933" id="Seg_2932" n="HIAT:w" s="T932">mannɨmpɔːtɨt</ts>
                  <nts id="Seg_2933" n="HIAT:ip">.</nts>
                  <nts id="Seg_2934" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T938" id="Seg_2936" n="HIAT:u" s="T933">
                  <ts e="T934" id="Seg_2938" n="HIAT:w" s="T933">A</ts>
                  <nts id="Seg_2939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T935" id="Seg_2941" n="HIAT:w" s="T934">tına</ts>
                  <nts id="Seg_2942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T936" id="Seg_2944" n="HIAT:w" s="T935">nätak</ts>
                  <nts id="Seg_2945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T937" id="Seg_2947" n="HIAT:w" s="T936">meːl</ts>
                  <nts id="Seg_2948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T938" id="Seg_2950" n="HIAT:w" s="T937">čʼäːŋka</ts>
                  <nts id="Seg_2951" n="HIAT:ip">.</nts>
                  <nts id="Seg_2952" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T950" id="Seg_2954" n="HIAT:u" s="T938">
                  <ts e="T939" id="Seg_2956" n="HIAT:w" s="T938">Toː</ts>
                  <nts id="Seg_2957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T940" id="Seg_2959" n="HIAT:w" s="T939">qəlla</ts>
                  <nts id="Seg_2960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T941" id="Seg_2962" n="HIAT:w" s="T940">mɔːtɨqopɨp</ts>
                  <nts id="Seg_2963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T942" id="Seg_2965" n="HIAT:w" s="T941">ınna</ts>
                  <nts id="Seg_2966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T943" id="Seg_2968" n="HIAT:w" s="T942">šitɨ</ts>
                  <nts id="Seg_2969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T944" id="Seg_2971" n="HIAT:w" s="T943">čʼap</ts>
                  <nts id="Seg_2972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T945" id="Seg_2974" n="HIAT:w" s="T944">nüŋɨtɨ</ts>
                  <nts id="Seg_2975" n="HIAT:ip">—</nts>
                  <nts id="Seg_2976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T946" id="Seg_2978" n="HIAT:w" s="T945">montɨ</ts>
                  <nts id="Seg_2979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T947" id="Seg_2981" n="HIAT:w" s="T946">nättak</ts>
                  <nts id="Seg_2982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T948" id="Seg_2984" n="HIAT:w" s="T947">nɨːnɨ</ts>
                  <nts id="Seg_2985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T949" id="Seg_2987" n="HIAT:w" s="T948">na</ts>
                  <nts id="Seg_2988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T950" id="Seg_2990" n="HIAT:w" s="T949">putilmɔːnna</ts>
                  <nts id="Seg_2991" n="HIAT:ip">.</nts>
                  <nts id="Seg_2992" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T955" id="Seg_2994" n="HIAT:u" s="T950">
                  <ts e="T951" id="Seg_2996" n="HIAT:w" s="T950">Aša</ts>
                  <nts id="Seg_2997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T952" id="Seg_2999" n="HIAT:w" s="T951">qatɔːtɨt</ts>
                  <nts id="Seg_3000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T953" id="Seg_3002" n="HIAT:w" s="T952">na</ts>
                  <nts id="Seg_3003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T954" id="Seg_3005" n="HIAT:w" s="T953">nɨmtɨlʼ</ts>
                  <nts id="Seg_3006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T955" id="Seg_3008" n="HIAT:w" s="T954">qumɨt</ts>
                  <nts id="Seg_3009" n="HIAT:ip">.</nts>
                  <nts id="Seg_3010" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T970" id="Seg_3012" n="HIAT:u" s="T955">
                  <ts e="T956" id="Seg_3014" n="HIAT:w" s="T955">Wänɨlʼ</ts>
                  <nts id="Seg_3015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T957" id="Seg_3017" n="HIAT:w" s="T956">mɔːtɨp</ts>
                  <nts id="Seg_3018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T958" id="Seg_3020" n="HIAT:w" s="T957">čʼəsɨla</ts>
                  <nts id="Seg_3021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T959" id="Seg_3023" n="HIAT:w" s="T958">nɔːtɨ</ts>
                  <nts id="Seg_3024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T960" id="Seg_3026" n="HIAT:w" s="T959">na</ts>
                  <nts id="Seg_3027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T961" id="Seg_3029" n="HIAT:w" s="T960">ütɛːrpintɔːtɨt</ts>
                  <nts id="Seg_3030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T962" id="Seg_3032" n="HIAT:w" s="T961">ɛj</ts>
                  <nts id="Seg_3033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T963" id="Seg_3035" n="HIAT:w" s="T962">amɨrpɔːtɨt</ts>
                  <nts id="Seg_3036" n="HIAT:ip">,</nts>
                  <nts id="Seg_3037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T964" id="Seg_3039" n="HIAT:w" s="T963">särtätta</ts>
                  <nts id="Seg_3040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T965" id="Seg_3042" n="HIAT:w" s="T964">ira</ts>
                  <nts id="Seg_3043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T966" id="Seg_3045" n="HIAT:w" s="T965">nʼenʼämtɨ</ts>
                  <nts id="Seg_3046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T967" id="Seg_3048" n="HIAT:w" s="T966">na</ts>
                  <nts id="Seg_3049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T968" id="Seg_3051" n="HIAT:w" s="T967">ijanɨk</ts>
                  <nts id="Seg_3052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T969" id="Seg_3054" n="HIAT:w" s="T968">na</ts>
                  <nts id="Seg_3055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T970" id="Seg_3057" n="HIAT:w" s="T969">kuralʼtɨmmɨntɨt</ts>
                  <nts id="Seg_3058" n="HIAT:ip">.</nts>
                  <nts id="Seg_3059" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T978" id="Seg_3061" n="HIAT:u" s="T970">
                  <ts e="T971" id="Seg_3063" n="HIAT:w" s="T970">Ilʼmatɨlʼ</ts>
                  <nts id="Seg_3064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T972" id="Seg_3066" n="HIAT:w" s="T971">qumoːqɨp</ts>
                  <nts id="Seg_3067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T973" id="Seg_3069" n="HIAT:w" s="T972">nʼentɨ</ts>
                  <nts id="Seg_3070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T974" id="Seg_3072" n="HIAT:w" s="T973">ompɨltäntɨla</ts>
                  <nts id="Seg_3073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T975" id="Seg_3075" n="HIAT:w" s="T974">tɛːttɨkolʼ</ts>
                  <nts id="Seg_3076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T976" id="Seg_3078" n="HIAT:w" s="T975">köt</ts>
                  <nts id="Seg_3079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T977" id="Seg_3081" n="HIAT:w" s="T976">čʼeːla</ts>
                  <nts id="Seg_3082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T978" id="Seg_3084" n="HIAT:w" s="T977">ütɨmɨmɨmpɔːtɨt</ts>
                  <nts id="Seg_3085" n="HIAT:ip">.</nts>
                  <nts id="Seg_3086" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T990" id="Seg_3088" n="HIAT:u" s="T978">
                  <ts e="T979" id="Seg_3090" n="HIAT:w" s="T978">Qumaqup</ts>
                  <nts id="Seg_3091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T980" id="Seg_3093" n="HIAT:w" s="T979">nʼentɨ</ts>
                  <nts id="Seg_3094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T981" id="Seg_3096" n="HIAT:w" s="T980">omtɨlpɨla</ts>
                  <nts id="Seg_3097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T982" id="Seg_3099" n="HIAT:w" s="T981">sərtəttalʼ</ts>
                  <nts id="Seg_3100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T983" id="Seg_3102" n="HIAT:w" s="T982">timnʼäsɨt</ts>
                  <nts id="Seg_3103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T984" id="Seg_3105" n="HIAT:w" s="T983">na</ts>
                  <nts id="Seg_3106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T985" id="Seg_3108" n="HIAT:w" s="T984">ijasa</ts>
                  <nts id="Seg_3109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T986" id="Seg_3111" n="HIAT:w" s="T985">tɔː</ts>
                  <nts id="Seg_3112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T987" id="Seg_3114" n="HIAT:w" s="T986">qärtɔːtɨt</ts>
                  <nts id="Seg_3115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T988" id="Seg_3117" n="HIAT:w" s="T987">ijat</ts>
                  <nts id="Seg_3118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T989" id="Seg_3120" n="HIAT:w" s="T988">mɔːttɨ</ts>
                  <nts id="Seg_3121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T990" id="Seg_3123" n="HIAT:w" s="T989">qanɨktä</ts>
                  <nts id="Seg_3124" n="HIAT:ip">.</nts>
                  <nts id="Seg_3125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T999" id="Seg_3127" n="HIAT:u" s="T990">
                  <ts e="T991" id="Seg_3129" n="HIAT:w" s="T990">Nʼentɨ</ts>
                  <nts id="Seg_3130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T992" id="Seg_3132" n="HIAT:w" s="T991">palla</ts>
                  <nts id="Seg_3133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T993" id="Seg_3135" n="HIAT:w" s="T992">na</ts>
                  <nts id="Seg_3136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T994" id="Seg_3138" n="HIAT:w" s="T993">ilimmɨntɔːtɨt</ts>
                  <nts id="Seg_3139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T995" id="Seg_3141" n="HIAT:w" s="T994">i</ts>
                  <nts id="Seg_3142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T996" id="Seg_3144" n="HIAT:w" s="T995">tiːtä</ts>
                  <nts id="Seg_3145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T997" id="Seg_3147" n="HIAT:w" s="T996">qos</ts>
                  <nts id="Seg_3148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T998" id="Seg_3150" n="HIAT:w" s="T997">to</ts>
                  <nts id="Seg_3151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T999" id="Seg_3153" n="HIAT:w" s="T998">ilimtɔːtɨt</ts>
                  <nts id="Seg_3154" n="HIAT:ip">.</nts>
                  <nts id="Seg_3155" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1005" id="Seg_3157" n="HIAT:u" s="T999">
                  <ts e="T1000" id="Seg_3159" n="HIAT:w" s="T999">Čʼäpta</ts>
                  <nts id="Seg_3160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1001" id="Seg_3162" n="HIAT:w" s="T1000">nʼeːmit</ts>
                  <nts id="Seg_3163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1002" id="Seg_3165" n="HIAT:w" s="T1001">toː</ts>
                  <nts id="Seg_3166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1003" id="Seg_3168" n="HIAT:w" s="T1002">na</ts>
                  <nts id="Seg_3169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1004" id="Seg_3171" n="HIAT:w" s="T1003">seːpʼe</ts>
                  <nts id="Seg_3172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1005" id="Seg_3174" n="HIAT:w" s="T1004">ɛːmmɨntɨ</ts>
                  <nts id="Seg_3175" n="HIAT:ip">.</nts>
                  <nts id="Seg_3176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T1005" id="Seg_3177" n="sc" s="T82">
               <ts e="T83" id="Seg_3179" n="e" s="T82">Laptɨlʼ </ts>
               <ts e="T84" id="Seg_3181" n="e" s="T83">nɔːkur </ts>
               <ts e="T85" id="Seg_3183" n="e" s="T84">timnʼäsɨtɨ. </ts>
               <ts e="T86" id="Seg_3185" n="e" s="T85">Šite </ts>
               <ts e="T87" id="Seg_3187" n="e" s="T86">ämäsäqä </ts>
               <ts e="T88" id="Seg_3189" n="e" s="T87">ilɨmmɨntɔː. </ts>
               <ts e="T89" id="Seg_3191" n="e" s="T88">Ijatɨ </ts>
               <ts e="T90" id="Seg_3193" n="e" s="T89">nɔːtɨ </ts>
               <ts e="T91" id="Seg_3195" n="e" s="T90">qumtɨšak </ts>
               <ts e="T92" id="Seg_3197" n="e" s="T91">orɨmpa. </ts>
               <ts e="T93" id="Seg_3199" n="e" s="T92">Iːjatɨ </ts>
               <ts e="T94" id="Seg_3201" n="e" s="T93">namɨm </ts>
               <ts e="T95" id="Seg_3203" n="e" s="T94">aša </ts>
               <ts e="T96" id="Seg_3205" n="e" s="T95">tɛnɨmɨmpat </ts>
               <ts e="T97" id="Seg_3207" n="e" s="T96">äsɨtɨ </ts>
               <ts e="T98" id="Seg_3209" n="e" s="T97">kušat </ts>
               <ts e="T99" id="Seg_3211" n="e" s="T98">qumpa. </ts>
               <ts e="T100" id="Seg_3213" n="e" s="T99">Ukkur </ts>
               <ts e="T101" id="Seg_3215" n="e" s="T100">tapčʼeːlʼi </ts>
               <ts e="T102" id="Seg_3217" n="e" s="T101">iːja </ts>
               <ts e="T103" id="Seg_3219" n="e" s="T102">əmɨntɨnɨk </ts>
               <ts e="T104" id="Seg_3221" n="e" s="T103">nalʼat </ts>
               <ts e="T105" id="Seg_3223" n="e" s="T104">ɛsa. </ts>
               <ts e="T106" id="Seg_3225" n="e" s="T105">Ama </ts>
               <ts e="T107" id="Seg_3227" n="e" s="T106">man </ts>
               <ts e="T108" id="Seg_3229" n="e" s="T107">äsanɨ </ts>
               <ts e="T109" id="Seg_3231" n="e" s="T108">kurampɨlʼ </ts>
               <ts e="T110" id="Seg_3233" n="e" s="T109">ɔːtatɨ </ts>
               <ts e="T111" id="Seg_3235" n="e" s="T110">kuni </ts>
               <ts e="T112" id="Seg_3237" n="e" s="T111">mäkkä </ts>
               <ts e="T113" id="Seg_3239" n="e" s="T112">atɨltätɨ. </ts>
               <ts e="T114" id="Seg_3241" n="e" s="T113">A </ts>
               <ts e="T115" id="Seg_3243" n="e" s="T114">ɔːtatɨj </ts>
               <ts e="T116" id="Seg_3245" n="e" s="T115">əmäsɨqa </ts>
               <ts e="T117" id="Seg_3247" n="e" s="T116">nɔːnɨ </ts>
               <ts e="T118" id="Seg_3249" n="e" s="T117">(nanɨrɨk) </ts>
               <ts e="T119" id="Seg_3251" n="e" s="T118">ɛppa – </ts>
               <ts e="T120" id="Seg_3253" n="e" s="T119">tətɨntɨsa </ts>
               <ts e="T121" id="Seg_3255" n="e" s="T120">ampatɨ, </ts>
               <ts e="T122" id="Seg_3257" n="e" s="T121">a </ts>
               <ts e="T123" id="Seg_3259" n="e" s="T122">ɔːtantij </ts>
               <ts e="T124" id="Seg_3261" n="e" s="T123">nılʼčʼilʼ </ts>
               <ts e="T125" id="Seg_3263" n="e" s="T124">tɔːqaštij </ts>
               <ts e="T126" id="Seg_3265" n="e" s="T125">ɛppɨntɨ. </ts>
               <ts e="T127" id="Seg_3267" n="e" s="T126">Nʼuːqotɨ </ts>
               <ts e="T128" id="Seg_3269" n="e" s="T127">qəːlɨt </ts>
               <ts e="T129" id="Seg_3271" n="e" s="T128">qopɨtɨ </ts>
               <ts e="T130" id="Seg_3273" n="e" s="T129">kuraktɨ </ts>
               <ts e="T131" id="Seg_3275" n="e" s="T130">ɛppa. </ts>
               <ts e="T132" id="Seg_3277" n="e" s="T131">Namɨmtɨ </ts>
               <ts e="T133" id="Seg_3279" n="e" s="T132">karnʼaltila </ts>
               <ts e="T134" id="Seg_3281" n="e" s="T133">ɔːtamtip </ts>
               <ts e="T135" id="Seg_3283" n="e" s="T134">nılʼčʼik </ts>
               <ts e="T136" id="Seg_3285" n="e" s="T135">moqona </ts>
               <ts e="T137" id="Seg_3287" n="e" s="T136">tɔːqqɨqolʼimɨmpatij. </ts>
               <ts e="T138" id="Seg_3289" n="e" s="T137">Əmɨtɨ </ts>
               <ts e="T139" id="Seg_3291" n="e" s="T138">naːlʼät </ts>
               <ts e="T140" id="Seg_3293" n="e" s="T139">ɛsa. </ts>
               <ts e="T141" id="Seg_3295" n="e" s="T140">Tan </ts>
               <ts e="T142" id="Seg_3297" n="e" s="T141">ɔːtasa </ts>
               <ts e="T143" id="Seg_3299" n="e" s="T142">kučʼa </ts>
               <ts e="T144" id="Seg_3301" n="e" s="T143">qənnantɨ? </ts>
               <ts e="T145" id="Seg_3303" n="e" s="T144">Ija </ts>
               <ts e="T146" id="Seg_3305" n="e" s="T145">naːlʼat </ts>
               <ts e="T147" id="Seg_3307" n="e" s="T146">ɛsa. </ts>
               <ts e="T148" id="Seg_3309" n="e" s="T147">Man </ts>
               <ts e="T149" id="Seg_3311" n="e" s="T148">nʼi </ts>
               <ts e="T150" id="Seg_3313" n="e" s="T149">kučʼatij </ts>
               <ts e="T151" id="Seg_3315" n="e" s="T150">äša </ts>
               <ts e="T152" id="Seg_3317" n="e" s="T151">qəntak. </ts>
               <ts e="T153" id="Seg_3319" n="e" s="T152">Mat </ts>
               <ts e="T154" id="Seg_3321" n="e" s="T153">tap </ts>
               <ts e="T155" id="Seg_3323" n="e" s="T154">puršɨmɔːtänoːqa. </ts>
               <ts e="T156" id="Seg_3325" n="e" s="T155">Karrät </ts>
               <ts e="T157" id="Seg_3327" n="e" s="T156">nʼärot </ts>
               <ts e="T158" id="Seg_3329" n="e" s="T157">qoltɛntak. </ts>
               <ts e="T159" id="Seg_3331" n="e" s="T158">Əmɨtɨ </ts>
               <ts e="T160" id="Seg_3333" n="e" s="T159">aša </ts>
               <ts e="T161" id="Seg_3335" n="e" s="T160">qatɨ. </ts>
               <ts e="T162" id="Seg_3337" n="e" s="T161">Qaːlʼimɨt </ts>
               <ts e="T163" id="Seg_3339" n="e" s="T162">puːtot </ts>
               <ts e="T164" id="Seg_3341" n="e" s="T163">taršältɨlä </ts>
               <ts e="T165" id="Seg_3343" n="e" s="T164">nʼamtɨ </ts>
               <ts e="T166" id="Seg_3345" n="e" s="T165">čʼarɨlʼ </ts>
               <ts e="T167" id="Seg_3347" n="e" s="T166">ɔːtalʼ </ts>
               <ts e="T168" id="Seg_3349" n="e" s="T167">orasilʼ </ts>
               <ts e="T169" id="Seg_3351" n="e" s="T168">tɛːmtɨ </ts>
               <ts e="T170" id="Seg_3353" n="e" s="T169">ılla </ts>
               <ts e="T171" id="Seg_3355" n="e" s="T170">iːntɨtɨ, </ts>
               <ts e="T172" id="Seg_3357" n="e" s="T171">montɨ </ts>
               <ts e="T173" id="Seg_3359" n="e" s="T172">mɨta </ts>
               <ts e="T174" id="Seg_3361" n="e" s="T173">poːlʼ </ts>
               <ts e="T175" id="Seg_3363" n="e" s="T174">kalat </ts>
               <ts e="T176" id="Seg_3365" n="e" s="T175">paqqət </ts>
               <ts e="T177" id="Seg_3367" n="e" s="T176">čʼarɨm </ts>
               <ts e="T178" id="Seg_3369" n="e" s="T177">eŋa. </ts>
               <ts e="T179" id="Seg_3371" n="e" s="T178">Iːjanɨk </ts>
               <ts e="T180" id="Seg_3373" n="e" s="T179">nılʼčʼɨŋä </ts>
               <ts e="T181" id="Seg_3375" n="e" s="T180">pɨŋtalʼtɨmmɨntɨ. </ts>
               <ts e="T182" id="Seg_3377" n="e" s="T181">Karrä </ts>
               <ts e="T183" id="Seg_3379" n="e" s="T182">tap </ts>
               <ts e="T184" id="Seg_3381" n="e" s="T183">ɔːtäntɨ </ts>
               <ts e="T185" id="Seg_3383" n="e" s="T184">puːtoːqɨt </ts>
               <ts e="T186" id="Seg_3385" n="e" s="T185">nɔːkɨr </ts>
               <ts e="T187" id="Seg_3387" n="e" s="T186">pekɨralʼ </ts>
               <ts e="T188" id="Seg_3389" n="e" s="T187">qoptɨ </ts>
               <ts e="T189" id="Seg_3391" n="e" s="T188">na </ts>
               <ts e="T190" id="Seg_3393" n="e" s="T189">ɛːntɨ. </ts>
               <ts e="T191" id="Seg_3395" n="e" s="T190">Ukkur </ts>
               <ts e="T192" id="Seg_3397" n="e" s="T191">pekɨralʼ </ts>
               <ts e="T193" id="Seg_3399" n="e" s="T192">qoptɨ </ts>
               <ts e="T194" id="Seg_3401" n="e" s="T193">na </ts>
               <ts e="T195" id="Seg_3403" n="e" s="T194">ɛːntɨ </ts>
               <ts e="T196" id="Seg_3405" n="e" s="T195">wärqɨlɔːqɨlʼ </ts>
               <ts e="T197" id="Seg_3407" n="e" s="T196">nam </ts>
               <ts e="T198" id="Seg_3409" n="e" s="T197">orqoltɨ. </ts>
               <ts e="T199" id="Seg_3411" n="e" s="T198">Toːnna </ts>
               <ts e="T200" id="Seg_3413" n="e" s="T199">mäːqıj </ts>
               <ts e="T201" id="Seg_3415" n="e" s="T200">ontıːj </ts>
               <ts e="T202" id="Seg_3417" n="e" s="T201">əːtɨla </ts>
               <ts e="T203" id="Seg_3419" n="e" s="T202">na </ts>
               <ts e="T204" id="Seg_3421" n="e" s="T203">tünɨntɔːqıj. </ts>
               <ts e="T205" id="Seg_3423" n="e" s="T204">Ija </ts>
               <ts e="T206" id="Seg_3425" n="e" s="T205">tɛːmnɨmtɨ </ts>
               <ts e="T207" id="Seg_3427" n="e" s="T206">kəš </ts>
               <ts e="T208" id="Seg_3429" n="e" s="T207">mɨnto </ts>
               <ts e="T209" id="Seg_3431" n="e" s="T208">na </ts>
               <ts e="T210" id="Seg_3433" n="e" s="T209">totqɨlpɨntɨtɨ. </ts>
               <ts e="T211" id="Seg_3435" n="e" s="T210">Nılʼčʼɨk </ts>
               <ts e="T212" id="Seg_3437" n="e" s="T211">karrä </ts>
               <ts e="T213" id="Seg_3439" n="e" s="T212">nä </ts>
               <ts e="T214" id="Seg_3441" n="e" s="T213">qənmmɨntɨ. </ts>
               <ts e="T215" id="Seg_3443" n="e" s="T214">Ɔːtantɨ </ts>
               <ts e="T216" id="Seg_3445" n="e" s="T215">puːtot </ts>
               <ts e="T217" id="Seg_3447" n="e" s="T216">peːkɨlla </ts>
               <ts e="T218" id="Seg_3449" n="e" s="T217">tına </ts>
               <ts e="T219" id="Seg_3451" n="e" s="T218">əmɨntɨ </ts>
               <ts e="T220" id="Seg_3453" n="e" s="T219">tompɨlʼ </ts>
               <ts e="T221" id="Seg_3455" n="e" s="T220">qoptɨp </ts>
               <ts e="T222" id="Seg_3457" n="e" s="T221">na </ts>
               <ts e="T223" id="Seg_3459" n="e" s="T222">orqɨlʼpantɨt. </ts>
               <ts e="T224" id="Seg_3461" n="e" s="T223">Konna </ts>
               <ts e="T225" id="Seg_3463" n="e" s="T224">tatɨŋɨtɨ </ts>
               <ts e="T226" id="Seg_3465" n="e" s="T225">sɔːralqə. </ts>
               <ts e="T227" id="Seg_3467" n="e" s="T226">Nat </ts>
               <ts e="T228" id="Seg_3469" n="e" s="T227">kuntə </ts>
               <ts e="T229" id="Seg_3471" n="e" s="T228">əmɨtɨ </ts>
               <ts e="T230" id="Seg_3473" n="e" s="T229">munte </ts>
               <ts e="T231" id="Seg_3475" n="e" s="T230">mɨta </ts>
               <ts e="T232" id="Seg_3477" n="e" s="T231">qalɨtɨt </ts>
               <ts e="T233" id="Seg_3479" n="e" s="T232">puːtoːqɨt </ts>
               <ts e="T234" id="Seg_3481" n="e" s="T233">nılʼčʼi </ts>
               <ts e="T235" id="Seg_3483" n="e" s="T234">qallɨ </ts>
               <ts e="T236" id="Seg_3485" n="e" s="T235">ılla </ts>
               <ts e="T237" id="Seg_3487" n="e" s="T236">iːmmɨntɨtɨ </ts>
               <ts e="T238" id="Seg_3489" n="e" s="T237">mɨnta </ts>
               <ts e="T239" id="Seg_3491" n="e" s="T238">mɨta </ts>
               <ts e="T240" id="Seg_3493" n="e" s="T239">nɔssarɨlʼ </ts>
               <ts e="T241" id="Seg_3495" n="e" s="T240">košar </ts>
               <ts e="T242" id="Seg_3497" n="e" s="T241">nɔːmtɨlʼ </ts>
               <ts e="T243" id="Seg_3499" n="e" s="T242">topɨtä. </ts>
               <ts e="T244" id="Seg_3501" n="e" s="T243">Ija </ts>
               <ts e="T245" id="Seg_3503" n="e" s="T244">qaːlɨmtɨ </ts>
               <ts e="T246" id="Seg_3505" n="e" s="T245">na </ts>
               <ts e="T247" id="Seg_3507" n="e" s="T246">saralqulammɨntɨ. </ts>
               <ts e="T248" id="Seg_3509" n="e" s="T247">Munta </ts>
               <ts e="T249" id="Seg_3511" n="e" s="T248">mɨta </ts>
               <ts e="T250" id="Seg_3513" n="e" s="T249">tına </ts>
               <ts e="T251" id="Seg_3515" n="e" s="T250">šittɨ </ts>
               <ts e="T252" id="Seg_3517" n="e" s="T251">pekaralʼ </ts>
               <ts e="T253" id="Seg_3519" n="e" s="T252">kɨpalɔːlʼ </ts>
               <ts e="T254" id="Seg_3521" n="e" s="T253">qoptoːqıt </ts>
               <ts e="T255" id="Seg_3523" n="e" s="T254">ontıː </ts>
               <ts e="T256" id="Seg_3525" n="e" s="T255">əːtɨla </ts>
               <ts e="T257" id="Seg_3527" n="e" s="T256">montɨ </ts>
               <ts e="T258" id="Seg_3529" n="e" s="T257">mɨta </ts>
               <ts e="T259" id="Seg_3531" n="e" s="T258">konna </ts>
               <ts e="T260" id="Seg_3533" n="e" s="T259">na </ts>
               <ts e="T261" id="Seg_3535" n="e" s="T260">tüːmmɨntɔː. </ts>
               <ts e="T262" id="Seg_3537" n="e" s="T261">Ija </ts>
               <ts e="T263" id="Seg_3539" n="e" s="T262">qalɨmtɨ </ts>
               <ts e="T264" id="Seg_3541" n="e" s="T263">sarala </ts>
               <ts e="T265" id="Seg_3543" n="e" s="T264">na </ts>
               <ts e="T266" id="Seg_3545" n="e" s="T265">tuːrtɨmmɨntɨt. </ts>
               <ts e="T267" id="Seg_3547" n="e" s="T266">Əmɨtɨ </ts>
               <ts e="T268" id="Seg_3549" n="e" s="T267">seːlʼčʼi </ts>
               <ts e="T269" id="Seg_3551" n="e" s="T268">kočʼenɨlʼ </ts>
               <ts e="T270" id="Seg_3553" n="e" s="T269">košar </ts>
               <ts e="T271" id="Seg_3555" n="e" s="T270">nɔːmtelʼ </ts>
               <ts e="T272" id="Seg_3557" n="e" s="T271">naraposa </ts>
               <ts e="T273" id="Seg_3559" n="e" s="T272">na </ts>
               <ts e="T274" id="Seg_3561" n="e" s="T273">miːnmɨntɨtɨ. </ts>
               <ts e="T275" id="Seg_3563" n="e" s="T274">Pekɨralʼ </ts>
               <ts e="T276" id="Seg_3565" n="e" s="T275">maːtɨrla </ts>
               <ts e="T277" id="Seg_3567" n="e" s="T276">meːmpɨlʼ </ts>
               <ts e="T278" id="Seg_3569" n="e" s="T277">soqqɨsa </ts>
               <ts e="T279" id="Seg_3571" n="e" s="T278">na </ts>
               <ts e="T280" id="Seg_3573" n="e" s="T279">tokalaltɨmmɨtɨt. </ts>
               <ts e="T281" id="Seg_3575" n="e" s="T280">Šentɨ </ts>
               <ts e="T282" id="Seg_3577" n="e" s="T281">maːtɨrla </ts>
               <ts e="T283" id="Seg_3579" n="e" s="T282">meːmpɨlʼ </ts>
               <ts e="T284" id="Seg_3581" n="e" s="T283">qälilʼ </ts>
               <ts e="T285" id="Seg_3583" n="e" s="T284">kulusa </ts>
               <ts e="T286" id="Seg_3585" n="e" s="T285">na </ts>
               <ts e="T287" id="Seg_3587" n="e" s="T286">tokalaltɨmmɨntɨt. </ts>
               <ts e="T288" id="Seg_3589" n="e" s="T287">Nɨːnɨ </ts>
               <ts e="T289" id="Seg_3591" n="e" s="T288">əmɨtɨ </ts>
               <ts e="T290" id="Seg_3593" n="e" s="T289">nılʼčʼiŋa </ts>
               <ts e="T291" id="Seg_3595" n="e" s="T290">soqošpɨntɨtɨ: </ts>
               <ts e="T292" id="Seg_3597" n="e" s="T291">ɔːtantɨ </ts>
               <ts e="T293" id="Seg_3599" n="e" s="T292">olɨ </ts>
               <ts e="T294" id="Seg_3601" n="e" s="T293">ɨkɨ </ts>
               <ts e="T295" id="Seg_3603" n="e" s="T294">laqɨrätɨ. </ts>
               <ts e="T296" id="Seg_3605" n="e" s="T295">Ontɨt </ts>
               <ts e="T297" id="Seg_3607" n="e" s="T296">našte </ts>
               <ts e="T298" id="Seg_3609" n="e" s="T297">totalʼ </ts>
               <ts e="T299" id="Seg_3611" n="e" s="T298">tukɛntɔːtɨt </ts>
               <ts e="T300" id="Seg_3613" n="e" s="T299">ɛj </ts>
               <ts e="T301" id="Seg_3615" n="e" s="T300">moqanä </ts>
               <ts e="T302" id="Seg_3617" n="e" s="T301">našti </ts>
               <ts e="T303" id="Seg_3619" n="e" s="T302">taːtɛntɔːtɨt. </ts>
               <ts e="T304" id="Seg_3621" n="e" s="T303">Ija </ts>
               <ts e="T305" id="Seg_3623" n="e" s="T304">ɔːqaltɨ </ts>
               <ts e="T306" id="Seg_3625" n="e" s="T305">tartaltɨlä </ts>
               <ts e="T307" id="Seg_3627" n="e" s="T306">ınna </ts>
               <ts e="T308" id="Seg_3629" n="e" s="T307">omtɨlʼa </ts>
               <ts e="T309" id="Seg_3631" n="e" s="T308">ɔːtantɨ </ts>
               <ts e="T310" id="Seg_3633" n="e" s="T309">olɨp </ts>
               <ts e="T311" id="Seg_3635" n="e" s="T310">nʼena </ts>
               <ts e="T312" id="Seg_3637" n="e" s="T311">na </ts>
               <ts e="T313" id="Seg_3639" n="e" s="T312">üːtɨmmɨntɨ. </ts>
               <ts e="T314" id="Seg_3641" n="e" s="T313">Nɨːnɨ </ts>
               <ts e="T315" id="Seg_3643" n="e" s="T314">nʼennɨ </ts>
               <ts e="T316" id="Seg_3645" n="e" s="T315">laqaltiptäːqɨntɨ </ts>
               <ts e="T317" id="Seg_3647" n="e" s="T316">qaj </ts>
               <ts e="T318" id="Seg_3649" n="e" s="T317">čʼumpɨk </ts>
               <ts e="T319" id="Seg_3651" n="e" s="T318">qänɨmpa, </ts>
               <ts e="T320" id="Seg_3653" n="e" s="T319">qaj </ts>
               <ts e="T321" id="Seg_3655" n="e" s="T320">qɔːmɨčʼäk. </ts>
               <ts e="T322" id="Seg_3657" n="e" s="T321">Ukkur </ts>
               <ts e="T323" id="Seg_3659" n="e" s="T322">nʼarqɨlʼ </ts>
               <ts e="T324" id="Seg_3661" n="e" s="T323">kotpas </ts>
               <ts e="T325" id="Seg_3663" n="e" s="T324">pɔːrɨntɨ </ts>
               <ts e="T326" id="Seg_3665" n="e" s="T325">na </ts>
               <ts e="T327" id="Seg_3667" n="e" s="T326">omtij </ts>
               <ts e="T328" id="Seg_3669" n="e" s="T327">ɛːmmɨntɨ. </ts>
               <ts e="T329" id="Seg_3671" n="e" s="T328">Kotpas </ts>
               <ts e="T330" id="Seg_3673" n="e" s="T329">pɔːroːqot </ts>
               <ts e="T331" id="Seg_3675" n="e" s="T330">ɔːmtɨla </ts>
               <ts e="T332" id="Seg_3677" n="e" s="T331">nılʼčʼik </ts>
               <ts e="T333" id="Seg_3679" n="e" s="T332">qoŋotɨ </ts>
               <ts e="T334" id="Seg_3681" n="e" s="T333">montɨ </ts>
               <ts e="T335" id="Seg_3683" n="e" s="T334">mɨta </ts>
               <ts e="T336" id="Seg_3685" n="e" s="T335">karrät </ts>
               <ts e="T337" id="Seg_3687" n="e" s="T336">toːt </ts>
               <ts e="T338" id="Seg_3689" n="e" s="T337">qanaqqɨt </ts>
               <ts e="T339" id="Seg_3691" n="e" s="T338">nɔːkɨr </ts>
               <ts e="T340" id="Seg_3693" n="e" s="T339">mɔːt </ts>
               <ts e="T341" id="Seg_3695" n="e" s="T340">ɔːmnontɨ. </ts>
               <ts e="T342" id="Seg_3697" n="e" s="T341">Na </ts>
               <ts e="T343" id="Seg_3699" n="e" s="T342">mɔːtoqanaqɨt </ts>
               <ts e="T344" id="Seg_3701" n="e" s="T343">qälʼi </ts>
               <ts e="T345" id="Seg_3703" n="e" s="T344">ɔːtä </ts>
               <ts e="T346" id="Seg_3705" n="e" s="T345">kuralɨmmɨntɔːtɨt. </ts>
               <ts e="T347" id="Seg_3707" n="e" s="T346">Ija </ts>
               <ts e="T348" id="Seg_3709" n="e" s="T347">aša </ts>
               <ts e="T349" id="Seg_3711" n="e" s="T348">qata, </ts>
               <ts e="T350" id="Seg_3713" n="e" s="T349">mɔːtta </ts>
               <ts e="T351" id="Seg_3715" n="e" s="T350">karala </ts>
               <ts e="T352" id="Seg_3717" n="e" s="T351">qaltɛlʼčʼe. </ts>
               <ts e="T353" id="Seg_3719" n="e" s="T352">Čʼontoːqɨntɨ </ts>
               <ts e="T354" id="Seg_3721" n="e" s="T353">onti </ts>
               <ts e="T355" id="Seg_3723" n="e" s="T354">mɨta </ts>
               <ts e="T356" id="Seg_3725" n="e" s="T355">seːpɨlak </ts>
               <ts e="T357" id="Seg_3727" n="e" s="T356">wärqa </ts>
               <ts e="T358" id="Seg_3729" n="e" s="T357">mɔːt </ts>
               <ts e="T359" id="Seg_3731" n="e" s="T358">ɔːmnɨntɨ. </ts>
               <ts e="T360" id="Seg_3733" n="e" s="T359">Na </ts>
               <ts e="T361" id="Seg_3735" n="e" s="T360">wärqɨ </ts>
               <ts e="T362" id="Seg_3737" n="e" s="T361">mɔːtanoːt </ts>
               <ts e="T363" id="Seg_3739" n="e" s="T362">čʼɔːtɨ </ts>
               <ts e="T364" id="Seg_3741" n="e" s="T363">ılla </ts>
               <ts e="T365" id="Seg_3743" n="e" s="T364">uterɛlʼčʼiŋɨtɨ. </ts>
               <ts e="T366" id="Seg_3745" n="e" s="T365">İlla </ts>
               <ts e="T367" id="Seg_3747" n="e" s="T366">uterɛːmpɨla </ts>
               <ts e="T368" id="Seg_3749" n="e" s="T367">qalintɨ </ts>
               <ts e="T369" id="Seg_3751" n="e" s="T368">šünʼčʼoːqɨt </ts>
               <ts e="T370" id="Seg_3753" n="e" s="T369">ɔːmttä. </ts>
               <ts e="T371" id="Seg_3755" n="e" s="T370">Mɔːtqɨnɨ </ts>
               <ts e="T372" id="Seg_3757" n="e" s="T371">ponä </ts>
               <ts e="T373" id="Seg_3759" n="e" s="T372">qumɨt </ts>
               <ts e="T374" id="Seg_3761" n="e" s="T373">nʼeːja </ts>
               <ts e="T375" id="Seg_3763" n="e" s="T374">tattɨmmɨntɔːtɨt. </ts>
               <ts e="T376" id="Seg_3765" n="e" s="T375">Ukkur </ts>
               <ts e="T377" id="Seg_3767" n="e" s="T376">ira </ts>
               <ts e="T378" id="Seg_3769" n="e" s="T377">montɨ </ts>
               <ts e="T379" id="Seg_3771" n="e" s="T378">mɨt </ts>
               <ts e="T380" id="Seg_3773" n="e" s="T379">ijanɨk </ts>
               <ts e="T381" id="Seg_3775" n="e" s="T380">tüːla </ts>
               <ts e="T382" id="Seg_3777" n="e" s="T381">naːlʼät </ts>
               <ts e="T383" id="Seg_3779" n="e" s="T382">ɛsɨmmɨntɨ. </ts>
               <ts e="T384" id="Seg_3781" n="e" s="T383">Qaːlʼ </ts>
               <ts e="T385" id="Seg_3783" n="e" s="T384">tɨtɨt </ts>
               <ts e="T386" id="Seg_3785" n="e" s="T385">qumantɨ </ts>
               <ts e="T387" id="Seg_3787" n="e" s="T386">ɛsantɨ? </ts>
               <ts e="T388" id="Seg_3789" n="e" s="T387">Iːja </ts>
               <ts e="T389" id="Seg_3791" n="e" s="T388">nälʼät </ts>
               <ts e="T390" id="Seg_3793" n="e" s="T389">ɛsɨmmɨntɨ: </ts>
               <ts e="T391" id="Seg_3795" n="e" s="T390">Man </ts>
               <ts e="T392" id="Seg_3797" n="e" s="T391">nʼe </ts>
               <ts e="T393" id="Seg_3799" n="e" s="T392">qumɨm </ts>
               <ts e="T394" id="Seg_3801" n="e" s="T393">aša </ts>
               <ts e="T395" id="Seg_3803" n="e" s="T394">tɛnʼimap, </ts>
               <ts e="T396" id="Seg_3805" n="e" s="T395">nʼe </ts>
               <ts e="T397" id="Seg_3807" n="e" s="T396">qaim </ts>
               <ts e="T398" id="Seg_3809" n="e" s="T397">aša </ts>
               <ts e="T399" id="Seg_3811" n="e" s="T398">tɛnimap. </ts>
               <ts e="T400" id="Seg_3813" n="e" s="T399">Pɛlʼikɔːl </ts>
               <ts e="T401" id="Seg_3815" n="e" s="T400">ilʼiptäːlʼ </ts>
               <ts e="T402" id="Seg_3817" n="e" s="T401">tätan </ts>
               <ts e="T403" id="Seg_3819" n="e" s="T402">nɔːnɨ. </ts>
               <ts e="T404" id="Seg_3821" n="e" s="T403">A </ts>
               <ts e="T405" id="Seg_3823" n="e" s="T404">tan </ts>
               <ts e="T406" id="Seg_3825" n="e" s="T405">mɨta </ts>
               <ts e="T407" id="Seg_3827" n="e" s="T406">qaj </ts>
               <ts e="T408" id="Seg_3829" n="e" s="T407">iraŋɨnta </ts>
               <ts e="T409" id="Seg_3831" n="e" s="T408">ɛsantä. </ts>
               <ts e="T410" id="Seg_3833" n="e" s="T409">Na </ts>
               <ts e="T411" id="Seg_3835" n="e" s="T410">iːra </ts>
               <ts e="T412" id="Seg_3837" n="e" s="T411">nılʼčʼik </ts>
               <ts e="T413" id="Seg_3839" n="e" s="T412">kətiŋɨtɨ. </ts>
               <ts e="T414" id="Seg_3841" n="e" s="T413">Meː </ts>
               <ts e="T415" id="Seg_3843" n="e" s="T414">mɨta </ts>
               <ts e="T416" id="Seg_3845" n="e" s="T415">nɔːkur </ts>
               <ts e="T417" id="Seg_3847" n="e" s="T416">sartätalʼ </ts>
               <ts e="T418" id="Seg_3849" n="e" s="T417">tɨmnʼäsɨmɨt, </ts>
               <ts e="T419" id="Seg_3851" n="e" s="T418">poːsɨ </ts>
               <ts e="T420" id="Seg_3853" n="e" s="T419">warqɨ </ts>
               <ts e="T421" id="Seg_3855" n="e" s="T420">sartətta </ts>
               <ts e="T422" id="Seg_3857" n="e" s="T421">nɨk </ts>
               <ts e="T423" id="Seg_3859" n="e" s="T422">mat </ts>
               <ts e="T424" id="Seg_3861" n="e" s="T423">ɛːmmintak. </ts>
               <ts e="T425" id="Seg_3863" n="e" s="T424">Tına </ts>
               <ts e="T426" id="Seg_3865" n="e" s="T425">pona </ts>
               <ts e="T427" id="Seg_3867" n="e" s="T426">tannɨmpɨlʼ </ts>
               <ts e="T428" id="Seg_3869" n="e" s="T427">qumɨt </ts>
               <ts e="T429" id="Seg_3871" n="e" s="T428">montɨ </ts>
               <ts e="T430" id="Seg_3873" n="e" s="T429">mɨta </ts>
               <ts e="T431" id="Seg_3875" n="e" s="T430">ɔːtalʼ </ts>
               <ts e="T432" id="Seg_3877" n="e" s="T431">orattɔːtɨt. </ts>
               <ts e="T433" id="Seg_3879" n="e" s="T432">Na </ts>
               <ts e="T434" id="Seg_3881" n="e" s="T433">ɔːtatɨt </ts>
               <ts e="T435" id="Seg_3883" n="e" s="T434">puːtot </ts>
               <ts e="T436" id="Seg_3885" n="e" s="T435">ukkur </ts>
               <ts e="T437" id="Seg_3887" n="e" s="T436">ɔːmtɨkɨtɨlʼ </ts>
               <ts e="T438" id="Seg_3889" n="e" s="T437">qälʼilʼ </ts>
               <ts e="T439" id="Seg_3891" n="e" s="T438">waqtalʼ </ts>
               <ts e="T440" id="Seg_3893" n="e" s="T439">čʼaktɨ </ts>
               <ts e="T441" id="Seg_3895" n="e" s="T440">orantɔːtɨt. </ts>
               <ts e="T442" id="Seg_3897" n="e" s="T441">Nılʼčʼik </ts>
               <ts e="T443" id="Seg_3899" n="e" s="T442">ija </ts>
               <ts e="T444" id="Seg_3901" n="e" s="T443">mannɨmpatɨ </ts>
               <ts e="T445" id="Seg_3903" n="e" s="T444">orqɨlqa </ts>
               <ts e="T446" id="Seg_3905" n="e" s="T445">tačʼalpɔːtɨt. </ts>
               <ts e="T447" id="Seg_3907" n="e" s="T446">Ija </ts>
               <ts e="T448" id="Seg_3909" n="e" s="T447">qaːlɨntɨ </ts>
               <ts e="T449" id="Seg_3911" n="e" s="T448">šunʼčʼoːqɨt </ts>
               <ts e="T450" id="Seg_3913" n="e" s="T449">ɔːmtɨlʼä </ts>
               <ts e="T451" id="Seg_3915" n="e" s="T450">tɛːmnim </ts>
               <ts e="T452" id="Seg_3917" n="e" s="T451">tɨnä </ts>
               <ts e="T453" id="Seg_3919" n="e" s="T452">totqɨlpɨntɨtäːnɨ </ts>
               <ts e="T454" id="Seg_3921" n="e" s="T453">čʼəktɨp </ts>
               <ts e="T455" id="Seg_3923" n="e" s="T454">ijat </ts>
               <ts e="T456" id="Seg_3925" n="e" s="T455">qanaŋmɨt </ts>
               <ts e="T457" id="Seg_3927" n="e" s="T456">na </ts>
               <ts e="T458" id="Seg_3929" n="e" s="T457">mintɨralʼ </ts>
               <ts e="T459" id="Seg_3931" n="e" s="T458">tämmintɔːtɨt. </ts>
               <ts e="T460" id="Seg_3933" n="e" s="T459">Iːja </ts>
               <ts e="T461" id="Seg_3935" n="e" s="T460">čʼəktɨp </ts>
               <ts e="T462" id="Seg_3937" n="e" s="T461">tatɨraltɨlä </ts>
               <ts e="T463" id="Seg_3939" n="e" s="T462">orqɨlpɨntɨlä </ts>
               <ts e="T464" id="Seg_3941" n="e" s="T463">tɛːmtɨ </ts>
               <ts e="T465" id="Seg_3943" n="e" s="T464">čʼam </ts>
               <ts e="T466" id="Seg_3945" n="e" s="T465">mišalpatɨ </ts>
               <ts e="T467" id="Seg_3947" n="e" s="T466">čʼəktɨt </ts>
               <ts e="T468" id="Seg_3949" n="e" s="T467">püqa </ts>
               <ts e="T469" id="Seg_3951" n="e" s="T468">toːnna </ts>
               <ts e="T470" id="Seg_3953" n="e" s="T469">seːpemmɨntɨ. </ts>
               <ts e="T471" id="Seg_3955" n="e" s="T470">Ija </ts>
               <ts e="T472" id="Seg_3957" n="e" s="T471">tɛːmnimtɨ </ts>
               <ts e="T473" id="Seg_3959" n="e" s="T472">ınna </ts>
               <ts e="T474" id="Seg_3961" n="e" s="T473">na </ts>
               <ts e="T475" id="Seg_3963" n="e" s="T474">totqɨlpɨntɨ. </ts>
               <ts e="T476" id="Seg_3965" n="e" s="T475">Nalʼät </ts>
               <ts e="T477" id="Seg_3967" n="e" s="T476">ɛsɨmpa: </ts>
               <ts e="T478" id="Seg_3969" n="e" s="T477">Tap </ts>
               <ts e="T479" id="Seg_3971" n="e" s="T478">tɛːmtilʼ </ts>
               <ts e="T480" id="Seg_3973" n="e" s="T479">tomɨ </ts>
               <ts e="T481" id="Seg_3975" n="e" s="T480">kämsa </ts>
               <ts e="T482" id="Seg_3977" n="e" s="T481">qajqə </ts>
               <ts e="T483" id="Seg_3979" n="e" s="T482">neklɔːltap? </ts>
               <ts e="T484" id="Seg_3981" n="e" s="T483">Na </ts>
               <ts e="T485" id="Seg_3983" n="e" s="T484">qumɨt </ts>
               <ts e="T486" id="Seg_3985" n="e" s="T485">ɔːtam </ts>
               <ts e="T487" id="Seg_3987" n="e" s="T486">tɨnä </ts>
               <ts e="T488" id="Seg_3989" n="e" s="T487">kırammintɔːtɨt. </ts>
               <ts e="T489" id="Seg_3991" n="e" s="T488">Qälilʼ </ts>
               <ts e="T490" id="Seg_3993" n="e" s="T489">qumɨt </ts>
               <ts e="T491" id="Seg_3995" n="e" s="T490">poqonnä </ts>
               <ts e="T492" id="Seg_3997" n="e" s="T491">tuštiqolammɨntɔːtɨt. </ts>
               <ts e="T493" id="Seg_3999" n="e" s="T492">Ija </ts>
               <ts e="T494" id="Seg_4001" n="e" s="T493">nälʼät </ts>
               <ts e="T495" id="Seg_4003" n="e" s="T494">na </ts>
               <ts e="T496" id="Seg_4005" n="e" s="T495">kuraltimmɨntɔːtɨt </ts>
               <ts e="T497" id="Seg_4007" n="e" s="T496">tat </ts>
               <ts e="T498" id="Seg_4009" n="e" s="T497">lʼa </ts>
               <ts e="T499" id="Seg_4011" n="e" s="T498">meːsʼa </ts>
               <ts e="T500" id="Seg_4013" n="e" s="T499">tuštäšik. </ts>
               <ts e="T501" id="Seg_4015" n="e" s="T500">Ija </ts>
               <ts e="T502" id="Seg_4017" n="e" s="T501">nalʼat </ts>
               <ts e="T503" id="Seg_4019" n="e" s="T502">na </ts>
               <ts e="T504" id="Seg_4021" n="e" s="T503">ɛsɨmmɨntɨ: </ts>
               <ts e="T505" id="Seg_4023" n="e" s="T504">Mat </ts>
               <ts e="T506" id="Seg_4025" n="e" s="T505">tuštɨqa </ts>
               <ts e="T507" id="Seg_4027" n="e" s="T506">aša </ts>
               <ts e="T508" id="Seg_4029" n="e" s="T507">tɛnimak. </ts>
               <ts e="T509" id="Seg_4031" n="e" s="T508">Tına </ts>
               <ts e="T510" id="Seg_4033" n="e" s="T509">sər </ts>
               <ts e="T511" id="Seg_4035" n="e" s="T510">təttalʼ </ts>
               <ts e="T512" id="Seg_4037" n="e" s="T511">irra </ts>
               <ts e="T513" id="Seg_4039" n="e" s="T512">nalʼät </ts>
               <ts e="T514" id="Seg_4041" n="e" s="T513">na </ts>
               <ts e="T515" id="Seg_4043" n="e" s="T514">ɛsɨmmɨntɨ. </ts>
               <ts e="T516" id="Seg_4045" n="e" s="T515">Tɛː </ts>
               <ts e="T517" id="Seg_4047" n="e" s="T516">qajqa </ts>
               <ts e="T518" id="Seg_4049" n="e" s="T517">ɔːmtoːlet. </ts>
               <ts e="T519" id="Seg_4051" n="e" s="T518">Wäčʼit </ts>
               <ts e="T520" id="Seg_4053" n="e" s="T519">mɔːttə </ts>
               <ts e="T521" id="Seg_4055" n="e" s="T520">tuːltila </ts>
               <ts e="T522" id="Seg_4057" n="e" s="T521">mušeriŋɨlɨt. </ts>
               <ts e="T523" id="Seg_4059" n="e" s="T522">Ija </ts>
               <ts e="T524" id="Seg_4061" n="e" s="T523">mannɨmpatɨ </ts>
               <ts e="T525" id="Seg_4063" n="e" s="T524">monte </ts>
               <ts e="T526" id="Seg_4065" n="e" s="T525">mɨtä </ts>
               <ts e="T527" id="Seg_4067" n="e" s="T526">mɔːtqonä </ts>
               <ts e="T528" id="Seg_4069" n="e" s="T527">ponä </ts>
               <ts e="T529" id="Seg_4071" n="e" s="T528">ukkur </ts>
               <ts e="T530" id="Seg_4073" n="e" s="T529">nätak </ts>
               <ts e="T531" id="Seg_4075" n="e" s="T530">na </ts>
               <ts e="T532" id="Seg_4077" n="e" s="T531">tannɨmmɨntɨ. </ts>
               <ts e="T533" id="Seg_4079" n="e" s="T532">Nätak </ts>
               <ts e="T534" id="Seg_4081" n="e" s="T533">tantɨlä </ts>
               <ts e="T535" id="Seg_4083" n="e" s="T534">wäčʼip </ts>
               <ts e="T536" id="Seg_4085" n="e" s="T535">maːtalä </ts>
               <ts e="T537" id="Seg_4087" n="e" s="T536">mɔːtto </ts>
               <ts e="T538" id="Seg_4089" n="e" s="T537">nä </ts>
               <ts e="T539" id="Seg_4091" n="e" s="T538">tultimmɨntɨ. </ts>
               <ts e="T540" id="Seg_4093" n="e" s="T539">Aša </ts>
               <ts e="T541" id="Seg_4095" n="e" s="T540">kuntɨ </ts>
               <ts e="T542" id="Seg_4097" n="e" s="T541">ɔːmtäla </ts>
               <ts e="T543" id="Seg_4099" n="e" s="T542">puːla </ts>
               <ts e="T544" id="Seg_4101" n="e" s="T543">ira </ts>
               <ts e="T545" id="Seg_4103" n="e" s="T544">ɛj </ts>
               <ts e="T546" id="Seg_4105" n="e" s="T545">iːja </ts>
               <ts e="T547" id="Seg_4107" n="e" s="T546">mɔːtta </ts>
               <ts e="T548" id="Seg_4109" n="e" s="T547">nä </ts>
               <ts e="T549" id="Seg_4111" n="e" s="T548">šeːrpintɔːqıj. </ts>
               <ts e="T550" id="Seg_4113" n="e" s="T549">Ija </ts>
               <ts e="T551" id="Seg_4115" n="e" s="T550">mɔːt </ts>
               <ts e="T552" id="Seg_4117" n="e" s="T551">šeːrlʼa </ts>
               <ts e="T553" id="Seg_4119" n="e" s="T552">mɔːtan </ts>
               <ts e="T554" id="Seg_4121" n="e" s="T553">ɔːktɨ </ts>
               <ts e="T555" id="Seg_4123" n="e" s="T554">čʼam </ts>
               <ts e="T556" id="Seg_4125" n="e" s="T555">nɨllɛːjoqolampa. </ts>
               <ts e="T557" id="Seg_4127" n="e" s="T556">Ira </ts>
               <ts e="T558" id="Seg_4129" n="e" s="T557">nalʼät </ts>
               <ts e="T559" id="Seg_4131" n="e" s="T558">na </ts>
               <ts e="T560" id="Seg_4133" n="e" s="T559">ɛsɨmmɨntɨ: </ts>
               <ts e="T561" id="Seg_4135" n="e" s="T560">tɛː </ts>
               <ts e="T562" id="Seg_4137" n="e" s="T561">qaj </ts>
               <ts e="T563" id="Seg_4139" n="e" s="T562">qumɨp </ts>
               <ts e="T564" id="Seg_4141" n="e" s="T563">aša </ts>
               <ts e="T565" id="Seg_4143" n="e" s="T564">qontɔːlɨt. </ts>
               <ts e="T566" id="Seg_4145" n="e" s="T565">Nʼenna </ts>
               <ts e="T567" id="Seg_4147" n="e" s="T566">üːtoqɨntɨtqa </ts>
               <ts e="T568" id="Seg_4149" n="e" s="T567">qajqa </ts>
               <ts e="T569" id="Seg_4151" n="e" s="T568">mɔːtan </ts>
               <ts e="T570" id="Seg_4153" n="e" s="T569">ɔːktɨ </ts>
               <ts e="T571" id="Seg_4155" n="e" s="T570">nɨlʼtimpɔːlɨt. </ts>
               <ts e="T572" id="Seg_4157" n="e" s="T571">Ijap </ts>
               <ts e="T573" id="Seg_4159" n="e" s="T572">nʼenna </ts>
               <ts e="T574" id="Seg_4161" n="e" s="T573">iːrat </ts>
               <ts e="T575" id="Seg_4163" n="e" s="T574">qönʼte </ts>
               <ts e="T576" id="Seg_4165" n="e" s="T575">na </ts>
               <ts e="T577" id="Seg_4167" n="e" s="T576">omtɨltämmɨntɔːtɨt. </ts>
               <ts e="T578" id="Seg_4169" n="e" s="T577">Wäčʼitsä </ts>
               <ts e="T579" id="Seg_4171" n="e" s="T578">na </ts>
               <ts e="T580" id="Seg_4173" n="e" s="T579">tıːtalʼpintɔːtɨt </ts>
               <ts e="T581" id="Seg_4175" n="e" s="T580">selʼčʼuqɨlʼ </ts>
               <ts e="T582" id="Seg_4177" n="e" s="T581">apsɨlʼ </ts>
               <ts e="T583" id="Seg_4179" n="e" s="T582">na </ts>
               <ts e="T584" id="Seg_4181" n="e" s="T583">tottɨmmɨntɔːtɨt. </ts>
               <ts e="T585" id="Seg_4183" n="e" s="T584">Šitte </ts>
               <ts e="T586" id="Seg_4185" n="e" s="T585">qumoːqıj </ts>
               <ts e="T587" id="Seg_4187" n="e" s="T586">pona </ts>
               <ts e="T588" id="Seg_4189" n="e" s="T587">tantɨla </ts>
               <ts e="T589" id="Seg_4191" n="e" s="T588">ütɨp </ts>
               <ts e="T590" id="Seg_4193" n="e" s="T589">mɔːttɨ </ts>
               <ts e="T591" id="Seg_4195" n="e" s="T590">nä </ts>
               <ts e="T592" id="Seg_4197" n="e" s="T591">tültimmontətij. </ts>
               <ts e="T593" id="Seg_4199" n="e" s="T592">Na </ts>
               <ts e="T594" id="Seg_4201" n="e" s="T593">ütim </ts>
               <ts e="T595" id="Seg_4203" n="e" s="T594">peralintɔːtɨt </ts>
               <ts e="T596" id="Seg_4205" n="e" s="T595">ɛj </ts>
               <ts e="T597" id="Seg_4207" n="e" s="T596">amɨrqulʼammɨntɔːtɨt. </ts>
               <ts e="T598" id="Seg_4209" n="e" s="T597">Iːja </ts>
               <ts e="T599" id="Seg_4211" n="e" s="T598">amɨrä </ts>
               <ts e="T600" id="Seg_4213" n="e" s="T599">čʼuntoːqɨt </ts>
               <ts e="T601" id="Seg_4215" n="e" s="T600">nılʼčʼiŋä </ts>
               <ts e="T602" id="Seg_4217" n="e" s="T601">üntɨšpɨntäna. </ts>
               <ts e="T603" id="Seg_4219" n="e" s="T602">Montä </ts>
               <ts e="T604" id="Seg_4221" n="e" s="T603">mɨta </ts>
               <ts e="T605" id="Seg_4223" n="e" s="T604">Laptelʼ </ts>
               <ts e="T606" id="Seg_4225" n="e" s="T605">nɔːkur </ts>
               <ts e="T607" id="Seg_4227" n="e" s="T606">timnʼäsɨt </ts>
               <ts e="T608" id="Seg_4229" n="e" s="T607">imalʼlʼä </ts>
               <ts e="T609" id="Seg_4231" n="e" s="T608">täp </ts>
               <ts e="T610" id="Seg_4233" n="e" s="T609">taːtɨmpɔːtɨt, </ts>
               <ts e="T611" id="Seg_4235" n="e" s="T610">sərtəttalʼ </ts>
               <ts e="T612" id="Seg_4237" n="e" s="T611">timnʼäsɨtkinte. </ts>
               <ts e="T613" id="Seg_4239" n="e" s="T612">Sərtəttalʼ </ts>
               <ts e="T614" id="Seg_4241" n="e" s="T613">timnʼäsɨt </ts>
               <ts e="T615" id="Seg_4243" n="e" s="T614">ukkur </ts>
               <ts e="T616" id="Seg_4245" n="e" s="T615">nʼenʼätɨt </ts>
               <ts e="T617" id="Seg_4247" n="e" s="T616">ɛːppɨntä. </ts>
               <ts e="T618" id="Seg_4249" n="e" s="T617">Namɨnɨk </ts>
               <ts e="T619" id="Seg_4251" n="e" s="T618">imalʼlʼi </ts>
               <ts e="T620" id="Seg_4253" n="e" s="T619">tümpɔːtɨt. </ts>
               <ts e="T621" id="Seg_4255" n="e" s="T620">Amɨrlʼä </ts>
               <ts e="T622" id="Seg_4257" n="e" s="T621">somak </ts>
               <ts e="T623" id="Seg_4259" n="e" s="T622">tortɨntɔːtɨk </ts>
               <ts e="T624" id="Seg_4261" n="e" s="T623">nɨːnɨ </ts>
               <ts e="T625" id="Seg_4263" n="e" s="T624">šitɨ </ts>
               <ts e="T626" id="Seg_4265" n="e" s="T625">na </ts>
               <ts e="T627" id="Seg_4267" n="e" s="T626">qənqolammɨntɔːtɨt. </ts>
               <ts e="T628" id="Seg_4269" n="e" s="T627">Əːtɨt </ts>
               <ts e="T629" id="Seg_4271" n="e" s="T628">montɨ </ts>
               <ts e="T630" id="Seg_4273" n="e" s="T629">mɨta </ts>
               <ts e="T631" id="Seg_4275" n="e" s="T630">nilʼčʼik </ts>
               <ts e="T632" id="Seg_4277" n="e" s="T631">nʼentɨ </ts>
               <ts e="T633" id="Seg_4279" n="e" s="T632">nɨː </ts>
               <ts e="T634" id="Seg_4281" n="e" s="T633">pintɔːtɨt. </ts>
               <ts e="T635" id="Seg_4283" n="e" s="T634">Täːlʼi </ts>
               <ts e="T636" id="Seg_4285" n="e" s="T635">nɔːtɨ </ts>
               <ts e="T637" id="Seg_4287" n="e" s="T636">qumaqumtij </ts>
               <ts e="T638" id="Seg_4289" n="e" s="T637">nʼentɨ </ts>
               <ts e="T639" id="Seg_4291" n="e" s="T638">na </ts>
               <ts e="T640" id="Seg_4293" n="e" s="T639">omtɨltɛntɔːtɨt. </ts>
               <ts e="T641" id="Seg_4295" n="e" s="T640">Qumɨtɨt </ts>
               <ts e="T642" id="Seg_4297" n="e" s="T641">qəntaːtqo </ts>
               <ts e="T643" id="Seg_4299" n="e" s="T642">ija </ts>
               <ts e="T644" id="Seg_4301" n="e" s="T643">ɛj </ts>
               <ts e="T645" id="Seg_4303" n="e" s="T644">moqona </ts>
               <ts e="T646" id="Seg_4305" n="e" s="T645">na </ts>
               <ts e="T647" id="Seg_4307" n="e" s="T646">qənqolamnɨtɨnɨ. </ts>
               <ts e="T648" id="Seg_4309" n="e" s="T647">Pona </ts>
               <ts e="T649" id="Seg_4311" n="e" s="T648">tantɨptäːqɨntɨt </ts>
               <ts e="T650" id="Seg_4313" n="e" s="T649">ija </ts>
               <ts e="T651" id="Seg_4315" n="e" s="T650">soqomtä </ts>
               <ts e="T652" id="Seg_4317" n="e" s="T651">soqaltiptäːqɨt </ts>
               <ts e="T653" id="Seg_4319" n="e" s="T652">Laptɨlʼ </ts>
               <ts e="T654" id="Seg_4321" n="e" s="T653">poːsa </ts>
               <ts e="T655" id="Seg_4323" n="e" s="T654">warqɨ </ts>
               <ts e="T656" id="Seg_4325" n="e" s="T655">timnʼätɨt </ts>
               <ts e="T657" id="Seg_4327" n="e" s="T656">nälʼat </ts>
               <ts e="T658" id="Seg_4329" n="e" s="T657">na </ts>
               <ts e="T659" id="Seg_4331" n="e" s="T658">ɛsɨmmɨntɨ, </ts>
               <ts e="T660" id="Seg_4333" n="e" s="T659">ijanɨk. </ts>
               <ts e="T661" id="Seg_4335" n="e" s="T660">Soqqante </ts>
               <ts e="T662" id="Seg_4337" n="e" s="T661">tasɨt </ts>
               <ts e="T663" id="Seg_4339" n="e" s="T662">mitə </ts>
               <ts e="T664" id="Seg_4341" n="e" s="T663">tına </ts>
               <ts e="T665" id="Seg_4343" n="e" s="T664">opčʼintas, </ts>
               <ts e="T666" id="Seg_4345" n="e" s="T665">a </ts>
               <ts e="T667" id="Seg_4347" n="e" s="T666">soqqant </ts>
               <ts e="T668" id="Seg_4349" n="e" s="T667">olä </ts>
               <ts e="T669" id="Seg_4351" n="e" s="T668">mitɨ </ts>
               <ts e="T670" id="Seg_4353" n="e" s="T669">tına </ts>
               <ts e="T671" id="Seg_4355" n="e" s="T670">poːlʼ </ts>
               <ts e="T672" id="Seg_4357" n="e" s="T671">kalan </ts>
               <ts e="T673" id="Seg_4359" n="e" s="T672">olɨ. </ts>
               <ts e="T674" id="Seg_4361" n="e" s="T673">Ija </ts>
               <ts e="T675" id="Seg_4363" n="e" s="T674">ne </ts>
               <ts e="T676" id="Seg_4365" n="e" s="T675">qajimi </ts>
               <ts e="T677" id="Seg_4367" n="e" s="T676">äčʼa </ts>
               <ts e="T678" id="Seg_4369" n="e" s="T677">na </ts>
               <ts e="T679" id="Seg_4371" n="e" s="T678">kətɨmnɨtɨ. </ts>
               <ts e="T680" id="Seg_4373" n="e" s="T679">Nɨːnɨ </ts>
               <ts e="T681" id="Seg_4375" n="e" s="T680">laqaltɛːmnɨt. </ts>
               <ts e="T682" id="Seg_4377" n="e" s="T681">Mɔːtt </ts>
               <ts e="T683" id="Seg_4379" n="e" s="T682">čʼap </ts>
               <ts e="T684" id="Seg_4381" n="e" s="T683">tülʼčʼa. </ts>
               <ts e="T685" id="Seg_4383" n="e" s="T684">Əmɨtɨ </ts>
               <ts e="T686" id="Seg_4385" n="e" s="T685">mɔːtan </ts>
               <ts e="T687" id="Seg_4387" n="e" s="T686">ɔːqɨt </ts>
               <ts e="T688" id="Seg_4389" n="e" s="T687">ətɨlʼ </ts>
               <ts e="T689" id="Seg_4391" n="e" s="T688">tükulä </ts>
               <ts e="T690" id="Seg_4393" n="e" s="T689">nɨŋka, </ts>
               <ts e="T691" id="Seg_4395" n="e" s="T690">ijamtɨ </ts>
               <ts e="T692" id="Seg_4397" n="e" s="T691">ɛtɨla. </ts>
               <ts e="T693" id="Seg_4399" n="e" s="T692">Ija </ts>
               <ts e="T694" id="Seg_4401" n="e" s="T693">tat </ts>
               <ts e="T695" id="Seg_4403" n="e" s="T694">kučʼa </ts>
               <ts e="T696" id="Seg_4405" n="e" s="T695">namɨšak </ts>
               <ts e="T697" id="Seg_4407" n="e" s="T696">qonɨnʼantɨ, </ts>
               <ts e="T698" id="Seg_4409" n="e" s="T697">mat </ts>
               <ts e="T699" id="Seg_4411" n="e" s="T698">puršimɔːtʼanoːqa </ts>
               <ts e="T700" id="Seg_4413" n="e" s="T699">karrʼat </ts>
               <ts e="T701" id="Seg_4415" n="e" s="T700">tap </ts>
               <ts e="T702" id="Seg_4417" n="e" s="T701">kotpas </ts>
               <ts e="T703" id="Seg_4419" n="e" s="T702">pɔːrɨqɨt </ts>
               <ts e="T704" id="Seg_4421" n="e" s="T703">ɔːmtɨkolʼimmɨntak. </ts>
               <ts e="T705" id="Seg_4423" n="e" s="T704">Na </ts>
               <ts e="T706" id="Seg_4425" n="e" s="T705">pit </ts>
               <ts e="T707" id="Seg_4427" n="e" s="T706">ija </ts>
               <ts e="T708" id="Seg_4429" n="e" s="T707">na </ts>
               <ts e="T709" id="Seg_4431" n="e" s="T708">šäqɨmmɨtɨ. </ts>
               <ts e="T710" id="Seg_4433" n="e" s="T709">Tɔːptɨlʼ </ts>
               <ts e="T711" id="Seg_4435" n="e" s="T710">čʼeːlɨ </ts>
               <ts e="T712" id="Seg_4437" n="e" s="T711">ɔːtamtɨ </ts>
               <ts e="T713" id="Seg_4439" n="e" s="T712">moqɨna </ts>
               <ts e="T714" id="Seg_4441" n="e" s="T713">tɔːqɨla </ts>
               <ts e="T715" id="Seg_4443" n="e" s="T714">ɔːtantɨ </ts>
               <ts e="T716" id="Seg_4445" n="e" s="T715">puːtoːqɨt </ts>
               <ts e="T717" id="Seg_4447" n="e" s="T716">peːkɨla </ts>
               <ts e="T718" id="Seg_4449" n="e" s="T717">šitɨ </ts>
               <ts e="T719" id="Seg_4451" n="e" s="T718">koškat </ts>
               <ts e="T720" id="Seg_4453" n="e" s="T719">tarɨlʼ </ts>
               <ts e="T721" id="Seg_4455" n="e" s="T720">qoptoːqın </ts>
               <ts e="T722" id="Seg_4457" n="e" s="T721">na </ts>
               <ts e="T723" id="Seg_4459" n="e" s="T722">orqɨlpɨntɨtɨ. </ts>
               <ts e="T724" id="Seg_4461" n="e" s="T723">Moːtɨkɔːlʼ </ts>
               <ts e="T725" id="Seg_4463" n="e" s="T724">qalɨmtɨ </ts>
               <ts e="T726" id="Seg_4465" n="e" s="T725">na </ts>
               <ts e="T727" id="Seg_4467" n="e" s="T726">sɔːrɔːlpɨntɨtɨnta. </ts>
               <ts e="T728" id="Seg_4469" n="e" s="T727">Kotälʼ </ts>
               <ts e="T729" id="Seg_4471" n="e" s="T728">malʼčʼamtɨ </ts>
               <ts e="T730" id="Seg_4473" n="e" s="T729">ɛj </ts>
               <ts e="T731" id="Seg_4475" n="e" s="T730">soqqɨmtɨ </ts>
               <ts e="T732" id="Seg_4477" n="e" s="T731">nä </ts>
               <ts e="T733" id="Seg_4479" n="e" s="T732">tokaltɨmmɨntɨtäːnɨ. </ts>
               <ts e="T734" id="Seg_4481" n="e" s="T733">Narapomtä </ts>
               <ts e="T735" id="Seg_4483" n="e" s="T734">iːmpatä </ts>
               <ts e="T736" id="Seg_4485" n="e" s="T735">qaj </ts>
               <ts e="T737" id="Seg_4487" n="e" s="T736">aša </ts>
               <ts e="T738" id="Seg_4489" n="e" s="T737">((…)) </ts>
               <ts e="T739" id="Seg_4491" n="e" s="T738">nılʼčʼik </ts>
               <ts e="T740" id="Seg_4493" n="e" s="T739">na </ts>
               <ts e="T741" id="Seg_4495" n="e" s="T740">laqalʼtɛːmmintɨ. </ts>
               <ts e="T742" id="Seg_4497" n="e" s="T741">Tolʼ </ts>
               <ts e="T743" id="Seg_4499" n="e" s="T742">qumɨtɨn </ts>
               <ts e="T744" id="Seg_4501" n="e" s="T743">mɔːttə </ts>
               <ts e="T745" id="Seg_4503" n="e" s="T744">čʼap </ts>
               <ts e="T746" id="Seg_4505" n="e" s="T745">tüːlʼčʼe </ts>
               <ts e="T747" id="Seg_4507" n="e" s="T746">monte </ts>
               <ts e="T748" id="Seg_4509" n="e" s="T747">mɨta </ts>
               <ts e="T749" id="Seg_4511" n="e" s="T748">Laptälʼ </ts>
               <ts e="T750" id="Seg_4513" n="e" s="T749">tɨmnʼäsɨt </ts>
               <ts e="T751" id="Seg_4515" n="e" s="T750">okoːt </ts>
               <ts e="T752" id="Seg_4517" n="e" s="T751">qaj </ts>
               <ts e="T753" id="Seg_4519" n="e" s="T752">tüːŋɔːtät. </ts>
               <ts e="T754" id="Seg_4521" n="e" s="T753">Ija </ts>
               <ts e="T755" id="Seg_4523" n="e" s="T754">ɔːqalʼtɨ </ts>
               <ts e="T756" id="Seg_4525" n="e" s="T755">ılla </ts>
               <ts e="T757" id="Seg_4527" n="e" s="T756">sɔːrɨlʼä </ts>
               <ts e="T758" id="Seg_4529" n="e" s="T757">mɔːttɨ </ts>
               <ts e="T759" id="Seg_4531" n="e" s="T758">na </ts>
               <ts e="T760" id="Seg_4533" n="e" s="T759">šeːrpɨtɨ. </ts>
               <ts e="T761" id="Seg_4535" n="e" s="T760">Mɔːttɨ </ts>
               <ts e="T762" id="Seg_4537" n="e" s="T761">šeːrlʼa </ts>
               <ts e="T763" id="Seg_4539" n="e" s="T762">mɔːtan </ts>
               <ts e="T764" id="Seg_4541" n="e" s="T763">ɔːktə </ts>
               <ts e="T765" id="Seg_4543" n="e" s="T764">čʼap </ts>
               <ts e="T766" id="Seg_4545" n="e" s="T765">nɨllɛːje </ts>
               <ts e="T767" id="Seg_4547" n="e" s="T766">ira </ts>
               <ts e="T768" id="Seg_4549" n="e" s="T767">tälʼ </ts>
               <ts e="T769" id="Seg_4551" n="e" s="T768">čʼeːlɨ </ts>
               <ts e="T770" id="Seg_4553" n="e" s="T769">montɨ </ts>
               <ts e="T771" id="Seg_4555" n="e" s="T770">mɨta </ts>
               <ts e="T772" id="Seg_4557" n="e" s="T771">nalʼät </ts>
               <ts e="T773" id="Seg_4559" n="e" s="T772">ɛsa. </ts>
               <ts e="T774" id="Seg_4561" n="e" s="T773">Tɛː </ts>
               <ts e="T775" id="Seg_4563" n="e" s="T774">qaj </ts>
               <ts e="T776" id="Seg_4565" n="e" s="T775">qoštɨlʼ </ts>
               <ts e="T777" id="Seg_4567" n="e" s="T776">qumɨp </ts>
               <ts e="T778" id="Seg_4569" n="e" s="T777">aša </ts>
               <ts e="T779" id="Seg_4571" n="e" s="T778">qontɔːlɨt. </ts>
               <ts e="T780" id="Seg_4573" n="e" s="T779">Nänna </ts>
               <ts e="T781" id="Seg_4575" n="e" s="T780">üːtočʼɨntɨtqa. </ts>
               <ts e="T782" id="Seg_4577" n="e" s="T781">Ija </ts>
               <ts e="T783" id="Seg_4579" n="e" s="T782">mannɨmpatɨ </ts>
               <ts e="T784" id="Seg_4581" n="e" s="T783">irat </ts>
               <ts e="T785" id="Seg_4583" n="e" s="T784">qöntɨ </ts>
               <ts e="T786" id="Seg_4585" n="e" s="T785">koptɨp </ts>
               <ts e="T787" id="Seg_4587" n="e" s="T786">šitä </ts>
               <ts e="T788" id="Seg_4589" n="e" s="T787">na </ts>
               <ts e="T789" id="Seg_4591" n="e" s="T788">meːntɔːtɨt. </ts>
               <ts e="T790" id="Seg_4593" n="e" s="T789">Ija </ts>
               <ts e="T791" id="Seg_4595" n="e" s="T790">nʼenna </ts>
               <ts e="T792" id="Seg_4597" n="e" s="T791">ılla </ts>
               <ts e="T793" id="Seg_4599" n="e" s="T792">omta. </ts>
               <ts e="T794" id="Seg_4601" n="e" s="T793">Nılʼčʼik </ts>
               <ts e="T795" id="Seg_4603" n="e" s="T794">mannɨmpatɨ. </ts>
               <ts e="T796" id="Seg_4605" n="e" s="T795">Montɨ </ts>
               <ts e="T797" id="Seg_4607" n="e" s="T796">mɨta </ts>
               <ts e="T798" id="Seg_4609" n="e" s="T797">qumaqumtij </ts>
               <ts e="T799" id="Seg_4611" n="e" s="T798">nʼentɨ </ts>
               <ts e="T800" id="Seg_4613" n="e" s="T799">omtalʼ </ts>
               <ts e="T801" id="Seg_4615" n="e" s="T800">tümpɨla </ts>
               <ts e="T802" id="Seg_4617" n="e" s="T801">ütɨmpɔːtɨt. </ts>
               <ts e="T803" id="Seg_4619" n="e" s="T802">Ija </ts>
               <ts e="T804" id="Seg_4621" n="e" s="T803">ɛj </ts>
               <ts e="T805" id="Seg_4623" n="e" s="T804">na </ts>
               <ts e="T806" id="Seg_4625" n="e" s="T805">qomintisa </ts>
               <ts e="T807" id="Seg_4627" n="e" s="T806">təm </ts>
               <ts e="T808" id="Seg_4629" n="e" s="T807">ɛj </ts>
               <ts e="T809" id="Seg_4631" n="e" s="T808">na </ts>
               <ts e="T810" id="Seg_4633" n="e" s="T809">ütɨmperalʼmɨtɨ. </ts>
               <ts e="T811" id="Seg_4635" n="e" s="T810">Montɨ </ts>
               <ts e="T812" id="Seg_4637" n="e" s="T811">mɨta </ts>
               <ts e="T813" id="Seg_4639" n="e" s="T812">ijap </ts>
               <ts e="T814" id="Seg_4641" n="e" s="T813">ün </ts>
               <ts e="T815" id="Seg_4643" n="e" s="T814">na </ts>
               <ts e="T816" id="Seg_4645" n="e" s="T815">šeːrtätä. </ts>
               <ts e="T817" id="Seg_4647" n="e" s="T816">Ü </ts>
               <ts e="T818" id="Seg_4649" n="e" s="T817">šeːrla </ts>
               <ts e="T819" id="Seg_4651" n="e" s="T818">ija </ts>
               <ts e="T820" id="Seg_4653" n="e" s="T819">alʼčʼa. </ts>
               <ts e="T821" id="Seg_4655" n="e" s="T820">Ü </ts>
               <ts e="T822" id="Seg_4657" n="e" s="T821">sʼeːrpɨlʼ </ts>
               <ts e="T823" id="Seg_4659" n="e" s="T822">qup </ts>
               <ts e="T824" id="Seg_4661" n="e" s="T823">qaj </ts>
               <ts e="T825" id="Seg_4663" n="e" s="T824">qaj </ts>
               <ts e="T826" id="Seg_4665" n="e" s="T825">tɛnimɨntä </ts>
               <ts e="T827" id="Seg_4667" n="e" s="T826">kušaŋ </ts>
               <ts e="T828" id="Seg_4669" n="e" s="T827">aša </ts>
               <ts e="T829" id="Seg_4671" n="e" s="T828">na </ts>
               <ts e="T830" id="Seg_4673" n="e" s="T829">ippɨnmɨntɨ. </ts>
               <ts e="T831" id="Seg_4675" n="e" s="T830">Ukkur </ts>
               <ts e="T832" id="Seg_4677" n="e" s="T831">tət </ts>
               <ts e="T833" id="Seg_4679" n="e" s="T832">čʼontoːqɨt </ts>
               <ts e="T834" id="Seg_4681" n="e" s="T833">iːja </ts>
               <ts e="T835" id="Seg_4683" n="e" s="T834">ınna </ts>
               <ts e="T836" id="Seg_4685" n="e" s="T835">čʼap </ts>
               <ts e="T837" id="Seg_4687" n="e" s="T836">qəŋa </ts>
               <ts e="T838" id="Seg_4689" n="e" s="T837">montɨ </ts>
               <ts e="T839" id="Seg_4691" n="e" s="T838">mɨta </ts>
               <ts e="T840" id="Seg_4693" n="e" s="T839">qup </ts>
               <ts e="T841" id="Seg_4695" n="e" s="T840">šıp </ts>
               <ts e="T842" id="Seg_4697" n="e" s="T841">qəttɨrorrij </ts>
               <ts e="T843" id="Seg_4699" n="e" s="T842">nʼoːtɨš. </ts>
               <ts e="T844" id="Seg_4701" n="e" s="T843">Nılʼčʼik </ts>
               <ts e="T845" id="Seg_4703" n="e" s="T844">ippɨlʼa </ts>
               <ts e="T846" id="Seg_4705" n="e" s="T845">üŋkultimpatɨ </ts>
               <ts e="T847" id="Seg_4707" n="e" s="T846">qäːlʼ </ts>
               <ts e="T848" id="Seg_4709" n="e" s="T847">täːttoːqɨt </ts>
               <ts e="T849" id="Seg_4711" n="e" s="T848">tına </ts>
               <ts e="T850" id="Seg_4713" n="e" s="T849">sərtətattɨt </ts>
               <ts e="T851" id="Seg_4715" n="e" s="T850">nʼenʼnʼa </ts>
               <ts e="T852" id="Seg_4717" n="e" s="T851">laŋkuškuna. </ts>
               <ts e="T853" id="Seg_4719" n="e" s="T852">Tɛː </ts>
               <ts e="T854" id="Seg_4721" n="e" s="T853">lʼa </ts>
               <ts e="T855" id="Seg_4723" n="e" s="T854">mompa </ts>
               <ts e="T856" id="Seg_4725" n="e" s="T855">mɨta </ts>
               <ts e="T857" id="Seg_4727" n="e" s="T856">qaj </ts>
               <ts e="T858" id="Seg_4729" n="e" s="T857">manpɨmmɨntalit. </ts>
               <ts e="T859" id="Seg_4731" n="e" s="T858">Tına </ts>
               <ts e="T860" id="Seg_4733" n="e" s="T859">šentɨk </ts>
               <ts e="T861" id="Seg_4735" n="e" s="T860">tümpɨlʼ </ts>
               <ts e="T862" id="Seg_4737" n="e" s="T861">iːjap </ts>
               <ts e="T863" id="Seg_4739" n="e" s="T862">na </ts>
               <ts e="T864" id="Seg_4741" n="e" s="T863">qättɔːtɨt, </ts>
               <ts e="T865" id="Seg_4743" n="e" s="T864">Loptɨlʼ </ts>
               <ts e="T866" id="Seg_4745" n="e" s="T865">tɨmnʼäsɨt. </ts>
               <ts e="T867" id="Seg_4747" n="e" s="T866">Ija </ts>
               <ts e="T868" id="Seg_4749" n="e" s="T867">asʼa </ts>
               <ts e="T869" id="Seg_4751" n="e" s="T868">qatta. </ts>
               <ts e="T870" id="Seg_4753" n="e" s="T869">Ippɨptäː </ts>
               <ts e="T871" id="Seg_4755" n="e" s="T870">säqɨt </ts>
               <ts e="T872" id="Seg_4757" n="e" s="T871">iːqɨntä </ts>
               <ts e="T873" id="Seg_4759" n="e" s="T872">qəttɨla </ts>
               <ts e="T874" id="Seg_4761" n="e" s="T873">ɔːlʼčʼimpɨlʼ </ts>
               <ts e="T875" id="Seg_4763" n="e" s="T874">qumimtɨ </ts>
               <ts e="T876" id="Seg_4765" n="e" s="T875">səntätɨsä </ts>
               <ts e="T877" id="Seg_4767" n="e" s="T876">šitä </ts>
               <ts e="T878" id="Seg_4769" n="e" s="T877">na </ts>
               <ts e="T879" id="Seg_4771" n="e" s="T878">qättɨmmɨntä. </ts>
               <ts e="T880" id="Seg_4773" n="e" s="T879">Nɨːnä </ts>
               <ts e="T881" id="Seg_4775" n="e" s="T880">šitɨmtalʼi </ts>
               <ts e="T882" id="Seg_4777" n="e" s="T881">šitɨ </ts>
               <ts e="T883" id="Seg_4779" n="e" s="T882">čʼammantə </ts>
               <ts e="T884" id="Seg_4781" n="e" s="T883">montɨ </ts>
               <ts e="T885" id="Seg_4783" n="e" s="T884">mɨta </ts>
               <ts e="T886" id="Seg_4785" n="e" s="T885">qälilʼ </ts>
               <ts e="T887" id="Seg_4787" n="e" s="T886">tɨmnʼäsɨt </ts>
               <ts e="T888" id="Seg_4789" n="e" s="T887">Loptɨlʼ </ts>
               <ts e="T889" id="Seg_4791" n="e" s="T888">nılʼčʼik </ts>
               <ts e="T890" id="Seg_4793" n="e" s="T889">qumpɔːtɨt, </ts>
               <ts e="T891" id="Seg_4795" n="e" s="T890">ɔːmɨnʼeːtɨt </ts>
               <ts e="T892" id="Seg_4797" n="e" s="T891">čʼontoːmɨt </ts>
               <ts e="T893" id="Seg_4799" n="e" s="T892">šitə </ts>
               <ts e="T894" id="Seg_4801" n="e" s="T893">säpäıːmpa. </ts>
               <ts e="T895" id="Seg_4803" n="e" s="T894">Ɔːmɨnʼeːtɨt </ts>
               <ts e="T896" id="Seg_4805" n="e" s="T895">kɨm </ts>
               <ts e="T897" id="Seg_4807" n="e" s="T896">šitə </ts>
               <ts e="T898" id="Seg_4809" n="e" s="T897">pasəıːmpa, </ts>
               <ts e="T899" id="Seg_4811" n="e" s="T898">a </ts>
               <ts e="T900" id="Seg_4813" n="e" s="T899">poːsɨ </ts>
               <ts e="T901" id="Seg_4815" n="e" s="T900">kɨpa </ts>
               <ts e="T902" id="Seg_4817" n="e" s="T901">qəːtsɨmɨlʼ </ts>
               <ts e="T903" id="Seg_4819" n="e" s="T902">timnʼatɨt </ts>
               <ts e="T904" id="Seg_4821" n="e" s="T903">muntɨ </ts>
               <ts e="T905" id="Seg_4823" n="e" s="T904">mɨta </ts>
               <ts e="T906" id="Seg_4825" n="e" s="T905">olotɨ </ts>
               <ts e="T907" id="Seg_4827" n="e" s="T906">meːl </ts>
               <ts e="T908" id="Seg_4829" n="e" s="T907">čʼäː, </ts>
               <ts e="T909" id="Seg_4831" n="e" s="T908">a </ts>
               <ts e="T910" id="Seg_4833" n="e" s="T909">na </ts>
               <ts e="T911" id="Seg_4835" n="e" s="T910">moːtɨkɔːl </ts>
               <ts e="T912" id="Seg_4837" n="e" s="T911">mɔːt </ts>
               <ts e="T913" id="Seg_4839" n="e" s="T912">čʼaŋalʼ </ts>
               <ts e="T914" id="Seg_4841" n="e" s="T913">moːtɨkɔːl </ts>
               <ts e="T915" id="Seg_4843" n="e" s="T914">tɔːpɨ </ts>
               <ts e="T916" id="Seg_4845" n="e" s="T915">qalimmɨnta. </ts>
               <ts e="T917" id="Seg_4847" n="e" s="T916">Mɔːtɨkota </ts>
               <ts e="T918" id="Seg_4849" n="e" s="T917">ɛj </ts>
               <ts e="T919" id="Seg_4851" n="e" s="T918">mɔːtɨqop </ts>
               <ts e="T920" id="Seg_4853" n="e" s="T919">täːlʼšɨlʼ </ts>
               <ts e="T921" id="Seg_4855" n="e" s="T920">tät </ts>
               <ts e="T922" id="Seg_4857" n="e" s="T921">toːt </ts>
               <ts e="T923" id="Seg_4859" n="e" s="T922">älpät </ts>
               <ts e="T924" id="Seg_4861" n="e" s="T923">ippɔːtɨt. </ts>
               <ts e="T925" id="Seg_4863" n="e" s="T924">İnnä </ts>
               <ts e="T926" id="Seg_4865" n="e" s="T925">wešila </ts>
               <ts e="T927" id="Seg_4867" n="e" s="T926">nılʼčʼik </ts>
               <ts e="T928" id="Seg_4869" n="e" s="T927">šitɨ </ts>
               <ts e="T929" id="Seg_4871" n="e" s="T928">mannɨmpatɨ </ts>
               <ts e="T930" id="Seg_4873" n="e" s="T929">qumɨt </ts>
               <ts e="T931" id="Seg_4875" n="e" s="T930">taqqempɨla </ts>
               <ts e="T932" id="Seg_4877" n="e" s="T931">täpɨp </ts>
               <ts e="T933" id="Seg_4879" n="e" s="T932">mannɨmpɔːtɨt. </ts>
               <ts e="T934" id="Seg_4881" n="e" s="T933">A </ts>
               <ts e="T935" id="Seg_4883" n="e" s="T934">tına </ts>
               <ts e="T936" id="Seg_4885" n="e" s="T935">nätak </ts>
               <ts e="T937" id="Seg_4887" n="e" s="T936">meːl </ts>
               <ts e="T938" id="Seg_4889" n="e" s="T937">čʼäːŋka. </ts>
               <ts e="T939" id="Seg_4891" n="e" s="T938">Toː </ts>
               <ts e="T940" id="Seg_4893" n="e" s="T939">qəlla </ts>
               <ts e="T941" id="Seg_4895" n="e" s="T940">mɔːtɨqopɨp </ts>
               <ts e="T942" id="Seg_4897" n="e" s="T941">ınna </ts>
               <ts e="T943" id="Seg_4899" n="e" s="T942">šitɨ </ts>
               <ts e="T944" id="Seg_4901" n="e" s="T943">čʼap </ts>
               <ts e="T945" id="Seg_4903" n="e" s="T944">nüŋɨtɨ— </ts>
               <ts e="T946" id="Seg_4905" n="e" s="T945">montɨ </ts>
               <ts e="T947" id="Seg_4907" n="e" s="T946">nättak </ts>
               <ts e="T948" id="Seg_4909" n="e" s="T947">nɨːnɨ </ts>
               <ts e="T949" id="Seg_4911" n="e" s="T948">na </ts>
               <ts e="T950" id="Seg_4913" n="e" s="T949">putilmɔːnna. </ts>
               <ts e="T951" id="Seg_4915" n="e" s="T950">Aša </ts>
               <ts e="T952" id="Seg_4917" n="e" s="T951">qatɔːtɨt </ts>
               <ts e="T953" id="Seg_4919" n="e" s="T952">na </ts>
               <ts e="T954" id="Seg_4921" n="e" s="T953">nɨmtɨlʼ </ts>
               <ts e="T955" id="Seg_4923" n="e" s="T954">qumɨt. </ts>
               <ts e="T956" id="Seg_4925" n="e" s="T955">Wänɨlʼ </ts>
               <ts e="T957" id="Seg_4927" n="e" s="T956">mɔːtɨp </ts>
               <ts e="T958" id="Seg_4929" n="e" s="T957">čʼəsɨla </ts>
               <ts e="T959" id="Seg_4931" n="e" s="T958">nɔːtɨ </ts>
               <ts e="T960" id="Seg_4933" n="e" s="T959">na </ts>
               <ts e="T961" id="Seg_4935" n="e" s="T960">ütɛːrpintɔːtɨt </ts>
               <ts e="T962" id="Seg_4937" n="e" s="T961">ɛj </ts>
               <ts e="T963" id="Seg_4939" n="e" s="T962">amɨrpɔːtɨt, </ts>
               <ts e="T964" id="Seg_4941" n="e" s="T963">särtätta </ts>
               <ts e="T965" id="Seg_4943" n="e" s="T964">ira </ts>
               <ts e="T966" id="Seg_4945" n="e" s="T965">nʼenʼämtɨ </ts>
               <ts e="T967" id="Seg_4947" n="e" s="T966">na </ts>
               <ts e="T968" id="Seg_4949" n="e" s="T967">ijanɨk </ts>
               <ts e="T969" id="Seg_4951" n="e" s="T968">na </ts>
               <ts e="T970" id="Seg_4953" n="e" s="T969">kuralʼtɨmmɨntɨt. </ts>
               <ts e="T971" id="Seg_4955" n="e" s="T970">Ilʼmatɨlʼ </ts>
               <ts e="T972" id="Seg_4957" n="e" s="T971">qumoːqɨp </ts>
               <ts e="T973" id="Seg_4959" n="e" s="T972">nʼentɨ </ts>
               <ts e="T974" id="Seg_4961" n="e" s="T973">ompɨltäntɨla </ts>
               <ts e="T975" id="Seg_4963" n="e" s="T974">tɛːttɨkolʼ </ts>
               <ts e="T976" id="Seg_4965" n="e" s="T975">köt </ts>
               <ts e="T977" id="Seg_4967" n="e" s="T976">čʼeːla </ts>
               <ts e="T978" id="Seg_4969" n="e" s="T977">ütɨmɨmɨmpɔːtɨt. </ts>
               <ts e="T979" id="Seg_4971" n="e" s="T978">Qumaqup </ts>
               <ts e="T980" id="Seg_4973" n="e" s="T979">nʼentɨ </ts>
               <ts e="T981" id="Seg_4975" n="e" s="T980">omtɨlpɨla </ts>
               <ts e="T982" id="Seg_4977" n="e" s="T981">sərtəttalʼ </ts>
               <ts e="T983" id="Seg_4979" n="e" s="T982">timnʼäsɨt </ts>
               <ts e="T984" id="Seg_4981" n="e" s="T983">na </ts>
               <ts e="T985" id="Seg_4983" n="e" s="T984">ijasa </ts>
               <ts e="T986" id="Seg_4985" n="e" s="T985">tɔː </ts>
               <ts e="T987" id="Seg_4987" n="e" s="T986">qärtɔːtɨt </ts>
               <ts e="T988" id="Seg_4989" n="e" s="T987">ijat </ts>
               <ts e="T989" id="Seg_4991" n="e" s="T988">mɔːttɨ </ts>
               <ts e="T990" id="Seg_4993" n="e" s="T989">qanɨktä. </ts>
               <ts e="T991" id="Seg_4995" n="e" s="T990">Nʼentɨ </ts>
               <ts e="T992" id="Seg_4997" n="e" s="T991">palla </ts>
               <ts e="T993" id="Seg_4999" n="e" s="T992">na </ts>
               <ts e="T994" id="Seg_5001" n="e" s="T993">ilimmɨntɔːtɨt </ts>
               <ts e="T995" id="Seg_5003" n="e" s="T994">i </ts>
               <ts e="T996" id="Seg_5005" n="e" s="T995">tiːtä </ts>
               <ts e="T997" id="Seg_5007" n="e" s="T996">qos </ts>
               <ts e="T998" id="Seg_5009" n="e" s="T997">to </ts>
               <ts e="T999" id="Seg_5011" n="e" s="T998">ilimtɔːtɨt. </ts>
               <ts e="T1000" id="Seg_5013" n="e" s="T999">Čʼäpta </ts>
               <ts e="T1001" id="Seg_5015" n="e" s="T1000">nʼeːmit </ts>
               <ts e="T1002" id="Seg_5017" n="e" s="T1001">toː </ts>
               <ts e="T1003" id="Seg_5019" n="e" s="T1002">na </ts>
               <ts e="T1004" id="Seg_5021" n="e" s="T1003">seːpʼe </ts>
               <ts e="T1005" id="Seg_5023" n="e" s="T1004">ɛːmmɨntɨ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T85" id="Seg_5024" s="T82">TVP_1965_ThreeBrothersLapta_flk.001 (001)</ta>
            <ta e="T88" id="Seg_5025" s="T85">TVP_1965_ThreeBrothersLapta_flk.002 (002.001)</ta>
            <ta e="T92" id="Seg_5026" s="T88">TVP_1965_ThreeBrothersLapta_flk.003 (002.002)</ta>
            <ta e="T99" id="Seg_5027" s="T92">TVP_1965_ThreeBrothersLapta_flk.004 (002.003)</ta>
            <ta e="T105" id="Seg_5028" s="T99">TVP_1965_ThreeBrothersLapta_flk.005 (003.001)</ta>
            <ta e="T113" id="Seg_5029" s="T105">TVP_1965_ThreeBrothersLapta_flk.006 (003.002)</ta>
            <ta e="T126" id="Seg_5030" s="T113">TVP_1965_ThreeBrothersLapta_flk.007 (003.003)</ta>
            <ta e="T131" id="Seg_5031" s="T126">TVP_1965_ThreeBrothersLapta_flk.008 (003.004)</ta>
            <ta e="T137" id="Seg_5032" s="T131">TVP_1965_ThreeBrothersLapta_flk.009 (003.005)</ta>
            <ta e="T140" id="Seg_5033" s="T137">TVP_1965_ThreeBrothersLapta_flk.010 (003.006)</ta>
            <ta e="T144" id="Seg_5034" s="T140">TVP_1965_ThreeBrothersLapta_flk.011 (003.007)</ta>
            <ta e="T147" id="Seg_5035" s="T144">TVP_1965_ThreeBrothersLapta_flk.012 (003.008)</ta>
            <ta e="T152" id="Seg_5036" s="T147">TVP_1965_ThreeBrothersLapta_flk.013 (003.009)</ta>
            <ta e="T155" id="Seg_5037" s="T152">TVP_1965_ThreeBrothersLapta_flk.014 (003.010)</ta>
            <ta e="T158" id="Seg_5038" s="T155">TVP_1965_ThreeBrothersLapta_flk.015 (003.011</ta>
            <ta e="T161" id="Seg_5039" s="T158">TVP_1965_ThreeBrothersLapta_flk.016 (003.012)</ta>
            <ta e="T178" id="Seg_5040" s="T161">TVP_1965_ThreeBrothersLapta_flk.017 (003.013)</ta>
            <ta e="T181" id="Seg_5041" s="T178">TVP_1965_ThreeBrothersLapta_flk.018 (003.014)</ta>
            <ta e="T190" id="Seg_5042" s="T181">TVP_1965_ThreeBrothersLapta_flk.019 (003.015)</ta>
            <ta e="T198" id="Seg_5043" s="T190">TVP_1965_ThreeBrothersLapta_flk.020 (003.016)</ta>
            <ta e="T204" id="Seg_5044" s="T198">TVP_1965_ThreeBrothersLapta_flk.021 (003.017)</ta>
            <ta e="T210" id="Seg_5045" s="T204">TVP_1965_ThreeBrothersLapta_flk.022 (003.018)</ta>
            <ta e="T214" id="Seg_5046" s="T210">TVP_1965_ThreeBrothersLapta_flk.023 (003.019)</ta>
            <ta e="T223" id="Seg_5047" s="T214">TVP_1965_ThreeBrothersLapta_flk.024 (003.020)</ta>
            <ta e="T226" id="Seg_5048" s="T223">TVP_1965_ThreeBrothersLapta_flk.025 (003.021)</ta>
            <ta e="T243" id="Seg_5049" s="T226">TVP_1965_ThreeBrothersLapta_flk.026 (003.022)</ta>
            <ta e="T247" id="Seg_5050" s="T243">TVP_1965_ThreeBrothersLapta_flk.027 (003.023)</ta>
            <ta e="T261" id="Seg_5051" s="T247">TVP_1965_ThreeBrothersLapta_flk.028 (003.024)</ta>
            <ta e="T266" id="Seg_5052" s="T261">TVP_1965_ThreeBrothersLapta_flk.029 (003.025)</ta>
            <ta e="T274" id="Seg_5053" s="T266">TVP_1965_ThreeBrothersLapta_flk.030 (003.026)</ta>
            <ta e="T280" id="Seg_5054" s="T274">TVP_1965_ThreeBrothersLapta_flk.031 (003.027)</ta>
            <ta e="T287" id="Seg_5055" s="T280">TVP_1965_ThreeBrothersLapta_flk.032 (003.028)</ta>
            <ta e="T295" id="Seg_5056" s="T287">TVP_1965_ThreeBrothersLapta_flk.033 (003.029)</ta>
            <ta e="T303" id="Seg_5057" s="T295">TVP_1965_ThreeBrothersLapta_flk.034 (003.030)</ta>
            <ta e="T313" id="Seg_5058" s="T303">TVP_1965_ThreeBrothersLapta_flk.035 (004.001)</ta>
            <ta e="T321" id="Seg_5059" s="T313">TVP_1965_ThreeBrothersLapta_flk.036 (004.002)</ta>
            <ta e="T328" id="Seg_5060" s="T321">TVP_1965_ThreeBrothersLapta_flk.037 (004.003)</ta>
            <ta e="T341" id="Seg_5061" s="T328">TVP_1965_ThreeBrothersLapta_flk.038 (004.004)</ta>
            <ta e="T346" id="Seg_5062" s="T341">TVP_1965_ThreeBrothersLapta_flk.039 (004.005)</ta>
            <ta e="T352" id="Seg_5063" s="T346">TVP_1965_ThreeBrothersLapta_flk.040 (004.006)</ta>
            <ta e="T359" id="Seg_5064" s="T352">TVP_1965_ThreeBrothersLapta_flk.041 (004.007)</ta>
            <ta e="T365" id="Seg_5065" s="T359">TVP_1965_ThreeBrothersLapta_flk.042 (004.008)</ta>
            <ta e="T370" id="Seg_5066" s="T365">TVP_1965_ThreeBrothersLapta_flk.043 (004.009)</ta>
            <ta e="T375" id="Seg_5067" s="T370">TVP_1965_ThreeBrothersLapta_flk.044 (004.010)</ta>
            <ta e="T383" id="Seg_5068" s="T375">TVP_1965_ThreeBrothersLapta_flk.045 (004.011)</ta>
            <ta e="T387" id="Seg_5069" s="T383">TVP_1965_ThreeBrothersLapta_flk.046 (004.012)</ta>
            <ta e="T399" id="Seg_5070" s="T387">TVP_1965_ThreeBrothersLapta_flk.047 (004.013)</ta>
            <ta e="T403" id="Seg_5071" s="T399">TVP_1965_ThreeBrothersLapta_flk.048 (004.014)</ta>
            <ta e="T409" id="Seg_5072" s="T403">TVP_1965_ThreeBrothersLapta_flk.049 (004.015)</ta>
            <ta e="T413" id="Seg_5073" s="T409">TVP_1965_ThreeBrothersLapta_flk.050 (004.016)</ta>
            <ta e="T424" id="Seg_5074" s="T413">TVP_1965_ThreeBrothersLapta_flk.051 (004.017)</ta>
            <ta e="T432" id="Seg_5075" s="T424">TVP_1965_ThreeBrothersLapta_flk.052 (004.018)</ta>
            <ta e="T441" id="Seg_5076" s="T432">TVP_1965_ThreeBrothersLapta_flk.053 (004.019)</ta>
            <ta e="T446" id="Seg_5077" s="T441">TVP_1965_ThreeBrothersLapta_flk.054 (004.020)</ta>
            <ta e="T459" id="Seg_5078" s="T446">TVP_1965_ThreeBrothersLapta_flk.055 (004.021)</ta>
            <ta e="T470" id="Seg_5079" s="T459">TVP_1965_ThreeBrothersLapta_flk.056 (004.022)</ta>
            <ta e="T475" id="Seg_5080" s="T470">TVP_1965_ThreeBrothersLapta_flk.057 (004.023)</ta>
            <ta e="T483" id="Seg_5081" s="T475">TVP_1965_ThreeBrothersLapta_flk.058 (004.024)</ta>
            <ta e="T488" id="Seg_5082" s="T483">TVP_1965_ThreeBrothersLapta_flk.059 (004.025)</ta>
            <ta e="T492" id="Seg_5083" s="T488">TVP_1965_ThreeBrothersLapta_flk.060 (004.026)</ta>
            <ta e="T500" id="Seg_5084" s="T492">TVP_1965_ThreeBrothersLapta_flk.061 (004.027)</ta>
            <ta e="T508" id="Seg_5085" s="T500">TVP_1965_ThreeBrothersLapta_flk.062 (004.028)</ta>
            <ta e="T515" id="Seg_5086" s="T508">TVP_1965_ThreeBrothersLapta_flk.063 (004.029)</ta>
            <ta e="T518" id="Seg_5087" s="T515">TVP_1965_ThreeBrothersLapta_flk.064 (004.030)</ta>
            <ta e="T522" id="Seg_5088" s="T518">TVP_1965_ThreeBrothersLapta_flk.065 (004.031)</ta>
            <ta e="T532" id="Seg_5089" s="T522">TVP_1965_ThreeBrothersLapta_flk.066 (004.032)</ta>
            <ta e="T539" id="Seg_5090" s="T532">TVP_1965_ThreeBrothersLapta_flk.067 (004.033)</ta>
            <ta e="T549" id="Seg_5091" s="T539">TVP_1965_ThreeBrothersLapta_flk.068 (004.034)</ta>
            <ta e="T556" id="Seg_5092" s="T549">TVP_1965_ThreeBrothersLapta_flk.069 (004.035)</ta>
            <ta e="T565" id="Seg_5093" s="T556">TVP_1965_ThreeBrothersLapta_flk.070 (004.036)</ta>
            <ta e="T571" id="Seg_5094" s="T565">TVP_1965_ThreeBrothersLapta_flk.071 (004.037)</ta>
            <ta e="T577" id="Seg_5095" s="T571">TVP_1965_ThreeBrothersLapta_flk.072 (004.038)</ta>
            <ta e="T584" id="Seg_5096" s="T577">TVP_1965_ThreeBrothersLapta_flk.073 (004.039)</ta>
            <ta e="T592" id="Seg_5097" s="T584">TVP_1965_ThreeBrothersLapta_flk.074 (004.040)</ta>
            <ta e="T597" id="Seg_5098" s="T592">TVP_1965_ThreeBrothersLapta_flk.075 (004.041)</ta>
            <ta e="T602" id="Seg_5099" s="T597">TVP_1965_ThreeBrothersLapta_flk.076 (004.042)</ta>
            <ta e="T612" id="Seg_5100" s="T602">TVP_1965_ThreeBrothersLapta_flk.077 (004.043)</ta>
            <ta e="T617" id="Seg_5101" s="T612">TVP_1965_ThreeBrothersLapta_flk.078 (004.044)</ta>
            <ta e="T620" id="Seg_5102" s="T617">TVP_1965_ThreeBrothersLapta_flk.079 (004.045)</ta>
            <ta e="T627" id="Seg_5103" s="T620">TVP_1965_ThreeBrothersLapta_flk.080 (004.046)</ta>
            <ta e="T634" id="Seg_5104" s="T627">TVP_1965_ThreeBrothersLapta_flk.081 (004.047)</ta>
            <ta e="T640" id="Seg_5105" s="T634">TVP_1965_ThreeBrothersLapta_flk.082 (004.048)</ta>
            <ta e="T647" id="Seg_5106" s="T640">TVP_1965_ThreeBrothersLapta_flk.083 (004.049)</ta>
            <ta e="T660" id="Seg_5107" s="T647">TVP_1965_ThreeBrothersLapta_flk.084 (004.050)</ta>
            <ta e="T673" id="Seg_5108" s="T660">TVP_1965_ThreeBrothersLapta_flk.085 (004.051)</ta>
            <ta e="T679" id="Seg_5109" s="T673">TVP_1965_ThreeBrothersLapta_flk.086 (004.052)</ta>
            <ta e="T681" id="Seg_5110" s="T679">TVP_1965_ThreeBrothersLapta_flk.087 (005.001)</ta>
            <ta e="T684" id="Seg_5111" s="T681">TVP_1965_ThreeBrothersLapta_flk.088 (005.002)</ta>
            <ta e="T692" id="Seg_5112" s="T684">TVP_1965_ThreeBrothersLapta_flk.089 (005.003)</ta>
            <ta e="T704" id="Seg_5113" s="T692">TVP_1965_ThreeBrothersLapta_flk.090 (005.004)</ta>
            <ta e="T709" id="Seg_5114" s="T704">TVP_1965_ThreeBrothersLapta_flk.091 (005.005)</ta>
            <ta e="T723" id="Seg_5115" s="T709">TVP_1965_ThreeBrothersLapta_flk.092 (006.001)</ta>
            <ta e="T727" id="Seg_5116" s="T723">TVP_1965_ThreeBrothersLapta_flk.093 (006.002)</ta>
            <ta e="T733" id="Seg_5117" s="T727">TVP_1965_ThreeBrothersLapta_flk.094 (006.003)</ta>
            <ta e="T741" id="Seg_5118" s="T733">TVP_1965_ThreeBrothersLapta_flk.095 (006.004)</ta>
            <ta e="T753" id="Seg_5119" s="T741">TVP_1965_ThreeBrothersLapta_flk.096 (006.005)</ta>
            <ta e="T760" id="Seg_5120" s="T753">TVP_1965_ThreeBrothersLapta_flk.097 (006.006)</ta>
            <ta e="T773" id="Seg_5121" s="T760">TVP_1965_ThreeBrothersLapta_flk.098 (006.007)</ta>
            <ta e="T779" id="Seg_5122" s="T773">TVP_1965_ThreeBrothersLapta_flk.099 (006.008)</ta>
            <ta e="T781" id="Seg_5123" s="T779">TVP_1965_ThreeBrothersLapta_flk.100 (006.009)</ta>
            <ta e="T789" id="Seg_5124" s="T781">TVP_1965_ThreeBrothersLapta_flk.101 (006.010)</ta>
            <ta e="T793" id="Seg_5125" s="T789">TVP_1965_ThreeBrothersLapta_flk.102 (006.011)</ta>
            <ta e="T795" id="Seg_5126" s="T793">TVP_1965_ThreeBrothersLapta_flk.103 (006.012)</ta>
            <ta e="T802" id="Seg_5127" s="T795">TVP_1965_ThreeBrothersLapta_flk.104 (006.013)</ta>
            <ta e="T810" id="Seg_5128" s="T802">TVP_1965_ThreeBrothersLapta_flk.105 (006.014)</ta>
            <ta e="T816" id="Seg_5129" s="T810">TVP_1965_ThreeBrothersLapta_flk.106 (006.015)</ta>
            <ta e="T820" id="Seg_5130" s="T816">TVP_1965_ThreeBrothersLapta_flk.107 (006.016)</ta>
            <ta e="T830" id="Seg_5131" s="T820">TVP_1965_ThreeBrothersLapta_flk.108 (006.017)</ta>
            <ta e="T843" id="Seg_5132" s="T830">TVP_1965_ThreeBrothersLapta_flk.109 (007.001)</ta>
            <ta e="T852" id="Seg_5133" s="T843">TVP_1965_ThreeBrothersLapta_flk.110 (007.002)</ta>
            <ta e="T858" id="Seg_5134" s="T852">TVP_1965_ThreeBrothersLapta_flk.111 (007.003)</ta>
            <ta e="T866" id="Seg_5135" s="T858">TVP_1965_ThreeBrothersLapta_flk.112 (007.004)</ta>
            <ta e="T869" id="Seg_5136" s="T866">TVP_1965_ThreeBrothersLapta_flk.113 (007.005)</ta>
            <ta e="T879" id="Seg_5137" s="T869">TVP_1965_ThreeBrothersLapta_flk.114 (007.006)</ta>
            <ta e="T894" id="Seg_5138" s="T879">TVP_1965_ThreeBrothersLapta_flk.115 (007.007)</ta>
            <ta e="T916" id="Seg_5139" s="T894">TVP_1965_ThreeBrothersLapta_flk.116 (007.008)</ta>
            <ta e="T924" id="Seg_5140" s="T916">TVP_1965_ThreeBrothersLapta_flk.117 (007.009)</ta>
            <ta e="T933" id="Seg_5141" s="T924">TVP_1965_ThreeBrothersLapta_flk.118 (007.010)</ta>
            <ta e="T938" id="Seg_5142" s="T933">TVP_1965_ThreeBrothersLapta_flk.119 (007.011)</ta>
            <ta e="T950" id="Seg_5143" s="T938">TVP_1965_ThreeBrothersLapta_flk.120 (007.012)</ta>
            <ta e="T955" id="Seg_5144" s="T950">TVP_1965_ThreeBrothersLapta_flk.121 (008.001)</ta>
            <ta e="T970" id="Seg_5145" s="T955">TVP_1965_ThreeBrothersLapta_flk.122 (008.002)</ta>
            <ta e="T978" id="Seg_5146" s="T970">TVP_1965_ThreeBrothersLapta_flk.123 (008.003)</ta>
            <ta e="T990" id="Seg_5147" s="T978">TVP_1965_ThreeBrothersLapta_flk.124 (008.004)</ta>
            <ta e="T999" id="Seg_5148" s="T990">TVP_1965_ThreeBrothersLapta_flk.125 (008.005)</ta>
            <ta e="T1005" id="Seg_5149" s="T999">TVP_1965_ThreeBrothersLapta_flk.126 (008.006)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T85" id="Seg_5150" s="T82">Lапты[ӓ]лʼ но̨кур тимнʼӓсыты.</ta>
            <ta e="T88" id="Seg_5151" s="T85">шʼите ′ӓмӓсӓkӓ ′ӣlымынто.</ta>
            <ta e="T92" id="Seg_5152" s="T88">иjаты ′нотъ ′kумд̂ышак ′орымпа.</ta>
            <ta e="T99" id="Seg_5153" s="T92">ӣ′jаты намым аша ′тӓнымымпат ′ӓсыты ку′шат kумпа.</ta>
            <ta e="T105" id="Seg_5154" s="T99">′уккур тап′чʼе̄лʼи ′ӣjа ′ӓ[ə]мыннтынык на′лʼат ′ӓ̄са.</ta>
            <ta e="T113" id="Seg_5155" s="T105">′ама ман ′ӓсаны kу̊′рампылʼ ′о̨таты ′куни ′мʼӓкка ‵атылʼ′тӓты.</ta>
            <ta e="T126" id="Seg_5156" s="T113">а ′о̨̄татый ъ̊′мӓсыkа ′ноны [на′нырык] ′ӓппа – ′ты[ъ̊̄]тынтыса ам′паты, а ′о̨̄тамтий ниlчиl ′то̨kашʼтий ′эппынты.</ta>
            <ta e="T131" id="Seg_5157" s="T126">нʼӱ̄ɣ[k]оты kəлыт′kопыты куракты[ъ]′еппа.</ta>
            <ta e="T137" id="Seg_5158" s="T131">намымты кар′нʼаlд[т]и[е]lа ′о̨тамтип ниlчик ′моkона ‵тоkкы‵ко̄лʼимым′патий.</ta>
            <ta e="T140" id="Seg_5159" s="T137">′ъ̊̄мыты на̄′lʼӓт ӓ̄[й]са.</ta>
            <ta e="T144" id="Seg_5160" s="T140">тан ′отаса ку′ча ′kъ̊ннанты?</ta>
            <ta e="T147" id="Seg_5161" s="T144">′иjа на̄′лʼат ӓ̄са.</ta>
            <ta e="T152" id="Seg_5162" s="T147">ман ′никучатий ӓс[ш]а kъ̊н′таk.</ta>
            <ta e="T155" id="Seg_5163" s="T152">матап ‵пурше′мотӓ‵ноkа.</ta>
            <ta e="T158" id="Seg_5164" s="T155">kа′р[р]е̨т ′нʼӓрот ′kолтентак.</ta>
            <ta e="T161" id="Seg_5165" s="T158">ъ̊мыты ашʼа ′kаты.</ta>
            <ta e="T178" id="Seg_5166" s="T161">kа̄′lимыт ′пӯтот та′ршӓлтыl[л]ӓ нʼамты ′чарылʼ ′о̨̄таl орасиl′тӓ̄мты ′иllа ′интыты, монты ′мыта по̄l kа̊̄lат ′паkkът ча̄ры′меңа.</ta>
            <ta e="T181" id="Seg_5167" s="T178">ӣ′jанык нилʼчиңӓ пынк′талʼтымынты.</ta>
            <ta e="T190" id="Seg_5168" s="T181">kа′рре тап ′о̨̄танты пӯ′тоɣыт но̨ку[ы]р ′пекыралʼ ′kопты на ′е̄нты.</ta>
            <ta e="T198" id="Seg_5169" s="T190">уккур ′пе̨кыраl ′kопты на ′енты ‵вӓрkы′лоkылʼ нам ′оркоlты.</ta>
            <ta e="T204" id="Seg_5170" s="T198">‵тонне′мӓɣ[k]ый ′онтий ъ̊̄тылана ‵тӱ̄нын′то̄kый.</ta>
            <ta e="T210" id="Seg_5171" s="T204">′иjа ′темнымты ′kъ̊шмынто на ′тотkылпынтыты.</ta>
            <ta e="T214" id="Seg_5172" s="T210">ниlчик ′kарре нӓ ′kъ̊нмынты.</ta>
            <ta e="T223" id="Seg_5173" s="T214">′о̨танты ′пӯтот ′пе̄ɣыllа ′тина ′ъ̊мынты ′томпылʼ ′kоптып на ‵орkылʼпантыт.</ta>
            <ta e="T226" id="Seg_5174" s="T223">kонна ′татыңыты ′со̄раl′kъ.</ta>
            <ta e="T243" id="Seg_5175" s="T226">нат кун′тъ ′ъ̊мыты мунте ′мыта kа̊lытыт пӯ′тоɣыт ниlчи ′kаllы ′иlla ӣма[ы]нтыты ′мынта ′мыта носсарылʼ ко′шар ′номтыl ′топытӓ.</ta>
            <ta e="T247" id="Seg_5176" s="T243">′иjа ′kа̄лымты насараl kу′lамынты.</ta>
            <ta e="T261" id="Seg_5177" s="T247">′мунта ′мыта ′тина ′шʼит[т]ы ′пе̄каралʼ kыпа′лолʼь ′kопто̄ɣыт ′онти ′ъ̊̄тыlа ′монты ′мыта ′kонна на ′тӱ̄мынто.</ta>
            <ta e="T266" id="Seg_5178" s="T261">′иjа ′kа̊lымты са′раlа на ′туртымынтыт.</ta>
            <ta e="T274" id="Seg_5179" s="T266">′ъ̊мыты сеlдʼжʼи ′коченылʼ ′кошар ′номтеl на′рапо′са на мӣнмы[и]нтыты.</ta>
            <ta e="T280" id="Seg_5180" s="T274">′пе̄кыралʼ ′матырла ′ме̨мпылʼ ′соkkыса на то kа′lаlтымытыт.</ta>
            <ta e="T287" id="Seg_5181" s="T280">′шʼенты ′матырла ′мемпылʼ кӓ̄лʼиl ′куlуза на′то kа′lаlти′минтыт.</ta>
            <ta e="T295" id="Seg_5182" s="T287">′ныны ′ъ̊мыты ′ниlчиңа ′соkош‵пынтыты: ′о̨̄танты ′оllы ыкы ‵lаkы′ре̄ты.</ta>
            <ta e="T303" id="Seg_5183" s="T295">′онтыт ′наш′те то′таlтукентотыт ей ′моkанӓ ′нашʼти ‵та̄тен′тотыт.</ta>
            <ta e="T313" id="Seg_5184" s="T303">′иjа о̄kалты тар′таlтылʼе ′инна ′омтылʼа ′о̨̄танты ′оlып нʼенанна ′ӱ̄тымынты.</ta>
            <ta e="T321" id="Seg_5185" s="T313">ныны ′нʼенны ‵lакаl′тип‵теɣынты kай чумпык kӓнымпа, kай ′kо̄мычек.</ta>
            <ta e="T328" id="Seg_5186" s="T321">уккур ′нʼӓркыl ′кот′пас ′порынты на ′омти ′jемынты.</ta>
            <ta e="T341" id="Seg_5187" s="T328">kотпас ′пороɣот ′о̨мтыла ′ниlчик ′kоңоты ′мунты ′мыта kар′рет тот kа′наkыт ′нокыр мо̄т ′омнонты.</ta>
            <ta e="T346" id="Seg_5188" s="T341">на ′мотоɣынаkыт kӓ̄лʼи ′о̨̄тӓ ку′ралымын′тотыт.</ta>
            <ta e="T352" id="Seg_5189" s="T346">′иjа ашʼа ′kата, ′мота kаралаɣаlте̄лʼче.</ta>
            <ta e="T359" id="Seg_5190" s="T352">чон′тоɣынты онти ′мыта ′се̨̄пылак ′вӓрkа мо̄т ′омнынты.</ta>
            <ta e="T365" id="Seg_5191" s="T359">на вӓрɣы мо̄тано̄[т]чоты ′иllа ӯте′релʼчи[е]ңыты.</ta>
            <ta e="T370" id="Seg_5192" s="T365">ы[и]llа уте′ремпыла ′kаlинты шʼун′чоɣыт ′о̄мттӓ.</ta>
            <ta e="T375" id="Seg_5193" s="T370">мо̄тkыны понӓ kумыт[н] ′нӓjа ‵таттымын′тотыт.</ta>
            <ta e="T383" id="Seg_5194" s="T375">уккур ′ира монтымыт и′jанык тӱ̄lа на̄′лʼӓт′ӓ̄сымынты.</ta>
            <ta e="T387" id="Seg_5195" s="T383">kа̄l′тытыт ′kуманты ′ӓ[й]санты?</ta>
            <ta e="T399" id="Seg_5196" s="T387">′ӣjа нӓ′lӓт ′ӓ̄сымынты. ман не kумы′машʼа ′тӓ̄нимап не kаим ашʼа ′тӓ̄нимап.</ta>
            <ta e="T403" id="Seg_5197" s="T399">′пе̄лʼикоl ′илʼиптӓl тӓ̄′тан[н]оны.</ta>
            <ta e="T409" id="Seg_5198" s="T403">а тан мы′та kай и′раңынта ′ӓ̄сантӓ.</ta>
            <ta e="T413" id="Seg_5199" s="T409">на ′ӣра ниlчик ′kъ̊̄тиңыты.</ta>
            <ta e="T424" id="Seg_5200" s="T413">ме̄мыта ′но̨кур сар′тӓтаl тымнʼӓсымыт, по̄сы ′варɣы[ъ]сар[ъ]′тъ̊тта нык мат′теминтак.</ta>
            <ta e="T432" id="Seg_5201" s="T424">′тина ′пона ′таннымпылʼ kо[у]мыт ′монты мыта ′о̨̄таl о′рат‵то̄тыт.</ta>
            <ta e="T441" id="Seg_5202" s="T432">на о̄татыт пӯтот уккур ′омтыɣытыl ′kӓ̄лʼиl ′ваkталʼ чак′ты о‵ран′то̄тыт.</ta>
            <ta e="T446" id="Seg_5203" s="T441">ниlчик ′иjа маннымпаты ′орɣылɣа ′тачаlпо̄тыт.</ta>
            <ta e="T459" id="Seg_5204" s="T446">иjа ′kа̄лымты шʼунʼ′чоɣыт ′омтылʼе тӓ̄мнимтынӓ тотkыл ‵пынты′тӓны ′чъ̊ктып и′jат ′kо[а]наңмыт на ′минты‵раl ‵тӓмин′тотыт.</ta>
            <ta e="T470" id="Seg_5205" s="T459">′ӣjа ′чъ̊ктып татыралʼтыlе ′орkылпынтылʼе ′те̄мты чам[п] ′мищ[т]алʼпа̄ты чъ̊ктыт пӱ̄ка ′тонасе′пе̄мынты.</ta>
            <ta e="T475" id="Seg_5206" s="T470">′иjа ′темнимты ина на′то̄тkыl пынты.</ta>
            <ta e="T483" id="Seg_5207" s="T475">на′lӓт ′ӓ̄сымпа. тап ′темтиl‵томы ′кӓмза ′kайkъ ′неглоl′тап?</ta>
            <ta e="T488" id="Seg_5208" s="T483">на ′kумыт ′о̨тамтынӓ ′кирамин′тотыт.</ta>
            <ta e="T492" id="Seg_5209" s="T488">′kӓ̄lиl kумыт поɣоннӓ ′туш′ти kо‵lамын′то̄тыт.</ta>
            <ta e="T500" id="Seg_5210" s="T492">′иjа нӓлʼӓт на ку′раlтимын′тотыт тат lа ′ме̄сʼа ‵туш′тешʼик.</ta>
            <ta e="T508" id="Seg_5211" s="T500">и′jа на͑лʼа͑т на ′ӓ̄сымынты мат ′тушʼтыɣа ′ашʼа ′тӓ̄нимак.</ta>
            <ta e="T515" id="Seg_5212" s="T508">тина съ̊р ′тъ̊ттаl ′ирра на′lӓт на ′ӓ̄сымынты.</ta>
            <ta e="T518" id="Seg_5213" s="T515">тӓ̄ ′kайkа ом′то̄лʼет.</ta>
            <ta e="T522" id="Seg_5214" s="T518">ва̊[ӓ]чит ′моттъ ′туlдиlа ‵муше′риңылыт.</ta>
            <ta e="T532" id="Seg_5215" s="T522">′иjа манным′паты монте мы′тӓ ′мо̄тkонӓ ′понӓ ′уккур ′нӓтак на̄′таннымынты.</ta>
            <ta e="T539" id="Seg_5216" s="T532">′нӓтак ′тантыlӓ ′вӓчип ′ма̄таlӓ мотонӓ ′туlдимынты.</ta>
            <ta e="T549" id="Seg_5217" s="T539">а̄ша ′кунты [′омтӓла ′пӯlа] ′ира ей ӣjа мотанӓ ‵шʼерпинтоɣий.</ta>
            <ta e="T556" id="Seg_5218" s="T549">иjа мо̄т ′шʼерлʼа мота′нокты чам ны′lе̨jоко′lампа.</ta>
            <ta e="T565" id="Seg_5219" s="T556">′ира на′лʼӓт на ӓ̄сымынты ′тӓkай kумып ′ашʼа kон′толыт.</ta>
            <ta e="T571" id="Seg_5220" s="T565">нʼенна ‵ӱ̄то′kынтытка kайkа ‵мо̄та′нокты ′нылʼтим′полыт.</ta>
            <ta e="T577" id="Seg_5221" s="T571">′иjап нʼенна ӣ′рат kӧнʼте на ′омтылʼтӓмын′то̄тыт.</ta>
            <ta e="T584" id="Seg_5222" s="T577">′вӓчит сӓ на ′титалʼпин′то̄тыт ′сеlчукылʼ апсылʼ на ′тоттымын′тотыт.</ta>
            <ta e="T592" id="Seg_5223" s="T584">′шитте kу′моkий ′по̄на ′тантыlа ′ӱ̄тып ′мо̄ттынӓ ‵тӱlти′монта[ъ]тий.</ta>
            <ta e="T597" id="Seg_5224" s="T592">на ′ӱтим пераlин′тотыт ей ′амыркулʼамынтотыт.</ta>
            <ta e="T602" id="Seg_5225" s="T597">′ӣjа ′амырʼе чун′тоɣыт нилʼчиңнӓ ‵ӱндышʼ′пынтӓ‵на.</ta>
            <ta e="T612" id="Seg_5226" s="T602">′монтӓмы′та лаптелʼ ′но̨ку[ы]р ти′мнʼӓсыт ӣмалʼе тӓп татымпотыт. сър′тъ̊ттаl ти[ы]м′нʼӓсыт‵кинте.</ta>
            <ta e="T617" id="Seg_5227" s="T612">сър′тъ̊ттаl тим′нʼӓсыт уккур нʼенʼӓтыт ӓ̄ппынтӓ.</ta>
            <ta e="T620" id="Seg_5228" s="T617">′намынык ′ӣмалʼлʼи тӱ̄м′потыт.</ta>
            <ta e="T627" id="Seg_5229" s="T620">′амырлʼе ′сомак ‵тортын′тотык ныны шʼиты на къ̊н ко′ламын′тотыт.</ta>
            <ta e="T634" id="Seg_5230" s="T627">ъ̊̄тыт ′монты мы′та ниlчик ′нӓнтыны пин′тотыт.</ta>
            <ta e="T640" id="Seg_5231" s="T634">тӓ̄lʼи ноты kумаɣумтий ненты на ′омтылʼтӓн′тотыт.</ta>
            <ta e="T647" id="Seg_5232" s="T640">′kумытыт kъ̊н′татkа[ъ̊] ′иjа ей моkо′нана ′kъ̊нkоlамнытыны.</ta>
            <ta e="T660" id="Seg_5233" s="T647">пона ′тантып′тӓɣынтыт ′иjа ′соkомтӓ сокаlтип′тӓ̄ɣыт lаптыl ′поза ′варɣы[а] тимнʼӓтыт ′нӓлʼат на ӓ̄сымынты, и′jанык.</ta>
            <ta e="T673" id="Seg_5234" s="T660">соk′kанте ′тасыт ′митъ ′тина ′опчин′тас а соk′kантоlӓ миты тӣна ′по̄лʼ ‵kа̄lа′ноlы.</ta>
            <ta e="T679" id="Seg_5235" s="T673">′иjа не kайими е̄тʼа на kъ̊тымныты.</ta>
            <ta e="T681" id="Seg_5236" s="T679">ныны lакаl′темныт.</ta>
            <ta e="T684" id="Seg_5237" s="T681">′мо̄чап ′тӱlча.</ta>
            <ta e="T692" id="Seg_5238" s="T684">ъ̊̄мыты ′мота′но̄kыт ′ъ̊̄тыl ′тӱкулӓ ′ныңка. и′ямты этыла. </ta>
            <ta e="T704" id="Seg_5239" s="T692">′ия тат куча намышак kоны′нян′ты, мат пуршимотянока kар[р]ят тап kотпаспорыkыт омтыколʼимынтак.</ta>
            <ta e="T709" id="Seg_5240" s="T704">на пит ия на щя[ӓ̄]кымыты .</ta>
            <ta e="T723" id="Seg_5241" s="T709">топтыльчеlы отамты моkына тоkыла. отанты путоkыт пе̄kыла шʼиты кошкат тарыль kоптоkын на орkылпынтыты.</ta>
            <ta e="T727" id="Seg_5242" s="T723">мотыkоль kалымты на соролпынтытынта.</ta>
            <ta e="T733" id="Seg_5243" s="T727">kотӓлʼ малʼ′чамты ей ′соkkымтынӓ то̄ каlтымынтытӓны.</ta>
            <ta e="T741" id="Seg_5244" s="T733">нара ′помтӓ ӣмпатӓ kай а̄шʼа… ′ниlчик на lа′kалʼ′теминты.</ta>
            <ta e="T753" id="Seg_5245" s="T741">то̄лʼ kӯмытын ′мо̄ттъ чап′тӱлʼче ′монте′мыта лаптӓлʼ тымнʼӓсыт о′кот kай тӱ̄′ңотӓт.</ta>
            <ta e="T760" id="Seg_5246" s="T753">′иjа о′каlты иllа ′сорылʼӓ ′мо̄тты на шʼе̄рпыты.</ta>
            <ta e="T773" id="Seg_5247" s="T760">мо̄тты ′шʼе̄рлʼа мо̄таноктъ чап ′нылӓjе. ′ира теlче̄lы монты мы′та налʼӓт ′ӓ̄са.</ta>
            <ta e="T779" id="Seg_5248" s="T773">тӓkай kоштылʼ ′kумып ашʼа кон′толыт.</ta>
            <ta e="T781" id="Seg_5249" s="T779">′нӓнна ′ӱ̄точынтытка.</ta>
            <ta e="T789" id="Seg_5250" s="T781">′иjа маннымпаты ′ират кӧнты kоптып шʼитӓна ′ме̄нтотыт.</ta>
            <ta e="T793" id="Seg_5251" s="T789">′иjа ′нʼӓнна ′иllа ′омта.</ta>
            <ta e="T795" id="Seg_5252" s="T793">ниlчик ′маннымпаты.</ta>
            <ta e="T802" id="Seg_5253" s="T795">монты мыта ′kумаkо[у]м′тий ′нʼӓнты ′омтаl ′тӱмпыла ӱ̄тым′потыт.</ta>
            <ta e="T810" id="Seg_5254" s="T802">′иjа е̨й на kо′минтиса тъ̊′ме̄й на ӱ̄тымпе′ралʼмыты.</ta>
            <ta e="T816" id="Seg_5255" s="T810">монд̂ы мы′та иjап ′ӱнна ′шʼе̄ртӓтӓ.</ta>
            <ta e="T820" id="Seg_5256" s="T816">ӱ сʼ[шʼ]е̄рlа ′иjа ′аlча.</ta>
            <ta e="T830" id="Seg_5257" s="T820">ӱ ′сʼе̄рпыl kуп kай kай ′тӓнимынтӓ ′кушʼаң ашʼа на ′иппынмынты.</ta>
            <ta e="T843" id="Seg_5258" s="T830">уккур тъ̊т чон′тоk[ɣ]ыт ′ӣjа инна чап ′kъ̊ңа монты ′мыта kуп шʼип ‵kъ̊тты′роррий′нʼӧтышʼ.</ta>
            <ta e="T852" id="Seg_5259" s="T843">ниlчик ′иппылʼа ‵ӱңкуlтим′паты kӓ̄l тӓ̄′ттоɣыт ′тина со[ъ̊]р′тъ̊′таттыт ′нʼенʼнʼа ‵lаңкуш′куна.</ta>
            <ta e="T858" id="Seg_5260" s="T852">тӓ̄ lа ′момпа ′мыта kай ′манпы‵мынтаlи[е]т.</ta>
            <ta e="T866" id="Seg_5261" s="T858">′тина шʼентык ′тӱмпыl ′ӣjап на kӓт′тотыт, ′lоптыl тымнʼӓсыт.</ta>
            <ta e="T869" id="Seg_5262" s="T866">′иjа асʼа ′kатта.</ta>
            <ta e="T879" id="Seg_5263" s="T869">ип‵пыптӓ′сӓɣыт ӣɣынтӓ kъ̊ттыlа оlчимпыl kу′мимты съ̊н′тӓтысӓ шʼитӓна ′kӓттымынтӓ.</ta>
            <ta e="T894" id="Seg_5264" s="T879">нынӓ ′шʼитымталʼи шʼиты ча[ъ̊]мантъ монты мыта ′kӓ̄lиl тымнʼӓсыт ′lоптыl ниlчик kум′по̄тыт, ‵о̄мы′нʼетыт чон′томыт ′шʼитъ сӓ′пӓимпа.</ta>
            <ta e="T916" id="Seg_5265" s="T894">омы′нʼетыт kым ′шитъ ′пасъ ′ӣмпа, а ′посы кы′па ′kъ̊тсымылʼ ′тимнʼатыт ′мунты мы′та ′оlоты ′мелʼче, а на ′мо̄тыкоl ′мо̄т ′чаңаl мотыкоl′топы ′kа̄лимынта.</ta>
            <ta e="T924" id="Seg_5266" s="T916">мо̄тыkота е̨й ′мо̄тыkоп те̄лʼша[ы]лʼ ′тӓтот ӓ̄l′пʼӓт ӣ′ппотыт.</ta>
            <ta e="T933" id="Seg_5267" s="T924">′иннӓ вӓшʼила ниlчик ′шʼиты маннымпаты ′kумыт таkkемпыlа тӓпып маннымпотыт.</ta>
            <ta e="T938" id="Seg_5268" s="T933">а тина нӓтак ме̄lчӓ̄ңка.</ta>
            <ta e="T950" id="Seg_5269" s="T938">то̄ ′kы̄[ъ̊]llа ′мо̄ты‵kопып ′инна ′шʼиты чап нʼӱңыты — мо̄нты ′нӓттак ныны на ′путиlмона.</ta>
            <ta e="T955" id="Seg_5270" s="T950">ашʼа kа′тотыт на нымтыl kумыт.</ta>
            <ta e="T970" id="Seg_5271" s="T955">вӓ̄ныl ′мотып ′чъ̊̄сыла ′но̄тына ′ӱтӓрпинтотыт ей амырпотыт, сӓртӓтта ′ира нʼе′нʼӓмты на ′иjанык на kу′ралʼтымынтыт. </ta>
            <ta e="T978" id="Seg_5272" s="T970">иl′матыl[лʼ] kу′мо̄kып ′нʼенты ′омпылʼ тӓнтыlа тӓттыколʼ кӧт ′че̄lа ′ӱтымымым′потыт.</ta>
            <ta e="T990" id="Seg_5273" s="T978">′kума kуп нʼӓнты омтыlпы′lа са[ъ]р′тъ̊ттаl тим′нʼӓсыт на и′jаса то kӓ′ртотыт и′jат мо̄ты ′kаныктӓ.</ta>
            <ta e="T999" id="Seg_5274" s="T990">нʼенты ′паllа на иlимын′тотыт и ′тӣтӓɣос ‵то ′иlимтотыт.</ta>
            <ta e="T1005" id="Seg_5275" s="T999">′чӓпта нʼемитто на сепʼе′е̄мынты.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T85" id="Seg_5276" s="T82">Laptɨ[ä]lʼ nokur timnʼäsɨtɨ.</ta>
            <ta e="T88" id="Seg_5277" s="T85">šite ämäsäqä iːlʼɨmɨnto.</ta>
            <ta e="T92" id="Seg_5278" s="T88">ijatɨ notə qumd̂ɨšak orɨmpa.</ta>
            <ta e="T99" id="Seg_5279" s="T92">iːjatɨ namɨm aša tänɨmɨmpat äsɨtɨ kušat qumpa.</ta>
            <ta e="T105" id="Seg_5280" s="T99">ukkur tapčʼeːlʼi iːja ä[ə]mɨnntɨnɨk nalʼat äːsa.</ta>
            <ta e="T113" id="Seg_5281" s="T105">ama man äsanɨ qurampɨlʼ otatɨ kuni mʼäkka atɨlʼtätɨ.</ta>
            <ta e="T126" id="Seg_5282" s="T113">a oːtatɨj əmäsɨqa nonɨ [nanɨrɨk] äppa – tɨ[əː]tɨntɨsa ampatɨ, a oːtamtij nilʼčilʼ toqaštij ɛppɨntɨ.</ta>
            <ta e="T131" id="Seg_5283" s="T126">nʼüːq[q]otɨ qəlɨtqopɨtɨ kuraktɨ[ə]eppa.</ta>
            <ta e="T137" id="Seg_5284" s="T131">namɨmtɨ karnʼalʼd[t]i[e]lʼa otamtip nilʼčik moqona toqkɨkoːlʼimɨmpatij.</ta>
            <ta e="T140" id="Seg_5285" s="T137">əːmɨtɨ naːlʼät äː[j]sa.</ta>
            <ta e="T144" id="Seg_5286" s="T140">tan otasa kuča qənnantɨ?</ta>
            <ta e="T147" id="Seg_5287" s="T144">ija naːlʼat äːsa.</ta>
            <ta e="T152" id="Seg_5288" s="T147">man nikučatij äs[š]a qəntaq.</ta>
            <ta e="T155" id="Seg_5289" s="T152">matap puršemotänoqa.</ta>
            <ta e="T158" id="Seg_5290" s="T155">qar[r]et nʼärot qoltentak.</ta>
            <ta e="T161" id="Seg_5291" s="T158">əmɨtɨ aša qatɨ.</ta>
            <ta e="T178" id="Seg_5292" s="T161">qaːlʼimɨt puːtot taršältɨlʼ[l]ä nʼamtɨ čarɨlʼ oːtalʼ orasilʼtäːmtɨ ilʼlʼa intɨtɨ, montɨ mɨta poːlʼ qaːlʼat paqqət čaːrɨmeŋa.</ta>
            <ta e="T181" id="Seg_5293" s="T178">iːjanɨk nilʼčiŋä pɨnktalʼtɨmɨntɨ.</ta>
            <ta e="T190" id="Seg_5294" s="T181">qarre tap oːtantɨ puːtoqɨt noku[ɨ]r pekɨralʼ qoptɨ na eːntɨ.</ta>
            <ta e="T198" id="Seg_5295" s="T190">ukkur pekɨralʼ qoptɨ na entɨ värqɨloqɨlʼ nam orkolʼtɨ.</ta>
            <ta e="T204" id="Seg_5296" s="T198">tonnemäq[q]ɨj ontij əːtɨlana tüːnɨntoːqɨj.</ta>
            <ta e="T210" id="Seg_5297" s="T204">ija temnɨmtɨ qəšmɨnto na totqɨlpɨntɨtɨ.</ta>
            <ta e="T214" id="Seg_5298" s="T210">nilʼčik qarre nä qənmɨntɨ.</ta>
            <ta e="T223" id="Seg_5299" s="T214">otantɨ puːtot peːqɨlʼlʼa tina əmɨntɨ tompɨlʼ qoptɨp na orqɨlʼpantɨt.</ta>
            <ta e="T226" id="Seg_5300" s="T223">qonna tatɨŋɨtɨ soːralʼqə.</ta>
            <ta e="T243" id="Seg_5301" s="T226">nat kuntə əmɨtɨ munte mɨta qalʼɨtɨt puːtoqɨt nilʼči qalʼlʼɨ ilʼlʼa iːma[ɨ]ntɨtɨ mɨnta mɨta nossarɨlʼ košar nomtɨlʼ topɨtä.</ta>
            <ta e="T247" id="Seg_5302" s="T243">ija qaːlɨmtɨ nasaralʼ qulʼamɨntɨ.</ta>
            <ta e="T261" id="Seg_5303" s="T247">munta mɨta tina šit[t]ɨ peːkaralʼ qɨpalolʼʼ qoptoːqɨt onti əːtɨlʼa montɨ mɨta qonna na tüːmɨnto.</ta>
            <ta e="T266" id="Seg_5304" s="T261">ija qalʼɨmtɨ saralʼa na turtɨmɨntɨt.</ta>
            <ta e="T274" id="Seg_5305" s="T266">əmɨtɨ selʼdʼžʼi kočenɨlʼ košar nomtelʼ naraposa na miːnmɨ[i]ntɨtɨ.</ta>
            <ta e="T280" id="Seg_5306" s="T274">peːkɨralʼ matɨrla mempɨlʼ soqqɨsa na to qalʼalʼtɨmɨtɨt.</ta>
            <ta e="T287" id="Seg_5307" s="T280">šentɨ matɨrla mempɨlʼ käːlʼilʼ kulʼuza nato qalʼalʼtimintɨt.</ta>
            <ta e="T295" id="Seg_5308" s="T287">nɨnɨ əmɨtɨ nilʼčiŋa soqošpɨntɨtɨ: oːtantɨ olʼlʼɨ ɨkɨ lʼaqɨreːtɨ.</ta>
            <ta e="T303" id="Seg_5309" s="T295">ontɨt našte totalʼtukentotɨt ej moqanä našti taːtentotɨt.</ta>
            <ta e="T313" id="Seg_5310" s="T303">ija oːqaltɨ tartalʼtɨlʼe inna omtɨlʼa oːtantɨ olʼɨp nʼenanna üːtɨmɨntɨ.</ta>
            <ta e="T321" id="Seg_5311" s="T313">nɨnɨ nʼennɨ lʼakalʼtipteqɨntɨ qaj čumpɨk qänɨmpa, qaj qoːmɨček.</ta>
            <ta e="T328" id="Seg_5312" s="T321">ukkur nʼärkɨlʼ kotpas porɨntɨ na omti jemɨntɨ.</ta>
            <ta e="T341" id="Seg_5313" s="T328">qotpas poroqot omtɨla nilʼčik qoŋotɨ muntɨ mɨta qarret tot qanaqɨt nokɨr moːt omnontɨ.</ta>
            <ta e="T346" id="Seg_5314" s="T341">na motoqɨnaqɨt qäːlʼi oːtä kuralɨmɨntotɨt.</ta>
            <ta e="T352" id="Seg_5315" s="T346">ija aša qata, mota qaralaqalʼteːlʼče.</ta>
            <ta e="T359" id="Seg_5316" s="T352">čontoqɨntɨ onti mɨta seːpɨlak värqa moːt omnɨntɨ.</ta>
            <ta e="T365" id="Seg_5317" s="T359">na värqɨ moːtanoː[t]čotɨ ilʼlʼa uːterelʼči[e]ŋɨtɨ.</ta>
            <ta e="T370" id="Seg_5318" s="T365">ɨ[i]lʼlʼa uterempɨla qalʼintɨ šunčoqɨt oːmttä.</ta>
            <ta e="T375" id="Seg_5319" s="T370">moːtqɨnɨ ponä qumɨt[n] näja tattɨmɨntotɨt.</ta>
            <ta e="T383" id="Seg_5320" s="T375">ukkur ira montɨmɨt ijanɨk tüːlʼa naːlʼätäːsɨmɨntɨ.</ta>
            <ta e="T387" id="Seg_5321" s="T383">qaːlʼtɨtɨt qumantɨ ä[j]santɨ?</ta>
            <ta e="T399" id="Seg_5322" s="T387">iːja nälʼät äːsɨmɨntɨ. man ne qumɨmaša täːnimap ne qaim aša täːnimap.</ta>
            <ta e="T403" id="Seg_5323" s="T399">peːlʼikolʼ ilʼiptälʼ täːtan[n]onɨ.</ta>
            <ta e="T409" id="Seg_5324" s="T403">a tan mɨta qaj iraŋɨnta äːsantä.</ta>
            <ta e="T413" id="Seg_5325" s="T409">na iːra nilʼčik qəːtiŋɨtɨ. </ta>
            <ta e="T424" id="Seg_5326" s="T413">meːmɨta nokur sartätalʼ tɨmnʼäsɨmɨt, poːsɨ varqɨ[ə]sar[ə]tətta nɨk mattemintak.</ta>
            <ta e="T432" id="Seg_5327" s="T424">tina pona tannɨmpɨlʼ qo[u]mɨt montɨ mɨta oːtalʼ orattoːtɨt.</ta>
            <ta e="T441" id="Seg_5328" s="T432">na oːtatɨt puːtot ukkur omtɨqɨtɨlʼ qäːlʼilʼ vaqtalʼ čaktɨ orantoːtɨt.</ta>
            <ta e="T446" id="Seg_5329" s="T441">nilʼčik ija mannɨmpatɨ orqɨlqa tačalʼpoːtɨt.</ta>
            <ta e="T459" id="Seg_5330" s="T446">ija qaːlɨmtɨ šunʼčoqɨt omtɨlʼe täːmnimtɨnä totqɨl pɨntɨtänɨ čəktɨp ijat qo[a]naŋmɨt na mintɨralʼ tämintotɨt.</ta>
            <ta e="T470" id="Seg_5331" s="T459">iːja čəktɨp tatɨralʼtɨlʼe orqɨlpɨntɨlʼe teːmtɨ čam[p] miš[t]alʼpaːtɨ čəktɨt püːka tonasepeːmɨntɨ.</ta>
            <ta e="T475" id="Seg_5332" s="T470">ija temnimtɨ ina natoːtqɨlʼ pɨntɨ.</ta>
            <ta e="T483" id="Seg_5333" s="T475">nalʼät äːsɨmpa. tap temtilʼtomɨ kämza qajqə neglolʼtap?</ta>
            <ta e="T488" id="Seg_5334" s="T483">na qumɨt otamtɨnä kiramintotɨt.</ta>
            <ta e="T492" id="Seg_5335" s="T488">qäːlʼilʼ qumɨt poqonnä tušti qolʼamɨntoːtɨt.</ta>
            <ta e="T500" id="Seg_5336" s="T492">ija nälʼät na kuralʼtimɨntotɨt tat lʼa meːsʼa tuštešik.</ta>
            <ta e="T508" id="Seg_5337" s="T500">ija nalʼat na äːsɨmɨntɨ mat tuštɨqa aša täːnimak.</ta>
            <ta e="T515" id="Seg_5338" s="T508">tina sər təttalʼ irra nalʼät na äːsɨmɨntɨ.</ta>
            <ta e="T518" id="Seg_5339" s="T515">täː qajqa omtoːlʼet.</ta>
            <ta e="T522" id="Seg_5340" s="T518">va[ä]čit mottə tulʼdilʼa mušeriŋɨlɨt.</ta>
            <ta e="T532" id="Seg_5341" s="T522">ija mannɨmpatɨ monte mɨtä moːtqonä ponä ukkur nätak naːtannɨmɨntɨ.</ta>
            <ta e="T539" id="Seg_5342" s="T532">nätak tantɨlʼä väčip maːtalʼä motonä tulʼdimɨntɨ.</ta>
            <ta e="T549" id="Seg_5343" s="T539">aːša kuntɨ [omtäla puːlʼa] ira ej iːja motanä šerpintoqij.</ta>
            <ta e="T556" id="Seg_5344" s="T549">ija moːt šerlʼa motanoktɨ čam nɨlʼejokolʼampa.</ta>
            <ta e="T565" id="Seg_5345" s="T556">ira nalʼät na äːsɨmɨntɨ täqaj qumɨp aša qontolɨt.</ta>
            <ta e="T571" id="Seg_5346" s="T565">nʼenna üːtoqɨntɨtka qajqa moːtanoktɨ nɨlʼtimpolɨt.</ta>
            <ta e="T577" id="Seg_5347" s="T571">ijap nʼenna iːrat qönʼte na omtɨlʼtämɨntoːtɨt.</ta>
            <ta e="T584" id="Seg_5348" s="T577">väčit sä na titalʼpintoːtɨt selʼčukɨlʼ apsɨlʼ na tottɨmɨntotɨt.</ta>
            <ta e="T592" id="Seg_5349" s="T584">šitte qumoqij poːna tantɨlʼa üːtɨp moːttɨnä tülʼtimonta[ə]tij.</ta>
            <ta e="T597" id="Seg_5350" s="T592">na ütim peralʼintotɨt ej amɨrkulʼamɨntotɨt.</ta>
            <ta e="T602" id="Seg_5351" s="T597">iːja amɨrʼe čuntoqɨt nilʼčiŋnä ündɨšpɨntäna.</ta>
            <ta e="T612" id="Seg_5352" s="T602">montämɨta laptelʼ noku[ɨ]r timnʼäsɨt iːmalʼe täp tatɨmpotɨt. sərtəttalʼ ti[ɨ]mnʼäsɨtkinte.</ta>
            <ta e="T617" id="Seg_5353" s="T612">sərtəttalʼ timnʼäsɨt ukkur nʼenʼätɨt äːppɨntä.</ta>
            <ta e="T620" id="Seg_5354" s="T617">namɨnɨk iːmalʼlʼi tüːmpotɨt.</ta>
            <ta e="T627" id="Seg_5355" s="T620">amɨrlʼe somak tortɨntotɨk nɨnɨ šitɨ na kən kolamɨntotɨt.</ta>
            <ta e="T634" id="Seg_5356" s="T627">əːtɨt montɨ mɨta nilʼčik näntɨnɨ pintotɨt.</ta>
            <ta e="T640" id="Seg_5357" s="T634">täːlʼi notɨ qumaqumtij nentɨ na omtɨlʼtäntotɨt.</ta>
            <ta e="T647" id="Seg_5358" s="T640">qumɨtɨt qəntatqa[ə] ija ej moqonana qənqolʼamnɨtɨnɨ.</ta>
            <ta e="T660" id="Seg_5359" s="T647">pona tantɨptäqɨntɨt ija. soqomtä sokalʼtiptäːqɨt. lʼaptɨlʼ poza varqɨ[a] timnʼätɨt nälʼat na äːsɨmɨntɨ, ijanɨk.</ta>
            <ta e="T673" id="Seg_5360" s="T660">soqqante tasɨt mitə tina opčintas a soqqantolʼä mitɨ tiːna poːlʼ qaːlʼanolʼɨ.</ta>
            <ta e="T679" id="Seg_5361" s="T673">ija ne qajimi eːča na qətɨmnɨtɨ.</ta>
            <ta e="T681" id="Seg_5362" s="T679">nɨnɨ lʼakalʼtemnɨt.</ta>
            <ta e="T684" id="Seg_5363" s="T681">moːčap tülʼča.</ta>
            <ta e="T692" id="Seg_5364" s="T684">əːmɨtɨ motanoːqɨt əːtɨlʼ tükulä nɨŋka. iяmtɨ ɛtɨla.</ta>
            <ta e="T704" id="Seg_5365" s="T692">iя tat kuča namɨšak qonɨnяntɨ, mat puršimotяnoka qar[r]яt tap qotpasporɨqɨt omtɨkolʼimɨntak.</ta>
            <ta e="T709" id="Seg_5366" s="T704">na pit iя na šя[äː]kɨmɨtɨ .</ta>
            <ta e="T723" id="Seg_5367" s="T709">toptɨlʼčelʼɨ otamtɨ moqɨna toqɨla. otantɨ putoqɨt peːqɨla šitɨ koškat tarɨlʼ qoptoqɨn na orqɨlpɨntɨtɨ.</ta>
            <ta e="T727" id="Seg_5368" s="T723">motɨqolʼ qalɨmtɨ na sorolpɨntɨtɨnta.</ta>
            <ta e="T733" id="Seg_5369" s="T727">qotälʼ malʼčamtɨ ej soqqɨmtɨnä toː kalʼtɨmɨntɨtänɨ.</ta>
            <ta e="T741" id="Seg_5370" s="T733">nara pomtä iːmpatä qaj aːša… nilʼčik na lʼaqalʼtemintɨ.</ta>
            <ta e="T753" id="Seg_5371" s="T741">toːlʼ quːmɨtɨn moːttə čaptülʼče montemɨta laptälʼ tɨmnʼäsɨt okot qaj tüːŋotät.</ta>
            <ta e="T760" id="Seg_5372" s="T753">ija okalʼtɨ ilʼlʼa sorɨlʼä moːttɨ na šeːrpɨtɨ.</ta>
            <ta e="T773" id="Seg_5373" s="T760">moːttɨ šeːrlʼa moːtanoktə čap nɨläje. ira telʼčeːlʼɨ montɨ mɨta nalʼät äːsa.</ta>
            <ta e="T779" id="Seg_5374" s="T773">täqaj qoštɨlʼ qumɨp aša kontolɨt.</ta>
            <ta e="T781" id="Seg_5375" s="T779">nänna üːtočɨntɨtka.</ta>
            <ta e="T789" id="Seg_5376" s="T781">ija mannɨmpatɨ irat köntɨ qoptɨp šitäna meːntotɨt.</ta>
            <ta e="T793" id="Seg_5377" s="T789">ija nʼänna ilʼlʼa omta.</ta>
            <ta e="T795" id="Seg_5378" s="T793">nilʼčik mannɨmpatɨ.</ta>
            <ta e="T802" id="Seg_5379" s="T795">montɨ mɨta qumaqo[u]mtij nʼäntɨ omtalʼ tümpɨla üːtɨmpotɨt.</ta>
            <ta e="T810" id="Seg_5380" s="T802">ija ej na qomintisa təmeːj na üːtɨmperalʼmɨtɨ.</ta>
            <ta e="T816" id="Seg_5381" s="T810">mond̂ɨ mɨta ijap ünna šeːrtätä.</ta>
            <ta e="T820" id="Seg_5382" s="T816">ü sʼ[š]eːrlʼa ija alʼča.</ta>
            <ta e="T830" id="Seg_5383" s="T820">ü sʼeːrpɨlʼ qup qaj qaj tänimɨntä kušaŋ aša na ippɨnmɨntɨ.</ta>
            <ta e="T843" id="Seg_5384" s="T830">ukkur tət čontoq[q]ɨt iːja inna čap qəŋa montɨ mɨta qup šip qəttɨrorrijnʼötɨš.</ta>
            <ta e="T852" id="Seg_5385" s="T843">nilʼčik ippɨlʼa üŋkulʼtimpatɨ qäːlʼ täːttoqɨt tina so[ə]rtətattɨt nʼenʼnʼa lʼaŋkuškuna.</ta>
            <ta e="T858" id="Seg_5386" s="T852">täː lʼa mompa mɨta qaj manpɨmɨntalʼi[e]t.</ta>
            <ta e="T866" id="Seg_5387" s="T858">tina šentɨk tümpɨlʼ iːjap na qättotɨt, lʼoptɨlʼ tɨmnʼäsɨt.</ta>
            <ta e="T869" id="Seg_5388" s="T866">ija asʼa qatta.</ta>
            <ta e="T879" id="Seg_5389" s="T869">ippɨptäsäqɨt iːqɨntä qəttɨlʼa olʼčimpɨlʼ qumimtɨ səntätɨsä šitäna qättɨmɨntä.</ta>
            <ta e="T894" id="Seg_5390" s="T879">nɨnä šitɨmtalʼi šitɨ ča[ə]mantə montɨ mɨta qäːlʼilʼ tɨmnʼäsɨt lʼoptɨlʼ nilʼčik qumpoːtɨt, oːmɨnʼetɨt čontomɨt šitə säpäimpa.</ta>
            <ta e="T916" id="Seg_5391" s="T894">omɨnʼetɨt qɨm šitə pasə iːmpa, a posɨ kɨpa qətsɨmɨlʼ timnʼatɨt muntɨ mɨta olʼotɨ melʼče, a na moːtɨkolʼ moːt čaŋalʼ motɨkolʼtopɨ qaːlimɨnta.</ta>
            <ta e="T924" id="Seg_5392" s="T916">moːtɨqota ej moːtɨqop teːlʼša[ɨ]lʼ tätot äːlʼpʼät iːppotɨt.</ta>
            <ta e="T933" id="Seg_5393" s="T924">innä väšila nilʼčik šitɨ mannɨmpatɨ qumɨt taqqempɨlʼa täpɨp mannɨmpotɨt.</ta>
            <ta e="T938" id="Seg_5394" s="T933">a tina nätak meːlʼčäːŋka.</ta>
            <ta e="T950" id="Seg_5395" s="T938">toː qɨː[ə]lʼlʼa moːtɨqopɨp inna šitɨ čap nʼüŋɨtɨ — moːntɨ nättak nɨnɨ na putilʼmona.</ta>
            <ta e="T955" id="Seg_5396" s="T950">aša qatotɨt na nɨmtɨlʼ qumɨt.</ta>
            <ta e="T970" id="Seg_5397" s="T955">väːnɨlʼ motɨp čəːsɨla noːtɨna ütärpintotɨt ej amɨrpotɨt, särtätta ira nʼenʼämtɨ na ijanɨk na quralʼtɨmɨntɨt. </ta>
            <ta e="T978" id="Seg_5398" s="T970">ilʼmatɨlʼ[lʼ] qumoːqɨp nʼentɨ ompɨlʼ täntɨlʼa tättɨkolʼ köt čeːlʼa ütɨmɨmɨmpotɨt.</ta>
            <ta e="T990" id="Seg_5399" s="T978">quma qup nʼäntɨ omtɨlʼpɨlʼa sa[ə]rtəttalʼ timnʼäsɨt na ijasa to qärtotɨt ijat moːtɨ qanɨktä.</ta>
            <ta e="T999" id="Seg_5400" s="T990">nʼentɨ palʼlʼa na ilʼimɨntotɨt i tiːtäqos to ilʼimtotɨt.</ta>
            <ta e="T1005" id="Seg_5401" s="T999">čäpta nʼemitto na sepʼeeːmɨntɨ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T85" id="Seg_5402" s="T82">Laptɨlʼ nɔːkur timnʼäsɨtɨ. </ta>
            <ta e="T88" id="Seg_5403" s="T85">Šite ämäsäqä ilɨmmɨntɔː. </ta>
            <ta e="T92" id="Seg_5404" s="T88">Ijatɨ nɔːtɨ qumtɨšak orɨmpa. </ta>
            <ta e="T99" id="Seg_5405" s="T92">Iːjatɨ namɨm aša tɛnɨmɨmpat äsɨtɨ kušat qumpa. </ta>
            <ta e="T105" id="Seg_5406" s="T99">Ukkur tapčʼeːlʼi iːja əmɨntɨnɨk nalʼat ɛsa. </ta>
            <ta e="T113" id="Seg_5407" s="T105">Ama man äsanɨ kurampɨlʼ ɔːtatɨ kuni mäkkä atɨltätɨ. </ta>
            <ta e="T126" id="Seg_5408" s="T113">A ɔːtatɨj əmäsɨqa nɔːnɨ (nanɨrɨk) ɛppa – tətɨntɨsa ampatɨ, a ɔːtantij nılʼčʼilʼ tɔːqaštij ɛppɨntɨ. </ta>
            <ta e="T131" id="Seg_5409" s="T126">Nʼuːqotɨ qəːlɨt qopɨtɨ kuraktɨ ɛppa. </ta>
            <ta e="T137" id="Seg_5410" s="T131">Namɨmtɨ karnʼaltila ɔːtamtip nılʼčʼik moqona tɔːqqɨqolʼimɨmpatij. </ta>
            <ta e="T140" id="Seg_5411" s="T137">Əmɨtɨ naːlʼät ɛsa. </ta>
            <ta e="T144" id="Seg_5412" s="T140">Tan ɔːtasa kučʼa qənnantɨ? </ta>
            <ta e="T147" id="Seg_5413" s="T144">Ija naːlʼat ɛsa. </ta>
            <ta e="T152" id="Seg_5414" s="T147">Man nʼi kučʼatij äša qəntak. </ta>
            <ta e="T155" id="Seg_5415" s="T152">Mat tap puršɨmɔːtänoːqa. </ta>
            <ta e="T158" id="Seg_5416" s="T155">Karrät nʼärot qoltɛntak. </ta>
            <ta e="T161" id="Seg_5417" s="T158">Əmɨtɨ aša qatɨ. </ta>
            <ta e="T178" id="Seg_5418" s="T161">Qaːlʼimɨt puːtot taršältɨlä nʼamtɨ čʼarɨlʼ ɔːtalʼ orasilʼ tɛːmtɨ ılla iːntɨtɨ, montɨ mɨta poːlʼ kalat paqqət čʼarɨm eŋa. </ta>
            <ta e="T181" id="Seg_5419" s="T178">Iːjanɨk nılʼčʼɨŋä pɨŋtalʼtɨmmɨntɨ. </ta>
            <ta e="T190" id="Seg_5420" s="T181">Karrä tap ɔːtäntɨ puːtoːqɨt nɔːkɨr pekɨralʼ qoptɨ na ɛːntɨ. </ta>
            <ta e="T198" id="Seg_5421" s="T190">Ukkur pekɨralʼ qoptɨ na ɛːntɨ wärqɨlɔːqɨlʼ nam orqoltɨ. </ta>
            <ta e="T204" id="Seg_5422" s="T198">Toːnna mäːqıj ontıːj əːtɨla na tünɨntɔːqıj. </ta>
            <ta e="T210" id="Seg_5423" s="T204">Ija tɛːmnɨmtɨ kəš mɨnto na totqɨlpɨntɨtɨ. </ta>
            <ta e="T214" id="Seg_5424" s="T210">Nılʼčʼɨk karrä nä qənmmɨntɨ. </ta>
            <ta e="T223" id="Seg_5425" s="T214">Ɔːtantɨ puːtot peːkɨlla tına əmɨntɨ tompɨlʼ qoptɨp na orqɨlʼpantɨt. </ta>
            <ta e="T226" id="Seg_5426" s="T223">Konna tatɨŋɨtɨ sɔːralqə. </ta>
            <ta e="T243" id="Seg_5427" s="T226">Nat kuntə əmɨtɨ munte mɨta qalɨtɨt puːtoːqɨt nılʼčʼi qallɨ ılla iːmmɨntɨtɨ mɨnta mɨta nɔssarɨlʼ košar nɔːmtɨlʼ topɨtä. </ta>
            <ta e="T247" id="Seg_5428" s="T243">Ija qaːlɨmtɨ na saralqulammɨntɨ. </ta>
            <ta e="T261" id="Seg_5429" s="T247">Munta mɨta tına šittɨ pekaralʼ kɨpalɔːlʼ qoptoːqıt ontıː əːtɨla montɨ mɨta konna na tüːmmɨntɔː. </ta>
            <ta e="T266" id="Seg_5430" s="T261">Ija qalɨmtɨ sarala na tuːrtɨmmɨntɨt. </ta>
            <ta e="T274" id="Seg_5431" s="T266">Əmɨtɨ seːlʼčʼi kočʼenɨlʼ košar nɔːmtelʼ naraposa na miːnmɨntɨtɨ. </ta>
            <ta e="T280" id="Seg_5432" s="T274">Pekɨralʼ maːtɨrla meːmpɨlʼ soqqɨsa na tokalaltɨmmɨtɨt. </ta>
            <ta e="T287" id="Seg_5433" s="T280">Šentɨ maːtɨrla meːmpɨlʼ qälilʼ kulusa na tokalaltɨmmɨntɨt. </ta>
            <ta e="T295" id="Seg_5434" s="T287">Nɨːnɨ əmɨtɨ nılʼčʼiŋa soqošpɨntɨtɨ: ɔːtantɨ olɨ ɨkɨ laqɨrätɨ. </ta>
            <ta e="T303" id="Seg_5435" s="T295">Ontɨt našte totalʼ tukɛntɔːtɨt ɛj moqanä našti taːtɛntɔːtɨt. </ta>
            <ta e="T313" id="Seg_5436" s="T303">Ija ɔːqaltɨ tartaltɨlä ınna omtɨlʼa ɔːtantɨ olɨp nʼena na üːtɨmmɨntɨ. </ta>
            <ta e="T321" id="Seg_5437" s="T313">Nɨːnɨ nʼennɨ laqaltiptäːqɨntɨ qaj čʼumpɨk qänɨmpa, qaj qɔːmɨčʼäk. </ta>
            <ta e="T328" id="Seg_5438" s="T321">Ukkur nʼarqɨlʼ kotpas pɔːrɨntɨ na omtij ɛːmmɨntɨ. </ta>
            <ta e="T341" id="Seg_5439" s="T328">Kotpas pɔːroːqot ɔːmtɨla nılʼčʼik qoŋotɨ montɨ mɨta karrät toːt qanaqqɨt nɔːkɨr mɔːt ɔːmnontɨ. </ta>
            <ta e="T346" id="Seg_5440" s="T341">Na mɔːtoqanaqɨt qälʼi ɔːtä kuralɨmmɨntɔːtɨt. </ta>
            <ta e="T352" id="Seg_5441" s="T346">Ija aša qata, mɔːtta karala qaltɛlʼčʼe. </ta>
            <ta e="T359" id="Seg_5442" s="T352">Čʼontoːqɨntɨ onti mɨta seːpɨlak wärqa mɔːt ɔːmnɨntɨ. </ta>
            <ta e="T365" id="Seg_5443" s="T359">Na wärqɨ mɔːtanoːt čʼɔːtɨ ılla uterɛlʼčʼiŋɨtɨ. </ta>
            <ta e="T370" id="Seg_5444" s="T365">İlla uterɛːmpɨla qalintɨ šünʼčʼoːqɨt ɔːmttä. </ta>
            <ta e="T375" id="Seg_5445" s="T370">Mɔːtqɨnɨ ponä qumɨt nʼeːja tattɨmmɨntɔːtɨt. </ta>
            <ta e="T383" id="Seg_5446" s="T375">Ukkur ira montɨ mɨt ijanɨk tüːla naːlʼät ɛsɨmmɨntɨ. </ta>
            <ta e="T387" id="Seg_5447" s="T383">Qaːlʼ tɨtɨt qumantɨ ɛsantɨ? </ta>
            <ta e="T399" id="Seg_5448" s="T387">Iːja nälʼät ɛsɨmmɨntɨ: Man nʼe qumɨm aša tɛnʼimap, nʼe qaim aša tɛnimap. </ta>
            <ta e="T403" id="Seg_5449" s="T399">Pɛlʼikɔːl ilʼiptäːlʼ tätan nɔːnɨ. </ta>
            <ta e="T409" id="Seg_5450" s="T403">A tan mɨta qaj iraŋɨnta ɛsantä. </ta>
            <ta e="T413" id="Seg_5451" s="T409">Na iːra nılʼčʼik kətiŋɨtɨ. </ta>
            <ta e="T424" id="Seg_5452" s="T413">Meː mɨta nɔːkur sartätalʼ tɨmnʼäsɨmɨt, poːsɨ warqɨ sartətta nɨk mat ɛːmmintak. </ta>
            <ta e="T432" id="Seg_5453" s="T424">Tına pona tannɨmpɨlʼ qumɨt montɨ mɨta ɔːtalʼ orattɔːtɨt. </ta>
            <ta e="T441" id="Seg_5454" s="T432">Na ɔːtatɨt puːtot ukkur ɔːmtɨkɨtɨlʼ qälʼilʼ waqtalʼ čʼaktɨ orantɔːtɨt. </ta>
            <ta e="T446" id="Seg_5455" s="T441">Nılʼčʼik ija mannɨmpatɨ orqɨlqa tačʼalpɔːtɨt. </ta>
            <ta e="T459" id="Seg_5456" s="T446">Ija qaːlɨntɨ šunʼčʼoːqɨt ɔːmtɨlʼä tɛːmnim tɨnä totqɨlpɨntɨtäːnɨ čʼəktɨp ijat qanaŋmɨt na mintɨralʼ tämmintɔːtɨt. </ta>
            <ta e="T470" id="Seg_5457" s="T459">Iːja čʼəktɨp tatɨraltɨlä orqɨlpɨntɨlä tɛːmtɨ čʼam mišalpatɨ čʼəktɨt püqa toːnna seːpemmɨntɨ. </ta>
            <ta e="T475" id="Seg_5458" s="T470">Ija tɛːmnimtɨ ınna na totqɨlpɨntɨ. </ta>
            <ta e="T483" id="Seg_5459" s="T475">Nalʼät ɛsɨmpa: Tap tɛːmtilʼ tomɨ kämsa qajqə neklɔːltap? </ta>
            <ta e="T488" id="Seg_5460" s="T483">Na qumɨt ɔːtam tɨnä kırammintɔːtɨt. </ta>
            <ta e="T492" id="Seg_5461" s="T488">Qälilʼ qumɨt poqonnä tuštiqolammɨntɔːtɨt. </ta>
            <ta e="T500" id="Seg_5462" s="T492">Ija nälʼät na kuraltimmɨntɔːtɨt tat lʼa meːsʼa tuštäšik. </ta>
            <ta e="T508" id="Seg_5463" s="T500">Ija nalʼat na ɛsɨmmɨntɨ: Mat tuštɨqa aša tɛnimak. </ta>
            <ta e="T515" id="Seg_5464" s="T508">Tına sər təttalʼ irra nalʼät na ɛsɨmmɨntɨ. </ta>
            <ta e="T518" id="Seg_5465" s="T515">Tɛː qajqa ɔːmtoːlet. </ta>
            <ta e="T522" id="Seg_5466" s="T518">Wäčʼit mɔːttə tuːltila mušeriŋɨlɨt. </ta>
            <ta e="T532" id="Seg_5467" s="T522">Ija mannɨmpatɨ monte mɨtä mɔːtqonä ponä ukkur nätak na tannɨmmɨntɨ. </ta>
            <ta e="T539" id="Seg_5468" s="T532">Nätak tantɨlä wäčʼip maːtalä mɔːtto nä tultimmɨntɨ. </ta>
            <ta e="T549" id="Seg_5469" s="T539">Aša kuntɨ ɔːmtäla puːla ira ɛj iːja mɔːtta nä šeːrpintɔːqıj. </ta>
            <ta e="T556" id="Seg_5470" s="T549">Ija mɔːt šeːrlʼa mɔːtan ɔːktɨ čʼam nɨllɛːjoqolampa. </ta>
            <ta e="T565" id="Seg_5471" s="T556">Ira nalʼät na ɛsɨmmɨntɨ: tɛː qaj qumɨp aša qontɔːlɨt. </ta>
            <ta e="T571" id="Seg_5472" s="T565">Nʼenna üːtoqɨntɨtqa qajqa mɔːtan ɔːktɨ nɨlʼtimpɔːlɨt. </ta>
            <ta e="T577" id="Seg_5473" s="T571">Ijap nʼenna iːrat qönʼte na omtɨltämmɨntɔːtɨt. </ta>
            <ta e="T584" id="Seg_5474" s="T577">Wäčʼitsä na tıːtalʼpintɔːtɨt selʼčʼuqɨlʼ apsɨlʼ na tottɨmmɨntɔːtɨt. </ta>
            <ta e="T592" id="Seg_5475" s="T584">Šitte qumoːqıj pona tantɨla ütɨp mɔːttɨ nä tültimmontətij. </ta>
            <ta e="T597" id="Seg_5476" s="T592">Na ütim peralintɔːtɨt ɛj amɨrqulʼammɨntɔːtɨt. </ta>
            <ta e="T602" id="Seg_5477" s="T597">Iːja amɨrä čʼuntoːqɨt nılʼčʼiŋä üntɨšpɨntäna. </ta>
            <ta e="T612" id="Seg_5478" s="T602">Montä mɨta Laptelʼ nɔːkur timnʼäsɨt imalʼlʼä täp taːtɨmpɔːtɨt, sərtəttalʼ timnʼäsɨtkinte. </ta>
            <ta e="T617" id="Seg_5479" s="T612">Sərtəttalʼ timnʼäsɨt ukkur nʼenʼätɨt ɛːppɨntä. </ta>
            <ta e="T620" id="Seg_5480" s="T617">Namɨnɨk imalʼlʼi tümpɔːtɨt. </ta>
            <ta e="T627" id="Seg_5481" s="T620">Amɨrlʼä somak tortɨntɔːtɨk nɨːnɨ šitɨ na qənqolammɨntɔːtɨt. </ta>
            <ta e="T634" id="Seg_5482" s="T627">Əːtɨt montɨ mɨta nilʼčʼik nʼentɨ nɨː pintɔːtɨt. </ta>
            <ta e="T640" id="Seg_5483" s="T634">Täːlʼi nɔːtɨ qumaqumtij nʼentɨ na omtɨltɛntɔːtɨt. </ta>
            <ta e="T647" id="Seg_5484" s="T640">Qumɨtɨt qəntaːtqo ija ɛj moqona na qənqolamnɨtɨnɨ. </ta>
            <ta e="T660" id="Seg_5485" s="T647">Pona tantɨptäːqɨntɨt ija soqomtä soqaltiptäːqɨt Laptɨlʼ poːsa warqɨ timnʼätɨt nälʼat na ɛsɨmmɨntɨ, ijanɨk. </ta>
            <ta e="T673" id="Seg_5486" s="T660">Soqqante tasɨt mitə tına opčʼintas, a soqqant olä mitɨ tına poːlʼ kalan olɨ. </ta>
            <ta e="T679" id="Seg_5487" s="T673">Ija ne qajimi äčʼa na kətɨmnɨtɨ. </ta>
            <ta e="T681" id="Seg_5488" s="T679">Nɨːnɨ laqaltɛːmnɨt. </ta>
            <ta e="T684" id="Seg_5489" s="T681">Mɔːtt čʼap tülʼčʼa. </ta>
            <ta e="T692" id="Seg_5490" s="T684">Əmɨtɨ mɔːtan ɔːqɨt ətɨlʼ tükulä nɨŋka, ijamtɨ ɛtɨla. </ta>
            <ta e="T704" id="Seg_5491" s="T692">Ija tat kučʼa namɨšak qonɨnʼantɨ, mat puršimɔːtʼanoːqa karrʼat tap kotpas pɔːrɨqɨt ɔːmtɨkolʼimmɨntak. </ta>
            <ta e="T709" id="Seg_5492" s="T704">Na pit ija na šäqɨmmɨtɨ. </ta>
            <ta e="T723" id="Seg_5493" s="T709">Tɔːptɨlʼ čʼeːlɨ ɔːtamtɨ moqɨna tɔːqɨla ɔːtantɨ puːtoːqɨt peːkɨla šitɨ koškat tarɨlʼ qoptoːqın na orqɨlpɨntɨtɨ. </ta>
            <ta e="T727" id="Seg_5494" s="T723">Moːtɨkɔːlʼ qalɨmtɨ na sɔːrɔːlpɨntɨtɨnta. </ta>
            <ta e="T733" id="Seg_5495" s="T727">Kotälʼ malʼčʼamtɨ ɛj soqqɨmtɨ nä tokaltɨmmɨntɨtäːnɨ. </ta>
            <ta e="T741" id="Seg_5496" s="T733">Narapomtä iːmpatä qaj aša … nılʼčʼik na laqalʼtɛːmmintɨ. </ta>
            <ta e="T753" id="Seg_5497" s="T741">Tolʼ qumɨtɨn mɔːttə čʼap tüːlʼčʼe monte mɨta Laptälʼ tɨmnʼäsɨt okoːt qaj tüːŋɔːtät. </ta>
            <ta e="T760" id="Seg_5498" s="T753">Ija ɔːqalʼtɨ ılla sɔːrɨlʼä mɔːttɨ na šeːrpɨtɨ. </ta>
            <ta e="T773" id="Seg_5499" s="T760">Mɔːttɨ šeːrlʼa mɔːtan ɔːktə čʼap nɨllɛːje ira tälʼ čʼeːlɨ montɨ mɨta nalʼät ɛsa. </ta>
            <ta e="T779" id="Seg_5500" s="T773">Tɛː qaj qoštɨlʼ qumɨp aša qontɔːlɨt. </ta>
            <ta e="T781" id="Seg_5501" s="T779">Nänna üːtočʼɨntɨtqa. </ta>
            <ta e="T789" id="Seg_5502" s="T781">Ija mannɨmpatɨ irat qöntɨ koptɨp šitä na meːntɔːtɨt. </ta>
            <ta e="T793" id="Seg_5503" s="T789">Ija nʼenna ılla omta. </ta>
            <ta e="T795" id="Seg_5504" s="T793">Nılʼčʼik mannɨmpatɨ. </ta>
            <ta e="T802" id="Seg_5505" s="T795">Montɨ mɨta qumaqumtij nʼentɨ omtalʼ tümpɨla ütɨmpɔːtɨt. </ta>
            <ta e="T810" id="Seg_5506" s="T802">Ija ɛj na qomintisa təm ɛj na ütɨmperalʼmɨtɨ. </ta>
            <ta e="T816" id="Seg_5507" s="T810">Montɨ mɨta ijap ün na šeːrtätä. </ta>
            <ta e="T820" id="Seg_5508" s="T816">Ü šeːrla ija alʼčʼa. </ta>
            <ta e="T830" id="Seg_5509" s="T820">Ü sʼeːrpɨlʼ qup qaj qaj tɛnimɨntä kušaŋ aša na ippɨnmɨntɨ. </ta>
            <ta e="T843" id="Seg_5510" s="T830">Ukkur tət čʼontoːqɨt iːja ınna čʼap qəŋa montɨ mɨta qup šıp qəttɨrorrij nʼoːtɨš. </ta>
            <ta e="T852" id="Seg_5511" s="T843">Nılʼčʼik ippɨlʼa üŋkultimpatɨ qäːlʼ täːttoːqɨt tına sərtətattɨt nʼenʼnʼa laŋkuškuna. </ta>
            <ta e="T858" id="Seg_5512" s="T852">Tɛː lʼa mompa mɨta qaj manpɨmmɨntalit. </ta>
            <ta e="T866" id="Seg_5513" s="T858">Tına šentɨk tümpɨlʼ iːjap na qättɔːtɨt, Loptɨlʼ tɨmnʼäsɨt. </ta>
            <ta e="T869" id="Seg_5514" s="T866">Ija asʼa qatta. </ta>
            <ta e="T879" id="Seg_5515" s="T869">Ippɨptäː säqɨt iːqɨntä qəttɨla ɔːlʼčʼimpɨlʼ qumimtɨ səntätɨsä šitä na qättɨmmɨntä. </ta>
            <ta e="T894" id="Seg_5516" s="T879">Nɨːnä šitɨmtalʼi šitɨ čʼammantə montɨ mɨta qälilʼ tɨmnʼäsɨt Loptɨlʼ nılʼčʼik qumpɔːtɨt, ɔːmɨnʼeːtɨt čʼontoːmɨt šitə säpäıːmpa. </ta>
            <ta e="T916" id="Seg_5517" s="T894">Ɔːmɨnʼeːtɨt kɨm šitə pasəıːmpa, a poːsɨ kɨpa qəːtsɨmɨlʼ timnʼatɨt muntɨ mɨta olotɨ meːl čʼäː, a na moːtɨkɔːl mɔːt čʼaŋalʼ moːtɨkɔːl tɔːpɨ qalimmɨnta. </ta>
            <ta e="T924" id="Seg_5518" s="T916">Mɔːtɨkota ɛj mɔːtɨqop täːlʼšɨlʼ tät toːt älpät ippɔːtɨt. </ta>
            <ta e="T933" id="Seg_5519" s="T924">İnnä wešila nılʼčʼik šitɨ mannɨmpatɨ qumɨt taqqempɨla täpɨp mannɨmpɔːtɨt. </ta>
            <ta e="T938" id="Seg_5520" s="T933">A tına nätak meːl čʼäːŋka. </ta>
            <ta e="T950" id="Seg_5521" s="T938">Toː qəlla mɔːtɨqopɨp ınna šitɨ čʼap nüŋɨtɨ— montɨ nättak nɨːnɨ na putilmɔːnna. </ta>
            <ta e="T955" id="Seg_5522" s="T950">Aša qatɔːtɨt na nɨmtɨlʼ qumɨt. </ta>
            <ta e="T970" id="Seg_5523" s="T955">Wänɨlʼ mɔːtɨp čʼəsɨla nɔːtɨ na ütɛːrpintɔːtɨt ɛj amɨrpɔːtɨt, särtätta ira nʼenʼämtɨ na ijanɨk na quralʼtɨmmɨntɨt. </ta>
            <ta e="T978" id="Seg_5524" s="T970">Ilʼmatɨlʼ qumoːqɨp nʼentɨ ompɨltäntɨla tɛːttɨkolʼ köt čʼeːla ütɨmɨmɨmpɔːtɨt. </ta>
            <ta e="T990" id="Seg_5525" s="T978">Qumaqup nʼentɨ omtɨlpɨla sərtəttalʼ timnʼäsɨt na ijasa tɔː qärtɔːtɨt ijat mɔːttɨ qanɨktä. </ta>
            <ta e="T999" id="Seg_5526" s="T990">Nʼentɨ palla na ilimmɨntɔːtɨt i tiːtä qos to ilimtɔːtɨt. </ta>
            <ta e="T1005" id="Seg_5527" s="T999">Čʼäpta nʼeːmit toː na seːpʼe ɛːmmɨntɨ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T83" id="Seg_5528" s="T82">Laptɨ-lʼ</ta>
            <ta e="T84" id="Seg_5529" s="T83">nɔːkur</ta>
            <ta e="T85" id="Seg_5530" s="T84">timnʼä-sɨ-tɨ</ta>
            <ta e="T86" id="Seg_5531" s="T85">šite</ta>
            <ta e="T87" id="Seg_5532" s="T86">ämä-sä-qä</ta>
            <ta e="T88" id="Seg_5533" s="T87">ilɨ-mmɨ-ntɔː</ta>
            <ta e="T89" id="Seg_5534" s="T88">ija-tɨ</ta>
            <ta e="T90" id="Seg_5535" s="T89">nɔːtɨ</ta>
            <ta e="T91" id="Seg_5536" s="T90">qum-tɨ-šak</ta>
            <ta e="T92" id="Seg_5537" s="T91">orɨ-m-pa</ta>
            <ta e="T93" id="Seg_5538" s="T92">iːja-tɨ</ta>
            <ta e="T94" id="Seg_5539" s="T93">namɨ-m</ta>
            <ta e="T95" id="Seg_5540" s="T94">aša</ta>
            <ta e="T96" id="Seg_5541" s="T95">tɛnɨmɨ-mpa-t</ta>
            <ta e="T97" id="Seg_5542" s="T96">äsɨ-tɨ</ta>
            <ta e="T98" id="Seg_5543" s="T97">kušat</ta>
            <ta e="T99" id="Seg_5544" s="T98">qu-mpa</ta>
            <ta e="T100" id="Seg_5545" s="T99">ukkur</ta>
            <ta e="T101" id="Seg_5546" s="T100">tapčʼeːlʼi</ta>
            <ta e="T102" id="Seg_5547" s="T101">iːja</ta>
            <ta e="T103" id="Seg_5548" s="T102">əmɨ-ntɨ-nɨk</ta>
            <ta e="T104" id="Seg_5549" s="T103">na-lʼa-t</ta>
            <ta e="T105" id="Seg_5550" s="T104">ɛsa</ta>
            <ta e="T106" id="Seg_5551" s="T105">ama</ta>
            <ta e="T107" id="Seg_5552" s="T106">man</ta>
            <ta e="T108" id="Seg_5553" s="T107">äsa-nɨ</ta>
            <ta e="T109" id="Seg_5554" s="T108">kura-mpɨlʼ</ta>
            <ta e="T110" id="Seg_5555" s="T109">ɔːta-tɨ</ta>
            <ta e="T111" id="Seg_5556" s="T110">kuni</ta>
            <ta e="T112" id="Seg_5557" s="T111">mäkkä</ta>
            <ta e="T113" id="Seg_5558" s="T112">atɨ-lt-ätɨ</ta>
            <ta e="T114" id="Seg_5559" s="T113">a</ta>
            <ta e="T115" id="Seg_5560" s="T114">ɔːta-tɨ-j</ta>
            <ta e="T116" id="Seg_5561" s="T115">əmä-sɨ-qa</ta>
            <ta e="T117" id="Seg_5562" s="T116">nɔː</ta>
            <ta e="T118" id="Seg_5563" s="T117">nanɨrɨk</ta>
            <ta e="T119" id="Seg_5564" s="T118">ɛ-ppa</ta>
            <ta e="T120" id="Seg_5565" s="T119">tətɨ-ntɨ-sa</ta>
            <ta e="T121" id="Seg_5566" s="T120">am-pa-tɨ</ta>
            <ta e="T122" id="Seg_5567" s="T121">a</ta>
            <ta e="T123" id="Seg_5568" s="T122">ɔːta-nti-j</ta>
            <ta e="T124" id="Seg_5569" s="T123">nılʼčʼi-lʼ</ta>
            <ta e="T125" id="Seg_5570" s="T124">tɔːqaš-ti-j</ta>
            <ta e="T126" id="Seg_5571" s="T125">ɛ-ppɨ-ntɨ</ta>
            <ta e="T127" id="Seg_5572" s="T126">nʼuːqo-tɨ</ta>
            <ta e="T128" id="Seg_5573" s="T127">qəːlɨ-t</ta>
            <ta e="T129" id="Seg_5574" s="T128">qopɨ-tɨ</ta>
            <ta e="T130" id="Seg_5575" s="T129">kurak-tɨ</ta>
            <ta e="T131" id="Seg_5576" s="T130">ɛ-ppa</ta>
            <ta e="T132" id="Seg_5577" s="T131">namɨ-m-tɨ</ta>
            <ta e="T133" id="Seg_5578" s="T132">karnʼ-al-ti-la</ta>
            <ta e="T134" id="Seg_5579" s="T133">ɔːta-m-ti-p</ta>
            <ta e="T135" id="Seg_5580" s="T134">nılʼčʼi-k</ta>
            <ta e="T136" id="Seg_5581" s="T135">moqona</ta>
            <ta e="T137" id="Seg_5582" s="T136">tɔːqqɨ-q-olʼim-ɨ-mpa-tij</ta>
            <ta e="T138" id="Seg_5583" s="T137">əmɨ-tɨ</ta>
            <ta e="T139" id="Seg_5584" s="T138">naː-lʼä-t</ta>
            <ta e="T140" id="Seg_5585" s="T139">ɛsa</ta>
            <ta e="T141" id="Seg_5586" s="T140">tat</ta>
            <ta e="T142" id="Seg_5587" s="T141">ɔːta-sa</ta>
            <ta e="T143" id="Seg_5588" s="T142">kučʼa</ta>
            <ta e="T144" id="Seg_5589" s="T143">qən-na-ntɨ</ta>
            <ta e="T145" id="Seg_5590" s="T144">ija</ta>
            <ta e="T146" id="Seg_5591" s="T145">na-lʼa-t</ta>
            <ta e="T147" id="Seg_5592" s="T146">ɛsa</ta>
            <ta e="T148" id="Seg_5593" s="T147">man</ta>
            <ta e="T149" id="Seg_5594" s="T148">nʼi</ta>
            <ta e="T150" id="Seg_5595" s="T149">kučʼatij</ta>
            <ta e="T151" id="Seg_5596" s="T150">äša</ta>
            <ta e="T152" id="Seg_5597" s="T151">qən-ta-k</ta>
            <ta e="T153" id="Seg_5598" s="T152">mat</ta>
            <ta e="T154" id="Seg_5599" s="T153">tap</ta>
            <ta e="T155" id="Seg_5600" s="T154">puršɨ-mɔːt-ä-noːqa</ta>
            <ta e="T156" id="Seg_5601" s="T155">karrä-t</ta>
            <ta e="T157" id="Seg_5602" s="T156">nʼäro-t</ta>
            <ta e="T158" id="Seg_5603" s="T157">qo-lt-ɛnta-k</ta>
            <ta e="T159" id="Seg_5604" s="T158">əmɨ-tɨ</ta>
            <ta e="T160" id="Seg_5605" s="T159">aša</ta>
            <ta e="T161" id="Seg_5606" s="T160">qatɨ</ta>
            <ta e="T162" id="Seg_5607" s="T161">qaːlʼi-mɨt</ta>
            <ta e="T163" id="Seg_5608" s="T162">puːto-t</ta>
            <ta e="T164" id="Seg_5609" s="T163">tar-š-äl-tɨ-lä</ta>
            <ta e="T165" id="Seg_5610" s="T164">nʼa-m-tɨ</ta>
            <ta e="T166" id="Seg_5611" s="T165">čʼarɨ-lʼ</ta>
            <ta e="T167" id="Seg_5612" s="T166">ɔːta-lʼ</ta>
            <ta e="T168" id="Seg_5613" s="T167">ora-si-lʼ</ta>
            <ta e="T169" id="Seg_5614" s="T168">tɛːmtɨ</ta>
            <ta e="T170" id="Seg_5615" s="T169">ılla</ta>
            <ta e="T171" id="Seg_5616" s="T170">iː-ntɨ-tɨ</ta>
            <ta e="T172" id="Seg_5617" s="T171">montɨ</ta>
            <ta e="T173" id="Seg_5618" s="T172">mɨta</ta>
            <ta e="T174" id="Seg_5619" s="T173">poː-lʼ</ta>
            <ta e="T175" id="Seg_5620" s="T174">kala-t</ta>
            <ta e="T176" id="Seg_5621" s="T175">paqqə-t</ta>
            <ta e="T177" id="Seg_5622" s="T176">čʼarɨm</ta>
            <ta e="T178" id="Seg_5623" s="T177">e-ŋa</ta>
            <ta e="T179" id="Seg_5624" s="T178">iːja-nɨk</ta>
            <ta e="T180" id="Seg_5625" s="T179">nılʼčʼɨ-ŋä</ta>
            <ta e="T181" id="Seg_5626" s="T180">pɨŋtalʼtɨ-mmɨ-ntɨ</ta>
            <ta e="T182" id="Seg_5627" s="T181">karrä</ta>
            <ta e="T183" id="Seg_5628" s="T182">tap</ta>
            <ta e="T184" id="Seg_5629" s="T183">ɔːtä-n-tɨ</ta>
            <ta e="T185" id="Seg_5630" s="T184">puːtoː-qɨt</ta>
            <ta e="T186" id="Seg_5631" s="T185">nɔːkɨr</ta>
            <ta e="T187" id="Seg_5632" s="T186">pekɨra-lʼ</ta>
            <ta e="T188" id="Seg_5633" s="T187">qoptɨ</ta>
            <ta e="T189" id="Seg_5634" s="T188">na</ta>
            <ta e="T190" id="Seg_5635" s="T189">ɛː-ntɨ</ta>
            <ta e="T191" id="Seg_5636" s="T190">ukkur</ta>
            <ta e="T192" id="Seg_5637" s="T191">pekɨra-lʼ</ta>
            <ta e="T193" id="Seg_5638" s="T192">qoptɨ</ta>
            <ta e="T194" id="Seg_5639" s="T193">na</ta>
            <ta e="T195" id="Seg_5640" s="T194">ɛː-ntɨ</ta>
            <ta e="T196" id="Seg_5641" s="T195">wärqɨ-lɔːqɨ-lʼ</ta>
            <ta e="T197" id="Seg_5642" s="T196">na-m</ta>
            <ta e="T198" id="Seg_5643" s="T197">or-qol-tɨ</ta>
            <ta e="T199" id="Seg_5644" s="T198">toːnna</ta>
            <ta e="T200" id="Seg_5645" s="T199">mäː-qıj</ta>
            <ta e="T201" id="Seg_5646" s="T200">ontıːj</ta>
            <ta e="T202" id="Seg_5647" s="T201">əːtɨ-la</ta>
            <ta e="T203" id="Seg_5648" s="T202">na</ta>
            <ta e="T204" id="Seg_5649" s="T203">tü-nɨ-ntɔː-qıj</ta>
            <ta e="T205" id="Seg_5650" s="T204">ija</ta>
            <ta e="T206" id="Seg_5651" s="T205">tɛːmnɨ-m-tɨ</ta>
            <ta e="T207" id="Seg_5652" s="T206">kəš</ta>
            <ta e="T208" id="Seg_5653" s="T207">mɨnto</ta>
            <ta e="T209" id="Seg_5654" s="T208">na</ta>
            <ta e="T210" id="Seg_5655" s="T209">tot-qɨl-pɨ-ntɨ-tɨ</ta>
            <ta e="T211" id="Seg_5656" s="T210">nılʼčʼɨ-k</ta>
            <ta e="T212" id="Seg_5657" s="T211">karrä</ta>
            <ta e="T213" id="Seg_5658" s="T212">nä</ta>
            <ta e="T214" id="Seg_5659" s="T213">qən-mmɨ-ntɨ</ta>
            <ta e="T215" id="Seg_5660" s="T214">ɔːta-n-tɨ</ta>
            <ta e="T216" id="Seg_5661" s="T215">puːto-t</ta>
            <ta e="T217" id="Seg_5662" s="T216">peː-kɨ-lla</ta>
            <ta e="T218" id="Seg_5663" s="T217">tına</ta>
            <ta e="T219" id="Seg_5664" s="T218">əmɨ-n-tɨ</ta>
            <ta e="T220" id="Seg_5665" s="T219">tom-pɨlʼ</ta>
            <ta e="T221" id="Seg_5666" s="T220">qoptɨ-p</ta>
            <ta e="T222" id="Seg_5667" s="T221">na</ta>
            <ta e="T223" id="Seg_5668" s="T222">orqɨlʼ-pa-ntɨ-t</ta>
            <ta e="T224" id="Seg_5669" s="T223">konna</ta>
            <ta e="T225" id="Seg_5670" s="T224">tatɨ-ŋɨ-tɨ</ta>
            <ta e="T226" id="Seg_5671" s="T225">sɔːr-al-qə</ta>
            <ta e="T227" id="Seg_5672" s="T226">na-t</ta>
            <ta e="T228" id="Seg_5673" s="T227">kuntə</ta>
            <ta e="T229" id="Seg_5674" s="T228">əmɨ-tɨ</ta>
            <ta e="T230" id="Seg_5675" s="T229">munte</ta>
            <ta e="T231" id="Seg_5676" s="T230">mɨta</ta>
            <ta e="T232" id="Seg_5677" s="T231">qalɨ-t-ɨ-t</ta>
            <ta e="T233" id="Seg_5678" s="T232">puːtoː-qɨt</ta>
            <ta e="T234" id="Seg_5679" s="T233">nılʼčʼi</ta>
            <ta e="T235" id="Seg_5680" s="T234">qallɨ</ta>
            <ta e="T236" id="Seg_5681" s="T235">ılla</ta>
            <ta e="T237" id="Seg_5682" s="T236">iː-mmɨ-ntɨ-tɨ</ta>
            <ta e="T238" id="Seg_5683" s="T237">mɨnta</ta>
            <ta e="T239" id="Seg_5684" s="T238">mɨta</ta>
            <ta e="T240" id="Seg_5685" s="T239">nɔ-ssar-ɨ-lʼ</ta>
            <ta e="T241" id="Seg_5686" s="T240">košar</ta>
            <ta e="T242" id="Seg_5687" s="T241">nɔːmtɨ-lʼ</ta>
            <ta e="T243" id="Seg_5688" s="T242">topɨ-tä</ta>
            <ta e="T244" id="Seg_5689" s="T243">ija</ta>
            <ta e="T245" id="Seg_5690" s="T244">qaːlɨ-m-tɨ</ta>
            <ta e="T246" id="Seg_5691" s="T245">na</ta>
            <ta e="T247" id="Seg_5692" s="T246">sar-al-q-ulam-mɨ-ntɨ</ta>
            <ta e="T248" id="Seg_5693" s="T247">munta</ta>
            <ta e="T249" id="Seg_5694" s="T248">mɨta</ta>
            <ta e="T250" id="Seg_5695" s="T249">tına</ta>
            <ta e="T251" id="Seg_5696" s="T250">šittɨ</ta>
            <ta e="T252" id="Seg_5697" s="T251">pekara-lʼ</ta>
            <ta e="T253" id="Seg_5698" s="T252">kɨpa-lɔː-lʼ</ta>
            <ta e="T254" id="Seg_5699" s="T253">qoptoː-qı-t</ta>
            <ta e="T255" id="Seg_5700" s="T254">ontıː</ta>
            <ta e="T256" id="Seg_5701" s="T255">əːtɨ-la</ta>
            <ta e="T257" id="Seg_5702" s="T256">montɨ</ta>
            <ta e="T258" id="Seg_5703" s="T257">mɨta</ta>
            <ta e="T259" id="Seg_5704" s="T258">konna</ta>
            <ta e="T260" id="Seg_5705" s="T259">na</ta>
            <ta e="T261" id="Seg_5706" s="T260">tüː-mmɨ-ntɔː</ta>
            <ta e="T262" id="Seg_5707" s="T261">ija</ta>
            <ta e="T263" id="Seg_5708" s="T262">qalɨ-m-tɨ</ta>
            <ta e="T264" id="Seg_5709" s="T263">sara-la</ta>
            <ta e="T265" id="Seg_5710" s="T264">na</ta>
            <ta e="T266" id="Seg_5711" s="T265">tuːr-tɨ-mmɨ-ntɨ-t</ta>
            <ta e="T267" id="Seg_5712" s="T266">əmɨ-tɨ</ta>
            <ta e="T268" id="Seg_5713" s="T267">seːlʼčʼi</ta>
            <ta e="T269" id="Seg_5714" s="T268">kočʼen-ɨ-lʼ</ta>
            <ta e="T270" id="Seg_5715" s="T269">košar</ta>
            <ta e="T271" id="Seg_5716" s="T270">nɔːmte-lʼ</ta>
            <ta e="T272" id="Seg_5717" s="T271">nara-po-sa</ta>
            <ta e="T273" id="Seg_5718" s="T272">na</ta>
            <ta e="T274" id="Seg_5719" s="T273">miː-n-mɨ-ntɨ-tɨ</ta>
            <ta e="T275" id="Seg_5720" s="T274">pekɨra-lʼ</ta>
            <ta e="T276" id="Seg_5721" s="T275">maːtɨ-r-la</ta>
            <ta e="T277" id="Seg_5722" s="T276">meː-mpɨlʼ</ta>
            <ta e="T278" id="Seg_5723" s="T277">soqqɨ-sa</ta>
            <ta e="T279" id="Seg_5724" s="T278">na</ta>
            <ta e="T280" id="Seg_5725" s="T279">tok-al-altɨ-mmɨ-tɨ-t</ta>
            <ta e="T281" id="Seg_5726" s="T280">šentɨ</ta>
            <ta e="T282" id="Seg_5727" s="T281">maːtɨ-r-la</ta>
            <ta e="T283" id="Seg_5728" s="T282">meː-mpɨlʼ</ta>
            <ta e="T284" id="Seg_5729" s="T283">qäli-lʼ</ta>
            <ta e="T286" id="Seg_5730" s="T285">na</ta>
            <ta e="T287" id="Seg_5731" s="T286">tok-al-altɨ-mmɨ-ntɨ-t</ta>
            <ta e="T288" id="Seg_5732" s="T287">nɨːnɨ</ta>
            <ta e="T289" id="Seg_5733" s="T288">əmɨ-tɨ</ta>
            <ta e="T290" id="Seg_5734" s="T289">nılʼčʼi-ŋa</ta>
            <ta e="T291" id="Seg_5735" s="T290">soqoš-pɨ-ntɨ-tɨ</ta>
            <ta e="T292" id="Seg_5736" s="T291">ɔːta-n-tɨ</ta>
            <ta e="T293" id="Seg_5737" s="T292">olɨ</ta>
            <ta e="T294" id="Seg_5738" s="T293">ɨkɨ</ta>
            <ta e="T295" id="Seg_5739" s="T294">laqɨ-r-ätɨ</ta>
            <ta e="T296" id="Seg_5740" s="T295">ontɨt</ta>
            <ta e="T297" id="Seg_5741" s="T296">našte</ta>
            <ta e="T299" id="Seg_5742" s="T298">tu-k-ɛntɔː-tɨt</ta>
            <ta e="T300" id="Seg_5743" s="T299">ɛj</ta>
            <ta e="T301" id="Seg_5744" s="T300">moqanä</ta>
            <ta e="T303" id="Seg_5745" s="T302">taːt-ɛntɔː-tɨt</ta>
            <ta e="T304" id="Seg_5746" s="T303">ija</ta>
            <ta e="T305" id="Seg_5747" s="T304">ɔːqal-tɨ</ta>
            <ta e="T306" id="Seg_5748" s="T305">tar-taltɨ-lä</ta>
            <ta e="T307" id="Seg_5749" s="T306">ınna</ta>
            <ta e="T308" id="Seg_5750" s="T307">omtɨ-lʼa</ta>
            <ta e="T309" id="Seg_5751" s="T308">ɔːta-n-tɨ</ta>
            <ta e="T310" id="Seg_5752" s="T309">olɨ-p</ta>
            <ta e="T311" id="Seg_5753" s="T310">nʼena</ta>
            <ta e="T312" id="Seg_5754" s="T311">na</ta>
            <ta e="T313" id="Seg_5755" s="T312">üːtɨ-mmɨ-ntɨ</ta>
            <ta e="T314" id="Seg_5756" s="T313">nɨːnɨ</ta>
            <ta e="T315" id="Seg_5757" s="T314">nʼennɨ</ta>
            <ta e="T316" id="Seg_5758" s="T315">laqalti-ptäː-qɨn-tɨ</ta>
            <ta e="T317" id="Seg_5759" s="T316">qaj</ta>
            <ta e="T318" id="Seg_5760" s="T317">čʼumpɨ-k</ta>
            <ta e="T319" id="Seg_5761" s="T318">qä-nɨ-mpa</ta>
            <ta e="T320" id="Seg_5762" s="T319">qaj</ta>
            <ta e="T321" id="Seg_5763" s="T320">qɔːmɨčʼä-k</ta>
            <ta e="T322" id="Seg_5764" s="T321">ukkur</ta>
            <ta e="T323" id="Seg_5765" s="T322">nʼar-qɨ-lʼ</ta>
            <ta e="T324" id="Seg_5766" s="T323">kotpas</ta>
            <ta e="T325" id="Seg_5767" s="T324">pɔːrɨ-ntɨ</ta>
            <ta e="T326" id="Seg_5768" s="T325">na</ta>
            <ta e="T327" id="Seg_5769" s="T326">om-tij</ta>
            <ta e="T328" id="Seg_5770" s="T327">ɛː-mmɨ-ntɨ</ta>
            <ta e="T329" id="Seg_5771" s="T328">kotpas</ta>
            <ta e="T330" id="Seg_5772" s="T329">pɔːroː-qot</ta>
            <ta e="T331" id="Seg_5773" s="T330">ɔːmtɨ-la</ta>
            <ta e="T332" id="Seg_5774" s="T331">nılʼčʼi-k</ta>
            <ta e="T333" id="Seg_5775" s="T332">qo-ŋo-tɨ</ta>
            <ta e="T334" id="Seg_5776" s="T333">montɨ</ta>
            <ta e="T335" id="Seg_5777" s="T334">mɨta</ta>
            <ta e="T336" id="Seg_5778" s="T335">karrä-t</ta>
            <ta e="T337" id="Seg_5779" s="T336">toː-t</ta>
            <ta e="T338" id="Seg_5780" s="T337">qanaq-qɨt</ta>
            <ta e="T339" id="Seg_5781" s="T338">nɔːkɨr</ta>
            <ta e="T340" id="Seg_5782" s="T339">mɔːt</ta>
            <ta e="T341" id="Seg_5783" s="T340">ɔːmno-ntɨ</ta>
            <ta e="T342" id="Seg_5784" s="T341">na</ta>
            <ta e="T343" id="Seg_5785" s="T342">mɔːt-o-qan-a-qɨt</ta>
            <ta e="T344" id="Seg_5786" s="T343">qälʼi</ta>
            <ta e="T345" id="Seg_5787" s="T344">ɔːtä</ta>
            <ta e="T346" id="Seg_5788" s="T345">kur-al-ɨ-mmɨ-ntɔː-tɨt</ta>
            <ta e="T347" id="Seg_5789" s="T346">ija</ta>
            <ta e="T348" id="Seg_5790" s="T347">aša</ta>
            <ta e="T349" id="Seg_5791" s="T348">qata</ta>
            <ta e="T350" id="Seg_5792" s="T349">mɔːt-ta</ta>
            <ta e="T351" id="Seg_5793" s="T350">kara-la</ta>
            <ta e="T352" id="Seg_5794" s="T351">qal-t-ɛlʼčʼe</ta>
            <ta e="T353" id="Seg_5795" s="T352">čʼontoː-qɨn-tɨ</ta>
            <ta e="T354" id="Seg_5796" s="T353">onti</ta>
            <ta e="T355" id="Seg_5797" s="T354">mɨta</ta>
            <ta e="T356" id="Seg_5798" s="T355">seːpɨlak</ta>
            <ta e="T357" id="Seg_5799" s="T356">wärqa</ta>
            <ta e="T358" id="Seg_5800" s="T357">mɔːt</ta>
            <ta e="T359" id="Seg_5801" s="T358">ɔːmnɨ-ntɨ</ta>
            <ta e="T360" id="Seg_5802" s="T359">na</ta>
            <ta e="T361" id="Seg_5803" s="T360">wärqɨ</ta>
            <ta e="T362" id="Seg_5804" s="T361">mɔːt-a-n-oː-t</ta>
            <ta e="T363" id="Seg_5805" s="T362">čʼɔːtɨ</ta>
            <ta e="T364" id="Seg_5806" s="T363">ılla</ta>
            <ta e="T365" id="Seg_5807" s="T364">ute-r-ɛlʼčʼi-ŋɨ-tɨ</ta>
            <ta e="T366" id="Seg_5808" s="T365">i̇lla</ta>
            <ta e="T367" id="Seg_5809" s="T366">ute-r-ɛː-mpɨ-la</ta>
            <ta e="T368" id="Seg_5810" s="T367">qali-n-tɨ</ta>
            <ta e="T369" id="Seg_5811" s="T368">šünʼčʼoː-qɨt</ta>
            <ta e="T370" id="Seg_5812" s="T369">ɔːmttä</ta>
            <ta e="T371" id="Seg_5813" s="T370">mɔːt-qɨnɨ</ta>
            <ta e="T372" id="Seg_5814" s="T371">ponä</ta>
            <ta e="T373" id="Seg_5815" s="T372">qum-ɨ-t</ta>
            <ta e="T374" id="Seg_5816" s="T373">nʼeː-ja</ta>
            <ta e="T375" id="Seg_5817" s="T374">tattɨ-mmɨ-ntɔː-tɨt</ta>
            <ta e="T376" id="Seg_5818" s="T375">ukkur</ta>
            <ta e="T377" id="Seg_5819" s="T376">ira</ta>
            <ta e="T378" id="Seg_5820" s="T377">montɨ</ta>
            <ta e="T379" id="Seg_5821" s="T378">mɨt</ta>
            <ta e="T380" id="Seg_5822" s="T379">ija-nɨk</ta>
            <ta e="T381" id="Seg_5823" s="T380">tüː-la</ta>
            <ta e="T382" id="Seg_5824" s="T381">naː-lʼä-t</ta>
            <ta e="T383" id="Seg_5825" s="T382">ɛsɨ-mmɨ-ntɨ</ta>
            <ta e="T384" id="Seg_5826" s="T383">qaː-lʼ</ta>
            <ta e="T385" id="Seg_5827" s="T384">tɨtɨ-t</ta>
            <ta e="T386" id="Seg_5828" s="T385">qum-a-ntɨ</ta>
            <ta e="T387" id="Seg_5829" s="T386">ɛsa-ntɨ</ta>
            <ta e="T388" id="Seg_5830" s="T387">iːja</ta>
            <ta e="T389" id="Seg_5831" s="T388">nä-lʼä-t</ta>
            <ta e="T390" id="Seg_5832" s="T389">ɛsɨ-mmɨ-ntɨ</ta>
            <ta e="T391" id="Seg_5833" s="T390">man</ta>
            <ta e="T392" id="Seg_5834" s="T391">nʼe</ta>
            <ta e="T393" id="Seg_5835" s="T392">qum-ɨ-m</ta>
            <ta e="T394" id="Seg_5836" s="T393">aša</ta>
            <ta e="T395" id="Seg_5837" s="T394">tɛnʼima-p</ta>
            <ta e="T396" id="Seg_5838" s="T395">nʼe</ta>
            <ta e="T397" id="Seg_5839" s="T396">qai-m</ta>
            <ta e="T398" id="Seg_5840" s="T397">aša</ta>
            <ta e="T399" id="Seg_5841" s="T398">tɛnima-p</ta>
            <ta e="T400" id="Seg_5842" s="T399">pɛlʼi-kɔːl</ta>
            <ta e="T401" id="Seg_5843" s="T400">ilʼi-ptäː-lʼ</ta>
            <ta e="T402" id="Seg_5844" s="T401">täta-n</ta>
            <ta e="T403" id="Seg_5845" s="T402">nɔː-nɨ</ta>
            <ta e="T404" id="Seg_5846" s="T403">a</ta>
            <ta e="T405" id="Seg_5847" s="T404">tat</ta>
            <ta e="T406" id="Seg_5848" s="T405">mɨta</ta>
            <ta e="T407" id="Seg_5849" s="T406">qaj</ta>
            <ta e="T408" id="Seg_5850" s="T407">ira-ŋɨ-nta</ta>
            <ta e="T409" id="Seg_5851" s="T408">ɛsa-ntä</ta>
            <ta e="T410" id="Seg_5852" s="T409">na</ta>
            <ta e="T411" id="Seg_5853" s="T410">iːra</ta>
            <ta e="T412" id="Seg_5854" s="T411">nılʼčʼi-k</ta>
            <ta e="T413" id="Seg_5855" s="T412">kəti-ŋɨ-tɨ</ta>
            <ta e="T414" id="Seg_5856" s="T413">meː</ta>
            <ta e="T415" id="Seg_5857" s="T414">mɨta</ta>
            <ta e="T416" id="Seg_5858" s="T415">nɔːkur</ta>
            <ta e="T417" id="Seg_5859" s="T416">sar-täta-lʼ</ta>
            <ta e="T418" id="Seg_5860" s="T417">tɨmnʼä-sɨ-mɨt</ta>
            <ta e="T419" id="Seg_5861" s="T418">poːsɨ</ta>
            <ta e="T420" id="Seg_5862" s="T419">warqɨ</ta>
            <ta e="T421" id="Seg_5863" s="T420">sar-tətta</ta>
            <ta e="T422" id="Seg_5864" s="T421">nɨk</ta>
            <ta e="T423" id="Seg_5865" s="T422">mat</ta>
            <ta e="T424" id="Seg_5866" s="T423">ɛː-mmi-nta-k</ta>
            <ta e="T425" id="Seg_5867" s="T424">tına</ta>
            <ta e="T426" id="Seg_5868" s="T425">pona</ta>
            <ta e="T427" id="Seg_5869" s="T426">tannɨ-mpɨlʼ</ta>
            <ta e="T428" id="Seg_5870" s="T427">qum-ɨ-t</ta>
            <ta e="T429" id="Seg_5871" s="T428">montɨ</ta>
            <ta e="T430" id="Seg_5872" s="T429">mɨta</ta>
            <ta e="T431" id="Seg_5873" s="T430">ɔːta-lʼ</ta>
            <ta e="T432" id="Seg_5874" s="T431">ora-ttɔː-tɨt</ta>
            <ta e="T433" id="Seg_5875" s="T432">na</ta>
            <ta e="T434" id="Seg_5876" s="T433">ɔːta-t-ɨ-t</ta>
            <ta e="T435" id="Seg_5877" s="T434">puːto-t</ta>
            <ta e="T436" id="Seg_5878" s="T435">ukkur</ta>
            <ta e="T437" id="Seg_5879" s="T436">ɔːmtɨ-kɨtɨ-lʼ</ta>
            <ta e="T438" id="Seg_5880" s="T437">qäli-lʼ</ta>
            <ta e="T439" id="Seg_5881" s="T438">waqta-lʼ</ta>
            <ta e="T440" id="Seg_5882" s="T439">čʼaktɨ</ta>
            <ta e="T441" id="Seg_5883" s="T440">ora-ntɔː-tɨt</ta>
            <ta e="T442" id="Seg_5884" s="T441">nılʼčʼi-k</ta>
            <ta e="T443" id="Seg_5885" s="T442">ija</ta>
            <ta e="T444" id="Seg_5886" s="T443">mannɨ-mpa-tɨ</ta>
            <ta e="T445" id="Seg_5887" s="T444">orqɨl-qa</ta>
            <ta e="T446" id="Seg_5888" s="T445">tačʼal-pɔː-tɨt</ta>
            <ta e="T447" id="Seg_5889" s="T446">ija</ta>
            <ta e="T448" id="Seg_5890" s="T447">qaːlɨ-n-tɨ</ta>
            <ta e="T449" id="Seg_5891" s="T448">šunʼčʼoː-qɨt</ta>
            <ta e="T450" id="Seg_5892" s="T449">ɔːmtɨ-lʼä</ta>
            <ta e="T451" id="Seg_5893" s="T450">tɛːmni-m</ta>
            <ta e="T452" id="Seg_5894" s="T451">tɨnä</ta>
            <ta e="T453" id="Seg_5895" s="T452">tot-qɨl-pɨ-ntɨ-täː-nɨ</ta>
            <ta e="T454" id="Seg_5896" s="T453">čʼəktɨ-p</ta>
            <ta e="T455" id="Seg_5897" s="T454">ija-t</ta>
            <ta e="T456" id="Seg_5898" s="T455">qanaŋ-mɨt</ta>
            <ta e="T457" id="Seg_5899" s="T456">na</ta>
            <ta e="T458" id="Seg_5900" s="T457">*mi-ntɨ-r-alʼ</ta>
            <ta e="T459" id="Seg_5901" s="T458">tä-mmi-ntɔː-tɨt</ta>
            <ta e="T460" id="Seg_5902" s="T459">iːja</ta>
            <ta e="T461" id="Seg_5903" s="T460">čʼəktɨ-p</ta>
            <ta e="T462" id="Seg_5904" s="T461">tatɨ-r-altɨ-lä</ta>
            <ta e="T463" id="Seg_5905" s="T462">orqɨl-pɨ-ntɨ-lä</ta>
            <ta e="T464" id="Seg_5906" s="T463">tɛːmtɨ</ta>
            <ta e="T465" id="Seg_5907" s="T464">čʼam</ta>
            <ta e="T466" id="Seg_5908" s="T465">mišal-pa-tɨ</ta>
            <ta e="T467" id="Seg_5909" s="T466">čʼəktɨ-t</ta>
            <ta e="T468" id="Seg_5910" s="T467">pü-qa</ta>
            <ta e="T469" id="Seg_5911" s="T468">toːnna</ta>
            <ta e="T470" id="Seg_5912" s="T469">seːpe-mmɨ-ntɨ</ta>
            <ta e="T471" id="Seg_5913" s="T470">ija</ta>
            <ta e="T472" id="Seg_5914" s="T471">tɛːmni-m-tɨ</ta>
            <ta e="T473" id="Seg_5915" s="T472">ınna</ta>
            <ta e="T474" id="Seg_5916" s="T473">na</ta>
            <ta e="T475" id="Seg_5917" s="T474">tot-qɨl-pɨ-ntɨ</ta>
            <ta e="T476" id="Seg_5918" s="T475">na-lʼä-t</ta>
            <ta e="T477" id="Seg_5919" s="T476">ɛsɨ-mpa</ta>
            <ta e="T478" id="Seg_5920" s="T477">tap</ta>
            <ta e="T479" id="Seg_5921" s="T478">tɛːmti-lʼ</ta>
            <ta e="T480" id="Seg_5922" s="T479">tomɨ</ta>
            <ta e="T481" id="Seg_5923" s="T480">käm-sa</ta>
            <ta e="T482" id="Seg_5924" s="T481">qajqə</ta>
            <ta e="T483" id="Seg_5925" s="T482">nek-lɔːl-ta-p</ta>
            <ta e="T484" id="Seg_5926" s="T483">na</ta>
            <ta e="T485" id="Seg_5927" s="T484">qum-ɨ-t</ta>
            <ta e="T486" id="Seg_5928" s="T485">ɔːta-m</ta>
            <ta e="T487" id="Seg_5929" s="T486">tɨnä</ta>
            <ta e="T488" id="Seg_5930" s="T487">kıra-mmi-ntɔː-tɨt</ta>
            <ta e="T489" id="Seg_5931" s="T488">qäli-lʼ</ta>
            <ta e="T490" id="Seg_5932" s="T489">qum-ɨ-t</ta>
            <ta e="T491" id="Seg_5933" s="T490">poqonnä</ta>
            <ta e="T492" id="Seg_5934" s="T491">tuš-ti-q-olam-mɨ-ntɔː-tɨt</ta>
            <ta e="T493" id="Seg_5935" s="T492">ija</ta>
            <ta e="T494" id="Seg_5936" s="T493">nä-lʼä-t</ta>
            <ta e="T495" id="Seg_5937" s="T494">na</ta>
            <ta e="T496" id="Seg_5938" s="T495">kur-alti-mmɨ-ntɔː-tɨt</ta>
            <ta e="T497" id="Seg_5939" s="T496">tan</ta>
            <ta e="T498" id="Seg_5940" s="T497">lʼa</ta>
            <ta e="T499" id="Seg_5941" s="T498">meːsʼa</ta>
            <ta e="T500" id="Seg_5942" s="T499">tuš-t-äšik</ta>
            <ta e="T501" id="Seg_5943" s="T500">ija</ta>
            <ta e="T502" id="Seg_5944" s="T501">na-lʼa-t</ta>
            <ta e="T503" id="Seg_5945" s="T502">na</ta>
            <ta e="T504" id="Seg_5946" s="T503">ɛsɨ-mmɨ-ntɨ</ta>
            <ta e="T505" id="Seg_5947" s="T504">mat</ta>
            <ta e="T506" id="Seg_5948" s="T505">tuš-tɨ-qa</ta>
            <ta e="T507" id="Seg_5949" s="T506">aša</ta>
            <ta e="T508" id="Seg_5950" s="T507">tɛnima-k</ta>
            <ta e="T509" id="Seg_5951" s="T508">tına</ta>
            <ta e="T510" id="Seg_5952" s="T509">sər</ta>
            <ta e="T511" id="Seg_5953" s="T510">tətta-lʼ</ta>
            <ta e="T512" id="Seg_5954" s="T511">irra</ta>
            <ta e="T513" id="Seg_5955" s="T512">na-lʼä-t</ta>
            <ta e="T514" id="Seg_5956" s="T513">na</ta>
            <ta e="T515" id="Seg_5957" s="T514">ɛsɨ-mmɨ-ntɨ</ta>
            <ta e="T516" id="Seg_5958" s="T515">tɛː</ta>
            <ta e="T517" id="Seg_5959" s="T516">qajqa</ta>
            <ta e="T518" id="Seg_5960" s="T517">ɔːmtoː-let</ta>
            <ta e="T519" id="Seg_5961" s="T518">wäčʼi-t</ta>
            <ta e="T520" id="Seg_5962" s="T519">mɔːt-tə</ta>
            <ta e="T521" id="Seg_5963" s="T520">tuː-lti-la</ta>
            <ta e="T522" id="Seg_5964" s="T521">muše-ri-ŋɨlɨt</ta>
            <ta e="T523" id="Seg_5965" s="T522">ija</ta>
            <ta e="T524" id="Seg_5966" s="T523">mannɨ-mpa-tɨ</ta>
            <ta e="T525" id="Seg_5967" s="T524">monte</ta>
            <ta e="T526" id="Seg_5968" s="T525">mɨtä</ta>
            <ta e="T527" id="Seg_5969" s="T526">mɔːt-qonä</ta>
            <ta e="T528" id="Seg_5970" s="T527">ponä</ta>
            <ta e="T529" id="Seg_5971" s="T528">ukkur</ta>
            <ta e="T530" id="Seg_5972" s="T529">nätak</ta>
            <ta e="T531" id="Seg_5973" s="T530">na</ta>
            <ta e="T532" id="Seg_5974" s="T531">tannɨ-mmɨ-ntɨ</ta>
            <ta e="T533" id="Seg_5975" s="T532">nätak</ta>
            <ta e="T534" id="Seg_5976" s="T533">tantɨ-lä</ta>
            <ta e="T535" id="Seg_5977" s="T534">wäčʼi-p</ta>
            <ta e="T536" id="Seg_5978" s="T535">maːta-lä</ta>
            <ta e="T537" id="Seg_5979" s="T536">mɔːt-to</ta>
            <ta e="T538" id="Seg_5980" s="T537">nä</ta>
            <ta e="T539" id="Seg_5981" s="T538">tul-ti-mmɨ-ntɨ</ta>
            <ta e="T540" id="Seg_5982" s="T539">aša</ta>
            <ta e="T541" id="Seg_5983" s="T540">kuntɨ</ta>
            <ta e="T542" id="Seg_5984" s="T541">ɔːmtä-la</ta>
            <ta e="T543" id="Seg_5985" s="T542">puːla</ta>
            <ta e="T544" id="Seg_5986" s="T543">ira</ta>
            <ta e="T545" id="Seg_5987" s="T544">ɛj</ta>
            <ta e="T546" id="Seg_5988" s="T545">iːja</ta>
            <ta e="T547" id="Seg_5989" s="T546">mɔːt-ta</ta>
            <ta e="T548" id="Seg_5990" s="T547">nä</ta>
            <ta e="T549" id="Seg_5991" s="T548">šeːr-pi-ntɔː-qıj</ta>
            <ta e="T550" id="Seg_5992" s="T549">ija</ta>
            <ta e="T551" id="Seg_5993" s="T550">mɔːt</ta>
            <ta e="T552" id="Seg_5994" s="T551">šeːr-lʼa</ta>
            <ta e="T553" id="Seg_5995" s="T552">mɔːta-n</ta>
            <ta e="T554" id="Seg_5996" s="T553">ɔːk-tɨ</ta>
            <ta e="T555" id="Seg_5997" s="T554">čʼam</ta>
            <ta e="T556" id="Seg_5998" s="T555">nɨll-ɛːj-o-q-olam-pa</ta>
            <ta e="T557" id="Seg_5999" s="T556">ira</ta>
            <ta e="T558" id="Seg_6000" s="T557">na-lʼä-t</ta>
            <ta e="T559" id="Seg_6001" s="T558">na</ta>
            <ta e="T560" id="Seg_6002" s="T559">ɛsɨ-mmɨ-ntɨ</ta>
            <ta e="T561" id="Seg_6003" s="T560">tɛː</ta>
            <ta e="T562" id="Seg_6004" s="T561">qaj</ta>
            <ta e="T563" id="Seg_6005" s="T562">qum-ɨ-p</ta>
            <ta e="T564" id="Seg_6006" s="T563">aša</ta>
            <ta e="T565" id="Seg_6007" s="T564">qo-ntɔː-lɨt</ta>
            <ta e="T566" id="Seg_6008" s="T565">nʼenna</ta>
            <ta e="T567" id="Seg_6009" s="T566">üːto-qɨntɨtqa</ta>
            <ta e="T568" id="Seg_6010" s="T567">qajqa</ta>
            <ta e="T569" id="Seg_6011" s="T568">mɔːta-n</ta>
            <ta e="T570" id="Seg_6012" s="T569">ɔːk-tɨ</ta>
            <ta e="T571" id="Seg_6013" s="T570">nɨ-lʼti-mpɔː-lɨt</ta>
            <ta e="T572" id="Seg_6014" s="T571">ija-p</ta>
            <ta e="T573" id="Seg_6015" s="T572">nʼenna</ta>
            <ta e="T574" id="Seg_6016" s="T573">iːra-t</ta>
            <ta e="T575" id="Seg_6017" s="T574">qö-nʼte</ta>
            <ta e="T576" id="Seg_6018" s="T575">na</ta>
            <ta e="T577" id="Seg_6019" s="T576">omtɨ-ltä-mmɨ-ntɔː-tɨt</ta>
            <ta e="T578" id="Seg_6020" s="T577">wäčʼi-t-sä</ta>
            <ta e="T579" id="Seg_6021" s="T578">na</ta>
            <ta e="T580" id="Seg_6022" s="T579">tıː-t-alʼ-pi-ntɔː-tɨt</ta>
            <ta e="T581" id="Seg_6023" s="T580">selʼčʼu-qɨlʼ</ta>
            <ta e="T582" id="Seg_6024" s="T581">apsɨ-lʼ</ta>
            <ta e="T583" id="Seg_6025" s="T582">na</ta>
            <ta e="T584" id="Seg_6026" s="T583">tottɨ-mmɨ-ntɔː-tɨt</ta>
            <ta e="T585" id="Seg_6027" s="T584">šitte</ta>
            <ta e="T586" id="Seg_6028" s="T585">qumoː-qıj</ta>
            <ta e="T587" id="Seg_6029" s="T586">pona</ta>
            <ta e="T588" id="Seg_6030" s="T587">tantɨ-la</ta>
            <ta e="T589" id="Seg_6031" s="T588">üt-ɨ-p</ta>
            <ta e="T590" id="Seg_6032" s="T589">mɔːt-tɨ</ta>
            <ta e="T591" id="Seg_6033" s="T590">nä</ta>
            <ta e="T592" id="Seg_6034" s="T591">tül-ti-mmo-ntə-tij</ta>
            <ta e="T593" id="Seg_6035" s="T592">na</ta>
            <ta e="T594" id="Seg_6036" s="T593">üt-i-m</ta>
            <ta e="T595" id="Seg_6037" s="T594">per-ali-ntɔː-tɨt</ta>
            <ta e="T596" id="Seg_6038" s="T595">ɛj</ta>
            <ta e="T597" id="Seg_6039" s="T596">am-ɨ-r-q-ulʼam-mɨ-ntɔː-tɨt</ta>
            <ta e="T598" id="Seg_6040" s="T597">iːja</ta>
            <ta e="T599" id="Seg_6041" s="T598">am-ɨ-r-ä</ta>
            <ta e="T600" id="Seg_6042" s="T599">čʼuntoː-qɨt</ta>
            <ta e="T601" id="Seg_6043" s="T600">nılʼčʼi-ŋä</ta>
            <ta e="T602" id="Seg_6044" s="T601">üntɨ-š-pɨ-ntä-na</ta>
            <ta e="T603" id="Seg_6045" s="T602">montä</ta>
            <ta e="T604" id="Seg_6046" s="T603">mɨta</ta>
            <ta e="T605" id="Seg_6047" s="T604">Lapte-lʼ</ta>
            <ta e="T606" id="Seg_6048" s="T605">nɔːkur</ta>
            <ta e="T607" id="Seg_6049" s="T606">timnʼä-sɨ-t</ta>
            <ta e="T608" id="Seg_6050" s="T607">ima-lʼ-lʼä</ta>
            <ta e="T609" id="Seg_6051" s="T608">täp</ta>
            <ta e="T610" id="Seg_6052" s="T609">taːtɨ-mpɔː-tɨt</ta>
            <ta e="T611" id="Seg_6053" s="T610">sər-tətta-lʼ</ta>
            <ta e="T612" id="Seg_6054" s="T611">timnʼä-sɨ-t-kinte</ta>
            <ta e="T613" id="Seg_6055" s="T612">sər-tətta-lʼ</ta>
            <ta e="T614" id="Seg_6056" s="T613">timnʼä-sɨ-t</ta>
            <ta e="T615" id="Seg_6057" s="T614">ukkur</ta>
            <ta e="T616" id="Seg_6058" s="T615">nʼenʼä-tɨt</ta>
            <ta e="T617" id="Seg_6059" s="T616">ɛː-ppɨ-ntä</ta>
            <ta e="T618" id="Seg_6060" s="T617">namɨ-nɨk</ta>
            <ta e="T619" id="Seg_6061" s="T618">ima-lʼ-lʼi</ta>
            <ta e="T620" id="Seg_6062" s="T619">tü-mpɔː-tɨt</ta>
            <ta e="T621" id="Seg_6063" s="T620">am-ɨ-r-lʼä</ta>
            <ta e="T622" id="Seg_6064" s="T621">soma-k</ta>
            <ta e="T623" id="Seg_6065" s="T622">tor-tɨ-ntɔː-tɨk</ta>
            <ta e="T624" id="Seg_6066" s="T623">nɨːnɨ</ta>
            <ta e="T625" id="Seg_6067" s="T624">šitɨ</ta>
            <ta e="T626" id="Seg_6068" s="T625">na</ta>
            <ta e="T627" id="Seg_6069" s="T626">qən-q-olam-mɨ-ntɔː-tɨt</ta>
            <ta e="T628" id="Seg_6070" s="T627">əːtɨ-t</ta>
            <ta e="T629" id="Seg_6071" s="T628">montɨ</ta>
            <ta e="T630" id="Seg_6072" s="T629">mɨta</ta>
            <ta e="T631" id="Seg_6073" s="T630">nilʼčʼi-k</ta>
            <ta e="T632" id="Seg_6074" s="T631">nʼentɨ</ta>
            <ta e="T633" id="Seg_6075" s="T632">nɨː</ta>
            <ta e="T634" id="Seg_6076" s="T633">pin-tɔː-tɨt</ta>
            <ta e="T635" id="Seg_6077" s="T634">täːlʼi</ta>
            <ta e="T636" id="Seg_6078" s="T635">nɔːtɨ</ta>
            <ta e="T637" id="Seg_6079" s="T636">qum-a-qum-t-i-j</ta>
            <ta e="T638" id="Seg_6080" s="T637">nʼentɨ</ta>
            <ta e="T639" id="Seg_6081" s="T638">na</ta>
            <ta e="T640" id="Seg_6082" s="T639">omtɨ-lt-ɛntɔː-tɨt</ta>
            <ta e="T641" id="Seg_6083" s="T640">qum-ɨ-t-ɨ-t</ta>
            <ta e="T642" id="Seg_6084" s="T641">qən-taː-tqo</ta>
            <ta e="T643" id="Seg_6085" s="T642">ija</ta>
            <ta e="T644" id="Seg_6086" s="T643">ɛj</ta>
            <ta e="T645" id="Seg_6087" s="T644">moqona</ta>
            <ta e="T646" id="Seg_6088" s="T645">na</ta>
            <ta e="T647" id="Seg_6089" s="T646">qən-q-olam-nɨ-tɨ-nɨ</ta>
            <ta e="T648" id="Seg_6090" s="T647">pona</ta>
            <ta e="T649" id="Seg_6091" s="T648">tantɨ-ptäː-qɨn-tɨt</ta>
            <ta e="T650" id="Seg_6092" s="T649">ija</ta>
            <ta e="T651" id="Seg_6093" s="T650">soqo-m-tä</ta>
            <ta e="T652" id="Seg_6094" s="T651">soq-al-ti-ptäː-qɨt</ta>
            <ta e="T653" id="Seg_6095" s="T652">Laptɨ-lʼ</ta>
            <ta e="T654" id="Seg_6096" s="T653">poːsa</ta>
            <ta e="T655" id="Seg_6097" s="T654">warqɨ</ta>
            <ta e="T656" id="Seg_6098" s="T655">timnʼä-t-ɨ-t</ta>
            <ta e="T657" id="Seg_6099" s="T656">nälʼat</ta>
            <ta e="T658" id="Seg_6100" s="T657">na</ta>
            <ta e="T659" id="Seg_6101" s="T658">ɛsɨ-mmɨ-ntɨ</ta>
            <ta e="T660" id="Seg_6102" s="T659">ija-nɨk</ta>
            <ta e="T661" id="Seg_6103" s="T660">soqqa-nte</ta>
            <ta e="T662" id="Seg_6104" s="T661">tasɨt</ta>
            <ta e="T663" id="Seg_6105" s="T662">mitə</ta>
            <ta e="T664" id="Seg_6106" s="T663">tına</ta>
            <ta e="T665" id="Seg_6107" s="T664">opčʼin-ta-s</ta>
            <ta e="T666" id="Seg_6108" s="T665">a</ta>
            <ta e="T667" id="Seg_6109" s="T666">soqqa-n-t</ta>
            <ta e="T668" id="Seg_6110" s="T667">olä</ta>
            <ta e="T669" id="Seg_6111" s="T668">mitɨ</ta>
            <ta e="T670" id="Seg_6112" s="T669">tına</ta>
            <ta e="T671" id="Seg_6113" s="T670">poː-lʼ</ta>
            <ta e="T672" id="Seg_6114" s="T671">kala-n</ta>
            <ta e="T673" id="Seg_6115" s="T672">olɨ</ta>
            <ta e="T674" id="Seg_6116" s="T673">ija</ta>
            <ta e="T675" id="Seg_6117" s="T674">ne</ta>
            <ta e="T676" id="Seg_6118" s="T675">qaj-i-mi</ta>
            <ta e="T677" id="Seg_6119" s="T676">äčʼa</ta>
            <ta e="T678" id="Seg_6120" s="T677">na</ta>
            <ta e="T679" id="Seg_6121" s="T678">kətɨ-mnɨ-tɨ</ta>
            <ta e="T680" id="Seg_6122" s="T679">nɨːnɨ</ta>
            <ta e="T681" id="Seg_6123" s="T680">laqalt-ɛː-mnɨ-t</ta>
            <ta e="T682" id="Seg_6124" s="T681">mɔːt-t</ta>
            <ta e="T683" id="Seg_6125" s="T682">čʼap</ta>
            <ta e="T684" id="Seg_6126" s="T683">tü-lʼčʼa</ta>
            <ta e="T685" id="Seg_6127" s="T684">əmɨ-tɨ</ta>
            <ta e="T686" id="Seg_6128" s="T685">mɔːta-n</ta>
            <ta e="T687" id="Seg_6129" s="T686">ɔː-qɨt</ta>
            <ta e="T688" id="Seg_6130" s="T687">ət-ɨlʼ</ta>
            <ta e="T689" id="Seg_6131" s="T688">tü-ku-lä</ta>
            <ta e="T690" id="Seg_6132" s="T689">nɨŋ-ka</ta>
            <ta e="T691" id="Seg_6133" s="T690">ija-m-tɨ</ta>
            <ta e="T692" id="Seg_6134" s="T691">ɛtɨ-la</ta>
            <ta e="T693" id="Seg_6135" s="T692">ija</ta>
            <ta e="T694" id="Seg_6136" s="T693">tan</ta>
            <ta e="T695" id="Seg_6137" s="T694">kučʼa</ta>
            <ta e="T696" id="Seg_6138" s="T695">namɨšak</ta>
            <ta e="T697" id="Seg_6139" s="T696">qon-ɨ-nʼa-ntɨ</ta>
            <ta e="T698" id="Seg_6140" s="T697">mat</ta>
            <ta e="T699" id="Seg_6141" s="T698">purši-mɔːtʼ-a-noːqa</ta>
            <ta e="T700" id="Seg_6142" s="T699">karrʼa-t</ta>
            <ta e="T701" id="Seg_6143" s="T700">tap</ta>
            <ta e="T702" id="Seg_6144" s="T701">kotpas</ta>
            <ta e="T703" id="Seg_6145" s="T702">pɔːrɨ-qɨt</ta>
            <ta e="T704" id="Seg_6146" s="T703">ɔːmtɨ-k-olʼi-mmɨ-nta-k</ta>
            <ta e="T705" id="Seg_6147" s="T704">na</ta>
            <ta e="T706" id="Seg_6148" s="T705">pi-t</ta>
            <ta e="T707" id="Seg_6149" s="T706">ija</ta>
            <ta e="T708" id="Seg_6150" s="T707">na</ta>
            <ta e="T709" id="Seg_6151" s="T708">šäqɨ-mmɨ-tɨ</ta>
            <ta e="T710" id="Seg_6152" s="T709">tɔːptɨlʼ</ta>
            <ta e="T711" id="Seg_6153" s="T710">čʼeːlɨ</ta>
            <ta e="T712" id="Seg_6154" s="T711">ɔːta-m-tɨ</ta>
            <ta e="T713" id="Seg_6155" s="T712">moqɨna</ta>
            <ta e="T714" id="Seg_6156" s="T713">tɔːqɨ-la</ta>
            <ta e="T715" id="Seg_6157" s="T714">ɔːta-n-tɨ</ta>
            <ta e="T716" id="Seg_6158" s="T715">puːtoː-qɨt</ta>
            <ta e="T717" id="Seg_6159" s="T716">peː-kɨ-la</ta>
            <ta e="T718" id="Seg_6160" s="T717">šitɨ</ta>
            <ta e="T719" id="Seg_6161" s="T718">koška-t</ta>
            <ta e="T720" id="Seg_6162" s="T719">tar-ɨ-lʼ</ta>
            <ta e="T721" id="Seg_6163" s="T720">qoptoː-qı-n</ta>
            <ta e="T722" id="Seg_6164" s="T721">na</ta>
            <ta e="T723" id="Seg_6165" s="T722">orqɨl-pɨ-ntɨ-tɨ</ta>
            <ta e="T724" id="Seg_6166" s="T723">moː-t-ɨ-kɔːlʼ</ta>
            <ta e="T725" id="Seg_6167" s="T724">qalɨ-m-tɨ</ta>
            <ta e="T726" id="Seg_6168" s="T725">na</ta>
            <ta e="T727" id="Seg_6169" s="T726">sɔːr-ɔːl-pɨ-ntɨ-tɨ-nta</ta>
            <ta e="T728" id="Seg_6170" s="T727">kotälʼ</ta>
            <ta e="T729" id="Seg_6171" s="T728">malʼčʼa-m-tɨ</ta>
            <ta e="T730" id="Seg_6172" s="T729">ɛj</ta>
            <ta e="T731" id="Seg_6173" s="T730">soqqɨ-m-tɨ</ta>
            <ta e="T732" id="Seg_6174" s="T731">nä</ta>
            <ta e="T733" id="Seg_6175" s="T732">tok-altɨ-mmɨ-ntɨ-täː-nɨ</ta>
            <ta e="T734" id="Seg_6176" s="T733">nara-po-m-tä</ta>
            <ta e="T735" id="Seg_6177" s="T734">iː-mpa-tä</ta>
            <ta e="T736" id="Seg_6178" s="T735">qaj</ta>
            <ta e="T737" id="Seg_6179" s="T736">aša</ta>
            <ta e="T739" id="Seg_6180" s="T738">nılʼčʼi-k</ta>
            <ta e="T740" id="Seg_6181" s="T739">na</ta>
            <ta e="T741" id="Seg_6182" s="T740">laqalʼt-ɛː-mmi-ntɨ</ta>
            <ta e="T742" id="Seg_6183" s="T741">to-lʼ</ta>
            <ta e="T743" id="Seg_6184" s="T742">qum-ɨ-t-ɨ-n</ta>
            <ta e="T744" id="Seg_6185" s="T743">mɔːt-tə</ta>
            <ta e="T745" id="Seg_6186" s="T744">čʼap</ta>
            <ta e="T746" id="Seg_6187" s="T745">tüː-lʼčʼe</ta>
            <ta e="T747" id="Seg_6188" s="T746">monte</ta>
            <ta e="T748" id="Seg_6189" s="T747">mɨta</ta>
            <ta e="T749" id="Seg_6190" s="T748">Laptä-lʼ</ta>
            <ta e="T750" id="Seg_6191" s="T749">tɨmnʼä-sɨ-t</ta>
            <ta e="T751" id="Seg_6192" s="T750">okoːt</ta>
            <ta e="T752" id="Seg_6193" s="T751">qaj</ta>
            <ta e="T753" id="Seg_6194" s="T752">tüː-ŋɔː-tät</ta>
            <ta e="T754" id="Seg_6195" s="T753">ija</ta>
            <ta e="T755" id="Seg_6196" s="T754">ɔːqalʼ-tɨ</ta>
            <ta e="T756" id="Seg_6197" s="T755">ılla</ta>
            <ta e="T757" id="Seg_6198" s="T756">sɔːrɨ-lʼä</ta>
            <ta e="T758" id="Seg_6199" s="T757">mɔːt-tɨ</ta>
            <ta e="T759" id="Seg_6200" s="T758">na</ta>
            <ta e="T760" id="Seg_6201" s="T759">šeːr-pɨ-tɨ</ta>
            <ta e="T761" id="Seg_6202" s="T760">mɔːt-tɨ</ta>
            <ta e="T762" id="Seg_6203" s="T761">šeːr-lʼa</ta>
            <ta e="T763" id="Seg_6204" s="T762">mɔːt-a-n</ta>
            <ta e="T764" id="Seg_6205" s="T763">ɔːk-tə</ta>
            <ta e="T765" id="Seg_6206" s="T764">čʼap</ta>
            <ta e="T766" id="Seg_6207" s="T765">nɨll-ɛː-je</ta>
            <ta e="T767" id="Seg_6208" s="T766">ira</ta>
            <ta e="T768" id="Seg_6209" s="T767">tälʼ</ta>
            <ta e="T769" id="Seg_6210" s="T768">čʼeːlɨ</ta>
            <ta e="T770" id="Seg_6211" s="T769">montɨ</ta>
            <ta e="T771" id="Seg_6212" s="T770">mɨta</ta>
            <ta e="T772" id="Seg_6213" s="T771">na-lʼä-t</ta>
            <ta e="T773" id="Seg_6214" s="T772">ɛsa</ta>
            <ta e="T774" id="Seg_6215" s="T773">tɛː</ta>
            <ta e="T775" id="Seg_6216" s="T774">qaj</ta>
            <ta e="T776" id="Seg_6217" s="T775">*qoštɨ-lʼ</ta>
            <ta e="T777" id="Seg_6218" s="T776">qum-ɨ-p</ta>
            <ta e="T778" id="Seg_6219" s="T777">aša</ta>
            <ta e="T779" id="Seg_6220" s="T778">qo-ntɔː-lɨt</ta>
            <ta e="T780" id="Seg_6221" s="T779">nänna</ta>
            <ta e="T781" id="Seg_6222" s="T780">üːto-čʼɨntɨtqa</ta>
            <ta e="T782" id="Seg_6223" s="T781">ija</ta>
            <ta e="T783" id="Seg_6224" s="T782">mannɨ-mpa-tɨ</ta>
            <ta e="T784" id="Seg_6225" s="T783">ira-t</ta>
            <ta e="T785" id="Seg_6226" s="T784">qö-ntɨ</ta>
            <ta e="T786" id="Seg_6227" s="T785">koptɨ-p</ta>
            <ta e="T787" id="Seg_6228" s="T786">šitä</ta>
            <ta e="T788" id="Seg_6229" s="T787">na</ta>
            <ta e="T789" id="Seg_6230" s="T788">meː-ntɔː-tɨt</ta>
            <ta e="T790" id="Seg_6231" s="T789">ija</ta>
            <ta e="T791" id="Seg_6232" s="T790">nʼenna</ta>
            <ta e="T792" id="Seg_6233" s="T791">ılla</ta>
            <ta e="T793" id="Seg_6234" s="T792">omta</ta>
            <ta e="T794" id="Seg_6235" s="T793">nılʼčʼi-k</ta>
            <ta e="T795" id="Seg_6236" s="T794">mannɨ-mpa-tɨ</ta>
            <ta e="T796" id="Seg_6237" s="T795">montɨ</ta>
            <ta e="T797" id="Seg_6238" s="T796">mɨta</ta>
            <ta e="T798" id="Seg_6239" s="T797">qum-a-qum-t-i-j</ta>
            <ta e="T799" id="Seg_6240" s="T798">nʼentɨ</ta>
            <ta e="T800" id="Seg_6241" s="T799">om-talʼ</ta>
            <ta e="T801" id="Seg_6242" s="T800">tü-mpɨ-la</ta>
            <ta e="T802" id="Seg_6243" s="T801">ütɨ-mpɔː-tɨt</ta>
            <ta e="T803" id="Seg_6244" s="T802">ija</ta>
            <ta e="T804" id="Seg_6245" s="T803">ɛj</ta>
            <ta e="T805" id="Seg_6246" s="T804">na</ta>
            <ta e="T806" id="Seg_6247" s="T805">qom-i-nti-sa</ta>
            <ta e="T807" id="Seg_6248" s="T806">təm</ta>
            <ta e="T808" id="Seg_6249" s="T807">ɛj</ta>
            <ta e="T809" id="Seg_6250" s="T808">na</ta>
            <ta e="T810" id="Seg_6251" s="T809">ütɨ-mpe-r-alʼ-mɨ-tɨ</ta>
            <ta e="T811" id="Seg_6252" s="T810">montɨ</ta>
            <ta e="T812" id="Seg_6253" s="T811">mɨta</ta>
            <ta e="T813" id="Seg_6254" s="T812">ija-p</ta>
            <ta e="T814" id="Seg_6255" s="T813">ün</ta>
            <ta e="T815" id="Seg_6256" s="T814">na</ta>
            <ta e="T816" id="Seg_6257" s="T815">šeːr-tä-tä</ta>
            <ta e="T817" id="Seg_6258" s="T816">ü</ta>
            <ta e="T818" id="Seg_6259" s="T817">šeːr-la</ta>
            <ta e="T819" id="Seg_6260" s="T818">ija</ta>
            <ta e="T820" id="Seg_6261" s="T819">alʼčʼa</ta>
            <ta e="T821" id="Seg_6262" s="T820">ü</ta>
            <ta e="T822" id="Seg_6263" s="T821">sʼeːr-pɨlʼ</ta>
            <ta e="T823" id="Seg_6264" s="T822">qup</ta>
            <ta e="T824" id="Seg_6265" s="T823">qaj</ta>
            <ta e="T825" id="Seg_6266" s="T824">qaj</ta>
            <ta e="T826" id="Seg_6267" s="T825">tɛnimɨ-ntä</ta>
            <ta e="T827" id="Seg_6268" s="T826">kušaŋ</ta>
            <ta e="T828" id="Seg_6269" s="T827">aša</ta>
            <ta e="T829" id="Seg_6270" s="T828">na</ta>
            <ta e="T830" id="Seg_6271" s="T829">ippɨ-nmɨ-ntɨ</ta>
            <ta e="T831" id="Seg_6272" s="T830">ukkur</ta>
            <ta e="T832" id="Seg_6273" s="T831">tət</ta>
            <ta e="T833" id="Seg_6274" s="T832">čʼontoː-qɨt</ta>
            <ta e="T834" id="Seg_6275" s="T833">iːja</ta>
            <ta e="T835" id="Seg_6276" s="T834">ınna</ta>
            <ta e="T836" id="Seg_6277" s="T835">čʼap</ta>
            <ta e="T837" id="Seg_6278" s="T836">qə-ŋa</ta>
            <ta e="T838" id="Seg_6279" s="T837">montɨ</ta>
            <ta e="T839" id="Seg_6280" s="T838">mɨta</ta>
            <ta e="T840" id="Seg_6281" s="T839">qup</ta>
            <ta e="T841" id="Seg_6282" s="T840">šıp</ta>
            <ta e="T842" id="Seg_6283" s="T841">qəttɨ-rorr-ij</ta>
            <ta e="T843" id="Seg_6284" s="T842">nʼoː-tɨ-š</ta>
            <ta e="T844" id="Seg_6285" s="T843">nılʼčʼi-k</ta>
            <ta e="T845" id="Seg_6286" s="T844">ippɨ-lʼa</ta>
            <ta e="T846" id="Seg_6287" s="T845">üŋkul-ti-mpa-tɨ</ta>
            <ta e="T847" id="Seg_6288" s="T846">qäː-lʼ</ta>
            <ta e="T848" id="Seg_6289" s="T847">täːttoː-qɨt</ta>
            <ta e="T849" id="Seg_6290" s="T848">tına</ta>
            <ta e="T850" id="Seg_6291" s="T849">sər-təta-tt-ɨ-t</ta>
            <ta e="T851" id="Seg_6292" s="T850">nʼenʼnʼa</ta>
            <ta e="T852" id="Seg_6293" s="T851">laŋku-š-kuna</ta>
            <ta e="T853" id="Seg_6294" s="T852">tɛː</ta>
            <ta e="T854" id="Seg_6295" s="T853">lʼa</ta>
            <ta e="T855" id="Seg_6296" s="T854">mompa</ta>
            <ta e="T856" id="Seg_6297" s="T855">mɨta</ta>
            <ta e="T857" id="Seg_6298" s="T856">qaj</ta>
            <ta e="T858" id="Seg_6299" s="T857">man-pɨ-mmɨ-nta-lit</ta>
            <ta e="T859" id="Seg_6300" s="T858">tına</ta>
            <ta e="T860" id="Seg_6301" s="T859">šentɨ-k</ta>
            <ta e="T861" id="Seg_6302" s="T860">tü-mpɨlʼ</ta>
            <ta e="T862" id="Seg_6303" s="T861">iːja-p</ta>
            <ta e="T863" id="Seg_6304" s="T862">na</ta>
            <ta e="T864" id="Seg_6305" s="T863">qät-tɔː-tɨt</ta>
            <ta e="T865" id="Seg_6306" s="T864">Loptɨ-lʼ</ta>
            <ta e="T866" id="Seg_6307" s="T865">tɨmnʼä-sɨ-t</ta>
            <ta e="T867" id="Seg_6308" s="T866">ija</ta>
            <ta e="T868" id="Seg_6309" s="T867">asʼa</ta>
            <ta e="T869" id="Seg_6310" s="T868">qatta</ta>
            <ta e="T870" id="Seg_6311" s="T869">ippɨ-ptäː</ta>
            <ta e="T871" id="Seg_6312" s="T870">sä-qɨt</ta>
            <ta e="T872" id="Seg_6313" s="T871">iː-qɨn-tä</ta>
            <ta e="T873" id="Seg_6314" s="T872">qəttɨ-la</ta>
            <ta e="T874" id="Seg_6315" s="T873">ɔːlʼčʼi-mpɨlʼ</ta>
            <ta e="T875" id="Seg_6316" s="T874">qum-i-m-tɨ</ta>
            <ta e="T876" id="Seg_6317" s="T875">səntä-tɨ-sä</ta>
            <ta e="T877" id="Seg_6318" s="T876">šitä</ta>
            <ta e="T878" id="Seg_6319" s="T877">na</ta>
            <ta e="T879" id="Seg_6320" s="T878">qättɨ-mmɨ-ntä</ta>
            <ta e="T880" id="Seg_6321" s="T879">nɨːnä</ta>
            <ta e="T881" id="Seg_6322" s="T880">šitɨ-mtalʼi</ta>
            <ta e="T882" id="Seg_6323" s="T881">šitɨ</ta>
            <ta e="T883" id="Seg_6324" s="T882">čʼa-mma-ntə</ta>
            <ta e="T884" id="Seg_6325" s="T883">montɨ</ta>
            <ta e="T885" id="Seg_6326" s="T884">mɨta</ta>
            <ta e="T886" id="Seg_6327" s="T885">qäli-lʼ</ta>
            <ta e="T887" id="Seg_6328" s="T886">tɨmnʼä-sɨ-t</ta>
            <ta e="T888" id="Seg_6329" s="T887">Loptɨ-lʼ</ta>
            <ta e="T889" id="Seg_6330" s="T888">nılʼčʼi-k</ta>
            <ta e="T890" id="Seg_6331" s="T889">qu-mpɔː-tɨt</ta>
            <ta e="T891" id="Seg_6332" s="T890">ɔːmɨ-nʼeː-t-ɨ-t</ta>
            <ta e="T892" id="Seg_6333" s="T891">čʼontoː-mɨt</ta>
            <ta e="T893" id="Seg_6334" s="T892">šitə</ta>
            <ta e="T894" id="Seg_6335" s="T893">säpä-ıː-mpa</ta>
            <ta e="T895" id="Seg_6336" s="T894">ɔːmɨ-nʼeː-t-ɨ-t</ta>
            <ta e="T896" id="Seg_6337" s="T895">kɨ-m</ta>
            <ta e="T897" id="Seg_6338" s="T896">šitə</ta>
            <ta e="T898" id="Seg_6339" s="T897">pasə-ıː-mpa</ta>
            <ta e="T899" id="Seg_6340" s="T898">a</ta>
            <ta e="T900" id="Seg_6341" s="T899">poːsɨ</ta>
            <ta e="T901" id="Seg_6342" s="T900">kɨpa</ta>
            <ta e="T902" id="Seg_6343" s="T901">qəːt-sɨm-ɨ-lʼ</ta>
            <ta e="T903" id="Seg_6344" s="T902">timnʼa-t-ɨ-t</ta>
            <ta e="T904" id="Seg_6345" s="T903">muntɨ</ta>
            <ta e="T905" id="Seg_6346" s="T904">mɨta</ta>
            <ta e="T906" id="Seg_6347" s="T905">olo-tɨ</ta>
            <ta e="T907" id="Seg_6348" s="T906">meːl</ta>
            <ta e="T908" id="Seg_6349" s="T907">čʼäː</ta>
            <ta e="T909" id="Seg_6350" s="T908">a</ta>
            <ta e="T910" id="Seg_6351" s="T909">na</ta>
            <ta e="T911" id="Seg_6352" s="T910">moː-t-ɨ-kɔːl</ta>
            <ta e="T912" id="Seg_6353" s="T911">mɔːt</ta>
            <ta e="T913" id="Seg_6354" s="T912">čʼaŋalʼ</ta>
            <ta e="T914" id="Seg_6355" s="T913">moː-t-ɨ-kɔːl</ta>
            <ta e="T915" id="Seg_6356" s="T914">tɔːpɨ</ta>
            <ta e="T916" id="Seg_6357" s="T915">qali-mmɨ-nta</ta>
            <ta e="T917" id="Seg_6358" s="T916">mɔːt-ɨ-kota</ta>
            <ta e="T918" id="Seg_6359" s="T917">ɛj</ta>
            <ta e="T919" id="Seg_6360" s="T918">mɔːt-ɨ-qopɨ</ta>
            <ta e="T920" id="Seg_6361" s="T919">täːlʼ-šɨlʼ</ta>
            <ta e="T921" id="Seg_6362" s="T920">tät</ta>
            <ta e="T922" id="Seg_6363" s="T921">toːt</ta>
            <ta e="T923" id="Seg_6364" s="T922">älpä-t</ta>
            <ta e="T924" id="Seg_6365" s="T923">ippɔː-tɨt</ta>
            <ta e="T925" id="Seg_6366" s="T924">i̇nnä</ta>
            <ta e="T926" id="Seg_6367" s="T925">weši-la</ta>
            <ta e="T927" id="Seg_6368" s="T926">nılʼčʼi-k</ta>
            <ta e="T928" id="Seg_6369" s="T927">šitɨ</ta>
            <ta e="T929" id="Seg_6370" s="T928">mannɨ-mpa-tɨ</ta>
            <ta e="T930" id="Seg_6371" s="T929">qum-ɨ-t</ta>
            <ta e="T931" id="Seg_6372" s="T930">taqqe-mpɨ-la</ta>
            <ta e="T932" id="Seg_6373" s="T931">täp-ɨ-p</ta>
            <ta e="T933" id="Seg_6374" s="T932">mannɨ-mpɔː-tɨt</ta>
            <ta e="T934" id="Seg_6375" s="T933">a</ta>
            <ta e="T935" id="Seg_6376" s="T934">tına</ta>
            <ta e="T936" id="Seg_6377" s="T935">nätak</ta>
            <ta e="T937" id="Seg_6378" s="T936">meːl</ta>
            <ta e="T938" id="Seg_6379" s="T937">čʼäːŋka</ta>
            <ta e="T939" id="Seg_6380" s="T938">toː</ta>
            <ta e="T940" id="Seg_6381" s="T939">qəl-la</ta>
            <ta e="T941" id="Seg_6382" s="T940">mɔːt-ɨ-qopɨ-p</ta>
            <ta e="T942" id="Seg_6383" s="T941">ınna</ta>
            <ta e="T943" id="Seg_6384" s="T942">šitɨ</ta>
            <ta e="T944" id="Seg_6385" s="T943">čʼap</ta>
            <ta e="T945" id="Seg_6386" s="T944">nü-ŋɨ-tɨ</ta>
            <ta e="T946" id="Seg_6387" s="T945">montɨ</ta>
            <ta e="T947" id="Seg_6388" s="T946">nättak</ta>
            <ta e="T948" id="Seg_6389" s="T947">nɨːnɨ</ta>
            <ta e="T949" id="Seg_6390" s="T948">na</ta>
            <ta e="T950" id="Seg_6391" s="T949">putil-mɔːn-na</ta>
            <ta e="T951" id="Seg_6392" s="T950">aša</ta>
            <ta e="T952" id="Seg_6393" s="T951">qatɔː-tɨt</ta>
            <ta e="T953" id="Seg_6394" s="T952">na</ta>
            <ta e="T954" id="Seg_6395" s="T953">nɨmtɨ-lʼ</ta>
            <ta e="T955" id="Seg_6396" s="T954">qum-ɨ-t</ta>
            <ta e="T956" id="Seg_6397" s="T955">wänɨlʼ</ta>
            <ta e="T957" id="Seg_6398" s="T956">mɔːt-ɨ-p</ta>
            <ta e="T958" id="Seg_6399" s="T957">čʼəsɨ-la</ta>
            <ta e="T959" id="Seg_6400" s="T958">nɔːtɨ</ta>
            <ta e="T960" id="Seg_6401" s="T959">na</ta>
            <ta e="T961" id="Seg_6402" s="T960">üt-ɛː-r-pi-ntɔː-tɨt</ta>
            <ta e="T962" id="Seg_6403" s="T961">ɛj</ta>
            <ta e="T963" id="Seg_6404" s="T962">am-ɨ-r-pɔː-tɨt</ta>
            <ta e="T964" id="Seg_6405" s="T963">sär-tätta</ta>
            <ta e="T965" id="Seg_6406" s="T964">ira</ta>
            <ta e="T966" id="Seg_6407" s="T965">nʼenʼä-m-tɨ</ta>
            <ta e="T967" id="Seg_6408" s="T966">na</ta>
            <ta e="T968" id="Seg_6409" s="T967">ija-nɨk</ta>
            <ta e="T969" id="Seg_6410" s="T968">na</ta>
            <ta e="T970" id="Seg_6411" s="T969">kur-alʼtɨ-mmɨ-ntɨ-t</ta>
            <ta e="T971" id="Seg_6412" s="T970">ilʼmatɨlʼ</ta>
            <ta e="T972" id="Seg_6413" s="T971">qum-oː-qɨp</ta>
            <ta e="T973" id="Seg_6414" s="T972">nʼentɨ</ta>
            <ta e="T974" id="Seg_6415" s="T973">om-pɨ-ltä-ntɨ-la</ta>
            <ta e="T975" id="Seg_6416" s="T974">tɛːttɨ-kolʼ</ta>
            <ta e="T976" id="Seg_6417" s="T975">köt</ta>
            <ta e="T977" id="Seg_6418" s="T976">čʼeːla</ta>
            <ta e="T978" id="Seg_6419" s="T977">ütɨ-mɨ-mɨ-mpɔː-tɨt</ta>
            <ta e="T979" id="Seg_6420" s="T978">qum-a-qup</ta>
            <ta e="T980" id="Seg_6421" s="T979">nʼentɨ</ta>
            <ta e="T981" id="Seg_6422" s="T980">om-tɨ-l-pɨ-la</ta>
            <ta e="T982" id="Seg_6423" s="T981">sər-tətta-lʼ</ta>
            <ta e="T983" id="Seg_6424" s="T982">timnʼä-sɨ-t</ta>
            <ta e="T984" id="Seg_6425" s="T983">na</ta>
            <ta e="T985" id="Seg_6426" s="T984">ija-sa</ta>
            <ta e="T986" id="Seg_6427" s="T985">tɔː</ta>
            <ta e="T987" id="Seg_6428" s="T986">qär-tɔː-tɨt</ta>
            <ta e="T988" id="Seg_6429" s="T987">ija-t</ta>
            <ta e="T989" id="Seg_6430" s="T988">mɔːt-tɨ</ta>
            <ta e="T990" id="Seg_6431" s="T989">qanɨk-tä</ta>
            <ta e="T991" id="Seg_6432" s="T990">nʼentɨ</ta>
            <ta e="T992" id="Seg_6433" s="T991">palla</ta>
            <ta e="T993" id="Seg_6434" s="T992">na</ta>
            <ta e="T994" id="Seg_6435" s="T993">ili-mmɨ-ntɔː-tɨt</ta>
            <ta e="T995" id="Seg_6436" s="T994">i</ta>
            <ta e="T996" id="Seg_6437" s="T995">tiːtä</ta>
            <ta e="T997" id="Seg_6438" s="T996">qos</ta>
            <ta e="T998" id="Seg_6439" s="T997">to</ta>
            <ta e="T999" id="Seg_6440" s="T998">ili-mtɔː-tɨt</ta>
            <ta e="T1000" id="Seg_6441" s="T999">čʼäpta</ta>
            <ta e="T1001" id="Seg_6442" s="T1000">nʼeː-mit</ta>
            <ta e="T1002" id="Seg_6443" s="T1001">toː</ta>
            <ta e="T1003" id="Seg_6444" s="T1002">na</ta>
            <ta e="T1004" id="Seg_6445" s="T1003">seːpʼe</ta>
            <ta e="T1005" id="Seg_6446" s="T1004">ɛː-mmɨ-ntɨ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T83" id="Seg_6447" s="T82">Lapta-lʼ</ta>
            <ta e="T84" id="Seg_6448" s="T83">nɔːkɨr</ta>
            <ta e="T85" id="Seg_6449" s="T84">timnʼa-sɨ-tɨ</ta>
            <ta e="T86" id="Seg_6450" s="T85">šittɨ</ta>
            <ta e="T87" id="Seg_6451" s="T86">ama-sɨ-qı</ta>
            <ta e="T88" id="Seg_6452" s="T87">ilɨ-mpɨ-ntɨ</ta>
            <ta e="T89" id="Seg_6453" s="T88">iːja-tɨ</ta>
            <ta e="T90" id="Seg_6454" s="T89">nɔːtɨ</ta>
            <ta e="T91" id="Seg_6455" s="T90">qum-ntɨ-ššak</ta>
            <ta e="T92" id="Seg_6456" s="T91">orɨ-m-mpɨ</ta>
            <ta e="T93" id="Seg_6457" s="T92">iːja-tɨ</ta>
            <ta e="T94" id="Seg_6458" s="T93">na-m</ta>
            <ta e="T95" id="Seg_6459" s="T94">ašša</ta>
            <ta e="T96" id="Seg_6460" s="T95">tɛnɨmɨ-mpɨ-tɨ</ta>
            <ta e="T97" id="Seg_6461" s="T96">əsɨ-tɨ</ta>
            <ta e="T98" id="Seg_6462" s="T97">kuššat</ta>
            <ta e="T99" id="Seg_6463" s="T98">qu-mpɨ</ta>
            <ta e="T100" id="Seg_6464" s="T99">ukkɨr</ta>
            <ta e="T101" id="Seg_6465" s="T100">tamčʼeːlɨ</ta>
            <ta e="T102" id="Seg_6466" s="T101">iːja</ta>
            <ta e="T103" id="Seg_6467" s="T102">ama-ntɨ-nɨŋ</ta>
            <ta e="T104" id="Seg_6468" s="T103">na-lʼa-t</ta>
            <ta e="T105" id="Seg_6469" s="T104">ɛsɨ</ta>
            <ta e="T106" id="Seg_6470" s="T105">ama</ta>
            <ta e="T107" id="Seg_6471" s="T106">man</ta>
            <ta e="T108" id="Seg_6472" s="T107">əsɨ-nɨ</ta>
            <ta e="T109" id="Seg_6473" s="T108">*kurɨ-mpɨlʼ</ta>
            <ta e="T110" id="Seg_6474" s="T109">ɔːtä-tɨ</ta>
            <ta e="T111" id="Seg_6475" s="T110">kunɨ</ta>
            <ta e="T112" id="Seg_6476" s="T111">mäkkä</ta>
            <ta e="T113" id="Seg_6477" s="T112">atɨ-ltɨ-ätɨ</ta>
            <ta e="T114" id="Seg_6478" s="T113">a</ta>
            <ta e="T115" id="Seg_6479" s="T114">ɔːtä-tɨ-lʼ</ta>
            <ta e="T116" id="Seg_6480" s="T115">ama-sɨ-qı</ta>
            <ta e="T117" id="Seg_6481" s="T116">*nɔː</ta>
            <ta e="T118" id="Seg_6482" s="T117">nannɛr</ta>
            <ta e="T119" id="Seg_6483" s="T118">ɛː-mpɨ</ta>
            <ta e="T120" id="Seg_6484" s="T119">təttɨ-ntɨ-sä</ta>
            <ta e="T121" id="Seg_6485" s="T120">am-mpɨ-tɨ</ta>
            <ta e="T122" id="Seg_6486" s="T121">a</ta>
            <ta e="T123" id="Seg_6487" s="T122">ɔːtä-ntɨ-lʼ</ta>
            <ta e="T124" id="Seg_6488" s="T123">nılʼčʼɨ-lʼ</ta>
            <ta e="T125" id="Seg_6489" s="T124">taqqaš-ntɨ-lʼ</ta>
            <ta e="T126" id="Seg_6490" s="T125">ɛː-mpɨ-ntɨ</ta>
            <ta e="T127" id="Seg_6491" s="T126">nʼuːqɨ-tɨ</ta>
            <ta e="T128" id="Seg_6492" s="T127">qəːlɨ-n</ta>
            <ta e="T129" id="Seg_6493" s="T128">qopɨ-tɨ</ta>
            <ta e="T130" id="Seg_6494" s="T129">kuras-tɨ</ta>
            <ta e="T131" id="Seg_6495" s="T130">ɛː-mpɨ</ta>
            <ta e="T132" id="Seg_6496" s="T131">na-m-tɨ</ta>
            <ta e="T133" id="Seg_6497" s="T132">*karnʼɨ-ätɔːl-tɨ-lä</ta>
            <ta e="T134" id="Seg_6498" s="T133">ɔːtä-m-ti-m</ta>
            <ta e="T135" id="Seg_6499" s="T134">nılʼčʼɨ-k</ta>
            <ta e="T136" id="Seg_6500" s="T135">moqɨnä</ta>
            <ta e="T137" id="Seg_6501" s="T136">tɔːqqɨ-qo-olam-ɨ-mpɨ-tɨt</ta>
            <ta e="T138" id="Seg_6502" s="T137">ama-tɨ</ta>
            <ta e="T139" id="Seg_6503" s="T138">na-lʼa-t</ta>
            <ta e="T140" id="Seg_6504" s="T139">ɛsɨ</ta>
            <ta e="T141" id="Seg_6505" s="T140">tan</ta>
            <ta e="T142" id="Seg_6506" s="T141">ɔːtä-sä</ta>
            <ta e="T143" id="Seg_6507" s="T142">kučʼčʼä</ta>
            <ta e="T144" id="Seg_6508" s="T143">qən-ŋɨ-ntɨ</ta>
            <ta e="T145" id="Seg_6509" s="T144">iːja</ta>
            <ta e="T146" id="Seg_6510" s="T145">na-lʼa-t</ta>
            <ta e="T147" id="Seg_6511" s="T146">ɛsɨ</ta>
            <ta e="T148" id="Seg_6512" s="T147">man</ta>
            <ta e="T149" id="Seg_6513" s="T148">nʼi</ta>
            <ta e="T150" id="Seg_6514" s="T149">kučʼčʼä</ta>
            <ta e="T151" id="Seg_6515" s="T150">ašša</ta>
            <ta e="T152" id="Seg_6516" s="T151">qən-ɛntɨ-k</ta>
            <ta e="T153" id="Seg_6517" s="T152">man</ta>
            <ta e="T154" id="Seg_6518" s="T153">tam</ta>
            <ta e="T155" id="Seg_6519" s="T154">puršɨ-mɔːt-ptäː-noːqo</ta>
            <ta e="T156" id="Seg_6520" s="T155">karrä-n</ta>
            <ta e="T157" id="Seg_6521" s="T156">nʼarɨ-n</ta>
            <ta e="T158" id="Seg_6522" s="T157">qo-ltɨ-ɛntɨ-k</ta>
            <ta e="T159" id="Seg_6523" s="T158">ama-tɨ</ta>
            <ta e="T160" id="Seg_6524" s="T159">ašša</ta>
            <ta e="T161" id="Seg_6525" s="T160">qattɨ</ta>
            <ta e="T162" id="Seg_6526" s="T161">qaqlɨ-mɨn</ta>
            <ta e="T163" id="Seg_6527" s="T162">puːtɨ-n</ta>
            <ta e="T164" id="Seg_6528" s="T163">*tar-š-äl-tɨ-lä</ta>
            <ta e="T165" id="Seg_6529" s="T164">na-m-tɨ</ta>
            <ta e="T166" id="Seg_6530" s="T165">čʼarɨm-lʼ</ta>
            <ta e="T167" id="Seg_6531" s="T166">ɔːtä-lʼ</ta>
            <ta e="T168" id="Seg_6532" s="T167">orɨ-sä-lʼ</ta>
            <ta e="T169" id="Seg_6533" s="T168">tɛːmnɨ</ta>
            <ta e="T170" id="Seg_6534" s="T169">ıllä</ta>
            <ta e="T171" id="Seg_6535" s="T170">iː-ntɨ-tɨ</ta>
            <ta e="T172" id="Seg_6536" s="T171">montɨ</ta>
            <ta e="T173" id="Seg_6537" s="T172">mɨta</ta>
            <ta e="T174" id="Seg_6538" s="T173">poː-lʼ</ta>
            <ta e="T175" id="Seg_6539" s="T174">kala-n</ta>
            <ta e="T176" id="Seg_6540" s="T175">paqqɨ-n</ta>
            <ta e="T177" id="Seg_6541" s="T176">čʼarɨm</ta>
            <ta e="T178" id="Seg_6542" s="T177">ɛː-ŋɨ</ta>
            <ta e="T179" id="Seg_6543" s="T178">iːja-nɨŋ</ta>
            <ta e="T180" id="Seg_6544" s="T179">nılʼčʼɨ-k</ta>
            <ta e="T181" id="Seg_6545" s="T180">pɨŋtaltɨ-mpɨ-ntɨ</ta>
            <ta e="T182" id="Seg_6546" s="T181">karrä</ta>
            <ta e="T183" id="Seg_6547" s="T182">tam</ta>
            <ta e="T184" id="Seg_6548" s="T183">ɔːtä-n-ntɨ</ta>
            <ta e="T185" id="Seg_6549" s="T184">puːtɨ-qɨn</ta>
            <ta e="T186" id="Seg_6550" s="T185">nɔːkɨr</ta>
            <ta e="T187" id="Seg_6551" s="T186">*pekɨra-lʼ</ta>
            <ta e="T188" id="Seg_6552" s="T187">qoptɨ</ta>
            <ta e="T189" id="Seg_6553" s="T188">na</ta>
            <ta e="T190" id="Seg_6554" s="T189">ɛː-ntɨ</ta>
            <ta e="T191" id="Seg_6555" s="T190">ukkɨr</ta>
            <ta e="T192" id="Seg_6556" s="T191">*pekɨra-lʼ</ta>
            <ta e="T193" id="Seg_6557" s="T192">qoptɨ</ta>
            <ta e="T194" id="Seg_6558" s="T193">na</ta>
            <ta e="T195" id="Seg_6559" s="T194">ɛː-ntɨ</ta>
            <ta e="T196" id="Seg_6560" s="T195">wərqɨ-lɔːqɨ-lʼ</ta>
            <ta e="T197" id="Seg_6561" s="T196">na-m</ta>
            <ta e="T198" id="Seg_6562" s="T197">ora-qɨl-ätɨ</ta>
            <ta e="T199" id="Seg_6563" s="T198">toːnna</ta>
            <ta e="T200" id="Seg_6564" s="T199">mɨ-qı</ta>
            <ta e="T201" id="Seg_6565" s="T200">ontıː</ta>
            <ta e="T202" id="Seg_6566" s="T201">əːtɨ-lä</ta>
            <ta e="T203" id="Seg_6567" s="T202">na</ta>
            <ta e="T204" id="Seg_6568" s="T203">tü-ntɨ-ɛntɨ-qı</ta>
            <ta e="T205" id="Seg_6569" s="T204">iːja</ta>
            <ta e="T206" id="Seg_6570" s="T205">tɛːmnɨ-m-tɨ</ta>
            <ta e="T207" id="Seg_6571" s="T206">kəš</ta>
            <ta e="T208" id="Seg_6572" s="T207">mɔːntɨ</ta>
            <ta e="T209" id="Seg_6573" s="T208">na</ta>
            <ta e="T210" id="Seg_6574" s="T209">tottɨ-qɨl-mpɨ-ntɨ-tɨ</ta>
            <ta e="T211" id="Seg_6575" s="T210">nılʼčʼɨ-k</ta>
            <ta e="T212" id="Seg_6576" s="T211">karrä</ta>
            <ta e="T213" id="Seg_6577" s="T212">na</ta>
            <ta e="T214" id="Seg_6578" s="T213">qən-mpɨ-ntɨ</ta>
            <ta e="T215" id="Seg_6579" s="T214">ɔːtä-n-tɨ</ta>
            <ta e="T216" id="Seg_6580" s="T215">puːtɨ-n</ta>
            <ta e="T217" id="Seg_6581" s="T216">peː-kkɨ-lä</ta>
            <ta e="T218" id="Seg_6582" s="T217">tına</ta>
            <ta e="T219" id="Seg_6583" s="T218">ama-n-tɨ</ta>
            <ta e="T220" id="Seg_6584" s="T219">tom-mpɨlʼ</ta>
            <ta e="T221" id="Seg_6585" s="T220">qoptɨ-m</ta>
            <ta e="T222" id="Seg_6586" s="T221">na</ta>
            <ta e="T223" id="Seg_6587" s="T222">orqɨl-mpɨ-ntɨ-tɨ</ta>
            <ta e="T224" id="Seg_6588" s="T223">konnä</ta>
            <ta e="T225" id="Seg_6589" s="T224">taːtɨ-ŋɨ-tɨ</ta>
            <ta e="T226" id="Seg_6590" s="T225">sɔːrɨ-ätɔːl-qo</ta>
            <ta e="T227" id="Seg_6591" s="T226">na-n</ta>
            <ta e="T228" id="Seg_6592" s="T227">kuntɨ</ta>
            <ta e="T229" id="Seg_6593" s="T228">ama-tɨ</ta>
            <ta e="T230" id="Seg_6594" s="T229">montɨ</ta>
            <ta e="T231" id="Seg_6595" s="T230">mɨta</ta>
            <ta e="T232" id="Seg_6596" s="T231">qaqlɨ-t-ɨ-n</ta>
            <ta e="T233" id="Seg_6597" s="T232">puːtɨ-qɨn</ta>
            <ta e="T234" id="Seg_6598" s="T233">nılʼčʼɨ</ta>
            <ta e="T235" id="Seg_6599" s="T234">qaqlɨ</ta>
            <ta e="T236" id="Seg_6600" s="T235">ıllä</ta>
            <ta e="T237" id="Seg_6601" s="T236">iː-mpɨ-ntɨ-tɨ</ta>
            <ta e="T238" id="Seg_6602" s="T237">montɨ</ta>
            <ta e="T239" id="Seg_6603" s="T238">mɨta</ta>
            <ta e="T240" id="Seg_6604" s="T239">nɔːkɨr-sar-ɨ-lʼ</ta>
            <ta e="T241" id="Seg_6605" s="T240">košar</ta>
            <ta e="T242" id="Seg_6606" s="T241">ɔːmtɨ-lʼ</ta>
            <ta e="T243" id="Seg_6607" s="T242">topɨ-tɨ</ta>
            <ta e="T244" id="Seg_6608" s="T243">iːja</ta>
            <ta e="T245" id="Seg_6609" s="T244">qaqlɨ-m-tɨ</ta>
            <ta e="T246" id="Seg_6610" s="T245">na</ta>
            <ta e="T247" id="Seg_6611" s="T246">*sar-ätɔːl-qo-olam-mpɨ-ntɨ</ta>
            <ta e="T248" id="Seg_6612" s="T247">montɨ</ta>
            <ta e="T249" id="Seg_6613" s="T248">mɨta</ta>
            <ta e="T250" id="Seg_6614" s="T249">tına</ta>
            <ta e="T251" id="Seg_6615" s="T250">šittɨ</ta>
            <ta e="T252" id="Seg_6616" s="T251">*pekɨra-lʼ</ta>
            <ta e="T253" id="Seg_6617" s="T252">kɨpa-lɔːqɨ-lʼ</ta>
            <ta e="T254" id="Seg_6618" s="T253">qoptɨ-qı-n</ta>
            <ta e="T255" id="Seg_6619" s="T254">ontıː</ta>
            <ta e="T256" id="Seg_6620" s="T255">əːtɨ-lä</ta>
            <ta e="T257" id="Seg_6621" s="T256">montɨ</ta>
            <ta e="T258" id="Seg_6622" s="T257">mɨta</ta>
            <ta e="T259" id="Seg_6623" s="T258">konnä</ta>
            <ta e="T260" id="Seg_6624" s="T259">na</ta>
            <ta e="T261" id="Seg_6625" s="T260">tü-mpɨ-ntɨ</ta>
            <ta e="T262" id="Seg_6626" s="T261">iːja</ta>
            <ta e="T263" id="Seg_6627" s="T262">qaqlɨ-m-tɨ</ta>
            <ta e="T264" id="Seg_6628" s="T263">sɔːrɨ-lä</ta>
            <ta e="T265" id="Seg_6629" s="T264">na</ta>
            <ta e="T266" id="Seg_6630" s="T265">tuːrıː-tɨ-mpɨ-ntɨ-tɨ</ta>
            <ta e="T267" id="Seg_6631" s="T266">ama-tɨ</ta>
            <ta e="T268" id="Seg_6632" s="T267">seːlʼčʼɨ</ta>
            <ta e="T269" id="Seg_6633" s="T268">kočʼɨnʼ-ɨ-lʼ</ta>
            <ta e="T270" id="Seg_6634" s="T269">košar</ta>
            <ta e="T271" id="Seg_6635" s="T270">ɔːmtɨ-lʼ</ta>
            <ta e="T272" id="Seg_6636" s="T271">narɨ-poː-sä</ta>
            <ta e="T273" id="Seg_6637" s="T272">na</ta>
            <ta e="T274" id="Seg_6638" s="T273">mi-tɨ-mpɨ-ntɨ-tɨ</ta>
            <ta e="T275" id="Seg_6639" s="T274">*pekɨra-lʼ</ta>
            <ta e="T276" id="Seg_6640" s="T275">maːtɨ-r-lä</ta>
            <ta e="T277" id="Seg_6641" s="T276">meː-mpɨlʼ</ta>
            <ta e="T278" id="Seg_6642" s="T277">soqqɨ-sä</ta>
            <ta e="T279" id="Seg_6643" s="T278">na</ta>
            <ta e="T280" id="Seg_6644" s="T279">tokk-ätɔːl-altɨ-mpɨ-ntɨ-tɨ</ta>
            <ta e="T281" id="Seg_6645" s="T280">šentɨ</ta>
            <ta e="T282" id="Seg_6646" s="T281">maːtɨ-r-lä</ta>
            <ta e="T283" id="Seg_6647" s="T282">meː-mpɨlʼ</ta>
            <ta e="T284" id="Seg_6648" s="T283">qälɨk-lʼ</ta>
            <ta e="T286" id="Seg_6649" s="T285">na</ta>
            <ta e="T287" id="Seg_6650" s="T286">tokk-ätɔːl-altɨ-mpɨ-ntɨ-tɨ</ta>
            <ta e="T288" id="Seg_6651" s="T287">nɨːnɨ</ta>
            <ta e="T289" id="Seg_6652" s="T288">ama-tɨ</ta>
            <ta e="T290" id="Seg_6653" s="T289">nılʼčʼɨ-k</ta>
            <ta e="T291" id="Seg_6654" s="T290">soqɨš-mpɨ-ntɨ-tɨ</ta>
            <ta e="T292" id="Seg_6655" s="T291">ɔːtä-n-tɨ</ta>
            <ta e="T293" id="Seg_6656" s="T292">olɨ</ta>
            <ta e="T294" id="Seg_6657" s="T293">ɨkɨ</ta>
            <ta e="T295" id="Seg_6658" s="T294">laqɨ-rɨ-ätɨ</ta>
            <ta e="T296" id="Seg_6659" s="T295">ontɨt</ta>
            <ta e="T299" id="Seg_6660" s="T298">tuː-kkɨ-ɛntɨ-tɨt</ta>
            <ta e="T300" id="Seg_6661" s="T299">aj</ta>
            <ta e="T301" id="Seg_6662" s="T300">moqɨnä</ta>
            <ta e="T303" id="Seg_6663" s="T302">taːtɨ-ɛntɨ-tɨt</ta>
            <ta e="T304" id="Seg_6664" s="T303">iːja</ta>
            <ta e="T305" id="Seg_6665" s="T304">ɔːqqalʼ-tɨ</ta>
            <ta e="T306" id="Seg_6666" s="T305">*tar-altɨ-lä</ta>
            <ta e="T307" id="Seg_6667" s="T306">ınnä</ta>
            <ta e="T308" id="Seg_6668" s="T307">omtɨ-lä</ta>
            <ta e="T309" id="Seg_6669" s="T308">ɔːtä-n-tɨ</ta>
            <ta e="T310" id="Seg_6670" s="T309">olɨ-m</ta>
            <ta e="T311" id="Seg_6671" s="T310">nʼennä</ta>
            <ta e="T312" id="Seg_6672" s="T311">na</ta>
            <ta e="T313" id="Seg_6673" s="T312">üːtɨ-mpɨ-ntɨ</ta>
            <ta e="T314" id="Seg_6674" s="T313">nɨːnɨ</ta>
            <ta e="T315" id="Seg_6675" s="T314">nʼennä</ta>
            <ta e="T316" id="Seg_6676" s="T315">laqaltɨ-ptäː-qɨn-ntɨ</ta>
            <ta e="T317" id="Seg_6677" s="T316">qaj</ta>
            <ta e="T318" id="Seg_6678" s="T317">čʼumpɨ-k</ta>
            <ta e="T319" id="Seg_6679" s="T318">qən-ntɨ-mpɨ</ta>
            <ta e="T320" id="Seg_6680" s="T319">qaj</ta>
            <ta e="T321" id="Seg_6681" s="T320">qɔːmɨčʼä-k</ta>
            <ta e="T322" id="Seg_6682" s="T321">ukkɨr</ta>
            <ta e="T323" id="Seg_6683" s="T322">nʼarɨ-qɨn-lʼ</ta>
            <ta e="T324" id="Seg_6684" s="T323">kotpas</ta>
            <ta e="T325" id="Seg_6685" s="T324">pɔːrɨ-ntɨ</ta>
            <ta e="T326" id="Seg_6686" s="T325">na</ta>
            <ta e="T327" id="Seg_6687" s="T326">omtɨ-ntɨlʼ</ta>
            <ta e="T328" id="Seg_6688" s="T327">ɛː-mpɨ-ntɨ</ta>
            <ta e="T329" id="Seg_6689" s="T328">kotpas</ta>
            <ta e="T330" id="Seg_6690" s="T329">pɔːrɨ-qɨn</ta>
            <ta e="T331" id="Seg_6691" s="T330">ɔːmtɨ-lä</ta>
            <ta e="T332" id="Seg_6692" s="T331">nılʼčʼɨ-k</ta>
            <ta e="T333" id="Seg_6693" s="T332">qo-ŋɨ-tɨ</ta>
            <ta e="T334" id="Seg_6694" s="T333">montɨ</ta>
            <ta e="T335" id="Seg_6695" s="T334">mɨta</ta>
            <ta e="T336" id="Seg_6696" s="T335">karrä-n</ta>
            <ta e="T337" id="Seg_6697" s="T336">toː-n</ta>
            <ta e="T338" id="Seg_6698" s="T337">qanɨŋ-qɨn</ta>
            <ta e="T339" id="Seg_6699" s="T338">nɔːkɨr</ta>
            <ta e="T340" id="Seg_6700" s="T339">mɔːt</ta>
            <ta e="T341" id="Seg_6701" s="T340">ɔːmtɨ-ntɨ</ta>
            <ta e="T342" id="Seg_6702" s="T341">na</ta>
            <ta e="T343" id="Seg_6703" s="T342">mɔːt-ɨ-qan-ɨ-qɨn</ta>
            <ta e="T344" id="Seg_6704" s="T343">qälɨk</ta>
            <ta e="T345" id="Seg_6705" s="T344">ɔːtä</ta>
            <ta e="T346" id="Seg_6706" s="T345">kur-ɔːl-a-mpɨ-ntɨ-tɨt</ta>
            <ta e="T347" id="Seg_6707" s="T346">iːja</ta>
            <ta e="T348" id="Seg_6708" s="T347">ašša</ta>
            <ta e="T349" id="Seg_6709" s="T348">qattɨ</ta>
            <ta e="T350" id="Seg_6710" s="T349">mɔːt-ntɨ</ta>
            <ta e="T351" id="Seg_6711" s="T350">*korɨ-lä</ta>
            <ta e="T352" id="Seg_6712" s="T351">qaqlɨ-k-ɛː</ta>
            <ta e="T353" id="Seg_6713" s="T352">čʼontɨ-qɨn-ntɨ</ta>
            <ta e="T354" id="Seg_6714" s="T353">ontɨ</ta>
            <ta e="T355" id="Seg_6715" s="T354">mɨta</ta>
            <ta e="T356" id="Seg_6716" s="T355">seːpɨlaŋ</ta>
            <ta e="T357" id="Seg_6717" s="T356">wərqɨ</ta>
            <ta e="T358" id="Seg_6718" s="T357">mɔːt</ta>
            <ta e="T359" id="Seg_6719" s="T358">ɔːmtɨ-ntɨ</ta>
            <ta e="T360" id="Seg_6720" s="T359">na</ta>
            <ta e="T361" id="Seg_6721" s="T360">wərqɨ</ta>
            <ta e="T362" id="Seg_6722" s="T361">mɔːt-ɨ-t-ɨ-n</ta>
            <ta e="T363" id="Seg_6723" s="T362">čʼɔːtɨ</ta>
            <ta e="T364" id="Seg_6724" s="T363">ıllä</ta>
            <ta e="T365" id="Seg_6725" s="T364">utɨ-r-ɛː-ŋɨ-tɨ</ta>
            <ta e="T366" id="Seg_6726" s="T365">ıllä</ta>
            <ta e="T367" id="Seg_6727" s="T366">utɨ-r-ɛː-mpɨ-lä</ta>
            <ta e="T368" id="Seg_6728" s="T367">qaqlɨ-n-tɨ</ta>
            <ta e="T369" id="Seg_6729" s="T368">šünʼčʼɨ-qɨn</ta>
            <ta e="T370" id="Seg_6730" s="T369">ɔːmtɨ</ta>
            <ta e="T371" id="Seg_6731" s="T370">mɔːt-qɨnɨ</ta>
            <ta e="T372" id="Seg_6732" s="T371">ponä</ta>
            <ta e="T373" id="Seg_6733" s="T372">qum-ɨ-t</ta>
            <ta e="T374" id="Seg_6734" s="T373">nʼeː-t</ta>
            <ta e="T375" id="Seg_6735" s="T374">tantɨ-mpɨ-ntɨ-tɨt</ta>
            <ta e="T376" id="Seg_6736" s="T375">ukkɨr</ta>
            <ta e="T377" id="Seg_6737" s="T376">ira</ta>
            <ta e="T378" id="Seg_6738" s="T377">montɨ</ta>
            <ta e="T379" id="Seg_6739" s="T378">mɨta</ta>
            <ta e="T380" id="Seg_6740" s="T379">iːja-nɨŋ</ta>
            <ta e="T381" id="Seg_6741" s="T380">tü-lä</ta>
            <ta e="T382" id="Seg_6742" s="T381">nal-ʼa-t</ta>
            <ta e="T383" id="Seg_6743" s="T382">ɛsɨ-mpɨ-ntɨ</ta>
            <ta e="T384" id="Seg_6744" s="T383">qaj-lʼ</ta>
            <ta e="T385" id="Seg_6745" s="T384">təttɨ-n</ta>
            <ta e="T386" id="Seg_6746" s="T385">qum-ɨ-ntɨ</ta>
            <ta e="T387" id="Seg_6747" s="T386">ɛsɨ-ntɨ</ta>
            <ta e="T388" id="Seg_6748" s="T387">iːja</ta>
            <ta e="T389" id="Seg_6749" s="T388">na-lʼa-t</ta>
            <ta e="T390" id="Seg_6750" s="T389">ɛsɨ-mpɨ-ntɨ</ta>
            <ta e="T391" id="Seg_6751" s="T390">man</ta>
            <ta e="T392" id="Seg_6752" s="T391">nʼi</ta>
            <ta e="T393" id="Seg_6753" s="T392">qum-ɨ-m</ta>
            <ta e="T394" id="Seg_6754" s="T393">ašša</ta>
            <ta e="T395" id="Seg_6755" s="T394">tɛnɨmɨ-m</ta>
            <ta e="T396" id="Seg_6756" s="T395">nʼi</ta>
            <ta e="T397" id="Seg_6757" s="T396">qaj-m</ta>
            <ta e="T398" id="Seg_6758" s="T397">ašša</ta>
            <ta e="T399" id="Seg_6759" s="T398">tɛnɨmɨ-m</ta>
            <ta e="T400" id="Seg_6760" s="T399">pɛlɨ-kɔːlɨ</ta>
            <ta e="T401" id="Seg_6761" s="T400">ilɨ-ptäː-lʼ</ta>
            <ta e="T402" id="Seg_6762" s="T401">təttɨ-n</ta>
            <ta e="T403" id="Seg_6763" s="T402">*nɔː-nɨ</ta>
            <ta e="T404" id="Seg_6764" s="T403">a</ta>
            <ta e="T405" id="Seg_6765" s="T404">tan</ta>
            <ta e="T406" id="Seg_6766" s="T405">mɨta</ta>
            <ta e="T407" id="Seg_6767" s="T406">qaj</ta>
            <ta e="T408" id="Seg_6768" s="T407">ira-ŋɨ-ntɨ</ta>
            <ta e="T409" id="Seg_6769" s="T408">ɛsɨ-ntɨ</ta>
            <ta e="T410" id="Seg_6770" s="T409">na</ta>
            <ta e="T411" id="Seg_6771" s="T410">ira</ta>
            <ta e="T412" id="Seg_6772" s="T411">nılʼčʼɨ-k</ta>
            <ta e="T413" id="Seg_6773" s="T412">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T414" id="Seg_6774" s="T413">meː</ta>
            <ta e="T415" id="Seg_6775" s="T414">mɨta</ta>
            <ta e="T416" id="Seg_6776" s="T415">nɔːkɨr</ta>
            <ta e="T417" id="Seg_6777" s="T416">sərɨ-tətta-lʼ</ta>
            <ta e="T418" id="Seg_6778" s="T417">timnʼa-sɨ-mɨt</ta>
            <ta e="T419" id="Seg_6779" s="T418">poːsɨ</ta>
            <ta e="T420" id="Seg_6780" s="T419">wərqɨ</ta>
            <ta e="T421" id="Seg_6781" s="T420">sərɨ-tətta</ta>
            <ta e="T422" id="Seg_6782" s="T421">nık</ta>
            <ta e="T423" id="Seg_6783" s="T422">man</ta>
            <ta e="T424" id="Seg_6784" s="T423">ɛː-mpɨ-ntɨ-k</ta>
            <ta e="T425" id="Seg_6785" s="T424">tına</ta>
            <ta e="T426" id="Seg_6786" s="T425">ponä</ta>
            <ta e="T427" id="Seg_6787" s="T426">tantɨ-mpɨlʼ</ta>
            <ta e="T428" id="Seg_6788" s="T427">qum-ɨ-t</ta>
            <ta e="T429" id="Seg_6789" s="T428">montɨ</ta>
            <ta e="T430" id="Seg_6790" s="T429">mɨta</ta>
            <ta e="T431" id="Seg_6791" s="T430">ɔːtä-lʼ</ta>
            <ta e="T432" id="Seg_6792" s="T431">ora-ntɨ-tɨt</ta>
            <ta e="T433" id="Seg_6793" s="T432">na</ta>
            <ta e="T434" id="Seg_6794" s="T433">ɔːtä-t-ɨ-n</ta>
            <ta e="T435" id="Seg_6795" s="T434">puːtɨ-n</ta>
            <ta e="T436" id="Seg_6796" s="T435">ukkɨr</ta>
            <ta e="T437" id="Seg_6797" s="T436">ɔːmtɨ-kɨtɨ-lʼ</ta>
            <ta e="T438" id="Seg_6798" s="T437">qälɨk-lʼ</ta>
            <ta e="T439" id="Seg_6799" s="T438">waŋkɨta-lʼ</ta>
            <ta e="T440" id="Seg_6800" s="T439">čʼəktɨ</ta>
            <ta e="T441" id="Seg_6801" s="T440">ora-ntɨ-tɨt</ta>
            <ta e="T442" id="Seg_6802" s="T441">nılʼčʼɨ-k</ta>
            <ta e="T443" id="Seg_6803" s="T442">iːja</ta>
            <ta e="T444" id="Seg_6804" s="T443">mantɨ-mpɨ-tɨ</ta>
            <ta e="T445" id="Seg_6805" s="T444">orqɨl-qo</ta>
            <ta e="T446" id="Seg_6806" s="T445">tačʼal-mpɨ-tɨt</ta>
            <ta e="T447" id="Seg_6807" s="T446">iːja</ta>
            <ta e="T448" id="Seg_6808" s="T447">qaqlɨ-n-tɨ</ta>
            <ta e="T449" id="Seg_6809" s="T448">šünʼčʼɨ-qɨn</ta>
            <ta e="T450" id="Seg_6810" s="T449">ɔːmtɨ-lä</ta>
            <ta e="T451" id="Seg_6811" s="T450">tɛːmnɨ-m</ta>
            <ta e="T452" id="Seg_6812" s="T451">tına</ta>
            <ta e="T453" id="Seg_6813" s="T452">tottɨ-qɨl-mpɨ-ntɨ-ptäː-nɨ</ta>
            <ta e="T454" id="Seg_6814" s="T453">čʼəktɨ-m</ta>
            <ta e="T455" id="Seg_6815" s="T454">iːja-n</ta>
            <ta e="T456" id="Seg_6816" s="T455">qan-mɨt</ta>
            <ta e="T457" id="Seg_6817" s="T456">na</ta>
            <ta e="T458" id="Seg_6818" s="T457">*mi-ntɨ-r-ntɨlʼ</ta>
            <ta e="T459" id="Seg_6819" s="T458">tɔːqqɨ-mpɨ-ntɨ-tɨt</ta>
            <ta e="T460" id="Seg_6820" s="T459">iːja</ta>
            <ta e="T461" id="Seg_6821" s="T460">čʼəktɨ-m</ta>
            <ta e="T462" id="Seg_6822" s="T461">taːtɨ-r-altɨ-lä</ta>
            <ta e="T463" id="Seg_6823" s="T462">orqɨl-mpɨ-ntɨ-lä</ta>
            <ta e="T464" id="Seg_6824" s="T463">tɛːmnɨ</ta>
            <ta e="T465" id="Seg_6825" s="T464">čʼam</ta>
            <ta e="T466" id="Seg_6826" s="T465">mišal-mpɨ-tɨ</ta>
            <ta e="T467" id="Seg_6827" s="T466">čʼəktɨ-n</ta>
            <ta e="T468" id="Seg_6828" s="T467">pü-qo</ta>
            <ta e="T469" id="Seg_6829" s="T468">toːnna</ta>
            <ta e="T470" id="Seg_6830" s="T469">seːpɨ-mpɨ-ntɨ</ta>
            <ta e="T471" id="Seg_6831" s="T470">iːja</ta>
            <ta e="T472" id="Seg_6832" s="T471">tɛːmnɨ-m-tɨ</ta>
            <ta e="T473" id="Seg_6833" s="T472">ınnä</ta>
            <ta e="T474" id="Seg_6834" s="T473">na</ta>
            <ta e="T475" id="Seg_6835" s="T474">tottɨ-qɨl-mpɨ-ntɨ</ta>
            <ta e="T476" id="Seg_6836" s="T475">na-lʼa-t</ta>
            <ta e="T477" id="Seg_6837" s="T476">ɛsɨ-mpɨ</ta>
            <ta e="T478" id="Seg_6838" s="T477">tam</ta>
            <ta e="T479" id="Seg_6839" s="T478">tɛːmnɨ-lʼ</ta>
            <ta e="T480" id="Seg_6840" s="T479">tomɨ</ta>
            <ta e="T481" id="Seg_6841" s="T480">kəm-sä</ta>
            <ta e="T482" id="Seg_6842" s="T481">qajqo</ta>
            <ta e="T483" id="Seg_6843" s="T482">nəkɨ-qɨl-tɨ-m</ta>
            <ta e="T484" id="Seg_6844" s="T483">na</ta>
            <ta e="T485" id="Seg_6845" s="T484">qum-ɨ-t</ta>
            <ta e="T486" id="Seg_6846" s="T485">ɔːtä-m</ta>
            <ta e="T487" id="Seg_6847" s="T486">tına</ta>
            <ta e="T488" id="Seg_6848" s="T487">kırɨ-mpɨ-ntɨ-tɨt</ta>
            <ta e="T489" id="Seg_6849" s="T488">qälɨk-lʼ</ta>
            <ta e="T490" id="Seg_6850" s="T489">qum-ɨ-t</ta>
            <ta e="T491" id="Seg_6851" s="T490">ponä</ta>
            <ta e="T492" id="Seg_6852" s="T491">tuš-tɨ-qo-olam-mpɨ-ntɨ-tɨt</ta>
            <ta e="T493" id="Seg_6853" s="T492">iːja</ta>
            <ta e="T494" id="Seg_6854" s="T493">na-lʼa-t</ta>
            <ta e="T495" id="Seg_6855" s="T494">na</ta>
            <ta e="T496" id="Seg_6856" s="T495">*kurɨ-altɨ-mpɨ-ntɨ-tɨt</ta>
            <ta e="T497" id="Seg_6857" s="T496">tan</ta>
            <ta e="T498" id="Seg_6858" s="T497">lʼa</ta>
            <ta e="T499" id="Seg_6859" s="T498">meːsä</ta>
            <ta e="T500" id="Seg_6860" s="T499">tuš-tɨ-äšɨk</ta>
            <ta e="T501" id="Seg_6861" s="T500">iːja</ta>
            <ta e="T502" id="Seg_6862" s="T501">na-lʼa-t</ta>
            <ta e="T503" id="Seg_6863" s="T502">na</ta>
            <ta e="T504" id="Seg_6864" s="T503">ɛsɨ-mpɨ-ntɨ</ta>
            <ta e="T505" id="Seg_6865" s="T504">man</ta>
            <ta e="T506" id="Seg_6866" s="T505">tuš-tɨ-qo</ta>
            <ta e="T507" id="Seg_6867" s="T506">ašša</ta>
            <ta e="T508" id="Seg_6868" s="T507">tɛnɨmɨ-k</ta>
            <ta e="T509" id="Seg_6869" s="T508">tına</ta>
            <ta e="T510" id="Seg_6870" s="T509">sərɨ</ta>
            <ta e="T511" id="Seg_6871" s="T510">tətta-lʼ</ta>
            <ta e="T512" id="Seg_6872" s="T511">ira</ta>
            <ta e="T513" id="Seg_6873" s="T512">na-lʼa-t</ta>
            <ta e="T514" id="Seg_6874" s="T513">na</ta>
            <ta e="T515" id="Seg_6875" s="T514">ɛsɨ-mpɨ-ntɨ</ta>
            <ta e="T516" id="Seg_6876" s="T515">tɛː</ta>
            <ta e="T517" id="Seg_6877" s="T516">qajqo</ta>
            <ta e="T518" id="Seg_6878" s="T517">ɔːmtɨ-lɨt</ta>
            <ta e="T519" id="Seg_6879" s="T518">wəčʼɨ-t</ta>
            <ta e="T520" id="Seg_6880" s="T519">mɔːt-ntɨ</ta>
            <ta e="T521" id="Seg_6881" s="T520">tuː-ltɨ-lä</ta>
            <ta e="T522" id="Seg_6882" s="T521">mušɨ-rɨ-ŋɨlɨt</ta>
            <ta e="T523" id="Seg_6883" s="T522">iːja</ta>
            <ta e="T524" id="Seg_6884" s="T523">mantɨ-mpɨ-tɨ</ta>
            <ta e="T525" id="Seg_6885" s="T524">montɨ</ta>
            <ta e="T526" id="Seg_6886" s="T525">mɨta</ta>
            <ta e="T527" id="Seg_6887" s="T526">mɔːt-qɨnɨ</ta>
            <ta e="T528" id="Seg_6888" s="T527">ponä</ta>
            <ta e="T529" id="Seg_6889" s="T528">ukkɨr</ta>
            <ta e="T530" id="Seg_6890" s="T529">nätäk</ta>
            <ta e="T531" id="Seg_6891" s="T530">na</ta>
            <ta e="T532" id="Seg_6892" s="T531">tantɨ-mpɨ-ntɨ</ta>
            <ta e="T533" id="Seg_6893" s="T532">nätäk</ta>
            <ta e="T534" id="Seg_6894" s="T533">tantɨ-lä</ta>
            <ta e="T535" id="Seg_6895" s="T534">wəčʼɨ-m</ta>
            <ta e="T536" id="Seg_6896" s="T535">maːtɨ-lä</ta>
            <ta e="T537" id="Seg_6897" s="T536">mɔːt-ntɨ</ta>
            <ta e="T538" id="Seg_6898" s="T537">na</ta>
            <ta e="T539" id="Seg_6899" s="T538">tul-tɨ-mpɨ-ntɨ</ta>
            <ta e="T540" id="Seg_6900" s="T539">ašša</ta>
            <ta e="T541" id="Seg_6901" s="T540">kuntɨ</ta>
            <ta e="T542" id="Seg_6902" s="T541">ɔːmtɨ-lä</ta>
            <ta e="T543" id="Seg_6903" s="T542">puːlä</ta>
            <ta e="T544" id="Seg_6904" s="T543">ira</ta>
            <ta e="T545" id="Seg_6905" s="T544">aj</ta>
            <ta e="T546" id="Seg_6906" s="T545">iːja</ta>
            <ta e="T547" id="Seg_6907" s="T546">mɔːt-ntɨ</ta>
            <ta e="T548" id="Seg_6908" s="T547">na</ta>
            <ta e="T549" id="Seg_6909" s="T548">šeːr-mpɨ-ntɨ-qı</ta>
            <ta e="T550" id="Seg_6910" s="T549">iːja</ta>
            <ta e="T551" id="Seg_6911" s="T550">mɔːt</ta>
            <ta e="T552" id="Seg_6912" s="T551">šeːr-lä</ta>
            <ta e="T553" id="Seg_6913" s="T552">mɔːta-n</ta>
            <ta e="T554" id="Seg_6914" s="T553">ɔːŋ-ntɨ</ta>
            <ta e="T555" id="Seg_6915" s="T554">čʼam</ta>
            <ta e="T556" id="Seg_6916" s="T555">nɨl-ɛː-ɨ-qo-olam-mpɨ</ta>
            <ta e="T557" id="Seg_6917" s="T556">ira</ta>
            <ta e="T558" id="Seg_6918" s="T557">na-lʼa-t</ta>
            <ta e="T559" id="Seg_6919" s="T558">na</ta>
            <ta e="T560" id="Seg_6920" s="T559">ɛsɨ-mpɨ-ntɨ</ta>
            <ta e="T561" id="Seg_6921" s="T560">tɛː</ta>
            <ta e="T562" id="Seg_6922" s="T561">qaj</ta>
            <ta e="T563" id="Seg_6923" s="T562">qum-ɨ-m</ta>
            <ta e="T564" id="Seg_6924" s="T563">ašša</ta>
            <ta e="T565" id="Seg_6925" s="T564">qo-ntɨ-lɨt</ta>
            <ta e="T566" id="Seg_6926" s="T565">nʼennä</ta>
            <ta e="T567" id="Seg_6927" s="T566">üːtɨ-qɨntɨtqo</ta>
            <ta e="T568" id="Seg_6928" s="T567">qajqo</ta>
            <ta e="T569" id="Seg_6929" s="T568">mɔːta-n</ta>
            <ta e="T570" id="Seg_6930" s="T569">ɔːŋ-ntɨ</ta>
            <ta e="T571" id="Seg_6931" s="T570">nɨ-ltɨ-mpɨ-lɨt</ta>
            <ta e="T572" id="Seg_6932" s="T571">iːja-m</ta>
            <ta e="T573" id="Seg_6933" s="T572">nʼennä</ta>
            <ta e="T574" id="Seg_6934" s="T573">ira-n</ta>
            <ta e="T575" id="Seg_6935" s="T574">qö-ntɨ</ta>
            <ta e="T576" id="Seg_6936" s="T575">na</ta>
            <ta e="T577" id="Seg_6937" s="T576">omtɨ-ltɨ-mpɨ-ntɨ-tɨt</ta>
            <ta e="T578" id="Seg_6938" s="T577">wəčʼɨ-ntɨ-sä</ta>
            <ta e="T579" id="Seg_6939" s="T578">na</ta>
            <ta e="T580" id="Seg_6940" s="T579">tıː-tɨ-alʼ-mpɨ-ntɨ-tɨt</ta>
            <ta e="T581" id="Seg_6941" s="T580">selʼčʼɨ-qɨlʼ</ta>
            <ta e="T582" id="Seg_6942" s="T581">apsɨ-lʼ</ta>
            <ta e="T583" id="Seg_6943" s="T582">na</ta>
            <ta e="T584" id="Seg_6944" s="T583">tottɨ-mpɨ-ntɨ-tɨt</ta>
            <ta e="T585" id="Seg_6945" s="T584">šittɨ</ta>
            <ta e="T586" id="Seg_6946" s="T585">qum-qı</ta>
            <ta e="T587" id="Seg_6947" s="T586">ponä</ta>
            <ta e="T588" id="Seg_6948" s="T587">tantɨ-lä</ta>
            <ta e="T589" id="Seg_6949" s="T588">üt-ɨ-m</ta>
            <ta e="T590" id="Seg_6950" s="T589">mɔːt-ntɨ</ta>
            <ta e="T591" id="Seg_6951" s="T590">na</ta>
            <ta e="T592" id="Seg_6952" s="T591">tul-tɨ-mpɨ-ntɨ-tɨt</ta>
            <ta e="T593" id="Seg_6953" s="T592">na</ta>
            <ta e="T594" id="Seg_6954" s="T593">üt-ɨ-m</ta>
            <ta e="T595" id="Seg_6955" s="T594">per-alʼ-ntɨ-tɨt</ta>
            <ta e="T596" id="Seg_6956" s="T595">aj</ta>
            <ta e="T597" id="Seg_6957" s="T596">am-ɨ-r-qo-olam-mpɨ-ntɨ-tɨt</ta>
            <ta e="T598" id="Seg_6958" s="T597">iːja</ta>
            <ta e="T599" id="Seg_6959" s="T598">am-ɨ-r-ptäː</ta>
            <ta e="T600" id="Seg_6960" s="T599">čʼontɨ-qɨn</ta>
            <ta e="T601" id="Seg_6961" s="T600">nılʼčʼɨ-k</ta>
            <ta e="T602" id="Seg_6962" s="T601">üntɨ-š-mpɨ-ntɨ-ŋɨ</ta>
            <ta e="T603" id="Seg_6963" s="T602">montɨ</ta>
            <ta e="T604" id="Seg_6964" s="T603">mɨta</ta>
            <ta e="T605" id="Seg_6965" s="T604">Lapta-lʼ</ta>
            <ta e="T606" id="Seg_6966" s="T605">nɔːkɨr</ta>
            <ta e="T607" id="Seg_6967" s="T606">timnʼa-sɨ-t</ta>
            <ta e="T608" id="Seg_6968" s="T607">ima-š-lä</ta>
            <ta e="T609" id="Seg_6969" s="T608">təp</ta>
            <ta e="T610" id="Seg_6970" s="T609">taːtɨ-mpɨ-tɨt</ta>
            <ta e="T611" id="Seg_6971" s="T610">sərɨ-tətta-lʼ</ta>
            <ta e="T612" id="Seg_6972" s="T611">timnʼa-sɨ-t-nkinı</ta>
            <ta e="T613" id="Seg_6973" s="T612">sərɨ-tətta-lʼ</ta>
            <ta e="T614" id="Seg_6974" s="T613">timnʼa-sɨ-t</ta>
            <ta e="T615" id="Seg_6975" s="T614">ukkɨr</ta>
            <ta e="T616" id="Seg_6976" s="T615">nennʼa-tɨt</ta>
            <ta e="T617" id="Seg_6977" s="T616">ɛː-mpɨ-ntɨ</ta>
            <ta e="T618" id="Seg_6978" s="T617">na-nɨŋ</ta>
            <ta e="T619" id="Seg_6979" s="T618">ima-š-lä</ta>
            <ta e="T620" id="Seg_6980" s="T619">tü-mpɨ-tɨt</ta>
            <ta e="T621" id="Seg_6981" s="T620">am-ɨ-r-lä</ta>
            <ta e="T622" id="Seg_6982" s="T621">soma-k</ta>
            <ta e="T623" id="Seg_6983" s="T622">tarɨ-ntɨ-ntɨ-tɨt</ta>
            <ta e="T624" id="Seg_6984" s="T623">nɨːnɨ</ta>
            <ta e="T625" id="Seg_6985" s="T624">šittɨ</ta>
            <ta e="T626" id="Seg_6986" s="T625">na</ta>
            <ta e="T627" id="Seg_6987" s="T626">qən-qo-olam-mpɨ-ntɨ-tɨt</ta>
            <ta e="T628" id="Seg_6988" s="T627">əːtɨ-t</ta>
            <ta e="T629" id="Seg_6989" s="T628">montɨ</ta>
            <ta e="T630" id="Seg_6990" s="T629">mɨta</ta>
            <ta e="T631" id="Seg_6991" s="T630">nılʼčʼɨ-k</ta>
            <ta e="T632" id="Seg_6992" s="T631">nʼentɨ</ta>
            <ta e="T633" id="Seg_6993" s="T632">nɨː</ta>
            <ta e="T634" id="Seg_6994" s="T633">pin-ntɨ-tɨt</ta>
            <ta e="T635" id="Seg_6995" s="T634">täːlɨ</ta>
            <ta e="T636" id="Seg_6996" s="T635">nɔːtɨ</ta>
            <ta e="T637" id="Seg_6997" s="T636">qum-ɨ-qum-t-ɨ-lʼ</ta>
            <ta e="T638" id="Seg_6998" s="T637">nʼentɨ</ta>
            <ta e="T639" id="Seg_6999" s="T638">na</ta>
            <ta e="T640" id="Seg_7000" s="T639">omtɨ-ltɨ-ɛntɨ-tɨt</ta>
            <ta e="T641" id="Seg_7001" s="T640">qum-ɨ-t-ɨ-n</ta>
            <ta e="T642" id="Seg_7002" s="T641">qən-ptäː-tqo</ta>
            <ta e="T643" id="Seg_7003" s="T642">iːja</ta>
            <ta e="T644" id="Seg_7004" s="T643">aj</ta>
            <ta e="T645" id="Seg_7005" s="T644">moqɨnä</ta>
            <ta e="T646" id="Seg_7006" s="T645">na</ta>
            <ta e="T647" id="Seg_7007" s="T646">qən-qo-olam-ntɨ-tɨ-ntɨ</ta>
            <ta e="T648" id="Seg_7008" s="T647">ponä</ta>
            <ta e="T649" id="Seg_7009" s="T648">tantɨ-ptäː-qɨn-ntɨt</ta>
            <ta e="T650" id="Seg_7010" s="T649">iːja</ta>
            <ta e="T651" id="Seg_7011" s="T650">soqqɨ-m-tɨ</ta>
            <ta e="T652" id="Seg_7012" s="T651">soqqɨ-al-ntɨ-ptäː-qɨn</ta>
            <ta e="T653" id="Seg_7013" s="T652">Lapta-lʼ</ta>
            <ta e="T654" id="Seg_7014" s="T653">poːsɨ</ta>
            <ta e="T655" id="Seg_7015" s="T654">wərqɨ</ta>
            <ta e="T656" id="Seg_7016" s="T655">timnʼa-t-ɨ-n</ta>
            <ta e="T658" id="Seg_7017" s="T657">na</ta>
            <ta e="T659" id="Seg_7018" s="T658">ɛsɨ-mpɨ-ntɨ</ta>
            <ta e="T660" id="Seg_7019" s="T659">iːja-nɨŋ</ta>
            <ta e="T661" id="Seg_7020" s="T660">soqqɨ-ntɨ</ta>
            <ta e="T662" id="Seg_7021" s="T661">tan</ta>
            <ta e="T663" id="Seg_7022" s="T662">mitɨ</ta>
            <ta e="T664" id="Seg_7023" s="T663">tına</ta>
            <ta e="T665" id="Seg_7024" s="T664">opčʼin-ntɨ-sä</ta>
            <ta e="T666" id="Seg_7025" s="T665">a</ta>
            <ta e="T667" id="Seg_7026" s="T666">soqqɨ-n-ntɨ</ta>
            <ta e="T668" id="Seg_7027" s="T667">olɨ</ta>
            <ta e="T669" id="Seg_7028" s="T668">mitɨ</ta>
            <ta e="T670" id="Seg_7029" s="T669">tına</ta>
            <ta e="T671" id="Seg_7030" s="T670">poː-lʼ</ta>
            <ta e="T672" id="Seg_7031" s="T671">kala-n</ta>
            <ta e="T673" id="Seg_7032" s="T672">olɨ</ta>
            <ta e="T674" id="Seg_7033" s="T673">iːja</ta>
            <ta e="T675" id="Seg_7034" s="T674">nʼi</ta>
            <ta e="T676" id="Seg_7035" s="T675">qaj-ɨ-m</ta>
            <ta e="T677" id="Seg_7036" s="T676">ašša</ta>
            <ta e="T678" id="Seg_7037" s="T677">na</ta>
            <ta e="T679" id="Seg_7038" s="T678">kətɨ-mpɨ-tɨ</ta>
            <ta e="T680" id="Seg_7039" s="T679">nɨːnɨ</ta>
            <ta e="T681" id="Seg_7040" s="T680">laqaltɨ-ɛː-mpɨ-ntɨ</ta>
            <ta e="T682" id="Seg_7041" s="T681">mɔːt-ntɨ</ta>
            <ta e="T683" id="Seg_7042" s="T682">čʼam</ta>
            <ta e="T684" id="Seg_7043" s="T683">tü-lʼčʼɨ</ta>
            <ta e="T685" id="Seg_7044" s="T684">ama-tɨ</ta>
            <ta e="T686" id="Seg_7045" s="T685">mɔːta-n</ta>
            <ta e="T687" id="Seg_7046" s="T686">ɔːŋ-qɨn</ta>
            <ta e="T688" id="Seg_7047" s="T687">ətɨ-ntɨlʼ</ta>
            <ta e="T689" id="Seg_7048" s="T688">tü-kkɨ-lä</ta>
            <ta e="T690" id="Seg_7049" s="T689">nɨŋ-kkɨ</ta>
            <ta e="T691" id="Seg_7050" s="T690">iːja-m-tɨ</ta>
            <ta e="T692" id="Seg_7051" s="T691">ətɨ-lä</ta>
            <ta e="T693" id="Seg_7052" s="T692">iːja</ta>
            <ta e="T694" id="Seg_7053" s="T693">tan</ta>
            <ta e="T695" id="Seg_7054" s="T694">kučʼčʼä</ta>
            <ta e="T696" id="Seg_7055" s="T695">namɨššak</ta>
            <ta e="T697" id="Seg_7056" s="T696">qən-ɨ-ŋɨ-ntɨ</ta>
            <ta e="T698" id="Seg_7057" s="T697">man</ta>
            <ta e="T699" id="Seg_7058" s="T698">puršɨ-mɔːt-ptäː-noːqo</ta>
            <ta e="T700" id="Seg_7059" s="T699">karrä-n</ta>
            <ta e="T701" id="Seg_7060" s="T700">tam</ta>
            <ta e="T702" id="Seg_7061" s="T701">kotpas</ta>
            <ta e="T703" id="Seg_7062" s="T702">pɔːrɨ-qɨn</ta>
            <ta e="T704" id="Seg_7063" s="T703">ɔːmtɨ-kkɨ-olʼ-mpɨ-ntɨ-k</ta>
            <ta e="T705" id="Seg_7064" s="T704">na</ta>
            <ta e="T706" id="Seg_7065" s="T705">pi-n</ta>
            <ta e="T707" id="Seg_7066" s="T706">iːja</ta>
            <ta e="T708" id="Seg_7067" s="T707">na</ta>
            <ta e="T709" id="Seg_7068" s="T708">šäqqɨ-mpɨ-ntɨ</ta>
            <ta e="T710" id="Seg_7069" s="T709">tɔːptɨlʼ</ta>
            <ta e="T711" id="Seg_7070" s="T710">čʼeːlɨ</ta>
            <ta e="T712" id="Seg_7071" s="T711">ɔːtä-m-tɨ</ta>
            <ta e="T713" id="Seg_7072" s="T712">moqɨnä</ta>
            <ta e="T714" id="Seg_7073" s="T713">tɔːqqɨ-lä</ta>
            <ta e="T715" id="Seg_7074" s="T714">ɔːtä-n-tɨ</ta>
            <ta e="T716" id="Seg_7075" s="T715">puːtɨ-qɨn</ta>
            <ta e="T717" id="Seg_7076" s="T716">peː-kkɨ-lä</ta>
            <ta e="T718" id="Seg_7077" s="T717">šittɨ</ta>
            <ta e="T719" id="Seg_7078" s="T718">koška-n</ta>
            <ta e="T720" id="Seg_7079" s="T719">tar-ɨ-lʼ</ta>
            <ta e="T721" id="Seg_7080" s="T720">qoptɨ-qı-n</ta>
            <ta e="T722" id="Seg_7081" s="T721">na</ta>
            <ta e="T723" id="Seg_7082" s="T722">orqɨl-mpɨ-ntɨ-tɨ</ta>
            <ta e="T724" id="Seg_7083" s="T723">moː-t-ɨ-kɔːlɨ</ta>
            <ta e="T725" id="Seg_7084" s="T724">qaqlɨ-m-tɨ</ta>
            <ta e="T726" id="Seg_7085" s="T725">na</ta>
            <ta e="T727" id="Seg_7086" s="T726">sɔːrɨ-ɔːl-mpɨ-ntɨ-tɨ-ntɨ</ta>
            <ta e="T728" id="Seg_7087" s="T727">kotɨlʼ</ta>
            <ta e="T729" id="Seg_7088" s="T728">malʼčʼä-m-tɨ</ta>
            <ta e="T730" id="Seg_7089" s="T729">aj</ta>
            <ta e="T731" id="Seg_7090" s="T730">soqqɨ-m-tɨ</ta>
            <ta e="T732" id="Seg_7091" s="T731">na</ta>
            <ta e="T733" id="Seg_7092" s="T732">tokk-altɨ-mpɨ-ntɨ-ptäː-nɨ</ta>
            <ta e="T734" id="Seg_7093" s="T733">narɨ-poː-m-tɨ</ta>
            <ta e="T735" id="Seg_7094" s="T734">iː-mpɨ-tɨ</ta>
            <ta e="T736" id="Seg_7095" s="T735">qaj</ta>
            <ta e="T737" id="Seg_7096" s="T736">ašša</ta>
            <ta e="T739" id="Seg_7097" s="T738">nılʼčʼɨ-k</ta>
            <ta e="T740" id="Seg_7098" s="T739">na</ta>
            <ta e="T741" id="Seg_7099" s="T740">laqaltɨ-ɛː-mpɨ-ntɨ</ta>
            <ta e="T742" id="Seg_7100" s="T741">to-lʼ</ta>
            <ta e="T743" id="Seg_7101" s="T742">qum-ɨ-t-ɨ-n</ta>
            <ta e="T744" id="Seg_7102" s="T743">mɔːt-ntɨ</ta>
            <ta e="T745" id="Seg_7103" s="T744">čʼam</ta>
            <ta e="T746" id="Seg_7104" s="T745">tü-lʼčʼɨ</ta>
            <ta e="T747" id="Seg_7105" s="T746">montɨ</ta>
            <ta e="T748" id="Seg_7106" s="T747">mɨta</ta>
            <ta e="T749" id="Seg_7107" s="T748">Lapta-lʼ</ta>
            <ta e="T750" id="Seg_7108" s="T749">timnʼa-sɨ-t</ta>
            <ta e="T751" id="Seg_7109" s="T750">ukoːn</ta>
            <ta e="T752" id="Seg_7110" s="T751">qaj</ta>
            <ta e="T753" id="Seg_7111" s="T752">tü-ŋɨ-tɨt</ta>
            <ta e="T754" id="Seg_7112" s="T753">iːja</ta>
            <ta e="T755" id="Seg_7113" s="T754">ɔːqqalʼ-tɨ</ta>
            <ta e="T756" id="Seg_7114" s="T755">ıllä</ta>
            <ta e="T757" id="Seg_7115" s="T756">sɔːrɨ-lä</ta>
            <ta e="T758" id="Seg_7116" s="T757">mɔːt-ntɨ</ta>
            <ta e="T759" id="Seg_7117" s="T758">na</ta>
            <ta e="T760" id="Seg_7118" s="T759">šeːr-mpɨ-ntɨ</ta>
            <ta e="T761" id="Seg_7119" s="T760">mɔːt-ntɨ</ta>
            <ta e="T762" id="Seg_7120" s="T761">šeːr-lä</ta>
            <ta e="T763" id="Seg_7121" s="T762">mɔːt-ɨ-n</ta>
            <ta e="T764" id="Seg_7122" s="T763">ɔːŋ-ntɨ</ta>
            <ta e="T765" id="Seg_7123" s="T764">čʼam</ta>
            <ta e="T766" id="Seg_7124" s="T765">nɨl-ɛː-ŋɨ</ta>
            <ta e="T767" id="Seg_7125" s="T766">ira</ta>
            <ta e="T768" id="Seg_7126" s="T767">täːlɨ</ta>
            <ta e="T769" id="Seg_7127" s="T768">čʼeːlɨ</ta>
            <ta e="T770" id="Seg_7128" s="T769">montɨ</ta>
            <ta e="T771" id="Seg_7129" s="T770">mɨta</ta>
            <ta e="T772" id="Seg_7130" s="T771">na-lʼa-t</ta>
            <ta e="T773" id="Seg_7131" s="T772">ɛsɨ</ta>
            <ta e="T774" id="Seg_7132" s="T773">tɛː</ta>
            <ta e="T775" id="Seg_7133" s="T774">qaj</ta>
            <ta e="T776" id="Seg_7134" s="T775">*qoš-lʼ</ta>
            <ta e="T777" id="Seg_7135" s="T776">qum-ɨ-m</ta>
            <ta e="T778" id="Seg_7136" s="T777">ašša</ta>
            <ta e="T779" id="Seg_7137" s="T778">qo-ntɨ-lɨt</ta>
            <ta e="T780" id="Seg_7138" s="T779">nʼennä</ta>
            <ta e="T781" id="Seg_7139" s="T780">üːtɨ-qɨntɨtqo</ta>
            <ta e="T782" id="Seg_7140" s="T781">iːja</ta>
            <ta e="T783" id="Seg_7141" s="T782">mantɨ-mpɨ-tɨ</ta>
            <ta e="T784" id="Seg_7142" s="T783">ira-n</ta>
            <ta e="T785" id="Seg_7143" s="T784">qö-ntɨ</ta>
            <ta e="T786" id="Seg_7144" s="T785">koptɨ-m</ta>
            <ta e="T787" id="Seg_7145" s="T786">šittɨ</ta>
            <ta e="T788" id="Seg_7146" s="T787">na</ta>
            <ta e="T789" id="Seg_7147" s="T788">meː-ntɨ-tɨt</ta>
            <ta e="T790" id="Seg_7148" s="T789">iːja</ta>
            <ta e="T791" id="Seg_7149" s="T790">nʼennä</ta>
            <ta e="T792" id="Seg_7150" s="T791">ıllä</ta>
            <ta e="T793" id="Seg_7151" s="T792">omtɨ</ta>
            <ta e="T794" id="Seg_7152" s="T793">nılʼčʼɨ-k</ta>
            <ta e="T795" id="Seg_7153" s="T794">mantɨ-mpɨ-tɨ</ta>
            <ta e="T796" id="Seg_7154" s="T795">montɨ</ta>
            <ta e="T797" id="Seg_7155" s="T796">mɨta</ta>
            <ta e="T798" id="Seg_7156" s="T797">qum-ɨ-qum-t-ɨ-lʼ</ta>
            <ta e="T799" id="Seg_7157" s="T798">nʼentɨ</ta>
            <ta e="T800" id="Seg_7158" s="T799">omtɨ-ntɨlʼ</ta>
            <ta e="T801" id="Seg_7159" s="T800">tü-mpɨ-lä</ta>
            <ta e="T802" id="Seg_7160" s="T801">ütɨ-mpɨ-tɨt</ta>
            <ta e="T803" id="Seg_7161" s="T802">iːja</ta>
            <ta e="T804" id="Seg_7162" s="T803">aj</ta>
            <ta e="T805" id="Seg_7163" s="T804">na</ta>
            <ta e="T806" id="Seg_7164" s="T805">qum-ɨ-ntɨ-sä</ta>
            <ta e="T807" id="Seg_7165" s="T806">təp</ta>
            <ta e="T808" id="Seg_7166" s="T807">aj</ta>
            <ta e="T809" id="Seg_7167" s="T808">na</ta>
            <ta e="T810" id="Seg_7168" s="T809">ütɨ-mpɨ-r-alʼ-mpɨ-ntɨ</ta>
            <ta e="T811" id="Seg_7169" s="T810">montɨ</ta>
            <ta e="T812" id="Seg_7170" s="T811">mɨta</ta>
            <ta e="T813" id="Seg_7171" s="T812">iːja-m</ta>
            <ta e="T814" id="Seg_7172" s="T813">üt</ta>
            <ta e="T815" id="Seg_7173" s="T814">na</ta>
            <ta e="T816" id="Seg_7174" s="T815">šeːr-ntɨ-tɨ</ta>
            <ta e="T817" id="Seg_7175" s="T816">üt</ta>
            <ta e="T818" id="Seg_7176" s="T817">šeːr-lä</ta>
            <ta e="T819" id="Seg_7177" s="T818">iːja</ta>
            <ta e="T820" id="Seg_7178" s="T819">alʼčʼɨ</ta>
            <ta e="T821" id="Seg_7179" s="T820">üt</ta>
            <ta e="T822" id="Seg_7180" s="T821">šeːr-mpɨlʼ</ta>
            <ta e="T823" id="Seg_7181" s="T822">qum</ta>
            <ta e="T824" id="Seg_7182" s="T823">qaj</ta>
            <ta e="T825" id="Seg_7183" s="T824">qaj</ta>
            <ta e="T826" id="Seg_7184" s="T825">tɛnɨmɨ-ɛntɨ</ta>
            <ta e="T827" id="Seg_7185" s="T826">kuššat</ta>
            <ta e="T828" id="Seg_7186" s="T827">ašša</ta>
            <ta e="T829" id="Seg_7187" s="T828">na</ta>
            <ta e="T830" id="Seg_7188" s="T829">ippɨ-mpɨ-ntɨ</ta>
            <ta e="T831" id="Seg_7189" s="T830">ukkɨr</ta>
            <ta e="T832" id="Seg_7190" s="T831">tɔːt</ta>
            <ta e="T833" id="Seg_7191" s="T832">čʼontɨ-qɨn</ta>
            <ta e="T834" id="Seg_7192" s="T833">iːja</ta>
            <ta e="T835" id="Seg_7193" s="T834">ınnä</ta>
            <ta e="T836" id="Seg_7194" s="T835">čʼam</ta>
            <ta e="T837" id="Seg_7195" s="T836">qən-ŋɨ</ta>
            <ta e="T838" id="Seg_7196" s="T837">montɨ</ta>
            <ta e="T839" id="Seg_7197" s="T838">mɨta</ta>
            <ta e="T840" id="Seg_7198" s="T839">qum</ta>
            <ta e="T841" id="Seg_7199" s="T840">mašım</ta>
            <ta e="T842" id="Seg_7200" s="T841">qättɨ-rorr-ntɨlʼ</ta>
            <ta e="T843" id="Seg_7201" s="T842">nʼoː-tɨ-š</ta>
            <ta e="T844" id="Seg_7202" s="T843">nılʼčʼɨ-k</ta>
            <ta e="T845" id="Seg_7203" s="T844">ippɨ-lä</ta>
            <ta e="T846" id="Seg_7204" s="T845">üŋkɨl-tɨ-mpɨ-tɨ</ta>
            <ta e="T847" id="Seg_7205" s="T846">qaj-lʼ</ta>
            <ta e="T848" id="Seg_7206" s="T847">təttɨ-qɨn</ta>
            <ta e="T849" id="Seg_7207" s="T848">tına</ta>
            <ta e="T850" id="Seg_7208" s="T849">sərɨ-tətta-t-ɨ-n</ta>
            <ta e="T851" id="Seg_7209" s="T850">nennʼa</ta>
            <ta e="T852" id="Seg_7210" s="T851">laŋkɨ-š-kunä</ta>
            <ta e="T853" id="Seg_7211" s="T852">tɛː</ta>
            <ta e="T854" id="Seg_7212" s="T853">lʼa</ta>
            <ta e="T855" id="Seg_7213" s="T854">mompa</ta>
            <ta e="T856" id="Seg_7214" s="T855">mɨta</ta>
            <ta e="T857" id="Seg_7215" s="T856">qaj</ta>
            <ta e="T858" id="Seg_7216" s="T857">mantɨ-mpɨ-mpɨ-ntɨ-lɨt</ta>
            <ta e="T859" id="Seg_7217" s="T858">tına</ta>
            <ta e="T860" id="Seg_7218" s="T859">šentɨ-k</ta>
            <ta e="T861" id="Seg_7219" s="T860">tü-mpɨlʼ</ta>
            <ta e="T862" id="Seg_7220" s="T861">iːja-m</ta>
            <ta e="T863" id="Seg_7221" s="T862">na</ta>
            <ta e="T864" id="Seg_7222" s="T863">qättɨ-ntɨ-tɨt</ta>
            <ta e="T865" id="Seg_7223" s="T864">Lapta-lʼ</ta>
            <ta e="T866" id="Seg_7224" s="T865">timnʼa-sɨ-t</ta>
            <ta e="T867" id="Seg_7225" s="T866">iːja</ta>
            <ta e="T868" id="Seg_7226" s="T867">aššă</ta>
            <ta e="T869" id="Seg_7227" s="T868">qattɨ</ta>
            <ta e="T870" id="Seg_7228" s="T869">ippɨ-ptäː</ta>
            <ta e="T871" id="Seg_7229" s="T870">*sak-qɨn</ta>
            <ta e="T872" id="Seg_7230" s="T871">*iː-qɨn-tɨ</ta>
            <ta e="T873" id="Seg_7231" s="T872">qättɨ-lä</ta>
            <ta e="T874" id="Seg_7232" s="T873">ɔːlʼčʼɨ-mpɨlʼ</ta>
            <ta e="T875" id="Seg_7233" s="T874">qum-ɨ-m-tɨ</ta>
            <ta e="T876" id="Seg_7234" s="T875">səntɨ-ntɨ-sä</ta>
            <ta e="T877" id="Seg_7235" s="T876">šittɨ</ta>
            <ta e="T878" id="Seg_7236" s="T877">na</ta>
            <ta e="T879" id="Seg_7237" s="T878">qättɨ-mpɨ-ntɨ</ta>
            <ta e="T880" id="Seg_7238" s="T879">nɨːnɨ</ta>
            <ta e="T881" id="Seg_7239" s="T880">šittɨ-mtäl</ta>
            <ta e="T882" id="Seg_7240" s="T881">šittɨ</ta>
            <ta e="T883" id="Seg_7241" s="T882">čʼattɨ-mpɨ-ntɨ</ta>
            <ta e="T884" id="Seg_7242" s="T883">montɨ</ta>
            <ta e="T885" id="Seg_7243" s="T884">mɨta</ta>
            <ta e="T886" id="Seg_7244" s="T885">qälɨk-lʼ</ta>
            <ta e="T887" id="Seg_7245" s="T886">timnʼa-sɨ-t</ta>
            <ta e="T888" id="Seg_7246" s="T887">Laptaɨ-lʼ</ta>
            <ta e="T889" id="Seg_7247" s="T888">nılʼčʼɨ-k</ta>
            <ta e="T890" id="Seg_7248" s="T889">qu-mpɨ-tɨt</ta>
            <ta e="T891" id="Seg_7249" s="T890">ɔːmɨ-nʼeː-t-ɨ-n</ta>
            <ta e="T892" id="Seg_7250" s="T891">čʼontɨ-mɨn</ta>
            <ta e="T893" id="Seg_7251" s="T892">šittɨ</ta>
            <ta e="T894" id="Seg_7252" s="T893">səpɨ-ıː-mpɨ</ta>
            <ta e="T895" id="Seg_7253" s="T894">ɔːmɨ-nʼeː-t-ɨ-n</ta>
            <ta e="T896" id="Seg_7254" s="T895">kɨ-m</ta>
            <ta e="T897" id="Seg_7255" s="T896">šittɨ</ta>
            <ta e="T898" id="Seg_7256" s="T897">pasɨ-ıː-mpɨ</ta>
            <ta e="T899" id="Seg_7257" s="T898">a</ta>
            <ta e="T900" id="Seg_7258" s="T899">poːsɨ</ta>
            <ta e="T901" id="Seg_7259" s="T900">kɨpa</ta>
            <ta e="T902" id="Seg_7260" s="T901">qəːtɨ-sɨma-ɨ-lʼ</ta>
            <ta e="T903" id="Seg_7261" s="T902">timnʼa-t-ɨ-n</ta>
            <ta e="T904" id="Seg_7262" s="T903">montɨ</ta>
            <ta e="T905" id="Seg_7263" s="T904">mɨta</ta>
            <ta e="T906" id="Seg_7264" s="T905">olɨ-tɨ</ta>
            <ta e="T907" id="Seg_7265" s="T906">meːltɨ</ta>
            <ta e="T908" id="Seg_7266" s="T907">čʼäː</ta>
            <ta e="T909" id="Seg_7267" s="T908">a</ta>
            <ta e="T910" id="Seg_7268" s="T909">na</ta>
            <ta e="T911" id="Seg_7269" s="T910">moː-t-ɨ-kɔːlɨ</ta>
            <ta e="T912" id="Seg_7270" s="T911">mɔːt</ta>
            <ta e="T913" id="Seg_7271" s="T912">čʼaŋalʼ</ta>
            <ta e="T914" id="Seg_7272" s="T913">moː-t-ɨ-kɔːlɨ</ta>
            <ta e="T915" id="Seg_7273" s="T914">tɔːpɨ</ta>
            <ta e="T916" id="Seg_7274" s="T915">qalɨ-mpɨ-ntɨ</ta>
            <ta e="T917" id="Seg_7275" s="T916">mɔːt-ɨ-kota</ta>
            <ta e="T918" id="Seg_7276" s="T917">aj</ta>
            <ta e="T919" id="Seg_7277" s="T918">mɔːt-ɨ-qopɨ</ta>
            <ta e="T920" id="Seg_7278" s="T919">täːlɨ-čʼeːlɨ</ta>
            <ta e="T921" id="Seg_7279" s="T920">tɔːt</ta>
            <ta e="T922" id="Seg_7280" s="T921">toːn</ta>
            <ta e="T923" id="Seg_7281" s="T922">älpä-n</ta>
            <ta e="T924" id="Seg_7282" s="T923">ippɨ-tɨt</ta>
            <ta e="T925" id="Seg_7283" s="T924">ınnä</ta>
            <ta e="T926" id="Seg_7284" s="T925">wešɨ-lä</ta>
            <ta e="T927" id="Seg_7285" s="T926">nılʼčʼɨ-k</ta>
            <ta e="T928" id="Seg_7286" s="T927">šittɨ</ta>
            <ta e="T929" id="Seg_7287" s="T928">mantɨ-mpɨ-tɨ</ta>
            <ta e="T930" id="Seg_7288" s="T929">qum-ɨ-t</ta>
            <ta e="T931" id="Seg_7289" s="T930">taqɨ-mpɨ-lä</ta>
            <ta e="T932" id="Seg_7290" s="T931">təp-ɨ-m</ta>
            <ta e="T933" id="Seg_7291" s="T932">mantɨ-mpɨ-tɨt</ta>
            <ta e="T934" id="Seg_7292" s="T933">a</ta>
            <ta e="T935" id="Seg_7293" s="T934">tına</ta>
            <ta e="T936" id="Seg_7294" s="T935">nätäk</ta>
            <ta e="T937" id="Seg_7295" s="T936">meːltɨ</ta>
            <ta e="T938" id="Seg_7296" s="T937">čʼäːŋkɨ</ta>
            <ta e="T939" id="Seg_7297" s="T938">toː</ta>
            <ta e="T940" id="Seg_7298" s="T939">qən-lä</ta>
            <ta e="T941" id="Seg_7299" s="T940">mɔːt-ɨ-qopɨ-m</ta>
            <ta e="T942" id="Seg_7300" s="T941">ınnä</ta>
            <ta e="T943" id="Seg_7301" s="T942">šittɨ</ta>
            <ta e="T944" id="Seg_7302" s="T943">čʼam</ta>
            <ta e="T945" id="Seg_7303" s="T944">nüː-ŋɨ-tɨ</ta>
            <ta e="T946" id="Seg_7304" s="T945">montɨ</ta>
            <ta e="T947" id="Seg_7305" s="T946">nätäk</ta>
            <ta e="T948" id="Seg_7306" s="T947">nɨːnɨ</ta>
            <ta e="T949" id="Seg_7307" s="T948">na</ta>
            <ta e="T950" id="Seg_7308" s="T949">*putɨl-mɔːt-ntɨ</ta>
            <ta e="T951" id="Seg_7309" s="T950">ašša</ta>
            <ta e="T952" id="Seg_7310" s="T951">qattɨ-tɨt</ta>
            <ta e="T953" id="Seg_7311" s="T952">na</ta>
            <ta e="T954" id="Seg_7312" s="T953">nɨmtɨ-lʼ</ta>
            <ta e="T955" id="Seg_7313" s="T954">qum-ɨ-t</ta>
            <ta e="T956" id="Seg_7314" s="T955">mənɨlʼ</ta>
            <ta e="T957" id="Seg_7315" s="T956">mɔːt-ɨ-m</ta>
            <ta e="T958" id="Seg_7316" s="T957">čʼəsɨ-lä</ta>
            <ta e="T959" id="Seg_7317" s="T958">nɔːtɨ</ta>
            <ta e="T960" id="Seg_7318" s="T959">na</ta>
            <ta e="T961" id="Seg_7319" s="T960">ütɨ-ɛː-r-mpɨ-ntɨ-tɨt</ta>
            <ta e="T962" id="Seg_7320" s="T961">aj</ta>
            <ta e="T963" id="Seg_7321" s="T962">am-ɨ-r-mpɨ-tɨt</ta>
            <ta e="T964" id="Seg_7322" s="T963">sərɨ-tətta</ta>
            <ta e="T965" id="Seg_7323" s="T964">ira</ta>
            <ta e="T966" id="Seg_7324" s="T965">nennʼa-m-tɨ</ta>
            <ta e="T967" id="Seg_7325" s="T966">na</ta>
            <ta e="T968" id="Seg_7326" s="T967">iːja-nɨŋ</ta>
            <ta e="T969" id="Seg_7327" s="T968">na</ta>
            <ta e="T970" id="Seg_7328" s="T969">*kurɨ-altɨ-mpɨ-ntɨ-tɨ</ta>
            <ta e="T971" id="Seg_7329" s="T970">ilʼmatɨlʼ</ta>
            <ta e="T972" id="Seg_7330" s="T971">qum-ɨ-qum</ta>
            <ta e="T973" id="Seg_7331" s="T972">nʼentɨ</ta>
            <ta e="T974" id="Seg_7332" s="T973">omtɨ-mpɨ-ltɨ-ntɨ-lä</ta>
            <ta e="T975" id="Seg_7333" s="T974">tɛːttɨ-kolʼ</ta>
            <ta e="T976" id="Seg_7334" s="T975">köt</ta>
            <ta e="T977" id="Seg_7335" s="T976">čʼeːlɨ</ta>
            <ta e="T978" id="Seg_7336" s="T977">ütɨ-mpɨ-mpɨ-mpɨ-tɨt</ta>
            <ta e="T979" id="Seg_7337" s="T978">qum-ɨ-qum</ta>
            <ta e="T980" id="Seg_7338" s="T979">nʼentɨ</ta>
            <ta e="T981" id="Seg_7339" s="T980">omtɨ-tɨ-lɨ-mpɨ-lä</ta>
            <ta e="T982" id="Seg_7340" s="T981">sərɨ-tətta-lʼ</ta>
            <ta e="T983" id="Seg_7341" s="T982">timnʼa-sɨ-t</ta>
            <ta e="T984" id="Seg_7342" s="T983">na</ta>
            <ta e="T985" id="Seg_7343" s="T984">iːja-sä</ta>
            <ta e="T986" id="Seg_7344" s="T985">tɔː</ta>
            <ta e="T987" id="Seg_7345" s="T986">kərɨ-tɨ-tɨt</ta>
            <ta e="T988" id="Seg_7346" s="T987">iːja-n</ta>
            <ta e="T989" id="Seg_7347" s="T988">mɔːt-tɨ</ta>
            <ta e="T990" id="Seg_7348" s="T989">qan-ntɨ</ta>
            <ta e="T991" id="Seg_7349" s="T990">nʼentɨ</ta>
            <ta e="T992" id="Seg_7350" s="T991">palčʼa</ta>
            <ta e="T993" id="Seg_7351" s="T992">na</ta>
            <ta e="T994" id="Seg_7352" s="T993">ilɨ-mpɨ-ntɨ-tɨt</ta>
            <ta e="T995" id="Seg_7353" s="T994">i</ta>
            <ta e="T996" id="Seg_7354" s="T995">tiː</ta>
            <ta e="T997" id="Seg_7355" s="T996">kos</ta>
            <ta e="T998" id="Seg_7356" s="T997">to</ta>
            <ta e="T999" id="Seg_7357" s="T998">ilɨ-mpɨ-tɨt</ta>
            <ta e="T1000" id="Seg_7358" s="T999">čʼaptä</ta>
            <ta e="T1001" id="Seg_7359" s="T1000">nʼeː-mɨn</ta>
            <ta e="T1002" id="Seg_7360" s="T1001">toː</ta>
            <ta e="T1003" id="Seg_7361" s="T1002">na</ta>
            <ta e="T1004" id="Seg_7362" s="T1003">seːpɨ</ta>
            <ta e="T1005" id="Seg_7363" s="T1004">ɛː-mpɨ-ntɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T83" id="Seg_7364" s="T82">Lapta-ADJZ</ta>
            <ta e="T84" id="Seg_7365" s="T83">three</ta>
            <ta e="T85" id="Seg_7366" s="T84">brother-DYA.[NOM]-3SG</ta>
            <ta e="T86" id="Seg_7367" s="T85">two</ta>
            <ta e="T87" id="Seg_7368" s="T86">mother-DYA-DU.[NOM]</ta>
            <ta e="T88" id="Seg_7369" s="T87">live-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T89" id="Seg_7370" s="T88">son.[NOM]-3SG</ta>
            <ta e="T90" id="Seg_7371" s="T89">then</ta>
            <ta e="T91" id="Seg_7372" s="T90">human.being-OBL.3SG-COR</ta>
            <ta e="T92" id="Seg_7373" s="T91">force-TRL-PST.NAR.[3SG.S]</ta>
            <ta e="T93" id="Seg_7374" s="T92">son.[NOM]-3SG</ta>
            <ta e="T94" id="Seg_7375" s="T93">this-ACC</ta>
            <ta e="T95" id="Seg_7376" s="T94">NEG</ta>
            <ta e="T96" id="Seg_7377" s="T95">know-PST.NAR-3SG.O</ta>
            <ta e="T97" id="Seg_7378" s="T96">father.[NOM]-3SG</ta>
            <ta e="T98" id="Seg_7379" s="T97">when</ta>
            <ta e="T99" id="Seg_7380" s="T98">die-PST.NAR.[3SG.S]</ta>
            <ta e="T100" id="Seg_7381" s="T99">one</ta>
            <ta e="T101" id="Seg_7382" s="T100">this.day.[NOM]</ta>
            <ta e="T102" id="Seg_7383" s="T101">son.[NOM]</ta>
            <ta e="T103" id="Seg_7384" s="T102">mother-OBL.3SG-ALL</ta>
            <ta e="T104" id="Seg_7385" s="T103">here-hey-%%</ta>
            <ta e="T105" id="Seg_7386" s="T104">say.[3SG.S]</ta>
            <ta e="T106" id="Seg_7387" s="T105">mother.[NOM]</ta>
            <ta e="T107" id="Seg_7388" s="T106">I.GEN</ta>
            <ta e="T108" id="Seg_7389" s="T107">father-GEN.1SG</ta>
            <ta e="T109" id="Seg_7390" s="T108">go-PTCP.PST</ta>
            <ta e="T110" id="Seg_7391" s="T109">reindeer.[NOM]-3SG</ta>
            <ta e="T111" id="Seg_7392" s="T110">where.from</ta>
            <ta e="T112" id="Seg_7393" s="T111">I.ALL</ta>
            <ta e="T113" id="Seg_7394" s="T112">be.visible-TR-IMP.2SG.O</ta>
            <ta e="T114" id="Seg_7395" s="T113">and</ta>
            <ta e="T115" id="Seg_7396" s="T114">reindeer-3SG-ADJZ</ta>
            <ta e="T116" id="Seg_7397" s="T115">mother-DYA-DU.[NOM]</ta>
            <ta e="T117" id="Seg_7398" s="T116">from</ta>
            <ta e="T118" id="Seg_7399" s="T117">so.much</ta>
            <ta e="T119" id="Seg_7400" s="T118">be-PST.NAR.[3SG.S]</ta>
            <ta e="T120" id="Seg_7401" s="T119">earth-OBL.3SG-INSTR</ta>
            <ta e="T121" id="Seg_7402" s="T120">eat-PST.NAR-3SG.O</ta>
            <ta e="T122" id="Seg_7403" s="T121">and</ta>
            <ta e="T123" id="Seg_7404" s="T122">reindeer-OBL.3SG-ADJZ</ta>
            <ta e="T124" id="Seg_7405" s="T123">such-ADJZ</ta>
            <ta e="T125" id="Seg_7406" s="T124">bedding-OBL.3SG-ADJZ</ta>
            <ta e="T126" id="Seg_7407" s="T125">be-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T127" id="Seg_7408" s="T126">skin.[NOM]-3SG</ta>
            <ta e="T128" id="Seg_7409" s="T127">fish-GEN</ta>
            <ta e="T129" id="Seg_7410" s="T128">skin.[NOM]-3SG</ta>
            <ta e="T130" id="Seg_7411" s="T129">appearance.[NOM]-3SG</ta>
            <ta e="T131" id="Seg_7412" s="T130">be-PST.NAR.[3SG.S]</ta>
            <ta e="T132" id="Seg_7413" s="T131">this-ACC-3SG</ta>
            <ta e="T133" id="Seg_7414" s="T132">scrape-MOM-TR-CVB</ta>
            <ta e="T134" id="Seg_7415" s="T133">reindeer-ACC-3SG-ACC</ta>
            <ta e="T135" id="Seg_7416" s="T134">such-ADVZ</ta>
            <ta e="T136" id="Seg_7417" s="T135">home</ta>
            <ta e="T137" id="Seg_7418" s="T136">shepherd-INF-be.going.to-EP-PST.NAR-3PL</ta>
            <ta e="T138" id="Seg_7419" s="T137">mother.[NOM]-3SG</ta>
            <ta e="T139" id="Seg_7420" s="T138">here-hey-%%</ta>
            <ta e="T140" id="Seg_7421" s="T139">say.[3SG.S]</ta>
            <ta e="T141" id="Seg_7422" s="T140">you.SG.NOM</ta>
            <ta e="T142" id="Seg_7423" s="T141">reindeer-COM</ta>
            <ta e="T143" id="Seg_7424" s="T142">where</ta>
            <ta e="T144" id="Seg_7425" s="T143">go.away-CO-2SG.S</ta>
            <ta e="T145" id="Seg_7426" s="T144">son.[NOM]</ta>
            <ta e="T146" id="Seg_7427" s="T145">this-hey-%%</ta>
            <ta e="T147" id="Seg_7428" s="T146">say.[3SG.S]</ta>
            <ta e="T148" id="Seg_7429" s="T147">I.NOM</ta>
            <ta e="T149" id="Seg_7430" s="T148">NEG</ta>
            <ta e="T150" id="Seg_7431" s="T149">where</ta>
            <ta e="T151" id="Seg_7432" s="T150">NEG</ta>
            <ta e="T152" id="Seg_7433" s="T151">go.away-FUT-1SG.S</ta>
            <ta e="T153" id="Seg_7434" s="T152">I.NOM</ta>
            <ta e="T154" id="Seg_7435" s="T153">this</ta>
            <ta e="T155" id="Seg_7436" s="T154">be.bored-DRV-ACTN-TRL.1SG</ta>
            <ta e="T156" id="Seg_7437" s="T155">down-ADV.LOC</ta>
            <ta e="T157" id="Seg_7438" s="T156">tundra-GEN</ta>
            <ta e="T158" id="Seg_7439" s="T157">see-TR-FUT-1SG.S</ta>
            <ta e="T159" id="Seg_7440" s="T158">mother.[NOM]-3SG</ta>
            <ta e="T160" id="Seg_7441" s="T159">NEG</ta>
            <ta e="T161" id="Seg_7442" s="T160">where.get.to.[3SG.S]</ta>
            <ta e="T162" id="Seg_7443" s="T161">sledge-PROL</ta>
            <ta e="T163" id="Seg_7444" s="T162">inside-ADV.LOC</ta>
            <ta e="T164" id="Seg_7445" s="T163">get.untied-US-MULO-TR-CVB</ta>
            <ta e="T165" id="Seg_7446" s="T164">this-ACC-3SG</ta>
            <ta e="T166" id="Seg_7447" s="T165">thick-ADJZ</ta>
            <ta e="T167" id="Seg_7448" s="T166">reindeer-ADJZ</ta>
            <ta e="T168" id="Seg_7449" s="T167">force-INSTR-ADJZ</ta>
            <ta e="T169" id="Seg_7450" s="T168">rope.[NOM]</ta>
            <ta e="T170" id="Seg_7451" s="T169">down</ta>
            <ta e="T171" id="Seg_7452" s="T170">take-INFER-3SG.O</ta>
            <ta e="T172" id="Seg_7453" s="T171">apparently</ta>
            <ta e="T173" id="Seg_7454" s="T172">as.if</ta>
            <ta e="T174" id="Seg_7455" s="T173">tree-ADJZ</ta>
            <ta e="T175" id="Seg_7456" s="T174">scoop-GEN</ta>
            <ta e="T176" id="Seg_7457" s="T175">handle-GEN</ta>
            <ta e="T177" id="Seg_7458" s="T176">thick</ta>
            <ta e="T178" id="Seg_7459" s="T177">be-CO</ta>
            <ta e="T179" id="Seg_7460" s="T178">guy-ALL</ta>
            <ta e="T180" id="Seg_7461" s="T179">such-ADVZ</ta>
            <ta e="T181" id="Seg_7462" s="T180">suggest-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T182" id="Seg_7463" s="T181">down</ta>
            <ta e="T183" id="Seg_7464" s="T182">this</ta>
            <ta e="T184" id="Seg_7465" s="T183">reindeer-GEN-OBL.2SG</ta>
            <ta e="T185" id="Seg_7466" s="T184">inside-LOC</ta>
            <ta e="T186" id="Seg_7467" s="T185">three</ta>
            <ta e="T187" id="Seg_7468" s="T186">speckled-ADJZ</ta>
            <ta e="T188" id="Seg_7469" s="T187">bull.[NOM]</ta>
            <ta e="T189" id="Seg_7470" s="T188">INFER</ta>
            <ta e="T190" id="Seg_7471" s="T189">be-INFER.[3SG.S]</ta>
            <ta e="T191" id="Seg_7472" s="T190">one</ta>
            <ta e="T192" id="Seg_7473" s="T191">speckled-ADJZ</ta>
            <ta e="T193" id="Seg_7474" s="T192">bull.[NOM]</ta>
            <ta e="T194" id="Seg_7475" s="T193">INFER</ta>
            <ta e="T195" id="Seg_7476" s="T194">be-INFER.[3SG.S]</ta>
            <ta e="T196" id="Seg_7477" s="T195">big-in.some.degree-ADJZ</ta>
            <ta e="T197" id="Seg_7478" s="T196">this-ACC</ta>
            <ta e="T198" id="Seg_7479" s="T197">catch-MULO-IMP.2SG.O</ta>
            <ta e="T199" id="Seg_7480" s="T198">that</ta>
            <ta e="T200" id="Seg_7481" s="T199">something-DU.[NOM]</ta>
            <ta e="T201" id="Seg_7482" s="T200">oneself.3DU</ta>
            <ta e="T202" id="Seg_7483" s="T201">bellow-CVB</ta>
            <ta e="T203" id="Seg_7484" s="T202">INFER</ta>
            <ta e="T204" id="Seg_7485" s="T203">come-IPFV-FUT-3DU.S</ta>
            <ta e="T205" id="Seg_7486" s="T204">guy.[NOM]</ta>
            <ta e="T206" id="Seg_7487" s="T205">rope-ACC-3SG</ta>
            <ta e="T207" id="Seg_7488" s="T206">half</ta>
            <ta e="T208" id="Seg_7489" s="T207">to.the.extent.of</ta>
            <ta e="T209" id="Seg_7490" s="T208">INFER</ta>
            <ta e="T210" id="Seg_7491" s="T209">put-MULO-PST.NAR-INFER-3SG.O</ta>
            <ta e="T211" id="Seg_7492" s="T210">such-ADVZ</ta>
            <ta e="T212" id="Seg_7493" s="T211">down</ta>
            <ta e="T213" id="Seg_7494" s="T212">INFER</ta>
            <ta e="T214" id="Seg_7495" s="T213">leave-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T215" id="Seg_7496" s="T214">reindeer-GEN-3SG</ta>
            <ta e="T216" id="Seg_7497" s="T215">inside-ADV.LOC</ta>
            <ta e="T217" id="Seg_7498" s="T216">look.for-HAB-CVB</ta>
            <ta e="T218" id="Seg_7499" s="T217">that</ta>
            <ta e="T219" id="Seg_7500" s="T218">mother-GEN-3SG</ta>
            <ta e="T220" id="Seg_7501" s="T219">speak-PTCP.PST</ta>
            <ta e="T221" id="Seg_7502" s="T220">bull-ACC</ta>
            <ta e="T222" id="Seg_7503" s="T221">INFER</ta>
            <ta e="T223" id="Seg_7504" s="T222">catch-PST.NAR-INFER-3SG.O</ta>
            <ta e="T224" id="Seg_7505" s="T223">upwards</ta>
            <ta e="T225" id="Seg_7506" s="T224">bring-CO-3SG.O</ta>
            <ta e="T226" id="Seg_7507" s="T225">harness-MOM-INF</ta>
            <ta e="T227" id="Seg_7508" s="T226">this-GEN</ta>
            <ta e="T228" id="Seg_7509" s="T227">during</ta>
            <ta e="T229" id="Seg_7510" s="T228">mother.[NOM]-3SG</ta>
            <ta e="T230" id="Seg_7511" s="T229">apparently</ta>
            <ta e="T231" id="Seg_7512" s="T230">as.if</ta>
            <ta e="T232" id="Seg_7513" s="T231">sledge-PL-EP-GEN</ta>
            <ta e="T233" id="Seg_7514" s="T232">inside-LOC</ta>
            <ta e="T234" id="Seg_7515" s="T233">such</ta>
            <ta e="T235" id="Seg_7516" s="T234">sledge.[NOM]</ta>
            <ta e="T236" id="Seg_7517" s="T235">down</ta>
            <ta e="T237" id="Seg_7518" s="T236">take-PST.NAR-INFER-3SG.O</ta>
            <ta e="T238" id="Seg_7519" s="T237">apparently</ta>
            <ta e="T239" id="Seg_7520" s="T238">as.if</ta>
            <ta e="T240" id="Seg_7521" s="T239">three-ten-EP-ADJZ</ta>
            <ta e="T241" id="Seg_7522" s="T240">mammoth</ta>
            <ta e="T242" id="Seg_7523" s="T241">horn-ADJZ</ta>
            <ta e="T243" id="Seg_7524" s="T242">leg.[NOM]-3SG</ta>
            <ta e="T244" id="Seg_7525" s="T243">guy.[NOM]</ta>
            <ta e="T245" id="Seg_7526" s="T244">sledge-ACC-3SG</ta>
            <ta e="T246" id="Seg_7527" s="T245">INFER</ta>
            <ta e="T247" id="Seg_7528" s="T246">harness-MOM-INF-begin-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T248" id="Seg_7529" s="T247">apparently</ta>
            <ta e="T249" id="Seg_7530" s="T248">as.if</ta>
            <ta e="T250" id="Seg_7531" s="T249">that</ta>
            <ta e="T251" id="Seg_7532" s="T250">two</ta>
            <ta e="T252" id="Seg_7533" s="T251">speckled-ADJZ</ta>
            <ta e="T253" id="Seg_7534" s="T252">small-in.some.degree-ADJZ</ta>
            <ta e="T254" id="Seg_7535" s="T253">bull-DU-GEN</ta>
            <ta e="T255" id="Seg_7536" s="T254">oneself.3DU</ta>
            <ta e="T256" id="Seg_7537" s="T255">bellow-CVB</ta>
            <ta e="T257" id="Seg_7538" s="T256">apparently</ta>
            <ta e="T258" id="Seg_7539" s="T257">as.if</ta>
            <ta e="T259" id="Seg_7540" s="T258">upwards</ta>
            <ta e="T260" id="Seg_7541" s="T259">INFER</ta>
            <ta e="T261" id="Seg_7542" s="T260">come-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T262" id="Seg_7543" s="T261">guy.[NOM]</ta>
            <ta e="T263" id="Seg_7544" s="T262">sledge-ACC-3SG</ta>
            <ta e="T264" id="Seg_7545" s="T263">harness-CVB</ta>
            <ta e="T265" id="Seg_7546" s="T264">INFER</ta>
            <ta e="T266" id="Seg_7547" s="T265">be.over-TR-PST.NAR-INFER-3SG.O</ta>
            <ta e="T267" id="Seg_7548" s="T266">mother.[NOM]-3SG</ta>
            <ta e="T268" id="Seg_7549" s="T267">seven</ta>
            <ta e="T269" id="Seg_7550" s="T268">joint-EP-ADJZ</ta>
            <ta e="T270" id="Seg_7551" s="T269">mammoth</ta>
            <ta e="T271" id="Seg_7552" s="T270">horn-ADJZ</ta>
            <ta e="T272" id="Seg_7553" s="T271">log-tree-INSTR</ta>
            <ta e="T273" id="Seg_7554" s="T272">INFER</ta>
            <ta e="T274" id="Seg_7555" s="T273">give-TR-PST.NAR-INFER-3SG.O</ta>
            <ta e="T275" id="Seg_7556" s="T274">speckled-ADJZ</ta>
            <ta e="T276" id="Seg_7557" s="T275">cut-FRQ-CVB</ta>
            <ta e="T277" id="Seg_7558" s="T276">make-PTCP.PST</ta>
            <ta e="T278" id="Seg_7559" s="T277">fur.clothing-INSTR</ta>
            <ta e="T279" id="Seg_7560" s="T278">INFER</ta>
            <ta e="T280" id="Seg_7561" s="T279">put.on-MOM-TR-PST.NAR-INFER-3SG.O</ta>
            <ta e="T281" id="Seg_7562" s="T280">new</ta>
            <ta e="T282" id="Seg_7563" s="T281">cut-FRQ-CVB</ta>
            <ta e="T283" id="Seg_7564" s="T282">make-PTCP.PST</ta>
            <ta e="T284" id="Seg_7565" s="T283">Nenets-ADJZ</ta>
            <ta e="T286" id="Seg_7566" s="T285">INFER</ta>
            <ta e="T287" id="Seg_7567" s="T286">put.on-MOM-TR-PST.NAR-INFER-3SG.O</ta>
            <ta e="T288" id="Seg_7568" s="T287">then</ta>
            <ta e="T289" id="Seg_7569" s="T288">mother.[NOM]-3SG</ta>
            <ta e="T290" id="Seg_7570" s="T289">such-ADVZ</ta>
            <ta e="T291" id="Seg_7571" s="T290">ask-PST.NAR-INFER-3SG.O</ta>
            <ta e="T292" id="Seg_7572" s="T291">reindeer-GEN-3SG</ta>
            <ta e="T293" id="Seg_7573" s="T292">head.[NOM]</ta>
            <ta e="T294" id="Seg_7574" s="T293">NEG.IMP</ta>
            <ta e="T295" id="Seg_7575" s="T294">move-CAUS-IMP.2SG.O</ta>
            <ta e="T296" id="Seg_7576" s="T295">oneself.3PL</ta>
            <ta e="T299" id="Seg_7577" s="T298">carry-HAB-FUT-3PL</ta>
            <ta e="T300" id="Seg_7578" s="T299">and</ta>
            <ta e="T301" id="Seg_7579" s="T300">home</ta>
            <ta e="T303" id="Seg_7580" s="T302">bring-FUT-3PL</ta>
            <ta e="T304" id="Seg_7581" s="T303">guy.[NOM]</ta>
            <ta e="T305" id="Seg_7582" s="T304">reins.[NOM]-3SG</ta>
            <ta e="T306" id="Seg_7583" s="T305">get.untied-CAUS-CVB</ta>
            <ta e="T307" id="Seg_7584" s="T306">up</ta>
            <ta e="T308" id="Seg_7585" s="T307">sit.down-CVB</ta>
            <ta e="T309" id="Seg_7586" s="T308">reindeer-GEN-3SG</ta>
            <ta e="T310" id="Seg_7587" s="T309">head-ACC</ta>
            <ta e="T311" id="Seg_7588" s="T310">forward</ta>
            <ta e="T312" id="Seg_7589" s="T311">INFER</ta>
            <ta e="T313" id="Seg_7590" s="T312">let.go-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T314" id="Seg_7591" s="T313">then</ta>
            <ta e="T315" id="Seg_7592" s="T314">forward</ta>
            <ta e="T316" id="Seg_7593" s="T315">start.for-ACTN-LOC-OBL.3SG</ta>
            <ta e="T317" id="Seg_7594" s="T316">either.or</ta>
            <ta e="T318" id="Seg_7595" s="T317">long-ADVZ</ta>
            <ta e="T319" id="Seg_7596" s="T318">go.away-IPFV-PST.NAR.[3SG.S]</ta>
            <ta e="T320" id="Seg_7597" s="T319">either.or</ta>
            <ta e="T321" id="Seg_7598" s="T320">short-ADVZ</ta>
            <ta e="T322" id="Seg_7599" s="T321">one</ta>
            <ta e="T323" id="Seg_7600" s="T322">tundra-LOC-ADJZ</ta>
            <ta e="T324" id="Seg_7601" s="T323">hill</ta>
            <ta e="T325" id="Seg_7602" s="T324">above-ILL</ta>
            <ta e="T326" id="Seg_7603" s="T325">INFER</ta>
            <ta e="T327" id="Seg_7604" s="T326">sit.down-PTCP.PRS</ta>
            <ta e="T328" id="Seg_7605" s="T327">be-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T329" id="Seg_7606" s="T328">hill</ta>
            <ta e="T330" id="Seg_7607" s="T329">above-LOC</ta>
            <ta e="T331" id="Seg_7608" s="T330">sit-CVB</ta>
            <ta e="T332" id="Seg_7609" s="T331">such-ADVZ</ta>
            <ta e="T333" id="Seg_7610" s="T332">see-CO-3SG.O</ta>
            <ta e="T334" id="Seg_7611" s="T333">apparently</ta>
            <ta e="T335" id="Seg_7612" s="T334">as.if</ta>
            <ta e="T336" id="Seg_7613" s="T335">down-ADV.LOC</ta>
            <ta e="T337" id="Seg_7614" s="T336">lake-GEN</ta>
            <ta e="T338" id="Seg_7615" s="T337">bank-LOC</ta>
            <ta e="T339" id="Seg_7616" s="T338">three</ta>
            <ta e="T340" id="Seg_7617" s="T339">house.[NOM]</ta>
            <ta e="T341" id="Seg_7618" s="T340">stand-INFER.[3SG.S]</ta>
            <ta e="T342" id="Seg_7619" s="T341">this</ta>
            <ta e="T343" id="Seg_7620" s="T342">tent-EP-near-EP-LOC</ta>
            <ta e="T344" id="Seg_7621" s="T343">Nenets</ta>
            <ta e="T345" id="Seg_7622" s="T344">reindeer</ta>
            <ta e="T346" id="Seg_7623" s="T345">run-MOM-EP-PST.NAR-INFER-3PL</ta>
            <ta e="T347" id="Seg_7624" s="T346">guy.[NOM]</ta>
            <ta e="T348" id="Seg_7625" s="T347">NEG</ta>
            <ta e="T349" id="Seg_7626" s="T348">where.get.to.[3SG.S]</ta>
            <ta e="T350" id="Seg_7627" s="T349">tent-ILL</ta>
            <ta e="T351" id="Seg_7628" s="T350">turn-CVB</ta>
            <ta e="T352" id="Seg_7629" s="T351">sledge-VBLZ-PFV.[3SG.S]</ta>
            <ta e="T353" id="Seg_7630" s="T352">middle-LOC-OBL.3SG</ta>
            <ta e="T354" id="Seg_7631" s="T353">oneself.3SG</ta>
            <ta e="T355" id="Seg_7632" s="T354">as.if</ta>
            <ta e="T356" id="Seg_7633" s="T355">enough</ta>
            <ta e="T357" id="Seg_7634" s="T356">big</ta>
            <ta e="T358" id="Seg_7635" s="T357">tent.[NOM]</ta>
            <ta e="T359" id="Seg_7636" s="T358">stand-INFER.[3SG.S]</ta>
            <ta e="T360" id="Seg_7637" s="T359">this</ta>
            <ta e="T361" id="Seg_7638" s="T360">big</ta>
            <ta e="T362" id="Seg_7639" s="T361">tent-EP-PL-EP-GEN</ta>
            <ta e="T363" id="Seg_7640" s="T362">opposite</ta>
            <ta e="T364" id="Seg_7641" s="T363">down</ta>
            <ta e="T365" id="Seg_7642" s="T364">stop-FRQ-PFV-CO-3SG.O</ta>
            <ta e="T366" id="Seg_7643" s="T365">down</ta>
            <ta e="T367" id="Seg_7644" s="T366">stop-FRQ-PFV-DUR-CVB</ta>
            <ta e="T368" id="Seg_7645" s="T367">sledge-GEN-3SG</ta>
            <ta e="T369" id="Seg_7646" s="T368">inside-LOC</ta>
            <ta e="T370" id="Seg_7647" s="T369">sit.[3SG.S]</ta>
            <ta e="T371" id="Seg_7648" s="T370">tent-EL</ta>
            <ta e="T372" id="Seg_7649" s="T371">outwards</ta>
            <ta e="T373" id="Seg_7650" s="T372">human.being-EP-PL.[NOM]</ta>
            <ta e="T374" id="Seg_7651" s="T373">somebody-PL.[NOM]</ta>
            <ta e="T375" id="Seg_7652" s="T374">go.out-PST.NAR-INFER-3PL</ta>
            <ta e="T376" id="Seg_7653" s="T375">one</ta>
            <ta e="T377" id="Seg_7654" s="T376">old.man.[NOM]</ta>
            <ta e="T378" id="Seg_7655" s="T377">apparently</ta>
            <ta e="T379" id="Seg_7656" s="T378">as.if</ta>
            <ta e="T380" id="Seg_7657" s="T379">guy-ALL</ta>
            <ta e="T381" id="Seg_7658" s="T380">come-CVB</ta>
            <ta e="T382" id="Seg_7659" s="T381">here-hey-%%</ta>
            <ta e="T383" id="Seg_7660" s="T382">say-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T384" id="Seg_7661" s="T383">what-ADJZ</ta>
            <ta e="T385" id="Seg_7662" s="T384">earth-GEN</ta>
            <ta e="T386" id="Seg_7663" s="T385">human.being-EP-2SG.S</ta>
            <ta e="T387" id="Seg_7664" s="T386">become-2SG.S</ta>
            <ta e="T388" id="Seg_7665" s="T387">guy.[NOM]</ta>
            <ta e="T389" id="Seg_7666" s="T388">here-hey-%%</ta>
            <ta e="T390" id="Seg_7667" s="T389">say-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T391" id="Seg_7668" s="T390">I.NOM</ta>
            <ta e="T392" id="Seg_7669" s="T391">NEG</ta>
            <ta e="T393" id="Seg_7670" s="T392">human.being-EP-ACC</ta>
            <ta e="T394" id="Seg_7671" s="T393">NEG</ta>
            <ta e="T395" id="Seg_7672" s="T394">know-1SG.O</ta>
            <ta e="T396" id="Seg_7673" s="T395">NEG</ta>
            <ta e="T397" id="Seg_7674" s="T396">what-ACC</ta>
            <ta e="T398" id="Seg_7675" s="T397">NEG</ta>
            <ta e="T399" id="Seg_7676" s="T398">know-1SG.O</ta>
            <ta e="T400" id="Seg_7677" s="T399">friend-CAR</ta>
            <ta e="T401" id="Seg_7678" s="T400">live-ACTN-ADJZ</ta>
            <ta e="T402" id="Seg_7679" s="T401">earth-GEN</ta>
            <ta e="T403" id="Seg_7680" s="T402">out-ADV.EL</ta>
            <ta e="T404" id="Seg_7681" s="T403">and</ta>
            <ta e="T405" id="Seg_7682" s="T404">you.SG.NOM</ta>
            <ta e="T406" id="Seg_7683" s="T405">as.if</ta>
            <ta e="T407" id="Seg_7684" s="T406">what.[NOM]</ta>
            <ta e="T408" id="Seg_7685" s="T407">old.man-CO-2SG.S</ta>
            <ta e="T409" id="Seg_7686" s="T408">become-2SG.S</ta>
            <ta e="T410" id="Seg_7687" s="T409">this</ta>
            <ta e="T411" id="Seg_7688" s="T410">old.man.[NOM]</ta>
            <ta e="T412" id="Seg_7689" s="T411">such-ADVZ</ta>
            <ta e="T413" id="Seg_7690" s="T412">say-CO-3SG.O</ta>
            <ta e="T414" id="Seg_7691" s="T413">we.PL.NOM</ta>
            <ta e="T415" id="Seg_7692" s="T414">as.if</ta>
            <ta e="T416" id="Seg_7693" s="T415">three</ta>
            <ta e="T417" id="Seg_7694" s="T416">white-rich.man-ADJZ</ta>
            <ta e="T418" id="Seg_7695" s="T417">brother-DYA.[NOM]-1PL</ta>
            <ta e="T419" id="Seg_7696" s="T418">most</ta>
            <ta e="T420" id="Seg_7697" s="T419">big</ta>
            <ta e="T421" id="Seg_7698" s="T420">white-rich.man.[NOM]</ta>
            <ta e="T422" id="Seg_7699" s="T421">so</ta>
            <ta e="T423" id="Seg_7700" s="T422">I.NOM</ta>
            <ta e="T424" id="Seg_7701" s="T423">be-DUR-INFER-1SG.S</ta>
            <ta e="T425" id="Seg_7702" s="T424">that</ta>
            <ta e="T426" id="Seg_7703" s="T425">outwards</ta>
            <ta e="T427" id="Seg_7704" s="T426">go.out-PTCP.PST</ta>
            <ta e="T428" id="Seg_7705" s="T427">human.being-EP-PL.[NOM]</ta>
            <ta e="T429" id="Seg_7706" s="T428">apparently</ta>
            <ta e="T430" id="Seg_7707" s="T429">as.if</ta>
            <ta e="T431" id="Seg_7708" s="T430">reindeer-ADJZ</ta>
            <ta e="T432" id="Seg_7709" s="T431">catch-INFER-3PL</ta>
            <ta e="T433" id="Seg_7710" s="T432">this</ta>
            <ta e="T434" id="Seg_7711" s="T433">reindeer-PL-EP-GEN</ta>
            <ta e="T435" id="Seg_7712" s="T434">inside-ADV.LOC</ta>
            <ta e="T436" id="Seg_7713" s="T435">one</ta>
            <ta e="T437" id="Seg_7714" s="T436">horn-CAR-ADJZ</ta>
            <ta e="T438" id="Seg_7715" s="T437">Nenets-ADJZ</ta>
            <ta e="T439" id="Seg_7716" s="T438">dry.female.reindeer-ADJZ</ta>
            <ta e="T440" id="Seg_7717" s="T439">female.reindeer.[NOM]</ta>
            <ta e="T441" id="Seg_7718" s="T440">catch-INFER-3PL</ta>
            <ta e="T442" id="Seg_7719" s="T441">such-ADVZ</ta>
            <ta e="T443" id="Seg_7720" s="T442">guy.[NOM]</ta>
            <ta e="T444" id="Seg_7721" s="T443">give.a.look-PST.NAR-3SG.O</ta>
            <ta e="T445" id="Seg_7722" s="T444">catch-INF</ta>
            <ta e="T446" id="Seg_7723" s="T445">not.can-PST.NAR-3PL</ta>
            <ta e="T447" id="Seg_7724" s="T446">guy.[NOM]</ta>
            <ta e="T448" id="Seg_7725" s="T447">sledge-GEN-3SG</ta>
            <ta e="T449" id="Seg_7726" s="T448">inside-LOC</ta>
            <ta e="T450" id="Seg_7727" s="T449">sit-CVB</ta>
            <ta e="T451" id="Seg_7728" s="T450">rope-ACC</ta>
            <ta e="T452" id="Seg_7729" s="T451">that</ta>
            <ta e="T453" id="Seg_7730" s="T452">put-MULO-DUR-IPFV-ACTN-ADV.EL</ta>
            <ta e="T454" id="Seg_7731" s="T453">female.reindeer-ACC</ta>
            <ta e="T455" id="Seg_7732" s="T454">guy-GEN</ta>
            <ta e="T456" id="Seg_7733" s="T455">near-PROL</ta>
            <ta e="T457" id="Seg_7734" s="T456">INFER</ta>
            <ta e="T458" id="Seg_7735" s="T457">leave.behind-IPFV-FRQ-PTCP.PRS</ta>
            <ta e="T459" id="Seg_7736" s="T458">shepherd-PST.NAR-INFER-3PL</ta>
            <ta e="T460" id="Seg_7737" s="T459">guy.[NOM]</ta>
            <ta e="T461" id="Seg_7738" s="T460">female.reindeer-ACC</ta>
            <ta e="T462" id="Seg_7739" s="T461">bring-FRQ-CAUS-CVB</ta>
            <ta e="T463" id="Seg_7740" s="T462">catch-DUR-IPFV-CVB</ta>
            <ta e="T464" id="Seg_7741" s="T463">rope.[NOM]</ta>
            <ta e="T465" id="Seg_7742" s="T464">hardly</ta>
            <ta e="T466" id="Seg_7743" s="T465">pull-PST.NAR-3SG.O</ta>
            <ta e="T467" id="Seg_7744" s="T466">female.reindeer-GEN</ta>
            <ta e="T468" id="Seg_7745" s="T467">cut.away-INF</ta>
            <ta e="T469" id="Seg_7746" s="T468">that</ta>
            <ta e="T470" id="Seg_7747" s="T469">be.able-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T471" id="Seg_7748" s="T470">guy.[NOM]</ta>
            <ta e="T472" id="Seg_7749" s="T471">rope-ACC-3SG</ta>
            <ta e="T473" id="Seg_7750" s="T472">up</ta>
            <ta e="T474" id="Seg_7751" s="T473">INFER</ta>
            <ta e="T475" id="Seg_7752" s="T474">put-MULO-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T476" id="Seg_7753" s="T475">here-hey-%%</ta>
            <ta e="T477" id="Seg_7754" s="T476">say-PST.NAR.[3SG.S]</ta>
            <ta e="T478" id="Seg_7755" s="T477">this</ta>
            <ta e="T479" id="Seg_7756" s="T478">rope-ADJZ</ta>
            <ta e="T480" id="Seg_7757" s="T479">that</ta>
            <ta e="T481" id="Seg_7758" s="T480">blood-INSTR</ta>
            <ta e="T482" id="Seg_7759" s="T481">why</ta>
            <ta e="T483" id="Seg_7760" s="T482">write-MULO-TR-1SG.O</ta>
            <ta e="T484" id="Seg_7761" s="T483">this</ta>
            <ta e="T485" id="Seg_7762" s="T484">human.being-EP-PL.[NOM]</ta>
            <ta e="T486" id="Seg_7763" s="T485">reindeer-ACC</ta>
            <ta e="T487" id="Seg_7764" s="T486">that</ta>
            <ta e="T488" id="Seg_7765" s="T487">skin-PST.NAR-INFER-3PL</ta>
            <ta e="T489" id="Seg_7766" s="T488">Nenets-ADJZ</ta>
            <ta e="T490" id="Seg_7767" s="T489">human.being-EP-PL.[NOM]</ta>
            <ta e="T491" id="Seg_7768" s="T490">outwards</ta>
            <ta e="T492" id="Seg_7769" s="T491">meat.served.frozen.and.raw-TR-INF-begin-PST.NAR-INFER-3PL</ta>
            <ta e="T493" id="Seg_7770" s="T492">guy.[NOM]</ta>
            <ta e="T494" id="Seg_7771" s="T493">here-hey-%%</ta>
            <ta e="T495" id="Seg_7772" s="T494">INFER</ta>
            <ta e="T496" id="Seg_7773" s="T495">go-CAUS-PST.NAR-INFER-3PL</ta>
            <ta e="T497" id="Seg_7774" s="T496">you.SG.NOM</ta>
            <ta e="T498" id="Seg_7775" s="T497">hey</ta>
            <ta e="T499" id="Seg_7776" s="T498">we.PL.COM</ta>
            <ta e="T500" id="Seg_7777" s="T499">meat.served.frozen.and.raw-TR-IMP.2SG.S</ta>
            <ta e="T501" id="Seg_7778" s="T500">guy.[NOM]</ta>
            <ta e="T502" id="Seg_7779" s="T501">here-hey-%%</ta>
            <ta e="T503" id="Seg_7780" s="T502">INFER</ta>
            <ta e="T504" id="Seg_7781" s="T503">say-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T505" id="Seg_7782" s="T504">I.NOM</ta>
            <ta e="T506" id="Seg_7783" s="T505">meat.served.frozen.and.raw-TR-INF</ta>
            <ta e="T507" id="Seg_7784" s="T506">NEG</ta>
            <ta e="T508" id="Seg_7785" s="T507">can-1SG.S</ta>
            <ta e="T509" id="Seg_7786" s="T508">that</ta>
            <ta e="T510" id="Seg_7787" s="T509">white</ta>
            <ta e="T511" id="Seg_7788" s="T510">rich.man-ADJZ</ta>
            <ta e="T512" id="Seg_7789" s="T511">old.man.[NOM]</ta>
            <ta e="T513" id="Seg_7790" s="T512">here-hey-%%</ta>
            <ta e="T514" id="Seg_7791" s="T513">INFER</ta>
            <ta e="T515" id="Seg_7792" s="T514">say-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T516" id="Seg_7793" s="T515">you.PL.NOM</ta>
            <ta e="T517" id="Seg_7794" s="T516">why</ta>
            <ta e="T518" id="Seg_7795" s="T517">sit-2PL</ta>
            <ta e="T519" id="Seg_7796" s="T518">meat-PL.[NOM]</ta>
            <ta e="T520" id="Seg_7797" s="T519">tent-ILL</ta>
            <ta e="T521" id="Seg_7798" s="T520">carry-TR-CVB</ta>
            <ta e="T522" id="Seg_7799" s="T521">be.cooking-CAUS-IMP.2PL</ta>
            <ta e="T523" id="Seg_7800" s="T522">guy.[NOM]</ta>
            <ta e="T524" id="Seg_7801" s="T523">give.a.look-PST.NAR-3SG.O</ta>
            <ta e="T525" id="Seg_7802" s="T524">apparently</ta>
            <ta e="T526" id="Seg_7803" s="T525">as.if</ta>
            <ta e="T527" id="Seg_7804" s="T526">tent-EL</ta>
            <ta e="T528" id="Seg_7805" s="T527">outwards</ta>
            <ta e="T529" id="Seg_7806" s="T528">one</ta>
            <ta e="T530" id="Seg_7807" s="T529">girl.[NOM]</ta>
            <ta e="T531" id="Seg_7808" s="T530">INFER</ta>
            <ta e="T532" id="Seg_7809" s="T531">go.out-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T533" id="Seg_7810" s="T532">girl.[NOM]</ta>
            <ta e="T534" id="Seg_7811" s="T533">go.out-CVB</ta>
            <ta e="T535" id="Seg_7812" s="T534">meat-ACC</ta>
            <ta e="T536" id="Seg_7813" s="T535">cut-CVB</ta>
            <ta e="T537" id="Seg_7814" s="T536">tent-ILL</ta>
            <ta e="T538" id="Seg_7815" s="T537">INFER</ta>
            <ta e="T539" id="Seg_7816" s="T538">bring-TR-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T540" id="Seg_7817" s="T539">NEG</ta>
            <ta e="T541" id="Seg_7818" s="T540">long</ta>
            <ta e="T542" id="Seg_7819" s="T541">sit-CVB</ta>
            <ta e="T543" id="Seg_7820" s="T542">after</ta>
            <ta e="T544" id="Seg_7821" s="T543">old.man.[NOM]</ta>
            <ta e="T545" id="Seg_7822" s="T544">and</ta>
            <ta e="T546" id="Seg_7823" s="T545">guy.[NOM]</ta>
            <ta e="T547" id="Seg_7824" s="T546">tent-ILL</ta>
            <ta e="T548" id="Seg_7825" s="T547">INFER</ta>
            <ta e="T549" id="Seg_7826" s="T548">come.in-PST.NAR-INFER-3DU.S</ta>
            <ta e="T550" id="Seg_7827" s="T549">guy.[NOM]</ta>
            <ta e="T551" id="Seg_7828" s="T550">house.[NOM]</ta>
            <ta e="T552" id="Seg_7829" s="T551">come.in-CVB</ta>
            <ta e="T553" id="Seg_7830" s="T552">door-GEN</ta>
            <ta e="T554" id="Seg_7831" s="T553">opening-ILL</ta>
            <ta e="T555" id="Seg_7832" s="T554">hardly</ta>
            <ta e="T556" id="Seg_7833" s="T555">stop-PFV-EP-INF-be.going.to-PST.NAR.[3SG.S]</ta>
            <ta e="T557" id="Seg_7834" s="T556">old.man.[NOM]</ta>
            <ta e="T558" id="Seg_7835" s="T557">here-hey-%%</ta>
            <ta e="T559" id="Seg_7836" s="T558">INFER</ta>
            <ta e="T560" id="Seg_7837" s="T559">say-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T561" id="Seg_7838" s="T560">you.PL.NOM</ta>
            <ta e="T562" id="Seg_7839" s="T561">what</ta>
            <ta e="T563" id="Seg_7840" s="T562">human.being-EP-ACC</ta>
            <ta e="T564" id="Seg_7841" s="T563">NEG</ta>
            <ta e="T565" id="Seg_7842" s="T564">see-IPFV-2PL</ta>
            <ta e="T566" id="Seg_7843" s="T565">forward</ta>
            <ta e="T567" id="Seg_7844" s="T566">let.go-SUP.2/3PL</ta>
            <ta e="T568" id="Seg_7845" s="T567">why</ta>
            <ta e="T569" id="Seg_7846" s="T568">door-GEN</ta>
            <ta e="T570" id="Seg_7847" s="T569">opening-ILL</ta>
            <ta e="T571" id="Seg_7848" s="T570">stand-TR-DUR-2PL</ta>
            <ta e="T572" id="Seg_7849" s="T571">guy-ACC</ta>
            <ta e="T573" id="Seg_7850" s="T572">forward</ta>
            <ta e="T574" id="Seg_7851" s="T573">old.man-GEN</ta>
            <ta e="T575" id="Seg_7852" s="T574">near-ILL</ta>
            <ta e="T576" id="Seg_7853" s="T575">INFER</ta>
            <ta e="T577" id="Seg_7854" s="T576">sit.down-TR-PST.NAR-INFER-3PL</ta>
            <ta e="T578" id="Seg_7855" s="T577">meat-OBL.3SG-INSTR</ta>
            <ta e="T579" id="Seg_7856" s="T578">INFER</ta>
            <ta e="T580" id="Seg_7857" s="T579">take.out-TR-INCH-PST.NAR-INFER-3PL</ta>
            <ta e="T581" id="Seg_7858" s="T580">diverse-ADJZ</ta>
            <ta e="T582" id="Seg_7859" s="T581">food-ADJZ</ta>
            <ta e="T583" id="Seg_7860" s="T582">INFER</ta>
            <ta e="T584" id="Seg_7861" s="T583">put-PST.NAR-INFER-3PL</ta>
            <ta e="T585" id="Seg_7862" s="T584">two</ta>
            <ta e="T586" id="Seg_7863" s="T585">human.being-DU.[NOM]</ta>
            <ta e="T587" id="Seg_7864" s="T586">outwards</ta>
            <ta e="T588" id="Seg_7865" s="T587">go.out-CVB</ta>
            <ta e="T589" id="Seg_7866" s="T588">spirit-EP-ACC</ta>
            <ta e="T590" id="Seg_7867" s="T589">tent-ILL</ta>
            <ta e="T591" id="Seg_7868" s="T590">INFER</ta>
            <ta e="T592" id="Seg_7869" s="T591">bring-TR-PST.NAR-INFER-3PL</ta>
            <ta e="T593" id="Seg_7870" s="T592">INFER</ta>
            <ta e="T594" id="Seg_7871" s="T593">spirit-EP-ACC</ta>
            <ta e="T595" id="Seg_7872" s="T594">%%-INCH-INFER-3PL</ta>
            <ta e="T596" id="Seg_7873" s="T595">and</ta>
            <ta e="T597" id="Seg_7874" s="T596">eat-EP-FRQ-INF-begin-PST.NAR-INFER-3PL</ta>
            <ta e="T598" id="Seg_7875" s="T597">guy.[NOM]</ta>
            <ta e="T599" id="Seg_7876" s="T598">eat-EP-FRQ-ACTN</ta>
            <ta e="T600" id="Seg_7877" s="T599">in.the.middle-LOC</ta>
            <ta e="T601" id="Seg_7878" s="T600">such-ADVZ</ta>
            <ta e="T602" id="Seg_7879" s="T601">be.heard-US-DUR-IPFV-CO.[3SG.S]</ta>
            <ta e="T603" id="Seg_7880" s="T602">apparently</ta>
            <ta e="T604" id="Seg_7881" s="T603">as.if</ta>
            <ta e="T605" id="Seg_7882" s="T604">Lapta-ADJZ</ta>
            <ta e="T606" id="Seg_7883" s="T605">three</ta>
            <ta e="T607" id="Seg_7884" s="T606">brother-DYA-PL.[NOM]</ta>
            <ta e="T608" id="Seg_7885" s="T607">woman-VBLZ-CVB</ta>
            <ta e="T609" id="Seg_7886" s="T608">(s)he.[NOM]</ta>
            <ta e="T610" id="Seg_7887" s="T609">bring-PST.NAR-3PL</ta>
            <ta e="T611" id="Seg_7888" s="T610">white-rich.man-ADJZ</ta>
            <ta e="T612" id="Seg_7889" s="T611">brother-DYA-PL-ALL</ta>
            <ta e="T613" id="Seg_7890" s="T612">white-rich.man-ADJZ</ta>
            <ta e="T614" id="Seg_7891" s="T613">brother-DYA-PL.[NOM]</ta>
            <ta e="T615" id="Seg_7892" s="T614">one</ta>
            <ta e="T616" id="Seg_7893" s="T615">sister.[NOM]-3PL</ta>
            <ta e="T617" id="Seg_7894" s="T616">be-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T618" id="Seg_7895" s="T617">this-ALL</ta>
            <ta e="T619" id="Seg_7896" s="T618">woman-VBLZ-CVB</ta>
            <ta e="T620" id="Seg_7897" s="T619">come-PST.NAR-3PL</ta>
            <ta e="T621" id="Seg_7898" s="T620">eat-EP-FRQ-CVB</ta>
            <ta e="T622" id="Seg_7899" s="T621">good-ADVZ</ta>
            <ta e="T623" id="Seg_7900" s="T622">go.out-IPFV-INFER-3PL</ta>
            <ta e="T624" id="Seg_7901" s="T623">then</ta>
            <ta e="T625" id="Seg_7902" s="T624">every.which.way</ta>
            <ta e="T626" id="Seg_7903" s="T625">INFER</ta>
            <ta e="T627" id="Seg_7904" s="T626">leave-INF-begin-PST.NAR-INFER-3PL</ta>
            <ta e="T628" id="Seg_7905" s="T627">word-PL.[NOM]</ta>
            <ta e="T629" id="Seg_7906" s="T628">apparently</ta>
            <ta e="T630" id="Seg_7907" s="T629">as.if</ta>
            <ta e="T631" id="Seg_7908" s="T630">such-ADVZ</ta>
            <ta e="T632" id="Seg_7909" s="T631">together</ta>
            <ta e="T633" id="Seg_7910" s="T632">there</ta>
            <ta e="T634" id="Seg_7911" s="T633">put-INFER-3PL</ta>
            <ta e="T635" id="Seg_7912" s="T634">tomorrow</ta>
            <ta e="T636" id="Seg_7913" s="T635">then</ta>
            <ta e="T637" id="Seg_7914" s="T636">human.being-EP-human.being-PL-EP-ADJZ</ta>
            <ta e="T638" id="Seg_7915" s="T637">together</ta>
            <ta e="T639" id="Seg_7916" s="T638">here</ta>
            <ta e="T640" id="Seg_7917" s="T639">sit.down-TR-FUT-3PL</ta>
            <ta e="T641" id="Seg_7918" s="T640">human.being-EP-PL-EP-GEN</ta>
            <ta e="T642" id="Seg_7919" s="T641">go.away-ACTN-TRL</ta>
            <ta e="T643" id="Seg_7920" s="T642">guy.[NOM]</ta>
            <ta e="T644" id="Seg_7921" s="T643">also</ta>
            <ta e="T645" id="Seg_7922" s="T644">home</ta>
            <ta e="T646" id="Seg_7923" s="T645">INFER</ta>
            <ta e="T647" id="Seg_7924" s="T646">go.away-INF-be.going.to-IPFV-%%-INFER.[3SG.S]</ta>
            <ta e="T648" id="Seg_7925" s="T647">outwards</ta>
            <ta e="T649" id="Seg_7926" s="T648">go.out-ACTN-LOC-OBL.3PL</ta>
            <ta e="T650" id="Seg_7927" s="T649">guy.[NOM]</ta>
            <ta e="T651" id="Seg_7928" s="T650">fur.clothing-ACC-3SG</ta>
            <ta e="T652" id="Seg_7929" s="T651">fur.clothing-TR-IPFV-ACTN-LOC</ta>
            <ta e="T653" id="Seg_7930" s="T652">Lapta-ADJZ</ta>
            <ta e="T654" id="Seg_7931" s="T653">most</ta>
            <ta e="T655" id="Seg_7932" s="T654">elder</ta>
            <ta e="T656" id="Seg_7933" s="T655">brother-PL-EP-GEN</ta>
            <ta e="T658" id="Seg_7934" s="T657">INFER</ta>
            <ta e="T659" id="Seg_7935" s="T658">say-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T660" id="Seg_7936" s="T659">guy-ALL</ta>
            <ta e="T661" id="Seg_7937" s="T660">fur.clothing-OBL.2SG</ta>
            <ta e="T662" id="Seg_7938" s="T661">you.SG.GEN</ta>
            <ta e="T663" id="Seg_7939" s="T662">like</ta>
            <ta e="T664" id="Seg_7940" s="T663">that</ta>
            <ta e="T665" id="Seg_7941" s="T664">bailer.for.bailing.out.fish-OBL.2SG-INSTR</ta>
            <ta e="T666" id="Seg_7942" s="T665">but</ta>
            <ta e="T667" id="Seg_7943" s="T666">fur.clothing-GEN-OBL.2SG</ta>
            <ta e="T668" id="Seg_7944" s="T667">head.[NOM]</ta>
            <ta e="T669" id="Seg_7945" s="T668">like</ta>
            <ta e="T670" id="Seg_7946" s="T669">that</ta>
            <ta e="T671" id="Seg_7947" s="T670">tree-ADJZ</ta>
            <ta e="T672" id="Seg_7948" s="T671">ladle-GEN</ta>
            <ta e="T673" id="Seg_7949" s="T672">head.[NOM]</ta>
            <ta e="T674" id="Seg_7950" s="T673">guy.[NOM]</ta>
            <ta e="T675" id="Seg_7951" s="T674">NEG</ta>
            <ta e="T676" id="Seg_7952" s="T675">what-EP-ACC</ta>
            <ta e="T677" id="Seg_7953" s="T676">NEG</ta>
            <ta e="T678" id="Seg_7954" s="T677">INFER</ta>
            <ta e="T679" id="Seg_7955" s="T678">say-PST.NAR-3SG.O</ta>
            <ta e="T680" id="Seg_7956" s="T679">then</ta>
            <ta e="T681" id="Seg_7957" s="T680">start.for-PFV-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T682" id="Seg_7958" s="T681">tent-ILL</ta>
            <ta e="T683" id="Seg_7959" s="T682">hardly</ta>
            <ta e="T684" id="Seg_7960" s="T683">come-PFV.[3SG.S]</ta>
            <ta e="T685" id="Seg_7961" s="T684">mother.[NOM]-3SG</ta>
            <ta e="T686" id="Seg_7962" s="T685">door-GEN</ta>
            <ta e="T687" id="Seg_7963" s="T686">opening-LOC</ta>
            <ta e="T688" id="Seg_7964" s="T687">wait-PTCP.PRS</ta>
            <ta e="T689" id="Seg_7965" s="T688">come-HAB-CVB</ta>
            <ta e="T690" id="Seg_7966" s="T689">stand-HAB.[3SG.S]</ta>
            <ta e="T691" id="Seg_7967" s="T690">son-ACC-3SG</ta>
            <ta e="T692" id="Seg_7968" s="T691">wait-CVB</ta>
            <ta e="T693" id="Seg_7969" s="T692">son.[NOM]</ta>
            <ta e="T694" id="Seg_7970" s="T693">you.SG.NOM</ta>
            <ta e="T695" id="Seg_7971" s="T694">where</ta>
            <ta e="T696" id="Seg_7972" s="T695">thus.much</ta>
            <ta e="T697" id="Seg_7973" s="T696">go.away-EP-CO-2SG.S</ta>
            <ta e="T698" id="Seg_7974" s="T697">I.NOM</ta>
            <ta e="T699" id="Seg_7975" s="T698">be.bored-DRV-ACTN-TRL.1SG</ta>
            <ta e="T700" id="Seg_7976" s="T699">down-ADV.LOC</ta>
            <ta e="T701" id="Seg_7977" s="T700">this</ta>
            <ta e="T702" id="Seg_7978" s="T701">hill.[NOM]</ta>
            <ta e="T703" id="Seg_7979" s="T702">above-LOC</ta>
            <ta e="T704" id="Seg_7980" s="T703">sit-HAB-FRQ-PST.NAR-INFER-1SG.S</ta>
            <ta e="T705" id="Seg_7981" s="T704">this</ta>
            <ta e="T706" id="Seg_7982" s="T705">night-ADV.LOC</ta>
            <ta e="T707" id="Seg_7983" s="T706">guy.[NOM]</ta>
            <ta e="T708" id="Seg_7984" s="T707">INFER</ta>
            <ta e="T709" id="Seg_7985" s="T708">spend.night-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T710" id="Seg_7986" s="T709">next</ta>
            <ta e="T711" id="Seg_7987" s="T710">day.[NOM]</ta>
            <ta e="T712" id="Seg_7988" s="T711">reindeer-ACC-3SG</ta>
            <ta e="T713" id="Seg_7989" s="T712">home</ta>
            <ta e="T714" id="Seg_7990" s="T713">shepherd-CVB</ta>
            <ta e="T715" id="Seg_7991" s="T714">reindeer-GEN-3SG</ta>
            <ta e="T716" id="Seg_7992" s="T715">inside-LOC</ta>
            <ta e="T717" id="Seg_7993" s="T716">look.for-HAB-CVB</ta>
            <ta e="T718" id="Seg_7994" s="T717">two</ta>
            <ta e="T719" id="Seg_7995" s="T718">cat-GEN</ta>
            <ta e="T720" id="Seg_7996" s="T719">hair-EP-ADJZ</ta>
            <ta e="T721" id="Seg_7997" s="T720">bull-DU-GEN</ta>
            <ta e="T722" id="Seg_7998" s="T721">INFER</ta>
            <ta e="T723" id="Seg_7999" s="T722">catch-PST.NAR-INFER-3SG.O</ta>
            <ta e="T724" id="Seg_8000" s="T723">board-PL-EP-CAR</ta>
            <ta e="T725" id="Seg_8001" s="T724">sledge-ACC-3SG</ta>
            <ta e="T726" id="Seg_8002" s="T725">INFER</ta>
            <ta e="T727" id="Seg_8003" s="T726">harness-MOM-DUR-IPFV-TR-INFER.[3SG.S]</ta>
            <ta e="T728" id="Seg_8004" s="T727">leather</ta>
            <ta e="T729" id="Seg_8005" s="T728">overclothes.with.deer.fur.inside-ACC-3SG</ta>
            <ta e="T730" id="Seg_8006" s="T729">and</ta>
            <ta e="T731" id="Seg_8007" s="T730">fur.clothing-ACC-3SG</ta>
            <ta e="T732" id="Seg_8008" s="T731">INFER</ta>
            <ta e="T733" id="Seg_8009" s="T732">put.on-TR-DUR-IPFV-ACTN-ADV.EL</ta>
            <ta e="T734" id="Seg_8010" s="T733">log-tree-ACC-3SG</ta>
            <ta e="T735" id="Seg_8011" s="T734">take-PST.NAR-3SG.O</ta>
            <ta e="T736" id="Seg_8012" s="T735">whether</ta>
            <ta e="T737" id="Seg_8013" s="T736">NEG</ta>
            <ta e="T739" id="Seg_8014" s="T738">such-ADVZ</ta>
            <ta e="T740" id="Seg_8015" s="T739">INFER</ta>
            <ta e="T741" id="Seg_8016" s="T740">start.for-PFV-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T742" id="Seg_8017" s="T741">that-ADJZ</ta>
            <ta e="T743" id="Seg_8018" s="T742">human.being-EP-PL-EP-GEN</ta>
            <ta e="T744" id="Seg_8019" s="T743">tent-ILL</ta>
            <ta e="T745" id="Seg_8020" s="T744">hardly</ta>
            <ta e="T746" id="Seg_8021" s="T745">come-PFV.[3SG.S]</ta>
            <ta e="T747" id="Seg_8022" s="T746">apparently</ta>
            <ta e="T748" id="Seg_8023" s="T747">as.if</ta>
            <ta e="T749" id="Seg_8024" s="T748">Lapta-ADJZ</ta>
            <ta e="T750" id="Seg_8025" s="T749">brother-DYA-PL.[NOM]</ta>
            <ta e="T751" id="Seg_8026" s="T750">earlier</ta>
            <ta e="T752" id="Seg_8027" s="T751">whether</ta>
            <ta e="T753" id="Seg_8028" s="T752">come-CO-3PL</ta>
            <ta e="T754" id="Seg_8029" s="T753">guy.[NOM]</ta>
            <ta e="T755" id="Seg_8030" s="T754">reins.[NOM]-3SG</ta>
            <ta e="T756" id="Seg_8031" s="T755">down</ta>
            <ta e="T757" id="Seg_8032" s="T756">bind-CVB</ta>
            <ta e="T758" id="Seg_8033" s="T757">tent-ILL</ta>
            <ta e="T759" id="Seg_8034" s="T758">INFER</ta>
            <ta e="T760" id="Seg_8035" s="T759">come.in-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T761" id="Seg_8036" s="T760">tent-ILL</ta>
            <ta e="T762" id="Seg_8037" s="T761">come.in-CVB</ta>
            <ta e="T763" id="Seg_8038" s="T762">tent-EP-GEN</ta>
            <ta e="T764" id="Seg_8039" s="T763">opening-ILL</ta>
            <ta e="T765" id="Seg_8040" s="T764">hardly</ta>
            <ta e="T766" id="Seg_8041" s="T765">stand-PFV-CO.[3SG.S]</ta>
            <ta e="T767" id="Seg_8042" s="T766">old.man.[NOM]</ta>
            <ta e="T768" id="Seg_8043" s="T767">yesterday</ta>
            <ta e="T769" id="Seg_8044" s="T768">day.[NOM]</ta>
            <ta e="T770" id="Seg_8045" s="T769">apparently</ta>
            <ta e="T771" id="Seg_8046" s="T770">as.if</ta>
            <ta e="T772" id="Seg_8047" s="T771">here-hey-%%</ta>
            <ta e="T773" id="Seg_8048" s="T772">say.[3SG.S]</ta>
            <ta e="T774" id="Seg_8049" s="T773">you.PL.NOM</ta>
            <ta e="T775" id="Seg_8050" s="T774">what</ta>
            <ta e="T776" id="Seg_8051" s="T775">bad-ADJZ</ta>
            <ta e="T777" id="Seg_8052" s="T776">human.being-EP-ACC</ta>
            <ta e="T778" id="Seg_8053" s="T777">NEG</ta>
            <ta e="T779" id="Seg_8054" s="T778">see-IPFV-2PL</ta>
            <ta e="T780" id="Seg_8055" s="T779">forward</ta>
            <ta e="T781" id="Seg_8056" s="T780">let.go-SUP.2/3PL</ta>
            <ta e="T782" id="Seg_8057" s="T781">guy.[NOM]</ta>
            <ta e="T783" id="Seg_8058" s="T782">give.a.look-PST.NAR-3SG.O</ta>
            <ta e="T784" id="Seg_8059" s="T783">old.man-GEN</ta>
            <ta e="T785" id="Seg_8060" s="T784">near-ILL</ta>
            <ta e="T786" id="Seg_8061" s="T785">place-ACC</ta>
            <ta e="T787" id="Seg_8062" s="T786">every.which.way</ta>
            <ta e="T788" id="Seg_8063" s="T787">INFER</ta>
            <ta e="T789" id="Seg_8064" s="T788">make-INFER-3PL</ta>
            <ta e="T790" id="Seg_8065" s="T789">guy.[NOM]</ta>
            <ta e="T791" id="Seg_8066" s="T790">forward</ta>
            <ta e="T792" id="Seg_8067" s="T791">down</ta>
            <ta e="T793" id="Seg_8068" s="T792">sit.down.[3SG.S]</ta>
            <ta e="T794" id="Seg_8069" s="T793">such-ADVZ</ta>
            <ta e="T795" id="Seg_8070" s="T794">give.a.look-PST.NAR-3SG.O</ta>
            <ta e="T796" id="Seg_8071" s="T795">apparently</ta>
            <ta e="T797" id="Seg_8072" s="T796">as.if</ta>
            <ta e="T798" id="Seg_8073" s="T797">human.being-EP-human.being-PL-EP-ADJZ</ta>
            <ta e="T799" id="Seg_8074" s="T798">together</ta>
            <ta e="T800" id="Seg_8075" s="T799">sit.down-PTCP.PRS</ta>
            <ta e="T801" id="Seg_8076" s="T800">come-DUR-CVB</ta>
            <ta e="T802" id="Seg_8077" s="T801">drink-PST.NAR-3PL</ta>
            <ta e="T803" id="Seg_8078" s="T802">guy.[NOM]</ta>
            <ta e="T804" id="Seg_8079" s="T803">also</ta>
            <ta e="T805" id="Seg_8080" s="T804">this</ta>
            <ta e="T806" id="Seg_8081" s="T805">human.being-EP-OBL.3SG-COM</ta>
            <ta e="T807" id="Seg_8082" s="T806">(s)he</ta>
            <ta e="T808" id="Seg_8083" s="T807">also</ta>
            <ta e="T809" id="Seg_8084" s="T808">INFER</ta>
            <ta e="T810" id="Seg_8085" s="T809">drink-DUR-FRQ-INCH-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T811" id="Seg_8086" s="T810">apparently</ta>
            <ta e="T812" id="Seg_8087" s="T811">as.if</ta>
            <ta e="T813" id="Seg_8088" s="T812">guy-ACC</ta>
            <ta e="T814" id="Seg_8089" s="T813">spirit.[NOM]</ta>
            <ta e="T815" id="Seg_8090" s="T814">INFER</ta>
            <ta e="T816" id="Seg_8091" s="T815">come.in-INFER-3SG.O</ta>
            <ta e="T817" id="Seg_8092" s="T816">spirit.[NOM]</ta>
            <ta e="T818" id="Seg_8093" s="T817">come.in-CVB</ta>
            <ta e="T819" id="Seg_8094" s="T818">guy.[NOM]</ta>
            <ta e="T820" id="Seg_8095" s="T819">fall.[3SG.S]</ta>
            <ta e="T821" id="Seg_8096" s="T820">spirit.[NOM]</ta>
            <ta e="T822" id="Seg_8097" s="T821">come.in-PTCP.PST</ta>
            <ta e="T823" id="Seg_8098" s="T822">human.being.[NOM]</ta>
            <ta e="T824" id="Seg_8099" s="T823">what.[NOM]</ta>
            <ta e="T825" id="Seg_8100" s="T824">what.[NOM]</ta>
            <ta e="T826" id="Seg_8101" s="T825">know-FUT.[3SG.S]</ta>
            <ta e="T827" id="Seg_8102" s="T826">when</ta>
            <ta e="T828" id="Seg_8103" s="T827">NEG</ta>
            <ta e="T829" id="Seg_8104" s="T828">INFER</ta>
            <ta e="T830" id="Seg_8105" s="T829">lie-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T831" id="Seg_8106" s="T830">one</ta>
            <ta e="T832" id="Seg_8107" s="T831">whole</ta>
            <ta e="T833" id="Seg_8108" s="T832">middle-LOC</ta>
            <ta e="T834" id="Seg_8109" s="T833">guy.[NOM]</ta>
            <ta e="T835" id="Seg_8110" s="T834">up</ta>
            <ta e="T836" id="Seg_8111" s="T835">hardly</ta>
            <ta e="T837" id="Seg_8112" s="T836">leave-CO.[3SG.S]</ta>
            <ta e="T838" id="Seg_8113" s="T837">apparently</ta>
            <ta e="T839" id="Seg_8114" s="T838">as.if</ta>
            <ta e="T840" id="Seg_8115" s="T839">human.being.[NOM]</ta>
            <ta e="T841" id="Seg_8116" s="T840">I.ACC</ta>
            <ta e="T842" id="Seg_8117" s="T841">hit-%%-PTCP.PRS</ta>
            <ta e="T843" id="Seg_8118" s="T842">catch.up-TR-US.[3SG.S]</ta>
            <ta e="T844" id="Seg_8119" s="T843">such-ADVZ</ta>
            <ta e="T845" id="Seg_8120" s="T844">lie-CVB</ta>
            <ta e="T846" id="Seg_8121" s="T845">hear-TR-PST.NAR-3SG.O</ta>
            <ta e="T847" id="Seg_8122" s="T846">what-ADJZ</ta>
            <ta e="T848" id="Seg_8123" s="T847">earth-LOC</ta>
            <ta e="T849" id="Seg_8124" s="T848">that</ta>
            <ta e="T850" id="Seg_8125" s="T849">white-rich.man-PL-EP-GEN</ta>
            <ta e="T851" id="Seg_8126" s="T850">sister.[NOM]</ta>
            <ta e="T852" id="Seg_8127" s="T851">cry-VBLZ-AUD.[3SG.S]</ta>
            <ta e="T853" id="Seg_8128" s="T852">you.PL.NOM</ta>
            <ta e="T854" id="Seg_8129" s="T853">hey</ta>
            <ta e="T855" id="Seg_8130" s="T854">it.is.said</ta>
            <ta e="T856" id="Seg_8131" s="T855">as.if</ta>
            <ta e="T857" id="Seg_8132" s="T856">what.[NOM]</ta>
            <ta e="T858" id="Seg_8133" s="T857">give.a.look-DUR-PST.NAR-INFER-2PL</ta>
            <ta e="T859" id="Seg_8134" s="T858">that</ta>
            <ta e="T860" id="Seg_8135" s="T859">new-ADVZ</ta>
            <ta e="T861" id="Seg_8136" s="T860">come-PTCP.PST</ta>
            <ta e="T862" id="Seg_8137" s="T861">guy-ACC</ta>
            <ta e="T863" id="Seg_8138" s="T862">INFER</ta>
            <ta e="T864" id="Seg_8139" s="T863">hit-INFER-3PL</ta>
            <ta e="T865" id="Seg_8140" s="T864">Lapta-ADJZ</ta>
            <ta e="T866" id="Seg_8141" s="T865">brother-DYA-PL.[NOM]</ta>
            <ta e="T867" id="Seg_8142" s="T866">guy.[NOM]</ta>
            <ta e="T868" id="Seg_8143" s="T867">NEG</ta>
            <ta e="T869" id="Seg_8144" s="T868">where.get.to.[3SG.S]</ta>
            <ta e="T870" id="Seg_8145" s="T869">lie-ACTN.[NOM]</ta>
            <ta e="T871" id="Seg_8146" s="T870">along-LOC</ta>
            <ta e="T872" id="Seg_8147" s="T871">on-LOC-3SG</ta>
            <ta e="T873" id="Seg_8148" s="T872">hit-CVB</ta>
            <ta e="T874" id="Seg_8149" s="T873">lie.down-PTCP.PST</ta>
            <ta e="T875" id="Seg_8150" s="T874">human.being-EP-ACC-3SG</ta>
            <ta e="T876" id="Seg_8151" s="T875">elbow-OBL.3SG-INSTR</ta>
            <ta e="T877" id="Seg_8152" s="T876">every.which.way</ta>
            <ta e="T878" id="Seg_8153" s="T877">INFER</ta>
            <ta e="T879" id="Seg_8154" s="T878">hit-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T880" id="Seg_8155" s="T879">then</ta>
            <ta e="T881" id="Seg_8156" s="T880">two-ORD</ta>
            <ta e="T882" id="Seg_8157" s="T881">every.which.way</ta>
            <ta e="T883" id="Seg_8158" s="T882">throw-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T884" id="Seg_8159" s="T883">apparently</ta>
            <ta e="T885" id="Seg_8160" s="T884">as.if</ta>
            <ta e="T886" id="Seg_8161" s="T885">Nenets-ADJZ</ta>
            <ta e="T887" id="Seg_8162" s="T886">brother-DYA-PL.[NOM]</ta>
            <ta e="T888" id="Seg_8163" s="T887">Lapta-ADJZ</ta>
            <ta e="T889" id="Seg_8164" s="T888">such-ADVZ</ta>
            <ta e="T890" id="Seg_8165" s="T889">die-PST.NAR-3PL</ta>
            <ta e="T891" id="Seg_8166" s="T890">some-somebody-PL-EP-GEN</ta>
            <ta e="T892" id="Seg_8167" s="T891">middle-PROL</ta>
            <ta e="T893" id="Seg_8168" s="T892">into.parts</ta>
            <ta e="T894" id="Seg_8169" s="T893">break-RFL.PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T895" id="Seg_8170" s="T894">some-somebody-PL-EP-GEN</ta>
            <ta e="T896" id="Seg_8171" s="T895">middle-ACC</ta>
            <ta e="T897" id="Seg_8172" s="T896">into.parts</ta>
            <ta e="T898" id="Seg_8173" s="T897">split-RFL.PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T899" id="Seg_8174" s="T898">but</ta>
            <ta e="T900" id="Seg_8175" s="T899">most</ta>
            <ta e="T901" id="Seg_8176" s="T900">young</ta>
            <ta e="T902" id="Seg_8177" s="T901">shaman.wisdom-PROPR-EP-ADJZ</ta>
            <ta e="T903" id="Seg_8178" s="T902">brother-PL-EP-GEN</ta>
            <ta e="T904" id="Seg_8179" s="T903">apparently</ta>
            <ta e="T905" id="Seg_8180" s="T904">as.if</ta>
            <ta e="T906" id="Seg_8181" s="T905">head.[NOM]-3SG</ta>
            <ta e="T907" id="Seg_8182" s="T906">all.the.time</ta>
            <ta e="T908" id="Seg_8183" s="T907">no(t)</ta>
            <ta e="T909" id="Seg_8184" s="T908">but</ta>
            <ta e="T910" id="Seg_8185" s="T909">this</ta>
            <ta e="T911" id="Seg_8186" s="T910">branch-PL-EP-CAR</ta>
            <ta e="T912" id="Seg_8187" s="T911">tent.[NOM]</ta>
            <ta e="T913" id="Seg_8188" s="T912">only</ta>
            <ta e="T914" id="Seg_8189" s="T913">branch-PL-EP-CAR</ta>
            <ta e="T915" id="Seg_8190" s="T914">bottom.[NOM]</ta>
            <ta e="T916" id="Seg_8191" s="T915">stay-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T917" id="Seg_8192" s="T916">tent-EP-sacking.[NOM]</ta>
            <ta e="T918" id="Seg_8193" s="T917">and</ta>
            <ta e="T919" id="Seg_8194" s="T918">tent-EP-skin.[NOM]</ta>
            <ta e="T920" id="Seg_8195" s="T919">tomorrow-day.[NOM]</ta>
            <ta e="T921" id="Seg_8196" s="T920">whole</ta>
            <ta e="T922" id="Seg_8197" s="T921">at.some.distance</ta>
            <ta e="T923" id="Seg_8198" s="T922">aside-ADV.LOC</ta>
            <ta e="T924" id="Seg_8199" s="T923">lie-3PL</ta>
            <ta e="T925" id="Seg_8200" s="T924">up</ta>
            <ta e="T926" id="Seg_8201" s="T925">get.up-CVB</ta>
            <ta e="T927" id="Seg_8202" s="T926">such-ADVZ</ta>
            <ta e="T928" id="Seg_8203" s="T927">every.which.way</ta>
            <ta e="T929" id="Seg_8204" s="T928">give.a.look-PST.NAR-3SG.O</ta>
            <ta e="T930" id="Seg_8205" s="T929">human.being-EP-PL.[NOM]</ta>
            <ta e="T931" id="Seg_8206" s="T930">gather-DUR-CVB</ta>
            <ta e="T932" id="Seg_8207" s="T931">(s)he-EP-ACC</ta>
            <ta e="T933" id="Seg_8208" s="T932">give.a.look-PST.NAR-3PL</ta>
            <ta e="T934" id="Seg_8209" s="T933">but</ta>
            <ta e="T935" id="Seg_8210" s="T934">that</ta>
            <ta e="T936" id="Seg_8211" s="T935">girl.[NOM]</ta>
            <ta e="T937" id="Seg_8212" s="T936">all.the.time</ta>
            <ta e="T938" id="Seg_8213" s="T937">NEG.EX.[3SG.S]</ta>
            <ta e="T939" id="Seg_8214" s="T938">away</ta>
            <ta e="T940" id="Seg_8215" s="T939">go.away-CVB</ta>
            <ta e="T941" id="Seg_8216" s="T940">tent-EP-skin-ACC</ta>
            <ta e="T942" id="Seg_8217" s="T941">up</ta>
            <ta e="T943" id="Seg_8218" s="T942">every.which.way</ta>
            <ta e="T944" id="Seg_8219" s="T943">hardly</ta>
            <ta e="T945" id="Seg_8220" s="T944">open-CO-3SG.O</ta>
            <ta e="T946" id="Seg_8221" s="T945">apparently</ta>
            <ta e="T947" id="Seg_8222" s="T946">girl.[NOM]</ta>
            <ta e="T948" id="Seg_8223" s="T947">from.here</ta>
            <ta e="T949" id="Seg_8224" s="T948">INFER</ta>
            <ta e="T950" id="Seg_8225" s="T949">jump.out-DRV-INFER.[3SG.S]</ta>
            <ta e="T951" id="Seg_8226" s="T950">NEG</ta>
            <ta e="T952" id="Seg_8227" s="T951">where.get.to-3PL</ta>
            <ta e="T953" id="Seg_8228" s="T952">this</ta>
            <ta e="T954" id="Seg_8229" s="T953">here-ADJZ</ta>
            <ta e="T955" id="Seg_8230" s="T954">human.being-EP-PL.[NOM]</ta>
            <ta e="T956" id="Seg_8231" s="T955">new</ta>
            <ta e="T957" id="Seg_8232" s="T956">tent-EP-ACC</ta>
            <ta e="T958" id="Seg_8233" s="T957">-CVB</ta>
            <ta e="T959" id="Seg_8234" s="T958">then</ta>
            <ta e="T960" id="Seg_8235" s="T959">INFER</ta>
            <ta e="T961" id="Seg_8236" s="T960">drink-PFV-FRQ-PST.NAR-INFER-3PL</ta>
            <ta e="T962" id="Seg_8237" s="T961">and</ta>
            <ta e="T963" id="Seg_8238" s="T962">eat-EP-FRQ-PST.NAR-3PL</ta>
            <ta e="T964" id="Seg_8239" s="T963">white-rich.man.[NOM]</ta>
            <ta e="T965" id="Seg_8240" s="T964">old.man.[NOM]</ta>
            <ta e="T966" id="Seg_8241" s="T965">sister-ACC-3SG</ta>
            <ta e="T967" id="Seg_8242" s="T966">this</ta>
            <ta e="T968" id="Seg_8243" s="T967">guy-ALL</ta>
            <ta e="T969" id="Seg_8244" s="T968">INFER</ta>
            <ta e="T970" id="Seg_8245" s="T969">go-CAUS-PST.NAR-INFER-3SG.O</ta>
            <ta e="T971" id="Seg_8246" s="T970">young</ta>
            <ta e="T972" id="Seg_8247" s="T971">human.being-EP-human.being.[NOM]</ta>
            <ta e="T973" id="Seg_8248" s="T972">together</ta>
            <ta e="T974" id="Seg_8249" s="T973">sit.down-DUR-TR-IPFV-CVB</ta>
            <ta e="T975" id="Seg_8250" s="T974">four-TEN.UNIT</ta>
            <ta e="T976" id="Seg_8251" s="T975">ten</ta>
            <ta e="T977" id="Seg_8252" s="T976">day.[NOM]</ta>
            <ta e="T978" id="Seg_8253" s="T977">drink-DUR-DUR-PST.NAR-3PL</ta>
            <ta e="T979" id="Seg_8254" s="T978">human.being-EP-human.being.[NOM]</ta>
            <ta e="T980" id="Seg_8255" s="T979">together</ta>
            <ta e="T981" id="Seg_8256" s="T980">sit.down-TR-RES-DUR-CVB</ta>
            <ta e="T982" id="Seg_8257" s="T981">white-rich.man-ADJZ</ta>
            <ta e="T983" id="Seg_8258" s="T982">brother-DYA-PL.[NOM]</ta>
            <ta e="T984" id="Seg_8259" s="T983">this</ta>
            <ta e="T985" id="Seg_8260" s="T984">guy-COM</ta>
            <ta e="T986" id="Seg_8261" s="T985">to.the.other.side</ta>
            <ta e="T987" id="Seg_8262" s="T986">deer.train-TR-3PL</ta>
            <ta e="T988" id="Seg_8263" s="T987">guy-GEN</ta>
            <ta e="T989" id="Seg_8264" s="T988">tent.[GEN]-3SG</ta>
            <ta e="T990" id="Seg_8265" s="T989">near-ILL</ta>
            <ta e="T991" id="Seg_8266" s="T990">together</ta>
            <ta e="T992" id="Seg_8267" s="T991">aheap</ta>
            <ta e="T993" id="Seg_8268" s="T992">INFER</ta>
            <ta e="T994" id="Seg_8269" s="T993">live-PST.NAR-INFER-3PL</ta>
            <ta e="T995" id="Seg_8270" s="T994">and</ta>
            <ta e="T996" id="Seg_8271" s="T995">now</ta>
            <ta e="T997" id="Seg_8272" s="T996">INDEF3</ta>
            <ta e="T998" id="Seg_8273" s="T997">and</ta>
            <ta e="T999" id="Seg_8274" s="T998">live-DUR-3PL</ta>
            <ta e="T1000" id="Seg_8275" s="T999">tale.[NOM]</ta>
            <ta e="T1001" id="Seg_8276" s="T1000">somebody-PROL</ta>
            <ta e="T1002" id="Seg_8277" s="T1001">to</ta>
            <ta e="T1003" id="Seg_8278" s="T1002">INFER</ta>
            <ta e="T1004" id="Seg_8279" s="T1003">enough.quantity.[NOM]</ta>
            <ta e="T1005" id="Seg_8280" s="T1004">be-PST.NAR-INFER.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T83" id="Seg_8281" s="T82">Лапта-ADJZ</ta>
            <ta e="T84" id="Seg_8282" s="T83">три</ta>
            <ta e="T85" id="Seg_8283" s="T84">брат-DYA.[NOM]-3SG</ta>
            <ta e="T86" id="Seg_8284" s="T85">два</ta>
            <ta e="T87" id="Seg_8285" s="T86">мать-DYA-DU.[NOM]</ta>
            <ta e="T88" id="Seg_8286" s="T87">жить-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T89" id="Seg_8287" s="T88">сын.[NOM]-3SG</ta>
            <ta e="T90" id="Seg_8288" s="T89">затем</ta>
            <ta e="T91" id="Seg_8289" s="T90">человек-OBL.3SG-COR</ta>
            <ta e="T92" id="Seg_8290" s="T91">сила-TRL-PST.NAR.[3SG.S]</ta>
            <ta e="T93" id="Seg_8291" s="T92">сын.[NOM]-3SG</ta>
            <ta e="T94" id="Seg_8292" s="T93">это-ACC</ta>
            <ta e="T95" id="Seg_8293" s="T94">NEG</ta>
            <ta e="T96" id="Seg_8294" s="T95">знать-PST.NAR-3SG.O</ta>
            <ta e="T97" id="Seg_8295" s="T96">отец.[NOM]-3SG</ta>
            <ta e="T98" id="Seg_8296" s="T97">когда</ta>
            <ta e="T99" id="Seg_8297" s="T98">умереть-PST.NAR.[3SG.S]</ta>
            <ta e="T100" id="Seg_8298" s="T99">один</ta>
            <ta e="T101" id="Seg_8299" s="T100">этот.день.[NOM]</ta>
            <ta e="T102" id="Seg_8300" s="T101">сын.[NOM]</ta>
            <ta e="T103" id="Seg_8301" s="T102">мать-OBL.3SG-ALL</ta>
            <ta e="T104" id="Seg_8302" s="T103">вот-эй-%%</ta>
            <ta e="T105" id="Seg_8303" s="T104">сказать.[3SG.S]</ta>
            <ta e="T106" id="Seg_8304" s="T105">мама.[NOM]</ta>
            <ta e="T107" id="Seg_8305" s="T106">я.GEN</ta>
            <ta e="T108" id="Seg_8306" s="T107">отец-GEN.1SG</ta>
            <ta e="T109" id="Seg_8307" s="T108">идти-PTCP.PST</ta>
            <ta e="T110" id="Seg_8308" s="T109">олень.[NOM]-3SG</ta>
            <ta e="T111" id="Seg_8309" s="T110">откуда</ta>
            <ta e="T112" id="Seg_8310" s="T111">я.ALL</ta>
            <ta e="T113" id="Seg_8311" s="T112">виднеться-TR-IMP.2SG.O</ta>
            <ta e="T114" id="Seg_8312" s="T113">а</ta>
            <ta e="T115" id="Seg_8313" s="T114">олень-3SG-ADJZ</ta>
            <ta e="T116" id="Seg_8314" s="T115">мать-DYA-DU.[NOM]</ta>
            <ta e="T117" id="Seg_8315" s="T116">от</ta>
            <ta e="T118" id="Seg_8316" s="T117">настолько</ta>
            <ta e="T119" id="Seg_8317" s="T118">быть-PST.NAR.[3SG.S]</ta>
            <ta e="T120" id="Seg_8318" s="T119">земля-OBL.3SG-INSTR</ta>
            <ta e="T121" id="Seg_8319" s="T120">съесть-PST.NAR-3SG.O</ta>
            <ta e="T122" id="Seg_8320" s="T121">а</ta>
            <ta e="T123" id="Seg_8321" s="T122">олень-OBL.3SG-ADJZ</ta>
            <ta e="T124" id="Seg_8322" s="T123">такой-ADJZ</ta>
            <ta e="T125" id="Seg_8323" s="T124">подстилка-OBL.3SG-ADJZ</ta>
            <ta e="T126" id="Seg_8324" s="T125">быть-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T127" id="Seg_8325" s="T126">кожа.[NOM]-3SG</ta>
            <ta e="T128" id="Seg_8326" s="T127">рыба-GEN</ta>
            <ta e="T129" id="Seg_8327" s="T128">шкура.[NOM]-3SG</ta>
            <ta e="T130" id="Seg_8328" s="T129">вид.[NOM]-3SG</ta>
            <ta e="T131" id="Seg_8329" s="T130">быть-PST.NAR.[3SG.S]</ta>
            <ta e="T132" id="Seg_8330" s="T131">это-ACC-3SG</ta>
            <ta e="T133" id="Seg_8331" s="T132">скрести-MOM-TR-CVB</ta>
            <ta e="T134" id="Seg_8332" s="T133">олень-ACC-3SG-ACC</ta>
            <ta e="T135" id="Seg_8333" s="T134">такой-ADVZ</ta>
            <ta e="T136" id="Seg_8334" s="T135">домой</ta>
            <ta e="T137" id="Seg_8335" s="T136">погнать-INF-собраться-EP-PST.NAR-3PL</ta>
            <ta e="T138" id="Seg_8336" s="T137">мать.[NOM]-3SG</ta>
            <ta e="T139" id="Seg_8337" s="T138">вот-эй-%%</ta>
            <ta e="T140" id="Seg_8338" s="T139">сказать.[3SG.S]</ta>
            <ta e="T141" id="Seg_8339" s="T140">ты.NOM</ta>
            <ta e="T142" id="Seg_8340" s="T141">олень-COM</ta>
            <ta e="T143" id="Seg_8341" s="T142">куда</ta>
            <ta e="T144" id="Seg_8342" s="T143">уйти-CO-2SG.S</ta>
            <ta e="T145" id="Seg_8343" s="T144">сын.[NOM]</ta>
            <ta e="T146" id="Seg_8344" s="T145">этот-эй-%%</ta>
            <ta e="T147" id="Seg_8345" s="T146">сказать.[3SG.S]</ta>
            <ta e="T148" id="Seg_8346" s="T147">я.NOM</ta>
            <ta e="T149" id="Seg_8347" s="T148">NEG</ta>
            <ta e="T150" id="Seg_8348" s="T149">куда</ta>
            <ta e="T151" id="Seg_8349" s="T150">NEG</ta>
            <ta e="T152" id="Seg_8350" s="T151">уйти-FUT-1SG.S</ta>
            <ta e="T153" id="Seg_8351" s="T152">я.NOM</ta>
            <ta e="T154" id="Seg_8352" s="T153">этот</ta>
            <ta e="T155" id="Seg_8353" s="T154">скучать-DRV-ACTN-TRL.1SG</ta>
            <ta e="T156" id="Seg_8354" s="T155">вниз-ADV.LOC</ta>
            <ta e="T157" id="Seg_8355" s="T156">тундра-GEN</ta>
            <ta e="T158" id="Seg_8356" s="T157">видеть-TR-FUT-1SG.S</ta>
            <ta e="T159" id="Seg_8357" s="T158">мать.[NOM]-3SG</ta>
            <ta e="T160" id="Seg_8358" s="T159">NEG</ta>
            <ta e="T161" id="Seg_8359" s="T160">куда.деваться.[3SG.S]</ta>
            <ta e="T162" id="Seg_8360" s="T161">нарта-PROL</ta>
            <ta e="T163" id="Seg_8361" s="T162">внутри-ADV.LOC</ta>
            <ta e="T164" id="Seg_8362" s="T163">развязать-US-MULO-TR-CVB</ta>
            <ta e="T165" id="Seg_8363" s="T164">это-ACC-3SG</ta>
            <ta e="T166" id="Seg_8364" s="T165">толстый-ADJZ</ta>
            <ta e="T167" id="Seg_8365" s="T166">олень-ADJZ</ta>
            <ta e="T168" id="Seg_8366" s="T167">сила-INSTR-ADJZ</ta>
            <ta e="T169" id="Seg_8367" s="T168">верёвка.[NOM]</ta>
            <ta e="T170" id="Seg_8368" s="T169">вниз</ta>
            <ta e="T171" id="Seg_8369" s="T170">взять-INFER-3SG.O</ta>
            <ta e="T172" id="Seg_8370" s="T171">видать</ta>
            <ta e="T173" id="Seg_8371" s="T172">будто</ta>
            <ta e="T174" id="Seg_8372" s="T173">дерево-ADJZ</ta>
            <ta e="T175" id="Seg_8373" s="T174">ковш-GEN</ta>
            <ta e="T176" id="Seg_8374" s="T175">ручка-GEN</ta>
            <ta e="T177" id="Seg_8375" s="T176">толстый</ta>
            <ta e="T178" id="Seg_8376" s="T177">быть-CO</ta>
            <ta e="T179" id="Seg_8377" s="T178">парень-ALL</ta>
            <ta e="T180" id="Seg_8378" s="T179">такой-ADVZ</ta>
            <ta e="T181" id="Seg_8379" s="T180">подсказать-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T182" id="Seg_8380" s="T181">вниз</ta>
            <ta e="T183" id="Seg_8381" s="T182">этот</ta>
            <ta e="T184" id="Seg_8382" s="T183">олень-GEN-OBL.2SG</ta>
            <ta e="T185" id="Seg_8383" s="T184">внутри-LOC</ta>
            <ta e="T186" id="Seg_8384" s="T185">три</ta>
            <ta e="T187" id="Seg_8385" s="T186">пестрый-ADJZ</ta>
            <ta e="T188" id="Seg_8386" s="T187">бык.[NOM]</ta>
            <ta e="T189" id="Seg_8387" s="T188">INFER</ta>
            <ta e="T190" id="Seg_8388" s="T189">быть-INFER.[3SG.S]</ta>
            <ta e="T191" id="Seg_8389" s="T190">один</ta>
            <ta e="T192" id="Seg_8390" s="T191">пестрый-ADJZ</ta>
            <ta e="T193" id="Seg_8391" s="T192">бык.[NOM]</ta>
            <ta e="T194" id="Seg_8392" s="T193">INFER</ta>
            <ta e="T195" id="Seg_8393" s="T194">быть-INFER.[3SG.S]</ta>
            <ta e="T196" id="Seg_8394" s="T195">большой-в.некоторой.степени-ADJZ</ta>
            <ta e="T197" id="Seg_8395" s="T196">это-ACC</ta>
            <ta e="T198" id="Seg_8396" s="T197">хватать-MULO-IMP.2SG.O</ta>
            <ta e="T199" id="Seg_8397" s="T198">тот</ta>
            <ta e="T200" id="Seg_8398" s="T199">нечто-DU.[NOM]</ta>
            <ta e="T201" id="Seg_8399" s="T200">сам.3DU</ta>
            <ta e="T202" id="Seg_8400" s="T201">кричать-CVB</ta>
            <ta e="T203" id="Seg_8401" s="T202">INFER</ta>
            <ta e="T204" id="Seg_8402" s="T203">прийти-IPFV-FUT-3DU.S</ta>
            <ta e="T205" id="Seg_8403" s="T204">парень.[NOM]</ta>
            <ta e="T206" id="Seg_8404" s="T205">верёвка-ACC-3SG</ta>
            <ta e="T207" id="Seg_8405" s="T206">половина</ta>
            <ta e="T208" id="Seg_8406" s="T207">в.меру</ta>
            <ta e="T209" id="Seg_8407" s="T208">INFER</ta>
            <ta e="T210" id="Seg_8408" s="T209">положить-MULO-PST.NAR-INFER-3SG.O</ta>
            <ta e="T211" id="Seg_8409" s="T210">такой-ADVZ</ta>
            <ta e="T212" id="Seg_8410" s="T211">вниз</ta>
            <ta e="T213" id="Seg_8411" s="T212">INFER</ta>
            <ta e="T214" id="Seg_8412" s="T213">отправиться-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T215" id="Seg_8413" s="T214">олень-GEN-3SG</ta>
            <ta e="T216" id="Seg_8414" s="T215">внутри-ADV.LOC</ta>
            <ta e="T217" id="Seg_8415" s="T216">искать-HAB-CVB</ta>
            <ta e="T218" id="Seg_8416" s="T217">тот</ta>
            <ta e="T219" id="Seg_8417" s="T218">мать-GEN-3SG</ta>
            <ta e="T220" id="Seg_8418" s="T219">сказать-PTCP.PST</ta>
            <ta e="T221" id="Seg_8419" s="T220">бык-ACC</ta>
            <ta e="T222" id="Seg_8420" s="T221">INFER</ta>
            <ta e="T223" id="Seg_8421" s="T222">схватить-PST.NAR-INFER-3SG.O</ta>
            <ta e="T224" id="Seg_8422" s="T223">вверх</ta>
            <ta e="T225" id="Seg_8423" s="T224">принести-CO-3SG.O</ta>
            <ta e="T226" id="Seg_8424" s="T225">запрягать-MOM-INF</ta>
            <ta e="T227" id="Seg_8425" s="T226">это-GEN</ta>
            <ta e="T228" id="Seg_8426" s="T227">в.течение</ta>
            <ta e="T229" id="Seg_8427" s="T228">мать.[NOM]-3SG</ta>
            <ta e="T230" id="Seg_8428" s="T229">видать</ta>
            <ta e="T231" id="Seg_8429" s="T230">будто</ta>
            <ta e="T232" id="Seg_8430" s="T231">нарта-PL-EP-GEN</ta>
            <ta e="T233" id="Seg_8431" s="T232">внутри-LOC</ta>
            <ta e="T234" id="Seg_8432" s="T233">такой</ta>
            <ta e="T235" id="Seg_8433" s="T234">нарта.[NOM]</ta>
            <ta e="T236" id="Seg_8434" s="T235">вниз</ta>
            <ta e="T237" id="Seg_8435" s="T236">взять-PST.NAR-INFER-3SG.O</ta>
            <ta e="T238" id="Seg_8436" s="T237">видать</ta>
            <ta e="T239" id="Seg_8437" s="T238">будто</ta>
            <ta e="T240" id="Seg_8438" s="T239">три-десять-EP-ADJZ</ta>
            <ta e="T241" id="Seg_8439" s="T240">мамонт</ta>
            <ta e="T242" id="Seg_8440" s="T241">рог-ADJZ</ta>
            <ta e="T243" id="Seg_8441" s="T242">нога.[NOM]-3SG</ta>
            <ta e="T244" id="Seg_8442" s="T243">парень.[NOM]</ta>
            <ta e="T245" id="Seg_8443" s="T244">нарта-ACC-3SG</ta>
            <ta e="T246" id="Seg_8444" s="T245">INFER</ta>
            <ta e="T247" id="Seg_8445" s="T246">запрячь-MOM-INF-начать-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T248" id="Seg_8446" s="T247">видать</ta>
            <ta e="T249" id="Seg_8447" s="T248">будто</ta>
            <ta e="T250" id="Seg_8448" s="T249">тот</ta>
            <ta e="T251" id="Seg_8449" s="T250">два</ta>
            <ta e="T252" id="Seg_8450" s="T251">пестрый-ADJZ</ta>
            <ta e="T253" id="Seg_8451" s="T252">маленький-в.некоторой.степени-ADJZ</ta>
            <ta e="T254" id="Seg_8452" s="T253">бык-DU-GEN</ta>
            <ta e="T255" id="Seg_8453" s="T254">сам.3DU</ta>
            <ta e="T256" id="Seg_8454" s="T255">кричать-CVB</ta>
            <ta e="T257" id="Seg_8455" s="T256">видать</ta>
            <ta e="T258" id="Seg_8456" s="T257">будто</ta>
            <ta e="T259" id="Seg_8457" s="T258">вверх</ta>
            <ta e="T260" id="Seg_8458" s="T259">INFER</ta>
            <ta e="T261" id="Seg_8459" s="T260">прийти-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T262" id="Seg_8460" s="T261">парень.[NOM]</ta>
            <ta e="T263" id="Seg_8461" s="T262">нарта-ACC-3SG</ta>
            <ta e="T264" id="Seg_8462" s="T263">запрягать-CVB</ta>
            <ta e="T265" id="Seg_8463" s="T264">INFER</ta>
            <ta e="T266" id="Seg_8464" s="T265">закончиться-TR-PST.NAR-INFER-3SG.O</ta>
            <ta e="T267" id="Seg_8465" s="T266">мать.[NOM]-3SG</ta>
            <ta e="T268" id="Seg_8466" s="T267">семь</ta>
            <ta e="T269" id="Seg_8467" s="T268">сустав-EP-ADJZ</ta>
            <ta e="T270" id="Seg_8468" s="T269">мамонт</ta>
            <ta e="T271" id="Seg_8469" s="T270">рог-ADJZ</ta>
            <ta e="T272" id="Seg_8470" s="T271">чурка-дерево-INSTR</ta>
            <ta e="T273" id="Seg_8471" s="T272">INFER</ta>
            <ta e="T274" id="Seg_8472" s="T273">дать-TR-PST.NAR-INFER-3SG.O</ta>
            <ta e="T275" id="Seg_8473" s="T274">пестрый-ADJZ</ta>
            <ta e="T276" id="Seg_8474" s="T275">резать-FRQ-CVB</ta>
            <ta e="T277" id="Seg_8475" s="T276">сделать-PTCP.PST</ta>
            <ta e="T278" id="Seg_8476" s="T277">сокуй-INSTR</ta>
            <ta e="T279" id="Seg_8477" s="T278">INFER</ta>
            <ta e="T280" id="Seg_8478" s="T279">надеть-MOM-TR-PST.NAR-INFER-3SG.O</ta>
            <ta e="T281" id="Seg_8479" s="T280">новый</ta>
            <ta e="T282" id="Seg_8480" s="T281">резать-FRQ-CVB</ta>
            <ta e="T283" id="Seg_8481" s="T282">сделать-PTCP.PST</ta>
            <ta e="T284" id="Seg_8482" s="T283">ненец-ADJZ</ta>
            <ta e="T286" id="Seg_8483" s="T285">INFER</ta>
            <ta e="T287" id="Seg_8484" s="T286">надеть-MOM-TR-PST.NAR-INFER-3SG.O</ta>
            <ta e="T288" id="Seg_8485" s="T287">потом</ta>
            <ta e="T289" id="Seg_8486" s="T288">мать.[NOM]-3SG</ta>
            <ta e="T290" id="Seg_8487" s="T289">такой-ADVZ</ta>
            <ta e="T291" id="Seg_8488" s="T290">просить-PST.NAR-INFER-3SG.O</ta>
            <ta e="T292" id="Seg_8489" s="T291">олень-GEN-3SG</ta>
            <ta e="T293" id="Seg_8490" s="T292">голова.[NOM]</ta>
            <ta e="T294" id="Seg_8491" s="T293">NEG.IMP</ta>
            <ta e="T295" id="Seg_8492" s="T294">двигаться-CAUS-IMP.2SG.O</ta>
            <ta e="T296" id="Seg_8493" s="T295">сам.3PL</ta>
            <ta e="T299" id="Seg_8494" s="T298">таскать-HAB-FUT-3PL</ta>
            <ta e="T300" id="Seg_8495" s="T299">и</ta>
            <ta e="T301" id="Seg_8496" s="T300">домой</ta>
            <ta e="T303" id="Seg_8497" s="T302">привезти-FUT-3PL</ta>
            <ta e="T304" id="Seg_8498" s="T303">парень.[NOM]</ta>
            <ta e="T305" id="Seg_8499" s="T304">вожжи.[NOM]-3SG</ta>
            <ta e="T306" id="Seg_8500" s="T305">развязать-CAUS-CVB</ta>
            <ta e="T307" id="Seg_8501" s="T306">вверх</ta>
            <ta e="T308" id="Seg_8502" s="T307">сесть-CVB</ta>
            <ta e="T309" id="Seg_8503" s="T308">олень-GEN-3SG</ta>
            <ta e="T310" id="Seg_8504" s="T309">голова-ACC</ta>
            <ta e="T311" id="Seg_8505" s="T310">вперёд</ta>
            <ta e="T312" id="Seg_8506" s="T311">INFER</ta>
            <ta e="T313" id="Seg_8507" s="T312">пустить-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T314" id="Seg_8508" s="T313">потом</ta>
            <ta e="T315" id="Seg_8509" s="T314">вперёд</ta>
            <ta e="T316" id="Seg_8510" s="T315">тронуться-ACTN-LOC-OBL.3SG</ta>
            <ta e="T317" id="Seg_8511" s="T316">то.ли</ta>
            <ta e="T318" id="Seg_8512" s="T317">длинный-ADVZ</ta>
            <ta e="T319" id="Seg_8513" s="T318">уйти-IPFV-PST.NAR.[3SG.S]</ta>
            <ta e="T320" id="Seg_8514" s="T319">то.ли</ta>
            <ta e="T321" id="Seg_8515" s="T320">короткий-ADVZ</ta>
            <ta e="T322" id="Seg_8516" s="T321">один</ta>
            <ta e="T323" id="Seg_8517" s="T322">тундра-LOC-ADJZ</ta>
            <ta e="T324" id="Seg_8518" s="T323">бугор</ta>
            <ta e="T325" id="Seg_8519" s="T324">над-ILL</ta>
            <ta e="T326" id="Seg_8520" s="T325">INFER</ta>
            <ta e="T327" id="Seg_8521" s="T326">сесть-PTCP.PRS</ta>
            <ta e="T328" id="Seg_8522" s="T327">быть-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T329" id="Seg_8523" s="T328">бугор</ta>
            <ta e="T330" id="Seg_8524" s="T329">над-LOC</ta>
            <ta e="T331" id="Seg_8525" s="T330">сидеть-CVB</ta>
            <ta e="T332" id="Seg_8526" s="T331">такой-ADVZ</ta>
            <ta e="T333" id="Seg_8527" s="T332">видеть-CO-3SG.O</ta>
            <ta e="T334" id="Seg_8528" s="T333">видать</ta>
            <ta e="T335" id="Seg_8529" s="T334">будто</ta>
            <ta e="T336" id="Seg_8530" s="T335">вниз-ADV.LOC</ta>
            <ta e="T337" id="Seg_8531" s="T336">озеро-GEN</ta>
            <ta e="T338" id="Seg_8532" s="T337">берег-LOC</ta>
            <ta e="T339" id="Seg_8533" s="T338">три</ta>
            <ta e="T340" id="Seg_8534" s="T339">дом.[NOM]</ta>
            <ta e="T341" id="Seg_8535" s="T340">стоять-INFER.[3SG.S]</ta>
            <ta e="T342" id="Seg_8536" s="T341">этот</ta>
            <ta e="T343" id="Seg_8537" s="T342">чум-EP-рядом-EP-LOC</ta>
            <ta e="T344" id="Seg_8538" s="T343">ненец</ta>
            <ta e="T345" id="Seg_8539" s="T344">олень</ta>
            <ta e="T346" id="Seg_8540" s="T345">бегать-MOM-EP-PST.NAR-INFER-3PL</ta>
            <ta e="T347" id="Seg_8541" s="T346">парень.[NOM]</ta>
            <ta e="T348" id="Seg_8542" s="T347">NEG</ta>
            <ta e="T349" id="Seg_8543" s="T348">куда.деваться.[3SG.S]</ta>
            <ta e="T350" id="Seg_8544" s="T349">чум-ILL</ta>
            <ta e="T351" id="Seg_8545" s="T350">повернуть-CVB</ta>
            <ta e="T352" id="Seg_8546" s="T351">нарта-VBLZ-PFV.[3SG.S]</ta>
            <ta e="T353" id="Seg_8547" s="T352">середина-LOC-OBL.3SG</ta>
            <ta e="T354" id="Seg_8548" s="T353">сам.3SG</ta>
            <ta e="T355" id="Seg_8549" s="T354">будто</ta>
            <ta e="T356" id="Seg_8550" s="T355">достаточно</ta>
            <ta e="T357" id="Seg_8551" s="T356">большой</ta>
            <ta e="T358" id="Seg_8552" s="T357">чум.[NOM]</ta>
            <ta e="T359" id="Seg_8553" s="T358">стоять-INFER.[3SG.S]</ta>
            <ta e="T360" id="Seg_8554" s="T359">этот</ta>
            <ta e="T361" id="Seg_8555" s="T360">большой</ta>
            <ta e="T362" id="Seg_8556" s="T361">чум-EP-PL-EP-GEN</ta>
            <ta e="T363" id="Seg_8557" s="T362">напротив</ta>
            <ta e="T364" id="Seg_8558" s="T363">вниз</ta>
            <ta e="T365" id="Seg_8559" s="T364">остановиться-FRQ-PFV-CO-3SG.O</ta>
            <ta e="T366" id="Seg_8560" s="T365">вниз</ta>
            <ta e="T367" id="Seg_8561" s="T366">остановиться-FRQ-PFV-DUR-CVB</ta>
            <ta e="T368" id="Seg_8562" s="T367">нарта-GEN-3SG</ta>
            <ta e="T369" id="Seg_8563" s="T368">внутри-LOC</ta>
            <ta e="T370" id="Seg_8564" s="T369">сидеть.[3SG.S]</ta>
            <ta e="T371" id="Seg_8565" s="T370">чум-EL</ta>
            <ta e="T372" id="Seg_8566" s="T371">наружу</ta>
            <ta e="T373" id="Seg_8567" s="T372">человек-EP-PL.[NOM]</ta>
            <ta e="T374" id="Seg_8568" s="T373">некто-PL.[NOM]</ta>
            <ta e="T375" id="Seg_8569" s="T374">выйти-PST.NAR-INFER-3PL</ta>
            <ta e="T376" id="Seg_8570" s="T375">один</ta>
            <ta e="T377" id="Seg_8571" s="T376">старик.[NOM]</ta>
            <ta e="T378" id="Seg_8572" s="T377">видать</ta>
            <ta e="T379" id="Seg_8573" s="T378">будто</ta>
            <ta e="T380" id="Seg_8574" s="T379">парень-ALL</ta>
            <ta e="T381" id="Seg_8575" s="T380">прийти-CVB</ta>
            <ta e="T382" id="Seg_8576" s="T381">вот-эй-%%</ta>
            <ta e="T383" id="Seg_8577" s="T382">сказать-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T384" id="Seg_8578" s="T383">что-ADJZ</ta>
            <ta e="T385" id="Seg_8579" s="T384">земля-GEN</ta>
            <ta e="T386" id="Seg_8580" s="T385">человек-EP-2SG.S</ta>
            <ta e="T387" id="Seg_8581" s="T386">стать-2SG.S</ta>
            <ta e="T388" id="Seg_8582" s="T387">парень.[NOM]</ta>
            <ta e="T389" id="Seg_8583" s="T388">вот-эй-%%</ta>
            <ta e="T390" id="Seg_8584" s="T389">сказать-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T391" id="Seg_8585" s="T390">я.NOM</ta>
            <ta e="T392" id="Seg_8586" s="T391">NEG</ta>
            <ta e="T393" id="Seg_8587" s="T392">человек-EP-ACC</ta>
            <ta e="T394" id="Seg_8588" s="T393">NEG</ta>
            <ta e="T395" id="Seg_8589" s="T394">знать-1SG.O</ta>
            <ta e="T396" id="Seg_8590" s="T395">NEG</ta>
            <ta e="T397" id="Seg_8591" s="T396">что-ACC</ta>
            <ta e="T398" id="Seg_8592" s="T397">NEG</ta>
            <ta e="T399" id="Seg_8593" s="T398">знать-1SG.O</ta>
            <ta e="T400" id="Seg_8594" s="T399">друг-CAR</ta>
            <ta e="T401" id="Seg_8595" s="T400">жить-ACTN-ADJZ</ta>
            <ta e="T402" id="Seg_8596" s="T401">земля-GEN</ta>
            <ta e="T403" id="Seg_8597" s="T402">из-ADV.EL</ta>
            <ta e="T404" id="Seg_8598" s="T403">а</ta>
            <ta e="T405" id="Seg_8599" s="T404">ты.NOM</ta>
            <ta e="T406" id="Seg_8600" s="T405">будто</ta>
            <ta e="T407" id="Seg_8601" s="T406">что.[NOM]</ta>
            <ta e="T408" id="Seg_8602" s="T407">старик-CO-2SG.S</ta>
            <ta e="T409" id="Seg_8603" s="T408">стать-2SG.S</ta>
            <ta e="T410" id="Seg_8604" s="T409">этот</ta>
            <ta e="T411" id="Seg_8605" s="T410">старик.[NOM]</ta>
            <ta e="T412" id="Seg_8606" s="T411">такой-ADVZ</ta>
            <ta e="T413" id="Seg_8607" s="T412">сказать-CO-3SG.O</ta>
            <ta e="T414" id="Seg_8608" s="T413">мы.PL.NOM</ta>
            <ta e="T415" id="Seg_8609" s="T414">будто</ta>
            <ta e="T416" id="Seg_8610" s="T415">три</ta>
            <ta e="T417" id="Seg_8611" s="T416">белый-богач-ADJZ</ta>
            <ta e="T418" id="Seg_8612" s="T417">брат-DYA.[NOM]-1PL</ta>
            <ta e="T419" id="Seg_8613" s="T418">самый</ta>
            <ta e="T420" id="Seg_8614" s="T419">большой</ta>
            <ta e="T421" id="Seg_8615" s="T420">белый-богач.[NOM]</ta>
            <ta e="T422" id="Seg_8616" s="T421">так</ta>
            <ta e="T423" id="Seg_8617" s="T422">я.NOM</ta>
            <ta e="T424" id="Seg_8618" s="T423">быть-DUR-INFER-1SG.S</ta>
            <ta e="T425" id="Seg_8619" s="T424">тот</ta>
            <ta e="T426" id="Seg_8620" s="T425">наружу</ta>
            <ta e="T427" id="Seg_8621" s="T426">выйти-PTCP.PST</ta>
            <ta e="T428" id="Seg_8622" s="T427">человек-EP-PL.[NOM]</ta>
            <ta e="T429" id="Seg_8623" s="T428">видать</ta>
            <ta e="T430" id="Seg_8624" s="T429">будто</ta>
            <ta e="T431" id="Seg_8625" s="T430">олень-ADJZ</ta>
            <ta e="T432" id="Seg_8626" s="T431">хватать-INFER-3PL</ta>
            <ta e="T433" id="Seg_8627" s="T432">этот</ta>
            <ta e="T434" id="Seg_8628" s="T433">олень-PL-EP-GEN</ta>
            <ta e="T435" id="Seg_8629" s="T434">внутри-ADV.LOC</ta>
            <ta e="T436" id="Seg_8630" s="T435">один</ta>
            <ta e="T437" id="Seg_8631" s="T436">рог-CAR-ADJZ</ta>
            <ta e="T438" id="Seg_8632" s="T437">ненец-ADJZ</ta>
            <ta e="T439" id="Seg_8633" s="T438">яловая.важенка-ADJZ</ta>
            <ta e="T440" id="Seg_8634" s="T439">важенка.[NOM]</ta>
            <ta e="T441" id="Seg_8635" s="T440">хватать-INFER-3PL</ta>
            <ta e="T442" id="Seg_8636" s="T441">такой-ADVZ</ta>
            <ta e="T443" id="Seg_8637" s="T442">парень.[NOM]</ta>
            <ta e="T444" id="Seg_8638" s="T443">взглянуть-PST.NAR-3SG.O</ta>
            <ta e="T445" id="Seg_8639" s="T444">схватить-INF</ta>
            <ta e="T446" id="Seg_8640" s="T445">не.мочь-PST.NAR-3PL</ta>
            <ta e="T447" id="Seg_8641" s="T446">парень.[NOM]</ta>
            <ta e="T448" id="Seg_8642" s="T447">нарта-GEN-3SG</ta>
            <ta e="T449" id="Seg_8643" s="T448">внутри-LOC</ta>
            <ta e="T450" id="Seg_8644" s="T449">сидеть-CVB</ta>
            <ta e="T451" id="Seg_8645" s="T450">верёвка-ACC</ta>
            <ta e="T452" id="Seg_8646" s="T451">тот</ta>
            <ta e="T453" id="Seg_8647" s="T452">положить-MULO-DUR-IPFV-ACTN-ADV.EL</ta>
            <ta e="T454" id="Seg_8648" s="T453">важенка-ACC</ta>
            <ta e="T455" id="Seg_8649" s="T454">парень-GEN</ta>
            <ta e="T456" id="Seg_8650" s="T455">рядом-PROL</ta>
            <ta e="T457" id="Seg_8651" s="T456">INFER</ta>
            <ta e="T458" id="Seg_8652" s="T457">перегнать-IPFV-FRQ-PTCP.PRS</ta>
            <ta e="T459" id="Seg_8653" s="T458">погнать-PST.NAR-INFER-3PL</ta>
            <ta e="T460" id="Seg_8654" s="T459">парень.[NOM]</ta>
            <ta e="T461" id="Seg_8655" s="T460">важенка-ACC</ta>
            <ta e="T462" id="Seg_8656" s="T461">принести-FRQ-CAUS-CVB</ta>
            <ta e="T463" id="Seg_8657" s="T462">схватить-DUR-IPFV-CVB</ta>
            <ta e="T464" id="Seg_8658" s="T463">верёвка.[NOM]</ta>
            <ta e="T465" id="Seg_8659" s="T464">едва</ta>
            <ta e="T466" id="Seg_8660" s="T465">дернуть-PST.NAR-3SG.O</ta>
            <ta e="T467" id="Seg_8661" s="T466">важенка-GEN</ta>
            <ta e="T468" id="Seg_8662" s="T467">срезать-INF</ta>
            <ta e="T469" id="Seg_8663" s="T468">тот</ta>
            <ta e="T470" id="Seg_8664" s="T469">смочь-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T471" id="Seg_8665" s="T470">парень.[NOM]</ta>
            <ta e="T472" id="Seg_8666" s="T471">верёвка-ACC-3SG</ta>
            <ta e="T473" id="Seg_8667" s="T472">вверх</ta>
            <ta e="T474" id="Seg_8668" s="T473">INFER</ta>
            <ta e="T475" id="Seg_8669" s="T474">положить-MULO-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T476" id="Seg_8670" s="T475">вот-эй-%%</ta>
            <ta e="T477" id="Seg_8671" s="T476">сказать-PST.NAR.[3SG.S]</ta>
            <ta e="T478" id="Seg_8672" s="T477">этот</ta>
            <ta e="T479" id="Seg_8673" s="T478">верёвка-ADJZ</ta>
            <ta e="T480" id="Seg_8674" s="T479">тот</ta>
            <ta e="T481" id="Seg_8675" s="T480">кровь-INSTR</ta>
            <ta e="T482" id="Seg_8676" s="T481">почему</ta>
            <ta e="T483" id="Seg_8677" s="T482">писать-MULO-TR-1SG.O</ta>
            <ta e="T484" id="Seg_8678" s="T483">этот</ta>
            <ta e="T485" id="Seg_8679" s="T484">человек-EP-PL.[NOM]</ta>
            <ta e="T486" id="Seg_8680" s="T485">олень-ACC</ta>
            <ta e="T487" id="Seg_8681" s="T486">тот</ta>
            <ta e="T488" id="Seg_8682" s="T487">ободрать-PST.NAR-INFER-3PL</ta>
            <ta e="T489" id="Seg_8683" s="T488">ненец-ADJZ</ta>
            <ta e="T490" id="Seg_8684" s="T489">человек-EP-PL.[NOM]</ta>
            <ta e="T491" id="Seg_8685" s="T490">наружу</ta>
            <ta e="T492" id="Seg_8686" s="T491">строганина-TR-INF-начать-PST.NAR-INFER-3PL</ta>
            <ta e="T493" id="Seg_8687" s="T492">парень.[NOM]</ta>
            <ta e="T494" id="Seg_8688" s="T493">вот-эй-%%</ta>
            <ta e="T495" id="Seg_8689" s="T494">INFER</ta>
            <ta e="T496" id="Seg_8690" s="T495">идти-CAUS-PST.NAR-INFER-3PL</ta>
            <ta e="T497" id="Seg_8691" s="T496">ты.NOM</ta>
            <ta e="T498" id="Seg_8692" s="T497">эй</ta>
            <ta e="T499" id="Seg_8693" s="T498">мы.PL.COM</ta>
            <ta e="T500" id="Seg_8694" s="T499">строганина-TR-IMP.2SG.S</ta>
            <ta e="T501" id="Seg_8695" s="T500">парень.[NOM]</ta>
            <ta e="T502" id="Seg_8696" s="T501">вот-эй-%%</ta>
            <ta e="T503" id="Seg_8697" s="T502">INFER</ta>
            <ta e="T504" id="Seg_8698" s="T503">сказать-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T505" id="Seg_8699" s="T504">я.NOM</ta>
            <ta e="T506" id="Seg_8700" s="T505">строганина-TR-INF</ta>
            <ta e="T507" id="Seg_8701" s="T506">NEG</ta>
            <ta e="T508" id="Seg_8702" s="T507">уметь-1SG.S</ta>
            <ta e="T509" id="Seg_8703" s="T508">тот</ta>
            <ta e="T510" id="Seg_8704" s="T509">белый</ta>
            <ta e="T511" id="Seg_8705" s="T510">богач-ADJZ</ta>
            <ta e="T512" id="Seg_8706" s="T511">старик.[NOM]</ta>
            <ta e="T513" id="Seg_8707" s="T512">вот-эй-%%</ta>
            <ta e="T514" id="Seg_8708" s="T513">INFER</ta>
            <ta e="T515" id="Seg_8709" s="T514">сказать-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T516" id="Seg_8710" s="T515">вы.PL.NOM</ta>
            <ta e="T517" id="Seg_8711" s="T516">почему</ta>
            <ta e="T518" id="Seg_8712" s="T517">сидеть-2PL</ta>
            <ta e="T519" id="Seg_8713" s="T518">мясо-PL.[NOM]</ta>
            <ta e="T520" id="Seg_8714" s="T519">чум-ILL</ta>
            <ta e="T521" id="Seg_8715" s="T520">таскать-TR-CVB</ta>
            <ta e="T522" id="Seg_8716" s="T521">свариться-CAUS-IMP.2PL</ta>
            <ta e="T523" id="Seg_8717" s="T522">парень.[NOM]</ta>
            <ta e="T524" id="Seg_8718" s="T523">взглянуть-PST.NAR-3SG.O</ta>
            <ta e="T525" id="Seg_8719" s="T524">видать</ta>
            <ta e="T526" id="Seg_8720" s="T525">будто</ta>
            <ta e="T527" id="Seg_8721" s="T526">чум-EL</ta>
            <ta e="T528" id="Seg_8722" s="T527">наружу</ta>
            <ta e="T529" id="Seg_8723" s="T528">один</ta>
            <ta e="T530" id="Seg_8724" s="T529">девушка.[NOM]</ta>
            <ta e="T531" id="Seg_8725" s="T530">INFER</ta>
            <ta e="T532" id="Seg_8726" s="T531">выйти-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T533" id="Seg_8727" s="T532">девушка.[NOM]</ta>
            <ta e="T534" id="Seg_8728" s="T533">выйти-CVB</ta>
            <ta e="T535" id="Seg_8729" s="T534">мясо-ACC</ta>
            <ta e="T536" id="Seg_8730" s="T535">рубить-CVB</ta>
            <ta e="T537" id="Seg_8731" s="T536">чум-ILL</ta>
            <ta e="T538" id="Seg_8732" s="T537">INFER</ta>
            <ta e="T539" id="Seg_8733" s="T538">занести-TR-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T540" id="Seg_8734" s="T539">NEG</ta>
            <ta e="T541" id="Seg_8735" s="T540">долго</ta>
            <ta e="T542" id="Seg_8736" s="T541">сидеть-CVB</ta>
            <ta e="T543" id="Seg_8737" s="T542">после</ta>
            <ta e="T544" id="Seg_8738" s="T543">старик.[NOM]</ta>
            <ta e="T545" id="Seg_8739" s="T544">и</ta>
            <ta e="T546" id="Seg_8740" s="T545">парень.[NOM]</ta>
            <ta e="T547" id="Seg_8741" s="T546">чум-ILL</ta>
            <ta e="T548" id="Seg_8742" s="T547">INFER</ta>
            <ta e="T549" id="Seg_8743" s="T548">войти-PST.NAR-INFER-3DU.S</ta>
            <ta e="T550" id="Seg_8744" s="T549">парень.[NOM]</ta>
            <ta e="T551" id="Seg_8745" s="T550">дом.[NOM]</ta>
            <ta e="T552" id="Seg_8746" s="T551">войти-CVB</ta>
            <ta e="T553" id="Seg_8747" s="T552">дверь-GEN</ta>
            <ta e="T554" id="Seg_8748" s="T553">отверстие-ILL</ta>
            <ta e="T555" id="Seg_8749" s="T554">едва</ta>
            <ta e="T556" id="Seg_8750" s="T555">остановиться-PFV-EP-INF-собраться-PST.NAR.[3SG.S]</ta>
            <ta e="T557" id="Seg_8751" s="T556">старик.[NOM]</ta>
            <ta e="T558" id="Seg_8752" s="T557">вот-эй-%%</ta>
            <ta e="T559" id="Seg_8753" s="T558">INFER</ta>
            <ta e="T560" id="Seg_8754" s="T559">сказать-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T561" id="Seg_8755" s="T560">вы.PL.NOM</ta>
            <ta e="T562" id="Seg_8756" s="T561">что</ta>
            <ta e="T563" id="Seg_8757" s="T562">человек-EP-ACC</ta>
            <ta e="T564" id="Seg_8758" s="T563">NEG</ta>
            <ta e="T565" id="Seg_8759" s="T564">видеть-IPFV-2PL</ta>
            <ta e="T566" id="Seg_8760" s="T565">вперёд</ta>
            <ta e="T567" id="Seg_8761" s="T566">пустить-SUP.2/3PL</ta>
            <ta e="T568" id="Seg_8762" s="T567">почему</ta>
            <ta e="T569" id="Seg_8763" s="T568">дверь-GEN</ta>
            <ta e="T570" id="Seg_8764" s="T569">отверстие-ILL</ta>
            <ta e="T571" id="Seg_8765" s="T570">стоять-TR-DUR-2PL</ta>
            <ta e="T572" id="Seg_8766" s="T571">парень-ACC</ta>
            <ta e="T573" id="Seg_8767" s="T572">вперёд</ta>
            <ta e="T574" id="Seg_8768" s="T573">старик-GEN</ta>
            <ta e="T575" id="Seg_8769" s="T574">около-ILL</ta>
            <ta e="T576" id="Seg_8770" s="T575">INFER</ta>
            <ta e="T577" id="Seg_8771" s="T576">сесть-TR-PST.NAR-INFER-3PL</ta>
            <ta e="T578" id="Seg_8772" s="T577">мясо-OBL.3SG-INSTR</ta>
            <ta e="T579" id="Seg_8773" s="T578">INFER</ta>
            <ta e="T580" id="Seg_8774" s="T579">вытащить-TR-INCH-PST.NAR-INFER-3PL</ta>
            <ta e="T581" id="Seg_8775" s="T580">разнообразный-ADJZ</ta>
            <ta e="T582" id="Seg_8776" s="T581">еда-ADJZ</ta>
            <ta e="T583" id="Seg_8777" s="T582">INFER</ta>
            <ta e="T584" id="Seg_8778" s="T583">положить-PST.NAR-INFER-3PL</ta>
            <ta e="T585" id="Seg_8779" s="T584">два</ta>
            <ta e="T586" id="Seg_8780" s="T585">человек-DU.[NOM]</ta>
            <ta e="T587" id="Seg_8781" s="T586">наружу</ta>
            <ta e="T588" id="Seg_8782" s="T587">выйти-CVB</ta>
            <ta e="T589" id="Seg_8783" s="T588">спирт-EP-ACC</ta>
            <ta e="T590" id="Seg_8784" s="T589">чум-ILL</ta>
            <ta e="T591" id="Seg_8785" s="T590">INFER</ta>
            <ta e="T592" id="Seg_8786" s="T591">занести-TR-PST.NAR-INFER-3PL</ta>
            <ta e="T593" id="Seg_8787" s="T592">INFER</ta>
            <ta e="T594" id="Seg_8788" s="T593">спирт-EP-ACC</ta>
            <ta e="T595" id="Seg_8789" s="T594">%%-INCH-INFER-3PL</ta>
            <ta e="T596" id="Seg_8790" s="T595">и</ta>
            <ta e="T597" id="Seg_8791" s="T596">съесть-EP-FRQ-INF-начать-PST.NAR-INFER-3PL</ta>
            <ta e="T598" id="Seg_8792" s="T597">парень.[NOM]</ta>
            <ta e="T599" id="Seg_8793" s="T598">съесть-EP-FRQ-ACTN</ta>
            <ta e="T600" id="Seg_8794" s="T599">посередине-LOC</ta>
            <ta e="T601" id="Seg_8795" s="T600">такой-ADVZ</ta>
            <ta e="T602" id="Seg_8796" s="T601">слышаться-US-DUR-IPFV-CO.[3SG.S]</ta>
            <ta e="T603" id="Seg_8797" s="T602">видать</ta>
            <ta e="T604" id="Seg_8798" s="T603">будто</ta>
            <ta e="T605" id="Seg_8799" s="T604">Лапта-ADJZ</ta>
            <ta e="T606" id="Seg_8800" s="T605">три</ta>
            <ta e="T607" id="Seg_8801" s="T606">брат-DYA-PL.[NOM]</ta>
            <ta e="T608" id="Seg_8802" s="T607">женщина-VBLZ-CVB</ta>
            <ta e="T609" id="Seg_8803" s="T608">он(а).[NOM]</ta>
            <ta e="T610" id="Seg_8804" s="T609">привезти-PST.NAR-3PL</ta>
            <ta e="T611" id="Seg_8805" s="T610">белый-богач-ADJZ</ta>
            <ta e="T612" id="Seg_8806" s="T611">брат-DYA-PL-ALL</ta>
            <ta e="T613" id="Seg_8807" s="T612">белый-богач-ADJZ</ta>
            <ta e="T614" id="Seg_8808" s="T613">брат-DYA-PL.[NOM]</ta>
            <ta e="T615" id="Seg_8809" s="T614">один</ta>
            <ta e="T616" id="Seg_8810" s="T615">сестра.[NOM]-3PL</ta>
            <ta e="T617" id="Seg_8811" s="T616">быть-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T618" id="Seg_8812" s="T617">это-ALL</ta>
            <ta e="T619" id="Seg_8813" s="T618">женщина-VBLZ-CVB</ta>
            <ta e="T620" id="Seg_8814" s="T619">прийти-PST.NAR-3PL</ta>
            <ta e="T621" id="Seg_8815" s="T620">съесть-EP-FRQ-CVB</ta>
            <ta e="T622" id="Seg_8816" s="T621">хороший-ADVZ</ta>
            <ta e="T623" id="Seg_8817" s="T622">выйти-IPFV-INFER-3PL</ta>
            <ta e="T624" id="Seg_8818" s="T623">потом</ta>
            <ta e="T625" id="Seg_8819" s="T624">в.разные.стороны</ta>
            <ta e="T626" id="Seg_8820" s="T625">INFER</ta>
            <ta e="T627" id="Seg_8821" s="T626">отправиться-INF-начать-PST.NAR-INFER-3PL</ta>
            <ta e="T628" id="Seg_8822" s="T627">слово-PL.[NOM]</ta>
            <ta e="T629" id="Seg_8823" s="T628">видать</ta>
            <ta e="T630" id="Seg_8824" s="T629">будто</ta>
            <ta e="T631" id="Seg_8825" s="T630">такой-ADVZ</ta>
            <ta e="T632" id="Seg_8826" s="T631">вместе</ta>
            <ta e="T633" id="Seg_8827" s="T632">туда</ta>
            <ta e="T634" id="Seg_8828" s="T633">положить-INFER-3PL</ta>
            <ta e="T635" id="Seg_8829" s="T634">завтра</ta>
            <ta e="T636" id="Seg_8830" s="T635">затем</ta>
            <ta e="T637" id="Seg_8831" s="T636">человек-EP-человек-PL-EP-ADJZ</ta>
            <ta e="T638" id="Seg_8832" s="T637">вместе</ta>
            <ta e="T639" id="Seg_8833" s="T638">вот</ta>
            <ta e="T640" id="Seg_8834" s="T639">сесть-TR-FUT-3PL</ta>
            <ta e="T641" id="Seg_8835" s="T640">человек-EP-PL-EP-GEN</ta>
            <ta e="T642" id="Seg_8836" s="T641">уйти-ACTN-TRL</ta>
            <ta e="T643" id="Seg_8837" s="T642">парень.[NOM]</ta>
            <ta e="T644" id="Seg_8838" s="T643">тоже</ta>
            <ta e="T645" id="Seg_8839" s="T644">домой</ta>
            <ta e="T646" id="Seg_8840" s="T645">INFER</ta>
            <ta e="T647" id="Seg_8841" s="T646">уйти-INF-собраться-IPFV-%%-INFER.[3SG.S]</ta>
            <ta e="T648" id="Seg_8842" s="T647">наружу</ta>
            <ta e="T649" id="Seg_8843" s="T648">выйти-ACTN-LOC-OBL.3PL</ta>
            <ta e="T650" id="Seg_8844" s="T649">парень.[NOM]</ta>
            <ta e="T651" id="Seg_8845" s="T650">сокуй-ACC-3SG</ta>
            <ta e="T652" id="Seg_8846" s="T651">сокуй-TR-IPFV-ACTN-LOC</ta>
            <ta e="T653" id="Seg_8847" s="T652">Лапта-ADJZ</ta>
            <ta e="T654" id="Seg_8848" s="T653">самый</ta>
            <ta e="T655" id="Seg_8849" s="T654">старший</ta>
            <ta e="T656" id="Seg_8850" s="T655">брат-PL-EP-GEN</ta>
            <ta e="T658" id="Seg_8851" s="T657">INFER</ta>
            <ta e="T659" id="Seg_8852" s="T658">сказать-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T660" id="Seg_8853" s="T659">парень-ALL</ta>
            <ta e="T661" id="Seg_8854" s="T660">сокуй-OBL.2SG</ta>
            <ta e="T662" id="Seg_8855" s="T661">ты.GEN</ta>
            <ta e="T663" id="Seg_8856" s="T662">словно</ta>
            <ta e="T664" id="Seg_8857" s="T663">тот</ta>
            <ta e="T665" id="Seg_8858" s="T664">морда-OBL.2SG-INSTR</ta>
            <ta e="T666" id="Seg_8859" s="T665">а</ta>
            <ta e="T667" id="Seg_8860" s="T666">сокуй-GEN-OBL.2SG</ta>
            <ta e="T668" id="Seg_8861" s="T667">голова.[NOM]</ta>
            <ta e="T669" id="Seg_8862" s="T668">словно</ta>
            <ta e="T670" id="Seg_8863" s="T669">тот</ta>
            <ta e="T671" id="Seg_8864" s="T670">дерево-ADJZ</ta>
            <ta e="T672" id="Seg_8865" s="T671">половник-GEN</ta>
            <ta e="T673" id="Seg_8866" s="T672">голова.[NOM]</ta>
            <ta e="T674" id="Seg_8867" s="T673">парень.[NOM]</ta>
            <ta e="T675" id="Seg_8868" s="T674">NEG</ta>
            <ta e="T676" id="Seg_8869" s="T675">что-EP-ACC</ta>
            <ta e="T677" id="Seg_8870" s="T676">NEG</ta>
            <ta e="T678" id="Seg_8871" s="T677">INFER</ta>
            <ta e="T679" id="Seg_8872" s="T678">сказать-PST.NAR-3SG.O</ta>
            <ta e="T680" id="Seg_8873" s="T679">потом</ta>
            <ta e="T681" id="Seg_8874" s="T680">тронуться-PFV-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T682" id="Seg_8875" s="T681">чум-ILL</ta>
            <ta e="T683" id="Seg_8876" s="T682">едва</ta>
            <ta e="T684" id="Seg_8877" s="T683">прийти-PFV.[3SG.S]</ta>
            <ta e="T685" id="Seg_8878" s="T684">мать.[NOM]-3SG</ta>
            <ta e="T686" id="Seg_8879" s="T685">дверь-GEN</ta>
            <ta e="T687" id="Seg_8880" s="T686">отверстие-LOC</ta>
            <ta e="T688" id="Seg_8881" s="T687">подождать-PTCP.PRS</ta>
            <ta e="T689" id="Seg_8882" s="T688">прийти-HAB-CVB</ta>
            <ta e="T690" id="Seg_8883" s="T689">стоять-HAB.[3SG.S]</ta>
            <ta e="T691" id="Seg_8884" s="T690">сын-ACC-3SG</ta>
            <ta e="T692" id="Seg_8885" s="T691">подождать-CVB</ta>
            <ta e="T693" id="Seg_8886" s="T692">сын.[NOM]</ta>
            <ta e="T694" id="Seg_8887" s="T693">ты.NOM</ta>
            <ta e="T695" id="Seg_8888" s="T694">куда</ta>
            <ta e="T696" id="Seg_8889" s="T695">настолько</ta>
            <ta e="T697" id="Seg_8890" s="T696">уйти-EP-CO-2SG.S</ta>
            <ta e="T698" id="Seg_8891" s="T697">я.NOM</ta>
            <ta e="T699" id="Seg_8892" s="T698">скучать-DRV-ACTN-TRL.1SG</ta>
            <ta e="T700" id="Seg_8893" s="T699">вниз-ADV.LOC</ta>
            <ta e="T701" id="Seg_8894" s="T700">этот</ta>
            <ta e="T702" id="Seg_8895" s="T701">бугор.[NOM]</ta>
            <ta e="T703" id="Seg_8896" s="T702">над-LOC</ta>
            <ta e="T704" id="Seg_8897" s="T703">сидеть-HAB-FRQ-PST.NAR-INFER-1SG.S</ta>
            <ta e="T705" id="Seg_8898" s="T704">этот</ta>
            <ta e="T706" id="Seg_8899" s="T705">ночь-ADV.LOC</ta>
            <ta e="T707" id="Seg_8900" s="T706">парень.[NOM]</ta>
            <ta e="T708" id="Seg_8901" s="T707">INFER</ta>
            <ta e="T709" id="Seg_8902" s="T708">ночевать-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T710" id="Seg_8903" s="T709">следующий</ta>
            <ta e="T711" id="Seg_8904" s="T710">день.[NOM]</ta>
            <ta e="T712" id="Seg_8905" s="T711">олень-ACC-3SG</ta>
            <ta e="T713" id="Seg_8906" s="T712">домой</ta>
            <ta e="T714" id="Seg_8907" s="T713">погнать-CVB</ta>
            <ta e="T715" id="Seg_8908" s="T714">олень-GEN-3SG</ta>
            <ta e="T716" id="Seg_8909" s="T715">внутри-LOC</ta>
            <ta e="T717" id="Seg_8910" s="T716">искать-HAB-CVB</ta>
            <ta e="T718" id="Seg_8911" s="T717">два</ta>
            <ta e="T719" id="Seg_8912" s="T718">кошка-GEN</ta>
            <ta e="T720" id="Seg_8913" s="T719">шерсть-EP-ADJZ</ta>
            <ta e="T721" id="Seg_8914" s="T720">бык-DU-GEN</ta>
            <ta e="T722" id="Seg_8915" s="T721">INFER</ta>
            <ta e="T723" id="Seg_8916" s="T722">схватить-PST.NAR-INFER-3SG.O</ta>
            <ta e="T724" id="Seg_8917" s="T723">доска-PL-EP-CAR</ta>
            <ta e="T725" id="Seg_8918" s="T724">нарта-ACC-3SG</ta>
            <ta e="T726" id="Seg_8919" s="T725">INFER</ta>
            <ta e="T727" id="Seg_8920" s="T726">запрягать-MOM-DUR-IPFV-TR-INFER.[3SG.S]</ta>
            <ta e="T728" id="Seg_8921" s="T727">кожаный</ta>
            <ta e="T729" id="Seg_8922" s="T728">малица-ACC-3SG</ta>
            <ta e="T730" id="Seg_8923" s="T729">и</ta>
            <ta e="T731" id="Seg_8924" s="T730">сокуй-ACC-3SG</ta>
            <ta e="T732" id="Seg_8925" s="T731">INFER</ta>
            <ta e="T733" id="Seg_8926" s="T732">надеть-TR-DUR-IPFV-ACTN-ADV.EL</ta>
            <ta e="T734" id="Seg_8927" s="T733">чурка-дерево-ACC-3SG</ta>
            <ta e="T735" id="Seg_8928" s="T734">взять-PST.NAR-3SG.O</ta>
            <ta e="T736" id="Seg_8929" s="T735">что.ли</ta>
            <ta e="T737" id="Seg_8930" s="T736">NEG</ta>
            <ta e="T739" id="Seg_8931" s="T738">такой-ADVZ</ta>
            <ta e="T740" id="Seg_8932" s="T739">INFER</ta>
            <ta e="T741" id="Seg_8933" s="T740">тронуться-PFV-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T742" id="Seg_8934" s="T741">тот-ADJZ</ta>
            <ta e="T743" id="Seg_8935" s="T742">человек-EP-PL-EP-GEN</ta>
            <ta e="T744" id="Seg_8936" s="T743">чум-ILL</ta>
            <ta e="T745" id="Seg_8937" s="T744">едва</ta>
            <ta e="T746" id="Seg_8938" s="T745">прийти-PFV.[3SG.S]</ta>
            <ta e="T747" id="Seg_8939" s="T746">видать</ta>
            <ta e="T748" id="Seg_8940" s="T747">будто</ta>
            <ta e="T749" id="Seg_8941" s="T748">Лапта-ADJZ</ta>
            <ta e="T750" id="Seg_8942" s="T749">брат-DYA-PL.[NOM]</ta>
            <ta e="T751" id="Seg_8943" s="T750">раньше</ta>
            <ta e="T752" id="Seg_8944" s="T751">что.ли</ta>
            <ta e="T753" id="Seg_8945" s="T752">прийти-CO-3PL</ta>
            <ta e="T754" id="Seg_8946" s="T753">парень.[NOM]</ta>
            <ta e="T755" id="Seg_8947" s="T754">вожжи.[NOM]-3SG</ta>
            <ta e="T756" id="Seg_8948" s="T755">вниз</ta>
            <ta e="T757" id="Seg_8949" s="T756">привязать-CVB</ta>
            <ta e="T758" id="Seg_8950" s="T757">чум-ILL</ta>
            <ta e="T759" id="Seg_8951" s="T758">INFER</ta>
            <ta e="T760" id="Seg_8952" s="T759">войти-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T761" id="Seg_8953" s="T760">чум-ILL</ta>
            <ta e="T762" id="Seg_8954" s="T761">войти-CVB</ta>
            <ta e="T763" id="Seg_8955" s="T762">чум-EP-GEN</ta>
            <ta e="T764" id="Seg_8956" s="T763">отверстие-ILL</ta>
            <ta e="T765" id="Seg_8957" s="T764">едва</ta>
            <ta e="T766" id="Seg_8958" s="T765">встать-PFV-CO.[3SG.S]</ta>
            <ta e="T767" id="Seg_8959" s="T766">старик.[NOM]</ta>
            <ta e="T768" id="Seg_8960" s="T767">вчера</ta>
            <ta e="T769" id="Seg_8961" s="T768">день.[NOM]</ta>
            <ta e="T770" id="Seg_8962" s="T769">видать</ta>
            <ta e="T771" id="Seg_8963" s="T770">будто</ta>
            <ta e="T772" id="Seg_8964" s="T771">вот-эй-%%</ta>
            <ta e="T773" id="Seg_8965" s="T772">сказать.[3SG.S]</ta>
            <ta e="T774" id="Seg_8966" s="T773">вы.PL.NOM</ta>
            <ta e="T775" id="Seg_8967" s="T774">что</ta>
            <ta e="T776" id="Seg_8968" s="T775">плохой-ADJZ</ta>
            <ta e="T777" id="Seg_8969" s="T776">человек-EP-ACC</ta>
            <ta e="T778" id="Seg_8970" s="T777">NEG</ta>
            <ta e="T779" id="Seg_8971" s="T778">видеть-IPFV-2PL</ta>
            <ta e="T780" id="Seg_8972" s="T779">вперёд</ta>
            <ta e="T781" id="Seg_8973" s="T780">пустить-SUP.2/3PL</ta>
            <ta e="T782" id="Seg_8974" s="T781">парень.[NOM]</ta>
            <ta e="T783" id="Seg_8975" s="T782">взглянуть-PST.NAR-3SG.O</ta>
            <ta e="T784" id="Seg_8976" s="T783">старик-GEN</ta>
            <ta e="T785" id="Seg_8977" s="T784">около-ILL</ta>
            <ta e="T786" id="Seg_8978" s="T785">место-ACC</ta>
            <ta e="T787" id="Seg_8979" s="T786">в.разные.стороны</ta>
            <ta e="T788" id="Seg_8980" s="T787">INFER</ta>
            <ta e="T789" id="Seg_8981" s="T788">сделать-INFER-3PL</ta>
            <ta e="T790" id="Seg_8982" s="T789">парень.[NOM]</ta>
            <ta e="T791" id="Seg_8983" s="T790">вперёд</ta>
            <ta e="T792" id="Seg_8984" s="T791">вниз</ta>
            <ta e="T793" id="Seg_8985" s="T792">сесть.[3SG.S]</ta>
            <ta e="T794" id="Seg_8986" s="T793">такой-ADVZ</ta>
            <ta e="T795" id="Seg_8987" s="T794">взглянуть-PST.NAR-3SG.O</ta>
            <ta e="T796" id="Seg_8988" s="T795">видать</ta>
            <ta e="T797" id="Seg_8989" s="T796">будто</ta>
            <ta e="T798" id="Seg_8990" s="T797">человек-EP-человек-PL-EP-ADJZ</ta>
            <ta e="T799" id="Seg_8991" s="T798">вместе</ta>
            <ta e="T800" id="Seg_8992" s="T799">сесть-PTCP.PRS</ta>
            <ta e="T801" id="Seg_8993" s="T800">прийти-DUR-CVB</ta>
            <ta e="T802" id="Seg_8994" s="T801">пить-PST.NAR-3PL</ta>
            <ta e="T803" id="Seg_8995" s="T802">парень.[NOM]</ta>
            <ta e="T804" id="Seg_8996" s="T803">тоже</ta>
            <ta e="T805" id="Seg_8997" s="T804">этот</ta>
            <ta e="T806" id="Seg_8998" s="T805">человек-EP-OBL.3SG-COM</ta>
            <ta e="T807" id="Seg_8999" s="T806">он(а)</ta>
            <ta e="T808" id="Seg_9000" s="T807">тоже</ta>
            <ta e="T809" id="Seg_9001" s="T808">INFER</ta>
            <ta e="T810" id="Seg_9002" s="T809">пить-DUR-FRQ-INCH-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T811" id="Seg_9003" s="T810">видать</ta>
            <ta e="T812" id="Seg_9004" s="T811">будто</ta>
            <ta e="T813" id="Seg_9005" s="T812">парень-ACC</ta>
            <ta e="T814" id="Seg_9006" s="T813">спирт.[NOM]</ta>
            <ta e="T815" id="Seg_9007" s="T814">INFER</ta>
            <ta e="T816" id="Seg_9008" s="T815">войти-INFER-3SG.O</ta>
            <ta e="T817" id="Seg_9009" s="T816">спирт.[NOM]</ta>
            <ta e="T818" id="Seg_9010" s="T817">войти-CVB</ta>
            <ta e="T819" id="Seg_9011" s="T818">парень.[NOM]</ta>
            <ta e="T820" id="Seg_9012" s="T819">упасть.[3SG.S]</ta>
            <ta e="T821" id="Seg_9013" s="T820">спирт.[NOM]</ta>
            <ta e="T822" id="Seg_9014" s="T821">войти-PTCP.PST</ta>
            <ta e="T823" id="Seg_9015" s="T822">человек.[NOM]</ta>
            <ta e="T824" id="Seg_9016" s="T823">что.[NOM]</ta>
            <ta e="T825" id="Seg_9017" s="T824">что.[NOM]</ta>
            <ta e="T826" id="Seg_9018" s="T825">знать-FUT.[3SG.S]</ta>
            <ta e="T827" id="Seg_9019" s="T826">когда</ta>
            <ta e="T828" id="Seg_9020" s="T827">NEG</ta>
            <ta e="T829" id="Seg_9021" s="T828">INFER</ta>
            <ta e="T830" id="Seg_9022" s="T829">лежать-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T831" id="Seg_9023" s="T830">один</ta>
            <ta e="T832" id="Seg_9024" s="T831">целый</ta>
            <ta e="T833" id="Seg_9025" s="T832">середина-LOC</ta>
            <ta e="T834" id="Seg_9026" s="T833">парень.[NOM]</ta>
            <ta e="T835" id="Seg_9027" s="T834">вверх</ta>
            <ta e="T836" id="Seg_9028" s="T835">едва</ta>
            <ta e="T837" id="Seg_9029" s="T836">отправиться-CO.[3SG.S]</ta>
            <ta e="T838" id="Seg_9030" s="T837">видать</ta>
            <ta e="T839" id="Seg_9031" s="T838">будто</ta>
            <ta e="T840" id="Seg_9032" s="T839">человек.[NOM]</ta>
            <ta e="T841" id="Seg_9033" s="T840">я.ACC</ta>
            <ta e="T842" id="Seg_9034" s="T841">ударить-%%-PTCP.PRS</ta>
            <ta e="T843" id="Seg_9035" s="T842">догонять-TR-US.[3SG.S]</ta>
            <ta e="T844" id="Seg_9036" s="T843">такой-ADVZ</ta>
            <ta e="T845" id="Seg_9037" s="T844">лежать-CVB</ta>
            <ta e="T846" id="Seg_9038" s="T845">слушать-TR-PST.NAR-3SG.O</ta>
            <ta e="T847" id="Seg_9039" s="T846">что-ADJZ</ta>
            <ta e="T848" id="Seg_9040" s="T847">земля-LOC</ta>
            <ta e="T849" id="Seg_9041" s="T848">тот</ta>
            <ta e="T850" id="Seg_9042" s="T849">белый-богач-PL-EP-GEN</ta>
            <ta e="T851" id="Seg_9043" s="T850">сестра.[NOM]</ta>
            <ta e="T852" id="Seg_9044" s="T851">крик-VBLZ-AUD.[3SG.S]</ta>
            <ta e="T853" id="Seg_9045" s="T852">вы.PL.NOM</ta>
            <ta e="T854" id="Seg_9046" s="T853">эй</ta>
            <ta e="T855" id="Seg_9047" s="T854">мол</ta>
            <ta e="T856" id="Seg_9048" s="T855">будто</ta>
            <ta e="T857" id="Seg_9049" s="T856">что.[NOM]</ta>
            <ta e="T858" id="Seg_9050" s="T857">взглянуть-DUR-PST.NAR-INFER-2PL</ta>
            <ta e="T859" id="Seg_9051" s="T858">тот</ta>
            <ta e="T860" id="Seg_9052" s="T859">новый-ADVZ</ta>
            <ta e="T861" id="Seg_9053" s="T860">прийти-PTCP.PST</ta>
            <ta e="T862" id="Seg_9054" s="T861">парень-ACC</ta>
            <ta e="T863" id="Seg_9055" s="T862">INFER</ta>
            <ta e="T864" id="Seg_9056" s="T863">ударить-INFER-3PL</ta>
            <ta e="T865" id="Seg_9057" s="T864">Лапта-ADJZ</ta>
            <ta e="T866" id="Seg_9058" s="T865">брат-DYA-PL.[NOM]</ta>
            <ta e="T867" id="Seg_9059" s="T866">парень.[NOM]</ta>
            <ta e="T868" id="Seg_9060" s="T867">NEG</ta>
            <ta e="T869" id="Seg_9061" s="T868">куда.деваться.[3SG.S]</ta>
            <ta e="T870" id="Seg_9062" s="T869">лежать-ACTN.[NOM]</ta>
            <ta e="T871" id="Seg_9063" s="T870">вдоль-LOC</ta>
            <ta e="T872" id="Seg_9064" s="T871">на-LOC-3SG</ta>
            <ta e="T873" id="Seg_9065" s="T872">ударить-CVB</ta>
            <ta e="T874" id="Seg_9066" s="T873">лечь-PTCP.PST</ta>
            <ta e="T875" id="Seg_9067" s="T874">человек-EP-ACC-3SG</ta>
            <ta e="T876" id="Seg_9068" s="T875">локоть-OBL.3SG-INSTR</ta>
            <ta e="T877" id="Seg_9069" s="T876">в.разные.стороны</ta>
            <ta e="T878" id="Seg_9070" s="T877">INFER</ta>
            <ta e="T879" id="Seg_9071" s="T878">ударить-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T880" id="Seg_9072" s="T879">потом</ta>
            <ta e="T881" id="Seg_9073" s="T880">два-ORD</ta>
            <ta e="T882" id="Seg_9074" s="T881">в.разные.стороны</ta>
            <ta e="T883" id="Seg_9075" s="T882">бросать-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T884" id="Seg_9076" s="T883">видать</ta>
            <ta e="T885" id="Seg_9077" s="T884">будто</ta>
            <ta e="T886" id="Seg_9078" s="T885">ненец-ADJZ</ta>
            <ta e="T887" id="Seg_9079" s="T886">брат-DYA-PL.[NOM]</ta>
            <ta e="T888" id="Seg_9080" s="T887">Лапта-ADJZ</ta>
            <ta e="T889" id="Seg_9081" s="T888">такой-ADVZ</ta>
            <ta e="T890" id="Seg_9082" s="T889">умереть-PST.NAR-3PL</ta>
            <ta e="T891" id="Seg_9083" s="T890">некоторый-некто-PL-EP-GEN</ta>
            <ta e="T892" id="Seg_9084" s="T891">середина-PROL</ta>
            <ta e="T893" id="Seg_9085" s="T892">на.части</ta>
            <ta e="T894" id="Seg_9086" s="T893">ломать-RFL.PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T895" id="Seg_9087" s="T894">некоторый-некто-PL-EP-GEN</ta>
            <ta e="T896" id="Seg_9088" s="T895">середина-ACC</ta>
            <ta e="T897" id="Seg_9089" s="T896">на.части</ta>
            <ta e="T898" id="Seg_9090" s="T897">треснуть-RFL.PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T899" id="Seg_9091" s="T898">а</ta>
            <ta e="T900" id="Seg_9092" s="T899">самый</ta>
            <ta e="T901" id="Seg_9093" s="T900">младший</ta>
            <ta e="T902" id="Seg_9094" s="T901">шаманская.мудрость-PROPR-EP-ADJZ</ta>
            <ta e="T903" id="Seg_9095" s="T902">брат-PL-EP-GEN</ta>
            <ta e="T904" id="Seg_9096" s="T903">видать</ta>
            <ta e="T905" id="Seg_9097" s="T904">будто</ta>
            <ta e="T906" id="Seg_9098" s="T905">голова.[NOM]-3SG</ta>
            <ta e="T907" id="Seg_9099" s="T906">всё.время</ta>
            <ta e="T908" id="Seg_9100" s="T907">нет</ta>
            <ta e="T909" id="Seg_9101" s="T908">а</ta>
            <ta e="T910" id="Seg_9102" s="T909">этот</ta>
            <ta e="T911" id="Seg_9103" s="T910">ветка-PL-EP-CAR</ta>
            <ta e="T912" id="Seg_9104" s="T911">чум.[NOM]</ta>
            <ta e="T913" id="Seg_9105" s="T912">исключительный</ta>
            <ta e="T914" id="Seg_9106" s="T913">ветка-PL-EP-CAR</ta>
            <ta e="T915" id="Seg_9107" s="T914">основание.[NOM]</ta>
            <ta e="T916" id="Seg_9108" s="T915">остаться-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T917" id="Seg_9109" s="T916">чум-EP-мешковина.[NOM]</ta>
            <ta e="T918" id="Seg_9110" s="T917">и</ta>
            <ta e="T919" id="Seg_9111" s="T918">чум-EP-шкура.[NOM]</ta>
            <ta e="T920" id="Seg_9112" s="T919">завтра-день.[NOM]</ta>
            <ta e="T921" id="Seg_9113" s="T920">целый</ta>
            <ta e="T922" id="Seg_9114" s="T921">поодаль</ta>
            <ta e="T923" id="Seg_9115" s="T922">в.сторону-ADV.LOC</ta>
            <ta e="T924" id="Seg_9116" s="T923">лежать-3PL</ta>
            <ta e="T925" id="Seg_9117" s="T924">вверх</ta>
            <ta e="T926" id="Seg_9118" s="T925">встать-CVB</ta>
            <ta e="T927" id="Seg_9119" s="T926">такой-ADVZ</ta>
            <ta e="T928" id="Seg_9120" s="T927">в.разные.стороны</ta>
            <ta e="T929" id="Seg_9121" s="T928">взглянуть-PST.NAR-3SG.O</ta>
            <ta e="T930" id="Seg_9122" s="T929">человек-EP-PL.[NOM]</ta>
            <ta e="T931" id="Seg_9123" s="T930">собирать-DUR-CVB</ta>
            <ta e="T932" id="Seg_9124" s="T931">он(а)-EP-ACC</ta>
            <ta e="T933" id="Seg_9125" s="T932">взглянуть-PST.NAR-3PL</ta>
            <ta e="T934" id="Seg_9126" s="T933">а</ta>
            <ta e="T935" id="Seg_9127" s="T934">тот</ta>
            <ta e="T936" id="Seg_9128" s="T935">девушка.[NOM]</ta>
            <ta e="T937" id="Seg_9129" s="T936">всё.время</ta>
            <ta e="T938" id="Seg_9130" s="T937">NEG.EX.[3SG.S]</ta>
            <ta e="T939" id="Seg_9131" s="T938">прочь</ta>
            <ta e="T940" id="Seg_9132" s="T939">уйти-CVB</ta>
            <ta e="T941" id="Seg_9133" s="T940">чум-EP-шкура-ACC</ta>
            <ta e="T942" id="Seg_9134" s="T941">вверх</ta>
            <ta e="T943" id="Seg_9135" s="T942">в.разные.стороны</ta>
            <ta e="T944" id="Seg_9136" s="T943">едва</ta>
            <ta e="T945" id="Seg_9137" s="T944">открыть-CO-3SG.O</ta>
            <ta e="T946" id="Seg_9138" s="T945">видать</ta>
            <ta e="T947" id="Seg_9139" s="T946">девушка.[NOM]</ta>
            <ta e="T948" id="Seg_9140" s="T947">отсюда</ta>
            <ta e="T949" id="Seg_9141" s="T948">INFER</ta>
            <ta e="T950" id="Seg_9142" s="T949">выскочить-DRV-INFER.[3SG.S]</ta>
            <ta e="T951" id="Seg_9143" s="T950">NEG</ta>
            <ta e="T952" id="Seg_9144" s="T951">куда.деваться-3PL</ta>
            <ta e="T953" id="Seg_9145" s="T952">этот</ta>
            <ta e="T954" id="Seg_9146" s="T953">здесь-ADJZ</ta>
            <ta e="T955" id="Seg_9147" s="T954">человек-EP-PL.[NOM]</ta>
            <ta e="T956" id="Seg_9148" s="T955">новый</ta>
            <ta e="T957" id="Seg_9149" s="T956">чум-EP-ACC</ta>
            <ta e="T958" id="Seg_9150" s="T957">натянуть.нюк-CVB</ta>
            <ta e="T959" id="Seg_9151" s="T958">затем</ta>
            <ta e="T960" id="Seg_9152" s="T959">INFER</ta>
            <ta e="T961" id="Seg_9153" s="T960">пить-PFV-FRQ-PST.NAR-INFER-3PL</ta>
            <ta e="T962" id="Seg_9154" s="T961">и</ta>
            <ta e="T963" id="Seg_9155" s="T962">съесть-EP-FRQ-PST.NAR-3PL</ta>
            <ta e="T964" id="Seg_9156" s="T963">белый-богач.[NOM]</ta>
            <ta e="T965" id="Seg_9157" s="T964">старик.[NOM]</ta>
            <ta e="T966" id="Seg_9158" s="T965">сестра-ACC-3SG</ta>
            <ta e="T967" id="Seg_9159" s="T966">этот</ta>
            <ta e="T968" id="Seg_9160" s="T967">парень-ALL</ta>
            <ta e="T969" id="Seg_9161" s="T968">INFER</ta>
            <ta e="T970" id="Seg_9162" s="T969">идти-CAUS-PST.NAR-INFER-3SG.O</ta>
            <ta e="T971" id="Seg_9163" s="T970">молодой</ta>
            <ta e="T972" id="Seg_9164" s="T971">человек-EP-человек.[NOM]</ta>
            <ta e="T973" id="Seg_9165" s="T972">вместе</ta>
            <ta e="T974" id="Seg_9166" s="T973">сесть-DUR-TR-IPFV-CVB</ta>
            <ta e="T975" id="Seg_9167" s="T974">четыре-TEN.UNIT</ta>
            <ta e="T976" id="Seg_9168" s="T975">десять</ta>
            <ta e="T977" id="Seg_9169" s="T976">день.[NOM]</ta>
            <ta e="T978" id="Seg_9170" s="T977">пить-DUR-DUR-PST.NAR-3PL</ta>
            <ta e="T979" id="Seg_9171" s="T978">человек-EP-человек.[NOM]</ta>
            <ta e="T980" id="Seg_9172" s="T979">вместе</ta>
            <ta e="T981" id="Seg_9173" s="T980">сесть-TR-RES-DUR-CVB</ta>
            <ta e="T982" id="Seg_9174" s="T981">белый-богач-ADJZ</ta>
            <ta e="T983" id="Seg_9175" s="T982">брат-DYA-PL.[NOM]</ta>
            <ta e="T984" id="Seg_9176" s="T983">этот</ta>
            <ta e="T985" id="Seg_9177" s="T984">парень-COM</ta>
            <ta e="T986" id="Seg_9178" s="T985">на.другую.сторону</ta>
            <ta e="T987" id="Seg_9179" s="T986">олений.обоз-TR-3PL</ta>
            <ta e="T988" id="Seg_9180" s="T987">парень-GEN</ta>
            <ta e="T989" id="Seg_9181" s="T988">чум.[GEN]-3SG</ta>
            <ta e="T990" id="Seg_9182" s="T989">рядом-ILL</ta>
            <ta e="T991" id="Seg_9183" s="T990">вместе</ta>
            <ta e="T992" id="Seg_9184" s="T991">в.куче</ta>
            <ta e="T993" id="Seg_9185" s="T992">INFER</ta>
            <ta e="T994" id="Seg_9186" s="T993">жить-PST.NAR-INFER-3PL</ta>
            <ta e="T995" id="Seg_9187" s="T994">и</ta>
            <ta e="T996" id="Seg_9188" s="T995">теперь</ta>
            <ta e="T997" id="Seg_9189" s="T996">INDEF3</ta>
            <ta e="T998" id="Seg_9190" s="T997">и</ta>
            <ta e="T999" id="Seg_9191" s="T998">жить-DUR-3PL</ta>
            <ta e="T1000" id="Seg_9192" s="T999">сказка.[NOM]</ta>
            <ta e="T1001" id="Seg_9193" s="T1000">некто-PROL</ta>
            <ta e="T1002" id="Seg_9194" s="T1001">по</ta>
            <ta e="T1003" id="Seg_9195" s="T1002">INFER</ta>
            <ta e="T1004" id="Seg_9196" s="T1003">достаточное.количество.[NOM]</ta>
            <ta e="T1005" id="Seg_9197" s="T1004">быть-PST.NAR-INFER.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T83" id="Seg_9198" s="T82">nprop-n&gt;adj</ta>
            <ta e="T84" id="Seg_9199" s="T83">num</ta>
            <ta e="T85" id="Seg_9200" s="T84">n-n&gt;n-n:case-n:poss</ta>
            <ta e="T86" id="Seg_9201" s="T85">num</ta>
            <ta e="T87" id="Seg_9202" s="T86">n-n&gt;n-n:num-n:case</ta>
            <ta e="T88" id="Seg_9203" s="T87">v-v:tense-v:mood-v:pn</ta>
            <ta e="T89" id="Seg_9204" s="T88">n-n:case-n:poss</ta>
            <ta e="T90" id="Seg_9205" s="T89">adv</ta>
            <ta e="T91" id="Seg_9206" s="T90">n-n:obl.poss-n:case</ta>
            <ta e="T92" id="Seg_9207" s="T91">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T93" id="Seg_9208" s="T92">n-n:case-n:poss</ta>
            <ta e="T94" id="Seg_9209" s="T93">pro-n:case</ta>
            <ta e="T95" id="Seg_9210" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_9211" s="T95">v-v:tense-v:pn</ta>
            <ta e="T97" id="Seg_9212" s="T96">n-n:case-n:poss</ta>
            <ta e="T98" id="Seg_9213" s="T97">interrog</ta>
            <ta e="T99" id="Seg_9214" s="T98">v-v:tense-v:pn</ta>
            <ta e="T100" id="Seg_9215" s="T99">num</ta>
            <ta e="T101" id="Seg_9216" s="T100">dem-n-n:case</ta>
            <ta e="T102" id="Seg_9217" s="T101">n-n:case</ta>
            <ta e="T103" id="Seg_9218" s="T102">n-n:obl.poss-n:case</ta>
            <ta e="T104" id="Seg_9219" s="T103">ptcl-ptcl-%%</ta>
            <ta e="T105" id="Seg_9220" s="T104">v-v:pn</ta>
            <ta e="T106" id="Seg_9221" s="T105">n-n:case</ta>
            <ta e="T107" id="Seg_9222" s="T106">pers</ta>
            <ta e="T108" id="Seg_9223" s="T107">n-n:case-n:poss</ta>
            <ta e="T109" id="Seg_9224" s="T108">v-v&gt;ptcp</ta>
            <ta e="T110" id="Seg_9225" s="T109">n-n:case-n:poss</ta>
            <ta e="T111" id="Seg_9226" s="T110">interrog</ta>
            <ta e="T112" id="Seg_9227" s="T111">pers</ta>
            <ta e="T113" id="Seg_9228" s="T112">v-v&gt;v-v:mood.pn</ta>
            <ta e="T114" id="Seg_9229" s="T113">conj</ta>
            <ta e="T115" id="Seg_9230" s="T114">n-n:poss-n&gt;adj</ta>
            <ta e="T116" id="Seg_9231" s="T115">n-n&gt;n-n:num-n:case</ta>
            <ta e="T117" id="Seg_9232" s="T116">pp</ta>
            <ta e="T118" id="Seg_9233" s="T117">adv</ta>
            <ta e="T119" id="Seg_9234" s="T118">v-v:tense-v:pn</ta>
            <ta e="T120" id="Seg_9235" s="T119">n-n:obl.poss-n:case</ta>
            <ta e="T121" id="Seg_9236" s="T120">v-v:tense-v:pn</ta>
            <ta e="T122" id="Seg_9237" s="T121">conj</ta>
            <ta e="T123" id="Seg_9238" s="T122">n-n:obl.poss-n&gt;adj</ta>
            <ta e="T124" id="Seg_9239" s="T123">dem-adj&gt;adj</ta>
            <ta e="T125" id="Seg_9240" s="T124">n-n:obl.poss-n&gt;adj</ta>
            <ta e="T126" id="Seg_9241" s="T125">v-v:tense-v:mood-v:pn</ta>
            <ta e="T127" id="Seg_9242" s="T126">n-n:case-n:poss</ta>
            <ta e="T128" id="Seg_9243" s="T127">n-n:case</ta>
            <ta e="T129" id="Seg_9244" s="T128">n-n:case-n:poss</ta>
            <ta e="T130" id="Seg_9245" s="T129">n-n:case-n:poss</ta>
            <ta e="T131" id="Seg_9246" s="T130">v-v:tense-v:pn</ta>
            <ta e="T132" id="Seg_9247" s="T131">pro-n:case-n:poss</ta>
            <ta e="T133" id="Seg_9248" s="T132">v-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T134" id="Seg_9249" s="T133">n-n:case-n:poss-n:case</ta>
            <ta e="T135" id="Seg_9250" s="T134">dem-adj&gt;adv</ta>
            <ta e="T136" id="Seg_9251" s="T135">adv</ta>
            <ta e="T137" id="Seg_9252" s="T136">v-v:inf-v-v:ins-v:tense-v:pn</ta>
            <ta e="T138" id="Seg_9253" s="T137">n-n:case-n:poss</ta>
            <ta e="T139" id="Seg_9254" s="T138">ptcl-ptcl-%%</ta>
            <ta e="T140" id="Seg_9255" s="T139">v-v:pn</ta>
            <ta e="T141" id="Seg_9256" s="T140">pers</ta>
            <ta e="T142" id="Seg_9257" s="T141">n-n:case</ta>
            <ta e="T143" id="Seg_9258" s="T142">interrog</ta>
            <ta e="T144" id="Seg_9259" s="T143">v-v:ins-v:pn</ta>
            <ta e="T145" id="Seg_9260" s="T144">n-n:case</ta>
            <ta e="T146" id="Seg_9261" s="T145">dem-ptcl-%%</ta>
            <ta e="T147" id="Seg_9262" s="T146">v-v:pn</ta>
            <ta e="T148" id="Seg_9263" s="T147">pers</ta>
            <ta e="T149" id="Seg_9264" s="T148">ptcl</ta>
            <ta e="T150" id="Seg_9265" s="T149">interrog</ta>
            <ta e="T151" id="Seg_9266" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_9267" s="T151">v-v:tense-v:pn</ta>
            <ta e="T153" id="Seg_9268" s="T152">pers</ta>
            <ta e="T154" id="Seg_9269" s="T153">dem</ta>
            <ta e="T155" id="Seg_9270" s="T154">v-v&gt;v-v&gt;n-n:case.poss</ta>
            <ta e="T156" id="Seg_9271" s="T155">adv-adv:case</ta>
            <ta e="T157" id="Seg_9272" s="T156">n-n:case</ta>
            <ta e="T158" id="Seg_9273" s="T157">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T159" id="Seg_9274" s="T158">n-n:case-n:poss</ta>
            <ta e="T160" id="Seg_9275" s="T159">ptcl</ta>
            <ta e="T161" id="Seg_9276" s="T160">qv-v:pn</ta>
            <ta e="T162" id="Seg_9277" s="T161">n-n:case</ta>
            <ta e="T163" id="Seg_9278" s="T162">pp-n&gt;adv</ta>
            <ta e="T164" id="Seg_9279" s="T163">v-v&gt;v-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T165" id="Seg_9280" s="T164">pro-n:case-n:poss</ta>
            <ta e="T166" id="Seg_9281" s="T165">adj-adj&gt;adj</ta>
            <ta e="T167" id="Seg_9282" s="T166">n-n&gt;adj</ta>
            <ta e="T168" id="Seg_9283" s="T167">n-n:case-n&gt;adj</ta>
            <ta e="T169" id="Seg_9284" s="T168">n-n:case</ta>
            <ta e="T170" id="Seg_9285" s="T169">preverb</ta>
            <ta e="T171" id="Seg_9286" s="T170">v-v:tense.mood-v:pn</ta>
            <ta e="T172" id="Seg_9287" s="T171">ptcl</ta>
            <ta e="T173" id="Seg_9288" s="T172">ptcl</ta>
            <ta e="T174" id="Seg_9289" s="T173">n-n&gt;adj</ta>
            <ta e="T175" id="Seg_9290" s="T174">n-n:case</ta>
            <ta e="T176" id="Seg_9291" s="T175">n-n:case</ta>
            <ta e="T177" id="Seg_9292" s="T176">adj</ta>
            <ta e="T178" id="Seg_9293" s="T177">v-v:tense</ta>
            <ta e="T179" id="Seg_9294" s="T178">n-n:case</ta>
            <ta e="T180" id="Seg_9295" s="T179">dem-adj&gt;adv</ta>
            <ta e="T181" id="Seg_9296" s="T180">v-v:tense-v:mood-v:pn</ta>
            <ta e="T182" id="Seg_9297" s="T181">adv</ta>
            <ta e="T183" id="Seg_9298" s="T182">dem</ta>
            <ta e="T184" id="Seg_9299" s="T183">n-n:case-n:obl.poss</ta>
            <ta e="T185" id="Seg_9300" s="T184">pp-n:case</ta>
            <ta e="T186" id="Seg_9301" s="T185">num</ta>
            <ta e="T187" id="Seg_9302" s="T186">adj-n&gt;adj</ta>
            <ta e="T188" id="Seg_9303" s="T187">n-n:case</ta>
            <ta e="T189" id="Seg_9304" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_9305" s="T189">v-v:tense.mood-v:pn</ta>
            <ta e="T191" id="Seg_9306" s="T190">num</ta>
            <ta e="T192" id="Seg_9307" s="T191">adj-adj&gt;adj</ta>
            <ta e="T193" id="Seg_9308" s="T192">n-n:case</ta>
            <ta e="T194" id="Seg_9309" s="T193">ptcl</ta>
            <ta e="T195" id="Seg_9310" s="T194">v-v:tense.mood-v:pn</ta>
            <ta e="T196" id="Seg_9311" s="T195">adj-adj&gt;adj-adj&gt;adj</ta>
            <ta e="T197" id="Seg_9312" s="T196">pro-n:case</ta>
            <ta e="T198" id="Seg_9313" s="T197">v-v&gt;v-v:mood.pn</ta>
            <ta e="T199" id="Seg_9314" s="T198">dem</ta>
            <ta e="T200" id="Seg_9315" s="T199">n-n:num-n:case</ta>
            <ta e="T201" id="Seg_9316" s="T200">emphpro</ta>
            <ta e="T202" id="Seg_9317" s="T201">v-v&gt;adv</ta>
            <ta e="T203" id="Seg_9318" s="T202">ptcl</ta>
            <ta e="T204" id="Seg_9319" s="T203">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T205" id="Seg_9320" s="T204">n-n:case</ta>
            <ta e="T206" id="Seg_9321" s="T205">n-n:case-n:poss</ta>
            <ta e="T207" id="Seg_9322" s="T206">n</ta>
            <ta e="T208" id="Seg_9323" s="T207">pp</ta>
            <ta e="T209" id="Seg_9324" s="T208">ptcl</ta>
            <ta e="T210" id="Seg_9325" s="T209">v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T211" id="Seg_9326" s="T210">dem-adj&gt;adv</ta>
            <ta e="T212" id="Seg_9327" s="T211">adv</ta>
            <ta e="T213" id="Seg_9328" s="T212">ptcl</ta>
            <ta e="T214" id="Seg_9329" s="T213">v-v:tense-v:mood-v:pn</ta>
            <ta e="T215" id="Seg_9330" s="T214">n-n:case-n:poss</ta>
            <ta e="T216" id="Seg_9331" s="T215">pp-n&gt;adv</ta>
            <ta e="T217" id="Seg_9332" s="T216">v-v&gt;v-v&gt;adv</ta>
            <ta e="T218" id="Seg_9333" s="T217">dem</ta>
            <ta e="T219" id="Seg_9334" s="T218">n-n:case-n:poss</ta>
            <ta e="T220" id="Seg_9335" s="T219">v-v&gt;ptcp</ta>
            <ta e="T221" id="Seg_9336" s="T220">n-n:case</ta>
            <ta e="T222" id="Seg_9337" s="T221">ptcl</ta>
            <ta e="T223" id="Seg_9338" s="T222">v-v:tense-v:mood-v:pn</ta>
            <ta e="T224" id="Seg_9339" s="T223">adv</ta>
            <ta e="T225" id="Seg_9340" s="T224">v-v:ins-v:pn</ta>
            <ta e="T226" id="Seg_9341" s="T225">v-v&gt;v-v:inf</ta>
            <ta e="T227" id="Seg_9342" s="T226">pro-n:case</ta>
            <ta e="T228" id="Seg_9343" s="T227">pp</ta>
            <ta e="T229" id="Seg_9344" s="T228">n-n:case-n:poss</ta>
            <ta e="T230" id="Seg_9345" s="T229">ptcl</ta>
            <ta e="T231" id="Seg_9346" s="T230">ptcl</ta>
            <ta e="T232" id="Seg_9347" s="T231">n-n:num-n:ins-n:case</ta>
            <ta e="T233" id="Seg_9348" s="T232">pp-n:case</ta>
            <ta e="T234" id="Seg_9349" s="T233">dem</ta>
            <ta e="T235" id="Seg_9350" s="T234">n-n:case</ta>
            <ta e="T236" id="Seg_9351" s="T235">preverb</ta>
            <ta e="T237" id="Seg_9352" s="T236">v-v:tense-v:mood-v:pn</ta>
            <ta e="T238" id="Seg_9353" s="T237">ptcl</ta>
            <ta e="T239" id="Seg_9354" s="T238">ptcl</ta>
            <ta e="T240" id="Seg_9355" s="T239">num-num&gt;num-n:ins-num&gt;adj</ta>
            <ta e="T241" id="Seg_9356" s="T240">n</ta>
            <ta e="T242" id="Seg_9357" s="T241">n-n&gt;adj</ta>
            <ta e="T243" id="Seg_9358" s="T242">n-n:case-n:poss</ta>
            <ta e="T244" id="Seg_9359" s="T243">n-n:case</ta>
            <ta e="T245" id="Seg_9360" s="T244">n-n:case-n:poss</ta>
            <ta e="T246" id="Seg_9361" s="T245">ptcl</ta>
            <ta e="T247" id="Seg_9362" s="T246">v-v&gt;v-v:inf-v-v:tense-v:mood-v:pn</ta>
            <ta e="T248" id="Seg_9363" s="T247">ptcl</ta>
            <ta e="T249" id="Seg_9364" s="T248">ptcl</ta>
            <ta e="T250" id="Seg_9365" s="T249">dem</ta>
            <ta e="T251" id="Seg_9366" s="T250">num</ta>
            <ta e="T252" id="Seg_9367" s="T251">adj-adj&gt;adj</ta>
            <ta e="T253" id="Seg_9368" s="T252">adj-adj&gt;adj-adj&gt;adj</ta>
            <ta e="T254" id="Seg_9369" s="T253">n-n:num-n:case</ta>
            <ta e="T255" id="Seg_9370" s="T254">emphpro</ta>
            <ta e="T256" id="Seg_9371" s="T255">v-v&gt;adv</ta>
            <ta e="T257" id="Seg_9372" s="T256">ptcl</ta>
            <ta e="T258" id="Seg_9373" s="T257">ptcl</ta>
            <ta e="T259" id="Seg_9374" s="T258">adv</ta>
            <ta e="T260" id="Seg_9375" s="T259">ptcl</ta>
            <ta e="T261" id="Seg_9376" s="T260">v-v:tense-v:mood-v:pn</ta>
            <ta e="T262" id="Seg_9377" s="T261">n-n:case</ta>
            <ta e="T263" id="Seg_9378" s="T262">n-n:case-n:poss</ta>
            <ta e="T264" id="Seg_9379" s="T263">v-v&gt;adv</ta>
            <ta e="T265" id="Seg_9380" s="T264">ptcl</ta>
            <ta e="T266" id="Seg_9381" s="T265">v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T267" id="Seg_9382" s="T266">n-n:case-n:poss</ta>
            <ta e="T268" id="Seg_9383" s="T267">num</ta>
            <ta e="T269" id="Seg_9384" s="T268">n-n:ins-n&gt;adj</ta>
            <ta e="T270" id="Seg_9385" s="T269">n</ta>
            <ta e="T271" id="Seg_9386" s="T270">n-n&gt;adj</ta>
            <ta e="T272" id="Seg_9387" s="T271">n-n-n:case</ta>
            <ta e="T273" id="Seg_9388" s="T272">ptcl</ta>
            <ta e="T274" id="Seg_9389" s="T273">v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T275" id="Seg_9390" s="T274">adj-adj&gt;adj</ta>
            <ta e="T276" id="Seg_9391" s="T275">v-v&gt;v-v&gt;adv</ta>
            <ta e="T277" id="Seg_9392" s="T276">v-v&gt;ptcp</ta>
            <ta e="T278" id="Seg_9393" s="T277">n-n:case</ta>
            <ta e="T279" id="Seg_9394" s="T278">ptcl</ta>
            <ta e="T280" id="Seg_9395" s="T279">v-v&gt;v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T281" id="Seg_9396" s="T280">adj</ta>
            <ta e="T282" id="Seg_9397" s="T281">v-v&gt;v-v&gt;adv</ta>
            <ta e="T283" id="Seg_9398" s="T282">v-v&gt;ptcp</ta>
            <ta e="T284" id="Seg_9399" s="T283">n-n&gt;adj</ta>
            <ta e="T286" id="Seg_9400" s="T285">ptcl</ta>
            <ta e="T287" id="Seg_9401" s="T286">v-v&gt;v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T288" id="Seg_9402" s="T287">adv</ta>
            <ta e="T289" id="Seg_9403" s="T288">n-n:case-n:poss</ta>
            <ta e="T290" id="Seg_9404" s="T289">dem-adj&gt;adv</ta>
            <ta e="T291" id="Seg_9405" s="T290">v-v:tense-v:mood-v:pn</ta>
            <ta e="T292" id="Seg_9406" s="T291">n-n:case-n:poss</ta>
            <ta e="T293" id="Seg_9407" s="T292">n-n:case</ta>
            <ta e="T294" id="Seg_9408" s="T293">ptcl</ta>
            <ta e="T295" id="Seg_9409" s="T294">v-v&gt;v-v:mood.pn</ta>
            <ta e="T296" id="Seg_9410" s="T295">emphpro</ta>
            <ta e="T299" id="Seg_9411" s="T298">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T300" id="Seg_9412" s="T299">conj</ta>
            <ta e="T301" id="Seg_9413" s="T300">adv</ta>
            <ta e="T303" id="Seg_9414" s="T302">v-v:tense-v:pn</ta>
            <ta e="T304" id="Seg_9415" s="T303">n-n:case</ta>
            <ta e="T305" id="Seg_9416" s="T304">n-n:case-n:poss</ta>
            <ta e="T306" id="Seg_9417" s="T305">v-v&gt;v-v&gt;adv</ta>
            <ta e="T307" id="Seg_9418" s="T306">adv</ta>
            <ta e="T308" id="Seg_9419" s="T307">v-v&gt;adv</ta>
            <ta e="T309" id="Seg_9420" s="T308">n-n:case-n:poss</ta>
            <ta e="T310" id="Seg_9421" s="T309">n-n:case</ta>
            <ta e="T311" id="Seg_9422" s="T310">adv</ta>
            <ta e="T312" id="Seg_9423" s="T311">ptcl</ta>
            <ta e="T313" id="Seg_9424" s="T312">v-v:tense-v:mood-v:pn</ta>
            <ta e="T314" id="Seg_9425" s="T313">adv</ta>
            <ta e="T315" id="Seg_9426" s="T314">adv</ta>
            <ta e="T316" id="Seg_9427" s="T315">v-v&gt;n-n:case-n:obl.poss</ta>
            <ta e="T317" id="Seg_9428" s="T316">conj</ta>
            <ta e="T318" id="Seg_9429" s="T317">adj-adj&gt;adv</ta>
            <ta e="T319" id="Seg_9430" s="T318">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T320" id="Seg_9431" s="T319">conj</ta>
            <ta e="T321" id="Seg_9432" s="T320">adj-adj&gt;adv</ta>
            <ta e="T322" id="Seg_9433" s="T321">num</ta>
            <ta e="T323" id="Seg_9434" s="T322">n-n:case-n&gt;adj</ta>
            <ta e="T324" id="Seg_9435" s="T323">n</ta>
            <ta e="T325" id="Seg_9436" s="T324">pp-n:case</ta>
            <ta e="T326" id="Seg_9437" s="T325">ptcl</ta>
            <ta e="T327" id="Seg_9438" s="T326">v-v&gt;ptcp</ta>
            <ta e="T328" id="Seg_9439" s="T327">v-v:tense-v:mood-v:pn</ta>
            <ta e="T329" id="Seg_9440" s="T328">n</ta>
            <ta e="T330" id="Seg_9441" s="T329">pp-n:case</ta>
            <ta e="T331" id="Seg_9442" s="T330">v-v&gt;adv</ta>
            <ta e="T332" id="Seg_9443" s="T331">dem-adj&gt;adv</ta>
            <ta e="T333" id="Seg_9444" s="T332">v-v:ins-v:pn</ta>
            <ta e="T334" id="Seg_9445" s="T333">ptcl</ta>
            <ta e="T335" id="Seg_9446" s="T334">ptcl</ta>
            <ta e="T336" id="Seg_9447" s="T335">adv-adv:case</ta>
            <ta e="T337" id="Seg_9448" s="T336">n-n:case</ta>
            <ta e="T338" id="Seg_9449" s="T337">n-n:case</ta>
            <ta e="T339" id="Seg_9450" s="T338">num</ta>
            <ta e="T340" id="Seg_9451" s="T339">n-n:case</ta>
            <ta e="T341" id="Seg_9452" s="T340">v-v:tense.mood-v:pn</ta>
            <ta e="T342" id="Seg_9453" s="T341">dem</ta>
            <ta e="T343" id="Seg_9454" s="T342">n-n:ins-pp-n:ins-n:case</ta>
            <ta e="T344" id="Seg_9455" s="T343">n</ta>
            <ta e="T345" id="Seg_9456" s="T344">n</ta>
            <ta e="T346" id="Seg_9457" s="T345">v-v&gt;v-v:ins-v:tense-v:mood-v:pn</ta>
            <ta e="T347" id="Seg_9458" s="T346">n-n:case</ta>
            <ta e="T348" id="Seg_9459" s="T347">ptcl</ta>
            <ta e="T349" id="Seg_9460" s="T348">qv-v:pn</ta>
            <ta e="T350" id="Seg_9461" s="T349">n-n:case</ta>
            <ta e="T351" id="Seg_9462" s="T350">v-v&gt;adv</ta>
            <ta e="T352" id="Seg_9463" s="T351">n-n&gt;v-v&gt;v-v:pn</ta>
            <ta e="T353" id="Seg_9464" s="T352">n-n:case-n:obl.poss</ta>
            <ta e="T354" id="Seg_9465" s="T353">emphpro</ta>
            <ta e="T355" id="Seg_9466" s="T354">ptcl</ta>
            <ta e="T356" id="Seg_9467" s="T355">adv</ta>
            <ta e="T357" id="Seg_9468" s="T356">adj</ta>
            <ta e="T358" id="Seg_9469" s="T357">n-n:case</ta>
            <ta e="T359" id="Seg_9470" s="T358">v-v:tense.mood-v:pn</ta>
            <ta e="T360" id="Seg_9471" s="T359">dem</ta>
            <ta e="T361" id="Seg_9472" s="T360">adj</ta>
            <ta e="T362" id="Seg_9473" s="T361">n-n:ins-n:num-n:ins-n:case</ta>
            <ta e="T363" id="Seg_9474" s="T362">pp</ta>
            <ta e="T364" id="Seg_9475" s="T363">preverb</ta>
            <ta e="T365" id="Seg_9476" s="T364">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T366" id="Seg_9477" s="T365">preverb</ta>
            <ta e="T367" id="Seg_9478" s="T366">v-v&gt;v-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T368" id="Seg_9479" s="T367">n-n:case-n:poss</ta>
            <ta e="T369" id="Seg_9480" s="T368">pp-n:case</ta>
            <ta e="T370" id="Seg_9481" s="T369">v-v:pn</ta>
            <ta e="T371" id="Seg_9482" s="T370">n-n:case</ta>
            <ta e="T372" id="Seg_9483" s="T371">adv</ta>
            <ta e="T373" id="Seg_9484" s="T372">n-n:ins-n:num-n:case</ta>
            <ta e="T374" id="Seg_9485" s="T373">n-n:num-n:case</ta>
            <ta e="T375" id="Seg_9486" s="T374">v-v:tense-v:mood-v:pn</ta>
            <ta e="T376" id="Seg_9487" s="T375">num</ta>
            <ta e="T377" id="Seg_9488" s="T376">n-n:case</ta>
            <ta e="T378" id="Seg_9489" s="T377">ptcl</ta>
            <ta e="T379" id="Seg_9490" s="T378">ptcl</ta>
            <ta e="T380" id="Seg_9491" s="T379">n-n:case</ta>
            <ta e="T381" id="Seg_9492" s="T380">v-v&gt;adv</ta>
            <ta e="T382" id="Seg_9493" s="T381">ptcl-ptcl-%%</ta>
            <ta e="T383" id="Seg_9494" s="T382">v-v:tense-v:mood-v:pn</ta>
            <ta e="T384" id="Seg_9495" s="T383">interrog-n&gt;adj</ta>
            <ta e="T385" id="Seg_9496" s="T384">n-n:case</ta>
            <ta e="T386" id="Seg_9497" s="T385">n-n:ins-v:pn</ta>
            <ta e="T387" id="Seg_9498" s="T386">v-v:pn</ta>
            <ta e="T388" id="Seg_9499" s="T387">n-n:case</ta>
            <ta e="T389" id="Seg_9500" s="T388">ptcl-ptcl-%%</ta>
            <ta e="T390" id="Seg_9501" s="T389">v-v:tense-v:mood-v:pn</ta>
            <ta e="T391" id="Seg_9502" s="T390">pers</ta>
            <ta e="T392" id="Seg_9503" s="T391">ptcl</ta>
            <ta e="T393" id="Seg_9504" s="T392">n-n:ins-n:case</ta>
            <ta e="T394" id="Seg_9505" s="T393">ptcl</ta>
            <ta e="T395" id="Seg_9506" s="T394">v-v:pn</ta>
            <ta e="T396" id="Seg_9507" s="T395">ptcl</ta>
            <ta e="T397" id="Seg_9508" s="T396">interrog-n:case</ta>
            <ta e="T398" id="Seg_9509" s="T397">ptcl</ta>
            <ta e="T399" id="Seg_9510" s="T398">v-v:pn</ta>
            <ta e="T400" id="Seg_9511" s="T399">n-n&gt;adj</ta>
            <ta e="T401" id="Seg_9512" s="T400">v-v&gt;n-n&gt;adj</ta>
            <ta e="T402" id="Seg_9513" s="T401">n-n:case</ta>
            <ta e="T403" id="Seg_9514" s="T402">pp-adv:case</ta>
            <ta e="T404" id="Seg_9515" s="T403">conj</ta>
            <ta e="T405" id="Seg_9516" s="T404">pers</ta>
            <ta e="T406" id="Seg_9517" s="T405">ptcl</ta>
            <ta e="T407" id="Seg_9518" s="T406">interrog-n:case</ta>
            <ta e="T408" id="Seg_9519" s="T407">n-v:ins-v:pn</ta>
            <ta e="T409" id="Seg_9520" s="T408">v-v:pn</ta>
            <ta e="T410" id="Seg_9521" s="T409">dem</ta>
            <ta e="T411" id="Seg_9522" s="T410">n-n:case</ta>
            <ta e="T412" id="Seg_9523" s="T411">dem-adj&gt;adv</ta>
            <ta e="T413" id="Seg_9524" s="T412">v-v:ins-v:pn</ta>
            <ta e="T414" id="Seg_9525" s="T413">pers</ta>
            <ta e="T415" id="Seg_9526" s="T414">ptcl</ta>
            <ta e="T416" id="Seg_9527" s="T415">num</ta>
            <ta e="T417" id="Seg_9528" s="T416">adj-n-n&gt;adj</ta>
            <ta e="T418" id="Seg_9529" s="T417">n-n&gt;n-n:case-v:pn</ta>
            <ta e="T419" id="Seg_9530" s="T418">adv</ta>
            <ta e="T420" id="Seg_9531" s="T419">adj</ta>
            <ta e="T421" id="Seg_9532" s="T420">adj-n-n:case</ta>
            <ta e="T422" id="Seg_9533" s="T421">adv</ta>
            <ta e="T423" id="Seg_9534" s="T422">pers</ta>
            <ta e="T424" id="Seg_9535" s="T423">v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T425" id="Seg_9536" s="T424">dem</ta>
            <ta e="T426" id="Seg_9537" s="T425">adv</ta>
            <ta e="T427" id="Seg_9538" s="T426">v-v&gt;ptcp</ta>
            <ta e="T428" id="Seg_9539" s="T427">n-n:ins-n:num-n:case</ta>
            <ta e="T429" id="Seg_9540" s="T428">ptcl</ta>
            <ta e="T430" id="Seg_9541" s="T429">ptcl</ta>
            <ta e="T431" id="Seg_9542" s="T430">n-n&gt;adj</ta>
            <ta e="T432" id="Seg_9543" s="T431">v-v:tense.mood-v:pn</ta>
            <ta e="T433" id="Seg_9544" s="T432">dem</ta>
            <ta e="T434" id="Seg_9545" s="T433">n-n:num-n:ins-n:case</ta>
            <ta e="T435" id="Seg_9546" s="T434">pp-adv:case</ta>
            <ta e="T436" id="Seg_9547" s="T435">num</ta>
            <ta e="T437" id="Seg_9548" s="T436">n-n&gt;n-n&gt;adj</ta>
            <ta e="T438" id="Seg_9549" s="T437">n-n&gt;adj</ta>
            <ta e="T439" id="Seg_9550" s="T438">n-n&gt;adj</ta>
            <ta e="T440" id="Seg_9551" s="T439">n-n:case</ta>
            <ta e="T441" id="Seg_9552" s="T440">v-v:tense.mood-v:pn</ta>
            <ta e="T442" id="Seg_9553" s="T441">dem-adj&gt;adv</ta>
            <ta e="T443" id="Seg_9554" s="T442">n-n:case</ta>
            <ta e="T444" id="Seg_9555" s="T443">v-v:tense-v:pn</ta>
            <ta e="T445" id="Seg_9556" s="T444">v-v:inf</ta>
            <ta e="T446" id="Seg_9557" s="T445">v-v:tense-v:pn</ta>
            <ta e="T447" id="Seg_9558" s="T446">n-n:case</ta>
            <ta e="T448" id="Seg_9559" s="T447">n-n:case-n:poss</ta>
            <ta e="T449" id="Seg_9560" s="T448">pp-n:case</ta>
            <ta e="T450" id="Seg_9561" s="T449">v-v&gt;adv</ta>
            <ta e="T451" id="Seg_9562" s="T450">n-n:case</ta>
            <ta e="T452" id="Seg_9563" s="T451">dem</ta>
            <ta e="T453" id="Seg_9564" s="T452">v-v&gt;v-v&gt;v-v&gt;v-v&gt;n-n&gt;adv</ta>
            <ta e="T454" id="Seg_9565" s="T453">n-n:case</ta>
            <ta e="T455" id="Seg_9566" s="T454">n-n:case</ta>
            <ta e="T456" id="Seg_9567" s="T455">pp-n:case</ta>
            <ta e="T457" id="Seg_9568" s="T456">ptcl</ta>
            <ta e="T458" id="Seg_9569" s="T457">v-v&gt;v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T459" id="Seg_9570" s="T458">v-v:tense-v:mood-v:pn</ta>
            <ta e="T460" id="Seg_9571" s="T459">n-n:case</ta>
            <ta e="T461" id="Seg_9572" s="T460">n-n:case</ta>
            <ta e="T462" id="Seg_9573" s="T461">v-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T463" id="Seg_9574" s="T462">v-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T464" id="Seg_9575" s="T463">n-n:case</ta>
            <ta e="T465" id="Seg_9576" s="T464">conj</ta>
            <ta e="T466" id="Seg_9577" s="T465">v-v:tense-v:pn</ta>
            <ta e="T467" id="Seg_9578" s="T466">n-n:case</ta>
            <ta e="T468" id="Seg_9579" s="T467">v-v:inf</ta>
            <ta e="T469" id="Seg_9580" s="T468">pro</ta>
            <ta e="T470" id="Seg_9581" s="T469">v-v:tense-v:mood-v:pn</ta>
            <ta e="T471" id="Seg_9582" s="T470">n-n:case</ta>
            <ta e="T472" id="Seg_9583" s="T471">n-n:case-n:poss</ta>
            <ta e="T473" id="Seg_9584" s="T472">preverb</ta>
            <ta e="T474" id="Seg_9585" s="T473">ptcl</ta>
            <ta e="T475" id="Seg_9586" s="T474">v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T476" id="Seg_9587" s="T475">ptcl-ptcl-%%</ta>
            <ta e="T477" id="Seg_9588" s="T476">v-v:tense-v:pn</ta>
            <ta e="T478" id="Seg_9589" s="T477">dem</ta>
            <ta e="T479" id="Seg_9590" s="T478">n-n&gt;adj</ta>
            <ta e="T480" id="Seg_9591" s="T479">pro</ta>
            <ta e="T481" id="Seg_9592" s="T480">n-n:case</ta>
            <ta e="T482" id="Seg_9593" s="T481">interrog</ta>
            <ta e="T483" id="Seg_9594" s="T482">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T484" id="Seg_9595" s="T483">dem</ta>
            <ta e="T485" id="Seg_9596" s="T484">n-n:ins-n:num-n:case</ta>
            <ta e="T486" id="Seg_9597" s="T485">n-n:case</ta>
            <ta e="T487" id="Seg_9598" s="T486">dem</ta>
            <ta e="T488" id="Seg_9599" s="T487">v-v:tense-v:mood-v:pn</ta>
            <ta e="T489" id="Seg_9600" s="T488">n-n&gt;adj</ta>
            <ta e="T490" id="Seg_9601" s="T489">n-n:ins-n:num-n:case</ta>
            <ta e="T491" id="Seg_9602" s="T490">adv</ta>
            <ta e="T492" id="Seg_9603" s="T491">n-n&gt;v-v:inf-v-v:tense-v:mood-v:pn</ta>
            <ta e="T493" id="Seg_9604" s="T492">n-n:case</ta>
            <ta e="T494" id="Seg_9605" s="T493">ptcl-ptcl-%%</ta>
            <ta e="T495" id="Seg_9606" s="T494">ptcl</ta>
            <ta e="T496" id="Seg_9607" s="T495">v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T497" id="Seg_9608" s="T496">pers</ta>
            <ta e="T498" id="Seg_9609" s="T497">ptcl</ta>
            <ta e="T499" id="Seg_9610" s="T498">pro</ta>
            <ta e="T500" id="Seg_9611" s="T499">n-n&gt;v-v:mood.pn</ta>
            <ta e="T501" id="Seg_9612" s="T500">n-n:case</ta>
            <ta e="T502" id="Seg_9613" s="T501">ptcl-ptcl-%%</ta>
            <ta e="T503" id="Seg_9614" s="T502">ptcl</ta>
            <ta e="T504" id="Seg_9615" s="T503">v-v:tense-v:mood-v:pn</ta>
            <ta e="T505" id="Seg_9616" s="T504">pers</ta>
            <ta e="T506" id="Seg_9617" s="T505">n-n&gt;v-v:inf</ta>
            <ta e="T507" id="Seg_9618" s="T506">ptcl</ta>
            <ta e="T508" id="Seg_9619" s="T507">v-v:pn</ta>
            <ta e="T509" id="Seg_9620" s="T508">dem</ta>
            <ta e="T510" id="Seg_9621" s="T509">adj</ta>
            <ta e="T511" id="Seg_9622" s="T510">n-n&gt;adj</ta>
            <ta e="T512" id="Seg_9623" s="T511">n-n:case</ta>
            <ta e="T513" id="Seg_9624" s="T512">ptcl-ptcl-%%</ta>
            <ta e="T514" id="Seg_9625" s="T513">ptcl</ta>
            <ta e="T515" id="Seg_9626" s="T514">v-v:tense-v:mood-v:pn</ta>
            <ta e="T516" id="Seg_9627" s="T515">pers</ta>
            <ta e="T517" id="Seg_9628" s="T516">interrog</ta>
            <ta e="T518" id="Seg_9629" s="T517">v-v:pn</ta>
            <ta e="T519" id="Seg_9630" s="T518">n-n:num.[n:case]</ta>
            <ta e="T520" id="Seg_9631" s="T519">n-n:case</ta>
            <ta e="T521" id="Seg_9632" s="T520">v-v&gt;v-v&gt;adv</ta>
            <ta e="T522" id="Seg_9633" s="T521">v-v&gt;v-v:mood.pn</ta>
            <ta e="T523" id="Seg_9634" s="T522">n-n:case</ta>
            <ta e="T524" id="Seg_9635" s="T523">v-v:tense-v:pn</ta>
            <ta e="T525" id="Seg_9636" s="T524">ptcl</ta>
            <ta e="T526" id="Seg_9637" s="T525">ptcl</ta>
            <ta e="T527" id="Seg_9638" s="T526">n-n:case</ta>
            <ta e="T528" id="Seg_9639" s="T527">adv</ta>
            <ta e="T529" id="Seg_9640" s="T528">num</ta>
            <ta e="T530" id="Seg_9641" s="T529">n-n:case</ta>
            <ta e="T531" id="Seg_9642" s="T530">ptcl</ta>
            <ta e="T532" id="Seg_9643" s="T531">v-v:tense-v:mood-v:pn</ta>
            <ta e="T533" id="Seg_9644" s="T532">n-n:case</ta>
            <ta e="T534" id="Seg_9645" s="T533">v-v&gt;adv</ta>
            <ta e="T535" id="Seg_9646" s="T534">n-n:case</ta>
            <ta e="T536" id="Seg_9647" s="T535">v-v&gt;adv</ta>
            <ta e="T537" id="Seg_9648" s="T536">n-n:case</ta>
            <ta e="T538" id="Seg_9649" s="T537">ptcl</ta>
            <ta e="T539" id="Seg_9650" s="T538">v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T540" id="Seg_9651" s="T539">ptcl</ta>
            <ta e="T541" id="Seg_9652" s="T540">adv</ta>
            <ta e="T542" id="Seg_9653" s="T541">v-v&gt;adv</ta>
            <ta e="T543" id="Seg_9654" s="T542">ptcl</ta>
            <ta e="T544" id="Seg_9655" s="T543">n-n:case</ta>
            <ta e="T545" id="Seg_9656" s="T544">conj</ta>
            <ta e="T546" id="Seg_9657" s="T545">n-n:case</ta>
            <ta e="T547" id="Seg_9658" s="T546">n-n:case</ta>
            <ta e="T548" id="Seg_9659" s="T547">ptcl</ta>
            <ta e="T549" id="Seg_9660" s="T548">v-v:tense-v:mood-v:pn</ta>
            <ta e="T550" id="Seg_9661" s="T549">n-n:case</ta>
            <ta e="T551" id="Seg_9662" s="T550">n-n:case</ta>
            <ta e="T552" id="Seg_9663" s="T551">v-v&gt;adv</ta>
            <ta e="T553" id="Seg_9664" s="T552">n-n:case</ta>
            <ta e="T554" id="Seg_9665" s="T553">n-n:case</ta>
            <ta e="T555" id="Seg_9666" s="T554">conj</ta>
            <ta e="T556" id="Seg_9667" s="T555">v-v&gt;v-v:ins-v:inf-v-v:tense-v:pn</ta>
            <ta e="T557" id="Seg_9668" s="T556">n-n:case</ta>
            <ta e="T558" id="Seg_9669" s="T557">ptcl-ptcl-%%</ta>
            <ta e="T559" id="Seg_9670" s="T558">ptcl</ta>
            <ta e="T560" id="Seg_9671" s="T559">v-v:tense-v:mood-v:pn</ta>
            <ta e="T561" id="Seg_9672" s="T560">pers</ta>
            <ta e="T562" id="Seg_9673" s="T561">interrog</ta>
            <ta e="T563" id="Seg_9674" s="T562">n-n:ins-n:case</ta>
            <ta e="T564" id="Seg_9675" s="T563">ptcl</ta>
            <ta e="T565" id="Seg_9676" s="T564">v-v&gt;v-v:pn</ta>
            <ta e="T566" id="Seg_9677" s="T565">adv</ta>
            <ta e="T567" id="Seg_9678" s="T566">v-v:inf.poss</ta>
            <ta e="T568" id="Seg_9679" s="T567">interrog</ta>
            <ta e="T569" id="Seg_9680" s="T568">n-n:case</ta>
            <ta e="T570" id="Seg_9681" s="T569">n-n:case</ta>
            <ta e="T571" id="Seg_9682" s="T570">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T572" id="Seg_9683" s="T571">n-n:case</ta>
            <ta e="T573" id="Seg_9684" s="T572">adv</ta>
            <ta e="T574" id="Seg_9685" s="T573">n-n:case</ta>
            <ta e="T575" id="Seg_9686" s="T574">pp-n:case</ta>
            <ta e="T576" id="Seg_9687" s="T575">ptcl</ta>
            <ta e="T577" id="Seg_9688" s="T576">v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T578" id="Seg_9689" s="T577">n-n:obl.poss-n:case</ta>
            <ta e="T579" id="Seg_9690" s="T578">ptcl</ta>
            <ta e="T580" id="Seg_9691" s="T579">v-v&gt;v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T581" id="Seg_9692" s="T580">adj-adj&gt;adj</ta>
            <ta e="T582" id="Seg_9693" s="T581">n-n&gt;adj</ta>
            <ta e="T583" id="Seg_9694" s="T582">ptcl</ta>
            <ta e="T584" id="Seg_9695" s="T583">v-v:tense-v:mood-v:pn</ta>
            <ta e="T585" id="Seg_9696" s="T584">num</ta>
            <ta e="T586" id="Seg_9697" s="T585">n-n:num-n:case</ta>
            <ta e="T587" id="Seg_9698" s="T586">adv</ta>
            <ta e="T588" id="Seg_9699" s="T587">v-v&gt;adv</ta>
            <ta e="T589" id="Seg_9700" s="T588">n-n:ins-n:case</ta>
            <ta e="T590" id="Seg_9701" s="T589">n-n:case</ta>
            <ta e="T591" id="Seg_9702" s="T590">ptcl</ta>
            <ta e="T592" id="Seg_9703" s="T591">v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T593" id="Seg_9704" s="T592">ptcl</ta>
            <ta e="T594" id="Seg_9705" s="T593">n-n:ins-n:case</ta>
            <ta e="T595" id="Seg_9706" s="T594">v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T596" id="Seg_9707" s="T595">conj</ta>
            <ta e="T597" id="Seg_9708" s="T596">v-v:ins-v&gt;v-v:inf-v-v:tense-v:mood-v:pn</ta>
            <ta e="T598" id="Seg_9709" s="T597">n-n:case</ta>
            <ta e="T599" id="Seg_9710" s="T598">v-v:ins-v&gt;v-v&gt;n</ta>
            <ta e="T600" id="Seg_9711" s="T599">pp-n:case</ta>
            <ta e="T601" id="Seg_9712" s="T600">dem-adj&gt;adv</ta>
            <ta e="T602" id="Seg_9713" s="T601">v-v&gt;v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T603" id="Seg_9714" s="T602">ptcl</ta>
            <ta e="T604" id="Seg_9715" s="T603">ptcl</ta>
            <ta e="T605" id="Seg_9716" s="T604">nprop-n&gt;adj</ta>
            <ta e="T606" id="Seg_9717" s="T605">num</ta>
            <ta e="T607" id="Seg_9718" s="T606">n-n&gt;n-n:num-n:case</ta>
            <ta e="T608" id="Seg_9719" s="T607">n-n&gt;v-v&gt;adv</ta>
            <ta e="T609" id="Seg_9720" s="T608">pers-n:case</ta>
            <ta e="T610" id="Seg_9721" s="T609">v-v:tense-v:pn</ta>
            <ta e="T611" id="Seg_9722" s="T610">adj-n-n&gt;adj</ta>
            <ta e="T612" id="Seg_9723" s="T611">n-n&gt;n-n:num-n:case</ta>
            <ta e="T613" id="Seg_9724" s="T612">adj-n-n&gt;adj</ta>
            <ta e="T614" id="Seg_9725" s="T613">n-n&gt;n-n:num-n:case</ta>
            <ta e="T615" id="Seg_9726" s="T614">num</ta>
            <ta e="T616" id="Seg_9727" s="T615">n-n:case-n:poss</ta>
            <ta e="T617" id="Seg_9728" s="T616">v-v:tense-v:mood-v:pn</ta>
            <ta e="T618" id="Seg_9729" s="T617">pro-n:case</ta>
            <ta e="T619" id="Seg_9730" s="T618">n-n&gt;v-v&gt;adv</ta>
            <ta e="T620" id="Seg_9731" s="T619">v-v:tense-v:pn</ta>
            <ta e="T621" id="Seg_9732" s="T620">v-v:ins-v&gt;v-v&gt;adv</ta>
            <ta e="T622" id="Seg_9733" s="T621">adj-adj&gt;adv</ta>
            <ta e="T623" id="Seg_9734" s="T622">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T624" id="Seg_9735" s="T623">adv</ta>
            <ta e="T625" id="Seg_9736" s="T624">preverb</ta>
            <ta e="T626" id="Seg_9737" s="T625">ptcl</ta>
            <ta e="T627" id="Seg_9738" s="T626">v-v:inf-v-v:tense-v:mood-v:pn</ta>
            <ta e="T628" id="Seg_9739" s="T627">n-n:num-n:case</ta>
            <ta e="T629" id="Seg_9740" s="T628">ptcl</ta>
            <ta e="T630" id="Seg_9741" s="T629">ptcl</ta>
            <ta e="T631" id="Seg_9742" s="T630">dem-adj&gt;adv</ta>
            <ta e="T632" id="Seg_9743" s="T631">preverb</ta>
            <ta e="T633" id="Seg_9744" s="T632">adv</ta>
            <ta e="T634" id="Seg_9745" s="T633">v-v:tense.mood-v:pn</ta>
            <ta e="T635" id="Seg_9746" s="T634">adv</ta>
            <ta e="T636" id="Seg_9747" s="T635">adv</ta>
            <ta e="T637" id="Seg_9748" s="T636">n-n:ins-n-n:num-n:ins-n&gt;adj</ta>
            <ta e="T638" id="Seg_9749" s="T637">adv</ta>
            <ta e="T639" id="Seg_9750" s="T638">ptcl</ta>
            <ta e="T640" id="Seg_9751" s="T639">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T641" id="Seg_9752" s="T640">n-n:ins-n:num-n:ins-n:case</ta>
            <ta e="T642" id="Seg_9753" s="T641">v-v&gt;n-n:case</ta>
            <ta e="T643" id="Seg_9754" s="T642">n-n:case</ta>
            <ta e="T644" id="Seg_9755" s="T643">ptcl</ta>
            <ta e="T645" id="Seg_9756" s="T644">adv</ta>
            <ta e="T646" id="Seg_9757" s="T645">ptcl</ta>
            <ta e="T647" id="Seg_9758" s="T646">v-v:inf-v-v&gt;v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T648" id="Seg_9759" s="T647">adv</ta>
            <ta e="T649" id="Seg_9760" s="T648">v-v&gt;n-n:case-n:obl.poss</ta>
            <ta e="T650" id="Seg_9761" s="T649">n-n:case</ta>
            <ta e="T651" id="Seg_9762" s="T650">n-n:case-n:poss</ta>
            <ta e="T652" id="Seg_9763" s="T651">n-n&gt;v-v&gt;v-v&gt;n-n:case</ta>
            <ta e="T653" id="Seg_9764" s="T652">nprop-n&gt;adj</ta>
            <ta e="T654" id="Seg_9765" s="T653">adv</ta>
            <ta e="T655" id="Seg_9766" s="T654">adj</ta>
            <ta e="T656" id="Seg_9767" s="T655">n-n:num-n:ins-n:case</ta>
            <ta e="T658" id="Seg_9768" s="T657">ptcl</ta>
            <ta e="T659" id="Seg_9769" s="T658">v-v:tense-v:mood-v:pn</ta>
            <ta e="T660" id="Seg_9770" s="T659">n-n:case</ta>
            <ta e="T661" id="Seg_9771" s="T660">n-n:obl.poss</ta>
            <ta e="T662" id="Seg_9772" s="T661">pers</ta>
            <ta e="T663" id="Seg_9773" s="T662">conj</ta>
            <ta e="T664" id="Seg_9774" s="T663">dem</ta>
            <ta e="T665" id="Seg_9775" s="T664">n-n:obl.poss-n:case</ta>
            <ta e="T666" id="Seg_9776" s="T665">conj</ta>
            <ta e="T667" id="Seg_9777" s="T666">n-n:case-n:obl.poss</ta>
            <ta e="T668" id="Seg_9778" s="T667">n-n:case</ta>
            <ta e="T669" id="Seg_9779" s="T668">conj</ta>
            <ta e="T670" id="Seg_9780" s="T669">dem</ta>
            <ta e="T671" id="Seg_9781" s="T670">n-n&gt;adj</ta>
            <ta e="T672" id="Seg_9782" s="T671">n-n:case</ta>
            <ta e="T673" id="Seg_9783" s="T672">n-n:case</ta>
            <ta e="T674" id="Seg_9784" s="T673">n-n:case</ta>
            <ta e="T675" id="Seg_9785" s="T674">ptcl</ta>
            <ta e="T676" id="Seg_9786" s="T675">interrog-n:ins-n:case</ta>
            <ta e="T677" id="Seg_9787" s="T676">ptcl</ta>
            <ta e="T678" id="Seg_9788" s="T677">ptcl</ta>
            <ta e="T679" id="Seg_9789" s="T678">v-v:tense-v:pn</ta>
            <ta e="T680" id="Seg_9790" s="T679">adv</ta>
            <ta e="T681" id="Seg_9791" s="T680">v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T682" id="Seg_9792" s="T681">n-n:case</ta>
            <ta e="T683" id="Seg_9793" s="T682">conj</ta>
            <ta e="T684" id="Seg_9794" s="T683">v-v&gt;v-v:pn</ta>
            <ta e="T685" id="Seg_9795" s="T684">n-n:case-n:poss</ta>
            <ta e="T686" id="Seg_9796" s="T685">n-n:case</ta>
            <ta e="T687" id="Seg_9797" s="T686">n-n:case</ta>
            <ta e="T688" id="Seg_9798" s="T687">v-v&gt;ptcp</ta>
            <ta e="T689" id="Seg_9799" s="T688">v-v&gt;v-v&gt;adv</ta>
            <ta e="T690" id="Seg_9800" s="T689">v-v&gt;v-v:pn</ta>
            <ta e="T691" id="Seg_9801" s="T690">n-n:case-n:poss</ta>
            <ta e="T692" id="Seg_9802" s="T691">v-v&gt;adv</ta>
            <ta e="T693" id="Seg_9803" s="T692">n-n:case</ta>
            <ta e="T694" id="Seg_9804" s="T693">pers</ta>
            <ta e="T695" id="Seg_9805" s="T694">interrog</ta>
            <ta e="T696" id="Seg_9806" s="T695">adv</ta>
            <ta e="T697" id="Seg_9807" s="T696">v-v:ins-v:ins-v:pn</ta>
            <ta e="T698" id="Seg_9808" s="T697">pers</ta>
            <ta e="T699" id="Seg_9809" s="T698">v-v&gt;v-v&gt;n-n:case.poss</ta>
            <ta e="T700" id="Seg_9810" s="T699">adv-adv:case</ta>
            <ta e="T701" id="Seg_9811" s="T700">dem</ta>
            <ta e="T702" id="Seg_9812" s="T701">n-n:case</ta>
            <ta e="T703" id="Seg_9813" s="T702">pp-n:case</ta>
            <ta e="T704" id="Seg_9814" s="T703">v-v&gt;v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T705" id="Seg_9815" s="T704">dem</ta>
            <ta e="T706" id="Seg_9816" s="T705">n-n&gt;adv</ta>
            <ta e="T707" id="Seg_9817" s="T706">n-n:case</ta>
            <ta e="T708" id="Seg_9818" s="T707">ptcl</ta>
            <ta e="T709" id="Seg_9819" s="T708">v-v:tense-v:mood-v:pn</ta>
            <ta e="T710" id="Seg_9820" s="T709">adj</ta>
            <ta e="T711" id="Seg_9821" s="T710">n-n:case</ta>
            <ta e="T712" id="Seg_9822" s="T711">n-n:case-n:poss</ta>
            <ta e="T713" id="Seg_9823" s="T712">adv</ta>
            <ta e="T714" id="Seg_9824" s="T713">v-v&gt;adv</ta>
            <ta e="T715" id="Seg_9825" s="T714">n-n:case-n:poss</ta>
            <ta e="T716" id="Seg_9826" s="T715">pp-n:case</ta>
            <ta e="T717" id="Seg_9827" s="T716">v-v&gt;v-v&gt;adv</ta>
            <ta e="T718" id="Seg_9828" s="T717">num</ta>
            <ta e="T719" id="Seg_9829" s="T718">n-n:case</ta>
            <ta e="T720" id="Seg_9830" s="T719">n-n:ins-n&gt;adj</ta>
            <ta e="T721" id="Seg_9831" s="T720">n-n:num-n:case</ta>
            <ta e="T722" id="Seg_9832" s="T721">ptcl</ta>
            <ta e="T723" id="Seg_9833" s="T722">v-v:tense-v:mood-v:pn</ta>
            <ta e="T724" id="Seg_9834" s="T723">n-n:num-n:ins-n&gt;adj</ta>
            <ta e="T725" id="Seg_9835" s="T724">n-n:case-n:poss</ta>
            <ta e="T726" id="Seg_9836" s="T725">ptcl</ta>
            <ta e="T727" id="Seg_9837" s="T726">v-v&gt;v-v&gt;v-v&gt;v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T728" id="Seg_9838" s="T727">adj</ta>
            <ta e="T729" id="Seg_9839" s="T728">n-n:case-n:poss</ta>
            <ta e="T730" id="Seg_9840" s="T729">conj</ta>
            <ta e="T731" id="Seg_9841" s="T730">n-n:case-n:poss</ta>
            <ta e="T732" id="Seg_9842" s="T731">ptcl</ta>
            <ta e="T733" id="Seg_9843" s="T732">v-v&gt;v-v&gt;v-v&gt;v-v&gt;n-n&gt;adv</ta>
            <ta e="T734" id="Seg_9844" s="T733">n-n-n:case-n:poss</ta>
            <ta e="T735" id="Seg_9845" s="T734">v-v:tense-v:pn</ta>
            <ta e="T736" id="Seg_9846" s="T735">ptcl</ta>
            <ta e="T737" id="Seg_9847" s="T736">ptcl</ta>
            <ta e="T739" id="Seg_9848" s="T738">dem-adj&gt;adv</ta>
            <ta e="T740" id="Seg_9849" s="T739">ptcl</ta>
            <ta e="T741" id="Seg_9850" s="T740">v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T742" id="Seg_9851" s="T741">dem-adj&gt;adj</ta>
            <ta e="T743" id="Seg_9852" s="T742">n-n:ins-n:num-n:ins-n:case</ta>
            <ta e="T744" id="Seg_9853" s="T743">n-n:case</ta>
            <ta e="T745" id="Seg_9854" s="T744">conj</ta>
            <ta e="T746" id="Seg_9855" s="T745">v-v&gt;v-v:pn</ta>
            <ta e="T747" id="Seg_9856" s="T746">ptcl</ta>
            <ta e="T748" id="Seg_9857" s="T747">ptcl</ta>
            <ta e="T749" id="Seg_9858" s="T748">nprop-n&gt;adj</ta>
            <ta e="T750" id="Seg_9859" s="T749">n-n&gt;n-n:num-n:case</ta>
            <ta e="T751" id="Seg_9860" s="T750">adv</ta>
            <ta e="T752" id="Seg_9861" s="T751">ptcl</ta>
            <ta e="T753" id="Seg_9862" s="T752">v-v:ins-v:pn</ta>
            <ta e="T754" id="Seg_9863" s="T753">n-n:case</ta>
            <ta e="T755" id="Seg_9864" s="T754">n-n:case-n:poss</ta>
            <ta e="T756" id="Seg_9865" s="T755">preverb</ta>
            <ta e="T757" id="Seg_9866" s="T756">v-v&gt;adv</ta>
            <ta e="T758" id="Seg_9867" s="T757">n-n:case</ta>
            <ta e="T759" id="Seg_9868" s="T758">ptcl</ta>
            <ta e="T760" id="Seg_9869" s="T759">v-v:tense-v:mood-v:pn</ta>
            <ta e="T761" id="Seg_9870" s="T760">n-n:case</ta>
            <ta e="T762" id="Seg_9871" s="T761">v-v&gt;adv</ta>
            <ta e="T763" id="Seg_9872" s="T762">n-n:ins-n:case</ta>
            <ta e="T764" id="Seg_9873" s="T763">n-n:case</ta>
            <ta e="T765" id="Seg_9874" s="T764">conj</ta>
            <ta e="T766" id="Seg_9875" s="T765">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T767" id="Seg_9876" s="T766">n-n:case</ta>
            <ta e="T768" id="Seg_9877" s="T767">adv</ta>
            <ta e="T769" id="Seg_9878" s="T768">n-n:case</ta>
            <ta e="T770" id="Seg_9879" s="T769">ptcl</ta>
            <ta e="T771" id="Seg_9880" s="T770">ptcl</ta>
            <ta e="T772" id="Seg_9881" s="T771">ptcl-ptcl-%%</ta>
            <ta e="T773" id="Seg_9882" s="T772">v-v:pn</ta>
            <ta e="T774" id="Seg_9883" s="T773">pers</ta>
            <ta e="T775" id="Seg_9884" s="T774">interrog</ta>
            <ta e="T776" id="Seg_9885" s="T775">adj-adj&gt;adj</ta>
            <ta e="T777" id="Seg_9886" s="T776">n-n:ins-n:case</ta>
            <ta e="T778" id="Seg_9887" s="T777">ptcl</ta>
            <ta e="T779" id="Seg_9888" s="T778">v-v&gt;v-v:pn</ta>
            <ta e="T780" id="Seg_9889" s="T779">adv</ta>
            <ta e="T781" id="Seg_9890" s="T780">v-v:inf.poss</ta>
            <ta e="T782" id="Seg_9891" s="T781">n-n:case</ta>
            <ta e="T783" id="Seg_9892" s="T782">v-v:tense-v:pn</ta>
            <ta e="T784" id="Seg_9893" s="T783">n-n:case</ta>
            <ta e="T785" id="Seg_9894" s="T784">pp-n:case</ta>
            <ta e="T786" id="Seg_9895" s="T785">n-n:case</ta>
            <ta e="T787" id="Seg_9896" s="T786">preverb</ta>
            <ta e="T788" id="Seg_9897" s="T787">ptcl</ta>
            <ta e="T789" id="Seg_9898" s="T788">v-v:tense.mood-v:pn</ta>
            <ta e="T790" id="Seg_9899" s="T789">n-n:case</ta>
            <ta e="T791" id="Seg_9900" s="T790">adv</ta>
            <ta e="T792" id="Seg_9901" s="T791">preverb</ta>
            <ta e="T793" id="Seg_9902" s="T792">v-v:pn</ta>
            <ta e="T794" id="Seg_9903" s="T793">dem-adj&gt;adv</ta>
            <ta e="T795" id="Seg_9904" s="T794">v-v:tense-v:pn</ta>
            <ta e="T796" id="Seg_9905" s="T795">ptcl</ta>
            <ta e="T797" id="Seg_9906" s="T796">ptcl</ta>
            <ta e="T798" id="Seg_9907" s="T797">n-n:ins-n-n:num-n:ins-n&gt;adj</ta>
            <ta e="T799" id="Seg_9908" s="T798">adv</ta>
            <ta e="T800" id="Seg_9909" s="T799">v-v&gt;ptcp</ta>
            <ta e="T801" id="Seg_9910" s="T800">v-v&gt;v-v&gt;adv</ta>
            <ta e="T802" id="Seg_9911" s="T801">v-v:tense-v:pn</ta>
            <ta e="T803" id="Seg_9912" s="T802">n-n:case</ta>
            <ta e="T804" id="Seg_9913" s="T803">ptcl</ta>
            <ta e="T805" id="Seg_9914" s="T804">dem</ta>
            <ta e="T806" id="Seg_9915" s="T805">n-n:ins-n:obl.poss-n:case</ta>
            <ta e="T807" id="Seg_9916" s="T806">pers</ta>
            <ta e="T808" id="Seg_9917" s="T807">ptcl</ta>
            <ta e="T809" id="Seg_9918" s="T808">ptcl</ta>
            <ta e="T810" id="Seg_9919" s="T809">v-v&gt;v-v&gt;v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T811" id="Seg_9920" s="T810">ptcl</ta>
            <ta e="T812" id="Seg_9921" s="T811">ptcl</ta>
            <ta e="T813" id="Seg_9922" s="T812">n-n:case</ta>
            <ta e="T814" id="Seg_9923" s="T813">n-n:case</ta>
            <ta e="T815" id="Seg_9924" s="T814">ptcl</ta>
            <ta e="T816" id="Seg_9925" s="T815">v-v:mood-v:pn</ta>
            <ta e="T817" id="Seg_9926" s="T816">n-n:case</ta>
            <ta e="T818" id="Seg_9927" s="T817">v-v&gt;adv</ta>
            <ta e="T819" id="Seg_9928" s="T818">n-n:case</ta>
            <ta e="T820" id="Seg_9929" s="T819">v-v:pn</ta>
            <ta e="T821" id="Seg_9930" s="T820">n-n:case</ta>
            <ta e="T822" id="Seg_9931" s="T821">v-v&gt;ptcp</ta>
            <ta e="T823" id="Seg_9932" s="T822">n-n:case</ta>
            <ta e="T824" id="Seg_9933" s="T823">interrog-n:case</ta>
            <ta e="T825" id="Seg_9934" s="T824">interrog-n:case</ta>
            <ta e="T826" id="Seg_9935" s="T825">v-v:tense-v:pn</ta>
            <ta e="T827" id="Seg_9936" s="T826">interrog</ta>
            <ta e="T828" id="Seg_9937" s="T827">ptcl</ta>
            <ta e="T829" id="Seg_9938" s="T828">ptcl</ta>
            <ta e="T830" id="Seg_9939" s="T829">v-v:tense-v:mood-v:pn</ta>
            <ta e="T831" id="Seg_9940" s="T830">num</ta>
            <ta e="T832" id="Seg_9941" s="T831">adj</ta>
            <ta e="T833" id="Seg_9942" s="T832">n-n:case</ta>
            <ta e="T834" id="Seg_9943" s="T833">n-n:case</ta>
            <ta e="T835" id="Seg_9944" s="T834">preverb</ta>
            <ta e="T836" id="Seg_9945" s="T835">conj</ta>
            <ta e="T837" id="Seg_9946" s="T836">v-v:ins-v:pn</ta>
            <ta e="T838" id="Seg_9947" s="T837">ptcl</ta>
            <ta e="T839" id="Seg_9948" s="T838">ptcl</ta>
            <ta e="T840" id="Seg_9949" s="T839">n-n:case</ta>
            <ta e="T841" id="Seg_9950" s="T840">pers</ta>
            <ta e="T842" id="Seg_9951" s="T841">v-%%-v&gt;ptcp</ta>
            <ta e="T843" id="Seg_9952" s="T842">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T844" id="Seg_9953" s="T843">dem-adj&gt;adv</ta>
            <ta e="T845" id="Seg_9954" s="T844">v-v&gt;adv</ta>
            <ta e="T846" id="Seg_9955" s="T845">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T847" id="Seg_9956" s="T846">interrog-n&gt;adj</ta>
            <ta e="T848" id="Seg_9957" s="T847">n-n:case</ta>
            <ta e="T849" id="Seg_9958" s="T848">dem</ta>
            <ta e="T850" id="Seg_9959" s="T849">adj-n-n:num-n:ins-n:case</ta>
            <ta e="T851" id="Seg_9960" s="T850">n-n:case</ta>
            <ta e="T852" id="Seg_9961" s="T851">n-n&gt;v-v:mood-v:pn</ta>
            <ta e="T853" id="Seg_9962" s="T852">pers</ta>
            <ta e="T854" id="Seg_9963" s="T853">ptcl</ta>
            <ta e="T855" id="Seg_9964" s="T854">ptcl</ta>
            <ta e="T856" id="Seg_9965" s="T855">ptcl</ta>
            <ta e="T857" id="Seg_9966" s="T856">interrog-n:case</ta>
            <ta e="T858" id="Seg_9967" s="T857">v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T859" id="Seg_9968" s="T858">dem</ta>
            <ta e="T860" id="Seg_9969" s="T859">adj-adj&gt;adv</ta>
            <ta e="T861" id="Seg_9970" s="T860">v-v&gt;ptcp</ta>
            <ta e="T862" id="Seg_9971" s="T861">n-n:case</ta>
            <ta e="T863" id="Seg_9972" s="T862">ptcl</ta>
            <ta e="T864" id="Seg_9973" s="T863">v-v:tense.mood-v:pn</ta>
            <ta e="T865" id="Seg_9974" s="T864">nprop-n&gt;adj</ta>
            <ta e="T866" id="Seg_9975" s="T865">n-n&gt;n-n:num-n:case</ta>
            <ta e="T867" id="Seg_9976" s="T866">n-n:case</ta>
            <ta e="T868" id="Seg_9977" s="T867">ptcl</ta>
            <ta e="T869" id="Seg_9978" s="T868">qv-v:pn</ta>
            <ta e="T870" id="Seg_9979" s="T869">v-v&gt;n-n:case</ta>
            <ta e="T871" id="Seg_9980" s="T870">pp-n:case</ta>
            <ta e="T872" id="Seg_9981" s="T871">pp-n:case-n:poss</ta>
            <ta e="T873" id="Seg_9982" s="T872">v-v&gt;adv</ta>
            <ta e="T874" id="Seg_9983" s="T873">v-v&gt;ptcp</ta>
            <ta e="T875" id="Seg_9984" s="T874">n-n:ins-n:case-n:poss</ta>
            <ta e="T876" id="Seg_9985" s="T875">n-n:obl.poss-n:case</ta>
            <ta e="T877" id="Seg_9986" s="T876">preverb</ta>
            <ta e="T878" id="Seg_9987" s="T877">ptcl</ta>
            <ta e="T879" id="Seg_9988" s="T878">v-v:tense-v:mood-v:pn</ta>
            <ta e="T880" id="Seg_9989" s="T879">adv</ta>
            <ta e="T881" id="Seg_9990" s="T880">num-num&gt;adv</ta>
            <ta e="T882" id="Seg_9991" s="T881">preverb</ta>
            <ta e="T883" id="Seg_9992" s="T882">v-v:tense-v:mood-v:pn</ta>
            <ta e="T884" id="Seg_9993" s="T883">ptcl</ta>
            <ta e="T885" id="Seg_9994" s="T884">ptcl</ta>
            <ta e="T886" id="Seg_9995" s="T885">n-n&gt;adj</ta>
            <ta e="T887" id="Seg_9996" s="T886">n-n&gt;n-n:num-n:case</ta>
            <ta e="T888" id="Seg_9997" s="T887">nprop-n&gt;adj</ta>
            <ta e="T889" id="Seg_9998" s="T888">dem-adj&gt;adv</ta>
            <ta e="T890" id="Seg_9999" s="T889">v-v:tense-v:pn</ta>
            <ta e="T891" id="Seg_10000" s="T890">adj-n-n:num-n:ins-n:case</ta>
            <ta e="T892" id="Seg_10001" s="T891">n-n:case</ta>
            <ta e="T893" id="Seg_10002" s="T892">preverb</ta>
            <ta e="T894" id="Seg_10003" s="T893">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T895" id="Seg_10004" s="T894">adj-n-n:num-n:ins-n:case</ta>
            <ta e="T896" id="Seg_10005" s="T895">n-n:case</ta>
            <ta e="T897" id="Seg_10006" s="T896">preverb</ta>
            <ta e="T898" id="Seg_10007" s="T897">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T899" id="Seg_10008" s="T898">conj</ta>
            <ta e="T900" id="Seg_10009" s="T899">adv</ta>
            <ta e="T901" id="Seg_10010" s="T900">adj</ta>
            <ta e="T902" id="Seg_10011" s="T901">n-n&gt;n-n:ins-n&gt;adj</ta>
            <ta e="T903" id="Seg_10012" s="T902">n-n:num-n:ins-n:case</ta>
            <ta e="T904" id="Seg_10013" s="T903">ptcl</ta>
            <ta e="T905" id="Seg_10014" s="T904">ptcl</ta>
            <ta e="T906" id="Seg_10015" s="T905">n-n:case-n:poss</ta>
            <ta e="T907" id="Seg_10016" s="T906">adv</ta>
            <ta e="T908" id="Seg_10017" s="T907">ptcl</ta>
            <ta e="T909" id="Seg_10018" s="T908">conj</ta>
            <ta e="T910" id="Seg_10019" s="T909">dem</ta>
            <ta e="T911" id="Seg_10020" s="T910">n-n:num-n:ins-n&gt;adj</ta>
            <ta e="T912" id="Seg_10021" s="T911">n-n:case</ta>
            <ta e="T913" id="Seg_10022" s="T912">adj</ta>
            <ta e="T914" id="Seg_10023" s="T913">n-n:num-n:ins-n&gt;adj</ta>
            <ta e="T915" id="Seg_10024" s="T914">n-n:case</ta>
            <ta e="T916" id="Seg_10025" s="T915">v-v:tense-v:mood-v:pn</ta>
            <ta e="T917" id="Seg_10026" s="T916">n-n:ins-n-n:case</ta>
            <ta e="T918" id="Seg_10027" s="T917">conj</ta>
            <ta e="T919" id="Seg_10028" s="T918">n-n:ins-n-n:case</ta>
            <ta e="T920" id="Seg_10029" s="T919">adv-n-n:case</ta>
            <ta e="T921" id="Seg_10030" s="T920">adj</ta>
            <ta e="T922" id="Seg_10031" s="T921">adv</ta>
            <ta e="T923" id="Seg_10032" s="T922">adv-adv:case</ta>
            <ta e="T924" id="Seg_10033" s="T923">v-v:pn</ta>
            <ta e="T925" id="Seg_10034" s="T924">adv</ta>
            <ta e="T926" id="Seg_10035" s="T925">v-v&gt;adv</ta>
            <ta e="T927" id="Seg_10036" s="T926">dem-adj&gt;adv</ta>
            <ta e="T928" id="Seg_10037" s="T927">preverb</ta>
            <ta e="T929" id="Seg_10038" s="T928">v-v:tense-v:pn</ta>
            <ta e="T930" id="Seg_10039" s="T929">n-n:ins-n:num-n:case</ta>
            <ta e="T931" id="Seg_10040" s="T930">v-v&gt;v-v&gt;adv</ta>
            <ta e="T932" id="Seg_10041" s="T931">pers-n:ins-n:case</ta>
            <ta e="T933" id="Seg_10042" s="T932">v-v:tense-v:pn</ta>
            <ta e="T934" id="Seg_10043" s="T933">conj</ta>
            <ta e="T935" id="Seg_10044" s="T934">dem</ta>
            <ta e="T936" id="Seg_10045" s="T935">n-n:case</ta>
            <ta e="T937" id="Seg_10046" s="T936">adv</ta>
            <ta e="T938" id="Seg_10047" s="T937">v-v:pn</ta>
            <ta e="T939" id="Seg_10048" s="T938">preverb</ta>
            <ta e="T940" id="Seg_10049" s="T939">v-v&gt;adv</ta>
            <ta e="T941" id="Seg_10050" s="T940">n-n:ins-n-n:case</ta>
            <ta e="T942" id="Seg_10051" s="T941">adv</ta>
            <ta e="T943" id="Seg_10052" s="T942">preverb</ta>
            <ta e="T944" id="Seg_10053" s="T943">conj</ta>
            <ta e="T945" id="Seg_10054" s="T944">v-v:ins-v:pn</ta>
            <ta e="T946" id="Seg_10055" s="T945">ptcl</ta>
            <ta e="T947" id="Seg_10056" s="T946">n-n:case</ta>
            <ta e="T948" id="Seg_10057" s="T947">adv</ta>
            <ta e="T949" id="Seg_10058" s="T948">ptcl</ta>
            <ta e="T950" id="Seg_10059" s="T949">v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T951" id="Seg_10060" s="T950">ptcl</ta>
            <ta e="T952" id="Seg_10061" s="T951">qv-v:pn</ta>
            <ta e="T953" id="Seg_10062" s="T952">dem</ta>
            <ta e="T954" id="Seg_10063" s="T953">adv-adv&gt;adj</ta>
            <ta e="T955" id="Seg_10064" s="T954">n-n:ins-n:num-n:case</ta>
            <ta e="T956" id="Seg_10065" s="T955">adj</ta>
            <ta e="T957" id="Seg_10066" s="T956">n-n:ins-n:case</ta>
            <ta e="T958" id="Seg_10067" s="T957">v-v&gt;adv</ta>
            <ta e="T959" id="Seg_10068" s="T958">adv</ta>
            <ta e="T960" id="Seg_10069" s="T959">ptcl</ta>
            <ta e="T961" id="Seg_10070" s="T960">v-v&gt;v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T962" id="Seg_10071" s="T961">conj</ta>
            <ta e="T963" id="Seg_10072" s="T962">v-v:ins-v&gt;v-v:tense-v:pn</ta>
            <ta e="T964" id="Seg_10073" s="T963">adj-n-n:case</ta>
            <ta e="T965" id="Seg_10074" s="T964">n-n:case</ta>
            <ta e="T966" id="Seg_10075" s="T965">n-n:case-n:poss</ta>
            <ta e="T967" id="Seg_10076" s="T966">dem</ta>
            <ta e="T968" id="Seg_10077" s="T967">n-n:case</ta>
            <ta e="T969" id="Seg_10078" s="T968">ptcl</ta>
            <ta e="T970" id="Seg_10079" s="T969">v-v&gt;v-v:tense-v:mood-v:pn</ta>
            <ta e="T971" id="Seg_10080" s="T970">adj</ta>
            <ta e="T972" id="Seg_10081" s="T971">n-n:ins-n-n:case</ta>
            <ta e="T973" id="Seg_10082" s="T972">adv</ta>
            <ta e="T974" id="Seg_10083" s="T973">v-v&gt;v-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T975" id="Seg_10084" s="T974">num-num&gt;num</ta>
            <ta e="T976" id="Seg_10085" s="T975">num</ta>
            <ta e="T977" id="Seg_10086" s="T976">n-n:case</ta>
            <ta e="T978" id="Seg_10087" s="T977">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T979" id="Seg_10088" s="T978">n-n:ins-n-n:case</ta>
            <ta e="T980" id="Seg_10089" s="T979">adv</ta>
            <ta e="T981" id="Seg_10090" s="T980">v-v&gt;v-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T982" id="Seg_10091" s="T981">adj-n-n&gt;adj</ta>
            <ta e="T983" id="Seg_10092" s="T982">n-n&gt;n-n:num-n:case</ta>
            <ta e="T984" id="Seg_10093" s="T983">dem</ta>
            <ta e="T985" id="Seg_10094" s="T984">n-n:case</ta>
            <ta e="T986" id="Seg_10095" s="T985">adv</ta>
            <ta e="T987" id="Seg_10096" s="T986">n-n&gt;v-v:pn</ta>
            <ta e="T988" id="Seg_10097" s="T987">n-n:case</ta>
            <ta e="T989" id="Seg_10098" s="T988">n-n:case-n:poss</ta>
            <ta e="T990" id="Seg_10099" s="T989">pp-n:case</ta>
            <ta e="T991" id="Seg_10100" s="T990">adv</ta>
            <ta e="T992" id="Seg_10101" s="T991">adv</ta>
            <ta e="T993" id="Seg_10102" s="T992">ptcl</ta>
            <ta e="T994" id="Seg_10103" s="T993">v-v:tense-v:mood-v:pn</ta>
            <ta e="T995" id="Seg_10104" s="T994">conj</ta>
            <ta e="T996" id="Seg_10105" s="T995">adv</ta>
            <ta e="T997" id="Seg_10106" s="T996">clit</ta>
            <ta e="T998" id="Seg_10107" s="T997">conj</ta>
            <ta e="T999" id="Seg_10108" s="T998">v-v&gt;v-v:pn</ta>
            <ta e="T1000" id="Seg_10109" s="T999">n-n:case</ta>
            <ta e="T1001" id="Seg_10110" s="T1000">n-n:case</ta>
            <ta e="T1002" id="Seg_10111" s="T1001">pp</ta>
            <ta e="T1003" id="Seg_10112" s="T1002">ptcl</ta>
            <ta e="T1004" id="Seg_10113" s="T1003">n-n:case</ta>
            <ta e="T1005" id="Seg_10114" s="T1004">v-v:tense-v:mood-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T83" id="Seg_10115" s="T82">adj</ta>
            <ta e="T84" id="Seg_10116" s="T83">num</ta>
            <ta e="T85" id="Seg_10117" s="T84">n</ta>
            <ta e="T86" id="Seg_10118" s="T85">num</ta>
            <ta e="T87" id="Seg_10119" s="T86">n</ta>
            <ta e="T88" id="Seg_10120" s="T87">v</ta>
            <ta e="T89" id="Seg_10121" s="T88">n</ta>
            <ta e="T90" id="Seg_10122" s="T89">adv</ta>
            <ta e="T91" id="Seg_10123" s="T90">n</ta>
            <ta e="T92" id="Seg_10124" s="T91">v</ta>
            <ta e="T93" id="Seg_10125" s="T92">n</ta>
            <ta e="T94" id="Seg_10126" s="T93">pro</ta>
            <ta e="T95" id="Seg_10127" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_10128" s="T95">v</ta>
            <ta e="T97" id="Seg_10129" s="T96">n</ta>
            <ta e="T98" id="Seg_10130" s="T97">interrog</ta>
            <ta e="T99" id="Seg_10131" s="T98">v</ta>
            <ta e="T100" id="Seg_10132" s="T99">num</ta>
            <ta e="T101" id="Seg_10133" s="T100">n</ta>
            <ta e="T102" id="Seg_10134" s="T101">n</ta>
            <ta e="T103" id="Seg_10135" s="T102">n</ta>
            <ta e="T104" id="Seg_10136" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_10137" s="T104">v</ta>
            <ta e="T106" id="Seg_10138" s="T105">n</ta>
            <ta e="T107" id="Seg_10139" s="T106">pers</ta>
            <ta e="T108" id="Seg_10140" s="T107">n</ta>
            <ta e="T109" id="Seg_10141" s="T108">ptcp</ta>
            <ta e="T110" id="Seg_10142" s="T109">n</ta>
            <ta e="T111" id="Seg_10143" s="T110">interrog</ta>
            <ta e="T112" id="Seg_10144" s="T111">pers</ta>
            <ta e="T113" id="Seg_10145" s="T112">v</ta>
            <ta e="T114" id="Seg_10146" s="T113">conj</ta>
            <ta e="T115" id="Seg_10147" s="T114">adj</ta>
            <ta e="T116" id="Seg_10148" s="T115">n</ta>
            <ta e="T117" id="Seg_10149" s="T116">pp</ta>
            <ta e="T118" id="Seg_10150" s="T117">adv</ta>
            <ta e="T119" id="Seg_10151" s="T118">v</ta>
            <ta e="T120" id="Seg_10152" s="T119">n</ta>
            <ta e="T121" id="Seg_10153" s="T120">v</ta>
            <ta e="T122" id="Seg_10154" s="T121">conj</ta>
            <ta e="T123" id="Seg_10155" s="T122">adj</ta>
            <ta e="T124" id="Seg_10156" s="T123">adj</ta>
            <ta e="T125" id="Seg_10157" s="T124">adj</ta>
            <ta e="T126" id="Seg_10158" s="T125">v</ta>
            <ta e="T127" id="Seg_10159" s="T126">n</ta>
            <ta e="T128" id="Seg_10160" s="T127">n</ta>
            <ta e="T129" id="Seg_10161" s="T128">n</ta>
            <ta e="T130" id="Seg_10162" s="T129">n</ta>
            <ta e="T131" id="Seg_10163" s="T130">v</ta>
            <ta e="T132" id="Seg_10164" s="T131">pro</ta>
            <ta e="T133" id="Seg_10165" s="T132">adv</ta>
            <ta e="T134" id="Seg_10166" s="T133">n</ta>
            <ta e="T135" id="Seg_10167" s="T134">adv</ta>
            <ta e="T136" id="Seg_10168" s="T135">adv</ta>
            <ta e="T137" id="Seg_10169" s="T136">v</ta>
            <ta e="T138" id="Seg_10170" s="T137">n</ta>
            <ta e="T139" id="Seg_10171" s="T138">ptcl</ta>
            <ta e="T140" id="Seg_10172" s="T139">v</ta>
            <ta e="T141" id="Seg_10173" s="T140">pers</ta>
            <ta e="T142" id="Seg_10174" s="T141">n</ta>
            <ta e="T143" id="Seg_10175" s="T142">interrog</ta>
            <ta e="T144" id="Seg_10176" s="T143">v</ta>
            <ta e="T145" id="Seg_10177" s="T144">n</ta>
            <ta e="T146" id="Seg_10178" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_10179" s="T146">v</ta>
            <ta e="T148" id="Seg_10180" s="T147">pers</ta>
            <ta e="T149" id="Seg_10181" s="T148">ptcl</ta>
            <ta e="T150" id="Seg_10182" s="T149">interrog</ta>
            <ta e="T151" id="Seg_10183" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_10184" s="T151">v</ta>
            <ta e="T153" id="Seg_10185" s="T152">pers</ta>
            <ta e="T154" id="Seg_10186" s="T153">dem</ta>
            <ta e="T155" id="Seg_10187" s="T154">n</ta>
            <ta e="T156" id="Seg_10188" s="T155">adv</ta>
            <ta e="T157" id="Seg_10189" s="T156">n</ta>
            <ta e="T158" id="Seg_10190" s="T157">v</ta>
            <ta e="T159" id="Seg_10191" s="T158">n</ta>
            <ta e="T160" id="Seg_10192" s="T159">ptcl</ta>
            <ta e="T161" id="Seg_10193" s="T160">qv</ta>
            <ta e="T162" id="Seg_10194" s="T161">n</ta>
            <ta e="T163" id="Seg_10195" s="T162">adv</ta>
            <ta e="T164" id="Seg_10196" s="T163">adv</ta>
            <ta e="T165" id="Seg_10197" s="T164">pro</ta>
            <ta e="T166" id="Seg_10198" s="T165">adj</ta>
            <ta e="T167" id="Seg_10199" s="T166">adj</ta>
            <ta e="T168" id="Seg_10200" s="T167">adj</ta>
            <ta e="T169" id="Seg_10201" s="T168">n</ta>
            <ta e="T170" id="Seg_10202" s="T169">preverb</ta>
            <ta e="T171" id="Seg_10203" s="T170">v</ta>
            <ta e="T172" id="Seg_10204" s="T171">ptcl</ta>
            <ta e="T173" id="Seg_10205" s="T172">ptcl</ta>
            <ta e="T174" id="Seg_10206" s="T173">adj</ta>
            <ta e="T175" id="Seg_10207" s="T174">n</ta>
            <ta e="T176" id="Seg_10208" s="T175">n</ta>
            <ta e="T177" id="Seg_10209" s="T176">adj</ta>
            <ta e="T178" id="Seg_10210" s="T177">v</ta>
            <ta e="T179" id="Seg_10211" s="T178">n</ta>
            <ta e="T180" id="Seg_10212" s="T179">adv</ta>
            <ta e="T181" id="Seg_10213" s="T180">v</ta>
            <ta e="T182" id="Seg_10214" s="T181">adv</ta>
            <ta e="T183" id="Seg_10215" s="T182">dem</ta>
            <ta e="T184" id="Seg_10216" s="T183">n</ta>
            <ta e="T185" id="Seg_10217" s="T184">pp</ta>
            <ta e="T186" id="Seg_10218" s="T185">num</ta>
            <ta e="T187" id="Seg_10219" s="T186">adj</ta>
            <ta e="T188" id="Seg_10220" s="T187">n</ta>
            <ta e="T189" id="Seg_10221" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_10222" s="T189">v</ta>
            <ta e="T191" id="Seg_10223" s="T190">num</ta>
            <ta e="T192" id="Seg_10224" s="T191">adj</ta>
            <ta e="T193" id="Seg_10225" s="T192">n</ta>
            <ta e="T194" id="Seg_10226" s="T193">ptcl</ta>
            <ta e="T195" id="Seg_10227" s="T194">v</ta>
            <ta e="T196" id="Seg_10228" s="T195">adj</ta>
            <ta e="T197" id="Seg_10229" s="T196">pro</ta>
            <ta e="T198" id="Seg_10230" s="T197">v</ta>
            <ta e="T199" id="Seg_10231" s="T198">dem</ta>
            <ta e="T200" id="Seg_10232" s="T199">n</ta>
            <ta e="T201" id="Seg_10233" s="T200">emphpro</ta>
            <ta e="T202" id="Seg_10234" s="T201">v</ta>
            <ta e="T203" id="Seg_10235" s="T202">ptcl</ta>
            <ta e="T204" id="Seg_10236" s="T203">v</ta>
            <ta e="T205" id="Seg_10237" s="T204">n</ta>
            <ta e="T206" id="Seg_10238" s="T205">n</ta>
            <ta e="T207" id="Seg_10239" s="T206">n</ta>
            <ta e="T208" id="Seg_10240" s="T207">n</ta>
            <ta e="T209" id="Seg_10241" s="T208">ptcl</ta>
            <ta e="T210" id="Seg_10242" s="T209">v</ta>
            <ta e="T211" id="Seg_10243" s="T210">dem</ta>
            <ta e="T212" id="Seg_10244" s="T211">adv</ta>
            <ta e="T213" id="Seg_10245" s="T212">ptcl</ta>
            <ta e="T214" id="Seg_10246" s="T213">v</ta>
            <ta e="T215" id="Seg_10247" s="T214">n</ta>
            <ta e="T216" id="Seg_10248" s="T215">adv</ta>
            <ta e="T217" id="Seg_10249" s="T216">adv</ta>
            <ta e="T218" id="Seg_10250" s="T217">dem</ta>
            <ta e="T219" id="Seg_10251" s="T218">n</ta>
            <ta e="T220" id="Seg_10252" s="T219">ptcp</ta>
            <ta e="T221" id="Seg_10253" s="T220">n</ta>
            <ta e="T222" id="Seg_10254" s="T221">ptcl</ta>
            <ta e="T223" id="Seg_10255" s="T222">v</ta>
            <ta e="T224" id="Seg_10256" s="T223">adv</ta>
            <ta e="T225" id="Seg_10257" s="T224">v</ta>
            <ta e="T226" id="Seg_10258" s="T225">v</ta>
            <ta e="T227" id="Seg_10259" s="T226">pro</ta>
            <ta e="T228" id="Seg_10260" s="T227">pp</ta>
            <ta e="T229" id="Seg_10261" s="T228">n</ta>
            <ta e="T230" id="Seg_10262" s="T229">ptcl</ta>
            <ta e="T231" id="Seg_10263" s="T230">ptcl</ta>
            <ta e="T232" id="Seg_10264" s="T231">n</ta>
            <ta e="T233" id="Seg_10265" s="T232">pp</ta>
            <ta e="T234" id="Seg_10266" s="T233">dem</ta>
            <ta e="T235" id="Seg_10267" s="T234">n</ta>
            <ta e="T236" id="Seg_10268" s="T235">preverb</ta>
            <ta e="T237" id="Seg_10269" s="T236">v</ta>
            <ta e="T238" id="Seg_10270" s="T237">ptcl</ta>
            <ta e="T239" id="Seg_10271" s="T238">ptcl</ta>
            <ta e="T240" id="Seg_10272" s="T239">adj</ta>
            <ta e="T241" id="Seg_10273" s="T240">n</ta>
            <ta e="T242" id="Seg_10274" s="T241">adj</ta>
            <ta e="T243" id="Seg_10275" s="T242">n</ta>
            <ta e="T244" id="Seg_10276" s="T243">n</ta>
            <ta e="T245" id="Seg_10277" s="T244">n</ta>
            <ta e="T246" id="Seg_10278" s="T245">ptcl</ta>
            <ta e="T247" id="Seg_10279" s="T246">v</ta>
            <ta e="T248" id="Seg_10280" s="T247">ptcl</ta>
            <ta e="T249" id="Seg_10281" s="T248">ptcl</ta>
            <ta e="T250" id="Seg_10282" s="T249">dem</ta>
            <ta e="T251" id="Seg_10283" s="T250">num</ta>
            <ta e="T252" id="Seg_10284" s="T251">adj</ta>
            <ta e="T253" id="Seg_10285" s="T252">adj</ta>
            <ta e="T254" id="Seg_10286" s="T253">n</ta>
            <ta e="T255" id="Seg_10287" s="T254">emphpro</ta>
            <ta e="T256" id="Seg_10288" s="T255">v</ta>
            <ta e="T257" id="Seg_10289" s="T256">ptcl</ta>
            <ta e="T258" id="Seg_10290" s="T257">ptcl</ta>
            <ta e="T259" id="Seg_10291" s="T258">adv</ta>
            <ta e="T260" id="Seg_10292" s="T259">ptcl</ta>
            <ta e="T261" id="Seg_10293" s="T260">v</ta>
            <ta e="T262" id="Seg_10294" s="T261">n</ta>
            <ta e="T263" id="Seg_10295" s="T262">n</ta>
            <ta e="T264" id="Seg_10296" s="T263">v</ta>
            <ta e="T265" id="Seg_10297" s="T264">ptcl</ta>
            <ta e="T266" id="Seg_10298" s="T265">v</ta>
            <ta e="T267" id="Seg_10299" s="T266">n</ta>
            <ta e="T268" id="Seg_10300" s="T267">num</ta>
            <ta e="T269" id="Seg_10301" s="T268">adj</ta>
            <ta e="T270" id="Seg_10302" s="T269">n</ta>
            <ta e="T271" id="Seg_10303" s="T270">adj</ta>
            <ta e="T272" id="Seg_10304" s="T271">n</ta>
            <ta e="T273" id="Seg_10305" s="T272">ptcl</ta>
            <ta e="T274" id="Seg_10306" s="T273">v</ta>
            <ta e="T275" id="Seg_10307" s="T274">adj</ta>
            <ta e="T276" id="Seg_10308" s="T275">v</ta>
            <ta e="T277" id="Seg_10309" s="T276">ptcp</ta>
            <ta e="T278" id="Seg_10310" s="T277">n</ta>
            <ta e="T279" id="Seg_10311" s="T278">ptcl</ta>
            <ta e="T280" id="Seg_10312" s="T279">v</ta>
            <ta e="T281" id="Seg_10313" s="T280">adj</ta>
            <ta e="T282" id="Seg_10314" s="T281">v</ta>
            <ta e="T283" id="Seg_10315" s="T282">ptcp</ta>
            <ta e="T284" id="Seg_10316" s="T283">adj</ta>
            <ta e="T286" id="Seg_10317" s="T285">ptcl</ta>
            <ta e="T287" id="Seg_10318" s="T286">v</ta>
            <ta e="T288" id="Seg_10319" s="T287">adv</ta>
            <ta e="T289" id="Seg_10320" s="T288">n</ta>
            <ta e="T290" id="Seg_10321" s="T289">adv</ta>
            <ta e="T291" id="Seg_10322" s="T290">v</ta>
            <ta e="T292" id="Seg_10323" s="T291">n</ta>
            <ta e="T293" id="Seg_10324" s="T292">n</ta>
            <ta e="T294" id="Seg_10325" s="T293">ptcl</ta>
            <ta e="T295" id="Seg_10326" s="T294">v</ta>
            <ta e="T296" id="Seg_10327" s="T295">emphpro</ta>
            <ta e="T299" id="Seg_10328" s="T298">v</ta>
            <ta e="T300" id="Seg_10329" s="T299">conj</ta>
            <ta e="T301" id="Seg_10330" s="T300">adv</ta>
            <ta e="T303" id="Seg_10331" s="T302">v</ta>
            <ta e="T304" id="Seg_10332" s="T303">n</ta>
            <ta e="T305" id="Seg_10333" s="T304">n</ta>
            <ta e="T306" id="Seg_10334" s="T305">adv</ta>
            <ta e="T307" id="Seg_10335" s="T306">adv</ta>
            <ta e="T308" id="Seg_10336" s="T307">adv</ta>
            <ta e="T309" id="Seg_10337" s="T308">n</ta>
            <ta e="T310" id="Seg_10338" s="T309">n</ta>
            <ta e="T311" id="Seg_10339" s="T310">adv</ta>
            <ta e="T312" id="Seg_10340" s="T311">ptcl</ta>
            <ta e="T313" id="Seg_10341" s="T312">v</ta>
            <ta e="T314" id="Seg_10342" s="T313">adv</ta>
            <ta e="T315" id="Seg_10343" s="T314">adv</ta>
            <ta e="T316" id="Seg_10344" s="T315">n</ta>
            <ta e="T317" id="Seg_10345" s="T316">conj</ta>
            <ta e="T318" id="Seg_10346" s="T317">adv</ta>
            <ta e="T319" id="Seg_10347" s="T318">v</ta>
            <ta e="T320" id="Seg_10348" s="T319">interrog</ta>
            <ta e="T321" id="Seg_10349" s="T320">adj</ta>
            <ta e="T322" id="Seg_10350" s="T321">num</ta>
            <ta e="T323" id="Seg_10351" s="T322">adj</ta>
            <ta e="T324" id="Seg_10352" s="T323">n</ta>
            <ta e="T325" id="Seg_10353" s="T324">pp</ta>
            <ta e="T326" id="Seg_10354" s="T325">ptcl</ta>
            <ta e="T327" id="Seg_10355" s="T326">ptcp</ta>
            <ta e="T328" id="Seg_10356" s="T327">v</ta>
            <ta e="T329" id="Seg_10357" s="T328">n</ta>
            <ta e="T330" id="Seg_10358" s="T329">pp</ta>
            <ta e="T331" id="Seg_10359" s="T330">adv</ta>
            <ta e="T332" id="Seg_10360" s="T331">adv</ta>
            <ta e="T333" id="Seg_10361" s="T332">v</ta>
            <ta e="T334" id="Seg_10362" s="T333">ptcl</ta>
            <ta e="T335" id="Seg_10363" s="T334">ptcl</ta>
            <ta e="T336" id="Seg_10364" s="T335">adv</ta>
            <ta e="T337" id="Seg_10365" s="T336">n</ta>
            <ta e="T338" id="Seg_10366" s="T337">n</ta>
            <ta e="T339" id="Seg_10367" s="T338">num</ta>
            <ta e="T340" id="Seg_10368" s="T339">n</ta>
            <ta e="T341" id="Seg_10369" s="T340">v</ta>
            <ta e="T342" id="Seg_10370" s="T341">dem</ta>
            <ta e="T343" id="Seg_10371" s="T342">n</ta>
            <ta e="T344" id="Seg_10372" s="T343">n</ta>
            <ta e="T345" id="Seg_10373" s="T344">n</ta>
            <ta e="T346" id="Seg_10374" s="T345">v</ta>
            <ta e="T347" id="Seg_10375" s="T346">n</ta>
            <ta e="T348" id="Seg_10376" s="T347">ptcl</ta>
            <ta e="T349" id="Seg_10377" s="T348">conj</ta>
            <ta e="T350" id="Seg_10378" s="T349">n</ta>
            <ta e="T351" id="Seg_10379" s="T350">v</ta>
            <ta e="T352" id="Seg_10380" s="T351">v</ta>
            <ta e="T353" id="Seg_10381" s="T352">n</ta>
            <ta e="T354" id="Seg_10382" s="T353">emphpro</ta>
            <ta e="T355" id="Seg_10383" s="T354">ptcl</ta>
            <ta e="T356" id="Seg_10384" s="T355">adv</ta>
            <ta e="T357" id="Seg_10385" s="T356">adj</ta>
            <ta e="T358" id="Seg_10386" s="T357">n</ta>
            <ta e="T359" id="Seg_10387" s="T358">v</ta>
            <ta e="T360" id="Seg_10388" s="T359">dem</ta>
            <ta e="T361" id="Seg_10389" s="T360">adj</ta>
            <ta e="T362" id="Seg_10390" s="T361">n</ta>
            <ta e="T363" id="Seg_10391" s="T362">pp</ta>
            <ta e="T364" id="Seg_10392" s="T363">preverb</ta>
            <ta e="T365" id="Seg_10393" s="T364">v</ta>
            <ta e="T366" id="Seg_10394" s="T365">adv</ta>
            <ta e="T367" id="Seg_10395" s="T366">adv</ta>
            <ta e="T368" id="Seg_10396" s="T367">n</ta>
            <ta e="T369" id="Seg_10397" s="T368">n</ta>
            <ta e="T370" id="Seg_10398" s="T369">v</ta>
            <ta e="T371" id="Seg_10399" s="T370">n</ta>
            <ta e="T372" id="Seg_10400" s="T371">adv</ta>
            <ta e="T373" id="Seg_10401" s="T372">n</ta>
            <ta e="T374" id="Seg_10402" s="T373">n</ta>
            <ta e="T375" id="Seg_10403" s="T374">v</ta>
            <ta e="T376" id="Seg_10404" s="T375">num</ta>
            <ta e="T377" id="Seg_10405" s="T376">n</ta>
            <ta e="T378" id="Seg_10406" s="T377">ptcl</ta>
            <ta e="T379" id="Seg_10407" s="T378">ptcl</ta>
            <ta e="T380" id="Seg_10408" s="T379">n</ta>
            <ta e="T381" id="Seg_10409" s="T380">adv</ta>
            <ta e="T382" id="Seg_10410" s="T381">ptcl</ta>
            <ta e="T383" id="Seg_10411" s="T382">v</ta>
            <ta e="T384" id="Seg_10412" s="T383">adj</ta>
            <ta e="T385" id="Seg_10413" s="T384">n</ta>
            <ta e="T386" id="Seg_10414" s="T385">v</ta>
            <ta e="T387" id="Seg_10415" s="T386">v</ta>
            <ta e="T388" id="Seg_10416" s="T387">n</ta>
            <ta e="T389" id="Seg_10417" s="T388">n</ta>
            <ta e="T390" id="Seg_10418" s="T389">v</ta>
            <ta e="T391" id="Seg_10419" s="T390">pers</ta>
            <ta e="T392" id="Seg_10420" s="T391">ptcl</ta>
            <ta e="T393" id="Seg_10421" s="T392">n</ta>
            <ta e="T394" id="Seg_10422" s="T393">ptcl</ta>
            <ta e="T395" id="Seg_10423" s="T394">v</ta>
            <ta e="T396" id="Seg_10424" s="T395">ptcl</ta>
            <ta e="T397" id="Seg_10425" s="T396">interrog</ta>
            <ta e="T398" id="Seg_10426" s="T397">ptcl</ta>
            <ta e="T399" id="Seg_10427" s="T398">v</ta>
            <ta e="T400" id="Seg_10428" s="T399">adj</ta>
            <ta e="T401" id="Seg_10429" s="T400">adj</ta>
            <ta e="T402" id="Seg_10430" s="T401">n</ta>
            <ta e="T403" id="Seg_10431" s="T402">pp</ta>
            <ta e="T404" id="Seg_10432" s="T403">conj</ta>
            <ta e="T405" id="Seg_10433" s="T404">pers</ta>
            <ta e="T406" id="Seg_10434" s="T405">ptcl</ta>
            <ta e="T407" id="Seg_10435" s="T406">interrog</ta>
            <ta e="T408" id="Seg_10436" s="T407">n</ta>
            <ta e="T409" id="Seg_10437" s="T408">v</ta>
            <ta e="T410" id="Seg_10438" s="T409">dem</ta>
            <ta e="T411" id="Seg_10439" s="T410">n</ta>
            <ta e="T412" id="Seg_10440" s="T411">adv</ta>
            <ta e="T413" id="Seg_10441" s="T412">v</ta>
            <ta e="T414" id="Seg_10442" s="T413">pers</ta>
            <ta e="T415" id="Seg_10443" s="T414">ptcl</ta>
            <ta e="T416" id="Seg_10444" s="T415">num</ta>
            <ta e="T417" id="Seg_10445" s="T416">adj</ta>
            <ta e="T418" id="Seg_10446" s="T417">n</ta>
            <ta e="T419" id="Seg_10447" s="T418">adv</ta>
            <ta e="T420" id="Seg_10448" s="T419">adj</ta>
            <ta e="T421" id="Seg_10449" s="T420">n</ta>
            <ta e="T422" id="Seg_10450" s="T421">adv</ta>
            <ta e="T423" id="Seg_10451" s="T422">pers</ta>
            <ta e="T424" id="Seg_10452" s="T423">v</ta>
            <ta e="T425" id="Seg_10453" s="T424">dem</ta>
            <ta e="T426" id="Seg_10454" s="T425">adv</ta>
            <ta e="T427" id="Seg_10455" s="T426">ptcp</ta>
            <ta e="T428" id="Seg_10456" s="T427">n</ta>
            <ta e="T429" id="Seg_10457" s="T428">ptcl</ta>
            <ta e="T430" id="Seg_10458" s="T429">ptcl</ta>
            <ta e="T431" id="Seg_10459" s="T430">adj</ta>
            <ta e="T432" id="Seg_10460" s="T431">v</ta>
            <ta e="T433" id="Seg_10461" s="T432">dem</ta>
            <ta e="T434" id="Seg_10462" s="T433">n</ta>
            <ta e="T435" id="Seg_10463" s="T434">pp</ta>
            <ta e="T436" id="Seg_10464" s="T435">num</ta>
            <ta e="T437" id="Seg_10465" s="T436">adj</ta>
            <ta e="T438" id="Seg_10466" s="T437">adj</ta>
            <ta e="T439" id="Seg_10467" s="T438">adj</ta>
            <ta e="T440" id="Seg_10468" s="T439">n</ta>
            <ta e="T441" id="Seg_10469" s="T440">v</ta>
            <ta e="T442" id="Seg_10470" s="T441">dem</ta>
            <ta e="T443" id="Seg_10471" s="T442">n</ta>
            <ta e="T444" id="Seg_10472" s="T443">v</ta>
            <ta e="T445" id="Seg_10473" s="T444">v</ta>
            <ta e="T446" id="Seg_10474" s="T445">v</ta>
            <ta e="T447" id="Seg_10475" s="T446">n</ta>
            <ta e="T448" id="Seg_10476" s="T447">n</ta>
            <ta e="T449" id="Seg_10477" s="T448">pp</ta>
            <ta e="T450" id="Seg_10478" s="T449">adv</ta>
            <ta e="T451" id="Seg_10479" s="T450">n</ta>
            <ta e="T452" id="Seg_10480" s="T451">dem</ta>
            <ta e="T453" id="Seg_10481" s="T452">adv</ta>
            <ta e="T454" id="Seg_10482" s="T453">n</ta>
            <ta e="T455" id="Seg_10483" s="T454">n</ta>
            <ta e="T456" id="Seg_10484" s="T455">pp</ta>
            <ta e="T457" id="Seg_10485" s="T456">ptcl</ta>
            <ta e="T458" id="Seg_10486" s="T457">ptcp</ta>
            <ta e="T459" id="Seg_10487" s="T458">v</ta>
            <ta e="T460" id="Seg_10488" s="T459">n</ta>
            <ta e="T461" id="Seg_10489" s="T460">n</ta>
            <ta e="T462" id="Seg_10490" s="T461">adv</ta>
            <ta e="T463" id="Seg_10491" s="T462">adv</ta>
            <ta e="T464" id="Seg_10492" s="T463">n</ta>
            <ta e="T465" id="Seg_10493" s="T464">conj</ta>
            <ta e="T466" id="Seg_10494" s="T465">v</ta>
            <ta e="T467" id="Seg_10495" s="T466">n</ta>
            <ta e="T468" id="Seg_10496" s="T467">v</ta>
            <ta e="T469" id="Seg_10497" s="T468">pro</ta>
            <ta e="T470" id="Seg_10498" s="T469">v</ta>
            <ta e="T471" id="Seg_10499" s="T470">n</ta>
            <ta e="T472" id="Seg_10500" s="T471">n</ta>
            <ta e="T473" id="Seg_10501" s="T472">preverb</ta>
            <ta e="T474" id="Seg_10502" s="T473">ptcl</ta>
            <ta e="T475" id="Seg_10503" s="T474">v</ta>
            <ta e="T476" id="Seg_10504" s="T475">ptcl</ta>
            <ta e="T477" id="Seg_10505" s="T476">v</ta>
            <ta e="T478" id="Seg_10506" s="T477">dem</ta>
            <ta e="T479" id="Seg_10507" s="T478">adj</ta>
            <ta e="T480" id="Seg_10508" s="T479">dem</ta>
            <ta e="T481" id="Seg_10509" s="T480">n</ta>
            <ta e="T482" id="Seg_10510" s="T481">interrog</ta>
            <ta e="T483" id="Seg_10511" s="T482">v</ta>
            <ta e="T484" id="Seg_10512" s="T483">dem</ta>
            <ta e="T485" id="Seg_10513" s="T484">n</ta>
            <ta e="T486" id="Seg_10514" s="T485">n</ta>
            <ta e="T487" id="Seg_10515" s="T486">dem</ta>
            <ta e="T488" id="Seg_10516" s="T487">v</ta>
            <ta e="T489" id="Seg_10517" s="T488">adj</ta>
            <ta e="T490" id="Seg_10518" s="T489">n</ta>
            <ta e="T491" id="Seg_10519" s="T490">adv</ta>
            <ta e="T492" id="Seg_10520" s="T491">v</ta>
            <ta e="T493" id="Seg_10521" s="T492">n</ta>
            <ta e="T494" id="Seg_10522" s="T493">ptcl</ta>
            <ta e="T495" id="Seg_10523" s="T494">ptcl</ta>
            <ta e="T496" id="Seg_10524" s="T495">v</ta>
            <ta e="T497" id="Seg_10525" s="T496">pers</ta>
            <ta e="T498" id="Seg_10526" s="T497">ptcl</ta>
            <ta e="T499" id="Seg_10527" s="T498">pers</ta>
            <ta e="T500" id="Seg_10528" s="T499">v</ta>
            <ta e="T501" id="Seg_10529" s="T500">n</ta>
            <ta e="T502" id="Seg_10530" s="T501">ptcl</ta>
            <ta e="T503" id="Seg_10531" s="T502">ptcl</ta>
            <ta e="T504" id="Seg_10532" s="T503">v</ta>
            <ta e="T505" id="Seg_10533" s="T504">pers</ta>
            <ta e="T506" id="Seg_10534" s="T505">v</ta>
            <ta e="T507" id="Seg_10535" s="T506">ptcl</ta>
            <ta e="T508" id="Seg_10536" s="T507">v</ta>
            <ta e="T509" id="Seg_10537" s="T508">dem</ta>
            <ta e="T510" id="Seg_10538" s="T509">n</ta>
            <ta e="T511" id="Seg_10539" s="T510">adj</ta>
            <ta e="T512" id="Seg_10540" s="T511">n</ta>
            <ta e="T513" id="Seg_10541" s="T512">ptcl</ta>
            <ta e="T514" id="Seg_10542" s="T513">ptcl</ta>
            <ta e="T515" id="Seg_10543" s="T514">v</ta>
            <ta e="T516" id="Seg_10544" s="T515">pers</ta>
            <ta e="T517" id="Seg_10545" s="T516">interrog</ta>
            <ta e="T518" id="Seg_10546" s="T517">v</ta>
            <ta e="T519" id="Seg_10547" s="T518">n</ta>
            <ta e="T520" id="Seg_10548" s="T519">n</ta>
            <ta e="T521" id="Seg_10549" s="T520">v</ta>
            <ta e="T522" id="Seg_10550" s="T521">v</ta>
            <ta e="T523" id="Seg_10551" s="T522">n</ta>
            <ta e="T524" id="Seg_10552" s="T523">v</ta>
            <ta e="T525" id="Seg_10553" s="T524">ptcl</ta>
            <ta e="T526" id="Seg_10554" s="T525">ptcl</ta>
            <ta e="T527" id="Seg_10555" s="T526">n</ta>
            <ta e="T528" id="Seg_10556" s="T527">adv</ta>
            <ta e="T529" id="Seg_10557" s="T528">num</ta>
            <ta e="T530" id="Seg_10558" s="T529">n</ta>
            <ta e="T531" id="Seg_10559" s="T530">ptcl</ta>
            <ta e="T532" id="Seg_10560" s="T531">v</ta>
            <ta e="T533" id="Seg_10561" s="T532">n</ta>
            <ta e="T534" id="Seg_10562" s="T533">adv</ta>
            <ta e="T535" id="Seg_10563" s="T534">n</ta>
            <ta e="T536" id="Seg_10564" s="T535">adv</ta>
            <ta e="T537" id="Seg_10565" s="T536">n</ta>
            <ta e="T538" id="Seg_10566" s="T537">ptcl</ta>
            <ta e="T539" id="Seg_10567" s="T538">v</ta>
            <ta e="T540" id="Seg_10568" s="T539">ptcl</ta>
            <ta e="T541" id="Seg_10569" s="T540">adv</ta>
            <ta e="T542" id="Seg_10570" s="T541">adv</ta>
            <ta e="T543" id="Seg_10571" s="T542">ptcl</ta>
            <ta e="T544" id="Seg_10572" s="T543">n</ta>
            <ta e="T545" id="Seg_10573" s="T544">conj</ta>
            <ta e="T546" id="Seg_10574" s="T545">n</ta>
            <ta e="T547" id="Seg_10575" s="T546">n</ta>
            <ta e="T548" id="Seg_10576" s="T547">ptcl</ta>
            <ta e="T549" id="Seg_10577" s="T548">v</ta>
            <ta e="T550" id="Seg_10578" s="T549">n</ta>
            <ta e="T551" id="Seg_10579" s="T550">n</ta>
            <ta e="T552" id="Seg_10580" s="T551">adv</ta>
            <ta e="T553" id="Seg_10581" s="T552">n</ta>
            <ta e="T554" id="Seg_10582" s="T553">n</ta>
            <ta e="T555" id="Seg_10583" s="T554">conj</ta>
            <ta e="T556" id="Seg_10584" s="T555">v</ta>
            <ta e="T557" id="Seg_10585" s="T556">n</ta>
            <ta e="T558" id="Seg_10586" s="T557">ptcl</ta>
            <ta e="T559" id="Seg_10587" s="T558">ptcl</ta>
            <ta e="T560" id="Seg_10588" s="T559">v</ta>
            <ta e="T561" id="Seg_10589" s="T560">pers</ta>
            <ta e="T562" id="Seg_10590" s="T561">ptcl</ta>
            <ta e="T563" id="Seg_10591" s="T562">n</ta>
            <ta e="T564" id="Seg_10592" s="T563">ptcl</ta>
            <ta e="T565" id="Seg_10593" s="T564">v</ta>
            <ta e="T566" id="Seg_10594" s="T565">adv</ta>
            <ta e="T567" id="Seg_10595" s="T566">v</ta>
            <ta e="T568" id="Seg_10596" s="T567">interrog</ta>
            <ta e="T569" id="Seg_10597" s="T568">n</ta>
            <ta e="T570" id="Seg_10598" s="T569">n</ta>
            <ta e="T571" id="Seg_10599" s="T570">v</ta>
            <ta e="T572" id="Seg_10600" s="T571">n</ta>
            <ta e="T573" id="Seg_10601" s="T572">adv</ta>
            <ta e="T574" id="Seg_10602" s="T573">n</ta>
            <ta e="T575" id="Seg_10603" s="T574">pp</ta>
            <ta e="T576" id="Seg_10604" s="T575">ptcl</ta>
            <ta e="T577" id="Seg_10605" s="T576">v</ta>
            <ta e="T578" id="Seg_10606" s="T577">n</ta>
            <ta e="T579" id="Seg_10607" s="T578">ptcl</ta>
            <ta e="T580" id="Seg_10608" s="T579">v</ta>
            <ta e="T581" id="Seg_10609" s="T580">adj</ta>
            <ta e="T582" id="Seg_10610" s="T581">adj</ta>
            <ta e="T583" id="Seg_10611" s="T582">ptcl</ta>
            <ta e="T584" id="Seg_10612" s="T583">v</ta>
            <ta e="T585" id="Seg_10613" s="T584">num</ta>
            <ta e="T586" id="Seg_10614" s="T585">n</ta>
            <ta e="T587" id="Seg_10615" s="T586">adv</ta>
            <ta e="T588" id="Seg_10616" s="T587">adv</ta>
            <ta e="T589" id="Seg_10617" s="T588">n</ta>
            <ta e="T590" id="Seg_10618" s="T589">n</ta>
            <ta e="T591" id="Seg_10619" s="T590">ptcl</ta>
            <ta e="T592" id="Seg_10620" s="T591">v</ta>
            <ta e="T593" id="Seg_10621" s="T592">ptcl</ta>
            <ta e="T594" id="Seg_10622" s="T593">n</ta>
            <ta e="T595" id="Seg_10623" s="T594">v</ta>
            <ta e="T596" id="Seg_10624" s="T595">conj</ta>
            <ta e="T597" id="Seg_10625" s="T596">v</ta>
            <ta e="T598" id="Seg_10626" s="T597">n</ta>
            <ta e="T599" id="Seg_10627" s="T598">n</ta>
            <ta e="T600" id="Seg_10628" s="T599">pp</ta>
            <ta e="T601" id="Seg_10629" s="T600">adv</ta>
            <ta e="T602" id="Seg_10630" s="T601">v</ta>
            <ta e="T603" id="Seg_10631" s="T602">ptcl</ta>
            <ta e="T604" id="Seg_10632" s="T603">ptcl</ta>
            <ta e="T605" id="Seg_10633" s="T604">adj</ta>
            <ta e="T606" id="Seg_10634" s="T605">num</ta>
            <ta e="T607" id="Seg_10635" s="T606">n</ta>
            <ta e="T608" id="Seg_10636" s="T607">adv</ta>
            <ta e="T609" id="Seg_10637" s="T608">pers</ta>
            <ta e="T610" id="Seg_10638" s="T609">v</ta>
            <ta e="T611" id="Seg_10639" s="T610">adj</ta>
            <ta e="T612" id="Seg_10640" s="T611">n</ta>
            <ta e="T613" id="Seg_10641" s="T612">adj</ta>
            <ta e="T614" id="Seg_10642" s="T613">n</ta>
            <ta e="T615" id="Seg_10643" s="T614">num</ta>
            <ta e="T616" id="Seg_10644" s="T615">n</ta>
            <ta e="T617" id="Seg_10645" s="T616">v</ta>
            <ta e="T618" id="Seg_10646" s="T617">pro</ta>
            <ta e="T619" id="Seg_10647" s="T618">adv</ta>
            <ta e="T620" id="Seg_10648" s="T619">v</ta>
            <ta e="T621" id="Seg_10649" s="T620">adv</ta>
            <ta e="T622" id="Seg_10650" s="T621">adv</ta>
            <ta e="T623" id="Seg_10651" s="T622">v</ta>
            <ta e="T624" id="Seg_10652" s="T623">adv</ta>
            <ta e="T625" id="Seg_10653" s="T624">preverb</ta>
            <ta e="T626" id="Seg_10654" s="T625">ptcl</ta>
            <ta e="T627" id="Seg_10655" s="T626">v</ta>
            <ta e="T628" id="Seg_10656" s="T627">n</ta>
            <ta e="T629" id="Seg_10657" s="T628">ptcl</ta>
            <ta e="T630" id="Seg_10658" s="T629">ptcl</ta>
            <ta e="T631" id="Seg_10659" s="T630">adv</ta>
            <ta e="T632" id="Seg_10660" s="T631">preverb</ta>
            <ta e="T633" id="Seg_10661" s="T632">adv</ta>
            <ta e="T634" id="Seg_10662" s="T633">v</ta>
            <ta e="T635" id="Seg_10663" s="T634">adv</ta>
            <ta e="T636" id="Seg_10664" s="T635">adv</ta>
            <ta e="T637" id="Seg_10665" s="T636">adj</ta>
            <ta e="T638" id="Seg_10666" s="T637">adv</ta>
            <ta e="T639" id="Seg_10667" s="T638">ptcl</ta>
            <ta e="T640" id="Seg_10668" s="T639">v</ta>
            <ta e="T641" id="Seg_10669" s="T640">n</ta>
            <ta e="T642" id="Seg_10670" s="T641">n</ta>
            <ta e="T643" id="Seg_10671" s="T642">n</ta>
            <ta e="T644" id="Seg_10672" s="T643">ptcl</ta>
            <ta e="T645" id="Seg_10673" s="T644">adv</ta>
            <ta e="T646" id="Seg_10674" s="T645">ptcl</ta>
            <ta e="T647" id="Seg_10675" s="T646">v</ta>
            <ta e="T648" id="Seg_10676" s="T647">adv</ta>
            <ta e="T649" id="Seg_10677" s="T648">n</ta>
            <ta e="T650" id="Seg_10678" s="T649">n</ta>
            <ta e="T651" id="Seg_10679" s="T650">n</ta>
            <ta e="T652" id="Seg_10680" s="T651">n</ta>
            <ta e="T653" id="Seg_10681" s="T652">adj</ta>
            <ta e="T654" id="Seg_10682" s="T653">n</ta>
            <ta e="T655" id="Seg_10683" s="T654">adj</ta>
            <ta e="T656" id="Seg_10684" s="T655">n</ta>
            <ta e="T658" id="Seg_10685" s="T657">ptcl</ta>
            <ta e="T659" id="Seg_10686" s="T658">v</ta>
            <ta e="T660" id="Seg_10687" s="T659">n</ta>
            <ta e="T661" id="Seg_10688" s="T660">n</ta>
            <ta e="T662" id="Seg_10689" s="T661">pers</ta>
            <ta e="T663" id="Seg_10690" s="T662">conj</ta>
            <ta e="T664" id="Seg_10691" s="T663">dem</ta>
            <ta e="T665" id="Seg_10692" s="T664">n</ta>
            <ta e="T666" id="Seg_10693" s="T665">conj</ta>
            <ta e="T667" id="Seg_10694" s="T666">n</ta>
            <ta e="T668" id="Seg_10695" s="T667">n</ta>
            <ta e="T669" id="Seg_10696" s="T668">conj</ta>
            <ta e="T670" id="Seg_10697" s="T669">dem</ta>
            <ta e="T671" id="Seg_10698" s="T670">adj</ta>
            <ta e="T672" id="Seg_10699" s="T671">n</ta>
            <ta e="T673" id="Seg_10700" s="T672">n</ta>
            <ta e="T674" id="Seg_10701" s="T673">n</ta>
            <ta e="T675" id="Seg_10702" s="T674">ptcl</ta>
            <ta e="T676" id="Seg_10703" s="T675">interrog</ta>
            <ta e="T677" id="Seg_10704" s="T676">ptcl</ta>
            <ta e="T678" id="Seg_10705" s="T677">ptcl</ta>
            <ta e="T679" id="Seg_10706" s="T678">v</ta>
            <ta e="T680" id="Seg_10707" s="T679">adv</ta>
            <ta e="T681" id="Seg_10708" s="T680">v</ta>
            <ta e="T682" id="Seg_10709" s="T681">n</ta>
            <ta e="T683" id="Seg_10710" s="T682">conj</ta>
            <ta e="T684" id="Seg_10711" s="T683">v</ta>
            <ta e="T685" id="Seg_10712" s="T684">n</ta>
            <ta e="T686" id="Seg_10713" s="T685">n</ta>
            <ta e="T687" id="Seg_10714" s="T686">n</ta>
            <ta e="T688" id="Seg_10715" s="T687">ptcp</ta>
            <ta e="T689" id="Seg_10716" s="T688">adv</ta>
            <ta e="T690" id="Seg_10717" s="T689">v</ta>
            <ta e="T691" id="Seg_10718" s="T690">n</ta>
            <ta e="T692" id="Seg_10719" s="T691">adv</ta>
            <ta e="T693" id="Seg_10720" s="T692">n</ta>
            <ta e="T694" id="Seg_10721" s="T693">pers</ta>
            <ta e="T695" id="Seg_10722" s="T694">interrog</ta>
            <ta e="T696" id="Seg_10723" s="T695">adv</ta>
            <ta e="T697" id="Seg_10724" s="T696">v</ta>
            <ta e="T698" id="Seg_10725" s="T697">pers</ta>
            <ta e="T699" id="Seg_10726" s="T698">n</ta>
            <ta e="T700" id="Seg_10727" s="T699">adv</ta>
            <ta e="T701" id="Seg_10728" s="T700">dem</ta>
            <ta e="T702" id="Seg_10729" s="T701">n</ta>
            <ta e="T703" id="Seg_10730" s="T702">pp</ta>
            <ta e="T704" id="Seg_10731" s="T703">v</ta>
            <ta e="T705" id="Seg_10732" s="T704">dem</ta>
            <ta e="T706" id="Seg_10733" s="T705">adv</ta>
            <ta e="T707" id="Seg_10734" s="T706">n</ta>
            <ta e="T708" id="Seg_10735" s="T707">ptcl</ta>
            <ta e="T709" id="Seg_10736" s="T708">v</ta>
            <ta e="T710" id="Seg_10737" s="T709">adj</ta>
            <ta e="T711" id="Seg_10738" s="T710">n</ta>
            <ta e="T712" id="Seg_10739" s="T711">n</ta>
            <ta e="T713" id="Seg_10740" s="T712">adv</ta>
            <ta e="T714" id="Seg_10741" s="T713">v</ta>
            <ta e="T715" id="Seg_10742" s="T714">n</ta>
            <ta e="T716" id="Seg_10743" s="T715">pp</ta>
            <ta e="T717" id="Seg_10744" s="T716">adv</ta>
            <ta e="T718" id="Seg_10745" s="T717">num</ta>
            <ta e="T719" id="Seg_10746" s="T718">n</ta>
            <ta e="T720" id="Seg_10747" s="T719">adj</ta>
            <ta e="T721" id="Seg_10748" s="T720">n</ta>
            <ta e="T722" id="Seg_10749" s="T721">ptcl</ta>
            <ta e="T723" id="Seg_10750" s="T722">v</ta>
            <ta e="T724" id="Seg_10751" s="T723">adj</ta>
            <ta e="T725" id="Seg_10752" s="T724">n</ta>
            <ta e="T726" id="Seg_10753" s="T725">ptcl</ta>
            <ta e="T727" id="Seg_10754" s="T726">v</ta>
            <ta e="T728" id="Seg_10755" s="T727">adj</ta>
            <ta e="T729" id="Seg_10756" s="T728">n</ta>
            <ta e="T730" id="Seg_10757" s="T729">ptcl</ta>
            <ta e="T731" id="Seg_10758" s="T730">n</ta>
            <ta e="T732" id="Seg_10759" s="T731">ptcl</ta>
            <ta e="T733" id="Seg_10760" s="T732">adv</ta>
            <ta e="T734" id="Seg_10761" s="T733">n</ta>
            <ta e="T735" id="Seg_10762" s="T734">v</ta>
            <ta e="T736" id="Seg_10763" s="T735">ptcl</ta>
            <ta e="T737" id="Seg_10764" s="T736">ptcl</ta>
            <ta e="T739" id="Seg_10765" s="T738">adv</ta>
            <ta e="T740" id="Seg_10766" s="T739">ptcl</ta>
            <ta e="T741" id="Seg_10767" s="T740">v</ta>
            <ta e="T742" id="Seg_10768" s="T741">adj</ta>
            <ta e="T743" id="Seg_10769" s="T742">n</ta>
            <ta e="T744" id="Seg_10770" s="T743">n</ta>
            <ta e="T745" id="Seg_10771" s="T744">conj</ta>
            <ta e="T746" id="Seg_10772" s="T745">v</ta>
            <ta e="T747" id="Seg_10773" s="T746">ptcl</ta>
            <ta e="T748" id="Seg_10774" s="T747">ptcl</ta>
            <ta e="T749" id="Seg_10775" s="T748">adj</ta>
            <ta e="T750" id="Seg_10776" s="T749">n</ta>
            <ta e="T751" id="Seg_10777" s="T750">adv</ta>
            <ta e="T752" id="Seg_10778" s="T751">ptcl</ta>
            <ta e="T753" id="Seg_10779" s="T752">v</ta>
            <ta e="T754" id="Seg_10780" s="T753">n</ta>
            <ta e="T755" id="Seg_10781" s="T754">n</ta>
            <ta e="T756" id="Seg_10782" s="T755">preverb</ta>
            <ta e="T757" id="Seg_10783" s="T756">adv</ta>
            <ta e="T758" id="Seg_10784" s="T757">n</ta>
            <ta e="T759" id="Seg_10785" s="T758">ptcl</ta>
            <ta e="T760" id="Seg_10786" s="T759">v</ta>
            <ta e="T761" id="Seg_10787" s="T760">n</ta>
            <ta e="T762" id="Seg_10788" s="T761">adv</ta>
            <ta e="T763" id="Seg_10789" s="T762">n</ta>
            <ta e="T764" id="Seg_10790" s="T763">n</ta>
            <ta e="T765" id="Seg_10791" s="T764">conj</ta>
            <ta e="T766" id="Seg_10792" s="T765">v</ta>
            <ta e="T767" id="Seg_10793" s="T766">n</ta>
            <ta e="T768" id="Seg_10794" s="T767">adj</ta>
            <ta e="T769" id="Seg_10795" s="T768">n</ta>
            <ta e="T770" id="Seg_10796" s="T769">ptcl</ta>
            <ta e="T771" id="Seg_10797" s="T770">ptcl</ta>
            <ta e="T772" id="Seg_10798" s="T771">ptcl</ta>
            <ta e="T773" id="Seg_10799" s="T772">v</ta>
            <ta e="T774" id="Seg_10800" s="T773">pers</ta>
            <ta e="T775" id="Seg_10801" s="T774">ptcl</ta>
            <ta e="T776" id="Seg_10802" s="T775">adj</ta>
            <ta e="T777" id="Seg_10803" s="T776">n</ta>
            <ta e="T778" id="Seg_10804" s="T777">ptcl</ta>
            <ta e="T779" id="Seg_10805" s="T778">v</ta>
            <ta e="T780" id="Seg_10806" s="T779">adv</ta>
            <ta e="T781" id="Seg_10807" s="T780">v</ta>
            <ta e="T782" id="Seg_10808" s="T781">n</ta>
            <ta e="T783" id="Seg_10809" s="T782">v</ta>
            <ta e="T784" id="Seg_10810" s="T783">n</ta>
            <ta e="T785" id="Seg_10811" s="T784">pp</ta>
            <ta e="T786" id="Seg_10812" s="T785">n</ta>
            <ta e="T787" id="Seg_10813" s="T786">preverb</ta>
            <ta e="T788" id="Seg_10814" s="T787">ptcl</ta>
            <ta e="T789" id="Seg_10815" s="T788">v</ta>
            <ta e="T790" id="Seg_10816" s="T789">n</ta>
            <ta e="T791" id="Seg_10817" s="T790">adv</ta>
            <ta e="T792" id="Seg_10818" s="T791">preverb</ta>
            <ta e="T793" id="Seg_10819" s="T792">v</ta>
            <ta e="T794" id="Seg_10820" s="T793">adv</ta>
            <ta e="T795" id="Seg_10821" s="T794">v</ta>
            <ta e="T796" id="Seg_10822" s="T795">ptcl</ta>
            <ta e="T797" id="Seg_10823" s="T796">ptcl</ta>
            <ta e="T798" id="Seg_10824" s="T797">adj</ta>
            <ta e="T799" id="Seg_10825" s="T798">adv</ta>
            <ta e="T800" id="Seg_10826" s="T799">ptcp</ta>
            <ta e="T801" id="Seg_10827" s="T800">adv</ta>
            <ta e="T802" id="Seg_10828" s="T801">v</ta>
            <ta e="T803" id="Seg_10829" s="T802">n</ta>
            <ta e="T804" id="Seg_10830" s="T803">ptcl</ta>
            <ta e="T805" id="Seg_10831" s="T804">dem</ta>
            <ta e="T806" id="Seg_10832" s="T805">n</ta>
            <ta e="T807" id="Seg_10833" s="T806">pers</ta>
            <ta e="T808" id="Seg_10834" s="T807">ptcl</ta>
            <ta e="T809" id="Seg_10835" s="T808">ptcl</ta>
            <ta e="T810" id="Seg_10836" s="T809">v</ta>
            <ta e="T811" id="Seg_10837" s="T810">ptcl</ta>
            <ta e="T812" id="Seg_10838" s="T811">ptcl</ta>
            <ta e="T813" id="Seg_10839" s="T812">n</ta>
            <ta e="T814" id="Seg_10840" s="T813">n</ta>
            <ta e="T815" id="Seg_10841" s="T814">ptcl</ta>
            <ta e="T816" id="Seg_10842" s="T815">v</ta>
            <ta e="T817" id="Seg_10843" s="T816">n</ta>
            <ta e="T818" id="Seg_10844" s="T817">adv</ta>
            <ta e="T819" id="Seg_10845" s="T818">n</ta>
            <ta e="T820" id="Seg_10846" s="T819">v</ta>
            <ta e="T821" id="Seg_10847" s="T820">n</ta>
            <ta e="T822" id="Seg_10848" s="T821">ptcp</ta>
            <ta e="T823" id="Seg_10849" s="T822">n</ta>
            <ta e="T824" id="Seg_10850" s="T823">interrog</ta>
            <ta e="T825" id="Seg_10851" s="T824">interrog</ta>
            <ta e="T826" id="Seg_10852" s="T825">v</ta>
            <ta e="T827" id="Seg_10853" s="T826">interrog</ta>
            <ta e="T828" id="Seg_10854" s="T827">ptcl</ta>
            <ta e="T829" id="Seg_10855" s="T828">ptcl</ta>
            <ta e="T830" id="Seg_10856" s="T829">v</ta>
            <ta e="T831" id="Seg_10857" s="T830">num</ta>
            <ta e="T832" id="Seg_10858" s="T831">adj</ta>
            <ta e="T833" id="Seg_10859" s="T832">n</ta>
            <ta e="T834" id="Seg_10860" s="T833">n</ta>
            <ta e="T835" id="Seg_10861" s="T834">adv</ta>
            <ta e="T836" id="Seg_10862" s="T835">conj</ta>
            <ta e="T837" id="Seg_10863" s="T836">v</ta>
            <ta e="T838" id="Seg_10864" s="T837">ptcl</ta>
            <ta e="T839" id="Seg_10865" s="T838">ptcl</ta>
            <ta e="T840" id="Seg_10866" s="T839">n</ta>
            <ta e="T841" id="Seg_10867" s="T840">pers</ta>
            <ta e="T842" id="Seg_10868" s="T841">ptcp</ta>
            <ta e="T843" id="Seg_10869" s="T842">v</ta>
            <ta e="T844" id="Seg_10870" s="T843">adv</ta>
            <ta e="T845" id="Seg_10871" s="T844">adv</ta>
            <ta e="T846" id="Seg_10872" s="T845">v</ta>
            <ta e="T847" id="Seg_10873" s="T846">adj</ta>
            <ta e="T848" id="Seg_10874" s="T847">n</ta>
            <ta e="T849" id="Seg_10875" s="T848">dem</ta>
            <ta e="T850" id="Seg_10876" s="T849">n</ta>
            <ta e="T851" id="Seg_10877" s="T850">n</ta>
            <ta e="T852" id="Seg_10878" s="T851">v</ta>
            <ta e="T853" id="Seg_10879" s="T852">pers</ta>
            <ta e="T854" id="Seg_10880" s="T853">ptcl</ta>
            <ta e="T855" id="Seg_10881" s="T854">ptcl</ta>
            <ta e="T856" id="Seg_10882" s="T855">ptcl</ta>
            <ta e="T857" id="Seg_10883" s="T856">interrog</ta>
            <ta e="T858" id="Seg_10884" s="T857">v</ta>
            <ta e="T859" id="Seg_10885" s="T858">dem</ta>
            <ta e="T860" id="Seg_10886" s="T859">adv</ta>
            <ta e="T861" id="Seg_10887" s="T860">ptcp</ta>
            <ta e="T862" id="Seg_10888" s="T861">n</ta>
            <ta e="T863" id="Seg_10889" s="T862">ptcl</ta>
            <ta e="T864" id="Seg_10890" s="T863">v</ta>
            <ta e="T865" id="Seg_10891" s="T864">adj</ta>
            <ta e="T866" id="Seg_10892" s="T865">n</ta>
            <ta e="T867" id="Seg_10893" s="T866">n</ta>
            <ta e="T868" id="Seg_10894" s="T867">ptcl</ta>
            <ta e="T869" id="Seg_10895" s="T868">qv</ta>
            <ta e="T870" id="Seg_10896" s="T869">n</ta>
            <ta e="T871" id="Seg_10897" s="T870">pp</ta>
            <ta e="T872" id="Seg_10898" s="T871">pp</ta>
            <ta e="T873" id="Seg_10899" s="T872">adv</ta>
            <ta e="T874" id="Seg_10900" s="T873">ptcp</ta>
            <ta e="T875" id="Seg_10901" s="T874">n</ta>
            <ta e="T876" id="Seg_10902" s="T875">n</ta>
            <ta e="T877" id="Seg_10903" s="T876">preverb</ta>
            <ta e="T878" id="Seg_10904" s="T877">ptcl</ta>
            <ta e="T879" id="Seg_10905" s="T878">v</ta>
            <ta e="T880" id="Seg_10906" s="T879">adv</ta>
            <ta e="T881" id="Seg_10907" s="T880">adv</ta>
            <ta e="T882" id="Seg_10908" s="T881">preverb</ta>
            <ta e="T883" id="Seg_10909" s="T882">v</ta>
            <ta e="T884" id="Seg_10910" s="T883">ptcl</ta>
            <ta e="T885" id="Seg_10911" s="T884">ptcl</ta>
            <ta e="T886" id="Seg_10912" s="T885">adj</ta>
            <ta e="T887" id="Seg_10913" s="T886">n</ta>
            <ta e="T888" id="Seg_10914" s="T887">adj</ta>
            <ta e="T889" id="Seg_10915" s="T888">adv</ta>
            <ta e="T890" id="Seg_10916" s="T889">v</ta>
            <ta e="T891" id="Seg_10917" s="T890">n</ta>
            <ta e="T892" id="Seg_10918" s="T891">n</ta>
            <ta e="T893" id="Seg_10919" s="T892">preverb</ta>
            <ta e="T894" id="Seg_10920" s="T893">v</ta>
            <ta e="T895" id="Seg_10921" s="T894">n</ta>
            <ta e="T896" id="Seg_10922" s="T895">n</ta>
            <ta e="T897" id="Seg_10923" s="T896">preverb</ta>
            <ta e="T898" id="Seg_10924" s="T897">v</ta>
            <ta e="T899" id="Seg_10925" s="T898">conj</ta>
            <ta e="T900" id="Seg_10926" s="T899">adv</ta>
            <ta e="T901" id="Seg_10927" s="T900">adj</ta>
            <ta e="T902" id="Seg_10928" s="T901">adj</ta>
            <ta e="T903" id="Seg_10929" s="T902">n</ta>
            <ta e="T904" id="Seg_10930" s="T903">ptcl</ta>
            <ta e="T905" id="Seg_10931" s="T904">ptcl</ta>
            <ta e="T906" id="Seg_10932" s="T905">n</ta>
            <ta e="T907" id="Seg_10933" s="T906">adv</ta>
            <ta e="T908" id="Seg_10934" s="T907">ptcl</ta>
            <ta e="T909" id="Seg_10935" s="T908">conj</ta>
            <ta e="T910" id="Seg_10936" s="T909">dem</ta>
            <ta e="T911" id="Seg_10937" s="T910">adj</ta>
            <ta e="T912" id="Seg_10938" s="T911">n</ta>
            <ta e="T913" id="Seg_10939" s="T912">adv</ta>
            <ta e="T914" id="Seg_10940" s="T913">adj</ta>
            <ta e="T915" id="Seg_10941" s="T914">n</ta>
            <ta e="T916" id="Seg_10942" s="T915">v</ta>
            <ta e="T917" id="Seg_10943" s="T916">n</ta>
            <ta e="T918" id="Seg_10944" s="T917">conj</ta>
            <ta e="T919" id="Seg_10945" s="T918">n</ta>
            <ta e="T920" id="Seg_10946" s="T919">n</ta>
            <ta e="T921" id="Seg_10947" s="T920">adj</ta>
            <ta e="T922" id="Seg_10948" s="T921">adv</ta>
            <ta e="T923" id="Seg_10949" s="T922">adv</ta>
            <ta e="T924" id="Seg_10950" s="T923">v</ta>
            <ta e="T925" id="Seg_10951" s="T924">preverb</ta>
            <ta e="T926" id="Seg_10952" s="T925">adv</ta>
            <ta e="T927" id="Seg_10953" s="T926">adv</ta>
            <ta e="T928" id="Seg_10954" s="T927">preverb</ta>
            <ta e="T929" id="Seg_10955" s="T928">v</ta>
            <ta e="T930" id="Seg_10956" s="T929">n</ta>
            <ta e="T931" id="Seg_10957" s="T930">v</ta>
            <ta e="T932" id="Seg_10958" s="T931">pers</ta>
            <ta e="T933" id="Seg_10959" s="T932">v</ta>
            <ta e="T934" id="Seg_10960" s="T933">conj</ta>
            <ta e="T935" id="Seg_10961" s="T934">dem</ta>
            <ta e="T936" id="Seg_10962" s="T935">n</ta>
            <ta e="T937" id="Seg_10963" s="T936">adv</ta>
            <ta e="T938" id="Seg_10964" s="T937">v</ta>
            <ta e="T939" id="Seg_10965" s="T938">preverb</ta>
            <ta e="T940" id="Seg_10966" s="T939">adv</ta>
            <ta e="T941" id="Seg_10967" s="T940">n</ta>
            <ta e="T942" id="Seg_10968" s="T941">adv</ta>
            <ta e="T943" id="Seg_10969" s="T942">preverb</ta>
            <ta e="T944" id="Seg_10970" s="T943">conj</ta>
            <ta e="T945" id="Seg_10971" s="T944">v</ta>
            <ta e="T946" id="Seg_10972" s="T945">ptcl</ta>
            <ta e="T947" id="Seg_10973" s="T946">n</ta>
            <ta e="T948" id="Seg_10974" s="T947">adv</ta>
            <ta e="T949" id="Seg_10975" s="T948">ptcl</ta>
            <ta e="T950" id="Seg_10976" s="T949">v</ta>
            <ta e="T951" id="Seg_10977" s="T950">ptcl</ta>
            <ta e="T952" id="Seg_10978" s="T951">qv</ta>
            <ta e="T953" id="Seg_10979" s="T952">dem</ta>
            <ta e="T954" id="Seg_10980" s="T953">adj</ta>
            <ta e="T955" id="Seg_10981" s="T954">n</ta>
            <ta e="T956" id="Seg_10982" s="T955">adj</ta>
            <ta e="T957" id="Seg_10983" s="T956">n</ta>
            <ta e="T958" id="Seg_10984" s="T957">adv</ta>
            <ta e="T959" id="Seg_10985" s="T958">adv</ta>
            <ta e="T960" id="Seg_10986" s="T959">ptcl</ta>
            <ta e="T961" id="Seg_10987" s="T960">v</ta>
            <ta e="T962" id="Seg_10988" s="T961">conj</ta>
            <ta e="T963" id="Seg_10989" s="T962">v</ta>
            <ta e="T964" id="Seg_10990" s="T963">n</ta>
            <ta e="T965" id="Seg_10991" s="T964">n</ta>
            <ta e="T966" id="Seg_10992" s="T965">n</ta>
            <ta e="T967" id="Seg_10993" s="T966">dem</ta>
            <ta e="T968" id="Seg_10994" s="T967">n</ta>
            <ta e="T969" id="Seg_10995" s="T968">ptcl</ta>
            <ta e="T970" id="Seg_10996" s="T969">v</ta>
            <ta e="T971" id="Seg_10997" s="T970">adj</ta>
            <ta e="T972" id="Seg_10998" s="T971">n</ta>
            <ta e="T973" id="Seg_10999" s="T972">adv</ta>
            <ta e="T974" id="Seg_11000" s="T973">adv</ta>
            <ta e="T975" id="Seg_11001" s="T974">num</ta>
            <ta e="T976" id="Seg_11002" s="T975">num</ta>
            <ta e="T977" id="Seg_11003" s="T976">n</ta>
            <ta e="T978" id="Seg_11004" s="T977">v</ta>
            <ta e="T979" id="Seg_11005" s="T978">n</ta>
            <ta e="T980" id="Seg_11006" s="T979">adv</ta>
            <ta e="T981" id="Seg_11007" s="T980">adv</ta>
            <ta e="T982" id="Seg_11008" s="T981">adj</ta>
            <ta e="T983" id="Seg_11009" s="T982">n</ta>
            <ta e="T984" id="Seg_11010" s="T983">dem</ta>
            <ta e="T985" id="Seg_11011" s="T984">n</ta>
            <ta e="T986" id="Seg_11012" s="T985">adv</ta>
            <ta e="T987" id="Seg_11013" s="T986">n</ta>
            <ta e="T988" id="Seg_11014" s="T987">n</ta>
            <ta e="T989" id="Seg_11015" s="T988">n</ta>
            <ta e="T990" id="Seg_11016" s="T989">pp</ta>
            <ta e="T991" id="Seg_11017" s="T990">adv</ta>
            <ta e="T992" id="Seg_11018" s="T991">adv</ta>
            <ta e="T993" id="Seg_11019" s="T992">ptcl</ta>
            <ta e="T994" id="Seg_11020" s="T993">v</ta>
            <ta e="T995" id="Seg_11021" s="T994">conj</ta>
            <ta e="T996" id="Seg_11022" s="T995">adv</ta>
            <ta e="T997" id="Seg_11023" s="T996">clit</ta>
            <ta e="T998" id="Seg_11024" s="T997">conj</ta>
            <ta e="T999" id="Seg_11025" s="T998">v</ta>
            <ta e="T1000" id="Seg_11026" s="T999">n</ta>
            <ta e="T1001" id="Seg_11027" s="T1000">n</ta>
            <ta e="T1002" id="Seg_11028" s="T1001">pp</ta>
            <ta e="T1003" id="Seg_11029" s="T1002">dem</ta>
            <ta e="T1004" id="Seg_11030" s="T1003">n</ta>
            <ta e="T1005" id="Seg_11031" s="T1004">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T87" id="Seg_11032" s="T86">np.h:Th</ta>
            <ta e="T89" id="Seg_11033" s="T88">np.h:P</ta>
            <ta e="T93" id="Seg_11034" s="T92">np.h:E</ta>
            <ta e="T94" id="Seg_11035" s="T93">pro.h:Th</ta>
            <ta e="T97" id="Seg_11036" s="T96">np.h:P 0.3.h:Poss</ta>
            <ta e="T101" id="Seg_11037" s="T100">np:Time</ta>
            <ta e="T102" id="Seg_11038" s="T101">np.h:A</ta>
            <ta e="T103" id="Seg_11039" s="T102">np.h:R</ta>
            <ta e="T107" id="Seg_11040" s="T106">np.h:Poss</ta>
            <ta e="T108" id="Seg_11041" s="T107">np.h:Poss</ta>
            <ta e="T110" id="Seg_11042" s="T109">np:Th</ta>
            <ta e="T112" id="Seg_11043" s="T111">pro.h:R</ta>
            <ta e="T113" id="Seg_11044" s="T112">0.2.h:A</ta>
            <ta e="T115" id="Seg_11045" s="T114">np:Th</ta>
            <ta e="T116" id="Seg_11046" s="T115">pp.h:Poss</ta>
            <ta e="T120" id="Seg_11047" s="T119">np:Th</ta>
            <ta e="T121" id="Seg_11048" s="T120">0.3:A</ta>
            <ta e="T123" id="Seg_11049" s="T122">np:Poss</ta>
            <ta e="T125" id="Seg_11050" s="T124">np:Th</ta>
            <ta e="T127" id="Seg_11051" s="T126">np:Th 0.3:Poss</ta>
            <ta e="T128" id="Seg_11052" s="T127">np:Poss</ta>
            <ta e="T129" id="Seg_11053" s="T128">np:Poss</ta>
            <ta e="T132" id="Seg_11054" s="T131">pro:P</ta>
            <ta e="T134" id="Seg_11055" s="T133">np:Th</ta>
            <ta e="T136" id="Seg_11056" s="T135">adv:G</ta>
            <ta e="T137" id="Seg_11057" s="T136">0.3.h:A</ta>
            <ta e="T138" id="Seg_11058" s="T137">np.h:A</ta>
            <ta e="T141" id="Seg_11059" s="T140">pro.h:A</ta>
            <ta e="T142" id="Seg_11060" s="T141">np:Com</ta>
            <ta e="T143" id="Seg_11061" s="T142">pro:G</ta>
            <ta e="T145" id="Seg_11062" s="T144">np.h:A</ta>
            <ta e="T148" id="Seg_11063" s="T147">pro.h:A</ta>
            <ta e="T150" id="Seg_11064" s="T149">pro:G</ta>
            <ta e="T153" id="Seg_11065" s="T152">pro.h:E</ta>
            <ta e="T158" id="Seg_11066" s="T157">0.1.h:A</ta>
            <ta e="T159" id="Seg_11067" s="T158">np.h:E</ta>
            <ta e="T165" id="Seg_11068" s="T164">pro:Th</ta>
            <ta e="T169" id="Seg_11069" s="T168">np:Th</ta>
            <ta e="T171" id="Seg_11070" s="T170">0.3.h:A</ta>
            <ta e="T175" id="Seg_11071" s="T174">np:Poss</ta>
            <ta e="T178" id="Seg_11072" s="T177">0.3:Th</ta>
            <ta e="T179" id="Seg_11073" s="T178">np.h:R</ta>
            <ta e="T181" id="Seg_11074" s="T180">0.3.h:A</ta>
            <ta e="T184" id="Seg_11075" s="T183">pp:L</ta>
            <ta e="T188" id="Seg_11076" s="T187">np:Th</ta>
            <ta e="T193" id="Seg_11077" s="T192">np:Th</ta>
            <ta e="T197" id="Seg_11078" s="T196">pro:Th</ta>
            <ta e="T198" id="Seg_11079" s="T197">0.2.h:A</ta>
            <ta e="T200" id="Seg_11080" s="T199">np:A</ta>
            <ta e="T205" id="Seg_11081" s="T204">np.h:A</ta>
            <ta e="T206" id="Seg_11082" s="T205">np:Th</ta>
            <ta e="T212" id="Seg_11083" s="T211">adv:G</ta>
            <ta e="T214" id="Seg_11084" s="T213">0.3.h:A</ta>
            <ta e="T221" id="Seg_11085" s="T220">np:Th</ta>
            <ta e="T223" id="Seg_11086" s="T222">0.3.h:A</ta>
            <ta e="T224" id="Seg_11087" s="T223">adv:G</ta>
            <ta e="T225" id="Seg_11088" s="T224">0.3.h:A 0.3:Th</ta>
            <ta e="T227" id="Seg_11089" s="T226">pp:Time</ta>
            <ta e="T229" id="Seg_11090" s="T228">np.h:A 0.3.h:Poss</ta>
            <ta e="T232" id="Seg_11091" s="T231">pp:L</ta>
            <ta e="T235" id="Seg_11092" s="T234">np:P</ta>
            <ta e="T243" id="Seg_11093" s="T242">0.3:Poss</ta>
            <ta e="T244" id="Seg_11094" s="T243">np.h:A</ta>
            <ta e="T245" id="Seg_11095" s="T244">np:Th</ta>
            <ta e="T254" id="Seg_11096" s="T253">np:A</ta>
            <ta e="T259" id="Seg_11097" s="T258">adv:Path</ta>
            <ta e="T262" id="Seg_11098" s="T261">np.h:A</ta>
            <ta e="T263" id="Seg_11099" s="T262">np:Th</ta>
            <ta e="T267" id="Seg_11100" s="T266">np.h:A</ta>
            <ta e="T272" id="Seg_11101" s="T271">np:Th</ta>
            <ta e="T274" id="Seg_11102" s="T273">0.3.h:R</ta>
            <ta e="T278" id="Seg_11103" s="T277">np:Th</ta>
            <ta e="T280" id="Seg_11104" s="T279">0.3.h:A 0.3.h:R</ta>
            <ta e="T287" id="Seg_11105" s="T286">0.3.h:A 0.3.h:R</ta>
            <ta e="T289" id="Seg_11106" s="T288">np.h:A</ta>
            <ta e="T292" id="Seg_11107" s="T291">np:Poss</ta>
            <ta e="T293" id="Seg_11108" s="T292">np:Th</ta>
            <ta e="T295" id="Seg_11109" s="T294">0.2.h:A</ta>
            <ta e="T296" id="Seg_11110" s="T295">pro:A</ta>
            <ta e="T301" id="Seg_11111" s="T300">adv:G</ta>
            <ta e="T303" id="Seg_11112" s="T302">0.3:A</ta>
            <ta e="T304" id="Seg_11113" s="T303">np.h:A</ta>
            <ta e="T305" id="Seg_11114" s="T304">np:Th</ta>
            <ta e="T309" id="Seg_11115" s="T308">pro:Poss</ta>
            <ta e="T310" id="Seg_11116" s="T309">np:Th</ta>
            <ta e="T311" id="Seg_11117" s="T310">adv:Path</ta>
            <ta e="T315" id="Seg_11118" s="T314">adv:Path</ta>
            <ta e="T316" id="Seg_11119" s="T315">0.3.h:A</ta>
            <ta e="T319" id="Seg_11120" s="T318">0.3.h:A</ta>
            <ta e="T324" id="Seg_11121" s="T323">pp:L</ta>
            <ta e="T328" id="Seg_11122" s="T327">0.3.h:A</ta>
            <ta e="T329" id="Seg_11123" s="T328">pp:L</ta>
            <ta e="T333" id="Seg_11124" s="T332">0.3.h:E</ta>
            <ta e="T336" id="Seg_11125" s="T335">adv:L</ta>
            <ta e="T337" id="Seg_11126" s="T336">np:Poss</ta>
            <ta e="T338" id="Seg_11127" s="T337">np:L</ta>
            <ta e="T340" id="Seg_11128" s="T339">np:Th</ta>
            <ta e="T343" id="Seg_11129" s="T342">pp:L</ta>
            <ta e="T345" id="Seg_11130" s="T344">np:A</ta>
            <ta e="T347" id="Seg_11131" s="T346">np.h:A</ta>
            <ta e="T350" id="Seg_11132" s="T349">np:G</ta>
            <ta e="T352" id="Seg_11133" s="T351">0.3.h:A</ta>
            <ta e="T353" id="Seg_11134" s="T352">np:L</ta>
            <ta e="T358" id="Seg_11135" s="T357">np:Th</ta>
            <ta e="T362" id="Seg_11136" s="T361">pp:L</ta>
            <ta e="T365" id="Seg_11137" s="T364">0.3.h:A</ta>
            <ta e="T368" id="Seg_11138" s="T367">pp:L</ta>
            <ta e="T370" id="Seg_11139" s="T369">0.3.h:Th</ta>
            <ta e="T371" id="Seg_11140" s="T370">np:So</ta>
            <ta e="T373" id="Seg_11141" s="T372">np.h:A</ta>
            <ta e="T374" id="Seg_11142" s="T373">np.h:A</ta>
            <ta e="T377" id="Seg_11143" s="T376">np.h:A</ta>
            <ta e="T380" id="Seg_11144" s="T379">np.h:G</ta>
            <ta e="T385" id="Seg_11145" s="T384">np:Poss</ta>
            <ta e="T387" id="Seg_11146" s="T386">0.2.h:Th</ta>
            <ta e="T388" id="Seg_11147" s="T387">np.h:A</ta>
            <ta e="T391" id="Seg_11148" s="T390">pro.h:E</ta>
            <ta e="T393" id="Seg_11149" s="T392">np.h:Th</ta>
            <ta e="T397" id="Seg_11150" s="T396">pro.h:Th</ta>
            <ta e="T399" id="Seg_11151" s="T398">0.1.h:E</ta>
            <ta e="T402" id="Seg_11152" s="T401">pp:So</ta>
            <ta e="T405" id="Seg_11153" s="T404">pro.h:Th</ta>
            <ta e="T411" id="Seg_11154" s="T410">np.h:A</ta>
            <ta e="T414" id="Seg_11155" s="T413">pro.h:Th</ta>
            <ta e="T423" id="Seg_11156" s="T422">pro.h:Th</ta>
            <ta e="T428" id="Seg_11157" s="T427">np.h:A</ta>
            <ta e="T431" id="Seg_11158" s="T430">np:Th</ta>
            <ta e="T434" id="Seg_11159" s="T433">pp:L</ta>
            <ta e="T440" id="Seg_11160" s="T439">np:Th</ta>
            <ta e="T441" id="Seg_11161" s="T440">0.3.h:A</ta>
            <ta e="T443" id="Seg_11162" s="T442">np.h:A</ta>
            <ta e="T445" id="Seg_11163" s="T444">v:Th</ta>
            <ta e="T446" id="Seg_11164" s="T445">0.3.h:E</ta>
            <ta e="T447" id="Seg_11165" s="T446">np.h:A</ta>
            <ta e="T448" id="Seg_11166" s="T447">pp:L</ta>
            <ta e="T451" id="Seg_11167" s="T450">np:Th</ta>
            <ta e="T454" id="Seg_11168" s="T453">np:Th</ta>
            <ta e="T455" id="Seg_11169" s="T454">pp:Path</ta>
            <ta e="T459" id="Seg_11170" s="T458">0.3.h:A</ta>
            <ta e="T460" id="Seg_11171" s="T459">np.h:A</ta>
            <ta e="T461" id="Seg_11172" s="T460">np:Th</ta>
            <ta e="T464" id="Seg_11173" s="T463">np:Th</ta>
            <ta e="T467" id="Seg_11174" s="T466">np:P</ta>
            <ta e="T468" id="Seg_11175" s="T467">v:Th</ta>
            <ta e="T470" id="Seg_11176" s="T469">0.3.h:E</ta>
            <ta e="T471" id="Seg_11177" s="T470">np.h:A</ta>
            <ta e="T472" id="Seg_11178" s="T471">np:Th</ta>
            <ta e="T477" id="Seg_11179" s="T476">0.3.h:A</ta>
            <ta e="T479" id="Seg_11180" s="T478">np:P</ta>
            <ta e="T481" id="Seg_11181" s="T480">np:Ins</ta>
            <ta e="T483" id="Seg_11182" s="T482">0.1.h:A</ta>
            <ta e="T485" id="Seg_11183" s="T484">np.h:A</ta>
            <ta e="T486" id="Seg_11184" s="T485">np:P</ta>
            <ta e="T490" id="Seg_11185" s="T489">np.h:A</ta>
            <ta e="T491" id="Seg_11186" s="T490">adv:L</ta>
            <ta e="T493" id="Seg_11187" s="T492">np.h:Th</ta>
            <ta e="T496" id="Seg_11188" s="T495">0.3.h:A</ta>
            <ta e="T497" id="Seg_11189" s="T496">pro.h:A</ta>
            <ta e="T499" id="Seg_11190" s="T498">pro:Com</ta>
            <ta e="T501" id="Seg_11191" s="T500">np.h:A</ta>
            <ta e="T505" id="Seg_11192" s="T504">pro.h:E</ta>
            <ta e="T506" id="Seg_11193" s="T505">v:Th</ta>
            <ta e="T512" id="Seg_11194" s="T511">np.h:A</ta>
            <ta e="T516" id="Seg_11195" s="T515">pro.h:Th</ta>
            <ta e="T519" id="Seg_11196" s="T518">np:Th</ta>
            <ta e="T520" id="Seg_11197" s="T519">np:G</ta>
            <ta e="T522" id="Seg_11198" s="T521">0.2.h:A</ta>
            <ta e="T523" id="Seg_11199" s="T522">np.h:A</ta>
            <ta e="T527" id="Seg_11200" s="T526">np:So</ta>
            <ta e="T528" id="Seg_11201" s="T527">adv:Path</ta>
            <ta e="T530" id="Seg_11202" s="T529">np.h:A</ta>
            <ta e="T533" id="Seg_11203" s="T532">np.h:A</ta>
            <ta e="T535" id="Seg_11204" s="T534">np:P</ta>
            <ta e="T537" id="Seg_11205" s="T536">np:Time</ta>
            <ta e="T544" id="Seg_11206" s="T543">np.h:A</ta>
            <ta e="T546" id="Seg_11207" s="T545">np.h:A</ta>
            <ta e="T547" id="Seg_11208" s="T546">np:G</ta>
            <ta e="T550" id="Seg_11209" s="T549">np.h:A</ta>
            <ta e="T551" id="Seg_11210" s="T550">np:G</ta>
            <ta e="T554" id="Seg_11211" s="T553">np:G</ta>
            <ta e="T557" id="Seg_11212" s="T556">np.h:A</ta>
            <ta e="T561" id="Seg_11213" s="T560">np.h:E</ta>
            <ta e="T563" id="Seg_11214" s="T562">np.h:Th</ta>
            <ta e="T566" id="Seg_11215" s="T565">adv:Path</ta>
            <ta e="T567" id="Seg_11216" s="T566">0.3.h:A</ta>
            <ta e="T569" id="Seg_11217" s="T568">np:Poss</ta>
            <ta e="T570" id="Seg_11218" s="T569">np:L</ta>
            <ta e="T571" id="Seg_11219" s="T570">0.3.h:A</ta>
            <ta e="T572" id="Seg_11220" s="T571">np.h:Th</ta>
            <ta e="T573" id="Seg_11221" s="T572">adv:Path</ta>
            <ta e="T574" id="Seg_11222" s="T573">pp:G</ta>
            <ta e="T577" id="Seg_11223" s="T576">0.3.h:A</ta>
            <ta e="T578" id="Seg_11224" s="T577">np:Th</ta>
            <ta e="T580" id="Seg_11225" s="T579">0.3.h:A 0.3.h:B</ta>
            <ta e="T582" id="Seg_11226" s="T581">np:Th</ta>
            <ta e="T584" id="Seg_11227" s="T583">0.3.h:A</ta>
            <ta e="T586" id="Seg_11228" s="T585">np.h:A</ta>
            <ta e="T587" id="Seg_11229" s="T586">adv:G</ta>
            <ta e="T589" id="Seg_11230" s="T588">np:Th</ta>
            <ta e="T590" id="Seg_11231" s="T589">np:G</ta>
            <ta e="T594" id="Seg_11232" s="T593">np:P</ta>
            <ta e="T595" id="Seg_11233" s="T594">0.3.h:A</ta>
            <ta e="T597" id="Seg_11234" s="T596">0.3.h:A</ta>
            <ta e="T598" id="Seg_11235" s="T597">np.h:E</ta>
            <ta e="T599" id="Seg_11236" s="T598">pp:Time</ta>
            <ta e="T607" id="Seg_11237" s="T606">np.h:A</ta>
            <ta e="T612" id="Seg_11238" s="T611">np:G</ta>
            <ta e="T614" id="Seg_11239" s="T613">np.h:Poss</ta>
            <ta e="T616" id="Seg_11240" s="T615">np.h:Th</ta>
            <ta e="T618" id="Seg_11241" s="T617">pro:G</ta>
            <ta e="T620" id="Seg_11242" s="T619">0.3.h:A</ta>
            <ta e="T623" id="Seg_11243" s="T622">0.3.h:A</ta>
            <ta e="T627" id="Seg_11244" s="T626">0.3.h:A</ta>
            <ta e="T628" id="Seg_11245" s="T627">np:Th</ta>
            <ta e="T634" id="Seg_11246" s="T633">0.3.h:A</ta>
            <ta e="T635" id="Seg_11247" s="T634">adv:Time</ta>
            <ta e="T636" id="Seg_11248" s="T635">adv:Time</ta>
            <ta e="T637" id="Seg_11249" s="T636">np.h:A</ta>
            <ta e="T641" id="Seg_11250" s="T640">np.h:A</ta>
            <ta e="T643" id="Seg_11251" s="T642">np.h:A</ta>
            <ta e="T645" id="Seg_11252" s="T644">adv:G</ta>
            <ta e="T648" id="Seg_11253" s="T647">adv:G</ta>
            <ta e="T650" id="Seg_11254" s="T649">np.h:A</ta>
            <ta e="T651" id="Seg_11255" s="T650">np:Th 0.3.h:Poss</ta>
            <ta e="T656" id="Seg_11256" s="T655">np.h:A</ta>
            <ta e="T662" id="Seg_11257" s="T661">pro.h:Poss</ta>
            <ta e="T667" id="Seg_11258" s="T666">0.2.h:Poss</ta>
            <ta e="T674" id="Seg_11259" s="T673">np.h:A</ta>
            <ta e="T676" id="Seg_11260" s="T675">np:Th</ta>
            <ta e="T681" id="Seg_11261" s="T680">0.3.h:A</ta>
            <ta e="T682" id="Seg_11262" s="T681">np:G</ta>
            <ta e="T684" id="Seg_11263" s="T683">0.3.h:A</ta>
            <ta e="T685" id="Seg_11264" s="T684">np.h:Th</ta>
            <ta e="T686" id="Seg_11265" s="T685">np:Poss</ta>
            <ta e="T687" id="Seg_11266" s="T686">np:L</ta>
            <ta e="T691" id="Seg_11267" s="T690">np.h:Th 0.3.h:Poss</ta>
            <ta e="T694" id="Seg_11268" s="T693">pro.h:A</ta>
            <ta e="T695" id="Seg_11269" s="T694">pro:G</ta>
            <ta e="T698" id="Seg_11270" s="T697">pro.h:Th</ta>
            <ta e="T699" id="Seg_11271" s="T698">0.1.h:E</ta>
            <ta e="T700" id="Seg_11272" s="T699">adv:L</ta>
            <ta e="T702" id="Seg_11273" s="T701">pp:L</ta>
            <ta e="T706" id="Seg_11274" s="T705">adv:Time</ta>
            <ta e="T707" id="Seg_11275" s="T706">np.h:Th</ta>
            <ta e="T711" id="Seg_11276" s="T710">np:Time</ta>
            <ta e="T712" id="Seg_11277" s="T711">np:Th</ta>
            <ta e="T713" id="Seg_11278" s="T712">adv:G</ta>
            <ta e="T715" id="Seg_11279" s="T714">pp:L</ta>
            <ta e="T721" id="Seg_11280" s="T720">np:Th</ta>
            <ta e="T723" id="Seg_11281" s="T722">0.3.h:A 0.3:Th</ta>
            <ta e="T725" id="Seg_11282" s="T724">np:Th</ta>
            <ta e="T727" id="Seg_11283" s="T726">0.3.h:A</ta>
            <ta e="T729" id="Seg_11284" s="T728">np:Th</ta>
            <ta e="T731" id="Seg_11285" s="T730">np:Th</ta>
            <ta e="T734" id="Seg_11286" s="T733">np:Th</ta>
            <ta e="T735" id="Seg_11287" s="T734">0.3.h:A</ta>
            <ta e="T741" id="Seg_11288" s="T740">0.3.h:A</ta>
            <ta e="T743" id="Seg_11289" s="T742">np.h:Poss</ta>
            <ta e="T744" id="Seg_11290" s="T743">np:G</ta>
            <ta e="T746" id="Seg_11291" s="T745">0.3.h:A</ta>
            <ta e="T750" id="Seg_11292" s="T749">np.h:A</ta>
            <ta e="T751" id="Seg_11293" s="T750">adv:Time</ta>
            <ta e="T754" id="Seg_11294" s="T753">np.h:A</ta>
            <ta e="T755" id="Seg_11295" s="T754">np:Th</ta>
            <ta e="T758" id="Seg_11296" s="T757">np:G</ta>
            <ta e="T761" id="Seg_11297" s="T760">np:G</ta>
            <ta e="T763" id="Seg_11298" s="T762">np:Poss</ta>
            <ta e="T764" id="Seg_11299" s="T763">np:L</ta>
            <ta e="T766" id="Seg_11300" s="T765">0.3.h:A</ta>
            <ta e="T767" id="Seg_11301" s="T766">np.h:A</ta>
            <ta e="T774" id="Seg_11302" s="T773">pro.h:E</ta>
            <ta e="T777" id="Seg_11303" s="T776">np.h:Th</ta>
            <ta e="T780" id="Seg_11304" s="T779">adv:Path</ta>
            <ta e="T781" id="Seg_11305" s="T780">0.3.h:A</ta>
            <ta e="T782" id="Seg_11306" s="T781">np.h:E</ta>
            <ta e="T784" id="Seg_11307" s="T783">pp:L</ta>
            <ta e="T786" id="Seg_11308" s="T785">np:Th</ta>
            <ta e="T789" id="Seg_11309" s="T788">0.3.h:A</ta>
            <ta e="T790" id="Seg_11310" s="T789">np.h:A</ta>
            <ta e="T795" id="Seg_11311" s="T794">0.3.h:A</ta>
            <ta e="T798" id="Seg_11312" s="T797">np.h:A</ta>
            <ta e="T803" id="Seg_11313" s="T802">np.h:A</ta>
            <ta e="T806" id="Seg_11314" s="T805">np.h:Com</ta>
            <ta e="T813" id="Seg_11315" s="T812">np.h:P</ta>
            <ta e="T814" id="Seg_11316" s="T813">np:Cau</ta>
            <ta e="T817" id="Seg_11317" s="T816">np:Cau</ta>
            <ta e="T819" id="Seg_11318" s="T818">np.h:P</ta>
            <ta e="T823" id="Seg_11319" s="T822">np.h:E</ta>
            <ta e="T830" id="Seg_11320" s="T829">0.3.h:Th</ta>
            <ta e="T833" id="Seg_11321" s="T832">np:Time</ta>
            <ta e="T834" id="Seg_11322" s="T833">np.h:P</ta>
            <ta e="T840" id="Seg_11323" s="T839">np.h:A</ta>
            <ta e="T841" id="Seg_11324" s="T840">pro.h:P</ta>
            <ta e="T846" id="Seg_11325" s="T845">0.3.h:E</ta>
            <ta e="T848" id="Seg_11326" s="T847">np:L</ta>
            <ta e="T850" id="Seg_11327" s="T849">np.h:Poss</ta>
            <ta e="T851" id="Seg_11328" s="T850">np.h:A</ta>
            <ta e="T853" id="Seg_11329" s="T852">pro.h:A</ta>
            <ta e="T857" id="Seg_11330" s="T856">pro:Th</ta>
            <ta e="T862" id="Seg_11331" s="T861">np.h:P</ta>
            <ta e="T864" id="Seg_11332" s="T863">0.3.h:A</ta>
            <ta e="T867" id="Seg_11333" s="T866">np.h:E</ta>
            <ta e="T872" id="Seg_11334" s="T871">pp:L</ta>
            <ta e="T875" id="Seg_11335" s="T874">np.h:P</ta>
            <ta e="T876" id="Seg_11336" s="T875">np:Ins</ta>
            <ta e="T879" id="Seg_11337" s="T878">0.3.h:A</ta>
            <ta e="T883" id="Seg_11338" s="T882">0.3.h:A</ta>
            <ta e="T887" id="Seg_11339" s="T886">np.h:P</ta>
            <ta e="T894" id="Seg_11340" s="T893">0.3.h:P</ta>
            <ta e="T898" id="Seg_11341" s="T897">0.3.h:A</ta>
            <ta e="T903" id="Seg_11342" s="T902">np.h:Poss</ta>
            <ta e="T906" id="Seg_11343" s="T905">np:Th</ta>
            <ta e="T912" id="Seg_11344" s="T911">np:Th</ta>
            <ta e="T915" id="Seg_11345" s="T914">np:Th</ta>
            <ta e="T917" id="Seg_11346" s="T916">np:Th</ta>
            <ta e="T919" id="Seg_11347" s="T918">np:Th</ta>
            <ta e="T920" id="Seg_11348" s="T919">np:Time</ta>
            <ta e="T922" id="Seg_11349" s="T921">adv:L</ta>
            <ta e="T923" id="Seg_11350" s="T922">adv:L</ta>
            <ta e="T929" id="Seg_11351" s="T928">0.3.h:A</ta>
            <ta e="T930" id="Seg_11352" s="T929">np.h:A</ta>
            <ta e="T932" id="Seg_11353" s="T931">pro.h:Th</ta>
            <ta e="T936" id="Seg_11354" s="T935">np.h:Th</ta>
            <ta e="T941" id="Seg_11355" s="T940">np:Th</ta>
            <ta e="T945" id="Seg_11356" s="T944">0.3.h:A</ta>
            <ta e="T947" id="Seg_11357" s="T946">np.h:A</ta>
            <ta e="T948" id="Seg_11358" s="T947">adv:So</ta>
            <ta e="T955" id="Seg_11359" s="T954">np.h:Th</ta>
            <ta e="T957" id="Seg_11360" s="T956">np:P</ta>
            <ta e="T961" id="Seg_11361" s="T960">0.3.h:A</ta>
            <ta e="T963" id="Seg_11362" s="T962">0.3.h:A</ta>
            <ta e="T965" id="Seg_11363" s="T964">np.h:A</ta>
            <ta e="T966" id="Seg_11364" s="T965">np.h:P</ta>
            <ta e="T968" id="Seg_11365" s="T967">np.h:R</ta>
            <ta e="T972" id="Seg_11366" s="T971">np.h:Th</ta>
            <ta e="T977" id="Seg_11367" s="T976">np:Time</ta>
            <ta e="T978" id="Seg_11368" s="T977">0.3.h:A</ta>
            <ta e="T979" id="Seg_11369" s="T978">np.h:Th</ta>
            <ta e="T983" id="Seg_11370" s="T982">np.h:A</ta>
            <ta e="T985" id="Seg_11371" s="T984">np:Com</ta>
            <ta e="T986" id="Seg_11372" s="T985">adv:G</ta>
            <ta e="T988" id="Seg_11373" s="T987">np.h:Poss</ta>
            <ta e="T989" id="Seg_11374" s="T988">pp:G</ta>
            <ta e="T994" id="Seg_11375" s="T993">0.3.h:Th</ta>
            <ta e="T999" id="Seg_11376" s="T998">0.3.h:Th</ta>
            <ta e="T1000" id="Seg_11377" s="T999">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T87" id="Seg_11378" s="T86">np.h:S</ta>
            <ta e="T88" id="Seg_11379" s="T87">v:pred</ta>
            <ta e="T89" id="Seg_11380" s="T88">np.h:S</ta>
            <ta e="T92" id="Seg_11381" s="T91">v:pred</ta>
            <ta e="T93" id="Seg_11382" s="T92">np.h:S</ta>
            <ta e="T94" id="Seg_11383" s="T93">pro:O</ta>
            <ta e="T96" id="Seg_11384" s="T95">v:pred</ta>
            <ta e="T99" id="Seg_11385" s="T96">s:compl</ta>
            <ta e="T102" id="Seg_11386" s="T101">np.h:S</ta>
            <ta e="T105" id="Seg_11387" s="T104">v:pred</ta>
            <ta e="T111" id="Seg_11388" s="T106">s:compl</ta>
            <ta e="T113" id="Seg_11389" s="T112">0.2.h:S v:pred</ta>
            <ta e="T115" id="Seg_11390" s="T114">np:S</ta>
            <ta e="T118" id="Seg_11391" s="T117">adv:pred</ta>
            <ta e="T119" id="Seg_11392" s="T118">cop</ta>
            <ta e="T121" id="Seg_11393" s="T120">0.3:S v:pred</ta>
            <ta e="T125" id="Seg_11394" s="T124">np:S</ta>
            <ta e="T126" id="Seg_11395" s="T125">cop</ta>
            <ta e="T127" id="Seg_11396" s="T126">np:S</ta>
            <ta e="T130" id="Seg_11397" s="T129">n:pred</ta>
            <ta e="T131" id="Seg_11398" s="T130">cop</ta>
            <ta e="T133" id="Seg_11399" s="T131">s:purp</ta>
            <ta e="T134" id="Seg_11400" s="T133">np:O</ta>
            <ta e="T137" id="Seg_11401" s="T136">0.3.h:S v:pred</ta>
            <ta e="T138" id="Seg_11402" s="T137">np.h:S</ta>
            <ta e="T140" id="Seg_11403" s="T139">v:pred</ta>
            <ta e="T141" id="Seg_11404" s="T140">pro.h:S</ta>
            <ta e="T144" id="Seg_11405" s="T143">v:pred</ta>
            <ta e="T145" id="Seg_11406" s="T144">np.h:S</ta>
            <ta e="T147" id="Seg_11407" s="T146">v:pred</ta>
            <ta e="T148" id="Seg_11408" s="T147">pro.h:S</ta>
            <ta e="T152" id="Seg_11409" s="T151">v:pred</ta>
            <ta e="T155" id="Seg_11410" s="T152">s:adv</ta>
            <ta e="T158" id="Seg_11411" s="T157">0.1.h:S v:pred</ta>
            <ta e="T159" id="Seg_11412" s="T158">np.h:S</ta>
            <ta e="T161" id="Seg_11413" s="T160">v:pred</ta>
            <ta e="T164" id="Seg_11414" s="T161">s:adv</ta>
            <ta e="T165" id="Seg_11415" s="T164">pro:O</ta>
            <ta e="T169" id="Seg_11416" s="T168">np:O</ta>
            <ta e="T171" id="Seg_11417" s="T170">0.3.h:S v:pred</ta>
            <ta e="T177" id="Seg_11418" s="T176">adj:pred</ta>
            <ta e="T178" id="Seg_11419" s="T177">0.3:S cop</ta>
            <ta e="T181" id="Seg_11420" s="T180">0.3.h:S v:pred</ta>
            <ta e="T188" id="Seg_11421" s="T187">np:S</ta>
            <ta e="T190" id="Seg_11422" s="T189">v:pred</ta>
            <ta e="T193" id="Seg_11423" s="T192">np:S</ta>
            <ta e="T195" id="Seg_11424" s="T194">cop</ta>
            <ta e="T196" id="Seg_11425" s="T195">adj:pred</ta>
            <ta e="T197" id="Seg_11426" s="T196">pro:O</ta>
            <ta e="T198" id="Seg_11427" s="T197">0.2.h:S v:pred</ta>
            <ta e="T200" id="Seg_11428" s="T199">np:S</ta>
            <ta e="T202" id="Seg_11429" s="T201">s:adv</ta>
            <ta e="T204" id="Seg_11430" s="T203">v:pred</ta>
            <ta e="T205" id="Seg_11431" s="T204">np.h:S</ta>
            <ta e="T206" id="Seg_11432" s="T205">np:O</ta>
            <ta e="T210" id="Seg_11433" s="T209">v:pred</ta>
            <ta e="T214" id="Seg_11434" s="T213">0.3.h:S v:pred</ta>
            <ta e="T217" id="Seg_11435" s="T214">s:temp</ta>
            <ta e="T220" id="Seg_11436" s="T217">s:rel</ta>
            <ta e="T221" id="Seg_11437" s="T220">np:O</ta>
            <ta e="T223" id="Seg_11438" s="T222">0.3.h:S v:pred</ta>
            <ta e="T225" id="Seg_11439" s="T224">0.3.h:S 0.3:O v:pred</ta>
            <ta e="T226" id="Seg_11440" s="T225">s:purp</ta>
            <ta e="T229" id="Seg_11441" s="T228">np.h:S</ta>
            <ta e="T235" id="Seg_11442" s="T234">np:O</ta>
            <ta e="T237" id="Seg_11443" s="T236">v:pred</ta>
            <ta e="T244" id="Seg_11444" s="T243">np.h:S</ta>
            <ta e="T245" id="Seg_11445" s="T244">np:O</ta>
            <ta e="T247" id="Seg_11446" s="T246">v:pred</ta>
            <ta e="T254" id="Seg_11447" s="T253">np:S</ta>
            <ta e="T256" id="Seg_11448" s="T255">s:adv</ta>
            <ta e="T261" id="Seg_11449" s="T260">v:pred</ta>
            <ta e="T262" id="Seg_11450" s="T261">np.h:S</ta>
            <ta e="T263" id="Seg_11451" s="T262">np:O</ta>
            <ta e="T266" id="Seg_11452" s="T265">v:pred</ta>
            <ta e="T267" id="Seg_11453" s="T266">np.h:S</ta>
            <ta e="T274" id="Seg_11454" s="T273">0.3.h:O v:pred</ta>
            <ta e="T280" id="Seg_11455" s="T279">0.3.h:S 0.3.h:O v:pred</ta>
            <ta e="T287" id="Seg_11456" s="T286">0.3.h:S 0.3.h:O v:pred</ta>
            <ta e="T289" id="Seg_11457" s="T288">np.h:S</ta>
            <ta e="T291" id="Seg_11458" s="T290">v:pred</ta>
            <ta e="T293" id="Seg_11459" s="T292">np:O</ta>
            <ta e="T295" id="Seg_11460" s="T294">0.2.h:S v:pred</ta>
            <ta e="T296" id="Seg_11461" s="T295">pro:S</ta>
            <ta e="T299" id="Seg_11462" s="T298">v:pred</ta>
            <ta e="T303" id="Seg_11463" s="T302">0.3:S v:pred</ta>
            <ta e="T304" id="Seg_11464" s="T303">np.h:S</ta>
            <ta e="T306" id="Seg_11465" s="T304">s:temp</ta>
            <ta e="T308" id="Seg_11466" s="T306">s:temp</ta>
            <ta e="T310" id="Seg_11467" s="T309">np:O</ta>
            <ta e="T313" id="Seg_11468" s="T312">v:pred</ta>
            <ta e="T316" id="Seg_11469" s="T313">s:temp</ta>
            <ta e="T319" id="Seg_11470" s="T318">0.3.h:S v:pred</ta>
            <ta e="T328" id="Seg_11471" s="T327">0.3.h:S v:pred</ta>
            <ta e="T331" id="Seg_11472" s="T328">s:temp</ta>
            <ta e="T333" id="Seg_11473" s="T332">0.3.h:S v:pred</ta>
            <ta e="T340" id="Seg_11474" s="T339">np:S</ta>
            <ta e="T341" id="Seg_11475" s="T340">v:pred</ta>
            <ta e="T345" id="Seg_11476" s="T344">np:S</ta>
            <ta e="T346" id="Seg_11477" s="T345">v:pred</ta>
            <ta e="T347" id="Seg_11478" s="T346">np.h:S</ta>
            <ta e="T349" id="Seg_11479" s="T348">v:pred</ta>
            <ta e="T352" id="Seg_11480" s="T351">0.3.h:S v:pred</ta>
            <ta e="T358" id="Seg_11481" s="T357">np:S</ta>
            <ta e="T359" id="Seg_11482" s="T358">v:pred</ta>
            <ta e="T365" id="Seg_11483" s="T364">0.3.h:S v:pred</ta>
            <ta e="T367" id="Seg_11484" s="T365">s:temp</ta>
            <ta e="T370" id="Seg_11485" s="T369">0.3.h:S v:pred</ta>
            <ta e="T373" id="Seg_11486" s="T372">np.h:S</ta>
            <ta e="T374" id="Seg_11487" s="T373">np.h:S</ta>
            <ta e="T375" id="Seg_11488" s="T374">v:pred</ta>
            <ta e="T377" id="Seg_11489" s="T376">np.h:S</ta>
            <ta e="T381" id="Seg_11490" s="T379">s:temp</ta>
            <ta e="T383" id="Seg_11491" s="T382">v:pred</ta>
            <ta e="T386" id="Seg_11492" s="T385">n:pred</ta>
            <ta e="T387" id="Seg_11493" s="T386">0.2.h:S cop</ta>
            <ta e="T388" id="Seg_11494" s="T387">np.h:S</ta>
            <ta e="T390" id="Seg_11495" s="T389">v:pred</ta>
            <ta e="T391" id="Seg_11496" s="T390">pro.h:S</ta>
            <ta e="T393" id="Seg_11497" s="T392">np.h:O</ta>
            <ta e="T395" id="Seg_11498" s="T394">v:pred</ta>
            <ta e="T397" id="Seg_11499" s="T396">pro.h:O</ta>
            <ta e="T399" id="Seg_11500" s="T398">0.1.h:S v:pred</ta>
            <ta e="T405" id="Seg_11501" s="T404">pro.h:S</ta>
            <ta e="T408" id="Seg_11502" s="T407">n:pred</ta>
            <ta e="T409" id="Seg_11503" s="T408">cop</ta>
            <ta e="T411" id="Seg_11504" s="T410">np.h:S</ta>
            <ta e="T413" id="Seg_11505" s="T412">v:pred</ta>
            <ta e="T414" id="Seg_11506" s="T413">pro.h:S</ta>
            <ta e="T418" id="Seg_11507" s="T417">n:pred</ta>
            <ta e="T421" id="Seg_11508" s="T420">n:pred</ta>
            <ta e="T423" id="Seg_11509" s="T422">pro.h:S</ta>
            <ta e="T424" id="Seg_11510" s="T423">cop</ta>
            <ta e="T427" id="Seg_11511" s="T424">s:rel</ta>
            <ta e="T428" id="Seg_11512" s="T427">np.h:S</ta>
            <ta e="T431" id="Seg_11513" s="T430">np:O</ta>
            <ta e="T432" id="Seg_11514" s="T431">v:pred</ta>
            <ta e="T440" id="Seg_11515" s="T439">np:O</ta>
            <ta e="T441" id="Seg_11516" s="T440">0.3.h:S v:pred</ta>
            <ta e="T443" id="Seg_11517" s="T442">np.h:S</ta>
            <ta e="T444" id="Seg_11518" s="T443">v:pred</ta>
            <ta e="T445" id="Seg_11519" s="T444">v:O</ta>
            <ta e="T446" id="Seg_11520" s="T445">0.3.h:S v:pred</ta>
            <ta e="T453" id="Seg_11521" s="T446">s:temp</ta>
            <ta e="T454" id="Seg_11522" s="T453">np:O</ta>
            <ta e="T459" id="Seg_11523" s="T458">0.3.h:S v:pred</ta>
            <ta e="T460" id="Seg_11524" s="T459">np.h:S</ta>
            <ta e="T462" id="Seg_11525" s="T460">s:temp</ta>
            <ta e="T463" id="Seg_11526" s="T462">s:temp</ta>
            <ta e="T464" id="Seg_11527" s="T463">np:O</ta>
            <ta e="T466" id="Seg_11528" s="T465">v:pred</ta>
            <ta e="T468" id="Seg_11529" s="T467">v:O</ta>
            <ta e="T470" id="Seg_11530" s="T469">0.3.h:S v:pred</ta>
            <ta e="T471" id="Seg_11531" s="T470">np.h:S</ta>
            <ta e="T472" id="Seg_11532" s="T471">np:O</ta>
            <ta e="T475" id="Seg_11533" s="T474">v:pred</ta>
            <ta e="T477" id="Seg_11534" s="T476">0.3.h:S v:pred</ta>
            <ta e="T479" id="Seg_11535" s="T478">np:O</ta>
            <ta e="T483" id="Seg_11536" s="T482">0.1.h:S v:pred</ta>
            <ta e="T485" id="Seg_11537" s="T484">np.h:S</ta>
            <ta e="T486" id="Seg_11538" s="T485">np:O</ta>
            <ta e="T488" id="Seg_11539" s="T487">v:pred</ta>
            <ta e="T490" id="Seg_11540" s="T489">np.h:S</ta>
            <ta e="T492" id="Seg_11541" s="T491">v:pred</ta>
            <ta e="T493" id="Seg_11542" s="T492">np.h:O</ta>
            <ta e="T496" id="Seg_11543" s="T495">0.3.h:S v:pred</ta>
            <ta e="T497" id="Seg_11544" s="T496">pro.h:S</ta>
            <ta e="T500" id="Seg_11545" s="T499">v:pred</ta>
            <ta e="T501" id="Seg_11546" s="T500">np.h:S</ta>
            <ta e="T504" id="Seg_11547" s="T503">v:pred</ta>
            <ta e="T505" id="Seg_11548" s="T504">pro.h:S</ta>
            <ta e="T506" id="Seg_11549" s="T505">v:O</ta>
            <ta e="T508" id="Seg_11550" s="T507">v:pred</ta>
            <ta e="T512" id="Seg_11551" s="T511">np.h:S</ta>
            <ta e="T515" id="Seg_11552" s="T514">v:pred</ta>
            <ta e="T516" id="Seg_11553" s="T515">pro.h:S</ta>
            <ta e="T518" id="Seg_11554" s="T517">v:pred</ta>
            <ta e="T519" id="Seg_11555" s="T518">np:O</ta>
            <ta e="T521" id="Seg_11556" s="T519">s:adv</ta>
            <ta e="T522" id="Seg_11557" s="T521">0.2.h:S v:pred</ta>
            <ta e="T523" id="Seg_11558" s="T522">np.h:S</ta>
            <ta e="T524" id="Seg_11559" s="T523">v:pred</ta>
            <ta e="T530" id="Seg_11560" s="T529">np.h:S</ta>
            <ta e="T532" id="Seg_11561" s="T531">v:pred</ta>
            <ta e="T533" id="Seg_11562" s="T532">np.h:S</ta>
            <ta e="T534" id="Seg_11563" s="T533">s:temp</ta>
            <ta e="T536" id="Seg_11564" s="T534">s:temp</ta>
            <ta e="T539" id="Seg_11565" s="T538">v:pred</ta>
            <ta e="T543" id="Seg_11566" s="T539">s:temp</ta>
            <ta e="T544" id="Seg_11567" s="T543">np.h:S</ta>
            <ta e="T546" id="Seg_11568" s="T545">np.h:S</ta>
            <ta e="T549" id="Seg_11569" s="T548">v:pred</ta>
            <ta e="T550" id="Seg_11570" s="T549">np.h:S</ta>
            <ta e="T552" id="Seg_11571" s="T550">s:adv</ta>
            <ta e="T556" id="Seg_11572" s="T555">v:pred</ta>
            <ta e="T557" id="Seg_11573" s="T556">np.h:S</ta>
            <ta e="T560" id="Seg_11574" s="T559">v:pred</ta>
            <ta e="T561" id="Seg_11575" s="T560">np.h:S</ta>
            <ta e="T563" id="Seg_11576" s="T562">np.h:O</ta>
            <ta e="T565" id="Seg_11577" s="T564">v:pred</ta>
            <ta e="T567" id="Seg_11578" s="T565">s:purp</ta>
            <ta e="T571" id="Seg_11579" s="T570">0.3.h:S v:pred</ta>
            <ta e="T572" id="Seg_11580" s="T571">np.h:O</ta>
            <ta e="T577" id="Seg_11581" s="T576">0.3.h:S v:pred</ta>
            <ta e="T580" id="Seg_11582" s="T579">0.3.h:S 0.3.h:O v:pred</ta>
            <ta e="T582" id="Seg_11583" s="T581">np:O</ta>
            <ta e="T584" id="Seg_11584" s="T583">0.3.h:S v:pred</ta>
            <ta e="T586" id="Seg_11585" s="T585">np.h:S</ta>
            <ta e="T588" id="Seg_11586" s="T586">s:temp</ta>
            <ta e="T589" id="Seg_11587" s="T588">np:O</ta>
            <ta e="T592" id="Seg_11588" s="T591">v:pred</ta>
            <ta e="T594" id="Seg_11589" s="T593">np:O</ta>
            <ta e="T595" id="Seg_11590" s="T594">0.3.h:S v:pred</ta>
            <ta e="T597" id="Seg_11591" s="T596">0.3.h:S v:pred</ta>
            <ta e="T598" id="Seg_11592" s="T597">np.h:S</ta>
            <ta e="T602" id="Seg_11593" s="T601">v:pred</ta>
            <ta e="T607" id="Seg_11594" s="T606">np.h:S</ta>
            <ta e="T608" id="Seg_11595" s="T607">s:purp</ta>
            <ta e="T610" id="Seg_11596" s="T609">v:pred</ta>
            <ta e="T616" id="Seg_11597" s="T615">np.h:S</ta>
            <ta e="T617" id="Seg_11598" s="T616">v:pred</ta>
            <ta e="T619" id="Seg_11599" s="T618">s:purp</ta>
            <ta e="T620" id="Seg_11600" s="T619">0.3.h:S v:pred</ta>
            <ta e="T622" id="Seg_11601" s="T620">s:temp</ta>
            <ta e="T623" id="Seg_11602" s="T622">0.3.h:S v:pred</ta>
            <ta e="T627" id="Seg_11603" s="T626">0.3.h:S v:pred</ta>
            <ta e="T628" id="Seg_11604" s="T627">np:O</ta>
            <ta e="T634" id="Seg_11605" s="T633">0.3.h:S v:pred</ta>
            <ta e="T637" id="Seg_11606" s="T636">np.h:S</ta>
            <ta e="T640" id="Seg_11607" s="T639">v:pred</ta>
            <ta e="T642" id="Seg_11608" s="T640">s:temp</ta>
            <ta e="T643" id="Seg_11609" s="T642">np.h:S</ta>
            <ta e="T647" id="Seg_11610" s="T646">v:pred</ta>
            <ta e="T649" id="Seg_11611" s="T647">s:temp</ta>
            <ta e="T652" id="Seg_11612" s="T649">s:temp</ta>
            <ta e="T656" id="Seg_11613" s="T655">np.h:S</ta>
            <ta e="T659" id="Seg_11614" s="T658">v:pred</ta>
            <ta e="T674" id="Seg_11615" s="T673">np.h:S</ta>
            <ta e="T676" id="Seg_11616" s="T675">np:O</ta>
            <ta e="T679" id="Seg_11617" s="T678">v:pred</ta>
            <ta e="T681" id="Seg_11618" s="T680">0.3.h:S v:pred</ta>
            <ta e="T684" id="Seg_11619" s="T683">0.3.h:S v:pred</ta>
            <ta e="T685" id="Seg_11620" s="T684">np.h:S</ta>
            <ta e="T689" id="Seg_11621" s="T687">s:adv</ta>
            <ta e="T690" id="Seg_11622" s="T689">v:pred</ta>
            <ta e="T692" id="Seg_11623" s="T690">s:adv</ta>
            <ta e="T694" id="Seg_11624" s="T693">pro.h:S</ta>
            <ta e="T697" id="Seg_11625" s="T696">v:pred</ta>
            <ta e="T698" id="Seg_11626" s="T697">pro.h:S</ta>
            <ta e="T699" id="Seg_11627" s="T698">s:temp</ta>
            <ta e="T704" id="Seg_11628" s="T703">v:pred</ta>
            <ta e="T707" id="Seg_11629" s="T706">np.h:S</ta>
            <ta e="T709" id="Seg_11630" s="T708">v:pred</ta>
            <ta e="T714" id="Seg_11631" s="T709">s:temp</ta>
            <ta e="T721" id="Seg_11632" s="T714">s:temp</ta>
            <ta e="T723" id="Seg_11633" s="T722">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T725" id="Seg_11634" s="T724">np:O</ta>
            <ta e="T727" id="Seg_11635" s="T726">0.3.h:S v:pred</ta>
            <ta e="T729" id="Seg_11636" s="T728">np:O</ta>
            <ta e="T731" id="Seg_11637" s="T730">np:O</ta>
            <ta e="T734" id="Seg_11638" s="T733">np:O</ta>
            <ta e="T735" id="Seg_11639" s="T734">0.3.h:S v:pred</ta>
            <ta e="T741" id="Seg_11640" s="T740">0.3.h:S v:pred</ta>
            <ta e="T746" id="Seg_11641" s="T745">0.3.h:S v:pred</ta>
            <ta e="T750" id="Seg_11642" s="T749">np.h:S</ta>
            <ta e="T753" id="Seg_11643" s="T752">v:pred</ta>
            <ta e="T754" id="Seg_11644" s="T753">np.h:S</ta>
            <ta e="T757" id="Seg_11645" s="T754">s:temp</ta>
            <ta e="T760" id="Seg_11646" s="T759">v:pred</ta>
            <ta e="T762" id="Seg_11647" s="T760">s:adv</ta>
            <ta e="T766" id="Seg_11648" s="T765">0.3.h:S v:pred</ta>
            <ta e="T767" id="Seg_11649" s="T766">np.h:S</ta>
            <ta e="T773" id="Seg_11650" s="T772">v:pred</ta>
            <ta e="T774" id="Seg_11651" s="T773">pro.h:S</ta>
            <ta e="T777" id="Seg_11652" s="T776">np.h:O</ta>
            <ta e="T779" id="Seg_11653" s="T778">v:pred</ta>
            <ta e="T782" id="Seg_11654" s="T781">np.h:S</ta>
            <ta e="T783" id="Seg_11655" s="T782">v:pred</ta>
            <ta e="T786" id="Seg_11656" s="T785">np:O</ta>
            <ta e="T789" id="Seg_11657" s="T788">0.3.h:S v:pred</ta>
            <ta e="T790" id="Seg_11658" s="T789">np.h:S</ta>
            <ta e="T793" id="Seg_11659" s="T792">v:pred</ta>
            <ta e="T795" id="Seg_11660" s="T794">0.3.h:S v:pred</ta>
            <ta e="T798" id="Seg_11661" s="T797">np.h:S</ta>
            <ta e="T801" id="Seg_11662" s="T798">s:adv</ta>
            <ta e="T802" id="Seg_11663" s="T801">v:pred</ta>
            <ta e="T803" id="Seg_11664" s="T802">np.h:S</ta>
            <ta e="T810" id="Seg_11665" s="T809">v:pred</ta>
            <ta e="T813" id="Seg_11666" s="T812">np.h:O</ta>
            <ta e="T814" id="Seg_11667" s="T813">np:S</ta>
            <ta e="T816" id="Seg_11668" s="T815">v:pred</ta>
            <ta e="T818" id="Seg_11669" s="T816">s:adv</ta>
            <ta e="T819" id="Seg_11670" s="T818">np.h:S</ta>
            <ta e="T820" id="Seg_11671" s="T819">v:pred</ta>
            <ta e="T822" id="Seg_11672" s="T820">s:rel</ta>
            <ta e="T823" id="Seg_11673" s="T822">np.h:S</ta>
            <ta e="T826" id="Seg_11674" s="T825">v:pred</ta>
            <ta e="T830" id="Seg_11675" s="T826">s:compl</ta>
            <ta e="T834" id="Seg_11676" s="T833">np.h:S</ta>
            <ta e="T837" id="Seg_11677" s="T836">v:pred</ta>
            <ta e="T840" id="Seg_11678" s="T839">np.h:S</ta>
            <ta e="T842" id="Seg_11679" s="T840">s:rel</ta>
            <ta e="T843" id="Seg_11680" s="T842">v:pred</ta>
            <ta e="T845" id="Seg_11681" s="T843">s:adv</ta>
            <ta e="T846" id="Seg_11682" s="T845">0.3.h:S v:pred</ta>
            <ta e="T851" id="Seg_11683" s="T850">np.h:S</ta>
            <ta e="T852" id="Seg_11684" s="T851">v:pred</ta>
            <ta e="T853" id="Seg_11685" s="T852">pro.h:S</ta>
            <ta e="T857" id="Seg_11686" s="T856">pro:O</ta>
            <ta e="T858" id="Seg_11687" s="T857">v:pred</ta>
            <ta e="T861" id="Seg_11688" s="T859">s:rel</ta>
            <ta e="T862" id="Seg_11689" s="T861">np.h:O</ta>
            <ta e="T864" id="Seg_11690" s="T863">0.3.h:S v:pred</ta>
            <ta e="T867" id="Seg_11691" s="T866">np.h:S</ta>
            <ta e="T869" id="Seg_11692" s="T868">v:pred</ta>
            <ta e="T873" id="Seg_11693" s="T872">s:adv</ta>
            <ta e="T875" id="Seg_11694" s="T874">np.h:O</ta>
            <ta e="T879" id="Seg_11695" s="T878">0.3.h:S v:pred</ta>
            <ta e="T883" id="Seg_11696" s="T882">0.3.h:S v:pred</ta>
            <ta e="T887" id="Seg_11697" s="T886">np.h:S</ta>
            <ta e="T890" id="Seg_11698" s="T889">v:pred</ta>
            <ta e="T894" id="Seg_11699" s="T893">0.3.h:S v:pred</ta>
            <ta e="T896" id="Seg_11700" s="T895">np:O</ta>
            <ta e="T898" id="Seg_11701" s="T897">0.3.h:S v:pred</ta>
            <ta e="T906" id="Seg_11702" s="T905">np:S</ta>
            <ta e="T908" id="Seg_11703" s="T907">ptcl:pred</ta>
            <ta e="T912" id="Seg_11704" s="T911">np:S</ta>
            <ta e="T914" id="Seg_11705" s="T913">adj:pred</ta>
            <ta e="T915" id="Seg_11706" s="T914">np:S</ta>
            <ta e="T916" id="Seg_11707" s="T915">v:pred</ta>
            <ta e="T917" id="Seg_11708" s="T916">np:S</ta>
            <ta e="T919" id="Seg_11709" s="T918">np:S</ta>
            <ta e="T924" id="Seg_11710" s="T923">v:pred</ta>
            <ta e="T926" id="Seg_11711" s="T924">s:adv</ta>
            <ta e="T929" id="Seg_11712" s="T928">0.3.h:S v:pred</ta>
            <ta e="T930" id="Seg_11713" s="T929">np.h:S</ta>
            <ta e="T931" id="Seg_11714" s="T930">s:adv</ta>
            <ta e="T932" id="Seg_11715" s="T931">pro.h:O</ta>
            <ta e="T933" id="Seg_11716" s="T932">v:pred</ta>
            <ta e="T936" id="Seg_11717" s="T935">np.h:S</ta>
            <ta e="T938" id="Seg_11718" s="T937">v:pred</ta>
            <ta e="T940" id="Seg_11719" s="T939">s:adv</ta>
            <ta e="T941" id="Seg_11720" s="T940">np:O</ta>
            <ta e="T945" id="Seg_11721" s="T944">0.3.h:S v:pred</ta>
            <ta e="T947" id="Seg_11722" s="T946">np.h:S</ta>
            <ta e="T950" id="Seg_11723" s="T949">v:pred</ta>
            <ta e="T952" id="Seg_11724" s="T951">v:pred</ta>
            <ta e="T955" id="Seg_11725" s="T954">np.h:S</ta>
            <ta e="T958" id="Seg_11726" s="T956">s:temp</ta>
            <ta e="T961" id="Seg_11727" s="T960">0.3.h:S v:pred</ta>
            <ta e="T963" id="Seg_11728" s="T962">0.3.h:S v:pred</ta>
            <ta e="T965" id="Seg_11729" s="T964">np.h:S</ta>
            <ta e="T966" id="Seg_11730" s="T965">np.h:O</ta>
            <ta e="T970" id="Seg_11731" s="T969">v:pred</ta>
            <ta e="T974" id="Seg_11732" s="T970">s:temp</ta>
            <ta e="T978" id="Seg_11733" s="T977">0.3.h:S v:pred</ta>
            <ta e="T981" id="Seg_11734" s="T978">s:temp</ta>
            <ta e="T983" id="Seg_11735" s="T982">np.h:S</ta>
            <ta e="T987" id="Seg_11736" s="T986">v:pred</ta>
            <ta e="T994" id="Seg_11737" s="T993">0.3.h:S v:pred</ta>
            <ta e="T999" id="Seg_11738" s="T998">0.3.h:S v:pred</ta>
            <ta e="T1000" id="Seg_11739" s="T999">np:S</ta>
            <ta e="T1004" id="Seg_11740" s="T1003">n:pred</ta>
            <ta e="T1005" id="Seg_11741" s="T1004">cop</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T87" id="Seg_11742" s="T86">new</ta>
            <ta e="T89" id="Seg_11743" s="T88">accs-sit</ta>
            <ta e="T93" id="Seg_11744" s="T92">giv-active</ta>
            <ta e="T94" id="Seg_11745" s="T93">new</ta>
            <ta e="T97" id="Seg_11746" s="T96">new</ta>
            <ta e="T99" id="Seg_11747" s="T98">0.giv-active</ta>
            <ta e="T102" id="Seg_11748" s="T101">giv-active</ta>
            <ta e="T103" id="Seg_11749" s="T102">giv-inactive</ta>
            <ta e="T105" id="Seg_11750" s="T104">quot-sp</ta>
            <ta e="T106" id="Seg_11751" s="T105">giv-active-Q</ta>
            <ta e="T110" id="Seg_11752" s="T109">new-Q</ta>
            <ta e="T112" id="Seg_11753" s="T111">giv-active-Q</ta>
            <ta e="T115" id="Seg_11754" s="T114">giv-active</ta>
            <ta e="T116" id="Seg_11755" s="T115">giv-active</ta>
            <ta e="T120" id="Seg_11756" s="T119">accs-gen</ta>
            <ta e="T123" id="Seg_11757" s="T122">giv-active</ta>
            <ta e="T125" id="Seg_11758" s="T124">accs-gen</ta>
            <ta e="T127" id="Seg_11759" s="T126">accs-inf</ta>
            <ta e="T130" id="Seg_11760" s="T129">new</ta>
            <ta e="T132" id="Seg_11761" s="T131">giv-active</ta>
            <ta e="T134" id="Seg_11762" s="T133">giv-active</ta>
            <ta e="T136" id="Seg_11763" s="T135">accs-gen</ta>
            <ta e="T137" id="Seg_11764" s="T136">0.giv-inactive</ta>
            <ta e="T138" id="Seg_11765" s="T137">giv-active</ta>
            <ta e="T140" id="Seg_11766" s="T139">quot-sp</ta>
            <ta e="T141" id="Seg_11767" s="T140">giv-active-Q</ta>
            <ta e="T142" id="Seg_11768" s="T141">giv-active-Q</ta>
            <ta e="T145" id="Seg_11769" s="T144">giv-active</ta>
            <ta e="T147" id="Seg_11770" s="T146">quot-sp</ta>
            <ta e="T148" id="Seg_11771" s="T147">giv-active-Q</ta>
            <ta e="T153" id="Seg_11772" s="T152">giv-active-Q</ta>
            <ta e="T157" id="Seg_11773" s="T156">accs-gen-Q</ta>
            <ta e="T158" id="Seg_11774" s="T157">0.giv-active-Q</ta>
            <ta e="T159" id="Seg_11775" s="T158">giv-inactive</ta>
            <ta e="T162" id="Seg_11776" s="T161">new</ta>
            <ta e="T165" id="Seg_11777" s="T164">new</ta>
            <ta e="T169" id="Seg_11778" s="T168">new</ta>
            <ta e="T171" id="Seg_11779" s="T170">giv-active</ta>
            <ta e="T179" id="Seg_11780" s="T178">giv-inactive</ta>
            <ta e="T181" id="Seg_11781" s="T180">0.giv-active quot-sp</ta>
            <ta e="T184" id="Seg_11782" s="T183">giv-inactive-Q</ta>
            <ta e="T188" id="Seg_11783" s="T187">accs-inf-Q</ta>
            <ta e="T193" id="Seg_11784" s="T192">giv-active-Q</ta>
            <ta e="T197" id="Seg_11785" s="T196">giv-active-Q</ta>
            <ta e="T198" id="Seg_11786" s="T197">giv-inactive-Q</ta>
            <ta e="T200" id="Seg_11787" s="T199">giv-inactive-Q</ta>
            <ta e="T205" id="Seg_11788" s="T204">giv-inactive</ta>
            <ta e="T206" id="Seg_11789" s="T205">giv-inactive</ta>
            <ta e="T214" id="Seg_11790" s="T213">0.giv-active</ta>
            <ta e="T215" id="Seg_11791" s="T214">giv-inactive</ta>
            <ta e="T221" id="Seg_11792" s="T220">giv-inactive</ta>
            <ta e="T223" id="Seg_11793" s="T222">0.giv-active 0.giv-active</ta>
            <ta e="T225" id="Seg_11794" s="T224">0.giv-active 0.giv-active</ta>
            <ta e="T229" id="Seg_11795" s="T228">giv-inactive</ta>
            <ta e="T235" id="Seg_11796" s="T234">accs-inf</ta>
            <ta e="T242" id="Seg_11797" s="T241">accs-gen</ta>
            <ta e="T244" id="Seg_11798" s="T243">giv-inactive</ta>
            <ta e="T245" id="Seg_11799" s="T244">giv-active</ta>
            <ta e="T254" id="Seg_11800" s="T253">accs-inf</ta>
            <ta e="T262" id="Seg_11801" s="T261">giv-inactive</ta>
            <ta e="T263" id="Seg_11802" s="T262">giv-inactive</ta>
            <ta e="T267" id="Seg_11803" s="T266">giv-inactive</ta>
            <ta e="T272" id="Seg_11804" s="T271">new</ta>
            <ta e="T274" id="Seg_11805" s="T273">0.giv-active</ta>
            <ta e="T278" id="Seg_11806" s="T277">new</ta>
            <ta e="T280" id="Seg_11807" s="T279">0.giv-active 0.giv-active</ta>
            <ta e="T285" id="Seg_11808" s="T284">giv-active</ta>
            <ta e="T287" id="Seg_11809" s="T286">0.giv-active 0.giv-active</ta>
            <ta e="T289" id="Seg_11810" s="T288">giv-active</ta>
            <ta e="T291" id="Seg_11811" s="T290">quot-sp</ta>
            <ta e="T293" id="Seg_11812" s="T292">accs-inf-Q</ta>
            <ta e="T295" id="Seg_11813" s="T294">0.giv-active-Q</ta>
            <ta e="T296" id="Seg_11814" s="T295">giv-inactive-Q</ta>
            <ta e="T301" id="Seg_11815" s="T300">accs-gen-Q</ta>
            <ta e="T304" id="Seg_11816" s="T303">giv-active</ta>
            <ta e="T306" id="Seg_11817" s="T305">giv-active</ta>
            <ta e="T310" id="Seg_11818" s="T309">giv-active</ta>
            <ta e="T319" id="Seg_11819" s="T318">0.giv-active</ta>
            <ta e="T325" id="Seg_11820" s="T324">accs-gen</ta>
            <ta e="T328" id="Seg_11821" s="T327">0.giv-active</ta>
            <ta e="T330" id="Seg_11822" s="T329">giv-active</ta>
            <ta e="T333" id="Seg_11823" s="T332">0.giv-active</ta>
            <ta e="T338" id="Seg_11824" s="T337">accs-gen</ta>
            <ta e="T340" id="Seg_11825" s="T339">accs-gen</ta>
            <ta e="T343" id="Seg_11826" s="T342">giv-active</ta>
            <ta e="T345" id="Seg_11827" s="T344">accs-gen</ta>
            <ta e="T347" id="Seg_11828" s="T346">giv-inactive</ta>
            <ta e="T350" id="Seg_11829" s="T349">giv-active</ta>
            <ta e="T353" id="Seg_11830" s="T352">accs-gen</ta>
            <ta e="T354" id="Seg_11831" s="T353">giv-active</ta>
            <ta e="T358" id="Seg_11832" s="T357">accs-inf</ta>
            <ta e="T363" id="Seg_11833" s="T362">accs-sit</ta>
            <ta e="T365" id="Seg_11834" s="T364">0.giv-active</ta>
            <ta e="T369" id="Seg_11835" s="T368">accs-inf</ta>
            <ta e="T370" id="Seg_11836" s="T369">0.giv-active</ta>
            <ta e="T371" id="Seg_11837" s="T370">giv-inactive</ta>
            <ta e="T373" id="Seg_11838" s="T372">accs-gen</ta>
            <ta e="T374" id="Seg_11839" s="T373">accs-gen</ta>
            <ta e="T377" id="Seg_11840" s="T376">new</ta>
            <ta e="T380" id="Seg_11841" s="T379">giv-inactive</ta>
            <ta e="T383" id="Seg_11842" s="T382">quot-sp</ta>
            <ta e="T387" id="Seg_11843" s="T386">0.giv-active-Q</ta>
            <ta e="T388" id="Seg_11844" s="T387">giv-active</ta>
            <ta e="T390" id="Seg_11845" s="T389">quot-sp</ta>
            <ta e="T391" id="Seg_11846" s="T390">giv-active-Q</ta>
            <ta e="T393" id="Seg_11847" s="T392">accs-gen-Q</ta>
            <ta e="T397" id="Seg_11848" s="T396">accs-gen-Q</ta>
            <ta e="T399" id="Seg_11849" s="T398">0.giv-active-Q</ta>
            <ta e="T405" id="Seg_11850" s="T404">giv-inactive-Q</ta>
            <ta e="T408" id="Seg_11851" s="T407">giv-active-Q</ta>
            <ta e="T411" id="Seg_11852" s="T410">giv-active</ta>
            <ta e="T413" id="Seg_11853" s="T412">quot-sp</ta>
            <ta e="T414" id="Seg_11854" s="T413">accs-inf-Q</ta>
            <ta e="T421" id="Seg_11855" s="T420">accs-inf-Q</ta>
            <ta e="T423" id="Seg_11856" s="T422">giv-active-Q</ta>
            <ta e="T428" id="Seg_11857" s="T427">accs-gen</ta>
            <ta e="T432" id="Seg_11858" s="T431">0.giv-active</ta>
            <ta e="T435" id="Seg_11859" s="T434">accs-inf</ta>
            <ta e="T440" id="Seg_11860" s="T439">accs-gen</ta>
            <ta e="T441" id="Seg_11861" s="T440">0.giv-active</ta>
            <ta e="T443" id="Seg_11862" s="T442">0.giv-inactive</ta>
            <ta e="T446" id="Seg_11863" s="T445">0.giv-active</ta>
            <ta e="T447" id="Seg_11864" s="T446">giv-active</ta>
            <ta e="T449" id="Seg_11865" s="T448">giv-inactive</ta>
            <ta e="T451" id="Seg_11866" s="T450">accs-inf</ta>
            <ta e="T454" id="Seg_11867" s="T453">giv-active</ta>
            <ta e="T459" id="Seg_11868" s="T458">0.giv-active</ta>
            <ta e="T460" id="Seg_11869" s="T459">giv-active</ta>
            <ta e="T461" id="Seg_11870" s="T460">giv-active</ta>
            <ta e="T464" id="Seg_11871" s="T463">giv-active</ta>
            <ta e="T470" id="Seg_11872" s="T469">0.giv-active</ta>
            <ta e="T471" id="Seg_11873" s="T470">giv-active</ta>
            <ta e="T472" id="Seg_11874" s="T471">giv-active</ta>
            <ta e="T477" id="Seg_11875" s="T476">0.giv-active quot-sp</ta>
            <ta e="T481" id="Seg_11876" s="T480">accs-gen-Q</ta>
            <ta e="T483" id="Seg_11877" s="T482">0.giv-active-Q</ta>
            <ta e="T485" id="Seg_11878" s="T484">giv-inactive</ta>
            <ta e="T486" id="Seg_11879" s="T485">giv-inactive</ta>
            <ta e="T490" id="Seg_11880" s="T489">giv-active</ta>
            <ta e="T491" id="Seg_11881" s="T490">accs-gen</ta>
            <ta e="T493" id="Seg_11882" s="T492">giv-inactive</ta>
            <ta e="T497" id="Seg_11883" s="T496">giv-active-Q</ta>
            <ta e="T499" id="Seg_11884" s="T498">giv-active-Q</ta>
            <ta e="T501" id="Seg_11885" s="T500">giv-active</ta>
            <ta e="T504" id="Seg_11886" s="T503">quot-sp</ta>
            <ta e="T505" id="Seg_11887" s="T504">giv-active-Q</ta>
            <ta e="T512" id="Seg_11888" s="T511">giv-inactive</ta>
            <ta e="T515" id="Seg_11889" s="T514">quot-sp</ta>
            <ta e="T516" id="Seg_11890" s="T515">giv-inactive-Q</ta>
            <ta e="T519" id="Seg_11891" s="T518">giv-inactive-Q</ta>
            <ta e="T520" id="Seg_11892" s="T519">giv-inactive-Q</ta>
            <ta e="T523" id="Seg_11893" s="T522">giv-inactive</ta>
            <ta e="T527" id="Seg_11894" s="T526">giv-active</ta>
            <ta e="T530" id="Seg_11895" s="T529">new</ta>
            <ta e="T533" id="Seg_11896" s="T532">giv-active</ta>
            <ta e="T535" id="Seg_11897" s="T534">giv-inactive</ta>
            <ta e="T537" id="Seg_11898" s="T536">giv-active</ta>
            <ta e="T544" id="Seg_11899" s="T543">giv-inactive</ta>
            <ta e="T546" id="Seg_11900" s="T545">giv-inactive</ta>
            <ta e="T547" id="Seg_11901" s="T546">giv-active</ta>
            <ta e="T550" id="Seg_11902" s="T549">giv-active</ta>
            <ta e="T551" id="Seg_11903" s="T550">giv-active</ta>
            <ta e="T554" id="Seg_11904" s="T553">accs-inf</ta>
            <ta e="T557" id="Seg_11905" s="T556">giv-inactive</ta>
            <ta e="T560" id="Seg_11906" s="T559">quot-sp</ta>
            <ta e="T561" id="Seg_11907" s="T560">giv-inactive-Q</ta>
            <ta e="T563" id="Seg_11908" s="T562">giv-active-Q</ta>
            <ta e="T567" id="Seg_11909" s="T566">0.giv-active-Q</ta>
            <ta e="T570" id="Seg_11910" s="T569">giv-inactive-Q</ta>
            <ta e="T571" id="Seg_11911" s="T570">0.giv-active-Q</ta>
            <ta e="T572" id="Seg_11912" s="T571">giv-active</ta>
            <ta e="T574" id="Seg_11913" s="T573">giv-active</ta>
            <ta e="T577" id="Seg_11914" s="T576">0.giv-active</ta>
            <ta e="T578" id="Seg_11915" s="T577">accs-gen</ta>
            <ta e="T580" id="Seg_11916" s="T579">0.giv-active</ta>
            <ta e="T582" id="Seg_11917" s="T581">accs-gen</ta>
            <ta e="T584" id="Seg_11918" s="T583">0.giv-active</ta>
            <ta e="T586" id="Seg_11919" s="T585">accs-sit</ta>
            <ta e="T589" id="Seg_11920" s="T588">accs-gen</ta>
            <ta e="T590" id="Seg_11921" s="T589">giv-inactive</ta>
            <ta e="T594" id="Seg_11922" s="T593">giv-active</ta>
            <ta e="T595" id="Seg_11923" s="T594">0.accs-aggr</ta>
            <ta e="T597" id="Seg_11924" s="T596">0.giv-active</ta>
            <ta e="T598" id="Seg_11925" s="T597">giv-inactive</ta>
            <ta e="T607" id="Seg_11926" s="T606">giv-inactive</ta>
            <ta e="T609" id="Seg_11927" s="T608">accs-sit</ta>
            <ta e="T612" id="Seg_11928" s="T611">giv-inactive</ta>
            <ta e="T613" id="Seg_11929" s="T612">giv-active</ta>
            <ta e="T614" id="Seg_11930" s="T613">accs-gen</ta>
            <ta e="T616" id="Seg_11931" s="T615">accs-gen</ta>
            <ta e="T623" id="Seg_11932" s="T622">0.accs-aggr</ta>
            <ta e="T627" id="Seg_11933" s="T626">0.giv-active</ta>
            <ta e="T628" id="Seg_11934" s="T627">accs-gen</ta>
            <ta e="T634" id="Seg_11935" s="T633">0.giv-active</ta>
            <ta e="T637" id="Seg_11936" s="T636">accs-sit</ta>
            <ta e="T640" id="Seg_11937" s="T639">0.giv-active</ta>
            <ta e="T641" id="Seg_11938" s="T640">0.giv-active</ta>
            <ta e="T643" id="Seg_11939" s="T642">giv-inactive</ta>
            <ta e="T645" id="Seg_11940" s="T644">accs-gen</ta>
            <ta e="T648" id="Seg_11941" s="T647">accs-gen</ta>
            <ta e="T649" id="Seg_11942" s="T648">0.giv-active</ta>
            <ta e="T650" id="Seg_11943" s="T649">giv-active</ta>
            <ta e="T651" id="Seg_11944" s="T650">giv-inactive</ta>
            <ta e="T652" id="Seg_11945" s="T651">giv-inactive</ta>
            <ta e="T656" id="Seg_11946" s="T655">accs-inf</ta>
            <ta e="T659" id="Seg_11947" s="T658">quot-sp</ta>
            <ta e="T660" id="Seg_11948" s="T659">giv-active</ta>
            <ta e="T661" id="Seg_11949" s="T660">giv-active-Q</ta>
            <ta e="T662" id="Seg_11950" s="T661">giv-active-Q</ta>
            <ta e="T665" id="Seg_11951" s="T664">new-Q</ta>
            <ta e="T668" id="Seg_11952" s="T667">accs-inf-Q</ta>
            <ta e="T673" id="Seg_11953" s="T672">new-Q</ta>
            <ta e="T674" id="Seg_11954" s="T673">giv-active</ta>
            <ta e="T679" id="Seg_11955" s="T678">quot-sp</ta>
            <ta e="T681" id="Seg_11956" s="T680">0.giv-active</ta>
            <ta e="T682" id="Seg_11957" s="T681">giv-inactive</ta>
            <ta e="T684" id="Seg_11958" s="T683">0.giv-active</ta>
            <ta e="T685" id="Seg_11959" s="T684">giv-inactive</ta>
            <ta e="T687" id="Seg_11960" s="T686">accs-inf</ta>
            <ta e="T691" id="Seg_11961" s="T690">giv-active</ta>
            <ta e="T693" id="Seg_11962" s="T692">giv-active-Q</ta>
            <ta e="T694" id="Seg_11963" s="T693">giv-active-Q</ta>
            <ta e="T698" id="Seg_11964" s="T697">giv-active-Q</ta>
            <ta e="T702" id="Seg_11965" s="T701">accs-gen-Q</ta>
            <ta e="T706" id="Seg_11966" s="T705">accs-gen</ta>
            <ta e="T707" id="Seg_11967" s="T706">giv-active</ta>
            <ta e="T711" id="Seg_11968" s="T710">accs-gen</ta>
            <ta e="T712" id="Seg_11969" s="T711">giv-inactive</ta>
            <ta e="T713" id="Seg_11970" s="T712">accs-gen</ta>
            <ta e="T715" id="Seg_11971" s="T714">giv-active</ta>
            <ta e="T721" id="Seg_11972" s="T720">new</ta>
            <ta e="T724" id="Seg_11973" s="T723">new</ta>
            <ta e="T725" id="Seg_11974" s="T724">giv-inactive</ta>
            <ta e="T727" id="Seg_11975" s="T726">0.giv-active</ta>
            <ta e="T729" id="Seg_11976" s="T728">new</ta>
            <ta e="T731" id="Seg_11977" s="T730">giv-inactive</ta>
            <ta e="T734" id="Seg_11978" s="T733">accs-sit</ta>
            <ta e="T735" id="Seg_11979" s="T734">0.giv-active</ta>
            <ta e="T741" id="Seg_11980" s="T740">0.giv-active</ta>
            <ta e="T743" id="Seg_11981" s="T742">giv-active</ta>
            <ta e="T744" id="Seg_11982" s="T743">giv-inactive</ta>
            <ta e="T750" id="Seg_11983" s="T749">giv-inactive</ta>
            <ta e="T754" id="Seg_11984" s="T753">giv-active</ta>
            <ta e="T755" id="Seg_11985" s="T754">giv-inactive</ta>
            <ta e="T758" id="Seg_11986" s="T757">giv-active</ta>
            <ta e="T761" id="Seg_11987" s="T760">giv-active</ta>
            <ta e="T764" id="Seg_11988" s="T763">giv-inactive</ta>
            <ta e="T766" id="Seg_11989" s="T765">0.giv-active</ta>
            <ta e="T767" id="Seg_11990" s="T766">giv-inactive</ta>
            <ta e="T769" id="Seg_11991" s="T768">accs-gen</ta>
            <ta e="T773" id="Seg_11992" s="T772">quot-S</ta>
            <ta e="T774" id="Seg_11993" s="T773">accs-gen-Q</ta>
            <ta e="T777" id="Seg_11994" s="T776">giv-active-Q</ta>
            <ta e="T781" id="Seg_11995" s="T780">0.giv-active</ta>
            <ta e="T782" id="Seg_11996" s="T781">giv-inactive</ta>
            <ta e="T786" id="Seg_11997" s="T785">accs-gen</ta>
            <ta e="T789" id="Seg_11998" s="T788">0.giv-active</ta>
            <ta e="T790" id="Seg_11999" s="T789">giv-active</ta>
            <ta e="T795" id="Seg_12000" s="T794">0.giv-active</ta>
            <ta e="T798" id="Seg_12001" s="T797">giv-inactive</ta>
            <ta e="T803" id="Seg_12002" s="T802">giv-inactive</ta>
            <ta e="T806" id="Seg_12003" s="T805">giv-active</ta>
            <ta e="T813" id="Seg_12004" s="T812">giv-active</ta>
            <ta e="T814" id="Seg_12005" s="T813">accs-gen</ta>
            <ta e="T817" id="Seg_12006" s="T816">giv-active</ta>
            <ta e="T819" id="Seg_12007" s="T818">giv-active</ta>
            <ta e="T821" id="Seg_12008" s="T820">giv-active</ta>
            <ta e="T823" id="Seg_12009" s="T822">giv-active</ta>
            <ta e="T834" id="Seg_12010" s="T833">giv-inactive</ta>
            <ta e="T840" id="Seg_12011" s="T839">new-Q</ta>
            <ta e="T841" id="Seg_12012" s="T840">giv-active-Q</ta>
            <ta e="T846" id="Seg_12013" s="T845">0.giv-active</ta>
            <ta e="T848" id="Seg_12014" s="T847">accs-gen</ta>
            <ta e="T851" id="Seg_12015" s="T850">giv-inactive</ta>
            <ta e="T852" id="Seg_12016" s="T851">quot-S</ta>
            <ta e="T853" id="Seg_12017" s="T852">accs-sit-Q</ta>
            <ta e="T862" id="Seg_12018" s="T861">giv-inactive-Q</ta>
            <ta e="T864" id="Seg_12019" s="T863">0.giv-inactive-Q</ta>
            <ta e="T866" id="Seg_12020" s="T865">giv-active-Q</ta>
            <ta e="T867" id="Seg_12021" s="T866">giv-active</ta>
            <ta e="T875" id="Seg_12022" s="T874">giv-inactive</ta>
            <ta e="T876" id="Seg_12023" s="T875">accs-inf</ta>
            <ta e="T879" id="Seg_12024" s="T878">0.giv-active</ta>
            <ta e="T883" id="Seg_12025" s="T882">0.giv-active</ta>
            <ta e="T887" id="Seg_12026" s="T886">giv-active</ta>
            <ta e="T891" id="Seg_12027" s="T890">accs-inf</ta>
            <ta e="T896" id="Seg_12028" s="T894">accs-sit</ta>
            <ta e="T898" id="Seg_12029" s="T897">0.giv-active</ta>
            <ta e="T903" id="Seg_12030" s="T902">giv-active</ta>
            <ta e="T906" id="Seg_12031" s="T905">accs-inf</ta>
            <ta e="T912" id="Seg_12032" s="T911">giv-inactive</ta>
            <ta e="T915" id="Seg_12033" s="T914">accs-inf</ta>
            <ta e="T917" id="Seg_12034" s="T916">accs-inf</ta>
            <ta e="T919" id="Seg_12035" s="T918">accs-inf</ta>
            <ta e="T920" id="Seg_12036" s="T919">accs-gen</ta>
            <ta e="T929" id="Seg_12037" s="T928">0.giv-active</ta>
            <ta e="T930" id="Seg_12038" s="T929">accs-gen</ta>
            <ta e="T932" id="Seg_12039" s="T931">giv-active</ta>
            <ta e="T936" id="Seg_12040" s="T935">giv-inactive</ta>
            <ta e="T941" id="Seg_12041" s="T940">giv-inactive</ta>
            <ta e="T945" id="Seg_12042" s="T944">0.giv-active</ta>
            <ta e="T947" id="Seg_12043" s="T946">giv-active</ta>
            <ta e="T955" id="Seg_12044" s="T954">giv-inactive</ta>
            <ta e="T957" id="Seg_12045" s="T956">new</ta>
            <ta e="T961" id="Seg_12046" s="T960">0.giv-active</ta>
            <ta e="T963" id="Seg_12047" s="T962">0.giv-active</ta>
            <ta e="T964" id="Seg_12048" s="T963">giv-inactive</ta>
            <ta e="T965" id="Seg_12049" s="T964">giv-inactive</ta>
            <ta e="T966" id="Seg_12050" s="T965">giv-inactive</ta>
            <ta e="T968" id="Seg_12051" s="T967">giv-inactive</ta>
            <ta e="T972" id="Seg_12052" s="T971">accs-aggr</ta>
            <ta e="T977" id="Seg_12053" s="T976">accs-gen</ta>
            <ta e="T978" id="Seg_12054" s="T977">0.giv-inactive</ta>
            <ta e="T979" id="Seg_12055" s="T978">giv-active</ta>
            <ta e="T983" id="Seg_12056" s="T982">giv-inactive</ta>
            <ta e="T985" id="Seg_12057" s="T984">giv-inactive</ta>
            <ta e="T987" id="Seg_12058" s="T986">accs-sit</ta>
            <ta e="T989" id="Seg_12059" s="T988">accs-sit</ta>
            <ta e="T994" id="Seg_12060" s="T993">0.giv-active</ta>
            <ta e="T999" id="Seg_12061" s="T998">0.giv-active</ta>
            <ta e="T1000" id="Seg_12062" s="T999">accs-gen</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T85" id="Seg_12063" s="T82">Три брата Лапта.</ta>
            <ta e="T88" id="Seg_12064" s="T85">Жили [вдвоём] мать с сыном.</ta>
            <ta e="T92" id="Seg_12065" s="T88">Сын затем человеком вырос.</ta>
            <ta e="T99" id="Seg_12066" s="T92">Сын не знал отца, когда [тот] умер.</ta>
            <ta e="T105" id="Seg_12067" s="T99">В один день сын матери сказал.</ta>
            <ta e="T113" id="Seg_12068" s="T105">"Мама, моего отца оленей [на которых он ездил] покажи мне."</ta>
            <ta e="T126" id="Seg_12069" s="T113">А оленей у матери с сыном столько было, что они землей питались, у оленей такая подстилка была.</ta>
            <ta e="T131" id="Seg_12070" s="T126">Кожа была как рыбья шкура.</ta>
            <ta e="T137" id="Seg_12071" s="T131">Чтобы их поскрести, оленей так домой собирались пригнать.</ta>
            <ta e="T140" id="Seg_12072" s="T137">Мать говорит.</ta>
            <ta e="T144" id="Seg_12073" s="T140">"Куда ты с оленями едешь?"</ta>
            <ta e="T147" id="Seg_12074" s="T144">Сын говорит.</ta>
            <ta e="T152" id="Seg_12075" s="T147">"Я никуда не поеду.</ta>
            <ta e="T155" id="Seg_12076" s="T152">Поскольку я скучаю.</ta>
            <ta e="T158" id="Seg_12077" s="T155">Внизу тундры увижу [что-то]".</ta>
            <ta e="T161" id="Seg_12078" s="T158">Мать не растерялась.</ta>
            <ta e="T178" id="Seg_12079" s="T161">Нарты внутри развязав, толстый крепкий аркан для оленей достала, [увидела, что] ручка деревянного ковша толстая была.</ta>
            <ta e="T181" id="Seg_12080" s="T178">Парню так подсказала.</ta>
            <ta e="T190" id="Seg_12081" s="T181">"Внизу среди [твоих] оленей три пестрых быка есть".</ta>
            <ta e="T198" id="Seg_12082" s="T190">Один пёстрый бык есть, немного побольше этого, его поймай.</ta>
            <ta e="T204" id="Seg_12083" s="T198">Те [двое] сами крича придут".</ta>
            <ta e="T210" id="Seg_12084" s="T204">Парень аркан до половины собрал.</ta>
            <ta e="T214" id="Seg_12085" s="T210">Так вниз пошёл.</ta>
            <ta e="T223" id="Seg_12086" s="T214">Поискав среди оленей того быка, о котором мать говорила, поймал его.</ta>
            <ta e="T226" id="Seg_12087" s="T223">Вверх привёл запрягать.</ta>
            <ta e="T243" id="Seg_12088" s="T226">Тем временем [видит, что] мать будто из нарт такие нарты достала, будто из тридцати мамонтовых рогов ножки [нарт].</ta>
            <ta e="T247" id="Seg_12089" s="T243">Парень стал нарты запрягать.</ta>
            <ta e="T261" id="Seg_12090" s="T247">[Видит, что] будто те два пестрых поменьше быка сами крича будто пришли.</ta>
            <ta e="T266" id="Seg_12091" s="T261">Парень нарту запрягать кончил.</ta>
            <ta e="T274" id="Seg_12092" s="T266">Мать семиколенный из мамонтовых рогов хорей дала.</ta>
            <ta e="T280" id="Seg_12093" s="T274">Пестрый с вырезкой сокуй [мать на него] надела.</ta>
            <ta e="T287" id="Seg_12094" s="T280">Новый с вырезкой ненецкий (?) [на него] надела.</ta>
            <ta e="T295" id="Seg_12095" s="T287">Потом мать [его] так попросила: "Оленя голову не трогай.</ta>
            <ta e="T303" id="Seg_12096" s="T295">Они [тебя] возить будут и домой привезут".</ta>
            <ta e="T313" id="Seg_12097" s="T303">Парень, вожжи развязав, наверх сел и оленей головами вперед пустил [ехать].</ta>
            <ta e="T321" id="Seg_12098" s="T313">Потом, когда вперёд поехал, то ли долго ехал, то ли коротко.</ta>
            <ta e="T328" id="Seg_12099" s="T321">На одном холме в тундре [он] сидел.</ta>
            <ta e="T341" id="Seg_12100" s="T328">Сидя на холме, так увидел, [что] будто внизу на берегу озера три чума стоят.</ta>
            <ta e="T346" id="Seg_12101" s="T341">Рядом с этими чумами ненецкие олени бегают.</ta>
            <ta e="T352" id="Seg_12102" s="T346">Парень [никуда] не делся, к чумам поехал [на нартах].</ta>
            <ta e="T359" id="Seg_12103" s="T352">[Видит, что] в середине будто довольно большой чум стоит.</ta>
            <ta e="T365" id="Seg_12104" s="T359">Напротив этих больших чумов остановился.</ta>
            <ta e="T370" id="Seg_12105" s="T365">Остановившись, внутри нарт сидит.</ta>
            <ta e="T375" id="Seg_12106" s="T370">Из дома на улицу люди вышли.</ta>
            <ta e="T383" id="Seg_12107" s="T375">Один старик, к парню подойдя, будто говорит.</ta>
            <ta e="T387" id="Seg_12108" s="T383">"[Из] какой земли человек ты будешь?"</ta>
            <ta e="T399" id="Seg_12109" s="T387">Парень говорит: "Я не знаю людей, никого не знаю.</ta>
            <ta e="T403" id="Seg_12110" s="T399">Из земли, [где] один жил. </ta>
            <ta e="T409" id="Seg_12111" s="T403">А ты, мол, что за старик будешь?"</ta>
            <ta e="T413" id="Seg_12112" s="T409">Этот старик так говорит.</ta>
            <ta e="T424" id="Seg_12113" s="T413">"Мы, мол, трое белые богачи братья, самый большой белый богач так [это] я и есть".</ta>
            <ta e="T432" id="Seg_12114" s="T424">[Видит, что] те на улицу вышедшие люди будто оленя ловят.</ta>
            <ta e="T441" id="Seg_12115" s="T432">Среди этих оленей одну ненецкую яловую важенку без рогов ловят.</ta>
            <ta e="T446" id="Seg_12116" s="T441">Парень [так] смотрит, поймать не могут.</ta>
            <ta e="T459" id="Seg_12117" s="T446">Пока парень, в нартах сидя, аркан собрал, убежавшую важенку рядом с парнем [мимо] прогнали.</ta>
            <ta e="T470" id="Seg_12118" s="T459">Парень, важенку подпустив и поймав, едва дёрнул аркан, как тот срезал важенки [шею].</ta>
            <ta e="T475" id="Seg_12119" s="T470">Парень аркан собрал.</ta>
            <ta e="T483" id="Seg_12120" s="T475">Говорит. "Зачем я эту верёвку кровью мажу?"</ta>
            <ta e="T488" id="Seg_12121" s="T483">Эти люди того оленя ободрали.</ta>
            <ta e="T492" id="Seg_12122" s="T488">Ненецкие люди на улице строганину начали есть.</ta>
            <ta e="T500" id="Seg_12123" s="T492">Парня привели: "Эй, ты с нами поешь строганину".</ta>
            <ta e="T508" id="Seg_12124" s="T500">Парень сказал: "Я строганину есть не умею".</ta>
            <ta e="T515" id="Seg_12125" s="T508">Тот белый богач старик сказал.</ta>
            <ta e="T518" id="Seg_12126" s="T515">"Вы почему сидите?</ta>
            <ta e="T522" id="Seg_12127" s="T518">Мясо в чум занесите и сварите".</ta>
            <ta e="T532" id="Seg_12128" s="T522">Парень смотрит, из чума будто на улицу одна девушка вышла.</ta>
            <ta e="T539" id="Seg_12129" s="T532">Девушка, выйдя, мясо нарезав, в чум [его] занесла.</ta>
            <ta e="T549" id="Seg_12130" s="T539">Посидев недолго, старик и парень в чум зашли.</ta>
            <ta e="T556" id="Seg_12131" s="T549">Парень, в чум зайдя, только хотел остановиться у порога.</ta>
            <ta e="T565" id="Seg_12132" s="T556">[Как] старик сказал: "Вы что, человека не видите?</ta>
            <ta e="T571" id="Seg_12133" s="T565">Пропустите вперед, почему в дверях заставляете стоять?"</ta>
            <ta e="T577" id="Seg_12134" s="T571">Парня вперед [пропустив], возле старика посадили.</ta>
            <ta e="T584" id="Seg_12135" s="T577">Мясо достали и разную еду поставили.</ta>
            <ta e="T592" id="Seg_12136" s="T584">Двое человек, на улицу выйдя, спирт в чум занесли.</ta>
            <ta e="T597" id="Seg_12137" s="T592">Стали спирт [пить?] и есть.</ta>
            <ta e="T602" id="Seg_12138" s="T597">Парень во время еды так слышит:</ta>
            <ta e="T612" id="Seg_12139" s="T602">Будто три брата Лапта свататься (приехали?), к братьям белым богачам.</ta>
            <ta e="T617" id="Seg_12140" s="T612">У братьев белых богачей одна сестра была.</ta>
            <ta e="T620" id="Seg_12141" s="T617">К ней свататься приехали.</ta>
            <ta e="T627" id="Seg_12142" s="T620">Хорошо поев, [люди] выходили и начали разъезжаться в разные стороны.</ta>
            <ta e="T634" id="Seg_12143" s="T627">Как будто договорились.</ta>
            <ta e="T640" id="Seg_12144" s="T634">Завтра молодожёнов вместе посадят.</ta>
            <ta e="T647" id="Seg_12145" s="T640">Поскольку люди уезжали, парень тоже домой собрался ехать.</ta>
            <ta e="T660" id="Seg_12146" s="T647">Когда на улицу вышли, пока парень сокуй надевал, самый старший брат Лапта сказал парню.</ta>
            <ta e="T673" id="Seg_12147" s="T660">"Сокуй у тебя будто морда [ловушка для рыбы], а верх сокуя будто черпак деревянного половника."</ta>
            <ta e="T679" id="Seg_12148" s="T673">Парень ничего не сказал.</ta>
            <ta e="T681" id="Seg_12149" s="T679">Потом поехал.</ta>
            <ta e="T684" id="Seg_12150" s="T681">Домой только приехал.</ta>
            <ta e="T692" id="Seg_12151" s="T684">Мать в дверях стоит и ждет парня. </ta>
            <ta e="T704" id="Seg_12152" s="T692">"Сын, ты куда так надолго уезжаешь, я, поскольку скучала, сидела [время от времени] внизу на этом холме". </ta>
            <ta e="T709" id="Seg_12153" s="T704">В эту ночь парень переночевал.</ta>
            <ta e="T723" id="Seg_12154" s="T709">На следующий день, пригнав домой оленей, разыскав среди них двух быков с шерстью как у кошки, поймал [их].</ta>
            <ta e="T727" id="Seg_12155" s="T723">Нарты без досок запряг.</ta>
            <ta e="T733" id="Seg_12156" s="T727">Надел (старую?) малицу и (такой же?) сокуй.</ta>
            <ta e="T741" id="Seg_12157" s="T733">Взяв хорей или нет, так поехал.</ta>
            <ta e="T753" id="Seg_12158" s="T741">В чум тех людей только приехал, [видит, что] будто братья Лапта раньше что ли приехали.</ta>
            <ta e="T760" id="Seg_12159" s="T753">Парень, вожжи привязав, в чум вошёл.</ta>
            <ta e="T773" id="Seg_12160" s="T760">В чум войдя, у порога только встал, [как] старик вчерашний будто говорит.</ta>
            <ta e="T779" id="Seg_12161" s="T773">"Вы что, бедного человека не видите?</ta>
            <ta e="T781" id="Seg_12162" s="T779">Вперед пропустите."</ta>
            <ta e="T789" id="Seg_12163" s="T781">Парень видит, со стариком рядом место с разных сторон освобождают.</ta>
            <ta e="T793" id="Seg_12164" s="T789">Парень туда садится.</ta>
            <ta e="T795" id="Seg_12165" s="T793">[Так] смотрит.</ta>
            <ta e="T802" id="Seg_12166" s="T795">[Видит, что] будто молодожены пришли и, вместе сидя, пьют.</ta>
            <ta e="T810" id="Seg_12167" s="T802">Парень тоже с этими людьми, он тоже стал пить.</ta>
            <ta e="T816" id="Seg_12168" s="T810">Видно, парня спирт опьянил.</ta>
            <ta e="T820" id="Seg_12169" s="T816">Спирт [его] опьянил и парень упал.</ta>
            <ta e="T830" id="Seg_12170" s="T820">Пьяный человек что будет знать, сколько он лежал.</ta>
            <ta e="T843" id="Seg_12171" s="T830">В одно время парень только очнулся: похоже, меня бьющий человек догоняет.</ta>
            <ta e="T852" id="Seg_12172" s="T843">Лёжа услышал, где-то тех белых богачей сестра кричит.</ta>
            <ta e="T858" id="Seg_12173" s="T852">"Эй, вы что, мол, смотрите!"</ta>
            <ta e="T866" id="Seg_12174" s="T858">Того недавно приехавшего парня бьют, братья Лапта.</ta>
            <ta e="T869" id="Seg_12175" s="T866">Парень не растерялся.</ta>
            <ta e="T879" id="Seg_12176" s="T869">Лежавших на нём навалившихся людей локтём в разные стороны разбросал.</ta>
            <ta e="T894" id="Seg_12177" s="T879">Потом второй раз в разные стороны разбросал, [видит, что] будто ненецкие братья Лапта [так] умерли, некоторых [он] пополам переломил.</ta>
            <ta e="T916" id="Seg_12178" s="T894">Некоторых [он] посередине переломил, а у самого младшего мудрого брата [Лапта] головы нет, а чум совсем без жердей, основание без жердей осталось.</ta>
            <ta e="T924" id="Seg_12179" s="T916">Мешковина и шкуры чума целый следующий день неподалёку в стороне лежат.</ta>
            <ta e="T933" id="Seg_12180" s="T924">Встав, так по сторонам смотрит, люди собираясь на него смотрят.</ta>
            <ta e="T938" id="Seg_12181" s="T933">А той девушки всё время нет.</ta>
            <ta e="T950" id="Seg_12182" s="T938">Пошел, шкуры чума вверх в стороны только раскрыл, [как видит, что] девушка оттуда будто выскочила.</ta>
            <ta e="T955" id="Seg_12183" s="T950">[Никуда] не делись здешние люди.</ta>
            <ta e="T970" id="Seg_12184" s="T955">Новый чум поставив, пили и ели, старик белый богач женил парня на своей сестре. </ta>
            <ta e="T978" id="Seg_12185" s="T970">Молодожёнов вместе посадив, четырнадцать дней пили.</ta>
            <ta e="T990" id="Seg_12186" s="T978">Молодожёнов вместе посадив, братья белые богачи с этим парнем на другую сторону перекочевали, рядом к чуму парня.</ta>
            <ta e="T999" id="Seg_12187" s="T990">Вместе кучей жили и теперь живут.</ta>
            <ta e="T1005" id="Seg_12188" s="T999">Сказка на этом и кончилась [на этом сказки достаточно было].</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T85" id="Seg_12189" s="T82">Three brothers Lapta.</ta>
            <ta e="T88" id="Seg_12190" s="T85">There lived a mother and her son [together].</ta>
            <ta e="T92" id="Seg_12191" s="T88">The son grew to a man.</ta>
            <ta e="T99" id="Seg_12192" s="T92">The son did not know his father when he died.</ta>
            <ta e="T105" id="Seg_12193" s="T99">One day the son told his mother:</ta>
            <ta e="T113" id="Seg_12194" s="T105">"Mother, show me where my father's reindeer are".</ta>
            <ta e="T126" id="Seg_12195" s="T113">The mother and the son had so many reindeer that [those] ate earth instead of moss.</ta>
            <ta e="T131" id="Seg_12196" s="T126">Their skin was like fish skin.</ta>
            <ta e="T137" id="Seg_12197" s="T131">To scrape them they were going to shepherd them home.</ta>
            <ta e="T140" id="Seg_12198" s="T137">The mother says:</ta>
            <ta e="T144" id="Seg_12199" s="T140">"Where are you going to with the reindeer?"</ta>
            <ta e="T147" id="Seg_12200" s="T144">The son says:</ta>
            <ta e="T152" id="Seg_12201" s="T147">"I won't go anywhere.</ta>
            <ta e="T155" id="Seg_12202" s="T152">As I am bored here,</ta>
            <ta e="T158" id="Seg_12203" s="T155">I'll take a look down the tundra.</ta>
            <ta e="T161" id="Seg_12204" s="T158">The mother kept her wits.</ta>
            <ta e="T178" id="Seg_12205" s="T161">She untied the sledge, put out the thick reindeer lasso so thick as a handle of a wooden scoop.</ta>
            <ta e="T181" id="Seg_12206" s="T178">She suggested the following to the young man:</ta>
            <ta e="T190" id="Seg_12207" s="T181">"There down among the reindeer there are three speckled bucks.</ta>
            <ta e="T198" id="Seg_12208" s="T190">Catch the buck that is a little bit bigger than this one.</ta>
            <ta e="T204" id="Seg_12209" s="T198">Those [two] will come themselves bawling.</ta>
            <ta e="T210" id="Seg_12210" s="T204">The young man collected the half of the rope. </ta>
            <ta e="T214" id="Seg_12211" s="T210">He went down (to the reindeer) so.</ta>
            <ta e="T223" id="Seg_12212" s="T214">Having looked for the buck his mother was talking about, he caught it.</ta>
            <ta e="T226" id="Seg_12213" s="T223">He brought it upwards to harness it.</ta>
            <ta e="T243" id="Seg_12214" s="T226">[He saw] that meanwhile his mother had put out a sledge that seemed [to be made] of thirty mammoth tusks and legs.</ta>
            <ta e="T247" id="Seg_12215" s="T243">The young man started to harness the sledge.</ta>
            <ta e="T261" id="Seg_12216" s="T247">He saw two other speckled bucks, the smaller ones, were coming themselves bawling.</ta>
            <ta e="T266" id="Seg_12217" s="T261">The young man finished to harness the sledge.</ta>
            <ta e="T274" id="Seg_12218" s="T266">The mother gave [him] a stick made of seven mammoth joints and horns.</ta>
            <ta e="T280" id="Seg_12219" s="T274">[His mother] put [him] a speckled patterned fur coat on.</ta>
            <ta e="T287" id="Seg_12220" s="T280">[She] put [him] a new patterned Nenets [coat?] on.</ta>
            <ta e="T295" id="Seg_12221" s="T287">Then [his] mother asked him the following: "Don't touch the head of the reindeer.</ta>
            <ta e="T303" id="Seg_12222" s="T295">They will carry [you] (on their backs) and [they] will bring you home".</ta>
            <ta e="T313" id="Seg_12223" s="T303">The young man untied the reins, sat down and let the reindeer go headfirst.</ta>
            <ta e="T321" id="Seg_12224" s="T313">Then he was going forward sooner or later.</ta>
            <ta e="T328" id="Seg_12225" s="T321">He sat down on one of the tundra hills.</ta>
            <ta e="T341" id="Seg_12226" s="T328">Sitting there he saw three tents standing down on the river shore.</ta>
            <ta e="T346" id="Seg_12227" s="T341">Nenets reindeer were running near those tents.</ta>
            <ta e="T352" id="Seg_12228" s="T346">The boy kept his wits and drove with his sledge to the tents.</ta>
            <ta e="T359" id="Seg_12229" s="T352">[He saw] a pretty big tent standing in the middle.</ta>
            <ta e="T365" id="Seg_12230" s="T359">He stopped opposite to those big tents.</ta>
            <ta e="T370" id="Seg_12231" s="T365">He stops and sits inside his sledge.</ta>
            <ta e="T375" id="Seg_12232" s="T370">People went out of their tents.</ta>
            <ta e="T383" id="Seg_12233" s="T375">An old man came up to the young man and said:</ta>
            <ta e="T387" id="Seg_12234" s="T383">"What land are you from?"</ta>
            <ta e="T399" id="Seg_12235" s="T387">The young man said: "I don't know humans, I don't know anyone.</ta>
            <ta e="T403" id="Seg_12236" s="T399">[I'm] from the land where I lived alone.</ta>
            <ta e="T409" id="Seg_12237" s="T403">And what kind of man are you?"</ta>
            <ta e="T413" id="Seg_12238" s="T409">That old man says the following:</ta>
            <ta e="T424" id="Seg_12239" s="T413">"We are the three white rich brothers, the biggest [=oldest] white rich man is me".</ta>
            <ta e="T432" id="Seg_12240" s="T424">There he saw as the other people in the street started to catch a reindeer.</ta>
            <ta e="T441" id="Seg_12241" s="T432">Among those reindeer they were trying to catch a hornless doe.</ta>
            <ta e="T446" id="Seg_12242" s="T441">The young man was watching [how] they couldn't catch it.</ta>
            <ta e="T459" id="Seg_12243" s="T446">As the boy was sitting inside the sledge collecting the rope, the doe ran near his sledge chased by people.</ta>
            <ta e="T470" id="Seg_12244" s="T459">The young man let the doe come closer and caught it, hardly had he pulled the rope, it cut off the doe's [neck].</ta>
            <ta e="T475" id="Seg_12245" s="T470">The young man collected the rope.</ta>
            <ta e="T483" id="Seg_12246" s="T475">He said: "Why did I spread blood on the rope?"</ta>
            <ta e="T488" id="Seg_12247" s="T483">Those people skinned that reindeer.</ta>
            <ta e="T492" id="Seg_12248" s="T488">The Nenets started to eat frozen meat outside.</ta>
            <ta e="T500" id="Seg_12249" s="T492">They brought the young man: "Hey, try frozen meat with us".</ta>
            <ta e="T508" id="Seg_12250" s="T500">The young man said: "I cannot eat frozen meat".</ta>
            <ta e="T515" id="Seg_12251" s="T508">That white rich man said:</ta>
            <ta e="T518" id="Seg_12252" s="T515">"Why are you sitting?</ta>
            <ta e="T522" id="Seg_12253" s="T518">Put the meat into the tent and cook it".</ta>
            <ta e="T532" id="Seg_12254" s="T522">The young man saw how a girl went out of a tent.</ta>
            <ta e="T539" id="Seg_12255" s="T532">The girl went out, cut the meat and brought it into the tent.</ta>
            <ta e="T549" id="Seg_12256" s="T539">That old man and the young man were sitting for a while and [then] they entered the tent.</ta>
            <ta e="T556" id="Seg_12257" s="T549">The young man entered the tent and wanted just to stay in the door.</ta>
            <ta e="T565" id="Seg_12258" s="T556">The old man said: "Don't you see the human?</ta>
            <ta e="T571" id="Seg_12259" s="T565">Let him forward, why are you keeping him standing in the door?"</ta>
            <ta e="T577" id="Seg_12260" s="T571">They let the young man forward and offered [him] a seat near the old man.</ta>
            <ta e="T584" id="Seg_12261" s="T577">They took out meat and put [out] diverse food.</ta>
            <ta e="T592" id="Seg_12262" s="T584">Two men went outside and brought the spirit into the tent.</ta>
            <ta e="T597" id="Seg_12263" s="T592">They started to drink it and to eat.</ta>
            <ta e="T602" id="Seg_12264" s="T597">During the meal the young man heard the following:</ta>
            <ta e="T612" id="Seg_12265" s="T602">Apparently, three Lapta brothers (came?) to the white rich brothers to woo her [the bride].</ta>
            <ta e="T617" id="Seg_12266" s="T612">The white rich brothers had a sister.</ta>
            <ta e="T620" id="Seg_12267" s="T617">They came to woo her.</ta>
            <ta e="T627" id="Seg_12268" s="T620">They ate good and went out to leave in different directions.</ta>
            <ta e="T634" id="Seg_12269" s="T627">Seemed like they came to an agreement.</ta>
            <ta e="T640" id="Seg_12270" s="T634">The next day they would seat the couple together.</ta>
            <ta e="T647" id="Seg_12271" s="T640">As the men were going away, the young man also decided to go.</ta>
            <ta e="T660" id="Seg_12272" s="T647">As they came outside, while the young man was putting on his fur coat, the eldest Lapta brother told him:</ta>
            <ta e="T673" id="Seg_12273" s="T660">"Your fur coat looks like a fish trap and its upper part is like a wooden ladle."</ta>
            <ta e="T679" id="Seg_12274" s="T673">The young man didn't say anything [back].</ta>
            <ta e="T681" id="Seg_12275" s="T679">Then he went on.</ta>
            <ta e="T684" id="Seg_12276" s="T681">He came home.</ta>
            <ta e="T692" id="Seg_12277" s="T684">The mother is standing in the door, waiting for the young man.</ta>
            <ta e="T704" id="Seg_12278" s="T692">"Son, where did you go for so long? I was missing you, sitting on this hill."</ta>
            <ta e="T709" id="Seg_12279" s="T704">The young man spent the night [there].</ta>
            <ta e="T723" id="Seg_12280" s="T709">The next day he shephered the reindeer home, among them he found two reindeer with cat-like hair, and caught them.</ta>
            <ta e="T727" id="Seg_12281" s="T723">He harnessed the sledge without boards.</ta>
            <ta e="T733" id="Seg_12282" s="T727">He put on an (old?) coat [with fur inside] and one [with fur outside].</ta>
            <ta e="T741" id="Seg_12283" s="T733">Whether he took his stick or not, he went on.</ta>
            <ta e="T753" id="Seg_12284" s="T741">He came to that tent and saw that Lapta brothers had come earlier.</ta>
            <ta e="T760" id="Seg_12285" s="T753">The young man bound the reins and entered the tent.</ta>
            <ta e="T773" id="Seg_12286" s="T760">He entered the tent and stayed in the door, as the old man from yesterday said:</ta>
            <ta e="T779" id="Seg_12287" s="T773">"Don't you see the poor man?</ta>
            <ta e="T781" id="Seg_12288" s="T779">Let him [pass] forward."</ta>
            <ta e="T789" id="Seg_12289" s="T781">The young man saw them making room on the seat near the old man.</ta>
            <ta e="T793" id="Seg_12290" s="T789">The young man sat down there.</ta>
            <ta e="T795" id="Seg_12291" s="T793">He was watching.</ta>
            <ta e="T802" id="Seg_12292" s="T795">He saw the engaged couple came, sat down and drank together.</ta>
            <ta e="T810" id="Seg_12293" s="T802">The young man also started to drink with those people.</ta>
            <ta e="T816" id="Seg_12294" s="T810">The spirit apparently made the young man drunk.</ta>
            <ta e="T820" id="Seg_12295" s="T816">The spirit made [him] drunk and he fell down.</ta>
            <ta e="T830" id="Seg_12296" s="T820">But would a drunk man know for how long he was lying [there]?</ta>
            <ta e="T843" id="Seg_12297" s="T830">At one moment the young man woke up: a man is beating me.</ta>
            <ta e="T852" id="Seg_12298" s="T843">Lying like this he heard the sister of those white rich men screaming somewhere:</ta>
            <ta e="T858" id="Seg_12299" s="T852">"Hey, what are you looking at!"</ta>
            <ta e="T866" id="Seg_12300" s="T858">That young man, who came recently, is being beaten by Lapta brothers.</ta>
            <ta e="T869" id="Seg_12301" s="T866">The young man kept his wits.</ta>
            <ta e="T879" id="Seg_12302" s="T869">With his elbows he pulled the men lying on him in every which way.</ta>
            <ta e="T894" id="Seg_12303" s="T879">He threw them in every which way for the second time and saw, the Nenets Lapta brothers died, some of them torn into parts.</ta>
            <ta e="T916" id="Seg_12304" s="T894">He tore some of them in parts in the middle, the youngest wise [Lapta] brother lost his head; the tent lost it's poles, it's foundation stood without poles.</ta>
            <ta e="T924" id="Seg_12305" s="T916">The tent covering was lying aside the whole next day.</ta>
            <ta e="T933" id="Seg_12306" s="T924">He stood up and saw people gathering together, looking at him.</ta>
            <ta e="T938" id="Seg_12307" s="T933">That girl wasn't there.</ta>
            <ta e="T950" id="Seg_12308" s="T938">He went [further], opened the tent covering and saw as the girl jumped out of there.</ta>
            <ta e="T955" id="Seg_12309" s="T950">The local people stayed there. </ta>
            <ta e="T970" id="Seg_12310" s="T955">They built a new tent, drank and ate, and the white rich man married his sister off to the young man.</ta>
            <ta e="T978" id="Seg_12311" s="T970">Having placed the married couple together they were drinking for fourteen days.</ta>
            <ta e="T990" id="Seg_12312" s="T978">Having placed the married couple together, the white rich brothers moved near to the young man's tent.</ta>
            <ta e="T999" id="Seg_12313" s="T990">They lived all together there and still live.</ta>
            <ta e="T1005" id="Seg_12314" s="T999">That is the end of the tale.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T85" id="Seg_12315" s="T82">Drei Brüder Lapta.</ta>
            <ta e="T88" id="Seg_12316" s="T85">Es lebten eine Mutter und ihr Sohn [zu zweit].</ta>
            <ta e="T92" id="Seg_12317" s="T88">Der Sohn wuchs zum Mann heran.</ta>
            <ta e="T99" id="Seg_12318" s="T92">Der Sohn kannte seinen Vater nicht, als [jener] starb.</ta>
            <ta e="T105" id="Seg_12319" s="T99">Eines Tages sagte der Sohn zu seiner Mutter:</ta>
            <ta e="T113" id="Seg_12320" s="T105">"Mutter, zeig mir, wo die Rentiere meines Vaters sind."</ta>
            <ta e="T126" id="Seg_12321" s="T113">Die Mutter und der Sohn hatten so viele Rentiere, dass diese Erde fressen mussten statt Moos, die Rentiere hatten so eine Unterlage.</ta>
            <ta e="T131" id="Seg_12322" s="T126">Ihre Haut war wie Fischhaut.</ta>
            <ta e="T137" id="Seg_12323" s="T131">Um die abzukratzen, wollten sie die Rentiere nach Hause treiben.</ta>
            <ta e="T140" id="Seg_12324" s="T137">Die Mutter sagt:</ta>
            <ta e="T144" id="Seg_12325" s="T140">"Wo gehst du mit den Rentieren hin?"</ta>
            <ta e="T147" id="Seg_12326" s="T144">Der Sohn sagt:</ta>
            <ta e="T152" id="Seg_12327" s="T147">"Ich gehe nirgendwo hin.</ta>
            <ta e="T155" id="Seg_12328" s="T152">Da mir langweilig ist,</ta>
            <ta e="T158" id="Seg_12329" s="T155">schaue ich mal unten die Tundra an (?)."</ta>
            <ta e="T161" id="Seg_12330" s="T158">Die Mutter hatte ihre fünf Sinne beisammen.</ta>
            <ta e="T178" id="Seg_12331" s="T161">Sie band den Schlitten los, holte eine dicke Fangschlinge für Rentiere raus, sie war offenbar so dick wie der Griff einer hölzernen Kelle.</ta>
            <ta e="T181" id="Seg_12332" s="T178">Sie schlug dem Jungen Folgendes vor:</ta>
            <ta e="T190" id="Seg_12333" s="T181">"Unter den Hirschen da unten sind drei gescheckte Bullen.</ta>
            <ta e="T198" id="Seg_12334" s="T190">Einen gescheckten Bullen gibt es, der etwas größer ist, fang den.</ta>
            <ta e="T204" id="Seg_12335" s="T198">Die anderen beiden werden brüllend und von selbst kommen.</ta>
            <ta e="T210" id="Seg_12336" s="T204">Der junge Mann wickelte die Fangschlinge bis zur Hälfte auf.</ta>
            <ta e="T214" id="Seg_12337" s="T210">So ging er hinunter.</ta>
            <ta e="T223" id="Seg_12338" s="T214">Er suchte unter den Rentieren den Bullen, über den seine Mutter gesprochen hatte, und fing ihn.</ta>
            <ta e="T226" id="Seg_12339" s="T223">Er brachte ihn nach oben, um ihn anzuspannen.</ta>
            <ta e="T243" id="Seg_12340" s="T226">Währenddessen hatte seine Mutter offenbar aus dem Schlitten so einen Schlitten gemacht, dessen Beine offenbar aus dreißig Mammutstoßzähnen waren.</ta>
            <ta e="T247" id="Seg_12341" s="T243">Der Junge fing an, den Schlitten anzuspannen.</ta>
            <ta e="T261" id="Seg_12342" s="T247">Offenbar kamen die beiden kleineren Bullen von selbst und brüllend.</ta>
            <ta e="T266" id="Seg_12343" s="T261">Der Junge Mann war fertig damit, den Schlitten anzuspannen.</ta>
            <ta e="T274" id="Seg_12344" s="T266">Die Mutter gab ihm einen siebengelenkigen Stock aus Mammutstoßzähnen.</ta>
            <ta e="T280" id="Seg_12345" s="T274">Seine Mutter zog ihm einen gesprenkelten und gemusterten Pelzmantel an. </ta>
            <ta e="T287" id="Seg_12346" s="T280">Sie zog ihm einen neuen nenzischen Mantel(?) an.</ta>
            <ta e="T295" id="Seg_12347" s="T287">Dann bat ihn seine Mutter: "Fass den Kopf des Rentiers nicht an.</ta>
            <ta e="T303" id="Seg_12348" s="T295">Sie selbst werden dich tragen und nach Hause bringen.</ta>
            <ta e="T313" id="Seg_12349" s="T303">Der Junge löste die Zügel, setzte sich und ließ die Rentiere mit den Köpfen voraus laufen.</ta>
            <ta e="T321" id="Seg_12350" s="T313">Dann, als er weiter fuhr, fuhr er kurz oder lang.</ta>
            <ta e="T328" id="Seg_12351" s="T321">Er setzte sich auf einen Hügel in der Tundra.</ta>
            <ta e="T341" id="Seg_12352" s="T328">Auf dem Hügel sitzend sah er, dass unten am Seeufer offenbar drei Zelte stehen.</ta>
            <ta e="T346" id="Seg_12353" s="T341">Neben diesen Zelten liefen nenzische Rentiere herum.</ta>
            <ta e="T352" id="Seg_12354" s="T346">Der junge Mann hatte seine fünf Sinne beisammen und fuhr mit dem Schlitten zu den Zelten.</ta>
            <ta e="T359" id="Seg_12355" s="T352">In der Mitte steht offenbar ein ziemlich großes Zelt.</ta>
            <ta e="T365" id="Seg_12356" s="T359">Er hielt gegenüber von diesen großen Zelten an.</ta>
            <ta e="T370" id="Seg_12357" s="T365">Er hält an und sitzt im Schlitten.</ta>
            <ta e="T375" id="Seg_12358" s="T370">Aus den Zelten kamen Leute heraus.</ta>
            <ta e="T383" id="Seg_12359" s="T375">Ein alter Mann näherte sich dem Jungen und sagte:</ta>
            <ta e="T387" id="Seg_12360" s="T383">"Aus welchem Land bist du?"</ta>
            <ta e="T399" id="Seg_12361" s="T387">Der Junge sagte: "Ich kenne keine Menschen, ich kenne niemanden.</ta>
            <ta e="T403" id="Seg_12362" s="T399">Aus dem Land, in dem ich allein lebte.</ta>
            <ta e="T409" id="Seg_12363" s="T403">Und was bist du für ein alter Mann?"</ta>
            <ta e="T413" id="Seg_12364" s="T409">Der Alte sagt Folgendes:</ta>
            <ta e="T424" id="Seg_12365" s="T413">"Wir sind die drei weißen reichen Brüder, der größte weiße reiche Mann bin ich."</ta>
            <ta e="T432" id="Seg_12366" s="T424">Die Leute, die hinausgegangen waren, fingen offenbar Rentiere.</ta>
            <ta e="T441" id="Seg_12367" s="T432">Von diesen Rentieren fangen sie eine geweihlose, nenzische, unfruchtbare Rentierkuh.</ta>
            <ta e="T446" id="Seg_12368" s="T441">Der Junge sah, sie konnten sie nicht fangen.</ta>
            <ta e="T459" id="Seg_12369" s="T446">Als der Junge im Schlitten saß und die Fangschlinge aufrollte, jagten sie die Rentierkuh neben dem Jungen entlang.</ta>
            <ta e="T470" id="Seg_12370" s="T459">Der Junge, nachdem er die Rentierkuh kommen lassen und gefangen hatte, zog heftig an der Fangschlinge und schnitt der Rentierkuh [den Kopf] ab.</ta>
            <ta e="T475" id="Seg_12371" s="T470">Der Junge wickelte die Fangschlinge auf.</ta>
            <ta e="T483" id="Seg_12372" s="T475">Er sagte: "Warum habe ich die Fangschlinge mit Blut beschmiert?"</ta>
            <ta e="T488" id="Seg_12373" s="T483">Die Leute häuteten das Rentier.</ta>
            <ta e="T492" id="Seg_12374" s="T488">Die Nenzen fingen draußen an rohes, gefrorenes Fleisch zu essen.</ta>
            <ta e="T500" id="Seg_12375" s="T492">Die brachten den jungen Mann: "Hey, iss rohes, gefrorenes Fleisch mit uns."</ta>
            <ta e="T508" id="Seg_12376" s="T500">Der Junge sagte: "Ich kann kein rohes, gefrorenes Fleisch essen."</ta>
            <ta e="T515" id="Seg_12377" s="T508">Jener weiße reiche Mann sagte:</ta>
            <ta e="T518" id="Seg_12378" s="T515">"Warum sitzt ihr?</ta>
            <ta e="T522" id="Seg_12379" s="T518">Bringt das Fleisch ins Zelt und kocht es."</ta>
            <ta e="T532" id="Seg_12380" s="T522">Der Junge sah wie aus einem Zelt offenbar ein Mädchen hinausging.</ta>
            <ta e="T539" id="Seg_12381" s="T532">Das Mädchen ging hinaus, schnitt das Fleisch und brachte es ins Zelt.</ta>
            <ta e="T549" id="Seg_12382" s="T539">Nachdem sie etwas gesessen hatten, kamen der alte Mann und der Junge ins Zelt hinein.</ta>
            <ta e="T556" id="Seg_12383" s="T549">Der Junge kam ins Zelt hinein und wollte nur im Eingang stehen bleiben.</ta>
            <ta e="T565" id="Seg_12384" s="T556">Der Alte sagte: "Seht ihr den Menschen etwa nicht?</ta>
            <ta e="T571" id="Seg_12385" s="T565">Lasst ihn nach vorne, warum lasst ihr ihn im Eingang stehen?"</ta>
            <ta e="T577" id="Seg_12386" s="T571">Sie [ließen] den Jungen nach vorne und setzten ihn neben den Alten.</ta>
            <ta e="T584" id="Seg_12387" s="T577">Sie holten Fleisch und stellten verschiedenes Essen hin.</ta>
            <ta e="T592" id="Seg_12388" s="T584">Zwei Männer gingen nach draußen und brachten Schnaps ins Zelt.</ta>
            <ta e="T597" id="Seg_12389" s="T592">Sie fingen an, den Schnaps zu [???] und zu essen.</ta>
            <ta e="T602" id="Seg_12390" s="T597">Während des Essens hört der Junge Folgendes:</ta>
            <ta e="T612" id="Seg_12391" s="T602">Drei Lapta Brüder (kamen?) offenbar zu den weißen reichen Brüdern, um um [die Braut] zu werben.</ta>
            <ta e="T617" id="Seg_12392" s="T612">Die weißen reichen Bruder hatten eine Schwester.</ta>
            <ta e="T620" id="Seg_12393" s="T617">Sie kamen, um um sie zu werben.</ta>
            <ta e="T627" id="Seg_12394" s="T620">Sie aßen gut und gingen hinaus, sie fuhren verschiedene Richtungen los.</ta>
            <ta e="T634" id="Seg_12395" s="T627">Anscheinend haben sie sich abgesprochen.</ta>
            <ta e="T640" id="Seg_12396" s="T634">Am nächsten Tag würden sie das junge Paar zusammen hinsetzen.</ta>
            <ta e="T647" id="Seg_12397" s="T640">Da die Männer wegfuhren, beschloss der Junge auch nach Hause zu fahren.</ta>
            <ta e="T660" id="Seg_12398" s="T647">Als sie hinausgingen und der Junge seinen Pelzmantel anzog, sagte ihm der ältester Lapta Bruder:</ta>
            <ta e="T673" id="Seg_12399" s="T660">"Dein Pelzmantel sieht wie eine Fischreuse aus und der obere Teil deines Pelzmantels sieht wie der Kopf einer Holzkelle aus."</ta>
            <ta e="T679" id="Seg_12400" s="T673">Der Junge sagte nichts dazu.</ta>
            <ta e="T681" id="Seg_12401" s="T679">Dann fuhr er los.</ta>
            <ta e="T684" id="Seg_12402" s="T681">Er kam gerade nach Hause.</ta>
            <ta e="T692" id="Seg_12403" s="T684">Die Mutter steht in der Tür, wartet auf den Jungen.</ta>
            <ta e="T704" id="Seg_12404" s="T692">"Junge, wo gehst du so lange hin? Ich saß hier auf dem Hügel und habe dich vermisst."</ta>
            <ta e="T709" id="Seg_12405" s="T704">Der junge Mann verbrachte die Nacht dort.</ta>
            <ta e="T723" id="Seg_12406" s="T709">Am nächsten Tag trieb er die Rentiere nach Hause, unter den Rentieren fand er zwei Bullen mit Katzenfell und fing sie.</ta>
            <ta e="T727" id="Seg_12407" s="T723">Er spannte die Rentiere ohne Bretter vor den Schlitten.</ta>
            <ta e="T733" id="Seg_12408" s="T727">Er zog den Mantel [mit Fell innen] und den Mantel [mit Fell draußen] an.</ta>
            <ta e="T741" id="Seg_12409" s="T733">Ob er seine Lenkstange nahm oder nicht, er fuhr los.</ta>
            <ta e="T753" id="Seg_12410" s="T741">Er kam gerade zum Zelt jener Leute, Lapta Brüder waren offensichtlich vorher gekommen.</ta>
            <ta e="T760" id="Seg_12411" s="T753">Der Junge löste die Zügel und betrat das Zelt.</ta>
            <ta e="T773" id="Seg_12412" s="T760">Er betrat das Zelt und blieb in der Tür stehen, der Mann von gestern sagte offenbar:</ta>
            <ta e="T779" id="Seg_12413" s="T773">"Seht ihr den armen Mann nicht?</ta>
            <ta e="T781" id="Seg_12414" s="T779">Lasst ihn nach vorne."</ta>
            <ta e="T789" id="Seg_12415" s="T781">Der Junge sah, wie man ihm neben dem alten Mann einen Platz freimachte.</ta>
            <ta e="T793" id="Seg_12416" s="T789">Der Junge setzt sich dort hin.</ta>
            <ta e="T795" id="Seg_12417" s="T793">Er beobachtete.</ta>
            <ta e="T802" id="Seg_12418" s="T795">Offenbar kam das junge Paar, setzte sich gemeinsam, und trank.</ta>
            <ta e="T810" id="Seg_12419" s="T802">Der Junge auch mit diesen Leuten, er fing auch an zu trinken.</ta>
            <ta e="T816" id="Seg_12420" s="T810">Anscheinend berauschte der Schnaps den jungen Mann.</ta>
            <ta e="T820" id="Seg_12421" s="T816">Der Schnaps berauschte ihn und der Junge fiel hin.</ta>
            <ta e="T830" id="Seg_12422" s="T820">Ein betrunkener Mann wird wohl nicht wissen, wie lange er dort gelegen hat.</ta>
            <ta e="T843" id="Seg_12423" s="T830">In einem Moment wachte der Junge auf: ein Mann, der mich schlägt, verfolgt mich.</ta>
            <ta e="T852" id="Seg_12424" s="T843">So liegend hörte er, dass irgendwo die Schwester jener weißen reichen Männern schrie:</ta>
            <ta e="T858" id="Seg_12425" s="T852">"Hey, was schaut ihr da zu!"</ta>
            <ta e="T866" id="Seg_12426" s="T858">Der Junge, der vor kurzem gekommen ist, wird geschlagen, [von] Lapta Brüdern.</ta>
            <ta e="T869" id="Seg_12427" s="T866">Der junge Mann hatte seine fünf Sinne beisammen.</ta>
            <ta e="T879" id="Seg_12428" s="T869">Mit seinen Ellbogen stieß er die Männer, die auf ihm drauf lagen, in alle Richtungen.</ta>
            <ta e="T894" id="Seg_12429" s="T879">Er warf sie zum zweiten Mal in alle Richtungen, die nenzischen Lapta Brüder starben offenbar, manche von ihnen in Stücke zerrissen.</ta>
            <ta e="T916" id="Seg_12430" s="T894">Manche zerriss er in der Mitte, der jüngster [Lapta] Bruder hat offenbar die ganze Zeit keinen Kopf; das Zelt blieb ganz ohne Stangen, das Fundament blieb ohne Stangen.</ta>
            <ta e="T924" id="Seg_12431" s="T916">Der Zeltsack und die Zeltabdeckung liegen den ganzen nächsten Tag an der Seite herum.</ta>
            <ta e="T933" id="Seg_12432" s="T924">Er stand auf und schaute sich um, Leute hatten sich versammelt und schauten ihn an.</ta>
            <ta e="T938" id="Seg_12433" s="T933">Das Mädchen ist die ganze Zeit nicht da.</ta>
            <ta e="T950" id="Seg_12434" s="T938">Er ging weiter, schob die Zeltabdeckung beiseite, offenbar sprang das Mädchen da heraus.</ta>
            <ta e="T955" id="Seg_12435" s="T950">Die Einheimischen blieben da.</ta>
            <ta e="T970" id="Seg_12436" s="T955">Sie bauten ein neues Zelt auf, tranken und aßen, und der weiße reiche Mann verheiratete den Jungen mit seiner Schwester.</ta>
            <ta e="T978" id="Seg_12437" s="T970">Nachdem man das junge Paar zusammengebracht hatte, trank man vierzehn Tage lang.</ta>
            <ta e="T990" id="Seg_12438" s="T978">Nachdem sie das junge Paar zusammengebracht hatten, zogen die weißen reichen Brüder auf die andere Seite, neben das Zelt des Jungen.</ta>
            <ta e="T999" id="Seg_12439" s="T990">Dort lebten sie alle zusammen und leben immer noch.</ta>
            <ta e="T1005" id="Seg_12440" s="T999">Das Märchen ist hiermit zu Ende.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T85" id="Seg_12441" s="T82">три брата Лаптыла.</ta>
            <ta e="T88" id="Seg_12442" s="T85">двое мать с сыном жили</ta>
            <ta e="T92" id="Seg_12443" s="T88">сын в роде людей вырос</ta>
            <ta e="T99" id="Seg_12444" s="T92">сын не знал отца когда умер</ta>
            <ta e="T105" id="Seg_12445" s="T99">в один прекрасный день сын к матери обратился (в роде обращения)</ta>
            <ta e="T113" id="Seg_12446" s="T105">мать мой отец ездил на оленях где мне покажи</ta>
            <ta e="T126" id="Seg_12447" s="T113">а оленей у матери с сыном столько было землей питались оленей такой постель была</ta>
            <ta e="T131" id="Seg_12448" s="T126">вроде [внешний вид, ввиде] вроде рыбей шкуры вид был</ta>
            <ta e="T137" id="Seg_12449" s="T131">их царапнуть оленей так домой пригоняли</ta>
            <ta e="T140" id="Seg_12450" s="T137">мать вроде говорит сыну [обращается к сыну]</ta>
            <ta e="T144" id="Seg_12451" s="T140">ты с оленями куда поедешь</ta>
            <ta e="T147" id="Seg_12452" s="T144">сын молвил [вроде отвечает]</ta>
            <ta e="T152" id="Seg_12453" s="T147">я никуда не поеду</ta>
            <ta e="T155" id="Seg_12454" s="T152">я мне скучаю здесь [сидит]</ta>
            <ta e="T158" id="Seg_12455" s="T155">там …щет в тундре ездить буду</ta>
            <ta e="T161" id="Seg_12456" s="T158">мать не (никуда не девалась)</ta>
            <ta e="T178" id="Seg_12457" s="T161">нарты внутри [средние] развязав такой толстый [толщины] аркан вниз достал (с нарт?) (удивился видимо) деревянный ковш ручка достал</ta>
            <ta e="T181" id="Seg_12458" s="T178">парню так подсказала</ta>
            <ta e="T190" id="Seg_12459" s="T181">чуть-чуть [в сторонке] ты оленьего [внутри] стада 3 пестрых (с белыми пятнами) быков [оленей] есть</ta>
            <ta e="T198" id="Seg_12460" s="T190">один [-ного из] пестрый [ого] бык [-ов / оленя] которые есть чуть-чуть больше этого поймай</ta>
            <ta e="T204" id="Seg_12461" s="T198">те сами вскрича придут</ta>
            <ta e="T210" id="Seg_12462" s="T204">парень аркан до половины собрал (намотал)</ta>
            <ta e="T214" id="Seg_12463" s="T210">так дальше (туда к оленям) пошел</ta>
            <ta e="T223" id="Seg_12464" s="T214">внутри [среди] оленей (в оленьем стаде) ища то, что мать говорила, быка поймал</ta>
            <ta e="T226" id="Seg_12465" s="T223">к саням принес запрягать</ta>
            <ta e="T243" id="Seg_12466" s="T226">тем временем мать будто бы из-под нарт такой нарту вниз достал будто бы из 30 мамонтовых рогов ножки (нарт)</ta>
            <ta e="T247" id="Seg_12467" s="T243">парень нарту запряг [стал запрягать]</ta>
            <ta e="T261" id="Seg_12468" s="T247">те двое [две] пестрые поменьше быка правда что мать говорила действительно они пришли</ta>
            <ta e="T266" id="Seg_12469" s="T261">парень нарту запрягать кончил</ta>
            <ta e="T274" id="Seg_12470" s="T266">мать 7 седьмиколенный из мамонтовых рог хорей мать дала</ta>
            <ta e="T280" id="Seg_12471" s="T274">пестрый вырезка сделанный [с узорными вырезками] гусём мать на него надела</ta>
            <ta e="T287" id="Seg_12472" s="T280">новый с вырезками [с узорчиками] сделанный ненецкий бокарь одела</ta>
            <ta e="T295" id="Seg_12473" s="T287">потом мать его так посоветовала: оленя головы не трогай</ta>
            <ta e="T303" id="Seg_12474" s="T295">они тебя возить будут и домой тебя привезут</ta>
            <ta e="T313" id="Seg_12475" s="T303">парень вожжи развязав вверх сел головы вперед пустил (ехать)</ta>
            <ta e="T321" id="Seg_12476" s="T313">потом вперед когда поехали или долго ехал или коротко</ta>
            <ta e="T328" id="Seg_12477" s="T321">один посреди [тундры] кочки верхушку остановился</ta>
            <ta e="T341" id="Seg_12478" s="T328">на кочке сидя так увидел там внизу у берега озера 3 чума стоят</ta>
            <ta e="T346" id="Seg_12479" s="T341">возле этих чумов ненецкие олени ходят</ta>
            <ta e="T352" id="Seg_12480" s="T346">парень не долго [сидя] к чумам поехал</ta>
            <ta e="T359" id="Seg_12481" s="T352">в центре этих (чумов) сильно большой чум стоит</ta>
            <ta e="T365" id="Seg_12482" s="T359">напротив этого большого чума останови [обратно] остановил</ta>
            <ta e="T370" id="Seg_12483" s="T365">остановив внутри саней сидит</ta>
            <ta e="T375" id="Seg_12484" s="T370">из дома на улицу люди вышли</ta>
            <ta e="T383" id="Seg_12485" s="T375">один старик к парню пришел [идя] обращается (к нему)</ta>
            <ta e="T387" id="Seg_12486" s="T383">с какой земли человек ты будешь</ta>
            <ta e="T399" id="Seg_12487" s="T387">парень молвил [отвечает] я не знаю человека [что за человек] никого не знаю</ta>
            <ta e="T403" id="Seg_12488" s="T399">[где] один жил с этой местности</ta>
            <ta e="T409" id="Seg_12489" s="T403">а ты что за старик будешь</ta>
            <ta e="T413" id="Seg_12490" s="T409">этот стар. так отвечает </ta>
            <ta e="T424" id="Seg_12491" s="T413">мы трое белые богачи братья, самый большой белый богач я и есть</ta>
            <ta e="T432" id="Seg_12492" s="T424">те на улицу вышедшие люди оленя ловят</ta>
            <ta e="T441" id="Seg_12493" s="T432">в этом стаде одного безрогатого ненецкого нерождающую [яловую теленка] важенку ловят</ta>
            <ta e="T446" id="Seg_12494" s="T441">так парень смотрит поймать не могут</ta>
            <ta e="T459" id="Seg_12495" s="T446">парень в нарте средине сидя аркан собрал (чтоб петлю делать) важенку возле парня поблизости прогнали</ta>
            <ta e="T470" id="Seg_12496" s="T459">парень важенку подпустивши (поближе) поймав аркана за конец дернул важенку шею срезал</ta>
            <ta e="T475" id="Seg_12497" s="T470">парень аркан обратно собрал.</ta>
            <ta e="T483" id="Seg_12498" s="T475">молвил [сам говорит] какого черта я веревку кровью намазал?</ta>
            <ta e="T488" id="Seg_12499" s="T483">эти люди оленя ободрали</ta>
            <ta e="T492" id="Seg_12500" s="T488">ненецкие люди на улице строганку стали (есть)</ta>
            <ta e="T500" id="Seg_12501" s="T492">парня попросили (заставили) кушай с нами ты с нами куша</ta>
            <ta e="T508" id="Seg_12502" s="T500">парень ответил я строганку не знаю</ta>
            <ta e="T515" id="Seg_12503" s="T508">тот же самый белый богач старик молвил</ta>
            <ta e="T518" id="Seg_12504" s="T515">вы зачем сидите</ta>
            <ta e="T522" id="Seg_12505" s="T518">мясо в чум занесите сварите</ta>
            <ta e="T532" id="Seg_12506" s="T522">парень смотрит из чума на улицу одна девушка вышла</ta>
            <ta e="T539" id="Seg_12507" s="T532">девушка выйдя мясо нарезала в чум занесла</ta>
            <ta e="T549" id="Seg_12508" s="T539">не долго после этого сидя старик и парень в чум зашли</ta>
            <ta e="T556" id="Seg_12509" s="T549">парень чум зайдя у порога хотел остановиться</ta>
            <ta e="T565" id="Seg_12510" s="T556">старик молвил вы что просто человека не видите</ta>
            <ta e="T571" id="Seg_12511" s="T565">пропустите вперед зачем там в дверях заставляете стоять</ta>
            <ta e="T577" id="Seg_12512" s="T571">парня вперед пропустили возле старика посадили</ta>
            <ta e="T584" id="Seg_12513" s="T577">мясо из котла достали всяческие еду поставили</ta>
            <ta e="T592" id="Seg_12514" s="T584">двое человек на улицу выйдя воды [спирта] в чум занесли</ta>
            <ta e="T597" id="Seg_12515" s="T592">стали спирт пить и закусывать</ta>
            <ta e="T602" id="Seg_12516" s="T597">парень во время еды так услыхал</ta>
            <ta e="T612" id="Seg_12517" s="T602">дескать Лаптеля 3 братьев свататься приехали белый богача братьям</ta>
            <ta e="T617" id="Seg_12518" s="T612">белобогач братья одна сестра была</ta>
            <ta e="T620" id="Seg_12519" s="T617">к ней свататься и приехали</ta>
            <ta e="T627" id="Seg_12520" s="T620">поев хорошо кончили стали разъезжаться в разные стороны (каждый в свою)</ta>
            <ta e="T634" id="Seg_12521" s="T627">словами сложились согласились</ta>
            <ta e="T640" id="Seg_12522" s="T634">завтра молодоженов вместе посадят</ta>
            <ta e="T647" id="Seg_12523" s="T640">люди стали ехать парень тоже домой хотел ехать</ta>
            <ta e="T660" id="Seg_12524" s="T647">на улицу вышел парень кожух надел Лаптыля самого старшего брата так молвил парню</ta>
            <ta e="T673" id="Seg_12525" s="T660">кожух у тебя как морда а кожуха голова видимо как деревянный кувшин</ta>
            <ta e="T679" id="Seg_12526" s="T673">парень ничего на это не ответил</ta>
            <ta e="T681" id="Seg_12527" s="T679">потом поехал</ta>
            <ta e="T684" id="Seg_12528" s="T681">домой приехал</ta>
            <ta e="T692" id="Seg_12529" s="T684">мать у дверей стоя ждет парня ждя </ta>
            <ta e="T704" id="Seg_12530" s="T692">парень ты куда так долго ездил я соскучился (дома сидеть) там я на кочках посиживал (отсиживался)</ta>
            <ta e="T709" id="Seg_12531" s="T704">в эту ночь парень переночевал</ta>
            <ta e="T723" id="Seg_12532" s="T709">на другой день оленей домой пригнал в оленьем стаде (внутри) разыскал два как у кошки шерсть быков и поймал</ta>
            <ta e="T727" id="Seg_12533" s="T723">нарты без досок запряг</ta>
            <ta e="T733" id="Seg_12534" s="T727">старую мальцу и кожуху такую же надел</ta>
            <ta e="T741" id="Seg_12535" s="T733">харей взял или нет так поехал (не торопясь)</ta>
            <ta e="T753" id="Seg_12536" s="T741">к тем людям в чум в приехал смотрит братья Лаптыля раньше (его) приехали</ta>
            <ta e="T760" id="Seg_12537" s="T753">парень вожжи привязал в чум зашел</ta>
            <ta e="T773" id="Seg_12538" s="T760">в чум войдя у порога хотел встать старик вчерашний вроде сказал</ta>
            <ta e="T779" id="Seg_12539" s="T773">вы что бедного человека не видите</ta>
            <ta e="T781" id="Seg_12540" s="T779">вперед пропустите</ta>
            <ta e="T789" id="Seg_12541" s="T781">парень видит со стариком рядом место пустым сделали (раздвинулись)</ta>
            <ta e="T793" id="Seg_12542" s="T789">парень туда сел</ta>
            <ta e="T795" id="Seg_12543" s="T793">так смотрит</ta>
            <ta e="T802" id="Seg_12544" s="T795">молодоженов вместе усадили и пьют</ta>
            <ta e="T810" id="Seg_12545" s="T802">парень тоже с этими людьми тоже стал пить</ta>
            <ta e="T816" id="Seg_12546" s="T810">парня спирт опьянил</ta>
            <ta e="T820" id="Seg_12547" s="T816">пьяный парень упал</ta>
            <ta e="T830" id="Seg_12548" s="T820">пьяный человек чего будет знать долго ли так он лежал</ta>
            <ta e="T843" id="Seg_12549" s="T830">в одно время парень верх очнулся человек дерётся (бьёт)</ta>
            <ta e="T852" id="Seg_12550" s="T843">так лежа слушает где-то да белых богачей сестра кричала</ta>
            <ta e="T858" id="Seg_12551" s="T852">вы что смотрите</ta>
            <ta e="T866" id="Seg_12552" s="T858">тот нового приезжего парня (бьют) Лоптыль братья</ta>
            <ta e="T869" id="Seg_12553" s="T866">парень не растерялся</ta>
            <ta e="T879" id="Seg_12554" s="T869">так лёжа на нём развалившись (?) навалившихся людей локтём сторону (наразброс) ударил</ta>
            <ta e="T894" id="Seg_12555" s="T879">вторично в сторону (осмотрелся) ненецкие братья Лоптыл так умерли, некоторые пополам переломились</ta>
            <ta e="T916" id="Seg_12556" s="T894">некоторые … раскололись а самый младший мудрый брат Лаптыл головы нет а жердей(?) и чум нету жердей концы только остались</ta>
            <ta e="T924" id="Seg_12557" s="T916">жерди и чум (шкура чума) в сторону в стороне лежат</ta>
            <ta e="T933" id="Seg_12558" s="T924">вверх встал так в сторону смотрит люди удивлённо его смотрят</ta>
            <ta e="T938" id="Seg_12559" s="T933">а та девушка нет</ta>
            <ta e="T950" id="Seg_12560" s="T938">туда пошел где шкура чума вверх в стороны раскрыл (чум) ? девушка оттуда выскочила</ta>
            <ta e="T955" id="Seg_12561" s="T950">никуда не девались (стали деваться) те, которые остались люди</ta>
            <ta e="T970" id="Seg_12562" s="T955">другой чум поставили только и начали пить и есть белый богач старик сестру этому парню заставил жениться </ta>
            <ta e="T978" id="Seg_12563" s="T970">молодоженов вместе сажают 14 дней пили (праздновали)</ta>
            <ta e="T990" id="Seg_12564" s="T978">молодоженов вместе посадив белый богач братья с парнем переехали к парню к чуму поближе</ta>
            <ta e="T999" id="Seg_12565" s="T990">вместе в одну кучу жили теперь так наверное и живут</ta>
            <ta e="T1005" id="Seg_12566" s="T999">сказка на этом и кончилась</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T85" id="Seg_12567" s="T82">[OSV:] "Lapta(?)" - a proper Nenets name, 'lapta' in Nenets means 'a plain'.</ta>
            <ta e="T105" id="Seg_12568" s="T99">[OSV:] 1) The form "əmɨnntɨnɨk" has been corrected into "əmɨntɨnɨk"; 2) unclear expression which is then often used in the text - "nalʼat ɛsɨqo" with the meaning "to say". </ta>
            <ta e="T126" id="Seg_12569" s="T113">[OSV:] The form "ɔːtamtij" has been corrected into "ɔːtantij". </ta>
            <ta e="T137" id="Seg_12570" s="T131">[OSV:] an unclear form "ɔːtamtip", more likely would be the form "ɔːta-t-i-p" (deer-PL-EP-ACC).</ta>
            <ta e="T161" id="Seg_12571" s="T158">[OSV:] 1) grammatically correct would be the verbal form "qatta"; maybe it is a form of "kətɨqo" - "to say"; 2) "ašša qattɨqo" has here the meaning "to keep one's wits" , see also 4.6, 7.5, 8.1.</ta>
            <ta e="T204" id="Seg_12572" s="T198"> ‎</ta>
            <ta e="T274" id="Seg_12573" s="T266">[OSV:] "narapo" - a long wooden pole used to drive a deer sledge.</ta>
            <ta e="T280" id="Seg_12574" s="T274">[OSV:] 1) grammatically correct would be the form "tokkalaltɨmmɨntɨt" (-ntɨ - INFER); 2) "soqqɨ" - clothing of the deer skin with the fur inside.</ta>
            <ta e="T303" id="Seg_12575" s="T295">[OSV:] unclear form "našte". </ta>
            <ta e="T352" id="Seg_12576" s="T346">[OSV:] "ašša qattɨqo" has here possibly the meaning "to keep one's wits", see also 3.12, 7.5, 8.1.</ta>
            <ta e="T459" id="Seg_12577" s="T446">[OSV:] The form "qaːlɨmtɨ" has been corrected into "qaːlɨntɨ". </ta>
            <ta e="T584" id="Seg_12578" s="T577">[OSV:] The form "wäčʼitsä" contains an affix -t which is possibly an allomorph of "-ntɨ" (OBL.3SG).</ta>
            <ta e="T634" id="Seg_12579" s="T627">[OSV:] "nʼentɨ pinqo" - "to come to an agreement".</ta>
            <ta e="T640" id="Seg_12580" s="T634">[OSV:] "qumaqum(ɨ)t" - "an engaged couple".</ta>
            <ta e="T673" id="Seg_12581" s="T660">[OSV:] "opčʼin" - "a bailer for bailing out fish".</ta>
            <ta e="T681" id="Seg_12582" s="T679">[OSV:] unclear verbal form "laqaltɛːmnɨt".</ta>
            <ta e="T727" id="Seg_12583" s="T723">[OSV:] The sledge was harnessed without underlay of boards. </ta>
            <ta e="T733" id="Seg_12584" s="T727">[OSV:] "malʼčʼa" - a clothing of deer skin with the fur inside. [KuAI:] also used among Nenets</ta>
            <ta e="T773" id="Seg_12585" s="T760">[OSV:] "tälʼ čʼeːlɨ" - "yesterday".</ta>
            <ta e="T779" id="Seg_12586" s="T773">[OSV:] "qoštɨlʼ" has here the meaning "poor".</ta>
            <ta e="T802" id="Seg_12587" s="T795">[OSV:] "qumaqum(ɨt)" - "an engaged couple".</ta>
            <ta e="T820" id="Seg_12588" s="T816">[OSV:] grammatically correct would be the Genitive of the subject "üt" of the converb "šeːrla".</ta>
            <ta e="T843" id="Seg_12589" s="T830">[OSV:] the unclear affix "-rorr" in "qəttɨrorrij" could be a variant of "-lɔːl" (MULO).</ta>
            <ta e="T869" id="Seg_12590" s="T866">[OSV:] "ašša qattɨqo" has here the meaning "to keep one's wits"^ see also 3.12, 4.6, 8.1.</ta>
            <ta e="T924" id="Seg_12591" s="T916">[OSV:] "mɔːtɨkota" - sacking used for covering the tent; "mɔːtɨqop" - deer skins with the fur outside used for covering the tent.</ta>
            <ta e="T950" id="Seg_12592" s="T938">[OSV:] unclear verbal form "putilmɔːnna".</ta>
            <ta e="T955" id="Seg_12593" s="T950">[OSV:] may be "ašša qattɨqo" has here the meaning "to keep one's wits", see also in 3.12, 4.6, 7.5.</ta>
            <ta e="T999" id="Seg_12594" s="T990">[OSV:] unclear collocation tiːtäqos.</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T733" id="Seg_12595" s="T727">мальца — одежда, которую носят и ненцы; шерсть вовнутрь</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T925" />
            <conversion-tli id="T926" />
            <conversion-tli id="T927" />
            <conversion-tli id="T928" />
            <conversion-tli id="T929" />
            <conversion-tli id="T930" />
            <conversion-tli id="T931" />
            <conversion-tli id="T932" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T935" />
            <conversion-tli id="T936" />
            <conversion-tli id="T937" />
            <conversion-tli id="T938" />
            <conversion-tli id="T939" />
            <conversion-tli id="T940" />
            <conversion-tli id="T941" />
            <conversion-tli id="T942" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T945" />
            <conversion-tli id="T946" />
            <conversion-tli id="T947" />
            <conversion-tli id="T948" />
            <conversion-tli id="T949" />
            <conversion-tli id="T950" />
            <conversion-tli id="T951" />
            <conversion-tli id="T952" />
            <conversion-tli id="T953" />
            <conversion-tli id="T954" />
            <conversion-tli id="T955" />
            <conversion-tli id="T956" />
            <conversion-tli id="T957" />
            <conversion-tli id="T958" />
            <conversion-tli id="T959" />
            <conversion-tli id="T960" />
            <conversion-tli id="T961" />
            <conversion-tli id="T962" />
            <conversion-tli id="T963" />
            <conversion-tli id="T964" />
            <conversion-tli id="T965" />
            <conversion-tli id="T966" />
            <conversion-tli id="T967" />
            <conversion-tli id="T968" />
            <conversion-tli id="T969" />
            <conversion-tli id="T970" />
            <conversion-tli id="T971" />
            <conversion-tli id="T972" />
            <conversion-tli id="T973" />
            <conversion-tli id="T974" />
            <conversion-tli id="T975" />
            <conversion-tli id="T976" />
            <conversion-tli id="T977" />
            <conversion-tli id="T978" />
            <conversion-tli id="T979" />
            <conversion-tli id="T980" />
            <conversion-tli id="T981" />
            <conversion-tli id="T982" />
            <conversion-tli id="T983" />
            <conversion-tli id="T984" />
            <conversion-tli id="T985" />
            <conversion-tli id="T986" />
            <conversion-tli id="T987" />
            <conversion-tli id="T988" />
            <conversion-tli id="T989" />
            <conversion-tli id="T990" />
            <conversion-tli id="T991" />
            <conversion-tli id="T992" />
            <conversion-tli id="T993" />
            <conversion-tli id="T994" />
            <conversion-tli id="T995" />
            <conversion-tli id="T996" />
            <conversion-tli id="T997" />
            <conversion-tli id="T998" />
            <conversion-tli id="T999" />
            <conversion-tli id="T1000" />
            <conversion-tli id="T1001" />
            <conversion-tli id="T1002" />
            <conversion-tli id="T1003" />
            <conversion-tli id="T1004" />
            <conversion-tli id="T1005" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
