<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>YIF_196X_Kamadzha2_flk</transcription-name>
         <referenced-file url="YIF_196X_Kamadzha2_flk.wav" />
         <referenced-file url="YIF_196X_Kamadzha2_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">YIF_196X_Kamadzha2_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">468</ud-information>
            <ud-information attribute-name="# HIAT:w">283</ud-information>
            <ud-information attribute-name="# e">288</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">7</ud-information>
            <ud-information attribute-name="# HIAT:u">51</ud-information>
            <ud-information attribute-name="# sc">102</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="YIF">
            <abbreviation>YIF</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="6.058" type="appl" />
         <tli id="T319" time="6.386529692117053" />
         <tli id="T2" time="7.186512534553427" />
         <tli id="T320" time="7.975494519111619" type="intp" />
         <tli id="T4" time="8.177" type="appl" />
         <tli id="T6" time="9.276" type="appl" />
         <tli id="T7" time="9.666" type="appl" />
         <tli id="T8" time="10.28025" type="appl" />
         <tli id="T9" time="10.8945" type="appl" />
         <tli id="T10" time="11.50875" type="appl" />
         <tli id="T11" time="12.433066676198646" />
         <tli id="T12" time="12.632" type="appl" />
         <tli id="T13" time="13.311" type="appl" />
         <tli id="T14" time="13.99" type="appl" />
         <tli id="T15" time="14.669" type="appl" />
         <tli id="T16" time="15.348" type="appl" />
         <tli id="T17" time="16.632" type="appl" />
         <tli id="T3" time="17.01925" type="intp" />
         <tli id="T18" time="17.4065" type="appl" />
         <tli id="T19" time="18.181" type="appl" />
         <tli id="T20" time="18.9555" type="appl" />
         <tli id="T21" time="19.73" type="appl" />
         <tli id="T22" time="20.506" type="appl" />
         <tli id="T23" time="21.117857142857144" type="appl" />
         <tli id="T24" time="21.729714285714287" type="appl" />
         <tli id="T25" time="22.341571428571427" type="appl" />
         <tli id="T26" time="22.95342857142857" type="appl" />
         <tli id="T27" time="23.565285714285714" type="appl" />
         <tli id="T28" time="24.177142857142858" type="appl" />
         <tli id="T29" time="24.789" type="appl" />
         <tli id="T30" time="26.249" type="appl" />
         <tli id="T31" time="27.38481818181818" type="appl" />
         <tli id="T32" time="28.520636363636363" type="appl" />
         <tli id="T33" time="29.656454545454544" type="appl" />
         <tli id="T34" time="30.792272727272728" type="appl" />
         <tli id="T35" time="31.92809090909091" type="appl" />
         <tli id="T36" time="33.06390909090909" type="appl" />
         <tli id="T37" time="34.19972727272727" type="appl" />
         <tli id="T38" time="35.33554545454545" type="appl" />
         <tli id="T39" time="36.471363636363634" type="appl" />
         <tli id="T40" time="37.60718181818182" type="appl" />
         <tli id="T41" time="38.743" type="appl" />
         <tli id="T42" time="39.471" type="appl" />
         <tli id="T5" time="39.522" type="intp" />
         <tli id="T43" time="40.483333333333334" type="appl" />
         <tli id="T44" time="41.495666666666665" type="appl" />
         <tli id="T45" time="42.507999999999996" type="appl" />
         <tli id="T46" time="43.52033333333333" type="appl" />
         <tli id="T47" time="44.53266666666667" type="appl" />
         <tli id="T48" time="45.545" type="appl" />
         <tli id="T115" time="46.65233276141455" />
         <tli id="T49" time="47.24565336955485" />
         <tli id="T50" time="47.82185714285714" type="appl" />
         <tli id="T51" time="48.419714285714285" type="appl" />
         <tli id="T52" time="49.01757142857143" type="appl" />
         <tli id="T53" time="49.615428571428566" type="appl" />
         <tli id="T54" time="50.21328571428571" type="appl" />
         <tli id="T55" time="50.45891778667429" />
         <tli id="T56" time="51.312232818606425" />
         <tli id="T57" time="52.964" type="appl" />
         <tli id="T58" time="53.65533333333333" type="appl" />
         <tli id="T59" time="54.346666666666664" type="appl" />
         <tli id="T60" time="55.038" type="appl" />
         <tli id="T61" time="55.72933333333333" type="appl" />
         <tli id="T62" time="56.42066666666667" type="appl" />
         <tli id="T63" time="57.112" type="appl" />
         <tli id="T326" time="57.45766666666667" type="intp" />
         <tli id="T64" time="57.803333333333335" type="appl" />
         <tli id="T65" time="58.49466666666667" type="appl" />
         <tli id="T66" time="59.186" type="appl" />
         <tli id="T67" time="61.05" type="appl" />
         <tli id="T68" time="61.710499999999996" type="appl" />
         <tli id="T69" time="62.370999999999995" type="appl" />
         <tli id="T70" time="63.0315" type="appl" />
         <tli id="T71" time="63.692" type="appl" />
         <tli id="T72" time="65.556" type="appl" />
         <tli id="T73" time="66.57377777777778" type="appl" />
         <tli id="T74" time="67.59155555555556" type="appl" />
         <tli id="T78" time="68.10044444444443" type="intp" />
         <tli id="T75" time="68.60933333333332" type="appl" />
         <tli id="T76" time="69.6271111111111" type="appl" />
         <tli id="T327" time="70.136" type="intp" />
         <tli id="T77" time="70.64488888888889" type="appl" />
         <tli id="T79" time="72.68044444444445" type="appl" />
         <tli id="T80" time="73.69822222222221" type="appl" />
         <tli id="T81" time="74.716" type="appl" />
         <tli id="T82" time="76.802" type="appl" />
         <tli id="T328" time="76.85300000000001" type="intp" />
         <tli id="T83" time="77.40966666666667" type="appl" />
         <tli id="T84" time="78.01733333333334" type="appl" />
         <tli id="T85" time="78.625" type="appl" />
         <tli id="T86" time="79.23266666666667" type="appl" />
         <tli id="T87" time="79.84033333333333" type="appl" />
         <tli id="T88" time="80.44800000000001" type="appl" />
         <tli id="T89" time="81.05566666666667" type="appl" />
         <tli id="T90" time="81.66333333333334" type="appl" />
         <tli id="T91" time="82.271" type="appl" />
         <tli id="T92" time="84.221" type="appl" />
         <tli id="T93" time="85.0828" type="appl" />
         <tli id="T94" time="85.94460000000001" type="appl" />
         <tli id="T95" time="86.8064" type="appl" />
         <tli id="T96" time="87.6682" type="appl" />
         <tli id="T97" time="88.53" type="appl" />
         <tli id="T98" time="89.875" type="appl" />
         <tli id="T99" time="90.8208" type="appl" />
         <tli id="T100" time="91.7666" type="appl" />
         <tli id="T101" time="92.7124" type="appl" />
         <tli id="T102" time="93.6582" type="appl" />
         <tli id="T103" time="94.604" type="appl" />
         <tli id="T104" time="96.085" type="appl" />
         <tli id="T105" time="96.514" type="appl" />
         <tli id="T106" time="96.943" type="appl" />
         <tli id="T107" time="97.372" type="appl" />
         <tli id="T108" time="97.801" type="appl" />
         <tli id="T109" time="99.048" type="appl" />
         <tli id="T110" time="99.84425" type="appl" />
         <tli id="T111" time="100.6405" type="appl" />
         <tli id="T112" time="101.43675" type="appl" />
         <tli id="T113" time="102.233" type="appl" />
         <tli id="T114" time="102.887" type="appl" />
         <tli id="T116" time="103.85" type="appl" />
         <tli id="T117" time="104.3315" type="appl" />
         <tli id="T118" time="104.813" type="appl" />
         <tli id="T119" time="107.159" type="appl" />
         <tli id="T120" time="107.71300000000001" type="appl" />
         <tli id="T121" time="108.267" type="appl" />
         <tli id="T122" time="108.821" type="appl" />
         <tli id="T123" time="109.375" type="appl" />
         <tli id="T124" time="109.929" type="appl" />
         <tli id="T125" time="110.483" type="appl" />
         <tli id="T126" time="111.03699999999999" type="appl" />
         <tli id="T127" time="111.591" type="appl" />
         <tli id="T128" time="112.641" type="appl" />
         <tli id="T129" time="113.85900000000001" type="appl" />
         <tli id="T130" time="115.077" type="appl" />
         <tli id="T131" time="116.295" type="appl" />
         <tli id="T132" time="117.159" type="appl" />
         <tli id="T133" time="117.6084" type="appl" />
         <tli id="T134" time="118.0578" type="appl" />
         <tli id="T135" time="118.50720000000001" type="appl" />
         <tli id="T136" time="118.95660000000001" type="appl" />
         <tli id="T137" time="119.406" type="appl" />
         <tli id="T324" time="120.24408774187398" />
         <tli id="T329" time="121.1640680106758" type="intp" />
         <tli id="T323" time="122.08404827947764" />
         <tli id="T330" time="124.26733478696025" type="intp" />
         <tli id="T322" time="126.45062129444285" />
         <tli id="T325" time="126.7272820274521" type="intp" />
         <tli id="T321" time="127.00394276046136" />
         <tli id="T138" time="130.19720760651987" />
         <tli id="T139" time="130.44386898293774" />
         <tli id="T140" time="131.60384410447048" />
         <tli id="T141" time="132.34382823372414" />
         <tli id="T142" time="132.51715784958535" />
         <tli id="T143" time="132.91714927080355" />
         <tli id="T144" time="133.801" type="appl" />
         <tli id="T145" time="134.257" type="appl" />
         <tli id="T146" time="134.90333333333334" type="appl" />
         <tli id="T147" time="135.54966666666667" type="appl" />
         <tli id="T148" time="136.196" type="appl" />
         <tli id="T149" time="136.504" type="appl" />
         <tli id="T150" time="137.143" type="appl" />
         <tli id="T151" time="137.78199999999998" type="appl" />
         <tli id="T152" time="138.421" type="appl" />
         <tli id="T153" time="139.06" type="appl" />
         <tli id="T154" time="140.418" type="appl" />
         <tli id="T155" time="141.35" type="appl" />
         <tli id="T156" time="142.28199999999998" type="appl" />
         <tli id="T157" time="143.214" type="appl" />
         <tli id="T158" time="144.146" type="appl" />
         <tli id="T159" time="145.01" type="appl" />
         <tli id="T160" time="145.96075" type="appl" />
         <tli id="T161" time="146.9115" type="appl" />
         <tli id="T162" time="147.86225" type="appl" />
         <tli id="T163" time="148.813" type="appl" />
         <tli id="T164" time="149.615" type="appl" />
         <tli id="T165" time="150.33733333333333" type="appl" />
         <tli id="T166" time="151.0596666666667" type="appl" />
         <tli id="T167" time="151.782" type="appl" />
         <tli id="T168" time="152.50433333333334" type="appl" />
         <tli id="T169" time="153.2266666666667" type="appl" />
         <tli id="T170" time="154.11669459536745" />
         <tli id="T171" time="155.455" type="appl" />
         <tli id="T172" time="156.48575" type="appl" />
         <tli id="T173" time="157.5165" type="appl" />
         <tli id="T174" time="158.54725000000002" type="appl" />
         <tli id="T175" time="159.76990668191783" />
         <tli id="T176" time="161.615" type="appl" />
         <tli id="T331" time="162.099" type="intp" />
         <tli id="T177" time="162.583" type="appl" />
         <tli id="T178" time="163.55100000000002" type="appl" />
         <tli id="T179" time="164.519" type="appl" />
         <tli id="T180" time="165.48700000000002" type="appl" />
         <tli id="T181" time="166.455" type="appl" />
         <tli id="T182" time="166.726" type="appl" />
         <tli id="T183" time="167.14275" type="appl" />
         <tli id="T184" time="167.5595" type="appl" />
         <tli id="T185" time="167.97625" type="appl" />
         <tli id="T186" time="168.393" type="appl" />
         <tli id="T187" time="168.726" type="appl" />
         <tli id="T188" time="169.26925" type="appl" />
         <tli id="T189" time="169.8125" type="appl" />
         <tli id="T190" time="170.35575" type="appl" />
         <tli id="T191" time="170.899" type="appl" />
         <tli id="T192" time="172.109" type="appl" />
         <tli id="T193" time="172.69387500000002" type="appl" />
         <tli id="T194" time="173.27875" type="appl" />
         <tli id="T195" time="173.863625" type="appl" />
         <tli id="T196" time="174.44850000000002" type="appl" />
         <tli id="T197" time="175.033375" type="appl" />
         <tli id="T198" time="175.61825000000002" type="appl" />
         <tli id="T199" time="176.203125" type="appl" />
         <tli id="T200" time="176.788" type="appl" />
         <tli id="T201" time="177.936" type="appl" />
         <tli id="T202" time="178.8252" type="appl" />
         <tli id="T203" time="179.7144" type="appl" />
         <tli id="T204" time="180.6036" type="appl" />
         <tli id="T205" time="181.49280000000002" type="appl" />
         <tli id="T333" time="181.62778416261557" type="intp" />
         <tli id="T206" time="182.3894215518063" />
         <tli id="T332" time="182.49375374130204" type="intp" />
         <tli id="T207" time="182.81383333333335" type="appl" />
         <tli id="T208" time="183.24566666666666" type="appl" />
         <tli id="T209" time="183.6775" type="appl" />
         <tli id="T210" time="184.10933333333335" type="appl" />
         <tli id="T211" time="184.54116666666667" type="appl" />
         <tli id="T212" time="184.973" type="appl" />
         <tli id="T213" time="186.319" type="appl" />
         <tli id="T214" time="186.87866666666665" type="appl" />
         <tli id="T215" time="187.43833333333333" type="appl" />
         <tli id="T216" time="187.998" type="appl" />
         <tli id="T217" time="188.55766666666665" type="appl" />
         <tli id="T218" time="189.11733333333333" type="appl" />
         <tli id="T219" time="189.677" type="appl" />
         <tli id="T220" time="189.739" type="appl" />
         <tli id="T221" time="190.2575" type="appl" />
         <tli id="T222" time="190.776" type="appl" />
         <tli id="T223" time="191.628" type="appl" />
         <tli id="T224" time="193.07549999999998" type="appl" />
         <tli id="T334" time="194.1623228719855" type="intp" />
         <tli id="T225" time="195.249145743971" />
         <tli id="T226" time="195.97050000000002" type="appl" />
         <tli id="T227" time="197.418" type="appl" />
         <tli id="T228" time="198.838" type="appl" />
         <tli id="T229" time="199.2885" type="appl" />
         <tli id="T230" time="199.73899999999998" type="appl" />
         <tli id="T231" time="200.18949999999998" type="appl" />
         <tli id="T232" time="200.64" type="appl" />
         <tli id="T233" time="200.801" type="appl" />
         <tli id="T234" time="201.7429" type="appl" />
         <tli id="T235" time="202.6848" type="appl" />
         <tli id="T236" time="203.6267" type="appl" />
         <tli id="T335" time="204.09765" type="intp" />
         <tli id="T237" time="204.5686" type="appl" />
         <tli id="T238" time="205.51049999999998" type="appl" />
         <tli id="T239" time="206.45239999999998" type="appl" />
         <tli id="T240" time="207.3943" type="appl" />
         <tli id="T241" time="208.3362" type="appl" />
         <tli id="T242" time="209.2781" type="appl" />
         <tli id="T243" time="210.22" type="appl" />
         <tli id="T244" time="211.529" type="appl" />
         <tli id="T336" time="211.98683333333332" type="intp" />
         <tli id="T245" time="212.44466666666665" type="appl" />
         <tli id="T246" time="213.36033333333333" type="appl" />
         <tli id="T247" time="214.276" type="appl" />
         <tli id="T248" time="215.19166666666666" type="appl" />
         <tli id="T249" time="216.10733333333332" type="appl" />
         <tli id="T250" time="217.023" type="appl" />
         <tli id="T251" time="219.135" type="appl" />
         <tli id="T252" time="220.07819999999998" type="appl" />
         <tli id="T253" time="221.0214" type="appl" />
         <tli id="T254" time="221.9646" type="appl" />
         <tli id="T255" time="222.90779999999998" type="appl" />
         <tli id="T256" time="223.851" type="appl" />
         <tli id="T257" time="224.7942" type="appl" />
         <tli id="T337" time="225.26579999999998" type="intp" />
         <tli id="T258" time="225.73739999999998" type="appl" />
         <tli id="T259" time="226.6806" type="appl" />
         <tli id="T260" time="227.6238" type="appl" />
         <tli id="T261" time="228.56699999999998" type="appl" />
         <tli id="T262" time="229.5102" type="appl" />
         <tli id="T263" time="230.4534" type="appl" />
         <tli id="T264" time="231.39659999999998" type="appl" />
         <tli id="T265" time="232.3398" type="appl" />
         <tli id="T266" time="233.283" type="appl" />
         <tli id="T267" time="233.9" type="appl" />
         <tli id="T268" time="234.471" type="appl" />
         <tli id="T269" time="235.042" type="appl" />
         <tli id="T270" time="235.613" type="appl" />
         <tli id="T271" time="236.184" type="appl" />
         <tli id="T272" time="238.332" type="appl" />
         <tli id="T273" time="238.89374999999998" type="appl" />
         <tli id="T274" time="239.4555" type="appl" />
         <tli id="T275" time="240.01725" type="appl" />
         <tli id="T276" time="240.579" type="appl" />
         <tli id="T277" time="241.14075" type="appl" />
         <tli id="T278" time="241.7025" type="appl" />
         <tli id="T279" time="242.26425" type="appl" />
         <tli id="T280" time="242.826" type="appl" />
         <tli id="T281" time="245.196" type="appl" />
         <tli id="T282" time="245.9967142857143" type="appl" />
         <tli id="T283" time="246.79742857142855" type="appl" />
         <tli id="T284" time="247.59814285714285" type="appl" />
         <tli id="T285" time="248.39885714285714" type="appl" />
         <tli id="T286" time="249.19957142857143" type="appl" />
         <tli id="T287" time="250.0002857142857" type="appl" />
         <tli id="T288" time="250.801" type="appl" />
         <tli id="T289" time="252.547" type="appl" />
         <tli id="T290" time="253.02790653893814" />
         <tli id="T291" time="253.281234439043" />
         <tli id="T339" time="254.1145498999142" />
         <tli id="T338" time="255.89451172433516" />
         <tli id="T292" time="257.695" type="appl" />
         <tli id="T293" time="259.405" type="appl" />
         <tli id="T294" time="260.1902" type="appl" />
         <tli id="T295" time="260.9754" type="appl" />
         <tli id="T296" time="261.7606" type="appl" />
         <tli id="T297" time="262.5458" type="appl" />
         <tli id="T298" time="263.331" type="appl" />
         <tli id="T299" time="264.676" type="appl" />
         <tli id="T300" time="265.50316666666663" type="appl" />
         <tli id="T301" time="266.33033333333333" type="appl" />
         <tli id="T302" time="267.1575" type="appl" />
         <tli id="T303" time="267.98466666666667" type="appl" />
         <tli id="T304" time="268.8118333333333" type="appl" />
         <tli id="T305" time="269.639" type="appl" />
         <tli id="T306" time="271.243" type="appl" />
         <tli id="T307" time="271.988" type="appl" />
         <tli id="T308" time="272.733" type="appl" />
         <tli id="T309" time="273.478" type="appl" />
         <tli id="T310" time="274.355" type="appl" />
         <tli id="T311" time="275.02775" type="appl" />
         <tli id="T312" time="275.70050000000003" type="appl" />
         <tli id="T313" time="276.37325" type="appl" />
         <tli id="T314" time="277.046" type="appl" />
         <tli id="T315" time="277.219" type="appl" />
         <tli id="T316" time="277.84433333333334" type="appl" />
         <tli id="T317" time="278.4696666666667" type="appl" />
         <tli id="T318" time="279.095" type="appl" />
         <tli id="T0" time="279.754" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="YIF"
                      type="t">
         <timeline-fork end="T47" start="T46">
            <tli id="T46.tx.1" />
         </timeline-fork>
         <timeline-fork end="T65" start="T64">
            <tli id="T64.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T6" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <nts id="Seg_3" n="HIAT:ip">(</nts>
                  <ts e="T319" id="Seg_5" n="HIAT:w" s="T1">Šidə</ts>
                  <nts id="Seg_6" n="HIAT:ip">)</nts>
                  <nts id="Seg_7" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_9" n="HIAT:w" s="T319">tɨmnʼäsɨɣa</ts>
                  <nts id="Seg_10" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_11" n="HIAT:ip">(</nts>
                  <ts e="T320" id="Seg_13" n="HIAT:w" s="T2">eːkundaɣ</ts>
                  <nts id="Seg_14" n="HIAT:ip">)</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_17" n="HIAT:ip">(</nts>
                  <nts id="Seg_18" n="HIAT:ip">(</nts>
                  <ats e="T4" id="Seg_19" n="HIAT:non-pho" s="T320">BRK</ats>
                  <nts id="Seg_20" n="HIAT:ip">)</nts>
                  <nts id="Seg_21" n="HIAT:ip">)</nts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_24" n="HIAT:w" s="T4">wargɨkondaɣ</ts>
                  <nts id="Seg_25" n="HIAT:ip">.</nts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T11" id="Seg_27" n="sc" s="T7">
               <ts e="T11" id="Seg_29" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">Warge</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">temnʼadə</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">nädemble</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">wargɨkwa</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T16" id="Seg_43" n="sc" s="T12">
               <ts e="T16" id="Seg_45" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">Üčega</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">temnʼadə</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_53" n="HIAT:w" s="T14">qwerat</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_56" n="HIAT:w" s="T15">Qamaǯʼä</ts>
                  <nts id="Seg_57" n="HIAT:ip">.</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T21" id="Seg_59" n="sc" s="T17">
               <ts e="T21" id="Seg_61" n="HIAT:u" s="T17">
                  <ts e="T3" id="Seg_63" n="HIAT:w" s="T17">Qamaǯʼän</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_66" n="HIAT:w" s="T3">optɨ</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_69" n="HIAT:w" s="T18">qwajakwaɣə</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_72" n="HIAT:w" s="T19">čaŋgɨl</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_75" n="HIAT:w" s="T20">wattout</ts>
                  <nts id="Seg_76" n="HIAT:ip">.</nts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T29" id="Seg_78" n="sc" s="T22">
               <ts e="T29" id="Seg_80" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_82" n="HIAT:w" s="T22">A</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_85" n="HIAT:w" s="T23">tabənan</ts>
                  <nts id="Seg_86" n="HIAT:ip">,</nts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_89" n="HIAT:w" s="T24">wargə</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_92" n="HIAT:w" s="T25">temnʼandenan</ts>
                  <nts id="Seg_93" n="HIAT:ip">,</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_96" n="HIAT:w" s="T26">emamtə</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_99" n="HIAT:w" s="T27">qwerkwattə</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_102" n="HIAT:w" s="T28">anʼǯä</ts>
                  <nts id="Seg_103" n="HIAT:ip">.</nts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T41" id="Seg_105" n="sc" s="T30">
               <ts e="T41" id="Seg_107" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_109" n="HIAT:w" s="T30">Tʼeper</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_112" n="HIAT:w" s="T31">oqqer</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_115" n="HIAT:w" s="T32">godet</ts>
                  <nts id="Seg_116" n="HIAT:ip">,</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_119" n="HIAT:w" s="T33">qwennaɣə</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_122" n="HIAT:w" s="T34">čaŋgəl</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_125" n="HIAT:w" s="T35">wattoɣənde</ts>
                  <nts id="Seg_126" n="HIAT:ip">,</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_129" n="HIAT:w" s="T36">a</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_132" n="HIAT:w" s="T37">Qamaǯʼä</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_135" n="HIAT:w" s="T38">parkwa</ts>
                  <nts id="Seg_136" n="HIAT:ip">:</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_138" n="HIAT:ip">"</nts>
                  <ts e="T40" id="Seg_140" n="HIAT:w" s="T39">Wašešpa</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_143" n="HIAT:w" s="T40">aŋga</ts>
                  <nts id="Seg_144" n="HIAT:ip">!</nts>
                  <nts id="Seg_145" n="HIAT:ip">"</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T48" id="Seg_147" n="sc" s="T42">
               <ts e="T48" id="Seg_149" n="HIAT:u" s="T42">
                  <ts e="T5" id="Seg_151" n="HIAT:w" s="T42">A</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_154" n="HIAT:w" s="T5">Qamaǯä</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_157" n="HIAT:w" s="T43">parkwa</ts>
                  <nts id="Seg_158" n="HIAT:ip">:</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_160" n="HIAT:ip">"</nts>
                  <ts e="T45" id="Seg_162" n="HIAT:w" s="T44">Aŋgəː</ts>
                  <nts id="Seg_163" n="HIAT:ip">,</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_166" n="HIAT:w" s="T45">meka</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46.tx.1" id="Seg_169" n="HIAT:w" s="T46">qoqal</ts>
                  <nts id="Seg_170" n="HIAT:ip">_</nts>
                  <ts e="T47" id="Seg_172" n="HIAT:w" s="T46.tx.1">qɨmbal</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_175" n="HIAT:w" s="T47">kwəǯeš</ts>
                  <nts id="Seg_176" n="HIAT:ip">!</nts>
                  <nts id="Seg_177" n="HIAT:ip">"</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T56" id="Seg_179" n="sc" s="T115">
               <ts e="T56" id="Seg_181" n="HIAT:u" s="T115">
                  <ts e="T49" id="Seg_183" n="HIAT:w" s="T115">A</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_186" n="HIAT:w" s="T49">wargə</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_189" n="HIAT:w" s="T50">temnʼadə</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_192" n="HIAT:w" s="T51">Qamaǯʼän</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_195" n="HIAT:w" s="T52">oɣənʼǯʼä</ts>
                  <nts id="Seg_196" n="HIAT:ip">:</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_198" n="HIAT:ip">"</nts>
                  <ts e="T54" id="Seg_200" n="HIAT:w" s="T53">Qajko</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_203" n="HIAT:w" s="T54">nɨlʼǯik</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_206" n="HIAT:w" s="T55">parkwand</ts>
                  <nts id="Seg_207" n="HIAT:ip">?</nts>
                  <nts id="Seg_208" n="HIAT:ip">"</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T66" id="Seg_210" n="sc" s="T57">
               <ts e="T66" id="Seg_212" n="HIAT:u" s="T57">
                  <nts id="Seg_213" n="HIAT:ip">"</nts>
                  <ts e="T58" id="Seg_215" n="HIAT:w" s="T57">A</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_218" n="HIAT:w" s="T58">mat</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_221" n="HIAT:w" s="T59">noɣ</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_224" n="HIAT:w" s="T60">nɨlʼǯik</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_227" n="HIAT:w" s="T61">parkwak</ts>
                  <nts id="Seg_228" n="HIAT:ip">,</nts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_231" n="HIAT:w" s="T62">maʒek</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_234" n="HIAT:w" s="T63">anʼǯäm</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_236" n="HIAT:ip">(</nts>
                  <ts e="T64" id="Seg_238" n="HIAT:w" s="T326">qɨmb-</ts>
                  <nts id="Seg_239" n="HIAT:ip">)</nts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64.tx.1" id="Seg_242" n="HIAT:w" s="T64">qɨmbahe</ts>
                  <nts id="Seg_243" n="HIAT:ip">_</nts>
                  <ts e="T65" id="Seg_245" n="HIAT:w" s="T64.tx.1">qoqahe</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_248" n="HIAT:w" s="T65">abədekkwa</ts>
                  <nts id="Seg_249" n="HIAT:ip">"</nts>
                  <nts id="Seg_250" n="HIAT:ip">.</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T71" id="Seg_252" n="sc" s="T67">
               <ts e="T71" id="Seg_254" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_256" n="HIAT:w" s="T67">Načil</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_259" n="HIAT:w" s="T68">qwajahaɣ</ts>
                  <nts id="Seg_260" n="HIAT:ip">,</nts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_263" n="HIAT:w" s="T69">čwesse</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_266" n="HIAT:w" s="T70">töːlaɣ</ts>
                  <nts id="Seg_267" n="HIAT:ip">.</nts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T81" id="Seg_269" n="sc" s="T72">
               <ts e="T81" id="Seg_271" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_273" n="HIAT:w" s="T72">Teperʼ</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_276" n="HIAT:w" s="T73">wargə</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_279" n="HIAT:w" s="T74">temnʼat</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_281" n="HIAT:ip">(</nts>
                  <nts id="Seg_282" n="HIAT:ip">(</nts>
                  <ats e="T75" id="Seg_283" n="HIAT:non-pho" s="T78">LAUGH</ats>
                  <nts id="Seg_284" n="HIAT:ip">)</nts>
                  <nts id="Seg_285" n="HIAT:ip">)</nts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_287" n="HIAT:ip">(</nts>
                  <ts e="T76" id="Seg_289" n="HIAT:w" s="T75">oralləlde</ts>
                  <nts id="Seg_290" n="HIAT:ip">)</nts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_293" n="HIAT:w" s="T76">imamdə</ts>
                  <nts id="Seg_294" n="HIAT:ip">,</nts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_296" n="HIAT:ip">(</nts>
                  <nts id="Seg_297" n="HIAT:ip">(</nts>
                  <ats e="T77" id="Seg_298" n="HIAT:non-pho" s="T327">…</ats>
                  <nts id="Seg_299" n="HIAT:ip">)</nts>
                  <nts id="Seg_300" n="HIAT:ip">)</nts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_303" n="HIAT:w" s="T77">nɨka</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_306" n="HIAT:w" s="T79">oralʼlʼe</ts>
                  <nts id="Seg_307" n="HIAT:ip">,</nts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_309" n="HIAT:ip">(</nts>
                  <ts e="T81" id="Seg_311" n="HIAT:w" s="T80">qwalləlde</ts>
                  <nts id="Seg_312" n="HIAT:ip">)</nts>
                  <nts id="Seg_313" n="HIAT:ip">.</nts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T91" id="Seg_315" n="sc" s="T82">
               <ts e="T91" id="Seg_317" n="HIAT:u" s="T82">
                  <nts id="Seg_318" n="HIAT:ip">(</nts>
                  <ts e="T328" id="Seg_320" n="HIAT:w" s="T82">Teperʼ=</ts>
                  <nts id="Seg_321" n="HIAT:ip">)</nts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_324" n="HIAT:w" s="T328">a</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_327" n="HIAT:w" s="T83">talʼǯel</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_330" n="HIAT:w" s="T84">onǯ</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_333" n="HIAT:w" s="T85">pʼalgalk</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_336" n="HIAT:w" s="T86">qwella</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_339" n="HIAT:w" s="T87">wargə</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_342" n="HIAT:w" s="T88">temnʼad</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_345" n="HIAT:w" s="T89">čangɨl</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_348" n="HIAT:w" s="T90">wattoɣend</ts>
                  <nts id="Seg_349" n="HIAT:ip">.</nts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T97" id="Seg_351" n="sc" s="T92">
               <ts e="T97" id="Seg_353" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_355" n="HIAT:w" s="T92">A</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_358" n="HIAT:w" s="T93">anʼǯäd</ts>
                  <nts id="Seg_359" n="HIAT:ip">,</nts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_362" n="HIAT:w" s="T94">oralle</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_365" n="HIAT:w" s="T95">Qamaǯäp</ts>
                  <nts id="Seg_366" n="HIAT:ip">,</nts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_369" n="HIAT:w" s="T96">qwallelde</ts>
                  <nts id="Seg_370" n="HIAT:ip">.</nts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T103" id="Seg_372" n="sc" s="T98">
               <ts e="T103" id="Seg_374" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_376" n="HIAT:w" s="T98">Qamaǯä</ts>
                  <nts id="Seg_377" n="HIAT:ip">,</nts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_380" n="HIAT:w" s="T99">manmut</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_383" n="HIAT:w" s="T100">pon</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_386" n="HIAT:w" s="T101">čanǯle</ts>
                  <nts id="Seg_387" n="HIAT:ip">,</nts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_390" n="HIAT:w" s="T102">qwella</ts>
                  <nts id="Seg_391" n="HIAT:ip">.</nts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T108" id="Seg_393" n="sc" s="T104">
               <ts e="T108" id="Seg_395" n="HIAT:u" s="T104">
                  <ts e="T105" id="Seg_397" n="HIAT:w" s="T104">Qwella</ts>
                  <nts id="Seg_398" n="HIAT:ip">,</nts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_401" n="HIAT:w" s="T105">ku</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_404" n="HIAT:w" s="T106">xajd</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_407" n="HIAT:w" s="T107">ada</ts>
                  <nts id="Seg_408" n="HIAT:ip">.</nts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T113" id="Seg_410" n="sc" s="T109">
               <ts e="T113" id="Seg_412" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_414" n="HIAT:w" s="T109">Čaǯa</ts>
                  <nts id="Seg_415" n="HIAT:ip">,</nts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_418" n="HIAT:w" s="T110">konǯernɨtə</ts>
                  <nts id="Seg_419" n="HIAT:ip">:</nts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_422" n="HIAT:w" s="T111">kule</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_425" n="HIAT:w" s="T112">wašešpɨnda</ts>
                  <nts id="Seg_426" n="HIAT:ip">.</nts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T118" id="Seg_428" n="sc" s="T114">
               <ts e="T118" id="Seg_430" n="HIAT:u" s="T114">
                  <ts e="T116" id="Seg_432" n="HIAT:w" s="T114">akəndə</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_435" n="HIAT:w" s="T116">tadrɨtə</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_438" n="HIAT:w" s="T117">qwel</ts>
                  <nts id="Seg_439" n="HIAT:ip">.</nts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T127" id="Seg_441" n="sc" s="T119">
               <ts e="T127" id="Seg_443" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_445" n="HIAT:w" s="T119">Tab</ts>
                  <nts id="Seg_446" n="HIAT:ip">,</nts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_449" n="HIAT:w" s="T120">Qamaǯä</ts>
                  <nts id="Seg_450" n="HIAT:ip">,</nts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_453" n="HIAT:w" s="T121">na</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_456" n="HIAT:w" s="T122">kulel</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_459" n="HIAT:w" s="T123">qajmut</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_462" n="HIAT:w" s="T124">töšpa</ts>
                  <nts id="Seg_463" n="HIAT:ip">,</nts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_466" n="HIAT:w" s="T125">načʼidə</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_469" n="HIAT:w" s="T126">qwella</ts>
                  <nts id="Seg_470" n="HIAT:ip">.</nts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T131" id="Seg_472" n="sc" s="T128">
               <ts e="T131" id="Seg_474" n="HIAT:u" s="T128">
                  <ts e="T129" id="Seg_476" n="HIAT:w" s="T128">Čaǯla</ts>
                  <nts id="Seg_477" n="HIAT:ip">,</nts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_480" n="HIAT:w" s="T129">medəla</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_483" n="HIAT:w" s="T130">kɨge</ts>
                  <nts id="Seg_484" n="HIAT:ip">.</nts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T137" id="Seg_486" n="sc" s="T132">
               <ts e="T137" id="Seg_488" n="HIAT:u" s="T132">
                  <ts e="T133" id="Seg_490" n="HIAT:w" s="T132">Na</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_493" n="HIAT:w" s="T133">kɨgeɣen</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_496" n="HIAT:w" s="T134">nɨlʼǯik</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_499" n="HIAT:w" s="T135">qwəl</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_502" n="HIAT:w" s="T136">kočʼeja</ts>
                  <nts id="Seg_503" n="HIAT:ip">.</nts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T321" id="Seg_505" n="sc" s="T324">
               <ts e="T321" id="Seg_507" n="HIAT:u" s="T324">
                  <ts e="T329" id="Seg_509" n="HIAT:w" s="T324">Tabə</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_511" n="HIAT:ip">(</nts>
                  <nts id="Seg_512" n="HIAT:ip">(</nts>
                  <ats e="T323" id="Seg_513" n="HIAT:non-pho" s="T329">PAUSE</ats>
                  <nts id="Seg_514" n="HIAT:ip">)</nts>
                  <nts id="Seg_515" n="HIAT:ip">)</nts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_518" n="HIAT:w" s="T323">pačalelʼčʼəlde</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_520" n="HIAT:ip">(</nts>
                  <nts id="Seg_521" n="HIAT:ip">(</nts>
                  <ats e="T322" id="Seg_522" n="HIAT:non-pho" s="T330">PAUSE</ats>
                  <nts id="Seg_523" n="HIAT:ip">)</nts>
                  <nts id="Seg_524" n="HIAT:ip">)</nts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_526" n="HIAT:ip">(</nts>
                  <ts e="T325" id="Seg_528" n="HIAT:w" s="T322">mop-</ts>
                  <nts id="Seg_529" n="HIAT:ip">)</nts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_531" n="HIAT:ip">(</nts>
                  <nts id="Seg_532" n="HIAT:ip">(</nts>
                  <ats e="T321" id="Seg_533" n="HIAT:non-pho" s="T325">BRK</ats>
                  <nts id="Seg_534" n="HIAT:ip">)</nts>
                  <nts id="Seg_535" n="HIAT:ip">)</nts>
                  <nts id="Seg_536" n="HIAT:ip">…</nts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T144" id="Seg_538" n="sc" s="T138">
               <ts e="T144" id="Seg_540" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_542" n="HIAT:w" s="T138">Tab</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_545" n="HIAT:w" s="T139">pačalelʼčʼəlde</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_548" n="HIAT:w" s="T140">mogep</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_551" n="HIAT:w" s="T141">i</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_554" n="HIAT:w" s="T142">kujam</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_557" n="HIAT:w" s="T143">melʼčʼəlde</ts>
                  <nts id="Seg_558" n="HIAT:ip">.</nts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T148" id="Seg_560" n="sc" s="T145">
               <ts e="T148" id="Seg_562" n="HIAT:u" s="T145">
                  <ts e="T146" id="Seg_564" n="HIAT:w" s="T145">Dawaj</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_567" n="HIAT:w" s="T146">qweləm</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_570" n="HIAT:w" s="T147">oɣolešpɨgu</ts>
                  <nts id="Seg_571" n="HIAT:ip">.</nts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T153" id="Seg_573" n="sc" s="T149">
               <ts e="T153" id="Seg_575" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_577" n="HIAT:w" s="T149">Oɣolde</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_580" n="HIAT:w" s="T150">qwelp</ts>
                  <nts id="Seg_581" n="HIAT:ip">,</nts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_584" n="HIAT:w" s="T151">tüp</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_587" n="HIAT:w" s="T152">čadəla</ts>
                  <nts id="Seg_588" n="HIAT:ip">.</nts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T158" id="Seg_590" n="sc" s="T154">
               <ts e="T158" id="Seg_592" n="HIAT:u" s="T154">
                  <ts e="T155" id="Seg_594" n="HIAT:w" s="T154">Kare</ts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_597" n="HIAT:w" s="T155">qwelp</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_599" n="HIAT:ip">(</nts>
                  <ts e="T157" id="Seg_601" n="HIAT:w" s="T156">čʼabolʼ</ts>
                  <nts id="Seg_602" n="HIAT:ip">)</nts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_605" n="HIAT:w" s="T157">maškelʼǯəqəlte</ts>
                  <nts id="Seg_606" n="HIAT:ip">.</nts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T163" id="Seg_608" n="sc" s="T159">
               <ts e="T163" id="Seg_610" n="HIAT:u" s="T159">
                  <ts e="T160" id="Seg_612" n="HIAT:w" s="T159">Trug</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_615" n="HIAT:w" s="T160">mančeǯʼla</ts>
                  <nts id="Seg_616" n="HIAT:ip">:</nts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_619" n="HIAT:w" s="T161">urop</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_622" n="HIAT:w" s="T162">töšpɨnd</ts>
                  <nts id="Seg_623" n="HIAT:ip">.</nts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T170" id="Seg_625" n="sc" s="T164">
               <ts e="T170" id="Seg_627" n="HIAT:u" s="T164">
                  <ts e="T165" id="Seg_629" n="HIAT:w" s="T164">Urop</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_632" n="HIAT:w" s="T165">töla</ts>
                  <nts id="Seg_633" n="HIAT:ip">,</nts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_636" n="HIAT:w" s="T166">oɣənʼǯʼəla</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_639" n="HIAT:w" s="T167">Qamaǯän</ts>
                  <nts id="Seg_640" n="HIAT:ip">:</nts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_642" n="HIAT:ip">"</nts>
                  <ts e="T169" id="Seg_644" n="HIAT:w" s="T168">Qaj</ts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_647" n="HIAT:w" s="T169">mešpɨndaɣ</ts>
                  <nts id="Seg_648" n="HIAT:ip">?</nts>
                  <nts id="Seg_649" n="HIAT:ip">"</nts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T175" id="Seg_651" n="sc" s="T171">
               <ts e="T175" id="Seg_653" n="HIAT:u" s="T171">
                  <ts e="T172" id="Seg_655" n="HIAT:w" s="T171">Mat</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_658" n="HIAT:w" s="T172">makɨlmɨ</ts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_661" n="HIAT:w" s="T173">kare</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_664" n="HIAT:w" s="T174">perešpɨndak</ts>
                  <nts id="Seg_665" n="HIAT:ip">.</nts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T181" id="Seg_667" n="sc" s="T176">
               <ts e="T181" id="Seg_669" n="HIAT:u" s="T176">
                  <ts e="T331" id="Seg_671" n="HIAT:w" s="T176">Urop</ts>
                  <nts id="Seg_672" n="HIAT:ip">,</nts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_675" n="HIAT:w" s="T331">no</ts>
                  <nts id="Seg_676" n="HIAT:ip">,</nts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_679" n="HIAT:w" s="T177">taɣ</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_682" n="HIAT:w" s="T178">omdeš</ts>
                  <nts id="Seg_683" n="HIAT:ip">,</nts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_686" n="HIAT:w" s="T179">tü</ts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_689" n="HIAT:w" s="T180">tae</ts>
                  <nts id="Seg_690" n="HIAT:ip">.</nts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T186" id="Seg_692" n="sc" s="T182">
               <ts e="T186" id="Seg_694" n="HIAT:u" s="T182">
                  <ts e="T183" id="Seg_696" n="HIAT:w" s="T182">Urop</ts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_699" n="HIAT:w" s="T183">tü</ts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_702" n="HIAT:w" s="T184">tae</ts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_705" n="HIAT:w" s="T185">omdla</ts>
                  <nts id="Seg_706" n="HIAT:ip">.</nts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T191" id="Seg_708" n="sc" s="T187">
               <ts e="T191" id="Seg_710" n="HIAT:u" s="T187">
                  <nts id="Seg_711" n="HIAT:ip">"</nts>
                  <ts e="T188" id="Seg_713" n="HIAT:w" s="T187">Qamaǯä</ts>
                  <nts id="Seg_714" n="HIAT:ip">,</nts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_717" n="HIAT:w" s="T188">meka</ts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_720" n="HIAT:w" s="T189">naj</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_723" n="HIAT:w" s="T190">mele</ts>
                  <nts id="Seg_724" n="HIAT:ip">!</nts>
                  <nts id="Seg_725" n="HIAT:ip">"</nts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T200" id="Seg_727" n="sc" s="T192">
               <ts e="T200" id="Seg_729" n="HIAT:u" s="T192">
                  <ts e="T193" id="Seg_731" n="HIAT:w" s="T192">Tʼäperʼ</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_734" n="HIAT:w" s="T193">čabɨ</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_737" n="HIAT:w" s="T194">muʒla</ts>
                  <nts id="Seg_738" n="HIAT:ip">,</nts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_741" n="HIAT:w" s="T195">tab</ts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_744" n="HIAT:w" s="T196">oqqer</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_747" n="HIAT:w" s="T197">čab</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_750" n="HIAT:w" s="T198">uromn</ts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_753" n="HIAT:w" s="T199">čačɨlde</ts>
                  <nts id="Seg_754" n="HIAT:ip">.</nts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T333" id="Seg_756" n="sc" s="T201">
               <ts e="T333" id="Seg_758" n="HIAT:u" s="T201">
                  <ts e="T202" id="Seg_760" n="HIAT:w" s="T201">Urop</ts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_763" n="HIAT:w" s="T202">abəlde</ts>
                  <nts id="Seg_764" n="HIAT:ip">:</nts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_766" n="HIAT:ip">"</nts>
                  <ts e="T204" id="Seg_768" n="HIAT:w" s="T203">Ox</ts>
                  <nts id="Seg_769" n="HIAT:ip">,</nts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_772" n="HIAT:w" s="T204">swa</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_775" n="HIAT:w" s="T205">ma</ts>
                  <nts id="Seg_776" n="HIAT:ip">!</nts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T212" id="Seg_778" n="sc" s="T206">
               <ts e="T212" id="Seg_780" n="HIAT:u" s="T206">
                  <nts id="Seg_781" n="HIAT:ip">(</nts>
                  <ts e="T332" id="Seg_783" n="HIAT:w" s="T206">Mat=</ts>
                  <nts id="Seg_784" n="HIAT:ip">)</nts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_787" n="HIAT:w" s="T332">dawaj</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_790" n="HIAT:w" s="T207">mat</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_793" n="HIAT:w" s="T208">makɨl</ts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_796" n="HIAT:w" s="T209">toʒe</ts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_799" n="HIAT:w" s="T210">te</ts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_802" n="HIAT:w" s="T211">baqqɨlǯ</ts>
                  <nts id="Seg_803" n="HIAT:ip">!</nts>
                  <nts id="Seg_804" n="HIAT:ip">"</nts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T219" id="Seg_806" n="sc" s="T213">
               <ts e="T219" id="Seg_808" n="HIAT:u" s="T213">
                  <ts e="T214" id="Seg_810" n="HIAT:w" s="T213">Qamaǯä</ts>
                  <nts id="Seg_811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_813" n="HIAT:w" s="T214">eǯalgwa</ts>
                  <nts id="Seg_814" n="HIAT:ip">:</nts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_816" n="HIAT:ip">"</nts>
                  <ts e="T216" id="Seg_818" n="HIAT:w" s="T215">Tat</ts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_821" n="HIAT:w" s="T216">maʒek</ts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_824" n="HIAT:w" s="T217">aː</ts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_827" n="HIAT:w" s="T218">ablende</ts>
                  <nts id="Seg_828" n="HIAT:ip">?</nts>
                  <nts id="Seg_829" n="HIAT:ip">"</nts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T222" id="Seg_831" n="sc" s="T220">
               <ts e="T222" id="Seg_833" n="HIAT:u" s="T220">
                  <nts id="Seg_834" n="HIAT:ip">"</nts>
                  <ts e="T221" id="Seg_836" n="HIAT:w" s="T220">Aː</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_839" n="HIAT:w" s="T221">ablage</ts>
                  <nts id="Seg_840" n="HIAT:ip">!</nts>
                  <nts id="Seg_841" n="HIAT:ip">"</nts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T227" id="Seg_843" n="sc" s="T223">
               <ts e="T227" id="Seg_845" n="HIAT:u" s="T223">
                  <ts e="T224" id="Seg_847" n="HIAT:w" s="T223">Tʼäperʼ</ts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_850" n="HIAT:w" s="T224">Qamaǯä</ts>
                  <nts id="Seg_851" n="HIAT:ip">:</nts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_853" n="HIAT:ip">"</nts>
                  <ts e="T225" id="Seg_855" n="HIAT:w" s="T334">No</ts>
                  <nts id="Seg_856" n="HIAT:ip">,</nts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_859" n="HIAT:w" s="T225">qote</ts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_862" n="HIAT:w" s="T226">qondeš</ts>
                  <nts id="Seg_863" n="HIAT:ip">!</nts>
                  <nts id="Seg_864" n="HIAT:ip">"</nts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T232" id="Seg_866" n="sc" s="T228">
               <ts e="T232" id="Seg_868" n="HIAT:u" s="T228">
                  <ts e="T229" id="Seg_870" n="HIAT:w" s="T228">Tabə</ts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_873" n="HIAT:w" s="T229">urop</ts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_876" n="HIAT:w" s="T230">qote</ts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_879" n="HIAT:w" s="T231">qondəla</ts>
                  <nts id="Seg_880" n="HIAT:ip">.</nts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T243" id="Seg_882" n="sc" s="T233">
               <ts e="T243" id="Seg_884" n="HIAT:u" s="T233">
                  <ts e="T234" id="Seg_886" n="HIAT:w" s="T233">Qamaǯä</ts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_889" n="HIAT:w" s="T234">paɣɨp</ts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_892" n="HIAT:w" s="T235">aŋdɨ</ts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_895" n="HIAT:w" s="T236">aləlde</ts>
                  <nts id="Seg_896" n="HIAT:ip">,</nts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_899" n="HIAT:w" s="T335">tʼaper</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_902" n="HIAT:w" s="T237">urot</ts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_905" n="HIAT:w" s="T238">pärgap</ts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_908" n="HIAT:w" s="T239">korreʒʼelde</ts>
                  <nts id="Seg_909" n="HIAT:ip">,</nts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_912" n="HIAT:w" s="T240">urop</ts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_915" n="HIAT:w" s="T241">nɨka</ts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_918" n="HIAT:w" s="T242">qula</ts>
                  <nts id="Seg_919" n="HIAT:ip">.</nts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T250" id="Seg_921" n="sc" s="T244">
               <ts e="T250" id="Seg_923" n="HIAT:u" s="T244">
                  <ts e="T336" id="Seg_925" n="HIAT:w" s="T244">Qamaǯä</ts>
                  <nts id="Seg_926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_928" n="HIAT:w" s="T336">täper</ts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_931" n="HIAT:w" s="T245">kɨrɨlde</ts>
                  <nts id="Seg_932" n="HIAT:ip">,</nts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_935" n="HIAT:w" s="T246">waǯemdə</ts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_938" n="HIAT:w" s="T247">wesʼ</ts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_941" n="HIAT:w" s="T248">te</ts>
                  <nts id="Seg_942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_944" n="HIAT:w" s="T249">ubirailde</ts>
                  <nts id="Seg_945" n="HIAT:ip">.</nts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T266" id="Seg_947" n="sc" s="T251">
               <ts e="T266" id="Seg_949" n="HIAT:u" s="T251">
                  <ts e="T252" id="Seg_951" n="HIAT:w" s="T251">Potom</ts>
                  <nts id="Seg_952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_954" n="HIAT:w" s="T252">wes</ts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_957" n="HIAT:w" s="T253">taptɨl</ts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_960" n="HIAT:w" s="T254">čʼel</ts>
                  <nts id="Seg_961" n="HIAT:ip">,</nts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_964" n="HIAT:w" s="T255">ile</ts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_967" n="HIAT:w" s="T256">wesʼ</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_969" n="HIAT:ip">(</nts>
                  <ts e="T337" id="Seg_971" n="HIAT:w" s="T257">katomkaɣ-</ts>
                  <nts id="Seg_972" n="HIAT:ip">)</nts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_975" n="HIAT:w" s="T337">katomkal</ts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_978" n="HIAT:w" s="T258">koǯaɣɨnd</ts>
                  <nts id="Seg_979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_981" n="HIAT:w" s="T259">pallde</ts>
                  <nts id="Seg_982" n="HIAT:ip">,</nts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_985" n="HIAT:w" s="T260">tirel</ts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_988" n="HIAT:w" s="T261">koǯagɨnd</ts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_991" n="HIAT:w" s="T262">pallde</ts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_994" n="HIAT:w" s="T263">i</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_997" n="HIAT:w" s="T264">ugulʼǯe</ts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1000" n="HIAT:w" s="T265">üpalʼčəla</ts>
                  <nts id="Seg_1001" n="HIAT:ip">.</nts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T271" id="Seg_1003" n="sc" s="T267">
               <ts e="T271" id="Seg_1005" n="HIAT:u" s="T267">
                  <ts e="T268" id="Seg_1007" n="HIAT:w" s="T267">Ugulʼǯe</ts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1010" n="HIAT:w" s="T268">medešpa</ts>
                  <nts id="Seg_1011" n="HIAT:ip">,</nts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1014" n="HIAT:w" s="T269">kaško</ts>
                  <nts id="Seg_1015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1017" n="HIAT:w" s="T270">čaŋgwa</ts>
                  <nts id="Seg_1018" n="HIAT:ip">.</nts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T280" id="Seg_1020" n="sc" s="T272">
               <ts e="T280" id="Seg_1022" n="HIAT:u" s="T272">
                  <ts e="T273" id="Seg_1024" n="HIAT:w" s="T272">Maːtə</ts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1026" n="HIAT:ip">(</nts>
                  <ts e="T274" id="Seg_1028" n="HIAT:w" s="T273">par</ts>
                  <nts id="Seg_1029" n="HIAT:ip">)</nts>
                  <nts id="Seg_1030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1032" n="HIAT:w" s="T274">enne</ts>
                  <nts id="Seg_1033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1035" n="HIAT:w" s="T275">čanǯla</ts>
                  <nts id="Seg_1036" n="HIAT:ip">,</nts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1039" n="HIAT:w" s="T276">šölʼ</ts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1041" n="HIAT:ip">(</nts>
                  <ts e="T278" id="Seg_1043" n="HIAT:w" s="T277">par</ts>
                  <nts id="Seg_1044" n="HIAT:ip">)</nts>
                  <nts id="Seg_1045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1047" n="HIAT:w" s="T278">elle</ts>
                  <nts id="Seg_1048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1050" n="HIAT:w" s="T279">mannemba</ts>
                  <nts id="Seg_1051" n="HIAT:ip">.</nts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T288" id="Seg_1053" n="sc" s="T281">
               <ts e="T288" id="Seg_1055" n="HIAT:u" s="T281">
                  <ts e="T282" id="Seg_1057" n="HIAT:w" s="T281">Tebnʼadə</ts>
                  <nts id="Seg_1058" n="HIAT:ip">,</nts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1061" n="HIAT:w" s="T282">anʼǯädə</ts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1064" n="HIAT:w" s="T283">oqqer</ts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1067" n="HIAT:w" s="T284">tüt</ts>
                  <nts id="Seg_1068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1070" n="HIAT:w" s="T285">xajp</ts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1073" n="HIAT:w" s="T286">šədu</ts>
                  <nts id="Seg_1074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1076" n="HIAT:w" s="T287">tarešpat</ts>
                  <nts id="Seg_1077" n="HIAT:ip">.</nts>
                  <nts id="Seg_1078" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T339" id="Seg_1079" n="sc" s="T289">
               <ts e="T339" id="Seg_1081" n="HIAT:u" s="T289">
                  <nts id="Seg_1082" n="HIAT:ip">"</nts>
                  <ts e="T290" id="Seg_1084" n="HIAT:w" s="T289">Taw</ts>
                  <nts id="Seg_1085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1087" n="HIAT:w" s="T290">mat</ts>
                  <nts id="Seg_1088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1090" n="HIAT:w" s="T291">Qamaǯäm</ts>
                  <nts id="Seg_1091" n="HIAT:ip">.</nts>
                  <nts id="Seg_1092" n="HIAT:ip">"</nts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T292" id="Seg_1094" n="sc" s="T338">
               <ts e="T292" id="Seg_1096" n="HIAT:u" s="T338">
                  <nts id="Seg_1097" n="HIAT:ip">(</nts>
                  <nts id="Seg_1098" n="HIAT:ip">(</nts>
                  <ats e="T292" id="Seg_1099" n="HIAT:non-pho" s="T338">…</ats>
                  <nts id="Seg_1100" n="HIAT:ip">)</nts>
                  <nts id="Seg_1101" n="HIAT:ip">)</nts>
                  <nts id="Seg_1102" n="HIAT:ip">.</nts>
                  <nts id="Seg_1103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T298" id="Seg_1104" n="sc" s="T293">
               <ts e="T298" id="Seg_1106" n="HIAT:u" s="T293">
                  <ts e="T294" id="Seg_1108" n="HIAT:w" s="T293">Qamaǯä</ts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1111" n="HIAT:w" s="T294">načʼat</ts>
                  <nts id="Seg_1112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1114" n="HIAT:w" s="T295">parǯla</ts>
                  <nts id="Seg_1115" n="HIAT:ip">:</nts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1117" n="HIAT:ip">"</nts>
                  <ts e="T297" id="Seg_1119" n="HIAT:w" s="T296">Mat</ts>
                  <nts id="Seg_1120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1122" n="HIAT:w" s="T297">tawejak</ts>
                  <nts id="Seg_1123" n="HIAT:ip">!</nts>
                  <nts id="Seg_1124" n="HIAT:ip">"</nts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T305" id="Seg_1126" n="sc" s="T299">
               <ts e="T305" id="Seg_1128" n="HIAT:u" s="T299">
                  <ts e="T300" id="Seg_1130" n="HIAT:w" s="T299">Qamaǯä</ts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1133" n="HIAT:w" s="T300">tabeštjaɣen</ts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1136" n="HIAT:w" s="T301">qwəl</ts>
                  <nts id="Seg_1137" n="HIAT:ip">,</nts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1140" n="HIAT:w" s="T302">waǯʼep</ts>
                  <nts id="Seg_1141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1143" n="HIAT:w" s="T303">elle</ts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1146" n="HIAT:w" s="T304">tülʼǯla</ts>
                  <nts id="Seg_1147" n="HIAT:ip">.</nts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T309" id="Seg_1149" n="sc" s="T306">
               <ts e="T309" id="Seg_1151" n="HIAT:u" s="T306">
                  <ts e="T307" id="Seg_1153" n="HIAT:w" s="T306">Abətəlde</ts>
                  <nts id="Seg_1154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1156" n="HIAT:w" s="T307">qwəlhe</ts>
                  <nts id="Seg_1157" n="HIAT:ip">,</nts>
                  <nts id="Seg_1158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1160" n="HIAT:w" s="T308">waǯʼəhe</ts>
                  <nts id="Seg_1161" n="HIAT:ip">.</nts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T314" id="Seg_1163" n="sc" s="T310">
               <ts e="T314" id="Seg_1165" n="HIAT:u" s="T310">
                  <ts e="T311" id="Seg_1167" n="HIAT:w" s="T310">A</ts>
                  <nts id="Seg_1168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1170" n="HIAT:w" s="T311">potom</ts>
                  <nts id="Seg_1171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1173" n="HIAT:w" s="T312">erelkwan</ts>
                  <nts id="Seg_1174" n="HIAT:ip">,</nts>
                  <nts id="Seg_1175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1177" n="HIAT:w" s="T313">wargelkwat</ts>
                  <nts id="Seg_1178" n="HIAT:ip">.</nts>
                  <nts id="Seg_1179" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T318" id="Seg_1180" n="sc" s="T315">
               <ts e="T318" id="Seg_1182" n="HIAT:u" s="T315">
                  <ts e="T316" id="Seg_1184" n="HIAT:w" s="T315">Nem</ts>
                  <nts id="Seg_1185" n="HIAT:ip">,</nts>
                  <nts id="Seg_1186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1188" n="HIAT:w" s="T316">ip</ts>
                  <nts id="Seg_1189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1191" n="HIAT:w" s="T317">qondkwat</ts>
                  <nts id="Seg_1192" n="HIAT:ip">.</nts>
                  <nts id="Seg_1193" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T6" id="Seg_1194" n="sc" s="T1">
               <ts e="T319" id="Seg_1196" n="e" s="T1">(Šidə) </ts>
               <ts e="T2" id="Seg_1198" n="e" s="T319">tɨmnʼäsɨɣa </ts>
               <ts e="T320" id="Seg_1200" n="e" s="T2">(eːkundaɣ) </ts>
               <ts e="T4" id="Seg_1202" n="e" s="T320"> ((BRK)) </ts>
               <ts e="T6" id="Seg_1204" n="e" s="T4">wargɨkondaɣ. </ts>
            </ts>
            <ts e="T11" id="Seg_1205" n="sc" s="T7">
               <ts e="T8" id="Seg_1207" n="e" s="T7">Warge </ts>
               <ts e="T9" id="Seg_1209" n="e" s="T8">temnʼadə </ts>
               <ts e="T10" id="Seg_1211" n="e" s="T9">nädemble </ts>
               <ts e="T11" id="Seg_1213" n="e" s="T10">wargɨkwa. </ts>
            </ts>
            <ts e="T16" id="Seg_1214" n="sc" s="T12">
               <ts e="T13" id="Seg_1216" n="e" s="T12">Üčega </ts>
               <ts e="T14" id="Seg_1218" n="e" s="T13">temnʼadə </ts>
               <ts e="T15" id="Seg_1220" n="e" s="T14">qwerat </ts>
               <ts e="T16" id="Seg_1222" n="e" s="T15">Qamaǯʼä. </ts>
            </ts>
            <ts e="T21" id="Seg_1223" n="sc" s="T17">
               <ts e="T3" id="Seg_1225" n="e" s="T17">Qamaǯʼän </ts>
               <ts e="T18" id="Seg_1227" n="e" s="T3">optɨ </ts>
               <ts e="T19" id="Seg_1229" n="e" s="T18">qwajakwaɣə </ts>
               <ts e="T20" id="Seg_1231" n="e" s="T19">čaŋgɨl </ts>
               <ts e="T21" id="Seg_1233" n="e" s="T20">wattout. </ts>
            </ts>
            <ts e="T29" id="Seg_1234" n="sc" s="T22">
               <ts e="T23" id="Seg_1236" n="e" s="T22">A </ts>
               <ts e="T24" id="Seg_1238" n="e" s="T23">tabənan, </ts>
               <ts e="T25" id="Seg_1240" n="e" s="T24">wargə </ts>
               <ts e="T26" id="Seg_1242" n="e" s="T25">temnʼandenan, </ts>
               <ts e="T27" id="Seg_1244" n="e" s="T26">emamtə </ts>
               <ts e="T28" id="Seg_1246" n="e" s="T27">qwerkwattə </ts>
               <ts e="T29" id="Seg_1248" n="e" s="T28">anʼǯä. </ts>
            </ts>
            <ts e="T41" id="Seg_1249" n="sc" s="T30">
               <ts e="T31" id="Seg_1251" n="e" s="T30">Tʼeper </ts>
               <ts e="T32" id="Seg_1253" n="e" s="T31">oqqer </ts>
               <ts e="T33" id="Seg_1255" n="e" s="T32">godet, </ts>
               <ts e="T34" id="Seg_1257" n="e" s="T33">qwennaɣə </ts>
               <ts e="T35" id="Seg_1259" n="e" s="T34">čaŋgəl </ts>
               <ts e="T36" id="Seg_1261" n="e" s="T35">wattoɣənde, </ts>
               <ts e="T37" id="Seg_1263" n="e" s="T36">a </ts>
               <ts e="T38" id="Seg_1265" n="e" s="T37">Qamaǯʼä </ts>
               <ts e="T39" id="Seg_1267" n="e" s="T38">parkwa: </ts>
               <ts e="T40" id="Seg_1269" n="e" s="T39">"Wašešpa </ts>
               <ts e="T41" id="Seg_1271" n="e" s="T40">aŋga!" </ts>
            </ts>
            <ts e="T48" id="Seg_1272" n="sc" s="T42">
               <ts e="T5" id="Seg_1274" n="e" s="T42">A </ts>
               <ts e="T43" id="Seg_1276" n="e" s="T5">Qamaǯä </ts>
               <ts e="T44" id="Seg_1278" n="e" s="T43">parkwa: </ts>
               <ts e="T45" id="Seg_1280" n="e" s="T44">"Aŋgəː, </ts>
               <ts e="T46" id="Seg_1282" n="e" s="T45">meka </ts>
               <ts e="T47" id="Seg_1284" n="e" s="T46">qoqal_qɨmbal </ts>
               <ts e="T48" id="Seg_1286" n="e" s="T47">kwəǯeš!" </ts>
            </ts>
            <ts e="T56" id="Seg_1287" n="sc" s="T115">
               <ts e="T49" id="Seg_1289" n="e" s="T115">A </ts>
               <ts e="T50" id="Seg_1291" n="e" s="T49">wargə </ts>
               <ts e="T51" id="Seg_1293" n="e" s="T50">temnʼadə </ts>
               <ts e="T52" id="Seg_1295" n="e" s="T51">Qamaǯʼän </ts>
               <ts e="T53" id="Seg_1297" n="e" s="T52">oɣənʼǯʼä: </ts>
               <ts e="T54" id="Seg_1299" n="e" s="T53">"Qajko </ts>
               <ts e="T55" id="Seg_1301" n="e" s="T54">nɨlʼǯik </ts>
               <ts e="T56" id="Seg_1303" n="e" s="T55">parkwand?" </ts>
            </ts>
            <ts e="T66" id="Seg_1304" n="sc" s="T57">
               <ts e="T58" id="Seg_1306" n="e" s="T57">"A </ts>
               <ts e="T59" id="Seg_1308" n="e" s="T58">mat </ts>
               <ts e="T60" id="Seg_1310" n="e" s="T59">noɣ </ts>
               <ts e="T61" id="Seg_1312" n="e" s="T60">nɨlʼǯik </ts>
               <ts e="T62" id="Seg_1314" n="e" s="T61">parkwak, </ts>
               <ts e="T63" id="Seg_1316" n="e" s="T62">maʒek </ts>
               <ts e="T326" id="Seg_1318" n="e" s="T63">anʼǯäm </ts>
               <ts e="T64" id="Seg_1320" n="e" s="T326">(qɨmb-) </ts>
               <ts e="T65" id="Seg_1322" n="e" s="T64">qɨmbahe_qoqahe </ts>
               <ts e="T66" id="Seg_1324" n="e" s="T65">abədekkwa". </ts>
            </ts>
            <ts e="T71" id="Seg_1325" n="sc" s="T67">
               <ts e="T68" id="Seg_1327" n="e" s="T67">Načil </ts>
               <ts e="T69" id="Seg_1329" n="e" s="T68">qwajahaɣ, </ts>
               <ts e="T70" id="Seg_1331" n="e" s="T69">čwesse </ts>
               <ts e="T71" id="Seg_1333" n="e" s="T70">töːlaɣ. </ts>
            </ts>
            <ts e="T81" id="Seg_1334" n="sc" s="T72">
               <ts e="T73" id="Seg_1336" n="e" s="T72">Teperʼ </ts>
               <ts e="T74" id="Seg_1338" n="e" s="T73">wargə </ts>
               <ts e="T78" id="Seg_1340" n="e" s="T74">temnʼat </ts>
               <ts e="T75" id="Seg_1342" n="e" s="T78">((LAUGH)) </ts>
               <ts e="T76" id="Seg_1344" n="e" s="T75">(oralləlde) </ts>
               <ts e="T327" id="Seg_1346" n="e" s="T76">imamdə, </ts>
               <ts e="T77" id="Seg_1348" n="e" s="T327">((…)) </ts>
               <ts e="T79" id="Seg_1350" n="e" s="T77">nɨka </ts>
               <ts e="T80" id="Seg_1352" n="e" s="T79">oralʼlʼe, </ts>
               <ts e="T81" id="Seg_1354" n="e" s="T80">(qwalləlde). </ts>
            </ts>
            <ts e="T91" id="Seg_1355" n="sc" s="T82">
               <ts e="T328" id="Seg_1357" n="e" s="T82">(Teperʼ=) </ts>
               <ts e="T83" id="Seg_1359" n="e" s="T328">a </ts>
               <ts e="T84" id="Seg_1361" n="e" s="T83">talʼǯel </ts>
               <ts e="T85" id="Seg_1363" n="e" s="T84">onǯ </ts>
               <ts e="T86" id="Seg_1365" n="e" s="T85">pʼalgalk </ts>
               <ts e="T87" id="Seg_1367" n="e" s="T86">qwella </ts>
               <ts e="T88" id="Seg_1369" n="e" s="T87">wargə </ts>
               <ts e="T89" id="Seg_1371" n="e" s="T88">temnʼad </ts>
               <ts e="T90" id="Seg_1373" n="e" s="T89">čangɨl </ts>
               <ts e="T91" id="Seg_1375" n="e" s="T90">wattoɣend. </ts>
            </ts>
            <ts e="T97" id="Seg_1376" n="sc" s="T92">
               <ts e="T93" id="Seg_1378" n="e" s="T92">A </ts>
               <ts e="T94" id="Seg_1380" n="e" s="T93">anʼǯäd, </ts>
               <ts e="T95" id="Seg_1382" n="e" s="T94">oralle </ts>
               <ts e="T96" id="Seg_1384" n="e" s="T95">Qamaǯäp, </ts>
               <ts e="T97" id="Seg_1386" n="e" s="T96">qwallelde. </ts>
            </ts>
            <ts e="T103" id="Seg_1387" n="sc" s="T98">
               <ts e="T99" id="Seg_1389" n="e" s="T98">Qamaǯä, </ts>
               <ts e="T100" id="Seg_1391" n="e" s="T99">manmut </ts>
               <ts e="T101" id="Seg_1393" n="e" s="T100">pon </ts>
               <ts e="T102" id="Seg_1395" n="e" s="T101">čanǯle, </ts>
               <ts e="T103" id="Seg_1397" n="e" s="T102">qwella. </ts>
            </ts>
            <ts e="T108" id="Seg_1398" n="sc" s="T104">
               <ts e="T105" id="Seg_1400" n="e" s="T104">Qwella, </ts>
               <ts e="T106" id="Seg_1402" n="e" s="T105">ku </ts>
               <ts e="T107" id="Seg_1404" n="e" s="T106">xajd </ts>
               <ts e="T108" id="Seg_1406" n="e" s="T107">ada. </ts>
            </ts>
            <ts e="T113" id="Seg_1407" n="sc" s="T109">
               <ts e="T110" id="Seg_1409" n="e" s="T109">Čaǯa, </ts>
               <ts e="T111" id="Seg_1411" n="e" s="T110">konǯernɨtə: </ts>
               <ts e="T112" id="Seg_1413" n="e" s="T111">kule </ts>
               <ts e="T113" id="Seg_1415" n="e" s="T112">wašešpɨnda. </ts>
            </ts>
            <ts e="T118" id="Seg_1416" n="sc" s="T114">
               <ts e="T116" id="Seg_1418" n="e" s="T114">akəndə </ts>
               <ts e="T117" id="Seg_1420" n="e" s="T116">tadrɨtə </ts>
               <ts e="T118" id="Seg_1422" n="e" s="T117">qwel. </ts>
            </ts>
            <ts e="T127" id="Seg_1423" n="sc" s="T119">
               <ts e="T120" id="Seg_1425" n="e" s="T119">Tab, </ts>
               <ts e="T121" id="Seg_1427" n="e" s="T120">Qamaǯä, </ts>
               <ts e="T122" id="Seg_1429" n="e" s="T121">na </ts>
               <ts e="T123" id="Seg_1431" n="e" s="T122">kulel </ts>
               <ts e="T124" id="Seg_1433" n="e" s="T123">qajmut </ts>
               <ts e="T125" id="Seg_1435" n="e" s="T124">töšpa, </ts>
               <ts e="T126" id="Seg_1437" n="e" s="T125">načʼidə </ts>
               <ts e="T127" id="Seg_1439" n="e" s="T126">qwella. </ts>
            </ts>
            <ts e="T131" id="Seg_1440" n="sc" s="T128">
               <ts e="T129" id="Seg_1442" n="e" s="T128">Čaǯla, </ts>
               <ts e="T130" id="Seg_1444" n="e" s="T129">medəla </ts>
               <ts e="T131" id="Seg_1446" n="e" s="T130">kɨge. </ts>
            </ts>
            <ts e="T137" id="Seg_1447" n="sc" s="T132">
               <ts e="T133" id="Seg_1449" n="e" s="T132">Na </ts>
               <ts e="T134" id="Seg_1451" n="e" s="T133">kɨgeɣen </ts>
               <ts e="T135" id="Seg_1453" n="e" s="T134">nɨlʼǯik </ts>
               <ts e="T136" id="Seg_1455" n="e" s="T135">qwəl </ts>
               <ts e="T137" id="Seg_1457" n="e" s="T136">kočʼeja. </ts>
            </ts>
            <ts e="T321" id="Seg_1458" n="sc" s="T324">
               <ts e="T329" id="Seg_1460" n="e" s="T324">Tabə </ts>
               <ts e="T323" id="Seg_1462" n="e" s="T329">((PAUSE)) </ts>
               <ts e="T330" id="Seg_1464" n="e" s="T323">pačalelʼčʼəlde </ts>
               <ts e="T322" id="Seg_1466" n="e" s="T330">((PAUSE)) </ts>
               <ts e="T325" id="Seg_1468" n="e" s="T322">(mop-) </ts>
               <ts e="T321" id="Seg_1470" n="e" s="T325">((BRK))… </ts>
            </ts>
            <ts e="T144" id="Seg_1471" n="sc" s="T138">
               <ts e="T139" id="Seg_1473" n="e" s="T138">Tab </ts>
               <ts e="T140" id="Seg_1475" n="e" s="T139">pačalelʼčʼəlde </ts>
               <ts e="T141" id="Seg_1477" n="e" s="T140">mogep </ts>
               <ts e="T142" id="Seg_1479" n="e" s="T141">i </ts>
               <ts e="T143" id="Seg_1481" n="e" s="T142">kujam </ts>
               <ts e="T144" id="Seg_1483" n="e" s="T143">melʼčʼəlde. </ts>
            </ts>
            <ts e="T148" id="Seg_1484" n="sc" s="T145">
               <ts e="T146" id="Seg_1486" n="e" s="T145">Dawaj </ts>
               <ts e="T147" id="Seg_1488" n="e" s="T146">qweləm </ts>
               <ts e="T148" id="Seg_1490" n="e" s="T147">oɣolešpɨgu. </ts>
            </ts>
            <ts e="T153" id="Seg_1491" n="sc" s="T149">
               <ts e="T150" id="Seg_1493" n="e" s="T149">Oɣolde </ts>
               <ts e="T151" id="Seg_1495" n="e" s="T150">qwelp, </ts>
               <ts e="T152" id="Seg_1497" n="e" s="T151">tüp </ts>
               <ts e="T153" id="Seg_1499" n="e" s="T152">čadəla. </ts>
            </ts>
            <ts e="T158" id="Seg_1500" n="sc" s="T154">
               <ts e="T155" id="Seg_1502" n="e" s="T154">Kare </ts>
               <ts e="T156" id="Seg_1504" n="e" s="T155">qwelp </ts>
               <ts e="T157" id="Seg_1506" n="e" s="T156">(čʼabolʼ) </ts>
               <ts e="T158" id="Seg_1508" n="e" s="T157">maškelʼǯəqəlte. </ts>
            </ts>
            <ts e="T163" id="Seg_1509" n="sc" s="T159">
               <ts e="T160" id="Seg_1511" n="e" s="T159">Trug </ts>
               <ts e="T161" id="Seg_1513" n="e" s="T160">mančeǯʼla: </ts>
               <ts e="T162" id="Seg_1515" n="e" s="T161">urop </ts>
               <ts e="T163" id="Seg_1517" n="e" s="T162">töšpɨnd. </ts>
            </ts>
            <ts e="T170" id="Seg_1518" n="sc" s="T164">
               <ts e="T165" id="Seg_1520" n="e" s="T164">Urop </ts>
               <ts e="T166" id="Seg_1522" n="e" s="T165">töla, </ts>
               <ts e="T167" id="Seg_1524" n="e" s="T166">oɣənʼǯʼəla </ts>
               <ts e="T168" id="Seg_1526" n="e" s="T167">Qamaǯän: </ts>
               <ts e="T169" id="Seg_1528" n="e" s="T168">"Qaj </ts>
               <ts e="T170" id="Seg_1530" n="e" s="T169">mešpɨndaɣ?" </ts>
            </ts>
            <ts e="T175" id="Seg_1531" n="sc" s="T171">
               <ts e="T172" id="Seg_1533" n="e" s="T171">Mat </ts>
               <ts e="T173" id="Seg_1535" n="e" s="T172">makɨlmɨ </ts>
               <ts e="T174" id="Seg_1537" n="e" s="T173">kare </ts>
               <ts e="T175" id="Seg_1539" n="e" s="T174">perešpɨndak. </ts>
            </ts>
            <ts e="T181" id="Seg_1540" n="sc" s="T176">
               <ts e="T331" id="Seg_1542" n="e" s="T176">Urop, </ts>
               <ts e="T177" id="Seg_1544" n="e" s="T331">no, </ts>
               <ts e="T178" id="Seg_1546" n="e" s="T177">taɣ </ts>
               <ts e="T179" id="Seg_1548" n="e" s="T178">omdeš, </ts>
               <ts e="T180" id="Seg_1550" n="e" s="T179">tü </ts>
               <ts e="T181" id="Seg_1552" n="e" s="T180">tae. </ts>
            </ts>
            <ts e="T186" id="Seg_1553" n="sc" s="T182">
               <ts e="T183" id="Seg_1555" n="e" s="T182">Urop </ts>
               <ts e="T184" id="Seg_1557" n="e" s="T183">tü </ts>
               <ts e="T185" id="Seg_1559" n="e" s="T184">tae </ts>
               <ts e="T186" id="Seg_1561" n="e" s="T185">omdla. </ts>
            </ts>
            <ts e="T191" id="Seg_1562" n="sc" s="T187">
               <ts e="T188" id="Seg_1564" n="e" s="T187">"Qamaǯä, </ts>
               <ts e="T189" id="Seg_1566" n="e" s="T188">meka </ts>
               <ts e="T190" id="Seg_1568" n="e" s="T189">naj </ts>
               <ts e="T191" id="Seg_1570" n="e" s="T190">mele!" </ts>
            </ts>
            <ts e="T200" id="Seg_1571" n="sc" s="T192">
               <ts e="T193" id="Seg_1573" n="e" s="T192">Tʼäperʼ </ts>
               <ts e="T194" id="Seg_1575" n="e" s="T193">čabɨ </ts>
               <ts e="T195" id="Seg_1577" n="e" s="T194">muʒla, </ts>
               <ts e="T196" id="Seg_1579" n="e" s="T195">tab </ts>
               <ts e="T197" id="Seg_1581" n="e" s="T196">oqqer </ts>
               <ts e="T198" id="Seg_1583" n="e" s="T197">čab </ts>
               <ts e="T199" id="Seg_1585" n="e" s="T198">uromn </ts>
               <ts e="T200" id="Seg_1587" n="e" s="T199">čačɨlde. </ts>
            </ts>
            <ts e="T333" id="Seg_1588" n="sc" s="T201">
               <ts e="T202" id="Seg_1590" n="e" s="T201">Urop </ts>
               <ts e="T203" id="Seg_1592" n="e" s="T202">abəlde: </ts>
               <ts e="T204" id="Seg_1594" n="e" s="T203">"Ox, </ts>
               <ts e="T205" id="Seg_1596" n="e" s="T204">swa </ts>
               <ts e="T333" id="Seg_1598" n="e" s="T205">ma! </ts>
            </ts>
            <ts e="T212" id="Seg_1599" n="sc" s="T206">
               <ts e="T332" id="Seg_1601" n="e" s="T206">(Mat=) </ts>
               <ts e="T207" id="Seg_1603" n="e" s="T332">dawaj </ts>
               <ts e="T208" id="Seg_1605" n="e" s="T207">mat </ts>
               <ts e="T209" id="Seg_1607" n="e" s="T208">makɨl </ts>
               <ts e="T210" id="Seg_1609" n="e" s="T209">toʒe </ts>
               <ts e="T211" id="Seg_1611" n="e" s="T210">te </ts>
               <ts e="T212" id="Seg_1613" n="e" s="T211">baqqɨlǯ!" </ts>
            </ts>
            <ts e="T219" id="Seg_1614" n="sc" s="T213">
               <ts e="T214" id="Seg_1616" n="e" s="T213">Qamaǯä </ts>
               <ts e="T215" id="Seg_1618" n="e" s="T214">eǯalgwa: </ts>
               <ts e="T216" id="Seg_1620" n="e" s="T215">"Tat </ts>
               <ts e="T217" id="Seg_1622" n="e" s="T216">maʒek </ts>
               <ts e="T218" id="Seg_1624" n="e" s="T217">aː </ts>
               <ts e="T219" id="Seg_1626" n="e" s="T218">ablende?" </ts>
            </ts>
            <ts e="T222" id="Seg_1627" n="sc" s="T220">
               <ts e="T221" id="Seg_1629" n="e" s="T220">"Aː </ts>
               <ts e="T222" id="Seg_1631" n="e" s="T221">ablage!" </ts>
            </ts>
            <ts e="T227" id="Seg_1632" n="sc" s="T223">
               <ts e="T224" id="Seg_1634" n="e" s="T223">Tʼäperʼ </ts>
               <ts e="T334" id="Seg_1636" n="e" s="T224">Qamaǯä: </ts>
               <ts e="T225" id="Seg_1638" n="e" s="T334">"No, </ts>
               <ts e="T226" id="Seg_1640" n="e" s="T225">qote </ts>
               <ts e="T227" id="Seg_1642" n="e" s="T226">qondeš!" </ts>
            </ts>
            <ts e="T232" id="Seg_1643" n="sc" s="T228">
               <ts e="T229" id="Seg_1645" n="e" s="T228">Tabə </ts>
               <ts e="T230" id="Seg_1647" n="e" s="T229">urop </ts>
               <ts e="T231" id="Seg_1649" n="e" s="T230">qote </ts>
               <ts e="T232" id="Seg_1651" n="e" s="T231">qondəla. </ts>
            </ts>
            <ts e="T243" id="Seg_1652" n="sc" s="T233">
               <ts e="T234" id="Seg_1654" n="e" s="T233">Qamaǯä </ts>
               <ts e="T235" id="Seg_1656" n="e" s="T234">paɣɨp </ts>
               <ts e="T236" id="Seg_1658" n="e" s="T235">aŋdɨ </ts>
               <ts e="T335" id="Seg_1660" n="e" s="T236">aləlde, </ts>
               <ts e="T237" id="Seg_1662" n="e" s="T335">tʼaper </ts>
               <ts e="T238" id="Seg_1664" n="e" s="T237">urot </ts>
               <ts e="T239" id="Seg_1666" n="e" s="T238">pärgap </ts>
               <ts e="T240" id="Seg_1668" n="e" s="T239">korreʒʼelde, </ts>
               <ts e="T241" id="Seg_1670" n="e" s="T240">urop </ts>
               <ts e="T242" id="Seg_1672" n="e" s="T241">nɨka </ts>
               <ts e="T243" id="Seg_1674" n="e" s="T242">qula. </ts>
            </ts>
            <ts e="T250" id="Seg_1675" n="sc" s="T244">
               <ts e="T336" id="Seg_1677" n="e" s="T244">Qamaǯä </ts>
               <ts e="T245" id="Seg_1679" n="e" s="T336">täper </ts>
               <ts e="T246" id="Seg_1681" n="e" s="T245">kɨrɨlde, </ts>
               <ts e="T247" id="Seg_1683" n="e" s="T246">waǯemdə </ts>
               <ts e="T248" id="Seg_1685" n="e" s="T247">wesʼ </ts>
               <ts e="T249" id="Seg_1687" n="e" s="T248">te </ts>
               <ts e="T250" id="Seg_1689" n="e" s="T249">ubirailde. </ts>
            </ts>
            <ts e="T266" id="Seg_1690" n="sc" s="T251">
               <ts e="T252" id="Seg_1692" n="e" s="T251">Potom </ts>
               <ts e="T253" id="Seg_1694" n="e" s="T252">wes </ts>
               <ts e="T254" id="Seg_1696" n="e" s="T253">taptɨl </ts>
               <ts e="T255" id="Seg_1698" n="e" s="T254">čʼel, </ts>
               <ts e="T256" id="Seg_1700" n="e" s="T255">ile </ts>
               <ts e="T257" id="Seg_1702" n="e" s="T256">wesʼ </ts>
               <ts e="T337" id="Seg_1704" n="e" s="T257">(katomkaɣ-) </ts>
               <ts e="T258" id="Seg_1706" n="e" s="T337">katomkal </ts>
               <ts e="T259" id="Seg_1708" n="e" s="T258">koǯaɣɨnd </ts>
               <ts e="T260" id="Seg_1710" n="e" s="T259">pallde, </ts>
               <ts e="T261" id="Seg_1712" n="e" s="T260">tirel </ts>
               <ts e="T262" id="Seg_1714" n="e" s="T261">koǯagɨnd </ts>
               <ts e="T263" id="Seg_1716" n="e" s="T262">pallde </ts>
               <ts e="T264" id="Seg_1718" n="e" s="T263">i </ts>
               <ts e="T265" id="Seg_1720" n="e" s="T264">ugulʼǯe </ts>
               <ts e="T266" id="Seg_1722" n="e" s="T265">üpalʼčəla. </ts>
            </ts>
            <ts e="T271" id="Seg_1723" n="sc" s="T267">
               <ts e="T268" id="Seg_1725" n="e" s="T267">Ugulʼǯe </ts>
               <ts e="T269" id="Seg_1727" n="e" s="T268">medešpa, </ts>
               <ts e="T270" id="Seg_1729" n="e" s="T269">kaško </ts>
               <ts e="T271" id="Seg_1731" n="e" s="T270">čaŋgwa. </ts>
            </ts>
            <ts e="T280" id="Seg_1732" n="sc" s="T272">
               <ts e="T273" id="Seg_1734" n="e" s="T272">Maːtə </ts>
               <ts e="T274" id="Seg_1736" n="e" s="T273">(par) </ts>
               <ts e="T275" id="Seg_1738" n="e" s="T274">enne </ts>
               <ts e="T276" id="Seg_1740" n="e" s="T275">čanǯla, </ts>
               <ts e="T277" id="Seg_1742" n="e" s="T276">šölʼ </ts>
               <ts e="T278" id="Seg_1744" n="e" s="T277">(par) </ts>
               <ts e="T279" id="Seg_1746" n="e" s="T278">elle </ts>
               <ts e="T280" id="Seg_1748" n="e" s="T279">mannemba. </ts>
            </ts>
            <ts e="T288" id="Seg_1749" n="sc" s="T281">
               <ts e="T282" id="Seg_1751" n="e" s="T281">Tebnʼadə, </ts>
               <ts e="T283" id="Seg_1753" n="e" s="T282">anʼǯädə </ts>
               <ts e="T284" id="Seg_1755" n="e" s="T283">oqqer </ts>
               <ts e="T285" id="Seg_1757" n="e" s="T284">tüt </ts>
               <ts e="T286" id="Seg_1759" n="e" s="T285">xajp </ts>
               <ts e="T287" id="Seg_1761" n="e" s="T286">šədu </ts>
               <ts e="T288" id="Seg_1763" n="e" s="T287">tarešpat. </ts>
            </ts>
            <ts e="T339" id="Seg_1764" n="sc" s="T289">
               <ts e="T290" id="Seg_1766" n="e" s="T289">"Taw </ts>
               <ts e="T291" id="Seg_1768" n="e" s="T290">mat </ts>
               <ts e="T339" id="Seg_1770" n="e" s="T291">Qamaǯäm." </ts>
            </ts>
            <ts e="T292" id="Seg_1771" n="sc" s="T338">
               <ts e="T292" id="Seg_1773" n="e" s="T338">((…)). </ts>
            </ts>
            <ts e="T298" id="Seg_1774" n="sc" s="T293">
               <ts e="T294" id="Seg_1776" n="e" s="T293">Qamaǯä </ts>
               <ts e="T295" id="Seg_1778" n="e" s="T294">načʼat </ts>
               <ts e="T296" id="Seg_1780" n="e" s="T295">parǯla: </ts>
               <ts e="T297" id="Seg_1782" n="e" s="T296">"Mat </ts>
               <ts e="T298" id="Seg_1784" n="e" s="T297">tawejak!" </ts>
            </ts>
            <ts e="T305" id="Seg_1785" n="sc" s="T299">
               <ts e="T300" id="Seg_1787" n="e" s="T299">Qamaǯä </ts>
               <ts e="T301" id="Seg_1789" n="e" s="T300">tabeštjaɣen </ts>
               <ts e="T302" id="Seg_1791" n="e" s="T301">qwəl, </ts>
               <ts e="T303" id="Seg_1793" n="e" s="T302">waǯʼep </ts>
               <ts e="T304" id="Seg_1795" n="e" s="T303">elle </ts>
               <ts e="T305" id="Seg_1797" n="e" s="T304">tülʼǯla. </ts>
            </ts>
            <ts e="T309" id="Seg_1798" n="sc" s="T306">
               <ts e="T307" id="Seg_1800" n="e" s="T306">Abətəlde </ts>
               <ts e="T308" id="Seg_1802" n="e" s="T307">qwəlhe, </ts>
               <ts e="T309" id="Seg_1804" n="e" s="T308">waǯʼəhe. </ts>
            </ts>
            <ts e="T314" id="Seg_1805" n="sc" s="T310">
               <ts e="T311" id="Seg_1807" n="e" s="T310">A </ts>
               <ts e="T312" id="Seg_1809" n="e" s="T311">potom </ts>
               <ts e="T313" id="Seg_1811" n="e" s="T312">erelkwan, </ts>
               <ts e="T314" id="Seg_1813" n="e" s="T313">wargelkwat. </ts>
            </ts>
            <ts e="T318" id="Seg_1814" n="sc" s="T315">
               <ts e="T316" id="Seg_1816" n="e" s="T315">Nem, </ts>
               <ts e="T317" id="Seg_1818" n="e" s="T316">ip </ts>
               <ts e="T318" id="Seg_1820" n="e" s="T317">qondkwat. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_1821" s="T1">YIF_196X_Kamadzha2_flk.001 (001)</ta>
            <ta e="T11" id="Seg_1822" s="T7">YIF_196X_Kamadzha2_flk.002 (003)</ta>
            <ta e="T16" id="Seg_1823" s="T12">YIF_196X_Kamadzha2_flk.003 (004)</ta>
            <ta e="T21" id="Seg_1824" s="T17">YIF_196X_Kamadzha2_flk.004 (005)</ta>
            <ta e="T29" id="Seg_1825" s="T22">YIF_196X_Kamadzha2_flk.005 (006)</ta>
            <ta e="T41" id="Seg_1826" s="T30">YIF_196X_Kamadzha2_flk.006 (007)</ta>
            <ta e="T48" id="Seg_1827" s="T42">YIF_196X_Kamadzha2_flk.007 (008)</ta>
            <ta e="T56" id="Seg_1828" s="T115">YIF_196X_Kamadzha2_flk.008 (009)</ta>
            <ta e="T66" id="Seg_1829" s="T57">YIF_196X_Kamadzha2_flk.009 (010)</ta>
            <ta e="T71" id="Seg_1830" s="T67">YIF_196X_Kamadzha2_flk.010 (011)</ta>
            <ta e="T81" id="Seg_1831" s="T72">YIF_196X_Kamadzha2_flk.011 (012)</ta>
            <ta e="T91" id="Seg_1832" s="T82">YIF_196X_Kamadzha2_flk.012 (013)</ta>
            <ta e="T97" id="Seg_1833" s="T92">YIF_196X_Kamadzha2_flk.013 (014)</ta>
            <ta e="T103" id="Seg_1834" s="T98">YIF_196X_Kamadzha2_flk.014 (015)</ta>
            <ta e="T108" id="Seg_1835" s="T104">YIF_196X_Kamadzha2_flk.015 (016)</ta>
            <ta e="T113" id="Seg_1836" s="T109">YIF_196X_Kamadzha2_flk.016 (017)</ta>
            <ta e="T118" id="Seg_1837" s="T114">YIF_196X_Kamadzha2_flk.017 (018)</ta>
            <ta e="T127" id="Seg_1838" s="T119">YIF_196X_Kamadzha2_flk.018 (019)</ta>
            <ta e="T131" id="Seg_1839" s="T128">YIF_196X_Kamadzha2_flk.019 (020)</ta>
            <ta e="T137" id="Seg_1840" s="T132">YIF_196X_Kamadzha2_flk.020 (021)</ta>
            <ta e="T321" id="Seg_1841" s="T324">YIF_196X_Kamadzha2_flk.021 (022)</ta>
            <ta e="T144" id="Seg_1842" s="T138">YIF_196X_Kamadzha2_flk.022 (022)</ta>
            <ta e="T148" id="Seg_1843" s="T145">YIF_196X_Kamadzha2_flk.023 (023)</ta>
            <ta e="T153" id="Seg_1844" s="T149">YIF_196X_Kamadzha2_flk.024 (024)</ta>
            <ta e="T158" id="Seg_1845" s="T154">YIF_196X_Kamadzha2_flk.025 (025)</ta>
            <ta e="T163" id="Seg_1846" s="T159">YIF_196X_Kamadzha2_flk.026 (026)</ta>
            <ta e="T170" id="Seg_1847" s="T164">YIF_196X_Kamadzha2_flk.027 (027)</ta>
            <ta e="T175" id="Seg_1848" s="T171">YIF_196X_Kamadzha2_flk.028 (028)</ta>
            <ta e="T181" id="Seg_1849" s="T176">YIF_196X_Kamadzha2_flk.029 (029)</ta>
            <ta e="T186" id="Seg_1850" s="T182">YIF_196X_Kamadzha2_flk.030 (030)</ta>
            <ta e="T191" id="Seg_1851" s="T187">YIF_196X_Kamadzha2_flk.031 (031)</ta>
            <ta e="T200" id="Seg_1852" s="T192">YIF_196X_Kamadzha2_flk.032 (032)</ta>
            <ta e="T333" id="Seg_1853" s="T201">YIF_196X_Kamadzha2_flk.033 (033.001)</ta>
            <ta e="T212" id="Seg_1854" s="T206">YIF_196X_Kamadzha2_flk.034 (033.002)</ta>
            <ta e="T219" id="Seg_1855" s="T213">YIF_196X_Kamadzha2_flk.035 (034)</ta>
            <ta e="T222" id="Seg_1856" s="T220">YIF_196X_Kamadzha2_flk.036 (035)</ta>
            <ta e="T227" id="Seg_1857" s="T223">YIF_196X_Kamadzha2_flk.037 (036)</ta>
            <ta e="T232" id="Seg_1858" s="T228">YIF_196X_Kamadzha2_flk.038 (037)</ta>
            <ta e="T243" id="Seg_1859" s="T233">YIF_196X_Kamadzha2_flk.039 (038)</ta>
            <ta e="T250" id="Seg_1860" s="T244">YIF_196X_Kamadzha2_flk.040 (039)</ta>
            <ta e="T266" id="Seg_1861" s="T251">YIF_196X_Kamadzha2_flk.041 (040)</ta>
            <ta e="T271" id="Seg_1862" s="T267">YIF_196X_Kamadzha2_flk.042 (041)</ta>
            <ta e="T280" id="Seg_1863" s="T272">YIF_196X_Kamadzha2_flk.043 (042)</ta>
            <ta e="T288" id="Seg_1864" s="T281">YIF_196X_Kamadzha2_flk.044 (043)</ta>
            <ta e="T339" id="Seg_1865" s="T289">YIF_196X_Kamadzha2_flk.045 (044)</ta>
            <ta e="T292" id="Seg_1866" s="T338">YIF_196X_Kamadzha2_flk.046 (044)</ta>
            <ta e="T298" id="Seg_1867" s="T293">YIF_196X_Kamadzha2_flk.047 (045)</ta>
            <ta e="T305" id="Seg_1868" s="T299">YIF_196X_Kamadzha2_flk.048 (046)</ta>
            <ta e="T309" id="Seg_1869" s="T306">YIF_196X_Kamadzha2_flk.049 (047)</ta>
            <ta e="T314" id="Seg_1870" s="T310">YIF_196X_Kamadzha2_flk.050 (048)</ta>
            <ta e="T318" id="Seg_1871" s="T315">YIF_196X_Kamadzha2_flk.051 (049)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_1872" s="T1">Тэбня́ҳе э̄кунда. Варг конда́ӷ.</ta>
            <ta e="T11" id="Seg_1873" s="T7">Варг тэбня́д нӓдэмбле́ варгыква́ӷ.</ta>
            <ta e="T16" id="Seg_1874" s="T12">Ӱҷэга́ тэбня́д квэ́рат Камаджӓ.</ta>
            <ta e="T21" id="Seg_1875" s="T17">Камаджӓ квая́кумба ча́нгэл ватто́утэ.</ta>
            <ta e="T48" id="Seg_1876" s="T42">Камаджӓ парква́: «А́нгэ, ме́ка ко́кал-кы́мбал квэ́джеш!»</ta>
            <ta e="T56" id="Seg_1877" s="T115">Варг тэбня́д Камаджӓн хогоньджӓ: «Кайко́ ны́льджик парква́нд?»</ta>
            <ta e="T66" id="Seg_1878" s="T57">«А мат ноӷ ны́льджик парква́к, ма́жэк аньджӓм кы́мбаҳе-ко́каҳе авбэдэква́».</ta>
            <ta e="T71" id="Seg_1879" s="T67">Начи́л квая́ҳаӷ, чвэ́ссе тӧлаӷ.</ta>
            <ta e="T81" id="Seg_1880" s="T72">Теперь варг тэмня́мд ора́ллендэ о́рманд и, ныка́ ора́лле, квалдэ́.</ta>
            <ta e="T91" id="Seg_1881" s="T82">А та́льджел ондж пялга́лк квэ́лла варг тэбня́д ча́нгэл ватто́гэнд.</ta>
            <ta e="T97" id="Seg_1882" s="T92">А аньджӓд, ора́лле Камаджӓп, квэ́ллелдэ.</ta>
            <ta e="T103" id="Seg_1883" s="T98">Камаджӓ, ма́нмут пон ҷанджле́, квэ́лла.</ta>
            <ta e="T108" id="Seg_1884" s="T104">Квэ́лла, ку хайд ада́.</ta>
            <ta e="T113" id="Seg_1885" s="T109">Ҷа́джа, конджэрны́т: куле́ важэ́шпында.</ta>
            <ta e="T118" id="Seg_1886" s="T114">Манымбы́т — а́конд та́дрыт квэл.</ta>
            <ta e="T127" id="Seg_1887" s="T119">Таб, Камаджӓ, на куле́ ка́ймут э́шпа, начи́д квэ́лла.</ta>
            <ta e="T131" id="Seg_1888" s="T128">Ҷа́джла, медла́ кыге́.</ta>
            <ta e="T137" id="Seg_1889" s="T132">На кыге́ӷэт ны́льджик квэл коче́я.</ta>
            <ta e="T144" id="Seg_1890" s="T138">Таб пача́лельчельдэ мо́гэп и ку́йяп ме́льчельдэ.</ta>
            <ta e="T148" id="Seg_1891" s="T145">Дава́й квэлм хоголджэ́шпэгу.</ta>
            <ta e="T153" id="Seg_1892" s="T149">Хоголдэ́ квэлп, тӱп ча́дла.</ta>
            <ta e="T158" id="Seg_1893" s="T154">Каре́ квэлп ча́бом машке́льджегэлдэ.</ta>
            <ta e="T163" id="Seg_1894" s="T159">Вдруг манҷэ́джьла: ху́руп тӧшпында.</ta>
            <ta e="T170" id="Seg_1895" s="T164">Ху́руп тӧла, хогоньджла́ Камаджӓн: «Кайп ме́шпындал?»</ta>
            <ta e="T175" id="Seg_1896" s="T171">«Мат ма́кылма каре́ пере́шпындак.</ta>
            <ta e="T181" id="Seg_1897" s="T176">Ху́руп, та́э умдэ́ш, тӱ та́э.»</ta>
            <ta e="T186" id="Seg_1898" s="T182">Ху́руп тӱ та́э омдла́.</ta>
            <ta e="T191" id="Seg_1899" s="T187">Камаджӓ: «Ме́ка най меле́!»</ta>
            <ta e="T200" id="Seg_1900" s="T192">Теперь чаб мужла́ — таб о́ӄӄэр чаб ху́рун ҷаҷэлдэ́.</ta>
            <ta e="T333" id="Seg_1901" s="T201">Ху́руп абэлдэ́: «Ох, сва ма!</ta>
            <ta e="T212" id="Seg_1902" s="T206">Дава́й мат ма́кыл то́же тэ па́ӄӄылдж!»</ta>
            <ta e="T219" id="Seg_1903" s="T213">Камаджӓ э́джалгва: «Тат ма́жэк а̄ абле́ндэ?»</ta>
            <ta e="T222" id="Seg_1904" s="T220">А̄ абла́ге!</ta>
            <ta e="T227" id="Seg_1905" s="T223">Теперь Камаджӓ: «Котэ́ кондэ́ш!»</ta>
            <ta e="T232" id="Seg_1906" s="T228">Тав ху́руп котэ́ кондла́.</ta>
            <ta e="T243" id="Seg_1907" s="T233">Камаджӓ па́эп а́нге хӓылдэ́, ху́рун пя́ргыт коррэ́джелдэ, ху́руп ныка́ ку́ла.</ta>
            <ta e="T250" id="Seg_1908" s="T244">Камаджӓ кырылдэ́, ва́джем весь тэ убира́елдэ.</ta>
            <ta e="T266" id="Seg_1909" s="T251">Потом вес та́птыл чел, и́ле весь кото́мкал, коджа́ӷынд палдэ́, тiрэл коджа́гынд палдэ́ и хугулджэ́ ӱподжьла.</ta>
            <ta e="T271" id="Seg_1910" s="T267">Хугулджэ́ медэ́шпа, ка́шко ча́ңгва.</ta>
            <ta e="T280" id="Seg_1911" s="T272">Ма̄т па́ронд эннэ́ ҷанджла́, шоль пар элле́ маннэмба́.</ta>
            <ta e="T288" id="Seg_1912" s="T281">Тэбня́д, аньджӓд о́ӄӄэр тӱт хай шэду́ та́решпат.</ta>
            <ta e="T339" id="Seg_1913" s="T289">«Тав мат Камаджӓн».</ta>
            <ta e="T298" id="Seg_1914" s="T293">Камаджӓ нача́т парджла́: «Мат таве́як!»</ta>
            <ta e="T305" id="Seg_1915" s="T299">Камаджӓ табэштья́ӷэн квэ́лэм, ва́джеп элле́ тӱльджла.</ta>
            <ta e="T309" id="Seg_1916" s="T306">Абэлдэ́ квэ́лҳе, ва́джьҳе.</ta>
            <ta e="T314" id="Seg_1917" s="T310">А потом эвелква́т — варгелква́т.</ta>
            <ta e="T318" id="Seg_1918" s="T315">Нэм, ит кондэква́т.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_1919" s="T1">Tebnʼaxe eːkunda. Warg kondaɣ.</ta>
            <ta e="T11" id="Seg_1920" s="T7">Warg tebnʼad nädemble wargɨkwaɣ.</ta>
            <ta e="T16" id="Seg_1921" s="T12">Üčega tebnʼad kverat Kamaǯä.</ta>
            <ta e="T21" id="Seg_1922" s="T17">Kamaǯä kvajakumba čʼangel vattoute.</ta>
            <ta e="T29" id="Seg_1923" s="T22">A tabenan, varg tebnʼandenan, ilʼmand kverkwatte anʼǯä.</ta>
            <ta e="T41" id="Seg_1924" s="T30">Tʼeper Oqqer godet, kvenaɣ čʼaŋgel vattogende, a Kamaǯä parkwa: «Vašešpa ang!»</ta>
            <ta e="T48" id="Seg_1925" s="T42">Kamaǯä parkva: «Ange, meka kokal-kɨmbal kveǯeš!»</ta>
            <ta e="T56" id="Seg_1926" s="T115">Varg tebnʼad Kamaǯän xogonʼǯä: «Kajko nɨlʼǯik parkvand?»</ta>
            <ta e="T66" id="Seg_1927" s="T57">«A mat noɣ nɨlʼǯik parkvak, maʒek anʼǯäm kɨmbaxe-kokaxe avbedekva».</ta>
            <ta e="T71" id="Seg_1928" s="T67">Načʼil kvajaxaɣ, čʼvesse tölaɣ.</ta>
            <ta e="T81" id="Seg_1929" s="T72">Teperʼ varg temnʼamd orallende imamdə, nɨka oralle, kvalde.</ta>
            <ta e="T91" id="Seg_1930" s="T82">A talʼǯel onǯ pʼalgalk kvella varg tebnʼad čʼangel vattogend.</ta>
            <ta e="T97" id="Seg_1931" s="T92">A anʼǯäd, oralle Kamaǯäp, kvellelde.</ta>
            <ta e="T103" id="Seg_1932" s="T98">Kamaǯä, manmut pon čanǯle, kvella.</ta>
            <ta e="T108" id="Seg_1933" s="T104">Kvella, ku xajd ada.</ta>
            <ta e="T113" id="Seg_1934" s="T109">Čaǯa, konǯernɨt: kule vaʒešpɨnda.</ta>
            <ta e="T118" id="Seg_1935" s="T114">Manɨmbɨt — akond tadrɨt kvel.</ta>
            <ta e="T127" id="Seg_1936" s="T119">Tab, Kamaǯä, na kule kajmut ešpa, načʼid kvella.</ta>
            <ta e="T131" id="Seg_1937" s="T128">Čaǯla, medla kɨge.</ta>
            <ta e="T137" id="Seg_1938" s="T132">Na kɨgeɣet nɨlʼǯik kvel kočʼeja.</ta>
            <ta e="T144" id="Seg_1939" s="T138">Tab pačʼalelʼčʼelʼde mogep i kujʼap melʼčʼelʼde.</ta>
            <ta e="T148" id="Seg_1940" s="T145">Davaj kvelm xogolǯešpegu.</ta>
            <ta e="T153" id="Seg_1941" s="T149">Xogolde kvelp, tüp čʼadla.</ta>
            <ta e="T158" id="Seg_1942" s="T154">Kare kvelp čʼabom maškelʼǯegelde.</ta>
            <ta e="T163" id="Seg_1943" s="T159">Vdrug mančeǯʼla: xurup töšpɨnda.</ta>
            <ta e="T170" id="Seg_1944" s="T164">Xurup töla, xogonʼǯla Kamaǯän: «Kajp mešpɨndal?»</ta>
            <ta e="T175" id="Seg_1945" s="T171">«Mat makɨlma kare perešpɨndak.</ta>
            <ta e="T181" id="Seg_1946" s="T176">Xurup, tae umdeš, tü tae.»</ta>
            <ta e="T186" id="Seg_1947" s="T182">Xurup tü tae omdla.</ta>
            <ta e="T191" id="Seg_1948" s="T187">Kamaǯä: «Meka naj mele!»</ta>
            <ta e="T200" id="Seg_1949" s="T192">Teperʼ čʼab muʒla — tab oqqer čʼab xurun čačelde.</ta>
            <ta e="T333" id="Seg_1950" s="T201">Xurup abelde: "Ox, swa ma! </ta>
            <ta e="T212" id="Seg_1951" s="T206">Davaj mat makɨl toʒe te paqqɨlǯ!»</ta>
            <ta e="T219" id="Seg_1952" s="T213">Kamaǯä eǯalgva: «Tat maʒek aː ablende?»</ta>
            <ta e="T222" id="Seg_1953" s="T220">Aː ablage!</ta>
            <ta e="T227" id="Seg_1954" s="T223">Teperʼ Kamaǯä: «Kote kondeš!»</ta>
            <ta e="T232" id="Seg_1955" s="T228">Tav xurup kote kondla.</ta>
            <ta e="T243" id="Seg_1956" s="T233">Kamaǯä paep ange xäɨlde, xurun pʼargɨt korreǯelde, xurup nɨka kula.</ta>
            <ta e="T250" id="Seg_1957" s="T244">Kamaǯä kɨrɨlde, vaǯem vesʼ te ubiraelde.</ta>
            <ta e="T266" id="Seg_1958" s="T251">Potom ves taptɨl čʼel, ile vesʼ kotomkal, koǯaɣɨnd palde, tirel koǯagɨnd palde i xugulǯe üpoǯʼla.</ta>
            <ta e="T271" id="Seg_1959" s="T267">Xugulǯe medešpa, kaško čʼaŋgva.</ta>
            <ta e="T280" id="Seg_1960" s="T272">Maːt parond enne čanǯla, šolʼ par elle mannemba.</ta>
            <ta e="T288" id="Seg_1961" s="T281">Tebnʼad, anʼǯäd oqqer tüt xaj šedu tarešpat.</ta>
            <ta e="T339" id="Seg_1962" s="T289">«Tav mat Kamaǯän».</ta>
            <ta e="T298" id="Seg_1963" s="T293">Kamaǯä načʼat parǯla: «Mat tavejak!»</ta>
            <ta e="T305" id="Seg_1964" s="T299">Kamaǯä tabeštjaɣen kvelem, vaǯep elle tülʼǯla.</ta>
            <ta e="T309" id="Seg_1965" s="T306">Abelde kvelxe, vaǯʼxe.</ta>
            <ta e="T314" id="Seg_1966" s="T310">A potom evelkvat — wargelkwat.</ta>
            <ta e="T318" id="Seg_1967" s="T315">Nem, it kondekvat.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_1968" s="T1">(Šidə) tɨmnʼäsɨɣa (eːkundaɣ) ((BRK)) wargɨkondaɣ. </ta>
            <ta e="T11" id="Seg_1969" s="T7">Warge temnʼadə nädemble wargɨkwa. </ta>
            <ta e="T16" id="Seg_1970" s="T12">Üčega temnʼadə qwerat Qamaǯʼä. </ta>
            <ta e="T21" id="Seg_1971" s="T17">Qamaǯʼän optɨ qwajakwaɣə čaŋgɨl wattout. </ta>
            <ta e="T29" id="Seg_1972" s="T22">A tabənan, wargə temnʼandenan, emamtə qwerkwattə anʼǯä. </ta>
            <ta e="T41" id="Seg_1973" s="T30">Tʼeper oqqer godet, qwennaɣə čaŋgəl wattoɣənde, a Qamaǯʼä parkwa: "Wašešpa aŋga!" </ta>
            <ta e="T48" id="Seg_1974" s="T42">A Qamaǯä parkwa: "Aŋgəː, meka qoqal-qɨmbal kwəǯeš!" </ta>
            <ta e="T56" id="Seg_1975" s="T115">A wargə temnʼadə Qamaǯʼän oɣənʼǯʼä: "Qajko nɨlʼǯik parkwand?" </ta>
            <ta e="T66" id="Seg_1976" s="T57">"A mat noɣ nɨlʼǯik parkwak, maʒek anʼǯäm (qɨmb-) qɨmbahe-qoqahe abədekkwa". </ta>
            <ta e="T71" id="Seg_1977" s="T67">Načil qwajahaɣ, čʼwesse töːlaɣ. </ta>
            <ta e="T81" id="Seg_1978" s="T72">Teperʼ wargə temnʼat (oralləlde) ((LAUGH)) imamdə, ((…)) nɨka oralʼlʼe, (qwalləlde). </ta>
            <ta e="T91" id="Seg_1979" s="T82">(Teperʼ=) a talʼǯel onǯ pʼalgalk qwella wargə temnʼad čangɨl wattoɣend. </ta>
            <ta e="T97" id="Seg_1980" s="T92">A anʼǯäd, oralle Qamaǯäp, qwallelde. </ta>
            <ta e="T103" id="Seg_1981" s="T98">Qamaǯä, manmut pon čanǯle, qwella. </ta>
            <ta e="T108" id="Seg_1982" s="T104">Qwella, ku xajd ada. </ta>
            <ta e="T113" id="Seg_1983" s="T109">Čaǯa, konǯernɨtə: kule wašešpɨnda. </ta>
            <ta e="T118" id="Seg_1984" s="T114">Akəndə tadrɨtə qwel. </ta>
            <ta e="T127" id="Seg_1985" s="T119">Tab, Qamaǯä, na kulel qajmut töšpa, načʼidə qwella. </ta>
            <ta e="T131" id="Seg_1986" s="T128">Čaǯla, medəla kɨge. </ta>
            <ta e="T137" id="Seg_1987" s="T132">Na kɨgeɣen nɨlʼǯik qwəl kočʼeja. </ta>
            <ta e="T321" id="Seg_1988" s="T324">Tabə ((PAUSE)) pačalelʼčʼəlde ((PAUSE)) (mop-) ((BRK))…</ta>
            <ta e="T144" id="Seg_1989" s="T138">Tab pačalelʼčʼəlde mogep i kujam melʼčʼəlde. </ta>
            <ta e="T148" id="Seg_1990" s="T145">Dawaj qweləm oɣolešpɨgu. </ta>
            <ta e="T153" id="Seg_1991" s="T149">Oɣolde qwəlp, tüp čadəla. </ta>
            <ta e="T158" id="Seg_1992" s="T154">Kare ((…)) qwelp (čʼabolʼ) maškelʼǯəqəlte. </ta>
            <ta e="T163" id="Seg_1993" s="T159">Trug mančeǯʼla: urop töšpɨnd. </ta>
            <ta e="T170" id="Seg_1994" s="T164">Urop töla, oɣənʼǯʼəla Qamaǯän: "Qaj mešpɨndaɣ?" </ta>
            <ta e="T175" id="Seg_1995" s="T171">"Mat makɨlmɨ kare perešpɨndak. </ta>
            <ta e="T181" id="Seg_1996" s="T176">Urop, no, tae omdeš, tü tae." </ta>
            <ta e="T186" id="Seg_1997" s="T182">Urop tü tae omdla. </ta>
            <ta e="T191" id="Seg_1998" s="T187">"Qamaǯä, meka naj mele!" </ta>
            <ta e="T200" id="Seg_1999" s="T192">Tʼäperʼ čabɨ muʒla, tab oqqer čab uromn čačɨlde. </ta>
            <ta e="T333" id="Seg_2000" s="T201">Urop abəlde: "Ox, swa ma! </ta>
            <ta e="T212" id="Seg_2001" s="T206">(Mat=) dawaj mat makɨl toʒe te baqqɨlǯ!" </ta>
            <ta e="T219" id="Seg_2002" s="T213">Qamaǯä eǯalgwa: "Tat maʒek aː ablende?" </ta>
            <ta e="T222" id="Seg_2003" s="T220">"Aː ablage!" </ta>
            <ta e="T227" id="Seg_2004" s="T223">Tʼäperʼ Qamaǯä: "No, qote qondeš!" </ta>
            <ta e="T232" id="Seg_2005" s="T228">Tabə urop qote qondəla. </ta>
            <ta e="T243" id="Seg_2006" s="T233">Qamaǯä paɣɨp aŋdɨ aləlde, tʼaper urot pärgap korreʒʼelde, urop nɨka qula. </ta>
            <ta e="T250" id="Seg_2007" s="T244">Qamaǯä täper kɨrɨlde, waǯemdə wesʼ te ubirailde. </ta>
            <ta e="T266" id="Seg_2008" s="T251">Potom wes taptɨl čʼel, ile wesʼ (katomkaɣ-) katomkal koǯaɣɨnd pallde, tirel koǯaɣɨnd pallde i ugulʼǯe üpalʼčəla. </ta>
            <ta e="T271" id="Seg_2009" s="T267">Ugulʼǯe medešpa, kaško čaŋgwa. </ta>
            <ta e="T280" id="Seg_2010" s="T272">Maːtə (par) enne čanǯla, šölʼ (par) elle mannemba. </ta>
            <ta e="T288" id="Seg_2011" s="T281">Temnʼadə, anʼǯädə oqqer tüt xajp šədu tarešpat. </ta>
            <ta e="T339" id="Seg_2012" s="T289">"Taw mat Qamaǯäm." </ta>
            <ta e="T292" id="Seg_2013" s="T338">((…)). </ta>
            <ta e="T298" id="Seg_2014" s="T293">Qamaǯä načʼat parǯla: "Mat tawejak!" </ta>
            <ta e="T305" id="Seg_2015" s="T299">Qamaǯä tabeštjaɣen qwəl, waǯʼep elle tülʼǯla. </ta>
            <ta e="T309" id="Seg_2016" s="T306">Abətəlde qwəlhe, waǯʼəhe. </ta>
            <ta e="T314" id="Seg_2017" s="T310">A potom erelkwan, wargelkwat. </ta>
            <ta e="T318" id="Seg_2018" s="T315">Nem, ip qondkwat. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T319" id="Seg_2019" s="T1">šidə</ta>
            <ta e="T2" id="Seg_2020" s="T319">tɨmnʼä-sɨ-ɣa</ta>
            <ta e="T320" id="Seg_2021" s="T2">eː-ku-nda-ɣ</ta>
            <ta e="T6" id="Seg_2022" s="T4">wargɨ-ko-nda-ɣ</ta>
            <ta e="T8" id="Seg_2023" s="T7">warge</ta>
            <ta e="T9" id="Seg_2024" s="T8">temnʼa-də</ta>
            <ta e="T10" id="Seg_2025" s="T9">näde-mb-le</ta>
            <ta e="T11" id="Seg_2026" s="T10">wargɨ-k-wa</ta>
            <ta e="T13" id="Seg_2027" s="T12">üčega</ta>
            <ta e="T14" id="Seg_2028" s="T13">temnʼa-də</ta>
            <ta e="T15" id="Seg_2029" s="T14">qwer-a-t</ta>
            <ta e="T16" id="Seg_2030" s="T15">Qamaǯʼä</ta>
            <ta e="T3" id="Seg_2031" s="T17">Qamaǯʼä-n</ta>
            <ta e="T18" id="Seg_2032" s="T3">optɨ</ta>
            <ta e="T19" id="Seg_2033" s="T18">qwaja-ku-wa-ɣə</ta>
            <ta e="T20" id="Seg_2034" s="T19">čaŋg-ɨ-l</ta>
            <ta e="T21" id="Seg_2035" s="T20">watto-ut</ta>
            <ta e="T23" id="Seg_2036" s="T22">a</ta>
            <ta e="T24" id="Seg_2037" s="T23">tab-ə-nan</ta>
            <ta e="T25" id="Seg_2038" s="T24">wargə</ta>
            <ta e="T26" id="Seg_2039" s="T25">temnʼa-n-de-nan</ta>
            <ta e="T27" id="Seg_2040" s="T26">emam-tə</ta>
            <ta e="T28" id="Seg_2041" s="T27">qwer-k-wa-ttə</ta>
            <ta e="T29" id="Seg_2042" s="T28">anʼǯä</ta>
            <ta e="T31" id="Seg_2043" s="T30">tʼeper</ta>
            <ta e="T32" id="Seg_2044" s="T31">oqqer</ta>
            <ta e="T33" id="Seg_2045" s="T32">god-e-t</ta>
            <ta e="T34" id="Seg_2046" s="T33">qwen-na-ɣə</ta>
            <ta e="T35" id="Seg_2047" s="T34">čaŋg-ə-l</ta>
            <ta e="T36" id="Seg_2048" s="T35">watto-ɣənde</ta>
            <ta e="T37" id="Seg_2049" s="T36">a</ta>
            <ta e="T38" id="Seg_2050" s="T37">Qamaǯʼä</ta>
            <ta e="T39" id="Seg_2051" s="T38">park-wa</ta>
            <ta e="T40" id="Seg_2052" s="T39">waše-špa</ta>
            <ta e="T41" id="Seg_2053" s="T40">aŋga</ta>
            <ta e="T5" id="Seg_2054" s="T42">a</ta>
            <ta e="T43" id="Seg_2055" s="T5">Qamaǯä</ta>
            <ta e="T44" id="Seg_2056" s="T43">park-wa</ta>
            <ta e="T45" id="Seg_2057" s="T44">aŋg-əː</ta>
            <ta e="T46" id="Seg_2058" s="T45">meka</ta>
            <ta e="T47" id="Seg_2059" s="T46">qoqa-l-qɨmba-l</ta>
            <ta e="T48" id="Seg_2060" s="T47">kwəǯ-eš</ta>
            <ta e="T49" id="Seg_2061" s="T115">a</ta>
            <ta e="T50" id="Seg_2062" s="T49">wargə</ta>
            <ta e="T51" id="Seg_2063" s="T50">temnʼa-də</ta>
            <ta e="T52" id="Seg_2064" s="T51">Qamaǯʼä-n</ta>
            <ta e="T53" id="Seg_2065" s="T52">oɣənʼǯʼä</ta>
            <ta e="T54" id="Seg_2066" s="T53">qaj-ko</ta>
            <ta e="T55" id="Seg_2067" s="T54">nɨlʼǯi-k</ta>
            <ta e="T56" id="Seg_2068" s="T55">park-wa-nd</ta>
            <ta e="T58" id="Seg_2069" s="T57">a</ta>
            <ta e="T59" id="Seg_2070" s="T58">mat</ta>
            <ta e="T60" id="Seg_2071" s="T59">noɣ</ta>
            <ta e="T61" id="Seg_2072" s="T60">nɨlʼǯi-k</ta>
            <ta e="T62" id="Seg_2073" s="T61">park-wa-k</ta>
            <ta e="T63" id="Seg_2074" s="T62">maʒek</ta>
            <ta e="T326" id="Seg_2075" s="T63">anʼǯä-m</ta>
            <ta e="T65" id="Seg_2076" s="T64">qɨmba-he-qoqa-he</ta>
            <ta e="T66" id="Seg_2077" s="T65">ab-ə-de-kk-wa</ta>
            <ta e="T68" id="Seg_2078" s="T67">nači-l</ta>
            <ta e="T69" id="Seg_2079" s="T68">qwaja-ha-ɣ</ta>
            <ta e="T70" id="Seg_2080" s="T69">čwesse</ta>
            <ta e="T71" id="Seg_2081" s="T70">töː-la-ɣ</ta>
            <ta e="T73" id="Seg_2082" s="T72">teperʼ</ta>
            <ta e="T74" id="Seg_2083" s="T73">wargə</ta>
            <ta e="T78" id="Seg_2084" s="T74">temnʼa-t</ta>
            <ta e="T76" id="Seg_2085" s="T75">oral-lə-l-de</ta>
            <ta e="T327" id="Seg_2086" s="T76">ima-m-də</ta>
            <ta e="T79" id="Seg_2087" s="T77">nɨka</ta>
            <ta e="T80" id="Seg_2088" s="T79">oralʼ-lʼe</ta>
            <ta e="T81" id="Seg_2089" s="T80">qwal-lə-l-de</ta>
            <ta e="T328" id="Seg_2090" s="T82">teperʼ</ta>
            <ta e="T83" id="Seg_2091" s="T328">a</ta>
            <ta e="T84" id="Seg_2092" s="T83">talʼǯel</ta>
            <ta e="T85" id="Seg_2093" s="T84">onǯ</ta>
            <ta e="T86" id="Seg_2094" s="T85">pʼalgalk</ta>
            <ta e="T87" id="Seg_2095" s="T86">qwel-la</ta>
            <ta e="T88" id="Seg_2096" s="T87">wargə</ta>
            <ta e="T89" id="Seg_2097" s="T88">temnʼa-d</ta>
            <ta e="T90" id="Seg_2098" s="T89">čang-ɨ-l</ta>
            <ta e="T91" id="Seg_2099" s="T90">watto-ɣend</ta>
            <ta e="T93" id="Seg_2100" s="T92">a</ta>
            <ta e="T94" id="Seg_2101" s="T93">anʼǯä-d</ta>
            <ta e="T95" id="Seg_2102" s="T94">oral-le</ta>
            <ta e="T96" id="Seg_2103" s="T95">Qamaǯä-p</ta>
            <ta e="T97" id="Seg_2104" s="T96">qwal-le-l-de</ta>
            <ta e="T99" id="Seg_2105" s="T98">Qamaǯä</ta>
            <ta e="T100" id="Seg_2106" s="T99">man-mut</ta>
            <ta e="T101" id="Seg_2107" s="T100">po-n</ta>
            <ta e="T102" id="Seg_2108" s="T101">čanǯ-le</ta>
            <ta e="T103" id="Seg_2109" s="T102">qwel-la</ta>
            <ta e="T105" id="Seg_2110" s="T104">qwel-la</ta>
            <ta e="T106" id="Seg_2111" s="T105">ku</ta>
            <ta e="T107" id="Seg_2112" s="T106">xaj-d</ta>
            <ta e="T108" id="Seg_2113" s="T107">ada</ta>
            <ta e="T110" id="Seg_2114" s="T109">čaǯa</ta>
            <ta e="T111" id="Seg_2115" s="T110">konǯer-nɨ-tə</ta>
            <ta e="T112" id="Seg_2116" s="T111">kule</ta>
            <ta e="T113" id="Seg_2117" s="T112">waše-špɨ-nda</ta>
            <ta e="T116" id="Seg_2118" s="T114">ak-ə-ndə</ta>
            <ta e="T117" id="Seg_2119" s="T116">tad-r-ɨ-tə</ta>
            <ta e="T118" id="Seg_2120" s="T117">qwel</ta>
            <ta e="T120" id="Seg_2121" s="T119">tab</ta>
            <ta e="T121" id="Seg_2122" s="T120">Qamaǯä</ta>
            <ta e="T122" id="Seg_2123" s="T121">na</ta>
            <ta e="T123" id="Seg_2124" s="T122">kule-l</ta>
            <ta e="T124" id="Seg_2125" s="T123">qaj-mut</ta>
            <ta e="T125" id="Seg_2126" s="T124">tö-špa</ta>
            <ta e="T126" id="Seg_2127" s="T125">načʼi-də</ta>
            <ta e="T127" id="Seg_2128" s="T126">qwel-la</ta>
            <ta e="T129" id="Seg_2129" s="T128">čaǯ-la</ta>
            <ta e="T130" id="Seg_2130" s="T129">medə-la</ta>
            <ta e="T131" id="Seg_2131" s="T130">kɨge</ta>
            <ta e="T133" id="Seg_2132" s="T132">na</ta>
            <ta e="T134" id="Seg_2133" s="T133">kɨge-ɣen</ta>
            <ta e="T135" id="Seg_2134" s="T134">nɨlʼǯi-k</ta>
            <ta e="T136" id="Seg_2135" s="T135">qwəl</ta>
            <ta e="T137" id="Seg_2136" s="T136">kočʼe-ja</ta>
            <ta e="T329" id="Seg_2137" s="T324">tabə</ta>
            <ta e="T330" id="Seg_2138" s="T323">pačal-e-lʼčʼə-l-de</ta>
            <ta e="T139" id="Seg_2139" s="T138">tab</ta>
            <ta e="T140" id="Seg_2140" s="T139">pačal-e-lʼčʼə-l-de</ta>
            <ta e="T141" id="Seg_2141" s="T140">moge-p</ta>
            <ta e="T142" id="Seg_2142" s="T141">i</ta>
            <ta e="T143" id="Seg_2143" s="T142">kuja-m</ta>
            <ta e="T144" id="Seg_2144" s="T143">me-lʼčʼə-l-de</ta>
            <ta e="T146" id="Seg_2145" s="T145">dawaj</ta>
            <ta e="T147" id="Seg_2146" s="T146">qwel-ə-m</ta>
            <ta e="T148" id="Seg_2147" s="T147">oɣol-e-špɨ-gu</ta>
            <ta e="T150" id="Seg_2148" s="T149">oɣol-de</ta>
            <ta e="T151" id="Seg_2149" s="T150">qwel-p</ta>
            <ta e="T152" id="Seg_2150" s="T151">tü-p</ta>
            <ta e="T153" id="Seg_2151" s="T152">čadə-la</ta>
            <ta e="T155" id="Seg_2152" s="T154">kare</ta>
            <ta e="T156" id="Seg_2153" s="T155">qwel-p</ta>
            <ta e="T157" id="Seg_2154" s="T156">čʼabo-lʼ</ta>
            <ta e="T158" id="Seg_2155" s="T157">maške-lʼǯe-qə-l-te</ta>
            <ta e="T160" id="Seg_2156" s="T159">trug</ta>
            <ta e="T161" id="Seg_2157" s="T160">manče-ǯʼ-la</ta>
            <ta e="T162" id="Seg_2158" s="T161">urop</ta>
            <ta e="T163" id="Seg_2159" s="T162">tö-špɨ-nd</ta>
            <ta e="T165" id="Seg_2160" s="T164">urop</ta>
            <ta e="T166" id="Seg_2161" s="T165">tö-la</ta>
            <ta e="T167" id="Seg_2162" s="T166">oɣənʼǯʼə-la</ta>
            <ta e="T168" id="Seg_2163" s="T167">Qamaǯä-n</ta>
            <ta e="T169" id="Seg_2164" s="T168">qaj</ta>
            <ta e="T170" id="Seg_2165" s="T169">me-špɨ-nda-ɣ</ta>
            <ta e="T172" id="Seg_2166" s="T171">mat</ta>
            <ta e="T173" id="Seg_2167" s="T172">makɨl-mɨ</ta>
            <ta e="T174" id="Seg_2168" s="T173">kare</ta>
            <ta e="T175" id="Seg_2169" s="T174">per-e-špɨ-nda-k</ta>
            <ta e="T331" id="Seg_2170" s="T176">urop</ta>
            <ta e="T177" id="Seg_2171" s="T331">no</ta>
            <ta e="T178" id="Seg_2172" s="T177">taɣ</ta>
            <ta e="T179" id="Seg_2173" s="T178">omde-š</ta>
            <ta e="T180" id="Seg_2174" s="T179">tü</ta>
            <ta e="T181" id="Seg_2175" s="T180">tae</ta>
            <ta e="T183" id="Seg_2176" s="T182">urop</ta>
            <ta e="T184" id="Seg_2177" s="T183">tü</ta>
            <ta e="T185" id="Seg_2178" s="T184">tae</ta>
            <ta e="T186" id="Seg_2179" s="T185">omd-la</ta>
            <ta e="T188" id="Seg_2180" s="T187">Qamaǯä</ta>
            <ta e="T189" id="Seg_2181" s="T188">meka</ta>
            <ta e="T190" id="Seg_2182" s="T189">naj</ta>
            <ta e="T191" id="Seg_2183" s="T190">me-le</ta>
            <ta e="T193" id="Seg_2184" s="T192">tʼäperʼ</ta>
            <ta e="T194" id="Seg_2185" s="T193">čabɨ</ta>
            <ta e="T195" id="Seg_2186" s="T194">muʒ-la</ta>
            <ta e="T196" id="Seg_2187" s="T195">tab</ta>
            <ta e="T197" id="Seg_2188" s="T196">oqqer</ta>
            <ta e="T198" id="Seg_2189" s="T197">čab</ta>
            <ta e="T199" id="Seg_2190" s="T198">urom-n</ta>
            <ta e="T200" id="Seg_2191" s="T199">čačɨ-l-de</ta>
            <ta e="T202" id="Seg_2192" s="T201">urop</ta>
            <ta e="T203" id="Seg_2193" s="T202">ab-ə-l-de</ta>
            <ta e="T204" id="Seg_2194" s="T203">Ox</ta>
            <ta e="T205" id="Seg_2195" s="T204">swa</ta>
            <ta e="T333" id="Seg_2196" s="T205">ma</ta>
            <ta e="T207" id="Seg_2197" s="T332">dawaj</ta>
            <ta e="T208" id="Seg_2198" s="T207">mat</ta>
            <ta e="T209" id="Seg_2199" s="T208">makɨl</ta>
            <ta e="T210" id="Seg_2200" s="T209">toʒe</ta>
            <ta e="T211" id="Seg_2201" s="T210">te</ta>
            <ta e="T212" id="Seg_2202" s="T211">baqqɨl-ǯ</ta>
            <ta e="T214" id="Seg_2203" s="T213">Qamaǯä</ta>
            <ta e="T215" id="Seg_2204" s="T214">eǯal-g-wa</ta>
            <ta e="T216" id="Seg_2205" s="T215">tat</ta>
            <ta e="T217" id="Seg_2206" s="T216">maʒek</ta>
            <ta e="T218" id="Seg_2207" s="T217">aː</ta>
            <ta e="T219" id="Seg_2208" s="T218">ab-le-nde</ta>
            <ta e="T221" id="Seg_2209" s="T220">aː</ta>
            <ta e="T222" id="Seg_2210" s="T221">ab-le-ge</ta>
            <ta e="T224" id="Seg_2211" s="T223">tʼäperʼ</ta>
            <ta e="T334" id="Seg_2212" s="T224">Qamaǯä</ta>
            <ta e="T225" id="Seg_2213" s="T334">no</ta>
            <ta e="T226" id="Seg_2214" s="T225">qo-te</ta>
            <ta e="T227" id="Seg_2215" s="T226">qonde-š</ta>
            <ta e="T229" id="Seg_2216" s="T228">tabə</ta>
            <ta e="T230" id="Seg_2217" s="T229">urop</ta>
            <ta e="T231" id="Seg_2218" s="T230">qo-te</ta>
            <ta e="T232" id="Seg_2219" s="T231">qondə-la</ta>
            <ta e="T234" id="Seg_2220" s="T233">Qamaǯä</ta>
            <ta e="T235" id="Seg_2221" s="T234">paɣɨ-p</ta>
            <ta e="T236" id="Seg_2222" s="T235">aŋdɨ</ta>
            <ta e="T335" id="Seg_2223" s="T236">alə-l-de</ta>
            <ta e="T237" id="Seg_2224" s="T335">tʼaper</ta>
            <ta e="T238" id="Seg_2225" s="T237">uro-t</ta>
            <ta e="T239" id="Seg_2226" s="T238">pʼarg-ɨ-t</ta>
            <ta e="T240" id="Seg_2227" s="T239">korreʒʼel-de</ta>
            <ta e="T241" id="Seg_2228" s="T240">urop</ta>
            <ta e="T242" id="Seg_2229" s="T241">nɨka</ta>
            <ta e="T243" id="Seg_2230" s="T242">qu-la</ta>
            <ta e="T336" id="Seg_2231" s="T244">Qamaǯä</ta>
            <ta e="T245" id="Seg_2232" s="T336">täper</ta>
            <ta e="T246" id="Seg_2233" s="T245">kɨrɨ-l-de</ta>
            <ta e="T247" id="Seg_2234" s="T246">waǯe-m-də</ta>
            <ta e="T248" id="Seg_2235" s="T247">wesʼ</ta>
            <ta e="T249" id="Seg_2236" s="T248">te</ta>
            <ta e="T250" id="Seg_2237" s="T249">ubirai-l-de</ta>
            <ta e="T252" id="Seg_2238" s="T251">potom</ta>
            <ta e="T253" id="Seg_2239" s="T252">wes</ta>
            <ta e="T254" id="Seg_2240" s="T253">taptɨl</ta>
            <ta e="T255" id="Seg_2241" s="T254">čʼel</ta>
            <ta e="T256" id="Seg_2242" s="T255">ile</ta>
            <ta e="T257" id="Seg_2243" s="T256">wesʼ</ta>
            <ta e="T258" id="Seg_2244" s="T337">katomka-l</ta>
            <ta e="T259" id="Seg_2245" s="T258">koǯa-ɣɨnd</ta>
            <ta e="T260" id="Seg_2246" s="T259">pal-l-de</ta>
            <ta e="T261" id="Seg_2247" s="T260">tire-l</ta>
            <ta e="T262" id="Seg_2248" s="T261">koǯa-gɨnd</ta>
            <ta e="T263" id="Seg_2249" s="T262">pal-l-de</ta>
            <ta e="T264" id="Seg_2250" s="T263">i</ta>
            <ta e="T265" id="Seg_2251" s="T264">ugulʼǯe</ta>
            <ta e="T266" id="Seg_2252" s="T265">üpa-lʼčə-la</ta>
            <ta e="T268" id="Seg_2253" s="T267">Ugulʼǯe</ta>
            <ta e="T269" id="Seg_2254" s="T268">mede-špa</ta>
            <ta e="T270" id="Seg_2255" s="T269">kaško</ta>
            <ta e="T271" id="Seg_2256" s="T270">čaŋg-wa</ta>
            <ta e="T273" id="Seg_2257" s="T272">maːtə</ta>
            <ta e="T274" id="Seg_2258" s="T273">par</ta>
            <ta e="T275" id="Seg_2259" s="T274">inne</ta>
            <ta e="T276" id="Seg_2260" s="T275">čanǯ-la</ta>
            <ta e="T277" id="Seg_2261" s="T276">šölʼ</ta>
            <ta e="T278" id="Seg_2262" s="T277">par</ta>
            <ta e="T279" id="Seg_2263" s="T278">elle</ta>
            <ta e="T280" id="Seg_2264" s="T279">manne-mba</ta>
            <ta e="T282" id="Seg_2265" s="T281">tebnʼa-də</ta>
            <ta e="T283" id="Seg_2266" s="T282">anʼǯä-də</ta>
            <ta e="T284" id="Seg_2267" s="T283">oqqer</ta>
            <ta e="T285" id="Seg_2268" s="T284">tü-t</ta>
            <ta e="T286" id="Seg_2269" s="T285">xaj-p</ta>
            <ta e="T287" id="Seg_2270" s="T286">šədu</ta>
            <ta e="T288" id="Seg_2271" s="T287">tar-e-špa-t</ta>
            <ta e="T290" id="Seg_2272" s="T289">taw</ta>
            <ta e="T291" id="Seg_2273" s="T290">mat</ta>
            <ta e="T339" id="Seg_2274" s="T291">Qamaǯä-m</ta>
            <ta e="T294" id="Seg_2275" s="T293">Qamaǯä</ta>
            <ta e="T295" id="Seg_2276" s="T294">načʼa-t</ta>
            <ta e="T296" id="Seg_2277" s="T295">parǯ-la</ta>
            <ta e="T297" id="Seg_2278" s="T296">mat</ta>
            <ta e="T298" id="Seg_2279" s="T297">taw-e-ja-k</ta>
            <ta e="T300" id="Seg_2280" s="T299">Qamaǯä</ta>
            <ta e="T301" id="Seg_2281" s="T300">tab-e-štja-ɣen</ta>
            <ta e="T302" id="Seg_2282" s="T301">qwəl</ta>
            <ta e="T303" id="Seg_2283" s="T302">waǯʼe-p</ta>
            <ta e="T304" id="Seg_2284" s="T303">elle</ta>
            <ta e="T305" id="Seg_2285" s="T304">tü-lʼǯ-la</ta>
            <ta e="T307" id="Seg_2286" s="T306">ab-ə-tə-l-de</ta>
            <ta e="T308" id="Seg_2287" s="T307">qwəl-he</ta>
            <ta e="T309" id="Seg_2288" s="T308">waǯʼə-he</ta>
            <ta e="T311" id="Seg_2289" s="T310">a</ta>
            <ta e="T312" id="Seg_2290" s="T311">potom</ta>
            <ta e="T313" id="Seg_2291" s="T312">ere-l-k-wa-n</ta>
            <ta e="T314" id="Seg_2292" s="T313">warge-lə-k-wa-t</ta>
            <ta e="T316" id="Seg_2293" s="T315">ne-m</ta>
            <ta e="T317" id="Seg_2294" s="T316">i-p</ta>
            <ta e="T318" id="Seg_2295" s="T317">qo-nd-k-wa-t</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T319" id="Seg_2296" s="T1">šitə</ta>
            <ta e="T2" id="Seg_2297" s="T319">tebnʼa-sɨ-qi</ta>
            <ta e="T320" id="Seg_2298" s="T2">e-ku-ndɨ-q</ta>
            <ta e="T6" id="Seg_2299" s="T4">wargɨ-ku-ndɨ-q</ta>
            <ta e="T8" id="Seg_2300" s="T7">wargɨ</ta>
            <ta e="T9" id="Seg_2301" s="T8">tebnʼa-tɨ</ta>
            <ta e="T10" id="Seg_2302" s="T9">nadɨ-mbɨ-le</ta>
            <ta e="T11" id="Seg_2303" s="T10">wargɨ-ku-wa</ta>
            <ta e="T13" id="Seg_2304" s="T12">üčega</ta>
            <ta e="T14" id="Seg_2305" s="T13">tebnʼa-tɨ</ta>
            <ta e="T15" id="Seg_2306" s="T14">qwär-ɨ-tɨ</ta>
            <ta e="T16" id="Seg_2307" s="T15">Qaːmaǯʼa</ta>
            <ta e="T3" id="Seg_2308" s="T17">Qaːmaǯʼa-n</ta>
            <ta e="T18" id="Seg_2309" s="T3">optɨ</ta>
            <ta e="T19" id="Seg_2310" s="T18">qwaja-ku-wa-q</ta>
            <ta e="T20" id="Seg_2311" s="T19">čaŋg-ɨ-lʼ</ta>
            <ta e="T21" id="Seg_2312" s="T20">wattə-ut</ta>
            <ta e="T23" id="Seg_2313" s="T22">a</ta>
            <ta e="T24" id="Seg_2314" s="T23">tab-ɨ-nan</ta>
            <ta e="T25" id="Seg_2315" s="T24">wargɨ</ta>
            <ta e="T26" id="Seg_2316" s="T25">tebnʼa-n-tɨ-nan</ta>
            <ta e="T27" id="Seg_2317" s="T26">emam-tɨ</ta>
            <ta e="T28" id="Seg_2318" s="T27">qwär-ku-wa-dət</ta>
            <ta e="T29" id="Seg_2319" s="T28">anǯʼa</ta>
            <ta e="T31" id="Seg_2320" s="T30">taper</ta>
            <ta e="T32" id="Seg_2321" s="T31">okkər</ta>
            <ta e="T33" id="Seg_2322" s="T32">god-ɨ-tɨ</ta>
            <ta e="T34" id="Seg_2323" s="T33">qwän-ŋɨ-q</ta>
            <ta e="T35" id="Seg_2324" s="T34">čaŋg-ɨ-lʼ</ta>
            <ta e="T36" id="Seg_2325" s="T35">wattə-qɨnt</ta>
            <ta e="T37" id="Seg_2326" s="T36">a</ta>
            <ta e="T38" id="Seg_2327" s="T37">Qaːmaǯʼa</ta>
            <ta e="T39" id="Seg_2328" s="T38">parka-wa</ta>
            <ta e="T40" id="Seg_2329" s="T39">waše-špɨ</ta>
            <ta e="T41" id="Seg_2330" s="T40">aŋga</ta>
            <ta e="T5" id="Seg_2331" s="T42">a</ta>
            <ta e="T43" id="Seg_2332" s="T5">Qaːmaǯʼa</ta>
            <ta e="T44" id="Seg_2333" s="T43">parka-wa</ta>
            <ta e="T45" id="Seg_2334" s="T44">aŋga-eː</ta>
            <ta e="T46" id="Seg_2335" s="T45">mäkkä</ta>
            <ta e="T47" id="Seg_2336" s="T46">qoqa-lʼ-qɨmba-lʼ</ta>
            <ta e="T48" id="Seg_2337" s="T47">kwešɨ-äšɨk</ta>
            <ta e="T49" id="Seg_2338" s="T115">a</ta>
            <ta e="T50" id="Seg_2339" s="T49">wargɨ</ta>
            <ta e="T51" id="Seg_2340" s="T50">tebnʼa-tɨ</ta>
            <ta e="T52" id="Seg_2341" s="T51">Qaːmaǯʼa-n</ta>
            <ta e="T53" id="Seg_2342" s="T52">hoqonǯe</ta>
            <ta e="T54" id="Seg_2343" s="T53">qaj-tqo</ta>
            <ta e="T55" id="Seg_2344" s="T54">nɨlʼǯi-k</ta>
            <ta e="T56" id="Seg_2345" s="T55">parka-wa-nd</ta>
            <ta e="T58" id="Seg_2346" s="T57">a</ta>
            <ta e="T59" id="Seg_2347" s="T58">man</ta>
            <ta e="T60" id="Seg_2348" s="T59">noɣ</ta>
            <ta e="T61" id="Seg_2349" s="T60">nɨlʼǯi-k</ta>
            <ta e="T62" id="Seg_2350" s="T61">parka-wa-k</ta>
            <ta e="T63" id="Seg_2351" s="T62">maᴣik</ta>
            <ta e="T326" id="Seg_2352" s="T63">anǯʼa-mɨ</ta>
            <ta e="T65" id="Seg_2353" s="T64">qɨmba-se-qoqa-se</ta>
            <ta e="T66" id="Seg_2354" s="T65">am-ɨ-dɨ-ku-wa</ta>
            <ta e="T68" id="Seg_2355" s="T67">nača-lʼ</ta>
            <ta e="T69" id="Seg_2356" s="T68">qwaja-sɨ-q</ta>
            <ta e="T70" id="Seg_2357" s="T69">čwesse</ta>
            <ta e="T71" id="Seg_2358" s="T70">töː-lɨ-q</ta>
            <ta e="T73" id="Seg_2359" s="T72">taper</ta>
            <ta e="T74" id="Seg_2360" s="T73">wargɨ</ta>
            <ta e="T78" id="Seg_2361" s="T74">temnʼa-tɨ</ta>
            <ta e="T76" id="Seg_2362" s="T75">oral-lə-la-tɨ</ta>
            <ta e="T327" id="Seg_2363" s="T76">ima-m-dɨ</ta>
            <ta e="T79" id="Seg_2364" s="T77">nɨka</ta>
            <ta e="T80" id="Seg_2365" s="T79">oral-le</ta>
            <ta e="T81" id="Seg_2366" s="T80">kwat-lə-la-tɨ</ta>
            <ta e="T328" id="Seg_2367" s="T82">taper</ta>
            <ta e="T83" id="Seg_2368" s="T328">a</ta>
            <ta e="T84" id="Seg_2369" s="T83">talǯʼel</ta>
            <ta e="T85" id="Seg_2370" s="T84">onǯe</ta>
            <ta e="T86" id="Seg_2371" s="T85">pʼalgalk</ta>
            <ta e="T87" id="Seg_2372" s="T86">qwän-la</ta>
            <ta e="T88" id="Seg_2373" s="T87">wargɨ</ta>
            <ta e="T89" id="Seg_2374" s="T88">tebnʼa-tɨ</ta>
            <ta e="T90" id="Seg_2375" s="T89">čaŋg-ɨ-lʼ</ta>
            <ta e="T91" id="Seg_2376" s="T90">wat-qɨnt</ta>
            <ta e="T93" id="Seg_2377" s="T92">a</ta>
            <ta e="T94" id="Seg_2378" s="T93">anǯʼa-tɨ</ta>
            <ta e="T95" id="Seg_2379" s="T94">oral-le</ta>
            <ta e="T96" id="Seg_2380" s="T95">Qaːmaǯʼa-p</ta>
            <ta e="T97" id="Seg_2381" s="T96">kwat-le-la-tɨ</ta>
            <ta e="T99" id="Seg_2382" s="T98">Qaːmaǯʼa</ta>
            <ta e="T100" id="Seg_2383" s="T99">maːt-mutə</ta>
            <ta e="T101" id="Seg_2384" s="T100">po-n</ta>
            <ta e="T102" id="Seg_2385" s="T101">čanǯe-le</ta>
            <ta e="T103" id="Seg_2386" s="T102">qwän-la</ta>
            <ta e="T105" id="Seg_2387" s="T104">qwän-la</ta>
            <ta e="T106" id="Seg_2388" s="T105">ku</ta>
            <ta e="T107" id="Seg_2389" s="T106">saj-tɨ</ta>
            <ta e="T108" id="Seg_2390" s="T107">adɨ</ta>
            <ta e="T110" id="Seg_2391" s="T109">čaːǯɨ</ta>
            <ta e="T111" id="Seg_2392" s="T110">qonǯer-ŋɨ-tɨ</ta>
            <ta e="T112" id="Seg_2393" s="T111">kuläː</ta>
            <ta e="T113" id="Seg_2394" s="T112">waše-špɨ-ndɨ</ta>
            <ta e="T116" id="Seg_2395" s="T114">ak-ɨ-nde</ta>
            <ta e="T117" id="Seg_2396" s="T116">tade-r-ɨ-tɨ</ta>
            <ta e="T118" id="Seg_2397" s="T117">qwel</ta>
            <ta e="T120" id="Seg_2398" s="T119">tab</ta>
            <ta e="T121" id="Seg_2399" s="T120">Qaːmaǯʼa</ta>
            <ta e="T122" id="Seg_2400" s="T121">na</ta>
            <ta e="T123" id="Seg_2401" s="T122">kuläː-l</ta>
            <ta e="T124" id="Seg_2402" s="T123">qaj-mun</ta>
            <ta e="T125" id="Seg_2403" s="T124">tö-špɨ</ta>
            <ta e="T126" id="Seg_2404" s="T125">nača-nt</ta>
            <ta e="T127" id="Seg_2405" s="T126">qwän-la</ta>
            <ta e="T129" id="Seg_2406" s="T128">čaːǯɨ-la</ta>
            <ta e="T130" id="Seg_2407" s="T129">medɨ-la</ta>
            <ta e="T131" id="Seg_2408" s="T130">kɨge</ta>
            <ta e="T133" id="Seg_2409" s="T132">na</ta>
            <ta e="T134" id="Seg_2410" s="T133">kɨge-qɨn</ta>
            <ta e="T135" id="Seg_2411" s="T134">nɨlʼǯi-k</ta>
            <ta e="T136" id="Seg_2412" s="T135">qwel</ta>
            <ta e="T137" id="Seg_2413" s="T136">koček-ja</ta>
            <ta e="T329" id="Seg_2414" s="T324">tab</ta>
            <ta e="T330" id="Seg_2415" s="T323">pačal-ɨ-lʼčǝ-la-tɨ</ta>
            <ta e="T139" id="Seg_2416" s="T138">tab</ta>
            <ta e="T140" id="Seg_2417" s="T139">pačal-ɨ-lʼčǝ-la-tɨ</ta>
            <ta e="T141" id="Seg_2418" s="T140">mugeː-p</ta>
            <ta e="T142" id="Seg_2419" s="T141">i</ta>
            <ta e="T143" id="Seg_2420" s="T142">kuja-p</ta>
            <ta e="T144" id="Seg_2421" s="T143">me-lʼčǝ-la-tɨ</ta>
            <ta e="T146" id="Seg_2422" s="T145">dawaj</ta>
            <ta e="T147" id="Seg_2423" s="T146">qwel-ɨ-m</ta>
            <ta e="T148" id="Seg_2424" s="T147">oɣol-ɨ-špɨ-gu</ta>
            <ta e="T150" id="Seg_2425" s="T149">oɣol-tɨ</ta>
            <ta e="T151" id="Seg_2426" s="T150">qwel-p</ta>
            <ta e="T152" id="Seg_2427" s="T151">tüː-m</ta>
            <ta e="T153" id="Seg_2428" s="T152">čʼadɨ-la</ta>
            <ta e="T155" id="Seg_2429" s="T154">kare</ta>
            <ta e="T156" id="Seg_2430" s="T155">qwel-p</ta>
            <ta e="T157" id="Seg_2431" s="T156">čʼabɨ-lʼ</ta>
            <ta e="T158" id="Seg_2432" s="T157">maške-lʼčǝ-ku-la-tɨ</ta>
            <ta e="T160" id="Seg_2433" s="T159">trug</ta>
            <ta e="T161" id="Seg_2434" s="T160">manǯu-lʼčǝ-la</ta>
            <ta e="T162" id="Seg_2435" s="T161">surup</ta>
            <ta e="T163" id="Seg_2436" s="T162">töː-špɨ-ndɨ</ta>
            <ta e="T165" id="Seg_2437" s="T164">surup</ta>
            <ta e="T166" id="Seg_2438" s="T165">töː-la</ta>
            <ta e="T167" id="Seg_2439" s="T166">hoqonǯe-la</ta>
            <ta e="T168" id="Seg_2440" s="T167">Qaːmaǯʼa-n</ta>
            <ta e="T169" id="Seg_2441" s="T168">qaj</ta>
            <ta e="T170" id="Seg_2442" s="T169">me-špɨ-ndɨ-q</ta>
            <ta e="T172" id="Seg_2443" s="T171">man</ta>
            <ta e="T173" id="Seg_2444" s="T172">maqqɨl-mɨ</ta>
            <ta e="T174" id="Seg_2445" s="T173">kare</ta>
            <ta e="T175" id="Seg_2446" s="T174">per-ɨ-špɨ-ndɨ-k</ta>
            <ta e="T331" id="Seg_2447" s="T176">surup</ta>
            <ta e="T177" id="Seg_2448" s="T331">nu</ta>
            <ta e="T178" id="Seg_2449" s="T177">taɣ</ta>
            <ta e="T179" id="Seg_2450" s="T178">omde-äšɨk</ta>
            <ta e="T180" id="Seg_2451" s="T179">tüː</ta>
            <ta e="T181" id="Seg_2452" s="T180">tae</ta>
            <ta e="T183" id="Seg_2453" s="T182">surup</ta>
            <ta e="T184" id="Seg_2454" s="T183">tüː</ta>
            <ta e="T185" id="Seg_2455" s="T184">tae</ta>
            <ta e="T186" id="Seg_2456" s="T185">omde-la</ta>
            <ta e="T188" id="Seg_2457" s="T187">Qaːmaǯʼa</ta>
            <ta e="T189" id="Seg_2458" s="T188">mäkkä</ta>
            <ta e="T190" id="Seg_2459" s="T189">naj</ta>
            <ta e="T191" id="Seg_2460" s="T190">me-le</ta>
            <ta e="T193" id="Seg_2461" s="T192">taper</ta>
            <ta e="T194" id="Seg_2462" s="T193">čabɨ</ta>
            <ta e="T195" id="Seg_2463" s="T194">muʒ-la</ta>
            <ta e="T196" id="Seg_2464" s="T195">tab</ta>
            <ta e="T197" id="Seg_2465" s="T196">okkər</ta>
            <ta e="T198" id="Seg_2466" s="T197">čab</ta>
            <ta e="T199" id="Seg_2467" s="T198">surup-nɨ</ta>
            <ta e="T200" id="Seg_2468" s="T199">čačɨ-la-tɨ</ta>
            <ta e="T202" id="Seg_2469" s="T201">surup</ta>
            <ta e="T203" id="Seg_2470" s="T202">am-ɨ-la-tɨ</ta>
            <ta e="T204" id="Seg_2471" s="T203">оh</ta>
            <ta e="T205" id="Seg_2472" s="T204">swa</ta>
            <ta e="T333" id="Seg_2473" s="T205">mɨ</ta>
            <ta e="T207" id="Seg_2474" s="T332">dawaj</ta>
            <ta e="T208" id="Seg_2475" s="T207">man</ta>
            <ta e="T209" id="Seg_2476" s="T208">maqqɨl</ta>
            <ta e="T210" id="Seg_2477" s="T209">toʒe</ta>
            <ta e="T211" id="Seg_2478" s="T210">teː</ta>
            <ta e="T212" id="Seg_2479" s="T211">paqɨl-äšɨk</ta>
            <ta e="T214" id="Seg_2480" s="T213">Qaːmaǯʼa</ta>
            <ta e="T215" id="Seg_2481" s="T214">ɛːǯal-ku-wa</ta>
            <ta e="T216" id="Seg_2482" s="T215">tan</ta>
            <ta e="T217" id="Seg_2483" s="T216">maᴣik</ta>
            <ta e="T218" id="Seg_2484" s="T217">aː</ta>
            <ta e="T219" id="Seg_2485" s="T218">am-la-nd</ta>
            <ta e="T221" id="Seg_2486" s="T220">aː</ta>
            <ta e="T222" id="Seg_2487" s="T221">am-la-k</ta>
            <ta e="T224" id="Seg_2488" s="T223">taper</ta>
            <ta e="T334" id="Seg_2489" s="T224">Qaːmaǯʼa</ta>
            <ta e="T225" id="Seg_2490" s="T334">nu</ta>
            <ta e="T226" id="Seg_2491" s="T225">qö-nde</ta>
            <ta e="T227" id="Seg_2492" s="T226">qontɨ-äšɨk</ta>
            <ta e="T229" id="Seg_2493" s="T228">taw</ta>
            <ta e="T230" id="Seg_2494" s="T229">surup</ta>
            <ta e="T231" id="Seg_2495" s="T230">qö-nde</ta>
            <ta e="T232" id="Seg_2496" s="T231">qontɨ-la</ta>
            <ta e="T234" id="Seg_2497" s="T233">Qaːmaǯʼa</ta>
            <ta e="T235" id="Seg_2498" s="T234">paɣɨ-p</ta>
            <ta e="T236" id="Seg_2499" s="T235">aːŋd</ta>
            <ta e="T335" id="Seg_2500" s="T236">hel-la-tɨ</ta>
            <ta e="T237" id="Seg_2501" s="T335">tʼaper</ta>
            <ta e="T238" id="Seg_2502" s="T237">hurup-n</ta>
            <ta e="T239" id="Seg_2503" s="T238">pʼarg-ɨ-tɨ</ta>
            <ta e="T240" id="Seg_2504" s="T239">korreǯel-tɨ</ta>
            <ta e="T241" id="Seg_2505" s="T240">hurup</ta>
            <ta e="T242" id="Seg_2506" s="T241">nɨka</ta>
            <ta e="T243" id="Seg_2507" s="T242">qu-la</ta>
            <ta e="T336" id="Seg_2508" s="T244">Qaːmaǯʼa</ta>
            <ta e="T245" id="Seg_2509" s="T336">taper</ta>
            <ta e="T246" id="Seg_2510" s="T245">kɨrɨ-la-tɨ</ta>
            <ta e="T247" id="Seg_2511" s="T246">waǯʼe-m-t</ta>
            <ta e="T248" id="Seg_2512" s="T247">wesʼ</ta>
            <ta e="T249" id="Seg_2513" s="T248">teː</ta>
            <ta e="T250" id="Seg_2514" s="T249">ubirain-la-tɨ</ta>
            <ta e="T252" id="Seg_2515" s="T251">patom</ta>
            <ta e="T253" id="Seg_2516" s="T252">wesʼ</ta>
            <ta e="T254" id="Seg_2517" s="T253">taptel</ta>
            <ta e="T255" id="Seg_2518" s="T254">čeːl</ta>
            <ta e="T256" id="Seg_2519" s="T255">illä</ta>
            <ta e="T257" id="Seg_2520" s="T256">wesʼ</ta>
            <ta e="T258" id="Seg_2521" s="T337">katomka-lʼ</ta>
            <ta e="T259" id="Seg_2522" s="T258">qoča-qɨnt</ta>
            <ta e="T260" id="Seg_2523" s="T259">pat-la-tɨ</ta>
            <ta e="T261" id="Seg_2524" s="T260">tɨrɨ-lʼ</ta>
            <ta e="T262" id="Seg_2525" s="T261">qoča-qɨnt</ta>
            <ta e="T263" id="Seg_2526" s="T262">pat-la-tɨ</ta>
            <ta e="T264" id="Seg_2527" s="T263">i</ta>
            <ta e="T265" id="Seg_2528" s="T264">sugulʼǯʼe</ta>
            <ta e="T266" id="Seg_2529" s="T265">üːppɨ-lʼčǝ-la</ta>
            <ta e="T268" id="Seg_2530" s="T267">sugulʼǯʼe</ta>
            <ta e="T269" id="Seg_2531" s="T268">medɨ-špɨ</ta>
            <ta e="T270" id="Seg_2532" s="T269">kašqɨ</ta>
            <ta e="T271" id="Seg_2533" s="T270">čaŋgɨ-wa</ta>
            <ta e="T273" id="Seg_2534" s="T272">maːt</ta>
            <ta e="T274" id="Seg_2535" s="T273">par</ta>
            <ta e="T275" id="Seg_2536" s="T274">inne</ta>
            <ta e="T276" id="Seg_2537" s="T275">čanǯe-la</ta>
            <ta e="T277" id="Seg_2538" s="T276">šolʼ</ta>
            <ta e="T278" id="Seg_2539" s="T277">par</ta>
            <ta e="T279" id="Seg_2540" s="T278">illä</ta>
            <ta e="T280" id="Seg_2541" s="T279">mantɨ-mbɨ</ta>
            <ta e="T282" id="Seg_2542" s="T281">tebnʼa-tɨ</ta>
            <ta e="T283" id="Seg_2543" s="T282">anǯʼa-tɨ</ta>
            <ta e="T284" id="Seg_2544" s="T283">okkər</ta>
            <ta e="T285" id="Seg_2545" s="T284">tüː-n</ta>
            <ta e="T286" id="Seg_2546" s="T285">saj-p</ta>
            <ta e="T287" id="Seg_2547" s="T286">šədə</ta>
            <ta e="T288" id="Seg_2548" s="T287">tar-ɨ-špɨ-dət</ta>
            <ta e="T290" id="Seg_2549" s="T289">taw</ta>
            <ta e="T291" id="Seg_2550" s="T290">man</ta>
            <ta e="T339" id="Seg_2551" s="T291">Qaːmaǯʼa-m</ta>
            <ta e="T294" id="Seg_2552" s="T293">Qaːmaǯʼa</ta>
            <ta e="T295" id="Seg_2553" s="T294">nača-n</ta>
            <ta e="T296" id="Seg_2554" s="T295">parʒa-la</ta>
            <ta e="T297" id="Seg_2555" s="T296">man</ta>
            <ta e="T298" id="Seg_2556" s="T297">taw-e-ja-k</ta>
            <ta e="T300" id="Seg_2557" s="T299">Qaːmaǯʼa</ta>
            <ta e="T301" id="Seg_2558" s="T300">tab-ɨ-štja-qɨn</ta>
            <ta e="T302" id="Seg_2559" s="T301">qwel</ta>
            <ta e="T303" id="Seg_2560" s="T302">waǯʼe-p</ta>
            <ta e="T304" id="Seg_2561" s="T303">illä</ta>
            <ta e="T305" id="Seg_2562" s="T304">töː-lǯe-la</ta>
            <ta e="T307" id="Seg_2563" s="T306">am-ɨ-dɨ-la-tɨ</ta>
            <ta e="T308" id="Seg_2564" s="T307">qwel-se</ta>
            <ta e="T309" id="Seg_2565" s="T308">waǯʼe-se</ta>
            <ta e="T311" id="Seg_2566" s="T310">a</ta>
            <ta e="T312" id="Seg_2567" s="T311">patom</ta>
            <ta e="T313" id="Seg_2568" s="T312">ele-lə-ku-wa-dət</ta>
            <ta e="T314" id="Seg_2569" s="T313">wargɨ-lə-ku-wa-dət</ta>
            <ta e="T316" id="Seg_2570" s="T315">neː-m</ta>
            <ta e="T317" id="Seg_2571" s="T316">iː-p</ta>
            <ta e="T318" id="Seg_2572" s="T317">qo-ntɨ-ku-wa-dət</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T319" id="Seg_2573" s="T1">two</ta>
            <ta e="T2" id="Seg_2574" s="T319">brother-DYA-DU</ta>
            <ta e="T320" id="Seg_2575" s="T2">be-HAB-INFER-3DU.S</ta>
            <ta e="T6" id="Seg_2576" s="T4">live-HAB-INFER-3DU.S</ta>
            <ta e="T8" id="Seg_2577" s="T7">big</ta>
            <ta e="T9" id="Seg_2578" s="T8">brother.[NOM]-3SG</ta>
            <ta e="T10" id="Seg_2579" s="T9">get.married-DUR-CVB</ta>
            <ta e="T11" id="Seg_2580" s="T10">live-HAB-CO.[3SG.S]</ta>
            <ta e="T13" id="Seg_2581" s="T12">small</ta>
            <ta e="T14" id="Seg_2582" s="T13">brother.[NOM]-3SG</ta>
            <ta e="T15" id="Seg_2583" s="T14">call-EP-3SG.O</ta>
            <ta e="T16" id="Seg_2584" s="T15">Kamacha.[NOM]</ta>
            <ta e="T3" id="Seg_2585" s="T17">Kamacha-GEN</ta>
            <ta e="T18" id="Seg_2586" s="T3">with</ta>
            <ta e="T19" id="Seg_2587" s="T18">go-HAB-PST-3DU.S</ta>
            <ta e="T20" id="Seg_2588" s="T19">deadfall.trap-EP-ADJZ</ta>
            <ta e="T21" id="Seg_2589" s="T20">road-PROL</ta>
            <ta e="T23" id="Seg_2590" s="T22">but</ta>
            <ta e="T24" id="Seg_2591" s="T23">(s)he-EP-ADES</ta>
            <ta e="T25" id="Seg_2592" s="T24">big</ta>
            <ta e="T26" id="Seg_2593" s="T25">brother-GEN-3SG-ADES</ta>
            <ta e="T27" id="Seg_2594" s="T26">bride.[NOM]-3SG</ta>
            <ta e="T28" id="Seg_2595" s="T27">call-HAB-CO-3PL</ta>
            <ta e="T29" id="Seg_2596" s="T28">daughter_in_law.[NOM]</ta>
            <ta e="T31" id="Seg_2597" s="T30">now</ta>
            <ta e="T32" id="Seg_2598" s="T31">one</ta>
            <ta e="T33" id="Seg_2599" s="T32">year-EP-3SG.O</ta>
            <ta e="T34" id="Seg_2600" s="T33">go.away-CO-3DU.S</ta>
            <ta e="T35" id="Seg_2601" s="T34">trap-EP-ADJZ</ta>
            <ta e="T36" id="Seg_2602" s="T35">road-ILL.3SG</ta>
            <ta e="T37" id="Seg_2603" s="T36">but</ta>
            <ta e="T38" id="Seg_2604" s="T37">Kamacha.[NOM]</ta>
            <ta e="T39" id="Seg_2605" s="T38">shout-CO.[3SG.S]</ta>
            <ta e="T40" id="Seg_2606" s="T39">fly-IPFV2.[3SG.S]</ta>
            <ta e="T41" id="Seg_2607" s="T40">capercaillie.[NOM]</ta>
            <ta e="T5" id="Seg_2608" s="T42">but</ta>
            <ta e="T43" id="Seg_2609" s="T5">Kamacha.[NOM]</ta>
            <ta e="T44" id="Seg_2610" s="T43">shout-CO.[3SG.S]</ta>
            <ta e="T45" id="Seg_2611" s="T44">capercaillie-VOC</ta>
            <ta e="T46" id="Seg_2612" s="T45">I.ALL</ta>
            <ta e="T47" id="Seg_2613" s="T46">%%-ADJZ-%%-ADJZ</ta>
            <ta e="T48" id="Seg_2614" s="T47">leave-IMP.2SG.S</ta>
            <ta e="T49" id="Seg_2615" s="T115">but</ta>
            <ta e="T50" id="Seg_2616" s="T49">big</ta>
            <ta e="T51" id="Seg_2617" s="T50">brother.[NOM]-3SG</ta>
            <ta e="T52" id="Seg_2618" s="T51">Kamacha-GEN</ta>
            <ta e="T53" id="Seg_2619" s="T52">ask.[3SG.S]</ta>
            <ta e="T54" id="Seg_2620" s="T53">what-TRL</ta>
            <ta e="T55" id="Seg_2621" s="T54">such-ADVZ</ta>
            <ta e="T56" id="Seg_2622" s="T55">shout-CO-2SG.S</ta>
            <ta e="T58" id="Seg_2623" s="T57">but</ta>
            <ta e="T59" id="Seg_2624" s="T58">I.NOM</ta>
            <ta e="T60" id="Seg_2625" s="T59">therefore</ta>
            <ta e="T61" id="Seg_2626" s="T60">such-ADVZ</ta>
            <ta e="T62" id="Seg_2627" s="T61">shout-CO-1SG.S</ta>
            <ta e="T63" id="Seg_2628" s="T62">I.ACC</ta>
            <ta e="T326" id="Seg_2629" s="T63">daughter_in_law-1SG</ta>
            <ta e="T65" id="Seg_2630" s="T64">%%-INSTR-%%-INSTR</ta>
            <ta e="T66" id="Seg_2631" s="T65">eat-EP-TR-HAB-CO.[3SG.S]</ta>
            <ta e="T68" id="Seg_2632" s="T67">there-ADJZ</ta>
            <ta e="T69" id="Seg_2633" s="T68">go-PST-3DU.S</ta>
            <ta e="T70" id="Seg_2634" s="T69">backward</ta>
            <ta e="T71" id="Seg_2635" s="T70">come-RES-3DU.S</ta>
            <ta e="T73" id="Seg_2636" s="T72">now</ta>
            <ta e="T74" id="Seg_2637" s="T73">big</ta>
            <ta e="T78" id="Seg_2638" s="T74">brother-3SG</ta>
            <ta e="T76" id="Seg_2639" s="T75">catch-%%-FUT-3SG.O</ta>
            <ta e="T327" id="Seg_2640" s="T76">wife-ACC-3SG</ta>
            <ta e="T79" id="Seg_2641" s="T77">here</ta>
            <ta e="T80" id="Seg_2642" s="T79">catch-CVB</ta>
            <ta e="T81" id="Seg_2643" s="T80">kill-%%-FUT-3SG.O</ta>
            <ta e="T328" id="Seg_2644" s="T82">now</ta>
            <ta e="T83" id="Seg_2645" s="T328">but</ta>
            <ta e="T84" id="Seg_2646" s="T83">tomorrow</ta>
            <ta e="T85" id="Seg_2647" s="T84">oneself.3SG</ta>
            <ta e="T86" id="Seg_2648" s="T85">alone</ta>
            <ta e="T87" id="Seg_2649" s="T86">go.away-FUT.[3SG.S]</ta>
            <ta e="T88" id="Seg_2650" s="T87">big</ta>
            <ta e="T89" id="Seg_2651" s="T88">brother.[NOM]-3SG</ta>
            <ta e="T90" id="Seg_2652" s="T89">trap-EP-ADJZ</ta>
            <ta e="T91" id="Seg_2653" s="T90">road-ILL.3SG</ta>
            <ta e="T93" id="Seg_2654" s="T92">but</ta>
            <ta e="T94" id="Seg_2655" s="T93">daughter_in_law.[NOM]-3SG</ta>
            <ta e="T95" id="Seg_2656" s="T94">catch-CVB</ta>
            <ta e="T96" id="Seg_2657" s="T95">Kamacha-ACC</ta>
            <ta e="T97" id="Seg_2658" s="T96">kill-%%-FUT-3SG.O</ta>
            <ta e="T99" id="Seg_2659" s="T98">Kamacha.[NOM]</ta>
            <ta e="T100" id="Seg_2660" s="T99">house-EL2</ta>
            <ta e="T101" id="Seg_2661" s="T100">space.outside-ILL2</ta>
            <ta e="T102" id="Seg_2662" s="T101">go.out-CVB</ta>
            <ta e="T103" id="Seg_2663" s="T102">go.away-FUT.[3SG.S]</ta>
            <ta e="T105" id="Seg_2664" s="T104">go.away-FUT.[3SG.S]</ta>
            <ta e="T106" id="Seg_2665" s="T105">where</ta>
            <ta e="T107" id="Seg_2666" s="T106">eye.[NOM]-3SG</ta>
            <ta e="T108" id="Seg_2667" s="T107">see.[3SG.S]</ta>
            <ta e="T110" id="Seg_2668" s="T109">travel.[3SG.S]</ta>
            <ta e="T111" id="Seg_2669" s="T110">see-CO-3SG.O</ta>
            <ta e="T112" id="Seg_2670" s="T111">raven.[NOM]</ta>
            <ta e="T113" id="Seg_2671" s="T112">fly-IPFV2-INFER.[3SG.S]</ta>
            <ta e="T116" id="Seg_2672" s="T114">mouth-EP-ILL</ta>
            <ta e="T117" id="Seg_2673" s="T116">bring-FRQ-EP-3SG.O</ta>
            <ta e="T118" id="Seg_2674" s="T117">fish.[NOM]</ta>
            <ta e="T120" id="Seg_2675" s="T119">(s)he.[NOM]</ta>
            <ta e="T121" id="Seg_2676" s="T120">Kamacha.[NOM]</ta>
            <ta e="T122" id="Seg_2677" s="T121">this</ta>
            <ta e="T123" id="Seg_2678" s="T122">raven.[NOM]-2SG</ta>
            <ta e="T124" id="Seg_2679" s="T123">what-PROL</ta>
            <ta e="T125" id="Seg_2680" s="T124">come-IPFV2.[3SG.S]</ta>
            <ta e="T126" id="Seg_2681" s="T125">there-ADV.ILL</ta>
            <ta e="T127" id="Seg_2682" s="T126">go.away-FUT.[3SG.S]</ta>
            <ta e="T129" id="Seg_2683" s="T128">run-FUT.[3SG.S]</ta>
            <ta e="T130" id="Seg_2684" s="T129">reach-FUT.[3SG.S]</ta>
            <ta e="T131" id="Seg_2685" s="T130">river.[NOM]</ta>
            <ta e="T133" id="Seg_2686" s="T132">this</ta>
            <ta e="T134" id="Seg_2687" s="T133">river-LOC</ta>
            <ta e="T135" id="Seg_2688" s="T134">such-ADVZ</ta>
            <ta e="T136" id="Seg_2689" s="T135">fish.[NOM]</ta>
            <ta e="T137" id="Seg_2690" s="T136">much-CO.[3SG.S]</ta>
            <ta e="T329" id="Seg_2691" s="T324">(s)he.[NOM]</ta>
            <ta e="T330" id="Seg_2692" s="T323">cut.down-EP-PFV-FUT-3SG.O</ta>
            <ta e="T139" id="Seg_2693" s="T138">(s)he.[NOM]</ta>
            <ta e="T140" id="Seg_2694" s="T139">cut.down-EP-PFV-FUT-3SG.O</ta>
            <ta e="T141" id="Seg_2695" s="T140">bird.cherry-ACC</ta>
            <ta e="T142" id="Seg_2696" s="T141">and</ta>
            <ta e="T143" id="Seg_2697" s="T142">scoop-ACC</ta>
            <ta e="T144" id="Seg_2698" s="T143">do-PFV-FUT-3SG.O</ta>
            <ta e="T146" id="Seg_2699" s="T145">INCH</ta>
            <ta e="T147" id="Seg_2700" s="T146">fish-EP-ACC</ta>
            <ta e="T148" id="Seg_2701" s="T147">scoop-EP-IPFV2-INF</ta>
            <ta e="T150" id="Seg_2702" s="T149">scoop-3SG.O</ta>
            <ta e="T151" id="Seg_2703" s="T150">fish-ACC</ta>
            <ta e="T152" id="Seg_2704" s="T151">fire-ACC</ta>
            <ta e="T153" id="Seg_2705" s="T152">light.fire-FUT.[3SG.S]</ta>
            <ta e="T155" id="Seg_2706" s="T154">to.the.fire</ta>
            <ta e="T156" id="Seg_2707" s="T155">fish-ACC</ta>
            <ta e="T157" id="Seg_2708" s="T156">skewer-%%</ta>
            <ta e="T158" id="Seg_2709" s="T157">put-PFV-HAB-FUT-3SG.O</ta>
            <ta e="T160" id="Seg_2710" s="T159">suddenly</ta>
            <ta e="T161" id="Seg_2711" s="T160">look-PFV-FUT.[3SG.S]</ta>
            <ta e="T162" id="Seg_2712" s="T161">wild.animal.[NOM]</ta>
            <ta e="T163" id="Seg_2713" s="T162">come-IPFV2-INFER.[3SG.S]</ta>
            <ta e="T165" id="Seg_2714" s="T164">wild.animal.[NOM]</ta>
            <ta e="T166" id="Seg_2715" s="T165">come-FUT.[3SG.S]</ta>
            <ta e="T167" id="Seg_2716" s="T166">ask-FUT.[3SG.S]</ta>
            <ta e="T168" id="Seg_2717" s="T167">Kamacha-ILL2</ta>
            <ta e="T169" id="Seg_2718" s="T168">what.[NOM]</ta>
            <ta e="T170" id="Seg_2719" s="T169">do-IPFV2-INFER-%%</ta>
            <ta e="T172" id="Seg_2720" s="T171">I.NOM</ta>
            <ta e="T173" id="Seg_2721" s="T172">kidney-1SG</ta>
            <ta e="T174" id="Seg_2722" s="T173">to.the.fire</ta>
            <ta e="T175" id="Seg_2723" s="T174">fry-EP-IPFV2-INFER-1SG.S</ta>
            <ta e="T331" id="Seg_2724" s="T176">wild.animal.[NOM]</ta>
            <ta e="T177" id="Seg_2725" s="T331">well</ta>
            <ta e="T178" id="Seg_2726" s="T177">in.front</ta>
            <ta e="T179" id="Seg_2727" s="T178">sit.down-IMP.2SG.S</ta>
            <ta e="T180" id="Seg_2728" s="T179">fire.[NOM]</ta>
            <ta e="T181" id="Seg_2729" s="T180">in.front</ta>
            <ta e="T183" id="Seg_2730" s="T182">wild.animal.[NOM]</ta>
            <ta e="T184" id="Seg_2731" s="T183">fire.[NOM]</ta>
            <ta e="T185" id="Seg_2732" s="T184">in.front</ta>
            <ta e="T186" id="Seg_2733" s="T185">sit.down-FUT.[3SG.S]</ta>
            <ta e="T188" id="Seg_2734" s="T187">Kamacha.[NOM]</ta>
            <ta e="T189" id="Seg_2735" s="T188">I.ALL</ta>
            <ta e="T190" id="Seg_2736" s="T189">also</ta>
            <ta e="T191" id="Seg_2737" s="T190">do-OPT.[3SG.S]</ta>
            <ta e="T193" id="Seg_2738" s="T192">now</ta>
            <ta e="T194" id="Seg_2739" s="T193">skewer.[NOM]</ta>
            <ta e="T195" id="Seg_2740" s="T194">cook-FUT.[3SG.S]</ta>
            <ta e="T196" id="Seg_2741" s="T195">(s)he.[NOM]</ta>
            <ta e="T197" id="Seg_2742" s="T196">one</ta>
            <ta e="T198" id="Seg_2743" s="T197">skewer.[NOM]</ta>
            <ta e="T199" id="Seg_2744" s="T198">wild.animal-ALL</ta>
            <ta e="T200" id="Seg_2745" s="T199">throw-FUT-3SG.O</ta>
            <ta e="T202" id="Seg_2746" s="T201">wild.animal.[NOM]</ta>
            <ta e="T203" id="Seg_2747" s="T202">eat-EP-FUT-3SG.O</ta>
            <ta e="T204" id="Seg_2748" s="T203">oh</ta>
            <ta e="T205" id="Seg_2749" s="T204">good</ta>
            <ta e="T333" id="Seg_2750" s="T205">something.[NOM]</ta>
            <ta e="T207" id="Seg_2751" s="T332">HORT</ta>
            <ta e="T208" id="Seg_2752" s="T207">I.GEN</ta>
            <ta e="T209" id="Seg_2753" s="T208">kidney.[NOM]</ta>
            <ta e="T210" id="Seg_2754" s="T209">also</ta>
            <ta e="T211" id="Seg_2755" s="T210">away</ta>
            <ta e="T212" id="Seg_2756" s="T211">dig-IMP.2SG.S</ta>
            <ta e="T214" id="Seg_2757" s="T213">Kamacha.[NOM]</ta>
            <ta e="T215" id="Seg_2758" s="T214">say-HAB-CO.[3SG.S]</ta>
            <ta e="T216" id="Seg_2759" s="T215">you.SG.NOM</ta>
            <ta e="T217" id="Seg_2760" s="T216">I.ACC</ta>
            <ta e="T218" id="Seg_2761" s="T217">NEG</ta>
            <ta e="T219" id="Seg_2762" s="T218">eat-FUT-2SG.S</ta>
            <ta e="T221" id="Seg_2763" s="T220">NEG</ta>
            <ta e="T222" id="Seg_2764" s="T221">eat-FUT-1SG.S</ta>
            <ta e="T224" id="Seg_2765" s="T223">now</ta>
            <ta e="T334" id="Seg_2766" s="T224">Kamacha.[NOM]</ta>
            <ta e="T225" id="Seg_2767" s="T334">well</ta>
            <ta e="T226" id="Seg_2768" s="T225">side-ILL</ta>
            <ta e="T227" id="Seg_2769" s="T226">lie.down-IMP.2SG.S</ta>
            <ta e="T229" id="Seg_2770" s="T228">this.[NOM]</ta>
            <ta e="T230" id="Seg_2771" s="T229">wild.animal.[NOM]</ta>
            <ta e="T231" id="Seg_2772" s="T230">side-ILL</ta>
            <ta e="T232" id="Seg_2773" s="T231">lie.down-FUT.[3SG.S]</ta>
            <ta e="T234" id="Seg_2774" s="T233">Kamacha.[NOM]</ta>
            <ta e="T235" id="Seg_2775" s="T234">knife-ACC</ta>
            <ta e="T236" id="Seg_2776" s="T235">sharp</ta>
            <ta e="T335" id="Seg_2777" s="T236">sharpen-FUT-3SG.O</ta>
            <ta e="T237" id="Seg_2778" s="T335">now</ta>
            <ta e="T238" id="Seg_2779" s="T237">wild.animal-GEN</ta>
            <ta e="T239" id="Seg_2780" s="T238">stomach-EP-3SG</ta>
            <ta e="T240" id="Seg_2781" s="T239">rip.up-3SG.O</ta>
            <ta e="T241" id="Seg_2782" s="T240">wild.animal.[NOM]</ta>
            <ta e="T242" id="Seg_2783" s="T241">here</ta>
            <ta e="T243" id="Seg_2784" s="T242">die-FUT.[3SG.S]</ta>
            <ta e="T336" id="Seg_2785" s="T244">Kamacha.[NOM]</ta>
            <ta e="T245" id="Seg_2786" s="T336">now</ta>
            <ta e="T246" id="Seg_2787" s="T245">pull.off-FUT-3SG.O</ta>
            <ta e="T247" id="Seg_2788" s="T246">meat-ACC-3SG</ta>
            <ta e="T248" id="Seg_2789" s="T247">all</ta>
            <ta e="T249" id="Seg_2790" s="T248">away</ta>
            <ta e="T250" id="Seg_2791" s="T249">take.away-FUT-3SG.O</ta>
            <ta e="T252" id="Seg_2792" s="T251">then</ta>
            <ta e="T253" id="Seg_2793" s="T252">all</ta>
            <ta e="T254" id="Seg_2794" s="T253">next</ta>
            <ta e="T255" id="Seg_2795" s="T254">day.[NOM]</ta>
            <ta e="T256" id="Seg_2796" s="T255">down</ta>
            <ta e="T257" id="Seg_2797" s="T256">all</ta>
            <ta e="T258" id="Seg_2798" s="T337">knapsack-ADJZ</ta>
            <ta e="T259" id="Seg_2799" s="T258">bag-ILL.3SG</ta>
            <ta e="T260" id="Seg_2800" s="T259">put-FUT-3SG.O</ta>
            <ta e="T261" id="Seg_2801" s="T260">fullness-ADJZ</ta>
            <ta e="T262" id="Seg_2802" s="T261">bag-ILL.3SG</ta>
            <ta e="T263" id="Seg_2803" s="T262">put-FUT-3SG.O</ta>
            <ta e="T264" id="Seg_2804" s="T263">and</ta>
            <ta e="T265" id="Seg_2805" s="T264">home</ta>
            <ta e="T266" id="Seg_2806" s="T265">leave-PFV-FUT.[3SG.S]</ta>
            <ta e="T268" id="Seg_2807" s="T267">home</ta>
            <ta e="T269" id="Seg_2808" s="T268">reach-IPFV2.[3SG.S]</ta>
            <ta e="T270" id="Seg_2809" s="T269">smoke.[NOM]</ta>
            <ta e="T271" id="Seg_2810" s="T270">NEG.EX-CO.[3SG.S]</ta>
            <ta e="T273" id="Seg_2811" s="T272">house.[NOM]</ta>
            <ta e="T274" id="Seg_2812" s="T273">top.[NOM]</ta>
            <ta e="T275" id="Seg_2813" s="T274">up</ta>
            <ta e="T276" id="Seg_2814" s="T275">go.out-FUT.[3SG.S]</ta>
            <ta e="T277" id="Seg_2815" s="T276">%%</ta>
            <ta e="T278" id="Seg_2816" s="T277">top.[NOM]</ta>
            <ta e="T279" id="Seg_2817" s="T278">down</ta>
            <ta e="T280" id="Seg_2818" s="T279">look-PST.NAR.[3SG.S]</ta>
            <ta e="T282" id="Seg_2819" s="T281">brother.[NOM]-3SG</ta>
            <ta e="T283" id="Seg_2820" s="T282">daughter_in_law-3SG</ta>
            <ta e="T284" id="Seg_2821" s="T283">one</ta>
            <ta e="T285" id="Seg_2822" s="T284">fire-GEN</ta>
            <ta e="T286" id="Seg_2823" s="T285">eye-ACC</ta>
            <ta e="T287" id="Seg_2824" s="T286">in.two</ta>
            <ta e="T288" id="Seg_2825" s="T287">split-EP-IPFV2-3PL</ta>
            <ta e="T290" id="Seg_2826" s="T289">this.[NOM]</ta>
            <ta e="T291" id="Seg_2827" s="T290">I.GEN</ta>
            <ta e="T339" id="Seg_2828" s="T291">Kamacha-1SG</ta>
            <ta e="T294" id="Seg_2829" s="T293">Kamacha.[NOM]</ta>
            <ta e="T295" id="Seg_2830" s="T294">there-ADV.LOC</ta>
            <ta e="T296" id="Seg_2831" s="T295">cry-FUT.[3SG.S]</ta>
            <ta e="T297" id="Seg_2832" s="T296">I.NOM</ta>
            <ta e="T298" id="Seg_2833" s="T297">there-be-CO-1SG.S</ta>
            <ta e="T300" id="Seg_2834" s="T299">Kamacha.[NOM]</ta>
            <ta e="T301" id="Seg_2835" s="T300">(s)he-EP-DU-LOC</ta>
            <ta e="T302" id="Seg_2836" s="T301">fish.[NOM]</ta>
            <ta e="T303" id="Seg_2837" s="T302">meat-ACC</ta>
            <ta e="T304" id="Seg_2838" s="T303">down</ta>
            <ta e="T305" id="Seg_2839" s="T304">come-TR-FUT.[3SG.S]</ta>
            <ta e="T307" id="Seg_2840" s="T306">eat-EP-TR-FUT-3SG.O</ta>
            <ta e="T308" id="Seg_2841" s="T307">fish-INSTR</ta>
            <ta e="T309" id="Seg_2842" s="T308">meat-INSTR</ta>
            <ta e="T311" id="Seg_2843" s="T310">but</ta>
            <ta e="T312" id="Seg_2844" s="T311">then</ta>
            <ta e="T313" id="Seg_2845" s="T312">live-INCH-HAB-CO-3PL</ta>
            <ta e="T314" id="Seg_2846" s="T313">live-INCH-HAB-CO-3PL</ta>
            <ta e="T316" id="Seg_2847" s="T315">daughter-ACC</ta>
            <ta e="T317" id="Seg_2848" s="T316">son-ACC</ta>
            <ta e="T318" id="Seg_2849" s="T317">find-IPFV-HAB-CO-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T319" id="Seg_2850" s="T1">два</ta>
            <ta e="T2" id="Seg_2851" s="T319">брат-DYA-DU</ta>
            <ta e="T320" id="Seg_2852" s="T2">быть-HAB-INFER-3DU.S</ta>
            <ta e="T6" id="Seg_2853" s="T4">жить-HAB-INFER-3DU.S</ta>
            <ta e="T8" id="Seg_2854" s="T7">большой</ta>
            <ta e="T9" id="Seg_2855" s="T8">брат.[NOM]-3SG</ta>
            <ta e="T10" id="Seg_2856" s="T9">жениться-DUR-CVB</ta>
            <ta e="T11" id="Seg_2857" s="T10">жить-HAB-CO.[3SG.S]</ta>
            <ta e="T13" id="Seg_2858" s="T12">маленький</ta>
            <ta e="T14" id="Seg_2859" s="T13">брат.[NOM]-3SG</ta>
            <ta e="T15" id="Seg_2860" s="T14">позвать-EP-3SG.O</ta>
            <ta e="T16" id="Seg_2861" s="T15">Камача.[NOM]</ta>
            <ta e="T3" id="Seg_2862" s="T17">Камача-GEN</ta>
            <ta e="T18" id="Seg_2863" s="T3">с</ta>
            <ta e="T19" id="Seg_2864" s="T18">идти-HAB-PST-3DU.S</ta>
            <ta e="T20" id="Seg_2865" s="T19">слопец-EP-ADJZ</ta>
            <ta e="T21" id="Seg_2866" s="T20">путь-PROL</ta>
            <ta e="T23" id="Seg_2867" s="T22">а</ta>
            <ta e="T24" id="Seg_2868" s="T23">он(а)-EP-ADES</ta>
            <ta e="T25" id="Seg_2869" s="T24">большой</ta>
            <ta e="T26" id="Seg_2870" s="T25">брат-GEN-3SG-ADES</ta>
            <ta e="T27" id="Seg_2871" s="T26">невеста.[NOM]-3SG</ta>
            <ta e="T28" id="Seg_2872" s="T27">позвать-HAB-CO-3PL</ta>
            <ta e="T29" id="Seg_2873" s="T28">сноха.[NOM]</ta>
            <ta e="T31" id="Seg_2874" s="T30">теперь</ta>
            <ta e="T32" id="Seg_2875" s="T31">один</ta>
            <ta e="T33" id="Seg_2876" s="T32">год-EP-3SG.O</ta>
            <ta e="T34" id="Seg_2877" s="T33">пойти-CO-3DU.S</ta>
            <ta e="T35" id="Seg_2878" s="T34">слопец-EP-ADJZ</ta>
            <ta e="T36" id="Seg_2879" s="T35">путь-ILL.3SG</ta>
            <ta e="T37" id="Seg_2880" s="T36">а</ta>
            <ta e="T38" id="Seg_2881" s="T37">Камача.[NOM]</ta>
            <ta e="T39" id="Seg_2882" s="T38">кричать-CO.[3SG.S]</ta>
            <ta e="T40" id="Seg_2883" s="T39">лететь-IPFV2.[3SG.S]</ta>
            <ta e="T41" id="Seg_2884" s="T40">глухарь.[NOM]</ta>
            <ta e="T5" id="Seg_2885" s="T42">а</ta>
            <ta e="T43" id="Seg_2886" s="T5">Камача.[NOM]</ta>
            <ta e="T44" id="Seg_2887" s="T43">кричать-CO.[3SG.S]</ta>
            <ta e="T45" id="Seg_2888" s="T44">глухарь-VOC</ta>
            <ta e="T46" id="Seg_2889" s="T45">я.ALL</ta>
            <ta e="T47" id="Seg_2890" s="T46">%%-ADJZ-%%-ADJZ</ta>
            <ta e="T48" id="Seg_2891" s="T47">оставить-IMP.2SG.S</ta>
            <ta e="T49" id="Seg_2892" s="T115">а</ta>
            <ta e="T50" id="Seg_2893" s="T49">большой</ta>
            <ta e="T51" id="Seg_2894" s="T50">брат.[NOM]-3SG</ta>
            <ta e="T52" id="Seg_2895" s="T51">Камача-GEN</ta>
            <ta e="T53" id="Seg_2896" s="T52">спрашивать.[3SG.S]</ta>
            <ta e="T54" id="Seg_2897" s="T53">что-TRL</ta>
            <ta e="T55" id="Seg_2898" s="T54">такой-ADVZ</ta>
            <ta e="T56" id="Seg_2899" s="T55">кричать-CO-2SG.S</ta>
            <ta e="T58" id="Seg_2900" s="T57">а</ta>
            <ta e="T59" id="Seg_2901" s="T58">я.NOM</ta>
            <ta e="T60" id="Seg_2902" s="T59">потому</ta>
            <ta e="T61" id="Seg_2903" s="T60">такой-ADVZ</ta>
            <ta e="T62" id="Seg_2904" s="T61">кричать-CO-1SG.S</ta>
            <ta e="T63" id="Seg_2905" s="T62">я.ACC</ta>
            <ta e="T326" id="Seg_2906" s="T63">сноха-1SG</ta>
            <ta e="T65" id="Seg_2907" s="T64">%%-INSTR-%%-INSTR</ta>
            <ta e="T66" id="Seg_2908" s="T65">есть-EP-TR-HAB-CO.[3SG.S]</ta>
            <ta e="T68" id="Seg_2909" s="T67">туда-ADJZ</ta>
            <ta e="T69" id="Seg_2910" s="T68">идти-PST-3DU.S</ta>
            <ta e="T70" id="Seg_2911" s="T69">назад</ta>
            <ta e="T71" id="Seg_2912" s="T70">прийти-RES-3DU.S</ta>
            <ta e="T73" id="Seg_2913" s="T72">теперь</ta>
            <ta e="T74" id="Seg_2914" s="T73">большой</ta>
            <ta e="T78" id="Seg_2915" s="T74">брат-3SG</ta>
            <ta e="T76" id="Seg_2916" s="T75">поймать-%%-FUT-3SG.O</ta>
            <ta e="T327" id="Seg_2917" s="T76">wife-ACC-3SG</ta>
            <ta e="T79" id="Seg_2918" s="T77">сюда</ta>
            <ta e="T80" id="Seg_2919" s="T79">поймать-CVB</ta>
            <ta e="T81" id="Seg_2920" s="T80">убить-%%-FUT-3SG.O</ta>
            <ta e="T328" id="Seg_2921" s="T82">теперь</ta>
            <ta e="T83" id="Seg_2922" s="T328">а</ta>
            <ta e="T84" id="Seg_2923" s="T83">завтра</ta>
            <ta e="T85" id="Seg_2924" s="T84">сам.3SG</ta>
            <ta e="T86" id="Seg_2925" s="T85">одиноко</ta>
            <ta e="T87" id="Seg_2926" s="T86">пойти-FUT.[3SG.S]</ta>
            <ta e="T88" id="Seg_2927" s="T87">большой</ta>
            <ta e="T89" id="Seg_2928" s="T88">брат.[NOM]-3SG</ta>
            <ta e="T90" id="Seg_2929" s="T89">слопец-EP-ADJZ</ta>
            <ta e="T91" id="Seg_2930" s="T90">путь-ILL.3SG</ta>
            <ta e="T93" id="Seg_2931" s="T92">а</ta>
            <ta e="T94" id="Seg_2932" s="T93">сноха.[NOM]-3SG</ta>
            <ta e="T95" id="Seg_2933" s="T94">поймать-CVB</ta>
            <ta e="T96" id="Seg_2934" s="T95">Камача-ACC</ta>
            <ta e="T97" id="Seg_2935" s="T96">убить-%%-FUT-3SG.O</ta>
            <ta e="T99" id="Seg_2936" s="T98">Камача.[NOM]</ta>
            <ta e="T100" id="Seg_2937" s="T99">дом-EL2</ta>
            <ta e="T101" id="Seg_2938" s="T100">пространство.снаружи-ILL2</ta>
            <ta e="T102" id="Seg_2939" s="T101">выходить-CVB</ta>
            <ta e="T103" id="Seg_2940" s="T102">пойти-FUT.[3SG.S]</ta>
            <ta e="T105" id="Seg_2941" s="T104">пойти-FUT.[3SG.S]</ta>
            <ta e="T106" id="Seg_2942" s="T105">куда</ta>
            <ta e="T107" id="Seg_2943" s="T106">глаз.[NOM]-3SG</ta>
            <ta e="T108" id="Seg_2944" s="T107">видеть.[3SG.S]</ta>
            <ta e="T110" id="Seg_2945" s="T109">ехать.[3SG.S]</ta>
            <ta e="T111" id="Seg_2946" s="T110">видеть-CO-3SG.O</ta>
            <ta e="T112" id="Seg_2947" s="T111">ворон.[NOM]</ta>
            <ta e="T113" id="Seg_2948" s="T112">лететь-IPFV2-INFER.[3SG.S]</ta>
            <ta e="T116" id="Seg_2949" s="T114">рот-EP-ILL</ta>
            <ta e="T117" id="Seg_2950" s="T116">принести-FRQ-EP-3SG.O</ta>
            <ta e="T118" id="Seg_2951" s="T117">рыба.[NOM]</ta>
            <ta e="T120" id="Seg_2952" s="T119">он(а).[NOM]</ta>
            <ta e="T121" id="Seg_2953" s="T120">Камача.[NOM]</ta>
            <ta e="T122" id="Seg_2954" s="T121">этот</ta>
            <ta e="T123" id="Seg_2955" s="T122">ворон.[NOM]-2SG</ta>
            <ta e="T124" id="Seg_2956" s="T123">что-PROL</ta>
            <ta e="T125" id="Seg_2957" s="T124">прийти-IPFV2.[3SG.S]</ta>
            <ta e="T126" id="Seg_2958" s="T125">туда-ADV.ILL</ta>
            <ta e="T127" id="Seg_2959" s="T126">пойти-FUT.[3SG.S]</ta>
            <ta e="T129" id="Seg_2960" s="T128">бегать-FUT.[3SG.S]</ta>
            <ta e="T130" id="Seg_2961" s="T129">достичь-FUT.[3SG.S]</ta>
            <ta e="T131" id="Seg_2962" s="T130">река.[NOM]</ta>
            <ta e="T133" id="Seg_2963" s="T132">этот</ta>
            <ta e="T134" id="Seg_2964" s="T133">река-LOC</ta>
            <ta e="T135" id="Seg_2965" s="T134">такой-ADVZ</ta>
            <ta e="T136" id="Seg_2966" s="T135">рыба.[NOM]</ta>
            <ta e="T137" id="Seg_2967" s="T136">много-CO.[3SG.S]</ta>
            <ta e="T329" id="Seg_2968" s="T324">он(а).[NOM]</ta>
            <ta e="T330" id="Seg_2969" s="T323">срубить-EP-PFV-FUT-3SG.O</ta>
            <ta e="T139" id="Seg_2970" s="T138">он(а).[NOM]</ta>
            <ta e="T140" id="Seg_2971" s="T139">срубить-EP-PFV-FUT-3SG.O</ta>
            <ta e="T141" id="Seg_2972" s="T140">черёмуха-ACC</ta>
            <ta e="T142" id="Seg_2973" s="T141">и</ta>
            <ta e="T143" id="Seg_2974" s="T142">‎‎сачок-ACC</ta>
            <ta e="T144" id="Seg_2975" s="T143">делать-PFV-FUT-3SG.O</ta>
            <ta e="T146" id="Seg_2976" s="T145">INCH</ta>
            <ta e="T147" id="Seg_2977" s="T146">рыба-EP-ACC</ta>
            <ta e="T148" id="Seg_2978" s="T147">черпать-EP-IPFV2-INF</ta>
            <ta e="T150" id="Seg_2979" s="T149">черпать-3SG.O</ta>
            <ta e="T151" id="Seg_2980" s="T150">рыба-ACC</ta>
            <ta e="T152" id="Seg_2981" s="T151">огонь-ACC</ta>
            <ta e="T153" id="Seg_2982" s="T152">поджигать-FUT.[3SG.S]</ta>
            <ta e="T155" id="Seg_2983" s="T154">на.огонь</ta>
            <ta e="T156" id="Seg_2984" s="T155">рыба-ACC</ta>
            <ta e="T157" id="Seg_2985" s="T156">шампур-%%</ta>
            <ta e="T158" id="Seg_2986" s="T157">класть-PFV-HAB-FUT-3SG.O</ta>
            <ta e="T160" id="Seg_2987" s="T159">вдруг</ta>
            <ta e="T161" id="Seg_2988" s="T160">смотреть-PFV-FUT.[3SG.S]</ta>
            <ta e="T162" id="Seg_2989" s="T161">зверь.[NOM]</ta>
            <ta e="T163" id="Seg_2990" s="T162">прийти-IPFV2-INFER.[3SG.S]</ta>
            <ta e="T165" id="Seg_2991" s="T164">зверь.[NOM]</ta>
            <ta e="T166" id="Seg_2992" s="T165">прийти-FUT.[3SG.S]</ta>
            <ta e="T167" id="Seg_2993" s="T166">спрашивать-FUT.[3SG.S]</ta>
            <ta e="T168" id="Seg_2994" s="T167">Камача-ILL2</ta>
            <ta e="T169" id="Seg_2995" s="T168">что.[NOM]</ta>
            <ta e="T170" id="Seg_2996" s="T169">делать-IPFV2-INFER-%%</ta>
            <ta e="T172" id="Seg_2997" s="T171">я.NOM</ta>
            <ta e="T173" id="Seg_2998" s="T172">почка-1SG</ta>
            <ta e="T174" id="Seg_2999" s="T173">на.огонь</ta>
            <ta e="T175" id="Seg_3000" s="T174">поджарить-EP-IPFV2-INFER-1SG.S</ta>
            <ta e="T331" id="Seg_3001" s="T176">зверь.[NOM]</ta>
            <ta e="T177" id="Seg_3002" s="T331">ну</ta>
            <ta e="T178" id="Seg_3003" s="T177">напротив</ta>
            <ta e="T179" id="Seg_3004" s="T178">сесть-IMP.2SG.S</ta>
            <ta e="T180" id="Seg_3005" s="T179">огонь.[NOM]</ta>
            <ta e="T181" id="Seg_3006" s="T180">напротив</ta>
            <ta e="T183" id="Seg_3007" s="T182">зверь.[NOM]</ta>
            <ta e="T184" id="Seg_3008" s="T183">огонь.[NOM]</ta>
            <ta e="T185" id="Seg_3009" s="T184">напротив</ta>
            <ta e="T186" id="Seg_3010" s="T185">сесть-FUT.[3SG.S]</ta>
            <ta e="T188" id="Seg_3011" s="T187">Камача.[NOM]</ta>
            <ta e="T189" id="Seg_3012" s="T188">я.ALL</ta>
            <ta e="T190" id="Seg_3013" s="T189">тоже</ta>
            <ta e="T191" id="Seg_3014" s="T190">делать-OPT.[3SG.S]</ta>
            <ta e="T193" id="Seg_3015" s="T192">теперь</ta>
            <ta e="T194" id="Seg_3016" s="T193">шампур.[NOM]</ta>
            <ta e="T195" id="Seg_3017" s="T194">вариться-FUT.[3SG.S]</ta>
            <ta e="T196" id="Seg_3018" s="T195">он(а).[NOM]</ta>
            <ta e="T197" id="Seg_3019" s="T196">один</ta>
            <ta e="T198" id="Seg_3020" s="T197">шампур.[NOM]</ta>
            <ta e="T199" id="Seg_3021" s="T198">зверь-ALL</ta>
            <ta e="T200" id="Seg_3022" s="T199">бросать-FUT-3SG.O</ta>
            <ta e="T202" id="Seg_3023" s="T201">зверь.[NOM]</ta>
            <ta e="T203" id="Seg_3024" s="T202">есть-EP-FUT-3SG.O</ta>
            <ta e="T204" id="Seg_3025" s="T203">о</ta>
            <ta e="T205" id="Seg_3026" s="T204">хороший</ta>
            <ta e="T333" id="Seg_3027" s="T205">нечто.[NOM]</ta>
            <ta e="T207" id="Seg_3028" s="T332">HORT</ta>
            <ta e="T208" id="Seg_3029" s="T207">я.GEN</ta>
            <ta e="T209" id="Seg_3030" s="T208">почка.[NOM]</ta>
            <ta e="T210" id="Seg_3031" s="T209">тоже</ta>
            <ta e="T211" id="Seg_3032" s="T210">прочь</ta>
            <ta e="T212" id="Seg_3033" s="T211">вырыть-IMP.2SG.S</ta>
            <ta e="T214" id="Seg_3034" s="T213">Камача.[NOM]</ta>
            <ta e="T215" id="Seg_3035" s="T214">сказать-HAB-CO.[3SG.S]</ta>
            <ta e="T216" id="Seg_3036" s="T215">ты.NOM</ta>
            <ta e="T217" id="Seg_3037" s="T216">я.ACC</ta>
            <ta e="T218" id="Seg_3038" s="T217">NEG</ta>
            <ta e="T219" id="Seg_3039" s="T218">есть-FUT-2SG.S</ta>
            <ta e="T221" id="Seg_3040" s="T220">NEG</ta>
            <ta e="T222" id="Seg_3041" s="T221">есть-FUT-1SG.S</ta>
            <ta e="T224" id="Seg_3042" s="T223">теперь</ta>
            <ta e="T334" id="Seg_3043" s="T224">Камача.[NOM]</ta>
            <ta e="T225" id="Seg_3044" s="T334">но</ta>
            <ta e="T226" id="Seg_3045" s="T225">сторона-ILL</ta>
            <ta e="T227" id="Seg_3046" s="T226">лечь-IMP.2SG.S</ta>
            <ta e="T229" id="Seg_3047" s="T228">этот.[NOM]</ta>
            <ta e="T230" id="Seg_3048" s="T229">зверь.[NOM]</ta>
            <ta e="T231" id="Seg_3049" s="T230">сторона-ILL</ta>
            <ta e="T232" id="Seg_3050" s="T231">лечь-FUT.[3SG.S]</ta>
            <ta e="T234" id="Seg_3051" s="T233">Камача.[NOM]</ta>
            <ta e="T235" id="Seg_3052" s="T234">нож-ACC</ta>
            <ta e="T236" id="Seg_3053" s="T235">острый</ta>
            <ta e="T335" id="Seg_3054" s="T236">наточить-FUT-3SG.O</ta>
            <ta e="T237" id="Seg_3055" s="T335">теперь</ta>
            <ta e="T238" id="Seg_3056" s="T237">зверь-GEN</ta>
            <ta e="T239" id="Seg_3057" s="T238">живот-EP-3SG</ta>
            <ta e="T240" id="Seg_3058" s="T239">распороть-3SG.O</ta>
            <ta e="T241" id="Seg_3059" s="T240">зверь.[NOM]</ta>
            <ta e="T242" id="Seg_3060" s="T241">сюда</ta>
            <ta e="T243" id="Seg_3061" s="T242">умирать-FUT.[3SG.S]</ta>
            <ta e="T336" id="Seg_3062" s="T244">Камача.[NOM]</ta>
            <ta e="T245" id="Seg_3063" s="T336">теперь</ta>
            <ta e="T246" id="Seg_3064" s="T245">ободрать-FUT-3SG.O</ta>
            <ta e="T247" id="Seg_3065" s="T246">мясо-ACC-3SG</ta>
            <ta e="T248" id="Seg_3066" s="T247">весь</ta>
            <ta e="T249" id="Seg_3067" s="T248">прочь</ta>
            <ta e="T250" id="Seg_3068" s="T249">убирать-FUT-3SG.O</ta>
            <ta e="T252" id="Seg_3069" s="T251">потом</ta>
            <ta e="T253" id="Seg_3070" s="T252">весь</ta>
            <ta e="T254" id="Seg_3071" s="T253">следующий</ta>
            <ta e="T255" id="Seg_3072" s="T254">день.[NOM]</ta>
            <ta e="T256" id="Seg_3073" s="T255">вниз</ta>
            <ta e="T257" id="Seg_3074" s="T256">весь</ta>
            <ta e="T258" id="Seg_3075" s="T337">котомка-ADJZ</ta>
            <ta e="T259" id="Seg_3076" s="T258">мешок-ILL.3SG</ta>
            <ta e="T260" id="Seg_3077" s="T259">положить-FUT-3SG.O</ta>
            <ta e="T261" id="Seg_3078" s="T260">полнота-ADJZ</ta>
            <ta e="T262" id="Seg_3079" s="T261">мешок-ILL.3SG</ta>
            <ta e="T263" id="Seg_3080" s="T262">положить-FUT-3SG.O</ta>
            <ta e="T264" id="Seg_3081" s="T263">и</ta>
            <ta e="T265" id="Seg_3082" s="T264">домой</ta>
            <ta e="T266" id="Seg_3083" s="T265">пойти-PFV-FUT.[3SG.S]</ta>
            <ta e="T268" id="Seg_3084" s="T267">домой</ta>
            <ta e="T269" id="Seg_3085" s="T268">достичь-IPFV2.[3SG.S]</ta>
            <ta e="T270" id="Seg_3086" s="T269">дым.[NOM]</ta>
            <ta e="T271" id="Seg_3087" s="T270">NEG.EX-CO.[3SG.S]</ta>
            <ta e="T273" id="Seg_3088" s="T272">дом.[NOM]</ta>
            <ta e="T274" id="Seg_3089" s="T273">верхняя.часть.[NOM]</ta>
            <ta e="T275" id="Seg_3090" s="T274">вверх</ta>
            <ta e="T276" id="Seg_3091" s="T275">выходить-FUT.[3SG.S]</ta>
            <ta e="T277" id="Seg_3092" s="T276">%%</ta>
            <ta e="T278" id="Seg_3093" s="T277">верхняя.часть.[NOM]</ta>
            <ta e="T279" id="Seg_3094" s="T278">вниз</ta>
            <ta e="T280" id="Seg_3095" s="T279">смотреть-PST.NAR.[3SG.S]</ta>
            <ta e="T282" id="Seg_3096" s="T281">брат.[NOM]-3SG</ta>
            <ta e="T283" id="Seg_3097" s="T282">сноха-3SG</ta>
            <ta e="T284" id="Seg_3098" s="T283">один</ta>
            <ta e="T285" id="Seg_3099" s="T284">огонь-GEN</ta>
            <ta e="T286" id="Seg_3100" s="T285">глаз-ACC</ta>
            <ta e="T287" id="Seg_3101" s="T286">пополам</ta>
            <ta e="T288" id="Seg_3102" s="T287">поделить-EP-IPFV2-3PL</ta>
            <ta e="T290" id="Seg_3103" s="T289">этот.[NOM]</ta>
            <ta e="T291" id="Seg_3104" s="T290">я.GEN</ta>
            <ta e="T339" id="Seg_3105" s="T291">Камача-1SG</ta>
            <ta e="T294" id="Seg_3106" s="T293">Камача.[NOM]</ta>
            <ta e="T295" id="Seg_3107" s="T294">туда-ADV.LOC</ta>
            <ta e="T296" id="Seg_3108" s="T295">кричать-FUT.[3SG.S]</ta>
            <ta e="T297" id="Seg_3109" s="T296">я.NOM</ta>
            <ta e="T298" id="Seg_3110" s="T297">там-быть-CO-1SG.S</ta>
            <ta e="T300" id="Seg_3111" s="T299">Камача.[NOM]</ta>
            <ta e="T301" id="Seg_3112" s="T300">он(а)-EP-DU-LOC</ta>
            <ta e="T302" id="Seg_3113" s="T301">рыба.[NOM]</ta>
            <ta e="T303" id="Seg_3114" s="T302">мясо-ACC</ta>
            <ta e="T304" id="Seg_3115" s="T303">вниз</ta>
            <ta e="T305" id="Seg_3116" s="T304">прийти-TR-FUT.[3SG.S]</ta>
            <ta e="T307" id="Seg_3117" s="T306">есть-EP-TR-FUT-3SG.O</ta>
            <ta e="T308" id="Seg_3118" s="T307">рыба-INSTR</ta>
            <ta e="T309" id="Seg_3119" s="T308">мясо-INSTR</ta>
            <ta e="T311" id="Seg_3120" s="T310">а</ta>
            <ta e="T312" id="Seg_3121" s="T311">потом</ta>
            <ta e="T313" id="Seg_3122" s="T312">жить-INCH-HAB-CO-3PL</ta>
            <ta e="T314" id="Seg_3123" s="T313">жить-INCH-HAB-CO-3PL</ta>
            <ta e="T316" id="Seg_3124" s="T315">дочь-ACC</ta>
            <ta e="T317" id="Seg_3125" s="T316">сын-ACC</ta>
            <ta e="T318" id="Seg_3126" s="T317">найти-IPFV-HAB-CO-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T319" id="Seg_3127" s="T1">num</ta>
            <ta e="T2" id="Seg_3128" s="T319">n-n&gt;n-n:num</ta>
            <ta e="T320" id="Seg_3129" s="T2">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T6" id="Seg_3130" s="T4">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T8" id="Seg_3131" s="T7">adj</ta>
            <ta e="T9" id="Seg_3132" s="T8">n.[n:case]-n:poss</ta>
            <ta e="T10" id="Seg_3133" s="T9">v-v&gt;v-v&gt;adv</ta>
            <ta e="T11" id="Seg_3134" s="T10">v-v&gt;v-v:ins.[v:pn]</ta>
            <ta e="T13" id="Seg_3135" s="T12">adj</ta>
            <ta e="T14" id="Seg_3136" s="T13">n.[n:case]-n:poss</ta>
            <ta e="T15" id="Seg_3137" s="T14">v-n:ins-v:pn</ta>
            <ta e="T16" id="Seg_3138" s="T15">nprop.[n:case]</ta>
            <ta e="T3" id="Seg_3139" s="T17">nprop-n:case</ta>
            <ta e="T18" id="Seg_3140" s="T3">pp</ta>
            <ta e="T19" id="Seg_3141" s="T18">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T20" id="Seg_3142" s="T19">n-n:ins-n&gt;adj</ta>
            <ta e="T21" id="Seg_3143" s="T20">n-n:case</ta>
            <ta e="T23" id="Seg_3144" s="T22">conj</ta>
            <ta e="T24" id="Seg_3145" s="T23">pers-n:ins-n:case</ta>
            <ta e="T25" id="Seg_3146" s="T24">adj</ta>
            <ta e="T26" id="Seg_3147" s="T25">n-n:case-n:poss-n:case</ta>
            <ta e="T27" id="Seg_3148" s="T26">n.[n:case]-n:poss</ta>
            <ta e="T28" id="Seg_3149" s="T27">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T29" id="Seg_3150" s="T28">n.[n:case]</ta>
            <ta e="T31" id="Seg_3151" s="T30">adv</ta>
            <ta e="T32" id="Seg_3152" s="T31">num</ta>
            <ta e="T33" id="Seg_3153" s="T32">n-n:ins-v:pn</ta>
            <ta e="T34" id="Seg_3154" s="T33">v-v:ins-v:pn</ta>
            <ta e="T35" id="Seg_3155" s="T34">n-n:ins-n&gt;adj</ta>
            <ta e="T36" id="Seg_3156" s="T35">n-n:case.poss</ta>
            <ta e="T37" id="Seg_3157" s="T36">conj</ta>
            <ta e="T38" id="Seg_3158" s="T37">nprop.[n:case]</ta>
            <ta e="T39" id="Seg_3159" s="T38">v-v:ins.[v:pn]</ta>
            <ta e="T40" id="Seg_3160" s="T39">v-v&gt;v.[v:pn]</ta>
            <ta e="T41" id="Seg_3161" s="T40">n.[n:case]</ta>
            <ta e="T5" id="Seg_3162" s="T42">conj</ta>
            <ta e="T43" id="Seg_3163" s="T5">nprop.[n:case]</ta>
            <ta e="T44" id="Seg_3164" s="T43">v-v:ins.[v:pn]</ta>
            <ta e="T45" id="Seg_3165" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_3166" s="T45">pers</ta>
            <ta e="T47" id="Seg_3167" s="T46">n-n&gt;adj-n-n&gt;adj</ta>
            <ta e="T48" id="Seg_3168" s="T47">v-v:mood.pn</ta>
            <ta e="T49" id="Seg_3169" s="T115">conj</ta>
            <ta e="T50" id="Seg_3170" s="T49">adj</ta>
            <ta e="T51" id="Seg_3171" s="T50">n.[n:case]-n:poss</ta>
            <ta e="T52" id="Seg_3172" s="T51">nprop-n:case</ta>
            <ta e="T53" id="Seg_3173" s="T52">v.[v:pn]</ta>
            <ta e="T54" id="Seg_3174" s="T53">interrog-n:case</ta>
            <ta e="T55" id="Seg_3175" s="T54">dem-adj&gt;adv</ta>
            <ta e="T56" id="Seg_3176" s="T55">v-v:ins-v:pn</ta>
            <ta e="T58" id="Seg_3177" s="T57">conj</ta>
            <ta e="T59" id="Seg_3178" s="T58">pers</ta>
            <ta e="T60" id="Seg_3179" s="T59">conj</ta>
            <ta e="T61" id="Seg_3180" s="T60">dem-adj&gt;adv</ta>
            <ta e="T62" id="Seg_3181" s="T61">v-v:ins-v:pn</ta>
            <ta e="T63" id="Seg_3182" s="T62">pers</ta>
            <ta e="T326" id="Seg_3183" s="T63">n-n:poss</ta>
            <ta e="T65" id="Seg_3184" s="T64">n-n:case-n-n:case</ta>
            <ta e="T66" id="Seg_3185" s="T65">v-v:ins-v&gt;v-v&gt;v-v:ins.[v:pn]</ta>
            <ta e="T68" id="Seg_3186" s="T67">adv-adv&gt;adj</ta>
            <ta e="T69" id="Seg_3187" s="T68">v-v:tense-v:pn</ta>
            <ta e="T70" id="Seg_3188" s="T69">adv</ta>
            <ta e="T71" id="Seg_3189" s="T70">v-v&gt;v-v:pn</ta>
            <ta e="T73" id="Seg_3190" s="T72">adv</ta>
            <ta e="T74" id="Seg_3191" s="T73">adj</ta>
            <ta e="T78" id="Seg_3192" s="T74">n-n:poss</ta>
            <ta e="T76" id="Seg_3193" s="T75">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T327" id="Seg_3194" s="T76">n-n:case-n:poss</ta>
            <ta e="T79" id="Seg_3195" s="T77">adv</ta>
            <ta e="T80" id="Seg_3196" s="T79">v-v&gt;adv</ta>
            <ta e="T81" id="Seg_3197" s="T80">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T328" id="Seg_3198" s="T82">adv</ta>
            <ta e="T83" id="Seg_3199" s="T328">conj</ta>
            <ta e="T84" id="Seg_3200" s="T83">adv</ta>
            <ta e="T85" id="Seg_3201" s="T84">emphpro</ta>
            <ta e="T86" id="Seg_3202" s="T85">adv</ta>
            <ta e="T87" id="Seg_3203" s="T86">v-v:tense.[v:pn]</ta>
            <ta e="T88" id="Seg_3204" s="T87">adj</ta>
            <ta e="T89" id="Seg_3205" s="T88">n.[n:case]-n:poss</ta>
            <ta e="T90" id="Seg_3206" s="T89">n-n:ins-n&gt;adj</ta>
            <ta e="T91" id="Seg_3207" s="T90">n-n:case.poss</ta>
            <ta e="T93" id="Seg_3208" s="T92">conj</ta>
            <ta e="T94" id="Seg_3209" s="T93">n.[n:case]-n:poss</ta>
            <ta e="T95" id="Seg_3210" s="T94">v-v&gt;adv</ta>
            <ta e="T96" id="Seg_3211" s="T95">nprop-n:case</ta>
            <ta e="T97" id="Seg_3212" s="T96">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T99" id="Seg_3213" s="T98">nprop.[n:case]</ta>
            <ta e="T100" id="Seg_3214" s="T99">n-n:case</ta>
            <ta e="T101" id="Seg_3215" s="T100">n-n:case</ta>
            <ta e="T102" id="Seg_3216" s="T101">v-v&gt;adv</ta>
            <ta e="T103" id="Seg_3217" s="T102">v-v:tense.[v:pn]</ta>
            <ta e="T105" id="Seg_3218" s="T104">v-v:tense.[v:pn]</ta>
            <ta e="T106" id="Seg_3219" s="T105">interrog</ta>
            <ta e="T107" id="Seg_3220" s="T106">n.[n:case]-n:poss</ta>
            <ta e="T108" id="Seg_3221" s="T107">v.[v:pn]</ta>
            <ta e="T110" id="Seg_3222" s="T109">v.[v:pn]</ta>
            <ta e="T111" id="Seg_3223" s="T110">v-v:ins-v:pn</ta>
            <ta e="T112" id="Seg_3224" s="T111">n.[n:case]</ta>
            <ta e="T113" id="Seg_3225" s="T112">v-v&gt;v-v:mood.[v:pn]</ta>
            <ta e="T116" id="Seg_3226" s="T114">n-n:ins-n:case</ta>
            <ta e="T117" id="Seg_3227" s="T116">v-v&gt;v-n:ins-v:pn</ta>
            <ta e="T118" id="Seg_3228" s="T117">n.[n:case]</ta>
            <ta e="T120" id="Seg_3229" s="T119">pers.[n:case]</ta>
            <ta e="T121" id="Seg_3230" s="T120">nprop.[n:case]</ta>
            <ta e="T122" id="Seg_3231" s="T121">dem</ta>
            <ta e="T123" id="Seg_3232" s="T122">n.[n:case]-n:poss</ta>
            <ta e="T124" id="Seg_3233" s="T123">interrog-n:case</ta>
            <ta e="T125" id="Seg_3234" s="T124">v-v&gt;v.[v:pn]</ta>
            <ta e="T126" id="Seg_3235" s="T125">adv-adv:case</ta>
            <ta e="T127" id="Seg_3236" s="T126">v-v:tense.[v:pn]</ta>
            <ta e="T129" id="Seg_3237" s="T128">v-v:tense.[v:pn]</ta>
            <ta e="T130" id="Seg_3238" s="T129">v-v:tense.[v:pn]</ta>
            <ta e="T131" id="Seg_3239" s="T130">n.[n:case]</ta>
            <ta e="T133" id="Seg_3240" s="T132">dem</ta>
            <ta e="T134" id="Seg_3241" s="T133">n-n:case</ta>
            <ta e="T135" id="Seg_3242" s="T134">dem-adj&gt;adv</ta>
            <ta e="T136" id="Seg_3243" s="T135">n.[n:case]</ta>
            <ta e="T137" id="Seg_3244" s="T136">quant-v:ins.[v:pn]</ta>
            <ta e="T329" id="Seg_3245" s="T324">pers.[n:case]</ta>
            <ta e="T330" id="Seg_3246" s="T323">v-v:ins-v&gt;v-v:tense-v:pn</ta>
            <ta e="T139" id="Seg_3247" s="T138">pers.[n:case]</ta>
            <ta e="T140" id="Seg_3248" s="T139">v-v:ins-v&gt;v-v:tense-v:pn</ta>
            <ta e="T141" id="Seg_3249" s="T140">n-n:case</ta>
            <ta e="T142" id="Seg_3250" s="T141">conj</ta>
            <ta e="T143" id="Seg_3251" s="T142">n-n:case</ta>
            <ta e="T144" id="Seg_3252" s="T143">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T146" id="Seg_3253" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_3254" s="T146">n-n:ins-n:case</ta>
            <ta e="T148" id="Seg_3255" s="T147">v-v:ins-v&gt;v-v:inf</ta>
            <ta e="T150" id="Seg_3256" s="T149">v-v:pn</ta>
            <ta e="T151" id="Seg_3257" s="T150">n-n:case</ta>
            <ta e="T152" id="Seg_3258" s="T151">n-n:case</ta>
            <ta e="T153" id="Seg_3259" s="T152">v-v:tense.[v:pn]</ta>
            <ta e="T155" id="Seg_3260" s="T154">adv</ta>
            <ta e="T156" id="Seg_3261" s="T155">n-n:case</ta>
            <ta e="T157" id="Seg_3262" s="T156">n-%%</ta>
            <ta e="T158" id="Seg_3263" s="T157">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T160" id="Seg_3264" s="T159">adv</ta>
            <ta e="T161" id="Seg_3265" s="T160">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T162" id="Seg_3266" s="T161">n.[n:case]</ta>
            <ta e="T163" id="Seg_3267" s="T162">v-v&gt;v-v:mood.[v:pn]</ta>
            <ta e="T165" id="Seg_3268" s="T164">n.[n:case]</ta>
            <ta e="T166" id="Seg_3269" s="T165">v-v:tense.[v:pn]</ta>
            <ta e="T167" id="Seg_3270" s="T166">v-v:tense.[v:pn]</ta>
            <ta e="T168" id="Seg_3271" s="T167">nprop-n:case</ta>
            <ta e="T169" id="Seg_3272" s="T168">interrog-n:case</ta>
            <ta e="T170" id="Seg_3273" s="T169">v-v&gt;v-v:mood-%%</ta>
            <ta e="T172" id="Seg_3274" s="T171">pers</ta>
            <ta e="T173" id="Seg_3275" s="T172">n-n:poss</ta>
            <ta e="T174" id="Seg_3276" s="T173">adv</ta>
            <ta e="T175" id="Seg_3277" s="T174">v-v:ins-v&gt;v-v:mood-v:pn</ta>
            <ta e="T331" id="Seg_3278" s="T176">n.[n:case]</ta>
            <ta e="T177" id="Seg_3279" s="T331">ptcl</ta>
            <ta e="T178" id="Seg_3280" s="T177">adv</ta>
            <ta e="T179" id="Seg_3281" s="T178">v-v:mood.pn</ta>
            <ta e="T180" id="Seg_3282" s="T179">n.[n:case]</ta>
            <ta e="T181" id="Seg_3283" s="T180">pp</ta>
            <ta e="T183" id="Seg_3284" s="T182">n.[n:case]</ta>
            <ta e="T184" id="Seg_3285" s="T183">n.[n:case]</ta>
            <ta e="T185" id="Seg_3286" s="T184">pp</ta>
            <ta e="T186" id="Seg_3287" s="T185">v-v:tense.[v:pn]</ta>
            <ta e="T188" id="Seg_3288" s="T187">nprop.[n:case]</ta>
            <ta e="T189" id="Seg_3289" s="T188">pers</ta>
            <ta e="T190" id="Seg_3290" s="T189">ptcl</ta>
            <ta e="T191" id="Seg_3291" s="T190">v-v:mood.[v:pn]</ta>
            <ta e="T193" id="Seg_3292" s="T192">adv</ta>
            <ta e="T194" id="Seg_3293" s="T193">n.[n:case]</ta>
            <ta e="T195" id="Seg_3294" s="T194">v-v:tense.[v:pn]</ta>
            <ta e="T196" id="Seg_3295" s="T195">pers.[n:case]</ta>
            <ta e="T197" id="Seg_3296" s="T196">num</ta>
            <ta e="T198" id="Seg_3297" s="T197">n.[n:case]</ta>
            <ta e="T199" id="Seg_3298" s="T198">n-n:case</ta>
            <ta e="T200" id="Seg_3299" s="T199">v-v:tense-v:pn</ta>
            <ta e="T202" id="Seg_3300" s="T201">n.[n:case]</ta>
            <ta e="T203" id="Seg_3301" s="T202">v-v:ins-v:tense-v:pn</ta>
            <ta e="T204" id="Seg_3302" s="T203">interj</ta>
            <ta e="T205" id="Seg_3303" s="T204">adj</ta>
            <ta e="T333" id="Seg_3304" s="T205">n.[n:case]</ta>
            <ta e="T207" id="Seg_3305" s="T332">ptcl</ta>
            <ta e="T208" id="Seg_3306" s="T207">pers</ta>
            <ta e="T209" id="Seg_3307" s="T208">n.[n:case]</ta>
            <ta e="T210" id="Seg_3308" s="T209">ptcl</ta>
            <ta e="T211" id="Seg_3309" s="T210">preverb</ta>
            <ta e="T212" id="Seg_3310" s="T211">v-v:mood.pn</ta>
            <ta e="T214" id="Seg_3311" s="T213">nprop.[n:case]</ta>
            <ta e="T215" id="Seg_3312" s="T214">v-v&gt;v-v:ins.[v:pn]</ta>
            <ta e="T216" id="Seg_3313" s="T215">pers</ta>
            <ta e="T217" id="Seg_3314" s="T216">pers</ta>
            <ta e="T218" id="Seg_3315" s="T217">ptcl</ta>
            <ta e="T219" id="Seg_3316" s="T218">v-v:tense-v:pn</ta>
            <ta e="T221" id="Seg_3317" s="T220">ptcl</ta>
            <ta e="T222" id="Seg_3318" s="T221">v-v:tense-v:pn</ta>
            <ta e="T224" id="Seg_3319" s="T223">adv</ta>
            <ta e="T334" id="Seg_3320" s="T224">nprop.[n:case]</ta>
            <ta e="T225" id="Seg_3321" s="T334">ptcl</ta>
            <ta e="T226" id="Seg_3322" s="T225">n-n:case</ta>
            <ta e="T227" id="Seg_3323" s="T226">v-v:mood.pn</ta>
            <ta e="T229" id="Seg_3324" s="T228">dem.[n:case]</ta>
            <ta e="T230" id="Seg_3325" s="T229">n.[n:case]</ta>
            <ta e="T231" id="Seg_3326" s="T230">n-n:case</ta>
            <ta e="T232" id="Seg_3327" s="T231">v-v:tense.[v:pn]</ta>
            <ta e="T234" id="Seg_3328" s="T233">nprop.[n:case]</ta>
            <ta e="T235" id="Seg_3329" s="T234">n-n:case</ta>
            <ta e="T236" id="Seg_3330" s="T235">adj</ta>
            <ta e="T335" id="Seg_3331" s="T236">v-v:tense-v:pn</ta>
            <ta e="T237" id="Seg_3332" s="T335">adv</ta>
            <ta e="T238" id="Seg_3333" s="T237">n-n:case</ta>
            <ta e="T239" id="Seg_3334" s="T238">n-n:ins-n:poss</ta>
            <ta e="T240" id="Seg_3335" s="T239">v-v:pn</ta>
            <ta e="T241" id="Seg_3336" s="T240">n.[n:case]</ta>
            <ta e="T242" id="Seg_3337" s="T241">adv</ta>
            <ta e="T243" id="Seg_3338" s="T242">v-v:tense-.[v:pn]</ta>
            <ta e="T336" id="Seg_3339" s="T244">nprop.[n:case]</ta>
            <ta e="T245" id="Seg_3340" s="T336">adv</ta>
            <ta e="T246" id="Seg_3341" s="T245">v-v&gt;v-v:pn</ta>
            <ta e="T247" id="Seg_3342" s="T246">n-n:case-n:poss</ta>
            <ta e="T248" id="Seg_3343" s="T247">quant</ta>
            <ta e="T249" id="Seg_3344" s="T248">preverb</ta>
            <ta e="T250" id="Seg_3345" s="T249">v-v:tense-v:pn</ta>
            <ta e="T252" id="Seg_3346" s="T251">adv</ta>
            <ta e="T253" id="Seg_3347" s="T252">quant</ta>
            <ta e="T254" id="Seg_3348" s="T253">adj</ta>
            <ta e="T255" id="Seg_3349" s="T254">n.[n:case]</ta>
            <ta e="T256" id="Seg_3350" s="T255">preverb</ta>
            <ta e="T257" id="Seg_3351" s="T256">quant</ta>
            <ta e="T258" id="Seg_3352" s="T337">n-n&gt;adj</ta>
            <ta e="T259" id="Seg_3353" s="T258">n-n:case.poss</ta>
            <ta e="T260" id="Seg_3354" s="T259">v-v:tense-v:pn</ta>
            <ta e="T261" id="Seg_3355" s="T260">n-n&gt;adj</ta>
            <ta e="T262" id="Seg_3356" s="T261">n-n:case.poss</ta>
            <ta e="T263" id="Seg_3357" s="T262">v-v:tense-v:pn</ta>
            <ta e="T264" id="Seg_3358" s="T263">conj</ta>
            <ta e="T265" id="Seg_3359" s="T264">adv</ta>
            <ta e="T266" id="Seg_3360" s="T265">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T268" id="Seg_3361" s="T267">adv</ta>
            <ta e="T269" id="Seg_3362" s="T268">v-v&gt;v.[v:pn]</ta>
            <ta e="T270" id="Seg_3363" s="T269">n.[n:case]</ta>
            <ta e="T271" id="Seg_3364" s="T270">v-v:ins.[v:pn]</ta>
            <ta e="T273" id="Seg_3365" s="T272">n.[n:case]</ta>
            <ta e="T274" id="Seg_3366" s="T273">n</ta>
            <ta e="T275" id="Seg_3367" s="T274">preverb</ta>
            <ta e="T276" id="Seg_3368" s="T275">v-v:tense.[v:pn]</ta>
            <ta e="T277" id="Seg_3369" s="T276">n</ta>
            <ta e="T278" id="Seg_3370" s="T277">n.[n:case]</ta>
            <ta e="T279" id="Seg_3371" s="T278">adv</ta>
            <ta e="T280" id="Seg_3372" s="T279">v-v:tense.[v:pn]</ta>
            <ta e="T282" id="Seg_3373" s="T281">n.[n:case]-n:poss</ta>
            <ta e="T283" id="Seg_3374" s="T282">n-n:poss</ta>
            <ta e="T284" id="Seg_3375" s="T283">num</ta>
            <ta e="T285" id="Seg_3376" s="T284">n-n:case</ta>
            <ta e="T286" id="Seg_3377" s="T285">n-n:case</ta>
            <ta e="T287" id="Seg_3378" s="T286">preverb</ta>
            <ta e="T288" id="Seg_3379" s="T287">v-n:ins-v&gt;v-v:pn</ta>
            <ta e="T290" id="Seg_3380" s="T289">dem.[n:case]</ta>
            <ta e="T291" id="Seg_3381" s="T290">pers</ta>
            <ta e="T339" id="Seg_3382" s="T291">nprop-n:poss</ta>
            <ta e="T294" id="Seg_3383" s="T293">nprop.[n:case]</ta>
            <ta e="T295" id="Seg_3384" s="T294">adv-adv:case</ta>
            <ta e="T296" id="Seg_3385" s="T295">v-v:tense.[v:pn]</ta>
            <ta e="T297" id="Seg_3386" s="T296">pers</ta>
            <ta e="T298" id="Seg_3387" s="T297">adv-v-v:ins-v:pn</ta>
            <ta e="T300" id="Seg_3388" s="T299">nprop.[n:case]</ta>
            <ta e="T301" id="Seg_3389" s="T300">pers-n:ins-n:num-n:case</ta>
            <ta e="T302" id="Seg_3390" s="T301">n.[n:case]</ta>
            <ta e="T303" id="Seg_3391" s="T302">n-n:case</ta>
            <ta e="T304" id="Seg_3392" s="T303">adv</ta>
            <ta e="T305" id="Seg_3393" s="T304">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T307" id="Seg_3394" s="T306">v-v:ins-v&gt;v-v:tense-v:pn</ta>
            <ta e="T308" id="Seg_3395" s="T307">n-n:case</ta>
            <ta e="T309" id="Seg_3396" s="T308">n-n:case</ta>
            <ta e="T311" id="Seg_3397" s="T310">conj</ta>
            <ta e="T312" id="Seg_3398" s="T311">adv</ta>
            <ta e="T313" id="Seg_3399" s="T312">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T314" id="Seg_3400" s="T313">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T316" id="Seg_3401" s="T315">n-n:case</ta>
            <ta e="T317" id="Seg_3402" s="T316">n-n:case</ta>
            <ta e="T318" id="Seg_3403" s="T317">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T319" id="Seg_3404" s="T1">num</ta>
            <ta e="T2" id="Seg_3405" s="T319">n</ta>
            <ta e="T320" id="Seg_3406" s="T2">v</ta>
            <ta e="T6" id="Seg_3407" s="T4">v</ta>
            <ta e="T8" id="Seg_3408" s="T7">adj</ta>
            <ta e="T9" id="Seg_3409" s="T8">n</ta>
            <ta e="T10" id="Seg_3410" s="T9">adv</ta>
            <ta e="T11" id="Seg_3411" s="T10">v</ta>
            <ta e="T13" id="Seg_3412" s="T12">adj</ta>
            <ta e="T14" id="Seg_3413" s="T13">n</ta>
            <ta e="T15" id="Seg_3414" s="T14">v</ta>
            <ta e="T16" id="Seg_3415" s="T15">nprop</ta>
            <ta e="T3" id="Seg_3416" s="T17">nprop</ta>
            <ta e="T18" id="Seg_3417" s="T3">pp</ta>
            <ta e="T19" id="Seg_3418" s="T18">v</ta>
            <ta e="T20" id="Seg_3419" s="T19">n</ta>
            <ta e="T21" id="Seg_3420" s="T20">n</ta>
            <ta e="T23" id="Seg_3421" s="T22">conj</ta>
            <ta e="T24" id="Seg_3422" s="T23">pers</ta>
            <ta e="T25" id="Seg_3423" s="T24">adj</ta>
            <ta e="T26" id="Seg_3424" s="T25">n</ta>
            <ta e="T27" id="Seg_3425" s="T26">n</ta>
            <ta e="T28" id="Seg_3426" s="T27">v</ta>
            <ta e="T29" id="Seg_3427" s="T28">n</ta>
            <ta e="T31" id="Seg_3428" s="T30">adv</ta>
            <ta e="T32" id="Seg_3429" s="T31">num</ta>
            <ta e="T33" id="Seg_3430" s="T32">n</ta>
            <ta e="T34" id="Seg_3431" s="T33">v</ta>
            <ta e="T35" id="Seg_3432" s="T34">adj</ta>
            <ta e="T36" id="Seg_3433" s="T35">n</ta>
            <ta e="T37" id="Seg_3434" s="T36">conj</ta>
            <ta e="T38" id="Seg_3435" s="T37">nprop</ta>
            <ta e="T39" id="Seg_3436" s="T38">v</ta>
            <ta e="T40" id="Seg_3437" s="T39">v</ta>
            <ta e="T41" id="Seg_3438" s="T40">n</ta>
            <ta e="T5" id="Seg_3439" s="T42">conj</ta>
            <ta e="T43" id="Seg_3440" s="T5">nprop</ta>
            <ta e="T44" id="Seg_3441" s="T43">v</ta>
            <ta e="T45" id="Seg_3442" s="T44">n</ta>
            <ta e="T46" id="Seg_3443" s="T45">pers</ta>
            <ta e="T47" id="Seg_3444" s="T46">n</ta>
            <ta e="T48" id="Seg_3445" s="T47">v</ta>
            <ta e="T49" id="Seg_3446" s="T115">conj</ta>
            <ta e="T50" id="Seg_3447" s="T49">adj</ta>
            <ta e="T51" id="Seg_3448" s="T50">n</ta>
            <ta e="T52" id="Seg_3449" s="T51">nprop</ta>
            <ta e="T53" id="Seg_3450" s="T52">v</ta>
            <ta e="T54" id="Seg_3451" s="T53">interrog</ta>
            <ta e="T55" id="Seg_3452" s="T54">adv</ta>
            <ta e="T56" id="Seg_3453" s="T55">v</ta>
            <ta e="T58" id="Seg_3454" s="T57">conj</ta>
            <ta e="T59" id="Seg_3455" s="T58">pers</ta>
            <ta e="T60" id="Seg_3456" s="T59">conj</ta>
            <ta e="T61" id="Seg_3457" s="T60">adv</ta>
            <ta e="T62" id="Seg_3458" s="T61">v</ta>
            <ta e="T63" id="Seg_3459" s="T62">pers</ta>
            <ta e="T326" id="Seg_3460" s="T63">n</ta>
            <ta e="T65" id="Seg_3461" s="T64">n</ta>
            <ta e="T66" id="Seg_3462" s="T65">v</ta>
            <ta e="T68" id="Seg_3463" s="T67">adj</ta>
            <ta e="T69" id="Seg_3464" s="T68">v</ta>
            <ta e="T70" id="Seg_3465" s="T69">adv</ta>
            <ta e="T71" id="Seg_3466" s="T70">v</ta>
            <ta e="T73" id="Seg_3467" s="T72">adv</ta>
            <ta e="T74" id="Seg_3468" s="T73">adj</ta>
            <ta e="T78" id="Seg_3469" s="T74">n</ta>
            <ta e="T76" id="Seg_3470" s="T75">v</ta>
            <ta e="T327" id="Seg_3471" s="T76">n</ta>
            <ta e="T79" id="Seg_3472" s="T77">adv</ta>
            <ta e="T80" id="Seg_3473" s="T79">adv</ta>
            <ta e="T81" id="Seg_3474" s="T80">v</ta>
            <ta e="T328" id="Seg_3475" s="T82">adv</ta>
            <ta e="T83" id="Seg_3476" s="T328">conj</ta>
            <ta e="T84" id="Seg_3477" s="T83">adv</ta>
            <ta e="T85" id="Seg_3478" s="T84">emphpro</ta>
            <ta e="T86" id="Seg_3479" s="T85">adv</ta>
            <ta e="T87" id="Seg_3480" s="T86">v</ta>
            <ta e="T88" id="Seg_3481" s="T87">adj</ta>
            <ta e="T89" id="Seg_3482" s="T88">n</ta>
            <ta e="T90" id="Seg_3483" s="T89">adj</ta>
            <ta e="T91" id="Seg_3484" s="T90">n</ta>
            <ta e="T93" id="Seg_3485" s="T92">conj</ta>
            <ta e="T94" id="Seg_3486" s="T93">n</ta>
            <ta e="T95" id="Seg_3487" s="T94">adv</ta>
            <ta e="T96" id="Seg_3488" s="T95">nprop</ta>
            <ta e="T97" id="Seg_3489" s="T96">v</ta>
            <ta e="T99" id="Seg_3490" s="T98">nprop</ta>
            <ta e="T100" id="Seg_3491" s="T99">n</ta>
            <ta e="T101" id="Seg_3492" s="T100">n</ta>
            <ta e="T102" id="Seg_3493" s="T101">v</ta>
            <ta e="T103" id="Seg_3494" s="T102">v</ta>
            <ta e="T105" id="Seg_3495" s="T104">v</ta>
            <ta e="T106" id="Seg_3496" s="T105">interrog</ta>
            <ta e="T107" id="Seg_3497" s="T106">n</ta>
            <ta e="T108" id="Seg_3498" s="T107">v</ta>
            <ta e="T110" id="Seg_3499" s="T109">v</ta>
            <ta e="T111" id="Seg_3500" s="T110">v</ta>
            <ta e="T112" id="Seg_3501" s="T111">n</ta>
            <ta e="T113" id="Seg_3502" s="T112">v</ta>
            <ta e="T116" id="Seg_3503" s="T114">n</ta>
            <ta e="T117" id="Seg_3504" s="T116">v</ta>
            <ta e="T118" id="Seg_3505" s="T117">n</ta>
            <ta e="T120" id="Seg_3506" s="T119">pers</ta>
            <ta e="T121" id="Seg_3507" s="T120">nprop</ta>
            <ta e="T122" id="Seg_3508" s="T121">pro</ta>
            <ta e="T123" id="Seg_3509" s="T122">n</ta>
            <ta e="T124" id="Seg_3510" s="T123">interrog</ta>
            <ta e="T125" id="Seg_3511" s="T124">v</ta>
            <ta e="T126" id="Seg_3512" s="T125">adv</ta>
            <ta e="T127" id="Seg_3513" s="T126">v</ta>
            <ta e="T129" id="Seg_3514" s="T128">adv</ta>
            <ta e="T130" id="Seg_3515" s="T129">v</ta>
            <ta e="T131" id="Seg_3516" s="T130">n</ta>
            <ta e="T133" id="Seg_3517" s="T132">pro</ta>
            <ta e="T134" id="Seg_3518" s="T133">n</ta>
            <ta e="T135" id="Seg_3519" s="T134">adv</ta>
            <ta e="T136" id="Seg_3520" s="T135">n</ta>
            <ta e="T137" id="Seg_3521" s="T136">quant</ta>
            <ta e="T329" id="Seg_3522" s="T324">pers</ta>
            <ta e="T330" id="Seg_3523" s="T323">v</ta>
            <ta e="T139" id="Seg_3524" s="T138">pers</ta>
            <ta e="T140" id="Seg_3525" s="T139">v</ta>
            <ta e="T141" id="Seg_3526" s="T140">n</ta>
            <ta e="T142" id="Seg_3527" s="T141">conj</ta>
            <ta e="T143" id="Seg_3528" s="T142">n</ta>
            <ta e="T144" id="Seg_3529" s="T143">v</ta>
            <ta e="T146" id="Seg_3530" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_3531" s="T146">n</ta>
            <ta e="T148" id="Seg_3532" s="T147">v</ta>
            <ta e="T150" id="Seg_3533" s="T149">v</ta>
            <ta e="T151" id="Seg_3534" s="T150">n</ta>
            <ta e="T152" id="Seg_3535" s="T151">n</ta>
            <ta e="T153" id="Seg_3536" s="T152">v</ta>
            <ta e="T155" id="Seg_3537" s="T154">adv</ta>
            <ta e="T156" id="Seg_3538" s="T155">n</ta>
            <ta e="T157" id="Seg_3539" s="T156">n</ta>
            <ta e="T158" id="Seg_3540" s="T157">v</ta>
            <ta e="T160" id="Seg_3541" s="T159">adv</ta>
            <ta e="T161" id="Seg_3542" s="T160">v</ta>
            <ta e="T162" id="Seg_3543" s="T161">n</ta>
            <ta e="T163" id="Seg_3544" s="T162">v</ta>
            <ta e="T165" id="Seg_3545" s="T164">n</ta>
            <ta e="T166" id="Seg_3546" s="T165">v</ta>
            <ta e="T167" id="Seg_3547" s="T166">v</ta>
            <ta e="T168" id="Seg_3548" s="T167">nprop</ta>
            <ta e="T169" id="Seg_3549" s="T168">interrog</ta>
            <ta e="T170" id="Seg_3550" s="T169">v</ta>
            <ta e="T172" id="Seg_3551" s="T171">pers</ta>
            <ta e="T173" id="Seg_3552" s="T172">n</ta>
            <ta e="T174" id="Seg_3553" s="T173">adv</ta>
            <ta e="T175" id="Seg_3554" s="T174">v</ta>
            <ta e="T331" id="Seg_3555" s="T176">n</ta>
            <ta e="T177" id="Seg_3556" s="T331">ptcl</ta>
            <ta e="T178" id="Seg_3557" s="T177">adv</ta>
            <ta e="T179" id="Seg_3558" s="T178">v</ta>
            <ta e="T180" id="Seg_3559" s="T179">n</ta>
            <ta e="T181" id="Seg_3560" s="T180">pp</ta>
            <ta e="T183" id="Seg_3561" s="T182">n</ta>
            <ta e="T184" id="Seg_3562" s="T183">n</ta>
            <ta e="T185" id="Seg_3563" s="T184">pp</ta>
            <ta e="T186" id="Seg_3564" s="T185">v</ta>
            <ta e="T188" id="Seg_3565" s="T187">nprop</ta>
            <ta e="T189" id="Seg_3566" s="T188">pers</ta>
            <ta e="T190" id="Seg_3567" s="T189">ptcl</ta>
            <ta e="T191" id="Seg_3568" s="T190">v</ta>
            <ta e="T193" id="Seg_3569" s="T192">adv</ta>
            <ta e="T194" id="Seg_3570" s="T193">n</ta>
            <ta e="T195" id="Seg_3571" s="T194">v</ta>
            <ta e="T196" id="Seg_3572" s="T195">pers</ta>
            <ta e="T197" id="Seg_3573" s="T196">num</ta>
            <ta e="T198" id="Seg_3574" s="T197">n</ta>
            <ta e="T199" id="Seg_3575" s="T198">n</ta>
            <ta e="T200" id="Seg_3576" s="T199">v</ta>
            <ta e="T202" id="Seg_3577" s="T201">n</ta>
            <ta e="T203" id="Seg_3578" s="T202">v</ta>
            <ta e="T204" id="Seg_3579" s="T203">interj</ta>
            <ta e="T205" id="Seg_3580" s="T204">adj</ta>
            <ta e="T333" id="Seg_3581" s="T205">n</ta>
            <ta e="T207" id="Seg_3582" s="T332">ptcl</ta>
            <ta e="T208" id="Seg_3583" s="T207">pers</ta>
            <ta e="T209" id="Seg_3584" s="T208">n</ta>
            <ta e="T210" id="Seg_3585" s="T209">ptcl</ta>
            <ta e="T211" id="Seg_3586" s="T210">preverb</ta>
            <ta e="T212" id="Seg_3587" s="T211">v</ta>
            <ta e="T214" id="Seg_3588" s="T213">nprop</ta>
            <ta e="T215" id="Seg_3589" s="T214">v</ta>
            <ta e="T216" id="Seg_3590" s="T215">pers</ta>
            <ta e="T217" id="Seg_3591" s="T216">pers</ta>
            <ta e="T218" id="Seg_3592" s="T217">ptcl</ta>
            <ta e="T219" id="Seg_3593" s="T218">v</ta>
            <ta e="T221" id="Seg_3594" s="T220">ptcl</ta>
            <ta e="T222" id="Seg_3595" s="T221">v</ta>
            <ta e="T224" id="Seg_3596" s="T223">adv</ta>
            <ta e="T334" id="Seg_3597" s="T224">nprop</ta>
            <ta e="T225" id="Seg_3598" s="T334">ptcl</ta>
            <ta e="T226" id="Seg_3599" s="T225">n</ta>
            <ta e="T227" id="Seg_3600" s="T226">v</ta>
            <ta e="T229" id="Seg_3601" s="T228">dem</ta>
            <ta e="T230" id="Seg_3602" s="T229">n</ta>
            <ta e="T231" id="Seg_3603" s="T230">n</ta>
            <ta e="T232" id="Seg_3604" s="T231">v</ta>
            <ta e="T234" id="Seg_3605" s="T233">nprop</ta>
            <ta e="T235" id="Seg_3606" s="T234">n</ta>
            <ta e="T236" id="Seg_3607" s="T235">adj</ta>
            <ta e="T335" id="Seg_3608" s="T236">v</ta>
            <ta e="T237" id="Seg_3609" s="T335">adv</ta>
            <ta e="T238" id="Seg_3610" s="T237">n</ta>
            <ta e="T239" id="Seg_3611" s="T238">n</ta>
            <ta e="T240" id="Seg_3612" s="T239">v</ta>
            <ta e="T241" id="Seg_3613" s="T240">n</ta>
            <ta e="T242" id="Seg_3614" s="T241">adv</ta>
            <ta e="T243" id="Seg_3615" s="T242">v</ta>
            <ta e="T336" id="Seg_3616" s="T244">nprop</ta>
            <ta e="T245" id="Seg_3617" s="T336">adv</ta>
            <ta e="T246" id="Seg_3618" s="T245">v</ta>
            <ta e="T247" id="Seg_3619" s="T246">n</ta>
            <ta e="T248" id="Seg_3620" s="T247">quant</ta>
            <ta e="T249" id="Seg_3621" s="T248">preverb</ta>
            <ta e="T250" id="Seg_3622" s="T249">v</ta>
            <ta e="T252" id="Seg_3623" s="T251">adv</ta>
            <ta e="T253" id="Seg_3624" s="T252">quant</ta>
            <ta e="T254" id="Seg_3625" s="T253">adj</ta>
            <ta e="T255" id="Seg_3626" s="T254">n</ta>
            <ta e="T256" id="Seg_3627" s="T255">preverb</ta>
            <ta e="T257" id="Seg_3628" s="T256">quant</ta>
            <ta e="T258" id="Seg_3629" s="T337">adj</ta>
            <ta e="T259" id="Seg_3630" s="T258">n</ta>
            <ta e="T260" id="Seg_3631" s="T259">v</ta>
            <ta e="T261" id="Seg_3632" s="T260">adj</ta>
            <ta e="T262" id="Seg_3633" s="T261">n</ta>
            <ta e="T263" id="Seg_3634" s="T262">v</ta>
            <ta e="T264" id="Seg_3635" s="T263">conj</ta>
            <ta e="T265" id="Seg_3636" s="T264">adv</ta>
            <ta e="T266" id="Seg_3637" s="T265">v</ta>
            <ta e="T268" id="Seg_3638" s="T267">adv</ta>
            <ta e="T269" id="Seg_3639" s="T268">v</ta>
            <ta e="T270" id="Seg_3640" s="T269">n</ta>
            <ta e="T271" id="Seg_3641" s="T270">v</ta>
            <ta e="T273" id="Seg_3642" s="T272">pers</ta>
            <ta e="T274" id="Seg_3643" s="T273">n</ta>
            <ta e="T275" id="Seg_3644" s="T274">preverb</ta>
            <ta e="T276" id="Seg_3645" s="T275">v</ta>
            <ta e="T277" id="Seg_3646" s="T276">n</ta>
            <ta e="T278" id="Seg_3647" s="T277">n</ta>
            <ta e="T279" id="Seg_3648" s="T278">adv</ta>
            <ta e="T280" id="Seg_3649" s="T279">v</ta>
            <ta e="T282" id="Seg_3650" s="T281">n</ta>
            <ta e="T283" id="Seg_3651" s="T282">n</ta>
            <ta e="T284" id="Seg_3652" s="T283">num</ta>
            <ta e="T285" id="Seg_3653" s="T284">n</ta>
            <ta e="T286" id="Seg_3654" s="T285">n</ta>
            <ta e="T287" id="Seg_3655" s="T286">preverb</ta>
            <ta e="T288" id="Seg_3656" s="T287">v</ta>
            <ta e="T290" id="Seg_3657" s="T289">dem</ta>
            <ta e="T291" id="Seg_3658" s="T290">pers</ta>
            <ta e="T339" id="Seg_3659" s="T291">nprop</ta>
            <ta e="T294" id="Seg_3660" s="T293">nprop</ta>
            <ta e="T295" id="Seg_3661" s="T294">adv</ta>
            <ta e="T296" id="Seg_3662" s="T295">v</ta>
            <ta e="T297" id="Seg_3663" s="T296">pers</ta>
            <ta e="T298" id="Seg_3664" s="T297">v</ta>
            <ta e="T300" id="Seg_3665" s="T299">nprop</ta>
            <ta e="T301" id="Seg_3666" s="T300">pro</ta>
            <ta e="T302" id="Seg_3667" s="T301">n</ta>
            <ta e="T303" id="Seg_3668" s="T302">n</ta>
            <ta e="T304" id="Seg_3669" s="T303">adv</ta>
            <ta e="T305" id="Seg_3670" s="T304">v</ta>
            <ta e="T307" id="Seg_3671" s="T306">v</ta>
            <ta e="T308" id="Seg_3672" s="T307">n</ta>
            <ta e="T309" id="Seg_3673" s="T308">n</ta>
            <ta e="T311" id="Seg_3674" s="T310">conj</ta>
            <ta e="T312" id="Seg_3675" s="T311">adv</ta>
            <ta e="T313" id="Seg_3676" s="T312">v</ta>
            <ta e="T314" id="Seg_3677" s="T313">v</ta>
            <ta e="T316" id="Seg_3678" s="T315">n</ta>
            <ta e="T317" id="Seg_3679" s="T316">n</ta>
            <ta e="T318" id="Seg_3680" s="T317">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_3681" s="T319">np.h:Th</ta>
            <ta e="T9" id="Seg_3682" s="T8">np.h:Th</ta>
            <ta e="T14" id="Seg_3683" s="T13">np.h:Th 0.3.h:Poss</ta>
            <ta e="T15" id="Seg_3684" s="T14">0.3.h:A</ta>
            <ta e="T3" id="Seg_3685" s="T17">pp:Com</ta>
            <ta e="T19" id="Seg_3686" s="T18">0.3.h:A</ta>
            <ta e="T21" id="Seg_3687" s="T20">np:Path</ta>
            <ta e="T24" id="Seg_3688" s="T23">pro.h:Poss</ta>
            <ta e="T26" id="Seg_3689" s="T25">np.h:Poss 0.3.h:Poss</ta>
            <ta e="T27" id="Seg_3690" s="T26">np.h:Th</ta>
            <ta e="T28" id="Seg_3691" s="T27">0.3.h:A</ta>
            <ta e="T31" id="Seg_3692" s="T30">adv:Time</ta>
            <ta e="T34" id="Seg_3693" s="T33">0.3.h:A</ta>
            <ta e="T36" id="Seg_3694" s="T35">np:G</ta>
            <ta e="T38" id="Seg_3695" s="T37">np.h:A</ta>
            <ta e="T41" id="Seg_3696" s="T40">np:A</ta>
            <ta e="T43" id="Seg_3697" s="T5">np.h:A</ta>
            <ta e="T46" id="Seg_3698" s="T45">pro.h:R</ta>
            <ta e="T47" id="Seg_3699" s="T46">np:Th</ta>
            <ta e="T48" id="Seg_3700" s="T47">0.2.h:A</ta>
            <ta e="T51" id="Seg_3701" s="T50">np.h:A 0.3.h:Poss</ta>
            <ta e="T52" id="Seg_3702" s="T51">np.h:R</ta>
            <ta e="T56" id="Seg_3703" s="T55">0.2.h:A</ta>
            <ta e="T59" id="Seg_3704" s="T58">pro.h:A</ta>
            <ta e="T63" id="Seg_3705" s="T62">pro.h:B</ta>
            <ta e="T326" id="Seg_3706" s="T63">np.h:A 0.1.h:Poss</ta>
            <ta e="T65" id="Seg_3707" s="T64">np:Ins</ta>
            <ta e="T69" id="Seg_3708" s="T68">0.3.h:A</ta>
            <ta e="T70" id="Seg_3709" s="T69">adv:G</ta>
            <ta e="T71" id="Seg_3710" s="T70">0.3.h:A</ta>
            <ta e="T73" id="Seg_3711" s="T72">adv:Time</ta>
            <ta e="T78" id="Seg_3712" s="T74">np.h:A 0.3.h:Poss</ta>
            <ta e="T327" id="Seg_3713" s="T76">np.h:P</ta>
            <ta e="T80" id="Seg_3714" s="T79">0.3.h:A</ta>
            <ta e="T81" id="Seg_3715" s="T80">0.3.h:A 0.3.h:P</ta>
            <ta e="T328" id="Seg_3716" s="T82">adv:Time</ta>
            <ta e="T84" id="Seg_3717" s="T83">adv:Time</ta>
            <ta e="T89" id="Seg_3718" s="T88">np.h:A 0.3.h:Poss</ta>
            <ta e="T91" id="Seg_3719" s="T90">np:G</ta>
            <ta e="T94" id="Seg_3720" s="T93">np.h:A 0.3.h:Poss</ta>
            <ta e="T95" id="Seg_3721" s="T94">0.3.h:A</ta>
            <ta e="T96" id="Seg_3722" s="T95">np.h:P</ta>
            <ta e="T97" id="Seg_3723" s="T96">0.3.h:P</ta>
            <ta e="T99" id="Seg_3724" s="T98">np.h:A</ta>
            <ta e="T100" id="Seg_3725" s="T99">np:So</ta>
            <ta e="T101" id="Seg_3726" s="T100">np:G</ta>
            <ta e="T102" id="Seg_3727" s="T101">0.3.h:A</ta>
            <ta e="T105" id="Seg_3728" s="T104">0.3.h:A</ta>
            <ta e="T106" id="Seg_3729" s="T105">adv:G</ta>
            <ta e="T107" id="Seg_3730" s="T106">np:E 0.3.h:Poss</ta>
            <ta e="T110" id="Seg_3731" s="T109">0.3.h:A</ta>
            <ta e="T111" id="Seg_3732" s="T110">0.3.h:E</ta>
            <ta e="T112" id="Seg_3733" s="T111">np:A</ta>
            <ta e="T116" id="Seg_3734" s="T114">np:L</ta>
            <ta e="T117" id="Seg_3735" s="T116">0.3:A</ta>
            <ta e="T118" id="Seg_3736" s="T117">np:Th</ta>
            <ta e="T120" id="Seg_3737" s="T119">pro.h:A</ta>
            <ta e="T123" id="Seg_3738" s="T122">np:A 0.2:Poss</ta>
            <ta e="T126" id="Seg_3739" s="T125">adv:G</ta>
            <ta e="T127" id="Seg_3740" s="T126">0.3.h:A</ta>
            <ta e="T129" id="Seg_3741" s="T128">0.3.h:A</ta>
            <ta e="T130" id="Seg_3742" s="T129">0.3.h:A</ta>
            <ta e="T131" id="Seg_3743" s="T130">np:Th</ta>
            <ta e="T134" id="Seg_3744" s="T133">np:L</ta>
            <ta e="T136" id="Seg_3745" s="T135">np:Th</ta>
            <ta e="T329" id="Seg_3746" s="T324">pro.h:A</ta>
            <ta e="T139" id="Seg_3747" s="T138">pro.h:A</ta>
            <ta e="T141" id="Seg_3748" s="T140">np:P</ta>
            <ta e="T143" id="Seg_3749" s="T142">np:P</ta>
            <ta e="T144" id="Seg_3750" s="T143">0.3.h:A</ta>
            <ta e="T147" id="Seg_3751" s="T146">np:P</ta>
            <ta e="T150" id="Seg_3752" s="T149">0.3.h:A</ta>
            <ta e="T151" id="Seg_3753" s="T150">np:P</ta>
            <ta e="T152" id="Seg_3754" s="T151">np:P</ta>
            <ta e="T153" id="Seg_3755" s="T152">0.3.h:A</ta>
            <ta e="T155" id="Seg_3756" s="T154">adv:G</ta>
            <ta e="T156" id="Seg_3757" s="T155">np:P</ta>
            <ta e="T158" id="Seg_3758" s="T157">0.3.h:A</ta>
            <ta e="T161" id="Seg_3759" s="T160">0.3.h:A</ta>
            <ta e="T162" id="Seg_3760" s="T161">np.h:A</ta>
            <ta e="T165" id="Seg_3761" s="T164">np.h:A</ta>
            <ta e="T167" id="Seg_3762" s="T166">0.3.h:A</ta>
            <ta e="T168" id="Seg_3763" s="T167">np.h:R</ta>
            <ta e="T169" id="Seg_3764" s="T168">pro:Th</ta>
            <ta e="T170" id="Seg_3765" s="T169">0.2.h:A</ta>
            <ta e="T172" id="Seg_3766" s="T171">pro.h:A</ta>
            <ta e="T173" id="Seg_3767" s="T172">np:P 0.1.h:Poss</ta>
            <ta e="T174" id="Seg_3768" s="T173">adv:L</ta>
            <ta e="T178" id="Seg_3769" s="T177">adv:G</ta>
            <ta e="T179" id="Seg_3770" s="T178">0.2.h:A</ta>
            <ta e="T180" id="Seg_3771" s="T179">pp:G</ta>
            <ta e="T183" id="Seg_3772" s="T182">np.h:A</ta>
            <ta e="T184" id="Seg_3773" s="T183">pp:L</ta>
            <ta e="T189" id="Seg_3774" s="T188">pro.h:R</ta>
            <ta e="T191" id="Seg_3775" s="T190">0.3.h:A</ta>
            <ta e="T193" id="Seg_3776" s="T192">adv:Time</ta>
            <ta e="T194" id="Seg_3777" s="T193">np:P</ta>
            <ta e="T196" id="Seg_3778" s="T195">pro.h:A</ta>
            <ta e="T198" id="Seg_3779" s="T197">np:Th</ta>
            <ta e="T199" id="Seg_3780" s="T198">np.h:G</ta>
            <ta e="T202" id="Seg_3781" s="T201">np.h:A</ta>
            <ta e="T203" id="Seg_3782" s="T202">0.3:P</ta>
            <ta e="T208" id="Seg_3783" s="T207">pro.h:Poss</ta>
            <ta e="T209" id="Seg_3784" s="T208">np:P</ta>
            <ta e="T212" id="Seg_3785" s="T211">0.2.h:A</ta>
            <ta e="T214" id="Seg_3786" s="T213">np.h:A</ta>
            <ta e="T216" id="Seg_3787" s="T215">pro.h:A</ta>
            <ta e="T217" id="Seg_3788" s="T216">pro.h:P</ta>
            <ta e="T222" id="Seg_3789" s="T221">0.1.h:A</ta>
            <ta e="T224" id="Seg_3790" s="T223">adv:Time</ta>
            <ta e="T226" id="Seg_3791" s="T225">np:G</ta>
            <ta e="T227" id="Seg_3792" s="T226">0.2.h:A</ta>
            <ta e="T230" id="Seg_3793" s="T229">np.h:P</ta>
            <ta e="T231" id="Seg_3794" s="T230">np:L</ta>
            <ta e="T234" id="Seg_3795" s="T233">np.h:A</ta>
            <ta e="T235" id="Seg_3796" s="T234">np:P</ta>
            <ta e="T238" id="Seg_3797" s="T237">np.h:Poss</ta>
            <ta e="T239" id="Seg_3798" s="T238">np:P</ta>
            <ta e="T240" id="Seg_3799" s="T239">0.3.h:A</ta>
            <ta e="T241" id="Seg_3800" s="T240">np.h:P</ta>
            <ta e="T242" id="Seg_3801" s="T241">adv:L</ta>
            <ta e="T336" id="Seg_3802" s="T244">np.h:A</ta>
            <ta e="T246" id="Seg_3803" s="T245">0.3.h:P</ta>
            <ta e="T247" id="Seg_3804" s="T246">np:Th 0.3:Poss</ta>
            <ta e="T250" id="Seg_3805" s="T249">0.3.h:A</ta>
            <ta e="T252" id="Seg_3806" s="T251">adv:Time</ta>
            <ta e="T255" id="Seg_3807" s="T254">np:Time</ta>
            <ta e="T259" id="Seg_3808" s="T258">np:G 0.3.h:Poss</ta>
            <ta e="T260" id="Seg_3809" s="T259">0.3.h:A 0.3:Th</ta>
            <ta e="T262" id="Seg_3810" s="T261">np:G 0.3.h:Poss</ta>
            <ta e="T263" id="Seg_3811" s="T262">0.3.h:A 0.3:Th</ta>
            <ta e="T265" id="Seg_3812" s="T264">adv:G</ta>
            <ta e="T266" id="Seg_3813" s="T265">0.3.h:A</ta>
            <ta e="T268" id="Seg_3814" s="T267">adv:G</ta>
            <ta e="T269" id="Seg_3815" s="T268">0.3.h:A</ta>
            <ta e="T270" id="Seg_3816" s="T269">np:Th</ta>
            <ta e="T274" id="Seg_3817" s="T273">np:G</ta>
            <ta e="T276" id="Seg_3818" s="T275">0.3.h:A</ta>
            <ta e="T279" id="Seg_3819" s="T278">adv:G</ta>
            <ta e="T280" id="Seg_3820" s="T279">0.3.h:A</ta>
            <ta e="T282" id="Seg_3821" s="T281">np.h:A 0.3.h:Poss</ta>
            <ta e="T283" id="Seg_3822" s="T282">np.h:A 0.3.h:Poss</ta>
            <ta e="T285" id="Seg_3823" s="T284">np:Poss</ta>
            <ta e="T286" id="Seg_3824" s="T285">np:P</ta>
            <ta e="T290" id="Seg_3825" s="T289">pro.h:Th</ta>
            <ta e="T291" id="Seg_3826" s="T290">pro.h:Poss</ta>
            <ta e="T294" id="Seg_3827" s="T293">np.h:A</ta>
            <ta e="T295" id="Seg_3828" s="T294">adv:L</ta>
            <ta e="T297" id="Seg_3829" s="T296">pro.h:Th</ta>
            <ta e="T300" id="Seg_3830" s="T299">np.h:A</ta>
            <ta e="T301" id="Seg_3831" s="T300">pro.h:G</ta>
            <ta e="T302" id="Seg_3832" s="T301">np:Th</ta>
            <ta e="T303" id="Seg_3833" s="T302">np:Th</ta>
            <ta e="T304" id="Seg_3834" s="T303">adv:G</ta>
            <ta e="T307" id="Seg_3835" s="T306">0.3.h:A 0.3.h:B</ta>
            <ta e="T308" id="Seg_3836" s="T307">np:Ins</ta>
            <ta e="T309" id="Seg_3837" s="T308">np:Ins</ta>
            <ta e="T312" id="Seg_3838" s="T311">adv:Time</ta>
            <ta e="T313" id="Seg_3839" s="T312">0.3.h:Th</ta>
            <ta e="T314" id="Seg_3840" s="T313">0.3.h:Th</ta>
            <ta e="T316" id="Seg_3841" s="T315">np.h:Th</ta>
            <ta e="T317" id="Seg_3842" s="T316">np.h:Th</ta>
            <ta e="T318" id="Seg_3843" s="T317">0.3.h:B</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_3844" s="T319">np.h:S</ta>
            <ta e="T320" id="Seg_3845" s="T2">v:pred</ta>
            <ta e="T6" id="Seg_3846" s="T4">v:pred</ta>
            <ta e="T9" id="Seg_3847" s="T8">np.h:S</ta>
            <ta e="T10" id="Seg_3848" s="T9">s:temp</ta>
            <ta e="T11" id="Seg_3849" s="T10">v:pred</ta>
            <ta e="T14" id="Seg_3850" s="T13">np.h:O</ta>
            <ta e="T15" id="Seg_3851" s="T14">0.3.h:S v:pred</ta>
            <ta e="T19" id="Seg_3852" s="T18">0.3.h:S v:pred</ta>
            <ta e="T27" id="Seg_3853" s="T26">np.h:O</ta>
            <ta e="T28" id="Seg_3854" s="T27">0.3.h:S v:pred</ta>
            <ta e="T34" id="Seg_3855" s="T33">0.3.h:S v:pred</ta>
            <ta e="T38" id="Seg_3856" s="T37">np.h:S</ta>
            <ta e="T39" id="Seg_3857" s="T38">v:pred</ta>
            <ta e="T40" id="Seg_3858" s="T39">v:pred</ta>
            <ta e="T41" id="Seg_3859" s="T40">np:S</ta>
            <ta e="T43" id="Seg_3860" s="T5">np.h:S</ta>
            <ta e="T44" id="Seg_3861" s="T43">v:pred</ta>
            <ta e="T47" id="Seg_3862" s="T46">np:O</ta>
            <ta e="T48" id="Seg_3863" s="T47">0.2.h:S v:pred</ta>
            <ta e="T51" id="Seg_3864" s="T50">np.h:S</ta>
            <ta e="T53" id="Seg_3865" s="T52">v:pred</ta>
            <ta e="T56" id="Seg_3866" s="T55">0.2.h:S v:pred</ta>
            <ta e="T59" id="Seg_3867" s="T58">pro.h:S</ta>
            <ta e="T62" id="Seg_3868" s="T61">v:pred</ta>
            <ta e="T63" id="Seg_3869" s="T62">pro.h:O</ta>
            <ta e="T326" id="Seg_3870" s="T63">np.h:S</ta>
            <ta e="T66" id="Seg_3871" s="T65">v:pred</ta>
            <ta e="T69" id="Seg_3872" s="T68">0.3.h:S v:pred</ta>
            <ta e="T71" id="Seg_3873" s="T70">0.3.h:S v:pred</ta>
            <ta e="T78" id="Seg_3874" s="T74">np.h:S</ta>
            <ta e="T76" id="Seg_3875" s="T75">v:pred</ta>
            <ta e="T327" id="Seg_3876" s="T76">np.h:O</ta>
            <ta e="T80" id="Seg_3877" s="T79">s:temp</ta>
            <ta e="T81" id="Seg_3878" s="T80">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T87" id="Seg_3879" s="T86">v:pred</ta>
            <ta e="T89" id="Seg_3880" s="T88">np.h:S</ta>
            <ta e="T94" id="Seg_3881" s="T93">np.h:S</ta>
            <ta e="T96" id="Seg_3882" s="T94">s:temp</ta>
            <ta e="T97" id="Seg_3883" s="T96">v:pred 0.3.h:O</ta>
            <ta e="T99" id="Seg_3884" s="T98">np.h:S</ta>
            <ta e="T102" id="Seg_3885" s="T99">s:temp</ta>
            <ta e="T103" id="Seg_3886" s="T102">v:pred</ta>
            <ta e="T105" id="Seg_3887" s="T104">0.3.h:S v:pred</ta>
            <ta e="T108" id="Seg_3888" s="T105">s:rel</ta>
            <ta e="T110" id="Seg_3889" s="T109">0.3.h:S v:pred</ta>
            <ta e="T111" id="Seg_3890" s="T110">0.3.h:S v:pred</ta>
            <ta e="T112" id="Seg_3891" s="T111">np:S</ta>
            <ta e="T113" id="Seg_3892" s="T112">v:pred</ta>
            <ta e="T117" id="Seg_3893" s="T116">0.3:S v:pred</ta>
            <ta e="T118" id="Seg_3894" s="T117">np:O</ta>
            <ta e="T120" id="Seg_3895" s="T119">pro.h:S</ta>
            <ta e="T125" id="Seg_3896" s="T121">s:rel</ta>
            <ta e="T127" id="Seg_3897" s="T126">0.3.h:S v:pred</ta>
            <ta e="T129" id="Seg_3898" s="T128">0.3.h:S v:pred</ta>
            <ta e="T130" id="Seg_3899" s="T129">0.3.h:S v:pred</ta>
            <ta e="T131" id="Seg_3900" s="T130">np:O</ta>
            <ta e="T136" id="Seg_3901" s="T135">np:S</ta>
            <ta e="T137" id="Seg_3902" s="T136">n:pred</ta>
            <ta e="T329" id="Seg_3903" s="T324">pro.h:S</ta>
            <ta e="T330" id="Seg_3904" s="T323">v:pred</ta>
            <ta e="T139" id="Seg_3905" s="T138">pro.h:S</ta>
            <ta e="T140" id="Seg_3906" s="T139">v:pred</ta>
            <ta e="T141" id="Seg_3907" s="T140">np:O</ta>
            <ta e="T143" id="Seg_3908" s="T142">np:O</ta>
            <ta e="T144" id="Seg_3909" s="T143">0.3.h:S v:pred</ta>
            <ta e="T150" id="Seg_3910" s="T149">0.3.h:S v:pred</ta>
            <ta e="T151" id="Seg_3911" s="T150">np:O</ta>
            <ta e="T152" id="Seg_3912" s="T151">np:O</ta>
            <ta e="T153" id="Seg_3913" s="T152">0.3.h:S v:pred</ta>
            <ta e="T156" id="Seg_3914" s="T155">np:O</ta>
            <ta e="T158" id="Seg_3915" s="T157">0.3.h:S v:pred</ta>
            <ta e="T161" id="Seg_3916" s="T160">0.3.h:S v:pred</ta>
            <ta e="T162" id="Seg_3917" s="T161">np.h:S</ta>
            <ta e="T163" id="Seg_3918" s="T162">v:pred</ta>
            <ta e="T165" id="Seg_3919" s="T164">np.h:S</ta>
            <ta e="T166" id="Seg_3920" s="T165">v:pred</ta>
            <ta e="T167" id="Seg_3921" s="T166">0.3.h:S v:pred</ta>
            <ta e="T169" id="Seg_3922" s="T168">pro:O</ta>
            <ta e="T170" id="Seg_3923" s="T169">0.2.h:S v:pred</ta>
            <ta e="T172" id="Seg_3924" s="T171">pro.h:S</ta>
            <ta e="T173" id="Seg_3925" s="T172">np:O</ta>
            <ta e="T175" id="Seg_3926" s="T174">v:pred</ta>
            <ta e="T179" id="Seg_3927" s="T178">0.2.h:S v:pred</ta>
            <ta e="T183" id="Seg_3928" s="T182">np.h:S</ta>
            <ta e="T186" id="Seg_3929" s="T185">v:pred</ta>
            <ta e="T191" id="Seg_3930" s="T190">0.3.h:S v:pred</ta>
            <ta e="T194" id="Seg_3931" s="T193">np:S</ta>
            <ta e="T195" id="Seg_3932" s="T194">v:pred</ta>
            <ta e="T196" id="Seg_3933" s="T195">pro.h:S</ta>
            <ta e="T198" id="Seg_3934" s="T197">np:O</ta>
            <ta e="T200" id="Seg_3935" s="T199">v:pred</ta>
            <ta e="T202" id="Seg_3936" s="T201">np.h:S</ta>
            <ta e="T203" id="Seg_3937" s="T202">v:pred 0.3:O</ta>
            <ta e="T209" id="Seg_3938" s="T208">np:O</ta>
            <ta e="T212" id="Seg_3939" s="T211">0.2.h:S v:pred</ta>
            <ta e="T214" id="Seg_3940" s="T213">np.h:S</ta>
            <ta e="T215" id="Seg_3941" s="T214">v:pred</ta>
            <ta e="T216" id="Seg_3942" s="T215">pro.h:S</ta>
            <ta e="T217" id="Seg_3943" s="T216">pro.h:O</ta>
            <ta e="T219" id="Seg_3944" s="T218">v:pred</ta>
            <ta e="T222" id="Seg_3945" s="T221">0.1.h:S v:pred</ta>
            <ta e="T227" id="Seg_3946" s="T226">0.2.h:S v:pred</ta>
            <ta e="T230" id="Seg_3947" s="T229">np.h:S</ta>
            <ta e="T232" id="Seg_3948" s="T231">v:pred</ta>
            <ta e="T234" id="Seg_3949" s="T233">np.h:S</ta>
            <ta e="T235" id="Seg_3950" s="T234">np:O</ta>
            <ta e="T335" id="Seg_3951" s="T236">v:pred</ta>
            <ta e="T239" id="Seg_3952" s="T238">np:O</ta>
            <ta e="T240" id="Seg_3953" s="T239">0.3.h:S v:pred</ta>
            <ta e="T241" id="Seg_3954" s="T240">np.h:S</ta>
            <ta e="T243" id="Seg_3955" s="T242">v:pred</ta>
            <ta e="T336" id="Seg_3956" s="T244">np.h:S</ta>
            <ta e="T246" id="Seg_3957" s="T245">v:pred 0.3.h:O</ta>
            <ta e="T247" id="Seg_3958" s="T246">np:O</ta>
            <ta e="T250" id="Seg_3959" s="T249">0.3.h:S v:pred</ta>
            <ta e="T260" id="Seg_3960" s="T259">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T263" id="Seg_3961" s="T262">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T266" id="Seg_3962" s="T265">0.3.h:S v:pred</ta>
            <ta e="T269" id="Seg_3963" s="T268">0.3.h:S v:pred</ta>
            <ta e="T270" id="Seg_3964" s="T269">np:S</ta>
            <ta e="T271" id="Seg_3965" s="T270">v:pred</ta>
            <ta e="T276" id="Seg_3966" s="T275">0.3.h:S v:pred</ta>
            <ta e="T280" id="Seg_3967" s="T279">0.3.h:S v:pred</ta>
            <ta e="T282" id="Seg_3968" s="T281">np.h:S</ta>
            <ta e="T283" id="Seg_3969" s="T282">np.h:S</ta>
            <ta e="T286" id="Seg_3970" s="T285">np:O</ta>
            <ta e="T288" id="Seg_3971" s="T287">v:pred</ta>
            <ta e="T290" id="Seg_3972" s="T289">pro.h:S</ta>
            <ta e="T339" id="Seg_3973" s="T291">n:pred</ta>
            <ta e="T294" id="Seg_3974" s="T293">np.h:S</ta>
            <ta e="T296" id="Seg_3975" s="T295">v:pred</ta>
            <ta e="T297" id="Seg_3976" s="T296">pro.h:S</ta>
            <ta e="T298" id="Seg_3977" s="T297">v:pred</ta>
            <ta e="T300" id="Seg_3978" s="T299">np.h:S</ta>
            <ta e="T302" id="Seg_3979" s="T301">np:O</ta>
            <ta e="T303" id="Seg_3980" s="T302">np:O</ta>
            <ta e="T305" id="Seg_3981" s="T304">v:pred</ta>
            <ta e="T307" id="Seg_3982" s="T306">0.3.h:S v:pred 0.3.h:O</ta>
            <ta e="T313" id="Seg_3983" s="T312">0.3.h:S v:pred</ta>
            <ta e="T314" id="Seg_3984" s="T313">0.3.h:S v:pred</ta>
            <ta e="T316" id="Seg_3985" s="T315">np.h:O</ta>
            <ta e="T317" id="Seg_3986" s="T316">np.h:O</ta>
            <ta e="T318" id="Seg_3987" s="T317">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T23" id="Seg_3988" s="T22">RUS:gram</ta>
            <ta e="T31" id="Seg_3989" s="T30">RUS:core</ta>
            <ta e="T33" id="Seg_3990" s="T32">RUS:core</ta>
            <ta e="T37" id="Seg_3991" s="T36">RUS:gram</ta>
            <ta e="T5" id="Seg_3992" s="T42">RUS:gram</ta>
            <ta e="T49" id="Seg_3993" s="T115">RUS:gram</ta>
            <ta e="T58" id="Seg_3994" s="T57">RUS:gram</ta>
            <ta e="T73" id="Seg_3995" s="T72">RUS:core</ta>
            <ta e="T328" id="Seg_3996" s="T82">RUS:core</ta>
            <ta e="T83" id="Seg_3997" s="T328">RUS:gram</ta>
            <ta e="T93" id="Seg_3998" s="T92">RUS:gram</ta>
            <ta e="T142" id="Seg_3999" s="T141">RUS:gram</ta>
            <ta e="T146" id="Seg_4000" s="T145">RUS:gram</ta>
            <ta e="T160" id="Seg_4001" s="T159">RUS:mod</ta>
            <ta e="T193" id="Seg_4002" s="T192">RUS:core</ta>
            <ta e="T207" id="Seg_4003" s="T332">RUS:gram</ta>
            <ta e="T210" id="Seg_4004" s="T209">RUS:core</ta>
            <ta e="T224" id="Seg_4005" s="T223">RUS:core</ta>
            <ta e="T248" id="Seg_4006" s="T247">RUS:core</ta>
            <ta e="T250" id="Seg_4007" s="T249">RUS:core</ta>
            <ta e="T252" id="Seg_4008" s="T251">RUS:core</ta>
            <ta e="T253" id="Seg_4009" s="T252">RUS:core</ta>
            <ta e="T257" id="Seg_4010" s="T256">RUS:core</ta>
            <ta e="T258" id="Seg_4011" s="T337">RUS:cult</ta>
            <ta e="T264" id="Seg_4012" s="T263">RUS:gram</ta>
            <ta e="T311" id="Seg_4013" s="T310">RUS:gram</ta>
            <ta e="T312" id="Seg_4014" s="T311">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T108" id="Seg_4015" s="T105">RUS:calq</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_4016" s="T1">There lived two brothers.</ta>
            <ta e="T11" id="Seg_4017" s="T7">The older brother lived with his wife.</ta>
            <ta e="T16" id="Seg_4018" s="T12">The youngest brother was called Kamacha.</ta>
            <ta e="T21" id="Seg_4019" s="T17">[The older brother] used to go [and lay snares] with Kamacha along the path with deadfall traps.</ta>
            <ta e="T29" id="Seg_4020" s="T22">His, the older brother’s, wife was called daughter-in-law.</ta>
            <ta e="T41" id="Seg_4021" s="T30">Once, they went to the snares, and Kamacha is shouting: “A capercaillie is flying!”</ta>
            <ta e="T48" id="Seg_4022" s="T42">Kamacha is shouting: “Capercaillie, leave the eggs and blood for me!”</ta>
            <ta e="T56" id="Seg_4023" s="T115">And the older brother asks Kamacha: “Why are you shouting so?”</ta>
            <ta e="T66" id="Seg_4024" s="T57">“I’m shouting so because the daughter-in-law fed me eggs and blood.”</ta>
            <ta e="T71" id="Seg_4025" s="T67">They walked and returned.</ta>
            <ta e="T81" id="Seg_4026" s="T72">Now, the older brother catches his wife and, catching like this, beats [her] up [=kills].</ta>
            <ta e="T91" id="Seg_4027" s="T82">And tomorrow he goes by himself, the older brother, to check on the snares.</ta>
            <ta e="T97" id="Seg_4028" s="T92">And the daughter-in-law, having caught Kamacha, beats him.</ta>
            <ta e="T103" id="Seg_4029" s="T98">Kamacha, having gone out of the house into the street, leaves.</ta>
            <ta e="T108" id="Seg_4030" s="T104">He goes wherever his feet are taking him.</ta>
            <ta e="T113" id="Seg_4031" s="T109">He walks and sees: a crow is flying.</ta>
            <ta e="T118" id="Seg_4032" s="T114">He looks – it is carrying a fish in its beak.</ta>
            <ta e="T127" id="Seg_4033" s="T119">He, Kamacha, will go there where this crow comes from.</ta>
            <ta e="T131" id="Seg_4034" s="T128">He goes and reaches the river.</ta>
            <ta e="T137" id="Seg_4035" s="T132">In this river there is a lot of fish.</ta>
            <ta e="T321" id="Seg_4036" s="T324">He cuts down…</ta>
            <ta e="T144" id="Seg_4037" s="T138">He cuts down the bird cherry and makes a scoop-net.</ta>
            <ta e="T148" id="Seg_4038" s="T145">He starts scooping the fish.</ta>
            <ta e="T153" id="Seg_4039" s="T149">He scoops fish and starts a fire.</ta>
            <ta e="T158" id="Seg_4040" s="T154">He puts fish on sticks to the fire.</ta>
            <ta e="T163" id="Seg_4041" s="T159">Suddenly he looks: a beast is coming.</ta>
            <ta e="T170" id="Seg_4042" s="T164">The beast is coming, asking Kamacha: “What are you doing?”</ta>
            <ta e="T175" id="Seg_4043" s="T171">“I’m roasting my kidney on the fire.</ta>
            <ta e="T181" id="Seg_4044" s="T176">Beast, sit down in front, on the other side of the fire.”</ta>
            <ta e="T186" id="Seg_4045" s="T182">The beast sits on the other side of the fire.</ta>
            <ta e="T191" id="Seg_4046" s="T187">[He says]: “Kamacha, give me some, too!”</ta>
            <ta e="T200" id="Seg_4047" s="T192">Now [the fish] is cooked – he throws one stick to the beast.</ta>
            <ta e="T333" id="Seg_4048" s="T201">The beast eats: “Oh, this is delicious!</ta>
            <ta e="T212" id="Seg_4049" s="T206">Cut [=Dig] my kidneys out, too!”</ta>
            <ta e="T219" id="Seg_4050" s="T213">Kamacha says: “You won’t eat me?”</ta>
            <ta e="T222" id="Seg_4051" s="T220">I won’t eat you!</ta>
            <ta e="T227" id="Seg_4052" s="T223">Now Kamacha: “Well, lie down on your back!”</ta>
            <ta e="T232" id="Seg_4053" s="T228">This beast lies down on his back.</ta>
            <ta e="T243" id="Seg_4054" s="T233">Kamacha sharpens the knife, cuts open the beast’s stomach, the beast dies.</ta>
            <ta e="T250" id="Seg_4055" s="T244">Kamacha strips him, takes all the meat.</ta>
            <ta e="T266" id="Seg_4056" s="T251">Then all the next day, he puts everything in the knapsack, loads the bag full and sets out home.</ta>
            <ta e="T271" id="Seg_4057" s="T267">He goes up to his house, there is no smoke.</ta>
            <ta e="T280" id="Seg_4058" s="T272">He climbs up the roof and looks down the chimney.</ta>
            <ta e="T288" id="Seg_4059" s="T281">His brother and the daughter-in-law are sharing one spark, half and half.</ta>
            <ta e="T339" id="Seg_4060" s="T289">“This is for my Kamacha.”</ta>
            <ta e="T292" id="Seg_4061" s="T338">[?]</ta>
            <ta e="T298" id="Seg_4062" s="T293">Kamacha shouts: “I’m here!”</ta>
            <ta e="T305" id="Seg_4063" s="T299">Kamacha lowers fish and meat down to them.</ta>
            <ta e="T309" id="Seg_4064" s="T306">He feeds them fish and meat.</ta>
            <ta e="T314" id="Seg_4065" s="T310">Then they live and live.</ta>
            <ta e="T318" id="Seg_4066" s="T315">They had [=found?] a daughter and a son.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_4067" s="T1">Es lebten zwei Brüder.</ta>
            <ta e="T11" id="Seg_4068" s="T7">Der älter Bruder lebte mit seiner Frau.</ta>
            <ta e="T16" id="Seg_4069" s="T12">Der jüngste Bruder hieß Kamatscha.</ta>
            <ta e="T21" id="Seg_4070" s="T17">[Der älter Bruder] ging mit Kamatscha [um Schlingen auszulegen] auf dem Weg mit Totfallen.</ta>
            <ta e="T29" id="Seg_4071" s="T22">Seine, des älteren Bruders, Frau hieß Schwiegertochter.</ta>
            <ta e="T41" id="Seg_4072" s="T30">Einmal, sie gingen mit den Fallstricken, ruft Kamatscha: „Ein Auerhuhn fliegt!“</ta>
            <ta e="T48" id="Seg_4073" s="T42">Kamatscha ruft: „Auerhuhn, lass mir die Eier und das Blut!“</ta>
            <ta e="T56" id="Seg_4074" s="T115">Und der älter Bruder fragt Kamatscha: „Warum rufst du so?“</ta>
            <ta e="T66" id="Seg_4075" s="T57">„Ich rufe so, weil die Schwiegertochter mir Eier und Blut zu essen gab.“</ta>
            <ta e="T71" id="Seg_4076" s="T67">Sie liefen und kehrten zurück.</ta>
            <ta e="T81" id="Seg_4077" s="T72">Nun, der ältere Bruder fängt seine Frau, und fängt sie so, schlägt [sie] [=tötet sie].</ta>
            <ta e="T91" id="Seg_4078" s="T82">Und morgen geht er alleine los, der ältere Bruder, um die Schlingen zu kontrollieren.</ta>
            <ta e="T97" id="Seg_4079" s="T92">Und die Schwiegertochter fängt Kamatscha und schlägt ihn.</ta>
            <ta e="T103" id="Seg_4080" s="T98">Kamatscha, der aus dem Haus auf die Straße gegangen ist, geht.</ta>
            <ta e="T108" id="Seg_4081" s="T104">Er geht wohin auch immer seine Füße ihn führen.</ta>
            <ta e="T113" id="Seg_4082" s="T109">Er läuft und sieht: eine Krähe fliegt.</ta>
            <ta e="T118" id="Seg_4083" s="T114">Er schaut – sie trägt einen Fisch im Schnabel.</ta>
            <ta e="T127" id="Seg_4084" s="T119">Er, Kamatscha, wird dorthin gehen, woher diese Krähe kommt.</ta>
            <ta e="T131" id="Seg_4085" s="T128">Er geht und erreicht den Fluss.</ta>
            <ta e="T137" id="Seg_4086" s="T132">In diesem Fluss sind viele Fische.</ta>
            <ta e="T321" id="Seg_4087" s="T324">Er fällt…</ta>
            <ta e="T144" id="Seg_4088" s="T138">Er fällt eine Vogelkirsche und macht ein Fischnetz.</ta>
            <ta e="T148" id="Seg_4089" s="T145">Er fängt an, die Fische zu fangen.</ta>
            <ta e="T153" id="Seg_4090" s="T149">Er fängt Fische und legt ein Feuer.</ta>
            <ta e="T158" id="Seg_4091" s="T154">Er legt Fische auf Stöcke am Feuer.</ta>
            <ta e="T163" id="Seg_4092" s="T159">Plötzlich schaut er: ein Ungeheuer kommt.</ta>
            <ta e="T170" id="Seg_4093" s="T164">Das Ungeheuer kommt, fragt Kamatscha: „Was machst du?“</ta>
            <ta e="T175" id="Seg_4094" s="T171">„Ich brate meine Niere am Feuer.</ta>
            <ta e="T181" id="Seg_4095" s="T176">Ungeheuer, setz dich gegenüber, auf die andere Seite des Feuers.“</ta>
            <ta e="T186" id="Seg_4096" s="T182">Das Ungeheuer setzt sich auf der anderen Seite des Feuers.</ta>
            <ta e="T191" id="Seg_4097" s="T187">[Es sagt]: „Kamatscha, gib mir auch etwas!“</ta>
            <ta e="T200" id="Seg_4098" s="T192">Nun ist [der Fisch] gekocht – er wirft dem Ungeheuer einen Stock.</ta>
            <ta e="T333" id="Seg_4099" s="T201">Das Ungeheuer frisst: „Oh, das ist köstlich! </ta>
            <ta e="T212" id="Seg_4100" s="T206">Steche mir auch die Nieren aus!“</ta>
            <ta e="T219" id="Seg_4101" s="T213">Kamatscha sagt: “Du frisst mich nicht?“</ta>
            <ta e="T222" id="Seg_4102" s="T220">Ich fresse dich nicht</ta>
            <ta e="T227" id="Seg_4103" s="T223">Nun Kamatscha: „Nun, lege dich auf den Rücken!“</ta>
            <ta e="T232" id="Seg_4104" s="T228">Dieses Ungeheuer legt sich auf den Rücken.</ta>
            <ta e="T243" id="Seg_4105" s="T233">Kamatscha schärft das Messer, schneidet den Bauch des Ungeheuers auf, das Ungeheuer stirbt.</ta>
            <ta e="T250" id="Seg_4106" s="T244">Kamatscha häutet es, nimmt das ganze Fleisch.</ta>
            <ta e="T266" id="Seg_4107" s="T251">Dann den ganzen nächsten Tag, steckt er alles in seinen Proviantbeutel, packt er die Tasche voll, und macht sich auf dem Heimweg.</ta>
            <ta e="T271" id="Seg_4108" s="T267">Er geht hinauf zu seinem Haus, dort ist kein Rauch.</ta>
            <ta e="T280" id="Seg_4109" s="T272">Er klettert aufs Dach und schaut durch den Schornstein hinunter.</ta>
            <ta e="T288" id="Seg_4110" s="T281">Sein Bruder und die Schwiegertochter teilen sich einen Funken, halbe-halbe.</ta>
            <ta e="T339" id="Seg_4111" s="T289">„Das ist für meinen Kamatscha.“</ta>
            <ta e="T292" id="Seg_4112" s="T338">[?]</ta>
            <ta e="T298" id="Seg_4113" s="T293">Kamatscha ruft: „Ich bin hier!“</ta>
            <ta e="T305" id="Seg_4114" s="T299">Kamatscha lässt ihnen Fisch und Fleisch hinab.</ta>
            <ta e="T309" id="Seg_4115" s="T306">Er gibt ihnen Fisch und Fleisch zu essen.</ta>
            <ta e="T314" id="Seg_4116" s="T310">Dann leben sie und leben.</ta>
            <ta e="T318" id="Seg_4117" s="T315">Sie bekamen [=fanden?] eine Tochter und einen Sohn.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_4118" s="T1">Жили-были два брата.</ta>
            <ta e="T11" id="Seg_4119" s="T7">Старший брат с женой жил.</ta>
            <ta e="T16" id="Seg_4120" s="T12">Младшего брата звали Камача.</ta>
            <ta e="T21" id="Seg_4121" s="T17">[Старший брат] ходил с Камачей [ставить петли] по дороге со слопцами.</ta>
            <ta e="T29" id="Seg_4122" s="T22">У него, старшего брата, жену называли сноха.</ta>
            <ta e="T41" id="Seg_4123" s="T30">Однажды, пошли на петли, а Камача кричит: «Летит глухарь!»</ta>
            <ta e="T48" id="Seg_4124" s="T42">Камача кричит: «Глухарь, мне яйца-кровь оставь!»</ta>
            <ta e="T56" id="Seg_4125" s="T115">А старший брат у Камачи спрашивает: «Зачем так кричишь?»</ta>
            <ta e="T66" id="Seg_4126" s="T57">«А я потому так кричу, меня сноха кровью-яйцами кормила».</ta>
            <ta e="T71" id="Seg_4127" s="T67">Там ходили, обратно вернулись.</ta>
            <ta e="T81" id="Seg_4128" s="T72">Теперь старший брат схватит жену и, так схватив, изобьёт [=убьёт].</ta>
            <ta e="T91" id="Seg_4129" s="T82">А завтра сам один пойдёт старший брат проверять петли.</ta>
            <ta e="T97" id="Seg_4130" s="T92">А сноха, схватив Камачу, побьёт.</ta>
            <ta e="T103" id="Seg_4131" s="T98">Камача, из дома на улицу выйдя, уйдёт.</ta>
            <ta e="T108" id="Seg_4132" s="T104">Уйдёт, куда глаза глядят.</ta>
            <ta e="T113" id="Seg_4133" s="T109">Идёт, видит: ворон летит.</ta>
            <ta e="T118" id="Seg_4134" s="T114">Смотрит — во рту несёт рыбу.</ta>
            <ta e="T127" id="Seg_4135" s="T119">Он, Камача, откуда этот ворон летит, туда пойдёт.</ta>
            <ta e="T131" id="Seg_4136" s="T128">Пойдёт, дойдёт до реки.</ta>
            <ta e="T137" id="Seg_4137" s="T132">В этой реке так много рыбы.</ta>
            <ta e="T321" id="Seg_4138" s="T324">Он отрубит…</ta>
            <ta e="T144" id="Seg_4139" s="T138">Он отрубит черёмуху и сачок сделает.</ta>
            <ta e="T148" id="Seg_4140" s="T145">Давай рыбу сачком ловить.</ta>
            <ta e="T153" id="Seg_4141" s="T149">Наловив рыбы, костёр разведёт.</ta>
            <ta e="T158" id="Seg_4142" s="T154">К огню рыбу ставит на палочках.</ta>
            <ta e="T163" id="Seg_4143" s="T159">Вдруг смотрит: зверь идёт.</ta>
            <ta e="T170" id="Seg_4144" s="T164">Зверь придёт, спросит у Камачи: «Что делаешь?»</ta>
            <ta e="T175" id="Seg_4145" s="T171">«Я почку свою на огне жарю.</ta>
            <ta e="T181" id="Seg_4146" s="T176">Зверь, напротив садись, против костра.»</ta>
            <ta e="T186" id="Seg_4147" s="T182">Зверь против костра сядет.</ta>
            <ta e="T191" id="Seg_4148" s="T187">[Говорит]: «Камача, мне тоже дай!»</ta>
            <ta e="T200" id="Seg_4149" s="T192">Теперь [рыба] поджарится — он одну палочку зверю бросит.</ta>
            <ta e="T333" id="Seg_4150" s="T201">Зверь съест: «Ох, вкусно это! </ta>
            <ta e="T212" id="Seg_4151" s="T206">Давай мои почки тоже вырежи [=выкопай]!»</ta>
            <ta e="T219" id="Seg_4152" s="T213">Камача говорит: «Ты меня не съешь?»</ta>
            <ta e="T222" id="Seg_4153" s="T220">«Не съем!»</ta>
            <ta e="T227" id="Seg_4154" s="T223">Теперь Камача: «Ну, навзничь ложись!»</ta>
            <ta e="T232" id="Seg_4155" s="T228">Этот зверь навзничь ляжет.</ta>
            <ta e="T243" id="Seg_4156" s="T233">Камача нож остро наточит, зверю живот распорет, зверь тут умрёт.</ta>
            <ta e="T250" id="Seg_4157" s="T244">Камача обдерёт, мясо всё прочь уберёт.</ta>
            <ta e="T266" id="Seg_4158" s="T251">Потом весь день, всё в котомку положит, полный мешок наложит и домой отправится.</ta>
            <ta e="T271" id="Seg_4159" s="T267">До дома доходит, дыма нет.</ta>
            <ta e="T280" id="Seg_4160" s="T272">На крышу наверх залезет, в трубу вниз смотрит.</ta>
            <ta e="T288" id="Seg_4161" s="T281">Брат со снохой одну искорку пополам делят.</ta>
            <ta e="T339" id="Seg_4162" s="T289">«Это моему Камаче».</ta>
            <ta e="T292" id="Seg_4163" s="T338">[?]</ta>
            <ta e="T298" id="Seg_4164" s="T293">Камача там закричит: «Я здесь!»</ta>
            <ta e="T305" id="Seg_4165" s="T299">Камача им двоим рыбу, мясо вниз спустит.</ta>
            <ta e="T309" id="Seg_4166" s="T306">Накормит рыбой, мясом.</ta>
            <ta e="T314" id="Seg_4167" s="T310">А потом стали жить-поживать.</ta>
            <ta e="T318" id="Seg_4168" s="T315">Дочь, сына родили [=нашли?].</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_4169" s="T1">С братьями было. Много спали.</ta>
            <ta e="T11" id="Seg_4170" s="T7">Старший брат с женой жили.</ta>
            <ta e="T16" id="Seg_4171" s="T12">Младшего брата звали Камаджа.</ta>
            <ta e="T21" id="Seg_4172" s="T17">Камаджа ходил ставить петли.</ta>
            <ta e="T29" id="Seg_4173" s="T22">У него, старшего брата, жену называли сноха.</ta>
            <ta e="T41" id="Seg_4174" s="T30">Однажды, пошли на петли, а Камаджа кричит: «Летит глухарь!» </ta>
            <ta e="T48" id="Seg_4175" s="T42">Камаджа кричит: «Глухарь, мне яйца-кровь оставь!»</ta>
            <ta e="T56" id="Seg_4176" s="T115">Старший брат у Камаджи спрашивает: «Зачем так кричишь?»</ta>
            <ta e="T66" id="Seg_4177" s="T57">«А я потому так кричу, меня сноха кровью-яйцами кормила».</ta>
            <ta e="T71" id="Seg_4178" s="T67">Там ходили, обратно вернулись.</ta>
            <ta e="T81" id="Seg_4179" s="T72">Теперь старший брат схватит [жену] силой и, сюда схватив, изобьёт [=убьёт].</ta>
            <ta e="T91" id="Seg_4180" s="T82">А завтра сам один пойдёт старший брат проверять петли.</ta>
            <ta e="T97" id="Seg_4181" s="T92">А сноха, схватив Камаджу, останется.</ta>
            <ta e="T103" id="Seg_4182" s="T98">Камаджа, из дома на улицу выйдя, уйдёт.</ta>
            <ta e="T108" id="Seg_4183" s="T104">Уйдёт, куда глаза глядят.</ta>
            <ta e="T113" id="Seg_4184" s="T109">Идёт, видит: ворон летит.</ta>
            <ta e="T118" id="Seg_4185" s="T114">Смотрит — во рту несёт рыбу.</ta>
            <ta e="T127" id="Seg_4186" s="T119">Он, Камаджа, где этот ворон был, туда пойдёт.</ta>
            <ta e="T131" id="Seg_4187" s="T128">Пойдёт, дойдёт до реки.</ta>
            <ta e="T137" id="Seg_4188" s="T132">В этой реке так много рыбы.</ta>
            <ta e="T144" id="Seg_4189" s="T138">Он отрубит черёмуху и сачок сделает.</ta>
            <ta e="T148" id="Seg_4190" s="T145">Давай рыбу черпать.</ta>
            <ta e="T153" id="Seg_4191" s="T149">Начерпает рыбы, костёр разведёт.</ta>
            <ta e="T158" id="Seg_4192" s="T154">На берегу рыбу накоптит.</ta>
            <ta e="T163" id="Seg_4193" s="T159">Вдруг смотрит: зверь идёт.</ta>
            <ta e="T170" id="Seg_4194" s="T164">Зверь придёт, спросит у Камаджи: «Что делаешь?»</ta>
            <ta e="T175" id="Seg_4195" s="T171">«Я почки на берегу жарю.</ta>
            <ta e="T181" id="Seg_4196" s="T176">Зверь, напротив садись, против костра.»</ta>
            <ta e="T186" id="Seg_4197" s="T182">Зверь против костра сядет.</ta>
            <ta e="T191" id="Seg_4198" s="T187">Камадже [говорит]: «Мне тоже дай!»</ta>
            <ta e="T200" id="Seg_4199" s="T192">Теперь лист сварится — он один лист зверю бросит.</ta>
            <ta e="T333" id="Seg_4200" s="T201">Зверь съест: «Ох, вкусное это!</ta>
            <ta e="T212" id="Seg_4201" s="T206">Давай мои почки тоже вырежи [=выкопай]!»</ta>
            <ta e="T219" id="Seg_4202" s="T213">Камаджа говорит: «Ты меня не съешь?»</ta>
            <ta e="T222" id="Seg_4203" s="T220">Не съем!</ta>
            <ta e="T227" id="Seg_4204" s="T223">Теперь Камаджа: «Навзничь усни!»</ta>
            <ta e="T232" id="Seg_4205" s="T228">Этот зверь навзничь (на спине) уснёт.</ta>
            <ta e="T243" id="Seg_4206" s="T233">Камаджа нож остро наточит, зверю живот распорет, зверь тут умрёт.</ta>
            <ta e="T250" id="Seg_4207" s="T244">Камаджа обдерёт, мясо всё прочь уберёт.</ta>
            <ta e="T266" id="Seg_4208" s="T251">Потом весь день, взяв котомку, в мешок накладёт, полный мешок накладёт и домой отправится.</ta>
            <ta e="T271" id="Seg_4209" s="T267">До дома доходит, дыма нет.</ta>
            <ta e="T280" id="Seg_4210" s="T272">На крышу наверх залезет, в трубу вниз смотрит.</ta>
            <ta e="T288" id="Seg_4211" s="T281">Брат, сноха один огонь пополам делят.</ta>
            <ta e="T339" id="Seg_4212" s="T289">«Это моему Камадже».</ta>
            <ta e="T298" id="Seg_4213" s="T293">Камаджа там закричит: «Я здесь!»</ta>
            <ta e="T305" id="Seg_4214" s="T299">Камаджа им двоим рыбу, мясо вниз спустит.</ta>
            <ta e="T309" id="Seg_4215" s="T306">Накормит рыбой, мясом.</ta>
            <ta e="T314" id="Seg_4216" s="T310">А потом живут-поживают.</ta>
            <ta e="T318" id="Seg_4217" s="T315">Дочь, сына родили (нашли).</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T6" id="Seg_4218" s="T1">[AAV:] most likely, (š)idə tɨmnʼäsɨɣa (cf. Kamadzha1); the verb might have a (DU) suffix cut off by tape restart.</ta>
            <ta e="T29" id="Seg_4219" s="T22">[AAV:] emam (Tym): "bride" (B 311)</ta>
            <ta e="T48" id="Seg_4220" s="T42">[AAV:] "Aŋgə": long final vowel (ə/e, not a), vocative; "qoqal-qɨmbal": "ribs and heads" in Kamadzha1</ta>
            <ta e="T81" id="Seg_4221" s="T72">[AAV:] orallde; overlapping voices</ta>
            <ta e="T144" id="Seg_4222" s="T138">[WNB:] The analysis of the verbs is tentaive.</ta>
            <ta e="T163" id="Seg_4223" s="T159">[AAV:] mančeǯʼla-k [-1SG.O] ? </ta>
            <ta e="T170" id="Seg_4224" s="T164">[AAV:] mešpɨndaɣ ? Expected -l [-2SG.O], as in Kamadzha1.</ta>
            <ta e="T191" id="Seg_4225" s="T187">[WNB:] The verb form is not clear, it should be IMP.2SG.</ta>
            <ta e="T212" id="Seg_4226" s="T206">[AAV:] makɨlm [-1SG] ?; Dawaj here is adressed to 2 pers., not hortative</ta>
            <ta e="T232" id="Seg_4227" s="T228">[AAV:] Tabə / taw ?</ta>
            <ta e="T243" id="Seg_4228" s="T233">[AAV:] a(ɣ)ɨlde ?</ta>
            <ta e="T266" id="Seg_4229" s="T251">[AAV:] üpalʼčəma ?</ta>
            <ta e="T280" id="Seg_4230" s="T272">[AAV:] par(l) enne ?; par(m) elle ?; cf. šölʼ "navel"</ta>
            <ta e="T298" id="Seg_4231" s="T293">[AAV:] načʼaɣət ?</ta>
            <ta e="T318" id="Seg_4232" s="T315">[AAV:] the last verb is unclear.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T319" />
            <conversion-tli id="T2" />
            <conversion-tli id="T320" />
            <conversion-tli id="T4" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T3" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T5" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T115" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T326" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T78" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T327" />
            <conversion-tli id="T77" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T328" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T324" />
            <conversion-tli id="T329" />
            <conversion-tli id="T323" />
            <conversion-tli id="T330" />
            <conversion-tli id="T322" />
            <conversion-tli id="T325" />
            <conversion-tli id="T321" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T331" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T333" />
            <conversion-tli id="T206" />
            <conversion-tli id="T332" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T334" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T335" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T336" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T337" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T339" />
            <conversion-tli id="T338" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
