<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>AR_1965_ThoughtsOfStepan_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">AR_1965_ThoughtsOfStepan_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">91</ud-information>
            <ud-information attribute-name="# HIAT:w">70</ud-information>
            <ud-information attribute-name="# e">69</ud-information>
            <ud-information attribute-name="# HIAT:u">11</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="AR">
            <abbreviation>AR</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T295" />
         <tli id="T296" />
         <tli id="T297" />
         <tli id="T298" />
         <tli id="T299" />
         <tli id="T300" />
         <tli id="T301" />
         <tli id="T302" />
         <tli id="T303" />
         <tli id="T304" />
         <tli id="T305" />
         <tli id="T306" />
         <tli id="T307" />
         <tli id="T308" />
         <tli id="T309" />
         <tli id="T310" />
         <tli id="T311" />
         <tli id="T312" />
         <tli id="T313" />
         <tli id="T314" />
         <tli id="T315" />
         <tli id="T316" />
         <tli id="T317" />
         <tli id="T318" />
         <tli id="T319" />
         <tli id="T320" />
         <tli id="T321" />
         <tli id="T322" />
         <tli id="T323" />
         <tli id="T324" />
         <tli id="T325" />
         <tli id="T326" />
         <tli id="T327" />
         <tli id="T328" />
         <tli id="T329" />
         <tli id="T330" />
         <tli id="T331" />
         <tli id="T332" />
         <tli id="T333" />
         <tli id="T334" />
         <tli id="T335" />
         <tli id="T336" />
         <tli id="T337" />
         <tli id="T338" />
         <tli id="T339" />
         <tli id="T340" />
         <tli id="T341" />
         <tli id="T342" />
         <tli id="T343" />
         <tli id="T344" />
         <tli id="T345" />
         <tli id="T346" />
         <tli id="T347" />
         <tli id="T348" />
         <tli id="T349" />
         <tli id="T350" />
         <tli id="T351" />
         <tli id="T352" />
         <tli id="T353" />
         <tli id="T354" />
         <tli id="T355" />
         <tli id="T356" />
         <tli id="T357" />
         <tli id="T358" />
         <tli id="T359" />
         <tli id="T360" />
         <tli id="T361" />
         <tli id="T362" />
         <tli id="T363" />
         <tli id="T364" />
         <tli id="T365" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="AR"
                      type="t">
         <timeline-fork end="T364" start="T362">
            <tli id="T362.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T365" id="Seg_0" n="sc" s="T295">
               <ts e="T300" id="Seg_2" n="HIAT:u" s="T295">
                  <ts e="T296" id="Seg_4" n="HIAT:w" s="T295">Stepan</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_7" n="HIAT:w" s="T296">alʼčʼisɨ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_9" n="HIAT:ip">(</nts>
                  <ts e="T298" id="Seg_11" n="HIAT:w" s="T297">paktɨrtɔːlsä</ts>
                  <nts id="Seg_12" n="HIAT:ip">)</nts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_15" n="HIAT:w" s="T298">pötpɨlʼ</ts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_18" n="HIAT:w" s="T299">üttə</ts>
                  <nts id="Seg_19" n="HIAT:ip">.</nts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T319" id="Seg_22" n="HIAT:u" s="T300">
                  <ts e="T301" id="Seg_24" n="HIAT:w" s="T300">Täm</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_27" n="HIAT:w" s="T301">uːqɨltɨsätɨ</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_30" n="HIAT:w" s="T302">kikap</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_33" n="HIAT:w" s="T303">šitə</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_36" n="HIAT:w" s="T304">pɔːrɨ</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_39" n="HIAT:w" s="T305">tɨː</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_42" n="HIAT:w" s="T306">i</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_45" n="HIAT:w" s="T307">moqona</ts>
                  <nts id="Seg_46" n="HIAT:ip">,</nts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_49" n="HIAT:w" s="T308">tantɨs</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_52" n="HIAT:w" s="T309">üt</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_55" n="HIAT:w" s="T310">nɔːnɨ</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_58" n="HIAT:w" s="T311">qanäktɨ</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_61" n="HIAT:w" s="T312">i</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_64" n="HIAT:w" s="T313">mɨkaj</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_67" n="HIAT:w" s="T314">soma</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_70" n="HIAT:w" s="T315">čʼuntɨ</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_73" n="HIAT:w" s="T316">paktɨsa</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_76" n="HIAT:w" s="T317">kika</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_79" n="HIAT:w" s="T318">nɔːnɨ</ts>
                  <nts id="Seg_80" n="HIAT:ip">.</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T326" id="Seg_83" n="HIAT:u" s="T319">
                  <ts e="T320" id="Seg_85" n="HIAT:w" s="T319">Moqonɨ</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_88" n="HIAT:w" s="T320">nän</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_91" n="HIAT:w" s="T321">qalɨsɔːtɨn</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_94" n="HIAT:w" s="T322">čʼuːran</ts>
                  <nts id="Seg_95" n="HIAT:ip">,</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_98" n="HIAT:w" s="T323">kika</ts>
                  <nts id="Seg_99" n="HIAT:ip">,</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_102" n="HIAT:w" s="T324">pirqɨ</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_105" n="HIAT:w" s="T325">lotɨk</ts>
                  <nts id="Seg_106" n="HIAT:ip">.</nts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T331" id="Seg_109" n="HIAT:u" s="T326">
                  <ts e="T327" id="Seg_111" n="HIAT:w" s="T326">Meː</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_114" n="HIAT:w" s="T327">mɨqɨnɨn</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_117" n="HIAT:w" s="T328">aša</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_120" n="HIAT:w" s="T329">nɨlʼčʼiŋ</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_123" n="HIAT:w" s="T330">ɛːŋa</ts>
                  <nts id="Seg_124" n="HIAT:ip">.</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T334" id="Seg_127" n="HIAT:u" s="T331">
                  <ts e="T332" id="Seg_129" n="HIAT:w" s="T331">Tap</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_132" n="HIAT:w" s="T332">qalʼtärä</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_135" n="HIAT:w" s="T333">uːtɨlʼpokɔːlɨŋ</ts>
                  <nts id="Seg_136" n="HIAT:ip">.</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T345" id="Seg_139" n="HIAT:u" s="T334">
                  <ts e="T335" id="Seg_141" n="HIAT:w" s="T334">Täpɨnɨŋ</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_144" n="HIAT:w" s="T335">čʼaptäːqɨn</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_147" n="HIAT:w" s="T336">kɨkɨmpa</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_150" n="HIAT:w" s="T337">tɛnɨmɨqa</ts>
                  <nts id="Seg_151" n="HIAT:ip">,</nts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_154" n="HIAT:w" s="T338">qaj</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_157" n="HIAT:w" s="T339">meːtɨntɔːtɨn</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_160" n="HIAT:w" s="T340">nɨmtɨ</ts>
                  <nts id="Seg_161" n="HIAT:ip">,</nts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_164" n="HIAT:w" s="T341">qajikoš</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_167" n="HIAT:w" s="T342">tomtɔːtɨn</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_170" n="HIAT:w" s="T343">Мoskva</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_173" n="HIAT:w" s="T344">čʼɔːtɨ</ts>
                  <nts id="Seg_174" n="HIAT:ip">.</nts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T348" id="Seg_177" n="HIAT:u" s="T345">
                  <ts e="T346" id="Seg_179" n="HIAT:w" s="T345">Stepan</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_182" n="HIAT:w" s="T346">nɨŋa</ts>
                  <nts id="Seg_183" n="HIAT:ip">,</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_186" n="HIAT:w" s="T347">tɛnɨrpa</ts>
                  <nts id="Seg_187" n="HIAT:ip">.</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T349" id="Seg_190" n="HIAT:u" s="T348">
                  <ts e="T349" id="Seg_192" n="HIAT:w" s="T348">Nɨllaje</ts>
                  <nts id="Seg_193" n="HIAT:ip">.</nts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T355" id="Seg_196" n="HIAT:u" s="T349">
                  <ts e="T350" id="Seg_198" n="HIAT:w" s="T349">Kočʼi</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_201" n="HIAT:w" s="T350">qumɨn</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_204" n="HIAT:w" s="T351">na</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_207" n="HIAT:w" s="T352">tätaqɨn</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_210" n="HIAT:w" s="T353">müttɨsɔːtɨn</ts>
                  <nts id="Seg_211" n="HIAT:ip">,</nts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_214" n="HIAT:w" s="T354">qukkɔːtɨn</ts>
                  <nts id="Seg_215" n="HIAT:ip">.</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T360" id="Seg_218" n="HIAT:u" s="T355">
                  <ts e="T356" id="Seg_220" n="HIAT:w" s="T355">Tap</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_223" n="HIAT:w" s="T356">tättɨ</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_226" n="HIAT:w" s="T357">muqultıːrɨŋ</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_229" n="HIAT:w" s="T358">kämsa</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_232" n="HIAT:w" s="T359">qampolʼıːmpa</ts>
                  <nts id="Seg_233" n="HIAT:ip">.</nts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T365" id="Seg_236" n="HIAT:u" s="T360">
                  <ts e="T361" id="Seg_238" n="HIAT:w" s="T360">Täp</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_241" n="HIAT:w" s="T361">čʼuːra</ts>
                  <nts id="Seg_242" n="HIAT:ip">,</nts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362.tx.1" id="Seg_245" n="HIAT:w" s="T362">saintɨ</ts>
                  <nts id="Seg_246" n="HIAT:ip">_</nts>
                  <ts e="T364" id="Seg_248" n="HIAT:w" s="T362.tx.1">qoni</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_251" n="HIAT:w" s="T364">qampalpa</ts>
                  <nts id="Seg_252" n="HIAT:ip">.</nts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T365" id="Seg_254" n="sc" s="T295">
               <ts e="T296" id="Seg_256" n="e" s="T295">Stepan </ts>
               <ts e="T297" id="Seg_258" n="e" s="T296">alʼčʼisɨ </ts>
               <ts e="T298" id="Seg_260" n="e" s="T297">(paktɨrtɔːlsä) </ts>
               <ts e="T299" id="Seg_262" n="e" s="T298">pötpɨlʼ </ts>
               <ts e="T300" id="Seg_264" n="e" s="T299">üttə. </ts>
               <ts e="T301" id="Seg_266" n="e" s="T300">Täm </ts>
               <ts e="T302" id="Seg_268" n="e" s="T301">uːqɨltɨsätɨ </ts>
               <ts e="T303" id="Seg_270" n="e" s="T302">kikap </ts>
               <ts e="T304" id="Seg_272" n="e" s="T303">šitə </ts>
               <ts e="T305" id="Seg_274" n="e" s="T304">pɔːrɨ </ts>
               <ts e="T306" id="Seg_276" n="e" s="T305">tɨː </ts>
               <ts e="T307" id="Seg_278" n="e" s="T306">i </ts>
               <ts e="T308" id="Seg_280" n="e" s="T307">moqona, </ts>
               <ts e="T309" id="Seg_282" n="e" s="T308">tantɨs </ts>
               <ts e="T310" id="Seg_284" n="e" s="T309">üt </ts>
               <ts e="T311" id="Seg_286" n="e" s="T310">nɔːnɨ </ts>
               <ts e="T312" id="Seg_288" n="e" s="T311">qanäktɨ </ts>
               <ts e="T313" id="Seg_290" n="e" s="T312">i </ts>
               <ts e="T314" id="Seg_292" n="e" s="T313">mɨkaj </ts>
               <ts e="T315" id="Seg_294" n="e" s="T314">soma </ts>
               <ts e="T316" id="Seg_296" n="e" s="T315">čʼuntɨ </ts>
               <ts e="T317" id="Seg_298" n="e" s="T316">paktɨsa </ts>
               <ts e="T318" id="Seg_300" n="e" s="T317">kika </ts>
               <ts e="T319" id="Seg_302" n="e" s="T318">nɔːnɨ. </ts>
               <ts e="T320" id="Seg_304" n="e" s="T319">Moqonɨ </ts>
               <ts e="T321" id="Seg_306" n="e" s="T320">nän </ts>
               <ts e="T322" id="Seg_308" n="e" s="T321">qalɨsɔːtɨn </ts>
               <ts e="T323" id="Seg_310" n="e" s="T322">čʼuːran, </ts>
               <ts e="T324" id="Seg_312" n="e" s="T323">kika, </ts>
               <ts e="T325" id="Seg_314" n="e" s="T324">pirqɨ </ts>
               <ts e="T326" id="Seg_316" n="e" s="T325">lotɨk. </ts>
               <ts e="T327" id="Seg_318" n="e" s="T326">Meː </ts>
               <ts e="T328" id="Seg_320" n="e" s="T327">mɨqɨnɨn </ts>
               <ts e="T329" id="Seg_322" n="e" s="T328">aša </ts>
               <ts e="T330" id="Seg_324" n="e" s="T329">nɨlʼčʼiŋ </ts>
               <ts e="T331" id="Seg_326" n="e" s="T330">ɛːŋa. </ts>
               <ts e="T332" id="Seg_328" n="e" s="T331">Tap </ts>
               <ts e="T333" id="Seg_330" n="e" s="T332">qalʼtärä </ts>
               <ts e="T334" id="Seg_332" n="e" s="T333">uːtɨlʼpokɔːlɨŋ. </ts>
               <ts e="T335" id="Seg_334" n="e" s="T334">Täpɨnɨŋ </ts>
               <ts e="T336" id="Seg_336" n="e" s="T335">čʼaptäːqɨn </ts>
               <ts e="T337" id="Seg_338" n="e" s="T336">kɨkɨmpa </ts>
               <ts e="T338" id="Seg_340" n="e" s="T337">tɛnɨmɨqa, </ts>
               <ts e="T339" id="Seg_342" n="e" s="T338">qaj </ts>
               <ts e="T340" id="Seg_344" n="e" s="T339">meːtɨntɔːtɨn </ts>
               <ts e="T341" id="Seg_346" n="e" s="T340">nɨmtɨ, </ts>
               <ts e="T342" id="Seg_348" n="e" s="T341">qajikoš </ts>
               <ts e="T343" id="Seg_350" n="e" s="T342">tomtɔːtɨn </ts>
               <ts e="T344" id="Seg_352" n="e" s="T343">Мoskva </ts>
               <ts e="T345" id="Seg_354" n="e" s="T344">čʼɔːtɨ. </ts>
               <ts e="T346" id="Seg_356" n="e" s="T345">Stepan </ts>
               <ts e="T347" id="Seg_358" n="e" s="T346">nɨŋa, </ts>
               <ts e="T348" id="Seg_360" n="e" s="T347">tɛnɨrpa. </ts>
               <ts e="T349" id="Seg_362" n="e" s="T348">Nɨllaje. </ts>
               <ts e="T350" id="Seg_364" n="e" s="T349">Kočʼi </ts>
               <ts e="T351" id="Seg_366" n="e" s="T350">qumɨn </ts>
               <ts e="T352" id="Seg_368" n="e" s="T351">na </ts>
               <ts e="T353" id="Seg_370" n="e" s="T352">tätaqɨn </ts>
               <ts e="T354" id="Seg_372" n="e" s="T353">müttɨsɔːtɨn, </ts>
               <ts e="T355" id="Seg_374" n="e" s="T354">qukkɔːtɨn. </ts>
               <ts e="T356" id="Seg_376" n="e" s="T355">Tap </ts>
               <ts e="T357" id="Seg_378" n="e" s="T356">tättɨ </ts>
               <ts e="T358" id="Seg_380" n="e" s="T357">muqultıːrɨŋ </ts>
               <ts e="T359" id="Seg_382" n="e" s="T358">kämsa </ts>
               <ts e="T360" id="Seg_384" n="e" s="T359">qampolʼıːmpa. </ts>
               <ts e="T361" id="Seg_386" n="e" s="T360">Täp </ts>
               <ts e="T362" id="Seg_388" n="e" s="T361">čʼuːra, </ts>
               <ts e="T364" id="Seg_390" n="e" s="T362">saintɨ_qoni </ts>
               <ts e="T365" id="Seg_392" n="e" s="T364">qampalpa. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T300" id="Seg_393" s="T295">AR_1965_ThoughtsOfStepan_nar.001 (001.001)</ta>
            <ta e="T319" id="Seg_394" s="T300">AR_1965_ThoughtsOfStepan_nar.002 (001.002)</ta>
            <ta e="T326" id="Seg_395" s="T319">AR_1965_ThoughtsOfStepan_nar.003 (001.003)</ta>
            <ta e="T331" id="Seg_396" s="T326">AR_1965_ThoughtsOfStepan_nar.004 (001.004)</ta>
            <ta e="T334" id="Seg_397" s="T331">AR_1965_ThoughtsOfStepan_nar.005 (001.005)</ta>
            <ta e="T345" id="Seg_398" s="T334">AR_1965_ThoughtsOfStepan_nar.006 (001.006)</ta>
            <ta e="T348" id="Seg_399" s="T345">AR_1965_ThoughtsOfStepan_nar.007 (001.007)</ta>
            <ta e="T349" id="Seg_400" s="T348">AR_1965_ThoughtsOfStepan_nar.008 (001.008)</ta>
            <ta e="T355" id="Seg_401" s="T349">AR_1965_ThoughtsOfStepan_nar.009 (001.009)</ta>
            <ta e="T360" id="Seg_402" s="T355">AR_1965_ThoughtsOfStepan_nar.010 (001.010)</ta>
            <ta e="T365" id="Seg_403" s="T360">AR_1965_ThoughtsOfStepan_nar.011 (001.011)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T300" id="Seg_404" s="T295">Степан ′а̄lчисы [′паkтыр′толсӓ] ′пӧтпыl ′ӱттъ.</ta>
            <ta e="T319" id="Seg_405" s="T300">тӓм ′ӯkылтысӓты ′кӣкап ′шʼитъ ′поръ ты и ′моkона, тантыс ӱтноны ′kа̄нӓкты и ′мыкай ′сома чунты ′паkтыса ′киканоны.</ta>
            <ta e="T326" id="Seg_406" s="T319">моkоны′нӓн ′kалысотын ′чуран, ′кика, ′пирkы ′лотык.</ta>
            <ta e="T331" id="Seg_407" s="T326">′ме′мыkынын ′аша ′ныlчиң ′еңа.</ta>
            <ta e="T334" id="Seg_408" s="T331">′тап ′kалʼтӓрӓ ′ӯтыlпоkа̄лың.</ta>
            <ta e="T345" id="Seg_409" s="T334">′тӓпының ча′птӓkын ′кыкымпа ′тӓнымыка, kай ′ме̄дынтотын ′нымды. ′kайикошʼ ′томтатын Москва′чоты.</ta>
            <ta e="T348" id="Seg_410" s="T345">Степан ′нынка, тӓнырпа.</ta>
            <ta e="T349" id="Seg_411" s="T348">′нылайе.</ta>
            <ta e="T355" id="Seg_412" s="T349">′кочи ′kумын на ′тӓтаkын ′мӱттысо̄тын kӯ′kо̄тын.</ta>
            <ta e="T360" id="Seg_413" s="T355">тап ′тӓтты ′мукулʼтирың ′kӓ̄п[м]са ′kамполʼимпа.</ta>
            <ta e="T365" id="Seg_414" s="T360">′тӓп ′чӯра ′саинты′kони ′kампалпа.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T300" id="Seg_415" s="T295">Сtepan aːlʼčisɨ [paqtɨrtolsä] pötpɨlʼ üttə.</ta>
            <ta e="T319" id="Seg_416" s="T300">täm uːqɨltɨsätɨ kiːkap šitə porə tɨ i moqona, tantɨs ütnonɨ qaːnäktɨ i mɨkaj soma čuntɨ paqtɨsa kikanonɨ.</ta>
            <ta e="T326" id="Seg_417" s="T319">moqonɨnän qalɨsotɨn čuran, kika, pirqɨ lotɨk.</ta>
            <ta e="T331" id="Seg_418" s="T326">memɨqɨnɨn aša nɨlʼčiŋ eŋa.</ta>
            <ta e="T334" id="Seg_419" s="T331">tap qalʼtärä uːtɨlʼpoqaːlɨŋ.</ta>
            <ta e="T345" id="Seg_420" s="T334">täpɨnɨŋ čaptäqɨn kɨkɨmpa tänɨmɨka, qaj meːdɨntotɨn nɨmdɨ. qajikoš tomtatɨn Мoskvačotɨ.</ta>
            <ta e="T348" id="Seg_421" s="T345">Сtepan nɨnka, tänɨrpa.</ta>
            <ta e="T349" id="Seg_422" s="T348">nɨlaje.</ta>
            <ta e="T355" id="Seg_423" s="T349">koči qumɨn na tätaqɨn müttɨsoːtɨn quːqoːtɨn.</ta>
            <ta e="T360" id="Seg_424" s="T355">tap tättɨ mukulʼtirɨŋ qäːp[m]sa qampolʼimpa.</ta>
            <ta e="T365" id="Seg_425" s="T360">täp čuːra saintɨqoni qampalpa.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T300" id="Seg_426" s="T295">Stepan alʼčʼisɨ (paktɨrtɔːlsä) pötpɨlʼ üttə. </ta>
            <ta e="T319" id="Seg_427" s="T300">Täm uːqɨltɨsätɨ kikap šitə pɔːrɨ tɨː i moqona, tantɨs üt nɔːnɨ qanäktɨ i mɨkaj soma čʼuntɨ paktɨsa kika nɔːnɨ. </ta>
            <ta e="T326" id="Seg_428" s="T319">Moqonɨ nän qalɨsɔːtɨn čʼuːran, kika, pirqɨ lotɨk. </ta>
            <ta e="T331" id="Seg_429" s="T326">Meː mɨqɨnɨn aša nɨlʼčʼiŋ ɛːŋa. </ta>
            <ta e="T334" id="Seg_430" s="T331">Tap qalʼtärä uːtɨlʼpokɔːlɨŋ. </ta>
            <ta e="T345" id="Seg_431" s="T334">Täpɨnɨŋ čʼaptäːqɨn kɨkɨmpa tɛnɨmɨqa, qaj meːtɨntɔːtɨn nɨmtɨ, qajikoš tomtɔːtɨn Мoskva čʼɔːtɨ. </ta>
            <ta e="T348" id="Seg_432" s="T345">Stepan nɨŋa, tɛnɨrpa. </ta>
            <ta e="T349" id="Seg_433" s="T348">Nɨllaje. </ta>
            <ta e="T355" id="Seg_434" s="T349">Kočʼi qumɨn na tätaqɨn müttɨsɔːtɨn, qukkɔːtɨn. </ta>
            <ta e="T360" id="Seg_435" s="T355">Tap tättɨ muqultıːrɨŋ kämsa qampolʼıːmpa. </ta>
            <ta e="T365" id="Seg_436" s="T360">Täp čʼuːra, saintɨ_qoni qampalpa. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T296" id="Seg_437" s="T295">stepan</ta>
            <ta e="T297" id="Seg_438" s="T296">alʼčʼi-sɨ</ta>
            <ta e="T298" id="Seg_439" s="T297">paktɨ-r-tɔːl-sä</ta>
            <ta e="T299" id="Seg_440" s="T298">pötpɨlʼ</ta>
            <ta e="T300" id="Seg_441" s="T299">üt-tə</ta>
            <ta e="T301" id="Seg_442" s="T300">täm</ta>
            <ta e="T302" id="Seg_443" s="T301">uː-qɨl-tɨ-sä-tɨ</ta>
            <ta e="T303" id="Seg_444" s="T302">kika-p</ta>
            <ta e="T304" id="Seg_445" s="T303">šitə</ta>
            <ta e="T305" id="Seg_446" s="T304">pɔːrɨ</ta>
            <ta e="T306" id="Seg_447" s="T305">tɨː</ta>
            <ta e="T307" id="Seg_448" s="T306">i</ta>
            <ta e="T308" id="Seg_449" s="T307">moqona</ta>
            <ta e="T309" id="Seg_450" s="T308">tantɨ-s</ta>
            <ta e="T310" id="Seg_451" s="T309">üt</ta>
            <ta e="T311" id="Seg_452" s="T310">nɔː-nɨ</ta>
            <ta e="T312" id="Seg_453" s="T311">qanäk-tɨ</ta>
            <ta e="T313" id="Seg_454" s="T312">i</ta>
            <ta e="T314" id="Seg_455" s="T313">mɨkaj</ta>
            <ta e="T315" id="Seg_456" s="T314">soma</ta>
            <ta e="T316" id="Seg_457" s="T315">čʼuntɨ</ta>
            <ta e="T317" id="Seg_458" s="T316">paktɨ-sa</ta>
            <ta e="T318" id="Seg_459" s="T317">kika</ta>
            <ta e="T319" id="Seg_460" s="T318">nɔː-nɨ</ta>
            <ta e="T320" id="Seg_461" s="T319">moqonɨ</ta>
            <ta e="T321" id="Seg_462" s="T320">nän</ta>
            <ta e="T322" id="Seg_463" s="T321">qalɨ-sɔː-tɨn</ta>
            <ta e="T323" id="Seg_464" s="T322">čʼuːra-n</ta>
            <ta e="T324" id="Seg_465" s="T323">kika</ta>
            <ta e="T325" id="Seg_466" s="T324">pirqɨ</ta>
            <ta e="T326" id="Seg_467" s="T325">lotɨk</ta>
            <ta e="T327" id="Seg_468" s="T326">meː</ta>
            <ta e="T328" id="Seg_469" s="T327">mɨ-qɨn-ɨ-n</ta>
            <ta e="T329" id="Seg_470" s="T328">aša</ta>
            <ta e="T330" id="Seg_471" s="T329">nɨlʼčʼi-ŋ</ta>
            <ta e="T331" id="Seg_472" s="T330">ɛː-ŋa</ta>
            <ta e="T332" id="Seg_473" s="T331">tap</ta>
            <ta e="T333" id="Seg_474" s="T332">qalʼ-tä-rä</ta>
            <ta e="T334" id="Seg_475" s="T333">uːtɨlʼpo-kɔːlɨ-ŋ</ta>
            <ta e="T335" id="Seg_476" s="T334">täp-ɨ-nɨŋ</ta>
            <ta e="T336" id="Seg_477" s="T335">čʼaptäː-qɨn</ta>
            <ta e="T337" id="Seg_478" s="T336">kɨkɨ-mpa</ta>
            <ta e="T338" id="Seg_479" s="T337">tɛnɨmɨ-qa</ta>
            <ta e="T339" id="Seg_480" s="T338">qaj</ta>
            <ta e="T340" id="Seg_481" s="T339">meː-tɨ-ntɔː-tɨn</ta>
            <ta e="T341" id="Seg_482" s="T340">nɨmtɨ</ta>
            <ta e="T342" id="Seg_483" s="T341">qaj-i-koš</ta>
            <ta e="T343" id="Seg_484" s="T342">tom-tɔː-tɨn</ta>
            <ta e="T344" id="Seg_485" s="T343">Мoskva</ta>
            <ta e="T345" id="Seg_486" s="T344">čʼɔːtɨ</ta>
            <ta e="T346" id="Seg_487" s="T345">stepan</ta>
            <ta e="T347" id="Seg_488" s="T346">nɨ-ŋa</ta>
            <ta e="T348" id="Seg_489" s="T347">tɛnɨ-r-pa</ta>
            <ta e="T349" id="Seg_490" s="T348">nɨll-a-je</ta>
            <ta e="T350" id="Seg_491" s="T349">kočʼi</ta>
            <ta e="T351" id="Seg_492" s="T350">qum-ɨ-n</ta>
            <ta e="T352" id="Seg_493" s="T351">na</ta>
            <ta e="T353" id="Seg_494" s="T352">täta-qɨn</ta>
            <ta e="T354" id="Seg_495" s="T353">müt-tɨ-sɔː-tɨn</ta>
            <ta e="T355" id="Seg_496" s="T354">qu-kkɔː-tɨn</ta>
            <ta e="T356" id="Seg_497" s="T355">tap</ta>
            <ta e="T357" id="Seg_498" s="T356">tättɨ</ta>
            <ta e="T358" id="Seg_499" s="T357">muqultıːrɨ-ŋ</ta>
            <ta e="T359" id="Seg_500" s="T358">käm-sa</ta>
            <ta e="T360" id="Seg_501" s="T359">qampolʼ-ıː-mpa</ta>
            <ta e="T361" id="Seg_502" s="T360">täp</ta>
            <ta e="T362" id="Seg_503" s="T361">čʼuːra</ta>
            <ta e="T364" id="Seg_504" s="T362">saintɨ_qoni</ta>
            <ta e="T365" id="Seg_505" s="T364">qampal-pa</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T296" id="Seg_506" s="T295">Stepan</ta>
            <ta e="T297" id="Seg_507" s="T296">alʼčʼɨ-sɨ</ta>
            <ta e="T298" id="Seg_508" s="T297">paktɨ-r-ätɔːl-sɨ</ta>
            <ta e="T299" id="Seg_509" s="T298">pötpɨlʼ</ta>
            <ta e="T300" id="Seg_510" s="T299">üt-ntɨ</ta>
            <ta e="T301" id="Seg_511" s="T300">təp</ta>
            <ta e="T302" id="Seg_512" s="T301">uː-qɨl-tɨ-sɨ-tɨ</ta>
            <ta e="T303" id="Seg_513" s="T302">kıkä-m</ta>
            <ta e="T304" id="Seg_514" s="T303">šittɨ</ta>
            <ta e="T305" id="Seg_515" s="T304">pɔːrɨ</ta>
            <ta e="T306" id="Seg_516" s="T305">tɨː</ta>
            <ta e="T307" id="Seg_517" s="T306">i</ta>
            <ta e="T308" id="Seg_518" s="T307">moqɨnä</ta>
            <ta e="T309" id="Seg_519" s="T308">tantɨ-sɨ</ta>
            <ta e="T310" id="Seg_520" s="T309">üt</ta>
            <ta e="T311" id="Seg_521" s="T310">*nɔː-nɨ</ta>
            <ta e="T312" id="Seg_522" s="T311">qanɨŋ-ntɨ</ta>
            <ta e="T313" id="Seg_523" s="T312">i</ta>
            <ta e="T314" id="Seg_524" s="T313">mɨkaj</ta>
            <ta e="T315" id="Seg_525" s="T314">soma</ta>
            <ta e="T316" id="Seg_526" s="T315">čʼuntɨ</ta>
            <ta e="T317" id="Seg_527" s="T316">paktɨ-sɨ</ta>
            <ta e="T318" id="Seg_528" s="T317">kıkä</ta>
            <ta e="T319" id="Seg_529" s="T318">*nɔː-nɨ</ta>
            <ta e="T320" id="Seg_530" s="T319">moqɨnä</ta>
            <ta e="T321" id="Seg_531" s="T320">nʼentɨ</ta>
            <ta e="T322" id="Seg_532" s="T321">qalɨ-sɨ-tɨt</ta>
            <ta e="T323" id="Seg_533" s="T322">čʼuːrɨ-t</ta>
            <ta e="T324" id="Seg_534" s="T323">kıkä</ta>
            <ta e="T325" id="Seg_535" s="T324">pirqɨ</ta>
            <ta e="T326" id="Seg_536" s="T325">lotɨŋ</ta>
            <ta e="T327" id="Seg_537" s="T326">meː</ta>
            <ta e="T328" id="Seg_538" s="T327">mɨ-qɨn-ɨ-naj</ta>
            <ta e="T329" id="Seg_539" s="T328">ašša</ta>
            <ta e="T330" id="Seg_540" s="T329">nılʼčʼɨ-k</ta>
            <ta e="T331" id="Seg_541" s="T330">ɛː-ŋɨ</ta>
            <ta e="T332" id="Seg_542" s="T331">təp</ta>
            <ta e="T333" id="Seg_543" s="T332">qälɨ-tä-r</ta>
            <ta e="T334" id="Seg_544" s="T333">uːtɨlʼpo-kɔːlɨ-k</ta>
            <ta e="T335" id="Seg_545" s="T334">təp-ɨ-nɨŋ</ta>
            <ta e="T336" id="Seg_546" s="T335">čʼaptä-qɨn</ta>
            <ta e="T337" id="Seg_547" s="T336">kɨkɨ-mpɨ</ta>
            <ta e="T338" id="Seg_548" s="T337">tɛnɨmɨ-qo</ta>
            <ta e="T339" id="Seg_549" s="T338">qaj</ta>
            <ta e="T340" id="Seg_550" s="T339">meː-tɨ-ntɨ-tɨt</ta>
            <ta e="T341" id="Seg_551" s="T340">nɨmtɨ</ta>
            <ta e="T342" id="Seg_552" s="T341">qaj-ɨ-kos</ta>
            <ta e="T343" id="Seg_553" s="T342">tom-ntɨ-tɨt</ta>
            <ta e="T344" id="Seg_554" s="T343">Мoskva</ta>
            <ta e="T345" id="Seg_555" s="T344">čʼɔːtɨ</ta>
            <ta e="T346" id="Seg_556" s="T345">Stepan</ta>
            <ta e="T347" id="Seg_557" s="T346">nɨ-ŋɨ</ta>
            <ta e="T348" id="Seg_558" s="T347">tɛnɨ-r-mpɨ</ta>
            <ta e="T349" id="Seg_559" s="T348">nɨl-ɨ-ŋɨ</ta>
            <ta e="T350" id="Seg_560" s="T349">kočʼčʼɨ</ta>
            <ta e="T351" id="Seg_561" s="T350">qum-ɨ-t</ta>
            <ta e="T352" id="Seg_562" s="T351">na</ta>
            <ta e="T353" id="Seg_563" s="T352">təttɨ-qɨn</ta>
            <ta e="T354" id="Seg_564" s="T353">mütɨ-tɨ-sɨ-tɨt</ta>
            <ta e="T355" id="Seg_565" s="T354">qu-kkɨ-tɨt</ta>
            <ta e="T356" id="Seg_566" s="T355">tam</ta>
            <ta e="T357" id="Seg_567" s="T356">təttɨ</ta>
            <ta e="T358" id="Seg_568" s="T357">*muqɨltıːrɨ-k</ta>
            <ta e="T359" id="Seg_569" s="T358">kəm-sä</ta>
            <ta e="T360" id="Seg_570" s="T359">qampolʼ-ıː-mpɨ</ta>
            <ta e="T361" id="Seg_571" s="T360">təp</ta>
            <ta e="T362" id="Seg_572" s="T361">čʼuːrɨ</ta>
            <ta e="T364" id="Seg_573" s="T362">sajɨn_qoni</ta>
            <ta e="T365" id="Seg_574" s="T364">qampolʼ-mpɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T296" id="Seg_575" s="T295">Stepan.[NOM]</ta>
            <ta e="T297" id="Seg_576" s="T296">fall-PST.[3SG.S]</ta>
            <ta e="T298" id="Seg_577" s="T297">jump-FRQ-MOM-PST.[3SG.S]</ta>
            <ta e="T299" id="Seg_578" s="T298">warm</ta>
            <ta e="T300" id="Seg_579" s="T299">water-ILL</ta>
            <ta e="T301" id="Seg_580" s="T300">(s)he.[NOM]</ta>
            <ta e="T302" id="Seg_581" s="T301">swim-MULO-TR-PST-3SG.O</ta>
            <ta e="T303" id="Seg_582" s="T302">small.river-ACC</ta>
            <ta e="T304" id="Seg_583" s="T303">two</ta>
            <ta e="T305" id="Seg_584" s="T304">time.[NOM]</ta>
            <ta e="T306" id="Seg_585" s="T305">here</ta>
            <ta e="T307" id="Seg_586" s="T306">and</ta>
            <ta e="T308" id="Seg_587" s="T307">back</ta>
            <ta e="T309" id="Seg_588" s="T308">go.out-PST.[3SG.S]</ta>
            <ta e="T310" id="Seg_589" s="T309">water.[NOM]</ta>
            <ta e="T311" id="Seg_590" s="T310">out-ADV.EL</ta>
            <ta e="T312" id="Seg_591" s="T311">bank-ILL</ta>
            <ta e="T313" id="Seg_592" s="T312">and</ta>
            <ta e="T314" id="Seg_593" s="T313">like</ta>
            <ta e="T315" id="Seg_594" s="T314">good</ta>
            <ta e="T316" id="Seg_595" s="T315">horse.[NOM]</ta>
            <ta e="T317" id="Seg_596" s="T316">run-PST.[3SG.S]</ta>
            <ta e="T318" id="Seg_597" s="T317">small.river.[NOM]</ta>
            <ta e="T319" id="Seg_598" s="T318">from-ADV.EL</ta>
            <ta e="T320" id="Seg_599" s="T319">home</ta>
            <ta e="T321" id="Seg_600" s="T320">together</ta>
            <ta e="T322" id="Seg_601" s="T321">stay-PST-3PL</ta>
            <ta e="T323" id="Seg_602" s="T322">sand-PL.[NOM]</ta>
            <ta e="T324" id="Seg_603" s="T323">small.river.[NOM]</ta>
            <ta e="T325" id="Seg_604" s="T324">high</ta>
            <ta e="T326" id="Seg_605" s="T325">equisetum.[NOM]</ta>
            <ta e="T327" id="Seg_606" s="T326">we.PL.GEN</ta>
            <ta e="T328" id="Seg_607" s="T327">something-LOC-EP-EMPH</ta>
            <ta e="T329" id="Seg_608" s="T328">NEG</ta>
            <ta e="T330" id="Seg_609" s="T329">such-ADVZ</ta>
            <ta e="T331" id="Seg_610" s="T330">be-CO.[3SG.S]</ta>
            <ta e="T332" id="Seg_611" s="T331">(s)he.[NOM]</ta>
            <ta e="T333" id="Seg_612" s="T332">go-%%-FRQ.[3SG.S]</ta>
            <ta e="T334" id="Seg_613" s="T333">work-CAR-ADVZ</ta>
            <ta e="T335" id="Seg_614" s="T334">(s)he-EP-ALL</ta>
            <ta e="T336" id="Seg_615" s="T335">tale-ADV.LOC</ta>
            <ta e="T337" id="Seg_616" s="T336">want-PST.NAR.[3SG.S]</ta>
            <ta e="T338" id="Seg_617" s="T337">know-INF</ta>
            <ta e="T339" id="Seg_618" s="T338">what.[NOM]</ta>
            <ta e="T340" id="Seg_619" s="T339">make-TR-IPFV-3PL</ta>
            <ta e="T341" id="Seg_620" s="T340">there</ta>
            <ta e="T342" id="Seg_621" s="T341">what.[NOM]-EP-INDEF3</ta>
            <ta e="T343" id="Seg_622" s="T342">speak-IPFV-3PL</ta>
            <ta e="T344" id="Seg_623" s="T343">Moscow.[NOM]</ta>
            <ta e="T345" id="Seg_624" s="T344">about</ta>
            <ta e="T346" id="Seg_625" s="T345">Stepan.[NOM]</ta>
            <ta e="T347" id="Seg_626" s="T346">stand-CO.[3SG.S]</ta>
            <ta e="T348" id="Seg_627" s="T347">think-FRQ-DUR.[3SG.S]</ta>
            <ta e="T349" id="Seg_628" s="T348">stop-EP-CO.[3SG.S]</ta>
            <ta e="T350" id="Seg_629" s="T349">much</ta>
            <ta e="T351" id="Seg_630" s="T350">human.being-EP-PL.[NOM]</ta>
            <ta e="T352" id="Seg_631" s="T351">this</ta>
            <ta e="T353" id="Seg_632" s="T352">earth-LOC</ta>
            <ta e="T354" id="Seg_633" s="T353">war-TR-PST-3PL</ta>
            <ta e="T355" id="Seg_634" s="T354">die-HAB-3PL</ta>
            <ta e="T356" id="Seg_635" s="T355">this</ta>
            <ta e="T357" id="Seg_636" s="T356">earth.[NOM]</ta>
            <ta e="T358" id="Seg_637" s="T357">whole-ADVZ</ta>
            <ta e="T359" id="Seg_638" s="T358">blood-INSTR</ta>
            <ta e="T360" id="Seg_639" s="T359">spill-RFL.PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T361" id="Seg_640" s="T360">(s)he.[NOM]</ta>
            <ta e="T362" id="Seg_641" s="T361">cry.[3SG.S]</ta>
            <ta e="T364" id="Seg_642" s="T362">tears.[NOM]</ta>
            <ta e="T365" id="Seg_643" s="T364">spill-PST.NAR.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T296" id="Seg_644" s="T295">Степан.[NOM]</ta>
            <ta e="T297" id="Seg_645" s="T296">упасть-PST.[3SG.S]</ta>
            <ta e="T298" id="Seg_646" s="T297">прыгнуть-FRQ-MOM-PST.[3SG.S]</ta>
            <ta e="T299" id="Seg_647" s="T298">тёплый</ta>
            <ta e="T300" id="Seg_648" s="T299">вода-ILL</ta>
            <ta e="T301" id="Seg_649" s="T300">он(а).[NOM]</ta>
            <ta e="T302" id="Seg_650" s="T301">плыть-MULO-TR-PST-3SG.O</ta>
            <ta e="T303" id="Seg_651" s="T302">речка-ACC</ta>
            <ta e="T304" id="Seg_652" s="T303">два</ta>
            <ta e="T305" id="Seg_653" s="T304">раз.[NOM]</ta>
            <ta e="T306" id="Seg_654" s="T305">сюда</ta>
            <ta e="T307" id="Seg_655" s="T306">и</ta>
            <ta e="T308" id="Seg_656" s="T307">назад</ta>
            <ta e="T309" id="Seg_657" s="T308">выйти-PST.[3SG.S]</ta>
            <ta e="T310" id="Seg_658" s="T309">вода.[NOM]</ta>
            <ta e="T311" id="Seg_659" s="T310">из-ADV.EL</ta>
            <ta e="T312" id="Seg_660" s="T311">берег-ILL</ta>
            <ta e="T313" id="Seg_661" s="T312">и</ta>
            <ta e="T314" id="Seg_662" s="T313">как</ta>
            <ta e="T315" id="Seg_663" s="T314">хороший</ta>
            <ta e="T316" id="Seg_664" s="T315">лошадь.[NOM]</ta>
            <ta e="T317" id="Seg_665" s="T316">побежать-PST.[3SG.S]</ta>
            <ta e="T318" id="Seg_666" s="T317">речка.[NOM]</ta>
            <ta e="T319" id="Seg_667" s="T318">от-ADV.EL</ta>
            <ta e="T320" id="Seg_668" s="T319">домой</ta>
            <ta e="T321" id="Seg_669" s="T320">вместе</ta>
            <ta e="T322" id="Seg_670" s="T321">остаться-PST-3PL</ta>
            <ta e="T323" id="Seg_671" s="T322">песок-PL.[NOM]</ta>
            <ta e="T324" id="Seg_672" s="T323">речка.[NOM]</ta>
            <ta e="T325" id="Seg_673" s="T324">высокий</ta>
            <ta e="T326" id="Seg_674" s="T325">хвощ.[NOM]</ta>
            <ta e="T327" id="Seg_675" s="T326">мы.PL.GEN</ta>
            <ta e="T328" id="Seg_676" s="T327">нечто-LOC-EP-EMPH</ta>
            <ta e="T329" id="Seg_677" s="T328">NEG</ta>
            <ta e="T330" id="Seg_678" s="T329">такой-ADVZ</ta>
            <ta e="T331" id="Seg_679" s="T330">быть-CO.[3SG.S]</ta>
            <ta e="T332" id="Seg_680" s="T331">он(а).[NOM]</ta>
            <ta e="T333" id="Seg_681" s="T332">ходить-%%-FRQ.[3SG.S]</ta>
            <ta e="T334" id="Seg_682" s="T333">работа-CAR-ADVZ</ta>
            <ta e="T335" id="Seg_683" s="T334">он(а)-EP-ALL</ta>
            <ta e="T336" id="Seg_684" s="T335">сказка-ADV.LOC</ta>
            <ta e="T337" id="Seg_685" s="T336">хотеть-PST.NAR.[3SG.S]</ta>
            <ta e="T338" id="Seg_686" s="T337">знать-INF</ta>
            <ta e="T339" id="Seg_687" s="T338">что.[NOM]</ta>
            <ta e="T340" id="Seg_688" s="T339">сделать-TR-IPFV-3PL</ta>
            <ta e="T341" id="Seg_689" s="T340">там</ta>
            <ta e="T342" id="Seg_690" s="T341">что.[NOM]-EP-INDEF3</ta>
            <ta e="T343" id="Seg_691" s="T342">сказать-IPFV-3PL</ta>
            <ta e="T344" id="Seg_692" s="T343">Москва.[NOM]</ta>
            <ta e="T345" id="Seg_693" s="T344">о</ta>
            <ta e="T346" id="Seg_694" s="T345">Степан.[NOM]</ta>
            <ta e="T347" id="Seg_695" s="T346">стоять-CO.[3SG.S]</ta>
            <ta e="T348" id="Seg_696" s="T347">думать-FRQ-DUR.[3SG.S]</ta>
            <ta e="T349" id="Seg_697" s="T348">остановиться-EP-CO.[3SG.S]</ta>
            <ta e="T350" id="Seg_698" s="T349">много</ta>
            <ta e="T351" id="Seg_699" s="T350">человек-EP-PL.[NOM]</ta>
            <ta e="T352" id="Seg_700" s="T351">этот</ta>
            <ta e="T353" id="Seg_701" s="T352">земля-LOC</ta>
            <ta e="T354" id="Seg_702" s="T353">война-TR-PST-3PL</ta>
            <ta e="T355" id="Seg_703" s="T354">умереть-HAB-3PL</ta>
            <ta e="T356" id="Seg_704" s="T355">этот</ta>
            <ta e="T357" id="Seg_705" s="T356">земля.[NOM]</ta>
            <ta e="T358" id="Seg_706" s="T357">целый-ADVZ</ta>
            <ta e="T359" id="Seg_707" s="T358">кровь-INSTR</ta>
            <ta e="T360" id="Seg_708" s="T359">разлить-RFL.PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T361" id="Seg_709" s="T360">он(а).[NOM]</ta>
            <ta e="T362" id="Seg_710" s="T361">плакать.[3SG.S]</ta>
            <ta e="T364" id="Seg_711" s="T362">слёзы.[NOM]</ta>
            <ta e="T365" id="Seg_712" s="T364">разлить-PST.NAR.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T296" id="Seg_713" s="T295">nprop-n:case</ta>
            <ta e="T297" id="Seg_714" s="T296">v-v:tense-v:pn</ta>
            <ta e="T298" id="Seg_715" s="T297">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T299" id="Seg_716" s="T298">adj</ta>
            <ta e="T300" id="Seg_717" s="T299">n-n:case</ta>
            <ta e="T301" id="Seg_718" s="T300">pers-n:case</ta>
            <ta e="T302" id="Seg_719" s="T301">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T303" id="Seg_720" s="T302">n-n:case</ta>
            <ta e="T304" id="Seg_721" s="T303">num</ta>
            <ta e="T305" id="Seg_722" s="T304">n-n:case</ta>
            <ta e="T306" id="Seg_723" s="T305">adv</ta>
            <ta e="T307" id="Seg_724" s="T306">conj</ta>
            <ta e="T308" id="Seg_725" s="T307">adv</ta>
            <ta e="T309" id="Seg_726" s="T308">v-v:tense-v:pn</ta>
            <ta e="T310" id="Seg_727" s="T309">n-n:case</ta>
            <ta e="T311" id="Seg_728" s="T310">pp-adv:case</ta>
            <ta e="T312" id="Seg_729" s="T311">n-n:case</ta>
            <ta e="T313" id="Seg_730" s="T312">conj</ta>
            <ta e="T314" id="Seg_731" s="T313">conj</ta>
            <ta e="T315" id="Seg_732" s="T314">adj</ta>
            <ta e="T316" id="Seg_733" s="T315">n-n:case</ta>
            <ta e="T317" id="Seg_734" s="T316">v-v:tense-v:pn</ta>
            <ta e="T318" id="Seg_735" s="T317">n-n:case</ta>
            <ta e="T319" id="Seg_736" s="T318">pp-adv:case</ta>
            <ta e="T320" id="Seg_737" s="T319">adv</ta>
            <ta e="T321" id="Seg_738" s="T320">preverb</ta>
            <ta e="T322" id="Seg_739" s="T321">v-v:tense-v:pn</ta>
            <ta e="T323" id="Seg_740" s="T322">n-n:num-n:case</ta>
            <ta e="T324" id="Seg_741" s="T323">n-n:case</ta>
            <ta e="T325" id="Seg_742" s="T324">adj</ta>
            <ta e="T326" id="Seg_743" s="T325">n-n:case</ta>
            <ta e="T327" id="Seg_744" s="T326">pers</ta>
            <ta e="T328" id="Seg_745" s="T327">n-n:case-n:ins-clit</ta>
            <ta e="T329" id="Seg_746" s="T328">ptcl</ta>
            <ta e="T330" id="Seg_747" s="T329">dem-adj&gt;adv</ta>
            <ta e="T331" id="Seg_748" s="T330">v-v:ins-v:pn</ta>
            <ta e="T332" id="Seg_749" s="T331">pers-n:case</ta>
            <ta e="T333" id="Seg_750" s="T332">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T334" id="Seg_751" s="T333">n-n&gt;adj-adj&gt;adv</ta>
            <ta e="T335" id="Seg_752" s="T334">pers-n:ins-n:case</ta>
            <ta e="T336" id="Seg_753" s="T335">n-n&gt;adv</ta>
            <ta e="T337" id="Seg_754" s="T336">v-v:tense-v:pn</ta>
            <ta e="T338" id="Seg_755" s="T337">v-v:inf</ta>
            <ta e="T339" id="Seg_756" s="T338">interrog-n:case</ta>
            <ta e="T340" id="Seg_757" s="T339">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T341" id="Seg_758" s="T340">adv</ta>
            <ta e="T342" id="Seg_759" s="T341">interrog-n:case-n:ins-clit</ta>
            <ta e="T343" id="Seg_760" s="T342">v-v&gt;v-v:pn</ta>
            <ta e="T344" id="Seg_761" s="T343">nprop-n:case</ta>
            <ta e="T345" id="Seg_762" s="T344">pp</ta>
            <ta e="T346" id="Seg_763" s="T345">nprop-n:case</ta>
            <ta e="T347" id="Seg_764" s="T346">v-v:ins-v:pn</ta>
            <ta e="T348" id="Seg_765" s="T347">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T349" id="Seg_766" s="T348">v-v:ins-v:ins-v:pn</ta>
            <ta e="T350" id="Seg_767" s="T349">quant</ta>
            <ta e="T351" id="Seg_768" s="T350">n-n:ins-n:num-n:case</ta>
            <ta e="T352" id="Seg_769" s="T351">dem</ta>
            <ta e="T353" id="Seg_770" s="T352">n-n:case</ta>
            <ta e="T354" id="Seg_771" s="T353">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T355" id="Seg_772" s="T354">v-v&gt;v-v:pn</ta>
            <ta e="T356" id="Seg_773" s="T355">dem</ta>
            <ta e="T357" id="Seg_774" s="T356">n-n:case</ta>
            <ta e="T358" id="Seg_775" s="T357">adj-adj&gt;adv</ta>
            <ta e="T359" id="Seg_776" s="T358">n-n:case</ta>
            <ta e="T360" id="Seg_777" s="T359">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T361" id="Seg_778" s="T360">pers-n:case</ta>
            <ta e="T362" id="Seg_779" s="T361">v-v:pn</ta>
            <ta e="T364" id="Seg_780" s="T362">n-n:case</ta>
            <ta e="T365" id="Seg_781" s="T364">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T296" id="Seg_782" s="T295">nprop</ta>
            <ta e="T297" id="Seg_783" s="T296">v</ta>
            <ta e="T298" id="Seg_784" s="T297">v</ta>
            <ta e="T299" id="Seg_785" s="T298">adj</ta>
            <ta e="T300" id="Seg_786" s="T299">n</ta>
            <ta e="T301" id="Seg_787" s="T300">pers</ta>
            <ta e="T302" id="Seg_788" s="T301">v</ta>
            <ta e="T303" id="Seg_789" s="T302">n</ta>
            <ta e="T304" id="Seg_790" s="T303">num</ta>
            <ta e="T305" id="Seg_791" s="T304">n</ta>
            <ta e="T306" id="Seg_792" s="T305">adv</ta>
            <ta e="T307" id="Seg_793" s="T306">conj</ta>
            <ta e="T308" id="Seg_794" s="T307">adv</ta>
            <ta e="T309" id="Seg_795" s="T308">v</ta>
            <ta e="T310" id="Seg_796" s="T309">n</ta>
            <ta e="T311" id="Seg_797" s="T310">pp</ta>
            <ta e="T312" id="Seg_798" s="T311">n</ta>
            <ta e="T313" id="Seg_799" s="T312">conj</ta>
            <ta e="T314" id="Seg_800" s="T313">conj</ta>
            <ta e="T315" id="Seg_801" s="T314">adj</ta>
            <ta e="T316" id="Seg_802" s="T315">n</ta>
            <ta e="T317" id="Seg_803" s="T316">v</ta>
            <ta e="T318" id="Seg_804" s="T317">n</ta>
            <ta e="T319" id="Seg_805" s="T318">pp</ta>
            <ta e="T320" id="Seg_806" s="T319">adv</ta>
            <ta e="T321" id="Seg_807" s="T320">preverb</ta>
            <ta e="T322" id="Seg_808" s="T321">v</ta>
            <ta e="T323" id="Seg_809" s="T322">n</ta>
            <ta e="T324" id="Seg_810" s="T323">n</ta>
            <ta e="T325" id="Seg_811" s="T324">adj</ta>
            <ta e="T326" id="Seg_812" s="T325">n</ta>
            <ta e="T327" id="Seg_813" s="T326">pers</ta>
            <ta e="T328" id="Seg_814" s="T327">n</ta>
            <ta e="T329" id="Seg_815" s="T328">ptcl</ta>
            <ta e="T330" id="Seg_816" s="T329">adv</ta>
            <ta e="T331" id="Seg_817" s="T330">v</ta>
            <ta e="T332" id="Seg_818" s="T331">pers</ta>
            <ta e="T333" id="Seg_819" s="T332">v</ta>
            <ta e="T334" id="Seg_820" s="T333">adv</ta>
            <ta e="T335" id="Seg_821" s="T334">pers</ta>
            <ta e="T336" id="Seg_822" s="T335">adv</ta>
            <ta e="T337" id="Seg_823" s="T336">v</ta>
            <ta e="T338" id="Seg_824" s="T337">v</ta>
            <ta e="T339" id="Seg_825" s="T338">interrog</ta>
            <ta e="T340" id="Seg_826" s="T339">v</ta>
            <ta e="T341" id="Seg_827" s="T340">adv</ta>
            <ta e="T342" id="Seg_828" s="T341">interrog</ta>
            <ta e="T343" id="Seg_829" s="T342">v</ta>
            <ta e="T344" id="Seg_830" s="T343">nprop</ta>
            <ta e="T345" id="Seg_831" s="T344">pp</ta>
            <ta e="T346" id="Seg_832" s="T345">nprop</ta>
            <ta e="T347" id="Seg_833" s="T346">v</ta>
            <ta e="T348" id="Seg_834" s="T347">v</ta>
            <ta e="T349" id="Seg_835" s="T348">v</ta>
            <ta e="T350" id="Seg_836" s="T349">quant</ta>
            <ta e="T351" id="Seg_837" s="T350">n</ta>
            <ta e="T352" id="Seg_838" s="T351">dem</ta>
            <ta e="T353" id="Seg_839" s="T352">n</ta>
            <ta e="T354" id="Seg_840" s="T353">v</ta>
            <ta e="T355" id="Seg_841" s="T354">v</ta>
            <ta e="T356" id="Seg_842" s="T355">dem</ta>
            <ta e="T357" id="Seg_843" s="T356">n</ta>
            <ta e="T358" id="Seg_844" s="T357">adv</ta>
            <ta e="T359" id="Seg_845" s="T358">n</ta>
            <ta e="T360" id="Seg_846" s="T359">v</ta>
            <ta e="T361" id="Seg_847" s="T360">pers</ta>
            <ta e="T362" id="Seg_848" s="T361">v</ta>
            <ta e="T364" id="Seg_849" s="T362">n</ta>
            <ta e="T365" id="Seg_850" s="T364">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T296" id="Seg_851" s="T295">np.h:A</ta>
            <ta e="T300" id="Seg_852" s="T299">np:G</ta>
            <ta e="T301" id="Seg_853" s="T300">pro.h:A</ta>
            <ta e="T303" id="Seg_854" s="T302">np:Th</ta>
            <ta e="T305" id="Seg_855" s="T304">np:Time</ta>
            <ta e="T306" id="Seg_856" s="T305">adv:G</ta>
            <ta e="T308" id="Seg_857" s="T307">adv:G</ta>
            <ta e="T309" id="Seg_858" s="T308">0.3.h:A</ta>
            <ta e="T310" id="Seg_859" s="T309">pp:So</ta>
            <ta e="T312" id="Seg_860" s="T311">np:G</ta>
            <ta e="T317" id="Seg_861" s="T316">0.3.h:A</ta>
            <ta e="T318" id="Seg_862" s="T317">pp:So</ta>
            <ta e="T320" id="Seg_863" s="T319">adv:G</ta>
            <ta e="T323" id="Seg_864" s="T322">np:Th</ta>
            <ta e="T324" id="Seg_865" s="T323">np:Th</ta>
            <ta e="T326" id="Seg_866" s="T325">np:Th</ta>
            <ta e="T327" id="Seg_867" s="T326">pro:Poss</ta>
            <ta e="T328" id="Seg_868" s="T327">np:L</ta>
            <ta e="T331" id="Seg_869" s="T330">0.3:Th</ta>
            <ta e="T332" id="Seg_870" s="T331">pro.h:A</ta>
            <ta e="T335" id="Seg_871" s="T334">pro.h:E</ta>
            <ta e="T336" id="Seg_872" s="T335">adv:L</ta>
            <ta e="T338" id="Seg_873" s="T337">v:Th</ta>
            <ta e="T339" id="Seg_874" s="T338">np:Th</ta>
            <ta e="T340" id="Seg_875" s="T339">0.3.h:A</ta>
            <ta e="T341" id="Seg_876" s="T340">adv:L</ta>
            <ta e="T342" id="Seg_877" s="T341">np:Th</ta>
            <ta e="T343" id="Seg_878" s="T342">0.3.h:A</ta>
            <ta e="T346" id="Seg_879" s="T345">np.h:Th</ta>
            <ta e="T348" id="Seg_880" s="T347">0.3.h:E</ta>
            <ta e="T349" id="Seg_881" s="T348">0.3.h:A</ta>
            <ta e="T351" id="Seg_882" s="T350">np.h:A</ta>
            <ta e="T353" id="Seg_883" s="T352">np:L</ta>
            <ta e="T355" id="Seg_884" s="T354">0.3.h:P</ta>
            <ta e="T357" id="Seg_885" s="T356">np:Th</ta>
            <ta e="T359" id="Seg_886" s="T358">np:Ins</ta>
            <ta e="T361" id="Seg_887" s="T360">pro.h:A</ta>
            <ta e="T364" id="Seg_888" s="T362">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T296" id="Seg_889" s="T295">np.h:S</ta>
            <ta e="T297" id="Seg_890" s="T296">v:pred</ta>
            <ta e="T298" id="Seg_891" s="T297">v:pred</ta>
            <ta e="T301" id="Seg_892" s="T300">pro.h:S</ta>
            <ta e="T302" id="Seg_893" s="T301">v:pred</ta>
            <ta e="T303" id="Seg_894" s="T302">np:O</ta>
            <ta e="T309" id="Seg_895" s="T308">0.3.h:S v:pred</ta>
            <ta e="T317" id="Seg_896" s="T316">0.3.h:S v:pred</ta>
            <ta e="T322" id="Seg_897" s="T321">v:pred</ta>
            <ta e="T323" id="Seg_898" s="T322">np:S</ta>
            <ta e="T324" id="Seg_899" s="T323">np:S</ta>
            <ta e="T326" id="Seg_900" s="T325">np:S</ta>
            <ta e="T331" id="Seg_901" s="T330">0.3:S v:pred</ta>
            <ta e="T332" id="Seg_902" s="T331">pro.h:S</ta>
            <ta e="T333" id="Seg_903" s="T332">v:pred</ta>
            <ta e="T337" id="Seg_904" s="T336">v:pred</ta>
            <ta e="T338" id="Seg_905" s="T337">v:O</ta>
            <ta e="T341" id="Seg_906" s="T338">s:compl</ta>
            <ta e="T345" id="Seg_907" s="T341">s:compl</ta>
            <ta e="T346" id="Seg_908" s="T345">np.h:S</ta>
            <ta e="T347" id="Seg_909" s="T346">v:pred</ta>
            <ta e="T348" id="Seg_910" s="T347">0.3.h:S v:pred</ta>
            <ta e="T349" id="Seg_911" s="T348">0.3.h:S v:pred</ta>
            <ta e="T351" id="Seg_912" s="T350">np.h:S</ta>
            <ta e="T354" id="Seg_913" s="T353">v:pred</ta>
            <ta e="T355" id="Seg_914" s="T354">0.3.h:S v:pred</ta>
            <ta e="T357" id="Seg_915" s="T356">np:S</ta>
            <ta e="T360" id="Seg_916" s="T359">v:pred</ta>
            <ta e="T361" id="Seg_917" s="T360">pro.h:S</ta>
            <ta e="T362" id="Seg_918" s="T361">v:pred</ta>
            <ta e="T364" id="Seg_919" s="T362">np:S</ta>
            <ta e="T365" id="Seg_920" s="T364">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T296" id="Seg_921" s="T295">new</ta>
            <ta e="T300" id="Seg_922" s="T299">accs-gen</ta>
            <ta e="T301" id="Seg_923" s="T300">giv-active</ta>
            <ta e="T303" id="Seg_924" s="T302">giv-active</ta>
            <ta e="T309" id="Seg_925" s="T308">0.giv-active</ta>
            <ta e="T310" id="Seg_926" s="T309">giv-active</ta>
            <ta e="T312" id="Seg_927" s="T311">accs-sit</ta>
            <ta e="T316" id="Seg_928" s="T315">accs-gen</ta>
            <ta e="T317" id="Seg_929" s="T316">0.giv-active</ta>
            <ta e="T318" id="Seg_930" s="T317">giv-active</ta>
            <ta e="T323" id="Seg_931" s="T322">accs-sit</ta>
            <ta e="T324" id="Seg_932" s="T323">giv-active</ta>
            <ta e="T326" id="Seg_933" s="T325">accs-sit</ta>
            <ta e="T327" id="Seg_934" s="T326">accs-sit</ta>
            <ta e="T332" id="Seg_935" s="T331">giv-inactive</ta>
            <ta e="T335" id="Seg_936" s="T334">giv-active</ta>
            <ta e="T336" id="Seg_937" s="T335">accs-gen</ta>
            <ta e="T343" id="Seg_938" s="T342">0.accs-gen</ta>
            <ta e="T344" id="Seg_939" s="T343">accs-gen</ta>
            <ta e="T346" id="Seg_940" s="T345">giv-active</ta>
            <ta e="T348" id="Seg_941" s="T347">0.giv-active</ta>
            <ta e="T349" id="Seg_942" s="T348">0.giv-active</ta>
            <ta e="T351" id="Seg_943" s="T350">accs-gen</ta>
            <ta e="T353" id="Seg_944" s="T352">accs-gen</ta>
            <ta e="T355" id="Seg_945" s="T354">0.giv-active</ta>
            <ta e="T357" id="Seg_946" s="T356">giv-active</ta>
            <ta e="T359" id="Seg_947" s="T358">accs-gen</ta>
            <ta e="T361" id="Seg_948" s="T360">giv-inactive</ta>
            <ta e="T364" id="Seg_949" s="T362">accs-gen</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T296" id="Seg_950" s="T295">RUS:cult</ta>
            <ta e="T344" id="Seg_951" s="T343">RUS:cult</ta>
            <ta e="T346" id="Seg_952" s="T345">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T300" id="Seg_953" s="T295">Степан упал [прыгнул] в теплую воду.</ta>
            <ta e="T319" id="Seg_954" s="T300">Он переплыл реку два раза туда и обратно, вышел из воды на берег и, как добрый конь, побежал от реки.</ta>
            <ta e="T326" id="Seg_955" s="T319">[По дороге] домой [позади] оставались пески, река, высокий хвощ.</ta>
            <ta e="T331" id="Seg_956" s="T326">У нас не так.</ta>
            <ta e="T334" id="Seg_957" s="T331">Он ходит без работы.</ta>
            <ta e="T345" id="Seg_958" s="T334">Он давно хотел знать, что там делают, что говорят о Москве.</ta>
            <ta e="T348" id="Seg_959" s="T345">Степан стоит, думает.</ta>
            <ta e="T349" id="Seg_960" s="T348">Остановился.</ta>
            <ta e="T355" id="Seg_961" s="T349">Много людей на этой земле воевали, умирали.</ta>
            <ta e="T360" id="Seg_962" s="T355">Эта земля вся кровью облита.</ta>
            <ta e="T365" id="Seg_963" s="T360">Он плачет, слёзы текут. </ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T300" id="Seg_964" s="T295">Stepan fell [jumped] into warm water.</ta>
            <ta e="T319" id="Seg_965" s="T300">He crossed the small river twice there and back, went out of the water and ran like a good horse away from the small river.</ta>
            <ta e="T326" id="Seg_966" s="T319">On [his] way home the sand, the small river and the high equisetum stayed [back].</ta>
            <ta e="T331" id="Seg_967" s="T326">We don't have it like this.</ta>
            <ta e="T334" id="Seg_968" s="T331">He is walking around without a job.</ta>
            <ta e="T345" id="Seg_969" s="T334">He always wanted to know what they are doing there, what they are saying about Moscow.</ta>
            <ta e="T348" id="Seg_970" s="T345">Stepan is standing and thinking.</ta>
            <ta e="T349" id="Seg_971" s="T348">He stopped.</ta>
            <ta e="T355" id="Seg_972" s="T349">Many people were fighting and dying on this land.</ta>
            <ta e="T360" id="Seg_973" s="T355">This land is covered with blood.</ta>
            <ta e="T365" id="Seg_974" s="T360">He cries, tears are running out of his eyes.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T300" id="Seg_975" s="T295">Stepan fiel [sprang] ins warme Wasser.</ta>
            <ta e="T319" id="Seg_976" s="T300">Er überquerte den kleinen Fluss zweimal hin und zurück, ging aus dem Wasser raus und lief wie ein gutes Pferd vom kleinen Fluss weg.</ta>
            <ta e="T326" id="Seg_977" s="T319">Auf dem Heimweg blieben Sand, der kleine Fluss und der hohe Schachtelhalm [hinter ihm zurück].</ta>
            <ta e="T331" id="Seg_978" s="T326">Bei uns ist es nicht so.</ta>
            <ta e="T334" id="Seg_979" s="T331">Er läuft arbeitslos herum. </ta>
            <ta e="T345" id="Seg_980" s="T334">Er wollte schon immer wissen, was sie dort machen, was sie über Moskau sagen.</ta>
            <ta e="T348" id="Seg_981" s="T345">Stepan steht, denkt.</ta>
            <ta e="T349" id="Seg_982" s="T348">Er blieb stehen.</ta>
            <ta e="T355" id="Seg_983" s="T349">Viele Menschen kämpften und starben auf diesem Land.</ta>
            <ta e="T360" id="Seg_984" s="T355">Dieses Land ist mit Blut bedeckt.</ta>
            <ta e="T365" id="Seg_985" s="T360">Er weint, aus seinen Augen strömen Tränen.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T300" id="Seg_986" s="T295">Степан упал [прыгнул] в теплую воду</ta>
            <ta e="T319" id="Seg_987" s="T300">он переплыл реку два раза туда и обратно, вышел из воды на берег как хорошая лошадь побежал от реки</ta>
            <ta e="T326" id="Seg_988" s="T319">пески высокая болотная трава</ta>
            <ta e="T331" id="Seg_989" s="T326">у нас не так есть</ta>
            <ta e="T334" id="Seg_990" s="T331">он ходит без работы [без дела]</ta>
            <ta e="T345" id="Seg_991" s="T334">ему давно хотелось знать что делают что там говорили о Москве</ta>
            <ta e="T348" id="Seg_992" s="T345">Степан стоит думает</ta>
            <ta e="T349" id="Seg_993" s="T348">остановился</ta>
            <ta e="T355" id="Seg_994" s="T349">много людей на этой земле воевали и умирали</ta>
            <ta e="T360" id="Seg_995" s="T355">эта земля вся кровью облита</ta>
            <ta e="T365" id="Seg_996" s="T360">он плачет, слезы текут</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T331" id="Seg_997" s="T326">[OSV:] "meː mɨqɨn" - "at our's". </ta>
            <ta e="T345" id="Seg_998" s="T334">[OSV:] "čʼaptäːqɨn" - "long time ago".</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T360" id="Seg_999" s="T355">′kамнымба - облита, kам′тӓмпа - облилось, kӓм - кровь</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
