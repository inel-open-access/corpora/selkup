<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>BMM_1964_Lifestory_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">BMM_1964_Lifestory_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">60</ud-information>
            <ud-information attribute-name="# HIAT:w">46</ud-information>
            <ud-information attribute-name="# e">46</ud-information>
            <ud-information attribute-name="# HIAT:u">11</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="BMM">
            <abbreviation>BMM</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="BMM"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T47" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">man</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">Qaŋuldoɣan</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">tʼeːlamban</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_14" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">nännɨ</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">tiː</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">manändi</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">orambaŋ</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_29" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">äːsem</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">mazim</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">tawkan</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">aːlǯə</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_44" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">tämdɨ</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_48" n="HIAT:ip">(</nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">tatpa</ts>
                  <nts id="Seg_51" n="HIAT:ip">)</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_54" n="HIAT:w" s="T14">laɣakkus</ts>
                  <nts id="Seg_55" n="HIAT:ip">.</nts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_58" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_60" n="HIAT:w" s="T15">tämdi</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_63" n="HIAT:w" s="T16">me</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_66" n="HIAT:w" s="T17">ilakusot</ts>
                  <nts id="Seg_67" n="HIAT:ip">.</nts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_70" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_72" n="HIAT:w" s="T18">tämdilʼelʼe</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_75" n="HIAT:w" s="T19">man</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_78" n="HIAT:w" s="T20">oramba</ts>
                  <nts id="Seg_79" n="HIAT:ip">,</nts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_82" n="HIAT:w" s="T21">tämdilʼelʼe</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_85" n="HIAT:w" s="T22">laqqälʼe</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">oːlǯeseŋ</ts>
                  <nts id="Seg_89" n="HIAT:ip">.</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_92" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_94" n="HIAT:w" s="T24">päiː</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_97" n="HIAT:w" s="T25">pilʼičukizaŋ</ts>
                  <nts id="Seg_98" n="HIAT:ip">.</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_101" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_103" n="HIAT:w" s="T26">nannä</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_106" n="HIAT:w" s="T27">načalʼnikan</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_109" n="HIAT:w" s="T28">maːttan</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_112" n="HIAT:w" s="T29">aŋgan</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_115" n="HIAT:w" s="T30">sasaj</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_118" n="HIAT:w" s="T31">čuwanʼdʼikusaŋ</ts>
                  <nts id="Seg_119" n="HIAT:ip">.</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_122" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_124" n="HIAT:w" s="T32">nännä</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_127" n="HIAT:w" s="T33">man</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_130" n="HIAT:w" s="T34">parahottə</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_133" n="HIAT:w" s="T35">kačegar</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_136" n="HIAT:w" s="T36">umbɨlʼe</ts>
                  <nts id="Seg_137" n="HIAT:ip">.</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_140" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_142" n="HIAT:w" s="T37">nar</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_145" n="HIAT:w" s="T38">pändə</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_148" n="HIAT:w" s="T39">kundə</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_151" n="HIAT:w" s="T40">man</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_154" n="HIAT:w" s="T41">paldʼüsan</ts>
                  <nts id="Seg_155" n="HIAT:ip">.</nts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_158" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_160" n="HIAT:w" s="T42">tettə</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_163" n="HIAT:w" s="T43">pändə</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_166" n="HIAT:w" s="T44">kundə</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_169" n="HIAT:w" s="T45">matrosam</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_172" n="HIAT:w" s="T46">umbikuzan</ts>
                  <nts id="Seg_173" n="HIAT:ip">.</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T47" id="Seg_175" n="sc" s="T1">
               <ts e="T2" id="Seg_177" n="e" s="T1">man </ts>
               <ts e="T3" id="Seg_179" n="e" s="T2">Qaŋuldoɣan </ts>
               <ts e="T4" id="Seg_181" n="e" s="T3">tʼeːlamban. </ts>
               <ts e="T5" id="Seg_183" n="e" s="T4">nännɨ </ts>
               <ts e="T6" id="Seg_185" n="e" s="T5">tiː </ts>
               <ts e="T7" id="Seg_187" n="e" s="T6">manändi </ts>
               <ts e="T8" id="Seg_189" n="e" s="T7">orambaŋ. </ts>
               <ts e="T9" id="Seg_191" n="e" s="T8">äːsem </ts>
               <ts e="T10" id="Seg_193" n="e" s="T9">mazim </ts>
               <ts e="T11" id="Seg_195" n="e" s="T10">tawkan </ts>
               <ts e="T12" id="Seg_197" n="e" s="T11">aːlǯə. </ts>
               <ts e="T13" id="Seg_199" n="e" s="T12">tämdɨ </ts>
               <ts e="T14" id="Seg_201" n="e" s="T13">(tatpa) </ts>
               <ts e="T15" id="Seg_203" n="e" s="T14">laɣakkus. </ts>
               <ts e="T16" id="Seg_205" n="e" s="T15">tämdi </ts>
               <ts e="T17" id="Seg_207" n="e" s="T16">me </ts>
               <ts e="T18" id="Seg_209" n="e" s="T17">ilakusot. </ts>
               <ts e="T19" id="Seg_211" n="e" s="T18">tämdilʼelʼe </ts>
               <ts e="T20" id="Seg_213" n="e" s="T19">man </ts>
               <ts e="T21" id="Seg_215" n="e" s="T20">oramba, </ts>
               <ts e="T22" id="Seg_217" n="e" s="T21">tämdilʼelʼe </ts>
               <ts e="T23" id="Seg_219" n="e" s="T22">laqqälʼe </ts>
               <ts e="T24" id="Seg_221" n="e" s="T23">oːlǯeseŋ. </ts>
               <ts e="T25" id="Seg_223" n="e" s="T24">päiː </ts>
               <ts e="T26" id="Seg_225" n="e" s="T25">pilʼičukizaŋ. </ts>
               <ts e="T27" id="Seg_227" n="e" s="T26">nannä </ts>
               <ts e="T28" id="Seg_229" n="e" s="T27">načalʼnikan </ts>
               <ts e="T29" id="Seg_231" n="e" s="T28">maːttan </ts>
               <ts e="T30" id="Seg_233" n="e" s="T29">aŋgan </ts>
               <ts e="T31" id="Seg_235" n="e" s="T30">sasaj </ts>
               <ts e="T32" id="Seg_237" n="e" s="T31">čuwanʼdʼikusaŋ. </ts>
               <ts e="T33" id="Seg_239" n="e" s="T32">nännä </ts>
               <ts e="T34" id="Seg_241" n="e" s="T33">man </ts>
               <ts e="T35" id="Seg_243" n="e" s="T34">parahottə </ts>
               <ts e="T36" id="Seg_245" n="e" s="T35">kačegar </ts>
               <ts e="T37" id="Seg_247" n="e" s="T36">umbɨlʼe. </ts>
               <ts e="T38" id="Seg_249" n="e" s="T37">nar </ts>
               <ts e="T39" id="Seg_251" n="e" s="T38">pändə </ts>
               <ts e="T40" id="Seg_253" n="e" s="T39">kundə </ts>
               <ts e="T41" id="Seg_255" n="e" s="T40">man </ts>
               <ts e="T42" id="Seg_257" n="e" s="T41">paldʼüsan. </ts>
               <ts e="T43" id="Seg_259" n="e" s="T42">tettə </ts>
               <ts e="T44" id="Seg_261" n="e" s="T43">pändə </ts>
               <ts e="T45" id="Seg_263" n="e" s="T44">kundə </ts>
               <ts e="T46" id="Seg_265" n="e" s="T45">matrosam </ts>
               <ts e="T47" id="Seg_267" n="e" s="T46">umbikuzan. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_268" s="T1">BMM_1964_Lifestory_nar.001 (001.001)</ta>
            <ta e="T8" id="Seg_269" s="T4">BMM_1964_Lifestory_nar.002 (001.002)</ta>
            <ta e="T12" id="Seg_270" s="T8">BMM_1964_Lifestory_nar.003 (001.003)</ta>
            <ta e="T15" id="Seg_271" s="T12">BMM_1964_Lifestory_nar.004 (001.004)</ta>
            <ta e="T18" id="Seg_272" s="T15">BMM_1964_Lifestory_nar.005 (001.005)</ta>
            <ta e="T24" id="Seg_273" s="T18">BMM_1964_Lifestory_nar.006 (001.006)</ta>
            <ta e="T26" id="Seg_274" s="T24">BMM_1964_Lifestory_nar.007 (001.007)</ta>
            <ta e="T32" id="Seg_275" s="T26">BMM_1964_Lifestory_nar.008 (001.008)</ta>
            <ta e="T37" id="Seg_276" s="T32">BMM_1964_Lifestory_nar.009 (001.009)</ta>
            <ta e="T42" id="Seg_277" s="T37">BMM_1964_Lifestory_nar.010 (001.010)</ta>
            <ta e="T47" id="Seg_278" s="T42">BMM_1964_Lifestory_nar.011 (001.011)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_279" s="T1">ман ′kаңуlдоɣан ′тʼе̄ламбан.</ta>
            <ta e="T8" id="Seg_280" s="T4">′нӓнны ′тӣ ма′нӓнди о′рамбаң.</ta>
            <ta e="T12" id="Seg_281" s="T8">ӓ̄′сем ма′зим таw′кан ′а̄lджъ.</ta>
            <ta e="T15" id="Seg_282" s="T12">′тӓмды (′татпа) lа′ɣаккус.</ta>
            <ta e="T18" id="Seg_283" s="T15">′тӓмди ме и′lакусот.</ta>
            <ta e="T24" id="Seg_284" s="T18">тӓм′дилʼелʼе ман орамба тӓм′дилʼелʼе ′lаkkӓлʼе ′о̄lдже(и)сең.</ta>
            <ta e="T26" id="Seg_285" s="T24">пӓӣ пилʼитшукизаң.</ta>
            <ta e="T32" id="Seg_286" s="T26">′наннӓ на′чалʼникан ′ма̄ттан ′аңган са′сайчу′ванʼдʼикусаң.</ta>
            <ta e="T37" id="Seg_287" s="T32">′нӓннӓ ман пара′хоттъ каче′гар ′умбылʼе.</ta>
            <ta e="T42" id="Seg_288" s="T37">нар ′пӓндъ ′кундъ ман ′паlдʼӱсан.</ta>
            <ta e="T47" id="Seg_289" s="T42">′теттъ ′пӓндъ кундъ ма′тросам умбикуз(с)ан.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_290" s="T1">man qaŋuldoɣan tʼeːlamban.</ta>
            <ta e="T8" id="Seg_291" s="T4">nännɨ tiː manändi orambaŋ.</ta>
            <ta e="T12" id="Seg_292" s="T8">äːsem mazim tawkan aːlǯə.</ta>
            <ta e="T15" id="Seg_293" s="T12">tämdɨ (tatpa) laɣakkus.</ta>
            <ta e="T18" id="Seg_294" s="T15">tämdi me ilakusot.</ta>
            <ta e="T24" id="Seg_295" s="T18">tämdilʼelʼe man oramba tämdilʼelʼe laqqälʼe oːlǯe(i)seŋ.</ta>
            <ta e="T26" id="Seg_296" s="T24">päiː pilʼitšukizaŋ.</ta>
            <ta e="T32" id="Seg_297" s="T26">nannä načalʼnikan maːttan aŋgan sasajčuvanʼdʼikusaŋ.</ta>
            <ta e="T37" id="Seg_298" s="T32">nännä man parahottə kačegar umbɨlʼe.</ta>
            <ta e="T42" id="Seg_299" s="T37">nar pändə kundə man paldʼüsan.</ta>
            <ta e="T47" id="Seg_300" s="T42">tettə pändə kundə matrosam umbikuz(s)an.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_301" s="T1">man Qaŋuldoɣan tʼeːlamban. </ta>
            <ta e="T8" id="Seg_302" s="T4">nännɨ tiː manändi orambaŋ. </ta>
            <ta e="T12" id="Seg_303" s="T8">äːsem mazim tawkan aːlǯə. </ta>
            <ta e="T15" id="Seg_304" s="T12">tämdɨ (tatpa) laɣakkus. </ta>
            <ta e="T18" id="Seg_305" s="T15">tämdi me ilakusot. </ta>
            <ta e="T24" id="Seg_306" s="T18">tämdilʼelʼe man oramba, tämdilʼelʼe laqqälʼe oːlǯeseŋ. </ta>
            <ta e="T26" id="Seg_307" s="T24">päiː pilʼičukizaŋ. </ta>
            <ta e="T32" id="Seg_308" s="T26">nannä načalʼnikan maːttan aŋgan sasaj čuwanʼdʼikusaŋ. </ta>
            <ta e="T37" id="Seg_309" s="T32">nännä man parahottə kačegar umbɨlʼe. </ta>
            <ta e="T42" id="Seg_310" s="T37">nar pändə kundə man paldʼüsan. </ta>
            <ta e="T47" id="Seg_311" s="T42">tettə pändə kundə matrosam umbikuzan. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_312" s="T1">man</ta>
            <ta e="T3" id="Seg_313" s="T2">Qaŋuldo-ɣan</ta>
            <ta e="T4" id="Seg_314" s="T3">tʼeːla-mba-n</ta>
            <ta e="T5" id="Seg_315" s="T4">nännɨ</ta>
            <ta e="T6" id="Seg_316" s="T5">tiː</ta>
            <ta e="T7" id="Seg_317" s="T6">ma-nändi</ta>
            <ta e="T8" id="Seg_318" s="T7">oram-ba-ŋ</ta>
            <ta e="T9" id="Seg_319" s="T8">äːse-mɨ</ta>
            <ta e="T10" id="Seg_320" s="T9">mazim</ta>
            <ta e="T11" id="Seg_321" s="T10">tawkan</ta>
            <ta e="T12" id="Seg_322" s="T11">aːlǯə</ta>
            <ta e="T13" id="Seg_323" s="T12">tämdɨ</ta>
            <ta e="T14" id="Seg_324" s="T13">tatpa</ta>
            <ta e="T15" id="Seg_325" s="T14">laɣa-ku-s</ta>
            <ta e="T16" id="Seg_326" s="T15">tämdi</ta>
            <ta e="T17" id="Seg_327" s="T16">me</ta>
            <ta e="T18" id="Seg_328" s="T17">ila-ku-so-t</ta>
            <ta e="T19" id="Seg_329" s="T18">tämdi-lʼelʼe</ta>
            <ta e="T20" id="Seg_330" s="T19">man</ta>
            <ta e="T21" id="Seg_331" s="T20">oram-ba</ta>
            <ta e="T22" id="Seg_332" s="T21">tämdi-lʼelʼe</ta>
            <ta e="T23" id="Seg_333" s="T22">laqqä-lʼe</ta>
            <ta e="T24" id="Seg_334" s="T23">oːlǯe-se-ŋ</ta>
            <ta e="T25" id="Seg_335" s="T24">pä-iː</ta>
            <ta e="T26" id="Seg_336" s="T25">pilʼ-i-ču-ki-za-ŋ</ta>
            <ta e="T27" id="Seg_337" s="T26">nannä</ta>
            <ta e="T28" id="Seg_338" s="T27">načalʼnik-a-n</ta>
            <ta e="T29" id="Seg_339" s="T28">maːtt-a-n</ta>
            <ta e="T30" id="Seg_340" s="T29">aŋ-gan</ta>
            <ta e="T31" id="Seg_341" s="T30">sasaj</ta>
            <ta e="T32" id="Seg_342" s="T31">čuwa-nʼdʼi-ku-sa-ŋ</ta>
            <ta e="T33" id="Seg_343" s="T32">nännä</ta>
            <ta e="T34" id="Seg_344" s="T33">man</ta>
            <ta e="T35" id="Seg_345" s="T34">parahot-tə</ta>
            <ta e="T36" id="Seg_346" s="T35">kačegar</ta>
            <ta e="T37" id="Seg_347" s="T36">u-mbɨ-lʼe</ta>
            <ta e="T38" id="Seg_348" s="T37">nar</ta>
            <ta e="T39" id="Seg_349" s="T38">pä-n-də</ta>
            <ta e="T40" id="Seg_350" s="T39">kundə</ta>
            <ta e="T41" id="Seg_351" s="T40">man</ta>
            <ta e="T42" id="Seg_352" s="T41">paldʼü-sa-n</ta>
            <ta e="T43" id="Seg_353" s="T42">tettə</ta>
            <ta e="T44" id="Seg_354" s="T43">pä-n-də</ta>
            <ta e="T45" id="Seg_355" s="T44">kundə</ta>
            <ta e="T46" id="Seg_356" s="T45">matrosam</ta>
            <ta e="T47" id="Seg_357" s="T46">u-mbi-ku-za-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_358" s="T1">man</ta>
            <ta e="T3" id="Seg_359" s="T2">Qaŋuldo-qən</ta>
            <ta e="T4" id="Seg_360" s="T3">tʼeːlɨ-mbɨ-ŋ</ta>
            <ta e="T5" id="Seg_361" s="T4">nɨːnɨ</ta>
            <ta e="T6" id="Seg_362" s="T5">tiː</ta>
            <ta e="T7" id="Seg_363" s="T6">maːt-nannɨ</ta>
            <ta e="T8" id="Seg_364" s="T7">orɨm-mbɨ-ŋ</ta>
            <ta e="T9" id="Seg_365" s="T8">ässɨ-mɨ</ta>
            <ta e="T10" id="Seg_366" s="T9">mašim</ta>
            <ta e="T11" id="Seg_367" s="T10">tawkan</ta>
            <ta e="T12" id="Seg_368" s="T11">aːlǯə</ta>
            <ta e="T13" id="Seg_369" s="T12">tɨmtɨ</ta>
            <ta e="T14" id="Seg_370" s="T13">tatpa</ta>
            <ta e="T15" id="Seg_371" s="T14">laqqɨ-ku-sɨ</ta>
            <ta e="T16" id="Seg_372" s="T15">tɨmtɨ</ta>
            <ta e="T17" id="Seg_373" s="T16">meː</ta>
            <ta e="T18" id="Seg_374" s="T17">illɨ-ku-sɨ-ut</ta>
            <ta e="T19" id="Seg_375" s="T18">tɨmtɨ-lʼelʼe</ta>
            <ta e="T20" id="Seg_376" s="T19">man</ta>
            <ta e="T21" id="Seg_377" s="T20">orɨm-mbɨ</ta>
            <ta e="T22" id="Seg_378" s="T21">tɨmtɨ-lʼelʼe</ta>
            <ta e="T23" id="Seg_379" s="T22">laqqɨ-le</ta>
            <ta e="T24" id="Seg_380" s="T23">oldǝ-sɨ-ŋ</ta>
            <ta e="T25" id="Seg_381" s="T24">po-ɨ</ta>
            <ta e="T26" id="Seg_382" s="T25">pilʼ-ɨ-ču-qi-sɨ-ŋ</ta>
            <ta e="T27" id="Seg_383" s="T26">nɨːnɨ</ta>
            <ta e="T28" id="Seg_384" s="T27">načalʼnik-ɨ-n</ta>
            <ta e="T29" id="Seg_385" s="T28">maːt-ɨ-n</ta>
            <ta e="T30" id="Seg_386" s="T29">aːŋ-qən</ta>
            <ta e="T31" id="Seg_387" s="T30">sasä</ta>
            <ta e="T32" id="Seg_388" s="T31">čawe-nče-ku-sɨ-ŋ</ta>
            <ta e="T33" id="Seg_389" s="T32">nɨːnɨ</ta>
            <ta e="T34" id="Seg_390" s="T33">man</ta>
            <ta e="T35" id="Seg_391" s="T34">parahot-ndɨ</ta>
            <ta e="T36" id="Seg_392" s="T35">kačegar</ta>
            <ta e="T37" id="Seg_393" s="T36">eː-mbɨ-le</ta>
            <ta e="T38" id="Seg_394" s="T37">nakkɨr</ta>
            <ta e="T39" id="Seg_395" s="T38">po-n-tɨ</ta>
            <ta e="T40" id="Seg_396" s="T39">kundɨ</ta>
            <ta e="T41" id="Seg_397" s="T40">man</ta>
            <ta e="T42" id="Seg_398" s="T41">paldʼu-sɨ-ŋ</ta>
            <ta e="T43" id="Seg_399" s="T42">tättɨ</ta>
            <ta e="T44" id="Seg_400" s="T43">po-n-tɨ</ta>
            <ta e="T45" id="Seg_401" s="T44">kundɨ</ta>
            <ta e="T46" id="Seg_402" s="T45">matrosam</ta>
            <ta e="T47" id="Seg_403" s="T46">eː-mbɨ-ku-sɨ-ŋ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_404" s="T1">I.NOM</ta>
            <ta e="T3" id="Seg_405" s="T2">Laskovo-LOC</ta>
            <ta e="T4" id="Seg_406" s="T3">be.born-PST.NAR-1SG.S</ta>
            <ta e="T5" id="Seg_407" s="T4">then</ta>
            <ta e="T6" id="Seg_408" s="T5">now</ta>
            <ta e="T7" id="Seg_409" s="T6">house-ABL</ta>
            <ta e="T8" id="Seg_410" s="T7">grow.up-PST.NAR-1SG.S</ta>
            <ta e="T9" id="Seg_411" s="T8">father.[NOM]-1SG</ta>
            <ta e="T10" id="Seg_412" s="T9">I.ACC</ta>
            <ta e="T11" id="Seg_413" s="T10">from.here</ta>
            <ta e="T12" id="Seg_414" s="T11">%%.[3SG.S]</ta>
            <ta e="T13" id="Seg_415" s="T12">here</ta>
            <ta e="T14" id="Seg_416" s="T13">%%</ta>
            <ta e="T15" id="Seg_417" s="T14">work-HAB-PST.[3SG.S]</ta>
            <ta e="T16" id="Seg_418" s="T15">here</ta>
            <ta e="T17" id="Seg_419" s="T16">we.NOM</ta>
            <ta e="T18" id="Seg_420" s="T17">live-HAB-PST-1PL</ta>
            <ta e="T19" id="Seg_421" s="T18">here-%%</ta>
            <ta e="T20" id="Seg_422" s="T19">I.NOM</ta>
            <ta e="T21" id="Seg_423" s="T20">grow.up-PST.NAR.[3SG.S]</ta>
            <ta e="T22" id="Seg_424" s="T21">here-%%</ta>
            <ta e="T23" id="Seg_425" s="T22">work-CVB</ta>
            <ta e="T24" id="Seg_426" s="T23">start-PST-1SG.S</ta>
            <ta e="T25" id="Seg_427" s="T24">firewood-EP.[NOM]</ta>
            <ta e="T26" id="Seg_428" s="T25">saw.off-EP-DRV-DU-PST-1SG.S</ta>
            <ta e="T27" id="Seg_429" s="T26">then</ta>
            <ta e="T28" id="Seg_430" s="T27">chief-EP-GEN</ta>
            <ta e="T29" id="Seg_431" s="T28">house-EP-GEN</ta>
            <ta e="T30" id="Seg_432" s="T29">mouth-LOC</ta>
            <ta e="T31" id="Seg_433" s="T30">garbage.[NOM]</ta>
            <ta e="T32" id="Seg_434" s="T31">sweep.up-IPFV3-HAB-PST-1SG.S</ta>
            <ta e="T33" id="Seg_435" s="T32">then</ta>
            <ta e="T34" id="Seg_436" s="T33">I.NOM</ta>
            <ta e="T35" id="Seg_437" s="T34">steamship-ILL</ta>
            <ta e="T36" id="Seg_438" s="T35">stoker.[NOM]</ta>
            <ta e="T37" id="Seg_439" s="T36">be-DUR-CVB</ta>
            <ta e="T38" id="Seg_440" s="T37">three</ta>
            <ta e="T39" id="Seg_441" s="T38">year-GEN-3SG</ta>
            <ta e="T40" id="Seg_442" s="T39">during</ta>
            <ta e="T41" id="Seg_443" s="T40">I.NOM</ta>
            <ta e="T42" id="Seg_444" s="T41">go-PST-1SG.S</ta>
            <ta e="T43" id="Seg_445" s="T42">four</ta>
            <ta e="T44" id="Seg_446" s="T43">year-GEN-3SG</ta>
            <ta e="T45" id="Seg_447" s="T44">during</ta>
            <ta e="T46" id="Seg_448" s="T45">seaman.INSTR</ta>
            <ta e="T47" id="Seg_449" s="T46">be-DUR-HAB-PST-1SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_450" s="T1">я.NOM</ta>
            <ta e="T3" id="Seg_451" s="T2">Ласково-LOC</ta>
            <ta e="T4" id="Seg_452" s="T3">родиться-PST.NAR-1SG.S</ta>
            <ta e="T5" id="Seg_453" s="T4">потом</ta>
            <ta e="T6" id="Seg_454" s="T5">теперь</ta>
            <ta e="T7" id="Seg_455" s="T6">дом-ABL</ta>
            <ta e="T8" id="Seg_456" s="T7">вырасти-PST.NAR-1SG.S</ta>
            <ta e="T9" id="Seg_457" s="T8">отец.[NOM]-1SG</ta>
            <ta e="T10" id="Seg_458" s="T9">я.ACC</ta>
            <ta e="T11" id="Seg_459" s="T10">отсюда</ta>
            <ta e="T12" id="Seg_460" s="T11">%%.[3SG.S]</ta>
            <ta e="T13" id="Seg_461" s="T12">здесь</ta>
            <ta e="T14" id="Seg_462" s="T13">%%</ta>
            <ta e="T15" id="Seg_463" s="T14">работать-HAB-PST.[3SG.S]</ta>
            <ta e="T16" id="Seg_464" s="T15">здесь</ta>
            <ta e="T17" id="Seg_465" s="T16">мы.NOM</ta>
            <ta e="T18" id="Seg_466" s="T17">жить-HAB-PST-1PL</ta>
            <ta e="T19" id="Seg_467" s="T18">здесь-%%</ta>
            <ta e="T20" id="Seg_468" s="T19">я.NOM</ta>
            <ta e="T21" id="Seg_469" s="T20">вырасти-PST.NAR.[3SG.S]</ta>
            <ta e="T22" id="Seg_470" s="T21">здесь-%%</ta>
            <ta e="T23" id="Seg_471" s="T22">работать-CVB</ta>
            <ta e="T24" id="Seg_472" s="T23">начать-PST-1SG.S</ta>
            <ta e="T25" id="Seg_473" s="T24">дрова-EP.[NOM]</ta>
            <ta e="T26" id="Seg_474" s="T25">отпилить-EP-DRV-DU-PST-1SG.S</ta>
            <ta e="T27" id="Seg_475" s="T26">потом</ta>
            <ta e="T28" id="Seg_476" s="T27">начальник-EP-GEN</ta>
            <ta e="T29" id="Seg_477" s="T28">дом-EP-GEN</ta>
            <ta e="T30" id="Seg_478" s="T29">рот-LOC</ta>
            <ta e="T31" id="Seg_479" s="T30">мусор.[NOM]</ta>
            <ta e="T32" id="Seg_480" s="T31">подмести-IPFV3-HAB-PST-1SG.S</ta>
            <ta e="T33" id="Seg_481" s="T32">потом</ta>
            <ta e="T34" id="Seg_482" s="T33">я.NOM</ta>
            <ta e="T35" id="Seg_483" s="T34">пароход-ILL</ta>
            <ta e="T36" id="Seg_484" s="T35">кочегар.[NOM]</ta>
            <ta e="T37" id="Seg_485" s="T36">быть-DUR-CVB</ta>
            <ta e="T38" id="Seg_486" s="T37">три</ta>
            <ta e="T39" id="Seg_487" s="T38">год-GEN-3SG</ta>
            <ta e="T40" id="Seg_488" s="T39">в.течение</ta>
            <ta e="T41" id="Seg_489" s="T40">я.NOM</ta>
            <ta e="T42" id="Seg_490" s="T41">ходить-PST-1SG.S</ta>
            <ta e="T43" id="Seg_491" s="T42">четыре</ta>
            <ta e="T44" id="Seg_492" s="T43">год-GEN-3SG</ta>
            <ta e="T45" id="Seg_493" s="T44">в.течение</ta>
            <ta e="T46" id="Seg_494" s="T45">матрос.INSTR</ta>
            <ta e="T47" id="Seg_495" s="T46">быть-DUR-HAB-PST-1SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_496" s="T1">pers</ta>
            <ta e="T3" id="Seg_497" s="T2">nprop-n:case</ta>
            <ta e="T4" id="Seg_498" s="T3">v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_499" s="T4">adv</ta>
            <ta e="T6" id="Seg_500" s="T5">adv</ta>
            <ta e="T7" id="Seg_501" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_502" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_503" s="T8">n.[n:case]-n:poss</ta>
            <ta e="T10" id="Seg_504" s="T9">pers</ta>
            <ta e="T11" id="Seg_505" s="T10">adv</ta>
            <ta e="T12" id="Seg_506" s="T11">v.[v:pn]</ta>
            <ta e="T13" id="Seg_507" s="T12">adv</ta>
            <ta e="T14" id="Seg_508" s="T13">adv</ta>
            <ta e="T15" id="Seg_509" s="T14">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T16" id="Seg_510" s="T15">adv</ta>
            <ta e="T17" id="Seg_511" s="T16">pers</ta>
            <ta e="T18" id="Seg_512" s="T17">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_513" s="T18">adv-adv&gt;adv</ta>
            <ta e="T20" id="Seg_514" s="T19">pers</ta>
            <ta e="T21" id="Seg_515" s="T20">v-v:tense.[v:pn]</ta>
            <ta e="T22" id="Seg_516" s="T21">adv-adv&gt;adv</ta>
            <ta e="T23" id="Seg_517" s="T22">v-v&gt;adv</ta>
            <ta e="T24" id="Seg_518" s="T23">v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_519" s="T24">n-n:ins.[n:case]</ta>
            <ta e="T26" id="Seg_520" s="T25">v-n:ins-v&gt;v-n:num-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_521" s="T26">adv</ta>
            <ta e="T28" id="Seg_522" s="T27">n-n:ins-n:case</ta>
            <ta e="T29" id="Seg_523" s="T28">n-n:ins-n:case</ta>
            <ta e="T30" id="Seg_524" s="T29">n-n:case</ta>
            <ta e="T31" id="Seg_525" s="T30">n.[n:case]</ta>
            <ta e="T32" id="Seg_526" s="T31">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T33" id="Seg_527" s="T32">adv</ta>
            <ta e="T34" id="Seg_528" s="T33">pers</ta>
            <ta e="T35" id="Seg_529" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_530" s="T35">n.[n:case]</ta>
            <ta e="T37" id="Seg_531" s="T36">v-v&gt;v-v&gt;adv</ta>
            <ta e="T38" id="Seg_532" s="T37">num</ta>
            <ta e="T39" id="Seg_533" s="T38">n-n:case-n:poss</ta>
            <ta e="T40" id="Seg_534" s="T39">pp</ta>
            <ta e="T41" id="Seg_535" s="T40">pers</ta>
            <ta e="T42" id="Seg_536" s="T41">v-v:tense-v:pn</ta>
            <ta e="T43" id="Seg_537" s="T42">num</ta>
            <ta e="T44" id="Seg_538" s="T43">n-n:case-n:poss</ta>
            <ta e="T45" id="Seg_539" s="T44">pp</ta>
            <ta e="T46" id="Seg_540" s="T45">n</ta>
            <ta e="T47" id="Seg_541" s="T46">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_542" s="T1">pro</ta>
            <ta e="T3" id="Seg_543" s="T2">nprop</ta>
            <ta e="T4" id="Seg_544" s="T3">v</ta>
            <ta e="T5" id="Seg_545" s="T4">adv</ta>
            <ta e="T6" id="Seg_546" s="T5">adv</ta>
            <ta e="T7" id="Seg_547" s="T6">n</ta>
            <ta e="T8" id="Seg_548" s="T7">v</ta>
            <ta e="T9" id="Seg_549" s="T8">n</ta>
            <ta e="T10" id="Seg_550" s="T9">pers</ta>
            <ta e="T11" id="Seg_551" s="T10">adv</ta>
            <ta e="T12" id="Seg_552" s="T11">v</ta>
            <ta e="T13" id="Seg_553" s="T12">adv</ta>
            <ta e="T14" id="Seg_554" s="T13">adv</ta>
            <ta e="T15" id="Seg_555" s="T14">v</ta>
            <ta e="T16" id="Seg_556" s="T15">adv</ta>
            <ta e="T17" id="Seg_557" s="T16">pers</ta>
            <ta e="T18" id="Seg_558" s="T17">v</ta>
            <ta e="T19" id="Seg_559" s="T18">adv</ta>
            <ta e="T20" id="Seg_560" s="T19">pers</ta>
            <ta e="T21" id="Seg_561" s="T20">v</ta>
            <ta e="T22" id="Seg_562" s="T21">adv</ta>
            <ta e="T23" id="Seg_563" s="T22">adv</ta>
            <ta e="T24" id="Seg_564" s="T23">v</ta>
            <ta e="T25" id="Seg_565" s="T24">n</ta>
            <ta e="T26" id="Seg_566" s="T25">v</ta>
            <ta e="T27" id="Seg_567" s="T26">adv</ta>
            <ta e="T28" id="Seg_568" s="T27">n</ta>
            <ta e="T29" id="Seg_569" s="T28">n</ta>
            <ta e="T30" id="Seg_570" s="T29">n</ta>
            <ta e="T31" id="Seg_571" s="T30">n</ta>
            <ta e="T32" id="Seg_572" s="T31">v</ta>
            <ta e="T33" id="Seg_573" s="T32">adv</ta>
            <ta e="T34" id="Seg_574" s="T33">pers</ta>
            <ta e="T35" id="Seg_575" s="T34">n</ta>
            <ta e="T36" id="Seg_576" s="T35">n</ta>
            <ta e="T37" id="Seg_577" s="T36">adv</ta>
            <ta e="T38" id="Seg_578" s="T37">num</ta>
            <ta e="T39" id="Seg_579" s="T38">n</ta>
            <ta e="T40" id="Seg_580" s="T39">pp</ta>
            <ta e="T41" id="Seg_581" s="T40">pers</ta>
            <ta e="T42" id="Seg_582" s="T41">v</ta>
            <ta e="T43" id="Seg_583" s="T42">n</ta>
            <ta e="T44" id="Seg_584" s="T43">n</ta>
            <ta e="T45" id="Seg_585" s="T44">pp</ta>
            <ta e="T46" id="Seg_586" s="T45">n</ta>
            <ta e="T47" id="Seg_587" s="T46">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_588" s="T1">pro.h:S</ta>
            <ta e="T4" id="Seg_589" s="T3">v:pred</ta>
            <ta e="T8" id="Seg_590" s="T7">0.3.h:S v:pred</ta>
            <ta e="T9" id="Seg_591" s="T8">np.h:S</ta>
            <ta e="T10" id="Seg_592" s="T9">pro.h:O</ta>
            <ta e="T12" id="Seg_593" s="T11">v:pred</ta>
            <ta e="T15" id="Seg_594" s="T14">0.3.h:S v:pred</ta>
            <ta e="T17" id="Seg_595" s="T16">pro.h:S</ta>
            <ta e="T18" id="Seg_596" s="T17">v:pred</ta>
            <ta e="T20" id="Seg_597" s="T19">pro.h:S</ta>
            <ta e="T21" id="Seg_598" s="T20">v:pred</ta>
            <ta e="T23" id="Seg_599" s="T22">v:O</ta>
            <ta e="T24" id="Seg_600" s="T23">0.1.h:S v:pred</ta>
            <ta e="T25" id="Seg_601" s="T24">np:O</ta>
            <ta e="T26" id="Seg_602" s="T25">0.1.h:S v:pred</ta>
            <ta e="T31" id="Seg_603" s="T30">np:O</ta>
            <ta e="T32" id="Seg_604" s="T31">0.1.h:S v:pred</ta>
            <ta e="T34" id="Seg_605" s="T33">pro.h:S</ta>
            <ta e="T41" id="Seg_606" s="T40">pro.h:S</ta>
            <ta e="T42" id="Seg_607" s="T41">v:pred</ta>
            <ta e="T46" id="Seg_608" s="T45">n:pred</ta>
            <ta e="T47" id="Seg_609" s="T46">0.1.h:S</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_610" s="T1">pro.h:P</ta>
            <ta e="T3" id="Seg_611" s="T2">np:L</ta>
            <ta e="T5" id="Seg_612" s="T4">adv:Time</ta>
            <ta e="T6" id="Seg_613" s="T5">adv:Time</ta>
            <ta e="T8" id="Seg_614" s="T7">0.3.h:P</ta>
            <ta e="T9" id="Seg_615" s="T8">np.h:A 0.1.h:Poss</ta>
            <ta e="T10" id="Seg_616" s="T9">pro.h:Th</ta>
            <ta e="T13" id="Seg_617" s="T12">adv:L</ta>
            <ta e="T15" id="Seg_618" s="T14">0.3.h:A</ta>
            <ta e="T16" id="Seg_619" s="T15">adv:L</ta>
            <ta e="T17" id="Seg_620" s="T16">pro.h:Th</ta>
            <ta e="T19" id="Seg_621" s="T18">adv:L</ta>
            <ta e="T20" id="Seg_622" s="T19">pro.h:P</ta>
            <ta e="T22" id="Seg_623" s="T21">adv:L</ta>
            <ta e="T24" id="Seg_624" s="T23">0.1.h:A</ta>
            <ta e="T25" id="Seg_625" s="T24">np:P</ta>
            <ta e="T26" id="Seg_626" s="T25">0.1.h:A</ta>
            <ta e="T27" id="Seg_627" s="T26">adv:Time</ta>
            <ta e="T28" id="Seg_628" s="T27">np.h:Poss</ta>
            <ta e="T29" id="Seg_629" s="T28">np:Poss</ta>
            <ta e="T30" id="Seg_630" s="T29">np:L</ta>
            <ta e="T31" id="Seg_631" s="T30">np:Th</ta>
            <ta e="T32" id="Seg_632" s="T31">0.1.h:A </ta>
            <ta e="T33" id="Seg_633" s="T32">adv:Time</ta>
            <ta e="T34" id="Seg_634" s="T33">pro.h:Th</ta>
            <ta e="T40" id="Seg_635" s="T37">pp:Time</ta>
            <ta e="T41" id="Seg_636" s="T40">pro.h:A</ta>
            <ta e="T45" id="Seg_637" s="T42">pp:Time</ta>
            <ta e="T47" id="Seg_638" s="T46">0.1.h:Th</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T28" id="Seg_639" s="T27">RUS:cult</ta>
            <ta e="T35" id="Seg_640" s="T34">RUS:cult</ta>
            <ta e="T36" id="Seg_641" s="T35">RUS:cult</ta>
            <ta e="T46" id="Seg_642" s="T45">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T28" id="Seg_643" s="T27">dir:infl</ta>
            <ta e="T35" id="Seg_644" s="T34">dir:infl</ta>
            <ta e="T36" id="Seg_645" s="T35">dir:bare</ta>
            <ta e="T46" id="Seg_646" s="T45">parad:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_647" s="T1">I was born in Laskovo.</ta>
            <ta e="T8" id="Seg_648" s="T4">I grew up here.</ta>
            <ta e="T12" id="Seg_649" s="T8">My father brought me here.</ta>
            <ta e="T15" id="Seg_650" s="T12">He worked here.</ta>
            <ta e="T18" id="Seg_651" s="T15">We lived here.</ta>
            <ta e="T24" id="Seg_652" s="T18">This is where I grew up and started working here.</ta>
            <ta e="T26" id="Seg_653" s="T24">I sawed wood.</ta>
            <ta e="T32" id="Seg_654" s="T26">Then, outside the chief's house, I swept away the trash.</ta>
            <ta e="T37" id="Seg_655" s="T32">Then I worked as a stoker on a stemship.</ta>
            <ta e="T42" id="Seg_656" s="T37">I worked three years.</ta>
            <ta e="T47" id="Seg_657" s="T42">The fourth year I was a seaman.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_658" s="T1">Ich bin in Laskovo geboren.</ta>
            <ta e="T8" id="Seg_659" s="T4">Ich bin hier aufgewachsen.</ta>
            <ta e="T12" id="Seg_660" s="T8">Mein Vater brachte mich hierher.</ta>
            <ta e="T15" id="Seg_661" s="T12">Er arbeitete hier.</ta>
            <ta e="T18" id="Seg_662" s="T15">Wir lebten hier.</ta>
            <ta e="T24" id="Seg_663" s="T18">Hier bin ich aufgewachsen und habe hier angefangen zu arbeiten.</ta>
            <ta e="T26" id="Seg_664" s="T24">Ich habe Holz gesägt.</ta>
            <ta e="T32" id="Seg_665" s="T26">Dann, vor dem Haus des Direktors, fegte ich den Müll weg.</ta>
            <ta e="T37" id="Seg_666" s="T32">Dann arbeitete ich als Heizer auf einem Boot.</ta>
            <ta e="T42" id="Seg_667" s="T37">Ich arbeitete drei Jahre.</ta>
            <ta e="T47" id="Seg_668" s="T42">Während des vierten Jahres war ich Matrose.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_669" s="T1">Я родился в Ласково.</ta>
            <ta e="T8" id="Seg_670" s="T4">Потом вырос от этого места(?).</ta>
            <ta e="T12" id="Seg_671" s="T8">Отец меня сюда привез.</ta>
            <ta e="T15" id="Seg_672" s="T12">Здесь он работал.</ta>
            <ta e="T18" id="Seg_673" s="T15">Здесь мы жили.</ta>
            <ta e="T24" id="Seg_674" s="T18">Здесь я вырос и здесь работать начал.</ta>
            <ta e="T26" id="Seg_675" s="T24">Дрова пилил.</ta>
            <ta e="T32" id="Seg_676" s="T26">Потом перед домом начальника мусор подметал.</ta>
            <ta e="T37" id="Seg_677" s="T32">Потом я стал кочегаром на параходе. </ta>
            <ta e="T42" id="Seg_678" s="T37">Три года я ходил.</ta>
            <ta e="T47" id="Seg_679" s="T42">В течение четырех лет я был матросом.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_680" s="T1">я родился в Ласково</ta>
            <ta e="T8" id="Seg_681" s="T4">потом вырос от этого места</ta>
            <ta e="T12" id="Seg_682" s="T8">отец меня сюда привез</ta>
            <ta e="T15" id="Seg_683" s="T12">здесь работали</ta>
            <ta e="T18" id="Seg_684" s="T15">здесь мы жили</ta>
            <ta e="T24" id="Seg_685" s="T18">здесь вырос здесь работать начал</ta>
            <ta e="T26" id="Seg_686" s="T24">дрова пилили</ta>
            <ta e="T32" id="Seg_687" s="T26">потом начальника дом на улице сор подметал</ta>
            <ta e="T37" id="Seg_688" s="T32">печку топить</ta>
            <ta e="T42" id="Seg_689" s="T37">три года я ходил</ta>
            <ta e="T47" id="Seg_690" s="T42">был матросом</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
