<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>BaEF_196X_Fishing_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">BaEF_196X_Fishing_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">60</ud-information>
            <ud-information attribute-name="# HIAT:w">44</ud-information>
            <ud-information attribute-name="# e">44</ud-information>
            <ud-information attribute-name="# HIAT:u">11</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="BaEF">
            <abbreviation>BaEF</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="BaEF"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T45" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Man</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">tʼüan</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">abowne</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_14" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">Me</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">qwassaj</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">qwälɨsku</ts>
                  <nts id="Seg_23" n="HIAT:ip">,</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">tʼörotdə</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_29" n="HIAT:w" s="T8">tudom</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_32" n="HIAT:w" s="T9">qwatku</ts>
                  <nts id="Seg_33" n="HIAT:ip">.</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_36" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">Atdokaze</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_41" n="HIAT:w" s="T11">üːderme</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_44" n="HIAT:w" s="T12">iːɣulʼǯaj</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_47" n="HIAT:w" s="T13">tʼörotdə</ts>
                  <nts id="Seg_48" n="HIAT:ip">.</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_51" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_53" n="HIAT:w" s="T14">Poŋgɨlam</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_56" n="HIAT:w" s="T15">poːsaj</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_59" n="HIAT:w" s="T16">qwälɨm</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_62" n="HIAT:w" s="T17">qwatku</ts>
                  <nts id="Seg_63" n="HIAT:ip">.</nts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_66" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_68" n="HIAT:w" s="T18">Qwälɨm</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_71" n="HIAT:w" s="T19">ass</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_74" n="HIAT:w" s="T20">qwannaj</ts>
                  <nts id="Seg_75" n="HIAT:ip">.</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_78" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_80" n="HIAT:w" s="T21">Sədə</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_83" n="HIAT:w" s="T22">dʼel</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_86" n="HIAT:w" s="T23">paldʼej</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_89" n="HIAT:w" s="T24">tamdʼel</ts>
                  <nts id="Seg_90" n="HIAT:ip">,</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_93" n="HIAT:w" s="T25">ass</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_96" n="HIAT:w" s="T26">qwannaj</ts>
                  <nts id="Seg_97" n="HIAT:ip">.</nts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_100" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_102" n="HIAT:w" s="T27">Šoɣɨrɨm</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_105" n="HIAT:w" s="T28">tʼädəgu</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_108" n="HIAT:w" s="T29">nadə</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_110" n="HIAT:ip">(</nts>
                  <ts e="T31" id="Seg_112" n="HIAT:w" s="T30">pʼötim</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_115" n="HIAT:w" s="T31">tʼädɨgu</ts>
                  <nts id="Seg_116" n="HIAT:ip">)</nts>
                  <nts id="Seg_117" n="HIAT:ip">.</nts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_120" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_122" n="HIAT:w" s="T32">Man</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_125" n="HIAT:w" s="T33">tamdʼel</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_128" n="HIAT:w" s="T34">qwassan</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_131" n="HIAT:w" s="T35">krɨbɨm</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_134" n="HIAT:w" s="T36">taqqɨlgu</ts>
                  <nts id="Seg_135" n="HIAT:ip">.</nts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_138" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_140" n="HIAT:w" s="T37">Krɨbɨm</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_143" n="HIAT:w" s="T38">taqqennau</ts>
                  <nts id="Seg_144" n="HIAT:ip">.</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_147" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_149" n="HIAT:w" s="T39">Čaːǯilʼe</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_152" n="HIAT:w" s="T40">pɨkkɨlʼdʼän</ts>
                  <nts id="Seg_153" n="HIAT:ip">,</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_156" n="HIAT:w" s="T41">qwannau</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_159" n="HIAT:w" s="T42">udou</ts>
                  <nts id="Seg_160" n="HIAT:ip">.</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_163" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_165" n="HIAT:w" s="T43">Dataon</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_168" n="HIAT:w" s="T44">közin</ts>
                  <nts id="Seg_169" n="HIAT:ip">.</nts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T45" id="Seg_171" n="sc" s="T1">
               <ts e="T2" id="Seg_173" n="e" s="T1">Man </ts>
               <ts e="T3" id="Seg_175" n="e" s="T2">tʼüan </ts>
               <ts e="T4" id="Seg_177" n="e" s="T3">abowne. </ts>
               <ts e="T5" id="Seg_179" n="e" s="T4">Me </ts>
               <ts e="T6" id="Seg_181" n="e" s="T5">qwassaj </ts>
               <ts e="T7" id="Seg_183" n="e" s="T6">qwälɨsku, </ts>
               <ts e="T8" id="Seg_185" n="e" s="T7">tʼörotdə </ts>
               <ts e="T9" id="Seg_187" n="e" s="T8">tudom </ts>
               <ts e="T10" id="Seg_189" n="e" s="T9">qwatku. </ts>
               <ts e="T11" id="Seg_191" n="e" s="T10">Atdokaze </ts>
               <ts e="T12" id="Seg_193" n="e" s="T11">üːderme </ts>
               <ts e="T13" id="Seg_195" n="e" s="T12">iːɣulʼǯaj </ts>
               <ts e="T14" id="Seg_197" n="e" s="T13">tʼörotdə. </ts>
               <ts e="T15" id="Seg_199" n="e" s="T14">Poŋgɨlam </ts>
               <ts e="T16" id="Seg_201" n="e" s="T15">poːsaj </ts>
               <ts e="T17" id="Seg_203" n="e" s="T16">qwälɨm </ts>
               <ts e="T18" id="Seg_205" n="e" s="T17">qwatku. </ts>
               <ts e="T19" id="Seg_207" n="e" s="T18">Qwälɨm </ts>
               <ts e="T20" id="Seg_209" n="e" s="T19">ass </ts>
               <ts e="T21" id="Seg_211" n="e" s="T20">qwannaj. </ts>
               <ts e="T22" id="Seg_213" n="e" s="T21">Sədə </ts>
               <ts e="T23" id="Seg_215" n="e" s="T22">dʼel </ts>
               <ts e="T24" id="Seg_217" n="e" s="T23">paldʼej </ts>
               <ts e="T25" id="Seg_219" n="e" s="T24">tamdʼel, </ts>
               <ts e="T26" id="Seg_221" n="e" s="T25">ass </ts>
               <ts e="T27" id="Seg_223" n="e" s="T26">qwannaj. </ts>
               <ts e="T28" id="Seg_225" n="e" s="T27">Šoɣɨrɨm </ts>
               <ts e="T29" id="Seg_227" n="e" s="T28">tʼädəgu </ts>
               <ts e="T30" id="Seg_229" n="e" s="T29">nadə </ts>
               <ts e="T31" id="Seg_231" n="e" s="T30">(pʼötim </ts>
               <ts e="T32" id="Seg_233" n="e" s="T31">tʼädɨgu). </ts>
               <ts e="T33" id="Seg_235" n="e" s="T32">Man </ts>
               <ts e="T34" id="Seg_237" n="e" s="T33">tamdʼel </ts>
               <ts e="T35" id="Seg_239" n="e" s="T34">qwassan </ts>
               <ts e="T36" id="Seg_241" n="e" s="T35">krɨbɨm </ts>
               <ts e="T37" id="Seg_243" n="e" s="T36">taqqɨlgu. </ts>
               <ts e="T38" id="Seg_245" n="e" s="T37">Krɨbɨm </ts>
               <ts e="T39" id="Seg_247" n="e" s="T38">taqqennau. </ts>
               <ts e="T40" id="Seg_249" n="e" s="T39">Čaːǯilʼe </ts>
               <ts e="T41" id="Seg_251" n="e" s="T40">pɨkkɨlʼdʼän, </ts>
               <ts e="T42" id="Seg_253" n="e" s="T41">qwannau </ts>
               <ts e="T43" id="Seg_255" n="e" s="T42">udou. </ts>
               <ts e="T44" id="Seg_257" n="e" s="T43">Dataon </ts>
               <ts e="T45" id="Seg_259" n="e" s="T44">közin. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_260" s="T1">BaEF_196X_Fishing_nar.001 (001.001)</ta>
            <ta e="T10" id="Seg_261" s="T4">BaEF_196X_Fishing_nar.002 (001.002)</ta>
            <ta e="T14" id="Seg_262" s="T10">BaEF_196X_Fishing_nar.003 (001.003)</ta>
            <ta e="T18" id="Seg_263" s="T14">BaEF_196X_Fishing_nar.004 (001.004)</ta>
            <ta e="T21" id="Seg_264" s="T18">BaEF_196X_Fishing_nar.005 (001.005)</ta>
            <ta e="T27" id="Seg_265" s="T21">BaEF_196X_Fishing_nar.006 (001.006)</ta>
            <ta e="T32" id="Seg_266" s="T27">BaEF_196X_Fishing_nar.007 (001.007)</ta>
            <ta e="T37" id="Seg_267" s="T32">BaEF_196X_Fishing_nar.008 (001.008)</ta>
            <ta e="T39" id="Seg_268" s="T37">BaEF_196X_Fishing_nar.009 (001.009)</ta>
            <ta e="T43" id="Seg_269" s="T39">BaEF_196X_Fishing_nar.010 (001.010)</ta>
            <ta e="T45" id="Seg_270" s="T43">BaEF_196X_Fishing_nar.011 (001.011)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_271" s="T1">ман ′тʼӱан а′бов(w)не.</ta>
            <ta e="T10" id="Seg_272" s="T4">ме kwа′ссай ′kwӓлыску, ′тʼӧротдъ ту′дом kwат′ку.</ta>
            <ta e="T14" id="Seg_273" s="T10">ат′доказе ′ӱ̄дерме ′ӣɣулʼджай ′тʼӧротдъ.</ta>
            <ta e="T18" id="Seg_274" s="T14">′поңг(к)ылам ′по̄сай ′kwӓлым kwат′ку.</ta>
            <ta e="T21" id="Seg_275" s="T18">′kwӓлым асс kwа′ннай.</ta>
            <ta e="T27" id="Seg_276" s="T21">съдъ′дʼел пал′дʼей там′дʼел, асс kwа′ннай.</ta>
            <ta e="T32" id="Seg_277" s="T27">′шоɣырым ′тʼӓдъгу надъ (′пʼӧтим ′тʼӓдыгу).</ta>
            <ta e="T37" id="Seg_278" s="T32">ман там′дʼел kwа′ссан кры′бым таkkыл′гу.</ta>
            <ta e="T39" id="Seg_279" s="T37">кры′бым ‵таkkе′ннау̹.</ta>
            <ta e="T43" id="Seg_280" s="T39">′тша̄джилʼе ‵пыккылʼ′дʼӓн, kwа′ннау̨ у′доу̹.</ta>
            <ta e="T45" id="Seg_281" s="T43">да ′таон кӧ′зин.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_282" s="T1">man tʼüan abow(w)ne.</ta>
            <ta e="T10" id="Seg_283" s="T4">me qwassaj qwälɨsku, tʼörotdə tudom qwatku.</ta>
            <ta e="T14" id="Seg_284" s="T10">atdokaze üːderme iːɣulʼǯaj tʼörotdə.</ta>
            <ta e="T18" id="Seg_285" s="T14">poŋg(k)ɨlam poːsaj qwälɨm qwatku.</ta>
            <ta e="T21" id="Seg_286" s="T18">qwälɨm ass qwannaj.</ta>
            <ta e="T27" id="Seg_287" s="T21">sədədʼel paldʼej tamdʼel, ass qwannaj.</ta>
            <ta e="T32" id="Seg_288" s="T27">šoɣɨrɨm tʼädəgu nadə (pʼötim tʼädɨgu).</ta>
            <ta e="T37" id="Seg_289" s="T32">man tamdʼel qwassan krɨbɨm taqqɨlgu.</ta>
            <ta e="T39" id="Seg_290" s="T37">krɨbɨm taqqennau̹.</ta>
            <ta e="T43" id="Seg_291" s="T39">čaːǯilʼe pɨkkɨlʼdʼän, qwannau udou̹.</ta>
            <ta e="T45" id="Seg_292" s="T43">da taon közin.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_293" s="T1">Man tʼüan abowne. </ta>
            <ta e="T10" id="Seg_294" s="T4">Me qwassaj qwälɨsku, tʼörotdə tudom qwatku. </ta>
            <ta e="T14" id="Seg_295" s="T10">Atdokaze üːderme iːɣulʼǯaj tʼörotdə. </ta>
            <ta e="T18" id="Seg_296" s="T14">Poŋgɨlam poːsaj qwälɨm qwatku. </ta>
            <ta e="T21" id="Seg_297" s="T18">Qwälɨm ass qwannaj. </ta>
            <ta e="T27" id="Seg_298" s="T21">Sədə dʼel paldʼej tamdʼel, ass qwannaj. </ta>
            <ta e="T32" id="Seg_299" s="T27">Šoɣɨrɨm tʼädəgu nadə (pʼötim tʼädɨgu). </ta>
            <ta e="T37" id="Seg_300" s="T32">Man tamdʼel qwassan krɨbɨm taqqɨlgu. </ta>
            <ta e="T39" id="Seg_301" s="T37">Krɨbɨm taqqennau. </ta>
            <ta e="T43" id="Seg_302" s="T39">Čaːǯilʼe pɨkkɨlʼdʼän, qwannau udou. </ta>
            <ta e="T45" id="Seg_303" s="T43">Dataon közin. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_304" s="T1">man</ta>
            <ta e="T3" id="Seg_305" s="T2">tʼü-a-n</ta>
            <ta e="T4" id="Seg_306" s="T3">abo-w-ne</ta>
            <ta e="T5" id="Seg_307" s="T4">me</ta>
            <ta e="T6" id="Seg_308" s="T5">qwas-sa-j</ta>
            <ta e="T7" id="Seg_309" s="T6">qwälɨ-s-ku</ta>
            <ta e="T8" id="Seg_310" s="T7">tʼör-o-tdə</ta>
            <ta e="T9" id="Seg_311" s="T8">tudo-m</ta>
            <ta e="T10" id="Seg_312" s="T9">qwat-ku</ta>
            <ta e="T11" id="Seg_313" s="T10">atdo-ka-ze</ta>
            <ta e="T12" id="Seg_314" s="T11">üːder-me</ta>
            <ta e="T13" id="Seg_315" s="T12">iːɣulʼ-ǯa-j</ta>
            <ta e="T14" id="Seg_316" s="T13">tʼör-o-tdə</ta>
            <ta e="T15" id="Seg_317" s="T14">poŋgɨ-la-m</ta>
            <ta e="T16" id="Seg_318" s="T15">poː-sa-j</ta>
            <ta e="T17" id="Seg_319" s="T16">qwäl-ɨ-m</ta>
            <ta e="T18" id="Seg_320" s="T17">qwat-ku</ta>
            <ta e="T19" id="Seg_321" s="T18">qwäl-ɨ-m</ta>
            <ta e="T20" id="Seg_322" s="T19">ass</ta>
            <ta e="T21" id="Seg_323" s="T20">qwan-na-j</ta>
            <ta e="T22" id="Seg_324" s="T21">sədə</ta>
            <ta e="T23" id="Seg_325" s="T22">dʼel</ta>
            <ta e="T24" id="Seg_326" s="T23">paldʼe-j</ta>
            <ta e="T25" id="Seg_327" s="T24">tam-dʼel</ta>
            <ta e="T26" id="Seg_328" s="T25">ass</ta>
            <ta e="T27" id="Seg_329" s="T26">qwan-na-j</ta>
            <ta e="T28" id="Seg_330" s="T27">šoɣɨr-ɨ-m</ta>
            <ta e="T29" id="Seg_331" s="T28">tʼädə-gu</ta>
            <ta e="T30" id="Seg_332" s="T29">nadə</ta>
            <ta e="T31" id="Seg_333" s="T30">pʼöti-m</ta>
            <ta e="T32" id="Seg_334" s="T31">tʼädɨ-gu</ta>
            <ta e="T33" id="Seg_335" s="T32">man</ta>
            <ta e="T34" id="Seg_336" s="T33">tam-dʼel</ta>
            <ta e="T35" id="Seg_337" s="T34">qwas-sa-n</ta>
            <ta e="T36" id="Seg_338" s="T35">krɨb-ɨ-m</ta>
            <ta e="T37" id="Seg_339" s="T36">taqqɨl-gu</ta>
            <ta e="T38" id="Seg_340" s="T37">krɨb-ɨ-m</ta>
            <ta e="T39" id="Seg_341" s="T38">taqqen-na-u</ta>
            <ta e="T40" id="Seg_342" s="T39">čaːǯi-lʼe</ta>
            <ta e="T41" id="Seg_343" s="T40">pɨkkɨlʼ-dʼä-n</ta>
            <ta e="T42" id="Seg_344" s="T41">qwan-na-u</ta>
            <ta e="T43" id="Seg_345" s="T42">ud-o-u</ta>
            <ta e="T44" id="Seg_346" s="T43">datao-n</ta>
            <ta e="T45" id="Seg_347" s="T44">közi-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_348" s="T1">man</ta>
            <ta e="T3" id="Seg_349" s="T2">tüː-ɨ-ŋ</ta>
            <ta e="T4" id="Seg_350" s="T3">aba-w-nä</ta>
            <ta e="T5" id="Seg_351" s="T4">me</ta>
            <ta e="T6" id="Seg_352" s="T5">qwan-sɨ-j</ta>
            <ta e="T7" id="Seg_353" s="T6">qwɛl-s-gu</ta>
            <ta e="T8" id="Seg_354" s="T7">tʼör-ɨ-ntə</ta>
            <ta e="T9" id="Seg_355" s="T8">tudo-m</ta>
            <ta e="T10" id="Seg_356" s="T9">qwat-gu</ta>
            <ta e="T11" id="Seg_357" s="T10">andǝ-ka-se</ta>
            <ta e="T12" id="Seg_358" s="T11">üdər-me</ta>
            <ta e="T13" id="Seg_359" s="T12">ügul-lǯɨ-j</ta>
            <ta e="T14" id="Seg_360" s="T13">tʼör-ɨ-ntə</ta>
            <ta e="T15" id="Seg_361" s="T14">poqqɨ-la-m</ta>
            <ta e="T16" id="Seg_362" s="T15">pot-sɨ-j</ta>
            <ta e="T17" id="Seg_363" s="T16">qwɛl-ɨ-m</ta>
            <ta e="T18" id="Seg_364" s="T17">qwat-gu</ta>
            <ta e="T19" id="Seg_365" s="T18">qwɛl-ɨ-m</ta>
            <ta e="T20" id="Seg_366" s="T19">asa</ta>
            <ta e="T21" id="Seg_367" s="T20">qwat-nɨ-j</ta>
            <ta e="T22" id="Seg_368" s="T21">sədə</ta>
            <ta e="T23" id="Seg_369" s="T22">dʼel</ta>
            <ta e="T24" id="Seg_370" s="T23">palʼdʼi-j</ta>
            <ta e="T25" id="Seg_371" s="T24">taw-dʼel</ta>
            <ta e="T26" id="Seg_372" s="T25">asa</ta>
            <ta e="T27" id="Seg_373" s="T26">qwat-nɨ-j</ta>
            <ta e="T28" id="Seg_374" s="T27">šoɣor-ɨ-m</ta>
            <ta e="T29" id="Seg_375" s="T28">čʼadɨ-gu</ta>
            <ta e="T30" id="Seg_376" s="T29">nadə</ta>
            <ta e="T31" id="Seg_377" s="T30">pötte-m</ta>
            <ta e="T32" id="Seg_378" s="T31">čʼadɨ-gu</ta>
            <ta e="T33" id="Seg_379" s="T32">man</ta>
            <ta e="T34" id="Seg_380" s="T33">taw-dʼel</ta>
            <ta e="T35" id="Seg_381" s="T34">qwan-sɨ-ŋ</ta>
            <ta e="T36" id="Seg_382" s="T35">krɨb-ɨ-m</ta>
            <ta e="T37" id="Seg_383" s="T36">taqqɨl-gu</ta>
            <ta e="T38" id="Seg_384" s="T37">krɨb-ɨ-m</ta>
            <ta e="T39" id="Seg_385" s="T38">taqqɨl-nɨ-w</ta>
            <ta e="T40" id="Seg_386" s="T39">čaǯɨ-le</ta>
            <ta e="T41" id="Seg_387" s="T40">pɨŋgəl-dʼi-n</ta>
            <ta e="T42" id="Seg_388" s="T41">qwat-nɨ-w</ta>
            <ta e="T43" id="Seg_389" s="T42">ut-ɨ-w</ta>
            <ta e="T44" id="Seg_390" s="T43">tatawa-n</ta>
            <ta e="T45" id="Seg_391" s="T44">qüzu-n</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_392" s="T1">I.NOM</ta>
            <ta e="T3" id="Seg_393" s="T2">come-EP-1SG.S</ta>
            <ta e="T4" id="Seg_394" s="T3">older.sister-1SG-ALL</ta>
            <ta e="T5" id="Seg_395" s="T4">we.[NOM]</ta>
            <ta e="T6" id="Seg_396" s="T5">leave-PST-1DU</ta>
            <ta e="T7" id="Seg_397" s="T6">fish-CAP-INF</ta>
            <ta e="T8" id="Seg_398" s="T7">lake-EP-ILL</ta>
            <ta e="T9" id="Seg_399" s="T8">crucian-ACC</ta>
            <ta e="T10" id="Seg_400" s="T9">manage.to.get-INF</ta>
            <ta e="T11" id="Seg_401" s="T10">boat-DIM-INSTR</ta>
            <ta e="T12" id="Seg_402" s="T11">move-%%</ta>
            <ta e="T13" id="Seg_403" s="T12">drag-TR-1DU</ta>
            <ta e="T14" id="Seg_404" s="T13">lake-EP-ILL</ta>
            <ta e="T15" id="Seg_405" s="T14">net-PL-ACC</ta>
            <ta e="T16" id="Seg_406" s="T15">settle.net-PST-1DU</ta>
            <ta e="T17" id="Seg_407" s="T16">fish-EP-ACC</ta>
            <ta e="T18" id="Seg_408" s="T17">manage.to.get-INF</ta>
            <ta e="T19" id="Seg_409" s="T18">fish-EP-ACC</ta>
            <ta e="T20" id="Seg_410" s="T19">NEG</ta>
            <ta e="T21" id="Seg_411" s="T20">manage.to.get-CO-1DU</ta>
            <ta e="T22" id="Seg_412" s="T21">two</ta>
            <ta e="T23" id="Seg_413" s="T22">day.[NOM]</ta>
            <ta e="T24" id="Seg_414" s="T23">walk-1DU</ta>
            <ta e="T25" id="Seg_415" s="T24">this-day.[NOM]</ta>
            <ta e="T26" id="Seg_416" s="T25">NEG</ta>
            <ta e="T27" id="Seg_417" s="T26">manage.to.get-CO-1DU</ta>
            <ta e="T28" id="Seg_418" s="T27">stove-EP-ACC</ta>
            <ta e="T29" id="Seg_419" s="T28">make.fire-INF</ta>
            <ta e="T30" id="Seg_420" s="T29">one.should</ta>
            <ta e="T31" id="Seg_421" s="T30">stove-ACC</ta>
            <ta e="T32" id="Seg_422" s="T31">burn.down-INF</ta>
            <ta e="T33" id="Seg_423" s="T32">I.NOM</ta>
            <ta e="T34" id="Seg_424" s="T33">this-day.[NOM]</ta>
            <ta e="T35" id="Seg_425" s="T34">leave-PST-1SG.S</ta>
            <ta e="T36" id="Seg_426" s="T35">mushroom-EP-ACC</ta>
            <ta e="T37" id="Seg_427" s="T36">gather-INF</ta>
            <ta e="T38" id="Seg_428" s="T37">mushroom-EP-ACC</ta>
            <ta e="T39" id="Seg_429" s="T38">gather-CO-1SG.O</ta>
            <ta e="T40" id="Seg_430" s="T39">go-CVB</ta>
            <ta e="T41" id="Seg_431" s="T40">fall.down-RFL-3SG.S</ta>
            <ta e="T42" id="Seg_432" s="T41">beat-CO-1SG.O</ta>
            <ta e="T43" id="Seg_433" s="T42">hand.[NOM]-EP-1SG</ta>
            <ta e="T44" id="Seg_434" s="T43">to.such.extent-ADV.LOC</ta>
            <ta e="T45" id="Seg_435" s="T44">ache-3SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_436" s="T1">я.NOM</ta>
            <ta e="T3" id="Seg_437" s="T2">приехать-EP-1SG.S</ta>
            <ta e="T4" id="Seg_438" s="T3">старшая.сестра-1SG-ALL</ta>
            <ta e="T5" id="Seg_439" s="T4">мы.[NOM]</ta>
            <ta e="T6" id="Seg_440" s="T5">отправиться-PST-1DU</ta>
            <ta e="T7" id="Seg_441" s="T6">рыба-CAP-INF</ta>
            <ta e="T8" id="Seg_442" s="T7">озеро-EP-ILL</ta>
            <ta e="T9" id="Seg_443" s="T8">карась-ACC</ta>
            <ta e="T10" id="Seg_444" s="T9">добыть-INF</ta>
            <ta e="T11" id="Seg_445" s="T10">обласок-DIM-INSTR</ta>
            <ta e="T12" id="Seg_446" s="T11">переселить-%%</ta>
            <ta e="T13" id="Seg_447" s="T12">притащить.волоком-TR-1DU</ta>
            <ta e="T14" id="Seg_448" s="T13">озеро-EP-ILL</ta>
            <ta e="T15" id="Seg_449" s="T14">сеть-PL-ACC</ta>
            <ta e="T16" id="Seg_450" s="T15">поставить.сеть-PST-1DU</ta>
            <ta e="T17" id="Seg_451" s="T16">рыба-EP-ACC</ta>
            <ta e="T18" id="Seg_452" s="T17">добыть-INF</ta>
            <ta e="T19" id="Seg_453" s="T18">рыба-EP-ACC</ta>
            <ta e="T20" id="Seg_454" s="T19">NEG</ta>
            <ta e="T21" id="Seg_455" s="T20">добыть-CO-1DU</ta>
            <ta e="T22" id="Seg_456" s="T21">два</ta>
            <ta e="T23" id="Seg_457" s="T22">день.[NOM]</ta>
            <ta e="T24" id="Seg_458" s="T23">ходить-1DU</ta>
            <ta e="T25" id="Seg_459" s="T24">этот-день.[NOM]</ta>
            <ta e="T26" id="Seg_460" s="T25">NEG</ta>
            <ta e="T27" id="Seg_461" s="T26">добыть-CO-1DU</ta>
            <ta e="T28" id="Seg_462" s="T27">печь-EP-ACC</ta>
            <ta e="T29" id="Seg_463" s="T28">растопить-INF</ta>
            <ta e="T30" id="Seg_464" s="T29">надо</ta>
            <ta e="T31" id="Seg_465" s="T30">печь-ACC</ta>
            <ta e="T32" id="Seg_466" s="T31">сжечь-INF</ta>
            <ta e="T33" id="Seg_467" s="T32">я.NOM</ta>
            <ta e="T34" id="Seg_468" s="T33">этот-день.[NOM]</ta>
            <ta e="T35" id="Seg_469" s="T34">отправиться-PST-1SG.S</ta>
            <ta e="T36" id="Seg_470" s="T35">гриб-EP-ACC</ta>
            <ta e="T37" id="Seg_471" s="T36">собрать-INF</ta>
            <ta e="T38" id="Seg_472" s="T37">гриб-EP-ACC</ta>
            <ta e="T39" id="Seg_473" s="T38">собрать-CO-1SG.O</ta>
            <ta e="T40" id="Seg_474" s="T39">ходить-CVB</ta>
            <ta e="T41" id="Seg_475" s="T40">упасть-RFL-3SG.S</ta>
            <ta e="T42" id="Seg_476" s="T41">побить-CO-1SG.O</ta>
            <ta e="T43" id="Seg_477" s="T42">рука.[NOM]-EP-1SG</ta>
            <ta e="T44" id="Seg_478" s="T43">до.того-ADV.LOC</ta>
            <ta e="T45" id="Seg_479" s="T44">болеть-3SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_480" s="T1">pers</ta>
            <ta e="T3" id="Seg_481" s="T2">v-n:ins-v:pn</ta>
            <ta e="T4" id="Seg_482" s="T3">n-n:poss-n:case</ta>
            <ta e="T5" id="Seg_483" s="T4">pers.[n:case]</ta>
            <ta e="T6" id="Seg_484" s="T5">v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_485" s="T6">n-n&gt;v-v:inf</ta>
            <ta e="T8" id="Seg_486" s="T7">n-n:ins-n:case</ta>
            <ta e="T9" id="Seg_487" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_488" s="T9">v-v:inf</ta>
            <ta e="T11" id="Seg_489" s="T10">n-n&gt;n-n:case</ta>
            <ta e="T12" id="Seg_490" s="T11">v-%%</ta>
            <ta e="T13" id="Seg_491" s="T12">v-v&gt;v-v:pn</ta>
            <ta e="T14" id="Seg_492" s="T13">n-n:ins-n:case</ta>
            <ta e="T15" id="Seg_493" s="T14">n-n:num-n:case</ta>
            <ta e="T16" id="Seg_494" s="T15">v-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_495" s="T16">n-n:ins-n:case</ta>
            <ta e="T18" id="Seg_496" s="T17">v-v:inf</ta>
            <ta e="T19" id="Seg_497" s="T18">n-n:ins-n:case</ta>
            <ta e="T20" id="Seg_498" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_499" s="T20">v-v:ins-v:pn</ta>
            <ta e="T22" id="Seg_500" s="T21">num</ta>
            <ta e="T23" id="Seg_501" s="T22">n.[n:case]</ta>
            <ta e="T24" id="Seg_502" s="T23">v-v:pn</ta>
            <ta e="T25" id="Seg_503" s="T24">dem-n.[n:case]</ta>
            <ta e="T26" id="Seg_504" s="T25">ptcl</ta>
            <ta e="T27" id="Seg_505" s="T26">v-v:ins-v:pn</ta>
            <ta e="T28" id="Seg_506" s="T27">n-n:ins-n:case</ta>
            <ta e="T29" id="Seg_507" s="T28">v-v:inf</ta>
            <ta e="T30" id="Seg_508" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_509" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_510" s="T31">v-v:inf</ta>
            <ta e="T33" id="Seg_511" s="T32">pers</ta>
            <ta e="T34" id="Seg_512" s="T33">dem-n.[n:case]</ta>
            <ta e="T35" id="Seg_513" s="T34">v-v:tense-v:pn</ta>
            <ta e="T36" id="Seg_514" s="T35">n-n:ins-n:case</ta>
            <ta e="T37" id="Seg_515" s="T36">v-v:inf</ta>
            <ta e="T38" id="Seg_516" s="T37">n-n:ins-n:case</ta>
            <ta e="T39" id="Seg_517" s="T38">v-v:ins-v:pn</ta>
            <ta e="T40" id="Seg_518" s="T39">v-v&gt;adv</ta>
            <ta e="T41" id="Seg_519" s="T40">v-v&gt;v-v:pn</ta>
            <ta e="T42" id="Seg_520" s="T41">v-v:ins-v:pn</ta>
            <ta e="T43" id="Seg_521" s="T42">n.[n:case]-n:ins-n:poss</ta>
            <ta e="T44" id="Seg_522" s="T43">adv-adv:case</ta>
            <ta e="T45" id="Seg_523" s="T44">v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_524" s="T1">pers</ta>
            <ta e="T3" id="Seg_525" s="T2">v</ta>
            <ta e="T4" id="Seg_526" s="T3">n</ta>
            <ta e="T5" id="Seg_527" s="T4">pers</ta>
            <ta e="T6" id="Seg_528" s="T5">v</ta>
            <ta e="T7" id="Seg_529" s="T6">v</ta>
            <ta e="T8" id="Seg_530" s="T7">n</ta>
            <ta e="T9" id="Seg_531" s="T8">n</ta>
            <ta e="T10" id="Seg_532" s="T9">v</ta>
            <ta e="T11" id="Seg_533" s="T10">n</ta>
            <ta e="T12" id="Seg_534" s="T11">v</ta>
            <ta e="T13" id="Seg_535" s="T12">v</ta>
            <ta e="T14" id="Seg_536" s="T13">n</ta>
            <ta e="T15" id="Seg_537" s="T14">n</ta>
            <ta e="T16" id="Seg_538" s="T15">v</ta>
            <ta e="T17" id="Seg_539" s="T16">n</ta>
            <ta e="T18" id="Seg_540" s="T17">v</ta>
            <ta e="T19" id="Seg_541" s="T18">n</ta>
            <ta e="T20" id="Seg_542" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_543" s="T20">v</ta>
            <ta e="T22" id="Seg_544" s="T21">num</ta>
            <ta e="T23" id="Seg_545" s="T22">n</ta>
            <ta e="T24" id="Seg_546" s="T23">v</ta>
            <ta e="T25" id="Seg_547" s="T24">adv</ta>
            <ta e="T26" id="Seg_548" s="T25">ptcl</ta>
            <ta e="T27" id="Seg_549" s="T26">v</ta>
            <ta e="T28" id="Seg_550" s="T27">n</ta>
            <ta e="T29" id="Seg_551" s="T28">v</ta>
            <ta e="T30" id="Seg_552" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_553" s="T30">n</ta>
            <ta e="T32" id="Seg_554" s="T31">v</ta>
            <ta e="T33" id="Seg_555" s="T32">pers</ta>
            <ta e="T34" id="Seg_556" s="T33">adv</ta>
            <ta e="T35" id="Seg_557" s="T34">n</ta>
            <ta e="T36" id="Seg_558" s="T35">n</ta>
            <ta e="T37" id="Seg_559" s="T36">v</ta>
            <ta e="T38" id="Seg_560" s="T37">n</ta>
            <ta e="T39" id="Seg_561" s="T38">v</ta>
            <ta e="T40" id="Seg_562" s="T39">adv</ta>
            <ta e="T41" id="Seg_563" s="T40">v</ta>
            <ta e="T42" id="Seg_564" s="T41">v</ta>
            <ta e="T43" id="Seg_565" s="T42">n</ta>
            <ta e="T44" id="Seg_566" s="T43">adv</ta>
            <ta e="T45" id="Seg_567" s="T44">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T30" id="Seg_568" s="T29">RUS:mod</ta>
            <ta e="T36" id="Seg_569" s="T35">RUS:core</ta>
            <ta e="T38" id="Seg_570" s="T37">RUS:core</ta>
            <ta e="T44" id="Seg_571" s="T43">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_572" s="T1">I came to my elder sister.</ta>
            <ta e="T10" id="Seg_573" s="T4">We went fishing to a lake to fish crucians.</ta>
            <ta e="T14" id="Seg_574" s="T10">We dragged a boat to the lake.</ta>
            <ta e="T18" id="Seg_575" s="T14">We put nets to catch fish.</ta>
            <ta e="T21" id="Seg_576" s="T18">We didn't catch any fish.</ta>
            <ta e="T27" id="Seg_577" s="T21">We were going [there] two days long, today we didn't catch anything.</ta>
            <ta e="T32" id="Seg_578" s="T27">The stove has to be heated.</ta>
            <ta e="T37" id="Seg_579" s="T32">Today I went to pick mushrooms.</ta>
            <ta e="T39" id="Seg_580" s="T37">I gathered mushrooms.</ta>
            <ta e="T43" id="Seg_581" s="T39">As I was walking I fell down and hurt my arm.</ta>
            <ta e="T45" id="Seg_582" s="T43">It hurts so much.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_583" s="T1">Ich kam zu meiner großen Schwester.</ta>
            <ta e="T10" id="Seg_584" s="T4">Wir gingen zu einem See fischen um Karauschen zu fangen.</ta>
            <ta e="T14" id="Seg_585" s="T10">Wir zogen ein Boot zum See.</ta>
            <ta e="T18" id="Seg_586" s="T14">Wir warfen Netze aus, um Fische zu fangen.</ta>
            <ta e="T21" id="Seg_587" s="T18">Wir fingen keine Fische.</ta>
            <ta e="T27" id="Seg_588" s="T21">Wir gingen zwei Tage lang [dorthin], heute haben wir nichts gefangen.</ta>
            <ta e="T32" id="Seg_589" s="T27">Der Ofen muss geheizt werden.</ta>
            <ta e="T37" id="Seg_590" s="T32">Heute ging ich, um Pilze zu sammeln.</ta>
            <ta e="T39" id="Seg_591" s="T37">Ich sammelte Pilze.</ta>
            <ta e="T43" id="Seg_592" s="T39">Während ich ging, stürzte ich und verletzte mich am Arm.</ta>
            <ta e="T45" id="Seg_593" s="T43">Es tut so weh.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_594" s="T1">Я приехала к старшей сестре.</ta>
            <ta e="T10" id="Seg_595" s="T4">Мы ходили рыбачить, на озеро карася добывать.</ta>
            <ta e="T14" id="Seg_596" s="T10">На обласке мы волок тащили к озеру.</ta>
            <ta e="T18" id="Seg_597" s="T14">Сетки поставили рыбу добывать.</ta>
            <ta e="T21" id="Seg_598" s="T18">Рыбы не добыли.</ta>
            <ta e="T27" id="Seg_599" s="T21">Два дня ходили, сегодня не добыли</ta>
            <ta e="T32" id="Seg_600" s="T27">Печку затопить надо (печь затопить).</ta>
            <ta e="T37" id="Seg_601" s="T32">Я сегодня ходила грибы собирать.</ta>
            <ta e="T39" id="Seg_602" s="T37">Грибов набрала.</ta>
            <ta e="T43" id="Seg_603" s="T39">Шла, упала, ушибла руку.</ta>
            <ta e="T45" id="Seg_604" s="T43">До того болит.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_605" s="T1">я приехала к старшей сестре</ta>
            <ta e="T10" id="Seg_606" s="T4">мы ходили рыбачить на озере карася добывать</ta>
            <ta e="T14" id="Seg_607" s="T10">на обласке мы волок тащили к озеру</ta>
            <ta e="T18" id="Seg_608" s="T14">сетки поставили рыбу добывать</ta>
            <ta e="T21" id="Seg_609" s="T18">рыбы не добыли</ta>
            <ta e="T27" id="Seg_610" s="T21">два дня ходили сегодня не добыли</ta>
            <ta e="T32" id="Seg_611" s="T27">печку затопить надо (на низу) печь затопить</ta>
            <ta e="T37" id="Seg_612" s="T32">я сегодня ходила грибы собирать</ta>
            <ta e="T39" id="Seg_613" s="T37">грибов набрала</ta>
            <ta e="T43" id="Seg_614" s="T39">шла упала ушибла руку</ta>
            <ta e="T45" id="Seg_615" s="T43">болит</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T18" id="Seg_616" s="T14">[KuAI:] Variant: 'poŋkɨlam'.</ta>
            <ta e="T27" id="Seg_617" s="T21">[BrM:] 'sədədʼel' changed to 'sədə dʼel'.</ta>
            <ta e="T45" id="Seg_618" s="T43">[BrM:] 'da taon' changed to 'dataon'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
