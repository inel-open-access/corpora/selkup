<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>ChAE_196X_OstyakFood_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">ChAE_196X_OstyakFood_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">70</ud-information>
            <ud-information attribute-name="# HIAT:w">53</ud-information>
            <ud-information attribute-name="# e">53</ud-information>
            <ud-information attribute-name="# HIAT:u">13</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="ChAE">
            <abbreviation>ChAE</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="ChAE"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T54" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Ukon</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">tʼümolɣulannan</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">nʼäj</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">tʼänkumban</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">Nʼäjim</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">az</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">aukumbattə</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_29" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">Kwälaze</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">elakumbattə</ts>
                  <nts id="Seg_35" n="HIAT:ip">,</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">watʼse</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_41" n="HIAT:w" s="T11">elakumbattə</ts>
                  <nts id="Seg_42" n="HIAT:ip">,</nts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_45" n="HIAT:w" s="T12">čoːborse</ts>
                  <nts id="Seg_46" n="HIAT:ip">.</nts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_49" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_51" n="HIAT:w" s="T13">Aurkumbattə</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_54" n="HIAT:w" s="T14">tʼiwi</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_57" n="HIAT:w" s="T15">čobor</ts>
                  <nts id="Seg_58" n="HIAT:ip">.</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_61" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_63" n="HIAT:w" s="T16">Qanɣo</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_66" n="HIAT:w" s="T17">mekumbattə</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_69" n="HIAT:w" s="T18">tʼium</ts>
                  <nts id="Seg_70" n="HIAT:ip">.</nts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_73" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_75" n="HIAT:w" s="T19">Lukoškand</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_78" n="HIAT:w" s="T20">penkumbattə</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_81" n="HIAT:w" s="T21">tʼeum</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_84" n="HIAT:w" s="T22">tʼiːrlʼe</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_87" n="HIAT:w" s="T23">i</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_90" n="HIAT:w" s="T24">paːrmand</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_92" n="HIAT:ip">(</nts>
                  <ts e="T26" id="Seg_94" n="HIAT:w" s="T25">paːrmunt</ts>
                  <nts id="Seg_95" n="HIAT:ip">)</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">kamčukumbattə</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">ürse</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">qaːnɣoː</ts>
                  <nts id="Seg_105" n="HIAT:ip">.</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_108" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_110" n="HIAT:w" s="T29">Qaj</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_113" n="HIAT:w" s="T30">apsot</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_116" n="HIAT:w" s="T31">täblanan</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_119" n="HIAT:w" s="T32">eːkumɨnt</ts>
                  <nts id="Seg_120" n="HIAT:ip">.</nts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_123" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_125" n="HIAT:w" s="T33">Čačekumbattə</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_128" n="HIAT:w" s="T34">qaːngoː</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_131" n="HIAT:w" s="T35">klʼeːsʼ</ts>
                  <nts id="Seg_132" n="HIAT:ip">.</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_135" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_137" n="HIAT:w" s="T36">I</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_140" n="HIAT:w" s="T37">wadʼim</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_143" n="HIAT:w" s="T38">peːŋgan</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_146" n="HIAT:w" s="T39">čaːgorkumbattə</ts>
                  <nts id="Seg_147" n="HIAT:ip">.</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_150" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_152" n="HIAT:w" s="T40">Nʼäjno</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_155" n="HIAT:w" s="T41">čagəmbədi</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_158" n="HIAT:w" s="T42">wadʼim</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_161" n="HIAT:w" s="T43">aurgumbattə</ts>
                  <nts id="Seg_162" n="HIAT:ip">.</nts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_165" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_167" n="HIAT:w" s="T44">I</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_170" n="HIAT:w" s="T45">qwälɨm</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_173" n="HIAT:w" s="T46">näj</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_176" n="HIAT:w" s="T47">čagurrukumbattə</ts>
                  <nts id="Seg_177" n="HIAT:ip">.</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_180" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_182" n="HIAT:w" s="T48">Porsɨno</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_185" n="HIAT:w" s="T49">mejakumbattə</ts>
                  <nts id="Seg_186" n="HIAT:ip">.</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_189" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_191" n="HIAT:w" s="T50">Lukoškaland</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_194" n="HIAT:w" s="T51">penkumbattə</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_197" n="HIAT:w" s="T52">qaːnɣoː</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_200" n="HIAT:w" s="T53">qwälɨm</ts>
                  <nts id="Seg_201" n="HIAT:ip">.</nts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T54" id="Seg_203" n="sc" s="T1">
               <ts e="T2" id="Seg_205" n="e" s="T1">Ukon </ts>
               <ts e="T3" id="Seg_207" n="e" s="T2">tʼümolɣulannan </ts>
               <ts e="T4" id="Seg_209" n="e" s="T3">nʼäj </ts>
               <ts e="T5" id="Seg_211" n="e" s="T4">tʼänkumban. </ts>
               <ts e="T6" id="Seg_213" n="e" s="T5">Nʼäjim </ts>
               <ts e="T7" id="Seg_215" n="e" s="T6">az </ts>
               <ts e="T8" id="Seg_217" n="e" s="T7">aukumbattə. </ts>
               <ts e="T9" id="Seg_219" n="e" s="T8">Kwälaze </ts>
               <ts e="T10" id="Seg_221" n="e" s="T9">elakumbattə, </ts>
               <ts e="T11" id="Seg_223" n="e" s="T10">watʼse </ts>
               <ts e="T12" id="Seg_225" n="e" s="T11">elakumbattə, </ts>
               <ts e="T13" id="Seg_227" n="e" s="T12">čoːborse. </ts>
               <ts e="T14" id="Seg_229" n="e" s="T13">Aurkumbattə </ts>
               <ts e="T15" id="Seg_231" n="e" s="T14">tʼiwi </ts>
               <ts e="T16" id="Seg_233" n="e" s="T15">čobor. </ts>
               <ts e="T17" id="Seg_235" n="e" s="T16">Qanɣo </ts>
               <ts e="T18" id="Seg_237" n="e" s="T17">mekumbattə </ts>
               <ts e="T19" id="Seg_239" n="e" s="T18">tʼium. </ts>
               <ts e="T20" id="Seg_241" n="e" s="T19">Lukoškand </ts>
               <ts e="T21" id="Seg_243" n="e" s="T20">penkumbattə </ts>
               <ts e="T22" id="Seg_245" n="e" s="T21">tʼeum </ts>
               <ts e="T23" id="Seg_247" n="e" s="T22">tʼiːrlʼe </ts>
               <ts e="T24" id="Seg_249" n="e" s="T23">i </ts>
               <ts e="T25" id="Seg_251" n="e" s="T24">paːrmand </ts>
               <ts e="T26" id="Seg_253" n="e" s="T25">(paːrmunt) </ts>
               <ts e="T27" id="Seg_255" n="e" s="T26">kamčukumbattə </ts>
               <ts e="T28" id="Seg_257" n="e" s="T27">ürse </ts>
               <ts e="T29" id="Seg_259" n="e" s="T28">qaːnɣoː. </ts>
               <ts e="T30" id="Seg_261" n="e" s="T29">Qaj </ts>
               <ts e="T31" id="Seg_263" n="e" s="T30">apsot </ts>
               <ts e="T32" id="Seg_265" n="e" s="T31">täblanan </ts>
               <ts e="T33" id="Seg_267" n="e" s="T32">eːkumɨnt. </ts>
               <ts e="T34" id="Seg_269" n="e" s="T33">Čačekumbattə </ts>
               <ts e="T35" id="Seg_271" n="e" s="T34">qaːngoː </ts>
               <ts e="T36" id="Seg_273" n="e" s="T35">klʼeːsʼ. </ts>
               <ts e="T37" id="Seg_275" n="e" s="T36">I </ts>
               <ts e="T38" id="Seg_277" n="e" s="T37">wadʼim </ts>
               <ts e="T39" id="Seg_279" n="e" s="T38">peːŋgan </ts>
               <ts e="T40" id="Seg_281" n="e" s="T39">čaːgorkumbattə. </ts>
               <ts e="T41" id="Seg_283" n="e" s="T40">Nʼäjno </ts>
               <ts e="T42" id="Seg_285" n="e" s="T41">čagəmbədi </ts>
               <ts e="T43" id="Seg_287" n="e" s="T42">wadʼim </ts>
               <ts e="T44" id="Seg_289" n="e" s="T43">aurgumbattə. </ts>
               <ts e="T45" id="Seg_291" n="e" s="T44">I </ts>
               <ts e="T46" id="Seg_293" n="e" s="T45">qwälɨm </ts>
               <ts e="T47" id="Seg_295" n="e" s="T46">näj </ts>
               <ts e="T48" id="Seg_297" n="e" s="T47">čagurrukumbattə. </ts>
               <ts e="T49" id="Seg_299" n="e" s="T48">Porsɨno </ts>
               <ts e="T50" id="Seg_301" n="e" s="T49">mejakumbattə. </ts>
               <ts e="T51" id="Seg_303" n="e" s="T50">Lukoškaland </ts>
               <ts e="T52" id="Seg_305" n="e" s="T51">penkumbattə </ts>
               <ts e="T53" id="Seg_307" n="e" s="T52">qaːnɣoː </ts>
               <ts e="T54" id="Seg_309" n="e" s="T53">qwälɨm. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_310" s="T1">ChAE_196X_OstyakFood_nar.001 (001.001)</ta>
            <ta e="T8" id="Seg_311" s="T5">ChAE_196X_OstyakFood_nar.002 (001.002)</ta>
            <ta e="T13" id="Seg_312" s="T8">ChAE_196X_OstyakFood_nar.003 (001.003)</ta>
            <ta e="T16" id="Seg_313" s="T13">ChAE_196X_OstyakFood_nar.004 (001.004)</ta>
            <ta e="T19" id="Seg_314" s="T16">ChAE_196X_OstyakFood_nar.005 (001.005)</ta>
            <ta e="T29" id="Seg_315" s="T19">ChAE_196X_OstyakFood_nar.006 (001.006)</ta>
            <ta e="T33" id="Seg_316" s="T29">ChAE_196X_OstyakFood_nar.007 (001.007)</ta>
            <ta e="T36" id="Seg_317" s="T33">ChAE_196X_OstyakFood_nar.008 (001.008)</ta>
            <ta e="T40" id="Seg_318" s="T36">ChAE_196X_OstyakFood_nar.009 (001.009)</ta>
            <ta e="T44" id="Seg_319" s="T40">ChAE_196X_OstyakFood_nar.010 (001.010)</ta>
            <ta e="T48" id="Seg_320" s="T44">ChAE_196X_OstyakFood_nar.011 (001.011)</ta>
            <ta e="T50" id="Seg_321" s="T48">ChAE_196X_OstyakFood_nar.012 (001.012)</ta>
            <ta e="T54" id="Seg_322" s="T50">ChAE_196X_OstyakFood_nar.013 (001.013)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_323" s="T1">укон ′тʼӱмоlɣуланнан нʼӓй ′тʼӓнк(г)умбан.</ta>
            <ta e="T8" id="Seg_324" s="T5">′нʼӓйим аз ‵аукум′баттъ.</ta>
            <ta e="T13" id="Seg_325" s="T8">квӓла′зе ′елакум‵баттъ, ватʼ′се ′елакум′баттъ, тшо̄′борсе.</ta>
            <ta e="T16" id="Seg_326" s="T13">‵ауркум′баттъ ′тʼиви ′тшобор.</ta>
            <ta e="T19" id="Seg_327" s="T16">kан′ɣо мекум′баттъ тʼи′ум.</ta>
            <ta e="T29" id="Seg_328" s="T19">лукошканд пе̨нкум′баттъ тʼе′ум ′тʼӣрлʼе и ′па̄рманд (′па̄рмунт) ‵камтшукум′баттъ ӱр′се ‵k(к)а̄н′ɣо̄.</ta>
            <ta e="T33" id="Seg_329" s="T29">kай ап′сот ′тӓбланан ′е̄ку‵мынт.</ta>
            <ta e="T36" id="Seg_330" s="T33">тшатшекумбаттъ k(к)а̄н′го̄ ′клʼе̄сʼ.</ta>
            <ta e="T40" id="Seg_331" s="T36">и ва′дʼим ′пе̄ңган ′тша̄горку′мбаттъ.</ta>
            <ta e="T44" id="Seg_332" s="T40">нʼӓйно ′тшагъмбъди ва′дʼим аургум′баттъ.</ta>
            <ta e="T48" id="Seg_333" s="T44">и ′kwӓлым ′нӓй ′тшагур‵рукум′баттъ.</ta>
            <ta e="T50" id="Seg_334" s="T48">порсы′но ′меjакум′баттъ.</ta>
            <ta e="T54" id="Seg_335" s="T50">лу′кошкаланд пенкумбаттъ k(к)а̄н′ɣо̄ kwӓлым.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_336" s="T1">ukon tʼümolɣulannan nʼäj tʼänk(g)umban.</ta>
            <ta e="T8" id="Seg_337" s="T5">nʼäjim az aukumbattə.</ta>
            <ta e="T13" id="Seg_338" s="T8">kwälaze elakumbattə, watʼse elakumbattə, čoːborse.</ta>
            <ta e="T16" id="Seg_339" s="T13">aurkumbattə tʼiwi čobor.</ta>
            <ta e="T19" id="Seg_340" s="T16">qanɣo mekumbattə tʼium.</ta>
            <ta e="T29" id="Seg_341" s="T19">lukoškand penkumbattə tʼeum tʼiːrlʼe i paːrmand (paːrmunt) kamčukumbattə ürse q(k)aːnɣoː.</ta>
            <ta e="T33" id="Seg_342" s="T29">qaj apsot täblanan eːkumɨnt.</ta>
            <ta e="T36" id="Seg_343" s="T33">čačekumbattə q(k)aːngoː klʼeːsʼ.</ta>
            <ta e="T40" id="Seg_344" s="T36">i wadʼim peːŋgan čaːgorkumbattə.</ta>
            <ta e="T44" id="Seg_345" s="T40">nʼäjno čagəmbədi wadʼim aurgumbattə.</ta>
            <ta e="T48" id="Seg_346" s="T44">i qwälɨm näj čagurrukumbattə.</ta>
            <ta e="T50" id="Seg_347" s="T48">porsɨno mejakumbattə.</ta>
            <ta e="T54" id="Seg_348" s="T50">lukoškaland penkumbattə q(k)aːnɣoː qwälɨm.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_349" s="T1">Ukon tʼümolɣulannan nʼäj tʼänkumban. </ta>
            <ta e="T8" id="Seg_350" s="T5">Nʼäjim az aukumbattə. </ta>
            <ta e="T13" id="Seg_351" s="T8">Kwälaze elakumbattə, watʼse elakumbattə, čoːborse. </ta>
            <ta e="T16" id="Seg_352" s="T13">Aurkumbattə tʼiwi čobor. </ta>
            <ta e="T19" id="Seg_353" s="T16">Qanɣo mekumbattə tʼium. </ta>
            <ta e="T29" id="Seg_354" s="T19">Lukoškand penkumbattə tʼeum tʼiːrlʼe i paːrmand (paːrmunt) kamčukumbattə ürse qaːnɣoː. </ta>
            <ta e="T33" id="Seg_355" s="T29">Qaj apsot täblanan eːkumɨnt. </ta>
            <ta e="T36" id="Seg_356" s="T33">Čačekumbattə qaːngoː klʼeːsʼ. </ta>
            <ta e="T40" id="Seg_357" s="T36">I wadʼim peːŋgan čaːgorkumbattə. </ta>
            <ta e="T44" id="Seg_358" s="T40">Nʼäjno čagəmbədi wadʼim aurgumbattə. </ta>
            <ta e="T48" id="Seg_359" s="T44">I qwälɨm näj čagurrukumbattə. </ta>
            <ta e="T50" id="Seg_360" s="T48">Porsɨno mejakumbattə. </ta>
            <ta e="T54" id="Seg_361" s="T50">Lukoškaland penkumbattə qaːnɣoː qwälɨm. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_362" s="T1">ukon</ta>
            <ta e="T3" id="Seg_363" s="T2">tʼüməlʼqum-la-nnan</ta>
            <ta e="T4" id="Seg_364" s="T3">nʼäj</ta>
            <ta e="T5" id="Seg_365" s="T4">tʼänku-mba-n</ta>
            <ta e="T6" id="Seg_366" s="T5">nʼäj-i-m</ta>
            <ta e="T7" id="Seg_367" s="T6">az</ta>
            <ta e="T8" id="Seg_368" s="T7">au-ku-mba-ttə</ta>
            <ta e="T9" id="Seg_369" s="T8">kwäl-a-ze</ta>
            <ta e="T10" id="Seg_370" s="T9">ela-ku-mba-ttə</ta>
            <ta e="T11" id="Seg_371" s="T10">watʼ-se</ta>
            <ta e="T12" id="Seg_372" s="T11">ela-ku-mba-ttə</ta>
            <ta e="T13" id="Seg_373" s="T12">čoːbor-se</ta>
            <ta e="T14" id="Seg_374" s="T13">au-r-ku-mba-ttə</ta>
            <ta e="T15" id="Seg_375" s="T14">tʼiwi</ta>
            <ta e="T16" id="Seg_376" s="T15">čobor</ta>
            <ta e="T17" id="Seg_377" s="T16">qa-nɣo</ta>
            <ta e="T18" id="Seg_378" s="T17">me-ku-mba-ttə</ta>
            <ta e="T19" id="Seg_379" s="T18">tʼiu-m</ta>
            <ta e="T20" id="Seg_380" s="T19">lukoška-nd</ta>
            <ta e="T21" id="Seg_381" s="T20">pen-ku-mba-ttə</ta>
            <ta e="T22" id="Seg_382" s="T21">tʼeu-m</ta>
            <ta e="T23" id="Seg_383" s="T22">tʼiːr-lʼe</ta>
            <ta e="T24" id="Seg_384" s="T23">i</ta>
            <ta e="T25" id="Seg_385" s="T24">paːr-man-d</ta>
            <ta e="T26" id="Seg_386" s="T25">paːr-mun-t</ta>
            <ta e="T27" id="Seg_387" s="T26">kamču-ku-mba-ttə</ta>
            <ta e="T28" id="Seg_388" s="T27">ür-se</ta>
            <ta e="T29" id="Seg_389" s="T28">qaː-nɣoː</ta>
            <ta e="T30" id="Seg_390" s="T29">qa-j</ta>
            <ta e="T31" id="Seg_391" s="T30">apsot</ta>
            <ta e="T32" id="Seg_392" s="T31">täb-la-nan</ta>
            <ta e="T33" id="Seg_393" s="T32">eː-ku-mɨ-nt</ta>
            <ta e="T34" id="Seg_394" s="T33">čače-ku-mba-ttə</ta>
            <ta e="T35" id="Seg_395" s="T34">qaː-ngoː</ta>
            <ta e="T36" id="Seg_396" s="T35">klʼeːsʼ</ta>
            <ta e="T37" id="Seg_397" s="T36">i</ta>
            <ta e="T38" id="Seg_398" s="T37">wadʼi-m</ta>
            <ta e="T39" id="Seg_399" s="T38">peːŋg-a-n</ta>
            <ta e="T40" id="Seg_400" s="T39">čaːgor-ku-mba-ttə</ta>
            <ta e="T41" id="Seg_401" s="T40">nʼäj-no</ta>
            <ta e="T42" id="Seg_402" s="T41">čagə-mbədi</ta>
            <ta e="T43" id="Seg_403" s="T42">wadʼi-m</ta>
            <ta e="T44" id="Seg_404" s="T43">au-r-gu-mba-ttə</ta>
            <ta e="T45" id="Seg_405" s="T44">i</ta>
            <ta e="T46" id="Seg_406" s="T45">qwäl-ɨ-m</ta>
            <ta e="T47" id="Seg_407" s="T46">näj</ta>
            <ta e="T48" id="Seg_408" s="T47">čagur-ru-ku-mba-ttə</ta>
            <ta e="T49" id="Seg_409" s="T48">porsɨ-no</ta>
            <ta e="T50" id="Seg_410" s="T49">me-ja-ku-mba-ttə</ta>
            <ta e="T51" id="Seg_411" s="T50">lukoška-la-nd</ta>
            <ta e="T52" id="Seg_412" s="T51">pen-ku-mba-ttə</ta>
            <ta e="T53" id="Seg_413" s="T52">qaː-nɣoː</ta>
            <ta e="T54" id="Seg_414" s="T53">qwäl-ɨ-m</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_415" s="T1">ugon</ta>
            <ta e="T3" id="Seg_416" s="T2">tʼüməlʼqum-la-nan</ta>
            <ta e="T4" id="Seg_417" s="T3">nʼäj</ta>
            <ta e="T5" id="Seg_418" s="T4">tʼäkku-mbɨ-n</ta>
            <ta e="T6" id="Seg_419" s="T5">nʼäj-ɨ-m</ta>
            <ta e="T7" id="Seg_420" s="T6">asa</ta>
            <ta e="T8" id="Seg_421" s="T7">am-ku-mbɨ-tɨn</ta>
            <ta e="T9" id="Seg_422" s="T8">qwɛl-ɨ-se</ta>
            <ta e="T10" id="Seg_423" s="T9">elɨ-ku-mbɨ-tɨn</ta>
            <ta e="T11" id="Seg_424" s="T10">wadʼi-se</ta>
            <ta e="T12" id="Seg_425" s="T11">elɨ-ku-mbɨ-tɨn</ta>
            <ta e="T13" id="Seg_426" s="T12">čobər-se</ta>
            <ta e="T14" id="Seg_427" s="T13">am-r-ku-mbɨ-tɨn</ta>
            <ta e="T15" id="Seg_428" s="T14">tivə</ta>
            <ta e="T16" id="Seg_429" s="T15">čobər</ta>
            <ta e="T17" id="Seg_430" s="T16">ka-no</ta>
            <ta e="T18" id="Seg_431" s="T17">meː-ku-mbɨ-tɨn</ta>
            <ta e="T19" id="Seg_432" s="T18">tivə-m</ta>
            <ta e="T20" id="Seg_433" s="T19">lukoška-ntə</ta>
            <ta e="T21" id="Seg_434" s="T20">pen-ku-mbɨ-tɨn</ta>
            <ta e="T22" id="Seg_435" s="T21">tivə-m</ta>
            <ta e="T23" id="Seg_436" s="T22">tiːrɨ-le</ta>
            <ta e="T24" id="Seg_437" s="T23">i</ta>
            <ta e="T25" id="Seg_438" s="T24">par-un-ntɨ</ta>
            <ta e="T26" id="Seg_439" s="T25">par-un-ntɨ</ta>
            <ta e="T27" id="Seg_440" s="T26">qamǯu-ku-mbɨ-tɨn</ta>
            <ta e="T28" id="Seg_441" s="T27">ür-se</ta>
            <ta e="T29" id="Seg_442" s="T28">ka-no</ta>
            <ta e="T30" id="Seg_443" s="T29">ka-lʼ</ta>
            <ta e="T31" id="Seg_444" s="T30">apsod</ta>
            <ta e="T32" id="Seg_445" s="T31">täp-la-nan</ta>
            <ta e="T33" id="Seg_446" s="T32">eː-ku-mbɨ-ntɨ</ta>
            <ta e="T34" id="Seg_447" s="T33">čeččɨ-ku-mbɨ-tɨn</ta>
            <ta e="T35" id="Seg_448" s="T34">ka-no</ta>
            <ta e="T36" id="Seg_449" s="T35">klʼes</ta>
            <ta e="T37" id="Seg_450" s="T36">i</ta>
            <ta e="T38" id="Seg_451" s="T37">wadʼi-m</ta>
            <ta e="T39" id="Seg_452" s="T38">peq-ɨ-n</ta>
            <ta e="T40" id="Seg_453" s="T39">čagər-ku-mbɨ-tɨn</ta>
            <ta e="T41" id="Seg_454" s="T40">nʼäj-no</ta>
            <ta e="T42" id="Seg_455" s="T41">čagɨ-mbɨdi</ta>
            <ta e="T43" id="Seg_456" s="T42">wadʼi-m</ta>
            <ta e="T44" id="Seg_457" s="T43">am-r-ku-mbɨ-tɨn</ta>
            <ta e="T45" id="Seg_458" s="T44">i</ta>
            <ta e="T46" id="Seg_459" s="T45">qwɛl-ɨ-m</ta>
            <ta e="T47" id="Seg_460" s="T46">naj</ta>
            <ta e="T48" id="Seg_461" s="T47">čagər-rɨ-ku-mbɨ-tɨn</ta>
            <ta e="T49" id="Seg_462" s="T48">porsə-no</ta>
            <ta e="T50" id="Seg_463" s="T49">meː-ja-ku-mbɨ-tɨn</ta>
            <ta e="T51" id="Seg_464" s="T50">lukoška-la-ntə</ta>
            <ta e="T52" id="Seg_465" s="T51">pen-ku-mbɨ-tɨn</ta>
            <ta e="T53" id="Seg_466" s="T52">ka-no</ta>
            <ta e="T54" id="Seg_467" s="T53">qwɛl-ɨ-m</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_468" s="T1">earlier</ta>
            <ta e="T3" id="Seg_469" s="T2">a.Selkup.man-PL-ADES</ta>
            <ta e="T4" id="Seg_470" s="T3">bread.[NOM]</ta>
            <ta e="T5" id="Seg_471" s="T4">NEG.EX-PST.NAR-3SG.S</ta>
            <ta e="T6" id="Seg_472" s="T5">bread-EP-ACC</ta>
            <ta e="T7" id="Seg_473" s="T6">NEG</ta>
            <ta e="T8" id="Seg_474" s="T7">eat-HAB-PST.NAR-3PL</ta>
            <ta e="T9" id="Seg_475" s="T8">fish-EP-INSTR</ta>
            <ta e="T10" id="Seg_476" s="T9">live-HAB-PST.NAR-3PL</ta>
            <ta e="T11" id="Seg_477" s="T10">meat-INSTR</ta>
            <ta e="T12" id="Seg_478" s="T11">live-HAB-PST.NAR-3PL</ta>
            <ta e="T13" id="Seg_479" s="T12">berry-INSTR</ta>
            <ta e="T14" id="Seg_480" s="T13">eat-FRQ-HAB-PST.NAR-3PL</ta>
            <ta e="T15" id="Seg_481" s="T14">bird.cherry.[NOM]</ta>
            <ta e="T16" id="Seg_482" s="T15">berry.[NOM]</ta>
            <ta e="T17" id="Seg_483" s="T16">winter-TRL</ta>
            <ta e="T18" id="Seg_484" s="T17">do-HAB-PST.NAR-3PL</ta>
            <ta e="T19" id="Seg_485" s="T18">bird.cherry-ACC</ta>
            <ta e="T20" id="Seg_486" s="T19">basket-ILL</ta>
            <ta e="T21" id="Seg_487" s="T20">put-HAB-PST.NAR-3PL</ta>
            <ta e="T22" id="Seg_488" s="T21">bird.cherry-ACC</ta>
            <ta e="T23" id="Seg_489" s="T22">fill-CVB</ta>
            <ta e="T24" id="Seg_490" s="T23">and</ta>
            <ta e="T25" id="Seg_491" s="T24">top-PROL-OBL.3SG</ta>
            <ta e="T26" id="Seg_492" s="T25">top-PROL-OBL.3SG</ta>
            <ta e="T27" id="Seg_493" s="T26">pour-HAB-PST.NAR-3PL</ta>
            <ta e="T28" id="Seg_494" s="T27">fat-COM</ta>
            <ta e="T29" id="Seg_495" s="T28">winter-TRL</ta>
            <ta e="T30" id="Seg_496" s="T29">winter-ADJZ</ta>
            <ta e="T31" id="Seg_497" s="T30">food.[NOM]</ta>
            <ta e="T32" id="Seg_498" s="T31">(s)he-PL-ADES</ta>
            <ta e="T33" id="Seg_499" s="T32">be-HAB-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T34" id="Seg_500" s="T33">put-HAB-PST.NAR-3PL</ta>
            <ta e="T35" id="Seg_501" s="T34">winter-TRL</ta>
            <ta e="T36" id="Seg_502" s="T35">barn.[NOM]</ta>
            <ta e="T37" id="Seg_503" s="T36">and</ta>
            <ta e="T38" id="Seg_504" s="T37">meat-ACC</ta>
            <ta e="T39" id="Seg_505" s="T38">elk-EP-GEN</ta>
            <ta e="T40" id="Seg_506" s="T39">dry.up-HAB-PST.NAR-3PL</ta>
            <ta e="T41" id="Seg_507" s="T40">bread-TRL</ta>
            <ta e="T42" id="Seg_508" s="T41">dry-PTCP.PST</ta>
            <ta e="T43" id="Seg_509" s="T42">meat-ACC</ta>
            <ta e="T44" id="Seg_510" s="T43">eat-FRQ-HAB-PST.NAR-3PL</ta>
            <ta e="T45" id="Seg_511" s="T44">and</ta>
            <ta e="T46" id="Seg_512" s="T45">fish-EP-ACC</ta>
            <ta e="T47" id="Seg_513" s="T46">also</ta>
            <ta e="T48" id="Seg_514" s="T47">dry.up-CAUS-HAB-PST.NAR-3PL</ta>
            <ta e="T49" id="Seg_515" s="T48">fish.flour-TRL</ta>
            <ta e="T50" id="Seg_516" s="T49">do-%%-HAB-PST.NAR-3PL</ta>
            <ta e="T51" id="Seg_517" s="T50">basket-PL-ILL</ta>
            <ta e="T52" id="Seg_518" s="T51">put-HAB-PST.NAR-3PL</ta>
            <ta e="T53" id="Seg_519" s="T52">winter-TRL</ta>
            <ta e="T54" id="Seg_520" s="T53">fish-EP-ACC</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_521" s="T1">раньше</ta>
            <ta e="T3" id="Seg_522" s="T2">селькуп-PL-ADES</ta>
            <ta e="T4" id="Seg_523" s="T3">хлеб.[NOM]</ta>
            <ta e="T5" id="Seg_524" s="T4">NEG.EX-PST.NAR-3SG.S</ta>
            <ta e="T6" id="Seg_525" s="T5">хлеб-EP-ACC</ta>
            <ta e="T7" id="Seg_526" s="T6">NEG</ta>
            <ta e="T8" id="Seg_527" s="T7">съесть-HAB-PST.NAR-3PL</ta>
            <ta e="T9" id="Seg_528" s="T8">рыба-EP-INSTR</ta>
            <ta e="T10" id="Seg_529" s="T9">жить-HAB-PST.NAR-3PL</ta>
            <ta e="T11" id="Seg_530" s="T10">мясо-INSTR</ta>
            <ta e="T12" id="Seg_531" s="T11">жить-HAB-PST.NAR-3PL</ta>
            <ta e="T13" id="Seg_532" s="T12">ягода-INSTR</ta>
            <ta e="T14" id="Seg_533" s="T13">съесть-FRQ-HAB-PST.NAR-3PL</ta>
            <ta e="T15" id="Seg_534" s="T14">черемуха.[NOM]</ta>
            <ta e="T16" id="Seg_535" s="T15">ягода.[NOM]</ta>
            <ta e="T17" id="Seg_536" s="T16">зима-TRL</ta>
            <ta e="T18" id="Seg_537" s="T17">сделать-HAB-PST.NAR-3PL</ta>
            <ta e="T19" id="Seg_538" s="T18">черемуха-ACC</ta>
            <ta e="T20" id="Seg_539" s="T19">лукошко-ILL</ta>
            <ta e="T21" id="Seg_540" s="T20">положить-HAB-PST.NAR-3PL</ta>
            <ta e="T22" id="Seg_541" s="T21">черемуха-ACC</ta>
            <ta e="T23" id="Seg_542" s="T22">наполнить-CVB</ta>
            <ta e="T24" id="Seg_543" s="T23">и</ta>
            <ta e="T25" id="Seg_544" s="T24">верхняя.часть-PROL-OBL.3SG</ta>
            <ta e="T26" id="Seg_545" s="T25">верхняя.часть-PROL-OBL.3SG</ta>
            <ta e="T27" id="Seg_546" s="T26">налить-HAB-PST.NAR-3PL</ta>
            <ta e="T28" id="Seg_547" s="T27">жир-COM</ta>
            <ta e="T29" id="Seg_548" s="T28">зима-TRL</ta>
            <ta e="T30" id="Seg_549" s="T29">зима-ADJZ</ta>
            <ta e="T31" id="Seg_550" s="T30">еда.[NOM]</ta>
            <ta e="T32" id="Seg_551" s="T31">он(а)-PL-ADES</ta>
            <ta e="T33" id="Seg_552" s="T32">быть-HAB-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T34" id="Seg_553" s="T33">поставить-HAB-PST.NAR-3PL</ta>
            <ta e="T35" id="Seg_554" s="T34">зима-TRL</ta>
            <ta e="T36" id="Seg_555" s="T35">амбар.[NOM]</ta>
            <ta e="T37" id="Seg_556" s="T36">и</ta>
            <ta e="T38" id="Seg_557" s="T37">мясо-ACC</ta>
            <ta e="T39" id="Seg_558" s="T38">лось-EP-GEN</ta>
            <ta e="T40" id="Seg_559" s="T39">засушить-HAB-PST.NAR-3PL</ta>
            <ta e="T41" id="Seg_560" s="T40">хлеб-TRL</ta>
            <ta e="T42" id="Seg_561" s="T41">высохнуть-PTCP.PST</ta>
            <ta e="T43" id="Seg_562" s="T42">мясо-ACC</ta>
            <ta e="T44" id="Seg_563" s="T43">съесть-FRQ-HAB-PST.NAR-3PL</ta>
            <ta e="T45" id="Seg_564" s="T44">и</ta>
            <ta e="T46" id="Seg_565" s="T45">рыба-EP-ACC</ta>
            <ta e="T47" id="Seg_566" s="T46">тоже</ta>
            <ta e="T48" id="Seg_567" s="T47">засушить-CAUS-HAB-PST.NAR-3PL</ta>
            <ta e="T49" id="Seg_568" s="T48">мука.из.рыбы-TRL</ta>
            <ta e="T50" id="Seg_569" s="T49">сделать-%%-HAB-PST.NAR-3PL</ta>
            <ta e="T51" id="Seg_570" s="T50">лукошко-PL-ILL</ta>
            <ta e="T52" id="Seg_571" s="T51">положить-HAB-PST.NAR-3PL</ta>
            <ta e="T53" id="Seg_572" s="T52">зима-TRL</ta>
            <ta e="T54" id="Seg_573" s="T53">рыба-EP-ACC</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_574" s="T1">adv</ta>
            <ta e="T3" id="Seg_575" s="T2">n-n:num-n:case</ta>
            <ta e="T4" id="Seg_576" s="T3">n.[n:case]</ta>
            <ta e="T5" id="Seg_577" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_578" s="T5">n-n:ins-n:case</ta>
            <ta e="T7" id="Seg_579" s="T6">ptcl</ta>
            <ta e="T8" id="Seg_580" s="T7">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_581" s="T8">n-n:ins-n:case</ta>
            <ta e="T10" id="Seg_582" s="T9">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_583" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_584" s="T11">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_585" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_586" s="T13">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T15" id="Seg_587" s="T14">n.[n:case]</ta>
            <ta e="T16" id="Seg_588" s="T15">n.[n:case]</ta>
            <ta e="T17" id="Seg_589" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_590" s="T17">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_591" s="T18">n-n:case</ta>
            <ta e="T20" id="Seg_592" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_593" s="T20">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_594" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_595" s="T22">v-v&gt;adv</ta>
            <ta e="T24" id="Seg_596" s="T23">conj</ta>
            <ta e="T25" id="Seg_597" s="T24">n-n:case-n:obl.poss</ta>
            <ta e="T26" id="Seg_598" s="T25">n-n:case-n:obl.poss</ta>
            <ta e="T27" id="Seg_599" s="T26">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_600" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_601" s="T28">n-n:case</ta>
            <ta e="T30" id="Seg_602" s="T29">n-n&gt;adj</ta>
            <ta e="T31" id="Seg_603" s="T30">n.[n:case]</ta>
            <ta e="T32" id="Seg_604" s="T31">pers-n:num-n:case</ta>
            <ta e="T33" id="Seg_605" s="T32">v-v&gt;v-v:tense-v:mood.[v:pn]</ta>
            <ta e="T34" id="Seg_606" s="T33">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T35" id="Seg_607" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_608" s="T35">n.[n:case]</ta>
            <ta e="T37" id="Seg_609" s="T36">conj</ta>
            <ta e="T38" id="Seg_610" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_611" s="T38">n-n:ins-n:case</ta>
            <ta e="T40" id="Seg_612" s="T39">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T41" id="Seg_613" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_614" s="T41">v-v&gt;ptcp</ta>
            <ta e="T43" id="Seg_615" s="T42">n-n:case</ta>
            <ta e="T44" id="Seg_616" s="T43">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T45" id="Seg_617" s="T44">conj</ta>
            <ta e="T46" id="Seg_618" s="T45">n-n:ins-n:case</ta>
            <ta e="T47" id="Seg_619" s="T46">ptcl</ta>
            <ta e="T48" id="Seg_620" s="T47">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T49" id="Seg_621" s="T48">n-n:case</ta>
            <ta e="T50" id="Seg_622" s="T49">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T51" id="Seg_623" s="T50">n-n:num-n:case</ta>
            <ta e="T52" id="Seg_624" s="T51">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T53" id="Seg_625" s="T52">n-n:case</ta>
            <ta e="T54" id="Seg_626" s="T53">n-n:ins-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_627" s="T1">adv</ta>
            <ta e="T3" id="Seg_628" s="T2">n</ta>
            <ta e="T4" id="Seg_629" s="T3">n</ta>
            <ta e="T5" id="Seg_630" s="T4">v</ta>
            <ta e="T6" id="Seg_631" s="T5">n</ta>
            <ta e="T7" id="Seg_632" s="T6">ptcl</ta>
            <ta e="T8" id="Seg_633" s="T7">v</ta>
            <ta e="T9" id="Seg_634" s="T8">n</ta>
            <ta e="T10" id="Seg_635" s="T9">v</ta>
            <ta e="T11" id="Seg_636" s="T10">n</ta>
            <ta e="T12" id="Seg_637" s="T11">v</ta>
            <ta e="T13" id="Seg_638" s="T12">n</ta>
            <ta e="T14" id="Seg_639" s="T13">v</ta>
            <ta e="T15" id="Seg_640" s="T14">n</ta>
            <ta e="T16" id="Seg_641" s="T15">n</ta>
            <ta e="T17" id="Seg_642" s="T16">n</ta>
            <ta e="T18" id="Seg_643" s="T17">v</ta>
            <ta e="T19" id="Seg_644" s="T18">n</ta>
            <ta e="T20" id="Seg_645" s="T19">n</ta>
            <ta e="T21" id="Seg_646" s="T20">v</ta>
            <ta e="T22" id="Seg_647" s="T21">n</ta>
            <ta e="T23" id="Seg_648" s="T22">v</ta>
            <ta e="T24" id="Seg_649" s="T23">conj</ta>
            <ta e="T25" id="Seg_650" s="T24">n</ta>
            <ta e="T26" id="Seg_651" s="T25">n</ta>
            <ta e="T27" id="Seg_652" s="T26">v</ta>
            <ta e="T28" id="Seg_653" s="T27">n</ta>
            <ta e="T29" id="Seg_654" s="T28">n</ta>
            <ta e="T30" id="Seg_655" s="T29">adj</ta>
            <ta e="T31" id="Seg_656" s="T30">n</ta>
            <ta e="T32" id="Seg_657" s="T31">pers</ta>
            <ta e="T33" id="Seg_658" s="T32">v</ta>
            <ta e="T34" id="Seg_659" s="T33">v</ta>
            <ta e="T35" id="Seg_660" s="T34">n</ta>
            <ta e="T36" id="Seg_661" s="T35">n</ta>
            <ta e="T37" id="Seg_662" s="T36">conj</ta>
            <ta e="T38" id="Seg_663" s="T37">n</ta>
            <ta e="T39" id="Seg_664" s="T38">n</ta>
            <ta e="T40" id="Seg_665" s="T39">v</ta>
            <ta e="T41" id="Seg_666" s="T40">n</ta>
            <ta e="T42" id="Seg_667" s="T41">ptcp</ta>
            <ta e="T43" id="Seg_668" s="T42">n</ta>
            <ta e="T44" id="Seg_669" s="T43">v</ta>
            <ta e="T45" id="Seg_670" s="T44">conj</ta>
            <ta e="T46" id="Seg_671" s="T45">n</ta>
            <ta e="T47" id="Seg_672" s="T46">ptcl</ta>
            <ta e="T48" id="Seg_673" s="T47">v</ta>
            <ta e="T49" id="Seg_674" s="T48">n</ta>
            <ta e="T50" id="Seg_675" s="T49">v</ta>
            <ta e="T51" id="Seg_676" s="T50">n</ta>
            <ta e="T52" id="Seg_677" s="T51">v</ta>
            <ta e="T53" id="Seg_678" s="T52">n</ta>
            <ta e="T54" id="Seg_679" s="T53">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_680" s="T1">adv:Time</ta>
            <ta e="T3" id="Seg_681" s="T2">np.h:Poss</ta>
            <ta e="T4" id="Seg_682" s="T3">np:Th</ta>
            <ta e="T6" id="Seg_683" s="T5">np:P</ta>
            <ta e="T8" id="Seg_684" s="T7">0.3.h:A</ta>
            <ta e="T10" id="Seg_685" s="T9">0.3.h:Th</ta>
            <ta e="T12" id="Seg_686" s="T11">0.3.h:Th</ta>
            <ta e="T14" id="Seg_687" s="T13">0.3.h:A</ta>
            <ta e="T15" id="Seg_688" s="T14">np:P</ta>
            <ta e="T18" id="Seg_689" s="T17">0.3.h:A</ta>
            <ta e="T19" id="Seg_690" s="T18">np:P</ta>
            <ta e="T20" id="Seg_691" s="T19">np:G</ta>
            <ta e="T21" id="Seg_692" s="T20">0.3.h:A</ta>
            <ta e="T22" id="Seg_693" s="T21">np:Th</ta>
            <ta e="T25" id="Seg_694" s="T24">np:Path</ta>
            <ta e="T27" id="Seg_695" s="T26">0.3.h:A</ta>
            <ta e="T28" id="Seg_696" s="T27">np:Th</ta>
            <ta e="T31" id="Seg_697" s="T30">np:Th</ta>
            <ta e="T32" id="Seg_698" s="T31">pro.h:Poss</ta>
            <ta e="T34" id="Seg_699" s="T33">0.3.h:A 0.3:Th</ta>
            <ta e="T36" id="Seg_700" s="T35">np:G</ta>
            <ta e="T38" id="Seg_701" s="T37">np:P</ta>
            <ta e="T39" id="Seg_702" s="T38">np:Poss</ta>
            <ta e="T40" id="Seg_703" s="T39">0.3.h:A</ta>
            <ta e="T43" id="Seg_704" s="T42">np:P</ta>
            <ta e="T44" id="Seg_705" s="T43">0.3.h:A</ta>
            <ta e="T46" id="Seg_706" s="T45">np:P</ta>
            <ta e="T48" id="Seg_707" s="T47">0.3.h:A</ta>
            <ta e="T50" id="Seg_708" s="T49">0.3.h:A 0.3:P</ta>
            <ta e="T51" id="Seg_709" s="T50">np:G</ta>
            <ta e="T52" id="Seg_710" s="T51">0.3.h:A</ta>
            <ta e="T54" id="Seg_711" s="T53">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_712" s="T3">np:S</ta>
            <ta e="T5" id="Seg_713" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_714" s="T5">np:O</ta>
            <ta e="T8" id="Seg_715" s="T7">0.3.h:S</ta>
            <ta e="T10" id="Seg_716" s="T9">0.3.h:S v:pred</ta>
            <ta e="T12" id="Seg_717" s="T11">0.3.h:S v:pred</ta>
            <ta e="T14" id="Seg_718" s="T13">0.3.h:S v:pred</ta>
            <ta e="T15" id="Seg_719" s="T14">np:O</ta>
            <ta e="T18" id="Seg_720" s="T17">0.3.h:S v:pred</ta>
            <ta e="T19" id="Seg_721" s="T18">np:O</ta>
            <ta e="T21" id="Seg_722" s="T20">0.3.h:S v:pred</ta>
            <ta e="T22" id="Seg_723" s="T21">np:O</ta>
            <ta e="T23" id="Seg_724" s="T22">s:temp</ta>
            <ta e="T27" id="Seg_725" s="T26">0.3.h:S v:pred</ta>
            <ta e="T28" id="Seg_726" s="T27">np:O</ta>
            <ta e="T31" id="Seg_727" s="T30">np:S</ta>
            <ta e="T33" id="Seg_728" s="T32">v:pred</ta>
            <ta e="T34" id="Seg_729" s="T33">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T38" id="Seg_730" s="T37">np:O</ta>
            <ta e="T40" id="Seg_731" s="T39">0.3.h:S v:pred</ta>
            <ta e="T43" id="Seg_732" s="T42">np:O</ta>
            <ta e="T44" id="Seg_733" s="T43">0.3.h:S v:pred</ta>
            <ta e="T46" id="Seg_734" s="T45">np:O</ta>
            <ta e="T48" id="Seg_735" s="T47">0.3.h:S v:pred</ta>
            <ta e="T50" id="Seg_736" s="T49">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T52" id="Seg_737" s="T51">0.3.h:S v:pred</ta>
            <ta e="T54" id="Seg_738" s="T53">np:O</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T20" id="Seg_739" s="T19">RUS:cult</ta>
            <ta e="T24" id="Seg_740" s="T23">RUS:gram</ta>
            <ta e="T37" id="Seg_741" s="T36">RUS:gram</ta>
            <ta e="T45" id="Seg_742" s="T44">RUS:gram</ta>
            <ta e="T49" id="Seg_743" s="T48">%%</ta>
            <ta e="T51" id="Seg_744" s="T50">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_745" s="T1">Earlier Selkups didn't have bread.</ta>
            <ta e="T8" id="Seg_746" s="T5">They didn't eat bread.</ta>
            <ta e="T13" id="Seg_747" s="T8">They lived by fish, by meat, by berries.</ta>
            <ta e="T16" id="Seg_748" s="T13">They ate bird cherries.</ta>
            <ta e="T19" id="Seg_749" s="T16">For winter time they stored bird cherry up.</ta>
            <ta e="T29" id="Seg_750" s="T19">For winter time they filled baskets with bird cherry and poured fish oil over it.</ta>
            <ta e="T33" id="Seg_751" s="T29">It was their winter food.</ta>
            <ta e="T36" id="Seg_752" s="T33">They put it into a barn for winter.</ta>
            <ta e="T40" id="Seg_753" s="T36">They used to dry elk meat.</ta>
            <ta e="T44" id="Seg_754" s="T40">They used to eat dry meat instead of bread.</ta>
            <ta e="T48" id="Seg_755" s="T44">They dried fish as well.</ta>
            <ta e="T50" id="Seg_756" s="T48">They made fish flour.</ta>
            <ta e="T54" id="Seg_757" s="T50">For winter time they put fish into baskets.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_758" s="T1">Früher hatten Selkupen kein Brot.</ta>
            <ta e="T8" id="Seg_759" s="T5">Sie aßen kein Brot.</ta>
            <ta e="T13" id="Seg_760" s="T8">Sie lebten von Fisch, von Fleisch, von Beeren.</ta>
            <ta e="T16" id="Seg_761" s="T13">Sie aßen Traubenkirschen.</ta>
            <ta e="T19" id="Seg_762" s="T16">Für den Winter machten sie Traubenkirschen haltbar.</ta>
            <ta e="T29" id="Seg_763" s="T19">Für den Winter füllten sie Körbe mit Traubenkirschen und gossen Fischöl darüber. </ta>
            <ta e="T33" id="Seg_764" s="T29">Es war ihr Winteressen.</ta>
            <ta e="T36" id="Seg_765" s="T33">Sie legten sie in die Scheune für den Winter.</ta>
            <ta e="T40" id="Seg_766" s="T36">Sie trockneten für gewöhnlich Elchfleisch.</ta>
            <ta e="T44" id="Seg_767" s="T40">Statt Brot aßen sie für gewöhnlich Trockenfleisch.</ta>
            <ta e="T48" id="Seg_768" s="T44">Sie trockneten auch Fisch.</ta>
            <ta e="T50" id="Seg_769" s="T48">Sie machten Fischmehl.</ta>
            <ta e="T54" id="Seg_770" s="T50">Für den Winter legten sie den Fisch in Körbe.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_771" s="T1">Раньше у селькупов хлеба не было.</ta>
            <ta e="T8" id="Seg_772" s="T5">Хлеб не ели.</ta>
            <ta e="T13" id="Seg_773" s="T8">На рыбе жили, на мясе жили, на ягоде.</ta>
            <ta e="T16" id="Seg_774" s="T13">Ели черемуху.</ta>
            <ta e="T19" id="Seg_775" s="T16">На зиму делали (запасали) черемуху.</ta>
            <ta e="T29" id="Seg_776" s="T19">В лукошко накладывали черемуху дополна и сверху заливали рыбьим жиром на зиму.</ta>
            <ta e="T33" id="Seg_777" s="T29">Зимнее питание это у них было.</ta>
            <ta e="T36" id="Seg_778" s="T33">Ставили на зиму в амбар.</ta>
            <ta e="T40" id="Seg_779" s="T36">И лосиное мясо сушили.</ta>
            <ta e="T44" id="Seg_780" s="T40">Вместо хлеба сушеное мясо ели.</ta>
            <ta e="T48" id="Seg_781" s="T44">И рыбу тоже сушили.</ta>
            <ta e="T50" id="Seg_782" s="T48">Высушенную рыбу делали.</ta>
            <ta e="T54" id="Seg_783" s="T50">Рыбу клали на зиму в лукошки.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_784" s="T1">раньше у остяков (по-низовски) хлеба не было</ta>
            <ta e="T8" id="Seg_785" s="T5">хлеб не ели</ta>
            <ta e="T13" id="Seg_786" s="T8">на рыбе жили на мясе жили на ягоде</ta>
            <ta e="T16" id="Seg_787" s="T13">ели черемуху</ta>
            <ta e="T19" id="Seg_788" s="T16">на зиму делали (запасали) черемуху</ta>
            <ta e="T29" id="Seg_789" s="T19">в лукошко накладывали черемуху полной и вверху заливали рыбьим жиром на зиму</ta>
            <ta e="T33" id="Seg_790" s="T29">(это) зимний хлеб (питание) у них был</ta>
            <ta e="T36" id="Seg_791" s="T33">ставили на зиму в амбар</ta>
            <ta e="T40" id="Seg_792" s="T36">лосиное мясо сушили</ta>
            <ta e="T44" id="Seg_793" s="T40">наместо хлеба сушеное мясо ели</ta>
            <ta e="T48" id="Seg_794" s="T44">из рыбы хлеб сушили</ta>
            <ta e="T50" id="Seg_795" s="T48">насушенную рыбу делали</ta>
            <ta e="T54" id="Seg_796" s="T50">рыбу в лукошко клали на зиму</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T5" id="Seg_797" s="T1">[KuAI:] Variant: 'tʼängumban'.</ta>
            <ta e="T29" id="Seg_798" s="T19">[KuAI:] Variant: 'kaːnɣoː'.</ta>
            <ta e="T36" id="Seg_799" s="T33">[KuAI:] Variant: 'kaːngoː'.</ta>
            <ta e="T54" id="Seg_800" s="T50">[KuAI:] Variant: 'kaːnɣoː'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T29" id="Seg_801" s="T19">′па̄рбыанд?</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
