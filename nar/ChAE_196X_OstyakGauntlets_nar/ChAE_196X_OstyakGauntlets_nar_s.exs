<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>ChAE_196X_OstyakGauntlets_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">ChAE_196X_OstyakGauntlets_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">33</ud-information>
            <ud-information attribute-name="# HIAT:w">25</ud-information>
            <ud-information attribute-name="# e">25</ud-information>
            <ud-information attribute-name="# HIAT:u">5</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="ChAE">
            <abbreviation>ChAE</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="ChAE"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T26" id="Seg_0" n="sc" s="T1">
               <ts e="T7" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Dʼädʼaw</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">äːʒulgus</ts>
                  <nts id="Seg_8" n="HIAT:ip">,</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_11" n="HIAT:w" s="T3">što</ts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_14" n="HIAT:w" s="T4">kulokaze</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_17" n="HIAT:w" s="T5">ass</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_20" n="HIAT:w" s="T6">tətämbɨkuzut</ts>
                  <nts id="Seg_21" n="HIAT:ip">.</nts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_24" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">Nodan</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_29" n="HIAT:w" s="T8">qoːbij</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_32" n="HIAT:w" s="T9">nobla</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_35" n="HIAT:w" s="T10">iːequzattə</ts>
                  <nts id="Seg_36" n="HIAT:ip">.</nts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_39" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_41" n="HIAT:w" s="T11">Wantowɨnt</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_44" n="HIAT:w" s="T12">qadonnal</ts>
                  <nts id="Seg_45" n="HIAT:ip">,</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_48" n="HIAT:w" s="T13">qawenʼǯiŋ</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_51" n="HIAT:w" s="T14">wandɨt</ts>
                  <nts id="Seg_52" n="HIAT:ip">.</nts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_55" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_57" n="HIAT:w" s="T15">Täp</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_60" n="HIAT:w" s="T16">tojelʼe</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_63" n="HIAT:w" s="T17">qwanǯiŋ</ts>
                  <nts id="Seg_64" n="HIAT:ip">.</nts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_67" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_69" n="HIAT:w" s="T18">Süsögula</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_72" n="HIAT:w" s="T19">warɨmbat</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_75" n="HIAT:w" s="T20">noːdən</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_78" n="HIAT:w" s="T21">qobiː</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_81" n="HIAT:w" s="T22">nobɨla</ts>
                  <nts id="Seg_82" n="HIAT:ip">,</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_85" n="HIAT:w" s="T23">qwäːgarɨn</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_88" n="HIAT:w" s="T24">qoːbiː</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_91" n="HIAT:w" s="T25">kalʼaška</ts>
                  <nts id="Seg_92" n="HIAT:ip">.</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T26" id="Seg_94" n="sc" s="T1">
               <ts e="T2" id="Seg_96" n="e" s="T1">Dʼädʼaw </ts>
               <ts e="T3" id="Seg_98" n="e" s="T2">äːʒulgus, </ts>
               <ts e="T4" id="Seg_100" n="e" s="T3">što </ts>
               <ts e="T5" id="Seg_102" n="e" s="T4">kulokaze </ts>
               <ts e="T6" id="Seg_104" n="e" s="T5">ass </ts>
               <ts e="T7" id="Seg_106" n="e" s="T6">tətämbɨkuzut. </ts>
               <ts e="T8" id="Seg_108" n="e" s="T7">Nodan </ts>
               <ts e="T9" id="Seg_110" n="e" s="T8">qoːbij </ts>
               <ts e="T10" id="Seg_112" n="e" s="T9">nobla </ts>
               <ts e="T11" id="Seg_114" n="e" s="T10">iːequzattə. </ts>
               <ts e="T12" id="Seg_116" n="e" s="T11">Wantowɨnt </ts>
               <ts e="T13" id="Seg_118" n="e" s="T12">qadonnal, </ts>
               <ts e="T14" id="Seg_120" n="e" s="T13">qawenʼǯiŋ </ts>
               <ts e="T15" id="Seg_122" n="e" s="T14">wandɨt. </ts>
               <ts e="T16" id="Seg_124" n="e" s="T15">Täp </ts>
               <ts e="T17" id="Seg_126" n="e" s="T16">tojelʼe </ts>
               <ts e="T18" id="Seg_128" n="e" s="T17">qwanǯiŋ. </ts>
               <ts e="T19" id="Seg_130" n="e" s="T18">Süsögula </ts>
               <ts e="T20" id="Seg_132" n="e" s="T19">warɨmbat </ts>
               <ts e="T21" id="Seg_134" n="e" s="T20">noːdən </ts>
               <ts e="T22" id="Seg_136" n="e" s="T21">qobiː </ts>
               <ts e="T23" id="Seg_138" n="e" s="T22">nobɨla, </ts>
               <ts e="T24" id="Seg_140" n="e" s="T23">qwäːgarɨn </ts>
               <ts e="T25" id="Seg_142" n="e" s="T24">qoːbiː </ts>
               <ts e="T26" id="Seg_144" n="e" s="T25">kalʼaška. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T7" id="Seg_145" s="T1">ChAE_196X_OstyakGauntlets_nar.001 (001.001)</ta>
            <ta e="T11" id="Seg_146" s="T7">ChAE_196X_OstyakGauntlets_nar.002 (001.002)</ta>
            <ta e="T15" id="Seg_147" s="T11">ChAE_196X_OstyakGauntlets_nar.003 (001.003)</ta>
            <ta e="T18" id="Seg_148" s="T15">ChAE_196X_OstyakGauntlets_nar.004 (001.004)</ta>
            <ta e="T26" id="Seg_149" s="T18">ChAE_196X_OstyakGauntlets_nar.005 (001.005)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T7" id="Seg_150" s="T1">′дʼӓдʼаw ′ӓ̄жулгус, што ку′локазе асс тъ(ӓ)тӓмбыку′зут.</ta>
            <ta e="T11" id="Seg_151" s="T7">′нодан kо̄′бий ноб′ла ′ӣе ку′заттъ.</ta>
            <ta e="T15" id="Seg_152" s="T11">ван′товынт ка′доннал, ка′венʼджиң ′вандыт.</ta>
            <ta e="T18" id="Seg_153" s="T15">тӓп ′тоjелʼе kwан′джиң.</ta>
            <ta e="T26" id="Seg_154" s="T18">′сӱсӧгула варым′бат ′но̄дъ(а)н kо′бӣ но′была, ′kwӓ̄гарын kо̄′бӣ ка′лʼашка.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T7" id="Seg_155" s="T1">dʼädʼaw äːʒulgus, što kulokaze ass tə(ä)tämbɨkuzut.</ta>
            <ta e="T11" id="Seg_156" s="T7">nodan qoːbij nobla iːe quzattə.</ta>
            <ta e="T15" id="Seg_157" s="T11">wantowɨnt qadonnal, qawenʼǯiŋ wandɨt.</ta>
            <ta e="T18" id="Seg_158" s="T15">täp tojelʼe qwanǯiŋ.</ta>
            <ta e="T26" id="Seg_159" s="T18">süsögula warɨmbat noːdə(a)n qobiː nobɨla, qwäːgarɨn qoːbiː kalʼaška.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T7" id="Seg_160" s="T1">Dʼädʼaw äːʒulgus, što kulokaze ass tətämbɨkuzut. </ta>
            <ta e="T11" id="Seg_161" s="T7">Nodan qoːbij nobla iːequzattə. </ta>
            <ta e="T15" id="Seg_162" s="T11">Wantowɨnt qadonnal, qawenʼǯiŋ wandɨt. </ta>
            <ta e="T18" id="Seg_163" s="T15">Täp tojelʼe qwanǯiŋ. </ta>
            <ta e="T26" id="Seg_164" s="T18">Süsögula warɨmbat noːdən qobiː nobɨla, qwäːgarɨn qoːbiː kalʼaška. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_165" s="T1">dʼädʼa-w</ta>
            <ta e="T3" id="Seg_166" s="T2">äːʒu-l-gu-s</ta>
            <ta e="T4" id="Seg_167" s="T3">što</ta>
            <ta e="T5" id="Seg_168" s="T4">kuloka-ze</ta>
            <ta e="T6" id="Seg_169" s="T5">ass</ta>
            <ta e="T7" id="Seg_170" s="T6">tətä-mbɨ-ku-z-ut</ta>
            <ta e="T8" id="Seg_171" s="T7">nodan</ta>
            <ta e="T9" id="Seg_172" s="T8">qoːb-i-j</ta>
            <ta e="T10" id="Seg_173" s="T9">nob-la</ta>
            <ta e="T11" id="Seg_174" s="T10">iːe-qu-za-ttə</ta>
            <ta e="T12" id="Seg_175" s="T11">wanto-wɨn-t</ta>
            <ta e="T13" id="Seg_176" s="T12">qad-on-na-l</ta>
            <ta e="T14" id="Seg_177" s="T13">qaw-enʼǯi-ŋ</ta>
            <ta e="T15" id="Seg_178" s="T14">wandɨ-t</ta>
            <ta e="T16" id="Seg_179" s="T15">täp</ta>
            <ta e="T17" id="Seg_180" s="T16">to-jelʼe</ta>
            <ta e="T18" id="Seg_181" s="T17">qwan-ǯi-ŋ</ta>
            <ta e="T19" id="Seg_182" s="T18">süsögu-la</ta>
            <ta e="T20" id="Seg_183" s="T19">warɨ-mba-t</ta>
            <ta e="T21" id="Seg_184" s="T20">noːdən</ta>
            <ta e="T22" id="Seg_185" s="T21">qob-iː</ta>
            <ta e="T23" id="Seg_186" s="T22">nob-ɨ-la</ta>
            <ta e="T24" id="Seg_187" s="T23">qwäːgar-ɨ-n</ta>
            <ta e="T25" id="Seg_188" s="T24">qoːb-iː</ta>
            <ta e="T26" id="Seg_189" s="T25">kalʼaška</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_190" s="T1">dʼaja-w</ta>
            <ta e="T3" id="Seg_191" s="T2">əǯu-l-ku-sɨ</ta>
            <ta e="T4" id="Seg_192" s="T3">što</ta>
            <ta e="T5" id="Seg_193" s="T4">kuloka-se</ta>
            <ta e="T6" id="Seg_194" s="T5">asa</ta>
            <ta e="T7" id="Seg_195" s="T6">tɨtdɨ-mbɨ-ku-sɨ-un</ta>
            <ta e="T8" id="Seg_196" s="T7">noːdɨn</ta>
            <ta e="T9" id="Seg_197" s="T8">qob-ɨ-lʼ</ta>
            <ta e="T10" id="Seg_198" s="T9">nob-la</ta>
            <ta e="T11" id="Seg_199" s="T10">eː-ku-sɨ-tɨn</ta>
            <ta e="T12" id="Seg_200" s="T11">wandǝ-un-ntɨ</ta>
            <ta e="T13" id="Seg_201" s="T12">qadə-ol-nɨ-l</ta>
            <ta e="T14" id="Seg_202" s="T13">qam-enǯɨ-n</ta>
            <ta e="T15" id="Seg_203" s="T14">wandǝ-tə</ta>
            <ta e="T16" id="Seg_204" s="T15">täp</ta>
            <ta e="T17" id="Seg_205" s="T16">to-jelʼe</ta>
            <ta e="T18" id="Seg_206" s="T17">qwan-enǯɨ-n</ta>
            <ta e="T19" id="Seg_207" s="T18">süsögum-la</ta>
            <ta e="T20" id="Seg_208" s="T19">warɨ-mbɨ-tɨn</ta>
            <ta e="T21" id="Seg_209" s="T20">noːdɨn</ta>
            <ta e="T22" id="Seg_210" s="T21">qob-lʼ</ta>
            <ta e="T23" id="Seg_211" s="T22">nob-ɨ-la</ta>
            <ta e="T24" id="Seg_212" s="T23">qweɣər-ɨ-n</ta>
            <ta e="T25" id="Seg_213" s="T24">qob-lʼ</ta>
            <ta e="T26" id="Seg_214" s="T25">kalʼaška</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_215" s="T1">uncle.[NOM]-1SG</ta>
            <ta e="T3" id="Seg_216" s="T2">say-DRV-HAB-PST.[3SG.S]</ta>
            <ta e="T4" id="Seg_217" s="T3">that</ta>
            <ta e="T5" id="Seg_218" s="T4">fist-COM</ta>
            <ta e="T6" id="Seg_219" s="T5">NEG</ta>
            <ta e="T7" id="Seg_220" s="T6">scuffle-DUR-HAB-PST-1PL</ta>
            <ta e="T8" id="Seg_221" s="T7">sterlet.[NOM]</ta>
            <ta e="T9" id="Seg_222" s="T8">skin-EP-ADJZ</ta>
            <ta e="T10" id="Seg_223" s="T9">mitten-PL.[NOM]</ta>
            <ta e="T11" id="Seg_224" s="T10">be-HAB-PST-3PL</ta>
            <ta e="T12" id="Seg_225" s="T11">face-PROL-OBL.3SG</ta>
            <ta e="T13" id="Seg_226" s="T12">scratch-MOM-CO-2SG.O</ta>
            <ta e="T14" id="Seg_227" s="T13">%bleed-FUT-3SG.S</ta>
            <ta e="T15" id="Seg_228" s="T14">face.[NOM]-3SG</ta>
            <ta e="T16" id="Seg_229" s="T15">(s)he.[NOM]</ta>
            <ta e="T17" id="Seg_230" s="T16">away-%%</ta>
            <ta e="T18" id="Seg_231" s="T17">run.away-FUT-3SG.S</ta>
            <ta e="T19" id="Seg_232" s="T18">Selkup-PL.[NOM]</ta>
            <ta e="T20" id="Seg_233" s="T19">wear-PST.NAR-3PL</ta>
            <ta e="T21" id="Seg_234" s="T20">sterlet.[NOM]</ta>
            <ta e="T22" id="Seg_235" s="T21">skin-ADJZ</ta>
            <ta e="T23" id="Seg_236" s="T22">mitten-EP-PL.[NOM]</ta>
            <ta e="T24" id="Seg_237" s="T23">sturgeon-EP-GEN</ta>
            <ta e="T25" id="Seg_238" s="T24">skin-ADJZ</ta>
            <ta e="T26" id="Seg_239" s="T25">bootleg.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_240" s="T1">дядя.[NOM]-1SG</ta>
            <ta e="T3" id="Seg_241" s="T2">сказать-DRV-HAB-PST.[3SG.S]</ta>
            <ta e="T4" id="Seg_242" s="T3">что</ta>
            <ta e="T5" id="Seg_243" s="T4">кулак-COM</ta>
            <ta e="T6" id="Seg_244" s="T5">NEG</ta>
            <ta e="T7" id="Seg_245" s="T6">подраться-DUR-HAB-PST-1PL</ta>
            <ta e="T8" id="Seg_246" s="T7">стерлядь.[NOM]</ta>
            <ta e="T9" id="Seg_247" s="T8">шкура-EP-ADJZ</ta>
            <ta e="T10" id="Seg_248" s="T9">рукавица-PL.[NOM]</ta>
            <ta e="T11" id="Seg_249" s="T10">быть-HAB-PST-3PL</ta>
            <ta e="T12" id="Seg_250" s="T11">лицо-PROL-OBL.3SG</ta>
            <ta e="T13" id="Seg_251" s="T12">поцарапать-MOM-CO-2SG.O</ta>
            <ta e="T14" id="Seg_252" s="T13">%кровить-FUT-3SG.S</ta>
            <ta e="T15" id="Seg_253" s="T14">лицо.[NOM]-3SG</ta>
            <ta e="T16" id="Seg_254" s="T15">он(а).[NOM]</ta>
            <ta e="T17" id="Seg_255" s="T16">прочь-%%</ta>
            <ta e="T18" id="Seg_256" s="T17">убежать-FUT-3SG.S</ta>
            <ta e="T19" id="Seg_257" s="T18">селькуп-PL.[NOM]</ta>
            <ta e="T20" id="Seg_258" s="T19">носить-PST.NAR-3PL</ta>
            <ta e="T21" id="Seg_259" s="T20">стерлядь.[NOM]</ta>
            <ta e="T22" id="Seg_260" s="T21">шкура-ADJZ</ta>
            <ta e="T23" id="Seg_261" s="T22">рукавица-EP-PL.[NOM]</ta>
            <ta e="T24" id="Seg_262" s="T23">осётр-EP-GEN</ta>
            <ta e="T25" id="Seg_263" s="T24">шкура-ADJZ</ta>
            <ta e="T26" id="Seg_264" s="T25">голенище.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_265" s="T1">n.[n:case]-n:poss</ta>
            <ta e="T3" id="Seg_266" s="T2">v-v&gt;v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T4" id="Seg_267" s="T3">conj</ta>
            <ta e="T5" id="Seg_268" s="T4">n-n:case</ta>
            <ta e="T6" id="Seg_269" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_270" s="T6">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_271" s="T7">n.[n:case]</ta>
            <ta e="T9" id="Seg_272" s="T8">n-n:ins-n&gt;adj</ta>
            <ta e="T10" id="Seg_273" s="T9">n-n:num.[n:case]</ta>
            <ta e="T11" id="Seg_274" s="T10">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T12" id="Seg_275" s="T11">n-n:case-n:obl.poss</ta>
            <ta e="T13" id="Seg_276" s="T12">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T14" id="Seg_277" s="T13">v-v:tense-v:pn</ta>
            <ta e="T15" id="Seg_278" s="T14">n.[n:case]-n:poss</ta>
            <ta e="T16" id="Seg_279" s="T15">pers.[n:case]</ta>
            <ta e="T17" id="Seg_280" s="T16">preverb-%%</ta>
            <ta e="T18" id="Seg_281" s="T17">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_282" s="T18">n-n:num.[n:case]</ta>
            <ta e="T20" id="Seg_283" s="T19">v-v:tense-n:poss</ta>
            <ta e="T21" id="Seg_284" s="T20">n.[n:case]</ta>
            <ta e="T22" id="Seg_285" s="T21">n-n&gt;adj</ta>
            <ta e="T23" id="Seg_286" s="T22">n-n:ins-n:num.[n:case]</ta>
            <ta e="T24" id="Seg_287" s="T23">n-n:ins-n:case</ta>
            <ta e="T25" id="Seg_288" s="T24">n-n&gt;adj</ta>
            <ta e="T26" id="Seg_289" s="T25">n.[n:case]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_290" s="T1">n</ta>
            <ta e="T3" id="Seg_291" s="T2">v</ta>
            <ta e="T4" id="Seg_292" s="T3">conj</ta>
            <ta e="T5" id="Seg_293" s="T4">n</ta>
            <ta e="T6" id="Seg_294" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_295" s="T6">v</ta>
            <ta e="T8" id="Seg_296" s="T7">n</ta>
            <ta e="T9" id="Seg_297" s="T8">adj</ta>
            <ta e="T10" id="Seg_298" s="T9">n</ta>
            <ta e="T11" id="Seg_299" s="T10">v</ta>
            <ta e="T12" id="Seg_300" s="T11">n</ta>
            <ta e="T13" id="Seg_301" s="T12">v</ta>
            <ta e="T14" id="Seg_302" s="T13">v</ta>
            <ta e="T15" id="Seg_303" s="T14">n</ta>
            <ta e="T16" id="Seg_304" s="T15">pers</ta>
            <ta e="T17" id="Seg_305" s="T16">preverb</ta>
            <ta e="T18" id="Seg_306" s="T17">v</ta>
            <ta e="T19" id="Seg_307" s="T18">n</ta>
            <ta e="T20" id="Seg_308" s="T19">v</ta>
            <ta e="T21" id="Seg_309" s="T20">n</ta>
            <ta e="T22" id="Seg_310" s="T21">adj</ta>
            <ta e="T23" id="Seg_311" s="T22">n</ta>
            <ta e="T24" id="Seg_312" s="T23">n</ta>
            <ta e="T25" id="Seg_313" s="T24">adj</ta>
            <ta e="T26" id="Seg_314" s="T25">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_315" s="T1">np.h:A 0.1.h:Poss</ta>
            <ta e="T5" id="Seg_316" s="T4">np:Ins</ta>
            <ta e="T7" id="Seg_317" s="T6">0.1.h:A</ta>
            <ta e="T10" id="Seg_318" s="T9">np:Th</ta>
            <ta e="T12" id="Seg_319" s="T11">np:Path</ta>
            <ta e="T13" id="Seg_320" s="T12">0.2.h:A</ta>
            <ta e="T15" id="Seg_321" s="T14">np:Th</ta>
            <ta e="T16" id="Seg_322" s="T15">pro.h:A</ta>
            <ta e="T19" id="Seg_323" s="T18">np.h:A</ta>
            <ta e="T23" id="Seg_324" s="T22">np:Th</ta>
            <ta e="T24" id="Seg_325" s="T23">np:Poss</ta>
            <ta e="T26" id="Seg_326" s="T25">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_327" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_328" s="T2">v:pred</ta>
            <ta e="T7" id="Seg_329" s="T3">s:compl</ta>
            <ta e="T10" id="Seg_330" s="T9">np:S</ta>
            <ta e="T11" id="Seg_331" s="T10">v:pred</ta>
            <ta e="T13" id="Seg_332" s="T12">0.2.h:S v:pred</ta>
            <ta e="T14" id="Seg_333" s="T13">v:pred</ta>
            <ta e="T15" id="Seg_334" s="T14">np:S</ta>
            <ta e="T16" id="Seg_335" s="T15">pro.h:S</ta>
            <ta e="T18" id="Seg_336" s="T17">v:pred</ta>
            <ta e="T19" id="Seg_337" s="T18">np.h:S</ta>
            <ta e="T20" id="Seg_338" s="T19">v:pred</ta>
            <ta e="T23" id="Seg_339" s="T22">np:O</ta>
            <ta e="T26" id="Seg_340" s="T25">np:O</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_341" s="T1">RUS:cult</ta>
            <ta e="T4" id="Seg_342" s="T3">RUS:gram</ta>
            <ta e="T5" id="Seg_343" s="T4">RUS:cult</ta>
            <ta e="T26" id="Seg_344" s="T25">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T7" id="Seg_345" s="T1">My uncle said, that earlier we didn't use fists when fighting.</ta>
            <ta e="T11" id="Seg_346" s="T7">There were mittens made of sterlet skin.</ta>
            <ta e="T15" id="Seg_347" s="T11">If you scratch the face – it will bleed.</ta>
            <ta e="T18" id="Seg_348" s="T15">He will step aside.</ta>
            <ta e="T26" id="Seg_349" s="T18">Selkups used to wear mittens made of sterlet skin and bootlegs made of sturgeon skin.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T7" id="Seg_350" s="T1">Mein Onkel sagte, dass wir früher nicht mit Fäusten kämpften.</ta>
            <ta e="T11" id="Seg_351" s="T7">Es gab Fäustlinge aus Sterlethaut.</ta>
            <ta e="T15" id="Seg_352" s="T11">Wenn du am Gesicht entlangschrammst - wird es bluten.</ta>
            <ta e="T18" id="Seg_353" s="T15">Er wird zur Seite gehen.</ta>
            <ta e="T26" id="Seg_354" s="T18">Selkupen trugen für gewöhnlich Fäustlinge aus Sterlethaut und Stiefelschäfte aus Störhaut.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T7" id="Seg_355" s="T1">Моя дядя говорил, что [раньше] мы кулаками не дрались.</ta>
            <ta e="T11" id="Seg_356" s="T7">Рукавички из стерляжьей кожи были.</ta>
            <ta e="T15" id="Seg_357" s="T11">По лицу царапнешь – кровь с лица побежит.</ta>
            <ta e="T18" id="Seg_358" s="T15">Он и отойдет (подальше).</ta>
            <ta e="T26" id="Seg_359" s="T18">Селькупы носили рукавицы из стерляжей кожи, голенище из осетриной кожи.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T7" id="Seg_360" s="T1">дядя говорил что раньше мы кулаками не дрались</ta>
            <ta e="T11" id="Seg_361" s="T7">рукавичкой стерляжей были</ta>
            <ta e="T15" id="Seg_362" s="T11">по лицу царапнешь кровь с лица побежит</ta>
            <ta e="T18" id="Seg_363" s="T15">он и отойдет (подальше)</ta>
            <ta e="T26" id="Seg_364" s="T18">остяки носили из стерляжей кожи рукавицы из осетриной кожи голенище</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T7" id="Seg_365" s="T1">[KuAI:] Variant: 'tätämbɨkuzut'.</ta>
            <ta e="T11" id="Seg_366" s="T7">[BrM:] 'iːe quzattə' changed to 'iːequzattə'. 'nodan' – GEN?</ta>
            <ta e="T26" id="Seg_367" s="T18">[KuAI:] Variant: 'noːdan'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
