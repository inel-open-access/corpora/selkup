<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>ChAE_196X_PickingBerries_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">ChAE_196X_PickingBerries_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">264</ud-information>
            <ud-information attribute-name="# HIAT:w">202</ud-information>
            <ud-information attribute-name="# e">202</ud-information>
            <ud-information attribute-name="# HIAT:u">38</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="ChAE">
            <abbreviation>ChAE</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="ChAE"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T203" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Naːgurtottə</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">qwassutt</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">čobɨrɨm</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">wadɨgu</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">Palʼdizut</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">i</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">qaj</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">čobɨrɨm</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">ass</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_34" n="HIAT:w" s="T10">koɣutt</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_38" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">Okkɨr</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">najɣum</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">qwess</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_49" n="HIAT:w" s="T14">parannɨŋ</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_52" n="HIAT:w" s="T15">jeːdent</ts>
                  <nts id="Seg_53" n="HIAT:ip">.</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_56" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">A</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_61" n="HIAT:w" s="T17">me</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_64" n="HIAT:w" s="T18">sɨtielʼi</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_67" n="HIAT:w" s="T19">qwannaj</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_70" n="HIAT:w" s="T20">toːlak</ts>
                  <nts id="Seg_71" n="HIAT:ip">.</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_74" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_76" n="HIAT:w" s="T21">Toːɣalʼe</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_79" n="HIAT:w" s="T22">qwalʼlʼebe</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_82" n="HIAT:w" s="T23">čobɨrɨm</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_85" n="HIAT:w" s="T24">kowaj</ts>
                  <nts id="Seg_86" n="HIAT:ip">,</nts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_89" n="HIAT:w" s="T25">čoborɨm</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_92" n="HIAT:w" s="T26">wataj</ts>
                  <nts id="Seg_93" n="HIAT:ip">,</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_96" n="HIAT:w" s="T27">qaptäm</ts>
                  <nts id="Seg_97" n="HIAT:ip">.</nts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_100" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_102" n="HIAT:w" s="T28">Jeːdaɣɨndo</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_105" n="HIAT:w" s="T29">qarʼe</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_108" n="HIAT:w" s="T30">tʼütʼöuzot</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_111" n="HIAT:w" s="T31">nagurtot</ts>
                  <nts id="Seg_112" n="HIAT:ip">,</nts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_115" n="HIAT:w" s="T32">kɨba</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_118" n="HIAT:w" s="T33">andoka</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_121" n="HIAT:w" s="T34">omdɨzot</ts>
                  <nts id="Seg_122" n="HIAT:ip">.</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_125" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_127" n="HIAT:w" s="T35">Andɨze</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_130" n="HIAT:w" s="T36">čaːǯizot</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_132" n="HIAT:ip">(</nts>
                  <ts e="T38" id="Seg_134" n="HIAT:w" s="T37">čaʒizottə</ts>
                  <nts id="Seg_135" n="HIAT:ip">)</nts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_138" n="HIAT:w" s="T38">Staraj</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_141" n="HIAT:w" s="T39">jedonǯan</ts>
                  <nts id="Seg_142" n="HIAT:ip">.</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_145" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_147" n="HIAT:w" s="T40">Wattoɣən</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_150" n="HIAT:w" s="T41">čagɨmbɨs</ts>
                  <nts id="Seg_151" n="HIAT:ip">.</nts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_154" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_156" n="HIAT:w" s="T42">Nogorlʼe</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_159" n="HIAT:w" s="T43">čaǯizottə</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_162" n="HIAT:w" s="T44">labɨze</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_165" n="HIAT:w" s="T45">nogotčilʼe</ts>
                  <nts id="Seg_166" n="HIAT:ip">.</nts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_169" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_171" n="HIAT:w" s="T46">Okɨr</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_174" n="HIAT:w" s="T47">mestaɣɨn</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_177" n="HIAT:w" s="T48">kwese</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_180" n="HIAT:w" s="T49">paralguzot</ts>
                  <nts id="Seg_181" n="HIAT:ip">.</nts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_184" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_186" n="HIAT:w" s="T50">Kwese</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_189" n="HIAT:w" s="T51">paralʼlʼibe</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_192" n="HIAT:w" s="T52">uːtərɨzottə</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_195" n="HIAT:w" s="T53">kän</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_198" n="HIAT:w" s="T54">baront</ts>
                  <nts id="Seg_199" n="HIAT:ip">.</nts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_202" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_204" n="HIAT:w" s="T55">Konnä</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_207" n="HIAT:w" s="T56">čaːnǯizottə</ts>
                  <nts id="Seg_208" n="HIAT:ip">.</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_211" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_213" n="HIAT:w" s="T57">Nʼäŋɣont</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_216" n="HIAT:w" s="T58">top</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_219" n="HIAT:w" s="T59">tokedikun</ts>
                  <nts id="Seg_220" n="HIAT:ip">,</nts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_223" n="HIAT:w" s="T60">näŋk</ts>
                  <nts id="Seg_224" n="HIAT:ip">.</nts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_227" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_229" n="HIAT:w" s="T61">Okkɨr</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_232" n="HIAT:w" s="T62">näjɣum</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_235" n="HIAT:w" s="T63">ugon</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_238" n="HIAT:w" s="T64">čanǯis</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_241" n="HIAT:w" s="T65">kän</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_244" n="HIAT:w" s="T66">baːrond</ts>
                  <nts id="Seg_245" n="HIAT:ip">.</nts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_248" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_250" n="HIAT:w" s="T67">Natedaɣɨndoː</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_253" n="HIAT:w" s="T68">poːm</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_255" n="HIAT:ip">(</nts>
                  <ts e="T70" id="Seg_257" n="HIAT:w" s="T69">polam</ts>
                  <nts id="Seg_258" n="HIAT:ip">)</nts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_261" n="HIAT:w" s="T70">qäsat</ts>
                  <nts id="Seg_262" n="HIAT:ip">.</nts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_265" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_267" n="HIAT:w" s="T71">Me</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_270" n="HIAT:w" s="T72">na</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_273" n="HIAT:w" s="T73">poːn</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_276" n="HIAT:w" s="T74">barmɨndo</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_279" n="HIAT:w" s="T75">čanǯizaj</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_282" n="HIAT:w" s="T76">konnä</ts>
                  <nts id="Seg_283" n="HIAT:ip">.</nts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_286" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_288" n="HIAT:w" s="T77">Konnä</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_291" n="HIAT:w" s="T78">čaːnǯilʼepe</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_294" n="HIAT:w" s="T79">qaptäno</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_297" n="HIAT:w" s="T80">wassutə</ts>
                  <nts id="Seg_298" n="HIAT:ip">,</nts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_301" n="HIAT:w" s="T81">qaptäm</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_304" n="HIAT:w" s="T82">wadəgu</ts>
                  <nts id="Seg_305" n="HIAT:ip">.</nts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_308" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_310" n="HIAT:w" s="T83">I</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_313" n="HIAT:w" s="T84">čoram</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_316" n="HIAT:w" s="T85">poǯimbɨlʼe</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_319" n="HIAT:w" s="T86">i</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_322" n="HIAT:w" s="T87">qaptäm</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_325" n="HIAT:w" s="T88">ass</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_328" n="HIAT:w" s="T89">kozut</ts>
                  <nts id="Seg_329" n="HIAT:ip">.</nts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_332" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_334" n="HIAT:w" s="T90">Watond</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_337" n="HIAT:w" s="T91">qweːse</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_340" n="HIAT:w" s="T92">čaːnǯesut</ts>
                  <nts id="Seg_341" n="HIAT:ip">.</nts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_344" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_346" n="HIAT:w" s="T93">Okɨr</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_349" n="HIAT:w" s="T94">näjqum</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_352" n="HIAT:w" s="T95">kweːsse</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_355" n="HIAT:w" s="T96">paːrass</ts>
                  <nts id="Seg_356" n="HIAT:ip">.</nts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_359" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_361" n="HIAT:w" s="T97">Vera</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_364" n="HIAT:w" s="T98">Demidowna</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_367" n="HIAT:w" s="T99">ass</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_370" n="HIAT:w" s="T100">kɨgelɨŋ</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_373" n="HIAT:w" s="T101">čoburi</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_376" n="HIAT:w" s="T102">wadəndigu</ts>
                  <nts id="Seg_377" n="HIAT:ip">.</nts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_380" n="HIAT:u" s="T103">
                  <nts id="Seg_381" n="HIAT:ip">“</nts>
                  <ts e="T104" id="Seg_383" n="HIAT:w" s="T103">Man</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_386" n="HIAT:w" s="T104">tobow</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_389" n="HIAT:w" s="T105">küzɨnd</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_392" n="HIAT:w" s="T106">palʼdʼugu</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_395" n="HIAT:w" s="T107">čoram</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_398" n="HIAT:w" s="T108">poʒɨmbɨgu</ts>
                  <nts id="Seg_399" n="HIAT:ip">.</nts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T117" id="Seg_402" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_404" n="HIAT:w" s="T109">Kwese</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_407" n="HIAT:w" s="T110">qwanǯaŋ</ts>
                  <nts id="Seg_408" n="HIAT:ip">,</nts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_411" n="HIAT:w" s="T111">ato</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_414" n="HIAT:w" s="T112">qardʼel</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_417" n="HIAT:w" s="T113">i</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_420" n="HIAT:w" s="T114">tobond</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_423" n="HIAT:w" s="T115">ass</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_426" n="HIAT:w" s="T116">nɨlʼedʼänanǯ</ts>
                  <nts id="Seg_427" n="HIAT:ip">.</nts>
                  <nts id="Seg_428" n="HIAT:ip">”</nts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_431" n="HIAT:u" s="T117">
                  <ts e="T118" id="Seg_433" n="HIAT:w" s="T117">No</ts>
                  <nts id="Seg_434" n="HIAT:ip">,</nts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_437" n="HIAT:w" s="T118">a</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_440" n="HIAT:w" s="T119">me</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_443" n="HIAT:w" s="T120">äːʒulgaj</ts>
                  <nts id="Seg_444" n="HIAT:ip">:</nts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_446" n="HIAT:ip">“</nts>
                  <ts e="T122" id="Seg_448" n="HIAT:w" s="T121">Čaǯik</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_451" n="HIAT:w" s="T122">kwese</ts>
                  <nts id="Seg_452" n="HIAT:ip">.</nts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T132" id="Seg_455" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_457" n="HIAT:w" s="T123">Me</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_460" n="HIAT:w" s="T124">oni</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_463" n="HIAT:w" s="T125">köːse</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_466" n="HIAT:w" s="T126">kwalajse</ts>
                  <nts id="Seg_467" n="HIAT:ip">,</nts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_470" n="HIAT:w" s="T127">natin</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_473" n="HIAT:w" s="T128">čobɨr</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_476" n="HIAT:w" s="T129">jeŋ</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_479" n="HIAT:w" s="T130">alʼi</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_482" n="HIAT:w" s="T131">tʼäŋwa</ts>
                  <nts id="Seg_483" n="HIAT:ip">.</nts>
                  <nts id="Seg_484" n="HIAT:ip">”</nts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_487" n="HIAT:u" s="T132">
                  <ts e="T133" id="Seg_489" n="HIAT:w" s="T132">Qwanaj</ts>
                  <nts id="Seg_490" n="HIAT:ip">.</nts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T138" id="Seg_493" n="HIAT:u" s="T133">
                  <ts e="T134" id="Seg_495" n="HIAT:w" s="T133">Čobɨrɨm</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_498" n="HIAT:w" s="T134">qowaj</ts>
                  <nts id="Seg_499" n="HIAT:ip">,</nts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_502" n="HIAT:w" s="T135">čobɨrɨm</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_505" n="HIAT:w" s="T136">wadaj</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_508" n="HIAT:w" s="T137">tʼiːssaŋ</ts>
                  <nts id="Seg_509" n="HIAT:ip">.</nts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_512" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_514" n="HIAT:w" s="T138">Sojen</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_517" n="HIAT:w" s="T139">tʼeres</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_520" n="HIAT:w" s="T140">pitʼöuzaj</ts>
                  <nts id="Seg_521" n="HIAT:ip">,</nts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_524" n="HIAT:w" s="T141">uːdə</ts>
                  <nts id="Seg_525" n="HIAT:ip">,</nts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_528" n="HIAT:w" s="T142">saːtɨrlʼe</ts>
                  <nts id="Seg_529" n="HIAT:ip">,</nts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_532" n="HIAT:w" s="T143">pöum</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_535" n="HIAT:w" s="T144">nimgelʼǯilʼebe</ts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_537" n="HIAT:ip">(</nts>
                  <ts e="T146" id="Seg_539" n="HIAT:w" s="T145">pöulam</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_542" n="HIAT:w" s="T146">nigelʼǯilʼe</ts>
                  <nts id="Seg_543" n="HIAT:ip">)</nts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_546" n="HIAT:w" s="T147">nelʼdʼä</ts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_549" n="HIAT:w" s="T148">topɨn</ts>
                  <nts id="Seg_550" n="HIAT:ip">.</nts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_553" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_555" n="HIAT:w" s="T149">Au</ts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_558" n="HIAT:w" s="T150">plʼeqant</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_561" n="HIAT:w" s="T151">petʼtʼowlʼe</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_564" n="HIAT:w" s="T152">pöwlam</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_567" n="HIAT:w" s="T153">sersaj</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_570" n="HIAT:w" s="T154">i</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_573" n="HIAT:w" s="T155">natʼe</ts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_576" n="HIAT:w" s="T156">tɨɣɨndoq</ts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_579" n="HIAT:w" s="T157">aj</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_582" n="HIAT:w" s="T158">peulam</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_585" n="HIAT:w" s="T159">ningelʼǯiquzaj</ts>
                  <nts id="Seg_586" n="HIAT:ip">.</nts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_589" n="HIAT:u" s="T160">
                  <ts e="T161" id="Seg_591" n="HIAT:w" s="T160">Sədə</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_594" n="HIAT:w" s="T161">paːr</ts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_597" n="HIAT:w" s="T162">sadɨrlʼe</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_600" n="HIAT:w" s="T163">pitʼöqwaj</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_603" n="HIAT:w" s="T164">qonnä</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_606" n="HIAT:w" s="T165">čanǯaj</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_609" n="HIAT:w" s="T166">nʼaŋgɨn</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_612" n="HIAT:w" s="T167">paroɣɨn</ts>
                  <nts id="Seg_613" n="HIAT:ip">.</nts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T171" id="Seg_616" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_618" n="HIAT:w" s="T168">Pöwow</ts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_621" n="HIAT:w" s="T169">nʼäŋgont</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_624" n="HIAT:w" s="T170">toquatquŋ</ts>
                  <nts id="Seg_625" n="HIAT:ip">.</nts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T177" id="Seg_628" n="HIAT:u" s="T171">
                  <ts e="T172" id="Seg_630" n="HIAT:w" s="T171">Man</ts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_633" n="HIAT:w" s="T172">uːdoze</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_636" n="HIAT:w" s="T173">ügolgow</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_639" n="HIAT:w" s="T174">aj</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_642" n="HIAT:w" s="T175">toboɣan</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_645" n="HIAT:w" s="T176">taqalʼčeqow</ts>
                  <nts id="Seg_646" n="HIAT:ip">.</nts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T186" id="Seg_649" n="HIAT:u" s="T177">
                  <ts e="T178" id="Seg_651" n="HIAT:w" s="T177">Aːw</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_654" n="HIAT:w" s="T178">tobow</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_657" n="HIAT:w" s="T179">toqowatqont</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_660" n="HIAT:w" s="T180">nʼäŋgont</ts>
                  <nts id="Seg_661" n="HIAT:ip">,</nts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_664" n="HIAT:w" s="T181">opʼät</ts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_667" n="HIAT:w" s="T182">aw</ts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_670" n="HIAT:w" s="T183">pöwow</ts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_673" n="HIAT:w" s="T184">üːgolguw</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_676" n="HIAT:w" s="T185">udɨze</ts>
                  <nts id="Seg_677" n="HIAT:ip">.</nts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T189" id="Seg_680" n="HIAT:u" s="T186">
                  <ts e="T187" id="Seg_682" n="HIAT:w" s="T186">Aj</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_685" n="HIAT:w" s="T187">toboɣan</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_688" n="HIAT:w" s="T188">togalʼǯiqow</ts>
                  <nts id="Seg_689" n="HIAT:ip">.</nts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T192" id="Seg_692" n="HIAT:u" s="T189">
                  <ts e="T190" id="Seg_694" n="HIAT:w" s="T189">Niŋi</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_697" n="HIAT:w" s="T190">qonnä</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_700" n="HIAT:w" s="T191">čanǯaj</ts>
                  <nts id="Seg_701" n="HIAT:ip">.</nts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T196" id="Seg_704" n="HIAT:u" s="T192">
                  <ts e="T193" id="Seg_706" n="HIAT:w" s="T192">Watond</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_709" n="HIAT:w" s="T193">nʼüʒɨm</ts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_712" n="HIAT:w" s="T194">poǯimbɨlʼe</ts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_715" n="HIAT:w" s="T195">čanǯaj</ts>
                  <nts id="Seg_716" n="HIAT:ip">.</nts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T199" id="Seg_719" n="HIAT:u" s="T196">
                  <ts e="T197" id="Seg_721" n="HIAT:w" s="T196">Na</ts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_724" n="HIAT:w" s="T197">saŋ</ts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_727" n="HIAT:w" s="T198">nunɨdʼäj</ts>
                  <nts id="Seg_728" n="HIAT:ip">.</nts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T203" id="Seg_731" n="HIAT:u" s="T199">
                  <ts e="T200" id="Seg_733" n="HIAT:w" s="T199">Qaq</ts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_736" n="HIAT:w" s="T200">serbɨlʼe</ts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_739" n="HIAT:w" s="T201">maːt</ts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_742" n="HIAT:w" s="T202">tüaj</ts>
                  <nts id="Seg_743" n="HIAT:ip">.</nts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T203" id="Seg_745" n="sc" s="T1">
               <ts e="T2" id="Seg_747" n="e" s="T1">Naːgurtottə </ts>
               <ts e="T3" id="Seg_749" n="e" s="T2">qwassutt </ts>
               <ts e="T4" id="Seg_751" n="e" s="T3">čobɨrɨm </ts>
               <ts e="T5" id="Seg_753" n="e" s="T4">wadɨgu. </ts>
               <ts e="T6" id="Seg_755" n="e" s="T5">Palʼdizut </ts>
               <ts e="T7" id="Seg_757" n="e" s="T6">i </ts>
               <ts e="T8" id="Seg_759" n="e" s="T7">qaj </ts>
               <ts e="T9" id="Seg_761" n="e" s="T8">čobɨrɨm </ts>
               <ts e="T10" id="Seg_763" n="e" s="T9">ass </ts>
               <ts e="T11" id="Seg_765" n="e" s="T10">koɣutt. </ts>
               <ts e="T12" id="Seg_767" n="e" s="T11">Okkɨr </ts>
               <ts e="T13" id="Seg_769" n="e" s="T12">najɣum </ts>
               <ts e="T14" id="Seg_771" n="e" s="T13">qwess </ts>
               <ts e="T15" id="Seg_773" n="e" s="T14">parannɨŋ </ts>
               <ts e="T16" id="Seg_775" n="e" s="T15">jeːdent. </ts>
               <ts e="T17" id="Seg_777" n="e" s="T16">A </ts>
               <ts e="T18" id="Seg_779" n="e" s="T17">me </ts>
               <ts e="T19" id="Seg_781" n="e" s="T18">sɨtielʼi </ts>
               <ts e="T20" id="Seg_783" n="e" s="T19">qwannaj </ts>
               <ts e="T21" id="Seg_785" n="e" s="T20">toːlak. </ts>
               <ts e="T22" id="Seg_787" n="e" s="T21">Toːɣalʼe </ts>
               <ts e="T23" id="Seg_789" n="e" s="T22">qwalʼlʼebe </ts>
               <ts e="T24" id="Seg_791" n="e" s="T23">čobɨrɨm </ts>
               <ts e="T25" id="Seg_793" n="e" s="T24">kowaj, </ts>
               <ts e="T26" id="Seg_795" n="e" s="T25">čoborɨm </ts>
               <ts e="T27" id="Seg_797" n="e" s="T26">wataj, </ts>
               <ts e="T28" id="Seg_799" n="e" s="T27">qaptäm. </ts>
               <ts e="T29" id="Seg_801" n="e" s="T28">Jeːdaɣɨndo </ts>
               <ts e="T30" id="Seg_803" n="e" s="T29">qarʼe </ts>
               <ts e="T31" id="Seg_805" n="e" s="T30">tʼütʼöuzot </ts>
               <ts e="T32" id="Seg_807" n="e" s="T31">nagurtot, </ts>
               <ts e="T33" id="Seg_809" n="e" s="T32">kɨba </ts>
               <ts e="T34" id="Seg_811" n="e" s="T33">andoka </ts>
               <ts e="T35" id="Seg_813" n="e" s="T34">omdɨzot. </ts>
               <ts e="T36" id="Seg_815" n="e" s="T35">Andɨze </ts>
               <ts e="T37" id="Seg_817" n="e" s="T36">čaːǯizot </ts>
               <ts e="T38" id="Seg_819" n="e" s="T37">(čaʒizottə) </ts>
               <ts e="T39" id="Seg_821" n="e" s="T38">Staraj </ts>
               <ts e="T40" id="Seg_823" n="e" s="T39">jedonǯan. </ts>
               <ts e="T41" id="Seg_825" n="e" s="T40">Wattoɣən </ts>
               <ts e="T42" id="Seg_827" n="e" s="T41">čagɨmbɨs. </ts>
               <ts e="T43" id="Seg_829" n="e" s="T42">Nogorlʼe </ts>
               <ts e="T44" id="Seg_831" n="e" s="T43">čaǯizottə </ts>
               <ts e="T45" id="Seg_833" n="e" s="T44">labɨze </ts>
               <ts e="T46" id="Seg_835" n="e" s="T45">nogotčilʼe. </ts>
               <ts e="T47" id="Seg_837" n="e" s="T46">Okɨr </ts>
               <ts e="T48" id="Seg_839" n="e" s="T47">mestaɣɨn </ts>
               <ts e="T49" id="Seg_841" n="e" s="T48">kwese </ts>
               <ts e="T50" id="Seg_843" n="e" s="T49">paralguzot. </ts>
               <ts e="T51" id="Seg_845" n="e" s="T50">Kwese </ts>
               <ts e="T52" id="Seg_847" n="e" s="T51">paralʼlʼibe </ts>
               <ts e="T53" id="Seg_849" n="e" s="T52">uːtərɨzottə </ts>
               <ts e="T54" id="Seg_851" n="e" s="T53">kän </ts>
               <ts e="T55" id="Seg_853" n="e" s="T54">baront. </ts>
               <ts e="T56" id="Seg_855" n="e" s="T55">Konnä </ts>
               <ts e="T57" id="Seg_857" n="e" s="T56">čaːnǯizottə. </ts>
               <ts e="T58" id="Seg_859" n="e" s="T57">Nʼäŋɣont </ts>
               <ts e="T59" id="Seg_861" n="e" s="T58">top </ts>
               <ts e="T60" id="Seg_863" n="e" s="T59">tokedikun, </ts>
               <ts e="T61" id="Seg_865" n="e" s="T60">näŋk. </ts>
               <ts e="T62" id="Seg_867" n="e" s="T61">Okkɨr </ts>
               <ts e="T63" id="Seg_869" n="e" s="T62">näjɣum </ts>
               <ts e="T64" id="Seg_871" n="e" s="T63">ugon </ts>
               <ts e="T65" id="Seg_873" n="e" s="T64">čanǯis </ts>
               <ts e="T66" id="Seg_875" n="e" s="T65">kän </ts>
               <ts e="T67" id="Seg_877" n="e" s="T66">baːrond. </ts>
               <ts e="T68" id="Seg_879" n="e" s="T67">Natedaɣɨndoː </ts>
               <ts e="T69" id="Seg_881" n="e" s="T68">poːm </ts>
               <ts e="T70" id="Seg_883" n="e" s="T69">(polam) </ts>
               <ts e="T71" id="Seg_885" n="e" s="T70">qäsat. </ts>
               <ts e="T72" id="Seg_887" n="e" s="T71">Me </ts>
               <ts e="T73" id="Seg_889" n="e" s="T72">na </ts>
               <ts e="T74" id="Seg_891" n="e" s="T73">poːn </ts>
               <ts e="T75" id="Seg_893" n="e" s="T74">barmɨndo </ts>
               <ts e="T76" id="Seg_895" n="e" s="T75">čanǯizaj </ts>
               <ts e="T77" id="Seg_897" n="e" s="T76">konnä. </ts>
               <ts e="T78" id="Seg_899" n="e" s="T77">Konnä </ts>
               <ts e="T79" id="Seg_901" n="e" s="T78">čaːnǯilʼepe </ts>
               <ts e="T80" id="Seg_903" n="e" s="T79">qaptäno </ts>
               <ts e="T81" id="Seg_905" n="e" s="T80">wassutə, </ts>
               <ts e="T82" id="Seg_907" n="e" s="T81">qaptäm </ts>
               <ts e="T83" id="Seg_909" n="e" s="T82">wadəgu. </ts>
               <ts e="T84" id="Seg_911" n="e" s="T83">I </ts>
               <ts e="T85" id="Seg_913" n="e" s="T84">čoram </ts>
               <ts e="T86" id="Seg_915" n="e" s="T85">poǯimbɨlʼe </ts>
               <ts e="T87" id="Seg_917" n="e" s="T86">i </ts>
               <ts e="T88" id="Seg_919" n="e" s="T87">qaptäm </ts>
               <ts e="T89" id="Seg_921" n="e" s="T88">ass </ts>
               <ts e="T90" id="Seg_923" n="e" s="T89">kozut. </ts>
               <ts e="T91" id="Seg_925" n="e" s="T90">Watond </ts>
               <ts e="T92" id="Seg_927" n="e" s="T91">qweːse </ts>
               <ts e="T93" id="Seg_929" n="e" s="T92">čaːnǯesut. </ts>
               <ts e="T94" id="Seg_931" n="e" s="T93">Okɨr </ts>
               <ts e="T95" id="Seg_933" n="e" s="T94">näjqum </ts>
               <ts e="T96" id="Seg_935" n="e" s="T95">kweːsse </ts>
               <ts e="T97" id="Seg_937" n="e" s="T96">paːrass. </ts>
               <ts e="T98" id="Seg_939" n="e" s="T97">Vera </ts>
               <ts e="T99" id="Seg_941" n="e" s="T98">Demidowna </ts>
               <ts e="T100" id="Seg_943" n="e" s="T99">ass </ts>
               <ts e="T101" id="Seg_945" n="e" s="T100">kɨgelɨŋ </ts>
               <ts e="T102" id="Seg_947" n="e" s="T101">čoburi </ts>
               <ts e="T103" id="Seg_949" n="e" s="T102">wadəndigu. </ts>
               <ts e="T104" id="Seg_951" n="e" s="T103">“Man </ts>
               <ts e="T105" id="Seg_953" n="e" s="T104">tobow </ts>
               <ts e="T106" id="Seg_955" n="e" s="T105">küzɨnd </ts>
               <ts e="T107" id="Seg_957" n="e" s="T106">palʼdʼugu </ts>
               <ts e="T108" id="Seg_959" n="e" s="T107">čoram </ts>
               <ts e="T109" id="Seg_961" n="e" s="T108">poʒɨmbɨgu. </ts>
               <ts e="T110" id="Seg_963" n="e" s="T109">Kwese </ts>
               <ts e="T111" id="Seg_965" n="e" s="T110">qwanǯaŋ, </ts>
               <ts e="T112" id="Seg_967" n="e" s="T111">ato </ts>
               <ts e="T113" id="Seg_969" n="e" s="T112">qardʼel </ts>
               <ts e="T114" id="Seg_971" n="e" s="T113">i </ts>
               <ts e="T115" id="Seg_973" n="e" s="T114">tobond </ts>
               <ts e="T116" id="Seg_975" n="e" s="T115">ass </ts>
               <ts e="T117" id="Seg_977" n="e" s="T116">nɨlʼedʼänanǯ.” </ts>
               <ts e="T118" id="Seg_979" n="e" s="T117">No, </ts>
               <ts e="T119" id="Seg_981" n="e" s="T118">a </ts>
               <ts e="T120" id="Seg_983" n="e" s="T119">me </ts>
               <ts e="T121" id="Seg_985" n="e" s="T120">äːʒulgaj: </ts>
               <ts e="T122" id="Seg_987" n="e" s="T121">“Čaǯik </ts>
               <ts e="T123" id="Seg_989" n="e" s="T122">kwese. </ts>
               <ts e="T124" id="Seg_991" n="e" s="T123">Me </ts>
               <ts e="T125" id="Seg_993" n="e" s="T124">oni </ts>
               <ts e="T126" id="Seg_995" n="e" s="T125">köːse </ts>
               <ts e="T127" id="Seg_997" n="e" s="T126">kwalajse, </ts>
               <ts e="T128" id="Seg_999" n="e" s="T127">natin </ts>
               <ts e="T129" id="Seg_1001" n="e" s="T128">čobɨr </ts>
               <ts e="T130" id="Seg_1003" n="e" s="T129">jeŋ </ts>
               <ts e="T131" id="Seg_1005" n="e" s="T130">alʼi </ts>
               <ts e="T132" id="Seg_1007" n="e" s="T131">tʼäŋwa.” </ts>
               <ts e="T133" id="Seg_1009" n="e" s="T132">Qwanaj. </ts>
               <ts e="T134" id="Seg_1011" n="e" s="T133">Čobɨrɨm </ts>
               <ts e="T135" id="Seg_1013" n="e" s="T134">qowaj, </ts>
               <ts e="T136" id="Seg_1015" n="e" s="T135">čobɨrɨm </ts>
               <ts e="T137" id="Seg_1017" n="e" s="T136">wadaj </ts>
               <ts e="T138" id="Seg_1019" n="e" s="T137">tʼiːssaŋ. </ts>
               <ts e="T139" id="Seg_1021" n="e" s="T138">Sojen </ts>
               <ts e="T140" id="Seg_1023" n="e" s="T139">tʼeres </ts>
               <ts e="T141" id="Seg_1025" n="e" s="T140">pitʼöuzaj, </ts>
               <ts e="T142" id="Seg_1027" n="e" s="T141">uːdə, </ts>
               <ts e="T143" id="Seg_1029" n="e" s="T142">saːtɨrlʼe, </ts>
               <ts e="T144" id="Seg_1031" n="e" s="T143">pöum </ts>
               <ts e="T145" id="Seg_1033" n="e" s="T144">nimgelʼǯilʼebe </ts>
               <ts e="T146" id="Seg_1035" n="e" s="T145">(pöulam </ts>
               <ts e="T147" id="Seg_1037" n="e" s="T146">nigelʼǯilʼe) </ts>
               <ts e="T148" id="Seg_1039" n="e" s="T147">nelʼdʼä </ts>
               <ts e="T149" id="Seg_1041" n="e" s="T148">topɨn. </ts>
               <ts e="T150" id="Seg_1043" n="e" s="T149">Au </ts>
               <ts e="T151" id="Seg_1045" n="e" s="T150">plʼeqant </ts>
               <ts e="T152" id="Seg_1047" n="e" s="T151">petʼtʼowlʼe </ts>
               <ts e="T153" id="Seg_1049" n="e" s="T152">pöwlam </ts>
               <ts e="T154" id="Seg_1051" n="e" s="T153">sersaj </ts>
               <ts e="T155" id="Seg_1053" n="e" s="T154">i </ts>
               <ts e="T156" id="Seg_1055" n="e" s="T155">natʼe </ts>
               <ts e="T157" id="Seg_1057" n="e" s="T156">tɨɣɨndoq </ts>
               <ts e="T158" id="Seg_1059" n="e" s="T157">aj </ts>
               <ts e="T159" id="Seg_1061" n="e" s="T158">peulam </ts>
               <ts e="T160" id="Seg_1063" n="e" s="T159">ningelʼǯiquzaj. </ts>
               <ts e="T161" id="Seg_1065" n="e" s="T160">Sədə </ts>
               <ts e="T162" id="Seg_1067" n="e" s="T161">paːr </ts>
               <ts e="T163" id="Seg_1069" n="e" s="T162">sadɨrlʼe </ts>
               <ts e="T164" id="Seg_1071" n="e" s="T163">pitʼöqwaj </ts>
               <ts e="T165" id="Seg_1073" n="e" s="T164">qonnä </ts>
               <ts e="T166" id="Seg_1075" n="e" s="T165">čanǯaj </ts>
               <ts e="T167" id="Seg_1077" n="e" s="T166">nʼaŋgɨn </ts>
               <ts e="T168" id="Seg_1079" n="e" s="T167">paroɣɨn. </ts>
               <ts e="T169" id="Seg_1081" n="e" s="T168">Pöwow </ts>
               <ts e="T170" id="Seg_1083" n="e" s="T169">nʼäŋgont </ts>
               <ts e="T171" id="Seg_1085" n="e" s="T170">toquatquŋ. </ts>
               <ts e="T172" id="Seg_1087" n="e" s="T171">Man </ts>
               <ts e="T173" id="Seg_1089" n="e" s="T172">uːdoze </ts>
               <ts e="T174" id="Seg_1091" n="e" s="T173">ügolgow </ts>
               <ts e="T175" id="Seg_1093" n="e" s="T174">aj </ts>
               <ts e="T176" id="Seg_1095" n="e" s="T175">toboɣan </ts>
               <ts e="T177" id="Seg_1097" n="e" s="T176">taqalʼčeqow. </ts>
               <ts e="T178" id="Seg_1099" n="e" s="T177">Aːw </ts>
               <ts e="T179" id="Seg_1101" n="e" s="T178">tobow </ts>
               <ts e="T180" id="Seg_1103" n="e" s="T179">toqowatqont </ts>
               <ts e="T181" id="Seg_1105" n="e" s="T180">nʼäŋgont, </ts>
               <ts e="T182" id="Seg_1107" n="e" s="T181">opʼät </ts>
               <ts e="T183" id="Seg_1109" n="e" s="T182">aw </ts>
               <ts e="T184" id="Seg_1111" n="e" s="T183">pöwow </ts>
               <ts e="T185" id="Seg_1113" n="e" s="T184">üːgolguw </ts>
               <ts e="T186" id="Seg_1115" n="e" s="T185">udɨze. </ts>
               <ts e="T187" id="Seg_1117" n="e" s="T186">Aj </ts>
               <ts e="T188" id="Seg_1119" n="e" s="T187">toboɣan </ts>
               <ts e="T189" id="Seg_1121" n="e" s="T188">togalʼǯiqow. </ts>
               <ts e="T190" id="Seg_1123" n="e" s="T189">Niŋi </ts>
               <ts e="T191" id="Seg_1125" n="e" s="T190">qonnä </ts>
               <ts e="T192" id="Seg_1127" n="e" s="T191">čanǯaj. </ts>
               <ts e="T193" id="Seg_1129" n="e" s="T192">Watond </ts>
               <ts e="T194" id="Seg_1131" n="e" s="T193">nʼüʒɨm </ts>
               <ts e="T195" id="Seg_1133" n="e" s="T194">poǯimbɨlʼe </ts>
               <ts e="T196" id="Seg_1135" n="e" s="T195">čanǯaj. </ts>
               <ts e="T197" id="Seg_1137" n="e" s="T196">Na </ts>
               <ts e="T198" id="Seg_1139" n="e" s="T197">saŋ </ts>
               <ts e="T199" id="Seg_1141" n="e" s="T198">nunɨdʼäj. </ts>
               <ts e="T200" id="Seg_1143" n="e" s="T199">Qaq </ts>
               <ts e="T201" id="Seg_1145" n="e" s="T200">serbɨlʼe </ts>
               <ts e="T202" id="Seg_1147" n="e" s="T201">maːt </ts>
               <ts e="T203" id="Seg_1149" n="e" s="T202">tüaj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_1150" s="T1">ChAE_196X_PickingBerries_nar.001 (001.001)</ta>
            <ta e="T11" id="Seg_1151" s="T5">ChAE_196X_PickingBerries_nar.002 (001.002)</ta>
            <ta e="T16" id="Seg_1152" s="T11">ChAE_196X_PickingBerries_nar.003 (001.003)</ta>
            <ta e="T21" id="Seg_1153" s="T16">ChAE_196X_PickingBerries_nar.004 (001.004)</ta>
            <ta e="T28" id="Seg_1154" s="T21">ChAE_196X_PickingBerries_nar.005 (001.005)</ta>
            <ta e="T35" id="Seg_1155" s="T28">ChAE_196X_PickingBerries_nar.006 (001.006)</ta>
            <ta e="T40" id="Seg_1156" s="T35">ChAE_196X_PickingBerries_nar.007 (001.007)</ta>
            <ta e="T42" id="Seg_1157" s="T40">ChAE_196X_PickingBerries_nar.008 (001.008)</ta>
            <ta e="T46" id="Seg_1158" s="T42">ChAE_196X_PickingBerries_nar.009 (001.009)</ta>
            <ta e="T50" id="Seg_1159" s="T46">ChAE_196X_PickingBerries_nar.010 (001.010)</ta>
            <ta e="T55" id="Seg_1160" s="T50">ChAE_196X_PickingBerries_nar.011 (001.011)</ta>
            <ta e="T57" id="Seg_1161" s="T55">ChAE_196X_PickingBerries_nar.012 (001.012)</ta>
            <ta e="T61" id="Seg_1162" s="T57">ChAE_196X_PickingBerries_nar.013 (001.013)</ta>
            <ta e="T67" id="Seg_1163" s="T61">ChAE_196X_PickingBerries_nar.014 (001.014)</ta>
            <ta e="T71" id="Seg_1164" s="T67">ChAE_196X_PickingBerries_nar.015 (001.015)</ta>
            <ta e="T77" id="Seg_1165" s="T71">ChAE_196X_PickingBerries_nar.016 (001.016)</ta>
            <ta e="T83" id="Seg_1166" s="T77">ChAE_196X_PickingBerries_nar.017 (001.017)</ta>
            <ta e="T90" id="Seg_1167" s="T83">ChAE_196X_PickingBerries_nar.018 (001.018)</ta>
            <ta e="T93" id="Seg_1168" s="T90">ChAE_196X_PickingBerries_nar.019 (001.019)</ta>
            <ta e="T97" id="Seg_1169" s="T93">ChAE_196X_PickingBerries_nar.020 (001.020)</ta>
            <ta e="T103" id="Seg_1170" s="T97">ChAE_196X_PickingBerries_nar.021 (001.021)</ta>
            <ta e="T109" id="Seg_1171" s="T103">ChAE_196X_PickingBerries_nar.022 (001.022)</ta>
            <ta e="T117" id="Seg_1172" s="T109">ChAE_196X_PickingBerries_nar.023 (001.023)</ta>
            <ta e="T123" id="Seg_1173" s="T117">ChAE_196X_PickingBerries_nar.024 (001.024)</ta>
            <ta e="T132" id="Seg_1174" s="T123">ChAE_196X_PickingBerries_nar.025 (001.025)</ta>
            <ta e="T133" id="Seg_1175" s="T132">ChAE_196X_PickingBerries_nar.026 (001.026)</ta>
            <ta e="T138" id="Seg_1176" s="T133">ChAE_196X_PickingBerries_nar.027 (001.027)</ta>
            <ta e="T149" id="Seg_1177" s="T138">ChAE_196X_PickingBerries_nar.028 (001.028)</ta>
            <ta e="T160" id="Seg_1178" s="T149">ChAE_196X_PickingBerries_nar.029 (001.029)</ta>
            <ta e="T168" id="Seg_1179" s="T160">ChAE_196X_PickingBerries_nar.030 (001.030)</ta>
            <ta e="T171" id="Seg_1180" s="T168">ChAE_196X_PickingBerries_nar.031 (001.031)</ta>
            <ta e="T177" id="Seg_1181" s="T171">ChAE_196X_PickingBerries_nar.032 (001.032)</ta>
            <ta e="T186" id="Seg_1182" s="T177">ChAE_196X_PickingBerries_nar.033 (001.033)</ta>
            <ta e="T189" id="Seg_1183" s="T186">ChAE_196X_PickingBerries_nar.034 (001.034)</ta>
            <ta e="T192" id="Seg_1184" s="T189">ChAE_196X_PickingBerries_nar.035 (001.035)</ta>
            <ta e="T196" id="Seg_1185" s="T192">ChAE_196X_PickingBerries_nar.036 (001.036)</ta>
            <ta e="T199" id="Seg_1186" s="T196">ChAE_196X_PickingBerries_nar.037 (001.037)</ta>
            <ta e="T203" id="Seg_1187" s="T199">ChAE_196X_PickingBerries_nar.038 (001.038)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_1188" s="T1">′на̄гуртоттъ kwа′ссутт ′тшобырым ′вадыгу.</ta>
            <ta e="T11" id="Seg_1189" s="T5">палʼди′зут и kай ′тшобырым асс ко′ɣутт.</ta>
            <ta e="T16" id="Seg_1190" s="T11">оккыр най′ɣум ′kwесс ′паранның ′jе̄дент.</ta>
            <ta e="T21" id="Seg_1191" s="T16">а ме сыт(д)и′елʼи kwа′ннай ′то̄лак.</ta>
            <ta e="T28" id="Seg_1192" s="T21">′то̄ɣалʼе kwа′лʼлʼе бе ′тшобы(о)рым ко′вай, ′тшоборым ва′тай, kап′тӓм.</ta>
            <ta e="T35" id="Seg_1193" s="T28">′jе̄даɣындо kа′рʼе тʼӱ′тʼӧузот ′нагуртот, кы′ба ан′дока омды′зот.</ta>
            <ta e="T40" id="Seg_1194" s="T35">анды′зе ′тша̄джизот (тшажизоттъ) ′Старай ′jедонджан.</ta>
            <ta e="T42" id="Seg_1195" s="T40">ва′ттоɣън ‵тшагым′быс.</ta>
            <ta e="T46" id="Seg_1196" s="T42">но′горлʼе ‵тшаджи′зоттъ лабы′зе но′готтшилʼе.</ta>
            <ta e="T50" id="Seg_1197" s="T46">окыр ′местаɣын ′квесе па′ралгу‵зот.</ta>
            <ta e="T55" id="Seg_1198" s="T50">′кве̨се̨ ′паралʼлʼибе ‵ӯтъ(у)ры′зоттъ кӓн ′баронт.</ta>
            <ta e="T57" id="Seg_1199" s="T55">ко′ннӓ ‵тша̄нджи′зоттъ.</ta>
            <ta e="T61" id="Seg_1200" s="T57">нʼӓң′ɣонт топ то′кедикун, ′нӓңк.</ta>
            <ta e="T67" id="Seg_1201" s="T61">оккыр ′нӓйɣум у′гон тшан′джис ‵кӓн ′ба̄ронд.</ta>
            <ta e="T71" id="Seg_1202" s="T67">на′тедаɣын′до̄ ′по̄м (′полам) ′kӓсат.</ta>
            <ta e="T77" id="Seg_1203" s="T71">ме на по̄н ′бармындо тшанджи′зай ко′ннӓ.</ta>
            <ta e="T83" id="Seg_1204" s="T77">кон′нӓ ‵тша̄нджилʼепе kап′тӓно ва′ссутъ, kап′тӓм вадъ′гу.</ta>
            <ta e="T90" id="Seg_1205" s="T83">и тшо′рам ‵поджимбы′лʼе и kап′тӓм асс ко′зут.</ta>
            <ta e="T93" id="Seg_1206" s="T90">ва′тонд ′kwе̄се ‵тша̄ндже′сут.</ta>
            <ta e="T97" id="Seg_1207" s="T93">о′кыр нӓй′kум ′кве̄ссе ′па̄расс.</ta>
            <ta e="T103" id="Seg_1208" s="T97">Вера Демидовна асс кы′гелың тшо′бури ′вадъ‵ндигу.</ta>
            <ta e="T109" id="Seg_1209" s="T103">ман то′боw кӱ′зынд палʼдʼу′гу тшо′рам ′пожымбы‵гу.</ta>
            <ta e="T117" id="Seg_1210" s="T109">′квесе kwан′джаң, а то kар′дʼел и тобонд асс ′нылʼедʼӓ′нандж.</ta>
            <ta e="T123" id="Seg_1211" s="T117">но, а ме ′ӓ̄жулгай: ′тшаджик ′квесе.</ta>
            <ta e="T132" id="Seg_1212" s="T123">ме о′ни ′кӧ̄се квалайсе, на′тин ′тшобыр jең алʼи ′тʼӓңва.</ta>
            <ta e="T133" id="Seg_1213" s="T132">kwа′най.</ta>
            <ta e="T138" id="Seg_1214" s="T133">тшобырым kо′вай, тшобырым ва′дай ′тʼӣссаң.</ta>
            <ta e="T149" id="Seg_1215" s="T138">′соjен ′тʼерес пи′тʼ(цʼ)ӧузай, ′ӯдъ, ′са̄тырлʼе, пӧ′ум ним′гелʼджилʼебе (пӧу′лам ни′гелʼджилʼе) нелʼ′дʼӓ ′топ(б)ын.</ta>
            <ta e="T160" id="Seg_1216" s="T149">ау плʼе′кант пе′тʼтʼ(цʼ)овлʼе пӧв′лам ′серсай и на′тʼе ‵тыɣындок ′ай пеу′лам нин′гелʼджикузай.</ta>
            <ta e="T168" id="Seg_1217" s="T160">′съдъ па̄р ′садырлʼе питʼӧ kwай ко′ннӓ тшан′джай нʼаңг(k)ын ′пароɣын.</ta>
            <ta e="T171" id="Seg_1218" s="T168">пӧ′воw нʼӓң′гонт ‵току′аткуң.</ta>
            <ta e="T177" id="Seg_1219" s="T171">ман ӯдо′зе ′ӱголгоw ай то′боɣан та ′калʼтше‵коw.</ta>
            <ta e="T186" id="Seg_1220" s="T177">′а̄в то′боw токо′ватконт нʼӓң′гонт, опʼӓт ав пӧ′воw ′ӱ̄голгу(о)w уды′зе.</ta>
            <ta e="T189" id="Seg_1221" s="T186">ай тобо′ɣан то′галʼджикоw.</ta>
            <ta e="T192" id="Seg_1222" s="T189">ни′ңи ко′ннӓ тшан′джай.</ta>
            <ta e="T196" id="Seg_1223" s="T192">ва′тонд ′нʼӱжым поджимбы′лʼе тшан′джай.</ta>
            <ta e="T199" id="Seg_1224" s="T196">на ′саң ′нуныдʼӓй.</ta>
            <ta e="T203" id="Seg_1225" s="T199">как ′сербылʼе ма̄т ′тӱай.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_1226" s="T1">naːgurtottə qwassutt čobɨrɨm wadɨgu.</ta>
            <ta e="T11" id="Seg_1227" s="T5">palʼdizut i qaj čobɨrɨm ass koɣutt.</ta>
            <ta e="T16" id="Seg_1228" s="T11">okkɨr najɣum qwess parannɨŋ jeːdent.</ta>
            <ta e="T21" id="Seg_1229" s="T16">a me sɨt(d)ielʼi qwannaj toːlak.</ta>
            <ta e="T28" id="Seg_1230" s="T21">toːɣalʼe qwalʼlʼe be čobɨ(o)rɨm kowaj, čoborɨm wataj, qaptäm.</ta>
            <ta e="T35" id="Seg_1231" s="T28">jeːdaɣɨndo qarʼe tʼütʼöuzot nagurtot, kɨba andoka omdɨzot.</ta>
            <ta e="T40" id="Seg_1232" s="T35">andɨze čaːǯizot (čaʒizottə) Staraj jedonǯan.</ta>
            <ta e="T42" id="Seg_1233" s="T40">wattoɣən čagɨmbɨs.</ta>
            <ta e="T46" id="Seg_1234" s="T42">nogorlʼe čaǯizottə labɨze nogotčilʼe.</ta>
            <ta e="T50" id="Seg_1235" s="T46">okɨr mestaɣɨn kwese paralguzot.</ta>
            <ta e="T55" id="Seg_1236" s="T50">kwese paralʼlʼibe uːtə(u)rɨzottə kän baront.</ta>
            <ta e="T57" id="Seg_1237" s="T55">konnä čaːnǯizottə.</ta>
            <ta e="T61" id="Seg_1238" s="T57">nʼäŋɣont top tokedikun, näŋk.</ta>
            <ta e="T67" id="Seg_1239" s="T61">okkɨr näjɣum ugon čanǯis kän baːrond.</ta>
            <ta e="T71" id="Seg_1240" s="T67">natedaɣɨndoː poːm (polam) qäsat.</ta>
            <ta e="T77" id="Seg_1241" s="T71">me na poːn barmɨndo čanǯizaj konnä.</ta>
            <ta e="T83" id="Seg_1242" s="T77">konnä čaːnǯilʼepe qaptäno wassutə, qaptäm wadəgu.</ta>
            <ta e="T90" id="Seg_1243" s="T83">i čoram poǯimbɨlʼe i qaptäm ass kozut.</ta>
            <ta e="T93" id="Seg_1244" s="T90">watond qweːse čaːnǯesut.</ta>
            <ta e="T97" id="Seg_1245" s="T93">okɨr näjqum kweːsse paːrass.</ta>
            <ta e="T103" id="Seg_1246" s="T97">Vera Demidowna ass kɨgelɨŋ čoburi wadəndigu.</ta>
            <ta e="T109" id="Seg_1247" s="T103">man tobow küzɨnd palʼdʼugu čoram poʒɨmbɨgu.</ta>
            <ta e="T117" id="Seg_1248" s="T109">kwese qwanǯaŋ, a to qardʼel i tobond ass nɨlʼedʼänanǯ.</ta>
            <ta e="T123" id="Seg_1249" s="T117">no, a me äːʒulgaj: čaǯik kwese.</ta>
            <ta e="T132" id="Seg_1250" s="T123">me oni köːse kwalajse, natin čobɨr jeŋ alʼi tʼäŋwa.</ta>
            <ta e="T133" id="Seg_1251" s="T132">qwanaj.</ta>
            <ta e="T138" id="Seg_1252" s="T133">čobɨrɨm qowaj, čobɨrɨm wadaj tʼiːssaŋ.</ta>
            <ta e="T149" id="Seg_1253" s="T138">sojen tʼeres pitʼ(cʼ)öuzaj, uːdə, saːtɨrlʼe, pöum nimgelʼǯilʼebe (pöulam nigelʼǯilʼe) nelʼdʼä top(b)ɨn.</ta>
            <ta e="T160" id="Seg_1254" s="T149">au plʼeqant petʼtʼ(cʼ)owlʼe pöwlam sersaj i natʼe tɨɣɨndoq aj peulam ningelʼǯiquzaj.</ta>
            <ta e="T168" id="Seg_1255" s="T160">sədə paːr sadɨrlʼe pitʼö qwaj qonnä čanǯaj nʼaŋg(q)ɨn paroɣɨn.</ta>
            <ta e="T171" id="Seg_1256" s="T168">pöwow nʼäŋgont toquatquŋ.</ta>
            <ta e="T177" id="Seg_1257" s="T171">man uːdoze ügolgow aj toboɣan ta qalʼčeqow.</ta>
            <ta e="T186" id="Seg_1258" s="T177">aːw tobow toqowatqont nʼäŋgont, opʼät aw pöwow üːgolgu(o)w udɨze.</ta>
            <ta e="T189" id="Seg_1259" s="T186">aj toboɣan togalʼǯiqow.</ta>
            <ta e="T192" id="Seg_1260" s="T189">niŋi qonnä čanǯaj.</ta>
            <ta e="T196" id="Seg_1261" s="T192">watond nʼüʒɨm poǯimbɨlʼe čanǯaj.</ta>
            <ta e="T199" id="Seg_1262" s="T196">na saŋ nunɨdʼäj.</ta>
            <ta e="T203" id="Seg_1263" s="T199">qaq serbɨlʼe maːt tüaj.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_1264" s="T1">Naːgurtottə qwassutt čobɨrɨm wadɨgu. </ta>
            <ta e="T11" id="Seg_1265" s="T5">Palʼdizut i qaj čobɨrɨm ass koɣutt. </ta>
            <ta e="T16" id="Seg_1266" s="T11">Okkɨr najɣum qwess parannɨŋ jeːdent. </ta>
            <ta e="T21" id="Seg_1267" s="T16">A me sɨtielʼi qwannaj toːlak. </ta>
            <ta e="T28" id="Seg_1268" s="T21">Toːɣalʼe qwalʼlʼebe čobɨrɨm kowaj, čoborɨm wataj, qaptäm. </ta>
            <ta e="T35" id="Seg_1269" s="T28">Jeːdaɣɨndo qarʼe tʼütʼöuzot nagurtot, kɨba andoka omdɨzot. </ta>
            <ta e="T40" id="Seg_1270" s="T35">Andɨze čaːǯizot (čaʒizottə) Staraj jedonǯan. </ta>
            <ta e="T42" id="Seg_1271" s="T40">Wattoɣən čagɨmbɨs. </ta>
            <ta e="T46" id="Seg_1272" s="T42">Nogorlʼe čaǯizottə labɨze nogotčilʼe. </ta>
            <ta e="T50" id="Seg_1273" s="T46">Okɨr mestaɣɨn kwese paralguzot. </ta>
            <ta e="T55" id="Seg_1274" s="T50">Kwese paralʼlʼibe uːtərɨzottə kän baront. </ta>
            <ta e="T57" id="Seg_1275" s="T55">Konnä čaːnǯizottə. </ta>
            <ta e="T61" id="Seg_1276" s="T57">Nʼäŋɣont top tokedikun, näŋk. </ta>
            <ta e="T67" id="Seg_1277" s="T61">Okkɨr näjɣum ugon čanǯis kän baːrond. </ta>
            <ta e="T71" id="Seg_1278" s="T67">Natedaɣɨndoː poːm (polam) qäsat. </ta>
            <ta e="T77" id="Seg_1279" s="T71">Me na poːn barmɨndo čanǯizaj konnä. </ta>
            <ta e="T83" id="Seg_1280" s="T77">Konnä čaːnǯilʼepe qaptäno wassutə, qaptäm wadəgu. </ta>
            <ta e="T90" id="Seg_1281" s="T83">I čoram poǯimbɨlʼe i qaptäm ass kozut. </ta>
            <ta e="T93" id="Seg_1282" s="T90">Watond qweːse čaːnǯesut. </ta>
            <ta e="T97" id="Seg_1283" s="T93">Okɨr näjqum kweːsse paːrass. </ta>
            <ta e="T103" id="Seg_1284" s="T97">Vera Demidowna ass kɨgelɨŋ čoburi wadəndigu. </ta>
            <ta e="T109" id="Seg_1285" s="T103">“Man tobow küzɨnd palʼdʼugu čoram poʒɨmbɨgu. </ta>
            <ta e="T117" id="Seg_1286" s="T109">Kwese qwanǯaŋ, ato qardʼel i tobond ass nɨlʼedʼänanǯ.” </ta>
            <ta e="T123" id="Seg_1287" s="T117">No, a me äːʒulgaj: “Čaǯik kwese. </ta>
            <ta e="T132" id="Seg_1288" s="T123">Me oni köːse kwalajse, natin čobɨr jeŋ alʼi tʼäŋwa.” </ta>
            <ta e="T133" id="Seg_1289" s="T132">Qwanaj. </ta>
            <ta e="T138" id="Seg_1290" s="T133">Čobɨrɨm qowaj, čobɨrɨm wadaj tʼiːssaŋ. </ta>
            <ta e="T149" id="Seg_1291" s="T138">Sojen tʼeres pitʼöuzaj, uːdə, saːtɨrlʼe, pöum nimgelʼǯilʼebe (pöulam nigelʼǯilʼe) nelʼdʼä topɨn. </ta>
            <ta e="T160" id="Seg_1292" s="T149">Au plʼeqant petʼtʼowlʼe pöwlam sersaj i natʼe tɨɣɨndoq aj peulam ningelʼǯiquzaj. </ta>
            <ta e="T168" id="Seg_1293" s="T160">Sədə paːr sadɨrlʼe pitʼöqwaj qonnä čanǯaj nʼaŋgɨn paroɣɨn. </ta>
            <ta e="T171" id="Seg_1294" s="T168">Pöwow nʼäŋgont toquatquŋ. </ta>
            <ta e="T177" id="Seg_1295" s="T171">Man uːdoze ügolgow aj toboɣan taqalʼčeqow. </ta>
            <ta e="T186" id="Seg_1296" s="T177">Aːw tobow toqowatqont nʼäŋgont, opʼät aw pöwow üːgolguw udɨze. </ta>
            <ta e="T189" id="Seg_1297" s="T186">Aj toboɣan togalʼǯiqow. </ta>
            <ta e="T192" id="Seg_1298" s="T189">Niŋi qonnä čanǯaj. </ta>
            <ta e="T196" id="Seg_1299" s="T192">Watond nʼüʒɨm poǯimbɨlʼe čanǯaj. </ta>
            <ta e="T199" id="Seg_1300" s="T196">Na saŋ nunɨdʼäj. </ta>
            <ta e="T203" id="Seg_1301" s="T199">Qaq serbɨlʼe maːt tüaj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_1302" s="T1">naːgur-to-ttə</ta>
            <ta e="T3" id="Seg_1303" s="T2">qwas-su-tt</ta>
            <ta e="T4" id="Seg_1304" s="T3">čobɨr-ɨ-m</ta>
            <ta e="T5" id="Seg_1305" s="T4">wadɨ-gu</ta>
            <ta e="T6" id="Seg_1306" s="T5">palʼdi-zu-t</ta>
            <ta e="T7" id="Seg_1307" s="T6">i</ta>
            <ta e="T8" id="Seg_1308" s="T7">qaj</ta>
            <ta e="T9" id="Seg_1309" s="T8">čobɨr-ɨ-m</ta>
            <ta e="T10" id="Seg_1310" s="T9">ass</ta>
            <ta e="T11" id="Seg_1311" s="T10">ko-ɣu-tt</ta>
            <ta e="T12" id="Seg_1312" s="T11">okkɨr</ta>
            <ta e="T13" id="Seg_1313" s="T12">na-j-ɣum</ta>
            <ta e="T14" id="Seg_1314" s="T13">qwess</ta>
            <ta e="T15" id="Seg_1315" s="T14">paran-nɨ-ŋ</ta>
            <ta e="T16" id="Seg_1316" s="T15">jeːde-nt</ta>
            <ta e="T17" id="Seg_1317" s="T16">a</ta>
            <ta e="T18" id="Seg_1318" s="T17">me</ta>
            <ta e="T19" id="Seg_1319" s="T18">sɨti-elʼi</ta>
            <ta e="T20" id="Seg_1320" s="T19">qwan-na-j</ta>
            <ta e="T21" id="Seg_1321" s="T20">toː-lak</ta>
            <ta e="T22" id="Seg_1322" s="T21">toːɣalʼe</ta>
            <ta e="T23" id="Seg_1323" s="T22">qwalʼ-lʼebe</ta>
            <ta e="T24" id="Seg_1324" s="T23">čobɨr-ɨ-m</ta>
            <ta e="T25" id="Seg_1325" s="T24">ko-wa-j</ta>
            <ta e="T26" id="Seg_1326" s="T25">čobor-ɨ-m</ta>
            <ta e="T27" id="Seg_1327" s="T26">wata-j</ta>
            <ta e="T28" id="Seg_1328" s="T27">qaptä-m</ta>
            <ta e="T29" id="Seg_1329" s="T28">jeːda-ɣɨndo</ta>
            <ta e="T30" id="Seg_1330" s="T29">qarʼe</ta>
            <ta e="T31" id="Seg_1331" s="T30">tʼütʼöu-zo-t</ta>
            <ta e="T32" id="Seg_1332" s="T31">nagur-to-t</ta>
            <ta e="T33" id="Seg_1333" s="T32">kɨba</ta>
            <ta e="T34" id="Seg_1334" s="T33">ando-ka</ta>
            <ta e="T35" id="Seg_1335" s="T34">omdɨ-zo-t</ta>
            <ta e="T36" id="Seg_1336" s="T35">andɨ-ze</ta>
            <ta e="T37" id="Seg_1337" s="T36">čaːǯi-zo-t</ta>
            <ta e="T38" id="Seg_1338" s="T37">čaʒi-zo-ttə</ta>
            <ta e="T39" id="Seg_1339" s="T38">staraj</ta>
            <ta e="T40" id="Seg_1340" s="T39">jedo-nǯ-an</ta>
            <ta e="T41" id="Seg_1341" s="T40">watto-ɣən</ta>
            <ta e="T42" id="Seg_1342" s="T41">čagɨ-mbɨ-s</ta>
            <ta e="T43" id="Seg_1343" s="T42">nogo-r-lʼe</ta>
            <ta e="T44" id="Seg_1344" s="T43">čaǯi-zo-ttə</ta>
            <ta e="T45" id="Seg_1345" s="T44">labɨ-ze</ta>
            <ta e="T46" id="Seg_1346" s="T45">nogo-tči-lʼe</ta>
            <ta e="T47" id="Seg_1347" s="T46">okɨr</ta>
            <ta e="T48" id="Seg_1348" s="T47">mesta-ɣɨn</ta>
            <ta e="T49" id="Seg_1349" s="T48">kwese</ta>
            <ta e="T50" id="Seg_1350" s="T49">paral-gu-zo-t</ta>
            <ta e="T51" id="Seg_1351" s="T50">kwese</ta>
            <ta e="T52" id="Seg_1352" s="T51">paralʼ-lʼibe</ta>
            <ta e="T53" id="Seg_1353" s="T52">uːtər-ɨ-zo-ttə</ta>
            <ta e="T54" id="Seg_1354" s="T53">kä-n</ta>
            <ta e="T55" id="Seg_1355" s="T54">bar-o-nt</ta>
            <ta e="T56" id="Seg_1356" s="T55">konnä</ta>
            <ta e="T57" id="Seg_1357" s="T56">čaːnǯi-zo-ttə</ta>
            <ta e="T58" id="Seg_1358" s="T57">nʼäŋ-ɣont</ta>
            <ta e="T59" id="Seg_1359" s="T58">top</ta>
            <ta e="T60" id="Seg_1360" s="T59">tokedi-ku-n</ta>
            <ta e="T61" id="Seg_1361" s="T60">näŋk</ta>
            <ta e="T62" id="Seg_1362" s="T61">okkɨr</ta>
            <ta e="T63" id="Seg_1363" s="T62">nä-j-ɣum</ta>
            <ta e="T64" id="Seg_1364" s="T63">ugon</ta>
            <ta e="T65" id="Seg_1365" s="T64">čanǯi-s</ta>
            <ta e="T66" id="Seg_1366" s="T65">kä-n</ta>
            <ta e="T67" id="Seg_1367" s="T66">baːr-o-nd</ta>
            <ta e="T68" id="Seg_1368" s="T67">nate-da-ɣɨndoː</ta>
            <ta e="T69" id="Seg_1369" s="T68">poː-m</ta>
            <ta e="T70" id="Seg_1370" s="T69">po-la-m</ta>
            <ta e="T71" id="Seg_1371" s="T70">qä-sa-t</ta>
            <ta e="T72" id="Seg_1372" s="T71">me</ta>
            <ta e="T73" id="Seg_1373" s="T72">na</ta>
            <ta e="T74" id="Seg_1374" s="T73">poː-n</ta>
            <ta e="T75" id="Seg_1375" s="T74">bar-mɨn-do</ta>
            <ta e="T76" id="Seg_1376" s="T75">čanǯi-za-j</ta>
            <ta e="T77" id="Seg_1377" s="T76">konnä</ta>
            <ta e="T78" id="Seg_1378" s="T77">konnä</ta>
            <ta e="T79" id="Seg_1379" s="T78">čaːnǯi-lʼepe</ta>
            <ta e="T80" id="Seg_1380" s="T79">qaptä-no</ta>
            <ta e="T81" id="Seg_1381" s="T80">was-s-utə</ta>
            <ta e="T82" id="Seg_1382" s="T81">qaptä-m</ta>
            <ta e="T83" id="Seg_1383" s="T82">wadə-gu</ta>
            <ta e="T84" id="Seg_1384" s="T83">i</ta>
            <ta e="T85" id="Seg_1385" s="T84">čora-m</ta>
            <ta e="T86" id="Seg_1386" s="T85">poǯi-mbɨ-lʼe</ta>
            <ta e="T87" id="Seg_1387" s="T86">i</ta>
            <ta e="T88" id="Seg_1388" s="T87">qaptä-m</ta>
            <ta e="T89" id="Seg_1389" s="T88">ass</ta>
            <ta e="T90" id="Seg_1390" s="T89">ko-z-ut</ta>
            <ta e="T91" id="Seg_1391" s="T90">wato-nd</ta>
            <ta e="T92" id="Seg_1392" s="T91">qweːse</ta>
            <ta e="T93" id="Seg_1393" s="T92">čaːnǯe-s-ut</ta>
            <ta e="T94" id="Seg_1394" s="T93">okɨr</ta>
            <ta e="T95" id="Seg_1395" s="T94">nä-j-qum</ta>
            <ta e="T96" id="Seg_1396" s="T95">kweːsse</ta>
            <ta e="T97" id="Seg_1397" s="T96">paːras-s</ta>
            <ta e="T98" id="Seg_1398" s="T97">Vera</ta>
            <ta e="T99" id="Seg_1399" s="T98">Demidowna</ta>
            <ta e="T100" id="Seg_1400" s="T99">ass</ta>
            <ta e="T101" id="Seg_1401" s="T100">kɨge-lɨ-ŋ</ta>
            <ta e="T102" id="Seg_1402" s="T101">čobur-i</ta>
            <ta e="T103" id="Seg_1403" s="T102">wadə-ndi-gu</ta>
            <ta e="T104" id="Seg_1404" s="T103">Man</ta>
            <ta e="T105" id="Seg_1405" s="T104">tobo-w</ta>
            <ta e="T106" id="Seg_1406" s="T105">küzɨ-nd</ta>
            <ta e="T107" id="Seg_1407" s="T106">palʼdʼu-gu</ta>
            <ta e="T108" id="Seg_1408" s="T107">čora-m</ta>
            <ta e="T109" id="Seg_1409" s="T108">poʒɨ-mbɨ-gu</ta>
            <ta e="T110" id="Seg_1410" s="T109">kwese</ta>
            <ta e="T111" id="Seg_1411" s="T110">qwan-ǯa-ŋ</ta>
            <ta e="T112" id="Seg_1412" s="T111">ato</ta>
            <ta e="T113" id="Seg_1413" s="T112">qar-dʼel</ta>
            <ta e="T114" id="Seg_1414" s="T113">i</ta>
            <ta e="T115" id="Seg_1415" s="T114">tob-o-nd</ta>
            <ta e="T116" id="Seg_1416" s="T115">ass</ta>
            <ta e="T117" id="Seg_1417" s="T116">nɨ-lʼe-dʼä-na-nǯ</ta>
            <ta e="T118" id="Seg_1418" s="T117">no</ta>
            <ta e="T119" id="Seg_1419" s="T118">a</ta>
            <ta e="T120" id="Seg_1420" s="T119">me</ta>
            <ta e="T121" id="Seg_1421" s="T120">äːʒu-l-ga-j</ta>
            <ta e="T122" id="Seg_1422" s="T121">čaǯi-k</ta>
            <ta e="T123" id="Seg_1423" s="T122">kwese</ta>
            <ta e="T124" id="Seg_1424" s="T123">me</ta>
            <ta e="T125" id="Seg_1425" s="T124">oni</ta>
            <ta e="T126" id="Seg_1426" s="T125">köːse</ta>
            <ta e="T127" id="Seg_1427" s="T126">kwa-la-j-se</ta>
            <ta e="T128" id="Seg_1428" s="T127">nati-n</ta>
            <ta e="T129" id="Seg_1429" s="T128">čobɨr</ta>
            <ta e="T130" id="Seg_1430" s="T129">je-ŋ</ta>
            <ta e="T131" id="Seg_1431" s="T130">alʼi</ta>
            <ta e="T132" id="Seg_1432" s="T131">tʼäŋ-wa</ta>
            <ta e="T133" id="Seg_1433" s="T132">qwan-a-j</ta>
            <ta e="T134" id="Seg_1434" s="T133">čobɨr-ɨ-m</ta>
            <ta e="T135" id="Seg_1435" s="T134">qo-wa-j</ta>
            <ta e="T136" id="Seg_1436" s="T135">čobɨr-ɨ-m</ta>
            <ta e="T137" id="Seg_1437" s="T136">wada-j</ta>
            <ta e="T138" id="Seg_1438" s="T137">tʼiː-ssaŋ</ta>
            <ta e="T139" id="Seg_1439" s="T138">soj-e-n</ta>
            <ta e="T140" id="Seg_1440" s="T139">tʼeres</ta>
            <ta e="T141" id="Seg_1441" s="T140">pitʼöu-za-j</ta>
            <ta e="T142" id="Seg_1442" s="T141">uːdə</ta>
            <ta e="T143" id="Seg_1443" s="T142">saːtɨr-lʼe</ta>
            <ta e="T144" id="Seg_1444" s="T143">pöu-m</ta>
            <ta e="T145" id="Seg_1445" s="T144">nimge-lʼǯi-lʼebe</ta>
            <ta e="T146" id="Seg_1446" s="T145">pöu-la-m</ta>
            <ta e="T147" id="Seg_1447" s="T146">nige-lʼǯi-lʼe</ta>
            <ta e="T148" id="Seg_1448" s="T147">nelʼdʼä</ta>
            <ta e="T149" id="Seg_1449" s="T148">top-ɨ-n</ta>
            <ta e="T150" id="Seg_1450" s="T149">au</ta>
            <ta e="T151" id="Seg_1451" s="T150">plʼeqa-nt</ta>
            <ta e="T152" id="Seg_1452" s="T151">petʼtʼow-lʼe</ta>
            <ta e="T153" id="Seg_1453" s="T152">pöw-la-m</ta>
            <ta e="T154" id="Seg_1454" s="T153">ser-sa-j</ta>
            <ta e="T155" id="Seg_1455" s="T154">i</ta>
            <ta e="T156" id="Seg_1456" s="T155">natʼe</ta>
            <ta e="T157" id="Seg_1457" s="T156">tɨ-ɣɨndo-q</ta>
            <ta e="T158" id="Seg_1458" s="T157">aj</ta>
            <ta e="T159" id="Seg_1459" s="T158">peu-la-m</ta>
            <ta e="T160" id="Seg_1460" s="T159">ninge-lʼǯi-qu-za-j</ta>
            <ta e="T161" id="Seg_1461" s="T160">sədə</ta>
            <ta e="T162" id="Seg_1462" s="T161">paːr</ta>
            <ta e="T163" id="Seg_1463" s="T162">sadɨr-lʼe</ta>
            <ta e="T164" id="Seg_1464" s="T163">pitʼö-q-wa-j</ta>
            <ta e="T165" id="Seg_1465" s="T164">qonnä</ta>
            <ta e="T166" id="Seg_1466" s="T165">čanǯa-j</ta>
            <ta e="T167" id="Seg_1467" s="T166">nʼaŋgɨ-n</ta>
            <ta e="T168" id="Seg_1468" s="T167">par-o-ɣɨn</ta>
            <ta e="T169" id="Seg_1469" s="T168">pöw-o-w</ta>
            <ta e="T170" id="Seg_1470" s="T169">nʼäŋgo-nt</ta>
            <ta e="T171" id="Seg_1471" s="T170">toquat-qu-ŋ</ta>
            <ta e="T172" id="Seg_1472" s="T171">man</ta>
            <ta e="T173" id="Seg_1473" s="T172">uːd-o-ze</ta>
            <ta e="T174" id="Seg_1474" s="T173">ügol-go-w</ta>
            <ta e="T175" id="Seg_1475" s="T174">aj</ta>
            <ta e="T176" id="Seg_1476" s="T175">tob-o-ɣan</ta>
            <ta e="T177" id="Seg_1477" s="T176">taqalʼče-qo-w</ta>
            <ta e="T178" id="Seg_1478" s="T177">aːw</ta>
            <ta e="T179" id="Seg_1479" s="T178">tob-o-w</ta>
            <ta e="T180" id="Seg_1480" s="T179">toqowat-qo-nt</ta>
            <ta e="T181" id="Seg_1481" s="T180">nʼäŋgo-nt</ta>
            <ta e="T182" id="Seg_1482" s="T181">opʼät</ta>
            <ta e="T183" id="Seg_1483" s="T182">aw</ta>
            <ta e="T184" id="Seg_1484" s="T183">pöw-o-w</ta>
            <ta e="T185" id="Seg_1485" s="T184">üːgol-gu-w</ta>
            <ta e="T186" id="Seg_1486" s="T185">ud-ɨ-ze</ta>
            <ta e="T187" id="Seg_1487" s="T186">aj</ta>
            <ta e="T188" id="Seg_1488" s="T187">tob-o-ɣan</ta>
            <ta e="T189" id="Seg_1489" s="T188">togalʼǯi-qo-w</ta>
            <ta e="T190" id="Seg_1490" s="T189">niŋi</ta>
            <ta e="T191" id="Seg_1491" s="T190">qonnä</ta>
            <ta e="T192" id="Seg_1492" s="T191">čanǯa-j</ta>
            <ta e="T193" id="Seg_1493" s="T192">wato-nd</ta>
            <ta e="T194" id="Seg_1494" s="T193">nʼüʒɨ-m</ta>
            <ta e="T195" id="Seg_1495" s="T194">poǯi-mbɨ-lʼe</ta>
            <ta e="T196" id="Seg_1496" s="T195">čanǯa-j</ta>
            <ta e="T197" id="Seg_1497" s="T196">na</ta>
            <ta e="T198" id="Seg_1498" s="T197">saŋ</ta>
            <ta e="T199" id="Seg_1499" s="T198">nunɨ-dʼä-j</ta>
            <ta e="T200" id="Seg_1500" s="T199">qaq</ta>
            <ta e="T201" id="Seg_1501" s="T200">ser-bɨ-lʼe</ta>
            <ta e="T202" id="Seg_1502" s="T201">maːt</ta>
            <ta e="T203" id="Seg_1503" s="T202">tü-a-j</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_1504" s="T1">nagur-to-tɨn</ta>
            <ta e="T3" id="Seg_1505" s="T2">qwan-sɨ-tɨn</ta>
            <ta e="T4" id="Seg_1506" s="T3">čobər-ɨ-m</ta>
            <ta e="T5" id="Seg_1507" s="T4">wadɛ-gu</ta>
            <ta e="T6" id="Seg_1508" s="T5">palʼdʼi-sɨ-tɨn</ta>
            <ta e="T7" id="Seg_1509" s="T6">i</ta>
            <ta e="T8" id="Seg_1510" s="T7">qaj</ta>
            <ta e="T9" id="Seg_1511" s="T8">čobər-ɨ-m</ta>
            <ta e="T10" id="Seg_1512" s="T9">asa</ta>
            <ta e="T11" id="Seg_1513" s="T10">qo-nɨ-tɨn</ta>
            <ta e="T12" id="Seg_1514" s="T11">okkɨr</ta>
            <ta e="T13" id="Seg_1515" s="T12">ne-lʼ-qum</ta>
            <ta e="T14" id="Seg_1516" s="T13">kössə</ta>
            <ta e="T15" id="Seg_1517" s="T14">paral-nɨ-n</ta>
            <ta e="T16" id="Seg_1518" s="T15">eːde-ntə</ta>
            <ta e="T17" id="Seg_1519" s="T16">a</ta>
            <ta e="T18" id="Seg_1520" s="T17">miː</ta>
            <ta e="T19" id="Seg_1521" s="T18">sədə-elʼi</ta>
            <ta e="T20" id="Seg_1522" s="T19">qwan-nɨ-j</ta>
            <ta e="T21" id="Seg_1523" s="T20">to-laq</ta>
            <ta e="T22" id="Seg_1524" s="T21">toːɣalʼe</ta>
            <ta e="T23" id="Seg_1525" s="T22">qwan-lʼewlʼe</ta>
            <ta e="T24" id="Seg_1526" s="T23">čobər-ɨ-m</ta>
            <ta e="T25" id="Seg_1527" s="T24">qo-nɨ-j</ta>
            <ta e="T26" id="Seg_1528" s="T25">čobər-ɨ-m</ta>
            <ta e="T27" id="Seg_1529" s="T26">watə-j</ta>
            <ta e="T28" id="Seg_1530" s="T27">qaptə-m</ta>
            <ta e="T29" id="Seg_1531" s="T28">eːde-qɨntɨ</ta>
            <ta e="T30" id="Seg_1532" s="T29">qare</ta>
            <ta e="T31" id="Seg_1533" s="T30">tʼötʼöu-sɨ-tɨn</ta>
            <ta e="T32" id="Seg_1534" s="T31">nagur-to-tɨn</ta>
            <ta e="T33" id="Seg_1535" s="T32">qɨba</ta>
            <ta e="T34" id="Seg_1536" s="T33">andǝ-ka</ta>
            <ta e="T35" id="Seg_1537" s="T34">omdɨ-sɨ-tɨn</ta>
            <ta e="T36" id="Seg_1538" s="T35">andǝ-se</ta>
            <ta e="T37" id="Seg_1539" s="T36">čaǯɨ-sɨ-tɨn</ta>
            <ta e="T38" id="Seg_1540" s="T37">čaǯɨ-sɨ-tɨn</ta>
            <ta e="T39" id="Seg_1541" s="T38">staːrəj</ta>
            <ta e="T40" id="Seg_1542" s="T39">eːde-nǯ-an</ta>
            <ta e="T41" id="Seg_1543" s="T40">watt-qɨn</ta>
            <ta e="T42" id="Seg_1544" s="T41">čagɨ-mbɨ-sɨ</ta>
            <ta e="T43" id="Seg_1545" s="T42">*nogo-r-le</ta>
            <ta e="T44" id="Seg_1546" s="T43">čaǯɨ-sɨ-tɨn</ta>
            <ta e="T45" id="Seg_1547" s="T44">labo-se</ta>
            <ta e="T46" id="Seg_1548" s="T45">*nogo-ču-le</ta>
            <ta e="T47" id="Seg_1549" s="T46">okkɨr</ta>
            <ta e="T48" id="Seg_1550" s="T47">mesta-qɨn</ta>
            <ta e="T49" id="Seg_1551" s="T48">kössə</ta>
            <ta e="T50" id="Seg_1552" s="T49">paral-ku-sɨ-tɨn</ta>
            <ta e="T51" id="Seg_1553" s="T50">kössə</ta>
            <ta e="T52" id="Seg_1554" s="T51">paral-lʼewlʼe</ta>
            <ta e="T53" id="Seg_1555" s="T52">udɨr-ɨ-sɨ-tɨn</ta>
            <ta e="T54" id="Seg_1556" s="T53">qä-n</ta>
            <ta e="T55" id="Seg_1557" s="T54">par-ɨ-ntə</ta>
            <ta e="T56" id="Seg_1558" s="T55">qonnä</ta>
            <ta e="T57" id="Seg_1559" s="T56">čanǯu-sɨ-tɨn</ta>
            <ta e="T58" id="Seg_1560" s="T57">nʼäŋgɨ-qɨntɨ</ta>
            <ta e="T59" id="Seg_1561" s="T58">tob</ta>
            <ta e="T60" id="Seg_1562" s="T59">tokedi-ku-n</ta>
            <ta e="T61" id="Seg_1563" s="T60">nʼäŋgɨ</ta>
            <ta e="T62" id="Seg_1564" s="T61">okkɨr</ta>
            <ta e="T63" id="Seg_1565" s="T62">ne-lʼ-qum</ta>
            <ta e="T64" id="Seg_1566" s="T63">ugon</ta>
            <ta e="T65" id="Seg_1567" s="T64">čanǯu-sɨ</ta>
            <ta e="T66" id="Seg_1568" s="T65">qä-n</ta>
            <ta e="T67" id="Seg_1569" s="T66">par-ɨ-ntə</ta>
            <ta e="T68" id="Seg_1570" s="T67">natʼe-ta-qɨntɨ</ta>
            <ta e="T69" id="Seg_1571" s="T68">po-m</ta>
            <ta e="T70" id="Seg_1572" s="T69">po-la-m</ta>
            <ta e="T71" id="Seg_1573" s="T70">qal-sɨ-t</ta>
            <ta e="T72" id="Seg_1574" s="T71">miː</ta>
            <ta e="T73" id="Seg_1575" s="T72">na</ta>
            <ta e="T74" id="Seg_1576" s="T73">po-n</ta>
            <ta e="T75" id="Seg_1577" s="T74">par-un-tə</ta>
            <ta e="T76" id="Seg_1578" s="T75">čanǯu-sɨ-j</ta>
            <ta e="T77" id="Seg_1579" s="T76">qonnä</ta>
            <ta e="T78" id="Seg_1580" s="T77">qonnä</ta>
            <ta e="T79" id="Seg_1581" s="T78">čanǯu-lʼewlʼe</ta>
            <ta e="T80" id="Seg_1582" s="T79">qaptə-no</ta>
            <ta e="T81" id="Seg_1583" s="T80">watə-sɨ-un</ta>
            <ta e="T82" id="Seg_1584" s="T81">qaptə-m</ta>
            <ta e="T83" id="Seg_1585" s="T82">watə-gu</ta>
            <ta e="T84" id="Seg_1586" s="T83">i</ta>
            <ta e="T85" id="Seg_1587" s="T84">čora-m</ta>
            <ta e="T86" id="Seg_1588" s="T85">poǯə-mbɨ-le</ta>
            <ta e="T87" id="Seg_1589" s="T86">i</ta>
            <ta e="T88" id="Seg_1590" s="T87">qaptə-m</ta>
            <ta e="T89" id="Seg_1591" s="T88">asa</ta>
            <ta e="T90" id="Seg_1592" s="T89">qo-sɨ-un</ta>
            <ta e="T91" id="Seg_1593" s="T90">watt-ntə</ta>
            <ta e="T92" id="Seg_1594" s="T91">kössə</ta>
            <ta e="T93" id="Seg_1595" s="T92">čanǯu-sɨ-un</ta>
            <ta e="T94" id="Seg_1596" s="T93">okkɨr</ta>
            <ta e="T95" id="Seg_1597" s="T94">ne-lʼ-qum</ta>
            <ta e="T96" id="Seg_1598" s="T95">kössə</ta>
            <ta e="T97" id="Seg_1599" s="T96">paral-sɨ</ta>
            <ta e="T98" id="Seg_1600" s="T97">Vera</ta>
            <ta e="T99" id="Seg_1601" s="T98">Demidowna</ta>
            <ta e="T100" id="Seg_1602" s="T99">asa</ta>
            <ta e="T101" id="Seg_1603" s="T100">kɨgɨ-lɨ-n</ta>
            <ta e="T102" id="Seg_1604" s="T101">čobər-ɨ</ta>
            <ta e="T103" id="Seg_1605" s="T102">watə-ntɨ-gu</ta>
            <ta e="T104" id="Seg_1606" s="T103">man</ta>
            <ta e="T105" id="Seg_1607" s="T104">tob-w</ta>
            <ta e="T106" id="Seg_1608" s="T105">küzɨ-ntɨ</ta>
            <ta e="T107" id="Seg_1609" s="T106">palʼdʼi-gu</ta>
            <ta e="T108" id="Seg_1610" s="T107">čora-m</ta>
            <ta e="T109" id="Seg_1611" s="T108">poǯə-mbɨ-gu</ta>
            <ta e="T110" id="Seg_1612" s="T109">kössə</ta>
            <ta e="T111" id="Seg_1613" s="T110">qwan-enǯɨ-ŋ</ta>
            <ta e="T112" id="Seg_1614" s="T111">ato</ta>
            <ta e="T113" id="Seg_1615" s="T112">qare-dʼel</ta>
            <ta e="T114" id="Seg_1616" s="T113">i</ta>
            <ta e="T115" id="Seg_1617" s="T114">tob-ɨ-ntə</ta>
            <ta e="T116" id="Seg_1618" s="T115">asa</ta>
            <ta e="T117" id="Seg_1619" s="T116">nɨ-lɨ-dʼi-enǯɨ-ntə</ta>
            <ta e="T118" id="Seg_1620" s="T117">nu</ta>
            <ta e="T119" id="Seg_1621" s="T118">a</ta>
            <ta e="T120" id="Seg_1622" s="T119">me</ta>
            <ta e="T121" id="Seg_1623" s="T120">əǯu-l-ku-j</ta>
            <ta e="T122" id="Seg_1624" s="T121">čaǯɨ-kɨ</ta>
            <ta e="T123" id="Seg_1625" s="T122">kössə</ta>
            <ta e="T124" id="Seg_1626" s="T123">miː</ta>
            <ta e="T125" id="Seg_1627" s="T124">onäj</ta>
            <ta e="T126" id="Seg_1628" s="T125">kössə</ta>
            <ta e="T127" id="Seg_1629" s="T126">qwan-lä-j-s</ta>
            <ta e="T128" id="Seg_1630" s="T127">natʼe-n</ta>
            <ta e="T129" id="Seg_1631" s="T128">čobər</ta>
            <ta e="T130" id="Seg_1632" s="T129">eː-n</ta>
            <ta e="T131" id="Seg_1633" s="T130">alʼi</ta>
            <ta e="T132" id="Seg_1634" s="T131">tʼäkku-nɨ</ta>
            <ta e="T133" id="Seg_1635" s="T132">qwan-nɨ-j</ta>
            <ta e="T134" id="Seg_1636" s="T133">čobər-ɨ-m</ta>
            <ta e="T135" id="Seg_1637" s="T134">qo-nɨ-j</ta>
            <ta e="T136" id="Seg_1638" s="T135">čobər-ɨ-m</ta>
            <ta e="T137" id="Seg_1639" s="T136">wadɛ-j</ta>
            <ta e="T138" id="Seg_1640" s="T137">tʼiː-sak</ta>
            <ta e="T139" id="Seg_1641" s="T138">sоj-ɨ-n</ta>
            <ta e="T140" id="Seg_1642" s="T139">tʼeres</ta>
            <ta e="T141" id="Seg_1643" s="T140">pitʼöl-sɨ-j</ta>
            <ta e="T142" id="Seg_1644" s="T141">uːdə</ta>
            <ta e="T143" id="Seg_1645" s="T142">sadɨr-le</ta>
            <ta e="T144" id="Seg_1646" s="T143">pöw-m</ta>
            <ta e="T145" id="Seg_1647" s="T144">niŋgɨ-lǯi-lʼewlʼe</ta>
            <ta e="T146" id="Seg_1648" s="T145">pöw-la-m</ta>
            <ta e="T147" id="Seg_1649" s="T146">niŋgɨ-lǯi-le</ta>
            <ta e="T148" id="Seg_1650" s="T147">nʼälʼdʼe</ta>
            <ta e="T149" id="Seg_1651" s="T148">tob-ɨ-n</ta>
            <ta e="T150" id="Seg_1652" s="T149">au</ta>
            <ta e="T151" id="Seg_1653" s="T150">plʼäka-ntə</ta>
            <ta e="T152" id="Seg_1654" s="T151">pitʼöl-le</ta>
            <ta e="T153" id="Seg_1655" s="T152">pöw-la-m</ta>
            <ta e="T154" id="Seg_1656" s="T153">ser-sɨ-j</ta>
            <ta e="T155" id="Seg_1657" s="T154">i</ta>
            <ta e="T156" id="Seg_1658" s="T155">natʼe</ta>
            <ta e="T157" id="Seg_1659" s="T156">tiː-qɨntɨ-ŋ</ta>
            <ta e="T158" id="Seg_1660" s="T157">aj</ta>
            <ta e="T159" id="Seg_1661" s="T158">pöw-la-m</ta>
            <ta e="T160" id="Seg_1662" s="T159">niŋgɨ-lǯi-ku-sɨ-j</ta>
            <ta e="T161" id="Seg_1663" s="T160">sədə</ta>
            <ta e="T162" id="Seg_1664" s="T161">par</ta>
            <ta e="T163" id="Seg_1665" s="T162">sadɨr-le</ta>
            <ta e="T164" id="Seg_1666" s="T163">pitʼöl-ku-nɨ-j</ta>
            <ta e="T165" id="Seg_1667" s="T164">qonnä</ta>
            <ta e="T166" id="Seg_1668" s="T165">čanǯu-j</ta>
            <ta e="T167" id="Seg_1669" s="T166">nʼäŋgɨ-n</ta>
            <ta e="T168" id="Seg_1670" s="T167">par-ɨ-qɨn</ta>
            <ta e="T169" id="Seg_1671" s="T168">pöw-ɨ-w</ta>
            <ta e="T170" id="Seg_1672" s="T169">nʼäŋgɨ-ntə</ta>
            <ta e="T171" id="Seg_1673" s="T170">toqwat-ku-n</ta>
            <ta e="T172" id="Seg_1674" s="T171">man</ta>
            <ta e="T173" id="Seg_1675" s="T172">ut-ɨ-se</ta>
            <ta e="T174" id="Seg_1676" s="T173">ügul-ku-w</ta>
            <ta e="T175" id="Seg_1677" s="T174">aj</ta>
            <ta e="T176" id="Seg_1678" s="T175">tob-ɨ-qɨn</ta>
            <ta e="T177" id="Seg_1679" s="T176">togalǯu-ku-w</ta>
            <ta e="T178" id="Seg_1680" s="T177">au</ta>
            <ta e="T179" id="Seg_1681" s="T178">tob-ɨ-w</ta>
            <ta e="T180" id="Seg_1682" s="T179">toqwat-ku-ntɨ</ta>
            <ta e="T181" id="Seg_1683" s="T180">nʼäŋgɨ-ntə</ta>
            <ta e="T182" id="Seg_1684" s="T181">apʼatʼ</ta>
            <ta e="T183" id="Seg_1685" s="T182">au</ta>
            <ta e="T184" id="Seg_1686" s="T183">pöw-ɨ-w</ta>
            <ta e="T185" id="Seg_1687" s="T184">ügul-ku-w</ta>
            <ta e="T186" id="Seg_1688" s="T185">ut-ɨ-se</ta>
            <ta e="T187" id="Seg_1689" s="T186">aj</ta>
            <ta e="T188" id="Seg_1690" s="T187">tob-ɨ-qɨn</ta>
            <ta e="T189" id="Seg_1691" s="T188">togalǯu-ku-w</ta>
            <ta e="T190" id="Seg_1692" s="T189">niŋ</ta>
            <ta e="T191" id="Seg_1693" s="T190">qonnä</ta>
            <ta e="T192" id="Seg_1694" s="T191">čanǯu-j</ta>
            <ta e="T193" id="Seg_1695" s="T192">watt-ntə</ta>
            <ta e="T194" id="Seg_1696" s="T193">nʼüʒə-m</ta>
            <ta e="T195" id="Seg_1697" s="T194">poǯə-mbɨ-le</ta>
            <ta e="T196" id="Seg_1698" s="T195">čanǯu-j</ta>
            <ta e="T197" id="Seg_1699" s="T196">na</ta>
            <ta e="T198" id="Seg_1700" s="T197">saŋ</ta>
            <ta e="T199" id="Seg_1701" s="T198">nunɨ-dʼi-j</ta>
            <ta e="T200" id="Seg_1702" s="T199">kak</ta>
            <ta e="T201" id="Seg_1703" s="T200">sɛːr-mbɨ-le</ta>
            <ta e="T202" id="Seg_1704" s="T201">maːt</ta>
            <ta e="T203" id="Seg_1705" s="T202">tüː-nɨ-j</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_1706" s="T1">three-%%-3PL</ta>
            <ta e="T3" id="Seg_1707" s="T2">leave-PST-3PL</ta>
            <ta e="T4" id="Seg_1708" s="T3">berry-EP-ACC</ta>
            <ta e="T5" id="Seg_1709" s="T4">gather-INF</ta>
            <ta e="T6" id="Seg_1710" s="T5">walk-PST-3PL</ta>
            <ta e="T7" id="Seg_1711" s="T6">and</ta>
            <ta e="T8" id="Seg_1712" s="T7">what.[NOM]</ta>
            <ta e="T9" id="Seg_1713" s="T8">berry-EP-ACC</ta>
            <ta e="T10" id="Seg_1714" s="T9">NEG</ta>
            <ta e="T11" id="Seg_1715" s="T10">find-CO-3PL</ta>
            <ta e="T12" id="Seg_1716" s="T11">one</ta>
            <ta e="T13" id="Seg_1717" s="T12">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T14" id="Seg_1718" s="T13">backward</ta>
            <ta e="T15" id="Seg_1719" s="T14">return-CO-3SG.S</ta>
            <ta e="T16" id="Seg_1720" s="T15">village-ILL</ta>
            <ta e="T17" id="Seg_1721" s="T16">and</ta>
            <ta e="T18" id="Seg_1722" s="T17">we.DU.[NOM]</ta>
            <ta e="T19" id="Seg_1723" s="T18">two-%COLL</ta>
            <ta e="T20" id="Seg_1724" s="T19">leave-CO-1DU</ta>
            <ta e="T21" id="Seg_1725" s="T20">away-ATTEN</ta>
            <ta e="T22" id="Seg_1726" s="T21">%further</ta>
            <ta e="T23" id="Seg_1727" s="T22">leave-CVB2</ta>
            <ta e="T24" id="Seg_1728" s="T23">berry-EP-ACC</ta>
            <ta e="T25" id="Seg_1729" s="T24">find-CO-1DU</ta>
            <ta e="T26" id="Seg_1730" s="T25">berry-EP-ACC</ta>
            <ta e="T27" id="Seg_1731" s="T26">gather-1DU</ta>
            <ta e="T28" id="Seg_1732" s="T27">currants-ACC</ta>
            <ta e="T29" id="Seg_1733" s="T28">village-EL.3SG</ta>
            <ta e="T30" id="Seg_1734" s="T29">downhill</ta>
            <ta e="T31" id="Seg_1735" s="T30">climb.down-PST-3PL</ta>
            <ta e="T32" id="Seg_1736" s="T31">three-%%-3PL</ta>
            <ta e="T33" id="Seg_1737" s="T32">small</ta>
            <ta e="T34" id="Seg_1738" s="T33">boat-DIM.[NOM]</ta>
            <ta e="T35" id="Seg_1739" s="T34">sit.down-PST-3PL</ta>
            <ta e="T36" id="Seg_1740" s="T35">boat-INSTR</ta>
            <ta e="T37" id="Seg_1741" s="T36">go-PST-3PL</ta>
            <ta e="T38" id="Seg_1742" s="T37">go-PST-3PL</ta>
            <ta e="T39" id="Seg_1743" s="T38">old</ta>
            <ta e="T40" id="Seg_1744" s="T39">village-ILL2-%%</ta>
            <ta e="T41" id="Seg_1745" s="T40">road-LOC</ta>
            <ta e="T42" id="Seg_1746" s="T41">dry-RES-PST.[3SG.S]</ta>
            <ta e="T43" id="Seg_1747" s="T42">%push-DRV-CVB</ta>
            <ta e="T44" id="Seg_1748" s="T43">go-PST-3PL</ta>
            <ta e="T45" id="Seg_1749" s="T44">oar-INSTR</ta>
            <ta e="T46" id="Seg_1750" s="T45">%push-TR-CVB</ta>
            <ta e="T47" id="Seg_1751" s="T46">one</ta>
            <ta e="T48" id="Seg_1752" s="T47">place-LOC</ta>
            <ta e="T49" id="Seg_1753" s="T48">backward</ta>
            <ta e="T50" id="Seg_1754" s="T49">return-HAB-PST-3PL</ta>
            <ta e="T51" id="Seg_1755" s="T50">backward</ta>
            <ta e="T52" id="Seg_1756" s="T51">return-CVB2</ta>
            <ta e="T53" id="Seg_1757" s="T52">land-EP-PST-3PL</ta>
            <ta e="T54" id="Seg_1758" s="T53">steep.bank-GEN</ta>
            <ta e="T55" id="Seg_1759" s="T54">top-EP-ILL</ta>
            <ta e="T56" id="Seg_1760" s="T55">up.the.shore</ta>
            <ta e="T57" id="Seg_1761" s="T56">go.out-PST-3PL</ta>
            <ta e="T58" id="Seg_1762" s="T57">silt-ILL.3SG</ta>
            <ta e="T59" id="Seg_1763" s="T58">leg.[NOM]</ta>
            <ta e="T60" id="Seg_1764" s="T59">%stick.in-HAB-3SG.S</ta>
            <ta e="T61" id="Seg_1765" s="T60">silt.[NOM]</ta>
            <ta e="T62" id="Seg_1766" s="T61">one</ta>
            <ta e="T63" id="Seg_1767" s="T62">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T64" id="Seg_1768" s="T63">earlier</ta>
            <ta e="T65" id="Seg_1769" s="T64">go.out-PST.[3SG.S]</ta>
            <ta e="T66" id="Seg_1770" s="T65">steep.bank-GEN</ta>
            <ta e="T67" id="Seg_1771" s="T66">top-EP-ILL</ta>
            <ta e="T68" id="Seg_1772" s="T67">there-INDEF-EL.3SG</ta>
            <ta e="T69" id="Seg_1773" s="T68">stick-ACC</ta>
            <ta e="T70" id="Seg_1774" s="T69">stick-PL-ACC</ta>
            <ta e="T71" id="Seg_1775" s="T70">throw.out-PST-3SG.O</ta>
            <ta e="T72" id="Seg_1776" s="T71">we.DU.[NOM]</ta>
            <ta e="T73" id="Seg_1777" s="T72">this</ta>
            <ta e="T74" id="Seg_1778" s="T73">stick-GEN</ta>
            <ta e="T75" id="Seg_1779" s="T74">top-PROL-3SG</ta>
            <ta e="T76" id="Seg_1780" s="T75">go.out-PST-1DU</ta>
            <ta e="T77" id="Seg_1781" s="T76">up.the.shore</ta>
            <ta e="T78" id="Seg_1782" s="T77">up.the.shore</ta>
            <ta e="T79" id="Seg_1783" s="T78">go.out-CVB2</ta>
            <ta e="T80" id="Seg_1784" s="T79">currants-TRL</ta>
            <ta e="T81" id="Seg_1785" s="T80">gather-PST-1PL</ta>
            <ta e="T82" id="Seg_1786" s="T81">currants-ACC</ta>
            <ta e="T83" id="Seg_1787" s="T82">gather-INF</ta>
            <ta e="T84" id="Seg_1788" s="T83">and</ta>
            <ta e="T85" id="Seg_1789" s="T84">thicket-ACC</ta>
            <ta e="T86" id="Seg_1790" s="T85">penetrate-DUR-CVB</ta>
            <ta e="T87" id="Seg_1791" s="T86">and</ta>
            <ta e="T88" id="Seg_1792" s="T87">currants-ACC</ta>
            <ta e="T89" id="Seg_1793" s="T88">NEG</ta>
            <ta e="T90" id="Seg_1794" s="T89">find-PST-1PL</ta>
            <ta e="T91" id="Seg_1795" s="T90">road-ILL</ta>
            <ta e="T92" id="Seg_1796" s="T91">backward</ta>
            <ta e="T93" id="Seg_1797" s="T92">go.out-PST-1PL</ta>
            <ta e="T94" id="Seg_1798" s="T93">one</ta>
            <ta e="T95" id="Seg_1799" s="T94">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T96" id="Seg_1800" s="T95">backward</ta>
            <ta e="T97" id="Seg_1801" s="T96">return-PST.[3SG.S]</ta>
            <ta e="T98" id="Seg_1802" s="T97">Vera.[NOM]</ta>
            <ta e="T99" id="Seg_1803" s="T98">Demidovna</ta>
            <ta e="T100" id="Seg_1804" s="T99">NEG</ta>
            <ta e="T101" id="Seg_1805" s="T100">want-RES-3SG.S</ta>
            <ta e="T102" id="Seg_1806" s="T101">berry-EP.[NOM]</ta>
            <ta e="T103" id="Seg_1807" s="T102">gather-IPFV-INF</ta>
            <ta e="T104" id="Seg_1808" s="T103">I.GEN</ta>
            <ta e="T105" id="Seg_1809" s="T104">leg.[NOM]-1SG</ta>
            <ta e="T106" id="Seg_1810" s="T105">hurt-INFER.[3SG.S]</ta>
            <ta e="T107" id="Seg_1811" s="T106">walk-INF</ta>
            <ta e="T108" id="Seg_1812" s="T107">thicket-ACC</ta>
            <ta e="T109" id="Seg_1813" s="T108">penetrate-DUR-INF</ta>
            <ta e="T110" id="Seg_1814" s="T109">backward</ta>
            <ta e="T111" id="Seg_1815" s="T110">leave-FUT-1SG.S</ta>
            <ta e="T112" id="Seg_1816" s="T111">otherwise</ta>
            <ta e="T113" id="Seg_1817" s="T112">morning-day.[NOM]</ta>
            <ta e="T114" id="Seg_1818" s="T113">and</ta>
            <ta e="T115" id="Seg_1819" s="T114">leg-EP-ILL</ta>
            <ta e="T116" id="Seg_1820" s="T115">NEG</ta>
            <ta e="T117" id="Seg_1821" s="T116">stand-RES-DRV-FUT-2SG.S</ta>
            <ta e="T118" id="Seg_1822" s="T117">now</ta>
            <ta e="T119" id="Seg_1823" s="T118">and</ta>
            <ta e="T120" id="Seg_1824" s="T119">we.[NOM]</ta>
            <ta e="T121" id="Seg_1825" s="T120">say-DRV-HAB-1DU</ta>
            <ta e="T122" id="Seg_1826" s="T121">go-IMP.2SG.S</ta>
            <ta e="T123" id="Seg_1827" s="T122">backward</ta>
            <ta e="T124" id="Seg_1828" s="T123">we.DU.[NOM]</ta>
            <ta e="T125" id="Seg_1829" s="T124">oneself.1DU</ta>
            <ta e="T126" id="Seg_1830" s="T125">backward</ta>
            <ta e="T127" id="Seg_1831" s="T126">leave-FUT-1DU-FUT</ta>
            <ta e="T128" id="Seg_1832" s="T127">there-ADV.LOC</ta>
            <ta e="T129" id="Seg_1833" s="T128">berry.[NOM]</ta>
            <ta e="T130" id="Seg_1834" s="T129">be-3SG.S</ta>
            <ta e="T131" id="Seg_1835" s="T130">or</ta>
            <ta e="T132" id="Seg_1836" s="T131">NEG.EX-CO.[3SG.S]</ta>
            <ta e="T133" id="Seg_1837" s="T132">leave-CO-1DU</ta>
            <ta e="T134" id="Seg_1838" s="T133">berry-EP-ACC</ta>
            <ta e="T135" id="Seg_1839" s="T134">find-CO-1DU</ta>
            <ta e="T136" id="Seg_1840" s="T135">berry-EP-ACC</ta>
            <ta e="T137" id="Seg_1841" s="T136">gather-1DU</ta>
            <ta e="T138" id="Seg_1842" s="T137">bucket-COR</ta>
            <ta e="T139" id="Seg_1843" s="T138">source-EP-GEN</ta>
            <ta e="T140" id="Seg_1844" s="T139">across</ta>
            <ta e="T141" id="Seg_1845" s="T140">swim.over-PST-1DU</ta>
            <ta e="T142" id="Seg_1846" s="T141">on.foot</ta>
            <ta e="T143" id="Seg_1847" s="T142">wander-CVB</ta>
            <ta e="T144" id="Seg_1848" s="T143">boots-ACC</ta>
            <ta e="T145" id="Seg_1849" s="T144">take.off-PFV-CVB2</ta>
            <ta e="T146" id="Seg_1850" s="T145">boots-PL-ACC</ta>
            <ta e="T147" id="Seg_1851" s="T146">take.off-PFV-CVB</ta>
            <ta e="T148" id="Seg_1852" s="T147">bare</ta>
            <ta e="T149" id="Seg_1853" s="T148">leg-EP-ADV.LOC</ta>
            <ta e="T150" id="Seg_1854" s="T149">other</ta>
            <ta e="T151" id="Seg_1855" s="T150">side-ILL</ta>
            <ta e="T152" id="Seg_1856" s="T151">swim.over-CVB</ta>
            <ta e="T153" id="Seg_1857" s="T152">boots-PL-ACC</ta>
            <ta e="T154" id="Seg_1858" s="T153">put.on-PST-1DU</ta>
            <ta e="T155" id="Seg_1859" s="T154">and</ta>
            <ta e="T156" id="Seg_1860" s="T155">there</ta>
            <ta e="T157" id="Seg_1861" s="T156">here-EL.3SG-ADVZ</ta>
            <ta e="T158" id="Seg_1862" s="T157">again</ta>
            <ta e="T159" id="Seg_1863" s="T158">boots-PL-ACC</ta>
            <ta e="T160" id="Seg_1864" s="T159">take.off-PFV-HAB-PST-1DU</ta>
            <ta e="T161" id="Seg_1865" s="T160">two</ta>
            <ta e="T162" id="Seg_1866" s="T161">time(s).[NOM]</ta>
            <ta e="T163" id="Seg_1867" s="T162">wander-CVB</ta>
            <ta e="T164" id="Seg_1868" s="T163">swim.over-HAB-CO-1DU</ta>
            <ta e="T165" id="Seg_1869" s="T164">up.the.shore</ta>
            <ta e="T166" id="Seg_1870" s="T165">go.out-1DU</ta>
            <ta e="T167" id="Seg_1871" s="T166">silt-GEN</ta>
            <ta e="T168" id="Seg_1872" s="T167">top-EP-LOC</ta>
            <ta e="T169" id="Seg_1873" s="T168">boots.[NOM]-EP-1SG</ta>
            <ta e="T170" id="Seg_1874" s="T169">silt-ILL</ta>
            <ta e="T171" id="Seg_1875" s="T170">stick.in-HAB-3SG.S</ta>
            <ta e="T172" id="Seg_1876" s="T171">I.NOM</ta>
            <ta e="T173" id="Seg_1877" s="T172">hand-EP-INSTR</ta>
            <ta e="T174" id="Seg_1878" s="T173">pull.out-HAB-1SG.O</ta>
            <ta e="T175" id="Seg_1879" s="T174">and</ta>
            <ta e="T176" id="Seg_1880" s="T175">leg-EP-LOC</ta>
            <ta e="T177" id="Seg_1881" s="T176">stick-HAB-1SG.O</ta>
            <ta e="T178" id="Seg_1882" s="T177">other</ta>
            <ta e="T179" id="Seg_1883" s="T178">leg.[NOM]-EP-1SG</ta>
            <ta e="T180" id="Seg_1884" s="T179">stick.in-HAB-INFER.[3SG.S]</ta>
            <ta e="T181" id="Seg_1885" s="T180">silt-ILL</ta>
            <ta e="T182" id="Seg_1886" s="T181">again</ta>
            <ta e="T183" id="Seg_1887" s="T182">other</ta>
            <ta e="T184" id="Seg_1888" s="T183">boots.[NOM]-EP-1SG</ta>
            <ta e="T185" id="Seg_1889" s="T184">drag-HAB-1SG.O</ta>
            <ta e="T186" id="Seg_1890" s="T185">hand-EP-INSTR</ta>
            <ta e="T187" id="Seg_1891" s="T186">again</ta>
            <ta e="T188" id="Seg_1892" s="T187">leg-EP-LOC</ta>
            <ta e="T189" id="Seg_1893" s="T188">stick-HAB-1SG.O</ta>
            <ta e="T190" id="Seg_1894" s="T189">so</ta>
            <ta e="T191" id="Seg_1895" s="T190">up.the.shore</ta>
            <ta e="T192" id="Seg_1896" s="T191">go.out-1DU</ta>
            <ta e="T193" id="Seg_1897" s="T192">road-ILL</ta>
            <ta e="T194" id="Seg_1898" s="T193">grass-ACC</ta>
            <ta e="T195" id="Seg_1899" s="T194">penetrate-DUR-CVB</ta>
            <ta e="T196" id="Seg_1900" s="T195">go.out-1DU</ta>
            <ta e="T197" id="Seg_1901" s="T196">this</ta>
            <ta e="T198" id="Seg_1902" s="T197">approximately.as</ta>
            <ta e="T199" id="Seg_1903" s="T198">get.tired-DRV-1DU</ta>
            <ta e="T200" id="Seg_1904" s="T199">as</ta>
            <ta e="T201" id="Seg_1905" s="T200">get.drunk-DUR-CVB</ta>
            <ta e="T202" id="Seg_1906" s="T201">house.[NOM]</ta>
            <ta e="T203" id="Seg_1907" s="T202">come-CO-1DU</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_1908" s="T1">три-%%-3PL</ta>
            <ta e="T3" id="Seg_1909" s="T2">отправиться-PST-3PL</ta>
            <ta e="T4" id="Seg_1910" s="T3">ягода-EP-ACC</ta>
            <ta e="T5" id="Seg_1911" s="T4">собирать-INF</ta>
            <ta e="T6" id="Seg_1912" s="T5">ходить-PST-3PL</ta>
            <ta e="T7" id="Seg_1913" s="T6">и</ta>
            <ta e="T8" id="Seg_1914" s="T7">что.[NOM]</ta>
            <ta e="T9" id="Seg_1915" s="T8">ягода-EP-ACC</ta>
            <ta e="T10" id="Seg_1916" s="T9">NEG</ta>
            <ta e="T11" id="Seg_1917" s="T10">найти-CO-3PL</ta>
            <ta e="T12" id="Seg_1918" s="T11">один</ta>
            <ta e="T13" id="Seg_1919" s="T12">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T14" id="Seg_1920" s="T13">назад</ta>
            <ta e="T15" id="Seg_1921" s="T14">вернуться-CO-3SG.S</ta>
            <ta e="T16" id="Seg_1922" s="T15">деревня-ILL</ta>
            <ta e="T17" id="Seg_1923" s="T16">а</ta>
            <ta e="T18" id="Seg_1924" s="T17">мы.DU.[NOM]</ta>
            <ta e="T19" id="Seg_1925" s="T18">два-%COLL</ta>
            <ta e="T20" id="Seg_1926" s="T19">отправиться-CO-1DU</ta>
            <ta e="T21" id="Seg_1927" s="T20">прочь-ATTEN</ta>
            <ta e="T22" id="Seg_1928" s="T21">%подальше</ta>
            <ta e="T23" id="Seg_1929" s="T22">отправиться-CVB2</ta>
            <ta e="T24" id="Seg_1930" s="T23">ягода-EP-ACC</ta>
            <ta e="T25" id="Seg_1931" s="T24">найти-CO-1DU</ta>
            <ta e="T26" id="Seg_1932" s="T25">ягода-EP-ACC</ta>
            <ta e="T27" id="Seg_1933" s="T26">собрать-1DU</ta>
            <ta e="T28" id="Seg_1934" s="T27">смородина-ACC</ta>
            <ta e="T29" id="Seg_1935" s="T28">деревня-EL.3SG</ta>
            <ta e="T30" id="Seg_1936" s="T29">под.гору</ta>
            <ta e="T31" id="Seg_1937" s="T30">слезть-PST-3PL</ta>
            <ta e="T32" id="Seg_1938" s="T31">три-%%-3PL</ta>
            <ta e="T33" id="Seg_1939" s="T32">маленький</ta>
            <ta e="T34" id="Seg_1940" s="T33">обласок-DIM.[NOM]</ta>
            <ta e="T35" id="Seg_1941" s="T34">сесть-PST-3PL</ta>
            <ta e="T36" id="Seg_1942" s="T35">обласок-INSTR</ta>
            <ta e="T37" id="Seg_1943" s="T36">ходить-PST-3PL</ta>
            <ta e="T38" id="Seg_1944" s="T37">ходить-PST-3PL</ta>
            <ta e="T39" id="Seg_1945" s="T38">старый</ta>
            <ta e="T40" id="Seg_1946" s="T39">деревня-ILL2-%%</ta>
            <ta e="T41" id="Seg_1947" s="T40">дорога-LOC</ta>
            <ta e="T42" id="Seg_1948" s="T41">высохнуть-RES-PST.[3SG.S]</ta>
            <ta e="T43" id="Seg_1949" s="T42">%толкнуть-DRV-CVB</ta>
            <ta e="T44" id="Seg_1950" s="T43">ходить-PST-3PL</ta>
            <ta e="T45" id="Seg_1951" s="T44">весло-INSTR</ta>
            <ta e="T46" id="Seg_1952" s="T45">%толкнуть-TR-CVB</ta>
            <ta e="T47" id="Seg_1953" s="T46">один</ta>
            <ta e="T48" id="Seg_1954" s="T47">место-LOC</ta>
            <ta e="T49" id="Seg_1955" s="T48">назад</ta>
            <ta e="T50" id="Seg_1956" s="T49">вернуться-HAB-PST-3PL</ta>
            <ta e="T51" id="Seg_1957" s="T50">назад</ta>
            <ta e="T52" id="Seg_1958" s="T51">вернуться-CVB2</ta>
            <ta e="T53" id="Seg_1959" s="T52">причалить-EP-PST-3PL</ta>
            <ta e="T54" id="Seg_1960" s="T53">крутой.берег-GEN</ta>
            <ta e="T55" id="Seg_1961" s="T54">верхняя.часть-EP-ILL</ta>
            <ta e="T56" id="Seg_1962" s="T55">на.берег</ta>
            <ta e="T57" id="Seg_1963" s="T56">выйти-PST-3PL</ta>
            <ta e="T58" id="Seg_1964" s="T57">ил-ILL.3SG</ta>
            <ta e="T59" id="Seg_1965" s="T58">нога.[NOM]</ta>
            <ta e="T60" id="Seg_1966" s="T59">%увязнуть-HAB-3SG.S</ta>
            <ta e="T61" id="Seg_1967" s="T60">ил.[NOM]</ta>
            <ta e="T62" id="Seg_1968" s="T61">один</ta>
            <ta e="T63" id="Seg_1969" s="T62">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T64" id="Seg_1970" s="T63">раньше</ta>
            <ta e="T65" id="Seg_1971" s="T64">выйти-PST.[3SG.S]</ta>
            <ta e="T66" id="Seg_1972" s="T65">крутой.берег-GEN</ta>
            <ta e="T67" id="Seg_1973" s="T66">верхняя.часть-EP-ILL</ta>
            <ta e="T68" id="Seg_1974" s="T67">туда-INDEF-EL.3SG</ta>
            <ta e="T69" id="Seg_1975" s="T68">палка-ACC</ta>
            <ta e="T70" id="Seg_1976" s="T69">палка-PL-ACC</ta>
            <ta e="T71" id="Seg_1977" s="T70">раскидать-PST-3SG.O</ta>
            <ta e="T72" id="Seg_1978" s="T71">мы.DU.[NOM]</ta>
            <ta e="T73" id="Seg_1979" s="T72">этот</ta>
            <ta e="T74" id="Seg_1980" s="T73">палка-GEN</ta>
            <ta e="T75" id="Seg_1981" s="T74">верхняя.часть-PROL-3SG</ta>
            <ta e="T76" id="Seg_1982" s="T75">выйти-PST-1DU</ta>
            <ta e="T77" id="Seg_1983" s="T76">на.берег</ta>
            <ta e="T78" id="Seg_1984" s="T77">на.берег</ta>
            <ta e="T79" id="Seg_1985" s="T78">выйти-CVB2</ta>
            <ta e="T80" id="Seg_1986" s="T79">смородина-TRL</ta>
            <ta e="T81" id="Seg_1987" s="T80">собрать-PST-1PL</ta>
            <ta e="T82" id="Seg_1988" s="T81">смородина-ACC</ta>
            <ta e="T83" id="Seg_1989" s="T82">собрать-INF</ta>
            <ta e="T84" id="Seg_1990" s="T83">и</ta>
            <ta e="T85" id="Seg_1991" s="T84">чаща-ACC</ta>
            <ta e="T86" id="Seg_1992" s="T85">проколоть-DUR-CVB</ta>
            <ta e="T87" id="Seg_1993" s="T86">и</ta>
            <ta e="T88" id="Seg_1994" s="T87">смородина-ACC</ta>
            <ta e="T89" id="Seg_1995" s="T88">NEG</ta>
            <ta e="T90" id="Seg_1996" s="T89">найти-PST-1PL</ta>
            <ta e="T91" id="Seg_1997" s="T90">дорога-ILL</ta>
            <ta e="T92" id="Seg_1998" s="T91">назад</ta>
            <ta e="T93" id="Seg_1999" s="T92">выйти-PST-1PL</ta>
            <ta e="T94" id="Seg_2000" s="T93">один</ta>
            <ta e="T95" id="Seg_2001" s="T94">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T96" id="Seg_2002" s="T95">назад</ta>
            <ta e="T97" id="Seg_2003" s="T96">вернуться-PST.[3SG.S]</ta>
            <ta e="T98" id="Seg_2004" s="T97">Вера.[NOM]</ta>
            <ta e="T99" id="Seg_2005" s="T98">Демидовна</ta>
            <ta e="T100" id="Seg_2006" s="T99">NEG</ta>
            <ta e="T101" id="Seg_2007" s="T100">хотеть-RES-3SG.S</ta>
            <ta e="T102" id="Seg_2008" s="T101">ягода-EP.[NOM]</ta>
            <ta e="T103" id="Seg_2009" s="T102">собрать-IPFV-INF</ta>
            <ta e="T104" id="Seg_2010" s="T103">я.GEN</ta>
            <ta e="T105" id="Seg_2011" s="T104">нога.[NOM]-1SG</ta>
            <ta e="T106" id="Seg_2012" s="T105">болеть-INFER.[3SG.S]</ta>
            <ta e="T107" id="Seg_2013" s="T106">ходить-INF</ta>
            <ta e="T108" id="Seg_2014" s="T107">чаща-ACC</ta>
            <ta e="T109" id="Seg_2015" s="T108">проколоть-DUR-INF</ta>
            <ta e="T110" id="Seg_2016" s="T109">назад</ta>
            <ta e="T111" id="Seg_2017" s="T110">отправиться-FUT-1SG.S</ta>
            <ta e="T112" id="Seg_2018" s="T111">а.то</ta>
            <ta e="T113" id="Seg_2019" s="T112">утро-день.[NOM]</ta>
            <ta e="T114" id="Seg_2020" s="T113">и</ta>
            <ta e="T115" id="Seg_2021" s="T114">нога-EP-ILL</ta>
            <ta e="T116" id="Seg_2022" s="T115">NEG</ta>
            <ta e="T117" id="Seg_2023" s="T116">стоять-RES-DRV-FUT-2SG.S</ta>
            <ta e="T118" id="Seg_2024" s="T117">ну</ta>
            <ta e="T119" id="Seg_2025" s="T118">а</ta>
            <ta e="T120" id="Seg_2026" s="T119">мы.[NOM]</ta>
            <ta e="T121" id="Seg_2027" s="T120">сказать-DRV-HAB-1DU</ta>
            <ta e="T122" id="Seg_2028" s="T121">ходить-IMP.2SG.S</ta>
            <ta e="T123" id="Seg_2029" s="T122">назад</ta>
            <ta e="T124" id="Seg_2030" s="T123">мы.DU.[NOM]</ta>
            <ta e="T125" id="Seg_2031" s="T124">сам.1DU</ta>
            <ta e="T126" id="Seg_2032" s="T125">назад</ta>
            <ta e="T127" id="Seg_2033" s="T126">отправиться-FUT-1DU-FUT</ta>
            <ta e="T128" id="Seg_2034" s="T127">туда-ADV.LOC</ta>
            <ta e="T129" id="Seg_2035" s="T128">ягода.[NOM]</ta>
            <ta e="T130" id="Seg_2036" s="T129">быть-3SG.S</ta>
            <ta e="T131" id="Seg_2037" s="T130">али</ta>
            <ta e="T132" id="Seg_2038" s="T131">NEG.EX-CO.[3SG.S]</ta>
            <ta e="T133" id="Seg_2039" s="T132">отправиться-CO-1DU</ta>
            <ta e="T134" id="Seg_2040" s="T133">ягода-EP-ACC</ta>
            <ta e="T135" id="Seg_2041" s="T134">найти-CO-1DU</ta>
            <ta e="T136" id="Seg_2042" s="T135">ягода-EP-ACC</ta>
            <ta e="T137" id="Seg_2043" s="T136">собирать-1DU</ta>
            <ta e="T138" id="Seg_2044" s="T137">ведерко-COR</ta>
            <ta e="T139" id="Seg_2045" s="T138">источник-EP-GEN</ta>
            <ta e="T140" id="Seg_2046" s="T139">через</ta>
            <ta e="T141" id="Seg_2047" s="T140">переплыть-PST-1DU</ta>
            <ta e="T142" id="Seg_2048" s="T141">пешком</ta>
            <ta e="T143" id="Seg_2049" s="T142">бродить-CVB</ta>
            <ta e="T144" id="Seg_2050" s="T143">чарки-ACC</ta>
            <ta e="T145" id="Seg_2051" s="T144">снять-PFV-CVB2</ta>
            <ta e="T146" id="Seg_2052" s="T145">чарки-PL-ACC</ta>
            <ta e="T147" id="Seg_2053" s="T146">снять-PFV-CVB</ta>
            <ta e="T148" id="Seg_2054" s="T147">голый</ta>
            <ta e="T149" id="Seg_2055" s="T148">нога-EP-ADV.LOC</ta>
            <ta e="T150" id="Seg_2056" s="T149">другой</ta>
            <ta e="T151" id="Seg_2057" s="T150">сторона-ILL</ta>
            <ta e="T152" id="Seg_2058" s="T151">переплыть-CVB</ta>
            <ta e="T153" id="Seg_2059" s="T152">чарки-PL-ACC</ta>
            <ta e="T154" id="Seg_2060" s="T153">надеть-PST-1DU</ta>
            <ta e="T155" id="Seg_2061" s="T154">и</ta>
            <ta e="T156" id="Seg_2062" s="T155">туда</ta>
            <ta e="T157" id="Seg_2063" s="T156">сюда-EL.3SG-ADVZ</ta>
            <ta e="T158" id="Seg_2064" s="T157">опять</ta>
            <ta e="T159" id="Seg_2065" s="T158">чарки-PL-ACC</ta>
            <ta e="T160" id="Seg_2066" s="T159">снять-PFV-HAB-PST-1DU</ta>
            <ta e="T161" id="Seg_2067" s="T160">два</ta>
            <ta e="T162" id="Seg_2068" s="T161">раз.[NOM]</ta>
            <ta e="T163" id="Seg_2069" s="T162">бродить-CVB</ta>
            <ta e="T164" id="Seg_2070" s="T163">переплыть-HAB-CO-1DU</ta>
            <ta e="T165" id="Seg_2071" s="T164">на.берег</ta>
            <ta e="T166" id="Seg_2072" s="T165">выйти-1DU</ta>
            <ta e="T167" id="Seg_2073" s="T166">ил-GEN</ta>
            <ta e="T168" id="Seg_2074" s="T167">верхняя.часть-EP-LOC</ta>
            <ta e="T169" id="Seg_2075" s="T168">чарки.[NOM]-EP-1SG</ta>
            <ta e="T170" id="Seg_2076" s="T169">ил-ILL</ta>
            <ta e="T171" id="Seg_2077" s="T170">увязнуть-HAB-3SG.S</ta>
            <ta e="T172" id="Seg_2078" s="T171">я.NOM</ta>
            <ta e="T173" id="Seg_2079" s="T172">рука-EP-INSTR</ta>
            <ta e="T174" id="Seg_2080" s="T173">вытащить-HAB-1SG.O</ta>
            <ta e="T175" id="Seg_2081" s="T174">и</ta>
            <ta e="T176" id="Seg_2082" s="T175">нога-EP-LOC</ta>
            <ta e="T177" id="Seg_2083" s="T176">насадить-HAB-1SG.O</ta>
            <ta e="T178" id="Seg_2084" s="T177">другой</ta>
            <ta e="T179" id="Seg_2085" s="T178">нога.[NOM]-EP-1SG</ta>
            <ta e="T180" id="Seg_2086" s="T179">увязнуть-HAB-INFER.[3SG.S]</ta>
            <ta e="T181" id="Seg_2087" s="T180">ил-ILL</ta>
            <ta e="T182" id="Seg_2088" s="T181">опять</ta>
            <ta e="T183" id="Seg_2089" s="T182">другой</ta>
            <ta e="T184" id="Seg_2090" s="T183">чарки.[NOM]-EP-1SG</ta>
            <ta e="T185" id="Seg_2091" s="T184">притащить.волоком-HAB-1SG.O</ta>
            <ta e="T186" id="Seg_2092" s="T185">рука-EP-INSTR</ta>
            <ta e="T187" id="Seg_2093" s="T186">опять</ta>
            <ta e="T188" id="Seg_2094" s="T187">нога-EP-LOC</ta>
            <ta e="T189" id="Seg_2095" s="T188">насадить-HAB-1SG.O</ta>
            <ta e="T190" id="Seg_2096" s="T189">так</ta>
            <ta e="T191" id="Seg_2097" s="T190">на.берег</ta>
            <ta e="T192" id="Seg_2098" s="T191">выйти-1DU</ta>
            <ta e="T193" id="Seg_2099" s="T192">дорога-ILL</ta>
            <ta e="T194" id="Seg_2100" s="T193">трава-ACC</ta>
            <ta e="T195" id="Seg_2101" s="T194">проколоть-DUR-CVB</ta>
            <ta e="T196" id="Seg_2102" s="T195">выйти-1DU</ta>
            <ta e="T197" id="Seg_2103" s="T196">этот</ta>
            <ta e="T198" id="Seg_2104" s="T197">приблизительно.как</ta>
            <ta e="T199" id="Seg_2105" s="T198">устать-DRV-1DU</ta>
            <ta e="T200" id="Seg_2106" s="T199">как</ta>
            <ta e="T201" id="Seg_2107" s="T200">опьянеть-DUR-CVB</ta>
            <ta e="T202" id="Seg_2108" s="T201">дом.[NOM]</ta>
            <ta e="T203" id="Seg_2109" s="T202">прийти-CO-1DU</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_2110" s="T1">num-num&gt;n-n:poss</ta>
            <ta e="T3" id="Seg_2111" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_2112" s="T3">n-n:ins-n:case</ta>
            <ta e="T5" id="Seg_2113" s="T4">v-v:inf</ta>
            <ta e="T6" id="Seg_2114" s="T5">v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_2115" s="T6">conj</ta>
            <ta e="T8" id="Seg_2116" s="T7">interrog.[n:case]</ta>
            <ta e="T9" id="Seg_2117" s="T8">n-n:ins-n:case</ta>
            <ta e="T10" id="Seg_2118" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_2119" s="T10">v-v:ins-v:pn</ta>
            <ta e="T12" id="Seg_2120" s="T11">num</ta>
            <ta e="T13" id="Seg_2121" s="T12">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T14" id="Seg_2122" s="T13">adv</ta>
            <ta e="T15" id="Seg_2123" s="T14">v-v:ins-v:pn</ta>
            <ta e="T16" id="Seg_2124" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_2125" s="T16">conj</ta>
            <ta e="T18" id="Seg_2126" s="T17">pers.[n:case]</ta>
            <ta e="T19" id="Seg_2127" s="T18">num-num&gt;num</ta>
            <ta e="T20" id="Seg_2128" s="T19">v-v:ins-v:pn</ta>
            <ta e="T21" id="Seg_2129" s="T20">adv-adv&gt;adv</ta>
            <ta e="T22" id="Seg_2130" s="T21">adv</ta>
            <ta e="T23" id="Seg_2131" s="T22">v-v&gt;adv</ta>
            <ta e="T24" id="Seg_2132" s="T23">n-n:ins-n:case</ta>
            <ta e="T25" id="Seg_2133" s="T24">v-v:ins-v:pn</ta>
            <ta e="T26" id="Seg_2134" s="T25">n-n:ins-n:case</ta>
            <ta e="T27" id="Seg_2135" s="T26">v-v:pn</ta>
            <ta e="T28" id="Seg_2136" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_2137" s="T28">n-n:case.poss</ta>
            <ta e="T30" id="Seg_2138" s="T29">adv</ta>
            <ta e="T31" id="Seg_2139" s="T30">v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_2140" s="T31">num-num&gt;n-n:poss</ta>
            <ta e="T33" id="Seg_2141" s="T32">adj</ta>
            <ta e="T34" id="Seg_2142" s="T33">n-n&gt;n.[n:case]</ta>
            <ta e="T35" id="Seg_2143" s="T34">v-v:tense-v:pn</ta>
            <ta e="T36" id="Seg_2144" s="T35">n-n:case</ta>
            <ta e="T37" id="Seg_2145" s="T36">v-v:tense-v:pn</ta>
            <ta e="T38" id="Seg_2146" s="T37">v-v:tense-v:pn</ta>
            <ta e="T39" id="Seg_2147" s="T38">adj</ta>
            <ta e="T40" id="Seg_2148" s="T39">n-n:case-%%</ta>
            <ta e="T41" id="Seg_2149" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_2150" s="T41">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T43" id="Seg_2151" s="T42">v-v&gt;v-v&gt;adv</ta>
            <ta e="T44" id="Seg_2152" s="T43">v-v:tense-v:pn</ta>
            <ta e="T45" id="Seg_2153" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_2154" s="T45">v-v&gt;v-v&gt;adv</ta>
            <ta e="T47" id="Seg_2155" s="T46">num</ta>
            <ta e="T48" id="Seg_2156" s="T47">n-n:case</ta>
            <ta e="T49" id="Seg_2157" s="T48">adv</ta>
            <ta e="T50" id="Seg_2158" s="T49">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T51" id="Seg_2159" s="T50">adv</ta>
            <ta e="T52" id="Seg_2160" s="T51">v-v&gt;adv</ta>
            <ta e="T53" id="Seg_2161" s="T52">v-v:ins-v:tense-v:pn</ta>
            <ta e="T54" id="Seg_2162" s="T53">n-n:case</ta>
            <ta e="T55" id="Seg_2163" s="T54">n-n:ins-n:case</ta>
            <ta e="T56" id="Seg_2164" s="T55">adv</ta>
            <ta e="T57" id="Seg_2165" s="T56">v-v:tense-v:pn</ta>
            <ta e="T58" id="Seg_2166" s="T57">n-n:case.poss</ta>
            <ta e="T59" id="Seg_2167" s="T58">n.[n:case]</ta>
            <ta e="T60" id="Seg_2168" s="T59">v-v&gt;v-v:pn</ta>
            <ta e="T61" id="Seg_2169" s="T60">n.[n:case]</ta>
            <ta e="T62" id="Seg_2170" s="T61">num</ta>
            <ta e="T63" id="Seg_2171" s="T62">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T64" id="Seg_2172" s="T63">adv</ta>
            <ta e="T65" id="Seg_2173" s="T64">v-v:tense.[v:pn]</ta>
            <ta e="T66" id="Seg_2174" s="T65">n-n:case</ta>
            <ta e="T67" id="Seg_2175" s="T66">n-n:ins-n:case</ta>
            <ta e="T68" id="Seg_2176" s="T67">adv-clit-n:case.poss</ta>
            <ta e="T69" id="Seg_2177" s="T68">n-n:case</ta>
            <ta e="T70" id="Seg_2178" s="T69">n-n:num-n:case</ta>
            <ta e="T71" id="Seg_2179" s="T70">v-v:tense-v:pn</ta>
            <ta e="T72" id="Seg_2180" s="T71">pers.[n:case]</ta>
            <ta e="T73" id="Seg_2181" s="T72">dem</ta>
            <ta e="T74" id="Seg_2182" s="T73">n-n:case</ta>
            <ta e="T75" id="Seg_2183" s="T74">n-n:case-n:poss</ta>
            <ta e="T76" id="Seg_2184" s="T75">v-v:tense-v:pn</ta>
            <ta e="T77" id="Seg_2185" s="T76">adv</ta>
            <ta e="T78" id="Seg_2186" s="T77">adv</ta>
            <ta e="T79" id="Seg_2187" s="T78">v-v&gt;adv</ta>
            <ta e="T80" id="Seg_2188" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_2189" s="T80">v-v:tense-v:pn</ta>
            <ta e="T82" id="Seg_2190" s="T81">n-n:case</ta>
            <ta e="T83" id="Seg_2191" s="T82">v-v:inf</ta>
            <ta e="T84" id="Seg_2192" s="T83">conj</ta>
            <ta e="T85" id="Seg_2193" s="T84">n-n:case</ta>
            <ta e="T86" id="Seg_2194" s="T85">v-v&gt;v-v&gt;adv</ta>
            <ta e="T87" id="Seg_2195" s="T86">conj</ta>
            <ta e="T88" id="Seg_2196" s="T87">n-n:case</ta>
            <ta e="T89" id="Seg_2197" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_2198" s="T89">v-v:tense-v:pn</ta>
            <ta e="T91" id="Seg_2199" s="T90">n-n:case</ta>
            <ta e="T92" id="Seg_2200" s="T91">adv</ta>
            <ta e="T93" id="Seg_2201" s="T92">v-v:tense-v:pn</ta>
            <ta e="T94" id="Seg_2202" s="T93">num</ta>
            <ta e="T95" id="Seg_2203" s="T94">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T96" id="Seg_2204" s="T95">adv</ta>
            <ta e="T97" id="Seg_2205" s="T96">v-v:tense.[v:pn]</ta>
            <ta e="T98" id="Seg_2206" s="T97">nprop.[n:case]</ta>
            <ta e="T99" id="Seg_2207" s="T98">nprop</ta>
            <ta e="T100" id="Seg_2208" s="T99">ptcl</ta>
            <ta e="T101" id="Seg_2209" s="T100">v-v&gt;v-v:pn</ta>
            <ta e="T102" id="Seg_2210" s="T101">n-n:ins.[n:case]</ta>
            <ta e="T103" id="Seg_2211" s="T102">v-v&gt;v-v:inf</ta>
            <ta e="T104" id="Seg_2212" s="T103">pers</ta>
            <ta e="T105" id="Seg_2213" s="T104">n.[n:case]-n:poss</ta>
            <ta e="T106" id="Seg_2214" s="T105">v-v:mood.[v:pn]</ta>
            <ta e="T107" id="Seg_2215" s="T106">v-v:inf</ta>
            <ta e="T108" id="Seg_2216" s="T107">n-n:case</ta>
            <ta e="T109" id="Seg_2217" s="T108">v-v&gt;v-v:inf</ta>
            <ta e="T110" id="Seg_2218" s="T109">adv</ta>
            <ta e="T111" id="Seg_2219" s="T110">v-v:tense-v:pn</ta>
            <ta e="T112" id="Seg_2220" s="T111">conj</ta>
            <ta e="T113" id="Seg_2221" s="T112">n-n.[n:case]</ta>
            <ta e="T114" id="Seg_2222" s="T113">conj</ta>
            <ta e="T115" id="Seg_2223" s="T114">n-n:ins-n:case</ta>
            <ta e="T116" id="Seg_2224" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_2225" s="T116">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T118" id="Seg_2226" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_2227" s="T118">conj</ta>
            <ta e="T120" id="Seg_2228" s="T119">pers.[n:case]</ta>
            <ta e="T121" id="Seg_2229" s="T120">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T122" id="Seg_2230" s="T121">v-v:mood.pn</ta>
            <ta e="T123" id="Seg_2231" s="T122">adv</ta>
            <ta e="T124" id="Seg_2232" s="T123">pers.[n:case]</ta>
            <ta e="T125" id="Seg_2233" s="T124">emphpro</ta>
            <ta e="T126" id="Seg_2234" s="T125">adv</ta>
            <ta e="T127" id="Seg_2235" s="T126">v-v:tense-v:pn-v:tense</ta>
            <ta e="T128" id="Seg_2236" s="T127">adv-adv:case</ta>
            <ta e="T129" id="Seg_2237" s="T128">n.[n:case]</ta>
            <ta e="T130" id="Seg_2238" s="T129">v-v:pn</ta>
            <ta e="T131" id="Seg_2239" s="T130">conj</ta>
            <ta e="T132" id="Seg_2240" s="T131">v-v:ins.[v:pn]</ta>
            <ta e="T133" id="Seg_2241" s="T132">v-v:ins-v:pn</ta>
            <ta e="T134" id="Seg_2242" s="T133">n-n:ins-n:case</ta>
            <ta e="T135" id="Seg_2243" s="T134">v-v:ins-v:pn</ta>
            <ta e="T136" id="Seg_2244" s="T135">n-n:ins-n:case</ta>
            <ta e="T137" id="Seg_2245" s="T136">v-v:pn</ta>
            <ta e="T138" id="Seg_2246" s="T137">n-n:case</ta>
            <ta e="T139" id="Seg_2247" s="T138">n-n:ins-n:case</ta>
            <ta e="T140" id="Seg_2248" s="T139">pp</ta>
            <ta e="T141" id="Seg_2249" s="T140">v-v:tense-v:pn</ta>
            <ta e="T142" id="Seg_2250" s="T141">adv</ta>
            <ta e="T143" id="Seg_2251" s="T142">v-v&gt;adv</ta>
            <ta e="T144" id="Seg_2252" s="T143">n-n:case</ta>
            <ta e="T145" id="Seg_2253" s="T144">v-v&gt;v-v&gt;adv</ta>
            <ta e="T146" id="Seg_2254" s="T145">n-n:num-n:case</ta>
            <ta e="T147" id="Seg_2255" s="T146">v-v&gt;v-v&gt;adv</ta>
            <ta e="T148" id="Seg_2256" s="T147">adj</ta>
            <ta e="T149" id="Seg_2257" s="T148">n-n:ins-n&gt;adv</ta>
            <ta e="T150" id="Seg_2258" s="T149">adj</ta>
            <ta e="T151" id="Seg_2259" s="T150">n-n:case</ta>
            <ta e="T152" id="Seg_2260" s="T151">v-v&gt;adv</ta>
            <ta e="T153" id="Seg_2261" s="T152">n-n:num-n:case</ta>
            <ta e="T154" id="Seg_2262" s="T153">v-v:tense-v:pn</ta>
            <ta e="T155" id="Seg_2263" s="T154">conj</ta>
            <ta e="T156" id="Seg_2264" s="T155">adv</ta>
            <ta e="T157" id="Seg_2265" s="T156">adv-n:case.poss-adj&gt;adv</ta>
            <ta e="T158" id="Seg_2266" s="T157">adv</ta>
            <ta e="T159" id="Seg_2267" s="T158">n-n:num-n:case</ta>
            <ta e="T160" id="Seg_2268" s="T159">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T161" id="Seg_2269" s="T160">num</ta>
            <ta e="T162" id="Seg_2270" s="T161">n.[n:case]</ta>
            <ta e="T163" id="Seg_2271" s="T162">v-v&gt;adv</ta>
            <ta e="T164" id="Seg_2272" s="T163">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T165" id="Seg_2273" s="T164">adv</ta>
            <ta e="T166" id="Seg_2274" s="T165">v-v:pn</ta>
            <ta e="T167" id="Seg_2275" s="T166">n-n:case</ta>
            <ta e="T168" id="Seg_2276" s="T167">n-n:ins-n:case</ta>
            <ta e="T169" id="Seg_2277" s="T168">n.[n:case]-n:ins-n:poss</ta>
            <ta e="T170" id="Seg_2278" s="T169">n-n:case</ta>
            <ta e="T171" id="Seg_2279" s="T170">v-v&gt;v-v:pn</ta>
            <ta e="T172" id="Seg_2280" s="T171">pers</ta>
            <ta e="T173" id="Seg_2281" s="T172">n-n:ins-n:case</ta>
            <ta e="T174" id="Seg_2282" s="T173">v-v&gt;v-v:pn</ta>
            <ta e="T175" id="Seg_2283" s="T174">conj</ta>
            <ta e="T176" id="Seg_2284" s="T175">n-n:ins-n:case</ta>
            <ta e="T177" id="Seg_2285" s="T176">v-v&gt;v-v:pn</ta>
            <ta e="T178" id="Seg_2286" s="T177">adj</ta>
            <ta e="T179" id="Seg_2287" s="T178">n.[n:case]-n:ins-n:poss</ta>
            <ta e="T180" id="Seg_2288" s="T179">v-v&gt;v-v:mood.[v:pn]</ta>
            <ta e="T181" id="Seg_2289" s="T180">n-n:case</ta>
            <ta e="T182" id="Seg_2290" s="T181">adv</ta>
            <ta e="T183" id="Seg_2291" s="T182">adj</ta>
            <ta e="T184" id="Seg_2292" s="T183">n.[n:case]-n:ins-n:poss</ta>
            <ta e="T185" id="Seg_2293" s="T184">v-v&gt;v-v:pn</ta>
            <ta e="T186" id="Seg_2294" s="T185">n-n:ins-n:case</ta>
            <ta e="T187" id="Seg_2295" s="T186">adv</ta>
            <ta e="T188" id="Seg_2296" s="T187">n-n:ins-n:case</ta>
            <ta e="T189" id="Seg_2297" s="T188">v-v&gt;v-v:pn</ta>
            <ta e="T190" id="Seg_2298" s="T189">adv</ta>
            <ta e="T191" id="Seg_2299" s="T190">adv</ta>
            <ta e="T192" id="Seg_2300" s="T191">v-v:pn</ta>
            <ta e="T193" id="Seg_2301" s="T192">n-n:case</ta>
            <ta e="T194" id="Seg_2302" s="T193">n-n:case</ta>
            <ta e="T195" id="Seg_2303" s="T194">v-v&gt;v-v&gt;adv</ta>
            <ta e="T196" id="Seg_2304" s="T195">v-v:pn</ta>
            <ta e="T197" id="Seg_2305" s="T196">dem</ta>
            <ta e="T198" id="Seg_2306" s="T197">pp</ta>
            <ta e="T199" id="Seg_2307" s="T198">v-v&gt;v-v:pn</ta>
            <ta e="T200" id="Seg_2308" s="T199">conj</ta>
            <ta e="T201" id="Seg_2309" s="T200">v-v&gt;v-v&gt;adv</ta>
            <ta e="T202" id="Seg_2310" s="T201">n.[n:case]</ta>
            <ta e="T203" id="Seg_2311" s="T202">v-v:ins-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_2312" s="T1">n</ta>
            <ta e="T3" id="Seg_2313" s="T2">v</ta>
            <ta e="T4" id="Seg_2314" s="T3">n</ta>
            <ta e="T5" id="Seg_2315" s="T4">v</ta>
            <ta e="T6" id="Seg_2316" s="T5">v</ta>
            <ta e="T7" id="Seg_2317" s="T6">conj</ta>
            <ta e="T8" id="Seg_2318" s="T7">interrog</ta>
            <ta e="T9" id="Seg_2319" s="T8">n</ta>
            <ta e="T10" id="Seg_2320" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_2321" s="T10">v</ta>
            <ta e="T12" id="Seg_2322" s="T11">num</ta>
            <ta e="T13" id="Seg_2323" s="T12">n</ta>
            <ta e="T14" id="Seg_2324" s="T13">adv</ta>
            <ta e="T15" id="Seg_2325" s="T14">v</ta>
            <ta e="T16" id="Seg_2326" s="T15">n</ta>
            <ta e="T17" id="Seg_2327" s="T16">conj</ta>
            <ta e="T18" id="Seg_2328" s="T17">pers</ta>
            <ta e="T19" id="Seg_2329" s="T18">num</ta>
            <ta e="T20" id="Seg_2330" s="T19">v</ta>
            <ta e="T21" id="Seg_2331" s="T20">adv</ta>
            <ta e="T22" id="Seg_2332" s="T21">adv</ta>
            <ta e="T23" id="Seg_2333" s="T22">adv</ta>
            <ta e="T24" id="Seg_2334" s="T23">n</ta>
            <ta e="T25" id="Seg_2335" s="T24">v</ta>
            <ta e="T26" id="Seg_2336" s="T25">subs</ta>
            <ta e="T27" id="Seg_2337" s="T26">v</ta>
            <ta e="T28" id="Seg_2338" s="T27">n</ta>
            <ta e="T29" id="Seg_2339" s="T28">n</ta>
            <ta e="T30" id="Seg_2340" s="T29">adv</ta>
            <ta e="T31" id="Seg_2341" s="T30">v</ta>
            <ta e="T32" id="Seg_2342" s="T31">n</ta>
            <ta e="T33" id="Seg_2343" s="T32">adj</ta>
            <ta e="T34" id="Seg_2344" s="T33">n</ta>
            <ta e="T35" id="Seg_2345" s="T34">v</ta>
            <ta e="T36" id="Seg_2346" s="T35">n</ta>
            <ta e="T37" id="Seg_2347" s="T36">v</ta>
            <ta e="T38" id="Seg_2348" s="T37">v</ta>
            <ta e="T39" id="Seg_2349" s="T38">adj</ta>
            <ta e="T40" id="Seg_2350" s="T39">n</ta>
            <ta e="T41" id="Seg_2351" s="T40">n</ta>
            <ta e="T42" id="Seg_2352" s="T41">v</ta>
            <ta e="T43" id="Seg_2353" s="T42">adv</ta>
            <ta e="T44" id="Seg_2354" s="T43">v</ta>
            <ta e="T45" id="Seg_2355" s="T44">n</ta>
            <ta e="T46" id="Seg_2356" s="T45">adv</ta>
            <ta e="T47" id="Seg_2357" s="T46">num</ta>
            <ta e="T48" id="Seg_2358" s="T47">n</ta>
            <ta e="T49" id="Seg_2359" s="T48">adv</ta>
            <ta e="T50" id="Seg_2360" s="T49">v</ta>
            <ta e="T51" id="Seg_2361" s="T50">adv</ta>
            <ta e="T52" id="Seg_2362" s="T51">v</ta>
            <ta e="T53" id="Seg_2363" s="T52">v</ta>
            <ta e="T54" id="Seg_2364" s="T53">n</ta>
            <ta e="T55" id="Seg_2365" s="T54">pp</ta>
            <ta e="T56" id="Seg_2366" s="T55">adv</ta>
            <ta e="T57" id="Seg_2367" s="T56">v</ta>
            <ta e="T58" id="Seg_2368" s="T57">n</ta>
            <ta e="T59" id="Seg_2369" s="T58">n</ta>
            <ta e="T60" id="Seg_2370" s="T59">v</ta>
            <ta e="T61" id="Seg_2371" s="T60">n</ta>
            <ta e="T62" id="Seg_2372" s="T61">num</ta>
            <ta e="T63" id="Seg_2373" s="T62">n</ta>
            <ta e="T64" id="Seg_2374" s="T63">adv</ta>
            <ta e="T65" id="Seg_2375" s="T64">v</ta>
            <ta e="T66" id="Seg_2376" s="T65">n</ta>
            <ta e="T67" id="Seg_2377" s="T66">pp</ta>
            <ta e="T68" id="Seg_2378" s="T67">adv</ta>
            <ta e="T69" id="Seg_2379" s="T68">n</ta>
            <ta e="T70" id="Seg_2380" s="T69">n</ta>
            <ta e="T71" id="Seg_2381" s="T70">v</ta>
            <ta e="T72" id="Seg_2382" s="T71">pers</ta>
            <ta e="T73" id="Seg_2383" s="T72">dem</ta>
            <ta e="T74" id="Seg_2384" s="T73">n</ta>
            <ta e="T75" id="Seg_2385" s="T74">pp</ta>
            <ta e="T76" id="Seg_2386" s="T75">v</ta>
            <ta e="T77" id="Seg_2387" s="T76">adv</ta>
            <ta e="T78" id="Seg_2388" s="T77">adv</ta>
            <ta e="T79" id="Seg_2389" s="T78">adv</ta>
            <ta e="T80" id="Seg_2390" s="T79">n</ta>
            <ta e="T81" id="Seg_2391" s="T80">v</ta>
            <ta e="T82" id="Seg_2392" s="T81">n</ta>
            <ta e="T83" id="Seg_2393" s="T82">v</ta>
            <ta e="T84" id="Seg_2394" s="T83">conj</ta>
            <ta e="T85" id="Seg_2395" s="T84">n</ta>
            <ta e="T86" id="Seg_2396" s="T85">adv</ta>
            <ta e="T87" id="Seg_2397" s="T86">conj</ta>
            <ta e="T88" id="Seg_2398" s="T87">n</ta>
            <ta e="T89" id="Seg_2399" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_2400" s="T89">v</ta>
            <ta e="T91" id="Seg_2401" s="T90">n</ta>
            <ta e="T92" id="Seg_2402" s="T91">adv</ta>
            <ta e="T93" id="Seg_2403" s="T92">v</ta>
            <ta e="T94" id="Seg_2404" s="T93">num</ta>
            <ta e="T95" id="Seg_2405" s="T94">n</ta>
            <ta e="T96" id="Seg_2406" s="T95">adv</ta>
            <ta e="T97" id="Seg_2407" s="T96">v</ta>
            <ta e="T98" id="Seg_2408" s="T97">nprop</ta>
            <ta e="T99" id="Seg_2409" s="T98">nprop</ta>
            <ta e="T100" id="Seg_2410" s="T99">ptcl</ta>
            <ta e="T101" id="Seg_2411" s="T100">v</ta>
            <ta e="T102" id="Seg_2412" s="T101">n</ta>
            <ta e="T103" id="Seg_2413" s="T102">v</ta>
            <ta e="T104" id="Seg_2414" s="T103">pers</ta>
            <ta e="T105" id="Seg_2415" s="T104">n</ta>
            <ta e="T106" id="Seg_2416" s="T105">v</ta>
            <ta e="T107" id="Seg_2417" s="T106">v</ta>
            <ta e="T108" id="Seg_2418" s="T107">n</ta>
            <ta e="T109" id="Seg_2419" s="T108">v</ta>
            <ta e="T110" id="Seg_2420" s="T109">adv</ta>
            <ta e="T111" id="Seg_2421" s="T110">v</ta>
            <ta e="T112" id="Seg_2422" s="T111">conj</ta>
            <ta e="T113" id="Seg_2423" s="T112">adv</ta>
            <ta e="T114" id="Seg_2424" s="T113">conj</ta>
            <ta e="T115" id="Seg_2425" s="T114">n</ta>
            <ta e="T116" id="Seg_2426" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_2427" s="T116">v</ta>
            <ta e="T118" id="Seg_2428" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_2429" s="T118">conj</ta>
            <ta e="T120" id="Seg_2430" s="T119">pers</ta>
            <ta e="T121" id="Seg_2431" s="T120">v</ta>
            <ta e="T122" id="Seg_2432" s="T121">v</ta>
            <ta e="T123" id="Seg_2433" s="T122">adv</ta>
            <ta e="T124" id="Seg_2434" s="T123">pers</ta>
            <ta e="T125" id="Seg_2435" s="T124">emphpro</ta>
            <ta e="T126" id="Seg_2436" s="T125">adv</ta>
            <ta e="T127" id="Seg_2437" s="T126">v</ta>
            <ta e="T128" id="Seg_2438" s="T127">adv</ta>
            <ta e="T129" id="Seg_2439" s="T128">n</ta>
            <ta e="T130" id="Seg_2440" s="T129">v</ta>
            <ta e="T131" id="Seg_2441" s="T130">conj</ta>
            <ta e="T132" id="Seg_2442" s="T131">v</ta>
            <ta e="T133" id="Seg_2443" s="T132">v</ta>
            <ta e="T134" id="Seg_2444" s="T133">n</ta>
            <ta e="T135" id="Seg_2445" s="T134">v</ta>
            <ta e="T136" id="Seg_2446" s="T135">n</ta>
            <ta e="T137" id="Seg_2447" s="T136">v</ta>
            <ta e="T138" id="Seg_2448" s="T137">n</ta>
            <ta e="T139" id="Seg_2449" s="T138">n</ta>
            <ta e="T140" id="Seg_2450" s="T139">pp</ta>
            <ta e="T141" id="Seg_2451" s="T140">v</ta>
            <ta e="T142" id="Seg_2452" s="T141">adv</ta>
            <ta e="T143" id="Seg_2453" s="T142">adv</ta>
            <ta e="T144" id="Seg_2454" s="T143">n</ta>
            <ta e="T145" id="Seg_2455" s="T144">adv</ta>
            <ta e="T146" id="Seg_2456" s="T145">n</ta>
            <ta e="T147" id="Seg_2457" s="T146">adv</ta>
            <ta e="T148" id="Seg_2458" s="T147">adj</ta>
            <ta e="T149" id="Seg_2459" s="T148">n</ta>
            <ta e="T150" id="Seg_2460" s="T149">adj</ta>
            <ta e="T151" id="Seg_2461" s="T150">n</ta>
            <ta e="T152" id="Seg_2462" s="T151">adv</ta>
            <ta e="T153" id="Seg_2463" s="T152">n</ta>
            <ta e="T154" id="Seg_2464" s="T153">v</ta>
            <ta e="T155" id="Seg_2465" s="T154">conj</ta>
            <ta e="T156" id="Seg_2466" s="T155">adv</ta>
            <ta e="T157" id="Seg_2467" s="T156">adv</ta>
            <ta e="T158" id="Seg_2468" s="T157">adv</ta>
            <ta e="T159" id="Seg_2469" s="T158">n</ta>
            <ta e="T160" id="Seg_2470" s="T159">v</ta>
            <ta e="T161" id="Seg_2471" s="T160">num</ta>
            <ta e="T162" id="Seg_2472" s="T161">n</ta>
            <ta e="T163" id="Seg_2473" s="T162">adv</ta>
            <ta e="T164" id="Seg_2474" s="T163">v</ta>
            <ta e="T165" id="Seg_2475" s="T164">adv</ta>
            <ta e="T166" id="Seg_2476" s="T165">v</ta>
            <ta e="T167" id="Seg_2477" s="T166">n</ta>
            <ta e="T168" id="Seg_2478" s="T167">pp</ta>
            <ta e="T169" id="Seg_2479" s="T168">n</ta>
            <ta e="T170" id="Seg_2480" s="T169">n</ta>
            <ta e="T171" id="Seg_2481" s="T170">v</ta>
            <ta e="T172" id="Seg_2482" s="T171">pers</ta>
            <ta e="T173" id="Seg_2483" s="T172">n</ta>
            <ta e="T174" id="Seg_2484" s="T173">v</ta>
            <ta e="T175" id="Seg_2485" s="T174">conj</ta>
            <ta e="T176" id="Seg_2486" s="T175">n</ta>
            <ta e="T177" id="Seg_2487" s="T176">v</ta>
            <ta e="T178" id="Seg_2488" s="T177">adj</ta>
            <ta e="T179" id="Seg_2489" s="T178">n</ta>
            <ta e="T180" id="Seg_2490" s="T179">v</ta>
            <ta e="T181" id="Seg_2491" s="T180">n</ta>
            <ta e="T182" id="Seg_2492" s="T181">adv</ta>
            <ta e="T183" id="Seg_2493" s="T182">adj</ta>
            <ta e="T184" id="Seg_2494" s="T183">n</ta>
            <ta e="T185" id="Seg_2495" s="T184">v</ta>
            <ta e="T186" id="Seg_2496" s="T185">n</ta>
            <ta e="T187" id="Seg_2497" s="T186">adv</ta>
            <ta e="T188" id="Seg_2498" s="T187">n</ta>
            <ta e="T189" id="Seg_2499" s="T188">v</ta>
            <ta e="T190" id="Seg_2500" s="T189">adv</ta>
            <ta e="T191" id="Seg_2501" s="T190">adv</ta>
            <ta e="T192" id="Seg_2502" s="T191">v</ta>
            <ta e="T193" id="Seg_2503" s="T192">n</ta>
            <ta e="T194" id="Seg_2504" s="T193">n</ta>
            <ta e="T195" id="Seg_2505" s="T194">adv</ta>
            <ta e="T196" id="Seg_2506" s="T195">v</ta>
            <ta e="T197" id="Seg_2507" s="T196">dem</ta>
            <ta e="T198" id="Seg_2508" s="T197">pp</ta>
            <ta e="T199" id="Seg_2509" s="T198">v</ta>
            <ta e="T200" id="Seg_2510" s="T199">conj</ta>
            <ta e="T201" id="Seg_2511" s="T200">adv</ta>
            <ta e="T202" id="Seg_2512" s="T201">n</ta>
            <ta e="T203" id="Seg_2513" s="T202">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_2514" s="T1">np.h:A</ta>
            <ta e="T4" id="Seg_2515" s="T3">np:Th</ta>
            <ta e="T6" id="Seg_2516" s="T5">0.3.h:A</ta>
            <ta e="T9" id="Seg_2517" s="T8">np:Th</ta>
            <ta e="T11" id="Seg_2518" s="T10">0.3.h:A</ta>
            <ta e="T13" id="Seg_2519" s="T12">np.h:A</ta>
            <ta e="T14" id="Seg_2520" s="T13">adv:G</ta>
            <ta e="T16" id="Seg_2521" s="T15">np:G</ta>
            <ta e="T18" id="Seg_2522" s="T17">pro.h:A</ta>
            <ta e="T22" id="Seg_2523" s="T21">adv:G</ta>
            <ta e="T24" id="Seg_2524" s="T23">np:Th</ta>
            <ta e="T25" id="Seg_2525" s="T24">0.1.h:A</ta>
            <ta e="T26" id="Seg_2526" s="T25">np:Th</ta>
            <ta e="T27" id="Seg_2527" s="T26">0.1.h:A</ta>
            <ta e="T28" id="Seg_2528" s="T27">np:Th</ta>
            <ta e="T29" id="Seg_2529" s="T28">np:So</ta>
            <ta e="T30" id="Seg_2530" s="T29">adv:G</ta>
            <ta e="T32" id="Seg_2531" s="T31">np.h:A</ta>
            <ta e="T35" id="Seg_2532" s="T34">0.3.h:A</ta>
            <ta e="T36" id="Seg_2533" s="T35">np:Ins</ta>
            <ta e="T37" id="Seg_2534" s="T36">0.3.h:A</ta>
            <ta e="T40" id="Seg_2535" s="T39">np:G</ta>
            <ta e="T41" id="Seg_2536" s="T40">np:L</ta>
            <ta e="T42" id="Seg_2537" s="T41">0.3:Th</ta>
            <ta e="T44" id="Seg_2538" s="T43">0.3.h:A</ta>
            <ta e="T45" id="Seg_2539" s="T44">np:Ins</ta>
            <ta e="T48" id="Seg_2540" s="T47">np:L</ta>
            <ta e="T49" id="Seg_2541" s="T48">adv:G</ta>
            <ta e="T50" id="Seg_2542" s="T49">0.3.h:A</ta>
            <ta e="T51" id="Seg_2543" s="T50">adv:G</ta>
            <ta e="T53" id="Seg_2544" s="T52">0.3.h:A</ta>
            <ta e="T54" id="Seg_2545" s="T53">pp:G</ta>
            <ta e="T56" id="Seg_2546" s="T55">adv:G</ta>
            <ta e="T57" id="Seg_2547" s="T56">0.3.h:A</ta>
            <ta e="T58" id="Seg_2548" s="T57">np:G</ta>
            <ta e="T59" id="Seg_2549" s="T58">np:P</ta>
            <ta e="T63" id="Seg_2550" s="T62">np.h:A</ta>
            <ta e="T64" id="Seg_2551" s="T63">adv:Time</ta>
            <ta e="T66" id="Seg_2552" s="T65">pp:G</ta>
            <ta e="T68" id="Seg_2553" s="T67">adv:So</ta>
            <ta e="T69" id="Seg_2554" s="T68">np:Th</ta>
            <ta e="T71" id="Seg_2555" s="T70">0.3.h:A</ta>
            <ta e="T72" id="Seg_2556" s="T71">pro.h:A</ta>
            <ta e="T74" id="Seg_2557" s="T73">pp:Path</ta>
            <ta e="T77" id="Seg_2558" s="T76">adv:G</ta>
            <ta e="T78" id="Seg_2559" s="T77">adv:G</ta>
            <ta e="T80" id="Seg_2560" s="T79">np:Th</ta>
            <ta e="T81" id="Seg_2561" s="T80">0.1.h:A</ta>
            <ta e="T85" id="Seg_2562" s="T84">np:Th</ta>
            <ta e="T88" id="Seg_2563" s="T87">np:Th</ta>
            <ta e="T90" id="Seg_2564" s="T89">0.1.h:A</ta>
            <ta e="T91" id="Seg_2565" s="T90">np:G</ta>
            <ta e="T93" id="Seg_2566" s="T92">0.1.h:A</ta>
            <ta e="T95" id="Seg_2567" s="T94">np.h:A</ta>
            <ta e="T96" id="Seg_2568" s="T95">adv:G</ta>
            <ta e="T98" id="Seg_2569" s="T97">np.h:E</ta>
            <ta e="T102" id="Seg_2570" s="T101">np:Th</ta>
            <ta e="T103" id="Seg_2571" s="T102">v:Th</ta>
            <ta e="T105" id="Seg_2572" s="T104">np:P 0.1.h:Poss</ta>
            <ta e="T110" id="Seg_2573" s="T109">adv:G</ta>
            <ta e="T111" id="Seg_2574" s="T110">0.1.h:A</ta>
            <ta e="T113" id="Seg_2575" s="T112">np:Time</ta>
            <ta e="T115" id="Seg_2576" s="T114">np:L</ta>
            <ta e="T117" id="Seg_2577" s="T116">0.2.h:Th</ta>
            <ta e="T120" id="Seg_2578" s="T119">pro.h:A</ta>
            <ta e="T122" id="Seg_2579" s="T121">0.2.h:A</ta>
            <ta e="T123" id="Seg_2580" s="T122">adv:G</ta>
            <ta e="T124" id="Seg_2581" s="T123">pro.h:A</ta>
            <ta e="T126" id="Seg_2582" s="T125">adv:G</ta>
            <ta e="T128" id="Seg_2583" s="T127">adv:L</ta>
            <ta e="T129" id="Seg_2584" s="T128">np:Th</ta>
            <ta e="T132" id="Seg_2585" s="T131">0.3:Th</ta>
            <ta e="T133" id="Seg_2586" s="T132">0.1.h:A</ta>
            <ta e="T134" id="Seg_2587" s="T133">np:Th</ta>
            <ta e="T135" id="Seg_2588" s="T134">0.1.h:A</ta>
            <ta e="T136" id="Seg_2589" s="T135">np:Th</ta>
            <ta e="T137" id="Seg_2590" s="T136">0.1.h:A</ta>
            <ta e="T139" id="Seg_2591" s="T138">pp:Path</ta>
            <ta e="T141" id="Seg_2592" s="T140">0.1.h:A</ta>
            <ta e="T144" id="Seg_2593" s="T143">np:Th</ta>
            <ta e="T151" id="Seg_2594" s="T150">np:G</ta>
            <ta e="T153" id="Seg_2595" s="T152">np:Th</ta>
            <ta e="T154" id="Seg_2596" s="T153">0.1.h:A</ta>
            <ta e="T159" id="Seg_2597" s="T158">np:Th</ta>
            <ta e="T160" id="Seg_2598" s="T159">0.1.h:A</ta>
            <ta e="T162" id="Seg_2599" s="T161">np:Time</ta>
            <ta e="T164" id="Seg_2600" s="T163">0.1.h:A</ta>
            <ta e="T165" id="Seg_2601" s="T164">adv:G</ta>
            <ta e="T166" id="Seg_2602" s="T165">0.1.h:A</ta>
            <ta e="T167" id="Seg_2603" s="T166">pp:L</ta>
            <ta e="T169" id="Seg_2604" s="T168">np:P 0.1.h:Poss</ta>
            <ta e="T170" id="Seg_2605" s="T169">np:G</ta>
            <ta e="T172" id="Seg_2606" s="T171">pro.h:A</ta>
            <ta e="T173" id="Seg_2607" s="T172">np:Ins</ta>
            <ta e="T176" id="Seg_2608" s="T175">np:G</ta>
            <ta e="T177" id="Seg_2609" s="T176">0.1.h:A 0.3:Th</ta>
            <ta e="T179" id="Seg_2610" s="T178">np:P 0.1.h:Poss</ta>
            <ta e="T181" id="Seg_2611" s="T180">np:G</ta>
            <ta e="T184" id="Seg_2612" s="T183">np:Th 0.1.h:Poss</ta>
            <ta e="T185" id="Seg_2613" s="T184">0.1.h:A</ta>
            <ta e="T186" id="Seg_2614" s="T185">np:Ins</ta>
            <ta e="T188" id="Seg_2615" s="T187">np:G</ta>
            <ta e="T189" id="Seg_2616" s="T188">0.1.h:A 0.3:Th</ta>
            <ta e="T191" id="Seg_2617" s="T190">adv:G</ta>
            <ta e="T192" id="Seg_2618" s="T191">0.1.h:A</ta>
            <ta e="T193" id="Seg_2619" s="T192">np:G</ta>
            <ta e="T194" id="Seg_2620" s="T193">np:Th</ta>
            <ta e="T196" id="Seg_2621" s="T195">0.1.h:A</ta>
            <ta e="T199" id="Seg_2622" s="T198">0.1.h:E</ta>
            <ta e="T202" id="Seg_2623" s="T201">np:G</ta>
            <ta e="T203" id="Seg_2624" s="T202">0.1.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_2625" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_2626" s="T2">v:pred</ta>
            <ta e="T5" id="Seg_2627" s="T3">s:purp</ta>
            <ta e="T6" id="Seg_2628" s="T5">0.3.h:S v:pred</ta>
            <ta e="T9" id="Seg_2629" s="T8">np:O</ta>
            <ta e="T11" id="Seg_2630" s="T10">0.3.h:S v:pred</ta>
            <ta e="T13" id="Seg_2631" s="T12">np.h:S</ta>
            <ta e="T15" id="Seg_2632" s="T14">v:pred</ta>
            <ta e="T18" id="Seg_2633" s="T17">pro.h:S</ta>
            <ta e="T20" id="Seg_2634" s="T19">v:pred</ta>
            <ta e="T23" id="Seg_2635" s="T21">s:temp</ta>
            <ta e="T24" id="Seg_2636" s="T23">np:O</ta>
            <ta e="T25" id="Seg_2637" s="T24">0.1.h:S v:pred</ta>
            <ta e="T26" id="Seg_2638" s="T25">np:O</ta>
            <ta e="T27" id="Seg_2639" s="T26">0.1.h:S v:pred</ta>
            <ta e="T28" id="Seg_2640" s="T27">np:O</ta>
            <ta e="T31" id="Seg_2641" s="T30">v:pred</ta>
            <ta e="T32" id="Seg_2642" s="T31">np.h:S</ta>
            <ta e="T35" id="Seg_2643" s="T34">0.3.h:S v:pred</ta>
            <ta e="T37" id="Seg_2644" s="T36">0.3.h:S v:pred</ta>
            <ta e="T42" id="Seg_2645" s="T41">0.3:S v:pred</ta>
            <ta e="T43" id="Seg_2646" s="T42">s:temp</ta>
            <ta e="T44" id="Seg_2647" s="T43">0.3.h:S v:pred</ta>
            <ta e="T46" id="Seg_2648" s="T44">s:temp</ta>
            <ta e="T50" id="Seg_2649" s="T49">0.3.h:S v:pred</ta>
            <ta e="T52" id="Seg_2650" s="T50">s:temp</ta>
            <ta e="T53" id="Seg_2651" s="T52">0.3.h:S v:pred</ta>
            <ta e="T57" id="Seg_2652" s="T56">0.3.h:S v:pred</ta>
            <ta e="T59" id="Seg_2653" s="T58">np:S</ta>
            <ta e="T60" id="Seg_2654" s="T59">v:pred</ta>
            <ta e="T63" id="Seg_2655" s="T62">np.h:S</ta>
            <ta e="T65" id="Seg_2656" s="T64">v:pred</ta>
            <ta e="T69" id="Seg_2657" s="T68">np:O</ta>
            <ta e="T71" id="Seg_2658" s="T70">0.3.h:S v:pred</ta>
            <ta e="T72" id="Seg_2659" s="T71">pro.h:S</ta>
            <ta e="T76" id="Seg_2660" s="T75">v:pred</ta>
            <ta e="T79" id="Seg_2661" s="T77">s:temp</ta>
            <ta e="T80" id="Seg_2662" s="T79">np:O</ta>
            <ta e="T81" id="Seg_2663" s="T80">0.1.h:S v:pred</ta>
            <ta e="T86" id="Seg_2664" s="T84">s:temp</ta>
            <ta e="T88" id="Seg_2665" s="T87">np:O</ta>
            <ta e="T90" id="Seg_2666" s="T89">0.1.h:S v:pred</ta>
            <ta e="T93" id="Seg_2667" s="T92">0.1.h:S v:pred</ta>
            <ta e="T95" id="Seg_2668" s="T94">np.h:S</ta>
            <ta e="T97" id="Seg_2669" s="T96">v:pred</ta>
            <ta e="T98" id="Seg_2670" s="T97">np.h:S</ta>
            <ta e="T101" id="Seg_2671" s="T100">v:pred</ta>
            <ta e="T103" id="Seg_2672" s="T102">v:O</ta>
            <ta e="T105" id="Seg_2673" s="T104">np:S</ta>
            <ta e="T106" id="Seg_2674" s="T105">v:pred</ta>
            <ta e="T111" id="Seg_2675" s="T110">0.1.h:S v:pred</ta>
            <ta e="T117" id="Seg_2676" s="T116">0.2.h:S v:pred</ta>
            <ta e="T120" id="Seg_2677" s="T119">pro.h:S</ta>
            <ta e="T121" id="Seg_2678" s="T120">v:pred</ta>
            <ta e="T122" id="Seg_2679" s="T121">0.2.h:S v:pred</ta>
            <ta e="T124" id="Seg_2680" s="T123">pro.h:S</ta>
            <ta e="T127" id="Seg_2681" s="T126">v:pred</ta>
            <ta e="T129" id="Seg_2682" s="T128">np:S</ta>
            <ta e="T130" id="Seg_2683" s="T129">v:pred</ta>
            <ta e="T132" id="Seg_2684" s="T131">0.3:S v:pred</ta>
            <ta e="T133" id="Seg_2685" s="T132">0.1.h:S v:pred</ta>
            <ta e="T134" id="Seg_2686" s="T133">np:O</ta>
            <ta e="T135" id="Seg_2687" s="T134">0.1.h:S v:pred</ta>
            <ta e="T136" id="Seg_2688" s="T135">np:O</ta>
            <ta e="T137" id="Seg_2689" s="T136">0.1.h:S v:pred</ta>
            <ta e="T141" id="Seg_2690" s="T140">0.1.h:S v:pred</ta>
            <ta e="T143" id="Seg_2691" s="T141">s:adv</ta>
            <ta e="T145" id="Seg_2692" s="T143">s:temp</ta>
            <ta e="T152" id="Seg_2693" s="T149">s:temp</ta>
            <ta e="T153" id="Seg_2694" s="T152">np:O</ta>
            <ta e="T154" id="Seg_2695" s="T153">0.1.h:S v:pred</ta>
            <ta e="T159" id="Seg_2696" s="T158">np:O</ta>
            <ta e="T160" id="Seg_2697" s="T159">0.1.h:S v:pred</ta>
            <ta e="T163" id="Seg_2698" s="T162">s:adv</ta>
            <ta e="T164" id="Seg_2699" s="T163">0.1.h:S v:pred</ta>
            <ta e="T166" id="Seg_2700" s="T165">0.1.h:S v:pred</ta>
            <ta e="T169" id="Seg_2701" s="T168">np:S</ta>
            <ta e="T171" id="Seg_2702" s="T170">v:pred</ta>
            <ta e="T172" id="Seg_2703" s="T171">pro.h:S</ta>
            <ta e="T174" id="Seg_2704" s="T173">v:pred</ta>
            <ta e="T177" id="Seg_2705" s="T176">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T179" id="Seg_2706" s="T178">np:S</ta>
            <ta e="T180" id="Seg_2707" s="T179">v:pred</ta>
            <ta e="T184" id="Seg_2708" s="T183">np:O</ta>
            <ta e="T185" id="Seg_2709" s="T184">0.1.h:S v:pred</ta>
            <ta e="T189" id="Seg_2710" s="T188">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T192" id="Seg_2711" s="T191">0.1.h:S v:pred</ta>
            <ta e="T195" id="Seg_2712" s="T193">s:temp</ta>
            <ta e="T196" id="Seg_2713" s="T195">0.1.h:S v:pred</ta>
            <ta e="T199" id="Seg_2714" s="T198">0.1.h:S v:pred</ta>
            <ta e="T201" id="Seg_2715" s="T199">s:adv</ta>
            <ta e="T203" id="Seg_2716" s="T202">0.1.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T7" id="Seg_2717" s="T6">RUS:gram</ta>
            <ta e="T17" id="Seg_2718" s="T16">RUS:gram</ta>
            <ta e="T39" id="Seg_2719" s="T38">RUS:core</ta>
            <ta e="T48" id="Seg_2720" s="T47">RUS:core</ta>
            <ta e="T68" id="Seg_2721" s="T67">RUS:gram</ta>
            <ta e="T84" id="Seg_2722" s="T83">RUS:gram</ta>
            <ta e="T87" id="Seg_2723" s="T86">RUS:gram</ta>
            <ta e="T112" id="Seg_2724" s="T111">RUS:gram</ta>
            <ta e="T114" id="Seg_2725" s="T113">RUS:gram</ta>
            <ta e="T118" id="Seg_2726" s="T117">RUS:disc</ta>
            <ta e="T119" id="Seg_2727" s="T118">RUS:gram</ta>
            <ta e="T131" id="Seg_2728" s="T130">RUS:gram</ta>
            <ta e="T140" id="Seg_2729" s="T139">RUS:core</ta>
            <ta e="T155" id="Seg_2730" s="T154">RUS:gram</ta>
            <ta e="T182" id="Seg_2731" s="T181">RUS:core</ta>
            <ta e="T200" id="Seg_2732" s="T199">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_2733" s="T1">Three [women] went to pick berries.</ta>
            <ta e="T11" id="Seg_2734" s="T5">They walked, but they didn't find any berries.</ta>
            <ta e="T16" id="Seg_2735" s="T11">One woman returned home.</ta>
            <ta e="T21" id="Seg_2736" s="T16">And the two of us went further.</ta>
            <ta e="T28" id="Seg_2737" s="T21">Having gone further, we found berries, we picked berries, currants.</ta>
            <ta e="T35" id="Seg_2738" s="T28">The three [women] left downhill from the village and got on a boat.</ta>
            <ta e="T40" id="Seg_2739" s="T35">They went by boat to Staroje Sondorovo.</ta>
            <ta e="T42" id="Seg_2740" s="T40">The road (=river) was dry.</ta>
            <ta e="T46" id="Seg_2741" s="T42">They went pushing [the boat] with the oars.</ta>
            <ta e="T50" id="Seg_2742" s="T46">At one moment they started going back.</ta>
            <ta e="T55" id="Seg_2743" s="T50">Having returned back, they landed. </ta>
            <ta e="T57" id="Seg_2744" s="T55">They stepped ashore.</ta>
            <ta e="T61" id="Seg_2745" s="T57">Their legs were sinking in silt, silt.</ta>
            <ta e="T67" id="Seg_2746" s="T61">One woman stepped forward onto the bank.</ta>
            <ta e="T71" id="Seg_2747" s="T67">She threw sticks from there.</ta>
            <ta e="T77" id="Seg_2748" s="T71">We went ashore using this stick (these sticks).</ta>
            <ta e="T83" id="Seg_2749" s="T77">Having stepped ashore we went to pick berries.</ta>
            <ta e="T90" id="Seg_2750" s="T83">We got through the thicket, but didn't find any currants.</ta>
            <ta e="T93" id="Seg_2751" s="T90">We went back onto the road.</ta>
            <ta e="T97" id="Seg_2752" s="T93">One woman returned home.</ta>
            <ta e="T103" id="Seg_2753" s="T97">Vera Demidovna didn't want to pick berries [any more].</ta>
            <ta e="T109" id="Seg_2754" s="T103">“My legs hurt, so that I can't walk or get through the thicket.</ta>
            <ta e="T117" id="Seg_2755" s="T109">I'll go back, otherwise I won't be able to get up tomorrow.”</ta>
            <ta e="T123" id="Seg_2756" s="T117">Well, we said: “Go back.</ta>
            <ta e="T132" id="Seg_2757" s="T123">We'll return by ourselves, [we'll have a look], whether there are berries over there or not.”</ta>
            <ta e="T133" id="Seg_2758" s="T132">We left.</ta>
            <ta e="T138" id="Seg_2759" s="T133">We found berries and picked a bucket of berries each.</ta>
            <ta e="T149" id="Seg_2760" s="T138">We crossed the river on foot, we took off our boots, barefoot.</ta>
            <ta e="T160" id="Seg_2761" s="T149">Having reached the other side, we put on our boots, then we took them off again.</ta>
            <ta e="T168" id="Seg_2762" s="T160">We crossed the river two times, we went ashore through silt.</ta>
            <ta e="T171" id="Seg_2763" s="T168">My boots got stuck in the silt.</ta>
            <ta e="T177" id="Seg_2764" s="T171">I pulled [my boot] with my hands and put it on.</ta>
            <ta e="T186" id="Seg_2765" s="T177">When the other foot got stuck in the silt, I pulled off my boot again with my hands.</ta>
            <ta e="T189" id="Seg_2766" s="T186">And then put it on again.</ta>
            <ta e="T192" id="Seg_2767" s="T189">So we went out onto the hill.</ta>
            <ta e="T196" id="Seg_2768" s="T192">Getting through the grass we came onto the road.</ta>
            <ta e="T199" id="Seg_2769" s="T196">We were so much tired.</ta>
            <ta e="T203" id="Seg_2770" s="T199">We came home as if we were drunk.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_2771" s="T1">Drei [Frauen] gingen, um Beeren zu sammeln.</ta>
            <ta e="T11" id="Seg_2772" s="T5">Sie liefen, aber sie fanden keine Beeren.</ta>
            <ta e="T16" id="Seg_2773" s="T11">Eine Frau kehrte nach Hause zurück.</ta>
            <ta e="T21" id="Seg_2774" s="T16">Und wir beide gingen weiter.</ta>
            <ta e="T28" id="Seg_2775" s="T21">Als wir weitergingen, fanden wir Beeren, wir sammelten Beeren, Johannisbeeren.</ta>
            <ta e="T35" id="Seg_2776" s="T28">Die drei [Frauen] gingen vom Dorf aus bergab und stiegen in ein Boot.</ta>
            <ta e="T40" id="Seg_2777" s="T35">Sie fuhren mit dem Boot nach Staroje Sondorovo.</ta>
            <ta e="T42" id="Seg_2778" s="T40">Die Straße (=Fluss) war trocken.</ta>
            <ta e="T46" id="Seg_2779" s="T42">Sie trieben [das Boot] mit den Rudern an.</ta>
            <ta e="T50" id="Seg_2780" s="T46">An einer Stelle fuhren sie zurück.</ta>
            <ta e="T55" id="Seg_2781" s="T50">Zurückgekehrt legten sie an.</ta>
            <ta e="T57" id="Seg_2782" s="T55">Sie gingen an Land.</ta>
            <ta e="T61" id="Seg_2783" s="T57">Ihre Beine sanken in den Schlamm, Schlamm.</ta>
            <ta e="T67" id="Seg_2784" s="T61">Eine Frau ging voran an Land.</ta>
            <ta e="T71" id="Seg_2785" s="T67">Von dort warf sie Stöcke.</ta>
            <ta e="T77" id="Seg_2786" s="T71">Wir gingen an Land auf diesen Stöckern.</ta>
            <ta e="T83" id="Seg_2787" s="T77">An Land gegangen sammelten wir die Beeren.</ta>
            <ta e="T90" id="Seg_2788" s="T83">Wir liefen durch das Dickicht, aber wir fanden keine Johannisbeeren.</ta>
            <ta e="T93" id="Seg_2789" s="T90">Wir kehrten auf die Straße zurück.</ta>
            <ta e="T97" id="Seg_2790" s="T93">Eine Frau kehrte nach Hause zurück.</ta>
            <ta e="T103" id="Seg_2791" s="T97">Vera Demidovna wollte keine Beeren [mehr] sammeln.</ta>
            <ta e="T109" id="Seg_2792" s="T103">"Meine Beine tun weh, sodass ich nicht gehen oder durchs Dickicht laufen kann.</ta>
            <ta e="T117" id="Seg_2793" s="T109">Ich gehe zurück, sonst kann ich morgen nicht aufstehen."</ta>
            <ta e="T123" id="Seg_2794" s="T117">Nun ja, wir sagten: "Geh zurück.</ta>
            <ta e="T132" id="Seg_2795" s="T123">Wir werden selbst zurückgehen, [wir schauen nach], ob dort drüben Beeren sind oder nicht."</ta>
            <ta e="T133" id="Seg_2796" s="T132">Wir gingen.</ta>
            <ta e="T138" id="Seg_2797" s="T133">Wir fanden Beeren und sammelten jeder einen Eimer voll Beeren.</ta>
            <ta e="T149" id="Seg_2798" s="T138">Wir überquerten den Fluss zu Fuß, wir zogen unsere Schuhe aus, barfuß.</ta>
            <ta e="T160" id="Seg_2799" s="T149">Als wir die andere Seite erreicht hatten, zogen wir unsere Schuhe an, dann zogen wir sie wieder aus.</ta>
            <ta e="T168" id="Seg_2800" s="T160">Wir überquerten den Fluss zweimal, wir liefen ans Ufer durch Schlamm.</ta>
            <ta e="T171" id="Seg_2801" s="T168">Meine Schuhe steckten im Schlamm fest.</ta>
            <ta e="T177" id="Seg_2802" s="T171">Ich zog [meinen Schuh] mit meinen Händen heraus und zog ihn an.</ta>
            <ta e="T186" id="Seg_2803" s="T177">Als der andere Fuß im Schlamm stecken blieb, zog ich meinen Schuh wieder mit den Händen heraus.</ta>
            <ta e="T189" id="Seg_2804" s="T186">Und dann zog ich ihn wieder an.</ta>
            <ta e="T192" id="Seg_2805" s="T189">So liefen wir das Ufer hinauf.</ta>
            <ta e="T196" id="Seg_2806" s="T192">Wir liefen durch das Gras und kamen auf die Straße.</ta>
            <ta e="T199" id="Seg_2807" s="T196">Wir waren so müde.</ta>
            <ta e="T203" id="Seg_2808" s="T199">Wir kamen nach Hause, als wären wir betrunken.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_2809" s="T1">Трое пошли ягоду собирать.</ta>
            <ta e="T11" id="Seg_2810" s="T5">Ходили и ягоду не нашли.</ta>
            <ta e="T16" id="Seg_2811" s="T11">Одна женщина обратно вернулась домой.</ta>
            <ta e="T21" id="Seg_2812" s="T16">А мы двое пошли подальше.</ta>
            <ta e="T28" id="Seg_2813" s="T21">Уйдя подальше, мы нашли ягоды, ягоды набрали, смородины.</ta>
            <ta e="T35" id="Seg_2814" s="T28">Из деревни спустились под гору трое, на маленький обласок сели.</ta>
            <ta e="T40" id="Seg_2815" s="T35">На обласке ехали в Старое Сондрово.</ta>
            <ta e="T42" id="Seg_2816" s="T40">Дорогой сухо было.</ta>
            <ta e="T46" id="Seg_2817" s="T42">Отталкиваясь плыли, веслами отталкиваясь.</ta>
            <ta e="T50" id="Seg_2818" s="T46">В одном месте вернулись обратно.</ta>
            <ta e="T55" id="Seg_2819" s="T50">Назад вернувшись, пристали к берегу.</ta>
            <ta e="T57" id="Seg_2820" s="T55">На берег вышли.</ta>
            <ta e="T61" id="Seg_2821" s="T57">В иле нога тонет, ил.</ta>
            <ta e="T67" id="Seg_2822" s="T61">Одна женщина вперед вышла на берег.</ta>
            <ta e="T71" id="Seg_2823" s="T67">Оттуда палок накидала.</ta>
            <ta e="T77" id="Seg_2824" s="T71">Мы по этой палке (по палкам) вышли на берег.</ta>
            <ta e="T83" id="Seg_2825" s="T77">Выйдя на берег, пошли по смородину, смородину собирать.</ta>
            <ta e="T90" id="Seg_2826" s="T83">И, по чаще лазая, смородины не нашли.</ta>
            <ta e="T93" id="Seg_2827" s="T90">На дорогу обратно вышли.</ta>
            <ta e="T97" id="Seg_2828" s="T93">Одна женщина обратно вернулась.</ta>
            <ta e="T103" id="Seg_2829" s="T97">Вера Демидовна не захотела ягоду собирать.</ta>
            <ta e="T109" id="Seg_2830" s="T103">“У меня ноги болят ходить, лазать по чаще.</ta>
            <ta e="T117" id="Seg_2831" s="T109">Назад пойду, а то завтра и на ноги не поднимешься (не встанешь)”.</ta>
            <ta e="T123" id="Seg_2832" s="T117">Ну а мы сказали: “Иди обратно.</ta>
            <ta e="T132" id="Seg_2833" s="T123">Мы сами обратно пойдем, там ягода есть или нет.”</ta>
            <ta e="T133" id="Seg_2834" s="T132">Пошли.</ta>
            <ta e="T138" id="Seg_2835" s="T133">Ягоды нашли, набрали ягод по ведру.</ta>
            <ta e="T149" id="Seg_2836" s="T138">Через исток перешли пешком, вброд, сняв чарки, босиком.</ta>
            <ta e="T160" id="Seg_2837" s="T149">Переправившись на ту сторону, мы чарки надели, оттуда (туда отсюда?) опять чарки скидывали.</ta>
            <ta e="T168" id="Seg_2838" s="T160">Два раза [речку] вброд переходили, на берег выходили по илу.</ta>
            <ta e="T171" id="Seg_2839" s="T168">Чарки в иле вязли.</ta>
            <ta e="T177" id="Seg_2840" s="T171">Я руками подтащу [чарки] и на ногу надену.</ta>
            <ta e="T186" id="Seg_2841" s="T177">Другая нога завязнет в иле, опять другой чарок подтяну рукой.</ta>
            <ta e="T189" id="Seg_2842" s="T186">Опять на ногу надену.</ta>
            <ta e="T192" id="Seg_2843" s="T189">Так мы на гору вышли.</ta>
            <ta e="T196" id="Seg_2844" s="T192">На дорогу, по траве пробираясь, вышли.</ta>
            <ta e="T199" id="Seg_2845" s="T196">Так устали пристали (устали).</ta>
            <ta e="T203" id="Seg_2846" s="T199">Как пьяные домой пришли.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_2847" s="T1">трое пошли ягоду брать</ta>
            <ta e="T11" id="Seg_2848" s="T5">ходили и ягоду не нашли</ta>
            <ta e="T16" id="Seg_2849" s="T11">одна женщина вернулась домой</ta>
            <ta e="T21" id="Seg_2850" s="T16"> а мы двое пошли подальше</ta>
            <ta e="T28" id="Seg_2851" s="T21">подальше ушли нашли ягоду ягоду набрали смородины</ta>
            <ta e="T35" id="Seg_2852" s="T28">из деревни спустились под гору трое на маленький обласок сели</ta>
            <ta e="T40" id="Seg_2853" s="T35">в обласке ехали в Старое Сондрово</ta>
            <ta e="T42" id="Seg_2854" s="T40">дорогой сухо было</ta>
            <ta e="T46" id="Seg_2855" s="T42">отталкивались (веслами) веслами отталкивались</ta>
            <ta e="T50" id="Seg_2856" s="T46">в одном месте вернулись обратно</ta>
            <ta e="T55" id="Seg_2857" s="T50">вернулись пристали на берег</ta>
            <ta e="T57" id="Seg_2858" s="T55">на берег вышли</ta>
            <ta e="T61" id="Seg_2859" s="T57">в ил нога тонет ил</ta>
            <ta e="T67" id="Seg_2860" s="T61">одна женщина вперед вышла на берег</ta>
            <ta e="T71" id="Seg_2861" s="T67">оттуда палки накидала</ta>
            <ta e="T77" id="Seg_2862" s="T71">мы по этой палке (по палкам) вышли на берег</ta>
            <ta e="T83" id="Seg_2863" s="T77">на гору вышли по смородину пошли смородину брать</ta>
            <ta e="T90" id="Seg_2864" s="T83">по чаще лазали лазали и смородины не нашли</ta>
            <ta e="T93" id="Seg_2865" s="T90">на дорогу обратно вышли</ta>
            <ta e="T97" id="Seg_2866" s="T93">одна женщина обратно вернулась</ta>
            <ta e="T103" id="Seg_2867" s="T97">не захотела ягоду брать</ta>
            <ta e="T109" id="Seg_2868" s="T103">мои ноги болят ходить по чаще</ta>
            <ta e="T117" id="Seg_2869" s="T109">назад пойду а то завтра и на ноги не поднимешься (не встанешь)</ta>
            <ta e="T123" id="Seg_2870" s="T117">а мы сказали иди обратно</ta>
            <ta e="T132" id="Seg_2871" s="T123">там ягода есть или нет</ta>
            <ta e="T133" id="Seg_2872" s="T132">пошли</ta>
            <ta e="T138" id="Seg_2873" s="T133">ягоду нашли ягоду набрали по ведру</ta>
            <ta e="T149" id="Seg_2874" s="T138">через исток перешли (переходили) пешком чарки скинули босиком</ta>
            <ta e="T160" id="Seg_2875" s="T149">на ту сторону перешли чарки одели оттуда опять чарки скидывали</ta>
            <ta e="T168" id="Seg_2876" s="T160">два раза босиком переходили взад-вперед на берег уходили по илу</ta>
            <ta e="T171" id="Seg_2877" s="T168">чарки в илу вязли</ta>
            <ta e="T177" id="Seg_2878" s="T171">я руками поддерну подтащу и на ногу одену</ta>
            <ta e="T186" id="Seg_2879" s="T177">другая нога завязнет в иле опять другую чарок (туфлю) подтяну рукой</ta>
            <ta e="T189" id="Seg_2880" s="T186">опять на ногу одену</ta>
            <ta e="T192" id="Seg_2881" s="T189">так на гору вышли</ta>
            <ta e="T196" id="Seg_2882" s="T192">по траве шли шли вышли</ta>
            <ta e="T199" id="Seg_2883" s="T196">так устали пристали (устали)</ta>
            <ta e="T203" id="Seg_2884" s="T199">как пьяные домой пришли</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T21" id="Seg_2885" s="T16">[KuAI:] Variant: 'sɨdielʼi'. [BrM:] Tentative analysis of 'toːlak'.</ta>
            <ta e="T28" id="Seg_2886" s="T21">[BrM:] 'qwalʼlʼe be' changed to 'qwalʼlʼebe'.</ta>
            <ta e="T42" id="Seg_2887" s="T40">[KuAI:] It means, that the river was dry.</ta>
            <ta e="T46" id="Seg_2888" s="T42">[BrM:] Tentative analysis of 'nogorlʼe', 'nogotčilʼe'.</ta>
            <ta e="T55" id="Seg_2889" s="T50">[KuAI:] Variant: 'uːturɨzottə'.</ta>
            <ta e="T83" id="Seg_2890" s="T77">[BrM:] Tentative analysis of 'wassutə'.</ta>
            <ta e="T117" id="Seg_2891" s="T109">[BrM:] 'a to' changed to 'ato'.</ta>
            <ta e="T149" id="Seg_2892" s="T138">[KuAI:] Variants: 'picʼöuzaj', 'tobɨn'.</ta>
            <ta e="T160" id="Seg_2893" s="T149">[KuAI:] Variant: 'pecʼowlʼe'. [BrM:] Tentative analysis of 'tɨɣɨndoq'.</ta>
            <ta e="T168" id="Seg_2894" s="T160">[BrM:] 'pitʼö qwaj' changed to 'pitʼöqwaj'. [KuAI:] Variant: 'nʼaŋqɨn'.</ta>
            <ta e="T177" id="Seg_2895" s="T171">[BrM:] 'ta qalʼčeqow' changed to 'taqalʼčeqow'. LOC?</ta>
            <ta e="T186" id="Seg_2896" s="T177">[KuAI:] Variant: 'üːgolgow'.</ta>
            <ta e="T189" id="Seg_2897" s="T186">[BrM:] LOC?</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
