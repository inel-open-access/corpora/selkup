<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>DN_196X_Bread_nar</transcription-name>
         <referenced-file url="DN_196X_Bread_nar.wav" />
         <referenced-file url="DN_196X_Bread_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">DN_196X_Bread_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">86</ud-information>
            <ud-information attribute-name="# HIAT:w">54</ud-information>
            <ud-information attribute-name="# e">50</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">3</ud-information>
            <ud-information attribute-name="# HIAT:u">12</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="DN">
            <abbreviation>DN</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.455" type="appl" />
         <tli id="T51" time="2.24" type="intp" />
         <tli id="T1" time="4.025" type="appl" />
         <tli id="T2" time="12.43" type="appl" />
         <tli id="T3" time="12.885" type="appl" />
         <tli id="T4" time="13.34" type="appl" />
         <tli id="T5" time="13.795" type="appl" />
         <tli id="T6" time="14.625" type="appl" />
         <tli id="T7" time="14.985" type="appl" />
         <tli id="T8" time="15.345" type="appl" />
         <tli id="T9" time="15.705" type="appl" />
         <tli id="T10" time="16.065" type="appl" />
         <tli id="T11" time="16.425" type="appl" />
         <tli id="T12" time="17.435" type="appl" />
         <tli id="T13" time="17.945" type="appl" />
         <tli id="T14" time="18.455" type="appl" />
         <tli id="T15" time="18.965" type="appl" />
         <tli id="T16" time="19.475" type="appl" />
         <tli id="T17" time="19.985" type="appl" />
         <tli id="T18" time="21.93" type="appl" />
         <tli id="T19" time="22.25" type="appl" />
         <tli id="T20" time="22.57" type="appl" />
         <tli id="T21" time="22.89" type="appl" />
         <tli id="T22" time="23.21" type="appl" />
         <tli id="T23" time="24.055" type="appl" />
         <tli id="T24" time="25.29" type="appl" />
         <tli id="T25" time="25.825" type="appl" />
         <tli id="T26" time="26.36" type="appl" />
         <tli id="T27" time="27.307" type="appl" />
         <tli id="T28" time="27.704" type="appl" />
         <tli id="T29" time="28.101" type="appl" />
         <tli id="T30" time="28.499" type="appl" />
         <tli id="T31" time="28.896" type="appl" />
         <tli id="T32" time="29.293" type="appl" />
         <tli id="T33" time="29.69" type="appl" />
         <tli id="T34" time="31.104672815218706" />
         <tli id="T35" time="31.715" type="appl" />
         <tli id="T36" time="32.354" type="appl" />
         <tli id="T37" time="32.677" type="appl" />
         <tli id="T38" time="33.0" type="appl" />
         <tli id="T39" time="33.324" type="appl" />
         <tli id="T40" time="33.648" type="appl" />
         <tli id="T41" time="33.971" type="appl" />
         <tli id="T42" time="34.294" type="appl" />
         <tli id="T43" time="34.618" type="appl" />
         <tli id="T44" time="34.942" type="appl" />
         <tli id="T45" time="35.265" type="appl" />
         <tli id="T46" time="37.524" type="appl" />
         <tli id="T47" time="37.862" type="appl" />
         <tli id="T48" time="38.201" type="appl" />
         <tli id="T49" time="38.54" type="appl" />
         <tli id="T50" time="41.377" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="DN"
                      type="t">
         <timeline-fork end="T51" start="T0">
            <tli id="T0.tx.1" />
            <tli id="T0.tx.2" />
            <tli id="T0.tx.3" />
            <tli id="T0.tx.4" />
            <tli id="T0.tx.5" />
            <tli id="T0.tx.6" />
            <tli id="T0.tx.7" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T49" id="Seg_0" n="sc" s="T0">
               <ts e="T51" id="Seg_2" n="HIAT:u" s="T0">
                  <nts id="Seg_3" n="HIAT:ip">(</nts>
                  <nts id="Seg_4" n="HIAT:ip">(</nts>
                  <ats e="T0.tx.1" id="Seg_5" n="HIAT:non-pho" s="T0">KuAI:</ats>
                  <nts id="Seg_6" n="HIAT:ip">)</nts>
                  <nts id="Seg_7" n="HIAT:ip">)</nts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx.2" id="Seg_10" n="HIAT:w" s="T0.tx.1">Дарья</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx.3" id="Seg_13" n="HIAT:w" s="T0.tx.2">Никифоровна</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx.4" id="Seg_16" n="HIAT:w" s="T0.tx.3">расскажет</ts>
                  <nts id="Seg_17" n="HIAT:ip">,</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx.5" id="Seg_20" n="HIAT:w" s="T0.tx.4">как</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx.6" id="Seg_23" n="HIAT:w" s="T0.tx.5">она</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx.7" id="Seg_26" n="HIAT:w" s="T0.tx.6">делает</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_29" n="HIAT:w" s="T0.tx.7">хлеб</ts>
                  <nts id="Seg_30" n="HIAT:ip">.</nts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T5" id="Seg_33" n="HIAT:u" s="T51">
                  <nts id="Seg_34" n="HIAT:ip">(</nts>
                  <nts id="Seg_35" n="HIAT:ip">(</nts>
                  <ats e="T1" id="Seg_36" n="HIAT:non-pho" s="T51">DN:</ats>
                  <nts id="Seg_37" n="HIAT:ip">)</nts>
                  <nts id="Seg_38" n="HIAT:ip">)</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_41" n="HIAT:w" s="T1">Man</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_44" n="HIAT:w" s="T2">nʼaj</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_47" n="HIAT:w" s="T3">uːčʼɨsak</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_50" n="HIAT:w" s="T4">tap</ts>
                  <nts id="Seg_51" n="HIAT:ip">.</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_54" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_56" n="HIAT:w" s="T5">Nʼaj</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_58" n="HIAT:ip">(</nts>
                  <nts id="Seg_59" n="HIAT:ip">(</nts>
                  <ats e="T7" id="Seg_60" n="HIAT:non-pho" s="T6">…</ats>
                  <nts id="Seg_61" n="HIAT:ip">)</nts>
                  <nts id="Seg_62" n="HIAT:ip">)</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_65" n="HIAT:w" s="T7">čʼap</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_68" n="HIAT:w" s="T8">pirɨqolamsak</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_71" n="HIAT:w" s="T9">čʼentaŋ</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_74" n="HIAT:w" s="T10">ɛsa</ts>
                  <nts id="Seg_75" n="HIAT:ip">.</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_78" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_80" n="HIAT:w" s="T11">Šittalä</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_83" n="HIAT:w" s="T12">aj</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_86" n="HIAT:w" s="T13">koːrap</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_89" n="HIAT:w" s="T14">čʼɔːtɨsak</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_92" n="HIAT:w" s="T15">nʼanʼmɨ</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_95" n="HIAT:w" s="T16">meːqo</ts>
                  <nts id="Seg_96" n="HIAT:ip">.</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_99" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_101" n="HIAT:w" s="T17">Šittalä</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_104" n="HIAT:w" s="T18">aj</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_107" n="HIAT:w" s="T19">ukkɨr</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_110" n="HIAT:w" s="T20">nʼaj</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_113" n="HIAT:w" s="T21">taqsak</ts>
                  <nts id="Seg_114" n="HIAT:ip">.</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_117" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_119" n="HIAT:w" s="T22">Meːsak</ts>
                  <nts id="Seg_120" n="HIAT:ip">.</nts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_123" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_125" n="HIAT:w" s="T23">Šʼittalʼ</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_128" n="HIAT:w" s="T24">qaj</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_131" n="HIAT:w" s="T25">nɔːt</ts>
                  <nts id="Seg_132" n="HIAT:ip">.</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_135" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_137" n="HIAT:w" s="T26">Šittalä</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_140" n="HIAT:w" s="T27">tɨm</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_143" n="HIAT:w" s="T28">omtak</ts>
                  <nts id="Seg_144" n="HIAT:ip">,</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_147" n="HIAT:w" s="T29">aa</ts>
                  <nts id="Seg_148" n="HIAT:ip">,</nts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_151" n="HIAT:w" s="T30">aj</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_154" n="HIAT:w" s="T31">čʼenta</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_157" n="HIAT:w" s="T32">ɛsa</ts>
                  <nts id="Seg_158" n="HIAT:ip">.</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_161" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_163" n="HIAT:w" s="T33">Koːramɨntɨ</ts>
                  <nts id="Seg_164" n="HIAT:ip">.</nts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_167" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_169" n="HIAT:w" s="T34">Aa</ts>
                  <nts id="Seg_170" n="HIAT:ip">.</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_173" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_175" n="HIAT:w" s="T35">Settoqaj</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_177" n="HIAT:ip">(</nts>
                  <ts e="T37" id="Seg_179" n="HIAT:w" s="T36">pö-</ts>
                  <nts id="Seg_180" n="HIAT:ip">)</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_183" n="HIAT:w" s="T37">pölla</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_186" n="HIAT:w" s="T38">quptänoːqa</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_189" n="HIAT:w" s="T39">nʼaj</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_192" n="HIAT:w" s="T40">mɨ</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_195" n="HIAT:w" s="T41">qaj</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_198" n="HIAT:w" s="T42">čʼentak</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_201" n="HIAT:w" s="T43">qaj</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_204" n="HIAT:w" s="T44">ɛsa</ts>
                  <nts id="Seg_205" n="HIAT:ip">.</nts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_208" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_210" n="HIAT:w" s="T45">Šittalä</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_213" n="HIAT:w" s="T46">natɨ</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_216" n="HIAT:w" s="T47">qaj</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_219" n="HIAT:w" s="T48">ɛsa</ts>
                  <nts id="Seg_220" n="HIAT:ip">.</nts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T49" id="Seg_222" n="sc" s="T0">
               <ts e="T51" id="Seg_224" n="e" s="T0">((KuAI:)) Дарья Никифоровна расскажет, как она делает хлеб. </ts>
               <ts e="T1" id="Seg_226" n="e" s="T51">((DN:)) </ts>
               <ts e="T2" id="Seg_228" n="e" s="T1">Man </ts>
               <ts e="T3" id="Seg_230" n="e" s="T2">nʼaj </ts>
               <ts e="T4" id="Seg_232" n="e" s="T3">uːčʼɨsak </ts>
               <ts e="T5" id="Seg_234" n="e" s="T4">tap. </ts>
               <ts e="T6" id="Seg_236" n="e" s="T5">Nʼaj </ts>
               <ts e="T7" id="Seg_238" n="e" s="T6">((…)) </ts>
               <ts e="T8" id="Seg_240" n="e" s="T7">čʼap </ts>
               <ts e="T9" id="Seg_242" n="e" s="T8">pirɨqolamsak </ts>
               <ts e="T10" id="Seg_244" n="e" s="T9">čʼentaŋ </ts>
               <ts e="T11" id="Seg_246" n="e" s="T10">ɛsa. </ts>
               <ts e="T12" id="Seg_248" n="e" s="T11">Šittalä </ts>
               <ts e="T13" id="Seg_250" n="e" s="T12">aj </ts>
               <ts e="T14" id="Seg_252" n="e" s="T13">koːrap </ts>
               <ts e="T15" id="Seg_254" n="e" s="T14">čʼɔːtɨsak </ts>
               <ts e="T16" id="Seg_256" n="e" s="T15">nʼanʼmɨ </ts>
               <ts e="T17" id="Seg_258" n="e" s="T16">meːqo. </ts>
               <ts e="T18" id="Seg_260" n="e" s="T17">Šittalä </ts>
               <ts e="T19" id="Seg_262" n="e" s="T18">aj </ts>
               <ts e="T20" id="Seg_264" n="e" s="T19">ukkɨr </ts>
               <ts e="T21" id="Seg_266" n="e" s="T20">nʼaj </ts>
               <ts e="T22" id="Seg_268" n="e" s="T21">taqsak. </ts>
               <ts e="T23" id="Seg_270" n="e" s="T22">Meːsak. </ts>
               <ts e="T24" id="Seg_272" n="e" s="T23">Šʼittalʼ </ts>
               <ts e="T25" id="Seg_274" n="e" s="T24">qaj </ts>
               <ts e="T26" id="Seg_276" n="e" s="T25">nɔːt. </ts>
               <ts e="T27" id="Seg_278" n="e" s="T26">Šittalä </ts>
               <ts e="T28" id="Seg_280" n="e" s="T27">tɨm </ts>
               <ts e="T29" id="Seg_282" n="e" s="T28">omtak, </ts>
               <ts e="T30" id="Seg_284" n="e" s="T29">aa, </ts>
               <ts e="T31" id="Seg_286" n="e" s="T30">aj </ts>
               <ts e="T32" id="Seg_288" n="e" s="T31">čʼenta </ts>
               <ts e="T33" id="Seg_290" n="e" s="T32">ɛsa. </ts>
               <ts e="T34" id="Seg_292" n="e" s="T33">Koːramɨntɨ. </ts>
               <ts e="T35" id="Seg_294" n="e" s="T34">Aa. </ts>
               <ts e="T36" id="Seg_296" n="e" s="T35">Settoqaj </ts>
               <ts e="T37" id="Seg_298" n="e" s="T36">(pö-) </ts>
               <ts e="T38" id="Seg_300" n="e" s="T37">pölla </ts>
               <ts e="T39" id="Seg_302" n="e" s="T38">quptänoːqa </ts>
               <ts e="T40" id="Seg_304" n="e" s="T39">nʼaj </ts>
               <ts e="T41" id="Seg_306" n="e" s="T40">mɨ </ts>
               <ts e="T42" id="Seg_308" n="e" s="T41">qaj </ts>
               <ts e="T43" id="Seg_310" n="e" s="T42">čʼentak </ts>
               <ts e="T44" id="Seg_312" n="e" s="T43">qaj </ts>
               <ts e="T45" id="Seg_314" n="e" s="T44">ɛsa. </ts>
               <ts e="T46" id="Seg_316" n="e" s="T45">Šittalä </ts>
               <ts e="T47" id="Seg_318" n="e" s="T46">natɨ </ts>
               <ts e="T48" id="Seg_320" n="e" s="T47">qaj </ts>
               <ts e="T49" id="Seg_322" n="e" s="T48">ɛsa. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T51" id="Seg_323" s="T0">DN_196X_Bread_nar.001 (001)</ta>
            <ta e="T5" id="Seg_324" s="T51">DN_196X_Bread_nar.002 (002)</ta>
            <ta e="T11" id="Seg_325" s="T5">DN_196X_Bread_nar.003 (003)</ta>
            <ta e="T17" id="Seg_326" s="T11">DN_196X_Bread_nar.004 (004)</ta>
            <ta e="T22" id="Seg_327" s="T17">DN_196X_Bread_nar.005 (005)</ta>
            <ta e="T23" id="Seg_328" s="T22">DN_196X_Bread_nar.006 (006)</ta>
            <ta e="T26" id="Seg_329" s="T23">DN_196X_Bread_nar.007 (007)</ta>
            <ta e="T33" id="Seg_330" s="T26">DN_196X_Bread_nar.008 (008)</ta>
            <ta e="T34" id="Seg_331" s="T33">DN_196X_Bread_nar.009 (009)</ta>
            <ta e="T35" id="Seg_332" s="T34">DN_196X_Bread_nar.010 (010)</ta>
            <ta e="T45" id="Seg_333" s="T35">DN_196X_Bread_nar.011 (011)</ta>
            <ta e="T49" id="Seg_334" s="T45">DN_196X_Bread_nar.012 (012)</ta>
         </annotation>
         <annotation name="st" tierref="st" />
         <annotation name="stl" tierref="stl">
            <ta e="T51" id="Seg_335" s="T0">((KuAI:)) Дарья Никифоровна расскажет, как она делает хлеб.</ta>
            <ta e="T5" id="Seg_336" s="T51">((DN:)) man nʼaj učʼisaɣ tap</ta>
            <ta e="T11" id="Seg_337" s="T5">nʼaj čʼap pirɨqolawsaɣ čʼentaɣ ɛːsa</ta>
            <ta e="T17" id="Seg_338" s="T11">šʼittalä aj qorap čʼotɨsaɣ nʼanʼmɨ meːɣa.</ta>
            <ta e="T22" id="Seg_339" s="T17">šʼittalä aj ukkɨr nʼaj taqsaɣ.</ta>
            <ta e="T23" id="Seg_340" s="T22">meːsaɣ.</ta>
            <ta e="T26" id="Seg_341" s="T23">šʼittalʼ qaj nɔːt. </ta>
            <ta e="T33" id="Seg_342" s="T26">šʼittalä tɨm omta, aa, aj čʼenta ɛːsa.</ta>
            <ta e="T34" id="Seg_343" s="T33">ɣoramɨntɨ.</ta>
            <ta e="T35" id="Seg_344" s="T34">aa</ta>
            <ta e="T45" id="Seg_345" s="T35">settoqaj pöla ɣoptanoːqa nʼaj mɨqaj čʼentak qaj ɛːsa.</ta>
            <ta e="T49" id="Seg_346" s="T45">šʼittalʼ natɨ ɣaj ɛːs(a).</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T51" id="Seg_347" s="T0">((KuAI:)) Дарья Никифоровна расскажет, как она делает хлеб.</ta>
            <ta e="T5" id="Seg_348" s="T51">((DN:)) Man nʼaj uːčʼɨsak tap. </ta>
            <ta e="T11" id="Seg_349" s="T5">Nʼaj ((…)) čʼap pirɨqolamsak čʼentaŋ ɛsa. </ta>
            <ta e="T17" id="Seg_350" s="T11">Šittalä aj koːrap čʼɔːtɨsak nʼanʼmɨ meːqo. </ta>
            <ta e="T22" id="Seg_351" s="T17">Šittalä aj ukkɨr nʼaj taqsak. </ta>
            <ta e="T23" id="Seg_352" s="T22">Meːsak. </ta>
            <ta e="T26" id="Seg_353" s="T23">Šʼittalʼ qaj nɔːt. </ta>
            <ta e="T33" id="Seg_354" s="T26">Šittalä tɨm omtak, aa, aj čʼenta ɛsa. </ta>
            <ta e="T34" id="Seg_355" s="T33">Koːramɨntɨ. </ta>
            <ta e="T35" id="Seg_356" s="T34">Aa. </ta>
            <ta e="T45" id="Seg_357" s="T35">Settoqaj (pö-) pölla quptänoːqa nʼaj mɨ qaj čʼentak qaj ɛsa. </ta>
            <ta e="T49" id="Seg_358" s="T45">Šittalä natɨ qaj ɛːsa. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_359" s="T1">man</ta>
            <ta e="T3" id="Seg_360" s="T2">nʼaj</ta>
            <ta e="T4" id="Seg_361" s="T3">uːčʼɨ-sa-k</ta>
            <ta e="T5" id="Seg_362" s="T4">tap</ta>
            <ta e="T6" id="Seg_363" s="T5">nʼaj</ta>
            <ta e="T8" id="Seg_364" s="T7">čʼap</ta>
            <ta e="T9" id="Seg_365" s="T8">pi-rɨ-q-olam-sa-k</ta>
            <ta e="T10" id="Seg_366" s="T9">čʼenta-ŋ</ta>
            <ta e="T11" id="Seg_367" s="T10">ɛsa</ta>
            <ta e="T12" id="Seg_368" s="T11">šittalä</ta>
            <ta e="T13" id="Seg_369" s="T12">aj</ta>
            <ta e="T14" id="Seg_370" s="T13">koːra-p</ta>
            <ta e="T15" id="Seg_371" s="T14">čʼɔːtɨ-sa-k</ta>
            <ta e="T16" id="Seg_372" s="T15">nʼanʼ-mɨ</ta>
            <ta e="T17" id="Seg_373" s="T16">meː-qo</ta>
            <ta e="T18" id="Seg_374" s="T17">šittalä</ta>
            <ta e="T19" id="Seg_375" s="T18">aj</ta>
            <ta e="T20" id="Seg_376" s="T19">ukkɨr</ta>
            <ta e="T21" id="Seg_377" s="T20">nʼaj</ta>
            <ta e="T22" id="Seg_378" s="T21">taq-sa-k</ta>
            <ta e="T23" id="Seg_379" s="T22">meː-sa-k</ta>
            <ta e="T24" id="Seg_380" s="T23">šʼittalʼ</ta>
            <ta e="T25" id="Seg_381" s="T24">qaj</ta>
            <ta e="T26" id="Seg_382" s="T25">nɔːt</ta>
            <ta e="T27" id="Seg_383" s="T26">šittalä</ta>
            <ta e="T28" id="Seg_384" s="T27">tɨm</ta>
            <ta e="T29" id="Seg_385" s="T28">omta-k</ta>
            <ta e="T30" id="Seg_386" s="T29">aa</ta>
            <ta e="T31" id="Seg_387" s="T30">aj</ta>
            <ta e="T32" id="Seg_388" s="T31">čʼenta</ta>
            <ta e="T33" id="Seg_389" s="T32">ɛsa</ta>
            <ta e="T34" id="Seg_390" s="T33">koːra-mɨn-tɨ</ta>
            <ta e="T35" id="Seg_391" s="T34">aa</ta>
            <ta e="T36" id="Seg_392" s="T35">settoqaj</ta>
            <ta e="T38" id="Seg_393" s="T37">pöl-la</ta>
            <ta e="T39" id="Seg_394" s="T38">qu-ptä-noːqa</ta>
            <ta e="T40" id="Seg_395" s="T39">nʼaj</ta>
            <ta e="T41" id="Seg_396" s="T40">mɨ</ta>
            <ta e="T42" id="Seg_397" s="T41">qaj</ta>
            <ta e="T43" id="Seg_398" s="T42">čʼenta-k</ta>
            <ta e="T44" id="Seg_399" s="T43">qaj</ta>
            <ta e="T45" id="Seg_400" s="T44">ɛsa</ta>
            <ta e="T46" id="Seg_401" s="T45">šittalä</ta>
            <ta e="T47" id="Seg_402" s="T46">natɨ</ta>
            <ta e="T48" id="Seg_403" s="T47">qaj</ta>
            <ta e="T49" id="Seg_404" s="T48">ɛsa</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_405" s="T1">man</ta>
            <ta e="T3" id="Seg_406" s="T2">nʼanʼ</ta>
            <ta e="T4" id="Seg_407" s="T3">uːčʼɨ-sɨ-k</ta>
            <ta e="T5" id="Seg_408" s="T4">tam</ta>
            <ta e="T6" id="Seg_409" s="T5">nʼanʼ</ta>
            <ta e="T8" id="Seg_410" s="T7">čʼam</ta>
            <ta e="T9" id="Seg_411" s="T8">pi-rɨ-qo-olam-sɨ-k</ta>
            <ta e="T10" id="Seg_412" s="T9">čʼenta-k</ta>
            <ta e="T11" id="Seg_413" s="T10">ɛsɨ</ta>
            <ta e="T12" id="Seg_414" s="T11">šittälʼ</ta>
            <ta e="T13" id="Seg_415" s="T12">aj</ta>
            <ta e="T14" id="Seg_416" s="T13">koːra-m</ta>
            <ta e="T15" id="Seg_417" s="T14">čʼɔːtɨ-sɨ-k</ta>
            <ta e="T16" id="Seg_418" s="T15">nʼanʼ-mɨ</ta>
            <ta e="T17" id="Seg_419" s="T16">meː-qo</ta>
            <ta e="T18" id="Seg_420" s="T17">šittälʼ</ta>
            <ta e="T19" id="Seg_421" s="T18">aj</ta>
            <ta e="T20" id="Seg_422" s="T19">ukkɨr</ta>
            <ta e="T21" id="Seg_423" s="T20">nʼanʼ</ta>
            <ta e="T22" id="Seg_424" s="T21">taqɨ-sɨ-k</ta>
            <ta e="T23" id="Seg_425" s="T22">meː-sɨ-k</ta>
            <ta e="T24" id="Seg_426" s="T23">šittälʼ</ta>
            <ta e="T25" id="Seg_427" s="T24">qaj</ta>
            <ta e="T26" id="Seg_428" s="T25">nɔːtɨ</ta>
            <ta e="T27" id="Seg_429" s="T26">šittälʼ</ta>
            <ta e="T28" id="Seg_430" s="T27">tɨm</ta>
            <ta e="T29" id="Seg_431" s="T28">omtɨ-k</ta>
            <ta e="T30" id="Seg_432" s="T29">aga</ta>
            <ta e="T31" id="Seg_433" s="T30">aj</ta>
            <ta e="T32" id="Seg_434" s="T31">čʼenta</ta>
            <ta e="T33" id="Seg_435" s="T32">ɛsɨ</ta>
            <ta e="T34" id="Seg_436" s="T33">koːra-mɨn-ntɨ</ta>
            <ta e="T35" id="Seg_437" s="T34">aga</ta>
            <ta e="T36" id="Seg_438" s="T35">satqaj</ta>
            <ta e="T38" id="Seg_439" s="T37">pöt-lä</ta>
            <ta e="T39" id="Seg_440" s="T38">qu-ptäː-noːqo</ta>
            <ta e="T40" id="Seg_441" s="T39">nʼanʼ</ta>
            <ta e="T41" id="Seg_442" s="T40">mɨ</ta>
            <ta e="T42" id="Seg_443" s="T41">qaj</ta>
            <ta e="T43" id="Seg_444" s="T42">čʼenta-k</ta>
            <ta e="T44" id="Seg_445" s="T43">qaj</ta>
            <ta e="T45" id="Seg_446" s="T44">ɛsɨ</ta>
            <ta e="T46" id="Seg_447" s="T45">šittälʼ</ta>
            <ta e="T47" id="Seg_448" s="T46">naːtɨ</ta>
            <ta e="T48" id="Seg_449" s="T47">qaj</ta>
            <ta e="T49" id="Seg_450" s="T48">ɛsɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_451" s="T1">I.NOM</ta>
            <ta e="T3" id="Seg_452" s="T2">bread.[NOM]</ta>
            <ta e="T4" id="Seg_453" s="T3">make-PST-1SG.S</ta>
            <ta e="T5" id="Seg_454" s="T4">this.[NOM]</ta>
            <ta e="T6" id="Seg_455" s="T5">bread.[NOM]</ta>
            <ta e="T8" id="Seg_456" s="T7">hardly</ta>
            <ta e="T9" id="Seg_457" s="T8">become.ready-TR-INF-be.going.to-PST-1SG.S</ta>
            <ta e="T10" id="Seg_458" s="T9">wet-ADVZ</ta>
            <ta e="T11" id="Seg_459" s="T10">become.[3SG.S]</ta>
            <ta e="T12" id="Seg_460" s="T11">then</ta>
            <ta e="T13" id="Seg_461" s="T12">again</ta>
            <ta e="T14" id="Seg_462" s="T13">sand-ACC</ta>
            <ta e="T15" id="Seg_463" s="T14">set.fire-PST-1SG.S</ta>
            <ta e="T16" id="Seg_464" s="T15">bread.[NOM]-1SG</ta>
            <ta e="T17" id="Seg_465" s="T16">make-INF</ta>
            <ta e="T18" id="Seg_466" s="T17">then</ta>
            <ta e="T19" id="Seg_467" s="T18">also</ta>
            <ta e="T20" id="Seg_468" s="T19">one</ta>
            <ta e="T21" id="Seg_469" s="T20">bread.[NOM]</ta>
            <ta e="T22" id="Seg_470" s="T21">close-PST-1SG.S</ta>
            <ta e="T23" id="Seg_471" s="T22">make-PST-1SG.S</ta>
            <ta e="T24" id="Seg_472" s="T23">then</ta>
            <ta e="T25" id="Seg_473" s="T24">what.[NOM]</ta>
            <ta e="T26" id="Seg_474" s="T25">more</ta>
            <ta e="T27" id="Seg_475" s="T26">then</ta>
            <ta e="T28" id="Seg_476" s="T27">%%</ta>
            <ta e="T29" id="Seg_477" s="T28">sit.down-1SG.S</ta>
            <ta e="T30" id="Seg_478" s="T29">aha</ta>
            <ta e="T31" id="Seg_479" s="T30">again</ta>
            <ta e="T32" id="Seg_480" s="T31">wet</ta>
            <ta e="T33" id="Seg_481" s="T32">become.[3SG.S]</ta>
            <ta e="T34" id="Seg_482" s="T33">sand-PROL-OBL.3SG</ta>
            <ta e="T35" id="Seg_483" s="T34">aha</ta>
            <ta e="T36" id="Seg_484" s="T35">through</ta>
            <ta e="T38" id="Seg_485" s="T37">warm-CVB</ta>
            <ta e="T39" id="Seg_486" s="T38">die-ACTN-TRL.1SG</ta>
            <ta e="T40" id="Seg_487" s="T39">bread.[NOM]</ta>
            <ta e="T41" id="Seg_488" s="T40">something.[NOM]</ta>
            <ta e="T42" id="Seg_489" s="T41">whether</ta>
            <ta e="T43" id="Seg_490" s="T42">wet-ADVZ</ta>
            <ta e="T44" id="Seg_491" s="T43">whether</ta>
            <ta e="T45" id="Seg_492" s="T44">become.[3SG.S]</ta>
            <ta e="T46" id="Seg_493" s="T45">then</ta>
            <ta e="T47" id="Seg_494" s="T46">%%</ta>
            <ta e="T48" id="Seg_495" s="T47">what.[NOM]</ta>
            <ta e="T49" id="Seg_496" s="T48">become.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_497" s="T1">я.NOM</ta>
            <ta e="T3" id="Seg_498" s="T2">хлеб.[NOM]</ta>
            <ta e="T4" id="Seg_499" s="T3">делать-PST-1SG.S</ta>
            <ta e="T5" id="Seg_500" s="T4">этот.[NOM]</ta>
            <ta e="T6" id="Seg_501" s="T5">хлеб.[NOM]</ta>
            <ta e="T8" id="Seg_502" s="T7">едва</ta>
            <ta e="T9" id="Seg_503" s="T8">стать.готовым-TR-INF-собраться-PST-1SG.S</ta>
            <ta e="T10" id="Seg_504" s="T9">сырой-ADVZ</ta>
            <ta e="T11" id="Seg_505" s="T10">стать.[3SG.S]</ta>
            <ta e="T12" id="Seg_506" s="T11">потом</ta>
            <ta e="T13" id="Seg_507" s="T12">опять</ta>
            <ta e="T14" id="Seg_508" s="T13">песок-ACC</ta>
            <ta e="T15" id="Seg_509" s="T14">зажечь-PST-1SG.S</ta>
            <ta e="T16" id="Seg_510" s="T15">хлеб.[NOM]-1SG</ta>
            <ta e="T17" id="Seg_511" s="T16">сделать-INF</ta>
            <ta e="T18" id="Seg_512" s="T17">потом</ta>
            <ta e="T19" id="Seg_513" s="T18">тоже</ta>
            <ta e="T20" id="Seg_514" s="T19">один</ta>
            <ta e="T21" id="Seg_515" s="T20">хлеб.[NOM]</ta>
            <ta e="T22" id="Seg_516" s="T21">закрыть-PST-1SG.S</ta>
            <ta e="T23" id="Seg_517" s="T22">сделать-PST-1SG.S</ta>
            <ta e="T24" id="Seg_518" s="T23">потом</ta>
            <ta e="T25" id="Seg_519" s="T24">что.[NOM]</ta>
            <ta e="T26" id="Seg_520" s="T25">еще</ta>
            <ta e="T27" id="Seg_521" s="T26">потом</ta>
            <ta e="T28" id="Seg_522" s="T27">%%</ta>
            <ta e="T29" id="Seg_523" s="T28">сесть-1SG.S</ta>
            <ta e="T30" id="Seg_524" s="T29">ага</ta>
            <ta e="T31" id="Seg_525" s="T30">опять</ta>
            <ta e="T32" id="Seg_526" s="T31">сырой</ta>
            <ta e="T33" id="Seg_527" s="T32">стать.[3SG.S]</ta>
            <ta e="T34" id="Seg_528" s="T33">песок-PROL-OBL.3SG</ta>
            <ta e="T35" id="Seg_529" s="T34">ага</ta>
            <ta e="T36" id="Seg_530" s="T35">насквозь</ta>
            <ta e="T38" id="Seg_531" s="T37">нагреться-CVB</ta>
            <ta e="T39" id="Seg_532" s="T38">умереть-ACTN-TRL.1SG</ta>
            <ta e="T40" id="Seg_533" s="T39">хлеб.[NOM]</ta>
            <ta e="T41" id="Seg_534" s="T40">нечто.[NOM]</ta>
            <ta e="T42" id="Seg_535" s="T41">что.ли</ta>
            <ta e="T43" id="Seg_536" s="T42">сырой-ADVZ</ta>
            <ta e="T44" id="Seg_537" s="T43">что.ли</ta>
            <ta e="T45" id="Seg_538" s="T44">стать.[3SG.S]</ta>
            <ta e="T46" id="Seg_539" s="T45">потом</ta>
            <ta e="T47" id="Seg_540" s="T46">%%</ta>
            <ta e="T48" id="Seg_541" s="T47">что.[NOM]</ta>
            <ta e="T49" id="Seg_542" s="T48">стать.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_543" s="T1">pers</ta>
            <ta e="T3" id="Seg_544" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_545" s="T3">v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_546" s="T4">dem-n:case</ta>
            <ta e="T6" id="Seg_547" s="T5">n-n:case</ta>
            <ta e="T8" id="Seg_548" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_549" s="T8">v-v&gt;v-v:inf-v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_550" s="T9">adj-adj&gt;adv</ta>
            <ta e="T11" id="Seg_551" s="T10">v-v:pn</ta>
            <ta e="T12" id="Seg_552" s="T11">adv</ta>
            <ta e="T13" id="Seg_553" s="T12">adv</ta>
            <ta e="T14" id="Seg_554" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_555" s="T14">v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_556" s="T15">n-n:case-n:poss</ta>
            <ta e="T17" id="Seg_557" s="T16">v-v:inf</ta>
            <ta e="T18" id="Seg_558" s="T17">adv</ta>
            <ta e="T19" id="Seg_559" s="T18">ptcl</ta>
            <ta e="T20" id="Seg_560" s="T19">num</ta>
            <ta e="T21" id="Seg_561" s="T20">n-n:case</ta>
            <ta e="T22" id="Seg_562" s="T21">v-v:tense-v:pn</ta>
            <ta e="T23" id="Seg_563" s="T22">v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_564" s="T23">adv</ta>
            <ta e="T25" id="Seg_565" s="T24">interrog-n:case</ta>
            <ta e="T26" id="Seg_566" s="T25">adv</ta>
            <ta e="T27" id="Seg_567" s="T26">adv</ta>
            <ta e="T28" id="Seg_568" s="T27">%%</ta>
            <ta e="T29" id="Seg_569" s="T28">v-v:pn</ta>
            <ta e="T30" id="Seg_570" s="T29">interj</ta>
            <ta e="T31" id="Seg_571" s="T30">adv</ta>
            <ta e="T32" id="Seg_572" s="T31">adj</ta>
            <ta e="T33" id="Seg_573" s="T32">v-v:pn</ta>
            <ta e="T34" id="Seg_574" s="T33">n-n:case-n:obl.poss</ta>
            <ta e="T35" id="Seg_575" s="T34">interj</ta>
            <ta e="T36" id="Seg_576" s="T35">adv</ta>
            <ta e="T38" id="Seg_577" s="T37">v-v&gt;adv</ta>
            <ta e="T39" id="Seg_578" s="T38">v-v&gt;n-n:case.poss</ta>
            <ta e="T40" id="Seg_579" s="T39">n-n:case</ta>
            <ta e="T41" id="Seg_580" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_581" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_582" s="T42">adj-adj&gt;adv</ta>
            <ta e="T44" id="Seg_583" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_584" s="T44">v-v:pn</ta>
            <ta e="T46" id="Seg_585" s="T45">adv</ta>
            <ta e="T47" id="Seg_586" s="T46">%%</ta>
            <ta e="T48" id="Seg_587" s="T47">interrog-n:case</ta>
            <ta e="T49" id="Seg_588" s="T48">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_589" s="T1">pers</ta>
            <ta e="T3" id="Seg_590" s="T2">n</ta>
            <ta e="T4" id="Seg_591" s="T3">v</ta>
            <ta e="T5" id="Seg_592" s="T4">dem</ta>
            <ta e="T6" id="Seg_593" s="T5">n</ta>
            <ta e="T8" id="Seg_594" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_595" s="T8">v</ta>
            <ta e="T10" id="Seg_596" s="T9">adv</ta>
            <ta e="T11" id="Seg_597" s="T10">v</ta>
            <ta e="T12" id="Seg_598" s="T11">adv</ta>
            <ta e="T13" id="Seg_599" s="T12">adv</ta>
            <ta e="T14" id="Seg_600" s="T13">n</ta>
            <ta e="T15" id="Seg_601" s="T14">v</ta>
            <ta e="T16" id="Seg_602" s="T15">n</ta>
            <ta e="T17" id="Seg_603" s="T16">v</ta>
            <ta e="T18" id="Seg_604" s="T17">adv</ta>
            <ta e="T19" id="Seg_605" s="T18">ptcl</ta>
            <ta e="T20" id="Seg_606" s="T19">num</ta>
            <ta e="T21" id="Seg_607" s="T20">n</ta>
            <ta e="T22" id="Seg_608" s="T21">v</ta>
            <ta e="T23" id="Seg_609" s="T22">v</ta>
            <ta e="T24" id="Seg_610" s="T23">adv</ta>
            <ta e="T25" id="Seg_611" s="T24">interrog</ta>
            <ta e="T26" id="Seg_612" s="T25">adv</ta>
            <ta e="T27" id="Seg_613" s="T26">adv</ta>
            <ta e="T29" id="Seg_614" s="T28">v</ta>
            <ta e="T30" id="Seg_615" s="T29">interj</ta>
            <ta e="T31" id="Seg_616" s="T30">adv</ta>
            <ta e="T32" id="Seg_617" s="T31">adj</ta>
            <ta e="T33" id="Seg_618" s="T32">v</ta>
            <ta e="T34" id="Seg_619" s="T33">n</ta>
            <ta e="T35" id="Seg_620" s="T34">interj</ta>
            <ta e="T36" id="Seg_621" s="T35">adv</ta>
            <ta e="T38" id="Seg_622" s="T37">v</ta>
            <ta e="T39" id="Seg_623" s="T38">n</ta>
            <ta e="T40" id="Seg_624" s="T39">n</ta>
            <ta e="T41" id="Seg_625" s="T40">n</ta>
            <ta e="T42" id="Seg_626" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_627" s="T42">adv</ta>
            <ta e="T44" id="Seg_628" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_629" s="T44">v</ta>
            <ta e="T46" id="Seg_630" s="T45">adv</ta>
            <ta e="T47" id="Seg_631" s="T46">v</ta>
            <ta e="T48" id="Seg_632" s="T47">interrog</ta>
            <ta e="T49" id="Seg_633" s="T48">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_634" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_635" s="T2">np:O</ta>
            <ta e="T4" id="Seg_636" s="T3">v:pred</ta>
            <ta e="T6" id="Seg_637" s="T5">np:O</ta>
            <ta e="T9" id="Seg_638" s="T8">0.3.h:S v:pred</ta>
            <ta e="T10" id="Seg_639" s="T9">adj:pred</ta>
            <ta e="T11" id="Seg_640" s="T10">0.3:S cop</ta>
            <ta e="T14" id="Seg_641" s="T13">np:O</ta>
            <ta e="T15" id="Seg_642" s="T14">0.1.h:S v:pred</ta>
            <ta e="T17" id="Seg_643" s="T15">s:purp</ta>
            <ta e="T21" id="Seg_644" s="T20">np:O</ta>
            <ta e="T22" id="Seg_645" s="T21">0.1.h:S v:pred</ta>
            <ta e="T23" id="Seg_646" s="T22">0.1.h:S v:pred</ta>
            <ta e="T29" id="Seg_647" s="T28">0.1.h:S v:pred</ta>
            <ta e="T32" id="Seg_648" s="T31">adj:pred</ta>
            <ta e="T33" id="Seg_649" s="T32">0.3:S cop</ta>
            <ta e="T39" id="Seg_650" s="T37">s:temp</ta>
            <ta e="T40" id="Seg_651" s="T39">np:S</ta>
            <ta e="T43" id="Seg_652" s="T42">adj:pred</ta>
            <ta e="T45" id="Seg_653" s="T44">cop</ta>
            <ta e="T48" id="Seg_654" s="T47">pro:S</ta>
            <ta e="T49" id="Seg_655" s="T48">v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_656" s="T1">np.h:A</ta>
            <ta e="T3" id="Seg_657" s="T2">np:P</ta>
            <ta e="T6" id="Seg_658" s="T5">np:P</ta>
            <ta e="T9" id="Seg_659" s="T8">0.3.h:A</ta>
            <ta e="T11" id="Seg_660" s="T10">0.3:Th</ta>
            <ta e="T14" id="Seg_661" s="T13">np:P</ta>
            <ta e="T15" id="Seg_662" s="T14">0.1.h:A</ta>
            <ta e="T21" id="Seg_663" s="T20">np:P</ta>
            <ta e="T22" id="Seg_664" s="T21">0.1.h:A</ta>
            <ta e="T23" id="Seg_665" s="T22">0.1.h:A</ta>
            <ta e="T29" id="Seg_666" s="T28">0.1.h:Th</ta>
            <ta e="T33" id="Seg_667" s="T32">0.3:Th</ta>
            <ta e="T39" id="Seg_668" s="T38">0.1.h:P</ta>
            <ta e="T40" id="Seg_669" s="T39">np:Th</ta>
            <ta e="T48" id="Seg_670" s="T47">pro:Th</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T51" id="Seg_671" s="T0">[KuAI:] Дарья Никифоровна расскажет, как она делает хлеб.</ta>
            <ta e="T5" id="Seg_672" s="T51">[DN:] Я хлеб стряпала, этот.</ta>
            <ta e="T11" id="Seg_673" s="T5">Я хлеб стала печь, сыровато получилось.</ta>
            <ta e="T17" id="Seg_674" s="T11">Потом снова песок зажгла, чтобы хлеб делать.</ta>
            <ta e="T22" id="Seg_675" s="T17">Потом еще один хлеб закопала.</ta>
            <ta e="T23" id="Seg_676" s="T22">Сделала.</ta>
            <ta e="T26" id="Seg_677" s="T23">Потом что еще.</ta>
            <ta e="T33" id="Seg_678" s="T26">И вот сижу, ага, опять сырой получился.</ta>
            <ta e="T34" id="Seg_679" s="T33">В песке.</ta>
            <ta e="T35" id="Seg_680" s="T34">А-а.</ta>
            <ta e="T45" id="Seg_681" s="T35">Вспотев (целиком нагревшись) до смерти моей, хлеб сырой что ли получился.</ta>
            <ta e="T49" id="Seg_682" s="T45">Потом что стало.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T51" id="Seg_683" s="T0">[KuAI:] Darya Nikiforovna will tell, how she makes bread.</ta>
            <ta e="T5" id="Seg_684" s="T51">[DN:] I baked bread, this one.</ta>
            <ta e="T11" id="Seg_685" s="T5">I started baking bread, it came out raw.</ta>
            <ta e="T17" id="Seg_686" s="T11">Then I heated again the sand up to make bread.</ta>
            <ta e="T22" id="Seg_687" s="T17">Then I put one more [loaf of] bread [into the sand].</ta>
            <ta e="T23" id="Seg_688" s="T22">I made it.</ta>
            <ta e="T26" id="Seg_689" s="T23">Then what else.</ta>
            <ta e="T33" id="Seg_690" s="T26">And so I am sitting, aha, it came out raw again.</ta>
            <ta e="T34" id="Seg_691" s="T33">In the sand.</ta>
            <ta e="T35" id="Seg_692" s="T34">Aha.</ta>
            <ta e="T45" id="Seg_693" s="T35">Having sweated to death, [I…]; the bread seemed to come out raw.</ta>
            <ta e="T49" id="Seg_694" s="T45">Then what happened.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T51" id="Seg_695" s="T0">[KuAI:] Darja Nikiforovna erzählt, wie sie Brot macht.</ta>
            <ta e="T5" id="Seg_696" s="T51">[DN:] Ich habe Brot gebacken, dieses.</ta>
            <ta e="T11" id="Seg_697" s="T5">Ich habe angefangen Brot zu backen, es wurde feucht.</ta>
            <ta e="T17" id="Seg_698" s="T11">Dann heizte ich den Sand nochmal auf, um Brot zu machen.</ta>
            <ta e="T22" id="Seg_699" s="T17">Dann vergrub ich noch ein Brot.</ta>
            <ta e="T23" id="Seg_700" s="T22">Ich machte es.</ta>
            <ta e="T26" id="Seg_701" s="T23">Dann was noch.</ta>
            <ta e="T33" id="Seg_702" s="T26">Und ich sitze so, aha, es wurde wieder feucht.</ta>
            <ta e="T34" id="Seg_703" s="T33">Im Sand.</ta>
            <ta e="T35" id="Seg_704" s="T34">Aha.</ta>
            <ta e="T45" id="Seg_705" s="T35">Zu Tode geschwitzt, [ich…]; das Brot schien feucht zu werden.</ta>
            <ta e="T49" id="Seg_706" s="T45">Dann passierte was.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T51" id="Seg_707" s="T0">Дарья Никифоровна расскажет, как она делает хлеб.</ta>
            <ta e="T5" id="Seg_708" s="T51">[DN:] я хлеб стряпала, вот.</ta>
            <ta e="T11" id="Seg_709" s="T5">хлеб пекла сыровато получилось</ta>
            <ta e="T17" id="Seg_710" s="T11">далее снова песок зажгла хлеб делать</ta>
            <ta e="T22" id="Seg_711" s="T17">потом еще один хлеб закопала</ta>
            <ta e="T23" id="Seg_712" s="T22">сделала</ta>
            <ta e="T26" id="Seg_713" s="T23">что не получилось</ta>
            <ta e="T33" id="Seg_714" s="T26">и вот сижу, опять сырой получился</ta>
            <ta e="T34" id="Seg_715" s="T33">песок.</ta>
            <ta e="T35" id="Seg_716" s="T34">аа</ta>
            <ta e="T45" id="Seg_717" s="T35">вот наверное вспотела аж до смерти хлеб сырой что-то получился</ta>
            <ta e="T49" id="Seg_718" s="T45">ну что теперь получилось</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T11" id="Seg_719" s="T5">[BrM:] Tentative transcription.</ta>
            <ta e="T33" id="Seg_720" s="T26">[BrM:] tɨm = tɨmtɨ 'here'? [AAV:] A male voice overlapping.</ta>
            <ta e="T34" id="Seg_721" s="T33">[AAV:] Another speaker.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T51" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
