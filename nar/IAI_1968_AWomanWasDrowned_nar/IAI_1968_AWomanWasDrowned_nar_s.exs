<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>IAI_1968_AWomanWasDrowned_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">IAI_1968_AWomanWasDrowned_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">88</ud-information>
            <ud-information attribute-name="# HIAT:w">72</ud-information>
            <ud-information attribute-name="# e">72</ud-information>
            <ud-information attribute-name="# HIAT:u">15</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="IAI">
            <abbreviation>IAI</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="IAI"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T72" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">aːwǯʼel</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">miː</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">qwajaɣut</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">Parabelʼ</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">naǯadelʼe</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">mi</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">čaːǯut</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">muhtut</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">qup</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_35" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">rɨbzavodqɨt</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">miː</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">iːɣut</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">poːqlap</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_50" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">üːdətko</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">taqqənaft</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">čaːǯugu</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">maːt</ts>
                  <nts id="Seg_62" n="HIAT:ip">.</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_65" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17">okɨr-p</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">qup</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">keːk</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">əːšeːrbɨ</ts>
                  <nts id="Seg_77" n="HIAT:ip">.</nts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_80" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_82" n="HIAT:w" s="T21">a</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_85" n="HIAT:w" s="T22">nalgup</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">tabɨp</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">qwärat</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_94" n="HIAT:w" s="T25">Grunʼa</ts>
                  <nts id="Seg_95" n="HIAT:ip">.</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_98" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_100" n="HIAT:w" s="T26">tab</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_103" n="HIAT:w" s="T27">keːk</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_106" n="HIAT:w" s="T28">naːdərɨt</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_109" n="HIAT:w" s="T29">pʼarüdop</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_112" n="HIAT:w" s="T30">öːdogu</ts>
                  <nts id="Seg_113" n="HIAT:ip">.</nts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_116" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_118" n="HIAT:w" s="T31">twälak</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_121" n="HIAT:w" s="T32">qulandɨhto</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_124" n="HIAT:w" s="T33">omdɨmba</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_127" n="HIAT:w" s="T34">katʼert</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_130" n="HIAT:w" s="T35">moɣoɣot</ts>
                  <nts id="Seg_131" n="HIAT:ip">.</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_134" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_136" n="HIAT:w" s="T36">koʒat</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_139" n="HIAT:w" s="T37">ödombat</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_142" n="HIAT:w" s="T38">heːlal</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_145" n="HIAT:w" s="T39">čorgup</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_148" n="HIAT:w" s="T40">i</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_151" n="HIAT:w" s="T41">kɨgɨmba</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_154" n="HIAT:w" s="T42">waʒigu</ts>
                  <nts id="Seg_155" n="HIAT:ip">,</nts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_158" n="HIAT:w" s="T43">tabnan</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_161" n="HIAT:w" s="T44">toːbɨt</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_164" n="HIAT:w" s="T45">nʼašolba</ts>
                  <nts id="Seg_165" n="HIAT:ip">.</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_168" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_170" n="HIAT:w" s="T46">i</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_173" n="HIAT:w" s="T47">onǯ</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_176" n="HIAT:w" s="T48">aːlčimba</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_179" n="HIAT:w" s="T49">Goldont</ts>
                  <nts id="Seg_180" n="HIAT:ip">.</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_183" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_185" n="HIAT:w" s="T50">kodnaj</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_188" n="HIAT:w" s="T51">tabɨp</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_191" n="HIAT:w" s="T52">miːɣondot</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_194" n="HIAT:w" s="T53">aː</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_197" n="HIAT:w" s="T54">qonǯirbat</ts>
                  <nts id="Seg_198" n="HIAT:ip">.</nts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_201" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_203" n="HIAT:w" s="T55">moɣunäɣondo</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_206" n="HIAT:w" s="T56">čaːǯi</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_209" n="HIAT:w" s="T57">samohotka</ts>
                  <nts id="Seg_210" n="HIAT:ip">.</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_213" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_215" n="HIAT:w" s="T58">načaɣundu</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_218" n="HIAT:w" s="T59">tabla</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_221" n="HIAT:w" s="T60">konǯurbat</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_224" n="HIAT:w" s="T61">qanduk</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_227" n="HIAT:w" s="T62">tab</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_230" n="HIAT:w" s="T63">aːlči</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_233" n="HIAT:w" s="T64">öːt</ts>
                  <nts id="Seg_234" n="HIAT:ip">.</nts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_237" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_239" n="HIAT:w" s="T65">oralgu</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_242" n="HIAT:w" s="T66">tabɨp</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_245" n="HIAT:w" s="T67">aː</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_248" n="HIAT:w" s="T68">oːrannat</ts>
                  <nts id="Seg_249" n="HIAT:ip">.</nts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_252" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_254" n="HIAT:w" s="T69">niːk</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_257" n="HIAT:w" s="T70">tab</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_260" n="HIAT:w" s="T71">koːwa</ts>
                  <nts id="Seg_261" n="HIAT:ip">.</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T72" id="Seg_263" n="sc" s="T0">
               <ts e="T1" id="Seg_265" n="e" s="T0">aːwǯʼel </ts>
               <ts e="T2" id="Seg_267" n="e" s="T1">miː </ts>
               <ts e="T3" id="Seg_269" n="e" s="T2">qwajaɣut </ts>
               <ts e="T4" id="Seg_271" n="e" s="T3">Parabelʼ. </ts>
               <ts e="T5" id="Seg_273" n="e" s="T4">naǯadelʼe </ts>
               <ts e="T6" id="Seg_275" n="e" s="T5">mi </ts>
               <ts e="T7" id="Seg_277" n="e" s="T6">čaːǯut </ts>
               <ts e="T8" id="Seg_279" n="e" s="T7">muhtut </ts>
               <ts e="T9" id="Seg_281" n="e" s="T8">qup. </ts>
               <ts e="T10" id="Seg_283" n="e" s="T9">rɨbzavodqɨt </ts>
               <ts e="T11" id="Seg_285" n="e" s="T10">miː </ts>
               <ts e="T12" id="Seg_287" n="e" s="T11">iːɣut </ts>
               <ts e="T13" id="Seg_289" n="e" s="T12">poːqlap. </ts>
               <ts e="T14" id="Seg_291" n="e" s="T13">üːdətko </ts>
               <ts e="T15" id="Seg_293" n="e" s="T14">taqqənaft </ts>
               <ts e="T16" id="Seg_295" n="e" s="T15">čaːǯugu </ts>
               <ts e="T17" id="Seg_297" n="e" s="T16">maːt. </ts>
               <ts e="T18" id="Seg_299" n="e" s="T17">okɨr-p </ts>
               <ts e="T19" id="Seg_301" n="e" s="T18">qup </ts>
               <ts e="T20" id="Seg_303" n="e" s="T19">keːk </ts>
               <ts e="T21" id="Seg_305" n="e" s="T20">əːšeːrbɨ. </ts>
               <ts e="T22" id="Seg_307" n="e" s="T21">a </ts>
               <ts e="T23" id="Seg_309" n="e" s="T22">nalgup </ts>
               <ts e="T24" id="Seg_311" n="e" s="T23">tabɨp </ts>
               <ts e="T25" id="Seg_313" n="e" s="T24">qwärat </ts>
               <ts e="T26" id="Seg_315" n="e" s="T25">Grunʼa. </ts>
               <ts e="T27" id="Seg_317" n="e" s="T26">tab </ts>
               <ts e="T28" id="Seg_319" n="e" s="T27">keːk </ts>
               <ts e="T29" id="Seg_321" n="e" s="T28">naːdərɨt </ts>
               <ts e="T30" id="Seg_323" n="e" s="T29">pʼarüdop </ts>
               <ts e="T31" id="Seg_325" n="e" s="T30">öːdogu. </ts>
               <ts e="T32" id="Seg_327" n="e" s="T31">twälak </ts>
               <ts e="T33" id="Seg_329" n="e" s="T32">qulandɨhto </ts>
               <ts e="T34" id="Seg_331" n="e" s="T33">omdɨmba </ts>
               <ts e="T35" id="Seg_333" n="e" s="T34">katʼert </ts>
               <ts e="T36" id="Seg_335" n="e" s="T35">moɣoɣot. </ts>
               <ts e="T37" id="Seg_337" n="e" s="T36">koʒat </ts>
               <ts e="T38" id="Seg_339" n="e" s="T37">ödombat </ts>
               <ts e="T39" id="Seg_341" n="e" s="T38">heːlal </ts>
               <ts e="T40" id="Seg_343" n="e" s="T39">čorgup </ts>
               <ts e="T41" id="Seg_345" n="e" s="T40">i </ts>
               <ts e="T42" id="Seg_347" n="e" s="T41">kɨgɨmba </ts>
               <ts e="T43" id="Seg_349" n="e" s="T42">waʒigu, </ts>
               <ts e="T44" id="Seg_351" n="e" s="T43">tabnan </ts>
               <ts e="T45" id="Seg_353" n="e" s="T44">toːbɨt </ts>
               <ts e="T46" id="Seg_355" n="e" s="T45">nʼašolba. </ts>
               <ts e="T47" id="Seg_357" n="e" s="T46">i </ts>
               <ts e="T48" id="Seg_359" n="e" s="T47">onǯ </ts>
               <ts e="T49" id="Seg_361" n="e" s="T48">aːlčimba </ts>
               <ts e="T50" id="Seg_363" n="e" s="T49">Goldont. </ts>
               <ts e="T51" id="Seg_365" n="e" s="T50">kodnaj </ts>
               <ts e="T52" id="Seg_367" n="e" s="T51">tabɨp </ts>
               <ts e="T53" id="Seg_369" n="e" s="T52">miːɣondot </ts>
               <ts e="T54" id="Seg_371" n="e" s="T53">aː </ts>
               <ts e="T55" id="Seg_373" n="e" s="T54">qonǯirbat. </ts>
               <ts e="T56" id="Seg_375" n="e" s="T55">moɣunäɣondo </ts>
               <ts e="T57" id="Seg_377" n="e" s="T56">čaːǯi </ts>
               <ts e="T58" id="Seg_379" n="e" s="T57">samohotka. </ts>
               <ts e="T59" id="Seg_381" n="e" s="T58">načaɣundu </ts>
               <ts e="T60" id="Seg_383" n="e" s="T59">tabla </ts>
               <ts e="T61" id="Seg_385" n="e" s="T60">konǯurbat </ts>
               <ts e="T62" id="Seg_387" n="e" s="T61">qanduk </ts>
               <ts e="T63" id="Seg_389" n="e" s="T62">tab </ts>
               <ts e="T64" id="Seg_391" n="e" s="T63">aːlči </ts>
               <ts e="T65" id="Seg_393" n="e" s="T64">öːt. </ts>
               <ts e="T66" id="Seg_395" n="e" s="T65">oralgu </ts>
               <ts e="T67" id="Seg_397" n="e" s="T66">tabɨp </ts>
               <ts e="T68" id="Seg_399" n="e" s="T67">aː </ts>
               <ts e="T69" id="Seg_401" n="e" s="T68">oːrannat. </ts>
               <ts e="T70" id="Seg_403" n="e" s="T69">niːk </ts>
               <ts e="T71" id="Seg_405" n="e" s="T70">tab </ts>
               <ts e="T72" id="Seg_407" n="e" s="T71">koːwa. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_408" s="T0">IAI_1968_AWomanWasDrowned_nar.001 (001.001)</ta>
            <ta e="T9" id="Seg_409" s="T4">IAI_1968_AWomanWasDrowned_nar.002 (001.002)</ta>
            <ta e="T13" id="Seg_410" s="T9">IAI_1968_AWomanWasDrowned_nar.003 (001.003)</ta>
            <ta e="T17" id="Seg_411" s="T13">IAI_1968_AWomanWasDrowned_nar.004 (001.004)</ta>
            <ta e="T21" id="Seg_412" s="T17">IAI_1968_AWomanWasDrowned_nar.005 (001.005)</ta>
            <ta e="T26" id="Seg_413" s="T21">IAI_1968_AWomanWasDrowned_nar.006 (001.006)</ta>
            <ta e="T31" id="Seg_414" s="T26">IAI_1968_AWomanWasDrowned_nar.007 (001.007)</ta>
            <ta e="T36" id="Seg_415" s="T31">IAI_1968_AWomanWasDrowned_nar.008 (001.008)</ta>
            <ta e="T46" id="Seg_416" s="T36">IAI_1968_AWomanWasDrowned_nar.009 (001.009)</ta>
            <ta e="T50" id="Seg_417" s="T46">IAI_1968_AWomanWasDrowned_nar.010 (001.010)</ta>
            <ta e="T55" id="Seg_418" s="T50">IAI_1968_AWomanWasDrowned_nar.011 (001.011)</ta>
            <ta e="T58" id="Seg_419" s="T55">IAI_1968_AWomanWasDrowned_nar.012 (001.012)</ta>
            <ta e="T65" id="Seg_420" s="T58">IAI_1968_AWomanWasDrowned_nar.013 (001.013)</ta>
            <ta e="T69" id="Seg_421" s="T65">IAI_1968_AWomanWasDrowned_nar.014 (001.014)</ta>
            <ta e="T72" id="Seg_422" s="T69">IAI_1968_AWomanWasDrowned_nar.015 (001.015)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_423" s="T0">′а̄wдʼжʼеl мӣ kwа′jаɣут пара′белʼ.</ta>
            <ta e="T9" id="Seg_424" s="T4">на‵джа′делʼе ми ′ча̄джут ′муhтут kуп.</ta>
            <ta e="T13" id="Seg_425" s="T9">рыбза′водkы(о)т мӣ ӣɣут ‵по̄kлап.</ta>
            <ta e="T17" id="Seg_426" s="T13">′ӱ̄дътко ‵таkkъ′нафт ′ча̄джугу ма̄т.</ta>
            <ta e="T21" id="Seg_427" s="T17">′окырп kуп ке̄к ъ̊̄′ше̄рбы.</ta>
            <ta e="T26" id="Seg_428" s="T21">а нал′гуп та′бып ′kwӓрат ′грунʼа.</ta>
            <ta e="T31" id="Seg_429" s="T26">′таб ке̄к ′на̄дърыт ‵пʼа′рӱдоп ӧ̄до′гу.</ta>
            <ta e="T36" id="Seg_430" s="T31">′твӓлак ′kуlандыhто ‵омды′мба катʼерт ‵мо′ɣоɣот.</ta>
            <ta e="T46" id="Seg_431" s="T36">ко′жат ӧдом′бат ′hе̨̄лал ′чоргуп и кыгы′мба важи′гу, таб′нан ‵то̄′быт нʼа′шолба.</ta>
            <ta e="T50" id="Seg_432" s="T46">и ′ондж ′а̄lчимба ‵kоl′донт.</ta>
            <ta e="T55" id="Seg_433" s="T50">′коднай та′бып ′мӣɣондо(у)т а̄ ‵kонджирбат.</ta>
            <ta e="T58" id="Seg_434" s="T55">‵моɣунӓɣондо ча̄джи ‵само′hотка.</ta>
            <ta e="T65" id="Seg_435" s="T58">на′чаɣунду та′бла ‵конджурбат kан′дук таб ′а̄lчи ′ӧ̄т.</ta>
            <ta e="T69" id="Seg_436" s="T65">о′ралгу та′бып а̄ о̄′раннат.</ta>
            <ta e="T72" id="Seg_437" s="T69">нӣк таб ко̄′ва.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_438" s="T0">aːwǯʼel miː qwajaɣut parabelʼ.</ta>
            <ta e="T9" id="Seg_439" s="T4">naǯadelʼe mi čaːǯut muhtut qup.</ta>
            <ta e="T13" id="Seg_440" s="T9">rɨbzavodqɨ(o)t miː iːɣut poːqlap.</ta>
            <ta e="T17" id="Seg_441" s="T13">üːdətko taqqənaft čaːǯugu maːt.</ta>
            <ta e="T21" id="Seg_442" s="T17">okɨrp qup keːk əːšeːrbɨ.</ta>
            <ta e="T26" id="Seg_443" s="T21">a nalgup tabɨp qwärat grunʼa.</ta>
            <ta e="T31" id="Seg_444" s="T26">tab keːk naːdərɨt pʼarüdop öːdogu.</ta>
            <ta e="T36" id="Seg_445" s="T31">tvälak qulandɨhto omdɨmba katʼert moɣoɣot.</ta>
            <ta e="T46" id="Seg_446" s="T36">koʒat ödombat heːlal čorgup i kɨgɨmba vaʒigu, tabnan toːbɨt nʼašolba.</ta>
            <ta e="T50" id="Seg_447" s="T46">i onǯ aːlčimba qoldont.</ta>
            <ta e="T55" id="Seg_448" s="T50">kodnaj tabɨp miːɣondo(u)t aː qonǯirbat.</ta>
            <ta e="T58" id="Seg_449" s="T55">moɣunäɣondo čaːǯi samohotka.</ta>
            <ta e="T65" id="Seg_450" s="T58">načaɣundu tabla konǯurbat qanduk tab aːlči öːt.</ta>
            <ta e="T69" id="Seg_451" s="T65">oralgu tabɨp aː oːrannat.</ta>
            <ta e="T72" id="Seg_452" s="T69">niːk tab koːva.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_453" s="T0">aːwǯʼel miː qwajaɣut Parabelʼ. </ta>
            <ta e="T9" id="Seg_454" s="T4">naǯadelʼe mi čaːǯut muhtut qup. </ta>
            <ta e="T13" id="Seg_455" s="T9">rɨbzavodqɨt miː iːɣut poːqlap. </ta>
            <ta e="T17" id="Seg_456" s="T13">üːdətko taqqənaft čaːǯugu maːt. </ta>
            <ta e="T21" id="Seg_457" s="T17">okɨrp qup keːk əːšeːrbɨ. </ta>
            <ta e="T26" id="Seg_458" s="T21">a nalgup tabɨp qwärat Grunʼa. </ta>
            <ta e="T31" id="Seg_459" s="T26">tab keːk naːdərɨt pʼarüdop öːdogu. </ta>
            <ta e="T36" id="Seg_460" s="T31">twälak qulandɨhto omdɨmba katʼert moɣoɣot. </ta>
            <ta e="T46" id="Seg_461" s="T36">koʒat ödombat heːlal čorgup i kɨgɨmba waʒigu, tabnan toːbɨt nʼašolba. </ta>
            <ta e="T50" id="Seg_462" s="T46">i onǯ aːlčimba Goldont. </ta>
            <ta e="T55" id="Seg_463" s="T50">kodnaj tabɨp miːɣondot aː qonǯirbat. </ta>
            <ta e="T58" id="Seg_464" s="T55">moɣunäɣondo čaːǯi samohotka. </ta>
            <ta e="T65" id="Seg_465" s="T58">načaɣundu tabla konǯurbat qanduk tab aːlči öːt. </ta>
            <ta e="T69" id="Seg_466" s="T65">oralgu tabɨp aː oːrannat. </ta>
            <ta e="T72" id="Seg_467" s="T69">niːk tab koːwa. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_468" s="T0">aːwǯʼel</ta>
            <ta e="T2" id="Seg_469" s="T1">miː</ta>
            <ta e="T3" id="Seg_470" s="T2">qwaja-ɣ-ut</ta>
            <ta e="T4" id="Seg_471" s="T3">Parabelʼ</ta>
            <ta e="T5" id="Seg_472" s="T4">naǯadelʼe</ta>
            <ta e="T6" id="Seg_473" s="T5">mi</ta>
            <ta e="T7" id="Seg_474" s="T6">čaːǯu-t</ta>
            <ta e="T8" id="Seg_475" s="T7">muhtut</ta>
            <ta e="T9" id="Seg_476" s="T8">qup</ta>
            <ta e="T10" id="Seg_477" s="T9">rɨbzavod-qɨt</ta>
            <ta e="T11" id="Seg_478" s="T10">miː</ta>
            <ta e="T12" id="Seg_479" s="T11">iː-ɣ-ut</ta>
            <ta e="T13" id="Seg_480" s="T12">poːq-la-p</ta>
            <ta e="T14" id="Seg_481" s="T13">üːdə-tko</ta>
            <ta e="T15" id="Seg_482" s="T14">taqqə-na-ft</ta>
            <ta e="T16" id="Seg_483" s="T15">čaːǯu-gu</ta>
            <ta e="T17" id="Seg_484" s="T16">maːt</ta>
            <ta e="T18" id="Seg_485" s="T17">okɨr-p</ta>
            <ta e="T19" id="Seg_486" s="T18">qup</ta>
            <ta e="T20" id="Seg_487" s="T19">keːk</ta>
            <ta e="T21" id="Seg_488" s="T20">əːšeːr-bɨ</ta>
            <ta e="T22" id="Seg_489" s="T21">a</ta>
            <ta e="T23" id="Seg_490" s="T22">na-lʼ-gup</ta>
            <ta e="T24" id="Seg_491" s="T23">tab-ɨ-p</ta>
            <ta e="T25" id="Seg_492" s="T24">qwär-a-t</ta>
            <ta e="T26" id="Seg_493" s="T25">Grunʼa</ta>
            <ta e="T27" id="Seg_494" s="T26">tab</ta>
            <ta e="T28" id="Seg_495" s="T27">keːk</ta>
            <ta e="T29" id="Seg_496" s="T28">naːdər-ɨ-t</ta>
            <ta e="T30" id="Seg_497" s="T29">pʼar-üdo-p</ta>
            <ta e="T31" id="Seg_498" s="T30">öːdo-gu</ta>
            <ta e="T32" id="Seg_499" s="T31">twälak</ta>
            <ta e="T33" id="Seg_500" s="T32">qu-la-ndɨhto</ta>
            <ta e="T34" id="Seg_501" s="T33">omdɨ-mba</ta>
            <ta e="T35" id="Seg_502" s="T34">katʼer-t</ta>
            <ta e="T36" id="Seg_503" s="T35">moqo-ɣot</ta>
            <ta e="T37" id="Seg_504" s="T36">koʒa-t</ta>
            <ta e="T38" id="Seg_505" s="T37">ödo-mba-t</ta>
            <ta e="T39" id="Seg_506" s="T38">heːlal</ta>
            <ta e="T40" id="Seg_507" s="T39">čorg-u-p</ta>
            <ta e="T41" id="Seg_508" s="T40">i</ta>
            <ta e="T42" id="Seg_509" s="T41">kɨgɨ-mba</ta>
            <ta e="T43" id="Seg_510" s="T42">waʒi-gu</ta>
            <ta e="T44" id="Seg_511" s="T43">tab-nan</ta>
            <ta e="T45" id="Seg_512" s="T44">toːb-ɨ-t</ta>
            <ta e="T46" id="Seg_513" s="T45">nʼašol-ba</ta>
            <ta e="T47" id="Seg_514" s="T46">i</ta>
            <ta e="T48" id="Seg_515" s="T47">onǯ</ta>
            <ta e="T49" id="Seg_516" s="T48">aːlči-mba</ta>
            <ta e="T50" id="Seg_517" s="T49">Gold-o-nt</ta>
            <ta e="T51" id="Seg_518" s="T50">kod-naj</ta>
            <ta e="T52" id="Seg_519" s="T51">tab-ɨ-p</ta>
            <ta e="T53" id="Seg_520" s="T52">miː-ɣondot</ta>
            <ta e="T54" id="Seg_521" s="T53">aː</ta>
            <ta e="T55" id="Seg_522" s="T54">qo-nǯ-i-r-ba-t</ta>
            <ta e="T56" id="Seg_523" s="T55">moɣu-nä-ɣondo</ta>
            <ta e="T57" id="Seg_524" s="T56">čaːǯi</ta>
            <ta e="T58" id="Seg_525" s="T57">samohotka</ta>
            <ta e="T59" id="Seg_526" s="T58">nača-ɣundu</ta>
            <ta e="T60" id="Seg_527" s="T59">tab-la</ta>
            <ta e="T61" id="Seg_528" s="T60">ko-nǯu-r-ba-t</ta>
            <ta e="T62" id="Seg_529" s="T61">qanduk</ta>
            <ta e="T63" id="Seg_530" s="T62">tab</ta>
            <ta e="T64" id="Seg_531" s="T63">aːlči</ta>
            <ta e="T65" id="Seg_532" s="T64">öːt</ta>
            <ta e="T66" id="Seg_533" s="T65">oral-gu</ta>
            <ta e="T67" id="Seg_534" s="T66">tab-ɨ-p</ta>
            <ta e="T68" id="Seg_535" s="T67">aː</ta>
            <ta e="T69" id="Seg_536" s="T68">oːran-na-t</ta>
            <ta e="T70" id="Seg_537" s="T69">niː-k</ta>
            <ta e="T71" id="Seg_538" s="T70">tab</ta>
            <ta e="T72" id="Seg_539" s="T71">koːw-a</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_540" s="T0">aːwǯʼel</ta>
            <ta e="T2" id="Seg_541" s="T1">mi</ta>
            <ta e="T3" id="Seg_542" s="T2">qwaja-ŋɨ-ut</ta>
            <ta e="T4" id="Seg_543" s="T3">Parabelʼ</ta>
            <ta e="T5" id="Seg_544" s="T4">načidelʼi</ta>
            <ta e="T6" id="Seg_545" s="T5">mi</ta>
            <ta e="T7" id="Seg_546" s="T6">čaːǯɨ-dət</ta>
            <ta e="T8" id="Seg_547" s="T7">muktət</ta>
            <ta e="T9" id="Seg_548" s="T8">qum</ta>
            <ta e="T10" id="Seg_549" s="T9">rɨbzavod-qɨn</ta>
            <ta e="T11" id="Seg_550" s="T10">mi</ta>
            <ta e="T12" id="Seg_551" s="T11">iː-ŋɨ-ut</ta>
            <ta e="T13" id="Seg_552" s="T12">poq-la-p</ta>
            <ta e="T14" id="Seg_553" s="T13">üːtǝ-tqo</ta>
            <ta e="T15" id="Seg_554" s="T14">taqqə-ŋɨ-ut</ta>
            <ta e="T16" id="Seg_555" s="T15">čaːǯɨ-gu</ta>
            <ta e="T17" id="Seg_556" s="T16">maːt</ta>
            <ta e="T18" id="Seg_557" s="T17">okkər-p</ta>
            <ta e="T19" id="Seg_558" s="T18">qum</ta>
            <ta e="T20" id="Seg_559" s="T19">keːk</ta>
            <ta e="T21" id="Seg_560" s="T20">əːšeːr-mbɨ</ta>
            <ta e="T22" id="Seg_561" s="T21">a</ta>
            <ta e="T23" id="Seg_562" s="T22">neː-lʼ-qum</ta>
            <ta e="T24" id="Seg_563" s="T23">tab-ɨ-m</ta>
            <ta e="T25" id="Seg_564" s="T24">qwär-ɨ-t</ta>
            <ta e="T26" id="Seg_565" s="T25">Grunʼa</ta>
            <ta e="T27" id="Seg_566" s="T26">tab</ta>
            <ta e="T28" id="Seg_567" s="T27">keːk</ta>
            <ta e="T29" id="Seg_568" s="T28">naːdɨr-ɨ-t</ta>
            <ta e="T30" id="Seg_569" s="T29">parüdi-üt-p</ta>
            <ta e="T31" id="Seg_570" s="T30">üdɨ-gu</ta>
            <ta e="T32" id="Seg_571" s="T31">twälak</ta>
            <ta e="T33" id="Seg_572" s="T32">qum-la-qondot</ta>
            <ta e="T34" id="Seg_573" s="T33">omde-mbɨ</ta>
            <ta e="T35" id="Seg_574" s="T34">katʼer-n</ta>
            <ta e="T36" id="Seg_575" s="T35">moqo-qɨt</ta>
            <ta e="T37" id="Seg_576" s="T36">kuča-t</ta>
            <ta e="T38" id="Seg_577" s="T37">üdɨ-mbɨ-t</ta>
            <ta e="T39" id="Seg_578" s="T38">helal</ta>
            <ta e="T40" id="Seg_579" s="T39">čorg-ɨ-p</ta>
            <ta e="T41" id="Seg_580" s="T40">i</ta>
            <ta e="T42" id="Seg_581" s="T41">kɨge-mbɨ</ta>
            <ta e="T43" id="Seg_582" s="T42">waše-gu</ta>
            <ta e="T44" id="Seg_583" s="T43">tab-nan</ta>
            <ta e="T45" id="Seg_584" s="T44">tob-ɨ-t</ta>
            <ta e="T46" id="Seg_585" s="T45">nʼašol-mbɨ</ta>
            <ta e="T47" id="Seg_586" s="T46">i</ta>
            <ta e="T48" id="Seg_587" s="T47">onǯe</ta>
            <ta e="T49" id="Seg_588" s="T48">alʼči-mbɨ</ta>
            <ta e="T50" id="Seg_589" s="T49">Kold-ɨ-nde</ta>
            <ta e="T51" id="Seg_590" s="T50">kutɨ-naj</ta>
            <ta e="T52" id="Seg_591" s="T51">tab-ɨ-m</ta>
            <ta e="T53" id="Seg_592" s="T52">mi-qondot</ta>
            <ta e="T54" id="Seg_593" s="T53">aː</ta>
            <ta e="T55" id="Seg_594" s="T54">qo-nǯe-ɨ-r-mbɨ-t</ta>
            <ta e="T56" id="Seg_595" s="T55">moqo-nɨ-nando</ta>
            <ta e="T57" id="Seg_596" s="T56">čaːǯɨ</ta>
            <ta e="T58" id="Seg_597" s="T57">samohotka</ta>
            <ta e="T59" id="Seg_598" s="T58">nača-nando</ta>
            <ta e="T60" id="Seg_599" s="T59">tab-la</ta>
            <ta e="T61" id="Seg_600" s="T60">qo-nǯe-r-mbɨ-dət</ta>
            <ta e="T62" id="Seg_601" s="T61">qanduk</ta>
            <ta e="T63" id="Seg_602" s="T62">tab</ta>
            <ta e="T64" id="Seg_603" s="T63">alʼči</ta>
            <ta e="T65" id="Seg_604" s="T64">üt</ta>
            <ta e="T66" id="Seg_605" s="T65">oral-gu</ta>
            <ta e="T67" id="Seg_606" s="T66">tab-ɨ-m</ta>
            <ta e="T68" id="Seg_607" s="T67">aː</ta>
            <ta e="T69" id="Seg_608" s="T68">oral-ŋɨ-dət</ta>
            <ta e="T70" id="Seg_609" s="T69">nɨdi-k</ta>
            <ta e="T71" id="Seg_610" s="T70">tab</ta>
            <ta e="T72" id="Seg_611" s="T71">kom-ɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_612" s="T0">day.before.yesterday</ta>
            <ta e="T2" id="Seg_613" s="T1">we.PL.NOM</ta>
            <ta e="T3" id="Seg_614" s="T2">go-CO-1PL</ta>
            <ta e="T4" id="Seg_615" s="T3">Parabel.[NOM]</ta>
            <ta e="T5" id="Seg_616" s="T4">there</ta>
            <ta e="T6" id="Seg_617" s="T5">we.PL.NOM</ta>
            <ta e="T7" id="Seg_618" s="T6">go-3PL</ta>
            <ta e="T8" id="Seg_619" s="T7">six.[NOM]</ta>
            <ta e="T9" id="Seg_620" s="T8">human.being.[NOM]</ta>
            <ta e="T10" id="Seg_621" s="T9">fish.factory-LOC</ta>
            <ta e="T11" id="Seg_622" s="T10">we.PL.NOM</ta>
            <ta e="T12" id="Seg_623" s="T11">take-CO-1PL</ta>
            <ta e="T13" id="Seg_624" s="T12">net-PL-ACC</ta>
            <ta e="T14" id="Seg_625" s="T13">evening-TRL</ta>
            <ta e="T15" id="Seg_626" s="T14">get.together-CO-1PL</ta>
            <ta e="T16" id="Seg_627" s="T15">go-INF</ta>
            <ta e="T17" id="Seg_628" s="T16">house.[NOM]</ta>
            <ta e="T18" id="Seg_629" s="T17">one-%%</ta>
            <ta e="T19" id="Seg_630" s="T18">human.being.[NOM]</ta>
            <ta e="T20" id="Seg_631" s="T19">very</ta>
            <ta e="T21" id="Seg_632" s="T20">get.drunk-PST.NAR.[3SG.S]</ta>
            <ta e="T22" id="Seg_633" s="T21">but</ta>
            <ta e="T23" id="Seg_634" s="T22">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T24" id="Seg_635" s="T23">(s)he-EP-ACC</ta>
            <ta e="T25" id="Seg_636" s="T24">call-EP-3SG.O</ta>
            <ta e="T26" id="Seg_637" s="T25">Grunya.[NOM]</ta>
            <ta e="T27" id="Seg_638" s="T26">(s)he.[NOM]</ta>
            <ta e="T28" id="Seg_639" s="T27">very</ta>
            <ta e="T29" id="Seg_640" s="T28">love-EP-3SG.O</ta>
            <ta e="T30" id="Seg_641" s="T29">bitter-water-ACC</ta>
            <ta e="T31" id="Seg_642" s="T30">drink-INF</ta>
            <ta e="T32" id="Seg_643" s="T31">%%</ta>
            <ta e="T33" id="Seg_644" s="T32">human.being-PL-ILL/LOC/EL.1PL</ta>
            <ta e="T34" id="Seg_645" s="T33">sit.down-PST.NAR.[3SG.S]</ta>
            <ta e="T35" id="Seg_646" s="T34">boat-GEN</ta>
            <ta e="T36" id="Seg_647" s="T35">back-LOC</ta>
            <ta e="T37" id="Seg_648" s="T36">when-ADV.LOC</ta>
            <ta e="T38" id="Seg_649" s="T37">drink-PST.NAR-3SG.O</ta>
            <ta e="T39" id="Seg_650" s="T38">whole</ta>
            <ta e="T40" id="Seg_651" s="T39">bottle-EP-ACC</ta>
            <ta e="T41" id="Seg_652" s="T40">and</ta>
            <ta e="T42" id="Seg_653" s="T41">want-PST.NAR.[3SG.S]</ta>
            <ta e="T43" id="Seg_654" s="T42">get.up-INF</ta>
            <ta e="T44" id="Seg_655" s="T43">(s)he-ADES</ta>
            <ta e="T45" id="Seg_656" s="T44">leg-EP-3SG</ta>
            <ta e="T46" id="Seg_657" s="T45">roll-PST.NAR.[3SG.S]</ta>
            <ta e="T47" id="Seg_658" s="T46">and</ta>
            <ta e="T48" id="Seg_659" s="T47">oneself.3SG</ta>
            <ta e="T49" id="Seg_660" s="T48">fall-PST.NAR.[3SG.S]</ta>
            <ta e="T50" id="Seg_661" s="T49">Ob-EP-ILL</ta>
            <ta e="T51" id="Seg_662" s="T50">who-EMPH.[NOM]</ta>
            <ta e="T52" id="Seg_663" s="T51">(s)he-EP-ACC</ta>
            <ta e="T53" id="Seg_664" s="T52">we.DU.NOM-ILL/LOC/EL.1PL</ta>
            <ta e="T54" id="Seg_665" s="T53">NEG</ta>
            <ta e="T55" id="Seg_666" s="T54">see-IPFV3-EP-FRQ-PST.NAR-3SG.O</ta>
            <ta e="T56" id="Seg_667" s="T55">back-OBL.1SG-ABL</ta>
            <ta e="T57" id="Seg_668" s="T56">go.[3SG.S]</ta>
            <ta e="T58" id="Seg_669" s="T57">self_propelled.gun.[NOM]</ta>
            <ta e="T59" id="Seg_670" s="T58">there-ABL</ta>
            <ta e="T60" id="Seg_671" s="T59">(s)he-PL.[NOM]</ta>
            <ta e="T61" id="Seg_672" s="T60">see-IPFV3-FRQ-PST.NAR-3PL</ta>
            <ta e="T62" id="Seg_673" s="T61">how</ta>
            <ta e="T63" id="Seg_674" s="T62">(s)he.[NOM]</ta>
            <ta e="T64" id="Seg_675" s="T63">fall.[3SG.S]</ta>
            <ta e="T65" id="Seg_676" s="T64">water.[NOM]</ta>
            <ta e="T66" id="Seg_677" s="T65">catch-INF</ta>
            <ta e="T67" id="Seg_678" s="T66">(s)he-EP-ACC</ta>
            <ta e="T68" id="Seg_679" s="T67">NEG</ta>
            <ta e="T69" id="Seg_680" s="T68">catch-CO-3PL</ta>
            <ta e="T70" id="Seg_681" s="T69">such-ADV</ta>
            <ta e="T71" id="Seg_682" s="T70">(s)he.[NOM]</ta>
            <ta e="T72" id="Seg_683" s="T71">drown-EP.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_684" s="T0">позавчера</ta>
            <ta e="T2" id="Seg_685" s="T1">мы.PL.NOM</ta>
            <ta e="T3" id="Seg_686" s="T2">идти-CO-1PL</ta>
            <ta e="T4" id="Seg_687" s="T3">Парабель.[NOM]</ta>
            <ta e="T5" id="Seg_688" s="T4">туда</ta>
            <ta e="T6" id="Seg_689" s="T5">мы.PL.NOM</ta>
            <ta e="T7" id="Seg_690" s="T6">идти-3PL</ta>
            <ta e="T8" id="Seg_691" s="T7">шесть.[NOM]</ta>
            <ta e="T9" id="Seg_692" s="T8">человек.[NOM]</ta>
            <ta e="T10" id="Seg_693" s="T9">рыбзавод-LOC</ta>
            <ta e="T11" id="Seg_694" s="T10">мы.PL.NOM</ta>
            <ta e="T12" id="Seg_695" s="T11">взять-CO-1PL</ta>
            <ta e="T13" id="Seg_696" s="T12">сеть-PL-ACC</ta>
            <ta e="T14" id="Seg_697" s="T13">вечер-TRL</ta>
            <ta e="T15" id="Seg_698" s="T14">собраться-CO-1PL</ta>
            <ta e="T16" id="Seg_699" s="T15">идти-INF</ta>
            <ta e="T17" id="Seg_700" s="T16">дом.[NOM]</ta>
            <ta e="T18" id="Seg_701" s="T17">один-%%</ta>
            <ta e="T19" id="Seg_702" s="T18">человек.[NOM]</ta>
            <ta e="T20" id="Seg_703" s="T19">очень</ta>
            <ta e="T21" id="Seg_704" s="T20">опьянеть-PST.NAR.[3SG.S]</ta>
            <ta e="T22" id="Seg_705" s="T21">а</ta>
            <ta e="T23" id="Seg_706" s="T22">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T24" id="Seg_707" s="T23">он(а)-EP-ACC</ta>
            <ta e="T25" id="Seg_708" s="T24">позвать-EP-3SG.O</ta>
            <ta e="T26" id="Seg_709" s="T25">Груня.[NOM]</ta>
            <ta e="T27" id="Seg_710" s="T26">он(а).[NOM]</ta>
            <ta e="T28" id="Seg_711" s="T27">очень</ta>
            <ta e="T29" id="Seg_712" s="T28">любить-EP-3SG.O</ta>
            <ta e="T30" id="Seg_713" s="T29">горький-вода-ACC</ta>
            <ta e="T31" id="Seg_714" s="T30">пить-INF</ta>
            <ta e="T32" id="Seg_715" s="T31">%%</ta>
            <ta e="T33" id="Seg_716" s="T32">человек-PL-ILL/LOC/EL.1PL</ta>
            <ta e="T34" id="Seg_717" s="T33">сесть-PST.NAR.[3SG.S]</ta>
            <ta e="T35" id="Seg_718" s="T34">катер-GEN</ta>
            <ta e="T36" id="Seg_719" s="T35">спина-LOC</ta>
            <ta e="T37" id="Seg_720" s="T36">когда-ADV.LOC</ta>
            <ta e="T38" id="Seg_721" s="T37">пить-PST.NAR-3SG.O</ta>
            <ta e="T39" id="Seg_722" s="T38">целый;</ta>
            <ta e="T40" id="Seg_723" s="T39">бутылка-EP-ACC</ta>
            <ta e="T41" id="Seg_724" s="T40">и</ta>
            <ta e="T42" id="Seg_725" s="T41">хотеть-PST.NAR.[3SG.S]</ta>
            <ta e="T43" id="Seg_726" s="T42">вставать-INF</ta>
            <ta e="T44" id="Seg_727" s="T43">он(а)-ADES</ta>
            <ta e="T45" id="Seg_728" s="T44">нога-EP-3SG</ta>
            <ta e="T46" id="Seg_729" s="T45">скатиться-PST.NAR.[3SG.S]</ta>
            <ta e="T47" id="Seg_730" s="T46">и</ta>
            <ta e="T48" id="Seg_731" s="T47">сам.3SG</ta>
            <ta e="T49" id="Seg_732" s="T48">упасть-PST.NAR.[3SG.S]</ta>
            <ta e="T50" id="Seg_733" s="T49">Обь-EP-ILL</ta>
            <ta e="T51" id="Seg_734" s="T50">кто-EMPH.[NOM]</ta>
            <ta e="T52" id="Seg_735" s="T51">он(а)-EP-ACC</ta>
            <ta e="T53" id="Seg_736" s="T52">мы.DU.NOM-ILL/LOC/EL.1PL</ta>
            <ta e="T54" id="Seg_737" s="T53">NEG</ta>
            <ta e="T55" id="Seg_738" s="T54">увидеть-IPFV3-EP-FRQ-PST.NAR-3SG.O</ta>
            <ta e="T56" id="Seg_739" s="T55">спина-OBL.1SG-ABL</ta>
            <ta e="T57" id="Seg_740" s="T56">идти.[3SG.S]</ta>
            <ta e="T58" id="Seg_741" s="T57">самоходка.[NOM]</ta>
            <ta e="T59" id="Seg_742" s="T58">там-ABL</ta>
            <ta e="T60" id="Seg_743" s="T59">он(а)-PL.[NOM]</ta>
            <ta e="T61" id="Seg_744" s="T60">увидеть-IPFV3-FRQ-PST.NAR-3PL</ta>
            <ta e="T62" id="Seg_745" s="T61">как</ta>
            <ta e="T63" id="Seg_746" s="T62">он(а).[NOM]</ta>
            <ta e="T64" id="Seg_747" s="T63">упасть.[3SG.S]</ta>
            <ta e="T65" id="Seg_748" s="T64">вода.[NOM]</ta>
            <ta e="T66" id="Seg_749" s="T65">поймать-INF</ta>
            <ta e="T67" id="Seg_750" s="T66">он(а)-EP-ACC</ta>
            <ta e="T68" id="Seg_751" s="T67">NEG</ta>
            <ta e="T69" id="Seg_752" s="T68">поймать-CO-3PL</ta>
            <ta e="T70" id="Seg_753" s="T69">такой-ADV</ta>
            <ta e="T71" id="Seg_754" s="T70">он(а).[NOM]</ta>
            <ta e="T72" id="Seg_755" s="T71">утонуть-EP.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_756" s="T0">n</ta>
            <ta e="T2" id="Seg_757" s="T1">pers</ta>
            <ta e="T3" id="Seg_758" s="T2">v-v:ins-v:pn</ta>
            <ta e="T4" id="Seg_759" s="T3">nprop-n:case</ta>
            <ta e="T5" id="Seg_760" s="T4">adv</ta>
            <ta e="T6" id="Seg_761" s="T5">pers</ta>
            <ta e="T7" id="Seg_762" s="T6">v-v:pn</ta>
            <ta e="T8" id="Seg_763" s="T7">num-n:case</ta>
            <ta e="T9" id="Seg_764" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_765" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_766" s="T10">pers</ta>
            <ta e="T12" id="Seg_767" s="T11">v-v:ins-v:pn</ta>
            <ta e="T13" id="Seg_768" s="T12">n-n:num-n:case</ta>
            <ta e="T14" id="Seg_769" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_770" s="T14">v-v:ins-v:pn</ta>
            <ta e="T16" id="Seg_771" s="T15">v-v:inf</ta>
            <ta e="T17" id="Seg_772" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_773" s="T17">num--%%</ta>
            <ta e="T19" id="Seg_774" s="T18">n-n:case</ta>
            <ta e="T20" id="Seg_775" s="T19">adv</ta>
            <ta e="T21" id="Seg_776" s="T20">v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_777" s="T21">conj</ta>
            <ta e="T23" id="Seg_778" s="T22">n-n&gt;adj-n-n:case</ta>
            <ta e="T24" id="Seg_779" s="T23">pers-n:ins-n:case</ta>
            <ta e="T25" id="Seg_780" s="T24">v-v:ins-v:pn</ta>
            <ta e="T26" id="Seg_781" s="T25">nprop-n:case</ta>
            <ta e="T27" id="Seg_782" s="T26">pers-n:case</ta>
            <ta e="T28" id="Seg_783" s="T27">adv</ta>
            <ta e="T29" id="Seg_784" s="T28">v-v:ins-v:pn</ta>
            <ta e="T30" id="Seg_785" s="T29">adj-n-n:case</ta>
            <ta e="T31" id="Seg_786" s="T30">v-v:inf</ta>
            <ta e="T32" id="Seg_787" s="T31">adj</ta>
            <ta e="T33" id="Seg_788" s="T32">n-n:num-n:case.poss</ta>
            <ta e="T34" id="Seg_789" s="T33">v-v:tense-v:pn</ta>
            <ta e="T35" id="Seg_790" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_791" s="T35">n-n:case</ta>
            <ta e="T37" id="Seg_792" s="T36">interrog-adv:case</ta>
            <ta e="T38" id="Seg_793" s="T37">v-v:tense-v:pn</ta>
            <ta e="T39" id="Seg_794" s="T38">adj</ta>
            <ta e="T40" id="Seg_795" s="T39">n-n:ins-n:case</ta>
            <ta e="T41" id="Seg_796" s="T40">conj</ta>
            <ta e="T42" id="Seg_797" s="T41">v-v:tense-v:pn</ta>
            <ta e="T43" id="Seg_798" s="T42">v-v:inf</ta>
            <ta e="T44" id="Seg_799" s="T43">pers-n:case</ta>
            <ta e="T45" id="Seg_800" s="T44">n-n:ins-n:poss</ta>
            <ta e="T46" id="Seg_801" s="T45">v-v:tense-v:pn</ta>
            <ta e="T47" id="Seg_802" s="T46">conj</ta>
            <ta e="T48" id="Seg_803" s="T47">emphpro</ta>
            <ta e="T49" id="Seg_804" s="T48">v-v:tense-v:pn</ta>
            <ta e="T50" id="Seg_805" s="T49">nprop-n:ins-n:case</ta>
            <ta e="T51" id="Seg_806" s="T50">interrog-clit-n:case</ta>
            <ta e="T52" id="Seg_807" s="T51">pers-n:ins-n:case</ta>
            <ta e="T53" id="Seg_808" s="T52">pers-n:case.poss</ta>
            <ta e="T54" id="Seg_809" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_810" s="T54">v-v&gt;v-v:ins-v&gt;v-v:tense-v:pn</ta>
            <ta e="T56" id="Seg_811" s="T55">n-n:obl.poss-n:case</ta>
            <ta e="T57" id="Seg_812" s="T56">v-v:pn</ta>
            <ta e="T58" id="Seg_813" s="T57">n-n:case</ta>
            <ta e="T59" id="Seg_814" s="T58">adv-n:case</ta>
            <ta e="T60" id="Seg_815" s="T59">pers-n:num-n:case</ta>
            <ta e="T61" id="Seg_816" s="T60">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T62" id="Seg_817" s="T61">interrog</ta>
            <ta e="T63" id="Seg_818" s="T62">pers-n:case</ta>
            <ta e="T64" id="Seg_819" s="T63">v-v:pn</ta>
            <ta e="T65" id="Seg_820" s="T64">n-n:case</ta>
            <ta e="T66" id="Seg_821" s="T65">v-v:inf</ta>
            <ta e="T67" id="Seg_822" s="T66">pers-n:ins-n:case</ta>
            <ta e="T68" id="Seg_823" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_824" s="T68">v-v:ins-v:pn</ta>
            <ta e="T70" id="Seg_825" s="T69">dem-adj&gt;adv</ta>
            <ta e="T71" id="Seg_826" s="T70">pers-n:case</ta>
            <ta e="T72" id="Seg_827" s="T71">v-v:ins-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_828" s="T0">n</ta>
            <ta e="T2" id="Seg_829" s="T1">pers</ta>
            <ta e="T3" id="Seg_830" s="T2">v</ta>
            <ta e="T4" id="Seg_831" s="T3">nprop</ta>
            <ta e="T5" id="Seg_832" s="T4">adv</ta>
            <ta e="T6" id="Seg_833" s="T5">pers</ta>
            <ta e="T7" id="Seg_834" s="T6">v</ta>
            <ta e="T8" id="Seg_835" s="T7">num</ta>
            <ta e="T9" id="Seg_836" s="T8">n</ta>
            <ta e="T10" id="Seg_837" s="T9">n</ta>
            <ta e="T11" id="Seg_838" s="T10">pers</ta>
            <ta e="T12" id="Seg_839" s="T11">n</ta>
            <ta e="T13" id="Seg_840" s="T12">n</ta>
            <ta e="T14" id="Seg_841" s="T13">n</ta>
            <ta e="T15" id="Seg_842" s="T14">v</ta>
            <ta e="T16" id="Seg_843" s="T15">v</ta>
            <ta e="T17" id="Seg_844" s="T16">pers</ta>
            <ta e="T18" id="Seg_845" s="T17">num</ta>
            <ta e="T19" id="Seg_846" s="T18">n</ta>
            <ta e="T20" id="Seg_847" s="T19">adv</ta>
            <ta e="T21" id="Seg_848" s="T20">v</ta>
            <ta e="T22" id="Seg_849" s="T21">conj</ta>
            <ta e="T23" id="Seg_850" s="T22">adj</ta>
            <ta e="T24" id="Seg_851" s="T23">pers</ta>
            <ta e="T25" id="Seg_852" s="T24">v</ta>
            <ta e="T26" id="Seg_853" s="T25">nprop</ta>
            <ta e="T27" id="Seg_854" s="T26">pers</ta>
            <ta e="T28" id="Seg_855" s="T27">adv</ta>
            <ta e="T29" id="Seg_856" s="T28">v</ta>
            <ta e="T30" id="Seg_857" s="T29">n</ta>
            <ta e="T31" id="Seg_858" s="T30">v</ta>
            <ta e="T32" id="Seg_859" s="T31">adj</ta>
            <ta e="T33" id="Seg_860" s="T32">n</ta>
            <ta e="T34" id="Seg_861" s="T33">v</ta>
            <ta e="T35" id="Seg_862" s="T34">n</ta>
            <ta e="T36" id="Seg_863" s="T35">n</ta>
            <ta e="T37" id="Seg_864" s="T36">interrog</ta>
            <ta e="T38" id="Seg_865" s="T37">v</ta>
            <ta e="T39" id="Seg_866" s="T38">adj</ta>
            <ta e="T40" id="Seg_867" s="T39">n</ta>
            <ta e="T41" id="Seg_868" s="T40">conj</ta>
            <ta e="T42" id="Seg_869" s="T41">v</ta>
            <ta e="T43" id="Seg_870" s="T42">v</ta>
            <ta e="T44" id="Seg_871" s="T43">pers</ta>
            <ta e="T45" id="Seg_872" s="T44">n</ta>
            <ta e="T46" id="Seg_873" s="T45">v</ta>
            <ta e="T47" id="Seg_874" s="T46">conj</ta>
            <ta e="T48" id="Seg_875" s="T47">emphpro</ta>
            <ta e="T49" id="Seg_876" s="T48">v</ta>
            <ta e="T50" id="Seg_877" s="T49">nprop</ta>
            <ta e="T51" id="Seg_878" s="T50">interrog</ta>
            <ta e="T52" id="Seg_879" s="T51">pers</ta>
            <ta e="T53" id="Seg_880" s="T52">pers</ta>
            <ta e="T54" id="Seg_881" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_882" s="T54">v</ta>
            <ta e="T56" id="Seg_883" s="T55">n</ta>
            <ta e="T57" id="Seg_884" s="T56">v</ta>
            <ta e="T58" id="Seg_885" s="T57">n</ta>
            <ta e="T59" id="Seg_886" s="T58">adv</ta>
            <ta e="T60" id="Seg_887" s="T59">pers</ta>
            <ta e="T61" id="Seg_888" s="T60">v</ta>
            <ta e="T62" id="Seg_889" s="T61">interrog</ta>
            <ta e="T63" id="Seg_890" s="T62">pers</ta>
            <ta e="T64" id="Seg_891" s="T63">v</ta>
            <ta e="T65" id="Seg_892" s="T64">v</ta>
            <ta e="T66" id="Seg_893" s="T65">v</ta>
            <ta e="T67" id="Seg_894" s="T66">pers</ta>
            <ta e="T68" id="Seg_895" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_896" s="T68">v</ta>
            <ta e="T70" id="Seg_897" s="T69">dem</ta>
            <ta e="T71" id="Seg_898" s="T70">pers</ta>
            <ta e="T72" id="Seg_899" s="T71">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_900" s="T1">pro.h:S</ta>
            <ta e="T3" id="Seg_901" s="T2">v:pred</ta>
            <ta e="T6" id="Seg_902" s="T5">pro.h:S</ta>
            <ta e="T7" id="Seg_903" s="T6">v:pred</ta>
            <ta e="T11" id="Seg_904" s="T10">pro.h:S</ta>
            <ta e="T12" id="Seg_905" s="T11">v:pred</ta>
            <ta e="T13" id="Seg_906" s="T12">np:O</ta>
            <ta e="T15" id="Seg_907" s="T14">0.1.h:S v:pred</ta>
            <ta e="T17" id="Seg_908" s="T15">s:purp</ta>
            <ta e="T19" id="Seg_909" s="T18">np.h:S</ta>
            <ta e="T21" id="Seg_910" s="T20">v:pred</ta>
            <ta e="T24" id="Seg_911" s="T23">pro.h:O</ta>
            <ta e="T25" id="Seg_912" s="T24">0.3.h:S v:pred</ta>
            <ta e="T27" id="Seg_913" s="T26">pro.h:S</ta>
            <ta e="T29" id="Seg_914" s="T28">v:pred</ta>
            <ta e="T31" id="Seg_915" s="T30">v:O</ta>
            <ta e="T34" id="Seg_916" s="T33">0.3.h:S v:pred</ta>
            <ta e="T40" id="Seg_917" s="T36">s:temp</ta>
            <ta e="T42" id="Seg_918" s="T41">0.3.h:S v:pred</ta>
            <ta e="T43" id="Seg_919" s="T42">v:O</ta>
            <ta e="T45" id="Seg_920" s="T44">np:S</ta>
            <ta e="T46" id="Seg_921" s="T45">v:pred</ta>
            <ta e="T48" id="Seg_922" s="T47">pro.h:S</ta>
            <ta e="T49" id="Seg_923" s="T48">v:pred</ta>
            <ta e="T51" id="Seg_924" s="T50">pro.h:S</ta>
            <ta e="T52" id="Seg_925" s="T51">pro.h:O</ta>
            <ta e="T55" id="Seg_926" s="T54">v:pred</ta>
            <ta e="T57" id="Seg_927" s="T56">v:pred</ta>
            <ta e="T58" id="Seg_928" s="T57">np:S</ta>
            <ta e="T60" id="Seg_929" s="T59">pro.h:S</ta>
            <ta e="T61" id="Seg_930" s="T60">v:pred</ta>
            <ta e="T65" id="Seg_931" s="T61">s:compl</ta>
            <ta e="T66" id="Seg_932" s="T65">v:O</ta>
            <ta e="T69" id="Seg_933" s="T68">0.3.h:S v:pred</ta>
            <ta e="T71" id="Seg_934" s="T70">pro.h:S</ta>
            <ta e="T72" id="Seg_935" s="T71">v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_936" s="T0">np:Time</ta>
            <ta e="T2" id="Seg_937" s="T1">pro.h:A</ta>
            <ta e="T4" id="Seg_938" s="T3">np:G</ta>
            <ta e="T5" id="Seg_939" s="T4">adv:G</ta>
            <ta e="T6" id="Seg_940" s="T5">pro.h:A</ta>
            <ta e="T10" id="Seg_941" s="T9">np:L</ta>
            <ta e="T11" id="Seg_942" s="T10">pro.h:A</ta>
            <ta e="T13" id="Seg_943" s="T12">np:Th</ta>
            <ta e="T14" id="Seg_944" s="T13">np:Time</ta>
            <ta e="T15" id="Seg_945" s="T14">0.1.h:A</ta>
            <ta e="T17" id="Seg_946" s="T16">np:G</ta>
            <ta e="T19" id="Seg_947" s="T18">np.h:E</ta>
            <ta e="T24" id="Seg_948" s="T23">pro.h:Th</ta>
            <ta e="T25" id="Seg_949" s="T24">0.3.h:A</ta>
            <ta e="T27" id="Seg_950" s="T26">pro.h:E</ta>
            <ta e="T30" id="Seg_951" s="T29">np:Th</ta>
            <ta e="T31" id="Seg_952" s="T30">v:Th</ta>
            <ta e="T34" id="Seg_953" s="T33">0.3.h:A</ta>
            <ta e="T35" id="Seg_954" s="T34">np:Poss</ta>
            <ta e="T36" id="Seg_955" s="T35">np:L</ta>
            <ta e="T38" id="Seg_956" s="T37">0.3.h:A</ta>
            <ta e="T40" id="Seg_957" s="T39">np:P</ta>
            <ta e="T42" id="Seg_958" s="T41">0.3.h:E</ta>
            <ta e="T43" id="Seg_959" s="T42">v:Th</ta>
            <ta e="T44" id="Seg_960" s="T43">pro.h:Poss</ta>
            <ta e="T45" id="Seg_961" s="T44">np:Th</ta>
            <ta e="T48" id="Seg_962" s="T47">pro.h:P</ta>
            <ta e="T50" id="Seg_963" s="T49">np:G</ta>
            <ta e="T51" id="Seg_964" s="T50">pro.h:E</ta>
            <ta e="T52" id="Seg_965" s="T51">pro.h:Th</ta>
            <ta e="T56" id="Seg_966" s="T55">np:L</ta>
            <ta e="T58" id="Seg_967" s="T57">np:A</ta>
            <ta e="T59" id="Seg_968" s="T58">adv:So</ta>
            <ta e="T60" id="Seg_969" s="T59">pro.h:E</ta>
            <ta e="T63" id="Seg_970" s="T62">pro.h:P</ta>
            <ta e="T65" id="Seg_971" s="T64">np:G</ta>
            <ta e="T66" id="Seg_972" s="T65">v:Th</ta>
            <ta e="T67" id="Seg_973" s="T66">pro.h:Th</ta>
            <ta e="T69" id="Seg_974" s="T68">0.3.h:A</ta>
            <ta e="T71" id="Seg_975" s="T70">pro.h:P</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T10" id="Seg_976" s="T9">RUS:cult</ta>
            <ta e="T15" id="Seg_977" s="T14">RUS:calq</ta>
            <ta e="T22" id="Seg_978" s="T21">RUS:gram</ta>
            <ta e="T35" id="Seg_979" s="T34">RUS:cult</ta>
            <ta e="T41" id="Seg_980" s="T40">RUS:gram</ta>
            <ta e="T47" id="Seg_981" s="T46">RUS:gram</ta>
            <ta e="T58" id="Seg_982" s="T57">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T10" id="Seg_983" s="T9">dir:infl</ta>
            <ta e="T35" id="Seg_984" s="T34">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_985" s="T0">Позавчера мы ездили в Парабель.</ta>
            <ta e="T9" id="Seg_986" s="T4">Туда мы ехали шесть человек.</ta>
            <ta e="T13" id="Seg_987" s="T9">На рыбзаводе мы взяли сети.</ta>
            <ta e="T17" id="Seg_988" s="T13">К вечеру собрались ехать домой.</ta>
            <ta e="T21" id="Seg_989" s="T17">Один человек сильно опьянел.</ta>
            <ta e="T26" id="Seg_990" s="T21">А одна женщина, ее звали Груня.</ta>
            <ta e="T31" id="Seg_991" s="T26">Она очень любила водку пить.</ta>
            <ta e="T36" id="Seg_992" s="T31">Втихаря от людей села сзади катера.</ta>
            <ta e="T46" id="Seg_993" s="T36">Когда она выпила целых пол-литра, хотела встать, но у нее нога поскользнулась.</ta>
            <ta e="T50" id="Seg_994" s="T46">И сама упала в Обь.</ta>
            <ta e="T55" id="Seg_995" s="T50">Никто из нас не видел ее.</ta>
            <ta e="T58" id="Seg_996" s="T55">Позади шла самоходка.</ta>
            <ta e="T65" id="Seg_997" s="T58">Оттуда они видели, как она упала в воду.</ta>
            <ta e="T69" id="Seg_998" s="T65">Поймать ее не поймали.</ta>
            <ta e="T72" id="Seg_999" s="T69">Так она и утонула.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_1000" s="T0">The day before yesterday we went to Parabel.</ta>
            <ta e="T9" id="Seg_1001" s="T4">We went there six people.</ta>
            <ta e="T13" id="Seg_1002" s="T9">We took nets at the fish factory.</ta>
            <ta e="T17" id="Seg_1003" s="T13">In the evening we were going to return home.</ta>
            <ta e="T21" id="Seg_1004" s="T17">One man got very drunk.</ta>
            <ta e="T26" id="Seg_1005" s="T21">And one woman, her name was Grunya.</ta>
            <ta e="T31" id="Seg_1006" s="T26">She liked very much to drink vodka.</ta>
            <ta e="T36" id="Seg_1007" s="T31">Secretly from the people she sat at the rear of the boat.</ta>
            <ta e="T46" id="Seg_1008" s="T36">When she drank a whole half-liter [bottle], she wanted to get up, but her foot slipped.</ta>
            <ta e="T50" id="Seg_1009" s="T46">And she fell into Ob.</ta>
            <ta e="T55" id="Seg_1010" s="T50">None of us saw her.</ta>
            <ta e="T58" id="Seg_1011" s="T55">A boat was going behind.</ta>
            <ta e="T65" id="Seg_1012" s="T58">From there they saw how she fell into the water.</ta>
            <ta e="T69" id="Seg_1013" s="T65">They could not catch her.</ta>
            <ta e="T72" id="Seg_1014" s="T69">So she drowned.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_1015" s="T0">Vorgestern fuhren wir nach Parabel.</ta>
            <ta e="T9" id="Seg_1016" s="T4">Wir fuhren [mit] sechs Leuten dorthin.</ta>
            <ta e="T13" id="Seg_1017" s="T9">Bei der Fischfabrik nahmen wir Netze.</ta>
            <ta e="T17" id="Seg_1018" s="T13">Am Abend wollten wir zurück nach Hause fahren.</ta>
            <ta e="T21" id="Seg_1019" s="T17">Ein Mann war sehr betrunken.</ta>
            <ta e="T26" id="Seg_1020" s="T21">Und eine Frau, sie hieß Grunja.</ta>
            <ta e="T31" id="Seg_1021" s="T26">Sie mochte sehr gerne Wodka trinken.</ta>
            <ta e="T36" id="Seg_1022" s="T31">Unbemerkt von den Leuten setzte sie sich in das Heck vom Boot.</ta>
            <ta e="T46" id="Seg_1023" s="T36">Als sie eine ganze [Halbliter-]Flasche getrunken hatte, wollte sie aufstehen, aber ihr Fuß rutschte weg.</ta>
            <ta e="T50" id="Seg_1024" s="T46">Und sie fiel in den Ob.</ta>
            <ta e="T55" id="Seg_1025" s="T50">Niemand von uns sah sie.</ta>
            <ta e="T58" id="Seg_1026" s="T55">Hinter uns fuhr ein Propellerboot.</ta>
            <ta e="T65" id="Seg_1027" s="T58">Von dort sahn man, wie sie ins Wasser fiel.</ta>
            <ta e="T69" id="Seg_1028" s="T65">Sie konnten sie nicht greifen.</ta>
            <ta e="T72" id="Seg_1029" s="T69">So ertrank sie.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_1030" s="T0">позавчера мы ездили в Парабель</ta>
            <ta e="T9" id="Seg_1031" s="T4">туда мы ехали пять человек</ta>
            <ta e="T13" id="Seg_1032" s="T9">на рыбзаводе мы взяли сети</ta>
            <ta e="T17" id="Seg_1033" s="T13">к вечеру собрались ехать домой</ta>
            <ta e="T21" id="Seg_1034" s="T17">один человек сильно опьянел</ta>
            <ta e="T26" id="Seg_1035" s="T21">а женщина ее звали Груня</ta>
            <ta e="T31" id="Seg_1036" s="T26">она очень любила водку пить</ta>
            <ta e="T36" id="Seg_1037" s="T31">втихаря от людей села сзади катера</ta>
            <ta e="T46" id="Seg_1038" s="T36">когда выпила целое поллитра хотела встать у нее нога подскользнулась</ta>
            <ta e="T50" id="Seg_1039" s="T46">сама упала в реку</ta>
            <ta e="T55" id="Seg_1040" s="T50">никто ее из нас не видел</ta>
            <ta e="T58" id="Seg_1041" s="T55">позади шла</ta>
            <ta e="T65" id="Seg_1042" s="T58">оттуда они видели как она упала в воду</ta>
            <ta e="T69" id="Seg_1043" s="T65">поймать ее не поймали</ta>
            <ta e="T72" id="Seg_1044" s="T69">так она и утонула</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
